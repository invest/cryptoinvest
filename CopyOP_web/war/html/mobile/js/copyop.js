
var skinMapLocale = {
	'1': 'en',
	'2': 'en',
	'3': 'en',
	'4': 'en',
	'5': 'es',
	'8': 'de',
	'9': 'it',
	'10': 'en',
	'12': 'fr',
	'13': 'en-us',
	'14': 'es-us',
	'15': 'en',
	'16': 'en',
	'17': 'en',
	'18': 'es',
	'19': 'de',
	'20': 'it',
	'21': 'fr',
	'22': 'en',
	'23': 'en',
	'24': 'en'
}

var regulatedSkins = [16, 18, 19, 20, 21];
function getUrlValue(key){
	var query = window.location.search.substring(1);
	var vars = query.split('&');
	for (var i = 0; i < vars.length; i++) {
		var pair = vars[i].split('=');
		if(key == pair[0]) {
			return decodeURIComponent(pair[1]);
		}
	}
	return '';
}

var copyOpMobileApp = angular.module('copyOpMobileApp', ['ngSanitize']);

copyOpMobileApp.controller('bonusTermsController', ['$sce', '$http', '$scope', function($sce, $http, $scope) {
	settings.skinId = getUrlValue('s');
	settings.skinId = (settings.skinId == '') ? 2 : settings.skinId;
	settings.decimalPointDigits = 2;
	settings.isRegulated = skinMap[settings.skinId].isRgulated;
	var data = eval("("+unescape(getUrlValue('data'))+")");
	$scope.data = data;
	settings.currencySymbol = data.currency.defaultSymbol;
	settings.currencyLeftSymbol = data.currency.isLeftSymbol;
	var turnoverFactor = eval("("+unescape(getUrlValue('turnoverFactor'))+")");
	var reg = (regulatedSkins.indexOf(settings.skinId) > -1) ? 'reg-' : '';
	// var gUrl = "html/minisite/popUpBonusTermsIframe_" + skinMapLocale[settings.skinId] + ".html";

	// $http.get(gUrl)
		// .success(function(data, status, headers, config) {
			// g('body').innerHTML = data;
		// })
		
	$scope.msgs = {};
	var getMsgsJson_first = true;
	//get all translations (bundle)
	$scope.getMsgsJson = function() {
		var msgsUrl = settings.msgsPath + settings.msgsFileName + skinMap[skinMap[settings.skinId].skinIdTexts].locale + ((getMsgsJson_first)?"":settings.msgsEUsufix) + settings.msgsExtension;
		settings.skinLanguage = skinMap[skinMap[settings.skinId].skinIdTexts].locale;
		$http.get(msgsUrl)
			.success(function(data, status) {
				if (typeof data == 'object') {
					if (getMsgsJson_first) {
						$scope.msgs = data;
					} else {
						$scope.msgs = jsonConcat($scope.msgs, data);
					}
					if (settings.isRegulated && getMsgsJson_first) {
						getMsgsJson_first = false;
						$scope.getMsgsJson();
					} else {
						init();
					}
				}
			})
			.error(function( data, status ) {
				// handleNetworkError(data);
			});
	}
	
	//get translation from key
	$scope.getMsgs = function(key, params) {
		if (typeof key != 'object') {
			if ($scope.msgs.hasOwnProperty(key)) {
				if (typeof params != 'undefined') {
					if (!isUndefined(params._noTrust) && params._noTrust) {
						return $scope.msgsParam($scope.msgs[key], params);
					} else {
						return $sce.trustAsHtml($scope.msgsParam($scope.msgs[key], params));
					}
					// return $sce.parseAsResourceUrl($scope.msgsParam($scope.msgs[key], params));
					// return $scope.msgsParam($scope.msgs[key], params);
				} else {
					return $scope.msgs[key];
				}
			} else {
				return "?msgs[" + key + "]?";
			}
		} else {
			if ($scope.msgs.hasOwnProperty(key[0])) {
				if ($scope.msgs[key[0]].hasOwnProperty(key[1])) {
					if (typeof params != 'undefined') {
						return $sce.trustAsHtml($scope.msgsParam($scope.msgs[key[0]][key[1]], params));
					} else {
						return $scope.msgs[key[0]][key[1]];
					}
				} else {
					return "?msgs[" + key[0] + '.' + key[1] + "]?";
				}
			} else {
				return "?msgs[" + key[0] + "]?";
			}
		
		}
	}
	
	//get msg with param
	$scope.msgsParam = function(msg, params){//TODO: it's called multiple times for no good reason
		for (var key in params) {
			if (params.hasOwnProperty(key)) {
				if(params[key] && params[key].replace){
					msg = msg.replace(new RegExp('{' + key + '}', 'g'), params[key].replace(new RegExp('\\$', 'g'), '$$$$')); //Fix for IE and $0, $1, etc. problems with RegEx
				}else{
					msg = msg.replace(new RegExp('{' + key + '}', 'g'), params[key]);
				}
			}
		}
		return msg;
	}
	
	$scope.texts = [];
	function init() {
		$scope.gUrl = "html/minisite/popUpBonusTermsIframe_" + settings.skinLanguage + ".html";
		// $scope.$apply();
		
		var mainKey = 'terms-bonus-' + data.typeId;
		var msgs = $scope.msgs[mainKey];
		for (var key in msgs) {
			var addKey = true;
			var addKeySpecial = false;
			if (data.typeId == 3) {
				addKey = false;
				if (data.numOfActions == 1) {
					var bonusStateIdArr = [1,5,6,7,10];
					if (bonusStateIdArr.indexOf(data.bonusStateId) > -1) {
						var keyArr = ['p1','p2','p3','p4'];
						if (keyArr.indexOf(key) > -1) {
							addKey = true;
						}
					}
					bonusStateIdArr = [2,3,4];
					if (bonusStateIdArr.indexOf(data.bonusStateId) > -1) {
						var keyArr = ['p5','p6','p7','p8'];
						if (keyArr.indexOf(key) > -1) {
							addKey = true;
						}
					}
				} else {
					var bonusStateIdArr = [1,3,4,5,6,7,10];
					if (bonusStateIdArr.indexOf(data.bonusStateId) > -1) {
						var keyArr = ['p9','p10','p11','p12'];
						if (keyArr.indexOf(key) > -1) {
							addKey = true;
						}
					}
					bonusStateIdArr = [2];
					if (bonusStateIdArr.indexOf(data.bonusStateId) > -1) {
						var keyArr = ['p13','p14','p15','p16'];
						if (keyArr.indexOf(key) > -1) {
							addKey = true;
						}
					}
				}
				if (settings.skinId == 9) {
					var bonusStateIdArr = [2];
					if (bonusStateIdArr.indexOf(data.bonusStateId) > -1) {
						var keyArr = ['p22','p23','p24','p25','p26','p27','p28','p29'];
						if (keyArr.indexOf(key) > -1) {
							addKey = true;
						}
					}
				}
				var keyArr = ['p17','p18','p19','p20','p21'];
				if (keyArr.indexOf(key) > -1) {
					addKey = true;
				}
			} else if (data.typeId == 4) {
				addKey = false;
				if (!data.isHasSteps) {
					if (data.numOfActions == 1) {
						var bonusStateIdArr = [1,3,4,5,6,7,10];
						if (bonusStateIdArr.indexOf(data.bonusStateId) > -1) {
							var keyArr = ['p1','p2','p4','p5'];
							if (keyArr.indexOf(key) > -1) {
								addKey = true;
							}
							if (data.bonusAmount != 0) {
								var keyArr = ['p3'];
								if (keyArr.indexOf(key) > -1) {
									addKey = true;
								}
							}
						}
						var bonusStateIdArr = [2];
						if (bonusStateIdArr.indexOf(data.bonusStateId) > -1) {
							var keyArr = ['p6','p7','p8','p9'];
							if (keyArr.indexOf(key) > -1) {
								addKey = true;
							}
						}
						if (settings.skinId == 9) {
							var bonusStateIdArr = [2];
							if (bonusStateIdArr.indexOf(data.bonusStateId) > -1) {
								var keyArr = ['p33','p34','p35','p36'];
								if (keyArr.indexOf(key) > -1) {
									addKey = true;
								}
							}
						}
					} else if (data.numOfActions > 1) {
						var bonusStateIdArr = [1,3,4,5,6,7,10];
						if (bonusStateIdArr.indexOf(data.bonusStateId) > -1) {
							if (settings.skinId != 9) {
								var keyArr = ['p10','p11','p12','p13'];
								if (keyArr.indexOf(key) > -1) {
									addKey = true;
								}
							} else {
								var keyArr = ['p37','p38','p39','p41'];
								if (keyArr.indexOf(key) > -1) {
									addKey = true;
								}
								if (data.bonusAmount != 0) {
									var keyArr = ['p40'];
									if (keyArr.indexOf(key) > -1) {
										addKey = true;
									}
								}
							}
						}
						var bonusStateIdArr = [2];
						if (bonusStateIdArr.indexOf(data.bonusStateId) > -1) {
							var keyArr = ['p6','p7','p8','p9'];
							if (keyArr.indexOf(key) > -1) {
								addKey = true;
							}
						}
						if (settings.skinId != 9) {
							var bonusStateIdArr = [2];
							if (bonusStateIdArr.indexOf(data.bonusStateId) > -1) {
								var keyArr = ['p42','p43','p44','p45','p46','p47','p48','p49'];
								if (keyArr.indexOf(key) > -1) {
									addKey = true;
								}
							}
						}
					}
				} else {
					var bonusStateIdArr = [1,3,4,5,6,7,10];
					if (bonusStateIdArr.indexOf(data.bonusStateId) > -1) {
						var keyArr = ['p18','p19','p21','p22'];
						if (keyArr.indexOf(key) > -1) {
							addKey = true;
						}
						var keyArr = ['p18_1'];
						if (keyArr.indexOf(key) > -1) {
							addKey = true;
							addKeySpecial = true;
						}
						if (data.bonusAmount != 0) {
							var keyArr = ['p20'];
							if (keyArr.indexOf(key) > -1) {
								addKey = true;
							}
						}
					}
					var bonusStateIdArr = [2];
					if (bonusStateIdArr.indexOf(data.bonusStateId) > -1) {
						var keyArr = ['p23','p24','p25','p26'];
						if (keyArr.indexOf(key) > -1) {
							addKey = true;
						}
					}
				}
				var keyArr = ['p27'];
				if (keyArr.indexOf(key) > -1) {
					addKey = true;
				}
				if (!data.isHasSteps || settings.skinId == 9) {
					var keyArr = ['p28'];
					if (keyArr.indexOf(key) > -1) {
						addKey = true;
					}
				}
				if (settings.skinId != 9 && settings.skinId != 10) {
					if (data.isHasSteps) {
						var keyArr = ['p29'];
						if (keyArr.indexOf(key) > -1) {
							addKey = true;
						}
						var keyArr = ['p29_1'];
						if (keyArr.indexOf(key) > -1) {
							addKey = true;
							addKeySpecial = true;
						}
					}
					var keyArr = ['p30','p31','p32'];
					if (keyArr.indexOf(key) > -1) {
						addKey = true;
					}
				}
				var keyArr = ['p50','p51'];
				if (keyArr.indexOf(key) > -1) {
					addKey = true;
				}
			} else if (data.typeId == 5) {
				var bonusStateIdArr = [1,5,6,7,10];
				if (bonusStateIdArr.indexOf(data.bonusStateId) > -1) {
					var keyArr = ['p1','p2','p3'];
					if (keyArr.indexOf(key) > -1) {
						addKey = true;
					}
					if (data.bonusId != 320) {
						var keyArr = ['p4'];
						if (keyArr.indexOf(key) > -1) {
							addKey = true;
						}
					}
				}
				var bonusStateIdArr = [2,3,4];
				if (bonusStateIdArr.indexOf(data.bonusStateId) > -1) {
					var keyArr = ['p5','p6','p7'];
					if (keyArr.indexOf(key) > -1) {
						addKey = true;
					}
					if (data.bonusId != 320) {
						var keyArr = ['p8'];
						if (keyArr.indexOf(key) > -1) {
							addKey = true;
						}
					}
				}
				var keyArr = ['p9','p11','p14','p15'];
				if (keyArr.indexOf(key) > -1) {
					addKey = true;
				}
				if (data.bonusId != 320) {
					var keyArr = ['p10'];
					if (keyArr.indexOf(key) > -1) {
						addKey = true;
					}
				}
			} else if (data.typeId == 6 || data.typeId == 7) {
				var bonusStateIdArr = [1,3,4,5,6,7,10];
				if (bonusStateIdArr.indexOf(data.bonusStateId) > -1) {
					var keyArr = ['p1','p2','p3','p4'];
					if (keyArr.indexOf(key) > -1) {
						addKey = true;
					}
				}
				var bonusStateIdArr = [2];
				if (bonusStateIdArr.indexOf(data.bonusStateId) > -1) {
					var keyArr = ['p5','p6','p7','p8'];
					if (keyArr.indexOf(key) > -1) {
						addKey = true;
					}
				}
				var keyArr = ['p9','p10','p11','p12','p13','p17','p18'];
				if (keyArr.indexOf(key) > -1) {
					addKey = true;
				}
			}

			if (addKey) {
				if (!addKeySpecial) {
					$scope.texts.push($scope.msgsParam(msgs[key], {
						bonusAmount: formatAmount(data.bonusAmount, 0, true),
						sumInvQualify: formatAmount(data.sumInvQualify, 0, true),
						endDate: data.endDateTxt,
						endDates: data.endDateTxt,
						wageringParam: data.wageringParam,
						minDeposit: formatAmount(data.minDepositAmount, 0, true),
						numOfActions: data.numOfActions,
						bonusPercent: (data.bonusPercent * 100),
						formula: (data.wageringParam *  data.bonusPercent / 100),
						'TurnOverFactorPercent-3': turnoverFactor[2],
						'TurnOverFactorPercent-4': turnoverFactor[3],
						cash_balance_parameter: formatAmount(data.cashBalance, 0, true)
					}));
				} else {
					for (var j = 0; j < data.bonusSteps.length; j++) {
						$scope.texts.push($scope.msgsParam(msgs[key], {
							minDepositAmount: formatAmount(data.bonusSteps[j].minDepositAmount, 0, true),
							maxDepositAmount: formatAmount(data.bonusSteps[j].maxDepositAmount, 0, true),
							num: j+1,
							bonusPercent: (data.bonusSteps[j].bonusPercent * 100),
							wageringParam_bonusPercent: data.bonusSteps[j].wageringParam * data.bonusSteps[j].bonusPercent
						}));
					}
				}
			}
		}
	}
	
	$scope.getMsgsJson();
}]);