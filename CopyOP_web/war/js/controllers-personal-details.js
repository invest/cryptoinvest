copyOpApp.controller('updateCopyopProfileAvatarCtr', ['$rootScope', '$scope', '$http', function($rootScope, $scope, $http) {
	$scope.ready = true;
	
	function waitForIt() {
		if (!$scope.$parent.ready) {
			setTimeout(function(){waitForIt()},50);
		} else {
			if (settings.loggedIn) {//if not logged in don't make request
				init();
			}
		}
	}
	
	function init() {
		$scope.avatar = '';
		$rootScope.uploadedImage = $scope.cUser.avatar;
	}
	
	
	$scope.randAvatar = function(sex) {
		if (typeof sex == 'undefined') {
			sex = Math.floor((Math.random() * 2));
		}
		var avatar = '';
		switch (sex) {
			case 0: avatar = 'm' + Math.floor((Math.random() * settings.numOfMaleAvatars) + 1) + '.png'; break;
			case 1: avatar = 'f' + Math.floor((Math.random() * settings.numOfFemaleAvatars) + 1) + '.png'; break;
		}
		$scope.avatar = $rootScope.uploadedImage = settings.jsonImagegLink + 'avatar/' + avatar;
		$scope.updateNewAvatar();
	}
	
	$scope.updateNewAvatar = function() {
		if ($scope.avatar != '' && $scope.avatar == $rootScope.uploadedImage) {
			var UpdateAvatarMethodRequest = jsonClone(getUserMethodRequest(), {
				avatar: $scope.avatar
			});
			
			$http.post(settings.jsonLink + 'updateCopyopProfileAvatar', UpdateAvatarMethodRequest)
				.success(function(data, status, headers, config) {
					if (!handleErrors(data, 'updateCopyopProfileAvatar')) {
						$rootScope.copyopUser.copyopProfile.avatar = $scope.avatar;
						$scope.cUser.avatar = $scope.avatar;
						
						closeElement({id:'popUp-avatar'});
						$rootScope.openCopyOpPopup({
							type: 'info', 
							bodyTxt: $rootScope.getMsgs('success')
						});
					}
				})
				.error(function(data, status, headers, config) {
					handleNetworkError(data);
				})
		} else if ($rootScope.uploadedImage.search('data:') > -1){
			angular.element(g('startUpload')).scope().start(0);
		}
	}
	
	$scope.uploadComplate = function(data) {
		if (!handleErrors(data, 'uploadServiceCtr')) {
			var date = new Date().getTime();
			$rootScope.copyopUser.copyopProfile.avatar = data.errorMessages[0].message + "?t=" + date;
			$scope.cUser.avatar = data.errorMessages[0].message + "?t=" + date;
			
			$rootScope.openCopyOpPopup({
				type: 'info', 
				bodyTxt: $rootScope.getMsgs('success')
			});
		} else {
			//TODO show error
		}
	}
	
	waitForIt();
}]);

copyOpApp.controller('updateUserCtr', ['$rootScope', '$scope', '$http', function($rootScope, $scope, $http) {
	$scope.ready = false;
	function waitForIt() {
		if (!$scope.$parent.ready) {
			setTimeout(function(){waitForIt()},50);
		} else {
			if (settings.loggedIn) {//if not logged in don't make request
				init();
			}
		}
	}
	
	function init() {
		$scope.yearMinus18 = new Date().getFullYear() - 18;
		$scope.getCountries($rootScope.copyopUser.user.countryId);
		
		$scope.user = $rootScope.copyopUser.user;
		$scope.user.timeBirthDateDsp = new Date($rootScope.copyopUser.user.timeBirthDate);
		$rootScope.copyopUser.user.emailCompare = $rootScope.copyopUser.user.email.toUpperCase();
	}
	
	$scope.submitUpdateUser = function(_form) {
		if (_form.$valid) {
			if (!$scope.loading) {
				$scope.loading = true;
			
				error_stack = [];
				$rootScope.copyopUser.user.countryId = $rootScope.copyopUser.user.country.id;
				var UpdateUserMethodRequest = jsonClone(getUserMethodRequest(), {
					user: $rootScope.copyopUser.user
				});
				
				$http.post(settings.jsonLink + 'updateUser', UpdateUserMethodRequest)
					.success(function(data, status, headers, config) {
						$scope.loading = false;
						if (!handleErrors(data, 'updateUser')) {
							if (!isUndefined($rootScope.copyopUser.user.gender)) {
								$scope.user.genderTxt = ($rootScope.copyopUser.user.gender == 'M') ? $rootScope.getMsgs('male') : $rootScope.getMsgs('female');
							}
							$rootScope.openCopyOpPopup({
								type: 'info', 
								bodyTxt: data.userMessages[0].message
							});
						}
					})
					.error(function(data, status, headers, config) {
						$scope.loading = false;
						handleNetworkError(data);
					})
			}
		} else {
			logIt({'type':3,'msg':_form.$error});
		}
	}
	
	waitForIt();
}]);

copyOpApp.controller('changePasswordCtr', ['$rootScope', '$scope', '$http', 'vcRecaptchaService', function($rootScope, $scope, $http, recaptcha) {
	$scope.ready = true;
	$scope.form = {};
	
	$scope.response = null;
	$scope.widgetId = null;
	
	$scope.setWidgetId = function (widgetId) {
		$scope.widgetId = widgetId;
    };

    $scope.setResponse = function (response) {
		$scope.response = response;
    };

    $scope.cbExpiration = function() {
		$scope.response = null;
		recaptcha.reload($scope.widgetId);
    };
	
	$scope.checkRecaptchaError = function(_form){
		return (_form.$submitted && _form.$error.recaptcha);
	}
	
	$scope.submitChangePassword = function(_form) {
		if (_form.$valid) {
			if (!$scope.loading) {
				$scope.loading = true;
				
				var recaptchaResponse = $scope.response;
				
				var ChangePasswordMethodRequest = jsonClone(getUserMethodRequest(), {
					password: $scope.form.password,
					passwordNew: $scope.form.passwordNew,
					passwordReType: $scope.form.passwordReType,
					encrypt: false,
					captcha: recaptchaResponse
				});
				
				$http.post(settings.jsonLink + 'getChangePassword',  ChangePasswordMethodRequest)
					.success(function(data, status, headers, config) {
						$scope.loading = false;
						data.globalErrorField = 'globalErrorFieldPassword';
						if (!handleErrors(data, 'getChangePassword')) {
							$rootScope.closeCopyOpPopup();
							$rootScope.copyopUser.user.isNeedChangePassword = false;
							
							$scope.setUser(data);
							$rootScope.openCopyOpPopup({
								type: 'info', 
								bodyTxt: data.userMessages[0].message,
								onclose: function() {$scope.stateMachine();},
								onOk: function() {$scope.stateMachine();}
							});
							$scope.form = {};
							_form.$setPristine();
						}else{
							recaptcha.reload($scope.widgetId);
						}
					})
					.error(function(data, status, headers, config) {
						recaptcha.reload($scope.widgetId);
						$scope.loading = false;
						handleNetworkError(data);
					})
			}
		} else {
			logIt({'type':3,'msg':_form.$error});
		}
	}
}]);

copyOpApp.controller('userMandatoryQuestionnaireCtr', ['$rootScope', '$scope', '$http', function($rootScope, $scope, $http) {
	$scope.ready = false;
	$scope.hasError = true;
	
	$rootScope.$watch('showQuestionary', function() {
		if ($rootScope.showQuestionary) {
			$scope.loadMandatoryQuestionaire();
		}
	});
	
	$scope.isMandatoryQuestionaireCalled = false;
	$scope.loadMandatoryQuestionaire = function() {
		if($scope.isMandatoryQuestionaireCalled){
			return;
		}
		$scope.isMandatoryQuestionaireCalled = true;
		$scope.userMandatoryQuestionnaire = {};
		if ($scope.showMandatoryQuestionnaire()) {
			$scope.userMandatoryQuestionnaire = getUserMethodRequest();
			
			$http.post(settings.jsonLink + 'getUserMandatoryQuestionnaire', $scope.userMandatoryQuestionnaire)
				.success(function(data, status, headers, config) {
					if (!handleErrors(data, 'getUserMandatoryQuestionnaire')) {
						if (data.errorCode == errorCodeMap.regulation_suspended) {
							$scope.errorMsg = data.userMessages[0].message;
						} else {
							$scope.hasError = false;
							$scope.userMandatoryQuestionnaire.questions = data.questions;
							$scope.userMandatoryQuestionnaire.userAnswers = data.userAnswers;
							var cAnswers = [0,0,0,0,0,1,0];
							for (var i = 0; i < data.questions.length; i++) {
								$scope.userMandatoryQuestionnaire.userAnswers[i].answerId = data.questions[i].answers[cAnswers[i]].id;
								if (i == 6) {
									$scope.userMandatoryQuestionnaire.userAnswers[6].answerIdDsp = true;
								}
							}
						}
					}
				})
				.error(function(data, status, headers, config) {
					handleNetworkError(data);
				})
		}

		$scope.userMandatoryQuestionnaire.submitMQuestionnaire = function() {
			if (!$scope.loading) {
				$scope.loading = true;
				$scope.userMandatoryQuestionnaire.userAnswers[6].answerId = ($scope.userMandatoryQuestionnaire.userAnswers[6].answerIdDsp) ?
					$scope.userMandatoryQuestionnaire.questions[6].answers[0].id : 
					$scope.userMandatoryQuestionnaire.questions[6].answers[1].id;
			
				$http.post(settings.jsonLink + 'updateUserQuestionnaireAnswers', $scope.userMandatoryQuestionnaire)
					.success(function(data, status, headers, config) {
						$scope.loading = false;
						if (!handleErrors(data, 'updateUserQuestionnaireAnswers')) {
							$scope.userMandatoryQuestionnaire.updateMandatoryQuestionnaireDone();
							//handleSuccess(data);
						}
					})
					.error(function(data, status, headers, config) {
						$scope.loading = false;
						handleNetworkError(data);
					})
			}
		}
		
		$scope.userMandatoryQuestionnaire.updateMandatoryQuestionnaireDone = function() {
			if (!$scope.loading) {
				$scope.loading = true;
				
				$http.post(settings.jsonLink + 'updateMandatoryQuestionnaireDone', getUserMethodRequest())
					.success(function(data, status, headers, config) {
						$scope.loading = false;
						if (!handleErrors(data, 'updateMandatoryQuestionnaireDone')) {
							$rootScope.copyopUser.userRegulation.approvedRegulationStep = 3;
							//handleSuccess(data);
							//Open popUp
							$rootScope.openCopyOpPopup({
								type: 'confirm',
								body: 'CMS.reg_man_quest.label.reg_opt_quest.text', 
								onclose: function(){
									$rootScope.$state.go('ln.home.a', {ln: $scope.skinLanguage});
								},
								onOk: function(){
									$rootScope.copyopUser.userRegulation.firstTimeShowOptionalQuestionnaire = true;
								}
							});
						}
					})
					.error(function(data, status, headers, config) {
						$scope.loading = false;
						handleNetworkError(data);
					})
			}
		}
	}
}]);

copyOpApp.controller('userQuiestionaireOptionalCtr', ['$rootScope', '$scope', '$http', function($rootScope, $scope, $http) {
	$scope.ready = false;
	
	if($rootScope.copyopUser && $rootScope.copyopUser.userRegulation){
		$rootScope.copyopUser.userRegulation.firstTimeShowOptionalQuestionnaire = false;
	}
	
	$rootScope.$watch('copyopUser.userRegulation.firstTimeShowOptionalQuestionnaire', function() {
		if ($rootScope.copyopUser.userRegulation && $rootScope.copyopUser.userRegulation.firstTimeShowOptionalQuestionnaire) {
			$scope.loadOptionalQuestionaire();
		}
	});
	
	$scope.isOptionalQuestionaireCalled = false;
	$scope.loadOptionalQuestionaire = function() {
		if($scope.isOptionalQuestionaireCalled){
			return;
		}
		$scope.isOptionalQuestionaireCalled = true;
		if ($rootScope.copyopUser.userRegulation.approvedRegulationStep != 0) {
			$http.post(settings.jsonLink + 'getUserOptionalQuestionnaire', getUserMethodRequest())
				.success(function(data, status, headers, config) {
					if (!handleErrors(data, 'getUserOptionalQuestionnaire')) {
						if (data.errorCode == errorCodeMap.regulation_suspended) {
							$scope.errorMsg = data.userMessages[0].message;
						} else {
							$scope.userAnswersDsp = [];
							for (var i = 0; i < data.userAnswers.length; i++) {
								$scope.userAnswersDsp[i] = {};
							}
							$scope.userAnswersDsp[10].answered = !isUndefined(data.userAnswers[10].answerId);
							$scope.userAnswersDsp[11].answered = !isUndefined(data.userAnswers[11].answerId);
							$scope.userAnswersDsp[12].answered = !isUndefined(data.userAnswers[12].answerId);
							$scope.userAnswersDsp[13].answered = !isUndefined(data.userAnswers[13].answerId);
							$scope.userAnswers = data.userAnswers;
							$scope.questions = data.questions;
							
							$scope.questionsDesp = [];
							for (var j = 0; j < data.questions.length; j++) {
								$scope.questionsDesp[j] = [];
								for (var i = 0; i < data.questions[j].answers.length; i++) {
									$scope.questionsDesp[j].push({
										answerId: data.questions[j].answers[i].id, 
										name: $scope.getMsgs(data.questions[j].answers[i].name)
									});
									if (data.userAnswers[j].answerId == data.questions[j].answers[i].id) {
										$scope.userAnswersDsp[j].answerId = data.questions[j].answers[i].id;
										$scope.userAnswersDsp[j].name = $scope.getMsgs(data.questions[j].answers[i].name);
									}
								}
							}
						}
					}
				})
				.error(function(data, status, headers, config) {
					handleNetworkError(data);
				})
		}
		
		$scope.submitOQuestionnaire = function(submit, _form) {
			if (!$scope.loading) {
				$scope.loading = true;
				
				for (var i = 0; i < $scope.userAnswers.length; i++) {
					$scope.userAnswers[i].answerId = $scope.userAnswersDsp[i].answerId;
				}
				
				$scope.userAnswers[10].answerId = ($scope.userAnswersDsp[10].answered) ? $scope.questions[10].answers[0].id : null;
				$scope.userAnswers[11].answerId = ($scope.userAnswersDsp[11].answered) ? $scope.questions[11].answers[1].id : null;
				$scope.userAnswers[12].answerId = ($scope.userAnswersDsp[12].answered) ? $scope.questions[12].answers[2].id : null;
				$scope.userAnswers[13].answerId = ($scope.userAnswersDsp[13].answered) ? $scope.questions[13].answers[3].id : null;
				
				$http.post(settings.jsonLink + 'updateUserQuestionnaireAnswers', {userAnswers: $scope.userAnswers})
					.success(function(data, status, headers, config) {
						$scope.loading = false;
						if (!handleErrors(data, 'updateUserQuestionnaireAnswers')) {
							if (submit) {
								$scope.updateMandatoryQuestionnaireDone(_form);
							}
							//handleSuccess(data);
						}
					})
					.error(function(data, status, headers, config) {
						$scope.loading = false;
						handleNetworkError(data);
					})
			}
		}
		
		$scope.updateMandatoryQuestionnaireDone = function(_form) {
			if (_form.$valid) {
				if (!$scope.loading) {
					$scope.loading = true;
					
					$http.post(settings.jsonLink + 'updateOptionalQuestionnaireDone', getUserMethodRequest())
						.success(function(data, status, headers, config) {
							$scope.loading = false;
							if (!handleErrors(data, 'updateOptionalQuestionnaireDone')) {
								$rootScope.copyopUser.userRegulation.optionalQuestionnaireDone = true;
								//handleSuccess(data);
							}
						})
						.error(function(data, status, headers, config) {
							$scope.loading = false;
							handleNetworkError(data);
						})
				}
			} else {
				logIt({'type':3,'msg':$scope._form.$error});
			}
		}
	}
}]);

copyOpApp.controller('settingsCtr', ['$rootScope', '$scope', '$http', function($rootScope, $scope, $http) {
	$scope.ready = true;
	
	$scope.set = function() {
		
		var CancelInvestmentCheckboxStateMethodRequest = jsonClone(getUserMethodRequest(), {
			showCancelInvestment: !$rootScope.copyopUser.user.showCancelInvestment
		});
		$http.post(settings.jsonLink + 'changeCancelInvestmentCheckboxState', CancelInvestmentCheckboxStateMethodRequest)
			.success(function(data, status, headers, config) {
				if (!handleErrors(data, 'changeCancelInvestmentCheckboxState')) {
					settings.showCnacelInv = $rootScope.copyopUser.user.showCancelInvestment = !$rootScope.copyopUser.user.showCancelInvestment;
				}
			})
			.error(function(data, status, headers, config) {
				handleNetworkError(data);
			})
	}
}]);
