copyOpApp.controller('loggedUserCtr', ['$rootScope', '$scope', '$http', function($rootScope, $scope, $http) {
	$scope.ready = false;
	function waitForIt() {
		if (!$scope.$parent.ready) {
			setTimeout(function(){waitForIt()},50);
		} else {
			if (settings.loggedIn) {//if not logged in don't make request
				init();
			}
		}
	}
	waitForIt();
	
	function init() {
		$rootScope.cUser = {
			userId: $rootScope.copyopUser.user.id,
			name: $rootScope.copyopUser.user.name,
			avatar: $rootScope.copyopUser.copyopProfile.avatar,
			copyingCount: $rootScope.copyopUser.copyopProfile.copying,
			copiersCount: $rootScope.copyopUser.copyopProfile.copiers,
			watchingCount: $rootScope.copyopUser.copyopProfile.watching,
			watchersCount: $rootScope.copyopUser.copyopProfile.watchers,
			hitRate: $rootScope.copyopUser.copyopProfile.hitRatePercentage,
			marketsTotalResult: false
		}
		
		if (typeof $rootScope.cUser != 'undefined') {
			$rootScope.cUser.sameUser = true;
		}
		
		
		$scope.lastViewed = [];
		$http.post(settings.jsonLink + 'getLastViewedUsers', getUserMethodRequest())
			.success(function(data, status, headers, config) {
				if (!handleErrors(data, 'getLastViewedUsers')) {
					$scope.lastViewed = data.lastViewedUser;
					$scope.ready = true;
				}
			})
			.error(function(data, status, headers, config) {
				handleNetworkError(data);
			});
		$scope.showLastViewed = function() {
			return ($scope.lastViewed.length == 0)?false:true;
		}
	}
}]);

copyOpApp.controller('copyopProfileCtr', ['$rootScope', '$scope', '$http', function($rootScope, $scope, $http) {
	$scope.ready = false;
	function waitForIt() {
		if (!$scope.$parent.ready) {
			setTimeout(function(){waitForIt()},50);
		} else {
			if (settings.loggedIn) {//if not logged in don't make request
				init();
			}
		}
	}
	waitForIt();

	function init() {
		if ($rootScope.copyopUser.userId == $rootScope.$stateParams.userId) {
			var requestParams = getMethodRequest();
			var requestUrl = 'myCopyopProfile';
			var sameUser = true;
		} else {
			var requestParams = getProfileMethodRequest({userId: $rootScope.$stateParams.userId});
			var requestUrl = 'getCopyopProfile';
			var sameUser = false;
		}

		$http.post(settings.jsonLink + requestUrl, requestParams)
			.success(function(data, status, headers, config) {
				data.inUser = true;
				if (!handleErrors(data, requestUrl)) {
					try {
						data.bestAssetMarketResultDsp = formatAmount(data.bestAssetMarketResult, 0, true);
						data.timeCreatedDsp = new Date(data.timeCreated);
						data.name = data.nickname;
						data.userId = $rootScope.$stateParams.userId;
						data.sameUser = sameUser;
						data.streakResultDsp = formatAmount(data.streakResult, 0, true);
						data.copiedProfiles = (data.copiedProfiles == null) ? data.similarProfiles : data.copiedProfiles;
						data.copiedProfilesWidth = (data.copiedProfiles.length * 148) - 43 + 'px';
						data.copiedProfilesShow = (data.copiedProfiles.length > 5) ? false : true;
						if (data.bestAssetMarketId > 0) {
							data.bestAssetMarket = $scope.markets[data.bestAssetMarketId].displayName;
						}
					} catch(e) {
						logIt({'type':3,'msg':e});
					}
					
					if (data.lastInvestments != null) {
						for (var i = 0; i < data.lastInvestments.length; i++) {
							data.lastInvestments[i].followBtn = (parseFloat(data.lastInvestments[i].expiryLevel) == 0) ? true : false;
							data.lastInvestments[i].amountDsp = formatAmount(data.lastInvestments[i].amount,0);
							data.lastInvestments[i].timeCreated = adjustFromUTCToLocal(new Date(data.lastInvestments[i].timeCreated)).getTime();
							data.lastInvestments[i].link = false;
							data.lastInvestments[i].expiryLevel = (parseFloat(data.lastInvestments[i].expiryLevel) == 0) ? $rootScope.getMsgs('open-options') : data.lastInvestments[i].expiryLevel;
							
							if ((data.lastInvestments[i].typeId == 1 && data.lastInvestments[i].currentLevel < parseFloat(data.lastInvestments[i].expiryLevel.replace(/\s/g, '').replace(/,/g,''))) ||
								(data.lastInvestments[i].typeId == 2 && data.lastInvestments[i].currentLevel > parseFloat(data.lastInvestments[i].expiryLevel.replace(/\s/g, '').replace(/,/g,'')))) {
								data.lastInvestments[i].win = true;
							} else {
								data.lastInvestments[i].win = false;
							}
							data.lastInvestments[i].data = {properties: {}};
							// data.lastInvestments[i].data.properties[FMT.PROPERTY_KEY_TIME_LAST_INVEST] = '';
							data.lastInvestments[i].data.properties[FMT.PROPERTY_KEY_MARKET_ID] = data.lastInvestments[i].marketId;
							data.lastInvestments[i].data.properties[FMT.PROPERTY_KEY_DIRECTION] = data.lastInvestments[i].typeId;
							data.lastInvestments[i].data.properties[FMT.PROPERTY_KEY_INV_ID] = data.lastInvestments[i].id;
						}
					} else {
						data.lastInvestments = [];
					}

					data.avgTrades = Math.round(data.avgTrades * 10) / 10;
					if (sameUser) {
						data.name = data.firstName + " " + data.lastName;
						var date = new Date().getTime();
						data.avatar = data.avatar + "?t=" + date;
						data.topTitle = $rootScope.getMsgs('user.page.stats.h3_self');
						data.marketCopiedTotal = data.copiedInvestmentsTotalSuccess + "%";
						data.successMsg = $rootScope.getMsgs('success');
						data.header3 = "user-overview.h3.self";
						data.fbFriends = data.fbFriends;
						data.topMarketsTraders = [];
						for (var i = 0; i < data.topTraders.length; i++) {
							data.topMarketsTraders.push({
								img: data.topTraders[i].avatar,
								title: data.topTraders[i].nickname,
								count: data.topTraders[i].investmentsCount,
								result: data.topTraders[i].profit,
								resultDsp: formatAmount(data.topTraders[i].profit, 0, true)
							});
						}
						data.classType = (data.copiedInvestmentsTotalSuccess == 0) ? 2 : 1;
						$rootScope.cUser = data;
						$rootScope.cUser.sameUser = sameUser;
					} else {
						data.marketCopiedTotal = formatAmount(data.marketsTotalResult, 0, true);
						data.topTitle = $rootScope.getMsgs('user.page.stats.h3',{'nickname':data.nickname});
						data.fbFriends = data.fbFriendsWithLink;
						data.topMarketsTraders = [];
						if(data.similarProfiles == null){
							data.header3 = "user-overview.h3";
						}else{
							data.header3 = "user-overview.h3.similar-to";
						}
						if (data.marketsTotalResult > 0) {
							data.user_cost_you = $rootScope.getMsgs('user-profit', {amount: '<br />' + formatAmount(data.marketsTotalResult, 0, true)});
							data.user_cost_you_in = $rootScope.getMsgs('profit_from', {nickname: data.nickname, amount: ''});
							data.classType = 1;
						} else {
							if (data.topMarkets.length > 0) {
								data.user_cost_you = $rootScope.getMsgs('user-loss', {amount: '<br />' + formatAmount(data.marketsTotalResult, 0, true)});
							} else {
								// data.user_cost_you = $rootScope.getMsgs('user-profit', {amount: '<br />' + formatAmount(data.marketsTotalResult, 0, true)});
								data.user_cost_you = '';
							}
							data.user_cost_you_in = $rootScope.getMsgs('loss_from', {nickname: data.nickname, amount: ''});
							data.classType = 0;
							if (data.marketsTotalResult == 0) {
								data.classType = 2;
							}
						}
						
						for (var i = 0; i < data.topMarkets.length; i++) {
							data.topMarketsTraders.push({
								marketId: data.topMarkets[i].marketId,
								img: 'img/markets/35/micn' + data.topMarkets[i].marketId + '.png',
								title: $scope.markets[data.topMarkets[i].marketId].displayName,
								count: data.topMarkets[i].investmentsCount,
								result: data.topMarkets[i].result,
								resultDsp: formatAmount(data.topMarkets[i].result, 0, true)
							});
						}
						$rootScope.usersInfo[$rootScope.$stateParams.userId] = {
							copying: data.copying,
							watching: data.watching,
							name: data.nickname,
							frozen: data.frozen
						};
						
						if (typeof $rootScope.cUser == 'undefined') {
							$rootScope.cUser = {sameUser: sameUser}
						} else {
							$rootScope.cUser.sameUser = sameUser;
						}
					}
					data.userId = $rootScope.$stateParams.userId;
					$scope.cUser = $rootScope.userProfile = data;
					$scope.cUser.copiers = data.copiersCount;
					
					$scope.ready = true;
				}
			})
			.error(function(data, status, headers, config) {
				handleNetworkError(data);
			});
	}
}]);

copyOpApp.controller('copyopProfileOverviewCtr', ['$rootScope', '$scope', '$http', function($rootScope, $scope, $http) {
	$scope.tblSettings = {
		buttons: true,
		followBtn: true,
		colspan: 4
	}
	$scope.rowsOnPage = 3;
	$scope.orderField = 'timeCreated';
	
	$scope.sortAssetsParams = {
		openFirst: true,
		openSortField: 'timeEstClosing',
		openSortFieldType: 'date',
		openSortOrder: 'desc',
		settledSortField: 'timeEstClosing',
		settledSortFieldType: 'date',
		settledSortOrder: 'desc'
	}
	
	$scope.orderFieldWay = true;
	if (isUndefined($rootScope.cUser)) {
		$rootScope.cUser = {lastInvestments:[]};
	}
	
	$scope.ready = false;
	$scope.params = {isSettled: false};
	function waitForIt() {
		if (!$scope.$parent.ready) {
			setTimeout(function(){waitForIt()},50);
		} else {
			if (settings.loggedIn) {//if not logged in don't make request
				init();
			}
		}
	}
	function init() {
		$scope.ready = true;
		$scope.closingColumTxt = $rootScope.getMsgs('expiry');
	}
	
	$scope.showLastTrades = function() {
		if ($rootScope.userProfile.lastInvestments == 0) {
			return false;
		} else {
			$scope.investments = $rootScope.userProfile.lastInvestments;
			return true;
		}
	}
	waitForIt();
}]);

copyOpApp.controller('copyopProfileStatsCtr', ['$rootScope', '$scope', '$http', function($rootScope, $scope, $http) {
	$scope.ready = false;
	function waitForIt() {
		if (!$scope.$parent.ready) {
			setTimeout(function(){waitForIt()},50);
		} else {
			if (settings.loggedIn) {//if not logged in don't make request
				init();
			}
		}
	}
	waitForIt();

	function init() {
		$http.post(settings.jsonLink + 'getCopyopHits', getProfileMethodRequest({userId: $rootScope.$stateParams.userId}))
			.success(function(data, status, headers, config) {
				if (!handleErrors(data, 'getCopyopHits')) {
					$scope.copyopHits = data;
					$scope.copyopHits.lastTradeTimeDsp = new Date(data.lastTradeTime);
					

					if (data.assetResults != null) {
						$scope.chartData = {barCircleMobile:[{
								i: 0,
								index:'0.1',
								value: 100,
								fill: '#fff',
								label: ''
							}]}
						for (var i = 0; i < data.assetResults.length; i++) {
							$scope.chartData.barCircleMobile.push({
								i: i+1,
								index:'0.' + (i+1),
								border:'#d5d5d5',
								value: data.assetResults[i].hitRate,
								fill: settings.donutChartColors[i],
								label: (data.assetResults[i].marketId == 0) ? $rootScope.getMsgs('other') : $scope.markets[data.assetResults[i].marketId].displayName
							});
						}
						
						//Order the assets in reverse order - highest percent in the outer circle
						var barArr = [];
						Object.keys($scope.chartData.barCircleMobile).forEach(function (key) {
							$scope.chartData.barCircleMobile[key].name = key;
							barArr.push($scope.chartData.barCircleMobile[key]);
						});
						barArr.sort(function (a, b) {
							if(a.label == ''){ //The empty asset should be first
								return false;
							}else if(b.label == ''){ //The empty asset should be first
								return true;
							}else if(a.value == b.value){ //If the values are equal, preserve order by index
								return b.index - a.index;
							}else{
								return a.value - b.value;
							}
						});
						
						for(var i  = 0; i < barArr.length; i++){
							if(i == 0){
								barArr[i].index = 1/10;
							}else{
								barArr[i].index = i/10;
							}
						}
						
						//drawBarCircleChart($scope.chartData.barCircleMobile, "#asset-hit");
						drawBarCircleChart(barArr, "#asset-hit");
					} else {
						data.assetResults = [];
					}
					
					$scope.ready = true;
				}
			})
			.error(function(data, status, headers, config) {
				handleNetworkError(data);
			});
	}
}]);

copyOpApp.controller('copyopConnectionsCtr', ['$rootScope', '$scope', '$http', function($rootScope, $scope, $http) {
	$scope.ready = false;
	function waitForIt() {
		if (!$scope.$parent.ready || isUndefined($rootScope.cUser)) {
			setTimeout(function(){waitForIt()},50);
		} else {
			if (settings.loggedIn) {//if not logged in don't make request
				init();
			}
		}
	}
	waitForIt();
	
	function init() {
		var requestUrl = '';
				switch ($rootScope.$state.current.name) {
			case 'ln.in.a.user.connections.my-copiers': 
				requestUrl = 'getCopyopCopiers';
				$scope.title = "user.page.connections.h3.my-copiers";
				break;
			case 'ln.in.a.user.connections.my-watchers': 
				requestUrl = 'getCopyopWatchers'; 
				$scope.title = "user.page.connections.h3.my-watchers";
				break;
			case 'ln.in.a.user.connections.im-copying': 
				requestUrl = 'getCopyopCopying'; 
				$scope.title = "user.page.connections.h3.im-copying";
				break;
			case 'ln.in.a.user.connections.im-watching': 
				requestUrl = 'getCopyopWatching'; 
				$scope.title = "user.page.connections.h3.im-watching";
				break;
			case 'ln.in.a.user.connections.facebook': 
				requestUrl = 'getFacebookFriends'; 
				$scope.title = "user.page.connections.h3.facebook";
				break;
			default: logIt({'type':3,'msg':'Wrong connections page.'});
		}
		if (!$rootScope.cUser.sameUser && $rootScope.$state.current.name == 'ln.in.a.user.connections.facebook') {
			if ($rootScope.cUser.fbFriendsWithLink != null) {
				for (var i = 0; i < $rootScope.cUser.fbFriendsWithLink.length; i++) {
					var userId = $rootScope.cUser.fbFriendsWithLink[i].userId;
					$scope.users[i].nickname = $rootScope.cUser.fbFriendsWithLink[i].firstName + ' ' + $rootScope.cUser.fbFriendsWithLink[i].lastName;
					
					if (userId != null) {
						$rootScope.usersInfo[userId] = {
							copying: ((searchJsonKeyInArray($rootScope.copyopCopying, 'userId', userId) > -1) ? true : false),
							watching: ((searchJsonKeyInArray($rootScope.copyopWatching, 'userId', userId) > -1) ? true : false),
							name: $rootScope.cUser.fbFriendsWithLink[i].nickname,
							frozen: $rootScope.cUser.fbFriendsWithLink[i].frozen
						}
					}
				}
			}
			$scope.ready = true;
		} else {
			$http.post(settings.jsonLink + requestUrl, getProfileMethodRequest({userId: $rootScope.$stateParams.userId, resetPaging: true}))
				.success(function(data, status, headers, config) {
					if (!handleErrors(data, requestUrl)) {
						switch ($rootScope.$state.current.name) {
							case 'ln.in.a.user.connections.my-copiers': $scope.users = data.copyList; break;
							case 'ln.in.a.user.connections.my-watchers': $scope.users = data.watchList; break;
							case 'ln.in.a.user.connections.im-copying': $scope.users = data.copyList; break;
							case 'ln.in.a.user.connections.im-watching': $scope.users = data.watchList; break;
							case 'ln.in.a.user.connections.facebook': $scope.users = data.friends; break;
						}
						if ($scope.users != null) {
							for (var i = 0; i < $scope.users.length; i++) {
								var userId = $scope.users[i].userId;
								if ($rootScope.$state.current.name == 'ln.in.a.user.connections.facebook') {
									$scope.users[i].nickname = $scope.users[i].firstName + ' ' + $scope.users[i].lastName;
								}
								if ($rootScope.$state.current.name == 'ln.in.a.user.connections.im-copying') {
									if ($scope.users[i].costEarned >= 0) {
										$scope.users[i].costEarnedDsp = $rootScope.getMsgs('user-profit', {amount: formatAmount($scope.users[i].costEarned, 0)});
										$scope.users[i].className = 'profit';
									} else if ($scope.users[i].costEarned < 0) {
										$scope.users[i].costEarnedDsp = $rootScope.getMsgs('user-loss', {amount: formatAmount($scope.users[i].costEarned, 0)});
										$scope.users[i].className = 'loss';
									} else {
										$scope.users[i].className = 'none';
									}
								} else {
									$scope.users[i].className = 'none';
								}
								
								if (userId != null) {
									$rootScope.usersInfo[userId] = {
										copying: ((searchJsonKeyInArray($rootScope.copyopCopying, 'userId', userId) > -1) ? true : false),
										watching: ((searchJsonKeyInArray($rootScope.copyopWatching, 'userId', userId) > -1) ? true : false),
										name: $scope.users[i].nickname,
										frozen: $scope.users[i].frozen
									}
								}
							}
						}
						$scope.ready = true;
					}
				})
				.error(function(data, status, headers, config) {
					handleNetworkError(data);
				});
		}
	}
}]);
