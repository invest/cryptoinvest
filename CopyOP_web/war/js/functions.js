/*shorted document.getElementById() it's only g() in order to be used very easy even inline*/
function g(id){return document.getElementById(id);}

/*add and remove events*/
function addEvent(el,e,fn){
	if(el.addEventListener){
		el.addEventListener(e, fn, false);
	}else{
		el.attachEvent('on'+e, fn);
	}
}
function remEvent(el,e,fn){
	if(el.removeEventListener){
		el.removeEventListener(e, fn, false);
	}else{
		el.detachEvent('on'+e, fn);
	}
}
Element.prototype.remove = function() {
	this.parentElement.removeChild(this);
}
//use ?logType=num to load page with loggin on
//0:do not log,1:log info,2:log warnings,3:log errors,4:log all
var logIt_type_timer = ''; 
function logIt(params){//type,msg
	if((params.type === 1) && (settings.logIt_type === 1 || settings.logIt_type === 4)){//info
		console.info(params.msg);
	}
	else if((params.type === 2) && (settings.logIt_type === 2 || settings.logIt_type === 4)){//warnings
		console.warn(params.msg);
	}
	else if((params.type == 3) && (settings.logIt_type === 3 || settings.logIt_type === 4)){//errors
		console.error(params.msg);
	}
	// console.log('test.log') - just info
	// console.debug('test.debug') - just info
	// console.info('test.info') - with info ico
	// console.warn('test.warn') - with warning info
	// console.error('test.error') - with error ico
	if(logIt_type_timer == null){ 
		logIt_type_timer = setInterval(function(){console.clear()},1000*60*5);
	}
}

//disable annoying demo Lightstreamer alert popup
(function() {
	var proxied = window.alert;
	window.alert = function() {
		if (arguments[0].toString().toLowerCase().search('lightstreamer') > -1) {
			logIt({'type':1,'msg':arguments});
		} else {
			return proxied.apply(this, arguments);
		}
	};
})();

if (!Array.prototype.indexOf) {
  Array.prototype.indexOf = function (searchElement /*, fromIndex */ ) {
	'use strict';
	if (this == null) {
	  throw new TypeError();
	}
	var n, k, t = Object(this),
		len = t.length >>> 0;

	if (len === 0) {
	  return -1;
	}
	n = 0;
	if (arguments.length > 1) {
	  n = Number(arguments[1]);
	  if (n != n) { // shortcut for verifying if it's NaN
		n = 0;
	  } else if (n != 0 && n != Infinity && n != -Infinity) {
		n = (n > 0 || -1) * Math.floor(Math.abs(n));
	  }
	}
	if (n >= len) {
	  return -1;
	}
	for (k = n >= 0 ? n : Math.max(len - Math.abs(n), 0); k < len; k++) {
	  if (k in t && t[k] === searchElement) {
		return k;
	  }
	}
	return -1;
  };
}
/*start*/
function openElement(params){
	var closed = false;
	if(typeof params.id != "undefined"){
		if(typeof params.e != "undefined"){
			var t = closeOnClickOut_params.target = params.e.target || params.e.srcElement;
		} else {
			var t = false;
		}
		if(closeOnClickOut_params.current_id == params.id){
			closeElement({target: t, 'id':closeOnClickOut_params.current_id});
			if (t) {
				t.className = t.className.replace(/ active/g, '');
			}
			closed = true;
		}else{
			g(params.id).style.display = "block";
			if (t) {
				t.className += ' active';
			}
		}
	}
	else if(typeof params.span != "undefined"){
		if(closeOnClickOut_params.current_el == params.span){
			closeElement({'id':closeOnClickOut_params.current_id,'span':closeOnClickOut_params.current_el});
			closed = true;
		}else{
			var ul = params.span.parentNode.getElementsByTagName('ul')[0];
			params.span.className += " active";
			ul.style.display = "block";
			if((typeof ul.id == 'undefined')||(typeof ul.id == null)||(ul.id == '')){
				params.id = 'rand_id_'+Math.round(Math.random()*1000000);
				ul.id = params.id;
			}else{
				params.id = ul.id;
			}
		}
	}
	if(params.autoClose && !closed){
		setTimeout(function(){closeOnClickOut({target: t, 'id':params.id,'span':params.span,'callBack':params.callBack});},20);
	}
}
function closeElement(params){//el,top_id,id
	if (params.target) {
		params.target.className = params.target.className.replace(/ active/g, '');
	}
	if(typeof params.last != "undefined"){
		if(closeOnClickOut_params.current_id != null){
			closeElement({'id':closeOnClickOut_params.current_id,'span':closeOnClickOut_params.current_el});
		}
	}
	else if(typeof params.id != "undefined"){//close by id
		var el = g(params.id);
		if (el != null){
			g(params.id).style.display = "none";
		}
		if(typeof params.span != "undefined"){//close drop down menu with ul
			params.span.className = params.span.className.replace(/ active/g,'');
		}
	}
	else if(typeof params.el != "undefined"){//close simple pop up
		params.el.parentNode.style.display = "none";
	}
	else if((typeof params.top_id != "undefined")&&(params.top_id != "")){//close using this and top level
		getTopLevelById(params.el,params.top_id).style.display = "none";
	}
	remEvent(window.document.body,'click',closeOnClickOutMain);
	closeOnClickOut_params = {target: null, 'current_id':null,'current_el':null,'callBack':null};
}
var closeOnClickOut_params = {target: null, 'current_id':null,'current_el':null,'callBack':null};
function closeOnClickOut(params){//id,callBack,el
	closeOnClickOut_params = {target: params.target, 'current_id':params.id,'current_el':params.span,'callBack':params.callBack};
	addEvent(window.document.body,'click',closeOnClickOutMain);
}
function closeOnClickOutMain(e){
	var t = e.target || e.srcElement;
	while(t){
		if(t.id == closeOnClickOut_params.current_id){
			return false;
		}
		else{
			t=t.parentNode;
		}
	}
	if(typeof closeOnClickOut_params.callBack != "undefined"){
		eval(closeOnClickOut_params.callBack);
		remEvent(window.document.body,'click',closeOnClickOutMain);
	}
	else{
		closeElement({target: closeOnClickOut_params.target, 'id':closeOnClickOut_params.current_id,'span':closeOnClickOut_params.current_el});
	}
	closeOnClickOut_params = {'current_id':null,'current_el':null,'callBack':null};
}
/*end*/

function drawBarCircleChart(data,target,values,labels){
	var w = 250,
		h = 250,
		size = data[0].value * 1,
		radius = 250,
		sectorWidth = .1,
		radScale = 25,
		sectorScale = 1.45,
		target = d3.select(target),
		valueText = d3.select(values),
		labelText = d3.select(labels);


	var arc = d3.svg.arc()
		.innerRadius(function(d,i){return (d.index/sectorScale) * radius + radScale; })
		.outerRadius(function(d,i){return ((d.index/sectorScale) + (sectorWidth/sectorScale)) * radius + radScale; })
		.startAngle(Math.PI*2)
		.endAngle(function(d) { return Math.PI*2 + (d.value / size) * 2 * Math.PI; });
	
	var path = target.selectAll("path")
		.data(data);

	//TODO: seperate color and index from data object, make it a pain to update object order
	path.enter().append("svg:path")
		.attr("fill",function(d,i){return d.fill})
		.attr("stroke",function(d,i){return d.border})
		.transition()
		.ease("elastic")
		.duration(1000)
		.delay(function(d,i){return i*100})
		.attrTween("d", arcTween);
		
	valueText.selectAll("tspan").data(data).enter()
		.append("tspan")
		.attr({
			x:50,
			y:function(d,i){return i*14},       
			"text-anchor":"end"
		})
		.text(function(d,i){return data[i].value});
	
	labelText.selectAll("tspan").data(data).enter()
		.append("tspan")
		.attr({
			x:0,
			y:function(d,i){return i*14}
		})
		.text(function(d,i){return data[i].label});

	function arcTween(b) {
		var i = d3.interpolate({value: 0}, b);
		return function(t) {
			return arc(i(t));
		};
	}
}

function cloneObj(obj){
	if(obj == null || typeof(obj) != 'object')
		return obj;

	var temp = obj.constructor(); // changed

	for(var key in obj)
		temp[key] = cloneObj(obj[key]);
	return temp;
}

function getCookie(c_name){
	var c_value = document.cookie;
	var c_start = c_value.indexOf(" " + c_name + "=");
	if (c_start == -1) {
		c_start = c_value.indexOf(c_name + "=");
	}
	if (c_start == -1) {
		c_value = null;
	} else {
		c_start = c_value.indexOf("=", c_start) + 1;
		var c_end = c_value.indexOf(";", c_start);
		if (c_end == -1) {
			c_end = c_value.length;
		}
		c_value = unescape(c_value.substring(c_start,c_end));
	}
	return c_value;
}

function jsonClone(o1, o2) {
	var json = cloneObj(o1);
	for (var key in o2) {
		json[key] = o2[key];
	}
	return json;
}

function jsonConcat(o1, o2) {
	for (var key in o2) {
		o1[key] = o2[key];
	}
	return o1;
}

var error_stack = [];
function handleErrors(data, name) {//returns true on error
	if (!isUndefined(angular.element('html').scope().requestsStatus)) {
		angular.element('html').scope().requestsStatus[data.serviceName] = 1;
	}
	if (data == '') {
		logIt({'type':3,'msg':name});
		logIt({'type':3,'msg':data});
		alert('Response is empty string. Check eclipse console for exeptions.');
		return true;
	} else if (data.errorCode == null) {
		logIt({'type':3,'msg':name});
		logIt({'type':3,'msg':data});
		alert('Error code is null. Check eclipse console for exeptions. ');
		return true;
	} else if (data.errorCode > errorCodeMap.success) {//0
		var defMsg = angular.element('html').scope().getMsgs('error-' + data.errorCode);
		logIt({'type':3,'msg':name + " (" + defMsg + ")"});
		logIt({'type':3,'msg':data});
		var globalErrorField;
		if (typeof data.globalErrorField != 'undefined') {
			globalErrorField = g(data.globalErrorField);
		} else if (g('globalErrorField') != null) {
			globalErrorField = g('globalErrorField');
		} else {
			globalErrorField = document.createElement('span');
			globalErrorField.id = 'globalErrorField';
			document.body.appendChild(globalErrorField);
		}
		globalErrorField.innerHTML = '';
		resetErrorMsgs(data);
		if (data.errorCode == errorCodeMap.regulation_suspended) { //susspended user 301
			try {
				angular.element('html').scope().closeCopyOpPopup();
				angular.element('html').scope().openCopyOpPopup({type: 'info', body: 'error.investment.user.suspended', header: 'info-popup-header'});
			} catch(e) {}
		} else if (data.errorCode == errorCodeMap.withdrawal_close) { //weekend withdraw 110
			return false;
		} else if (data.errorCode == errorCodeMap.regulation_missing_questionnaire) { //regulation missing questionnaire 300
			try {
				angular.element('html').scope().closeCopyOpPopup();
				angular.element('html').scope().openQuestionary();
			} catch(e) {}
		} else if (data.errorCode == errorCodeMap.regulation_suspended_documents) { // 302
			try {
				angular.element('html').scope().closeCopyOpPopup();
				angular.element('html').scope().stateMachine(data);
			} catch(e) {}
		} else if (angular.element('html').scope().copyopUser.user.countryId == countries.spain && (data.errorCode == errorCodeMap.regulation_user_restricted || data.errorCode == errorCodeMap.reg_suspended_quest_incorrect)) { //suspended cnmv
			try {
				angular.element('html').scope().closeCopyOpPopup();
				angular.element('html').scope().openCopyOpPopup({type: 'regulationStatus', errorCode: data.errorCode, txtStatus: 'suspended-cnmv'});
			} catch(e) {}
		} else if (angular.element('html').scope().copyopUser.user.countryId == countries.spain && data.errorCode == errorCodeMap.regulation_suspended_cnmv_docs) { //suspended cnmv 313 - require documents
			try {
				angular.element('html').scope().closeCopyOpPopup();
				angular.element('html').scope().openCopyOpPopup({type: 'regulationStatus', errorCode: data.errorCode, txtStatus: 'suspended-cnmv-docs'});
			} catch(e) {}
		} else if (data.errorCode == errorCodeMap.regulation_user_restricted || data.errorCode == errorCodeMap.reg_suspended_quest_incorrect) { // 304, 305
			try {
				angular.element('html').scope().closeCopyOpPopup();
				angular.element('html').scope().openCopyOpPopup({type: 'regulationStatus', errorCode: data.errorCode, txtStatus: 'restricted'});
			} catch(e) {}
		} /*else if (data.errorCode == errorCodeMap.reg_suspended_quest_incorrect) { // 305
			try {
				angular.element('html').scope().closeCopyOpPopup();
				angular.element('html').scope().openCopyOpPopup({type: 'regulationStatus', errorCode: data.errorCode, txtStatus: 'blocked'});
			} catch(e) {}
		}*/ else if (data.errorCode == errorCodeMap.regulation_user_pep_prohibited) { // 307
			try {
				angular.element('html').scope().closeCopyOpPopup();
				angular.element('html').scope().openCopyOpPopup({type: 'regulationStatus', errorCode: data.errorCode, txtStatus: 'blocked-pep'});
			} catch(e) {}
		} else if (data.errorCode == errorCodeMap.regulation_user_is_treshold_block) { // 308
			try {
				angular.element('html').scope().closeCopyOpPopup();
				angular.element('html').scope().openCopyOpPopup({type: 'regulationStatus', errorCode: data.errorCode, txtStatus: 'restricted-loss'});
			} catch(e) {}
		} else if (data.errorCode == errorCodeMap.user_not_regulated) { //not regulated 303
			alert('User not regulated');
		} else if (data.errorCode == errorCodeMap.session_expired) { //session expired 419
			if (isUndefined(data.loginPage)) {
				try {
					angular.element('html').scope().changeHeader(false);
					angular.element('html').scope().$state.go('ln.login', {ln: settings.skinLanguage});
					angular.element('html').scope().initSettings();
				} catch(e) {}
			}
		} else if (data.errorCode == errorCodeMap.not_accepted_terms) { //terms not accepted 701
			angular.element('html').scope().$state.go('ln.anyoption', {ln: settings.skinLanguage});
			return false;
		} else if (data.errorCode == errorCodeMap.user_removed) { //removed user 800
			angular.element('html').scope().openCopyOpPopup({
				type: 'info', 
				header: 'error',
				body: 'error',
				onclose: function(){angular.element('html').scope().$state.go('ln.home.a', {ln: settings.skinLanguage});}
			});
			if (!isUndefined(data.loginNotCheck) && !data.loginNotCheck) {
				angular.element('html').scope().loginCheck();
			}
			return false;
		} else if (data.errorCode == errorCodeMap.copyop_max_copiers) { //fully copyed 1001
			angular.element('html').scope().openCopyOpPopup({type: 'fullyCopyied'});
		} else {
			if (data.userMessages != null) {
				for (var i = 0; i < data.userMessages.length; i++) {
					if (data.userMessages[i].field == '' || data.userMessages[i].field == null) {
						globalErrorField.innerHTML += "<span>" + data.userMessages[i].message + "</span>";
					} else {
						var input = g(data.userMessages[i].field);
						if (input != null) {
							input.className += " error";
						}
						var error_el_id = data.userMessages[i].field + '-error';
						error_stack.push(data.userMessages[i].field);
						var err_place = g(error_el_id);
						if (err_place != null) {
							err_place.innerHTML = data.userMessages[i].message;
						} else {
							var field_place = g(data.userMessages[i].field + '-container');
							if (field_place != null) {
								var el = document.createElement('span');
								el.id = error_el_id;
								el.className = 'error-message icon';
								el.innerHTML = data.userMessages[i].message;
								field_place.appendChild(el);
							} else {
								globalErrorField.innerHTML += "<span>" + data.userMessages[i].message.replace(data.rplElm, data.rplWith) + "</span>";
							}
						}
					}
				}
			} else {
				logIt({'type':3,'msg':'unhandled errorCode: ' + data.errorCode});
				globalErrorField.innerHTML += "<span>" + defMsg + "</span>";
			}
		}
		return true;
	} else {
		logIt({'type':1,'msg':data});
		if (!isUndefined(data.loginNotCheck) && !data.loginNotCheck) {
			angular.element('html').scope().loginCheck();
		}
		return false;
	}
}

function handleSuccess(data) {
	if (data.userMessages != null && data.userMessages[0].message != null) {
		alert(data.userMessages[0].message);
	} else {
		alert('Great, it works!')
	}
	logIt({'type':1,'msg':data});
}

function handleNetworkError(data) {
	// alert('Network error (aka 404, 500 etc)');
	logIt({'type':1,'msg':data});
}

function resetErrorMsgs(data) {
	if (typeof data.stopClear == 'undefined' || !data.stopClear) {
		for (var i = 0; i < error_stack.length; i++) {
			var error_holder = g(error_stack[i]);
			if (error_holder != null) {
				g(error_stack[i]).className = g(error_stack[i]).className.replace(/ error/g, '');
			}
			var error_holder = g(error_stack[i] + '-error');
			if (error_holder != null) {
				g(error_stack[i] + '-error').remove();
			}
		}
		error_stack = [];
	}
}

function searchJsonKeyInArray(arr, key, val) {
	if (!isUndefined(arr)) {
		for (var i = 0; i < arr.length; i++) {
			if (arr[i] != null && arr[i][key] == val) {
				return i;
			}
		}
	}
	return -1;
}
function searchJsonKeyInJson(obj, key, val) {
	if (!isUndefined(obj)) {
		for (var el in obj) {
			if (obj[el][key] == val) {
				return el;
			}
		}
	}
	return -1;
}
//chart needed
function formatAmount(amount, centsPart, withoutDecimalPoint, round) {//centsPart: 1-upper span,2-no cents if 00,0-default show full format with .00,3- upper span without 00
	try{
		if (!isNaN(amount)) {
			var rtn = '';
			if (amount.toString().charAt(0) == '-') {
				rtn = '-';
				amount *= -1;
			}
			if (typeof withoutDecimalPoint != 'undefined' && withoutDecimalPoint) {
				amount = amountToFloat(amount);
			}
			if (typeof round == 'undefined' || !round || amount < 1000 ) {
				amount = Math.round(amount*100)/100;
				var tmp = amount.toString().split('.');
				var decimal = parseInt(tmp[0]);
				var cents = (typeof tmp[1] != 'undefined')?tmp[1]:0;
				if((cents === '') || (cents === 0)){cents = '00';}
				else if(cents.length == '1'){cents = cents + '0';}
				else if(cents.length == '2' && parseInt(cents) < 10){cents = '0' + parseInt(cents);}
				
				if(settings.currencyLeftSymbol){
					rtn += settings.currencySymbol;
				}
				rtn += numberWithCommas(decimal);
				if(settings.decimalPointDigits == 0){
					//Absolutly nothing. Just chill!
				}
				else if((centsPart == 1) || ((centsPart == 3) && (parseInt(cents) != 0))){
					rtn += "<span>"+cents+"</span>";
				}
				else if((centsPart == 3) && (parseInt(cents) == 0)){
					//Absolutly nothing. Just chill!
				}
				else if((centsPart == 2) && (parseInt(cents) == 0)){
					//Absolutly nothing. Just chill!
				}
				else{
					rtn += "."+cents;
				}
				if(!settings.currencyLeftSymbol){
					rtn += settings.currencySymbol;
				}
			} else {
				if(settings.currencyLeftSymbol){
					rtn += settings.currencySymbol;
				}
				rtn += (Math.round(amount / 100)) / 10 + "K";
				if(!settings.currencyLeftSymbol){
					rtn += settings.currencySymbol;
				}
			}
			return rtn;
		} else {
			return ' ';
		}
	} catch(e) {
		logIt({'type':3,'msg':e});
	}
}
//chart needed
function numberWithCommas(x) {//convert value with camas every 1000
	x = x.toString().replace(/,/g,'');
	var tmp = x.split('.');
	var rtn = tmp[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
	if(typeof tmp[1] != 'undefined'){
		rtn += '.' + tmp[1];
	}
	return rtn;
}

//facebook
(function(d, s, id) {
	var js, fjs = d.getElementsByTagName(s)[0];
	if (d.getElementById(id)) return;
	js = d.createElement(s); js.id = id;
	js.src = "//connect.facebook.net/en_US/sdk.js";
	fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));

window.fbAsyncInit = function() {
	FB.init({
		appId      : settings.fbAppId,
		cookie     : true,  // enable cookies to allow the server to access 
							// the session
		xfbml      : true,  // parse social plugins on this page
		version    : 'v2.1' // use version 2.1
	});
};
try{
	if (settings.isLive) {
		(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start': new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
		j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src='//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
		})(window,document,'script','dataLayer','GTM-RWQ5');
	}
} catch (e){}

function validateEmail(email) {
	var emailValidationRegEx = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
	return emailValidationRegEx.test(email);
}
//get url value from key (?key=value)
function getUrlValue(key){
	var params = (window.location.search == '') ? settings.url_params : window.location.search;
	var query = params.substring(1);
	var vars = query.split('&');
	for (var i = 0; i < vars.length; i++) {
		var pair = vars[i].split('=');
		if(key == pair[0]){
			return decodeURIComponent(pair[1]);
		}
	}
	return '';
}

//convert amount from valur without cents to one with one
function amountToFloat(amount) {
	return (amount/Math.pow(10,settings.decimalPointDigits));
}
//detect credit card type
function detectCCtype(card, fullCheck){
	var cardTypes = {
		unknown: 'unknown',
		visa: 2,
		mastercard: 1,
		solo: 'solo',
		switch_type: 'switch',
		maestro: 6,
		amex: 5,
		discover: 'discover',
		diners: 3,
		isracard: 4,
		allcards: 'allcards',
		cup: 7
	}
	card = card.toString().replace(/-/g,'').replace(/ /g,'');
	//visa - starts with 4
	var part = parseInt(card.substr(0, 1));
	if(part == 4){
		if(fullCheck){
			if((card.length == 13 || card.length == 16) && luhnValidate(card)){
				return cardTypes.visa;
			}
			else{
				return cardTypes.unknown;
			}
		}
		return cardTypes.visa;
	}
	//Mastercard - starts with 51,52,53,54,55
	var part = parseInt(card.substr(0, 2));
	if(part >= 51 && part <= 55){
		if(fullCheck){
			if(card.length == 16 && luhnValidate(card)){
				return cardTypes.mastercard;
			}
			else{
				return cardTypes.unknown;
			}
		}
		return cardTypes.mastercard;
	}
	//Solo - starts with 6334,6767
	var part = parseInt(card.substr(0, 4));
	if(part == 6334 || part == 6767){
		if(fullCheck){
			if(card.length >= 16 && card.length <= 19 && luhnValidate(card)){
				return cardTypes.solo;
			}
			else{
				return cardTypes.unknown;
			}
		}
		return cardTypes.solo;
	}
	//Switch - starts with 4903,4905,4911,4936,6333,6759 or 564182,633110
	var part_arr = [4903,4905,4911,4936,6333,6759];
	var part2_arr = [564182,633110];
	var part = parseInt(card.substr(0, 4));
	var part2 = parseInt(card.substr(0, 6));
	if(part_arr.indexOf(part) > -1 || part2_arr.indexOf(part2) > -1){
		if(fullCheck){
			if(card.length >= 16 && card.length <= 19 && luhnValidate(card)){
				return cardTypes.switch_type;
			}
			else{
				return cardTypes.unknown;
			}
		}
		return cardTypes.switch_type;
	}
	//Maestro - starts with 50,56,57,58,59,60,62,63,64,67,90
	var part_arr = [50,56,57,58,59,60,62,63,64,67,90];
	var part = parseInt(card.substr(0, 2));
	if(part_arr.indexOf(part) > -1){
		if(fullCheck){
			if(card.length >= 16 && card.length <= 18 && luhnValidate(card)){
				return cardTypes.maestro;
			}
			else{
				return cardTypes.unknown;
			}
		}
		return cardTypes.maestro;
	}
	//Amex - starts with 34,37
	var part_arr = [34,37];
	var part = parseInt(card.substr(0, 2));
	if(part_arr.indexOf(part) > -1){
		if(fullCheck){
			if(card.length == 15 && luhnValidate(card)){
				return cardTypes.amex;
			}
			else{
				return cardTypes.unknown;
			}
		}
		return cardTypes.amex;
	}
	//Discover - starts with 6011
	var part_arr = [6011];
	var part = parseInt(card.substr(0, 4));
	if(part_arr.indexOf(part) > -1){
		if(fullCheck){
			if(card.length == 16 && luhnValidate(card)){
				return cardTypes.discover;
			}
			else{
				return cardTypes.unknown;
			}
		}
		return cardTypes.discover;
	}
	//Diners - starts with 300,301,302,303,304,305 or 36
	var part_arr = [300,301,302,303,304,305];
	var part2_arr = [36];
	var part = parseInt(card.substr(0, 3));
	var part2 = parseInt(card.substr(0, 2));
	if(part_arr.indexOf(part) > -1 || part2_arr.indexOf(part2) > -1){
		if(fullCheck){
			if(card.length == 14 && luhnValidate(card)){
				return cardTypes.diners;
			}
			else{
				return cardTypes.unknown;
			}
		}
		return cardTypes.diners;
	}
	//CUP - starts with 62
	var part_arr = [62];
	var part = parseInt(card.substr(0, 2));
	if(part_arr.indexOf(part) > -1){
		if(fullCheck){
			if(card.length >= 16 && card.length <= 19 && luhnValidate(card)){
				return cardTypes.cup;
			}
			else{
				return cardTypes.unknown;
			}
		}
		return cardTypes.diners;
	}
	//Isracard - complicated :)
	if(fullCheck){
		var cardIsr = card;
		if(cardIsr.length == 8){
			cardIsr = "0" + cardIsr;
		}
		else if(cardIsr.length > 9){
			cardIsr = cardIsr.substr(0, 9);
		}
		
		if(cardIsr.length == 9){
			var diff = "987654321",
				sum = 0,
				a = 0,
				b = 0,
				c = 0;
			for(var i=1;i<9;i++){
				sum += parseInt(diff.substr(i,1))*parseInt(cardIsr.substr(i,1));
			}
			if(sum % 11 == 0){
				return cardTypes.isracard;
			}
		}
	}
	//allcard
	if(card.length >= 12 && card.length <= 20 && luhnValidate(card)){
		return cardTypes.allcards;
	}
	return cardTypes.unknown;
}
// The Luhn algorithm is basically a CRC type
// system for checking the validity of an entry.
// All major credit cards use numbers that will
// pass the Luhn check. Also, all of them are based
// on MOD 10.

function luhnValidate(value) {
  // accept only digits, dashes or spaces
	if (/[^0-9-\s]+/.test(value)) return false;
 
	// The Luhn Algorithm. It's so pretty.
	var nCheck = 0, nDigit = 0, bEven = false;
	value = value.replace(/\D/g, "");
 
	for (var n = value.length - 1; n >= 0; n--) {
		var cDigit = value.charAt(n),
			  nDigit = parseInt(cDigit, 10);
 
		if (bEven) {
			if ((nDigit *= 2) > 9) nDigit -= 9;
		}
 
		nCheck += nDigit;
		bEven = !bEven;
	}
 
	return (nCheck % 10) == 0;
}

/*file: clock.js*/
function tS(){
	var x = new Date();
	
	if (null == diffTimeMs) {
		diffTimeMs = settings.serverTime - x.getTime();
	}
	var diffTimeZoneOffsetMs = offset + (x.getTimezoneOffset() * 60 * 1000);

	x.setTime(x.getTime() + diffTimeMs + diffTimeZoneOffsetMs + settings.dstOffset);
	return x;
}
function lZ(x){ return (x>9)?x:'0'+x; }
function dT(){ g('header_clock').innerHTML=eval(oT); setTimeout('dT()',1000); }
function getTimeString() {
	return lZ(tS().getHours())+':'+lZ(tS().getMinutes())+':'+lZ(tS().getSeconds()) + ', ' + day + '.' + monthName +'.' + year ;
}
var time = new Date();
var offset = time.getTimezoneOffset()*60*1000*(-1);
var diffTimeMs = null;
var dateFormat = new Date();
var day = dateFormat.getDate();
var month = dateFormat.getMonth();
var year = dateFormat.getFullYear();
var monthNames = new Array('01','02','03','04','05','06','07','08','09','10','11','12');
var monthName = monthNames[month];
var oT = "";
function startClock(){
	oT="lZ(tS().getHours())+':'+lZ(tS().getMinutes())+':'+lZ(tS().getSeconds()) + ', ' + day + '.' + monthName + '.' + year ";
	if (g('header_clock') == null) {
		setTimeout(function(){startClock()}, 500);
		return;
	}
	dT();

	var timeStr = getTimeString();
	var clockId = g("header_clock");
	clockId.innerHTML = timeStr;
}
function switchColor(color) {
	switch(color){
		case '0':return " below";
		case '1':return " same";
		case '2':return " above";
	}
}
function cardFilter(cardNum) {
	cardNum = cardNum.toString();
	var cardNumLength = cardNum.length;
	var newCardNum = '';
	for (var i = 0; i < cardNumLength; i++) {
		if (i < cardNumLength - 4) {
			newCardNum += "X";
		} else {
			newCardNum += cardNum[i];
		}
	}
	return newCardNum;
}
function setColumsHeight() {
	var size = screenDimensions();
	var rc = g('rightColumHeight');
	var lc = g('leftColumHeight');
	var header = g('header').offsetHeight;
	var footer = g('footer').offsetHeight;
	var inContent = g('inContent');
	var mainContainer = g('main-container');
	if (inContent != null && mainContainer != null) {
		mainContainer = g('main-container').offsetHeight;
		inContent = inContent.offsetHeight;
		if (rc != null) {
			var newsHeader = g('news-header').offsetHeight;
			var h = 0;
			var contPlus = mainContainer + header + footer;
			if (size.h > contPlus) {
				h = size.h - header - footer - newsHeader;
			} else {
				h = mainContainer - newsHeader;
			}
			rc.style.height = h + 'px'
		}
		if (lc != null) {
			var newsHeader = g('left-header').offsetHeight;
			var h = 0;
			var contPlus = mainContainer + header + footer;
			if (size.h > contPlus) {
				h = size.h - header - footer - newsHeader - 30;
			} else {
				h = mainContainer - newsHeader - 30;
			}
			lc.style.height = h + 'px'
		}
	}
	
	//Code to place the footer at the bottom of the page
	if(g("header") && g("footer")){
		var contentHeight = "100%";
		contentHeight = $(window).height() - $("#header").outerHeight() - $("#footer").outerHeight();
		if(contentHeight >= $("#main-container").outerHeight()){
			$(".main-content").css("height", contentHeight);
			if($(".highlights")){
				$(".highlights").css("min-height", contentHeight);
			}
		}else{
			$(".main-content").css("height", "");
			if($(".highlights")){
				$(".highlights").css("min-height", "");
			}
		}
	}
}
function screenDimensions() {
	var rtn = {w: 0, h: 0};
	if (document.body && document.body.offsetWidth) {
		rtn.w = document.body.offsetWidth;
		rtn.h = document.body.offsetHeight;
	}
	if (document.compatMode=='CSS1Compat' && document.documentElement && document.documentElement.offsetWidth ) {
		rtn.w = document.documentElement.offsetWidth;
		rtn.h = document.documentElement.offsetHeight;
	}
	if (window.innerWidth && window.innerHeight) {
		rtn.w = window.innerWidth;
		rtn.h = window.innerHeight;
	}
	return rtn;
}
var gd_timerSlider = null;
function gd_slideShow(dir, id){
	clearTimeout(gd_timerSlider);
	var id = g(id);
	var holder_W = id.parentNode.offsetWidth;
	var scr = holder_W;
	var offW = id.offsetWidth;
	var offL = id.offsetLeft;
	if((scr > (offW-holder_W+offL))&&(dir == "right")){
		scr = offW+offL-holder_W;
	}
	if((scr > offL)&&(dir == "left")){
		scr = offL*-1;
	}
	var inc=0,riscr=0;
	var scr_left = scr;

	var doSetTimeout = function(){
		inc = Math.round(scr_left*0.2);
		if(inc < 1){inc = 1};
		if(dir == "left"){riscr+=inc;id.style.left = offL+riscr+"px";}
		else if(dir == "right"){riscr+=inc;id.style.left = offL-riscr+"px";}
		scr_left -= inc;

		if(scr_left > 0){
			timerSlider = setTimeout(doSetTimeout,30);
		}
		else{

		}
	}
	if(scr_left > 0){
		doSetTimeout();
	}
}
//check where ever something is undefined or null
function isUndefined(el) {
	return (typeof el == 'undefined' || el == null);
}
//
function adjustTimeToBrowser(time, addOffset) {
	if (!isNaN(time)) {
		return new Date(time);
	} else {
		if (isNaN(new Date(time).getTime())) {
			var newTime = time.split(' ')
			var time_date = newTime[0].split('/');
			time = new Date(time_date[2] + '/' + time_date[1] + '/' + time_date[0] + ' ' + newTime[1]);
		}
		if (addOffset) {
			return adjustFromUTCToLocal(new Date(time)).getTime() - settings.serverOffsetMillis;
		} else {
			return adjustFromUTCToLocal(new Date(time)).getTime();
		}
	}
}
function adjustFromUTCToLocal(toAdj) {
	var adjM = toAdj.getMinutes() - new Date().getTimezoneOffset();
	var adjH = 0;
	if (adjM >= 0) {
		adjH = Math.floor(adjM / 60);
		adjM = adjM % 60;
	} else {
		adjH = Math.floor(Math.abs(adjM) / 60);
		if (Math.abs(adjM) % 60 != 0) {
			adjH += 1;
			adjM = 60 - Math.abs(adjM) % 60;
		} else {
			adjM = Math.abs(adjM) % 60;
		}
		adjH = -adjH;
	}
	toAdj.setHours(toAdj.getHours() + adjH);
	toAdj.setMinutes(adjM);
	return toAdj;
}

function detectDevice() {
	if (/iPhone|iPad|iPod/i.test(navigator.userAgent)) {
		return 'ios';
	} else if (/Android/i.test(navigator.userAgent)) {
		return 'android';
	} else {
		return '';
	}
}

/** Custom scroller - start */
/*
* Usage - There must be a parent element of class "scroll-wrapper", with only one direct child of class "scroll-content".
* The element with class "scroll-content" must not have an overflow-y set.
*/
function eventPreventDefaultAndBubble(e){
	e.stopPropagation ? e.stopPropagation() : (e.cancelBubble=true);
	e.preventDefault ? e.preventDefault() : (e.returnValue=false);
	return false;
}

function isOutside(evt, parent) {
	var elem = evt.relatedTarget || evt.toElement || evt.fromElement || evt.target;
	
	while(elem && elem !== parent){
		elem = elem.parentNode;
	}
	
	if(elem !== parent){
		return true;
	}
} 

var xPos = 0;
var yPos = 0;
function fixPageXY(e) {
	if (e.pageX == null && e.clientX != null ) { 
		var html = document.documentElement
		var body = document.body
		
		e.pageX = e.clientX + (html.scrollLeft || body && body.scrollLeft || 0)
		e.pageX -= html.clientLeft || 0
		
		e.pageY = e.clientY + (html.scrollTop || body && body.scrollTop || 0)
		e.pageY -= html.clientTop || 0
	}
}

function scrollerAdd(el, doNotUpdateContentHeight){
	if(!isUndefined(el.className) && (el.className.search("scroll-wrapper") > -1) && (el.hasCustomScroller !== true)){
		el.hasCustomScroller = true;
		el.doNotUpdateContentHeight = doNotUpdateContentHeight;
		
		//Check if there is a child of the required class
		var scrollContent = null;
		for(var i = 0; i < el.childNodes.length; i++){
			if(!isUndefined(el.childNodes[i].className) && (el.childNodes[i].className.search("scroll-content") > -1)){
				scrollContent = el.childNodes[i];
				break;
			}
		}
		if(scrollContent == null){
			return;
		}else{
			el.scrollContent = scrollContent;
		}
		
		el.scrollbarsWidth = getScrollBarWidth();
		
		
		//Create the DOM elements
		var scrollScrollerContainer = document.createElement("div");
		scrollScrollerContainer.className = "scroll-scroller-container";
		
		var scrollScroller = document.createElement("div");
		scrollScroller.className = "scroll-scroller";
		
		scrollScrollerContainer.appendChild(scrollScroller);
		el.appendChild(scrollScrollerContainer);
		
		//Create references to the newly created elements in the el object
		el.scrollerContainer = scrollScrollerContainer;
		el.scroller = scrollScroller;
		
		//Calculate heights
		scrollerRecalculateMetrics(el);
		
		scrollerSetScroll(el);
		
		//Mouse events
		el.scroller.ondragstart = function(){return false;}
		el.scroller.onclick = function(e){
			return eventPreventDefaultAndBubble(e);
		}
		el.scroller.onmousedown = function(emd){
			window.isScrolling = true;
			
			//Preserve old document events
			var oldDocumentOnselectstart = document.onselectstart;
			var oldDocumentOnmousemove = document.onmousemove;
			
			document.onselectstart = function(){return false;}
			el.onmouseout = null;
			
			emd = emd || event;
			fixPageXY(emd);
			xPos = emd.pageX;
			yPos = emd.pageY;
			
			//Prevent propagation of events
			eventPreventDefaultAndBubble(emd);
			
			var startTop = isNaN(parseFloat(el.scroller.style.top)) ? 0 : parseFloat(el.scroller.style.top);
			var lastDelta = 0;
			document.onmousemove = function(e){
				e = e || event;
				fixPageXY(e);
				
				//Calculate the distance the scroller should travel
				var delta = e.pageY - yPos;
				if((startTop + delta) < 0){
					delta = -startTop;
				}else if((startTop + delta + el.scrollerHeight) > el.visibileHeight){
					delta = el.visibileHeight - startTop - el.scrollerHeight;
				}
				
				if(((startTop + lastDelta <= 0) && (startTop + delta <= 0)) || (((startTop + lastDelta + el.scrollerHeight) >= el.visibileHeight) && ((startTop + delta + el.scrollerHeight) >= el.visibileHeight))){
					//No need to do anything, nothing new happened
				}else{
					el.scroller.style.top = startTop + delta + "px";
					el.scrollContent.scrollTop = (startTop + delta)/el.heightRatio;
				}
				lastDelta = delta;
			}
			
			document.onmouseup = function(e) {
				window.isScrolling = false;
				
				e = e || event;
				fixPageXY(e);
				
				eventPreventDefaultAndBubble(e);
				
				document.onmousemove = oldDocumentOnmousemove;
				document.onselectstart = oldDocumentOnselectstart;
				el.onmouseout = function(e){
					if(isOutside(e, el)){
						scrollerHide(el);
					}
				}
				if(isOutside(e, el)){
					scrollerHide(el);
				}
			}
		}
		
		el.scrollerContainer.ondragstart = function(){return false;}
		el.scrollerContainer.onclick = function(e){
			return eventPreventDefaultAndBubble(e);
		}
		el.scrollerContainer.onmousedown = function(emd){
			emd = emd || event;
			
			yOffset = emd.offsetY || emd.layerY;
			
			var goDown = false;
			if(parseFloat(el.scroller.style.top) < yOffset){
				goDown = true;
			}
			
			//Prevent propagation of events
			eventPreventDefaultAndBubble(emd);
			
			bigScroll(goDown);
			
			var iterations = 0;
			var interval = setInterval(function(){littleScroll(el, goDown);}, 10);
			
			el.scrollerContainer.onmouseup = function(e){
				clearInterval(interval);
			}
			
			function bigScroll(goDown){
				var step = el.scroller.clientHeight/el.heightRatio;
				if(goDown){
					el.scrollContent.scrollTop += step;
				}else{
					el.scrollContent.scrollTop -= step;
				}
			}
			function littleScroll(el, goDown){
				iterations++;
				if(iterations > 50){
					var step = (el.scroller.clientHeight/el.heightRatio)/30;
					var scrollReachedClickPoint = false;
					if(goDown && (parseFloat(el.scroller.style.top) + el.scroller.clientHeight >= yOffset)){
						scrollReachedClickPoint = true;
					}else if(!goDown && (parseFloat(el.scroller.style.top) <= yOffset)){
						scrollReachedClickPoint = true;
					}
					if(scrollReachedClickPoint){
						clearInterval(interval);
					}else{
						if(goDown){
							el.scrollContent.scrollTop += step;
						}else{
							el.scrollContent.scrollTop -= step;
						}
					}
				}
			}
			
			return false;
		}
		
		el.onmouseover = function(e){scrollerShow(el);}
		el.onmouseout = function(e){
			if(isOutside(e, el)){
				scrollerHide(el);
			}
		}
		el.scrollContent.onscroll = function(e){
			e = e || event;
			fixPageXY(e);
			
			if(!isOutside(e, el.scrollContent)){
				scrollerSetScroll(el);
			}
		}
	}
}

function scrollerHide(el){
	el.scrollerContainer.style.transition = "opacity 0.5s ease";
	el.scrollerContainer.style.opacity = "0";
}

function scrollerShow(el){
	scrollerRecalculateMetrics(el);
	if(el.heightRatio < 1){
		el.scrollerContainer.style.transition = "opacity 0.5s ease";
		el.scrollerContainer.style.opacity = "1";
	}else{
		scrollerHide(el);
	}
}

function scrollerRecalculateMetrics(el){
	var wrapperBorderTopWidth = parseFloat(getComputedStyle(el,null).getPropertyValue('border-top-width'));
	var wrapperBorderBottomWidth = parseFloat(getComputedStyle(el,null).getPropertyValue('border-bottom-width'));
	var contentBorderTopWidth = parseFloat(getComputedStyle(el.scrollContent,null).getPropertyValue('border-top-width'));
	var contentBorderBottomWidth = parseFloat(getComputedStyle(el.scrollContent,null).getPropertyValue('border-bottom-width'));
	var wrapperPaddingTop = parseFloat(getComputedStyle(el,null).getPropertyValue('padding-top'));
	var wrapperPaddingBottom = parseFloat(getComputedStyle(el,null).getPropertyValue('padding-bottom'));
	
	el.scrollContent.style.overflowY = "scroll";
	
	el.visibileHeight = el.clientHeight - wrapperPaddingTop - wrapperPaddingBottom;
	if(!el.doNotUpdateContentHeight){
		el.scrollContent.style.height = el.visibileHeight + "px";
	}
	
	el.totalHeight = el.scrollContent.scrollHeight;
	el.heightRatio = el.visibileHeight/el.totalHeight;
	el.scrollerHeight = el.visibileHeight*el.heightRatio;
	el.scroller.style.height = el.scrollerHeight + "px";
	
	el.scrollContent.style.width = "calc(100% + " + el.scrollbarsWidth + "px)";
	
	el.scrollerContainer.style.height = el.visibileHeight - (wrapperBorderTopWidth + wrapperBorderBottomWidth + contentBorderTopWidth + contentBorderBottomWidth) + "px";
	el.scrollerContainer.style.top = wrapperBorderTopWidth + wrapperPaddingTop + contentBorderTopWidth + "px";
}

function scrollerSetScroll(el){
	el.scroller.style.top = el.scrollContent.scrollTop*el.heightRatio + "px";
}

function getScrollBarWidth(){
	var inner = document.createElement('p');
	inner.style.width = "100%";
	inner.style.height = "200px";

	var outer = document.createElement('div');
	outer.style.position = "absolute";
	outer.style.top = "0px";
	outer.style.left = "0px";
	outer.style.visibility = "hidden";
	outer.style.width = "200px";
	outer.style.height = "150px";
	outer.style.overflow = "hidden";
	outer.appendChild (inner);

	document.body.appendChild (outer);
	var w1 = inner.offsetWidth;
	outer.style.overflow = 'scroll';
	var w2 = inner.offsetWidth;
	if (w1 == w2) w2 = outer.clientWidth;

	document.body.removeChild (outer);

	return (w1 - w2);
};
/** Custom scroller - end */

function addOverlay(overlayId, overlayClassName, hideBodyScroll){
	removeOverlay(overlayId, overlayClassName);
	var overlay = document.createElement('div');
	overlay.id = overlayId;
	overlay.className = overlayClassName;
	overlay.style.height = document.body.offsetHeight + "px";
	overlay.style.width = document.body.offsetWidth + "px";
	document.body.appendChild(overlay);
	if(hideBodyScroll){
		//Hide scrollbars
		document.documentElement.style.height = "100%";
		document.documentElement.style.overflow = "hidden";
	}
}

function removeOverlay(overlayId, overlayClassName, showBodyScroll){
	if(g(overlayId)){
		document.body.removeChild(g(overlayId));
		if(showBodyScroll){
			//Show scrollbars
			document.documentElement.style.height = "100%";
			document.documentElement.style.overflow = "auto";
		}
	}
}

function getVisibleHeight(selector){    
    var $el = $(selector),
        scrollTop = $(this).scrollTop(),
        scrollBot = scrollTop + $(this).height(),
        elTop = $el.offset().top,
        elBottom = elTop + $el.outerHeight(),
        visibleTop = elTop < scrollTop ? scrollTop : elTop,
        visibleBottom = elBottom > scrollBot ? scrollBot : elBottom;
    return (visibleBottom - visibleTop);
}

function getVisibleWidth(selector){    
    var $el = $(selector),
        scrollLeft = $(this).scrollLeft(),
        scrollRight = scrollLeft + $(this).width(),
        elLeft = $el.offset().left,
        elRight = elLeft + $el.outerWidth(),
        visibleLeft = elLeft < scrollLeft ? scrollLeft : elLeft,
        visibleRight = elRight > scrollRight ? scrollRight : elRight;
    return (visibleRight - visibleLeft);
}


/** Outline element - start */
var outlineContainerId = "outline-overlay-container";
var outlineContainerClassName = "outline-overlay-container";
var outlineBlockClassName = "outline-overlay";
var outlineTransparentBlockClassName = "outline-overlay-transparent";

function addOutline(id, scroll, callback, topBarId, padding){
	var $el = $("#" + id);
	var metrics = {};
	
	if(g(id)){
		if($el.offset() && scroll){
			$("html, body").stop(true);
			$("html, body").animate({ scrollTop: $el.offset().top - 100 }, 600);
		}
		
		metrics = recalculateOutline(id, topBarId, padding);
		recalculateOutlineBlocks(metrics);
		
		var outlineContainer = g(outlineContainerId);
		if(!isUndefined(outlineContainer)){
			var result;
			clearInterval(outlineContainer.recalculateInterval);
			outlineContainer.recalculateInterval = setInterval(function(){
					var metrics = {};
					try{
						metrics = recalculateOutline(id, topBarId, padding);
						recalculateOutlineBlocks(metrics);
					}catch(e){
						logIt({'type':3,'msg':e});
					}
					if(callback && typeof callback == 'function'){
						result = callback(metrics);
						if (!result) {
							removeOutline();
						}
					}
				}, 500);
		}
	}
	
	return metrics;
}

function recalculateOutline(id, topBarId, padding){
	var $el = $("#"+id);
	var metrics = {};
	
	if(g(id)){
		var paddingTop = 0;
		var paddingBottom = 0;
		var paddingLeft = 0;
		var paddingRight = 0;
		if(!isUndefined(padding)){
			if(!isUndefined(padding.top)){paddingTop = parseInt(padding.top);}
			if(!isUndefined(padding.bottom)){paddingBottom = parseInt(padding.bottom);}
			if(!isUndefined(padding.left)){paddingLeft = parseInt(padding.left);}
			if(!isUndefined(padding.right)){paddingRight = parseInt(padding.right);}
		}
		
		metrics.windowScrollTop = $(window).scrollTop();
		metrics.windowScrollLeft = $(window).scrollLeft();
		
		metrics.height = $("#"+id).outerHeight() + paddingTop + paddingBottom;
		metrics.width = $("#"+id).outerWidth() + paddingLeft + paddingRight;
		
		var topBar = metrics.windowScrollTop;
		if(g(topBarId)){
			topBar += $("#"+topBarId).outerHeight();
		}
		
		metrics.top = $el.offset().top - paddingTop;
		//Compensate the topBar
		if(topBar > metrics.top){
			metrics.top = topBar;
			metrics.height = Math.max(metrics.height - (topBar - $el.offset().top + paddingTop), 0);
		}
		metrics.bottom = metrics.top + metrics.height;
		
		metrics.left = $el.offset().left - paddingLeft;
		metrics.right = metrics.left + metrics.width;
	}
	
	return metrics;
}

function recalculateOutlineBlocks(metrics){
	if(Object.keys(metrics).length){
		var container = addOutlineBlock({
			top: 0, 
			left: 0,
			height: $("body").height(),
			width: $("body").width(),
			id: outlineContainerId,
			className: outlineContainerClassName,
			outlineBlock: g(outlineContainerId)
		});
		
		if(container.topOutline && container.topOutline.parentNode){container.topOutline.parentNode.removeChild(container.topOutline); container.topOutline = null;}
		if(container.bottomOutline && container.bottomOutline.parentNode){container.bottomOutline.parentNode.removeChild(container.bottomOutline); container.bottomOutline = null;}
		if(container.rightOutline && container.rightOutline.parentNode){container.rightOutline.parentNode.removeChild(container.rightOutline); container.rightOutline = null;}
		
		container.topOutline = addOutlineBlock({
			top: 0,
			left: 0,
			height: metrics.top, 
			width: $(document).width(),
			className: outlineBlockClassName,
			insertInElement: container,
			outlineBlock: container.topOutline
		});
		container.bottomOutline = addOutlineBlock({
			top: metrics.bottom,
			left: 0,
			height: $("body").height() - metrics.bottom,
			width: $(document).width(),
			className: outlineBlockClassName,
			insertInElement: container,
			outlineBlock: container.bottomOutline
		});
		container.leftOutline = addOutlineBlock({
			top: metrics.top,
			left: 0,
			height: metrics.height, 
			width: metrics.left,
			className: outlineBlockClassName,
			insertInElement: container,
			outlineBlock: container.leftOutline
		});
		container.rightOutline = addOutlineBlock({
			top: metrics.top,
			left: metrics.right,
			height: metrics.height,
			width: $(document).width() - metrics.right,
			className: outlineBlockClassName,
			insertInElement: container,
			outlineBlock: container.rightOutline
		});
		/*container.centerOutline = addOutlineBlock({
			top: metrics.top, 
			left: metrics.left,
			height: metrics.height,
			width: metrics.width,
			className: outlineTransparentBlockClassName,
			insertInElement: container,
			outlineBlock: container.centerOutline
		});*/
	}
}

function addOutlineBlock(params){
	if(isUndefined(params.outlineBlock)){ //Block is not yet created
		var overlay = document.createElement('div');
		overlay.className = params.className;
		if(!isUndefined(params.id)){
			overlay.id = params.id;
		}
		overlay.style.height = params.height + "px";
		overlay.style.width = params.width + "px";
		overlay.style.top = params.top + "px";
		overlay.style.left = params.left + "px";
		overlay.style.position = "absolute";
		if(isUndefined(params.insertInElement)){
			document.body.appendChild(overlay);
		}else{
			params.insertInElement.appendChild(overlay);
		}
	}else{ //Block is already created, just recalculate
		var overlay = params.outlineBlock;
		overlay.style.height = params.height + "px";
		overlay.style.width = params.width + "px";
		overlay.style.top = params.top + "px";
		overlay.style.left = params.left + "px";
	}
	return overlay;
}

function removeOutline(){
	clearInterval(g(outlineContainerId).recalculateInterval);
	$("#"+outlineContainerId).remove();
}
/** Outline element - end */

function getSetOfRandomNumbers(min, max, length) {
	var arr = []
	var counter = 0;
	if (min != max && max > min && length > 0) {
		while (arr.length < length && counter < 1000 * length) {//дай боже никои не ползва този фунция за мн рандом числа!!!
			var randomNumber = Math.ceil(Math.random() * (max - min) + min);
			var found = false;
			for(var i = 0; i < arr.length; i++){
				if (arr[i] == randomNumber) {
					found=true;
					break;
				}
			}
			if (!found) {
				arr.push(randomNumber);
			}
			
			counter++;
		}
	}
	
	return arr;
}

function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays*24*60*60*1000));
    var expires = "expires="+d.toUTCString();
    document.cookie = cname + "=" + cvalue + "; " + expires;
}

function optimoveReportGeneralEvent(eid, optimoveParams){
	if(typeof optimoveParams == "undefined"){
		optimoveParams = {};
	}
	optimoveParams.platform = "CO";
	if(settings.optimoveActive){
		OptiRealApi.reportEvent( eid, optimoveParams, settings.userId + "_U", optimoveTid);
	}
}
function optimoveReportTradeEvent(params){
	var result = 0;
	
	var pageName = "Trade page";
	var assetName = settings.marketsNames[params.box.marketId];
	
	var optimoveParams = {pageName: pageName, assetName: assetName};
	optimoveParams.platform = "CO";
	if(settings.optimoveActive){
		OptiRealApi.reportEvent( optimoveEvents.trade, optimoveParams, settings.userId + "_U", optimoveTid);
	}
	
	return result;
}

function multipleReplace(map, string) {
	$.each(map, function(k, v) {
		string = string.split(k).join(v);
	});
	return string;
}

function animateBalanceUpdate() {
	//animate balance update
	$('#popUp-container-info').addClass('show-balance');
	$('#info-popup-container').addClass('animatedOnce zoomOutUp');
}

function goToByScroll(id, adjust, container) {
	adjust = (!isUndefined(adjust)) ? adjust : 0;
	if (!container) {
		container = 'html,body';
	} else {
		adjust -= $('body').scrollTop();
	}
	$(container).animate({scrollTop: $("#"+id).offset().top + adjust}, 'slow');
}

function threeDSecure(data) {
	if (data.threeDParams) {
		if (!isUndefined(data.threeDParams.acsUrl)) {
			$( "body" ).append($('<form id="threeDSecureForm" method="post" action="' + data.threeDParams.acsUrl + '"><input name="PaReq" value="' + encodeURI(data.threeDParams.paReq) + '"/><input name="TermUrl" value="' + encodeURI(data.threeDParams.termUrl) + '"/><input name="MD" value="' + encodeURI(data.threeDParams.MD) + '"/></form>'))
			$('#threeDSecureForm').submit();
		} else {
			var bgr = $('<div class="fibonatix-bgr" id="fibonatix-bgr"/>').append($("<iframe class='fibonatix-iframe' id='fibonatix-iframe' />").attr({"srcdoc":data.threeDParams.form}));
			$("#body").append(bgr);
		}
		return true;
	}
	return false;
}