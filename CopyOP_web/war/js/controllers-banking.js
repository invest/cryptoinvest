copyOpApp.controller('UserInvestmentsCtrl', ['$rootScope', '$scope', '$http', function($rootScope, $scope, $http) {
	$scope.ready = false;
	$scope.params = {isSettled: $rootScope.$state.current.isSettiled};
	$scope.currentPage = 0;
	$scope.lastPage = -1;
	$scope.history = [];
	$scope.tooltipTimer = null;
	$scope.rowsOnPage = settings.resultsOnPage;
	
	$scope.displayShowoff = true;
	$scope.displayExpiryColumn = true;
	
	$scope.sortAssetsParams = {
		openFirst: true,
		openSortField: 'timeCreated',
		openSortFieldType: 'date',
		openSortOrder: 'desc',
		settledSortField: 'timeCreated',
		settledSortFieldType: 'date',
		settledSortOrder: 'desc'
	}
	
	function waitForIt() {
		if (!$scope.$parent.ready) {
			setTimeout(function(){waitForIt()},50);
		} else {
			if (settings.loggedIn) {//if not logged in don't make request
				init();
			}
		}
	}
	
	function init() {
		if ($rootScope.$state.current.isSettiled) {
			$scope.closingColumTxt = $rootScope.getMsgs('closing');
		} else {
			$scope.closingColumTxt = $rootScope.getMsgs('current');
		}
		$scope.tblSettings = {
			tableClass: 'settle-table',
			buttons: false,
			totalRow: false
		}
		$scope.getPage(0);
	}
	
	$scope.getPage = function(page) {
		$scope.ready = false;
		if (page == 'prev') {
			page = --$scope.currentPage;
			if (page < 0) {page = 0;}
		} else if (page == 'next') {
			page = ++$scope.currentPage;
		}
		$scope.currentPage = page;
		if (isUndefined($scope.history[page])) {
			$http.post(settings.jsonLink + 'getUserInvestments', jsonClone(getUserMethodRequest(), {
					isSettled: $rootScope.$state.current.isSettiled,
					startRow: (page * settings.resultsOnPage),
					pageSize: (settings.resultsOnPage + 1)
				}))
				.success(function(data, status, headers, config) {
					if (!handleErrors(data, 'getUserInvestments')) {
						$scope.ready = true;
						data.page = page;
						if (data.investments.length == 0) {
							$scope.lastPage = page;
							$scope.investments = [];
						} else if (data.investments.length < (settings.resultsOnPage + 1)) {
							$scope.lastPage = page;
							$scope.setInfo(data);
						} else {
							$scope.setInfo(data);
						}
					}
				})
				.error(function(data, status, headers, config) {
					handleNetworkError(data);
				});
		} else {
			$scope.investments = $scope.history[page];
		}
	}
	
	$scope.setInfo = function(data) {
		$scope.setUser(data);
		$scope.investments = data.investments;
		for (var n = 0; n < $scope.investments.length; n++) {
			$scope.investments[n].index = n;
			$scope.investments[n].timeSettledDsp = $scope.formatDateNow(adjustTimeToBrowser($scope.investments[n].timeSettled));
			// $scope.investments[n].expiryLevel = $scope.investments[n].expiryLevel.replace(/,/g,'');
			if ($scope.investments[n].isCanceled) {
				$scope.investments[n].expiryLevel = $rootScope.getMsgs('canceled');
			} else if ($scope.investments[n].typeId == 7 && parseFloat($scope.investments[n].expiryLevel) == 0) {
				$scope.investments[n].expiryLevel = 'Dynamics';
			} else {
				$scope.investments[n].expiryLevel = ($scope.investments[n].isSettled == 0) ? $rootScope.getMsgs('open-options') : $scope.investments[n].expiryLevel;
			}
			
			if (($scope.investments[n].typeId == 1 && $scope.investments[n].currentLevel < $scope.investments[n].expiryLevel.replace(/,/g,'').replace(/ /g,'')) ||
				($scope.investments[n].typeId == 2 && $scope.investments[n].currentLevel > $scope.investments[n].expiryLevel.replace(/,/g,'').replace(/ /g,''))) {
				$scope.investments[n].win = true;
				$scope.investments[n].winAmount = formatAmount($scope.investments[n].oddsWin*$scope.investments[n].amount, 0, true);
			} else {
				$scope.investments[n].win = false;
			}
			
			$scope.investments[n].timeCreated = adjustTimeToBrowser(data.investments[n].timeCreated);
			$scope.investments[n].timeEstClosing = adjustFromUTCToLocal(new Date($scope.investments[n].timeEstClosing)).getTime();
		}
		$scope.history[data.page] = $scope.investments;
	}
	
	$scope.showCopiedFrom = function(index) {
		if ($scope.investments[index].copyopType == 'COPY') {
			clearTimeout($scope.tooltipTimer);
			for (var i = 0; i < $scope.investments.length; i++) {
				if (i != index) {
					$scope.investments[i].show = false;
				}
			}
			if (!$scope.investments[index].show) {
				$scope.investments[index].show = true;
				if (isUndefined($scope.investments[index].orgProfile)) {
					$http.post(settings.jsonLink + 'getCopyopOriginalInvProfile', jsonClone(getUserMethodRequest(), {
							copiedInvestmentId: $scope.investments[index].id
						}))
						.success(function(data, status, headers, config) {
							/* if (!handleErrors(data, 'getCopyopOriginalInvProfile')) { */
								$scope.investments[index].orgProfile = data;
							/* } */
						})
						.error(function(data, status, headers, config) {
							handleNetworkError(data);
						});
				}
			}
		}
	}
	
	$scope.hideCopiedFrom = function(index) {
		// $scope.investments[index].show = false;
		// $scope.tooltipTimer = setTimeout(function($scope) {
		// }, 4000);
	}
	waitForIt();
}]);

copyOpApp.controller('copyopProfileCoinsCtrl', ['$rootScope', '$scope', '$http', function($rootScope, $scope, $http) {
	$scope.ready = false;
	function waitForIt() {
		if (!$scope.$parent.ready) {
			setTimeout(function(){waitForIt()},50);
		} else {
			if (settings.loggedIn) {//if not logged in don't make request
				init();
			}
		}
	}
	waitForIt();
	
	function init() {
		$http.post(settings.jsonLink + 'getCopyopProfileCoins', getUserMethodRequest())
			.success(function(data, status, headers, config) {
				if (!handleErrors(data, 'getCopyopProfileCoins')) {
					$scope.ready = true;
					$scope.setCoins(data);
				}
			})
			.error(function(data, status, headers, config) {
				handleNetworkError(data);
			});
	}
}]);

copyOpApp.controller('insertCardCtrl', ['$rootScope', '$scope', '$http', '$timeout', function($rootScope, $scope, $http, $timeout) {
	$scope.isPopUp = false;
	$scope.passParams = function(params){
		$scope.isPopUp = params.isPopUp;
	}
	
	$scope.ready = false;
	$scope.form = {};
	$scope.additionalFields = false;
	$scope.shortYear = settings.shortYear;
	$scope.shortYearFuture = settings.shortYearFuture;
	$scope.yearMinus18 = new Date().getFullYear() - 18;
	
	function waitForIt() {
		if (!$scope.$parent.ready) {
			setTimeout(function(){waitForIt()},50);
		} else {
			if (settings.loggedIn) {//if not logged in don't make request
				init();
			}
		}
	}
	waitForIt();
	
	function init() {
		$scope.getMonths();
		$rootScope.getSkinCurrencies($rootScope.copyopUser.user.currencyId);
		$scope.form.amount = $rootScope.copyopUser.user.predefinedDepositAmount;
		$scope.ready = true;
		var FirstEverDepositCheckRequest = jsonClone(getMethodRequest(), {userId: $rootScope.copyopUser.userId});
		$http.post(settings.jsonLink + 'getFirstEverDepositCheck', FirstEverDepositCheckRequest)
			.success(function(data, status, headers, config) {
				if (!handleErrors(data, 'getFirstEverDepositCheck')) {
					$scope.additionalFields = data.firstEverDeposit;
					// if ($rootScope.$state.current.name != 'first-deposit' && data.firstEverDeposit) {
						// $rootScope.$state.go('ln.first-deposit', {ln: $scope.skinLanguage});
					// }
				}
			})
			.error(function(data, status, headers, config) {
				handleNetworkError(data);
			});
	}
	
	$scope.updateDefaultDepositAmount = function(_form) {
		if (!_form.amount.$touched) {
			$scope.form.amount = $rootScope.predefinedDepositAmountMap[$rootScope.copyopUser.user.currency.id];
		}
	}
	
	$scope.submitNewCard = function(_form) {
		if (_form.$valid) {
			if ($scope.additionalFields) {
				if (!$scope.loading) {
					$scope.loading = true;
					
					var UpdateUserMethodRequest = jsonClone(getUserMethodRequest(), {
						user: {
							id: $rootScope.copyopUser.user.id,
							currencyId: $rootScope.copyopUser.user.currency.id,
							street: $scope.form.street,
							cityName: $scope.form.cityName,
							zipCode: $scope.form.zipCode,
							birthDayUpdate: $scope.form.birthDayUpdate,
							birthMonthUpdate: $scope.form.birthMonthUpdate,
							birthYearUpdate: $scope.form.birthYearUpdate
						}
					});
					$http.post(settings.jsonLink + 'updateUserAdditionalFields', UpdateUserMethodRequest)
						.success(function(data, status, headers, config) {
							$scope.loading = false;
							if ($scope.isPopUp) {
								data.globalErrorField = "globalErrorFieldPopup";
							}
							if (!handleErrors(data, 'updateUserAdditionalFields')) {
								settings.defaultCurrencyId = $rootScope.copyopUser.user.currency.id;
								settings.currencySymbol = $rootScope.copyopUser.user.currency.symbol;
								settings.currencyLeftSymbol = $rootScope.copyopUser.user.currency.isLeftSymbol;
								settings.decimalPointDigits = $rootScope.copyopUser.user.currency.decimalPointDigits;
								$scope.additionalFields = false;
								// $scope.setUser(data);
								$scope.insertCard(_form);
								// $scope.getCopyopUser({onStateChange: true});
							}
						})
						.error(function(data, status, headers, config) {
							$scope.loading = false;
							handleNetworkError(data);
						});
				}
			} else {
				$scope.insertCard(_form);
			}
		} else {
			logIt({'type':3,'msg':_form.$error});
		}
	}
	
	$scope.insertCard = function(_form) {
		if (!$scope.loading) {
			$scope.loading = true;
			
			var ccType = detectCCtype($scope.form.ccNumber, true);
			var CardMethodRequest = jsonClone(getUserMethodRequest(), {
				card: {
					// creditAmount: $scope.form.amount,
					typeId: (isNaN(ccType) ? 0 : ccType),
					ccNumber: $scope.form.ccNumber,
					ccPass: $scope.form.ccPass,
					expMonth: $scope.form.expMonth.id,
					expYear: $scope.form.expYear,
					holderName: $scope.form.holderName,
					holderId: (settings.skinId != 1) ? '@"No-Id"' : $scope.form.holderId
				}
			});
			
			$http.post(settings.jsonLink + 'insertCard', CardMethodRequest)
				.success(function(data, status, headers, config) {
					$scope.loading = false;
					if ($scope.isPopUp) {
						data.globalErrorField = "globalErrorFieldPopup";
					}
					if (!handleErrors(data, 'insertCard')) {
						$scope.insertDepositCard(data,_form);
					}
				})
				.error(function(data, status, headers, config) {
					$scope.loading = false;
					handleNetworkError(data);
				});
		}
	}
	
	$scope.insertDepositCard = function(dataResp,_form) {
		if (!$scope.loading) {
			$scope.loading = true;
			
			if (dataResp.error != null) {
				var CcDepositMethodRequest = jsonClone(getUserMethodRequest(), {
					amount: $scope.form.amount,
					cardId: dataResp.card.id,
					error: dataResp.error
				});	
			} else { // firstNewCardChanged
				var CcDepositMethodRequest = jsonClone(getUserMethodRequest(), {
					amount: $scope.form.amount,
					cardId: dataResp.card.id,
					ccPass: $scope.form.ccPass
				});	
			}
			
			$http.post(settings.jsonLink + 'insertDepositCard', CcDepositMethodRequest)
				.success(function(data, status, headers, config) {
					if (threeDSecure(data)) {
						//that will way more cool when it is a component :)
						function checkIframe() {
							var iframeUrl = '';
							try{
								iframeUrl = $('#fibonatix-iframe').contents().get(0).location.href;
							} catch(e){}
							
							if (iframeUrl.toLowerCase().indexOf('copyop.com') >= 0 || iframeUrl.toLowerCase().indexOf('threeDSecureResult') >= 0) {
								//some very ugly stuff copied from other ugly code :)
								var request = getUserMethodRequest();
								
								$http.post(settings.jsonLink + 'finish3dTransaction', request)
									.success(function(data, status, headers, config) {
										$('#fibonatix-bgr').remove();
										if (data.errorCode) {
											$rootScope.openCopyOpPopup({
												type: 'info', 
												body: 'error-' + errorCodeMap.transaction_failed, 
											});
										} else {
											$rootScope.copyopUser.user.balance = data.balance;
											$scope.balance = formatAmount(data.balance, 0);
											$rootScope.copyopUser.user.balanceWF = $scope.balance;
											
											var onclose = null;
											var isFirstDeposit = 0;
											if (data.isFirstDeposit) {
												isFirstDeposit = 1;
												onclose = function(){$rootScope.openCopyOpPopup({type: 'autoWatch', destUserId: $rootScope.copyopUser.user.id})};
												$scope.getPixel({pixelType: 1, transactionId: data.transactionId});
												$scope.openQuestionary();
												$rootScope.copyopUser.user.firstDepositId = data.transactionId;
											} else {
												$scope.addGTMPixel({pixelType: 'repeatDeposit', transaction: data});
												$rootScope.$state.go('ln.in.a.asset');
											}
											
											if (settings.isLive) {
												//google tracking
												dataLayer.push({
													'is_deposit': '1',
													'is_first': isFirstDeposit,
													'deposit_sum': data.dollarAmount,
													'deposit_currency': $rootScope.copyopUser.user.currency.code
												});
											}
											
											$rootScope.openCopyOpPopup({
												type: 'info', 
												header: 'deposit-success-header', 
												body: 'deposit-success-body', 
												onclose: onclose,
												bodyParams: {
													amount: formatAmount(data.amount, 2),
													transactionId: data.transactionId,
												}
											});
										}
									})
									.error(function(data, status, headers, config) {
										$scope.loading = false;
										handleNetworkError(data);
									});
							} else {
								setTimeout(function() {
									checkIframe();
								},500);
							}
						}
						
						if (isUndefined(data.threeDParams.acsUrl)) {
							checkIframe();
						}
						return;
					}

					$scope.loading = false;
					var device = detectDevice();
					if ($scope.isPopUp || device != '') {
						data.globalErrorField = "globalErrorFieldPopup";
					}
					data.globalErrorField = 'globalErrorFieldPopup';
					if (!handleErrors(data, 'insertDepositCard')) {
						_form.$setPristine();
						$rootScope.closeCopyOpPopup();
						$rootScope.copyopUser.user.balance = data.balance;
						$scope.balance = formatAmount(data.balance, 0);
						$rootScope.copyopUser.user.balanceWF = $scope.balance;
						var onclose = null;
						var isFirstDeposit = 0;
						if (data.isFirstDeposit) {
							isFirstDeposit = 1;
							onclose = function() {
								$rootScope.openCopyOpPopup({
									type: 'autoWatch',
									destUserId: $rootScope.copyopUser.user.id
								})
							};
							$scope.getPixel({pixelType: 1, transactionId: data.transactionId});
							$scope.addGTMPixel({pixelType: 'firstDeposit', transaction: data});
							if (device == '') {
								$scope.openQuestionary();
							}
							$rootScope.copyopUser.user.firstDepositId = data.transactionId;
						} else {
							$scope.addGTMPixel({pixelType: 'repeatDeposit', transaction: data});
							$rootScope.$state.go('ln.in.a.asset');
						}
						optimoveReportGeneralEvent(optimoveEvents.deposit, {});
						if (settings.isLive) {
							//google tracking
							dataLayer.push({
								'is_deposit': '1',
								'is_first': isFirstDeposit,
								'deposit_sum': $scope.form.amount,
								'deposit_currency': $rootScope.copyopUser.user.currency.code
							});
							$scope.fb_FTD_pixel('Deposit', 0, {
								em: $rootScope.copyopUser.user.email, 
								fb_order_id: data.transactionId
							});
						}
	
						$scope.bodyParams = {
							amount: '<b>' + formatAmount($scope.form.amount, 2) + '</b>',
							transactionId: '<b>' + data.transactionId + '</b>',
							provider: '<b>' + data.clearingProvider + '</b>',
							
						}
						if (device == '') {
							$rootScope.openCopyOpPopup({
								type: 'info-img', 
								// header: 'deposit-success-header', 
								header: $rootScope.getMsgs('successful-deposit'),
								text: 'deposit-success-body' + ((data.clearingProvider) ? '-provider' : ''), 
								imgClass: 'success-deposit-popUp',
								onOk: onclose,
								animate: 1,
								animateFuncton: function() {
									animateBalanceUpdate()
								},
								bodyParams: $scope.bodyParams
							});
						} else {	
							$scope.setSuccess($rootScope.getMsgs('deposit-success-body' + ((data.clearingProvider) ? '-provider' : ''), $scope.bodyParams));
						}
						
						$scope.form = {};
						$rootScope.afterDeposit = true;
						var checkStateMachine = !$scope.isQuestionairePEP();
						if (checkStateMachine) {
							checkStateMachine = !$scope.isTresholdBlock();
						}
						if (device == '') {
							$scope.getCopyopUser({onStateChange: true, stateMachine: checkStateMachine});
						}
						$rootScope.copyopUser.user.firstDepositId = data.transactionId;
						
						for(var i = 0; i < $scope.popUps.length; i++){
							if($scope.popUps[i].config.type == "deposit"){
								closeCopyOpPopup({'popupId' : i});
								break;
							}
						}
					} else if (data.errorCode == 207 && data.userMessages && (data.userMessages[0].field == null || data.userMessages[0].field == '')) {
						$('#globalErrorFieldPopup').html('');
						$rootScope.openCopyOpPopup({
							type: 'info-img', 
							header: $rootScope.getMsgs('unsuccessful-deposit'),
							buttonTxt: $rootScope.getMsgs('CMS.optionplus.popup.text.try.again'),
							imgClass: 'failed-deposit-popUp',
							bodyTxt: data.userMessages[0].message
						});
					}
				})
				.error(function(data, status, headers, config) {
					$scope.loading = false;
					handleNetworkError(data);
				});
		}
	}
}]);

copyOpApp.controller('DepositCtrl', ['$rootScope', '$scope', '$http', function($rootScope, $scope, $http) {
	$scope.ready = false;
	function waitForIt() {
		if (!$scope.$parent.ready) {
			setTimeout(function(){waitForIt()},50);
		} else {
			if (settings.loggedIn) {//if not logged in don't make request
				init();
			}
		}
	}
	waitForIt();
	
	function init() {
		$http.post(settings.jsonLink + 'getAvailableCreditCardsWithEmptyLine', getUserMethodRequest())
			.success(function(data, status, headers, config) {
				if (!handleErrors(data, 'getAvailableCreditCardsWithEmptyLine')) {
					$scope.cards = data.options;
					if ($scope.cards != null) {
						if ($scope.cards.length == 1) {
							$rootScope.$state.go('ln.in.a.personal.my-account.banking.deposit-new-card', {ln: $scope.skinLanguage});
						} else {
							var selected = 1;
							for (var i = 0; i < $scope.cards.length; i++) {
								$scope.cards[i].ccNumberDsp = cardFilter($scope.cards[i].ccNumber);
								if ($scope.cards[i].selected) {
									selected = i;
								}
							}
							$scope.depositNum = $scope.cards[selected];
						}
					}
					$scope.ready = true;
				}
			})
			.error(function(data, status, headers, config) {
				handleNetworkError(data);
			});
	}

	$scope.insertDepositCard = function(_form) {
		if (_form.$valid) {
			if (!$scope.loading) {
				$scope.loading = true;
				
				var CcDepositMethodRequest = jsonClone(getUserMethodRequest(), {
					cardId: $scope.depositNum.id,
					amount: $scope.depositAmount,
					ccPass: $scope.ccPass
				});
				
				$http.post(settings.jsonLink + 'insertDepositCard', CcDepositMethodRequest)
					.success(function(data, status, headers, config) {
						$scope.loading = false;
						if (!handleErrors(data, 'insertDepositCard')) {
							if (threeDSecure(data)) {
								//that will be way more cool when it is a component :)
								function checkIframe() {
									var iframeUrl = '';
									try{
										iframeUrl = $('#fibonatix-iframe').contents().get(0).location.href;
									} catch(e){}
									
									if (iframeUrl.toLowerCase().indexOf('copyop.com') >= 0 || iframeUrl.toLowerCase().indexOf('threeDSecureResult') >= 0) {
										//some very ugly stuff copied from other ugly code :)
										var request = getUserMethodRequest();
										
										$http.post(settings.jsonLink + 'finish3dTransaction', request)
											.success(function(data, status, headers, config) {
												$('#fibonatix-bgr').remove();
												if (data.errorCode) {
													$rootScope.openCopyOpPopup({
														type: 'info', 
														body: 'error-' + errorCodeMap.transaction_failed, 
													});
												} else {
													$rootScope.copyopUser.user.balance = data.balance;
													$scope.balance = formatAmount(data.balance, 0);
													$rootScope.copyopUser.user.balanceWF = $scope.balance;
													
													var onclose = null;
													var isFirstDeposit = 0;
													if (data.isFirstDeposit) {
														isFirstDeposit = 1;
														onclose = function(){$rootScope.openCopyOpPopup({type: 'autoWatch', destUserId: $rootScope.copyopUser.user.id})};
														$scope.getPixel({pixelType: 1, transactionId: data.transactionId});
														$scope.openQuestionary();
														$rootScope.copyopUser.user.firstDepositId = data.transactionId;
													} else {
														$scope.addGTMPixel({pixelType: 'repeatDeposit', transaction: data});
														$rootScope.$state.go('ln.in.a.asset');
													}
													
													if (settings.isLive) {
														//google tracking
														dataLayer.push({
															'is_deposit': '1',
															'is_first': isFirstDeposit,
															'deposit_sum': data.dollarAmount,
															'deposit_currency': $rootScope.copyopUser.user.currency.code
														});
													}
													
													$rootScope.openCopyOpPopup({
														type: 'info', 
														header: 'deposit-success-header', 
														body: 'deposit-success-body', 
														onclose: onclose,
														bodyParams: {
															amount: formatAmount(data.amount, 2),
															transactionId: data.transactionId,
														}
													});
												}
											})
											.error(function(data, status, headers, config) {
												$scope.loading = false;
												handleNetworkError(data);
											});
									} else {
										setTimeout(function() {
											checkIframe();
										},500);
									}
								}
								
								if (isUndefined(data.threeDParams.acsUrl)) {
									checkIframe();
								}
								return;
							}
							
							var onclose = null;
							var isFirstDeposit = 0;
							if (data.isFirstDeposit) {
								onclose = function() {$rootScope.openCopyOpPopup({type: 'autoWatch', destUserId: $rootScope.copyopUser.user.id})};
								$scope.addGTMPixel({pixelType: 'firstDeposit', transaction: data});
								$scope.openQuestionary();
								$rootScope.copyopUser.user.firstDepositId = data.transactionId;
								isFirstDeposit = 1;
							} else {
								onclose = function() {
									$rootScope.afterDeposit = true;
									var checkStateMachine = !$scope.isQuestionairePEP();
									if (checkStateMachine) {
										checkStateMachine = !$scope.isTresholdBlock();
									}
									$scope.getCopyopUser({onStateChange: true, stateMachine: checkStateMachine});
								};
								$scope.addGTMPixel({pixelType: 'repeatDeposit', transaction: data});
								angular.element(g('UserTransactionsCtrlHolder')).scope().getUserTransactions();
							}
							optimoveReportGeneralEvent(optimoveEvents.deposit, {});
							
							$rootScope.openCopyOpPopup({
								type: 'info', 
								header: 'deposit-success-header', 
								body: 'deposit-success-body' + ((data.clearingProvider) ? '-provider' : ''), 
								onclose: onclose,
								animate: 1,
								animateFuncton: function() {
									animateBalanceUpdate()
								},
								bodyParams: {
									amount: '<b>' + formatAmount($scope.depositAmount, 2) + '</b>',
									transactionId: '<b>' + data.transactionId + '</b>',
									provider: '<b>' + data.clearingProvider + '</b>',
								}
							});
							
							$rootScope.copyopUser.user.balance = data.balance;
							$scope.balance = formatAmount(data.balance, 0);
							$rootScope.copyopUser.user.balanceWF = $scope.balance;
							
							if (settings.isLive) {
								//google tracking
								dataLayer.push({
									'is_deposit': '1',
									'is_first': isFirstDeposit,
									'deposit_sum': $scope.depositAmount,
									'deposit_currency': $rootScope.copyopUser.user.currency.code
								});
							}
						} else if (data.errorCode == 207 && data.userMessages) {
							$('#globalErrorFieldPopup').html('');
							$rootScope.openCopyOpPopup({
								type: 'info-img', 
								header: $rootScope.getMsgs('unsuccessful-deposit'),
								buttonTxt: $rootScope.getMsgs('CMS.optionplus.popup.text.try.again'),
								imgClass: 'failed-deposit-popUp',
								bodyTxt: data.userMessages[0].message
							});
						}
					})
					.error(function(data, status, headers, config) {
						$scope.loading = false;
						handleNetworkError(data);
					});
			}
		} else {
			logIt({'type':3,'msg':_form.$error});
		}
	}
}]);

copyOpApp.controller('WithdrawCtrl', ['$rootScope', '$scope', '$http', function($rootScope, $scope, $http) {
	$scope.ready = false;
	$scope.isWithdrawalWeekendsAvailable = true;
	$scope.hasCC = false;
	$scope.hasWire = false;
	function waitForIt() {
		if (!$scope.$parent.ready) {
			setTimeout(function(){waitForIt()},50);
		} else {
			if (settings.loggedIn) {//if not logged in don't make request
				init();
			}
		}
	}
	
	function init() {
		$http.post(settings.jsonLink + 'getWithdrawCCList', getUserMethodRequest())
			.success(function(data, status, headers, config) {
				if (!handleErrors(data, 'getWithdrawCCList')) {
					$scope.cards = [];
					if (data.options != null) {
						for (var i = 0; i < data.options.length; i++) {
							if (!isUndefined(data.options[i])) {
								data.options[i].ccNumberDsp = cardFilter(data.options[i].ccNumber);
								$scope.cards.push(data.options[i]);
							}
						}
						$scope.hasCC = true;
						$scope.withdrawMethod = 'creditCard';
					} else {
						$scope.hasWire = true;
						$scope.withdrawMethod = 'bankWire';
					}
					$scope.ready = true;
				}
			})
			.error(function(data, status, headers, config) {
				handleNetworkError(data);
			});
			
		$http.post(settings.jsonLink + 'isWithdrawalWeekendsAvailable', getUserMethodRequest())
			.success(function(data, status, headers, config) {
				if (!handleErrors(data, 'isWithdrawalWeekendsAvailable')) {
					if (data.errorCode == errorCodeMap.withdrawal_close) {
						$scope.isWithdrawalWeekendsAvailable = false;
					}
				}
			})
			.error(function(data, status, headers, config) {
				handleNetworkError(data);
			});
			
		$scope.getPermissionsToDisplay();
	}
	
	$scope.getPermissionsToDisplay = function() {
		var GmWithdrawalPermissionToDisplayRequest = {
			userId: $rootScope.copyopUser.userId
		};
		
		$http.post(settings.commonServiceLink + 'GmWithdrawalServices/getPermissionsToDisplay', GmWithdrawalPermissionToDisplayRequest)
			.then(function(data) {
				data = data.data;
				if (!handleErrors(data, 'getPermissionsToDisplay')) {
					$scope.displayDirect24 = data.displayDirect24;
					$scope.displayEPS = data.displayEPS;
					$scope.displayGiropay = data.displayGiropay;
				}
			})
			.catch(function(data) {
				handleNetworkError(data);
			})
	}
	
	$scope.setMethod = function(method) {
		$scope.withdrawMethod = method;
	}

	waitForIt();
}]);

copyOpApp.controller('WithdrawCcCtrl', ['$rootScope', '$scope', '$http', function($rootScope, $scope, $http) {
	$scope.ready = false;
	function waitForIt() {
		if (!$scope.$parent.ready) {
			setTimeout(function(){waitForIt()},50);
		} else {
			if (settings.loggedIn) {//if not logged in don't make request
				init();
			}
		}
	}
	waitForIt();
	
	function init() {
		
	}
	
	$scope.showWithdrawSurvey = function(_form){
		if (_form.$valid) {
			$rootScope.openCopyOpPopup({
				type: 'withdrawSurvey',
				form: _form,
				scope: $scope
			});
		} else {
			logIt({'type':3,'msg':_form.$error});
		}
	}
	
	$scope.submit = function(_form) {
		if (_form.$valid) {
			$('#globalErrorField_withrow').html('');
			if (!$scope.loading) {
				$scope.loading = true;
				
				var WithdrawBonusRegulationStateMethodRequest = jsonClone(getUserMethodRequest(), {
					transactionTypeId: 10,//only cc for now
					ccId: $scope.withdrawNum.id
				}); 

				$http.post(settings.jsonLink + 'getWithdrawBonusRegulationState', WithdrawBonusRegulationStateMethodRequest)
					.success(function(data, status, headers, config) {
						data.globalErrorField = "globalErrorField_withrow";
						if (!handleErrors(data, 'getWithdrawBonusRegulationState')) {
							if (data.state == "NO_OPEN_BONUSES") {
								$scope.loading = false;
								$scope.submitWithdrow(_form);
							} else if (data.state == "REGULATED_OPEN_BONUSES") {
								$rootScope.openCopyOpPopup({
									type: 'confirm',
									onOk: function() {$scope.submitWithdrow(_form)},
									onclose: function() {$scope.insertWithdrawCancel(10)},
									bodyTxt: $rootScope.getMsgs('CMS.withdrawResetBonus_PopUp.text',{'user-name': $rootScope.copyopUser.user.name})
								});
								$scope.loading = false;
							} else if (data.state == "NOT_REGULATED_OPEN_BONUSES") {
								$rootScope.openCopyOpPopup({
									type: 'info', 
									bodyTxt: $rootScope.getMsgs('error.withdrow-not-reg')
								});
								$scope.loading = false;
							}
						}
					})
					.error(function(data, status, headers, config) {
						$scope.loading = false;
						handleNetworkError(data);
					});
			}
		} else {
			logIt({'type':3,'msg':_form.$error});
		}
	}
	
	var fee = 0;
	$scope.submitWithdrow = function(_form) {
		if (_form.$valid) {
			if (!$scope.loading) {
				$scope.loading = true;
				
				$scope.CcDepositMethodRequest = jsonClone(getUserMethodRequest(), {
					cardId: $scope.withdrawNum.id,
					amount: $scope.withdrawAmount,
					userAnswerId: $scope.userAnswerId,
					textAnswer: $scope.textAnswer
				});
				
				$http.post(settings.jsonLink + 'validateWithdrawCard', $scope.CcDepositMethodRequest)
					.success(function(data, status, headers, config) {
						$scope.loading = false;
						data.globalErrorField = "globalErrorField_withrow";
						if (!handleErrors(data, 'validateWithdrawCard')) {
							fee = data.fee;
							$rootScope.openCopyOpPopup({
								type: 'withdraw', 
								lowAmountWithdraw: data.lowAmountWithdraw, 
								onOk: function(){$scope.insertWithdrawCard()},
								//body: (data.lowAmountWithdraw == 0) ? 'withdraw.cc.approve.message' : 'withdraw.cc.approve.message.low.amount',
								body: 'withdraw.cc.approve.message.low.amount',
								bodyParams: {
									fee: data.fee,
									amount: formatAmount(data.lowAmountWithdraw, 0, true)
								}
							});
						}
					})
					.error(function(data, status, headers, config) {
						$scope.loading = false;
						handleNetworkError(data);
					});
			}
		} else {
			logIt({'type':3,'msg':_form.$error});
		}
	}

	$scope.insertWithdrawCancel = function() {
		var InsertWithdrawCancelMethodRequest = jsonClone(getUserMethodRequest(), {
			transactionTypeId: 10,
			ccId: $scope.withdrawNum.id
		});
		
		$http.post(settings.jsonLink + 'insertWithdrawCancel', InsertWithdrawCancelMethodRequest)
			.success(function(data, status, headers, config) {
				if (!handleErrors(data, 'insertWithdrawCancel')) {
					$rootScope.closeCopyOpPopup();
				}
			})
			.error(function(data, status, headers, config) {
				$scope.loading = false;
				handleNetworkError(data);
			});
	}
	
	$scope.insertWithdrawCard = function() {
		if (!$scope.loading) {
			$scope.loading = true;
			
			$http.post(settings.jsonLink + 'insertWithdrawCard', $scope.CcDepositMethodRequest)
				.success(function(data, status, headers, config) {
					$scope.loading = false;
					data.globalErrorField = "globalErrorField_withrow";
					if (!handleErrors(data, 'insertWithdrawCard')) {
						$rootScope.copyopUser.user.balance = data.balance;
						$rootScope.copyopUser.user.balanceWF = formatAmount(data.balance, 0);
						$rootScope.openCopyOpPopup({
							scope: $scope,
							type: 'withdrawSuccess', 
							lowAmountWithdraw: data.lowAmountWithdraw, 
							//body: (data.lowAmountWithdraw == 0) ? 'withdraw.cc.success' : 'withdraw.cc.success.low.amount',
							body: 'withdraw.cc.success.low.amount',
							onclose: function(){$scope.getCopyopUser({onStateChange: true, stateMachine: false});},
							bodyParams: {
								heading: 'withdraw_popUp_heading',
								amount: data.formattedAmount,
								fee: fee
							}
						});
						angular.element(g('UserTransactionsCtrlHolder')).scope().getUserTransactions();
						optimoveReportGeneralEvent(optimoveEvents.withdraw, {});
					}
				})
				.error(function(data, status, headers, config) {
					$scope.loading = false;
					handleNetworkError(data);
				});
		}
	}
}]);

copyOpApp.controller('WithdrawWireCtrl', ['$rootScope', '$scope', '$http', function($rootScope, $scope, $http) {
	$scope.ready = false;
	function waitForIt() {
		if (!$scope.$parent.ready) {
			setTimeout(function(){waitForIt()},50);
		} else {
			if (settings.loggedIn) {//if not logged in don't make request
			}
		}
	}
	
	$scope.showWithdrawSurvey = function(_form){
		if (_form.$valid) {
			$rootScope.openCopyOpPopup({
				type: 'withdrawSurvey',
				form: _form,
				scope: $scope
			});
		} else {
			logIt({'type':3,'msg':_form.$error});
		}
	}	
	
	$scope.submit = function(_form) {
		$('#globalErrorField_withrow').html('');
		if (!$scope.loading) {
			$scope.loading = true;
			
			var WithdrawBonusRegulationStateMethodRequest = jsonClone(getUserMethodRequest(), {
				transactionTypeId: 14,//only cc for now
			}); 

			$http.post(settings.jsonLink + 'getWithdrawBonusRegulationState', WithdrawBonusRegulationStateMethodRequest)
				.success(function(data, status, headers, config) {
					data.globalErrorField = "globalErrorField_withrow";
					if (!handleErrors(data, 'getWithdrawBonusRegulationState')) {
						if (data.state == "NO_OPEN_BONUSES") {
							$scope.loading = false;
							$scope.submitWithdrow(_form);
						} else if (data.state == "REGULATED_OPEN_BONUSES") {
							$rootScope.openCopyOpPopup({
								type: 'confirm',
								onOk: function() {$scope.submitWithdrow(_form)},
								onclose: function() {$scope.insertWithdrawCancel(10)},
								bodyTxt: $rootScope.getMsgs('CMS.withdrawResetBonus_PopUp.text',{'user-name': $rootScope.copyopUser.user.name})
							});
							$scope.loading = false;
						} else if (data.state == "NOT_REGULATED_OPEN_BONUSES") {
							$rootScope.openCopyOpPopup({
								type: 'info', 
								bodyTxt: $rootScope.getMsgs('error.withdrow-not-reg')
							});
							$scope.loading = false;
						}
					}
				})
				.error(function(data, status, headers, config) {
					$scope.loading = false;
					handleNetworkError(data);
				});
		}
	}
	
	var fee = 0;
	$scope.submitWithdrow = function(_form) {
		if (_form.$valid) {
			if (!$scope.loading) {
				$scope.loading = true;
				
				$scope.request = {};
				var url = '';
				if ($scope.withdrawMethod == 'bankWire') {
					$scope.request = jsonClone(getUserMethodRequest(), {
						wire: {
							beneficiaryName: $scope.beneficiaryName,
							swift: $scope.swift,
							iban: $scope.iban,
							accountNum: $scope.accountNum,
							bankNameTxt: $scope.bankNameTxt,
							branchName: $scope.branchName,
							branchAddress: $scope.branchAddress,
							amount: $scope.withdrawAmount
						},
						userAnswerId: $scope.userAnswerId,
						textAnswer: $scope.textAnswer
					});
					url = settings.jsonLink + 'validateWithdrawBank';
				} else {
					switch($scope.withdrawMethod) {
						case 'direct24': $scope.gmType = 59;break;
						case 'giropay': $scope.gmType = 60;break;
						case 'eps': $scope.gmType = 61;break;
					}
					$scope.request = jsonClone(getMethodRequest(), {
						beneficiaryName: $scope.beneficiaryName,
						swiftBicCode: ($scope.gmType == 61) ? $scope.swift : null,// EPS only
						iban: ($scope.gmType == 61) ? $scope.iban : null,//EPS only
						accountNumber: parseInt($scope.accountNum),
						bankName: $scope.bankNameTxt,
						branchNumber: parseInt($scope.branchName),
						branchAddress: $scope.branchAddress,
						amount: parseInt($scope.withdrawAmount),
						gmType: $scope.gmType, // EPS value->61, Giropay value->60, Direct24 value->59
						userId: $rootScope.copyopUser.userId,
						answerId: ($scope.userAnswerId == 0) ? null : $scope.userAnswerId,
						textAnswer: $scope.textAnswer
					});
					url = settings.commonServiceLink + 'GmWithdrawalServices/validateGMWithdraw';
				}
				
				$http.post(url, $scope.request)
					.success(function(data, status, headers, config) {
						$scope.loading = false;
						data.globalErrorField = "globalErrorField_withrow";
						if (data.userMessages != null) {
							data.userMessages = data.userMessages.map(function(current) {
								if (current.field == 'amount') {
									current.field = 'withdrawAmount';
								}
								return current;
							});
						}
						if (!handleErrors(data, 'validateWithdrawBank')) {
							fee = data.fee;
							$rootScope.openCopyOpPopup({
								type: 'withdraw', 
								lowAmountWithdraw: data.lowAmountWithdraw, 
								onOk: function(){$scope.insertWithdrawBank()},
								//body: (data.lowAmountWithdraw == 0) ? 'withdraw.cc.approve.message' : 'withdraw.cc.approve.message.low.amount',
								body: 'withdraw.cc.approve.message.low.amount',
								bodyParams: {
									fee: ($scope.withdrawMethod == 'bankWire') ? data.fee : formatAmount(data.fee, 0, true),
									amount: formatAmount(data.lowAmountWithdraw, 0, true)
								}
							});
						}
					})
					.error(function(data, status, headers, config) {
						$scope.loading = false;
						handleNetworkError(data);
					});
			}
		} else {
			logIt({'type':3,'msg':_form.$error});
		}
	}

	$scope.insertWithdrawCancel = function() {
		switch($scope.withdrawMethod) {
			case 'direct24': $scope.transactionTypeId = 59;break;
			case 'giropay': $scope.transactionTypeId = 60;break;
			case 'eps': $scope.transactionTypeId = 61;break;
			case 'bankWire': $scope.transactionTypeId = 14;break;
		}
		var InsertWithdrawCancelMethodRequest = jsonClone(getUserMethodRequest(), {
			transactionTypeId: $scope.transactionTypeId
		});
		
		$http.post(settings.jsonLink + 'insertWithdrawCancel', InsertWithdrawCancelMethodRequest)
			.success(function(data, status, headers, config) {
				if (!handleErrors(data, 'insertWithdrawCancel')) {
					$rootScope.closeCopyOpPopup();
				}
			})
			.error(function(data, status, headers, config) {
				$scope.loading = false;
				handleNetworkError(data);
			});
	}
	
	$scope.insertWithdrawBank = function() {
		if (!$scope.loading) {
			$scope.loading = true;
			var url = '';
			if ($scope.withdrawMethod == 'bankWire') {
				url = settings.jsonLink + 'insertWithdrawBank';
			} else {
				url = settings.commonServiceLink + 'GmWithdrawalServices/insertGmWithdraw';
			}
			
			$http.post(url, $scope.request)
				.success(function(data, status, headers, config) {
					$scope.loading = false;
					data.globalErrorField = "globalErrorField_withrow";
					if (!handleErrors(data, 'insertWithdrawBank')) {
						$rootScope.copyopUser.user.balance = data.balance;
						$rootScope.copyopUser.user.balanceWF = formatAmount(data.balance, 0);
						$rootScope.openCopyOpPopup({
							scope: $scope,
							type: 'withdrawSuccess', 
							lowAmountWithdraw: data.lowAmountWithdraw, 
							//body: (data.lowAmountWithdraw == 0) ? 'withdraw.cc.success' : 'withdraw.cc.success.low.amount',
							body: 'withdraw.bank-wire.success.low.amount',
							onclose: function(){$scope.getCopyopUser({onStateChange: true, stateMachine: false});},
							bodyParams: {
								heading: 'withdraw_popUp_heading',
								amount: formatAmount($scope.withdrawAmount, 0),
								fee: ($scope.withdrawMethod == 'bankWire') ? fee : formatAmount(fee, 0, true),
							}
						});
						angular.element(g('UserTransactionsCtrlHolder')).scope().getUserTransactions();
						optimoveReportGeneralEvent(optimoveEvents.withdraw, {});
					}
				})
				.error(function(data, status, headers, config) {
					$scope.loading = false;
					handleNetworkError(data);
				});
		}
	}

	waitForIt();
}]);

copyOpApp.controller('UserTransactionsCtrl', ['$rootScope', '$scope', '$http', function($rootScope, $scope, $http) {
	$scope.ready = false;
	function waitForIt() {
		if (!$scope.$parent.$parent.ready) {
			setTimeout(function(){waitForIt()},50);
		} else {
			if (settings.loggedIn) {//if not logged in don't make request
				$scope.getUserTransactions();
			}
		}
	}

	$scope.getUserTransactions = function() {
		$http.post(settings.jsonLink + 'getUserTransactions', getUserMethodRequest())
			.success(function(data, status, headers, config) {
				if (!handleErrors(data, 'getUserTransactions')) {
					$scope.transactions = data.transactions;
					$scope.ready = true;
				}
			})
			.error(function(data, status, headers, config) {
				handleNetworkError(data);
			});
	}

	$scope.insertWithdrawsReverse = function(entry) {
		if (!$scope.loading) {
			$scope.loading = true;
			
			var TransactionMethodRequest = jsonClone(getUserMethodRequest(), {
				trans: [entry]
			});
			
			$http.post(settings.jsonLink + 'insertWithdrawsReverse', TransactionMethodRequest)
				.success(function(data, status, headers, config) {
					$scope.loading = false;
					if (!handleErrors(data, 'insertWithdrawsReverse')) {
						$rootScope.copyopUser.user.balance = data.balance;
						$rootScope.copyopUser.user.balanceWF = formatAmount(data.balance, 0, true);
						$scope.getUserTransactions();
						$rootScope.openCopyOpPopup({
							type: 'info', 
							bodyTxt: data.userMessages[0].message
						});
					}
				})
				.error(function(data, status, headers, config) {
					$scope.loading = false;
					handleNetworkError(data);
				});
		}
	}
	waitForIt();
}]);

copyOpApp.controller('EditCardCtrl', ['$rootScope', '$scope', '$http', '$state', function($rootScope, $scope, $http, $state) {
	$scope.ready = false;
	$scope.shortYear = settings.shortYear;
	$scope.shortYearFuture = settings.shortYearFuture;
	function waitForIt() {
		if (!$scope.$parent.ready) {
			setTimeout(function(){waitForIt()},50);
		} else {
			if (settings.loggedIn) {//if not logged in don't make request
				init();
			}
		}
	}
	waitForIt();

	function init() {
		$http.post(settings.jsonLink + 'getAvailableCreditCardsWithEmptyLine', getUserMethodRequest())
			.success(function(data, status, headers, config) {
				if (!handleErrors(data, 'getAvailableCreditCardsWithEmptyLine')) {
					$scope.cards = data.options;
					if ($scope.cards != null) {
						for (var i = 0; i < $scope.cards.length; i++) {
							$scope.cards[i].ccNumberDsp = cardFilter($scope.cards[i].ccNumber);
						}
					}
					$scope.ready = true;
				}
			})
			.error(function(data, status, headers, config) {
				handleNetworkError(data);
			});
	}
	
	$scope.selected = false;
	$scope.showCcInfo = function(data) {
		$scope.selected = true;
		$scope.editCard = data;
	}
	
	$scope.updateCard = function(_form) {
		if (_form.$valid) {
			if (!$scope.loading) {
				$scope.loading = true;
				
				var CardMethodRequest = jsonClone(getUserMethodRequest(), {
					card: $scope.editCard
				});

				$http.post(settings.jsonLink + 'updateCard', CardMethodRequest)
					.success(function(data, status, headers, config) {
						$scope.loading = false;
						if (!handleErrors(data, 'updateCard')) {
							$scope.selected = false;
							$rootScope.openCopyOpPopup({
								type: 'info', 
								bodyTxt: data.userMessages[0].message
							});
						}
					})
					.error(function(data, status, headers, config) {
						$scope.loading = false;
						handleNetworkError(data);
					});
			}
		} else {
			logIt({'type':3,'msg':_form.$error});
		}
	}
	$scope.cancelUpdateCard = function() {
		$scope.selected = false;
	}
	
	$scope.deleteList = [];
	$scope.deleteCreditCards = function() {
		if (!$scope.loading) {
			$scope.loading = true;
			for (var i = 0; i < $scope.deleteList.length; i++) {
				$scope.deleteCreditCard($scope.deleteList[i]);
			}
			$scope.deleteList = [];
		}
	}
	$scope.deleteCreditCard = function(cardId) {
		var CreditCardRequest = jsonClone(getUserMethodRequest(), {
			cardId: cardId
		});
		
		$http.post(settings.jsonLink + 'deleteCreditCard', CreditCardRequest)
			.success(function(data, status, headers, config) {
				$scope.loading = false;
				if (!handleErrors(data, 'deleteCreditCard')) {
					var inx = searchJsonKeyInArray($scope.cards, 'id', cardId);
					$scope.cards.splice(inx, 1); 
				}
			})
			.error(function(data, status, headers, config) {
				$scope.loading = false;
				handleNetworkError(data);
			});
	}
	
	$scope.selectCheckbox = function($event, id) {
		var idx = $scope.deleteList.indexOf(id);
		// is currently selected
		if (idx > -1) {
			$scope.deleteList.splice(idx, 1);
		}
		// is newly selected
		else {
			$scope.deleteList.push(id);
		}
	}
}]);

copyOpApp.controller('UserBonusesCtrl', ['$rootScope', '$scope', '$http', function($rootScope, $scope, $http) {
	$scope.ready = false;
	$scope.currentPage = 0;
	$scope.lastPage = -1;
	$scope.history = [];
	$scope.rowsOnPage = settings.resultsOnPage;

	function waitForIt() {
		if (!$scope.$parent.ready) {
			setTimeout(function(){waitForIt()},50);
		} else {
			if (settings.loggedIn) {//if not logged in don't make request
				init();
			}
		}
	}
	
	function init() {
		$scope.getPage(0);
	}

	$scope.getPage = function(page) {
		if (page == 'prev') {
			page = --$scope.currentPage;
			if (page < 0) {page = 0;}
		} else if (page == 'next') {
			page = ++$scope.currentPage;
		}
		$scope.currentPage = page;
		if (isUndefined($scope.history[page])) {
			var BonusMethodRequest = jsonClone(getUserMethodRequest(), {
					startRow: (page * settings.resultsOnPage),
					pageSize: (settings.resultsOnPage + 1)
				})
			$http.post(settings.jsonLink + 'getUserBonuses', BonusMethodRequest)
				.success(function(data, status, headers, config) {
					if (!handleErrors(data, 'getUserBonuses')) {
						data.page = page;
						$scope.turnoverFactor = data.turnoverFactor;
						$scope.ready = true;
						
						if (data.bonuses.length == 0) {
							$scope.lastPage = page;
							$scope.bonuses = [];
						} else if (data.bonuses.length < (settings.resultsOnPage + 1)) {
							$scope.lastPage = page;
							$scope.setInfo(data);
						} else {
							$scope.setInfo(data);
						}
					}
				})
				.error(function(data, status, headers, config) {
					handleNetworkError(data);
				});
		} else {
			$scope.bonuses = $scope.history[page];
		}
	}
	
	$scope.setInfo = function(data) {
		$scope.bonuses = data.bonuses;
		$scope.history[data.page] = $scope.bonuses;
	}
	
	$scope.updateBonusStatus = function(entry) {
		if (!$scope.loading) {
			$scope.loading = true;
			
			var bonusMethodRequest = jsonClone(getUserMethodRequest() ,{
				bonusId: entry.id,
				bonusStateId: 5
			});
			
			$http.post(settings.jsonLink + 'updateBonusStatus', bonusMethodRequest)
				.success(function(data, status, headers, config) {
					$scope.loading = false;
					if (!handleErrors(data, 'updateBonusStatus')) {
						entry.bonusStateId = 5;
						$scope.getCopyopUser({onStateChange: true});
					}
				})
				.error(function(data, status, headers, config) {
					$scope.loading = false;
					handleNetworkError(data);
				});
		}
	}

	waitForIt();
}]);

copyOpApp.controller('bankWireCtr', ['$rootScope', '$scope', '$http', function($rootScope, $scope, $http) {
	$scope.ready = false;
	function waitForIt() {
		if (isUndefined(settings.defaultCurrencyId)) {
			setTimeout(function(){waitForIt()},50);
		} else {
			if (settings.loggedIn) {//if not logged in don't make request
				init();
			}
		}
	}

	function init() {
//		$scope.currencyShort = currencyMap[settings.defaultCurrencyId].shortName;
//		$scope.regulatedShort = (settings.isRegulated) ? 'reg' : 'nonreg';
		
		var defaultInfo = -1;
		var currencyInfo = -1;
		
		bankWireInfo.map(function(current, index) {
			if (current.currencies.indexOf('all') > -1) {
				defaultInfo = index;
			} else if (current.currencies.indexOf(settings.defaultCurrencyId) > -1) {
				currencyInfo = index;
			}
		});
		
		$scope.bankWireInfo = bankWireInfo[(currencyInfo > -1) ? currencyInfo : defaultInfo];
		
		$scope.hasDirect24 = $scope.hasDirect24();
		$scope.hasGiropay = $scope.hasGiropay();
		$scope.hasEps = $scope.hasEps();
	}
//	init();
	
	waitForIt();
}]);

copyOpApp.controller('NetellerCcControler', ['$rootScope', '$scope', '$http', function($rootScope, $scope, $http) {
	$scope.ready = false;
	$scope.form = {};
	$scope.additionalFields = false;
	$scope.shortYear = settings.shortYear;
	$scope.shortYearFuture = settings.shortYearFuture;
	$scope.yearMinus18 = new Date().getFullYear() - 18;
	function waitForIt() {
		if (!$scope.$parent.ready) {
			setTimeout(function(){waitForIt()},50);
		} else {
			if (settings.loggedIn) {//if not logged in don't make request
				init();
			}
		}
	}
	waitForIt();
	
	function init() {
		$scope.getMonths();
		$rootScope.getSkinCurrencies($rootScope.copyopUser.user.currencyId);
		$scope.ready = true;
		
		var FirstEverDepositCheckRequest = jsonClone(getMethodRequest(), {
			userId: $rootScope.copyopUser.userId}
		);
		
		$http.post(settings.jsonLink + 'getFirstEverDepositCheck', FirstEverDepositCheckRequest)
			.success(function(data, status, headers, config) {
				if (!handleErrors(data, 'getFirstEverDepositCheck')) {
					$scope.additionalFields = data.firstEverDeposit;
				}
			})
			.error(function(data, status, headers, config) {
				handleNetworkError(data);
			});
			
		$scope.hasDirect24 = $scope.hasDirect24();
		$scope.hasGiropay = $scope.hasGiropay();
		$scope.hasEps = $scope.hasEps();
	}
	
	
	$scope.submitNeteller = function(_form) {
		if (_form.$valid) {
			if ($scope.additionalFields) {
				if (!$scope.loading) {
					$scope.loading = true;
					
					var UpdateUserMethodRequest = jsonClone(getUserMethodRequest(), {
						user: {
							id: $rootScope.copyopUser.user.id,
							currencyId: $rootScope.copyopUser.user.currency.id,
							street: $scope.form.street,
							cityName: $scope.form.cityName,
							zipCode: $scope.form.zipCode,
							birthDayUpdate: $scope.form.birthDayUpdate,
							birthMonthUpdate: $scope.form.birthMonthUpdate,
							birthYearUpdate: $scope.form.birthYearUpdate
						}
					});
					
					$http.post(settings.jsonLink + 'updateUserAdditionalFields', UpdateUserMethodRequest)
						.success(function(data, status, headers, config) {
							$scope.loading = false;
							if (!handleErrors(data, 'updateUserAdditionalFields')) {
								settings.defaultCurrencyId = $rootScope.copyopUser.user.currency.id;
								settings.currencySymbol = $rootScope.copyopUser.user.currency.symbol;
								settings.currencyLeftSymbol = $rootScope.copyopUser.user.currency.isLeftSymbol;
								settings.decimalPointDigits = $rootScope.copyopUser.user.currency.decimalPointDigits;
								$scope.additionalFields = false;
								//$scope.setUser(data);
								$scope.insertNetellerDeposit();
								// $scope.getCopyopUser({onStateChange: true});
							}
						})
						.error(function(data, status, headers, config) {
							$scope.loading = false;
							handleNetworkError(data);
						});
				}
			} else {
				$scope.insertNetellerDeposit();
			}
		} else {
			logIt({'type':3,'msg':_form.$error});
		}
	}
	
	$scope.insertNetellerDeposit = function() {
		if (!$scope.loading) {
			$scope.loading = true;
			
			var request = jsonClone(getUserMethodRequest(), {
				amount: $scope.form.depositAmount,
				email: $scope.form.email,
				verificationCode: $scope.form.verificationCode
			});
			
			$http.post(settings.jsonLink + 'insertNetellerDeposit', request)
				.success(function(data, status, headers, config) {
					$scope.loading = false;
					if (!handleErrors(data, 'insertNetellerDeposit')) {
						$rootScope.closeCopyOpPopup();
						$rootScope.openCopyOpPopup({
							type: 'info', 
							header: 'deposit-success-header', 
							body: 'deposit-success-body', 
							onclose: null,
							bodyParams: {
								amount: formatAmount($scope.form.depositAmount, 2),
								transactionId: data.transactionId,
							}
						});
						$rootScope.afterDeposit = true;
						$scope.getCopyopUser({onStateChange: true});
					}
				})
				.error(function(data, status, headers, config) {
					$scope.loading = false;
					handleNetworkError(data);
				});
		}
	}
}]);

copyOpApp.controller('insertDirectDeposit', ['$rootScope', '$scope', '$http', function($rootScope, $scope, $http) {
	$scope.ready = false;
	$scope.form = {};
	$scope.additionalFields = false;
	$scope.shortYear = settings.shortYear;
	$scope.shortYearFuture = settings.shortYearFuture;
	$scope.yearMinus18 = new Date().getFullYear() - 18;
	function waitForIt() {
		if (!$scope.$parent.ready) {
			setTimeout(function(){waitForIt()},50);
		} else {
			if (settings.loggedIn) {//if not logged in don't make request
				init();
			}
		}
	}
	waitForIt();
	
	function init() {
		$scope.getMonths();
		$rootScope.getSkinCurrencies($rootScope.copyopUser.user.currencyId);
		$scope.ready = true;
		
		var FirstEverDepositCheckRequest = jsonClone(getMethodRequest(), {
			userId: $rootScope.copyopUser.userId}
		);
		
		$http.post(settings.jsonLink + 'getFirstEverDepositCheck', FirstEverDepositCheckRequest)
			.success(function(data, status, headers, config) {
				if (!handleErrors(data, 'getFirstEverDepositCheck')) {
					$scope.additionalFields = data.firstEverDeposit;
				}
			})
			.error(function(data, status, headers, config) {
				handleNetworkError(data);
			});
		
		$scope.hasDirect24 = $scope.hasDirect24();
		$scope.hasGiropay = $scope.hasGiropay();
		$scope.hasEps = $scope.hasEps();
	}
	
	
	$scope.submitDirectDecposit = function(_form) {
		if (_form.$valid) {
			if ($scope.additionalFields) {
				if (!$scope.loading) {
					$scope.loading = true;
					
					var UpdateUserMethodRequest = jsonClone(getUserMethodRequest(), {
						user: {
							id: $rootScope.copyopUser.user.id,
							currencyId: $rootScope.copyopUser.user.currency.id,
							street: $scope.form.street,
							cityName: $scope.form.cityName,
							zipCode: $scope.form.zipCode,
							birthDayUpdate: $scope.form.birthDayUpdate,
							birthMonthUpdate: $scope.form.birthMonthUpdate,
							birthYearUpdate: $scope.form.birthYearUpdate
						}
					});
					
					$http.post(settings.jsonLink + 'updateUserAdditionalFields', UpdateUserMethodRequest)
						.success(function(data, status, headers, config) {
							$scope.loading = false;
							if (!handleErrors(data, 'updateUserAdditionalFields')) {
								settings.defaultCurrencyId = $rootScope.copyopUser.user.currency.id;
								settings.currencySymbol = $rootScope.copyopUser.user.currency.symbol;
								settings.currencyLeftSymbol = $rootScope.copyopUser.user.currency.isLeftSymbol;
								settings.decimalPointDigits = $rootScope.copyopUser.user.currency.decimalPointDigits;
								$scope.additionalFields = false;
								//$scope.setUser(data);
								$scope.insertDirectDeposit();
								// $scope.getCopyopUser({onStateChange: true});
							}
						})
						.error(function(data, status, headers, config) {
							$scope.loading = false;
							handleNetworkError(data);
						});
				}
			} else {
				$scope.insertDirectDeposit();
			}
		} else {
			logIt({'type':3,'msg':_form.$error});
		}
	}
	
	$scope.insertDirectDeposit = function() {
		if (!$scope.loading) {
			$scope.loading = true;
			
			var paymentTypeParam = 1;
			switch($rootScope.$state.current.name) {
				case 'ln.in.a.personal.my-account.banking.direct24':
					paymentTypeParam = payment_type.direct24;
					break;
				case 'ln.in.a.personal.my-account.banking.giropay':
					paymentTypeParam = payment_type.giropay;
					break;
				case 'ln.in.a.personal.my-account.banking.eps':
					paymentTypeParam = payment_type.eps;
					break;
			}
			var InatecDepositMethodRequest = jsonClone(getUserMethodRequest(), {
				depositAmount: $scope.form.depositAmount,
				accountNum: $scope.form.accountNum,
				bankSortCode: $scope.form.bankSortCode,
				beneficiaryName: $scope.form.beneficiaryName,
				paymentTypeParam: paymentTypeParam
			});
			
			$http.post(settings.jsonLink + 'insertDirectDeposit', InatecDepositMethodRequest)
				.success(function(data, status, headers, config) {
					$scope.loading = false;
					if (!handleErrors(data, 'insertDirectDeposit')) {
						if (data.success) {
							$rootScope.openCopyOpPopup({
								type: 'info', 
								header: 'deposit-success-header', 
								body: 'deposit-success-body', 
								onclose: null,
								bodyParams: {
									amount: formatAmount($scope.form.depositAmount, 2),
									transactionId: data.transactionId,
								}
							});
							$rootScope.afterDeposit = true;
							$scope.getCopyopUser({onStateChange: true});
						} else {
							window.location = data.redirectUrl;
						}
					}
				})
				.error(function(data, status, headers, config) {
					$scope.loading = false;
					handleNetworkError(data);
				});
		}
	}
}]);