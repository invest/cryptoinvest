
/**
* Sorts assets
*/
copyOpApp.filter('sortAssets', function () {
	function isOpen(inv) {
		return ((isNaN(parseFloat(inv.expiryLevel)) || (parseFloat(inv.expiryLevel) == 0)) ? 1 : 0);
	}
	
	return function (assets, sortAssetsParams) {
		if(typeof sortAssetsParams === "undefined"){
			return assets;
		}
		var array = [];
		var result = [];
		var openFirst = sortAssetsParams.openFirst;
		var openSortField = sortAssetsParams.openSortField;
		var openSortFieldType = sortAssetsParams.openSortFieldType;
		var openSortOrder = sortAssetsParams.openSortOrder;
		var settledSortField = sortAssetsParams.settledSortField;
		var settledSortFieldType = sortAssetsParams.settledSortFieldType;
		var settledSortOrder = sortAssetsParams.settledSortOrder;
		
		if(typeof openFirst === "undefined"){openFirst = true;}
		if(typeof openSortField === "undefined"){openSortField = 'timeEstClosing';}
		if(typeof openSortFieldType === "undefined"){openSortFieldType = 'int';}
		if(typeof openSortOrder === "undefined"){openSortOrder = 'desc';}
		if(typeof settledSortField === "undefined"){settledSortField = 'timeEstClosing';}
		if(typeof settledSortFieldType === "undefined"){settledSortFieldType = 'int';}
		if(typeof settledSortOrder === "undefined"){settledSortOrder = 'desc';}
		
		if(typeof assets !== "undefined"){
			Object.keys(assets).forEach(function (key) {
				assets[key].name = key;
				array.push(assets[key]);
			});
			
			var openAssetsArr = [];
			for(var i = 0; i < array.length; i++){
				if(isOpen(array[i]) == 1){
					openAssetsArr.push(array[i]);
				}
			}
			
			var settledAssetsArr = [];
			for(var i = 0; i < array.length; i++){
				if(isOpen(array[i]) != 1){
					settledAssetsArr.push(array[i]);
				}
			}
			
			openAssetsArr.sort(function (a, b) {
				if(openSortFieldType == 'date'){
					if(openSortOrder == 'desc'){
						return new Date(b[openSortField]).getTime() - new Date(a[openSortField]).getTime();
					}else{
						return new Date(a[openSortField]).getTime() - new Date(b[openSortField]).getTime();
					}
				}else if(openSortFieldType == 'int'){
					if(openSortOrder == 'desc'){
						return b[openSortField] - a[openSortField];
					}else{
						return a[openSortField] - b[openSortField];
					}
				}else{
					if(openSortOrder == 'desc'){
						return b[openSortField] - a[openSortField];
					}else{
						return a[openSortField] - b[openSortField];
					}
				}
			});
			
			settledAssetsArr.sort(function (a, b) {
				if(settledSortFieldType == 'date'){
					if(settledSortOrder == 'desc'){
						return new Date(b[settledSortField]).getTime() - new Date(a[settledSortField]).getTime();
					}else{
						return new Date(a[settledSortField]).getTime() - new Date(b[settledSortField]).getTime();
					}
				}else if(settledSortFieldType == 'int'){
					if(settledSortOrder == 'desc'){
						return b[settledSortField] - a[settledSortField];
					}else{
						return a[settledSortField] - b[settledSortField];
					}
				}else{
					if(settledSortOrder == 'desc'){
						return b[settledSortField] - a[settledSortField];
					}else{
						return a[settledSortField] - b[settledSortField];
					}
				}
			});
			
			
			if(openFirst){
				result = openAssetsArr.concat(settledAssetsArr);
			}else{
				result = settledAssetsArr.concat(openAssetsArr);
			}
		}
		
		return result;
	};
});

copyOpApp.filter('shuffle', function() {
    var shuffledArr = []
    var shuffledLength = 0;
    return function(arr) {
        var o = arr.slice(0, arr.length);
        if (shuffledLength == arr.length) return shuffledArr;
        for(var j, x, i = o.length; i; j = parseInt(Math.random() * i), x = o[--i], o[i] = o[j], o[j] = x);
        shuffledArr = o;
        shuffledLength = o.length;
        return o;
    };
});