function configureLsCopyOpConnection(){
	var lsClient = new LightstreamerClient(settings.lsServer, 'COPYOP');//Adapter name
	lsClient.connectionOptions.setMaxBandwidth(20);
	lsClient.connectionOptions.setIdleMillis(30000);
	lsClient.connectionOptions.setPollingMillis(1000);
	lsClient.connectionOptions.setReconnectTimeout(10000);
	lsClient.connectionSharing.enableSharing("CopyOpCommonConnection", "ATTACH", "CREATE");
	if (settings.loggedIn) {
		lsClient.connectionDetails.setUser("a" + settings.userName + "a");
		lsClient.connectionDetails.setPassword(settings.hashedPassword);
	}
	return lsClient;
}

function connectToLsCopyOp($rootScope, $scope, params){//['name':'','subscriptionType':subscriptionType,'group':group,'schema':schema,'dataAdapterName':dataAdapterName]
	logIt({'type':1,'msg':'connectToLsCopyOp: ' + params.name});
	var obj = new Subscription(params.type, params.subscription, params.schema);
	obj.setDataAdapter(params.dataAdapterName);
	obj.setRequestedSnapshot("yes");
	obj.setRequestedMaxFrequency(1.0);
	obj.addListener({
		onSubscription: function() {
			logIt({'type':1,'msg':'SUBSCRIBED: ' + params.name});
		},
		onUnsubscription: function() {
			logIt({'type':1,'msg':'UNSUBSCRIBED: ' + params.name});
		},
		onItemUpdate: function (updateObject) { 
			logIt({'type':1,'msg':'ItemUpdate: ' + params.name});
			params.callBack($rootScope, $scope, updateObject);
		}
	});
	$rootScope.lsCoptyOp.lsCpClient.subscribe(obj);
	return obj;
}

function connectToLsCopyOp_global($rootScope, $scope) {
	if (isUndefined($rootScope.lsCoptyOp.lsCpClient)) {
		$rootScope.lsCoptyOp.lsCpClient = configureLsCopyOpConnection();
		$rootScope.lsCoptyOp.lsCpClient.connect();
	}
	$rootScope.lsCoptyOp.lsEngineReady = true;
	
	var queueType = {
		news: 1,
		explore: 2,
		inbox: 3,
		mobile_notification: 4, 
		in_app_strip: 5, 
		hot: 6,
		system: 7,
		web_popup: 8
	}
	var subscriptionEnd = "_" + settings.userName + "_" + $rootScope.copyopUser.userId;
	var schemaFeed = [
        FMT.PROPERTY_KEY,
    	FMT.PROPERTY_COMMAND,
		FMT.PROPERTY_KEY_UPDATE_TYPE,
		FMT.PROPERTY_KEY_AMOUNT_IN_CURR + settings.defaultCurrencyId, 
		FMT.PROPERTY_KEY_ASSET_LAST_TRADING_HOURS,
		FMT.PROPERTY_KEY_ASSET_TREND, 
		FMT.PROPERTY_KEY_AVATAR, 
		FMT.PROPERTY_KEY_COPIED_COUNT, 
		FMT.PROPERTY_KEY_COPIERS, 
		FMT.PROPERTY_KEY_DEST_USER_ID, 
		FMT.PROPERTY_KEY_DIRECTION, 
		FMT.PROPERTY_KEY_HIT_RATE, 
		FMT.PROPERTY_KEY_INV_ID, 
		FMT.PROPERTY_KEY_INVESTMENT_TYPE, 
		FMT.PROPERTY_KEY_LEVEL, 
		FMT.PROPERTY_KEY_LEVEL,
		FMT.PROPERTY_KEY_MARKET_ID, 
		FMT.PROPERTY_KEY_NICKNAME, 
		FMT.PROPERTY_KEY_ODDS_LOSE, 
		FMT.PROPERTY_KEY_ODDS_WIN, 
		FMT.PROPERTY_KEY_OPPORTUNITY_EXPIRE, 
		FMT.PROPERTY_KEY_OPPORTUNITY_ID, 
		FMT.PROPERTY_KEY_SCHEDULED,
		FMT.PROPERTY_KEY_PROFIT + settings.defaultCurrencyId,
		FMT.PROPERTY_KEY_TIME_SETTLED, 
		FMT.PROPERTY_KEY_TIME_CREATED, 
		FMT.PROPERTY_KEY_USER_ID, 
		FMT.PROPERTY_KEY_WATCHERS,
		FMT.PROPERTY_KEY_DEST_NICKNAME, 
		FMT.PROPERTY_BONUS_TYPE_ID, 
		FMT.PROPERTY_BONUS_DESCRIPTION,
		FMT.PROPERTY_KEY_TOP_RANK, 
		FMT.PROPERTY_KEY_RANKED_IN_TOP,
		FMT.PROPERTY_KEY_COINS_BALANCE,
		FMT.PROPERTY_KEY_SEATS_LEFT, 
		FMT.PROPERTY_KEY_FIRST_NAME, 
		FMT.PROPERTY_KEY_LAST_NAME
	];
	//news feed
	$rootScope.lsCoptyOp.newsProperties = {
		name: 'feed',
		type: $rootScope.lsCoptyOp.subscriptionType,
		subscription: queueType.news + subscriptionEnd,
		schema: schemaFeed,
		dataAdapterName: $rootScope.lsCoptyOp.dataAdapterName,
		callBack: lsFeedUpdate
	}
	$rootScope.lsCoptyOp.newsConnect = connectToLsCopyOp($rootScope, $scope, $rootScope.lsCoptyOp.newsProperties);
	
	//explore
	$rootScope.lsCoptyOp.exploreProperties = {
		name: 'explore',
		type: $rootScope.lsCoptyOp.subscriptionType,
		subscription: queueType.explore + subscriptionEnd,
		schema: schemaFeed,
		dataAdapterName: $rootScope.lsCoptyOp.dataAdapterName,
		callBack: lsExploreUpdate
	}
	$rootScope.lsCoptyOp.exploreConnect = connectToLsCopyOp($rootScope, $scope, $rootScope.lsCoptyOp.exploreProperties);
	
	//inbox
	$rootScope.lsCoptyOp.inboxProperties = {
		name: 'inbox',
		type: $rootScope.lsCoptyOp.subscriptionType,
		subscription: queueType.inbox + subscriptionEnd,
		schema: schemaFeed,
		dataAdapterName: $rootScope.lsCoptyOp.dataAdapterName,
		callBack: lsInboxUpdate
	}
	$rootScope.lsCoptyOp.indexConnect = connectToLsCopyOp($rootScope, $scope, $rootScope.lsCoptyOp.inboxProperties);
	
	var schemaCoShoppingBag = [
		FMT.PROPERTY_KEY,
		FMT.PROPERTY_COMMAND,
		FMT.PROPERTY_KEY_SRC_AVATAR,
		FMT.PROPERTY_KEY_DIRECTION,
		FMT.PROPERTY_KEY_LEVEL,
		FMT.PROPERTY_KEY_MARKET_ID,
		FMT.PROPERTY_KEY_SRC_NICKNAME,
		FMT.PROPERTY_KEY_SRC_USER_ID,
		FMT.PROPERTY_KEY_AMOUNT
	];
	//shoppingBag stuff (WEB_POPUP)
	$rootScope.lsCoptyOp.shoppingBagCopyop = {
		name: 'web_popup',
		type: $rootScope.lsCoptyOp.subscriptionType,
		subscription: queueType.web_popup + subscriptionEnd,
		schema: schemaCoShoppingBag,
		dataAdapterName: $rootScope.lsCoptyOp.dataAdapterName,
		callBack: lsWebPopupUpdate
	}
	$rootScope.lsCoptyOp.shoppingConnect = connectToLsCopyOp($rootScope, $scope, $rootScope.lsCoptyOp.shoppingBagCopyop);
}

function lsFeedUpdate($rootScope, $scope, data) {
	var resp = {properties: {}};
	resp.properties = convertLSUpdateToUpdate(data);
	resp.msgType = data.getValue(FMT.PROPERTY_KEY_UPDATE_TYPE);
	
	$rootScope.newsFeed.unshift($rootScope.feedSwitch(resp));
	if ($rootScope.newsFeed.length > settings.feedLimit) {
		$rootScope.newsFeed = $rootScope.newsFeed.slice(0, settings.feedLimit);
	}
	if ($rootScope.unread.currentTab == 1) {
		$rootScope.unread.news = 0;
	} else if ($rootScope.unread.news < settings.feedLimit) {
		$rootScope.unread.news++;
	}
	$rootScope.$apply();
}

function lsExploreUpdate($rootScope, $scope, data) {
	var resp = {properties: {}};
	resp.properties = convertLSUpdateToUpdate(data);
	resp.msgType = data.getValue(FMT.PROPERTY_KEY_UPDATE_TYPE);
	
	$rootScope.exploreFeed.unshift($rootScope.feedSwitch(resp));
	if ($rootScope.exploreFeed.length > settings.feedLimit) {
		$rootScope.exploreFeed = $rootScope.exploreFeed.slice(0, settings.feedLimit);
	}
	if ($rootScope.unread.currentTab == 3) {
		$rootScope.unread.explore = 0;
	} else if ($rootScope.unread.explore < settings.feedLimit) {
		$rootScope.unread.explore++;
	}
	$rootScope.$apply();
}

function lsInboxUpdate($rootScope, $scope, data) {
	var resp = {properties: {}};
	resp.properties = convertLSUpdateToUpdate(data);
	resp.msgType = data.getValue(FMT.PROPERTY_KEY_UPDATE_TYPE);
	
	$rootScope.inbox.unshift($rootScope.feedSwitch(resp));
	if ($rootScope.inbox.length > settings.feedLimit) {
		$rootScope.inbox = $rootScope.inbox.slice(0, settings.feedLimit);
	}
	if ($rootScope.unread.inbox < settings.feedLimit) {
		$rootScope.unread.inbox++;
	}
	$rootScope.$apply();
}

var lsWebPopupUpdateTimer = null;
function lsWebPopupUpdate($rootScope, $scope, updateInfo) {
	if (lsWebPopupUpdateTimer == null) {
		var popIndex = searchJsonKeyInArray($scope.popUps,'type','bonusBalance');
		if (popIndex > -1) {
			$rootScope.closeCopyOpPopup({popupId: popIndex});
		}
		var popupId = $rootScope.openCopyOpPopup({type: 'copyopShoppingBag'});
		//Update assets in trading section
		if($rootScope.$state.includes('ln.in.a.asset')){
			angular.element(g('optionOptionsTab')).scope().getInvestments(settings.optionsLimit);
		}
	}
	
	var rtn = {
		userId: updateInfo.getValue(FMT.PROPERTY_KEY_SRC_USER_ID),
		nickname: updateInfo.getValue(FMT.PROPERTY_KEY_SRC_NICKNAME),
		avatar: updateInfo.getValue(FMT.PROPERTY_KEY_SRC_AVATAR),
		direction: (updateInfo.getValue(FMT.PROPERTY_KEY_DIRECTION) == 1) ? 'call' : 'put',
		asset: $scope.markets[updateInfo.getValue(FMT.PROPERTY_KEY_MARKET_ID)].displayName,
		amount: updateInfo.getValue(FMT.PROPERTY_KEY_AMOUNT),
		amountDsp: formatAmount(updateInfo.getValue(FMT.PROPERTY_KEY_AMOUNT), 0, true),
		level: updateInfo.getValue(FMT.PROPERTY_KEY_LEVEL),
	};
	
	$rootScope.copyOpShoppingBag.push(rtn);
	$rootScope.copyopUser.user.balance -= parseInt(rtn.amount);
	$rootScope.copyopUser.user.balanceWF = formatAmount($rootScope.copyopUser.user.balance, 0, true);
	var total = 0;
	for (var i = 0; i < $rootScope.copyOpShoppingBag.length; i++) {
		total += parseInt($rootScope.copyOpShoppingBag[i].amount);
	}
	$rootScope.totalCoShopingBag = formatAmount(total, 0, true);
	
	$rootScope.$apply();
	
	clearTimeout(lsWebPopupUpdateTimer);
	lsWebPopupUpdateTimer = setTimeout(function (){
		$scope.lsWebPopupClose(popupId);
	}, settings.timeToShowCoShopingBag);
}

function convertLSUpdateToUpdate(update) {
	var map = {};

	map[FMT.PROPERTY_KEY_OPPORTUNITY_ID] = update.getValue(FMT.PROPERTY_KEY_OPPORTUNITY_ID);
	map[FMT.PROPERTY_KEY_UPDATE_TYPE] = update.getValue(FMT.PROPERTY_KEY_UPDATE_TYPE);
	map[FMT.PROPERTY_KEY_AMOUNT_IN_CURR + settings.defaultCurrencyId] = update.getValue(FMT.PROPERTY_KEY_AMOUNT_IN_CURR + settings.defaultCurrencyId);
	map[FMT.PROPERTY_KEY_ASSET_LAST_TRADING_HOURS] = update.getValue(FMT.PROPERTY_KEY_ASSET_LAST_TRADING_HOURS);
	map[FMT.PROPERTY_KEY_ASSET_TREND] = update.getValue(FMT.PROPERTY_KEY_ASSET_TREND);
	map[FMT.PROPERTY_KEY_AVATAR] = update.getValue(FMT.PROPERTY_KEY_AVATAR);
	map[FMT.PROPERTY_KEY_COPIED_COUNT] = update.getValue(FMT.PROPERTY_KEY_COPIED_COUNT);
	map[FMT.PROPERTY_KEY_COPIERS] = update.getValue(FMT.PROPERTY_KEY_COPIERS);
	map[FMT.PROPERTY_KEY_DEST_USER_ID] = update.getValue(FMT.PROPERTY_KEY_DEST_USER_ID);
	map[FMT.PROPERTY_KEY_DEST_NICKNAME] = update.getValue(FMT.PROPERTY_KEY_DEST_NICKNAME);
	map[FMT.PROPERTY_KEY_DIRECTION] = update.getValue(FMT.PROPERTY_KEY_DIRECTION);
	map[FMT.PROPERTY_KEY_HIT_RATE] = update.getValue(FMT.PROPERTY_KEY_HIT_RATE);
	map[FMT.PROPERTY_KEY_INV_ID] = update.getValue(FMT.PROPERTY_KEY_INV_ID);
	map[FMT.PROPERTY_KEY_INVESTMENT_TYPE] = update.getValue(FMT.PROPERTY_KEY_INVESTMENT_TYPE);
	map[FMT.PROPERTY_KEY_LEVEL] = update.getValue(FMT.PROPERTY_KEY_LEVEL);
	map[FMT.PROPERTY_KEY_MARKET_ID] = update.getValue(FMT.PROPERTY_KEY_MARKET_ID);
	map[FMT.PROPERTY_KEY_NICKNAME] = update.getValue(FMT.PROPERTY_KEY_NICKNAME);
	map[FMT.PROPERTY_KEY_ODDS_LOSE] = update.getValue(FMT.PROPERTY_KEY_ODDS_LOSE);
	map[FMT.PROPERTY_KEY_ODDS_WIN] = update.getValue(FMT.PROPERTY_KEY_ODDS_WIN);
	map[FMT.PROPERTY_KEY_OPPORTUNITY_EXPIRE] = update.getValue(FMT.PROPERTY_KEY_OPPORTUNITY_EXPIRE);
	map[FMT.PROPERTY_KEY_PROFIT + settings.defaultCurrencyId] = update.getValue(FMT.PROPERTY_KEY_PROFIT + settings.defaultCurrencyId);
	map[FMT.PROPERTY_KEY_TIME_SETTLED] = update.getValue(FMT.PROPERTY_KEY_TIME_SETTLED);
	map[FMT.PROPERTY_KEY_USER_ID] = update.getValue(FMT.PROPERTY_KEY_USER_ID);
	map[FMT.PROPERTY_KEY_WATCHERS] = update.getValue(FMT.PROPERTY_KEY_WATCHERS);
	map[FMT.PROPERTY_BONUS_TYPE_ID] = update.getValue(FMT.PROPERTY_BONUS_TYPE_ID);
	map[FMT.PROPERTY_BONUS_DESCRIPTION] = update.getValue(FMT.PROPERTY_BONUS_DESCRIPTION);
	map[FMT.PROPERTY_KEY_TOP_RANK] = update.getValue(FMT.PROPERTY_KEY_TOP_RANK);
	map[FMT.PROPERTY_KEY_RANKED_IN_TOP] = update.getValue(FMT.PROPERTY_KEY_RANKED_IN_TOP);
	map[FMT.PROPERTY_KEY_COINS_BALANCE] = update.getValue(FMT.PROPERTY_KEY_COINS_BALANCE);
	map[FMT.PROPERTY_KEY_TIME_CREATED] = update.getValue(FMT.PROPERTY_KEY_TIME_CREATED);
	map[FMT.PROPERTY_KEY_FIRST_NAME] = update.getValue(FMT.PROPERTY_KEY_FIRST_NAME);
	map[FMT.PROPERTY_KEY_LAST_NAME] = update.getValue(FMT.PROPERTY_KEY_LAST_NAME);

	logIt({'type':1,'msg':map});
	return map;
}
