function gd_slider(params) {
	var obj = {};
	obj.params = params;
	obj.generalTiming = (isUndefined(params.generalTiming)) ? 8 : params.generalTiming;
	obj.generalTimingFlag = isUndefined(params.timing);
	obj.mainEl = g(params.mainId);
	obj.mainElList = [];
	obj.navEl = g(params.navId);
	obj.navElList = [];
	obj.current = 0;
	obj.timerSlide = null;
	obj.timerSliding = null;
	
	//main sliders
	var els = obj.mainEl.getElementsByTagName('li');
	var num = 0;
	for (var i = 0; i < els.length; i++) {
		if (els[i].getAttribute('data-ignore') != 'true') {
			var t = (obj.generalTimingFlag) ? obj.generalTiming : (!isUndefined(obj.params.timing[i])) ? obj.params.timing[i] : obj.generalTiming;
			els[i].style.display = 'block';
			if (obj.params.slideType) {
				els[i].style.left = els[i].offsetWidth + 'px';
			}
			obj.mainElList.push({
				el: els[i],
				time: t,
				num: num
			});
			num++;
		}
	}
	
	//navigation
	var navPlace = 0;
	var els = obj.navEl.getElementsByTagName('li');
	var num = 0;
	for (var i = 0; i < els.length; i++) {
		if (els[i].getAttribute('data-ignore') != 'true') {
			if (navPlace == obj.current) {
				els[i].className += " current";
			}
			var dataHidden = els[i].getAttribute('data-hidden');
			if (!isUndefined(dataHidden)) {
				els[i].style.display = "none";
				break;
			}
			
			var dataShowAfter = els[i].getAttribute('data-show-after');
			if (!isUndefined(dataShowAfter)) {
				if (!isNaN(dataShowAfter)) {
					dataShowAfter = parseInt(dataShowAfter);
				} else {
					dataShowAfter = navPlace-1;
				}
				dataShowAfter = parseInt(els[i].getAttribute('data-show-after'));
				els[i].className += " hidden";
			}
			
			els[i].setAttribute('data-i', navPlace);
			
			addEvent(els[i], 'click', function(){
				obj.play({
					el: this
				})
			});
			
			var dataMap = parseInt(els[i].getAttribute('data-map'));
			
			obj.navElList.push({
				el: els[i],
				navPlace: navPlace,
				dataMap: (isNaN(dataMap)) ? 1 : dataMap,
				dataShowAfter: dataShowAfter
			});
			
			
			var dataMapCount = 1;
			if (!isNaN(dataMap)) {
				dataMapCount = parseInt(dataMap);
			}
			navPlace += dataMapCount;
		}
	}

	obj.prevBtn = g(params.mainId + '-prev');
	addEvent(obj.prevBtn, 'click', function(){
		obj.play({
			dir: 'prev',
		})
	});
	
	obj.nextBtn = g(params.mainId + '-next');
	addEvent(obj.nextBtn, 'click', function(){
		obj.play({
			dir: 'next',
		})
	});
	
	obj.mainElList[obj.current].el.style.zIndex = 2;
	obj.mainElList[obj.current].el.style.left = 0;
	
	if (obj.mainElList.length > 0 && params.autoStart) {
		obj.timerSlide = setTimeout(function() {
			obj.play({
				dir: 'next'
			});
		},obj.mainElList[obj.current].time * 1000);
	}

	//do the magic
	obj.play = function(params) {
		if (obj.timerSliding == null) {
			var target = null;
			var clickedElNum;
			var clickedEl;
			if (!isUndefined(params.el)) {
				// target = params.e.target || params.e.srcElement;
				target = params.el;
				clickedElNum = parseInt(target.getAttribute('data-i'));
				//if you click on the same element that is displayed
				if (clickedElNum == obj.current) {
					return;
				}
			}

			clearTimeout(obj.timerSlide);
			var next;
			if (!isUndefined(params.dir)) {
				if (params.dir == 'prev') {
					next = obj.current - 1;
					if (next < 0){
						next = obj.mainElList.length - 1;
					}
				}
				else{
					next = obj.current + 1;
					if (next >= obj.mainElList.length) {
						next = 0;
					}
				}
			} else if (!isUndefined(params.startFrom)) {
				next = params.startFrom;
			} else {
				next = clickedElNum;
			}
			
			var inc = 0;
			var scr = obj.mainEl.offsetWidth;
			var scr_left = scr;
			for (var i = 0; i < obj.mainElList.length; i++) {
				obj.mainElList[i].el.style.zIndex = 1;
			}
			
			if (params.dir == "prev") {
				obj.mainElList[next].el.style.left = -scr + 'px';
			} else {
				obj.mainElList[next].el.style.left = scr + 'px';
			}
			
			obj.mainElList[next].el.style.zIndex = 3;
			obj.mainElList[obj.current].el.style.zIndex = 2;
			
			for (var i = 0; i < obj.navElList.length; i++) {
				var className = obj.navElList[i].el.className;
				className = className.replace(/ current/g,'');
				className = className.replace(/ passed/g,'');
				obj.navElList[i].el.className = className;
				if (next >= obj.navElList[i].navPlace && next < (obj.navElList[i].navPlace + obj.navElList[i].dataMap)) {
					obj.navElList[i].el.className += " current";
				} else if (obj.navElList[i].navPlace < next) {
					obj.navElList[i].el.className += " passed";
				}
			}
			
			var doSetTimeout = function() {
				inc = Math.round(scr_left * 0.2);
				if(inc < 1){inc = 1;}

				if(params.dir == "prev"){
					obj.mainElList[next].el.style.left = obj.mainElList[next].el.offsetLeft + inc + "px";
					if (obj.params.slideType) {
						obj.mainElList[obj.current].el.style.left = obj.mainElList[obj.current].el.offsetLeft + inc + "px";
					}
				} else {
					obj.mainElList[next].el.style.left = obj.mainElList[next].el.offsetLeft - inc + "px";
					if (obj.params.slideType) {
						obj.mainElList[obj.current].el.style.left = obj.mainElList[obj.current].el.offsetLeft - inc + "px";
					}
				}
				
				scr_left -= inc;
				if (scr_left > 0) {
					obj.timerSliding = setTimeout(doSetTimeout,30);
				} else {
					obj.timerSliding = null;

					if (obj.mainElList[next].time > 0 && obj.params.autoSlide) {
						obj.timerSlide = setTimeout(function() {
							obj.play({
								dir: 'next'
							});
						},obj.mainElList[next].time * 1000);
					}
					
					obj.current = next;
					if (!isUndefined(obj.params.callBackOnChange)) {
						obj.params.callBackOnChange(obj.current);
					}
				}
			}
			doSetTimeout();
		}
	}
	
	//resize
	obj.restart = function() {
		clearTimeout(obj.timerSlide);
		clearTimeout(obj.timerSliding);
		obj.timerSliding = null;
		for (var i = 0; i < obj.mainElList.length; i++) {
			if (obj.params.slideType) {
				obj.mainElList[i].el.style.left = obj.mainElList[i].el.offsetWidth + 'px';
			}
		}
		obj.current = 0;
		obj.mainElList[obj.current].el.style.left = 0;
		if (obj.mainElList.length > 0 && params.autoStart) {
			obj.timerSlide = setTimeout(function() {
				obj.play({
					dir: 'next'
				});
			}, obj.mainElList[obj.current].time * 1000);
		}
	}
	
	return obj;
}