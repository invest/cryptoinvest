copyOpApp.controller('copyopNewsCtrl', ['$rootScope', '$scope', '$http', function($rootScope, $scope, $http) {
	$scope.ready = false;
	function waitForIt() {
		if (!$rootScope.ready) {
			setTimeout(function(){waitForIt()},50);
		} else {
			if (settings.loggedIn) {//if not logged in don't make request
				init();
			}
		}
	}
	waitForIt();

	function init() {
		if ($rootScope.newsFeed.length == 0) {
			$http.post(settings.jsonLink + 'getCopyopNews', getUserMethodRequest())
				.success(function(data, status, headers, config) {
					if (!handleErrors(data, 'getCopyopNews')) {
						$scope.ready = true;
						if (data.feed != null && data.feed.length > 0) {
							for (var i = 0; i < data.feed.length; i++) {
								$rootScope.newsFeed.push($rootScope.feedSwitch(data.feed[i]));
							}
						}
						// $rootScope.feedTimers.news = setInterval(function() {
							// $rootScope.feedFollowCheck('newsFeed');
						// },1000);
					}
				})
				.error(function(data, status, headers, config) {
					handleNetworkError(data);
				});
		} else {
			$scope.ready = true;
		}
	}
}]);

copyOpApp.controller('copyopExploreCtrl', ['$rootScope', '$scope', '$http', function($rootScope, $scope, $http) {
	$scope.ready = false;
	function waitForIt() {
		if (!$rootScope.ready) {
			setTimeout(function(){waitForIt()},50);
		} else {
			if (settings.loggedIn) {//if not logged in don't make request
				init();
			}
		}
	}
	waitForIt();

	function init() {
		if ($rootScope.exploreFeed.length == 0) {
			$http.post(settings.jsonLink + 'copyopExplore', getUserMethodRequest())
				.success(function(data, status, headers, config) {
					if (!handleErrors(data, 'copyopExplore')) {
						$scope.ready = true;
						if (data.feed != null && data.feed.length > 0) {
							for (var i = 0; i < data.feed.length; i++) {
								$rootScope.exploreFeed.push($rootScope.feedSwitch(data.feed[i]));
							}
						}
					}
				})
				.error(function(data, status, headers, config) {
					handleNetworkError(data);
				});
		} else {
			$scope.ready = true;
		}
	}
}]);

copyOpApp.controller('copyopHotTradersCtrl', ['$rootScope', '$scope', '$http', function($rootScope, $scope, $http) {
	$scope.feedLimit = 3;
	$scope.ready = false;
	function waitForIt() {
		if (!$rootScope.ready) {
			setTimeout(function(){waitForIt()},50);
		} else {
			init();
		}
	}

	$scope.updateType = [];
	$scope.hours = 0;

	$scope.getInfo = function() {
		var copyopHotDataMethodRequest = jsonConcat(getUserMethodRequest(), {
			updateTypeEnumId: $scope.updateTypeEnumId,
			isInitCall: $scope.isInitCall
		});
		$http.post(settings.jsonLink + 'getCopyopHotTraders', copyopHotDataMethodRequest)
			.success(function(data, status, headers, config) {
				data.loginNotCheck = true;
				if (!handleErrors(data, 'getCopyopHotTraders')) {
					$scope.ready = true;
					$rootScope.hotTraders = [];
					if (data.feed != null && data.feed.length > 0) {
						$rootScope.hotData.hotTraders.isFakeInfo = false;
						if (isUndefined($rootScope.hotData.hotTraders) || isUndefined($rootScope.hotData.hotTraders.hours)) {
							if (data.feed[0].properties.cpop_lastTradinghours < 100) {
								$scope.hours = data.feed[0].properties.cpop_lastTradinghours;
							}
						} else {
							$scope.hours = $rootScope.hotData.hotTraders.hours;
						}
						switch (data.feed[0].msgType) {
							case 61:
								$rootScope.hotData.hotTraders.hours = $scope.hours;
								$rootScope.hotData.hotTraders.intervalType = 'best-type-short-h';
								$rootScope.hotData.hotTraders.intervalTypeNum = $scope.hours;
								break;
							case 611:
								$rootScope.hotData.hotTraders.intervalType = 'best-type-short-w';
								$rootScope.hotData.hotTraders.intervalTypeNum = 1;
								break;
							case 612:
								$rootScope.hotData.hotTraders.intervalType = 'best-type-short-m';
								$rootScope.hotData.hotTraders.intervalTypeNum = 1;
								break;
							case 613:
								$rootScope.hotData.hotTraders.intervalType = 'best-type-short-m';
								$rootScope.hotData.hotTraders.intervalTypeNum = 3;
								break;
						}
						$rootScope.hotData.hotTraders.updateType = [];
						for (var i = 0; i < data.existUpdateTypeEnumId.length; i++) {
							var type = 0;
							if (data.existUpdateTypeEnumId[i].toString().length > 2) {
								type = data.existUpdateTypeEnumId[i].toString()[2];
							}
							$rootScope.hotData.hotTraders.updateType.push({
								hours: $scope.hours,
								type: 'best-type-' + type,
								fullType: data.existUpdateTypeEnumId[i]
							});
						}
						for (var i = 0; i < data.feed.length; i++) {
							$rootScope.hotTraders[i] = $rootScope.feedSwitch(data.feed[i]);
						}
					} else if($rootScope.$state.includes('ln.index')){
						$rootScope.hotData.hotTraders.isFakeInfo = true;
						var data = {"feed":[{"userId":-1,"queueType":6,"timeCreated":"Feb 25, 2015 3:01:47 PM","timeCreatedUUID":"38880c40-bcff-11e4-b681-d508b7730cb7","msgType":61,"properties":{"cpop_avatar":"https://www.copyop.com/jsonService/avatar/f9.png","cpop_watchers":"33","cpop_copying":"0","cpop_copiers":"5","cpop_user_frozen":"false","cpop_watching":"0","cpop_lastTradinghours":"12","cpop_profit_1":"15290.98","cpop_profit_5":"9541.36","cpop_userId":"869851","cpop_profit_4":"2499.84","cpop_nickname":"StepH485","cpop_profit_3":"3406.0","cpop_profit_2":"3864.0","cpop_hitRate":"54","cpop_profit_8":"4288452.58","cpop_profit_7":"24169.67","cpop_profit_6":"242678.08"}},{"userId":-1,"queueType":6,"timeCreated":"Feb 25, 2015 3:01:47 PM","timeCreatedUUID":"3887be20-bcff-11e4-b681-d508b7730cb7","msgType":61,"properties":{"cpop_avatar":"https://www.copyop.com/jsonService/avatar/m57.png","cpop_watchers":"29","cpop_copying":"0","cpop_copiers":"1","cpop_user_frozen":"false","cpop_watching":"0","cpop_lastTradinghours":"12","cpop_profit_1":"6991.47","cpop_profit_5":"4362.0","cpop_userId":"715920","cpop_profit_4":"1143.0","cpop_nickname":"&JuJohn","cpop_profit_3":"1557.55","cpop_profit_2":"1766.73","cpop_hitRate":"55","cpop_profit_8":"1960801.05","cpop_profit_7":"11051.05","cpop_profit_6":"110959.24"}},{"userId":-1,"queueType":6,"timeCreated":"Feb 25, 2015 3:01:47 PM","timeCreatedUUID":"388748f0-bcff-11e4-b681-d508b7730cb7","msgType":61,"properties":{"cpop_avatar":"https://www.copyop.com/jsonService/avatar/m70.png","cpop_watchers":"612","cpop_copying":"0","cpop_copiers":"29","cpop_user_frozen":"false","cpop_watching":"0","cpop_lastTradinghours":"12","cpop_profit_1":"6127.17","cpop_profit_5":"3823.27","cpop_userId":"560453","cpop_profit_4":"1001.7","cpop_nickname":"&JHa","cpop_profit_3":"1365.0","cpop_profit_2":"1548.32","cpop_hitRate":"67","cpop_profit_8":"1718402.4","cpop_profit_7":"9684.9","cpop_profit_6":"97242.21"}}],"existUpdateTypeEnumId":[61,611,612,613],"errorCode":0,"errorMessages":null,"userMessages":null,"apiCode":null,"apiCodeDescription":null};
						for (var i = 0; i < data.feed.length; i++) {
							$rootScope.hotTraders[i] = $rootScope.feedSwitch(data.feed[i]);
						}
					}
				}
			})
			.error(function(data, status, headers, config) {
				handleNetworkError(data);
			});
	}
	
	$scope.changeInterval = function(num) {
		closeElement({id: 'time-range-dropdown-traders'});
		$scope.updateTypeEnumId = num;
		$scope.isInitCall = false;
		$scope.getInfo();
	}
	
	function init() {
		$scope.updateTypeEnumId = 61;
		$scope.isInitCall = true;
		if ($rootScope.hotTraders.length == 0 || $rootScope.hotData.hotTraders.isFakeInfo) {
			$scope.getInfo();
		} else {
			$scope.ready = true;
		}
	}
	
	waitForIt();
}]);

copyOpApp.controller('copyopAssetSpecialistsCtrl', ['$rootScope', '$scope', '$http', function($rootScope, $scope, $http) {
	$scope.feedLimit = 3;
	$scope.ready = false;
	function waitForIt() {
		if (!$rootScope.ready) {
			setTimeout(function(){waitForIt()},50);
		} else {
			init();
		}
	}

	$scope.updateType = [];
	$scope.hours = 0;

	$scope.getInfo = function() {
		var CoyopHotDataMethodResult = jsonConcat(getUserMethodRequest(), {
			updateTypeEnumId: $scope.updateTypeEnumId,
			isInitCall: $scope.isInitCall
		});
		$http.post(settings.jsonLink + 'getCopyopAssetSpecialists', CoyopHotDataMethodResult)//getCopyopAssetSpecialists
			.success(function(data, status, headers, config) {
				data.loginNotCheck = true;
				if (!handleErrors(data, 'getCopyopAssetSpecialists')) {
					$scope.ready = true;
					$rootScope.assetSpecialists = [];
					if (data.feed != null && data.feed.length > 0) {
						$rootScope.hotData.assetSpecialists.isFakeInfo = false;
						if (isUndefined($rootScope.hotData.assetSpecialists) || isUndefined($rootScope.hotData.assetSpecialists.hours)) {
							if (data.feed[0].properties.cpop_lastTradinghours < 100) {
								$scope.hours = data.feed[0].properties.cpop_lastTradinghours;
							}
						} else {
							$scope.hours = $rootScope.hotData.assetSpecialists.hours;
						}
						switch (data.feed[0].msgType) {
							case 64:
								$rootScope.hotData.assetSpecialists.hours = $scope.hours;
								$rootScope.hotData.assetSpecialists.intervalType = 'best-type-short-h';
								$rootScope.hotData.assetSpecialists.intervalTypeNum = $scope.hours;
								break;
							case 641:
								$rootScope.hotData.assetSpecialists.intervalType = 'best-type-short-w';
								$rootScope.hotData.assetSpecialists.intervalTypeNum = 1;
								break;
							case 642:
								$rootScope.hotData.assetSpecialists.intervalType = 'best-type-short-m';
								$rootScope.hotData.assetSpecialists.intervalTypeNum = 1;
								break;
							case 643:
								$rootScope.hotData.assetSpecialists.intervalType = 'best-type-short-m';
								$rootScope.hotData.assetSpecialists.intervalTypeNum = 3;
								break;
						}
						$rootScope.hotData.assetSpecialists.updateType = [];
						for (var i = 0; i < data.existUpdateTypeEnumId.length; i++) {
							var type = 0;
							if (data.existUpdateTypeEnumId[i].toString().length > 2) {
								type = data.existUpdateTypeEnumId[i].toString()[2];
							}
							$rootScope.hotData.assetSpecialists.updateType.push({
								hours: $scope.hours,
								type: 'best-type-' + type,
								fullType: data.existUpdateTypeEnumId[i]
							});
						}
						for (var i = 0; i < data.feed.length; i++) {
							$rootScope.assetSpecialists[i] = $rootScope.feedSwitch(data.feed[i]);
						}
					} else if($rootScope.$state.includes('ln.index')) {
						$rootScope.hotData.assetSpecialists.isFakeInfo = true;
						var data = {"feed":[{"userId":-1,"queueType":6,"timeCreated":"Feb 25, 2015 3:01:47 PM","timeCreatedUUID":"38880c40-bcff-11e4-b681-d508b7730cb7","msgType":61,"properties":{"cpop_avatar":"https://www.copyop.com/jsonService/avatar/f9.png","cpop_watchers":"33","cpop_copying":"0","cpop_copiers":"5","cpop_user_frozen":"false","cpop_watching":"0","cpop_lastTradinghours":"12","cpop_profit_1":"15290.98","cpop_profit_5":"9541.36","cpop_userId":"869851","cpop_profit_4":"2499.84","cpop_nickname":"StepH485","cpop_profit_3":"3406.0","cpop_profit_2":"3864.0","cpop_hitRate":"54","cpop_profit_8":"4288452.58","cpop_profit_7":"24169.67","cpop_profit_6":"242678.08"}},{"userId":-1,"queueType":6,"timeCreated":"Feb 25, 2015 3:01:47 PM","timeCreatedUUID":"3887be20-bcff-11e4-b681-d508b7730cb7","msgType":61,"properties":{"cpop_avatar":"https://www.copyop.com/jsonService/avatar/m57.png","cpop_watchers":"29","cpop_copying":"0","cpop_copiers":"1","cpop_user_frozen":"false","cpop_watching":"0","cpop_lastTradinghours":"12","cpop_profit_1":"6991.47","cpop_profit_5":"4362.0","cpop_userId":"715920","cpop_profit_4":"1143.0","cpop_nickname":"&JuJohn","cpop_profit_3":"1557.55","cpop_profit_2":"1766.73","cpop_hitRate":"55","cpop_profit_8":"1960801.05","cpop_profit_7":"11051.05","cpop_profit_6":"110959.24"}},{"userId":-1,"queueType":6,"timeCreated":"Feb 25, 2015 3:01:47 PM","timeCreatedUUID":"388748f0-bcff-11e4-b681-d508b7730cb7","msgType":61,"properties":{"cpop_avatar":"https://www.copyop.com/jsonService/avatar/m70.png","cpop_watchers":"612","cpop_copying":"0","cpop_copiers":"29","cpop_user_frozen":"false","cpop_watching":"0","cpop_lastTradinghours":"12","cpop_profit_1":"6127.17","cpop_profit_5":"3823.27","cpop_userId":"560453","cpop_profit_4":"1001.7","cpop_nickname":"&JHa","cpop_profit_3":"1365.0","cpop_profit_2":"1548.32","cpop_hitRate":"67","cpop_profit_8":"1718402.4","cpop_profit_7":"9684.9","cpop_profit_6":"97242.21"}}],"existUpdateTypeEnumId":[61,611,612,613],"errorCode":0,"errorMessages":null,"userMessages":null,"apiCode":null,"apiCodeDescription":null};
						for (var i = 0; i < data.feed.length; i++) {
							$rootScope.assetSpecialists[i] = $rootScope.feedSwitch(data.feed[i]);
						}
					}
				}
			})
			.error(function(data, status, headers, config) {
				handleNetworkError(data);
			});
	}
	
	$scope.changeInterval = function(num) {
		closeElement({id: 'time-range-dropdown-specialists'});
		$scope.updateTypeEnumId = num;
		$scope.isInitCall = false;
		$scope.getInfo();
	}
	
	function init() {
		$scope.updateTypeEnumId = 61;
		$scope.isInitCall = true;
		if ($rootScope.assetSpecialists.length == 0 || $rootScope.hotData.assetSpecialists.isFakeInfo) {
			$scope.getInfo();
		} else {
			$scope.ready = true;
		}
	}
	
	waitForIt();
}]);

copyOpApp.controller('copyopHotTradesCtrl', ['$rootScope', '$scope', '$http', '$timeout', '$interval', function($rootScope, $scope, $http, $timeout, $interval) {
	$scope.feedLimit = 3;
	$scope.ready = false;
	$scope.random = false;
	
	function waitForIt() {
		if (!$rootScope.ready) {
			setTimeout(function(){waitForIt()},50);
		} else {
			init();
		}
	}
	
	$scope.getInfo = function() {
		var copyopHotDataMethodRequest = jsonConcat(getUserMethodRequest(), {
			updateTypeEnumId: $scope.updateTypeEnumId,
			isInitCall: $scope.isInitCall
		});
		$scope.requestsStatus.getCopyopHotTrades = 0;
		$http.post(settings.jsonLink + 'getCopyopHotTrades', copyopHotDataMethodRequest)
			.success(function(data, status, headers, config) {
				data.serviceName = 'getCopyopHotTrades';
				data.loginNotCheck = true;
				if (!handleErrors(data, 'getCopyopHotTrades')) {
					$scope.ready = true;
					$scope.requestsStatus.getCopyopHotTrades = 2;
					$rootScope.hotTrades = [];
					if (data.feed != null && data.feed.length > 0) {
						$rootScope.hotData.hotTrades.isFakeInfo = false;
						if (isUndefined($rootScope.hotData.hotTrades) || isUndefined($rootScope.hotData.hotTrades.hours)) {
							if (data.feed[0].properties.cpop_lastTradinghours < 100) {
								$scope.hours = data.feed[0].properties.cpop_lastTradinghours;
							}
						} else {
							$scope.hours = $rootScope.hotData.hotTrades.hours;
						}
						switch (data.feed[0].msgType) {
							case 62:
								$rootScope.hotData.hotTrades.hours = $scope.hours;
								$rootScope.hotData.hotTrades.intervalType = 'best-type-short-h';
								$rootScope.hotData.hotTrades.intervalTypeNum = $scope.hours;
								break;
							case 621:
								$rootScope.hotData.hotTrades.intervalType = 'best-type-short-w';
								$rootScope.hotData.hotTrades.intervalTypeNum = 1;
								break;
							case 622:
								$rootScope.hotData.hotTrades.intervalType = 'best-type-short-m';
								$rootScope.hotData.hotTrades.intervalTypeNum = 1;
								break;
							case 623:
								$rootScope.hotData.hotTrades.intervalType = 'best-type-short-m';
								$rootScope.hotData.hotTrades.intervalTypeNum = 3;
								break;
						} 
						$rootScope.hotData.hotTrades.updateType = [];
						for (var i = 0; i < data.existUpdateTypeEnumId.length; i++) {
							var type = 0;
							if (data.existUpdateTypeEnumId[i].toString().length > 2) {
								type = data.existUpdateTypeEnumId[i].toString()[2];
							}
							$rootScope.hotData.hotTrades.updateType.push({
								hours: $scope.hours,
								type: 'best-type-' + type,
								fullType: data.existUpdateTypeEnumId[i]
							});
						}
						for (var i = 0; i < data.feed.length; i++) {
							$rootScope.hotTrades[i] = $rootScope.feedSwitch(data.feed[i]);
							new Image().src = data.feed[i].properties.cpop_avatar;
						}
					} else if ($rootScope.$state.includes('ln.index')) {
						$rootScope.hotData.hotTrades.isFakeInfo = true;
						var data = {"feed":[{"userId":-1,"queueType":6,"timeCreated":"Feb 25, 2015 3:02:06 PM","timeCreatedUUID":"4357fe00-bcff-11e4-b681-d508b7730cb7","msgType":62,"properties":{"cpop_avatar":"https://www.copyop.com/jsonService/avatar/f9.png","cpop_watchers":"33","cpop_copying":"0","cpop_copiers":"5","cpop_user_frozen":"false","cpop_marketId":"553","cpop_assetCreateDateTime":"25/02/2015 12:06","cpop_watching":"0","cpop_lastTradinghours":"12","cpop_profit_1":"10773.04","cpop_userId":"869851","cpop_profit_5":"6722.23","cpop_nickname":"StepH485","cpop_profit_4":"1761.23","cpop_profit_3":"2400.0","cpop_profit_2":"2722.33","cpop_profit_8":"3021366.86","cpop_profit_7":"17028.39","cpop_profit_6":"170975.31"}},{"userId":-1,"queueType":6,"timeCreated":"Feb 25, 2015 3:02:06 PM","timeCreatedUUID":"435788d0-bcff-11e4-b681-d508b7730cb7","msgType":62,"properties":{"cpop_avatar":"https://www.copyop.com/jsonService/avatar/f9.png","cpop_watchers":"33","cpop_copying":"0","cpop_copiers":"5","cpop_user_frozen":"false","cpop_marketId":"553","cpop_assetCreateDateTime":"25/02/2015 11:02","cpop_watching":"0","cpop_lastTradinghours":"12","cpop_profit_1":"10773.04","cpop_userId":"869851","cpop_profit_5":"6722.23","cpop_nickname":"StepH485","cpop_profit_4":"1761.23","cpop_profit_3":"2400.0","cpop_profit_2":"2722.33","cpop_profit_8":"3021366.86","cpop_profit_7":"17028.39","cpop_profit_6":"170975.31"}},{"userId":-1,"queueType":6,"timeCreated":"Feb 25, 2015 3:02:06 PM","timeCreatedUUID":"435713a0-bcff-11e4-b681-d508b7730cb7","msgType":62,"properties":{"cpop_avatar":"https://www.copyop.com/jsonService/avatar/f9.png","cpop_watchers":"33","cpop_copying":"0","cpop_copiers":"5","cpop_user_frozen":"false","cpop_marketId":"553","cpop_assetCreateDateTime":"25/02/2015 13:13","cpop_watching":"0","cpop_lastTradinghours":"12","cpop_profit_1":"10773.04","cpop_userId":"869851","cpop_profit_5":"6722.23","cpop_nickname":"StepH485","cpop_profit_4":"1761.23","cpop_profit_3":"2400.0","cpop_profit_2":"2722.33","cpop_profit_8":"3021366.86","cpop_profit_7":"17028.39","cpop_profit_6":"170975.31"}}],"existUpdateTypeEnumId":[62,621,622,623],"errorCode":0,"errorMessages":null,"userMessages":null,"apiCode":null,"apiCodeDescription":null};
						
						for (var i = 0; i < data.feed.length; i++) {
							$rootScope.hotTrades[i] = $rootScope.feedSwitch(data.feed[i]);
							new Image().src = data.feed[i].properties.cpop_avatar;
						}
					}
					var randomised = function() {
						for (var i = 0; i < $rootScope.hotTrades.length; i++) {
							$rootScope.hotTrades[i].randomChoosen = false;
						}
						$timeout(function() {
							var randNumsArr = getSetOfRandomNumbers(0, $rootScope.hotTrades.length - 1, 3);
							for (var n = 0; n < randNumsArr.length; n++) {
								$rootScope.hotTrades[randNumsArr[n]].randomChoosen = true;
							}
						}, 600);
					}
					randomised();
					$rootScope.hotTradesTimer = $interval(function() {
						randomised();
					},5000);
				}
			})
			.error(function(data, status, headers, config) {
				$scope.requestsStatus.getCopyopHotTrades = 2;
				handleNetworkError(data);
			});
	}

	$scope.changeInterval = function(num) {
		closeElement({id: 'time-range-dropdown-trades'});
		$scope.updateTypeEnumId = num;
		$scope.isInitCall = false;
		$scope.getInfo();
	}
	
	function init() {
		$scope.updateTypeEnumId = 62;
		$scope.isInitCall = true;
		if (isUndefined($scope.requestsStatus.getCopyopHotTrades) || $rootScope.hotData.hotTrades.isFakeInfo) {
			$scope.getInfo();
		} else {
			$scope.ready = true;
		}
	}
	
	waitForIt();
}]);

copyOpApp.controller('copyopHotCopiersCtrl', ['$rootScope', '$scope', '$http', function($rootScope, $scope, $http) {
	$scope.feedLimit = 3;
	$scope.ready = false;
	function waitForIt() {
		if (!$rootScope.ready) {
			setTimeout(function(){waitForIt()},50);
		} else {
			init();
		}
	}

	$scope.getInfo = function() {
		var copyopHotDataMethodRequest = jsonConcat(getUserMethodRequest(), {
			updateTypeEnumId: $scope.updateTypeEnumId,
			isInitCall: $scope.isInitCall
		});
		$http.post(settings.jsonLink + 'getCopyopHotCopiers', copyopHotDataMethodRequest)
			.success(function(data, status, headers, config) {
				data.loginNotCheck = true;
				if (!handleErrors(data, 'getCopyopHotCopiers')) {
					$scope.ready = true;
					$rootScope.hotCopiers = [];
					if (data.feed != null && data.feed.length > 0) {
						$rootScope.hotData.hotCopiers.isFakeInfo = false;
						if (typeof $rootScope.hotData.hotCopiers == 'undefined' || typeof $rootScope.hotData.hotCopiers.hours == 'undefined') {
							if (data.feed[0].properties.cpop_lastTradinghours < 100) {
								$scope.hours = data.feed[0].properties.cpop_lastTradinghours;
							}
						} else {
							$scope.hours = $rootScope.hotData.hotCopiers.hours;
						}
						switch (data.feed[0].msgType) {
							case 63:
								$rootScope.hotData.hotCopiers.hours = $scope.hours;
								$rootScope.hotData.hotCopiers.intervalType = 'best-type-short-h';
								$rootScope.hotData.hotCopiers.intervalTypeNum = $scope.hours;
								break;
							case 631:
								$rootScope.hotData.hotCopiers.intervalType = 'best-type-short-w';
								$rootScope.hotData.hotCopiers.intervalTypeNum = 1;
								break;
							case 632:
								$rootScope.hotData.hotCopiers.intervalType = 'best-type-short-m';
								$rootScope.hotData.hotCopiers.intervalTypeNum = 1;
								break;
							case 633:
								$rootScope.hotData.hotCopiers.intervalType = 'best-type-short-m';
								$rootScope.hotData.hotCopiers.intervalTypeNum = 3;
								break;
						} 
						
						$rootScope.hotData.hotCopiers.updateType = [];
						for (var i = 0; i < data.existUpdateTypeEnumId.length; i++) {
							var type = 0;
							if (data.existUpdateTypeEnumId[i].toString().length > 2) {
								type = data.existUpdateTypeEnumId[i].toString()[2];
							}
							$rootScope.hotData.hotCopiers.updateType.push({
								hours: $scope.hours,
								type: 'best-type-' + type,
								fullType: data.existUpdateTypeEnumId[i]
							});
						}
						
						for (var i = 0; i < data.feed.length; i++) {
							$rootScope.hotCopiers[i] = $rootScope.feedSwitch(data.feed[i]);
						}
					} else if ($rootScope.$state.includes('ln.index')) {
						$rootScope.hotData.hotCopiers.isFakeInfo = true;
						var data = {"feed":[{"userId":-1,"queueType":6,"timeCreated":"Feb 25, 2015 3:02:23 PM","timeCreatedUUID":"4e059ab0-bcff-11e4-b681-d508b7730cb7","msgType":63,"properties":{"cpop_avatar":"https://www.copyop.com/jsonService/avatar/m38.png","cpop_watchers":"3","cpop_copying":"2","cpop_copiers":"5","cpop_user_frozen":"false","cpop_watching":"3","cpop_lastTradinghours":"24","cpop_profit_1":"2063.91","cpop_profit_5":"1287.85","cpop_userId":"523690","cpop_profit_4":"337.42","cpop_nickname":"Freeze","cpop_profit_3":"460.0","cpop_profit_2":"521.55","cpop_hitRate":"83","cpop_profit_8":"578834.48","cpop_profit_7":"3262.31","cpop_profit_6":"32755.51"}},{"userId":-1,"queueType":6,"timeCreated":"Feb 25, 2015 3:02:23 PM","timeCreatedUUID":"4e03ed00-bcff-11e4-b681-d508b7730cb7","msgType":63,"properties":{"cpop_avatar":"https://www.copyop.com/jsonService/avatar/m57.png","cpop_watchers":"3","cpop_copying":"3","cpop_copiers":"0","cpop_user_frozen":"false","cpop_watching":"1","cpop_lastTradinghours":"24","cpop_profit_1":"1088.26","cpop_profit_5":"679.06","cpop_userId":"1079661","cpop_profit_4":"177.92","cpop_nickname":"memo","cpop_profit_3":"242.45","cpop_profit_2":"275.0","cpop_hitRate":"86","cpop_profit_8":"305208.75","cpop_profit_7":"1720.16","cpop_profit_6":"17271.38"}},{"userId":-1,"queueType":6,"timeCreated":"Feb 25, 2015 3:02:23 PM","timeCreatedUUID":"4e059ab0-bcff-11e4-b681-d508b7730cb7","msgType":63,"properties":{"cpop_avatar":"https://www.copyop.com/jsonService/avatar/m38.png","cpop_watchers":"3","cpop_copying":"2","cpop_copiers":"3","cpop_user_frozen":"false","cpop_watching":"3","cpop_lastTradinghours":"24","cpop_profit_1":"2063.91","cpop_profit_5":"1287.85","cpop_userId":"523690","cpop_profit_4":"337.42","cpop_nickname":"levz","cpop_profit_3":"460.0","cpop_profit_2":"321.55","cpop_hitRate":"89","cpop_profit_8":"578834.48","cpop_profit_7":"3262.31","cpop_profit_6":"32755.51"}}],"existUpdateTypeEnumId":[63,632,633],"errorCode":0,"errorMessages":null,"userMessages":null,"apiCode":null,"apiCodeDescription":null};
						for (var i = 0; i < data.feed.length; i++) {
							$rootScope.hotCopiers[i] = $rootScope.feedSwitch(data.feed[i]);
						}
					}
				}
			})
			.error(function(data, status, headers, config) {
				handleNetworkError(data);
			});
	}
	
	$scope.changeInterval = function(num) {
		closeElement({id: 'time-range-dropdown-copiers'});
		$scope.updateTypeEnumId = num;
		$scope.isInitCall = false;
		$scope.getInfo();
	}
	
	function init() {
		$scope.updateTypeEnumId = 63;
		$scope.isInitCall = true;
		if ($rootScope.hotCopiers.length == 0 || $rootScope.hotData.hotCopiers.isFakeInfo) {
			$scope.getInfo();
		} else {
			$scope.ready = true;
		}
	}
	
	waitForIt();
}]);

copyOpApp.controller('copyopInboxCtrl', ['$rootScope', '$scope', '$http', function($rootScope, $scope, $http) {
	$scope.ready = false;
	function waitForIt() {
		if (!$rootScope.ready) {
			setTimeout(function(){waitForIt()},50);
		} else {
			if (settings.loggedIn) {//if not logged in don't make request
				init();
			}
		}
	}
	waitForIt();

	function init() {
		if ($rootScope.inbox.length == 0) {
			$http.post(settings.jsonLink + 'getCopyopInbox', getUserMethodRequest())
				.success(function(data, status, headers, config) {
					if (!handleErrors(data, 'getCopyopInbox')) {
						$scope.ready = true;
						if (data.feed != null && data.feed.length > 0) {
							for (var i = 0; i < data.feed.length; i++) {
								$rootScope.inbox[i] = $rootScope.feedSwitch(data.feed[i]);
							}
						}
					}
				})
				.error(function(data, status, headers, config) {
					handleNetworkError(data);
				});
		} else {
			$scope.ready = true;
		}
	}
	
	$scope.resetCount = function() {
		$rootScope.unread.inbox = 0;
	}
}]);
