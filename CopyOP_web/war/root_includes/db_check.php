<?php


/// Sending JSON data to Tomcat using Curl //////

$data = array ("service" => "db_check");
$data_string = json_encode($data);
$ch = curl_init('http://localhost:8080/jsonService/AnyoptionService/getSystemChecks');
	curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST"); 
	curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
	    'Content-Type: application/json', 
            'Content-Length: ' . strlen($data_string))

);

/// Return json data from curl //////
$array = curl_exec($ch);


/// Decode json data from curl  
$object = json_decode($array);



// Poiting a Specific value in json
$apiCode = $object->apiCode;

//Sending HTTP CODE 200  if apiCode = dbok 
if($apiCode == "dbok" ){

echo $apiCode;	
http_response_code(200);
}
//Sending HTTP CODE 204 if apiCode = somethingelse 
else {


http_response_code(204);
}


?>
