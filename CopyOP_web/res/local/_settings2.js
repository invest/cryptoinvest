//var server = 'bgtest';
//var server = 'production';
var server = 'local';
//var server = 'integration';

if (server == 'bgtest') {
	settings.jsonLink = 'https://www.bgtestenv.anyoption.com/jsonService/AnyoptionService/';
	settings.commonServiceLink = 'https://www.bgtestenv.anyoption.com/jsonService/CommonService/AnyoptionService/';
	settings.lsServer = 'https://ls.bgtestenv.anyoption.com';
	settings.jsonImagegLink = 'https://www.bgtestenv.anyoption.com/jsonService/';
	settings.optimoveActive = false;
} else if (server == 'production') {
	settings.jsonLink = 'https://www.copyop.com/jsonService/AnyoptionService/';
	settings.commonServiceLink = 'https://www.copyop.com/jsonService/CommonService/AnyoptionService/';
	settings.lsServer = 'https://ls.copyop.com';
	settings.jsonImagegLink = 'http://www.copyop.com/jsonService/';
	settings.optimoveActive = false;
} else if (server == 'integration') {
        settings.jsonLink = 'https://www.integration.copyop.com/jsonService/AnyoptionService/';
        settings.commonServiceLink = 'https://www.integration.copyop.com/jsonService/CommonService/AnyoptionService/';
        settings.lsServer = 'https://ls.integration.copyop.com';
        settings.jsonImagegLink = 'http://www.integration.copyop.com/jsonService/';
        settings.optimoveActive = false;
} else {
	settings.jsonLink = 'http://www.georgid.bg.copyop.com/jsonService/AnyoptionService/';
	settings.commonServiceLink = 'http://www.georgid.bg.copyop.com/jsonService/CommonService/AnyoptionService/';
	settings.lsServer = 'http://ls.bgtestenv.anyoption.com';
	settings.jsonImagegLink = 'http://www.georgid.bg.copyop.com/jsonService/';
	settings.optimoveActive = false;
}


settings.isLive = false;
settings.logIt_type = 4;
settings.base = '/jsonService/#/';