settings.jsonLink = 'https://www.copyop.com/jsonService/AnyoptionService/';
settings.commonServiceLink = 'https://www.copyop.com/jsonService/CommonService/AnyoptionService/';
settings.jsonImagegLink = 'https://www.copyop.com/jsonService/';
settings.isLive = true;
settings.lsServer = 'https://ls.copyop.com';
settings.logIt_type = 3;
settings.base = '/';
settings.optimoveActive = false;