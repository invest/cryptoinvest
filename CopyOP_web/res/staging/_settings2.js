settings.jsonLink = 'https://www.staging.copyop.com/jsonService/AnyoptionService/';
settings.commonServiceLink = 'https://www.staging.copyop.com/jsonService/CommonService/AnyoptionService/';
settings.lsServer = 'https://ls.staging.copyop.com';
settings.jsonImagegLink = 'https://www.staging.copyop.com/jsonService/';
settings.optimoveActive = false;

settings.isLive = false;
settings.logIt_type = 4;
settings.base = '/';
