package com.anyoption.ets.beans;

public class MarketingCampCombinationRequestedObj implements java.io.Serializable {
	

	private static final long serialVersionUID = 1657430793485404851L;

	//1:Campaign 2:Combination
	private long requestedType;
    //1:Insert 2:Update
	private long actionType;
	private long primaryId;
	private long foreignId;
    /**
     * @return the requestedType
     */
    public long getRequestedType() {
        return requestedType;
    }
    /**
     * @param requestedType the requestedType to set
     */
    public void setRequestedType(long requestedType) {
        this.requestedType = requestedType;
    }
    /**
     * @return the actionType
     */
    public long getActionType() {
        return actionType;
    }
    /**
     * @param actionType the actionType to set
     */
    public void setActionType(long actionType) {
        this.actionType = actionType;
    }
    /**
     * @return the primaryId
     */
    public long getPrimaryId() {
        return primaryId;
    }
    /**
     * @param primaryId the primaryId to set
     */
    public void setPrimaryId(long primaryId) {
        this.primaryId = primaryId;
    }
    /**
     * @return the foreignId
     */
    public long getForeignId() {
        return foreignId;
    }
    /**
     * @param foreignId the foreignId to set
     */
    public void setForeignId(long foreignId) {
        this.foreignId = foreignId;
    }

 }


