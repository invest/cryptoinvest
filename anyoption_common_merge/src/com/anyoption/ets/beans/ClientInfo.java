package com.anyoption.ets.beans;




public class ClientInfo implements java.io.Serializable {
	

    private static final long serialVersionUID = 2763659499000496209L;
   
	private String etsCookie;
    private String userAgent;
	private String userResolution;
	private String platform;
    /**
     * @return the etsCookie
     */
    public String getEtsCookie() {
        return etsCookie;
    }
    /**
     * @param etsCookie the etsCookie to set
     */
    public void setEtsCookie(String etsCookie) {
        this.etsCookie = etsCookie;
    }
    /**
     * @return the userAgent
     */
    public String getUserAgent() {
        return userAgent;
    }
    /**
     * @param userAgent the userAgent to set
     */
    public void setUserAgent(String userAgent) {
        this.userAgent = userAgent;
    }
    /**
     * @return the userResolution
     */
    public String getUserResolution() {
        return userResolution;
    }
    /**
     * @param userResolution the userResolution to set
     */
    public void setUserResolution(String userResolution) {
        this.userResolution = userResolution;
    }
    /**
     * @return the platform
     */
    public String getPlatform() {
        return platform;
    }
    /**
     * @param platform the platform to set
     */
    public void setPlatform(String platform) {
        this.platform = platform;
    }
}


