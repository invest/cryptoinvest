package com.anyoption.ets.beans;




public class MarketingTrackingParam extends MarketingTrackingParamBase implements java.io.Serializable {
	

	private static final long serialVersionUID = 1657430793485404851L;

	private long marketingTrackingActivityId;
	private long contactId;
	private long userId;
	private String ip;	
	private boolean isMobile = false;

    /**
     * @return the marketingTrackingActivityId
     */
    public long getMarketingTrackingActivityId() {
        return marketingTrackingActivityId;
    }
    /**
     * @param marketingTrackingActivityId the marketingTrackingActivityId to set
     */
    public void setMarketingTrackingActivityId(long marketingTrackingActivityId) {
        this.marketingTrackingActivityId = marketingTrackingActivityId;
    }
    /**
     * @return the contactId
     */
    public long getContactId() {
        return contactId;
    }
    /**
     * @param contactId the contactId to set
     */
    public void setContactId(long contactId) {
        this.contactId = contactId;
    }
    /**
     * @return the userId
     */
    public long getUserId() {
        return userId;
    }
    /**
     * @param userId the userId to set
     */
    public void setUserId(long userId) {
        this.userId = userId;
    }
    /**
     * @return the ip
     */
    public String getIp() {
        return ip;
    }
    /**
     * @param ip the ip to set
     */
    public void setIp(String ip) {
        this.ip = ip;
    }
    /**
     * @return the isMobile
     */
    public boolean isMobile() {
        return isMobile;
    }
    /**
     * @param isMobile the isMobile to set
     */
    public void setMobile(boolean isMobile) {
        this.isMobile = isMobile;
    }
}


