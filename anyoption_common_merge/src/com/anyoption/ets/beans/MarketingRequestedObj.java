package com.anyoption.ets.beans;




public class MarketingRequestedObj extends MarketingTrackingParamBase implements java.io.Serializable {
	

	private static final long serialVersionUID = 1657430793485404851L;

	private long requestedType;
	private long objectId;

    public long getObjectId() {
        return objectId;
    }
    /**
     * @param objectId the objectId to set
     */
    public void setObjectId(long objectId) {
        this.objectId = objectId;
    }
    /**
     * @return the requestedType
     */
    public long getRequestedType() {
        return requestedType;
    }
    /**
     * @param requestedType the requestedType to set
     */
    public void setRequestedType(long requestedType) {
        this.requestedType = requestedType;
    }
 }


