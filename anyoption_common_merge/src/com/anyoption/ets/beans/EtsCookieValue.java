package com.anyoption.ets.beans;

import java.util.Date;



public class EtsCookieValue implements java.io.Serializable {
	

	private static final long serialVersionUID = 1657430793485404851L;

	private Date createDateTime;
	private long lastActivity;
	private String UId;
   
   
    /**
     * @return the uId
     */
    public String getUId() {
        return UId;
    }
    /**
     * @param uId the uId to set
     */
    public void setUId(String uId) {
        UId = uId;
    }
    /**
     * @return the createDateTime
     */
    public Date getCreateDateTime() {
        return createDateTime;
    }
    /**
     * @param createDateTime the createDateTime to set
     */
    public void setCreateDateTime(Date createDateTime) {
        this.createDateTime = createDateTime;
    }
    /**
     * @return the lastActivity
     */
    public long getLastActivity() {
        return lastActivity;
    }
    /**
     * @param lastActivity the lastActivity to set
     */
    public void setLastActivity(long lastActivity) {
        this.lastActivity = lastActivity;
    }
 }


