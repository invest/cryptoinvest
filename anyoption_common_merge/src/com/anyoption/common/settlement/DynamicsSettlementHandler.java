package com.anyoption.common.settlement;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.Connection;
import java.sql.SQLException;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import com.anyoption.common.beans.Investment;
import com.anyoption.common.beans.User;

/**
 * @author kirilim
 */
public class DynamicsSettlementHandler extends SettlementHandler implements SettlementHandlerIfc {

	private static final Logger log = Logger.getLogger(DynamicsSettlementHandler.class);
	
	@Override
	public SettlementHandlerResult calculate(Connection con, Investment investment, long investmentFeeAmount, long sellPrice, User user) {
		SettlementHandlerResult rslt = new SettlementHandlerResult();
		boolean callWin = false;
		boolean putWin = false;
		boolean win = false;
		if (sellPrice <= 0) {
			
			callWin = investment.getTypeId() == Investment.INVESTMENT_TYPE_DYNAMICS_BUY && investment.getClosingLevel() > investment.getOppCurrentLevel();
			putWin = investment.getTypeId() == Investment.INVESTMENT_TYPE_DYNAMICS_SELL && investment.getClosingLevel() < investment.getOppCurrentLevel();
			
			win = callWin || putWin;
			if (log.isEnabledFor(Level.INFO)) {
				log.log(Level.INFO,
						"investment.getTypeId() = "+ investment.getTypeId() + "; investment.getClosingLevel() = "
									+ investment.getClosingLevel() + "; investment.getCurrentLevel() = " + investment.getCurrentLevel()
									+ "; opp.current_level = " + investment.getOppCurrentLevel());
				log.log(Level.INFO, "putWin = " + putWin + "; callWin = " + callWin + "; win= " + win);
			}
		} else {
			if (sellPrice > investment.getAmount() - investment.getOptionPlusFee()) {
				win = true;
			}
		}

		// investment.getOptionPlusFee is also 0100 fee
		long invesAmount = investment.getAmount() - investment.getOptionPlusFee(); // TODO investment.getOptionPlusFee() should be added as
		// param
		log.debug("settlemnt inv id: "+ investment.getId() + " getBonusOddsChangeTypeId " + investment.getBonusOddsChangeTypeId() + " win " + win);
		long result;
		long netResult;
		if (sellPrice <= 0) {
			if (win) {
				result = (new BigDecimal(10000).multiply(getDynamicsNumberOfContracts(investment))).longValue();
				netResult = Math.abs(result - invesAmount); // abs is for the rare case when price > 100 
			} else {
				result = 0;
				netResult = -invesAmount;
			}
		} else {
				result = sellPrice;
				netResult = Math.round(sellPrice - invesAmount);
		}
		
		long premiaC = investment.getOptionPlusFee();
		long winLoseSubtrahend = investment.getOptionPlusFee();
		long bonusAmount = handleBonusOnSettlement(con, investment, win);
		calculateCommon(con, investment, premiaC, winLoseSubtrahend, win, false, netResult, result, user, rslt, bonusAmount);

		rslt.setResult(result);
		rslt.setNetResult(netResult);
		rslt.setWin(win);
		return rslt;
	}

	@Override
	public void handleBonusManagementSystemActions(	Connection con, Investment investment, boolean win, boolean isNextInvestOnUs,
													long bonusAmount, long netResult, long sellPrice, long writerId,
													long loginId) throws SQLException {
		// coefficient - multiplicative factor for the formula
				double coefficient;
				if (sellPrice <= 0) { // settle end time: 0-100.
					coefficient = 0;
					if (win) {
						coefficient = 10000 * getDynamicsNumberOfContracts(investment).doubleValue(); // 100 for wining * number of
						// contracts * 100 (in cents)
					}
					log.debug("BMS, settle end time Dynamics. coefficient:" + coefficient);
				} else { // settle before time. not golden
					coefficient = sellPrice;
					log.debug("BMS, settle before time. not golden, coefficient:" + coefficient);
				}
				handleCommonBonusManagementSystemActions(	con, investment, win, isNextInvestOnUs, bonusAmount, netResult, writerId, loginId,
															coefficient);
	}

	@Override
	public void handleBalanceLogCmd(int balanceLogCmd, Investment investment, long result, long GMInsuranceAmount,
									User user) throws SQLException {
		log.warn("Not implemented");
	}
	
	public static BigDecimal getDynamicsNumberOfContracts(Investment i) {
		BigDecimal amount = new BigDecimal(i.getAmount()-i.getOptionPlusFee());
		BigDecimal price = new BigDecimal(i.getPrice()).multiply(new BigDecimal(100));
		if(i.getTypeId() == Investment.INVESTMENT_TYPE_DYNAMICS_BUY) {
			return amount.divide(price, 4, RoundingMode.HALF_UP); // 7, buy, above = bid
		} else {
			return amount.divide(new BigDecimal(10000).subtract(price), 4, RoundingMode.HALF_UP); // 8, sell, bellow = (100 - offer)
		}
	}

}