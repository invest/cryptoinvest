package com.anyoption.common.settlement;

import java.sql.Connection;
import java.sql.SQLException;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import com.anyoption.common.beans.Investment;
import com.anyoption.common.beans.User;
import com.anyoption.common.managers.InvestmentsManagerBase;

/**
 * @author kirilim
 */
public class OneTouchSettlementHandler extends SettlementHandler implements SettlementHandlerIfc {

	private static final Logger log = Logger.getLogger(OneTouchSettlementHandler.class);

	@Override
	public SettlementHandlerResult calculate(Connection con, Investment investment, long investmentFeeAmount, long sellPrice, User user) {
		SettlementHandlerResult rslt = new SettlementHandlerResult();
		boolean win = false;
		long upDown;
		try {
			upDown = InvestmentsManagerBase.getOneTouchInvestmentUpDown(con, investment.getId());
		} catch (SQLException e) {
			log.error("Cannot load one touch investment with id = " + investment.getId());
			return null;
		}
    	if (upDown == Investment.INVESTMENT_ONE_TOUCH_UP) {
    		win = investment.getClosingLevel() >= investment.getCurrentLevel();
    	} else if (upDown == Investment.INVESTMENT_ONE_TOUCH_DOWN) {
    		win = investment.getClosingLevel() <= investment.getCurrentLevel();
    	} else if (upDown == -1) {
    		if (log.isEnabledFor(Level.INFO)) {
            	log.log(Level.ERROR, "The one touch opportunity " + investment.getOpportunityId() + " related to this investment " + investment.getId() + " is invalid");
            }
    		return null;
    	}
    	if (log.isEnabledFor(Level.INFO)) {
			log.log(Level.INFO,
					"investment.getTypeId() = "+ investment.getTypeId() + "; investment.getClosingLevel() = "
								+ investment.getClosingLevel() + "; investment.getCurrentLevel() = " + investment.getCurrentLevel()
								+ "; investment.isUpInvestment() = " + InvestmentsManagerBase.isUpInvestment(investment)
								+ "; opp.current_level = " + investment.getOppCurrentLevel());
			log.log(Level.INFO, "isOneTouch = true; win= " + win);
        }

    	long invesAmount = investment.getAmount();
        log.debug("settlemnt inv id: " + investment.getId() + " getBonusOddsChangeTypeId " + investment.getBonusOddsChangeTypeId() + " win " + win);
        long result;
        long netResult;
        if (win) {
        	result = Math.round(invesAmount * (1 + investment.getOddsWin()));
            netResult = Math.round(invesAmount * investment.getOddsWin());
        } else {
        	result = Math.round(invesAmount * (1 - investment.getOddsLose()));
            netResult = Math.round(-invesAmount * investment.getOddsLose());
        }
        long bonusAmount = handleBonusOnSettlement(con, investment, win);        
        calculateCommon(con, investment, 0l, 0l, win, false, netResult, result, user, rslt, bonusAmount);

        rslt.setResult(result);
        rslt.setNetResult(netResult);
        rslt.setWin(win);
    	return rslt;
	}

	@Override
	public void handleBonusManagementSystemActions(	Connection con, Investment investment, boolean win, boolean isNextInvestOnUs,
													long bonusAmount, long netResult, long sellPrice, long writerId,
													long loginId) throws SQLException {
		// coefficient - multiplicative factor for the formula
		double coefficient = investment.getAmount() + investment.getWin() - investment.getLose();
		handleCommonBonusManagementSystemActions(	con, investment, win, isNextInvestOnUs, bonusAmount, netResult, writerId, loginId,
													coefficient);
	}

	@Override
	public void handleBalanceLogCmd(int balanceLogCmd, Investment investment, long result, long GMInsuranceAmount,
									User user) throws SQLException {
		return;
	}
}