package com.anyoption.common.settlement;

/**
 * @author kirilim
 */
public class SettlementHandlerResult {

	private long result;
	private long netResult;
	private long tax;
	private long winLose;
	private boolean win;
	private boolean voidBet;
	private boolean isNextInvestOnUs;
	private long bonusAmount;

	public long getResult() {
		return result;
	}

	public void setResult(long result) {
		this.result = result;
	}

	public long getTax() {
		return tax;
	}

	public void setTax(long tax) {
		this.tax = tax;
	}

	public long getWinLose() {
		return winLose;
	}

	public void setWinLose(long winLose) {
		this.winLose = winLose;
	}

	public boolean isWin() {
		return win;
	}

	public void setWin(boolean win) {
		this.win = win;
	}

	public boolean isVoidBet() {
		return voidBet;
	}

	public void setVoidBet(boolean voidBet) {
		this.voidBet = voidBet;
	}

	public boolean isNextInvestOnUs() {
		return isNextInvestOnUs;
	}

	public void setNextInvestOnUs(boolean isNextInvestOnUs) {
		this.isNextInvestOnUs = isNextInvestOnUs;
	}

	public long getBonusAmount() {
		return bonusAmount;
	}

	public void setBonusAmount(long bonusAmount) {
		this.bonusAmount = bonusAmount;
	}

	public long getNetResult() {
		return netResult;
	}

	public void setNetResult(long netResult) {
		this.netResult = netResult;
	}

	public String toString() {
		String ls = System.getProperty("line.separator");
		return ls + "SettlementHandlerResult: " + ls
		    	+ super.toString() + ls
		        + "result: " + result + ls
		        + "netResult: " + netResult + ls
		        + "tax: " + tax + ls
		        + "winLose: " + winLose + ls
		        + "win: " + win + ls
		        + "voidBet: " + voidBet + ls
		        + "isNextInvestOnUs: " + isNextInvestOnUs + ls
		        + "bonusAmount: " + bonusAmount + ls;
	}
}