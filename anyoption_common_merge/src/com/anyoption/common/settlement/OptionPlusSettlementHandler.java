package com.anyoption.common.settlement;

import java.sql.Connection;
import java.sql.SQLException;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import com.anyoption.common.beans.Investment;
import com.anyoption.common.beans.User;

/**
 * @author kirilim
 */
public class OptionPlusSettlementHandler extends SettlementHandler implements SettlementHandlerIfc {

	private static final Logger log = Logger.getLogger(OptionPlusSettlementHandler.class);

	@Override
	public SettlementHandlerResult calculate(Connection con, Investment investment, long investmentFeeAmount, long sellPrice, User user) {
		SettlementHandlerResult rslt = new SettlementHandlerResult();
		boolean win = false;
		if (sellPrice <= 0) {
			boolean callWin = investment.getTypeId() == Investment.INVESTMENT_TYPE_CALL
								&& investment.getClosingLevel() > investment.getCurrentLevel();
			boolean putWin = investment.getTypeId() == Investment.INVESTMENT_TYPE_PUT
								&& investment.getClosingLevel() < investment.getCurrentLevel();
			win = callWin || putWin;
			boolean voidBet = false;
			if (investment.getClosingLevel() == investment.getCurrentLevel()) {
				voidBet = true;
			}
			if (log.isEnabledFor(Level.INFO)) {
				log.info("investment.getTypeId() = "+ investment.getTypeId() + "; investment.getClosingLevel() = "
							+ investment.getClosingLevel() + "; investment.getCurrentLevel() = " + investment.getCurrentLevel()
							+ "; opp.current_level = " + investment.getOppCurrentLevel());
				log.info("putWin = " + putWin + "; callWin = " + callWin + "; win= " + win + "; voidBet= " + voidBet);
			}
		} else {
			if (sellPrice > investment.getAmount() - investment.getOptionPlusFee()) {
				win = true;
			}
		}

		long invesAmount = investment.getAmount() - investment.getOptionPlusFee(); // TODO investment.getOptionPlusFee() should be added as
																					 // param
		log.debug("settlemnt inv id: "+ investment.getId() + " getBonusOddsChangeTypeId " + investment.getBonusOddsChangeTypeId() + " win "
					+ win);
		long result;
		long netResult;
		if (sellPrice <= 0) {
			if (win) {
				result = Math.round(invesAmount * (1 + investment.getOddsWin()));
				netResult = Math.round(invesAmount * investment.getOddsWin());
			} else {
				result = Math.round(invesAmount * (1 - investment.getOddsLose()));
				netResult = Math.round(-invesAmount * investment.getOddsLose());
			}
		} else {
			if (win) {
				result = sellPrice;
				netResult = Math.round(sellPrice - invesAmount);
			} else {
				result = sellPrice;
				netResult = Math.round(-1 * (invesAmount - sellPrice));
			}
		}
		long premiaC = investment.getOptionPlusFee();
		long winLoseSubtrahend = investment.getOptionPlusFee();
		long bonusAmount = handleBonusOnSettlement(con, investment, win);
		calculateCommon(con, investment, premiaC, winLoseSubtrahend, win, false, netResult, result, user, rslt, bonusAmount);

		rslt.setResult(result);
		rslt.setNetResult(netResult);
		rslt.setWin(win);
		return rslt;
	}

	@Override
	public void handleBonusManagementSystemActions(	Connection con, Investment investment, boolean win, boolean isNextInvestOnUs,
													long bonusAmount, long netResult, long sellPrice, long writerId,
													long loginId) throws SQLException {
		// coefficient - multiplicative factor for the formula
		double coefficient = sellPrice;
		handleCommonBonusManagementSystemActions(	con, investment, win, isNextInvestOnUs, bonusAmount, netResult, writerId, loginId,
													coefficient);
	}

	@Override
	public void handleBalanceLogCmd(int balanceLogCmd, Investment investment, long result, long GMInsuranceAmount,
									User user) throws SQLException {
		return;
	}
}