package com.anyoption.common.settlement;

import java.sql.Connection;
import java.sql.SQLException;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import com.anyoption.common.beans.Investment;
import com.anyoption.common.beans.Opportunity;
import com.anyoption.common.beans.User;
import com.anyoption.common.managers.InvestmentsManagerBase;

/**
 * @author kirilim
 */
public class Binary0100SettlementHandler extends SettlementHandler implements SettlementHandlerIfc {

	private static final Logger log = Logger.getLogger(Binary0100SettlementHandler.class);

	@Override
	public SettlementHandlerResult calculate(Connection con, Investment investment, long investmentFeeAmount, long sellPrice, User user) {
		SettlementHandlerResult rslt = new SettlementHandlerResult();
		boolean callWin = false;
		boolean putWin = false;
		boolean win = false;
		if (sellPrice <= 0) {
			if (investment.getOpportunityTypeId() == Opportunity.TYPE_BINARY_0_100_ABOVE) {
				
				callWin = investment.getTypeId() == Investment.INVESTMENT_TYPE_BUY && investment.getClosingLevel() > investment.getOppCurrentLevel();
				putWin = investment.getTypeId() == Investment.INVESTMENT_TYPE_SELL && investment.getClosingLevel() <= investment.getOppCurrentLevel();
			
			} else if (investment.getOpportunityTypeId() == Opportunity.TYPE_BINARY_0_100_BELOW) {
				
				putWin = investment.getTypeId() == Investment.INVESTMENT_TYPE_SELL && investment.getClosingLevel() >= investment.getOppCurrentLevel();
				callWin = investment.getTypeId() == Investment.INVESTMENT_TYPE_BUY && investment.getClosingLevel() < investment.getOppCurrentLevel();
			
			}
			win = callWin || putWin;
			if (log.isEnabledFor(Level.INFO)) {
				log.log(Level.INFO,
						"investment.getTypeId() = "+ investment.getTypeId() + "; investment.getClosingLevel() = "
									+ investment.getClosingLevel() + "; investment.getCurrentLevel() = " + investment.getCurrentLevel()
									+ "; opp.current_level = " + investment.getOppCurrentLevel());
				log.log(Level.INFO, "putWin = " + putWin + "; callWin = " + callWin + "; win= " + win);
			}
		} else {
			if (sellPrice > investment.getAmount() - investment.getOptionPlusFee()) {
				win = true;
			}
		}

		// investment.getOptionPlusFee is also 0100 fee
		long invesAmount = investment.getAmount() - investment.getOptionPlusFee(); // TODO investment.getOptionPlusFee() should be added as
		// param
		log.debug("settlemnt inv id: "+ investment.getId() + " getBonusOddsChangeTypeId " + investment.getBonusOddsChangeTypeId() + " win " + win);
		long result;
		long netResult;
		if (sellPrice <= 0) {
			if (win) {
				result = 10000 * InvestmentsManagerBase.getBinary0100NumberOfContracts(investment);
				netResult = (10000 - invesAmount / InvestmentsManagerBase.getBinary0100NumberOfContracts(investment)) * InvestmentsManagerBase.getBinary0100NumberOfContracts(investment);
			} else {
				result = 0;
				netResult = -invesAmount;
			}
		} else {
				result = sellPrice;
				netResult = Math.round(sellPrice - invesAmount);
		}
		long premiaC = investment.getOptionPlusFee();
		long winLoseSubtrahend = investment.getOptionPlusFee();
		long bonusAmount = handleBonusOnSettlement(con, investment, win);
		calculateCommon(con, investment, premiaC, winLoseSubtrahend, win, false, netResult, result, user, rslt, bonusAmount);

		rslt.setResult(result);
		rslt.setNetResult(netResult);
		rslt.setWin(win);
		return rslt;
	}

	@Override
	public void handleBonusManagementSystemActions(	Connection con, Investment investment, boolean win, boolean isNextInvestOnUs,
													long bonusAmount, long netResult, long sellPrice, long writerId,
													long loginId) throws SQLException {
		// coefficient - multiplicative factor for the formula
		double coefficient;
		if (sellPrice <= 0) { // settle end time: 0-100.
			coefficient = 0;
			if (win) {
				coefficient = 10000 * InvestmentsManagerBase.getBinary0100NumberOfContracts(investment); // 100 for wining * number of
				// contracts * 100 (in cents)
			}
			log.debug("BMS, settle end time 0-100. coefficient:" + coefficient);
		} else { // settle before time. not golden
			coefficient = sellPrice;
			log.debug("BMS, settle before time. not golden, coefficient:" + coefficient);
		}
		handleCommonBonusManagementSystemActions(	con, investment, win, isNextInvestOnUs, bonusAmount, netResult, writerId, loginId,
													coefficient);
	}

	@Override
	public void handleBalanceLogCmd(int balanceLogCmd, Investment investment, long result, long GMInsuranceAmount,
									User user) throws SQLException {
		return;
	}
}