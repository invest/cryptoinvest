package com.anyoption.common.settlement;

import java.sql.Connection;
import java.sql.SQLException;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import com.anyoption.common.beans.Investment;
import com.anyoption.common.beans.Notification;
import com.anyoption.common.beans.User;
import com.anyoption.common.managers.NotificationManagerBase;
import com.anyoption.common.util.ConstantsBase;
//import com.copyop.common.jms.CopyOpAsyncEventSender;
//import com.copyop.common.jms.events.InvSettleEvent;

/**
 * @author kirilim
 */
public class BinarySettlementHandler extends SettlementHandler implements SettlementHandlerIfc {

	private static final Logger log = Logger.getLogger(BinarySettlementHandler.class);

	@Override
	public SettlementHandlerResult calculate(Connection con, Investment investment, long investmentFeeAmount, long sellPrice, User user) {
		SettlementHandlerResult rslt = new SettlementHandlerResult();
		boolean win;
		boolean voidBet = false;
		if (investmentFeeAmount == 0) {
			boolean callWin = investment.getTypeId() == Investment.INVESTMENT_TYPE_CALL
								&& investment.getClosingLevel() > investment.getCurrentLevel();
			boolean putWin = investment.getTypeId() == Investment.INVESTMENT_TYPE_PUT
								&& investment.getClosingLevel() < investment.getCurrentLevel();
			win = callWin || putWin;
			if (investment.getClosingLevel() == investment.getCurrentLevel()) {
				voidBet = true;
			}
			if (log.isEnabledFor(Level.INFO)) {
				log.info("investment.getTypeId() = "+ investment.getTypeId() + "; investment.getClosingLevel() = "
							+ investment.getClosingLevel() + "; investment.getCurrentLevel() = " + investment.getCurrentLevel()
							+ "; opp.current_level = " + investment.getOppCurrentLevel());
				log.info("putWin = " + putWin + "; callWin = " + callWin + "; win= " + win + "; voidBet= " + voidBet);
			}
		} else { // golden minutes settlement(i.e. take profit)
			win = true;
		}

		long invesAmount = investment.getAmount() - investment.getInsuranceAmountRU();
		log.debug("settlemnt inv id: "+ investment.getId() + " getBonusOddsChangeTypeId " + investment.getBonusOddsChangeTypeId() + " win "
					+ win);
		long result;
		long netResult;
		if (win) {
			result = Math.round(invesAmount * (1 + investment.getOddsWin()));
			netResult = Math.round(invesAmount * investment.getOddsWin());
		} else {
			result = Math.round(invesAmount * (1 - investment.getOddsLose()));
			netResult = Math.round(-invesAmount * investment.getOddsLose());
		}
		long premiaC = investment.getInsuranceAmountRU() + investmentFeeAmount;
		long winLoseSubtrahend = investment.getInsuranceAmountRU() + investmentFeeAmount;
		long bonusAmount = handleBonusOnSettlement(con, investment, win);
		calculateCommon(con, investment, premiaC, winLoseSubtrahend, win, voidBet, netResult, result, user, rslt, bonusAmount);

		rslt.setResult(result);
		rslt.setNetResult(netResult);
		rslt.setWin(win);
		rslt.setVoidBet(voidBet);
		return rslt;
	}

	@Override
	public void handleBonusManagementSystemActions(	Connection con, Investment investment, boolean win, boolean isNextInvestOnUs,
													long bonusAmount, long netResult, long sellPrice, long writerId,
													long loginId) throws SQLException {
		// coefficient - multiplicative factor for the formula
		double coefficient = investment.getAmount() + investment.getWin() - investment.getLose();
		handleCommonBonusManagementSystemActions(	con, investment, win, isNextInvestOnUs, bonusAmount, netResult, writerId, loginId,
													coefficient);
	}

	@Override
	public void handleBalanceLogCmd(int balanceLogCmd, Investment investment, long result, long GMInsuranceAmount,
									User u) throws SQLException {
		// is not manual resettlement
//		if (balanceLogCmd != ConstantsBase.LOG_BALANCE_RESETTLE_INVESTMENT) {
//			if ((CommonUtil.getProperty("copyop.isEnabled").equalsIgnoreCase("true"))) {
//				log.debug("About to send to copyop: " + investment.getId());
//				// TODO is like hourly?
//				if (investment.isLikeHourly()) {
//					if (u.getSkinId() != Skin.SKIN_ETRADER) {
//						CopyOpAsyncEventSender.sendEvent(new InvSettleEvent(investment.getUserId(), investment.getId(), result,
//																			CopyOpInvTypeEnum.of(investment.getCopyOpTypeId()),
//																			investment.getMarketId(), investment.getWin(),
//																			investment.getLose(), investment.getClosingLevel(), new Date(),
//																			investment.getCopyOpInvId(), u.getCurrencyId(),
//																			investment.getRate()));
//						log.debug("Send to copyop: " + investment.getId());
//					}
//				}
//			}
//		}
		try {
			if (investment.getApiExternalUserId() > 0 && GMInsuranceAmount == 0) {
				Notification notification = new Notification();
				notification.setNotification("userId: "+ investment.getUserId() + ", amount: " + result + ", timeSettled: "
												+ investment.getTimeEstClosing().getTime());
				notification.setReference(String.valueOf(investment.getId()));
				notification.setStatus(Notification.STATUS_QUEUED);
				long notificationType = Notification.TYPE_INSERT_INVESTMENT;
				if (balanceLogCmd == ConstantsBase.LOG_BALANCE_RESETTLE_INVESTMENT) {
					notificationType = Notification.TYPE_INSERT_INVESTMENT;
				}
				notification.setType(notificationType);
				notification.setExternalUserId(investment.getApiExternalUserId());
				NotificationManagerBase.insertNotifications(notification);
			}
		} catch (RuntimeException e) {
			log.error("cant insert notification for external user id " + investment.getApiExternalUserId());
		}
	}
}