package com.anyoption.common.bl_vos;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;

public class PromotionBannerSlider implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	private long id;
	private boolean isActive;
	private int position;
	private Date timeCreated;
	private Date timeUpdated;
	private Date startDate;
	private Date endDate;
	private long bannerId;
	private long writerId;
	private long linkTypeId;
	private String defaultImageName;
	private ArrayList<PromotionBannerSliderImage> bannerSliderImages;
	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public boolean isActive() {
		return isActive;
	}

	public void setActive(boolean isActive) {
		this.isActive = isActive;
	}

	public int getPosition() {
		return position;
	}

	public void setPosition(int position) {
		this.position = position;
	}

	public Date getTimeCreated() {
		return timeCreated;
	}

	public void setTimeCreated(Date timeCreated) {
		this.timeCreated = timeCreated;
	}

	public Date getTimeUpdated() {
		return timeUpdated;
	}

	public void setTimeUpdated(Date timeUpdated) {
		this.timeUpdated = timeUpdated;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public long getBannerId() {
		return bannerId;
	}

	public void setBannerId(long bannerId) {
		this.bannerId = bannerId;
	}

	public long getWriterId() {
		return writerId;
	}

	public void setWriterId(long writerId) {
		this.writerId = writerId;
	}

	public long getLinkTypeId() {
		return linkTypeId;
	}

	public void setLinkTypeId(long linkTypeId) {
		this.linkTypeId = linkTypeId;
	}
	
    public String getDefaultImageName() {
		return defaultImageName;
	}

	public void setDefaultImageName(String defaultImageUrl) {
		this.defaultImageName = defaultImageUrl;
	}

	public ArrayList<PromotionBannerSliderImage> getBannerSliderImages() {
		return bannerSliderImages;
	}

	public void setBannerSliderImages(ArrayList<PromotionBannerSliderImage> bannerSliderImages) {
		this.bannerSliderImages = bannerSliderImages;
	}

	public String toString() {
        String ls = System.getProperty("line.separator");
        return ls + "PromotionBannerSlider: "
            + super.toString()
            + "id: " + id + ls
            + "isActive:" + isActive + ls
            + "position: " + position + ls
            + "timeCreated: " + timeCreated + ls
            + "timeUpdated: " + timeUpdated + ls
            + "startDate: " + startDate + ls
            + "endDate: " + endDate + ls
            + "bannerId: " + bannerId + ls
            + "writerId: " + writerId + ls
            + "linkTypeId: " + linkTypeId + ls;
    }
	
	

}
