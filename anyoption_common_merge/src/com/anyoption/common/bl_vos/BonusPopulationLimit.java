package com.anyoption.common.bl_vos;


/**
 * Bonus population limit
 *
 * @author Kobi
 *
 */
public class BonusPopulationLimit {
	private long id;
	private long bonusId;
	private long popTypeId;
	private long currencyId;
	private long minDepositParam;
	private long maxDepositParam;
	private long minResult;
	private long multiplication;
	private long level;
	private long stepRangeMul;
	private long stepLevelMul;
	private double stepBonusAddition;
	private double minBonusPercent;
	private int limitUpdateType;
	private long bonusAmount;
	private long minInvestmentAmount;
	private long maxInvestmentAmount;

	/**
	 * @return the limitUpdateType
	 */
	public int getLimitUpdateType() {
		return limitUpdateType;
	}

	/**
	 * @param limitUpdateType the limitUpdateType to set
	 */
	public void setLimitUpdateType(int limitUpdateType) {
		this.limitUpdateType = limitUpdateType;
	}

	public long getBonusId() {
		return bonusId;
	}

	public void setBonusId(long bonusId) {
		this.bonusId = bonusId;
	}

	public long getCurrencyId() {
		return currencyId;
	}

	public void setCurrencyId(long currencyId) {
		this.currencyId = currencyId;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public long getMaxDepositParam() {
		return maxDepositParam;
	}

	public void setMaxDepositParam(long maxDepositParam) {
		this.maxDepositParam = maxDepositParam;
	}

	public long getMinDepositParam() {
		return minDepositParam;
	}

	public void setMinDepositParam(long minDepositParam) {
		this.minDepositParam = minDepositParam;
	}

	public long getMinResult() {
		return minResult;
	}

	public void setMinResult(long minResult) {
		this.minResult = minResult;
	}

	public long getMultiplication() {
		return multiplication;
	}

	public void setMultiplication(long multiplication) {
		this.multiplication = multiplication;
	}

	public long getPopTypeId() {
		return popTypeId;
	}

	public void setPopTypeId(long popTypeId) {
		this.popTypeId = popTypeId;
	}

	public long getLevel() {
		return level;
	}

	public void setLevel(long level) {
		this.level = level;
	}

	public long getStepLevelMul() {
		return stepLevelMul;
	}

	public void setStepLevelMul(long steplevelMul) {
		this.stepLevelMul = steplevelMul;
	}

	public long getStepRangeMul() {
		return stepRangeMul;
	}

	public void setStepRangeMul(long stepRangeMul) {
		this.stepRangeMul = stepRangeMul;
	}

	public double getMinBonusPercent() {
		return minBonusPercent;
	}

	public void setMinBonusPercent(double minBonusPercent) {
		this.minBonusPercent = minBonusPercent;
	}

	public double getStepBonusAddition() {
		return stepBonusAddition;
	}

	public void setStepBonusAddition(double stepBonusAddition) {
		this.stepBonusAddition = stepBonusAddition;
	}

	/**
	 * @return the bonusAmount
	 */
	public long getBonusAmount() {
		return bonusAmount;
	}

	/**
	 * @param bonusAmount the bonusAmount to set
	 */
	public void setBonusAmount(long bonusAmount) {
		this.bonusAmount = bonusAmount;
	}

	/**
	 * @return the maxInvestmentAmount
	 */
	public long getMaxInvestmentAmount() {
		return maxInvestmentAmount;
	}

	/**
	 * @param maxInvestmentAmount the maxInvestmentAmount to set
	 */
	public void setMaxInvestmentAmount(long maxInvestmentAmount) {
		this.maxInvestmentAmount = maxInvestmentAmount;
	}

	/**
	 * @return the minInvestmentAmount
	 */
	public long getMinInvestmentAmount() {
		return minInvestmentAmount;
	}

	/**
	 * @param minInvestmentAmount the minInvestmentAmount to set
	 */
	public void setMinInvestmentAmount(long minInvestmentAmount) {
		this.minInvestmentAmount = minInvestmentAmount;
	}

}
