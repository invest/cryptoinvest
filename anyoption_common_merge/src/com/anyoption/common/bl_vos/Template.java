package com.anyoption.common.bl_vos;


public class Template implements java.io.Serializable{
	private static final long serialVersionUID = 8043477347321184097L;

	// Mail templates
	public static int WELCOME_MAIL_ID = 1;
	public static int WELCOME_MAIL_CAL_GAME_ID = 20;
	public static int WRONG_NUMBER_MAIL_CONTACT = 23;
	public static int CC_RECEIPT_MAIL_ID = 24;
	public static int CC_WITHDRAWAL_SUCCESS_MAIL_ID = 25;
	public static int WIRE_WITHDRAWAL_SUCCESS_MAIL_ID = 26;
	public static int CANCEL_WITHDRAWAL_EMAIL_MAIL_ID = 27;
	public static int BONUS_EXP_MAIL_ID = 28;
	public static int NOT_REACHED_MAIL_ID = 29;
	public static int CALLME_NO_CALL_MAIL_ID = 30;
	public static int SUCCESS_MAIL_ID = 31;
	public static int MAIL_VALIDATION = 35;
	public static int MONETA_WITHDRAWAL_SUCCESS_MAIL_ID = 36;
	public static int WEBMONEY_WITHDRAWAL_SUCCESS_MAIL_ID = 37;
	public static int NOT_REACHED_MADE_DEPOSIT = 44;
	public static int NOT_REACHED_NEVER_DEPOSIT = 45;
	public static int WRONG_NUMBER_MAIL = 46;
	public static int WELCOME_MAIL_GOOGLE_ID = 55;
	public static int BAROPAY_WITHDRAWAL_SUCCEED_ID = 60;
	public static int CLOSE_ARAB_SKIN_ID = 61;
	public static int BONUS_CLEANUP_MAIL_ID = 63;
	public static final int CAP_INTERNAL_MAIL_D3 = 78;
	public static final int CAP_INTERNAL_MAIL_D6 = 79;
	public static final int CAP_INTERNAL_MAIL_D10 = 80;
	public static final int CAP_INTERNAL_MAIL_D13 = 81;
	public static final int CAP_INTERNAL_MAIL_D14 = 82;
	
	protected long id;
	protected String name;
	protected String subject;
	protected String fileName;
	protected boolean isDisplayed;
	protected long mailBoxEmailType;

	public Template() {
		id=0;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}


	/**
	 * Constructs a <code>String</code> with all attributes
	 * in name = value format.
	 *
	 * @return a <code>String</code> representation
	 * of this object.
	 */
	public String toString()
	{
	    final String TAB = " \n ";

	    String retValue = "";

	    retValue = "Template ( "
	        + super.toString() + TAB
	        + "id = " + this.id + TAB
	        + "name = " + this.name + TAB
	        + "subject = " + this.subject + TAB
	        + "fileName = " + this.fileName + TAB
	        + " )";

	    return retValue;
	}

	/**
	 * @return the isDisplayed
	 */
	public boolean isDisplayed() {
		return isDisplayed;
	}

	/**
	 * @param isDisplayed the isDisplayed to set
	 */
	public void setDisplayed(boolean isDisplayed) {
		this.isDisplayed = isDisplayed;
	}

	public long getMailBoxEmailType() {
		return mailBoxEmailType;
	}

	public void setMailBoxEmailType(long mailBoxEmailType) {
		this.mailBoxEmailType = mailBoxEmailType;
	}

}
