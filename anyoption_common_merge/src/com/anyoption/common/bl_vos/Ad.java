package com.anyoption.common.bl_vos;


public class Ad implements java.io.Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1032411292739499496L;
	private long id;
	private int isDefault;
	private String name;

	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public int getIsDefault() {
		return isDefault;
	}

	public boolean isDefaultBoolean() {
		return isDefault==1;
	}
	public void setIsDefault(int isDefault) {
		this.isDefault = isDefault;
	}
	public void setDefaultBoolean(boolean b) {
		if (b)
			isDefault=1;
		else
			isDefault=0;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * Constructs a <code>String</code> with all attributes
	 * in name = value format.
	 *
	 * @return a <code>String</code> representation 
	 * of this object.
	 */
	public String toString()
	{
	    final String TAB = " \n ";
	    
	    String retValue = "";
	    
	    retValue = "Ad ( "
	        + super.toString() + TAB
	        + "id = " + this.id + TAB
	        + "isDefault = " + this.isDefault + TAB
	        + "name = " + this.name + TAB
	        + " )";
	
	    return retValue;
	}




}
