package com.anyoption.common.bl_vos;


import java.io.Serializable;
import java.util.Date;

/**
 * Population Entry VO class
 * @author Kobi
 *
 */
public class PopulationEntryBase implements Serializable {

	private static final long serialVersionUID = 3742840073444569819L;

	// PopualtionUser
	protected long populationUserId;
	protected long userId;
	protected long contactId;
	protected int entryTypeId;
	protected long entryTypeActionId;
	protected long assignWriterId;
	protected long lockHistoryId;
	protected long skinId;
	protected long delayId;
	protected long userBrandId;

	// Current Popualtion Entry
	protected long currEntryId;
	protected long currPopualtionTypeId;
	protected String currPopulationName;
	protected long populationId;
	protected long groupId;
	protected Date qualificationTime;
	protected long currPopualtionDeptId;


	// Old Popualtion Entry - when getting specific entry for DB it will entered into this fields
	protected long oldPopEntryId;
	protected long oldPopulationTypeId;
	protected String oldPopulationName;
	protected boolean isDisplayed;
	
	protected long userRankId;
	protected long userStatusId;
	protected Date timeLastLogin;
	protected boolean balanceBelowMinInvestAmount;
	protected boolean madeDepositButdidntMakeInv24HAfter;
	protected double conversion;
	protected double limitValue;
	protected int campaignPriorityId;
	protected long populationSalesTypeId;
	protected long populationTypeId;
	protected double sumDepositsBaseAmount;
	protected Date timeJoinControlGroup;

	public long getPopulationTypeId() {
		return populationTypeId;
	}

	public void setPopulationTypeId(long populationTypeId) {
		this.populationTypeId = populationTypeId;
	}

	// used to assign writers on main screen
	protected long tempAssignWriterId;
	
	protected String userName;
	protected long userClassId;

	public PopulationEntryBase() {
    }

	public boolean isEntryLocked(){
		return (lockHistoryId > 0);
	}


	/**
	 * Load population entry
	 * @param entry
	 */
	public void loadEntry(PopulationEntryBase entry) {
		this.currEntryId = entry.currEntryId;
		this.populationId = entry.populationId;
		this.userId = entry.userId;
		this.groupId = entry.groupId;
		this.contactId = entry.contactId;
		this.entryTypeId = entry.entryTypeId;
		this.oldPopEntryId = entry.oldPopEntryId;
		this.oldPopulationTypeId = entry.oldPopulationTypeId;
		this.oldPopulationName = entry.oldPopulationName;
		this.currPopualtionTypeId = entry.currPopualtionTypeId;
	}

	/**
	 * @return the assignWriterId
	 */
	public long getAssignWriterId() {
		return assignWriterId;
	}

	/**
	 * @param assignWriterId the assignWriterId to set
	 */
	public void setAssignWriterId(long assignWriterId) {
		this.assignWriterId = assignWriterId;
		this.tempAssignWriterId = assignWriterId;
	}

	/**
	 * @return the contactId
	 */
	public long getContactId() {
		return contactId;
	}

	/**
	 * @param contactId the contactId to set
	 */
	public void setContactId(long contactId) {
		this.contactId = contactId;
	}

	/**
	 * @return the entryId
	 */
	public long getCurrEntryId() {
		return currEntryId;
	}

	/**
	 * @param entryId the entryId to set
	 */
	public void setCurrEntryId(long entryId) {
		this.currEntryId = entryId;
	}

	/**
	 * @return the entryTypeId
	 */
	public int getEntryTypeId() {
		return entryTypeId;
	}

	/**
	 * @param entryTypeId the entryTypeId to set
	 */
	public void setEntryTypeId(int entryTypeId) {
		this.entryTypeId = entryTypeId;
	}

	/**
	 * @return the entryTypeActionId
	 */
	public long getEntryTypeActionId() {
		return entryTypeActionId;
	}

	/**
	 * @param entryTypeActionId the entryTypeActionId to set
	 */
	public void setEntryTypeActionId(long entryTypeActionId) {
		this.entryTypeActionId = entryTypeActionId;
	}

	/**
	 * @return the groupId
	 */
	public long getGroupId() {
		return groupId;
	}

	/**
	 * @param groupId the groupId to set
	 */
	public void setGroupId(long groupId) {
		this.groupId = groupId;
	}

	/**
	 * @return the lockingWriter
	 */
	public long getLockHistoryId() {
		return lockHistoryId;
	}

	/**
	 * @param lockingWriter the lockingWriter to set
	 */
	public void setLockHistoryId(long lockingWriter) {
		this.lockHistoryId = lockingWriter;
	}

	/**
	 * @return the populationId
	 */
	public long getPopulationId() {
		return populationId;
	}

	/**
	 * @param populationId the populationId to set
	 */
	public void setPopulationId(long populationId) {
		this.populationId = populationId;
	}

	/**
	 * @return the populationUserId
	 */
	public long getPopulationUserId() {
		return populationUserId;
	}

	/**
	 * @param populationUserId the populationUserId to set
	 */
	public void setPopulationUserId(long populationUserId) {
		this.populationUserId = populationUserId;
	}

	/**
	 * @return the qualificationTime
	 */
	public Date getQualificationTime() {
		return qualificationTime;
	}

	/**
	 * @param qualificationTime the qualificationTime to set
	 */
	public void setQualificationTime(Date qualificationTime) {
		this.qualificationTime = qualificationTime;
	}

	/**
	 * @return the skinId
	 */
	public long getSkinId() {
		return skinId;
	}

	/**
	 * @param skinId the skinId to set
	 */
	public void setSkinId(long skinId) {
		this.skinId = skinId;
	}

	/**
	 * @return the userId
	 */
	public long getUserId() {
		return userId;
	}

	/**
	 * @param userId the userId to set
	 */
	public void setUserId(long userId) {
		this.userId = userId;
	}

	/**
	 * @return the popualtionTypeId
	 */
	public long getCurrPopualtionTypeId() {
		return currPopualtionTypeId;
	}

	/**
	 * @param popualtionTypeId the popualtionTypeId to set
	 */
	public void setCurrPopualtionTypeId(long popualtionTypeId) {
		this.currPopualtionTypeId = popualtionTypeId;
	}

	/**
	 * @return the populationName
	 */
	public String getCurrPopulationName() {
		return currPopulationName;
	}

	/**
	 * @param populationName the populationName to set
	 */
	public void setCurrPopulationName(String populationName) {
		this.currPopulationName = populationName;
	}

	/**
	 * @return the oldPopEntryId
	 */
	public long getOldPopEntryId() {
		return oldPopEntryId;
	}

	/**
	 * @param oldPopEntryId the oldPopEntryId to set
	 */
	public void setOldPopEntryId(long oldPopEntryId) {
		this.oldPopEntryId = oldPopEntryId;
	}

	/**
	 * @return the oldPopulation
	 */
	public String getOldPopulationName() {
		return oldPopulationName;
	}

	/**
	 * @param oldPopulation the oldPopulation to set
	 */
	public void setOldPopulationName(String oldPopulation) {
		this.oldPopulationName = oldPopulation;
	}

	/**
	 * @return the oldPopulationTypeId
	 */
	public long getOldPopulationTypeId() {
		return oldPopulationTypeId;
	}

	/**
	 * @param oldPopulationTypeId the oldPopulationTypeId to set
	 */
	public void setOldPopulationTypeId(long oldPopulationTypeId) {
		this.oldPopulationTypeId = oldPopulationTypeId;
	}

	/**
	 * @return the currPopualtionDeptId
	 */
	public long getCurrPopualtionDeptId() {
		return currPopualtionDeptId;
	}

	/**
	 * @param currPopualtionDeptId the currPopualtionDeptId to set
	 */
	public void setCurrPopualtionDeptId(long currPopualtionDeptId) {
		this.currPopualtionDeptId = currPopualtionDeptId;
	}

	/**
	 * @return the isDisplay
	 */
	public boolean isDisplayed() {
		return isDisplayed;
	}

	/**
	 * @param isDisplay the isDisplay to set
	 */
	public void setDisplayed(boolean isDisplay) {
		this.isDisplayed = isDisplay;
	}

	/**
	 * @return the delayId
	 */
	public long getDelayId() {
		return delayId;
	}

	/**
	 * @param delayId the delayId to set
	 */
	public void setDelayId(long delayId) {
		this.delayId = delayId;
	}

	public String toString()
	{
	    final String TAB = " \n ";

	    String retValue = "";

	    retValue = "IssueStatus ( "
	        + super.toString() + TAB
	        + "populationUserId= " + this.populationUserId + TAB
	        + "userId= " + this.userId + TAB
	        + "contactId= " + this.contactId + TAB
	        + "entryTypeId= " + this.entryTypeId + TAB
	        + "assignWriterId" + this.assignWriterId + TAB
	        + "timeJoinControlGroup= " + this.timeJoinControlGroup + TAB
	        + " )";

	    return retValue;
	}

	/**
	 * @return the tempAssignWriterId
	 */
	public long getTempAssignWriterId() {
		return tempAssignWriterId;
	}

	/**
	 * @param tempAssignWriterId the tempAssignWriterId to set
	 */
	public void setTempAssignWriterId(long tempAssignWriterId) {
		this.tempAssignWriterId = tempAssignWriterId;
	}

	public void updateAssignedWriter(){
		this.assignWriterId = this.tempAssignWriterId;
	}


	/**
	 * @return the userRankId
	 */
	public long getUserRankId() {
		return userRankId;
	}

	/**
	 * @param userRankId the userRankId to set
	 */
	public void setUserRankId(long userRankId) {
		this.userRankId = userRankId;
	}

	/**
	 * @return the userStatusId
	 */
	public long getUserStatusId() {
		return userStatusId;
	}

	/**
	 * @param userStatusId the userStatusId to set
	 */
	public void setUserStatusId(long userStatusId) {
		this.userStatusId = userStatusId;
	}

	/**
	 * @return the timeLastLogin
	 */
	public Date getTimeLastLogin() {
		return timeLastLogin;
	}

	/**
	 * @param timeLastLogin the timeLastLogin to set
	 */
	public void setTimeLastLogin(Date timeLastLogin) {
		this.timeLastLogin = timeLastLogin;
	}

	/**
	 * @return the balanceBelowMinInvestAmount
	 */
	public boolean isBalanceBelowMinInvestAmount() {
		return balanceBelowMinInvestAmount;
	}

	/**
	 * @param balanceBelowMinInvestAmount the balanceBelowMinInvestAmount to set
	 */
	public void setBalanceBelowMinInvestAmount(boolean balanceBelowMinInvestAmount) {
		this.balanceBelowMinInvestAmount = balanceBelowMinInvestAmount;
	}

	/**
	 * @return the madeDepositButdidntMakeInv24HAfter
	 */
	public boolean isMadeDepositButdidntMakeInv24HAfter() {
		return madeDepositButdidntMakeInv24HAfter;
	}

	/**
	 * @param madeDepositButdidntMakeInv24HAfter the madeDepositButdidntMakeInv24HAfter to set
	 */
	public void setMadeDepositButdidntMakeInv24HAfter(
			boolean madeDepositButdidntMakeInv24HAfter) {
		this.madeDepositButdidntMakeInv24HAfter = madeDepositButdidntMakeInv24HAfter;
	}

	/**
	 * @return the conversion
	 */
	public double getConversion() {
		return conversion;
	}

	/**
	 * @param conversion the conversion to set
	 */
	public void setConversion(double conversion) {
		this.conversion = conversion;
	}

	/**
	 * @return the limitValue
	 */
	public double getLimitValue() {
		return limitValue;
	}

	/**
	 * @param limitValue the limitValue to set
	 */
	public void setLimitValue(double limitValue) {
		this.limitValue = limitValue;
	}

	/**
	 * @return the campaignPriorityId
	 */
	public int getCampaignPriorityId() {
		return campaignPriorityId;
	}

	/**
	 * @param campaignPriorityId the campaignPriorityId to set
	 */
	public void setCampaignPriorityId(int campaignPriorityId) {
		this.campaignPriorityId = campaignPriorityId;
	}

	/**
	 * @return the populationSalesTypeId
	 */
	public long getPopulationSalesTypeId() {
		return populationSalesTypeId;
	}

	/**
	 * @param populationSalesTypeId the populationSalesTypeId to set
	 */
	public void setPopulationSalesTypeId(long populationSalesTypeId) {
		this.populationSalesTypeId = populationSalesTypeId;
	}

	/**
	 * @return the sumDepositsBaseAmount
	 */
	public double getSumDepositsBaseAmount() {
		return sumDepositsBaseAmount;
	}

	/**
	 * @param sumDepositsBaseAmount the sumDepositsBaseAmount to set
	 */
	public void setSumDepositsBaseAmount(double sumDepositsBaseAmount) {
		this.sumDepositsBaseAmount = sumDepositsBaseAmount;
	}

	/**
	 * @return the timeJoinControlGroup
	 */
	public Date getTimeJoinControlGroup() {
		return timeJoinControlGroup;
	}

	/**
	 * @param timeJoinControlGroup the timeJoinControlGroup to set
	 */
	public void setTimeJoinControlGroup(Date timeJoinControlGroup) {
		this.timeJoinControlGroup = timeJoinControlGroup;
	}

	/**
	 * @return the userName
	 */
	public String getUserName() {
		return userName;
	}

	/**
	 * @param userName the userName to set
	 */
	public void setUserName(String userName) {
		this.userName = userName;
	}

	/**
	 * @return the userClassId
	 */
	public long getUserClassId() {
		return userClassId;
	}

	/**
	 * @param userClassId the userClassId to set
	 */
	public void setUserClassId(long userClassId) {
		this.userClassId = userClassId;
	}
	
	/**
	 * @return the userBrandId
	 */
	public long getUserBrandId() {
		return userBrandId;
	}

	/**
	 * @param userBrandId the userBrandId to set
	 */
	public void setUserBrandId(long userBrandId) {
		this.userBrandId = userBrandId;
	}
	
	/**
	 * PopulationEntryBase
	 * @param peb
	 */
	public PopulationEntryBase(PopulationEntryBase peb) {
		super();
		this.populationUserId = peb.getPopulationUserId();
		this.userId = peb.getUserId();
		this.contactId = peb.getContactId();
		this.entryTypeId = peb.getEntryTypeId();
		this.entryTypeActionId = peb.getEntryTypeActionId();
		this.assignWriterId = peb.getAssignWriterId();
		this.lockHistoryId = peb.getLockHistoryId();
		this.skinId = peb.getSkinId();
		this.delayId = peb.getDelayId();
		this.userBrandId = peb.getUserBrandId();
		this.currEntryId = peb.getCurrEntryId();
		this.currPopualtionTypeId = peb.getCurrPopualtionTypeId();
		this.currPopulationName = peb.getCurrPopulationName();
		this.populationId = peb.getPopulationId();
		this.groupId = peb.getGroupId();
		this.qualificationTime = peb.getQualificationTime();
		this.currPopualtionDeptId = peb.getCurrPopualtionDeptId();
		this.oldPopEntryId = peb.getOldPopEntryId();
		this.oldPopulationTypeId = peb.getOldPopulationTypeId();
		this.oldPopulationName = peb.getOldPopulationName();
		this.isDisplayed = peb.isDisplayed();
		this.userRankId = peb.getUserRankId();
		this.userStatusId = peb.getUserStatusId();
		this.timeLastLogin = peb.getTimeLastLogin();
		this.balanceBelowMinInvestAmount = peb.isBalanceBelowMinInvestAmount();
		this.madeDepositButdidntMakeInv24HAfter = peb.isMadeDepositButdidntMakeInv24HAfter();
		this.conversion = peb.getConversion();
		this.limitValue = peb.getLimitValue();
		this.campaignPriorityId = peb.getCampaignPriorityId();
		this.populationSalesTypeId = peb.getPopulationSalesTypeId();
		this.populationTypeId = peb.getPopulationTypeId();
		this.sumDepositsBaseAmount = peb.getSumDepositsBaseAmount();
		this.timeJoinControlGroup = peb.getTimeJoinControlGroup();
		this.tempAssignWriterId = peb.getTempAssignWriterId();
		this.userName = peb.getUserName();
		this.userClassId = peb.getUserClassId();
	}
}
