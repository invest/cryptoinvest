package com.anyoption.common.bl_vos;

import java.util.Date;

/**
 * @author Eran
 *
 */
public class WebMoneyDeposit implements java.io.Serializable {

	private static final long serialVersionUID = 1L;
	private long id;
	private long transactionId;
	private Date timeCreated;
	private String lmi_payment_amount;
	private String lmi_mode;
	private String lmi_sys_invs_no;
	private String lmi_sys_trans_no;
	private String lmi_payer_purse;
	private String lmi_payer_wm;
	private String lmi_hash;
	private String lmi_sys_trans_date;
	private long currencyId;

	public WebMoneyDeposit() {
		id = 0;
		transactionId = 0;
	}

	/**
	 * @return the id
	 */
	public long getId() {
		return id;
	}


	/**
	 * @param id the id to set
	 */
	public void setId(long id) {
		this.id = id;
	}


	/**
	 * @return the lmi_hash
	 */
	public String getLmi_hash() {
		return lmi_hash;
	}


	/**
	 * @param lmi_hash the lmi_hash to set
	 */
	public void setLmi_hash(String lmi_hash) {
		this.lmi_hash = lmi_hash;
	}


	/**
	 * @return the lmi_mode
	 */
	public String getLmi_mode() {
		return lmi_mode;
	}


	/**
	 * @param lmi_mode the lmi_mode to set
	 */
	public void setLmi_mode(String lmi_mode) {
		this.lmi_mode = lmi_mode;
	}


	/**
	 * @return the lmi_payer_purse
	 */
	public String getLmi_payer_purse() {
		return lmi_payer_purse;
	}


	/**
	 * @param lmi_payer_purse the lmi_payer_purse to set
	 */
	public void setLmi_payer_purse(String lmi_payer_purse) {
		this.lmi_payer_purse = lmi_payer_purse;
	}


	/**
	 * @return the lmi_payer_wm
	 */
	public String getLmi_payer_wm() {
		return lmi_payer_wm;
	}


	/**
	 * @param lmi_payer_wm the lmi_payer_wm to set
	 */
	public void setLmi_payer_wm(String lmi_payer_wm) {
		this.lmi_payer_wm = lmi_payer_wm;
	}


	/**
	 * @return the lmi_payment_amount
	 */
	public String getLmi_payment_amount() {
		return lmi_payment_amount;
	}


	/**
	 * @param lmi_payment_amount the lmi_payment_amount to set
	 */
	public void setLmi_payment_amount(String lmi_payment_amount) {
		this.lmi_payment_amount = lmi_payment_amount;
	}


	/**
	 * @return the lmi_sys_invs_no
	 */
	public String getLmi_sys_invs_no() {
		return lmi_sys_invs_no;
	}


	/**
	 * @param lmi_sys_invs_no the lmi_sys_invs_no to set
	 */
	public void setLmi_sys_invs_no(String lmi_sys_invs_no) {
		this.lmi_sys_invs_no = lmi_sys_invs_no;
	}


	/**
	 * @return the lmi_sys_trans_date
	 */
	public String getLmi_sys_trans_date() {
		return lmi_sys_trans_date;
	}


	/**
	 * @param lmi_sys_trans_date the lmi_sys_trans_date to set
	 */
	public void setLmi_sys_trans_date(String lmi_sys_trans_date) {
		this.lmi_sys_trans_date = lmi_sys_trans_date;
	}


	/**
	 * @return the lmi_sys_trans_no
	 */
	public String getLmi_sys_trans_no() {
		return lmi_sys_trans_no;
	}


	/**
	 * @param lmi_sys_trans_no the lmi_sys_trans_no to set
	 */
	public void setLmi_sys_trans_no(String lmi_sys_trans_no) {
		this.lmi_sys_trans_no = lmi_sys_trans_no;
	}


	/**
	 * @return the transactionId
	 */
	public long getTransactionId() {
		return transactionId;
	}


	/**
	 * @param transactionId the transactionId to set
	 */
	public void setTransactionId(long transactionId) {
		this.transactionId = transactionId;
	}

	/**
	 * @return the timeCreated
	 */
	public Date getTimeCreated() {
		return timeCreated;
	}

	/**
	 * @param timeCreated the timeCreated to set
	 */
	public void setTimeCreated(Date timeCreated) {
		this.timeCreated = timeCreated;
	}

	/**
	 * @return the currencyId
	 */
	public long getCurrencyId() {
		return currencyId;
	}

	/**
	 * @param currencyId the currencyId to set
	 */
	public void setCurrencyId(long currencyId) {
		this.currencyId = currencyId;
	}

	/**
	 * Constructs a <code>String</code> with all attributes
	 * in name = value format.
	 *
	 * @return a <code>String</code> representation
	 * of this object.
	 */
	public String toString()
	{
	    final String ls = " \n ";
	    String retValue = "";
	    retValue = "WebMoneyDeposit ( "
	        + super.toString() + ls
	        + "id = " + this.id + ls
	        + "transactionId = " + this.transactionId + ls
	    	+ "lmi_payment_amount=" + this.lmi_payment_amount + ls
	    	+ "lmi_mode=" + this.lmi_mode + ls
	    	+ "lmi_sys_invs_no=" + this.lmi_sys_invs_no + ls
	    	+ "lmi_sys_trans_no=" + this.lmi_sys_trans_no + ls
	    	+ "lmi_payer_purse=" + this.lmi_payer_purse + ls
	    	+ "lmi_payer_wm=" + this.lmi_payer_wm + ls
	    	+ "lmi_hash=" + this.lmi_hash + ls
	    	+ "lmi_sys_trans_date=" + this.lmi_sys_trans_date + ls
	        + " )";

	    return retValue;
	}

}
