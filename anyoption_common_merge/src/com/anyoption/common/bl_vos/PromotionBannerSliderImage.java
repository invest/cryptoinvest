package com.anyoption.common.bl_vos;

import java.io.Serializable;
import java.util.Date;

public class PromotionBannerSliderImage implements Serializable {

private static final long serialVersionUID = 1L;
	
	private long id;
	private String imageName;
	private String imageUrl;
	private Date timeCreated;
	private Date timeUpdated;
	private long bannerSliderId;
	private long writerId;
	private long languageId;
	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getImageName() {
		return imageName;
	}

	public void setImageName(String imageName) {
		this.imageName = imageName;
	}

	public String getImageUrl() {
		return imageUrl;
	}

	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}

	public Date getTimeCreated() {
		return timeCreated;
	}

	public void setTimeCreated(Date timeCreated) {
		this.timeCreated = timeCreated;
	}

	public Date getTimeUpdated() {
		return timeUpdated;
	}

	public void setTimeUpdated(Date timeUpdated) {
		this.timeUpdated = timeUpdated;
	}

	public long getBannerSliderId() {
		return bannerSliderId;
	}

	public void setBannerSliderId(long bannerSliderId) {
		this.bannerSliderId = bannerSliderId;
	}

	public long getWriterId() {
		return writerId;
	}

	public void setWriterId(long writerId) {
		this.writerId = writerId;
	}

	public long getLanguageId() {
		return languageId;
	}

	public void setLanguageId(long languageId) {
		this.languageId = languageId;
	}


	public String toString() {
        String ls = System.getProperty("line.separator");
        return ls + "PromotionBannerSliderImage: "
            + super.toString()
            + "id: " + id + ls
            + "imageName: " + imageName + ls
            + "imageUrl: " + imageUrl + ls
            + "timeCreated: " + timeCreated + ls
            + "timeUpdated: " + timeUpdated + ls
            + "bannerSliderId: " + bannerSliderId + ls
            + "writerId: " + writerId + ls
            + "languageId: " + languageId + ls;
    }
}
