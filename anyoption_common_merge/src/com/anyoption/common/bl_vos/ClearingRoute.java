package com.anyoption.common.bl_vos;

import java.io.Serializable;
import java.util.Date;

import com.anyoption.common.beans.base.Platform;

public class ClearingRoute implements Serializable {

	private static final long serialVersionUID = -5611653841085882196L;
	
	private long id;
	private String startBIN;
	private String endBIN;
	private long binId;
	private long depositClearingProviderId;
	private long withdrawClearingProviderId;
	private long CFTClearingProviderId;
	private String comments;
	private Date timelastSuccess;
	private Date timeLastReroute;
	private long businessSkinId;
	private boolean isActive;
	private long currnecyId;
	private long ccType;
	private long countryId;
	private long skinId;
	private String bin;

	private String depositProviderName;
	private String withdrawProviderName;
	private String CFTProviderName;
	private String businessSkinName;
	private String currencyName;
	private String creditCardName; //CC type name
	private String countryName;
	private String skinName;
	
	protected Platform platform = new Platform();
	
	
	@Override
	public String toString() {
		return "ClearingRoute [id=" + id + ", startBIN=" + startBIN
				+ ", endBIN=" + endBIN + ", binId=" + binId
				+ ", depositClearingProviderId=" + depositClearingProviderId
				+ ", withdrawClearingProviderId=" + withdrawClearingProviderId
				+ ", CFTClearingProviderId=" + CFTClearingProviderId
				+ ", comments=" + comments + ", timelastSuccess="
				+ timelastSuccess + ", timeLastReroute=" + timeLastReroute
				+ ", businessSkinId=" + businessSkinId + ", isActive="
				+ isActive + ", currnecyId=" + currnecyId + ", ccType="
				+ ccType + ", countryId=" + countryId + ", skinId=" + skinId
				+ ", bin=" + bin + ", platform=" + platform + "]";
	}
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getStartBIN() {
		return startBIN;
	}
	public void setStartBIN(String startBIN) {
		this.startBIN = startBIN;
	}
	public long getDepositClearingProviderId() {
		return depositClearingProviderId;
	}
	public void setDepositClearingProviderId(long depositClearingProviderId) {
		this.depositClearingProviderId = depositClearingProviderId;
	}
	public long getWithdrawClearingProviderId() {
		return withdrawClearingProviderId;
	}
	public void setWithdrawClearingProviderId(long withdrawClearingProviderId) {
		this.withdrawClearingProviderId = withdrawClearingProviderId;
	}
	public long getCFTClearingProviderId() {
		return CFTClearingProviderId;
	}
	public void setCFTClearingProviderId(long cFTClearingProviderId) {
		CFTClearingProviderId = cFTClearingProviderId;
	}
	public Date getTimelastSuccess() {
		return timelastSuccess;
	}
	public void setTimelastSuccess(Date timelastSuccess) {
		this.timelastSuccess = timelastSuccess;
	}
	public long getBinId() {
		return binId;
	}
	public void setBinId(long binId) {
		this.binId = binId;
	}
	public Date getTimeLastReroute() {
		return timeLastReroute;
	}
	public void setTimeLastReroute(Date timeLastReroute) {
		this.timeLastReroute = timeLastReroute;
	}
	public long getBusinessSkinId() {
		return businessSkinId;
	}
	public void setBusinessSkinId(long businessSkinId) {
		this.businessSkinId = businessSkinId;
	}
	public boolean isActive() {
		return isActive;
	}
	public void setActive(boolean isActive) {
		this.isActive = isActive;
	}
	public long getCurrnecyId() {
		return currnecyId;
	}
	public void setCurrnecyId(long currnecyId) {
		this.currnecyId = currnecyId;
	}
	public long getCountryId() {
		return countryId;
	}
	public void setCountryId(long countryId) {
		this.countryId = countryId;
	}
	public long getCcType() {
		return ccType;
	}
	public void setCcType(long ccType) {
		this.ccType = ccType;
	}
	public long getSkinId() {
		return skinId;
	}
	public void setSkinId(long skinId) {
		this.skinId = skinId;
	}
	/**
	 * @return the platform
	 */
	public Platform getPlatform() {
		return platform;
	}
	/**
	 * @param platform the platform to set
	 */
	public void setPlatform(Platform platform) {
		this.platform = platform;
	}
	/**
	 * @return the endBIN
	 */
	public String getEndBIN() {
		return endBIN;
	}
	/**
	 * @param endBIN the endBIN to set
	 */
	public void setEndBIN(String endBIN) {
		this.endBIN = endBIN;
	}
	/**
	 * @return the comments
	 */
	public String getComments() {
		return comments;
	}
	/**
	 * @param comments the comments to set
	 */
	public void setComments(String comments) {
		this.comments = comments;
	}
	/**
	 * @return the bin
	 */
	public String getBin() {
		return bin;
	}
	/**
	 * @param bin the bin to set
	 */
	public void setBin(String bin) {
		this.startBIN = this.endBIN = this.bin = bin;	
	}
} 
