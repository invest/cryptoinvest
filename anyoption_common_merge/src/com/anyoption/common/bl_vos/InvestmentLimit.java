package com.anyoption.common.bl_vos;

import java.util.Date;

public class InvestmentLimit implements java.io.Serializable  {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 62396445758134359L;
	public static final int TYPE_DEFUALT = 1;
	public static final int TYPE_MARKET = 2;
	public static final int TYPE_USERS = 3;
	public static final int TYPE_USER_MARKET = 4;

	protected long id;
    protected Long currencyId;
    protected long minAmount;
    protected long maxAmount;
    protected long minAmountGroupId;
    protected long maxAmountGroupId;
    protected long oneTouchMin;
    protected long userId;
	protected long marketId;
	protected Date startDate;
	protected Date endDate;
	protected long writerId;
	protected Date timeCreated;
	protected long opportunityTypeId;
	protected String opportunityTypeName;
	protected long scheduled;
	protected boolean isActive;
	protected int invLimitTypeId;
	protected long period;      // disable period from start time (in minutes)

	protected String userName;
	protected String marketNameKey;
	protected String startDateMin;
	protected String startDateHour;
    protected String currencyName;
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public Long getCurrencyId() {
		return currencyId;
	}
	public void setCurrencyId(Long currencyId) {
		this.currencyId = currencyId;
	}
	public long getMinAmount() {
		return minAmount;
	}
	public void setMinAmount(long minAmount) {
		this.minAmount = minAmount;
	}
	public long getMaxAmount() {
		return maxAmount;
	}
	public void setMaxAmount(long maxAmount) {
		this.maxAmount = maxAmount;
	}
	public long getMinAmountGroupId() {
		return minAmountGroupId;
	}
	public void setMinAmountGroupId(long minAmountGroupId) {
		this.minAmountGroupId = minAmountGroupId;
	}
	public long getMaxAmountGroupId() {
		return maxAmountGroupId;
	}
	public void setMaxAmountGroupId(long maxAmountGroupId) {
		this.maxAmountGroupId = maxAmountGroupId;
	}
	public long getOneTouchMin() {
		return oneTouchMin;
	}
	public void setOneTouchMin(long oneTouchMin) {
		this.oneTouchMin = oneTouchMin;
	}
	public long getUserId() {
		return userId;
	}
	public void setUserId(long userId) {
		this.userId = userId;
	}
	public long getMarketId() {
		return marketId;
	}
	public void setMarketId(long marketId) {
		this.marketId = marketId;
	}
	public Date getStartDate() {
		return startDate;
	}
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}
	public Date getEndDate() {
		return endDate;
	}
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
	public long getWriterId() {
		return writerId;
	}
	public void setWriterId(long writerId) {
		this.writerId = writerId;
	}
	public Date getTimeCreated() {
		return timeCreated;
	}
	public void setTimeCreated(Date timeCreated) {
		this.timeCreated = timeCreated;
	}
	public long getOpportunityTypeId() {
		return opportunityTypeId;
	}
	public void setOpportunityTypeId(long opportunityTypeId) {
		this.opportunityTypeId = opportunityTypeId;
	}
	public String getOpportunityTypeName() {
		return opportunityTypeName;
	}
	public void setOpportunityTypeName(String opportunityTypeName) {
		this.opportunityTypeName = opportunityTypeName;
	}
	public long getScheduled() {
		return scheduled;
	}
	public void setScheduled(long scheduled) {
		this.scheduled = scheduled;
	}
	public boolean isActive() {
		return isActive;
	}
	public void setActive(boolean isActive) {
		this.isActive = isActive;
	}
	public int getInvLimitTypeId() {
		return invLimitTypeId;
	}
	public void setInvLimitTypeId(int invLimitTypeId) {
		this.invLimitTypeId = invLimitTypeId;
	}
	public long getPeriod() {
		return period;
	}
	public void setPeriod(long period) {
		this.period = period;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getMarketNameKey() {
		return marketNameKey;
	}
	public void setMarketNameKey(String marketNameKey) {
		this.marketNameKey = marketNameKey;
	}
	public String getStartDateMin() {
		return startDateMin;
	}
	public void setStartDateMin(String startDateMin) {
		this.startDateMin = startDateMin;
	}
	public String getStartDateHour() {
		return startDateHour;
	}
	public void setStartDateHour(String startDateHour) {
		this.startDateHour = startDateHour;
	}
	public String getCurrencyName() {
		return currencyName;
	}
	public void setCurrencyName(String currencyName) {
		this.currencyName = currencyName;
	}

}
