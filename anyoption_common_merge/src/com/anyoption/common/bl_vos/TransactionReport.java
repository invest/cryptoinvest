package com.anyoption.common.bl_vos;

import java.util.Date;

public class TransactionReport {

	private long transactionId;
	private Date transactionDate;
	private String writerName;
	private long issueActionId;
	private Date timeInserted;
	private String issueComment;
	private String usersMobilePhone;
	private String usersLandLinePhone;
	
	
	/**
	 * @return the transactionId
	 */
	public long getTransactionId() {
		return transactionId;
	}
	/**
	 * @param transactionId the transactionId to set
	 */
	public void setTransactionId(long transactionId) {
		this.transactionId = transactionId;
	}
	/**
	 * @return the transactionDate
	 */
	public Date getTransactionDate() {
		return transactionDate;
	}
	/**
	 * @param transactionDate the transactionDate to set
	 */
	public void setTransactionDate(Date transactionDate) {
		this.transactionDate = transactionDate;
	}
	/**
	 * @return the writerName
	 */
	public String getWriterName() {
		return writerName;
	}
	/**
	 * @param writerName the writerName to set
	 */
	public void setWriterName(String writerName) {
		this.writerName = writerName;
	}
	/**
	 * @return the issueActionId
	 */
	public long getIssueActionId() {
		return issueActionId;
	}
	/**
	 * @param issueActionId the issueActionId to set
	 */
	public void setIssueActionId(long issueActionId) {
		this.issueActionId = issueActionId;
	}
	/**
	 * @return the timeInserted
	 */
	public Date getTimeInserted() {
		return timeInserted;
	}
	/**
	 * @param timeInserted the timeInserted to set
	 */
	public void setTimeInserted(Date timeInserted) {
		this.timeInserted = timeInserted;
	}
	/**
	 * @return the issueComment
	 */
	public String getIssueComment() {
		return issueComment;
	}
	/**
	 * @param issueComment the issueComment to set
	 */
	public void setIssueComment(String issueComment) {
		this.issueComment = issueComment;
	}
	/**
	 * @return the usersMobilePhone
	 */
	public String getUsersMobilePhone() {
		return usersMobilePhone;
	}
	/**
	 * @param usersMobilePhone the usersMobilePhone to set
	 */
	public void setUsersMobilePhone(String usersMobilePhone) {
		this.usersMobilePhone = usersMobilePhone;
	}
	/**
	 * @return the usersLandLinePhone
	 */
	public String getUsersLandLinePhone() {
		return usersLandLinePhone;
	}
	/**
	 * @param usersLandLinePhone the usersLandLinePhone to set
	 */
	public void setUsersLandLinePhone(String usersLandLinePhone) {
		this.usersLandLinePhone = usersLandLinePhone;
	}
	
}
