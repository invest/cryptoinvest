package com.anyoption.common.jobs;

import java.sql.Connection;
import java.sql.SQLException;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.Job;
import com.anyoption.common.daos.JobsDAOBase;
import com.anyoption.common.managers.BaseBLManager;

public class JobUtil {
    private static Logger log = Logger.getLogger(JobUtil.class);
    
    public static Job startJob(Connection conn, long jobId, String serverId, String application) throws SQLException {
        log.info("startJob - id: " + jobId + " serverId: " + serverId);
        Job job = null;
        try {
            conn.setAutoCommit(false);
            job = JobsDAOBase.getByIdWithLock(conn, jobId, serverId, application);
            if (null != job/* && job.getRunning() == Job.RUNNING_NO*/) {
                JobsDAOBase.setRunningById(conn, jobId, Job.RUNNING_YES, serverId, false);
            } else {
                job = null;
            }
            conn.commit();
        } catch (SQLException sqle) {
            BaseBLManager.rollback(conn);
            job = null;
            throw sqle;
        } finally {
            BaseBLManager.setAutoCommitBack(conn);
        }
        log.info("startJob - success: " + (null != job));
        return job;
    }
    
    public static void stopJob(Connection conn, long jobId, String serverId, boolean updateRunTime) throws SQLException {
        log.info("stopJob - id: " + jobId + " updateRunTime: " + updateRunTime);
        int count = JobsDAOBase.setRunningById(conn, jobId, Job.RUNNING_NO, serverId, updateRunTime);
        log.info("stopJob - count: " + count);
    }
}