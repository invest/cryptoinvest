package com.anyoption.common.sms;

/**
 * @author kirilim
 */
public class SMS {
	/**
	 * the initial statuses are :
	 * STATUS_QUEUED and STATUS_NOT_VERIFIED 
	 * pooled by the worker from the sms table
	 * 
	 * STATUS_NOT_VERIFIED -> STATUS_WAITING_HLR
	 * STATUS_WAITING_HLR -> STATUS_INVALID or STATUS_QUEUED
 	 * STATUS_QUEUED -> STATUS_SUBMITTED
 	 * STATUS_SUBMITTED ->  STATUS_DELIVERED or STATUS_FAILED
	 */
    public static final long STATUS_QUEUED				= 1;
    public static final long STATUS_SUBMITTED			= 2;
    public static final long STATUS_FAILED_TO_SUBMIT	= 3;
    public static final long STATUS_DELIVERED_TO_SMSC	= 4;
    public static final long STATUS_QUEUED_ON_SMSC		= 5;
    public static final long STATUS_DELIVERED			= 6;
    public static final long STATUS_FAILED				= 7;
    public static final long STATUS_UNROUTABLE			= 8;
    public static final long STATUS_NOT_VERIFIED		= 9;
    public static final long STATUS_INVALID				= 10;
    public static final long STATUS_WAITING_HLR			= 11;
   
    public static final long TYPE_TEXT		= 1;
    public static final long TYPE_WAP_PUSH	= 2;
    public static final long TYPE_JME_PUSH	= 3;

    public static final long APP_TYPE_NONE = 0;
    public static final long APP_TYPE_SMS_PROMOTION = 1;
    
    public static final long DESCRIPTION_RESET_PASSWORD	= 1;
    public static final long DESCRIPTION_EXPIRY			= 2;
    public static final long DESCRIPTION_WELCOME		= 3;
    public static final long DESCRIPTION_PROMOTION		= 4;
    public static final long DESCRIPTION_DOWNLOAD		= 5;
}
