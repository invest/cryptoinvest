package com.anyoption.common.sms;

import java.util.Locale;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.User;
import com.anyoption.common.beans.base.User.MobileNumberValidation;
import com.anyoption.common.managers.SMSManagerBase;
import com.anyoption.common.util.CommonUtil;

/**
 * This thread is responsible for the sending of messages upon registration. If the first message fails because of invalid phone number, no
 * additional messages will be sent. The thread will retry sending additional messages based on
 * {@link RegisterSMSThread#REGISTER_SMS_RETRIES_LIMIT} property.
 * 
 * @author kiril.mutafchiev
 */
public class RegisterSMSThread extends Thread {

	private static final Logger log = Logger.getLogger(RegisterSMSThread.class);
	private static final long REGISTER_SMS_SLEEP_INTERVAL = 15 * 1000;
	private static final long REGISTER_SMS_LOADING_SLEEP_INTERVAL = 3 * 1000;
	private static final int REGISTER_SMS_RETRIES_LIMIT = 3;
	private boolean running;
	private int retries;
	private User user;
	private Locale locale;
	private int platformId;
	private long origin;

	public RegisterSMSThread(User user, Locale locale, int platformId, long origin) {
		this.user = user;
		this.locale = locale;
		this.platformId = platformId;
		this.origin = origin;
		running = false;
		retries = 0;
	}

	public void run() {
		Thread.currentThread().setName("RegisterSMSThread(" + origin + ";" + user.getId() + ")");
		log.debug("RegisterSMSThread starting");
		running = true;
		CommonUtil.sendRegistrationSMSByTextLocalProvider(user, locale);
		putThreadToSleep(REGISTER_SMS_LOADING_SLEEP_INTERVAL);
		while (running) { // check status of first sms, if the phone number is invalid don't send second sms
			user.setMobileNumberValidated(SMSManagerBase.getMobileNumberValidation(user.getId()));
			MobileNumberValidation validation = user.getMobileNumberValidation();
			if (validation == MobileNumberValidation.INVALID) {
				running = false;
				log.debug("Status received for mobile phone validation is " + validation + ". Aborting phone verification sms");
				return;
			} else if (validation == MobileNumberValidation.VALID
						|| (validation == MobileNumberValidation.NOTVALIDATED && retries == REGISTER_SMS_RETRIES_LIMIT)) {
				if (validation != MobileNumberValidation.VALID) {
					log.debug("Mobile number still not validated, but retries limit reached. Sending sms regardless of status");
				}
				SMSManagerBase.sendPhoneNumberVerificationMessage(user, locale, platformId);
				running = false;
				log.debug("All sms sent, thread finished");
				return;
			} else if (retries > REGISTER_SMS_RETRIES_LIMIT) {
				log.debug("Retries above the limit, aborting");
				running = false;
				return;
			}
			log.trace("Status is still " + MobileNumberValidation.NOTVALIDATED + ", retrying(" + retries + ")");
			retries++;
			putThreadToSleep(REGISTER_SMS_SLEEP_INTERVAL);
		}
	}

	private void putThreadToSleep(long interval) {
		log.trace("Putting RegisterSMSThread to sleep");
		try {
			Thread.sleep(interval);
		} catch (InterruptedException e) {
			log.error("Unable to put thread to sleep", e);
		}
	}
}