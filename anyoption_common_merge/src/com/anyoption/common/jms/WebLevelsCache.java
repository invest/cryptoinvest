package com.anyoption.common.jms;

import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.concurrent.ConcurrentHashMap;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.ObjectMessage;

import org.apache.log4j.Logger;

import com.anyoption.common.jms.ifc.LevelsCacheListener;
import com.anyoption.common.util.ConstantsBase;

/**
 * JMS communication component for WEB applications.
 *
 * @author Tony
 */
public class WebLevelsCache extends LevelsCache {
    private static final Logger log = Logger.getLogger(WebLevelsCache.class);

    private List<WebLevelLookupBean> requests = null;
    private Hashtable<Long, long[]> currentMarketsOnHomePage;
    private long[] openedMarkets;
    private ConcurrentHashMap<Long, Map<Long, Set<Long>>> skinMarketsByGroup;
    private Map<Long, SortedSet<WLCOppsCacheBean>> marketsOpportunities;

    public WebLevelsCache(String initialContextFactory, String providerURL, String connectionFactoryName, String queueName, String topicName, int msgPoolSize, int recoveryPause, LevelsCacheListener listener) {
        super(initialContextFactory, providerURL, connectionFactoryName, queueName, topicName, msgPoolSize, recoveryPause, listener);

        requests = Collections.synchronizedList(new LinkedList<WebLevelLookupBean>());
        currentMarketsOnHomePage = new Hashtable<Long, long[]>();
        skinMarketsByGroup = new ConcurrentHashMap<Long, Map<Long, Set<Long>>>();
        marketsOpportunities = new ConcurrentHashMap<Long, SortedSet<WLCOppsCacheBean>>();
    }

    public void getLevels(WebLevelLookupBean wllb) {
        requests.add(wllb);
        sender.send("level_" + wllb.getFeedName() + "_" + wllb.getOppId() + "_" + wllb.getSkinGroup().getId());
        synchronized (wllb) {
            try {
                wllb.wait(3000);
            } catch (InterruptedException ie) {
                // do nothing
            }
        }
    }

    public long[] getMarketsOnHomePage(long skinId) {
        return currentMarketsOnHomePage.get(skinId);
    }

    /**
     * @param skinId
     * @return a map of the top markets by groups for a given skin.
     */
    public Map<Long, Set<Long>> getMarketsByGroup(long skinId) {
    	return skinMarketsByGroup.get(skinId);
    }

    public long[] getOpenedMarkets() {
        return openedMarkets;
    }

    @Override
	public void onMessage(Message message) {
        if (null == message) {
            log.warn("Invalid message");
            return;
        }
        if (log.isDebugEnabled()) {
            log.debug("Received message");
        }
        //we have to extract data from the Message object
        ETraderFeedMessage feedMsg = null;
        try {
            if (message instanceof ObjectMessage) {
                //cast Message to ObjectMessage.
                Object obj = ((ObjectMessage) message).getObject();
                if (obj instanceof ETraderFeedMessage) {
                    feedMsg = (ETraderFeedMessage) obj;
                } else if (obj instanceof String) {
                    String msg = (String) obj;
                    if (log.isDebugEnabled()) {
                        log.debug("String message received: " + msg);
                    }
                    if (msg.startsWith("aohp")) {
                        String[] arr = msg.split("_");
                        String[] ms = arr[2].split("\\|");
                        long[] lms = new long[ms.length];
                        for (int i = 0; i < ms.length; i++) {
                            lms[i] = Long.parseLong(ms[i]);
                        }
                        currentMarketsOnHomePage.put(Long.parseLong(arr[1]), lms);
                    } else if (msg.startsWith("aost")) {
                        String[] arr = msg.split("_");
                        long[] lms;
                        if (arr.length > 1) {
                            String[] ms = arr[1].split("\\|");
                            lms = new long[ms.length];
                            for (int i = 0; i < ms.length; i++) {
                                lms[i] = Long.parseLong(ms[i]);
                            }
                        } else {
                            lms = new long[0];
                        }
                        openedMarkets = lms;
                    } else if (msg.startsWith(ConstantsBase.JMS_MARKETS_BY_GROUP_PREF)) {
                    	parseTopMarketsByGroupMsg(msg);
                    } else if (msg.startsWith(ConstantsBase.JMS_OPPS_UPDATE)) {
                    	String[] arr = msg.split("_");
                    	String command = arr[WLCOppsCacheBean.COMMAND_FIELD];
                    	if (command.equals(ConstantsBase.JMS_OPPS_UPDATE_COMMAND_CLEAR)) {
                    		marketsOpportunities.clear();
                    		return;
                    	}
                    	TreeSet<WLCOppsCacheBean> marketTreeSet = (TreeSet<WLCOppsCacheBean>) marketsOpportunities.get(Long.valueOf(arr[WLCOppsCacheBean.MARKETID_FIELD]));
                    	if (null == marketTreeSet) {
                    		marketTreeSet = new TreeSet<WLCOppsCacheBean>(new Comparator<WLCOppsCacheBean>() {
                    			@Override
                    			public int compare(WLCOppsCacheBean opp1, WLCOppsCacheBean opp2) {
                    				if (opp1.getOpportunityId() == opp2.getOpportunityId()) {
                    					return 0;
                    				}
                    	            return opp1.getTimeEstClosing().after(opp2.getTimeEstClosing()) ? 1 : -1;
                    	        }
							});
                    		marketsOpportunities.put(Long.valueOf(arr[WLCOppsCacheBean.MARKETID_FIELD]), marketTreeSet);
                		}
                    	if (command.equals(ConstantsBase.JMS_OPPS_UPDATE_COMMAND_ADD)) {
                    		WLCOppsCacheBean opp = new WLCOppsCacheBean(msg);
                    		if (!marketTreeSet.add(opp)) {
                    			Iterator<WLCOppsCacheBean> iterator = marketTreeSet.iterator();
                    			while(iterator.hasNext()) {
                    				WLCOppsCacheBean cacheOpp = iterator.next();
                    				if (cacheOpp.getOpportunityId() == opp.getOpportunityId()) {
                    					cacheOpp.setState(opp.getState());
                    					break;
                    				}
                    			}
                    		}
                    	} if (command.equals(ConstantsBase.JMS_OPPS_UPDATE_COMMAND_DELETE)) {
                    		for (Iterator<WLCOppsCacheBean> iterator = marketTreeSet.iterator(); iterator.hasNext();) {
								WLCOppsCacheBean wlcOppsCacheBean = (WLCOppsCacheBean) iterator.next();
								if (wlcOppsCacheBean.getOpportunityId() == Long.valueOf(arr[WLCOppsCacheBean.OPPORTUNITYID_FIELD])) {
									marketTreeSet.remove(wlcOppsCacheBean);
									break;
								}
							}
                    	}
                    } else {
                        if (log.isDebugEnabled()) {
                            log.debug("Got message: " + message);
                        }
                    }
                    /* !!! WARNING !! METHOD EXIT POITN !!! */
                    return;
                } else {
                    if (log.isDebugEnabled()) {
                        log.debug("Got message: " + message);
                    }
                    return;
                }
                if (log.isDebugEnabled()) {
                    log.debug("Valid message");
                }
            } else {
                if (log.isDebugEnabled()) {
                    log.debug("Got message: " + message);
                }
                return;
            }
        } catch (ClassCastException jmse) {
            //if message isn't an ObjectMessage or message.getObject() isn't a ETraderFeedMessage
            //then this update is not "correct"
            log.warn("Invalid message - (no ETraderFeedMessage instance)");
            return;
        } catch (JMSException jmse) {
            log.error("StockQuotesJMSAdapter.onMessage - JMSException: " + jmse.getMessage(), jmse);
            return;
        }
        if (log.isTraceEnabled()) {
            log.trace("Message: " + feedMsg);
        }
        try {
            WebLevelLookupBean wllb = null;
            for (Iterator<WebLevelLookupBean> i = requests.iterator(); i.hasNext();) {
                wllb = i.next();
                if (wllb.getOppId() == feedMsg.oppId) {
                    i.remove();
                    wllb.setDevCheckLevel(feedMsg.devCheckLevel);
                    wllb.setRealLevel(feedMsg.realLevel);
                    wllb.setAutoDisable(feedMsg.isAutoDisable);
                    wllb.setBid(feedMsg.bid);
                    wllb.setOffer(feedMsg.offer);
                    wllb.setInvestmentsRejectInfo(feedMsg.investmentsRejectInfo);
                    wllb.setGroupClose(Integer.parseInt(feedMsg.currentValues.get("ET_GROUP_CLOSE")));
                    synchronized (wllb) {
                        wllb.notify();
                    }
                }
            }
        } catch (Throwable t) {
            log.error("Failed to process level update.", t);
        }
    }

    /**
     * Parses the JMS message that carries data about the top markets by groups
     * for a skin.
     *
     * @param msg
     */
    private void parseTopMarketsByGroupMsg(String msg) {
    	String[] arr = msg.split("_");
    	long skinId = Long.parseLong(arr[1]);
    	if (arr.length > 2) {
    		Map<Long, Set<Long>> marketsByGroup = new HashMap<Long, Set<Long>>();
    		Set<Long> markets = null;
    		String [] arrMarkets = null;
    		for (int idx = 2; idx < arr.length; idx++) {
    			String [] groupMarkets = arr[idx].split("\\$");
    			markets = new HashSet<Long>();
    			marketsByGroup.put(Long.parseLong(groupMarkets[0]), markets);
    			arrMarkets = groupMarkets[1].split("\\|");
    			for (String strMarketId : arrMarkets) {
					markets.add(Long.parseLong(strMarketId));
				}
    		}
    		skinMarketsByGroup.put(skinId, marketsByGroup);
    	} else {
    		skinMarketsByGroup.remove(skinId);
    	}
    }

    @Override
	public void getSnapshot() {
        sender.send("homepages");
    }

    public SortedSet<WLCOppsCacheBean> getMarketOpportunities(long marketId) {
    	return marketsOpportunities.get(marketId);
    }
}