package com.anyoption.common.jms;

import javax.jms.JMSException;
import javax.naming.NamingException;

import org.apache.log4j.Logger;

/**
 * Loop until connected to JMS provider.
 */
public abstract class ConnectionLoop extends Thread {
    private static final Logger log = Logger.getLogger(ConnectionLoop.class);
    
	protected JMSHandler jmsHandler;
	private int recoveryPause;
	
	private int localPhase;
	private static volatile int phase;
	private boolean abort;
    protected String threadName;
	
	public ConnectionLoop(JMSHandler jmsHandler, int recoveryPause) {
		this.jmsHandler = jmsHandler;
		this.recoveryPause = recoveryPause;
		this.localPhase = ++phase;
        this.abort = false;
        this.threadName = "Connection";
	}
	
	public void run() {
        Thread.currentThread().setName(threadName);
        if (log.isInfoEnabled()) {
            log.info("ConnectionLoop starting...");
        }
		boolean loop = false;
		do {
			if (this.localPhase != phase) {
				return;
			}
			loop = false;
			//reset JMSHandler
//			jmsHandler.reset();
            reset();
			try {
				//call the concrete method
				connectionCall();
			} catch(JMSException je) {
				//JMS not yet available
				//problems on connecting to JMS. We keep on trying 
				//to reach JMS while the Server goes on 
				log.error("JMSException: " + je.getMessage(), je);
				loop = true;
			} catch (NamingException ne) {
				//at least one name is wrong. We keep on trying, in the case
				//that the wrong name will subsequently become valid
				log.error("NamingException: " + ne.getMessage(), ne);
				loop = true;
			} 
			
			if (loop && this.localPhase == phase) {
				try {
					Thread.sleep(recoveryPause);
				} catch (InterruptedException e) {
				}
			} else if (this.localPhase == phase) {
				//handle connection/reconnection 
				onConnectionCall();
			} else {
				//another thread started
				return;
			}
			synchronized (this) {
                if (abort) {
                    break;
                }
            }
		} while (loop);
        if (log.isInfoEnabled()) {
            log.info("ConnectionLoop finished.");
        }
	}
    
    protected void reset() {
        jmsHandler.reset();
    }
	
    public void abort() {
        synchronized (this) {
            abort = true;
        }
    }
    
	/** 
	 * this method will be implemented by subclasses. Each subclass
	 * will call its onConnection handler
	 */
	protected abstract void onConnectionCall();
	
	/**
	 * this method will be implemented by subclasses. Each subclass
	 * will init its needed QueueSender/QueueReceiver/TopicPublisher/TopicSubscriber
	 */
	protected abstract void connectionCall() throws JMSException, NamingException;
}