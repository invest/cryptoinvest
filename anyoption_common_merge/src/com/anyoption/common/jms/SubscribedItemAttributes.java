package com.anyoption.common.jms;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import com.anyoption.common.beans.Opportunity;
import com.anyoption.common.beans.OpportunitySkinGroupMap;
import com.anyoption.common.enums.SkinGroup;

public class SubscribedItemAttributes {
    public String oppId;
    public HashMap<Integer, Boolean> home;
    public HashMap<String, String> crrState;
    public double level;
    public long groupId;
    public double realLevel;
    public double devCheckLevel;
    public HashMap<Integer, HashMap<String, Integer>> skinsPriorities;
    public long marketId;
    public int scheduled;
//  for tt
	public double autoShiftParameter;
	public double calls;
	public double puts;
	public String feedName;
	public long decimalPoint;
	public boolean isTraderDisable;
	public boolean isAutoDisable;
	public double contractsBought;
	public double contractsSold;
	public Object parameters;
	public long opportunityTypeId;
	public boolean notClosed;
	public Map<SkinGroup, OpportunitySkinGroupMap> skinGroupMapping;

	public SubscribedItemAttributes(
			String oppId,
			HashMap<Integer, Boolean> home,
			HashMap<String, String> crrState,
			double level,
			long groupId,
			double realLevel,
			double devCheckLevel,
			HashMap<Integer, HashMap<String, Integer>> skinsPriorities,
			double autoShiftParameter,
    		double calls,
    		double puts,
    		String feedName,
    		long decimalPoint,
    		boolean isTraderDisable,
    		boolean isAutoDisable,
    		double contractsBought,
    		double contractsSold,
    		Object parameters,
    		boolean notClosed,
    		Map<SkinGroup, OpportunitySkinGroupMap> skinGroupMapping) {
        this.oppId = oppId;
//        this.home = new HashMap<Integer, Boolean>();
        this.home = home;
        this.crrState = crrState;
        this.level = level;
        this.groupId = groupId;
        this.realLevel = realLevel;
        this.devCheckLevel = devCheckLevel;
        this.skinsPriorities = skinsPriorities;
        this.autoShiftParameter = autoShiftParameter;
        this.calls = calls;
        this.puts = puts;
        this.feedName = feedName;
        this.decimalPoint = decimalPoint;
        this.isTraderDisable = isTraderDisable;
        this.isAutoDisable = isAutoDisable;
        this.contractsBought = contractsBought;
        this.contractsSold = contractsSold;
        this.parameters = parameters;
        this.notClosed = notClosed;
        this.skinGroupMapping = skinGroupMapping;

        // this two wont change over the opportunity life so it is safe to take
        // them only once
        opportunityTypeId = 1; //default is binary
        try {
            opportunityTypeId = Long.parseLong(crrState.get("AO_TYPE"));
		} catch (NumberFormatException e) {
		}

		String marketSubscibeName = "ET_MARKET";
		if (opportunityTypeId == Opportunity.TYPE_BINARY_0_100_ABOVE || opportunityTypeId == Opportunity.TYPE_BINARY_0_100_BELOW || opportunityTypeId == Opportunity.TYPE_DYNAMICS) {
			marketSubscibeName = "BZ_MARKET_ID";
			scheduled = 1;
		} else {
			try {
	            scheduled = Integer.parseInt(crrState.get("ET_SCHEDULED"));
	        } catch (Throwable t) {
	            // do nothing
	        }
		}

        try {
            marketId = Long.parseLong(crrState.get(marketSubscibeName));
        } catch (Throwable t) {
            // do nothing
        }

	}

    public String toString() {
        String ls = System.getProperty("line.separator");
        StringBuffer sb = new StringBuffer();
        sb.append(ls).append("SubscribedItemAttributes: ").append(ls);
        sb.append("oppId: ").append(oppId).append(ls);
        sb.append("home: ").append(null != home ? home.toString() : "null").append(ls);
        sb.append("level: ").append(level).append(ls);
        sb.append("realLevel: ").append(realLevel).append(ls);
        sb.append("devCheckLevel: ").append(devCheckLevel).append(ls);
        sb.append("groupId: ").append(groupId).append(ls);
        String key = null;
        for (Iterator<String> i = crrState.keySet().iterator(); i.hasNext();) {
            key = i.next();
            sb.append(key).append(": ").append(crrState.get(key)).append(ls);
        }
        sb.append("groupId: ").append(skinsPriorities.toString()).append(ls);
        sb.append("autoShiftParameter: ").append(autoShiftParameter).append(ls);
        sb.append("calls: ").append(calls).append(ls);
        sb.append("puts: ").append(puts).append(ls);
        sb.append("feedName: ").append(feedName).append(ls);
        sb.append("decimalPoint: ").append(decimalPoint).append(ls);
        sb.append("isTraderDisable: ").append(isTraderDisable).append(ls);
        sb.append("isAutoDisable: ").append(isAutoDisable).append(ls);
        sb.append("contractsBought: ").append(contractsBought).append(ls);
        sb.append("contractsSold: ").append(contractsSold).append(ls);
        sb.append("parameters: ").append(null != parameters ? parameters.toString() : "null").append(ls);
        sb.append("notClosed: ").append(notClosed).append(ls);
        sb.append("skinGroupMapping: ").append(skinGroupMapping).append(ls);
        return sb.toString();
    }
}