package com.anyoption.common.jms;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Hashtable;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.ObjectMessage;

import org.apache.log4j.Logger;

import com.anyoption.common.enums.SkinGroup;
import com.anyoption.common.jms.ifc.LevelsCacheListener;

public class BackendLevelsCache extends LevelsCache {
    private static final Logger log = Logger.getLogger(BackendLevelsCache.class);

    private long lifeId;
    private Object requestSequenceLock;
    private long requestSequence;
    private WebLevelLookupBean wllb;

    private Hashtable<String, RequestBean> requests = null;

    public BackendLevelsCache(String initialContextFactory, String providerURL, String connectionFactoryName, String queueName, String topicName, int msgPoolSize, int recoveryPause, LevelsCacheListener listener) {
        super(initialContextFactory, providerURL, connectionFactoryName, queueName, topicName, msgPoolSize, recoveryPause, listener);

        // not the best thing to do but unless we start two backends in the same millis it should do
        lifeId = System.currentTimeMillis();
        requestSequenceLock = new Object();
        requestSequence = 1;

        requests = new Hashtable<String, RequestBean>();
    }

    public void getLevels(WebLevelLookupBean wllb) {
    	this.wllb = wllb;
        sender.send("level_" + wllb.getFeedName() + "_" + wllb.getOppId());
        synchronized (wllb) {
            try {
                wllb.wait(3000);
            } catch (InterruptedException ie) {
                // do nothing
            }
        }
    }

    /**
     * Notify service for change in market configuration.
     *
     * @param feedName the feed name of the market (.TA25 for example)
     * @param updatesPause min pause in milliseconds between updates sent to JMS
     * @param significantPercentage percent of change under which update will be discarded (eg: 1% is 0.01)
     * @param realUpdatesPause min pause in milliseconds between updates sent to JMS for real level
     * @param realSignificantPercentage percent of change under which real updates will be discarded (eg: 1% is 0.01)
     * @param randomCeiling up boundary of random change for the current level (eg: 0.025% is 0.00025)
     * @param randomFloor down boundary of random change for the current level (eg: -0.025% is -0.00025)
     * @param firstShiftParameter what is the first shift size in percentage (eg: 0.02 for 2%)
     * @param percentageOfAvgInvestment what is the percentage of the average_investments.
     *      in case |sum of call - sum of puts| >  percentage_of_average_investments * 100 * average_investments
     *      -> we will shift
     *      (eg: 0.1 for 10%)
     * @param fixedAmountForShifting what is the amount incase
     *      |sum of call � sum of puts| > fixed_amount_for_shifting -> we will shift
     *      (full currency units)
     * @param typeOfShifting what type of shifting to use:
     *      -) <code>Market.TYPE_OF_SHIFTING_FIXED_AMOUNT</code> - only
     *          fixed_amount_for_shifting (no percentage)
     *      -) <code>Market.TYPE_OF_SHIFTING_AVERAGE_INVESTMENT</code> -
     *          only average_investments * percentage_of_average_investments * 100
     *      -) <code>Market.TYPE_OF_SHIFTING_MAX_OF_BOTH</code> -
     *          the highest from both
     * @param disableAfterPeriod period in millis after which the market opps will be disabled. 0 to turn this monitoring off.
     * @return <code>true</code> if successfull else <code>false</code>
     */
    @Override
    public boolean notifyForMarketCfgChange(
            String feedName,
            int updatesPause,
            BigDecimal significantPercentage,
            int realUpdatesPause,
            BigDecimal realSignificantPercentage,
            BigDecimal randomCeiling,
            BigDecimal randomFloor,
            BigDecimal firstShiftParameter,
            BigDecimal percentageOfAvgInvestment,
            BigDecimal fixedAmountForShifting,
            int typeOfShifting,
            long disableAfterPeriod,
            String quoteParams) {
        return makeRequest(
                "marketcfg_" +
                feedName + "_" +
                updatesPause + "_" +
                significantPercentage + "_" +
                realUpdatesPause + "_" +
                realSignificantPercentage + "_" +
                randomCeiling + "_" +
                randomFloor + "_" +
                firstShiftParameter + "_" +
                percentageOfAvgInvestment + "_" +
                fixedAmountForShifting + "_" +
                typeOfShifting + "_" +
                disableAfterPeriod + "_" +
                quoteParams);
    }

    /**
     * Notify LevelService about TA25 parameter change.
     *
     * @param ta25Parameter the new parameter value
     * @return <code>true</code> if successfull else <code>false</code>
     */
    @Override
    public boolean notifyForTA25ParameterChange(BigDecimal ta25Parameter) {
        return makeRequest("ta25_" + ta25Parameter);
    }

    /**
     * Notify LevelService about FTSE parameter change.
     *
     * @param ftseParameter the new parameter value
     * @return <code>true</code> if successfull else <code>false</code>
     */
    @Override
    public boolean notifyForFTSEParameterChange(BigDecimal ftseParameter) {
        return makeRequest("ftse_" + ftseParameter);
    }

    /**
     * Notify LevelService about CAC parameter change.
     *
     * @param cacParameter the new parameter value
     * @return <code>true</code> if successfull else <code>false</code>
     */
    @Override
    public boolean notifyForCACParameterChange(BigDecimal cacParameter) {
        return makeRequest("cac_" + cacParameter);
    }

    /**
     * Notify LevelService about SP500 parameter change.
     *
     * @param SP500Parameter the new parameter value
     * @return <code>true</code> if successfull else <code>false</code>
     */
    @Override
    public boolean notifyForSP500ParameterChange(BigDecimal SP500Parameter) {
        return makeRequest("sp500_" + SP500Parameter);
    }

    /**
     * Notify LevelService about KLSE parameter change.
     *
     * @param klseParameter the new parameter value
     * @return <code>true</code> if successfull else <code>false</code>
     */
    public boolean notifyForKLSEParameterChange(BigDecimal klseParameter) {
        return makeRequest("klse_" + klseParameter);
    }

    /**
     * Notify LevelService about opportunity odds change.
     *
     * @param oppId the id of the opp which odds have been changed
     * @return <code>true</code> if successfull else <code>false</code>
     */
    @Override
    public boolean notifyForOpportunityOddsChange(long oppId) {
        return makeRequest("odds_" + oppId);
    }

    /**
     * Notify LevelService about opportunity shift parameter change.
     *
     * @param oppId the id of the opp which shift parameter have been changed
     * @param shiftParameterET the new value of the opp shift parameter
     *      (in % - the current level is multiplied by (1 + shiftParameter))
     *      (eg. pass 0.01 for 1%)
     * @param shiftParameterAO the new value of the opp shift parameter
     *      (in % - the current level is multiplied by (1 + shiftParameter))
     *      (eg. pass 0.01 for 1%)
     * @param spreadType spread type 0 for both 1 etrader 2 anyoption
     * @return <code>true</code> if successfull else <code>false</code>
     */
    public boolean notifyForOpportunityShiftParameterChange(long oppId, ArrayList<BigDecimal> shiftParameters) {
    	String msg = "shift_" + oppId ;
    	for(BigDecimal shift: shiftParameters){
    		msg += "_" + shift.toPlainString();
    	}
        return makeRequest(msg);
    }

    /**
     * Manually close opportunity.
     *
     * @param oppId the id of the opportunity to close
     * @param closingLevel the closing level for the opportunity
     * @return <code>true</code> if successfull else <code>false</code>
     */
    @Override
    public boolean manuallyCloseOpportunity(long oppId, BigDecimal closingLevel) {
        return makeRequest("close_" + oppId + "_" + closingLevel);
    }

    /**
     * Suspend/unsuspend marekt.
     *
     * @param feedName the feed name of the market to suspend/unsuspend
     * @param suspend <code>true</code> to suspend <code>false</code> to unsuspend
     * @param message the message to put on the suspended market on the trading pages
     * @return <code>true</code> if successfull else <code>false</code>
     */
    @Override
    public boolean suspendMarket(String feedName, boolean suspend, String message) {
        return makeRequest("suspend_" + feedName + "_" + suspend + "_" + (null == message || message.equals("") ? " " : message));
    }

    /**
     * notify service to reload this market ric bcoz we change option expiration
     * @param marketId the market id to reload
     * @return <code>true</code> if successfull else <code>false</code>
     */
    public boolean notifyForOptionExpirationChange(Long marketId) {
        return makeRequest("optionExpiration_" + marketId);
    }

    /**
     * Manually update max exposure.
     *
     * @param oppId the id of the opportunity to update
     * @param newExposure the new exposure value
     * @return <code>true</code> if successfull else <code>false</code>
     */
    public boolean updateMaxExposureAllSkinGroups(long oppId, int maxExposure) {
    	String msg = "updateMaxExposure_" + oppId ;
    	for(SkinGroup sg: SkinGroup.NON_AUTO_SET){
    		msg += "_"+maxExposure;
    	}
        return makeRequest(msg);
    }

    /**
     * Manually update Trader Disable.
     *
     * @param oppId the id of the opportunity to update
     * @param isDisabled the disable value
     * @return <code>true</code> if successfull else <code>false</code>
     */
    public boolean updateTraderDisable(long oppId, boolean isDisabled) {
        return makeRequest("updateTraderDisable_" + oppId + "_" + isDisabled);
    }

    protected RequestBean getRequestBean() {
        RequestBean rb = new RequestBean();
        synchronized (requestSequenceLock) {
            rb.requestId = lifeId + "-" + requestSequence;
            requestSequence++;
        }
        return rb;
    }

    public boolean updateMarketPriority() {
    	return makeRequest("updateMarketPriority");
    }
    
    protected boolean makeRequest(String request) {
        RequestBean req = getRequestBean();
        requests.put(req.requestId, req);
        sender.send(request + "_" + req.requestId);
        synchronized (req) {
            try {
                req.wait(5000);
            } catch (InterruptedException ie) {
                // do nothing
            }
        }
        if (null == req.response) {
            sender.cancel(request + "_" + req.requestId);
        }
        boolean res = null != req.response && req.response.equals("ok");
        if (log.isDebugEnabled()) {
            log.debug("Request: " + req.requestId + " result: " + res);
        }
        return res;
    }

    @Override
    public void onMessage(Message message) {
        if (null == message) {
            log.warn("No message");
            return;
        }
        if (log.isDebugEnabled()) {
            log.debug("Received message");
        }
        ETraderFeedMessage feedMsg = null;
        try {
            if (message instanceof ObjectMessage) {
                Object obj = ((ObjectMessage) message).getObject();
                if (obj instanceof String) {
                    String msg = (String) obj;
                    if (log.isInfoEnabled()) {
                        log.info("Received: " + msg);
                    }
                    if (msg.startsWith("cmdresp")) {
                        String[] msga = msg.split("_");
                        String reqId = msga[1];
                        RequestBean rb = requests.get(reqId);
                        if (null != rb) {
                            rb.response = msga[2];
                            synchronized (rb) {
                                rb.notify();
                            }
                        } else {
                            log.warn("Can't find the request bean for: " + reqId);
                        }
                    }
                } if (obj instanceof ETraderFeedMessage) {
                    feedMsg = (ETraderFeedMessage) obj;
                } else {
                    log.trace("Not processing message: " + message);
                }
            } else if (log.isTraceEnabled()) {
                log.trace("Not processing message: " + message);
            }
        } catch (JMSException jmse) {
            log.error("Problem processing message.", jmse);
        }

        try {
           if (null != wllb && null != feedMsg) {
        	   if (wllb.getOppId() == feedMsg.oppId) {
	                wllb.setDevCheckLevel(feedMsg.devCheckLevel);
	                wllb.setRealLevel(feedMsg.realLevel);
	                synchronized (wllb) {
	                    wllb.notify();
	                }
        	   }
        	   wllb = null;
           }
        } catch (Throwable t) {
            log.error("Failed to process level update.", t);
        }

    }

    @Override
    public void getSnapshot() {
        // do nothing
    }

    class RequestBean {
        String requestId;
        String response;
    }

    /**
     * Notify LevelService about BanksBursa parameter change.
     *
     * @param banksBursaParameter the new parameter value
     * @return <code>true</code> if successfull else <code>false</code>
     */
	public boolean notifyForBanksBursaParameterChange(BigDecimal banksBursa) {
		return makeRequest("banksBursa_" + banksBursa);
	}

	/**
     * Notify LevelService about d4Banks parameter change.
     *
     * @param d4BanksParameter the new parameter value
     * @return <code>true</code> if successfull else <code>false</code>
     */
	public boolean notifyForD4BanksParameterChange(BigDecimal d4Banks) {
		return makeRequest("d4Banks_" + d4Banks);
	}

	/**
     * Notify LevelService about d4Spread parameter change.
     *
     * @param d4SpreadParameter the new parameter value
     * @return <code>true</code> if successfull else <code>false</code>
     */
	public boolean notifyForD4SpreadParameterChange(BigDecimal d4Spread) {
		return makeRequest("d4Spread_" + d4Spread);
	}

//	public boolean updateBinary0100opp(long oppId, String paramValue) {
//		return makeRequest("binary0100opp_" + oppId + "_" + paramValue);
//	}

	public boolean updateDynamicsQuoteParams(long oppId, String paramValue) {
		return makeRequest("dynamicsOpportunityQuoteParams_" + oppId + "_" + paramValue);
	}

//	public boolean binary0100market(String feedName, String paramValue) {
//		return makeRequest("binary0100market_" + feedName + "_" + paramValue);
//	}

	public boolean updateDynamicsMarketQuoteParams(String feedName, String paramValue) {
		return makeRequest("dynamicsMarketQuoteParams_" + feedName + "_" + paramValue);
	}
	
	/**
	 * @param oppId		The given Opportunity ID
	 * @param paramValue	int value of 1 or 0 if suspended or unsuspended
	 * @return 		<code>true</code> if successful Suspend Opportunity Communicated to LS else <code>false</code>
	 */
	public boolean notifyForSuspOpp(long oppId, String feedName, int paramValue) {
		return makeRequest("opportunitySuspend_" + feedName + "_" + oppId + "_" + paramValue);
	}

	public boolean notifyForILSInterestParameterChange(BigDecimal paramValue) {
		return makeRequest("ILS1MD_" + paramValue);
	}
}