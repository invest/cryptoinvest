package com.anyoption.common.payments;

import com.anyoption.common.clearing.ClearingInfo;

/**
 * @author liors
 *
 */
public class CancelDeposit {
	private ClearingInfo clearingInfo;
	private int status;
	private int writerId;
	private String userUtcOffset;
	
	@Override
	public String toString() {
		return "CancelDeposit [clearingInfo=" + clearingInfo + ", status="
				+ status + ", writerId=" + writerId + ", userUtcOffset="
				+ userUtcOffset + "]";
	}

	/**
	 * @return the clearingInfo
	 */
	public ClearingInfo getClearingInfo() {
		return clearingInfo;
	}
	
	/**
	 * @param clearingInfo the clearingInfo to set
	 */
	public void setClearingInfo(ClearingInfo clearingInfo) {
		this.clearingInfo = clearingInfo;
	}
	
	/**
	 * @return the status
	 */
	public int getStatus() {
		return status;
	}
	
	/**
	 * @param status the status to set
	 */
	public void setStatus(int status) {
		this.status = status;
	}
	
	/**
	 * @return the writerId
	 */
	public int getWriterId() {
		return writerId;
	}
	
	/**
	 * @param writerId the writerId to set
	 */
	public void setWriterId(int writerId) {
		this.writerId = writerId;
	}
	
	/**
	 * @return the userUtcOffset
	 */
	public String getUserUtcOffset() {
		return userUtcOffset;
	}
	
	/**
	 * @param userUtcOffset the userUtcOffset to set
	 */
	public void setUserUtcOffset(String userUtcOffset) {
		this.userUtcOffset = userUtcOffset;
	}
}
