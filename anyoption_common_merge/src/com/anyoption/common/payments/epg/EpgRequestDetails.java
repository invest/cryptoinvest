package com.anyoption.common.payments.epg;

/**
 * @author liors
 *
 * https://anyoption.atlassian.net/wiki/pages/viewpage.action?pageId=88604681
 */
public class EpgRequestDetails {
	public String balance;
	public String taxBalance;
	//public String currencyId;
	//public String firstName;
	//public String lastName;
	//public String street;
	//public String userCityName;
	//public String zipCode;
	//public String timeCreated; //registrationDate
	public String isActive;
	public String platform; //(CO\AO\ET) - brand
	//public String skinId; //TODO skin id?
	//public String currency; //TODO id?
	//public String userId;
	//public String platformId; //(web\ mobile) writer?
	//public String country;
	public String registrationDate; //time created
	public String registrationIP;
	public String FtdDate;
	public String totalSumDeposit;
	public String successfulDepositCount;
	public String successfulWithdrawalCount;
	public String isControlApproved;
	public String hasActiveRiskIssueInHighPriority; //TODO ??? (active=all statuses, except "closed")
	public String hasBlockedCC;
	public String CampaignNumber;
	public String affiliateKey;
	
	//for CC transaction
	//public String bin;
	public String cardNumber; //encrypted
	//public String cardExpiry;
	//public String transactionIP;
	//public String transactionWriter;
	//public String cardHolderName;	
	public String firstSuccessfulDepositWithSameCC;
	public String isUploadDocumentForThisCC;
	public String isHighestDepositMadeByTheUser;
	
	//make private member if it must not be send on the request.
	
	/**
	 * @return the balance
	 */
	public String getBalance() {
		return balance;
	}

	/**
	 * @param balance the balance to set
	 */
	public void setBalance(String balance) {
		this.balance = balance;
	}

	/**
	 * @return the taxBalance
	 */
	public String getTaxBalance() {
		return taxBalance;
	}

	/**
	 * @param taxBalance the taxBalance to set
	 */
	public void setTaxBalance(String taxBalance) {
		this.taxBalance = taxBalance;
	}

	/*/**
	 * @return the currencyId
	 *//*
	public String getCurrencyId() {
		return currencyId;
	}

	*//**
	 * @param currencyId the currencyId to set
	 *//*
	public void setCurrencyId(String currencyId) {
		this.currencyId = currencyId;
	}*/

	/**
	 * @return the firstName
	 *//*
	public String getFirstName() {
		return firstName;
	}

	*//**
	 * @param firstName the firstName to set
	 *//*
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	*//**
	 * @return the lastName
	 *//*
	public String getLastName() {
		return lastName;
	}

	*//**
	 * @param lastName the lastName to set
	 *//*
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}*/

	/**
	 * @return the street
	 *//*
	public String getStreet() {
		return street;
	}

	*//**
	 * @param street the street to set
	 *//*
	public void setStreet(String street) {
		this.street = street;
	}*/

	/**
	 * @return the zipCode
	 *//*
	public String getZipCode() {
		return zipCode;
	}

	*//**
	 * @param zipCode the zipCode to set
	 *//*
	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}*/

	/**
	 * @return the isActive
	 */
	public String getIsActive() {
		return isActive;
	}

	/**
	 * @param isActive the isActive to set
	 */
	public void setIsActive(String isActive) {
		this.isActive = isActive;
	}


	/**
	 * @return the userId
	 *//*
	public String getUserId() {
		return userId;
	}

	*//**
	 * @param userId the userId to set
	 *//*
	public void setUserId(String userId) {
		this.userId = userId;
	}*/

	/**
	 * @return the country
	 *//*
	public String getCountry() {
		return country;
	}

	*//**
	 * @param country the country to set
	 *//*
	public void setCountry(String country) {
		this.country = country;
	}*/

	/**
	 * @return the registrationDate
	 */
	public String getRegistrationDate() {
		return registrationDate;
	}

	/**
	 * @param registrationDate the registrationDate to set
	 */
	public void setRegistrationDate(String registrationDate) {
		this.registrationDate = registrationDate;
	}

	/**
	 * @return the registrationIP
	 */
	public String getRegistrationIP() {
		return registrationIP;
	}

	/**
	 * @param registrationIP the registrationIP to set
	 */
	public void setRegistrationIP(String registrationIP) {
		this.registrationIP = registrationIP;
	}

	/**
	 * @return the ftdDate
	 */
	public String getFtdDate() {
		return FtdDate;
	}

	/**
	 * @param ftdDate the ftdDate to set
	 */
	public void setFtdDate(String ftdDate) {
		FtdDate = ftdDate;
	}

	/**
	 * @return the totalSumDeposit
	 */
	public String getTotalSumDeposit() {
		return totalSumDeposit;
	}

	/**
	 * @param totalSumDeposit the totalSumDeposit to set
	 */
	public void setTotalSumDeposit(String totalSumDeposit) {
		this.totalSumDeposit = totalSumDeposit;
	}

	/**
	 * @return the successfulDepositCount
	 */
	public String getSuccessfulDepositCount() {
		return successfulDepositCount;
	}

	/**
	 * @param successfulDepositCount the successfulDepositCount to set
	 */
	public void setSuccessfulDepositCount(String successfulDepositCount) {
		this.successfulDepositCount = successfulDepositCount;
	}

	/**
	 * @return the successfulWithdrawalCount
	 */
	public String getSuccessfulWithdrawalCount() {
		return successfulWithdrawalCount;
	}

	/**
	 * @param successfulWithdrawalCount the successfulWithdrawalCount to set
	 */
	public void setSuccessfulWithdrawalCount(String successfulWithdrawalCount) {
		this.successfulWithdrawalCount = successfulWithdrawalCount;
	}

	/**
	 * @return the isControlApproved
	 */
	public String getIsControlApproved() {
		return isControlApproved;
	}

	/**
	 * @param isControlApproved the isControlApproved to set
	 */
	public void setIsControlApproved(String isControlApproved) {
		this.isControlApproved = isControlApproved;
	}

	/**
	 * @return the hasActiveRiskIssueInHighPriority
	 */
	public String getHasActiveRiskIssueInHighPriority() {
		return hasActiveRiskIssueInHighPriority;
	}

	/**
	 * @param hasActiveRiskIssueInHighPriority the hasActiveRiskIssueInHighPriority to set
	 */
	public void setHasActiveRiskIssueInHighPriority(
			String hasActiveRiskIssueInHighPriority) {
		this.hasActiveRiskIssueInHighPriority = hasActiveRiskIssueInHighPriority;
	}

	/**
	 * @return the hasBlockedCC
	 */
	public String getHasBlockedCC() {
		return hasBlockedCC;
	}

	/**
	 * @param hasBlockedCC the hasBlockedCC to set
	 */
	public void setHasBlockedCC(String hasBlockedCC) {
		this.hasBlockedCC = hasBlockedCC;
	}

	/**
	 * @return the campaignNumber
	 */
	public String getCampaignNumber() {
		return CampaignNumber;
	}

	/**
	 * @param campaignNumber the campaignNumber to set
	 */
	public void setCampaignNumber(String campaignNumber) {
		CampaignNumber = campaignNumber;
	}

	/**
	 * @return the affiliateKey
	 */
	public String getAffiliateKey() {
		return affiliateKey;
	}

	/**
	 * @param affiliateKey the affiliateKey to set
	 */
	public void setAffiliateKey(String affiliateKey) {
		this.affiliateKey = affiliateKey;
	}

/*	*//**
	 * @return the bin
	 *//*
	public String getBin() {
		return bin;
	}

	*//**
	 * @param bin the bin to set
	 *//*
	public void setBin(String bin) {
		this.bin = bin;
	}*/

	/**
	 * @return the cardNumber
	 */
	public String getCardNumber() {
		return cardNumber;
	}

	/**
	 * @param cardNumber the cardNumber to set
	 */
	public void setCardNumber(String cardNumber) {
		this.cardNumber = cardNumber;
	}

	/**
	 * @return the cardExpiry
	 *//*
	public String getCardExpiry() {
		return cardExpiry;
	}

	*//**
	 * @param cardExpiry the cardExpiry to set
	 *//*
	public void setCardExpiry(String cardExpiry) {
		this.cardExpiry = cardExpiry;
	}

	*//**
	 * @return the transactionIP
	 *//*
	public String getTransactionIP() {
		return transactionIP;
	}

	*//**
	 * @param transactionIP the transactionIP to set
	 *//*
	public void setTransactionIP(String transactionIP) {
		this.transactionIP = transactionIP;
	}

	*//**
	 * @return the cardHolderName
	 *//*
	public String getCardHolderName() {
		return cardHolderName;
	}

	*//**
	 * @param cardHolderName the cardHolderName to set
	 *//*
	public void setCardHolderName(String cardHolderName) {
		this.cardHolderName = cardHolderName;
	}
*/
	/**
	 * @return the firstSuccessfulDepositWithSameCC
	 */
	public String getFirstSuccessfulDepositWithSameCC() {
		return firstSuccessfulDepositWithSameCC;
	}

	/**
	 * @param firstSuccessfulDepositWithSameCC the firstSuccessfulDepositWithSameCC to set
	 */
	public void setFirstSuccessfulDepositWithSameCC(
			String firstSuccessfulDepositWithSameCC) {
		this.firstSuccessfulDepositWithSameCC = firstSuccessfulDepositWithSameCC;
	}

	/**
	 * @return the isUploadDocumentForThisCC
	 */
	public String getIsUploadDocumentForThisCC() {
		return isUploadDocumentForThisCC;
	}

	/**
	 * @param isUploadDocumentForThisCC the isUploadDocumentForThisCC to set
	 */
	public void setIsUploadDocumentForThisCC(String isUploadDocumentForThisCC) {
		this.isUploadDocumentForThisCC = isUploadDocumentForThisCC;
	}

	/**
	 * @return the isHighestDepositMadeByTheUser
	 */
	public String getIsHighestDepositMadeByTheUser() {
		return isHighestDepositMadeByTheUser;
	}

	/**
	 * @param isHighestDepositMadeByTheUser the isHighestDepositMadeByTheUser to set
	 */
	public void setIsHighestDepositMadeByTheUser(
			String isHighestDepositMadeByTheUser) {
		this.isHighestDepositMadeByTheUser = isHighestDepositMadeByTheUser;
	}

	/**
	 * @return the platform
	 */
	public String getPlatform() {
		return platform;
	}

	/**
	 * @param platform the platform to set
	 */
	public void setPlatform(String platform) {
		this.platform = platform;
	}

	/**
	 * @return the transactionWriter
	 *//*
	public String getTransactionWriter() {
		return transactionWriter;
	}

	*//**
	 * @param transactionWriter the transactionWriter to set
	 *//*
	public void setTransactionWriter(String transactionWriter) {
		this.transactionWriter = transactionWriter;
	}*/

	/**
	 * @return the userCityName
	 *//*
	public String getUserCityName() {
		return userCityName;
	}

	*//**
	 * @param userCityName the userCityName to set
	 *//*
	public void setUserCityName(String userCityName) {
		this.userCityName = userCityName;
	}	*/
}
