package com.anyoption.common.payments.epg;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author LioR SoLoMoN
 *
 */
@XmlRootElement(namespace = "com.anyoption.common.payments.epg.Response")
public class PaymentDetails {
	private String account;
	
	public String getAccount() {
		return account;
	}
	/**
	 * @param account the account to set
	 */
	public void setAccount(String account) {
		this.account = account;
	}
}
