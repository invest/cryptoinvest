package com.anyoption.common.payments.epg;

import java.util.ArrayList;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author LioR SoLoMoN
 *
 */
@XmlRootElement(name = "PaymentSolutions")
public class PaymentSolution {
	private ArrayList<String> paymentSolutions;

	/**
	 * @return the paymentSolution
	 */
	public ArrayList<String> getPaymentSolution() {
		return paymentSolutions;
	}

	/**
	 * @param paymentSolution the paymentSolution to set
	 */
	@XmlElement
	public void setPaymentSolution(ArrayList<String> paymentSolutions) {
		this.paymentSolutions = paymentSolutions;
	}
}
