package com.anyoption.common.payments.epg;

import java.util.ArrayList;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author LioR SoLoMoN
 *
 */
@XmlRootElement(name = "payfrex-response")
public class Response {
	
	private String message;
	private ArrayList<Operation> operations;
	private ArrayList<Entry> optionalTransactionParams;
	private String status;
	private WorkFlowResponse workFlowResponse;

	/**
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * @param message the message to set
	 */
	@XmlElement
	public void setMessage(String message) {
		this.message = message;
	}

	/**
	 * @return the operations
	 */
	public ArrayList<Operation> getOperations() {
		return operations;
	}

	/**
	 * @param operations the operations to set
	 */
	@XmlElementWrapper(name = "operations")
	@XmlElement(name = "operation")
	public void setOperations(ArrayList<Operation> operations) {
		this.operations = operations;
	}

	/**
	 * @return the optionalTransactionParams
	 */
	public ArrayList<Entry> getOptionalTransactionParams() {
		return optionalTransactionParams;
	}

	/**
	 * @param optionalTransactionParams the optionalTransactionParams to set
	 */
	@XmlElementWrapper(name = "optionalTransactionParams")
	@XmlElement(name = "entry")
	public void setOptionalTransactionParams(ArrayList<Entry> optionalTransactionParams) {
		this.optionalTransactionParams = optionalTransactionParams;
	}

	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @param status the status to set
	 */
	@XmlElement 
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * @return the workFlowResponse
	 */
	public WorkFlowResponse getWorkFlowResponse() {
		return workFlowResponse;
	}

	/**
	 * @param workFlowResponse the workFlowResponse to set
	 */
	@XmlElement
	public void setWorkFlowResponse(WorkFlowResponse workFlowResponse) {
		this.workFlowResponse = workFlowResponse;
	}
}
