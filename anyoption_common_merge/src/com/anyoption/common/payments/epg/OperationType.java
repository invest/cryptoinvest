package com.anyoption.common.payments.epg;

/**
 * @author LioR SoLoMoN
 *
 */
public enum OperationType {
	DEBIT("DEBIT")
	,CREDIT("CREDIT");
	
	private String value;
	
	private OperationType(String value) {
		this.value = value;
	}
	
	public String getValue() {
		return this.value;
	}
}
