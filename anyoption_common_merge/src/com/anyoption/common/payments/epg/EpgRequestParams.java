package com.anyoption.common.payments.epg;

/**
 * @author LioR SoLoMoN
 *
 * For credit card and not credit card.
 * Checkout  API & Direct Payout API
 *
 * @param <T>
 */
public class EpgRequestParams<T> {
	public T operationType;
	public T merchantId;	
	public T customerId;
	public T merchantTransactionId;
	public T amount;
	public T country;
	public T currency;
	public T ipAddress;
	public T addressLine1;
	public T addressLine2;
	public T city;
	public T postCode;
	public T telephone;
	public T firstname;
	public T lastname;
	public T cardNumber;
	public T chName;
	public T cvnNumber;
	public T expDate;
	public T paymentSolution;
	public T chAddress1;
	public T chCity;
	public T chPostCode;
	public T chCountry;
	public T successURL;
	public T errorURL;
	public T statusURL;
	public T cancelURL;
	public T cssURL;
	public T skinId;
	public T transactionIP;
	public T bin;
	public T cardHolderName;
	public T productId;	
	public T bankAuth;
	public T dob;
	
	public T merchantParams;//Need to be the last member!
		
	public EpgRequestParams() {
		
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "EpgRequestParams [operationType=" + operationType
				+ ", merchantId=" + merchantId + ", customerId=" + customerId
				+ ", merchantTransactionId=" + merchantTransactionId
				+ ", amount=" + amount + ", country=" + country + ", currency="
				+ currency + ", ipAddress=" + ipAddress + ", addressLine1="
				+ addressLine1 + ", addressLine2=" + addressLine2 + ", city="
				+ city + ", postCode=" + postCode + ", telephone=" + telephone
				+ ", firstname=" + firstname + ", lastname=" + lastname
				+ ", cardNumber=" + cardNumber + ", chName=" + chName
				+ ", cvnNumber=" + cvnNumber + ", expDate=" + expDate
				+ ", paymentSolution=" + paymentSolution + ", chAddress1="
				+ chAddress1 + ", chCity=" + chCity + ", chPostCode="
				+ chPostCode + ", chCountry=" + chCountry + ", successURL="
				+ successURL + ", errorURL=" + errorURL + ", statusURL="
				+ statusURL + ", cancelURL=" + cancelURL + ", cssURL=" + cssURL
				+ ", skinId=" + skinId + ", transactionIP=" + transactionIP
				+ ", bin=" + bin + ", cardHolderName=" + cardHolderName
				+ ", productId=" + productId + ", bankAuth=" + bankAuth
				+ ", dob=" + dob + ", merchantParams=" + merchantParams + "]";
	}

	/**
	 * @return the operationType
	 */
	public T getOperationType() {
		return operationType;
	}

	/**
	 * @param operationType the operationType to set
	 */
	public void setOperationType(T operationType) {
		this.operationType = operationType;
	}

	/**
	 * @return the merchantId
	 */
	public T getMerchantId() {
		return merchantId;
	}

	/**
	 * @param merchantId the merchantId to set
	 */
	public void setMerchantId(T merchantId) {
		this.merchantId = merchantId;
	}

	/**
	 * @return the customerId
	 */
	public T getCustomerId() {
		return customerId;
	}

	/**
	 * @param customerId the customerId to set
	 */
	public void setCustomerId(T customerId) {
		this.customerId = customerId;
	}

	/**
	 * @return the merchantTransactionId
	 */
	public T getMerchantTransactionId() {
		return merchantTransactionId;
	}

	/**
	 * @param merchantTransactionId the merchantTransactionId to set
	 */
	public void setMerchantTransactionId(T merchantTransactionId) {
		this.merchantTransactionId = merchantTransactionId;
	}

	/**
	 * @return the amount
	 */
	public T getAmount() {
		return amount;
	}

	/**
	 * @param amount the amount to set
	 */
	public void setAmount(T amount) {
		this.amount = amount;
	}

	/**
	 * @return the country
	 */
	public T getCountry() {
		return country;
	}

	/**
	 * @param country the country to set
	 */
	public void setCountry(T country) {
		this.country = country;
	}

	/**
	 * @return the currency
	 */
	public T getCurrency() {
		return currency;
	}

	/**
	 * @param currency the currency to set
	 */
	public void setCurrency(T currency) {
		this.currency = currency;
	}

	/**
	 * @return the cardNumber
	 */
	public T getCardNumber() {
		return cardNumber;
	}

	/**
	 * @param cardNumber the cardNumber to set
	 */
	public void setCardNumber(T cardNumber) {
		this.cardNumber = cardNumber;
	}

	/**
	 * @return the chName
	 */
	public T getChName() {
		return chName;
	}

	/**
	 * @param chName the chName to set
	 */
	public void setChName(T chName) {
		this.chName = chName;
	}

	/**
	 * @return the cvnNumber
	 */
	public T getCvnNumber() {
		return cvnNumber;
	}

	/**
	 * @param cvnNumber the cvnNumber to set
	 */
	public void setCvnNumber(T cvnNumber) {
		this.cvnNumber = cvnNumber;
	}

	/**
	 * @return the expDate
	 */
	public T getExpDate() {
		return expDate;
	}

	/**
	 * @param expDate the expDate to set
	 */
	public void setExpDate(T expDate) {
		this.expDate = expDate;
	}

	/**
	 * @return the paymentSolution
	 */
	public T getPaymentSolution() {
		return paymentSolution;
	}

	/**
	 * @param paymentSolution the paymentSolution to set
	 */
	public void setPaymentSolution(T paymentSolution) {
		this.paymentSolution = paymentSolution;
	}

	/**
	 * @return the chAddress1
	 */
	public T getChAddress1() {
		return chAddress1;
	}

	/**
	 * @param chAddress1 the chAddress1 to set
	 */
	public void setChAddress1(T chAddress1) {
		this.chAddress1 = chAddress1;
	}

	/**
	 * @return the chCity
	 */
	public T getChCity() {
		return chCity;
	}

	/**
	 * @param chCity the chCity to set
	 */
	public void setChCity(T chCity) {
		this.chCity = chCity;
	}

	/**
	 * @return the chPostCode
	 */
	public T getChPostCode() {
		return chPostCode;
	}

	/**
	 * @param chPostCode the chPostCode to set
	 */
	public void setChPostCode(T chPostCode) {
		this.chPostCode = chPostCode;
	}

	/**
	 * @return the chCountry
	 */
	public T getChCountry() {
		return chCountry;
	}

	/**
	 * @param chCountry the chCountry to set
	 */
	public void setChCountry(T chCountry) {
		this.chCountry = chCountry;
	}

	/**
	 * @return the successURL
	 */
	public T getSuccessURL() {
		return successURL;
	}

	/**
	 * @param successURL the successURL to set
	 */
	public void setSuccessURL(T successURL) {
		this.successURL = successURL;
	}

	/**
	 * @return the errorURL
	 */
	public T getErrorURL() {
		return errorURL;
	}

	/**
	 * @param errorURL the errorURL to set
	 */
	public void setErrorURL(T errorURL) {
		this.errorURL = errorURL;
	}

	/**
	 * @return the statusURL
	 */
	public T getStatusURL() {
		return statusURL;
	}

	/**
	 * @param statusURL the statusURL to set
	 */
	public void setStatusURL(T statusURL) {
		this.statusURL = statusURL;
	}

	/**
	 * @return the cancelURL
	 */
	public T getCancelURL() {
		return cancelURL;
	}

	/**
	 * @param cancelURL the cancelURL to set
	 */
	public void setCancelURL(T cancelURL) {
		this.cancelURL = cancelURL;
	}

	/**
	 * @return the skinId
	 */
	public T getSkinId() {
		return skinId;
	}

	/**
	 * @param skinId the skinId to set
	 */
	public void setSkinId(T skinId) {
		this.skinId = skinId;
	}

	/**
	 * @return the transactionIP
	 */
	public T getTransactionIP() {
		return transactionIP;
	}

	/**
	 * @param transactionIP the transactionIP to set
	 */
	public void setTransactionIP(T transactionIP) {
		this.transactionIP = transactionIP;
	}

	/**
	 * @return the bin
	 */
	public T getBin() {
		return bin;
	}

	/**
	 * @param bin the bin to set
	 */
	public void setBin(T bin) {
		this.bin = bin;
	}

	/**
	 * @return the cardHolderName
	 */
	public T getCardHolderName() {
		return cardHolderName;
	}

	/**
	 * @param cardHolderName the cardHolderName to set
	 */
	public void setCardHolderName(T cardHolderName) {
		this.cardHolderName = cardHolderName;
	}

	/**
	 * @return the productId
	 */
	public T getProductId() {
		return productId;
	}

	/**
	 * @param productId the productId to set
	 */
	public void setProductId(T productId) {
		this.productId = productId;
	}

	/**
	 * @return the bankAuth
	 */
	public T getBankAuth() {
		return bankAuth;
	}

	/**
	 * @param bankAuth the bankAuth to set
	 */
	public void setBankAuth(T bankAuth) {
		this.bankAuth = bankAuth;
	}

	/**
	 * @return the merchantParams
	 */
	public T getMerchantParams() {
		return merchantParams;
	}

	/**
	 * @param merchantParams the merchantParams to set
	 */
	public void setMerchantParams(T merchantParams) {
		this.merchantParams = merchantParams;
	}

	/**
	 * @return the dob
	 */
	public T getDob() {
		return dob;
	}

	/**
	 * @param dob the dob to set
	 */
	public void setDob(T dob) {
		this.dob = dob;
	}

	/**
	 * @return the ipAddress
	 */
	public T getIpAddress() {
		return ipAddress;
	}

	/**
	 * @param ipAddress the ipAddress to set
	 */
	public void setIpAddress(T ipAddress) {
		this.ipAddress = ipAddress;
	}

	/**
	 * @return the addressLine1
	 */
	public T getAddressLine1() {
		return addressLine1;
	}

	/**
	 * @param addressLine1 the addressLine1 to set
	 */
	public void setAddressLine1(T addressLine1) {
		this.addressLine1 = addressLine1;
	}

	/**
	 * @return the addressLine2
	 */
	public T getAddressLine2() {
		return addressLine2;
	}

	/**
	 * @param addressLine2 the addressLine2 to set
	 */
	public void setAddressLine2(T addressLine2) {
		this.addressLine2 = addressLine2;
	}

	/**
	 * @return the city
	 */
	public T getCity() {
		return city;
	}

	/**
	 * @param city the city to set
	 */
	public void setCity(T city) {
		this.city = city;
	}

	/**
	 * @return the postCode
	 */
	public T getPostCode() {
		return postCode;
	}

	/**
	 * @param postCode the postCode to set
	 */
	public void setPostCode(T postCode) {
		this.postCode = postCode;
	}

	/**
	 * @return the telephone
	 */
	public T getTelephone() {
		return telephone;
	}

	/**
	 * @param telephone the telephone to set
	 */
	public void setTelephone(T telephone) {
		this.telephone = telephone;
	}

	/**
	 * @return the firstname
	 */
	public T getFirstname() {
		return firstname;
	}

	/**
	 * @param firstname the firstname to set
	 */
	public void setFirstname(T firstname) {
		this.firstname = firstname;
	}

	/**
	 * @return the lastname
	 */
	public T getLastname() {
		return lastname;
	}

	/**
	 * @param lastname the lastname to set
	 */
	public void setLastname(T lastname) {
		this.lastname = lastname;
	}

	/**
	 * @return the cssURL
	 */
	public T getCssURL() {
		return cssURL;
	}

	/**
	 * @param cssURL the cssURL to set
	 */
	public void setCssURL(T cssURL) {
		this.cssURL = cssURL;
	}
}
