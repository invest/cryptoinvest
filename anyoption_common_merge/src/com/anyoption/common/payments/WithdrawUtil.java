package com.anyoption.common.payments;

import java.io.Serializable;

import org.apache.log4j.Logger;

import com.anyoption.common.managers.TransactionsManagerBase;
import com.anyoption.common.payments.WithdrawEntryResult.WithdrawResult;
import com.anyoption.common.service.CommonJSONService;
import com.anyoption.common.util.CommonUtil;

/**
 * @author liors
 *
 */
public class WithdrawUtil implements Serializable {
	private static final long serialVersionUID = -2299143990646335638L;
	private static final Logger logger = Logger.getLogger(WithdrawUtil.class);
	private static final String LOG_PREFIX = "WithdrawUtil; ";
	
	public static WithdrawEntryResult preWithdrawEntry(WithdrawEntryInfo info) {
		logger.debug(LOG_PREFIX + " start preWithdrawEntry");
		WithdrawEntryResult withdrawEntryResult = new WithdrawEntryResult();
		try {
			if (!CommonUtil.getConfig("application.source").equalsIgnoreCase("backend") && TransactionsManagerBase.isHasOpenWithdraw(info.getUser().getId())) {
				logger.info("user has open withdraw already; userId: " + info.getUser().getId());
				withdrawEntryResult.getWithdrawResult().add(WithdrawResult.OPEN_WITHDRAWAL);
				withdrawEntryResult.setErrorCode(0);
				withdrawEntryResult.addErrorMessage("", CommonUtil.getMessage(info.getUser().getLocale(), "user.have.open.withdrawal", null));
				withdrawEntryResult.addUserMessage("", CommonUtil.getMessage(info.getUser().getLocale(), "user.have.open.withdrawal", null));
			}
					
			if (CommonUtil.isWithdrawalWeekendsNotAvailable(info.getUser())) {
				logger.info("Withdrawal Weekend Not Available; userId: " + info.getUser().getId());
				withdrawEntryResult.getWithdrawResult().add(WithdrawResult.BLOCK_WEEKENDS_WITHDRAWALS);
				withdrawEntryResult.setErrorCode(1);
				withdrawEntryResult.addErrorMessage("", CommonUtil.getMessage(info.getUser().getLocale(), "unknown.error.contact.support", null));
				withdrawEntryResult.addUserMessage("", CommonUtil.getMessage(info.getUser().getLocale(), "unknown.error.contact.support", null));
			}
			
			//add authentications/validations
		} catch (Exception e) {
			withdrawEntryResult.getWithdrawResult().add(WithdrawResult.UNKNOWN_ERROR);
			withdrawEntryResult.setErrorCode(CommonJSONService.ERROR_CODE_UNKNOWN);
			logger.error(LOG_PREFIX  + "ERROR! problem in preWithdrawEntry", e);
		}
		
		logger.debug(LOG_PREFIX + " end preWithdrawEntry");
		return withdrawEntryResult;
	}
	
	//TODO unit tests.
	public static WithdrawEntryResult postWithdrawEntry(WithdrawEntryInfo info) {
		logger.info(LOG_PREFIX + "start postWithdrawEntry");
		//TODO implement.		
		return null;	
	}
	
	//TODO unit tests.
	public static WithdrawEntryResult finalWithdrawEntry(WithdrawEntryInfo info) {
		logger.info(LOG_PREFIX + "start finalWithdrawEntry");
		//TODO implement.	
		
		//TODO !!!Highly critical!!!
		//TODO make update only if it's the first time for this transaction to invoke this method!!!!
		//TODO This method could be invoked more than one for each transaction!!! make sure it's the first time. balance history... 
		
		return null;	
	}
}
