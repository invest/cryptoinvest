package com.anyoption.common.payments;

import java.io.Serializable;

import com.anyoption.common.beans.User;

public class WithdrawEntryInfo implements Serializable {
	private static final long serialVersionUID = -2421192008159409721L;
	private User user;
	
	public WithdrawEntryInfo(User user) {
		// TODO Auto-generated constructor stub
		this.user = user;
	}
	
	/**
	 * @param user
	 * @param amount
	 */
	public WithdrawEntryInfo(User user, String amount) {
		// TODO Auto-generated constructor stub
		this.user = user;
	}
	
	public WithdrawEntryInfo(User user, long transactionId) {
		// TODO Auto-generated constructor stub
		this.user = user;
	}
	
	public WithdrawEntryInfo(User user, long transactionId, String amount) {
		// TODO Auto-generated constructor stub
		this.user = user;
	}
	
	//TODO add constructors with transaction & clearingInfo.  there is no transaction and clearingInfo class in the common merge 

	public WithdrawEntryInfo(com.anyoption.common.beans.base.User user,
			long transactionId) {
		// TODO Auto-generated constructor stub
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}
}