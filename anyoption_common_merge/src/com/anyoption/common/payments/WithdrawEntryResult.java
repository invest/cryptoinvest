package com.anyoption.common.payments;

import java.io.Serializable;
import java.util.EnumSet;

import com.anyoption.common.service.results.MethodResult;

/**
 * @author liors
 *
 */
public class WithdrawEntryResult extends MethodResult implements Serializable {
	private static final long serialVersionUID = -312233060131282592L;
	private EnumSet<WithdrawResult> withdrawResult;
	
	public WithdrawEntryResult() {
		withdrawResult = EnumSet.noneOf(WithdrawResult.class);
	}

	public enum WithdrawResult {
		OPEN_WITHDRAWAL(0, "user.have.open.withdrawal"), //TODO constants
		BLOCK_WEEKENDS_WITHDRAWALS(1,"unknown.error.contact.support"),
		//add...
		UNKNOWN_ERROR(999, "unknown.error.contact.support");

		private final int code;
		private final String description;
		
		private WithdrawResult(int code, String description) {
			this.code = code;
			this.description = description;
		}
		
		public String getDescription() {
			return description;
		}

		public int getCode() {
			return code;
		}

		@Override
		public String toString() {
			return code + ": " + description;
		}
	}
	
	public EnumSet<WithdrawResult> getWithdrawResult() {
		return withdrawResult;
	}
}
