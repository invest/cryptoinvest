package com.anyoption.common.bonus;

import java.sql.Connection;
import java.sql.SQLException;

import com.anyoption.common.beans.InvestmentBonusData;
import com.anyoption.common.beans.Opportunity;
import com.anyoption.common.beans.User;
import com.anyoption.common.beans.base.BonusUsers;
import com.anyoption.common.bonus.BonusHandlerBase;
import com.anyoption.common.bonus.BonusHandlersException;
import com.anyoption.common.bonus.BonusUtil;
import com.anyoption.common.daos.BonusDAOBase;
import com.anyoption.common.util.ConstantsBase;

public class BonusInvIncReturnAndRefundHandler extends BonusHandlerBase {

	@Override
	public boolean acceptBonus(Connection conn, BonusUsers bu, User user, String ip, long loginId) throws SQLException {
		long stateId = ConstantsBase.BONUS_STATE_ACTIVE;
		if (bu.getSumDeposits() > 0) {
			stateId = ConstantsBase.BONUS_STATE_GRANTED;
		}
		return BonusDAOBase.acceptBonusUser(conn, bu.getId(), stateId);
	}

	/**
	 *  isActivateBonus event handler implementation
	 */
	@Override
	public boolean isActivateBonusAfterTransactionSuccess(Connection conn, BonusUsers bu, long amount, long transactionId) throws BonusHandlersException{

    	long depositsSum = bu.getSumDeposits();

    	// Check if requiered deposits sum > 0
    	if (depositsSum > 0){

    		if (bu.getSumDepositsReached() == 0 && amount >= depositsSum){
    			// Add the amount of current deposit to sum deposits reached
        		try{
        			bu.setSumDepositsReached(amount);
        			BonusDAOBase.addBonusUsersSumDeposits(conn, bu.getId(), amount);
//        			TransactionsDAOBase.setTransactionBonuUsersId(conn, transactionId, bu.getId());
        		}catch (SQLException e) {
    				throw new BonusHandlersException("can't addBonusUsersSumDeposits ", e);
    			}
    		}

        	if (bu.getSumDepositsReached() >= depositsSum){
        		return true;
        	}else{
        		return false;
        	}
    	}

    	return false;
	}

	/**
	 *  activateBonusAfterTransactionSuccess event handler implementation
	 */
	@Override
	public void activateBonusAfterTransactionSuccess(Connection conn, BonusUsers bu, long transactionId, long userId, long amount, long writerId, long loginId) throws BonusHandlersException{
    	try {
    		BonusDAOBase.activateBonusUsers(conn, bu, transactionId, 0);
    	} catch (SQLException sqle) {
            throw new BonusHandlersException("can't activateBonusUsers ",sqle);
        }
	}

	/**
	 *  touchBonusesAfterInvestmentSuccess event handler implementation
	 */
	@Override
	public long touchBonusesAfterInvestmentSuccess(Connection conn, BonusUsers bu, long amountLeft, long opportunityTypeId) throws BonusHandlersException{
		if (opportunityTypeId == Opportunity.TYPE_BUBBLES) { // no touch for bubbles
			return amountLeft;
		}
		try {
			BonusDAOBase.useBonusUsers(conn, bu.getId());
		} catch (SQLException e) {
			throw new BonusHandlersException("Error in touchBonusesAfterInvestmentSuccess ",e);
		}
        return (amountLeft - bu.getBonusAmount());
	}

	/**
	 *  wageringAfterInvestmentSuccess event handler implementation
	 */
	@Override
	public long wageringAfterInvestmentSuccess(Connection conn, BonusUsers bu, long amountLeft, long investmentId) throws BonusHandlersException{
		if (bu.getBonusStateId() != ConstantsBase.BONUS_STATE_ACTIVE){
			amountLeft = super.wageringAfterInvestmentSuccess(conn, bu, amountLeft, investmentId);
		}
		return amountLeft;
	}


	/**
	 *  setBonusStateDescription event handler implementation
	 */
	@Override
    public void setBonusStateDescription(BonusUsers bonusUser) throws BonusHandlersException{
		// Do Nothing
	}

	/**
	 *  activateBonusAfterInvestmentSuccessByInvAmount event handler implementation
	 */
	@Override
	public boolean activateBonusAfterInvestmentSuccessByInvAmount(Connection conn, BonusUsers bu, long amountLeft, long userId, long investmentId, long writerId, boolean isFree, long loginId, long opportunityTypeId) throws BonusHandlersException{
		return false;
	}

	/**
	 *  activateBonusAfterInvestmentSuccessByInvCount event handler implementation
	 */
	@Override
    public boolean activateBonusAfterInvestmentSuccessByInvCount(Connection conn, BonusUsers bu,  boolean isInvWasCountForActivation, long investmentId, long opportunityTypeId) throws BonusHandlersException{
		if (opportunityTypeId == Opportunity.TYPE_BUBBLES) { // no activate for bubbles
			return false;
		}
		return isInvWasCountForActivation;
	}

	/**
	 *  handleBonusOnSettleInvestment event handler implementation
	 */
	@Override
	public long handleBonusOnSettleInvestment(Connection conn, InvestmentBonusData investment, boolean isWin) throws BonusHandlersException{
		long invesAmount = investment.getAmount() - investment.getInsuranceAmountRU();
        long bonusAmount = 0;

        if (isWin) {
            bonusAmount = Math.round(invesAmount * (investment.getBonusWinOdds() - investment.getOddsWin())) ;
        } else {
            bonusAmount = Math.round(invesAmount * (investment.getBonusLoseOdds() - (1 - investment.getOddsLose()))) ;
        }

        return bonusAmount;
	}

	/**
	 *  getAmountThatUserCantWithdraw event handler implementation
	 */
	@Override
	public long getAmountThatUserCantWithdraw(long win, long lose, long invAmount, BonusUsers bu){
		if (win > 0){
			return invAmount;
		}else{
			return Math.round(invAmount * bu.getOddsLose() - (invAmount - lose));
		}
	}
	
	/**
	 *  cancelBonusToUser event handler implementation
	 */
	@Override
    public void cancelBonusToUser(Connection conn, BonusUsers bonusUser, long stateToUpdate, String utcOffset, long writerId, long skinId, String ip, long loginId) throws BonusHandlersException {
		BonusUtil.cancelBonusToUserWithdraw(conn, bonusUser, stateToUpdate, utcOffset, writerId, skinId, ip, loginId);
    }
}