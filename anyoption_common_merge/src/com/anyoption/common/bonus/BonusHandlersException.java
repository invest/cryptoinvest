package com.anyoption.common.bonus;

/**
 * BonusHandlersException class
 * @author Eliran
 *
 */
public class BonusHandlersException extends Exception {

	private static final long serialVersionUID = 485561988152796219L;

	public BonusHandlersException() {
        super();
    }

    public BonusHandlersException(String message) {
        super(message);
    }

    public BonusHandlersException(String message, Throwable t) {
        super(message, t);
    }
}