/**
 *
 */
package com.anyoption.common.bonus;

import org.apache.log4j.Logger;

import com.anyoption.common.util.ConstantsBase;


/**
 * Factory class for bonus handlers
 * @author Eliran
 */
public abstract class BonusHandlerFactory {

	private static final Logger log = Logger.getLogger(BonusHandlerFactory.class);


	 /**
     * Create instance of the right subclass of bonusHandler
     *
     * @param bonusType population typeId - split logically bonusHandler one from another
     * @return
     */
    public static BonusHandlerBase getInstance(long bonusType) {
        if (log.isTraceEnabled()) {
            log.trace("Getting instance for bonusType: " + bonusType);
        }

        if(bonusType == ConstantsBase.BONUS_TYPE_INSTANT_AMOUNT) {
        	return new BonusInstantAmountHandler();
        } else if(bonusType == ConstantsBase.BONUS_TYPE_INSTANT_NEXT_INVEST_ON_US) {
        	return new BonusNextInvOnUsHandler();
        } else if(bonusType == ConstantsBase.BONUS_TYPE_AMOUNT_AFTER_DEPOSIT) {
        	return new BonusAmountAfterDepositHandler();
        } else if(bonusType == ConstantsBase.BONUS_TYPE_PERCENT_AFTER_DEPOSIT) {
        	return new BonusPercentAfterDepositHandler();
        } else if(bonusType == ConstantsBase.BONUS_TYPE_AMOUNT_AFTER_WAGERING) {
        	return new BonusAmountAfterWageringHandler();
        } else if(bonusType == ConstantsBase.BONUS_TYPE_NEXT_INVEST_ON_US_AFTER_WAGERING) {
        	return new BonusNextInvOnUsAfterWageringHandler();
        } else if(bonusType == ConstantsBase.BONUS_TYPE_NEXT_INVEST_ON_US_AFTER_X_INVS) {
        	return new BonusNextInvOnUsAfterXInvsHandler();
        } else if (bonusType == ConstantsBase.BONUS_TYPE_INVESTMENT_INCREASED_RETURN_AND_REFUND) {
        	return new BonusInvIncReturnAndRefundHandler();
        } else if (bonusType == ConstantsBase.BONUS_TYPE_INVESTMENT_INCREASED_RETURN) {
        	return new BonusInvIncReturnOrRefundHandler();
        } else if (bonusType == ConstantsBase.BONUS_TYPE_INVESTMENT_INCREASED_REFUND) {
        	return new BonusInvIncReturnOrRefundHandler();
        } else if (bonusType == ConstantsBase.BONUS_TYPE_PERCENT_AFTER_DEPOSIT_AND_SUM_INVEST) {
        	return new BonusPercentAfterDepositAndSumInvestHandler();
        } else if (bonusType == ConstantsBase.BONUS_TYPE_ROUNDUP) {
        	return new BonusRoundUpHandler();
        } else {
        	// create only instance with relevant type
        	log.error("Error in finding bonus handler for bonus type " + bonusType);
        	return null;
        }
     }
}
