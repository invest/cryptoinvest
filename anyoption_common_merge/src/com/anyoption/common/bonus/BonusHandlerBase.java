package com.anyoption.common.bonus;


import java.sql.Connection;
import java.sql.SQLException;

import com.anyoption.common.beans.Bonus;
import com.anyoption.common.beans.InvestmentBonusData;
import com.anyoption.common.beans.User;
import com.anyoption.common.beans.base.BonusUsers;
import com.anyoption.common.bl_vos.BonusCurrency;
import com.anyoption.common.daos.BonusDAOBase;
import com.anyoption.common.daos.GeneralDAO;
import com.anyoption.common.daos.TransactionsDAOBase;
import com.anyoption.common.daos.UsersDAOBase;
import com.anyoption.common.managers.BonusManagerBase;
import com.anyoption.common.managers.TransactionsManagerBase;
import com.anyoption.common.util.ConstantsBase;

public abstract class BonusHandlerBase {

    /**
     * Process actions before bonus insert through this handler.
     *
     * @param conn - Connection
     * @param bu bonus user
     * @param b bonus
     * @param bc Bonus Currency
     * @param user
     * @param writerId
     * @param popEntryId
     * @param bonusPopLimitTypeId
     */
	public boolean bonusInsert(Connection conn,BonusUsers bu, Bonus b, BonusCurrency bc, long userId, long currencyId, long writerId, long popEntryId, int bonusPopLimitTypeId, boolean isNeedToSendInternalMail) throws BonusHandlersException{
		boolean res = false;
		bu.setBonusStateId(ConstantsBase.BONUS_STATE_PENDING);
		
		try{
			BonusManagerBase.getLimitsForBonus(conn, bu, b, bc, popEntryId, bonusPopLimitTypeId, userId, currencyId, false);

			res = BonusDAOBase.insertBonusUser(conn, bu, b, bc, popEntryId, isNeedToSendInternalMail);

		} catch (SQLException e) {
			throw new BonusHandlersException("Error in inserting bonus" , e);
		}
		return res;
	}

    /**
     * accept bonus and move the bonus to state granted
     * @param conn db connection
     * @param bu bonus user to update
     * @param user user to update
     * @return
     * @throws SQLException 
     */
    public boolean acceptBonus(Connection conn, BonusUsers bu, User user, String ip, long loginId) throws SQLException {	
		return BonusDAOBase.acceptBonusUser(conn, bu.getId(), ConstantsBase.BONUS_STATE_GRANTED);
	}
    
    /**
     * Process bonus message for writers through this handler.
     * @param bc Bonus Currency
     * @param popEnteyId
     * @param bonusPopLimitTypeId
     * @param userId
     * @param bu bonus user
     
    public abstract String getBonusMessage(Bonus b, BonusCurrency bc, long popEnteyId, int bonusPopLimitTypeId, long userId, long bonusAmount, long sumInvQualify, long sumDeposit) throws BonusHandlersException,SQLException;
     */
    
    /**
     * Process a check for bonus activation through this handler.
     *
     * @param conn - Connection
     * @param bu - bonus user
     * @param amount - bonus amount
     * @param transactionId TODO
     */
    public abstract boolean isActivateBonusAfterTransactionSuccess(Connection conn, BonusUsers bu, long amount, long transactionId) throws BonusHandlersException;

    /**
     * Process an activation for bonus through this handler.
     *
     * @param conn - Connection
	 * @param bu - bonus user
	 * @param transactionId
	 * @param userId
	 * @param amount - bonus amount
	 * @param writerId
     */
    public abstract void activateBonusAfterTransactionSuccess(Connection conn, BonusUsers bu, long transactionId, long userId, long amount, long writerId, long loginId) throws BonusHandlersException;

    /**
     * Process used update for bonuses after inv success through this handler.
     *
     * @param conn - Connection
	 * @param bu - bonus user
	 * @param amountLeft - the amount that left after current bonus was used
     * @param opportunityTypeId 
     */
    public abstract long touchBonusesAfterInvestmentSuccess(Connection conn, BonusUsers bu, long amountLeft, long opportunityTypeId) throws BonusHandlersException;

    /**
     * Process update wagering for bonuses after inv success through this handler.
     *
     * @param conn - Connection
     * @param bu - bonus user
     * @param amountLeft - the amount that left before current bonus was used
     * @param investmentId
	 * @return amountLeft  - the amount that left after current bonus was used
     */
    public long wageringAfterInvestmentSuccess(Connection conn, BonusUsers bu, long amountLeft, long investmentId) throws BonusHandlersException{
		try {
			if (bu.getSumInvWithdrawal() - bu.getSumInvWithdrawalReached() <= amountLeft) {
				BonusDAOBase.doneBonusUsers(conn, bu.getId(),true, investmentId);
				return (amountLeft - (bu.getSumInvWithdrawal() - bu.getSumInvWithdrawalReached()));
	        } else {
	            BonusDAOBase.addBonusUserWithdrawWagering(conn, bu.getId(), amountLeft);
	            return 0;
	        }
		} catch (SQLException e) {
			throw new BonusHandlersException("Error in wageringAfterInvestmentSuccess ", e);
		}
    }

    /**
     * cancel bonus to user
     * This method update bonus user state by given stateToUpdate.
     * Override this method when needed.
     *
     * @param conn
     * @param bonusUser
     * @param stateToUpdate
     * @param utcOffset
     * @param writerId
     */
    public void cancelBonusToUser(Connection conn, BonusUsers bonusUser, long stateToUpdate, String utcOffset, long writerId, long skinId, String ip, long loginId) throws BonusHandlersException {
    	try {
	    	BonusManagerBase.updateBonusUser(conn, bonusUser.getId(), stateToUpdate, bonusUser.getWriterIdCancel());
		} catch (SQLException e) {
			throw new BonusHandlersException("Error cancelBonusToUser ",e);
		}
    }

    /**
     * setBonusStateDescription
     * @param bonusUser
     */
    public abstract void setBonusStateDescription(BonusUsers bonusUser) throws BonusHandlersException;

    /**
     * Process activate bonuses after inv success by inv amount through this handler.
     *
     * @param conn - Connection
	 * @param bu - bonus user
	 * @param amountLeft - the amount that left before current bonus was used
	 * @param userId
	 * @param investmentId
	 * @param writerId
	 * @param isFree - is this a free bonus type
     * @param opportunityTypeId 
     * @return TODO
	 * @return amountLeft  - the amount that left after current bonus was used
     */
    public abstract boolean activateBonusAfterInvestmentSuccessByInvAmount(Connection conn, BonusUsers bu, long amountLeft, long userId, long investmentId, long writerId, boolean isFree, long loginId, long opportunityTypeId) throws BonusHandlersException;

    /**
     * Process activate bonuses after inv success by inv amount through this handler.
     *
     * @param conn - Connection
     * @param bu - bonus user
     * @param isInvWasCountForActivation - is inv was count for bonus activation before this one
     * @param investmentId TODO
     * @param opportunityTypeId 
	 * @return isInvWasCountForActivation - is inv was count for bonus activation after this one
     */
    public abstract boolean activateBonusAfterInvestmentSuccessByInvCount(Connection conn, BonusUsers bu,  boolean isInvWasCountForActivation, long investmentId, long opportunityTypeId) throws BonusHandlersException;

    /**
     * Returns BonusAmountOnSettleInvestment
     * @param conn TODO
     * @param investment - The settled investment
     * @param isWin - is investment in win status
     * @param isVoidBet - is investment in VoidBet status
     *
     * @return bonus amount
     */
    public abstract long handleBonusOnSettleInvestment(Connection conn, InvestmentBonusData investment, boolean isWin) throws BonusHandlersException;


    /**
     * Returns the amount that user can't withdraw due to used niou class bonuses.
     *
     * @param win - investment win amount
     * @param lose TODO
     * @param invAmount
     * @param bu - BonusUsers
     * @return bonus amount
     */
    public abstract long getAmountThatUserCantWithdraw(long win, long lose, long invAmount, BonusUsers bu);


    protected void activateBonusAndAddToBalance(Connection conn, BonusUsers bu, long userId, long writerId, long transactionId, long investmentId, long loginId) throws BonusHandlersException {
		try{
	        BonusDAOBase.activateBonusUsers(conn, bu, transactionId, investmentId);

            UsersDAOBase.addToBalance(conn, userId, bu.getBonusAmount());
            User user = UsersDAOBase.getUser(conn, userId);
            
            TransactionsDAOBase.insert(conn, TransactionsManagerBase.createBonusTransaction(userId, bu.getBonusAmount(), bu.getId(), writerId, user.getUtcOffset(), loginId));
            GeneralDAO.insertBalanceLog(
                    conn,
                    writerId,
                    userId,
                    ConstantsBase.TABLE_BONUS_USERS,
                    bu.getId(),
                    ConstantsBase.LOG_BALANCE_BONUS_DEPOSIT,
                    user.getUtcOffset());

		}catch (SQLException e) {
			throw new BonusHandlersException("Error in activateBonus from activateBonusAndAddToBalance ", e);
		}
	}
    
    protected void addBonusToBalance(Connection conn, BonusUsers bu, long userId, long writerId, long transactionId, long investmentId, long loginId) throws BonusHandlersException{
		try{
            UsersDAOBase.addToBalance(conn, userId, bu.getBonusAmount());
            User user = UsersDAOBase.getUser(conn, userId);
            TransactionsDAOBase.insert(conn, TransactionsManagerBase.createBonusTransaction(userId, bu.getBonusAmount(), bu.getId(), writerId,user.getUtcOffset(), loginId));
            GeneralDAO.insertBalanceLog(
                    conn,
                    writerId,
                    userId,
                    ConstantsBase.TABLE_BONUS_USERS,
                    bu.getId(),
                    ConstantsBase.LOG_BALANCE_BONUS_DEPOSIT,
                    user.getUtcOffset());

		}catch (SQLException e) {
			throw new BonusHandlersException("Error in addToBalance from addBonusToBalance ", e);
		}
	}
    
    protected void activateBonus(Connection conn, BonusUsers bu, long userId, long writerId, long transactionId, long investmentId) throws BonusHandlersException{
		try{
	        BonusDAOBase.activateBonusUsers(conn, bu, transactionId, investmentId);
		}catch (SQLException e) {
			throw new BonusHandlersException("Error in activateBonus from activateBonusAndAddToBalance ", e);
		}
	}
    
    /**
     * @param conn
     * @param bu
     * @param investmentId
     * @param bonusAmountFromInv - the part of the amount from the investment that the user used.
     * @param adjustedAmountHistory
     * @param typeId
     * @throws BonusHandlersException
     */
    public void insertBonusInvestments(Connection conn, BonusUsers bu, long investmentId, long bonusAmountFromInv, long adjustedAmountHistory, int typeId) throws BonusHandlersException {
		try {
			BonusDAOBase.insertBonusInvestments(conn, investmentId, bu.getId(), bonusAmountFromInv, adjustedAmountHistory, typeId);
		} catch (SQLException e) {
			throw new BonusHandlersException("BMS, Error in insertBonusInvestments, bonus_users_id: " + bu.getId(), e);
		}
    }


	/**
	 * Call dao base to update bonus adjusted amount, adjusted amount will be 0 if bonus used < 0.
	 * @param conn
	 * @param bu
	 * @param bonusUsed - can be negative.
	 * @return bonusUsed for the next bonus. if negative => make it positive to insert to the next bonus (adjusted amount).
	 * @throws BonusHandlersException
	 */
	public long updateAdjustedAmount(Connection conn, BonusUsers bu, long bonusUsed) throws BonusHandlersException {		
		try {
			BonusDAOBase.updateAdjustedAmount(conn, bu.getId(), bonusUsed < 0 ? 0 : bonusUsed); 
		} catch (SQLException e) {
			throw new BonusHandlersException("BMS, Error in wageringAfterInvestmentSuccess bonus_users id:" + bu.getId() + ", bonusUsed: " + bonusUsed , e);
		}
		return Math.abs(bonusUsed);
	}
}