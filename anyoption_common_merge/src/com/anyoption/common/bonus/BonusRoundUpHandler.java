package com.anyoption.common.bonus;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;

import com.anyoption.common.beans.Bonus;
import com.anyoption.common.beans.User;
import com.anyoption.common.beans.base.BonusUsers;
import com.anyoption.common.beans.base.Currency;
import com.anyoption.common.bl_vos.BonusCurrency;
import com.anyoption.common.bonus.BonusHandlersException;
import com.anyoption.common.daos.BonusDAOBase;
import com.anyoption.common.daos.CurrenciesDAOBase;
import com.anyoption.common.daos.InvestmentsDAOBase;
import com.anyoption.common.daos.UsersDAOBase;
import com.anyoption.common.managers.BonusManagerBase;
import com.anyoption.common.managers.TransactionsManagerBase;
import com.anyoption.common.util.ConstantsBase;

public class BonusRoundUpHandler extends BonusInstantAmountHandler {

	/**
	 *  bonusInsert event handler implementation
	 */
	@Override
	public boolean bonusInsert(Connection conn, BonusUsers bu, Bonus b, BonusCurrency bc, long userId, long userCurrencyId, long writerId, long popEntryId, int bonusPopLimitTypeId, boolean isNeedToSendInternalMail) throws BonusHandlersException {	
		boolean res = false;		
		bu.setBonusStateId(ConstantsBase.BONUS_STATE_PENDING);

		try {
			BonusManagerBase.getLimitsForBonus(conn, bu, b, bc, popEntryId,bonusPopLimitTypeId, userId, userCurrencyId, false);
			long balance = UsersDAOBase.getUserBalance(conn, userId);
			long bonusAmount = 0;
			if (b.getRoundUpType() == ConstantsBase.BONUS_ROUND_UP_TYPE_ROUND_UP) {
	    		long minInv = InvestmentsDAOBase.getUserMinInvestmentLimit(conn, userCurrencyId);
		    	long avgInv = InvestmentsDAOBase.getUserAvgInvestment(conn, userId);
		    	avgInv = (long) (0.5 * avgInv);
		    	bonusAmount = Math.max(minInv, avgInv);
		    	bonusAmount = (bonusAmount/100);
		    	bonusAmount = Math.round(bonusAmount/5.0) * 5;
		    	bonusAmount = (bonusAmount*100);
		    	bonusAmount = bonusAmount - balance;
		    	if (bonusAmount <= 0){
		    		return false;
		    	}
	    	} else if (b.getRoundUpType() == ConstantsBase.BONUS_ROUND_UP_TYPE_GET_STARTED){
	    		HashMap<Long, Long> minInvByCurrencyHM = new HashMap<Long, Long>();
        		HashMap<Long, Currency> hm = CurrenciesDAOBase.getAllCurrencies(conn);
        		for (Long currencyId : hm.keySet()) {
        			minInvByCurrencyHM.put(currencyId, InvestmentsDAOBase.getUserMinInvestmentLimit(conn, currencyId));
        		}
	    		long minInv = minInvByCurrencyHM.get(userCurrencyId);
	    		bonusAmount = 4 * minInv;
	    	} else if (b.getRoundUpType() == ConstantsBase.BONUS_ROUND_UP_TYPE_MIN_INVESTMENT){
	    		bonusAmount = InvestmentsDAOBase.getUserMinInvestmentLimit(conn, userCurrencyId);
	    		bonusAmount = (bonusAmount/100);
		    	bonusAmount = Math.round(bonusAmount/5.0) * 5;
		    	bonusAmount = (bonusAmount*100);
		    	bonusAmount = bonusAmount - balance;
		    	if (bonusAmount <= 0){
		    		return false;
		    	}
	    	}
			bc.setBonusAmount(bonusAmount);
			
			bu.setSumInvWithdrawal(bc.getBonusAmount() * b.getWageringParameter());

			res = BonusDAOBase.insertBonusUser(conn, bu, b, bc, popEntryId, isNeedToSendInternalMail);

		} catch (SQLException e) {
			throw new BonusHandlersException("Error in inserting bonus" , e);
		}
		return res;
	}
	
	@Override
	public boolean acceptBonus(Connection conn, BonusUsers bu, User user, String ip, long loginId) throws SQLException {
		boolean res = false;

		long stateId = ConstantsBase.BONUS_STATE_DONE;;
		//	for bonus deposit amount that have no wagering
		if (bu.getWageringParam() != 0) {
			stateId = ConstantsBase.BONUS_STATE_ACTIVE;
		} 
		
		res = BonusDAOBase.acceptBonusUser(conn, bu.getId(), stateId);
		
		if (res) {
			if (user.getCurrency() == null) {
				user.setCurrency(CurrenciesDAOBase.getById(conn, user.getCurrencyId().intValue()));
			}
			res = TransactionsManagerBase.insertBonusDepositBase(user, bu.getId(), bu.getWriterId(), bu.getBonusAmount(), bu.getComments(), ip, conn, loginId);
		}
		
		return res;
	}
}