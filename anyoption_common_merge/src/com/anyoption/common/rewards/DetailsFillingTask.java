package com.anyoption.common.rewards;

import org.apache.log4j.Logger;
/**
 * @author liors
 *
 */
public class DetailsFillingTask extends RewardTasksHandler {
	private static final Logger logger = Logger.getLogger(DetailsFillingTask.class);
	private static volatile DetailsFillingTask detailsFillingTask;

	private DetailsFillingTask() {} 

	/**
	 * @return
	 */
	public synchronized DetailsFillingTask getInstance() {
		if (detailsFillingTask == null) {
			detailsFillingTask = new DetailsFillingTask();
		}
		return detailsFillingTask;
	}

}
