package com.anyoption.common.rewards;

import java.sql.Connection;
import java.sql.SQLException;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.RewardTier.TierChanges;
import com.anyoption.common.beans.RewardUserHistory;
import com.anyoption.common.beans.User;
import com.anyoption.common.daos.RewardDAO;
import com.anyoption.common.daos.RewardTasksDAO;
import com.anyoption.common.daos.RewardUsersDAO;
import com.anyoption.common.daos.RewardUsersHistoryDAO;
import com.anyoption.common.managers.RewardManager;
import com.anyoption.common.managers.RewardUserTasksManager;
import com.anyoption.common.managers.RewardUsersHistoryManager;
import com.anyoption.common.util.ConstantsBase;

/**
 * @author liors
 *
 */
public abstract class RewardTasksHandler implements TaskMechanism {
	private static final Logger logger = Logger.getLogger(RewardTasksHandler.class);
	
	/** 
	 * 
	 * @param connection
	 * @param taskUser
	 * @return
	 * @throws TaskHandlerException
	 * @throws SQLException 
	 */
	public TaskUser getUserTasks(Connection connection, TaskUser taskUser) throws Exception {
		return RewardTasksDAO.getUserTasks(connection, taskUser, 0);
	}
	
	/**
	 * @param connection
	 * @param taskUser
	 * @param user 
	 * @return
	 * @throws TaskHandlerException
	 * @throws SQLException 
	 */
	public TaskUser calculateExperiencePoint(Connection connection, TaskUser taskUser) throws Exception {
		for (Task task : taskUser.getTasks()) {
			task.getRewardUserTask().setNumOfActions(task.getRewardUserTask().getNumOfActions() + 1);
			logger.debug(ConstantsBase.REWARD_PLAN_LOG_PREFIX + "task id = " + task.getRewardTask().getId() + " task type = " + task.getRewardTask().getTypeId() +"\n"
					+ " user task num action = " + task.getRewardUserTask().getNumOfActions() + "  task num action = " + task.getRewardTask().getNumOfActions());
			if (task.getRewardUserTask().getNumOfActions() == task.getRewardTask().getNumOfActions()) {
				task.getRewardUserTask().setNumOfActions(0);
				task.getRewardUserTask().setNumOfRecurring(task.getRewardUserTask().getNumOfRecurring() + 1);
				task.getRewardUserTask().setDoneRecurring(true);
				taskUser.setTotalExperiencePoints(taskUser.getTotalExperiencePoints() + task.getRewardTask().getExperiencePoints());
				logger.debug(ConstantsBase.REWARD_PLAN_LOG_PREFIX + "got exp points " + getTaskExperiencePoints(connection, task, taskUser) + "\n" +
						"user task Num Of Recurring = " + task.getRewardUserTask().getNumOfRecurring() + "  task Num Of Recurring = " + task.getRewardTask().getNumOfRecurring());
				if (task.getRewardTask().getNumOfRecurring() == task.getRewardUserTask().getNumOfRecurring()) {
					task.getRewardUserTask().setToDone();		
				}
			}
		}
		return taskUser;
	}
	
	public void updateTask(Connection connection, User user, TaskUser taskUser) throws Exception {
		double userExpPoints = user.getRewardUser().getExperiencePoints();
		RewardUserHistory rewardUserHistory = null;
		double taskExperiencePoints;
		for (Task task : taskUser.getTasks()) {
			taskExperiencePoints = getTaskExperiencePoints(connection, task, taskUser);
			RewardUserTasksManager.updateOrInsert(connection, task.getRewardUserTask());
			if (task.getRewardUserTask().isDoneRecurring()) {
				userExpPoints += taskExperiencePoints;
				rewardUserHistory = new  RewardUserHistory(taskUser.getUserId(), task.getRewardUserTask().getId(), taskUser.getRefernceId(), 
						RewardUsersHistoryManager.ADD_EXPERIENCE_POINTS, taskUser.getWriterId(), task.getRewardUser().getTierId(),
						userExpPoints, RewardManager.TASK, "", taskExperiencePoints);
				RewardUsersHistoryDAO.insertValueObject(connection, rewardUserHistory);
			}
			
			if (task.getRewardUserTask().isDone()) {
				rewardUserHistory.setRewardHistoryTypeId(RewardUsersHistoryManager.TASK_COMPLETION);
				rewardUserHistory.setRewardActionId(task.getRewardUserTask().getId());
				rewardUserHistory.setAmountExperiencePoints(0);
				RewardUsersHistoryDAO.insertValueObject(connection, rewardUserHistory);			
			}
		}
	}
	
	/**
	 * get the task Experience Points 
	 * @param connection 
	 * @param task
	 * @param taskUser
	 * @return the task Experience Points
	 */
	public double getTaskExperiencePoints(Connection connection, Task task, TaskUser taskUser) throws SQLException {
		return task.getRewardTask().getExperiencePoints();
	}
	
	public TierChanges calculateTier(Connection connection, User user, TaskUser taskUser) throws Exception {
		user.getRewardUser().setExperiencePoints(taskUser.getTotalExperiencePoints() + user.getRewardUser().getExperiencePoints());
		TierChanges tierChanges = new TierChanges(user.getRewardUser().getTierId(), false);
		int newTtierId = RewardDAO.getTierIdByExperiencePoints(connection, user.getRewardUser().getExperiencePoints());
		if (newTtierId > user.getRewardUser().getTierId()) {
			tierChanges.setMoveTier(true);
			user.getRewardUser().setTierId(newTtierId);
			tierChanges.setTierId(newTtierId);
			RewardUsersHistoryDAO.insertValueObject(connection, new RewardUserHistory(user.getId(), RewardUsersHistoryManager.PROMOTE_TIER, taskUser.getWriterId(),
					user.getRewardUser().getExperiencePoints(), user.getRewardUser().getTierId()));
		} else if (newTtierId < user.getRewardUser().getTierId()) {
			logger.warn(ConstantsBase.REWARD_PLAN_LOG_PREFIX + "WARN new tier is lower from current tier");
		}
		return tierChanges;
	}
		
	public void updateRewardUser(Connection connection, User user, TaskUser taskUser, TierChanges tierChanges) throws SQLException {
		RewardUsersDAO.update(connection, user.getId(), tierChanges.getTierId(), user.getRewardUser().getExperiencePoints(), tierChanges.isMoveTier());
	}
	
	public boolean calculateReward(Connection connection, User user, TaskUser taskUser, int tierId, Class<?> bonusManagerClass) throws Exception {
		boolean isSkipped = getIsSkipped();
		return RewardUtil.grantRewards(connection, user.getId(), taskUser.getWriterId(), tierId, isSkipped, bonusManagerClass, user.getRewardUser().getExperiencePoints());
	}
	
	protected boolean getIsSkipped() {
		return false;
	}

	/**
	 * Reward Plan flow.
	 * 
	 * @param connection
	 * @param taskUser
	 * @param bonusManagerClass 
	 * @return
	 * @throws TaskHandlerException
	 * @throws SQLException 
	 */
	public boolean taskHandler(Connection connection, TaskUser taskUser, User user, Class<?> bonusManagerClass) throws Exception {
		logger.info(ConstantsBase.REWARD_PLAN_LOG_PREFIX + "about to getUserTasks. " + taskUser.toString());
		taskUser = getUserTasks(connection, taskUser);
		if (taskUser.hasTasks()) {
			logger.info(ConstantsBase.REWARD_PLAN_LOG_PREFIX + "taskUser hasTasks " + taskUser.toString());
			taskUser = calculateExperiencePoint(connection, taskUser);
			updateTask(connection, user, taskUser);
			TierChanges tierChanges = calculateTier(connection, user, taskUser);
			logger.info(ConstantsBase.REWARD_PLAN_LOG_PREFIX + "tierChanges: " + tierChanges.toString() + " " + taskUser.toString());
			if (tierChanges.isMoveTier()) {
				logger.info(ConstantsBase.REWARD_PLAN_LOG_PREFIX + "userId: " + user.getId() + " moved tier to : " + tierChanges.getTierId());
				calculateReward(connection, user, taskUser, tierChanges.getTierId(), bonusManagerClass);
			}
			if (taskUser.getTotalExperiencePoints() > 0 || tierChanges.isMoveTier()) {
				logger.info(ConstantsBase.REWARD_PLAN_LOG_PREFIX + "about to update Reward UserId: " + user.getId());
				updateRewardUser(connection, user, taskUser, tierChanges);
			}
		}
		return true;
	}
}