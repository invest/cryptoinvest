package com.anyoption.common.rewards;

import java.util.ArrayList;

import com.anyoption.common.managers.RewardUserTasksManager.TaskGroupType;

/**
 * @author liors
 *
 */
public class TaskUser {
	private TaskGroupType taskGroupType;
	private long userId;
	private long amount;
	private int currencyId; //TODO long/Long?
	private double totalExperiencePoints;
	private long refernceId;
	private int writerId;
	private int productTypeId;
	private long opportunityTypeId;
	private ArrayList<Task> tasks;
	
	public TaskUser() {
		tasks = new ArrayList<Task>();
	}
	
	public TaskUser(TaskGroupType taskGroupType, long userId, long amount, int currencyId, long refernceId, int writerId, int productTypeId, long opportunityTypeId) {
		this.taskGroupType = taskGroupType;
		this.userId = userId;
		this.amount = amount;
		this.currencyId = currencyId;
		this.refernceId = refernceId;
		this.writerId = writerId;
		this.totalExperiencePoints = 0;
		this.productTypeId = productTypeId;
		this.opportunityTypeId = opportunityTypeId;
		tasks = new ArrayList<Task>();
	}
	
	public boolean hasTasks() {
		return !tasks.isEmpty();
	}

	/**
	 * @return the taskGroupTypeId
	 */
	public int getTaskGroupTypeId() {
		return taskGroupType.getId();
	}

	/**
	 * @param taskGroupTypeId the taskGroupTypeId to set
	 */
	public void setTaskGroupType(TaskGroupType taskGroupType) {
		this.taskGroupType = taskGroupType;
	}

	/**
	 * @return the userId
	 */
	public long getUserId() {
		return userId;
	}
	/**
	 * @param userId the userId to set
	 */
	public void setUserId(long userId) {
		this.userId = userId;
	}
	/**
	 * @return the amount
	 */
	public long getAmount() {
		return amount;
	}
	/**
	 * @param amount the amount to set
	 */
	public void setAmount(long amount) {
		this.amount = amount;
	}
	/**
	 * @return the currencyId
	 */
	public int getCurrencyId() {
		return currencyId;
	}
	/**
	 * @param currencyId the currencyId to set
	 */
	public void setCurrencyId(int currencyId) {
		this.currencyId = currencyId;
	}

	/**
	 * @return the tasks
	 */
	public ArrayList<Task> getTasks() {
		return tasks;
	}
	/**
	 * @param tasks the tasks to set
	 */
	public void setTasks(ArrayList<Task> tasks) {
		this.tasks = tasks;
	}

	public long getRefernceId() {
		return refernceId;
	}

	public void setRefernceId(long refernceId) {
		this.refernceId = refernceId;
	}

	@Override
	public String toString() {
		return "TaskUser [userId=" + userId + ", amount=" + amount
				+ ", currencyId=" + currencyId + ", totalExperiencePoints="
				+ totalExperiencePoints + ", refernceId=" + refernceId
				+ ", writerId=" + writerId + ", productTypeId=" + productTypeId
				+ ", opportunityTypeId=" + opportunityTypeId + ", tasks="
				+ tasks + "]";
	}

	/**
	 * @return the writerId
	 */
	public int getWriterId() {
		return writerId;
	}

	/**
	 * @param writerId the writerId to set
	 */
	public void setWriterId(int writerId) {
		this.writerId = writerId;
	}

	public double getTotalExperiencePoints() {
		return totalExperiencePoints;
	}

	public void setTotalExperiencePoints(double totalExperiencePoints) {
		this.totalExperiencePoints = totalExperiencePoints;
	}

	public int getProductTypeId() {
		return productTypeId;
	}

	public void setProductTypeId(int productTypeId) {
		this.productTypeId = productTypeId;
	}

	public long getOpportunityTypeId() {
		return opportunityTypeId;
	}

	public void setOpportunityTypeId(long opportunityTypeId) {
		this.opportunityTypeId = opportunityTypeId;
	}
}
