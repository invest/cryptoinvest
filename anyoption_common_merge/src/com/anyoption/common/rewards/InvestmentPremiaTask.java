package com.anyoption.common.rewards;

import java.sql.Connection;
import java.sql.SQLException;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.RewardTaskType;
import com.anyoption.common.beans.RewardUserPlatform;
import com.anyoption.common.daos.RewardTasksDAO;
import com.anyoption.common.daos.RewardUserPlatformsDAO;

/**
 * @author liors
 *
 */
public class InvestmentPremiaTask extends InvestmentTask {
	private static final Logger logger = Logger.getLogger(InvestmentPremiaTask.class);
	private static volatile InvestmentPremiaTask investmentPremiaTask;
	
	public static InvestmentPremiaTask getInstance() {
		if (investmentPremiaTask == null) {
			investmentPremiaTask = new InvestmentPremiaTask();
		}
		return investmentPremiaTask;
	}
	
	/** 
	 * 
	 * @param connection
	 * @param taskUser
	 * @return
	 * @throws TaskHandlerException
	 * @throws SQLException 
	 */
	public TaskUser getUserTasks(Connection connection, TaskUser taskUser) throws Exception {
		return RewardTasksDAO.getUserTasks(connection, taskUser, RewardTaskType.REWARD_TASK_TYPE_INV_PNTS);
	}
	
	/**
	 * @param connection
	 * @param taskUser
	 * @param user 
	 * @return
	 * @throws TaskHandlerException
	 * @throws SQLException 
	 */
	public TaskUser calculateExperiencePoint(Connection connection, TaskUser taskUser) throws Exception {
		for (Task task : taskUser.getTasks()) {
			logger.debug("task id = " + task.getRewardTask().getId() + " task type = " + task.getRewardTask().getTypeId() + " writer id = " + taskUser.getWriterId());
			task.getRewardUserTask().setNumOfActions(task.getRewardUserTask().getNumOfActions() + 1);
			logger.debug("user task num action = " + task.getRewardUserTask().getNumOfActions() + "  task num action = " + task.getRewardTask().getNumOfActions());
			if (task.getRewardUserTask().getNumOfActions() == task.getRewardTask().getNumOfActions()) {
				task.getRewardUserTask().setNumOfActions(0);
				task.getRewardUserTask().setNumOfRecurring(task.getRewardUserTask().getNumOfRecurring() + 1);
				task.getRewardUserTask().setDoneRecurring(true);
				taskUser.setTotalExperiencePoints(taskUser.getTotalExperiencePoints() + getTaskExperiencePoints(connection, task, taskUser));
				logger.debug("got exp points " + getTaskExperiencePoints(connection, task, taskUser) + " user task Num Of Recurring = " + task.getRewardUserTask().getNumOfRecurring() + "  task Num Of Recurring = " + task.getRewardTask().getNumOfRecurring());
				if (task.getRewardTask().getNumOfRecurring() == task.getRewardUserTask().getNumOfRecurring()) {
					task.getRewardUserTask().setToDone();		
				}
			}
		}
		logger.debug("got total exp points " + taskUser.getTotalExperiencePoints());
		//insert or update Reward User Platforms
		RewardUserPlatform rewardUserPlatform = new RewardUserPlatform();
		rewardUserPlatform.setId(taskUser.getTasks().get(0).getRewardUserPlatform().getId());
		rewardUserPlatform.setNumOfInvestments(0);
		rewardUserPlatform.setTurnover(taskUser.getAmount());
		rewardUserPlatform.setExperiencePoints(taskUser.getTotalExperiencePoints());
		logger.debug("rewardUserPlatform.getId() = " + rewardUserPlatform.getId());
		if (rewardUserPlatform.getId() == 0) {
			rewardUserPlatform.setProductTypeId(taskUser.getProductTypeId());
			rewardUserPlatform.setUserId(taskUser.getUserId());
			RewardUserPlatformsDAO.insert(connection, rewardUserPlatform);
		} else {
			RewardUserPlatformsDAO.updateExpPointsAndTurnover(connection, rewardUserPlatform);
		}
		return taskUser;
	}
	
}
