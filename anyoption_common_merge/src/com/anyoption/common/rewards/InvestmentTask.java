package com.anyoption.common.rewards;

import java.sql.Connection;
import java.sql.SQLException;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.RewardTaskType;
import com.anyoption.common.beans.RewardUserPlatform;
import com.anyoption.common.beans.Writer;
import com.anyoption.common.daos.InvestmentsDAOBase;
import com.anyoption.common.daos.RewardUserPlatformsDAO;
import com.anyoption.common.util.ConstantsBase;

/**
 * @author liors
 *
 */
public class InvestmentTask extends RewardTasksHandler {
	private static volatile InvestmentTask investmentTask;
	private static final Logger logger = Logger.getLogger(InvestmentTask.class);
	
	public static InvestmentTask getInstance() {
		if (investmentTask == null) {
			investmentTask = new InvestmentTask();
		}
		return investmentTask;
	}
	
	/**
	 * @param connection
	 * @param taskUser
	 * @param user 
	 * @return
	 * @throws TaskHandlerException
	 * @throws SQLException 
	 */
	public TaskUser calculateExperiencePoint(Connection connection, TaskUser taskUser) throws Exception {
		//insert or update Reward User Platforms
		RewardUserPlatform rewardUserPlatform = new RewardUserPlatform();
		rewardUserPlatform.setId(taskUser.getTasks().get(0).getRewardUserPlatform().getId());
		rewardUserPlatform.setNumOfInvestments(1);
		rewardUserPlatform.setTurnover(taskUser.getAmount());
		logger.debug(ConstantsBase.REWARD_PLAN_LOG_PREFIX + "rewardUserPlatform.getId() = " + rewardUserPlatform.getId());
		if (rewardUserPlatform.getId() == 0) {
			rewardUserPlatform.setProductTypeId(taskUser.getProductTypeId());
			rewardUserPlatform.setUserId(taskUser.getUserId());
			RewardUserPlatformsDAO.insert(connection, rewardUserPlatform);
		} else {
			RewardUserPlatformsDAO.updateAfterInvestment(connection, rewardUserPlatform);
		}
		for (Task task : taskUser.getTasks()) {
			logger.debug(ConstantsBase.REWARD_PLAN_LOG_PREFIX + "task id = " + task.getRewardTask().getId() + " task type = " + task.getRewardTask().getTypeId() + " writer id = " + taskUser.getWriterId());
			//TODO: do REWARD_TASK_TYPE_INVITED_FRIEND_INVESTED
			if (task.getRewardTask().getTypeId() == RewardTaskType.REWARD_TASK_TYPE_INVITED_FRIEND_INVESTED) {
				continue;
			}
			if ((task.getRewardTask().getTypeId() == RewardTaskType.REWARD_TASK_TYPE_INV_VIA_APP && taskUser.getWriterId() == Writer.WRITER_ID_MOBILE) ||
					(task.getRewardTask().getTypeId() == RewardTaskType.REWARD_TASK_TYPE_NEW_PLATFORMS && RewardUserPlatformsDAO.isTaskDone(connection, taskUser.getUserId(), task.getRewardTask().getId())) ||
					(task.getRewardTask().getTypeId() != RewardTaskType.REWARD_TASK_TYPE_INV_VIA_APP && task.getRewardTask().getTypeId() != RewardTaskType.REWARD_TASK_TYPE_NEW_PLATFORMS)) {
				task.getRewardUserTask().setNumOfActions(task.getRewardUserTask().getNumOfActions() + 1);
			}
			logger.debug(ConstantsBase.REWARD_PLAN_LOG_PREFIX + "user task num action = " + task.getRewardUserTask().getNumOfActions() + "  task num action = " + task.getRewardTask().getNumOfActions());
			if (task.getRewardUserTask().getNumOfActions() == task.getRewardTask().getNumOfActions()) {
				task.getRewardUserTask().setNumOfActions(0);
				task.getRewardUserTask().setNumOfRecurring(task.getRewardUserTask().getNumOfRecurring() + 1);
				task.getRewardUserTask().setDoneRecurring(true);
				double taskExperiencePoints = getTaskExperiencePoints(connection, task, taskUser);
				taskUser.setTotalExperiencePoints(taskUser.getTotalExperiencePoints() + taskExperiencePoints);
				//update Reward User Platforms exp points
				if (task.getRewardTask().getTypeId() == RewardTaskType.REWARD_TASK_TYPE_INV_PNTS) {
					rewardUserPlatform.setExperiencePoints(taskExperiencePoints);
					RewardUserPlatformsDAO.updateExpPoints(connection, rewardUserPlatform);
				}
				logger.debug(ConstantsBase.REWARD_PLAN_LOG_PREFIX + "got exp points " + getTaskExperiencePoints(connection, task, taskUser) + " user task Num Of Recurring = " + task.getRewardUserTask().getNumOfRecurring() + "  task Num Of Recurring = " + task.getRewardTask().getNumOfRecurring());
				if (task.getRewardTask().getNumOfRecurring() == task.getRewardUserTask().getNumOfRecurring()) {
					task.getRewardUserTask().setToDone();		
				}
			}
		}
		logger.debug(ConstantsBase.REWARD_PLAN_LOG_PREFIX + "got total exp points " + taskUser.getTotalExperiencePoints());
		
		return taskUser;
	}
	
	/**
	 * get the task Experience Points 
	 * @param task
	 * @param taskUser
	 * @return the task Experience Points
	 * @throws SQLException 
	 */
	public double getTaskExperiencePoints(Connection connection, Task task, TaskUser taskUser) throws SQLException {
		double taskExperiencePoints = task.getRewardTask().getExperiencePoints();
		if (task.getRewardTask().getTypeId() == RewardTaskType.REWARD_TASK_TYPE_INV_PNTS) {
			double factor = InvestmentsDAOBase.getTurnoverFactor(connection, taskUser.getOpportunityTypeId());
			if (factor != 0) {
				taskExperiencePoints = ((taskUser.getAmount()/100) / (task.getRewardTask().getRewardTaskCurrency().getAmountToPoints()/100)) * 
						(task.getRewardTask().getExperiencePoints() * factor);
			}			
		}
		return taskExperiencePoints;
	}
}
