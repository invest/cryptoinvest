package com.anyoption.common.util;

import java.io.File;
import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.sql.Clob;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.MessageFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.MissingResourceException;
import java.util.Properties;
import java.util.Random;
import java.util.ResourceBundle;
import java.util.SortedSet;
import java.util.StringTokenizer;
import java.util.TimeZone;
import java.util.TreeSet;

import javax.activation.DataHandler;
import javax.activation.FileDataSource;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.SendFailedException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import com.anyoption.common.beans.CreditCard;
import com.anyoption.common.beans.EmailConfig;
import com.anyoption.common.beans.Enumerator;
import com.anyoption.common.beans.Issue;
import com.anyoption.common.beans.Opportunity;
import com.anyoption.common.beans.OpportunityType;
import com.anyoption.common.beans.ServerConfiguration;
import com.anyoption.common.beans.Transaction;
import com.anyoption.common.beans.Writer;
import com.anyoption.common.beans.base.Currency;
import com.anyoption.common.beans.base.Skin;
import com.anyoption.common.beans.base.User;
import com.anyoption.common.beans.base.User.MobileNumberValidation;
import com.anyoption.common.beans.base.UserRegulationBase;
import com.anyoption.common.clearing.ClearingManagerConstants;
import com.anyoption.common.daos.GeneralDAO;
import com.anyoption.common.enums.CopyOpInvTypeEnum;
import com.anyoption.common.managers.CountryManagerBase;
import com.anyoption.common.managers.CurrenciesManagerBase;
import com.anyoption.common.managers.CurrencyRatesManagerBase;
import com.anyoption.common.managers.DBUtil;
import com.anyoption.common.managers.GeneralManager;
import com.anyoption.common.managers.IssuesManagerBase;
import com.anyoption.common.managers.SMSManagerBase;
import com.anyoption.common.managers.SkinsManagerBase;
import com.anyoption.common.managers.UserRegulationManager;
import com.anyoption.common.managers.UsersManagerBase;
import com.anyoption.common.managers.WritersManagerBase;
import com.anyoption.common.sms.SMS;
import com.anyoption.common.sms.SMSException;



/**
 * @author LioR SoLoMoN
 *
 */
public class CommonUtil {
	private static final Logger log = Logger.getLogger(CommonUtil.class);

	private static final long MILLIS_PER_DAY = 86400000;
    protected static ResourceBundle config = null;
    private static ResourceBundle properties = null;
    public static String CONFIGURATION_FILE = "server";
    protected static String configurationFilePath = "server";
    private static String serverName;
    public static String MESSAGES_BUNDLE_NAME = "MessageResources";
    protected static Hashtable<Locale, ResourceBundle> messages = new Hashtable<Locale, ResourceBundle>();
    private static String etsServerName;
    protected static final String AO_COOKIE_NAME = "ao";
    protected static ArrayList<Enumerator> enumerators = null;
    private static long lastRefresh = 0;
    private static String REFRESH_PERIOD = "config.reloadperiod";
	public static String USER_AGENT = "userAgent";

    /**
     * Format currency amount and add currency symbol.
     *
     * @param amount amount to display (in cents)
     * @param showCurrencySymbol if to show currency symbol
     * @param currency
     * @param displayFormat
     * @param currencySymbol
     * @return
     */
    public static String formatCurrencyAmount(double amount,
    											boolean showCurrency,
    											Currency currency,
    											String displayFormat,
    											String currencySymbol) {
        DecimalFormat sd = new DecimalFormat(displayFormat, new DecimalFormatSymbols(Locale.US));
        String out = "";
        if (showCurrency) {
            if (currency.getIsLeftSymbolBool()) {
                out = currencySymbol + sd.format(amount);
            } else {
                out = sd.format(amount) + currencySymbol;
            }
        } else {
            out = sd.format(amount);
        }
        return out;
    }
    
    public static String formatCurrencyAmountCustomerReport(long amount, Currency currency) {
		double amountDecimal = amount;
		amountDecimal /= 100;    	
    	return formatCurrencyAmount(amountDecimal, true, currency, "###,###.##", currency.getSymbol());
    }
    
    public static String formatCurrencyAmountWithoutDecimalPoints(long amount, long currencyId) {
    	Currency currency = CurrenciesManagerBase.getCurrency(currencyId);
		double amountDecimal = amount;
		double decimalPoints = Math.pow(10, currency.getDecimalPointDigits());
		amountDecimal /= decimalPoints;
    	return formatCurrencyAmount(amountDecimal, true, currency, "###,###,###", currency.getSymbol());
    }
    
    /**
     * Format currency amount and add currency symbol.
     *
     * @param amount amount to display (in dollars - not cents)
     * @param showCurrencySymbol if to show currency symbol
     * @param currency
     * @return
     */
    public static String formatCurrencyAmount(double amount, boolean showCurrency, Currency currency) {
        return formatCurrencyAmount(amount,
									showCurrency,
									currency,
									CurrenciesManagerBase.getAmountFormat(currency, false),
									currency.getDefaultSymbol());
    }

    /**
     * Format a message with specified locale, key and parameters.
     *
     * @param locale
     * @param key message key
     * @param params parameters for the message
     * @return
     */
    public static String getMessage(Locale locale, String key, Object[] params) {
        if (null == key || key.trim().equals("")) {
            return "";
        }

        String text = null;
        try {
        	ResourceBundle bundle = messages.get(locale);
        	if (null == bundle) {
        		if (null == locale) {
        			bundle = ResourceBundle.getBundle(MESSAGES_BUNDLE_NAME);
        		} else {
        			bundle = ResourceBundle.getBundle(MESSAGES_BUNDLE_NAME, locale);
        		}
        		messages.put(locale, bundle);
        	}
            text = bundle.getString(key);
        } catch (MissingResourceException e) {
        	log.trace("Unable to laod resource", e);
            text = "?" + key + "?";
        }
        if (null != params) {
        	text = text.replaceAll("'", "@@@@@");
            text = MessageFormat.format(text, params);
            text = text.replaceAll("@@@@@", "'");
        }
        return text;
    }

    /**
     * Check server name with unix command
     * @param mail
     * @return serve name
     */
    public static String getServerNameUnix() {
    	if (getConfig("system").equalsIgnoreCase("local")) {
			 return "Local-SERVER";
		}
		String command = ConstantsBase.UNIX_HOSTNAME_COMMAND;
		String unixRes = UnixUtil.runUnixCommand(command);
		if (!CommonUtil.isParameterEmptyOrNull(unixRes)) {
			int index = 2; // in web we want to return server 00
			if (getConfig("application.source").equalsIgnoreCase("backend")) { // in BE we want to return BE00
				index = 0;
			}
			try {
				return unixRes.substring(index, unixRes.indexOf("."));
			} catch (Exception e) {
				return unixRes;
			}
		}
		return "";
	}

 	/**
 	 * Check if some variable is empty or not.
 	 *
 	 * @param par
 	 *            The parameter that need to check if he is empty or not
 	 */
 	public static boolean isParameterEmptyOrNull(String par) {
 		if( (par == null || par.length() == 0) ) {
 			return true;
 		}
 		return false;
 	}

    public static String getConfig(String key) {
        return getConfig(key, "?" + key + "?");
    }

    public static String getConfig(String key, String defVal) {
        if (null == config) {
            config = ResourceBundle.getBundle(CONFIGURATION_FILE);
        }
        if (null == key || key.trim().equals("")) {
            return "";
        }
        String text = null;
        try {
            text = config.getString(key);
        } catch (MissingResourceException e) {
            if (log.isDebugEnabled()) {
                log.debug("Missing value for key: " + key + " Default value is: " + defVal);
            }
            return defVal;
        }
        return text.trim();
    }

	/**
	 * Create cookie and add to response
	 * @param name
	 * 		cookie name
	 * @param value
	 * 		cookie value
	 * @param description
	 * 		description for the log
	 * @param response
	 */
	public static void addCookie(String name, String value, HttpServletRequest request, HttpServletResponse response, String description) {
		Cookie cookie = new Cookie(name, value);
		cookie.setPath("/");
		cookie.setMaxAge(365 * 24 * 60 * 60 * 1000); // about year
		//sub domain
		String property = getPropertyByHostRequest(request, ConstantsBase.COOKIE_DOMAIN, null);
		if (null != property) {
			cookie.setDomain(property);
		}
		response.addCookie(cookie);
		log.log(Level.DEBUG, "saving " + description + " in cookie with value:" + value);
	}

	/**
	 * Delete exists cookie, it's deleted when we add to the response the same
	 * cookie with no maxAge.
	 * @param name cookie name
	 * @param response
	 */
	public static void deleteCookie(Cookie cookie, HttpServletResponse response) {
		cookie.setPath("/");
		cookie.setMaxAge(0);
		response.addCookie(cookie);
		log.log(Level.DEBUG, "delete cookie " + cookie.getName());
	}

    public static Cookie getCookie(HttpServletRequest request, String cookieName) {
        Cookie cookie = null;
        Cookie[] cs = request.getCookies();
        try {
            if (cs != null) {

                for (int i = 0; i < cs.length; i++) {
                    if (cs[i].getName().equals(cookieName)) {
                        cookie = cs[i];
                    }
                }
            }
        } catch (Exception e) {
            log.error("When get cookie ", e);
        }
        return cookie;
    }

    public static boolean isHebrewSkin(long skinId) {
        return skinId == Skin.SKIN_ETRADER || skinId == Skin.SKIN_TLV;
    }

    /**
	 * This method receives a date\time in GMT and an utcOffset and returns a new date\time minus the utc offset - used for all filters
	 * @param d - date\time
	 * @param timeZoneName - utcOffset
	 * @return new date\time
	 */
	public static Date getDateTimeFormat(Date d, String timeZoneName) {
		TimeZone tz = TimeZone.getTimeZone(timeZoneName);
		long utcOffset = tz.getRawOffset();
		return new Date(d.getTime() - utcOffset);
	}

    /**
     * Send email with specified properties through specified server.
     *
     * @param server
     *            hashtable with server properties: "url" the url of the SMTP
     *            server to send mail through "auth" if the server requires
     *            authentication "user" if the server requires authentication
     *            the user name to use "pass" if the server requires
     *            authentication the pass to use
     * @param msg
     *            hasmap with email properties: "subject" the subject of the
     *            email "to" the receipient of the mail (can be multiple emails
     *            separated by ";") "from" the sender of the mail "body" the
     *            email body
     */
    public static void sendEmail(Hashtable<String, String> server, Hashtable<String, String> msg, File [] attachments) {
        Properties props = new Properties();
        props.put("mail.smtp.host", server.get("url"));
        String subject = msg.get("subject");
        String to = msg.get("to");
        String fromStr = msg.get("from");
        String body = msg.get("body");
        InternetAddress from = null;

        try {
//			if (fromStr.contains("anyoption")) {
//				from = new InternetAddress(fromStr, "Anyoption");
//			} else if (fromStr.contains("168qiquan")) {
//				// in VIP 168 we have the ZH locale as default so "en" should
//				// fall back there and produce the same result
//				// TODO fix this address; related to DEV-180
//				// from = new InternetAddress(fromStr,
//				// CommonUtil.getMessage("email.sender.168qiquan", null, new
//				// Locale(ConstantsBase.LOCALE_DEFAULT)), "UTF-8");
//				from = new InternetAddress(fromStr);
//			} else if (fromStr.contains("copyop")) {
//				from = new InternetAddress(fromStr, "copyop");
//			} else {
//				from = new InternetAddress(fromStr, "Etrader");
//			}
//			
//			if (fromStr.contains("BackendAdmin")) {
//				from = new InternetAddress(fromStr, "Admin");
//			}
			
			from = new InternetAddress(fromStr);
		} catch (Exception e) {
		    log.error("Can't send email", e);
		    return;
		}

        String[] splitTo = to.split(";");
        log.info("Trying to send email");
        for (int i = 0; i < splitTo.length; i++) {
			if (attachments == null) {
				sendSingleEmail(props, subject, splitTo[i], from, body);
			} else {  // add attachment
				sendSingleEmail(props, subject, splitTo[i], from, body, attachments);
			}
        }
        log.info("Successfully sent email");
    }

    /**
     * Send email to one receipient.
     *
     * @param props the session default properties
     * @param subject email subject
     * @param to email rcpt
     * @param from email sender
     * @param body email body
     */
    private static void sendSingleEmail(Properties props, String subject, String to, InternetAddress from, String body) {
        Session session = Session.getInstance(props);
		MimeMessage mess = new MimeMessage(session);

		try {
		    mess.setFrom(from);
			mess.setHeader("To", to);
			mess.setHeader("Content-Type", "text/html; charset=UTF-8");
			mess.setHeader("Content-Transfer-Encoding", "base64");
			mess.setSubject(subject, "UTF-8");
			mess.setContent(body, "text/html; charset=UTF-8");
			Transport.send(mess);
		} catch (AddressException ae) {
			log.log(Level.FATAL, "Error Sending Mail to: " + to + "body: " + body, ae);
		} catch (MessagingException me) {
			log.log(Level.FATAL, "Error Sending Mail to: " + to + "body: " + body, me);
		}
    }

    /**
	 * Send single email with HTML content + attachment
	 * @param props
	 * @param subject
	 * @param to
	 * @param from
	 * @param body  HTML content of the email
	 * @param server
	 * @param attachment file attachment
	 */
	private static void sendSingleEmail(Properties props, String subject, String to, InternetAddress from, String body,File [] attachment ) {
		Session session = Session.getInstance(props);
		MimeMessage mess = new MimeMessage(session);

		try {
			// Set Headers
			mess.setFrom(from);
			mess.setHeader("To", to);
			mess.setHeader("Content-Type", "text/html; charset=UTF-8");
			mess.setHeader("Content-Transfer-Encoding", "base64");
			mess.setSubject(subject, "UTF-8");

			// set the email Text
			MimeBodyPart messagePart = new MimeBodyPart();
			//messagePart.setText(body);
			messagePart.setContent(body, "text/html; charset=UTF-8");
			Multipart multipart = new MimeMultipart();
			// set the email attachment file
			MimeBodyPart attachmentPart;
			for(int i = 0; i<attachment.length; i++){
			FileDataSource fileDataSource = new FileDataSource(attachment [i]) {
				@Override
				public String getContentType() {
					return "application/octet-stream";
				}
			};
			attachmentPart = new MimeBodyPart();
			attachmentPart.setDataHandler(new DataHandler(fileDataSource));
			attachmentPart.setFileName(attachment[i].getName());
			multipart.addBodyPart(attachmentPart);
			}
			multipart.addBodyPart(messagePart);


			mess.setContent(multipart);
			Transport.send(mess);

		} catch (AddressException ae) {
			log.log(Level.FATAL, "Error Sending Mail to: " + to + "body: " + body, ae);
		} catch (MessagingException me) {
			log.log(Level.FATAL, "Error Sending Mail to: " + to + "body: " + body, me);
		}
	}

	 /**
		 * Send single email with HTML content + attachment
		 * @param props
		 * @param subject
		 * @param to
		 * @param from
		 * @param body  HTML content of the email
		 * @param server
		 * @param attachment file attachment
	*/


	/**
	 * Format <code>Date</code> with pattern and time zone.
	 *
	 * @param d
	 * @param timeZone
	 * @param pattern
	 * @return
	 */
	public static String formatDate(Date d, String timeZone, String pattern) {
	    if (null == d) {
	        return "";
	    }
	    SimpleDateFormat sdf = new SimpleDateFormat(pattern);
	    if (null != timeZone) {
	        TimeZone tz = TimeZone.getTimeZone(timeZone);
	        if (null != tz) {
	            sdf.setTimeZone(tz);
	        }
	    }
	    return sdf.format(d);
	}

	/**
	 * get server id from server properties
	 * @return server name
	 */
	public static String getServerName() {
    	if (null == serverName) {
    		serverName = getConfig("server.id");
        }
		return serverName;
	}

	   /**
     * Check mail validation with unix command
     * @param mail
     * @return
     */
    public static boolean isEmailValidUnix(String mail) {
   	 if (getProperty("system").equalsIgnoreCase("local")){ //cancel validation from local env.
   		 return true;
   	 }
   	 if (!mail.trim().equals("") && mail.contains("@")) {
   		 String[] mailSuffix = mail.split("@");
   		 if (mailSuffix.length != 2){ //email must have only one '@'
   			 return false;
   		 }
   		 String command = ConstantsBase.UNIX_CHECKMX_COMMAND + mailSuffix[1];
   		 String unixRes = UnixUtil.runUnixCommand(command);

   		 if ( isParameterEmptyOrNull(unixRes)) {   // error with running command
   			 log.warn("Error with running unix checkmx  command, res: " + unixRes);
   			 return false;
   		 } else if (unixRes.equals("1")){ // result from CHECKMX
   			 return true;
   		 }
   	 }
		 return false;
    }

    public static String getProperty(String key) {
		return getProperty(key, "?" + key + "?");
	}

	public static String getProperty(String key, String defval) {
		if (properties == null) {
			properties = ResourceBundle.getBundle(configurationFilePath);
        }
		if (key == null || key.trim().equals("")) {
			return "";
        }
		String text = null;
		try {
			text = properties.getString(key);
		} catch (MissingResourceException e) {
			if (log.isDebugEnabled()) {
	            log.debug("Missing value for key :" + key + " Default value is :" + defval);
            }
			return defval;
		}
		return text.trim();
	}

    /**
     * Take a long value from the server.properties.
     *
     * @param key
     * @return The requested server.properties parameter as long or 0 if not found.
     */
    public static long getPropertyLong(String key) {
        String val = getProperty(key);
        long rez = 0;
        try {
            rez = Long.parseLong(val);
        } catch (Throwable t) {
            log.warn("Missing config key: " + key + ". Using 0.");
        }
        return rez;
    }

	public static int daysBetween(Date date1, Date date2) {
		long msDiff;
		msDiff = date1.getTime() - date2.getTime();
		int daysDiff = Math.round(msDiff / MILLIS_PER_DAY);
		return daysDiff;
	}

	public static boolean validateNumbersAndCommasOnly(String value) throws Exception {
		String pattern = "^[0-9,]+$";
		String v = value;
		if (!v.matches(pattern) || v.trim().equals("")) {
			return false;
		}
		return true;
	}

    /**
     * Get contry code by ip with unix command 
     * @param ip  for getting country code 
     * @return country code 
     */ 
    public static String getCountryCodeByIp(String ip) {		
    	String countryCode = null; 
   	 	try {
   	 		String[] geoLocationFields = getGeoLocationFields(ip);
   	 		if (geoLocationFields != null) {
   	 			countryCode = geoLocationFields[0]; 
       		} else {       			
       			log.warn("Not detect Country! Returning hardcoded " + ConstantsBase.COUNTRY_A2_UNITED_KINGDOM + " country code");
       			countryCode = ConstantsBase.COUNTRY_A2_UNITED_KINGDOM;       			
       		}  		 
   	 	} catch (Exception e) {
   	 		log.warn("Error to extract countryCode with running unix geoiplookup command, res: " + e);
   	 	}     	 
   	 	return countryCode;
    } 

    public static String getPropertyByHostRequest(HttpServletRequest request, String key, String defval) {
        return getPropertyByHost(request, key, defval);
    }

    /**
     * Get property by host
     * For taking differents urls for each host
     */
    public static String getPropertyByHost(HttpServletRequest request, String key, String defval) {
        String text = null;
        try {
            String host = new URL(request.getRequestURL().toString()).getHost();
            String configurationFile = configurationFilePath;
            if (host.indexOf(ConstantsBase.HOST_ANYOPTION) > -1) {
                // handle sub domain
                String confFile = "_" + ConstantsBase.CONFIG_HOST_ANYOPTION;
                if (host.startsWith(ConstantsBase.SUBDOMAIN_MOBILE_ANYOPTION + ".")) {
                    confFile = "_" + ConstantsBase.SUBDOMAIN_MOBILE_ANYOPTION + confFile;
                }
                configurationFile += confFile;
            }
            if (host.indexOf(ConstantsBase.HOST_ANYOPTION_IT) > -1) {
                // handle sub domain
                String confFile = "_" + ConstantsBase.CONFIG_HOST_ANYOPTION_IT;
                if (host.startsWith(ConstantsBase.SUBDOMAIN_MOBILE_ANYOPTION + ".")) {
                    confFile = "_" + ConstantsBase.SUBDOMAIN_MOBILE_ANYOPTION + confFile;
                }
                configurationFile += confFile;
            }
            if (host.indexOf(ConstantsBase.HOST_ANYOPTION_TR) > -1) {
                String confFile = "_" + ConstantsBase.CONFIG_HOST_ANYOPTION_TR;
                if (host.startsWith(ConstantsBase.SUBDOMAIN_MOBILE_ANYOPTION + ".")) {
                    confFile = "_" + ConstantsBase.SUBDOMAIN_MOBILE_ANYOPTION + confFile;
                }
                configurationFile += confFile;
            }
            if (host.indexOf(ConstantsBase.HOST_MIOPCIONES) > -1) {
                configurationFile += "_" + ConstantsBase.CONFIG_HOST_MIOPCIONES;
            }

            if (host.indexOf(ConstantsBase.HOST_TLV_TRADE) > -1) {
                configurationFile += "_" + ConstantsBase.CONFIG_HOST_TLV_TRADE;
            }

            ResourceBundle properties = ResourceBundle.getBundle(configurationFile);
            if (key == null || key.trim().equals("")) {
                return "";
            }
            text = properties.getString(key);
        } catch (MalformedURLException e) {
            log.error("Can't get property by host name. Default value is :" + defval);
            return defval;
        } catch (MissingResourceException e) {
            log.error("Missing value for key :" + key + " Default value is :" + defval);
            return defval;
        }
        return text.trim();
    }

    public static boolean containsTestDomain(String argument) {
    	// for testing
    	return argument.toLowerCase().endsWith("anyoption.com") || argument.toLowerCase().endsWith("etrader.co.il");
    }

    public static String getStackTrace() {
        StackTraceElement[] st = Thread.currentThread().getStackTrace();
        String ls = System.getProperty("line.separator");
        StringBuffer sb = new StringBuffer();
        sb.append(ls).append("Stack trace:").append(ls);
        for (int i = 0; i < st.length; i++) {
            sb
                .append(st[i].getClassName())
                .append(".")
                .append(st[i].getMethodName())
                .append("(")
                .append(st[i].getFileName())
                .append(":")
                .append(st[i].getLineNumber())
                .append(")")
                .append(ls);
        }
        return sb.toString();
    }

    /**
     * The function check if the writer has sales type department id equals to parameter 'salesTypeDeptId'
     * @param writerId
     * @param salesTypeDeptId
     * @return true if the writer has relevant sales type department id.
     */
    public static Boolean isHasRetentionDepartmentId(long writerId, long salesTypeDeptId) {
		try {
			Writer writer = new Writer();
			WritersManagerBase.getSalesTypeDepartmentId(writerId, writer);
			if (writer.getSalesType() == ConstantsBase.SALES_TYPE_RETENTION && writer.getSalesTypeDepartmentId() == salesTypeDeptId) {
				return true;
			}
		} catch (SQLException e) {
			log.error("Can't get sales type department", e);
		}
    	return false;
	}

    public static void sendEmailMsg(String from, String to, String subject, String body){
		final String username = CommonUtil.getProperty("email.uname");
		final String password = CommonUtil.getProperty("email.pass");

		Properties serverProperties = new Properties();
		serverProperties.put("mail.smtp.auth", "true");
		serverProperties.put("mail.smtp.starttls.enable", "false");
		serverProperties.put("mail.smtp.host", CommonUtil.getProperty("email.server"));

		Session session = Session.getInstance(serverProperties, new javax.mail.Authenticator() {
			@Override
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(username, password);
			}
		});

		try {
 			Message message = new MimeMessage(session);
			message.setFrom(new InternetAddress(from));
			message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(to));
			message.setSubject(subject);
			message.setText(body);

			Transport.send(message);
		} catch (MessagingException e) {
			log.debug("Can't send message", e);
		}
	}
    
    public static void sendEmailMsgHtml(String from, String to, String subject, String body){
		final String username = CommonUtil.getProperty("email.uname");
		final String password = CommonUtil.getProperty("email.pass");

		Properties serverProperties = new Properties();
		serverProperties.put("mail.smtp.auth", "true");
		serverProperties.put("mail.smtp.starttls.enable", "false");
		serverProperties.put("mail.smtp.host", CommonUtil.getProperty("email.server"));

		Session session = Session.getInstance(serverProperties, new javax.mail.Authenticator() {
			@Override
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(username, password);
			}
		});

		try {
			MimeMessage message = new MimeMessage(session);
			message.setHeader("Content-Type", "text/html; charset=UTF-8");
			message.setHeader("Content-Transfer-Encoding", "base64");
			message.setFrom(new InternetAddress(from));
			message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(to));
			message.setSubject(subject);
			message.setContent(body, "text/html; charset=UTF-8");
			Transport.send(message); 
		} catch (MessagingException e) {
			log.debug("Can't send message", e);
		}
		log.debug("mail sent");
	}

	public static String convertToCSV(ArrayList<?> list){
		if(list== null || list.size()==0) {
			return null;
		}

		StringBuffer sb = new StringBuffer();
		Iterator<?> it = list.iterator();
		while (it.hasNext()) {
			sb.append(it.next());
			if (it.hasNext()) {
				sb.append(",");
			}
		}
		return sb.toString();
	}

    public static String formatLevelByDecimalPoint(Double d, long dpoint) {
        if (d == null) {
            return "";
        }
        String format = "###,###,##0.";
        for (int i = 0; i < dpoint; i++) {
            format += "0";
        }
        DecimalFormat f = new DecimalFormat(format);
        f.setDecimalSeparatorAlwaysShown(true);

        // Cut '.' if needed
        String res = f.format(d);
        int lastIndex = res.length() - 1;
        if ( res.endsWith(".") ) {
            res = res.substring(0, lastIndex);
        }
        return res;
    }

    public static String formatLevel(Double d, long decimal) {
        if (null == d) {
            return "";
        }
        String pattern = "###,###,##0";
        if (decimal > 0) {
            pattern += ".";
            for (int i = 0; i < decimal; i++) {
                pattern += "0";
            }
        }
        return new DecimalFormat(pattern).format(d);
    }

    public static Timestamp convertToTimeStamp(Date d) {
		return convertToTimeStamp(d, false);
	}


   public static Timestamp convertToTimeStamp(Date d, boolean endOfSecond) {
		if (d == null) {
			return null;
       }
		Timestamp ts = new Timestamp(d.getTime());
		if (endOfSecond) {
			ts.setNanos(999999999);
		}
		return ts;
	}

   /**
    * get boolean value as string.
    * @param param
    * @return if true - "1", else "0".
    */
   public static String getBooleanAsString(boolean param) {
       if (param) {
           return ConstantsBase.STRING_TRUE;
       }
       return ConstantsBase.STRING_FALSE;
   }

   /**
    * get boolean value as string.
    * @param param
    * @return if true - "Yes", else "No".
    */
   public static String getBooleanAsStringYesNo(boolean param) {
       if (param) {
           return ConstantsBase.STRING_YES;
       }
       return ConstantsBase.STRING_NO;
   }

   public static String getIPAddress(HttpServletRequest req) {	   
       String ipNotFound = "IP NOT FOUND!";
       String ip = null;
       if (req != null) {
           ip = req.getHeader("x-forwarded-for");
           if (null == ip) {
               ip = req.getRemoteAddr();
           }
           log.info("Full ip from header = " + ip);
           int indexOfLastComma = ip.indexOf(',');    // for proxy users(list of ip's) we need only the first ip in the list
           if (indexOfLastComma != -1) {
               ip = ip.substring(0, indexOfLastComma).trim();
               log.info("First ip only = " + ip);
           }
       }
       return ip == null ? ipNotFound : ip ;
   }

   public static String getEtsServerName() {
       if (null == etsServerName) {
           etsServerName = getProperty("ets.server");
       }
       return etsServerName;
   }

   public static boolean setAOCookieValue(HttpServletRequest request, HttpServletResponse response, String paramName, String pramValue){

       boolean res = true;
       Cookie aoCookie = null;

       try {
           aoCookie = getCookie(request, AO_COOKIE_NAME);
           String value = "";
           String newParam = paramName + "|" + pramValue + "V3L";
           boolean isSet = false;
           if (aoCookie == null){
               value = newParam;
               addCookie(AO_COOKIE_NAME, value, request, response, AO_COOKIE_NAME);
           } else {
               log.debug("Found AO cookie with value:" + aoCookie.getValue());
               String[] parts = aoCookie.getValue().split("V3L");
               for (String s : parts){
                   String[] paramParts = s.split("\\|");
                   if (paramParts[0].contains(paramName)){
                       value = value + newParam;
                       isSet = true;
                   } else {
                       value = value + s + "V3L";
                   }
               }
               if(!isSet){
                   value = value + newParam;
               }
               addCookie(AO_COOKIE_NAME, value, request, response, AO_COOKIE_NAME);
           }

    } catch (Exception e) {
        log.error("When set AO cookie",e);
        deleteCookie(aoCookie, response);
        res = false;
    }
       return res;
   }

   public static String getAOCookieValue(HttpServletRequest request, HttpServletResponse response, String paramName){
        String paramValue = "";
        Cookie aoCookie = null;
        try {
            aoCookie = getCookie(request, AO_COOKIE_NAME);
            if (aoCookie == null) {
                log.debug("Not Found AO cookie!!!");
                return paramValue;
            } else {
                log.debug("Found AO cookie with value:" + aoCookie.getValue());
                String[] parts = aoCookie.getValue().split("V3L");
                for (String s : parts) {
                    String[] paramParts = s.split("\\|");
                    if (paramParts[0].contains(paramName)) {
                        paramValue = paramParts[1];
                    }
                }
            }
        } catch (Exception e) {
            log.error("When get AO cookie", e);
            deleteCookie(aoCookie, response);
        }
        return paramValue;
    }

	/**
    * Round a double value to have "decimals" number of digits after
    * the decimal point.
    * For example roundDouble(1.12345, 3) = 1.123
    *
    * @param val
    * @param decimals
    * @return
    */
   public static double roundDouble(double val, long decimals) {
       BigDecimal bd = new BigDecimal(Double.toString(val));
       MathContext mc = new MathContext(bd.precision() - bd.scale() + (int) decimals, RoundingMode.HALF_UP);
       return bd.round(mc).doubleValue();
   }

	/**
    * Round a double value to have "decimals" number of digits after
    * the decimal point.
    * For example roundDouble(1.12345, 3) = 1.123
    *
    * @param val
    * @param decimals
    * @return
    */
   public static double ceilingDouble(double val, long decimals) {
       BigDecimal bd = new BigDecimal(Double.toString(val));
       MathContext mc = new MathContext(bd.precision() - bd.scale() + (int) decimals, RoundingMode.CEILING);
       return bd.round(mc).doubleValue();
   }

	/**
	 * This method checks the Israeli personal ID number and returns error key if the number is wrong.
	 *
	 * @param idNum
	 * @return the error key if there is something wrong with the ID or <code>null</code> if ID number is OK.
	 */
	public static String validateIdNum(String idNum) {
		try {
			long id = Long.parseLong(idNum);
			if (id < 1000) {
				return "error.idnum";
			}
		} catch (NumberFormatException e) {
			return "error.idnum";
		}

		if (idNum.length() < 9) {
			return "error.idnumLength";
		}

		int idNum1 = Integer.parseInt(idNum.substring(0, 1)) * 1;
		int idNum2 = Integer.parseInt(idNum.substring(1, 2)) * 2;
		int idNum3 = Integer.parseInt(idNum.substring(2, 3)) * 1;
		int idNum4 = Integer.parseInt(idNum.substring(3, 4)) * 2;
		int idNum5 = Integer.parseInt(idNum.substring(4, 5)) * 1;
		int idNum6 = Integer.parseInt(idNum.substring(5, 6)) * 2;
		int idNum7 = Integer.parseInt(idNum.substring(6, 7)) * 1;
		int idNum8 = Integer.parseInt(idNum.substring(7, 8)) * 2;
		int idNum9 = Integer.parseInt(idNum.substring(8, 9)) * 1;

		if (idNum1 > 9) {
			idNum1 = (idNum1 % 10) + 1;
		}
		if (idNum2 > 9) {
			idNum2 = (idNum2 % 10) + 1;
		}
		if (idNum3 > 9) {
			idNum3 = (idNum3 % 10) + 1;
		}
		if (idNum4 > 9) {
			idNum4 = (idNum4 % 10) + 1;
		}
		if (idNum5 > 9) {
			idNum5 = (idNum5 % 10) + 1;
		}
		if (idNum6 > 9) {
			idNum6 = (idNum6 % 10) + 1;
		}
		if (idNum7 > 9) {
			idNum7 = (idNum7 % 10) + 1;
		}
		if (idNum8 > 9) {
			idNum8 = (idNum8 % 10) + 1;
		}
		if (idNum9 > 9) {
			idNum9 = (idNum9 % 10) + 1;
		}

		int sumval = idNum1 + idNum2 + idNum3 + idNum4 + idNum5 + idNum6 + idNum7 + idNum8 + idNum9;

		sumval = sumval % 10;

		if (sumval > 0) {
			return "error.idnum";
		}

		return null;
	}

   /**
    * Get array into string
    * @param temp
    * @return String
    */
   public static String getArrayToString(List<Long> temp) {
       String list = ConstantsBase.EMPTY_STRING;
       for (int i = 0; i < temp.size(); i++) {
           list += temp.get(i) + ",";
       }
       return list.substring(0,list.length()-1);
   }

   /**
    *
    * @param map
    * @return sorted by VALUE SortedSet
    */
   public static <K,V extends Comparable<? super V>>
   SortedSet<Map.Entry<K,V>> entriesSortedByValues(Map<K,V> map) {
       SortedSet<Map.Entry<K,V>> sortedEntries = new TreeSet<Map.Entry<K,V>>(
           new Comparator<Map.Entry<K,V>>() {
               @Override
               public int compare(Map.Entry<K,V> e1, Map.Entry<K,V> e2) {
                   return e1.getValue().compareTo(e2.getValue());
               }
           }
       );
       sortedEntries.addAll(map.entrySet());
       return sortedEntries;
   }

   public static boolean isCopyopWriter(long writerId) {
	   return writerId == Writer.WRITER_ID_COPYOP_WEB || writerId == Writer.WRITER_ID_COPYOP_MOBILE || writerId == Writer.WRITER_ID_CO_MINISITE;
   }

   public static String buildNotifyForInvestment(long oppId,
													double investmentId,
													double amount,
													long type,
													String amountForDisplay,
													String cityNameInEnglish,
													long countryId,
													long marketId,
													int productTypeId,
													long userSkinId,
													double level,
													long time,
													String globeLat,
													String globeLong,
													double aboveTotal,
													double belowTotal,
													long flooredAmount,
													CopyOpInvTypeEnum cpOpInvType) {
	   return new StringBuilder("investment_").append(oppId)
						   			.append("_").append(investmentId)
						   			.append("_").append(amount)
						   			.append("_").append(type)
						   			.append("_").append(amountForDisplay)
						   			.append("_").append(cityNameInEnglish)
						   			.append("_").append(countryId)
						   			.append("_").append(marketId)
						   			.append("_").append(productTypeId)
						   			.append("_").append(userSkinId)
						   			.append("_").append(level)
						   			.append("_").append(time)
						   			.append("_").append(globeLat)
						   			.append("_").append(globeLong)
						   			.append("_").append(aboveTotal)
						   			.append("_").append(belowTotal)
						   			.append("_").append(flooredAmount)
						   			.append("_").append(cpOpInvType.getCode())
						   			.toString();
   }

   /**
	 * geographic location by ip
	 * 
	 * assumptions: The string contain ", " after ": " 
	 * @param ip
	 * @return fields <b>if</b> found = [country, region, city, postalCode, latitude, longitude, metroCode, areaCode]
	 * <br/><b>else</b> return null
	 */
	public static String[] getGeoLocationFields(String ip) {
		String[] geoLocationFields = null;
		
		try {
			String geographicLocation = UnixUtil.runUnixCommand(ConstantsBase.UNIX_GEO_IP_LOOKUP_COMMAND + ip);
			log.info("after unix geo city ip " + ip + " lookup command, res: " + geographicLocation);			
			
			if (!isParameterEmptyOrNull(geographicLocation) && geographicLocation.contains(": ")) {
				String[] locationSplit = geographicLocation.split(": ");
				if (locationSplit.length > 1 && locationSplit[1].contains(",")) {
					geoLocationFields = locationSplit[1].split(", ");	
				}				
			}		
		} catch (Exception e) {
			log.error("ERROR! getGeoLocationFields " , e);
		}
		
		return geoLocationFields;
	}
	
	/**
	 * @param ip
	 * @return true if from closed CA regions, if more countries are added, the constant needs to be refactored to DB config with cache
	 */
	public static boolean isRegionForbiden(String ip) {
		boolean isRegionForbiden = false;
		String[] geoLocationFields = getGeoLocationFields(ip);
		if (geoLocationFields != null && 
			geoLocationFields.length > 1 &&
			geoLocationFields[0].equalsIgnoreCase(ConstantsBase.COUNTRY_A2_CANADA) &&
			ConstantsBase.COUNTRY_REGION_CA_FORBIDEN.indexOf(geoLocationFields[1].toUpperCase()) > -1) {		
			isRegionForbiden = true;
		}
		
		return isRegionForbiden;
	}	
	
	public static Long getCountryId(String ip){
		String countryCode = CommonUtil.getCountryCodeByIp(ip);
		Long countryId = null;
		if (!CommonUtil.isParameterEmptyOrNull(countryCode)){
			try {
				countryId = CountryManagerBase.getIdByCode(countryCode);
			} catch (SQLException e) {
				log.warn("Error! problem getting countryId by code : " + e);
				countryId = null;
			}
		}
		if(null !=countryId && countryId == 0){
			countryId = null;
		}
		return countryId;
	}
    
    //TODO: test it (it was just unit tested).
    /**
     * This method check the number by the common Israeli id algorithm.
     * @param id
     * @return true if the given String is a valid Israeli id number. false otherwise.
     */
    public static boolean isIsraelIDValid(String id) {
    	log.info("Start, isIsraelIDValid with: " + id);
    	if (CommonUtil.isParameterEmptyOrNull(id) || id.length() != 9 || !isNaturalNumber(id)) {
    		return false;
    	}
    	
    	int digitWeightValue;
    	int sumDigits = 0;
    	for (int i = 0; i < 9; i++) {
    		digitWeightValue = Character.getNumericValue(id.charAt(i)) * ((i % 2) + 1);
    		sumDigits += (digitWeightValue > 9) ? digitWeightValue - 9 : digitWeightValue;
    	}
    	return (sumDigits % 10 == 0);
    }
    
    /**
     *  
     * @param str
     * @return true if the given String is a natural number that <= 2^63 - 1 = 9223372036854775807 (Max long value). false otherwise.
     */
    public static boolean isNaturalNumber(String str) {  
		boolean isNaturalNum = true;
		long num = 0;
		try	{
			num = Long.parseLong(str);  
		} catch(NumberFormatException nfe)	{  
			isNaturalNum = false;
		}  
		return isNaturalNum && (num >= 0);  
	}
    
	public static String clobToString(Clob clob) throws SQLException {
		if(clob == null) {
			log.error("Empty description !");
			return "";
		}
		Reader reader = null;
		try{
			reader = clob.getCharacterStream();
		    int size;
		    char[] cbuff = new char[256];
		    StringBuilder  targetString = new StringBuilder();
		    while ((size = reader.read(cbuff)) != -1) {
		        targetString.append(cbuff, 0, size);
		    }
		    return targetString.toString();
		} catch(IOException ex) {
			log.error("Can't read clob object", ex);
			return "";
		} finally {
			if(reader != null) {
				try { 
					reader.close();
				} catch (IOException io) {/*ignore*/}
			}
		}
	}
	
	/**
	 * 
	 * 
	 * @param user
	 * @return true if it's etrader user && the user registered after  ENUM_ETRADER_BONUS_RESTRICT_DATE &&  ENUM_ETRADER_BONUS_RESTRICT set to true.
	 * false - otherwise.
	 */
	public static boolean isBonuseUserRestricted(User user) {
		boolean isRestricted = false;
		
		try {
			String dateString = CommonUtil.getEnum(ConstantsBase.ENUM_ETRADER_BONUS, ConstantsBase.ENUM_ETRADER_BONUS_RESTRICT_DATE);
			Boolean isEtraderBonusRestricted = new Boolean(CommonUtil.getEnum(ConstantsBase.ENUM_ETRADER_BONUS, ConstantsBase.ENUM_ETRADER_BONUS_RESTRICT));
			if (CommonUtil.isParameterEmptyOrNull(dateString)) {
				return false;
			}
			
			Date startDate = new SimpleDateFormat("dd/MM/yyyy").parse(dateString);
			if (user.getSkinId() == Skin.SKIN_ETRADER &&
					user.getTimeCreated().after(startDate) &&
					isEtraderBonusRestricted) {
				isRestricted = true;
			}
		} catch (Exception e) {
			log.error(e);
			return false;
		}
		return isRestricted;
	}
	
	/**
	 * return true if the enums should be refreshed , false otherwise
	 * @return boolean
	 * 	In case the value isn't in properties , need to take default value.
	 *  Used in the method refreshEnums
	 */
	protected static boolean shouldRefreshEnums() {
        if (null == enumerators) {
            return true;
        }
		boolean needToRefresh = false;
		long curTime = Calendar.getInstance().getTimeInMillis();
		String refreshIntervalStr = getProperty(REFRESH_PERIOD ,ConstantsBase.DEFAULT_ENUMS_REFRESH );
		long refreshInterval = Long.parseLong(refreshIntervalStr);
		needToRefresh =  curTime - lastRefresh > refreshInterval;
		return needToRefresh;
	}
	
	protected static void refreshEnums() throws SQLException {
		if (shouldRefreshEnums()) {
			Connection con = DBUtil.getDataSource().getConnection();
            try {
                refreshEnums(con);
            } finally {
                con.close();
            }
		}
	}
	
	protected static void refreshEnums(Connection conn) throws SQLException {
		long curTime = new Date().getTime();
		if (shouldRefreshEnums()) {
			enumerators = GeneralDAO.getAllEnumerators(conn);
			lastRefresh = curTime;
		}
	}
	
	public static String getEnum(String enumerator, String code) throws SQLException {
		refreshEnums();
		for (int i = 0; i < enumerators.size(); i++) {
			Enumerator e = (Enumerator) enumerators.get(i);
			if (e.getEnumerator().equals(enumerator) && e.getCode().equals(code))
				return e.getValue();
		}
		return null;
	}
	
	public static ArrayList<Enumerator> getEnum(String enumerator) throws SQLException {
		ArrayList<Enumerator> l = new ArrayList<Enumerator>();
		refreshEnums();
		for (int i = 0; i < enumerators.size(); i++) {
			Enumerator e = (Enumerator) enumerators.get(i);
			if (e.getEnumerator().equals(enumerator))
				l.add(e);
		}
		return l;
	}
	
	/**
	 * Builds a dropdown containning list of <SelectItem>
	 * @param String listName - The dropdown list name
	 * @return ArrayList<SelectItem> - returns an ArrayList of <SelectItems>
	 */
	public static ArrayList<SelectItem> enumSI(String listName) throws SQLException {
		ArrayList<SelectItem> dropDown = new ArrayList<SelectItem>();
		//getting the dropdown list from Enumerators table
		ArrayList<Enumerator> enumsList = getEnum(listName);
		for ( int i=0; i<enumsList.size(); i++) {
			String tmp = ((Enumerator)enumsList.get(i)).getValue();
			long tmpLong  = new Long(tmp).longValue();
			dropDown.add(new SelectItem(tmpLong, ((Enumerator)enumsList.get(i)).getCode()));
		}
		return dropDown;
	}
	
	public static String getEnum(Connection conn, String enumerator, String code) throws SQLException {
		refreshEnums(conn);
		for (int i = 0; i < enumerators.size(); i++) {
			Enumerator e = (Enumerator) enumerators.get(i);
			if (e.getEnumerator().equals(enumerator) && e.getCode().equals(code))
				return e.getValue();
		}
		return null;
	}
	
	public static String getRequestParameter(HttpServletRequest request, String key) {
		HttpSession session = request.getSession();
		String parameter = (String) session.getAttribute(key);
		log.log(Level.DEBUG, "parameter is: " + parameter);
		if (parameter != null) {
			return parameter;
		}

		Cookie[] cs = request.getCookies();
		Cookie cookie = null;
		if (cs != null) {
			for (int i = 0; i < cs.length; i++) {
				if (cs[i].getName().equals(key)) {
					log.log(Level.DEBUG, "Found parameter in cookie!");
					cookie = cs[i];
				}
			}
		}
		if (cookie != null && cookie.getValue() != null
				&& !cookie.getValue().equals("")
				&& cookie.getValue().indexOf("null") == -1) {
				return cookie.getValue();
		}

		return null;
	}
	
	public static String generateRandomKey() {
		char[] charsArray = { 'q', 'w', 'e', 't', 'y', 'u', 'i', 'o', 'p', 'a',
				's', 'd', 'f', 'g', 'h', 'j', 'k', 'l', 'z', 'x', 'c', 'v',
				'b', 'n', 'm', '1', '2', '3', '4', '5', '6', '7', '8', '9', '0' };
		String resKey1 = "";
		String resKey2 = "";
		for (int j = 0; j < 8; j++) {
			resKey1 += charsArray[new Random().nextInt(charsArray.length)];
			resKey2 += charsArray[new Random().nextInt(charsArray.length)];
		}
		return resKey1 + new Date().getTime() + resKey2;
	}
	
	public static String generateActivationLink(long userId, long hashType, long skinId) throws SQLException {
		String hashKey = generateRandomKey();
		String link = null;

		if (skinId == Skin.SKIN_ETRADER_INT) {
			link = CommonUtil.getConfig("homepage.url.etrader") + "jsonService/regulationActivationLink?"
					+ ConstantsBase.REG_HASH_KEY + "=" + hashKey;
			hashType = UserRegulationBase.HASH_TYPE_ET_UNSUSPEND;
		} else {
			link = CommonUtil.getConfig("homepage.url") + "jsonService/regulationActivationLink?"
					+ ConstantsBase.REG_HASH_KEY + "=" + hashKey;
		}

		UserRegulationManager.insertHashKey(userId, hashKey, hashType);
		log.info("Generated link: " + link + " for user with ID: " + userId);
		return link;
	}

	public static String generateAOActivationLinkAndInsertLinkIssue(long userId, long skinId) throws SQLException{
		IssuesManagerBase.insertIssueUserRegulationRestricted(userId, IssuesManagerBase.ISSUE_ACTION_TYPE_RESTRICTED_SCORE_GROUP, Writer.WRITER_ID_AUTO);
		return generateActivationLink(userId, UserRegulationBase.HASH_TYPE_AO_UNRESTRICTED, skinId);
	}
	
	public static String generateAOActivationLinkAfterFaildQuest(long userId, long skinId) throws SQLException{		
		return generateActivationLink(userId, UserRegulationBase.HASH_TYPE_AO_UNRESTRICTED, skinId);
	}
	
	public static String generateAOActivationLinkForBlockedUsers(long userId, long skinId) throws SQLException {
		return generateActivationLink(userId, UserRegulationBase.HASH_TYPE_AO_UNBLOCKTRESHOLD, skinId);
	}
	
    public static String generateEtActivationLink(long userId, long suspendReasonId, long treshold, boolean isUserRequest, long skinId) throws SQLException {
    	
    	if ((suspendReasonId == UserRegulationBase.ET_SUSPEND_TRESHEHOLD_LOW_SCORE_GROUP || suspendReasonId == UserRegulationBase.ET_SUSPEND_TRESHEHOLD_MEDIUM_SCORE_GROUP || suspendReasonId == UserRegulationBase.ET_SUSPEND_LOW_SCORE_GROUP) && isUserRequest) {
    		Issue issue = UserRegulationManager.getSuspendedIssueId(userId);
    	  if (issue != null) {
    		  String comments = "";
    		  if (suspendReasonId == UserRegulationBase.ET_SUSPEND_LOW_SCORE_GROUP) {
    			  comments = "User is suspended due to low score, activation link sent";
    		  } else if (suspendReasonId == UserRegulationBase.ET_SUSPEND_TRESHEHOLD_LOW_SCORE_GROUP || suspendReasonId == UserRegulationBase.ET_SUSPEND_TRESHEHOLD_MEDIUM_SCORE_GROUP) {
    			  comments = "User is suspended due to " + treshold/100 + " total investments loss, activation link sent";
    		  }
    		IssuesManagerBase.insertLinkIssueAction(userId, issue, comments, String.valueOf(IssuesManagerBase.ISSUE_ACTION_TYPE_LOSS_THRESHOLD_SUSPEND_EMAIL), String.valueOf(IssuesManagerBase.ISSUE_CHANNEL_EMAIL), suspendReasonId, String.valueOf(IssuesManagerBase.ISSUE_PRIORITY_LOW));
    	   } else {
    		   log.info("Suspend issue ID is NULL for user with ID: " + userId);
    	   }
    	}
    	return generateActivationLink(userId, UserRegulationBase.HASH_TYPE_ET_UNSUSPEND, skinId);
    }
   
    public static String getDateTimeFormatDisplay(Date d, String timeZoneName, String delimiter, String formater) {
		if (d == null) {
			return "";
		}
        TimeZone tz = null;
        if (null != timeZoneName) {
            tz = TimeZone.getTimeZone(timeZoneName);
        }

		SimpleDateFormat sd2 =  new SimpleDateFormat(formater);
        if (null != tz) {
            sd2.setTimeZone(tz);
        }
		return  delimiter + sd2.format(d);
	}
    
    // Encode message for register new User at anyoption, backend, etrader and copyop platforms (UNICODE)
	public static String encodeRegisterMessage(String message) {
		StringBuilder newMessage = new StringBuilder();
		newMessage.append("@U");

		for (char c : message.toCharArray()) {
			String charHex = String.format("%1$4s", Integer.toHexString(c));
			newMessage.append(charHex);
		}

		return newMessage.toString();
	}
	
    public static String getGclid(String value) {
    	if(value!=null && value.length()>0) {
    		return ConstantsBase.GCLID + "_" + value ;
    	} else {
    		return null;
    	}
    }
    
	/**
	 * include user offset.
	 * @param user
	 * @return true if the user cannot make withdraw because it's weekend.
	 */
	public static boolean isWithdrawalWeekendsNotAvailable(User user) {
		boolean result = false;
		try { 	
			int userUtcOffset = TimeZone.getTimeZone(user.getUtcOffset()).getRawOffset();
			Date userTime = new Date(new Date().getTime() + userUtcOffset);
			
			String weekendString = CommonUtil.getEnum(ConstantsBase.ENUM_WITHDRAWAL_WEEKEND, ConstantsBase.ENUM_WITHDRAWAL_WEEKEND_ANYOPTION);
			if (user.getSkinId() == Skin.SKIN_ETRADER) {
				weekendString = CommonUtil.getEnum(ConstantsBase.ENUM_WITHDRAWAL_WEEKEND, ConstantsBase.ENUM_WITHDRAWAL_WEEKEND_ETRADER);
			}
			log.debug("Weekend String: " + weekendString);
			String[] weekend = weekendString.split("-");
			int[] date = getDateToSet(weekend[0]);		
	    	Date anyoptionWeekendStart = getDateToSet(date[0], date[1], date[2], true);	    	    
	    	Date anyoptionWeekendEnd = getDateToSet(anyoptionWeekendStart, Integer.valueOf(weekend[1]).intValue());
		
	    	result = userTime.after(anyoptionWeekendStart) && userTime.before(anyoptionWeekendEnd);
	    	log.debug("Weekend check transaction date: " + userTime.toString() + " between " + anyoptionWeekendStart.toString() + " and " + anyoptionWeekendEnd.toString());
		} catch (Exception e) {
			log.error("ERROR! general exception, isWithdrawalWeekendsAvailable", e);
			result = false;
		}
		return result;
	}
	
	
	 public static Date getDateToSet(int dayOfWeek, int hourOfDay, int minOfHour, boolean first) {
	    Calendar cal = Calendar.getInstance();
    	if (first) {
    		cal.setFirstDayOfWeek(dayOfWeek);
    	}
    	cal.set(Calendar.DAY_OF_WEEK,dayOfWeek);
    	cal.set(Calendar.HOUR_OF_DAY,hourOfDay);
    	cal.set(Calendar.MINUTE,minOfHour);
    	cal.set(Calendar.SECOND,0);
    	cal.set(Calendar.MILLISECOND,0); 
	    	
    	return cal.getTime();
	 }
	 
	 public static Date getDateToSet(Date date, int hourOfDay) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.add(Calendar.HOUR_OF_DAY, hourOfDay);		
		return cal.getTime();
	}   
	    
	 public static int[] getDateToSet(String str) {
    	int[] dates = new int[3];
    	String sac[] = str.split(";");   	
    	dates[0] = Integer.valueOf(sac[0]);
    	dates[1] = Integer.valueOf(sac[1]);
    	dates[2] = Integer.valueOf(sac[2]);
    					
    	return dates;   	
    }
	 
	/**
	 * get product type from opp type id 
	 * @param opportunityTypeId opportunity Type Id
	 * @return product type id
	 */
	public static int getProductTypeId(int opportunityTypeId) {
    	switch (opportunityTypeId) {
		case (int) Opportunity.TYPE_REGULAR:
			return OpportunityType.PRODUCT_TYPE_BINARY;
		case (int) Opportunity.TYPE_ONE_TOUCH:
			return OpportunityType.PRODUCT_TYPE_ONE_TOUCH;
		case (int) Opportunity.TYPE_OPTION_PLUS:
			return OpportunityType.PRODUCT_TYPE_OPTION_PLUS;
		case (int) Opportunity.TYPE_BINARY_0_100_ABOVE:
		case (int) Opportunity.TYPE_BINARY_0_100_BELOW:
			return OpportunityType.PRODUCT_TYPE_BINARY_0_100;
		default:
			return OpportunityType.PRODUCT_TYPE_BINARY;
		}
    }
	 
	 public static String getTimeFromMillis(long millis) {
		 long second = (millis / 1000) % 60;
		 long minute = (millis / (1000 * 60)) % 60;
		 long hour = (millis / (1000 * 60 * 60)) % 24;

		 String time = String.format("%02d:%02d:%02d %d", hour, minute, second, millis);
		 return time;
	 }
	 
	/**
	 * This method adds a day to the date received
	 * @param d - date received
	 * @return  -date received + 1 day
	 */
	public static Date addDay(Date d) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(d);
		cal.add(Calendar.DATE, 1);
		return cal.getTime();
	}
	
	/**
	 * Return currency from the currenciesList by id
     *
	 * @param currencyid currency id
	 * @return currency instance
	 */
	public static Currency getCurrencyById(ArrayList<Currency> currenciesList, long currencyId) {
		Currency currency = null;
		for (Currency c : currenciesList) {
			if (c.getId() == currencyId) {
				currency = c;
				break;
			}
		}
		return currency;
	}

    /**
     * Round a BigDecimal to certain order (power of 10).
     *
     * eg:
     * round(new BigDecimal("123.456"), new BigDecimal("100"))   = 100
     * round(new BigDecimal("123.456"), new BigDecimal("10"))    = 120
     * round(new BigDecimal("123.456"), new BigDecimal("1"))     = 123
     * round(new BigDecimal("123.456"), new BigDecimal("0.1"))   = 123.5
     * round(new BigDecimal("123.456"), new BigDecimal("0.01"))  = 123.46
     * round(new BigDecimal("123.456"), new BigDecimal("0.001")) = 123.456
     *
     * @param number
     * @param roundOrder
     * @return
     */
    public static BigDecimal round(BigDecimal number, BigDecimal roundOrder) {
        BigDecimal result = number.divide(roundOrder);
        int rmp = result.precision() - result.scale();
        if (rmp < 0) {
            rmp = 1;
        }
        result = result.round(new MathContext(rmp));
        result = result.multiply(roundOrder);
        return result;
    }	
    
    /**
     * "Escapes" a String of any special characters in it
     * 
     * @param toBeEscapedStr
     * @return an as is String value
     */
    public static String appendDQ(String toBeEscapedStr) {
	    return "\"" + toBeEscapedStr + "\"";
	}
    
	public static String getPredefinedDepositAmountJsonMap() {
		return GeneralManager.getPredefinedDepositAmountJsonMap();
	}
	
	public static HashMap<Long, String> getPredefinedDepositAmount() {
		return GeneralManager.getPredefinedDepositAmount();
	}
	
	/**
	 * @param value
	 * @return 0 if it's not safe to convert long to int.
	 */
	public static int safeLongToInt(long value) {
	    if (value < Integer.MIN_VALUE || value > Integer.MAX_VALUE) {
	    	log.warn("WARNING,  not safe to convert long to int");
	    	return 0;
	    }
	    return (int) value;
	}
	
	/**
	 * Gets a string and capitalize the first letter fir each word.
	 * @param str the string
	 * @return the new string with the capitalized text
	 */
	public static String capitalizeFirstLetters(String str) {
		if(!isParameterEmptyOrNull(str)) {
			StringTokenizer st = new StringTokenizer(str , " ", true );
			StringBuilder sb = new StringBuilder();
			
			while (st.hasMoreTokens()) {
				String token = st.nextToken();
				token = String.format( "%s%s",
						Character.toUpperCase(token.charAt(0)),
						token.substring(1) );
				sb.append( token );
			}
			
			return sb.toString();
		}
		return str;
	}
	
    public static boolean isOldiOSApp(String appVersion, long writerId) {
    	

    	//Check for AO ver before&Equal  2.0.58
    	final int LAST_IOS_AO_VER = 58;
    	final String FIRST_PART_IOS_AO_VER = "2";
    	final String SECOND_PART_IOS_AO_VER = "0";
    	
    	int prevVersion = LAST_IOS_AO_VER;
    	String firstPart = FIRST_PART_IOS_AO_VER;
    	String secondPart = SECOND_PART_IOS_AO_VER;

    	//Check for Copyop ver before&Equal  22.0.54    	
    	final int LAST_IOS_CO_VER = 54;
    	final String FIRST_PART_IOS_CO_VER = "22";
    	final String SECOND_PART_IOS_CO_VER = "0";
    	    	    	
    	if(Writer.WRITER_ID_COPYOP_MOBILE == writerId){
    		prevVersion = LAST_IOS_CO_VER;
    		firstPart = FIRST_PART_IOS_CO_VER;
        	secondPart = SECOND_PART_IOS_CO_VER;
    	}
    	
    	
    	log.debug("BEGIN iOS app Ver. CHECK:" + appVersion);
    	try {
			String appNumber [] = appVersion.split("\\.");
			if(appNumber[0].contains(firstPart) && appNumber[1].contains(secondPart)){
				int ver = new Integer(appNumber[2]);
				if((ver%2)== 0 && ver <= prevVersion){
					log.debug("The iOS app is OLD Ver. Need Update:" + appVersion);
					return true;
				}
			}	
		} catch (Exception e) {
			log.error("Can't get the app version:" + appVersion);
		}
    	log.debug("APP ver is OK");
    	return false;
    }
    
    public static String createBubblesToken(HttpSession session, long userId) { //simps
    	String token = ConstantsBase.DEFAULT_BUBBLES_TOKEN;
//    	if(writerId == Writer.WRITER_ID_MOBILE) {
//    		writerId = Writer.WRITER_ID_BUBBLES_MOBILE;
//    	} else if (writerId == Writer.WRITER_ID_WEB) {
//    		writerId = Writer.WRITER_ID_BUBBLES_WEB;
//    	}
    	if (userId > 0L) { //user is loged in
	    	token = (String) session.getAttribute("token");
	    	long loginId = (long) session.getAttribute(ConstantsBase.LOGIN_ID);
	    	if( token == null || token.equals(ConstantsBase.DEFAULT_BUBBLES_TOKEN)) {
	        	String serverId = ServerConfiguration.getInstance().getServerName();
	    		try {
	    			token = Sha1.encode(session.getId());
	    			session.setAttribute("token", token);
					UsersManagerBase.saveToken(token, serverId, userId, loginId);
				} catch (Exception e) {
					log.error("Can't save token:" + userId);
				}
	    	} 
    	} else {
    		session.setAttribute("token", token);
    	}
    	return token;
    }
    
    public static boolean hitPage(String url) {
		log.debug("url to hit " + url);		
		
		try {
			URL obj = new URL(url);		
			HttpURLConnection con = (HttpURLConnection) obj.openConnection();
	
			// optional default is GET
			con.setRequestMethod("GET");
	
			int responseCode = con.getResponseCode();
			if (responseCode == ConstantsBase.RESPONSE_STATUS_SUCCESS) {
				return true;
			}
			con.disconnect();
		} catch (Exception e) {
			log.error("can't hit url " + url, e);
		}
		return false;
    }
    
    /**
     * Get HashMap Enumerator
     * @param enumerator
     * @return HashMap<String, Enumerator>
     * @throws SQLException
     */
    public static HashMap<String, Enumerator> getEnumHM(String enumerator) throws SQLException {
    	HashMap<String, Enumerator> hm = new HashMap<String, Enumerator>();
		refreshEnums();
		for (int i = 0; i < enumerators.size(); i++) {
			Enumerator e = (Enumerator) enumerators.get(i);
			if (e.getEnumerator().equals(enumerator)) {
				hm.put(e.getCode(), e);
			}
		}
		return hm;
	}
    
    public static boolean isEpgProvider(long clearingProviderId) { //y long?
		if (clearingProviderId == ClearingManagerConstants.EPG_AO_PROVIDER_ID
				|| clearingProviderId == ClearingManagerConstants.EPG_ET_PROVIDER_ID
				|| clearingProviderId == ClearingManagerConstants.EPG_OUROBOROS_PROVIDER_ID
				|| clearingProviderId == ClearingManagerConstants.EPG_CHECKOUT_PROVIDER_ID) {
			return true;
		}
		return false; 
	}

    /**
     * :)
     * @param emailConfig
     */
    public static void sendSingleEmail(EmailConfig emailConfig) {
    	final String username = emailConfig.getAuthenticationUsername();
        final String password = emailConfig.getAuthenticationPassword();
        
		Session session = Session.getInstance(emailConfig.getServerProp(),
		      new Authenticator() {
		 			protected PasswordAuthentication getPasswordAuthentication() {
		 				return new PasswordAuthentication(
		 						username, password);
		 			}
		      });	 
		try {    
			MimeMessage mimeMessage = new MimeMessage(session);
			mimeMessage.setHeader("Content-Type", "text/html; charset=utf-8;");
			mimeMessage.setSubject(emailConfig.getSubject(),"UTF-8");
			mimeMessage.setFrom(new InternetAddress(emailConfig.getFrom()));
			mimeMessage.setRecipients(Message.RecipientType.TO, emailConfig.getTo());
			mimeMessage.setRecipients(Message.RecipientType.CC, emailConfig.getCc());
			mimeMessage.setRecipients(Message.RecipientType.BCC, emailConfig.getBcc());
			mimeMessage.setContent(emailConfig.getBody(),"text/html; charset=utf-8;");
			Transport.send(mimeMessage);
		} catch (SendFailedException sfe){
			log.log(Level.ERROR, "Mail Adress not correct! " + emailConfig.getTo());
		}
		catch (AddressException ae) {
			log.log(Level.FATAL, "Error Sending Mail to: " + emailConfig.getTo() + "body: " + emailConfig.getBody(), ae);
		} catch (MessagingException me) {
			log.log(Level.FATAL, "Error Sending Mail to: " + emailConfig.getTo() + "body: " + emailConfig.getBody(), me);
		}
	}
    
	/**
	 * @param writerId
	 * @return
	 */
    //TODO for java 7+
	public static String getWriterTransactionName(String writerId) {
		String res = "";
		/*switch (writerId) {
			case "0" :
			case "1" :
				res = "WEB";
				break;
			case "10000" :
				res = "WEB COPYOP";
				break;
			case "200" :
				res = "MOBILE";
				break;
			case "20000" :
				res = "MOBILE COPYOP";
				break;
			default :
				res = "BE";
				break;
		}	*/
		return res;	
	}
	
    /**
     * Check if specified time by <code>Date</code> object is today.
     *
     * @param date the to check for
     * @return <code>true</code> if the passed time is today else <code>false</code> (even if can't parse).
     */
    public static boolean isToday(Date date, String timeZone) {
        Calendar today = Calendar.getInstance(TimeZone.getTimeZone(timeZone));

        Calendar dc = Calendar.getInstance();
/*        if (log.isDebugEnabled()) {
            log.debug("timezone dc: " + dc.getTimeZone().getDisplayName() + " timeZone today: " + today.getTimeZone().getDisplayName());
        }*/
        dc.setTime(date);
/*        if (log.isDebugEnabled()) {
            log.debug("timezone dc: " + dc.getTimeZone().getDisplayName() + " timeZone today: " + today.getTimeZone().getDisplayName());
            log.debug("is today(Date date) today day of year = " + today.get(Calendar.DAY_OF_YEAR) + "  dc day of year = " + dc.get(Calendar.DAY_OF_YEAR) + "  today year = " + today.get(Calendar.YEAR) + " dc year = " + dc.get(Calendar.YEAR));
        }*/
        return dc.get(Calendar.YEAR) == today.get(Calendar.YEAR) &&
                dc.get(Calendar.DAY_OF_YEAR) == today.get(Calendar.DAY_OF_YEAR);
    }
    
    public static String formatJSUTCOffsetToString(int utcOffset) {
        // The Date.getTimezoneOffset() in JS returns -120 for GMT+2 for example and 240 for GMT+4
        String sign = utcOffset < 0 ? "+" : "-";
        int absUtcOffset = Math.abs(utcOffset);
        String hours = String.valueOf(absUtcOffset / 60);
        if (hours.length() < 2) {
            hours = "0" + hours;
        }
        String mins = String.valueOf(absUtcOffset % 60);
        if (mins.length() < 2) {
            mins = "0" + mins;
        }
        return "GMT" + sign + hours + ":" + mins;
    }
    
    /**
     * The opposite of <code>formatJSUTCOffsetToString</code>. It takes an offset formatted like
     * GMT+/-XX:XX and turn it to +/-min value from gmt. For example GMT+02:00 becomes -120.
     *
     * @param utcOffset
     * @return Returns the offset in min. Just like the JS Date.getTimezoneOffset.
     */
    public static int getUtcOffsetInMin(String utcOffset) {
    	if (utcOffset==null||utcOffset.isEmpty()) {
    		return 0;
    	}
        int h = 0;
        int m = 0;
        try {
            h = Integer.parseInt(utcOffset.substring(4, 6));
            if (utcOffset.charAt(3) == '+') {
                h = -h;
            }
            m = Integer.parseInt(utcOffset.substring(7));
        } catch (Exception e) {
            log.error("Can't convert UTC offset \"" + utcOffset + "\" to mins.", e);
        }
        return h * 60 + m;
    }
    
	public static Long getSkinId(HttpSession session, HttpServletRequest request) {
        return Skin.SKIN_ETRADER;
    }
	
    public static boolean isEtraderUserSkin(HttpSession session, HttpServletRequest request) {
        if (getSkinId(session, request) == Skin.SKIN_ETRADER) {
            return true;
        }
        return false;
    }
    
    /**
     * Get defualt utcOffset
     * @return
     */
    public static String getDefualtUtcOffset(HttpSession session, HttpServletRequest request) {
        String utcOffsetSession = ConstantsBase.OFFSET_GMT;
        if (isEtraderUserSkin(session, request)) {
            TimeZone tzEt = TimeZone.getTimeZone("Israel");
            utcOffsetSession = ConstantsBase.UTCOFFSET_DEFAULT_PERIOD2;
            if (tzEt.inDaylightTime(new Date())) {
                utcOffsetSession = ConstantsBase.UTCOFFSET_DEFAULT_PERIOD1;
            }
        }
        return utcOffsetSession;
    }
    
    /**
     * Get UtcOffset with GMT+/-HH:MM format.
     * First take utcOffset from Session, if null return default.
     *
     * @return
     *      The passed UTC offset in format GMT+/-HH:MM.
     */
    public static String getUtcOffset(HttpSession session, HttpServletRequest request) {
        String utcOffsetSession = (String) session.getAttribute(ConstantsBase.UTC_OFFSET);
        if (null == utcOffsetSession || utcOffsetSession.length() == 0) {
            utcOffsetSession = getDefualtUtcOffset(session, request);
            log.trace("Using default GMT offset");
        }
        return utcOffsetSession;
    }
    
	public static String displayAmount(long amount, boolean showCurrency, long currencyId, boolean isSMS) {
		Currency currency;
		String displayFormat = null;
		try {
			currency = CurrenciesManagerBase.getCurrency(currencyId);
			displayFormat = CurrenciesManagerBase.getAmountFormat(currency, false);
		} catch (Exception e) {
			log.error("Can't format displayAmount.", e);
			return null;
		}

		return displayAmount(amount, showCurrency, currencyId, isSMS, displayFormat);
	}

	public static String displayAmount(long amount, boolean showCurrency, long currencyId, boolean isSMS, String displayFormat) {
		Currency currency;
		try {
			currency = CurrenciesManagerBase.getCurrency(currencyId);
		} catch (Exception e) {
			log.error("Can't get currency [" + currencyId + "]", e);
			return null;
		}
		DecimalFormat sd = new DecimalFormat(displayFormat, new DecimalFormatSymbols(Locale.US));
		double amountDecimal = amount;
		amountDecimal /= 100;
		String out = null;
		if (showCurrency) {
		    if (null != currency && currency.getIsLeftSymbolBool()) {
		        out = getMessage(new Locale(ConstantsBase.LOCALE_DEFAULT),(isSMS ? getCurrencySymbolSMS(currencyId) : getCurrencySymbol(currencyId)), null) + sd.format(amountDecimal);
		    } else {
                out = sd.format(amountDecimal) + getMessage(new Locale(ConstantsBase.LOCALE_DEFAULT),(isSMS ? getCurrencySymbolSMS(currencyId) : getCurrencySymbol(currencyId)), null);
		    }
		}
		if (null == out) {
			out = sd.format(amountDecimal);
		}
		return out;
	}
	
	public static String getCurrencySymbolSMS(long currencyId) {
		String curr = ConstantsBase.EMPTY_STRING;
		switch ((int)currencyId) {
		case (int)ConstantsBase.CURRENCY_ILS_ID:
			curr = ConstantsBase.CURRENCY_SMS_ILS;
			break;
		case (int)ConstantsBase.CURRENCY_USD_ID:
			curr = ConstantsBase.CURRENCY_SMS_USD;
			break;
		case (int)ConstantsBase.CURRENCY_EUR_ID:
			curr = ConstantsBase.CURRENCY_SMS_EUR;
			break;
		case (int)ConstantsBase.CURRENCY_GBP_ID:
			curr = ConstantsBase.CURRENCY_SMS_GBP;
			break;
		case (int)ConstantsBase.CURRENCY_TRY_ID:
			curr = ConstantsBase.CURRENCY_SMS_TRY;
			break;
		case (int)ConstantsBase.CURRENCY_RUB_ID:
			curr = ConstantsBase.CURRENCY_SMS_RUB;
			break;
        case (int)ConstantsBase.CURRENCY_CNY_ID:
            curr = ConstantsBase.CURRENCY_SMS_CNY;
            break;
        case (int)ConstantsBase.CURRENCY_KRW_ID:
            curr = ConstantsBase.CURRENCY_SMS_KRW;
            break;
        case (int)ConstantsBase.CURRENCY_SEK_ID:
            curr = ConstantsBase.CURRENCY_SMS_SEK;
            break;
        case (int)ConstantsBase.CURRENCY_AUD_ID:
            curr = ConstantsBase.CURRENCY_SMS_AUD;
            break;
        case (int)ConstantsBase.CURRENCY_ZAR_ID:
            curr = ConstantsBase.CURRENCY_SMS_ZAR;
            break; 
        case (int)ConstantsBase.CURRENCY_CZK_ID:
        	curr = ConstantsBase.CURRENCY_SMS_CZK;
        	break;
        case (int)ConstantsBase.CURRENCY_PLN_ID:
        	curr = ConstantsBase.CURRENCY_SMS_PLN;        
            break; 
		default:
			break;
		}
		return curr;
	}
	
	public static String getCurrencySymbol(long currencyId) {
		String curr = ConstantsBase.EMPTY_STRING;
		switch ((int)currencyId) {
		case (int)ConstantsBase.CURRENCY_ILS_ID:
			curr = ConstantsBase.CURRENCY_ILS;
			break;
		case (int)ConstantsBase.CURRENCY_USD_ID:
			curr = ConstantsBase.CURRENCY_USD;
			break;
		case (int)ConstantsBase.CURRENCY_EUR_ID:
			curr = ConstantsBase.CURRENCY_EUR;
			break;
		case (int)ConstantsBase.CURRENCY_GBP_ID:
			curr = ConstantsBase.CURRENCY_GBP;
			break;
		case (int)ConstantsBase.CURRENCY_TRY_ID:
			curr = ConstantsBase.CURRENCY_TRY;
			break;
		case (int)ConstantsBase.CURRENCY_RUB_ID:
			curr = ConstantsBase.CURRENCY_RUB;
			break;
        case (int)ConstantsBase.CURRENCY_CNY_ID:
            curr = ConstantsBase.CURRENCY_CNY;
            break;
        case (int)ConstantsBase.CURRENCY_KRW_ID:
            curr = ConstantsBase.CURRENCY_KRW;
            break;
        case (int)ConstantsBase.CURRENCY_SEK_ID:
            curr = ConstantsBase.CURRENCY_SEK;
            break;
        case (int)ConstantsBase.CURRENCY_AUD_ID:
            curr = ConstantsBase.CURRENCY_AUD;
            break;
        case (int)ConstantsBase.CURRENCY_ZAR_ID:
            curr = ConstantsBase.CURRENCY_ZAR;
            break; 
        case (int)ConstantsBase.CURRENCY_CZK_ID:
            curr = ConstantsBase.CURRENCY_CZK;
            break; 
        case (int)ConstantsBase.CURRENCY_PLN_ID:
            curr = ConstantsBase.CURRENCY_PLN;
            break;             
		default:
			break;
		}
		return curr;
	}
	
	public static boolean isUserRegulated(User user) {
		return SkinsManagerBase.getSkin(user.getSkinId()).isRegulated();
	}

	/**
	 * Check if some variable is empty or not.
	 *
	 * @param par
	 *            The parameter that need to check if he is empty or not
	 */
	public static boolean IsParameterEmptyOrNull(String par) {
		if( (par == null || par.length() == 0) ) {
			return true;
		}
		return false;
	}

	   /**
		 * display amount with different symbol location
		 */
		public static String formatCurrencyAmountAO(long amount, long currencyId) {
			return formatCurrencyAmountAO(amount, true, currencyId);
		}

		/**
	     * Format currency amount and add currency symbol.
	     *
	     * @param amount amount to display (in cents)
	     * @param showCurrencySymbol if to show currency symbol
	     * @param currencyId
	     * @return
	     */
		public static String formatCurrencyAmountAO(double amount, boolean showCurrency, long currencyId) {
			if (currencyId <= 0l) {
				// setting the default currency ID
				log.debug("Invalid currency [" + currencyId
							+ "] for formatting. Setting the default currency ["
							+ ConstantsBase.CURRENCY_BASE_ID + "]");
				currencyId = ConstantsBase.CURRENCY_BASE_ID;
			}
			return formatCurrencyAmountAO(amount, showCurrency, CurrenciesManagerBase.getCurrency(currencyId));
		}


		/**
	     * Format currency amount and add currency symbol.
	     *
	     * @param amount amount to display (in cents)
	     * @param showCurrencySymbol if to show currency symbol
	     * @param currency
	     * @return
	     */
	    public static String formatCurrencyAmountAO(double amount, boolean showCurrency, Currency currency) {
	        return formatCurrencyAmountAO(amount,
										showCurrency,
										currency,
										CurrenciesManagerBase.getAmountFormat(currency, false));
	    }


	    /**
	     * Format currency amount and add currency symbol.
	     *
	     * @param amount amount to display (in cents)
	     * @param showCurrencySymbol if to show currency symbol
	     * @param currency
	     * @param displayFormat
	     * @return
	     */
	    public static String formatCurrencyAmountAO(double amount,
	    											boolean showCurrency,
	    											Currency currency,
	    											String displayFormat) {
	    	return formatCurrencyAmount(amount /= 100,
										showCurrency,
										currency,
										displayFormat,
										currency.getSymbol());
	    }
	    
	/**
	 * Format <code>Date</code> with pattern "HH:mm dd/MM/yy" and time zone.
	 *
	 * @param d
	 * @param timeZoneName
	 * @return
	 */
	public static String getDateAndTimeFormat(Date d, String timeZoneName) {
	    return formatDate(d, timeZoneName, "HH:mm dd/MM/yy");
	}

	/**
	 * Format <code>Date</code> with pattern "dd.MM.yy" and time zone.
	 *
	 * @param d
	 * @param timeZoneName
	 * @return
	 */
	public static String getDateAndTimeFormatByDots(Date d, String timeZoneName) {
	    return formatDate(d, timeZoneName, "dd.MM.yy");
	}
	
	/**
	 * Format <code>Date</code> with pattern "dd/MM/yy HH:mm" and time zone.
	 *
	 * @param d
	 * @param timeZoneName
	 * @return
	 */
	public static String getTimeAndDateFormat(Date d, String timeZoneName) {
	    return formatDate(d, timeZoneName, "dd/MM/yy HH:mm");
	}

	/**
	 * Format <code>Date</code> with pattern ""HH:mm" and time zone.
	 *
	 * @param d
	 * @param timeZoneName
	 * @return
	 */
    public static String getTimeFormat(Date d, String timeZoneName) {
       return formatDate(d, timeZoneName, "HH:mm");
    }

	public static String formatLevelByMarket(Double d, long marketId, long dpoint) {
		if (d == null) {
			return "";
		}
		String format = "###,###,##0.";
		for (int i = 0; i < dpoint; i++) {
			format += "0";
		}
		DecimalFormat f = new DecimalFormat(format);
		f.setDecimalSeparatorAlwaysShown(true);

		// Cut '.' if needed
		String res = f.format(d);
		int lastIndex = res.length() - 1;
		if (res.endsWith(".")) {
			res = res.substring(0, lastIndex);
		}
		return res;
	}
	
	/**
	  * Get a string which represent a number (otherwise return xx...x and print an exception to the 'log') 
	  * if the number of digits in the number is >= 14 digits
	  *  	return format: 6 digits + xx...x + 4 digits
	  * else 
	  * 	return format: xx...x + 4 digits
	  * @param number 
	  *        The number to replace his digits.
	  * @return Scramble number, 
	  *      	Format: 6 digits + xx...x + 4 digits
	  */
	public static String getNumberXXXX(String number) {
		String mask = "";
		String maskedCcnum = "";
		try {
			if (number.length() >= CreditCard.CC_NUMBER_DIGITS) {
				for (int i = 0; i < number.length() - 10; i++) {
					mask += "x";
				}
				maskedCcnum = number.substring(0, 6) + mask + number.substring(number.length() - 4);
			} else {
				for (int i = 0; i < number.length() - 4; i++) {
					mask += "x";
				}
				maskedCcnum = mask + number.substring(number.length() - 4);
			}
		} catch (Exception e) {
			log.fatal("Can't get getNumberXXXX for: " + number, e);
			maskedCcnum = "xxxxxxx";
		}
		return maskedCcnum;
	}
	
    /**
     * get class and xml as a String and 
     * return a java Object constructed from the xml,
     * Otherwise return null.
     * 
     * using JAXB
     *
     * @param clazz
     * @param xml
     * @return
     */
    public static Object generateXmlToObject(Class<?> clazz, String xml) {
    	Object result = null;
    	try {
			JAXBContext jaxbContext = JAXBContext.newInstance(clazz);		
			Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
			result = jaxbUnmarshaller.unmarshal(new StringReader(xml));
			log.debug("generate Xml To Object: " + result.toString() );
    	} catch (Exception e) {
    		log.error("Can't construct Object from xml ", e);
    	}
		return result;
	}
    
    /**
     * Get an object reference variable and return the member class
     * generate, concatenate and return query string format 
     * 
     * Examples:
     * 	field1=value1&field2=value2&field3=value3
     * 	field1:value1;field2:value2;field3:value3
     * 
     * use reflection on the member class fields
     * @param objRef
     * @param interToken
     * @param endToken
     * @param includeNullData
     * @return
     */

    public static String generateObjectToQueryString(Object objRef, String interToken , String endToken, boolean includeNullData) {
    	StringBuilder params = new StringBuilder();
    	String endKeyValue = "";    	
    	try {
	    	Field[] fields = objRef.getClass().getFields();
	    	for (Field field : fields) {
	    		if (field.get(objRef) != null || includeNullData) {
	    			params.append(endKeyValue).append(field.getName()).append(interToken).append(field.get(objRef));
					endKeyValue = endToken;
	    		}
	    	}
	    	log.debug("generate Object To Query String: " + params);    	
    	} catch (IllegalArgumentException | IllegalAccessException e) {
			log.error("ERROR! generateObjectToQueryString, Illegal ", e);
    	} catch (Exception e) {
    		log.error("ERROR! Can't construct Query String from the retrieved object", e);
    	}
		return params.toString();
	}
    
    public static String getUserAgent(HttpServletRequest request) {
		String userAgent = (String) request.getSession().getAttribute(USER_AGENT);
		if (null == userAgent) {  // not in session
			userAgent = request.getHeader("User-Agent");
			if (null != userAgent) {
				userAgent = userAgent.toLowerCase();
				request.getSession().setAttribute(USER_AGENT, userAgent);
			} else {
				log.trace("User-Agent is Null! Request: " + request.getRequestURL());
			}
		} 
		return userAgent;
    }
    
    /**
     * Check if request came from mobile
     * @param request
     * @return
     */
    public static boolean isMobile(HttpServletRequest request) {
		String userAgent = getUserAgent(request);  
    	if (null != userAgent &&
    			(userAgent.matches(".*(android|avantgo|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od|ad)|iris|kindle|lge |maemo|midp|mmp|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\\/|plucker|pocket|psp|symbian|treo|up\\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino).*")||userAgent.substring(0,4).matches("1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\\-(n|u)|c55\\/|capi|ccwa|cdm\\-|cell|chtm|cldc|cmd\\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\\-s|devi|dica|dmob|do(c|p)o|ds(12|\\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\\-|_)|g1 u|g560|gene|gf\\-5|g\\-mo|go(\\.w|od)|gr(ad|un)|haie|hcit|hd\\-(m|p|t)|hei\\-|hi(pt|ta)|hp( i|ip)|hs\\-c|ht(c(\\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\\-(20|go|ma)|i230|iac( |\\-|\\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\\/)|klon|kpt |kwc\\-|kyo(c|k)|le(no|xi)|lg( g|\\/(k|l|u)|50|54|e\\-|e\\/|\\-[a-w])|libw|lynx|m1\\-w|m3ga|m50\\/|ma(te|ui|xo)|mc(01|21|ca)|m\\-cr|me(di|rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\\-2|po(ck|rt|se)|prox|psio|pt\\-g|qa\\-a|qc(07|12|21|32|60|\\-[2-7]|i\\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\\-|oo|p\\-)|sdk\\/|se(c(\\-|0|1)|47|mc|nd|ri)|sgh\\-|shar|sie(\\-|m)|sk\\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\\-|v\\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\\-|tdg\\-|tel(i|m)|tim\\-|t\\-mo|to(pl|sh)|ts(70|m\\-|m3|m5)|tx\\-9|up(\\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|xda(\\-|2|g)|yas\\-|your|zeto|zte\\-"))) {
    		return true;
    	}
    	return false;
	}
	
    public static String getDollarAmount(Transaction t) {
		try {
			DecimalFormat sd = new DecimalFormat("###########0.00");
			return sd.format(CommonUtil.convertToBaseAmount(t.getAmount(), t.getCurrency().getId(), t.getTimeCreated())/100);
		} catch (Exception e) {
			log.error("cant get dollar amount return 0 for transaction id = " + t.getId() , e);
		}
		return "0";
	}

	public static String getEuroAmount(Transaction t) {
		try {
			DecimalFormat sd = new DecimalFormat("###########0.00");
			return sd.format(CommonUtil.convertToEuroAmount(t.getAmount(), t.getCurrency().getId(), t.getTimeCreated())/100);
		} catch (Exception e) {
			log.error("cant get euro amount return 0 for transaction id = " + t.getId() , e);
		}
		return "0";
	}
	
	/**
	 * this method convert an amount from one currency to an amount in base currency
	 * @param amount - the amount to convert from
	 * @param currencyId - the currency to convert from
	 * @param date - the exchange rate date
	 * @return the new amount
	 */
	public static Double convertToBaseAmount(long amount, long currencyId, Date date) {
		return CurrencyRatesManagerBase.convertToBaseAmount(amount, currencyId, date);
	}

	public static Double convertToEuroAmount(long amount, long currencyId, Date date) {
			return CurrencyRatesManagerBase.convertToEuroAmount(amount, currencyId, date);
	}
	
	/**
	 * This method receives a date\time in GMT and an utcOffset and returns a new date\time in the specified utcOffset
	 * @param d - date\time
	 * @param timeZoneName - utcOffset
	 * @return new date\time
	 */
	public static Date getDateTimeToUTC(Date d, String timeZoneName) {
		ZoneId zoneId = ZoneId.of(timeZoneName);  
		ZonedDateTime zonedToConvert = d.toInstant().atZone(zoneId);
		DateTimeFormatter format = DateTimeFormatter.ofPattern("MM.dd.yyyy HH:mm:ss");
		String formatedTime = zonedToConvert.format(format);
		DateFormat df = new SimpleDateFormat("MM.dd.yyyy HH:mm:ss");
		Date retDate = new Date();
		try {
			retDate = (Date) df.parse(formatedTime);
		} catch (ParseException e) {
			log.debug("cannot parse date", e);
		}
		return retDate;
		}
	
	public static int scoreGroupToIssueActionType(long scoreGroup) {
		if(scoreGroup == UserRegulationBase.AO_REGULATION_RESTRICTED_FAILED_Q_SCORE_GROUP_ID) {
			return IssuesManagerBase.ISSUE_ACTION_TYPE_RESTRICTED_FAILED_QUESTIONNAIRE;
		} else {
			return IssuesManagerBase.ISSUE_ACTION_TYPE_RESTRICTED_SCORE_GROUP;
		}
	}
	
	public static long calcAmount(String s) throws NumberFormatException {
		BigDecimal bd = new BigDecimal(s);
		return bd.multiply(new BigDecimal(100)).longValue();
	}
	
	/**
	 * Get array integer to string
	 * @param temp
	 * @return
	 */
	public static String getArrayIntToString(List<Integer> temp) {
	       String list = ConstantsBase.EMPTY_STRING;
	       for (int i = 0; i < temp.size(); i++) {
	           list += temp.get(i) + ",";
	       }
	       return list.substring(0,list.length()-1);
	   }

	public static void sendRegistrationSMSByTextLocalProvider(com.anyoption.common.beans.User user, Locale locale) {
		if (user == null || user.getId() == 0) {
			log.error("User is null! Registration SMS can not be send!");
			return;
		} else if (user.getSkinId() == Skin.SKIN_CHINESE || user.getSkinId() == Skin.SKIN_KOREAN) {
			log.error("Can not send registration SMS for CHINESE and KOREAN skin!");
			return;
		}
		try {
			String senderName = "";
			String senderNumber = "";
			String phoneCode = "";
			// String encodedMsg = "";
			String msg = "";
			long smsId = 0;

			// determine SMS message and link app based on user platform id
			senderNumber = CountryManagerBase.getById(user.getCountryId()).getSupportPhone();
			phoneCode = CountryManagerBase.getPhoneCodeById(user.getCountryId());
			Object[] params = new Object[1];
			if (user.getPlatformId() == ConstantsBase.PLATFORM_ID_ERTADER) {
				senderName = "etrader";
				params[0] = getMessage(locale, "sms.register.link.etrader", null);
				msg = getMessage(locale, "sms.register", params);
			} else if (user.getPlatformId() == ConstantsBase.PLATFORM_ID_ANYOPTION) {
				senderName = "anyoption";
				params[0] = getMessage(locale, "sms.register.link.anyoption", null);
				msg = getMessage(locale, "sms.register", params);
			} else if (user.getPlatformId() == ConstantsBase.PLATFORM_ID_COPYOP) {
				senderName = "copyop";
				params[0] = getMessage(locale, "sms.register.link.copyop", null);
				msg = getMessage(locale, "sms.register.copyop", params);
			} else {
				senderName = "backend";
				if (user.getSkinId() == Skin.SKIN_ETRADER) {
					params[0] = getMessage(locale, "sms.register.link.etrader", null);
					msg = getMessage(locale, "sms.register", params);
				} else {
					params[0] = getMessage(locale, "sms.register.link.anyoption", null);
					msg = getMessage(locale, "sms.register", params);
				}
			}

			if (msg.equals("")) {
				log.error("Can NOT get sms message from bundle.");
				return;
			}
			// determine provider by character length
			/*
			 * if ( msg.length() <= ConstantsBase.MAX_CHARACTERS_ALLOWED_BY_MBLOX_PROVIDER ) { smsId =
			 * SMSManagerBase.sendTextMessage(senderName, senderNumber, phoneCode + user.getMobilePhone(), msg, user.getId(),
			 * SMSManagerBase.SMS_KEY_TYPE_SMS, ConstantsBase.MBLOX_PROVIDER); } else {}
			 */
			// encodedMsg = encodeRegisterMessage(msg);
			/* STOP encoding msg for TextLocal provider MONI's request!!! */
			// Switching sms providers from TextLocal to Mobivate and no need for special encoding for now...
			smsId = SMSManagerBase.sendTextMessage(	senderName, senderNumber, phoneCode + user.getMobilePhone(), msg, user.getId(),
													SMSManagerBase.SMS_KEY_TYPE_USERID, ConstantsBase.MOBIVATE_PROVIDER,
													SMS.DESCRIPTION_WELCOME, MobileNumberValidation.NOTVALIDATED);

			// insert SMS issue for backend traking purposes
			if (smsId == 0) {
				log.error("SMS id is 0.");
				return;
			} else {
				SMSManagerBase.insertIssueForSMS(user, new Date(), msg, smsId);
			}

		} catch (SMSException smse) {
			log.error("Failed to send SMS for user with id: " + user.getId(), smse);
		} catch (SQLException e) {
			log.error("Failed to send SMS for user with id: " + user.getId(), e);
		}
	}
	
	public static String getIPAddress() {

		FacesContext f = FacesContext.getCurrentInstance();
		String ipNotFound = "IP NOT FOUND!";
		String ip = null;
		HttpServletRequest req = null;
		if (f != null) {
			req = (HttpServletRequest) f.getExternalContext().getRequest();
			ip =  getIPAddress(req);
		}
		return ip == null ? ipNotFound : ip ;
	}
}   