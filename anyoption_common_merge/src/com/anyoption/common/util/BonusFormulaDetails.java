package com.anyoption.common.util;

/**
 * 
 * @author liors
 *
 * This class is part of the BMS - Bonus Management System.
 * This class present the BMS formula logic.
 * 
 */
public class BonusFormulaDetails implements java.io.Serializable {
	private static final long serialVersionUID = 1L;
	private long investmentId;
	private long bonusUsersId;
	private long amount;
	private long adjustedAmount;
	
	/**
	 * @return the investmentId
	 */
	public long getInvestmentId() {
		return investmentId;
	}
	/**
	 * @param investmentId the investmentId to set
	 */
	public void setInvestmentId(long investmentId) {
		this.investmentId = investmentId;
	}
	/**
	 * @return the bonusUsersId
	 */
	public long getBonusUsersId() {
		return bonusUsersId;
	}
	/**
	 * @param bonusUsersId the bonusUsersId to set
	 */
	public void setBonusUsersId(long bonusUsersId) {
		this.bonusUsersId = bonusUsersId;
	}
	/**
	 * @return the amount
	 */
	public long getAmount() {
		return amount;
	}
	/**
	 * @param amount the amount to set
	 */
	public void setAmount(long amount) {
		this.amount = amount;
	}
	/**
	 * @return the adjustedAmount
	 */
	public long getAdjustedAmount() {
		return adjustedAmount;
	}
	/**
	 * @param adjustedAmount the adjustedAmount to set
	 */
	public void setAdjustedAmount(long adjustedAmount) {
		this.adjustedAmount = adjustedAmount;
	}
}