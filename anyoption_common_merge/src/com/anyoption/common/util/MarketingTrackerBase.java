package com.anyoption.common.util;

import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.base.MarketingTracking;
import com.anyoption.common.beans.base.MarketingTrackingCookieDynamic;
import com.anyoption.common.beans.base.MarketingTrackingCookieStatic;
import com.anyoption.common.managers.MarketingTrackingManager;
import com.anyoption.common.managers.SkinsManagerBase;

public class MarketingTrackerBase {
    
    private static final Logger log = Logger.getLogger(MarketingTrackerBase.class);
    
    protected static int FOURTY_FIVE_CHECK_SUM = 45;
    public static Long DEFAULT_COMBINATION_ID = (long) 22;
    
    private static String getStaticPartByContactDetailsInsertingContacts(String email, String phone){
        String staticPart = null;
        try {                         
            MarketingTrackingCookieStatic mtcStatic = new MarketingTrackingCookieStatic();
            String mId = MarketingTrackingManager.getMidByContactDetail(email, phone); 
            mtcStatic = MarketingTrackingManager.getMarketingTrackingCookieStatic(mId);
            
            if (mtcStatic.getMs() != null){
                String dateForDisplay = new SimpleDateFormat("HH:mm:ss dd/MM/yy").format(mtcStatic.getTs());
                staticPart = MarketingTrackerBase.getStaticCookieData(mtcStatic.getMs(), mtcStatic.getCs(), mtcStatic.getHttp(), mtcStatic.getDp(),
                		dateForDisplay, mtcStatic.getUtmSource(), true, mtcStatic.getAff_sub1(), mtcStatic.getAff_sub2(), mtcStatic.getAff_sub3());
            }
        } catch (Exception e) {
            log.error("Marketing Tracker getStaticPartByContactDetailsInsertingContacts for email: " + email + " and phone " + phone + " Exc: ", e);
        }
        return staticPart;
    }
    
    private static String getStaticPartByContactDetailsInsertingUser(String email, String mobilePhone, String landLinePhone, long contactId, long userId){
        String staticPart = null;
        try {                         
            MarketingTrackingCookieStatic mtcStatic = new MarketingTrackingCookieStatic();
            String mId = MarketingTrackingManager.getMidByContactDetailsInsertingUser(email, mobilePhone, landLinePhone, contactId, userId);
            mtcStatic = MarketingTrackingManager.getMarketingTrackingCookieStatic(mId);
            
            if (mtcStatic.getMs() != null){
                String dateForDisplay = new SimpleDateFormat("HH:mm:ss dd/MM/yy").format(mtcStatic.getTs());
                staticPart = MarketingTrackerBase.getStaticCookieData(mtcStatic.getMs(), mtcStatic.getCs(), mtcStatic.getHttp(), mtcStatic.getDp(),
                		dateForDisplay, mtcStatic.getUtmSource(), true, mtcStatic.getAff_sub1(), mtcStatic.getAff_sub2(), mtcStatic.getAff_sub3());
            }
        } catch (Exception e) {
            log.error("Marketing Tracker getStaticPartByContactDetailsInsertingUser for userId: " + userId + "  Exc: ", e);
        }
        return staticPart;
    }
    
    private static boolean afterFourtyFiveDays(String mId) {
        long daysBetween = 0;
        boolean result = false;
        try {
            MarketingTrackingCookieStatic mtcStatic =  new MarketingTrackingCookieStatic();
            mtcStatic = MarketingTrackingManager.getMarketingTrackingCookieStatic(mId);
            if (mtcStatic.getMs() != null){                
                Date date = Calendar.getInstance(TimeZone.getTimeZone("GMT")).getTime();
                long millsPerDay = 1000 * 60 * 60 * 24;
                long d2 = date.getTime() / millsPerDay;
                long d1 = mtcStatic.getTs().getTime() / millsPerDay;
                daysBetween = d2 - d1;
                if (daysBetween > FOURTY_FIVE_CHECK_SUM) {
                    if (!MarketingTrackingManager.isHaveActivity(mtcStatic.getMs())) {
                        result = true;
                    }
                }
            }
        } catch (Exception e) {
            log.error( "Marketing Tracker afterFourtyFiveDays with mId:" + mId + " Exc: ", e);
        }
        return result;
    }
    
    protected static String getStaticCookieData(String mId, String combinationStatic, String httpReferer, String dynamicParam, String createdDate, String utmSource, boolean isHex, String affSub1, String affSub2, String affSub3){
        String params = null;
        params = "MS{" + mId + "^CS{" + combinationStatic + "^HTTP{" + httpReferer + "^DP{" + dynamicParam + "^TS{" + createdDate + "^UTM{" + utmSource + "^aff_sub1{" + affSub1 + "^aff_sub2{" + affSub2 + "^aff_sub3{" + affSub3;
        if(isHex){
            try {
                params = HexUtil.byteArrayToHexString(params.getBytes("UTF-8"));
            } catch (Exception e) {
                log.error("Marketing Tracker byteArrayToHexString ", e);
            }
        }
        // TODO - check size
        return params;
    } 
    /* not used  
    protected static String getDynamicCookieData(String combinationDynamic, String httpReferer, String dynamicParam, String createdDate, boolean isHex){
        String params = null;
        params = "HTTP{" + httpReferer + "^CD{" + combinationDynamic + "^TD{" + createdDate + "^DP{" + dynamicParam;
        if(isHex){
            try {
                params = CommonUtil.byteArrayToHexString(params.getBytes("UTF-8"));
            } catch (Exception e) {
                log.error("Marketing Tracker byteArrayToHexString ", e);
            }
        }        
        return params;
    } 
    */
    protected static ArrayList<MarketingTracking> getMarketingTrackerListForClick(String staticPart, String dynamicPart) {        
        ArrayList<MarketingTracking> list = new ArrayList<MarketingTracking>();
        try {           
            MarketingTrackingCookieStatic mtcStatic = MarketingTrackerUtil.getStaticParsedValue(staticPart);
            MarketingTrackingCookieDynamic mtcDynamic = MarketingTrackerUtil.getDynamicParsedValue(dynamicPart);            
            if(mtcStatic != null){              
                MarketingTracking mtS = new MarketingTracking();
                mtS.setmId(mtcStatic.getMs());
                
                //try to cast combId to Long
                Long combId;
                try {
                    combId = new Long(mtcStatic.getCs());
                } catch (Exception e) {
                    combId = DEFAULT_COMBINATION_ID;
                    log.info("Marketing Tracker CombId error set default:" + mtcStatic.getCs() + " Exc: ", e);
                }
                mtS.setCombinationId(combId);
                mtS.setTimeStatic(mtcStatic.getTs());
                mtS.setHttpReferer(mtcStatic.getHttp());
                mtS.setDyanmicParameter(mtcStatic.getDp());
                
                if (mtcDynamic != null){ 
                    //try to cast combId to Long
                    Long combIdyn;
                    try {
                        combIdyn = new Long(mtcDynamic.getCd());
                    } catch (Exception e) {
                        combIdyn = DEFAULT_COMBINATION_ID;
                        log.info("Marketing Tracker CombId error set default:" + mtcDynamic.getCd() + " Exc: ", e);
                    }
                mtS.setUtmSource(mtcStatic.getUtmSource());
                mtS.setCombinationIdDynamic(combIdyn);
                mtS.setTimeDynamic(mtcDynamic.getTd());
                mtS.setHttpRefererDynamic(mtcDynamic.getHttp());
                mtS.setDyanmicParameterDynamic(mtcDynamic.getDp());
                mtS.setUtmSourceDynamic(mtcDynamic.getUtmSource());
                mtS.setAff_sub1(mtcStatic.getAff_sub1());
                mtS.setAff_sub2(mtcStatic.getAff_sub2());
                mtS.setAff_sub3(mtcStatic.getAff_sub3());
                mtS.setContactId(0);
                mtS.setUserId(0);
                mtS.setMarketingTrackingActivityId(ConstantsBase.MARKETING_TRACKING_CLICK);
                } else {
                    mtS.setMarketingTrackingActivityId(-1);
                }
                list.add(mtS);                  
            }
        } catch (Exception e) {
            log.error("Marketing Tracker getMarketingTrackerListForClick with Static part:" + staticPart + " and Dynamic part : " + dynamicPart + " Exc: ", e);
        }
        return list;
    }
    
    public static String createDefaultStatciCookieData(Long combinationStatic, String httpReferer, String dynamicParam, String utmSource, long skinId, String aff_sub1, String aff_sub2, String aff_sub3 ){
        String result = "";
        try {
            String mId = MarketingTrackerUtil.generateRandomUUId("Json-");
            
            Date date = Calendar.getInstance(TimeZone.getTimeZone("GMT")).getTime();
            String dateForDisplay = new SimpleDateFormat("HH:mm:ss dd/MM/yy").format(date);
            if(combinationStatic == null){
                combinationStatic = SkinsManagerBase.getCombinationIdtBySkins(skinId);
            }
            
            result = getStaticCookieData(mId, combinationStatic.toString(), httpReferer, dynamicParam, dateForDisplay, utmSource, true, aff_sub1, aff_sub2, aff_sub3 );
        } catch (Exception e) {
            log.error("Marketing Tracker createDefaultStatciCookieData Exc: ", e);
        }        
        return result;
    }
    
    public static String getMidFromStaticPart(String staticPart){
        String mId="";
        try {
            MarketingTrackingCookieStatic mtcStatic = MarketingTrackerUtil.getStaticParsedValue(staticPart);
            mId = mtcStatic.getMs();
        } catch (Exception e) {
            log.error("Marketing Tracker getMidFromStaticPart with Static part:" + staticPart + " Exc: ", e);
        }        
        return mId;
    }
    
    public static String getStaticPartFromMid(String mId){
        String staticPart=null;
        try {
            MarketingTrackingCookieStatic mtcStatic = MarketingTrackingManager.getMarketingTrackingCookieStatic(mId);
            if(mtcStatic.getMs() != null){
                String dateForDisplay = new SimpleDateFormat("HH:mm:ss dd/MM/yy").format(mtcStatic.getTs());
                staticPart = getStaticCookieData(mtcStatic.getMs(), mtcStatic.getCs(), mtcStatic.getHttp(), mtcStatic.getDp(), dateForDisplay,
                		mtcStatic.getUtmSource(), true, mtcStatic.getAff_sub1(), mtcStatic.getAff_sub2(), mtcStatic.getAff_sub3());
            }
        } catch (Exception e) {
            log.error("Marketing Tracker getStaticPartFromMid with mId " + mId + " Exc: ", e);
        }        
        return staticPart;
    }
    
//    public static String getMidByContactDetailsJson(String email, String phone){
//        String mId = null;
//        try {                         
//            mId = MarketingTrackingManager.getMidByContactDetail(email, phone);            
//        } catch (Exception e) {
//            log.error("Marketing Tracker getMidByContactDetailsJson for email: " + email + " and phone " + phone + " Exc: ", e);
//        }
//        return mId;
//    }    
    
    public static long getMarketingTrackingActivity( long contactType){
        long result =  ConstantsBase.MARKETING_TRACKING_CALL_ME;
        
        try {                         
            if (contactType == 5 || contactType == 6){
                result = ConstantsBase.MARKETING_TRACKING_SHORT_REG;
            }
        } catch (Exception e) {
            log.error("Marketing Tracker getMarketingTrackingActivity Exc: ", e);
        }
        
        return result;
    }
    
    public static String checkStaticPartByContactDetailsInsertingContact(String email, String phone, String currentStaticPart){
        String newStaticPartResult = null;
        try {                         
            String newStaticPart = getStaticPartByContactDetailsInsertingContacts(email, phone);             
            
            if (newStaticPart != null){
                MarketingTrackingCookieStatic mtcStaticByConctact = MarketingTrackerUtil.getStaticParsedValue(newStaticPart);
                MarketingTrackingCookieStatic mtcStaticCurrent = MarketingTrackerUtil.getStaticParsedValue(currentStaticPart); 
                
                if(mtcStaticByConctact != null && mtcStaticCurrent != null){
                    
                     if(!mtcStaticByConctact.getMs().equals(mtcStaticCurrent.getMs())){
                         String dateForDisplay = new SimpleDateFormat("HH:mm:ss dd/MM/yy").format(mtcStaticByConctact.getTs());
                         newStaticPartResult = getStaticCookieData(mtcStaticByConctact.getMs(), mtcStaticByConctact.getCs(), mtcStaticByConctact.getHttp(),
                                 mtcStaticByConctact.getDp(), dateForDisplay, mtcStaticByConctact.getUtmSource(), true, mtcStaticByConctact.getAff_sub1(), mtcStaticByConctact.getAff_sub2(), mtcStaticByConctact.getAff_sub3());
                     }
                }
            }
        } catch (Exception e) {
            log.error("Marketing Tracker checkStaticPartByContactDetailsJsonn for email: " + email + " and phone " + phone + "and current " +
            		"static part: "+ currentStaticPart +" Exc: ", e);
        }
        return newStaticPartResult;
    }
    
    public static String checkMidByContactDetailsInsertingContacts(String email, String phone, String currentMid, long skinId){
        String newMidResult = null;
        try {                         
            String newStaticPart = getStaticPartByContactDetailsInsertingContacts(email, phone);             
            
            if (newStaticPart != null){
                MarketingTrackingCookieStatic mtcStaticByConctact = MarketingTrackerUtil.getStaticParsedValue(newStaticPart);               
                
                if(mtcStaticByConctact != null ){
                    
                     if(!mtcStaticByConctact.getMs().equals(currentMid)){                         
                         newMidResult = mtcStaticByConctact.getMs();
                     }
                }
            } else {
                if (afterFourtyFiveDays(currentMid)){
                    String newDefaultStaticPart = createDefaultStatciCookieData(null, null, null, null, skinId, null, null, null);
                    insertMarketingTrackerClick(newDefaultStaticPart, null);
                    newMidResult = getMidFromStaticPart(newDefaultStaticPart);
                }
            }
        } catch (Exception e) {
            log.error("Marketing Tracker checkStaticPartByContactDetailsJsonn for email: " + email + " and phone " + phone + "and current " +
                    "static part: "+ currentMid +" Exc: ", e);
        }
        return newMidResult;
    }
    
    public static String checkStaticPartByContactDetailsInsertingUser(String email, String mobilePhone, String landLinePhone, long contactId, long userId, String currentStaticPart){
        String newStaticPartResult = null;
        try {                         
            String newStaticPart = getStaticPartByContactDetailsInsertingUser(email, mobilePhone, landLinePhone, contactId, userId);             
            
            if (newStaticPart != null){
                MarketingTrackingCookieStatic mtcStaticByConctact = MarketingTrackerUtil.getStaticParsedValue(newStaticPart);
                MarketingTrackingCookieStatic mtcStaticCurrent = MarketingTrackerUtil.getStaticParsedValue(currentStaticPart); 
                
                if(mtcStaticByConctact != null && mtcStaticCurrent != null){
                    
                     if(!mtcStaticByConctact.getMs().equals(mtcStaticCurrent.getMs())){
                         String dateForDisplay = new SimpleDateFormat("HH:mm:ss dd/MM/yy").format(mtcStaticByConctact.getTs());
                         newStaticPartResult = getStaticCookieData(mtcStaticByConctact.getMs(), mtcStaticByConctact.getCs(), mtcStaticByConctact.getHttp(),
                                 mtcStaticByConctact.getDp(), dateForDisplay, mtcStaticByConctact.getUtmSource(), true, mtcStaticByConctact.getAff_sub1(), mtcStaticByConctact.getAff_sub2(), mtcStaticByConctact.getAff_sub3());
                     }
                }
            }
        } catch (Exception e) {
            log.error("Marketing Tracker checkStaticPartByContactDetailsInsertingUser for UserId: " + userId +  " and current " + "static part: "+ currentStaticPart +" Exc: ", e);
        }
        return newStaticPartResult;
    }
    
    public static String checkMidByContactDetailsInsertingUser(String email, String mobilePhone, String landLinePhone, long contactId, long userId, String currentMid, long skinId){
        String newMidResult = null;
        try {                         
            String newStaticPart = getStaticPartByContactDetailsInsertingUser(email, mobilePhone, landLinePhone, contactId, userId);             
            
            if (newStaticPart != null){
                MarketingTrackingCookieStatic mtcStaticByConctact = MarketingTrackerUtil.getStaticParsedValue(newStaticPart);               
                
                if(mtcStaticByConctact != null ){
                    
                     if(!mtcStaticByConctact.getMs().equals(currentMid)){
                         newMidResult = mtcStaticByConctact.getMs();
                     }
                }
            } else {
                // Check 45 days 
                if (afterFourtyFiveDays(currentMid)){
                    String newDefaultStaticPart = createDefaultStatciCookieData(null, null, null, null, skinId, null, null, null);
                    insertMarketingTrackerClick(newDefaultStaticPart, null);
                    newMidResult = getMidFromStaticPart(newDefaultStaticPart);
                }
            }
        } catch (Exception e) {
            log.error("Marketing Tracker checkStaticPartByContactDetailsInsertingUser for UserId: " + userId +  " and current " + "static mId: "+ currentMid +" Exc: ", e);
        }
        return newMidResult;
    }
    
    public static void insertMarketingTrackerClick(String staticPart, String dynamicPart) throws SQLException {             
        try {                                       
            ArrayList<MarketingTracking> list = getMarketingTrackerListForClick(staticPart, dynamicPart);
            if (list.size() > 0) {
                MarketingTrackingManager.insertMarketingTracking(list);                             
            }
        } catch (Exception e) {
            log.error("Marketing Tracker insertMarketingTrackerClick with Static part : " + staticPart + " and Dynamic part : " + dynamicPart + " Exc: ", e);
        }
    }
    
    public static String insertMarketingTrackerClickFirstOpen(String mId, Long combinationId, String dynamicParam, String utmSource, long skinId, String aff_sub1, String aff_sub2, String aff_sub3) throws SQLException {
        String resultmId = null;
        boolean isGenerateMId = false;
        try {
            
            if(CommonUtil.isParameterEmptyOrNull(mId)){
                mId = MarketingTrackerUtil.generateRandomUUId("Json"); 
                isGenerateMId = true;
            }
            
            if (combinationId == null){
                combinationId = SkinsManagerBase.getCombinationIdtBySkins(skinId);               
            }            
            
            Date date = Calendar.getInstance(TimeZone.getTimeZone("GMT")).getTime();
            String dateForDisplay = new SimpleDateFormat("HH:mm:ss dd/MM/yy").format(date);
            String staticPart =  getStaticCookieData(mId, combinationId.toString(), null, dynamicParam, dateForDisplay, utmSource, true, aff_sub1, aff_sub2, aff_sub3);
            String dynamicPart = null;//getDynamicCookieData(combinationId.toString(), null, dynamicParam, dateForDisplay, true);
            
            insertMarketingTrackerClick(staticPart, dynamicPart);
            
            if(isGenerateMId){
                resultmId = mId;
            }
                                              
        } catch (Exception e) {
            log.error("Marketing Tracker insertMarketingTrackerClickFirtsOpen with mId : " + mId + " and CombinationId : " + combinationId + " and DynamicParam : " + dynamicParam + " Exc: ", e);
        }
        
        return resultmId;
    }
    
    public static void insertMarketingTrackerJson(String staticPart, String mId, long contactId, long userId, long type)
            throws SQLException {
        try {                       
            ArrayList<MarketingTracking> list = new ArrayList<MarketingTracking>();
            MarketingTrackingCookieStatic mtcStatic = new MarketingTrackingCookieStatic();
            
            if (mId != null){
                mtcStatic = MarketingTrackingManager.getMarketingTrackingCookieStatic(mId);
            }else {
                mtcStatic = MarketingTrackerUtil.getStaticParsedValue(staticPart);
            }
            
            if (mtcStatic.getMs() == null || mtcStatic.getCs() == null)
            {
                mtcStatic = MarketingTrackerUtil.getStaticParsedValue(staticPart);
            }
            
            if (mtcStatic != null){
                MarketingTracking mtActivity = new MarketingTracking();
                mtActivity.setmId(mtcStatic.getMs());
                mtActivity.setCombinationId(new Long(mtcStatic.getCs()));
                mtActivity.setTimeStatic(mtcStatic.getTs());
                mtActivity.setHttpReferer(mtcStatic.getHttp());
                mtActivity.setDyanmicParameter(mtcStatic.getDp());
                mtActivity.setUtmSource(mtcStatic.getUtmSource());

                mtActivity.setCombinationIdDynamic(0);
                mtActivity.setTimeDynamic(Calendar.getInstance(TimeZone.getTimeZone("GMT")).getTime());
                mtActivity.setHttpRefererDynamic("");
                mtActivity.setDyanmicParameterDynamic("");
                mtActivity.setUtmSourceDynamic("");
                mtActivity.setContactId(contactId);
                mtActivity.setUserId(userId);
                mtActivity.setMarketingTrackingActivityId(type);
                list.add(mtActivity);
            }
            
            if (list.size() > 0) {                              
                MarketingTrackingManager.insertMarketingTracking(list);     
            }
        } catch (Exception e) {
            log.error("Marketing Tracker insertMarketingTrackerJson cookies value: " + staticPart + " Exc: ", e);
        }
    }
    
    public static void insertMarketingTrackerUserCreatedBE(String email, String mobilePhone, String landLinePhone, long contactId, long userId, long skinId)
            throws SQLException {
        try {
            if (log.isDebugEnabled()) {
                log.debug("Marketing Tracker Inserted User : " + userId + " from BE, try to get MID");
            }
            ArrayList<MarketingTracking> list = new ArrayList<MarketingTracking>();
            MarketingTrackingCookieStatic mtcStatic = new MarketingTrackingCookieStatic();
            String mId = MarketingTrackingManager.getMidByContactDetailsInsertingUser(email, mobilePhone, landLinePhone, contactId, userId);
            if (mId != null){
                mtcStatic = MarketingTrackingManager.getMarketingTrackingCookieStatic(mId);
                if (log.isDebugEnabled()) {
                    log.debug("Marketing Tracker Find Contact for User :" + userId + " with MID :" + mtcStatic.getMs() );
                }
                if(mtcStatic != null){
                    // Add Activity             
                        MarketingTracking mtActivity = new MarketingTracking();
                        mtActivity.setmId(mtcStatic.getMs());
                        mtActivity.setCombinationId(new Long(mtcStatic.getCs()));
                        mtActivity.setTimeStatic(mtcStatic.getTs());
                        mtActivity.setHttpReferer(mtcStatic.getHttp());
                        mtActivity.setDyanmicParameter(mtcStatic.getDp());
                        mtActivity.setUtmSource(mtcStatic.getUtmSource());
        
                        mtActivity.setCombinationIdDynamic(0);
                        mtActivity.setTimeDynamic(Calendar.getInstance(TimeZone.getTimeZone("GMT")).getTime());
                        mtActivity.setHttpRefererDynamic("");
                        mtActivity.setDyanmicParameterDynamic("");
                        mtActivity.setUtmSourceDynamic("");
                        mtActivity.setContactId(contactId);
                        mtActivity.setUserId(userId);
                        mtActivity.setMarketingTrackingActivityId(ConstantsBase.MARKETING_TRACKING_FULL_REG);
        
                        list.add(mtActivity);
                }
            } else {
                if (log.isDebugEnabled()) {
                    log.debug("Marketing Tracker can't find Contact for User :" + userId + " set default MID");
                }
                Long combId = SkinsManagerBase.getCombinationIdtBySkins(skinId);
                Date date = Calendar.getInstance(TimeZone.getTimeZone("GMT")).getTime();
                
                MarketingTracking mtActivity = new MarketingTracking();                
                mtActivity.setmId("BE" + userId);
                mtActivity.setCombinationId(combId);
                mtActivity.setTimeStatic(date);
                mtActivity.setHttpReferer(null);
                mtActivity.setDyanmicParameter(null);

                mtActivity.setCombinationIdDynamic(0);
                mtActivity.setTimeDynamic(Calendar.getInstance(TimeZone.getTimeZone("GMT")).getTime());
                mtActivity.setHttpRefererDynamic("");
                mtActivity.setDyanmicParameterDynamic("");
                mtActivity.setContactId(contactId);
                mtActivity.setUserId(userId);
                mtActivity.setMarketingTrackingActivityId(ConstantsBase.MARKETING_TRACKING_FULL_REG);

                list.add(mtActivity);
            }            
            if (list.size() > 0) {              
                
                MarketingTrackingManager.insertMarketingTracking(list);     
            }
        } catch (Exception e) {
            log.error("Marketing Tracker insertMarketingTrackerUserCreatedBE for user: " + userId + " Exc: ", e);
        }
    }
}
