package com.anyoption.common.util;

import java.sql.Connection;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.util.Hashtable;

import org.apache.log4j.Logger;

//import com.anyoption.common.beans.BinaryZeroOneHundred;
import com.anyoption.common.beans.Investment;
import com.anyoption.common.beans.InvestmentRejects;
import com.anyoption.common.beans.Market;
import com.anyoption.common.beans.Opportunity;
import com.anyoption.common.beans.OpportunityMiniBean;
import com.anyoption.common.beans.User;
import com.anyoption.common.beans.base.Skin;
import com.anyoption.common.bl_vos.InvestmentType;
import com.anyoption.common.jms.WebLevelLookupBean;
import com.anyoption.common.jms.WebLevelsCache;
import com.anyoption.common.managers.ApiExternalUsersManagerBase;
import com.anyoption.common.managers.InvestmentRejectsManagerBase;
import com.anyoption.common.managers.InvestmentsManagerBase;
import com.anyoption.common.managers.MarketsManagerBase;
import com.anyoption.common.managers.OpportunitiesManagerBase;
import com.anyoption.common.managers.UsersManagerBase;

public class InvestmentValidatorDynamics extends InvestmentValidatorAbstract {
    private static final Logger log = Logger.getLogger(InvestmentValidatorDynamics.class);

    protected InvestmentValidatorParams params;

    public InvestmentValidatorDynamics(InvestmentValidatorParams params) {
        this.params = params;
    }

    @Override
    public MessageToFormat validate(Connection conn, User user, Investment inv, OpportunityCacheBean o, String sessionId, WebLevelsCache levelsCache, WebLevelLookupBean wllb, double convertAmount, InvestmentRejects invRej, double rate, double requestAmount) throws SQLException {
    	MessageToFormat msg = validateCurrentLevel(inv.getCurrentLevel());
    	OpportunityMiniBean oppMiniBean = null;
    	if (msg == null) {
    		oppMiniBean = OpportunitiesManagerBase.getOpportunityMiniBean(o.getId());
    		msg = validateOpportunityOpened(oppMiniBean, user, sessionId, invRej);
    	}
        if (null == msg) {
            msg = validateUsersBalance(conn, user, inv);
        }
        if (null == msg) {
            long s = System.currentTimeMillis();
            msg = validateInvestment(conn, user, o, inv.getAmount(), inv.getOddsWin(), inv.getOddsLose(), invRej, inv.getUtcOffsetCreated());
            if (log.isDebugEnabled()) {
                log.debug("TIME: validateInvestmentLimits " + (System.currentTimeMillis() - s));
            }
        }
        
        if(inv.getPrice() == 0 ) {
        	log.error("Invalid dynamics investment price is 0 !");
        	return new MessageToFormat("error.investment", null);
        }

        Market market = MarketsManagerBase.getMarket(o.getMarketId());
        wllb.setOppId(inv.getOpportunityId());
        wllb.setFeedName(market.getFeedName());
        levelsCache.getLevels(wllb);

        // first deviation check
        if (null == msg) {
            msg = deviationCheck(wllb, o, inv.getTypeId(), inv.getPrice(), user.getUserName(), sessionId, invRej);
        }
        
        //  market disabled check
        if (null == msg && UsersManagerBase.isUserMarketDisabled(user.getId(), o.getMarketId(), o.getScheduled(), false, inv.getApiExternalUserId())) {
            log.warn("User Market Disabled..." + printDetails(user.getUserName(), sessionId, o, inv, wllb));
            invRej.setRejectTypeId(ConstantsBase.INVESTMENT_REJECT_USER_MARKET_DISABELD);
            invRej.setRejectAdditionalInfo("User Market Disabled");
            InvestmentRejectsManagerBase.insertInvestmentReject(invRej);
            msg = new MessageToFormat("error.investment.disabled", null);
        }
        
        //Locale locale = new Locale(LanguagesManagerBase.getLanguage(SkinsManagerBase.getSkin(user.getSkinId()).getDefaultLanguageId()).getCode());
        
		//check exposure - check if we pass the cThreashold
		if (wllb.isAutoDisable()) {
			log.warn("Trading halt " + wllb.getInvestmentsRejectInfo());
			invRej.setRejectTypeId(ConstantsBase.INVESTMENT_REJECT_EXPOSURE);
            invRej.setRejectAdditionalInfo("Trading halt:, " + wllb.getInvestmentsRejectInfo());
            InvestmentRejectsManagerBase.insertInvestmentReject(invRej);
            //sendNotifyMail(user.getUserName(), amount, new Float(0), (int)contractType, e, user.getCurrencyId(), 0, "exposure", null);
            return new MessageToFormat("error.investment", null);
			//return new MessageToFormat("error.dynamic.exposure", null);
		}
        
        // check seconds between two investments
        if (null == msg && params.getSecBetweenInvestmentsSameIp() > 0 &&
                UsersManagerBase.hasTheSameInvestmentInTheLastSecSameIp(inv.getIp(), params.getSecBetweenInvestmentsSameIp(), invRej)) {
            log.info("Submitting investment twice in less than " + params.getSecBetweenInvestmentsSameIp() + " sec from same ip:" +
                    " oppId: " + inv.getOpportunityId() +
                    " choice: " + inv.getTypeId() +
                    " user: " + user.getUserName() +
                    " sessionId: " + sessionId +
                    " ip: " + inv.getIp());
            msg = new MessageToFormat("error.investment.twicein10sec", null);
        }
        invRej.setSecBetweenInvestments(params.getSecBetweenInvestments());
        if (null == msg && UsersManagerBase.hasTheSameInvestmentInTheLastSec(user.getId(), inv.getOpportunityId(), inv.getTypeId(), invRej)) {
            log.info("Submitting the same investment twice in less than " + params.getSecBetweenInvestments() + " sec:" +
                    " oppId: " + inv.getOpportunityId() +
                    " choice: " + inv.getTypeId() +
                    " user: " + user.getUserName() +
                    " sessionId: " + sessionId);
            msg = new MessageToFormat("error.investment.twicein10sec", null);
        }
        
        if (null == msg) {
            msg = InvestmentsManagerBase.validateOpportunity(user, o, oppMiniBean, inv.getOddsWin(), inv.getOddsLose(), invRej);
        }

//      //NIOU validation
//     	if (null == msg) {
//     		String msgNIOU = InvestmentsManagerBase.validateNIOUTypeWithCashBalance(
//     				new ValidatorParamNIOU(user, 0, 0, o.getOpportunityTypeId(), inv.getAmount(), 0, 0), invRej);       		
//     		if (msgNIOU != null) {
//     			msg = new MessageToFormat(msgNIOU, null);
//     		}
//     	}        

        // balance check before insert investment to avoid negative balance when doing simultaneous investments        
        if (null == msg) {
            msg = validateUsersBalance(conn, user, inv);
        }
        if (null == msg) {
        	wllb.setDevCheckLevel(wllb.getDevCheckLevel());
        	wllb.setRealLevel(wllb.getRealLevel());
        	wllb.setGroupClose(wllb.getGroupClose());
        }
        if(msg != null) {
        	log.debug("dynamics validation msg: "+msg.getKey());
        }
        return msg;
    }

    
    public static MessageToFormat deviationCheck(WebLevelLookupBean wllb, OpportunityCacheBean o, long invTypeId, double quote, String userName, String sessionId, InvestmentRejects invRej) {
//
//        BinaryZeroOneHundred quoteParams = o.getBinaryZeroOneHundred();
//        if(quoteParams == null ) {
//        	log.error("Empty quote params - can't load deviation");
//        	return new MessageToFormat("error.investment", null);
//        }
    	Market market = MarketsManagerBase.getMarket(o.getMarketId());
        if (wllb != null && wllb.getDevCheckLevel() != 0 && wllb.getRealLevel() != 0) { // check if we have levels (else we have conn prob)
        	double checkLevel = invTypeId == InvestmentType.DYNAMICS_SELL ? wllb.getBid() : wllb.getOffer();
            double crrSpread = Math.abs(checkLevel - quote);
            double acceptableSpread = market.getAcceptableDeviation().doubleValue();
            if (log.isDebugEnabled()) {
                log.debug("contractType: " + invTypeId +
                        " checkLevel: " + checkLevel +
                        " quote: " + quote +
                        " crr spread: " + crrSpread +
                        " acceptable spread: " + acceptableSpread);
            }
            if (crrSpread <= acceptableSpread) {               
            		return null;
            } else {
            	String warn = "Unacceptable deviation dynamics.";
                log.warn(warn);
                DecimalFormat sd = new DecimalFormat("###,###,##0.000000");
                if(invRej != null) {
	                invRej.setRejectTypeId(ConstantsBase.INVESTMENT_REJECT_DEVIATION_0_100);
	                invRej.setRejectAdditionalInfo(warn + ":, crrSpread: " + sd.format(crrSpread) + " , acceptableSpread: " + sd.format(acceptableSpread) );
	                invRej.setRealLevel(wllb.getRealLevel());
	                invRej.setWwwLevel(wllb.getDevCheckLevel());
	                InvestmentRejectsManagerBase.insertInvestmentReject(invRej);
                }
                return new MessageToFormat("error.investment.deviation", null);
            }
        } else {
            log.warn("No www level or real level. user: " + userName + " sessionId: " + sessionId);
        }
        return new MessageToFormat("error.investment", null);
    }

    public static void sendNotifyMail(User user, double convertedAmount, Investment inv, long currencyId, String emailType, Double realLevel, OpportunityCacheBean o, Market market){
        log.info("Sending mail about " + emailType.toUpperCase() + " to group... ");
        Hashtable<String, String> serverProperties = new Hashtable<String, String>();
        String titleType = user.getClassId() == ConstantsBase.USER_CLASS_TEST ? "TEST " : "";
        titleType += emailType.equalsIgnoreCase("deviation3") ? "D3: " : emailType + " REACHED: ";
        String apiexternalreference = user.getUserName();
        if (inv.getApiExternalUserId() > 0) {
        	try {
				apiexternalreference = ApiExternalUsersManagerBase.getApiExternalUserReferenceById(inv.getApiExternalUserId()) + "(" + apiexternalreference + ")";
			} catch (SQLException e) {
				log.error("cant get api external user reference for api external user id = " + inv.getApiExternalUserId(), e);
			}
        }
    	String marketName = MarketsManagerBase.getMarketName(Skin.SKIN_REG_EN, market.getId());
    	if (o.getOpportunityTypeId() == Opportunity.TYPE_OPTION_PLUS) {
    		marketName += " +";
    	}
        serverProperties.put("url", CommonUtil.getConfig("email.server"));
        serverProperties.put("auth", "true");
        serverProperties.put("user", CommonUtil.getConfig("email.uname"));
        serverProperties.put("pass", CommonUtil.getConfig("email.pass"));

        Hashtable<String, String> emailProperties = new Hashtable<String, String>();
        emailProperties.put("from", CommonUtil.getConfig("email.from"));
        emailProperties.put("to", CommonUtil.getConfig(emailType + ".mailgroup"));
        emailProperties.put("subject", titleType + " - " + apiexternalreference + " - " + Investment.TYPE_NAMES.get(inv.getTypeId()) + " - " + marketName + " - " + Investment.SCHEDULED_NAMES.get(o.getScheduled()));
        DecimalFormat sd = new DecimalFormat("###,###,##0.000000");
        String body = new String();
        body += " user: " + apiexternalreference +"<br/>";
        body += " market name: " + marketName +"<br/>";
        body += " scheduled: " + Investment.SCHEDULED_NAMES.get(o.getScheduled()) +"<br/>";
        body += " exp time: " + o.getTimeEstClosing() +"<br/>";
        body += " type: " + Investment.TYPE_NAMES.get(inv.getTypeId()) + "<br/>";
        body += " amount: " + CommonUtil.formatCurrencyAmountAO(convertedAmount, true, currencyId) + "<br/>" ;
        body += " return: " + inv.getOddsWin() +"<br/>";
        body += " level: " + inv.getCurrentLevel() +"<br/>";
        if (null != realLevel) { //dev 3 email
            body += " reuters level: " + sd.format(realLevel) +"<br/>";
            double crrSpread = Math.abs(realLevel - inv.getCurrentLevel());
            body += " spread: " + sd.format(crrSpread) +"<br/>";
            body += " acceptable level dev 3: " + sd.format(market.getAcceptableDeviation3().doubleValue()) +"<br/>";
        }
        emailProperties.put("body", body);

        CommonUtil.sendEmail(serverProperties, emailProperties, null);
    }
    
    @Override
    protected String printDetails(String userName, String sessionId, OpportunityCacheBean o, Investment inv, WebLevelLookupBean wllb) {
		String details = super.printDetails(userName, sessionId, o, inv, wllb);
		details = " type id (CALL = 1, PUT = 2): " + inv.getTypeId() +
				" realLevel: " + wllb.getRealLevel() +
				" wwwLevel: " + wllb.getDevCheckLevel();
    	return details;
    }
}