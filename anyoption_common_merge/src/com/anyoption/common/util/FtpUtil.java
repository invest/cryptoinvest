/**
 * 
 */
package com.anyoption.common.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;

import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPConnectionClosedException;
import org.apache.commons.net.ftp.FTPReply;
import org.apache.log4j.Logger;

/**
 * @author kirilim
 *
 */
public class FtpUtil {

	private static final Logger log = Logger.getLogger(FtpUtil.class);
	public static final String FTP_SILVERPOP_DOWNLOAD_DIRECTORY = "download";
	public static final String FTP_SILVERPOP_UPLOAD_DIRECTORY = "upload";
	private String ftpServer;
	private String username;
	private String password;
	private int transferType;
	private int port;
	private FTPClient ftpClient;

	/**
	 * Constructs FTP with given transfer type and given working directory.
	 * 
	 * @param ftpServer The FTP server to connect
	 * @param port The FTP server port
	 * @param username The user name for the server
	 * @param password The password for the server
	 * @param transferType The file transfer type
	 * @param workingDirectory The FTP working directory
	 */
	public FtpUtil(String ftpServer, int port, String username, String password, int transferType) {
		this.ftpServer = ftpServer;
		this.port = port;
		this.username = username;
		this.password = password;
		this.transferType = transferType;
		this.ftpClient = new FTPClient();
	}

	/**
	 * Constructs FTP with default text transfer type and default working directory.
	 * 
	 * @param ftpServer The FTP server to connect
	 * @param username The user name for the server
	 * @param password The password for the server
	 */
	public FtpUtil(String ftpServer, String username, String password) {
		this(ftpServer, 0, username, password, FTP.ASCII_FILE_TYPE);
	}

	/**
	 * Constructs FTP with given transfer type and default working directory.
	 * 
	 * @param ftpServer The FTP server to connect
	 * @param username The user name for the server
	 * @param password The password for the server
	 * @param transferType The file transfer type
	 */
	public FtpUtil(String ftpServer, String username, String password, int transferType) {
		this(ftpServer, 0, username, password, transferType);
	}

	/**
	 * Upload a single file
	 * 
	 * @param filePath The file path of the file to be uploaded
	 * 
	 * @return True if successfully uploaded the file, false if not.
	 */
	public boolean uploadFile(String filePath) {
		return uploadFile(filePath, null);
	}

	/**
	 * Upload a single file on specified directory
	 * 
	 * @param filePath The file path of the file to be uploaded
	 * @param workingDirectory The working directory to which the file should be uploaded
	 * 
	 * @return True if successfully uploaded the file, false if not.
	 */
	public boolean uploadFile(String filePath, String workingDirectory) {
		ArrayList<String> list = new ArrayList<String>();
		list.add(filePath);
		return uploadFiles(list, workingDirectory);
	}

	/**
	 * Upload multiple files
	 * 
	 * @param filePaths The file paths of the files to be uploaded
	 * 
	 * @return True if successfully uploaded all files, false if not.
	 */
	public boolean uploadFiles(String[] filePaths) {
		return uploadFiles(filePaths, null);
	}

	/**
	 * Upload multiple files on specified directory
	 * 
	 * @param filePaths The file paths of the files to be uploaded
	 * @param workingDirectory The working directory to which the file should be uploaded
	 * 
	 * @return True if successfully uploaded all files, false if not.
	 */
	public boolean uploadFiles(String[] filePaths, String workingDirectory) {
		return uploadFiles(Arrays.asList(filePaths), workingDirectory);
	}

	/**
	 * Upload multiple files
	 * 
	 * @param filePaths The file paths of the files to be uploaded
	 * 
	 * @return True if successfully uploaded all files, false if not.
	 */
	public boolean uploadFiles(Collection<String> filePaths) {
		return uploadFiles(filePaths, null);
	}

	/**
	 * Upload multiple files on specified directory
	 * 
	 * @param filePaths The file paths of the files to be uploaded
	 * @param workingDirectory The working directory to which the file should be uploaded
	 * 
	 * @return True if successfully uploaded all files, false if not.
	 */
	public boolean uploadFiles(Collection<String> filePaths, String workingDirectory) {
		try {
			boolean result = initFtp(workingDirectory);
			if (!result) {
				log.error("Refused connection or failed login");
				return false;
			}
			log.info("FTP initialized successfully");

			boolean allUploaded = false;
			for (String filePath : filePaths) {
				File file = new File(filePath);
				allUploaded = uploadFile(file);
			}

			releaseFtp();
			log.info("FTP disconnected successfully");
			return allUploaded;
		} catch (SocketException e) {
			log.debug("Socket timeout could not be set", e);
			return false;
		} catch (FileNotFoundException fnfe) {
			log.debug("No file found", fnfe);
			return false;
		} catch (FTPConnectionClosedException cce) {
			log.debug("Connection closed", cce);
			return false;
		} catch (IOException e) {
			log.debug("FTP connect, disconnect or login problem", e);
			return false;
		} finally {
			if (ftpClient.isConnected()) {
				try {
					ftpClient.disconnect();
				} catch (IOException ioe) {
					log.error("Cannot disconnect", ioe);
				}
			}
		}
	}

	/**
	 * Download a single file
	 * 
	 * @param filePath The file path of the file to download
	 * 
	 * @return True if successfully downloaded the file, false if not.
	 */
	public boolean downloadFile(String filePath) {
		return downloadFile(filePath, null);
	}

	/**
	 * Download a single file from specified directory
	 * 
	 * @param filePath The file path of the file to download
	 * @param workingDirectory The working directory from which the file should be downloaded
	 * 
	 * @return True if successfully downloaded the file, false if not.
	 */
	public boolean downloadFile(String filePath, String workingDirectory) {
		ArrayList<String> list = new ArrayList<String>();
		list.add(filePath);
		return downloadFiles(list, workingDirectory);
	}

	/**
	 * Download multiple files
	 * 
	 * @param filePaths The file paths of the files to download
	 * 
	 * @return True if successfully downloaded all files, false if not.
	 */
	public boolean downloadFiles(String[] filePaths) {
		return downloadFiles(filePaths, null);
	}

	/**
	 * Download multiple files from specified directory
	 * 
	 * @param filePaths The file paths of the files to download
	 * @param workingDirectory The working directory from which the files should be downloaded
	 * 
	 * @return True if successfully downloaded all files, false if not.
	 */
	public boolean downloadFiles(String[] filePaths, String workingDirectory) {
		return downloadFiles(Arrays.asList(filePaths), workingDirectory);
	}

	/**
	 * Download multiple files
	 * 
	 * @param filePaths The file paths of the files to download
	 * 
	 * @return True if successfully downloaded all files, false if not.
	 */
	public boolean downloadFiles(Collection<String> filePaths) {
		return downloadFiles(filePaths, null);
	}

	/**
	 * Download multiple files from specified directory
	 * 
	 * @param filePaths The file paths of the files to download
	 * @param workingDirectory The working directory from which the files should be downloaded
	 * 
	 * @return True if successfully downloaded all files, false if not.
	 */
	public boolean downloadFiles(Collection<String> filePaths, String workingDirectory) {
		try {
			boolean result = initFtp(workingDirectory);
			if (!result) {
				log.error("Refused connection or failed login");
				return false;
			}
			log.info("FTP initialized successfully");

			boolean allDownloaded = false;
			for (String filePath : filePaths) {
				File file = new File(filePath);
				allDownloaded = downloadFile(file);
			}

			releaseFtp();
			log.info("FTP disconnected successfully");
			return allDownloaded;
		} catch (SocketException e) {
			log.debug("Socket timeout could not be set", e);
			return false;
		} catch (FileNotFoundException fnfe) {
			log.debug("No file found", fnfe);
			return false;
		} catch (FTPConnectionClosedException cce) {
			log.debug("Connection closed", cce);
			return false;
		} catch (IOException e) {
			log.debug("FTP connect, disconnect or login problem", e);
			return false;
		} finally {
			if (ftpClient.isConnected()) {
				try {
					ftpClient.disconnect();
				} catch (IOException ioe) {
					log.error("Cannot disconnect", ioe);
				}
			}
		}
	}

	private boolean uploadFile(File file) throws IOException {
		log.info("Uploading file: " + file.getPath());
//		ftpClient.enterRemotePassiveMode();
		ftpClient.enterLocalPassiveMode();
		FileInputStream inpStream = new FileInputStream(file);
		boolean result = ftpClient.storeFile(file.getName(), inpStream);
		inpStream.close();
		ftpClient.noop();
		log.info("Uploading result: " + result);
		return result;
	}

	private boolean downloadFile(File file) throws IOException {
		log.info("Downloading file: " + file.getPath());
		ftpClient.enterLocalPassiveMode();
		FileOutputStream outStream = new FileOutputStream(file);
		boolean result = ftpClient.retrieveFile(file.getName(), outStream);
		outStream.close();
		ftpClient.noop();
		log.info("Downloading result: " + result);
		return result;
	}

	private boolean initFtp(String workingDirectory) throws SocketException, IOException {
		if (port != 0) {
			ftpClient.connect(ftpServer, port);
		} else {
			ftpClient.connect(ftpServer);
		}

		// to check if the connection was successful, if not disconnect and stop
		if (!FTPReply.isPositiveCompletion(ftpClient.getReplyCode())) {
			ftpClient.disconnect();
			log.error("FTP server refused connection");
			return false;
		}

		boolean loggedIn = ftpClient.login(username, password);
		if (!loggedIn) {
			// Problem with login to the ftp server, logout and stop
			ftpClient.logout();
			return false;
		}

		ftpClient.setFileType(transferType);

		if (workingDirectory != null && !workingDirectory.isEmpty()) {
			boolean changedDir = ftpClient.changeWorkingDirectory(workingDirectory);
			if (!changedDir) {
				log.info("Couldn't change working directory");
			}
		}

		return true;
	}

	private void releaseFtp() throws IOException {
		ftpClient.logout();
		ftpClient.disconnect();
	}

	/**
	 * @return the transfer type
	 */
	public int getTransferType() {
		return transferType;
	}

	/**
	 * Changes the ftp transfer type
	 * 
	 * @param transferType the transfer type to set
	 */
	public void setTransferType(int transferType) {
		this.transferType = transferType;
	}

	public String getFtpServer() {
		return ftpServer;
	}

	public String getUsername() {
		return username;
	}

	public String getPassword() {
		return password;
	}
}