package com.anyoption.common.util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import org.jboss.logging.Logger;
import oracle.jdbc.driver.OracleDriver;

/**
 * @author LioR SoLoMoN
 *
 */
public abstract class OracleUtil {
	private static final Logger logger = Logger.getLogger(OracleUtil.class);
	
    /**
     * @param url
     * @param user
     * @param password
     * @return
     * @throws SQLException
     */
    public static Connection getConnection(String url, String user, String password) throws SQLException { 
    	logger.info("about to get oracle database connection");
    	DriverManager.registerDriver(new OracleDriver());
   	 	return DriverManager.getConnection(url, user, password);
    }

    /**
     * close connection. 
     * set auto commit to true if it was false.
     * @param connection
     */
    public static void closeConnection(Connection connection) {
    	logger.info("about to close oracle database connection");
    	if (connection != null) {
    		try {    		
	    		if(!connection.getAutoCommit()) {
	    			logger.error("*** The connection is with AutoCommit false ***");
	    			logger.error("Stack Trace:");
	    			for (StackTraceElement ste : Thread.currentThread().getStackTrace()) {
	    				logger.error(ste);
					}
	    			logger.info("*** about to switch the connection to AutoCommit true ***");
	    			connection.setAutoCommit(true);
	    		}
    		} catch (SQLException sqle) {
                logger.warn("Can not make auto commit true", sqle);
            }    		
    		try {
                connection.close();
                logger.info("*** connection closed ***");
            } catch (SQLException sqle) {
                logger.error("Can not close connection", sqle);
            }
        }
    }
}
