package com.anyoption.common.util;

import java.security.MessageDigest;

/**
 * SHA is a cryptographic message digest algorithm similar to MD5. SHA-1 hash considered to be one
 * of the most secure hashing functions, producing a 160-bit digest (40 hex numbers) from any data
 * with a maximum size of 264 bits
 */
public class Sha1 {
	/**
	 * convert byte array to Hex String, second implementation option
	 * @param data
	 * @return
	 */
    private static String toHexString(byte[] data) {
        StringBuffer sb = new StringBuffer();
        String s = null;
        for (int i = 0; i < data.length; i++) {
            s = Integer.toHexString(data[i] & 0x000000FF);
            if (s.length() == 1) {
                sb.append("0");
            }
            sb.append(s);
        }
        return sb.toString();
    }

    /**
     * Sha1 encode function
     * @param text the text to encode
     * @return
     * @throws Exception
     */
    public static String encode(String text) throws Exception {
		MessageDigest md = MessageDigest.getInstance("SHA-1");
		md.update(text.getBytes("iso-8859-1"), 0, text.length());
		return toHexString(md.digest());
    }
}