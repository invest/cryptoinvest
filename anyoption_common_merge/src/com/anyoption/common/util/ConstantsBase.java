package com.anyoption.common.util;

import java.util.GregorianCalendar;

public class ConstantsBase implements com.copyop.common.Constants{ // TODO remove interface 
	// platform id's
	public static final int PLATFORM_ID_ERTADER = 1;
	public static final int PLATFORM_ID_ANYOPTION = 2;
	public static final int PLATFORM_ID_COPYOP = 3;
	// login failed count
	public static final int WEB_FAILED_LOGIN_COUNT = 5;
	public static final int BACKEND_FAILED_LOGIN_COUNT = 5;

	public static final String LOGIN_ID = "loginId";
	
	public static final int MAX_COMMENTS = 80;

	public static final String SKIN_ID = "s";
	public static final String COMBINATION_ID = "combid";
	public static final String DYNAMIC_PARAM = "dp";
	public static final String HTTP_REFERE = "Referer";
	public static final String UTM_MEDIUM = "utm_medium";
	public static final String UTM_SOURCE = "utm_source";

	public static final String HOST_ANYOPTION = "anyoption.com";
	public static final String HOST_MIOPCIONES = "miopciones.com";
	public static final String HOST_TLV_TRADE = "tlvtrade.co.il";
    public static final String HOST_ETRADER = "etrader.co.il";
    public static final String HOST_VIP168 = "168qiquan.com";
    public static final String HOST_ANYOPTION_IT = "anyoption.it";
    public static final String HOST_ANYOPTION_TR = "anyoption.cc";
	public static final String CONFIG_HOST_ANYOPTION = "anyoption_com";
	public static final String CONFIG_HOST_ANYOPTION_IT = "anyoption_it";
    public static final String CONFIG_HOST_ANYOPTION_TR = "anyoption_tr";
	public static final String CONFIG_HOST_MIOPCIONES = "miopciones_com";
	public static final String CONFIG_HOST_TLV_TRADE = "tlvtrade";
	public static final String SUBDOMAIN_MOBILE_ANYOPTION = "m";

	public static final long MARKETING_TRACKING_CLICK = 1;
	public static final long MARKETING_TRACKING_CALL_ME = 2;
	public static final long MARKETING_TRACKING_SHORT_REG = 3;
	public static final long MARKETING_TRACKING_FULL_REG = 4;
	public static final long MARKETING_TRACKING_FIRST_DEPOSIT = 5;
	public static final long MARKETING_TRACKING_LOGIN = 6;
	public static final long MARKETING_TRACKING_DAYS_CHECK = 45;

	public static final String TABLE_TRANSACTIONS = "transactions";
    public static final String TABLE_INVESTMENTS = "investments";
    public static final String TABLE_TAX_HISTORY = "tax_history";
    public static final String TABLE_BONUS_USERS = "bonus_users";

    public static final String ERROR_DEPOSIT_VELOCITY_AO_NO_DOC = "error.deposit.velocity.ao.no.doc";
    public static final String ERROR_DEPOSIT_VELOCITY_AO_HAVE_DOC = "error.deposit.velocity.ao.have.doc";
    public static final String ERROR_DEPOSIT_VELOCITY_ET_NO_DOC = "error.deposit.velocity.et.no.doc";
    public static final String ERROR_DEPOSIT_VELOCITY_ET_HAVE_DOC = "error.deposit.velocity.et.have.doc";
    public static final String ERROR_DISABLED_CC_DEPOSIT = "error.disabled.cc.deposit";
    public static final String UNIX_HOSTNAME_COMMAND = "hostname ";
    public static String UNIX_ERROR_STREAM = "runError: ";
    public static String IP_NOT_FOUND = "IP Address not found";
    public static final String EMPTY_STRING = "";
    public static final String REG_HASH_KEY = "regHashKey";

    public static final String COOKIE_DOMAIN = "cookie.domain";
    public static final String CREATE_ISSUE_FOR_REGULATION = "regMandQuestDone";
    public static final long NETREFER_CAMPAIGN_ID = 365;
    public static final long REFERPARTNER_CAMPAIGN_ID = 1535;

    public static final String DISPLAY_FORMAT_LIVE_PAGE = "###,###,##0";

    public static final int JSON_ERROR_CODE_SUCCESS	= 0;
    public static int BONUS_TERMS_EXAMPLE = 2000;

    public static final long LOGIN_FROM_REGULAR = 0;

    public static final String UTCOFFSET_DEFAULT_PERIOD1 = "GMT+03:00";
    public static final String UTCOFFSET_DEFAULT_PERIOD2 = "GMT+02:00";
    public static final String UTC_OFFSET = "utcOffset";
    //Issue type
	public static int ISSUE_TYPE_REGULAR  = 1;

	public static final int COLOR_BLACK_ID = 1;
	public static final int COLOR_BLUE_ID = 2;
	public static final int COLOR_RED_ID = 3;
	public static final int COLOR_GREEN_ID = 4;
	public static String COLOR_BLACK = "#000000";
	public static String COLOR_BLUE = "#0101DF";
	public static String COLOR_RED = "#FF0000";
	public static String COLOR_GREEN = "#088A08";
	public static String COLOR_ORANGE = "#FF8000";
	public static String COLOR_HIGHLIGHT_RECORD= "background-color: #FDD017;";
	public static String COLOR_RISK_HIGHLIGHT_RECORD= "background-color:#B0E2FF;";
	public static String COLOR_ISSUE_PENDING_DOCS = "background-color:#CCCCFF;";
	public static String COLOR_HIGHLIGHT_RECORD_ORANGE= "color: #FF9900;font-weight: bold;";

	//issue subject
	public static final int ISSUE_SUBJECT_PENDING_DOC = 56;
	public static final String ISSUE_ACTIVITY_PENDING_DOC="44";
	public static final String ISSUE_ACTION_TYPE_COMMENT = "37";
	
	//issue priority
	public static final long ISSUE_PRIORITY_LOW = 1;
	
	//issue status
	public static final long ISSUE_STATUS_PENDING = 2;
	
	//issue component
	public static final int ISSUE_COMPONENT_ETRADER_ANYOPTION = 1;
	
	//issue channel id
	public static final String ISSUE_CHANNEL_COMMENTS_ID = "4";
	
	public static final String LANDING_QUERY_STRING = "landing_query_string";
    public static String UNIX_CHECKMX_COMMAND = "checkmx ";

    public static String UNIX_GEO_IP_LOOKUP_COMMAND = "geoiplookup ";
    public static String UNIX_GEO_IP_LOOKUP_RES_COUNTRY = ",";
    public static String UNIX_GEO_IP_LOOKUP_RES_ERROR_COUNTRY = "--";

    //Bonus Management System (BMS)
    public static int BONUS_MANAGEMENT_TIME_FRAME = 3;
    public static double BONUS_MANAGEMENT_PROFIT_PERCENT = 0.075;
    public static int BONUS_INVESTMENTS_TYPE_INSERT_INV = 1;
    public static int BONUS_INVESTMENTS_TYPE_SETTLED_INV = 2;

    // Calcalist
	public static final String CAL_GAME_USER_NAME_SUFFIX = "_cg";

	public static final long SALES_TYPE_DEPARTMENT_GENERAL_RETENTION = 1;
	public static final long SALES_TYPE_DEPARTMENT_COMA_RETENTION = 2;
	public static final long SALES_TYPE_DEPARTMENT_UPGRADE_RETENTION = 3;
	public static final int DO_NOT_SELECT = -1;

	public static final long SALES_TYPE_CONVERSION = 1;
    public static final long SALES_TYPE_RETENTION = 2;
    public static final long SALES_TYPE_BOTH = 3;

	public static final long BANK_WIRE_OTHER_BANK = 40;

	public static final int FROM_GRAPH_DEFAULT = 0;
	public static final int FROM_GRAPH_FLASH = 1; //investment from profit line ,option + or binary 0-100.

	public static final int INVESTMENT_REJECT_OPP_DISABELD = 1;
	public static final int INVESTMENT_REJECT_OPP_SUSPENDED = 111;
	public static final int INVESTMENT_REJECT_MIN_INV_LIMIT = 2;
	public static final int INVESTMENT_REJECT_MAX_INV_LIMIT = 3;
	public static final int INVESTMENT_REJECT_ODDS_CHANGE = 4;
	public static final int INVESTMENT_REJECT_DEVIATION = 5;
	public static final int INVESTMENT_REJECT_DEVIATION_2 = 6;
	public static final int INVESTMENT_REJECT_LAST_INV_IN_LESS_THEN_X_SEC = 7;
	public static final int INVESTMENT_REJECT_USER_MARKET_DISABELD = 8;
	public static final int INVESTMENT_REJECT_EXPOSURE = 9;
	public static final int INVESTMENT_REJECT_DIFFRENT_LEVEL_ONE_TOUCH = 10;
    public static final int INVESTMENT_REJECT_DEVIATION_3 = 11;
    public static final int INVESTMENT_REJECT_LAST_INV_IN_LESS_THEN_X_SEC_IP = 12;
    public static final int INVESTMENT_REJECT_DEVIATION_0_100 = 13;
    public static final int INVESTMENT_REJECT_EXPOSURE_0_100 = 14;
    public static final int INVESTMENT_REJECT_DEVIATION_WRONG_DIRECTION = 15;
    public static final int INVESTMENT_REJECT_INV_SUM_LIMIT = 16;
    public static final int INVESTMENT_REJECT_HIGH_EXPOSURE_LIMIT = 17;
    public static final int INVESTMENT_REJECT_NIOU_NO_CASH_BALANCE = 18;

    public static final int OPPORTUNITIES_TYPE_DEFAULT = 1;
    public static final int OPPORTUNITIES_TYPE_ONE_TOUCH = 2;
//    public static final int ONE_TOUCH_IS_UP = 1;
//    public static final int ONE_TOUCH_IS_DOWN = 0;

	public static final String STRING_TRUE = "1";
	public static final String STRING_FALSE = "0";
	public static final String STRING_YES = "Yes";
	public static final String STRING_NO = "No";
	public static final String UNASSIGN_RETENTION_REP = "0";
	public static final long NON_SELECTED = 0;

	public static final String ETS_MARKETING_PARAMETARS_SERVLET = "param";
	public static final String ETS_CAMPAIGN_COMBINATION_SERVLET = "comb";
	public static final String ETS_FIRST_DEPOSIT_SERVLET = "fd";
	public static final String ETS_ORGANIC_CLICK_SERVLET = "org";

    //insurance types
    public static int INSURANCE_GOLDEN_MINUTES = 1;
    public static int INSURANCE_ROLL_UP = 2;

	public static final int OPPORTUNITY_TYPE_BINARY_ = 1;
	public static final int OPPORTUNITY_TYPE_ONE_TOUCH = 2;
	public static final int OPPORTUNITY_TYPE_OPTION_PLUS = 3;

	/**
	 * This is the prefix for JMS messages that carry top markets ids by market
	 * group for a skin.
	 */
	public static final String JMS_MARKETS_BY_GROUP_PREF = "aomargr";
	public static final String JMS_OPPS_UPDATE = "opps";
	public static final String JMS_OPPS_UPDATE_COMMAND_ADD = "add";
	public static final String JMS_OPPS_UPDATE_COMMAND_DELETE = "delete";
	public static final String JMS_OPPS_UPDATE_COMMAND_CLEAR = "cleat";

	public static final long MARKET_GROUP_OPTION_PLUS_ID = 99;
	public static final long MARKET_GROUP_DYNAMICS_ID = 100;
	
    public static final String MARKET_GROUP_OPTION_PLUS_DISPLAY_NAME = "Option";
    public static final String MARKET_GROUP_DYNAMICS_NAME = "Dynamics";

	public static final int BONUS_NEW_FLAG_EXPIRY_DAYS = 14;

	public static final long MC_URL_SOUREC_TYPE_WEB = 1;
	public static final long MC_URL_SOUREC_TYPE_ANDROID = 2;
	public static final long MC_URL_SOUREC_TYPE_IOS = 3;
	// Enumerator for the total sum of deposits.
	public static final String ENUM_SUM_DEPOSIT_CODE = "distinguish_upgrade_retention_team";
	public static final String ENUM_SUM_DEPOSIT_ENUMERATOR = "sum_deposit";
	// Enumerator for the total sum of deposits new.
	public static final String ENUM_COUNT_DEPOSIT_CODE = "distinguish_upgrade_retention_team_new";
	public static final String ENUM_COUNT_DEPOSIT_ENUMERATOR = "deposits_count";

	public static final long BONUS_STATE_GRANTED = 1;
    public static final long BONUS_STATE_ACTIVE = 2;
	public static final long BONUS_STATE_USED = 3;
    public static final long BONUS_STATE_DONE = 4;
    public static final long BONUS_STATE_REFUSED = 5;
    public static final long BONUS_STATE_MISSED = 6;
	public static final long BONUS_STATE_CANCELED = 7;
	public static final long BONUS_STATE_WITHDRAWN = 8;
	public static final long BONUS_STATE_WAGERING_WAIVED = 9;
	public static final long BONUS_STATE_PENDING = 10;


	public static final String REWARD_PLAN_LOG_PREFIX = "Reward Plan; ";
	public static int NUM_OF_REAL_SUCCESS_DEPOSIT = 1;
	public static int REWARD_MAXIMUM_VALID_EXP_PNTS = 100000000;
	public static long CC_VISIBLE = 1;

	public static final int USER_CLASS_COMPANY = 2;
	public static final int USER_CLASS_PRIVATE= 1;
	public static final int USER_CLASS_TEST = 0;
	public static final int USER_CLASS_ALL = 100;
	public static final int USER_CLASS_COMPANY_OR_PRIVATE = 101;
	public static final String USER_CLASS_COMPANY_OR_PRIVATE_LABLE = "user.class.privatecompany";

	public static final String FILES_PATH_AVATAR = "files.path.avatar";
	public static final String SERVER_URL_AVATAR = "server.url.avatar";
	public static final String SERVER_URL_HOME_PAGE = "homepage.url";
	public static final String SERVER_URL_HOME_PAGE_HTTPS = "homepage.url.https";
	public static final String ALLOWED_FILE_EXTENSIONS_REGEX = "^.*\\.(jpg|jpeg|bmp|png|pdf|tif|tiff)$";

	public static final int MAXIMUM_ORACLE_IN_CLAUSE = 1000;

	public static final int FILTER_ALL = -1;
	public static final int FILTER_YES = 0;
	public static final int FILTER_NO = 1;

	public static final String MOBILE_DES_IPHONE = "Iphone";
	public static final String MOBILE_DES_IPOD = "Ipod";
	public static final String MOBILE_DES_ANDROID = "Android";
	public static final String MOBILE_DES_OTHER = "Other";
	public static final String MOBILE_DES_NEW_ANDROID_APP = "Android";
	public static final String MOBILE_DES_NEW_IOS_APP = "iOS";
	
    public static final int LOG_BALANCE_CC_DEPOSIT = 1;
    public static final int LOG_BALANCE_CANCEL_DEPOSIT = 2;
    public static final int LOG_BALANCE_ADMIN_DEPOSIT = 3;
    public static final int LOG_BALANCE_ADMIN_WITHDRAW = 4;
    public static final int LOG_BALANCE_WIRE_DEPOSIT = 5;
    public static final int LOG_BALANCE_REJECT_WITHDRAW = 6;
    public static final int LOG_BALANCE_REVERSE_WITHDRAW = 7;
    public static final int LOG_BALANCE_CANCEL_INVESTMENT = 8;
    public static final int LOG_BALANCE_CHEQUE_WITHDRAW = 9;
    public static final int LOG_BALANCE_INSERT_INVESTMENT = 10;
    public static final int LOG_BALANCE_SETTLE_INVESTMENT = 11;
    public static final int LOG_BALANCE_CASH_DEPOSIT = 12;
    public static final int LOG_BALANCE_TAX_PAYMENT = 13;
    public static final int LOG_BALANCE_CC_WITHDRAW = 14;
    public static final int LOG_BALANCE_BONUS_DEPOSIT = 15;
    public static final int LOG_BALANCE_BONUS_WITHDRAW = 16;
    public static final int LOG_BALANCE_BANK_WIRE = 17;
    public static final int LOG_BALANCE_DIRECT_DEPOSIT = 18;
    public static final int LOG_BALANCE_POINTS_TO_CASH = 19;
    public static final int LOG_BALANCE_FIX_BALANCE_DEPOSIT = 20;
    public static final int LOG_BALANCE_FIX_BALANCE_WITHDRAW = 21;
    public static final int LOG_BALANCE_DEPOSIT_BY_COMPANY = 22;
    public static final int LOG_BALANCE_PAYPAL_DEPOSIT = 23;
    public static final int LOG_BALANCE_PAYPAL_WITHDRAW = 24;
    public static final int LOG_BALANCE_CASHU_DEPOSIT = 25;
    public static final int LOG_BALANCE_ACH_DEPOSIT = 26;
    public static final int LOG_BALANCE_UKASH_DEPOSIT = 27;
    public static final int LOG_BALANCE_CHARGE_BACK = 28;
    public static final int LOG_BALANCE_ENVOY_WITHDRAW = 29;
    public static final int LOG_BALANCE_MONEYBOOKERS_DEPOSIT = 30;
    public static final int LOG_BALANCE_RESETTLE_INVESTMENT = 31;
    public static final int LOG_BALANCE_EFT_DEPOSIT = 32;
    public static final int LOG_BALANCE_EFT_WITHDRAW = 33;
    public static final int LOG_BALANCE_MONEYBOOKERS_WITHDARW = 34;
    public static final int LOG_BALANCE_WEBMONEY_DEPOSIT = 35;
    public static final int LOG_BALANCE_WEBMONEY_WITHDRAW = 36;
    public static final int LOG_BALANCE_DELTA_PAY_CHINA_DEPOSIT = 37;
    public static final int LOG_BALANCE_DELTA_PAY_CHINA_WITHDRAW = 38;
    public static final int LOG_BALANCE_CDPAY_CHINA_DEPOSIT = 39;
    public static final int LOG_BALANCE_CDPAY_CHINA_WITHDRAW = 40;
    public static final int LOG_BALANCE_BAROPAY_DEPOSIT = 41;
    public static final int LOG_BALANCE_BAROPAY_WITHDRAW = 42;
    public static final int LOG_BALANCE_CUP_DEPOSIT = 43;
    public static final int LOG_BALANCE_CUP_WITHDRAW = 44;
    public static final int LOG_BALANCE_MAINTENANCE_FEE = 45;
    public static final int LOG_BALANCE_MAINTENANCE_FEE_CANCEL = 46;
    public static final int LOG_BALANCE_FIX_NEGATIVE_BALANCE_DEPOSIT = 47;
    public static final int LOG_BALANCE_COPYOP_COINS_TO_CASH = 48;
    public static final int LOG_BALANCE_INATEC_IFRAME_DEPOSIT = 49;
    public static final int LOG_BALANCE_EPG_FAILED_WITHDRAW = 50;
    public static final int LOG_BALANCE_EPG_CHECKOUT_DEPOSIT = 52;
    public static final int LOG_BALANCE_EPS_WITHDRAW = 53;
    public static final int LOG_BALANCE_GIROPAY_WITHDRAW = 54;
    public static final int LOG_BALANCE_DIRECT24_WITHDRAW = 55;
    
	public static final String COUNTRY_A2_UNITED_KINGDOM = "GB";
	public static final String COUNTRY_A2_CANADA = "CA";
	public static final String COUNTRY_REGION_CA_FORBIDEN = "QC SK ON"; //insert new CA regions here
    // ************** When adding a new LOG_BALANCE enumorator, change the MAX_LOG_BALANCE_ID to the last enum value ************
    // *************** And update DailyBalanceReport Job (See "Changes upon adding a new log_balance Enumorator" on dailyBalanceReport document) ***************
    public static final int MAX_LOG_BALANCE_ID = 48;
    
    public static final int SERVER_PIXEL_TYPE_FIRST_DEPOSIT = 1;
    public static final int SERVER_PIXEL_TYPE_TRADE_PROFIT = 2;
    public static final int SERVER_PIXEL_TYPE_REGISTER = 4;
    
    public static final String ID_NUM_DEFAULT_VALUE = "No-Id";
    public static final String GENDER_MALE = "M";

    //Default time for refresh ENUMS
    public static final String DEFAULT_ENUMS_REFRESH = "60000";
    
    public static final String ENUM_ETRADER_BONUS = "etrader_bonus";
    public static final String ENUM_ETRADER_BONUS_RESTRICT_DATE = "etrader_bonus_restrict_date";
    public static final String ENUM_ETRADER_BONUS_RESTRICT = "etrader_bonus_restrict";
    
    public static final String ENUM_WITHDRAWAL_WEEKEND = "withdrawal_weekend";
    public static final String ENUM_WITHDRAWAL_WEEKEND_ANYOPTION = "withdrawal_weekend_anyoption";
    public static final String ENUM_WITHDRAWAL_WEEKEND_ETRADER = "withdrawal_weekend_etrader";
    
    
    public static final long MARKETING_COMPANY_APPSFLYER = 1;
    public static final String QUERY_STRING = "query_string";
    // ET Regulation
    public static final int BO_KNOWLEDGE_QUESTION_ANSWER_IS_YES= 1;
    public static final int BO_KNOWLEDGE_QUESTION_ANSWER_IS_NO= 0;
    
    //AO Regulation
    public static final int AO_REGULATION_USER_UNRESTRICTED = 1;
    
    // SMS providers
    public static final int MAX_CHARACTERS_ALLOWED_BY_MBLOX_PROVIDER = 156;
    public static final int MAX_CHARACTERS_ALLOWED_BY_MOBIVATE_PROVIDER = 160;
    public static final long TEXTLOCAL_PROVIDER = 5;
    public static final long MOBIVATE_PROVIDER = 6;
	// marketing landing page
	public static final int MARKETING_LANDING_PAGE_STATIC = 1;
	public static final int MARKETING_LANDING_PAGE_DYNAMIC = 2;
	
    public static final long COUNTRY_NORTH_KOREA_ID = 154;    
    
	public static final int ANYOPTION_URL = 2;
	public static final int COPYOP_URL = 2;
	public static final int MIOPCIONES_URL = 3;
	
	public static final String FILES_PATH = "files.path";
	public static final int NUM_OF_AVATARS = 50;
	
    //Bank wire
    public static final int BANK_TYPE_DEPOSIT  = 1;
    public static final int BANK_TYPE_WITHDRAWAL  = 2;

    public static final String ERROR_MIN_DEPOSIT="error.min.deposit";
	public static final String ERROR_MAX_DEPOSIT="error.max.deposit";
	public static final String ERROR_MAX_DAILY_DEPOSIT="error.max.daily.deposit";
	public static final String ERROR_MAX_MONTHLY_DEPOSIT="error.max.monthly.deposit";
	public static final String ERROR_CARD_NOT_ALLOWED_WRONG_ID="error.card.not.allowed.wrong.id";
	public static final String ERROR_CARD_NOT_ALLOWED_DUP_CARD="error.card.not.allowed.dup.card";
	public static final String ERROR_CARD_NOT_ALLOWED="error.card.not.allowed";
	public static final String ERROR_CARD_COUNTRY_BLOCKED="error.country.blocked";
	public static final String ERROR_CARD_EXPIRED = "error.creditcard.expdate";
	public static final String ERROR_CARD_FAILURE_REPEAT = "error.card.failure.repeat";

	public static final String ERROR_CARD_IN_USE="error.card.already.inuse";
	public static final String ERROR_BLACK_LIST="error.black.list";
	public static final String ERROR_BIN_BLACK_LIST="error.bin.black.list";
	public static final String ERROR_FAILED_CLEARING="error.card.clearing";
	public static final String ERROR_CLEARING_TIMEOUT="error.creditcard.timeout";
	public static final String ERROR_ILLEGAL_HOLDER_ID="error.creditcard.holderid.not.match.card";
	public static final String ERROR_FAILED_CAPTURE="error.card.capture";
	public static final String ERROR_BONUS_DEPOSIT="error.bonus.deposit";
	public static final String ERROR_BONUS_WITHDRAW="error.bonus.withdraw";
	public static final String ERROR_DEPOSIT_DOCS_REQUIRED="error.deposit.docs.required";
	public static final String ERROR_DEPOSIT_DOCS_REQUIRED_MSG="error.deposit.docs.required.msg";
	public static final String ERROR_PAYPAL_DEPOSIT_CANCELED="error.paypal.deposit.canceled";
	public static final String ERROR_MONEYBOOKERS_DEPOSIT_CANCELED="error.moneybookers.deposit.canceled";
	public static final String ERROR_BAROPAY_DEPOSIT_CANCELED="error.baropay.deposit.canceled";
	public static final String ERROR_CDPAY_DEPOSIT_CANCELED="error.cdpay.deposit.canceled";
	
	public static final long BONUS_CLASS_TYPE_FIXED = 1;
	public static final long BONUS_CLASS_TYPE_PERCENT = 2;
	public static final long BONUS_CLASS_TYPE_NEXT_INVEST_ON_US = 3;
	
    public static final int CC_FEE_CANCEL_YES = 1;
    public static final int CC_FEE_CANCEL_NO = 0;
    
    public static final long RATE_PRECISISON = 5;
    
	public static long CC_TYPE_MASTERCARD = 1;
	public static long CC_TYPE_VISA = 2;
	
    public static final long BONUS_TYPE_INSTANT_AMOUNT = 1;
    public static final long BONUS_TYPE_INSTANT_NEXT_INVEST_ON_US = 2;
    public static final long BONUS_TYPE_AMOUNT_AFTER_DEPOSIT = 3;
    public static final long BONUS_TYPE_PERCENT_AFTER_DEPOSIT = 4;
    public static final long BONUS_TYPE_AMOUNT_AFTER_WAGERING = 5;
    public static final long BONUS_TYPE_NEXT_INVEST_ON_US_AFTER_WAGERING = 6;
    public static final long BONUS_TYPE_NEXT_INVEST_ON_US_AFTER_X_INVS = 7;
    public static final long BONUS_TYPE_CONVERT_POINTS_TO_CASH = 8;
    public static final long BONUS_TYPE_INVESTMENT_INCREASED_RETURN_AND_REFUND = 9;
    public static final long BONUS_TYPE_INVESTMENT_INCREASED_RETURN = 10;
    public static final long BONUS_TYPE_INVESTMENT_INCREASED_REFUND = 11;
    public static final long BONUS_TYPE_PERCENT_AFTER_DEPOSIT_AND_SUM_INVEST = 12;
    public static final long BONUS_TYPE_ROUNDUP = 13;
    
	public static final int MAILBOX_STATUS_NEW = 1;
	public static final int MAILBOX_STATUS_READ = 2;
	public static final int MAILBOX_STATUS_DETETED = 3;

	public static final int MAILBOX_TYPE_BONUS = 1;
	public static final int MAILBOX_TYPE_FREE_TEXT = 2;
	public static final int MAILBOX_TYPE_BONUS_ROUND_BALANCE = 15;

	public static final long MAILBOX_POPUP_TYPE_NONE = 1;
	public static final long MAILBOX_POPUP_TYPE_EMAIL_CONTENT = 2;
	public static final long MAILBOX_POPUP_TYPE_EMAIL_ALERT = 3;

    public static final long DYNAMIC_DEPOSIT_GET_AMOUNT_AFTER_SUM_INVEST_ID = 319;
    public static final long DYNAMIC_AMOUNT_AFTER_SUM_INVEST_ID = 320;
    public static final long SUM_INVEST_OR_AMOUNT_ZERO = 0;

    public static final String FREE_INVEST_MIN = "free_invest_min";
    public static final String FREE_INVEST_MAX = "free_invest_max";

	public static final long XMASS_BONUS_ID = 242;
	
	public static final int BONUS_AUTO_PARAMETER_TYPE_REGULAR 		= 1;
	public static final int BONUS_AUTO_PARAMETER_TYPE_REMARKETING 	= 2;
	public static final int BONUS_AUTO_PARAMETER_TYPE_LOGIN 		= 3;

    public static final int BONUS_ROUND_UP_TYPE_ROUND_UP = 1;
    public static final int BONUS_ROUND_UP_TYPE_GET_STARTED = 2;
    public static final int BONUS_ROUND_UP_TYPE_MIN_INVESTMENT = 4;

    public static final long BONUS_ACTIVATION_TYPE_BY_TRANSACTION = 1;
    public static final long BONUS_ACTIVATION_TYPE_BY_INV_AMOUNT = 2;
    public static final long BONUS_ACTIVATION_TYPE_BY_INV_COUNT = 3;
    
    public static final String RECAPTCHA_REQUEST = "g-recaptcha-response";
    
    public static final String RECAPTCHA_SECRET_ANYOPTION = "6Lfmig0TAAAAACiZMPJjN04IQCwGPTYgQrv7Tmbk";
    public static final String RECAPTCHA_SECRET_COPYOP = "6Lf_jA0TAAAAAFufL0j4KDNpFzDfQBFZZJyRfN0e";
    public static final String RECAPTCHA_SECRET_ETRADER = "6Le7jA0TAAAAACE6xEN1eCFOhePwBOnSsN--aprQ";
    
    //Mail Templates
    public static long TEMPLATE_REGULATION_PEP = 75;
    public static final String PARAM_TEMPLATE_PEP_MAIL = "PEP_procedure.html";
    public static final String PARAM_COMPLIANCE_MAIL = "compliance@anyoption.com";
    
    public static final String NO_CASH_FOR_NIOU_MSG = "NO_CASH_FOR_NIOU";

	public static final double TAX_PERCENTAGE = 0.25;

  	public static final String APPLICATION_SOURCE = "application.source";
    public static final String APPLICATION_SOURCE_WEB = "web";
    public static final String APPLICATION_SOURCE_BANNERS = "banners";
    public static final String APPLICATION_SOURCE_BACKEND = "backend";
    public static final String APPLICATION_SOURCE_CONSOLE = "console";
    
    public static final int RESPONSE_STATUS_SUCCESS = 200;

    public static final String CURRENCY_BASE = "currency.usd";
    public static final long CURRENCY_BASE_ID = 2;

  	public static final String CURRENCY_ALL = "currencies.all";
    public static final long CURRENCY_ALL_ID = 0;
    public static final String CURRENCY_ILS = "currency.ils";
    public static final String CURRENCY_SMS_ILS = "currency.sms.ils";
    public static final long CURRENCY_ILS_ID = 1;
    public static final String CURRENCY_USD = "currency.usd";
    public static final String CURRENCY_SMS_USD = "currency.sms.usd";
    public static final long CURRENCY_USD_ID = 2;
    public static final String CURRENCY_USD_CODE = "currency.usd.code";
    public static final String CURRENCY_USD_IBAN = "DE 94 5123 0800 0000 0501 44";
    public static final String CURRENCY_USD_ACCOUNT_NUMBER = "50144" ;
    public static final String CURRENCY_EUR = "currency.eur";
    public static final String CURRENCY_SMS_EUR = "currency.sms.eur";
    public static final long CURRENCY_EUR_ID = 3;
    public static final String CURRENCY_EUR_IBAN = "DE 58 5123 0800 0000 0501 13";
    public static final String CURRENCY_EUR_ACCOUNT_NUMBER = "50113" ;
    public static final String CURRENCY_GBP = "currency.gbp";
    public static final String CURRENCY_SMS_GBP = "currency.sms.gbp";
    public static final String CURRENCY_GBP_IBAN = "DE67 5123 0800 0000 0501 45";
    public static final String CURRENCY_GBP_ACCOUNT_NUMBER = "50145" ;
    public static final long CURRENCY_GBP_ID = 4;
    public static final String CURRENCY_TRY = "currency.try";
    public static final String CURRENCY_SMS_TRY = "currency.sms.try";
    public static final String CURRENCY_TRY_IBAN = "DE 42 5123 0800 0000 0502 07";
    public static final String CURRENCY_TRY_ACCOUNT_NUMBER = "50207" ;
    public static final long CURRENCY_TRY_ID = 5;
    public static final long CURRENCY_RUB_ID = 6;
    public static final String CURRENCY_RUB = "currency.rub";
    public static final String CURRENCY_SMS_RUB = "currency.sms.rub";
    public static final long CURRENCY_CNY_ID = 7;
    public static final String CURRENCY_CNY = "currency.cny";
    public static final String CURRENCY_SMS_CNY = "currency.sms.cny";
    public static final long CURRENCY_KRW_ID = 8;
    public static final long CURRENCY_KR_ID = 8;
    public static final String CURRENCY_KRW = "currency.krw";
    public static final String CURRENCY_SMS_KRW = "currency.sms.krw";
    public static final long CURRENCY_SEK_ID = 9;
    public static final String CURRENCY_SEK = "currency.sek";
    public static final String CURRENCY_SMS_SEK = "currency.sms.sek";
    public static final long CURRENCY_AUD_ID = 10;
    public static final String CURRENCY_AUD = "currency.aud";
    public static final String CURRENCY_SMS_AUD = "currency.sms.aud";
    public static final long CURRENCY_ZAR_ID = 11;
    public static final String CURRENCY_ZAR = "currency.zar";
    public static final String CURRENCY_SMS_ZAR = "currency.sms.zar";
    public static final long CURRENCY_CZK_ID = 12;
    public static final String CURRENCY_CZK = "currency.czk";
    public static final String CURRENCY_SMS_CZK = "currency.sms.czk";
    public static final long CURRENCY_PLN_ID = 13;
    public static final String CURRENCY_PLN = "currency.pln";
    public static final String CURRENCY_SMS_PLN = "currency.sms.pln";
    
    public static final String ETRADER_LOCALE = "iw";
    public static final String TURKISH_LOCALE = "tr";
    public static final String SPANISH_LOCALE = "es";
    public static final String ARABIC_LOCALE = "ar";
    public static final String GERMAN_LOCALE = "de";
    public static final String LOCALE_DEFAULT = "en";
    public static final String RUSSIAN_LOCALE = "ru";
    public static final String ITALIAN_LOCALE = "it";
    public static final String FRENCH_LOCALE = "fr";
    public static final String CHINESE_LOCALE = "zh";
    public static final String CHINESE_LOCALE_BACLEND = "hk";
    
    // Offset constants
    public static final String GMT_2_OFFSET_DELTA = "+2/24-2/1440";
    public static final String GMT_3_OFFSET_DELTA = "+3/24-2/1440";
    public static final int GMT_2_TO_3_FROM_MONTH = 4;
    public static final int GMT_2_TO_3_TO_MONTH = 9;
    // constants for tax_job
    public static final String TAX_JOB_PERIOD1 = "1";
    public static final String TAX_JOB_PERIOD2 = "2";

    // current tax year
    public static final GregorianCalendar CURRENT_YEAR = new GregorianCalendar();
    public static final long TAX_JOB_YEAR_LONG = CURRENT_YEAR.get(GregorianCalendar.YEAR);
    public static final long NEXT_YEAR = TAX_JOB_YEAR_LONG + 1;
    public static final String TAX_JOB_YEAR = String.valueOf(CURRENT_YEAR.get(GregorianCalendar.YEAR));

    public static final String TAX_JOB_PERIOD1_START_PART = "0101";
    //public static final String TAX_JOB_PERIOD1_START = TAX_JOB_PERIOD1_START_PART+TAX_JOB_YEAR;

    public static final String TAX_JOB_PERIOD1_END_PART = "3006";
    //public static final String TAX_JOB_PERIOD1_END_PART = "0107";
    //public static final String TAX_JOB_PERIOD1_END = TAX_JOB_PERIOD1_END_PART+TAX_JOB_YEAR;

    public static final String TAX_JOB_PERIOD2_START_PART = "0101";
    //public static final String TAX_JOB_PERIOD2_START = TAX_JOB_PERIOD2_START_PART+TAX_JOB_YEAR;
    public static final String TAX_JOB_PERIOD2_END_PART = "3112";
    //public static final String TAX_JOB_PERIOD2_END = TAX_JOB_PERIOD2_END_PART+TAX_JOB_YEAR;

    public static final String TAX_JOB_PERIOD2_FROM = "0107";

    public static final String TAX_JOB_PERIOD1_END_PART_MONTH = "07";
    public static final String TAX_JOB_PERIOD1_START_PART_MONTH = "01";

    public static final int ZERO_TAX_BALANCE = 0;
    public static final int NO_EXEMPTION = 0;

    public static final int TAX_JOB_WRITER_ID = 29;

    public static final String DB_URL = "tax.db.url";
    public static final String DB_USER = "tax.db.user";
    public static final String DB_PASS = "tax.db.pass";
    public static final long[] COUNTRY_ID_REGULATED = new long[]{15,22,33,53,55,56,57,67,72,73,80,83,95,96,101,102,112,117,118,119,127,145,156,166,167,171,185,186,192,200,219};

    // product view flags
    public static final String HAS_DYNAMICS_FLAG = "HasDynamics";
    public static final String HAS_BUBBLES_FLAG = "HasBubbles";
    
    public static final String DEFAULT_BUBBLES_TOKEN = "123456789";

    public static final int MAX_SIZE_SENDER_NAME = 11;
    
    // market feed name
    public static final String FEED_NAME_OPTION_PLUS = "%Option+";
    public static final String FEED_NAME_DYNAMICS = "%Dynamicssss";

    public static final String OBJECT_SHARE_CHARTS_UPDATER = "chartsUpdater";
    public static final String OBJECT_SHARE_LEVELS_HISTORY_CACHE = "levelHistoryCache";
    public static final String OBJECT_SHARE_CHARTS_HISTORY_CACHE = "chartHistoryCache";
    
    public static final String TERMS_FILES_PATH_PREVIEW = "terms.files.path.preview";
    public static final String TERMS_FILES_PATH = "terms.files.path";    
    
    public static final String ACADEMY_FILES_PATH = "academy.files.path";
    
    /*
     * 	D3 alert mail should stay active as usual only for investments above 150$ (in any currency).
 	*	for every 10 D3 investment below 150$ per user (same opp'),
 	*	send 1 alert mail (for the first D3 inv' send an alert mail, for the next 9 do not)
 	*	also in mobile !
     */
    public static int d3Threshold = 15000;// dollars * 100
    public static final String DEV2_PARAMETER_NAME = "DEV2=";
    public static final String REQUEST_PARAMETER_ID = "id:";
    public static final String REQUEST_PARAMETER_AMOUNT = "amount";
    public static final String REQUEST_PARAMETER_PAGE_LEVEL = "pageLevel";
    public static final String REQUEST_PARAMETER_PAGE_ODDS_WIN = "pageOddsWin";
    public static final String REQUEST_PARAMETER_PAGE_ODDS_LOSE = "pageOddsLose";
    public static final String REQUEST_PARAMETER_CHOISE = "choice";
    public static final String REQUEST_PARAMETER_TIME_BEGIN = "timeBegin";
    
	// pixels parameters
	public static final String MARKETING_PARAMETER_USERID = "userIdParam";
	public static final String MARKETING_PARAMETER_MOBIE = "mobilePhoneParam";
	public static final String MARKETING_PARAMETER_CURRENCY_CODE = "currencyCodeParam";
	public static final String MARKETING_PARAMETER_TRANS_AMOUNT = "tranAmountParam";
	public static final String MARKETING_PARAMETER_TRANS_ID = "tranIdParam";
	public static final String MARKETING_PARAMETER_TRANS_AMOUNT_USD = "tranAmountUsdParam";
	public static final String MARKETING_PARAMETER_TRANS_AMOUNT_EUR = "tranAmountEurParam";
	public static final String MARKETING_PARAMETER_DYNAMIC = "dynamicParam";
	public static final String MARKETING_PARAMETER_SUB_AFF_CODE = "subAffParam";
	public static final String MARKETING_PARAMETER_CONTACTID = "contactIdParam";
	public static final String MARKETING_PARAMETER_DUID = "DUIDParam";
	public static final String MARKETING_PARAMETER_IP = "IPParam";
	public static final String MARKETING_PARAMETER_AFF_SUB1 = "affSub1Param";
	public static final String MARKETING_PARAMETER_AFF_SUB2 = "affSub2Param";
	public static final String MARKETING_PARAMETER_AFF_SUB3 = "affSub3Param";
	public static final String MARKETING_PARAMETER_EMAIL 	= "emailParam";
	
	public static final String BANNER_SLIDER_IMAGES_PATH = "bannes.slider.images.path";    
	public static final String ALLOWED_SLIDER_IMAGE_EXTENSIONS_REGEX = "^.*\\.(jpeg|png|jpg|gif)$";
	
    public static final long MARKET_ID_TEVA_NY = 594;
    public static final long MARKET_ID_TEVA_TA = 12;
    
    public static final String SESSION_SKIN = "sessionSkin";
    public static final String SESSION_COUNTRY_ID = "sessionCountryId";
    
    public static final long COUNTRY_ID_US = 220;
    public static final long COUNTRY_ID_IL = 1;
    public static final long COUNTRY_ID_IRAN = 99;
    public static final long COUNTRY_ID_ITALY = 102;
    public static final long COUNTRY_ID_NORTH_KOREA = 154;
    public static final long COUNTRY_ID_SOUTH_KOREA = 191;
    public static final long COUNTRY_ID_INDIA = 97;
    public static final long COUNTRY_ID_CHINA = 44;
    public static final long COUNTRY_ID_SPAIN = 192;
    public static final long COUNTRY_ID_SWEDEN = 200;
    
    public static final int ACCOUNTING_APPROVED_YES = 1;
    public static final int ACCOUNTING_APPROVED_NO = 0;

	public static final String NO_ID_NUM = "No-Id";
	
	public static final String TEMPLATE_WITHDRAW_CC_SUCCEED = "ccWithdrawalSucceed.html";
	public static final String TEMPLATE_WITHDRAW_BW_SUCCEED = "bankWireWithdrawalSucceed.html";
	public static final String TEMPLATE_WITHDRAW_PAYPAL_SUCCEED = "paypalWithdrawalSucceed.html";
	public static final String TEMPLATE_WITHDRAW_MONETA_SUCCEED = "monetaWithdrawalSucceed.html";
	public static final String TEMPLATE_WITHDRAW_WEBMONEY_SUCCEED = "webmoneyWithdrawalSucceed.html";
	public static final String TEMPLATE_WITHDRAW_BAROPAY_SUCCEED = "baroPayWithdrawalSucceed.html";
	
	//Doc request type
    public static final String RISK_DOC_REQ_ALL = "0";
    public static final String RISK_DOC_REQ_ID = "1";
    public static final String RISK_DOC_REQ_CC = "2";
    public static final String RISK_DOC_REQ_CONTACT_USER = "3";
    public static final String RISK_DOC_REQ_REGULATION = "4";

    public static final int BLACK_LIST_BIN = -1;
    
	public static final int CLEARING_PROVIDER_NOT_SELECTED = -1;
	
	public static final int FILE_TYPE_DEFAULT = 37;

	public static final String BIND_USER_MIGRATION = "userMigration";
}
