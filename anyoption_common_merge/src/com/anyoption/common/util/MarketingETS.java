package com.anyoption.common.util;

import java.io.IOException;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.jfree.util.Log;

import com.anyoption.common.managers.MarketingTrackingManager;
import com.anyoption.common.managers.SkinsManagerBase;
import com.anyoption.ets.beans.EtsCookieValue;
import com.anyoption.ets.beans.MarketingRequestedObj;
import com.anyoption.ets.beans.MarketingTrackingParam;
import com.google.gson.Gson;

public class MarketingETS extends MarketingTracker {

    private static final Logger log = Logger.getLogger(MarketingETS.class);
    public static final String ETS_COOKIE_NAME = "ets";
    public static final String ETS_APP_PARAM = "etsmid="; 
    private static final String ETS_COOKIE_ACTIVITY_NAME = "mla";

    private static EtsCookieValue parseEtsCookie(String value, String activity) {
        EtsCookieValue ets = new EtsCookieValue();
        try {
            String ts = value.substring(0, value.indexOf("|"));
            SimpleDateFormat sdf = new SimpleDateFormat("ddMMyyyyHHmmss");
            sdf.setTimeZone(TimeZone.getTimeZone("GMT"));
            ets.setCreateDateTime(sdf.parse(ts));

            ets.setLastActivity(Long.parseLong(activity));
            ets.setUId(value.substring(value.indexOf("|") + 1, value.length()));
        } catch (Exception e) {
            log.error("ParseEtsCookie: ", e);
        }
        return ets;
    }

    private static boolean isAfterCheckDatesOrNull(Date ts) {
        boolean result = true;

        if (ts != null) {
            Date date = Calendar.getInstance(TimeZone.getTimeZone("GMT")).getTime();
            long millsPerDay = 1000 * 60 * 60 * 24;
            long d2 = date.getTime() / millsPerDay;
            long d1 = ts.getTime() / millsPerDay;
            long daysBetween = d2 - d1;
            if (daysBetween >= ConstantsBase.MARKETING_TRACKING_DAYS_CHECK) {
                result = true;
            } else {
                result = false;
            }
        } else {
            result = true;
        }
        return result;
    }

    private static boolean isNeedReplaceETSCookie(String mId, String etsActivity) {
        boolean res = false;
        EtsCookieValue ets = parseEtsCookie(mId, etsActivity);
        if (isAfterCheckDatesOrNull(ets.getCreateDateTime())) {
            if (ets.getLastActivity() == ConstantsBase.MARKETING_TRACKING_CLICK) {
                res = true;
            }
        }
        return res;
    }

    private static String getEtsDefaultMidBE(long userId) {
        Date date = Calendar.getInstance(TimeZone.getTimeZone("GMT")).getTime();
        String dateForDisplay = new SimpleDateFormat("ddMMyyyyHHmmss").format(date);
        String mId = dateForDisplay + "|BE" + String.valueOf(userId);
        return mId;
    }

    public static void etsInsertUserBackend(String userEmail, String userMobilePhone, String userLandLinePhone, Long UserContactId, Long userId,
            long skinId) throws SQLException, IOException {

        try {
            String mId = null;
            mId = checkMarketingEtsMidInsertingUser("", userEmail, userMobilePhone, userLandLinePhone, UserContactId, userId);
            if (mId == null) {
                mId = getEtsDefaultMidBE(userId);
                etsOrganicClikcServletCall(mId, String.valueOf(SkinsManagerBase.getCombinationIdtBySkins(skinId)), "", "", "BE", "BE", false);
            }
            if (UserContactId != 0) {
                MarketingTrackingManager.updateContactMid(UserContactId, mId);
            }
            etsActivityServletCall(userId, ConstantsBase.MARKETING_TRACKING_FULL_REG, mId, "BE");
        } catch (Exception e) {
            log.error("Marketing ETS when inserting user from BE, in MarketingETS : ", e);
        }
    }
    

    protected static void etsOrganicClikcServletCall(String mId, String combinationId, String dynamicParam, String httpRefferer, String requestUrl,
            String ip, boolean isMobile) throws IOException {
        MarketingTrackingParam mtp = new MarketingTrackingParam();
        mtp.setmId(mId);
        mtp.setCombinationId(combinationId);
        mtp.setDyanmicParameter(dynamicParam);
        mtp.setHttpReferer(httpRefferer);
        mtp.setRequestUrl(requestUrl);
        mtp.setMarketingTrackingActivityId(ConstantsBase.MARKETING_TRACKING_CLICK);
        mtp.setIp(ip);
        mtp.setMobile(isMobile);

        Gson gson = new Gson();
        String json = gson.toJson(mtp);
        log.debug("Send ETS for organic Click : " + json);
        MarketingTrackerUtil.callEtsServlet(ConstantsBase.ETS_ORGANIC_CLICK_SERVLET, json);
    }

    public static void etsMarketingOrganic(HttpServletRequest request, HttpServletResponse response) {
        Cookie etsCookie = getMarketingTracker(request, ETS_COOKIE_NAME);
        Cookie etsActivityCookie = getMarketingTracker(request, ETS_COOKIE_ACTIVITY_NAME);
        try {
            boolean isNeedReplaceETSCookie = false;
            if (etsCookie != null && etsActivityCookie != null) {
                isNeedReplaceETSCookie = isNeedReplaceETSCookie(etsCookie.getValue(), etsActivityCookie.getValue());
            }
            if (etsCookie == null || etsActivityCookie == null || isNeedReplaceETSCookie) {
                HttpSession session = request.getSession();
                String mId = "ao" + session.getId();
                Date date = Calendar.getInstance(TimeZone.getTimeZone("GMT")).getTime();
                String dateForDisplay = new SimpleDateFormat("ddMMyyyyHHmmss").format(date);

                String cookieValue = dateForDisplay + "|" + mId;
                CommonUtil.addCookie(ETS_COOKIE_NAME, cookieValue, request, response, ETS_COOKIE_NAME);
                CommonUtil.addCookie(ETS_COOKIE_ACTIVITY_NAME, String.valueOf(ConstantsBase.MARKETING_TRACKING_CLICK), request, response,
                        ETS_COOKIE_ACTIVITY_NAME);

                String combId = (String) request.getParameter(ConstantsBase.COMBINATION_ID);
                if (null == combId) {
                    combId = Long.toString(SkinsManagerBase.getCombinationIdtBySkins(Utils.getSkinId(session, request)));
                }
                String dynamicParam = (String) request.getParameter(ConstantsBase.DYNAMIC_PARAM);
                String httpRefferer = (String) request.getHeader(ConstantsBase.HTTP_REFERE);
                String requestUrl = request.getRequestURL().toString();
                if (request.getQueryString() != null) {
                    httpRefferer = httpRefferer + "?" + request.getQueryString();
                }
                String ip = CommonUtil.getIPAddress(request);

                etsOrganicClikcServletCall(cookieValue, combId, dynamicParam, httpRefferer, requestUrl, ip, false);
            }

        } catch (Exception e) {
            log.error("Marketing Tracker etsMarketingOrganic Exc: ", e);
        }
    }

    public static void etsMarketingLoginUser(HttpServletRequest request, HttpServletResponse response, long userId, Long firstDepositId,
            String userMid, Long userCombinationId, String userDynamicParam, String userHttpRefer) {
        try {
            Cookie etsCookie = getMarketingTracker(request, ETS_COOKIE_NAME);
            Cookie etsActivity = getMarketingTracker(request, ETS_COOKIE_ACTIVITY_NAME);
            boolean isHaveFirstDepoits = false;

            if (firstDepositId > 0) {
                isHaveFirstDepoits = true;
            }

            if (etsCookie != null) {
                if (userMid != null) {
                    if (!etsCookie.getValue().contains(userMid)) {
                        CommonUtil.addCookie(ETS_COOKIE_NAME, userMid, request, response, ETS_COOKIE_NAME);
                    }
                } else {
                    String mId = "login" + userId;
                    Date date = Calendar.getInstance(TimeZone.getTimeZone("GMT")).getTime();
                    String dateForDisplay = new SimpleDateFormat("ddMMyyyyHHmmss").format(date);

                    String cookieValue = dateForDisplay + "|" + mId;

                    CommonUtil.addCookie(ETS_COOKIE_NAME, cookieValue, request, response, ETS_COOKIE_NAME);
                    //Add Click!!!
                    String ip = CommonUtil.getIPAddress(request);
                    etsOrganicClikcServletCall(cookieValue, userCombinationId.toString(), userDynamicParam, userHttpRefer, "When user login", ip, false);
                   
                    MarketingTrackingManager.updateUserMid(userId, cookieValue);                    
                }
            }

            if (etsActivity != null) {
                if (isHaveFirstDepoits && (Long.valueOf(etsActivity.getValue()) != ConstantsBase.MARKETING_TRACKING_FIRST_DEPOSIT)) {
                    CommonUtil.addCookie(ETS_COOKIE_ACTIVITY_NAME, String.valueOf(ConstantsBase.MARKETING_TRACKING_FIRST_DEPOSIT), request, response,
                            ETS_COOKIE_ACTIVITY_NAME);
                } else if (Long.valueOf(etsActivity.getValue()) != ConstantsBase.MARKETING_TRACKING_FULL_REG) {
                    CommonUtil.addCookie(ETS_COOKIE_ACTIVITY_NAME, String.valueOf(ConstantsBase.MARKETING_TRACKING_FULL_REG), request, response,
                            ETS_COOKIE_ACTIVITY_NAME);
                }
            }

        } catch (Exception e) {
            log.error("Marketing ETS user login Exc: ", e);
        }
    }

    public static void etsUsersFirstDeposit() {
        try {
            ArrayList<String> users = MarketingTrackingManager.getEtsUsersFirstDeposit();
            if (users.size() > 0) {
                String usersParams = "";
                for (String s : users) {
                    usersParams += s.toString() + ";";
                }
                log.debug("ETS FD try to call servlet");
                String resp = MarketingTrackerUtil.callEtsServlet(ConstantsBase.ETS_FIRST_DEPOSIT_SERVLET, usersParams);
                if (resp.contains(Integer.toString(HttpServletResponse.SC_OK))) {
                    log.debug("ETS FD update status");
                    MarketingTrackingManager.updateEtsUserFDState(users);
                } else {
                    log.error("ETS FD Servlet have Exc " + resp);
                }
            } else {
                log.debug("ETS FD no USERS with FD");
            }
        } catch (Exception e) {
            log.error("When send ETS FD", e);
        }
    }
    
    public static String checkMarketingEtsMidInsertingUser(String etsMid, String email, String mobilePhone, String landLinePhone, long contactId,
            long userId) {
        String newEtsMid = null;
        try {
            String checkedEtsMid = MarketingTrackingManager.getEtsMidByContactDetail(email, mobilePhone, landLinePhone, contactId, userId);

            if (checkedEtsMid != null) {
                if (etsMid != null) {
                    if (!checkedEtsMid.equals(etsMid)) {
                        newEtsMid = checkedEtsMid;
                    }
                } else {
                    newEtsMid = checkedEtsMid;
                }
            }
        } catch (Exception e) {
            log.error("Marketing ETS for email: " + email + " and mobilePhone " + mobilePhone + "and current " + "etsMid part: " + etsMid + " Exc: ",
                    e);
        }
        return newEtsMid;
    }
    
    public static String checkMarketingEtsMidInsertingContact(String etsMid, String email, String phone) {
        String newEtsMid = null;
        try {
            String checkedEtsMid = MarketingTrackingManager.getEtsMidByContactDetail(email, phone);

            if (checkedEtsMid != null) {
                if (etsMid != null) {
                    if (!checkedEtsMid.equals(etsMid)) {
                        newEtsMid = checkedEtsMid;
                    }
                } else {
                    newEtsMid = checkedEtsMid;
                }
            }
        } catch (Exception e) {
            log.error("Marketing ETS for email: " + email + " and phone " + phone + "and current " + "etsMid part: " + etsMid + " Exc: ", e);
        }
        return newEtsMid;
    }
    
    protected static void etsActivityServletCall(long objId, long marketingActivity, String mId, String ip) throws IOException, SQLException {
        MarketingRequestedObj mro = new MarketingRequestedObj();
        mro.setmId(mId);
        mro.setObjectId(objId);
        mro.setRequestedType(marketingActivity);
        mro.setIp(ip);

        Gson gson = new Gson();
        String json = gson.toJson(mro);
        String etsRes = MarketingTrackerUtil.callEtsServlet(ConstantsBase.ETS_MARKETING_PARAMETARS_SERVLET, json);
        Log.debug("ETS resault is : " + etsRes);
        if (marketingActivity == ConstantsBase.MARKETING_TRACKING_FULL_REG) {
            MarketingTrackingManager.updateUserMid(objId, mId);
        } else {
            MarketingTrackingManager.updateContactMid(objId, mId);
        }
    }
    
    public static void etsMarketingActivity(HttpServletRequest request, HttpServletResponse response, long objId, long marketingActivity,
            String contactEmail, String contactMobilePhone, String userEmail, String userMobilePhone, String userLandLinePhone, Long UserContactId,
            String ip)
            throws SQLException {
        try {
            Cookie etsCookie = getMarketingTracker(request, ETS_COOKIE_NAME);
            Cookie etsActivity = getMarketingTracker(request, ETS_COOKIE_ACTIVITY_NAME);
            long activityType = marketingActivity;

            if (etsActivity != null) {
                activityType = Long.valueOf(etsActivity.getValue());
            }

            if (activityType != ConstantsBase.MARKETING_TRACKING_FIRST_DEPOSIT) {
                if (etsCookie != null) {
                    String mId = etsCookie.getValue();

                    String checkExistMid = null;
                    if (marketingActivity == ConstantsBase.MARKETING_TRACKING_FULL_REG) {                        
                        checkExistMid = checkMarketingEtsMidInsertingUser(mId, userEmail, userMobilePhone, userLandLinePhone, UserContactId, objId);
                    } else {
                        checkExistMid = checkMarketingEtsMidInsertingContact(mId, contactEmail, contactMobilePhone);                        
                    }
                    if (checkExistMid != null) {
                        log.debug("ETS Found exist cookie replace:" + mId + " with " + checkExistMid);
                        mId = checkExistMid;
                        CommonUtil.addCookie(ETS_COOKIE_NAME, checkExistMid, request, response, ETS_COOKIE_NAME);

                    }
                    CommonUtil.addCookie(ETS_COOKIE_ACTIVITY_NAME, String.valueOf(marketingActivity), request, response, ETS_COOKIE_ACTIVITY_NAME);
                    //Call Servlet
                    etsActivityServletCall(objId, marketingActivity, mId, ip);
                } else {
                    log.debug("Marketing ETS COOKIE are null, create deafult!!! ");
                    etsMarketingOrganic(request, response);
                }
            }

        } catch (Exception e) {
            log.error("Marketing ETS when insert Activity Exc: ", e);
        }
    }
}
