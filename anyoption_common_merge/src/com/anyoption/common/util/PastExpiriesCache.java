package com.anyoption.common.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TimeZone;
import java.util.TreeSet;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.Opportunity;
import com.anyoption.common.managers.OpportunitiesManagerBase;

/**
 * @author kirilim
 */
public class PastExpiriesCache {

	private static final Logger log = Logger.getLogger(PastExpiriesCache.class);
	private Map<Long, TreeSet<Opportunity>> expiries;
	private Map<Long, TreeSet<Opportunity>> endOfDayExpiries;
	private Map<Long, Integer> endOfDayExpiriesLockedMinutes;
	private List<Integer> endOfDayScheduledList = new ArrayList<>(Arrays.asList(Opportunity.SCHEDULED_HOURLY, Opportunity.SCHEDULED_DAYLY,
																				Opportunity.SCHEDULED_WEEKLY, Opportunity.SCHEDULED_MONTHLY,
																				Opportunity.SCHEDULED_QUARTERLY));
	private Map<Long, TreeSet<Opportunity>> endOfWeekExpiries;
	private Map<Long, TreeSet<Opportunity>> endOfMonthExpiries;
	private static final int MONTH_PERIOD_SPLITTER_DAY = 15;
	private List<Integer> endOfWeekMonthScheduledList = new ArrayList<>(Arrays.asList(	Opportunity.SCHEDULED_DAYLY,
																						Opportunity.SCHEDULED_WEEKLY,
																						Opportunity.SCHEDULED_MONTHLY,
																						Opportunity.SCHEDULED_QUARTERLY));
	private Map<Long, TreeSet<Opportunity>> endOfQuarterExpiries;
	private List<Integer> endOfQuarterScheduledList = new ArrayList<>(Arrays.asList(Opportunity.SCHEDULED_WEEKLY,
																					Opportunity.SCHEDULED_MONTHLY,
																					Opportunity.SCHEDULED_QUARTERLY));
	private PastExpiriesCacheThread cleaner;
	private long lastLoadedTime;
	private final int PAST_EXPIRED_OPPORTUNITIES_COUNT = 3;
	private final long CACHE_SLEEP_INTERVAL = 1000 * 60;

	public PastExpiriesCache() {
		cleaner = new PastExpiriesCacheThread();
		cleaner.start();
	}

	public Set<Opportunity> getPastExpiries(long marketId) {
		return (expiries.get(marketId) != null) ? expiries.get(marketId) : new TreeSet<Opportunity>();
	}

	public Set<Opportunity> getEndOfDayExpiries(long marketId) {
		return (endOfDayExpiries.get(marketId) != null) ? endOfDayExpiries.get(marketId) : new TreeSet<>();
	}

	public Set<Opportunity> getEndOfWeekExpiries(long marketId) {
		return (endOfWeekExpiries.get(marketId) != null) ? endOfWeekExpiries.get(marketId) : new TreeSet<>();
	}

	public Set<Opportunity> getEndOfMonthExpiries(long marketId) {
		return (endOfMonthExpiries.get(marketId) != null) ? endOfMonthExpiries.get(marketId) : new TreeSet<>();
	}

	/**
	 * Returns end of quarter expiries for the given market id after the provided date. The {@code timeFirstInvest} parameter should be the
	 * time first invest of the quarterly opportunity whose expiries are needed.
	 * 
	 * @param marketId id of the market
	 * @param timeFirstInvest date after which expiries in the set will be returned
	 * @return the set containing end of quarter expiries
	 */
	public Set<Opportunity> getEndOfQuarterExpiries(long marketId, Date timeFirstInvest) {
		Calendar cal = cleaner.createCalendar();
		cal.add(Calendar.MONTH, -2);
		if (cal.getTime().getTime() > timeFirstInvest.getTime()) {
			return (endOfQuarterExpiries.get(marketId) != null) ? endOfQuarterExpiries.get(marketId) : new TreeSet<>();
		} else {
			TreeSet<Opportunity> oppsCache = endOfQuarterExpiries.get(marketId);
			if (oppsCache != null) {
				Iterator<Opportunity> iterator = oppsCache.iterator();
				Opportunity cachedOpp;
				while (iterator.hasNext()) {
					cal.setTime(timeFirstInvest);
					cal.add(Calendar.MONTH, -2);
					cal.set(Calendar.DAY_OF_MONTH, 1);
					cal.set(Calendar.HOUR_OF_DAY, 0);
					cal.set(Calendar.MINUTE, 0);
					cal.set(Calendar.SECOND, 0);
					cal.set(Calendar.MILLISECOND, 0);
					cachedOpp = iterator.next();
					if (cachedOpp.getTimeEstClosing().getTime() < cal.getTime().getTime()) {
						continue;
					} else {
						return oppsCache.subSet(cachedOpp, true, oppsCache.last(), true);
					}
				}
			}
		}
		// at this point there aren't expiries that satisfy the criteria
		return new TreeSet<>();
	}

	public void stopPastExpiriesCache() {
		cleaner.stopPastExpiriesCache();
	}

	private class PastExpiriesCacheThread extends Thread {

		private boolean running;

		public void run() {
			Thread.currentThread().setName("PastExpiriesCacheThread");
			log.info("PastExpiriesCacheThread starting");
			initCaches();
			StringBuilder sb = new StringBuilder();
			StringBuilder endOfDayExpiriesBuilder = new StringBuilder();
			StringBuilder endOfWeekExpiriesBuilder = new StringBuilder();
			StringBuilder endOfMonthExpiriesBuilder = new StringBuilder();
			StringBuilder endOfQuarterExpiriesBuilder = new StringBuilder();
			running = true;
			while (running) {
				log.debug("Updating PastExpiriesCache");
				long now = System.currentTimeMillis();
				Iterator<Opportunity> oppsIterator = OpportunitiesManagerBase.getPastExpiredOpportunities(lastLoadedTime).iterator();
				lastLoadedTime = now;
				sb.append("Adding opportunity ids: [");
				endOfDayExpiriesBuilder.append("Adding to end of day expiries ids: [");
				endOfWeekExpiriesBuilder.append("Adding to end of week expiries ids: [");
				endOfMonthExpiriesBuilder.append("Adding to end of month expiries ids: [");
				endOfQuarterExpiriesBuilder.append("Adding to end of quarter expiries ids: [");
				while (oppsIterator.hasNext()) {
					Opportunity opp = oppsIterator.next();
					TreeSet<Opportunity> cachedOpps = expiries.get(opp.getMarketId());
					if (cachedOpps == null) {
						cachedOpps = new TreeSet<Opportunity>(new PastExpiriesCacheComparator());
						expiries.put(opp.getMarketId(), cachedOpps);
					}
					cachedOpps.add(opp);
					sb.append(opp.getId());
					if (oppsIterator.hasNext()) {
						sb.append(", ");
					}
					while (cachedOpps.size() > PAST_EXPIRED_OPPORTUNITIES_COUNT) {
						cachedOpps.pollLast();
					}
					if (opp.getTypeId() == Opportunity.TYPE_REGULAR) {
						if (addOppAndReset(opp, endOfDayExpiries, Opportunity.SCHEDULED_DAYLY, endOfDayScheduledList)) {
							appendOppToLog(opp, endOfDayExpiriesBuilder);
						}
						if (addOppAndReset(opp, endOfWeekExpiries, Opportunity.SCHEDULED_WEEKLY, endOfWeekMonthScheduledList)) {
							appendOppToLog(opp, endOfWeekExpiriesBuilder);
						}
						if (addOppAndReset(opp, endOfMonthExpiries, Opportunity.SCHEDULED_MONTHLY, endOfWeekMonthScheduledList)) {
							appendOppToLog(opp, endOfMonthExpiriesBuilder);
						}
						if (addOppAndReset(opp, endOfQuarterExpiries, Opportunity.SCHEDULED_QUARTERLY, endOfQuarterScheduledList)) {
							appendOppToLog(opp, endOfQuarterExpiriesBuilder);
						}
					}
				}
				sb.append("]");
				log.info(sb.toString());
				sb.setLength(0);
				log.info(endOfDayExpiriesBuilder.append("]"));
				endOfDayExpiriesBuilder.setLength(0);
				log.info(endOfWeekExpiriesBuilder.append("]"));
				endOfWeekExpiriesBuilder.setLength(0);
				log.info(endOfMonthExpiriesBuilder.append("]"));
				endOfMonthExpiriesBuilder.setLength(0);
				log.info(endOfQuarterExpiriesBuilder.append("]"));
				endOfQuarterExpiriesBuilder.setLength(0);
				putThreadToSleep();
			}
			log.info("PastExpiriesCacheThread done");
		}

		private void initCaches() {
			long now = System.currentTimeMillis();
			initPastExpiriesCache();
			initProlongedGraphCaches();
			lastLoadedTime = now;
			log.info("PastExpiriesCacheThread has been initialized");
		}

		private void initPastExpiriesCache() {
			expiries = new HashMap<Long, TreeSet<Opportunity>>();
			List<Opportunity> opps = OpportunitiesManagerBase.getPastExpiredOppsForCacheInit(PAST_EXPIRED_OPPORTUNITIES_COUNT);
			for (Opportunity opp : opps) {
				TreeSet<Opportunity> oppsCache = expiries.get(opp.getMarketId());
				if (oppsCache == null) {
					oppsCache = new TreeSet<Opportunity>(new PastExpiriesCacheComparator());
					expiries.put(opp.getMarketId(), oppsCache);
				}
				oppsCache.add(opp);
			}
		}

		private void initProlongedGraphCaches() {
			initWeekMonthGraphCache();
			initDayGraphCache();
			initQuarterGraphCache();
		}

		/**
		 * Initializes the {@code endOfDayExpiries} cache. The cache contains hourly opportunities from today. The first opportunity in the
		 * cache is special - it is the daily opportunity from the previous day(note that the previous day may not be yesterday). All other
		 * opportunities in the cache have estimate closing time after the first invest time of the daily opportunity. For every market a
		 * minute lock is used - {@link PastExpiriesCache#endOfDayExpiriesLockedMinutes}. Only opportunities from these minutes of every
		 * hour are included in the cache(i.e. only from 00s or 15s or 30s etc.). When initializing the minute lock, a lock on the smallest
		 * opportunity at this hour is tried, this is done in order to lock the opportunity at 00 minutes, if possible.
		 * {@link PastExpiriesCache#endOfDayScheduledList} represents the opportunity scheduled types that are included in the cache.
		 * 
		 * @see PastExpiriesCache#endOfDayExpiries
		 * @see PastExpiriesCache#endOfDayExpiriesLockedMinutes
		 * @see PastExpiriesCache#endOfDayScheduledList
		 */
		private void initDayGraphCache() {
			// remember that the last expiry from the monthly and weekly graph should be the first in the daily graph(it's the asset's
			// expiry of the day before)
			endOfDayExpiries = new HashMap<>();
			endOfDayExpiriesLockedMinutes = new HashMap<>();
			Calendar cal = createCalendar();
			cal.set(Calendar.HOUR_OF_DAY, 0);
			cal.set(Calendar.MINUTE, 0);
			cal.set(Calendar.SECOND, 0);
			cal.set(Calendar.MILLISECOND, 0);
			List<Opportunity> oppsForDay = OpportunitiesManagerBase.initPastExpiredOppsForProlongedGraph(	endOfDayScheduledList,
																											cal.getTime(), true);
			TreeSet<Opportunity> oppsInHour = new TreeSet<>(new ProlongedGraphCacheComparator());
			int currentHour = 0;
			long marketTaken = 0l;
			Map<Long, TreeSet<Opportunity>> cacheMap = (endOfWeekExpiries != null) ? endOfWeekExpiries : endOfMonthExpiries;
			if (cacheMap == null) {
				initWeekMonthGraphCache();
				cacheMap = (endOfWeekExpiries != null) ? endOfWeekExpiries : endOfMonthExpiries;
				if (cacheMap == null) {
					log.warn("Could not initialize end of week/month expiries");
					return;
				}
			}
			if (!oppsForDay.isEmpty()) {
				cal.setTime(oppsForDay.get(0).getTimeEstClosing());
				currentHour = cal.get(Calendar.HOUR_OF_DAY);
				marketTaken = oppsForDay.get(0).getMarketId();
			}
			long lastDayExpiryTime = 0l;
			boolean currentSameAsFirstOppDay = false;
			StringBuilder endOfDayBuilder = new StringBuilder("Initializing end of day expiries with ids: [");
			Iterator<Opportunity> oppsForDayIterator = oppsForDay.iterator();
			while (oppsForDayIterator.hasNext()) {
				Opportunity opp = oppsForDayIterator.next();
				// here only one from the hourly opps should be taken (00s or 15s or 30s etc.)
				cal.setTime(opp.getTimeEstClosing());
				// determine which opp should go in the cache
				if (cal.get(Calendar.HOUR_OF_DAY) != currentHour || opp.getMarketId() != marketTaken || !oppsForDayIterator.hasNext()) {
					if (lastDayExpiryTime == 0 && cacheMap.get(marketTaken) != null && !cacheMap.get(marketTaken).isEmpty()) {
						Opportunity lastDayExpiry = cacheMap.get(marketTaken).last();
						Calendar lastDayExpiryCal = createCalendar();
						lastDayExpiryCal.setTime(new Date());
						int currentDay = lastDayExpiryCal.get(Calendar.DAY_OF_YEAR);
						lastDayExpiryCal.setTime(lastDayExpiry.getTimeFirstInvest());
						int lastDayExpiryTFIDay = lastDayExpiryCal.get(Calendar.DAY_OF_YEAR);
						lastDayExpiryCal.add(Calendar.DAY_OF_YEAR, currentDay - lastDayExpiryTFIDay);
						lastDayExpiryTime = lastDayExpiryCal.getTime().getTime();
						currentSameAsFirstOppDay = currentDay == lastDayExpiryTFIDay;
						if (addOppToProlongedGraphCache(lastDayExpiry, endOfDayExpiries, true)) {
							appendOppToLog(lastDayExpiry, endOfDayBuilder);
						}
					}
					// if not present the very last opp, which is from the same hour, is missed in the cache
					boolean lastOppFromHour = false;
					if (cal.get(Calendar.HOUR_OF_DAY) == currentHour && opp.getMarketId() == marketTaken && !oppsForDayIterator.hasNext()) {
						oppsInHour.add(opp);
						lastOppFromHour = true;
						// mark the situation here maybe?
					}

					Opportunity chosenOpp = null;
					Iterator<Opportunity> iterator = oppsInHour.iterator();
					Calendar hourOppCal = createCalendar();
					Integer minuteLock = endOfDayExpiriesLockedMinutes.get(marketTaken);
					while (iterator.hasNext()) {
						Opportunity hourOpp = iterator.next();
						// we don't add opps that are before time first invest or from the day of the last daily expiry
						if (lastDayExpiryTime >= hourOpp.getTimeEstClosing().getTime() || currentSameAsFirstOppDay) {
							break;
						}
						if (chosenOpp == null) {
							chosenOpp = hourOpp;
						}
						if (minuteLock != null) {
							hourOppCal.setTime(hourOpp.getTimeEstClosing());
							if (minuteLock == hourOppCal.get(Calendar.MINUTE)) {
								chosenOpp = hourOpp;
								break;
							}
						} else { // because this is the smallest opp
							hourOppCal.setTime(chosenOpp.getTimeEstClosing());
							minuteLock = hourOppCal.get(Calendar.MINUTE);
							endOfDayExpiriesLockedMinutes.put(marketTaken, minuteLock);
							break;
						}
					}
					currentHour = cal.get(Calendar.HOUR_OF_DAY);
					oppsInHour.clear();
					oppsInHour.add(opp);
					if (addOppToProlongedGraphCache(chosenOpp, endOfDayExpiries, true)) {
						appendOppToLog(chosenOpp, endOfDayBuilder);
					}
					// if not present the very last opp, which is from new hour or market, is missed in the cache
					if (!lastOppFromHour && !oppsForDayIterator.hasNext()) {
						if (opp.getTimeEstClosing().getTime() > lastDayExpiryTime && !currentSameAsFirstOppDay) {
							if (addOppToProlongedGraphCache(opp, endOfDayExpiries, true)) {
								endOfDayBuilder.append(", "); // to not skip the last comma, because the iterator has no more elements
								appendOppToLog(opp, endOfDayBuilder);
							}
						}
					}
					if (opp.getMarketId() != marketTaken) {
						marketTaken = opp.getMarketId();
						lastDayExpiryTime = 0;
						currentSameAsFirstOppDay = false;
					}
				} else {
					oppsInHour.add(opp);
				}
			}
			log.info(endOfDayBuilder.append("]"));
		}

		/**
		 * Initializes {@code endOfWeekExpiries} and {@code endOfMonthExpiries} caches. The {@code endOfWeekExpiries} cache contains
		 * opportunities from the current and past weeks. The {@code endOfMonthExpiries} cache contains opportunities from this month. If
		 * the current day is before 15th(including) the {@code endOfMonthExpiries} cache will contain also opportunities from 16th till the
		 * end of previous month. {@link PastExpiriesCache#endOfWeekMonthScheduledList} represents the opportunity scheduled types that are
		 * included in the caches.
		 * 
		 * @see PastExpiriesCache#endOfWeekExpiries
		 * @see PastExpiriesCache#endOfMonthExpiries
		 * @see PastExpiriesCache#endOfWeekMonthScheduledList
		 */
		private void initWeekMonthGraphCache() {
			endOfWeekExpiries = new HashMap<>();
			endOfMonthExpiries = new HashMap<>();
			Calendar cal = createCalendar();
			if (cal.get(Calendar.DAY_OF_MONTH) > MONTH_PERIOD_SPLITTER_DAY) {
				cal.set(Calendar.DAY_OF_MONTH, 1);
			} else {
				cal.add(Calendar.MONTH, -1);
				cal.set(Calendar.DAY_OF_MONTH, MONTH_PERIOD_SPLITTER_DAY);
			}
			cal.set(Calendar.HOUR_OF_DAY, 0);
			cal.set(Calendar.MINUTE, 0);
			cal.set(Calendar.SECOND, 0);
			cal.set(Calendar.MILLISECOND, 0);
			List<Opportunity> oppsForMonthWeek = OpportunitiesManagerBase.initPastExpiredOppsForProlongedGraph(	endOfWeekMonthScheduledList,
																												cal.getTime(), false);
			cal.setTime(new Date());
			cal.add(Calendar.WEEK_OF_YEAR, -1);
			cal.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);
			cal.set(Calendar.HOUR_OF_DAY, 0);
			Date previousWeekDate = cal.getTime();
			StringBuilder endOfMonthExpiriesBuilder = new StringBuilder("Initializing end of month expiries with ids: [");
			StringBuilder endOfWeekExpiriesBuilder = new StringBuilder("Initializing end of week expiries with ids: [");
			Iterator<Opportunity> iterator = oppsForMonthWeek.iterator();
			while (iterator.hasNext()) {
				Opportunity opp = iterator.next();
				if (addOppToProlongedGraphCache(opp, endOfMonthExpiries, true)) {
					appendOppToLog(opp, endOfMonthExpiriesBuilder);
				}
				// adding expiries for weekly graph only if they are from previous or current week
				if (opp.getTimeEstClosing().after(previousWeekDate)) {
					if (addOppToProlongedGraphCache(opp, endOfWeekExpiries, true)) {
						appendOppToLog(opp, endOfWeekExpiriesBuilder);
					}
				}
			}
			log.info(endOfMonthExpiriesBuilder.append("]"));
			log.info(endOfWeekExpiriesBuilder.append("]"));
		}

		/**
		 * Initializes {@code endOfQuarterExpiries} cache. The cache contains weekly opportunities from two to three months back, starting from the 1st {@link Calendar#DAY_OF_MONTH}.
		 * {@link PastExpiriesCache#endOfQuarterScheduledList} represents the opportunity scheduled types that are included in the cache.
		 * 
		 * @see PastExpiriesCache#endOfQuarterExpiries
		 * @see PastExpiriesCache#endOfQuarterScheduledList
		 */
		private void initQuarterGraphCache() {
			endOfQuarterExpiries = new HashMap<>();
			Calendar cal = createCalendar();
			int month = cal.get(Calendar.MONTH) + 1; // because JAN is 0 in the calendar
			if ((month % 3) == 2) { // means we are at the second month of the year quarter
				// load 2 months back from db
				cal.add(Calendar.MONTH, -2);
			} else {
				// load 3 months back from db
				cal.add(Calendar.MONTH, -3);
			}
			cal.set(Calendar.DAY_OF_MONTH, 1);
			cal.set(Calendar.HOUR_OF_DAY, 0);
			cal.set(Calendar.MINUTE, 0);
			cal.set(Calendar.SECOND, 0);
			cal.set(Calendar.MILLISECOND, 0);
			List<Opportunity> oppsForQuarter = OpportunitiesManagerBase.initPastExpiredOppsForProlongedGraph(endOfQuarterScheduledList, cal.getTime(), false);
			StringBuilder endOfQuarterExpiriesBuilder = new StringBuilder("Initializing end of quarter expiries with ids: [");
			Iterator<Opportunity> iterator = oppsForQuarter.iterator();
			while (iterator.hasNext()) {
				Opportunity opp = iterator.next();
				if (addOppToProlongedGraphCache(opp, endOfQuarterExpiries, true)) {
					appendOppToLog(opp, endOfQuarterExpiriesBuilder);
				}
			}
			log.info(endOfQuarterExpiriesBuilder.append("]"));
		}

		/**
		 * Adds the specified opportunity to the cache if it's not already present. If the cache already contains the opportunity, the call
		 * leaves the cache unchanged and returns {@code false}. If the call is made not during initialization and there is no opportunity
		 * cache for this market, the cache for the market is created containing the expiry from the previous day.
		 * 
		 * @param opp the opportunity to be added
		 * @param cache the cache to which the opportunity to be added
		 * @param init indicates whether or not the call is made during initialization
		 * @return {@code true} if the cache did not already contain the specified opportunity, {@code false} otherwise
		 */
		private boolean addOppToProlongedGraphCache(Opportunity opp, Map<Long, TreeSet<Opportunity>> cache, boolean init) {
			if (opp == null) {
				return false;
			}
			TreeSet<Opportunity> oppsCache = cache.get(opp.getMarketId());
			if (oppsCache == null) {
				oppsCache = new TreeSet<>(new ProlongedGraphCacheComparator());
				cache.put(opp.getMarketId(), oppsCache);
				if (!init) {
					Map<Long, TreeSet<Opportunity>> cacheMap = (endOfWeekExpiries != null) ? endOfWeekExpiries : endOfMonthExpiries;
					if (cacheMap != null && cacheMap.get(opp.getMarketId()) != null && !cacheMap.get(opp.getMarketId()).isEmpty()) {
						oppsCache.add(cacheMap.get(opp.getMarketId()).last());
					}
				}
			}
			return oppsCache.add(opp);
		}

		/**
		 * Adds the specified opportunity to the cache if it's not already present. If the opportunity has scheduled type greater than or
		 * equal to the {@code cacheScheduledType} the cache will clear opportunities that are not required. For the
		 * {@link PastExpiriesCache#endOfMonthExpiries} cache a special case for clearing is triggered.
		 * 
		 * @param opp the opportunity to be added
		 * @param cache the cache to which the opportunity to be added
		 * @param cacheScheduledType the scheduled type of the graph for this cache(i.e. for daily {@link Opportunity#SCHEDULED_DAYLY})
		 * @param cacheScheduledList list of all possible scheduled types that can be contained in the specific cache
		 * @return {@code true} if the cache did not already contain the specified opportunity, {@code false} otherwise
		 * 
		 * @see PastExpiriesCache#endOfDayExpiries
		 * @see PastExpiriesCache#endOfWeekExpiries
		 * @see PastExpiriesCache#endOfMonthExpiries
		 * @see PastExpiriesCache#endOfDayScheduledList
		 * @see PastExpiriesCache#endOfWeekMonthScheduledList
		 */
		private boolean addOppAndReset(Opportunity opp, Map<Long, TreeSet<Opportunity>> cache, int cacheScheduledType,
									List<Integer> cacheScheduledList) {
			if (!cacheScheduledList.contains(opp.getScheduled())) {
				return false;
			}
			if (opp.getScheduled() >= cacheScheduledType) { // this means that we need cache refresh; for monthly graph cache we need the
															 // quarterly opps
				// clear specific cache, this means wipe the daily, leave expiries from past week for weekly and from 15th for monthly
				clearSpecificCache(opp, cache, cacheScheduledType, true);
				// add the last opp
				return addOppToProlongedGraphCache(opp, cache, false);
			}
			if (cacheScheduledType == Opportunity.SCHEDULED_MONTHLY) { // this is needed because right now the monthly opps are last, we
																		 // don't have quarterly, after quarterly are implemented, we'll
																		 // need this changed for them
				clearMonthGraphCache(opp, cache, false);
				// add the last opp
				return addOppToProlongedGraphCache(opp, cache, false);
			}
			// TODO right now the opp.getScheduled() >= cacheScheduledType check doesn't work right for the quarterlies, we rely on this
			// check for clear
			if (cacheScheduledType == Opportunity.SCHEDULED_QUARTERLY) { // this is needed because the quarterly opps are last
				clearQuarterGraphCache(opp, cache);
				// add the last opp
				return addOppToProlongedGraphCache(opp, cache, false);
			}
			// if it's not time for clearing, just add the opp to the cache
			TreeSet<Opportunity> oppsCache = cache.get(opp.getMarketId());
			long firstOppFirstInvestNextDay = 0;
			boolean currentSameAsFirstOppDay = false;
			if (oppsCache != null && !oppsCache.isEmpty()) {
				Calendar cal = createCalendar();
				int currentDay = cal.get(Calendar.DAY_OF_YEAR);
				cal.setTime(oppsCache.first().getTimeFirstInvest());
				int firstOppTFIDay = cal.get(Calendar.DAY_OF_YEAR);
				cal.add(Calendar.DAY_OF_YEAR, currentDay - firstOppTFIDay);
				firstOppFirstInvestNextDay = cal.getTime().getTime();
				currentSameAsFirstOppDay = currentDay == firstOppTFIDay;
			}
			if (opp.getTimeEstClosing().getTime() > firstOppFirstInvestNextDay && !currentSameAsFirstOppDay) {
				// for daily we need the minute lock, we don't add all opps
				if (cacheScheduledType == Opportunity.SCHEDULED_DAYLY) {
					Calendar cal = createCalendar();
					cal.setTime(opp.getTimeEstClosing());
					Integer minuteLock = endOfDayExpiriesLockedMinutes.get(opp.getMarketId());
					if (minuteLock == null) {
						minuteLock = cal.get(Calendar.MINUTE);
						endOfDayExpiriesLockedMinutes.put(opp.getMarketId(), minuteLock);
					}
					if (cal.get(Calendar.MINUTE) == minuteLock) {
						return addOppToProlongedGraphCache(opp, cache, false);
					}
				} else {
					return addOppToProlongedGraphCache(opp, cache, false);
				}
			}
			return false;
		}

		/**
		 * Removes the unnecessary opportunities from the specified cache.
		 * 
		 * @param opp the opportunity that will be added after the cache clear
		 * @param cache the cache to be cleared
		 * @param cacheScheduledType the scheduled type of the graph for this cache(i.e. for daily {@link Opportunity#SCHEDULED_DAYLY})
		 * @param periodEnd {@code true} if the clear is triggered because of an end of period opportunity(end of a day for daily, end of a
		 *            week for weekly, etc.), {@code false} - special case for clearing
		 */
		private void clearSpecificCache(Opportunity opp, Map<Long, TreeSet<Opportunity>> cache, int cacheScheduledType, boolean periodEnd) {
			switch (cacheScheduledType) {
			case Opportunity.SCHEDULED_DAYLY:
				clearDayGraphCache(opp, cache);
				break;

			case Opportunity.SCHEDULED_WEEKLY:
				clearWeekGraphCache(opp, cache);
				break;

			case Opportunity.SCHEDULED_MONTHLY:
				clearMonthGraphCache(opp, cache, periodEnd);
				break;

			case Opportunity.SCHEDULED_QUARTERLY:
				clearQuarterGraphCache(opp, cache);
				break;

			default:
				break;
			}
		}

		/**
		 * Removes all opportunities from the specified cache for this opportunity's market.
		 * 
		 * @param opp the opportunity that will be added after the cache clear
		 * @param cache the cache to be cleared
		 */
		private void clearDayGraphCache(Opportunity opp, Map<Long, TreeSet<Opportunity>> cache) {
			log.debug("Clearing end of day expiries cache for market with id: " + opp.getMarketId());
			TreeSet<Opportunity> oppsCache = cache.get(opp.getMarketId());
			if (oppsCache != null) {
				oppsCache.clear();
			}
		}

		/**
		 * Removes all opportunities from the previous week of the specified cache for this opportunity's market.
		 * 
		 * @param opp the opportunity that will be added after the cache clear
		 * @param cache the cache to be cleared
		 */
		private void clearWeekGraphCache(Opportunity opp, Map<Long, TreeSet<Opportunity>> cache) {
			log.debug("Clearing end of week expiries cache for market with id: " + opp.getMarketId());
			StringBuilder sb = new StringBuilder("Removing opportunities with ids: [");
			TreeSet<Opportunity> oppsCache = cache.get(opp.getMarketId());
			if (oppsCache != null && !oppsCache.isEmpty()) {
				Calendar cal = createCalendar();
				cal.setTime(opp.getTimeEstClosing());
				int oppWeek = cal.get(Calendar.WEEK_OF_YEAR);
				Iterator<Opportunity> iterator = oppsCache.iterator();
				Opportunity cachedOpp = iterator.next();
				cal.setTime(cachedOpp.getTimeEstClosing());
				int lastOppWeek = cal.get(Calendar.WEEK_OF_YEAR);
				while (lastOppWeek != oppWeek) {
					iterator.remove();
					appendOppToLog(cachedOpp, sb);
					if (!iterator.hasNext()) {
						break;
					}
					cal.setTime(iterator.next().getTimeEstClosing());
					lastOppWeek = cal.get(Calendar.WEEK_OF_YEAR);
				}
			}
			log.debug(sb.append("]"));
		}

		/**
		 * Removes all unnecessary opportunities. If the clearing is triggered due to end of period, opportunities before
		 * {@link PastExpiriesCache#MONTH_PERIOD_SPLITTER_DAY} are removed from the specified cache. If a special case for clearing is
		 * triggered and the current day is after {@link PastExpiriesCache#MONTH_PERIOD_SPLITTER_DAY}, opportunities from previous month are
		 * cleared.
		 * 
		 * @param opp the opportunity that will be added after the cache clear
		 * @param cache the cache to be cleared
		 * @param periodEnd {@code true} if the clear is triggered because of an end of period opportunity(end of a month for montly),
		 *            {@code false} - special case for clearing
		 */
		private void clearMonthGraphCache(Opportunity opp, Map<Long, TreeSet<Opportunity>> cache, boolean periodEnd) {
			log.debug("Clearing end of month expiries cache for market with id: " + opp.getMarketId());
			StringBuilder sb = new StringBuilder("Removing opportunities with ids: [");
			TreeSet<Opportunity> oppsCache = cache.get(opp.getMarketId());
			Calendar cal = createCalendar();
			if (oppsCache != null && !oppsCache.isEmpty()) {
				if (periodEnd) {
					Iterator<Opportunity> iterator = oppsCache.iterator();
					while (iterator.hasNext()) {
						Opportunity cachedOpp = iterator.next();
						cal.setTime(cachedOpp.getTimeEstClosing());
						if (cal.get(Calendar.DAY_OF_MONTH) < MONTH_PERIOD_SPLITTER_DAY) {
							iterator.remove();
							appendOppToLog(cachedOpp, sb);
						} else {
							break;
						}
					}
				} else {
					cal.setTime(opp.getTimeEstClosing());
					int oppDay = cal.get(Calendar.DAY_OF_MONTH);
					int oppMonth = cal.get(Calendar.MONTH);
					if (oppDay >= MONTH_PERIOD_SPLITTER_DAY) {
						Iterator<Opportunity> iterator = oppsCache.iterator();
						while (iterator.hasNext()) {
							Opportunity cachedOpp = iterator.next();
							cal.setTime(cachedOpp.getTimeEstClosing());
							if (cal.get(Calendar.MONTH) != oppMonth) {
								iterator.remove();
								appendOppToLog(cachedOpp, sb);
							} else {
								break;
							}
						}
					}
				}
			}
			log.debug(sb.append("]"));
		}

		/**
		 * Removes opportunities from the cache. Depending on the month all of the opportunities from the earliest one, two or zero months
		 * are removed. For example at the beginning of January all opportunities from September(the earliest one month in the cache) will
		 * be removed, at the beginning of February all opportunities from October and November will be removed.
		 * 
		 * @param opp the opportunity that will be added after the cache clear
		 * @param cache the cache to be cleared
		 */
		private void clearQuarterGraphCache(Opportunity opp, Map<Long, TreeSet<Opportunity>> cache) {
			log.debug("Clearing end of quarter expiries cache for market with id: " + opp.getMarketId());
			StringBuilder sb = new StringBuilder("Removing opportunities with ids: [");
			TreeSet<Opportunity> oppsCache = cache.get(opp.getMarketId());
			Calendar cal = createCalendar();
			if (oppsCache != null && !oppsCache.isEmpty()) {
				Opportunity lastCachedOpp = oppsCache.last();
				cal.setTime(lastCachedOpp.getTimeEstClosing());
				int lastCachedOppMonth = cal.get(Calendar.MONTH);
				cal.setTime(opp.getTimeEstClosing());
				int oppMonth = cal.get(Calendar.MONTH);
				if (oppMonth > lastCachedOppMonth) {
					// start clearing
					oppMonth++; // because JAN is 0 in the java calendar
					int monthsToClear = oppMonth % 3;
					int monthsCleared = -1;
					int currentMonth = -1;
					Iterator<Opportunity> iterator = oppsCache.iterator();
					while (iterator.hasNext()) {
						Opportunity cachedOpp = iterator.next();
						cal.setTime(cachedOpp.getTimeEstClosing());
						int cachedOppMonth = cal.get(Calendar.MONTH);
						if (currentMonth < cachedOppMonth) {
							monthsCleared++;
							currentMonth = cachedOppMonth;
						}
						if (monthsCleared < monthsToClear) {
							iterator.remove();
							appendOppToLog(cachedOpp, sb);
						} else {
							break;
						}
					}
				}
			}
			log.debug(sb.append("]"));
		}

		private void appendOppToLog(Opportunity opp, StringBuilder builder) {
			if (opp != null) {
				if (builder.charAt(builder.length() - 1) != '[') {
					builder.append(", ");
				}
				builder.append(opp.getId());
			}
		}

		private Calendar createCalendar() {
			return Calendar.getInstance(TimeZone.getTimeZone("GMT"));
		}

		public void stopPastExpiriesCache() {
			log.info("PastExpiriesCacheThread stopping");
			running = false;
			this.interrupt();
		}

		private void putThreadToSleep() {
			try {
				log.info("Putting PastExpiriesCacheThread to sleep");
				Thread.sleep(CACHE_SLEEP_INTERVAL);
			} catch (InterruptedException e) {
				log.error("Unable to put thread to sleep", e);
			}
		}
	}

	private class PastExpiriesCacheComparator implements Comparator<Opportunity> {

		@Override
		public int compare(Opportunity o1, Opportunity o2) {
			if (o1.getId() == o2.getId()) {
				return 0;
			}
			return o1.getTimeEstClosing().after(o2.getTimeEstClosing()) ? -1 : 1;
		}
	}

	private class ProlongedGraphCacheComparator implements Comparator<Opportunity> {

		@Override
		public int compare(Opportunity o1, Opportunity o2) {
			if (o1.getId() == o2.getId()) {
				return 0;
			}
			return o1.getTimeEstClosing().before(o2.getTimeEstClosing()) ? -1 : 1;
		}
	}
}