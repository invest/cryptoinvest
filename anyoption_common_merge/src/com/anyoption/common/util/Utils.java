package com.anyoption.common.util;

import java.math.BigDecimal;
import java.net.URL;

import javax.faces.context.FacesContext;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * web utils
 */
public class Utils extends CommonUtil {
	private static final Logger log = Logger.getLogger(Utils.class);

    public static String getPropertyByHost(String key) {
        FacesContext context = FacesContext.getCurrentInstance();
        HttpServletRequest request = (HttpServletRequest) context.getExternalContext().getRequest();
        return getPropertyByHost(request, key);
    }

	public static String getPropertyByHost(HttpServletRequest request, String key) {
		return getPropertyByHost(request, key, "?" + key + "?");
	}
    
    /**
     * Get default skin id property from Host property
     */
    public static String getDefaultSkin(HttpServletRequest request) {
        return Utils.getPropertyByHost(request, "skin.default");
    }

	/**
	 * Get skin id, first search in the session and after that in the cookies.
	 * @return skinId
	 */
	public static Long getSkinId(HttpSession session, HttpServletRequest request) {
		String skinId = (String)session.getAttribute(ConstantsBase.SKIN_ID);
		//logger.log(Level.DEBUG, "skinId: " + skinId);
		if ( null != skinId ) {
			return new Long(skinId);
		}

		Cookie[] cs = request.getCookies();
		Cookie cookie = null;
		if (cs != null) {
			for (int i = 0; i < cs.length; i++) {
				if (cs[i].getName().equals(ConstantsBase.SKIN_ID)) {
					log.log(Level.TRACE, "found skinId cookie!");
					cookie = cs[i];
					break;
				}
			}
		}
		if (null != cookie && cookie.getValue() != null
				&& !cookie.getValue().equals("")
				&& cookie.getValue().indexOf("null") == -1) {
			try {
				return Long.valueOf(cookie.getValue());
			} catch (Exception e) {
				if (log.isEnabledFor(Level.WARN)) {
					log.warn("SkinId cookie with strange value!", e);
				}
			}
		}

		// return default skinId in a case skinId not in session and cookie
		Long skin = Long.valueOf(getDefaultSkin(request));
		log.debug("SkinId not in session and cookies, Take default skin id: " + skin );

		return skin;
	}
	
	/**
	 * Round a double
	 * @param unrounded the number u want to round
	 * @param precision the number of decimal points you desire.
	 * @param roundingMode what rounding mode to use, ex: BigDecimal.ROUND_HALF_UP
	 * @return
	 */
	public static double round(double unrounded, int precision, int roundingMode) {
	    BigDecimal bd = new BigDecimal(unrounded);
	    BigDecimal rounded = bd.setScale(precision, roundingMode);
	    return rounded.doubleValue();
	}
	
	public static boolean validateCaptcha(String recap, String remoteip, int platformID) throws IOException, JSONException
	{
		String secret = "";
		
		if (platformID == ConstantsBase.PLATFORM_ID_ANYOPTION) {
			secret = ConstantsBase.RECAPTCHA_SECRET_ANYOPTION;
		} 
		else if (platformID == ConstantsBase.PLATFORM_ID_ERTADER) {
			secret = ConstantsBase.RECAPTCHA_SECRET_ETRADER;
		} else if (platformID == ConstantsBase.PLATFORM_ID_COPYOP){
			secret = ConstantsBase.RECAPTCHA_SECRET_COPYOP;
		}
		
		String addressURL = "https://www.google.com/recaptcha/api/siteverify";
		String postData = "secret="+secret+"&response="+recap+"&remoteip="+remoteip;
		URL url = new URL(addressURL);
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        
        conn.setDoOutput(true);
        conn.setRequestMethod("POST");
        conn.setRequestProperty( "Content-Type", "application/x-www-form-urlencoded"); 
        conn.setRequestProperty( "charset", "utf-8");
        conn.setRequestProperty( "Content-Length", Integer.toString(postData.getBytes("UTF-8").length));
        
        DataOutputStream wr = new DataOutputStream( conn.getOutputStream());
        wr.write(postData.getBytes("UTF-8"));
        
        String line, outputString = "";
        BufferedReader reader = new BufferedReader(
                        new InputStreamReader(conn.getInputStream()));
        while ((line = reader.readLine()) != null) {
            outputString += line;
        }
        
        JSONObject obj = new JSONObject(outputString);
        return Boolean.valueOf(obj.getString("success"));
	}
	
	public static boolean validateFileExtension(String fileName) {
		boolean result = false;
		if (fileName != null && !fileName.isEmpty()) {
			Pattern regex = Pattern.compile(ConstantsBase.ALLOWED_FILE_EXTENSIONS_REGEX,
	    			Pattern.CANON_EQ);
	    	Matcher matcher = regex.matcher(fileName.toLowerCase());
	    	if (matcher.matches()) {
	    			result=true;
	    	}
		}
    	return result;
	}

}