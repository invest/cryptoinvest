package com.anyoption.common.util;

import java.util.Calendar;

import org.apache.log4j.Logger;

public class GoldenMinute {
	static Logger log = Logger.getLogger(GoldenMinute.class);
	
    public enum GoldenMinuteType { 
    		STANDARD(1), ADDITIONAL(2);
    		int id;
    		private GoldenMinuteType(int id) {
    			this.id = id;
			}
    		
    		public final static GoldenMinuteType getById(int id) {
    			switch(id) {
    				case 1:
    					return STANDARD;
    				case 2:
    					return ADDITIONAL;
    				default:
    					log.error("Unrecognized gm type:"+id);
    					return null;
    				
    			}
    		}
    	};

    /**
     * expiry (settlement) 
     * the minutes of the hour when the expiry occurs
     */
    public static enum Expiry {
    	AT_FULL(0), AT_QUARTER(15), AT_HALF(30), AT_THREE_QUARTERS(45);
    	public int minutes;
    	Expiry(int minutes) {
    		this.minutes = minutes;
    	}
   	};
   	
    public static enum GoldenMinutePeriod {
    	STANDARD_AT_QUARTER(Expiry.AT_QUARTER, GoldenMinuteType.STANDARD),
    	ADDITIONAL_AT_QUARTER(Expiry.AT_QUARTER, GoldenMinuteType.ADDITIONAL),

    	STANDARD_AT_HALF(Expiry.AT_HALF, GoldenMinuteType.STANDARD),
    	ADDITIONAL_AT_HALF(Expiry.AT_HALF, GoldenMinuteType.ADDITIONAL),

    	STANDARD_AT_THREEQUARTERS(Expiry.AT_THREE_QUARTERS, GoldenMinuteType.STANDARD),
    	ADDITIONAL_AT_THREEQUARTERS(Expiry.AT_THREE_QUARTERS, GoldenMinuteType.ADDITIONAL),

    	STANDARD_AT_FULL(Expiry.AT_FULL, GoldenMinuteType.STANDARD),
    	ADDITIONAL_AT_FULL(Expiry.AT_FULL, GoldenMinuteType.ADDITIONAL);

    	public Expiry expiry;
    	public GoldenMinuteType type;
    	public long id;
		GoldenMinutePeriod(Expiry expiry, GoldenMinuteType type) {
			this.expiry = expiry;
			this.type = type;
			id =  (long)expiry.minutes << 32 | type.id & 0xFFFFFFFFL;
		}
	};

	public static void main(String[] args) {
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.MINUTE, -50);
		System.out.println(cal.toString());
	}
}
