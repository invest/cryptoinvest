package com.anyoption.common.util;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.base.CreditCard;
import com.anyoption.common.beans.base.Skin;

/**
 * Borowed from WinAsUGo... mostly.
 */
public class AnyOptionCreditCardValidator {
	
    private static final Logger log = Logger.getLogger(AnyOptionCreditCardValidator.class);
    public static final int CREDIT_CARD_ISRACARD_MIN_DIGITS = 8;
    public static final int CREDIT_CARD_ISRACARD_DIGITS_CHECK = 9;
    public static final int CREDIT_CARD_ISRACARD_NUMBER_MOD_CHECK = 11;
    
    public final static int CC_VALID = 1;
    public final static int CC_INVALID = 2;
    public final static int CC_DINERS = 3;
    public final static int CC_AMEX = 5;
    
    public static boolean isDiners;
    public static boolean isAmex;

    private static Collection<CreditCardType> cardTypes = new ArrayList<CreditCardType>();
    static {
        cardTypes.add(new Visa());
        cardTypes.add(new Mastercard());
        cardTypes.add(new Solo());
        cardTypes.add(new Switch());
        cardTypes.add(new Maestro());
        cardTypes.add(new Amex());
        cardTypes.add(new Discover());
        cardTypes.add(new Diners());
        cardTypes.add(new Isracard());
        cardTypes.add(new AllCard());
        cardTypes.add(new CUP());
    }

    /**
     * Check cc number
     *
     * @param value cc number
     * @return true if the number is proper
     * @throws Exception
     */
    public static boolean validateCreditCardNumber(String value, long skinId) throws Exception {
        if (log.isDebugEnabled()) {
            log.debug("Validate card " + value);
        }
        setDiners(false);
        setAmex(false);
        //Minimal Credit length (Isracard) should be 8
        if(null != value && !value.trim().equals("") && value.length() >= CREDIT_CARD_ISRACARD_MIN_DIGITS) {
            Iterator<CreditCardType> types = cardTypes.iterator();
            CreditCardType type = null;
            while (types.hasNext()) {
                type = types.next();
                if (type.matches(value)) {
                	 if(type.toString().contains("Diners")){
                		 setDiners(true);
                		 return false;
                	 }else if(type.toString().contains("Amex") && !(skinId == Skin.SKIN_ETRADER)){
                		 setAmex(true);
                		 return false;
                	 }
                    return true;
                }
            }
        }
        return false;
    }


    public static int validateCCNumber(String value, long skinId) throws Exception {
        if (log.isDebugEnabled()) {
            log.debug("Validate card " + value);
        }
        setAmex(false);
        //Minimal Credit length (Isracard) should be 8
        if(null != value && !value.trim().equals("") && value.length() >= CREDIT_CARD_ISRACARD_MIN_DIGITS) {
            Iterator<CreditCardType> types = cardTypes.iterator();
            CreditCardType type = null;
            while (types.hasNext()) {
                type = types.next();
                if (type.matches(value)) {
                	 if(type.toString().contains("Diners")){
                		 setDiners(true);
                		 return CC_DINERS;
                	 }else if((type.toString().contains("Amex")) && !(skinId == Skin.SKIN_ETRADER)){
                		 setAmex(true);
                		 return CC_AMEX;
                	 }
                    return CC_VALID;
                }
            }
        }
        return CC_INVALID;
    }
    /**
     * Check isracrad Withdraw cc number
     *
     * @param value cc number
     * @return true if the number is proper
     * @throws Exception
     */
    public static boolean validateNonIsracardCreditCardNumber(String value) throws Exception {
    	if (log.isDebugEnabled()) {
	        log.debug("Validate card " + value);
	    }
	    //Minimal Credit length ( Isracard) should be 8
	    if (!isParameterEmptyOrNull(value) &&
	    		 value.length() >= CREDIT_CARD_ISRACARD_MIN_DIGITS) {
	    Iterator<CreditCardType> types = cardTypes.iterator();
	    CreditCardType type = null;
	    	while (types.hasNext()) {
	    		type = types.next();
	    		if (!(type instanceof Isracard)) {
	    			if (type.matches(value)) {
	    				return true;
	    			}
	    		}
	    	}
	    }
	    return false;
    }

    /**
     * Check amex Withdraw cc number
     *
     * @param creditCard
     * @return true if the number is proper
     * @throws Exception
     */
    public static boolean validateAmexCreditCardNumber(CreditCard creditCard) throws Exception {
    	if (log.isDebugEnabled()) {
	        log.debug("Validate card " + creditCard.getCcNumber());
	    }
    	if (creditCard.getTypeId() == 5 && String.valueOf(creditCard.getCcNumber()).startsWith("3755")) {
    		return false;
    	}
	    return true;
    }

    // The Luhn algorithm is basically a CRC type
    // system for checking the validity of an entry.
    // All major credit cards use numbers that will
    // pass the Luhn check. Also, all of them are based
    // on MOD 10.
    public static boolean luhnValidate(String numberString) {
        char[] charArray = numberString.toCharArray();
        int[] number = new int[charArray.length];
        int total = 0;

        for (int i = 0; i < charArray.length; i++) {
            number[i] = Character.getNumericValue(charArray[i]);
        }

        for (int i = number.length - 2; i > -1; i -= 2) {
            number[i] *= 2;

            if (number[i] > 9) {
                number[i] -= 9;
            }
        }

        for (int i = 0; i < number.length; i++) {
            total += number[i];
        }

        if (total % 10 != 0) {
            return false;
        }
        return true;
    }
    
    private static boolean isParameterEmptyOrNull(String par) {
		if( (par == null || par.length() == 0) ) {
			return true;
		}
		return false;
	}

    /**
     * CreditCardType implementations define how validation is performed
     * for one type/brand of credit card.
     * @since Validator 1.1.2
     */
    public interface CreditCardType {
        /**
         * Returns true if the card number matches this type of credit
         * card.  Note that this method is <strong>not</strong> responsible
         * for analyzing the general form of the card number because
         * <code>CreditCardValidator</code> performs those checks before
         * calling this method.  It is generally only required to valid the
         * length and prefix of the number to determine if it's the correct
         * type.
         * @param card The card number, never null.
         * @return true if the number matches.
         */
        boolean matches(String card);
    }

    private static class Visa implements CreditCardType {
        private static final String PREFIX = "4";
        public boolean matches(String card) {
            return (card.substring(0, 1).equals(PREFIX)
                    && (card.length() == 13 || card.length() == 16)
                    && luhnValidate(card));
        }
    }

    private static class Amex implements CreditCardType {
        private static final String PREFIX = "34,37,";
        public boolean matches(String card) {
            String prefix2 = card.substring(0, 2) + ",";
            return ((PREFIX.indexOf(prefix2) != -1)
                    && (card.length() == 15)
                    && luhnValidate(card));
        }
    }

    private static class Discover implements CreditCardType {
        private static final String PREFIX = "6011";
        public boolean matches(String card) {
            return (card.substring(0, 4).equals(PREFIX)
                    && (card.length() == 16)
                    && luhnValidate(card));
        }
    }

    private static class Mastercard implements CreditCardType {
        private static final String PREFIX = "51,52,53,54,55,";
        public boolean matches(String card) {
            String prefix2 = card.substring(0, 2) + ",";
            return ((PREFIX.indexOf(prefix2) != -1)
                    && (card.length() == 16)
                    && luhnValidate(card));
        }
    }

    private static class Solo implements CreditCardType {
        private static final String PREFIX = "6334,6767,";
        public boolean matches(String card) {
            String prefix2 = card.substring(0, 4) + ",";
            return ((PREFIX.indexOf(prefix2) != -1)
                    && (card.length() >= 16)
                    && (card.length() <= 19)
                    && luhnValidate(card));
        }
    }

    private static class Switch implements CreditCardType {
        private static final String PREFIX = "4903,4905,4911,4936,6333,6759,";
        private static final String PREFIX2 = "564182,633110,";
        public boolean matches(String card) {
            String prefix = card.substring(0, 4) + ",";
            String prefix2 = card.substring(0, 6) + ",";
            return (((PREFIX.indexOf(prefix) != -1) || (PREFIX2.indexOf(prefix2) != -1))
                    && (card.length() >= 16)
                    && (card.length() <= 19)
                    && luhnValidate(card));
        }
    }

    public static class Maestro implements CreditCardType {
        //private static final String PREFIX = "5018,5020,5038,6304,6759,6761,";
        private static final String PREFIX = "50,56,57,58,59,60,62,63,64,67,90, ";
        public boolean matches(String card) {
            //Validate according to 2 Digits
            String prefix2 = card.substring(0, 2) + ",";
            if (log.isDebugEnabled()) {
                log.debug("Maestro validating " + card + " prefix: " + prefix2 + " luhnValidate: " + luhnValidate(card));
            }
            return ((PREFIX.indexOf(prefix2) != -1)
                    && (card.length() >= 16)
                    && (card.length() <= 18)
                    && luhnValidate(card));
        }
    }
    
    public static class CUP implements CreditCardType {
    	private static final String PREFIX = "62";
		public boolean matches(String card) {
			 return ((card.substring(0, 2).equals(PREFIX))
	                    && (card.length() >= 16)
	                    && (card.length() <= 19)
	                    && luhnValidate(card));
		}
    }

    public static class Isracard implements CreditCardType {
        /**
         * Validation function for isracard credit card
         * The Algorithem:
         *      1. if cc number length equal to 8, then add zero from left.
         *      2. build weightArray : {9,8,7,6,5,4,3,2,1}.
         *      3. multiply each cell in the cc number with weightArray.
         *      4. calculate the sum of the multiplication. (total)
         *      5. if total % 11 is zero then its proper isracard cc.
         *
         * @param numberString the cc number
         * @return true if the cc number is proper
         */
        public boolean matches(String numberString) {
            // adding zero to the left of the number if cc number less then 9
            if (numberString.length() == CREDIT_CARD_ISRACARD_MIN_DIGITS) {
                numberString = "0" + numberString;
            } else if (numberString.length() > CREDIT_CARD_ISRACARD_DIGITS_CHECK) {
                // takes only first 9 digits
                numberString = numberString.substring(0, CREDIT_CARD_ISRACARD_DIGITS_CHECK);
            }

            char[] charArray = numberString.toCharArray();
            int[] number = new int[charArray.length];
            int[] weightArray = {9, 8, 7, 6, 5, 4, 3, 2, 1};
            int total = 0;

            for (int i = 0; i < charArray.length; i++) {
                number[i] = Character.getNumericValue(charArray[i]);

                // multiplication the number cell with the weightArray cell and calculating the sum
                total += number[i] * weightArray[i];
            }

            // final step : check if the total % 11 = 0, if yes then this number is proper
            if ((total % CREDIT_CARD_ISRACARD_NUMBER_MOD_CHECK == 0)) {
                return true;
            }
            return false;
        }
    }

    public static class Diners implements CreditCardType {
        /*
         * Diners Club Carte Blanche   300-305      14  Luhn algorithm
         * Diners Club International[4]     36 	    14  Luhn algorithm
         * Diners Club US & Canada[5]   54, 55 	    16  Luhn algorithm
         */
        private static final String PREFIX_CARTE_BLANCHE = "300,301,302,303,304,305,";
        private static final String PREFIX_INTERNATIONAL = "36,";
        /*private static final String PREFIX_US_CANADA = "54,55,"; */

        public boolean matches(String card) {
            String prefix2 = card.substring(0, 2) + ",";
            String prefix3 = card.substring(0, 3) + ",";
            int cardLength = card.length();
            if (log.isDebugEnabled()) {
                log.debug("Diners validating " + card + " prefix: " + prefix2 + " luhnValidate: " + luhnValidate(card));
            }
            return ((
                    (PREFIX_CARTE_BLANCHE.indexOf(prefix3) != -1 && cardLength == 14)||
                    (PREFIX_INTERNATIONAL.indexOf(prefix2) != -1 && cardLength == 14))
                    /*(PREFIX_US_CANADA.indexOf(prefix2) != -1 && cardLength == 16))*/
                    && luhnValidate(card)
            );
        }
    }

    private static class AllCard implements CreditCardType {
        public boolean matches(String card) {
            int cardLength = card.length();
            if (log.isDebugEnabled()) {
                log.debug("AllCard validating " + card +  "luhnValidate: " + luhnValidate(card));
            }
            return 12 <= cardLength && cardLength <=20 && luhnValidate(card);
        }
    }
    
    public static  boolean isDiners() {
		return isDiners;
	}

	public static void setDiners(boolean diners) {
		isDiners = diners;
	}
	
	
    public static  boolean isAmex() {
		return isAmex;
	}

	public static void setAmex(boolean amex) {
		isAmex = amex;
	}
}