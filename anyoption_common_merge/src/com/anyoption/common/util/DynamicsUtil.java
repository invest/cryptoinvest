package com.anyoption.common.util;

import com.anyoption.common.beans.MarketDynamicsQuoteParams;
import com.anyoption.common.beans.Opportunity;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class DynamicsUtil {
	public static void parseOpportunityQuoteParams(Opportunity o) {
		o.setQuoteParamsObject(parseOpportunityQuoteParams(o.getQuoteParams()));
	}
	
	public static MarketDynamicsQuoteParams parseOpportunityQuoteParams(String quoteParams) {
		Gson gson = new GsonBuilder().serializeNulls().create();
		return gson.fromJson(quoteParams, MarketDynamicsQuoteParams.class);
	}
	
	public static String quoteParamsToString(MarketDynamicsQuoteParams quoteParams) {
		Gson gson = new GsonBuilder().serializeNulls().create();
		return gson.toJson(quoteParams);
	}
}