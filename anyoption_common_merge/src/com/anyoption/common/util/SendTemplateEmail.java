package com.anyoption.common.util;

import java.util.Hashtable;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.apache.velocity.Template;
import org.apache.velocity.app.VelocityEngine;

import com.anyoption.common.beans.base.Platform;

public class SendTemplateEmail extends Thread {
    public static final Logger log = Logger.getLogger(SendTemplateEmail.class);
    
    // The email parameters that we need
  	public static final String PARAM_USER_ID = "userId";
  	public static final String PARAM_EMAIL = "email";
  	public static final String PARAM_USER_FIRST_NAME = "userFirstName";
  	public static final String PARAM_USER_LAST_NAME = "userLastName";
  	public static final String PARAM_USER_NAME = "userName";
  	public static final String PARAM_ACCOUNT_ID = "accountId";
  	public static final String PARAM_PASSWORD = "password";
  	public static final String PARAM_USER_BIRTH_DAY = "userBirthDay";
  	public static final String PARAM_USER_ADDRESS = "userAddress";
  	public static final String PARAM_USER_ADDRESS_STREET = "userAddressStreet";
  	public static final String PARAM_USER_ADDRESS_NUMBER = "userAddressNumber";
  	public static final String PARAM_USER_ADDRESS_CITY = "userAddressCity";
  	public static final String PARAM_USER_ADDRESS_ZIP_CODE = "userAddressZipCode";
  	public static final String PARAM_WRITER_FIRST_NAME = "writerFirstName";
  	public static final String PARAM_WRITER_LAST_NAME = "writerLastName";
  	public static final String PARAM_WRITER_FULL_NAME = "writerFullName";
  	public static final String PARAM_USER_BALANCE = "userBalance";
  	public static final String PARAM_USER_TAX_BALANCE = "userTaxBalance";
  	public static final String PARAM_MOBILE_PHONE = "mobilePhone";
  	public static final String PARAM_TAX = "tax";
  	public static final String PARAM_WIN = "win";
  	public static final String PARAM_LOSE = "lose";
  	public static final String PARAM_SUM_INVESTMENTS = "sumInvest";
  	public static final String PARAM_ID_NUMBER = "userIdNum";
  	public static final String PARAM_SUPPORT_PHONE ="supportPhone";
  	public static final String PARAM_SUPPORT_FAX ="supportFax";
  	public static final String PARAM_BONUS_AMOUNT ="bonusAmount";
  	public static final String PARAM_TRANSACTION_AMOUNT = "transactionAmount";
  	public static final String PARAM_TRANSACTION_PAN = "transactionPan";
  	public static final String PARAM_TRANSACTION_BANK_NAME = "transactionBankName";
  	public static final String PARAM_DATE_DMMYYYY = "dateDDMMYYYY";
  	public static final String PARAM_DATE_MMMMMDYYYY = "dateMMMMMDYYYY";
  	public static final String PARAM_GENDER_DESC_EN_OR_HE = "genderTxt";
  	public static final String PARAM_PAYPAL_EMAIL = "paypalEmail";
  	public static final String PARAM_RESET_PASSWORD_LINK = "resetLink";
  	public static final String PARAM_CANCEL_WITHDRAWL_DAYS = "cancelWithdrawalDays";
  	public static final String PARAM_BONUS_EXP_DAYS = "bonusExpDays";
  	public static final String PARAM_AUTHENTICATION_LINK = "authenticationLink";
      public static final String PARAM_DOWNLOAD_LINK = "downloadLink";
      public static final String PARAM_FOUR_CONDITION = "fourCondition";
      public static final String PARAM_NICK_NAME = "writerNickName";
      public static final String PARAM_TEMPLATE_FOLLOEING_SALES_CALL_FILE_NAME = "Following_SALES_call.html";
      public static final String PARAM_TEMPLATE_NOT_REACHED_MADE_DEPOSIT = "notReachedMadeDeposit.html";
      public static final String PARAM_TEMPLATE_NOT_REACHED_NEVER_DEPOSIT = "notReachedNeverDeposit.html";
      public static final String PARAM_TEMPLATE_ACTIVATION_LOW_GROUP_MAIL = "Activation_questionnaire.html";
      public static final String PARAM_TEMPLATE_ACTIVATION_TRESHOLD_MAIL = "Activation_loss_threshold.html";
      public static final String PARAM_ISSUE_ACTIONS_TIME = "issueActionsTime";
      public static final String PARAM_ISSUE_ACTIONS_DATE = "issueActionsDate";
      public static final String PARAM_USER_TIME_CREATED_DATE = "userTimeCreatedDate";
      public static final String PARAM_USER_TIME_CREATED_TIME = "userTimeCreatedTime";
      public static final String PARAM_BANK_TRANSFER_DETAILS = "bankTransferDetails";
      public static final String PARAM_RECEIPT_NUM = "receiptNum";
  	public static final String PARAM_DATE = "date";
  	public static final String PARAM_AMOUNT = "amount";
  	public static final String PARAM_FOOTER = "footer";
  	public static final String PARAM_USER_FULL_NAME = "userFullName";
  	public static final String PARAM_TRANSFER_AMOUNT = "transferAmount";
  	public static final String PARAM_MAINTENANCE_FEE_TIME = "YearFromLastInvestmentsdate";
  	public static final String PARAM_MAINTENANCE_FEE_AMOUNT = "MaintenanceFeeCurrency";
  	public static final String PARAM_ACTIVATION_LINK = "activationLink";
  	public static final String PARAM_LOSS_TRESHOLD = "lossThreshold";
  	public static final String PARAM_SUPPORT_MAIL_ET = "Support.team@etrader.co.il";
  	public static final String PARAM_MISSING_DOCUMENTS = "missingDocuments";
  	public static final String PARAM_MISSING_PRECENT = "percent";
  	public static final String PARAM_MISSING_YEAR = "year";
  	public static final String PARAM_BLANK_EMAIL = "blankEmailParam";
  	public static final String PARAM_MIN_WITHDRAW = "MinWithdraw";
  	
  	public static final String PARAM_DYNAMIC_POP = "DynamicPOP";
  	public static final String PARAM_DYNAMIC_POP_SHOW = "isShowBullet";

    private static final String MAIL_TEMPLATES_COPYOP_SUBFOLDER = "copyop/";

    public static Template getEmailTemplate(String fileName, String lang, long skinId, String templatePath, long recipientUserWriterId, int platformId) throws Exception {
    	//TODO: The one who wrote this function in the first place should rewrite this function again :)
    	if (CommonUtil.isCopyopWriter(recipientUserWriterId) || platformId == Platform.COPYOP) {
    		templatePath += MAIL_TEMPLATES_COPYOP_SUBFOLDER;
    	}
        templatePath += lang + "_" + skinId + "/" ;
        log.debug("Template Path : " + templatePath + ", Template file: " + fileName);

        Properties props = new Properties();
        props.setProperty("file.resource.loader.path", templatePath);
        props.setProperty("resource.loader", "file");
        props.setProperty("file.resource.loader.class", "org.apache.velocity.runtime.resource.loader.FileResourceLoader");
        props.setProperty("file.resource.loader.cache", "true");
        VelocityEngine v = new VelocityEngine();
        v.init(props);
        try {
        	return v.getTemplate(fileName, "UTF-8");
        } catch (Exception e) {
        	log.error("!!! ERROR >> Cannot find email template : " + e.getMessage());
        	throw e;
        }
    }
    
    protected void updateCopyopSenderAndSubject(Hashtable<String, String> emailProperties, 
    		long recipientUserWriterId, long platformId) {
    	//TODO: The one who wrote this function in the first place should rewrite this function again :)
    	if (CommonUtil.isCopyopWriter(recipientUserWriterId) || platformId == Platform.COPYOP) {
	    	// NOTE: in CommonUtil.sendEmail there is code that sets name for the "from" field
	    	String subject = emailProperties.get("subject");
	    	if (null != subject) {
	    		subject = subject.replaceAll("anyoption", "copyop");
	    		emailProperties.put("subject", subject);
	    	}
	    	String from = emailProperties.get("from");
	    	if (null != from) {
	    		from = from.replaceAll("anyoption", "copyop");
	    		emailProperties.put("from", from);
	    	}
    	}
    }
}