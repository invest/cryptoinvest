package com.anyoption.common.util;

import java.io.BufferedInputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import javax.mail.Address;
import javax.mail.Flags;
import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.Multipart;
import javax.mail.Part;
import javax.mail.Session;
import javax.mail.Store;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.search.FlagTerm;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.User;
import com.anyoption.common.managers.UsersManagerBase;
import com.anyoption.common.service.CommonJSONService;
import com.anyoption.common.service.UploadDocumentsService;
import com.anyoption.common.service.results.MethodResult;
import com.sun.mail.util.MailSSLSocketFactory;

public class IMAPUtil {
	
	private static final Logger log = Logger.getLogger(IMAPUtil.class);
	
	public static void fetchUnread(long limitation, String emailReportRecipients,
									String host, String user, String password) {
		try {
			Properties props = System.getProperties();
			
			props.setProperty("mail.imap.auth", "true"); 
			props.setProperty("mail.imap.starttls.enable", "false");
			props.setProperty("mail.imap.port", "143");
			
			MailSSLSocketFactory sf = new MailSSLSocketFactory();
			sf.setTrustAllHosts(true); 
			props.put("mail.imap.ssl.socketFactory", sf);
			props.setProperty("mail.imap.ssl.trust", "*");
			
			props.setProperty("mail.imap.ssl.enable", "false"); 
			
			Session session = Session.getInstance(props/*, authenticator*/);
			
			//session.setDebug(true);
			
			Store store = session.getStore("imap");
			store.connect(host, user, password);
			Folder inbox = store.getFolder( "INBOX" );
			inbox.open( Folder.READ_WRITE );
			
			// Fetch unseen messages from inbox folder
			Message[] messages = inbox.search(new FlagTerm(new Flags(Flags.Flag.SEEN), false));
			ArrayList<Message> messagesWithAttachment = new ArrayList<Message>();
			
			for (Message message : messages) {
				if (messagesWithAttachment.size() == limitation) {
					break;
				}
				String contentType = message.getContentType();
				if (contentType.contains("multipart")) {
					messagesWithAttachment.add(message);
				} else {
					message.setFlag(Flags.Flag.SEEN, false);
				}
			}
			
			Map<Message, String> emailsMessageMap = getEmailsForCheck(messagesWithAttachment);
			if (emailsMessageMap != null && !emailsMessageMap.isEmpty()) {
				ArrayList<String> emailsForReport = processEmails(emailsMessageMap);
				sendReport(emailsForReport, emailReportRecipients);
			}
			inbox.close(true);
			store.close();
			
		} catch (Exception e) {
			log.error("Cannot process emails!", e);
		}
	}
	
	private static void sendReport(ArrayList<String> emailsForReport, String emailReportRecipients) {
    	Hashtable<String, String> serverProperties = new Hashtable<String, String>();
		serverProperties.put("url", CommonUtil.getProperty("email.server"));
		serverProperties.put("auth", "true");
		serverProperties.put("user", CommonUtil.getProperty("email.uname"));
		serverProperties.put("pass", CommonUtil.getProperty("email.pass"));

		Hashtable<String, String> emailProperties = new Hashtable<String, String>();

		emailProperties.put("from", CommonUtil.getProperty("email.from"));
		emailProperties.put("to", emailReportRecipients);
		emailProperties.put("subject", "User emails mismatch report");
		StringBuilder body = new StringBuilder();
		body.append("List with all emails from the inbox that do not match user emails: <br/><br/>");
		for (String s : emailsForReport) {
			body.append(s);
			body.append("<br/>");
		}
		String stringBody = body.toString();
		emailProperties.put("body", stringBody);
		CommonUtil.sendEmail(serverProperties, emailProperties, null);
	}
	
	private static ArrayList<String> processEmails(Map<Message, String> emailsMessageMap) throws Exception {
		ArrayList<String> emailsForReport = new ArrayList<String>();
		Set<String> emailsForCheck = new HashSet<String>();
		for (Map.Entry<Message, String> m : emailsMessageMap.entrySet()) {
			emailsForCheck.add(m.getValue());
		}
    	HashMap<String, User> existingEmails = UsersManagerBase.checkForExistingEmails(
    																	new ArrayList<String>(emailsForCheck));
    	for (Map.Entry<Message, String> map : emailsMessageMap.entrySet()) {
    		Message message = map.getKey();
    		if (existingEmails.get(emailsMessageMap.get(message)) != null) {
    			Multipart multiPart = (Multipart) message.getContent();
    			int numberOfParts = multiPart.getCount();
    			int numberOfProcessedParts = 0;
    			for (int partCount = 0; partCount < numberOfParts; partCount++) {
    				MimeBodyPart part = (MimeBodyPart) multiPart.getBodyPart(partCount);
    				if (Part.ATTACHMENT.equalsIgnoreCase(part.getDisposition())) {
    					BufferedInputStream bis = new BufferedInputStream(part.getInputStream());
    					String fileName = part.getFileName();
    					MethodResult result = new MethodResult();
    					UploadDocumentsService.uploadFile(bis, existingEmails.get(map.getValue()),
    													  ConstantsBase.FILE_TYPE_DEFAULT, fileName, 0, 0, result);
    					if (result.getErrorCode() == CommonJSONService.ERROR_CODE_SUCCESS) {
    						numberOfProcessedParts++;
    					}
    				}
    			}
    			if (numberOfProcessedParts == 0) {
    				message.setFlag(Flags.Flag.SEEN, false);
    			}
    		} else {
    			Address[] froms = message.getFrom();
				String email = froms == null ? null : ((InternetAddress) froms[0]).getAddress();
    			if (email.equalsIgnoreCase("support@anyoption.com")) {
        			message.setFlag(Flags.Flag.SEEN, false);
    			} else {
	    			message.setFlag(Flags.Flag.SEEN, true);
	    			emailsForReport.add(emailsMessageMap.get(message));
    			}
    		}
    	}
    	return emailsForReport;
	}
	
	private static HashMap<Message, String> getEmailsForCheck(ArrayList<Message> messages) throws Exception {
		HashMap<Message, String> emailsMessageMap = null;
		if (messages.size() > 0) {
			emailsMessageMap = new HashMap<>();
			for (Message message : messages) {
				Multipart multiPart = (Multipart) message.getContent();
				int numberOfParts = multiPart.getCount();
				int firstPart = 0;
				for (int partCount = 0; partCount < numberOfParts; partCount++) {
					MimeBodyPart part = (MimeBodyPart) multiPart.getBodyPart(partCount);
					if (Part.ATTACHMENT.equalsIgnoreCase(part.getDisposition()) && firstPart == 0) {
						Address[] froms = message.getFrom();
						String email = froms == null ? null : ((InternetAddress) froms[0]).getAddress();
						String subject = null;
						if (email.equalsIgnoreCase("suppoert@anyoption.com")) {
							subject = message.getSubject();
						}
						if (subject != null && !subject.isEmpty()) {
							email = subject;
						}
                        emailsMessageMap.put(message, email.trim().toUpperCase());
						firstPart++;
					}
				}
				if (firstPart == 0) {
					message.setFlag(Flags.Flag.SEEN, false);
				}
			}
		}
		return emailsMessageMap;
	}
}
