package com.anyoption.common.util;

import java.io.IOException;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;
import java.util.UUID;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.base.MarketingTrackingCookieDynamic;
import com.anyoption.common.beans.base.MarketingTrackingCookieStatic;
import com.anyoption.ets.beans.MarketingCampCombinationRequestedObj;
import com.google.gson.Gson;

public class MarketingTrackerUtil {
    
    private static final Logger log = Logger.getLogger(MarketingTrackerUtil.class);
    
   
    public static MarketingTrackingCookieStatic getStaticParsedValue(String staticPartHex){        
        MarketingTrackingCookieStatic mtcStatic = new MarketingTrackingCookieStatic();

        try {
            String decodedVal = new String(HexUtil.hexStringToByteArray(staticPartHex), "UTF-8"); 
            
           int staticIndex	= decodedVal.indexOf("MS{");         
           int indexCS		= decodedVal.indexOf("^CS{");
           int indexHTTP	= decodedVal.indexOf("^HTTP{");
           int indexDP		= decodedVal.indexOf("^DP{");
           int indexTS		= decodedVal.indexOf("^TS{");
           int indexUTM		= decodedVal.indexOf("^UTM{");
           int indexAS1		= decodedVal.indexOf("^aff_sub1{");
           int indexAS2		= decodedVal.indexOf("^aff_sub2{");
           int indexAS3		= decodedVal.indexOf("^aff_sub3{");

            mtcStatic.setMs(decodedVal.substring(staticIndex + 3, indexCS));
            mtcStatic.setCs(decodedVal.substring(indexCS + 4, indexHTTP));
            mtcStatic.setHttp(decodedVal.substring(indexHTTP + 6, indexDP));
            mtcStatic.setDp(decodedVal.substring(indexDP + 4, indexTS));
            String ts = decodedVal.substring(indexTS + 4, indexUTM);
            if(indexAS1 >0 && indexAS2 > indexAS1 && indexAS3 > indexAS2) {
	            mtcStatic.setUtmSource(decodedVal.substring(indexUTM + 5, indexAS1));
	            mtcStatic.setAff_sub1(decodedVal.substring(indexAS1 + 10, indexAS2));
	            mtcStatic.setAff_sub2(decodedVal.substring(indexAS2 + 10, indexAS3));
	            mtcStatic.setAff_sub3(decodedVal.substring(indexAS3 + 10, decodedVal.length()));
            } else {
            	mtcStatic.setUtmSource(decodedVal.substring(indexUTM + 5, decodedVal.length()));
            }
            SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss dd/MM/yy");
            sdf.setTimeZone(TimeZone.getTimeZone("GMT"));
            mtcStatic.setTs(sdf.parse(ts));
        } catch (Exception e) {
            log.info("Marketing Tracker Catch when getStaticParsedValue with value:" + staticPartHex);
            mtcStatic = null;
        }
        return mtcStatic;
    }
    
    public static MarketingTrackingCookieDynamic getDynamicParsedValue(String clickValue) {

        MarketingTrackingCookieDynamic mtcDynamic = new MarketingTrackingCookieDynamic();
        String time = "";
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss dd/MM/yy");
        sdf.setTimeZone(TimeZone.getTimeZone("GMT"));

        try {
        	String dynamic = new String(HexUtil.hexStringToByteArray(clickValue), "UTF-8");
        	int http	= dynamic.indexOf("HTTP{");
        	int cd		= dynamic.indexOf("^CD{");
            int td		= dynamic.indexOf("^TD{");
            int dp		= dynamic.indexOf("^DP{");
            int utm		= dynamic.indexOf("^UTM{");
            int affSub1	= dynamic.indexOf("^aff_sub1{");
            int affSub2	= dynamic.indexOf("^aff_sub2{");
            int affSub3	= dynamic.indexOf("^aff_sub3{");
            
            mtcDynamic.setHttp(dynamic.substring( http + 5, cd));
            mtcDynamic.setCd(dynamic.substring(cd + 4, td));
            time = dynamic.substring(td + 4, dp);
            mtcDynamic.setTd(sdf.parse(time));
        	mtcDynamic.setDp(dynamic.substring(dp + 4, utm));
        	
        	if(affSub1 >0 && affSub2 > affSub1 && affSub3 > affSub2) {
        		mtcDynamic.setUtmSource(dynamic.substring(utm + 5, affSub1));
	        	mtcDynamic.setAff_sub1(dynamic.substring(affSub1 + 10, affSub2));
	        	mtcDynamic.setAff_sub2(dynamic.substring(affSub2 + 10, affSub3));
	        	mtcDynamic.setAff_sub3(dynamic.substring(affSub3 + 10, dynamic.length()));
        	} else {
        		mtcDynamic.setUtmSource(dynamic.substring(utm + 5, dynamic.length()));
        	}
        } catch (Exception e) {
            log.info("Marketing Tracker Catch when getDynamicParsedValue with cookie value:" + clickValue);
            mtcDynamic = null;
        }
        return mtcDynamic;
    }
    
    public static boolean isAfterCheckDatesOrNull (Date ts){
        boolean result = true;
        
        if(ts != null){
            Date date = Calendar.getInstance(TimeZone.getTimeZone("GMT")).getTime();
            long millsPerDay = 1000 * 60 * 60 * 24;
            long d2 = date.getTime() / millsPerDay;
            long d1 = ts.getTime() / millsPerDay;
            long daysBetween = d2 - d1;
            if(daysBetween >= MarketingTracker.FOURTY_FIVE_CHECK_SUM) {
                result = true;
            } else {
                result = false;
            }
        } else {
            result = true;
        }
        return result;
    }
    
    public static String generateRandomUUId(String prefix){
         UUID uniqueKey = UUID.randomUUID();
         
         return (prefix + uniqueKey);
    }
    
    public static void marketingTrackingEtsCampaignCombination(MarketingCampCombinationRequestedObj mtObj) {
        Gson gson = new Gson();  
        // convert java object to JSON format,  
        // and returned as JSON formatted string          
        try {
        	String param = gson.toJson(mtObj);
			callEtsServlet(ConstantsBase.ETS_CAMPAIGN_COMBINATION_SERVLET, param);
		} catch (IOException e) {
			log.error("Failed calling ETS servlet!", e);
		}
    	
    }
    
    public static String callEtsServlet(String servlet, String param) throws IOException {
            return "";
//        String url = CommonUtil.getEtsServerName() + servlet;
//        log.log(Level.DEBUG, url);
//        log.log(Level.DEBUG, param);      
//        Calendar beginTime = Calendar.getInstance();
//        //log.debug("Begin Time:" + beginTime);
//        String response = null;
//
//        InputStream is = null;
//        InputStreamReader isr = null;
//        BufferedReader br = null;
//        
//        int readTimeout = 1000;
//        int connectTimeout = 1000;
//        if (servlet.contains(ConstantsBase.ETS_FIRST_DEPOSIT_SERVLET)){
//            readTimeout = 20000;
//            connectTimeout = 20000;
//        }
//        try {
//            byte[] data = param.getBytes("UTF-8");
//            URL u = new URL(url);
//            HttpURLConnection httpCon = (HttpURLConnection) u.openConnection();
//            httpCon.setDoOutput(true);
//            httpCon.setRequestMethod("POST");
//            httpCon.setRequestProperty("Content-Type", "application/json;charset=UTF-8");
//            httpCon.setRequestProperty("Content-Length",String.valueOf(data.length));
//            
//            httpCon.setReadTimeout(readTimeout);
//            httpCon.setConnectTimeout(connectTimeout);
//
//            OutputStream os = httpCon.getOutputStream();
//            os.write(data, 0, data.length);
//            os.flush();
//            try {
//                os.close();
//            } catch (Exception e) {
//                log.log(Level.ERROR, "Can't close output stream.", e);
//            }
//            if (log.isEnabledFor(Level.DEBUG)) {
//                log.log(Level.DEBUG, "Request sent.");
//            }
//            int respCode = httpCon.getResponseCode();
//            if (log.isEnabledFor(Level.DEBUG)) {
//                log.log(Level.DEBUG, "ResponseCode: " + respCode);
//            }
//            is = httpCon.getInputStream();
//            isr = new InputStreamReader(is, "UTF-8");
//
//            br = new BufferedReader(isr);
//            StringBuffer xmlResp = new StringBuffer();
//            String line = null;
//            while ((line = br.readLine()) != null) {
//                xmlResp.append(line);
//            }
//            response = xmlResp.toString();
//            if (log.isEnabledFor(Level.DEBUG)) {
//                log.log(Level.DEBUG, "Response received.");
//                log.log(Level.DEBUG, response);
//            }
//            Calendar endTime = Calendar.getInstance();
//           // log.debug("End Time:" + endTime);
//            log.debug("Called servlet " + url + "in time:" + (endTime.getTimeInMillis() - beginTime.getTimeInMillis()) + " ms");
//           // log.debug("Need time: " + (beginTime - endTime));
//        } catch (IOException ioe) {
//            log.log(Level.ERROR, "Give up");
//            throw ioe;
//        } finally {
//            if (null != br) {
//                try {
//                    br.close();
//                } catch (Exception e) {
//                    log.log(Level.ERROR, "Can't close the buffered reader.", e);
//                }
//            }
//            if (null != isr) {
//                try {
//                    isr.close();
//                } catch (Exception e) {
//                    log.log(Level.ERROR,
//                            "Can't close the input stream reader.", e);
//                }
//            }
//            if (null != is) {
//                try {
//                    is.close();
//                } catch (Exception e) {
//                    log.log(Level.ERROR, "Can't close the input stream.", e);
//                }
//            }
//        }           
//        return response;
    }

    public static String getMarketingTrackerAppLinkParam(HttpServletRequest request) {
        Cookie marketingTracker = null;
        String params = "";
        try {
            marketingTracker = CommonUtil.getCookie(request, MarketingETS.ETS_COOKIE_NAME);
            if (marketingTracker != null) {
                params = MarketingETS.ETS_APP_PARAM + marketingTracker.getValue();
                params = URLEncoder.encode(params, "UTF-8");
            }
        } catch (Exception e) {
            log.error("When marketingTracker Exc: ", e);
        }
        return params;
    }
}
