/**
 * 
 */
package com.anyoption.common.util;

/**
 * Holds all format patterns that should be used in presentation layer (back-end, web and mobile).
 * 
 * @author dzhamaldv
 *
 */
public class PresetationPatternsBase {
	
	private String timeAndDatePattern;
	
	private String timePattern;
	
	private String datePattern;

	/**
	 * @return the timeAndDatePattern
	 */
	public String getTimeAndDatePattern() {
		return timeAndDatePattern;
	}

	/**
	 * @param timeAndDatePattern the timeAndDatePattern to set
	 */
	public void setTimeAndDatePattern(String timeAndDatePattern) {
		this.timeAndDatePattern = timeAndDatePattern;
	}

	/**
	 * @return the timePattern
	 */
	public String getTimePattern() {
		return timePattern;
	}

	/**
	 * @param timePattern the timePattern to set
	 */
	public void setTimePattern(String timePattern) {
		this.timePattern = timePattern;
	}

	/**
	 * @return the datePattern
	 */
	public String getDatePattern() {
		return datePattern;
	}

	/**
	 * @param datePattern the datePattern to set
	 */
	public void setDatePattern(String datePattern) {
		this.datePattern = datePattern;
	}
	
	/**
	 * @return the displayDecimalPattern
	 */
	public String getDisplayDecimalPattern() {
		return "###,###,###,##0.0#########";
	}

	/**
	 * @return the displayLongPattern
	 */
	public String getDisplayLongPattern() {
		return "###,###,###,##0";
	}

}
