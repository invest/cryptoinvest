package com.anyoption.common.util;

import com.anyoption.common.beans.Transaction;
import com.anyoption.common.beans.User;

/**
 * class Pixel
 * @author eyal.ohana
 */
public class Pixel {
	
	/**
	 * The method will replace parameters in the pixel.
	 * @param pixel
	 * @param user
	 * @param transaction
	 * @return newPixel as String
	 */
 	public static String replaceParametersPixel(String pixel, User user, Transaction tra) {
 		String newPixel = pixel;
 		// replace all parameters with values
 		if (newPixel.indexOf(ConstantsBase.MARKETING_PARAMETER_USERID) > -1) {
 			newPixel = newPixel.replace(ConstantsBase.MARKETING_PARAMETER_USERID, String.valueOf(user.getId()));
 		}
 		if (newPixel.indexOf(ConstantsBase.MARKETING_PARAMETER_MOBIE) > -1) {
 			newPixel = newPixel.replace(ConstantsBase.MARKETING_PARAMETER_MOBIE, user.getMobilePhone());
 		}
 		if (newPixel.indexOf(ConstantsBase.MARKETING_PARAMETER_CURRENCY_CODE) > -1) {
 			newPixel = newPixel.replace(ConstantsBase.MARKETING_PARAMETER_CURRENCY_CODE, user.getCurrency().getCode());
 		}
 		if (newPixel.indexOf(ConstantsBase.MARKETING_PARAMETER_DYNAMIC) > -1) {
 			if (null != user.getDynamicParam()) {
 				newPixel = newPixel.replace(ConstantsBase.MARKETING_PARAMETER_DYNAMIC, user.getDynamicParam());
 			} else {
 				newPixel = newPixel.replace(ConstantsBase.MARKETING_PARAMETER_DYNAMIC, ConstantsBase.EMPTY_STRING);
 			}
 		}
 		if (newPixel.indexOf(ConstantsBase.MARKETING_PARAMETER_SUB_AFF_CODE) > -1) {
 			if (user.getSubAffiliateKey() > 0) {
 				newPixel = newPixel.replace(ConstantsBase.MARKETING_PARAMETER_SUB_AFF_CODE, String.valueOf(user.getSubAffiliateKey()));
 			} else {
 				newPixel = newPixel.replace(ConstantsBase.MARKETING_PARAMETER_SUB_AFF_CODE, ConstantsBase.EMPTY_STRING);
 			}
 		}
 		if (newPixel.indexOf(ConstantsBase.MARKETING_PARAMETER_TRANS_AMOUNT) > -1) {   // in receipt
 			if (null != tra) {
 				newPixel = newPixel.replace(ConstantsBase.MARKETING_PARAMETER_TRANS_AMOUNT, CommonUtil.displayAmount(tra.getAmount(), false, ConstantsBase.CURRENCY_USD_ID, false));
 			}
 		}
 		if (newPixel.indexOf(ConstantsBase.MARKETING_PARAMETER_TRANS_ID) > -1) {   // in receipt
 			if ( null != tra ) {
 				newPixel = newPixel.replace(ConstantsBase.MARKETING_PARAMETER_TRANS_ID, String.valueOf(tra.getId()));
 			}
 		}
 		if (newPixel.indexOf(ConstantsBase.MARKETING_PARAMETER_TRANS_AMOUNT_USD) > -1) {   // in receipt
 			if ( null != tra ) {
 				newPixel = newPixel.replace(ConstantsBase.MARKETING_PARAMETER_TRANS_AMOUNT_USD, CommonUtil.getDollarAmount(tra));
 			}
 		}
 		if (newPixel.indexOf(ConstantsBase.MARKETING_PARAMETER_TRANS_AMOUNT_EUR) > -1) {   // in receipt 			
 			if ( null != tra ) {
 				newPixel = newPixel.replace(ConstantsBase.MARKETING_PARAMETER_TRANS_AMOUNT_EUR, CommonUtil.getEuroAmount(tra));
 			}
 		}
 		if (newPixel.indexOf(ConstantsBase.MARKETING_PARAMETER_AFF_SUB1) > -1) {   
 			if (null != user.getAffSub1()) {
 				newPixel = newPixel.replace(ConstantsBase.MARKETING_PARAMETER_AFF_SUB1, user.getAffSub1());
 			} else {
 				newPixel = newPixel.replace(ConstantsBase.MARKETING_PARAMETER_AFF_SUB1, ConstantsBase.EMPTY_STRING);
 			} 			
 		}
 		if (newPixel.indexOf(ConstantsBase.MARKETING_PARAMETER_AFF_SUB2) > -1) {   
 			if (null != user.getAffSub2()) {
 				newPixel = newPixel.replace(ConstantsBase.MARKETING_PARAMETER_AFF_SUB2, user.getAffSub2());
 			} else {
 				newPixel = newPixel.replace(ConstantsBase.MARKETING_PARAMETER_AFF_SUB2, ConstantsBase.EMPTY_STRING);
 			} 			
 		}
 		if (newPixel.indexOf(ConstantsBase.MARKETING_PARAMETER_AFF_SUB3) > -1) {   
 			if (null != user.getAffSub3()) {
 				newPixel = newPixel.replace(ConstantsBase.MARKETING_PARAMETER_AFF_SUB3, user.getAffSub3());
 			} else {
 				newPixel = newPixel.replace(ConstantsBase.MARKETING_PARAMETER_AFF_SUB3, ConstantsBase.EMPTY_STRING);
 			} 			
 		}
 		if (newPixel.indexOf(ConstantsBase.MARKETING_PARAMETER_EMAIL) > -1) {
 			newPixel = newPixel.replace(ConstantsBase.MARKETING_PARAMETER_EMAIL, user.getEmail());
 		}
 		//change all & to &amp;
 		newPixel = newPixel.replace("&", "&amp;");
 		return newPixel;
 	}
}
