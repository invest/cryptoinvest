/**
 *
 */
package com.anyoption.common.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import org.apache.log4j.Logger;

/**
 * All Unix related stuff
 *
 * @author pavelhe
 *
 */
public class UnixUtil {
	private static final Logger log = Logger.getLogger(UnixUtil.class);

	/**
	 * Run unix command
	 *
	 * @param command
	 *            unix command to run
	 * @return
	 */
	public static String runUnixCommand(String command) {

		String s = "";
		String res = "";
		Process p = null;
		BufferedReader stdInput = null;
		BufferedReader stdError = null;

		try {
			log.info("going to run unix command: " + command);
			p = Runtime.getRuntime().exec(command);

			stdInput = new BufferedReader(new InputStreamReader(
					p.getInputStream()));
			stdError = new BufferedReader(new InputStreamReader(
					p.getErrorStream()));

			// read the output from the command
			while ((s = stdInput.readLine()) != null) {
				log.info(s);
				res += s;
			}

			// read any errors from the attempted command
			while ((s = stdError.readLine()) != null) {
				res = ConstantsBase.UNIX_ERROR_STREAM + s;
				log.info(s);
				break;
			}

		} catch (IOException e) {
			log.info("Exception running unix command: " + e);
			res = ConstantsBase.UNIX_ERROR_STREAM;
		} finally {
			try {
				stdError.close();
				stdInput.close();
			} catch (Exception e) {
			}
		}

		return res;
	}
}
