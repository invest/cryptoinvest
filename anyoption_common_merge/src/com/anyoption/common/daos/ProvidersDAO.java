package com.anyoption.common.daos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.base.ProvidersIP;

/**
 * @author liors
 *
 */
public class ProvidersDAO  extends DAOBase {
	private static final Logger logger = Logger.getLogger(ProvidersDAO.class);
	
	/**
	 * @param connection
	 * @return
	 * @throws SQLException
	 */
	public static ArrayList<ProvidersIP> loadProvidersIP(Connection connection) throws SQLException {
		PreparedStatement pstmt = null;
		ResultSet resultSet = null;
		ArrayList<ProvidersIP> providersIPList = new ArrayList<ProvidersIP>();
		try {
			String sql = 
					"SELECT " +
					"	* " +
					"FROM " +
					"	providers p" +
					"	,provider_types pt " +
					"	,providers_ip pi " +
					"	,provider_groups pg " +
					"WHERE " +
					"	p.id= pi.providers_id" +
					"	and p.provider_groups_id = pg.id " +
					"	and pg.provider_types_id = pt.id ";
			
			pstmt = connection.prepareStatement(sql);
			resultSet = pstmt.executeQuery();
			while (resultSet.next()) {
				ProvidersIP providersIP = new ProvidersIP();
				providersIP.setAllow(resultSet.getBoolean("allow"));
				providersIP.setFromIp(resultSet.getString("from_ip"));
				providersIP.setToIp(resultSet.getString("to_ip"));
				providersIP.setProviderGroupId(resultSet.getInt("provider_groups_id"));
				providersIPList.add(providersIP);
			}
		} finally {
			closeStatement(pstmt);
		}
		return providersIPList;
	}
}
