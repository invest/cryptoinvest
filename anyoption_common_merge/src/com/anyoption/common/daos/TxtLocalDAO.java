package com.anyoption.common.daos;

import java.io.BufferedOutputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.log4j.Logger;

public class TxtLocalDAO extends DAOBase {
    private static final Logger logger = Logger.getLogger(TxtLocalDAO.class);
    private static final String DELIMITER = ",";
    private static final String DATE_FORMATTER = "_yyyy_MM_dd_HH_mm";

    public static String exportUsers(Connection connection, String filePath, String groupId) throws SQLException, UnsupportedEncodingException, FileNotFoundException {
        SimpleDateFormat sd = new SimpleDateFormat(DATE_FORMATTER);
        String fileName = "txtLocal"+sd.format(new Date()) + ".csv";
        String filePathName = filePath + fileName;
        
        Statement statement = null;
        ResultSet resultSet = null;
        PrintWriter writer = null;

        try {
            writer = new PrintWriter(new OutputStreamWriter(new BufferedOutputStream(new FileOutputStream(filePathName)), "UTF-8"));
            statement = connection.createStatement(ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);

            logger.debug("Start fetching");
            resultSet = statement.executeQuery("SELECT user_id, first_name, last_name, phone FROM txtlocal_users_export");
            int counter = 0;
            while (resultSet.next()) {
            			writer.append(resultSet.getString("phone")).append(DELIMITER);
            			if(groupId != null) {
            				writer.append(groupId).append(DELIMITER);
            			}
            			writer.append(resultSet.getString("first_name")).append(DELIMITER)
        				.append(resultSet.getString("last_name")).append(DELIMITER)
                		.append(resultSet.getString("user_id"))
            			.println();
                counter++;
            }
            logger.debug("Fetched "+counter+" lines");
            return filePathName;
        } finally { 
            if (resultSet != null) try { resultSet.close(); } catch (SQLException logOrIgnore) {}
            if (statement != null) try { statement.close(); } catch (SQLException logOrIgnore) {}
            if (connection != null) try { connection.close(); } catch (SQLException logOrIgnore) {}
            if (writer != null) { writer.close(); } 
        }
    }

}
