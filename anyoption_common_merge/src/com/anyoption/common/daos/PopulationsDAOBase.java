package com.anyoption.common.daos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.anyoption.common.bl_vos.PopulationEntryBase;

public class PopulationsDAOBase extends DAOBase {
	
	public static PopulationEntryBase getPopulationUserByUserId(Connection con, long userId) throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			String sql = " SELECT " +
							" pu.id population_users_id, " +
						  	" pu.*, " +
						  	" pe.*," +
						  	" p.skin_id, " +
						  	" p.population_type_id, " +
						  	" p_old_pop.name p_old_name, " +
				            " p_old_pop.population_type_id pe_old_population_type_id, " +
				            " pe_old_pop.id pe_old_id, " +
				            " u.class_id, " +
							" u.user_name " +
						 " FROM " +
							 " population_users pu " +
							 	" LEFT JOIN users u on pu.user_id = u.id " +
						 		" LEFT JOIN population_entries pe on pu.curr_population_entry_id = pe.id " +
						 			" LEFT JOIN populations p on pe.population_id = p.id " +
					 			" LEFT JOIN issue_actions ia on pu.entry_type_action_id = ia.id " +
                                 	" LEFT JOIN issues i_old_pop on ia.issue_id = i_old_pop.id " +
                                 		" LEFT JOIN population_entries pe_old_pop on i_old_pop.population_entry_id = pe_old_pop.id " +
	                                 			" LEFT JOIN populations p_old_pop on pe_old_pop.population_id = p_old_pop.id " +
						 " WHERE " +
						 	" pu.user_id = ? ";

			ps = con.prepareStatement(sql);
			ps.setLong(1, userId);
			rs = ps.executeQuery();
			if (rs.next()) {
				PopulationEntryBase vo = new PopulationEntryBase();
				getEntryBaseVO(rs, vo);
				vo.setOldPopulationName(rs.getString("p_old_name"));
				vo.setOldPopEntryId(rs.getLong("pe_old_id"));
				vo.setOldPopulationTypeId(rs.getLong("pe_old_population_type_id"));
				vo.setUserName(rs.getString("user_name"));
                vo.setUserClassId(rs.getLong("class_id"));
				return vo;


			}
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		return null;
	}
	
	
	public static void getEntryBaseVO(ResultSet rs, PopulationEntryBase peb) throws SQLException {
		peb.setAssignWriterId(rs.getLong("curr_assigned_writer_id"));
		peb.setContactId(rs.getLong("contact_id"));
		peb.setCurrEntryId(rs.getLong("curr_population_entry_id"));
		peb.setEntryTypeId(rs.getInt("entry_type_id"));
		peb.setEntryTypeActionId(rs.getLong("entry_type_action_id"));
		peb.setGroupId(rs.getLong("group_id"));
		peb.setLockHistoryId(rs.getLong("lock_history_id"));
		peb.setCurrPopualtionTypeId(rs.getLong("population_type_id"));
		peb.setPopulationId(rs.getLong("population_id"));
		peb.setPopulationUserId(rs.getLong("population_users_id"));
		peb.setQualificationTime(convertToDate(rs.getTimestamp("qualification_Time")));
		peb.setSkinId(rs.getLong("skin_Id"));
		peb.setUserId(rs.getLong("user_Id"));
		peb.setDelayId(rs.getLong("delay_id"));
		peb.setDisplayed(rs.getInt("IS_DISPLAYED")==1?true:false);
		peb.setTimeJoinControlGroup(convertToDate(rs.getTimestamp("time_control")));
	}

}
