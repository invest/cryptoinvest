package com.anyoption.common.daos;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.sql.Types;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Random;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.AppsflyerEvent;
import com.anyoption.common.beans.AppsflyerTrade;
import com.anyoption.common.beans.CompanyProfitBonusWagering;
import com.anyoption.common.beans.ExposureBean;
import com.anyoption.common.beans.Investment;
import com.anyoption.common.beans.InvestmentRejects;
import com.anyoption.common.beans.OneTouchFields;
import com.anyoption.common.beans.Opportunity;
import com.anyoption.common.beans.ServerConfiguration;
import com.anyoption.common.beans.Writer;
import com.anyoption.common.beans.base.BonusUsers;
import com.anyoption.common.bl_vos.InvestmentLimit;
import com.anyoption.common.bl_vos.OptionPlusQuote;
import com.anyoption.common.enums.CopyOpInvTypeEnum;
import com.anyoption.common.enums.SkinGroup;
import com.anyoption.common.managers.InvestmentsManagerBase;
import com.anyoption.common.managers.LanguagesManagerBase;
import com.anyoption.common.managers.SkinsManagerBase;
import com.anyoption.common.util.CommonUtil;
import com.anyoption.common.util.ConstantsBase;
import com.anyoption.common.util.OpportunityCacheBean;
//import com.copyop.common.dto.CopiedInvestment;
//import com.copyop.common.enums.base.UserStateEnum;

import oracle.jdbc.OracleTypes;

/**
 * @author pavelhe
 */
public class InvestmentsDAOBase extends DAOBase {
	private static final Logger log = Logger.getLogger(com.anyoption.common.daos.InvestmentsDAOBase.class);

	public static Long getOriginalInvestmentUser(Connection conn, long copiedInvId) throws SQLException {
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
            pstmt = conn.prepareStatement(
            			" SELECT user_id FROM investments WHERE id = (SELECT COPYOP_INV_ID FROM investments WHERE id = ?) ");
            pstmt.setLong(1, copiedInvId);
            rs = pstmt.executeQuery();
            if (rs.next()) {
            	return rs.getLong("user_id");
            } else {
            	return null;
            }
        } finally {
            closeStatement(pstmt);
            closeResultSet(rs);
        }
	}

	/**
     * @param conn
     * @param opportunityId
     * @param amount
     * @param typeId
	 * @param invRej
	 * @param skinGroup the sking grop which exposure param should be checked
     * @return <code>true</code> if the exposrue after this investment is ok else <code>false</code>
     * @throws SQLException
     */
    public static ExposureBean getInvestmentExposure(
									Connection conn,
									long opportunityId,
									double amount,
									long typeId,
									InvestmentRejects invRej,
									SkinGroup skinGroup) throws SQLException {
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            String sql =
                    " SELECT NVL(SUM(CASE WHEN (A.type_id = 1 OR A.type_id = " + Investment.TYPE_BUBBLE + " ) THEN 1 " +
                    			" ELSE -1 END * A.amount * A.rate),0) AS exp, ";
			if (typeId == Investment.TYPE_BUBBLE) {
				sql += " C.worst_case_return AS max_exp";
			} else {
				sql += " C.max_exposure AS max_exp ";
			}
                    sql += " FROM opportunity_skin_group_map C left join investments A on C.opportunity_id = A.opportunity_id " +
                    		" AND A.is_canceled = 0 " +
                    		" AND A.is_settled  = 0 " +
                    		" AND A.user_id IN (SELECT u.id FROM users u WHERE u.class_id != 0) " +
            		" WHERE C.opportunity_id = ? AND C.skin_group_id = ? ";
			if (typeId == Investment.TYPE_BUBBLE) {
				sql += " GROUP BY C.worst_case_return ";
			} else {
				sql += " GROUP BY C.max_exposure ";
			}

            pstmt = conn.prepareStatement(sql);
            pstmt.setLong(1, opportunityId);
            pstmt.setLong(2, skinGroup.getId());
            rs = pstmt.executeQuery();
            if (rs.next()) {
            	return new ExposureBean(rs.getDouble("exp"), rs.getDouble("max_exp"));
            }
        } finally {
            closeStatement(pstmt);
            closeResultSet(rs);
        }
        return new ExposureBean();
    }


	/**
     * @param conn
     * @param userId
     * @param opportunityId
     * @param amount
     * @return <code>true</code> for every 10 D3 investment below the amount per user (same opp') else <code>false</code>
     * @throws SQLException
     */
    public static boolean sendD3Alert(Connection conn, long userId, long opportunityId, double rate, int amount) throws SQLException {
    	   PreparedStatement pstmt = null;
           ResultSet rs = null;

           try{
               String sql = " SELECT " +
               							" count(*) as count" +
               				" FROM " +
               							" investment_rejects " +
               				" WHERE " +
               							" user_id = ? " +
               				" AND " +
               							" opportunity_id = ? " +
               				" AND " +
               							" reject_type_id = 11 " + // D3 Investment_Reject_Types
               				" AND " +
               							" amount * ? < " + amount;
               pstmt = conn.prepareStatement(sql);
               pstmt.setLong(1, userId);
               pstmt.setLong(2, opportunityId);
               pstmt.setDouble(3, rate);
               rs = pstmt.executeQuery();
               if (rs.next()) {
            	   int i = rs.getInt("count");
            	   if((i-1)%10==0) { /* first, eleventh, twenty first ... */
            		   return true;
            	   } else {
            		   return false;
            	   }
               }
           } finally {
               closeStatement(pstmt);
               closeResultSet(rs);
           }

           return false;
    }

    public static double getTurnoverFactor(Connection conn, long opportunityTypeId) throws SQLException {
        double factor = 1;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            String sql =
                " SELECT " +
                "   tf.factor " +
                " FROM " +
                "   turnover_factor tf " +
                " WHERE " +
                "   tf.opportunity_type_id = ?";

            ps = conn.prepareStatement(sql);
            ps.setLong(1, opportunityTypeId);
            rs = ps.executeQuery();
            if (rs.next()) {
                factor = rs.getDouble("factor");
            }
        } finally {
            closeResultSet(rs);
            closeStatement(ps);
        }

        return factor;
    }
    
//	public static ArrayList<CopiedInvestment> getCopiedInvFromLastLogout(Connection con, Date lastOffLogoutnDateTime , long userId) throws SQLException {		
//		PreparedStatement ps = null;
//		ResultSet rs = null;
//		ArrayList<CopiedInvestment> list = new ArrayList<CopiedInvestment>();	
//		log.debug("Try to get lastLogOutCopiedInv for userId:" + userId + " from lastOffLogoutnDateTime:" +lastOffLogoutnDateTime);
//		try {
//            String sql = " SELECT res.*, case when ? > sysdate -1 then 1 else 0 end as from_last_logoff " +
//            			 " FROM " +
//		            			" (SELECT " +
//		            				" copy_inv.user_id, " +
//		            				" sum((i.amount - i.house_result) - i.amount) as profit, " +
//		            				" count(*) as inv_cnt " +
//		            			" FROM " +
//		            				" investments i " +
//		            				" ,investments copy_inv " +
//		            				 ", users_active_data uad" +
//		            			" WHERE " +
//		            				" i.copyop_type_id = 1 " +
//		            				" and i.is_settled = 1 " +
//		            				" and i.is_canceled = 0 " +
//									" and i.copyop_inv_id = copy_inv.id " +
//									" and uad.user_id = copy_inv.user_id" +
//									" and uad.copyop_user_status in (" + UserStateEnum.STATE_REGULAR.getId() + ", " + UserStateEnum.STATE_BLOCKED.getId() + " )" +
//									" and i.user_id =  ? " +  
//		            				" and i.time_settled >= greatest(? , sysdate - 1) " + 
//								" GROUP BY " +
//									" copy_inv.user_id ) res" +
//						" ORDER BY " +
//							" 2 desc";
//            
//			ps = con.prepareStatement(sql);
//			
//			ps.setTimestamp(1, CommonUtil.convertToTimeStamp(lastOffLogoutnDateTime));
//			ps.setLong(2, userId);
//			ps.setTimestamp(3, CommonUtil.convertToTimeStamp(lastOffLogoutnDateTime));
//			
//			rs = ps.executeQuery();
//
//			while (rs.next()) {
//				CopiedInvestment copiedInvestment = new CopiedInvestment(userId, rs.getLong("user_id"), 
//																				 rs.getInt("inv_cnt"), 
//																				 rs.getLong("profit"),
//																				 rs.getLong("from_last_logoff") == 1 ? true : false);									
//				list.add(copiedInvestment);
//			}			
//		} finally {
//			closeResultSet(rs);
//			closeStatement(ps);
//		}
//		return list;
//	}
	
	public static void insertAppsflyerInvestmentHouseResult(Connection con, int interval, String users) throws SQLException {
		PreparedStatement ps = null;		
		try {
			String sql =" INSERT INTO APPSFLYER_INVESTMENTS " +
					 		" (ID, USER_ID, APPSFLYER_ID, AMOUNT, APP_ID, DEV_KEY, IP, OS_TYPE_ID, TIME_SETTLED) " +
					 			" SELECT " +
					 				" SEQ_APPSFLYER_INVESTMENTS.nextval, " +
					 				" inv.*, " +
					 				" sysdate appsflyer_time " +
				 				" FROM " +
					 				" (SELECT " +
									   " u.id user_id, " +
									   " duis.appsflyer_id, " +
									   " SUM((i.house_result * i.rate*i.contracts_count)/100) AS amount, " +
									   " ac.app_id, " +
									   " ac.dev_key, " +
									   " u.ip, " +
									   " ac.os_type_id " +
									" FROM " + 
									  " ( SELECT " +
									  		" ii.*, " +
											" row_number() " +
											" over(" +
												" partition by " +
													" ii.user_id, " +
													" ii.opportunity_id, " +
													" ii.type_id, " +
													" ii.amount, " +
													" ii.time_created, " +
													" ii.time_settled, " +
													" ii.group_inv_id order by ii.id desc " +
											" ) id_order, " +
											" count(*)" +
											" over( " +
												" partition by " +
													" ii.user_id, " +
													" ii.opportunity_id, " +
													" ii.type_id, ii.amount, " +
													" ii.time_created, " +
													" ii.time_settled, " +
													" ii.group_inv_id " +
											" ) contracts_count " +
									   " FROM investments ii " +
									   " WHERE  (ii.is_settled = 1)) i, " +
									  " users u, " +
									  " (select " +
									  		" device_unique_id, " +
									  		" appsflyer_id, " +
									  		" os_type_id " +
									  	" from " + 
									  		" device_unique_ids_skin duis " +
									  	" group by " +
									  		" device_unique_id, " +
									  		" appsflyer_id, " +
									  		" os_type_id) duis, " +
									  " appsflyer_config ac " +
									" WHERE " +
										" i.user_id = u.id " +
										" AND u.device_unique_id = duis.device_unique_id " +
										" AND duis.appsflyer_id IS NOT NULL " +
										" AND ac.os_type_id    = duis.os_type_id " +
										" AND ac.platform_id = u.platform_id " +
										" AND u.writer_id in (" + Writer.WRITER_ID_MOBILE + ", " + Writer.WRITER_ID_COPYOP_MOBILE + ") " +
										" AND (i.house_result * i.rate*i.contracts_count)/100 <> 0 " +
										" AND i.time_settled >= trunc((sysdate -  " + interval + " ) ,'DD') and i.time_settled < trunc((sysdate) ,'DD') ";										
										if (!CommonUtil.isParameterEmptyOrNull(users)) {
											sql += " AND u.id in (" + users + ") ";
										}
							sql += " GROUP BY " +
										" u.id, " +
										" duis.appsflyer_id, " +
										" ac.app_id, " +
										" ac.dev_key, " +
										" ac.os_type_id, " +
										" u.ip ) inv ";
			
			ps = con.prepareStatement(sql);
			ps.executeUpdate();			
		} finally {
			closeStatement(ps);
		}
	}

	public static HashMap<Long, AppsflyerTrade> getAppsflyerInvestmentHouseResult(Connection con, int numberOfRecords) throws SQLException {
		HashMap<Long, AppsflyerTrade> eventsMap = new HashMap<Long, AppsflyerTrade>();
		PreparedStatement ps = null;
		ResultSet rs = null;
		
		try {
			String sql = " SELECT " +
							" * " +
							" FROM " +
								" (SELECT " +
									" * " +
								" FROM " +
									" appsflyer_investments ai " +
								" WHERE " +
									" ai.status = 0 " +
								" order by ai.id) " +
							" WHERE ROWNUM <= " + numberOfRecords;
			
			ps = con.prepareStatement(sql);
			rs = ps.executeQuery();
			while (rs.next()) {
				AppsflyerEvent event = new AppsflyerEvent();
				AppsflyerTrade trade = new AppsflyerTrade();
				event.setAppsflyerId(rs.getString("appsflyer_id"));
				event.setAmount(rs.getDouble("amount"));
				event.setAppId(rs.getString("app_id"));
				event.setDevKey(rs.getString("dev_key"));
				event.setOsTypeId(rs.getInt("os_type_id"));
				event.setIp(rs.getString("ip"));
				event.setTimeCreated(rs.getDate("time_settled"));
				trade.setAppsflyerEvent(event);
				trade.setUserId(rs.getLong("user_id"));
				eventsMap.put(rs.getLong("id"), trade);
			}
			
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		return eventsMap;
		
	}

	public static void updateAppsflyerEventsStatus(Connection con, ArrayList<Long> idForUpdate) throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		StringBuilder b = new StringBuilder();
		Iterator<?> it = idForUpdate.iterator();
		while (it.hasNext()) {
		  b.append(it.next());
		  if (it.hasNext()) {
		    b.append(',');
		  }
		}
		String result = b.toString();
		try {

			String sql = " UPDATE " +
								" appsflyer_investments " + 
						 " SET status = 1 " + 
						 " WHERE " +
						 		" id in ( " + result + " ) ";

			ps = con.prepareStatement(sql);
			ps.executeUpdate();
		} finally {
			closeStatement(ps);
			closeResultSet(rs);
		}
	}

 
   /**
    * Return the min default inv amount by currency.
    *
    * @param conn the db connection to use
    * @param currencyId the currency
    * @return The requested Investment Limit amount.
    * @throws SQLException
    */
   public static long getUserMinInvestmentLimit(Connection conn, long currencyId) throws SQLException {
       PreparedStatement pstmt = null;
       ResultSet rs = null;
       try {
           String sql =
               " SELECT " +
	                " MIN(ilgcmin.value) min_amount " +
	            " FROM " +
	                " investment_limits il," +
	                " investment_limit_types ilt," +
	                " INVESTMENT_LIMIT_GROUP_CURR ilgcmin " +
	            " WHERE " +
	                " il.LIMIT_TYPE_ID = ilt.id " +
	                " AND il.MIN_LIMIT_GROUP_ID = ilgcmin.INVESTMENT_LIMIT_GROUP_ID " +
	                " AND ilgcmin.CURRENCY_ID = ? " +
	                " AND il.IS_ACTIVE = 1 " +
	                " AND ilt.IS_ACTIVE = 1 " +
	                " AND il.LIMIT_TYPE_ID = " + InvestmentLimit.TYPE_DEFUALT +
	            " ORDER BY " +
	            	" MIN(ilgcmin.value)";

           pstmt = conn.prepareStatement(sql);
           pstmt.setLong(1, currencyId);

           rs = pstmt.executeQuery();
           if (rs.next()) {
               return rs.getLong("min_amount");
           }
       } finally {
           closeResultSet(rs);
           closeStatement(pstmt);
       }
       return 0;
   }

	/**
    * Return the average inv amount by user.
    *
    * @param conn the db connection to use
    * @param currencyId the currency
    * @return The requested Investment Limit amount.
    * @throws SQLException
    */
   public static long getUserAvgInvestment(Connection conn, long userId) throws SQLException {
       PreparedStatement pstmt = null;
       ResultSet rs = null;
       try {
           String sql =
           	"SELECT " +
           		"avg(i.amount) avg_inv " +
           	"FROM " +
           		"investments i, " +
           		"users u " +
           	"WHERE " +
           		"i.user_id = u.id " +
           		"AND i.is_canceled = 0 " +
           		"AND u.id = ? " +
           	"GROUP BY u.id";
           pstmt = conn.prepareStatement(sql);
           pstmt.setLong(1, userId);

           rs = pstmt.executeQuery();
           if (rs.next()) {
               return rs.getLong("avg_inv");
           }
       } finally {
           closeResultSet(rs);
           closeStatement(pstmt);
       }
       return 0;
   }

	public static long getSumAllActiveInvestments(Connection con, long oppId, long userId) throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		long sumInvestments = 0;
		try {
			String sql = " select sum(i.amount) as total_investment "
						+ "from investments i "
						+ "where i.opportunity_id = ? and i.user_id = ? "
							+ "and i.is_canceled = 0 and i.is_settled = 0 and i.copyop_type_id < 2 ";
			ps = con.prepareStatement(sql);
			ps.setLong(1, oppId);
			ps.setLong(2, userId);
			rs = ps.executeQuery();
			while (rs.next()) {
				sumInvestments = rs.getLong("total_investment");
			}
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		return sumInvestments;
	}
	
//	   public static List<CopiedInvestment> getCopiedInvestmentsForUser(Connection conn, long userId) throws SQLException {
//	       PreparedStatement pstmt = null;
//	       ResultSet rs = null;
//	       ArrayList<CopiedInvestment> list = new ArrayList<CopiedInvestment>();
//	       try {
//	           String sql = 
//							 "SELECT " +  
//							        "copied_inv.time_created tc, " +  
//							        "-copied_inv.house_result hr, " +  
//							        "master_inv.user_id usid, " +  
//							        "o.market_id mid " + 
//							  "FROM " +  
//							        "investments copied_inv, " +  
//							        "investments master_inv, " +  
//							        "opportunities o " +  
//							  "WHERE " +  
//							        "copied_inv.copyop_inv_id = master_inv.ID " +   
//							        "AND copied_inv.opportunity_id = o.id " +  
//							        "AND copied_inv.copyop_type_id = 1 " +  
//							        "AND copied_inv.copyop_inv_id IS NOT NULL " +  
//							        "AND copied_inv.is_settled = 1 " +  
//							        "AND copied_inv.is_canceled = 0 " +  
//							        "AND copied_inv.user_id = ? " +
//					        	"ORDER BY copied_inv.time_created";
//	           pstmt = conn.prepareStatement(sql);
//	           pstmt.setLong(1, userId);
//
//	           rs = pstmt.executeQuery();
//	           while(rs.next()) {
//	               CopiedInvestment ci =  new CopiedInvestment();
//	               ci.setCopyFromUserId(rs.getLong("usid"));
//	               ci.setMarketId(rs.getLong("mid"));
//	               ci.setProfit(rs.getLong("hr"));
//	               ci.setUserId(userId);
//	               ci.setTimeCreated(rs.getDate("tc"));
//	               list.add(ci);
//	           }
//	       } finally {
//	           closeResultSet(rs);
//	           closeStatement(pstmt);
//	       }
//	       return list;
//	   }

	public static long getExposureHighAmountLimitCoefficient(	Connection conn, int highExposureInvestmentLimitGroup,
													long currencyId) throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			String sql = "SELECT VALUE "
						+ "FROM INVESTMENT_LIMIT_GROUP_CURR "
						+ "WHERE INVESTMENT_LIMIT_GROUP_ID = ? AND CURRENCY_ID = ?";
			ps = conn.prepareStatement(sql);
			ps.setInt(1, highExposureInvestmentLimitGroup);
			ps.setLong(2, currencyId);
			rs = ps.executeQuery();
			if (rs.next()) {
				return rs.getLong("VALUE");
			}
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		return 0l;
	}

	public static CompanyProfitBonusWagering getBonusWageringInvestmentCalc(Connection conn, BonusUsers bonusUser) throws SQLException {
		PreparedStatement ps = null;
        ResultSet rs = null;
        CompanyProfitBonusWagering profit = new CompanyProfitBonusWagering();
        try {
            String sql ="SELECT " +
            			"	SUM(i.amount * bwi.ratio) as sum_inv_amount " +
            			"	,SUM(i.house_result * bwi.ratio) as sum_house_result " +
            			"FROM " +
            			"   bonus_wagering_investments bwi " +
            			"   ,investments i " +
            			"WHERE " +
            			"	bwi.investment_id = i.id " +
            			"   AND bwi.bonus_users_id = ? " ;
            ps = conn.prepareStatement(sql);
            ps.setLong(1, bonusUser.getId());
			rs = ps.executeQuery();
			if (rs.next()) {				
				profit.setSumInvAmount(rs.getDouble("sum_inv_amount"));
				profit.setSumHouseResult(rs.getDouble("sum_house_result"));			
			}			
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		return profit;
	}

    /**
     * This method check if the user have open/close investments by given user id.
     * If the given settled param is `false` than the method check and return true if the user
     * has open investment.
     * @param con
     * @param userId
     * @param settled
     * @return true if statement fetch data's, false otherwise.
     * @throws SQLException
     */
    public static boolean isHasInvestments(Connection con, long userId, boolean settled) throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		boolean hasOpenInv = false;
		try {
			String sql = 
					"SELECT * " +
					"FROM " +
					"	investments " +
					"WHERE " +
					"	user_id = ? and " +
					"	is_settled = ?";

			ps = con.prepareStatement(sql);
			ps.setLong(1, userId);
			ps.setInt(2, settled == true ? 1 : 0);
			rs = ps.executeQuery();
			
			if (rs.next()) {
				hasOpenInv = true;
			}
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		return hasOpenInv;
	}
    
	public static ArrayList<Long> getBubbleInvestmentsForClose(Connection con, long addTimeToBubbleEndTime, long addDaysToSearchPeriod, long queryRowLimit)throws SQLException {
		ArrayList<Long> bubbleIds = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			String sql = "SELECT i.id " +
						 " FROM investments i " +
					     " WHERE i.type_id = ? " +
				         " AND i.is_settled <> 1 " +
					     " AND i.bubble_end_time + 1 / 24 / 60 * ? < SYSDATE " +
				         " AND i.time_created > SYSDATE - ?" + 
					     " AND rownum <= ?";
			ps = con.prepareStatement(sql);
			ps.setLong(1, Investment.TYPE_BUBBLE);
			ps.setLong(2, addTimeToBubbleEndTime);
			ps.setLong(3, addDaysToSearchPeriod);
			ps.setLong(4, queryRowLimit);
			rs = ps.executeQuery();
			while (rs.next()) {
				bubbleIds = new ArrayList<Long>();
				bubbleIds.add(rs.getLong("id"));
			}
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}

		return bubbleIds;
	}

	/**
     * List all the investments done on opportunity that is closed and are not
     * settled, canceled or void.
     *
     * @param conn the db connection to use
     * @param investmentId the id of the investment to load
     * @param oppSettled true if u want to get the investment only if the opp is closed, false if u want also not closed opp (use for 5 golden minutes)
     * @param isGM if its take profit (GM) true else false
     * @return
     * @throws SQLException
     */
    public static Investment getInvestmentToSettle(Connection conn, long investmentId, boolean oppSettled, boolean isGM, Long userId) throws SQLException {
        Investment i = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            String sql =
                    "SELECT " +
                        "A.*, ";
                        if (isGM) { //if we want to settle take profit investment (GM) we should set the closing level from opportunity.gm_level
                            sql += "B.gm_level as closing_level, ";
                        } else {
                            sql += "B.closing_level, ";
                        }
                 sql += "M.id as market_id, " +
                        "M.insurance_premia_percent, " +
                        "M.roll_up_premia_percent, " +
                        "M.display_name m_display_name, " +
                        "M.decimal_point m_decimal_point, " +
                        "it.display_name i_type_name, " +
                        "to_char(B.time_est_closing, 'YYYY-MM-DD HH24:MI:SS.FF3 TZHTZM') AS time_est_closing, " +
                        "bu.odds_win as bonus_odds_win, " +
                        "bu.odds_lose as bonus_odds_lose, " +
                        "b.opportunity_type_id, " +
                        "b.scheduled scheduled, " +
                        "b.current_level as opp_current_level " +
                    "FROM " +
                        "investments A left join bonus_users bu on A.bonus_user_id = bu.id, " +
                        "opportunities B, " +
                        "markets M, " +
                        "investment_types it " +
                    "WHERE " +
                        "A.opportunity_id = B.id AND "+
                        "M.id = B.market_id AND " +
                        "A.type_id = it.id AND ";
            if (oppSettled) {
                sql +=  "NOT ";
            }
            sql +=      "B.time_act_closing IS NULL AND " +
                        "A.is_settled = 0 AND " +
                        "A.is_canceled = 0 AND " +
                        "A.is_void = 0 AND " +
                        "A.id = ? ";
            if (null != userId) {
            	sql +=  "AND A.user_id = ?";
            }
            pstmt = conn.prepareStatement(sql);
            pstmt.setLong(1, investmentId);
            if (null != userId) {
            	pstmt.setLong(2, userId.longValue());
            }
            rs = pstmt.executeQuery();
            while(rs.next()) {
                i = getVO(rs);
               
                i.setClosingLevel(rs.getDouble("closing_level"));
                i.setBonusUserId(rs.getLong("bonus_user_id"));
                i.setMarketId(rs.getLong("market_id"));
                i.setInsurancePremiaPercent(rs.getDouble("insurance_premia_percent"));
                i.setRollUpPremiaPercent(rs.getDouble("roll_up_premia_percent"));
                i.setMarketName(rs.getString("m_display_name"));
                i.setDecimalPoint(rs.getLong("m_decimal_point"));
                i.setTypeName(rs.getString("i_type_name"));
                i.setBonusLoseOdds(rs.getDouble("bonus_odds_lose"));
                i.setBonusWinOdds(rs.getDouble("bonus_odds_win"));
                SimpleDateFormat localDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS Z");
                try {
					i.setTimeEstClosing(localDateFormat.parse(rs.getString("time_est_closing")));
				} catch (ParseException e) {
					log.error("Problem parsing timeEstClosing! " + e);
				}
				i.setOpportunityTypeId(rs.getLong("opportunity_type_id"));
				i.setOppCurrentLevel(rs.getDouble("opp_current_level"));
				i.setApiExternalUserId(rs.getLong("api_external_user_id"));
				i.setScheduledId(rs.getLong("scheduled"));
            }
        } finally {
            closeResultSet(rs);
            closeStatement(pstmt);
        }
        return i;
    }

    protected static Investment getVO(ResultSet rs) throws SQLException {
		Investment vo = new Investment();
		vo.setId(rs.getLong("id"));
		vo.setUserId(rs.getLong("user_id"));
		vo.setOpportunityId(rs.getLong("opportunity_Id"));
		vo.setTypeId(rs.getLong("type_Id"));
		vo.setTimeCreated(convertToDate(rs.getTimestamp("time_created")));
		vo.setAmount(rs.getLong("amount"));
		vo.setCurrentLevel(rs.getDouble("current_level"));
		vo.setRealLevel(rs.getDouble("real_level"));
		vo.setWwwLevel(rs.getBigDecimal("www_level"));
		vo.setOddsWin(rs.getDouble("odds_Win"));
		vo.setOddsLose(rs.getDouble("odds_Lose"));
		vo.setWriterId(rs.getLong("writer_id"));
		vo.setCanceledWriterId(rs.getBigDecimal("canceled_writer_id"));
		vo.setIp(rs.getString("ip"));
		vo.setWin(rs.getLong("win"));
		vo.setLose(rs.getLong("lose"));
		vo.setHouseResult(rs.getLong("house_Result"));
		vo.setTimeSettled(convertToDate(rs.getTimestamp("time_settled")));
		vo.setIsSettled(rs.getInt("is_settled"));
		vo.setIsCanceled(rs.getInt("is_canceled"));
		vo.setTimeCanceled(convertToDate(rs.getTimestamp("time_canceled")));
		vo.setIsVoid(rs.getInt("is_void"));
		vo.setBonusOddsChangeTypeId(rs.getInt("bonus_odds_change_type_id"));
        vo.setUtcOffsetCreated(rs.getString("utc_offset_created"));
        vo.setUtcOffsetSettled(rs.getString("utc_offset_settled"));
        vo.setUtcOffsetCancelled(rs.getString("utc_offset_cancelled"));
        vo.setInsuranceAmountGM(rs.getLong("insurance_amount_gm"));
        vo.setInsuranceAmountRU(rs.getLong("insurance_amount_ru"));
        vo.setReferenceInvestmentId(rs.getLong("REFERENCE_INVESTMENT_ID"));
        vo.setRate(rs.getDouble("rate"));
        vo.setPrice(rs.getDouble("price"));
        vo.setInsuranceFlag(rs.getInt("insurance_flag"));
        if (rs.wasNull()) { //check if insurance flag was null in db if yes set it to null
            vo.setInsuranceFlag(null);
        }
        vo.setInsuranceFlagAdditional(rs.getInt("insurance_flag_add"));
        if (rs.wasNull()) { //check if insurance flag was null in db if yes set it to null
            vo.setInsuranceFlagAdditional(null);
        }

        vo.setAcceptedSms(rs.getLong("is_accepted_sms") == 1 ? true : false);
        vo.setOptionPlusFee(rs.getLong("option_plus_fee"));
        vo.setCopyOpInvId(rs.getLong("copyop_inv_id"));
        vo.setCopyOpTypeId(rs.getInt("copyop_type_id"));
        vo.setLikeHourly(rs.getLong("is_like_hourly") == 1 ? true : false);
        vo.setBubbleStartTime(convertToDate(rs.getTimestamp("bubble_start_time")));
        vo.setBubbleEndTime(convertToDate(rs.getTimestamp("bubble_end_time")));
        vo.setBubbleLowLevel(rs.getDouble("bubble_low_level"));
        vo.setBubbleHighLevel(rs.getDouble("bubble_high_level"));
		return vo;
	}
    
    


protected static InvestmentLimit getInvestmentLimitVo(ResultSet rs) throws SQLException {

	InvestmentLimit vo = new InvestmentLimit();
	vo.setId(rs.getLong("ID"));
	vo.setInvLimitTypeId(rs.getInt("LIMIT_TYPE_ID"));
	vo.setOpportunityTypeId(rs.getLong("OPPORTUNITY_TYPE_ID"));
	vo.setScheduled(rs.getLong("SCHEDULED"));
	vo.setUserId(rs.getLong("USER_ID"));
	vo.setMarketId(rs.getLong("MARKET_ID"));
	vo.setStartDate(convertToDate(rs.getTimestamp("START_DATE")));
	vo.setEndDate(convertToDate(rs.getTimestamp("END_DATE")));
	vo.setTimeCreated(convertToDate(rs.getTimestamp("TIME_CREATED")));
	vo.setWriterId(rs.getLong("WRITER_ID"));
	vo.setMinAmountGroupId(rs.getLong("MIN_LIMIT_GROUP_ID"));
	vo.setMaxAmountGroupId(rs.getLong("MAX_LIMIT_GROUP_ID"));
	vo.setActive(rs.getInt("IS_ACTIVE") == 1 ? true : false);

	return vo;
}



private static final int ONE_DAY_PERIOD = 1;
private static final int ONE_WEEK_PERIOD = 2;
private static final int ONE_MONTH_PERIOD = 3;

/**
* Locate and load <code>InvestmentLimit</code> by group and currency.
*
* @param conn the db connection to use
* @param investmentLimitsGroupId the investment limits group
* @param currencyId the currency
* @return The requested <code>InvestmentLimit</code>.
* @throws SQLException
*/
public static InvestmentLimit getInvestmentLimit(Connection conn, long oppTypeId, long oppScheduled, long oppMarketId, long userCurrencyId, long userId) throws SQLException {
  InvestmentLimit il = null;
  PreparedStatement pstmt = null;
  ResultSet rs = null;

  try {
      String sql =
              " SELECT " +
                  " il.*, " +
                  " ilgcmin.value min_amount," +
                  " ilgcmax.value max_amount " +
              " FROM " +
                  " investment_limits il," +
                  " investment_limit_types ilt," +
                  " INVESTMENT_LIMIT_GROUP_CURR ilgcmin, " +
                  " INVESTMENT_LIMIT_GROUP_CURR ilgcmax " +
              " WHERE " +
                  " il.LIMIT_TYPE_ID = ilt.id " +
                  " AND il.MIN_LIMIT_GROUP_ID = ilgcmin.INVESTMENT_LIMIT_GROUP_ID " +
                  " AND ilgcmin.CURRENCY_ID = ? " +
                  " AND il.MAX_LIMIT_GROUP_ID = ilgcmax.INVESTMENT_LIMIT_GROUP_ID " +
                  " AND ilgcmax.CURRENCY_ID = ? " +
                  " AND il.IS_ACTIVE = 1 " +
                  " AND ilt.IS_ACTIVE = 1 " +
                  " AND (il.user_id is null or il.user_id = ?) " +
                  " AND (il.OPPORTUNITY_TYPE_ID is null or il.OPPORTUNITY_TYPE_ID = ?) " +
                  " AND (il.SCHEDULED is null or il.SCHEDULED = ?) " +
                  " AND (il.MARKET_ID is null or il.MARKET_ID = ?) " +
                  " AND (il.START_DATE is null " +
                  	  " or il.END_DATE is null" +
                  	  " or sysdate between il.START_DATE and il.END_DATE) " +
                  " AND (il.START_TIME is null " +
                        " OR il.END_TIME is null " +
                        " OR to_char(sysdate, 'hh24:mi') >= il.START_TIME " +
                        " OR to_char(sysdate, 'hh24:mi') < il.END_TIME) " +
              " ORDER BY " +
              	" ilt.priority ";

      pstmt = conn.prepareStatement(sql);
      pstmt.setLong(1, userCurrencyId);
      pstmt.setLong(2, userCurrencyId);
      pstmt.setLong(3, userId);
      pstmt.setLong(4, oppTypeId);
      pstmt.setLong(5, oppScheduled);
      pstmt.setLong(6, oppMarketId);

      rs = pstmt.executeQuery();
      if (rs.next()) {
          il = getInvestmentLimitVo(rs);
          il.setMinAmount(rs.getLong("min_amount"));
          il.setMaxAmount(rs.getLong("max_amount"));
      }
  } finally {
      closeResultSet(rs);
      closeStatement(pstmt);
  }
  return il;
}

/**
* Locate and load <code>InvestmentLimit</code> by group and currency.
*
* @param conn the db connection to use
* @param investmentLimitsGroupId the investment limits group
* @param currencyId the currency
* @return The requested <code>InvestmentLimit</code>.
* @throws SQLException
*/
public static InvestmentLimit getInvestmentLimit(Connection conn, long investmentLimitsGroupId, long currencyId) throws SQLException {
  InvestmentLimit il = null;
  PreparedStatement pstmt = null;
  ResultSet rs = null;
  try {
      String sql =
              "SELECT " +
                  "B.* " +
              "FROM " +
                  "investment_limit_group A, " +
                  "investment_limits B " +
              "WHERE " +
                  "A.investment_limit_id = B.id AND " +
                  "A.investment_limits_group_id = ? AND " +
                  "B.currency_id = ?";
      pstmt = conn.prepareStatement(sql);
      pstmt.setLong(1, investmentLimitsGroupId);
      pstmt.setLong(2, currencyId);
      rs = pstmt.executeQuery();
      if (rs.next()) {
          il = getInvestmentLimit(rs);
      }
  } finally {
      closeResultSet(rs);
      closeStatement(pstmt);
  }
  return il;
}

/**
* Load the <code>InvestmentLimit</code> VO details from <code>ResultSet</code>.
* The result set should be positioned at the record to load.
*
* @param rs the <code>ResultSet</code> to read the VO details from
* @return New <code>InvestmentLimit</code> VO with the details loaded.
* @throws SQLException
*/
private static InvestmentLimit getInvestmentLimit(ResultSet rs) throws SQLException {
  InvestmentLimit il = new InvestmentLimit();
  il.setId(rs.getLong("id"));
  il.setCurrencyId(rs.getLong("currency_id"));
  il.setMinAmount(rs.getLong("min"));
  il.setMaxAmount(rs.getLong("max"));
  il.setOneTouchMin(rs.getLong("ONE_TOUCH_MIN"));
  return il;
}

/**
* Insert investment.
*
* @param conn the db connection to use
* @param userId the user id
* @param amount the investment amount
* @param level the opp level at the time of invest
* @param ip the ip from which the invest request came from
* @param realLevel the current real level
* @param wwwLevel the level the investment was checked against for acceptable deviation
* @param optionPlusFee the fee when insert investment from option +
* @param writerId TODO
* @param defaultAmountValue the current user default investment amount
* @param slipEntry take the oppId, choice
* @param bonus Odds Change Type Id if to mark the investment as (0 - regular, 1 next invest on us, 2 odds change percent, 3 odds change multiply)
* @param utcOffset the UTC offset of the user when he did the investment (eg. GMT+02:00)
* @param copyopTypeId
* @param copyopInvId
* @return The id of the inserted investment.
* @throws SQLException
*/
public static long insertInvestment(
      Connection conn,
      long userId,
      OpportunityCacheBean opportunity,
      long choice,
      long amount,
      double level,
      String ip,
      double realLevel,
      double wwwLevel,
      long bonusOddsChangeTypeId,
      String utcOffsetCreated,
      double rate,
      long bonusUsersId,
      boolean fromGraph,
      long optionPlusFee,
      long writerId,
      long countryId,
      long apiExternalUserId,
      Long defaultAmountValue,
      double oddsWin,
      double oddsLose,
      long copyopInvId,
      long copyopTypeId,
      boolean isLikeHourly,
      Date bubbleStartTime,
		Date bubbleEndTime,
		double bubbleLowLevel,
		double bubbleHighLevel,
		long loginId,
		double price
		) throws SQLException {
  return insertInvestment(conn,
              userId,
              opportunity.getId(),
              choice,
              amount,
              level,
              ip,
              realLevel,
              wwwLevel,
              bonusOddsChangeTypeId,
              utcOffsetCreated,
              rate,
              bonusUsersId,
              oddsWin,
              oddsLose,
              0,
              0,
              fromGraph,
              optionPlusFee,
              writerId,
              countryId,
              apiExternalUserId,
              defaultAmountValue,
              copyopInvId,
              copyopTypeId,
              isLikeHourly,
              bubbleStartTime,
  			bubbleEndTime,
  			bubbleLowLevel,
  			bubbleHighLevel,
  			loginId,
  			price
  			);
}


/**
* Insert investment.
*
* @param conn the db connection to use
* @param userId the user id
* @param oppId the opp id
* @param choice up or down
* @param amount the investment amount
* @param level the opp level at the time of invest
* @param ip the ip from which the invest request came from
* @param realLevel the current real level
* @param wwwLevel the level the investment was checked against for acceptable deviation
* @param bonusOddsChangeTypeId to mark the investment as bonus odds change type (0 - regular, 1 - free, 2 - odds change percent, 3 - odds change multiply)
* @param overOddsWin odds for win
* @param overOddsLose odds for lose
* @param referenceInvestmentId the is of the investment that we copy the info from (0 if its new investment)
* @param insuranceAmount for roll up insert the insurance amount
* @param optionPlusFee the fee to pay when insert investment from option +
* @param writerId
* @param defaultAmountValue the current user default investment amount
* @param utcOffset the UTC offset of the user when he did the investment (eg. GMT+02:00)
* @param copyopTypeId
* @param copyopInvId
* @return The id of the inserted investment.
* @throws SQLException
*/
public static long insertInvestment(
      Connection conn,
      long userId,
      long oppId,
      long choice,
      long amount,
      double level,
      String ip,
      double realLevel,
      double wwwLevel,
      long bonusOddsChangeTypeId,
      String utcOffsetCreated,
      double rate,
      long bonusUsersId,
      double overOddsWin,
      double overOddsLose,
      long referenceInvestmentId,
      double insuranceAmount,
      boolean fromGraph,
      long optionPlusFee, long writerId,
      long countryId,
      long apiExternalUserId,
      Long defaultAmountValue,
      long copyopInvId,
      long copyopTypeId,
      boolean isLikeHourly,
      Date bubbleStartTime,
		Date bubbleEndTime,
		double bubbleLowLevel,
		double bubbleHighLevel,
		long loginId,
		double price
		) throws SQLException {
  long id = 0;
  PreparedStatement pstmt = null;
  ResultSet rs = null;

  try {
      String sql =
              "INSERT INTO investments " +
                  "(id, user_id, opportunity_id, type_id, amount, current_level, odds_win, odds_lose, writer_id, ip, " +
                  "win, lose, house_result, time_created, is_settled, is_canceled, is_void, real_level, www_level, bonus_odds_change_type_id, " +
                  "utc_offset_created, rate, bonus_user_id, reference_investment_id, insurance_amount_ru, is_from_graph, server_id, option_plus_fee, " +
                  "country_id, group_inv_id, api_external_user_id, def_inv_amount, copyop_inv_id, copyop_type_id, is_like_hourly, bubble_start_time, " +
                  "bubble_end_time, bubble_low_level, bubble_high_level, login_id, price) " +
              "VALUES " +
                  "(SEQ_INVESTMENTS.NEXTVAL, ?, ?, ?, ?, ?, ?, ?, ?, ?, 0, 0, 0, current_date, 0, 0, 0, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

      pstmt = conn.prepareStatement(sql);
      long seqGroupInvId = getSequenceNextVal(conn, "SEQ_INVESTMENTS_GROUP_INV_ID");
      pstmt.setLong(1, userId);
      pstmt.setLong(2, oppId);
      pstmt.setLong(3, choice);
      pstmt.setLong(4, amount);
      pstmt.setDouble(5, level);
      pstmt.setDouble(6, overOddsWin);
      pstmt.setDouble(7, overOddsLose);
      pstmt.setLong(8, writerId);
      pstmt.setString(9, ip);
      pstmt.setDouble(10, realLevel);
      pstmt.setDouble(11, wwwLevel);
      pstmt.setLong(12, bonusOddsChangeTypeId);
      pstmt.setString(13, utcOffsetCreated);
      pstmt.setDouble(14, rate);
      if (bonusOddsChangeTypeId > 0 && bonusUsersId > 0) {
          pstmt.setLong(15, bonusUsersId);
      } else {
          pstmt.setNull(15, Types.NUMERIC);
      }
      pstmt.setLong(16, referenceInvestmentId);
      pstmt.setDouble(17, insuranceAmount);
      pstmt.setBoolean(18, fromGraph);
      pstmt.setString(19, ServerConfiguration.getInstance().getServerName());
      pstmt.setLong(20, optionPlusFee);
      pstmt.setLong(21, countryId);
      pstmt.setLong(22, seqGroupInvId);
      if (apiExternalUserId != 0) {
      	pstmt.setLong(23, apiExternalUserId);
      } else {
          pstmt.setNull(23, Types.NUMERIC);
      }
      pstmt.setLong(24, defaultAmountValue);
      if (copyopInvId == 0) {
          pstmt.setNull(25, Types.NUMERIC);
      } else {
      	pstmt.setLong(25, copyopInvId);
      }
      pstmt.setLong(26, copyopTypeId);
      pstmt.setInt(27, isLikeHourly == true ? 1 : 0);
      
      
      if (bubbleStartTime == null) {
          pstmt.setNull(28, Types.DATE);
      } else {
      	pstmt.setTimestamp(28, CommonUtil.convertToTimeStamp(bubbleStartTime));
      }
      
      if (bubbleEndTime == null) {
          pstmt.setNull(29, Types.DATE);
      } else {
      	pstmt.setTimestamp(29, CommonUtil.convertToTimeStamp(bubbleEndTime));
      }
      
      if (bubbleLowLevel == 0) {
          pstmt.setNull(30, Types.NUMERIC);
      } else {
      	pstmt.setDouble(30, bubbleLowLevel);
      }
      
      if (bubbleHighLevel == 0) {
          pstmt.setNull(31, Types.NUMERIC);
      } else {
      	pstmt.setDouble(31, bubbleHighLevel);
      }
      
      if (loginId == 0) {
          pstmt.setNull(32, Types.NUMERIC);
      } else {
      	pstmt.setLong(32, loginId);
      }

      if (price == 0) {
          pstmt.setNull(33, Types.NUMERIC);
      } else {
      	pstmt.setDouble(33, price);
      }


      pstmt.executeUpdate();

      id = getSeqCurValue(conn, "SEQ_INVESTMENTS");
  } finally {
      closeResultSet(rs);
      closeStatement(pstmt);
  }
  return id;
}



	/**
	* Insert investment.
	*
	* @param conn the db connection to use
	* @param userId the user id
	* @param oppId the opp id
	* @param choice up or down
	* @param amount the investment amount
	* @param level the opp level at the time of invest
	* @param ip the ip from which the invest request came from
	* @param realLevel the current real level
	* @param wwwLevel the level the investment was checked against for acceptable deviation
	* @param bonusOddsChangeTypeId to mark the investment as bonus odds change type (0 - regular, 1 - free, 2 - odds change percent, 3 - odds change multiply)
	* @param overOddsWin odds for win
	* @param overOddsLose odds for lose
	* @param referenceInvestmentId the is of the investment that we copy the info from (0 if its new investment)
	* @param insuranceAmount for roll up insert the insurance amount
	* @param optionPlusFee the fee to pay when insert investment from option +
	* @param writerId
	* @param defaultAmountValue the current user default investment amount
	* @param utcOffset the UTC offset of the user when he did the investment (eg. GMT+02:00)
	* @param copyopTypeId
	* @param copyopInvId
	* @return The id of the inserted investment.
	* @throws SQLException
	*/
	public static long insertInvestment(
	      Connection conn,
	      long userId,
	      long oppId,
	      long choice,
	      long amount,
	      double level,
	      String ip,
	      double realLevel,
	      double wwwLevel,
	      long bonusOddsChangeTypeId,
	      String utcOffsetCreated,
	      double rate,
	      long bonusUsersId,
	      double overOddsWin,
	      double overOddsLose,
	      long referenceInvestmentId,
	      double insuranceAmount,
	      boolean fromGraph,
	      long writerId,
	      long countryId,
	      long apiExternalUserId,
	      Long defaultAmountValue,
	      long copyopInvId,
	      long copyopTypeId,
	      boolean isLikeHourly,
		long loginId) throws SQLException {
			  long id = 0;
			  PreparedStatement pstmt = null;
			  ResultSet rs = null;
			
			  try {
			      String sql =
			              "INSERT INTO investments " +
			                  "(id, user_id, opportunity_id, type_id, amount, current_level, odds_win, odds_lose, writer_id, ip, " +
			                  "win, lose, house_result, time_created, is_settled, is_canceled, is_void, real_level, www_level, bonus_odds_change_type_id, " +
			                  "utc_offset_created, rate, bonus_user_id, reference_investment_id, insurance_amount_ru, is_from_graph, server_id, option_plus_fee, " +
			                  "country_id, group_inv_id, api_external_user_id, def_inv_amount, copyop_inv_id, copyop_type_id, is_like_hourly, bubble_start_time, " +
			                  "bubble_end_time, bubble_low_level, bubble_high_level, login_id) " +
			              "VALUES " +
			                  "(SEQ_INVESTMENTS.NEXTVAL, ?, ?, ?, ?, ?, ?, ?, ?, ?, 0, 0, 0, current_date, 0, 0, 0, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
			
			      pstmt = conn.prepareStatement(sql);
			      long seqGroupInvId = getSequenceNextVal(conn, "SEQ_INVESTMENTS_GROUP_INV_ID");
			      int paramId = 0;
			      pstmt.setLong(paramId++, userId);
			      pstmt.setLong(paramId++, oppId);
			      pstmt.setLong(paramId++, choice);
			      pstmt.setLong(paramId++, amount);
			      pstmt.setDouble(paramId++, level);
			      pstmt.setDouble(paramId++, overOddsWin);
			      pstmt.setDouble(paramId++, overOddsLose);
			      pstmt.setLong(paramId++, writerId);
			      pstmt.setString(paramId++, ip);
			      pstmt.setDouble(paramId++, realLevel);
			      pstmt.setDouble(paramId++, wwwLevel);
			      pstmt.setLong(paramId++, bonusOddsChangeTypeId);
			      pstmt.setString(paramId++, utcOffsetCreated);
			      pstmt.setDouble(paramId++, rate);
			      if (bonusOddsChangeTypeId > 0 && bonusUsersId > 0) {
			          pstmt.setLong(paramId++, bonusUsersId);
			      } else {
			          pstmt.setNull(paramId++, Types.NUMERIC);
			      }
			      pstmt.setLong(paramId++, referenceInvestmentId);
			      pstmt.setDouble(paramId++, insuranceAmount);
			      pstmt.setBoolean(paramId++, fromGraph);
			      pstmt.setString(paramId++, ServerConfiguration.getInstance().getServerName());
			      pstmt.setLong(paramId++, countryId);
			      pstmt.setLong(paramId++, seqGroupInvId);
			      if (apiExternalUserId != 0) {
			      	pstmt.setLong(paramId++, apiExternalUserId);
			      } else {
			          pstmt.setNull(paramId++, Types.NUMERIC);
			      }
			      pstmt.setLong(paramId++, defaultAmountValue);
			      if (copyopInvId == 0) {
			          pstmt.setNull(paramId++, Types.NUMERIC);
			      } else {
			      	pstmt.setLong(paramId++, copyopInvId);
			      }
			      pstmt.setLong(paramId++, copyopTypeId);
			      pstmt.setInt(paramId++, isLikeHourly == true ? 1 : 0);
			    
			      if (loginId == 0) {
			          pstmt.setNull(paramId++, Types.NUMERIC);
			      } else {
			      	pstmt.setLong(paramId++, loginId);
			      }
			
			      pstmt.executeUpdate();
			      id = getSeqCurValue(conn, "SEQ_INVESTMENTS");
			  } finally {
			      closeResultSet(rs);
			      closeStatement(pstmt);
			  }
			  return id;
	}

public static void setInvestmentBonuUsersId(Connection conn, long investmentId, long bonusUsersId) throws SQLException {
  PreparedStatement pstmt = null;
  try{
      String sql =
              "UPDATE " +
                  "investments " +
              "SET " +
                  "bonus_user_id = ? " +
              "WHERE " +
                  "id = ?";
      pstmt = conn.prepareStatement(sql);
      pstmt.setLong(1, bonusUsersId);
      pstmt.setLong(2, investmentId);
      pstmt.executeUpdate();
  } finally {
      closeStatement(pstmt);
  }
}

	public static ArrayList<Investment> getByUser(Connection con, long id, Date from, Date to, boolean isSettled,
			long groupId, long marketId, long skinId, int startRow, int pageSize, long writerId, int period)
			throws SQLException {
		ArrayList<Investment> list = new ArrayList<Investment>();
		CallableStatement cstmt = null;
		ResultSet rs = null;
		int index = 1;
		try {
			cstmt = con.prepareCall("{call pkg_investments.get_by_user_id( o_data => ? " +
																		 " , i_user_id => ? " +
																	     " , i_period => ? " +
																	     " , i_writer_id => ? " +
																	     " , i_fromdate => ? " +
																	     " , i_todate => ? " +
																	     " , i_is_settled => ? " +
																	     " , i_market_id => ? " +
																	     " , i_group_id => ? " +
																	     " , i_skin_id => ? " +
																	     " , i_pagesize => ? " +
																	     " , i_startrow => ? )}" );
			
			cstmt.registerOutParameter(index++, OracleTypes.CURSOR);
			cstmt.setLong(index++, id);
			cstmt.setLong(index++, period);
			cstmt.setLong(index++, writerId);
			cstmt.setTimestamp(index++, convertToTimestamp(from));
			cstmt.setTimestamp(index++, convertToTimestamp(to));
			cstmt.setBoolean(index++, isSettled);
			if (marketId == 0) {
				cstmt.setNull(index++, OracleTypes.NUMBER);
			} else {
				cstmt.setLong(index++, marketId);
			}
			if (groupId == 0) {
				cstmt.setNull(index++, OracleTypes.NUMBER);
			} else {
				cstmt.setLong(index++, groupId);
			}
			cstmt.setLong(index++, skinId);
			if (pageSize == 0) {
				cstmt.setNull(index++, OracleTypes.NUMBER);
			} else {
				cstmt.setLong(index++, pageSize);
			}
			cstmt.setLong(index++, startRow);
			
			cstmt.execute();
			
			rs = (ResultSet) cstmt.getObject(1);

			Locale locale = new Locale(LanguagesManagerBase
					.getLanguage(SkinsManagerBase.getSkin(skinId).getDefaultLanguageId()).getCode());
			while (rs.next()) {
				Investment i = new Investment();
				i = getVOExtended(con, rs);
				i.setBonusDisplayName(rs.getString("bonus_display_name"));
				i.setOpportunityTypeId(rs.getLong("opportunity_type_id"));
				i.setOpenGraph(rs.getBoolean("is_open_graph"));
				i.setHaveHourly(rs.getLong("hasHourly") == 1);
				setAdditionalInfo(i, locale, isSettled);
				list.add(i);
			}
		} finally {
			closeResultSet(rs);
			closeStatement(cstmt);
		}
		return list;
	}

//protected static Investment getVO(ResultSet rs) throws SQLException {
//  Investment vo = new Investment();
//  vo.setId(rs.getLong("id"));
//  vo.setUserId(rs.getLong("user_id"));
//  vo.setOpportunityId(rs.getLong("opportunity_Id"));
//  vo.setTypeId(rs.getLong("type_Id"));
//  vo.setTimeCreated(convertToDate(rs.getTimestamp("time_created")));
//  vo.setAmount(rs.getLong("amount"));
//  vo.setCurrentLevel(rs.getDouble("current_level"));
//  vo.setRealLevel(rs.getDouble("real_level"));
//  vo.setWwwLevel(rs.getBigDecimal("www_level"));
//  vo.setOddsWin(rs.getDouble("odds_Win"));
//  vo.setOddsLose(rs.getDouble("odds_Lose"));
//  vo.setWriterId(rs.getLong("writer_id"));
//  vo.setCanceledWriterId(rs.getBigDecimal("canceled_writer_id"));
//  vo.setIp(rs.getString("ip"));
//  vo.setWin(rs.getLong("win"));
//  vo.setLose(rs.getLong("lose"));
//  vo.setHouseResult(rs.getLong("house_Result"));
//  vo.setTimeSettled(convertToDate(rs.getTimestamp("time_settled")));
//  vo.setIsSettled(rs.getInt("is_settled"));
//  vo.setIsCanceled(rs.getInt("is_canceled"));
//  vo.setTimeCanceled(convertToDate(rs.getTimestamp("time_canceled")));
//  vo.setIsVoid(rs.getInt("is_void"));
//  //vo.setIsFree(rs.getInt("is_free"));
//  vo.setUtcOffsetCreated(rs.getString("utc_offset_created"));
//  vo.setUtcOffsetSettled(rs.getString("utc_offset_settled"));
//  vo.setUtcOffsetCancelled(rs.getString("utc_offset_cancelled"));
//  vo.setInsuranceAmountGM(rs.getLong("insurance_amount_gm"));
//  vo.setInsuranceAmountRU(rs.getLong("insurance_amount_ru"));
//  vo.setReferenceInvestmentId(rs.getLong("REFERENCE_INVESTMENT_ID"));
//  vo.setRate(rs.getDouble("rate"));
//  vo.setInsuranceFlag(rs.getInt("insurance_flag"));
//  if (rs.wasNull()) { //check if insurance flag was null in db if yes set it to null
//  	vo.setInsuranceFlag(null);
//  }
//  vo.setBonusOddsChangeTypeId(rs.getInt("bonus_odds_change_type_id"));
//  vo.setAcceptedSms(rs.getLong("is_accepted_sms") == 1 ? true : false);
//  vo.setOptionPlusFee(rs.getLong("option_plus_fee"));
//  vo.setDefaultAmountValue(rs.getLong("def_inv_amount"));
//  vo.setCopyopInvId(rs.getLong("copyop_inv_id"));
//  vo.setCopyopType(CopyOpInvTypeEnum.of(rs.getInt("copyop_type_id")));
//  vo.setLikeHourly(rs.getLong("is_like_hourly") == 1 ? true : false);
//  vo.setBubbleStartTime(convertToDate(rs.getTimestamp("bubble_start_time")));
//  vo.setBubbleEndTime(convertToDate(rs.getTimestamp("bubble_end_time")));
//  vo.setBubbleLowLevel(rs.getDouble("bubble_low_level"));
//  vo.setBubbleHighLevel(rs.getDouble("bubble_high_level"));
//  return vo;
//}

protected static Investment getVOExtended(Connection conn, ResultSet rs) throws SQLException {
  Investment i = getVO(rs);
  i.setMarketName(rs.getString("market_name"));
  i.setMarketId(rs.getLong("market_id"));
  i.setTypeName(rs.getString("type_name"));
  i.setClosingLevel(rs.getDouble("closing_level"));
  i.setTimeEstClosing(getTimeWithTimezone(rs.getString("time_est_closing")));
  i.setTimeLastInvest(getTimeWithTimezone(rs.getString("time_last_invest")));
  i.setTimeFirstInvest(getTimeWithTimezone(rs.getString("time_first_invest")));
  i.setUtcOffsetEstClosing(rs.getString("utc_offset_created"));
  i.setEventLevel(rs.getDouble("event_level"));
  if (rs.getString("time_act_closing") != null) {
      i.setTimeActClosing(getTimeWithTimezone(rs.getString("time_act_closing")));
      if (null != rs.getString("utc_offset_settled")) {
          i.setUtcOffsetEstClosing(rs.getString("utc_offset_settled")); //the offset of time_act_closing of the opportunity is equal to offset of time_settled of the investment
      }
  }
  i.setCopyopType(CopyOpInvTypeEnum.of(rs.getInt("copyop_type_id")));
  i.setScheduledId(rs.getLong("scheduled"));
  i.setUserName(rs.getString("username"));
  i.setUserFirstName(rs.getString("firstname"));
  i.setUserLastName(rs.getString("lastname"));
  i.setCurrencyId(rs.getLong("currency_id"));

  // set up_down field for one touch investments
  if (i.getTypeId() == Investment.INVESTMENT_TYPE_ONE) {
      OneTouchFields oneTouchFields = getOneTouchFields(conn, i.getOpportunityId());
      i.setOneTouchDecimalPoint(oneTouchFields.getDecimalPoint());
      i.setOneTouchUpDown(oneTouchFields.getUpDown());
  }
  if(i.getTypeId() == Investment.INVESTMENT_TYPE_BUBBLES) {
	  i.setClosingLevel(rs.getDouble("bubbles_closing_level"));
  }
  
  i.setRolledInvId(rs.getLong("rolled_inv_id"));
  return i;
}

public static OneTouchFields getOneTouchFields(Connection conn, long oppId) throws SQLException {
  OneTouchFields retVal = new OneTouchFields();
  PreparedStatement pstmt = null;
  ResultSet rs = null;
  try {
      String sql =
          "SELECT " +
              "op.decimal_point, " +
              "op.up_down " +
          "FROM " +
              "opportunities o, " +
              "opportunity_one_touch op " +
          "WHERE " +
              "o.id = ? AND " +
              "o.id = op.opportunity_id AND " +
              "o.opportunity_type_id = ?";
      pstmt = conn.prepareStatement(sql);
      pstmt.setLong(1, oppId);
      pstmt.setLong(2, ConstantsBase.OPPORTUNITIES_TYPE_ONE_TOUCH);
      rs = pstmt.executeQuery();
      if (rs.next()) {
          retVal.setDecimalPoint(rs.getInt("decimal_point"));
          retVal.setUpDown(rs.getInt("up_down"));
      }
  } finally {
      closeResultSet(rs);
      closeStatement(pstmt);
  }
  return retVal;
}

/**
* Load todays opened investments for specified user. Only the fields needed for the trading page
* are loaded (market name, investment type, current level, est close time, market_id).
*
* @param conn
* @param userId
* @return <code>ArrayList<Investment></code>
* @throws SQLException
*/
public static ArrayList<Investment> getTodaysOpenedInvestments(Connection conn, long userId, String userOffset) throws SQLException {
  ArrayList<Investment> l = new ArrayList<Investment>();
  PreparedStatement pstmt = null;
  ResultSet rs = null;
  try {
      String sql =
              "SELECT " +
                  "A.id, " +
                  "C.display_name, " +
                  "A.type_id, " +
                  "A.current_level, " +
                  "A.utc_offset_created, "+
                  "to_char(B.time_est_closing, 'YYYY-MM-DD HH24:MI:SS.FF3 TZHTZM') AS time_est_closing, " +
                  "B.market_id, " +
                  "A.amount, " +
                  "u.currency_id, " +
                  "A.opportunity_id, " +
                  "B.scheduled, " +
                  "B.is_open_graph " +
              "FROM " +
                  "investments A, " +
                  "opportunities B, " +
                  "markets C, " +
                  "users u " +
              "WHERE " +
                  "A.opportunity_id = B.id AND " +
                  "B.market_id = C.id AND " +
                  "A.user_id = ? AND " +
                  "trunc(B.time_est_closing) = trunc(current_date) AND " +
                  "A.is_settled = 0 AND " +
                  "A.is_canceled = 0 AND " +
                  "u.id = A.user_id AND " +
                  "A.is_void = 0 " +
              "ORDER BY " +
                  "B.time_est_closing, " +
                  "A.time_created";
      pstmt = conn.prepareStatement(sql);
      pstmt.setLong(1, userId);
      rs = pstmt.executeQuery();
      SimpleDateFormat localDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS Z");
      int cnt = 0;
      while (rs.next() && cnt < 6) { // Oracle LIMIT clause (LOL)
          Investment i = new Investment();
          i.setId(rs.getLong("id"));
          i.setMarketName(rs.getString("display_name") + ".short");
          i.setTypeId(rs.getLong("type_id"));
          i.setCurrentLevel(rs.getDouble("current_level"));
          i.setCurrencyId(rs.getLong("currency_id"));
          i.setUtcOffsetCreated(rs.getString("utc_offset_created"));
          try {
              i.setTimeEstClosing(localDateFormat.parse(rs.getString("time_est_closing")));
              i.setUtcOffsetEstClosing(userOffset);
          } catch (Throwable t) {
              log.error("Can't parse time est closing.", t);
          }

          if (i.getTypeId() == Investment.INVESTMENT_TYPE_ONE ) {
				OneTouchFields oneTouchFields = InvestmentsDAOBase.getOneTouchFields(conn, Integer.parseInt(rs.getString("opportunity_id")));
				i.setOneTouchDecimalPoint(oneTouchFields.getDecimalPoint());
				i.setOneTouchUpDown(oneTouchFields.getUpDown());
			}
          i.setMarketId(rs.getLong("market_id"));
          i.setAmount(rs.getLong("amount"));
          i.setScheduledId(rs.getLong("scheduled"));
          i.setOpenGraph(rs.getInt("is_open_graph")==1);
          i.setOpportunityId(rs.getLong("opportunity_id"));
          l.add(i);
          cnt++;
      }
  } finally {
      closeResultSet(rs);
      closeStatement(pstmt);
  }
  return l;
}


/**
* List all the investments done on opportunity that is closed and are not
* settled, canceled or void.
*
* @param conn the db connection to use
* @param investmentId the id of the investment to load
* @param oppSettled true if u want to get the investment only if the opp is closed, false if u want also not closed opp (use for 5 golden minutes)
* @param isGM if its take profit (GM) true else false
* @return
* @throws SQLException
*/
public static Investment getInvestmentToSettle(Connection conn, long investmentId, boolean oppSettled, boolean isGM) throws SQLException {
  Investment i = null;
  PreparedStatement pstmt = null;
  ResultSet rs = null;
  try {
      String sql =
              "SELECT " +
                  "A.*, ";
                  if (isGM) { //if we want to settle take profit investment (GM) we should set the closing level from opportunity.gm_level
                      sql += "B.gm_level as closing_level, ";
                  } else {
                      sql += "B.closing_level, ";
                  }
           sql += "M.id as market_id, " +
                  "M.insurance_premia_percent, " +
                  "M.roll_up_premia_percent, " +
                  "M.display_name m_display_name, " +
                  "M.decimal_point m_decimal_point, " +
                  "B.opportunity_type_id, " + 
                  "it.display_name i_type_name, " +
                  "to_char(B.time_est_closing, 'YYYY-MM-DD HH24:MI:SS.FF3 TZHTZM') AS time_est_closing, " +
                  "bu.odds_win as bonus_odds_win, " +
                  "bu.odds_lose as bonus_odds_lose " +
              "FROM " +
                  "investments A left join bonus_users bu on A.bonus_user_id = bu.id, " +
                  "opportunities B, " +
                  "markets M, " +
                  "investment_types it " +
              "WHERE " +
                  "A.opportunity_id = B.id AND "+
                  "M.id = B.market_id AND " +
                  "A.type_id = it.id AND ";
      if (oppSettled) {
          sql +=  "NOT ";
      }
      sql +=      "B.time_act_closing IS NULL AND " +
                  "A.is_settled = 0 AND " +
                  "A.is_canceled = 0 AND " +
                  "A.is_void = 0 AND " +
                  "A.id = ?";
      pstmt = conn.prepareStatement(sql);
      pstmt.setLong(1, investmentId);
      rs = pstmt.executeQuery();
      while(rs.next()) {
          i = getVO(rs);
          i.setClosingLevel(rs.getDouble("closing_level"));
          i.setBonusUserId(rs.getLong("bonus_user_id"));
          i.setMarketId(rs.getLong("market_id"));
          i.setInsurancePremiaPercent(rs.getDouble("insurance_premia_percent"));
          i.setRollUpPremiaPercent(rs.getDouble("roll_up_premia_percent"));
          i.setMarketName(rs.getString("m_display_name"));
          i.setDecimalPoint(rs.getLong("m_decimal_point"));
          i.setTypeName(rs.getString("i_type_name"));
          i.setBonusLoseOdds(rs.getDouble("bonus_odds_lose"));
          i.setBonusWinOdds(rs.getDouble("bonus_odds_win"));
          i.setCopyOpInvId(rs.getLong("copyop_inv_id"));
          i.setCopyopType(CopyOpInvTypeEnum.of(rs.getInt("copyop_type_id")));
          i.setOpportunityTypeId(rs.getLong("opportunity_type_id"));
          SimpleDateFormat localDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS Z");
          try {
				i.setTimeEstClosing(localDateFormat.parse(rs.getString("time_est_closing")));
			} catch (ParseException e) {
				log.error("Problem parsing timeEstClosing! " + e);
			}

      }
  } finally {
      closeResultSet(rs);
      closeStatement(pstmt);
  }
  return i;
}

public static double getOptionsPlusePrice(Connection conn, double promil, double minPass, long marketId) throws SQLException {
  double price = 0;
  PreparedStatement pstmt = null;
  ResultSet rs = null;
  try {
      String sql =
          "SELECT " +
              "opp.price " +
          "FROM " +
              "options_plus_prices opp, " +
              "options_plus_minutes_range mi, " +
              "options_plus_promile_range pr " +
          "WHERE " +
              "opp.minutes_range_id = mi.id AND " +
              "mi.market_id = ? AND " +
              "? between mi.min_minutes AND mi.max_minutes AND " +
              "opp.promile_range_id = pr.id AND " +
              "pr.market_id = ? AND " +
              "? between pr.min_promile AND pr.max_promile";

      pstmt = conn.prepareStatement(sql);
      pstmt.setLong(1, marketId);
      pstmt.setDouble(2, minPass);
      pstmt.setLong(3, marketId);
      pstmt.setDouble(4, promil);

      rs = pstmt.executeQuery();
      if (rs.next()) {
          price = rs.getDouble("price");
      }
  } finally {
      closeResultSet(rs);
      closeStatement(pstmt);
  }
  return price;
}

public static ArrayList<Investment> getOpenedByUser(Connection con, long userId, long opportunityId, long typeId, long marketId) throws SQLException {
	PreparedStatement pstmt = null;
	ResultSet rs = null;
	ArrayList<Investment> list = new ArrayList<Investment>();
	try {
		String sql =
				"SELECT " +
                    "i.*, " +
                    "m.display_name market_name, " +
                    "m.id market_id, " +
                    "int.display_name type_name, " +
                    "u.user_name username, " +
                    "u.first_name firstname, " +
                    "u.last_name lastname, " +
                    "to_char(o.time_est_closing, 'YYYY-MM-DD HH24:MI:SS.FF3 TZHTZM') AS time_est_closing, " +
                    "to_char(o.time_last_invest, 'YYYY-MM-DD HH24:MI:SS.FF3 TZHTZM') AS time_last_invest, " +
                    "to_char(o.time_act_closing, 'YYYY-MM-DD HH24:MI:SS.FF3 TZHTZM') AS time_act_closing, " +
                    "to_char(o.time_first_invest, 'YYYY-MM-DD HH24:MI:SS.FF3 TZHTZM') AS time_first_invest, " +
                    "o.closing_level, " +
                    "o.scheduled scheduled, " +
                    "u.CURRENCY_ID, " +
                    "iref.d rolled_inv_id, " +
                    "o.current_level AS event_level " +
                "FROM " +
                    "investments i, " +
                    "opportunities o, " +
                    "markets m, " +
                    "investment_types int, " +
                    "users u, " +
                    "(select " +
                    "   i2.id as d, " +
                    "   i2.REFERENCE_INVESTMENT_ID as r " +
                    "from " +
                    "   investments i2 " +
                    "where " +
                    "   i2.REFERENCE_INVESTMENT_ID > 0 and " +
                    "   i2.user_id = ?) iref " +
				"WHERE " +
				    "i.type_id = int.id " +
                    "AND u.id = i.user_id " +
					"AND i.user_id = ? " +
					"AND i.opportunity_id = o.id " +
                    "AND o.market_id = m.id " +
                    "AND i.id = iref.r(+) " +
				    "AND i.is_settled = 0 " +
				    "AND o.is_published = 1 " + // only not settled investments
				    (typeId > 0 ? "AND o.opportunity_type_id = ? " : "" ) +
                    (opportunityId > 0 ? "AND i.opportunity_id = ? " : "") +
                    (marketId > 0 ? "AND market_id = ? " : "") +
				"ORDER BY " +
                    "i.time_created";
		pstmt = con.prepareStatement(sql);
		pstmt.setLong(1, userId);
        pstmt.setLong(2, userId);
        int indx = 3;
        if(typeId > 0 ) {
        	pstmt.setLong(indx++, typeId);
        }
        if (opportunityId > 0) {
            pstmt.setLong(indx++, opportunityId);
        }
        if (marketId > 0) {
        	pstmt.setLong(indx++, marketId);
        }
		rs = pstmt.executeQuery();
		while(rs.next()) {
			list.add(getVOExtended(con, rs));
		}
	} finally {
		closeResultSet(rs);
		closeStatement(pstmt);
	}
	return list;
}

public static void insertQuote(Connection conn, long userId, long investmentId, Date timeQuoted, double price, double minPromile, double maxPromile, double minMinutes, double maxMinutes, double level) throws SQLException {
	PreparedStatement pstmt = null;
	try {
		String sql =
	        " INSERT INTO " +
	            " OPTIONS_PLUS_QUOTES(ID,TIME_CREATED,INVESTMENT_ID,TIME_QUOTED,PRICE,MIN_PROMILE,MAX_PROMILE,MIN_MINUTES,MAX_MINUTES,QUOTE_LEVEL) " +
          " VALUES (SEQ_OPTIONS_PLUS_QUOTES.NEXTVAL,sysdate,?,?,?,?,?,?,?,?) ";
	pstmt = conn.prepareStatement(sql);
	pstmt.setLong(1, investmentId);
	pstmt.setTimestamp(2, CommonUtil.convertToTimeStamp(timeQuoted));
	pstmt.setDouble(3, price);
	pstmt.setDouble(4, minPromile);
	pstmt.setDouble(5, maxPromile);
	pstmt.setDouble(6, minMinutes);
	pstmt.setDouble(7, maxMinutes);
	pstmt.setDouble(8, level);
	pstmt.executeUpdate();
	} finally {
		closeStatement(pstmt);
	}
}

public static double calcOptionsPlusePrice(Connection conn, double promil, double minPass, long marketId, long userId, long investmentId, Date timeQuoted, BigDecimal invAmount, double level) throws SQLException {
  double price = 0;
  PreparedStatement pstmt = null;
  ResultSet rs = null;
  try {
      String sql =
          " SELECT " +
              " opp.price price_percent, " +
              " pr.min_promile, " +
              " pr.max_promile, " +
              " mi.min_minutes, " +
              " mi.max_minutes " +
          " FROM " +
              " options_plus_prices opp, " +
              " options_plus_minutes_range mi, " +
              " options_plus_promile_range pr " +
          " WHERE " +
              " opp.minutes_range_id = mi.id  " +
              " AND mi.market_id = ? " +
              " AND ? between mi.min_minutes AND mi.max_minutes " +
              " AND opp.promile_range_id = pr.id " +
              " AND pr.market_id = ? " +
              " AND ? between pr.min_promile AND pr.max_promile ";

      pstmt = conn.prepareStatement(sql);
      pstmt.setLong(1, marketId);
      pstmt.setDouble(2, minPass);
      pstmt.setLong(3, marketId);
      pstmt.setDouble(4, promil);

      rs = pstmt.executeQuery();
      if (rs.next()) {
      	double pricePercent = rs.getDouble("price_percent");

          Random r = new Random();
          double rnd = (r.nextDouble() - 0.5);
          BigDecimal bRnd = new BigDecimal(String.valueOf(rnd)).setScale(1, RoundingMode.HALF_UP);
          log.debug("option plus random is : " + bRnd.doubleValue());
      	BigDecimal p = (new BigDecimal(String.valueOf(pricePercent)).multiply(invAmount)).add(bRnd);
          price = p.round(new MathContext(p.precision() - p.scale() + 2, RoundingMode.HALF_UP)).doubleValue();

          insertQuote(conn, userId, investmentId, timeQuoted, price,
          		rs.getLong("min_promile"), rs.getLong("max_promile"),
          		rs.getLong("min_minutes"), rs.getLong("max_minutes"), level);
      }
  } finally {
      closeResultSet(rs);
      closeStatement(pstmt);
  }
  return price;
}

public static void updatePurchasedQuote(Connection conn, long quoteId) throws SQLException {
	PreparedStatement pstmt = null;
	try {
		String sql =
		        " Update " +
		            " OPTIONS_PLUS_QUOTES opq " +
              " Set " +
              	" opq.IS_PURCHASED = 1 " +
              " Where " +
              	" opq.id = ? ";
		pstmt = conn.prepareStatement(sql);
		pstmt.setLong(1, quoteId);
		pstmt.executeUpdate();
	} finally {
		closeStatement(pstmt);
	}
}

/**
* Update the user balances when settle investment.
*
* @param conn
*            the db connection to use
* @param userId
*            the id of the user who did the investment
* @param result
*            the result of the investment - the amount in cents won/lost
*            from this investment(to be added to/taken from the balance).
*            Positive amount means winning, negative losing. If a positive
*            amount is passed it is expected to include the invested amount
*            also (not net win).
* @param tax
*            the tax balance amount after the settlement
* @throws SQLException
*/
public static void settleInvestmentUpdateUserBalances(Connection conn, long userId, long result, long tax, long winLose) throws SQLException {
	PreparedStatement pstmt = null;
	try {
		String sql =
              "UPDATE " +
                  "users " +
              "SET " +
                  "tax_balance = CASE WHEN tax_exemption = 1 THEN 0 ELSE ? END, " +
                  "balance = balance + tax_balance + ? - CASE WHEN tax_exemption = 1 THEN 0 ELSE ? END, " +
                  "win_lose = ? " +
              "WHERE " +
                  "id = ?";
		pstmt = conn.prepareStatement(sql);
		pstmt.setLong(1, tax);
		pstmt.setLong(2, result);
      pstmt.setLong(3, tax);
      pstmt.setLong(4, winLose);
		pstmt.setLong(5, userId);
		pstmt.executeUpdate();
	} finally {
		closeStatement(pstmt);
	}
}

	/**
	 * Return whether a one touch opportunity related to this investmentis up or down
	 * 
	 * @param con - the connection string
	 * @param investmentId - the investment id
	 * @return 1 - up, 0 - down, -1 - not one touch
	 */
	public static long getOneTouchInvestmentUpDown(Connection con, long investmentId) throws SQLException {
		long isUpDown = -1;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			String sql = "SELECT "+ "up_down " + "FROM " + "investments i, opportunity_one_touch oot " + "WHERE "
							+ "i.opportunity_id = oot.opportunity_id AND " + "i.id = ?";
			pstmt = con.prepareStatement(sql);
			pstmt.setLong(1, investmentId);
			rs = pstmt.executeQuery();
			if (rs.next()) {
				isUpDown = rs.getLong("up_down");
			}
		} finally {
			closeResultSet(rs);
			closeStatement(pstmt);
		}
		return isUpDown;
	}

	/**
	 * Update an investment to settled state.
	 *
	 * @param conn the db connection to use
	 * @param userId the id of the user who did the investment
	 * @param investmentId the id of the investment to settle
	 * @param result the result of the investment - the amount in cents won/lost from this investment. Positive amount means winning,
	 *            negative losing. If a positive amount is passed it is expected to include the invested amount also (not net win).
	 * @param netResult the clean win/lose from this investment in cents (if win it should not include the amount invested only the profit)
	 * @param closingLevel
	 * @param insuranceAmount gm insurance amount
	 * @throws SQLException
	 */
	public static void settleInvestment(Connection conn, long userId, long investmentId, long result, boolean voidBet,
										long insuranceAmountGM, long win, long lose, Double closingLevel, Date timeQuoted, Date now,
										boolean shouldUpdateTimeSettled) throws SQLException {
		PreparedStatement pstmt = null;
		try {
			String sql =
			        " UPDATE " +
			            " investments i " +
	                " SET " +
	                    " i.win = ?, " +
	                    " i.lose = ?, " +
	                    " i.house_result = ?, " +
	                    " i.utc_offset_settled = (select utc_offset from users u where i.user_id = u.id), " + //utc_offset is taken from the user of the investment
	                    " i.is_settled = 1, " +
	                    " i.is_void = ?, " +
	                    " i.insurance_amount_gm = ?, " +
	                    " i.amount = i.amount + ?, " +    // Add insuranceAmountGM for GM investments(if it's not GM it will be amount + 0)
	                    " i.settled_level = ?, " +
	                    " i.time_quoted = ? ";
	       if (shouldUpdateTimeSettled) {
	             sql += " ,i.time_settled = ?, " +
		                " i.time_settled_year = EXTRACT(YEAR FROM GET_TIME_BY_PERIOD(?)), " +
		                " i.time_settled_month = EXTRACT(MONTH FROM GET_TIME_BY_PERIOD(?)) ";
	       }
	             sql += " WHERE " +
	                    " i.id = ?"
	                    + "and i.is_settled = 0";
			int i = 1;
			pstmt = conn.prepareStatement(sql);
			pstmt.setLong(i++, win);
			pstmt.setLong(i++, lose);
			pstmt.setLong(i++, -(win - lose));
			pstmt.setBoolean(i++, voidBet);
			pstmt.setDouble(i++, insuranceAmountGM);
			pstmt.setLong(i++, insuranceAmountGM);
			pstmt.setDouble(i++, closingLevel);
			pstmt.setTimestamp(i++, CommonUtil.convertToTimeStamp(timeQuoted));
			if (shouldUpdateTimeSettled) {
				Timestamp t = CommonUtil.convertToTimeStamp(null != now ? now : new Date());
				pstmt.setTimestamp(i++, t);
				pstmt.setTimestamp(i++, t);
				pstmt.setTimestamp(i++, t);
			}
			pstmt.setLong(i++, investmentId);
			if (log.isDebugEnabled()) {
				String ls = System.getProperty("line.separator");
				log.debug(ls + 
				          "SettleInvestment SQL: " + sql + ls
				          	+ "Values: " + ls
				          	+ "win: " + win + ls
				          	+ "lose: " + lose + ls
							+ "houseResult: " + -(win - lose) + ls
							+ "is_void: " + (voidBet) + ls
							+ "investment id: " + investmentId + ls
							+ "insurance Amount GM: " + insuranceAmountGM + ls);
			}
			int updateCount = pstmt.executeUpdate();
			if (updateCount == 0) {
				throw new SQLException("No rows updated! Probably the user tries to settle investment twice");
			}
		} finally {
			closeStatement(pstmt);
		}
	}

public static List<Investment> getDetails(Connection con, List<Long> ids, int investmentLimit) throws SQLException {
	PreparedStatement ps = null;
	ResultSet rs = null;
	SimpleDateFormat tzDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS Z");
	List<Investment> result = new ArrayList<Investment>();
	if (ids.isEmpty()) {
		return result;
	}
	try {
		String sql =
			"SELECT " +
			    "i.*, " +
			    "o.market_id, " +
			    "int.display_name type_name, " +
			    "o.closing_level, " +
			    "m.display_name market_name, " +
			    "to_char(o.time_act_closing, 'YYYY-MM-DD HH24:MI:SS.FF3 TZHTZM') AS time_act_closing, " +
			    "to_char(o.time_est_closing, 'YYYY-MM-DD HH24:MI:SS.FF3 TZHTZM') AS time_est_closing, " +
			    "o.scheduled scheduled, " +
			    "u.currency_id currency_id, " +
			    "u.utc_offset utc_offset, " +
			    "m.decimal_point, " +
              "iref.d rolled_inv_id, " +
			    "o.opportunity_type_id " +
			"FROM " +
			    "investments i, " +
			    "opportunities o, " +
			    "markets m, " +
			    "investment_types int, " +
			    "users u, " +
              "(select " +
              "   i2.id as d, " +
              "   i2.REFERENCE_INVESTMENT_ID as r " +
              "from " +
              "   investments i2 " +
              "where " +
              "   i2.REFERENCE_INVESTMENT_ID > 0) iref " +
          "WHERE " +
              "i.user_id = u.id AND " +
//              "i.id = ? AND " +
              "i.opportunity_id = o.id AND " +
              "o.market_id = m.id AND " +
              "i.type_id = int.id AND " +
              "i.id = iref.r(+) AND " +
				"i.id IN (";
		for (int i = 1; i < investmentLimit + 1; i++) {
			sql += "?,";
		}
		sql = sql.substring(0, sql.length() - 1);
		sql += ")";
		ps = con.prepareStatement(sql);
		long id = 0L;
		int index = 1;
		Iterator<Long> iterator = ids.iterator();
		while (iterator.hasNext()) {
			id = iterator.next();
			ps.setLong(index++, id);
		}
		while (index < (investmentLimit + 1)) {
			ps.setLong(index++, id);
		}
		rs = ps.executeQuery();
		while (rs.next()) {
			Investment i = getVO(rs);
			i.setScheduledId(rs.getLong("scheduled"));
			i.setMarketId(rs.getLong("market_id"));
          i.setDecimalPoint(rs.getLong("decimal_point"));
			i.setClosingLevel(rs.getDouble("closing_Level"));
			i.setTypeName(rs.getString("type_name"));
			if (rs.getString("time_act_closing") != null) {
				i.setTimeActClosing(tzDateFormat.parse(rs.getString("time_act_closing")));
				i.setUtcOffsetActClosing(rs.getString("utc_offset"));
			}
			if (rs.getString("time_est_closing") != null) {
				i.setTimeEstClosing(tzDateFormat.parse(rs.getString("time_est_closing")));
				i.setUtcOffsetEstClosing(rs.getString("utc_offset"));
			}
			i.setMarketName(rs.getString("market_name"));

			// set up_down field for one touch investments
			if ( i.getTypeId() == Investment.INVESTMENT_TYPE_ONE ) {
				int upDown = InvestmentsDAOBase.getOneTouchFields(con, i.getOpportunityId()).getUpDown();
				i.setOneTouchUpDown(upDown);
			}
          i.setRolledInvId(rs.getLong("rolled_inv_id"));
          i.setCurrencyId(rs.getLong("currency_id"));
			i.setOpportunityTypeId(rs.getLong("opportunity_type_id"));
			result.add(i);
		}
	} catch (ParseException e) {
		log.debug(e.toString());
	} finally {
		closeResultSet(rs);
		closeStatement(ps);
	}
	return result;
}

/**
* Check if user have investments
*
* @param con
* @param userId
* @param oneTouch
* @return
* @throws SQLException
*/
public static boolean hasInvestments(Connection con, long userId, boolean oneTouch) throws SQLException {
  PreparedStatement pstmt = null;
  ResultSet rs = null;
  try {
      String sql ="SELECT " +
                      "count(*) num_inv " +
                  "FROM " +
                      "investments " +
                  "WHERE " +
                      "user_id=" + userId;

      pstmt = con.prepareStatement(sql);
      rs = pstmt.executeQuery();
      if (rs.next()) {
          if (oneTouch && rs.getLong("num_inv") > 1) {
              return true;
          } else if (!oneTouch && rs.getLong("num_inv") > 0) {
              return true;
          }
      }
  } finally {
      closeResultSet(rs);
      closeStatement(pstmt);
  }
  return false;
}


public static void setAdditionalInfo(Investment i, Locale locale, boolean isSettled) {
  //Additional info
  if (isSettled){
  	if (InvestmentsManagerBase.isGmBought(i)) {
			i.setAdditionalInfoText(CommonUtil.getMessage(locale, "investments.goldenMinutes1", null)+ " "
									+ CommonUtil.formatCurrencyAmountAO(InvestmentsManagerBase.getAmountWithoutFees(i), i.getCurrencyId())
									+ " " + CommonUtil.getMessage(locale, "investments.goldenMinutes2", null) + " "
									+ CommonUtil.formatCurrencyAmountAO(i.getInsuranceAmountGM(), i.getCurrencyId()) + " "
									+ CommonUtil.getMessage(locale, "investments.goldenMinutes3", null));
  		i.setAdditionalInfoText(i.getAdditionalInfoText().replace("<br />", " "));
  		i.setAdditionalInfoType(Investment.INVESTMENT_ADDITIONAL_INFO_GM);
  	} else if (InvestmentsManagerBase.isRolledUp(i)) {
  		i.setAdditionalInfoText(CommonUtil.getMessage(locale, "investments.rolledUp", null));
  		i.setAdditionalInfoText(i.getAdditionalInfoText().replace("<br />", " "));
  		i.setAdditionalInfoType(Investment.INVESTMENT_ADDITIONAL_INFO_RU);
  	}
  }
  if (InvestmentsManagerBase.isRollUpBought(i)) {
		i.setAdditionalInfoText(CommonUtil.getMessage(locale, "investments.rollUpBought1", null)+ " "
								+ CommonUtil.formatCurrencyAmountAO(InvestmentsManagerBase.getAmountWithoutFees(i), i.getCurrencyId())
								+ " " + CommonUtil.getMessage(locale, "investments.rollUpBought2", null) + " "
								+ CommonUtil.formatCurrencyAmountAO(i.getInsuranceAmountRU(), i.getCurrencyId()));
		i.setAdditionalInfoText(i.getAdditionalInfoText().replace("<br />", " "));
		i.setAdditionalInfoType(Investment.INVESTMENT_ADDITIONAL_INFO_RU_BOUGHT);
	}
	if (i.getBonusOddsChangeTypeId() > 0) {
		i.setAdditionalInfoText(CommonUtil.getMessage(locale, "bonus.toolTip", null) + " " +
				CommonUtil.getMessage(locale, "bonus.toolTip", null)  + " " + InvestmentsManagerBase.getBonusName(i, locale));
		i.setAdditionalInfoText(i.getAdditionalInfoText().replace("<br />", " "));
		i.setAdditionalInfoType(Investment.INVESTMENT_ADDITIONAL_INFO_BONUS);
	} else if (InvestmentsManagerBase.isOptionPlusOpportunityTypeId(i)) {
		i.setAdditionalInfoText(CommonUtil.getMessage(locale, "optionPlus.toolTip1", null)+ " "
								+ CommonUtil.formatCurrencyAmountAO(i.getOptionPlusFee(), i.getCurrencyId()) + " "
								+ CommonUtil.getMessage(locale, "optionPlus.toolTip2", null));
		i.setAdditionalInfoText(i.getAdditionalInfoText().replace("<br />", " "));
		i.setAdditionalInfoType(Investment.INVESTMENT_ADDITIONAL_INFO_OPTION_PLUS);
	}
	if (i.getScheduledId() > Opportunity.SCHEDULED_DAYLY || (i.getScheduledId() == Opportunity.SCHEDULED_DAYLY && i.isHaveHourly()) || InvestmentsManagerBase.getIsOneTouchInv(i)) {
		i.setAdditionalInfoText(CommonUtil.getMessage(locale, "investments.not.hourly.info", null));
		i.setAdditionalInfoType(Investment.INVESTMENT_ADDITIONAL_INFO_NOT_HOURLY);
	}
}

public static Investment getById(Connection con, long id) throws SQLException {
  PreparedStatement ps = null;
  ResultSet rs = null;
  Investment inv = null;
  try {
      String sql = "SELECT " +
                       "i.*," +
                       "u.currency_id currency_id, " +
                       "u.skin_id skin_Id, " +
                       "o.opportunity_type_id oppTypeId " +
                   "FROM " +
                       "investments i, " +
                       "opportunities o," +
                       "users u " +
                   "WHERE " +
                       "i.user_id = u.id and " +
                       "i.opportunity_id = o.id and " +
                       "i.id = ? ";
      ps = con.prepareStatement(sql);
      ps.setLong(1, id);
      rs = ps.executeQuery();
      if (rs.next())  {
          inv =  getVO(rs);
          inv.setSkin(rs.getString("skin_id"));
          inv.setCurrencyId(rs.getLong("currency_id"));
          inv.setOpportunityTypeId(rs.getLong("oppTypeId"));
          return inv;
      }
  } finally {
      closeResultSet(rs);
      closeStatement(ps);
  }
  return null;
}

/**
* Update is_accepted_sms by investment id
*
* @param con
* @param invId investment to update
* @throws SQLException
*/
public static void updateAcceptedSmsById(Connection con, long invId) throws SQLException {
  PreparedStatement ps = null;
  try {
      String sql =
          "UPDATE " +
              "investments " +
          "SET " +
              "is_accepted_sms = 1 " +
          "WHERE " +
              "id = ?";
      ps = con.prepareStatement(sql);
      ps.setLong(1, invId);
      ps.executeUpdate();
  } finally {
      closeStatement(ps);
  }
}

public static void cancelInvestment(Connection con, long id, long writerId, boolean isSettled) throws SQLException {
  PreparedStatement ps = null;
  try {
      String sql;
      if (isSettled) {
          sql =
                  "UPDATE " +
                      "investments i " +
                  "SET " +
                      "i.is_settled = 1, " +
                      "i.is_canceled = 1, " +
                      "i.time_canceled = sysdate, " +
                      "i.utc_offset_cancelled = (" +
                          "SELECT " +
                              "utc_offset " +
                          "FROM " +
                              "users u " +
                          "WHERE " +
                              "i.user_id = u.id), " +
                      "canceled_writer_id = ? " +
                  "WHERE " +
                      "id = ? " +
                      "AND i.is_settled = 0 " +
                      "AND i.is_canceled = 0 " ;

      } else { // open investment
          sql =
                  "UPDATE " +
                      "investments i " +
                  "SET " +
                      "i.time_settled = sysdate, " +
                      "i.house_result = 0, " +
                      "i.is_settled = 1, " +
                      "i.is_canceled = 1, " +
                      "i.time_canceled = sysdate, " +
                      "i.utc_offset_cancelled = (" +
                          "SELECT " +
                              "utc_offset " +
                          "FROM " +
                              "users u " +
                          "WHERE " +
                              "i.user_id = u.id), " +
                      "canceled_writer_id = ? " +
                  "WHERE " +
                      "id = ? " +
                      "AND i.is_settled = 0 " +
                      "AND i.is_canceled = 0 " ;
      }
      ps = con.prepareStatement(sql);
      ps.setLong(1, writerId);
      ps.setLong(2, id);
      int k = ps.executeUpdate();
      if(k != 1) {
    	  throw new SQLException("Cancel inv updated failed: "+ id+ " writerID:"+ writerId);
      }
  } finally {
      closeStatement(ps);
  }
}

public static boolean isGMBannerViewed(Connection conn, long userId) throws SQLException {
  boolean isViewed = false;
  PreparedStatement pstmt = null;
  ResultSet rs = null;
  try {
      String sql =
          "SELECT " +
              "1 " +
          "FROM " +
              "golden_minutes_viewing gmv " +
          "WHERE " +
              "gmv.type_id = 1 AND " +
              "gmv.user_id = ? AND " +
              "gmv.time_created >= sysdate - ((SELECT value FROM enumerators WHERE enumerator LIKE 'insurance_period_start_time' ) - (SELECT value FROM enumerators WHERE enumerator LIKE 'insurance_period_end_time'))/1440 ";

      pstmt = conn.prepareStatement(sql);
      pstmt.setLong(1, userId);

      rs = pstmt.executeQuery();
      if (rs.next()) {
          isViewed = true;
      }
  } finally {
      closeResultSet(rs);
      closeStatement(pstmt);
  }
  return isViewed;
}

/**
* insert row that mark that this user saw the banner of golden minutes
* @param conn
* @param id user id
* @return true if insert was good
* @throws SQLException
*/
public static boolean insertGMBannerViewed(Connection conn, long id) throws SQLException {
  boolean isUpdate = false;
  PreparedStatement pstmt = null;
  try {
      String sql =
          "INSERT INTO golden_minutes_viewing " +
              "( " +
                  "id, " +
                  "user_id, " +
                  "type_id, " +
                  "time_created " +
              ") " +
          "VALUES " +
              "( " +
                  "seq_golden_minutes_viewing.NEXTVAL, " +
                  "?, " +
                  "1, " +
                  "sysdate " +
              ") ";
      pstmt = conn.prepareStatement(sql);
      pstmt.setLong(1, id);
      pstmt.executeUpdate();
      isUpdate = true;
  } finally {
      closeStatement(pstmt);
  }
  return isUpdate;
}

public static long getUserOpenedInvestmentsCount(Connection con, long userId, long writerId) throws SQLException {
	PreparedStatement ps = null;
	ResultSet rs = null;
	String products = Opportunity.TYPE_REGULAR + "," +  Opportunity.TYPE_OPTION_PLUS + "," +  Opportunity.TYPE_DYNAMICS + "," +  Opportunity.TYPE_BUBBLES;
	//String products = ConstantsBase.OPPORTUNITY_TYPE_BINARY_ + "," + ConstantsBase.OPPORTUNITY_TYPE_OPTION_PLUS + ", " + ConstantsBase.OPPORTUNITY_TYPE;
	if (writerId == Writer.WRITER_ID_COPYOP_WEB || writerId == Writer.WRITER_ID_COPYOP_MOBILE) {
		products = Integer.toString(ConstantsBase.OPPORTUNITY_TYPE_BINARY_);
  }
	try {
		String sql = "select count(i.id) as count"
						+ " from investments i, opportunities o"
						+ " where i.opportunity_id = o.id and i.is_settled = 0 and i.user_id = ?"
							+ " and o.opportunity_type_id in (" + products + ")";
		ps = con.prepareStatement(sql);
		ps.setLong(1, userId);
		rs = ps.executeQuery();
		if (rs.next()) {
			return rs.getLong("count");
		}

		return 0l;
	} finally {
		closeResultSet(rs);
		closeStatement(ps);
	}
}

	public static long getUserOpenedInvestmentsCount(Connection con, long userId) throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
	
		try {
			String sql = "select count(i.id) as count"
							+ " from investments i, opportunities o"
							+ " where i.opportunity_id = o.id and i.is_settled = 0 and i.user_id = ?";
	
			ps = con.prepareStatement(sql);
			ps.setLong(1, userId);
			rs = ps.executeQuery();
			if (rs.next()) {
				return rs.getLong("count");
			}
	
			return 0l;
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
	}

public static OptionPlusQuote getLastQuote(Connection conn, long userId, long investmentId, double price) throws SQLException {

	PreparedStatement pstmt = null;
  ResultSet rs = null;
  OptionPlusQuote q = null;
  try {
      String sql =
          " SELECT " +
              " opq.id quote_id, " +
              " opq.quote_level, " +
              " opq.time_quoted, " +
              " opq.time_quoted + (select dp.num_value from DB_PARAMETERS dp where dp.name = 'quote_time') time_quote_expired " +
          " FROM " +
              " options_plus_quotes opq " +
          " WHERE " +
              " opq.investment_id = ? " +
              " AND opq.price = ? " +
          " ORDER BY "  +
          	" opq.time_quoted desc";

      pstmt = conn.prepareStatement(sql);
      pstmt.setLong(1, investmentId);
      pstmt.setDouble(2, price);

      rs = pstmt.executeQuery();
      if (rs.next()) {
      	q = new OptionPlusQuote();
          q.setId(rs.getLong("quote_id"));
          q.setQuoteLevel(rs.getDouble("quote_level"));
          q.setTimeQuoted(convertToDate(rs.getTimestamp("time_quoted")));
          q.setTimeQuoteExpired(convertToDate(rs.getTimestamp("time_quote_expired")));
      }
  } finally {
      closeResultSet(rs);
      closeStatement(pstmt);
  }
  return q;
}

	/**
	* Get the current win/lose amount for specified user. In the calculation
	* are taken the investments done during the current year and are settled.
	*
	* @param conn the db connection to use
	* @param userId the id of the user to calculate for
	* @return The current win/lose amount for the user for the current year.
	* @throws SQLException
	*/
	public static long getTotalWinLoseForTheYear(Connection conn, long userId, boolean isPayTax,String utcOffset) throws SQLException {
		long wl = 0;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		String dateDelta = "";
	
		if(utcOffset.equals(ConstantsBase.UTCOFFSET_DEFAULT_PERIOD2)){
			dateDelta = ConstantsBase.GMT_2_OFFSET_DELTA;
		}else if(utcOffset.equals(ConstantsBase.UTCOFFSET_DEFAULT_PERIOD1)){
			dateDelta = ConstantsBase.GMT_3_OFFSET_DELTA;
		}
	
		try {
			String sql =
					" SELECT " +
					    " SUM(i.win) - SUM(i.lose) + SUM(NVL(t.amount,0)) AS win_lose " +
	              " FROM " +
	                  " investments i left join transactions t on t.bonus_user_id = i.bonus_user_id and i.bonus_odds_change_type_id > 1 " +
	              " WHERE " +
	                  " i.user_id = ? AND " +  /*i change time_created->time_settled*/
	                  " i.time_settled_year = EXTRACT(YEAR FROM sysdate " + dateDelta + ") AND " +
	                  " i.is_settled = 1 AND " +
	                  " i.is_canceled = 0 AND " +
	                  " not (i.bonus_odds_change_type_id = 1 and i.lose > 0)"; //do not include "next invest on us" investments that have lost
	
			if ( isPayTax ) {  // user already paid tax for the mid-year
				sql += " AND i.time_settled_month >= " + ConstantsBase.TAX_JOB_PERIOD1_END_PART_MONTH;
			}
			pstmt = conn.prepareStatement(sql);
			pstmt.setLong(1, userId);
			rs = pstmt.executeQuery();
			if (rs.next()) {
				wl = rs.getLong("win_lose");
			}
		} finally {
			closeResultSet(rs);
			closeStatement(pstmt);
		}
		return wl;
	}
	
	/**
	* check if the investment settled in Mid-year.
	* @param con the db connection to use
	* @param invId the id of the investment
	* @return boolean
	*          true if the investment settled in Mid-year
	*          false if the investment doesnt settled in Mid-year
	* @throws SQLException
	*/
	public static boolean isSettleInMidYear(Connection con, long invId, String year) throws SQLException {
		 PreparedStatement ps = null;
		 ResultSet rs = null;
		
		 String from = ConstantsBase.TAX_JOB_PERIOD1_START_PART + year;
		 String to = ConstantsBase.TAX_JOB_PERIOD1_END_PART + year;
		 try {
		     String sql=
		         "SELECT " +
		             "* " +
		         "FROM " +
		             "investments "+
		         "WHERE " +
		             "id = ? AND " +
		             "time_settled >= to_date('" + from + "','DDMMYYYY') " +
		             "AND time_settled <= to_date('" + to + "','DDMMYYYY')";
		     ps = con.prepareStatement(sql);
		     ps.setLong(1, invId);
		     rs = ps.executeQuery();
		     if (rs.next()) {
		         return true;
		     }
		 } finally {
		     closeResultSet(rs);
		     closeStatement(ps);
		 }
		 return false;
	}
	
    public static long insertInvestment(
								            Connection conn,
								            long userId,
								            long oppId,
								            long choice,
								            long amount,
								            double level,
								            String ip,
								            double realLevel,
								            double wwwLevel,
								            long bonusOddsChangeTypeId,
								            String utcOffsetCreated,
								            double rate,
								            long bonusUsersId,
								            long referenceInvestmentId,
								            boolean isLikeHourly,
								            long loginID,
								            long writerId
    									) throws SQLException {
        long id = 0;
        PreparedStatement pstmt = null;
        ResultSet rs = null;

        try {
            String sql =
                    "INSERT INTO investments " +
                        "("
                        + "id, "
                        + "user_id, "
                        + "opportunity_id, "
                        + "type_id, "
                        + "amount, "
                        + "current_level, "
                        + "writer_id, "
                        + "ip, "
                        + "win, "
                        + "lose, "
                        + "house_result, "
                        + "is_settled, "
                        + "is_canceled, "
                        + "is_void, "
                        + "real_level, "
                        + "www_level, "
                        + "bonus_odds_change_type_id, "
                        + "utc_offset_created, "
                        + "rate, "
                        + "bonus_user_id, "
                        + "reference_investment_id, "
                        + "server_id, "
                        + "time_created, "
                        + "is_like_hourly, "
                        + "login_id, "
                        + "odds_win, "
                        + "odds_lose ) " +
                    "VALUES " +
                        "("
                        + "SEQ_INVESTMENTS.NEXTVAL, "
                        + "?, "
                        + "?, "
                        + "?, "
                        + "?, "
                        + "?, "
                        + "?, "
                        + "?, "
                        + "0, "
                        + "0, "
                        + "0, "
                        + "0, "
                        + "0, "
                        + "0, "
                        + "?, "
                        + "?, "
                        + "?, "
                        + "?, "
                        + "?, "
                        + "?, "
                        + "?, "
                        + "?, "
                        + "?, "
                        + "?, "
                        + "?,"
                        + "?,"
                        + "?"
                        + ")";

            pstmt = conn.prepareStatement(sql);
            Date currentDate = new Date();


            int paramId = 1;
    	    pstmt.setLong(paramId++, userId);
            pstmt.setLong(paramId++, oppId);
            pstmt.setLong(paramId++, choice);
            pstmt.setLong(paramId++, amount);
            pstmt.setDouble(paramId++, level);
            pstmt.setLong(paramId++, writerId);
            pstmt.setString(paramId++, ip);

            pstmt.setDouble(paramId++, realLevel);
            pstmt.setDouble(paramId++, wwwLevel);
            pstmt.setLong(paramId++, bonusOddsChangeTypeId);
            pstmt.setString(paramId++, utcOffsetCreated);
            pstmt.setDouble(paramId++, rate);
            if (bonusOddsChangeTypeId > 0 && bonusUsersId > 0) {
                pstmt.setLong(paramId++, bonusUsersId);
            } else {
                pstmt.setNull(paramId++, Types.NUMERIC);
	        }
	        pstmt.setLong(paramId++, referenceInvestmentId);
	        pstmt.setString(paramId++, ServerConfiguration.getInstance().getServerName());
	        pstmt.setTimestamp(paramId++, CommonUtil.convertToTimeStamp(currentDate));
	        pstmt.setInt(paramId++, isLikeHourly == true ? 1 : 0);
            if (loginID > 0 ) {
                pstmt.setLong(paramId++, loginID);
            } else {
                pstmt.setNull(paramId++, Types.NUMERIC);
	        }
            
            pstmt.setInt(paramId++, 0);
            pstmt.setInt(paramId++, 0);
            
            pstmt.executeUpdate();
            
            id = getSeqCurValue(conn, "SEQ_INVESTMENTS");
            log.debug("finished investment insert:"+ id);
        } finally {
            closeResultSet(rs);
            closeStatement(pstmt);
        }
        return id;
    }

    public static ArrayList<Long> getUserOpenedInvestments(Connection con, long userId, long oppId) throws SQLException {
    	PreparedStatement ps = null;
    	ResultSet rs = null;
    	ArrayList<Long> list = new ArrayList<Long>();
    	try {
    		String sql = " SELECT "
    								+ " i.id id "
   						+ " FROM "
    							+ " investments i "
   						+ " WHERE "
   									+ " i.opportunity_id = ? "
						+ " AND "
									+ " i.is_settled = 0 "
						+ " AND "
									+ " i.is_canceled = 0 "
						+ " AND "
									+ " i.user_id = ?" ;
    		
    		ps = con.prepareStatement(sql);

    		ps.setLong(1, oppId);
    		ps.setLong(2, userId);

    		rs = ps.executeQuery();
    		while (rs.next()) {
    			list.add(rs.getLong("id"));
    		}

    		return list;
    	} finally {
    		closeResultSet(rs);
    		closeStatement(ps);
    	}
    }
    
    public static ArrayList<Long> getUserOpenedInvestments(Connection con, long userId, long oppId, long invTypeId) throws SQLException {
    	PreparedStatement ps = null;
    	ResultSet rs = null;
    	ArrayList<Long> list = new ArrayList<Long>();
    	try {
    		String sql = " SELECT "
    								+ " i.id id "
   						+ " FROM "
    							+ " investments i "
   						+ " WHERE "
   									+ " i.opportunity_id = ? "
						+ " AND "
									+ " i.is_settled = 0 "
						+ " AND "
									+ " i.is_canceled = 0 "
						+ " AND "
									+ " i.user_id = ?"
						+ " AND "
									+ " i.type_id = ? "	;
    		
    		ps = con.prepareStatement(sql);

    		ps.setLong(1, oppId);
    		ps.setLong(2, userId);
    		ps.setLong(3, invTypeId);

    		rs = ps.executeQuery();
    		while (rs.next()) {
    			list.add(rs.getLong("id"));
    		}

    		return list;
    	} finally {
    		closeResultSet(rs);
    		closeStatement(ps);
    	}
    }

    public static boolean updateInvestmentLose(Connection con, long id, long loseAmount) throws SQLException {
    	  PreparedStatement ps = null;
    	  try {
    	      String sql = 
    	    		  		" UPDATE " +
    	    		  					" investments " +
	    		  			" SET " +
	    		  			 			" lose = ?, " +
	    		  			 			" house_result = ? " + 
	  			 			" WHERE " +
    	    		  					" id = ?";
	      	ps = con.prepareStatement(sql);
	      	
      		ps.setLong(1, loseAmount);
      		ps.setLong(2, loseAmount);
      		ps.setLong(3, id);

      		int k = ps.executeUpdate();
      		return k == 1;
      	} finally {
      		closeStatement(ps);
      	}
    }

	public static boolean investmentIsCancelled(Connection conn, long invId) throws SQLException {
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
            pstmt = conn.prepareStatement(
            			" SELECT is_canceled FROM investments WHERE id = ? ");
            pstmt.setLong(1, invId);
            rs = pstmt.executeQuery();
            if (rs.next()) {
            	return rs.getBoolean("is_canceled");
            } else {
            	return false;
            }
        } finally {
            closeStatement(pstmt);
            closeResultSet(rs);
        }
	}
	
	
	public static boolean updateBubblesClosingLevel(Connection conn, long investmentId, double closingLevel) throws SQLException {
		PreparedStatement pstmt = null;
		try {
			String sql =
			      " UPDATE " +
			        			" investments i " +
	              " SET " +
	              				" i.bubbles_closing_level = ? " +
	              " WHERE " +
	              				" i.id = ? ";
			pstmt = conn.prepareStatement(sql);
			pstmt.setDouble(1, closingLevel);
			pstmt.setLong(2, investmentId);
			int updated = pstmt.executeUpdate();
			return updated == 1;
		} finally {
			closeStatement(pstmt);
		}
	}
}