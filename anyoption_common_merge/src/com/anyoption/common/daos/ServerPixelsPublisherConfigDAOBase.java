package com.anyoption.common.daos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.ServerPixelsPublisherConfig;

/**
 * ServerPixelsPublisherConfigDAOBase
 * @author eyalo
 */
public class ServerPixelsPublisherConfigDAOBase extends DAOBase {
	public static final Logger log = Logger.getLogger(ServerPixelsPublisherConfigDAOBase.class);

	/**
	 * Get server pixels publisher config
	 * @param conn
	 * @param vo
	 * @throws SQLException
	 */
	public static void getServerPixelsPublisherConfig(Connection conn, ServerPixelsPublisherConfig vo) throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		int index = 1;
		try {
			String sql = " SELECT "
							+ " * "
						+ " FROM "
							+ " server_pixels_publisher_config sppc "
						+ " WHERE "
							+ " sppc.server_pixels_publisher_id = ? "
							+ " AND sppc.server_pixels_type_id = ? ";
			if (vo.getPlatformId() != 0) {
				sql += " AND sppc.platform_id = ? ";
			}
			ps = conn.prepareStatement(sql);
			ps.setLong(index++, vo.getServerPixelsPublisherId());
			ps.setLong(index++, vo.getServerPixelsTypeId());
			if (vo.getPlatformId() != 0) {
				ps.setLong(index++, vo.getPlatformId());
			}
			rs = ps.executeQuery();
			if (rs.next()) {
				loadServerPixelsPublisherConfigVO(rs, vo);
			}
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
	}
	
	/**
	 * Load server pixels publisher config vo
	 * @param rs
	 * @param vo
	 * @throws SQLException
	 */
	public static void loadServerPixelsPublisherConfigVO(ResultSet rs, ServerPixelsPublisherConfig vo) throws SQLException {
		vo.setId(rs.getLong("id"));
		vo.setServerPixelsPublisherId(rs.getLong("server_pixels_publisher_id"));
		vo.setServerPixelsTypeId(rs.getLong("server_pixels_type_id"));
		vo.setPixelUrl(rs.getString("pixel_url"));
		vo.setHandlerClass(rs.getString("handler_class"));
		vo.setHandlerClassParams(rs.getString("handler_class_params"));
		vo.setWriterId(rs.getLong("writer_id"));
		vo.setTimeCreated(convertToDate(rs.getTimestamp("time_created")));
	}
}
