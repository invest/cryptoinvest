package com.anyoption.common.daos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.anyoption.common.beans.TransactionRequestResponse;

/**
 * TransactionRequestResponseDAO
 * @author eyalo
 */
public class TransactionRequestResponseDAO extends DAOBase {
	
	/**
	 * insert into transaction_request_response
	 * @param con
	 * @param vo
	 * @throws SQLException
	 */
	public static void insert(Connection con, TransactionRequestResponse vo) throws SQLException {
		PreparedStatement ps = null;
		int index = 1;
		try {
			String sql = 
					" INSERT INTO transaction_request_response "
					+ "( "
					+ "ID "
					+ ", TRANSACTION_ID "
					+ ", REQUEST "
					+ ", RESPONSE "
					+ ", TIME_CREATED "
					+ ", TIME_UPDATED "
					+ ", PAYMENT_SOLUTION "
					+ ", PAYMENT_DETAILS"
					+ ") " +
					" VALUES "
					+ "("
					+ "seq_transaction_request_res.nextval "
					+ ", ? "
					+ ", ? "
					+ ", ? "
					+ ", sysdate "
					+ ", '' "
					+ ", ? "
					+ ", ? "
					+ ")";
			ps = con.prepareStatement(sql);
			ps.setLong(index++, vo.getTransactionId());
			ps.setString(index++, vo.getRequest());
			ps.setString(index++, vo.getResponse());
			ps.setString(index++, vo.getPaymentSolution());
			ps.setString(index++, vo.getPaymentDetails());
			ps.executeUpdate();
		} finally {
			closeStatement(ps);
		}
	}
	
	/**
	 * update transaction_request_response
	 * @param con
	 * @param vo
	 * @throws SQLException
	 */
	public static void update(Connection con, TransactionRequestResponse vo) throws SQLException {
		PreparedStatement ps = null;
		int index = 1;
		try {
			String sql = 
					" UPDATE " +
						" transaction_request_response trr " +
					" SET " +
						" trr.response = ?, " +
						" trr.payment_solution = ?, " +
						" trr.time_updated = sysdate " +
					" WHERE " +
						" trr.transaction_id = ? ";
			ps = con.prepareStatement(sql);
			ps.setString(index++, vo.getResponse());
			ps.setString(index++, vo.getPaymentSolution());
			ps.setLong(index++, vo.getTransactionId());
			ps.executeUpdate();
		} finally {
			closeStatement(ps);
		}
	}
	
	public static TransactionRequestResponse getByTranId(Connection con, long transactionId) throws SQLException {

		  PreparedStatement ps = null;
		  ResultSet rs = null;

		  TransactionRequestResponse transactionRequestResponse = null;

		  try {
			  String sql = "SELECT " +
			  			   		"* " +
			  			   "FROM " +
			  			   		"transaction_request_response trr " +
			  			   "WHERE " +
			  			   		"trr.transaction_id = ? ";
			  ps = con.prepareStatement(sql);
			  ps.setLong(1, transactionId);
			  rs = ps.executeQuery();
			  //TODO: what to if more then 1 row
			  if (rs.next()) {
				  transactionRequestResponse = getVO(rs);
			  }
		  } finally {
			  closeResultSet(rs);
			  closeStatement(ps);
		  }
		  return transactionRequestResponse;
	 }
	
	 private static TransactionRequestResponse getVO(ResultSet rs) throws SQLException{
		TransactionRequestResponse vo = new TransactionRequestResponse();
		 
		vo.setId(rs.getLong("id"));
		vo.setTransactionId(rs.getLong("transaction_id"));
		vo.setRequest(null != rs.getClob("request") ? rs.getClob("request").toString() : null);
		vo.setResponse(null != rs.getClob("response") ? rs.getClob("response").toString() : null);
		vo.setTimeCreated(convertToDate(rs.getTimestamp("time_created")));
		vo.setTimeUpdated(convertToDate(rs.getTimestamp("time_updated")));
		vo.setPaymentSolution(rs.getString("payment_solution"));
		
		return vo;
	 }
}
