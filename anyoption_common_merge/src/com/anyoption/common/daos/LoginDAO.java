/**
 * 
 */
package com.anyoption.common.daos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 * @author kirilim
 *
 */
public class LoginDAO extends DAOBase {

	public static void updateLoginFailed(Connection con, String userName, boolean isNotActive) throws SQLException {
		PreparedStatement ps = null;
		try {
			String sql =
					"UPDATE " +
						"users " +
					"SET " +
						"last_failed_time = SYSDATE, " +
						"failed_count = FAILED_COUNT+1 ";

			if (isNotActive) {
				sql += ", IS_ACTIVE = 0 ";
			}

			sql += "WHERE " +
				   		"user_name = ?";

			ps = con.prepareStatement(sql);
			ps.setString(1, userName.toUpperCase());
			ps.executeUpdate();
		} finally {
			closeStatement(ps);
		}
	}
	
	public static void updateBackendLoginFailed(Connection con, String userName) throws SQLException {
		PreparedStatement ps = null;
		try {
			String sql =
					"UPDATE " +
						"writers " +
					"SET " +
						"last_failed_time = SYSDATE, " +
						"failed_count = FAILED_COUNT+1 " +
					"WHERE " +
				   		"user_name = ?";

			ps = con.prepareStatement(sql);
			ps.setString(1, userName.toUpperCase());
			ps.executeUpdate();
		} finally {
			closeStatement(ps);
		}
	}
	
	public static void updateLoginSucceed(Connection con, String userName) throws SQLException {
		PreparedStatement ps = null;
		try {
			String sql =
					"UPDATE " +
						"users " +
					"SET " +
						"last_failed_time = NULL, " +
						"failed_count = 0, " +
						"time_last_login = sysdate " +
					"WHERE " +
				   		"user_name = ?";

			ps = con.prepareStatement(sql);
			ps.setString(1, userName.toUpperCase());
			ps.executeUpdate();
		} finally {
			closeStatement(ps);
		}
	}
	
	public static void updateBackendLoginSucceed(Connection con, String userName) throws SQLException {
		PreparedStatement ps = null;
		try {
			String sql =
					"UPDATE " +
						"writers " +
					"SET " +
						"last_failed_time = NULL, " +
						"failed_count = 0 " +
					"WHERE " +
				   		"user_name = ?";

			ps = con.prepareStatement(sql);
			ps.setString(1, userName.toUpperCase());
			ps.executeUpdate();
		} finally {
			closeStatement(ps);
		}
	}
}