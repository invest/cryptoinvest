package com.anyoption.common.daos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.base.DeeplinkParamsToUrl;

public class DeeplinkParamsToUrlDAO extends DAOBase{
	
	  private static final Logger logger = Logger.getLogger(DeeplinkParamsToUrlDAO.class);

	  public static HashMap<String, DeeplinkParamsToUrl> getAll(Connection con) throws SQLException {

			PreparedStatement ps = null;
			ResultSet rs = null;
			HashMap<String, DeeplinkParamsToUrl> deeplinkParamsToUrlMap = new HashMap<String, DeeplinkParamsToUrl>();

			try {

				String sql = "SELECT " + 
								  "* " +
							  "FROM " +
								  "deeplink_params_to_url ";								  

				ps = con.prepareStatement(sql);
				rs = ps.executeQuery();

				while (rs.next()) {
					deeplinkParamsToUrlMap.put(rs.getString("parameter_name"), getVO(rs));
				}

			} finally {
				closeResultSet(rs);
				closeStatement(ps);
			}

			return deeplinkParamsToUrlMap;

		}

	private static DeeplinkParamsToUrl getVO(ResultSet rs) throws SQLException {
		DeeplinkParamsToUrl deeplinkParamsToUrl = new DeeplinkParamsToUrl();
		deeplinkParamsToUrl.setId(rs.getInt("id"));
		deeplinkParamsToUrl.setParameterName(rs.getString("parameter_name"));
		deeplinkParamsToUrl.setAnyoptionUrl(rs.getString("anyoption_url"));
		deeplinkParamsToUrl.setCopyopUrl(rs.getString("copyop_url"));
		deeplinkParamsToUrl.setTimeCreated(convertToDate(rs.getTimestamp("time_created")));
		deeplinkParamsToUrl.setTimeUpdated(convertToDate(rs.getTimestamp("time_updated")));
		deeplinkParamsToUrl.setWriterId(rs.getLong("writer_id"));
		return deeplinkParamsToUrl;
	}

}
