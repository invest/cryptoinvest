package com.anyoption.common.daos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.InvestmentRejects;
import com.anyoption.common.beans.ServerConfiguration;

/**
 * @author pavelhe
 */
public class InvestmentRejectsDAOBase extends DAOBase {
	protected static Logger log = Logger.getLogger(InvestmentRejectsDAOBase.class);
	
	/**
     * Insert investment.
     *
     * @param conn the db connection to use
     * @param inv Investment Rejects data to insert to db
     * @throws SQLException
     */
    public static void insertInvestmentRejects(Connection conn, InvestmentRejects inv) throws SQLException  {
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
	        String sql =
	                "INSERT INTO INVESTMENT_REJECTS " +
	                    "(id, user_id, opportunity_id, REJECT_TYPE_ID, SESSION_ID , REAL_LEVEL , PAGE_LEVEL , WWW_LEVEL , AMOUNT , RETURN , " +
	                    "REJECT_ADDITIONAL_INFO , TIME_CREATED , WRITER_ID, RATE, TYPE_ID, OPPORTUNITY_TYPE, IS_FROM_GRAPH, SERVER_ID, IS_REJECTED_DURING_CLOSE, REFUND, API_EXTERNAL_USER_ID) " +
	                "VALUES " +
	                    "(SEQ_INVESTMENT_REJECTS.NEXTVAL, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, current_date, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

	        pstmt = conn.prepareStatement(sql);
	        pstmt.setLong(1, inv.getUserId());
	        pstmt.setLong(2, inv.getOpportunityId());
	        pstmt.setLong(3, inv.getRejectTypeId());
	        pstmt.setString(4, inv.getSessionId());
	        pstmt.setDouble(5, inv.getRealLevel() != null ? inv.getRealLevel() : 0);
	        pstmt.setDouble(6, inv.getPageLevel() != null ? inv.getPageLevel() : 0);
	        pstmt.setDouble(7, inv.getWwwLevel() != null ? inv.getWwwLevel() : 0);
	        pstmt.setLong(8, inv.getAmount() != null ? inv.getAmount() : 0);
	        pstmt.setFloat(9, inv.getReturnInv() != null ? inv.getReturnInv() : 0);
	        pstmt.setString(10, inv.getRejectAdditionalInfo());
	        pstmt.setLong(11, inv.getWriterId());
	        pstmt.setDouble(12, inv.getRate());
	        pstmt.setDouble(13, inv.getTypeId());
	        pstmt.setInt(14, inv.getOpportunityType());
	        pstmt.setInt(15, inv.getFromGraph());
	        pstmt.setString(16, ServerConfiguration.getInstance().getServerName());
	        pstmt.setBoolean(17, inv.isRejectedDuringClose());
	        pstmt.setFloat(18, inv.getRefundInv() != null ? inv.getRefundInv() : 0);
	        pstmt.setLong(19, inv.getApiExternalUserId());
	        pstmt.executeUpdate();
	        log.debug("insert Investment Rejects into db sucessfully");
        } finally {
            closeResultSet(rs);
            closeStatement(pstmt);
        }
    }
}