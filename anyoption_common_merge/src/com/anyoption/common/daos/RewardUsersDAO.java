package com.anyoption.common.daos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.RewardUser;
import com.anyoption.common.util.ConstantsBase;
import com.anyoption.common.util.Utils;

/**
 * @author liors
 *
 */
public class RewardUsersDAO extends DAOBase {
	private final static Logger logger = Logger.getLogger(RewardUsersDAO.class);
		
	public static boolean insertValueObject(Connection connection, RewardUser rewardUser) throws SQLException {
		PreparedStatement preparedStatement = null;
		int index = 1;
		try {
			String sql =
						"INSERT INTO rwd_users" +
						"	(" +
						"		id" +
						"		,user_id" +
						"		,rwd_tier_id" +
						"		,exp_points" +
						"		,time_last_activity" +
						"		,time_last_reduced_points" +
						"	)" +
						"	VALUES" +
						"	(" +
						"		seq_rwd_users.nextval" +
						"		,?" +
						"		,?" +
						"		,?" +
						"		,sysdate" +
						"		,?" +
						"	)";

			preparedStatement = connection.prepareStatement(sql);
			preparedStatement.setLong(index++, rewardUser.getUserId());
			preparedStatement.setInt(index++, rewardUser.getTierId());
			preparedStatement.setDouble(index++, rewardUser.getExperiencePoints());
			preparedStatement.setNull(index++, Types.DATE);
			preparedStatement.executeUpdate();
			logger.info(ConstantsBase.REWARD_PLAN_LOG_PREFIX + "Insert Reward User; " + rewardUser.toString());
		} finally {
			closeStatement(preparedStatement);
		}	
		return true;
	}
	
	/**
	 * update rwd_users table.
	 * 
	 * @param connection
	 * @param userId
	 * @param tierId
	 * @param experiencePoints
	 * @return
	 * @throws SQLException
	 */
	public static boolean update(Connection connection, long userId, int tierId, double experiencePoints, boolean isMoveTier) throws SQLException {
		PreparedStatement preparedStatement = null;
		int index = 1;
		try {
			String sql = 
							"UPDATE " +
								"	rwd_users " +
								"SET" +
								"	exp_points = ? ";
					
					if (isMoveTier) {
						sql += "	,rwd_tier_id = ? ";
					}
					
					sql += 		"WHERE" +
								"	user_id = ?";
			
			preparedStatement = connection.prepareStatement(sql);
			preparedStatement.setDouble(index++, Utils.roundDouble(experiencePoints, 2));
			if (isMoveTier) {
				preparedStatement.setInt(index++, tierId);
			}
			preparedStatement.setLong(index++, userId);
			preparedStatement.executeUpdate();
		} finally {
			closeStatement(preparedStatement);
		}	
		return true;
	}
	
	/**
	 * function that fill RewardUser object
	 * @param resultSet
	 * @return RewardUser object
	 * @throws SQLException
	 */
	public static RewardUser getRewardUserValueObject(ResultSet resultSet) throws SQLException {
		return getRewardUserValueObject(resultSet, "id");
	}
	
	/**
	 * function that fill RewardUser object  use it in case u override the id column name in the sql
	 * @param resultSet
	 * @param id the name of column id
	 * @return RewardUser object
	 * @throws SQLException
	 */
	public static RewardUser getRewardUserValueObject(ResultSet resultSet, String id) throws SQLException {
		RewardUser valueObject = new RewardUser();
		valueObject.setId(resultSet.getInt(id));
		valueObject.setUserId(resultSet.getLong("user_id"));
		valueObject.setTierId(resultSet.getInt("rwd_tier_id"));
		valueObject.setExperiencePoints(resultSet.getDouble("exp_points"));
		valueObject.setTimeLastActivity(resultSet.getDate("time_last_activity"));
		valueObject.setTimeLastReducedPoints(resultSet.getDate("time_last_reduced_points"));
		return valueObject;
	}

	public static RewardUser getRewardUser(Connection connection, long userId) throws SQLException {
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null; 
		int index = 1;
		RewardUser rewardUser = null;
		
		try {
			String sql = 
					"SELECT" +
					"	* " +
					"FROM" +
					"	rwd_users " +
					"WHERE" +
					"	user_id = ?";
			preparedStatement = connection.prepareStatement(sql);
			preparedStatement.setLong(index++, userId);
			resultSet = preparedStatement.executeQuery();
			
			while (resultSet.next()) {
				rewardUser = getRewardUserValueObject(resultSet);
			}
		} finally {
			closeStatement(preparedStatement);
		}
		
		return rewardUser;
	}
	
	/**
	 * function that fill RewardUser object  use it in case u override the id column name in the sql
	 * @param resultSet
	 * @param id the name of column id
	 * @return RewardUser object
	 * @throws SQLException
	 */
	public static RewardUser getRewardUserCustomValue(ResultSet resultSet) throws SQLException {
		RewardUser valueObject = new RewardUser();
		valueObject.setId(resultSet.getInt("ru_id"));
		valueObject.setUserId(resultSet.getLong("user_id"));
		valueObject.setTierId(resultSet.getInt("rwd_tier_id"));
		valueObject.setExperiencePoints(resultSet.getDouble("ru_exp_points"));
		valueObject.setTimeLastReducedPoints(resultSet.getDate("time_last_activity"));
		valueObject.setTimeLastReducedPoints(resultSet.getDate("time_last_reduced_points"));
		return valueObject;
	}
}
