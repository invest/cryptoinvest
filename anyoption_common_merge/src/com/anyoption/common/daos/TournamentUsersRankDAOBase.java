package com.anyoption.common.daos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.TournamentUsersRank;



public class TournamentUsersRankDAOBase extends DAOBase {

	protected static final Logger log = Logger.getLogger(TournamentUsersRankDAOBase.class);

	public static ArrayList<TournamentUsersRank> getTournamentUsers (Connection conn, long tournmanetId, Long userId) throws SQLException {
		  PreparedStatement ps = null;
		  ResultSet rs = null;
		  ArrayList<TournamentUsersRank> list = new ArrayList<TournamentUsersRank>();
		  try{
			  String sql = "SELECT sq.* "
					  + "FROM "
					  + "(SELECT sqq.*, "
					    + "rownum AS position "
					  + "FROM "
					    + "(SELECT tur.*, "
					      + "tnou.users_to_show, "
					      + "u.first_name, "
					      + "SUBSTR(u.last_name, 1, 1) AS last_name, "
					      + "c.id AS currency_id "
					   + "FROM tournament_users_rank tur, "
					      + "users u, "
					      + "countries c, "
					      + "tournaments t, "
					      + "tournament_num_of_users tnou "
					    + "WHERE tur.user_id        = u.id "
					    + "AND c.id                 = u.country_id "
					    + "AND t.id                 = tur.tournament_id "
					    + "AND t.number_of_users_id = tnou.id "
					    + "AND tur.tournament_id    = ? "
					    + "ORDER BY tur.score DESC, tur.score_2 DESC, tur.time_updated "
					    + ") sqq "
					  + ") sq ";
			if (userId == null) {
				sql += " WHERE ROWNUM <= sq.users_to_show";
			} else {
				sql += " WHERE sq.user_id = ?";
			}
			  		
			  ps = conn.prepareStatement(sql);
			  ps.setLong(1, tournmanetId);
			  if(userId != null){
				  ps.setLong(2, userId);
			  }
			  rs = ps.executeQuery();
			  
			  while (rs.next()){
				  TournamentUsersRank t = getVO(rs);
				  list.add(t);
			  }
		  } finally {
				closeResultSet(rs);
				closeStatement(ps);
		  }
		  return list;
	}

	private static TournamentUsersRank getVO(ResultSet rs) throws SQLException {
		TournamentUsersRank tournamentUsersRank = new TournamentUsersRank();
		tournamentUsersRank.setRank(rs.getLong("position"));
		tournamentUsersRank.setScore(rs.getDouble("score"));
		tournamentUsersRank.setTournamentId(rs.getLong("tournament_id"));
		tournamentUsersRank.setUserId(rs.getLong("user_id"));
		tournamentUsersRank.setFirstName(rs.getString("first_name"));
		tournamentUsersRank.setLastName(rs.getString("last_name"));
		tournamentUsersRank.setCountryId(rs.getLong("currency_id"));
		return tournamentUsersRank;
	}
}