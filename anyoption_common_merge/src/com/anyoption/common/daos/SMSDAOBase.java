package com.anyoption.common.daos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;

/**
 * @author kirilim
 */
public class SMSDAOBase extends DAOBase {

	public static long queueMessage(Connection conn, long typeId, String sender, String senderNumber, String phone, String message,
									String wapPushURL, long dstPort, long keyValue, long keyType, long providerId,
									long descriptionId, long initialStatus) throws SQLException {
		PreparedStatement pstmt = null;
		try {
			String sql =
                    "INSERT INTO sms " +
                        "(id, sms_type_id, sender, phone, message, wap_url, dst_port, scheduled_time, sms_status_id, key_value, key_type, provider_Id, sender_number, sms_description_id) " +
                     "VALUES " +
                         "(SEQ_SMS.NEXTVAL, ?, ?, ?, ?, ?, ?, sysdate, ?, ?, ?, ?, ?, ?)";
			pstmt = conn.prepareStatement(sql);
			pstmt.setLong(1, typeId);
			pstmt.setString(2, sender);
			pstmt.setString(3, phone);
			pstmt.setString(4, message);
			if (null != wapPushURL) {
				pstmt.setString(5, wapPushURL);
			} else {
				pstmt.setNull(5, Types.VARCHAR);
			}
			if (dstPort > 0) {
				pstmt.setLong(6, dstPort);
			} else {
				pstmt.setNull(6, Types.NUMERIC);
			}
			pstmt.setLong(7, initialStatus);
			pstmt.setLong(8, keyValue);
			pstmt.setLong(9, keyType);
			pstmt.setLong(10, providerId);
			if (null != senderNumber) {
				pstmt.setString(11, senderNumber);
			} else {
				pstmt.setNull(11, Types.VARCHAR);
			}
			pstmt.setLong(12, descriptionId);
			pstmt.executeUpdate();

			return getSeqCurValue(conn, "SEQ_SMS");
		} finally {
			closeStatement(pstmt);
		}
	}
	/*
	 * mid - id in the sms table
	 * key_type = 2 -> Users as in sms_key_types
	 */
	public static Long getUserIdByMid(Connection con, long mid) throws SQLException  {
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			String sql = "SELECT key_value FROM sms WHERE id = ? and key_type = 2 ";
			ps = con.prepareStatement(sql);
			ps.setLong(1, mid);
			rs=ps.executeQuery();
            if( rs.next()) {
            	return rs.getLong("key_value");
            } else {
            	return null;
            }
            
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
	}
	public static int getMobileNumberValidation(Connection con, long userId) throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			String sql = "select mobile_phone_validated from users where id = ?";
			ps = con.prepareStatement(sql);
			ps.setLong(1, userId);
			rs = ps.executeQuery();
			if (rs.next()) {
				return rs.getInt("mobile_phone_validated");
			}
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		return 0;
	}
}