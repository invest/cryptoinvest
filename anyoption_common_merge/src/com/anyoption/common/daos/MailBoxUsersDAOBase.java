package com.anyoption.common.daos;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import oracle.jdbc.OracleResultSet;
import oracle.sql.BLOB;

import com.anyoption.common.beans.MailBoxUser;

public class MailBoxUsersDAOBase extends DAOBase {
    /**
     * Inser email to user internal mailbox.
     * 
     * @param con db connection
     * @param email
     * @throws SQLException
     */
    public static void insert(Connection con, MailBoxUser email) throws SQLException {
        PreparedStatement ps = null;
        try {
            String sql =
                "INSERT INTO " +
                      "mailbox_users " +
                            "(id, " +
                            "template_id, " +
                            "user_id, " +
                            "status_id, " +
                            "time_created, " +
                            "writer_id, " +
                            "free_text, " +
                            "sender_id, " +
                            "is_high_priority, " +
                            "subject," +
                            "bonus_user_id," +
                            "popup_type_id," +
                            "transaction_id, " +
                            "template_parameters, " +
                            "attachment_id) " +
                    "VALUES" +
                            "(SEQ_MAILBOX_USERS.nextval,?,?,?,sysdate,?,?,?,?,?,?,?,?,?,?)";

            ps = con.prepareStatement(sql);

            ps.setLong(1, email.getTemplateId());
            ps.setLong(2, email.getUserId());
            ps.setLong(3, MailBoxUser.MAILBOX_STATUS_NEW);
            ps.setLong(4, email.getWriterId());
            ps.setString(5, email.getFreeText());
            ps.setLong(6, email.getSenderId());
            ps.setLong(7, email.getIsHighPriority() ? 1 : 0);
            ps.setString(8, email.getSubject());
            if (email.getBonusUserId() != 0) {
                ps.setLong(9, email.getBonusUserId());
            } else {
                ps.setString(9, null);
            }
            ps.setLong(10, email.getPopupTypeId());
            if (email.getTransactionId() != 0) {
                ps.setLong(11, email.getTransactionId());
            } else {
                ps.setString(11, null);
            }
            ps.setString(12, email.getParameters());
            if (null != email.getAttachmentId()) {
                ps.setLong(13, email.getAttachmentId());
            } else {
                ps.setString(13, null);
            }

            ps.executeUpdate();
        } finally {
            closeStatement(ps);
        }
    }
    
    /**
     * insert PDF as BLOB
     * @param conn
     * @param attachment PDF file
     * @throws SQLException
     * @throws IOException
     */
	public static long insertAttachmentAsBLOB(Connection conn, File attachment) throws SQLException, IOException {
        FileInputStream inputFileInputStream = null;
        OutputStream blobOutputStream = null;
        Statement stmt = null;
        ResultSet rset = null;
        BLOB attchBlob = null;
        long attachmentId = 0;

        try {
            stmt = conn.createStatement();

            inputFileInputStream = new FileInputStream(attachment);

            String sql =
	            "INSERT INTO " +
				  "mailbox_users_attachments " +
				  		"(id, " +
				  		"attachment_name, " +
				  		"attachment) " +
				"VALUES " +
	                  "(SEQ_MAILBOX_USERS_ATT.nextval, '" + attachment.getName() + "', EMPTY_BLOB())";

            stmt.executeUpdate(sql);
            attachmentId = getSeqCurValue(conn, "SEQ_MAILBOX_USERS_ATT");

            sql =
                "SELECT " +
                	"attachment " +
                "FROM " +
                	"mailbox_users_attachments " +
                "WHERE " +
                	"id = " + attachmentId + " " +
                "FOR UPDATE ";   // lock the record in the cursor result set. released after commit / rollBack

            rset = stmt.executeQuery(sql);
            rset.next();
            attchBlob = ((OracleResultSet) rset).getBLOB("attachment");

            int bufferSize = attchBlob.getBufferSize();
            byte[] byteBuffer = new byte[bufferSize];
            blobOutputStream = attchBlob.getBinaryOutputStream();

            int bytesRead = 0;
            while ((bytesRead = inputFileInputStream.read(byteBuffer)) != -1) {
                blobOutputStream.write(byteBuffer, 0, bytesRead);
            }
            inputFileInputStream.close();
            blobOutputStream.close();

            conn.commit();

        } finally {
        	closeResultSet(rset);
            closeStatement(stmt);
        }
        return attachmentId;
	}
	
	
	  public static void updateMailBoxAttName(Connection con, long attachmentId , long userId) throws SQLException {
			PreparedStatement ps = null;
			
			try {
				String sql = 	"UPDATE " +
									" mailbox_users_attachments " +
								" SET " +
									" attachment_name = '" + userId + "_'  || attachment_name " +
								" WHERE " +
									" id = ? ";

				ps =  con.prepareStatement(sql);
				ps.setLong(1, attachmentId);
				ps.executeUpdate();

			} finally {
				closeStatement(ps);
			}
	  }
	  
	/**
	 * get user emails by template
	 * @param con db connection
	 * @return ArrayList of mail 
	 * @throws SQLException
	 */
	public static ArrayList<MailBoxUser> getUserMailByTemplate(Connection con, long userId, long mailboxTemplateId) throws SQLException {
		ArrayList<MailBoxUser> list = new ArrayList<MailBoxUser>();
		PreparedStatement ps = null;
		ResultSet rs = null;
		try	{
		    String sql =
		    	  "SELECT " +
	                  "mbu.* " +
	              "FROM " +
	                  "mailbox_users mbu " +
	              "WHERE " +
	                  "mbu.user_id = ? AND " +
	                  "mbu.template_id = ? " +
	              "ORDER BY " +
	              	"mbu.time_created desc ";

			ps = con.prepareStatement(sql);
			ps.setLong(1, userId);
			ps.setLong(2, mailboxTemplateId);
			
			rs = ps.executeQuery();

			while ( rs.next() ) {
				MailBoxUser vo = getVOBasic(rs);
				list.add(vo);
			}
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		return list;
	}
		
	public static MailBoxUser getVOBasic(ResultSet rs) throws SQLException {
		MailBoxUser vo = new MailBoxUser();
		vo.setId(rs.getLong("id"));
		vo.setTemplateId(rs.getLong("template_id"));
		vo.setUserId(rs.getLong("user_id"));
		vo.setStatusId(rs.getLong("status_id"));
		vo.setTimeCreated(convertToDate(rs.getTimestamp("time_created")));
		vo.setTimeRead(convertToDate(rs.getTimestamp("time_read")));
		vo.setUtcOffsetRead(rs.getString("utc_offset_read"));
		vo.setWriterId(rs.getLong("writer_id"));
		vo.setFreeText(rs.getString("free_text"));
		vo.setSenderId(rs.getLong("sender_id"));
		vo.setIsHighPriority(rs.getInt("is_high_priority") == 1 ? true : false);
		vo.setSubject(rs.getString("subject"));
		vo.setPopupTypeId(rs.getLong("popup_type_id"));
		return vo;
	}
}