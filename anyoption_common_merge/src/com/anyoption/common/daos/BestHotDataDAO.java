package com.anyoption.common.daos;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

import org.apache.log4j.Logger;

import com.anyoption.common.managers.BestHotDataManager;

public class BestHotDataDAO extends DAOBase {

	private static final Logger logger = Logger.getLogger(BestHotDataManager.class);

	private static void initTradingHoursTable(Connection conn, long hours) throws SQLException {

		String sql = "BEGIN COPYOP.INIT_TRADING_HOURS(?); END; ";
		CallableStatement cs = null;
		try {
			cs = conn.prepareCall(sql);
			cs.setLong(1, hours);
			logger.debug("Begin init the TRADING HOURS session table for " + hours + "h");
			cs.execute();
		} finally {
			closeStatement(cs);
		}
	}

	public static Date getTradingDateBack(Connection con, long hours) throws SQLException {

		initTradingHoursTable(con, hours);
		PreparedStatement ps = null;
		ResultSet rs = null;
		Date tradingDateBack = null;
		try {
			String sql =" SELECT " +
						"	copyop.GET_TRADING_DATE_TIME(?) as tr_date_back " +
						" FROM " +
						"	DUAL ";
	        ps = con.prepareStatement(sql);
	        ps.setLong(1, hours);
	        rs = ps.executeQuery();
	        if (rs.next()) {
	        	tradingDateBack = rs.getTimestamp("tr_date_back");
	        	String dateForDisplay = new SimpleDateFormat("HH:mm:ss dd/MM/yyyy").format(tradingDateBack);
	        	logger.debug("The trading date back for " + hours + "h is " + dateForDisplay);
	        }
		} finally {
			closeResultSet(rs);
	        closeStatement(ps);
		}
		return tradingDateBack;
	}

//	public static ArrayList<FeedMessage> getHotBestTraders(Connection con, long hours, long returnRecords, UpdateTypeEnum msgEnumType) throws SQLException {
//		Date tradingDateTimeBack = getTradingDateBack(con, hours);
//		PreparedStatement ps = null;
//		ResultSet rs = null;
//		ArrayList<FeedMessage> list = new ArrayList<FeedMessage>();
//
//		long minHit = new Long(ConfigurationCopyopManager.getCopyopPropertiesValue("HOT_BEST_TRADERS_MIN_HIT", "40"));
//		long minProfit = new Long(ConfigurationCopyopManager.getCopyopPropertiesValue("HOT_BEST_TRADERS_MIN_PROFIT", "10000"));
//		long aboveProfit = new Long(ConfigurationCopyopManager.getCopyopPropertiesValue("HOT_BEST_TRADERS_ABOVE_PROFIT", "100000"));
//		long aboveHit = new Long(ConfigurationCopyopManager.getCopyopPropertiesValue("HOT_BEST_TRADERS_ABOVE_HIT", "80"));
//
//		try {
//            String sql = " SELECT * FROM (select * from " +
//            					" ( " +
//            					" select bts.user_id, round(bts.hit) as hit, " +
//            					" sum(bts.user_prof) as user_profit ," +
//            					" bts.currency_id  ," +
//            					" sum(bts.ret) as profit " +
//            					" from " +
//            					" ( " +
//            					  " select i.user_id, ch.hit, " +
//            					  " ((i.amount - i.house_result) - i.amount)  as user_prof,  " +
//            					  " u.currency_id , " +
//            					  " ((i.amount - i.house_result) - i.amount)*i.rate as Ret " +
//            					  " from " +
//            					   " copyop_hits ch, " +
//            					   " investments i, " +
//            					   " opportunities op, " +
//            					   " users_active_data uad, " +
//            					   " users u,  " +
//            					   " copyop_frozen cf " +
//            					  " where " +
//            					   " ch.user_id = i.user_id " +
//            					   " and u.id = i.user_id " +
//            					   " and u.is_active = 1 " +
//            					   " and u.class_id != " + ConstantsBase.USER_CLASS_TEST +
//            					   " and u.skin_id != " + Skin.SKIN_ETRADER +
//            					   " and u.id = uad.user_id " +
//            					   " and uad.copyop_user_status = " + UserStateEnum.STATE_REGULAR.getId() +
//            					   " and i.opportunity_id = op.id" +
//            					   " and (op.time_est_closing) at time zone 'GMT' + 0 >= ? " +
//            					   " and i.time_created >= ? -1" +
//            					   " and op.is_settled = 1 " +
//            					   " and (op.scheduled = 1 OR i.is_like_hourly  = 1 ) " +
//            					   " and op.opportunity_type_id = 1 " +
//            					   " and i.is_settled = 1 " +
//            					   " and i.is_canceled = 0 " +            					   
//            					   " and i.copyop_type_id in (0,2) " +
//            					   " and u.id = cf.user_id " +
//            					   " and cf.is_frozen = 0 " +
//            					 " )bts " +
//            					 " group by " +
//            					 " bts.user_id, bts.hit, bts.currency_id " +
//            			  "  ) b_traders " +
//            			  " where " +
//            			  		" (b_traders.profit >= " + minProfit + " and b_traders.hit >= " + minHit + " ) " +
//            			  		" OR (b_traders.profit >= " + aboveProfit + " ) " +
//            			  		" OR (b_traders.hit >= " + aboveHit + " and b_traders.profit > 0  ) " +
//            			  " order by " +
//            			  "b_traders.profit desc, " +
//            			  "b_traders.hit desc ) WHERE rownum <= " + returnRecords + " ORDER BY profit, hit";
//
//			ps = con.prepareStatement(sql);
//			ps.setTimestamp(1, CommonUtil.convertToTimeStamp(tradingDateTimeBack));
//			ps.setTimestamp(2, CommonUtil.convertToTimeStamp(tradingDateTimeBack));
//			rs = ps.executeQuery();
//			while(rs.next()) {
//				FeedMessage hotBestTraders = new FeedMessage();
//				hotBestTraders.setUserId(FeedMessage.HOT_SYSTEM_FEED_USER_ID);
//				hotBestTraders.setQueueType(QueueTypeEnum.HOT.getId());
//				hotBestTraders.setMsgType(msgEnumType.getId());
//				UUID timeCreatedUUID = UUIDs.timeBased();
//				hotBestTraders.setTimeCreatedUUID(timeCreatedUUID);
//				hotBestTraders.setTimeCreated(new Date(UUIDs.unixTimestamp(timeCreatedUUID)));
//
//				Map<String, String> properties = new HashMap<String, String>();
//				properties.put(FeedMessage.PROPERTY_KEY_USER_ID, String.valueOf(rs.getLong("user_id")));
//				properties.put(FeedMessage.PROPERTY_KEY_HIT_RATE, String.valueOf(rs.getInt("hit")));
//
//				for (Map.Entry<String, String> entry :getAmountInAllCurr(rs.getDouble("profit"), rs.getLong("user_profit"), rs.getInt("currency_id")).entrySet()) {
//					properties.put(entry.getKey(), entry.getValue());
//				}
//				//properties.put(FeedMessage.PROPERTY_KEY_PROFIT, String.valueOf(rs.getDouble("profit")));
//				properties.put(FeedMessage.PROPERTY_KEY_ASSET_LAST_TRADING_HOURS, String.valueOf(hours));
//				hotBestTraders.setProperties(properties);
//
//				list.add(hotBestTraders);
//			}
//		} finally {
//			closeResultSet(rs);
//			closeStatement(ps);
//		}
//		return list;
//	}
	
//	public static ArrayList<FeedMessage> getHotAssetSpecialistsBestTraders(Connection con, long hours, long returnRecords, UpdateTypeEnum msgEnumType) throws SQLException {
//		Date tradingDateTimeBack = getTradingDateBack(con, hours);
//		PreparedStatement ps = null;
//		ResultSet rs = null;
//		ArrayList<FeedMessage> list = new ArrayList<FeedMessage>();
//
//		long minHit = new Long(ConfigurationCopyopManager.getCopyopPropertiesValue("HOT_ASSET_SPECIALISTS_MIN_HIT", "40"));
//		long minProfit = new Long(ConfigurationCopyopManager.getCopyopPropertiesValue("HOT_ASSET_SPECIALISTS_MIN_PROFIT", "10000"));
//		long aboveProfit = new Long(ConfigurationCopyopManager.getCopyopPropertiesValue("HOT_ASSET_SPECIALISTS_ABOVE_PROFIT", "100000"));
//		long aboveHit = new Long(ConfigurationCopyopManager.getCopyopPropertiesValue("HOT_ASSET_SPECIALISTS_ABOVE_HIT", "80"));
//
//		try {
//        String sql = " SELECT * FROM (select * from " +
//        					" ( " +
//        					" select bts.user_id, round(bts.hit) as hit, inv_time_created, " +
//                					" sum(bts.user_prof) as user_profit ," +
//                					" bts.currency_id  ," +
//                					" sum(bts.ret) as profit, " +
//                					" bts.market_id as market_id" +
//        					" from " +
//        					" ( " +
//        					  " select i.user_id, ch.hit, i.time_created inv_time_created, " +
//                					  " ((i.amount - i.house_result) - i.amount)  as user_prof,  " +
//                					  " u.currency_id , " +
//                					  " ((i.amount - i.house_result) - i.amount)*i.rate as Ret, " + 
//                					  "m.id as market_id " +
//        					  " from " +
//                					   " copyop_hits ch, " +
//                					   " investments i, " +
//                					   " opportunities op, " +
//                					   " users_active_data uad, " +
//                					   " users u,  " +
//                					   " markets m, " +
//                					   " copyop_frozen cf " +
//        					  " where " +
//                					   " ch.user_id = i.user_id " +
//                					   " and op.market_id = m.id " +
//                					   " and u.id = i.user_id " +
//                					   " and u.is_active = 1 " +
//                					   " and u.class_id != " + ConstantsBase.USER_CLASS_TEST +
//                					   " and u.skin_id != " + Skin.SKIN_ETRADER +
//                					   " and u.id = uad.user_id " +
//                					   " and uad.copyop_user_status = " + UserStateEnum.STATE_REGULAR.getId() +
//                					   " and i.opportunity_id = op.id" +
//                					   " and (op.time_est_closing) at time zone 'GMT' + 0 >= ? " +
//                					   " and i.time_created >= ? -1" +
//                					   " and op.is_settled = 1 " +
//                					   " and (op.scheduled = 1 OR i.is_like_hourly  = 1 ) " +
//                					   " and op.opportunity_type_id = 1 " +
//                					   " and i.is_settled = 1 " +
//                					   " and i.is_canceled = 0 " +            					   
//                					   " and i.copyop_type_id in (0,2) " +
//                					   " and u.id = cf.user_id " +
//                					   " and cf.is_frozen = 0 " +
//                					   " )bts " +
//        					 " group by " +
//        					 	   " bts.user_id, bts.hit, bts.currency_id, market_id, inv_time_created " +
//        			  "  ) b_traders " +
//        			  " where " +
//			  		" (b_traders.profit >= " + minProfit + " and b_traders.hit >= " + minHit + " ) " +
//			  		" OR (b_traders.profit >= " + aboveProfit + " ) " +
//			  		" OR (b_traders.hit >= " + aboveHit + " and b_traders.profit > 0  ) " +
//        			  " order by " +
//                			  " b_traders.profit desc, " +
//                			  " b_traders.hit desc ) WHERE rownum <= " + returnRecords + " ORDER BY profit, market_id, hit";
//
//			ps = con.prepareStatement(sql);
//			ps.setTimestamp(1, CommonUtil.convertToTimeStamp(tradingDateTimeBack));
//			ps.setTimestamp(2, CommonUtil.convertToTimeStamp(tradingDateTimeBack));
//			rs = ps.executeQuery();
//			while(rs.next()) {
//				FeedMessage hotAssetSpecialists = new FeedMessage();
//				hotAssetSpecialists.setUserId(FeedMessage.HOT_SYSTEM_FEED_USER_ID);
//				hotAssetSpecialists.setQueueType(QueueTypeEnum.HOT.getId());
//				hotAssetSpecialists.setMsgType(msgEnumType.getId());
//				UUID timeCreatedUUID = UUIDs.timeBased();
//				hotAssetSpecialists.setTimeCreatedUUID(timeCreatedUUID);
//				hotAssetSpecialists.setTimeCreated(new Date(UUIDs.unixTimestamp(timeCreatedUUID)));
//
//				Map<String, String> properties = new HashMap<String, String>();
//				properties.put(FeedMessage.PROPERTY_KEY_USER_ID, String.valueOf(rs.getLong("user_id")));
//				properties.put(FeedMessage.PROPERTY_KEY_HIT_RATE, String.valueOf(rs.getInt("hit")));
//
//				for (Map.Entry<String, String> entry : getAmountInAllCurr(rs.getDouble("profit"), rs.getLong("user_profit"), rs.getInt("currency_id")).entrySet()) {
//					properties.put(entry.getKey(), entry.getValue());
//				}
//
//				properties.put(FeedMessage.PROPERTY_KEY_ASSET_LAST_TRADING_HOURS, String.valueOf(hours));
//				
//				properties.put(FeedMessage.PROPERTY_KEY_MARKET_ID, rs.getString("market_id"));
//				Date assetCreateDateTime = rs.getTimestamp("inv_time_created");
//				properties.put(FeedMessage.PROPERTY_KEY_ASSET_CREATE_DATE_TIME,  new SimpleDateFormat("dd/MM/yyyy HH:mm").format(assetCreateDateTime));
//				properties.put(FeedMessage.PROPERTY_KEY_ASSET_LAST_TRADING_HOURS, String.valueOf(hours));
//				hotAssetSpecialists.setProperties(properties);
//
//				list.add(hotAssetSpecialists);
//			}
//		} finally {
//			closeResultSet(rs);
//			closeStatement(ps);
//		}
//		return list;
//	}

//	public static ArrayList<FeedMessage> getHotBestTrades(Connection con, long hours, long returnRecords, UpdateTypeEnum msgEnumType) throws SQLException {
//		Date tradingDateTimeBack = getTradingDateBack(con, hours);
//		PreparedStatement ps = null;
//		ResultSet rs = null;
//		ArrayList<FeedMessage> list = new ArrayList<FeedMessage>();
//		long minProfit = new Long(ConfigurationCopyopManager.getCopyopPropertiesValue("HOT_BEST_TRADES_MIN_PROFIT_AMOUNT", "20000"));
//		try {
//            String sql = " SELECT * FROM (select " +
//            				" bts.user_id, bts.hit, inv_time_created, " +
//            				" bts.profit, bts.market_id, bts.user_profit, bts.currency_id " +
//            			 " from  " +
//            			 " ( " +
//            			 	" select i.user_id, ch.hit, m.id as market_id, i.time_created as inv_time_created, " +
//            			 	" ((i.amount - i.house_result) - i.amount)  as user_profit, " +
//            			 	"  u.currency_id, " +
//            			 	" ((i.amount - i.house_result) - i.amount)*i.rate as profit " +
//            			 	" from  " +
//            			 		" copyop_hits ch, " +
//            			 		" investments i, " +
//            			 		" opportunities op, " +
//            			 		" markets m, " +
//            			 		" users_active_data uad, " +
//            			 		" users u, " +
//         					    " copyop_frozen cf " +
//            			 	" where " +
//            			 		" ch.user_id = i.user_id " +
//            			 		" and u.id = i.user_id " +
//         					    " and u.is_active = 1 " +
//         					    " and u.class_id != " + ConstantsBase.USER_CLASS_TEST +
//         					    " and u.skin_id != " + Skin.SKIN_ETRADER +
//         					    " and u.id = uad.user_id " +
//         					    " and uad.copyop_user_status = " + UserStateEnum.STATE_REGULAR.getId() +
//            			 		" and i.opportunity_id = op.id " +
//            			 		" and op.market_id = m.id " +
//            			 		" and (op.time_est_closing) at time zone 'GMT' + 0 >= ? " +
//         					    " and i.time_created >= ? -1" +
//            			 		" and op.is_settled = 1 " +
//            			 		" and op.opportunity_type_id = 1 " +
//            			 		" and (op.scheduled = 1 OR i.is_like_hourly  = 1 ) " +
//            			 		" and i.is_settled = 1 " +
//            			 		" and i.is_canceled = 0 " +
//            			 		" and i.copyop_type_id in (0,2) " +
//         					   " and u.id = cf.user_id " +
//         					   " and cf.is_frozen = 0 " +
//            			 	" )bts " +
//            			 	" WHERE profit >= " + minProfit +
//            			 " order by  " +
//            			 	" bts.profit desc, " +
//            			 	" bts.hit desc) WHERE rownum <= " + returnRecords + "ORDER BY profit, hit";
//
//			ps = con.prepareStatement(sql);
//			ps.setTimestamp(1, CommonUtil.convertToTimeStamp(tradingDateTimeBack));
//			ps.setTimestamp(2, CommonUtil.convertToTimeStamp(tradingDateTimeBack));
//			rs = ps.executeQuery();
//			int br = 0;
//			while(rs.next() && (br <= returnRecords - 1 )) {
//				FeedMessage hotBestTrades = new FeedMessage();
//				hotBestTrades.setUserId(FeedMessage.HOT_SYSTEM_FEED_USER_ID);
//				hotBestTrades.setQueueType(QueueTypeEnum.HOT.getId());
//				UUID timeCreatedUUID = UUIDs.timeBased();
//				hotBestTrades.setTimeCreatedUUID(timeCreatedUUID);
//				hotBestTrades.setTimeCreated(new Date(UUIDs.unixTimestamp(timeCreatedUUID)));
//				hotBestTrades.setMsgType(msgEnumType.getId());
//
//				Map<String, String> properties = new HashMap<String, String>();
//				properties.put(FeedMessage.PROPERTY_KEY_USER_ID, String.valueOf(rs.getLong("user_id")));
//				for (Map.Entry<String, String> entry :getAmountInAllCurr(rs.getDouble("profit"), rs.getLong("user_profit"), rs.getInt("currency_id")).entrySet()) {
//					properties.put(entry.getKey(), entry.getValue());
//				}
//				//properties.put(FeedMessage.PROPERTY_KEY_PROFIT, String.valueOf(rs.getDouble("profit")));
//				properties.put(FeedMessage.PROPERTY_KEY_MARKET_ID, rs.getString("market_id"));
//				Date assetCreateDateTime =rs.getTimestamp("inv_time_created");
//				properties.put(FeedMessage.PROPERTY_KEY_ASSET_CREATE_DATE_TIME,  new SimpleDateFormat("dd/MM/yyyy HH:mm").format(assetCreateDateTime));
//				properties.put(FeedMessage.PROPERTY_KEY_ASSET_LAST_TRADING_HOURS, String.valueOf(hours));
//				hotBestTrades.setProperties(properties);
//
//				list.add(hotBestTrades);
//				br ++;
//			}
//		} finally {
//			closeResultSet(rs);
//			closeStatement(ps);
//		}
//		return list;
//	}

//	public static ArrayList<FeedMessage> getHotBestCopiers(Connection con, long hours, long returnRecords, UpdateTypeEnum msgEnumType) throws SQLException {
//		Date tradingDateTimeBack = getTradingDateBack(con, hours);
//		PreparedStatement ps = null;
//		ResultSet rs = null;
//		ArrayList<FeedMessage> list = new ArrayList<FeedMessage>();
//
//		long minHit = new Long(ConfigurationCopyopManager.getCopyopPropertiesValue("HOT_BEST_COPIERS_MIN_HIT", "0"));
//		long minProfit = new Long(ConfigurationCopyopManager.getCopyopPropertiesValue("HOT_BEST_COPIERS_MIN_PROFIT", "10000"));
//		long aboveHit = new Long(ConfigurationCopyopManager.getCopyopPropertiesValue("HOT_BEST_COPIERS_ABOVE_HIT", "80"));
//		
//		try {
//            String sql = " SELECT * FROM (select * from " +
//            					" ( " +
//            					" select bts.user_id, bts.hit, " +
//            					" sum(bts.ret) as profit, " +
//            					" sum(bts.user_profit) as user_profit, " +
//            					" bts.currency_id " +
//            					" from " +
//            					" ( " +
//            					  " select i.user_id, ch.hit_copied as hit, " +
//            					  " ((i.amount - i.house_result) - i.amount) as user_profit, " +
//            					  "  u.currency_id, " +
//            					  " ((i.amount - i.house_result) - i.amount)*i.rate as Ret " +
//            					  " from " +
//            					   " copyop_hits ch, " +
//            					   " investments i, " +
//            					   " opportunities op," +
//            					   " users_active_data uad, " +
//            					   " users u, " +
//            					   " copyop_frozen cf " +
//            					  " where " +
//            					   " ch.user_id = i.user_id " +
//            					   " and u.id = i.user_id " +
//            					   " and u.is_active = 1 " +
//             					   " and u.class_id != " + ConstantsBase.USER_CLASS_TEST +
//             					   " and u.skin_id != " + Skin.SKIN_ETRADER +
//             					   " and u.id = uad.user_id " +
//             					   " and uad.copyop_user_status = " + UserStateEnum.STATE_REGULAR.getId() +
//            					   " and i.opportunity_id = op.id" +
//            					   " and (op.time_est_closing) at time zone 'GMT' + 0 >= ? " +
//            					   " and i.time_created >= ? -1" +
//            					   " and op.is_settled = 1 " +
//            					   " and op.opportunity_type_id = 1 " +
//            					   " and (op.scheduled = 1 OR i.is_like_hourly  = 1 ) " +
//            					   " and i.is_settled = 1 " +
//            					   " and i.is_canceled = 0 " +
//            					   " and i.copyop_type_id = 1 " +
//            					   " and u.id = cf.user_id " +
//            					   " and cf.is_frozen = 0 " +
//            					 " )bts " +
//            					 " group by " +
//            					 " bts.user_id, bts.hit, bts.currency_id " +
//            			  "  ) b_traders " +
//            			  " where (b_traders.profit >= " + minProfit +  " and b_traders.hit >= " + minHit + ") " +
//            			     " OR (b_traders.profit > 0 and  b_traders.hit >= " + aboveHit +  " ) " +
//            			  " order by " +
//            			  "	b_traders.profit desc, " +
//            			  " b_traders.hit desc ) WHERE rownum <= " + returnRecords + " ORDER BY profit, hit";
//
//			ps = con.prepareStatement(sql);
//			ps.setTimestamp(1, CommonUtil.convertToTimeStamp(tradingDateTimeBack));
//			ps.setTimestamp(2, CommonUtil.convertToTimeStamp(tradingDateTimeBack));
//			rs = ps.executeQuery();
//			while(rs.next()) {
//				FeedMessage hotBestCopiers = new FeedMessage();
//				hotBestCopiers.setUserId(FeedMessage.HOT_SYSTEM_FEED_USER_ID);
//				hotBestCopiers.setQueueType(QueueTypeEnum.HOT.getId());
//				UUID timeCreatedUUID = UUIDs.timeBased();
//				hotBestCopiers.setTimeCreatedUUID(timeCreatedUUID);
//				hotBestCopiers.setTimeCreated(new Date(UUIDs.unixTimestamp(timeCreatedUUID)));
//				hotBestCopiers.setMsgType(msgEnumType.getId());
//
//				Map<String, String> properties = new HashMap<String, String>();
//				properties.put(FeedMessage.PROPERTY_KEY_USER_ID, String.valueOf(rs.getLong("user_id")));
//				properties.put(FeedMessage.PROPERTY_KEY_HIT_RATE, String.valueOf(rs.getInt("hit")));
//				for (Map.Entry<String, String> entry :getAmountInAllCurr(rs.getDouble("profit"), rs.getLong("user_profit"), rs.getInt("currency_id")).entrySet()) {
//					properties.put(entry.getKey(), entry.getValue());
//				}
//				//properties.put(FeedMessage.PROPERTY_KEY_PROFIT, String.valueOf(rs.getDouble("profit")));
//				properties.put(FeedMessage.PROPERTY_KEY_ASSET_LAST_TRADING_HOURS, String.valueOf(hours));
//				hotBestCopiers.setProperties(properties);
//
//				list.add(hotBestCopiers);
//			}
//		} finally {
//			closeResultSet(rs);
//			closeStatement(ps);
//		}
//		return list;
//	}

//	public static ArrayList<FeedMessage> getSystemHighestHit(Connection con, long hours, long returnRecords) throws SQLException {
//		PreparedStatement ps = null;
//		ResultSet rs = null;
//		ArrayList<FeedMessage> list = new ArrayList<FeedMessage>();
//
//		long minHit = new Long(ConfigurationCopyopManager.getCopyopPropertiesValue("SYSTEM_HIGHEST_HIT_MIN_HIT", "70"));
//		long minProfit = new Long(ConfigurationCopyopManager.getCopyopPropertiesValue("SYSTEM_HIGHEST_HIT_MIN_PROFIT", "10000"));
//		long minInv = new Long(ConfigurationCopyopManager.getCopyopPropertiesValue("SYSTEM_HIGHEST_HIT_MIN_INV", "3"));
//
//		try {
//            String sql = " select * from " +
//            				" ( " +
//            						" select decode(sum(win),0,0,100/(count(*)/sum(win))) as hit , " +
//            							" count(*) as num_inv, sum(profit) as profit, user_id, currency_id, sum(user_profit) as user_profit " +
//            						" from " +
//            							" (select  case when i.win > i.lose then 1 else 0 end as win , " +
//            								" ((i.amount - i.house_result) - i.amount)*i.rate as profit, " +
//            								" ((i.amount - i.house_result) - i.amount) as user_profit, " +
//            								" u.currency_id, " +
//            								" i.user_id " +
//            							  " from " +
//            							  	" investments i, " +
//            							  	" opportunities op," +
//            							  	" users_active_data uad, " +
//            							  	" users u" +
//            							  " where i.opportunity_id = op.id " +
//            							  " and u.id = i.user_id " +
//                   					      " and u.is_active = 1 " +
//                 					      " and u.class_id != " + ConstantsBase.USER_CLASS_TEST +
//                 					      " and u.skin_id != " + Skin.SKIN_ETRADER +
//                 					      " and u.id = uad.user_id " +
//                 					      " and uad.copyop_user_status = " + UserStateEnum.STATE_REGULAR.getId() +
//            							  " and i.copyop_type_id in (0,2) " +
//            							  " and (op.scheduled = 1 OR i.is_like_hourly  = 1 ) " +
//            							  " and op.is_settled = 1 " +
//            							  " and i.is_settled = 1 " +
//            							  " and i.is_canceled = 0 " +
//            							  " and op.opportunity_type_id = 1 " +
//            							  " and i.time_created >= sysdate - 1/24*" + hours +
//            							 " ) inv " +
//            						" group by user_id, currency_id " +
//            				  " ) " +
//            				" where " +
//            					" num_inv >= " + minInv +
//            					" and hit >= " + minHit +
//            					" and profit >= " + minProfit +
//            					" and user_id not in (select user_id from copyop_frozen where is_frozen = 1) " +
//            					" and user_id in (select user_id from copyop_hits) " +
//            			   " order by hit desc, profit desc";
//
//			ps = con.prepareStatement(sql);
//			rs = ps.executeQuery();
//			int br = 0;
//			Map<Long, Integer> insertedUserMsg = FeedManager.getFeedPostedUsersMsgNow();
//			while(rs.next() && (br <= returnRecords -1)) {
//				FeedMessage systemHighestHit = new FeedMessage();
//
//				systemHighestHit.setUserId(FeedMessage.HOT_SYSTEM_FEED_USER_ID);
//				systemHighestHit.setQueueType(QueueTypeEnum.SYSTEM.getId());
//				UUID timeCreatedUUID = UUIDs.timeBased();
//				systemHighestHit.setTimeCreatedUUID(timeCreatedUUID);
//				systemHighestHit.setTimeCreated(new Date(UUIDs.unixTimestamp(timeCreatedUUID)));
//				systemHighestHit.setMsgType(UpdateTypeEnum.SYS_8H_HIGHEST_SR.getId());
//
//				Map<String, String> properties = new HashMap<String, String>();
//				properties.put(FeedMessage.PROPERTY_KEY_USER_ID, String.valueOf(rs.getLong("user_id")));
//				properties.put(FeedMessage.PROPERTY_KEY_HIT_RATE, String.valueOf(rs.getInt("hit")));
//				for (Map.Entry<String, String> entry :getAmountInAllCurr(rs.getDouble("profit"), rs.getLong("user_profit"), rs.getInt("currency_id")).entrySet()) {
//					properties.put(entry.getKey(), entry.getValue());
//				}
//				//properties.put(FeedMessage.PROPERTY_KEY_PROFIT, String.valueOf(rs.getDouble("profit")));
//				properties.put(FeedMessage.PROPERTY_KEY_ASSET_LAST_TRADING_HOURS, String.valueOf(hours));
//				systemHighestHit.setProperties(properties);
//
//				//No trader could be on the same day in more than 1 msgType
//				if(canBeInsertUser(rs.getLong("user_id"), UpdateTypeEnum.SYS_8H_HIGHEST_SR.getId(), insertedUserMsg)){
//					list.add(systemHighestHit);
//					br ++;
//				}
//			}
//		} finally {
//			closeResultSet(rs);
//			closeStatement(ps);
//		}
//		return list;
//	}

	public static boolean canBeInsertUser(long userId, int feedMsgType, Map<Long, Integer> insertedUserMsg) {
		//No trader could be on the same day in more than 1 msgType
		boolean res = true;
		if (insertedUserMsg.containsKey(userId)) {
			if (insertedUserMsg.get(userId) == feedMsgType) {
				res = false;
			}
		}
		return res;
	}

//	public static ArrayList<FeedMessage> getSystemTrendiestTrends(Connection con,long returnRecords) throws SQLException {
//		SimpleDateFormat formatterHour = new SimpleDateFormat("dd/MM/yyyy HH");
//		PreparedStatement ps = null;
//		ResultSet rs = null;
//		ArrayList<FeedMessage> list = new ArrayList<FeedMessage>();
//
//		Date nowH = null;
//		try {
//			nowH = formatterHour.parse(formatterHour.format(new Date()));
//		} catch (ParseException e) {
//			logger.error("When format date", e);
//		}
//		HashSet<Integer> postedAssets = FeedManager.getFeedPostedAsset(nowH);
//		logger.debug("postedAssets:" + postedAssets);
//
//		long minTrend = new Long(ConfigurationCopyopManager.getCopyopPropertiesValue("SYSTEM_TRENDIEST_TRENDS_MIN_TREND", "80"));
//		long minInv = new Long(ConfigurationCopyopManager.getCopyopPropertiesValue("SYSTEM_TRENDIEST_TRENDS_MIN_INV", "10"));
//		long thresholdHotBestTradersMessage = Long.parseLong(ConfigurationCopyopManager.getCopyopPropertiesValue("HOT_BEST_TRADERS_MESSAGE_TRESHOLD", "200"));
//
//		try {
//            String sql = " SELECT * FROM (" +
//		            				"  select GREATEST(call_pct, put_pct) as trend_pct, " +
//											" case when call_pct > put_pct then 1 else 2 end as type_id, " +
//											" id,  " +
//											" time_est_closing," +
//											" market_id," +
//											" inv_count  " +
//									  " from (select round(case  when sum(decode(type_id, 1, 1, 0)) != 0 then " +
//									                       " 100 / ((sum(decode(type_id, 1, 1, 0)) + sum(decode(type_id, 2, 1, 0))) / sum(decode(type_id, 1, 1, 0))) " +
//									                       " else 0  end,  2) as call_pct, " +
//									               " round(case when sum(decode(type_id, 2, 1, 0)) != 0 then " +
//									                       " 100 / ((sum(decode(type_id, 1, 1, 0)) + sum(decode(type_id, 2, 1, 0))) / sum(decode(type_id, 2, 1, 0))) " +
//									                       " else 0 end, 2) as put_pct, " +
//									               " op.id ," +
//									               " op.time_est_closing," +
//									               "count(i.id) as inv_count," +
//									               "op.market_id " +
//									          " from investments i, opportunities op, users_active_data uad, users u " +
//									         " where i.opportunity_id = op.id " +
//									           " and u.id = i.user_id" +
//				         					   " and u.is_active = 1 " +
//				         					   " and u.class_id != " + ConstantsBase.USER_CLASS_TEST +
//				         					   " and u.skin_id != " + Skin.SKIN_ETRADER +
//				         					   " and u.id = uad.user_id " +
//				         					   " and uad.copyop_user_status = " + UserStateEnum.STATE_REGULAR.getId() +
//									           " and op.is_published = 1 " +
//									           " and (op.scheduled = 1 OR i.is_like_hourly  = 1 ) " +
//									           " and op.opportunity_type_id = 1 " +
//									           " and i.copyop_type_id in (0, 2) " +
//									           " and op.is_settled = 0 " +
//									           " and i.is_canceled = 0 " +
//									           " and ((i.amount * i.rate)/100) > " + thresholdHotBestTradersMessage +
//									           " and i.time_created >= sysdate - 1 " +
//									           " and i.user_id not in (select user_id from copyop_frozen where is_frozen = 1) " +
//									         " group by op.id, op.time_est_closing, op.market_id) opp )" +
//                             " WHERE " +
//                             " trend_pct >= " + minTrend +
//                             " and inv_count >= " + minInv;
//
//			ps = con.prepareStatement(sql);
//			rs = ps.executeQuery();
//			int br = 0;
//
//			while(rs.next() && (br <= returnRecords -1)) {
//				FeedMessage systemTrendiestTrends = new FeedMessage();
//				systemTrendiestTrends.setUserId(FeedMessage.HOT_SYSTEM_FEED_USER_ID);
//				systemTrendiestTrends.setQueueType(QueueTypeEnum.SYSTEM.getId());
//				UUID timeCreatedUUID = UUIDs.timeBased();
//				systemTrendiestTrends.setTimeCreatedUUID(timeCreatedUUID);
//				systemTrendiestTrends.setTimeCreated(new Date(UUIDs.unixTimestamp(timeCreatedUUID)));
//				systemTrendiestTrends.setMsgType(UpdateTypeEnum.SYS_1H_TRENDIEST.getId());
//
//				Map<String, String> properties = new HashMap<String, String>();
//				properties.put(FeedMessage.PROPERTY_KEY_ASSET_TREND, String.valueOf(rs.getDouble("trend_pct")));
//				properties.put(FeedMessage.PROPERTY_KEY_INVESTMENT_TYPE, String.valueOf(rs.getLong("type_id")));
//				properties.put(FeedMessage.PROPERTY_KEY_OPPORTUNITY_EXPIRE, String.valueOf(rs.getDate("time_est_closing").getTime()));
//				properties.put(FeedMessage.PROPERTY_KEY_MARKET_ID, rs.getString("market_id"));
//				systemTrendiestTrends.setProperties(properties);
//
//				//Check if market are insert/show in some Hours.Couldn't be posted twice in the same hour.
//				if(!postedAssets.contains(rs.getInt("market_id"))){
//					list.add(systemTrendiestTrends);
//					br ++;
//				} else {
//					logger.debug("The marketId:" + rs.getInt("market_id") + " was posted in the same hour!");
//				}
//			}
//		} finally {
//			closeResultSet(rs);
//			closeStatement(ps);
//		}
//		return list;
//	}

//	private static Map<String, String> getAmountInAllCurr(double amountInUSD, long amount, long userCurrId){
//		Map<String, String> res = new HashMap<String, String>();
//
//		for (Map.Entry<Long, Double> entry : CurrenciesCacheManager.getCurrenciesRatesToUSD().entrySet()) {
//			Long currencyId = entry.getKey();
//			double rate = 0;
//			double amountForConvert = 0;
//			if(currencyId != userCurrId){
//			     rate = entry.getValue();
//			     amountForConvert = amountInUSD / 100;
//
//		    } else {
//			     rate = 1;
//			     amountForConvert = amount / 100;
//		    }
//
//			BigDecimal convertedAmount = new BigDecimal(amountForConvert).multiply(new BigDecimal(rate)).setScale(2, RoundingMode.CEILING);
//			res.put(FeedMessage.PROPERTY_KEY_PROFIT + "_" + currencyId, "" + convertedAmount.doubleValue());
//
//		}
//		// Add USD
//		amountInUSD = amountInUSD / 100;
//		BigDecimal convertedAmount = new BigDecimal(amountInUSD).multiply(new BigDecimal(1)).setScale(2, RoundingMode.CEILING);
//		res.put(FeedMessage.PROPERTY_KEY_PROFIT + "_" + Currency.CURRENCY_USD_ID, "" + convertedAmount.doubleValue());
//
//		return res;
//	}

//	public static ArrayList<FeedMessage> getTopProfitable(Connection con) throws SQLException {
//		PreparedStatement ps = null;
//		ResultSet rs = null;
//		ArrayList<FeedMessage> list = new ArrayList<FeedMessage>();
//
//		long hours = new Long(ConfigurationCopyopManager.getCopyopPropertiesValue("TOP_PROFITABLE_LAST_HOURS", "24"));
//		long topCount = new Long(ConfigurationCopyopManager.getCopyopPropertiesValue("TOP_PROFITABLE_RANK_NUMBER", "100"));
//
//		try {
//            String sql = "select *  from " +
//            				" (select " +
//                                 " sum(((i.amount - i.house_result) - i.amount)*i.rate) as profit, " +
//	                             " sum(((i.amount - i.house_result) - i.amount)) as user_profit, " +
//	                             " u.currency_id, " +
//	                             " u.skin_id, " +
//	                             " i.user_id " +
//                             " from " +
//                               " investments i, " +
//                               " opportunities op, " +
//                               " users_active_data uad, " +
//                               " users u " +
//                             " where i.opportunity_id = op.id " +
//                             	" and u.id = i.user_id " +
//                                " and u.is_active = 1 " +
//                                " and u.class_id != " + ConstantsBase.USER_CLASS_TEST +
//                                " and u.skin_id != " + Skin.SKIN_ETRADER +
//                                " and u.id = uad.user_id " +
//                                " and uad.copyop_user_status = " + UserStateEnum.STATE_REGULAR.getId() +
//                                " and i.copyop_type_id in (0,2) " +
//	                             " and (op.scheduled = 1 OR i.is_like_hourly  = 1 ) " +
//	                             " and op.is_settled = 1 " +
//	                             " and i.is_settled = 1 " +
//	                             " and i.is_canceled = 0 " +
//	                             " and op.opportunity_type_id = 1 " +
//	                             " and  i.time_settled >= sysdate - 1/24*" +  hours +
//	                             " and i.time_created >= sysdate - 3 " +
//                              " group by u.currency_id, u.skin_id, " +
//                                " i.user_id order by 1 desc) top " +
//                             " where profit > 0 " +
//                             " and rownum <= " +  topCount;
//
//			ps = con.prepareStatement(sql);
//			rs = ps.executeQuery();
//			int br = 1;
//			int rankedTop = 0;
//			while(rs.next()) {
//				FeedMessage topProfitable = new FeedMessage();
//				topProfitable.setUserId(rs.getLong("user_id"));
//				UUID timeCreatedUUID = UUIDs.timeBased();
//				topProfitable.setTimeCreatedUUID(timeCreatedUUID);
//				topProfitable.setTimeCreated(new Date(UUIDs.unixTimestamp(timeCreatedUUID)));
//				topProfitable.setMsgType(UpdateTypeEnum.AB_IM_TOP_PROFITABLE.getId());
//
//				Map<String, String> properties = new HashMap<String, String>();
//				properties.put(FeedMessage.PROPERTY_KEY_USER_ID, String.valueOf(rs.getLong("user_id")));
//				properties.put(FeedMessage.PROPERTY_KEY_PUSH_SKIN_ID, rs.getString("skin_id"));
//				properties.put(FeedMessage.PROPERTY_KEY_TOP_RANK, String.valueOf(br));
//				if(br < 6){
//					rankedTop = 5;
//				} else if(br > 5  && br <= 10 ){
//					rankedTop = 10;
//				} else if(br > 10  && br <= 50 ){
//					rankedTop = 50;
//				} else {
//					rankedTop = 100;
//				}
//				properties.put(FeedMessage.PROPERTY_KEY_RANKED_IN_TOP, String.valueOf(rankedTop));
//				double amountForConvert = rs.getLong("user_profit") / 100;
//				int currencyId = rs.getInt("currency_id");
//				BigDecimal convertedAmount = new BigDecimal(amountForConvert).multiply(new BigDecimal(1)).setScale(2, RoundingMode.CEILING);
//				properties.put(FeedMessage.PROPERTY_KEY_PROFIT + "_" + currencyId, "" + convertedAmount.doubleValue());
//
//				topProfitable.setProperties(properties);
//				list.add(topProfitable);
//				br ++;
//			}
//		} finally {
//			closeResultSet(rs);
//			closeStatement(ps);
//		}
//		return list;
//	}

}
