/**
 * 
 */
package com.anyoption.common.daos;

import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Types;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.bouncycastle.crypto.CryptoException;

import com.anyoption.common.beans.Bonus;
import com.anyoption.common.beans.BubblesToken;
import com.anyoption.common.beans.InvestmentRejects;
import com.anyoption.common.beans.Opportunity;
import com.anyoption.common.beans.Transaction;
import com.anyoption.common.beans.User;
import com.anyoption.common.beans.UserActiveData;
import com.anyoption.common.beans.UserMigration;
import com.anyoption.common.beans.Writer;
import com.anyoption.common.beans.base.MarketingInfo;
import com.anyoption.common.beans.base.User.MobileNumberValidation;
import com.anyoption.common.beans.base.UsersDetailsHistory;
import com.anyoption.common.managers.InvestmentRejectsManagerBase;
import com.anyoption.common.managers.IssuesManagerBase;
import com.anyoption.common.managers.PopulationsManagerBase;
import com.anyoption.common.util.AESUtil;
import com.anyoption.common.util.CommonUtil;
import com.anyoption.common.util.ConstantsBase;
import com.anyoption.common.util.DbmsOutput;
import com.anyoption.common.util.Key;
//import com.copyop.common.beans.UserUniqueDetails;
//import com.copyop.common.enums.base.UserStateEnum;

import oracle.jdbc.OracleTypes;


/**
 * @author kirilim
 *
 */
public class UsersDAOBase extends DAOBase {

    public static final Logger log = Logger.getLogger(UsersDAOBase.class);
	protected static final String ACTIVE_SKIN_CONSTRAINT = "ETRADER.CHECK_SKIN_ACTIVE";

    /**
     * This method loads user info specifically for the login logic
     * @param conn
     * @param userName
     * @param user
     * @throws SQLException
     */
    public static void loadUserByName(Connection conn, String userName, User user) throws SQLException {
    	 if (null == userName || userName.trim().equals("")) {
             return;
         }

         PreparedStatement pstmt = null;
         ResultSet rs = null;
         try {
			String sql = "SELECT u.*, uad.* "
						+ "FROM USERS u, USERS_ACTIVE_DATA uad "
						+ "WHERE u.ID = uad.USER_ID "
						+ "AND USER_NAME = ? ";
             pstmt = conn.prepareStatement(sql);
             pstmt.setString(1, userName.toUpperCase());
             rs = pstmt.executeQuery();
             if (rs.next()) {
                 loadUserVO(rs, user);
 				Hashtable<String, Boolean> productViewFlags = new Hashtable<String, Boolean>();
 				productViewFlags.put(ConstantsBase.HAS_DYNAMICS_FLAG, rs.getBoolean("has_dynamics"));
 				if (CommonUtil.getProperty("isHasBubbles.always.flag").equalsIgnoreCase("true")) {
					productViewFlags.put(ConstantsBase.HAS_BUBBLES_FLAG, true);
				} else {					
					productViewFlags.put(ConstantsBase.HAS_BUBBLES_FLAG, rs.getBoolean("has_bubbles"));
				}
 				user.setProductViewFlags(productViewFlags);
 				user.setShowCancelInvestment(rs.getBoolean("show_cancel_investment"));
 				user.setNonRegSuspend(rs.getBoolean("IS_NON_REG_SUSPEND"));
             }
         } finally {
             closeResultSet(rs);
             closeStatement(pstmt);
         }
    }
    
    public static void loadUserVO(ResultSet rs, User vo) throws SQLException {
    	vo.setId(rs.getLong("id"));
    	vo.setUserName(rs.getString("USER_NAME"));
    	try {
			vo.setPassword(AESUtil.decrypt(rs.getString("password")));
		} catch (	CryptoException | InvalidKeyException | IllegalBlockSizeException | BadPaddingException | NoSuchAlgorithmException
					| NoSuchPaddingException | InvalidAlgorithmParameterException ce) {
			// FIXME very bad way of handling exceptions
			throw new SQLException("CryptoException: " + ce.getMessage());
		}
    	vo.setIsActive(rs.getString("IS_ACTIVE"));
    	vo.setLastFailedTime(convertToDate(rs.getTimestamp("last_failed_time")));
    	vo.setFailedCount(rs.getInt("failed_count"));
    	vo.setSkinId(rs.getLong("skin_id"));
    	vo.setDocsRequired(rs.getBoolean("docs_required"));
    	vo.setBalance(rs.getLong("BALANCE"));
    	vo.setTaxBalance(rs.getLong("TAX_BALANCE"));
    	vo.setLimitId(rs.getLong("limit_id"));
    	
    	vo.setCurrencyId(rs.getLong("CURRENCY_ID"));
        vo.setFirstName(rs.getString("FIRST_NAME"));
        vo.setLastName(rs.getString("LAST_NAME"));
        vo.setStreet(rs.getString("STREET"));
        vo.setStreetNo(rs.getString("STREET_no"));
        vo.setCityId(rs.getLong("CITY_ID"));
        vo.setZipCode(rs.getString("ZIP_CODE"));

        if(rs.getTimestamp("TIME_CREATED")!=null) {
        	vo.setTimeCreated(convertToDate(rs.getTimestamp("TIME_CREATED")));
        }
        vo.setTimeBirthDate(convertToDate(rs.getTimestamp("TIME_BIRTH_DATE")));
        if(rs.getTimestamp("TIME_FIRST_VISIT")!=null) {
        	vo.setTimeFirstVisit(convertToDate(rs.getTimestamp("TIME_FIRST_VISIT")));
        }

        vo.setEmail(rs.getString("EMAIL"));
        vo.setCombinationId(rs.getLong("combination_id"));

        vo.setIsContactByEmail(rs.getInt("IS_CONTACT_BY_EMAIL"));
        vo.setMobilePhone(rs.getString("MOBILE_PHONE"));
        vo.setLandLinePhone(rs.getString("LAND_LINE_PHONE"));
        vo.setGender(rs.getString("GENDER"));
        try {
        	if(rs.getString("id_num")!=null){
        	vo.setIdNum(AESUtil.decrypt(rs.getString("id_num")));}
		} catch (	CryptoException | InvalidKeyException | IllegalBlockSizeException | BadPaddingException | NoSuchAlgorithmException
					| NoSuchPaddingException | InvalidAlgorithmParameterException ce) {
			// FIXME very bad way of handling exceptions
			throw new SQLException("CryptoException: " + ce.getMessage());
		}

        vo.setIsContactBySMS(rs.getLong("is_contact_by_sms"));
        vo.setIsContactByPhone(rs.getLong("is_contact_by_phone"));

        // for anyOption
        vo.setCountryId(rs.getLong("country_id"));

        vo.setUtcOffset(rs.getString("utc_offset"));

        vo.setState(rs.getLong("state_code"));

        vo.setDynamicParam(rs.getString("dynamic_param"));

		if (rs.getInt("is_accepted_terms") == 1){
			vo.setAcceptedTerms(true);
		} else {
			vo.setAcceptedTerms(false);
		}


        vo.setContactId(rs.getLong("contact_id"));
        vo.setUserAgent(rs.getString("user_agent"));
        vo.setDeviceUniqueId(rs.getString("device_unique_id"));
        vo.setFirstDepositId(rs.getLong("first_deposit_id"));
        vo.setStatusId(rs.getLong("status_id"));
        vo.setStopReverseWithdrawOption(rs.getBoolean("is_stop_reverse_withdraw"));

        ResultSetMetaData rsmd = rs.getMetaData();

        for (int i = 1; i <= rsmd.getColumnCount(); i++) {

        	if("con_id".equalsIgnoreCase(rsmd.getColumnName(i))){
        		vo.setContactId(rs.getLong("con_id"));
        	}
        	if("first_name_uc".equalsIgnoreCase(rsmd.getColumnName(i))){
        		vo.setFirstName(rs.getString("first_name_uc"));
        	}
        	if("last_name_uc".equalsIgnoreCase(rsmd.getColumnName(i))){
        		vo.setLastName(rs.getString("last_name_uc"));
        	}
        	if("email_uc".equalsIgnoreCase(rsmd.getColumnName(i))){
        		vo.setEmail(rs.getString("email_uc"));
        	}
        	if("mobile_uc".equalsIgnoreCase(rsmd.getColumnName(i))){
        		vo.setMobilePhone(rs.getString("mobile_uc"));
        	}
        	if("phone_uc".equalsIgnoreCase(rsmd.getColumnName(i))){
        		vo.setLandLinePhone(rs.getString("phone_uc"));
        	}
        	if("skin_id_uc".equalsIgnoreCase(rsmd.getColumnName(i))){
        		vo.setSkinId(rs.getLong("skin_id_uc"));
        	}
        }
        vo.setPlatformId(rs.getInt("platform_id"));
        vo.setMobileNumberValidated(rs.getInt("mobile_phone_validated"));
	}
    
    /**
	 * Check if there is existing user with the specified email.
	 *
	 * @param conn the db connection to use
	 * @param email the email to check
	 * @param userId the user id that should not be checked
     * @return <code>true</code> if the email is in use else
	 *         <code>false</code>.
	 * @throws DAORuntimeException
	 */
	public static boolean isEmailInUse(Connection conn, String email, long userId) throws SQLException {
		boolean inUse = false;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		if (CommonUtil.containsTestDomain(email)) {  // for testing
			return false;
		}
		try {
			String sql = "SELECT " +
							"id " +
						 "FROM " +
						 	"users " +
						 "WHERE " +
						 	"upper(email) = upper(?) ";
			if (userId != 0) {
				sql += "AND id <> " + userId;
			}
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, email);
			rs = pstmt.executeQuery();
			if (rs.next()) {
				inUse = true;
			}
		} finally {
			closeResultSet(rs);
			closeStatement(pstmt);
		}
		return inUse;
	}

	public static boolean isEmailInUseIgnoreCase(Connection conn, String email) throws SQLException {
		boolean inUse = false;
		if(email == null){
			return false;
		}
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		if (CommonUtil.containsTestDomain(email)) {  // for testing
			return false;
		}
		try {
			String sql = "SELECT " +
							"id " +
						 "FROM " +
						 	"users " +
						 "WHERE " +
						 	"UPPER(email) = upper(?) ";
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, email.toUpperCase());
			rs = pstmt.executeQuery();
			if (rs.next()) {
				inUse = true;
			}
		} finally {
			closeResultSet(rs);
			closeStatement(pstmt);
		}
		return inUse;
	}
	
	public static boolean isEmailInUseIgnoreCase(Connection conn, String email, long userId) throws SQLException {
		boolean inUse = false;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		if (CommonUtil.containsTestDomain(email)) {  // for testing
			return false;
		}
		try {
			String sql = "SELECT " +
							"id " +
						 "FROM " +
						 	"users " +
						 "WHERE " +
						 	"UPPER(email) =UPPER(?) ";
			if (userId != 0) {
				sql += "AND id <> " + userId;
			}
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, email);
			rs = pstmt.executeQuery();
			if (rs.next()) {
				inUse = true;
			}
		} finally {
			closeResultSet(rs);
			closeStatement(pstmt);
		}
		return inUse;
	}
	
	/**
	 * Check if there is existing user with the specified user name.
	 *
	 * @param conn the db connection to use
	 * @param userName the user name to check
	 * @param checkDomain whether to check if it is on test domain or not
	 * @param userWriter used as out parameter for the writerId. If not needed pass it as null
	 * @return <code>true</code> if the user name is in use else
	 *         <code>false</code>.
	 * @throws DAORuntimeException
	 */
	public static boolean isUserNameInUse(Connection conn, String userName, boolean checkDomain, Writer userWriter) throws SQLException {
		boolean inUse = false;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		if (checkDomain && CommonUtil.containsTestDomain(userName)) {  // for testing
			return false;
		}
		try {
			String sql = "SELECT id, writer_id " +
						 "FROM users " +
						 "WHERE upper(user_name) = ? or upper(user_name) = ?";
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1,userName.toUpperCase());
			pstmt.setString(2,(userName + ConstantsBase.CAL_GAME_USER_NAME_SUFFIX).toUpperCase());
			rs = pstmt.executeQuery();
			if (rs.next()) {
				inUse = true;
				if (userWriter != null) {
					userWriter.setId(rs.getLong("writer_id"));
				}
			}
		} finally {
			closeResultSet(rs);
			closeStatement(pstmt);
		}
		return inUse;
	}
	
	public static boolean isUserNameInUseIgnoreCase(Connection conn, String userName, long userId) throws SQLException {
		boolean inUse = false;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {
			String sql = "SELECT " +
							"id " +
						 "FROM " +
						 	"users " +
						 "WHERE " +
						 	"UPPER(user_name) = UPPER(?) ";
			if (userId != 0) {
				sql += "AND id <> " + userId;
			}
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, userName);
			rs = pstmt.executeQuery();
			if (rs.next()) {
				inUse = true;
			}
		} finally {
			closeResultSet(rs);
			closeStatement(pstmt);
		}
		return inUse;
	}
	
	public static void insertPasswordRecoveryHistory(Connection con, int writer, String email, String mobilePhone, int status) throws SQLException {
		
		PreparedStatement ps = null;
		try {
			String sql =
					"INSERT " +
						"INTO password_recovery_history " +
						"VALUES " +
							"(SEQ_PASSWORD_RECOVERY_HISTORY.NEXTVAL,sysdate,?,?,?,?)";

			ps = (PreparedStatement) con.prepareStatement(sql);
			ps.setInt(1, writer);
			ps.setString(2, email);
			ps.setString(3, mobilePhone);
			ps.setInt(4, status);
			ps.executeUpdate();
		} finally {
			closeStatement(ps);
		}
	}
	
	public static long updateUserFields(Connection conn,
										long userId,
										long currencyId, 
										String streetAddr,
										String streetNo,
										String zipCode,
										String cityName,
										long skinId,
										Date birthDate,
										String idNum,
										String gender,
										long cityId) throws SQLException {
		String sql = "begin "
						+ " UPDATE users u "
							+ " SET currency_id = ?, "
							+ " street = ?, "
							+ " street_no = ?, "
							+ " zip_code =  ?, "
							+ " city_name = ?, "
							+ " time_birth_date = ?, "							
							+ " id_num = ?, "
							+ " id_num_back = ?, "
							+ " gender = ?, "
							+ " city_id = ?, " 
							
							+ " limit_id = (SELECT limit_id "
												+ " FROM skin_currencies "
												+ " WHERE skin_id   = ? "
												+ " AND currency_id = ? "
												+ " AND is_default  = 1) "
						+ " WHERE id = ? returning limit_id into ?; "
					+ " end; ";
		
		CallableStatement cs = null;
		try {
			cs = conn.prepareCall(sql);
			cs.setLong(1, currencyId);
			cs.setString(2, streetAddr);
			cs.setString(3, streetNo);
			cs.setString(4, zipCode);
			cs.setString(5, cityName);
			cs.setTimestamp(6, CommonUtil.convertToTimeStamp(birthDate));
			try {
				cs.setString(7, AESUtil.encrypt(idNum));
			} catch (	CryptoException | InvalidKeyException | NoSuchAlgorithmException | NoSuchPaddingException | IllegalBlockSizeException
						| BadPaddingException | UnsupportedEncodingException | InvalidAlgorithmParameterException e) {
				log.error("When encrypt idNum", e);
			}
			cs.setString(8, idNum);
			cs.setString(9, gender);
			cs.setLong(10, cityId);
			cs.setLong(11, skinId);
			cs.setLong(12, currencyId);
			cs.setLong(13, userId);
			cs.registerOutParameter(14, Types.NUMERIC);
			cs.execute();
			return cs.getLong(14);
		} finally {
			closeStatement(cs);
		}		
	}
	
	
	public static long updateUserFields(Connection conn, 
										long userId,
										long currencyId, 
										String streetAddr, 
										String zipCode,
										String cityName, 
										long skinId, 
										Date birthDate) throws SQLException {
			String sql = "begin "
				+ " UPDATE users u "
				+ " SET currency_id = ?, "
				+ " street = ?, "
				+ " zip_code =  ?, "
				+ " city_name = ?, "
				+ " time_birth_date = ?, "
				+ " limit_id = (SELECT limit_id "
									+ " FROM skin_currencies "
									+ " WHERE skin_id   = ? "
									+ " AND currency_id = ? "
									+ " AND is_default  = 1) "
				+ " WHERE id = ? returning limit_id into ?; "
				+ " end; ";

		CallableStatement cs = null;
		try {
			cs = conn.prepareCall(sql);
			cs.setLong(1, currencyId);
			cs.setString(2, streetAddr);
			cs.setString(3, zipCode);
			cs.setString(4, cityName);
			cs.setTimestamp(5, CommonUtil.convertToTimeStamp(birthDate));
			cs.setLong(6, skinId);
			cs.setLong(7, currencyId);
			cs.setLong(8, userId);
			cs.registerOutParameter(9, Types.NUMERIC);
			cs.execute();
			return cs.getLong(9);
		} finally {
			closeStatement(cs);
		}
	}

	/**
	 * return user balance by given userId
	 * @param conn
	 * @param userId - unique identifier.
	 * @return user balance.
	 * @throws SQLException
	 */
	public static long getUserBalance(Connection conn, long userId) throws SQLException {
		PreparedStatement pstmt = null;
        ResultSet rs = null;
        long balance = 0;

        try {
            String sql =
            	"SELECT " +
            	"	u.balance " +
            	"FROM " +
            	"	users u " +
            	"WHERE " +
            	"	u.id = ? ";
            
            pstmt = conn.prepareStatement(sql);
            pstmt.setLong(1, userId);
            rs = pstmt.executeQuery();
            
            if (rs.next()) {
            	balance = rs.getLong("balance");
            }
        } finally {
            closeResultSet(rs);
            closeStatement(pstmt);
        }
       return balance;		
	}
	
	public static boolean updateUserSkin(Connection con, long userId, long newSkinId) throws SQLException {
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {
			String sql = "UPDATE " +
									" users " +
						" SET " +
									" skin_id = ? " +
						" WHERE " + 
									" id = ? ";

			pstmt = con.prepareStatement(sql);
			pstmt.setLong(1, newSkinId);
			pstmt.setLong(2, userId);
			return pstmt.executeUpdate()>0;
		} finally {
			closeResultSet(rs);
			closeStatement(pstmt);
		}
	}
	
	/**
	 * Get user cardinals by UUID
	 * @param con
	 * @return
	 * @throws SQLException
	 * @throws CryptoException 
	 * @throws NoSuchPaddingException 
	 * @throws NoSuchAlgorithmException 
	 * @throws BadPaddingException 
	 * @throws IllegalBlockSizeException 
	 * @throws InvalidKeyException 
	 * @throws InvalidAlgorithmParameterException 
	 */
	public static User getUserByLoginToken(	Connection con, String uuid,
											long userId)	throws SQLException, CryptoException, InvalidKeyException,
															IllegalBlockSizeException, BadPaddingException, NoSuchAlgorithmException,
															NoSuchPaddingException, InvalidAlgorithmParameterException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		User user = null;
		try {
			String sql= " SELECT " +
						"	u.user_name, " +
						"	u.password, " +
						"	u.skin_id " +												
						" FROM " +
						"	api_login_token alt," +
						"   users u " +
						" WHERE " +
						"   u.id = alt.user_id " +
						"	AND uuid = ? " +
						"	AND u.id = ? " +
						"	AND SYSDATE  <=  alt.time_created + INTERVAL '30' MINUTE ";
	        ps = con.prepareStatement(sql);	        
			ps.setString(1, uuid);
			ps.setLong(2, userId);
			rs = ps.executeQuery();
	        if (rs.next()) {
	        	user = new User();
	        	user.setUserName(rs.getString("user_name"));
	        	user.setPassword(AESUtil.decrypt(rs.getString("password")));
	        	user.setSkinId(rs.getLong("skin_id"));
	        }			
		} finally {
			closeResultSet(rs);
	        closeStatement(ps);
		}
		return user;
	}
	
	/**
	 * insert users details history
	 * @param con
	 * @param writerId
	 * @param userId
	 * @param userName
	 * @param concatenationTypeId
	 * @param concatenationOldData
	 * @param concatenationNewData
	 * @param classId
	 * @throws SQLException
	 */
	public static void insertUsersDetailsHistory(Connection con, long writerId, long userId,  String userName, ArrayList<UsersDetailsHistory> usersDetailsHistoryList, String classId, long seq) throws SQLException {
		PreparedStatement ps = null;
		try {
			String sql = " INSERT INTO users_details_history (ID, TIME_CREATED, WRITER_ID, USER_ID, USER_NAME, LAST_REACHED, CLASS_ID)" +
						 " VALUES (?, " +
						 		" sysdate,?,?,?, ";
						sql +=  " (SELECT " +
					 				" ia_last_call.action_time " +
					 			" FROM " +
					 				" population_users pu " +
					 					" LEFT JOIN issues i_last_call on pu.curr_population_entry_id = i_last_call.population_entry_id " +
					 						" LEFT JOIN issue_actions ia_last_call on i_last_call.last_call_action_id = ia_last_call.id " +
					 							" LEFT JOIN issue_action_types iat_last_call on iat_last_call.id = ia_last_call.issue_action_type_id and iat_last_call.reached_status_id = ? " +
					 			" WHERE " +
					 				" pu.user_id = ?), ";
					if (CommonUtil.isParameterEmptyOrNull(classId)) {
						sql +=  " (SELECT u.class_id FROM users u WHERE u.id = ?)) ";
					} else {
						sql +=  " ?) ";
					}	

			ps = (PreparedStatement) con.prepareStatement(sql);
			ps.setLong(1, seq);
			ps.setLong(2, writerId);
			ps.setLong(3, userId);
			ps.setString(4, userName.toUpperCase());
			ps.setLong(5, IssuesManagerBase.ISSUE_REACHED_STATUS_REACHED);
			ps.setLong(6, userId);
			if (CommonUtil.isParameterEmptyOrNull(classId)) {
				ps.setLong(7, userId);
			} else {
				ps.setLong(7, Long.parseLong(classId));
			}
			ps.executeUpdate();
		} finally {
			closeStatement(ps);
		}
	}
	
	/**
	 * insert users details history changes
	 * @param con
	 * @param userId
	 * @param usersDetailsHistory
	 * @param usersDetailsHistoryId
	 * @throws SQLException
	 */
	public static void insertUsersDetailsHistoryChanges(Connection con, long userId, ArrayList<UsersDetailsHistory> usersDetailsHistoryList, long usersDetailsHistoryId) throws SQLException {
        PreparedStatement ps = null;
        try {
            String sql = " INSERT INTO users_details_history_changes (ID, USERS_DETAILS_HISTORY_ID, USERS_DETAILS_HISTORY_TYPE_ID, FIELD_BEFORE, FIELD_AFTER)" +
                            " VALUES (SEQ_USERS_DETAILS_HIS_CHANGES.NEXTVAL,?,?,?,?)";
            ps = (PreparedStatement) con.prepareStatement(sql);
            for (UsersDetailsHistory udh : usersDetailsHistoryList) {
            	ps.setLong(1, usersDetailsHistoryId);
                ps.setLong(2, udh.getTypeId());
                ps.setString(3, udh.getFieldBefore());
                ps.setString(4, udh.getFieldAfter());
                ps.addBatch();
            }
            ps.executeBatch();
        } finally {
            closeStatement(ps);
        }
    }
	
	/**
	 * Insert users details history changes CC
	 * @param con
	 * @param userId
	 * @param udh
	 * @param usersDetailsHistoryId
	 * @throws SQLException
	 */
	public static void insertUsersDetailsHistoryChangesCC(Connection con, long userId, UsersDetailsHistory udh, long usersDetailsHistoryId) throws SQLException {
        PreparedStatement ps = null;
        try {
        	String sql = " INSERT INTO users_details_history_changes (ID, USERS_DETAILS_HISTORY_ID, USERS_DETAILS_HISTORY_TYPE_ID, FIELD_BEFORE, FIELD_AFTER)" +
                    		" VALUES (seq_users_details_his_changes.nextval, ?, ?, " +
		                    			" (SELECT " +
				                            " cc_num.* " +
				                        " FROM " +
				                            "(SELECT " +
				                                " cc.cc_number " +
				                            " FROM " +
				                                " credit_cards cc " +
				                            " WHERE " +
				                                " cc.user_id = ? " +
				                            " ORDER BY " +
				                                " cc.time_created DESC) cc_num " +
				                        " WHERE " +
				                            " rownum <= 1) , ?) ";
            ps = (PreparedStatement) con.prepareStatement(sql);
            ps.setLong(1, usersDetailsHistoryId);
            ps.setLong(2, udh.getTypeId());
            ps.setLong(3, userId);
            ps.setString(4, udh.getFieldAfter());
            ps.executeUpdate();
        } finally {
            closeStatement(ps);
        }
    }
	
	public static void addToBalance(Connection con, long id, long amount) throws SQLException {
		PreparedStatement ps = null;
		if (log.isEnabledFor(Level.DEBUG)) {
			String ls = System.getProperty("line.separator");
			log.log(Level.DEBUG,"Adding to balance - "+amount+" to user:"+id + ls );
		}
		try {
			String sql =
				" update users " +
				" set " +
					" balance = balance + ?," +
					" time_modified = sysdate, " +
					" utc_offset_modified = utc_offset " +
				" where id = ?";

			ps =  con.prepareStatement(sql);
			ps.setLong(1,amount);
			ps.setLong(2,id);
			ps.executeUpdate();
		} finally {
			closeStatement(ps);
		}
	}
	
	/**
	 * @param connection
	 * @param userId
	 * @return
	 * @throws SQLException
	 */
	public static User getUserWithReward(Connection connection, long userId) throws SQLException {
		PreparedStatement ps = null;
		ResultSet resultSet = null;
		User user = null;

		try {
			String sql = 
					"SELECT " +
					"	u.*" +
					"	,ru.id as ru_id" +
					"	,ru.user_id" +
					"	,ru.rwd_tier_id" +
					"	,ru.exp_points" +
					"	,ru.time_last_activity" +
					"	,ru.time_last_reduced_points " +
					"FROM" +
					"	users u" +
					"	,rwd_users ru " +
					"WHERE " +
					"	u.id = ru.user_id" +
					"	AND u.id = ? " +
					"FOR UPDATE";

			ps = connection.prepareStatement(sql);
			ps.setLong(1,userId);
			resultSet = ps.executeQuery();
			if (resultSet.next()) {
				user = new User();
				loadUserVO(resultSet, user);
				user.setRewardUser(RewardUsersDAO.getRewardUserValueObject(resultSet, "ru_id"));			
			} else {
				log.info(ConstantsBase.REWARD_PLAN_LOG_PREFIX + "No UserId: " + userId + " In rwd_users table!!!");
			}
		} finally {
			closeResultSet(resultSet);
			closeStatement(ps);
		}
		return user;
	}
	
	/**
	 * @param connection
	 * @param userId
	 * @return
	 * @throws SQLException
	 */
	public static User getUser(Connection connection, long userId) throws SQLException {
		PreparedStatement ps = null;
		ResultSet resultSet = null;
		User user = null;

		try {
			String sql = 
					"SELECT " +
					"	u.* " +
					"FROM" +
					"	users u " +
					"WHERE " +
					"	u.id = ?";

			ps = connection.prepareStatement(sql);
			ps.setLong(1,userId);
			resultSet = ps.executeQuery();
			if (resultSet.next()) {
				user = new User();
				loadUserVO(resultSet, user);	
			} 
		} finally {
			closeResultSet(resultSet);
			closeStatement(ps);
		}
		return user;
	}
	
	public static void updateUserLastLoginId(Connection con, long userId, long lastLoginId) throws SQLException {
		PreparedStatement ps = null;
		if (log.isEnabledFor(Level.DEBUG)) {
			String ls = System.getProperty("line.separator");
			log.log(Level.DEBUG,"Updating user's last login ID - "+lastLoginId+" to user:"+userId + ls );
		}
		try {
			String sql =
				" update users " +
				" set " +
					" last_login_id = ? " +
				" where id = ?";

			ps =  con.prepareStatement(sql);
			ps.setLong(1,lastLoginId);
			ps.setLong(2,userId);
			ps.executeUpdate();
		} finally {
			closeStatement(ps);
		}
	}
		
	public static void lockUserTable(Connection con, long userId)
			throws SQLException {

		PreparedStatement ps = null;
		ResultSet rs = null;

		try {
			String sql = " select * from users where id = ? FOR UPDATE ";
			ps = con.prepareStatement(sql);
			ps.setLong(1, userId);

			rs = ps.executeQuery();
		}

		finally {
			closeResultSet(rs);
			closeStatement(ps);
		}

		log.debug("!!!Locked userId:" + userId);
	}
	
	public static HashMap<Long, User> getUserFirstLastName(Connection connection, List<Long> userIds) throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		if (userIds.isEmpty()) {
			return new HashMap<Long, User>();
		}
		String userIdsSting = CommonUtil.getArrayToString(userIds);
		HashMap<Long, User> hm = new HashMap<Long, User>();
		try {
			String sql = 
					"SELECT " +
					"	u.* " +
					"FROM" +
					"	users u " +
					"WHERE " +
					"	u.id in ( " + userIdsSting + ")";

			ps = connection.prepareStatement(sql);
			rs = ps.executeQuery();
			while (rs.next()) {
				User user = new User();
				user.setFirstName(rs.getString("first_name"));
				user.setLastName(rs.getString("last_name"));
				hm.put(rs.getLong("id"), user);
			} 
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		return hm;
	}
	
	//Not the best name :)
	/**
	 * precondition: usersIds is not null || is not empty || size > 0
	 * @param conn
	 * @param usersIds
	 * @return
	 * @throws SQLException
	 */
//	public static HashMap<Long, UserUniqueDetails> getUserUniqueDetails(Connection conn, StringBuilder usersIds) throws SQLException {
//		HashMap<Long, UserUniqueDetails> usersUniqueDetails = new HashMap<Long, UserUniqueDetails>();
//		PreparedStatement pstmt = null;
//        ResultSet rs = null;
//
//        try {
//            String sql =
//            	"SELECT " +
//            	"	u.id as user_id" +
//            	"	,u.balance as user_balance" +
//            	"	,sum(i.amount*i.rate) as turnover" +
//            	"	,u.currency_id" +
//            	"	,u.skin_id " +
//            	"FROM " +
//            	"	users u left join investments i  on u.id = i.user_id " +
//            	"WHERE " +
//            	"	u.id in (" + usersIds + ") " +
//            	"GROUP BY" +
//            	"	u.id" +
//            	"	,u.balance" +
//            	"	,u.currency_id" +
//            	"	,skin_id";
//            
//            pstmt = conn.prepareStatement(sql);
//            rs = pstmt.executeQuery();
//            
//            while (rs.next()) {
//            	UserUniqueDetails userUniqueDetails = new UserUniqueDetails();
//            	userUniqueDetails.setBalance(rs.getLong("user_balance"));
//            	userUniqueDetails.setTurnOver(rs.getLong("turnover"));
//            	userUniqueDetails.setCurrencyId(rs.getInt("currency_id"));
//            	userUniqueDetails.setSkinId(rs.getInt("skin_id"));
//            	usersUniqueDetails.put(rs.getLong("user_id"), userUniqueDetails);
//            }
//        } finally {
//            closeResultSet(rs);
//            closeStatement(pstmt);
//        }
//        return usersUniqueDetails;		
//	}
	
	/**
	 * after user have more then 1000 sum deposit he need to move to upgrade
	 * @param connection
	 * @param userId
	 * @return true if user need to move
	 * @throws SQLException
	 */
	public static boolean isUserNeedToMoveToUpgrade(Connection connection, long userId) throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;		
		boolean shouldMove = false;
		try {
			String sql = 
					"SELECT " +
					  "1 " +
					"FROM " +
					  "USERS_ACTIVE_DATA uad " + 
					"WHERE " +
						"uad.user_id = ? AND " +
						"uad.SUM_DEPOSITS >= (SELECT e.value FROM enumerators e WHERE e.code = 'distinguish_upgrade_retention_team') AND " + 
						"NOT EXISTS ( " + //check if he his already in upgrade
		                            "SELECT " +
		                            	"1 " +
		                            "FROM " +
		                            	"population_users pu, " +
		                            	"population_entries pe, " +
		                            	"population_entries_hist peh " +
		                            "WHERE " +
		                            	"pu.id = pe.population_users_id AND " +
		                            	"pe.id = peh.population_entry_id AND " +
		                            	"pu.user_id = uad.user_id AND " +
		                            	"peh.status_id = " + PopulationsManagerBase.POP_ENT_HIS_STATUS_MOVING_UPGRADE_TO_RETENTION + " " +
		                            ") AND " +
						"NOT EXISTS ( " + //check if user is locked
									    "SELECT " +
						    				 "1 " +
						    			"FROM " +
			            			 		"population_users pu, " +
			            			 		"population_entries_hist peh " + 
			            			 	"WHERE " +
			            			 		"pu.lock_history_id = peh.id AND " +
			            			 		"pu.user_id = uad.user_id " +
						")";

			ps = connection.prepareStatement(sql);
			ps.setLong(1, userId);
			rs = ps.executeQuery();
			if (rs.next()) {
				shouldMove = true;
			} 
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		return shouldMove;
	}
	
	public static void updateAcceptTerms(Connection con, long userId) throws SQLException {
		PreparedStatement ps = null;

		try {
			String sql =
					"update users " +
					"set is_accepted_terms = ? " +
					"where id = ? ";

			ps = con.prepareStatement(sql);

			ps.setInt(1,1); // Set accepted terms to true
			ps.setLong(2,userId);
			ps.executeUpdate();
		} finally {
			closeStatement(ps);
		}
	}
	
	public static User getUserDetails(Connection con, long userId) throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		User user = null;
		try {
			String sql = "SELECT " +
							" u.user_name, " +
							" u.skin_id, " +
							" u.currency_id, " +
							" u.balance, " +
							" uad.sum_deposits, " +
							" uad.bubbles_pricing_level, " +
							" u.class_id, " +
							" u.time_created " +
						" FROM " +
							" users u, " +
							" users_active_data uad " +
						" WHERE " +
							" u.id = uad.user_id " +
							" AND u.id = ? ";
			ps = con.prepareStatement(sql);
			ps.setLong(1, userId);
			rs = ps.executeQuery();
			if (rs.next()) {
				user = new User();
				user.setUserName(rs.getString("user_name"));
	        	user.setSkinId(rs.getLong("skin_id"));
	        	user.setCurrencyId(rs.getLong("currency_id"));
	        	user.setBalance(rs.getLong("balance"));
	        	UserActiveData userActiveData = new UserActiveData();
	        	userActiveData.setSumDeposits(rs.getLong("sum_deposits"));
	        	userActiveData.setBubblesPricingLevel(rs.getInt("bubbles_pricing_level"));
	        	user.setUserActiveData(userActiveData);
	        	user.setClassId(rs.getLong("class_id"));
	        	user.setTimeCreated(rs.getDate("time_created"));
			}
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		return user;
	}

	public static List<Long> areTestUsers(Connection conn, List<Long> usersId) throws SQLException {
		ArrayList<Long> testUsers = new ArrayList<Long>();
		PreparedStatement pstmt = null;
        ResultSet rs = null;

        try {
            String sql =
            	"SELECT " +
            				"u.id as user_id " +
            	"FROM " +
            				"users u " +
            	"WHERE " +
            				"u.id in (" + usersId.toString().replaceAll("[\\[\\]]", "") + ") " +
            	"AND " +
            				"u.class_id = 0"; 
            
            pstmt = conn.prepareStatement(sql);
            rs = pstmt.executeQuery();
            
            while (rs.next()) {
            	testUsers.add(rs.getLong(1));
            }
        } finally {
            closeResultSet(rs);
            closeStatement(pstmt);
        }
        return testUsers;		
		
	}
	
//	public static boolean updateCopyopUserState(Connection conn, long userId, String issueActionTypeID) {
//		PreparedStatement ps = null;
//		UserStateEnum userState =  UserStateEnum.STATE_REGULAR;
//		
//		if(issueActionTypeID.equals(IssueAction.ISSUE_ACTION_USER_COPYOP_BLOCK)){
//			userState = UserStateEnum.STATE_BLOCKED;
//		}else if(issueActionTypeID.equals(IssueAction.ISSUE_ACTION_USER_COPYOP_UNBLOCK) || issueActionTypeID.equals(IssueAction.ISSUE_ACTION_USER_COPYOP_UNREMOVED)){
//			userState = UserStateEnum.STATE_REGULAR;
//		}else if(issueActionTypeID.equals(IssueAction.ISSUE_ACTION_USER_COPYOP_REMOVE)) {
//			userState = UserStateEnum.STATE_REMOVED;
//		}
//		try {
//			String sql = "UPDATE users_active_data"
//					+ " SET copyop_user_status = ?,"
//					+ " status_update_date = SYSDATE"
//					+ " WHERE user_id = ?";
//			ps = conn.prepareStatement(sql);
//			ps.setInt(1, userState.getId());
//			ps.setLong(2, userId);
//			ps.executeUpdate();
//			
//		}catch(SQLException e){
//			log.error("Unable to update users_active_data.copyop_user_status for userID: " + userId +" with issueActionTypeID : " + issueActionTypeID, e);
//			return false;
//		} finally {
//			closeStatement(ps);
//        }
//		return true;
//	}
	
	
	public static HashMap<Long, Integer> getUsersCopyopStatus(Connection connection, List<Long> userIds) throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		if (userIds.isEmpty()) {
			return new HashMap<Long, Integer>();
		}
		String userIdsSting = CommonUtil.getArrayToString(userIds);
		HashMap<Long, Integer> hm = new HashMap<Long, Integer>();
		try {
			String sql = 
					"SELECT " +
					"   user_id, " +
					"   copyop_user_status " +
					"FROM" +
					"	users_active_data u " +
					"WHERE " +
					"	user_id in ( " + userIdsSting + ")";

			ps = connection.prepareStatement(sql);
			rs = ps.executeQuery();
			while (rs.next()) {
				hm.put(rs.getLong("user_id"), rs.getInt("copyop_user_status"));
			} 
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		return hm;
	}	
	
	public static boolean isUserEngagedBonus(Connection conn, long userId) throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		boolean result = false;
		try {
			String sql = 
					"SELECT " +
					"	1 " +
					"FROM " +
					"	bonus_users bu inner join users u on bu.user_id = u.id " +
					"WHERE " +
					"	u.id = ? " +
					"	AND (bu.time_activated is not null or bu.time_used is not null)";
			ps = conn.prepareStatement(sql);
			ps.setLong(1, userId);
			rs = ps.executeQuery();
			if (rs.next()) {
				result = true;
			}
		}
		finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		return result;
	}
	
    public static boolean isUserETRegulation(Connection conn, long userId) throws SQLException {
        boolean b = false;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try{
            String sql =
                    " select * " +
                    " from " + 
            			" users_regulation ur, " +
            		"	 users u " +
            	    " where ur.user_id = u.id " +
            			" and u.skin_id = 1 " +
            			" and ur.user_id = ? ";
            pstmt = conn.prepareStatement(sql);
            pstmt.setLong(1, userId);
            
            rs = pstmt.executeQuery();
            if (rs.next()) {
                b = true;
            }
        } finally {
            closeStatement(pstmt);
            closeResultSet(rs);
        }
        return b;
    }
    
    public static void insertAffSub(Connection con, long userId, String affSub1, String affSub2, String affSub3) throws SQLException {
        PreparedStatement ps = null;
        try{
            String sql = "UPDATE " +
            					"users_active_data " +
            			" SET " +
            							" aff_sub1 = ?," +
            							" aff_sub2 = ?," +
            							" aff_sub3 = ? " +
    							"WHERE " +
    									" user_id = ? ";
            			
            ps = con.prepareStatement(sql);
            
            ps.setString(1, affSub1);
            ps.setString(2, affSub2);
            ps.setString(3, affSub3);
            ps.setLong(4, userId);

            ps.executeUpdate();
        } finally {
            closeStatement(ps);
        }
    }

	public static String[] getAffSubForUser(Connection conn, long userId) throws SQLException {
	    String[] affSub = new String[3];
	    PreparedStatement pstmt = null;
	    ResultSet rs = null;
	    try{
	        String sql =
	                " SELECT" +
	                			" aff_sub1, aff_sub2, aff_sub3 " +
	                " FROM " + 
	        					" users_active_data " +
	        	    " WHERE" +
	        	    			" id = ? ";
	        pstmt = conn.prepareStatement(sql);
	        pstmt.setLong(1, userId);
	        
	        rs = pstmt.executeQuery();
	        if (rs.next()) {
	        	affSub[0] = rs.getString("aff_sub1");
	        	affSub[1] = rs.getString("aff_sub2");
	        	affSub[2] = rs.getString("aff_sub3");
	        }
	    } finally {
	        closeStatement(pstmt);
	        closeResultSet(rs);
	    }
	    return affSub;
	}

	public static String[] getAffSubForContact(Connection con, long contactId) throws SQLException {
	    String[] affSub = new String[3];
	    PreparedStatement pstmt = null;
	    ResultSet rs = null;
	    try{
	        String sql =
	                " SELECT" +
	                			" aff_sub1, aff_sub2, aff_sub3 " +
	                " FROM " + 
	        					" contacts " +
	        	    " WHERE" +
	        	    			" id = ? ";
	        pstmt = con.prepareStatement(sql);
	        pstmt.setLong(1, contactId);
	        
	        rs = pstmt.executeQuery();
	        if (rs.next()) {
	        	affSub[0] = rs.getString("aff_sub1");
	        	affSub[1] = rs.getString("aff_sub2");
	        	affSub[2] = rs.getString("aff_sub3");
	        }
	    } finally {
	        closeStatement(pstmt);
	        closeResultSet(rs);
	    }
	    return affSub;
	}
	
	public static boolean isUserIdNumberInUse(Connection conn, String idnum, long skinId, long userId) throws SQLException {
		boolean inUse = false;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			String sql = "SELECT " +
							"id " +
						 "FROM " +
							"users " +
						 "WHERE " +
							"id_num = ? " +
							"AND skin_id = ? " +
							"AND id != ? ";
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, AESUtil.encrypt(idnum));
			pstmt.setLong(2, skinId);
			pstmt.setLong(3, userId);
			rs = pstmt.executeQuery();
			if (rs.next()) {
				inUse = true;
			}
		} catch (	CryptoException | InvalidKeyException | NoSuchAlgorithmException | NoSuchPaddingException | IllegalBlockSizeException
					| BadPaddingException | UnsupportedEncodingException | InvalidAlgorithmParameterException ce) {
			throw new SQLException(ce.getMessage());
		} finally {
			closeResultSet(rs);
			closeStatement(pstmt);
		}
		return inUse;
	}
	
	public static long getUserDefInvAmount(Connection con, long userId) throws SQLException {
		long defInvAmount = 0;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			String sql = "SELECT " +
							" uad.def_inv_amount " +
						" FROM " +
							" users_active_data uad " +
						" WHERE " +
							" uad.user_id = ? ";
			ps = con.prepareStatement(sql);
			ps.setLong(1, userId);
			rs = ps.executeQuery();
			if (rs.next()) {
				defInvAmount = rs.getLong("def_inv_amount");
			}
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		return defInvAmount;
	}
	
	/**
	 * Get marketing users
	 * @param con
	 * @param mi
	 * @param affiliateKey
	 * @param dateRequest
	 * @return MarketingInfo
	 * @throws SQLException
	 * @throws ParseException
	 */
	public static MarketingInfo getMarketingUsers(Connection con, MarketingInfo mi, long affiliateKey, String dateRequest) throws SQLException, ParseException {
		CallableStatement stmt = null;
		ResultSet rs = null;
		int index = 1;
		List<String> list = new ArrayList<String>();
		try {
			String sql = "{? = call API.GET_MARKETING_USERS (?,?)}";
			DbmsOutput dbmsOutput = new DbmsOutput( con );
	        dbmsOutput.enable();
			stmt = con.prepareCall(sql);
	        stmt.registerOutParameter(index++, OracleTypes.CURSOR);
	        stmt.setString(index++, dateRequest);
	        stmt.setLong(index++, affiliateKey);
	        stmt.execute();
	        dbmsOutput.show();
	        dbmsOutput.close();
	        rs = (ResultSet)stmt.getObject(1);
			while (rs.next()) {
				mi.setCountSignups(rs.getLong("num_users"));
				String dp = rs.getString("dp");
				if (null != dp) {
					list.add(dp);
				}
			}
			mi.setDynamicParams(list.toArray(new String[list.size()]));
		} finally {
			closeResultSet(rs);
			closeStatement(stmt);
		}
		return mi;
	}

  	public static long getSkinIdByUserId(Connection con,long userId)  throws SQLException{

		  PreparedStatement ps=null;
		  ResultSet rs=null;
		  long skinId=0;
		  try {
			    String sql="select skin_id from users where id=?";

				ps = con.prepareStatement(sql);
				ps.setLong(1, userId);

				rs=ps.executeQuery();

				if (rs.next()) {
					skinId = rs.getLong("SKIN_ID");
				}
			}

			finally
			{
				closeResultSet(rs);
				closeStatement(ps);
			}
			return skinId;

	  }
  	
    public static long getUserTotalBonusesAmount(Connection conn, long userId) throws SQLException {
        long total = 0;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            String sql =
                    "SELECT " +
                        "SUM(bonus_amount) AS total " +
                    "FROM " +
                        "bonus_users " +
                    "WHERE " +
                        "user_id = ? AND " +
                        "bonus_state_id IN (" + ConstantsBase.BONUS_STATE_ACTIVE + ", " + ConstantsBase.BONUS_STATE_USED + ")";
            pstmt = conn.prepareStatement(sql);
            pstmt.setLong(1, userId);
            rs = pstmt.executeQuery();
            if (rs.next()) {
                total = rs.getLong("total");
            }
        } finally {
            closeResultSet(rs);
            closeStatement(pstmt);
        }
        return total;
    }
    
	public static void updateUserNeedChangePass(Connection con, long userId, boolean isNeedChangePass) throws SQLException {
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {
			String sql = "UPDATE " +
									" users_active_data " +
						" SET " +
									" is_need_change_password = ? " +
						" WHERE " + 
									" user_id = ? ";

			pstmt = con.prepareStatement(sql);
			pstmt.setBoolean(1, isNeedChangePass);
			pstmt.setLong(2, userId);
			
			pstmt.executeUpdate();
		} finally {
			closeResultSet(rs);
			closeStatement(pstmt);
		}
	}

	public static void saveToken(Connection con, String token, String serverId, long userId, long loginId) throws SQLException {
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {
			String sql = "INSERT into "
										+ " bubbles_token " 
										+ " (token, user_id, server_id, time_created, login_id, writer_id) "
						+ " VALUES "
										+ " (?, ?, ?, sysdate, ?, (select decode(writer_id, "
																				+ Writer.WRITER_ID_WEB + ", "
																				+ Writer.WRITER_ID_BUBBLES_WEB + ", "
																				+ Writer.WRITER_ID_MOBILE + ", "
																				+ Writer.WRITER_ID_BUBBLES_MOBILE +", "
																				+ "writer_id) from LOGINS where id=?)) ";

			pstmt = con.prepareStatement(sql);
			pstmt.setString(1, token);
			pstmt.setLong(2, userId);
			pstmt.setString(3, serverId);
			pstmt.setLong(4, loginId);
			pstmt.setLong(5, loginId);
			
			pstmt.executeUpdate();
		} finally {
			closeResultSet(rs);
			closeStatement(pstmt);
		}
		
	}
	
	public static void deleteToken(Connection con, String token) throws SQLException {
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {
			String sql = "DELETE FROM "
										+ " bubbles_token "
						+ " WHERE "
										+ " token = ? ";

			pstmt = con.prepareStatement(sql);
			pstmt.setString(1, token);
		
			pstmt.executeUpdate();
		} finally {
			closeResultSet(rs);
			closeStatement(pstmt);
		}
		
	}
	
	public static void cleanAllTokens(Connection con, String serverId) throws SQLException {
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {
			String sql = "delete from bubbles_token where server_id = ?";

			pstmt = con.prepareStatement(sql);
			pstmt.setString(1, serverId);
		
			int k = pstmt.executeUpdate();
			log.debug("Deleted ["+k+"] tokens for "+serverId);
		} finally {
			closeResultSet(rs);
			closeStatement(pstmt);
		}
		
	}
	
	public static BubblesToken tokenExists(Connection con, String token, long userId) throws SQLException {
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		BubblesToken bt = null;

		try {
			String sql = "SELECT * FROM "
										+ " bubbles_token "
						+ " WHERE "
										+ " token = ? "
						+ " AND "
										+ " user_id = ?"
						;

			pstmt = con.prepareStatement(sql);
			pstmt.setString(1, token);
			pstmt.setLong(2, userId);
			
			 rs = pstmt.executeQuery();
            if (rs.next()) {
            	bt = new BubblesToken();
            	bt.setToken(rs.getString("token"));
            	bt.setUserId(rs.getLong("user_id"));
            	bt.setServerId(rs.getString("server_id"));
            	bt.setTimeCreated(rs.getDate("time_created"));
            	bt.setLoginId(rs.getLong("login_id"));
            	bt.setWriterId(rs.getLong("writer_id"));
            } 
		} finally {
			closeResultSet(rs);
			closeStatement(pstmt);
		}
		return bt;
	}
	
	/**
	 * @param con
	 * @param userId
	 * @return
	 * @throws SQLException
	 */
	public static User getUserById(Connection con, long userId) throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		User user = null;

		try {
			String sql = "SELECT " +
							 "u.*, " +
							 "uad.* " +
					     "FROM users u, " +
					     	"users_active_data uad " +
					     "WHERE " +
					     	"u.id = uad.user_id " +
					     	"AND u.id =  ? ";

			ps = con.prepareStatement(sql);
			ps.setLong(1,userId);

			rs = ps.executeQuery();
			if (rs.next()) {
				user = new User();
				loadUserVO(rs, user);
				user.setShowCancelInvestment(rs.getBoolean("show_cancel_investment"));
				user.setNonRegSuspend(rs.getBoolean("IS_NON_REG_SUSPEND"));
				//user.setCurrency(ApplicationDataBase.getCurrencyById(user.getCurrencyId()));
			}
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		return user;
	}
	
    public static long getCurrencyIdByUserName(Connection conn, String userName) throws SQLException {
        if (null == userName || userName.trim().equals("")) {
            return 0;
        }

        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            String sql =
                " SELECT " +
                    " u.currency_id " +
                " FROM " +
                    " users u " +
                " WHERE " +
                    " user_name = ? ";
            pstmt = conn.prepareStatement(sql);
            pstmt.setString(1, userName.toUpperCase());
            rs = pstmt.executeQuery();
            if (rs.next()) {
                return rs.getLong("currency_id");
            }
        } finally {
            closeResultSet(rs);
            closeStatement(pstmt);
        }
        return 0;
    }
    
	public static boolean hasTheSameInvestmentInTheLastSecSameIp(Connection conn, String ip, int sec, InvestmentRejects invRej) throws SQLException {
        boolean has = false;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try{
            String sql =
                        "SELECT " +
                            "id, " +
                            "((current_date - time_created)*86400) secBetween " +
                        "FROM " +
                            "investments " +
                        "WHERE " +
                            "ip like ? AND " +
                            "current_date - time_created < ? /86400 ";
            pstmt = conn.prepareStatement(sql);
            pstmt.setString(1, ip);
            pstmt.setInt(2, sec);
            rs = pstmt.executeQuery();
            if (rs.next()) {
                has = true;
                invRej.setSecBetweenInvestments(sec);
                invRej.setRejectTypeId(ConstantsBase.INVESTMENT_REJECT_LAST_INV_IN_LESS_THEN_X_SEC_IP);
                invRej.setRejectAdditionalInfo("Same investment in less:, secBetweenInvestments: " + rs.getLong("secBetween") + " , market secBetweenInvestments: " + sec);
                InvestmentRejectsManagerBase.insertInvestmentReject(invRej);
            }
        } finally {
            closeStatement(pstmt);
            closeResultSet(rs);
        }
        return has;
    }
	
	public static boolean hasTheSameInvestmentInTheLastSec(Connection conn, long userId, long oppId, long typeId, InvestmentRejects invRej) throws SQLException {
        boolean has = false;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try{
            String sql =
                    "SELECT " +
                        "id," +
                        "((current_date - time_created) * 86400) secBetween " +
                    "FROM " +
                        "investments " +
                    "WHERE " +
                        "user_id = ? AND " +
                        "opportunity_id = ? AND " +
                        "type_id = ? AND " +
                        "current_date - time_created < (SELECT sec_between_investments FROM markets WHERE id = (SELECT market_id FROM opportunities WHERE id = ?)) / 86400";
            pstmt = conn.prepareStatement(sql);
            pstmt.setLong(1, userId);
            pstmt.setLong(2, oppId);
            pstmt.setLong(3, typeId);
            pstmt.setLong(4, oppId);
            rs = pstmt.executeQuery();
            if (rs.next()) {
                has = true;
                invRej.setRejectTypeId(ConstantsBase.INVESTMENT_REJECT_LAST_INV_IN_LESS_THEN_X_SEC);
                invRej.setRejectAdditionalInfo("Same investment in less:, secBetweenInvestments: " + rs.getLong("secBetween") + " , market secBetweenInvestments: " + invRej.getSecBetweenInvestments());
                InvestmentRejectsManagerBase.insertInvestmentReject(invRej);
            }
        } finally {
            closeStatement(pstmt);
            closeResultSet(rs);
        }
        return has;
    }

    /**
     * Check if there is disabled record for the specified user / market covering the current moment.
     *
     * @param conn
     * @param userId
     * @param marketId
     * @return
     * @throws SQLException
     */
    public static boolean isUserMarketDisabled(Connection conn, long userId, long marketId, int scheduled, boolean isDev3, long apiExternalUserId) throws SQLException {
        boolean b = false;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try{
            String sql =
                    "SELECT " +
                        "id " +
                    "FROM " +
                        "user_market_disable " +
                    "WHERE " +
                        "(user_id = ? AND nvl(api_external_user_id, 0) = ?) AND ";
            if (isDev3) {
                 sql += "(market_id = ? OR market_id = 0) AND ";
            } else {
                 sql += "market_id = ? AND ";
            }
                 sql += "is_active = 1 AND " +
                        "current_date >= start_date AND " +
                        "current_date <= end_date AND " +
                        "(scheduled = ? OR scheduled = ? ) AND " +   // in case we have the same scheduled like the parameter, or
            			"is_dev3 = ?";									        // scheduled = 0 (all) - for all scheduled types
            pstmt = conn.prepareStatement(sql);
            pstmt.setLong(1, userId);
            pstmt.setLong(2, apiExternalUserId);
            pstmt.setLong(3, marketId);
            pstmt.setInt(4, scheduled);
            pstmt.setInt(5, Opportunity.SCHEDULED_ALL);
            pstmt.setInt(6, isDev3 ? 1 : 0);

            rs = pstmt.executeQuery();
            if (rs.next()) {
                b = true;
            }
        } finally {
            closeStatement(pstmt);
            closeResultSet(rs);
        }
        return b;
    }

    public static void insertBalanceLog(Connection con, long writerId, long userId, String table, long key, int command, String utcoffset) throws SQLException {
        PreparedStatement pstmt = null;
        try {
            String sql =
                "INSERT INTO balance_history " +
                    "(id, time_created, user_id, balance, tax_balance, writer_id, table_name, key_value, command, utc_offset) " +
                "VALUES " +
                    "(seq_balance_history.nextval, sysdate, ?, " +
                        "(select balance from users where id = ?), " +
                        "(select tax_balance from users where id = ?), ?, ?, ?, ?, ?)";
            pstmt = con.prepareStatement(sql);
            pstmt.setLong(1, userId);
            pstmt.setLong(2, userId);
            pstmt.setLong(3, userId);
            pstmt.setLong(4, writerId);
            pstmt.setString(5, table);
            pstmt.setLong(6, key);
            pstmt.setInt(7, command);
            pstmt.setString(8, utcoffset);
            pstmt.executeUpdate();
        } finally {
            closeStatement(pstmt);
        }
    }
    
	/**
     * Is user have bonus
     * @param con db connection
     * @param userId user id
     * @return
     * @throws SQLException
     */
	public static boolean isHaveBonus(Connection con, long userId) throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;

		try	{
		    String sql = "SELECT bu.id " +
		    			 "FROM " +
		    			 	"bonus_users bu, bonus_states bs " +
		    			 "WHERE " +
		    			 	"user_id = ? AND " +
		    			 	"bu.bonus_state_id = bs.id AND " +
							"bs.id in ( " + Bonus.STATE_PENDING + " ) AND " +
		    			 	"sysdate - bu.time_created <= " + ConstantsBase.BONUS_NEW_FLAG_EXPIRY_DAYS +" AND " +
		    			 	"bu.type_id <> ? " +
		    			 "ORDER BY " +
		    			 "	bu.id DESC ";

			ps = con.prepareStatement(sql);
			ps.setLong(1, userId);
			ps.setLong(2, Bonus.TYPE_CONVERT_POINTS_TO_CASH);
			rs = ps.executeQuery();

			if ( rs.next() ) {
				return true;
			}
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		return false;
	}
	
  	/**
  	 * this method returns the utcoffset of a user by userId
  	 * @param con - DB connection
  	 * @param userId - the user Id
  	 * @return UTC_OFFSET of the user
  	 * @throws SQLException
  	 */
  	public static String getUtcOffset(Connection con,long userId) throws SQLException{
  		PreparedStatement ps = null;
		ResultSet rs = null;

		try {
			    String sql = "select utc_offset from users where id=?";
				ps = con.prepareStatement(sql);
				ps.setLong(1, userId);
				rs = ps.executeQuery();

				if (rs.next()) {
					return rs.getString("utc_offset");
				}

		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}

		return ConstantsBase.EMPTY_STRING;
  	}

	public static void loadUserContacts(Connection con, String userName, User user) throws SQLException {
  		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			    String sql = 
					    " SELECT " + 
						    		" u.id, " + 
						    		" u.country_id, " +  
						    		" u.email, " + 
						    		" u.mobile_phone, " +  
						    		" c.phone_code " + 
			    		" FROM " + 
			    					" users u, " +
			    					" countries c " + 
			    		" WHERE " +
			    					" u.country_id = c.id " +
			    		" AND " +
			    					" u.user_name = ? ";

			    ps = con.prepareStatement(sql);
				ps.setString(1, userName);
				rs = ps.executeQuery();

				if (rs.next()) {
					user.setId(rs.getLong("id"));
					user.setEmail(rs.getString("email"));
					user.setMobilePhone(rs.getString("mobile_phone"));
					user.setMobilePhonePrefix(""+rs.getInt("phone_code"));
				}

		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
	}
	
  	public static long getCurrIdByUserId(Connection con, long userId) throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;

		try {
			String sql = "SELECT CURRENCY_ID FROM USERS WHERE ID = ? ";

			ps = con.prepareStatement(sql);
			ps.setLong(1,userId);
			rs = ps.executeQuery();

			if (rs.next()) {
				return rs.getLong("CURRENCY_ID");
			}
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		return 0;
	}

  	
  	/**
  	 * @param connection
  	 * @param skinId
  	 * @param platformId
  	 * @return
  	 * @throws SQLException
  	 */
  	public static int getEPGProductId(Connection connection, int skinId, int platformId) throws SQLException {
		PreparedStatement ps = null;
		ResultSet resultSet = null;
		int productId = 0;

		try {
			String sql = 
					"SELECT " +
					"	bcpm.epg_product_id " +
					"FROM" +
					"	business_cases_platforms_map bcpm " +
					"	,skins s " + 
					"WHERE " +
					"	s.id = bcpm.skin_business_cases_id " +
					"	and s.id = ? " + 
					"	and bcpm.platforms_id = ? ";

			ps = connection.prepareStatement(sql);
			ps.setInt(1, skinId);
			ps.setInt(2, platformId);
			resultSet = ps.executeQuery();
			if (resultSet.next()) {
				productId = resultSet.getInt("epg_product_id");	
			} 
		} finally {
			closeResultSet(resultSet);
			closeStatement(ps);
		}
		return productId;
	}
  	
  	/**
  	 * @param connection
  	 * @return
  	 * @throws SQLException
  	 */
  	public static Map<Key, Integer> getEPGProductIds(Connection connection) throws SQLException {
		PreparedStatement ps = null;
		ResultSet resultSet = null;
		Map<Key, Integer> productIds = new HashMap<Key, Integer>();

		try {
			String sql = 
					"SELECT " +
					"	bcpm.skin_business_cases_id " +
					"	,bcpm.platforms_id " +
					"	,bcpm.epg_product_id " +
					"FROM " +
					"	business_cases_platforms_map bcpm ";

			ps = connection.prepareStatement(sql);
			resultSet = ps.executeQuery();
			while (resultSet.next()) {
				productIds.put(
						new Key(resultSet.getInt("skin_business_cases_id"), resultSet.getInt("platforms_id")),
								resultSet.getInt("epg_product_id")); 
			} 
		} finally {
			closeResultSet(resultSet);
			closeStatement(ps);
		}
		return productIds;
	}

	public static boolean changeCancelInvestmentCheckboxState(Connection con, long userId, boolean showCancelInvestment) throws SQLException {
		PreparedStatement ps = null;
		try {
			String sql = "update users_active_data set show_cancel_investment = ? where user_id = ?";
			ps = con.prepareStatement(sql);
			ps.setBoolean(1, showCancelInvestment);
			ps.setLong(2, userId);
			if (ps.executeUpdate() > 0) {
				return true;
			}
			return false;
		} finally {
			closeStatement(ps);
		}
	}
	
	public static boolean updateUserPhoneValidated(Connection con, Long id, MobileNumberValidation validation) throws SQLException {
		PreparedStatement ps = null;
		try {
				String sql = "UPDATE users SET mobile_phone_validated = ? WHERE id = ?";
				ps = con.prepareStatement(sql);
				ps.setInt(1, MobileNumberValidation.toInt(validation));
				ps.setLong(2, id);
				
				if (ps.executeUpdate() > 0) {
					return true;
				}
				return false;
		} finally {
			closeStatement(ps);
		}
	}

	public static long addToBalanceNonNeg(Connection con, long userId, long amount, long negTransactionFixId, double rate, String negFixDescription, String utcOffset, String negFixComment) throws SQLException {
		CallableStatement stmt = null;
		log.debug("Adding to balance:" + amount + " to user:"+ userId);
		try {
			String sql = "{? = call pkg_transactions_backend.update_user_balance(?,?,?,?,?,?,?)}";
			int index = 1;
			stmt = con.prepareCall(sql);
	        stmt.registerOutParameter(index++, OracleTypes.NUMBER);
	        stmt.setLong(index++, userId);
	        stmt.setLong(index++, amount);
	        stmt.setLong(index++, negTransactionFixId);
	        stmt.setDouble(index++, rate);
	        stmt.setString(index++, negFixComment);
	        stmt.setString(index++, utcOffset);
	        stmt.setString(index++, negFixDescription);
	        stmt.execute();
			return stmt.getLong(1);
		} finally {
			closeStatement(stmt);
		}
	}

	public static void main(String args[]) {
		System.out.println(MobileNumberValidation.toInt(MobileNumberValidation.INVALID));
	}
	
	public static long getFirstTransactionId(Connection con, long userId) throws SQLException {

		PreparedStatement ps = null;
		ResultSet rs = null;

		try {
			String sql = "select first_deposit_id from users where id=?";

			ps = con.prepareStatement(sql);
			ps.setLong(1,userId);

			rs = ps.executeQuery();

			if (rs.next()) {
				return rs.getLong("first_deposit_id");
			}
		}

		finally {
			closeResultSet(rs);
			closeStatement(ps);
		}

		return 0;
	}
    /**
     * Check if it's test user
     * @param con
     * @param userId
     * @return
     * @throws SQLException
     */
    public static boolean isTestUser(Connection con, long userId) throws SQLException {
   	 	PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            String sql =
            			" SELECT " +
            			"	u.class_id " +
            			" FROM " +
            			"	users u " +
            			" WHERE " +
            			"	u.id = ? ";
            pstmt = con.prepareStatement(sql);
            pstmt.setLong(1, userId);
            rs = pstmt.executeQuery();
            if (rs.next()) {
            	long classId = rs.getLong("class_id");
            	if (classId == 0) {
            		return true;
            	}
            }
        } finally {
    		closeResultSet(rs);
    		closeStatement(pstmt);
		}
    	return false;
    }
    
	public static long getUserIdByEmail(Connection conn, String email) throws SQLException {
		CallableStatement cstmt = null;
		ResultSet rs = null;
		int index = 1;
		try {
			cstmt = conn.prepareCall("{call PKG_USER.GET_USER_BY_EMAIL(o_data => ? " +
																	   ",i_email => ?)}");
			cstmt.registerOutParameter(index++, OracleTypes.CURSOR);
			cstmt.setString(index++, email);
			cstmt.executeQuery();

			rs = (ResultSet) cstmt.getObject(1);
			if (rs.next()) {
				return rs.getLong("user_id");
			}
		} finally {
			closeStatement(cstmt);
			closeResultSet(rs);
		}
		return 0;
	}
	
	/**
	 * Set isDeclined
	 * 
	 * @param conn 
	 *            Db connection
	 * @param userId
	 *            user id
	 * @throws SQLException
	 */
	public static void setUserLastDecline(Connection con, long userId, boolean isDeclined, Transaction tran)
			throws SQLException {
		PreparedStatement pstmt = null;
		try {
			String sql = "UPDATE users " + "SET is_declined = ? ";

			if (isDeclined) {
				sql += ", last_decline_date = ? ";
			}
			sql += "WHERE id = " + userId;

			pstmt = con.prepareStatement(sql);
			pstmt.setInt(1, (isDeclined == true ? 1 : 0));

			if (isDeclined) {
				pstmt.setTimestamp(2, convertToTimestamp(tran.getTimeSettled()));
			}
			pstmt.executeUpdate();
		} finally {
			closeStatement(pstmt);
		}
	}
	
	public static void updateLastDepositOrInvestDate(Connection conn, long userId) throws SQLException {
		CallableStatement cstmt = null;
		int index = 1;
		try {
			String sql = "{call PKG_USER.SET_LAST_DEP_OR_INVEST_DATE( i_user_id => ? )}";
			cstmt = conn.prepareCall(sql);
	
			cstmt.setLong(index++, userId);
			cstmt.executeQuery();	
			log.debug("Updated last Deposit or Invest date for "
					+ " userId[" + userId + "]");
		} finally {
			closeStatement(cstmt);
		}			
	}

	public static HashMap<String, User> checkForExistingEmails(Connection conn,
															ArrayList<String> emailsForCheck) throws SQLException {
		CallableStatement cstmt = null;
		ResultSet rs = null;
		int index = 1;
		HashMap<String, User> existingMails = new HashMap<String, User>();
		try {
			cstmt = conn.prepareCall("{call PKG_USER.CHECK_EXISTING_EMAILS(o_emails => ? " +
																	   	  ",i_emails_for_check => ?)}");
			cstmt.registerOutParameter(index++, OracleTypes.CURSOR);
			setArrayVal(emailsForCheck, index++, cstmt, conn);
			cstmt.executeQuery();

			rs = (ResultSet) cstmt.getObject(1);
			while (rs.next()) {
				User u = new User();
				u = new User();
				loadUserVO(rs, u);
                existingMails.put(u.getEmail().trim().toUpperCase(), u);
			}
		} finally {
			closeStatement(cstmt);
			closeResultSet(rs);
		}
		return existingMails;
	}
	
	public static boolean isNonRegUserSuspend(Connection conn, long userId) throws SQLException {
		boolean isSuspend = false;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			String sql = "SELECT " +
							"id " +
						 "FROM " +
						 	"users_active_data " +
						 "WHERE " +
						 	"user_id = ? " +
						    " and is_non_reg_suspend = 1";
			pstmt = conn.prepareStatement(sql);
			pstmt.setLong(1, userId);
			rs = pstmt.executeQuery();
			if (rs.next()) {
				isSuspend = true;
			}
		} finally {
			closeResultSet(rs);
			closeStatement(pstmt);
		}
		return isSuspend;
	}

	public static UserMigration getUserMigration(Connection conn, long userId) throws SQLException {
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			String sql = "SELECT * from INVEST_MIGRATION where user_id = ? AND DATE_PROCESSED is null ";
			pstmt = conn.prepareStatement(sql);
			pstmt.setLong(1, userId);
			rs = pstmt.executeQuery();
			if (rs.next()) {
				UserMigration um = new UserMigration();
				um.setTargetPlatform(rs.getInt("target_platform"));
				um.setUserId(rs.getLong("user_id"));
				return um;
			}
		} finally {
			closeResultSet(rs);
			closeStatement(pstmt);
		}
		return null;
			
	}
	
	public static void getUserMigration(Connection conn, long userId, UserMigration um, boolean processed) throws SQLException {
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			String sql = "SELECT * from INVEST_MIGRATION where user_id = ? AND DATE_PROCESSED is "+ (processed ? " not " : "") + " null ";
			pstmt = conn.prepareStatement(sql);
			pstmt.setLong(1, userId);
			rs = pstmt.executeQuery();
			if (rs.next()) {
				um.setTargetPlatform(rs.getInt("target_platform"));
				um.setUserId(rs.getLong("user_id"));
			}
		} finally {
			closeResultSet(rs);
			closeStatement(pstmt);
		}
	}
	
	public static void updateUserMigration(Connection conn, long userId) throws SQLException {
		PreparedStatement pstmt = null;
		try {
			String sql = "UPDATE INVEST_MIGRATION set DATE_PROCESSED = sysdate where user_id = ?";
			pstmt = conn.prepareStatement(sql);
			pstmt.setLong(1, userId);
			pstmt.executeUpdate();
		} finally {
			closeStatement(pstmt);
		}
	}
	
	public static void insertUserMigrationJob(Connection conn, UserMigration um) throws SQLException {
		PreparedStatement pstmt = null;
		try {
			String sql = "INSERT INTO INVEST_MIGRATION_USERS " +
					" (ID, USER_ID, FIRST_NAME, LAST_NAME, EMAIL, PHONE, COUNTRY, LANGUAGE, CURRENCY, ACCEPTED_MAILING, ACCEPTED_TRANSFER, BALANCE, PARTIAL) "+
					" VALUES" +
					" (SEQ_INVEST_MIGRATION_USERS.nextval, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
			pstmt = conn.prepareStatement(sql);
			pstmt.setLong(1, um.getUserId());
			pstmt.setString(2, um.getFirstName());
			pstmt.setString(3, um.getLastName());
			pstmt.setString(4, um.getEmail());
			pstmt.setString(5, um.getPhone());
			pstmt.setString(6, um.getCountry());
			pstmt.setString(7, um.getLanguage());
			pstmt.setString(8, um.getCurrency());
			pstmt.setInt(9, um.isAcceptedMailing()?1:0);
			pstmt.setInt(10, um.isAcceptedTransfer()?1:0);
			pstmt.setLong(11, um.getBalance());
			pstmt.setInt(12, um.isPartial()?1:0);
			pstmt.executeUpdate();
		} finally {
			closeStatement(pstmt);
		}
	}
}
