package com.anyoption.common.daos;

import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import org.apache.log4j.Logger;
import org.bouncycastle.crypto.CryptoException;

import com.anyoption.common.beans.CreditCard;
import com.anyoption.common.beans.base.CreditCardType;
import com.anyoption.common.beans.base.Skin;
import com.anyoption.common.managers.TransactionsManagerBase;
import com.anyoption.common.util.AESUtil;
import com.anyoption.common.util.ConstantsBase;

import oracle.jdbc.OracleTypes;

public class CreditCardsDAOBase extends DAOBase {
	private static final Logger log = Logger.getLogger(CreditCardsDAOBase.class);
    public static CreditCard getById(Connection conn, long id) throws SQLException {
        CreditCard vo = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            String sql =
                "SELECT " +
                    "cc.*, " +
                    "cct.id cct_id, " +
                    "cct.name cct_name " +
                "FROM " +
                    "credit_cards cc, " +
                    "credit_card_types cct " +
                "WHERE " +
                    "cc.id = ? AND " +
                    "cct.id = cc.type_id_by_bin";
            pstmt = conn.prepareStatement(sql);
            pstmt.setLong(1, id);
            rs = pstmt.executeQuery();
            if (rs.next()) {
                vo = getVO(rs);
            }
        } finally {
            closeResultSet(rs);
            closeStatement(pstmt);
        }
        return vo;
    }
    
    public static CreditCard getByCardIdAndUserId(Connection conn, long cardId, long userId) throws SQLException {
        CreditCard vo = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            String sql =
                " SELECT " +
                    " cc.*, " +
                    " cct.id cct_id, " +
                    " cct.name cct_name " +
                " FROM " +
                    " credit_cards cc, " +
                    " credit_card_types cct, " +
                    " users u " +
                " WHERE " +
                	" cct.id = cc.type_id" +                    
                	" AND u.id = cc.user_id " +                
                    " AND cc.id = ?  " +
                    " AND u.id = ? ";

            pstmt = conn.prepareStatement(sql);
            pstmt.setLong(1, cardId);
            pstmt.setLong(2, userId);
            rs = pstmt.executeQuery();
            if (rs.next()) {
                vo = getVO(rs);
            }
        } finally {
            closeResultSet(rs);
            closeStatement(pstmt);
        }
        return vo;
    }    

    private static CreditCard getVO(ResultSet rs) throws SQLException {
        CreditCard vo = new CreditCard();
        try {
	        vo.setId(rs.getLong("id"));
	        vo.setUserId(rs.getLong("user_id"));
	        vo.setTypeId(rs.getLong("TYPE_ID"));
	        vo.setTypeIdByBin(rs.getLong("type_id_by_bin"));
	        vo.setVisible(rs.getBoolean("is_visible"));
	        vo.setCcNumber(Long.valueOf(AESUtil.decrypt(rs.getString("CC_NUMBER"))));
	        vo.setCcPass(rs.getString("CC_PASS"));
	        vo.setTimeCreated(convertToDate(rs.getTimestamp("TIME_CREATED")));
	        vo.setTimeModified(convertToDate(rs.getTimestamp("TIME_MODIFIED")));
	        vo.setExpMonth(rs.getString("EXP_MONTH"));
	        vo.setExpYear(rs.getString("EXP_YEAR"));
	        vo.setPermission(rs.getInt("IS_allowed"));
	        vo.setHolderName(rs.getString("HOLDER_NAME"));
	        vo.setHolderIdNum(rs.getString("HOLDER_ID_NUM"));
	        vo.setUtcOffsetCreated(rs.getString("utc_offset_created"));
	        vo.setUtcOffsetModified(rs.getString("utc_offset_modified"));
	        vo.setDocumentsSent(rs.getBoolean("is_documents_sent"));
	        vo.setType(new CreditCardType());
	        vo.getType().setId(rs.getLong("cct_id"));
            vo.getType().setName(rs.getString("cct_name"));
            vo.setCountryId(rs.getLong("country_id"));
            vo.setRecurringTransaction(rs.getString("recurring_transaction"));
            vo.setBin(rs.getLong("bin"));
		} catch (	CryptoException | NumberFormatException | InvalidKeyException | IllegalBlockSizeException | BadPaddingException
					| NoSuchAlgorithmException | NoSuchPaddingException | InvalidAlgorithmParameterException ce) {
			// FIXME very bad way of handling exceptions
			throw new SQLException(ce.getMessage());
		}
        return vo;
    }

	public static CreditCard getByNumberAndUserId(	Connection conn, long ccn,
													long userId)	throws SQLException, InvalidKeyException, NoSuchAlgorithmException,
																	NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException,
																	UnsupportedEncodingException, InvalidAlgorithmParameterException {
        CreditCard vo = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            String sql =
                "SELECT " +
                    "cc.*, " +
                    "cct.id cct_id, " +
                    "cct.name cct_name " +
                "FROM " +
                    "credit_cards cc, " +
                    "credit_card_types cct " +
                "WHERE " +
                    "cc.cc_number = ? AND " +
                    "cc.user_id = ? AND " +
                    "cct.id = cc.type_id";
            pstmt = conn.prepareStatement(sql);
            pstmt.setString(1, AESUtil.encrypt(ccn));
            pstmt.setLong(2, userId);
            rs = pstmt.executeQuery();
            if (rs.next()) {
                vo = getVO(rs);
            }
        } catch (NumberFormatException e) {
        	log.error("NumberFormatException exceptin", e);
		} catch (CryptoException e) {
			log.error("Encryptor exceptin", e);
		} finally {
            closeResultSet(rs);
            closeStatement(pstmt);
        }
        return vo;
    }

    /**
     * Check if the specified card is used by user different than the specified one.
     *
     * @param conn
     * @param ccn
     * @param userId
     * @return Comma separated list of users different than the specified one that use this card or <code>null</code>
     *      if card not used by other users.
     * @throws SQLException
     * @throws UnsupportedEncodingException 
     * @throws BadPaddingException 
     * @throws IllegalBlockSizeException 
     * @throws NoSuchPaddingException 
     * @throws NoSuchAlgorithmException 
     * @throws InvalidKeyException 
     * @throws InvalidAlgorithmParameterException 
     */
	public static	ArrayList<com.anyoption.common.beans.base.User>
			checkCardExistence(Connection conn, long ccn, long userId)	throws SQLException, InvalidKeyException, NoSuchAlgorithmException,
																		NoSuchPaddingException, IllegalBlockSizeException,
																		BadPaddingException, UnsupportedEncodingException,
																		InvalidAlgorithmParameterException {
    	ArrayList<com.anyoption.common.beans.base.User> users = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            String sql =
                "SELECT " +
                    " u.user_name username, " +
                	" u.id user_id " +
                "FROM " +
                    "credit_cards c, " +
                    "users u " +
                "WHERE " +
                    "c.cc_number = ? AND " +
                    "c.user_id != ? AND " +
                    "u.id = c.user_id";
            pstmt = conn.prepareStatement(sql);
            pstmt.setString(1, AESUtil.encrypt(ccn));
            pstmt.setLong(2, userId);
            rs = pstmt.executeQuery();
            while (rs.next()) {
                if (users == null) {
                    users = new ArrayList<com.anyoption.common.beans.base.User>();
                }
                com.anyoption.common.beans.base.User u = new com.anyoption.common.beans.base.User();
                u.setId(rs.getLong("user_id"));
                u.setUserName(rs.getString("username"));
                users.add(u);
            }
        } catch (CryptoException e) {
        	log.error("Encryptor exceptin", e);
		} finally {
            closeResultSet(rs);
            closeStatement(pstmt);
        }
        return users;
    }
    
	public static void insert(Connection conn, CreditCard vo)	throws SQLException, InvalidKeyException, NoSuchAlgorithmException,
																NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException,
																UnsupportedEncodingException, InvalidAlgorithmParameterException {
		  PreparedStatement psBin=null;
		  ResultSet rsBin=null;
		  PreparedStatement ps = null;
		  ResultSet rs=null;
		  try {
			    String ccBinSql =
			    	" select * " +
			    	" from bins b " +
			    	" where ? between b.from_bin and b.to_bin ";

			    psBin = conn.prepareStatement(ccBinSql);
			    psBin.setString(1, vo.getBinStr());
			    rsBin = psBin.executeQuery();

			    if (rsBin.next()){
			    	long countryId = rsBin.getLong("country_id");
			    	long typeIdByBin = rsBin.getLong("card_type_id");

			    	if (countryId != 0){
			    		vo.setCountryId(countryId);
			    	}

			    	if (typeIdByBin != 0){
			    		vo.setTypeIdByBin(typeIdByBin);
			    	}else{
			    		vo.setTypeIdByBin(vo.getTypeId());
			    	}
			    } else {
			    	vo.setTypeIdByBin(vo.getTypeId());
			    }

				String sql=
					" insert into credit_cards(id,user_id,type_id,is_visible,cc_number,cc_number_back,cc_Pass,time_created," +
											 " exp_month,exp_year,holder_name,holder_id_num,time_modified,is_allowed, " +
											 " utc_offset_created, utc_offset_modified, country_id, type_id_by_bin, recurring_transaction, bin, cc_number_last_4_digits) " +
					" values(seq_credit_cards.nextval,?,?,?,?,?,?,sysdate,?,?,?,?,sysdate,?,?,?,?,?,'" + ConstantsBase.EMPTY_STRING + "',?,?) ";

				ps = conn.prepareStatement(sql);

				ps.setLong(1, vo.getUserId());
				ps.setLong(2, vo.getTypeId());
				ps.setInt(3, vo.isVisible() ? 1 : 0);
				ps.setString(4, AESUtil.encrypt(vo.getCcNumber()));
				ps.setLong(5, vo.getCcNumber());
				ps.setNull(6, Types.VARCHAR);

				ps.setString(7, vo.getExpMonth());
				ps.setString(8, vo.getExpYear());
				ps.setString(9, vo.getHolderName());
				ps.setString(10, vo.getHolderIdNum());

				ps.setInt(11, vo.getPermission());
				ps.setString(12, vo.getUtcOffsetCreated());
				ps.setString(13, vo.getUtcOffsetModified());
				ps.setLong(14, vo.getCountryId());
				ps.setLong(15, vo.getTypeIdByBin());
				ps.setLong(16, Long.parseLong(vo.getBinStr()));
				ps.setString(17, vo.getLast4DigitsStr());

				ps.executeUpdate();
				vo.setId(getSeqCurValue(conn,"seq_credit_cards"));
		  } catch (CryptoException ce) {
			  throw new SQLException(ce.getMessage());
		  } finally {
			  closeStatement(ps);
			  closeResultSet(rs);
			  closeStatement(psBin);
			  closeResultSet(rsBin);
		  }
    }

	public static void update(Connection con, CreditCard vo)	throws SQLException, InvalidKeyException, NoSuchAlgorithmException,
																NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException,
																UnsupportedEncodingException, InvalidAlgorithmParameterException {
        PreparedStatement pstmt = null;
        int index = 1;
        try {
            String sql =
                " UPDATE " +
                    "credit_cards " +
                " SET " +
                    "user_id = ?, " +
                    "type_id = ?, " +
                    "is_visible = ?, " +
                    "cc_number = ?, " +                        
                    "exp_month = ?, " +
                    "exp_year = ?, " +
                    "holder_name = ?, " +
                    "holder_id_num = ?, " +
                    "time_modified = sysdate, " +
                    "is_allowed = ?, " +
                    "utc_offset_modified = ?, " +
                    "is_documents_sent = ?, " +
                    "cc_Number_back = ? , " +
                    "recurring_transaction = ? , " +
                    "bin = ?, " +
                    "cc_number_last_4_digits = ? " +
                " WHERE " +
                    "id = ?";
            pstmt = con.prepareStatement(sql);
            pstmt.setLong(index++, vo.getUserId());
            pstmt.setLong(index++, vo.getTypeId());
            pstmt.setBoolean(index++, vo.isVisible());
            pstmt.setString(index++, AESUtil.encrypt(vo.getCcNumber()));             
            pstmt.setString(index++, vo.getExpMonth());
            pstmt.setString(index++, vo.getExpYear());
            pstmt.setString(index++, vo.getHolderName());
            pstmt.setString(index++, vo.getHolderIdNum());
            pstmt.setInt(index++, vo.getPermission());
            pstmt.setString(index++, vo.getUtcOffsetModified());
            pstmt.setBoolean(index++, vo.isDocumentsSent());
            pstmt.setLong(index++, vo.getCcNumber());
            pstmt.setString(index++, vo.getRecurringTransaction());
            pstmt.setLong(index++, Long.parseLong(vo.getBinStr()));
            pstmt.setString(index++, vo.getLast4DigitsStr());
            pstmt.setLong(index++, vo.getId());
            pstmt.executeUpdate();
        } catch (CryptoException ce) {
        	 throw new SQLException(ce.getMessage());
        } finally {
            closeStatement(pstmt);
        }
    }

	public static ArrayList<CreditCard> getVisibleByUserId(Connection con, long userId) throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		ArrayList<CreditCard> list = new ArrayList<CreditCard>();
		try {
		   String sql =
			   "SELECT " +
			   		"cc.*, " +
                    "cct.id cct_id, " +
                    "cct.name cct_name " +
			   "FROM " +
			   		"credit_cards cc, " +
                    "credit_card_types cct " +
			   "WHERE " +
			   		"cc.user_id = ? AND " +
			   		"cc.is_visible = 1 AND " +
                    "cct.id = cc.type_id";

			ps = con.prepareStatement(sql);
			ps.setLong(1, userId);
			rs = ps.executeQuery();

			while (rs.next()) {
				CreditCard vo = getVO(rs);
				list.add(vo);
			}
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		return list;
	  }

	  public static void updateWebDetails(Connection con, CreditCard card) throws SQLException {
		  PreparedStatement ps = null;
		  int ind = 1;
		  try {
				String sql =
							"UPDATE " +
							"	credit_cards " +
							"SET " +
							"	exp_month = ?, " +
							"	exp_year = ?, " +
							"	time_modified = sysdate, " +
							"	is_visible = 1, " +
							"	utc_offset_modified = ? " +
							"WHERE " +
							"	id = ?";

				ps = con.prepareStatement(sql);
							
				ps.setString(ind++, card.getExpMonth());
				ps.setString(ind++, card.getExpYear());
				ps.setString(ind++, card.getUtcOffsetModified());
				ps.setLong(ind++, card.getId());
				ps.executeUpdate();
		  }	finally	{
				closeStatement(ps);
		  }
	  }

	  public static void updateCardNotVisible(Connection con, long id) throws SQLException {
		  updateQuery(con,"update credit_cards set is_visible = 0 where id = " + id);
	  }

	  public static boolean isCreditCard3DSecure(Connection conn, String bin) throws SQLException {
	      boolean b = false;
	      PreparedStatement pstmt = null;
	      ResultSet rs = null;
	      try {
	          String sql =
	              "SELECT " +
	                  "id " +
	              "FROM " +
	                  "credit_card_3d_secure " +
	              "WHERE " +
	                  "start_bin <= ? AND " +
	                  "end_bin >= ?";
	          pstmt = conn.prepareStatement(sql);
	          pstmt.setString(1, bin);
	          pstmt.setString(2, bin);
	          rs = pstmt.executeQuery();
	          if (rs.next()) {
	              b = true;
	          }
	      } finally {
	          closeResultSet(rs);
	          closeStatement(pstmt);
	      }
	      return b;
	  }
	  
	  
	  /**
	   * Return all availble user credit cards, credit cards that the user make some
	   * deposit from them and the deposit is approved / pending.
	   * For not CFT enabled cards(Credit), need to display the credit amount that allow to withdrawal
	   * CFT enabled - in case the bin not! in bin_black_List for withdrawal
	   * To check that the deposit is approved, we take only cc that existing is some transaction
	   * with type of cc_deposit and the transaction status is success or panding
	   * @param con db connection
	   * @param id user id
	   * @return Array list of credit cards
	   * @throws SQLException
	   */
	  public static ArrayList<CreditCard> getCcWithrawalByUserId(Connection con,long id, Long creditCardId, long minToWithdrawal, long skinId, boolean onlyVisibleCards)  throws SQLException {
		  PreparedStatement ps = null;
		  ResultSet rs = null;
		  ArrayList<CreditCard> list = new ArrayList<CreditCard>();
		  List<CreditCard> tempList = new ArrayList<CreditCard>();
		  CreditCard vo = null;
//		  Date etraderMcStartDate = null;
		  int index = 1;

		  try {
			  	String sql = " SELECT " +
			  					" c.*, " +
			  					" cct.id cct_id, " +
			  					" cct.name cct_name "  +
							 " FROM " +
							 	" credit_cards c, " +
							 	" users u, " +
							 	" credit_card_types cct " +
							 " WHERE " +
								" c.user_id = ? ";
			  				if (null != creditCardId) {
  					   sql+=	" AND c.id = ? ";		
			  				}
							if (onlyVisibleCards){
					   sql+=	" AND c.is_visible = ? ";
							}
					   sql+=	" AND c.type_id = cct.id " +
					   			" AND c.user_id = u.id " +
								" AND EXISTS ( select 1 " +
												" from credit_cards c1, transactions t1 " +
												" where t1.credit_card_id = c1.id and c1.id = c.id " +
												" and t1.type_id = ? " +
												" and ( t1.status_id = ? or t1.status_id = ? ) " +
											" ) ";


				ps = con.prepareStatement(sql);
				ps.setLong(index++ , id);
				if (null != creditCardId) {
					ps.setLong(index++ , creditCardId);
				}
				if (onlyVisibleCards){
					ps.setLong(index++ , ConstantsBase.CC_VISIBLE);
				}
				ps.setLong(index++ , TransactionsManagerBase.TRANS_TYPE_CC_DEPOSIT);
				ps.setLong(index++ , TransactionsManagerBase.TRANS_STATUS_SUCCEED);
				ps.setLong(index++ , TransactionsManagerBase.TRANS_STATUS_PENDING);

				rs = ps.executeQuery();

				while (rs.next()) {
					vo = getVO(rs);
					tempList.add(vo);
				}
		  } catch (Exception e) {
				log.error("Error getting user credit cards", e);
			} finally {
				closeResultSet(rs);
				closeStatement(ps);
			}

//			// Get etraderMcEndTime
//			if (CommonUtil.isHebrewSkin(skinId)){
//				try {
//					String sql1 = " select to_date(value,'dd/mm/yyyy') start_time " +
//								  " from  ENUMERATORS " +
//								  " where code = ? ";
//
//					ps = con.prepareStatement(sql1);
//					ps.setString(1, ConstantsBase.ENUM_ET_MC_START_DATE);
//					rs = ps.executeQuery();
//					if (rs.next()) {
//						etraderMcStartDate = CommonUtil.convertToDate(rs.getTimestamp("start_time"));
//					}
//				} catch (Exception e) {
//					logger.error("Error getting etraderMcEndTime", e);
//				} finally {
//					closeResultSet(rs);
//					closeStatement(ps);
//				}
//			}

			try {
				for (int i = 0; i < tempList.size(); i++) {
					String sql =
  					" SELECT " +
  						" c.* , " +
	  					" cct.id cct_id, " +
	  					" cct.name cct_name, "  +
					    " u.balance, " +
					    " cr.withdraw_clearing_provider_id, " +
					    " cr.cft_clearing_provider_id, " +
  						" ( NVL( (select sum (t1.amount - t1.credit_amount) " +
							   " from transactions t1, credit_cards c1 " +
							   " where c1.id = t1.credit_card_id " +
							   		"AND decode(ur.time_created,null,u.time_created,ur.time_created) < t1.time_created " +
							   		"and t1.credit_card_id = c.id " +
								  	" and t1.type_id = ? " +
								  	" and ( t1.status_id = ? or t1.status_id = ? )  " +
								  	(skinId == Skin.SKIN_ETRADER && tempList.get(i).getTypeIdByBin() == TransactionsManagerBase.CC_TYPE_VISA ? " and to_char(t1.time_created, 'yyyymmdd') > '20121209' " : " " ) +
								  	" and t1.user_id = ? " +
								  	"),0 ) - NVL( (select sum (t2.amount) " +
									                 "from transactions t2, credit_cards c2 " +
									                 "where c2.id = t2.credit_card_id " +
										                 "AND decode(ur.time_created,null,u.time_created,ur.time_created) < t2.time_created " +
										                 "and t2.credit_card_id = c.id " +
										                 (skinId == Skin.SKIN_ETRADER && tempList.get(i).getTypeIdByBin() == TransactionsManagerBase.CC_TYPE_VISA ? " and to_char(t2.time_created, 'yyyymmdd') > '20121209' " : " " ) +
										                 "and t2.type_id = ?" +
										                 "and t2.status_id in (" + TransactionsManagerBase.TRANS_STATUS_REQUESTED + "," + TransactionsManagerBase.TRANS_STATUS_APPROVED + "," + TransactionsManagerBase.TRANS_STATUS_SECOND_APPROVAL + ")" +
										                 "and t2.user_id = ? and t2.is_credit_withdrawal = 1 " +
				               "),0) " +
				        " ) AS CREDIT_AMOUNT_ALLOW, " +
						" ( select max(t.time_created) " +
								  " from transactions t " +
								  " where " +
								  		" t.type_id = " + TransactionsManagerBase.TRANS_TYPE_CC_DEPOSIT + " " +
								  		"AND decode(ur.time_created,null,u.time_created,ur.time_created) < t.time_created " +
								  		" AND t.status_id in (" + TransactionsManagerBase.SUCCESS_DEPOSITS_TYPES + ") " +
								  		" AND t.credit_card_id = c.id  " +
								   " ) AS LAST_CC_DEPOSIT " +

					" FROM " +
						" credit_cards c, " +
						" clearing_routes cr, " +
						" credit_card_types cct, " +
						" users u left join users_regulation ur on u.id = ur.user_id " +
					" WHERE " +
						" c.id = ? " + 
						(skinId == Skin.SKIN_ETRADER && tempList.get(i).getTypeIdByBin() == TransactionsManagerBase.CC_TYPE_VISA ? " and c.TYPE_ID_BY_BIN = 2 " : " " ) +
						" and c.type_id = cct.id " +
						" and c.user_id = u.id " +
						" and cr.is_active = 1 " +
						" and (cr.start_bin is null or (substr(?,0,6) between cr.start_bin and cr.end_bin) ) " +
						" and (cr.cc_type is null or cr.cc_type = c.type_id_by_bin) " +
						" and (cr.country_id is null or cr.country_id = c.country_id) " +
						" and (cr.currency_id is null or cr.currency_id = u.currency_id) " +
						" and (cr.skin_id is null or cr.skin_id = u.skin_id) " +
						" and (cr.business_skin_id is null or cr.business_skin_id in (select s.business_case_id " +
						   														    " from skins s " +
																					" where s.id = u.skin_id))" +
					    " and NOT EXISTS ( select 1 " +
											"from cc_black_List " +
											"where cc_number = ? and " +
											"is_active = 1 " +
											") " +
						" and cr.platform_id = u.platform_id " +
					" ORDER BY " +
		                " cr.start_bin, " +
		                " cr.currency_id, " +
		                " cr.cc_type, " +
		                " cr.country_id, " +
		                " cr.skin_id, " +
		                " cr.business_skin_id ";

				ps = con.prepareStatement(sql);
				ps.setLong(1, TransactionsManagerBase.TRANS_TYPE_CC_DEPOSIT);
				ps.setLong(2, TransactionsManagerBase.TRANS_STATUS_SUCCEED);
				ps.setLong(3, TransactionsManagerBase.TRANS_STATUS_PENDING);
				ps.setLong(4, id);
				ps.setLong(5, TransactionsManagerBase.TRANS_TYPE_CC_WITHDRAW);
				ps.setLong(6, id);
				
					ps.setLong(7, tempList.get(i).getId());
					ps.setLong(8, tempList.get(i).getCcNumber());
					ps.setLong(9, tempList.get(i).getCcNumber());
					rs = ps.executeQuery();

					if (rs.next()) {
						vo = getVO(rs);
						long creditClearingProviderId = rs.getLong("withdraw_clearing_provider_id");
						vo.setClearingProviderId(creditClearingProviderId);
						vo.setCreditEnabled(creditClearingProviderId > 0);

						// CFT / CREDIT
						long cftClearingProviderId = rs.getLong("cft_clearing_provider_id");
						vo.setCftClearingProviderId(cftClearingProviderId);
						vo.setCftAvailable(cftClearingProviderId > 0 && skinId != Skin.SKIN_ETRADER);


//						long nonCftAvailable = ClearingManager.getProviderNonCftAvailable(clearingProviderId);
//						if ( nonCftAvailable == ConstantsBase.EXISTS ) {
//							vo.setNonCftAvailable(true);
//						} else {
//							vo.setNonCftAvailable(false);
//						}

						if (vo.isCftAvailable()) {
							vo.setCreditEnabled(false);
						}

						// set the credit amount
						long userBalance = rs.getLong("balance");
						long creditAmountAllow = rs.getLong("CREDIT_AMOUNT_ALLOW");
						vo.setCreditAmount(Math.min(userBalance, creditAmountAllow));

//						// Inatec CFT clearing provider
//						if (vo.getClearingProviderId() == ClearingManager.INATEC_PROVIDER_ID_CFT) {
//							vo.setCftAvailable(true);
//							vo.setCreditEnabled(false);
//							list.put(vo.getId(), vo);
//							continue;
//						}

//						String appSource = ApplicationDataBase.getAppSource();
						// Display cft cards or credit with amount >= minToWithdrawal
//						if (appSource.equals(ConstantsBase.APPLICATION_SOURCE_WEB)) {
						if (vo.isCftAvailable() || (vo.isCreditEnabled() && vo.getCreditAmount() >= minToWithdrawal)) {
							list.add(vo);
						}
//						} else {  // for Backend use - allow to withdrawal any amount
//							if (vo.isCftAvailable() || ( vo.isCreditEnabled() && vo.getCreditAmount() >= 0 )) {
//								list.put(vo.getId(), vo);
//							}
//						}
					}
					closeResultSet(rs);
				}
			} catch (Exception e) {
				log.error("Error getting user credit cards.", e);
			} finally {
				closeResultSet(rs);
				closeStatement(ps);
			}
			return list;		  
	  }
	  
	  /**
		* Check if credit cart exist for this user 
		* @param con db connection
		* @param creditCardId credit cart id
		* @param userId user id
		* @throws SQLException
		*/
		public static boolean checkIfCardIsValid(Connection con, long creditCardId, long userId){
			PreparedStatement pstmt = null;
			ResultSet rs = null;
			try {
				String sql = "select * from credit_cards"+ 
						" where credit_cards.id = ?"+
						" and credit_cards.user_id = ?";
				pstmt = con.prepareStatement(sql);
				pstmt.setLong(1, creditCardId);
				pstmt.setLong(2, userId);
				rs = pstmt.executeQuery();
				if (rs.next()) {
					return true;
				}
			} catch (SQLException e) {
				log.error("" + e);
			} finally {
				closeStatement(pstmt);
				closeResultSet(rs);
			}
			return false;
		}
		
		public static long getLastActiveCreditCardsByUser(Connection con, long userId) throws SQLException {
			PreparedStatement ps = null;
			ResultSet rs = null;
			long returnCard = 0L;
			try {
			   String sql =
				   " select" +
						   " credit_card_id " +
				   " from (" +
				   		" select " +
				   			" credit_card_id " +
				   		" from transactions " +
				   		" where status_id in (" + TransactionsManagerBase.TRANS_STATUS_SUCCEED + ", " + TransactionsManagerBase.TRANS_STATUS_PENDING + ", " + TransactionsManagerBase.TRANS_STATUS_CANCELED_ETRADER + ") " +
				   			" and credit_card_id is not null " +
				   			" and user_id=? " +
				   		" order by time_created desc) " +
				   " where rownum<=1";
				ps = con.prepareStatement(sql);
				ps.setLong(1, userId); 
				rs = ps.executeQuery();

				while (rs.next()) {
					returnCard = rs.getLong("credit_card_id");

				}
			} finally {
				closeResultSet(rs);
				closeStatement(ps);
			}
			return returnCard;
		  }
		public static boolean isCcInBinsList(Connection con, long ccNumber) throws SQLException{
			PreparedStatement ps=null;
			ResultSet rs=null;

			try {
				String sql=
					" select * " +
				  	" from bins b " +
				  	" where " +
				  		" b.from_bin <= substr(?,1,6) " +
				  		" and b.to_bin >= substr(?,1,6) " +
				  		" and b.card_type_id = " + ConstantsBase.CC_TYPE_MASTERCARD;

				ps = con.prepareStatement(sql);
				ps.setLong(1, ccNumber);
				ps.setLong(2, ccNumber);
				rs=ps.executeQuery();

				if (rs.next()) {
					return true;
				}

			} finally {
				closeResultSet(rs);
				closeStatement(ps);
			}

			return false;
		}
		
		public static HashMap<Long, ArrayList<String>> getCCStolenProviderErrCodes(Connection con){
			PreparedStatement pstmt = null;
			ResultSet rs = null;
			HashMap<Long, ArrayList<String>> hm = new HashMap<>();
			try {
				String sql = "select * from stolen_cc_err_code "; 
				pstmt = con.prepareStatement(sql);
				rs = pstmt.executeQuery();
				while(rs.next()) {
					long providerType = rs.getLong("PROVIDER_TYPE");
					if(hm.get(providerType) == null){
						ArrayList<String> err = new ArrayList<>();
						hm.put(providerType, err);
					}
					String errCode = rs.getString("CODE");
					hm.get(providerType).add(errCode);
				
				}
			} catch (SQLException e) {
				log.error("Can't getCCStolenProviderErrCodes " + e);
			} finally {
				closeStatement(pstmt);
				closeResultSet(rs);
			}
			return hm;
		}
		
	public static int updateCCDocumentSent(long fileTypeId, long userId, long ccId, Connection conn) throws SQLException {
		CallableStatement cstmt = null;
		ResultSet rs = null;
		int index = 1;
		try {
			cstmt = conn.prepareCall("{call PKG_CREDIT_CARD.UPDATE_CC_DOCUMENT_SENT(O_ROWS_UPDATED => ? " + 
																				  ",I_USER_ID => ? " +
																				  ",I_FILE_TYPE_ID => ? " + 
																				  ",I_CC_ID => ?)}");
			cstmt.registerOutParameter(index++, OracleTypes.INTEGER);
			cstmt.setLong(index++, userId);
			cstmt.setLong(index++, fileTypeId);
			cstmt.setLong(index, ccId);

			cstmt.executeUpdate();
			return cstmt.getInt(1);

		} finally {
			closeStatement(cstmt);
			closeResultSet(rs);
		}
	}
		
}