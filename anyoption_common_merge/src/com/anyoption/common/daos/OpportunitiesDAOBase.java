/**
 * 
 */
package com.anyoption.common.daos;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.EnumMap;
import java.util.List;
import java.util.Map;

import com.anyoption.common.beans.Market;
import com.anyoption.common.beans.Opportunity;
import com.anyoption.common.beans.OpportunityMiniBean;
import com.anyoption.common.beans.OpportunitySkinGroupMap;
import com.anyoption.common.beans.ReutersQuotes;
import com.anyoption.common.beans.base.OpportunityOddsPair;
import com.anyoption.common.enums.SkinGroup;
import com.anyoption.common.util.DynamicsUtil;
import com.anyoption.common.util.OpportunityCacheBean;

/**
 * @author pavelhe
 *
 */
public class OpportunitiesDAOBase extends DAOBase {
	
	protected static Map<SkinGroup, OpportunitySkinGroupMap> loadOpportunitySkingGroupMappings(Connection conn, long opportunityId) throws SQLException {
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		Map<SkinGroup, OpportunitySkinGroupMap> map = new EnumMap<SkinGroup, OpportunitySkinGroupMap>(SkinGroup.class);
		String sql = " select * from OPPORTUNITY_SKIN_GROUP_MAP where opportunity_id = ? ";
		
		try {
			pstmt = conn.prepareStatement(sql);
			pstmt.setLong(1, opportunityId);
			rs = pstmt.executeQuery();
			OpportunitySkinGroupMap oppMapping = null;
			while (rs.next()) {
				oppMapping = new OpportunitySkinGroupMap();
				oppMapping.setId(rs.getLong("ID"));
				oppMapping.setOpportunityId(rs.getLong("OPPORTUNITY_ID"));
				oppMapping.setSkinGroup(SkinGroup.getById(rs.getLong("SKIN_GROUP_ID")));
				oppMapping.setTimeCreated(convertToDate(rs.getTimestamp("time_created")));
				oppMapping.setWriterId(rs.getLong("WRITER_ID"));
				oppMapping.setShiftParameter(rs.getDouble("SHIFT_PARAMETER"));
				oppMapping.setMaxExposure(rs.getInt("MAX_EXPOSURE"));
				map.put(oppMapping.getSkinGroup(), oppMapping);
			}
			
			// Create default mapping objects for those groups that have no records in DB
			for (SkinGroup group : SkinGroup.NON_AUTO_SET) {
				if (!map.containsKey(group)) {
					oppMapping = new OpportunitySkinGroupMap();
					oppMapping.setOpportunityId(opportunityId);
					oppMapping.setSkinGroup(group);
					oppMapping.setTimeCreated(new Date());
				}
			}
			
		} finally {
			closeResultSet(rs);
            closeStatement(pstmt);
		}
		
		return map;
	}

	/**
	 * Get all odds pair for selector
	 * @param con
	 * @return
	 * @throws SQLException
	 */
	public static ArrayList<OpportunityOddsPair> getOpportunityOddsPair(Connection con) throws SQLException {
		ArrayList<OpportunityOddsPair> list = new ArrayList<OpportunityOddsPair>();
		PreparedStatement ps = null;
		ResultSet rs = null;

		try {
			String sql = "SELECT " +
							" oop.selector_id, " +
							" oop.refund, " +
							" oop.return " +
						"FROM " +
					        "opportunity_odds_pair oop " +
					    "ORDER BY  oop.selector_id,oop.return " ;

			ps = con.prepareStatement(sql);
			rs = ps.executeQuery();

			while (rs.next()) {
				OpportunityOddsPair op = new OpportunityOddsPair();
				op.setSelectorID(rs.getLong("selector_id"));
				op.setRefundSelector(rs.getLong("refund"));
				op.setReturnSelector(rs.getLong("return"));
				list.add(op);
			}
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}

		return list;
	}

	public static OpportunityMiniBean getOpportunityMiniBean(Connection con, long id) throws SQLException {
		OpportunityMiniBean oppMiniBean = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			String sql = "SELECT o.ID, o.ODDS_TYPE_ID, o.ODDS_GROUP, o.IS_DISABLED, m.IS_SUSPENDED, o.is_suspended as o_is_suspended "
						+ "FROM opportunities o, markets m "
						+ "WHERE o.MARKET_ID = m.ID AND o.ID = ?";
			ps = con.prepareStatement(sql);
			ps.setLong(1, id);
			rs = ps.executeQuery();
			if (rs.next()) {
				oppMiniBean = new OpportunityMiniBean();
				oppMiniBean.setId(rs.getLong("ID"));
				oppMiniBean.setOddsTypeId(rs.getLong("ODDS_TYPE_ID"));
				oppMiniBean.setOddsGroup(rs.getString("ODDS_GROUP"));
				oppMiniBean.setDisabled(rs.getBoolean("IS_DISABLED"));
				oppMiniBean.setMarketSuspended(rs.getBoolean("IS_SUSPENDED"));
				oppMiniBean.setOppSuspended(rs.getBoolean("o_is_suspended"));
			}
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		return oppMiniBean;
	}

	public static List<Opportunity> getPastExpiredOpportunities(Connection con, long after) throws SQLException, ParseException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		List<Opportunity> opps = new ArrayList<Opportunity>();
		try {
			String sql = "SELECT closing_level, to_char(time_est_closing, 'YYYY-MM-DD HH24:MI:SS.FF3 TZHTZM') AS ftime_est_closing, "
							+ "market_id, o.id, scheduled, to_char(time_first_invest, 'YYYY-MM-DD HH24:MI:SS.FF3 TZHTZM') as ftime_first_invest, "
							+ "opportunity_type_id, last, ask, bid "
						+ "FROM opportunities o, reuters_quotes rq "
						+ "WHERE SYS_EXTRACT_UTC(time_act_closing) > (sysdate-7) AND time_act_closing > ? AND o.id = rq.opportunity_id "
						+ "ORDER BY time_est_closing desc";
			ps = con.prepareStatement(sql);
			ps.setTimestamp(1, new Timestamp(after));
			rs = ps.executeQuery();
			while (rs.next()) {
				addPastExpiredOpp(opps, rs, true);
			}
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		return opps;
	}

	public static List<Opportunity> getPastExpiredOppsForCacheInit(Connection con, int count) throws SQLException, ParseException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		List<Opportunity> opps = new ArrayList<Opportunity>();
		try {
			String sql = "SELECT * "
						+ "FROM "
							+ "(SELECT row_number() over (partition by op.market_id order by op.time_act_closing desc nulls last ) rank, op.market_id, "
									+ "to_char(op.time_est_closing, 'YYYY-MM-DD HH24:MI:SS.FF3 TZHTZM') AS ftime_est_closing, op.closing_level, op.id, "
									+ "op.scheduled, to_char(op.time_first_invest, 'YYYY-MM-DD HH24:MI:SS.FF3 TZHTZM') AS ftime_first_invest, "
									+ "op.opportunity_type_id "
								+ "FROM opportunities op, (select distinct market_id  from OPPORTUNITY_TEMPLATES  where is_active = 1) active_markets "
								+ "WHERE SYS_EXTRACT_UTC(op.time_act_closing) > (sysdate-7) and active_markets.market_id = op.MARKET_ID "
								+ "ORDER BY op.market_id, op.time_est_closing desc) "
						+ "WHERE rank <= ?";
			ps = con.prepareStatement(sql);
			ps.setInt(1, count);
			rs = ps.executeQuery();
			while (rs.next()) {
				addPastExpiredOpp(opps, rs, false);
			}
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		return opps;
	}

	private static void addPastExpiredOpp(List<Opportunity> list, ResultSet rs, boolean loadQuotes) throws SQLException, ParseException {
		Opportunity opp = new Opportunity();
		list.add(opp);
		opp.setId(rs.getLong("id"));
		opp.setClosingLevel(rs.getDouble("closing_level"));
		SimpleDateFormat localDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS Z");
		opp.setTimeEstClosing(localDateFormat.parse(rs.getString("ftime_est_closing")));
		opp.setMarketId(rs.getLong("market_id"));
		opp.setScheduled(rs.getInt("scheduled"));
		opp.setTimeFirstInvest(localDateFormat.parse(rs.getString("ftime_first_invest")));
		opp.setTypeId(rs.getLong("opportunity_type_id"));
		if (loadQuotes) {
			opp.setLastUpdateQuote(new ReutersQuotes(null, rs.getBigDecimal("last"), rs.getBigDecimal("ask"), rs.getBigDecimal("bid")));
		}
	}
	
    public static OpportunityCacheBean getOpportunityCacheBean(Connection conn, long oppId) throws SQLException {
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        OpportunityCacheBean oppcb = null;
        try {
            String sql =
                "SELECT " +
                    "A.id, " +
                    "to_char(A.time_first_invest, 'YYYY-MM-DD HH24:MI:SS.FF3 TZHTZM') AS time_first_invest, " +
                    "to_char(A.time_last_invest, 'YYYY-MM-DD HH24:MI:SS.FF3 TZHTZM') AS time_last_invest, " +
                    "to_char(A.time_est_closing, 'YYYY-MM-DD HH24:MI:SS.FF3 TZHTZM') AS time_est_closing, " +
                    "A.scheduled, " +
                    "B.display_name, " +
                    "B.decimal_point, " +
                    "B.id AS market_id, " +
                    "B.quote_params, " +
                    "A.opportunity_type_id, " +
                    "A.current_level, " +
                    "A.max_inv_coeff, " +
                    "B.type_id " +
                "FROM " +
                    "opportunities A, " +
                    "markets B " +
                "WHERE " +
                    "A.market_id = B.id AND " +
                    "A.id = ?";

            pstmt = conn.prepareStatement(sql);
            pstmt.setLong(1, oppId);
            rs = pstmt.executeQuery();
            if (rs.next()) {
                oppcb = new OpportunityCacheBean();
                oppcb.setId(rs.getLong("id"));
                oppcb.setTimeFirstInvest(getTimeWithTimezone(rs.getString("time_first_invest")));
                oppcb.setTimeLastInvest(getTimeWithTimezone(rs.getString("time_last_invest")));
                oppcb.setTimeEstClosing(getTimeWithTimezone(rs.getString("time_est_closing")));
                oppcb.setScheduled(rs.getInt("scheduled"));
                oppcb.setMarketDisplayName(rs.getString("display_name"));
                oppcb.setMarketDecimalPoint(rs.getLong("decimal_point"));
                oppcb.setMarketId(rs.getLong("market_id"));
                oppcb.setCurrentLevel(rs.getDouble("current_level"));
                oppcb.setOpportunityTypeId(rs.getLong("opportunity_type_id"));
                oppcb.setMaxInvAmountCoeffPerUser(rs.getDouble("max_inv_coeff"));
                String quoteParams = rs.getString("quote_params"); // Binary 0-100 and Dynamics parameters
                int marketTypeId = rs.getInt("type_id");
                if(marketTypeId == Market.PRODUCT_TYPE_DYNAMICS) {
                	oppcb.setQuoteParams(DynamicsUtil.parseOpportunityQuoteParams(quoteParams));
                }
            }
        } finally {
            closeResultSet(rs);
            closeStatement(pstmt);
        }
        return oppcb;
    }
    
	public static long getMarketCurrentOpportunityBinary0100(Connection conn, long marketId) throws SQLException {
	    long oppId = 0;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            String sql =
                "SELECT " +
                    "A.id " +
                "FROM " +
                    "opportunities A, " +
                    "markets B " +
                "WHERE " +
                    "A.market_id = B.id AND " +
                    "A.market_id = ? AND " +
                    "A.is_published = 1 AND " +
//                    "current_timestamp < A.time_last_invest AND " +
                    "A.current_level > 0 AND " + 
                    "A.is_suspended = 0 " + //opportunity not suspended
                "ORDER BY " +
                    "A.time_est_closing";
            pstmt = conn.prepareStatement(sql);
            pstmt.setLong(1, marketId);
            rs = pstmt.executeQuery();
            if (rs.next()) {
                oppId = rs.getInt("id");
            }
        } finally {
            closeResultSet(rs);
            closeStatement(pstmt);
        }
        return oppId;
	}
	
	public static long getBinary0100CurrentMarket(Connection conn, long skinId) throws SQLException {
        long mId = 0;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            String sql =
           		"SELECT " +
            		"B.market_id " +
            	"FROM " +
            		"skin_market_groups A, " +
            		"skin_market_group_markets B, " +
            		"markets C, " +
            		"opportunities D, " +
            		"opportunity_types E " +
            	"WHERE " +
            		"A.skin_id = ? AND " +
            		"A.id = B.skin_market_group_id AND " +
            		"B.market_id = C.id AND " +
            		"B.market_id = D.market_id AND " +
            		"D.opportunity_type_id = E.id AND " +
            		"E.product_type_id = 4 AND " + // Binary 0-100
            		"D.is_published = 1 AND " +
            		"C.is_suspended = 0 AND " +
            		"D.closing_level IS NULL AND " +
            		"D.current_level > 0 AND "+ 
            		"D.is_suspended = 0 " + 	//opportunity not suspended
            	"GROUP BY " +
            		"B.market_id, " +
            		"B.home_page_priority, " +
            		"CASE WHEN current_timestamp < D.time_last_invest THEN 0 ELSE 1 END " +
            	"ORDER BY " +
            		"CASE WHEN current_timestamp < D.time_last_invest THEN 0 ELSE 1 END, " +
            		"B.home_page_priority";
            pstmt = conn.prepareStatement(sql);
            pstmt.setLong(1, skinId);
            rs = pstmt.executeQuery();
            if (rs.next()) {
            	mId = rs.getLong("market_id");
            }
        } finally {
            closeResultSet(rs);
            closeStatement(pstmt);
        }
        return mId;
	}
	
	public static void createOpportunitiesForMarket(Connection conn, long marketId) throws SQLException {
		CallableStatement cstmt = null;
		try {
			cstmt = conn.prepareCall("{call pkg_markets.create_opportunities(i_market_id => ? )}");
			cstmt.setLong(1, marketId);
			cstmt.execute();
		} finally {
			closeStatement(cstmt);
		}
	}

	public static List<Opportunity> initPastExpiriesForProlongedGraph(	Connection con, List<Integer> scheduledTypes, Date after,
																		boolean order) throws SQLException, ParseException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		List<Opportunity> opps = new ArrayList<Opportunity>();
		try {
			String sql = "select closing_level, to_char(time_est_closing, 'YYYY-MM-DD HH24:MI:SS.FF3 TZHTZM') AS ftime_est_closing, market_id, "
							+ "o.id, scheduled, to_char(time_first_invest, 'YYYY-MM-DD HH24:MI:SS.FF3 TZHTZM') as ftime_first_invest, "
							+ "opportunity_type_id, last, ask, bid "
						+ "from opportunities o, reuters_quotes rq "
						+ "where scheduled member of ? and time_act_closing >= ? and opportunity_type_id = " + Opportunity.TYPE_REGULAR + " and o.id = rq.opportunity_id ";
			if (order) {
				sql += "order by market_id, time_est_closing";
			}
			ps = con.prepareStatement(sql);
			ps.setArray(1, getPreparedSqlArray(con, scheduledTypes));
			ps.setTimestamp(2, new Timestamp(after.getTime()));
			rs = ps.executeQuery();
			while (rs.next()) {
				addPastExpiredOpp(opps, rs, true);
			}
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		return opps;
	}
}