package com.anyoption.common.daos;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Locale;
import java.util.Map;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.MarketTranslations;
import com.anyoption.common.beans.Skin;
import com.anyoption.common.beans.base.AssetIndexFormula;
import com.anyoption.common.beans.base.ExpiryFormulaCalculation;
import com.anyoption.common.beans.base.Market;
import com.anyoption.common.beans.base.MarketAssetIndex;
import com.anyoption.common.beans.base.MarketAssetIndexInfo;
import com.anyoption.common.managers.LanguagesManagerBase;
import com.anyoption.common.managers.MarketAssetIndexManager;
import com.anyoption.common.managers.MarketsManagerBase;
import com.anyoption.common.managers.SkinsManagerBase;
import com.anyoption.common.util.CommonUtil;

import oracle.jdbc.OracleTypes;

public class MarketAssetIndexDAO extends DAOBase {
	
	private static final Logger log = Logger.getLogger(MarketAssetIndexDAO.class);
	
	public static Map<Long, MarketAssetIndex> getMarketAssetIndicesBySkin(Connection con, long skinId) throws SQLException {
		log.debug("Begin init MarketAssetIndices for SkinID:" + skinId);
		PreparedStatement ps = null;
		ResultSet rs = null;
		Map<Long, MarketAssetIndex> marketAssetIndicesBySkin = new LinkedHashMap<Long, MarketAssetIndex>();
		try {
            String sql = 
            		" select " +
            				" market_description, " +
            				" order_data.* " +
            		" from " +
            				" (select base_date.*, " +
            		              "case " +
            		                 " when base_date.is_bo = 1 then 1 " +
            		                 " when base_date.is_oplus = 1 then 3 " +
            		                 " when base_date.is_bubbles = 1 then 6 else 7 end opp_type " +
            		        " from " +
            		               " (select " +
            		               		" distinct " +
            		               		" m.display_name market_display_name, " +
            		                    " m.feed_name market_feed_name, " +
            		                    " m.id marketid, " +
            		                    " m.name marketname, " +
            		                    " m.time_created market_time_created, " +
            		                    " ot.*, " +
            		                    " case " +
            		                    	" when mdsm.hour_closing_formula_id is not null then 1 else 0 end hourly, " +
            		                    " case " +
            		                        " when mdsm.day_closing_formula_id is not null then 1 else 0 end daily, " +
            		                    " case " +
            		                        " when mdsm.long_term_formula_id is not null then 1 else 0 end weekly, " +
            		                    " case " +
            		                        " when mdsm.long_term_formula_id is not null then 1 else 0 end monthly, " +
            		                    " sm.market_group_id, " +
            		                    " mg.display_name group_name, " +
            		                    " mdg.asset_display_name display_group_name_key, " +
            		                    " mdg.id display_group_id, " +
            		                    " nvl(smg.skin_display_group_id, 0) country_groups, " +
            		                    " mns.name market_name_by_skin, " + 
            		                    " m.option_plus_market_id, " +
            		                    " aii.id indexs_info_id, " +
            		                    " (select i.is_24_7 from asset_index i where i.market_id = m.id) is_24_7, " +
            		                    " (select i.trading_days from asset_index i where i.market_id = m.id) trading_days, " +
            		                    " (select is_full_day from asset_index i where i.market_id = m.id) is_full_day, " +
            		                    " (select to_timestamp_tz(to_char(sysdate, 'yyyymmdd ') || i.start_break_time " +
                                            " ,'yyyymmdd hh24:mi') from asset_index i where i.market_id = m.id) start_break_time, " +
            		                    " (select to_timestamp_tz(to_char(sysdate, 'yyyymmdd ') || i.end_break_time " +
											" ,'yyyymmdd hh24:mi') from asset_index i where i.market_id = m.id) end_break_time, " +
            		                    " sm.skin_id indx_skin_id, " +
            		                    " aii.additional_text, " +
            		                    " aii.id, " +
            		                    " aii.page_description, " +
            		                    " aii.page_keywords, " +
            		                    " aii.page_title, " + 
            		                    " aii.skin_id, " +
            		                    " aii.market_description_page market_description_in_page, " +
            		                    " nvl(mdsm.hour_closing_formula_id, 0) hour_formula, " +
            		                    " nvl(mdsm.day_closing_formula_id, 0) day_formula, " +
            		                    " nvl(mdsm.long_term_formula_id, 0) long_term_formula " +
            		                " from " +
            		                    " markets m " +                
            		                    	" join " +
            		                    		" (select " +
            		                    				" * " +
            		                    		 " from " +
            		                    				" (select " +
            		                    						" ot.market_id, " +
            		                    						" ot.scheduled, " +
            		                    						" max(case " +
            		                    								" when ot.opportunity_type_id in (1) then 1 else 0 end) is_bo, " +
            		                    						" max(case " +
            		                    								" when ot.opportunity_type_id in (3) then 1 else 0 end) is_oplus, " +
            		                    						" max(case " +
            		                    								" when ot.opportunity_type_id in (6) then 1 else 0 end) is_bubbles, " +
            		                    						" max(case " +
            		                    								" when ot.opportunity_type_id in (7) then 1 else 0 end) is_dynamics, " +               
            		                    						" min(sys_extract_utc(to_timestamp_tz(to_char(sysdate, 'yyyymmdd ') || ot.time_first_invest || ' ' || ot.time_zone " +
            		                                                                            " ,'yyyymmdd hh24:mi tzr'))) start_time, " +
            		                                            " max(sys_extract_utc(to_timestamp_tz(to_char(sysdate, 'yyyymmdd ') || ot.time_est_closing || ' ' || ot.time_zone " +
            		                                                                            " ,'yyyymmdd hh24:mi tzr'))) end_time, " +
            		                                            " row_number() over (partition by ot.market_id order by case ot.scheduled when 2 then 1 when 1 then 2 else ot.scheduled end) rn " +
            		                                     " from " +
            		                                            " opportunity_templates ot " +
            		                                     " where " +
            		                                            " ot.is_active = 1 " +
            		                                            //" and ot.is_full_day in (1, 2) " +
            		                                            " and (ot.scheduled = 2 or ot.opportunity_type_id in (3, 6, 7)) " +
        		                                         " group  by market_id, scheduled) " +
        		                                 " where  rn = 1) ot " +
        		                                 		" on ot.market_id = m.id join skin_market_group_markets smg " +
        		                                 		" on smg.market_id = m.id join skin_market_groups sm " +
        		                                 		" on sm.id = smg.skin_market_group_id left join market_groups mg " +
        		                                 		" on mg.id = sm.market_group_id left join market_name_skin mns " +
        		                                 		" on mns.market_id = m.id and mns.skin_id = sm.skin_id left join asset_indexs_info aii " +
        		                                 		" on aii.market_id = m.id and aii.skin_id = sm.skin_id left join market_data_source_maps mdsm " +
        		                                 		" on mdsm.market_id = m.id left join market_display_groups mdg " +
        		                                 		" on smg.skin_display_group_id = mdg.id " +
            		                			 " where  sm.skin_id = ? " +
            		                	" ) base_date " +
            		                " ) order_data join asset_indexs_info ai " +
            		                " on ai.market_id = order_data.marketid and ai.skin_id = order_data.skin_id " +
            		                " order  by " +
            		                	" case " +
            		                		" when order_data.market_group_id = 2 and order_data.opp_type = 1 then 1 " +
            		                		" when order_data.market_group_id = 4 and order_data.opp_type = 1 then 2 " +
            		                		" when order_data.market_group_id = 5 and order_data.opp_type = 1 then 3 " +
            		                		" when order_data.market_group_id in (3, 6) and order_data.opp_type = 1 then 4 " +
            		                		" when order_data.opp_type = 3 then 5 " +
            		                		" when order_data.opp_type = 6 then 6 " +
            		                		" when order_data.opp_type = 7 then 7 end, " +
            		                	" case " +
            		                		" when order_data.country_groups = 6 and order_data.opp_type = 1 then -1 " +
            		                		" when order_data.country_groups = 1 and order_data.opp_type = 1 then 1 " +
            		                		" when order_data.country_groups = 2 and order_data.opp_type = 1 then 2 " +
            		                		" when order_data.country_groups = 4 and order_data.opp_type = 1 then 3 " +
            		                		" when order_data.country_groups = 3 and order_data.opp_type = 1 then 4 " +
            		                		" when order_data.country_groups = 5 and order_data.opp_type = 1 then 5 " +
            		                		" else 6 end, " +
            		                	" upper(market_name_by_skin) ";

			ps = con.prepareStatement(sql);
            ps.setLong(1, skinId);
			rs = ps.executeQuery();

			while(rs.next()) {
				MarketAssetIndex marketAssetIndex = new MarketAssetIndex();
				Market market = new Market();
				MarketAssetIndexInfo marketAssetIndexInfo = new MarketAssetIndexInfo();
				
				market.setDisplayNameKey(rs.getString("market_display_name"));
				market.setDisplayName(rs.getString("market_name_by_skin"));
				market.setId(rs.getLong("marketId"));
				market.setName(rs.getString("marketName"));
				market.setTimeCreated(convertToDate(rs.getTimestamp("market_time_created")));
				market.setOptionPlusMarketId(rs.getLong("option_plus_market_id"));								
				market.setHaveHourly(rs.getInt("hourly") == 0 ? false : true);
				market.setHaveDaily(rs.getInt("daily") == 0 ? false : true);
				market.setHaveWeekly(rs.getInt("weekly") == 0 ? false : true);
				market.setHaveMonthly(rs.getInt("monthly") == 0 ? false : true);
                market.setGroupId(rs.getLong("market_group_id"));
                market.setGroupName(rs.getString("group_name"));
                market.setDisplayGroupNameKey(rs.getString("display_group_name_key"));
                market.setOptionPlusMarket(rs.getInt("option_plus_market_id") > 0 ? true : false);
                if (rs.getString("market_feed_Name").indexOf("Binary0-100") > -1) {
                	market.setFeedName(rs.getString("market_feed_Name").substring(0, rs.getString("market_feed_Name").length() - 12));
                } else if (rs.getString("market_feed_Name").indexOf("Option+") > -1) {
                	market.setFeedName(rs.getString("market_feed_Name").substring(0, rs.getString("market_feed_Name").length() - 8));
                } else if (rs.getString("market_feed_Name").indexOf("Dynamicssss") > -1) {
                	market.setFeedName(rs.getString("market_feed_Name").substring(0, rs.getString("market_feed_Name").length() - 12));
                } else if (rs.getString("market_feed_Name").indexOf("Bubbles") > -1) {
                	market.setFeedName(rs.getString("market_feed_Name").substring(0, rs.getString("market_feed_Name").length() - 8));
                } else {
                	market.setFeedName(rs.getString("market_feed_Name"));
                }
                market.setFeedName(getDisplayFeedName(market.getFeedName()));
                
                market.setNewMarket(MarketsManagerBase.isNewMarket(market));
            	marketAssetIndex.setMarket(market);
            	marketAssetIndex.setOpportunityTypeId(rs.getLong("OPP_TYPE"));
            	marketAssetIndex.setMarketGroupId(rs.getLong("market_group_id"));
            	marketAssetIndex.setMarketDisplayGroupId(rs.getLong("display_group_id"));
                
            	marketAssetIndexInfo.setMarketId(rs.getLong("marketId"));
            	marketAssetIndexInfo.setSkinId(skinId);
            	marketAssetIndexInfo.setMarketDescription(CommonUtil.clobToString(rs.getClob(("MARKET_DESCRIPTION"))));
            	marketAssetIndexInfo.setAdditionalText(rs.getString("ADDITIONAL_TEXT"));
            	marketAssetIndexInfo.setPageTitle(rs.getString("PAGE_TITLE"));
            	marketAssetIndexInfo.setPageKeyWords(rs.getString("PAGE_KEYWORDS"));
            	marketAssetIndexInfo.setPageDescription(rs.getString("PAGE_DESCRIPTION"));
            	marketAssetIndexInfo.setMarketDescriptionPage(rs.getString("MARKET_DESCRIPTION_IN_PAGE"));
                if(marketAssetIndexInfo.getMarketDescriptionPage() == null) { // instead of NVL
                	marketAssetIndexInfo.setMarketDescriptionPage(marketAssetIndexInfo.getMarketDescription());
                }
				marketAssetIndexInfo.setExpiryFormulaCalculations(getExpiryFormulaCalculation(market.getFeedName(),
						rs.getLong("hour_formula"),	rs.getLong("day_formula"), rs.getLong("long_term_formula"), null, null, skinId));
                marketAssetIndexInfo.setIs24h7(rs.getInt("is_24_7") == 0 ? false : true);
                marketAssetIndexInfo.setTradingDays(rs.getString("trading_days"));
                marketAssetIndexInfo.setStartTime(convertToDate(rs.getTimestamp("start_time")));
                marketAssetIndexInfo.setEndTime(convertToDate(rs.getTimestamp("end_time")));
                marketAssetIndexInfo.setBreakStartTime(convertToDate(rs.getTimestamp("start_break_time")));
                marketAssetIndexInfo.setBreakEndTime(convertToDate(rs.getTimestamp("end_break_time")));
                //marketAssetIndexInfo.setFullDay(rs.getBoolean("is_full_day"));
                marketAssetIndexInfo.setOpportunityType(rs.getLong("OPP_TYPE"));
                
                marketAssetIndex.setMarketAssetIndexInfo(marketAssetIndexInfo);
                
                marketAssetIndicesBySkin.put(rs.getLong("marketId"), marketAssetIndex);           
			}
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		log.debug("End init AssetIndexMarkets");
		return marketAssetIndicesBySkin;
	}
	
	public static Map<Long, MarketAssetIndex> mobileAssetIndexMarketsBySkin(Connection con, long skinId) throws SQLException {
		log.debug("Begin init mobile MarketAssetIndices for SkinID:" + skinId);
		PreparedStatement ps = null;
		ResultSet rs = null;
		Map<Long, MarketAssetIndex> mobileMarketAssetIndicesBySkin = new LinkedHashMap<Long, MarketAssetIndex>();
		try {
            String sql = " select " +
                                    " aii.market_description description, " +
    		                        " m.feed_name market_feed_name, " +  
        		                    " ot.*, " +
        		                    " case " +
	    		                    	" when mdsm.hour_closing_formula_id is not null then 1 else 0 end hourly, " +
	    		                    " case " +
	    		                        " when mdsm.day_closing_formula_id is not null then 1 else 0 end daily, " +
	    		                    " case " +
	    		                        " when mdsm.long_term_formula_id is not null then 1 else 0 end weekly, " +
	    		                    " case " +
	    		                        " when mdsm.long_term_formula_id is not null then 1 else 0 end monthly, " +
        		                    " (select i.trading_days from asset_index i where i.market_id = m.id) trading_days, " +  
        		                    " (select to_timestamp_tz(to_char(sysdate, 'yyyymmdd ') || i.start_break_time " + 
                                             " ,'yyyymmdd hh24:mi') from asset_index i where i.market_id = m.id) start_break_time, " +
        		                    " (select to_timestamp_tz(to_char(sysdate, 'yyyymmdd ') || i.end_break_time " + 
                                             " ,'yyyymmdd hh24:mi') from asset_index i where i.market_id = m.id) end_break_time, " +   
        		                    " nvl(mdsm.hour_closing_formula_id, 0) hour_formula, " + 
        		                    " nvl(mdsm.day_closing_formula_id, 0) day_formula, " + 
        		                    " nvl(mdsm.long_term_formula_id, 0) long_term_formula, " +
                                    " (select formula_description_key from market_formulas mf where mf.id = mdsm.hour_closing_formula_id) hour_formula_key, " +
                                    " (select formula_description_key from market_formulas mf where mf.id = mdsm.day_closing_formula_id) day_formula_key " +
    		                " from " + 
        		                    " markets m " +                 
    		                    	 " join " + 
        		                    		" (select " + 
        		                    				 " * " + 
        		                    		  " from " + 
        		                    				 " (select " + 
        		                    						 " ot.market_id, " + 
        		                    						 " ot.scheduled, " +            
        		                    						 " min(sys_extract_utc(to_timestamp_tz(to_char(sysdate, 'yyyymmdd ') || ot.time_first_invest || ' ' || ot.time_zone " + 
                                                                         " ,'yyyymmdd hh24:mi tzr'))) start_time, " + 
        		                                             " max(sys_extract_utc(to_timestamp_tz(to_char(sysdate, 'yyyymmdd ') || ot.time_est_closing || ' ' || ot.time_zone " + 
                                                                         " ,'yyyymmdd hh24:mi tzr'))) end_time, " + 
        		                                             " row_number() over (partition by ot.market_id order by case ot.scheduled " +
                                                 						 " when 2 then 1 when 1 then 2 else ot.scheduled end) rn " + 
        		                                      " from " + 
        		                                             " opportunity_templates ot " + 
        		                                      " where " + 
        		                                             " ot.is_active = 1 " + 
        		                                             //" and ot.is_full_day in (1, 2) " + 
        		                                             " and (ot.scheduled = 2 or ot.opportunity_type_id in (3, 6, 7)) " + 
    		                                          " group  by market_id, scheduled) " + 
    		                                  " where  rn = 1) ot " + 
                             		 " on ot.market_id = m.id join skin_market_group_markets smg " + 
                             		 " on smg.market_id = m.id join skin_market_groups sm " + 
                             		 " on sm.id = smg.skin_market_group_id  left join asset_indexs_info aii " + 
                             		 " on aii.market_id = m.id and aii.skin_id = sm.skin_id left join market_data_source_maps mdsm " + 
                             		 " on mdsm.market_id = m.id left join market_display_groups mdg " + 
                             		 " on smg.skin_display_group_id = mdg.id " + 
                			  " where  sm.skin_id = ? ";
            ps = con.prepareStatement(sql);
            ps.setLong(1, skinId);
			rs = ps.executeQuery();

			while(rs.next()) {
				MarketAssetIndex marketAssetIndex = new MarketAssetIndex();
				MarketAssetIndexInfo marketAssetIndexInfo = new MarketAssetIndexInfo();
				Market market = new Market();
				String feedName = "";
                if (rs.getString("market_feed_Name").indexOf("Binary0-100") > -1) {
                	feedName = rs.getString("market_feed_Name").substring(0, rs.getString("market_feed_Name").length() - 12);
                } else if (rs.getString("market_feed_Name").indexOf("Option+") > -1) {
                	feedName = rs.getString("market_feed_Name").substring(0, rs.getString("market_feed_Name").length() - 8);
                } else if (rs.getString("market_feed_Name").indexOf("Dynamicssss") > -1) {
                	feedName = rs.getString("market_feed_Name").substring(0, rs.getString("market_feed_Name").length() - 12);
                } else if (rs.getString("market_feed_Name").indexOf("Bubbles") > -1) {
                	feedName = rs.getString("market_feed_Name").substring(0, rs.getString("market_feed_Name").length() - 8);
                } else {
                	feedName = rs.getString("market_feed_Name");
                }
                feedName = getDisplayFeedName(feedName);
        		market.setHaveHourly(rs.getInt("hourly") == 0 ? false : true);
				market.setHaveDaily(rs.getInt("daily") == 0 ? false : true);
				market.setHaveWeekly(rs.getInt("weekly") == 0 ? false : true);
				market.setHaveMonthly(rs.getInt("monthly") == 0 ? false : true);
            	marketAssetIndexInfo.setMarketId(rs.getLong("market_id"));
            	marketAssetIndexInfo.setFeedName(feedName);
            	marketAssetIndexInfo.setMarketDescription(CommonUtil.clobToString(rs.getClob(("description"))));
				marketAssetIndexInfo.setExpiryFormulaCalculations(getExpiryFormulaCalculation(marketAssetIndexInfo.getFeedName(),
								rs.getLong("hour_formula"), rs.getLong("day_formula"), rs.getLong("long_term_formula"),
								rs.getString("hour_formula_key"), rs.getString("day_formula_key"), skinId));
                marketAssetIndexInfo.setTradingDays(rs.getString("trading_days"));
                marketAssetIndexInfo.setStartTime(convertToDate(rs.getTimestamp("start_time")));
                marketAssetIndexInfo.setEndTime(convertToDate(rs.getTimestamp("end_time")));
                marketAssetIndexInfo.setBreakStartTime(convertToDate(rs.getTimestamp("start_break_time")));
                marketAssetIndexInfo.setBreakEndTime(convertToDate(rs.getTimestamp("end_break_time")));
                
                marketAssetIndex.setMarketAssetIndexInfo(marketAssetIndexInfo);
                marketAssetIndex.setMarket(market);
                
                mobileMarketAssetIndicesBySkin.put(rs.getLong("market_id"), marketAssetIndex);
			}
            
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		log.debug("End init mobile AssetIndexMarkets");
		return mobileMarketAssetIndicesBySkin;
            
	}
	
	public static String getDisplayFeedName(String feedName) {
		String returnFeedName = feedName;
		if( feedName.equals("NQ") || feedName.equals("TRF30") || feedName.equals("FKLI") || feedName.equals(".KLSE") || feedName.equals("SPX")){
		  feedName += "c1";
		  returnFeedName = feedName;
		  }
		else if (feedName.equals("SI") || feedName.equals("GC") || feedName.equals("CL") || feedName.equals("HG") || feedName.equals("S") || feedName.equals("NG")) {
		    feedName += "v1";
		    returnFeedName = feedName;
		  }
		return returnFeedName;
	}
	
	private static ArrayList<ExpiryFormulaCalculation> getExpiryFormulaCalculation(String feedName,	long hourlyClosingId,
					long dailyClosingId, long longTermClosingId, String hourFormulaKey, String dayFormulaKey, long skinId) {
		ArrayList<ExpiryFormulaCalculation> list = new ArrayList<ExpiryFormulaCalculation>();
		Locale locale = new Locale(LanguagesManagerBase.getLanguage(SkinsManagerBase.getSkin(skinId).getDefaultLanguageId()).getCode());
		if(hourlyClosingId > 0) {
			ExpiryFormulaCalculation expFormulaCalc = new ExpiryFormulaCalculation();
			expFormulaCalc.setExpiryFormulaId(hourlyClosingId);
			expFormulaCalc.setReutersField(feedName);
			expFormulaCalc.setExpiryTypeId(MarketAssetIndexManager.EXPIRY_TYPE_HOURLY);
			if (hourFormulaKey != null) {
				expFormulaCalc.setExpiryFormulaTxt(CommonUtil.getMessage(locale, hourFormulaKey, null));
			}
			list.add(expFormulaCalc);
		}
		if(dailyClosingId > 0) {
			ExpiryFormulaCalculation expFormulaCalc = new ExpiryFormulaCalculation();
			expFormulaCalc.setExpiryFormulaId(dailyClosingId);
			if (hourlyClosingId > 0 && (hourlyClosingId != dailyClosingId)) {
				expFormulaCalc.setReutersField(feedName + " Last value");
			} else {
				expFormulaCalc.setReutersField(feedName);
			}
			expFormulaCalc.setExpiryTypeId(MarketAssetIndexManager.EXPIRY_TYPE_DAILY);
			if (hourFormulaKey != null) {
				expFormulaCalc.setExpiryFormulaTxt(CommonUtil.getMessage(locale, hourFormulaKey, null));
			}
			list.add(expFormulaCalc);
		}
		if(longTermClosingId > 0) {
			ExpiryFormulaCalculation expFormulaCalc = new ExpiryFormulaCalculation();
			expFormulaCalc.setExpiryTypeId(MarketAssetIndexManager.EXPIRY_TYPE_LONG_TERM);
			list.add(expFormulaCalc);
		}
		return list;
	}
	
	public static ArrayList<AssetIndexFormula> getAssetIndexFormulas(Connection con) throws SQLException {
		log.debug("Begin init AssetIndexFormulas ");
		PreparedStatement ps = null;
		ResultSet rs = null;
		ArrayList<AssetIndexFormula> list = new ArrayList<AssetIndexFormula>();
		
		try {
			String sql = " select id, formula_description_key from market_formulas order by id ";
			
			ps = con.prepareStatement(sql);
			rs = ps.executeQuery();

			while(rs.next()) {
				AssetIndexFormula formula = new AssetIndexFormula();
				formula.setFormulaId(rs.getInt("id"));
				formula.setFormulaKey(rs.getString("formula_description_key"));
				list.add(formula);
			}
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		log.debug("End init AssetIndexFormulas");
		return list;
	}
	
	public static MarketTranslations getMarketTranslationsByMarketId(Connection con, long marketId) throws SQLException {
		log.debug("Begin init Market Translations ");
		ResultSet rs = null;
		CallableStatement cstmt = null;
		int index = 1;
		MarketTranslations marketTranslations = new MarketTranslations();
		Map<Long, String> marketName = new HashMap<Long, String>();
		Map<Long, String> marketShortName = new HashMap<Long, String>();
		Map<Long, String> marketDescription = new HashMap<Long, String>();
		Map<Long, String> additionalText = new HashMap<Long, String>();
		Map<Long, String> pageDescription = new HashMap<Long, String>();
		Map<Long, String> pageKeywords = new HashMap<Long, String>();
		Map<Long, String> pageTitle = new HashMap<Long, String>();
		Map<Long, String> descriptionInPage = new HashMap<Long, String>();
		
		try {
			cstmt = con.prepareCall("{call pkg_markets.get_market_translations(o_data => ? " +
																				",i_market_id => ? )}");

			cstmt.registerOutParameter(index++, OracleTypes.CURSOR);
			cstmt.setLong(index++, marketId);
			
			cstmt.executeQuery();
			rs = (ResultSet) cstmt.getObject(1);
			while (rs.next()) {
				marketName.put(rs.getLong("default_language_id"), rs.getString("name"));
				marketShortName.put(rs.getLong("default_language_id"), rs.getString("short_name"));
				marketDescription.put(rs.getLong("default_language_id"), CommonUtil.clobToString(rs.getClob("MARKET_DESCRIPTION")));
				additionalText.put(rs.getLong("default_language_id"), rs.getString("additional_text"));
				pageDescription.put(rs.getLong("default_language_id"), rs.getString("page_description"));
				pageKeywords.put(rs.getLong("default_language_id"), rs.getString("page_keywords"));
				pageTitle.put(rs.getLong("default_language_id"), rs.getString("page_title"));
				descriptionInPage.put(rs.getLong("default_language_id"), rs.getString("market_description_page"));
			}
			marketTranslations.setMarketName(marketName);
			marketTranslations.setMarketShortName(marketShortName);
			marketTranslations.setMarketDescription(marketDescription);
			marketTranslations.setAdditionalText(additionalText);
			marketTranslations.setPageDescription(pageDescription);
			marketTranslations.setPageKeywords(pageKeywords);
			marketTranslations.setPageTitle(pageTitle);
			marketTranslations.setDescriptionInPage(descriptionInPage);
		} finally {
			closeStatement(cstmt);
			closeResultSet(rs);
		}
		log.debug("End init Market Translations");
		return marketTranslations;
	}
	
	public static void updateMarketTranslations(Connection con, long marketId, long languageId, String name, String shortName,
												String additionalText, String marketDescription, String pageDescription,
												String pageKeywords, String pageTitle, String marketDescriptionInPage) throws SQLException {
		log.debug("Begin update Market Translations ");
		CallableStatement cstmt = null;
		int index = 1;
		try {
			String sql = "{call pkg_markets.update_market_translations(i_market_id => ? , " +
					 													" i_language_id => ? , " +
					 													" i_name => ? , " +
					 													" i_short_name => ? , " +
					 													" i_additional_text => ? , " +
					 													" i_market_description => ? , " +
					 													" i_page_description => ? , " +
					 													" i_page_keywords => ? , " +
					 													" i_page_title => ? , " +
					 													" i_market_description_page => ? " +
					 												")}";

			cstmt = con.prepareCall(sql);
			cstmt.setLong(index++, marketId);
			cstmt.setLong(index++, languageId);
			setStatementValue(name.isEmpty() ? MarketsManagerBase.getMarketName(Skin.SKIN_REG_EN, marketId) : name, index++, cstmt);
			setStatementValue(shortName.isEmpty() ? MarketsManagerBase.getMarketName(Skin.SKIN_REG_EN, marketId) : shortName, index++, cstmt);
			setStatementValue(additionalText, index++, cstmt);
			setStatementValue(marketDescription, index++, cstmt);
			setStatementValue(pageDescription, index++, cstmt);
			setStatementValue(pageKeywords, index++, cstmt);
			setStatementValue(pageTitle, index++, cstmt);
			setStatementValue(marketDescriptionInPage, index++, cstmt);
			cstmt.executeUpdate();
		} finally {
			closeStatement(cstmt);
		}
	}

}