package com.anyoption.common.daos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.RewardTask;
import com.anyoption.common.beans.RewardTaskCurrency;
import com.anyoption.common.beans.RewardUserPlatform;
/**
 * @author liors
 *
 */
public class RewardUserPlatformsDAO extends DAOBase {
	private final static Logger logger = Logger.getLogger(RewardUserPlatformsDAO.class);

	public static boolean isTaskDone(Connection connection, long userId, long taskId) throws SQLException {
    	PreparedStatement pstmt = null;
		ResultSet resultSet = null;
		int ind = 1;
		try {   
    		String sql =	"SELECT " +
							  "COUNT(*) " +
							"FROM " +
							  "rwd_user_platforms rup " +
							"WHERE " +
							  "rup.user_id = ? AND " +
							  "rup.num_inv >= ( " +
							  				"SELECT " +
							  					"rtp.num_inv " +
							  				"FROM " +
							  					"rwd_task_platforms rtp " +
							  				"WHERE " +
							  					"rtp.rwd_task_id = ?) " +
							"HAVING " +
							  "COUNT(*) >= ( " +
							  				"SELECT " +
							  					"rtp.num_platforms " +
							  				"FROM " +
							  					"rwd_task_platforms rtp " +
							  				"WHERE " +
							  					"rtp.rwd_task_id = ?) ";
				            
		    pstmt = connection.prepareStatement(sql);
		    pstmt.setLong(ind++, userId);
		    pstmt.setLong(ind++, taskId);
		    pstmt.setLong(ind++, taskId);
		    resultSet = pstmt.executeQuery();

		    if (resultSet.next()) {
		    	logger.debug("isTaskDone true");
		    	return true;
		    }
    	} finally {
		   closeStatement(pstmt);
		   closeResultSet(resultSet);
		}
		logger.debug("isTaskDone false");
		return false;
	}
	
	public static void insert(Connection connection, RewardUserPlatform vo) throws SQLException {
		PreparedStatement ps = null;
		int ind = 1;
		try {
			String sql =
						"INSERT " +
						"INTO RWD_USER_PLATFORMS " +
					    "( " +
					      "turnover, " +
					      "num_inv, " +
					      "user_id, " +
					      "product_type_id, " +
					      "id, " +
					      "exp_points " +
					    ") " +
					    "VALUES " +
					    "( " +
					      "?, " +
					      "?, " +
					      "?, " +
					      "?, " +
					      "seq_rwd_user_platforms.nextval, " +
					      "? " +
					    ")";

			ps = connection.prepareStatement(sql);
			ps.setLong(ind++, vo.getTurnover());
			ps.setInt(ind++, vo.getNumOfInvestments());
			ps.setLong(ind++, vo.getUserId());
			ps.setInt(ind++, vo.getProductTypeId());
			ps.setDouble(ind++, vo.getExperiencePoints());
			ps.executeUpdate();
			
			vo.setId(getSeqCurValue(connection, "seq_rwd_user_platforms"));
		} finally {
			closeStatement(ps);
		}
	}
	
	public static void updateAfterInvestment(Connection con, RewardUserPlatform vo) throws SQLException {

		PreparedStatement ps = null;

		try {

			String sql = "UPDATE " +
							"rwd_user_platforms " +
						 "SET " + 
						 	"turnover = turnover + ?, " +
						    "num_inv = num_inv + 1 " +
						 "WHERE " +
						 	"id = ?";

			ps = con.prepareStatement(sql);

			ps.setLong(1, vo.getTurnover());
			ps.setLong(2, vo.getId());
			ps.executeUpdate();
	
		} finally {
			closeStatement(ps);
		}
	  }
	
	public static void updateExpPointsAndTurnover(Connection con, RewardUserPlatform vo) throws SQLException {

		PreparedStatement ps = null;

		try {

			String sql = "UPDATE " +
							"rwd_user_platforms " +
						 "SET " + 
						 	"exp_points	= exp_points + ?, " +
						 	"turnover = turnover + ? " +
						 "WHERE " +
						 	"id = ?";

			ps = con.prepareStatement(sql);

			ps.setDouble(1, vo.getExperiencePoints());
			ps.setLong(2, vo.getTurnover());
			ps.setLong(3, vo.getId());
			ps.executeUpdate();
	
		} finally {
			closeStatement(ps);
		}
	}
	
	public static void updateExpPoints(Connection con, RewardUserPlatform vo) throws SQLException {

		PreparedStatement ps = null;

		try {

			String sql = "UPDATE " +
							"rwd_user_platforms " +
						 "SET " + 
						 	"exp_points	= exp_points + ? " +
						 "WHERE " +
						 	"id = ?";

			ps = con.prepareStatement(sql);

			ps.setDouble(1, vo.getExperiencePoints());
			ps.setLong(2, vo.getId());
			ps.executeUpdate();
	
		} finally {
			closeStatement(ps);
		}
	}
	
	public static RewardTask getRewardTaskCustomValue(ResultSet resultSet) throws SQLException {
		RewardTask valueObject = new RewardTask();
		valueObject.setId(resultSet.getInt("rt_id"));
		valueObject.setTypeId(resultSet.getInt("rwd_task_type_id"));
		valueObject.setLevelId(resultSet.getInt("rwd_task_level_id"));
		valueObject.setRewardTierId(resultSet.getInt("rwd_tier_id"));
		valueObject.setNumOfActions(resultSet.getInt("num_actions"));
		valueObject.setNumOfRecurring(resultSet.getInt("num_recurring"));
		valueObject.setExperiencePoints(resultSet.getDouble("rt_exp_points"));
		valueObject.setRewardTaskCurrency(new RewardTaskCurrency(resultSet.getDouble("amount_to_points")));
		
		return valueObject;
	}
}
