package com.anyoption.common.daos;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Types;

import org.apache.log4j.Logger;

public class RiskAlertsDAOBase extends DAOBase {
	  private static final Logger log = Logger.getLogger(RiskAlertsDAOBase.class);

		public static boolean isCCFromDiffCountries(Connection conn, long ccID, long transactionId) throws SQLException {
			CallableStatement cstmt = null;
			boolean res;
			int index = 1;
			try {
				String sql = "{call pkg_ccalert.check_card_country(?, ?, ?)}";
				cstmt = conn.prepareCall(sql);
				cstmt.registerOutParameter(index++, Types.INTEGER);
				cstmt.setLong(index++, ccID);
				cstmt.setLong(index++, transactionId);
				cstmt.executeQuery();				
				
				res = cstmt.getBoolean(1);					
				log.debug("The credit card ID:" + ccID + " is from Diff Countries");
			} finally {
				closeStatement(cstmt);
			}			
			return res;
		}
}
