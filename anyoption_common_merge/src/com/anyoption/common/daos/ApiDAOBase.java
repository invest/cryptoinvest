package com.anyoption.common.daos;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.UUID;

import javax.faces.model.SelectItem;

import org.bouncycastle.crypto.CryptoException;

import com.anyoption.common.beans.ApiUser;
import com.anyoption.common.beans.ApiUserService;
import com.anyoption.common.beans.base.ApiErrorCode;
import com.anyoption.common.beans.base.ApiPage;
import com.anyoption.common.beans.base.ApiUserActivity;
import com.anyoption.common.beans.base.FTDUser;

import oracle.jdbc.OracleTypes;

/**
 * @author EranL
 *
 */
public class ApiDAOBase extends DAOBase {

	/**
	 * Get API user
	 * @param con
	 * @param userName
	 * @return
	 * @throws SQLException
	 */
	public static void getAPIUser(Connection con, String userName, String password, ApiUser apiUser) throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			String sql =" SELECT " +
						"	*	" +
	                    " FROM " +
	                    "	api_users au " +
	                    " WHERE " +
	                    "	au.user_name = ? " +
	                    "	AND au.password = ? ";
	        ps = con.prepareStatement(sql);
	        ps.setString(1, userName);
	        ps.setString(2, password);
	        rs = ps.executeQuery();
	        if (rs.next()) {
	        	apiUser.setId(rs.getLong("ID"));
	        	apiUser.setUserName(rs.getString("USER_NAME"));
	        	apiUser.setPassword(rs.getString("PASSWORD"));
	        	apiUser.setTimeCreated(convertToDate(rs.getTimestamp("TIME_CREATED")));
	        	apiUser.setType(rs.getInt("TYPE"));
	        	apiUser.setAffiliateCode(rs.getLong("AFFILIATE_CODE"));
	        	apiUser.setActive(rs.getInt("IS_ACTIVE") == 1 ? true : false);
	        	apiUser.setRoleId(rs.getLong("role_id"));
	        }
		} finally {
			closeResultSet(rs);
	        closeStatement(ps);
		}
	}


	/**
	 * Get API error codes
	 * @param con
	 * @return
	 * @throws SQLException
	 */
	public static Hashtable<String, ApiErrorCode> getAPIErrorCodes(Connection con) throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		Hashtable<String, ApiErrorCode> list = new Hashtable<String, ApiErrorCode>();
		try {
			String sql =" SELECT " +
						"	*	" +
	                    " FROM " +
	                    "	api_error_codes ";
	        ps = con.prepareStatement(sql);
	        rs = ps.executeQuery();
	        while (rs.next()) {
	        	ApiErrorCode vo = new ApiErrorCode();
	        	vo.setId(rs.getLong("ID"));
	        	vo.setTypeId(rs.getLong("TYPE_ID"));
	        	vo.setCode(rs.getString("CODE"));
	        	vo.setDescription(rs.getString("DESCRIPTION"));
	        	vo.setDisplayName(rs.getString("DISPLAY_NAME"));
	        	list.put(vo.getCode(), vo);
	        }
		} finally {
			closeResultSet(rs);
	        closeStatement(ps);
		}
		return list;
	}


	/**
	 * Insert into API User Activity
	 * 1. insert user
	 * 2. insert contact
	 * 3. insert investment
	 * @param con
	 * @return
	 * @throws SQLException
	 */
	public static void insertAPIUserActivity(Connection con, ApiUserActivity apiUserActivity) throws SQLException {
		PreparedStatement ps = null;
		try {
			String sql= " INSERT " +
						" INTO api_user_activity(id, " +
						"						 type_id, " +
						"						 key_value," +
						"						 api_user_id," +
						"						 time_created) " +
						" VALUES(seq_api_user_activity.NEXTVAL, ?, ?, ?, SYSDATE)";
			ps = con.prepareStatement(sql);
			ps.setInt(1, apiUserActivity.getTypeId());
			ps.setLong(2, apiUserActivity.getKeyValue());
			ps.setLong(3, apiUserActivity.getApiUserId());
			ps.executeUpdate();
		} finally {
			closeStatement(ps);
		}
	}

	/**
	 * Insert user id into Login token
	 * @param con
	 * @return
	 * @throws SQLException
	 */
	public static String insertLoginToken(Connection con, long userId) throws SQLException {
		PreparedStatement ps = null;
		String uuidStr = null;
		try {
			String sql= " INSERT " +
						" INTO api_login_token(id, " +
						"					   user_id, " +
						"					   time_created, " +
						"					   uuid) " +
						" VALUES(seq_api_login_token.NEXTVAL, ?, SYSDATE, ?)";
			ps = con.prepareStatement(sql);
			ps.setLong(1, userId);
			UUID uuid = UUID.randomUUID();
			uuidStr = uuid.toString();
			ps.setString(2, uuidStr);
			ps.executeUpdate();
		} finally {
			closeStatement(ps);
		}
		return uuidStr;
	}

	/**
	 * Get API pages - for login procedure
	 * @param con
	 * @return
	 * @throws SQLException
	 */
	public static Hashtable<Long, ApiPage> getApiPages(Connection con) throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		Hashtable<Long, ApiPage> list = new Hashtable<Long, ApiPage>();
		try {
			String sql =" SELECT " +
						"	*	" +
	                    " FROM " +
	                    "	api_pages ";
	        ps = con.prepareStatement(sql);
	        rs = ps.executeQuery();
	        while (rs.next()) {
	        	ApiPage vo = new ApiPage();
	        	vo.setId(rs.getLong("ID"));
	        	vo.setUrl(rs.getString("URL"));
	        	vo.setDescription(rs.getString("DESCRIPTION"));
	        	vo.setCanManualRedirect(rs.getInt("CAN_MANUAL_REDIRECT") == 0 ? false : true);
	        	list.put(vo.getId(), vo);
	        }
		} finally {
			closeResultSet(rs);
	        closeStatement(ps);
		}
		return list;
	}

	/**
	 * Get API user
	 * @param con
	 * @param userName
	 * @return
	 * @throws SQLException
	 * @throws CryptoException
	 */
	public static String validateAPIUser(Connection con, String userName, String password) throws SQLException, CryptoException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			String sql =" SELECT " +
						"	password	" +
	                    " FROM " +
	                    "	api_users au " +
	                    " WHERE " +
	                    "	au.user_name = ? " +
	                    "	AND au.is_active = 1 ";
	        ps = con.prepareStatement(sql);
	        ps.setString(1, userName);

	        rs = ps.executeQuery();
	        if (rs.next()) {
	        	return rs.getString("password");
	        }
		} finally {
			closeResultSet(rs);
	        closeStatement(ps);
		}
		return null;
	}
	
	/**
	 * Get API pages - for login procedure
	 * @param con
	 * @return
	 * @throws SQLException
	 */
	public static Hashtable<Long, Hashtable<Long, Long>> getAPIUsersGroup(Connection con) throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		Hashtable<Long, Hashtable<Long, Long>> list = new Hashtable<Long, Hashtable<Long, Long>>();
		try {
			String sql =" SELECT " +
						"	* " +
						" FROM " +
						"	api_users_group " +
						" ORDER BY " +
						"	api_user_id, " +
						"	user_id ";
	        ps = con.prepareStatement(sql);
	        rs = ps.executeQuery();
	        while (rs.next()) {
	        	long apiUserId = rs.getLong("API_USER_ID");	  
	        	long userId = rs.getLong("USER_ID");
	        	Hashtable<Long, Long> apiUserGroup = list.get(apiUserId); 
	        	if (apiUserGroup == null) {
	        		apiUserGroup = new Hashtable<Long, Long>();	
	        		list.put(new Long(apiUserId), apiUserGroup);
	        	}	        		        	
	        	if (list.get(apiUserId).get(userId) == null) {
	        		list.get(apiUserId).put(userId, userId);
	        	}
	        }
		} finally {
			closeResultSet(rs);
	        closeStatement(ps);
		}
		return list;
	}
	
	/**
	 * Update notifications status and comments for API users
	 * @param con
	 * @param apiUserId
	 * @param listOfId
	 * @param comment
	 * @param statusId
	 * @throws SQLException
	 */
	public static void updateAPINotifications(Connection con, long apiUserId ,String listOfId, String comment, long statusId) throws SQLException {	
		PreparedStatement ps = null;
		try {
			String sql= " UPDATE " +
						"	api_notification n " +
						" SET " +
						"	n.status_id = ?, " +
						"	n.comments = n.comments || ' | ' || ? " +
						" WHERE " +
						"	n.id in ( " +
						"				SELECT " +
						"				    an.id " +
						"				FROM " +
						"				    api_notification an, " +
						"					api_external_users aeu, " +
						"					api_users au " +
						"				WHERE " +
						"					au.id = aeu.api_user_id " +
						"					AND an.api_external_user_id = aeu.id " +
						"					AND an.id IN ( " + listOfId + " ) " +  
						"					AND au.id = ? " +
						"			)		";
						
			ps = con.prepareStatement(sql);
			ps.setLong(1, statusId);
			ps.setString(2, comment);
			ps.setLong(3, apiUserId);
			ps.executeUpdate();
		} finally {
			closeStatement(ps);
		}
	}

    /**
     * Get API user
     * @param con
     * @param userName
     * @return
     * @throws SQLException
     * @throws CryptoException
     */
    public static ArrayList<SelectItem> getApiUsersTypeExternalSI(Connection con) throws SQLException {
        ArrayList<SelectItem> list = new ArrayList<SelectItem>();
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            String sql = " SELECT " +
                            " au.id, " +
                            " au.user_name " +
                         " FROM " +
                            " api_users au " +
                         " WHERE " +
                            " au.type = " + ApiUser.API_USER_TYPE_EXTERNAL + " " +
                         " ORDER BY " +
                            " user_name";
            ps = con.prepareStatement(sql);

            rs = ps.executeQuery();
            while (rs.next()) {
                SelectItem user = new SelectItem(new Long(rs.getLong("id")), rs.getString("user_name"));
                list.add(user);
            }
        } finally {
            closeResultSet(rs);
            closeStatement(ps);
        }
        return list;
    }
    
    /**
     * Get first time deposit users by date and API user
     * @param con
     * @return
     * @throws SQLException
     */
    public static ArrayList<FTDUser> getFTDUsersByDate(Connection con, long apiUserId, String dateRequest) throws SQLException { 
    	ArrayList<FTDUser> list = new ArrayList<FTDUser>();
        CallableStatement cs = null;
        ResultSet rs = null;
        try {
        	String sql = "{call pkg_apiusers.get_ftd_users_bydate(o_data => ?, i_apiuser_id => ?, i_date_string => ?)}";
        	                                    
            cs = con.prepareCall(sql);
            cs.registerOutParameter(1, OracleTypes.CURSOR);
            cs.setLong(2, apiUserId);
            cs.setString(3, dateRequest);
            
            cs.execute();
            
            rs = (ResultSet) cs.getObject(1);

            while (rs.next()) {                
            	FTDUser user = new FTDUser();
            	user.setTransactionId(rs.getLong("transaction_id"));
            	user.setAmountInUSD(rs.getLong("usd_amount"));
            	user.setUserId(rs.getLong("user_id"));            	
            	user.setDynamicParameter(rs.getString("dynamic_parameter"));
            	user.setSubAffId(rs.getLong("sub_affiliate_id"));            	
                list.add(user);
            }
        } finally {
            closeResultSet(rs);
            closeStatement(cs);
        }	
        return list;
    }
    
    /**
     * Check if affiliate key is valid for API
     * @param con
     * @param apiUserId
     * @param affKey
     * @return true if affiliate key is valid for API
     * @throws SQLException
     */
    public static boolean isAffKeyValidForApi(Connection con, long apiUserId, long affKey) throws SQLException {
    	PreparedStatement ps = null;
        ResultSet rs = null;
        try {
        	String sql =  " SELECT "
        					+ " aua.id "
        				+ " FROM "
        					+ " API_USER_AFFILIATES aua "
        				+ " WHERE "
        					+ " aua.API_USER_ID = ? "
        					+ " and aua.AFFILIATE_KEY = ?";
        	ps = con.prepareStatement(sql);
        	ps.setLong(1, apiUserId);
        	ps.setLong(2, affKey);
        	rs = ps.executeQuery();
        	if (rs.next()) {
        		return true;
        	}
        } finally {
        	closeResultSet(rs);
        	closeStatement(ps);
        }
        return false;
    }
    
    /**
     * Get service roles
     * @param con
     * @return Hashtable<String, Hashtable<Long, String>>
     * @throws SQLException
     */
    public static Hashtable<String, Hashtable<Long, String>> getServiceRoles(Connection con) throws SQLException {
    	Hashtable<String, Hashtable<Long, String>> serviceRoles = new Hashtable<String, Hashtable<Long,String>>();
    	PreparedStatement ps = null;
    	ResultSet rs = null;
    	try {
    		String sql = " SELECT "
    						+ " ausr.id, "
    						+ " aus.user_method_name, "
    						+ " aur.id as role_id, "
    						+ " aur.name as role_name "
    					+ " FROM "
    						+ " api_user_service_roles ausr, "
    						+ " api_user_services aus, "
    						+ " api_user_roles aur "
    					+ " WHERE "
    						+ " ausr.role_id = aur.id "
    						+ " and ausr.service_id = aus.id "
    					+ " ORDER BY "
    						+ " ausr.id";
    		ps = con.prepareStatement(sql);
    		rs = ps.executeQuery();
    		while (rs.next()) {
    			String serviceMethodName = rs.getString("user_method_name");
    			Hashtable<Long, String> roles = serviceRoles.get(serviceMethodName);
    			if (null == roles) {
    				roles = new Hashtable<Long, String>();
    				serviceRoles.put(serviceMethodName, roles);
    			}
    			roles.put(rs.getLong("role_id"), rs.getString("role_name"));
    		}
    	} finally {
    		closeResultSet(rs);
    		closeStatement(ps);
    	}
    	return serviceRoles;
    }
    
    /**
     * Get API Services as hashtable
     * @param con
     * @return
     * @throws SQLException
     */
    public static Hashtable<String, ApiUserService> getUserServices(Connection con) throws SQLException {
    	Hashtable<String, ApiUserService> ht = new  Hashtable<String, ApiUserService>();
    	PreparedStatement ps = null;
    	ResultSet rs = null;
    	try {
    		String sql =" SELECT " +
    					"	* " +
    					" FROM " +
    					"	api_user_services ";
    		ps = con.prepareStatement(sql);
    		rs = ps.executeQuery(); 
    		while (rs.next()) {
    			ApiUserService aus = new ApiUserService(); 
    			aus.setId(rs.getInt("ID"));
    			aus.setName(rs.getString("NAME"));
    			aus.setMethodName(rs.getString("METHOD_NAME"));
    			aus.setClassParam(rs.getString("CLASS_PARAM"));
    			aus.setUseHttps(rs.getInt("USE_HTTPS") == 1 ? true : false);
    			aus.setUserMethodName(rs.getString("USER_METHOD_NAME"));
    			aus.setJsonService(rs.getInt("JSON_SERVICE"));
    			ht.put(aus.getUserMethodName(), aus);    			    			
    		}
    	} finally {
    		closeResultSet(rs);
    		closeStatement(ps);
    	}
    	return ht;
    }
}