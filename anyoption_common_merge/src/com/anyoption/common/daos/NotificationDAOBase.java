package com.anyoption.common.daos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import com.anyoption.common.beans.Notification;

public class NotificationDAOBase extends DAOBase {

	public static void insertNotifications(Connection con, Notification notification) throws SQLException {
		PreparedStatement ps = null;
		try {
			String sql= " INSERT INTO API_NOTIFICATION " +
						"( " +
							"TIME_CREATED , " +
							"REFERENCE , " +
							"STATUS_ID , " +
							"NOTIFICATION , " +
							"TYPE_ID , " +
							"ID , " +
							"API_EXTERNAL_USER_ID " +
						") " +
						"VALUES " +
						"( " +
						  	"sysdate , " +
						  	"? , " +
						  	"? , " +
						  	"? , " +
						  	"? , " +
						  	"SEQ_API_NOTIFICATION.nextval , " +
						  	"? " +
						")";
			ps = con.prepareStatement(sql);
			int index = 1;
			ps.setString(index++, notification.getReference());
			ps.setLong(index++, notification.getStatus());
			ps.setString(index++, notification.getNotification());
			ps.setLong(index++, notification.getType());
			ps.setLong(index++, notification.getExternalUserId());
			ps.executeUpdate();
		} finally {
			closeStatement(ps);
		}

	}


}