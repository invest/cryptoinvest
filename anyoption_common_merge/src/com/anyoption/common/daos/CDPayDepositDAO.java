package com.anyoption.common.daos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.anyoption.common.bl_vos.CDPayDeposit;

/**
 * @author Lior 
 *
 */
public class CDPayDepositDAO extends DAOBase {

	/**
	 * Insert CDPay Deposit to CDPAY_DEPOSIT table.
	 * @param con
	 * @param vo
	 * @throws SQLException
	 */
	  public static void insert(Connection con, CDPayDeposit cdpayDeposit) throws SQLException {
		  PreparedStatement ps = null;
		  try {
			  String sql = "INSERT into " +
			  			   "	CDPAY_DEPOSIT " +
			  			   "		( " +
			  			   "			id, TRANSACTION_ID, CDPAY_TRANSACTION_ID, " +
			  			   "			AMOUNT, CURRENCY_SYMBOL, BANK_CODE, " +
			  			   " 			BANK_TRANSACTION_ID, STATUS, STATUS_MESSAGE, " +
			  			   "			BILLING_DESCRIPTOR, CONTROL, TIME_CREATED " +
			  			   "		) " +
			  			   "	VALUES " +
			  			   "		( " +
			  			   "			seq_cdpay_deposit.nextval, ?, ?, " +
			  			   "			?, ?, ?, " +
			  			   "			?, ?, ?, " +
			  			   "			?, ?, sysdate" +
			  			   "		) ";

			  ps = con.prepareStatement(sql);
			  ps.setLong(1, cdpayDeposit.getTransactionId());
			  ps.setLong(2, cdpayDeposit.getCdpayTransactionId());
			  ps.setLong(3, cdpayDeposit.getAmount());
			  ps.setString(4,cdpayDeposit.getCurrencySymbol());
			  ps.setString(5, cdpayDeposit.getBankcode());
			  ps.setString(6, cdpayDeposit.getBankTransactionId());
			  ps.setString(7, cdpayDeposit.getStatus());
			  ps.setString(8, cdpayDeposit.getStatusMessage());
			  ps.setString(9, cdpayDeposit.getBillingdescriptor());
			  ps.setString(10, cdpayDeposit.getControl());
			  ps.executeUpdate();
			  cdpayDeposit.setId(getSeqCurValue(con,"seq_cdpay_deposit"));
		  } finally {
			  closeStatement(ps);
		  }
	  }

	/**
	 * Get CDPay deposit details by transactionId.
	 * @param con
	 * @param id
	 * @return
	 * @throws SQLException
	 */
	public static CDPayDeposit get(Connection con,long id) throws SQLException {
		  PreparedStatement ps = null;
		  ResultSet rs = null;
		  CDPayDeposit cdpayDeposit = null;
		  try {
			  String sql = " SELECT " +
			  			   " 	* " +
			  			   " FROM " +
			  			   " 	cdpay_deposit cd  " +
			  			   " WHERE " +
			  			   " 	cd.transaction_id = ? ";
			  ps = con.prepareStatement(sql);
			  ps.setLong(1, id);
			  rs = ps.executeQuery();
			  if (rs.next()) {
				  cdpayDeposit = new CDPayDeposit();
				  cdpayDeposit.setTransactionId(rs.getLong("transaction_id"));
				  cdpayDeposit.setCdpayTransactionId(rs.getLong("cdpay_transaction_id"));
				  cdpayDeposit.setAmount(rs.getLong("amount"));
				  cdpayDeposit.setCurrencySymbol(rs.getString("currency_symbol"));
				  cdpayDeposit.setBankcode(rs.getString("bank_code"));
				  cdpayDeposit.setBankTransactionId(rs.getString("bank_transaction_id"));
				  cdpayDeposit.setStatus(rs.getString("status"));
				  cdpayDeposit.setStatusMessage(rs.getString("status_message"));
				  cdpayDeposit.setBillingdescriptor(rs.getString("billing_descriptor"));
				  cdpayDeposit.setControl(rs.getString("control"));
				  cdpayDeposit.setTimeCreated(convertToDate(rs.getTimestamp("time_created")));
			  }
		  } finally {
			  closeResultSet(rs);
			  closeStatement(ps);
		  }
		  return cdpayDeposit; 
	  }
}