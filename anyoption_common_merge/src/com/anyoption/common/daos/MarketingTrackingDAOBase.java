package com.anyoption.common.daos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import org.apache.log4j.Logger;
import org.jfree.util.Log;

import com.anyoption.common.beans.base.MarketingTracking;
import com.anyoption.common.beans.base.MarketingTrackingCookieStatic;
import com.anyoption.common.util.MarketingTrackerBase;

public class MarketingTrackingDAOBase extends DAOBase{
	
	private static final Logger logger = Logger.getLogger(MarketingTrackingDAOBase.class);
	
	public static boolean insertMarketingTracking(Connection con, ArrayList<MarketingTracking> tracking) throws SQLException{
	        
		int counter = 0;
		boolean response = false;
		PreparedStatement ps = null;
		// TODO - fix procedure to accept aff_sub
		String sql = "Begin MARKETING.INSERT_MARKETING_TRACKING(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);  End;";
		try {
			for (MarketingTracking tr : tracking) {
				ps = null;
				ps = con.prepareStatement(sql);
				ps.setString(1, tr.getmId());
				if(tr.getCombinationId() == 0){
					Log.debug("!!!CombinationId is 0 set Default:" + tr);
					ps.setLong(2, MarketingTrackerBase.DEFAULT_COMBINATION_ID);
				} else {
					ps.setLong(2, tr.getCombinationId());
				}				
				ps.setTimestamp(3, convertToTimestamp(tr.getTimeStatic()));
				ps.setString(4, tr.getHttpReferer());
				ps.setString(5, tr.getDyanmicParameter());
				ps.setString(6, tr.getUtmSource());
				ps.setLong(7, tr.getCombinationIdDynamic());
				ps.setTimestamp(8, convertToTimestamp(tr.getTimeDynamic()));
				ps.setString(9, tr.getHttpRefererDynamic());
				ps.setString(10, tr.getDyanmicParameterDynamic());
				ps.setString(11, tr.getUtmSourceDynamic());
				ps.setLong(12, tr.getContactId());
				ps.setLong(13, tr.getUserId());
				ps.setLong(14, tr.getMarketingTrackingActivityId());
				ps.setString(15, tr.getAff_sub1());
				ps.setString(16, tr.getAff_sub2());
				ps.setString(17, tr.getAff_sub3());
				counter = counter + ps.executeUpdate();
			}
		} finally {
			if (counter == tracking.size()) {
				response = true;
			} else {
				response = false;
				con.rollback();
			}
			closeStatement(ps);
		}
		return response;
	}
	
	public static String getMidByUserId(Connection con, long userId)
			throws SQLException {
		String mId = "";
		PreparedStatement stmt = null;
		ResultSet rs = null;
		try {
			String sql = "SELECT " + " MARKETING.GET_MID_BY_USER_ID(?) as MID "
					+ " FROM " + " DUAL ";
			stmt = con.prepareStatement(sql);
			stmt.setLong(1, userId);
			rs = stmt.executeQuery();
			if (rs.next()) {
				mId = rs.getString("MID");
			}
		} finally {
			closeResultSet(rs);
			closeStatement(stmt);
		}
		return mId;
	}
	
	public static String getMidByContactDetailsInsertingUser(Connection con, String email, String mobilePhone, String landLinePhone, long contactId, long userId)
	            throws SQLException {
	        String mId = "";
	        PreparedStatement stmt = null;
	        ResultSet rs = null;
	        try {
	            String sql = "SELECT " + " MARKETING.GET_MID_BY_CONTACT_DETAILS_BE(?, ?, ?, ?, ?) as MID "
	                    + " FROM " + " DUAL ";
	            stmt = con.prepareStatement(sql);
	            stmt.setString(1, email);
	            stmt.setString(2, mobilePhone);
	            stmt.setString(3, landLinePhone);
	            stmt.setLong(4, contactId);
	            stmt.setLong(5, userId);
	            rs = stmt.executeQuery();
	            if (rs.next()) {
	                mId = rs.getString("MID");
	            }
	            
	            if (mId.contains("none")){
	                mId = null;
	            }
	        } finally {
	            closeResultSet(rs);
	            closeStatement(stmt);
	        }
	        return mId;
	    }
	
	   public static String getMidByContactDetails(Connection con, String email, String phone)
               throws SQLException {
           String mId = null;
           PreparedStatement stmt = null;
           ResultSet rs = null;
           try {
               String sql = "SELECT " + " MARKETING.GET_MID_BY_CONTACT_DETAILS_EP(?, ?) as MID "
                       + " FROM " + " DUAL ";
               stmt = con.prepareStatement(sql);
               stmt.setString(1, email);
               stmt.setString(2, phone);
               rs = stmt.executeQuery();
               if (rs.next()) {
                   mId = rs.getString("MID");
               }               
               if (mId.contains("none")){
                   mId = null; 
               }
               
           } finally {
               closeResultSet(rs);
               closeStatement(stmt);
           }
           return mId;
       }
	
	   public static String getMidByContactDetailsJson(Connection con, String email, String mobilePhone, String landLinePhone, long contactId, long userId)
               throws SQLException {
           String mId = "";
           PreparedStatement stmt = null;
           ResultSet rs = null;
           try {
               String sql = "SELECT " + " MARKETING.GET_MID_BY_CONTACT_DETAILS(?, ?, ?, ?, ?) as MID "
                       + " FROM " + " DUAL ";
               stmt = con.prepareStatement(sql);
               stmt.setString(1, email);
               stmt.setString(2, mobilePhone);
               stmt.setString(3, landLinePhone);
               stmt.setLong(4, contactId);
               stmt.setLong(5, userId);
               rs = stmt.executeQuery();
               if (rs.next()) {
                   mId = rs.getString("MID");
               }
           } finally {
               closeResultSet(rs);
               closeStatement(stmt);
           }
           return mId;
       }
	
	public static Long getUserIdByMid(Connection con, String mid)
			throws SQLException {
		long userId = 0;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		try {
			String sql = "SELECT " +
							" distinct mt.USER_ID as USER_ID " + 
						  " FROM " +
							" marketing_tracking mt, marketing_tracking_static ms " + 
							" WHERE " + 
							" mt.marketing_tracking_static_id = ms.id  " +
							" and mid = ? " +
							" and USER_ID>0";
			stmt = con.prepareStatement(sql);
			stmt.setString(1, mid);
			rs = stmt.executeQuery();
			if (rs.next()) {
				userId = rs.getLong("USER_ID");
			}
		} finally {
			closeResultSet(rs);
			closeStatement(stmt);
		}
		return userId;
	}
	
	public static Long getContactIdByMid(Connection con, String mid)
			throws SQLException {
		long contactId = 0;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		try {
			String sql = "SELECT " +
					" distinct mt.CONTACT_ID as CONTACT_ID " + 
				  " FROM " +
					" marketing_tracking mt, marketing_tracking_static ms " + 
					" WHERE " + 
					" mt.marketing_tracking_static_id = ms.id  " +
					" and mid = ? " +
					" and CONTACT_ID>0";
			stmt = con.prepareStatement(sql);
			stmt.setString(1, mid);
			rs = stmt.executeQuery();
			if (rs.next()) {
				contactId = rs.getLong("CONTACT_ID");
			}
		} finally {
			closeResultSet(rs);
			closeStatement(stmt);
		}
		return contactId;
	}
	
	public static boolean isHaveActivity (Connection con, String mid) throws SQLException{
		boolean result = false;
		long activity = 0;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		try {
			String sql = "SELECT " + " MARKETING.GET_ACTIVITY_BY_MID(?) as ACTIVITY "
					+ " FROM " + " DUAL ";
			stmt = con.prepareStatement(sql);
			stmt.setString(1, mid);
			rs = stmt.executeQuery();
			if (rs.next()) {
				activity = rs.getLong("ACTIVITY");
			}
			if(activity > 0){
				result = true;
			}
			
		} finally {
			closeResultSet(rs);
			closeStatement(stmt);
		}
		
		return result;
	}
	
	public static MarketingTrackingCookieStatic getMarketingTrackingCookieStatic(Connection con, String mId) throws SQLException {
	   	  
		PreparedStatement ps = null;
		  ResultSet rs = null;
		  MarketingTrackingCookieStatic mtc = new MarketingTrackingCookieStatic();

		  try {
			    String sql = "SELECT " +
			    				" * " +
			    			 "FROM " +
			    			 	"marketing_tracking_static mts " +
			    			 "WHERE mts.mid = ? ";

				ps = con.prepareStatement(sql);
				ps.setString(1, mId);

				rs = ps.executeQuery();

				if (rs.next()) {
					mtc.setMs(rs.getString("mid"));
					mtc.setCs(rs.getString("combination_id"));
					mtc.setTs(rs.getTimestamp("time_static"));
					mtc.setHttp(rs.getString("http_referer"));
					mtc.setDp(rs.getString("dynamic_param"));
					mtc.setAff_sub1(rs.getString("aff_sub1"));
					mtc.setAff_sub2(rs.getString("aff_sub2"));
					mtc.setAff_sub3(rs.getString("aff_sub3"));
					mtc.setUtmSource(rs.getString("utm_source"));
				}
			}
			finally {
				closeResultSet(rs);
				closeStatement(ps);
			}
			return mtc;
	}
	
	   public static void updateContactMid(Connection con, long contactId, String mId) throws SQLException{
	       PreparedStatement ps = null;
	        try {
	            String sql =
	                    "UPDATE " +
	                        "contacts " +
	                    "SET " +
	                        "MID = ? " +
	                    "WHERE " +
	                        "ID = ?";

	            ps = con.prepareStatement(sql);
	            ps.setString(1, mId);
	            ps.setLong(2, contactId);
	            ps.executeUpdate();
	            logger.debug("Contact ID:" + contactId + " with MID:" + mId);
	        } finally {
	            closeStatement(ps);
	        }
	    }
	   
       public static void updateUserMid(Connection con, long userId, String mId) throws SQLException{
           PreparedStatement ps = null;
            try {
                String sql =
                        "UPDATE " +
                            "users " +
                        "SET " +
                            "MID = ? " +
                        "WHERE " +
                            "ID = ?";

                ps = con.prepareStatement(sql);
                ps.setString(1, mId);
                ps.setLong(2, userId);
                ps.executeUpdate();
                logger.debug("Contact ID:" + userId + " with MID:" + mId);
            } finally {
                closeStatement(ps);
            }
        }
	   
       public static String getEtsMidByContactDetails(Connection con, String email, String phone)
               throws SQLException {
           String mId = null;
           PreparedStatement stmt = null;
           ResultSet rs = null;
           try {
               String sql = "SELECT " + " MARKETING.GET_ETS_MID_BY_CONTACT_DETAILS(?, ?) as MID "
                       + " FROM " + " DUAL ";
               stmt = con.prepareStatement(sql);
               stmt.setString(1, email);
               stmt.setString(2, phone);
               rs = stmt.executeQuery();
               if (rs.next()) {
                   mId = rs.getString("MID");
               }               
               if (mId.contains("none")){
                   mId = null; 
               }
               
           } finally {
               closeResultSet(rs);
               closeStatement(stmt);
           }
           return mId;
       }
       
       public static String getEtsMidByContactDetails(Connection con, String email, String mobilePhone, String landLinePhone, long contactId, long userId)
               throws SQLException {
           String mId = "";
           PreparedStatement stmt = null;
           ResultSet rs = null;
           try {
               String sql = "SELECT " + " MARKETING.GET_ETS_MID_CNTCT_DETAILS_BE(?, ?, ?, ?, ?) as MID "
                       + " FROM " + " DUAL ";
               stmt = con.prepareStatement(sql);
               stmt.setString(1, email);
               stmt.setString(2, mobilePhone);
               stmt.setString(3, landLinePhone);
               stmt.setLong(4, contactId);
               stmt.setLong(5, userId);
               rs = stmt.executeQuery();
               if (rs.next()) {
                   mId = rs.getString("MID");
               }
               
               if (mId.contains("none")){
                   mId = null;
               }
           } finally {
               closeResultSet(rs);
               closeStatement(stmt);
           }
           return mId;
       }
              
       public static ArrayList<String> getEtsUsersFirstDeposit(Connection con) throws SQLException {
           String mId = "";
           PreparedStatement stmt = null;
           ResultSet rs = null;
           ArrayList<String> usersFirstDeposit = new ArrayList<String>();
           try {
               String sql = "select fd.user_id||','|| nvl(u.mid,'x') " +
               		        " from " + 
                               " marketing_tracking_ets_fd fd, " +
                               " users u " +
                            " where " + 
                               " fd.user_id=u.id(+) " + 
                               " and send_to_ets = 0 ";
               stmt = con.prepareStatement(sql);
               rs = stmt.executeQuery();
               while (rs.next()) {
                   usersFirstDeposit.add(rs.getString(1));
               }
               
               if (mId.contains("none")){
                   mId = null;
               }
           } finally {
               closeResultSet(rs);
               closeStatement(stmt);
           }
           return usersFirstDeposit;
       }
       
       public static void updateEtsUserFDState(Connection con, ArrayList<String> users) throws SQLException{
           PreparedStatement ps = null;
           String usersForUpdte="";
            try {
                if (users.size() > 0){
                    for (String s : users){
                        String[] parts = s.split(",");
                        usersForUpdte += parts[0] + ",";
                    }
                    usersForUpdte += "0";
                }
                
                String sql =
                        "UPDATE " +
                            "marketing_tracking_ets_fd " +
                        "SET " +
                            "SEND_TO_ETS = 1 ," +
                            "SEND_TIME = sysdate " +
                        "WHERE " +
                            "USER_ID in (" + usersForUpdte + ")";

                ps = con.prepareStatement(sql);
                ps.executeUpdate();
                logger.debug("ETS updateEtsUserFDState:" + usersForUpdte);
            } finally {
                closeStatement(ps);
            }
        } 
}
