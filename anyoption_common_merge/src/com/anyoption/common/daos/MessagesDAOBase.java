package com.anyoption.common.daos;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import com.anyoption.common.beans.base.Message;

/**
 * Common messages DAO.
 *
 * @author Tony
 */
public class MessagesDAOBase extends DAOBase {
    /**
     * Fill <code>Message</code> object with data from the current record.
     *
     * @param rs
     * @return
     * @throws SQLException
     */
    protected static Message getMessage(ResultSet rs) throws SQLException {
        Message m = new Message();
        m.setId(rs.getLong("id"));
        m.setText(rs.getString("text"));
        m.setWebScreen(rs.getLong("web_screen"));
        m.setStartEffDate(new Date(rs.getTimestamp("start_eff_date").getTime()));
        m.setEndEffDate(new Date(rs.getTimestamp("end_eff_date").getTime()));
        m.setSkinId(rs.getLong("skin_id"));
        m.setLanguageId(rs.getLong("language_id"));
        return m;
    }
}