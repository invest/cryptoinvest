package com.anyoption.common.daos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;

import org.apache.log4j.Logger;

/**
 * MarketingDictionaryParamsDAO
 * @author eyalo
 */
public class MarketingDictionaryParamsDAO extends DAOBase {
	private static final Logger logger = Logger.getLogger(MarketingDictionaryParamsDAO.class);
	
	public static HashMap<Long, HashMap<String, String>> getMarketingDictionaryParams(Connection con) throws SQLException {
		logger.debug("Going to get MarketingDictionaryParams." );
		HashMap<Long, HashMap<String, String>> MarketingDictionaryParamsHM = new HashMap<Long, HashMap<String,String>>();
		HashMap<String, String> translateParamsHM;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			String sql = "SELECT " +
							" mdp.* " +
						" FROM " +
							" MARKETING_DICTIONARY_PARAMS mdp";
			ps = con.prepareStatement(sql);
			rs = ps.executeQuery();
			while (rs.next()) {
				long marketingCompanyId = rs.getLong("marketing_company_id");
				translateParamsHM = MarketingDictionaryParamsHM.get(marketingCompanyId);
				if (null == translateParamsHM) {
					translateParamsHM = new HashMap<String, String>();
					MarketingDictionaryParamsHM.put(marketingCompanyId, translateParamsHM);
				}
				translateParamsHM.put(rs.getString("original_param"), rs.getString("translated_param"));
			}
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		logger.debug("End to get MarketingDictionaryParams." );
		return MarketingDictionaryParamsHM;
	}
}
