package com.anyoption.common.daos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.anyoption.common.beans.base.Message;

/**
 * Messages web DAO.
 *
 * @author Tony
 */
public class MessagesDAO extends MessagesDAOBase {
    /**
     * Load all current messages from the db.
     *
     * @param conn
     * @return
     * @throws SQLException
     */
    public static ArrayList<Message> getMessages(Connection conn, long skinId, String languageCode) throws SQLException {
        ArrayList<Message> l = new ArrayList<Message>();
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            String sql =
                    "SELECT " +
                        "* " +
                    "FROM " +
                        "messages " +
                    "WHERE " +
                        "end_eff_date > sysdate and " +
                        "skin_id = ? and " +
                        "language_id = (select id from languages where code = ?) ";

            pstmt = conn.prepareStatement(sql);
            pstmt.setLong(1, skinId);
            pstmt.setString(2, languageCode);
            rs = pstmt.executeQuery();
            while (rs.next()) {
                l.add(getMessage(rs));
            }
        } finally {
            closeResultSet(rs);
            closeStatement(pstmt);
        }
        return l;
    }

	public static ArrayList<Message> getAllMessages(Connection conn) throws SQLException {
		ArrayList<Message> l = new ArrayList<Message>();
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            String sql =
                    "SELECT " +
                        "* " +
                    "FROM " +
                        "messages " +
                    "WHERE " +
                        "end_eff_date > sysdate ";

            pstmt = conn.prepareStatement(sql);
            rs = pstmt.executeQuery();
            while (rs.next()) {
                l.add(getMessage(rs));
            }
        } finally {
            closeResultSet(rs);
            closeStatement(pstmt);
        }
        return l;
	}
}