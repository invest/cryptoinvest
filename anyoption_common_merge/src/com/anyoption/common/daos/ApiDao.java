//package com.anyoption.common.daos;
//
//import java.sql.Connection;
//import java.sql.PreparedStatement;
//import java.sql.ResultSet;
//import java.sql.SQLException;
//import java.util.ArrayList;
//import java.util.Hashtable;
//import java.util.UUID;
//
//import javax.faces.model.SelectItem;
//
//import org.bouncycastle.crypto.CryptoException;
//
//import com.anyoption.common.beans.ApiUser;
//import com.anyoption.common.beans.base.ApiErrorCode;
//import com.anyoption.common.beans.base.ApiPage;
//import com.anyoption.common.beans.base.ApiUserActivity;
//import com.anyoption.common.util.Encryptor;
//
///**
// * @author EranL
// *
// */
//public class ApiDao extends DAOBase {
//
//	/**
//	 * Get API user
//	 * @param con
//	 * @param userName
//	 * @return
//	 * @throws SQLException
//	 */
//	public static void getAPIUser(Connection con, String userName, String password, ApiUser apiUser) throws SQLException {
//		PreparedStatement ps = null;
//		ResultSet rs = null;
//		try {
//			String sql =" SELECT " +
//						"	*	" +
//	                    " FROM " +
//	                    "	api_users au " +
//	                    " WHERE " +
//	                    "	au.user_name = ? " +
//	                    "	AND au.password = ? ";
//	        ps = con.prepareStatement(sql);
//	        ps.setString(1, userName);
//	        ps.setString(2, password);
//	        rs = ps.executeQuery();
//	        if (rs.next()) {
//	        	apiUser.setId(rs.getLong("ID"));
//	        	apiUser.setUserName(rs.getString("USER_NAME"));
//	        	apiUser.setPassword(rs.getString("PASSWORD"));
//	        	apiUser.setTimeCreated(convertToDate(rs.getTimestamp("TIME_CREATED")));
//	        	apiUser.setType(rs.getInt("TYPE"));
//	        	apiUser.setAffiliateCode(rs.getLong("AFFILIATE_CODE"));
//	        	apiUser.setActive(rs.getInt("IS_ACTIVE") == 1 ? true : false);
//	        }
//		} finally {
//			closeResultSet(rs);
//	        closeStatement(ps);
//		}
//	}
//
//
//	/**
//	 * Get API error codes
//	 * @param con
//	 * @return
//	 * @throws SQLException
//	 */
//	public static Hashtable<String, ApiErrorCode> getAPIErrorCodes(Connection con) throws SQLException {
//		PreparedStatement ps = null;
//		ResultSet rs = null;
//		Hashtable<String, ApiErrorCode> list = new Hashtable<String, ApiErrorCode>();
//		try {
//			String sql =" SELECT " +
//						"	*	" +
//	                    " FROM " +
//	                    "	api_error_codes ";
//	        ps = con.prepareStatement(sql);
//	        rs = ps.executeQuery();
//	        while (rs.next()) {
//	        	ApiErrorCode vo = new ApiErrorCode();
//	        	vo.setId(rs.getLong("ID"));
//	        	vo.setTypeId(rs.getLong("TYPE_ID"));
//	        	vo.setCode(rs.getString("CODE"));
//	        	vo.setDescription(rs.getString("DESCRIPTION"));
//	        	vo.setDisplayName(rs.getString("DISPLAY_NAME"));
//	        	list.put(vo.getCode(), vo);
//	        }
//		} finally {
//			closeResultSet(rs);
//	        closeStatement(ps);
//		}
//		return list;
//	}
//
//
//	/**
//	 * Insert into API User Activity
//	 * 1. insert user
//	 * 2. insert contact
//	 * 3. insert investment
//	 * @param con
//	 * @return
//	 * @throws SQLException
//	 */
//	public static void insertAPIUserActivity(Connection con, ApiUserActivity apiUserActivity) throws SQLException {
//		PreparedStatement ps = null;
//		try {
//			String sql= " INSERT " +
//						" INTO api_user_activity(id, " +
//						"						 type_id, " +
//						"						 key_value," +
//						"						 api_user_id," +
//						"						 time_created) " +
//						" VALUES(seq_api_user_activity.NEXTVAL, ?, ?, ?, SYSDATE)";
//			ps = con.prepareStatement(sql);
//			ps.setInt(1, apiUserActivity.getTypeId());
//			ps.setLong(2, apiUserActivity.getKeyValue());
//			ps.setLong(3, apiUserActivity.getApiUserId());
//			ps.executeUpdate();
//		} finally {
//			closeStatement(ps);
//		}
//	}
//
//	/**
//	 * Insert user id into Login token
//	 * @param con
//	 * @return
//	 * @throws SQLException
//	 */
//	public static String insertLoginToken(Connection con, long userId) throws SQLException {
//		PreparedStatement ps = null;
//		String uuidStr = null;
//		try {
//			String sql= " INSERT " +
//						" INTO api_login_token(id, " +
//						"					   user_id, " +
//						"					   time_created, " +
//						"					   uuid) " +
//						" VALUES(seq_api_login_token.NEXTVAL, ?, SYSDATE, ?)";
//			ps = con.prepareStatement(sql);
//			ps.setLong(1, userId);
//			UUID uuid = UUID.randomUUID();
//			uuidStr = uuid.toString();
//			ps.setString(2, uuidStr);
//			ps.executeUpdate();
//		} finally {
//			closeStatement(ps);
//		}
//		return uuidStr;
//	}
//
//	/**
//	 * Get API pages - for login procedure
//	 * @param con
//	 * @return
//	 * @throws SQLException
//	 */
//	public static Hashtable<Long, ApiPage> getApiPages(Connection con) throws SQLException {
//		PreparedStatement ps = null;
//		ResultSet rs = null;
//		Hashtable<Long, ApiPage> list = new Hashtable<Long, ApiPage>();
//		try {
//			String sql =" SELECT " +
//						"	*	" +
//	                    " FROM " +
//	                    "	api_pages ";
//	        ps = con.prepareStatement(sql);
//	        rs = ps.executeQuery();
//	        while (rs.next()) {
//	        	ApiPage vo = new ApiPage();
//	        	vo.setId(rs.getLong("ID"));
//	        	vo.setUrl(rs.getString("URL"));
//	        	vo.setDescription(rs.getString("DESCRIPTION"));
//	        	vo.setCanManualRedirect(rs.getInt("CAN_MANUAL_REDIRECT") == 0 ? false : true);
//	        	list.put(vo.getId(), vo);
//	        }
//		} finally {
//			closeResultSet(rs);
//	        closeStatement(ps);
//		}
//		return list;
//	}
//
//	/**
//	 * Get API user
//	 * @param con
//	 * @param userName
//	 * @return
//	 * @throws SQLException
//	 * @throws CryptoException
//	 */
//	public static String validateAPIUser(Connection con, String userName, String password) throws SQLException, CryptoException {
//		PreparedStatement ps = null;
//		ResultSet rs = null;
//		try {
//			String sql =" SELECT " +
//						"	password	" +
//	                    " FROM " +
//	                    "	api_users au " +
//	                    " WHERE " +
//	                    "	au.user_name = ? " +
//	                    "	AND au.is_active = 1 ";
//	        ps = con.prepareStatement(sql);
//	        ps.setString(1, userName);
//
//	        rs = ps.executeQuery();
//	        if (rs.next()) {
//	        	return rs.getString("password");
//	        }
//		} finally {
//			closeResultSet(rs);
//	        closeStatement(ps);
//		}
//		return null;
//	}
//
//	/**
//	 * Get API user
//	 * @param con
//	 * @param userName
//	 * @return
//	 * @throws SQLException
//	 * @throws CryptoException
//	 */
//	public static ArrayList<SelectItem> getApiUsersTypeExternalSI(Connection con) throws SQLException {
//		ArrayList<SelectItem> list = new ArrayList<SelectItem>();
//		PreparedStatement ps = null;
//		ResultSet rs = null;
//		try {
//			String sql = " SELECT " +
//						 	" au.id, " +
//						    " au.user_name " +
//						 " FROM " +
//						  	" api_users au " +
//						 " WHERE " +
//						 	" au.type = " + ApiUser.API_USER_TYPE_EXTERNAL + " " +
//						 " ORDER BY " +
//							" user_name";
//	        ps = con.prepareStatement(sql);
//
//	        rs = ps.executeQuery();
//	        while (rs.next()) {
//	        	SelectItem user = new SelectItem(new Long(rs.getLong("id")), rs.getString("user_name"));
//	        	list.add(user);
//	        }
//		} finally {
//			closeResultSet(rs);
//	        closeStatement(ps);
//		}
//		return list;
//	}
//}
