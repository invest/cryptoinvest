package com.anyoption.common.daos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.Bonus;
import com.anyoption.common.beans.MailBoxTemplate;
import com.anyoption.common.beans.MailBoxUser;
import com.anyoption.common.beans.Writer;
import com.anyoption.common.beans.base.BonusUsers;
import com.anyoption.common.beans.base.BonusUsersStep;
import com.anyoption.common.beans.base.Currency;
import com.anyoption.common.beans.base.Skin;
import com.anyoption.common.bl_vos.BonusCurrency;
import com.anyoption.common.bl_vos.BonusPopulationLimit;
import com.anyoption.common.bonus.BonusHandlerBase;
import com.anyoption.common.bonus.BonusHandlerFactory;
import com.anyoption.common.bonus.BonusHandlersException;
import com.anyoption.common.managers.CurrenciesManagerBase;
import com.anyoption.common.util.BonusFormulaDetails;
import com.anyoption.common.util.CommonUtil;
import com.anyoption.common.util.ConstantsBase;

/**
 * BonusDAOBase
 * @author eyalo
 */
public class BonusDAOBase extends DAOBase {
	public static final Logger log = Logger.getLogger(BonusDAOBase.class);
	
	/**
	 * Fill Bonus object
	 * @param rs
	 * 		ResultSet
	 * @return
	 * 		Bonus object
	 * @throws SQLException
	 */
	protected static Bonus getVO(ResultSet rs) throws SQLException {
		Bonus vo = new Bonus();
		vo.setId(rs.getLong("ID"));
		vo.setStartDate(convertToDate(rs.getTimestamp("START_DATE")));
		vo.setEndDate(convertToDate(rs.getTimestamp("END_DATE")));
		vo.setWageringParameter(rs.getLong("WAGERING_PARAMETER"));
		vo.setDefaultPeriod(rs.getLong("DEFAULT_PERIOD"));
		vo.setAutoRenewInd(rs.getLong("AUTOMATIC_RENEW_IND"));
		vo.setRenewFrequency(rs.getLong("RENEW_FREQUENCY"));
		vo.setTypeId(rs.getLong("TYPE_ID"));
		vo.setWriterId(rs.getLong("WRITER_ID"));
		vo.setTimeCreated(convertToDate(rs.getTimestamp("TIME_CREATED")));
		vo.setNumberOfActions(rs.getLong("NUMBER_OF_ACTIONS"));
		vo.setName(rs.getString("name"));
		vo.setHasSteps(rs.getInt("HAS_STEPS")==1?true:false);
		vo.setOddsWin(rs.getDouble("odds_win"));
		vo.setOddsLose(rs.getDouble("odds_lose"));
		vo.setTurnoverParam(rs.getLong("turnover_param"));
		vo.setRoundUpType(rs.getInt("round_up_type"));
		return vo;
	}
	

	/**
	 * Fill BonusUsers object
	 * @param rs
	 * 		ResultSet
	 * @return
	 * 		Bonus object
	 * @throws SQLException
	 */
	public static BonusUsers getBonusUsersVO(ResultSet rs, Connection conn) throws SQLException {
		BonusUsers vo = new BonusUsers();
		vo.setId(rs.getLong("id"));
		vo.setUserId(rs.getLong("user_id"));
		vo.setStartDate(convertToDate(rs.getTimestamp("start_date")));
		vo.setEndDate(convertToDate(rs.getTimestamp("end_date")));
		vo.setBonusStateId(rs.getLong("bonus_state_id"));
		vo.setBonusAmount(rs.getLong("bonus_amount"));
		vo.setBonusPercent(rs.getDouble("bonus_percent"));
		vo.setMinDepositAmount(rs.getLong("min_deposit_amount"));
		vo.setMaxDepositAmount(rs.getLong("max_deposit_amount"));
		vo.setSumInvQualify(rs.getLong("sum_invest_qualify"));
		vo.setWriterId(rs.getLong("writer_id"));
		vo.setTimeCreated(convertToDate(rs.getTimestamp("time_created")));
		vo.setSumInvQualifyReached(rs.getLong("sum_invest_qualify_reached"));
		vo.setSumInvWithdrawal(rs.getLong("sum_invest_withdraw"));
		vo.setSumInvWithdrawalReached(rs.getLong("sum_invest_withdraw_reached"));
		vo.setTimeActivated(convertToDate(rs.getTimestamp("time_activated")));
		vo.setTimeUsed(convertToDate(rs.getTimestamp("time_used")));
		vo.setTimeDone(convertToDate(rs.getTimestamp("time_done")));
		vo.setTypeId(rs.getLong("type_id"));
		vo.setWageringParam(rs.getLong("wagering_parameter"));
		vo.setNumOfActions(rs.getLong("number_of_actions"));
		vo.setNumOfActionsReached(rs.getLong("number_of_actions_reached"));
		vo.setOddsWin(rs.getDouble("odds_win"));
		vo.setOddsLose(rs.getDouble("odds_lose"));
		vo.setMinInvestAmount(rs.getLong("min_invest_amount"));
		vo.setMaxInvestAmount(rs.getLong("max_invest_amount"));
		vo.setSumDeposits(rs.getLong("sum_deposits"));
		vo.setSumDepositsReached(rs.getLong("sum_deposits_reached"));
		vo.setActivatedTransactionId(rs.getLong("ACTIVATED_TRANSACTION_ID"));
		vo.setActivatedInvestmentId(rs.getLong("ACTIVATED_INVESTMENT_ID"));
		vo.setWageringEendInvestmentId(rs.getLong("WAGERING_END_INVESTMENT_ID"));

		long bonusId = rs.getLong("bonus_id");
		vo.setBonusId(bonusId);
		vo.setHasSteps(getBonusById(conn, bonusId).isHasSteps());
		vo.setAdjustedAmount(rs.getLong("adjusted_amount"));
		vo.setTimeWageringWaived(convertToDate(rs.getTimestamp("time_wagering_waived")));
		vo.setTimeWithdrawn(convertToDate(rs.getTimestamp("time_withdrawn")));
		vo.setComments(rs.getString("comments"));
		return vo;
	}
	
    /**
	 * Get bonus by id
	 * @param con db connection
	 * @param id bonus id for search
	 * @return
	 * @throws SQLException
	 */
	public static Bonus getBonusById(Connection con, long id) throws SQLException {
		Bonus b = null;
		PreparedStatement ps = null;
		ResultSet rs = null;

		try	{
		    String sql = " SELECT " +
		    				" b.*, " +
		    				" bt.class_type_id, " +
		    				" bt.condition," +
		    				" bt.is_check_limit " +
		    			 " FROM " +
		    			 	" bonus b, " +
		    			 	" bonus_types bt " +
		    			 " WHERE " +
		    			 	" b.id = ? " +
		    			 	" AND b.type_id = bt.id";

			ps = con.prepareStatement(sql);
			ps.setLong(1, id);

			rs = ps.executeQuery();

			if ( rs.next() ) {
				b = getVO(rs);
				b.setClassType(rs.getLong("class_type_id"));
				b.setCondition(rs.getString("condition"));
				b.setCheckLimit(rs.getInt("is_check_limit")==1?true:false);
			}
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		return b;
	}
	
    /**
     * Accept bonus user
     * @param con
     * @param bonusUserId
     * @param stateId
     * @throws SQLException
     */
    public static boolean acceptBonusUser(Connection con, long bonusUserId, long stateId) throws SQLException {
    	PreparedStatement ps = null;
    	boolean result = false;
    	int index = 1;
    	try {
    		String sql = 
        			" UPDATE " +
        				" bonus_users " +
        		    " SET " +
        		    	" bonus_state_id = ?, " +
        		    	" time_activated = DECODE(?, ?, sysdate, time_activated), " +
        		    	" time_done = DECODE(?, ?, sysdate, time_done) " +
                    " WHERE " +
                    	" id = ? ";
    		ps = con.prepareStatement(sql);
    		ps.setLong(index++, stateId);
    		ps.setLong(index++, stateId);
    		ps.setLong(index++, ConstantsBase.BONUS_STATE_ACTIVE);
    		ps.setLong(index++, stateId);
    		ps.setLong(index++, ConstantsBase.BONUS_STATE_DONE);
    		ps.setLong(index++, bonusUserId);
    		result = ps.executeUpdate() > 0 ? true : false;
    	} finally {
    		closeStatement(ps);
    	}
    	return result;
    }
    
    public static int useNextInvestOnUs(Connection conn, long bonusUsersId, long amount) throws SQLException {
        int updatedCount = 0;
        PreparedStatement pstmt = null;
        try {
            String sql =
                    "UPDATE " +
                        "bonus_users " +
                    "SET " +
                        "bonus_state_id = 3, " +
                        "time_used = current_date, " +
                        "sum_invest_withdraw = wagering_parameter * ? " +
                    "WHERE " +
                        "id = ?";
            pstmt = conn.prepareStatement(sql);
            pstmt.setLong(1, amount);
            pstmt.setLong(2, bonusUsersId);
            updatedCount = pstmt.executeUpdate();
        } finally {
            closeStatement(pstmt);
        }
        return updatedCount;
    }

    /**
     * Check if bonus on registration is available
     * @param con db connection
     * @param bonusId the bonus id on registration
     * @return
     * @throws SQLException
     */
    public static boolean isBonusUponRegistrationAvailable(Connection con, long bonusId) throws SQLException {
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            String sql =
            	"SELECT * " +
            	"FROM " +
            		"bonus " +
            	"WHERE " +
            		"id = ? AND " +
            		"sysdate >= start_date AND " +
            		"sysdate <= end_date ";
            ps = con.prepareStatement(sql);
            ps.setLong(1, bonusId);
            rs = ps.executeQuery();
            if (rs.next()) {
            	return true;
            }
        } finally {
        	closeResultSet(rs);
        	closeStatement(ps);
        }
        return false;
    }
    
	/**
	 * Insert Statement to bonus_investments table
	 * @param conn
	 * @param investmentId
	 * @param bonusUsersId
	 * @param bonusAmount
	 * @param adjustedAmountHistory
	 * @param typeId
	 * @throws SQLException
	 */
	public static void insertBonusInvestments(Connection conn, long investmentId, long bonusUsersId, long bonusAmount, long adjustedAmountHistory, int typeId) throws SQLException {
		PreparedStatement ps = null;
		int ind = 1;
		try {
			String sql =
						"INSERT INTO bonus_investments " +
						"	( " +
						"		id, " +
						"		investments_id, " +
						"		bonus_users_id, " +
						"		bonus_amount, " +
						"		time_created, " +
						"		adjusted_amount, " +
						"		type_id " +
						" 	) " +
						"VALUES	" +
						"	( " +
						"		SEQ_BONUS_INVESTMENTS.nextval, " +
						"		?, " +
						"		?, " +
						"		?, " +
						"		sysdate, " +
						"		?, " +
						"		? " +
						"	) ";

			ps = conn.prepareStatement(sql);
			ps.setLong(ind++, investmentId);
			ps.setLong(ind++, bonusUsersId);
			ps.setLong(ind++, bonusAmount);
			ps.setLong(ind++, adjustedAmountHistory);
			ps.setLong(ind++, typeId);
			ps.executeUpdate();
			log.debug("\nBMS, insertBonusInvestments, investmentId:" + investmentId + ", bonusUsersId:" + bonusUsersId + ", bonusAmount:" + bonusAmount +
					", adjustedAmount:" + adjustedAmountHistory + ", typeId:" + typeId);
		} finally {
			closeStatement(ps);
		}
	}
	
	/**
	 * insert bonus to user
	 * @param con db connection
	 * @param popEntryId TODO
	 * @throws SQLException
	 */
	public static boolean insertBonusUser(Connection con, BonusUsers bu, Bonus b,BonusCurrency bc, long popEntryId, boolean isNeedToSendInternalMail) throws SQLException {
		PreparedStatement ps = null;
		try {
			String sql="INSERT " +
						"INTO bonus_users(ID, " +
										  "USER_ID," +
										  "START_DATE," +
										  "END_DATE," +
										  "BONUS_STATE_ID," +
										  "BONUS_AMOUNT," +
										  "BONUS_PERCENT," +
										  "MIN_DEPOSIT_AMOUNT," +
										  "MAX_DEPOSIT_AMOUNT," +
										  "SUM_INVEST_QUALIFY," +
										  "WRITER_ID," +
										  "TIME_CREATED," +
										  "SUM_INVEST_QUALIFY_REACHED," +
										  "SUM_INVEST_WITHDRAW," +
										  "SUM_INVEST_WITHDRAW_REACHED," +
										  "TIME_ACTIVATED," +
										  "TIME_USED," +
										  "TIME_DONE," +
										  "TYPE_ID," +
										  "WAGERING_PARAMETER," +
										  "NUMBER_OF_ACTIONS," +
										  "NUMBER_OF_ACTIONS_REACHED," +
										  "BONUS_ID, " +
										  "min_invest_amount, " +
										  "max_invest_amount, " +
										  "odds_win, " +
										  "odds_lose," +
										  "SUM_DEPOSITS," +
										  "POPULATION_ENTRY_ID," +
										  "TURNOVER_PARAM," +
										  "ADJUSTED_AMOUNT, " +
										  "COMMENTS) " +
						"VALUES(SEQ_BONUS_USERS.nextval,?,sysdate,?,?,?,?,?,?,?," +
								"?,sysdate,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
	
			// calculate end date
			Calendar cal = Calendar.getInstance();
			cal.set(Calendar.HOUR_OF_DAY, 21);
	        cal.set(Calendar.MINUTE, 0);
	        cal.set(Calendar.SECOND, 0);
	
			if (bu.getBonusId() != ConstantsBase.XMASS_BONUS_ID ){ // not xmass bonus
				cal.add(Calendar.DAY_OF_MONTH, ((int)b.getDefaultPeriod()));
			}else{ // xmass bonus
	            cal.set(GregorianCalendar.DAY_OF_MONTH, 31);
			}
	
			ps = con.prepareStatement(sql);
			ps.setLong(1, bu.getUserId());
			ps.setTimestamp(2, CommonUtil.convertToTimeStamp(cal.getTime()));
			ps.setLong(3, bu.getBonusStateId());
			ps.setLong(4, bc.getBonusAmount());
			ps.setDouble(5, bc.getBonusPercent());
			ps.setLong(6, bc.getMinDepositAmount());
			ps.setLong(7, bc.getMaxDepositAmount());
			ps.setLong(8, bc.getSumInvestQualify());
			ps.setLong(9, bu.getWriterId());
			ps.setLong(10, bu.getSumInvQualifyReached());
			ps.setLong(11, bu.getSumInvWithdrawal());
			ps.setLong(12, bu.getSumInvWithdrawalReached());
			ps.setTimestamp(13, CommonUtil.convertToTimeStamp(bu.getTimeActivated()));
			ps.setTimestamp(14, null);
			ps.setTimestamp(15, CommonUtil.convertToTimeStamp(bu.getTimeDone()));
			ps.setLong(16, b.getTypeId());
			ps.setLong(17, b.getWageringParameter());
			ps.setLong(18, b.getNumberOfActions());
			ps.setLong(19, bu.getNumOfActionsReached());
			ps.setLong(20, bu.getBonusId());
			ps.setLong(21, bc.getMinInvestAmount());
			ps.setLong(22, bc.getMaxInvestAmount());
			ps.setDouble(23, b.getOddsWin());
			ps.setDouble(24, b.getOddsLose());
			ps.setLong(25, bu.getSumDeposits());
			if (popEntryId > 0){
				ps.setLong(26, popEntryId);
			}else{
				ps.setString(26, null);
			}
			ps.setLong(27, b.getTurnoverParam());
			ps.setLong(28, bc.getBonusAmount());
			ps.setString(29, bu.getComments());
			ps.executeUpdate();
	
			// save needed values in bonus user instance
			bu.setTypeId(b.getTypeId());
			bu.setBonusAmount(bc.getBonusAmount());
			bu.setId(getSeqCurValue(con, "SEQ_BONUS_USERS"));
	
			// Insert new email
			try {
				int skinId = (int)UsersDAOBase.getSkinIdByUserId(con, bu.getUserId());
				if (/*!CommonUtil.isHebrewSkin(skinId) && */isNeedToSendInternalMail) {
					//int langId = SkinsDAOBase.getById(con, skinId).getDefaultLanguageId();
					int langId = SkinsDAOBase.getDefaultLanguageIdBySkinId(con, skinId);
					MailBoxTemplate template = MailBoxTemplatesDAOBase.getTemplateByTypeSkinLanguage(con, ConstantsBase.MAILBOX_TYPE_BONUS, skinId, langId);
					MailBoxUser email = new MailBoxUser();
					email.setTemplateId(template.getId());
					email.setUserId(bu.getUserId());
					email.setWriterId(bu.getWriterId());
					email.setSenderId(template.getSenderId());
					email.setIsHighPriority(template.getIsHighPriority());
					email.setPopupTypeId(template.getPopupTypeId());
					email.setSubject(template.getSubject());
					email.setBonusUserId(bu.getId());
					MailBoxUsersDAOBase.insert(con, email);
				}
			} catch (Exception e) {
				log.warn("Problem sending a new email to user inbox!", e);
			}
		} finally {
			closeStatement(ps);
		}
		return true;
	}
	
  public static void activateBonusUsers(Connection conn, BonusUsers bu, long transactionId, long investmentId) throws SQLException {
        PreparedStatement pstmt = null;
        int index = 1;
        try{
            String sql =
                    "UPDATE " +
                        "bonus_users " +
                    "SET " +
                        "bonus_state_id = " + ConstantsBase.BONUS_STATE_ACTIVE + ", " +
                        "time_activated = current_date, " +
                        "number_of_actions_reached = number_of_actions_reached + 1 , ";
            
            if (bu.getTypeId() != ConstantsBase.BONUS_TYPE_PERCENT_AFTER_DEPOSIT_AND_SUM_INVEST &&
            		bu.getBonusId() != ConstantsBase.DYNAMIC_AMOUNT_AFTER_SUM_INVEST_ID) {
            	sql += "sum_invest_qualify_reached = sum_invest_qualify , ";
            }
                        
            sql += "bonus_amount = ? , " +
            	   "adjusted_amount = ? , " +
                   "sum_invest_withdraw = ? ";

            if (transactionId > 0){
            	sql += " ,activated_transaction_id = "	+ transactionId + " ";
            }

            if (investmentId > 0){
            	sql += " ,activated_investment_id = "	+ investmentId + " ";
            }

            if (bu.isHasSteps()){
            	sql += 	"," +
            			"bonus_percent = ?, " +
			            "min_deposit_amount = ?, " +
			            "max_deposit_amount  = ? ";
            }

            sql +=  "WHERE " +
			            "id = ?";


            pstmt = conn.prepareStatement(sql);

            pstmt.setLong(index++, bu.getBonusAmount());
            pstmt.setLong(index++, bu.getBonusAmount());
            pstmt.setLong(index++, bu.getSumInvWithdrawal());

            if (bu.isHasSteps()){
				pstmt.setDouble(index++, bu.getBonusPercent());
				pstmt.setLong(index++, bu.getMinDepositAmount());
				pstmt.setLong(index++, bu.getMaxDepositAmount());
				pstmt.setLong(index++, bu.getId());
            } else {
            	pstmt.setLong(index++, bu.getId());
            }

            pstmt.executeUpdate();
        } finally {
            closeStatement(pstmt);
        }
    }
    public static void addBonusUserWithdrawWagering(Connection conn, long bonusUsersId, long amount) throws SQLException {
        PreparedStatement pstmt = null;
        try{
            String sql =
                    "UPDATE " +
                        "bonus_users " +
                    "SET " +
                        "sum_invest_withdraw_reached = sum_invest_withdraw_reached + ? " +
                    "WHERE " +
                        "id = ?";
            pstmt = conn.prepareStatement(sql);
            pstmt.setLong(1, amount);
            pstmt.setLong(2, bonusUsersId);
            pstmt.executeUpdate();
        } finally {
            closeStatement(pstmt);
        }
    }
    
    public static void updateBonusesSeen(Connection conn, long bonusUsersId) throws SQLException {
        PreparedStatement pstmt = null;
        try{
            String sql =
                    "UPDATE " +
                        "bonus_users " +
                    "SET " +
                        "IS_BONUS_SEEN = 1 " +
                    "WHERE " +
                        "user_id = ? ";
            pstmt = conn.prepareStatement(sql);
            pstmt.setLong(1, bonusUsersId);
            pstmt.executeUpdate();
        } finally {
            closeStatement(pstmt);
        }
    }

    public static void doneBonusUsers(Connection conn, long bonusUsersId, boolean updateSumInvestWithdraw, long investmentId) throws SQLException {
        PreparedStatement pstmt = null;
        try{
            String sql =
                    " UPDATE " +
                        " bonus_users " +
                    " SET ";

            if (updateSumInvestWithdraw){
            	sql += " sum_invest_withdraw_reached = sum_invest_withdraw, " +
            		   " wagering_end_investment_id = " + (investmentId > 0 ? investmentId : "null") + ", ";
            }

            sql +=      " bonus_state_id = " + ConstantsBase.BONUS_STATE_DONE + ", " +
                        " time_done = current_date " +
                    " WHERE " +
                        " id = ?";
            pstmt = conn.prepareStatement(sql);
            pstmt.setLong(1, bonusUsersId);
            pstmt.executeUpdate();
        } finally {
            closeStatement(pstmt);
        }
    }
    
    /**
     * Update adjusted_amount attribute by the given bonus_users id.
     * @param conn
     * @param id - of bonus_users table
     * @param amount - to be insert to adjusted amount attribute
     * @throws SQLException
     */
    public static void updateAdjustedAmount(Connection conn, long id, long amount) throws SQLException {
        PreparedStatement ps = null;
        int ind = 1;
        try {
            String sql = 
            			"UPDATE " +
            			"	bonus_users " +
            		    "SET " +
            		    "	adjusted_amount = ? " +
                        "WHERE " +
                        "	id = ?";
            ps = conn.prepareStatement(sql);
            ps.setLong(ind++, amount);
            ps.setLong(ind++, id);
            ps.executeUpdate();
            log.debug("\nBMS, updateAdjustedAmount in bonus_users table, adjusted_amount:" + amount + " where id: " + id);
        } finally {
            closeStatement(ps);
        }
    }
    /**
     * Load all bonus population limits
     * @param con
     * @return
     * @throws SQLException
     */
    public static ArrayList<BonusPopulationLimit> getBonusPopulationLimits(Connection con, long popEntryId, long bonusId, long currencyId) throws SQLException {
        PreparedStatement ps = null;
        ResultSet rs = null;
        ArrayList<BonusPopulationLimit> list = new ArrayList<BonusPopulationLimit>();
        try	{
            String sql =
            	" SELECT " +
            		" bp.bonus_id, " +
            		" bp.population_type_id, " +
            		" bp.update_type, " +
            		" bpl.* " +
            	" FROM " +
            		" bonus_populations bp, " +
            		" bonus_population_limits bpl," +
            		" populations p," +
            		" population_entries pe " +
            	" WHERE " +
            		" bpl.bonus_population_id = bp.id " +
            		" AND bp.population_type_id = p.population_type_id " +
            		" AND p.id = pe.population_id " +
            		" AND pe.id = " + popEntryId + " " +
            		" AND bp.bonus_id = " + bonusId + " " +
            		" AND bpl.currency_id = " + currencyId + " " +
            	" ORDER BY " +
            		" bpl.bonus_population_id, " +
            		" bpl.currency_id, " +
            		" bpl.level_no ";

            ps = con.prepareStatement(sql);
            rs = ps.executeQuery();
            while (rs.next()) {
            	BonusPopulationLimit bpl = new BonusPopulationLimit();
            	bpl.setId(rs.getLong("id"));
            	bpl.setBonusId(rs.getLong("bonus_id"));
            	bpl.setPopTypeId(rs.getLong("population_type_id"));
            	bpl.setCurrencyId(rs.getLong("currency_id"));
            	bpl.setMinDepositParam(rs.getLong("min_deposit_param"));
            	bpl.setMaxDepositParam(rs.getLong("max_deposit_param"));
            	bpl.setMinResult(rs.getLong("min_result"));
            	bpl.setMultiplication(rs.getLong("multiplication"));
            	bpl.setLevel(rs.getLong("level_no"));
            	bpl.setStepRangeMul(rs.getLong("STEP_RANGE_MUL"));
            	bpl.setStepLevelMul(rs.getLong("STEP_LEVEL_MUL"));
            	bpl.setStepBonusAddition(rs.getDouble("STEP_BONUS_ADDITION"));
            	bpl.setMinBonusPercent(rs.getDouble("MIN_BONUS_PERCENT"));
            	bpl.setLimitUpdateType(rs.getInt("UPDATE_TYPE"));
            	bpl.setBonusAmount(rs.getLong("BONUS_AMOUNT"));
            	bpl.setMinInvestmentAmount(rs.getLong("MIN_INVESTMENT_AMOUNT"));
            	bpl.setMaxInvestmentAmount(rs.getLong("MAX_INVESTMENT_AMOUNT"));
                list.add(bpl);
            }
        } finally {
            closeStatement(ps);
            closeResultSet(rs);
        }
        return list;
    }
	/**
	 * Get bonusCurrencyId list by bonus id and currency id
	 * @return
	 * @throws SQLException
	 */
	public static ArrayList<BonusUsersStep> getBonusSteps(Connection con, long bonusId, long currencyId) throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		ArrayList<BonusUsersStep> list =  new ArrayList<BonusUsersStep>();

		try	{
		    String sql = "SELECT * " +
		    			 "FROM " +
		    			 	"bonus_currency " +
		    			 "WHERE " +
		    			 	"bonus_id = ? AND " +
		    			 	"currency_id = ? " +
		    			 " ORDER BY step ";

			ps = con.prepareStatement(sql);
			ps.setLong(1, bonusId);
			ps.setLong(2, currencyId);
			rs = ps.executeQuery();

			while ( rs.next() ) {
				BonusUsersStep temp = new BonusUsersStep();
		    	temp.setBonusPercent(rs.getDouble("bonus_percent"));
		    	temp.setMinDepositAmount(rs.getLong("min_deposit_amount"));
		    	temp.setMaxDepositAmount(rs.getLong("max_deposit_amount"));
				list.add(temp);
			}
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		return list;
	}
    /**
     * Update state id of the bonus
     * @param con db connection
     * @param id bonus users id
     * @param stateId bonus state id for update
     * @throws SQLException
     */
    public static void updateState(Connection con, long id, long stateId, long writerIdCancel) throws SQLException {

        PreparedStatement ps = null;
        try {
            String sql = " UPDATE " +
            				" bonus_users " +
            		     " SET " +
            		     	" bonus_state_id = ? ";

            if (ConstantsBase.BONUS_STATE_CANCELED == stateId){
            	sql += 		" ,TIME_CANCELED = sysdate ";
            }else if (ConstantsBase.BONUS_STATE_REFUSED == stateId){
            	sql += 		" ,TIME_REFUSED = sysdate ";
            } else if (ConstantsBase.BONUS_STATE_WITHDRAWN == stateId) { //TODO need to be refactor - contain time status change in other table. 
            	sql += 		" ,TIME_WITHDRAWN = sysdate ";
            } else if (ConstantsBase.BONUS_STATE_WAGERING_WAIVED == stateId) {
            	sql += 		" ,TIME_WAGERING_WAIVED = sysdate ";
            }
            
            if (stateId == ConstantsBase.BONUS_STATE_CANCELED || 
            		stateId == ConstantsBase.BONUS_STATE_WITHDRAWN || 
            		stateId == ConstantsBase.BONUS_STATE_WAGERING_WAIVED) {
            	sql += 		" ,WRITER_ID_CANCEL = " + writerIdCancel + " ";
            }
            
            sql +=
                        " WHERE " +
                            " id = ?";

            ps = con.prepareStatement(sql);

            ps.setLong(1, stateId);
            ps.setLong(2, id);
            ps.executeUpdate();

        } finally {
            closeStatement(ps);
        }
    }
    
    public static void addBonusUsersAction(Connection conn, long bonusUsersId) throws SQLException {
        PreparedStatement pstmt = null;
        try{
            String sql =
                    "UPDATE " +
                        "bonus_users " +
                    "SET " +
                        "number_of_actions_reached = number_of_actions_reached + 1 " +
                    "WHERE " +
                        "id = ?";
            pstmt = conn.prepareStatement(sql);
            pstmt.setLong(1, bonusUsersId);
            pstmt.executeUpdate();
        } finally {
            closeStatement(pstmt);
        }
    }
    
    public static void useBonusUsers(Connection conn, long bonusUsersId) throws SQLException {
        PreparedStatement pstmt = null;
        try{
            String sql =
                    " UPDATE " +
                        " bonus_users " +
                    " SET " +
                        " bonus_state_id = " + ConstantsBase.BONUS_STATE_USED + ", " +
                        " time_used = current_date " +
                    " WHERE " +
                        " id = ? " +
                        " and time_used is null ";
            pstmt = conn.prepareStatement(sql);
            pstmt.setLong(1, bonusUsersId);
            pstmt.executeUpdate();
        } finally {
            closeStatement(pstmt);
        }
    }
	public static void updateBonusUsers(Connection conn, BonusUsers bu) throws SQLException {
        PreparedStatement pstmt = null;
        try{
            String sql =
                    "UPDATE " +
                        "bonus_users " +
                    "SET " +
                        "bonus_amount = ? , " +
                        "sum_invest_withdraw = ? , " +
                        "sum_invest_qualify = ? " +
                    "WHERE " +
			            "id = ?";


            pstmt = conn.prepareStatement(sql);

            pstmt.setLong(1, bu.getBonusAmount());
            pstmt.setLong(2, bu.getSumInvWithdrawal());
            pstmt.setLong(3, bu.getSumInvQualify());
            pstmt.setLong(4, bu.getId());

            pstmt.executeUpdate();
        } finally {
            closeStatement(pstmt);
        }
    }
	
    public static void addBonusUsersSumDeposits(Connection conn, long bonusUsersId, long amount) throws SQLException {
        PreparedStatement pstmt = null;
        try{
            String sql =
                    "UPDATE " +
                        "bonus_users " +
                    "SET " +
                    	"sum_deposits_reached = sum_deposits_reached + ? " +
                    "WHERE " +
                        "id = ?";
            pstmt = conn.prepareStatement(sql);
            pstmt.setLong(1, amount);
            pstmt.setLong(2, bonusUsersId);
            pstmt.executeUpdate();
        } finally {
            closeStatement(pstmt);
        }
    }
    
    public static void addBonusUserQualifyWagering(Connection conn, long bonusUsersId, long amount) throws SQLException {
        PreparedStatement pstmt = null;
        try{
            String sql =
                    "UPDATE " +
                        "bonus_users " +
                    "SET " +
                        "sum_invest_qualify_reached = sum_invest_qualify_reached + ? " +
                    "WHERE " +
                        "id = ?";
            pstmt = conn.prepareStatement(sql);
            pstmt.setLong(1, amount);
            pstmt.setLong(2, bonusUsersId);
            pstmt.executeUpdate();
        } finally {
            closeStatement(pstmt);
        }
    }

	public static void updateBonusUsersQualify(Connection conn, BonusUsers bu) throws SQLException {
        PreparedStatement pstmt = null;
        try{
            String sql =
                    "UPDATE " +
                        "bonus_users " +
                    "SET " +
                        "sum_invest_qualify_reached = sum_invest_qualify " +
                    "WHERE " +
			            "id = ?";

            pstmt = conn.prepareStatement(sql);
            pstmt.setLong(1, bu.getId());
            pstmt.executeUpdate();
        } finally {
            closeStatement(pstmt);
        }
    }
	/**
	 * insert bonus to user
	 * @param con db connection
	 * @throws SQLException
	 */
	public static boolean insertBonusUserSteps(Connection con, ArrayList<BonusUsersStep> bonusSteps, long bonusUserId) throws SQLException {
		PreparedStatement ps = null;

		try {

			for (int index=0; index < bonusSteps.size(); index++){

				String sql="INSERT " +
							"INTO bonus_users_steps(ID, " +
													"BONUS_PERCENT," +
													"MIN_DEPOSIT_AMOUNT," +
													"MAX_DEPOSIT_AMOUNT," +
													"BONUS_USER_ID) " +
							"VALUES(SEQ_BONUS_USERS_STEPS.nextval,?,?,?,?)";

				ps = con.prepareStatement(sql);

				ps.setDouble(1, bonusSteps.get(index).getBonusPercent());
				ps.setLong(2, bonusSteps.get(index).getMinDepositAmount());
				ps.setLong(3, bonusSteps.get(index).getMaxDepositAmount());
				ps.setLong(4, bonusUserId);

				ps.executeUpdate();

			}

		}
		finally {
			closeStatement(ps);
		}

		return true;
	}
	  /**
     * get bonus users steps
     * @param bonususerId
     * @return list of bonus users steps
     * @throws SQLException
     */
    public static ArrayList<BonusUsersStep> getBonusUsersSteps(Connection con, long bonusUserId) throws SQLException {
    	ArrayList<BonusUsersStep> list = new ArrayList<BonusUsersStep>();
    	PreparedStatement pstmt = null;
		ResultSet rs = null;

		try{
		    String sql =
		            "SELECT * " +
		            "FROM " +
		            	"bonus_users_Steps bus " +
		            "WHERE " +
		            	"bus.bonus_user_id = ? " +
		            "ORDER BY " +
				    	"bonus_percent";

		    pstmt = con.prepareStatement(sql);
		    pstmt.setLong(1, bonusUserId);

		    rs = pstmt.executeQuery();
		    while (rs.next()) {
		    	BonusUsersStep temp = new BonusUsersStep();
		    	temp.setId(rs.getLong("id"));
		    	temp.setBonusPercent(rs.getDouble("bonus_percent"));
		    	temp.setMinDepositAmount(rs.getLong("min_deposit_amount"));
		    	temp.setMaxDepositAmount(rs.getLong("max_deposit_amount"));
		    	temp.setBonusUserId(rs.getLong("bonus_user_Id"));
		    	list.add(temp);
		    }
		} finally {
		   closeStatement(pstmt);
		   closeResultSet(rs);
		}
		return list;
	}
    
    public static ArrayList<BonusUsers> getActiveAndUsed(Connection conn, long userId) throws SQLException {
        ArrayList<BonusUsers> list = new ArrayList<BonusUsers>();
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try{
            String sql =
                    " SELECT " +
                        " u.*, " +
                        " bt.class_type_id " +
                    " FROM " +
                        " bonus_users u, " +
                        " bonus_types bt " +
                    " WHERE " +
                        " u.user_id = ? AND " +
                        " u.type_id = bt.id AND " +
                        " u.bonus_state_id IN (" + Bonus.STATE_ACTIVE + ", " + Bonus.STATE_USED + ") AND " +
                        " (u.start_date IS NULL OR u.start_date <= current_date) " +
                    " ORDER BY " +
                        	" u.time_created";
            pstmt = conn.prepareStatement(sql);
            pstmt.setLong(1, userId);
            rs = pstmt.executeQuery();
            while (rs.next()) {
            	BonusUsers bu = getBonusUsersVO(rs, conn);
            	bu.setBonusClassType(rs.getLong("class_type_id"));
                list.add(bu);
            }
        } finally {
            closeResultSet(rs);
            closeStatement(pstmt);
        }
        return list;
    }
    
    public static ArrayList<BonusUsers> getBonusesForActivation(Connection conn, long userId) throws SQLException {
        ArrayList<BonusUsers> list = new ArrayList<BonusUsers>();
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try{
            String sql =
                    " SELECT " +
                        " * " +
                    " FROM " +
                        " bonus_users bu," +
                        " bonus_types bt " +
                    " WHERE " +
                        " bu.user_id = ? " +
                        " AND (bu.bonus_state_id = " + ConstantsBase.BONUS_STATE_GRANTED + " " +
                        		" OR (bu.bonus_state_id = " + ConstantsBase.BONUS_STATE_ACTIVE + " AND bu.bonus_id = " + ConstantsBase.DYNAMIC_DEPOSIT_GET_AMOUNT_AFTER_SUM_INVEST_ID + ") " +
                        		" OR (bu.bonus_state_id = " + ConstantsBase.BONUS_STATE_ACTIVE + " AND bu.bonus_id = " + ConstantsBase.DYNAMIC_AMOUNT_AFTER_SUM_INVEST_ID + ")) " +
                        " AND (bu.start_date IS NULL OR bu.start_date <= current_date) " +
                        " AND(bu.end_date IS NULL OR bu.end_date >= current_date) " +
                        " AND bu.type_id = bt.id " +
                    " ORDER BY " +
                        " time_created ";
            pstmt = conn.prepareStatement(sql);
            pstmt.setLong(1, userId);
            rs = pstmt.executeQuery();
            while (rs.next()) {
            	BonusUsers bu = getBonusUsersVO(rs, conn);
            	bu.setBonusClassType(rs.getLong("class_type_id"));
            	bu.setTurnoverParam(rs.getLong("turnover_param"));
                list.add(bu);
            }
        } finally {
            closeResultSet(rs);
            closeStatement(pstmt);
        }
        return list;
    }
    
    /**
     * chec if the user have next invest on us bonus
     * @param conn
     * @param userId
     * @return BonusUsers with the requested bonus
     * @throws SQLException
     */
    public static BonusUsers getNextInvestOnUsBonusUserIdAndType(Connection conn, long userId) throws SQLException {
    	BonusUsers bu = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            String sql =
                    " SELECT " +
	                    " bu.id, " +
	                    " bu.type_id " +
                    " FROM " +
                        " bonus_users bu, " +
                        " bonus_types bt " +
                    " WHERE " +
                        " bu.user_id = ? AND " +
                        " bt.class_type_id = " + Bonus.CLASS_TYPE_NEXT_INVEST_ON_US + " AND " +
                        " bu.bonus_state_id = " + Bonus.STATE_ACTIVE + " AND " +
                        " bt.id = bu.type_id AND "+
                        " (bu.start_date IS NULL OR bu.start_date <= current_date) AND " +
                        " (bu.end_date IS NULL OR bu.end_date >= current_date) " +
                    " ORDER BY " +
                        " bu.id";
            pstmt = conn.prepareStatement(sql);
            pstmt.setLong(1, userId);
            rs = pstmt.executeQuery();
            if (rs.next()) {
                bu = new BonusUsers();
                bu.setId(rs.getLong("id"));
                bu.setTypeId(rs.getLong("type_id"));
            }
        } finally {
            closeResultSet(rs);
            closeStatement(pstmt);
        }
        return bu;
    }
    
    /**
     * Get min and max values for free investment
     * @param conn
     * @param userId
     * @return <code>HashMap</code> with min & max values
     * @throws SQLException
     */
    public static HashMap<String,Long> getMinAndMaxFreeInv(Connection conn, long bonusUserId) throws SQLException {
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        HashMap<String, Long> freeInvValues = new HashMap<String, Long>();
        try {
            String sql =
                "SELECT " +
                    "min_invest_amount, " +
                    "max_invest_amount " +
                "FROM " +
                    "bonus_users " +
                "WHERE " +
                    "id = ?";
            pstmt = conn.prepareStatement(sql);
            pstmt.setLong(1, bonusUserId);
            rs = pstmt.executeQuery();
            if (rs.next()) {
                freeInvValues.put(ConstantsBase.FREE_INVEST_MIN, rs.getLong("min_invest_amount"));
                freeInvValues.put(ConstantsBase.FREE_INVEST_MAX, rs.getLong("max_invest_amount"));
            }
        } finally {
            closeResultSet(rs);
            closeStatement(pstmt);
        }
        return freeInvValues;
    }
    
    /**
     * @param conn
     * @param bu
     * @param investmentId
     * @param ratio
     * @throws SQLException
     */
    public static void insertBonusWageringInvestments(Connection conn, BonusUsers bu, long investmentId, double ratio, long relative) throws SQLException {
		PreparedStatement ps = null;
		int ind = 1;
		try {
			String sql =
						"INSERT INTO bonus_wagering_investments " +
						"	( " +
						"		id " +
						"		,bonus_users_id " +
						"		,ratio " +
						"		,investment_id " +
						"		,time_created" +
						"		,relative_part" +
						" 	) " +
						"VALUES	" +
						"	( " +
						"		seq_bonus_wagering_investments.nextval " +
						"		,? " +
						"		,? " +
						"		,? " +
						"		,sysdate " +
						"		,? " +
						"	) ";

			ps = conn.prepareStatement(sql);
			ps.setLong(ind++, bu.getId());
			ps.setDouble(ind++, ratio);
			ps.setLong(ind++, investmentId);
			ps.setLong(ind++, relative);
			ps.executeUpdate();
			log.info("about to insert into bonus_wagering_investments: "
					+ "bonus_users_id: " +  bu.getId() + " "
					+ "ratio: " + ratio + " "
					+ "investment_id: " + investmentId);
		} finally {
			closeStatement(ps);
		}
	}
    
    /**
	 * @param conn
	 * @param id
	 * @param writer
	 * @param userId
	 * @throws SQLException
	 */
	public static void reverseBonusWagering(Connection conn, long id, long writer, long userId) throws SQLException {
		PreparedStatement pstmt = null;
		ResultSet rs = null;		
		try{
		    String sql =
		    		"UPDATE " +
		    		"	bonus_users bu " +
    				"SET " +
    				"	bu.sum_invest_withdraw_reached = bu.sum_invest_withdraw_reached - " +
    				"		(" +
    				"		SELECT " +
	    			"			bwi.relative_part " + //bwi.ratio * tf.factor * i.amount 
    				"		FROM " +
					"			investments i " +
					"			,bonus_wagering_investments bwi " +
					//"			,turnover_factor tf " +
					//"			,opportunities o " +
					"		WHERE " +
					"			i.id = bwi.investment_id " +
					//"			AND tf.OPPORTUNITY_TYPE_ID = o.OPPORTUNITY_TYPE_ID " +
					//"			AND o.id = i.OPPORTUNITY_ID " +
					"			AND i.id = ? " +
					"			AND bwi.bonus_users_id = bu.id" +
					"		) " +
		    		"WHERE " +
					"	EXISTS " +
		    		"	(" +
					"	SELECT " +
					"		1 " +
		    		"	FROM " +
		    		"		bonus_wagering_investments bwi " +
		    		"	WHERE " +
		    		"		bwi.investment_id = ? " +
		    		"		AND bu.id =  bwi.BONUS_USERS_ID " +
		    		"	)";
		    pstmt = conn.prepareStatement(sql);
		    pstmt.setLong(1, id);
		    pstmt.setLong(2, id);
		    pstmt.executeUpdate();
		    log.info("\nreverse Bonus Wagering\n"
		    		+ "update bonus_users.sum_invest_withdraw_reached:\n"
					+ "investment_id: " + id);
    	} finally {
		   closeStatement(pstmt);
		   closeResultSet(rs);
		}
	}
	
	/**
     * check if the user have next invest on us bonus
     * @param conn
     * @param userId
     * @return String with the bonus id and bonus type (bonusid_typeid)
     * @throws SQLException
     */
    public static String hasNextInvestOnUsWithBonusType(Connection conn, long userId, boolean isSelectedDeafultOdds) throws SQLException {
        String id_type = "0_0";
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            String sql =
                    "SELECT " +
                        "bu.id, " +
                        "bt.id bonus_type " +
                    "FROM " +
                        "bonus_users bu, " +
                        "bonus_types bt " +
                    "WHERE " +
                        "bu.user_id = ? AND " +
                        "bt.class_type_id = " + ConstantsBase.BONUS_CLASS_TYPE_NEXT_INVEST_ON_US + " AND " +
                        "bu.bonus_state_id = " + ConstantsBase.BONUS_STATE_ACTIVE + " AND " +
                        "bt.id = bu.type_id AND "+
                        "(bu.start_date IS NULL OR bu.start_date <= current_date) AND " +
                        "(bu.end_date IS NULL OR bu.end_date >= current_date) ";
            if (!isSelectedDeafultOdds){
            	sql += " AND bt.id not in (9,10,11) ";
            }
           sql += "ORDER BY " +
                  "bu.id";
            pstmt = conn.prepareStatement(sql);
            pstmt.setLong(1, userId);
            rs = pstmt.executeQuery();
            if (rs.next()) {
            	id_type = rs.getLong("id") + "_" + rs.getLong("bonus_type");
            }
        } finally {
            closeResultSet(rs);
            closeStatement(pstmt);
        }
        return id_type;
    }

	
	/**
	 * This method return the sum of adjusted amount by given user id if the bonus is used and has transaction.
	 * Bonus that count as "free" dose not have transaction on investment, but if the user is not win on the investment 
	 * there will be a transaction.
	 *  
	 * @param conn
	 * @param userId
	 * @return
	 * @throws SQLException
	 */
	public static long getUserTotalAdjustedAmount(Connection conn, long userId) throws SQLException {
		long total = 0;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            String sql =
            			"SELECT " +
                        "	SUM(adjusted_amount) as total " +
                        "FROM " +
                        "	bonus_users bu " +
                        "WHERE " +
                        "	bu.wagering_parameter > 0 and " +
                        "	bu.user_id = ? and " +
                        "	bu.bonus_state_id in (" + ConstantsBase.BONUS_STATE_USED + "," + ConstantsBase.BONUS_STATE_ACTIVE + ") ";
            pstmt = conn.prepareStatement(sql);
            pstmt.setLong(1, userId);
            rs = pstmt.executeQuery();
            if (rs.next()) {
                total = rs.getLong("total");
            }
        } finally {
            closeResultSet(rs);
            closeStatement(pstmt);
        }
        return total;
	}
	
	/**
	 * Use Select statement to retrieve all bonus_users data where  the state is using ("used").
	 * @param conn
	 * @param userId
	 * @return
	 * @throws SQLException
	 */
	public static ArrayList<BonusUsers> getBonusUsed(Connection conn, long userId) throws SQLException {
        ArrayList<BonusUsers> list = new ArrayList<BonusUsers>();
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            String sql =
                    	"SELECT " +
                    	" 	bu.* " +
                        //"	t.class_type_id " +
                        "FROM " +
                        " 	bonus_users bu " +
                        "WHERE " +
                        " 	bu.wagering_parameter > 0 and " +
                        " 	bu.user_id = ? and " +
                        "	bu.bonus_state_id = " + ConstantsBase.BONUS_STATE_USED + " " +
                    	"ORDER BY " +
                        "	bu.time_created";
            pstmt = conn.prepareStatement(sql);
            pstmt.setLong(1, userId);
            rs = pstmt.executeQuery();
            while (rs.next()) {
            	BonusUsers bu = getBonusUsersVO(rs, conn);
            	//bu.setBonusClassType(rs.getLong("class_type_id"));
                list.add(bu);
            }
        } finally {
            closeResultSet(rs);
            closeStatement(pstmt);
        }
        return list;
    }
	
	/**
	 * Get Auto bonus id by user's parameters
	 * @param con
	 * @param user
	 * @return
	 * @throws SQLException
	 */
	public static long getAutoBonusId(Connection con, long userId) throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		long bonusId = 0;

		try	{
		    String sql = " SELECT " +
		    			 	" bap.bonus_id " +
		    			 " FROM " +
		    			 	" bonus_auto_parmeters bap, " +
		    			 	" marketing_combinations mc, " +
		    			 	" users u, " +
		    			 	" skins s " +
		    			 " WHERE " +
		    			 	" u.id = ? " +
		    			 	" and bap.type = ? " +
			    			" and u.combination_id = mc.id " +
			    			" and u.skin_id = s.id " +
			    			" and bap.combination_id in (0,u.combination_id) " +
			    			" and bap.campaign_id in (0,mc.campaign_id) " +
			    			" and bap.skin_id in (0,u.skin_id) " +
			    			" and bap.business_skin_id in (0,s.business_case_id) " +
			    			" and bap.country_id in (0,u.country_id) " +
			    			" and bap.affiliate_key in (0,u.affiliate_key) " +
			    			" and bap.is_active = 1 " +
			    			" and sysdate between bap.time_start and bap.time_end " +
			    		 " ORDER BY " +
			    		 	" bap.combination_id desc, " +
			    			" bap.campaign_id desc, " +
			    			" bap.skin_id desc, " +
			    			" bap.business_skin_id desc, " +
			    			" bap.country_id desc, " +
			    			" bap.affiliate_key desc ";

			ps = con.prepareStatement(sql);
			ps.setLong(1, userId);
			ps.setLong(2, ConstantsBase.BONUS_AUTO_PARAMETER_TYPE_REGULAR);

			rs = ps.executeQuery();

			if ( rs.next() ) {
				bonusId = rs.getLong("bonus_id");
			}
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		return bonusId;
	}

	/**
	 * Get bonus users by filtets
	 * @param con db connection
	 * @param from fromDate filter
	 * @param to toDate filter
	 * @return
	 * @throws SQLException
	 */
	public static ArrayList<BonusUsers> getBonusUsers(Connection con, long userId, Date from, Date to, String userName, String skins, long classId,
			long bonusTypeId, long bonusStateId, long userActionId, long writerGroupId, long populationTypeId, long writerId, String utcOffset) throws SQLException {

	  PreparedStatement ps = null;
	  ResultSet rs = null;
	  ArrayList<BonusUsers> list = new ArrayList<BonusUsers>();

	  try {
			String sql = "SELECT " +
				        	"bu.* , " +
				        	"u.user_name, " +
				        	"u.utc_offset, " +
				        	"u.skin_id, " +
				         	"bs.name bonus_state_name, " +
				         	"b.name bonus_name, " +
				         	"bt.name bonus_type_name, " +
				         	//"ua.name user_action, " +
				         	"w.user_name writer_user_name, " +
				         	"bt.class_type_id, " +
				         	"bc.currency_id, "+
	    					"i.win as inv_win, " +
	    					"i.bonus_odds_change_type_id, " +
	    					"i.house_result, " +
	    					"CASE WHEN EXISTS (SELECT 1 FROM transactions t WHERE t.bonus_user_id = bu.id) THEN 1 ELSE 0 END as has_bonus_tran " + 
				 		 "FROM  " +
				         	"bonus_users bu " +
				         	"	left join investments i on i.bonus_user_id = bu.id, " +
					        "users u, " +
					        "bonus b,  " +
					        //"user_actions ua, " +
					        "bonus_types bt, " +
					        "bonus_states bs, " +
					        "writers w, " +
					        "bonus_currency bc " +
				 		 "WHERE " +
					        "bu.user_id = u.id AND " +
					      	"bu.bonus_id = b.id AND " +
					      	//"bu.user_action_id = ua.id AND " +
					      	"bu.bonus_state_id = bs.id AND " +
					      	"bu.writer_id = w.id AND " +
					      	"bc.bonus_id=b.id AND "+
					      	"bu.type_id = bt.id AND " +
					      	"bc.currency_id = u.currency_id AND " +
					      	"bc.step in (0,1) AND " + // Either it's stepless bonus or it's the first step
					      	"bu.bonus_id IN (SELECT bonus_id " +
                                      		 "FROM bonus_skins " +
                                      		 "WHERE skin_id IN (" + skins + ") " +
                                          ") ";

			if (userId > 0) {
				sql += "AND u.id = " + userId + " ";
			}

			if ( null != from ) {
				sql += "AND bu.time_created >= ? ";
			}

			if ( null != to ) {
				sql += "AND bu.time_created <= ? ";
			}

			if (!CommonUtil.isParameterEmptyOrNull(userName)) {
				sql += "AND u.user_name = '"+userName+"' ";
			}

		  	if ( classId != ConstantsBase.USER_CLASS_ALL )   {

		  		if ( classId == ConstantsBase.USER_CLASS_COMPANY_OR_PRIVATE )  {
		  			sql += "AND (u.class_id="+ConstantsBase.USER_CLASS_PRIVATE+""+
		  				  " OR u.class_id="+ConstantsBase.USER_CLASS_COMPANY+")";
		  		}
		  		else {
		  				sql += " AND u.class_id =" + classId + " ";
		  		}
		  	}

		  	if (bonusTypeId > 0) {
		  		sql += "AND bt.id = " + bonusTypeId + " ";
		  	}

		  	if (bonusStateId > 0) {
		  		sql += "AND bs.id = " + bonusStateId + " ";
		  	}

		  	if (writerId > 0) {
		  		sql += " AND bu.writer_id = ? ";
		  	}

		  	/*if (userActionId > 0) {
		  		sql += "AND ua.id = " + userActionId + " ";
		  	}*/

//		  	if (populationTypeId > 0) {  // from retention entry
//		  		sql += "AND " + populationTypeId +
//		  					" IN ( " +
//		  					 		"SELECT bp.population_type_id " +
//		  					 		"FROM bonus_populations bp " +
//		  					 		"WHERE bp.bonus_id = bu.bonus_id " +
//		  					 ") ";
//		  	}
//
//		  	sql += "AND " + writerGroupId +
//					" IN ( " +
//				 		"SELECT bwg.writer_group_id " +
//				 		"FROM bonus_writer_groups bwg " +
//				 		"WHERE bwg.bonus_id = bu.bonus_id " +
//				 	 ") ";

		    sql += "AND u.skin_id in (" + skins + ") ";   	//writer permissions

			sql += "ORDER BY " +
						"bu.start_date";

			ps = con.prepareStatement(sql);

			if ( null != from ) {
				ps.setTimestamp(1, CommonUtil.convertToTimeStamp(CommonUtil.getDateTimeFormat(from, utcOffset)));
			}

			if ( null != to ) {
				ps.setTimestamp(2, CommonUtil.convertToTimeStamp(CommonUtil.getDateTimeFormat(CommonUtil.addDay(to), utcOffset)));
			}

			if (writerId > 0) {
				ps.setLong(3, writerId);
			}

			rs = ps.executeQuery();

			while (rs.next()) {
				BonusUsers vo = getBonusUsersVO(rs, con);
				if ((rs.getInt("bonus_odds_change_type_id") > 0) && (rs.getLong("house_result") < 0) && rs.getInt("has_bonus_tran") == 0 && vo.getWageringParam() > 0) {
    				vo.setBonusAmount(rs.getLong("skin_id") == Skin.SKIN_ETRADER ? (Math.round(rs.getLong("inv_win") * (1 - ConstantsBase.TAX_PERCENTAGE))) : rs.getLong("inv_win"));
    				vo.setBonusOddsWin(true);
    			}
				vo.setCurrency(CurrenciesManagerBase.getCurrency(rs.getInt("currency_id")));
				vo.setBonusName(rs.getString("bonus_name"));
				vo.setBonusTypeName(rs.getString("bonus_type_name"));
				vo.setBonusStateName(rs.getString("bonus_state_name"));
				vo.setUserName(rs.getString("user_name"));
				//vo.setUserAction(rs.getString("user_action"));
				vo.setWriterName(rs.getString("writer_user_name"));
				vo.setBonusClassType(rs.getLong("class_type_id"));
				vo.setUtcOffsetUser(rs.getString("utc_offset"));
				list.add(vo);
			}
		} finally {
				closeResultSet(rs);
				closeStatement(ps);
		}
		return list;
	}

    /**
     * @param con
     * @param bonusUserId
     * @return true if the bonus user can cancel on active
     * @throws SQLException
     */
    public static boolean isCanCancelOnActive(Connection con, long bonusUserId) throws SQLException {
    	PreparedStatement pstmt = null;
		ResultSet rs = null;
    	boolean isCanExpireOnActive = false;
		
		try{
		    String sql =
		            "SELECT " +
	            	"	bt.is_can_cancel_on_active " +
		            "FROM " +
	            	"	bonus_users bu, " +
	            	"	bonus_types bt " +
		            "WHERE " +
		            "	bu.type_id = bt.id AND " +
	        		"	bu.id = ? ";

		    pstmt = con.prepareStatement(sql);
		    pstmt.setLong(1, bonusUserId);
		    rs = pstmt.executeQuery();

		    if (rs.next()) {
		    	isCanExpireOnActive = rs.getBoolean("is_can_cancel_on_active");
		    }
    	} finally {
		   closeStatement(pstmt);
		   closeResultSet(rs);
		}
		return isCanExpireOnActive;
    }

    /**
     * get all user bonus by user id
     * @param userId
     * @return list of bonus
     * @throws SQLException
     */
    public static List<BonusUsers> getAllUserBonus(Connection con, long userId, Date from, Date to, long state, String utcOffset, ArrayList<Currency> currencyList) throws SQLException {
    	List<BonusUsers> list = new ArrayList<BonusUsers>();
    	PreparedStatement pstmt = null;
		ResultSet rs = null;
		String stateCondition = "<= " + ConstantsBase.BONUS_STATE_PENDING  + " AND ";
		if (state > 0 && state != 3 && state != 4) {
			stateCondition = "= " + state + " AND ";
		} else if (state == 3) {
			stateCondition = "IN (" + ConstantsBase.BONUS_STATE_USED + ", " + ConstantsBase.BONUS_STATE_DONE + ", " + ConstantsBase.BONUS_STATE_WITHDRAWN + ", " + ConstantsBase.BONUS_STATE_WAGERING_WAIVED + ") AND ";
		} else if (state == 4) {
			stateCondition = "IN (" + ConstantsBase.BONUS_STATE_GRANTED + ", " + ConstantsBase.BONUS_STATE_ACTIVE + ") AND ";
		}
		try{
		    String sql =
		            "SELECT " +
		            	"bu.*, " +
		            	"btsm.message, " +
		            	"u.currency_id, " +
		            	"bt.class_type_id, " +
		            	"i.id as invId " +
		            "FROM " +
		            	"bonus_users bu, " +
		            	"bonus_type_state_msg btsm, " +
		            	"users u, " +
		            	"bonus_types bt, " +
		            	"investments i " +
		            "WHERE " +
		            	"btsm.state = bu.bonus_state_id AND " +
		            	"btsm.type = bu.type_id AND " +
		            	"bu.bonus_state_id " + stateCondition +
		            	"u.id = bu.user_id AND " +
		            	"bt.id = bu.type_id AND " +
		            	"bu.id = i.bonus_user_id(+) AND " +
		        		"bu.user_id = ? AND " +
		        		"bt.id <> ? AND " + 
		        		"bu.bonus_id <> " + Bonus.COPYOP_COINS_CONVERT; // ignore all copyop coins convert actions
		    if (null != from && null != to) {
		    	 sql += "AND bu.start_date >= ? "+
				 		"AND bu.start_date <= ? ";
		    }
		    // order by unclaimed aka pending then date
		    sql += "ORDER BY " +
				    	"case when bu.bonus_state_id  = '10' then -1 end, " +
				    	"bu.start_date DESC";
		    pstmt = con.prepareStatement(sql);
		    pstmt.setLong(1, userId);
		    pstmt.setLong(2, ConstantsBase.BONUS_TYPE_CONVERT_POINTS_TO_CASH);
		    if (null != from && null != to) {
		    	pstmt.setTimestamp(3, CommonUtil.convertToTimeStamp(CommonUtil.getDateTimeFormat(from, utcOffset)));
		    	pstmt.setTimestamp(4, CommonUtil.convertToTimeStamp(CommonUtil.getDateTimeFormat(CommonUtil.addDay(to), utcOffset)));
		    }
		    rs = pstmt.executeQuery();
		    while (rs.next()) {
		    	BonusUsers temp = getBonusUsersVO(rs, con);
		    	temp.setCurrency((CommonUtil.getCurrencyById(currencyList, rs.getInt("currency_id"))));
		    	temp.setBonusStateDescription(rs.getString("message"));
		    	temp.setBonusClassType(rs.getLong("class_type_id"));
		    	temp.setInvestmentId(rs.getLong("invId"));
		    	temp.setTurnoverParam(rs.getLong("turnover_param"));
		    	temp.setStateExpired();

		    	BonusHandlerBase bh = BonusHandlerFactory.getInstance(temp.getTypeId());

		    	if (null != bh){
		    		try {
						bh.setBonusStateDescription(temp);
					} catch (BonusHandlersException e) {
						log.error("Error in setBonusStateDescription for bonus user id " + temp.getId(),e);
					}
		    	}

		    	list.add(temp);
		    }
		} finally {
		   closeStatement(pstmt);
		   closeResultSet(rs);
		}
		return list;
	}

        public static boolean hasNotSeenBonuses(Connection con, long userId, long seenParam) throws SQLException {
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			String sql =
                " SELECT " +
                    " u.IS_BONUS_SEEN " +
                " FROM " +
                    " bonus_users u " +
                " WHERE " +
                    " u.user_id = ? AND " +
                    " u.IS_BONUS_SEEN = ? ";
			pstmt = con.prepareStatement(sql);
			pstmt.setLong(1, userId);
			pstmt.setLong(2, seenParam);
			rs = pstmt.executeQuery();
			if (rs.next()) {
				return true;
			}
		} finally {
			closeResultSet(rs);
			closeStatement(pstmt);
		}
		return false;	
        }

	public static boolean hasOpenedBonuses(Connection con, long userId) throws SQLException {
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			String sql =
                    " SELECT " +
                        " u.*, " +
                        " bt.class_type_id " +
                    " FROM " +
                        " bonus_users u, " +
                        " bonus_types bt " +
                    " WHERE " +
                        " u.user_id = ? AND " +
                        " u.type_id = bt.id AND " +
                        " u.bonus_state_id IN (" + Bonus.STATE_GRANTED + ", " + Bonus.STATE_ACTIVE + ", " + Bonus.STATE_USED + ", "
                        							+ Bonus.STATE_PENDING + ") AND " +
                        " (u.start_date IS NULL OR u.start_date <= current_date) " +
                    " ORDER BY " +
                        	" u.time_created";
			pstmt = con.prepareStatement(sql);
			pstmt.setLong(1, userId);
			rs = pstmt.executeQuery();
			if (rs.next()) {
				return true;
			}
		} finally {
			closeResultSet(rs);
			closeStatement(pstmt);
		}
		return false;
	}
	
    public static List<BonusUsers> getTurnoverParamsForBonus(Connection con, List<BonusUsers> bonusUserList) throws SQLException {
    	PreparedStatement pstmt = null;
		ResultSet rs = null;
		
		try{
		    String sql ="SELECT ot.product_type_id AS product_type, rtf.factor * 100 AS factor "+
		    		  	  "FROM RETENTION_TURNOVER_FACTOR rtf, opportunity_types ot "+
		    		  	 "WHERE rtf.opportunity_type_id = ot.id "+
		    		      "AND ot.product_type_id IN (?, ?) "+
		    		     "GROUP BY rtf.factor, ot.product_type_id";

		    pstmt = con.prepareStatement(sql);
		    pstmt.setLong(1, 3);//option+
		    pstmt.setLong(2, 4);//binary 0-100
		    rs = pstmt.executeQuery();

		    while (rs.next()) {
				for (BonusUsers item : bonusUserList) {
					if (rs.getLong("product_type") == 3) {
						item.setTurnOverParamOptionPlus(rs.getLong("factor"));
					} else if (rs.getLong("product_type") == 4) {
						item.setTurnOverParamBinaryOneHundred(rs.getLong("factor"));
					}
				}
		    }
    	} finally {
		   closeStatement(pstmt);
		   closeResultSet(rs);
		}
		return bonusUserList;
    }

    /**
     * Get bonus amount when there was insert investment by given investmentId and type.
     * 
    /**
     * @param conn
     * @param investmentId
     * @param isSettled 
     * @return
     * @throws SQLException
     */
    public static ArrayList<BonusFormulaDetails> getBonusAmounts(Connection conn, long investmentId, boolean isSettled) throws SQLException {
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        ArrayList<BonusFormulaDetails> bonusFormulaD = new ArrayList<BonusFormulaDetails>();
        try {
            String sql =
		            	"SELECT " +
		            	"	bu.id as bonus_users_id, " +
		            	"	bi.bonus_amount, " +
		            	"	bu.adjusted_amount " +
		            	"FROM " +
		            	"	bonus_investments bi, " +
		            	" 	bonus_users bu " +
		            	"WHERE " +
	            		"	bu.id = bi.bonus_users_id AND " +
	            		"	bi.investments_id = ? AND " +
	            		"	bi.type_id = ? ";
            pstmt = conn.prepareStatement(sql);
            pstmt.setLong(1, investmentId);
            pstmt.setLong(2, isSettled == true ? ConstantsBase.BONUS_INVESTMENTS_TYPE_SETTLED_INV : ConstantsBase.BONUS_INVESTMENTS_TYPE_INSERT_INV);
            rs = pstmt.executeQuery();
            while (rs.next()) {
            	BonusFormulaDetails bfd = new BonusFormulaDetails();
            	bfd.setAdjustedAmount(rs.getLong("adjusted_amount"));
            	bfd.setAmount(rs.getLong("bonus_amount"));
            	bfd.setBonusUsersId(rs.getLong("bonus_users_id"));
            	bonusFormulaD.add(bfd);
            }
        } finally {
            closeResultSet(rs);
            closeStatement(pstmt);
        }
        return bonusFormulaD;
    }
    
    /**
	 * Get Auto bonus id by user's parameters
	 * @param con
	 * @param login id
	 * @return bonus id
	 * @throws SQLException
	 */
	public static long getAutoBonusByLoginId(Connection con, long loginId) throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		long bonusId = 0;

		try	{
		    String sql = " SELECT " +
		    			 	" bap.bonus_id " +
		    			 " FROM " +
		    			 	" bonus_auto_parmeters bap, " +
		    			 	" marketing_combinations mc, " +
		    			 	" users u, " +
		    			 	" logins l, " +
		    			 	" skins s " +
		    			 " WHERE " +
		    			 	" u.id = l.user_id " +
		    			 	" and l.id = ? " +
		    			 	" and bap.type = ? " +
			    			" and u.combination_id = mc.id " +
			    			" and u.skin_id = s.id " +
			    			" and bap.combination_id in (0,l.combination_id) " +
			    			" and bap.campaign_id in (0,mc.campaign_id) " +
			    			" and bap.skin_id in (0,u.skin_id) " +
			    			" and bap.business_skin_id in (0,s.business_case_id) " +
			    			" and bap.country_id in (0,u.country_id) " +
			    			" and bap.affiliate_key in (0, Marketing.Get_Affilate_Key(l.Dynamic_Param, l.Combination_Id)) " +
			    			" and bap.is_active = 1 " +
			    			" and sysdate between bap.time_start and bap.time_end " +
			    			" and NOT EXISTS ( "
			    							+ " SELECT "
			    								+ "1 "
			    							+ " FROM "
			    								+ "Bonus_Users bu "
			    							+ " WHERE "
			    								+ "bu.bonus_id = bap.bonus_id "
			    								+ "and bu.Time_Created >= bap.time_start "
			    								+ "and bu.Time_Created < bap.time_end "
			    								+ "and bu.Writer_Id in (" + Writer.WRITER_ID_AUTO + "," + Writer.WRITER_ID_WEB + "," +
			    															Writer.WRITER_ID_MOBILE + "," + Writer.WRITER_ID_COPYOP_WEB + "," + 
		    																Writer.WRITER_ID_COPYOP_MOBILE + "," + Writer.WRITER_ID_AO_MINISITE + "," +
			    															Writer.WRITER_ID_CO_MINISITE + ") "
		    									+ "AND bu.User_Id = l.User_Id "
			    			+ " )" +
			    		 " ORDER BY " +
			    		 	" bap.combination_id desc, " +
			    			" bap.campaign_id desc, " +
			    			" bap.skin_id desc, " +
			    			" bap.business_skin_id desc, " +
			    			" bap.country_id desc, " +
			    			" bap.affiliate_key desc ";

			ps = con.prepareStatement(sql);
			ps.setLong(1, loginId);
			ps.setLong(2, ConstantsBase.BONUS_AUTO_PARAMETER_TYPE_LOGIN);

			rs = ps.executeQuery();

			if (rs.next()) {
				bonusId = rs.getLong("bonus_id");
			}
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		return bonusId;
	}

	public static long getNIOUBonusForReverse(Connection con, long investmentId) throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		try	{
			String sql = "select bu.id "
						+ "from bonus_users bu, investments i "
						+ "where bu.id = i.bonus_user_id and bu.type_id in (" + Bonus.TYPE_INSTANT_NEXT_INVEST_ON_US + ","
																				+ Bonus.TYPE_NEXT_INVEST_ON_US_AFTER_WAGERING + ","
																				+ Bonus.TYPE_NEXT_INVEST_ON_US_AFTER_X_INVS + ") "
							+ "and i.id = ?";
			ps = con.prepareStatement(sql);
			ps.setLong(1, investmentId);
			rs = ps.executeQuery();
			if (rs.next()) {
				return rs.getLong("id");
			}
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}

		return 0l;
	}

	public static boolean reverseNIOUBonus(Connection con, long buId) throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			String sql = "update bonus_users "
						+ "set bonus_state_id = " + Bonus.STATE_ACTIVE + ", "
								+ "time_used = null, "
								+ "time_done = null, "
								+ "wagering_end_investment_id = null "
						+ "where id = ?";
			ps = con.prepareStatement(sql);
			ps.setLong(1, buId);
			return ps.executeUpdate() > 0 ? true : false;
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
	}
	
	 /**
     * Check if user can withdraw.
     * User cannot withdraw in case : 1. have bonuses with used state.
     * 								  2. have bonuses with active state and amountForWithdraw > (balance - sumActiveAmount)
     * @param userId user id
     * @param con db connection
     * @return true only if user dont have any bonus in state used else false
     * @throws SQLException
     */
    public static boolean userCanWithdraw(Connection con, long userId, long balance, long amount) throws SQLException {
    	boolean canWithdraw = true;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
	    long sumWageringAmount = 0;

	    try{
		    String sql =
		            " SELECT " +
		            	" bu.*," +
		            	" bt.class_type_id, " +
		            	" i.win, " +
		            	" i.lose, " +
		            	" i.amount inv_amount, " +
		            	" i.is_settled " +
		            " FROM " +
		            	" bonus_types bt, " +
		            	" bonus_users bu " +
		            		" left join investments i on i.bonus_user_id = bu.id " +
		            " WHERE " +
		            	" bu.type_id = bt.id " +
		        		" AND bu.user_id = ? ";

		    pstmt = con.prepareStatement(sql);
		    pstmt.setLong(1, userId);
		    rs = pstmt.executeQuery();

			while (rs.next()) {
				BonusUsers bu = getBonusUsersVO(rs, con);

				if (ConstantsBase.BONUS_CLASS_TYPE_NEXT_INVEST_ON_US == rs.getLong("class_type_id")) {

					if (bu.getBonusStateId() == ConstantsBase.BONUS_STATE_USED && rs.getInt("IS_SETTLED") == 1) {
						BonusHandlerBase bh = BonusHandlerFactory.getInstance(bu.getTypeId());
						long win = rs.getLong("win");
						long lose = rs.getLong("lose");
						long invAmount = rs.getLong("inv_amount");

						sumWageringAmount += bh.getAmountThatUserCantWithdraw(win, lose, invAmount, bu);
					}

				} else {
					if (bu.getBonusStateId() == ConstantsBase.BONUS_STATE_USED) {
						canWithdraw = false;
						break;
					} else if (bu.getBonusStateId() == ConstantsBase.BONUS_STATE_ACTIVE) {
						sumWageringAmount += bu.getBonusAmount();
					}
				}
			}

		    if (canWithdraw && sumWageringAmount > 0) {  // have active bonuses and don't have used bonuses
		    	if (amount > (balance - sumWageringAmount) ) {
		    		canWithdraw = false;
		    	}
		    }

		} finally {
		   closeStatement(pstmt);
		   closeResultSet(rs);
		}
		return canWithdraw;
	}
}