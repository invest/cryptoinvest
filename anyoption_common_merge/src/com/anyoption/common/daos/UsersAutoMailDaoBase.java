package com.anyoption.common.daos;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;

import com.anyoption.common.managers.CurrencyRatesManagerBase;

public class UsersAutoMailDaoBase extends DAOBase {	
	public static void notifeReachedTotalDeposit(Connection conn, long userId) throws SQLException {
		CallableStatement cstmt = null;
		int index = 1;		
		try {
			double currEurRate = CurrencyRatesManagerBase.getEurRate();
			cstmt = conn.prepareCall("{call PKG_USERS_AUTO_MAIL.NOTIF_REACHED_TOTAL_DEPOSIT(   I_USER_ID => ? " +
																							" ,I_RATE_USD_EUR => ? )}"); 																					   
			setStatementValue(userId, index++, cstmt);
			setStatementValue(currEurRate, index++, cstmt);
			cstmt.execute();
		} finally {
			closeStatement(cstmt);
		}
	}
}