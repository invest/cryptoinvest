package com.anyoption.common.daos;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.anyoption.common.beans.RewardTier;
/**
 * @author liors
 *
 */
public class RewardTiersDAO extends DAOBase {
	public static RewardTier getVO(ResultSet rs) throws SQLException {
		RewardTier valueObject = new RewardTier();
		valueObject.setId(rs.getInt("id"));
		valueObject.setDaysToDemote(rs.getInt("days_to_demote"));
		valueObject.setExperiencePointsToReduce(rs.getInt("exp_points_to_reduce"));
		valueObject.setInActiveDays(rs.getInt("in_active_days"));
		valueObject.setMinExperiencePoints(rs.getInt("min_exp_points"));
		valueObject.setName(rs.getString("name"));
		return valueObject;
	}
}
