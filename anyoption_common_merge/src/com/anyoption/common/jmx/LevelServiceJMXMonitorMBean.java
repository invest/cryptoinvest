package com.anyoption.common.jmx;

import com.anyoption.common.jmx.base.JMXBaseMBean;

public interface LevelServiceJMXMonitorMBean extends JMXBaseMBean {
	
	public String getLevelServiceStatus();

}
