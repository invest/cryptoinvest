package com.anyoption.common.jmx;

import java.lang.management.ManagementFactory;

import javax.management.MBeanServer;
import javax.management.ObjectName;

import org.apache.log4j.Logger;

import com.anyoption.common.util.AssetIndexMarketCacheJson;

public class JMXCacheCleanPartsJson implements JMXCacheCleanPartsJsonMBean {
	public static final Logger log = Logger.getLogger(JMXCacheCleanPartsJson.class);
	
	public static final String JSON_CLEAR_CLEAN_PART_JMX_OBJECT_NAME_AO	= "com.anyoption.common.jmx:type=jsonCleanCache,name=AOCleanCache";
	public static final String JSON_CLEAR_CLEAN_PART_JMX_OBJECT_NAME_ET	= "com.anyoption.common.jmx:type=jsonCleanCache,name=ETCleanCache";

	protected String jmxName;

	public boolean initSkinAssetIndexMarketsJson() {
		try {
			//AssetIndexMarketCacheJson.init();			
			return true;
		} catch (Exception e) {
			log.debug("Can't init AssetIndexMarkets!!!", e);
			return false;
		}
	}
	/**
     * Register the instance in the platform MBean server under given name.
     *
     * @param name the name under which to register
     */
    public void registerJMX(String name) {
        jmxName = name;
        try {
            MBeanServer mbs = ManagementFactory.getPlatformMBeanServer();
            ObjectName on = new ObjectName(jmxName);
            mbs.registerMBean(this, on);
            if (log.isInfoEnabled()) {
                log.info("JMXCacheClean MBean registered - name: " + jmxName);
            }
        } catch (Exception e) {
            log.error("Failed to register in the MBean server.", e);
        }
    }

    /**
     * Unregister the instance from the platform MBean server. Use the name
     * under which it was registered.
     */
    public void unregisterJMX() {
        try {
            MBeanServer mbs = ManagementFactory.getPlatformMBeanServer();
            ObjectName on = new ObjectName(jmxName);
            mbs.unregisterMBean(on);
            if (log.isInfoEnabled()) {
                log.info("JMXCacheClean MBean unregistered - name: " + jmxName);
            }
        } catch (Exception e) {
            log.error("Failed to unregister from the MBean server.", e);
        }
    }
}
