package com.anyoption.common.jmx.base;

import java.lang.management.ManagementFactory;

import javax.management.MBeanServer;
import javax.management.ObjectName;

import org.apache.log4j.Logger;

public class JMXBase implements JMXBaseMBean {
	public static final Logger log = Logger.getLogger(JMXBase.class);
	
	public static final String OK = "ok";
	public static final String NOK = "Nok";
	public static final String DISABLED = "Disabled";
	protected String jmxName;
	protected String suffix;
	private boolean enabled = true;
	
	protected JMXBase(String jmxName, String jmxSuffix) {
		if(jmxSuffix == null) {
			jmxSuffix = "";
		}
		this.jmxName = jmxName + "_"+ jmxSuffix;
		this.suffix = jmxSuffix;
	}
	
	/**
     * Register the instance in the platform MBean server under given name.
     *
     * @param name the name under which to register
     */
    public void registerJMX() {
        try {
			ObjectName on = new ObjectName(jmxName);
            MBeanServer mbs = ManagementFactory.getPlatformMBeanServer();
			if(mbs.isRegistered(on)) {
				log.info("JMX MBean already registered : " + jmxName);
				return;
			}        	
            mbs.registerMBean(this, on);
            if (log.isInfoEnabled()) {
                log.info("JMX MBean registered - name: " + jmxName);
            }
        } catch (Exception e) {
            log.error("Failed to register in the MBean server.", e);
        }
    }

    /**
     * Unregister the instance from the platform MBean server. Use the name
     * under which it was registered.
     */
    public void unregisterJMX() {
        try {
            MBeanServer mbs = ManagementFactory.getPlatformMBeanServer();
            ObjectName on = new ObjectName(jmxName);
            mbs.unregisterMBean(on);
            if (log.isInfoEnabled()) {
                log.info("JMX MBean unregistered - name: " + jmxName);
            }
        } catch (Exception e) {
            log.error("Failed to unregister from the MBean server.", e);
        }
    }

	public String getJmxName() {
		return jmxName;
	}

	@Override
	public void enable() {
		enabled = true;
	}

	@Override
	public void disable() {
		enabled = false;
	}

	@Override
	public boolean isEnabled() {
		return enabled;
	}

	public String getSuffix() {
		return suffix;
	}

}
