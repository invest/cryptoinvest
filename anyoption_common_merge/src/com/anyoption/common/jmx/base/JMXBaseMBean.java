package com.anyoption.common.jmx.base;

public interface JMXBaseMBean {
	public void enable();
	public void disable();	
	public boolean isEnabled();
}
