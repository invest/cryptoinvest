package com.anyoption.common.jmx;

import com.anyoption.common.enums.SkinGroup;
import com.anyoption.common.jms.WebLevelLookupBean;
import com.anyoption.common.jms.WebLevelsCache;
import com.anyoption.common.jmx.base.JMXBase;

public class LevelServiceJMXMonitor extends JMXBase implements LevelServiceJMXMonitorMBean {

	static final String jmxName = "com.anyoption.common.jmx:type=LevelServiceJMXMonitor,name=LevelServiceJMXMonitor";
	static WebLevelsCache  wlc;

	public LevelServiceJMXMonitor(WebLevelsCache  wlc, String suffix) {
		super(jmxName, suffix);
		LevelServiceJMXMonitor.wlc = wlc;
	}

	public String getLevelServiceStatus() {
		if(!isEnabled()) {
			return DISABLED;
		}
		WebLevelLookupBean wllb = new WebLevelLookupBean();
        wllb.setOppId(-1);
        wllb.setFeedName("WLCCHECK");
        wllb.setSkinGroup(SkinGroup.ETRADER); // HARDCODED to avoid NPE
        wlc.getLevels(wllb);
        if (wllb.getDevCheckLevel() != 0 && wllb.getRealLevel() != 0) {
            log.debug("Wlc check: ok");
            return OK;
        } else {
            log.debug("Wlc check: not ok" + wllb);
            return NOK;
        }
	}
}
