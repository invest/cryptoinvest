package com.anyoption.common.jmx;

import com.anyoption.common.jmx.base.JMXBaseMBean;

public interface DbJMXMonitorMBean extends JMXBaseMBean {

	public String getDBConnectionStatus();
	
	public String getDGSyncStatus();

}
