package com.anyoption.common.jmx;

import java.sql.Connection;
import java.util.concurrent.TimeUnit;

import javax.sql.DataSource;

import org.apache.log4j.Logger;

import com.anyoption.common.daos.GeneralDAO;
import com.anyoption.common.jmx.base.JMXBase;


public class DbJMXMonitor extends JMXBase implements DbJMXMonitorMBean {

	final static String jmxName = "com.anyoption.common.jmx:type=DbJMXMonitor,name=DbJMXMonitor";
	public static final Logger log = Logger.getLogger(DbJMXMonitor.class);
	DataSource dataSource;
	
	public DbJMXMonitor(DataSource dataSource, String jmxSuffix) {
		super(jmxName, jmxSuffix);
		this.dataSource = dataSource;
	}
	
	public String getDBConnectionStatus() {
		if(!isEnabled()) {
			return DISABLED;
		}
		Connection con = null;
		long timeBefore = 0;
		long timeAfter = 0;

		try {
			 timeBefore = System.nanoTime();
			 con = dataSource.getConnection();
			 GeneralDAO.getTestQuery(con);
			 timeAfter = System.nanoTime();
			 log.debug("Db check: ok, time: " + String.valueOf(TimeUnit.NANOSECONDS.toMillis(timeAfter-timeBefore)) + "ms");
			 return OK;
		} catch (Exception e) {
			log.debug("Db check: NOT ok, time: " + String.valueOf(TimeUnit.NANOSECONDS.toMillis(timeAfter-timeBefore)) + "ms", e);
			return e.getClass().getName();
		} finally {
			try {
				con.close();
			} catch (Exception e){ /* ignore */ }
		}
	}
	
	public String getDGSyncStatus () {
		if(!isEnabled()) {
			return DISABLED;
		}
		Connection con = null;

		try {
			 con = dataSource.getConnection();
			 if (GeneralDAO.getDGSyncQuery(con)) {
				 log.debug("DataGuard sync: ok");
				 return OK;
			 } 
			 else {
				 log.debug("DataGuard sync: not ok");
				 return NOK;
			 }
		} catch (Exception e) {
			log.debug("DataGuard sync: not ok", e);
			return e.getClass().getName();
		} finally {
			try {
				con.close();
			} catch (Exception e){ /* ignore */ }
		}
	}
	
}