/**
 * 
 */
package com.anyoption.common.enums;

import java.util.HashMap;

/**
 * Enum type that defines the status of the country in a country bean. Not blocked means that the country isn't blocked; Rejected means that
 * the country will be rejected on registration submit, but is present in drop downs; Blocked means that the country is not present anywhere
 * and we do not accept registrants from it
 * 
 * @author kirilim
 */
public enum CountryStatus {
	NOTBLOCKED(0l), REJECTED(1l), BLOCKED(2l), NON_REG_NOTBLOCKED(3l);

	private final long id;
	private static final HashMap<Long, CountryStatus> idStatusMap = new HashMap<Long, CountryStatus>(CountryStatus.values().length);
	static {
		for (CountryStatus family : CountryStatus.values()) {
			idStatusMap.put(family.getId(), family);
		}
	}

	CountryStatus(long id) {
		this.id = id;
	}

	public long getId() {
		return id;
	}

	public static CountryStatus getById(long id) {
		CountryStatus family = idStatusMap.get(id);
		if (family == null) {
			throw new IllegalArgumentException("No country status with ID: " + id);
		} else {
			return family;
		}
	}
}