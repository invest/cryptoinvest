package com.anyoption.common.enums;
import java.util.HashMap;

public enum AssetIndexGroupTypeByName {
	INDICES(1l), CURRENCY_PAIRS(2l), COMMODITIES(3l), STOCKS(4l), OPTION_PL(5l), BINARY_0_100(6l);

	private final long id;
	private static final HashMap<Long, AssetIndexGroupTypeByName> idGroupMap = new HashMap<Long, AssetIndexGroupTypeByName>(AssetIndexGroupTypeByName.values().length);
	static {
		for (AssetIndexGroupTypeByName family : AssetIndexGroupTypeByName.values()) {
			idGroupMap.put(family.getId(), family);
		}
	}
	
	AssetIndexGroupTypeByName(long id) {
		this.id = id;
	}

	public long getId() {
		return id;
	}

	public static AssetIndexGroupTypeByName getById(long id) {
		AssetIndexGroupTypeByName family = idGroupMap.get(id);
		if (family == null) {
			throw new IllegalArgumentException("No asset Index Group with ID: " + id);
		} else {
			return family;
		}
	}
}