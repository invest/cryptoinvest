/**
 * 
 */
package com.anyoption.common.enums;

import java.util.HashMap;

/**
 * @author kirilim
 *
 */
public enum DeviceFamily {
	NONMOBILE(-1l), PHONE(0l), TABLET(1l);

	private final long id;
	private static final HashMap<Long, DeviceFamily> ID_FAMILY_MAP = new HashMap<Long, DeviceFamily>(DeviceFamily.values().length);
	static {
		for (DeviceFamily family : DeviceFamily.values()) {
			ID_FAMILY_MAP.put(family.getId(), family);
		}
	}

	DeviceFamily(long id) {
		this.id = id;
	}

	public long getId() {
		return id;
	}

	public static DeviceFamily getById(long id) {
		DeviceFamily family = ID_FAMILY_MAP.get(id);
		if (family == null) {
			throw new IllegalArgumentException("No device family with ID: " + id);
		} else {
			return family;
		}
	}
}