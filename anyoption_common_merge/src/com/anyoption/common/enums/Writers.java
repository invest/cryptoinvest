package com.anyoption.common.enums;

import java.util.HashMap;
import java.util.Map;
import com.anyoption.common.beans.Writer;

/**
 * @author liors
 *
 */
public enum Writers {
	DEFAULT(-1,"BE"),
	AUTO (Writer.WRITER_ID_AUTO, "AUTO"),
	WEB (Writer.WRITER_ID_WEB, "WEB"),
	MOBILE (Writer.WRITER_ID_MOBILE, "MOBILE"),
	API (Writer.WRITER_ID_API, "API"),
	COPYOP_WEB(Writer.WRITER_ID_COPYOP_WEB, "COPYOP_WEB"),
	COPYOP_MOBILE(Writer.WRITER_ID_COPYOP_MOBILE, "COPYOP_MOBILE"),
	BUBBLES(Writer.WRITER_ID_BUBBLES_WEB, "BUBBLES");
	
	private static final Map<Integer, Writers> MAP_ID_NAME = new HashMap<Integer, Writers>(Writers.values().length);
	static {
		for (Writers writer : Writers.values()) {
			MAP_ID_NAME.put(writer.getId(), writer);
		}
	}
	
	private int id;
    private String name;
	
	private Writers(int id, String name) { 
		this.id = id;
		this.name = name; 
	}  
	
	/**
	 * @param id
	 * @return
	 */
	public static Writers getWriterName(int id) {
		Writers writer = MAP_ID_NAME.get(id);
		if (writer == null) {
			writer = DEFAULT;
		} 
		return writer;
    }

	public int getId() {
		return id;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
}
