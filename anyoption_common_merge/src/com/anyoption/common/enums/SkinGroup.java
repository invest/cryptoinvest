package com.anyoption.common.enums;

import java.util.EnumSet;
import java.util.HashMap;
import java.util.Map;

public enum SkinGroup {
	AUTO(0L), ETRADER(1L), ANYOPTION(2L), ZH(3L), TR(4L);
	
	public static final EnumSet<SkinGroup> NON_AUTO_SET = EnumSet.complementOf(EnumSet.of(AUTO));
	
	private static final Map<Long, SkinGroup> ID_TO_GROUP_MAP = new HashMap<Long, SkinGroup>(SkinGroup.values().length);
	static {
		for (SkinGroup group : SkinGroup.values()) {
			ID_TO_GROUP_MAP.put(group.getId(), group);
		}
	}
	
	private long id;
	
	private String levelUpdateKey;
	private String colorUpdateKey;
	private String insCurrentLevelUpdateKey;
	private String tTLevelUpdateKey;
	private String tTShiftParameterUpdateKey;
	private String tTMaxExposureUpdateKey;
	
	SkinGroup(long id) {
		this.id = id;
		this.levelUpdateKey = "LEVEL_" + id;
		this.colorUpdateKey = "CLR_" + id;
		this.insCurrentLevelUpdateKey = "INS_CRR_LEVEL_" + id;
		this.tTLevelUpdateKey = "level_" + id;
		this.tTShiftParameterUpdateKey = "shiftParameter_" + id;
		this.tTMaxExposureUpdateKey = "maxExposure_" + id;
	}
	
	/**
	 * @return the skin group ID
	 */
	public long getId() {
		return id;
	}
	
	/**
	 * @return the levelUpdateKey
	 */
	public String getLevelUpdateKey() {
		return levelUpdateKey;
	}

	/**
	 * @return the colorUpdateKey
	 */
	public String getColorUpdateKey() {
		return colorUpdateKey;
	}

	/**
	 * @return the insCurrentLevelUpdateKey
	 */
	public String getInsCurrentLevelUpdateKey() {
		return insCurrentLevelUpdateKey;
	}

	/**
	 * @return the tTLevelUpdateKey
	 */
	public String getTTLevelUpdateKey() {
		return tTLevelUpdateKey;
	}

	/**
	 * @return the tTShiftParameterUpdateKey
	 */
	public String getTTShiftParameterUpdateKey() {
		return tTShiftParameterUpdateKey;
	}

	/**
	 * @return the tTMaxExposureUpdateKey
	 */
	public String getTTMaxExposureUpdateKey() {
		return tTMaxExposureUpdateKey;
	}

	public static SkinGroup getById(long id) {
		SkinGroup group = ID_TO_GROUP_MAP.get(id);
		if (group == null) {
			throw new IllegalArgumentException("No skin group with ID: " + id);
		} else {
			return group;
		}
	}
	
}