package com.anyoption.common.encryption;

import java.security.MessageDigest;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.binary.Base64;

/**
 * @author liors
 *
 */
public abstract class SecureAlgorithms {
	
	/**
	 * convert byte array to Hex String, second implementation option
	 * @param data
	 * @return
	 */
    private static String toHexString(byte[] data) {
        StringBuffer sb = new StringBuffer();
        String s = null;
        for (int i = 0; i < data.length; i++) {
            s = Integer.toHexString(data[i] & 0x000000FF);
            if (s.length() == 1) {
                sb.append("0");
            }
            sb.append(s);
        }
        return sb.toString();
    }
    
    /**
     * use UTF-8.
     * 
     * @param algorithm
     * @param text
     * @return
     * @throws Exception
     */
    private static String createHash(String algorithm, String text) throws Exception {
		MessageDigest md = MessageDigest.getInstance(algorithm);
		md.update(text.getBytes("UTF-8"), 0, text.length());
		return toHexString(md.digest());
    }
    
    /**
     * SHA is a cryptographic message digest algorithm similar to MD5. SHA-1 hash considered to be one
     * of the most secure hashing functions, producing a 160-bit digest (40 hex numbers) from any data
     * with a maximum size of 264 bits
     */
    public static class Sha1 {
    	
        public static String encode(String text) throws Exception {
        	return createHash("SHA-1", text);
        }
    }
    
    /**
     * SHA-256 and SHA-512 are novel hash functions computed with 32-bit and 64-bit words, respectively. 
     * They use different shift amounts and additive constants, but their structures are otherwise virtually identical, differing only in the number of rounds.
     */
    public static class Sha256 {
    	
        public static String encode(String text) throws Exception {
    		return createHash("SHA-256", text);
        }
    }

	/**
	 * SHA-256 and SHA-512 are novel hash functions computed with 32-bit and 64-bit words, respectively. 
	 * They use different shift amounts and additive constants, but their structures are otherwise virtually identical, differing only in the number of rounds.
	 */
	public static class Sha512 {
		
	    public static String encode(String text) throws Exception {
			return createHash("SHA-512", text);
	    }
	}
	
	/**
	 * The MD5 message-digest algorithm is a widely used cryptographic hash function producing a 128-bit (16-byte) hash value,
	 * typically expressed in text format as a 32 digit hexadecimal number. 
	 */
	public static class MD5 {
		
	    public static String encode(String text) throws Exception {
			return createHash("MD5", text);
	    }
	}
	
	/**
	 * :)
	 */
	public static class LShashStyle {
		
		public static int encode(String text) throws Exception {
			char[] chars = text.toCharArray();
			int hashCode = 0;
			
			for (char c : chars) {
				hashCode <<= 7;
			    hashCode |= c & 0x7F;
			    hashCode = (hashCode << 1) | (hashCode >> (32 - 1));
			    hashCode ^= c;
			    hashCode = 31 * hashCode + c;
			}
			return -1*hashCode;
	    }		
	}
	
	/**
     * Encrypts using AES algorithm
     *
     * @param input
     * @param key
     * @return Base64 encodeBase64String crypted
    */
    public static String encryptAES(String input, String key) throws Exception {
    		byte[] crypted = null;
			SecretKeySpec skey = new SecretKeySpec(key.getBytes(), "AES");
			Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
			cipher.init(Cipher.ENCRYPT_MODE, skey);
			crypted = cipher.doFinal(input.getBytes());
			return Base64.encodeBase64String(crypted);
    }
}

