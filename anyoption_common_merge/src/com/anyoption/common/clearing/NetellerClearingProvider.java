package com.anyoption.common.clearing;

import java.util.Base64;

import org.apache.log4j.Logger;
import org.scribe.model.OAuthRequest;
import org.scribe.model.Response;
import org.scribe.model.Verb;

import com.anyoption.common.clearing.neteller.AccountProfile;
import com.anyoption.common.clearing.neteller.Errors;
import com.anyoption.common.clearing.neteller.Payment;
import com.anyoption.common.clearing.neteller.PaymentMethod;
import com.anyoption.common.clearing.neteller.TokenResponse;
import com.anyoption.common.clearing.neteller.Transaction;
import com.anyoption.common.clearing.neteller.TransferIn;
import com.anyoption.common.clearing.neteller.TransferOut;
import com.anyoption.json.util.JsonExclusionStrategy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class NetellerClearingProvider extends ClearingProvider {
	private static final Logger log = Logger.getLogger(NetellerClearingProvider.class);
	
	String apiSecret = null;
		
	@Override
	public void setConfig(ClearingProviderConfig config) throws ClearingException {
		super.setConfig(config);

        if (null != config.getProps() && !config.getProps().trim().equals("")) {
            String[] ps = config.getProps().split(";");
            for (int i = 0; i < ps.length; i++) {
                String[] p = ps[i].split("=");
                if (p[0].equals("apiSecret")) {
                	apiSecret = p[1];
                }
            }
        }

	}
	
	@Override
	public void authorize(ClearingInfo info) throws ClearingException {
		log.error("authorize is not supported opperation");
		throw new ClearingException("Unsupported operation.");
		
	}
	
	@Override
	public void capture(ClearingInfo info) throws ClearingException {
		log.error("capture is not supported opperation");
		throw new ClearingException("Unsupported operation.");
	}
	
	@Override
	public void cancel(ClearingInfo info) throws ClearingException {
		log.error("cancel is not supported opperation");
		throw new ClearingException("Unsupported operation.");
		
	}
	
	@Override
	public void enroll(ClearingInfo info) throws ClearingException {
		log.error("enroll is not supported opperation");
		throw new ClearingException("Unsupported operation.");
		
	}
	
	@Override
	public void purchase(ClearingInfo info) throws ClearingException {
		log.error("purchase is not supported opperation");
		throw new ClearingException("Unsupported operation.");
		
	}
	
	@Override
	public void withdraw(ClearingInfo info) throws ClearingException {
		log.error("withdraw is not supported opperation");
		throw new ClearingException("Unsupported operation.");
		
	}
	
	@Override
	public void bookback(ClearingInfo info) throws ClearingException {
		log.error("bookback is not supported opperation");
		throw new ClearingException("Unsupported operation.");
	}
	
	
	public void deposit(ClearingInfo info, String currency, String email, String verificationCode) {
		Gson gson = new GsonBuilder().setExclusionStrategies(new JsonExclusionStrategy()).create();
		
		OAuthRequest tokenRequest = new OAuthRequest(Verb.POST, url+"oauth2/token?grant_type=client_credentials");
		tokenRequest.addHeader("Authorization", "Basic " + Base64.getEncoder().encodeToString((username+":"+apiSecret).getBytes()));
		tokenRequest.addHeader("Content-Type", "application/json");
		tokenRequest.addHeader("Cache-Control", "no-cache");

		Response response = tokenRequest.send();
		TokenResponse token = gson.fromJson(response.getBody(), TokenResponse.class);

		OAuthRequest depositRequest = new OAuthRequest(Verb.POST, url+"transferIn");
		depositRequest.addHeader("Authorization", "Bearer " + token.getAccessToken());
		depositRequest.addHeader("Content-Type", "application/json");
		depositRequest.addPayload(generateDepositJson(gson, currency, info.getAmount(), info.getTransactionId(), email, verificationCode));
		
		Response depositResponse = depositRequest.send();
		log.info(depositResponse.getBody());
		Payment payment = gson.fromJson(depositResponse.getBody(), Payment.class);
		
		log.info(payment);
		if(payment.getTransaction() != null) {
			if(Transaction.STATUS_ACCEPTED.equals(payment.getTransaction().getStatus())) {
				info.setSuccessful(true);
				info.setMessage("Successful");
				info.setAuthNumber("" + payment.getTransaction().getId());
			} else {
				info.setSuccessful(false);
				info.setMessage("Failed");
			}
		} else {
			Errors errors = gson.fromJson(depositResponse.getBody(), Errors.class);
			info.setSuccessful(false);
			info.setMessage(errors.getError().toString());
		}
	}
	
	public void tranfer(ClearingInfo info, String currency, String email, String verificationCode) {
		Gson gson = new GsonBuilder().setExclusionStrategies(new JsonExclusionStrategy()).create();
		
		OAuthRequest tokenRequest = new OAuthRequest(Verb.POST, url+"oauth2/token?grant_type=client_credentials");
		tokenRequest.addHeader("Authorization", "Basic " + Base64.getEncoder().encodeToString((username+":"+apiSecret).getBytes()));
		tokenRequest.addHeader("Content-Type", "application/json");
		tokenRequest.addHeader("Cache-Control", "no-cache");

		Response response = tokenRequest.send();
		TokenResponse token = gson.fromJson(response.getBody(), TokenResponse.class);

		OAuthRequest withdrawRequest = new OAuthRequest(Verb.POST, url+"transferOut");
		withdrawRequest.addHeader("Authorization", "Bearer " + token.getAccessToken());
		withdrawRequest.addHeader("Content-Type", "application/json");
		withdrawRequest.addPayload(generateWithdrawJson(gson, email, currency, info.getAmount(), info.getTransactionId()));
		
		Response withdrawResponse = withdrawRequest.send();
		log.info(withdrawResponse.getBody());
		Payment payment = gson.fromJson(withdrawResponse.getBody(), Payment.class);
		
		log.info(payment);
		if(payment.getTransaction() != null) {
			if(Transaction.STATUS_ACCEPTED.equals(payment.getTransaction().getStatus())) {
				info.setSuccessful(true);
				info.setMessage("Successful");
				info.setAuthNumber("" + payment.getTransaction().getId());
			} else {
				info.setSuccessful(false);
				info.setMessage("Failed");
			}
		} else {
			Errors errors = gson.fromJson(withdrawResponse.getBody(), Errors.class);
			info.setSuccessful(false);
			info.setMessage(errors.getError().toString());
		}
	}
	
	private String generateWithdrawJson(Gson gson, String email, String currency, long amount, long transactionId) {
		AccountProfile payeeProfile = new AccountProfile();
		payeeProfile.setEmail(email);
		
		Transaction transaction = new Transaction();
		transaction.setAmount(amount);
		transaction.setCurrency(currency); 
		transaction.setMerchantRefId(String.valueOf(transactionId));
		TransferOut transferOut = new TransferOut();
		transferOut.setPayeeProfile(payeeProfile);
		transferOut.setTransaction(transaction);
		transferOut.setMessage("N/A");
		
		String json = gson.toJson(transferOut);

		return json;
	}

	private String generateDepositJson(Gson gson, String currency, long amount, long transactionId, String email, String verificationCode) {
		PaymentMethod paymentMethod = new PaymentMethod();
		paymentMethod.setType("neteller");
		paymentMethod.setValue(email);
		
		Transaction transaction = new Transaction();
		transaction.setAmount(amount);
		transaction.setCurrency(currency); 
		transaction.setMerchantRefId(String.valueOf(transactionId));
		
		TransferIn transferIn = new TransferIn();
		transferIn.setPaymentMethod(paymentMethod);
		transferIn.setTransaction(transaction);
		transferIn.setVerificationCode(verificationCode);
		
		String json = gson.toJson(transferIn);

		return json;
	}
}