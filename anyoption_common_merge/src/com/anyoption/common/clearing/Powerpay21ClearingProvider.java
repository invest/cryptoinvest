package com.anyoption.common.clearing;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

import org.apache.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;

import com.anyoption.common.managers.CountryManagerBase;
import com.anyoption.common.util.ClearingUtil;
import com.anyoption.common.util.CommonUtil;
import com.anyoption.common.util.Sha1;

public class Powerpay21ClearingProvider extends ClearingProvider {
	private static final Logger log = Logger.getLogger(Powerpay21ClearingProvider.class);
	
	/**
	 * paymentMethod 1 - All credit card types: MasterCard, VISA, Maestro, JCB (except AmEx)
	 * 
	 * 
	 * paymentMethod 5 AND custom2 field determines which APS will be selected:
	 * ideal, giropay, eps
	 * p24, przelewy24, directpay,  trustpay,
	 * sepadirectdebit, skrill, paysafecard, safetypay
	 */
	private int paymentMethod = 1;
	
	private static final int PAYMENT_METHOD_APS		= 5;
	private String custom2;
	private boolean demo = false;
	
	private String url_failed;
	private String url_success;
	private String url_internal;
	
	private String authorizeUrlSuffix	= "payment_preauthorize";
	private String captureUrlSuffix		= "payment_capture";
	private String cancelUrlSuffix		= "payment_reversal";
	private String cftUrlSuffix			= "payment_cft";
	private String cftUrlRefundSuffix	= "payment_refund";
	
	private String diagnoseSuffix		= "tx_diagnose";
	
	/**
	 * parses configuration from clearing_providers data
	 */
	@Override
	public void setConfig(ClearingProviderConfig config) throws ClearingException {
		super.setConfig(config);
        try {
	        if (null != config.getProps() && !config.getProps().trim().equals("")) {
	            String[] ps = config.getProps().split(";");
	            for (int i = 0; i < ps.length; i++) {
	                String[] p = ps[i].split("=");
	                if (p[0].equals("paymentMethod")) {
	                	paymentMethod = Integer.parseInt(p[1]);
	                } else if (p[0].equals("authorizeUrlSuffix")) {
	                	authorizeUrlSuffix = p[1];
	                }  else if (p[0].equals("captureUrlSuffix")) {
	                	captureUrlSuffix = p[1];
	                }  else if (p[0].equals("custom2")) {
	                	custom2 = p[1];
	                } else if (p[0].equals("demo")) {
	                	demo = "true".equals(p[1]);
	                } else if (p[0].equals("url_failed")) {
	                	url_failed = p[1];
	                } else if (p[0].equals("url_success")) {
	                	url_success = p[1];
	                } else if (p[0].equals("url_internal")) {
	                	url_internal = p[1];
	                }
	            }
	        }
        } catch (Exception e) {
        	log.error("ERROR! set config - job?", e);
        }
	}
	
	/**
	 * A request to the Pre-Authorization interface will verify the credit card data,
	 *  credit line, and reserve the requested funds.
	 */
	@Override
	public void authorize(ClearingInfo info) throws ClearingException {
		Powerpay21Info p = new Powerpay21Info(privateKey);
		
		p.setMerchantId(depositTerminalId);
		p.setTransactionId(info.getProviderTransactionId());
		p.setAmount(new BigDecimal(info.getAmount()).divide(new BigDecimal(100)).intValue());
		
		p.setCardholderName(info.getOwner());
		p.setCcn(info.getCcn());
		p.setCity(info.getCity());
		p.setCountry(CountryManagerBase.getCountries().get(info.getCountryId()).getA3());
		p.setCompany("Anyoption");
		p.setCurrency(info.getCurrencySymbol());
		p.setCustomerIp(info.getIp());
		p.setCvcCode(info.getCvv());
		p.setEmail(info.getEmail());
		p.setExpMonth(info.getExpireMonth());
		p.setExpYear("20"+info.getExpireYear());
		p.setFirstname(info.getFirstName());
		p.setLastname(info.getLastName());

		p.setOrderId(String.valueOf(info.getTransactionId()));
		p.setPaymentMethod(paymentMethod);
		p.setStreet(info.getAddress());
		p.setZip(info.getZip());
		
		try {
			String response = ClearingUtil.executePOSTRequest(url + authorizeUrlSuffix, p.toPreauthPost());
			parseResponse(info, response, false);
		} catch (Exception e) {
			setFailed(info);
			log.error("Can't authorize:",e);
			throw new ClearingException("Authorize failed");
		} 
	}
	
	/**
	* The Capture request follows a successful Preauthorize request. The request will send an authorization request to the authorization system,
	*  which will book the amount previously reserved by the Preauthorize request and the customer’s credit card will then be charged.
	*/
	@Override
	public void capture(ClearingInfo info) throws ClearingException {
		Powerpay21Info p = new Powerpay21Info(privateKey);
		p.setMerchantId(depositTerminalId);
		p.setTransactionId(info.getProviderTransactionId());
		try {
			String response = ClearingUtil.executePOSTRequest(url + captureUrlSuffix, p.toCapturePost());
			parseResponse(info, response, false);
		} catch (Exception e) {
			setFailed(info);
			log.error("Can't capture:",e);
			throw new ClearingException("Capture failed");
		} 
	}

	/**
	 * It is possible to send a Reversal/Cancellation request for a successful Preauthorization/Order 
	 * request to free the fund reservation of the cardholder provided the transaction has not already been captured.
	 */
	@Override
	public void cancel(ClearingInfo info) throws ClearingException {
		Powerpay21Info p = new Powerpay21Info(privateKey);
		p.setMerchantId(depositTerminalId);
		p.setTransactionId(info.getProviderTransactionId());
		try {
			String response = ClearingUtil.executePOSTRequest(url + cancelUrlSuffix, p.toCancelPost());
			parseResponse(info, response, false);
		} catch (Exception e) {
			setFailed(info);
			log.error("Can't cancel:",e);
			throw new ClearingException("Cancel failed");
		} 
	}

	/**
	 * start 3d secure transaction
	 */
	@Override
	public void enroll(ClearingInfo info) throws ClearingException {
		throw new ClearingException("Not Supported");
	}

	/**
	 *  finish 3d Secure Transaction
	 */
	@Override
	public void purchase(ClearingInfo info) throws ClearingException {
		throw new ClearingException("Not Supported");
	}

	/**
	 * CFT – Credit Fund Transfer
	 */
	@Override
	public void withdraw(ClearingInfo info) throws ClearingException {
		Powerpay21Info p = new Powerpay21Info(privateKey);
		p.setMerchantId(depositTerminalId);
		p.setOrderId(String.valueOf(info.getTransactionId()));
		p.setAmount(new BigDecimal(info.getAmount()).divide(new BigDecimal(100)).intValue());
		p.setCurrency(info.getCurrencySymbol());		
		p.setPaymentMethod(paymentMethod);

		p.setCcn(info.getCcn());
		p.setExpMonth(info.getExpireMonth());
		p.setExpYear("20"+info.getExpireYear());
		p.setCardholderName(info.getOwner());		

		try {
			String response = ClearingUtil.executePOSTRequest(url + cftUrlSuffix, p.toCftPost());
			parseResponse(info, response, true);
		} catch (Exception e) {
			setFailed(info);
			log.error("Can't withdraw:",e);
			throw new ClearingException("Withdraw failed");			
		} 
	}
	
	/**
	 * The Refund request will send a credit note to the authorization
	 * system after the customer’s credit card has already been charged. 
	 */
	@Override
	public void bookback(ClearingInfo info) throws ClearingException {
		Powerpay21Info p = new Powerpay21Info(privateKey);
		p.setMerchantId(depositTerminalId);
		p.setTransactionId(info.getProviderDepositId());
		p.setCurrency(info.getCurrencySymbol());		
		p.setPrice(new BigDecimal(info.getAmount()).divide(new BigDecimal(100)).floatValue());
		p.setOrderId(info.getProviderDepositId());

		try {
			String response = ClearingUtil.executePOSTRequest(url + cftUrlRefundSuffix, p.toRefundPost());
			parseResponse(info, response, true);
		} catch (Exception e) {
			setFailed(info);
			log.error("Can't authorize:",e);
			throw new ClearingException("Authorization failed");
		} 
	}
	
	/*
	 * Hosted Page initialization request
	 */
	public ClearingInfo directSofortDeposit(String hpUrl, long amount, String iban, String bic, String accountName, long transactionId, String email) throws Exception {
		Powerpay21Info p = new Powerpay21Info(privateKey);
		p.setMerchantId(depositTerminalId);
		p.setOrderid(String.valueOf(transactionId));
		p.setCurrency("EUR");		
		p.setAmount(new BigDecimal(amount).divide(new BigDecimal(100)).intValue());
		p.setPayment_method(PAYMENT_METHOD_APS);
		p.setEmail(email);
		// Sofort related parameters
		p.setBankcountry("DE");
		p.setIban(iban);
		p.setBic(bic);
		p.setAccountname(accountName);
		p.setUrl_failed(hpUrl + url_failed);
		p.setUrl_success(hpUrl + url_success);
		p.setUrl_internal(url_internal);
		if(demo) {
			p.setCustom2("dumbdummy");
		} else {
			p.setCustom2("directpay");
		}

		String request = p.toSoforPost();
		
		String response = ClearingUtil.executePOSTRequest(url + authorizeUrlSuffix, request);
		return parseDirectResponse(response);
	}
	
	public ClearingInfo directEpsDeposit(String hpUrl, long amount, String accountName, long transactionId, String email) throws Exception {
		Powerpay21Info p = new Powerpay21Info(privateKey);
		p.setMerchantId(depositTerminalId);
		p.setOrderid(String.valueOf(transactionId));
		p.setCurrency("EUR");		
		p.setAmount(new BigDecimal(amount).divide(new BigDecimal(100)).intValue());
		p.setPayment_method(PAYMENT_METHOD_APS);
		p.setBankcountry("AT");
		p.setEmail(email);
		if(demo) {
			p.setCustom2("dumbdummy");
		} else {
			p.setCustom2("eps");
		}
		// EPS related parameters
		p.setAccountname(accountName);
		p.setUrl_failed(hpUrl + url_failed);
		p.setUrl_success(hpUrl + url_success);
		p.setUrl_internal(url_internal);
		String request = p.toEpsPost();
		
		String response = ClearingUtil.executePOSTRequest(url + authorizeUrlSuffix, request);
		return parseDirectResponse(response);
	}
	
	public ClearingInfo directGiropayDeposit(String hpUrl, long amount, String iban, String bic, String accountName, long transactionId, String email) throws Exception {
		Powerpay21Info p = new Powerpay21Info(privateKey);
		p.setMerchantId(depositTerminalId);
		p.setOrderid(String.valueOf(transactionId));
		p.setCurrency("EUR");		
		p.setAmount(new BigDecimal(amount).divide(new BigDecimal(100)).intValue());
		p.setPayment_method(PAYMENT_METHOD_APS);
		p.setEmail(email);
		if(demo) {
			p.setCustom2("dumbdummy");
		} else {
			p.setCustom2("giropay");
		}
		// Giropay related parameters
		p.setIban(iban);
		p.setBic(bic);
		p.setAccountname(accountName);
		p.setUrl_failed(hpUrl + url_failed);
		p.setUrl_success(hpUrl + url_success);
		p.setUrl_internal(url_internal);
		p.setBankcountry("DE");
		String request = p.toGiropayPost();
		
		String response = ClearingUtil.executePOSTRequest(url + authorizeUrlSuffix, request);
		return parseDirectResponse(response);
	}

	
	public void diagnose(InatecInfo info) throws ClearingException {
		Powerpay21Info p = new Powerpay21Info(privateKey);
		p.setMerchantId(depositTerminalId);
		p.setTransactionId(info.getAuthNumber());
		p.setType("tx");
		
		try{
			String request = p.toDiagnosePost();
			String response = ClearingUtil.executePOSTRequest(url + diagnoseSuffix, request);
			parseDiagnoseResponse(response, info);
		} catch (Throwable t ) {
			log.error("Error in diagnose:", t);
			setFailed(info);
		}
	}

	public boolean isSuccessSofort(Map<String, String> params, boolean isTest) {
		return ("credited".equalsIgnoreCase(params.get("payment_state")) || ( "not_credited_yet".equalsIgnoreCase(params.get("payment_state")) && isTest));
	}
	
//	public boolean isValidSofort(Map<String, String> p) {
//		TreeMap<String, String> params = new TreeMap<String, String>();
//		params.putAll(p);
//		
//		if(params.containsKey("payment_state") &&	// status of the request to the bank
//			params.containsKey("security") &&		// Signature : checksum for validation 
//			params.containsKey("transactionid") &&	// orderid in the provider system
//			params.containsKey("orderid") &&		// orderid in anyoption system
//			params.containsKey("errormessage"))		// errormessage	error messages of the gateway or empty
//			{
//				StringBuilder makeChecksum = new StringBuilder();
//				String check = params.remove("security");
//				
//				for(Map.Entry<String, String> entry : params.entrySet()) {
//					System.out.println(entry.getKey() + " " + entry.getValue());
//					makeChecksum.append(entry.getValue());
//				}
//				makeChecksum.append(this.privateKey);
//				System.out.println(makeChecksum);
//				String checksum = makeChecksum.toString();
//				try {
//					checksum = Sha1.encode(checksum);
//				} catch (Exception e) {
//					log.error("Can't create powerpay sha1", e);
//					return false;
//				}
//				return checksum.equals(check);
//		} else {
//			log.error("Not SOFORT call Incomplete parameters:"+params);
//			return false;
//		}
//	}
	
	public boolean isSuccessEPS(Map<String, String> pm) {
		return "SUCCEEDED".equals(pm.get("STATUS"));
	}
	
	public boolean isValidEps(Map<String, String> p) {
		TreeMap<String, String> params = new TreeMap<String, String>();
		params.putAll(p);
		
		if(params.containsKey("REQUESTSTATUS") && //SUCCEEDED, FAILED etc.	status of the request to the bank
				params.containsKey("TAG") && //giropay, ideal etc	payment system used for the transaction
				params.containsKey("TXID") && //empty or orderid value	orderid in the merchant shop system
				params.containsKey("PAYMENTGUARANTEE") && //NONE, VALIDATED or FULL	Payment guarantee, if any.
				params.containsKey("STATUS") && //SUCCEEDED, PENDING, FAILED	status of the transaction.
				params.containsKey("ERRMSG") && //errormessage	error messages of the gateway or empty
				params.containsKey("HASH")) {  //Signature	: checksum for validation 
					StringBuilder makeChecksum = new StringBuilder();
					String check = new String(params.get("HASH"));
					params.remove("HASH");
					for(Map.Entry<String, String> entry : params.entrySet()) {		
						makeChecksum.append(entry.getValue());
					}
					makeChecksum.append(this.privateKey);
					String checksum = makeChecksum.toString();
					try {
						checksum = Sha1.encode(checksum);
					} catch (Exception e) {
						log.error("Can't create powerpay sha1", e);
						return false;
					}
					return checksum.equals(check);
			} else {
				log.error("Not EPS call Incomplete parameters:"+params);
				return false;
			}
	}

	
	
	private void setFailed(ClearingInfo info) {
		info.setSuccessful(false);
		info.setResult("-1");
		info.setUserMessage("-1 | Unknown Error");
	}

	private ClearingInfo parseDirectResponse(String response) {
		HashMap<String, String> params = parseResponseString(response);
		String status = params.get("status");
		String errorMessage = params.get("errormessage");
		String providerTransactionId  = params.get("transactionid");
		boolean successful = false;
		
		if("2000".equals(status) || "0".equals(status)) {
			successful = true;
		} else {
			log.error("Unexpected status :" + status + " message :"+ errorMessage + " merchant trx id :" + providerTransactionId);
			successful = false;
		}
		String redirectUrl = params.get("redirect_url");
		ClearingInfo info = new ClearingInfo();
		info.setSuccessful(successful);
		info.setProviderTransactionId(providerTransactionId);
		info.setAcsUrl(redirectUrl);
		info.setMessage(errorMessage);
		
		return info;
	}
	
	private void parseResponse(ClearingInfo info, String response, boolean withdraw) {
		HashMap<String, String> params = parseResponseString(response);
		String statusStr = params.get("status");
		if(!CommonUtil.isParameterEmptyOrNull(statusStr)) {
			Integer status = Integer.parseInt(statusStr);
			if(status == 0) {
				info.setSuccessful(true);
				info.setProviderTransactionId(params.get("transactionid"));
				info.setMessage("Permitted transaction.");
			} else if( status == 2000) {
				if(withdraw) {
					info.setSuccessful(true);
					info.setProviderTransactionId(params.get("transactionid"));
					info.setMessage("PENDING transaction.");
				} else {
					// needs 3DSecure validation
					log.error("3dSecure validation not supported" + info);
					setFailed(info);
				}
			} else {
				info.setSuccessful(false);
				info.setMessage(params.get("errormessage"));
				info.setProviderTransactionId(params.get("transactionid"));
			}
		} else {
			setFailed(info);
			log.error("Can't parse status:" + response);
		}
	}

	private HashMap<String, String> parseResponseString(String response) {
		HashMap<String, String> params = new HashMap<>();
		String[] paramsString = response.split("&");
		for(String param: paramsString) {
			String[] p = param.split("=");
			if(p.length == 1) {
				params.put(p[0], "");
			} else {
				params.put(p[0], p[1]);
			}
		}
		return params;
	}
	
	public void parseDiagnoseResponse(String response, InatecInfo info) {
		try{
			Document responseDocument = ClearingUtil.parseXMLToDocument(response);
			NodeList nodes = responseDocument.getElementsByTagName("error_code");
			if(nodes.getLength() > 0) {
				String s = nodes.item(nodes.getLength()-1).getTextContent();
				if("0".equals(s)) {
					info.setSuccessful(true);
					info.setMessage("Permitted transaction.");
				} else if( "2000".equals(s)) {
					info.setNeedNotification(true);
				} else {
					setFailed(info);
				}
			} else {
				info.setNeedNotification(true);
			}
			
		} catch(Exception e) {
			log.error("Can't parse dignose response", e);
			setFailed(info);
		}
	}
}
