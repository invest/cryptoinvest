package com.anyoption.common.clearing;

import java.io.Serializable;
import java.util.Date;

import com.anyoption.common.beans.Country;
import com.anyoption.common.beans.CreditCard;
import com.anyoption.common.beans.TransactionRequestResponse;
import com.anyoption.common.beans.User;
import com.anyoption.common.managers.CountryManagerBase;
import com.anyoption.common.managers.CurrenciesManagerBase;
import com.anyoption.common.util.CommonUtil;

/**
 * Clearing info bean.
 *
 * @author Tony
 */
public class ClearingInfo implements Serializable {
	
	
	
    /**
	 * 
	 */
	private static final long serialVersionUID = 616891080822114300L;
	protected String cardUserId;
    protected String firstName;
    protected String lastName;
    protected String email;
    protected String ccn;
    protected String cvv;
    protected String expireMonth;
    protected String expireYear;
    protected int ccTypeId;
    protected String owner;
    protected long currencyId;
    protected long amount;
    protected long transactionType;
    protected long userId;
    protected String userName;
    protected long cardId;
    protected long transactionId;
    protected String recurringTransaction;
    protected String acquirerResponseId;
    protected String xorIdCapture;
    protected String MD;
    protected long userClassId;

    protected String providerTransactionId;
    protected String result;
    protected String message;
    protected String userMessage;
    protected String authNumber;
    protected String captureNumber;
    protected boolean successful;
    protected long providerId;
    protected String currencySymbol;
    protected long skinId;
    protected String address;
    protected String city;
    protected String zip;    
    protected long countryId;
    protected String countryA2;
    protected String ip;
    protected String acsUrl;
    protected String paReq;
    protected String paRes;
    protected long ccCountryId;

    protected long originalDepositId;
    protected String providerDepositId;
    
    protected String homePageUrl;  // get the url per domain from the web app
    protected Date userDOB;
    protected String userPhone;
    protected Long userState; // if USA
    protected int platformId;
    protected String termsURL;
    protected String additionalRequestParams;
    protected TransactionRequestResponse requestResponse;
    protected String utcOffset;
    protected String productId;
    protected String paymentSolution;
    protected String formData;
    
    /**
     * the environment source of the transaction - WEB, Backend etc
     */
    protected int source;
    
    protected boolean is3dSecure;
    
    //dont do rerouting in any case (for EPG)
    protected boolean isNotRerouting;

	public static final String NEEDS_POLLING =	"POLL";
    
    public ClearingInfo() {
    }

    public ClearingInfo(User user, long transactionAmount, long transactionTypeId, long transId, String transactionIp, String transactionAcquirerResponseId) {
        firstName = user.getFirstName();
        lastName = user.getLastName();
        email = user.getEmail();
        currencyId = user.getCurrencyId().intValue();
        amount = transactionAmount;
        transactionType = transactionTypeId;
        userId = user.getId();
        userName = user.getUserName();
        transactionId = transId;
        successful = false;
        currencySymbol = user.getCurrency().getNameKey();
        skinId = user.getSkinId();
        address = user.getStreet();
        if(user.getCountry() != null) {
	        countryId = user.getCountry().getId();
	        countryA2 = user.getCountry().getA2();
        } else {
            Country c = CountryManagerBase.getCountry(user.getCountryId());
            countryId = c.getId();
            countryA2 = c.getA2();
        }
        ip = transactionIp;
        acquirerResponseId = transactionAcquirerResponseId;
        userClassId = user.getClassId();
        city = user.getCityName();
        zip = user.getZipCode();
        userDOB = user.getTimeBirthDate();
        userPhone = user.getMobilePhone();
        userState = user.getState();
        platformId = user.getPlatformId();
    }

    public ClearingInfo(CreditCard card, User user, long transactionAmount, long transactionTypeId, long transId, String transactionIp, String transactionAcquirerResponseId){
    	this(user, transactionAmount, transactionTypeId, transId, transactionIp, transactionAcquirerResponseId);
    	cardUserId = card.getHolderIdNum();
        ccn = String.valueOf(card.getCcNumber());
        cvv = card.getCcPass();
        expireMonth = card.getExpMonth();
        expireYear = card.getExpYear();
        ccTypeId = (int) card.getTypeIdByBin();
        cardId = card.getId();
        owner = card.getHolderName();
        ccCountryId = card.getCountryId();
        recurringTransaction = card.getRecurringTransaction(); 
    }
    
    public ClearingInfo(User user, long transactionAmount, long transactionTypeId, long transId, String transactionIp, String transactionAcquirerResponseId, int source) {
        firstName = user.getFirstName();
        lastName = user.getLastName();
        email = user.getEmail();
        currencyId = user.getCurrencyId();
        amount = transactionAmount;
        transactionType = transactionTypeId;
        userId = user.getId();
        transactionId = transId;
        successful = false;
        currencySymbol = CurrenciesManagerBase.getCurrency(user.getCurrencyId()).getNameKey();
        skinId = user.getSkinId();
        address = user.getStreet();
        Country c = CountryManagerBase.getCountry(user.getCountryId());
        countryId = c.getId();
        countryA2 = c.getA2();
        ip = transactionIp;
        this.source = source;
        acquirerResponseId = transactionAcquirerResponseId;
        userClassId = user.getClassId();
        city = user.getCityName();
        zip = user.getZipCode();
        userDOB = user.getTimeBirthDate();
        userPhone = user.getMobilePhone();
        userState = user.getState();
        platformId = user.getPlatformId();
    }

    public ClearingInfo(CreditCard card, User user, long transactionAmount, long transactionTypeId, long transId, String transactionIp, String transactionAcquirerResponseId, int source){
    	this(user, transactionAmount, transactionTypeId, transId, transactionIp, transactionAcquirerResponseId, source);
    	cardUserId = card.getHolderIdNum();
        ccn = String.valueOf(card.getCcNumber());
        cvv = card.getCcPass();
        expireMonth = card.getExpMonth();
        expireYear = card.getExpYear();
        ccTypeId = (int) card.getTypeId();
        cardId = card.getId();
        owner = card.getHolderName();
        ccCountryId = card.getCountryId();
        recurringTransaction = card.getRecurringTransaction();
        city = user.getCityName();
        zip = user.getZipCode();
        userDOB = user.getTimeBirthDate();
        userPhone = user.getMobilePhone();
        userState = user.getState();
        
    }

    public ClearingInfo(ClearingInfo ci){
    	owner = ci.getOwner();
    	firstName = ci.getFirstName();
        lastName = ci.getLastName();
        email = ci.getEmail();
        currencyId = ci.getCurrencyId();
        currencySymbol = ci.getCurrencySymbol();
        amount = ci.getAmount();
        transactionType = ci.getTransactionType();
        userId = ci.getUserId();
        userName = ci.getUserName();
        //transactionId = ci.getTransactionId();
        successful = false;
        currencySymbol = ci.getCurrencySymbol();
        skinId = ci.getSkinId();
        address = ci.getAddress();
        countryId = ci.getCountryId();
        countryA2 = ci.getCountryA2();
        cardId = ci.getCardId();
        ccn = ci.getCcn();
        expireYear = ci.getExpireYear();
        expireMonth = ci.getExpireMonth();
        cvv = ci.getCvv();
        recurringTransaction = ci.getRecurringTransaction();
        ip = ci.getIp();
        platformId = ci.getPlatformId();   
        userDOB = ci.getUserDOB(); //can't be null pointer...
    }
    
    public String toString() {
        String ls = System.getProperty("line.separator");
        return ls + "ClearingInfo:" + ls +
                "cardUserId: " + cardUserId + ls +
                "firstName: " + firstName + ls +
                "lastName: " + lastName + ls +
                "email: " + email + ls +
                "ccn: " + ccn + ls +
                "cvv: " + cvv + ls +
                "expireMonth: " + expireMonth + ls +
                "expireYear: " + expireYear + ls +
                "ccTypeId: " + ccTypeId + ls +
                "currencyId: " + currencyId + ls +
                "amount: " + amount + ls +
                "transactionType: " + transactionType + ls +
                "userId: " + userId + ls +
                "cardId: " + cardId + ls +
                "recurringTransaction:" + recurringTransaction + ls +
                "transactionId: " + transactionId + ls +
                "result: " + result + ls +
                "message: " + message + ls +
                "userMessage: " + userMessage + ls +
                "authNumber: " + authNumber + ls +
                "successful: " + successful + ls +
                "providerId: " + providerId + ls +
                "currencySymbol: " + currencySymbol + ls +
                "providerTransactionId: " + providerTransactionId + ls +
                "skinId: " + skinId + ls +
                "owner: " + owner + ls +
                "address: " + address + ls +
                "countryId: " + countryId + ls +
                "countryA2: " + countryA2 + ls +
                "ip: " + ip + ls +
                "acsUrl: " + acsUrl + ls +
                "paReq: " + paReq + ls +
                "paRes: " + paRes + ls +
        		"originalDepositId(ForXorCredit): " + originalDepositId + ls +
        		"providerDepositId(ForWcCredit): " + providerDepositId + ls +
        		"userClassId: " + userClassId + ls + 
        		"platformId: " + platformId + ls +
        		"userDOB: " + userDOB + ls;
    }

	public long getAmount() {
		return amount;
	}

	public void setAmount(long amount) {
		this.amount = amount;
	}

	public String getAuthNumber() {
		return authNumber;
	}

	public void setAuthNumber(String authNumber) {
		this.authNumber = authNumber;
	}

	public long getCardId() {
		return cardId;
	}

	public void setCardId(long cardId) {
		this.cardId = cardId;
	}

	public String getCcn() {
		return ccn;
	}

	public void setCcn(String ccn) {
		this.ccn = ccn;
	}

	public int getCcTypeId() {
		return ccTypeId;
	}

	public void setCcTypeId(int ccTypeId) {
		this.ccTypeId = ccTypeId;
	}

	public long getCurrencyId() {
		return currencyId;
	}

	public void setCurrencyId(long currencyId) {
		this.currencyId = currencyId;
	}

	public String getCvv() {
		return cvv;
	}

	public void setCvv(String cvv) {
		this.cvv = cvv;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getExpireMonth() {
		return expireMonth;
	}

	public void setExpireMonth(String expireMonth) {
		this.expireMonth = expireMonth;
	}

	public String getExpireYear() {
		return expireYear;
	}

	public void setExpireYear(String expireYear) {
		this.expireYear = expireYear;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public long getProviderId() {
        return providerId;
    }

    public void setProviderId(long providerId) {
        this.providerId = providerId;
    }

    public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}

	public boolean isSuccessful() {
		return successful;
	}

	public void setSuccessful(boolean successful) {
		this.successful = successful;
	}

	public long getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(long transactionId) {
		this.transactionId = transactionId;
	}

	public long getTransactionType() {
        return transactionType;
    }

    public void setTransactionType(long transactionType) {
        this.transactionType = transactionType;
    }

    public String getUserMessage() {
		return userMessage;
	}

	public void setUserMessage(String userMessage) {
		this.userMessage = userMessage;
	}

	public String getCardUserId() {
		return cardUserId;
	}

	public void setCardUserId(String cardUserId) {
		this.cardUserId = cardUserId;
	}

	public long getUserId() {
		return userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}

	public String getCurrencySymbol() {
		return currencySymbol;
	}

	public void setCurrencySymbol(String currencySymbol) {
		this.currencySymbol = currencySymbol;
	}

    public String getProviderTransactionId() {
        return providerTransactionId;
    }

    public void setProviderTransactionId(String providerTransactionId) {
        this.providerTransactionId = providerTransactionId;
    }

    public long getSkinId() {
        return skinId;
    }

    public void setSkinId(long skinId) {
        this.skinId = skinId;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public long getCountryId() {
        return countryId;
    }

    public void setCountryId(long countryId) {
        this.countryId = countryId;
    }

    public String getCountryA2() {
        return countryA2;
    }

    public void setCountryA2(String countryA2) {
        this.countryA2 = countryA2;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getAcsUrl() {
        return acsUrl;
    }

    public void setAcsUrl(String acsUrl) {
        this.acsUrl = acsUrl;
    }

    public String getPaReq() {
        return paReq;
    }

    public void setPaReq(String paReq) {
        this.paReq = paReq;
    }

    public String getPaRes() {
        return paRes;
    }

    public void setPaRes(String paRes) {
        this.paRes = paRes;
    }

	/**
	 * @return the originalDepositId
	 */
	public long getOriginalDepositId() {
		return originalDepositId;
	}

	/**
	 * @param originalDepositId the originalDepositId to set
	 */
	public void setOriginalDepositId(long originalDepositId) {
		this.originalDepositId = originalDepositId;
	}

	/**
	 * @return the providerDepositId
	 */
	public String getProviderDepositId() {
		return providerDepositId;
	}

	/**
	 * @param providerDepositId the providerDepositId to set
	 */
	public void setProviderDepositId(String providerDepositId) {
		this.providerDepositId = providerDepositId;
	}

	public String getCc4Digits() {
		return ccn.substring(ccn.length()-4);
	}

//	TODO extract to caller
//	public String getAmountText() {
//		return CommonUtil.formatCurrencyAmount(amount, true, currencyId);
//	}
//	
	public String getAmountToString(){
		return String.valueOf(amount);
	}

	public void setUserName(String userName) {

		this.userName = userName;
	}

	public String getUserName() {

		return userName;
	}

    public int getSource() {
        return source;
    }

    public void setSource(int source) {
        this.source = source;
    }

	/**
	 * @return the ccCountryId
	 */
	public long getCcCountryId() {
		return ccCountryId;
	}

	/**
	 * @param ccCountryId the ccCountryId to set
	 */
	public void setCcCountryId(long ccCountryId) {
		this.ccCountryId = ccCountryId;
	}

	/**
	 * @return the homePageUrl
	 */
	public String getHomePageUrl() {
		return homePageUrl;
	}

	/**
	 * @param homePageUrl the homePageUrl to set
	 */
	public void setHomePageUrl(String homePageUrl) {
		this.homePageUrl = homePageUrl;
	}
	
	public boolean isFirstTimeTBI(){
		if(CommonUtil.isParameterEmptyOrNull(recurringTransaction)){
			return true;
		}else{
			return false;
		}		
	}

	public String getRecurringTransaction() {
		return recurringTransaction;
	}

	public void setRecurringTransaction(String recurringTransaction) {
		this.recurringTransaction = recurringTransaction;
	}

	public String getAcquirerResponseId() {
		return acquirerResponseId;
	}

	public void setAcquirerResponseId(String acquirerResponseId) {
		this.acquirerResponseId = acquirerResponseId;
	}
	
	public long getUserClassId() {
		return userClassId;
	}


	public void setUserClassId(long userClassId) {
		this.userClassId = userClassId;
	}
	
	public String getCity() {
		return city;
	}
	
	public Date getUserDOB() {
		return userDOB;
	}
	
	public String getUserPhone() {
		return userPhone;
	}
	
	public Long getUserState() {
		return userState;
	}
	
	public String getZip() {
		return zip;
	}

	public void setUserPhone(String userPhone) {
		this.userPhone = userPhone;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public void setZip(String zip) {
		this.zip = zip;
	}

	/**
	 * @return the platformId
	 */
	public int getPlatformId() {
		return platformId;
	}

	/**
	 * @param platformId the platformId to set
	 */
	public void setPlatformId(int platformId) {
		this.platformId = platformId;
	}

	public boolean isNotRerouting() {
		return isNotRerouting;
	}

	public void setNotRerouting(boolean isNotRerouting) {
		this.isNotRerouting = isNotRerouting;
	}

	public String getTermsURL() {
		return termsURL;
	}

	public void setTermsURL(String termsURL) {
		this.termsURL = termsURL;
	}
	
	public boolean isEpgProvider() {
		if (providerId == ClearingManagerConstants.EPG_AO_PROVIDER_ID
				|| providerId == ClearingManagerConstants.EPG_ET_PROVIDER_ID
				|| providerId == ClearingManagerConstants.EPG_OUROBOROS_PROVIDER_ID) {
			return true;
		}
		return false; 
	}

	/**
	 * @return the additionalRequestParams
	 */
	public String getAdditionalRequestParams() {
		return additionalRequestParams;
	}

	/**
	 * @param additionalRequestParams the additionalRequestParams to set
	 */
	public void setAdditionalRequestParams(String additionalRequestParams) {
		this.additionalRequestParams = additionalRequestParams;
	}

	/**
	 * @return the requestResponse
	 */
	public TransactionRequestResponse getRequestResponse() {
		return requestResponse;
	}

	/**
	 * @param requestResponse the requestResponse to set
	 */
	public void setRequestResponse(TransactionRequestResponse requestResponse) {
		this.requestResponse = requestResponse;
	}

	/**
	 * @return the utcOffset
	 */
	public String getUtcOffset() {
		return utcOffset;
	}

	/**
	 * @param utcOffset the utcOffset to set
	 */
	public void setUtcOffset(String utcOffset) {
		this.utcOffset = utcOffset;
	}

	/**
	 * @return the productId
	 */
	public String getProductId() {
		return productId;
	}

	/**
	 * @param productId the productId to set
	 */
	public void setProductId(String productId) {
		this.productId = productId;
	}

	/**
	 * @return the paymentSolution
	 */
	public String getPaymentSolution() {
		return paymentSolution;
	}

	/**
	 * @param paymentSolution the paymentSolution to set
	 */
	public void setPaymentSolution(String paymentSolution) {
		this.paymentSolution = paymentSolution;
	}

	public boolean is3dSecure() {
		return is3dSecure;
	}

	public void setIs3dSecure(boolean is3dSecure) {
		this.is3dSecure = is3dSecure;
	}

	public String getCaptureNumber() {
		return captureNumber;
	}

	public void setCaptureNumber(String captureNumber) {
		this.captureNumber = captureNumber;
	}

	public String getFormData() {
		return formData;
	}

	public void setFormData(String formData) {
		this.formData = formData;
	}

	public String getMD() {
		return MD;
	}

	public void setMD(String mD) {
		MD = mD;
	}
	
} 