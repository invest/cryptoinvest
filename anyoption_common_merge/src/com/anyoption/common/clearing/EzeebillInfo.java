package com.anyoption.common.clearing;

public class EzeebillInfo extends ClearingInfo {

	public String url ;
	public String depositTerminalId;
    public String returnUrl;
    public String version;
    public String accessId;
    public String payType;
    public String apiAccess;
    public String merchId;
    public String action;
    
    public EzeebillInfo() {
    	
    }
    
	public EzeebillInfo(long transactionId, long userId, long amount, String domain) {
		this.transactionId = transactionId;
		this.userId = userId;
		this.amount = amount;
		
		this.url = EzeebillClearingProvider.accessUrl ;
		this.depositTerminalId = EzeebillClearingProvider.terminalId;
		this.returnUrl = domain + EzeebillClearingProvider.returnUrl;
		this.version = EzeebillClearingProvider.version;
		this.accessId = EzeebillClearingProvider.accessId;
		this.payType = EzeebillClearingProvider.payType;
		this.apiAccess = EzeebillClearingProvider.apiAccess;
		this.merchId = EzeebillClearingProvider.merchId;
		this.action = EzeebillClearingProvider.action;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getDepositTerminalId() {
		return depositTerminalId;
	}

	public void setDepositTerminalId(String depositTerminalId) {
		this.depositTerminalId = depositTerminalId;
	}

	public String getReturnUrl() {
		return returnUrl;
	}

	public void setReturnUrl(String returnUrl) {
		this.returnUrl = returnUrl;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public String getAccessId() {
		return accessId;
	}

	public void setAccessId(String accessId) {
		this.accessId = accessId;
	}

	public String getPayType() {
		return payType;
	}

	public void setPayType(String payType) {
		this.payType = payType;
	}

	public String getApiAccess() {
		return apiAccess;
	}

	public void setApiAccess(String apiAccess) {
		this.apiAccess = apiAccess;
	}

	public String getMerchId() {
		return merchId;
	}

	public void setMerchId(String merchId) {
		this.merchId = merchId;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	@Override
	public String toString() {
		return "EzeebillInfo [url=" + url + ", depositTerminalId=" + depositTerminalId + ", returnUrl=" + returnUrl
				+ ", version=" + version + ", accessId=" + accessId + ", payType=" + payType + ", apiAccess="
				+ apiAccess + ", merchId=" + merchId + ", action=" + action + "]";
	}
	
	
}
