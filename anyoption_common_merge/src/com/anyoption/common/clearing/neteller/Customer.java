package com.anyoption.common.clearing.neteller;

public class Customer {
	
	Link link;

	public Link getLink() {
		return link;
	}

	public void setLink(Link link) {
		this.link = link;
	}

	@Override
	public String toString() {
		return "Customer [link=" + link + "]";
	}
	
	
}
