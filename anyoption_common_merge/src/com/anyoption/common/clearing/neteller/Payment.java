package com.anyoption.common.clearing.neteller;

import java.util.Arrays;

public class Payment {

	Customer		customer;
	Transaction		transaction;
	Link[]			links;
	
	public Customer getCustomer() {
		return customer;
	}
	
	public void setCustomer(Customer customer) {
		this.customer = customer;
	}
	
	public Transaction getTransaction() {
		return transaction;
	}
	
	public void setTransaction(Transaction transaction) {
		this.transaction = transaction;
	}

	public Link[] getLinks() {
		return links;
	}

	public void setLinks(Link[] links) {
		this.links = links;
	}

	@Override
	public String toString() {
		return "Payment [customer=" + customer + ", transaction=" + transaction + ", links=" + Arrays.toString(links)
				+ "]";
	}
	
	
}
