package com.anyoption.common.clearing.neteller;

public class Link {
	
	String url;
	String rel;
	String method;
	
	public String getUrl() {
		return url;
	}
	
	public void setUrl(String url) {
		this.url = url;
	}
	
	public String getRel() {
		return rel;
	}
	
	public void setRel(String rel) {
		this.rel = rel;
	}
	
	public String getMethod() {
		return method;
	}
	
	public void setMethod(String method) {
		this.method = method;
	}
	
	@Override
	public String toString() {
		return "Link [url=" + url + ", rel=" + rel + ", method=" + method + "]";
	}
}
