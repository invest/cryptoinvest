package com.anyoption.common.clearing.neteller;

public class TransferIn {
	
	PaymentMethod paymentMethod;
	Transaction transaction;
	String verificationCode;
	
	public Transaction getTransaction() {
		return transaction;
	}
	
	public void setTransaction(Transaction transaction) {
		this.transaction = transaction;
	}
	
	public String getVerificationCode() {
		return verificationCode;
	}
	
	public void setVerificationCode(String verificationCode) {
		this.verificationCode = verificationCode;
	}

	public PaymentMethod getPaymentMethod() {
		return paymentMethod;
	}

	public void setPaymentMethod(PaymentMethod paymentMethod) {
		this.paymentMethod = paymentMethod;
	}

	@Override
	public String toString() {
		return "TransferIn [paymentMethod=" + paymentMethod + ", transaction=" + transaction + ", verificationCode="
				+ verificationCode + "]";
	}
}
