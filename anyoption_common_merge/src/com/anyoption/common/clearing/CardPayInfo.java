package com.anyoption.common.clearing;

import java.math.BigDecimal;

/**
 * @author Kiril.m
 */
public class CardPayInfo {

	private long number;
	private String description;
	private String currency;
	private BigDecimal amount;
	private String email;
	private String orderXML;
	private String orderXMLEncoded;
	private String sha512;
	private long cardPayId;
	private String status;
	private int declineCode;
	private boolean threeD;
	private String declineReason;

	public CardPayInfo(long number, String description, String currency, BigDecimal amount, String email) {
		this.number = number;
		this.description = description;
		this.currency = currency;
		this.amount = amount;
		this.email = email;
	}

	public CardPayInfo(String orderXMLEncoded) {
		this.orderXMLEncoded = orderXMLEncoded;
	}

	public long getNumber() {
		return number;
	}

	public void setNumber(long number) {
		this.number = number;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getOrderXML() {
		return orderXML;
	}

	public void setOrderXML(String orderXML) {
		this.orderXML = orderXML;
	}

	public String getOrderXMLEncoded() {
		return orderXMLEncoded;
	}

	public void setOrderXMLEncoded(String orderXMLEncoded) {
		this.orderXMLEncoded = orderXMLEncoded;
	}

	public String getSha512() {
		return sha512;
	}

	public void setSha512(String sha512) {
		this.sha512 = sha512;
	}

	public long getCardPayId() {
		return cardPayId;
	}

	public void setCardPayId(long cardPayId) {
		this.cardPayId = cardPayId;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public int getDeclineCode() {
		return declineCode;
	}

	public void setDeclineCode(int declineCode) {
		this.declineCode = declineCode;
	}

	public boolean isThreeD() {
		return threeD;
	}

	public void setThreeD(boolean threeD) {
		this.threeD = threeD;
	}

	public String getDeclineReason() {
		return declineReason;
	}

	public void setDeclineReason(String declineReason) {
		this.declineReason = declineReason;
	}

	@Override
	public String toString() {
		String ls = System.lineSeparator();
		return ls + this.getClass().getName() + ls
				+ super.toString() + ls
				+ "number: " + number + ls
				+ "description: " + description + ls
				+ "currency: " + currency + ls
				+ "amount: " + amount + ls
				+ "email: " + email + ls
				+ "orderXML: " + orderXML + ls
				+ "orderXMLEncoded: " + orderXMLEncoded + ls
				+ "sha512: " + sha512 + ls
				+ "cardPayId: " + cardPayId + ls
				+ "status: " + status + ls
				+ "declineCode: " + declineCode + ls
				+ "threeD: " + threeD + ls
				+ "declineReason: " + declineReason + ls;
	}
}