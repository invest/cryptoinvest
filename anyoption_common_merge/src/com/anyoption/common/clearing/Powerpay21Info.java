package com.anyoption.common.clearing;

import java.lang.annotation.Annotation;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.reflect.Field;
import java.util.TreeMap;
import java.util.stream.Collectors;

import org.apache.log4j.Logger;

import com.anyoption.common.encryption.SecureAlgorithms.Sha1;


public class Powerpay21Info {
	private static final Logger log = Logger.getLogger(Powerpay21Info.class);
	
	@Retention(RetentionPolicy.RUNTIME)
	private @interface Capture {
	}
	
	@Retention(RetentionPolicy.RUNTIME)
	private @interface Preauthorize {
	}

	@Retention(RetentionPolicy.RUNTIME)
	private @interface Cancel {
	}

	@Retention(RetentionPolicy.RUNTIME)
	private @interface Withdraw {
	}
	
	@Retention(RetentionPolicy.RUNTIME)
	private @interface Diagnose {
	}

	@Retention(RetentionPolicy.RUNTIME)
	private @interface Refund {
	}

	@Retention(RetentionPolicy.RUNTIME)
	private @interface Soffort {
	}
	
	@Retention(RetentionPolicy.RUNTIME)
	private @interface Eps {
	}
	
	@Retention(RetentionPolicy.RUNTIME)
	private @interface Giropay {
	}


	//Customer Data
	@Preauthorize 
	private String	firstname;
	@Preauthorize 
	private String	lastname;
	@Preauthorize 
	private String	street;
	@Preauthorize 
	private String	zip;
	@Preauthorize 
	private String	city;
	@Preauthorize 
	private String	state;
	@Preauthorize 
	private String	country;
	@Preauthorize @Soffort @Giropay @Eps
	private String	email;
	@Preauthorize 
	private String	customerip;
	@Preauthorize
	private String	company;
	@Preauthorize
	private long	customerid;
	
	// Payment Data
	@Capture @Preauthorize @Cancel @Withdraw @Refund @Soffort @Giropay @Eps @Diagnose
	private String	merchantid;
	@Preauthorize @Withdraw @Soffort @Giropay @Eps
	private String	orderid;
	@Preauthorize @Withdraw @Refund @Soffort @Giropay @Eps
	private String	currency;
	@Preauthorize @Withdraw
	private String	ccn;
	@Preauthorize @Withdraw
	private String	exp_month;
	@Preauthorize @Withdraw
	private String	exp_year;
	@Preauthorize
	private String	cvc_code;
	@Preauthorize @Withdraw
	private String	cardholder_name;
	@Preauthorize @Cancel @Withdraw @Refund @Soffort @Giropay @Eps
	private String	language;
	@Preauthorize @Withdraw @Soffort @Giropay @Eps
	private long	payment_method;
	@Preauthorize @Withdraw @Soffort @Giropay @Eps
	private int	amount;
	@Refund
	private float price;
	@Preauthorize
	private String	param_3d = "non3d";
	@Capture @Cancel @Refund @Diagnose
	private String	transactionid;
	
	// Alternative Payment Systems
	@Soffort @Giropay
	private String iban;
	@Soffort @Giropay
	private String bic;
	@Soffort @Giropay @Eps
	private String accountname;
	@Soffort @Giropay @Eps
	private String bankcountry;
	@Soffort @Giropay @Eps
	private String url_success;
	@Soffort @Giropay @Eps
	private String url_failed;
	@Soffort @Giropay @Eps
	private String url_internal;

	@Soffort @Giropay @Eps 
	private String custom2;
	
	@Diagnose
	private String type;
	 
	// secret - appended at the end
	private String secret;
	//  SHA-1 hex value of concatenated values + secret
	private String	signature;
	
	private Powerpay21Info() {
		// initialize to empty String to avoid "null" as parameter
		merchantid		= "";
		orderid			= "";
		currency		= "";
		signature		= "";
		ccn 			= "";
		exp_month 		= "";
		exp_year 		= "";
		cvc_code 		= "";
		cardholder_name = "";
		firstname		= "";
		lastname		= "";
		street			= "";
		zip				= "";
		city			= "";
		state			= "";
		country			= "";
		email			= "";
		customerip		= "";
		firstname		= "";
		lastname		= "";
		street			= "";
		zip				= "";
		city			= "";
		state			= "";
		country			= "";
		email			= "";
		customerip		= "";
		company			= "";
		iban			= "";
		bic				= "";
		accountname		= "";
		bankcountry		= "";
		url_success		= "";
		url_failed		= "";
		url_internal	= "";
		custom2			= "";

		// language for error message
		language	= "en";		
		// by default turn off 3d secure validation
		param_3d = "non3d";
	}
	
	public Powerpay21Info(String secret) {
		this();
		this.secret = secret;
	}

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public String getStreet() {
		return street;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	public String getZip() {
		return zip;
	}

	public void setZip(String zip) {
		this.zip = zip;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getCustomerIp() {
		return customerip;
	}

	public void setCustomerIp(String customerIp) {
		this.customerip = customerIp;
	}

	public String getMerchantId() {
		return merchantid;
	}

	public void setMerchantId(String merchantId) {
		this.merchantid = merchantId;
	}

	public String getOrderId() {
		return orderid;
	}

	public void setOrderId(String orderId) {
		this.orderid = orderId;
	}

	public int getAmount() {
		return amount;
	}

	public void setAmount(int amount) {
		this.amount = amount;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public long getPaymentMethod() {
		return payment_method;
	}

	public void setPaymentMethod(long paymentMethod) {
		this.payment_method = paymentMethod;
	}

	public String getSignature() {
		return signature;
	}

	public void setSignature(String signature) {
		this.signature = signature;
	}

	public String getCcn() {
		return ccn;
	}

	public void setCcn(String ccn) {
		this.ccn = ccn;
	}

	public String getExpMonth() {
		return exp_month;
	}

	public void setExpMonth(String expMonth) {
		this.exp_month = expMonth;
	}

	public String getExpYear() {
		return exp_year;
	}

	public void setExpYear(String expYear) {
		this.exp_year = expYear;
	}

	public String getCvcCode() {
		return cvc_code;
	}

	public void setCvcCode(String cvcCode) {
		this.cvc_code = cvcCode;
	}

	public String getCardholderName() {
		return cardholder_name;
	}

	public void setCardholderName(String cardholderName) {
		this.cardholder_name = cardholderName;
	}
	
	public String getCompany() {
		return company;
	}

	public void setCompany(String company) {
		this.company = company;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public String getTransactionId() {
		return transactionid;
	}

	public String getCustomerip() {
		return customerip;
	}

	public void setCustomerip(String customerip) {
		this.customerip = customerip;
	}

	public long getCustomerid() {
		return customerid;
	}

	public void setCustomerid(long customerid) {
		this.customerid = customerid;
	}

	public String getMerchantid() {
		return merchantid;
	}

	public void setMerchantid(String merchantid) {
		this.merchantid = merchantid;
	}

	public String getOrderid() {
		return orderid;
	}

	public void setOrderid(String orderid) {
		this.orderid = orderid;
	}

	public String getExp_month() {
		return exp_month;
	}

	public void setExp_month(String exp_month) {
		this.exp_month = exp_month;
	}

	public String getExp_year() {
		return exp_year;
	}

	public void setExp_year(String exp_year) {
		this.exp_year = exp_year;
	}

	public String getCvc_code() {
		return cvc_code;
	}

	public void setCvc_code(String cvc_code) {
		this.cvc_code = cvc_code;
	}

	public String getCardholder_name() {
		return cardholder_name;
	}

	public void setCardholder_name(String cardholder_name) {
		this.cardholder_name = cardholder_name;
	}

	public long getPayment_method() {
		return payment_method;
	}

	public void setPayment_method(long payment_method) {
		this.payment_method = payment_method;
	}

	public float getPrice() {
		return price;
	}

	public void setPrice(float price) {
		this.price = price;
	}

	public String getParam_3d() {
		return param_3d;
	}

	public void setParam_3d(String param_3d) {
		this.param_3d = param_3d;
	}

	public String getTransactionid() {
		return transactionid;
	}

	public String getSecret() {
		return secret;
	}

	public void setSecret(String secret) {
		this.secret = secret;
	}

	public void setTransactionId(String transactionId) {
		this.transactionid = transactionId;
	}

	public String getCustom2() {
		return custom2;
	}

	public void setCustom2(String custom2) {
		this.custom2 = custom2;
	}

	public String getIban() {
		return iban;
	}

	public void setIban(String iban) {
		this.iban = iban;
	}

	public String getBic() {
		return bic;
	}

	public void setBic(String bic) {
		this.bic = bic;
	}

	public String getAccountname() {
		return accountname;
	}

	public void setAccountname(String accountname) {
		this.accountname = accountname;
	}

	public String getBankcountry() {
		return bankcountry;
	}

	public void setBankcountry(String bankcountry) {
		this.bankcountry = bankcountry;
	}

	public String getUrl_success() {
		return url_success;
	}

	public void setUrl_success(String url_success) {
		this.url_success = url_success;
	}

	public String getUrl_failed() {
		return url_failed;
	}

	public void setUrl_failed(String url_failed) {
		this.url_failed = url_failed;
	}

	public String getUrl_internal() {
		return url_internal;
	}

	public void setUrl_internal(String url_internal) {
		this.url_internal = url_internal;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String toPreauthPost() throws Exception {
		return toPostRequest(Preauthorize.class);
	}
	
	public String toCapturePost() throws Exception {
		return toPostRequest(Capture.class);
	}
	
	public String toCancelPost() throws Exception {
		return toPostRequest(Cancel.class);
	}
	
	public String toCftPost() throws Exception {
		return toPostRequest(Withdraw.class);
	}

	public String toRefundPost() throws Exception {
		return toPostRequest(Refund.class);
	}

	public String toSoforPost() throws Exception {
		return toPostRequest(Soffort.class);
	}
	
	public String toEpsPost() throws Exception {
		return toPostRequest(Eps.class);
	}

	public String toGiropayPost() throws Exception {
		return toPostRequest(Giropay.class);
	}
	
	public String toDiagnosePost() throws Exception {
		return toPostRequest(Diagnose.class);
	} 

	private String toPostRequest(Class<? extends Annotation> a) throws Exception {
		Field[] fields = Powerpay21Info.class.getDeclaredFields();
		TreeMap<String, String> t = new TreeMap<>();
		StringBuilder request = new StringBuilder();
		for(Field field: fields) {
	        if(field.getAnnotation(a) != null) {
				String value = String.valueOf(field.get(this));
				if(value == null) {
					log.error("Empty value for:" + field.getName());
				}
				t.put(field.getName(), value);
				request.append(field.getName());
				request.append("=");
				request.append(value);
				request.append("&");
	        }
		}
		
		String s = t.values().stream().collect(Collectors.joining());
		s += secret;
		
		String signature = Sha1.encode(s);
		request.append("signature").append("=").append(signature);
		
		return request.toString();
	}

}
