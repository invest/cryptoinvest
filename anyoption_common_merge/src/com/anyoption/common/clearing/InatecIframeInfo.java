package com.anyoption.common.clearing;

import java.io.Serializable;
import java.sql.SQLException;
import java.util.Map;
import java.util.TreeMap;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.Transaction;
import com.anyoption.common.beans.User;
import com.anyoption.common.managers.LanguagesManagerBase;
import com.anyoption.common.util.CommonUtil;
import com.anyoption.common.util.Sha1;

/**
 * inatect iframe info clearing info class
* @author liors
*
**/
public class InatecIframeInfo extends ClearingInfo implements Serializable {		
	private static final long serialVersionUID = 1L;
	private static final Logger log = Logger.getLogger(InatecIframeInfo.class);	
	public static final String RETURN_PAGE_NAME = "inatecIframeUrlReturn";
	public static final String SUCCESS_PAGE_NAME = "inatecIframeUrlSuccess";
	public static final String FAILED_PAGE_NAME = "inatecIframeUrlFailed";
	public static final String INTERNAL_PAGE_NAME = "inatecIframeUrlInternal";
	private static final String URL_RETURN = "jsp/pages/" + RETURN_PAGE_NAME + ".jsf";
	private static final String URL_SUCCESS = "jsp/pages/" + SUCCESS_PAGE_NAME + ".jsf";
	private static final String URL_FAILED = "jsp/pages/" + FAILED_PAGE_NAME + ".jsf";
	private static final String URL_INTERNAL = "jsp/" + INTERNAL_PAGE_NAME + ".jsf";
	private static final int PAYMENT_METHOD_IDEAL = 5;
	//STATUS
	public static final String STATUS_SUCCEEDED = "SUCCEEDED";
	public static final String STATUS_PENDING = "PENDING";
	public static final String STATUS_FAILED = "FAILED";
	
	private String redirectURL;
	private String urlReturn;
	private String urlSuccess;
	private String urlFailed;
	private String urlInternal;
	private String merchantAccount;
	private String language;
	private String currency;	
	private String depositAmount;
	//private String merchantId; //marchantAccount
	//orderId -> transactionId.
	private String countryA3;
	private String signature; // special secret hashcode (detail description see appendix A)
	private String privateKey; //secret.
	
	//response
	private String requestStatus;
	private String tag;
	private String txid;
	private String paymentGuarantee;
	private String status;
	private String errmsg;
	private String hash;
		
	private String responseComment;
	private Map<String, String> infoParamMap = new TreeMap<String, String>();
	private long transactionStatusId;
	private String custom2 = "ideal";
	private int paymentMethod; 
		
	public InatecIframeInfo() {
		this.responseComment = "";
		this.paymentMethod = PAYMENT_METHOD_IDEAL;
    	this.custom2 = "ideal";
    	this.currency = "EUR"; //TODO: not hard coded.
    	this.countryA3 = "NLD";
    }

	/**
	 * @param user
	 * @param transaction
	 * @param language
	 * @param homePageUrlHttps
	 * @param amount
	 * @throws SQLException
	 */
	public InatecIframeInfo(User user, Transaction t, String language, String homePageUrlHttps, String amount) throws SQLException{
    	super(user, t.getAmount(), t.getTypeId(), t.getId(), t.getIp(), t.getAcquirerResponseId());
    	this.urlReturn = homePageUrlHttps + URL_RETURN;
    	this.urlSuccess = homePageUrlHttps + URL_SUCCESS;
    	this.urlFailed = homePageUrlHttps + URL_FAILED;
    	this.urlInternal = homePageUrlHttps + URL_INTERNAL;
    	this.depositAmount = amount;
    	this.language = language;
    	this.responseComment = "";
		this.paymentMethod = PAYMENT_METHOD_IDEAL;
    	this.custom2 = "ideal";
    	this.currency = "EUR"; //TODO: not hard coded.
    	this.countryA3 = "NLD";
    	if (user.getCountry().getA3() != null && !CommonUtil.isParameterEmptyOrNull(user.getCountry().getA3())) {
    		this.countryA3 = user.getCountry().getA3();
    	}	    	
    }
			
	private void createParamMap() {
		infoParamMap.put("merchantid", this.merchantAccount);
		infoParamMap.put("orderid", String.valueOf(this.transactionId));
		infoParamMap.put("amount", this.depositAmount);
		infoParamMap.put("currency", this.currency);
		infoParamMap.put("firstname", this.firstName);
		infoParamMap.put("lastname", this.lastName);
		infoParamMap.put("street", this.address);
		infoParamMap.put("zip", this.zip);
		infoParamMap.put("city", this.city);
		infoParamMap.put("country", this.countryA3);
		infoParamMap.put("email", this.email);
		infoParamMap.put("custom2", this.custom2);
		infoParamMap.put("customerip", this.ip);
		infoParamMap.put("payment_method", String.valueOf(this.paymentMethod));
		infoParamMap.put("url_internal", this.urlInternal);
		infoParamMap.put("url_return", this.urlReturn);
		infoParamMap.put("bankcountry", "NL");
		infoParamMap.put("accountname", "Account name");
	}

	/**
	 * @param user
	 * @param transaction
	 * @throws SQLException
	 */
	public InatecIframeInfo(User user, Transaction t) throws SQLException {
    	super(user, t.getAmount(), t.getTypeId(), t.getId(), t.getIp(), t.getAcquirerResponseId());
        language = LanguagesManagerBase.getCodeById(user.getLanguageId());
    }
	
	public static boolean checksum() {
		
		return false;			
	}
	
	/**
	 * Create InatecIframe checksum to ensure that the transaction is initiated by anyoption user.
	 * It is created based upon the InatecIframe parameters, and is protected using the partner_control value, the secret 32-character key.
	 * Using hash method: SHA-1.
	 * @return hash string.
	 */
	public String createInatecIframeChecksumBuilder() {
		String checksum = "";
		//need to be ordered by parameter name!!!
		createParamMap();

		StringBuilder makeChecksum = new StringBuilder();
		for(Map.Entry<String, String> entry : infoParamMap.entrySet()) {		
			makeChecksum.append(entry.getValue());
		}
		makeChecksum.append(this.privateKey);
		checksum = makeChecksum.toString();
		log.info("inatectIframe checksum creation: (Before) " + checksum);
		try {
			checksum = Sha1.encode(checksum);
			log.info("inatectIframe checksum creation: (After SHA-1): " + checksum);
		} catch (Exception e) {
			log.error("ERROR! Can't create checksum for inatectIframe using SHA-1 algorithm. creation", e);
		}
		return checksum;
	}
	
	/**
	 * Create InatecIframe checksum to ensure that the transaction is initiated by anyoption user.
	 * It is created based upon the InatecIframe parameters, and is protected using the partner_control value, the secret 32-character key.
	 * Using hash method: SHA-1.
	 * @return hash string.
	 */
	@Deprecated
	public String createInatecIframeChecksum() {
		String checksum = "";
		//TODO get to hash/array to easier sorting.
		//need to be ordered by parameter name!!!
		checksum = 
				(this.depositAmount +
				this.city +
				this.countryA3 +
				this.currency +
				this.ip +
				this.email +
				this.firstName +
				this.lastName +
				this.merchantAccount +
				this.transactionId +
				this.address +
				//this.urlFailed +
				this.urlInternal +
				this.urlReturn +
				//this.urlSuccess +
				this.zip +
				this.privateKey);

		log.info("inatectIframe checksum creation: (Before) " + checksum);
		try {
			checksum = Sha1.encode(checksum);
			log.info("inatectIframe checksum creation: (After SHA-1): " + checksum);
		} catch (Exception e) {
			log.error("ERROR! Can't create checksum for inatectIframe using SHA-1 algorithm. creation", e);
		}
		return checksum;
	}
	
	/**
	 * Create inatectIframe checksum for the result authentication.
	 * Using hash method: SHA-1. 
	 * @return
	 */
	public String resultChecksumAuthentication() {
		String checksum = "";
		checksum = "";
		log.info("inatectIframe checksum authentication result string (Before SHA-1): " + checksum);
		try {
			checksum = Sha1.encode(checksum);
			log.info("inatectIframe checksum authentication result string (After SHA-1): " + checksum);
		} catch (Exception e) {
			log.error("ERROR! Can't create checksum for inatectIframe using SHA-1 algorithm. authenticat", e);
		}	
		return checksum;
	}
	
	@Deprecated
	public String createBody() {
		//TODO hash/array ordered
		String body =
				"merchantid=" + this.merchantAccount + "&" +
				"orderid=" + this.transactionId + "&" +
				"amount=" + this.depositAmount + "&" +
				"currency=" + this.currency + "&" +
				//"custom2=" + this.custom2 + "&" +
				"firstname=" + this.firstName + "&" +
				"lastname=" + this.lastName + "&" +
				"street=" + this.address + "&" +
				"zip=" + this.zip + "&" +
				"city=" + this.city + "&" +
				"country=" + this.countryA3 + "&" +
				"email=" + this.email + "&" +
				"customerip=" + this.ip + "&" +
				//"url_failed=" + this.urlFailed + "&" +
				"url_internal=" + this.urlInternal + "&" +
				"url_return=" + this.urlReturn + "&" +
				//"url_success=" + this.urlSuccess + "&" +
				"signature=" + this.signature;
		return body;
	}
	
	//TODO remove assumption. 
	/**
	 * assumption: signature not empty.
	 * @param maps
	 * @return
	 */
	public StringBuilder createBodyBuilder() {
		StringBuilder body = new StringBuilder();
		String ampersand = "";
		for(Map.Entry<String, String> entry : infoParamMap.entrySet()) {		
			body.append(ampersand).append(entry.getKey()).append("=").append(entry.getValue());
			 ampersand="&";
		}
		body.append("&").append("signature=").append(this.signature);
		return body;
	}

/*		*//**
	 * @return the merchantId
	 *//*
	public String getMerchantId() {
		return merchantId;
	}

	*//**
	 * @param merchantId the merchantId to set
	 *//*
		public void setMerchantId(String merchantId) {
			this.merchantId = merchantId;
		}
*/
	/**
	 * @return the countryA3
	 */
	public String getCountryA3() {
		return countryA3;
	}

	/**
	 * @param countryA3 the countryA3 to set
	 */
	public void setCountryA3(String countryA3) {
		this.countryA3 = countryA3;
	}

	/**
	 * @return the signature
	 */
	public String getSignature() {
		return signature;
	}

	/**
	 * @param signature the signature to set
	 */
	public void setSignature(String signature) {
		this.signature = signature;
	}

	/**
	 * @return the privateKey
	 */
	public String getPrivateKey() {
		return privateKey;
	}

	/**
	 * @param privateKey the privateKey to set
	 */
	public void setPrivateKey(String privateKey) {
		this.privateKey = privateKey;
	}
	
	/**
	 * @return the merchantAccount
	 */
	public String getMerchantAccount() {
		return merchantAccount;
	}

	/**
	 * @param merchantAccount the merchantAccount to set
	 */
	public void setMerchantAccount(String merchantAccount) {
		this.merchantAccount = merchantAccount;
	}

	/**
	 * @return the redirectURL
	 */
	public String getRedirectURL() {
		return redirectURL;
	}

	/**
	 * @param redirectURL the redirectURL to set
	 */
	public void setRedirectURL(String redirectURL) {
		this.redirectURL = redirectURL;
	}

	/**
	 * @return the language
	 */
	public String getLanguage() {
		return language;
	}

	/**
	 * @param language the language to set
	 */
	public void setLanguage(String language) {
		this.language = language;
	}

	/**
	 * @return the currency
	 */
	public String getCurrency() {
		return currency;
	}

	/**
	 * @param currency the currency to set
	 */
	public void setCurrency(String currency) {
		this.currency = currency;
	}

	/**
	 * @return the depositAmount
	 */
	public String getDepositAmount() {
		return depositAmount;
	}

	/**
	 * @param depositAmount the depositAmount to set
	 */
	public void setDepositAmount(String depositAmount) {
		this.depositAmount = depositAmount;
	}
	
	/**
	 * @param requestParameterMap
	 * @return
	 */
	public boolean handleStatusUrl(Map<?, ?> requestParameterMap) {
		log.debug("START: inatecIframe function: handleStatusUrl");
		
		try {
			this.requestStatus = (String)requestParameterMap.get("REQUESTSTATUS");
			this.tag = (String)requestParameterMap.get("TAG");
			this.txid = (String)requestParameterMap.get("TXID");
			this.paymentGuarantee = (String)requestParameterMap.get("PAYMENTGUARANTEE");
			this.status = (String)requestParameterMap.get("STATUS");
			this.errmsg = (String)requestParameterMap.get("ERRMSG");
			this.hash = (String)requestParameterMap.get("HASH");
			responseComment = "| REQUESTSTATUS: " + requestStatus +
					" | TAG: " + tag +
					" | TXID:" + txid +
					" | PAYMENTGUARANTEE:" + paymentGuarantee + 
					" | STATUS:" + status + 
					" | ERRMSG:" + errmsg + 
					" | HASH:" + hash; 
		} catch (Exception e){
			log.error("Error retrieving data from inatecIframe status URL " + e);
			return false;
		}
		log.debug("END: inatecIframe function: handleStatusUrl; responseComment: " + responseComment); 
		return true;
	}
	
	/**
	 * @return the urlSuccess
	 */
	public String getUrlSuccess() {
		return urlSuccess;
	}


	/**
	 * @param urlSuccess the urlSuccess to set
	 */
	public void setUrlSuccess(String urlSuccess) {
		this.urlSuccess = urlSuccess;
	}


	/**
	 * @return the urlFailed
	 */
	public String getUrlFailed() {
		return urlFailed;
	}


	/**
	 * @param urlFailed the urlFailed to set
	 */
	public void setUrlFailed(String urlFailed) {
		this.urlFailed = urlFailed;
	}


	/**
	 * @return the urlInternal
	 */
	public String getUrlInternal() {
		return urlInternal;
	}


	/**
	 * @param urlInternal the urlInternal to set
	 */
	public void setUrlInternal(String urlInternal) {
		this.urlInternal = urlInternal;
	}


	/**
	 * @return the urlReturn
	 */
	public String getUrlReturn() {
		return urlReturn;
	}


	/**
	 * @param urlReturn the urlReturn to set
	 */
	public void setUrlReturn(String urlReturn) {
		this.urlReturn = urlReturn;
	}


	/**
	 * @return the requestStatus
	 */
	public String getRequestStatus() {
		return requestStatus;
	}


	/**
	 * @param requestStatus the requestStatus to set
	 */
	public void setRequestStatus(String requestStatus) {
		this.requestStatus = requestStatus;
	}


	/**
	 * @return the tag
	 */
	public String getTag() {
		return tag;
	}


	/**
	 * @param tag the tag to set
	 */
	public void setTag(String tag) {
		this.tag = tag;
	}


	/**
	 * @return the txid
	 */
	public String getTxid() {
		return txid;
	}


	/**
	 * @param txid the txid to set
	 */
	public void setTxid(String txid) {
		this.txid = txid;
	}


	/**
	 * @return the paymentGuarantee
	 */
	public String getPaymentGuarantee() {
		return paymentGuarantee;
	}


	/**
	 * @param paymentGuarantee the paymentGuarantee to set
	 */
	public void setPaymentGuarantee(String paymentGuarantee) {
		this.paymentGuarantee = paymentGuarantee;
	}


	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}


	/**
	 * @param status the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}


	/**
	 * @return the errmsg
	 */
	public String getErrmsg() {
		return errmsg;
	}


	/**
	 * @param errmsg the errmsg to set
	 */
	public void setErrmsg(String errmsg) {
		this.errmsg = errmsg;
	}


	/**
	 * @return the hash
	 */
	public String getHash() {
		return hash;
	}


	/**
	 * @param hash the hash to set
	 */
	public void setHash(String hash) {
		this.hash = hash;
	}


	/**
	 * @return the responseComment
	 */
	public String getResponseComment() {
		return responseComment;
	}


	/**
	 * @param responseComment the responseComment to set
	 */
	public void setResponseComment(String responseComment) {
		this.responseComment = responseComment;
	}


	/**
	 * @return the transactionStatusId
	 */
	public long getTransactionStatusId() {
		return transactionStatusId;
	}


	/**
	 * @param transactionStatusId the transactionStatusId to set
	 */
	public void setTransactionStatusId(long transactionStatusId) {
		this.transactionStatusId = transactionStatusId;
	}

	/**
	 * @return the custom2
	 */
	public String getCustom2() {
		return custom2;
	}

	/**
	 * @param custom2 the custom2 to set
	 */
	public void setCustom2(String custom2) {
		this.custom2 = custom2;
	}

	/**
	 * @return the paymentMethod
	 */
	public int getPaymentMethod() {
		return paymentMethod;
	}

	/**
	 * @param paymentMethod the paymentMethod to set
	 */
	public void setPaymentMethod(int paymentMethod) {
		this.paymentMethod = paymentMethod;
	}
	
	public static void main(String args[]) {
		 Map<String, String> infoParamMap = new TreeMap<String, String>();
		infoParamMap.put("merchantid", "1");
		infoParamMap.put("orderid", "2");
		infoParamMap.put("amount", "3");
		infoParamMap.put("currency", "4");
		infoParamMap.put("firstname", "5");
		infoParamMap.put("lastname", "6");
		infoParamMap.put("street", "7");
		infoParamMap.put("zip", "8");
		infoParamMap.put("city", "9");
		infoParamMap.put("country", "10");
		infoParamMap.put("email", "11");
		infoParamMap.put("custom2", "12");
		infoParamMap.put("customerip", "13");
		infoParamMap.put("payment_method", "14");
		infoParamMap.put("url_internal", "15");
		infoParamMap.put("url_return", "16");
		infoParamMap.put("bankcountry", "17");
		infoParamMap.put("accountname", "18");
		String  ampersand="&";
		StringBuilder body = new StringBuilder();
		for(Map.Entry<String, String> entry : infoParamMap.entrySet()) {		
			body.append(ampersand).append(entry.getKey()).append("=").append(entry.getValue());
		}
		System.out.println(infoParamMap);
	}
}