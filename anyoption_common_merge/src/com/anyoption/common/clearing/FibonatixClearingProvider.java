package com.anyoption.common.clearing;

import java.net.URLDecoder;
import java.util.HashMap;

import org.apache.log4j.Logger;

import com.anyoption.common.util.ClearingUtil;
import com.anyoption.common.util.Sha1;

public class FibonatixClearingProvider extends ClearingProvider {
	
	private static final Logger log = Logger.getLogger(FibonatixClearingProvider.class);

	String paymentDescription;

	String urlStatus; 
	String suffixReturn;

	int poll_count_auth = 3;
	int poll_interval_auth = 3100;
	
	int poll_count_capture = 20;
	int poll_interval_capture = 3100;

	private static final String STATUS_APPROVED		= "approved";
	private static final String STATUS_DECLINED		= "declined";
	private static final String STATUS_ERROR 		= "error";
	private static final String STATUS_FILTERED 	= "filtered";
	private static final String STATUS_PROCESSING 	= "processing";
	private static final String STATUS_UNKNOWN 		= "unknown";
	
	/**
	 * parses configuration from clearing_providers data
	 */
	@Override
	public void setConfig(ClearingProviderConfig config) throws ClearingException {
		super.setConfig(config);
        try {
	        if (null != config.getProps() && !config.getProps().trim().equals("")) {
	            String[] ps = config.getProps().split(";");
	            for (int i = 0; i < ps.length; i++) {
	                String[] p = ps[i].split("=");
	                if (p[0].equals("poll_count_auth")) {
	                	poll_count_auth = Integer.parseInt(p[1]);
	                } else if (p[0].equals("poll_interval_auth")) {
	                	poll_interval_auth = Integer.parseInt(p[1]);
	                }  else if (p[0].equals("poll_count_capture")) {
	                	poll_count_capture = Integer.parseInt(p[1]);
	                }  else if (p[0].equals("poll_interval_capture")) {
	                	poll_interval_capture = Integer.parseInt(p[1]);
	                } else if (p[0].equals("paymentDescription")) {
	                	paymentDescription = p[1];
	                } else if (p[0].equals("urlStatus")) {
	                	urlStatus = p[1];
	                } else if (p[0].equals("suffixReturn")) {
	                	suffixReturn = p[1];
	                }
	            }
	        }
        } catch (Exception e) {
        	log.error("ERROR! set config - job?", e);
        }
	}
	
	@Override
	public void authorize(ClearingInfo info) throws ClearingException {
		try {
			process(info, "preauth");
			if(info.isSuccessful()) {
				for(int i =0 ;i<poll_count_auth;i++) {
					getStatus(info);
					if(ClearingInfo.NEEDS_POLLING.equals(info.getResult())) {
						Thread.sleep(poll_interval_auth);
					} else {
						return;
					}
				}
			}
		} catch (Exception e) {
			log.error("Failed to authorize:", e);
			throw new ClearingException(e.getMessage());
		}
	}

	@Override
	public void capture(ClearingInfo info) throws ClearingException {
		try {
			String request = 
					"login=" + username +
					"&client_orderid=" +   info.getTransactionId()  +
					"&orderid=" +  info.getAuthNumber()  +
					"&control=" +  Sha1.encode(username+info.getTransactionId()+info.getAuthNumber()+privateKey);
			String response = ClearingUtil.executePOSTRequest(url + "capture" + "/group/" + depositTerminalId, request);

			if(parseResponse(info, response)) {
				for(int i=0;i<poll_count_capture;i++) {
					getStatus(info);
					if(ClearingInfo.NEEDS_POLLING.equals(info.getResult())) {
						Thread.sleep(poll_interval_capture);
					} else {
						return;
					}
				}
			}
		} catch (Exception e) {
			log.error("Failed to capture:", e);
			throw new ClearingException(e.getMessage());
		}
	}

	@Override
	public void cancel(ClearingInfo info) throws ClearingException {
		try {
			String request = 
					"login=" + username +
					"&orderid=" + info.getAcquirerResponseId() +
					"&client_orderid=" + info.getTransactionId() +
					"&control=" + Sha1.encode(username + info.getTransactionId() + info.getAcquirerResponseId()+ privateKey) +
					"&comment=n/a" ;  
			String response = ClearingUtil.executePOSTRequest(url + "return" + "/group/" + depositTerminalId, request);

			if(parseResponse(info, response)) {
				for(int i =0 ;i<poll_count_auth;i++) {
					getStatus(info);
					if(ClearingInfo.NEEDS_POLLING.equals(info.getResult())) {
						Thread.sleep(poll_interval_auth);
					} else {
						return;
					}
				}
			}
		} catch (Exception e) {
			log.error("cancel failed:"+info, e);
			throw new ClearingException(e.getMessage());
		}
	}
	
	@Override
	public void enroll(ClearingInfo info) throws ClearingException {
		log.error("enroll is not supported opperation");
		info.setSuccessful(false);
	}

	@Override
	public void purchase(ClearingInfo info) throws ClearingException {
		log.error("purchase is not supported opperation");
		info.setSuccessful(false);
	}

	@Override
	public void withdraw(ClearingInfo info) throws ClearingException {
			log.error("CFT not supported");
			throw new ClearingException("CFT not supported");
	}

	@Override
	public void bookback(ClearingInfo info) throws ClearingException {
		try {
			String request = 
					"login=" + username +
					"&orderid=" + info.getAuthNumber() +
					"&client_orderid=" + info.getTransactionId() +
					"&amount=" +  info.getAmount()/100  + 
					"&currency=" +  info.getCurrencySymbol()  +
					// login + client_orderid + orderid + amount_in_cents + currency + merchant-control
					"&control=" + Sha1.encode(username + info.getTransactionId() + info.getAuthNumber() + info.getAmount() + info.getCurrencySymbol() + privateKey) +
					"&comment=n/a" ;  
			String response = ClearingUtil.executePOSTRequest(url + "return" + "/group/" + depositTerminalId, request);

			if(parseResponse(info, response)) {
				for(int i =0 ;i<poll_count_auth;i++) {
					getStatus(info);
					if(ClearingInfo.NEEDS_POLLING.equals(info.getResult())) {
						Thread.sleep(poll_interval_auth);
					} else {
						return;
					}
				}
			}
		} catch (Exception e) {
			log.error("cancel failed:"+info, e);
			throw new ClearingException(e.getMessage());
		}
	}
	/**
	 * 
	 * @param info
	 * @throws Exception
	 * 
	 * Control : 
	 * 		ENDPOINTID/ENDPOINTGROUPID 
	 * 		client_orderid
	 * 		minimal monetary units amount (i.e. cent, penny etc.)
	 * 		email
	 * 		merchant_control
	 */
	private void process(ClearingInfo info, String operation) throws Exception {
		String request = 
				"client_orderid=" +  info.getTransactionId()  +
				"&order_desc=" +  paymentDescription  +
				"&card_printed_name=" +  info.getOwner()  +
				"&first_name=" +  info.getFirstName()  +
				"&last_name=" +  info.getLastName()  +
				"&address1=" +  info.getAddress()  +
				"&city=" +  info.getCity()  +
				"&zip_code=" +  info.getZip()  +
				"&country=" +  info.getCountryA2()  +
				"&phone=" +  info.getUserPhone()  +
				"&email=" +  info.getEmail()  +
				"&amount=" +  info.getAmount()/100  + 
				"&currency=" +  info.getCurrencySymbol()  + 
				"&credit_card_number=" +  info.getCcn()  +
				"&expire_month=" +  info.getExpireMonth()  +
				"&expire_year=" +  "20"+info.getExpireYear()  +
				"&cvv2=" +  info.getCvv()  +
				"&ipaddress=" +  info.getIp()  +
				"&control=" + Sha1.encode(depositTerminalId+info.getTransactionId()+info.getAmount()+info.getEmail()+privateKey) +    
				"&redirect_url=" + info.getHomePageUrl()+suffixReturn ;
		if(urlStatus != null && urlStatus.length() > 0 ) {
				request += "&server_callback_url=" + urlStatus; 
		}
		
		log.debug("request:"+request);
		String response = ClearingUtil.executePOSTRequest(url + operation + "/group/" + depositTerminalId, request);
		parseResponse(info, response);
	}
	
	private boolean parseResponse(ClearingInfo info, String response) {
		HashMap<String, String> responseParams = queryToMap(response);
		if(responseParams != null) {
			String paynetOrderId = responseParams.get("paynet-order-id");
			String serialNumber = responseParams.get("serial-number");
			info.setAuthNumber(paynetOrderId);
			info.setAcquirerResponseId(paynetOrderId);
			info.setProviderTransactionId(serialNumber);
			String type = responseParams.get("type");
			
			if(type.indexOf("error")< 0) {
				info.setSuccessful(true);
				return true;
			} else {
				log.error(response);
				info.setMessage(responseParams.get("error-message" ));
				setFailed(info);
			}
		} else {
			log.error("Failed to process transaction:"+response);
			setFailed(info);
		}
		return false;
	}
	/**
	 * @param info
	 * @throws Exception
	 * 
	 * 		Merchant login name
	 * 		Merchant order identifier of the transaction for which the status is requested
	 * 		Order id assigned to the order by Fibonatix
	 * 		Serial number assigned to the specific request by Fibonatix. 
	 * 		This is SHA-1 checksum of the concatenation login + client-order-id + paynet-order-id + merchant-control. 
	 */
	public void getStatus(ClearingInfo info) throws Exception {
		String request = 
		"login=" + username	+							 
		"&client_orderid=" + info.getTransactionId() +	 
		"&orderid=" + info.getAuthNumber() +	 
		"&control=" + Sha1.encode(username+info.getTransactionId()+info.getAuthNumber() + privateKey)+
		"&by-request-sn=" + info.getProviderTransactionId(); 
		
		String response = ClearingUtil.executePOSTRequest(url + "status" + "/group/" + depositTerminalId, request);
		log.info(response);
		HashMap<String, String> params = queryToMap(response);
		String status = params.get("status"); 
		if(STATUS_APPROVED.equals(status)) {
			info.setSuccessful(true);
			info.setResult("1000");
			if("AUTHENTICATED".equals(params.get("verified-3d-status"))) {
				info.setFormData(params.get("html"));
				info.setIs3dSecure(true);
			}

			info.setProviderTransactionId(params.get("by-request-sn"));
			info.setMessage("Captured [receipt:"+params.get("receipt-id")+"]");
			info.setUserMessage("[reversal:"+params.get("gate-partial-reversal")+"]");
		} else if(STATUS_DECLINED.equals(status) || STATUS_ERROR.equals(status) || STATUS_FILTERED.equals(status) || STATUS_UNKNOWN.equals(status) ) {
			info.setSuccessful(false);
			info.setResult(params.get("error-code"));
			String errorMessage = params.get("error-message");
			if(errorMessage != null) {
				info.setMessage(URLDecoder.decode(errorMessage, "UTF-8"));
			}
		} else if (STATUS_PROCESSING.equals(status)) {
			// needs polling
			info.setSuccessful(false);
			info.setNotRerouting(true);
			String formData = params.get("html");
			if(formData != null && formData.length() > 0) {
				info.setFormData(URLDecoder.decode(formData, "UTF-8"));
				info.setSuccessful(true);
				info.setIs3dSecure(true);
				info.setResult("");
				info.setUserMessage("3d");
				info.setProviderTransactionId(params.get("by-request-sn"));
			} else {
				info.setMessage("polling");
				info.setResult(ClearingInfo.NEEDS_POLLING);
			}
		} else {
			log.error("UNKNOWN STATUS:"+ status);
			setFailed(info);
		}
		//&receipt-id=2050-3c93-a061-8a19b6c0068f
		//&by-request-sn=00000000-0000-0000-0000-0000005b5ece
		//info.setAuthNumber(params.get("paynet-order-id"));
	}
	
	private HashMap<String, String> queryToMap(String response) { 
		HashMap<String, String> parameters = null;
		if(response!=null 
				&& response.length() > 0 
					&& response.indexOf("&") > 0 
						&& response.indexOf("=") > 0) {
			parameters = new HashMap<>();
			String[] params = response.split("&");
			for(String p : params) {
				String[] g = p.split("=");
				parameters.put(g[0], g[1]);
			}
		}
		return parameters;
	}

	private void setFailed(ClearingInfo info) {
		info.setSuccessful(false);
		info.setResult("-1");
		info.setUserMessage("-1 | Unknown Error");
	}
	
	public boolean isValidCall(String control, String status, String orderid, String clientOrderid) {
		String checksum = "";
		try {
			checksum = Sha1.encode(status+orderid+clientOrderid+privateKey);
		} catch (Exception e) {
			log.error("Sha1 failed", e);
		}
		return checksum.equals(control);
	}
}

