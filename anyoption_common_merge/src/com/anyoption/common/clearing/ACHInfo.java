package com.anyoption.common.clearing;

import com.anyoption.common.beans.Transaction;
import com.anyoption.common.beans.User;

/**
 * ACH clearing info class
 * @author Asher
 *
 */
public class ACHInfo extends ClearingInfo {

    // for ACH
	private String payeeName;
	private String payeeEmail;
	private String payeeAddress;
    private String payeeCity;
    private String payeeStateProvince;
    private String payeeZipCode;
    private String payeePhone;
    private String currency;
    private String routingNo;
    private String accountNo;
    private String status;
    private String statusCode;
    private String statusMessage;

    public ACHInfo() {
    	providerId = ClearingManagerConstants.ACH_PROVIDER_ID;
    }

    public ACHInfo(User user, Transaction t){
    	super(user, t.getAmount(), t.getTypeId(), t.getId(), t.getIp(), t.getAcquirerResponseId());
    	providerId = ClearingManagerConstants.ACH_PROVIDER_ID;
    	setDetailsByUser(user);
    }

    public ACHInfo(User user){
    	providerId = ClearingManagerConstants.ACH_PROVIDER_ID;
    	setDetailsByUser(user);
    }

    private void setDetailsByUser(User user) {
    	payeeName = user.getFirstName() + " " + user.getLastName();
    	payeeEmail = user.getEmail();
    	payeeAddress = user.getStreetNo() + " " + user.getStreet();
    	payeeCity = user.getCityName();
    	payeeStateProvince = "NY";
        payeeZipCode = user.getZipCode();
        payeePhone = user.getMobilePhone();
        currency = user.getCurrency().getCode();
    }

	/**
	 * @return the city
	 */
	public String getCity() {
		return payeeCity;
	}
	/**
	 * @param city the city to set
	 */
	public void setCity(String city) {
		this.payeeCity = city;
	}

	/**
	 * Implement toString function
	 */
	   public String toString() {
	        String ls = System.getProperty("line.separator");
	        String superToStr = super.toString();  // base toString
	        return ls + superToStr + ls + "ACH Info:" + ls +
	        	"payeeName:" + payeeName + ls +
		    	"payeeEmail:" + payeeEmail + ls +
		    	"payeeAddress:" + payeeAddress + ls +
		    	"payeeCity:" + payeeCity + ls +
		    	"payeeStateProvince:" + payeeStateProvince + ls +
		    	"payeeZipCode:" + payeeZipCode + ls +
		    	"payeePhone:" + payeePhone + ls +
		    	"routingNo:" + routingNo + ls +
		    	"accountNo:" + accountNo + ls +
		    	"status:" + status + ls +
		    	"statusCode:" + statusCode + ls +
		    	"statusMessage:" + statusMessage + ls;
	    }

	public String getAccountNo() {
		return accountNo;
	}

	public void setAccountNo(String accountNo) {
		this.accountNo = accountNo;
	}

	public String getRoutingNo() {
		return routingNo;
	}

	public void setRoutingNo(String routingNo) {
		this.routingNo = routingNo;
	}

	public String getPayeeAddress() {
		return payeeAddress;
	}

	public void setPayeeAddress(String payeeAddress) {
		this.payeeAddress = payeeAddress;
	}

	public String getPayeeCity() {
		return payeeCity;
	}

	public void setPayeeCity(String payeeCity) {
		this.payeeCity = payeeCity;
	}

	public String getPayeeEmail() {
		return payeeEmail;
	}

	public void setPayeeEmail(String payeeEmail) {
		this.payeeEmail = payeeEmail;
	}

	public String getPayeeName() {
		return payeeName;
	}

	public void setPayeeName(String payeeName) {
		this.payeeName = payeeName;
	}

	public String getPayeePhone() {
		return payeePhone;
	}

	public void setPayeePhone(String payeePhone) {
		this.payeePhone = payeePhone;
	}

	public String getPayeeZipCode() {
		return payeeZipCode;
	}

	public void setPayeeZipCode(String payeeZipCode) {
		this.payeeZipCode = payeeZipCode;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}

	public String getStatusMessage() {
		return statusMessage;
	}

	public void setStatusMessage(String statusMessage) {
		this.statusMessage = statusMessage;
	}

	public String getPayeeStateProvince() {
		return payeeStateProvince;
	}

	public void setPayeeStateProvince(String payeeStateProvince) {
		this.payeeStateProvince = payeeStateProvince;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

}
