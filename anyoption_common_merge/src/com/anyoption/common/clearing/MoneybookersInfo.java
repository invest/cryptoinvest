package com.anyoption.common.clearing;


import java.io.Serializable;
import java.sql.SQLException;
import java.util.Map;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.User;
import com.anyoption.common.util.CommonUtil;
import com.anyoption.common.util.ConstantsBase;

/**
 * Moneybookers clearing info class
 * @author Eran
 *
 */

public class MoneybookersInfo extends ClearingInfo implements Serializable {

	private static final long serialVersionUID = 1L;

	private static final Logger log = Logger.getLogger(MoneybookersInfo.class);
	private final String CANCEL_URL = "jsp/pages/moneybookersCancelURL.jsf";
	private final String STATUS_URL = "jsp/moneybookersStatusURL.jsf";
	private final String RETURN_URL = "jsp/pages/moneybookersReturnURL.jsf";
	public static final String SECRET_WORD_TEST = "1qazx";
	public static final String SECRET_WORD_LIVE = "3edcx";
	private String redirectURL;
	private String merchantAccount;
	private String language;
	private String currency;
	private String cancelURL;
	private String returnURL;
	private String statusURL;
	private String logoURL;
	private String clientAccount;
	private String depositAmount;
	private String mailStatus;

	//status URL fields
	private String pay_to_email;
	private String pay_from_email;
	private String merchant_id;
	private String customer_id;
	private String transaction_id;
	private String mb_transaction_id;
	private String mb_amount;
	private String mb_currency;
	private String failed_reason_code;
	private String md5sig;
	private String transactionAmount;
	private String transactionCurrency;
	private String payment_type;
	private String merchant_fields;
	private String status;


	public MoneybookersInfo() {
    }

	public MoneybookersInfo(User user, long transactionAmount, long transactionTypeId, long transId, String transactionIp,
							String transactionAcquirerResponseId, String language, String hpURL, String clientAccount,
							String amount, boolean isLive, String imagesDomain) throws SQLException {
    	super(user, transactionAmount, transactionTypeId, transId, transactionIp, transactionAcquirerResponseId);
    	this.language = language;
    	if (isLive){
    		this.statusURL = hpURL + STATUS_URL;
    		this.mailStatus = "payments@anyoption.com";
    	} else {
    		this.statusURL = "http://www.testenv.anyoption.com/" + STATUS_URL;
    		this.mailStatus = "eranl@anyoption.com";
    	}
    	this.cancelURL = hpURL + CANCEL_URL;
    	this.returnURL = hpURL + RETURN_URL;
    	this.currency = user.getCurrency().getCode();
    	this.logoURL = imagesDomain + "/header_anyoption_597.gif";
    	this.clientAccount = clientAccount;
    	this.depositAmount = amount;
    }

	/**
	 * @return the merchantAccount
	 */
	public String getMerchantAccount() {
		return merchantAccount;
	}

	/**
	 * @param merchantAccount the merchantAccount to set
	 */
	public void setMerchantAccount(String merchantAccount) {
		this.merchantAccount = merchantAccount;
	}

	/**
	 * @return the redirectURL
	 */
	public String getRedirectURL() {
		return redirectURL;
	}

	/**
	 * @param redirectURL the redirectURL to set
	 */
	public void setRedirectURL(String redirectURL) {
		this.redirectURL = redirectURL;
	}

	/**
	 * @return the cancelURL
	 */
	public String getCancelURL() {
		return cancelURL;
	}

	/**
	 * @param cancelURL the cancelURL to set
	 */
	public void setCancelURL(String cancelURL) {
		this.cancelURL = cancelURL;
	}

	/**
	 * @return the language
	 */
	public String getLanguage() {
		return language;
	}

	/**
	 * @param language the language to set
	 */
	public void setLanguage(String language) {
		this.language = language;
	}

	/**
	 * @return the returnURL
	 */
	public String getReturnURL() {
		return returnURL;
	}

	/**
	 * @param returnURL the returnURL to set
	 */
	public void setReturnURL(String returnURL) {
		this.returnURL = returnURL;
	}

	/**
	 * @return the statusURL
	 */
	public String getStatusURL() {
		return statusURL;
	}

	/**
	 * @param statusURL the statusURL to set
	 */
	public void setStatusURL(String statusURL) {
		this.statusURL = statusURL;
	}

	/**
	 * @return the currency
	 */
	public String getCurrency() {
		return currency;
	}

	/**
	 * @param currency the currency to set
	 */
	public void setCurrency(String currency) {
		this.currency = currency;
	}

	/**
	 * @return the logoURL
	 */
	public String getLogoURL() {
		return logoURL;
	}

	/**
	 * @param logoURL the logoURL to set
	 */
	public void setLogoURL(String logoURL) {
		this.logoURL = logoURL;
	}

	/**
	 * @return the clientAccount
	 */
	public String getClientAccount() {
		return clientAccount;
	}

	/**
	 * @param clientAccount the clientAccount to set
	 */
	public void setClientAccount(String clientAccount) {
		this.clientAccount = clientAccount;
	}

	/**
	 * @return the depositAmount
	 */
	public String getDepositAmount() {
		return depositAmount;
	}

	/**
	 * @param depositAmount the depositAmount to set
	 */
	public void setDepositAmount(String depositAmount) {
		this.depositAmount = depositAmount;
	}

	/**
	 * @return the customer_id
	 */
	public String getCustomer_id() {
		return customer_id;
	}

	/**
	 * @param customer_id the customer_id to set
	 */
	public void setCustomer_id(String customer_id) {
		this.customer_id = customer_id;
	}

	/**
	 * @return the failed_reason_code
	 */
	public String getFailed_reason_code() {
		return failed_reason_code;
	}

	/**
	 * @param failed_reason_code the failed_reason_code to set
	 */
	public void setFailed_reason_code(String failed_reason_code) {
		this.failed_reason_code = failed_reason_code;
	}

	/**
	 * @return the mb_amount
	 */
	public String getMb_amount() {
		return mb_amount;
	}

	/**
	 * @param mb_amount the mb_amount to set
	 */
	public void setMb_amount(String mb_amount) {
		this.mb_amount = mb_amount;
	}

	/**
	 * @return the mb_currency
	 */
	public String getMb_currency() {
		return mb_currency;
	}

	/**
	 * @param mb_currency the mb_currency to set
	 */
	public void setMb_currency(String mb_currency) {
		this.mb_currency = mb_currency;
	}

	/**
	 * @return the mb_transaction_id
	 */
	public String getMb_transaction_id() {
		return mb_transaction_id;
	}

	/**
	 * @param mb_transaction_id the mb_transaction_id to set
	 */
	public void setMb_transaction_id(String mb_transaction_id) {
		this.mb_transaction_id = mb_transaction_id;
	}

	/**
	 * @return the md5sig
	 */
	public String getMd5sig() {
		return md5sig;
	}

	/**
	 * @param md5sig the md5sig to set
	 */
	public void setMd5sig(String md5sig) {
		this.md5sig = md5sig;
	}

	/**
	 * @return the merchant_fields
	 */
	public String getMerchant_fields() {
		return merchant_fields;
	}

	/**
	 * @param merchant_fields the merchant_fields to set
	 */
	public void setMerchant_fields(String merchant_fields) {
		this.merchant_fields = merchant_fields;
	}

	/**
	 * @return the merchant_id
	 */
	public String getMerchant_id() {
		return merchant_id;
	}

	/**
	 * @param merchant_id the merchant_id to set
	 */
	public void setMerchant_id(String merchant_id) {
		this.merchant_id = merchant_id;
	}

	/**
	 * @return the pay_from_email
	 */
	public String getPay_from_email() {
		return pay_from_email;
	}

	/**
	 * @param pay_from_email the pay_from_email to set
	 */
	public void setPay_from_email(String pay_from_email) {
		this.pay_from_email = pay_from_email;
	}

	/**
	 * @return the pay_to_email
	 */
	public String getPay_to_email() {
		return pay_to_email;
	}

	/**
	 * @param pay_to_email the pay_to_email to set
	 */
	public void setPay_to_email(String pay_to_email) {
		this.pay_to_email = pay_to_email;
	}

	/**
	 * @return the payment_type
	 */
	public String getPayment_type() {
		return payment_type;
	}

	/**
	 * @param payment_type the payment_type to set
	 */
	public void setPayment_type(String payment_type) {
		this.payment_type = payment_type;
	}

	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @param status the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * @return the transaction_id
	 */
	public String getTransaction_id() {
		return transaction_id;
	}

	/**
	 * @param transaction_id the transaction_id to set
	 */
	public void setTransaction_id(String transaction_id) {
		this.transaction_id = transaction_id;
	}

	/**
	 * @return the transactionAmount
	 */
	public String getTransactionAmount() {
		return transactionAmount;
	}

	/**
	 * @param transactionAmount the transactionAmount to set
	 */
	public void setTransactionAmount(String transactionAmount) {
		this.transactionAmount = transactionAmount;
	}

	/**
	 * @return the transactionCurrency
	 */
	public String getTransactionCurrency() {
		return transactionCurrency;
	}

	/**
	 * @param transactionCurrency the transactionCurrency to set
	 */
	public void setTransactionCurrency(String transactionCurrency) {
		this.transactionCurrency = transactionCurrency;
	}

	/**
	 * @return the mailStatus
	 */
	public String getMailStatus() {
		return mailStatus;
	}

	/**
	 * @param mailStatus the mailStatus to set
	 */
	public void setMailStatus(String mailStatus) {
		this.mailStatus = mailStatus;
	}

	/**
	 * Handle status URL from Moneybookers
	 * @param requestParameterMap
	 * @return
	 */
	public boolean handleStatusUrl(Map<String, String> requestParameterMap){
		try {
		 pay_to_email = (String) requestParameterMap.get("pay_to_email");
		 pay_from_email = (String) requestParameterMap.get("pay_from_email");
		 merchant_id = (String) requestParameterMap.get("merchant_id");
		 customer_id = (String) requestParameterMap.get("customer_id");
		 transaction_id = (String) requestParameterMap.get("transaction_id");
		 mb_transaction_id = (String) requestParameterMap.get("mb_transaction_id");
		 mb_amount = (String) requestParameterMap.get("mb_amount");
		 mb_currency = (String) requestParameterMap.get("mb_currency");
		 failed_reason_code = (String) requestParameterMap.get("failed_reason_code");
		 md5sig = (String) requestParameterMap.get("md5sig");
		 transactionAmount = (String) requestParameterMap.get("amount");
		 currency = (String) requestParameterMap.get("currency");
		 payment_type = (String) requestParameterMap.get("payment_type");
		 merchant_fields = (String) requestParameterMap.get("merchant_fields");
		 status = (String) requestParameterMap.get("status");

		 if (CommonUtil.isParameterEmptyOrNull(failed_reason_code)){
			 failed_reason_code = ConstantsBase.EMPTY_STRING;
		 }

		 message = "Status=" + returnStatusString() + "|FailedReasonCode=" + failed_reason_code + "|PaymentType=" + payment_type + "|customerId=" + customer_id + "|mbAmount=" + mb_amount;
		 String TAB = "\n";
			log.info( TAB + "Moneybookers status_url fields:" + TAB
					+ "pay_to_email=" + pay_to_email + TAB
					+ "pay_from_email=" + pay_from_email + TAB
					+ "merchant_id=" + merchant_id + TAB
					+ "customer_id=" + customer_id + TAB
					+ "transaction_id=" + transaction_id + TAB
					+ "mb_transaction_id=" + mb_transaction_id + TAB
					+ "mb_amount=" + mb_amount + TAB
					+ "mb_currency=" + mb_currency + TAB
					+ "failed_reason_code=" + failed_reason_code + TAB
					+ "md5sig=" + md5sig + TAB
					+ "amount=" + transactionAmount + TAB
					+ "currency=" + currency + TAB
					+ "payment_type=" + payment_type + TAB
					+ "merchant_fields=" + merchant_fields + TAB
					+ "status=" + status);
		} catch (Exception e){
			log.error("Error retrieving data from Moneybookers status URL " + e);
			return false;
		}
		return true;
	}

	public String returnStatusString(){
		if (status.equals(MoneybookersClearingProvider.STATUS_PROCESSED_CODE)){
			return MoneybookersClearingProvider.STATUS_PROCESSED_STRING;
		} else if (status.equals(MoneybookersClearingProvider.STATUS_CANCELLED_CODE)){
			return MoneybookersClearingProvider.STATUS_CANCELLED_STRING;
		} else if (status.equals(MoneybookersClearingProvider.STATUS_FAILED_CODE)){
			return MoneybookersClearingProvider.STATUS_FAILED_STRING;
		} else if (status.equals(MoneybookersClearingProvider.STATUS_PENDING_CODE)){
			return MoneybookersClearingProvider.STATUS_PENDING_STRING;
		} else if (status.equals(MoneybookersClearingProvider.STATUS_CHARGEBACK_CODE)){
			return MoneybookersClearingProvider.STATUS_CHARGEBACK_STRING;
		}
		return null;
	}

}