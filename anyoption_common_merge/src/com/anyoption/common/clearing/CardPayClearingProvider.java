package com.anyoption.common.clearing;

import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.Base64;
import java.util.HashMap;
import java.util.Map;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.TransformerFactoryConfigurationError;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.apache.commons.configuration.ConfigurationException;
import org.apache.log4j.Logger;
import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import com.anyoption.common.encryption.SecureAlgorithms.Sha512;

/**
 * @author Kiril.m
 */
public class CardPayClearingProvider extends ClearingProvider {

	private static final Logger log = Logger.getLogger(CardPayClearingProvider.class);

	public static final String XML_DESCRIPTION_ATTRIBUTE = "description";
	public static final String XML_CARD_PAY_ID_ATTRIBUTE = "id";
	public static final String XML_DECLINE_CODE_ATTRIBUTE = "decline_code";
	public static final String XML_STATUS_ATTRIBUTE = "status";

	private static final String XML_ORDER_ELEMENT = "order";
	private static final String XML_WALLET_ID_ATTRIBUTE = "wallet_id";
	private static final String XML_NUMBER_ATTRIBUTE = "number";
	private static final String XML_CURRENCY_ATTRIBUTE = "currency";
	private static final String XML_AMOUNT_ATTRIBUTE = "amount";
	private static final String XML_EMAIL_ATTRIBUTE = "email";
	private static final String XML_THREE_D_ATTRIBUTE = "is_3d";
	private static final String XML_DECLINE_REASON_ATTRIBUTE = "decline_reason";
	private static final String XML_SUCCESS_URL_ATTRIBUTE = "success_url";
	private static final String XML_SUCCESS_URL_PROPERTY = "successUrl";
	private static final String XML_DECLINE_URL_ATTRIBUTE = "decline_url";
	private static final String XML_DECLINE_URL_PROPERTY = "declineUrl";
	private static final String XML_CANCEL_URL_ATTRIBUTE = "cancel_url";
	private static final String XML_CANCEL_URL_PROPERTY = "cancelUrl";
	private static final String XML_NOTE_ATTRIBUTE = "note";
	private static final String XML_REDIRECT_URL_PROPERTY = "redirectUrl";
	private static final String REGEX_DIGITS = "\\d+";

	public static final String STATUS_APPROVED = "APPROVED";
	public static final String STATUS_DECLINED = "DECLINED";
	public static final String STATUS_PENDING = "PENDING";

	private static final String CONFIG_DELIMITER = ",";
	private static final String PARAM_DELIMITER = ":=";
	private static final Map<String, String> providerConfig = new HashMap<>();
	static {
		providerConfig.put(XML_SUCCESS_URL_PROPERTY, null);
		providerConfig.put(XML_DECLINE_URL_PROPERTY, null);
		providerConfig.put(XML_CANCEL_URL_PROPERTY, null);
		providerConfig.put(XML_REDIRECT_URL_PROPERTY, null);
	}

	@Override
	public void setConfig(ClearingProviderConfig config) throws ClearingException {
		super.setConfig(config);
		if (config.getProps() != null && !config.getProps().trim().isEmpty()) {
			String[] props = config.getProps().split(CONFIG_DELIMITER);
			for (String param : props) {
				String[] paramSplit = param.split(PARAM_DELIMITER);
				if (paramSplit.length == 2 && providerConfig.containsKey(paramSplit[0])) {
					providerConfig.put(paramSplit[0], paramSplit[1]);
				}
			}
		}
	}

	public void constructParameters(CardPayInfo info) throws Throwable {
		try {
			for (String key : providerConfig.keySet()) {
				if (providerConfig.get(key) == null) {
					log.error("Incorrect provider configuration");
					throw new ConfigurationException("Incorrect provider configuration. Value of " + key + " is null");
				}
			}
			constructOrderXML(info);
			constructDigest(info);
		} catch (Throwable e) {
			throw e;
		}
	}

	private void constructOrderXML(CardPayInfo info) throws Throwable {
		try {
			Document document = DocumentBuilderFactory.newInstance().newDocumentBuilder().newDocument();
			Element order = document.createElement(XML_ORDER_ELEMENT);
			document.appendChild(order);
			addAttribute(document, order, XML_WALLET_ID_ATTRIBUTE, username);
			addAttribute(document, order, XML_NUMBER_ATTRIBUTE, String.valueOf(info.getNumber()));
			addAttribute(document, order, XML_DESCRIPTION_ATTRIBUTE, info.getDescription());
			addAttribute(document, order, XML_CURRENCY_ATTRIBUTE, info.getCurrency());
			addAttribute(document, order, XML_AMOUNT_ATTRIBUTE, info.getAmount().toString());
			addAttribute(document, order, XML_EMAIL_ATTRIBUTE, info.getEmail());
			addAttribute(document, order, XML_SUCCESS_URL_ATTRIBUTE, providerConfig.get(XML_SUCCESS_URL_PROPERTY));
			addAttribute(document, order, XML_DECLINE_URL_ATTRIBUTE, providerConfig.get(XML_DECLINE_URL_PROPERTY));
			addAttribute(document, order, XML_CANCEL_URL_ATTRIBUTE, providerConfig.get(XML_CANCEL_URL_PROPERTY));
			addAttribute(document, order, XML_NOTE_ATTRIBUTE, providerConfig.get(XML_REDIRECT_URL_PROPERTY));
			Transformer transformer = TransformerFactory.newInstance().newTransformer();
			DOMSource source = new DOMSource(order);
			StreamResult result = new StreamResult(new StringWriter());
			transformer.transform(source, result);
			String orderXML = result.getWriter().toString();
			info.setOrderXML(orderXML);
			info.setOrderXMLEncoded(Base64.getEncoder().encodeToString(orderXML.getBytes()));
		} catch (ParserConfigurationException e) {
			log.debug("Unable to construct clearing parameters", e);
			throw e;
		} catch (TransformerConfigurationException | TransformerFactoryConfigurationError e) {
			log.debug("Unable to transform clearing parameters", e);
			throw e;
		}
	}

	private void addAttribute(Document document, Element element, String attributeName, String attributeValue) {
		Attr attr = document.createAttribute(attributeName);
		attr.setValue(attributeValue);
		element.setAttributeNode(attr);
	}

	private void constructDigest(CardPayInfo info) throws Exception {
		String orderXML = info.getOrderXML() + password;
		try {
			info.setSha512(Sha512.encode(orderXML));
		} catch (Exception e) {
			log.debug("Unable to encode clearing parameter", e);
			throw e;
		}
	}

	private boolean validateResult(CardPayInfo info, String sha512) throws Exception {
		constructDigest(info);
		return info.getSha512().equals(sha512);
	}

	public CardPayInfo constructResult(String orderXMLEncoded, String sha512) throws Exception {
		CardPayInfo info = new CardPayInfo(orderXMLEncoded);
		info.setOrderXML(new String(Base64.getDecoder().decode(info.getOrderXMLEncoded())));
		log.info("orderXML: " + info.getOrderXML());
		if (!validateResult(info, sha512)) {
			log.debug("Given xml is not valid");
			return null;
		}
		try {
			DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
			Document document = builder.parse(new InputSource(new StringReader(info.getOrderXML())));
			NodeList elements = document.getElementsByTagName(XML_ORDER_ELEMENT);
			if (elements.getLength() > 0) {
				Node order = elements.item(0);
				NamedNodeMap attributes = order.getAttributes();
				Node node = attributes.getNamedItem(XML_CARD_PAY_ID_ATTRIBUTE);
				if (node.getNodeValue().matches(REGEX_DIGITS)) {
					info.setCardPayId(Long.valueOf(node.getNodeValue()));
				}
				info.setNumber(Long.valueOf(attributes.getNamedItem(XML_NUMBER_ATTRIBUTE).getNodeValue()));
				info.setStatus(attributes.getNamedItem(XML_STATUS_ATTRIBUTE).getNodeValue());
				info.setDescription(attributes.getNamedItem(XML_DESCRIPTION_ATTRIBUTE).getNodeValue());
				info.setCurrency(attributes.getNamedItem(XML_CURRENCY_ATTRIBUTE).getNodeValue());
				node = attributes.getNamedItem("decline_code");
				if (node != null && node.getNodeValue().matches(REGEX_DIGITS)) {
					info.setDeclineCode(Integer.valueOf(node.getNodeValue()));
					info.setDeclineReason(attributes.getNamedItem(XML_DECLINE_REASON_ATTRIBUTE).getNodeValue());
				}
				info.setThreeD(Boolean.valueOf(attributes.getNamedItem(XML_THREE_D_ATTRIBUTE).getNodeValue()));
			}
			return info;
		} catch (ParserConfigurationException | SAXException | IOException e) {
			log.debug("Unable to parse xml parameters", e);
			throw e;
		}
	}

	@Override
	public void authorize(ClearingInfo info) throws ClearingException {}

	@Override
	public void capture(ClearingInfo info) throws ClearingException {}

	@Override
	public void cancel(ClearingInfo info) throws ClearingException {}

	@Override
	public void enroll(ClearingInfo info) throws ClearingException {}

	@Override
	public void purchase(ClearingInfo info) throws ClearingException {}

	@Override
	public void withdraw(ClearingInfo info) throws ClearingException {}

	@Override
	public void bookback(ClearingInfo info) throws ClearingException {}
}