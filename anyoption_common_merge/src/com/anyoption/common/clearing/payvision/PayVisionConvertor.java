package com.anyoption.common.clearing.payvision;

import com.anyoption.common.beans.base.Currency;

public class PayVisionConvertor {
	
	// List of ISO 4217 Currencies 
	public static final int ISO_USD = 840;
	public static final int ISO_EUR = 978;
	public static final int ISO_GBP = 826;
	public static final int ISO_TRY = 949;
	public static final int ISO_RUB = 643;
	public static final int ISO_CNY = 392;
	public static final int ISO_KRW = 410;
	public static final int ISO_SEK = 752;
	public static final int ISO_AUD = 36;
	public static final int ISO_ZAR = 710;
	public static final int ISO_CZK = 203;
	public static final int ISO_PLN = 985;
	
	public static int aoCurrencyToIso(int currencyId) throws IllegalArgumentException {
		switch(currencyId) {
			case (int) Currency.CURRENCY_USD_ID :
				return ISO_USD;
			case (int) Currency.CURRENCY_EUR_ID :
				return ISO_EUR;
			case (int) Currency.CURRENCY_GBP_ID :
				return ISO_GBP;
			case (int) Currency.CURRENCY_TRY_ID :
				return ISO_TRY;			
			case (int) Currency.CURRENCY_RUB_ID :
				return ISO_RUB;			
			case (int) Currency.CURRENCY_CNY_ID :
				return ISO_CNY;			
			case (int) Currency.CURRENCY_KRW_ID :
				return ISO_KRW;			
			case (int) Currency.CURRENCY_SEK_ID :
				return ISO_SEK;			
			case (int) Currency.CURRENCY_AUD_ID :
				return ISO_AUD;			
			case (int) Currency.CURRENCY_ZAR_ID :
				return ISO_ZAR;			
			case (int) Currency.CURRENCY_CZK_ID :
				return ISO_CZK;
			case (int) Currency.CURRENCY_PLN_ID :
				return ISO_PLN;
			default :
				throw new IllegalArgumentException("Unsupported currency:"+currencyId);
		}
	}
	
	// List of ISO 3166 Countries
	public static int aoCountryToIso(String a2) throws IllegalArgumentException {
		switch(a2) {
			case 	"AF"	 : return 	4	;
			case 	"AX"	 : return 	248	;	
			case 	"AL"	 : return 	8	;	
			case 	"DZ"	 : return 	12	;	
			case 	"AS"	 : return 	16	;	
			case 	"AD"	 : return 	20	;	
			case 	"AO"	 : return 	24	;	
			case 	"AI"	 : return 	660	;	
			case 	"AQ"	 : return 	10	;	
			case 	"AG"	 : return 	28	;	
			case 	"AR"	 : return 	32	;	
			case 	"AM"	 : return 	51	;	
			case 	"AW"	 : return 	533	;	
			case 	"AU"	 : return 	36	;	
			case 	"AT"	 : return 	40	;	
			case 	"AZ"	 : return 	31	;	
			case 	"BS"	 : return 	44	;	
			case 	"BH"	 : return 	48	;	
			case 	"BD"	 : return 	50	;	
			case 	"BB"	 : return 	52	;	
			case 	"BY"	 : return 	112	;	
			case 	"BE"	 : return 	56	;	
			case 	"BZ"	 : return 	84	;	
			case 	"BJ"	 : return 	204	;	
			case 	"BM"	 : return 	60	;	
			case 	"BT"	 : return 	64	;	
			case 	"BO"	 : return 	68	;	
			case 	"BA"	 : return 	70	;	
			case 	"BW"	 : return 	72	;	
			case 	"BV"	 : return 	74	;	
			case 	"BR"	 : return 	76	;	
			case 	"VG"	 : return 	92	;	
			case 	"IO"	 : return 	86	;	
			case 	"BN"	 : return 	96	;	
			case 	"BG"	 : return 	100	;	
			case 	"BF"	 : return 	854	;	
			case 	"BI"	 : return 	108	;	
			case 	"KH"	 : return 	116	;	
			case 	"CM"	 : return 	120	;	
			case 	"CA"	 : return 	124	;	
			case 	"CV"	 : return 	132	;	
			case 	"KY"	 : return 	136	;	
			case 	"CF"	 : return 	140	;	
			case 	"TD"	 : return 	148	;	
			case 	"CL"	 : return 	152	;	
			case 	"CN"	 : return 	156	;	
			case 	"HK"	 : return 	344	;	
			case 	"MO"	 : return 	446	;	
			case 	"CX"	 : return 	162	;	
			case 	"CC"	 : return 	166	;	
			case 	"CO"	 : return 	170	;	
			case 	"KM"	 : return 	174	;	
			case 	"CG"	 : return 	178	;	
			case 	"CD"	 : return 	180	;	
			case 	"CK"	 : return 	184	;	
			case 	"CR"	 : return 	188	;	
			case 	"CI"	 : return 	384	;	
			case 	"HR"	 : return 	191	;	
			case 	"CU"	 : return 	192	;	
			case 	"CY"	 : return 	196	;	
			case 	"CZ"	 : return 	203	;	
			case 	"DK"	 : return 	208	;	
			case 	"DJ"	 : return 	262	;	
			case 	"DM"	 : return 	212	;	
			case 	"DO"	 : return 	214	;	
			case 	"EC"	 : return 	218	;	
			case 	"EG"	 : return 	818	;	
			case 	"SV"	 : return 	222	;	
			case 	"GQ"	 : return 	226	;	
			case 	"ER"	 : return 	232	;	
			case 	"EE"	 : return 	233	;	
			case 	"ET"	 : return 	231	;	
			case 	"FK"	 : return 	238	;	
			case 	"FO"	 : return 	234	;	
			case 	"FJ"	 : return 	242	;	
			case 	"FI"	 : return 	246	;	
			case 	"FR"	 : return 	250	;	
			case 	"GF"	 : return 	254	;	
			case 	"PF"	 : return 	258	;	
			case 	"TF"	 : return 	260	;	
			case 	"GA"	 : return 	266	;	
			case 	"GM"	 : return 	270	;	
			case 	"GE"	 : return 	268	;	
			case 	"DE"	 : return 	276	;	
			case 	"GH"	 : return 	288	;	
			case 	"GI"	 : return 	292	;	
			case 	"GR"	 : return 	300	;	
			case 	"GL"	 : return 	304	;	
			case 	"GD"	 : return 	308	;	
			case 	"GP"	 : return 	312	;	
			case 	"GU"	 : return 	316	;	
			case 	"GT"	 : return 	320	;	
			case 	"GG"	 : return 	831	;	
			case 	"GN"	 : return 	324	;	
			case 	"GW"	 : return 	624	;	
			case 	"GY"	 : return 	328	;	
			case 	"HT"	 : return 	332	;	
			case 	"HM"	 : return 	334	;	
			case 	"VA"	 : return 	336	;	
			case 	"HN"	 : return 	340	;	
			case 	"HU"	 : return 	348	;	
			case 	"IS"	 : return 	352	;	
			case 	"IN"	 : return 	356	;	
			case 	"ID"	 : return 	360	;	
			case 	"IR"	 : return 	364	;	
			case 	"IQ"	 : return 	368	;	
			case 	"IE"	 : return 	372	;	
			case 	"IM"	 : return 	833	;	
			case 	"IL"	 : return 	376	;	
			case 	"IT"	 : return 	380	;	
			case 	"JM"	 : return 	388	;	
			case 	"JP"	 : return 	392	;	
			case 	"JE"	 : return 	832	;	
			case 	"JO"	 : return 	400	;	
			case 	"KZ"	 : return 	398	;	
			case 	"KE"	 : return 	404	;	
			case 	"KI"	 : return 	296	;	
			case 	"KP"	 : return 	408	;	
			case 	"KR"	 : return 	410	;	
			case 	"KW"	 : return 	414	;	
			case 	"KG"	 : return 	417	;	
			case 	"LA"	 : return 	418	;	
			case 	"LV"	 : return 	428	;	
			case 	"LB"	 : return 	422	;	
			case 	"LS"	 : return 	426	;	
			case 	"LR"	 : return 	430	;	
			case 	"LY"	 : return 	434	;	
			case 	"LI"	 : return 	438	;	
			case 	"LT"	 : return 	440	;	
			case 	"LU"	 : return 	442	;	
			case 	"MK"	 : return 	807	;	
			case 	"MG"	 : return 	450	;	
			case 	"MW"	 : return 	454	;	
			case 	"MY"	 : return 	458	;	
			case 	"MV"	 : return 	462	;	
			case 	"ML"	 : return 	466	;	
			case 	"MT"	 : return 	470	;	
			case 	"MH"	 : return 	584	;	
			case 	"MQ"	 : return 	474	;	
			case 	"MR"	 : return 	478	;	
			case 	"MU"	 : return 	480	;	
			case 	"YT"	 : return 	175	;	
			case 	"MX"	 : return 	484	;	
			case 	"FM"	 : return 	583	;	
			case 	"MD"	 : return 	498	;	
			case 	"MC"	 : return 	492	;	
			case 	"MN"	 : return 	496	;	
			case 	"ME"	 : return 	499	;	
			case 	"MS"	 : return 	500	;	
			case 	"MA"	 : return 	504	;	
			case 	"MZ"	 : return 	508	;	
			case 	"MM"	 : return 	104	;	
			case 	"NA"	 : return 	516	;	
			case 	"NR"	 : return 	520	;	
			case 	"NP"	 : return 	524	;	
			case 	"NL"	 : return 	528	;	
			case 	"AN"	 : return 	530	;	
			case 	"NC"	 : return 	540	;	
			case 	"NZ"	 : return 	554	;	
			case 	"NI"	 : return 	558	;	
			case 	"NE"	 : return 	562	;	
			case 	"NG"	 : return 	566	;	
			case 	"NU"	 : return 	570	;	
			case 	"NF"	 : return 	574	;	
			case 	"MP"	 : return 	580	;	
			case 	"NO"	 : return 	578	;	
			case 	"OM"	 : return 	512	;	
			case 	"PK"	 : return 	586	;	
			case 	"PW"	 : return 	585	;	
			case 	"PS"	 : return 	275	;	
			case 	"PA"	 : return 	591	;	
			case 	"PG"	 : return 	598	;	
			case 	"PY"	 : return 	600	;	
			case 	"PE"	 : return 	604	;	
			case 	"PH"	 : return 	608	;	
			case 	"PN"	 : return 	612	;	
			case 	"PL"	 : return 	616	;	
			case 	"PT"	 : return 	620	;	
			case 	"PR"	 : return 	630	;	
			case 	"QA"	 : return 	634	;	
			case 	"RE"	 : return 	638	;	
			case 	"RO"	 : return 	642	;	
			case 	"RU"	 : return 	643	;	
			case 	"RW"	 : return 	646	;	
			case 	"BL"	 : return 	652	;	
			case 	"SH"	 : return 	654	;	
			case 	"KN"	 : return 	659	;	
			case 	"LC"	 : return 	662	;	
			case 	"MF"	 : return 	663	;	
			case 	"PM"	 : return 	666	;	
			case 	"VC"	 : return 	670	;	
			case 	"WS"	 : return 	882	;	
			case 	"SM"	 : return 	674	;	
			case 	"ST"	 : return 	678	;	
			case 	"SA"	 : return 	682	;	
			case 	"SN"	 : return 	686	;	
			case 	"RS"	 : return 	688	;	
			case 	"SC"	 : return 	690	;	
			case 	"SL"	 : return 	694	;	
			case 	"SG"	 : return 	702	;	
			case 	"SK"	 : return 	703	;	
			case 	"SI"	 : return 	705	;	
			case 	"SB"	 : return 	90	;	
			case 	"SO"	 : return 	706	;	
			case 	"ZA"	 : return 	710	;	
			case 	"GS"	 : return 	239	;	
			case 	"SS"	 : return 	728	;	
			case 	"ES"	 : return 	724	;	
			case 	"LK"	 : return 	144	;	
			case 	"SD"	 : return 	736	;	
			case 	"SR"	 : return 	740	;	
			case 	"SJ"	 : return 	744	;	
			case 	"SZ"	 : return 	748	;	
			case 	"SE"	 : return 	752	;	
			case 	"CH"	 : return 	756	;	
			case 	"SY"	 : return 	760	;	
			case 	"TW"	 : return 	158	;	
			case 	"TJ"	 : return 	762	;	
			case 	"TZ"	 : return 	834	;	
			case 	"TH"	 : return 	764	;	
			case 	"TL"	 : return 	626	;	
			case 	"TG"	 : return 	768	;	
			case 	"TK"	 : return 	772	;	
			case 	"TO"	 : return 	776	;	
			case 	"TT"	 : return 	780	;	
			case 	"TN"	 : return 	788	;	
			case 	"TR"	 : return 	792	;	
			case 	"TM"	 : return 	795	;	
			case 	"TC"	 : return 	796	;	
			case 	"TV"	 : return 	798	;	
			case 	"UG"	 : return 	800	;	
			case 	"UA"	 : return 	804	;	
			case 	"AE"	 : return 	784	;	
			case 	"GB"	 : return 	826	;	
			case 	"US"	 : return 	840	;	
			case 	"UM"	 : return 	581	;	
			case 	"UY"	 : return 	858	;	
			case 	"UZ"	 : return 	860	;	
			case 	"VU"	 : return 	548	;	
			case 	"VE"	 : return 	862	;	
			case 	"VN"	 : return 	704	;	
			case 	"VI"	 : return 	850	;	
			case 	"WF"	 : return 	876	;	
			case 	"EH"	 : return 	732	;	
			case 	"YE"	 : return 	887	;	
			case 	"ZM"	 : return 	894	;	
			case 	"ZW"	 : return 	716	;	
			default :
				throw new IllegalArgumentException("Unsupported country:"+a2);
		}
	}
}
