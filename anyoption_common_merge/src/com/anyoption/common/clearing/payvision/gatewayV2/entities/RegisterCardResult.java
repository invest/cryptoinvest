/**
 * RegisterCardResult.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.anyoption.common.clearing.payvision.gatewayV2.entities;

public class RegisterCardResult  extends BaseResult  implements java.io.Serializable {
	private java.lang.String cardGuid;

    private java.lang.Long cardId;

    public RegisterCardResult() {
    }

    public RegisterCardResult(
           CdcEntry[] cdc,
           java.lang.String message,
           java.lang.Integer result,
           java.lang.String cardGuid,
           java.lang.Long cardId) {
        super(
            cdc,
            message,
            result);
        this.cardGuid = cardGuid;
        this.cardId = cardId;
    }


    /**
     * Gets the cardGuid value for this RegisterCardResult.
     * 
     * @return cardGuid
     */
    public java.lang.String getCardGuid() {
        return cardGuid;
    }


    /**
     * Sets the cardGuid value for this RegisterCardResult.
     * 
     * @param cardGuid
     */
    public void setCardGuid(java.lang.String cardGuid) {
        this.cardGuid = cardGuid;
    }


    /**
     * Gets the cardId value for this RegisterCardResult.
     * 
     * @return cardId
     */
    public java.lang.Long getCardId() {
        return cardId;
    }


    /**
     * Sets the cardId value for this RegisterCardResult.
     * 
     * @param cardId
     */
    public void setCardId(java.lang.Long cardId) {
        this.cardId = cardId;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (obj == null) return false;
        if (!(obj instanceof RegisterCardResult)) return false;
        RegisterCardResult other = (RegisterCardResult) obj;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.cardGuid==null && other.getCardGuid()==null) || 
             (this.cardGuid!=null &&
              this.cardGuid.equals(other.getCardGuid()))) &&
            ((this.cardId==null && other.getCardId()==null) || 
             (this.cardId!=null &&
              this.cardId.equals(other.getCardId())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getCardGuid() != null) {
            _hashCode += getCardGuid().hashCode();
        }
        if (getCardId() != null) {
            _hashCode += getCardId().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(RegisterCardResult.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Payvision.Billing.CardEngine.GatewayV2.Entities", "RegisterCardResult"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cardGuid");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Payvision.Billing.CardEngine.GatewayV2.Entities", "CardGuid"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cardId");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Payvision.Billing.CardEngine.GatewayV2.Entities", "CardId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "long"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
