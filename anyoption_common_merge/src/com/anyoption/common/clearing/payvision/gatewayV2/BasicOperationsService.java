/**
 * BasicOperationsService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.anyoption.common.clearing.payvision.gatewayV2;

public interface BasicOperationsService extends javax.xml.rpc.Service {
    public java.lang.String getBasicHttpBinding_BasicOperationsAddress();

    public BasicOperations getBasicHttpBinding_BasicOperations() throws javax.xml.rpc.ServiceException;

    public BasicOperations getBasicHttpBinding_BasicOperations(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
