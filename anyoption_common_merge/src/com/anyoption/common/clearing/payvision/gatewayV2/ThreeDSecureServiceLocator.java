/**
 * ThreeDSecureServiceLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.anyoption.common.clearing.payvision.gatewayV2;

import java.math.BigDecimal;
import java.rmi.RemoteException;
import java.util.UUID;

import javax.xml.rpc.ServiceException;

import org.apache.axis.types.UnsignedByte;

import com.anyoption.common.clearing.payvision.gatewayV2.entities.EnrollmentResult;

public class ThreeDSecureServiceLocator extends org.apache.axis.client.Service implements ThreeDSecureService {

    public ThreeDSecureServiceLocator() {
    }


    public ThreeDSecureServiceLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public ThreeDSecureServiceLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for BasicHttpBinding_ThreeDSecure
    private java.lang.String BasicHttpBinding_ThreeDSecure_address = "https://testprocessor.payvisionservices.com/GatewayV2/ThreeDSecureService.svc";

    public java.lang.String getBasicHttpBinding_ThreeDSecureAddress() {
        return BasicHttpBinding_ThreeDSecure_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String BasicHttpBinding_ThreeDSecureWSDDServiceName = "BasicHttpBinding_ThreeDSecure";

    public java.lang.String getBasicHttpBinding_ThreeDSecureWSDDServiceName() {
        return BasicHttpBinding_ThreeDSecureWSDDServiceName;
    }

    public void setBasicHttpBinding_ThreeDSecureWSDDServiceName(java.lang.String name) {
        BasicHttpBinding_ThreeDSecureWSDDServiceName = name;
    }

    public ThreeDSecure getBasicHttpBinding_ThreeDSecure() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(BasicHttpBinding_ThreeDSecure_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getBasicHttpBinding_ThreeDSecure(endpoint);
    }

    public ThreeDSecure getBasicHttpBinding_ThreeDSecure(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            BasicHttpBinding_ThreeDSecureStub _stub = new BasicHttpBinding_ThreeDSecureStub(portAddress, this);
            _stub.setPortName(getBasicHttpBinding_ThreeDSecureWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setBasicHttpBinding_ThreeDSecureEndpointAddress(java.lang.String address) {
        BasicHttpBinding_ThreeDSecure_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (ThreeDSecure.class.isAssignableFrom(serviceEndpointInterface)) {
                BasicHttpBinding_ThreeDSecureStub _stub = new BasicHttpBinding_ThreeDSecureStub(new java.net.URL(BasicHttpBinding_ThreeDSecure_address), this);
                _stub.setPortName(getBasicHttpBinding_ThreeDSecureWSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("BasicHttpBinding_ThreeDSecure".equals(inputPortName)) {
            return getBasicHttpBinding_ThreeDSecure();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://payvision.com/gatewayV2", "ThreeDSecureService");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://payvision.com/gatewayV2", "BasicHttpBinding_ThreeDSecure"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("BasicHttpBinding_ThreeDSecure".equals(portName)) {
            setBasicHttpBinding_ThreeDSecureEndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }
    
    public static void main(String args[]) {
    	try {
    		ThreeDSecureServiceLocator locator = new ThreeDSecureServiceLocator();
			ThreeDSecure secure = locator.getBasicHttpBinding_ThreeDSecure();
			EnrollmentResult result = secure.checkEnrollment(
					1004079l,								// member Id, 
					"7F7BEDD0-AE85-47CF-9535-2388F115EC37",	// member Guid, 
					new BigDecimal(1.0),					// amount,  
					978,									// currency Id ( euro ) 
					UUID.randomUUID().toString(),			// trackingMemberCode, 
					"4907639999990022",  					// card number
					"Test Test", 							// card holder
					new UnsignedByte(12),  					// expire month
					new Short((short) 2017),  				// expire year
					null 									// not used : additionalInfo
					);
			
			System.out.println("result:" + result.getResult() +" msg:" + result.getMessage()+" id:"+result.getTrackingMemberCode());
			System.out.println(" url:" + result.getIssuerUrl());
		} catch (ServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NumberFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }

}
