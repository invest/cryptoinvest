/**
 * EnrollmentResult.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.anyoption.common.clearing.payvision.gatewayV2.entities;

public class EnrollmentResult  extends BaseResult  implements java.io.Serializable {
    private java.util.Calendar dateTime;

    private java.lang.Long enrollmentId;

    private java.lang.String issuerUrl;

    private java.lang.String paymentAuthenticationRequest;

    private java.lang.String trackingMemberCode;

    public EnrollmentResult() {
    }

    public EnrollmentResult(
           CdcEntry[] cdc,
           java.lang.String message,
           java.lang.Integer result,
           java.util.Calendar dateTime,
           java.lang.Long enrollmentId,
           java.lang.String issuerUrl,
           java.lang.String paymentAuthenticationRequest,
           java.lang.String trackingMemberCode) {
        super(
            cdc,
            message,
            result);
        this.dateTime = dateTime;
        this.enrollmentId = enrollmentId;
        this.issuerUrl = issuerUrl;
        this.paymentAuthenticationRequest = paymentAuthenticationRequest;
        this.trackingMemberCode = trackingMemberCode;
    }


    /**
     * Gets the dateTime value for this EnrollmentResult.
     * 
     * @return dateTime
     */
    public java.util.Calendar getDateTime() {
        return dateTime;
    }


    /**
     * Sets the dateTime value for this EnrollmentResult.
     * 
     * @param dateTime
     */
    public void setDateTime(java.util.Calendar dateTime) {
        this.dateTime = dateTime;
    }


    /**
     * Gets the enrollmentId value for this EnrollmentResult.
     * 
     * @return enrollmentId
     */
    public java.lang.Long getEnrollmentId() {
        return enrollmentId;
    }


    /**
     * Sets the enrollmentId value for this EnrollmentResult.
     * 
     * @param enrollmentId
     */
    public void setEnrollmentId(java.lang.Long enrollmentId) {
        this.enrollmentId = enrollmentId;
    }


    /**
     * Gets the issuerUrl value for this EnrollmentResult.
     * 
     * @return issuerUrl
     */
    public java.lang.String getIssuerUrl() {
        return issuerUrl;
    }


    /**
     * Sets the issuerUrl value for this EnrollmentResult.
     * 
     * @param issuerUrl
     */
    public void setIssuerUrl(java.lang.String issuerUrl) {
        this.issuerUrl = issuerUrl;
    }


    /**
     * Gets the paymentAuthenticationRequest value for this EnrollmentResult.
     * 
     * @return paymentAuthenticationRequest
     */
    public java.lang.String getPaymentAuthenticationRequest() {
        return paymentAuthenticationRequest;
    }


    /**
     * Sets the paymentAuthenticationRequest value for this EnrollmentResult.
     * 
     * @param paymentAuthenticationRequest
     */
    public void setPaymentAuthenticationRequest(java.lang.String paymentAuthenticationRequest) {
        this.paymentAuthenticationRequest = paymentAuthenticationRequest;
    }


    /**
     * Gets the trackingMemberCode value for this EnrollmentResult.
     * 
     * @return trackingMemberCode
     */
    public java.lang.String getTrackingMemberCode() {
        return trackingMemberCode;
    }


    /**
     * Sets the trackingMemberCode value for this EnrollmentResult.
     * 
     * @param trackingMemberCode
     */
    public void setTrackingMemberCode(java.lang.String trackingMemberCode) {
        this.trackingMemberCode = trackingMemberCode;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof EnrollmentResult)) return false;
        EnrollmentResult other = (EnrollmentResult) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.dateTime==null && other.getDateTime()==null) || 
             (this.dateTime!=null &&
              this.dateTime.equals(other.getDateTime()))) &&
            ((this.enrollmentId==null && other.getEnrollmentId()==null) || 
             (this.enrollmentId!=null &&
              this.enrollmentId.equals(other.getEnrollmentId()))) &&
            ((this.issuerUrl==null && other.getIssuerUrl()==null) || 
             (this.issuerUrl!=null &&
              this.issuerUrl.equals(other.getIssuerUrl()))) &&
            ((this.paymentAuthenticationRequest==null && other.getPaymentAuthenticationRequest()==null) || 
             (this.paymentAuthenticationRequest!=null &&
              this.paymentAuthenticationRequest.equals(other.getPaymentAuthenticationRequest()))) &&
            ((this.trackingMemberCode==null && other.getTrackingMemberCode()==null) || 
             (this.trackingMemberCode!=null &&
              this.trackingMemberCode.equals(other.getTrackingMemberCode())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getDateTime() != null) {
            _hashCode += getDateTime().hashCode();
        }
        if (getEnrollmentId() != null) {
            _hashCode += getEnrollmentId().hashCode();
        }
        if (getIssuerUrl() != null) {
            _hashCode += getIssuerUrl().hashCode();
        }
        if (getPaymentAuthenticationRequest() != null) {
            _hashCode += getPaymentAuthenticationRequest().hashCode();
        }
        if (getTrackingMemberCode() != null) {
            _hashCode += getTrackingMemberCode().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(EnrollmentResult.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Payvision.Billing.CardEngine.GatewayV2.Entities", "EnrollmentResult"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("dateTime");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Payvision.Billing.CardEngine.GatewayV2.Entities", "DateTime"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("enrollmentId");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Payvision.Billing.CardEngine.GatewayV2.Entities", "EnrollmentId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "long"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("issuerUrl");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Payvision.Billing.CardEngine.GatewayV2.Entities", "IssuerUrl"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("paymentAuthenticationRequest");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Payvision.Billing.CardEngine.GatewayV2.Entities", "PaymentAuthenticationRequest"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("trackingMemberCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Payvision.Billing.CardEngine.GatewayV2.Entities", "TrackingMemberCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
