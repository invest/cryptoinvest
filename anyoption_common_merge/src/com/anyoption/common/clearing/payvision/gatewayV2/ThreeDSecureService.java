/**
 * ThreeDSecureService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.anyoption.common.clearing.payvision.gatewayV2;

public interface ThreeDSecureService extends javax.xml.rpc.Service {
    public java.lang.String getBasicHttpBinding_ThreeDSecureAddress();

    public ThreeDSecure getBasicHttpBinding_ThreeDSecure() throws javax.xml.rpc.ServiceException;

    public ThreeDSecure getBasicHttpBinding_ThreeDSecure(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
