package com.anyoption.common.clearing;

import java.io.IOException;
import java.math.BigDecimal;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import javax.xml.parsers.ParserConfigurationException;
import org.apache.log4j.Logger;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;
import com.anyoption.common.beans.PaymentMethodService;
import com.anyoption.common.beans.TransactionRequestResponse;
import com.anyoption.common.encryption.SecureAlgorithms;
import com.anyoption.common.payments.epg.CheckOutParams;
import com.anyoption.common.payments.epg.EpgRequestParams;
import com.anyoption.common.payments.epg.Response;
import com.anyoption.common.payments.epg.ResponseCheckout;
import com.anyoption.common.util.ClearingUtil;
import com.anyoption.common.util.CommonUtil;

/**
 * Implement the communication with EPG
 *
 * @author EyalG
 */
public class EPGClearingProvider extends ClearingProvider {
	private static final Logger log = Logger.getLogger(EPGClearingProvider.class);
	public static final String DEBIT = "DEBIT";
	public static final String CREDIT = "CREDIT";
	public static final String CREDIT_CARD = "creditcards";
	
	private String successURL;
	private String errorURL;
	private String statusURL;
	private String cancelURL;
	private String threeDTermsURL;
	private String paymentSolutionsURL;
	private String cssURL;

	public void authorize(ClearingInfo info) throws ClearingException {
		try {		
			EpgRequestParams<Object> requestParam = setParams(info, DEBIT);			
			String request = CommonUtil.generateObjectToQueryString(requestParam, "=", "&", false);
			log.info(request);
			String secureRequest = secureRequest(request);
			String response = ClearingUtil.executePOSTRequest(url, secureRequest);
			info.setAcsUrl(response);
			TransactionRequestResponse requestResponse = null;
			//credit card
	    	if (info.getPaymentSolution().equalsIgnoreCase(CREDIT_CARD)) {
		    	info.setNotRerouting(true);
				info.setTermsURL(threeDTermsURL); //for 3d
	    		
				//FIXME
				//Response responseObj = (Response) CommonUtil.generateXmlToObject(Response.class, response);				
				//requestResponse = parseResult(responseObj, info);		
				requestResponse = parseResult(response, info);
				
				request = request.replaceFirst("&cardNumber=\\d+&", "&cardNumber=xxxx&");
				request = request.replaceFirst("&cvnNumber=\\d+&", "&cvnNumber=xxx&");
	    	}
	    	requestResponse = 
	    			new TransactionRequestResponse(info.getTransactionId(), 
	    					request, response, info.getPaymentSolution());		
			info.setRequestResponse(requestResponse);
		} catch (Exception e) {
			log.error("ERROR! authorize", e);
		}
    }
    
    /*private TransactionRequestResponse parseResult(Response responseObj, ClearingInfo info) {
    	if (null == info) {
    		info = new ClearingInfo();
    	}
    	TransactionRequestResponse tr = new TransactionRequestResponse();
    	PaymentMethodService pms = new PaymentMethodService();
    	//assumption 1 operation
    	
    	if ( responseObj.getOperations() != null) {
    		Operation operation = responseObj.getOperations().get(0);
    		if (operation != null) {
    			pms.setOperationStatus(operation.getStatus());
    			boolean isSuccess = false;
    			String result = "-1";
    			String userMessage = "Transaction failed.";
    			
    		}
    	}
		return null;
	}*/

	private String doRequest(ClearingInfo info, String operationType) throws IOException {
    	EpgRequestParams<Object> requestParam = setParams(info, operationType);			
		String request = CommonUtil.generateObjectToQueryString(requestParam, "=", "&", true);
		String secureRequest = secureRequest(request);
		return ClearingUtil.executePOSTRequest(url, secureRequest);
	}

	public void withdraw(ClearingInfo info) throws ClearingException {
	    try {	    	
	    	/*EpgRequestParams<Object> requestParam = setParams(info, CREDIT);			
			String request = preparedRequest(requestParam);
			String secureRequest = secureRequest(request);
			
			String xml = ClearingUtil.executePOSTRequest(url, secureRequest);*/
			
	    	String xml = doRequest(info, CREDIT);
			Response response  = (Response) CommonUtil.generateXmlToObject(Response.class, xml);
			//handleWithdrawResponse(response);
			
			//ransactionRequestResponse requestResponse = new TransactionRequestResponse();
			//requestResponse.setTransactionId(info.getTransactionId());
			//request = request.replaceFirst("&cardNumber=\\d+&", "&cardNumber=xxxx&");
			//request = request.replaceFirst("&cvnNumber=\\d+&", "&cvnNumber=xxx&");
	        //requestResponse.setRequest(request);
			//requestResponse.setResponse(xml);
			//info.setRequestResponse(requestResponse);			
	    } catch (Exception e) {
			log.error("ERROR! authorize", e);
		}
    }

	public void capture(ClearingInfo info) throws ClearingException {
    	throw new ClearingException("Unsupported operation.");
    }

    public void enroll(ClearingInfo info) throws ClearingException {
    	throw new ClearingException("Unsupported operation.");
    }

    public void purchase(ClearingInfo info) throws ClearingException {
    	throw new ClearingException("Unsupported operation.");
    }

	public void bookback(ClearingInfo info) throws ClearingException {
    	throw new ClearingException("Unsupported operation.");
    }

    /**
     * @param str
     * @return
     */
    private String secureRequest(String str) {
    	String sha256ParamsIntegrityCheck = null;
    	String request = "";
    	try {
			sha256ParamsIntegrityCheck = SecureAlgorithms.Sha256.encode(str);	
			String encryptedParams = SecureAlgorithms.encryptAES(str, privateKey);
			request = "merchantId=" + username + "&encrypted=" + 
							URLEncoder.encode(encryptedParams, "UTF-8") + "&integrityCheck=" + sha256ParamsIntegrityCheck;
			log.debug("secureRequest: " + request);
    	} catch (Exception e) {
			log.error("ERROR! secureRequest ", e);
		}
    	return request;
    }

    /**
     * @param info
     * @param operationType
     * @return
     */
    public EpgRequestParams<Object> setParams(ClearingInfo info, String operationType) {
    	EpgRequestParams<Object> req = new EpgRequestParams<Object>();
    	
    	//TODO if  - not credit card
    	req.setOperationType(operationType); 
    	req.setMerchantId(username);//
    	req.setCustomerId(info.getUserId());
    	req.setMerchantTransactionId(info.getTransactionId());
    	req.setAmount(info.getAmount() / 100);
    	req.setCountry(info.getCountryA2());
    	req.setCurrency(info.getCurrencySymbol()); 
    	req.setIpAddress(info.getIp());
    	req.setAddressLine1(info.getAddress());
    	req.setCity(info.getCity());
    	req.setPostCode(info.getZip());
    	//req.setTelephone("");
    	req.setFirstname(info.getFirstName());
    	req.setLastname(info.getLastName()); 	
    	req.setPaymentSolution(info.getPaymentSolution());    	
    	req.setStatusURL(statusURL);    	 	
    	req.setSkinId(info.getSkinId());
    	req.setTransactionIP(info.getIp());
    	req.setDob(new SimpleDateFormat("dd-MM-yyyy").format(info.getUserDOB()));
    	req.setProductId(info.getProductId()); 
    	
    	if (!CommonUtil.isParameterEmptyOrNull(info.getHomePageUrl())) {
    		req.setSuccessURL(info.getHomePageUrl() + successURL);
        	req.setErrorURL(info.getHomePageUrl() + errorURL);
    		req.setCancelURL(info.getHomePageUrl() + cancelURL);
        	req.setCssURL(info.getHomePageUrl() + cssURL);
    	}
    	
    	//credit card
    	if (info.getPaymentSolution().equalsIgnoreCase(CREDIT_CARD)) {
    		req.setChName(info.getFirstName() + " " + info.getLastName());
        	req.setChAddress1(info.getAddress());
        	req.setChCity(info.getCity());
        	req.setChPostCode(info.getZip());
        	req.setChCountry(info.getCountryA2());
    		req.setBin(info.getCcn().substring(0, 6));
    		req.setCardNumber(info.getCcn());
    		if (!CommonUtil.isParameterEmptyOrNull(info.getCvv())) {
        		req.setCvnNumber(info.getCvv());
        	}
    		req.setExpDate(info.getExpireMonth() + info.getExpireYear());
    		req.setCardHolderName(info.getOwner());
    		
    		if (null != info.getAuthNumber()) {
        		req.setBankAuth(info.getAuthNumber());
        	}
    	}
    	
    	if (!CommonUtil.isParameterEmptyOrNull(info.getAdditionalRequestParams())) {
    		req.setMerchantParams(info.getAdditionalRequestParams());
    	}
    	
    	return req;
    }

	public void setProviderDetails(EPGInfo info) {
    	info.setTokenizeURL(url);
    	info.setExpressCheckoutURL(paymentSolutionsURL);
    	info.setMerchantId(username);
    }

	@Override
	public void cancel(ClearingInfo info) throws ClearingException {
		// TODO Auto-generated method stub
	}
	
	/**
     * Set clearing provider configuration.
     *
     * @param key
     * @param props
     * @throws ClearingException
     */
	//FIXME change this code.
    public void setConfig(ClearingProviderConfig config) throws ClearingException {
        super.setConfig(config);
        try {
	        if (null != config.getProps() && !config.getProps().trim().equals("")) {
	            String[] ps = config.getProps().split(";");
	            for (int i = 0; i < ps.length; i++) {
	                String[] p = ps[i].split("=");
	                if (p[0].equals("successURL")) {
	                	successURL = p[1];
	                } else if (p[0].equals("errorURL")) {
	                	errorURL = p[1];
	                } else if (p[0].equals("statusURL")) {
	                	statusURL = CommonUtil.getProperty("epg.status.url", null) + p[1];
	                } else if (p[0].equals("cancelURL")) {
	                	cancelURL = p[1];
	                } else if (p[0].equals("3dTermsUrl")) {
	                	threeDTermsURL = p[1] + "=" + p[2] + "=";
	                } else if (p[0].equals("payment_solutions_available")) {
	                	paymentSolutionsURL = p[1];
	                } else if (p[0].equals("CSS_URL")) {
	                	cssURL = p[1];
	                }
	            }
	        }
        } catch (Exception e) {
        	log.error("ERROR! set config - job?", e);
        }
    }

	public static String getAdditionalDetails(Object details) {
    	String detailsString = null;			    	
    	try {
    		detailsString = CommonUtil.generateObjectToQueryString(details, ":", ";", true);
		} catch (Exception e) {
			log.error("ERROR! Cannot get EPG request additional details from class!", e);
		} 
		return detailsString;
	}
    
    /**
     * @param checkOutParams
     * @param info
     * @return
     */
    public static ArrayList<String> getEPGPaymentSolutions(CheckOutParams checkOutParams, EPGInfo info) {
    	ArrayList<String> paymentSolutions = null;
		String request = CommonUtil.generateObjectToQueryString(checkOutParams, "=","&", true);
		log.info("EPG Payment Solutions request: " + request);
        ResponseCheckout res = new ResponseCheckout();
		try {
			//Example, response: <?xml version="1.0" encoding="UTF-8" standalone="yes"?><checkout-response><paymentSolutions><paymentSolution>ChargeBackHunter</paymentSolution><paymentSolution>CreditCards</paymentSolution><paymentSolution>Neteller</paymentSolution></paymentSolutions></checkout-response>
			String response = ClearingUtil.executePOSTRequest(info.getExpressCheckoutURL(), request);
	        if (!CommonUtil.isParameterEmptyOrNull(response)) {
	        	log.info("Response from server: " + response);
		        res = (ResponseCheckout) CommonUtil.generateXmlToObject(ResponseCheckout.class, URLDecoder.decode(response, "UTF-8"));
		        if (res != null && res.getPaymentSolutions() != null) {
		        	paymentSolutions = res.getPaymentSolutions().getPaymentSolution();
		        } else {
		        	log.error("ERROR! chooseEPGCheckoutDeposit. don't get any payment solutions from EPG!");
		        }
	        }
		} catch (Exception e) {
			log.error("ERROR! chooseEPGCheckoutDeposit", e);
		}
		return paymentSolutions;
	}

	/**
	 * @return the paymentSolutionsURL
	 */
	public String getPaymentSolutionsURL() {
		return paymentSolutionsURL;
	}

	/**
	 * @param paymentSolutionsURL the paymentSolutionsURL to set
	 */
	public void setPaymentSolutionsURL(String paymentSolutionsURL) {
		this.paymentSolutionsURL = paymentSolutionsURL;
	}

	/**
	 * @return the cssURL
	 */
	public String getCssURL() {
		return cssURL;
	}

	/**
	 * @param cssURL the cssURL to set
	 */
	public void setCssURL(String cssURL) {
		this.cssURL = cssURL;
	}
	
	
	public static TransactionRequestResponse parseResult(String xmlResponse, ClearingInfo info) throws ParserConfigurationException, SAXException, IOException, SQLException {
    	if (null == info) {
    		info = new ClearingInfo();
    	}
    	Document doc = ClearingUtil.parseXMLToDocument(xmlResponse);
    	TransactionRequestResponse tr = new TransactionRequestResponse();
    	PaymentMethodService pms = new PaymentMethodService();
    	
    	/* payFrexTransactionId */
		ArrayList<String> EPGTransactionIdList = ClearingUtil.getListValueByTagName(doc, "payFrexTransactionId");
		if (!EPGTransactionIdList.isEmpty()) {
			info.setProviderTransactionId(EPGTransactionIdList.get(0));
		}
		
		/* status */
		ArrayList<String> statusList = ClearingUtil.getListValueByTagName(doc, "status");
		if (!statusList.isEmpty()) {
			/* operationStatus */
			pms.setOperationStatus(statusList.get(0));
			boolean isSuccess = false;
			String result = "-1";
			String userMessage = "Transaction failed.";
			if ((statusList.get(0).equalsIgnoreCase("pending") && statusList.get(1).equalsIgnoreCase("success")) 
					|| statusList.get(0).equalsIgnoreCase("success")){
				isSuccess = true;
				result = "1000";
				userMessage = "Permitted transaction.";
			} else if (statusList.get(0).equalsIgnoreCase("redirected")) {
				isSuccess = true;
				result = "1000";
				userMessage = "Permitted transaction.";
				ArrayList<String> redirectionResponseList = ClearingUtil.getListValueByTagName(doc, "redirectionResponse");
				if (!redirectionResponseList.isEmpty()) {
					String redirectionResponse = redirectionResponseList.get(0);
					String[] resp = redirectionResponse.split("\\?");
					String[] respparams = resp[1].split("&");
					info.setPaReq(URLDecoder.decode(respparams[1].split("=")[1], "UTF-8"));
					info.setAcsUrl(respparams[2].split("=")[1]);
					info.setTermsURL(info.getTermsURL() + info.getProviderTransactionId());
				}
			}
			info.setSuccessful(isSuccess);
			info.setResult(result);
			info.setUserMessage(userMessage);
		}
		/* message */
		ArrayList<String> messageList = ClearingUtil.getListValueByTagName(doc, "message");
		if (!messageList.isEmpty()) {
			String message = messageList.get(0);
			if (messageList.size() > 1) {
				message += " | " + messageList.get(messageList.size() - 1);
			}
			info.setMessage(message);
		}
		
		/* paySolTransactionId */
		ArrayList<String> paySolTransactionIdList = ClearingUtil.getListValueByTagName(doc, "paySolTransactionId");
		if (!paySolTransactionIdList.isEmpty()) {
			info.setAuthNumber(paySolTransactionIdList.get(0));
		}
		/* paymentSolution */
		ArrayList<String> paymentSolutionList = ClearingUtil.getListValueByTagName(doc, "paymentSolution");
		if (!paymentSolutionList.isEmpty()) {
			tr.setPaymentSolution(paymentSolutionList.get(0));
		}
		/* amount */
		ArrayList<String> amountList = ClearingUtil.getListValueByTagName(doc, "amount");
		if (!amountList.isEmpty()) {
			BigDecimal bd = new BigDecimal(amountList.get(0));
			bd = bd.multiply(new BigDecimal(100));
			info.setAmount(bd.longValue());
		}
		/* merchantTransactionId */
		ArrayList<String> merchantTransactionList = ClearingUtil.getListValueByTagName(doc, "merchantTransactionId");
		if (!merchantTransactionList.isEmpty()) {
			long tranId = 0;
			Long merchantTransactionId = Long.valueOf(merchantTransactionList.get(0).replace("SECONDARY-", ""));
			if (null != merchantTransactionId) {
				tranId = merchantTransactionId.longValue();
			}
			tr.setTransactionId(tranId);
			if (info.getTransactionId() == 0) {
				info.setTransactionId(tranId);
			}
		}
		/* operationType */
		ArrayList<String> operationTypeList = ClearingUtil.getListValueByTagName(doc, "operationType");
		if (!operationTypeList.isEmpty()) {
			pms.setOperationType(operationTypeList.get(0));
		}
		/* paymentMethodService */
		tr.setPaymentMethodService(pms);
		/* xml */
		tr.setResponse(xmlResponse);
		/* clearingInfo */
		tr.setClearingInfo(info);
		log.info(tr.toString());
		return tr;
    }
    
    /**
     * Get the node where the status is not error we need the data only from this node
     * @param nodeList
     * @param tagName
     * @return
     */
    /*public static Element getElementWithStatusNotError(NodeList nodeList, String tagName) {
		for (int i = 0; i < nodeList.getLength(); i++) {
			Node node = nodeList.item(i); 
			if (node.getNodeType() == Node.ELEMENT_NODE) {
				Element docElement = (Element)node;
				ArrayList<String> statusList = getListValueByTagName(docElement, null, tagName);
				if (!statusList.isEmpty()) {
					if (!statusList.get(0).equalsIgnoreCase("error")) {
						return docElement;
					}
				}
			}
		}
		return null;
    }*/
    
    /**
     * get the value of all tags from tag name
     * @param element element to search in
     * @param tagName the node name we look for
     * @return ArrayList<String> with all values from this node name in the current element
     */
    /*public static ArrayList<String> getListValueByTagName(Element element,Document doc, String tagName) {
    	if (null != element) {
	    	NodeList nodeList = element.getElementsByTagName(tagName);
			ArrayList<String> valList = new ArrayList<String>();
			for (int i = 0; i < nodeList.getLength(); i++) {
				valList.add(nodeList.item(i).getTextContent());
			}
			return valList;
    	} else {
    		return ClearingUtil.getListValueByTagName(doc, tagName);
    	}
    }*/
	
    /*private TransactionRequestResponse handleDepositResponse(Response response, ClearingInfo info) {
		TransactionRequestResponse tr = new TransactionRequestResponse();
		PaymentMethodService pms = new PaymentMethodService();
			
		//payFrexTransactionId 
		if (!response.getOperations().isEmpty()) {
			info.setProviderTransactionId(response.getOperations().get(0).getPayFrexTransactionId());
		}
		
		//status 
		if (!CommonUtil.isParameterEmptyOrNull(response.getOperations().get(0).getStatus())) {
			// operationStatus 
			pms.setOperationStatus(response.getStatus());
			boolean isSuccess = false;
			String result = "-1";
			String userMessage = "Transaction failed.";
			if ((response.getStatus().equalsIgnoreCase("pending")) || response.getStatus().equalsIgnoreCase("success")){
				isSuccess = true;
				result = "1000";
				userMessage = "Permitted transaction.";
			} 
			
			//3d
			else if (response.getStatus().equalsIgnoreCase("redirected")) {
				isSuccess = true;
				result = "1000";
				userMessage = "Permitted transaction.";
				ArrayList<String> redirectionResponseList = getListValueByTagName(element, doc, "redirectionResponse");
				if (!redirectionResponseList.isEmpty()) {
					String redirectionResponse = redirectionResponseList.get(0);
					String[] resp = redirectionResponse.split("\\?");
					String[] respparams = resp[1].split("&");
					info.setPaReq(URLDecoder.decode(respparams[1].split("=")[1], "UTF-8"));
					info.setAcsUrl(respparams[2].split("=")[1]);
					info.setTermsURL(info.getTermsURL() + info.getProviderTransactionId());
				}
			}
			
			info.setSuccessful(isSuccess);
			info.setResult(result);
			info.setUserMessage(userMessage);
		}
		//message 
		String msg = info.getMessage() + " | " + response.getMessage();
		info.setMessage(msg);
		
		//paySolTransactionId 
		info.setAuthNumber(response.getOperations().get(0).getPaySolTransactionId());
		
		// paymentSolution 
		tr.setPaymentSolution(response.getOperations().get(0).getPaymentSolution());
		
		// amount 
		BigDecimal bd = new BigDecimal(response.getOperations().get(0).getAmount());
		bd = bd.multiply(new BigDecimal(100));
		info.setAmount(bd.longValue());
		
		// merchantTransactionId 
		ArrayList<String> merchantTransactionList = getListValueByTagName(element, doc, "merchantTransactionId");
		if (!merchantTransactionList.isEmpty()) {
			long tranId = 0;
			Long merchantTransactionId = Long.valueOf(merchantTransactionList.get(0).replace("SECONDARY-", ""));
			if (null != merchantTransactionId) {
				tranId = merchantTransactionId.longValue();
			}
			tr.setTransactionId(tranId);
			if (info.getTransactionId() == 0) {
				info.setTransactionId(tranId);
			}
		}
		//operationType 
		ArrayList<String> operationTypeList = getListValueByTagName(element, doc, "operationType");
		if (!operationTypeList.isEmpty()) {
			pms.setOperationType(operationTypeList.get(0));
		}
		 //paymentMethodService 
		tr.setPaymentMethodService(pms);
		// xml 
		tr.setResponse(xmlResponse);
		// clearingInfo 
		tr.setClearingInfo(info);
		log.info(tr.toString());
		return tr;
	}*/
}