package com.anyoption.common.annotations;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;
import java.lang.annotation.ElementType;
import java.lang.annotation.RetentionPolicy;

import com.anyoption.common.util.ConstantsBase;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD) //can use in method only.
public @interface MethodInfoAnnotations {
	public static final String TYPE_DATE = "date";
	public static final long FORMAT_TYPE_FROM_USD_SMALL_AMOUNT = 1;
	
	public String type() default ConstantsBase.EMPTY_STRING;
	public long formatType() default 0;
	public boolean isMsgKey() default false;
}
