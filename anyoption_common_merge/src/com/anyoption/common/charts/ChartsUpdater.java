package com.anyoption.common.charts;

import java.util.ArrayList;
import java.util.EnumMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.ChartsUpdaterMarket;
import com.anyoption.common.beans.MarketRate;
import com.anyoption.common.beans.base.Opportunity;
import com.anyoption.common.enums.SkinGroup;
import com.anyoption.common.managers.ChartsUpdaterManager;

public class ChartsUpdater extends Thread {
    private static final Logger log = Logger.getLogger(ChartsUpdater.class);

    private boolean running;
    private boolean loaded;
    private Hashtable<Long, MarketRate> lastMarketRates;
    private Hashtable<Long, Boolean> marketStates;
    private ArrayList<ChartsUpdaterListener> listeners;

    public ChartsUpdater() {
        listeners = new ArrayList<ChartsUpdaterListener>();
        setDaemon(true);
    }

    public void addListener(ChartsUpdaterListener listener) {
    	addListener(listener, 2);   // for web use(we expect 2 listeners here - LevelHistoryCache and ChartHistoryCache)
    }

    public void addListener(ChartsUpdaterListener listener, long numListeners) {
        listeners.add(listener);
        if (listeners.size() >= numListeners && !isAlive()) {
            // if the audience arrived start the show :)
            this.start();
        }
    }

    public void removeListener(ChartsUpdaterListener listener) {
        listeners.remove(listener);
    }

    @Override
	public void run() {
        Thread.currentThread().setName("ChartsUpdater");
        loaded = false;
        running = true;

        lastMarketRates = new Hashtable<Long, MarketRate>();
        marketStates = new Hashtable<Long, Boolean>();
        long tickStartTime;
        while (running) {
            if (log.isDebugEnabled()) {
                log.debug("Loading markets rates.");
            }
            tickStartTime = System.currentTimeMillis();
            try {
                ChartsUpdaterMarket market = null;
                MarketRate mr = null;
                Map<SkinGroup, List<MarketRate>> mrs = null;
                ArrayList<ChartsUpdaterMarket> markets = ChartsUpdaterManager.getMarkets(Opportunity.TYPE_REGULAR);
                markets.addAll(ChartsUpdaterManager.getMarkets(Opportunity.TYPE_OPTION_PLUS));
                markets.addAll(ChartsUpdaterManager.getMarkets(Opportunity.TYPE_BINARY_0_100_ABOVE));
                markets.addAll(ChartsUpdaterManager.getMarkets(Opportunity.TYPE_DYNAMICS));// build pack_ao-charts, then replace .jar file in tomcat lib directory
                log.trace("Finished loading markets");										// if you want to test it locally!!!
                for (int i = 0; i < markets.size(); i++) {
                    market = markets.get(i);
                    if (market.isOpened()) {
                        mr = lastMarketRates.get(market.getId());
                        mrs = ChartsUpdaterManager.getMarketRates(
                        								market.getId(),
                        								(int) market.getDecimalPoint(),
                        								null != mr ? mr.getRateTime() : null);
                        if (log.isTraceEnabled()) {
                            log.trace("Loading market: " + market.getDisplayNameKey()
                            			+ " last rate: " + (null != mr ? mr.getRateTime() : "null")
                            			+ " rates loaded: " + getRatesNumberPerGroupMap(mrs));
                        }
                        if (mrs.size() > 0) {
                            for (int j = 0; j < listeners.size(); j++) {
                                listeners.get(j).marketRates(market, mrs);
                            }

                            Iterator<Map.Entry<SkinGroup, List<MarketRate>>> iter = mrs.entrySet().iterator();
                            List<MarketRate> ratesList = null;
                            if (iter.hasNext()) {
                            	ratesList = iter.next().getValue();
                            }
                            lastMarketRates.put(market.getId(), ratesList.get(ratesList.size() - 1));
                        }
                    } else {
                    	log.trace("Closed");
                        if (null != marketStates.get(market.getId())) {
                            boolean wasOpened = marketStates.get(market.getId());
                            if (wasOpened) {
                                if (log.isDebugEnabled()) {
                                    log.debug(market.getDisplayNameKey() + " closed.");
                                }
                                lastMarketRates.remove(market.getId());
                                for (int j = 0; j < listeners.size(); j++) {
                                    listeners.get(j).marketClosed(market);
                                }
                            }
                        }
                    }
                    marketStates.put(market.getId(), market.isOpened());
                }
                loaded = true;
                log.debug("Loaded markets rates in: " + (System.currentTimeMillis() - tickStartTime));
            } catch (Exception e) {
                log.error("Problem in the charts updater.", e);
            }
            try {
                Thread.sleep(6000);
            } catch (Exception e) {
                log.error("ChartsUpdater sleep interrupted.", e);
            }
        }
    }

    public void stopChartsUpdater() {
        running = false;
        this.interrupt();
    }

    public boolean isLoaded() {
        return loaded;
    }

    public boolean isRunning() {
        return running;
    }

    private Map<SkinGroup, Integer> getRatesNumberPerGroupMap(Map<SkinGroup, List<MarketRate>> map) {
    	Map<SkinGroup, Integer> ratesNumberPerGroup = new EnumMap<SkinGroup, Integer>(SkinGroup.class);
    	for (Map.Entry<SkinGroup, List<MarketRate>> entry : map.entrySet()) {
			ratesNumberPerGroup.put(entry.getKey(), entry.getValue().size());
		}
    	return ratesNumberPerGroup;
    }
}