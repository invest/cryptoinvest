package com.anyoption.common.charts;

import java.util.ArrayList;
import java.util.EnumMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import com.anyoption.common.beans.ChartsUpdaterMarket;
import com.anyoption.common.beans.MarketRate;
import com.anyoption.common.enums.SkinGroup;

/**
 * Keep the the level history of each market from the current hour. To be used in showing the history of hour opps.
 *
 * @author Tony
 */
public class LevelHistoryCache implements ChartsUpdaterListener {
    private Hashtable<Long, Map<SkinGroup, List<MarketRate>>> histories;

    public LevelHistoryCache() {
        histories = new Hashtable<Long, Map<SkinGroup, List<MarketRate>>>();
    }

    public void marketRates(ChartsUpdaterMarket m, Map<SkinGroup, List<MarketRate>> mrs) {
    	Map<SkinGroup, List<MarketRate>> h = histories.get(m.getId());
        if (null == h) {
        	h = new EnumMap<SkinGroup, List<MarketRate>>(SkinGroup.class);
            for (SkinGroup skinGroup : SkinGroup.NON_AUTO_SET) {
				h.put(skinGroup, new ArrayList<MarketRate>());
			}
            histories.put(m.getId(), h);
        }
        for (Map.Entry<SkinGroup, List<MarketRate>> entry : h.entrySet()) {
        	SkinGroup skinGroup = entry.getKey();
        	List<MarketRate> cachedRates = entry.getValue();
	        while (cachedRates.size() > 0
	        		&& ((null != m.getLastHourClosingTime()
	    					&& cachedRates.get(0).getRateTime().compareTo(m.getLastHourClosingTime()) < 0)
	    				|| (System.currentTimeMillis() - cachedRates.get(0).getRateTime().getTime() > 4 * 60 * 60 * 1000))) {
	            // clearing all before last closing is good for hour opps
	            // BUT end of the day is also considered as hour opp. and we can have quotes in the db after
	            // end of the day est closing (as actual closing can be after est closing)
	            // so make a raw guess there are at least 4 hours "night brake" and clean all rates older than that
	        	cachedRates.remove(0);
	        }
	        List<MarketRate> rates = mrs.get(skinGroup);
        	if (rates != null) {
		        for (int i = 0; i < rates.size(); i++) {
		            if (null == m.getLastHourClosingTime()
		            		|| rates.get(i).getRateTime().compareTo(m.getLastHourClosingTime()) > 0) {
		            	cachedRates.add(rates.get(i));
		            }
		        }
        	}
        }
    }

    public void marketClosed(ChartsUpdaterMarket m) {
        histories.remove(m.getId());
    }

    public Map<SkinGroup, List<MarketRate>> getMarketHistory(long marketId) {
    	Map<SkinGroup, List<MarketRate>> h = histories.get(marketId);
        if (null != h) {
        	Map<SkinGroup, List<MarketRate>> map = new EnumMap<SkinGroup, List<MarketRate>>(h);
        	for (SkinGroup skinGroup : map.keySet()) {
				map.put(skinGroup, new ArrayList<MarketRate>(map.get(skinGroup)));
			}
            return map;
        }
        return null;
    }
}