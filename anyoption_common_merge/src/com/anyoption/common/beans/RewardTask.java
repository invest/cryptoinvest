package com.anyoption.common.beans;

/**
 * @author liors
 *
 */
public class RewardTask implements java.io.Serializable {
	private static final long serialVersionUID = 3996933059225679678L;
	private int id;
	private int typeId;
	private int levelId;
	private int rewardTierId;
	private int numOfActions;
	private int numOfRecurring;
	private double experiencePoints;
	private RewardTaskCurrency rewardTaskCurrency;
	
	/**
	 * @return the rewardTierId
	 */
	public int getRewardTierId() {
		return rewardTierId;
	}
	/**
	 * @param rewardTierId the rewardTierId to set
	 */
	public void setRewardTierId(int rewardTierId) {
		this.rewardTierId = rewardTierId;
	}
	/**
	 * @return the numOfActions
	 */
	public int getNumOfActions() {
		return numOfActions;
	}
	/**
	 * @param numOfActions the numOfActions to set
	 */
	public void setNumOfActions(int numOfActions) {
		this.numOfActions = numOfActions;
	}
	/**
	 * @return the numOfRecurring
	 */
	public int getNumOfRecurring() {
		return numOfRecurring;
	}
	/**
	 * @param numOfRecurring the numOfRecurring to set
	 */
	public void setNumOfRecurring(int numOfRecurring) {
		this.numOfRecurring = numOfRecurring;
	}
	/**
	 * 
	 * @return
	 */
	public double getExperiencePoints() {
		return experiencePoints;
	}
	/**
	 * 
	 * @param experiencePoints
	 */
	public void setExperiencePoints(double experiencePoints) {
		this.experiencePoints = experiencePoints;
	}
	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}
	/**
	 * @return the typeId
	 */
	public int getTypeId() {
		return typeId;
	}
	/**
	 * @param typeId the typeId to set
	 */
	public void setTypeId(int typeId) {
		this.typeId = typeId;
	}
	/**
	 * @return the levelId
	 */
	public int getLevelId() {
		return levelId;
	}
	/**
	 * @param levelId the levelId to set
	 */
	public void setLevelId(int levelId) {
		this.levelId = levelId;
	}
	/**
	 * @return the rewardTaskCurrency
	 */
	public RewardTaskCurrency getRewardTaskCurrency() {
		return rewardTaskCurrency;
	}
	/**
	 * @param rewardTaskCurrency the rewardTaskCurrency to set
	 */
	public void setRewardTaskCurrency(RewardTaskCurrency rewardTaskCurrency) {
		this.rewardTaskCurrency = rewardTaskCurrency;
	} 
}
