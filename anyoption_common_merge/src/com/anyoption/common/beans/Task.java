package com.anyoption.common.beans;

/**
 * @author LioR SoLoMoN
 *
 */
public class Task {
	private long taskSubjectsId;
	private String clazz;
	private String parameters;
	private long referenceId;
	private Status status;
	private int writerId;
	private int taskSubjectsGroupId;
	private int numOfRetries;
	private int maxRetries;
	
	/**
	 * 
	 */
	public Task() {
		
	}
	
	/**
	 * @param referenceId
	 * @param taskSubjectsGroupId
	 */
	public Task(long referenceId, int taskSubjectsGroupId) {
		this.referenceId = referenceId;
		this.taskSubjectsGroupId = taskSubjectsGroupId;
	}

	/**
	 * @param referenceId
	 * @param status
	 */
	public Task(long referenceId, Status status) {
		this.referenceId = referenceId;
		this.status = status;
	}
	
	/**
	 * @param referenceId
	 * @param status
	 * @param writerId
	 * @param taskSubjectsGroupId
	 */
	public Task(long referenceId, Status status, int writerId,
			int taskSubjectsGroupId) {
		this.referenceId = referenceId;
		this.status = status;
		this.writerId = writerId;
		this.taskSubjectsGroupId = taskSubjectsGroupId;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Task [taskSubjectsId=" + taskSubjectsId + ", clazz=" + clazz
				+ ", parameters=" + parameters + ", referenceId=" + referenceId
				+ ", status=" + status + ", writerId=" + writerId
				+ ", taskSubjectsGroupId=" + taskSubjectsGroupId
				+ ", numOfRetries=" + numOfRetries + "]";
	}

	/**
	 * @return the clazz
	 */
	public String getClazz() {
		return clazz;
	}
	/**
	 * @param clazz the clazz to set
	 */
	public void setClazz(String clazz) {
		this.clazz = clazz;
	}
	/**
	 * @return the parameters
	 */
	public String getParameters() {
		return parameters;
	}
	/**
	 * @param parameters the parameters to set
	 */
	public void setParameters(String parameters) {
		this.parameters = parameters;
	}
	/**
	 * @return the writerId
	 */
	public int getWriterId() {
		return writerId;
	}
	/**
	 * @param writerId the writerId to set
	 */
	public void setWriterId(int writerId) {
		this.writerId = writerId;
	}
	
	/**
	 * @return the status
	 */
	public Status getStatus() {
		return status;
	}
	/**
	 * @param status the status to set
	 */
	public void setStatus(Status status) {
		this.status = status;
	}

	/**
	 * @return the referenceId
	 */
	public long getReferenceId() {
		return referenceId;
	}
	/**
	 * @param referenceId the referenceId to set
	 */
	public void setReferenceId(long referenceId) {
		this.referenceId = referenceId;
	}

	/**
	 * @return the taskSubjectsId
	 */
	public long getTaskSubjectsId() {
		return taskSubjectsId;
	}

	/**
	 * @param taskSubjectsId the taskSubjectsId to set
	 */
	public void setTaskSubjectsId(long taskSubjectsId) {
		this.taskSubjectsId = taskSubjectsId;
	}

	/**
	 * @return the taskSubjectsGroupId
	 */
	public int getTaskSubjectsGroupId() {
		return taskSubjectsGroupId;
	}

	/**
	 * @param taskSubjectsGroupId the taskSubjectsGroupId to set
	 */
	public void setTaskSubjectsGroupId(int taskSubjectsGroupId) {
		this.taskSubjectsGroupId = taskSubjectsGroupId;
	}

	/**
	 * @return the numOfRetries
	 */
	public int getNumOfRetries() {
		return numOfRetries;
	}

	/**
	 * @param numOfRetries the numOfRetries to set
	 */
	public void setNumOfRetries(int numOfRetries) {
		this.numOfRetries = numOfRetries;
	}

	/**
	 * @return the maxRetries
	 */
	public int getMaxRetries() {
		return maxRetries;
	}

	/**
	 * @param maxRetries the maxRetries to set
	 */
	public void setMaxRetries(int maxRetries) {
		this.maxRetries = maxRetries;
	}

	public enum Status {
		FAIL(0), DONE(1), PENDING(2), IN_PROGRESS(3);
		
		Status(int id) {
			this.setId(id);
		}
		
		/**
		 * @return the id
		 */
		public int getId() {
			return id;
		}

		/**
		 * @param id the id to set
		 */
		public void setId(int id) {
			this.id = id;
		}

		private int id;
	}
}
