/**
 *
 */
package com.anyoption.common.beans;

import java.util.Date;

import com.anyoption.common.annotations.AnyoptionNoJSON;

/**
 * @author IdanZ
 *
 */
public class SpecialCare implements java.io.Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    @AnyoptionNoJSON
    protected Date timeScTurnover;
    @AnyoptionNoJSON    
    protected Date timeScHouseResult;
    @AnyoptionNoJSON
    protected Date timeScManual;
    @AnyoptionNoJSON
    protected long scUpdatedBy;

    @AnyoptionNoJSON
    boolean isSpecialCareRed;
    @AnyoptionNoJSON
    boolean isSpecialCareGreen;
    @AnyoptionNoJSON
    String isSpecialCareRedTxt;
    @AnyoptionNoJSON
    String isSpecialCareGreenTxt;


    public SpecialCare (){}


    /**
     * @return the timeScTurnover
     */
    public Date getTimeScTurnover() {
        return timeScTurnover;
    }


    /**
     * @param timeScTurnover the timeScTurnover to set
     */
    public void setTimeScTurnover(Date timeScTurnover) {
        this.timeScTurnover = timeScTurnover;
    }


    /**
     * @return the timeScHouseResult
     */
    public Date getTimeScHouseResult() {
        return timeScHouseResult;
    }


    /**
     * @param timeScHouseResult the timeScHouseResult to set
     */
    public void setTimeScHouseResult(Date timeScHouseResult) {
        this.timeScHouseResult = timeScHouseResult;
    }

    public boolean isSpecialCare(){
        return (timeScHouseResult != null || timeScTurnover != null || timeScManual != null);
    }

    public boolean isSpecialCareRed(){
        return (timeScTurnover != null);
    }

    public boolean isSpecialCareGreen(){
        return (timeScHouseResult != null);
    }

    public boolean isSpecialCareManual(){
        return (timeScManual != null);
    }

    public String getIsSpecialCareTxt() {
        return (timeScHouseResult != null || timeScTurnover != null || timeScManual != null ? "yes" : "no");
    }

    public boolean isAlreadySpecialCare(){
        return (getIsSpecialCareTxt() == "no");
    }

    public boolean isCanChangeToNo(){
        return ((timeScHouseResult == null && timeScTurnover == null && timeScManual != null) || (timeScHouseResult == null && timeScTurnover == null && timeScManual == null));
    }

    public String getImage() {
        if (timeScHouseResult != null) {
            return "special_care_green.jpg";
        } else if (timeScTurnover != null || timeScManual != null) {
            return "special_care.jpg";
        }
        return null;
    }

    /**
     * @return the updatedBy
     */
    public long getScUpdatedBy() {
        return scUpdatedBy;
    }


    /**
     * @param updatedBy the updatedBy to set
     */
    public void setScUpdatedBy(long updatedBy) {
        this.scUpdatedBy = updatedBy;
    }


    /**
     * @return the timeScManual
     */
    public Date getTimeScManual() {
        return timeScManual;
    }


    /**
     * @param timeScManual the timeScManual to set
     */
    public void setTimeScManual(Date timeScManual) {
        this.timeScManual = timeScManual;
    }

}
