package com.anyoption.common.beans;

import java.util.Date;


public class UsersAwardBonusGroup implements java.io.Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -4808873616666565346L;
	private long id;
	private String description;
	private Date timeCreated;
	private Date redeemUntilDate;
    private long bonusId;

	public long getId() {
		return id;
	}
	
	public void setId(long id) {
		this.id = id;
	}
	
	public String getDescription() {
		return description;
	}
	
	public void setDescription(String description) {
		this.description = description;
	}
	
	public Date getTimeCreated() {
		return timeCreated;
	}
	
	public void setTimeCreated(Date timeCreated) {
		this.timeCreated = timeCreated;
	}
	
	public long getBonusId() {
		return bonusId;
	}
	
	public void setBonusId(long bonusId) {
		this.bonusId = bonusId;
	}

	public Date getRedeemUntilDate() {
		return redeemUntilDate;
	}

	public void setRedeemUntilDate(Date redeemUntilDate) {
		this.redeemUntilDate = redeemUntilDate;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
}
