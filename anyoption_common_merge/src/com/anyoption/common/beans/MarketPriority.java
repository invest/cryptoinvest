package com.anyoption.common.beans;

public class MarketPriority {
	long skinMarketGroupMarketId;
	long marketId;
	String marketName;
	public long getSkinMarketGroupMarketId() {
		return skinMarketGroupMarketId;
	}
	public void setSkinMarketGroupMarketId(long skinMarketGroupMarketId) {
		this.skinMarketGroupMarketId = skinMarketGroupMarketId;
	}
	public long getMarketId() {
		return marketId;
	}
	public void setMarketId(long marketId) {
		this.marketId = marketId;
	}
	public String getMarketName() {
		return marketName;
	}
	public void setMarketName(String marketName) {
		this.marketName = marketName;
	}
	@Override
	public String toString() {
		return "MarketPriority [skinMarketGroupMarketId=" + skinMarketGroupMarketId + ", marketId=" + marketId
				+ ", marketName=" + marketName + "]";
	}
	
	
}
