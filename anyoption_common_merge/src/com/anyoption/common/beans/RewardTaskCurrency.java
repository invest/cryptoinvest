package com.anyoption.common.beans;

/**
 * @author liors
 *
 */
public class RewardTaskCurrency implements java.io.Serializable {

	private static final long serialVersionUID = -5294468506726270182L;
	private int id;
	private int taskId;
	private int currencyId;
	private long fromAmount;
	private long toAmount;
	private double amountToPoints;	
	
	public RewardTaskCurrency(double amountToPoints) {
		this.id = 0;
		this.taskId = 0;
		this.currencyId = 0;
		this.fromAmount = 0;
		this.toAmount = 0;
		this.amountToPoints = amountToPoints;
	}
	
	/**
	 * @param id
	 * @param taskId
	 * @param currencyId
	 * @param fromAmount
	 * @param toAmount
	 * @param amountToPoints
	 */
	public RewardTaskCurrency(int id, int taskId, int currencyId,
			long fromAmount, long toAmount, double amountToPoints) {
		this.id = id;
		this.taskId = taskId;
		this.currencyId = currencyId;
		this.fromAmount = fromAmount;
		this.toAmount = toAmount;
		this.amountToPoints = amountToPoints;
	}

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}
	/**
	 * @return the taskId
	 */
	public int getTaskId() {
		return taskId;
	}
	/**
	 * @param taskId the taskId to set
	 */
	public void setTaskId(int taskId) {
		this.taskId = taskId;
	}
	/**
	 * @return the currencyId
	 */
	public int getCurrencyId() {
		return currencyId;
	}
	/**
	 * @param currencyId the currencyId to set
	 */
	public void setCurrencyId(int currencyId) {
		this.currencyId = currencyId;
	}
	/**
	 * @return the fromAmount
	 */
	public long getFromAmount() {
		return fromAmount;
	}
	/**
	 * @param fromAmount the fromAmount to set
	 */
	public void setFromAmount(long fromAmount) {
		this.fromAmount = fromAmount;
	}
	/**
	 * @return the toAmount
	 */
	public long getToAmount() {
		return toAmount;
	}
	/**
	 * @param toAmount the toAmount to set
	 */
	public void setToAmount(long toAmount) {
		this.toAmount = toAmount;
	}
	/**
	 * @return the amountToPoints
	 */
	public double getAmountToPoints() {
		return amountToPoints;
	}
	/**
	 * @param amountToPoints the amountToPoints to set
	 */
	public void setAmountToPoints(double amountToPoints) {
		this.amountToPoints = amountToPoints;
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "RewardTaskCurrency [id=" + id + ", taskId=" + taskId
				+ ", currencyId=" + currencyId + ", fromAmount=" + fromAmount
				+ ", toAmount=" + toAmount + ", amountToPoints="
				+ amountToPoints + "]";
	}
}
