/**
 * 
 */
package com.anyoption.common.beans;

import java.io.Serializable;
import java.util.Date;

/**
 * @author EyalG
 *
 */
public class TournamentUsers implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private long id;
	private long tournamentId;
	private long userId;
	private double score;
	private double score2;
	private Date timeCreated;
	
	public TournamentUsers() {
	}

	public TournamentUsers(long tournamentId, long userId, double score, double score2) {
		super();
		this.tournamentId = tournamentId;
		this.userId = userId;
		this.score = score;
		this.score2 = score2;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public long getTournamentId() {
		return tournamentId;
	}

	public void setTournamentId(long tournamentId) {
		this.tournamentId = tournamentId;
	}

	public long getUserId() {
		return userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}

	public double getScore() {
		return score;
	}

	public void setScore(double score) {
		this.score = score;
	}

	public double getScore2() {
		return score2;
	}

	public void setScore2(double score2) {
		this.score2 = score2;
	}

	public Date getTimeCreated() {
		return timeCreated;
	}

	public void setTimeCreated(Date timeCreated) {
		this.timeCreated = timeCreated;
	}

	@Override
	public String toString() {
		return "TournamentUsers [id=" + id + ", tournamentId=" + tournamentId
				+ ", userId=" + userId + ", score=" + score + ", score2="
				+ score2 + ", timeCreated=" + timeCreated + "]";
	}
}
