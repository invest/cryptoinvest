/**
 * 
 */
package com.anyoption.common.beans;

import java.io.Serializable;

/**
 * @author pavelt
 *
 */
public class TournamentLanguageTabs implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1999924300835073380L;
	private long tabId;
	private String tabLabel;
	private long langId;
	
	public long getTabId() {
		return tabId;
	}
	public void setTabId(long tabId) {
		this.tabId = tabId;
	}
	public String getTabLabel() {
		return tabLabel;
	}
	public void setTabLabel(String tabLabel) {
		this.tabLabel = tabLabel;
	}
	public long getLangId() {
		return langId;
	}
	public void setLangId(long langId) {
		this.langId = langId;
	}
	
	public String toString(){
		return "TournamentLanguageTabs [tabId=" + tabId + 
				",tabLabel=" + tabLabel + 
				",langId= " + langId + "]";
	}
}
