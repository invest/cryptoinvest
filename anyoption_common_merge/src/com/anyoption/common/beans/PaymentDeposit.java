package com.anyoption.common.beans;

/**
 * @author LioR SoLoMoN
 *
 */
public class PaymentDeposit {
	private String depositVal; 
	private long writerId;
	private User user;
	private int source; 
	private String formName; 
	private long loginId;
	private int providerId;
	private int transactionType;
	private String paymentSolution;
	private int paymentTypeId;
	private String prefixURL;
	
	/**
	 * @param depositVal
	 * @param writerId
	 * @param user
	 * @param source
	 * @param formName
	 * @param loginId
	 * @param providerId
	 * @param transactionType
	 */
	public PaymentDeposit(String depositVal, long writerId, User user,
			int source, String formName, long loginId, int providerId,
			int transactionType, String paymentSolution, int paymentTypeId,
			String prefixURL) {
		this.depositVal = depositVal;
		this.writerId = writerId;
		this.user = user;
		this.source = source;
		this.formName = formName;
		this.loginId = loginId;
		this.providerId = providerId;
		this.transactionType = transactionType;
		this.paymentSolution = paymentSolution;
		this.paymentTypeId = paymentTypeId;
		this.prefixURL = prefixURL;
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "PaymentDeposit [depositVal=" + depositVal + ", writerId="
				+ writerId + ", user=" + user + ", source=" + source
				+ ", formName=" + formName + ", loginId=" + loginId
				+ ", providerId=" + providerId + ", transactionType="
				+ transactionType + ", paymentSolution=" + paymentSolution + ", paymentTypeId="
				+ paymentTypeId + ", prefixURL=" + prefixURL + "]";
	}
	/**
	 * @return the depositVal
	 */
	public String getDepositVal() {
		return depositVal;
	}
	/**
	 * @param depositVal the depositVal to set
	 */
	public void setDepositVal(String depositVal) {
		this.depositVal = depositVal;
	}
	/**
	 * @return the writerId
	 */
	public long getWriterId() {
		return writerId;
	}
	/**
	 * @param writerId the writerId to set
	 */
	public void setWriterId(long writerId) {
		this.writerId = writerId;
	}
	/**
	 * @return the user
	 */
	public User getUser() {
		return user;
	}
	/**
	 * @param user the user to set
	 */
	public void setUser(User user) {
		this.user = user;
	}
	/**
	 * @return the source
	 */
	public int getSource() {
		return source;
	}
	/**
	 * @param source the source to set
	 */
	public void setSource(int source) {
		this.source = source;
	}
	/**
	 * @return the formName
	 */
	public String getFormName() {
		return formName;
	}
	/**
	 * @param formName the formName to set
	 */
	public void setFormName(String formName) {
		this.formName = formName;
	}
	/**
	 * @return the loginId
	 */
	public long getLoginId() {
		return loginId;
	}
	/**
	 * @param loginId the loginId to set
	 */
	public void setLoginId(long loginId) {
		this.loginId = loginId;
	}
	/**
	 * @return the providerId
	 */
	public int getProviderId() {
		return providerId;
	}
	/**
	 * @param providerId the providerId to set
	 */
	public void setProviderId(short providerId) {
		this.providerId = providerId;
	}
	/**
	 * @return the transactionType
	 */
	public int getTransactionType() {
		return transactionType;
	}
	/**
	 * @param transactionType the transactionType to set
	 */
	public void setTransactionType(short transactionType) {
		this.transactionType = transactionType;
	}

	/**
	 * @return the paymentSolution
	 */
	public String getPaymentSolution() {
		return paymentSolution;
	}

	/**
	 * @param paymentSolution the paymentSolution to set
	 */
	public void setPaymentSolution(String paymentSolution) {
		this.paymentSolution = paymentSolution;
	}

	/**
	 * @return the paymentTypeId
	 */
	public int getPaymentTypeId() {
		return paymentTypeId;
	}

	/**
	 * @param paymentTypeId the paymentTypeId to set
	 */
	public void setPaymentTypeId(int paymentTypeId) {
		this.paymentTypeId = paymentTypeId;
	}

	/**
	 * @return the prefixURL
	 */
	public String getPrefixURL() {
		return prefixURL;
	}

	/**
	 * @param prefixURL the prefixURL to set
	 */
	public void setPrefixURL(String prefixURL) {
		this.prefixURL = prefixURL;
	}
}
