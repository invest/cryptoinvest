package com.anyoption.common.beans;

public class MarketPauseDetails {

	protected long	marketPauseId;
	protected int marketId;
	protected byte scheduled;
	protected boolean halfDay;
	protected String pauseStart;
	protected String pauseEnd;
	protected byte validDays;
	protected boolean active;
	
	public int getMarketId() {
		return marketId;
	}
	
	public void setMarketId(int marketId) {
		this.marketId = marketId;
	}
	
	public byte getScheduled() {
		return scheduled;
	}
	
	public void setScheduled(byte scheduled) {
		this.scheduled = scheduled;
	}
	
	public boolean isHalfDay() {
		return halfDay;
	}
	
	public void setHalfDay(boolean halfDay) {
		this.halfDay = halfDay;
	}
	
	public String getPauseStart() {
		return pauseStart;
	}
	
	public void setPauseStart(String pauseStart) {
		this.pauseStart = pauseStart;
	}
	
	public String getPauseEnd() {
		return pauseEnd;
	}
	
	public void setPauseEnd(String pauseEnd) {
		this.pauseEnd = pauseEnd;
	}
	
	public byte getValidDays() {
		return validDays;
	}
	
	public void setValidDays(byte validDays) {
		this.validDays = validDays;
	}
	
	public long getMarketPauseId() {
		return marketPauseId;
	}
	
	public void setMarketPauseId(long marketPauseId) {
		this.marketPauseId = marketPauseId;
	}
	
	public boolean isActive() {
		return active;
	}
	
	public void setActive(boolean active) {
		this.active = active;
	}
}
