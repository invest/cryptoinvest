package com.anyoption.common.beans;

public class EmirReport {
	
	private Long assetId;
	private String commodBase;
	private String commodDet;
	private String exchRateBasis;
	private String ncurr1;
	private String ncurr2;
	private String ospccy;
	private String pac;
	private String pidval1;
	private String uait;
	private String uasset;
	
	public Long getAssetId() {
		return assetId;
	}
	
	public void setAssetId(Long assetId) {
		this.assetId = assetId;
	}
	
	public String getCommodBase() {
		return commodBase;
	}
	
	public void setCommodBase(String commodBase) {
		this.commodBase = commodBase;
	}
	
	public String getCommodDet() {
		return commodDet;
	}
	
	public void setCommodDet(String commodDet) {
		this.commodDet = commodDet;
	}
	
	public String getExchRateBasis() {
		return exchRateBasis;
	}
	
	public void setExchRateBasis(String exchRateBasis) {
		this.exchRateBasis = exchRateBasis;
	}
	
	public String getNcurr1() {
		return ncurr1;
	}
	
	public void setNcurr1(String ncurr1) {
		this.ncurr1 = ncurr1;
	}
	
	public String getNcurr2() {
		return ncurr2;
	}
	
	public void setNcurr2(String ncurr2) {
		this.ncurr2 = ncurr2;
	}
	
	public String getOspccy() {
		return ospccy;
	}
	
	public void setOspccy(String ospccy) {
		this.ospccy = ospccy;
	}
	
	public String getPac() {
		return pac;
	}
	
	public void setPac(String pac) {
		this.pac = pac;
	}
	
	public String getPidval1() {
		return pidval1;
	}
	
	public void setPidval1(String pidval1) {
		this.pidval1 = pidval1;
	}
	
	public String getUait() {
		return uait;
	}
	
	public void setUait(String uait) {
		this.uait = uait;
	}
	
	public String getUasset() {
		return uasset;
	}
	
	public void setUasset(String uasset) {
		this.uasset = uasset;
	}
}