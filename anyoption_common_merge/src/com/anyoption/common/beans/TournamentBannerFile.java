/**
 * 
 */
package com.anyoption.common.beans;

import java.io.Serializable;

import org.apache.myfaces.custom.fileupload.UploadedFile;

/**
 * @author pavelt
 *
 */
public class TournamentBannerFile implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -8151283878166147088L;
	
	private long id;
	private long bannerIndex;
	private String bannerImage;
	private UploadedFile file;
	
	public long getBannerIndex() {
		return bannerIndex;
	}
	public void setBannerIndex(long bannerIndex) {
		this.bannerIndex = bannerIndex;
	}
	public String getBannerImage() {
		return bannerImage;
	}
	public void setBannerImage(String bannerImage) {
		this.bannerImage = bannerImage;
	}
	public UploadedFile getFile() {
		return file;
	}
	public void setFile(UploadedFile file) {
		this.file = file;
	}
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	
	public String toString() {
		return "Tournament Banner [id=" + id + ", bannerIndex=" + bannerIndex
				+ ", bannerImage=" + bannerImage + "]";
	}
	
}
