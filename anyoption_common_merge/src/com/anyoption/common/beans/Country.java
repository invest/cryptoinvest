package com.anyoption.common.beans;

import java.util.HashMap;

/**
 * Country bean wrapper
 *
 * @author KobiM
 */
public class Country extends com.anyoption.common.beans.base.Country {

	private static final long serialVersionUID = 1L;
	private boolean isSmsAvailable;
	private boolean isHaveUkashSite;
	private boolean isAlphaNumericSender;
    private boolean isNetreferRegBlock;
	private HashMap<Long, PaymentMethodCountry> paymentsMethodCountry;
	private boolean isRegulated;

	public String toString() {
	    String ls = System.getProperty("line.separator");
	    return ls + "Country" + ls
	        + super.toString() + ls
	        + "id: " + id + ls
	        + "name: " + name + ls
	        + "a2: " + a2 + ls
	        + "a3: " + a3 + ls
	        + "phoneCode: " + phoneCode + ls
	        + "gmtOffset: " + gmtOffset + ls;
	}

	/**
	 * @return isSmsAvailable
	 */
	public boolean isSmsAvailable() {
		return isSmsAvailable;
	}

	/**
	 * @param isSmsAvailable
	 */
	public void setSmsAvailable(boolean isSmsAvailable) {
		this.isSmsAvailable = isSmsAvailable;
	}

	/**
	 * @return isHaveUkashSite
	 */
	public boolean isHaveUkashSite() {
		return isHaveUkashSite;
	}

	/**
	 * @param isHaveUkashSite
	 */
	public void setHaveUkashSite(boolean isHaveUkashSite) {
		this.isHaveUkashSite = isHaveUkashSite;
	}
	
	public boolean isAlphaNumericSender() {
		return isAlphaNumericSender;
	}

	public void setAlphaNumericSender(boolean isAlphaNumericSender) {
		this.isAlphaNumericSender = isAlphaNumericSender;
	}

    /**
     * @return the isNetreferRegBlock
     */
    public boolean isNetreferRegBlock() {
        return isNetreferRegBlock;
    }

    /**
     * @param isNetreferRegBlock the isNetreferRegBlock to set
     */
    public void setNetreferRegBlock(boolean isNetreferRegBlock) {
        this.isNetreferRegBlock = isNetreferRegBlock;
    }
	
	/**
	 * @return the paymentsMethodCountry
	 */
	public HashMap<Long, PaymentMethodCountry> getPaymentsMethodCountry() {
		return paymentsMethodCountry;
	}
	
	/**
	 * @param payments the paymentsMethodCountry to set
	 */
	public void setPaymentsMethodCountry(HashMap<Long, PaymentMethodCountry> paymentsMethodCountry) {
		this.paymentsMethodCountry = paymentsMethodCountry;
	}

	public boolean isRegulated() {
		return isRegulated;
	}

	public void setRegulated(boolean isRegulated) {
		this.isRegulated = isRegulated;
	}
}