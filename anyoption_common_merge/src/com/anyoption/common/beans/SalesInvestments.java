/**
 * 
 */
package com.anyoption.common.beans;

import java.io.Serializable;

/**
 * @author Eyal Goren
 *
 */
public class SalesInvestments implements Serializable {
	
	private static final long serialVersionUID = 430585515538557258L;
	private String name;
	private double totalInv; //cents
	private double traderValue; //cents
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public double getTotalInv() {
		return totalInv;
	}
	
	public void setTotalInv(double totalInv) {
		this.totalInv = totalInv;
	}
	
	public double getTraderValue() {
		return traderValue;
	}
	
	public void setTraderValue(double traderValue) {
		this.traderValue = traderValue;
	}
	
}
