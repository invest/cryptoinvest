package com.anyoption.common.beans;

import java.io.Serializable;
import java.util.Date;

/**
 * ServerPixelsPublisherConfig
 * @author eyalo
 */
public class ServerPixelsPublisherConfig implements Serializable {
	private static final long serialVersionUID = -1609432375960940721L;
	
	private long id;
	private long serverPixelsPublisherId;
	private long serverPixelsTypeId;
	private String pixelUrl;
	private String handlerClass;
	private String handlerClassParams;
	private long writerId;
	private Date timeCreated;
	private long platformId;
	
	/**
	 * @return the id
	 */
	public long getId() {
		return id;
	}
	
	/**
	 * @param id the id to set
	 */
	public void setId(long id) {
		this.id = id;
	}
	
	/**
	 * @return the serverPixelsPublisherId
	 */
	public long getServerPixelsPublisherId() {
		return serverPixelsPublisherId;
	}
	
	/**
	 * @param serverPixelsPublisherId the serverPixelsPublisherId to set
	 */
	public void setServerPixelsPublisherId(long serverPixelsPublisherId) {
		this.serverPixelsPublisherId = serverPixelsPublisherId;
	}
	
	/**
	 * @return the serverPixelsTypeId
	 */
	public long getServerPixelsTypeId() {
		return serverPixelsTypeId;
	}
	
	/**
	 * @param serverPixelsTypeId the serverPixelsTypeId to set
	 */
	public void setServerPixelsTypeId(long serverPixelsTypeId) {
		this.serverPixelsTypeId = serverPixelsTypeId;
	}
	
	/**
	 * @return the pixelUrl
	 */
	public String getPixelUrl() {
		return pixelUrl;
	}
	
	/**
	 * @param pixelUrl the pixelUrl to set
	 */
	public void setPixelUrl(String pixelUrl) {
		this.pixelUrl = pixelUrl;
	}
	
	/**
	 * @return the handlerClass
	 */
	public String getHandlerClass() {
		return handlerClass;
	}
	
	/**
	 * @param handlerClass the handlerClass to set
	 */
	public void setHandlerClass(String handlerClass) {
		this.handlerClass = handlerClass;
	}
	
	/**
	 * @return the handlerClassParams
	 */
	public String getHandlerClassParams() {
		return handlerClassParams;
	}
	
	/**
	 * @param handlerClassParams the handlerClassParams to set
	 */
	public void setHandlerClassParams(String handlerClassParams) {
		this.handlerClassParams = handlerClassParams;
	}
	
	/**
	 * @return the writerId
	 */
	public long getWriterId() {
		return writerId;
	}
	
	/**
	 * @param writerId the writerId to set
	 */
	public void setWriterId(long writerId) {
		this.writerId = writerId;
	}
	
	/**
	 * @return the timeCreated
	 */
	public Date getTimeCreated() {
		return timeCreated;
	}
	
	/**
	 * @param timeCreated the timeCreated to set
	 */
	public void setTimeCreated(Date timeCreated) {
		this.timeCreated = timeCreated;
	}

	/**
	 * @return the platformId
	 */
	public long getPlatformId() {
		return platformId;
	}

	/**
	 * @param platformId the platformId to set
	 */
	public void setPlatformId(long platformId) {
		this.platformId = platformId;
	}
}
