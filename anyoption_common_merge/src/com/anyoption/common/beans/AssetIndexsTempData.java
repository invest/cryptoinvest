package com.anyoption.common.beans;

public class AssetIndexsTempData {

	
	private long marketId;
	private String marketName;
	private String marketDescription;
	private boolean isHourly;
	private boolean isEndOfDay;
	private boolean isEndOfWeek;
	private boolean isEndOfMonth;
	private String expFormulaFirst;
	private String expFormulaSecond;
	private String additionalText;
	private boolean isLastValueHourly;
	private boolean isLastValueEndOfDay;
	private boolean isLastValueEndOfWeek;
	private boolean isLastValueEndOfMonth;
	private boolean isClosingLevelHourly;
	private boolean isClosingLevelEndOfDay;
	private boolean isClosingLevelEndOfWeek;
	private boolean isClosingLevelEndOfMonth;
	private String reutersFieald;
	private String marketFeedName;
	public long getMarketId() {
		return marketId;
	}
	public void setMarketId(long marketId) {
		this.marketId = marketId;
	}
	public String getMarketDescription() {
		return marketDescription;
	}
	public void setMarketDescription(String marketDescription) {
		this.marketDescription = marketDescription;
	}
	public String getMarketName() {
		return marketName;
	}
	public void setMarketName(String marketName) {
		this.marketName = marketName;
	}
	public boolean isHourly() {
		return isHourly;
	}
	public void setHourly(boolean isHourly) {
		this.isHourly = isHourly;
	}
	public boolean isEndOfDay() {
		return isEndOfDay;
	}
	public void setEndOfDay(boolean isEndOfDay) {
		this.isEndOfDay = isEndOfDay;
	}
	public boolean isEndOfWeek() {
		return isEndOfWeek;
	}
	public void setEndOfWeek(boolean isEndOfWeek) {
		this.isEndOfWeek = isEndOfWeek;
	}
	public boolean isEndOfMonth() {
		return isEndOfMonth;
	}
	public void setEndOfMonth(boolean isEndOfMonth) {
		this.isEndOfMonth = isEndOfMonth;
	}
	public String getExpFormulaFirst() {
		return expFormulaFirst;
	}
	public void setExpFormulaFirst(String expFormulaFirst) {
		this.expFormulaFirst = expFormulaFirst;
	}
	public String getExpFormulaSecond() {
		return expFormulaSecond;
	}
	public void setExpFormulaSecond(String expFormulaSecond) {
		this.expFormulaSecond = expFormulaSecond;
	}
	public String getAdditionalText() {
		return additionalText;
	}
	public void setAdditionalText(String additionalText) {
		this.additionalText = additionalText;
	}
	public boolean isLastValueHourly() {
		return isLastValueHourly;
	}
	public void setLastValueHourly(boolean isLastValueHourly) {
		this.isLastValueHourly = isLastValueHourly;
	}
	public boolean isLastValueEndOfDay() {
		return isLastValueEndOfDay;
	}
	public void setLastValueEndOfDay(boolean isLastValueEndOfDay) {
		this.isLastValueEndOfDay = isLastValueEndOfDay;
	}
	public boolean isLastValueEndOfWeek() {
		return isLastValueEndOfWeek;
	}
	public void setLastValueEndOfWeek(boolean isLastValueEndOfWeek) {
		this.isLastValueEndOfWeek = isLastValueEndOfWeek;
	}
	public boolean isLastValueEndOfMonth() {
		return isLastValueEndOfMonth;
	}
	public void setLastValueEndOfMonth(boolean isLastValueEndOfMonth) {
		this.isLastValueEndOfMonth = isLastValueEndOfMonth;
	}
	public boolean isClosingLevelHourly() {
		return isClosingLevelHourly;
	}
	public void setClosingLevelHourly(boolean isClosingLevelHourly) {
		this.isClosingLevelHourly = isClosingLevelHourly;
	}
	public boolean isClosingLevelEndOfDay() {
		return isClosingLevelEndOfDay;
	}
	public void setClosingLevelEndOfDay(boolean isClosingLevelEndOfDay) {
		this.isClosingLevelEndOfDay = isClosingLevelEndOfDay;
	}
	public boolean isClosingLevelEndOfWeek() {
		return isClosingLevelEndOfWeek;
	}
	public void setClosingLevelEndOfWeek(boolean isClosingLevelEndOfWeek) {
		this.isClosingLevelEndOfWeek = isClosingLevelEndOfWeek;
	}
	public boolean isClosingLevelEndOfMonth() {
		return isClosingLevelEndOfMonth;
	}
	public void setClosingLevelEndOfMonth(boolean isClosingLevelEndOfMonth) {
		this.isClosingLevelEndOfMonth = isClosingLevelEndOfMonth;
	}
	public String getReutersFieald() {
		return reutersFieald;
	}
	public void setReutersFieald(String reutersFieald) {
		this.reutersFieald = reutersFieald;
	}
	public String getMarketFeedName() {
		return marketFeedName;
	}
	public void setMarketFeedName(String marketFeedName) {
		this.marketFeedName = marketFeedName;
	}
	
}
