package com.anyoption.common.beans;

import java.io.Serializable;
import java.util.Iterator;
import java.util.Map;

@SuppressWarnings("serial")
public class LiveJMSMessage implements Serializable {
	public static final String KEY_GLOBE = "globe";
	public static final String KEY_INVESTMENTS_PREFIX = "live";
	public static final String KEY_TRENDS = "trends";
	
	public Map<String, String> fields;
	
	public String toString() {
		String ls = System.getProperty("line.separator");
		StringBuffer sb = new StringBuffer();
        sb.append(ls).append("LiveJMSMessage: ").append(ls);
        if (null != fields) {
            String key = null;
            for (Iterator<String> i = fields.keySet().iterator(); i.hasNext();) {
                key = i.next();
                sb.append(key).append(": ").append(fields.get(key)).append(ls);
            }
        }
		return sb.toString();
	}
}