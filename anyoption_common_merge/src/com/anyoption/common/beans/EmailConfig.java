package com.anyoption.common.beans;

import java.util.Properties;
import javax.mail.Address;

/**
 * @author LioR SoLoMoN
 *
 */
public class EmailConfig {
	private Properties serverProp;
	private String authenticationUsername;
	private String authenticationPassword;
	private String subject;
	private String from;
	private Address[] to;
	private Address[] cc;
	private Address[] bcc;
	private String body;

	public EmailConfig() {
		
	}
	
	/**
	 * @return the subject
	 */
	public String getSubject() {
		return subject;
	}
	/**
	 * @param subject the subject to set
	 */
	public void setSubject(String subject) {
		this.subject = subject;
	}
	/**
	 * @return the from
	 */
	public String getFrom() {
		return from;
	}
	/**
	 * @param from the from to set
	 */
	public void setFrom(String from) {
		this.from = from;
	}
	/**
	 * @return the body
	 */
	public String getBody() {
		return body;
	}
	/**
	 * @param body the body to set
	 */
	public void setBody(String body) {
		this.body = body;
	}
	/**
	 * @return the serverProp
	 */
	public Properties getServerProp() {
		return serverProp;
	}
	/**
	 * @param serverProp the serverProp to set
	 */
	public void setServerProp(Properties serverProp) {
		this.serverProp = serverProp;
	}


	/**
	 * @return the authenticationUsername
	 */
	public String getAuthenticationUsername() {
		return authenticationUsername;
	}


	/**
	 * @param authenticationUsername the authenticationUsername to set
	 */
	public void setAuthenticationUsername(String authenticationUsername) {
		this.authenticationUsername = authenticationUsername;
	}


	/**
	 * @return the authenticationPassword
	 */
	public String getAuthenticationPassword() {
		return authenticationPassword;
	}


	/**
	 * @param authenticationPassword the authenticationPassword to set
	 */
	public void setAuthenticationPassword(String authenticationPassword) {
		this.authenticationPassword = authenticationPassword;
	}

	/**
	 * @return the to
	 */
	public Address[] getTo() {
		return to;
	}

	/**
	 * @param to the to to set
	 */
	public void setTo(Address[] to) {
		this.to = to;
	}

	/**
	 * @return the cc
	 */
	public Address[] getCc() {
		return cc;
	}

	/**
	 * @param cc the cc to set
	 */
	public void setCc(Address[] cc) {
		this.cc = cc;
	}

	/**
	 * @return the bcc
	 */
	public Address[] getBcc() {
		return bcc;
	}

	/**
	 * @param bcc the bcc to set
	 */
	public void setBcc(Address[] bcc) {
		this.bcc = bcc;
	}
}
