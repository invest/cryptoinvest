/**
 * 
 */
package com.anyoption.common.beans;

import java.io.Serializable;

/**
 * @author pavelt
 *
 */
public class Bins implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private long id;
	private String cardPaymentType;
	private String bank;
	private String category;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getCardPaymentType() {
		return cardPaymentType;
	}

	public void setCardPaymentType(String cardPaymentType) {
		this.cardPaymentType = cardPaymentType;
	}

	public String getBank() {
		return bank;
	}

	public void setBank(String bank) {
		this.bank = bank;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}
	
    public String toString() {
        String ls = System.getProperty("line.separator");
        return ls + "Bins: "
            + "id: " + id + ls
            + "cardPaymentType: " + cardPaymentType + ls
            + "bank: " + bank + ls
            + "category: " + category + ls;
    }
}
