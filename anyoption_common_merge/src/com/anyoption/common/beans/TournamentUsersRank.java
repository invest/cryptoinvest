/**
 * 
 */
package com.anyoption.common.beans;

import java.io.Serializable;

/**
 * @author EyalG
 *
 */
public class TournamentUsersRank implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private long rank;
	private long tournamentId;
	private long userId;
	private double score;	
	private String firstName;
	private String lastName;
	private String countryA2;
	private long countryId;
	
	public TournamentUsersRank() {
	}

	public long getRank() {
		return rank;
	}

	public void setRank(long rank) {
		this.rank = rank;
	}

	public long getTournamentId() {
		return tournamentId;
	}

	public void setTournamentId(long tournamentId) {
		this.tournamentId = tournamentId;
	}

	public long getUserId() {
		return userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public double getScore() {
		return score;
	}

	public void setScore(double score) {
		this.score = score;
	}

	public String getCountryA2() {
		return countryA2;
	}

	public void setCountryA2(String countryA2) {
		this.countryA2 = countryA2;
	}
	
	public long getCountryId() {
		return countryId;
	}

	public void setCountryId(long countryId) {
		this.countryId = countryId;
	}

	@Override
	public String toString() {
		return "TournamentUsersRank [rank=" + rank + ", tournamentId="
				+ tournamentId + ", userId=" + userId + ", score=" + score
				+ ", firstName=" + firstName + ", lastName=" + lastName
				+ ", countryA2=" + countryA2 + ", countryId=" + countryId + "]";
	}
}
