package com.anyoption.common.beans;

/**
 * @author kirilim
 */
public class OpportunityMiniBean {

	private long id;
	private long oddsTypeId;
	private String oddsGroup;
	private boolean disabled;
	private boolean marketSuspended;
	private boolean oppSuspended;

	public boolean isOppSuspended() {
	    return oppSuspended;
	}

	public void setOppSuspended(boolean oppSuspended) {
	    this.oppSuspended = oppSuspended;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public long getOddsTypeId() {
		return oddsTypeId;
	}

	public void setOddsTypeId(long oddsTypeId) {
		this.oddsTypeId = oddsTypeId;
	}

	public String getOddsGroup() {
		return oddsGroup;
	}

	public void setOddsGroup(String oddsGroup) {
		this.oddsGroup = oddsGroup;
	}

	public boolean isDisabled() {
		return disabled;
	}

	public void setDisabled(boolean disabled) {
		this.disabled = disabled;
	}

	public boolean isMarketSuspended() {
		return marketSuspended;
	}

	public void setMarketSuspended(boolean marketSuspended) {
		this.marketSuspended = marketSuspended;
	}
}