package com.anyoption.common.beans;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import com.anyoption.common.annotations.AnyoptionNoJSON;

public class Market extends com.anyoption.common.beans.base.Market implements Cloneable {
	
    private static final long serialVersionUID = 1L;
    public static final long PRODUCT_TYPE_BINARY = 1;
    public static final long PRODUCT_TYPE_ONE_TOUCH = 2;
    public static final long PRODUCT_TYPE_OPTION_PLUS = 3;
    public static final long PRODUCT_TYPE_BINARY0100 = 4;
    public static final long PRODUCT_TYPE_BUBBLES = 5;
    public static final long PRODUCT_TYPE_DYNAMICS = 6;
    
    /**
     * Use the fixed value in the markets.fixed_amount_for_shifting in the
     * exposure calculations.
     */
    public static final int TYPE_OF_SHIFTING_FIXED_AMOUNT = 0;

    /**
     * Use the auto calculated average investment in the last 30 days
     * (markets.average_investments) in exposure calculations.
     */
    public static final int TYPE_OF_SHIFTING_AVERAGE_INVESTMENT = 1;

    /**
     * Use the higher of the fixed markets.average_investments and
     * markets.fixed_amount_for_shifting in the
     * exposure calculations.
     */
    public static final int TYPE_OF_SHIFTING_MAX_OF_BOTH = 2;

    public static final int MARKET_TEL_AVIV_25_ID         = 3;
    public static final int MARKET_FTSE_ID                = 4;
    public static final int MARKET_SP_500				  = 6;
    public static final int MARKET_NASDAQ_ID              = 7;
    public static final int MARKET_CAC_ID                 = 10;
    public static final int MARKET_USD_ILS_ID             = 15;
    public static final int MARKET_USD_EUR_ID             = 16;
    public static final int MARKET_USD_TRY_ID             = 17;
    public static final int MARKET_USD_JPY_ID             = 18;
    public static final int MARKET_GOLD_ID                = 20;
    public static final int MARKET_OIL_ID                 = 21;
    public static final int MARKET_USD_RAND_ID            = 26;
    public static final int MARKET_NASDAQF_ID             = 136;
    public static final int MARKET_SILVER_ID              = 137;
    public static final int MARKET_COPPER_ID              = 138;
    public static final int MARKET_AUD_USD_ID             = 289;
    public static final int MARKET_NZD_USD_ID             = 290;
    public static final int MARKET_SHANGHAI_ID            = 294;
    public static final int MARKET_JAKARTA_ID		      = 370;
    public static final int MARKET_ENERGY_ISRAEL_ID       = 435;
    public static final int MARKET_TEL_AVIV_25_MAOF_ID    = 436;
    public static final int MARKET_ISE30_INDEX_ID         = 437;
    public static final int MARKET_KLSE_FUTURE_ID         = 477;
    public static final int MARKET_VIX_ID                 = 538;
    public static final int MARKET_ISE_FINANCIAL_ID       = 539;
    public static final int MARKET_ISE_INDUSTRIAL_ID      = 540;
    public static final int MARKET_DAX_FUTURE_ID          = 541;
    public static final int MARKET_SP_FUTURE_ID           = 542;
    public static final int MARKET_USD_EUR_PLUS_ID        = 552;
    public static final int MARKET_RTS_FUTURE_ID          = 557;
    public static final int MARKET_OIL_PLUS_ID            = 558;
    public static final int MARKET_DAX_FUTURE_PLUS_ID     = 559;
    public static final int MARKET_SP_FUTURE_PLUS_ID      = 560;
    public static final int MARKET_USD_JPY_PLUS_ID        = 561;
    public static final int MARKET_RTS_FUTURE_PLUS_ID     = 571;
    public static final int MARKET_AUD_USD_PLUS_ID        = 581;
    public static final int MARKET_CAC_FUTURE_ID          = 588;
    public static final int MARKET_CAC_FUTURE_PLUS_ID     = 589;
    public static final int MARKET_USD_KRW_ID             = 637;
    public static final int MARKET_BITCOIN_USD_ID 		  = 638;
    public static final int MARKET_LITECOIN_USD_ID 		  = 652;
    public static final int MARKET_SILVER_PLUS_ID         = 653;

    public static final ArrayList<Long> MARKETS_FUTURES  = new ArrayList<Long>() {{
    	add(588L);
        add(541L);
        add(477L);
        add(136L);
        add(557L);
        add(542L);
        add(437L);
    }};

//	private Date timeCreated;
	private long writerId;
    private Long investmentLimitsGroupId;
    private Long exchangeId;
    @AnyoptionNoJSON
    private int groupPriority;
    private BigDecimal currentLevelAlertPerc;

    private int updatesPause;
    private BigDecimal significantPercentage;
    private int realUpdatesPause;
    private BigDecimal realSignificantPercentage;
    private BigDecimal randomCeiling;
    private BigDecimal randomFloor;
    private BigDecimal firstShiftParameter;
    private BigDecimal averageInvestments;
    private BigDecimal percentageOfAverageInvestments;
    private BigDecimal fixedAmountForShifting;
    private BigDecimal acceptableDeviation;
    private BigDecimal acceptableDeviation3;
    private Long typeOfShifting;
    private Long disableAfterPeriod;
    private boolean suspended;
    private String suspendedMessage;
    private boolean hasFutureAgreement;
    private boolean nextAgreementSameDay;
    private String quoteParams;
    private long typeId;

    private String exchangeName;
    private String investmentLimitsGroupName;

    // banner_priority used to tell us the priority of this market
    private int bannersPriority;

    private String timeFirstInvest;
    private String timeEstClosing;
    private String timeZone;

    private boolean isHasLongTermOpp;

    private int secBetweenInvestments;
    private int secBetweenInvestmentsMobile;
    private int secForDev2;
    private int secForDev2Mobile;
    private float amountForDev2;
    private float amountForDev2Night;
    private float amountForDev3;
    private boolean isLimitedForHighAmounts;
    private long limitedFromAmount;

	private int maxExposure;
    // frequencePriority used to tell the frequence of level change when user not login bugId: 2029
    private Date lastHourClosingTime;
    private boolean opened;

    //for 5 golden minutse
    private double insurancePremiaPercent;
    private double insuranceQualifyPercent;
    private boolean isGoldenMinutes;

//  for roll up
    private double rollUpPremiaPercent;
    private double rollUpQualifyPercent;
    private boolean isRollUp;

    private double noAutoSettleAfter;

    private long optionPlusFee;
    private String tradingDays;  // assetIndex
    private int groupMarketsCounter; // assetIndex

    private int secBetweenInvestmentsSameIp;
    private int secBetweenInvestmentsSameIpMobile;


    private boolean isFullDay;    // for mobile assetIndex use
    private boolean isOptionPlus;

//  for 1T
    private long exposureReached;
    private long raiseExposureUpAmount;
    private long raiseExposureDownAmount;
    private double upStrikeRaiseLowerPercent;
    private double downStrikeRaiseLowerPercent;

//    is market open 24/7
//    @AnyoptionNoJSON
//    private boolean twentyFourSeven;

    //for binary 0-100
//    @AnyoptionNoJSON
//    private BinaryZeroOneHundred zeroOneHundred;

    private boolean isLiveFakeInvestments;
    
    // bubble
    private long worstCaseReturn;
    
    private double instantSpread;

    public double getInstantSpread() {
	return instantSpread;
    }

    public void setInstantSpread(double instantSpread) {
        this.instantSpread = instantSpread;
    }

	/**
     * @return the investmentLimitsGroupId
     */
    public Long getInvestmentLimitsGroupId() {
        return investmentLimitsGroupId;
    }

    /**
     * @param investmentLimitsGroupId the investmentLimitsGroupId to set
     */
    public void setInvestmentLimitsGroupId(Long investmentLimitsGroupId) {
        this.investmentLimitsGroupId = investmentLimitsGroupId;
    }

	public BigDecimal getRealSignificantPercentage() {
        return realSignificantPercentage;
    }

    public void setRealSignificantPercentage(BigDecimal realSignificantPercentage) {
        this.realSignificantPercentage = realSignificantPercentage;
    }

    public int getRealUpdatesPause() {
        return realUpdatesPause;
    }

    public void setRealUpdatesPause(int realUpdatesPause) {
        this.realUpdatesPause = realUpdatesPause;
    }

    public BigDecimal getSignificantPercentage() {
        return significantPercentage;
    }

    public void setSignificantPercentage(BigDecimal significantPercentage) {
        this.significantPercentage = significantPercentage;
    }

//    public Date getTimeCreated() {
//		return timeCreated;
//	}
//
//	public void setTimeCreated(Date timeCreated) {
//		this.timeCreated = timeCreated;
//	}

    /**
     * Mobile Refactoring:Remove in markets.xhtml and market.xhtml
     */
//	public String getWriterName() throws SQLException{
//
//		return ApplicationDataBase.getWriterName(writerId);
//	}


	public long getWriterId() {
		return writerId;
	}

	public void setWriterId(long writerId) {
		this.writerId = writerId;
	}

	@Override
	public Long getExchangeId() {
		return exchangeId;
	}

	@Override
	public void setExchangeId(Long exchangeId) {
		this.exchangeId = exchangeId;
	}

	public String getExchangeName() {
		return exchangeName;
	}

	public void setExchangeName(String exchangeName) {
		this.exchangeName = exchangeName;
	}

	public String getInvestmentLimitsGroupName() {
		return investmentLimitsGroupName;
	}

	public void setInvestmentLimitsGroupName(String investmentLimitsGroupName) {
		this.investmentLimitsGroupName = investmentLimitsGroupName;
	}

	public int getUpdatesPause() {
        return updatesPause;
    }

    public void setUpdatesPause(int updatesPause) {
        this.updatesPause = updatesPause;
    }

    public BigDecimal getAverageInvestments() {
        return averageInvestments;
    }

    public void setAverageInvestments(BigDecimal averageInvestments) {
        this.averageInvestments = averageInvestments;
    }

    public BigDecimal getFirstShiftParameter() {
        return firstShiftParameter;
    }

    public void setFirstShiftParameter(BigDecimal firstShiftParameter) {
        this.firstShiftParameter = firstShiftParameter;
    }

    public BigDecimal getFixedAmountForShifting() {
        return fixedAmountForShifting;
    }

    public void setFixedAmountForShifting(BigDecimal fixedAmountForShifting) {
        this.fixedAmountForShifting = fixedAmountForShifting;
    }

    public BigDecimal getPercentageOfAverageInvestments() {
        return percentageOfAverageInvestments;
    }

    public void setPercentageOfAverageInvestments(
            BigDecimal percentageOfAverageInvestments) {
        this.percentageOfAverageInvestments = percentageOfAverageInvestments;
    }

    public BigDecimal getRandomCeiling() {
        return randomCeiling;
    }

    public void setRandomCeiling(BigDecimal randomCeiling) {
        this.randomCeiling = randomCeiling;
    }

    public BigDecimal getRandomFloor() {
        return randomFloor;
    }

    public void setRandomFloor(BigDecimal randomFloor) {
        this.randomFloor = randomFloor;
    }

    public Long getTypeOfShifting() {
        return typeOfShifting;
    }

    public void setTypeOfShifting(Long typeOfShifting) {
        this.typeOfShifting = typeOfShifting;
    }

    public Long getDisableAfterPeriod() {
        return disableAfterPeriod;
    }

    public void setDisableAfterPeriod(Long disableAfterPeriod) {
        this.disableAfterPeriod = disableAfterPeriod;
    }

	public boolean isSuspended() {
        return suspended;
    }

    public void setSuspended(boolean suspended) {
        this.suspended = suspended;
    }

    public String getSuspendedMessage() {
        return suspendedMessage;
    }

    public void setSuspendedMessage(String suspendedMessage) {
        this.suspendedMessage = suspendedMessage;
    }

	public float getAmountForDev2() {
        return amountForDev2;
    }

    public void setAmountForDev2(float amountForDev2) {
        this.amountForDev2 = amountForDev2;
    }

    public int getSecBetweenInvestments() {
        return secBetweenInvestments;
    }

    public void setSecBetweenInvestments(int secBetweenInvestments) {
        this.secBetweenInvestments = secBetweenInvestments;
    }

    public int getSecBetweenInvestmentsMobile() {
        return secBetweenInvestmentsMobile;
    }

    public void setSecBetweenInvestmentsMobile(int secBetweenInvestmentsMobile) {
        this.secBetweenInvestmentsMobile = secBetweenInvestmentsMobile;
    }

    public int getSecForDev2() {
        return secForDev2;
    }

    public void setSecForDev2(int secForDev2) {
        this.secForDev2 = secForDev2;
    }

	public int getSecForDev2Mobile() {
        return secForDev2Mobile;
    }

    public void setSecForDev2Mobile(int secForDev2Mobile) {
        this.secForDev2Mobile = secForDev2Mobile;
    }

    public BigDecimal getCurrentLevelAlertPerc() {
		return currentLevelAlertPerc;
	}

	public void setCurrentLevelAlertPerc(BigDecimal currentLevelAlertPerc) {
		this.currentLevelAlertPerc = currentLevelAlertPerc;
	}

	public BigDecimal getAcceptableDeviation() {
        return acceptableDeviation;
    }

    public void setAcceptableDeviation(BigDecimal acceptableDeviation) {
        this.acceptableDeviation = acceptableDeviation;
    }

    public boolean isHasFutureAgreement() {
		return hasFutureAgreement;
	}

	public void setHasFutureAgreement(boolean hasFutureAgreement) {
		this.hasFutureAgreement = hasFutureAgreement;
	}

	public boolean isNextAgreementSameDay() {
		return nextAgreementSameDay;
	}

	public void setNextAgreementSameDay(boolean nextAgreementSameDay) {
		this.nextAgreementSameDay = nextAgreementSameDay;
	}

	public int getBannersPriority() {
		return bannersPriority;
	}

	public void setBannersPriority(int bannersPriority) {
		this.bannersPriority = bannersPriority;
	}

	public String getTimeEstClosing() {
		return timeEstClosing;
	}

	public void setTimeEstClosing(String timeEstClosing) {
		this.timeEstClosing = timeEstClosing;
	}

	public String getTimeFirstInvest() {
		return timeFirstInvest;
	}

	public void setTimeFirstInvest(String timeFirstInvest) {
		this.timeFirstInvest = timeFirstInvest;
	}

	public String getTimeZone() {
		return timeZone;
	}

	public void setTimeZone(String timeZone) {
		this.timeZone = timeZone;
	}

	public Date getLastHourClosingTime() {
        return lastHourClosingTime;
    }

    public void setLastHourClosingTime(Date lastHourClosingTime) {
        this.lastHourClosingTime = lastHourClosingTime;
    }

    public boolean isOpened() {
        return opened;
    }

    public void setOpened(boolean opened) {
        this.opened = opened;
    }

    @Override
	public Market clone(){
		Market retVal = new Market();
		retVal.setId(this.id);
		retVal.setName(this.name);
        retVal.setDisplayName(this.displayName);
        retVal.setDisplayNameKey(this.displayNameKey);
        retVal.setFeedName(this.feedName);
		retVal.setTimeCreated(this.timeCreated);
		retVal.setWriterId(this.writerId);
		retVal.setDecimalPoint(this.decimalPoint);
		retVal.setGroupId (this.groupId);
		retVal.setInvestmentLimitsGroupId(investmentLimitsGroupId);
		retVal.setGroupName (groupName);
		retVal.setExchangeId (exchangeId);
		retVal.setCurrentLevelAlertPerc (currentLevelAlertPerc);
		retVal.setUpdatesPause (updatesPause);
		retVal.setSignificantPercentage (significantPercentage);
		retVal.setRealUpdatesPause (realUpdatesPause);
		retVal.setRealSignificantPercentage (realSignificantPercentage);
		retVal.setRandomCeiling (randomCeiling);
		retVal.setRandomFloor (randomFloor);
		retVal.setFirstShiftParameter (firstShiftParameter);
		retVal.setAverageInvestments (averageInvestments);
		retVal.setPercentageOfAverageInvestments (percentageOfAverageInvestments);
		retVal.setFixedAmountForShifting (fixedAmountForShifting);
		retVal.setAcceptableDeviation (acceptableDeviation);
        retVal.setAcceptableDeviation3 (acceptableDeviation3);
		retVal.setTypeOfShifting (typeOfShifting);
		retVal.setDisableAfterPeriod (disableAfterPeriod);
		retVal.setSuspended (suspended);
		retVal.setSuspendedMessage (suspendedMessage);
		retVal.setHasFutureAgreement (hasFutureAgreement);
		retVal.setNextAgreementSameDay (nextAgreementSameDay);
		retVal.setExchangeName (exchangeName);
		retVal.setInvestmentLimitsGroupName (investmentLimitsGroupName);
		retVal.setBannersPriority(bannersPriority);
		retVal.setTimeFirstInvest(timeFirstInvest);
		retVal.setTimeEstClosing(timeEstClosing);
		retVal.setTimeZone(timeZone);
		retVal.setHaveHourly(haveHourly);
		retVal.setHaveDaily (haveDaily);
		retVal.setHaveWeekly (haveWeekly);
		retVal.setHaveMonthly (haveMonthly);
        retVal.setSecBetweenInvestments(secBetweenInvestments);
        retVal.setSecForDev2(secForDev2);
        retVal.setAmountForDev2(amountForDev2);
        retVal.setAmountForDev2(amountForDev2Night);
        retVal.setAmountForDev3(amountForDev3);
        retVal.setMaxExposure(maxExposure);
        retVal.setLastHourClosingTime(lastHourClosingTime);
        retVal.setOpened(opened);
        retVal.setHasLongTerm(isHasLongTermOpp);
        retVal.setNoAutoSettleAfter(noAutoSettleAfter);
        retVal.setTradingDays(tradingDays);
        retVal.setDisplayGroupNameKey(displayGroupNameKey);
        retVal.setOptionPlusMarketId(optionPlusMarketId);
        retVal.setGroupMarketsCounter(groupMarketsCounter);
        retVal.setSecBetweenInvestmentsSameIp(secBetweenInvestmentsSameIp);
        retVal.setDecimalPointSubtractDigits(decimalPointSubtractDigits);
        retVal.setFullDay(isFullDay);
        retVal.setOptionPlus(isOptionPlus);
        retVal.setGroupPriority(groupPriority);
//        retVal.setZeroOneHundred(zeroOneHundred);
        retVal.setInstantSpread(instantSpread);
        retVal.setQuoteParams(quoteParams);
		return retVal;
	}
	public int getMaxExposure() {
		return maxExposure;
	}
	public void setMaxExposure(int max_exposure) {
		this.maxExposure = max_exposure;
	}

	public int getMax_exposure() {
		return maxExposure;
	}
	public void setMax_exposure(int max_exposure) {
		this.maxExposure = max_exposure;
	}
    /**
     * @return the insurancePremiaPercent
     */
    public double getInsurancePremiaPercent() {
        return insurancePremiaPercent;
    }

    /**
     * @param insurancePremiaPercent the insurancePremiaPercent to set
     */
    public void setInsurancePremiaPercent(double insurancePremiaPercent) {
        this.insurancePremiaPercent = insurancePremiaPercent;
    }

    /**
     * @return the insuranceQualifyPercent
     */
    public double getInsuranceQualifyPercent() {
        return insuranceQualifyPercent;
    }

    /**
     * @param insuranceQualifyPercent the insuranceQualifyPercent to set
     */
    public void setInsuranceQualifyPercent(double insuranceQualifyPercent) {
        this.insuranceQualifyPercent = insuranceQualifyPercent;
    }

    /**
     * @return the isGoldenMinutes
     */
    public boolean isGoldenMinutes() {
        return isGoldenMinutes;
    }

    /**
     * @param isGoldenMinutes the isGoldenMinutes to set
     */
    public void setGoldenMinutes(boolean isGoldenMinutes) {
        this.isGoldenMinutes = isGoldenMinutes;
    }

    /**
     * @return the isRollUp
     */
    public boolean isRollUp() {
        return isRollUp;
    }

    /**
     * @param isRollUp the isRollUp to set
     */
    public void setRollUp(boolean isRollUp) {
        this.isRollUp = isRollUp;
    }

    /**
     * @return the rollUpPremiaPercent
     */
    public double getRollUpPremiaPercent() {
        return rollUpPremiaPercent;
    }

    /**
     * @param rollUpPremiaPercent the rollUpPremiaPercent to set
     */
    public void setRollUpPremiaPercent(double rollUpPremiaPercent) {
        this.rollUpPremiaPercent = rollUpPremiaPercent;
    }

    /**
     * @return the rollUpQualifyPercent
     */
    public double getRollUpQualifyPercent() {
        return rollUpQualifyPercent;
    }

    /**
     * @param rollUpQualifyPercent the rollUpQualifyPercent to set
     */
    public void setRollUpQualifyPercent(double rollUpQualifyPercent) {
        this.rollUpQualifyPercent = rollUpQualifyPercent;
    }

    /**
     * @return the isHasLongTerm
     */
    public boolean isHasLongTermOpp() {
        return isHasLongTermOpp;
    }

    /**
     * @param isHasLongTerm the isHasLongTerm to set
     */
    public void setHasLongTerm(boolean isHasLongTermOpp) {
        this.isHasLongTermOpp = isHasLongTermOpp;
    }

    /**
     * @return the noAutoSettleAfter
     */
    public double getNoAutoSettleAfter() {
        return noAutoSettleAfter;
    }

    /**
     * @param noAutoSettleAfter the noAutoSettleAfter to set
     */
    public void setNoAutoSettleAfter(double noAutoSettleAfter) {
        this.noAutoSettleAfter = noAutoSettleAfter;
    }

	    /**
	     * if market time created is less then 3 months return true else false
	     * @return true if its new else false
	     */
	    public boolean isNewMarket() {
	        Calendar createTime = Calendar.getInstance();
	        createTime.setTime(timeCreated);
	        Calendar monthsAgo = createTime;
	        monthsAgo.set(Calendar.MONTH, createTime.get(Calendar.MONTH) + 3);
	        Calendar timeNow = Calendar.getInstance();
	        if (monthsAgo.after(timeNow)) {
	            return true;
	        }
	        return false;
	    }

	    /**
	     * @return the group Priority
	     */
	    public int getGroupPriority() {
	        return groupPriority;
	    }

	    /**
	     * @return the group Priority with 2 digits or more
	     */
	    public String getGroupPriority2Digit() {
	        if (groupPriority < 10) {
	            return "0" + groupPriority;
	        }
	        return String.valueOf(groupPriority);
	    }

	    /**
	     * @param groupPriority the group Priority to set
	     */
	    public void setGroupPriority(int groupPriority) {
	        this.groupPriority = groupPriority;
	    }
	    /**

    /**
     * @return the exposureReached
     */
    public long getExposureReached() {
        return exposureReached;
    }
    /**
     * @param exposureReached the exposureReached to set
     */
    public void setExposureReached(long exposureReached) {
        this.exposureReached = exposureReached;
    }
    /**
     * @return the raiseExposureUpAmount
     */
    public long getRaiseExposureUpAmount() {
        return raiseExposureUpAmount;
    }
    /**
     * @param raiseExposureUpAmount the raiseExposureUpAmount to set
     */
    public void setRaiseExposureUpAmount(long raiseExposureUpAmount) {
        this.raiseExposureUpAmount = raiseExposureUpAmount;
    }
    /**
     * @return the raiseExposureDownAmount
     */
    public long getRaiseExposureDownAmount() {
        return raiseExposureDownAmount;
    }
    /**
     * @param raiseExposureDownAmount the raiseExposureDownAmount to set
     */
    public void setRaiseExposureDownAmount(long raiseExposureDownAmount) {
        this.raiseExposureDownAmount = raiseExposureDownAmount;
    }
    /**
     * @return the upStrikeRaiseLowerPercent
     */
    public double getUpStrikeRaiseLowerPercent() {
        return upStrikeRaiseLowerPercent;
    }
    /**
     * @param upStrikeRaiseLowerPercent the upStrikeRaiseLowerPercent to set
     */
    public void setUpStrikeRaiseLowerPercent(double upStrikeRaiseLowerPercent) {
        this.upStrikeRaiseLowerPercent = upStrikeRaiseLowerPercent;
    }
    /**
     * @return the downStrikeRaiseLowerPercent
     */
    public double getDownStrikeRaiseLowerPercent() {
        return downStrikeRaiseLowerPercent;
    }
    /**
     * @param downStrikeRaiseLowerPercent the downStrikeRaiseLowerPercent to set
     */
    public void setDownStrikeRaiseLowerPercent(double downStrikeRaiseLowerPercent) {
        this.downStrikeRaiseLowerPercent = downStrikeRaiseLowerPercent;
    }

	/**
	 * @return the twentyFourSeven
	 */
//	public boolean isTwentyFourSeven() {
//		return twentyFourSeven;
//	}
	/**
	 * @param twentyFourSeven the twentyFourSeven to set
	 */
//	public void setTwentyFourSeven(boolean twentyFourSeven) {
//		this.twentyFourSeven = twentyFourSeven;
//	}

//	public BinaryZeroOneHundred getZeroOneHundred() {
//		return zeroOneHundred;
//	}
//	public void setZeroOneHundred(BinaryZeroOneHundred zeroOneHundred) {
//		this.zeroOneHundred = zeroOneHundred;
//	}

    /**
     * @return the isOptionPlus
     */
    public boolean isOptionPlus() {
        return isOptionPlus;
    }

    /**
     * @param isOptionPlus the isOptionPlus to set
     */
    public void setOptionPlus(boolean isOptionPlus) {
        this.isOptionPlus = isOptionPlus;
    }

	/**
	 * @return the isFullDay
	 */
	public boolean isFullDay() {
		return isFullDay;
	}
	/**
	 * @param isFullDay the isFullDay to set
	 */
	public void setFullDay(boolean isFullDay) {
		this.isFullDay = isFullDay;
	}

	/**
	 * @return the groupMarketsCounter
	 */
	public int getGroupMarketsCounter() {
		return groupMarketsCounter;
	}

	/**
	 * @param groupMarketsCounter the groupMarketsCounter to set
	 */
	public void setGroupMarketsCounter(int groupMarketsCounter) {
		this.groupMarketsCounter = groupMarketsCounter;
	}

	/**
	 * @return the optionPlusFee
	 */
	public long getOptionPlusFee() {
		return optionPlusFee;
	}

	/**
	 * @param optionPlusFee the optionPlusFee to set
	 */
	public void setOptionPlusFee(long optionPlusFee) {
		this.optionPlusFee = optionPlusFee;
	}

	/**
	 * @return the secBetweenInvestmentsSameIp
	 */
	public int getSecBetweenInvestmentsSameIp() {
		return secBetweenInvestmentsSameIp;
	}

	/**
	 * @param secBetweenInvestmentsSameIp the secBetweenInvestmentsSameIp to set
	 */
	public void setSecBetweenInvestmentsSameIp(int secBetweenInvestmentsSameIp) {
		this.secBetweenInvestmentsSameIp = secBetweenInvestmentsSameIp;
	}

    public int getSecBetweenInvestmentsSameIpMobile() {
        return secBetweenInvestmentsSameIpMobile;
    }

    public void setSecBetweenInvestmentsSameIpMobile(
            int secBetweenInvestmentsSameIpMobile) {
        this.secBetweenInvestmentsSameIpMobile = secBetweenInvestmentsSameIpMobile;
    }

    /**
	 * @return the tradingDays
	 */
	public String getTradingDays() {
		return tradingDays;
	}

	/**
	 * @param tradingDays the tradingDays to set
	 */
	public void setTradingDays(String tradingDays) {
		this.tradingDays = tradingDays;
	}

	/**
	 * @param isHasLongTermOpp the isHasLongTermOpp to set
	 */
	public void setHasLongTermOpp(boolean isHasLongTermOpp) {
		this.isHasLongTermOpp = isHasLongTermOpp;
	}

	public BigDecimal getAcceptableDeviation3() {
		return acceptableDeviation3;
	}

	public void setAcceptableDeviation3(BigDecimal acceptableDeviation3) {
		this.acceptableDeviation3 = acceptableDeviation3;
	}

	public float getAmountForDev2Night() {
		return amountForDev2Night;
	}

	public void setAmountForDev2Night(float amountForDev2Night) {
		this.amountForDev2Night = amountForDev2Night;
	}

	public float getAmountForDev3() {
		return amountForDev3;
	}

	public void setAmountForDev3(float amountForDev3) {
		this.amountForDev3 = amountForDev3;
	}

	@Override
	public String toString() {
        String ls = System.getProperty("line.separator");
        return ls + "Market" + ls
            + super.toString() + ls
            + "id: " + id + ls
            + "name: " + name + ls
            + "displayName: " + displayName + ls
            + "displayNameKey: " + displayNameKey + ls
            + "feedName: " + feedName + ls
            + "timeCreated: " + timeCreated + ls
            + "writerId: " + writerId + ls
            + "decimalPoint: " + decimalPoint + ls
            + "groupId: " + groupId + ls
            + "investmentLimitsGroupId: " + investmentLimitsGroupId + ls
            + "groupName: " + groupName + ls
            + "exchangeId: " + exchangeId + ls
            + "updatesPause: " + updatesPause + ls
            + "significantPercentage: " + significantPercentage + ls
            + "realUpdatesPause: " + realUpdatesPause + ls
            + "realSignificantPercentage: " + realSignificantPercentage + ls
            + "randomCeiling: " + randomCeiling + ls
            + "randomFloor: " + randomFloor + ls
            + "firstShiftParameter: " + firstShiftParameter + ls
            + "acceptableDeviation: " + acceptableDeviation + ls
            + "acceptableDeviation3: " + acceptableDeviation3 + ls
            + "averageInvestments: " + averageInvestments + ls
            + "percentageOfAverageInvestments: " + percentageOfAverageInvestments + ls
            + "fixedAmountForShifting: " + fixedAmountForShifting + ls
            + "typeOfShifting: " + typeOfShifting + ls
            + "disableAfterPeriod: " + disableAfterPeriod + ls
            + "suspended: " + suspended + ls
            + "suspendedMessage: " + suspendedMessage + ls
            + "exchangeName: " + exchangeName + ls
            + "investmentLimitsGroupName: " + investmentLimitsGroupName + ls
            + "bannersPriority: " + bannersPriority + ls
            + "secBetweenInvestments: " + secBetweenInvestments + ls
            + "secForDev2: " + secForDev2 + ls
            + "amountForDev2: " + amountForDev2 + ls
            + "lastHourClosingTime: " + lastHourClosingTime + ls
            + "opened: " + opened + ls
            + "isHasLongTermOpp: " + isHasLongTermOpp + ls
            + "isLiveFakeInvestments: " + isLiveFakeInvestments + ls
            + "noAutoSettleAfter: " + noAutoSettleAfter + ls
            + "worstCaseReturn: " + worstCaseReturn + ls
            + "isLimitedForHighAmount: " + isLimitedForHighAmounts + ls
            + "instantSpread: " + instantSpread + ls
            + "quoteParams: " + quoteParams + ls
            + "typeId: " + typeId + ls;
    }

	/**
	 * @return the isLiveFakeInvestments
	 */
	public boolean isLiveFakeInvestments() {
		return isLiveFakeInvestments;
	}

	/**
	 * @param isLiveFakeInvestments the isLiveFakeInvestments to set
	 */
	public void setLiveFakeInvestments(boolean isLiveFakeInvestments) {
		this.isLiveFakeInvestments = isLiveFakeInvestments;
	}

	public long getWorstCaseReturn() {
		return worstCaseReturn;
	}

	public void setWorstCaseReturn(long worstCaseReturn) {
		this.worstCaseReturn = worstCaseReturn;
	}

	public boolean isLimitedForHighAmounts() {
		return isLimitedForHighAmounts;
	}

	public void setLimitedForHighAmounts(boolean isLimitedForHighAmounts) {
		this.isLimitedForHighAmounts = isLimitedForHighAmounts;
	}

	public long getLimitedFromAmount() {
		return limitedFromAmount;
	}

	public void setLimitedFromAmount(long limitedFromAmount) {
		this.limitedFromAmount = limitedFromAmount;
	}

	public String getQuoteParams() {
		return quoteParams;
	}

	public void setQuoteParams(String quoteParams) {
		this.quoteParams = quoteParams;
	}

	public long getTypeId() {
		return typeId;
	}

	public void setTypeId(long typeId) {
		this.typeId = typeId;
	}
}