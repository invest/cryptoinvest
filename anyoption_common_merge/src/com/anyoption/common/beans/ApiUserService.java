package com.anyoption.common.beans;

import java.io.Serializable;

/**
 * @author eranl
 *
 */
public class ApiUserService implements Serializable {

	private static final long serialVersionUID = -1018129504110387572L;
	
	private int id;
	private String name;
	private String methodName;
	private String classParam;
	private boolean useHttps;
	private String userMethodName;
	private int jsonService;
	
	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * @return the methodName
	 */
	public String getMethodName() {
		return methodName;
	}
	/**
	 * @param methodName the methodName to set
	 */
	public void setMethodName(String methodName) {
		this.methodName = methodName;
	}
	/**
	 * @return the classParam
	 */
	public String getClassParam() {
		return classParam;
	}
	/**
	 * @param classParam the classParam to set
	 */
	public void setClassParam(String classParam) {
		this.classParam = classParam;
	}
	/**
	 * @return the useHttps
	 */
	public boolean isUseHttps() {
		return useHttps;
	}
	/**
	 * @param useHttps the useHttps to set
	 */
	public void setUseHttps(boolean useHttps) {
		this.useHttps = useHttps;
	}
	/**
	 * @return the userMethodName
	 */
	public String getUserMethodName() {
		return userMethodName;
	}
	/**
	 * @param userMethodName the userMethodName to set
	 */
	public void setUserMethodName(String userMethodName) {
		this.userMethodName = userMethodName;
	}
	/**
	 * @return the jsonService
	 */
	public int getJsonService() {
		return jsonService;
	}
	/**
	 * @param jsonService the jsonService to set
	 */
	public void setJsonService(int jsonService) {
		this.jsonService = jsonService;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "ApiUserService [id=" + id + ", name=" + name + ", methodName="
				+ methodName + ", classParam=" + classParam + ", useHttps="
				+ useHttps + ", userMethodName=" + userMethodName
				+ ", jsonService=" + jsonService + "]";
	}
	
}
