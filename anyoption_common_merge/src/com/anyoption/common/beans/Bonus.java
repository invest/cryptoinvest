package com.anyoption.common.beans;

import java.util.ArrayList;


/**
 * Bonus bean wrapper
 *
 * @author KobiM
 *
 */
public class Bonus extends com.anyoption.common.beans.base.Bonus {
	
	private static final long serialVersionUID = -3272562632014053542L;
	
	public static final long STATE_GRANTED = 1;
    public static final long STATE_ACTIVE = 2;
    public static final long STATE_USED = 3;
    public static final long STATE_DONE = 4;
    public static final long STATE_REFUSED = 5;
    public static final long STATE_MISSED = 6;
    public static final long STATE_CANCELED = 7;
	public static final long STATE_WITHDRAWN = 8;
	public static final long STATE_WAGERING_WAIVED = 9;
	public static final long STATE_PENDING = 10;

    public static final long CLASS_TYPE_FIXED = 1;
    public static final long CLASS_TYPE_PERCENT = 2;
    public static final long CLASS_TYPE_NEXT_INVEST_ON_US = 3;

    /**
     *  user shouldn't get new bonus if got one in this period.
     */
    public static final int GRANT_LIMIT_PERIOD = 30;

    public static final long TYPE_INSTANT_AMOUNT = 1;
    public static final long TYPE_INSTANT_NEXT_INVEST_ON_US = 2;
    public static final long TYPE_AMOUNT_AFTER_DEPOSIT = 3;
    public static final long TYPE_PERCENT_AFTER_DEPOSIT = 4;
    public static final long TYPE_AMOUNT_AFTER_WAGERING = 5;
    public static final long TYPE_NEXT_INVEST_ON_US_AFTER_WAGERING = 6;
    public static final long TYPE_NEXT_INVEST_ON_US_AFTER_X_INVS = 7;
    public static final long TYPE_CONVERT_POINTS_TO_CASH = 8;
    public static final long TYPE_INVESTMENT_INCREASED_RETURN_AND_REFUND = 9;
    public static final long TYPE_INVESTMENT_INCREASED_RETURN = 10;
    public static final long TYPE_INVESTMENT_INCREASED_REFUND = 11;
    public static final long TYPE_ROUNDUP = 13;
    
    public static final int ROUND_UP_TYPE_ROUND_UP = 1;
    public static final int ROUND_UP_TYPE_GET_STARTED = 2;

    public static final long NEXT_INVEST_REGISTRATION = 90;
    public static final long COPYOP_COINS_CONVERT = 405;

    private int roundUpType;
    
	protected static ArrayList<BonusStateItem> bonusStates;

    public static ArrayList<BonusStateItem> getBonusStates() {
    	if (null == bonusStates) {
    		bonusStates = new ArrayList<BonusStateItem>();
    		Bonus b = new Bonus();
    		bonusStates.add(b.new BonusStateItem(0, "general.all"));
    		bonusStates.add(b.new BonusStateItem(1, "bonus.state1"));
    		bonusStates.add(b.new BonusStateItem(2, "bonus.state2"));
    		bonusStates.add(b.new BonusStateItem(3, "bonus.state3"));
    		bonusStates.add(b.new BonusStateItem(4, "bonus.state1and2"));
    		bonusStates.add(b.new BonusStateItem(5, "bonus.state5"));
    		bonusStates.add(b.new BonusStateItem(6, "bonus.state6"));
    		bonusStates.add(b.new BonusStateItem(7, "bonus.state7"));
    	}
    	return bonusStates;
    }

    public class BonusStateItem {
        private long id;
        private String value;

        public BonusStateItem(long id, String value) {
        	this.id = id;
        	this.value = value;
        }

		/**
		 * @return the id
		 */
		public long getId() {
			return id;
		}

		/**
		 * @param id the id to set
		 */
		public void setId(long id) {
			this.id = id;
		}

		/**
		 * @return the value
		 */
		public String getValue() {
			return value;
		}

		/**
		 * @param value the value to set
		 */
		public void setValue(String value) {
			this.value = value;
		}

    }

	public int getRoundUpType() {
		return roundUpType;
	}

	public void setRoundUpType(int roundUpType) {
		this.roundUpType = roundUpType;
	}
}
