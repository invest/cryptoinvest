/**
 * 
 */
package com.anyoption.common.beans;

import java.io.Serializable;
import java.util.List;

import com.anyoption.common.beans.base.BalanceStepPredefValue;

/**
 * 
 * @author kirilim
 */
public class BalanceStep implements Serializable {

	private static final long serialVersionUID = 4523377593673263029L;

	/**
	 * The step id from the db
	 */
	private long realStepId;

	/**
	 * The currency id for the balance step
	 */
	private long currencyId;

	/**
	 * The balance step number for that currency
	 */
	private long id;

	/**
	 * The lower bound for the balance step
	 */
	private long avgBalanceMinRange;

	/**
	 * The upper bound for the balance step
	 */
	private long avgBalanceMaxRange;

	/**
	 * The balance step default amount value
	 */
	private long defaultAmountValue;

	/**
	 * The balance step predefined value suggestions
	 */
	private List<BalanceStepPredefValue> predefValues;

	/**
	 * @return the id
	 */
	public long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(long id) {
		this.id = id;
	}

	/**
	 * @return the avgBalanceRange
	 */
	public long getAvgBalanceMinRange() {
		return avgBalanceMinRange;
	}

	/**
	 * @param avgBalanceRange the avgBalanceRange to set
	 */
	public void setAvgBalanceMinRange(long avgBalanceMinRange) {
		this.avgBalanceMinRange = avgBalanceMinRange;
	}

	/**
	 * @return the defaultAmountValue
	 */
	public long getDefaultAmountValue() {
		return defaultAmountValue;
	}

	/**
	 * @param defaultAmountValue the defaultAmountValue to set
	 */
	public void setDefaultAmountValue(long defaultAmountValue) {
		this.defaultAmountValue = defaultAmountValue;
	}

	/**
	 * @return the predefValues
	 */
	public List<BalanceStepPredefValue> getPredefValues() {
		return predefValues;
	}

	/**
	 * @param predefValues the predefValues to set
	 */
	public void setPredefValues(List<BalanceStepPredefValue> predefValues) {
		this.predefValues = predefValues;
	}

	/**
	 * @return the avgBalanceMaxRange
	 */
	public long getAvgBalanceMaxRange() {
		return avgBalanceMaxRange;
	}

	/**
	 * @param avgBalanceMaxRange the avgBalanceMaxRange to set
	 */
	public void setAvgBalanceMaxRange(long avgBalanceMaxRange) {
		this.avgBalanceMaxRange = avgBalanceMaxRange;
	}

	/**
	 * @return the currencyId
	 */
	public long getCurrencyId() {
		return currencyId;
	}

	/**
	 * @param currencyId the currencyId to set
	 */
	public void setCurrencyId(long currencyId) {
		this.currencyId = currencyId;
	}

	/**
	 * @return the realStepId
	 */
	public long getRealStepId() {
		return realStepId;
	}

	/**
	 * @param realStepId the realStepId to set
	 */
	public void setRealStepId(long realStepId) {
		this.realStepId = realStepId;
	}
}