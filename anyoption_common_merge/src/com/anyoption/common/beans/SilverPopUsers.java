package com.anyoption.common.beans;

public class SilverPopUsers implements java.io.Serializable{

    private static final long serialVersionUID = -5312016309131285914L;
    private long userId;
    private long isContactByEmail;
    /**
     * @return the userId
     */
    public long getUserId() {
        return userId;
    }
    /**
     * @param userId the userId to set
     */
    public void setUserId(long userId) {
        this.userId = userId;
    }
    /**
     * @return the isContactByEmail
     */
    public long getIsContactByEmail() {
        return isContactByEmail;
    }
    /**
     * @param isContactByEmail the isContactByEmail to set
     */
    public void setIsContactByEmail(long isContactByEmail) {
        this.isContactByEmail = isContactByEmail;
    }

}
