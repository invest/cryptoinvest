package com.anyoption.common.beans;

public class UsersAwardBonus implements java.io.Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -4808873616666565346L;
	private long id;
	private long userId;
	private boolean isBonusGranted;
	private UsersAwardBonusGroup usersAwardBonusGroup;

	public UsersAwardBonus() {
		usersAwardBonusGroup = new UsersAwardBonusGroup();
	}

	public long getId() {
		return id;
	}
	
	public void setId(long id) {
		this.id = id;
	}
	
	public long getUserId() {
		return userId;
	}
	
	public void setUserId(long userId) {
		this.userId = userId;
	}

	public boolean isBonusGranted() {
		return isBonusGranted;
	}

	public void setBonusGranted(boolean isBonusGranted) {
		this.isBonusGranted = isBonusGranted;
	}

	public UsersAwardBonusGroup getUsersAwardBonusGroup() {
		return usersAwardBonusGroup;
	}

	public void setUsersAwardBonusGroup(UsersAwardBonusGroup usersAwardBonusGroup) {
		this.usersAwardBonusGroup = usersAwardBonusGroup;
	}
}
