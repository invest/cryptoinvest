package com.anyoption.common.beans;

/**
 * Define if a file type need addtional info
 * @author EranL
 */
public class FilesAdditionalInfo implements java.io.Serializable {

	private static final long serialVersionUID = 1L;

	private int fileType;
	private boolean idNumber;
	private boolean firstName;
	private boolean lastName;
	private boolean expDate;
	private boolean countryId;
	private boolean ccExpDate;
	private boolean dateOfBirth;

	public boolean isCountryId() {
		return countryId;
	}

	public void setCountryId(boolean countryId) {
		this.countryId = countryId;
	}

	public boolean isExpDate() {
		return expDate;
	}

	public void setExpDate(boolean expDate) {
		this.expDate = expDate;
	}

	public int getFileType() {
		return fileType;
	}

	public void setFileType(int fileType) {
		this.fileType = fileType;
	}

	public boolean isFirstName() {
		return firstName;
	}

	public void setFirstName(boolean firstName) {
		this.firstName = firstName;
	}

	public boolean isIdNumber() {
		return idNumber;
	}

	public void setIdNumber(boolean idNumber) {
		this.idNumber = idNumber;
	}

	public boolean isLastName() {
		return lastName;
	}

	public void setLastName(boolean lastName) {
		this.lastName = lastName;
	}

	public String toString() {
		String ls = System.getProperty("line.separator");
		return ls + "FilesAdditionalInfo: " + ls
				+ super.toString() + ls
				+ "fileType: " + fileType + ls
				+ "idNumber: " + idNumber + ls
				+ "firstName: " + firstName + ls
				+ "lastName: " + lastName + ls
				+ "expDate: " + expDate + ls
				+ "countryId: " + countryId + ls;
	}

	public boolean isCcExpDate() {
		return ccExpDate;
	}

	public void setCcExpDate(boolean ccExpDate) {
		this.ccExpDate = ccExpDate;
	}

	public boolean isDateOfBirth() {
		return dateOfBirth;
	}

	public void setDateOfBirth(boolean dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}
}