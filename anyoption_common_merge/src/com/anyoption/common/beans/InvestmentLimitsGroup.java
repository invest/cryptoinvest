package com.anyoption.common.beans;

import java.util.HashMap;

public class InvestmentLimitsGroup implements java.io.Serializable {

	private static final long serialVersionUID = -7019118875090490384L;

	public static long NO_MAX_LIMITS_ID = 3;
	public static final int HIGH_EXPOSURE_INVESTMENT_LIMIT = 17;

	private long id;
    private String groupName;
    private boolean isActive;
    private int level;
    private HashMap<Long, Long> amountPerCurrency;
    private String amountPerCurrencyDisplay;

    public InvestmentLimitsGroup() {
    	amountPerCurrency = new HashMap<Long, Long>();
    	amountPerCurrencyDisplay = "";
	}

	/**
	 * @return the amountPerCurrency
	 */
	public HashMap<Long, Long> getAmountPerCurrency() {
		return amountPerCurrency;
	}

	/**
	 * @param amountPerCurrency the amountPerCurrency to set
	 */
	public void setAmountPerCurrency(HashMap<Long, Long> amountPerCurrency) {
		this.amountPerCurrency = amountPerCurrency;
	}

	/**
	 * @return the level
	 */
	public int getLevel() {
		return level;
	}

	/**
	 * @param level the level to set
	 */
	public void setLevel(int level) {
		this.level = level;
	}

	/**
     * @return the id
     */
    public long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(long id) {
        this.id = id;
    }

    /**
     * @return the groupName
     */
    public String getGroupName() {
        return groupName;
    }

    /**
     * @param groupName the groupName to set
     */
    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

	/**
	 * @return the isActive
	 */
	public boolean isActive() {
		return isActive;
	}

	/**
	 * @param isActive the isActive to set
	 */
	public void setActive(boolean isActive) {
		this.isActive = isActive;
	}

	/**
	 * Constructs a <code>String</code> with all attributes
	 * in name = value format.
	 *
	 * @return a <code>String</code> representation
	 * of this object.
	 */
	public String toString()
	{
	    final String TAB = " \n ";

	    String retValue = "";

	    retValue = "InvestmentLimitsGroup ( "
	        + super.toString() + TAB
	        + "id = " + this.id + TAB
	        + "groupName = " + this.groupName + TAB
	        + "isActive = " + this.isActive + TAB
	        + " )";

	    return retValue;
	}



	public String getGroupNameDisplay() {
		if (NO_MAX_LIMITS_ID == id){
			return groupName;
		}else{
			return groupName + ": " + amountPerCurrencyDisplay;
		}
	}

	/**
	 * @return the amountPerCurrencyDisplay
	 */
	public String getAmountPerCurrencyDisplay() {
		return amountPerCurrencyDisplay;
	}

	/**
	 * @param amountPerCurrencyDisplay the amountPerCurrencyDisplay to set
	 */
	public void setAmountPerCurrencyDisplay(String amountPerCurrencyDisplay) {
		this.amountPerCurrencyDisplay = amountPerCurrencyDisplay;
	}

}