
package com.anyoption.common.beans;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Locale;
import java.util.NoSuchElementException;
import java.util.StringTokenizer;

import org.jfree.util.Log;

import com.anyoption.common.annotations.AnyoptionNoJSON;
import com.anyoption.common.beans.base.BaroPayRequest;
import com.anyoption.common.beans.base.BaroPayResponse;
import com.anyoption.common.beans.base.Currency;
import com.anyoption.common.beans.base.ThreeDParams;
import com.anyoption.common.bl_vos.ACHDepositBase;
import com.anyoption.common.bl_vos.CDPayDeposit;
import com.anyoption.common.bl_vos.ChargeBack;
import com.anyoption.common.bl_vos.Cheque;
import com.anyoption.common.bl_vos.UkashDeposit;
import com.anyoption.common.bl_vos.WebMoneyDeposit;
import com.anyoption.common.bl_vos.WireBase;
import com.anyoption.common.clearing.MoneybookersInfo;
import com.anyoption.common.managers.BaroPayManagerBase;
import com.anyoption.common.managers.CDPayManagerBase;
import com.anyoption.common.managers.ChargeBacksManagerBase;
import com.anyoption.common.managers.ChequesManagerBase;
import com.anyoption.common.managers.TransactionRequestResponseManager;
import com.anyoption.common.managers.TransactionsManagerBase;
import com.anyoption.common.util.CommonUtil;

public class Transaction extends com.anyoption.common.beans.base.Transaction implements Serializable {

	private static final long serialVersionUID = -503518307911032252L;
	@AnyoptionNoJSON
	protected long userId;
	@AnyoptionNoJSON
	protected String description;
	@AnyoptionNoJSON
	protected long writerId;
	@AnyoptionNoJSON
	protected String ip;
	@AnyoptionNoJSON
	protected String comments;
	@AnyoptionNoJSON
	protected long processedWriterId;
	@AnyoptionNoJSON
	protected BigDecimal chequeId;
	@AnyoptionNoJSON
	protected BigDecimal referenceId;
	@AnyoptionNoJSON
	protected BigDecimal wireId;
	@AnyoptionNoJSON
	protected BigDecimal chargeBackId;
	@AnyoptionNoJSON
	protected String XorIdAuthorize;
	@AnyoptionNoJSON
	protected String XorIdCapture;
	@AnyoptionNoJSON
	protected boolean feeCancel;
	@AnyoptionNoJSON
	protected boolean feeCancelByAdmin;
	@AnyoptionNoJSON
	protected boolean isAccountingApproved;
	@AnyoptionNoJSON
	protected long classType;
	@AnyoptionNoJSON
	protected boolean selected;
	@AnyoptionNoJSON
	protected String userName;
	@AnyoptionNoJSON
	protected Cheque cheque;
	@AnyoptionNoJSON
	protected CreditCard creditCard;
	@AnyoptionNoJSON
	protected WireBase wire;
	@AnyoptionNoJSON
	protected ChargeBack chargeBack;
	@AnyoptionNoJSON
	private String userFirstName;
	@AnyoptionNoJSON
	private String userLastName;
	@AnyoptionNoJSON
	protected Currency currency;
	@AnyoptionNoJSON
	protected long currencyId;
	@AnyoptionNoJSON
	protected long clearingProviderId;
	@AnyoptionNoJSON
	protected String skin;
	@AnyoptionNoJSON
	protected String userCountryCode;
	@AnyoptionNoJSON
    protected String clearingProviderName;
	@AnyoptionNoJSON
    protected boolean isCreditWithdrawal;
	@AnyoptionNoJSON
    protected long depositReferenceId;
	@AnyoptionNoJSON
    protected long splittedReferenceId;
	@AnyoptionNoJSON
    protected long creditAmount;
	@AnyoptionNoJSON
    protected boolean haveCredits;    // true: in case we have credit withdrawals to the same card(not Cft) of the depositTrx
    								  // (for cancle action)

    // Inatec banking depisit fields,  need to save it in the session. (for bank redirect handle)
	@AnyoptionNoJSON
    private String inatecRedirectSecret;
	@AnyoptionNoJSON
	private String inatecBankSortCode;
	@AnyoptionNoJSON
	private String inatecAccountNum;
	@AnyoptionNoJSON
	private String beneficiaryName;
	@AnyoptionNoJSON
    private long bonusUserId;
	@AnyoptionNoJSON
    private String countryName;
	@AnyoptionNoJSON
    private long countryId;
	@AnyoptionNoJSON
    private String campaignName;
	@AnyoptionNoJSON
    private String businessCaseName;
	@AnyoptionNoJSON
    private long baseAmount;
	@AnyoptionNoJSON
    protected String paymentTypeDesc;
	@AnyoptionNoJSON
    protected String paymentTypeName;
	@AnyoptionNoJSON
	protected String acquirerResponseId;
	@AnyoptionNoJSON
	protected long reroutingTranId;
	@AnyoptionNoJSON
	protected boolean isRerouted;
	@AnyoptionNoJSON
	private long selectorId;
	@AnyoptionNoJSON
	private String redirectUrl;
	@AnyoptionNoJSON
	protected Date lastWithdrawlSettled;
	@AnyoptionNoJSON
	protected ACHDepositBase ach;
	@AnyoptionNoJSON
    protected MoneybookersInfo moneybookers;
	@AnyoptionNoJSON
    protected WebMoneyDeposit webMoneyDeposit;
	@AnyoptionNoJSON
    protected BaroPayRequest baroPayRequest;
	@AnyoptionNoJSON
    protected BaroPayResponse baroPayResponse;
	@AnyoptionNoJSON
    protected CDPayDeposit cdpayDeposit;
	@AnyoptionNoJSON
    private long paymentTypeId;
	@AnyoptionNoJSON
    private String wireAmountAfterFee;
	@AnyoptionNoJSON
    private String payPalEmail;
	@AnyoptionNoJSON
    private String payPalWAmount;
	@AnyoptionNoJSON
    private String envoyAccountNum;
	@AnyoptionNoJSON
    private Date pixelRunTime;
	@AnyoptionNoJSON
    private String moneybookersEmail;
	@AnyoptionNoJSON
    private String webMoneyPurse;
	@AnyoptionNoJSON
    private String webMoneyId;
	@AnyoptionNoJSON
    private boolean updateByAdmin;
	@AnyoptionNoJSON
    protected Date depositsFrom;
	@AnyoptionNoJSON
    protected Date depositsTo;
	@AnyoptionNoJSON
    protected SpecialCare specialCare;
	@AnyoptionNoJSON
    protected Date timeScTurnover;
	@AnyoptionNoJSON
    protected Date timeScHouseResult;
	@AnyoptionNoJSON
    protected long manualRouted;
	@AnyoptionNoJSON
	private boolean isUserStopReverseWithdrawOption;
	@AnyoptionNoJSON
	private boolean isUserImmediateWithdraw;
	@AnyoptionNoJSON
	private boolean isRegulated;
	@AnyoptionNoJSON
	private int RewardUserTierId;
	@AnyoptionNoJSON
	protected boolean noAch;
	@AnyoptionNoJSON
    protected UkashDeposit ukash;
	@AnyoptionNoJSON
	private int totalLine;
	@AnyoptionNoJSON
	private TransactionPostponed transactionPostponed;
	@AnyoptionNoJSON
	private long loginId;
	@AnyoptionNoJSON
	private boolean isControl;
	@AnyoptionNoJSON
	private ThreeDParams threeDParams;
	@AnyoptionNoJSON
	private double rate;
	@AnyoptionNoJSON
	private TransactionRequestResponse transactionRequestResponse;
	@AnyoptionNoJSON
	private long withdrawalFeeExemptWriterId;
	@AnyoptionNoJSON
	private String writerUserName;
	@AnyoptionNoJSON
	private long depositOrWithdraw;
	@AnyoptionNoJSON
    private String epgPaymentDetails;
	@AnyoptionNoJSON
	private double amountInUsd;
	private boolean isAllowedApproveDepositAction;
	private boolean isAllowedCancelDepositAction;
	private boolean isAllowedCancelDepositFraudAction;
	private boolean isAllowedCancelDepositMistakeAction;
	private boolean isAllowedChargebackEditAction;
	private boolean isAllowedChargebackAddAction;
	private String writerWithdrawalApprove;
	private String transactionSplittedAs;
	private long withdrawalChangesId;
	private boolean isCftWithdrawal;
	private boolean isCrWithdrawInternal;
	private String qmTextAnswer;// text answer column qm_user_answers table
	private String qmaAnswerName;// answer_name column  qm_answers table
	private boolean isMaintenanceFeeCancel;
	private String statusName;
	private String clearingProviderDescriptor;
	@AnyoptionNoJSON
    private boolean is3D;
	
	public Transaction(int totalLine) {
        this.totalLine = totalLine;
    }
	
    public Transaction() {
		isCreditWithdrawal = false;
	}

	public boolean isShowFee() {
		if ((chequeId != null) && (typeId != TransactionsManagerBase.TRANS_TYPE_REVERSE_WITHDRAW)) {
			return true;
		}
		return false;
	}

	public boolean isSelected() {
		return selected;
	}

	public void setSelected(boolean selected) {
		this.selected = selected;
	}

	public long getClassType() {
		return classType;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

    public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public long getProcessedWriterId() {
		return processedWriterId;
	}

	public void setProcessedWriterId(long processedWriterId) {
		this.processedWriterId = processedWriterId;
	}

	public long getUserId() {
		return userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}

	public long getWriterId() {
		return writerId;
	}

	public void setWriterId(long writerId) {
		this.writerId = writerId;
	}

	public BigDecimal getChequeId() {
        return chequeId;
    }

    public void setChequeId(BigDecimal chequeId) {
        this.chequeId = chequeId;
    }

	public BigDecimal getReferenceId() {
        return referenceId;
    }

    public void setReferenceId(BigDecimal referenceId) {
        this.referenceId = referenceId;
    }

	public BigDecimal getWireId() {
        return wireId;
    }

    public void setWireId(BigDecimal wireId) {
        this.wireId = wireId;
    }

    public BigDecimal getChargeBackId() {
        return chargeBackId;
    }

    public void setChargeBackId(BigDecimal chargeBackId) {
        this.chargeBackId = chargeBackId;
    }

	public boolean isCcDeposit() {
		if (typeId==TransactionsManagerBase.TRANS_TYPE_CC_DEPOSIT)
			return true;

		return false;
	}
	
	public boolean isSkrillDeposit() {
		if (typeId==TransactionsManagerBase.TRANS_TYPE_MONEYBOOKERS_DEPOSIT) {
			return true;
		}
		return false;
	}

	public boolean isWireDeposit() {
		if (typeId==TransactionsManagerBase.TRANS_TYPE_WIRE_DEPOSIT)
			return true;

		return false;
	}
	
	public boolean isIdealInatecDeposit() {
		if (typeId == TransactionsManagerBase.TRANS_TYPE_INATEC_IFRAME_DEPOSIT){
			return true;
		}			
		return false;
	}
	
	public boolean isDirect24Deposit() {
		if (typeId == TransactionsManagerBase.TRANS_TYPE_DIRECT_BANK_DEPOSIT
				&& paymentTypeId == TransactionsManagerBase.PAYMENT_TYPE_DIRECT24){
			return true;
		}			
		return false;
	}

	public boolean isEpsDeposit() {
		if (typeId == TransactionsManagerBase.TRANS_TYPE_DIRECT_BANK_DEPOSIT
				&& paymentTypeId == TransactionsManagerBase.PAYMENT_TYPE_EPS){
			return true;
		}			
		return false;
	}
	
	public boolean isGiropayDeposit() {
		if (typeId == TransactionsManagerBase.TRANS_TYPE_DIRECT_BANK_DEPOSIT
				&& paymentTypeId == TransactionsManagerBase.PAYMENT_TYPE_GIROPAY){
			return true;
		}			
		return false;
	}
	
	public boolean isPayPalDepositAndNotChargeBack() {
		return (isPayPalDeposit()&&chargeBackId==null&&statusId==TransactionsManagerBase.TRANS_STATUS_SUCCEED);
	}

	public boolean isChargeBackEdit() {
		return ((isCcDeposit() || isSkrillDeposit()) && chargeBackId != null);
	}
	
	public boolean isChargeBackAdd() {
		return ((isCcDeposit() || isSkrillDeposit()) && chargeBackId == null && statusId == TransactionsManagerBase.TRANS_STATUS_SUCCEED);
	}

	public boolean isCcDepositAndNotChargeBack() {
		return (isCcDeposit() && chargeBackId == null && statusId == TransactionsManagerBase.TRANS_STATUS_SUCCEED);
	}

	public boolean isCcDepositAndPending() {
		return (isCcDeposit() && chargeBackId == null && statusId == TransactionsManagerBase.TRANS_STATUS_PENDING);
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getReceiptNumTxt() {
		if (receiptNum == null) {
			return "";
		}
		return "1-" + receiptNum;
	}

	public void setChargeBack(ChargeBack chargeBack) {
		this.chargeBack = chargeBack;
	}

	public void setCheque(Cheque cheque) {
		this.cheque = cheque;
	}

	public void setCreditCard(CreditCard creditCard) {
		this.creditCard = creditCard;
	}

	public void setWire(WireBase wire) {
		this.wire = wire;
	}
	
	public String getUserFirstName() {
		return userFirstName;
	}

	public void setUserFirstName(String userFirstName) {
		this.userFirstName = userFirstName;
	}

	public String getUserLastName() {
		return userLastName;
	}

	public void setUserLastName(String userLastName) {
		this.userLastName = userLastName;
	}

	public boolean isWeekOldTransaction() {
		int days = Integer.valueOf(CommonUtil.getConfig("check_long_withdraw_request_period"));
		GregorianCalendar gc = new GregorianCalendar();
		gc.add(GregorianCalendar.DAY_OF_MONTH, -days);
		if (timeCreated.before(gc.getTime())) {
			return true;
		}
		return false;
	}

	public boolean isOldTransaction() {
		int days = Integer.valueOf(CommonUtil.getConfig("check_withdraw_request_period"));
		GregorianCalendar gc=new GregorianCalendar();
		gc.add(GregorianCalendar.DAY_OF_MONTH, -days);
		if (timeCreated.before(gc.getTime())) {
			return true;
		}
		return false;
	}

	public String getXorIdAuthorize() {
		return XorIdAuthorize;
	}

	public void setXorIdAuthorize(String xorIdAuthorize) {
		XorIdAuthorize = xorIdAuthorize;
	}

	public String getXorIdCapture() {
		return XorIdCapture;
	}

	public void setXorIdCapture(String xorIdCapture) {
		XorIdCapture = xorIdCapture;
	}

	/**
	 * @return the feeCancel
	 */
	public boolean isFeeCancel() {
		return feeCancel;
	}

	/**
	 * @param feeCancel the feeCancel to set
	 */
	public void setFeeCancel(boolean feeCancel) {
		this.feeCancel = feeCancel;
	}

	/**
	 * @return the feeCancelByAdmin
	 */
	public boolean isFeeCancelByAdmin() {
		return feeCancelByAdmin;
	}

	/**
	 * @param feeCancelByAdmin the feeCancelByAdmin to set
	 */
	public void setFeeCancelByAdmin(boolean feeCancelByAdmin) {
		this.feeCancelByAdmin = feeCancelByAdmin;
	}

	/**
	 * @return the isAccountingApproved
	 */
	public boolean isAccountingApproved() {
		return isAccountingApproved;
	}

	/**
	 * @param isAccountingApproved the isAccountingApproved to set
	 */
	public void setAccountingApproved(boolean isAccountingApproved) {
		this.isAccountingApproved = isAccountingApproved;
	}

	public void setCurrencyId(long currencyId) {
		this.currencyId = currencyId;
	}

	public long getCurrencyId() {
		return currencyId;
	}

	public Currency getCurrency() {
		return currency;
	}

	public void setCurrency(Currency currency) {
		this.currency = currency;
		if(currency != null) {
			setCurrencyId(currency.getId());
		}
	}

    public long getClearingProviderId() {
        return clearingProviderId;
    }

    public void setClearingProviderId(long clearingProviderId) {
        this.clearingProviderId = clearingProviderId;
    }

	public String getSkin() {
		return skin;
	}

	public void setSkin(String skin) {
		this.skin = skin;
	}

	/**
	 * @return the userCountryCode
	 */
	public String getUserCountryCode() {
		return userCountryCode;
	}

	/**
	 * @param userCountryCode the userCountryCode to set
	 */
	public void setUserCountryCode(String userCountryCode) {
		this.userCountryCode = userCountryCode;
	}

	public String getClearingProviderName() {
		return clearingProviderName;
	}

	public void setClearingProviderName(String clearingProviderName) {
		this.clearingProviderName = clearingProviderName;
	}

	/**
	 * @return the isCreditWithdrawal
	 */
	public boolean isCreditWithdrawal() {
		return isCreditWithdrawal;
	}

	/**
	 * @param isCreditWithdrawal the isCreditWithdrawal to set
	 */
	public void setCreditWithdrawal(boolean isCreditWithdrawal) {
		this.isCreditWithdrawal = isCreditWithdrawal;
	}

	/**
	 * @return the creditAmount
	 */
	public long getCreditAmount() {
		return creditAmount;
	}

	/**
	 * @param creditAmount the creditAmount to set
	 */
	public void setCreditAmount(long creditAmount) {
		this.creditAmount = creditAmount;
	}

	/**
	 * @return the depositReferenceId
	 */
	public long getDepositReferenceId() {
		return depositReferenceId;
	}

	/**
	 * @param depositReferenceId the depositReferenceId to set
	 */
	public void setDepositReferenceId(long depositReferenceId) {
		this.depositReferenceId = depositReferenceId;
	}

	/**
	 * @return the splittedReferenceId
	 */
	public long getSplittedReferenceId() {
		return splittedReferenceId;
	}

	/**
	 * @param splittedReferenceId the splittedReferenceId to set
	 */
	public void setSplittedReferenceId(long splittedReferenceId) {
		this.splittedReferenceId = splittedReferenceId;
	}

	/**
	 * @return the haveCredits
	 */
	public boolean isHaveCredits() {
		return haveCredits;
	}

	/**
	 * @param haveCredits the haveCredits to set
	 */
	public void setHaveCredits(boolean haveCredits) {
		this.haveCredits = haveCredits;
	}

	/**
	 * Check if credit action available,
	 * true in case we have available amount to withdrawal from and not have pending withdrawals
	 * to the same credit card.
	 * @return
	 */
	public boolean isCreditAvailable() {
		if ( (amount-creditAmount > 0) && !haveCredits
				&& statusId == TransactionsManagerBase.TRANS_STATUS_SUCCEED) {
			return true;
		}
		return false;
	}

	/**
	 * Return available credit amount
	 * @return
	 */
	public long getAvailableCreditAmount() {
		return amount-creditAmount;
	}

	/**
	 * @return the inatecRedirectSecret
	 */
	public String getInatecRedirectSecret() {
		return inatecRedirectSecret;
	}

	/**
	 * @param inatecRedirectSecret the inatecRedirectSecret to set
	 */
	public void setInatecRedirectSecret(String inatecRedirectSecret) {
		this.inatecRedirectSecret = inatecRedirectSecret;
	}

	/**
	 * @return the inatecAccountNum
	 */
	public String getInatecAccountNum() {
		return inatecAccountNum;
	}

	/**
	 * @param inatecAccountNum the inatecAccountNum to set
	 */
	public void setInatecAccountNum(String inatecAccountNum) {
		this.inatecAccountNum = inatecAccountNum;
	}

	/**
	 * @return the inatecBankSortCode
	 */
	public String getInatecBankSortCode() {
		return inatecBankSortCode;
	}

	/**
	 * @param inatecBankSortCode the inatecBankSortCode to set
	 */
	public void setInatecBankSortCode(String inatecBankSortCode) {
		this.inatecBankSortCode = inatecBankSortCode;
	}


	/**
	 * @return the beneficiaryName
	 */
	public String getBeneficiaryName() {
		return beneficiaryName;
	}

	/**
	 * @param beneficiaryName the beneficiaryName to set
	 */
	public void setBeneficiaryName(String beneficiaryName) {
		this.beneficiaryName = beneficiaryName;
	}

	/**
	 * Check if it's inatec direct banking deposit with status in progress that allow
	 * to call Inatec status request for getting final status
	 * @return
	 */
	public boolean isInatecInProgress() {
		if (this.typeId == TransactionsManagerBase.TRANS_TYPE_DIRECT_BANK_DEPOSIT &&
				this.statusId == TransactionsManagerBase.TRANS_STATUS_IN_PROGRESS) {
					return true;
		}
		return false;
	}

	/**
	 * @return the bonusId
	 */
	public long getBonusUserId() {
		return bonusUserId;
	}

	/**
	 * @param bonusId the bonusId to set
	 */
	public void setBonusUserId(long bonusUserId) {
		this.bonusUserId = bonusUserId;
	}

	/**
	 * @return the countryId
	 */
	public long getCountryId() {
		return countryId;
	}

	/**
	 * @param countryId the countryId to set
	 */
	public void setCountryId(long countryId) {
		this.countryId = countryId;
	}

	/**
	 * @return the countryName
	 */
	public String getCountryName() {
		return countryName;
	}

	/**
	 * @param countryName the countryName to set
	 */
	public void setCountryName(String countryName) {
		this.countryName = countryName;
	}

    /**
     * @return the campaignName
     */
    public String getCampaignName() {
        return campaignName;
    }

    /**
     * @param campaignName the campaignName to set
     */
    public void setCampaignName(String campaignName) {
        this.campaignName = campaignName;
    }

    /**
     * @return the businessCaseName
     */
    public String getBusinessCaseName() {
        return businessCaseName;
    }

    /**
     * @param businessCaseName the businessCaseName to set
     */
    public void setBusinessCaseName(String businessCaseName) {
        this.businessCaseName = businessCaseName;
    }

    /**
     * @return the baseAmount
     */
    public long getBaseAmount() {
        return baseAmount;
    }

    /**
     * @param baseAmount the baseAmount to set
     */
    public void setBaseAmount(long baseAmount) {
        this.baseAmount = baseAmount;
    }

	/**
	 * set type name message
	 * in case transaction type is direct banking deposit: add payment type name to message
	 * @return
	 */
	public void setTypeNameTxt(Locale l) {
		typeName = CommonUtil.getMessage(l, typeName, null);
		if (typeId == TransactionsManagerBase.TRANS_TYPE_DIRECT_BANK_DEPOSIT &&
				null != paymentTypeDesc) {
			typeName = CommonUtil.getMessage(l, paymentTypeDesc, null);
			if (isInatecInProgress()) {
				typeName += " " + CommonUtil.getMessage(l, "direct.banking.deposit.pending", null);
			}
		} else if (typeId == TransactionsManagerBase.TRANS_TYPE_ENVOY_ONLINE_DEPOSIT ||
					typeId == TransactionsManagerBase.TRANS_TYPE_ENVOY_BANKING_DEPOSIT ||
					typeId == TransactionsManagerBase.TRANS_TYPE_ENVOY_WITHDRAW){

			typeName += " " + paymentTypeName;

			if (this.statusId == TransactionsManagerBase.TRANS_STATUS_IN_PROGRESS){
				typeName += " " + CommonUtil.getMessage(l, "direct.banking.deposit.pending", null);
			}
		}
	}

	/**
	 * @return the paymentTypeDesc
	 */
	public String getPaymentTypeDesc() {
		return paymentTypeDesc;
	}

	/**
	 * @param paymentTypeDesc the paymentTypeDesc to set
	 */
	public void setPaymentTypeDesc(String paymentTypeDesc) {
		this.paymentTypeDesc = paymentTypeDesc;
	}

	/**
	 * @return the paymentTypeName
	 */
	public String getPaymentTypeName() {
		return paymentTypeName;
	}

	/**
	 * @param paymentTypeName the paymentTypeName to set
	 */
	public void setPaymentTypeName(String paymentTypeName) {
		this.paymentTypeName = paymentTypeName;
	}

    public String getAcquirerResponseId() {
		return acquirerResponseId;
	}

	public void setAcquirerResponseId(String acquirerResponseId) {
		this.acquirerResponseId = acquirerResponseId;
	}

	public long getReroutingTranId() {
		return reroutingTranId;
	}

	public void setReroutingTranId(long reroutingTranId) {
		this.reroutingTranId = reroutingTranId;
	}

	public boolean isRerouted() {
		return isRerouted;
	}

	public void setIsRerouted(boolean isRerouted) {
		this.isRerouted = isRerouted;
	}

	public long getSelectorId() {
		return selectorId;
	}

	public void setSelectorId(long selectorId) {
		this.selectorId = selectorId;
	}

	public String getRedirectUrl() {
		return redirectUrl;
	}

	public void setRedirectUrl(String redirectUrl) {
		this.redirectUrl = redirectUrl;
	}

	public boolean isCashDeposit() {
		return (typeId==TransactionsManagerBase.TRANS_TYPE_CASH_DEPOSIT);
	}

	public boolean isCcDepositReroute() {
		if(isRerouted && reroutingTranId == id){
			return true;
		}
		return false;
	}
	
	public boolean isCcDepositRerouteFrom() {
		if(isRerouted && reroutingTranId != id){
			return true;
		}
		return false;
	}
	
	public boolean isMaintenanceFee() {
		return (typeId==TransactionsManagerBase.TRANS_TYPE_MAINTENANCE_FEE);
	}

    public boolean isEFTDeposit() {
        return (typeId==TransactionsManagerBase.TRANS_TYPE_EFT_DEPOSIT);
    }

	public boolean isPayPalDeposit() {
		return (typeId == TransactionsManagerBase.TRANS_TYPE_PAYPAL_DEPOSIT);
	}

	public boolean isAchDeposit() {
		return (typeId == TransactionsManagerBase.TRANS_TYPE_ACH_DEPOSIT);
	}

	public boolean isEnvoyBankingDeposit() {
		return (typeId == TransactionsManagerBase.TRANS_TYPE_ENVOY_BANKING_DEPOSIT);
	}

	public boolean isEnvoyOnlineDeposit() {
		return (typeId == TransactionsManagerBase.TRANS_TYPE_ENVOY_ONLINE_DEPOSIT);
	}

	public boolean isEnvoyWithdraw() {
		return (typeId == TransactionsManagerBase.TRANS_TYPE_ENVOY_WITHDRAW);
	}

    public boolean isChequeWithdraw() {
    	return (typeId == TransactionsManagerBase.TRANS_TYPE_CHEQUE_WITHDRAW);
    }
    
	public boolean isBaroPayWithdrawl() { 
		return (typeId == TransactionsManagerBase.TRANS_TYPE_BAROPAY_WITHDRAW);
	}

	public boolean isWireDepositAndPending() {
		return (isWireDeposit()&&statusId==TransactionsManagerBase.TRANS_STATUS_PENDING);
	}
	public boolean isCashDepositAndPending() {
		return (isCashDeposit()&&statusId==TransactionsManagerBase.TRANS_STATUS_PENDING);
	}
	public boolean isPaypalDepositAndPending() {
		return (isPayPalDeposit()&&statusId==TransactionsManagerBase.TRANS_STATUS_PENDING);
	}

	public String getReceiptNumWebDepositTxt() {
		if (receiptNum == null) {
			return "";
		}
		return receiptNum.toString();
	}

	/**
	 * @return if the fee was cancel by admin it should be cancel, else according
	 */
	public boolean getUpdateFeeByAdmin() {
		if (updateByAdmin == true) {
			return feeCancelByAdmin;
		} else {
			return feeCancel;
		}
	}

	/**
	 * @return the isCreditWithdrawalInternal
	 */
	public boolean isCreditWithdrawalInternal() {
		if (depositReferenceId > 0 && typeId == TransactionsManagerBase.TRANS_TYPE_INTERNAL_CREDIT) {
			setCrWithdrawInternal(true);
			return true;
		}
		return false;

	}
	
	/**
	 * @return ifCFTWithdrawlInternal
	 */
	public boolean isCFTWithdrawalInternal() {
		if (depositReferenceId == 0 && typeId == TransactionsManagerBase.TRANS_TYPE_INTERNAL_CREDIT) {
			setCftWithdrawal(true);
			return true;
		}
		return false;
	}

	/**
	 * @return the paymentTypeId
	 */
	public long getPaymentTypeId() {
		return paymentTypeId;
	}

	/**
	 * @param paymentTypeId the paymentTypeId to set
	 */
	public void setPaymentTypeId(long paymentTypeId) {
		this.paymentTypeId = paymentTypeId;
	}

	/**
	 * @return the wireAmountAfterFee
	 */
	public String getWireAmountAfterFee() {
		return wireAmountAfterFee;
	}

	/**
	 * @param wireAmountAfterFee the wireAmountAfterFee to set
	 */
	public void setWireAmountAfterFee(String wireAmountAfterFee) {
		this.wireAmountAfterFee = wireAmountAfterFee;
	}

	/**
	 * @return the payPalEmail
	 */
	public String getPayPalEmail() {
		return payPalEmail;
	}

	/**
	 * @param payPalEmail the payPalEmail to set
	 */
	public void setPayPalEmail(String payPalEmail) {
		this.payPalEmail = payPalEmail;
	}

	/**
	 * @return the payPalWAmount
	 */
	public String getPayPalWAmount() {
		return payPalWAmount;
	}

	/**
	 * @param payPalWAmount the payPalWAmount to set
	 */
	public void setPayPalWAmount(String payPalWAmount) {
		this.payPalWAmount = payPalWAmount;
	}

	/**
	 * @param webMoneyDeposit the webMoneyDeposit to set
	 */
	public void setWebMoneyDeposit(WebMoneyDeposit webMoneyDeposit) {
		this.webMoneyDeposit = webMoneyDeposit;
	}
	
	
	/**
	 * @return the baroPayResponse
	 * @throws SQLException 
	 */
	public BaroPayResponse getBaroPayResponse() throws SQLException {
		if (null == baroPayResponse) {
			baroPayResponse = BaroPayManagerBase.getBaroPayResponse(id); 
		}
		return baroPayResponse;
	}

	/**
	 * @param baroPayResponse the baroPayResponse to set
	 */
	public void setBaroPayResponse(BaroPayResponse baroPayResponse) {
		this.baroPayResponse = baroPayResponse;
	}
	
	/**
	 * @return the cdpayDeposit
	 * @throws SQLException 
	 */
	public CDPayDeposit getCDPayDeposit() throws SQLException {
		if (null == cdpayDeposit) {
			cdpayDeposit = CDPayManagerBase.getCDPayDeposit(id); 
		}
		return cdpayDeposit;
	}
	
	/**
	 * @return the baroPayRequest
	 * @throws SQLException 
	 */
	public BaroPayRequest getBaroPayRequest() throws SQLException {
		if (null == baroPayRequest) { 
			baroPayRequest = BaroPayManagerBase.getBaroPayRequest(id); 
		}
		return baroPayRequest;
	}

	/**
	 * @param baroPayRequest the baroPayRequest to set
	 */
	public void setBaroPayRequest(BaroPayRequest baroPayRequest) {
		this.baroPayRequest = baroPayRequest;
	}

	public boolean isDisplayCancelTranHint(){
		if (TransactionsManagerBase.TRANS_STATUS_CANCELED_ETRADER == statusId
				|| TransactionsManagerBase.TRANS_STATUS_CANCELED_FRAUDS == statusId
				|| TransactionsManagerBase.TRANS_STATUS_CANCEL_S_DEPOSIT == statusId
				|| TransactionsManagerBase.TRANS_STATUS_CANCEL_S_M == statusId
				|| TransactionsManagerBase.TRANS_STATUS_CANCEL_M_N_R == statusId
				|| TransactionsManagerBase.TRANS_STATUS_CHARGE_BACK == statusId){
			return true;
		}
		return false;
	}

    /**
	 * @return the envoyAccountNum
	 */
	public String getEnvoyAccountNum() {
		return envoyAccountNum;
	}

	/**
	 * @param envoyAccountNum the envoyAccountNum to set
	 */
	public void setEnvoyAccountNum(String envoyAccountNum) {
		this.envoyAccountNum = envoyAccountNum;
	}

	/**
	 * @return the pixelRunTime
	 */
	public Date getPixelRunTime() {
		return pixelRunTime;
	}

	/**
	 * @param pixelRunTime the pixelRunTime to set
	 */
	public void setPixelRunTime(Date pixelRunTime) {
		this.pixelRunTime = pixelRunTime;
	}

	/**
	 * @return the moneybookersEmail
	 */
	public String getMoneybookersEmail() {
		return moneybookersEmail;
	}

	/**
	 * @param moneybookersEmail the moneybookersEmail to set
	 */
	public void setMoneybookersEmail(String moneybookersEmail) {
		this.moneybookersEmail = moneybookersEmail;
	}

	/**
	 * @return the webMoneyPurse
	 */
	public String getWebMoneyPurse() {
		return webMoneyPurse;
	}

	/**
	 * @param webMoneyPurse the webMoneyPurse to set
	 */
	public void setWebMoneyPurse(String webMoneyPurse) {
		this.webMoneyPurse = webMoneyPurse;
	}

	/**
	 * @return the webMoneyId
	 */
	public String getWebMoneyId() {
		return webMoneyId;
	}

	/**
	 * @param webMoneyId the webMoneyId to set
	 */
	public void setWebMoneyId(String webMoneyId) {
		this.webMoneyId = webMoneyId;
	}

    /**
	 * @return the moneybookers
	 */
	public MoneybookersInfo getMoneybookers() {
		return moneybookers;
	}

	/**
	 * @param moneybookers the moneybookers to set
	 */
	public void setMoneybookers(MoneybookersInfo moneybookers) {
		this.moneybookers = moneybookers;
	}

	/**
	 * Get Moneybookers transaction details from comments field
	 * @return
	 */
	public MoneybookersInfo getMoneybookersDetails(){
		if (null == moneybookers) {
			 HashMap<String, String> nvp = new HashMap<String, String>();
			 String s = this.getComments();
			 if (!CommonUtil.isParameterEmptyOrNull(s)){
			     StringTokenizer stTok = new StringTokenizer( s, "|");
			     while (stTok.hasMoreTokens())  {
			    	  String stTokNext = stTok.nextToken();
			    	  if (stTokNext.contains("=")){
			    		  try{
			    		  	StringTokenizer stInternalTokenizer = new StringTokenizer(stTokNext, "=");
			            	String key = stInternalTokenizer.nextToken();
			            	String value = stInternalTokenizer.nextToken().toLowerCase();
			            	nvp.put(key.toUpperCase(), value);
			    		  } catch (NoSuchElementException e) { //for an empty value
						}
			    	  }
			      }
			      moneybookers = new MoneybookersInfo();
			      moneybookers.setStatus(nvp.get("STATUS"));
			      moneybookers.setFailed_reason_code(nvp.get("FAILEDREASONCODE"));
			      moneybookers.setPayment_type(nvp.get("PAYMENTTYPE"));
			      moneybookers.setCustomer_id(nvp.get("CUSTOMERID"));
			      moneybookers.setMb_amount(nvp.get("MBAMOUNT"));
			 }
		}
		return moneybookers;
    }

	public Date getDepositsTo() {
		return depositsTo;
	}

	public void setDepositsTo(Date depositsTo) {
		this.depositsTo = depositsTo;
	}

	public Date getDepositsFrom() {
		return depositsFrom;
	}

	public void setDepositsFrom(Date depositsFrom) {
		this.depositsFrom = depositsFrom;
	}

    /**
     * @return the specialCare
     */
    public SpecialCare getSpecialCare() {
        return specialCare;
    }

    /**
     * @param specialCare the specialCare to set
     */
    public void setSpecialCare(SpecialCare specialCare) {
        this.specialCare = specialCare;
    }

	public boolean isUpdateByAdmin() {
		return updateByAdmin;
	}

	public void setUpdateByAdmin(boolean updateByAdmin) {
		this.updateByAdmin = updateByAdmin;
	}

	public Date getLastWithdrawlSettled() {
		return lastWithdrawlSettled;
	}

	public void setLastWithdrawlSettled(Date lastWithdrawlSettled) {
		this.lastWithdrawlSettled = lastWithdrawlSettled;
	}
	
	public long getManualRouted() {
		return manualRouted;
	}

	public void setManualRouted(long manualRouted) {
		this.manualRouted = manualRouted;
	}

	/**
	 * @return the isUserStopReverseWithdrawOption
	 */
	public boolean isUserStopReverseWithdrawOption() {
		return isUserStopReverseWithdrawOption;
	}

	/**
	 * @param isUserStopReverseWithdrawOption the isUserStopReverseWithdrawOption to set
	 */
	public void setUserStopReverseWithdrawOption(
			boolean isUserStopReverseWithdrawOption) {
		this.isUserStopReverseWithdrawOption = isUserStopReverseWithdrawOption;
	}

	/**
	 * @return the isUserImmediateWithdraw
	 */
	public boolean isUserImmediateWithdraw() {
		return isUserImmediateWithdraw;
	}

	/**
	 * @param isUserImmediateWithdraw the isUserImmediateWithdraw to set
	 */
	public void setUserImmediateWithdraw(boolean isUserImmediateWithdraw) {
		this.isUserImmediateWithdraw = isUserImmediateWithdraw;
	}

	public boolean isRegulated() {
		return isRegulated;
	}

	public void setRegulated(boolean isRegulated) {
		this.isRegulated = isRegulated;
	}
		 

	/**
	 * @return the rewardUserTierId
	 */
	public int getRewardUserTierId() {
		return RewardUserTierId;
	}

	/**
	 * @param rewardUserTierId the rewardUserTierId to set
	 */
	public void setRewardUserTierId(int rewardUserTierId) {
		RewardUserTierId = rewardUserTierId;
	}
	
	public long getHowTransactionSplitted() throws SQLException{
		ArrayList<Transaction> list = TransactionsManagerBase.getRelatedSplittedTran(this);
		boolean credit = false;
		boolean cft = false;
		for(Transaction t : list ){
			if(t.isCreditWithdrawalInternal()){
				credit = true;
			}
			if(t.isCFTWithdrawalInternal()){
				cft = true;
			}
		}
		if(credit && !cft){
			return 1;
		} else if (!credit && cft ){
			return 2;
		} else if(credit && cft){
			return 3;
		}
		return 0;
	}
	
	public CreditCard getCreditCard() throws SQLException{
		if (creditCard==null && creditCardId != null) {
			creditCard=TransactionsManagerBase.getCreditCard(creditCardId);
		}
		return creditCard;
	}
	
//	public ACHDepositBase getAch() throws SQLException{
//		if (null == ach && !noAch) {
//			ach = TransactionsManagerBase.getAch(id);
//			if (null == ach) {
//				noAch = true;
//			}
//		}
//		return ach;
//	}
	
	/**
	 * @param ukash the ukash to set
	 */
	public void setUkash(UkashDeposit ukash) {
		this.ukash = ukash;
	}
	
	public WireBase getWire() throws SQLException{
		if (wire==null && wireId != null) {
			wire=TransactionsManagerBase.getWire(wireId);
		}
		return wire;
	}
	
	public void setClassType(long ct) {
		this.classType = ct;

		if (this.classType == TransactionsManagerBase.TRANS_CLASS_DEPOSIT ||
				(this.classType == TransactionsManagerBase.TRANS_CLASS_INTERNALS && this.typeId == TransactionsManagerBase.TRANS_TYPE_CC_DEPOSIT_REROUTE) || 
				this.classType == TransactionsManagerBase.TRANS_CLASS_ADMIN_DEPOSITS ||
				this.classType == TransactionsManagerBase.TRANS_CLASS_BONUS_DEPOSITS ||
				this.classType == TransactionsManagerBase.TRANS_CLASS_REVERSE_WITHDRAWALS || 
				(this.classType == TransactionsManagerBase.TRANS_CLASS_INTERNALS && this.typeId == TransactionsManagerBase.TRANS_TYPE_MAINTENANCE_FEE_CANCEL) ) {

			credit = true;
		} else {
			credit = false;
		}
	}

	public boolean isUserDeposit() {
		if (typeId == TransactionsManagerBase.TRANS_TYPE_CC_DEPOSIT ||
				typeId == TransactionsManagerBase.TRANS_TYPE_WIRE_DEPOSIT ||
                typeId == TransactionsManagerBase.TRANS_TYPE_EFT_DEPOSIT ||
				typeId == TransactionsManagerBase.TRANS_TYPE_CASH_DEPOSIT) {
			return true;
		}

		return false;
	}
	/**
	 * @return the ukash
	 */
	public UkashDeposit getUkash() {
		return ukash;
	}
	
	/**
	 * @return the webMoneyDeposit
	 * @throws SQLException
	 */
	public WebMoneyDeposit getWebMoneyDeposit() throws SQLException {
		if (null == webMoneyDeposit) {
			webMoneyDeposit = TransactionsManagerBase.getWebMoneyDeposit(id);
		}
		return webMoneyDeposit;
	}
	
	/**
	 * @return
	 * @throws SQLException
	 */
	public String getEPGPaymentDetails() throws SQLException {
		if (CommonUtil.isParameterEmptyOrNull(epgPaymentDetails)) {
			epgPaymentDetails = TransactionsManagerBase.getEPGPaymentDetails(id);
		}
		return epgPaymentDetails;
	}
	
	public String getUpdateDepositPixelRunTime() throws SQLException {
		if (id != 0) {
			TransactionsManagerBase.updatePixelRunTime(id);
		}
		return null;
	}
	
	public boolean isCcOrInternalWithdrawal() {
		if (typeId == TransactionsManagerBase.TRANS_TYPE_CC_WITHDRAW ||
				typeId == TransactionsManagerBase.TRANS_TYPE_INTERNAL_CREDIT
				|| typeId == TransactionsManagerBase.TRANS_TYPE_CUP_INTERNAL_CREDIT
				) {
			return true;
		}
		return false;
	}
    /**
     * @return the totalLine
     */
    public int getTotalLine() {
        return totalLine;
    }

    /**
     * @param totalLine the totalLine to set
     */
    public void setTotalLine(int totalLine) {
        this.totalLine = totalLine;
    }
    
	public boolean isTransactionCanceled(){
		try {
			if(TransactionsManagerBase.isMaintenanceFeeCanceled(id)){
				return false;	
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return true;
	}
	
	public UkashDeposit getUkashDeposit() throws SQLException{
		if (ukash == null) {
			setUkash(TransactionsManagerBase.getUkash(id));
		}
		return ukash;
	}
	
	/**
	 * @return
	 * @throws Exception
	 */
	public ArrayList<Long> getTransactionsReroutConnectedIDById() throws Exception {
    	return TransactionsManagerBase.getTransactionsReroutConnectedIDById(id);
    }
	public Cheque getCheque() throws SQLException {
		if (cheque == null && chequeId != null) {
			cheque = ChequesManagerBase.getById(chequeId.longValue());
		}
		return cheque;
	}


	public ChargeBack getChargeBack() throws SQLException {
		if (chargeBack == null && chargeBackId != null) {
			chargeBack = ChargeBacksManagerBase.getById(chargeBackId.longValue());
		}
		return chargeBack;
	}

	public TransactionPostponed getTransactionPostponed() {
		return transactionPostponed;
	}

	public void setTransactionPostponed(TransactionPostponed transactionPostponed) {
		this.transactionPostponed = transactionPostponed;
	}

	public long getLoginId() {
		return loginId;
	}

	public void setLoginId(long loginId) {
		this.loginId = loginId;
	}
	
	public boolean isControl() {
		return isControl;
	}

	public void setControl(boolean isControl) {
		this.isControl = isControl;
	}
	
	public boolean isEpgProvider() {
		return CommonUtil.isEpgProvider(clearingProviderId);
	}

	public double getRate() {
		return rate;
	}

	public void setRate(double rate) {
		this.rate = rate;
	}

	public ThreeDParams getThreeDParams() {
		return threeDParams;
	}

	public void setThreeDParams(ThreeDParams threeDParams) {
		this.threeDParams = threeDParams;
	}
	
	public TransactionRequestResponse getTranReqRespDetails() {
		if (null == transactionRequestResponse) {
			try {
				return transactionRequestResponse = TransactionRequestResponseManager.getTranId(id);
			} catch (SQLException e) {
				Log.debug("can't load transactionRequestResponse for tran id " + id, e);
			}
		}
		return transactionRequestResponse;
	}

	public TransactionRequestResponse getTransactionRequestResponse() {
		return transactionRequestResponse;
	}

	public void setTransactionRequestResponse(
			TransactionRequestResponse transactionRequestResponse) {
		this.transactionRequestResponse = transactionRequestResponse;
	}

	public long getWithdrawalFeeExemptWriterId() {
		return withdrawalFeeExemptWriterId;
	}

	public void setWithdrawalFeeExemptWriterId(long withdrawalFeeExemptWriterId) {
		this.withdrawalFeeExemptWriterId = withdrawalFeeExemptWriterId;
	}
	
	public boolean isWithdrawal() {
		if (typeId == TransactionsManagerBase.TRANS_TYPE_CC_WITHDRAW ||	typeId == TransactionsManagerBase.TRANS_TYPE_BONUS_WITHDRAW
				|| typeId == TransactionsManagerBase.TRANS_TYPE_CHEQUE_WITHDRAW || typeId == TransactionsManagerBase.TRANS_TYPE_ADMIN_WITHDRAW 
				|| typeId == TransactionsManagerBase.TRANS_TYPE_PAYPAL_WITHDRAW || typeId == TransactionsManagerBase.TRANS_TYPE_ENVOY_WITHDRAW 
				|| typeId == TransactionsManagerBase.TRANS_TYPE_FIX_BALANCE_WITHDRAW || typeId == TransactionsManagerBase.TRANS_TYPE_WEBMONEY_WITHDRAW 
				|| typeId == TransactionsManagerBase.TRANS_TYPE_EFT_WITHDRAW || typeId == TransactionsManagerBase.TRANS_TYPE_MONEYBOOKERS_WITHDRAW 
				|| typeId == TransactionsManagerBase.TRANS_TYPE_BAROPAY_WITHDRAW || typeId == TransactionsManagerBase.TRANS_TYPE_CUP_CHINA_WITHDRAW 
				|| typeId == TransactionsManagerBase.TRANS_TYPE_FOCAL_PAYMENTS_WITHDRAW || typeId == TransactionsManagerBase.TRANS_TYPE_BANK_WIRE_WITHDRAW
				|| typeId == TransactionsManagerBase.TRANS_TYPE_DIRECT24_WITHDRAW
				|| typeId ==TransactionsManagerBase.TRANS_TYPE_GIROPAY_WITHDRAW
				|| typeId ==TransactionsManagerBase.TRANS_TYPE_EPS_WITHDRAW
				) {
			return true;
		}
		return false;
	}

	public String getWriterUserName() {
		return writerUserName;
	}

	public void setWriterUserName(String writerUserName) {
		this.writerUserName = writerUserName;
	}

	public long getDepositOrWithdraw() {
		return depositOrWithdraw;
	}

	public void setDepositOrWithdraw(long depositOrWithdraw) {
		this.depositOrWithdraw = depositOrWithdraw;
	}

	public double getAmountInUsd() {
		return amountInUsd;
	}

	public void setAmountInUsd(double amountInUsd) {
		this.amountInUsd = amountInUsd;
	}

	public boolean isAllowedApproveDepositAction() {
		return isAllowedApproveDepositAction;
	}

	public void setAllowedApproveDepositAction(boolean isAllowedApproveDepositAction) {
		this.isAllowedApproveDepositAction = isAllowedApproveDepositAction;
	}

	public boolean isAllowedCancelDepositAction() {
		return isAllowedCancelDepositAction;
	}

	public void setAllowedCancelDepositAction(boolean isAllowedCancelDepositAction) {
		this.isAllowedCancelDepositAction = isAllowedCancelDepositAction;
	}

	public boolean isAllowedCancelDepositFraudAction() {
		return isAllowedCancelDepositFraudAction;
	}

	public void setAllowedCancelDepositFraudAction(boolean isAllowedCancelDepositFraudAction) {
		this.isAllowedCancelDepositFraudAction = isAllowedCancelDepositFraudAction;
	}

	public boolean isAllowedCancelDepositMistakeAction() {
		return isAllowedCancelDepositMistakeAction;
	}

	public void setAllowedCancelDepositMistakeAction(boolean isAllowedCancelDepositMistakeAction) {
		this.isAllowedCancelDepositMistakeAction = isAllowedCancelDepositMistakeAction;
	}

	public boolean isAllowedChargebackEditAction() {
		return isAllowedChargebackEditAction;
	}

	public void setAllowedChargebackEditAction(boolean isAllowedChargebackEditAction) {
		this.isAllowedChargebackEditAction = isAllowedChargebackEditAction;
	}

	public boolean isAllowedChargebackAddAction() {
		return isAllowedChargebackAddAction;
	}

	public void setAllowedChargebackAddAction(boolean isAllowedChargebackAddAction) {
		this.isAllowedChargebackAddAction = isAllowedChargebackAddAction;
	}

	public String getWriterWithdrawalApprove() {
		return writerWithdrawalApprove;
	}

	public void setWriterWithdrawalApprove(String writerWithdrawalApprove) {
		this.writerWithdrawalApprove = writerWithdrawalApprove;
	}

	public String getTransactionSplittedAs() {
		return transactionSplittedAs;
	}

	public void setTransactionSplittedAs(String transactionSplittedAs) {
		this.transactionSplittedAs = transactionSplittedAs;
	}

	public long getWithdrawalChangesId() {
		return withdrawalChangesId;
	}

	public void setWithdrawalChangesId(long withdrawalChangesId) {
		this.withdrawalChangesId = withdrawalChangesId;
	}

	public boolean isCftWithdrawal() {
		return isCftWithdrawal;
	}

	public void setCftWithdrawal(boolean isCftWithdrawal) {
		this.isCftWithdrawal = isCftWithdrawal;
	}

	public boolean isCrWithdrawInternal() {
		return isCrWithdrawInternal;
	}

	public void setCrWithdrawInternal(boolean isCrWithdrawInternal) {
		this.isCrWithdrawInternal = isCrWithdrawInternal;
	}

	public String getQmTextAnswer() {
		return qmTextAnswer;
	}

	public void setQmTextAnswer(String qmTextAnswer) {
		this.qmTextAnswer = qmTextAnswer;
	}

	public String getQmaAnswerName() {
		return qmaAnswerName;
	}

	public void setQmaAnswerName(String qmaAnswerName) {
		this.qmaAnswerName = qmaAnswerName;
	}
	
	public boolean isMaintenanceFeeCancel() {
		return isMaintenanceFeeCancel;
	}

	public void setMaintenanceFeeCancel(boolean isMaintenanceFeeCancel) {
		this.isMaintenanceFeeCancel = isMaintenanceFeeCancel;
	}
	
	public String getStatusName() {
		return statusName;
	}

	public void setStatusName(String statusName) {
		this.statusName = statusName;
	}
	
	public String getClearingProviderDescriptor() {
		return clearingProviderDescriptor;
	}

	public void setClearingProviderDescriptor(String clearingProviderDescriptor) {
		this.clearingProviderDescriptor = clearingProviderDescriptor;
	}
	
	public boolean isIs3D() {
		return is3D;
	}

	public void setIs3D(boolean is3d) {
		is3D = is3d;
	}

	public String toString() {
        String ls = System.getProperty("line.separator");
        return ls + "Transaction" + ls
            + super.toString() + ls
            + "id: " + id + ls
            + "userId: " + userId + ls
            + "creditCardId: " + creditCardId + ls
            + "cc4digit: " + cc4digit + ls
            + "timeCreated: " + timeCreated + ls
            + "amount: " + amount + ls
            + "writerId: " + writerId + ls
            + "chequeId: " + chequeId + ls
            + "wireId: " + wireId + ls
            + "feeCancel: " + feeCancel + ls
            + "feeCancelByAdmin: " + feeCancelByAdmin + ls
            + "isAccountingApproved: " + isAccountingApproved + ls
            + "userFirstName: " + userFirstName + ls
            + "userLastName: " + userLastName + ls
            + "userFirstName: " + userFirstName + ls
            + "skin: " + skin + ls
            + "clearingProviderName: " + clearingProviderName + ls
            + "statusId: " + statusId + ls
            + "isAllowedApproveDepositAction: " + isAllowedApproveDepositAction + ls
            + "isAllowedCancelDepositAction: " + isAllowedCancelDepositAction + ls
            + "isAllowedCancelDepositFraudAction: " + isAllowedCancelDepositFraudAction + ls
            + "isAllowedCancelDepositMistakeAction: " + isAllowedCancelDepositMistakeAction + ls
            + "isAllowedChargebackAction: " + isAllowedChargebackEditAction + ls
            + "isAllowedInitChargebackAction: " + isAllowedChargebackAddAction + ls
            + "isCftWithdrawal: " + isCftWithdrawal + ls
            + "isCrWithdrawInternal: " + isCrWithdrawInternal + ls
            + "qmTextAnswer: " + qmTextAnswer + ls
            + "qmaAnswerName: " + qmaAnswerName + ls
            + "isMaintenanceFeeCancel: " + isMaintenanceFeeCancel + ls;
    }
}
