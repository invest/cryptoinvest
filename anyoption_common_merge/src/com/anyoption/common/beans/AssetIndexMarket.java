package com.anyoption.common.beans;

import com.anyoption.common.beans.base.AssetIndexInfo;
public class AssetIndexMarket implements java.io.Serializable {

	private static final long serialVersionUID = -3531713893236096649L;
	
	private Market market;
	private AssetIndexInfo assetIndexInfo;
	private long assetIndexGroupType;
	private String assetIndexGroupTypeName;
	private long groupType;

	public Market getMarket() {
		return market;
	}

	public void setMarket(Market market) {
		this.market = market;
	}

	public AssetIndexInfo getAssetIndexInfo() {
		return assetIndexInfo;
	}

	public void setAssetIndexInfo(AssetIndexInfo assetIndexInfo) {
		this.assetIndexInfo = assetIndexInfo;
	}

	public long getAssetIndexGroupType() {
		return assetIndexGroupType;
	}

	public void setAssetIndexGroupType(long assetIndexGroupType) {
		this.assetIndexGroupType = assetIndexGroupType;
	}

	public String getAssetIndexGroupTypeName() {
		return assetIndexGroupTypeName;
	}

	public void setAssetIndexGroupTypeName(String assetIndexGroupTypeName) {
		this.assetIndexGroupTypeName = assetIndexGroupTypeName;
	}

	public long getGroupType() {
		return groupType;
	}

	public void setGroupType(long groupType) {
		this.groupType = groupType;
	}   
}
