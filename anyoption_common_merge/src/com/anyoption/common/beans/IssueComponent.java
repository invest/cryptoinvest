package com.anyoption.common.beans;


/**
 * @author eranl
 *
 */
public class IssueComponent implements java.io.Serializable{

	private static final long serialVersionUID = -6141768983962634775L;
	private long id;
	private String name;
	
	public static final int ISSUE_COMPONENT_ET_AO = 1;
	public static final int ISSUE_COMPONENT_COPYOP = 2;
	
	/**
	 * @return the id
	 */
	public long getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(long id) {
		this.id = id;
	}
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	
}