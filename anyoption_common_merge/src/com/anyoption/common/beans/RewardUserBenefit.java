package com.anyoption.common.beans;

import java.io.Serializable;
import java.util.Date;

public class RewardUserBenefit implements Serializable {

	private static final long serialVersionUID = 5228251519630439214L;
	
	public static final long REWARD_USER_BENEFIT_ACTIVE = 1;
	
	private int id;
	private long userId;
	private int rewardBenefitId;
	private boolean isActive;
	private Date timeCreated;
	private String rewardBenefitName;
	private int rewardTierId;
	
	public RewardUserBenefit() {
		
	}
	
	/**
	 * @param id
	 * @param userId
	 * @param rewardBenefitId
	 * @param isActive
	 * @param timeCreated
	 */
	public RewardUserBenefit(long userId, int rewardBenefitId, boolean isActive) {
		this.id = 0;
		this.userId = userId;
		this.rewardBenefitId = rewardBenefitId;
		this.isActive = isActive;
		this.timeCreated = null;
	}
	
	/**
	 * @param id
	 * @param userId
	 * @param rewardBenefitId
	 * @param isActive
	 * @param timeCreated
	 */
	public RewardUserBenefit(int id, long userId, int rewardBenefitId, boolean isActive, Date timeCreated) {
		this.id = id;
		this.userId = userId;
		this.rewardBenefitId = rewardBenefitId;
		this.isActive = isActive;
		this.timeCreated = timeCreated;
	}
	
	/**
	 * @return the userId
	 */
	public long getUserId() {
		return userId;
	}
	/**
	 * @param userId the userId to set
	 */
	public void setUserId(long userId) {
		this.userId = userId;
	}
	/**
	 * @return the rewardBenefitId
	 */
	public int getRewardBenefitId() {
		return rewardBenefitId;
	}
	/**
	 * @param rewardBenefitId the rewardBenefitId to set
	 */
	public void setRewardBenefitId(int rewardBenefitId) {
		this.rewardBenefitId = rewardBenefitId;
	}
	/**
	 * @return the isActive
	 */
	public boolean isActive() {
		return isActive;
	}
	/**
	 * @param isActive the isActive to set
	 */
	public void setActive(boolean isActive) {
		this.isActive = isActive;
	}
	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}
	/**
	 * @return the timeCreated
	 */
	public Date getTimeCreated() {
		return timeCreated;
	}
	/**
	 * @param timeCreated the timeCreated to set
	 */
	public void setTimeCreated(Date timeCreated) {
		this.timeCreated = timeCreated;
	}

	/**
	 * @return the rewardBenefitName
	 */
	public String getRewardBenefitName() {
		return rewardBenefitName;
	}

	/**
	 * @param rewardBenefitName the rewardBenefitName to set
	 */
	public void setRewardBenefitName(String rewardBenefitName) {
		this.rewardBenefitName = rewardBenefitName;
	}
	/**
	 * @return the rewardTierId
	 */
	public int getRewardTierId() {
		return rewardTierId;
	}
	/**
	 * @param rewardTierId the rewardTierId to set
	 */
	public void setRewardTierId(int rewardTierId) {
		this.rewardTierId = rewardTierId;
	}
}
