/**
 * 
 */
package com.anyoption.common.beans;

import java.io.Serializable;

/**
 * @author pavelhe
 *
 */
public class LiveGlobeCity implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private long id;
	private String cityName;
	private long countryId;
	private String globeLatitude;
	private String globeLongtitude;
	private boolean isActive; 
	private long currencyId;
	
	/**
	 * @return the id
	 */
	public long getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(long id) {
		this.id = id;
	}
	/**
	 * @return the cityName
	 */
	public String getCityName() {
		return cityName;
	}
	/**
	 * @param cityName the cityName to set
	 */
	public void setCityName(String cityName) {
		this.cityName = cityName;
	}
	/**
	 * @return the countryId
	 */
	public long getCountryId() {
		return countryId;
	}
	/**
	 * @param countryId the countryId to set
	 */
	public void setCountryId(long countryId) {
		this.countryId = countryId;
	}
	/**
	 * @return the globeLatitude
	 */
	public String getGlobeLatitude() {
		return globeLatitude;
	}
	/**
	 * @param globeLatitude the globeLatitude to set
	 */
	public void setGlobeLatitude(String globeLatitude) {
		this.globeLatitude = globeLatitude;
	}
	/**
	 * @return the globeLongtitude
	 */
	public String getGlobeLongtitude() {
		return globeLongtitude;
	}
	/**
	 * @param globeLongtitude the globeLongtitude to set
	 */
	public void setGlobeLongtitude(String globeLongtitude) {
		this.globeLongtitude = globeLongtitude;
	}
	/**
	 * @return the isActive
	 */
	public boolean isActive() {
		return isActive;
	}
	/**
	 * @param isActive the isActive to set
	 */
	public void setActive(boolean isActive) {
		this.isActive = isActive;
	}
	/**
	 * @return the currencyId
	 */
	public long getCurrencyId() {
		return currencyId;
	}
	/**
	 * @param currencyId the currencyId to set
	 */
	public void setCurrencyId(long currencyId) {
		this.currencyId = currencyId;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "LiveGlobeCity [id=" + id + ", cityName=" + cityName
				+ ", countryId=" + countryId + ", globeLatitude="
				+ globeLatitude + ", globeLongtitude=" + globeLongtitude
				+ ", isActive=" + isActive + ", currencyId=" + currencyId + "]";
	}
	
}
