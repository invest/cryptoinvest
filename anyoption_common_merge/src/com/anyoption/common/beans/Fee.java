package com.anyoption.common.beans;

public class Fee {
	public static final long MAINTENANCE_FEE = 1;
	public static final long OPTION_PLUS_COMMISSION_FEE = 2;
	
	private long id;
	private String name;
	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String toString(){
		final String ls = System.getProperty("line.separator");
		String retValue = "";
		retValue = "Fee ( "
		        + super.toString() + ls
		        + "id = " + this.id + ls
		        + "name = " + this.name + ls
		        + " )";
		return retValue;
	}
}