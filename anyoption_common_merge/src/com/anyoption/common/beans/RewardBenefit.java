package com.anyoption.common.beans;

/**
 * @author liors
 *
 */
public class RewardBenefit implements java.io.Serializable {
	private static final long serialVersionUID = -1301845943716907875L;
	
	public static final long ANYOPTION_ACADEMY_1ST_2ND_RESTRICTION_LEVELS = 2;
	public static final long ACADEMY_3RD_RESTRICTION_LEVEL = 5;
	public static final long ANYOPTION_ACADEMY_4TH_RESTRICTION_LEVEL = 10;
	
	public final static long REWARD_BENEFIT_WEEKLY_MARKET = 1;
	public final static long REWARD_BENEFIT_ACADEMY_FIRST_AND_SECOND = 2;
	public final static long REWARD_BENEFIT_WELCOME_PACKAGE = 3;
	public final static long REWARD_BENEFIT_PERSONAL_ACCOUNT_MANAGER = 4;
	public final static long REWARD_BENEFIT_ACADEMY_THIRD = 5;
	public final static long REWARD_BENEFIT_DAILY_EMAIL_FINANCIAL = 6;
	public final static long REWARD_BENEFIT_DEPOSIT_500_GET_10_PERCENT = 7;
	public final static long REWARD_BENEFIT_48_HOUR_WITHDRAWALS = 8;
	public final static long REWARD_BENEFIT_PERSONAL_SENIOR_ACCOUNT_MANAGER = 9;
	public final static long REWARD_BENEFIT_ACADEMY_FOURTH = 10;
	public final static long REWARD_BENEFIT_RISK_FREE_INV = 11;
	public final static long REWARD_BENEFIT_WIRE_TRANSFER_FREE_COMMISSION = 12;
	public final static long REWARD_BENEFIT_DEPOSIT_2000_GET_20_PERCENT = 13;
	public final static long REWARD_BENEFIT_24_HOURS_WITHDRAWAL = 14;
	public final static long REWARD_BENEFIT_EXEMPT_WITHDRAWAL_FEE = 15;
	public final static long REWARD_BENEFIT_WEEKLY_SMS_SUMMARY = 16;
	public final static long REWARD_BENEFIT_SMS_TRADING_ALERTS = 17;
	public final static long REWARD_BENEFIT_INV_OVER_10000_GET_500 = 18;
	public final static long REWARD_BENEFIT_DEPOSIT_10000_GET_30_PERCENT = 19;
	
	private int id;
	private String name;
	private int rewardTierId;
	private long bonusId;
	private int coinsCost;
	private boolean isAffectedFromDemotion;
	private boolean isGrantOnce; 
	private boolean isGrantWhenSkip;
	
	public RewardBenefit() {
		
	}
	
	/**
	 * @param id
	 * @param name
	 * @param rewardTierId
	 * @param bonusId
	 * @param coinsCost
	 * @param isAffectedFromDemotion
	 * @param isGrantOnce
	 * @param isGrantWhenSkip
	 */
	public RewardBenefit(int id, String name, int rewardTierId, long bonusId,
			int coinsCost, boolean isAffectedFromDemotion, boolean isGrantOnce,
			boolean isGrantWhenSkip) {
		this.id = id;
		this.name = name;
		this.rewardTierId = rewardTierId;
		this.bonusId = bonusId;
		this.coinsCost = coinsCost;
		this.isAffectedFromDemotion = isAffectedFromDemotion;
		this.isGrantOnce = isGrantOnce;
		this.isGrantWhenSkip = isGrantWhenSkip;
	}

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * @return the rewardTierId
	 */
	public int getRewardTierId() {
		return rewardTierId;
	}
	/**
	 * @param rewardTierId the rewardTierId to set
	 */
	public void setRewardTierId(int rewardTierId) {
		this.rewardTierId = rewardTierId;
	}
	/**
	 * @return the bonusId
	 */
	public long getBonusId() {
		return bonusId;
	}
	/**
	 * @param bonusId the bonusId to set
	 */
	public void setBonusId(long bonusId) {
		this.bonusId = bonusId;
	}
	/**
	 * @return the coinsCost
	 */
	public int getCoinsCost() {
		return coinsCost;
	}
	/**
	 * @param coinsCost the coinsCost to set
	 */
	public void setCoinsCost(int coinsCost) {
		this.coinsCost = coinsCost;
	}
	/**
	 * @return the isAffectedFromDemotion
	 */
	public boolean isAffectedFromDemotion() {
		return isAffectedFromDemotion;
	}
	/**
	 * @param isAffectedFromDemotion the isAffectedFromDemotion to set
	 */
	public void setAffectedFromDemotion(boolean isAffectedFromDemotion) {
		this.isAffectedFromDemotion = isAffectedFromDemotion;
	}
	/**
	 * @return the isGrantOnce
	 */
	public boolean isGrantOnce() {
		return isGrantOnce;
	}
	/**
	 * @param isGrantOnce the isGrantOnce to set
	 */
	public void setGrantOnce(boolean isGrantOnce) {
		this.isGrantOnce = isGrantOnce;
	}
	/**
	 * @return the isGrantWhenSkip
	 */
	public boolean isGrantWhenSkip() {
		return isGrantWhenSkip;
	}
	/**
	 * @param isGrantWhenSkip the isGrantWhenSkip to set
	 */
	public void setGrantWhenSkip(boolean isGrantWhenSkip) {
		this.isGrantWhenSkip = isGrantWhenSkip;
	}
}
