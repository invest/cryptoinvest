/**
 * 
 */
package com.anyoption.common.beans;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;

/**
 * @author pavelt
 *
 */
public class TournamentsBanners implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7298426994698326887L;
	public static final String TOURNAMENTS_BANNERS_DIR_NAME = "tournamentsBanners";

	private long tabId;
	private String tabLabel;
	private long writerId;
	private Date dateCreated;
	private ArrayList<TournamentBannerFile> files = new ArrayList<TournamentBannerFile>();

	public TournamentsBanners() {
	}

	public long getTabId() {
		return tabId;
	}

	public void setTabId(long tabId) {
		this.tabId = tabId;
	}

	public long getWriterId() {
		return writerId;
	}

	public void setWriterId(long writerId) {
		this.writerId = writerId;
	}

	public Date getDateCreated() {
		return dateCreated;
	}

	public void setDateCreated(Date dateCreated) {
		this.dateCreated = dateCreated;
	}
	public ArrayList<TournamentBannerFile> getFiles() {
		return files;
	}

	public void setFiles(ArrayList<TournamentBannerFile> files) {
		this.files = files;
	}
	
	public String toString() {
		return "Tournament Banner [tabId=" + tabId
				+ ", writerId=" + writerId + ", dateCreated="
				+ dateCreated + "]";
	}

	public String getTabLabel() {
		return tabLabel;
	}

	public void setTabLabel(String tabLabel) {
		this.tabLabel = tabLabel;
	}

}
