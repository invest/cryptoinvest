package com.anyoption.common.beans;

import java.io.Serializable;

/**
 * @author liors
 *
 */
public class RewardBenefitManagement implements Serializable {
	private static final long serialVersionUID = -8735898781334145340L;
	private int benefitId;
	private int userBenefitId;
	private long bonusId;
	
	public RewardBenefitManagement() {
		 
	}

	public RewardBenefitManagement(int benefitId, int userBenefitId, long bonusId) {
		 this.benefitId = benefitId;
		 this.userBenefitId = userBenefitId;
		 this.bonusId = bonusId;
	}
	
	/**
	 * @return the benefitId
	 */
	public int getBenefitId() {
		return benefitId;
	}

	/**
	 * @param benefitId the benefitId to set
	 */
	public void setBenefitId(int benefitId) {
		this.benefitId = benefitId;
	}

	/**
	 * @return the userBenefitId
	 */
	public int getUserBenefitId() {
		return userBenefitId;
	}

	/**
	 * @param userBenefitId the userBenefitId to set
	 */
	public void setUserBenefitId(int userBenefitId) {
		this.userBenefitId = userBenefitId;
	}

	/**
	 * @return the bonusId
	 */
	public long getBonusId() {
		return bonusId;
	}

	/**
	 * @param bonusId the bonusId to set
	 */
	public void setBonusId(long bonusId) {
		this.bonusId = bonusId;
	}
	

}
