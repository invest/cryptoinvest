/**
 * 
 */
package com.anyoption.common.beans;

import com.anyoption.common.annotations.AnyoptionNoJSON;

/**
 * @author kirilim 
 */
public class MailBoxUser extends com.anyoption.common.beans.base.MailBoxUser {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7420189542668686769L;
	@AnyoptionNoJSON
	private boolean selected;
	@AnyoptionNoJSON
	private String statusName;
	@AnyoptionNoJSON
	private String popupTypeIdTxt;

	/**
	 * @return the selected
	 */
	public boolean isSelected() {
		return selected;
	}

	/**
	 * @param selected
	 *            the selected to set
	 */
	public void setSelected(boolean selected) {
		this.selected = selected;
	}

	public String getStatusName() {
		return statusName;
	}

	public void setStatusName(String statusName) {
		this.statusName = statusName;
	}	

	public String getPopupTypeIdTxt() {
		return popupTypeIdTxt;
	}

	public void setPopupTypeIdTxt(String popupTypeIdTxt) {
		this.popupTypeIdTxt = popupTypeIdTxt;
	}	
}