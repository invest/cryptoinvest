package com.anyoption.common.beans;

import java.util.Date;

import org.apache.log4j.Logger;

/**
 * @author EyalG
 *
 */
public class Notification {
    private static Logger log = Logger.getLogger(Notification.class);

    public static final long STATUS_QUEUED = 1;
    public static final long STATUS_SUCCEED = 2;
    public static final long STATUS_FAILED = 3;

    public static final long TYPE_INSERT_INVESTMENT = 1;
    public static final long TYPE_RESETTLE_INVESTMENT = 2;

    public static final String COMMENT_SUCCESS_RECEIVED = "success received";
    public static final String COMMENT_SUCCESS_SENT = "success sent";
    public static final String COMMENT_FAILED_TO_SUBMIT = "Failed submit: ";
    public static final String COMMENT_FAILED_RECEIVED = "Failed received ";

    protected long id;
    protected long Type;
    protected String notification;
    protected String reference; //investment/transaction id for now
    protected Date timeCreate;
    protected Date timeSent;
    protected long status;
    protected long retries;
    protected String comment;
    protected long externalUserId;

    public Notification() {

    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getStatus() {
        return status;
    }

    public void setStatus(long status) {
        this.status = status;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public long getRetries() {
        return retries;
    }

    public void setRetries(long retries) {
        this.retries = retries;
    }

    public Date getTimeSent() {
        return timeSent;
    }

    public void setTimeSent(Date timeSent) {
        this.timeSent = timeSent;
    }

	public String toString() {
        String ls = System.getProperty("line.separator");
        return ls + "Notification:" + ls +
            "id: " + id + ls +
            "Type: " + Type + ls +
            "notification: " + notification + ls +
            "reference: " + reference + ls +
            "timeCreate: " + timeCreate + ls +
            "timeSent: " + timeSent + ls +
            "status: " + status + ls +
            "retries: " + retries + ls +
            "comment: " + comment + ls;
    }

	public String getJsonNotfication() {
		 String ls = ", ";
		return "{ " +
		 "id: " + id + ls +
         "Type: " + Type + ls +
         "reference: " + reference + ls +
         notification +
         " }";
	}

	/**
	 * @return the comment
	 */
	public String getComment() {
		return comment;
	}

	/**
	 * @param comment the comment to set
	 */
	public void setComment(String comment) {
		this.comment = comment;
	}

	/**
	 * @return the notification
	 */
	public String getNotification() {
		return notification;
	}

	/**
	 * @param notification the notification to set
	 */
	public void setNotification(String notification) {
		this.notification = notification;
	}

	/**
	 * @return the timeCreate
	 */
	public Date getTimeCreate() {
		return timeCreate;
	}

	/**
	 * @param timeCreate the timeCreate to set
	 */
	public void setTimeCreate(Date timeCreate) {
		this.timeCreate = timeCreate;
	}

	/**
	 * @return the type
	 */
	public long getType() {
		return Type;
	}

	/**
	 * @param type the type to set
	 */
	public void setType(long type) {
		Type = type;
	}

	/**
	 * @return the externalUserId
	 */
	public long getExternalUserId() {
		return externalUserId;
	}

	/**
	 * @param externalUserId the externalUserId to set
	 */
	public void setExternalUserId(long externalUserId) {
		this.externalUserId = externalUserId;
	}
}