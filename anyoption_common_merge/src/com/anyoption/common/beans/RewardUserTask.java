package com.anyoption.common.beans;

/**
 * @author liors
 *
 */
public class RewardUserTask implements java.io.Serializable {
	private static final long serialVersionUID = -953183205640968778L;
	private int id;
	private long userId;
	private int taskId; 
	private int numOfActions;
	private int numOfRecurring;
	private boolean done;
	private boolean doneRecurring;
	
	public void setToDone() {
		this.done = true;
	}
	
	public boolean isFirstUserTask() {
		return id == 0;
	}
	
	/**
	 * @return the userId
	 */
	public long getUserId() {
		return userId;
	}
	/**
	 * @param userId the userId to set
	 */
	public void setUserId(long userId) {
		this.userId = userId;
	}
	/**
	 * @return the taskId
	 */
	public int getTaskId() {
		return taskId;
	}
	/**
	 * @param taskId the taskId to set
	 */
	public void setTaskId(int taskId) {
		this.taskId = taskId;
	}
	/**
	 * @return the numOfActions
	 */
	public int getNumOfActions() {
		return numOfActions;
	}
	/**
	 * @param numOfActions the numOfActions to set
	 */
	public void setNumOfActions(int numOfActions) {
		this.numOfActions = numOfActions;
	}
	/**
	 * @return the numOfRecurring
	 */
	public int getNumOfRecurring() {
		return numOfRecurring;
	}
	/**
	 * @param numOfRecurring the numOfRecurring to set
	 */
	public void setNumOfRecurring(int numOfRecurring) {
		this.numOfRecurring = numOfRecurring;
	}
	/**
	 * @return the done
	 */
	public boolean isDone() {
		return done;
	}
	/**
	 * @param done the done to set
	 */
	public void setDone(boolean done) {
		this.done = done;
	}

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the doneRecurring
	 */
	public boolean isDoneRecurring() {
		return doneRecurring;
	}

	/**
	 * @param doneRecurring the doneRecurring to set
	 */
	public void setDoneRecurring(boolean doneRecurring) {
		this.doneRecurring = doneRecurring;
	}
}
