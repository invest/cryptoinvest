/**
 * 
 */
package com.anyoption.common.beans;

import java.io.Serializable;
import java.util.Date;

import com.anyoption.common.enums.SkinGroup;

/**
 * @author pavelhe
 *
 */
public class OpportunitySkinGroupMap implements Serializable{

    private static final long serialVersionUID = -8445224645723625284L;
    
    private long id;
	private long opportunityId;
	private SkinGroup skinGroup;
	private Date timeCreated;
	private long writerId = -1L;
	private double shiftParameter;
	private int maxExposure;
	
	/**
	 * @return the id
	 */
	public long getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(long id) {
		this.id = id;
	}
	/**
	 * @return the opportunityId
	 */
	public long getOpportunityId() {
		return opportunityId;
	}
	/**
	 * @param opportunityId the opportunityId to set
	 */
	public void setOpportunityId(long opportunityId) {
		this.opportunityId = opportunityId;
	}
	/**
	 * @return the skinGroup
	 */
	public SkinGroup getSkinGroup() {
		return skinGroup;
	}
	/**
	 * @param skinGroup the skinGroup to set
	 */
	public void setSkinGroup(SkinGroup skinGroup) {
		this.skinGroup = skinGroup;
	}
	/**
	 * @return the timeCreated
	 */
	public Date getTimeCreated() {
		return timeCreated;
	}
	/**
	 * @param timeCreated the timeCreated to set
	 */
	public void setTimeCreated(Date timeCreated) {
		this.timeCreated = timeCreated;
	}
	/**
	 * @return the writerId
	 */
	public long getWriterId() {
		return writerId;
	}
	/**
	 * @param writerId the writerId to set
	 */
	public void setWriterId(long writerId) {
		this.writerId = writerId;
	}
	/**
	 * @return the shiftParameter
	 */
	public double getShiftParameter() {
		return shiftParameter;
	}
	/**
	 * @param shiftParameter the shiftParameter to set
	 */
	public void setShiftParameter(double shiftParameter) {
		this.shiftParameter = shiftParameter;
	}
	/**
	 * @return the maxExposure
	 */
	public int getMaxExposure() {
		return maxExposure;
	}
	/**
	 * @param maxExposure the maxExposure to set
	 */
	public void setMaxExposure(int maxExposure) {
		this.maxExposure = maxExposure;
	}
	
	@Override
	public String toString() {
		return "OpportunitySkinGroupMap [id=" + id + ", opportunityId="
				+ opportunityId + ", skinGroup=" + skinGroup + ", timeCreated="
				+ timeCreated + ", writerId=" + writerId + ", shiftParameter="
				+ shiftParameter + ", maxExposure=" + maxExposure + "]";
	}
	
}
