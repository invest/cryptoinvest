package com.anyoption.common.beans;

import java.math.BigDecimal;

import com.anyoption.common.annotations.AnyoptionNoJSON;
import com.anyoption.common.enums.SkinGroup;

/**
 * Market rate bean.
 */
public class MarketRate extends com.anyoption.common.beans.base.MarketRate {
	private static final long serialVersionUID = 3369820410949771494L;
	
	@AnyoptionNoJSON
    protected long id;
    @AnyoptionNoJSON
    protected long marketId;
    @AnyoptionNoJSON
    protected BigDecimal rate;
    @AnyoptionNoJSON
    protected SkinGroup skinGroup;
    @AnyoptionNoJSON
    protected BigDecimal last;
    @AnyoptionNoJSON
    protected BigDecimal ask;
    @AnyoptionNoJSON
    protected BigDecimal bid;
    
    public long getId() {
        return id;
    }
    
    public void setId(long id) {
        this.id = id;
    }
    
    public long getMarketId() {
        return marketId;
    }
    
    public void setMarketId(long marketId) {
        this.marketId = marketId;
    }
    
    public BigDecimal getRate() {
        return rate;
    }

    public void setRate(BigDecimal rate) {
        this.rate = rate;
    }

	public SkinGroup getSkinGroup() {
		return skinGroup;
	}

	public void setSkinGroup(SkinGroup skinGroup) {
		this.skinGroup = skinGroup;
	}

	public BigDecimal getLast() {
		return last;
	}

	public void setLast(BigDecimal last) {
		this.last = last;
	}

	public BigDecimal getAsk() {
		return ask;
	}

	public void setAsk(BigDecimal ask) {
		this.ask = ask;
	}

	public BigDecimal getBid() {
		return bid;
	}

	public void setBid(BigDecimal bid) {
		this.bid = bid;
	}

	@Override
	public String toString() {
		String ls = System.getProperty("line.separator");
		return ls + "MarketRate" + ls
				+ super.toString() + ls
				+ "id: " + id + ls
				+ "marketId: " + marketId + ls
				+ "rate: " + rate + ls
				+ "skinGroup: " + skinGroup + ls
				+ "last: " + last + ls
				+ "ask: " + ask + ls
				+ "bid: " + bid + ls;
	}
}