package com.anyoption.common.beans;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.Map;

import org.apache.log4j.Logger;

import com.anyoption.common.annotations.AnyoptionNoJSON;
import com.anyoption.common.beans.base.Skin;
import com.anyoption.common.enums.SkinGroup;
import com.anyoption.common.util.CommonUtil;
import com.anyoption.common.util.ConstantsBase;
import com.anyoption.common.util.GoldenMinute.Expiry;

public class Opportunity extends com.anyoption.common.beans.base.Opportunity {
    /**
     *
     */
    private static final long serialVersionUID = -1355812159570461475L;

    private static final Logger log = Logger.getLogger(Opportunity.class);

	public static final long TYPE_REGULAR = 1;
	public static final long TYPE_ONE_TOUCH = 2;
	public static final long TYPE_OPTION_PLUS = 3;
	public static final long TYPE_PRODUCT_BINARY_0100 = 4;
	public static final long TYPE_BINARY_0_100_ABOVE = 4;
	public static final long TYPE_BINARY_0_100_BELOW = 5;
	public static final long TYPE_BUBBLES = 6;
	public static final long TYPE_DYNAMICS = 7;
	public static final String TYPE_BINARY_0_100_ABOVE_S = "above";
	public static final String TYPE_BINARY_0_100_BELOW_S = "below";

	public static final String TYPE_REGULAR_STAING = "1";
	public static final String TYPE_ONE_TOUCH_STAING = "2";
	public static final String TYPE_OPTION_PLUS_STAING = "3";
	public static final String TYPE_BINARY_0_100_STAING = "4,5";

	public static final int ISRAEL_EXCHANGE    = 1;
    public static final int USD_ILS_EXCHANGE   = 9;
    public static final int TOKYO_EXCHANGE     = 4;
    public static final int HONG_KONG_EXCHANGE = 7;
    public static final int CHINA_EXCHANGE     = 25;
    public static final int TURKISH_EXCHANGE   = 24;
    public static final int JAKARTA_EXCHANGE   = 30;
    public static final int DUBAI_EXCHANGE     = 31;
    public static final int SINGAPORE_EXCHANGE = 39;

    public static final String TURKISH_BREAK_START = "12:00"; // 12:00 in turkish time zone
    public static final String TURKISH_BREAK_END = "14:30"; // 14:30 in turkish time zone

    public static final String TOKYO_BREAK_START = "11:00"; // 11:00 in TOKYO time zone
    public static final String TOKYO_BREAK_END = "12:40"; // 12:40 in TOKYO time zone

    public static final String HONG_KONG_BREAK_START = "12:00"; // 12:30 in HONG_KONG time zone
    public static final String HONG_KONG_BREAK_END = "13:40"; // 14:30 in HONG_KONG time zone

    public static final String CHINA_BREAK_START = "11:30"; // 11:30 in CHINA time zone
    public static final String CHINA_BREAK_END = "13:00"; // 13:00 in CHINA time zone

    public static final String JAKARTA_BREAK_START = "12:00"; // in JAKARTA time zone
    public static final String JAKARTA_BREAK_END = "13:40"; // in JAKARTA time zone

    public static final String DUBAI_BREAK_START = "12:45"; // in DUBAI time zone
    public static final String DUBAI_BREAK_END = "14:40"; // in DUBAI time zone

    public static final String SINGAPORE_BREAK_START = "12:30"; // 12:30 in SINGAPORE time zone
    public static final String SINGAPORE_BREAK_END = "14:10"; // 14:10 in SINGAPORE time zone

    // Constants useful for ET_GROUP_CLOSE property
    public static final int CLOSING_FILTER_FLAG_CLOSEST = 1;
    public static final int CLOSING_FILTER_FLAG_TODAY   = 2;
    public static final int CLOSING_FILTER_FLAG_WEEK    = 4;
    public static final int CLOSING_FILTER_FLAG_MONTH   = 8;
    public static final int CLOSING_FILTER_FLAG_QUARTER = 32;

	@AnyoptionNoJSON
    private Market market;
	@AnyoptionNoJSON
	private Date timeCreated;
	@AnyoptionNoJSON
	private Date timeActClosing;
	@AnyoptionNoJSON
	private Date timeSettled;
	@AnyoptionNoJSON
	private long oddsTypeId;
	@AnyoptionNoJSON
	private String oddsGroup;
	@AnyoptionNoJSON
	private OpportunityOddsType oddsType;
	@AnyoptionNoJSON
	private long opportunityTypeId;
	@AnyoptionNoJSON
	private boolean disabledService;
	@AnyoptionNoJSON
	private boolean disabledTrader;
	@AnyoptionNoJSON
	private long writerId;
	@AnyoptionNoJSON
	private long exchangeId;
	@AnyoptionNoJSON
	private long investmentLimitGroupId;
    @AnyoptionNoJSON
    private InvestmentLimitsGroup investmentLimitGroup;
    @AnyoptionNoJSON
    private Map<SkinGroup, OpportunitySkinGroupMap> skinGroupMappings;
	@AnyoptionNoJSON
	private double autoShiftParameter;
	@AnyoptionNoJSON
	private long winLose;
	@AnyoptionNoJSON
	private boolean deductWinOdds;
	@AnyoptionNoJSON
	private String opportunityTypeDesc;
	@AnyoptionNoJSON
	private String opportunityOddsTypeName;

	// for the LS data adaptor - the exposure calculation amounts
	@AnyoptionNoJSON
	private double puts;
	@AnyoptionNoJSON
	private double calls;
	@AnyoptionNoJSON
    private int putsCount;
	@AnyoptionNoJSON
    private int callsCount;
	@AnyoptionNoJSON
    private double putsShift;
	@AnyoptionNoJSON
    private double callsShift;
	@AnyoptionNoJSON
	private boolean shifting;
	@AnyoptionNoJSON
	private boolean shiftUp;
	@AnyoptionNoJSON
	private BigDecimal lastShift;
	@AnyoptionNoJSON
	private boolean timeToClose;
	@AnyoptionNoJSON
	private Date timeNextOpen;
	@AnyoptionNoJSON
	private int putsCountET;
	@AnyoptionNoJSON
	private int callsCountET;

	/**
	 * For hour opportunities the level is updated until update for next hour is
	 * received. This flag should be set to true once an update for the next
	 * hour is received.
	 */
	@AnyoptionNoJSON
	private boolean closingLevelReceived;
	@AnyoptionNoJSON
	private double nextClosingLevel;
	@AnyoptionNoJSON
	private boolean notClosedAlertSent;

	// one touch variables
	@AnyoptionNoJSON
	private String oneTouchMsg;
	@AnyoptionNoJSON
	private long oneTouchUpDown;
	@AnyoptionNoJSON
	private boolean isOneTouchUp;
	@AnyoptionNoJSON
	private long decimalPointZeroOneHundred;
	@AnyoptionNoJSON
	protected double gmLevel;
	@AnyoptionNoJSON
	protected boolean isOpenGraph;
	@AnyoptionNoJSON
	protected long oneTouchOppExposure;
	@AnyoptionNoJSON
	protected boolean useManualClosingLevel;  /** if to use manual closing level for this opportunity */
	@AnyoptionNoJSON
	protected double manualClosingLevel;  /** manual closing level to use for this opportunity */

	   //bean to calculate the binary Zero One Hundred formula
//	@AnyoptionNoJSON
//    private BinaryZeroOneHundred binaryZeroOneHundred;
	@AnyoptionNoJSON
	private double contractsBought; //in $
	@AnyoptionNoJSON
	private double contractsSold; //in $
	@AnyoptionNoJSON
    private ReutersQuotes lastUpdateQuote;
	@AnyoptionNoJSON
	private ReutersQuotes beforeLastUpdateQuote;
	@AnyoptionNoJSON
	private ReutersQuotes firstUpdateQuote;
	@AnyoptionNoJSON
    private boolean isHaveReutersQuote;

	@AnyoptionNoJSON
	private OpportunityType type;
	@AnyoptionNoJSON
	private Exchange exchange;
	@AnyoptionNoJSON
	private double maxInvAmountCoeffPerUser;
	
	@AnyoptionNoJSON
	private boolean suspended;
	@AnyoptionNoJSON
	private String quoteParams;
	@AnyoptionNoJSON
	private Object quoteParamsObject;

	public boolean isSuspended() {
	    return suspended;
	}

	public void setSuspended(boolean suspended) {
	    this.suspended = suspended;
	}

	public Opportunity() {
		super();
		state = 0;
		timeToClose = false;
		notClosedAlertSent = false;
		closingLevelReceived = false;
	}

	/**
	 * @return the calls
	 */
	public double getCalls() {
		return calls;
	}

	/**
	 * @param calls
	 *            the calls to set
	 */
	public void setCalls(double calls) {
		this.calls = calls;
	}

	/**
	 * @return the exchangeId
	 */
	public long getExchangeId() {
		return exchangeId;
	}

	/**
	 * @param exchangeId the exchangeId to set
	 */
	public void setExchangeId(long exchangeId) {
		this.exchangeId = exchangeId;
	}

	public String getIdTxt() {
		return Long.toString(this.id);
	}

	/**
	 * @return the investmentLimitGroupId
	 */
	public long getInvestmentLimitGroupId() {
		return investmentLimitGroupId;
	}

	/**
	 * @param investmentLimitGroupId the investmentLimitGroupId to set
	 */
	public void setInvestmentLimitGroupId(long investmentLimitGroupId) {
		this.investmentLimitGroupId = investmentLimitGroupId;
	}

	   /**
     * @return the investmentLimitGroup
     */
    public InvestmentLimitsGroup getInvestmentLimitGroup() {
        return investmentLimitGroup;
    }

    /**
     * @param investmentLimitGroup
     *            the investmentLimitGroup to set
     */
    public void setInvestmentLimitGroup(
            InvestmentLimitsGroup investmentLimitGroup) {
        this.investmentLimitGroup = investmentLimitGroup;
    }

	public Market getMarket() {
        return market;
    }

    public void setMarket(Market market) {
        this.market = market;
    }

    public long getOddsTypeId() {
		return oddsTypeId;
	}

	public void setOddsTypeId(long oddsTypeId) {
		this.oddsTypeId = oddsTypeId;
	}

   public String getOddsGroup() {
        return oddsGroup;
    }

    public void setOddsGroup(String oddsGroup) {
        this.oddsGroup = oddsGroup;
    }

	public OpportunityOddsType getOddsType() {
        return oddsType;
    }

    public void setOddsType(OpportunityOddsType oddsType) {
        this.oddsType = oddsType;
    }

    /**
	 * @return the puts
	 */
	public double getPuts() {
		return puts;
	}

	/**
	 * @param puts the puts to set
	 */
	public void setPuts(double puts) {
		this.puts = puts;
	}

	public BigDecimal getLastShift() {
		return lastShift;
	}

	public void setLastShift(BigDecimal lastShift) {
		this.lastShift = lastShift;
	}

    /**
	 * @return the shifting
	 */
	public boolean isShifting() {
		return shifting;
	}

	/**
	 * @param shifting the shifting to set
	 */
	public void setShifting(boolean shifting) {
		this.shifting = shifting;
	}

    /**
     * @return the skinGroupMappings
     */
    public Map<SkinGroup, OpportunitySkinGroupMap> getSkinGroupMappings() {
        return skinGroupMappings;
    }

    /**
     * @param skinGroupMappings the skinGroupMappings to set
     */
    public void setSkinGroupMappings(Map<SkinGroup,
                                        OpportunitySkinGroupMap> skinGroupMappings) {
        this.skinGroupMappings = skinGroupMappings;
    }

	public double getAutoShiftParameter() {
		return autoShiftParameter;
	}

	public void setAutoShiftParameter(double autoShiftParameter) {
		this.autoShiftParameter = autoShiftParameter;
	}

	/**
	 * @return the shiftUp
	 */
	public boolean isShiftUp() {
		return shiftUp;
	}

	/**
	 * @param shiftUp the shiftUp to set
	 */
	public void setShiftUp(boolean shiftUp) {
		this.shiftUp = shiftUp;
	}

	/**
	 * @return <code>true</code> if the state is one that need to receive
	 *         market updates (from Reuters) else <code>false</code>. State
	 *         that does not need updates for example is "CREATED".
	 */
	public boolean isInOppenedState() {
		return state >= Opportunity.STATE_OPENED && state <= Opportunity.STATE_CLOSED;
	}

    /**
     * @return <code>true</code> if users can invest in this opportunity, otherwise <code>false</code>.
     * NOTE: the opportunity can be OPEN but the state may not allow investing.
     * See {@link Opportunity#isInOppenedState()}
     */
    public boolean isAvailableForTrading() {
        return state == Opportunity.STATE_OPENED || state == Opportunity.STATE_LAST_10_MIN;
    }

	/**
	 * @return the timeCreated
	 */
	public Date getTimeCreated() {
		return timeCreated;
	}

	/**
	 * @param timeCreated the timeCreated to set
	 */
	public void setTimeCreated(Date timeCreated) {
		this.timeCreated = timeCreated;
	}

	public boolean isLessThanHourToClosing() {
	    return getTimeEstClosing().getTime() - System.currentTimeMillis() <= 60L * 60L * 1000L;
	}

	/**
	 * @return the timeActClosing
	 */
	public Date getTimeActClosing() {
		return timeActClosing;
	}

	/**
	 * @param timeActClosing the timeActClosing to set
	 */
	public void setTimeActClosing(Date timeActClosing) {
		this.timeActClosing = timeActClosing;
	}

	/**
	 * @return the timeNextOpen
	 */
	public Date getTimeNextOpen() {
		return timeNextOpen;
	}

	/**
	 * @param timeNextOpen the timeNextOpen to set
	 */
	public void setTimeNextOpen(Date timeNextOpen) {
		this.timeNextOpen = timeNextOpen;
	}

    public String getOneTouchCurrentLevel() {

        return CommonUtil.formatLevelByDecimalPoint(currentLevel, decimalPointOneTouch);

        /*
         * if (marketId == 21 || marketId == 200) { DecimalFormat df = new
         * DecimalFormat("#"); return df.format(currentLevel); } DecimalFormat
         * df = new DecimalFormat("#.00"); return df.format(currentLevel);
         */
    }

    public String getZeroOneHundredCurrentLevel(){
        return CommonUtil.formatLevelByDecimalPoint(currentLevel, decimalPointZeroOneHundred);
    }
	/**
	 * @return the timeSettled
	 */
	public Date getTimeSettled() {
		return timeSettled;
	}

	/**
	 * @param timeSettled the timeSettled to set
	 */
	public void setTimeSettled(Date timeSettled) {
		this.timeSettled = timeSettled;
	}

	public long getWriterId() {
		return writerId;
	}

	public void setWriterId(long writerId) {
		this.writerId = writerId;
	}

	public long getOpportunityTypeId() {
		return opportunityTypeId;
	}

	public void setOpportunityTypeId(long opportunityTypeId) {
		this.opportunityTypeId = opportunityTypeId;
	}

	public String getOpportunityOddsTypeName() {
		return opportunityOddsTypeName;
	}

	public void setOpportunityOddsTypeName(String opportunityOddsTypeName) {
		this.opportunityOddsTypeName = opportunityOddsTypeName;
	}

	public String getOpportunityTypeDesc() {
		return opportunityTypeDesc;
	}

	public void setOpportunityTypeDesc(String opportunityTypeDesc) {
		this.opportunityTypeDesc = opportunityTypeDesc;
	}

	public boolean isDisabledService() {
		return disabledService;
	}

	public void setDisabledService(boolean disabledService) {
		this.disabledService = disabledService;
	}

	public boolean isDisabledTrader() {
		return disabledTrader;
	}

	public void setDisabledTrader(boolean disabledTrader) {
		this.disabledTrader = disabledTrader;
	}

	public boolean isLongTerm() {
		return scheduled == Opportunity.SCHEDULED_WEEKLY	|| scheduled == Opportunity.SCHEDULED_MONTHLY
				|| scheduled == Opportunity.SCHEDULED_QUARTERLY;
	}

	public boolean isTimeToClose() {
		return timeToClose;
	}

	public void setTimeToClose(boolean timeToClose) {
		this.timeToClose = timeToClose;
	}

	public boolean isClosingLevelReceived() {
		return closingLevelReceived;
	}

	public void setClosingLevelReceived(boolean closingLevelReceived) {
		this.closingLevelReceived = closingLevelReceived;
	}

	public double getNextClosingLevel() {
		return nextClosingLevel;
	}

	public void setNextClosingLevel(double nextClosingLevel) {
		this.nextClosingLevel = nextClosingLevel;
	}

	public boolean isNotClosedAlertSent() {
		return notClosedAlertSent;
	}

	public void setNotClosedAlertSent(boolean notClosedAlertSent) {
		this.notClosedAlertSent = notClosedAlertSent;
	}

	public boolean isDeductWinOdds() {
		return deductWinOdds;
	}

	public void setDeductWinOdds(boolean deductWinOdds) {
		this.deductWinOdds = deductWinOdds;
	}

    public float getOverOddsWin() {
        if (!deductWinOdds) {
            return oddsType.getOverOddsWin();
        }
        long timeToLastInvest = timeLastInvest.getTime() - System.currentTimeMillis();
        int steps = 0;
        if (timeToLastInvest < 8 * 60000 && timeToLastInvest >= 6 * 60000) {
            steps = 1;
        } else if (timeToLastInvest < 6 * 60000 && timeToLastInvest >= 4 * 60000) {
            steps = 2;
        } else if (timeToLastInvest < 4 * 60000 && timeToLastInvest >= 2 * 60000) {
            steps = 3;
        } else if (timeToLastInvest < 2 * 60000) {
            steps = 4;
        }
        BigDecimal odds = new BigDecimal(Float.toString(oddsType.getOverOddsWin()));
        BigDecimal stps = new BigDecimal(steps);
        BigDecimal deductStep = new BigDecimal(Float.toString(oddsType.getOddsWinDeductStep()));
        float oow = odds.subtract(stps.multiply(deductStep)).floatValue();
        if (log.isTraceEnabled()) {
            log.trace(
                    "timeToLastInvest: " + timeToLastInvest
                    + " steps: " + steps
                    + " odds: " + odds
                    + " stps: " + stps
                    + " deductStep: " + deductStep
                    + " oow: " + oow);
        }
        return oow;
    }

	public long getWinLose() {
		return winLose;
	}

	public void setWinLose(long winLose) {
		this.winLose = winLose;
	}

	public String getMsg() {
		return oneTouchMsg;
	}

	public void setOneTouchMsg(String oneTouchMsg) {
		this.oneTouchMsg = oneTouchMsg;
	}

	public long getOneTouchUpDown() {
		return oneTouchUpDown;
	}

	public void setOneTouchUpDown(long upDown) {
		this.oneTouchUpDown = upDown;

		if (this.oneTouchUpDown == 1) {
			this.isOneTouchUp = true;
		} else {
			this.isOneTouchUp = false;
		}
	}

	public boolean getIsBooleanOneTouchUpDown() {
		if (this.oneTouchUpDown == 1) {
			return true;
		} else {
			return false;
		}
	}

	public boolean getIsOneTouchUp() {
		return isOneTouchUp;
	}

	public void setIsOneTouchUp(boolean isOneTouchUp) {
		this.isOneTouchUp = isOneTouchUp;
	}

    public String getOverOddsWinFormat() {
        DecimalFormat percentFormat = new DecimalFormat("#0%");
        return percentFormat.format(oddsType.getOverOddsWin());
    }

    public String getOverOddsWinFormatAO() {
        // no need in java formatting 'cause there is problem in arabic and
        // turkish skin
        return String.valueOf((int) (oddsType.getOverOddsWin() * 100));
    }

	public boolean getIsOneTouchOpp() {
		if (opportunityTypeId == ConstantsBase.OPPORTUNITIES_TYPE_ONE_TOUCH) {
			return true;
		}
		return false;
	}

    public boolean getIsOppSettled() {
        if (isSettled == 1) {
            return true;
        } else {
            return false;
        }
    }

	/**
	 * Check if this opportunity is good for current Golden Minutes interval.
	 *
	 * @param endOfHour
	 *            if the current Golden Minutes interval is a end of hour or mid
	 *            hour one
	 * @return <code>true</code> if this opp is good for the current GM
	 *         interval else <code>false</code>
	 */
	public boolean isGoodForGoldenMinute(Expiry expiry) {
		if (scheduled != SCHEDULED_HOURLY || !isInOppenedState()) {
			return false;
		}
		Calendar c = Calendar.getInstance();
		c.setTime(timeEstClosing);
		int min = c.get(Calendar.MINUTE);
		return min == expiry.minutes;
	}

	public double getGmLevel() {
		return gmLevel;
	}

	public void setGmLevel(double gmLevel) {
		this.gmLevel = gmLevel;
	}

	/**
	 * @return the isOpenGraph
	 */
	public boolean isOpenGraph() {
		return isOpenGraph;
	}

	/**
	 * @param isOpenGraph the isOpenGraph to set
	 */
	public void setOpenGraph(boolean isOpenGraph) {
		this.isOpenGraph = isOpenGraph;
	}

    /**
	 * @return the manualClosingLevel
	 */
	public double getManualClosingLevel() {
		return manualClosingLevel;
	}

	/**
	 * @param manualClosingLevel the manualClosingLevel to set
	 */
	public void setManualClosingLevel(double manualClosingLevel) {
	    this.useManualClosingLevel = true;
		this.manualClosingLevel = manualClosingLevel;
	}

	/**
	 * @return the useManualClosingLevel
	 */
	public boolean isUseManualClosingLevel() {
		return useManualClosingLevel;
	}

	/**
	 * @param useManualClosingLevel the useManualClosingLevel to set
	 */
	public void setUseManualClosingLevel(boolean useManualClosingLevel) {
		this.useManualClosingLevel = useManualClosingLevel;
	}

	/**
	 * @return the oneTouchOppExposure
	 */
	public long getOneTouchOppExposure() {
		return oneTouchOppExposure;
	}

	/**
	 * @param oneTouchOppExposure the oneTouchOppExposure to set
	 */
	public void setOneTouchOppExposure(long oneTouchOppExposure) {
		this.oneTouchOppExposure = oneTouchOppExposure;
	}

	public Exchange getExchange() {
		return exchange;
	}

	public void setExchange(Exchange exchange) {
		this.exchange = exchange;
	}

	public OpportunityType getType() {
		return type;
	}

	public void setType(OpportunityType type) {
		this.type = type;
	}

	@Override
	public String toString() {
        String ls = System.getProperty("line.separator");
        return ls + "Opportunity" + ls
            + super.toString() + ls
            + "market: " + market + ls
            + "timeCreated: " + timeCreated + ls
            + "timeActClosing: " + timeActClosing + ls
            + "timeSettled: " + timeSettled + ls
            + "published: " + isPublished() + ls
            + "settled: " + isSettled() + ls
            + "oddsTypeId: " + oddsTypeId + ls
            + "oddsGroup = " + oddsGroup + ls
            + "opportunityTypeId: " + opportunityTypeId + ls
            + "disabledService: " + disabledService + ls
            + "disabledTrader: " + disabledTrader + ls
            + "writerId: " + writerId + ls
            + "exchangeId: " + exchangeId + ls
            + "investmentLimitGroupId: " + investmentLimitGroupId + ls
            + "investmentLimitGroup = " + investmentLimitGroup + ls
            + "skinGroupMappings: " + skinGroupMappings + ls
            + "autoShiftParameter: " + autoShiftParameter + ls
            + "opportunityTypeDesc: " + opportunityTypeDesc + ls
            + "opportunityOddsTypeName: " + opportunityOddsTypeName + ls
            + "puts: " + puts + ls
            + "calls: " + calls + ls
            + "putsCount: " + putsCount + ls
            + "callsCount: " + callsCount + ls
            + "putsShift: " + putsShift + ls
            + "callsShift: " + callsShift + ls
            + "shifting: " + shifting + ls
            + "shiftUp: " + shiftUp + ls
            + "lastShift: " + lastShift + ls
            + "timeToClose: " + timeToClose + ls
            + "timeNextOpen: " + timeNextOpen + ls
            + "putsCountET: " + putsCountET + ls
            + "callsCountET: " + callsCountET + ls
            + "closingLevelReceived: " + closingLevelReceived + ls
            + "nextClosingLevel: " + nextClosingLevel + ls
            + "notClosedAlertSent: " + notClosedAlertSent + ls
            + "deductWinOdds: " + deductWinOdds + ls
            + "maxInvAmountCoeffPerUser: " + maxInvAmountCoeffPerUser + ls
            + "isSuspended: " + suspended + ls
            + "quoteParams: " + quoteParams + ls;
    }

    /**
     * @return the putsCount
     */
    public int getPutsCount() {
        return putsCount;
    }

    /**
     * @param putsCount the putsCount to set
     */
    public void setPutsCount(int putsCount) {
        this.putsCount = putsCount;
    }

    /**
     * @return the callsCount
     */
    public int getCallsCount() {
        return callsCount;
    }

    /**
     * @param callsCount the callsCount to set
     */
    public void setCallsCount(int callsCount) {
        this.callsCount = callsCount;
    }

    /**
     * @return a string representing the calls rate used for trends functionality
     */
    public String getCallsCountRate(long skinBusinessCase) {
        if (skinBusinessCase == Skin.SKIN_BUSINESS_ETRADER) {
            if (getCallsCountET() + getPutsCountET() > 0 /* avoid division by zero */) {
                return new DecimalFormat("0.00", new DecimalFormatSymbols(Locale.US))
                            .format(getCallsCountET() / (double) (getCallsCountET() + getPutsCountET()));
            }
        } else { //SKIN_BUSINESS_ANYOPTION
            int callCountAO = getCallsCount() - getCallsCountET();
            int putCountAO = getPutsCount() - getPutsCountET();
            if (callCountAO + putCountAO > 0 /* avoid division by zero */) {
                return new DecimalFormat("0.00", new DecimalFormatSymbols(Locale.US))
                            .format(callCountAO / (double) (callCountAO + putCountAO));
            }
        }
        return "0.0";
    }

    /**
     * @return the putsShift
     */
    public double getPutsShift() {
        return putsShift;
    }

    /**
     * @param putsShift the putsShift to set
     */
    public void setPutsShift(double putsShift) {
        this.putsShift = putsShift;
    }

    /**
     * @return the callsShift
     */
    public double getCallsShift() {
        return callsShift;
    }

    /**
     * @param callsShift the callsShift to set
     */
    public void setCallsShift(double callsShift) {
        this.callsShift = callsShift;
    }

    /**
     * @return the putsCountET
     */
    public int getPutsCountET() {
        return putsCountET;
    }

    /**
     * @param putsCountET the putsCountET to set
     */
    public void setPutsCountET(int putsCountET) {
        this.putsCountET = putsCountET;
    }

    /**
     * @return the callsCountET
     */
    public int getCallsCountET() {
        return callsCountET;
    }

    /**
     * @param callsCountET the callsCountET to set
     */
    public void setCallsCountET(int callsCountET) {
        this.callsCountET = callsCountET;
    }

    /**
     * @return the decimalPointZeroOneHundred
     */
    public long getDecimalPointZeroOneHundred() {
        return decimalPointZeroOneHundred;
    }

    /**
     * @param decimalPointZeroOneHundred the decimalPointZeroOneHundred to set
     */
    public void setDecimalPointZeroOneHundred(long decimalPointZeroOneHundred) {
        this.decimalPointZeroOneHundred = decimalPointZeroOneHundred;
    }

    public boolean isBinary0100Type() {
        if (opportunityTypeId == TYPE_BINARY_0_100_ABOVE || opportunityTypeId == TYPE_BINARY_0_100_BELOW) {
            return true;
        }
        return false;
    }

//    /**
//     * @return the binaryZeroOneHundred
//     */
//    public BinaryZeroOneHundred getBinaryZeroOneHundred() {
//        return binaryZeroOneHundred;
//    }
//
//    /**
//     * @param binaryZeroOneHundred the binaryZeroOneHundred to set
//     */
//    public void setBinaryZeroOneHundred(BinaryZeroOneHundred binaryZeroOneHundred) {
//        this.binaryZeroOneHundred = binaryZeroOneHundred;
//    }

    /**
     * @return the contractsBought
     */
    public double getContractsBought() {
        return contractsBought;
    }

    /**
     * @param contractsBought the contractsBought to set
     */
    public void setContractsBought(double contractsBought) {
        this.contractsBought = contractsBought;
    }

    /**
     * @return the contractsSold
     */
    public double getContractsSold() {
        return contractsSold;
    }

    /**
     * @param contractsSold the contractsSold to set
     */
    public void setContractsSold(double contractsSold) {
        this.contractsSold = contractsSold;
    }

    /**
     * @return the lastUpdateQuote
     */
    public ReutersQuotes getLastUpdateQuote() {
        return lastUpdateQuote;
    }

    /**
     * @param lastUpdateQuote the lastUpdateQuote to set
     */
    public void setLastUpdateQuote(ReutersQuotes lastUpdateQuote) {
        this.lastUpdateQuote = lastUpdateQuote;
    }

    /**
     * @return the beforeLastUpdateQuote
     */
    public ReutersQuotes getBeforeLastUpdateQuote() {
        return beforeLastUpdateQuote;
    }

    /**
     * @param beforeLastUpdateQuote the beforeLastUpdateQuote to set
     */
    public void setBeforeLastUpdateQuote(ReutersQuotes beforeLastUpdateQuote) {
        this.beforeLastUpdateQuote = beforeLastUpdateQuote;
    }

    /**
     * @return the firstUpdateQuote
     */
    public ReutersQuotes getFirstUpdateQuote() {
        return firstUpdateQuote;
    }

    /**
     * @param firstUpdateQuote the firstUpdateQuote to set
     */
    public void setFirstUpdateQuote(ReutersQuotes firstUpdateQuote) {
        this.firstUpdateQuote = firstUpdateQuote;
    }

    /**
     * @param lastUpdateQuote the lastUpdateQuote to set
     */
    public void setLastAndBeforeUpdateQuote(ReutersQuotes lastUpdateQuote) {
        this.beforeLastUpdateQuote = this.lastUpdateQuote;
        this.lastUpdateQuote = lastUpdateQuote;
    }

    /**
     * @return the isHaveReutersQuote
     */
    public boolean isHaveReutersQuote() {
        return isHaveReutersQuote;
    }

    /**
     * @param isHaveReutersQuote the isHaveReutersQuote to set
     */
    public void setHaveReutersQuote(boolean isHaveReutersQuote) {
        this.isHaveReutersQuote = isHaveReutersQuote;
    }

	public double getMaxInvAmountCoeffPerUser() {
		return maxInvAmountCoeffPerUser;
	}

	public void setMaxInvAmountCoeffPerUser(double maxInvAmountCoeffPerUser) {
		this.maxInvAmountCoeffPerUser = maxInvAmountCoeffPerUser;
	}

	public String getQuoteParams() {
		return quoteParams;
	}

	public void setQuoteParams(String quoteParams) {
		this.quoteParams = quoteParams;
	}

	public Object getQuoteParamsObject() {
		return quoteParamsObject;
	}

	public void setQuoteParamsObject(Object quoteParamsObject) {
		this.quoteParamsObject = quoteParamsObject;
	}
}