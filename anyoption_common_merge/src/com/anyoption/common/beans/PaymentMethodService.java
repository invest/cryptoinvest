package com.anyoption.common.beans;

/**
 * PaymentMethodService
 * @author eyalo
 */
public class PaymentMethodService {
	
	private long id;
	private long transactionClassTypeId;
	private long transactionStatusId;
	private String operationType;
	private String operationStatus;
	private String methodName;
	
	/**
	 * 
	 */
	public PaymentMethodService() {
		// TODO Auto-generated constructor stub
	}
	
	/**
	 * @param transactionClassTypeId
	 * @param transactionStatusId
	 * @param operationType
	 * @param operationStatus 
	 */
	public PaymentMethodService(long transactionClassTypeId,
			long transactionStatusId, String operationType, String operationStatus) {
		this.transactionClassTypeId = transactionClassTypeId;
		this.transactionStatusId = transactionStatusId;
		this.operationType = operationType;
		this.operationStatus = operationStatus;
	}

	/**
	 * @return the id
	 */
	public long getId() {
		return id;
	}
	
	/**
	 * @param id the id to set
	 */
	public void setId(long id) {
		this.id = id;
	}
	
	/**
	 * @return the transactionStatusId
	 */
	public long getTransactionStatusId() {
		return transactionStatusId;
	}
	
	/**
	 * @param transactionStatusId the transactionStatusId to set
	 */
	public void setTransactionStatusId(long transactionStatusId) {
		this.transactionStatusId = transactionStatusId;
	}
	
	/**
	 * @return the methodName
	 */
	public String getMethodName() {
		return methodName;
	}
	
	/**
	 * @param methodName the methodName to set
	 */
	public void setMethodName(String methodName) {
		this.methodName = methodName;
	}

	/**
	 * @return the transactionClassTypeId
	 */
	public long getTransactionClassTypeId() {
		return transactionClassTypeId;
	}

	/**
	 * @param transactionClassTypeId the transactionClassTypeId to set
	 */
	public void setTransactionClassTypeId(long transactionClassTypeId) {
		this.transactionClassTypeId = transactionClassTypeId;
	}

	/**
	 * @return the operationType
	 */
	public String getOperationType() {
		return operationType;
	}

	/**
	 * @param operationType the operationType to set
	 */
	public void setOperationType(String operationType) {
		this.operationType = operationType;
	}

	/**
	 * @return the operationStatus
	 */
	public String getOperationStatus() {
		return operationStatus;
	}

	/**
	 * @param operationStatus the operationStatus to set
	 */
	public void setOperationStatus(String operationStatus) {
		this.operationStatus = operationStatus;
	}
}
