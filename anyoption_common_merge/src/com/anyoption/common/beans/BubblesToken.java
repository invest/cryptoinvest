package com.anyoption.common.beans;

import java.util.Date;

public class BubblesToken implements java.io.Serializable {

	private static final long serialVersionUID = -4929020946391919858L;

	private String token;
	private long userId;
	private String serverId;
	private Date timeCreated;
	private long loginId;
	private long writerId;
	
	public String getToken() {
		return token;
	}
	
	public void setToken(String token) {
		this.token = token;
	}
	
	public long getUserId() {
		return userId;
	}
	
	public void setUserId(long userId) {
		this.userId = userId;
	}
	
	public String getServerId() {
		return serverId;
	}
	
	public void setServerId(String serverId) {
		this.serverId = serverId;
	}
	
	public Date getTimeCreated() {
		return timeCreated;
	}
	
	public void setTimeCreated(Date timeCreated) {
		this.timeCreated = timeCreated;
	}
	
	public long getLoginId() {
		return loginId;
	}
	
	public void setLoginId(long loginId) {
		this.loginId = loginId;
	}
	
	public long getWriterId() {
		return writerId;
	}
	
	public void setWriterId(long writerId) {
		this.writerId = writerId;
	}
}
