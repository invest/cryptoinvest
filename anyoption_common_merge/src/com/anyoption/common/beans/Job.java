package com.anyoption.common.beans;

import java.util.Date;

public class Job {
    public static final long ID_AUTOMATIC_MAIL_TEMPLATES = 1;
    
    public static final int RUNNING_NO = 0;
    public static final int RUNNING_YES = 1;
    
    private long id;
    private int running;
    private Date lastRunTime;
    private String server;
    private int runInterval;
    private String description;
    private String jobClass;
    private String config;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public int getRunning() {
        return running;
    }

    public void setRunning(int running) {
        this.running = running;
    }

    public Date getLastRunTime() {
        return lastRunTime;
    }

    public void setLastRunTime(Date lastRunTime) {
        this.lastRunTime = lastRunTime;
    }

    public String getServer() {
        return server;
    }

    public void setServer(String server) {
        this.server = server;
    }

    public int getRunInterval() {
        return runInterval;
    }

    public void setRunInterval(int runInterval) {
        this.runInterval = runInterval;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getJobClass() {
        return jobClass;
    }

    public void setJobClass(String jobClass) {
        this.jobClass = jobClass;
    }

    public String getConfig() {
        return config;
    }

    public void setConfig(String config) {
        this.config = config;
    }

    public String toString() {
        String ls = System.getProperty("line.separator");
        return ls + "Job:" + ls
                + super.toString() + ls
                + "id: " + id + ls
                + "running: " + running + ls
                + "lastRunTime: " + lastRunTime + ls
                + "server: " + server + ls
                + "runInterval: " + runInterval + ls
                + "description: " + description + ls
                + "jobClass: " + jobClass + ls
                + "config: " + config + ls;
    }
}