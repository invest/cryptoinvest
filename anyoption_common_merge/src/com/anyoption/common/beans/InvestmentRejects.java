package com.anyoption.common.beans;

import java.util.Date;

public class InvestmentRejects {

	private long userId;
	private long opportunityId;
	private int rejectTypeId;
	private String sessionId;
	private Double realLevel;
	private Double pageLevel;
	private Double wwwLevel;
	private Long amount;
	private Float returnInv;
	private String rejectAdditionalInfo;
	private Date timeCreated;
	private long writerId;
	private int secBetweenInvestments;
	private Double rate;
	private int typeId;
	private int opportunityType;
    private int fromGraph;
    private boolean isRejectedDuringClose;
    private int needToOpen;
    private int needToClose;
    private Float refundInv;
    private long apiExternalUserId;
	
	public InvestmentRejects(long oppId,
								double pageLevel,
								String sessionId,
								long invAmount,
								long userId,
								double userRate,
								float pageOddsWin,
								Float pageOddsLose,
								int choice,
								int oppType,
								long writerId,
								long apiExternalUserId) {
	    this.opportunityId = oppId;
	    this.pageLevel = pageLevel;
	    this.sessionId = sessionId;
	    this.amount = invAmount;
	    this.userId = userId;
	    this.returnInv = pageOddsWin;
	    this.refundInv = pageOddsLose;
	    this.rate = userRate;
	    this.typeId = choice;
	    this.opportunityType = oppType;
	    this.writerId = writerId;
	    this.apiExternalUserId = apiExternalUserId;
	}

	public InvestmentRejects(long oppId,
								double pageLevel1,
								String sessionId1,
								Double invAmount,
								long userId1,
								double userRate,
								float pageOddsWin,
								Float pageOddsLose,
								int choice,
								int opp_type,
								int fromGraph,
								long writerId) {
	    this(oppId,
	    		pageLevel1,
	    		sessionId1,
	    		invAmount,
	    		userId1,
	    		userRate,
	    		pageOddsWin,
	    		pageOddsLose,
	    		choice,
	    		opp_type,
	    		fromGraph,
	    		1,
	    		0,
	    		writerId);
	}

	public InvestmentRejects(long oppId,
								double pageLevel1,
								String sessionId1,
								Double invAmount,
								long userId1,
								double userRate,
								Float pageOddsWin,
								Float pageOddsLose,
								int choice,
								int opportunityType,
								int fromGraph,
								int needToOpen,
								int needToClose,
								long writerId) {
		rejectAdditionalInfo = null;
		realLevel = null;
		wwwLevel = null;

		this.opportunityId = oppId;
	    this.pageLevel = pageLevel1;
	    this.sessionId = sessionId1;
	    this.amount = invAmount.longValue();
	    this.userId = userId1;
	    this.returnInv = pageOddsWin;
	    this.refundInv = pageOddsLose;
	    this.rate = userRate;
	    this.typeId = choice;
	    this.opportunityType = opportunityType;
	    this.fromGraph = fromGraph;
	    this.needToClose = needToClose;
	    this.needToOpen = needToOpen;
	    this.isRejectedDuringClose = false;
	    this.writerId = writerId;
	}

	/**
	 * @return the rate
	 */
	public Double getRate() {
		return rate;
	}
	
	/**
	 * @param rate the rate to set
	 */
	public void setRate(Double rate) {
		this.rate = rate;
	}
	
	/**
	 * @return the amount
	 */
	public Long getAmount() {
		return amount;
	}
	/**
	 * @param amount the amount to set
	 */
	public void setAmount(Long amount) {
		this.amount = amount;
	}
	/**
	 * @return the opportunityId
	 */
	public long getOpportunityId() {
		return opportunityId;
	}
	/**
	 * @param opportunityId the opportunityId to set
	 */
	public void setOpportunityId(long opportunityId) {
		this.opportunityId = opportunityId;
	}
	/**
	 * @return the pageLevel
	 */
	public Double getPageLevel() {
		return pageLevel;
	}
	/**
	 * @param pageLevel the pageLevel to set
	 */
	public void setPageLevel(Double pageLevel) {
		this.pageLevel = pageLevel;
	}
	/**
	 * @return the realLevel
	 */
	public Double getRealLevel() {
		return realLevel;
	}
	/**
	 * @param realLevel the realLevel to set
	 */
	public void setRealLevel(Double realLevel) {
		this.realLevel = realLevel;
	}
	/**
	 * @return the rejectAdditionalInfo
	 */
	public String getRejectAdditionalInfo() {
		return rejectAdditionalInfo;
	}
	/**
	 * @param rejectAdditionalInfo the rejectAdditionalInfo to set
	 */
	public void setRejectAdditionalInfo(String rejectAdditionalInfo) {
		this.rejectAdditionalInfo = rejectAdditionalInfo;
	}
	/**
	 * @return the rejectTypeId
	 */
	public int getRejectTypeId() {
		return rejectTypeId;
	}
	/**
	 * @param rejectTypeId the rejectTypeId to set
	 */
	public void setRejectTypeId(int rejectTypeId) {
		this.rejectTypeId = rejectTypeId;
	}
	/**
	 * @return the returnInv
	 */
	public Float getReturnInv() {
		return returnInv;
	}
	/**
	 * @param returnInv the returnInv to set
	 */
	public void setReturnInv(Float returnInv) {
		this.returnInv = returnInv;
	}
	/**
	 * @return the secBetweenInvestments
	 */
	public int getSecBetweenInvestments() {
		return secBetweenInvestments;
	}
	/**
	 * @param secBetweenInvestments the secBetweenInvestments to set
	 */
	public void setSecBetweenInvestments(int secBetweenInvestments) {
		this.secBetweenInvestments = secBetweenInvestments;
	}
	/**
	 * @return the sessionId
	 */
	public String getSessionId() {
		return sessionId;
	}
	/**
	 * @param sessionId the sessionId to set
	 */
	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}
	/**
	 * @return the timeCreated
	 */
	public Date getTimeCreated() {
		return timeCreated;
	}
	/**
	 * @param timeCreated the timeCreated to set
	 */
	public void setTimeCreated(Date timeCreated) {
		this.timeCreated = timeCreated;
	}
	/**
	 * @return the userId
	 */
	public long getUserId() {
		return userId;
	}
	/**
	 * @param userId the userId to set
	 */
	public void setUserId(long userId) {
		this.userId = userId;
	}
	/**
	 * @return the writerId
	 */
	public long getWriterId() {
		return writerId;
	}
	/**
	 * @param writerId the writerId to set
	 */
	public void setWriterId(long writerId) {
		this.writerId = writerId;
	}
	/**
	 * @return the wwwLevel
	 */
	public Double getWwwLevel() {
		return wwwLevel;
	}
	/**
	 * @param wwwLevel the wwwLevel to set
	 */
	public void setWwwLevel(Double wwwLevel) {
		this.wwwLevel = wwwLevel;
	}

	/**
	 * @return the typeId
	 */
	public int getTypeId() {
		return typeId;
	}

	/**
	 * @param typeId the typeId to set
	 */
	public void setTypeId(int typeId) {
		this.typeId = typeId;
	}

	/**
	 * @return the opportunity_type
	 */
	public int getOpportunityType() {
		return opportunityType;
	}

	/**
	 * @param opportunity_type the opportunity_type to set
	 */
	public void setOpportunityType(int opportunityType) {
		this.opportunityType = opportunityType;
	}

	public boolean isRejectedDuringClose() {
		return isRejectedDuringClose;
	}

	public void setRejectedDuringClose(boolean isRejectedDuringClose) {
		this.isRejectedDuringClose = isRejectedDuringClose;
	}

	public int getNeedToClose() {
		return needToClose;
	}

	public void setNeedToClose(int needToClose) {
		this.needToClose = needToClose;
	}

	public int getNeedToOpen() {
		return needToOpen;
	}

	public void setNeedToOpen(int needToOpen) {
		this.needToOpen = needToOpen;
	}

	/**
	 * @return the fromGraph
	 */
	public int getFromGraph() {
		return fromGraph;
	}

	/**
	 * @param fromGraph the fromGraph to set
	 */
	public void setFromGraph(int fromGraph) {
		this.fromGraph = fromGraph;
	}

    /**
     * @return the refundInv
     */
    public Float getRefundInv() {
        return refundInv;
    }

    /**
     * @param refundInv the refundInv to set
     */
    public void setRefundInv(Float refundInv) {
        this.refundInv = refundInv;
    }

	@Override
	public String toString() {
		return "InvestmentRejects [userId=" + userId + ", opportunityId="
				+ opportunityId + ", rejectTypeId=" + rejectTypeId
				+ ", sessionId=" + sessionId + ", realLevel=" + realLevel
				+ ", pageLevel=" + pageLevel + ", wwwLevel=" + wwwLevel
				+ ", amount=" + amount + ", returnInv=" + returnInv
				+ ", rejectAdditionalInfo=" + rejectAdditionalInfo
				+ ", timeCreated=" + timeCreated + ", writerId=" + writerId
				+ ", secBetweenInvestments=" + secBetweenInvestments
				+ ", rate=" + rate + ", typeId=" + typeId
				+ ", opportunityType=" + opportunityType + ", fromGraph="
				+ fromGraph + ", isRejectedDuringClose="
				+ isRejectedDuringClose + ", needToOpen=" + needToOpen
				+ ", needToClose=" + needToClose + ", refundInv=" + refundInv
				+ ", apiExternalUserId= " + apiExternalUserId
				+ "]";
	}
	
	/**
	 * @return the apiExternalUserId
	 */
	public long getApiExternalUserId() {
		return apiExternalUserId;
	}

	/**
	 * @param apiExternalUserId the apiExternalUserId to set
	 */
	public void setApiExternalUserId(long apiExternalUserId) {
		this.apiExternalUserId = apiExternalUserId;
	}

}