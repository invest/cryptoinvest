/**
 * 
 */
package com.anyoption.common.beans;
import java.io.Serializable;
import org.apache.log4j.Logger;
import org.apache.myfaces.custom.fileupload.UploadedFile;

/**
 * @author pavelt
 *
 */
public class TournamentSkinsLanguages implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8475225324861765377L;
	
	private static final Logger logger = Logger.getLogger(TournamentSkinsLanguages.class);
	private long id;
	private long tournamentId;
	private long tabId;// configuration table for BE tabs
	private String prizeText;
	private String prizeImage;
	private UploadedFile file;
	private String tabLabel;
	private long tabDefLangId;
		
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public long getTournamentId() {
		return tournamentId;
	}
	public void setTournamentId(long tournamentId) {
		this.tournamentId = tournamentId;
	}
	public String getPrizeText() {
		return prizeText;
	}
	public void setPrizeText(String prizeText) {
		this.prizeText = prizeText;
	}
	public String getPrizeImage() {
		return prizeImage;
	}
	public void setPrizeImage(String prizeImage) {
		this.prizeImage = prizeImage;
	}
	
	public String toString(){
		return "TournamentSkinsLanguages [id=" + id + 
							",tournamentId=" + tournamentId + 
							",tabId= " + tabId + 
							",prizeText= "+ prizeText + 
							",tabLabel= "+ tabLabel + 
							",tabDefLangId= "+ tabDefLangId + 
							",prizeImage= " + prizeImage + ", file=" + (file == null ? "" : file.getName()) +"]";
	}
	public long getTabId() {
		return tabId;
	}
	public void setTabId(long tabId) {
		this.tabId = tabId;
	}
	public UploadedFile getFile() {
		return file;
	}
	public void setFile(UploadedFile file) {
		this.file = file;
	}


	public String getTabLabel() {
		return tabLabel;
	}


	public void setTabLabel(String tabLabel) {
		this.tabLabel = tabLabel;
	}


	public long getTabDefLangId() {
		return tabDefLangId;
	}


	public void setTabDefLangId(long tabDefLangId) {
		this.tabDefLangId = tabDefLangId;
	}
}
