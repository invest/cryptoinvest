package com.anyoption.common.beans;

/**
 * @author liors
 *
 */

public class ValidatorParamNIOU {
	private User user;
	private double oppOddsWin;
	private double oppOddsLose;
	private long oppTypeId;
	private long invAmount;
	private double oddsWin;
	private double oddsLose;

	/**
	 * 
	 */
	public ValidatorParamNIOU() {
		
	}
	
	/**
	 * @param user
	 * @param oppOddsWin
	 * @param oppOddsLose
	 * @param oppTypeId
	 * @param invAmount
	 * @param oddsWin
	 * @param oddsLose
	 */
	public ValidatorParamNIOU(User user, double oppOddsWin, double oppOddsLose,
			long oppTypeId, long invAmount, double oddsWin, double oddsLose) {
		super();
		this.user = user;
		this.oppOddsWin = oppOddsWin;
		this.oppOddsLose = oppOddsLose;
		this.oppTypeId = oppTypeId;
		this.invAmount = invAmount;
		this.oddsWin = oddsWin;
		this.oddsLose = oddsLose;
	}

	/**
	 * @return the user
	 */
	public User getUser() {
		return user;
	}
	
	/**
	 * @param user the user to set
	 */
	public void setUser(User user) {
		this.user = user;
	}

	/**
	 * @return the oppOddsWin
	 */
	public double getOppOddsWin() {
		return oppOddsWin;
	}

	/**
	 * @param oppOddsWin the oppOddsWin to set
	 */
	public void setOppOddsWin(double oppOddsWin) {
		this.oppOddsWin = oppOddsWin;
	}

	/**
	 * @return the oppOddsLose
	 */
	public double getOppOddsLose() {
		return oppOddsLose;
	}

	/**
	 * @param oppOddsLose the oppOddsLose to set
	 */
	public void setOppOddsLose(double oppOddsLose) {
		this.oppOddsLose = oppOddsLose;
	}

	/**
	 * @return the oppTypeId
	 */
	public long getOppTypeId() {
		return oppTypeId;
	}

	/**
	 * @param oppTypeId the oppTypeId to set
	 */
	public void setOppTypeId(long oppTypeId) {
		this.oppTypeId = oppTypeId;
	}

	/**
	 * @return the invAmount
	 */
	public long getInvAmount() {
		return invAmount;
	}

	/**
	 * @param invAmount the invAmount to set
	 */
	public void setInvAmount(long invAmount) {
		this.invAmount = invAmount;
	}

	/**
	 * @return the oddsWin
	 */
	public double getOddsWin() {
		return oddsWin;
	}

	/**
	 * @param oddsWin the oddsWin to set
	 */
	public void setOddsWin(double oddsWin) {
		this.oddsWin = oddsWin;
	}

	/**
	 * @return the oddsLose
	 */
	public double getOddsLose() {
		return oddsLose;
	}

	/**
	 * @param oddsLose the oddsLose to set
	 */
	public void setOddsLose(double oddsLose) {
		this.oddsLose = oddsLose;
	}
	
	@Override
	public String toString() {
		return "ValidatorParamNIOU [userID=" + user.getId() + ", oppOddsWin="
				+ oppOddsWin + ", oppOddsLose=" + oppOddsLose + ", oppTypeId="
				+ oppTypeId + ", invAmount=" + invAmount + ", oddsWin="
				+ oddsWin + ", oddsLose=" + oddsLose + "]";
	}
}
