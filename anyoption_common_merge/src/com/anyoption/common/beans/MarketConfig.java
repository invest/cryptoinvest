/**
 *
 */
package com.anyoption.common.beans;

/**
 * @author pavelhe
 *
 */
public class MarketConfig implements java.io.Serializable {

	private static final long serialVersionUID = -7459468104488005582L;
	private String timeZone;
	private boolean valuesCheckDisable; // TODO what is this?

	private int hourLevelFormulaId;
	private String hourLevelFormulaClass;
	private String hourLevelFormulaParams;
	private int hourClosingFormulaId;
	private String hourClosingFormulaClass;
	private String hourClosingFormulaParams;
	private int dayClosingFormulaId;
	private String dayClosingFormulaClass;
	private String dayClosingFormulaParams;
	private Integer longTermFormulaId;
	private String longTermFormulaClass;
	private String longTermFormulaParams;
	private int realLevelFormulaId;
	private String realLevelFormulaClass;
	private String realLevelFormulaParams;
	private String marketParams;
	private String marketDisableConditions;

	/**
	 * @return the timeZone
	 */
	public String getTimeZone() {
		return timeZone;
	}

	/**
	 * @param timeZone the timeZone to set
	 */
	public void setTimeZone(String timeZone) {
		this.timeZone = timeZone;
	}

	/**
	 * @return the valuesCheckDisable
	 */
	public boolean isValuesCheckDisable() {
		return valuesCheckDisable;
	}

	/**
	 * @param valuesCheckDisable the valuesCheckDisable to set
	 */
	public void setValuesCheckDisable(boolean valuesCheckDisable) {
		this.valuesCheckDisable = valuesCheckDisable;
	}
	
	public int getHourLevelFormulaId() {
		return hourLevelFormulaId;
	}

	
	public void setHourLevelFormulaId(int hourLevelFormulaId) {
		this.hourLevelFormulaId = hourLevelFormulaId;
	}

	/**
	 * @return the hourLevelFormulaClass
	 */
	public String getHourLevelFormulaClass() {
		return hourLevelFormulaClass;
	}

	/**
	 * @param hourLevelFormulaClass the hourLevelFormulaClass to set
	 */
	public void setHourLevelFormulaClass(String hourLevelFormulaClass) {
		this.hourLevelFormulaClass = hourLevelFormulaClass;
	}

	/**
	 * @return the hourLevelFormulaParams
	 */
	public String getHourLevelFormulaParams() {
		return hourLevelFormulaParams;
	}

	/**
	 * @param hourLevelFormulaParams the hourLevelFormulaParams to set
	 */
	public void setHourLevelFormulaParams(String hourLevelFormulaParams) {
		this.hourLevelFormulaParams = hourLevelFormulaParams;
	}

	public int getHourClosingFormulaId() {
		return hourClosingFormulaId;
	}

	
	public void setHourClosingFormulaId(int hourClosingFormulaId) {
		this.hourClosingFormulaId = hourClosingFormulaId;
	}

	/**
	 * @return the hourClosingFormulaClass
	 */
	public String getHourClosingFormulaClass() {
		return hourClosingFormulaClass;
	}

	/**
	 * @param hourClosingFormulaClass the hourClosingFormulaClass to set
	 */
	public void setHourClosingFormulaClass(String hourClosingFormulaClass) {
		this.hourClosingFormulaClass = hourClosingFormulaClass;
	}

	/**
	 * @return the hourClosingFormulaParams
	 */
	public String getHourClosingFormulaParams() {
		return hourClosingFormulaParams;
	}

	/**
	 * @param hourClosingFormulaParams the hourClosingFormulaParams to set
	 */
	public void setHourClosingFormulaParams(String hourClosingFormulaParams) {
		this.hourClosingFormulaParams = hourClosingFormulaParams;
	}

	
	public int getDayClosingFormulaId() {
		return dayClosingFormulaId;
	}

	
	public void setDayClosingFormulaId(int dayClosingFormulaId) {
		this.dayClosingFormulaId = dayClosingFormulaId;
	}

	/**
	 * @return the dayClosingFormulaClass
	 */
	public String getDayClosingFormulaClass() {
		return dayClosingFormulaClass;
	}

	/**
	 * @param dayClosingFormulaClass the dayClosingFormulaClass to set
	 */
	public void setDayClosingFormulaClass(String dayClosingFormulaClass) {
		this.dayClosingFormulaClass = dayClosingFormulaClass;
	}

	/**
	 * @return the dayClosingFormulaParams
	 */
	public String getDayClosingFormulaParams() {
		return dayClosingFormulaParams;
	}

	/**
	 * @param dayClosingFormulaParams the dayClosingFormulaParams to set
	 */
	public void setDayClosingFormulaParams(String dayClosingFormulaParams) {
		this.dayClosingFormulaParams = dayClosingFormulaParams;
	}

	
	public Integer getLongTermFormulaId() {
		return longTermFormulaId;
	}

	
	public void setLongTermFormulaId(Integer longTermFormulaId) {
		this.longTermFormulaId = longTermFormulaId;
	}

	/**
	 * @return the longTermFormulaClass
	 */
	public String getLongTermFormulaClass() {
		return longTermFormulaClass;
	}

	/**
	 * @param longTermFormulaClass the longTermFormulaClass to set
	 */
	public void setLongTermFormulaClass(String longTermFormulaClass) {
		this.longTermFormulaClass = longTermFormulaClass;
	}

	/**
	 * @return the longTermFormulaParams
	 */
	public String getLongTermFormulaParams() {
		return longTermFormulaParams;
	}

	/**
	 * @param longTermFormulaParams the longTermFormulaParams to set
	 */
	public void setLongTermFormulaParams(String longTermFormulaParams) {
		this.longTermFormulaParams = longTermFormulaParams;
	}

	
	public int getRealLevelFormulaId() {
		return realLevelFormulaId;
	}

	
	public void setRealLevelFormulaId(int realLevelFormulaId) {
		this.realLevelFormulaId = realLevelFormulaId;
	}

	/**
	 * @return the realLevelFormulaClass
	 */
	public String getRealLevelFormulaClass() {
		return realLevelFormulaClass;
	}

	/**
	 * @param realLevelFormulaClass the realLevelFormulaClass to set
	 */
	public void setRealLevelFormulaClass(String realLevelFormulaClass) {
		this.realLevelFormulaClass = realLevelFormulaClass;
	}

	/**
	 * @return the realLevelFormulaParams
	 */
	public String getRealLevelFormulaParams() {
		return realLevelFormulaParams;
	}

	/**
	 * @param realLevelFormulaParams the realLevelFormulaParams to set
	 */
	public void setRealLevelFormulaParams(String realLevelFormulaParams) {
		this.realLevelFormulaParams = realLevelFormulaParams;
	}

	public String getMarketParams() {
		return marketParams;
	}

	public void setMarketParams(String marketParams) {
		this.marketParams = marketParams;
	}

	public String getMarketDisableConditions() {
		return marketDisableConditions;
	}

	public void setMarketDisableConditions(String marketDisableConditions) {
		this.marketDisableConditions = marketDisableConditions;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("MarketConfig [timeZone=").append(timeZone)
				.append(", valuesCheckDisable=").append(valuesCheckDisable)
				.append(", hourLevelFormulaId=").append(hourLevelFormulaId)
				.append(", hourLevelFormulaClass=").append(hourLevelFormulaClass)
				.append(", hourLevelFormulaParams=").append(hourLevelFormulaParams)
				.append(", hourClosingFormulaId=").append(hourClosingFormulaId)
				.append(", hourClosingFormulaClass=").append(hourClosingFormulaClass)
				.append(", hourClosingFormulaParams=").append(hourClosingFormulaParams)
				.append(", dayClosingFormulaId=").append(dayClosingFormulaId)
				.append(", dayClosingFormulaClass=").append(dayClosingFormulaClass)
				.append(", dayClosingFormulaParams=").append(dayClosingFormulaParams)
				.append(", longTermFormulaId=").append(longTermFormulaId)
				.append(", longTermFormulaClass=").append(longTermFormulaClass)
				.append(", longTermFormulaParams=").append(longTermFormulaParams)
				.append(", realLevelFormulaId=").append(realLevelFormulaId)
				.append(", realLevelFormulaClass=").append(realLevelFormulaClass)
				.append(", realLevelFormulaParams=").append(realLevelFormulaParams)
				.append(", marketParams=").append(marketParams)
				.append(", marketDisableConditions=").append(marketDisableConditions)
				.append("]");
		return builder.toString();
	}
}