package com.anyoption.common.beans;

public class IssuePriority implements java.io.Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -6310165240634944195L;
	private long id;
	private String name;

	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * Constructs a <code>String</code> with all attributes
	 * in name = value format.
	 *
	 * @return a <code>String</code> representation
	 * of this object.
	 */
	public String toString()
	{
	    final String TAB = " \n ";

	    String retValue = "";

	    retValue = "IssuePriority ( "
	        + super.toString() + TAB
	        + "id = " + this.id + TAB
	        + "name = " + this.name + TAB
	        + " )";

	    return retValue;
	}
}