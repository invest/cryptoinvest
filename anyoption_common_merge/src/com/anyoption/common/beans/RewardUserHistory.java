package com.anyoption.common.beans;

import java.util.Date;

/**
 * @author liors
 *
 */
public class RewardUserHistory implements java.io.Serializable {
	private static final long serialVersionUID = -7847870889912697418L;
	private long userId;
	private int rewardActionId;
	private long referenceId;
	private int rewardHistoryTypeId;
	private int writerId;
	private int rewardTierId;
	private double balanceExperiencePoints;
	private int rewardActionTypeId;
	private String comment;
	private double amountExperiencePoints;
	private String rewardActionName;
	private String rewardHistoryTypeName;
	private Date timeCreated; 
				
	public RewardUserHistory() {		
	
	}
	
	/**
	 * @param userId
	 * @param rewardHistoryTypeId
	 * @param writerId
	 */
	public RewardUserHistory(long userId, int rewardHistoryTypeId, int writerId, double balanceExperiencePoints, int rewardTierId) {
		this.userId = userId;
		this.rewardHistoryTypeId = rewardHistoryTypeId;
		this.writerId = writerId;
		this.rewardActionId = 0;
		this.referenceId = 0;
		this.rewardTierId = rewardTierId;
		this.balanceExperiencePoints = balanceExperiencePoints;
		this.rewardActionTypeId = 0;
		this.comment = "";
		this.amountExperiencePoints = 0;
	}
	
	/**
	 * @param userId
	 * @param rewardActionId
	 * @param referenceId
	 * @param rewardHistoryTypeId
	 * @param writerId
	 * @param rewardTierId
	 * @param balanceExperiencePoints
	 * @param rewardActionTypeId
	 * @param comment
	 * @param amountExperiencePoints
	 */
	public RewardUserHistory(long userId, int rewardActionId, long referenceId, int rewardHistoryTypeId, int writerId, 
			int rewardTierId, double balanceExperiencePoints, int rewardActionTypeId, String comment, double amountExperiencePoints) {
		
		this.userId = userId;
		this.rewardActionId = rewardActionId;
		this.referenceId = referenceId;
		this.rewardHistoryTypeId = rewardHistoryTypeId;
		this.writerId = writerId;
		this.rewardTierId = rewardTierId;
		this.balanceExperiencePoints = balanceExperiencePoints;
		this.rewardActionTypeId = rewardActionTypeId;
		this.comment = comment;
		this.amountExperiencePoints = amountExperiencePoints;
	}

	/**
	 * @return the userId
	 */
	public long getUserId() {
		return userId;
	}
	/**
	 * @param userId the userId to set
	 */
	public void setUserId(long userId) {
		this.userId = userId;
	}
	/**
	 * @return the referenceId
	 */
	public long getReferenceId() {
		return referenceId;
	}
	/**
	 * @param referenceId the referenceId to set
	 */
	public void setReferenceId(long referenceId) {
		this.referenceId = referenceId;
	}
	/**
	 * @return the rewardActionId
	 */
	public int getRewardActionId() {
		return rewardActionId;
	}
	/**
	 * @param rewardActionId the rewardActionId to set
	 */
	public void setRewardActionId(int rewardActionId) {
		this.rewardActionId = rewardActionId;
	}
	/**
	 * @return the rewardHistoryTypeId
	 */
	public int getRewardHistoryTypeId() {
		return rewardHistoryTypeId;
	}
	/**
	 * @param rewardHistoryTypeId the rewardHistoryTypeId to set
	 */
	public void setRewardHistoryTypeId(int rewardHistoryTypeId) {
		this.rewardHistoryTypeId = rewardHistoryTypeId;
	}
	/**
	 * @return the writerId
	 */
	public int getWriterId() {
		return writerId;
	}
	/**
	 * @param writerId the writerId to set
	 */
	public void setWriterId(int writerId) {
		this.writerId = writerId;
	}
	/**
	 * @return the rewardTierId
	 */
	public int getRewardTierId() {
		return rewardTierId;
	}
	/**
	 * @param rewardTierId the rewardTierId to set
	 */
	public void setRewardTierId(int rewardTierId) {
		this.rewardTierId = rewardTierId;
	}

	/**
	 * @return the rewardActionTypeId
	 */
	public int getRewardActionTypeId() {
		return rewardActionTypeId;
	}
	/**
	 * @param rewardActionTypeId the rewardActionTypeId to set
	 */
	public void setRewardActionTypeId(int rewardActionTypeId) {
		this.rewardActionTypeId = rewardActionTypeId;
	}
	/**
	 * @return the comment
	 *//*
	public StringBuilder getComment() {
		return comment;
	}*/
	/**
	 * @param comment the comment to set
	 */
	public void setComment(String comment) {
		this.comment = comment;
	}
	
	/**
	 * @return comment
	 */
	public String getComment() {
		return comment;
	}

	public double getBalanceExperiencePoints() {
		return balanceExperiencePoints;
	}

	public void setBalanceExperiencePoints(double balanceExperiencePoints) {
		this.balanceExperiencePoints = balanceExperiencePoints;
	}

	public double getAmountExperiencePoints() {
		return amountExperiencePoints;
	}

	public void setAmountExperiencePoints(double amountExperiencePoints) {
		this.amountExperiencePoints = amountExperiencePoints;
	}
	
	/**
	 * @return the rewardActionName
	 */
	public String getRewardActionName() {
		return rewardActionName;
	}

	/**
	 * @param rewardActionName the rewardActionName to set
	 */
	public void setRewardActionName(String rewardActionName) {
		this.rewardActionName = rewardActionName;
	}
	
	/**
	 * @return the rewardHistoryTypeName
	 */
	public String getRewardHistoryTypeName() {
		return rewardHistoryTypeName;
	}

	/**
	 * @param rewardHistoryTypeName the rewardHistoryTypeName to set
	 */
	public void setRewardHistoryTypeName(String rewardHistoryTypeName) {
		this.rewardHistoryTypeName = rewardHistoryTypeName;
	}
	
	/**
	 * @return the timeCreated
	 */
	public Date getTimeCreated() {
		return timeCreated;
	}

	/**
	 * @param timeCreated the timeCreated to set
	 */
	public void setTimeCreated(Date timeCreated) {
		this.timeCreated = timeCreated;
	}
	
	@Override
	public String toString() {
		return "RewardUserHistory [userId=" + userId + ", rewardActionId="
				+ rewardActionId + ", referenceId=" + referenceId
				+ ", rewardHistoryTypeId=" + rewardHistoryTypeId
				+ ", writerId=" + writerId + ", rewardTierId=" + rewardTierId
				+ ", balanceExperiencePoints=" + balanceExperiencePoints
				+ ", rewardActionTypeId=" + rewardActionTypeId + ", comment="
				+ comment + ", amountExperiencePoints="
				+ amountExperiencePoints + "]";
	}
}
