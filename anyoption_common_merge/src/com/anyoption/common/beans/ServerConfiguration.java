package com.anyoption.common.beans;

import com.anyoption.common.util.CommonUtil;
import com.anyoption.common.util.ConstantsBase;

/**
 * @author liors
 * Singleton class to set one instance of server configuration.
 * This class will represent all server details.
 *
 */

public class ServerConfiguration {
	private static volatile ServerConfiguration instance;
	private String serverName = ConstantsBase.EMPTY_STRING;
	
	/**
	 * Singleton Constructor.
	 */
	private ServerConfiguration() {
		serverName = CommonUtil.getServerNameUnix();	
	} 

	/**
	 * @return
	 */
	public static synchronized ServerConfiguration getInstance() {
		if (instance == null) {
			instance = new ServerConfiguration();
		}
		return instance;
	}

	/**
	 * @return
	 */
	public String getServerName() {
		return serverName;
	}
}