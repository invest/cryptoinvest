package com.anyoption.common.beans.base;

import java.util.Date;



public class MarketingTracking implements java.io.Serializable {
	

	private static final long serialVersionUID = 1657430793485404851L;
	public static final String EMPTY_STRING = "";
	public static final String NOT_AVAILABLE = "N/A";

	private long id;
	private String mId;
	private long combinationId;
	private Date timeStatic;
	private String httpReferer;
	private String dyanmicParameter;
    private String aff_sub1;
    private String aff_sub2;
    private String aff_sub3;
	private String utmSource;
	
	private long combinationIdDynamic;
	private Date timeDynamic;
	private String httpRefererDynamic;
	private String dyanmicParameterDynamic;
	private String utmSourceDynamic;
	private long contactId;
	private long userId;
	private long marketingTrackingActivityId;
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	/**
	 * @return the combinationId
	 */
	public long getCombinationId() {
		return combinationId;
	}
	/**
	 * @param combinationId the combinationId to set
	 */
	public void setCombinationId(long combinationId) {
		this.combinationId = combinationId;
	}
	/**
	 * @return the timeStatic
	 */
	public Date getTimeStatic() {
		return timeStatic;
	}
	/**
	 * @param timeStatic the timeStatic to set
	 */
	public void setTimeStatic(Date timeStatic) {
		this.timeStatic = timeStatic;
	}
	/**
	 * @return the httpReferer
	 */
	public String getHttpReferer() {
		return httpReferer;
	}
	/**
	 * @param httpReferer the httpReferer to set
	 */
	public void setHttpReferer(String httpReferer) {
		this.httpReferer = httpReferer;
	}
	/**
	 * @return the timeDynamic
	 */
	public Date getTimeDynamic() {
		return timeDynamic;
	}
	/**
	 * @param timeDynamic the timeDynamic to set
	 */
	public void setTimeDynamic(Date timeDynamic) {
		this.timeDynamic = timeDynamic;
	}
	/**
	 * @return the dyanmicParameter
	 */
	public String getDyanmicParameter() {
		return dyanmicParameter;
	}
	/**
	 * @param dyanmicParameter the dyanmicParameter to set
	 */
	public void setDyanmicParameter(String dyanmicParameter) {
	    if(dyanmicParameter == null || dyanmicParameter.contains("null") || dyanmicParameter.contains("undefined")){
            this.dyanmicParameter = EMPTY_STRING;
        } else {
            this.dyanmicParameter = dyanmicParameter;
        }
	}
	/**
	 * @return the contactId
	 */
	public long getContactId() {
		return contactId;
	}
	/**
	 * @param contactId the contactId to set
	 */
	public void setContactId(long contactId) {
		this.contactId = contactId;
	}
	/**
	 * @return the userId
	 */
	public long getUserId() {
		return userId;
	}
	/**
	 * @param userId the userId to set
	 */
	public void setUserId(long userId) {
		this.userId = userId;
	}
	/**
	 * @return the marketingTrackingActivityId
	 */
	public long getMarketingTrackingActivityId() {
		return marketingTrackingActivityId;
	}
	/**
	 * @param marketingTrackingActivityId the marketingTrackingActivityId to set
	 */
	public void setMarketingTrackingActivityId(long marketingTrackingActivityId) {
		this.marketingTrackingActivityId = marketingTrackingActivityId;
	}
	/**
	 * @return the mId
	 */
	public String getmId() {
		return mId;
	}
	/**
	 * @param mId the mId to set
	 */
	public void setmId(String mId) {
		this.mId = mId;
	}
	/**
	 * @return the combinationIdDynamic
	 */
	public long getCombinationIdDynamic() {
		return combinationIdDynamic;
	}
	/**
	 * @param combinationIdDynamic the combinationIdDynamic to set
	 */
	public void setCombinationIdDynamic(long combinationIdDynamic) {
		this.combinationIdDynamic = combinationIdDynamic;
	}
	/**
	 * @return the httpRefererDynamic
	 */
	public String getHttpRefererDynamic() {
		return httpRefererDynamic;
	}
	/**
	 * @param httpRefererDynamic the httpRefererDynamic to set
	 */
	public void setHttpRefererDynamic(String httpRefererDynamic) {
		this.httpRefererDynamic = httpRefererDynamic;
	}
	/**
	 * @return the dyanmicParameterDynamic
	 */
	public String getDyanmicParameterDynamic() {
		return dyanmicParameterDynamic;
	}
	/**
	 * @param dyanmicParameterDynamic the dyanmicParameterDynamic to set
	 */
	public void setDyanmicParameterDynamic(String dyanmicParameterDynamic) {
		if (dyanmicParameterDynamic == null || dyanmicParameterDynamic.contains("null") || dyanmicParameterDynamic.contains("undefined")){
			this.dyanmicParameterDynamic = EMPTY_STRING;
		} else {
			this.dyanmicParameterDynamic = dyanmicParameterDynamic;   
		}
	}

	public String getUtmSource() {
		return utmSource;
	}

	public void setUtmSource(String utmSource) {

		if (utmSource != null) {
			if (utmSource.contains("null") || utmSource.contains("undefined")) {
				this.utmSource = NOT_AVAILABLE;
			} else {
				this.utmSource = utmSource;
			}
		} else {
			this.utmSource = "";
		}
	}

	public String getUtmSourceDynamic() {
		return utmSourceDynamic;
	}

	public void setUtmSourceDynamic(String utmSourceDynamic) {

		if (utmSourceDynamic != null) {
			if (utmSourceDynamic.contains("null")
					|| utmSourceDynamic.contains("undefined")) {
				this.utmSourceDynamic = NOT_AVAILABLE;
			} else {
				this.utmSourceDynamic = utmSourceDynamic;
			}
		} else {
			this.utmSourceDynamic = "";
		}
	}
	public String getAff_sub1() {
		return aff_sub1;
	}
	public void setAff_sub1(String aff_sub) {
		if (aff_sub == null || aff_sub.contains("null") || aff_sub.contains("undefined")){
			this.aff_sub1 = EMPTY_STRING;
		} else {
			this.aff_sub1 = aff_sub;
		}
	}
	public String getAff_sub2() {
		return aff_sub2;
	}
	public void setAff_sub2(String aff_sub) {
		if (aff_sub == null || aff_sub.contains("null") || aff_sub.contains("undefined")){
			this.aff_sub2 = EMPTY_STRING;
		} else {
			this.aff_sub2 = aff_sub;
		}
	}
	
	public String getAff_sub3() {
		return aff_sub3;
	}
	
	public void setAff_sub3(String aff_sub) {
		if (aff_sub == null || aff_sub.contains("null") || aff_sub.contains("undefined")){
			this.aff_sub3 = EMPTY_STRING;
		} else {
			this.aff_sub3 = aff_sub;
		}
	}
	@Override
	public String toString() {
		return "MarketingTracking [id=" + id + ", mId=" + mId
				+ ", combinationId=" + combinationId + ", timeStatic="
				+ timeStatic + ", httpReferer=" + httpReferer
				+ ", dyanmicParameter=" + dyanmicParameter + ", aff_sub1="
				+ aff_sub1 + ", aff_sub2=" + aff_sub2 + ", aff_sub3="
				+ aff_sub3 + ", utmSource=" + utmSource
				+ ", combinationIdDynamic=" + combinationIdDynamic
				+ ", timeDynamic=" + timeDynamic + ", httpRefererDynamic="
				+ httpRefererDynamic + ", dyanmicParameterDynamic="
				+ dyanmicParameterDynamic + ", utmSourceDynamic="
				+ utmSourceDynamic + ", contactId=" + contactId + ", userId="
				+ userId + ", marketingTrackingActivityId="
				+ marketingTrackingActivityId + "]";
	}
}


