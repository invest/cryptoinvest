package com.anyoption.common.beans.base;

import java.io.Serializable;

/**
 * @author eranl
 *
 */
public class AssetResult implements Serializable {

	private static final long serialVersionUID = 8271895534485828699L;
	private long marketId;
	private int hitRate;
	private String marketName;
	
	/**
	 * @return the marketId
	 */
	public long getMarketId() {
		return marketId;
	}
	/**
	 * @param marketId the marketId to set
	 */
	public void setMarketId(long marketId) {
		this.marketId = marketId;
	}
	/**
	 * @return the hitRate
	 */
	public int getHitRate() {
		return hitRate;
	}
	/**
	 * @param hitRate the hitRate to set
	 */
	public void setHitRate(int hitRate) {
		this.hitRate = hitRate;
	}		
	/**
	 * @return the marketName
	 */
	public String getMarketName() {
		return marketName;
	}
	/**
	 * @param marketName the marketName to set
	 */
	public void setMarketName(String marketName) {
		this.marketName = marketName;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "AssetResult [marketId=" + marketId + ", hitRate=" + hitRate
				+ "]";
	}
	
}