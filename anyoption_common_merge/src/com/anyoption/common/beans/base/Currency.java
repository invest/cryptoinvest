/**
 *
 */
package com.anyoption.common.beans.base;

import java.io.Serializable;

/**
 * @author kirilim
 *
 */
public class Currency implements Serializable {

	private static final long serialVersionUID = -3101601606676240957L;

    public static final long CURRENCY_ILS_ID = 1;
    public static final long CURRENCY_USD_ID = 2;
    public static final long CURRENCY_EUR_ID = 3;
    public static final long CURRENCY_GBP_ID = 4;
    public static final long CURRENCY_TRY_ID = 5;
    public static final long CURRENCY_RUB_ID = 6;
    public static final long CURRENCY_CNY_ID = 7;
    public static final long CURRENCY_KRW_ID = 8;
    public static final long CURRENCY_SEK_ID = 9;
    public static final long CURRENCY_AUD_ID = 10;
    public static final long CURRENCY_ZAR_ID = 11;
    public static final long CURRENCY_CZK_ID = 12;
    public static final long CURRENCY_PLN_ID = 13;

	protected long id;
	protected String symbol;
	protected String defaultSymbol;
	protected int isLeftSymbol;
	protected String code;
	protected String nameKey;
	protected String displayName;
	protected int decimalPointDigits;

	public Currency() {
	}

	public Currency(Currency c) {
		this.id = c.id;
		this.symbol=c.symbol;
		this.defaultSymbol = c.defaultSymbol;
		this.isLeftSymbol=c.isLeftSymbol;
		this.code=c.code;
		this.nameKey=c.nameKey;
		this.displayName=c.displayName;
		this.decimalPointDigits = c.decimalPointDigits;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public int getIsLeftSymbol() {
		return isLeftSymbol;
	}

	public void setIsLeftSymbol(int isLeftSymbol) {
		this.isLeftSymbol = isLeftSymbol;
	}

	public boolean getIsLeftSymbolBool() {
		return ( isLeftSymbol == 1 ? true : false );
	}

	public String getNameKey() {
		return nameKey;
	}

	public void setNameKey(String nameKey) {
		this.nameKey = nameKey;
	}

	public String getSymbol() {
		return symbol;
	}

	public void setSymbol(String symbol) {
		this.symbol = symbol;
	}

    /**
	 * @return the defaultSymbol
	 */
	public String getDefaultSymbol() {
		return defaultSymbol;
	}

	/**
	 * @param defaultSymbol the defaultSymbol to set
	 */
	public void setDefaultSymbol(String defaultSymbol) {
		this.defaultSymbol = defaultSymbol;
	}

	public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

	/**
	 * @return the decimalPointDigits
	 */
	public int getDecimalPointDigits() {
		return decimalPointDigits;
	}

	/**
	 * @param decimalPointDigits the decimalPointDigits to set
	 */
	public void setDecimalPointDigits(int decimalPointDigits) {
		this.decimalPointDigits = decimalPointDigits;
	}

	@Override
	public String toString() {
        String ls = System.getProperty("line.separator");
	    return ls + "Currency" + ls
	        + super.toString() + ls
	        + "id: " + id + ls
	        + "symbol: " + symbol + ls
	        + "defaultSymbol: " + defaultSymbol + ls
	        + "isLeftSymbol: " + isLeftSymbol + ls
	        + "code: " + code + ls
	        + "nameKey: " + nameKey + ls
            + "displayName: " + displayName + ls
            + "decimalPointDigits: " + decimalPointDigits + ls;
	}


}