package com.anyoption.common.beans.base;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

public class MarketAssetIndexInfo implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private long marketId;
	private long skinId;
	private String feedName;
	private String marketDescription;
	private String additionalText;
	private String pageTitle;
	private String pageKeyWords;
	private String pageDescription;
	private String marketDescriptionPage;
	private List<ExpiryFormulaCalculation> expiryFormulaCalculations;
	private boolean is24h7;
	private boolean isFullDay;
	private long opportunityType;
	private Date startTime;
	private Date endTime;
	private Date breakStartTime;
	private Date breakEndTime;
	private String tradingDays;
	
	public long getMarketId() {
		return marketId;
	}
	
	public void setMarketId(long marketId) {
		this.marketId = marketId;
	}
	
	public long getSkinId() {
		return skinId;
	}
	
	public void setSkinId(long skinId) {
		this.skinId = skinId;
	}
	
	public String getMarketDescription() {
		return marketDescription;
	}
	
	public void setMarketDescription(String marketDescription) {
		this.marketDescription = marketDescription;
	}
	
	public String getFeedName() {
		return feedName;
	}

	public void setFeedName(String feedName) {
		this.feedName = feedName;
	}

	public String getAdditionalText() {
		return additionalText;
	}
	
	public void setAdditionalText(String additionalText) {
		this.additionalText = additionalText;
	}
	
	public String getPageTitle() {
		return pageTitle;
	}
	
	public void setPageTitle(String pageTitle) {
		this.pageTitle = pageTitle;
	}
	
	public String getPageKeyWords() {
		return pageKeyWords;
	}
	
	public void setPageKeyWords(String pageKeyWords) {
		this.pageKeyWords = pageKeyWords;
	}
	
	public String getPageDescription() {
		return pageDescription;
	}
	
	public void setPageDescription(String pageDescription) {
		this.pageDescription = pageDescription;
	}
	
	public String getMarketDescriptionPage() {
		return marketDescriptionPage;
	}
	
	public void setMarketDescriptionPage(String marketDescriptionPage) {
		this.marketDescriptionPage = marketDescriptionPage;
	}
	
	public List<ExpiryFormulaCalculation> getExpiryFormulaCalculations() {
		return expiryFormulaCalculations;
	}
	
	public void setExpiryFormulaCalculations(List<ExpiryFormulaCalculation> expiryFormulaCalculations) {
		this.expiryFormulaCalculations = expiryFormulaCalculations;
	}
	
	public boolean isIs24h7() {
		return is24h7;
	}
	
	public void setIs24h7(boolean is24h7) {
		this.is24h7 = is24h7;
	}
	
	public long getOpportunityType() {
		return opportunityType;
	}
	
	public void setOpportunityType(long opportunityType) {
		this.opportunityType = opportunityType;
	}

	public String getTradingDays() {
		return tradingDays;
	}

	public void setTradingDays(String tradingDays) {
		this.tradingDays = tradingDays;
	}

	public Date getStartTime() {
		return startTime;
	}

	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}

	public Date getEndTime() {
		return endTime;
	}

	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}

	public Date getBreakStartTime() {
		return breakStartTime;
	}

	public void setBreakStartTime(Date breakStartTime) {
		this.breakStartTime = breakStartTime;
	}

	public Date getBreakEndTime() {
		return breakEndTime;
	}

	public void setBreakEndTime(Date breakEndTime) {
		this.breakEndTime = breakEndTime;
	}

	public boolean isFullDay() {
		return isFullDay;
	}

	public void setFullDay(boolean isFullDay) {
		this.isFullDay = isFullDay;
	}
}
