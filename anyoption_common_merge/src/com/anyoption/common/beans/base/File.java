package com.anyoption.common.beans.base;

import java.io.Serializable;

/**
 * @author kirilim
 */
public class File implements Serializable {

	private static final long serialVersionUID = 4772836970142188890L;

	public static final int STATUS_REQUESTED = 1;
	public static final int STATUS_IN_PROGRESS = 2;
	public static final int STATUS_DONE = 3;
	public static final int STATUS_REJECTED = 4;
	public static final int STATUS_NOT_NEEDED = 5;

	private String fileName;
	private long fileTypeId;
	private String fileNameForClient;
	private boolean hideField;
	private boolean isApproved;
	private long ccId;
	private String ccName;
	private String ccType;
	private int statusMsgId;
	private boolean isLock;	
	private String rejetReasonType;
	protected long fileStatusId;
	private boolean isControlApproved;
	private long transactionId;
	

	public String getFileNameForClient() {
		return fileNameForClient;
	}

	public void setFileNameForClient(String fileNameForClient) {
		this.fileNameForClient = fileNameForClient;
	}

	public long getFileStatusId() {
		return fileStatusId;
	}

	public void setFileStatusId(long fileStatusId) {
		this.fileStatusId = fileStatusId;
	}

	public boolean isHideField() {
		return hideField;
	}

	public void setHideField(boolean hideField) {
		this.hideField = hideField;
	}

	public boolean isApproved() {
		return isApproved;
	}

	public void setApproved(boolean isApproved) {
		this.isApproved = isApproved;
	}

	public boolean isControlApproved() {
		return isControlApproved;
	}

	public void setControlApproved(boolean isControlApproved) {
		this.isControlApproved = isControlApproved;
	}

	public long getCcId() {
		return ccId;
	}

	public void setCcId(long ccId) {
		this.ccId = ccId;
	}

	public String getCcName() {
		return ccName;
	}

	public void setCcName(String ccName) {
		this.ccName = ccName;
	}

	public int getStatusMsgId() {
		return statusMsgId;
	}

	public void setStatusMsgId(int statusMsgId) {
		this.statusMsgId = statusMsgId;
	}

	public boolean isLock() {
		return isLock;
	}

	public void setLock(boolean isLock) {
		this.isLock = isLock;
	}
	
	public long getFileTypeId() {
		return fileTypeId;
	}

	public void setFileTypeId(long fileTypeId) {
		this.fileTypeId = fileTypeId;
	}
	
	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public long getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(long transactionId) {
		this.transactionId = transactionId;
	}
	
	public String toString() {
		String ls = System.getProperty("line.separator");
		return ls + "com.anyoption.common.beans.base.File: " + ls
				+ "fileTypeId: " + fileTypeId + ls
				+ "fileName: " + fileName + ls
				+ "fileNameForClient: " + fileNameForClient + ls
				+ "hideField: " + hideField + ls
				+ "isApproved: " + isApproved + ls
				+ "ccId: " + ccId + ls
				+ "ccName: " + ccName + ls
				+ "statusMsgId: " + statusMsgId + ls
				+ "fileStatusId: " + fileStatusId + ls
				+ "isControlApproved: " + isControlApproved + ls
				+ "isLock: " + isLock + ls
				+ "ccName: " + ccName + ls
				+ "transactionId: " + transactionId + ls;
	}

	public String getCcType() {
		return ccType;
	}

	public void setCcType(String ccType) {
		this.ccType = ccType;
	}

	public String getRejetReasonType() {
		return rejetReasonType;
	}

	public void setRejetReasonType(String rejetReasonType) {
		this.rejetReasonType = rejetReasonType;
	}
}