/**
 * 
 */
package com.anyoption.common.beans.base;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * @author pavelhe
 *
 */
public class QuestionnaireGroup implements Serializable {
	
	public static final String REGULATION_MANDATORY_QUESTIONNAIRE_NAME	= "REGULATION_MANDATORY_QUESTIONNAIRE";
	public static final String REGULATION_OPTIONAL_QUESTIONNAIRE_NAME	= "REGULATION_OPTIONAL_QUESTIONNAIRE";
	public static final String REGULATION_CAPITAL_QUESTIONNAIRE_NAME	= "REGULATION_CAPITAL_QUESTIONNAIRE";
	public static final String REGULATION_SINGLE_QUESTIONNAIRE_NAME		= "REGULATION_SINGLE_QUESTIONNAIRE";

	public static final int REGULATION_MANDATORY_QUESTIONNAIRE_ID	= 5;	// REGULATION_MANDATORY_QUESTIONNAIRE
	public static final int REGULATION_OPTIONAL_QUESTIONNAIRE_ID	= 6;	// REGULATION_OPTIONAL_QUESTIONNAIRE
	public static final int REGULATION_CAPITAL_QUESTIONNAIRE_ID		= 7;	// REGULATION_CAPITAL_QUESTIONNAIRE
	public static final int REGULATION_SINGLE_QUESTIONNAIRE_ID		= 8; 	// REGULATION_SINGLE_QUESTIONNAIRE
	


	
	public static final Long WITHDRAW_SURVEY_ID = 9L;

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private long id;
	private String name;
	private boolean active;
	
	private List<QuestionnaireQuestion> questions = new ArrayList<QuestionnaireQuestion>();
	
	/**
	 * @return the id
	 */
	public long getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(long id) {
		this.id = id;
	}
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * @return the active
	 */
	public boolean isActive() {
		return active;
	}
	/**
	 * @param active the active to set
	 */
	public void setActive(boolean active) {
		this.active = active;
	}
	/**
	 * @return the questions
	 */
	public List<QuestionnaireQuestion> getQuestions() {
		return questions;
	}
	/**
	 * @param questions the questions to set
	 */
	public void setQuestions(List<QuestionnaireQuestion> questions) {
		this.questions = questions;
	}
	
	@Override
	public String toString() {
		return "QuestionnaireGroup [id=" + id + ", name=" + name + ", active="
				+ active + ", questions=" + questions + "]";
	}

}
