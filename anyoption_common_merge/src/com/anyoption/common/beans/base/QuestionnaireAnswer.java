/**
 * 
 */
package com.anyoption.common.beans.base;

import java.io.Serializable;

/**
 * @author pavelhe
 *
 */
public class QuestionnaireAnswer implements Serializable {

	
	public enum UserAnswerType { 
		REGULAR, FREETEXT;
		public static UserAnswerType get(int k) {
			switch(k){
				case 0:
					return UserAnswerType.REGULAR;
				case 1:
					return UserAnswerType.FREETEXT;
				default:
					return null;
			}
		}
	};
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private long id;
	private String name;
	private String translation;
	private long questionId;
	private long orderId;
	private Long score;
	private UserAnswerType answerType;

	/**
	 * @return the id
	 */
	public long getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(long id) {
		this.id = id;
	}
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * @return the questionId
	 */
	public long getQuestionId() {
		return questionId;
	}
	/**
	 * @param questionId the questionId to set
	 */
	public void setQuestionId(long questionId) {
		this.questionId = questionId;
	}
	/**
	 * @return the orderId
	 */
	public long getOrderId() {
		return orderId;
	}
	/**
	 * @param orderId the orderId to set
	 */
	public void setOrderId(long orderId) {
		this.orderId = orderId;
	}
	
	public Long getScore() {
		return score;
	}
	public void setScore(Long score) {
		this.score = score;
	}
	public String getTranslation() {
		return translation;
	}
	public void setTranslation(String translation) {
		this.translation = translation;
	}
	public UserAnswerType getAnswerType() {
		return answerType;
	}
	public void setAnswerType(UserAnswerType answerType) {
		this.answerType = answerType;
	}
	@Override
	public String toString() {
		return "QuestionnaireAnswer [id=" + id + ", name=" + name + ", translation=" + translation + ", questionId="
				+ questionId + ", orderId=" + orderId + ", score=" + score + ", answerType=" + answerType + "]";
	}
}
