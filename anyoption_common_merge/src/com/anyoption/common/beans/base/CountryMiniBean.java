/**
 * 
 */
package com.anyoption.common.beans.base;

import java.io.Serializable;
import java.text.CollationKey;

import com.anyoption.common.enums.CountryStatus;

/**
 * This bean is used for serialization to Json for web use
 * 
 * @author kirilim
 *
 */
public class CountryMiniBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1205435821050379644L;
	public static final int COUNTRY_PHONE_CODE_PREFIX_LENGTH = 3;
	public static final long COUNTRY_ID_BARBADOS = 20;
	// this shouldn't be serialized, because we already have it as a key in the Json map
	private transient long id;
	private String displayName;
	private String phoneCode;
	private String a2;
	private String supportPhone;
	private String supportFax;
	private CountryStatus countryStatus;
	// this shouldn't be serialized, because we don't need it in the Json
	private transient CollationKey displayNameCollationKey;
	
	/**
	 * 
	 * @param id country id
	 * @param displayName localized country name
	 * @param phoneCode the phone prefix
	 * @param a2 the A2 country code
	 * @param collationKey this key is used for bitwise comparison
	 */
	public CountryMiniBean(long id, String displayName, String phoneCode, String a2, CollationKey collationKey) {
		this.id = id;
		this.displayName = displayName;
		this.phoneCode = phoneCode;
		this.a2 = a2;
		this.displayNameCollationKey = collationKey;
	}
	
	public CountryMiniBean(long id, String displayName, String phoneCode, String a2, CollationKey collationKey, 
			String supportPhone, String supportFax, CountryStatus countryStatus) {
		this.id = id;
		this.displayName = displayName;
		this.phoneCode = phoneCode;
		this.a2 = a2;
		this.displayNameCollationKey = collationKey;
		this.supportPhone = supportPhone;
		this.supportFax = supportFax;
		this.countryStatus = countryStatus;
	}
	
	public long getId() {
		return id;
	}
	
	public void setId(long id) {
		this.id = id;
	}
	
	public String getDisplayName() {
		return displayName;
	}
	
	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	public String getPhoneCode() {
		return phoneCode;
	}

	public void setPhoneCode(String phoneCode) {
		this.phoneCode = phoneCode;
	}

	public String getA2() {
		return a2;
	}

	public void setA2(String a2) {
		this.a2 = a2;
	}

	public CollationKey getDisplayNameCollationKey() {
		return displayNameCollationKey;
	}

	public void setDisplayNameCollationKey(CollationKey displayNameCollationKey) {
		this.displayNameCollationKey = displayNameCollationKey;
	}

	public String getSupportPhone() {
		return supportPhone;
	}

	public void setSupportPhone(String supportPhone) {
		this.supportPhone = supportPhone;
	}

	public String getSupportFax() {
		return supportFax;
	}

	public void setSupportFax(String supportFax) {
		this.supportFax = supportFax;
	}

	public CountryStatus getCountryStatus() {
		return countryStatus;
	}

	public void setCountryStatus(CountryStatus countryStatus) {
		this.countryStatus = countryStatus;
	}
}