package com.anyoption.common.beans.base;

import java.io.Serializable;

public class DepositBonusBalanceBase implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private long userId;
	private long depositCashBalance;
	private long bonusBalance;
	private long totalBalance;
	private boolean show = true;
	private long bonusWinnings;
	
	public long getUserId() {
		return userId;
	}
	public void setUserId(long userId) {
		this.userId = userId;
	}
	public long getDepositCashBalance() {
		return depositCashBalance;
	}
	public void setDepositCashBalance(long depositCashBalance) {
		this.depositCashBalance = depositCashBalance;
	}
	public long getBonusBalance() {
		return bonusBalance;
	}
	public void setBonusBalance(long bonusBalance) {
		this.bonusBalance = bonusBalance;
	}

	public boolean isShow() {
		return show;
	}
	public void setShow(boolean show) {
		this.show = show;
	}
	public long getTotalBalance() {		 
		 return totalBalance;
	}
	public void setTotalBalance(long totalBalance) {
		this.totalBalance = totalBalance;
	}
	public long getBonusWinnings() {
		return bonusWinnings;
	}
	public void setBonusWinnings(long bonusWinnings) {
		this.bonusWinnings = bonusWinnings;
	}
	
}
