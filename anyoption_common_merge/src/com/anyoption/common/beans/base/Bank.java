package com.anyoption.common.beans.base;

import java.io.Serializable;

public class Bank implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8923567146637269768L;
	protected Long id;
	protected String name;
	private Long bankCode;

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	public Long getBankCode() {
		return bankCode;
	}

	public void setBankCode(Long bankCode) {
		this.bankCode = bankCode;
	}

}