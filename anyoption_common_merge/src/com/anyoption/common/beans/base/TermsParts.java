package com.anyoption.common.beans.base;

public class TermsParts implements java.io.Serializable {

	public TermsParts(long partsId, String partsName) {
		super();
		this.partsId = partsId;
		this.partsName = partsName;
	}
	private static final long serialVersionUID = -3531713893236096649L;
	
	private long partsId;
	private String partsName;
	
	public long getPartsId() {
		return partsId;
	}
	public void setPartsId(long partsId) {
		this.partsId = partsId;
	}
	public String getPartsName() {
		return partsName;
	}
	public void setPartsName(String partsName) {
		this.partsName = partsName;
	}

}
