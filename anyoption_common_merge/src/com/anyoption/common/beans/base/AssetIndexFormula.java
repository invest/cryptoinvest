package com.anyoption.common.beans.base;

import java.io.Serializable;

/**
 * 
 * @author Ivan Petkov
 *
 */

public class AssetIndexFormula implements Serializable {

	private static final long serialVersionUID = 6172548562428565659L;
	
	protected int formulaId;
	protected String formulaKey;
	protected String formulaTranslation;
	
	public int getFormulaId() {
		return formulaId;
	}
	public void setFormulaId(int formulaId) {
		this.formulaId = formulaId;
	}
	public String getFormulaKey() {
		return formulaKey;
	}
	public void setFormulaKey(String formulaKey) {
		this.formulaKey = formulaKey;
	}
	public String getFormulaTranslation() {
		return formulaTranslation;
	}
	public void setFormulaTranslation(String formulaTranslation) {
		this.formulaTranslation = formulaTranslation;
	}
}
