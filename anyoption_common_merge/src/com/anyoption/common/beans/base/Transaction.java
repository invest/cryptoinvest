/**
 * 
 */
package com.anyoption.common.beans.base;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * @author kirilim
 *
 */
public class Transaction implements Serializable {
	
	private static final long serialVersionUID = 2367445277218216644L;
	protected long id;
	
	protected BigDecimal creditCardId;
	protected long typeId;
	protected Date timeCreated;
	protected long amount;
	protected String amountWF;
	protected long statusId;
	protected Date timeSettled;
	protected BigDecimal receiptNum;
	protected String authNumber;
	protected String captureNumber;
	protected String typeName;
	protected boolean credit;
	protected String cc4digit;
    protected String utcOffsetCreated;
    protected String utcOffsetSettled;
    protected String timeCreatedTxt;
    protected boolean threeD;
    protected boolean render;

    private String assignee;
    
    public String getTimeCreatedTxt() {
		return timeCreatedTxt;
	}

	public void setTimeCreatedTxt(String timeCreatedTxt) {
		this.timeCreatedTxt = timeCreatedTxt;
	}

	public boolean getCredit() {
		return credit;
	}

	public void setCredit(boolean credit) {
		this.credit = credit;
	}

	public String getTypeName() {
		return typeName;
	}

	public void setTypeName(String typeName) {
		this.typeName = typeName;
	}

	public long getAmount() {
		return amount;
	}

	public void setAmount(long amount) {
		this.amount = amount;
	}

	public BigDecimal getCreditCardId() {
        return creditCardId;
    }

    public void setCreditCardId(BigDecimal creditCardId) {
        this.creditCardId = creditCardId;
    }

	public long getStatusId() {
		return statusId;
	}

	public void setStatusId(long statusId) {
		this.statusId = statusId;
	}

	public Date getTimeCreated() {
		return timeCreated;
	}

	public void setTimeCreated(Date timeCreated) {
		this.timeCreated = timeCreated;
	}

	public Date getTimeSettled() {
		return timeSettled;
	}

	public void setTimeSettled(Date timeSettled) {
		this.timeSettled = timeSettled;
	}

	public long getTypeId() {
		return typeId;
	}

	public void setTypeId(long typeId) {
		this.typeId = typeId;
	}

	public BigDecimal getReceiptNum() {
        return receiptNum;
    }

    public void setReceiptNum(BigDecimal receiptNum) {
        this.receiptNum = receiptNum;
    }

    public String getCc4digit() {
		return cc4digit;
	}

	public void setCc4digit(String cc4digit) {
		this.cc4digit = cc4digit;
	}

	public String getAuthNumber() {
		return authNumber;
	}

	public void setAuthNumber(String authNumber) {
		this.authNumber = authNumber;
	}

	/**
	 * @return the utcOffsetCreated
	 */
	public String getUtcOffsetCreated() {
		return utcOffsetCreated;
	}

	/**
	 * @param utcOffsetCreated the utcOffsetCreated to set
	 */
	public void setUtcOffsetCreated(String utcOffsetCreated) {
		this.utcOffsetCreated = utcOffsetCreated;
	}

	/**
	 * @return the utcOffsetSettled
	 */
	public String getUtcOffsetSettled() {
		return utcOffsetSettled;
	}

	/**
	 * @param utcOffsetSettled the utcOffsetSettled to set
	 */
	public void setUtcOffsetSettled(String utcOffsetSettled) {
		this.utcOffsetSettled = utcOffsetSettled;
	}

	/**
	 * @return the amountWF
	 */
	public String getAmountWF() {
		return amountWF;
	}

	/**
	 * @param amountWF the amountWF to set
	 */
	public void setAmountWF(String amountWF) {
		this.amountWF = amountWF;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public boolean isThreeD() {
		return threeD;
	}

	public void setThreeD(boolean threeD) {
		this.threeD = threeD;
	}

	public String getCaptureNumber() {
		return captureNumber;
	}

	public void setCaptureNumber(String captureNumber) {
		this.captureNumber = captureNumber;
	}
	
	public boolean isRender() {
		return render;
	}

	public void setRender(boolean render) {
		this.render = render;
	}

	public String getAssignee() {
		return assignee;
	}

	public void setAssignee(String assignee) {
		this.assignee = assignee;
	}

	@Override
	public String toString() {
		return "Transaction [id=" + id + ", creditCardId=" + creditCardId
				+ ", typeId=" + typeId + ", timeCreated=" + timeCreated
				+ ", amount=" + amount + ", amountWF=" + amountWF
				+ ", statusId=" + statusId + ", timeSettled=" + timeSettled
				+ ", receiptNum=" + receiptNum + ", authNumber=" + authNumber
				+ ", typeName=" + typeName + ", credit=" + credit
				+ ", cc4digit=" + cc4digit + ", utcOffsetCreated="
				+ utcOffsetCreated + ", utcOffsetSettled=" + utcOffsetSettled
				+ ", timeCreatedTxt=" + timeCreatedTxt + ", assignee=" + assignee + "]";
	}	
}