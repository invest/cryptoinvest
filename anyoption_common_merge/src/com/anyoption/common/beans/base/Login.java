package com.anyoption.common.beans.base;


/**
 * Login bean wrapper
 *
 * @author EyalG
 *
 */
public class Login {
	/**
	 * 
	 */
	private static final long serialVersionUID = 5670488754164775837L;

	protected Long combinationId;
	protected String dynamicParam;
	protected String affSub1;
	protected String affSub2;
	protected String affSub3;

	public Long getCombinationId() {
		return combinationId;
	}

	public void setCombinationId(Long combinationId) {
		this.combinationId = combinationId;
	}

	public String getDynamicParam() {
		return dynamicParam;
	}
	
	public void setDynamicParam(String dynamicParam) {
		this.dynamicParam = dynamicParam;
	}
	
	public String getAffSub1() {
		return affSub1;
	}
	
	public void setAffSub1(String affSub1) {
		this.affSub1 = affSub1;
	}
	
	public String getAffSub2() {
		return affSub2;
	}
	
	public void setAffSub2(String affSub2) {
		this.affSub2 = affSub2;
	}
	
	public String getAffSub3() {
		return affSub3;
	}
	
	public void setAffSub3(String affSub3) {
		this.affSub3 = affSub3;
	}
}
