package com.anyoption.common.beans.base;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;

/**
 * MailBoxUser vo class.
 * @author Kobi
 *
 */
public class MailBoxUser implements java.io.Serializable {
	
	public static final int MAILBOX_STATUS_NEW = 1;
	public static final int MAILBOX_STATUS_READ = 2;
	public static final int MAILBOX_STATUS_DETETED = 3;
	
	private static final long serialVersionUID = 1L;
	protected long id;
	protected long templateId;
	protected long userId;
	protected long statusId;
	protected long writerId;
	protected Date timeCreated;
	protected Date timeRead;
	protected String utcOffsetRead;
	protected MailBoxTemplate template;
	protected boolean isHighPriority;
	protected long senderId;
	protected String freeText;
	protected String subject;
	protected long bonusUserId;
	protected long popupTypeId;
	protected long transactionId;
	protected Long attachmentId;
	protected String parameters;
	protected String senderName;
	protected String attachmentName;
	protected String timeCreatedTxt;
	protected String subjectShortTxt;
	/**
	 * @return the subjectShortTxt
	 */
	public String getSubjectShortTxt() {
		return subjectShortTxt;
	}
	
	/**
	 * sets the subjectShortTxt
	 */
	public void setSubjectShortTxt(String subjectShortTxt) {
		this.subjectShortTxt = subjectShortTxt;
	}

	/**
	 * @return the timeCreatedTxt
	 */
	public String getTimeCreatedTxt() {
		return timeCreatedTxt;
	}
	
	/**
	 * sets the timeCreatedTxt
	 */
	public void setTimeCreatedTxt(String timeCreatedTxt) {
		this.timeCreatedTxt = timeCreatedTxt;
	}

	/**
	 * @return the attachmentName
	 */
	public String getAttachmentName() {
		return attachmentName;
	}

	/**
	 * @param attachmentName the attachmentName to set
	 */
	public void setAttachmentName(String attachmentName) {
		this.attachmentName = attachmentName;
	}

	/**
	 * @return the senderName
	 */
	public String getSenderName() {
		return senderName;
	}

	/**
	 * @param senderName the senderName to set
	 */
	public void setSenderName(String senderName) {
		this.senderName = senderName;
	}

	/**
	 * @return the id
	 */
	public long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(long id) {
		this.id = id;
	}

	/**
	 * @return the statusId
	 */
	public long getStatusId() {
		return statusId;
	}

	/**
	 * @param statusId the statusId to set
	 */
	public void setStatusId(long statusId) {
		this.statusId = statusId;
	}

	/**
	 * @return the templateId
	 */
	public long getTemplateId() {
		return templateId;
	}

	/**
	 * @param templateId the templateId to set
	 */
	public void setTemplateId(long templateId) {
		this.templateId = templateId;
	}

	/**
	 * @return the timeCreated
	 */
	public Date getTimeCreated() {
		return timeCreated;
	}

	/**
	 * @param timeCreated the timeCreated to set
	 */
	public void setTimeCreated(Date timeCreated) {
		this.timeCreated = timeCreated;
	}

	/**
	 * @return the timeRead
	 */
	public Date getTimeRead() {
		return timeRead;
	}

	/**
	 * @param timeRead the timeRead to set
	 */
	public void setTimeRead(Date timeRead) {
		this.timeRead = timeRead;
	}

	/**
	 * @return the userId
	 */
	public long getUserId() {
		return userId;
	}

	/**
	 * @param userId the userId to set
	 */
	public void setUserId(long userId) {
		this.userId = userId;
	}

	/**
	 * @return the utcOffsetRead
	 */
	public String getUtcOffsetRead() {
		return utcOffsetRead;
	}

	/**
	 * @param utcOffsetRead the utcOffsetRead to set
	 */
	public void setUtcOffsetRead(String utcOffsetRead) {
		this.utcOffsetRead = utcOffsetRead;
	}

	/**
	 * @return the writerId
	 */
	public long getWriterId() {
		return writerId;
	}

	/**
	 * @param writerId the writerId to set
	 */
	public void setWriterId(long writerId) {
		this.writerId = writerId;
	}

	/**
	 * @return the template
	 */
	public MailBoxTemplate getTemplate() {
		return template;
	}

	/**
	 * @param template the template to set
	 */
	public void setTemplate(MailBoxTemplate template) {
		this.template = template;
	}

	public boolean isNew() {
		if (statusId == MAILBOX_STATUS_NEW) {
			return true;
		}
		return false;
	}

	public String getTemplateContent() {
		return this.template.getTemplate();
	}

	public String getSubject() {
		return this.subject;
	}

	public boolean getIsHighPriority() {
		return isHighPriority;
	}

	public void setIsHighPriority(boolean isHighPriority) {
		this.isHighPriority = isHighPriority;
	}

	public long getSenderId() {
		return senderId;
	}

	public void setSenderId(long senderId) {
		this.senderId = senderId;
	}

	public String getFreeText() {
		return freeText;
	}

	public void setFreeText(String freeText) {
		this.freeText = freeText;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getHighPriorityTxt() {
		return isHighPriority ? "true" : "false";
	}

	public long getBonusUserId() {
		return bonusUserId;
	}

	public void setBonusUserId(long bonusUserId) {
		this.bonusUserId = bonusUserId;
	}

	public long getPopupTypeId() {
		return popupTypeId;
	}

	public void setPopupTypeId(long popupTypeId) {
		this.popupTypeId = popupTypeId;
	}

	public long getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(long transactionId) {
		this.transactionId = transactionId;
	}

	/**
	 * toString implementation.
	 */
	public String toString() {
        String ls = System.getProperty("line.separator");
        return ls + "MailBoxUser:" + ls +
            "id: " + id + ls +
            "templateId: " + templateId + ls +
            "subject: " + subject + ls +
            "PopupTypeId: " + popupTypeId + ls +
            "isHighPriority" + isHighPriority + ls +
            "senderId: " + senderId + ls +
            "freeText: " + freeText + ls +
            "writerId: " + writerId + ls +
            "bonusUserId: " + bonusUserId + ls +
	        "timeCreated: " + timeCreated + ls;
   }

	public String getParameters() {
		return parameters;
	}

	public void setParameters(String parameters) {
		this.parameters = parameters;
	}

	/**
	 * @return the attachmentId
	 */
	public Long getAttachmentId() {
		return attachmentId;
	}

	/**
	 * @param attachmentId the attachmentId to set
	 */
	public void setAttachmentId(Long attachmentId) {
		this.attachmentId = attachmentId;
	}

	public void setParameters(HashMap<String, String> parameters) {
		boolean firstParam = true;
		Iterator<String> iter = parameters.keySet().iterator();
		while (iter.hasNext()) {
			String key = iter.next();
			String value = parameters.get(key);
			if (firstParam) {
				firstParam = false;
				this.parameters = "";
			} else {
				this.parameters += '&';
			}
			try {
				this.parameters += key + '=' + URLEncoder.encode(value, "UTF-8");
			} catch (UnsupportedEncodingException e) {
				this.parameters += key + '=' + value;
			}
		}
	}

}
