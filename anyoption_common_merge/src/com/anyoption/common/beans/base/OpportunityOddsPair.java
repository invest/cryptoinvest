package com.anyoption.common.beans.base;

import java.io.Serializable;

import com.google.gson.annotations.Expose;

public class OpportunityOddsPair implements Serializable {

	private static final long serialVersionUID = 3390919696907599041L;
	@Expose
	private long selectorID;
	@Expose
	private long returnSelector;
	@Expose
	private long refundSelector;

	/**
	 * @return the selectorID
	 */
	public long getSelectorID() {
		return selectorID;
	}

	/**
	 * @param selectorID the selectorID to set
	 */
	public void setSelectorID(long selectorID) {
		this.selectorID = selectorID;
	}

	/**
	 * @return the returnSelector
	 */
	public long getReturnSelector() {
		return returnSelector;
	}

	/**
	 * @param returnSelector the returnSelector to set
	 */
	public void setReturnSelector(long returnSelector) {
		this.returnSelector = returnSelector;
	}

	/**
	 * @return the refundSelector
	 */
	public long getRefundSelector() {
		return refundSelector;
	}

	/**
	 * @param refundSelector the refundSelector to set
	 */
	public void setRefundSelector(long refundSelector) {
		this.refundSelector = refundSelector;
	}
}