package com.anyoption.common.beans.base;

import java.io.Serializable;

/**
 * @author EranL
 *
 */
public class MarketWidget extends Market implements Serializable {

	private static final long serialVersionUID = 1L;
	private int pageOddsWin;
	private long timeEstClosingMillsec;
	private double currentLevel;
	private String currentLevelStr;
	private float callsTrend;
	private float putsTrend;
	private String name;

	/**
	 * @return the pageOddsWin
	 */
	public int getPageOddsWin() {
		return pageOddsWin;
	}
	/**
	 * @param pageOddsWin the pageOddsWin to set
	 */
	public void setPageOddsWin(int pageOddsWin) {
		this.pageOddsWin = pageOddsWin;
	}
	/**
	 * @return the timeEstClosingMillsec
	 */
	public long getTimeEstClosingMillsec() {
		return timeEstClosingMillsec;
	}
	/**
	 * @param timeEstClosingMillsec the timeEstClosingMillsec to set
	 */
	public void setTimeEstClosingMillsec(long timeEstClosingMillsec) {
		this.timeEstClosingMillsec = timeEstClosingMillsec;
	}
	/**
	 * @return the currentLevel
	 */
	public double getCurrentLevel() {
		return currentLevel;
	}
	/**
	 * @param currentLevel the currentLevel to set
	 */
	public void setCurrentLevel(double currentLevel) {
		this.currentLevel = currentLevel;
	}
	/**
	 * @return the callsTrend
	 */
	public float getCallsTrend() {
		return callsTrend;
	}
	/**
	 * @param callsTrend the callsTrend to set
	 */
	public void setCallsTrend(float callsTrend) {
		this.callsTrend = callsTrend;
	}
	/**
	 * @return the putsTrend
	 */
	public float getPutsTrend() {
		return putsTrend;
	}
	/**
	 * @param putsTrend the putsTrend to set
	 */
	public void setPutsTrend(float putsTrend) {
		this.putsTrend = putsTrend;
	}
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * @return the currentLevelStr
	 */
	public String getCurrentLevelStr() {
		return currentLevelStr;
	}
	/**
	 * @param currentLevelStr the currentLevelStr to set
	 */
	public void setCurrentLevelStr(String currentLevelStr) {
		this.currentLevelStr = currentLevelStr;
	}	
		
}
