package com.anyoption.common.beans.base;

import java.io.Serializable;

/**
 * @author EranL
 *
 */
public class ApiPage implements Serializable {

	private static final long serialVersionUID = 1L; 
	private long id;
	private String url;
	private String description;
	private boolean canManualRedirect;
	
	
	public final static long API_PAGE_DEPOSIT		= 1;
	public final static long API_PAGE_WITHDRAW		= 2;
	public final static long API_PAGE_QUESTIONNAIRE	= 3;
	public final static long API_PAGE_LOGIN			= 4;
	public final static long API_PAGE_TERMS			= 5;
		
	/**
	 * @return the id
	 */
	public long getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(long id) {
		this.id = id;
	}
	/**
	 * @return the url
	 */
	public String getUrl() {
		return url;
	}
	/**
	 * @param url the url to set
	 */
	public void setUrl(String url) {
		this.url = url;
	}
	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}
	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}
	/**
	 * @return the canManualRedirect
	 */
	public boolean isCanManualRedirect() {
		return canManualRedirect;
	}
	/**
	 * @param canManualRedirect the canManualRedirect to set
	 */
	public void setCanManualRedirect(boolean canManualRedirect) {
		this.canManualRedirect = canManualRedirect;
	}
	
	
}
