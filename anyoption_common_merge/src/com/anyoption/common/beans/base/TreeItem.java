package com.anyoption.common.beans.base;

import java.io.Serializable;

/**
 * @author dzhamaldv
 *
 */
public class TreeItem implements Serializable {

	private int marketId;
	private String marketName;
	private String groupName;
	private String marketSubGroupDisplayName;
	private boolean printGroup;
	private boolean printGeoGroup;

    /**
	 * @return the groupName
	 */
	public String getGroupName() {
		return groupName;
	}

	/**
	 * @param groupName the groupName to set
	 */
	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	/**
	 * @return the marketId
	 */
	public int getMarketId() {
		return marketId;
	}

	/**
	 * @param marketId the marketId to set
	 */
	public void setMarketId(int marketId) {
		this.marketId = marketId;
	}

	/**
	 * @return the marketName
	 */
	public String getMarketName() {
		return marketName;
	}

	/**
	 * @param marketName the marketName to set
	 */
	public void setMarketName(String marketName) {
		this.marketName = marketName;
	}

	/**
	 * @return the marketSubGroupDisplayName
	 */
	public String getMarketSubGroupDisplayName() {
		return marketSubGroupDisplayName;
	}

	/**
	 * @param marketSubGroupDisplayName the marketSubGroupDisplayName to set
	 */
	public void setMarketSubGroupDisplayName(String marketSubGroupDisplayName) {
		this.marketSubGroupDisplayName = marketSubGroupDisplayName;
	}
	
	   /**
	 * Is not CamelCase, because in LastLevelsForm.marketGroupList have case when
        one time return ArrayList<SelectItem> from DB and second time 
        return ArrayList<TreeItem>
	 */
	public String getGroup_name() {
			return groupName;
		}

	   /**
	 * Is not CamelCase, because in LastLevelsForm.marketGroupList have case when
     one time return ArrayList<SelectItem> from DB and second time 
     return ArrayList<TreeItem>
	 */
		public void setGroup_name(String group_name) {
			this.groupName = group_name;
		}
		   
		/**
		 * Is not CamelCase, because in LastLevelsForm.marketGroupList have case when
	        one time return ArrayList<SelectItem> from DB and second time 
	        return ArrayList<TreeItem>
		 */
		public int getMarket_id() {
			return this.marketId;
		}

		   /**
		 * Is not CamelCase, because in LastLevelsForm.marketGroupList have case when
	        one time return ArrayList<SelectItem> from DB and second time 
	        return ArrayList<TreeItem>
		 */
		public void setMarket_id(int market_id) {
			this.marketId = market_id;
		}

		   /**
		 * Is not CamelCase, because in LastLevelsForm.marketGroupList have case when
	        one time return ArrayList<SelectItem> from DB and second time 
	        return ArrayList<TreeItem>
		 */
		public String getMarket_name() {
			return this.marketName;
		}

		   /**
		 * Is not CamelCase, because in LastLevelsForm.marketGroupList have case when
	        one time return ArrayList<SelectItem> from DB and second time 
	        return ArrayList<TreeItem>
		 */
		public void setMarket_name(String market_name) {
			this.marketName = market_name;
		}

		   /**
		 * Is not CamelCase, because in LastLevelsForm.marketGroupList have case when
	        one time return ArrayList<SelectItem> from DB and second time 
	        return ArrayList<TreeItem>
		 */
		public String getMarket_subGroup_display_name() {
			return this.marketSubGroupDisplayName;
		}

		   /**
		 * Is not CamelCase, because in LastLevelsForm.marketGroupList have case when
	        one time return ArrayList<SelectItem> from DB and second time 
	        return ArrayList<TreeItem>
		 */
		public void setMarket_subGroup_display_name(String market_subGroup_display_name) {
			this.marketSubGroupDisplayName = market_subGroup_display_name;
		}

		
	    public boolean isPrintGeoGroup() {
	        return printGeoGroup;
	    }

	    public void setPrintGeoGroup(boolean printGeoGroup) {
	        this.printGeoGroup = printGeoGroup;
	    }

	    public boolean isPrintGroup() {
	        return printGroup;
	    }

	    public void setPrintGroup(boolean printGroup) {
	        this.printGroup = printGroup;
	    }
	    

}