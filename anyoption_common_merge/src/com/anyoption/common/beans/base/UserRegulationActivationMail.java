package com.anyoption.common.beans.base;

public class UserRegulationActivationMail implements java.io.Serializable {

	private static final long serialVersionUID = -1382192838699918241L;
	
	public static final int SUSPENDED_LOW_X_TRESHOLD = 13;
	public static final int SUSPENDED_HIGH_Y_TRESHOLD = 14;	
	
	private long id;
	private long userId;
	private long suspendedReasonId;
	private boolean isSend;
	private long skinId;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public long getUserId() {
		return userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}

	public long getSuspendedReasonId() {
		return suspendedReasonId;
	}

	public void setSuspendedReasonId(long suspendedReasonId) {
		this.suspendedReasonId = suspendedReasonId;
	}

	public boolean isSend() {
		return isSend;
	}

	public void setSend(boolean isSend) {
		this.isSend = isSend;
	}

	public long getSkinId() {
		return skinId;
	}

	public void setSkinId(long skinId) {
		this.skinId = skinId;
	}
}
