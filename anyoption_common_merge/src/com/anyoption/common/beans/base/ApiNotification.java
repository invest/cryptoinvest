package com.anyoption.common.beans.base;

import java.io.Serializable;

/**
 * @author EranL
 *
 */
public class ApiNotification implements Serializable {

	private static final long serialVersionUID = 1L;
		
	private long id;
	private int type;
	private String reference;
	private String amount;
	private String timeSettled;
	private long userId;
	private String externalUserRef;
	
	/**
	 * @return the id
	 */
	public long getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(long id) {
		this.id = id;
	}
	/**
	 * @return the type
	 */
	public int getType() {
		return type;
	}
	/**
	 * @param type the type to set
	 */
	public void setType(int type) {
		this.type = type;
	}
	/**
	 * @return the reference
	 */
	public String getReference() {
		return reference;
	}
	/**
	 * @param reference the reference to set
	 */
	public void setReference(String reference) {
		this.reference = reference;
	}
	/**
	 * @return the amount
	 */
	public String getAmount() {
		return amount;
	}
	/**
	 * @param amount the amount to set
	 */
	public void setAmount(String amount) {
		this.amount = amount;
	}
	
	/**
	 * @return the timeSettled
	 */
	public String getTimeSettled() {
		return timeSettled;
	}
	/**
	 * @param timeSettled the timeSettled to set
	 */
	public void setTimeSettled(String timeSettled) {
		this.timeSettled = timeSettled;
	}
	/**
	 * @return the userId
	 */
	public long getUserId() {
		return userId;
	}
	/**
	 * @param userId the userId to set
	 */
	public void setUserId(long userId) {
		this.userId = userId;
	}
	/**
	 * @return the externalUserRef
	 */
	public String getExternalUserRef() {
		return externalUserRef;
	}
	/**
	 * @param externalUserRef the externalUserRef to set
	 */
	public void setExternalUserRef(String externalUserRef) {
		this.externalUserRef = externalUserRef;
	}

}
