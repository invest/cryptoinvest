package com.anyoption.common.beans.base;

import java.io.Serializable;

public class PendingUserWithdrawalsDetailsBase implements Serializable {

	
	public static final long USER_BONUS_STATE_YES = 1;
	public static final long USER_BONUS_STATE_NO = 2;
	public static final long USER_BONUS_STATE_PENDING = 3;
	
	private static final long serialVersionUID = 1L;
	
	
	private long userId;
	private long currencyId;
	private boolean isHaveInvestments;
	private String isHaveInvestmentsTxt;
	private long winLoseBalance;
	private String winloseBalanceTxt;
	private long userBonusState;
	private String userBonusStateTxt;
	private long successfulWithdrawals;
	private long depositAmountInUSD;
	private String depositAmountInUSDTxt;
	private long depositAmount;
	private String depositAmountTxt;
	
	public long getUserId() {
		return userId;
	}
	public void setUserId(long userId) {
		this.userId = userId;
	}
	public boolean isHaveInvestments() {
		return isHaveInvestments;
	}
	public void setHaveInvestments(boolean isHaveInvestments) {
		//Set userBonusStateTxt
    	if(isHaveInvestments){
    		isHaveInvestmentsTxt = "Yes";
    	} else {
    		isHaveInvestmentsTxt = "No";
    	}
		this.isHaveInvestments = isHaveInvestments;
	}
	public long getWinLoseBalance() {
		return winLoseBalance;
	}
	public void setWinLoseBalance(long winLoseBalance) {
		this.winLoseBalance = winLoseBalance;
	}
	public long getUserBonusState() {
		return userBonusState;
	}
	public void setUserBonusState(long userBonusState) {		
		this.userBonusState = userBonusState;
		
		//Set userBonusStateTxt
    	if(userBonusState == USER_BONUS_STATE_YES){
    		userBonusStateTxt = "Yes";
    	} else if(userBonusState == USER_BONUS_STATE_PENDING){
    		userBonusStateTxt = "Pending";
    	} else {
    		userBonusStateTxt = "No";
    	}
	}
	public long getSuccessfulWithdrawals() {
		return successfulWithdrawals;
	}
	public void setSuccessfulWithdrawals(long successfulWithdrawals) {
		this.successfulWithdrawals = successfulWithdrawals;
	}
	public long getDepositAmountInUSD() {
		return depositAmountInUSD;
	}
	public void setDepositAmountInUSD(long depositAmountInUSD) {
		this.depositAmountInUSD = depositAmountInUSD;
	}
	public long getDepositAmount() {
		return depositAmount;
	}
	public void setDepositAmount(long depositAmount) {
		this.depositAmount = depositAmount;
	}
	public long getCurrencyId() {
		return currencyId;
	}
	public void setCurrencyId(long currencyId) {
		this.currencyId = currencyId;
	}
	public String getWinloseBalanceTxt() {
		return winloseBalanceTxt;
	}
	public void setWinloseBalanceTxt(String winloseBalanceTxt) {
		this.winloseBalanceTxt = winloseBalanceTxt;
	}
	public String getUserBonusStateTxt() {
		return userBonusStateTxt;
	}
	public void setUserBonusStateTxt(String userBonusStateTxt) {
		this.userBonusStateTxt = userBonusStateTxt;
	}
	public String getDepositAmountTxt() {
		return depositAmountTxt;
	}
	public void setDepositAmountTxt(String depositAmountTxt) {
		this.depositAmountTxt = depositAmountTxt;
	}
	public String getDepositAmountInUSDTxt() {
		return depositAmountInUSDTxt;
	}
	public void setDepositAmountInUSDTxt(String depositAmountInUSDTxt) {
		this.depositAmountInUSDTxt = depositAmountInUSDTxt;
	}
	
	
	@Override
	public String toString() {
		return "PendingUserWithdrawalsDetailsBase [userId=" + userId + ", currencyId=" + currencyId
				+ ", isHaveInvestments=" + isHaveInvestments +  ", winLoseBalance;=" + winLoseBalance  
				+ ", winloseBalanceTxt=" + winloseBalanceTxt +  ", userBonusState=" + userBonusState  
				+ ", successfulWithdrawals=" + successfulWithdrawals +  ", depositAmountInUSD=" + depositAmountInUSD  
				+ ", depositAmount=" + depositAmount + "]";
	}
	public String getIsHaveInvestmentsTxt() {
		return isHaveInvestmentsTxt;
	}
	public void setIsHaveInvestmentsTxt(String isHaveInvestmentsTxt) {
		this.isHaveInvestmentsTxt = isHaveInvestmentsTxt;
	}
}
