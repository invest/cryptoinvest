package com.anyoption.common.beans.base;

import java.io.Serializable;

import com.google.gson.annotations.Expose;

/**
 * MarketingInfo
 * @author eyalo
 */
public class MarketingInfo implements Serializable {
	private static final long serialVersionUID = 6533370913277106416L;
	
	@Expose
	protected String[] dynamicParams;
	@Expose
	protected long countSignups;
	@Expose
	protected long countFtds;
	
	/**
	 * @return the dynamicParams
	 */
	public String[] getDynamicParams() {
		return dynamicParams;
	}
	
	/**
	 * @param dynamicParams the dynamicParams to set
	 */
	public void setDynamicParams(String[] dynamicParams) {
		this.dynamicParams = dynamicParams;
	}
	
	/**
	 * @return the countFtds
	 */
	public long getCountFtds() {
		return countFtds;
	}
	
	/**
	 * @param countFtds the countFtds to set
	 */
	public void setCountFtds(long countFtds) {
		this.countFtds = countFtds;
	}

	/**
	 * @return the countSignups
	 */
	public long getCountSignups() {
		return countSignups;
	}

	/**
	 * @param countSignups the countSignups to set
	 */
	public void setCountSignups(long countSignups) {
		this.countSignups = countSignups;
	}
}