package com.anyoption.common.beans.base;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import com.google.gson.annotations.Expose;

/**
 * @author LioR SoLoMoN
 *
 */
public class UserAnycapital implements Serializable {
	private static final long serialVersionUID = 2164946227631692699L;	
	@Expose
	private long id;
	@Expose
	private long balance;
	@Expose
	private String userName;
	@Expose
	private String password;
	@Expose
	private int currencyId;
	@Expose
	private String firstName;
	@Expose
	private String lastName;
	@Expose
	private String street;
	@Expose	
	private String zipCode;
	@Expose
	private Date timeCreated;
	@Expose
	private Date timeModified;	
	@Expose
	private Date timeLastLogin;
	@Expose
	private int isActive;
	@Expose
	private String email;
	@Expose
	private String comments;
	@Expose
	private String ip;
	@Expose
	private Date timeBirthDate;
	@Expose
	private int isContactByEmail;
	@Expose
	private int isContactBySms;
	@Expose
	private int isContactByPhone;
	@Expose
	private int isAcceptedTerms;
	@Expose
	private String mobilePhone;
	@Expose
	private String landLinePhone;
	@Expose
	private Gender gender;
	@Expose
	private Clazz clazz;
	@Expose
	private String streetNo;
	@Expose
	private String utcOffsetModified;
	@Expose
	private int languageId;
	@Expose
	private String utcOffset;
	@Expose
	private String cityName;
	@Expose
	private String userAgent;
	@Expose
	private String httpReferer;
	@Expose
	private int countryId;
	@Expose
	private int writerId;
	@Expose
	private int aoUserId;
	@Expose
	private String utcOffsetCreated;
	
	@Override
	public String toString() {
		return "UserAnycapital [id=" + id + ", balance=" + balance
				+ ", userName=" + userName + ", password=" + password
				+ ", currencyId=" + currencyId + ", firstName=" + firstName
				+ ", lastName=" + lastName + ", street=" + street
				+ ", zipCode=" + zipCode + ", timeCreated=" + timeCreated
				+ ", timeModified=" + timeModified + ", timeLastLogin="
				+ timeLastLogin + ", isActive=" + isActive + ", email=" + email
				+ ", comments=" + comments + ", ip=" + ip + ", timeBirthDate="
				+ timeBirthDate + ", isContactByEmail=" + isContactByEmail
				+ ", isContactBySms=" + isContactBySms + ", isContactByPhone="
				+ isContactByPhone + ", isAcceptedTerms=" + isAcceptedTerms
				+ ", mobilePhone=" + mobilePhone + ", landLinePhone="
				+ landLinePhone + ", gender=" + gender + ", clazz=" + clazz
				+ ", streetNo=" + streetNo + ", utcOffsetModified="
				+ utcOffsetModified + ", languageId=" + languageId
				+ ", utcOffset=" + utcOffset + ", cityName=" + cityName
				+ ", userAgent=" + userAgent + ", httpReferer=" + httpReferer
				+ ", countryId=" + countryId + ", writerId=" + writerId
				+ ", aoUserId=" + aoUserId + ", utcOffsetCreated="
				+ utcOffsetCreated + "]";
	}

	/**
	 * @return the id
	 */
	public long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(long id) {
		this.id = id;
	}

	/**
	 * @return the balance
	 */
	public long getBalance() {
		return balance;
	}

	/**
	 * @param balance the balance to set
	 */
	public void setBalance(long balance) {
		this.balance = balance;
	}

	/**
	 * @return the userName
	 */
	public String getUserName() {
		return userName;
	}

	/**
	 * @param userName the userName to set
	 */
	public void setUserName(String userName) {
		this.userName = userName;
	}

	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * @param password the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * @return the currencyId
	 */
	public int getCurrencyId() {
		return currencyId;
	}

	/**
	 * @param currencyId the currencyId to set
	 */
	public void setCurrencyId(int currencyId) {
		this.currencyId = currencyId;
	}

	/**
	 * @return the firstName
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * @param firstName the firstName to set
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	/**
	 * @return the lastName
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * @param lastName the lastName to set
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	/**
	 * @return the street
	 */
	public String getStreet() {
		return street;
	}

	/**
	 * @param street the street to set
	 */
	public void setStreet(String street) {
		this.street = street;
	}

	/**
	 * @return the zipCode
	 */
	public String getZipCode() {
		return zipCode;
	}

	/**
	 * @param zipCode the zipCode to set
	 */
	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}

	/**
	 * @return the timeCreated
	 */
	public Date getTimeCreated() {
		return timeCreated;
	}

	/**
	 * @param timeCreated the timeCreated to set
	 */
	public void setTimeCreated(Date timeCreated) {
		this.timeCreated = timeCreated;
	}

	/**
	 * @return the timeModified
	 */
	public Date getTimeModified() {
		return timeModified;
	}

	/**
	 * @param timeModified the timeModified to set
	 */
	public void setTimeModified(Date timeModified) {
		this.timeModified = timeModified;
	}

	/**
	 * @return the timeLastLogin
	 */
	public Date getTimeLastLogin() {
		return timeLastLogin;
	}

	/**
	 * @param timeLastLogin the timeLastLogin to set
	 */
	public void setTimeLastLogin(Date timeLastLogin) {
		this.timeLastLogin = timeLastLogin;
	}

	/**
	 * @return the isActive
	 */
	public int getIsActive() {
		return isActive;
	}

	/**
	 * @param isActive the isActive to set
	 */
	public void setIsActive(int isActive) {
		this.isActive = isActive;
	}

	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * @param email the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * @return the comments
	 */
	public String getComments() {
		return comments;
	}

	/**
	 * @param comments the comments to set
	 */
	public void setComments(String comments) {
		this.comments = comments;
	}

	/**
	 * @return the ip
	 */
	public String getIp() {
		return ip;
	}

	/**
	 * @param ip the ip to set
	 */
	public void setIp(String ip) {
		this.ip = ip;
	}

	/**
	 * @return the timeBirthDate
	 */
	public Date getTimeBirthDate() {
		return timeBirthDate;
	}

	/**
	 * @param timeBirthDate the timeBirthDate to set
	 */
	public void setTimeBirthDate(Date timeBirthDate) {
		this.timeBirthDate = timeBirthDate;
	}

	/**
	 * @return the isContactByEmail
	 */
	public int getIsContactByEmail() {
		return isContactByEmail;
	}

	/**
	 * @param isContactByEmail the isContactByEmail to set
	 */
	public void setIsContactByEmail(int isContactByEmail) {
		this.isContactByEmail = isContactByEmail;
	}

	/**
	 * @return the isContactBySms
	 */
	public int getIsContactBySms() {
		return isContactBySms;
	}

	/**
	 * @param isContactBySms the isContactBySms to set
	 */
	public void setIsContactBySms(int isContactBySms) {
		this.isContactBySms = isContactBySms;
	}

	/**
	 * @return the isContactByPhone
	 */
	public int getIsContactByPhone() {
		return isContactByPhone;
	}

	/**
	 * @param isContactByPhone the isContactByPhone to set
	 */
	public void setIsContactByPhone(int isContactByPhone) {
		this.isContactByPhone = isContactByPhone;
	}

	/**
	 * @return the isAcceptedTerms
	 */
	public int getIsAcceptedTerms() {
		return isAcceptedTerms;
	}

	/**
	 * @param isAcceptedTerms the isAcceptedTerms to set
	 */
	public void setIsAcceptedTerms(int isAcceptedTerms) {
		this.isAcceptedTerms = isAcceptedTerms;
	}

	/**
	 * @return the mobilePhone
	 */
	public String getMobilePhone() {
		return mobilePhone;
	}

	/**
	 * @param mobilePhone the mobilePhone to set
	 */
	public void setMobilePhone(String mobilePhone) {
		this.mobilePhone = mobilePhone;
	}

	/**
	 * @return the landLinePhone
	 */
	public String getLandLinePhone() {
		return landLinePhone;
	}

	/**
	 * @param landLinePhone the landLinePhone to set
	 */
	public void setLandLinePhone(String landLinePhone) {
		this.landLinePhone = landLinePhone;
	}

	/**
	 * @return the gender
	 */
	public Gender getGender() {
		return gender;
	}

	/**
	 * @param gender the gender to set
	 */
	public void setGender(Gender gender) {
		this.gender = gender;
	}

	/**
	 * @return the clazz
	 */
	public Clazz getClazz() {
		return clazz;
	}

	/**
	 * @param clazz the clazz to set
	 */
	public void setClazz(Clazz clazz) {
		this.clazz = clazz;
	}

	/**
	 * @return the streetNo
	 */
	public String getStreetNo() {
		return streetNo;
	}

	/**
	 * @param streetNo the streetNo to set
	 */
	public void setStreetNo(String streetNo) {
		this.streetNo = streetNo;
	}

	/**
	 * @return the utcOffsetModified
	 */
	public String getUtcOffsetModified() {
		return utcOffsetModified;
	}

	/**
	 * @param utcOffsetModified the utcOffsetModified to set
	 */
	public void setUtcOffsetModified(String utcOffsetModified) {
		this.utcOffsetModified = utcOffsetModified;
	}

	/**
	 * @return the languageId
	 */
	public int getLanguageId() {
		return languageId;
	}

	/**
	 * @param languageId the languageId to set
	 */
	public void setLanguageId(int languageId) {
		this.languageId = languageId;
	}

	/**
	 * @return the utcOffset
	 */
	public String getUtcOffset() {
		return utcOffset;
	}

	/**
	 * @param utcOffset the utcOffset to set
	 */
	public void setUtcOffset(String utcOffset) {
		this.utcOffset = utcOffset;
	}

	/**
	 * @return the cityName
	 */
	public String getCityName() {
		return cityName;
	}

	/**
	 * @param cityName the cityName to set
	 */
	public void setCityName(String cityName) {
		this.cityName = cityName;
	}

	/**
	 * @return the userAgent
	 */
	public String getUserAgent() {
		return userAgent;
	}

	/**
	 * @param userAgent the userAgent to set
	 */
	public void setUserAgent(String userAgent) {
		this.userAgent = userAgent;
	}

	/**
	 * @return the httpReferer
	 */
	public String getHttpReferer() {
		return httpReferer;
	}

	/**
	 * @param httpReferer the httpReferer to set
	 */
	public void setHttpReferer(String httpReferer) {
		this.httpReferer = httpReferer;
	}

	/**
	 * @return the countryId
	 */
	public int getCountryId() {
		return countryId;
	}

	/**
	 * @param countryId the countryId to set
	 */
	public void setCountryId(int countryId) {
		this.countryId = countryId;
	}

	/**
	 * @return the writerId
	 */
	public int getWriterId() {
		return writerId;
	}

	/**
	 * @param writerId the writerId to set
	 */
	public void setWriterId(int writerId) {
		this.writerId = writerId;
	}

	/**
	 * @return the aoUserId
	 */
	public int getAoUserId() {
		return aoUserId;
	}

	/**
	 * @param aoUserId the aoUserId to set
	 */
	public void setAoUserId(int aoUserId) {
		this.aoUserId = aoUserId;
	}

	/**
	 * @return the utcOffsetCreated
	 */
	public String getUtcOffsetCreated() {
		return utcOffsetCreated;
	}

	/**
	 * @param utcOffsetCreated the utcOffsetCreated to set
	 */
	public void setUtcOffsetCreated(String utcOffsetCreated) {
		this.utcOffsetCreated = utcOffsetCreated;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	/**
	 * ISO/IEC 5218
	 * 
	 */
	public enum Gender {
		MALE("M", 1), FEMALE("F", 2), NOT_KNOWN("K", 0), NOT_APPLICABLE("A", 9);
		
		private static final Map<String, Gender> TOKEN_GENDER_MAP = new HashMap<String, Gender>(Gender.values().length);
		static {
			for (Gender g : Gender.values()) {
				TOKEN_GENDER_MAP.put(g.getToken(), g);
			}
		}
		
		private String token;
		private int code;
		
		/**
		 * @param token
		 * @param code
		 */
		private Gender(String token, int code) {
			this.token = token;
			this.code = code;
		}
		
		/**
		 * token - "M","F"...
		 * 
		 * @param token
		 * @return
		 */
		public static Gender getByToken(String token) {
			Gender g = TOKEN_GENDER_MAP.get(token);
			if (g == null) {
				throw new IllegalArgumentException("No Gender with token: " + token);
			} else {
				return g;
			}
		}
		
		/**
		 * @return the token
		 */
		public String getToken() {
			return this.token;
		}
		
		/**
		 * @return the code
		 */
		public int getCode() {
			return this.code;
		}
	}
	
	/**
	 * 
	 *
	 */
	public enum Clazz {
		TESTER(0), REAL(1);
		
		private static final Map<Integer, Clazz> TOKEN_CLAZZ_MAP = new HashMap<Integer, Clazz>(Clazz.values().length);
		static {
			for (Clazz g : Clazz.values()) {
				TOKEN_CLAZZ_MAP.put(g.getToken(), g);
			}
		}
		
		private int token;
		
		/**
		 * @param token
		 * @return
		 */
		public static Clazz getByToken(int token) {
			Clazz c = TOKEN_CLAZZ_MAP.get(token);
			if (c == null) {
				throw new IllegalArgumentException("No Clazz with token: " + token);
			} else {
				return c;
			}
		}
		
		/**
		 * @param token
		 */
		private Clazz(int token) {
			this.token = token;
		}
		
		/**
		 * @return
		 */
		public int getToken() {
			return this.token;
		}		
	}
}
