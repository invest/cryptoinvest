package com.anyoption.common.beans.base;

import java.io.Serializable;
import java.util.Date;

import org.apache.log4j.Logger;

/**
 * DeeplinkParamsToUrl bean wrapper
 *
 * @author Eyal Goren
 *
 */
public class DeeplinkParamsToUrl implements Serializable {

	private static final Logger log = Logger.getLogger(DeeplinkParamsToUrl.class);

	private int id;
	private String parameterName;
	private String anyoptionUrl;
	private String copyopUrl;
	private Date timeCreated;
	private Date timeUpdated;
	private long writerId;
	
	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	
	public String getParameterName() {
		return parameterName;
	}
	
	public void setParameterName(String parameterName) {
		this.parameterName = parameterName;
	}
	
	public String getAnyoptionUrl() {
		return anyoptionUrl;
	}
	
	public void setAnyoptionUrl(String anyoptionUrl) {
		this.anyoptionUrl = anyoptionUrl;
	}
	
	public String getCopyopUrl() {
		return copyopUrl;
	}
	
	public void setCopyopUrl(String copyopUrl) {
		this.copyopUrl = copyopUrl;
	}

	public Date getTimeCreated() {
		return timeCreated;
	}

	public void setTimeCreated(Date timeCreated) {
		this.timeCreated = timeCreated;
	}

	public Date getTimeUpdated() {
		return timeUpdated;
	}

	public void setTimeUpdated(Date timeUpdated) {
		this.timeUpdated = timeUpdated;
	}

	public long getWriterId() {
		return writerId;
	}

	public void setWriterId(long writerId) {
		this.writerId = writerId;
	}
}
