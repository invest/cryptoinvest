package com.anyoption.common.beans.base;

import java.io.Serializable;

/**
 * @author liors
 *
 */
public class ProvidersIP implements Serializable {
	public static final int PROVIDER_GROUP_ENVOY = 4;
	private static final long serialVersionUID = -1313687568107444664L;
	private String fromIp;
	private String toIp;
	private boolean allow;
	private int providersId;
	private int providerGroupId;
	
	@Override
	public String toString() {
		return "ProvidersIP [fromIp=" + fromIp + ", toIp=" + toIp + ", allow="
				+ allow + ", providersId=" + providersId + ", providerGroupId="
				+ providerGroupId + "]";
	}
	
	/**
	 * @return the fromIp
	 */
	public String getFromIp() {
		return fromIp;
	}
	/**
	 * @param fromIp the fromIp to set
	 */
	public void setFromIp(String fromIp) {
		this.fromIp = fromIp;
	}
	/**
	 * @return the toIp
	 */
	public String getToIp() {
		return toIp;
	}
	/**
	 * @param toIp the toIp to set
	 */
	public void setToIp(String toIp) {
		this.toIp = toIp;
	}
	/**
	 * @return the allow
	 */
	public boolean isAllow() {
		return allow;
	}
	/**
	 * @param allow the allow to set
	 */
	public void setAllow(boolean allow) {
		this.allow = allow;
	}
	/**
	 * @return the providersId
	 */
	public int getProvidersId() {
		return providersId;
	}
	/**
	 * @param providersId the providersId to set
	 */
	public void setProvidersId(int providersId) {
		this.providersId = providersId;
	}

	/**
	 * @return the providerGroupId
	 */
	public int getProviderGroupId() {
		return providerGroupId;
	}

	/**
	 * @param providerGroupId the providerGroupId to set
	 */
	public void setProviderGroupId(int providerGroupId) {
		this.providerGroupId = providerGroupId;
	}
}
