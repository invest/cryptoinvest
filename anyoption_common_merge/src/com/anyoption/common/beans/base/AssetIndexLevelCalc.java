package com.anyoption.common.beans.base;

import java.io.Serializable;

public class AssetIndexLevelCalc implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private long id;
	private long infoId;
	private long marketId;
	private long skinId;
	private String expiryTypeText;
	private String reutersFieldText;
	private String expiryFormulaText;
	private boolean isSameNextRow;
	
	public long getMarketId() {
		return marketId;
	}
	public void setMarketId(long marketId) {
		this.marketId = marketId;
	}
	public long getSkinId() {
		return skinId;
	}
	public void setSkinId(long skinId) {
		this.skinId = skinId;
	}
	public long getInfoId() {
		return infoId;
	}
	public void setInfoId(long infoId) {
		this.infoId = infoId;
	}
	public String getExpiryTypeText() {
		return expiryTypeText;
	}
	public void setExpiryTypeText(String expiryTypeText) {
		this.expiryTypeText = expiryTypeText;
	}
	public String getReutersFieldText() {
		return reutersFieldText;
	}
	public void setReutersFieldText(String reutersFieldText) {
		this.reutersFieldText = reutersFieldText;
	}
	public String getExpiryFormulaText() {
		return expiryFormulaText;
	}
	public void setExpiryFormulaText(String expiryFormulaText) {
		this.expiryFormulaText = expiryFormulaText;
	}
	public boolean isSameNextRow() {
		return isSameNextRow;
	}
	public void setSameNextRow(boolean isSameNextRow) {
		this.isSameNextRow = isSameNextRow;
	}
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
}
