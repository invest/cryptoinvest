package com.anyoption.common.beans.base;

import java.io.Serializable;

public class ApiErrorCode implements Serializable {

	private static final long serialVersionUID = 1L; 
	private long id;
	private long typeId;
	private String code;
	private String description;
	private String displayName;
	
	public final static String API_ERROR_CODE_GENERAL_SUCCESS				 				= "G000";
	public final static String API_ERROR_CODE_GENERAL_CANT_PARSE_JSON 						= "G001";
	public final static String API_ERROR_CODE_GENERAL_CANT_FIND_METHOD		 				= "G002";
	public final static String API_ERROR_CODE_GENERAL_NO_LOCALE_WAS_FOUND		 			= "G003";
	public final static String API_ERROR_CODE_GENERAL_CANT_CONNECT_TO_SERVER	 			= "G004";
	public final static String API_ERROR_CODE_GENERAL_REQUEST_IS_NOT_IN_THE_RIGHT_PROTOCOL	= "G005";	
	public final static String API_ERROR_CODE_AUTHENTICATION_INCORRECT_USERNAME_OR_PASSWORD = "A001";
	public final static String API_ERROR_CODE_AUTHENTICATION_USER_OR_PASSWORD_MISSING 		= "A002";
	public final static String API_ERROR_CODE_AUTHENTICATION_USER_NOT_ACTIVE 				= "A003";
	public final static String API_ERROR_CODE_AUTHENTICATION_PERMISSION_DENIED 				= "A004";
	public final static String API_ERROR_CODE_VALIDATION_ERROR				 				= "V999";
	public final static String API_ERROR_CODE_VALIDATION_MANDATORY_FIELD	 				= "V001";
	public final static String API_ERROR_CODE_VALIDATION_INVALID_NUMBER	 					= "V002";
	public final static String API_ERROR_CODE_VALIDATION_VALUE_SHORTER_THAN_ALLOWED 		= "V003";
	public final static String API_ERROR_CODE_VALIDATION_VALUE_LONGER_THAN_ALLOWED 			= "V004";
	public final static String API_ERROR_CODE_VALIDATION_LETTERS_ONLY		 				= "V005";
	public final static String API_ERROR_CODE_VALIDATION_RETYPED_DOES_NOT_MATCH				= "V006";
	public final static String API_ERROR_CODE_VALIDATION_MUST_ACCEPT_TERMS					= "V007";
	public final static String API_ERROR_CODE_VALIDATION_INVALID_EMAIL	 					= "V008";
	public final static String API_ERROR_CODE_VALIDATION_INVALID_PARAMETER	 				= "V009";
	public final static String API_ERROR_CODE_VALIDATION_COUNTRY_NOT_EXISTS	 				= "V010";
	public final static String API_ERROR_CODE_VALIDATION_INVALID_IP	 						= "V011";
	public final static String API_ERROR_CODE_VALIDATION_CURRENCY_NOT_EXISTS				= "V012";
	public final static String API_ERROR_CODE_VALIDATION_CURRENCY_NOT_SUPPORTED				= "V013";
	public final static String API_ERROR_CODE_VALIDATION_BALANCE_LOWER_REQ_INV_AMOUNT		= "V014";
	public final static String API_ERROR_CODE_VALIDATION_AMOUNT_BELOW_MIN_INVESTMENT		= "V015";
	public final static String API_ERROR_CODE_VALIDATION_AMOUNT_ABOVE_MAX_INVESTMENT		= "V016";
	public final static String API_ERROR_CODE_VALIDATION_OPTION_CURRENTLY_NOT_AVAILABLE		= "V017"; //investment
	public final static String API_ERROR_CODE_VALIDATION_OPTION_NO_LONGER_AVAILABLE			= "V018"; //investment
	public final static String API_ERROR_CODE_VALIDATION_CANT_FIND_OPPORTUNITY				= "V019"; //investment
	public final static String API_ERROR_CODE_VALIDATION_EMAIL_IN_USE						= "V020";
	public final static String API_ERROR_CODE_VALIDATION_LEVEL_IS_INVALID					= "V021";
	public final static String API_ERROR_CODE_VALIDATION_FORBIDDEN_COUNTRY					= "V022";
	public final static String API_ERROR_CODE_VALIDATION_INVALID_USER_FOR_API				= "V023";
	public final static String API_ERROR_CODE_VALIDATION_INVALID_DATE_FORMAT				= "V024";
	public final static String API_ERROR_CODE_VALIDATION_INVALID_AFF_KEY_FOR_API			= "V025";
	public static final int API_ERROR_CODE_VALIDATION_YEARS_OLD_18 = 18;

	
	
	/**
	 * @return the id
	 */
	public long getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(long id) {
		this.id = id;
	}
	/**
	 * @return the typeId
	 */
	public long getTypeId() {
		return typeId;
	}
	/**
	 * @param typeId the typeId to set
	 */
	public void setTypeId(long typeId) {
		this.typeId = typeId;
	}
	/**
	 * @return the code
	 */
	public String getCode() {
		return code;
	}
	/**
	 * @param code the code to set
	 */
	public void setCode(String code) {
		this.code = code;
	}
	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}
	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}
	/**
	 * @return the displayName
	 */
	public String getDisplayName() {
		return displayName;
	}
	/**
	 * @param displayName the displayName to set
	 */
	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

}
