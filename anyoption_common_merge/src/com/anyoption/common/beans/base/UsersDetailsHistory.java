package com.anyoption.common.beans.base;

import java.io.Serializable;
import java.util.Date;

/**
 * @author EyalO
 */
public class UsersDetailsHistory implements Serializable{

    private static final long serialVersionUID = 1L;
    private long id;
    private Date timeCreated;
    private long writerId;
    private long userId;
    private String userName;
    private long typeId;
    private String fieldBefore;
    private String fieldAfter;
    private Date lastReached;
    private long classId;
    private String fieldChanged;
    private Boolean isCheckboxField;
    private String email;
    private String writerFirstName;
    private String writerLastName;

    /**
     * @return the id
     */
    public long getId() {
        return id;
    }


    /**
     * @param id the id to set
     */
    public void setId(long id) {
        this.id = id;
    }


    /**
     * @return the timeCreated
     */
    public Date getTimeCreated() {
        return timeCreated;
    }


    /**
     * @param timeCreated the timeCreated to set
     */
    public void setTimeCreated(Date timeCreated) {
        this.timeCreated = timeCreated;
    }


    /**
     * @return the writerId
     */
    public long getWriterId() {
        return writerId;
    }


    /**
     * @param writerId the writerId to set
     */
    public void setWriterId(long writerId) {
        this.writerId = writerId;
    }


    /**
     * @return the userId
     */
    public long getUserId() {
        return userId;
    }


    /**
     * @param userId the userId to set
     */
    public void setUserId(long userId) {
        this.userId = userId;
    }


    /**
     * @return the userName
     */
    public String getUserName() {
        return userName;
    }


    /**
     * @param userName the userName to set
     */
    public void setUserName(String userName) {
        this.userName = userName;
    }

    
    /**
     * @return the lastReached
     */
    public Date getLastReached() {
        return lastReached;
    }


    /**
     * @param lastReached the lastReached to set
     */
    public void setLastReached(Date lastReached) {
        this.lastReached = lastReached;
    }


    /**
     * @return the classId
     */
    public long getClassId() {
        return classId;
    }


    /**
     * @param classId the classId to set
     */
    public void setClassId(long classId) {
        this.classId = classId;
    }
    
    
    /**
     * @return the typeId
     */
    public long getTypeId() {
        return typeId;
    }


    /**
     * @param typeId the typeId to set
     */
    public void setTypeId(long typeId) {
        this.typeId = typeId;
    }


    /**
     * @return the fieldBefore
     */
    public String getFieldBefore() {
        return fieldBefore;
    }


    /**
     * @param fieldBefore the fieldBefore to set
     */
    public void setFieldBefore(String fieldBefore) {
        this.fieldBefore = fieldBefore;
    }


    /**
     * @return the fieldAfter
     */
    public String getFieldAfter() {
        return fieldAfter;
    }


    /**
     * @param fieldAfter the fieldAfter to set
     */
    public void setFieldAfter(String fieldAfter) {
        this.fieldAfter = fieldAfter;
    }
    
    
    /**
     * @return the fieldChanged
     */
    public String getFieldChanged() {
        return fieldChanged;
    }


    /**
     * @param fieldChanged the fieldChanged to set
     */
    public void setFieldChanged(String fieldChanged) {
        this.fieldChanged = fieldChanged;
    }
    

    /**
     * @return the isCheckboxField
     */
    public Boolean getIsCheckboxField() {
        return isCheckboxField;
    }


    /**
     * @param isCheckboxField the isCheckboxField to set
     */
    public void setIsCheckboxField(Boolean isCheckboxField) {
        this.isCheckboxField = isCheckboxField;
    }
    
    /**
     * Constructs a <code>String</code> with all attributes
     * in name = value format.
     *
     * @return a <code>String</code> representation
     * of this object.
     */
    public String toString() {
        final String TAB = " \n ";
        String retValue = "";
        retValue = "API Issue ( "
            + super.toString() + TAB
            + "id = " + this.id + TAB
            + "timeCreated = " + this.timeCreated + TAB
            + "writerId = " + this.writerId + TAB
            + "userId = " + this.userId + TAB
            + "userName = " + this.userName + TAB
            + "typeId = " + this.typeId + TAB
            + "fieldChanged = " + this.fieldChanged + TAB
            + "fieldBefore = " + this.fieldBefore + TAB
            + "fieldAfter = " + this.fieldAfter + TAB
            + "lastReached = " + this.lastReached + TAB
            + "classId = " + this.classId + TAB
            + "isCheckboxField = " + this.isCheckboxField + TAB
            + " )";
        return retValue;
    }


	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}


	/**
	 * @param email the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}


	/**
	 * @return the writerFirstName
	 */
	public String getWriterFirstName() {
		return writerFirstName;
	}


	/**
	 * @param writerFirstName the writerFirstName to set
	 */
	public void setWriterFirstName(String writerFirstName) {
		this.writerFirstName = writerFirstName;
	}


	/**
	 * @return the writerLastName
	 */
	public String getWriterLastName() {
		return writerLastName;
	}


	/**
	 * @param writerLastName the writerLastName to set
	 */
	public void setWriterLastName(String writerLastName) {
		this.writerLastName = writerLastName;
	}

}
