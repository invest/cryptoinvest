package com.anyoption.common.beans.base;

import java.io.Serializable;

public class CreditCardType implements Serializable {

//  Credit Cards Types
    public static final int CC_TYPE_MASTERCARD = 1;
    public static final int CC_TYPE_VISA = 2;
    public static final int CC_TYPE_DINERS = 3;
    public static final int CC_TYPE_ISRACARD = 4;
    public static final int CC_TYPE_AMEX = 5;
    public static final int CC_TYPE_MAESTRO = 6;
    public static final int CC_TYPE_CUP = 7;
    
    // Credit card's type by credit card's first digits number
    public static final int CC_TYPE_IDENTIFY_VISA = 4;
    public static final int CC_TYPE_IDENTIFY_MASTERCARD_START = 51;
    public static final int CC_TYPE_IDENTIFY_MASTERCARD_END = 55;
    public static final int CC_TYPE_IDENTIFY_AMEX_A = 34;
    public static final int CC_TYPE_IDENTIFY_AMEX_B = 37;

	/**
	 *
	 */
	private static final long serialVersionUID = -8114032561770149806L;
	private long id;
	private String name;
    private boolean threeDSecure;
    private String description;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public boolean isThreeDSecure() {
        return threeDSecure;
    }

    public void setThreeDSecure(boolean threeDSecure) {
        this.threeDSecure = threeDSecure;
    }
    
    public String toString() {
	    String ls = System.getProperty("line.separator");
	    return ls + "CreditCardType:" + ls
	        + "id: " + getId() + ls
	        + "name: " + getName() + ls
            + "threeDSecure (3D): " + isThreeDSecure() + ls
            + "description: " + getDescription() + ls;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	} 
} 