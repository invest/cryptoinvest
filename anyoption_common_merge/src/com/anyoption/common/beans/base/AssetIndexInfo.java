package com.anyoption.common.beans.base;

import java.io.Serializable;
import java.util.List;
public class AssetIndexInfo implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private long id;
	private long marketId;
	private long skinId;
	private String marketDescription;
	private String additionalText;
	private String pageTitle;
	private String pageKeyWords;
	private String pageDescription;
	private String marketDescriptionPage;
	private List<AssetIndexLevelCalc> assetIndexLevelCalc;
	private boolean is24h7;
	private long opportunityType; 
		
	
	public long getMarketId() {
		return marketId;
	}
	public void setMarketId(long marketId) {
		this.marketId = marketId;
	}
	public long getSkinId() {
		return skinId;
	}
	public void setSkinId(long skinId) {
		this.skinId = skinId;
	}
	public String getMarketDescription() {
		return marketDescription;
	}
	public void setMarketDescription(String marketDescription) {
		this.marketDescription = marketDescription;
	}
	public String getAdditionalText() {
		return additionalText;
	}
	public void setAdditionalText(String additionalText) {
		this.additionalText = additionalText;
	}
	public String getPageTitle() {
		return pageTitle;
	}
	public void setPageTitle(String pageTitle) {
		this.pageTitle = pageTitle;
	}
	public String getPageKeyWords() {
		return pageKeyWords;
	}
	public void setPageKeyWords(String pageKeyWords) {
		this.pageKeyWords = pageKeyWords;
	}
	public String getPageDescription() {
		return pageDescription;
	}
	public void setPageDescription(String pageDescription) {
		this.pageDescription = pageDescription;
	}
	public String getMarketDescriptionPage() {
		return marketDescriptionPage;
	}
	public void setMarketDescriptionPage(String marketDescriptionPage) {
		this.marketDescriptionPage = marketDescriptionPage;
	}
	public List<AssetIndexLevelCalc> getAssetIndexLevelCalc() {
		return assetIndexLevelCalc;
	}
	public void setAssetIndexLevelCalc(List<AssetIndexLevelCalc> assetIndexLevelCalc) {
		this.assetIndexLevelCalc = assetIndexLevelCalc;
	}
	public boolean isIs24h7() {
		return is24h7;
	}
	public void setIs24h7(boolean is24h7) {
		this.is24h7 = is24h7;
	}
	public long getOpportunityType() {
		return opportunityType;
	}
	public void setOpportunityType(long opportunityType) {
		this.opportunityType = opportunityType;
	}
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}

}
