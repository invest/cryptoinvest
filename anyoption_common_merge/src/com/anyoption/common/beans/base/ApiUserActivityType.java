package com.anyoption.common.beans.base;

import java.io.Serializable;

/**
 * @author EranL
 *
 */
public class ApiUserActivityType implements Serializable {

	private static final long serialVersionUID = 1L; 
	private long id;
	private String name;
	
	public final static int API_USER_ACTIVITY_TYPE_INSERT_CALL_ME		= 1;
	public final static int API_USER_ACTIVITY_TYPE_INSERT_USER 	  		= 2;
	public final static int API_USER_ACTIVITY_TYPE_INSERT_BINARY_INV 	= 3;
	
	/**
	 * @return the id
	 */
	public long getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(long id) {
		this.id = id;
	}
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	
}
