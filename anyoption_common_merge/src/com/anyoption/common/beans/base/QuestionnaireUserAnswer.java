/**
 * 
 */
package com.anyoption.common.beans.base;

import java.io.Serializable;
import java.util.Date;

/**
 * @author pavelhe
 *
 */
public class QuestionnaireUserAnswer implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private long userAnswerId;
	private Long answerId;
	private long userId;
	private long questionId;
	private long groupId;
	private String textAnswer;
	private Date timeCreated;
	// Always set writer ID !!!
	private long writerId;
	private Long score;
	private Long transactionId;
	
	
	/**
	 * @return the userAnswerId
	 */
	public long getUserAnswerId() {
		return userAnswerId;
	}
	/**
	 * @param userAnswerId the userAnswerId to set
	 */
	public void setUserAnswerId(long userAnswerId) {
		this.userAnswerId = userAnswerId;
	}
	/**
	 * @return the answerId
	 */
	public Long getAnswerId() {
		return answerId;
	}
	/**
	 * @param answerId the answerId to set
	 */
	public void setAnswerId(Long answerId) {
		this.answerId = answerId;
	}
	/**
	 * @return the userId
	 */
	public long getUserId() {
		return userId;
	}
	/**
	 * @param userId the userId to set
	 */
	public void setUserId(long userId) {
		this.userId = userId;
	}
	/**
	 * @return the questionId
	 */
	public long getQuestionId() {
		return questionId;
	}
	/**
	 * @param questionId the questionId to set
	 */
	public void setQuestionId(long questionId) {
		this.questionId = questionId;
	}
	/**
	 * @return the groupId
	 */
	public long getGroupId() {
		return groupId;
	}
	/**
	 * @param groupId the groupId to set
	 */
	public void setGroupId(long groupId) {
		this.groupId = groupId;
	}
	/**
	 * @return the textAnswer
	 */
	public String getTextAnswer() {
		return textAnswer;
	}
	/**
	 * @param textAnswer the textAnswer to set
	 */
	public void setTextAnswer(String textAnswer) {
		this.textAnswer = textAnswer;
	}
	/**
	 * @return the timeCreated
	 */
	public Date getTimeCreated() {
		return timeCreated;
	}
	/**
	 * @param timeCreated the timeCreated to set
	 */
	public void setTimeCreated(Date timeCreated) {
		this.timeCreated = timeCreated;
	}
	
	/**
	 * @return the writerId
	 */
	public long getWriterId() {
		return writerId;
	}
	/**
	 * @param writerId the writerId to set
	 */
	public void setWriterId(long writerId) {
		this.writerId = writerId;
	}
	
	public Long getScore() {
		return score;
	}
	public void setScore(Long score) {
		this.score = score;
	}
	
	public Long getTransactionId() {
		return transactionId;
	}
	public void setTransactionId(Long transactionId) {
		this.transactionId = transactionId;
	}


	@Override
	public String toString() {
		return "QuestionnaireUserAnswer [userAnswerId=" + userAnswerId + ", answerId=" + answerId + ", userId=" + userId
				+ ", questionId=" + questionId + ", groupId=" + groupId + ", textAnswer=" + textAnswer
				+ ", timeCreated=" + timeCreated + ", writerId=" + writerId
				+ ", score=" + score + ", transactionId=" + transactionId + "]";
	}
}
