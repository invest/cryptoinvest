package com.anyoption.common.beans.base;

import java.util.Date;

public class MarketingTrackingCookieDynamic implements java.io.Serializable{

	private static final long serialVersionUID = -5149043541284386194L;
	public static final String EMPTY_STRING = "";
	
	private String http;
	private String cd;
	private Date td;
	private String dp;
    private String aff_sub1;
    private String aff_sub2;
    private String aff_sub3;
	private String utmSource;
	/**
	 * @return the http
	 */
	public String getHttp() {
		return http;
	}
	/**
	 * @param http the http to set
	 */
	public void setHttp(String http) {
		this.http = http;
	}
	/**
	 * @return the cd
	 */
	public String getCd() {
		return cd;
	}
	/**
	 * @param cd the cd to set
	 */
	public void setCd(String cd) {
		this.cd = cd;
	}
	/**
	 * @return the td
	 */
	public Date getTd() {
		return td;
	}
	/**
	 * @param td the td to set
	 */
	public void setTd(Date td) {
		this.td = td;
	}
	/**
	 * @return the dp
	 */
	public String getDp() {
		return dp;
	}
	/**
	 * @param dp the dp to set
	 */
	public void setDp(String dp) {
	    if(dp == null || dp.contains("null")){
	        this.dp = EMPTY_STRING;
	    } else {
	        this.dp = dp;   
	    }
	}
	public String getUtmSource() {
		return utmSource;
	}
	
	public void setUtmSource(String utmSource) {
		this.utmSource = utmSource;
	}
	
	public String getAff_sub1() {
		return aff_sub1;
	}
	
	public void setAff_sub1(String aff_sub1) {
		this.aff_sub1 = aff_sub1;
	}
	
	public String getAff_sub2() {
		return aff_sub2;
	}
	
	public void setAff_sub2(String aff_sub2) {
		this.aff_sub2 = aff_sub2;
	}
	
	public String getAff_sub3() {
		return aff_sub3;
	}
	
	public void setAff_sub3(String aff_sub3) {
		this.aff_sub3 = aff_sub3;
	}
}
