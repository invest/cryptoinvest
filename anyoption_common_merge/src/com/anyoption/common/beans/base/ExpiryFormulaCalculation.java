package com.anyoption.common.beans.base;

import java.io.Serializable;

public class ExpiryFormulaCalculation implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private long expiryTypeId;
	private String reutersField;
	private long expiryFormulaId;
	private String expiryFormulaTxt;
	
	public long getExpiryTypeId() {
		return expiryTypeId;
	}
	
	public void setExpiryTypeId(long expiryTypeId) {
		this.expiryTypeId = expiryTypeId;
	}
	
	public String getReutersField() {
		return reutersField;
	}
	
	public void setReutersField(String reutersField) {
		this.reutersField = reutersField;
	}
	
	public long getExpiryFormulaId() {
		return expiryFormulaId;
	}
	public void setExpiryFormulaId(long expiryFormulaId) {
		this.expiryFormulaId = expiryFormulaId;
	}
	public String getExpiryFormulaTxt() {
		return expiryFormulaTxt;
	}
	
	public void setExpiryFormulaTxt(String expiryFormulaTxt) {
		this.expiryFormulaTxt = expiryFormulaTxt;
	}
}
