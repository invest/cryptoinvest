package com.anyoption.common.beans;

/**
 * @author liors
 *
 */
public class RewardTier implements java.io.Serializable, Cloneable {
	private static final long serialVersionUID = -309171211050383463L;
	private int id; 
	private String name;
	private int minExperiencePoints;
	private int inActiveDays;
	private int experiencePointsToReduce;
	private int daysToDemote;
	
	public static final int TIER_NOOB_0 = 0;
	public static final int TIER_TRADER_1 = 1;
	public static final int TIER_VETERAN_TRADER_2 = 2;
	public static final int TIER_BROKER_3 = 3;
	public static final int TIER_ANYOPTION_WINNER_4 = 4;
	
	public RewardTier() {
		
	}
	
	/**
	 * @param id
	 * @param name
	 * @param minExperiencePoints
	 * @param inActiveDays
	 * @param experiencePointsToReduce
	 * @param daysToDemote
	 */
	public RewardTier(int id, String name, int minExperiencePoints, int inActiveDays, int experiencePointsToReduce, int daysToDemote) {
		this.id = id;
		this.name = name;
		this.minExperiencePoints = minExperiencePoints;
		this.inActiveDays = inActiveDays;
		this.experiencePointsToReduce = experiencePointsToReduce;
		this.daysToDemote = daysToDemote;
	}
	
	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * @return the minExperiencePoints
	 */
	public int getMinExperiencePoints() {
		return minExperiencePoints;
	}
	/**
	 * @param minExperiencePoints the minExperiencePoints to set
	 */
	public void setMinExperiencePoints(int minExperiencePoints) {
		this.minExperiencePoints = minExperiencePoints;
	}
	/**
	 * @return the inActiveDays
	 */
	public int getInActiveDays() {
		return inActiveDays;
	}
	/**
	 * @param inActiveDays the inActiveDays to set
	 */
	public void setInActiveDays(int inActiveDays) {
		this.inActiveDays = inActiveDays;
	}
	/**
	 * @return the experiencePointsToReduce
	 */
	public int getExperiencePointsToReduce() {
		return experiencePointsToReduce;
	}
	/**
	 * @param experiencePointsToReduce the experiencePointsToReduce to set
	 */
	public void setExperiencePointsToReduce(int experiencePointsToReduce) {
		this.experiencePointsToReduce = experiencePointsToReduce;
	}
	/**
	 * @return the daysToDemote
	 */
	public int getDaysToDemote() {
		return daysToDemote;
	}
	/**
	 * @param daysToDemote the daysToDemote to set
	 */
	public void setDaysToDemote(int daysToDemote) {
		this.daysToDemote = daysToDemote;
	}
	
	@Override
	public String toString() {
		return "RewardTier [id=" + id + ", name=" + name
				+ ", minExperiencePoints=" + minExperiencePoints
				+ ", inActiveDays=" + inActiveDays
				+ ", experiencePointsToReduce=" + experiencePointsToReduce
				+ ", daysToDemote=" + daysToDemote + "]";
	}

	/**
	 * @author liors
	 *  @param tierId 
	 *  @param isMoveTier
	 */
	public static class TierChanges {
		private int tierId;
		private boolean isMoveTier;
		
		public TierChanges() {
			
		}
		
		public TierChanges(int tierId, boolean isMoveTier) {
			setTierChanges(tierId, isMoveTier);
		}

		/**
		 * @return the tierId
		 */
		public int getTierId() {
			return tierId;
		}

		/**
		 * @param tierId the tierId to set
		 */
		public void setTierId(int tierId) {
			this.tierId = tierId;
		}

		/**
		 * @return the isMoveTier
		 */
		public boolean isMoveTier() {
			return isMoveTier;
		}

		/**
		 * @param isMoveTier the isMoveTier to set
		 */
		public void setMoveTier(boolean isMoveTier) {
			this.isMoveTier = isMoveTier;
		}
		
		public void setTierChanges(int tierId, boolean isMoveTier) {
			this.tierId = tierId;
			this.isMoveTier = isMoveTier;
		}

		@Override
		public String toString() {
			return "TierChanges [tierId=" + tierId + ", isMoveTier="
					+ isMoveTier + "]";
		}
	}
	
	@Override
	public Object clone() throws CloneNotSupportedException {
		return super.clone();
	}
	
}
