package com.anyoption.common.service.withdrawuserdetails;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.base.WithdrawUserDetails;
import com.anyoption.common.daos.DAOBase;

import oracle.jdbc.OracleTypes;

public class WithdrawUserDetailsDAO extends DAOBase {
	
	private static final Logger logger = Logger.getLogger(WithdrawUserDetailsDAO.class);
	public static void insertWithdrawUserDetailas(Connection conn, WithdrawUserDetails wud) throws SQLException{		
		CallableStatement cstmt = null;
		int index = 1;
		try {
			cstmt = conn.prepareCall("{call pkg_withdraw_backend.create_withdraw_user_details("
																				+ "o_id => ? " +
																				 ",i_user_id => ? " + 
																				 ",i_beneficiary_name => ? " +
																				 ",i_swift => ? " +
																				 ",i_iban => ? " +
																				 ",i_account_num => ? " +
																				 ",i_bank_name => ?" +
																				 ",i_branch_number => ?" +
																				 ",i_branch_address => ?" +
																				 ",i_file_id => ?" +
																				 ",i_skrill_email => ?" +
																				 ",i_crteated_writer_id => ?" 
																				+ ")}");

			cstmt.registerOutParameter(index++, OracleTypes.INTEGER);
			setStatementValue(wud.getUserId(), index++, cstmt);			
			setStatementValue(wud.getBeneficiaryName(), index++, cstmt);			
			setStatementValue(wud.getSwift(), index++, cstmt);			
			setStatementValue(wud.getIban(), index++, cstmt);
			setStatementValue(wud.getAccountNum(), index++, cstmt);
			setStatementValue(wud.getBankName(), index++, cstmt);
			setStatementValue(wud.getBranchNumber(), index++, cstmt);
			setStatementValue(wud.getBranchAddress(), index++, cstmt);
			setStatementValue(wud.getFileId(), index++, cstmt);
			setStatementValue(wud.getSkrillEmail(), index++, cstmt);
			setStatementValue(wud.getCreatedWriterId(), index++, cstmt);
			
			cstmt.executeUpdate();		
			wud.setId(cstmt.getLong(1));
			logger.debug("WithdrawUserDetails was inserted: " + wud);

		} finally {
			closeStatement(cstmt);
		}
	}
	
	public static void updateWithdrawWireUserDetils(Connection conn, WithdrawUserDetails wud) throws SQLException{		
		CallableStatement cstmt = null;
		int index = 1;
		try {
			cstmt = conn.prepareCall("{call pkg_withdraw_backend.update_wthdr_wire_user_details("
																				+ "i_user_id => ? " + 
																				 ",i_beneficiary_name => ? " +
																				 ",i_swift => ? " +
																				 ",i_iban => ? " +
																				 ",i_account_num => ? " +
																				 ",i_bank_name => ?" +
																				 ",i_branch_number => ?" +
																				 ",i_branch_address => ?" +
																				 ",i_file_id => ?" +
																				 ",i_writer_id => ? "
																				+ ")}");

			setStatementValue(wud.getUserId(), index++, cstmt);			
			setStatementValue(wud.getBeneficiaryName(), index++, cstmt);			
			setStatementValue(wud.getSwift(), index++, cstmt);			
			setStatementValue(wud.getIban(), index++, cstmt);
			setStatementValue(wud.getAccountNum(), index++, cstmt);
			setStatementValue(wud.getBankName(), index++, cstmt);
			setStatementValue(wud.getBranchNumber(), index++, cstmt);
			setStatementValue(wud.getBranchAddress(), index++, cstmt);
			setStatementValue(wud.getFileId(), index++, cstmt);
			setStatementValue(wud.getUpdatedWriterId(), index++, cstmt);
			
			cstmt.executeUpdate();			
			logger.debug("updateWithdrawWireUserDetilas was updated: " + wud);

		} finally {
			closeStatement(cstmt);
		}
	}
	
	public static void updateWithdrawSkrillUserDetils(Connection conn, WithdrawUserDetails wud) throws SQLException{		
		CallableStatement cstmt = null;
		int index = 1;
		try {
			cstmt = conn.prepareCall("{call pkg_withdraw_backend.update_wthdr_skrl_user_details("
																				+ "i_user_id => ? "  
																				+ ",i_skrill_email => ? "
																				+ ",i_writer_id => ? "
																				+ ")}");

			cstmt.setLong(index++, wud.getUserId());
			cstmt.setString(index++, wud.getSkrillEmail());
			setStatementValue(wud.getUpdatedWriterId(), index++, cstmt);
			
			cstmt.executeUpdate();			
			logger.debug("updateWithdrawSkrillUserDetilas was updated: " + wud);

		} finally {
			closeStatement(cstmt);
		}
	}
	
	public static void updateWithdrawFileIdUserDetils(Connection conn, long userId, long fileId, long writerId) throws SQLException{		
		CallableStatement cstmt = null;
		int index = 1;
		try {
			cstmt = conn.prepareCall("{call pkg_withdraw_backend.update_wthdr_file_id("
																				+ "i_user_id => ? " 
																				+ ",i_file_id => ? "
																				+ ",i_writer_id=> ? "
																				+ ")}");

			cstmt.setLong(index++, userId);
			cstmt.setLong(index++, fileId);
			setStatementValue(writerId, index++, cstmt);
			
			cstmt.executeUpdate();			
			logger.debug("updateWithdrawFileIdUserDetils with fileId: " + fileId);
		} finally {
			closeStatement(cstmt);
		}
	}
	
	  public static WithdrawUserDetails getWithdrawUserDetailas(Connection con, long userId) throws SQLException {
		  	WithdrawUserDetails wud = null;
			CallableStatement cstmt = null;
			ResultSet rs = null;
			int index = 1;
			try {
				String sql = "{call pkg_withdraw_backend.get_withdraw_user_details(?, ?)}";
				cstmt = con.prepareCall(sql);
				cstmt.registerOutParameter(index++, OracleTypes.CURSOR);
				cstmt.setLong(index++, userId);
				cstmt.executeQuery();
				
				rs = (ResultSet) cstmt.getObject(1);
				if (rs.next()) {
					wud = new WithdrawUserDetails();
					wud.setId(rs.getLong("ID"));
					wud.setUserId(rs.getLong("USER_ID"));
					wud.setBeneficiaryName(rs.getString("BENEFICIARY_NAME"));
					wud.setSwift(rs.getString("SWIFT"));
					wud.setIban(rs.getString("IBAN"));
					wud.setAccountNum(rs.getLong("ACCOUNT_NUM"));
					wud.setBankName(rs.getString("BANK_NAME"));
					wud.setBranchNumber(rs.getLong("BRANCH_NUMBER"));
					wud.setBranchAddress(rs.getString("BRANCH_ADDRESS"));
					wud.setFileId(rs.getLong("FILE_ID"));
					wud.setFileName(rs.getString("FILE_NAME"));
					wud.setSkrillEmail(rs.getString("SKRILL_EMAIL"));
					wud.setCreatedWriterId(rs.getLong("CREATED_WRITER_ID"));					
				}
			} finally {
				closeStatement(cstmt);			
			}
			return wud;
	  }

	public static void skipedWithdrawBankUserDetils(Connection conn, long userId, long issueActionId) throws SQLException{		
		CallableStatement cstmt = null;
		int index = 1;
		try {
			cstmt = conn.prepareCall("{call pkg_withdraw_backend.skiped_wthdr_bank_user_details("
																				+ "i_user_id => ? " + 
																				 ",i_issue_action_id => ? "
																				+ ")}");
	
			cstmt.setLong(index++, userId);
			cstmt.setLong(index++, issueActionId);
			
			cstmt.executeUpdate();			
			logger.debug("skipedWithdrawBankUserDetils userId: " + userId + " with issueActionId:" + issueActionId);
	
		} finally {
			closeStatement(cstmt);
		}
	}
	
	public static boolean showWithdrawUserDetailas(Connection conn, long userId) throws SQLException{		
		CallableStatement cstmt = null;
		boolean res;
		int index = 1;
		try {
			cstmt = conn.prepareCall("{call pkg_withdraw_backend.show_wthdr_details_popup("
																				+ "o_show_popup => ? " +
																				 ",i_user_id => ? "
																				+ ")}");

			cstmt.registerOutParameter(index++, OracleTypes.INTEGER);
			setStatementValue(userId, index++, cstmt);			
			cstmt.executeUpdate();		
			res = cstmt.getBoolean(1);
			return res;
		} finally {
			closeStatement(cstmt);
		}
	}
}
