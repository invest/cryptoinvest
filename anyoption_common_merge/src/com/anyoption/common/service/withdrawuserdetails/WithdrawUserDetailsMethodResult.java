package com.anyoption.common.service.withdrawuserdetails;

import com.anyoption.common.beans.base.WithdrawUserDetails;
import com.anyoption.common.service.results.MethodResult;

public class WithdrawUserDetailsMethodResult extends MethodResult {
	
	private WithdrawUserDetails withdrawUserDetails;
	private boolean showPopup;

	public WithdrawUserDetails getWithdrawUserDetails() {
		return withdrawUserDetails;
	}

	public void setWithdrawUserDetails(WithdrawUserDetails withdrawUserDetails) {
		this.withdrawUserDetails = withdrawUserDetails;
	}

	public boolean isShowPopup() {
		return showPopup;
	}

	public void setShowPopup(boolean showPopup) {
		this.showPopup = showPopup;
	}
}
