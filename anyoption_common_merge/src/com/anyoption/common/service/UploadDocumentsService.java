package com.anyoption.common.service;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Map;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.File;
import com.anyoption.common.beans.FileType;
import com.anyoption.common.beans.User;
import com.anyoption.common.beans.base.WithdrawUserDetails;
import com.anyoption.common.managers.FilesManagerBase;
import com.anyoption.common.managers.UserRegulationManager;
import com.anyoption.common.service.results.MethodResult;
import com.anyoption.common.service.userdocuments.UserDocumentsManager;
import com.anyoption.common.service.withdrawuserdetails.WithdrawUserDetailsManager;
import com.anyoption.common.util.CommonUtil;
import com.invest.common.documents.DocumentsHelper;
import com.invest.common.email.beans.EmailTemplate;
import com.invest.common.email.factories.EmailProviderFactory;
import com.invest.common.email.factories.EmailTemplateFactory;

public class UploadDocumentsService {

	private static final Logger log = Logger.getLogger(UploadDocumentsService.class);

	public static void uploadFile(BufferedInputStream bis, User user, long fileType, String fileName, long ccId, int writerId, MethodResult result) {
		log.debug("uploadFile method, collected parameters: " + logParams(fileType, fileName, ccId, writerId));
		if(fileType == FileType.BANK_WIRE_PROTOCOL_FILE){
			uploadBankWireProtocolFile(bis, user, fileType, fileName, writerId, result);
		} else {
			try {
                fileName = FilesManagerBase.saveFile(fileName, bis, user.getId(), fileType, writerId == 0 ? true : false, false);
			} catch (IOException e) {
				log.debug("Cannot save file", e);
				if (result != null) {
					result.setErrorCode(CommonJSONService.ERROR_CODE_UNKNOWN);
				}
				return;
			}
			if (!com.anyoption.common.util.Utils.validateFileExtension(fileName)) {
				log.debug("File extension not allowed: " + fileName);
				FilesManagerBase.deleteFileByName(fileName);
				if (result != null) {
					result.setErrorCode(CommonJSONService.ERROR_CODE_UNKNOWN);
				}
				return;
			}
			if (fileName != null) {
				Long creditCardId = (ccId == 0) ? null : ccId;
				UserDocumentsManager.uploadUserDocs(user.getId(), fileType, writerId, fileName, creditCardId);
				if(fileType == FileType.CNMV_FILE){
					try {
						UserRegulationManager.CNMVFileUnSuspend(user, writerId);
					} catch (SQLException e) {
						log.error("When CNMVFileUnSuspend", e);
					}
				}
				if (FilesManagerBase.isCheckDocumentsStatus(fileType)) {
					Map<Long, File> documents = FilesManagerBase.getDocuments(user.getId());
					if (DocumentsHelper.isPassReceived(fileType, documents) || DocumentsHelper.isIdReceived(fileType, documents)) {
						EmailTemplate template = EmailTemplateFactory.getDocumentsReceivedTemplate(	user.getId(),
																									CommonUtil.getConfig("email.from"),
																									user.getEmail(), null,
																									user.getFirstName(),
																									CommonUtil.getConfig("email.template.homepage"),
																									CommonUtil.getConfig("email.support"));
						EmailProviderFactory.getEmailProvider().scheduleTemplateEmail(template);
					}
				}
			} else {
				if (result != null) {
					result.setErrorCode(CommonJSONService.ERROR_CODE_FILE_TOO_LARGE);
				}
			}
		}
	}

	private static void uploadBankWireProtocolFile(BufferedInputStream bis, User user, long fileType, String fileName, int writerId, MethodResult result) {
		try {
			WithdrawUserDetails wud = WithdrawUserDetailsManager.getWithdrawUserDetails(user.getId());
			if(wud != null){	
				fileName = FilesManagerBase.saveFile(fileName, bis, user.getId(), fileType, false, false);
				log.debug("Uploaded Bank wire protocol file with fileName:" + fileName + " for userId:" + user.getId());
				FilesManagerBase.insertUploadFile(user.getId(), fileType, writerId, fileName);
			} else {
				log.debug("Can't find WithdrawUserDetails and can't upload bank wire file for userId:" + user.getId());
				result.setErrorCode(CommonJSONService.ERROR_CODE_UNKNOWN);
			}				
		} catch (Exception e) {
			log.debug("Cannot save file", e);
			result.setErrorCode(CommonJSONService.ERROR_CODE_UNKNOWN);
			return;
		}
	}

	private static String logParams(long fileType, String fileName, long ccId, int writerId) {
		String ls = System.getProperty("line.separator");
		return ls + "fileType: " + fileType + ls
				+ "fileName: " + fileName + ls
				+ "ccId: " + ccId + ls
				+ "writerId: " + writerId + ls;
	}
}