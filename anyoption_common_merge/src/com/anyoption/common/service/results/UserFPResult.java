package com.anyoption.common.service.results;

public class UserFPResult extends MethodResult {
	public String obfuscatedEmail;
	public String obfuscatedPhone;
	
	public String getObfuscatedEmail() {
		return obfuscatedEmail;
	}
	public void setObfuscatedEmail(String obfuscatedEmail) {
		this.obfuscatedEmail = obfuscatedEmail;
	}
	public String getObfuscatedPhone() {
		return obfuscatedPhone;
	}
	public void setObfuscatedPhone(String obfuscatedPhone) {
		this.obfuscatedPhone = obfuscatedPhone;
	}
	@Override
	public String toString() {
		return "UserFPResult [obfuscatedUser=" + obfuscatedEmail + ", obfuscatedPhone=" + obfuscatedPhone + "]";
	}
}
