package com.anyoption.common.service.results;

import java.util.ArrayList;
import java.util.Date;

import com.anyoption.common.beans.base.Investment;
import com.google.gson.annotations.Expose;

public class ChartDataResult extends UserMethodResult {
	@Expose
	private long marketId;
	@Expose
	private long opportunityId;
	@Expose
	private Date timeEstClosing;
	@Expose
	private int scheduled;
	@Expose
	private long decimalPoint;
	private Date timeFirstInvest;
	private Date timeLastInvest;
	@Expose
	private Investment[] investments;
	@Expose
	private long opportunityTypeId;
	private String serverTime;
	private String timezoneOffset;
	private String timeEstClose;
	private boolean hasPreviousHour;
	private double balance;
	private double eventLevel; //opportunity static(open) level 
	private double maxInvAmountCoeffPerUser;
	@Expose
	private ArrayList<Long> ratesTimes;
	@Expose
	private ArrayList<Double> rates;
	private ArrayList<Double> lasts;
	private ArrayList<Double> asks;
	private ArrayList<Double> bids;
	
	/**
	 * @return
	 */
	public Date getTimeFirstInvest() {
		return timeFirstInvest;
	}

	/**
	 * @param timeFirstInvest
	 */
	public void setTimeFirstInvest(Date timeFirstInvest) {
		this.timeFirstInvest = timeFirstInvest;
	}

	/**
	 * @return timeLastInvest
	 */
	public Date getTimeLastInvest() {
		return timeLastInvest;
	}

	/**
	 * @param timeLastInvest
	 */
	public void setTimeLastInvest(Date timeLastInvest) {
		this.timeLastInvest = timeLastInvest;
	}

	/**
	 * @return
	 */
	public long getDecimalPoint() {
		return decimalPoint;
	}

	/**
	 * @param decimalPoint
	 */
	public void setDecimalPoint(long decimalPoint) {
		this.decimalPoint = decimalPoint;
	}

	/**
	 * @return time Estimation Closing - The time when the opportunity is
	 *         supposed to settle
	 */
	public Date getTimeEstClosing() {
		return timeEstClosing;
	}

	/**
	 * @param timeEstClosing
	 */
	public void setTimeEstClosing(Date timeEstClosing) {
		this.timeEstClosing = timeEstClosing;
	}

	/**
	 * @return
	 */
	public int getScheduled() {
		return scheduled;
	}

	/**
	 * @param scheduled
	 */
	public void setScheduled(int scheduled) {
		this.scheduled = scheduled;
	}

	/**
	 * @return market Id
	 */
	public long getMarketId() {
		return marketId;
	}

	/**
	 * @param marketId
	 */
	public void setMarketId(long marketId) {
		this.marketId = marketId;
	}

	/**
	 * @return opportunity Id
	 */
	public long getOpportunityId() {
		return opportunityId;
	}

	/**
	 * @param opportunityId
	 */
	public void setOpportunityId(long opportunityId) {
		this.opportunityId = opportunityId;
	}

	/**
	 * @return list of investments
	 */
	public Investment[] getInvestments() {
		return investments;
	}

	/**
	 * @param investments
	 */
	public void setInvestments(Investment[] investments) {
		this.investments = investments;
	}

	/**
	 * @return opportunity Type Id
	 */
	public long getOpportunityTypeId() {
		return opportunityTypeId;
	}

	/**
	 * @param opportunityTypeId
	 */
	public void setOpportunityTypeId(long opportunityTypeId) {
		this.opportunityTypeId = opportunityTypeId;
	}

	public String getServerTime() {
		return serverTime;
	}

	public void setServerTime(String serverTime) {
		this.serverTime = serverTime;
	}

	public String getTimezoneOffset() {
		return timezoneOffset;
	}

	public void setTimezoneOffset(String timezoneOffset) {
		this.timezoneOffset = timezoneOffset;
	}

	public String getTimeEstClose() {
		return timeEstClose;
	}

	public void setTimeEstClose(String timeEstClose) {
		this.timeEstClose = timeEstClose;
	}

	public boolean isHasPreviousHour() {
		return hasPreviousHour;
	}

	public void setHasPreviousHour(boolean hasPreviousHour) {
		this.hasPreviousHour = hasPreviousHour;
	}


	public double getBalance() {
		return balance;
	}

	public void setBalance(double balance) {
		this.balance = balance;
	}

	public double getMaxInvAmountCoeffPerUser() {
		return maxInvAmountCoeffPerUser;
	}

	public void setMaxInvAmountCoeffPerUser(double maxInvAmountCoeffPerUser) {
		this.maxInvAmountCoeffPerUser = maxInvAmountCoeffPerUser;
	}

	public double getEventLevel() {
		return eventLevel;
	}

	public void setEventLevel(double eventLevel) {
		this.eventLevel = eventLevel;
	}

	public ArrayList<Long> getRatesTimes() {
		return ratesTimes;
	}

	public void setRatesTimes(ArrayList<Long> ratesTimes) {
		this.ratesTimes = ratesTimes;
	}

	public ArrayList<Double> getRates() {
		return rates;
	}

	public void setRates(ArrayList<Double> rates) {
		this.rates = rates;
	}

	public ArrayList<Double> getLasts() {
		return lasts;
	}

	public void setLasts(ArrayList<Double> lasts) {
		this.lasts = lasts;
	}

	public ArrayList<Double> getAsks() {
		return asks;
	}

	public void setAsks(ArrayList<Double> asks) {
		this.asks = asks;
	}

	public ArrayList<Double> getBids() {
		return bids;
	}

	public void setBids(ArrayList<Double> bids) {
		this.bids = bids;
	}
}