package com.anyoption.common.service.results;

import java.util.ArrayList;

import com.google.gson.annotations.Expose;

/**
 * Contain the messages for the results.
 * 
 * @author liors
 *
 */
public class MethodResult {
    protected int errorCode;
    protected ArrayList<ErrorMessage> errorMessages;
    @Deprecated
	@Expose
    protected ArrayList<ErrorMessage> userMessages;
    @Deprecated
	@Expose
    protected String apiCode; //API error code
    @Deprecated
	@Expose
    protected String apiCodeDescription; //API error code description  
	private int responseCode;
    
    /**
     * @return
     */
    public int getErrorCode() {
        return errorCode;
    }

    /**
     * @param errorCode
     */
    public void setErrorCode(int errorCode) {
        this.errorCode = errorCode;
    }

    public void addErrorMessage(String field, int errorCode) {
        if (null == errorMessages) {
            errorMessages = new ArrayList<ErrorMessage>();
        }
        errorMessages.add(new ErrorMessage(field, errorCode));
    }

    /**
     * @param field
     * @param message
     */
    public void addErrorMessage(String field, String message) {
        if (null == errorMessages) {
            errorMessages = new ArrayList<ErrorMessage>();
        }
        errorMessages.add(new ErrorMessage(field, message));
    }

    /**
     * @param errorMessages
     */
    public void addErrorMessages(ArrayList<ErrorMessage> errorMessages) {
        if (null == this.errorMessages) {
            this.errorMessages = new ArrayList<>();
        }
        this.errorMessages.addAll(errorMessages);
    }

    /**
     * @return
     */
    public ErrorMessage[] getErrorMessages() {
        if (null == errorMessages) {
            return null;
        }
        return errorMessages.toArray(new ErrorMessage[errorMessages.size()]);
    }

    /**
     * @param field
     * @param message
     */
    public void addUserMessage(String field, String message) {
        if (null == userMessages) {
        	userMessages = new ArrayList<ErrorMessage>();
        }
        userMessages.add(new ErrorMessage(field, message));
    }
    
    /**
     * @param field
     * @param message
     */
    public void addUserMessage(String field, String message, String apiErrorCode) {
        if (null == userMessages) {
        	userMessages = new ArrayList<ErrorMessage>();
        }
        userMessages.add(new ErrorMessage(field, message, apiErrorCode));
    }

    /**
     * @param userMessages
     */
    public void addUserMessages(ArrayList<ErrorMessage> userMessages) {
        if (null == this.userMessages) {
            this.userMessages = userMessages;
            return;
        }
        this.userMessages.addAll(userMessages);
    }

    /**
     * @return
     */
    public ErrorMessage[] getUserMessages() {
        if (null == userMessages) {
            return null;
        }
        return userMessages.toArray(new ErrorMessage[userMessages.size()]);
    }
    
    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    public String toString() {
        String ls = System.getProperty("line.separator");
        return ls + "MethodResult: " + ls
        	+ super.toString() + ls
            + "errorCode: " + errorCode + ls
            + "errorMessages: " + errorMessages + ls
            + "userMessages: " +userMessages  + ls;
    }

	/**
	 * @return the apiCode
	 */
	public String getApiCode() {
		return apiCode;
	}

	/**
	 * @param apiCode the apiCode to set
	 */
	public void setApiCode(String apiCode) {
		this.apiCode = apiCode;
	}

	/**
	 * @return the apiCodeDescription
	 */
	public String getApiCodeDescription() {
		return apiCodeDescription;
	}

	/**
	 * @param apiCodeDescription the apiCodeDescription to set
	 */
	public void setApiCodeDescription(String apiCodeDescription) {
		this.apiCodeDescription = apiCodeDescription;
	}

	/**
	 * @param userMessages the userMessages to set
	 */
	public void setUserMessages(ArrayList<ErrorMessage> userMessages) {
		this.userMessages = userMessages;
	}

	public int getResponseCode() {
		return responseCode;
	}

	public void setResponseCode(int responseCode) {
		this.responseCode = responseCode;
	}
	

}