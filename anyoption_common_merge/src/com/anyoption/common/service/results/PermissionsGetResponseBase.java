package com.anyoption.common.service.results;

import java.util.ArrayList;

public class PermissionsGetResponseBase extends MethodResult {
	
	private ArrayList<String> permissionsList;	

	public ArrayList<String> getPermissionsList() {
		return permissionsList;
	}

	public void setPermissionsList(ArrayList<String> permissionsList) {
		this.permissionsList = permissionsList;
	}	
}
