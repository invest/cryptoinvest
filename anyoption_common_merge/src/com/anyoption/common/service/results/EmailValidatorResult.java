package com.anyoption.common.service.results;

public class EmailValidatorResult {
	
	int returnCode;

	public int getReturnCode() {
		return returnCode;
	}

	public void setReturnCode(int returnCode) {
		this.returnCode = returnCode;
	}

	@Override
	public String toString() {
		return "EmailValidatorResult [returnCode=" + returnCode + "]";
	}
	
}
