package com.anyoption.common.service.results;

import com.anyoption.common.beans.base.Investment;
import com.google.gson.annotations.Expose;

/**
 * 
 * @author liors
 *
 */
public class InvestmentMethodResult extends UserMethodResult {

	@Expose
	protected Investment investment;
	@Expose
	protected String userCashBalance;
	private int dev2Seconds;
	private int cancelSeconds;
	/**
	 * @return
	 */
	public Investment getInvestment() {
		return investment;
	}

	/**
	 * @param investment
	 */
	public void setInvestment(Investment investment) {
		this.investment = investment;
	}

	/**
	 * @return the userCashBalance
	 */
	public String getUserCashBalance() {
		return userCashBalance;
	}

	/**
	 * @param userCashBalance the userCashBalance to set
	 */
	public void setUserCashBalance(String userCashBalance) {
		this.userCashBalance = userCashBalance;
	}

	public int getDev2Seconds() {
		return dev2Seconds;
	}

	public void setDev2Seconds(int dev2Seconds) {
		this.dev2Seconds = dev2Seconds;
	}

    public int getCancelSeconds() {
		return cancelSeconds;
	}

	public void setCancelSeconds(int cancelSeconds) {
		this.cancelSeconds = cancelSeconds;
	}

	public String toString() {
    	String ls = System.getProperty("line.separator");
    	return ls + "InvestmentMethodResult"
    			+ super.toString()
    			+ "investment: " + investment + ls 
    			+ "userCashBalance: " + userCashBalance + ls
    			+ "dev2Seconds: " + dev2Seconds + ls 
    			+ "cancelSeconds: " + cancelSeconds + ls;
    }
}