package com.anyoption.common.service.results;

/**
 * @author AviadH
 *
 */
public class SkinMethodResult extends MethodResult {

	private Long skinId;
	private boolean loggedIn;
	private boolean isRegulated;
	private long currencyId;
	private long skinGroupId;
	private long currentTime;
	private String currencySymbol;
	private boolean currencyLeftSymbol;
	private String dstOffset;

	/**
	 * @return the skinId
	 */
	public Long getSkinId() {
		return skinId;
	}

	/**
	 * @param skinId the skinId to set
	 */
	public void setSkinId(Long skinId) {
		this.skinId = skinId;
	}

	public boolean isLoggedIn() {
		return loggedIn;
	}

	public void setLoggedIn(boolean loggedIn) {
		this.loggedIn = loggedIn;
	}

	public boolean isRegulated() {
		return isRegulated;
	}

	public void setRegulated(boolean isRegulated) {
		this.isRegulated = isRegulated;
	}

	public long getCurrencyId() {
		return currencyId;
	}

	public void setCurrencyId(long defaultCurrencyId) {
		this.currencyId = defaultCurrencyId;
	}

	public long getSkinGroupId() {
		return skinGroupId;
	}

	public void setSkinGroupId(long skinGroupId) {
		this.skinGroupId = skinGroupId;
	}

	public long getCurrentTime() {
		return currentTime;
	}

	public void setCurrentTime(long currentTime) {
		this.currentTime = currentTime;
	}

	public String getCurrencySymbol() {
		return currencySymbol;
	}

	public void setCurrencySymbol(String currencySymbol) {
		this.currencySymbol = currencySymbol;
	}

	public boolean isCurrencyLeftSymbol() {
		return currencyLeftSymbol;
	}

	public void setCurrencyLeftSymbol(boolean currencyLeftSymbol) {
		this.currencyLeftSymbol = currencyLeftSymbol;
	}

	public String getDstOffset() {
		return dstOffset;
	}

	public void setDstOffset(String dstOffset) {
		this.dstOffset = dstOffset;
	}
	
}
