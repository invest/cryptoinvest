package com.anyoption.common.service.results;

import com.anyoption.common.beans.base.UserAnycapital;
import com.google.gson.annotations.Expose;

/**
 * @author LioR SoLoMoN
 *
 */
public class UserAnycapitalMethodResult extends MethodResult {
	@Expose
	private UserAnycapital data;

	/**
	 * @return the user
	 */
	public UserAnycapital getData() {
		return data;
	}

	/**
	 * @param user the user to set
	 */
	public void setData(UserAnycapital data) {
		this.data = data;
	}
}