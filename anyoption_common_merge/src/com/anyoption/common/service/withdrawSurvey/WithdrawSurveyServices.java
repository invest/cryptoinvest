/**
 * 
 */
package com.anyoption.common.service.withdrawSurvey;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import com.anyoption.common.backend.annotations.BackendPermission;
import com.anyoption.common.beans.Limit;
import com.anyoption.common.beans.User;
import com.anyoption.common.managers.BonusManagerBase;
import com.anyoption.common.managers.CurrencyRatesManagerBase;
import com.anyoption.common.managers.LanguagesManagerBase;
import com.anyoption.common.managers.LimitsManagerBase;
import com.anyoption.common.managers.SkinsManagerBase;
import com.anyoption.common.managers.TransactionsManagerBase;
import com.anyoption.common.managers.UserRegulationManager;
import com.anyoption.common.managers.UsersManagerBase;
import com.anyoption.common.managers.WriterPermisionsManagerBase;
import com.anyoption.common.payments.WithdrawEntryInfo;
import com.anyoption.common.payments.WithdrawEntryResult;
import com.anyoption.common.payments.WithdrawUtil;
import com.anyoption.common.service.CommonJSONService;
import com.anyoption.common.service.CommonServiceServlet;
import com.anyoption.common.service.gmwithdrawal.GmWithdrawalRequest;
import com.anyoption.common.service.requests.MethodRequest;
import com.anyoption.common.service.requests.UserMethodRequest;
import com.anyoption.common.service.results.ErrorMessage;
import com.anyoption.common.service.results.MethodResult;
import com.anyoption.common.service.results.PermissionsGetResponseBase;
import com.anyoption.common.util.CommonUtil;
import com.anyoption.common.util.MessageToFormat;
import com.anyoption.common.util.Utils;


/**
 * @author eran.levy
 *
 */
public class WithdrawSurveyServices {
	
	
	private static final Logger logger = Logger.getLogger(WithdrawSurveyServices.class);
	
	
	
	public static MethodResult getFirstLevelQuestion(MethodRequest request, HttpServletRequest httpRequest) {
		logger.debug("getFirstLevelQuestion:" + request);
		MethodResult result = new MethodResult();
		
		Map<String, MessageToFormat> mtf = new HashMap<String, MessageToFormat>();
				
			
		return result;
	}
	
	
	/**
	 * @param userId
	 * @return
	 */
	private boolean isFirstWithdraw(long userId) {
		boolean res = true;
		return res;
	}
	
	/**
	 * @param userId
	 * @return
	 */
	private boolean isSpecialOffer(long userId) {
		boolean res = true;
		return res;
	}
	
//	private static boolean validateRequest(MethodRequest request, MethodResult result) {
//		if (request.getUserId() <= 0) {
//			logger.debug("Wrong userId: " + request.getUserId());
//		}
//		return false;
//	}
	
	
	
}
