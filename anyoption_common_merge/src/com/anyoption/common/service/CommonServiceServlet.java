package com.anyoption.common.service;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.lang.reflect.Method;
import java.lang.reflect.Parameter;
import java.sql.SQLException;
import java.util.Enumeration;
import java.util.HashMap;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.anyoption.common.annotations.PrintLogAnnotations;
import com.anyoption.common.beans.ActivationMail;
import com.anyoption.common.beans.Writer;
import com.anyoption.common.beans.base.UserRegulationBase;
import com.anyoption.common.charts.ChartHistoryCache;
import com.anyoption.common.charts.ChartsUpdater;
import com.anyoption.common.charts.LevelHistoryCache;
import com.anyoption.common.jms.BackendLevelsCache;
import com.anyoption.common.jms.LevelsCache;
import com.anyoption.common.jms.WebLevelsCache;
import com.anyoption.common.managers.IssuesManagerBase;
import com.anyoption.common.managers.UserRegulationManager;
import com.anyoption.common.service.requests.MethodRequest;
import com.anyoption.common.util.CommonUtil;
import com.anyoption.common.util.ConstantsBase;
import com.anyoption.common.util.OpportunityCache;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/**
 * @author Jamal
 */
public class CommonServiceServlet extends HttpServlet {

	private static final long serialVersionUID = -3536866919755220527L;
	private static final Logger log = Logger.getLogger(CommonServiceServlet.class);
	
	public static final int ERROR_CODE_WRITER_IS_NOT_IN_SESSION				= 6999;
	public static final int ERROR_CODE_USER_NOT_HAVE_PERMISSION				= 6998;
	private static final String REQUEST_POINT_TO_COMMON_PKG_FROM_BE = "/BackendService/CommonService/";

	@Override
	public void init() {
		log.info("CommonServiceServlet starting...");
	}
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) {
		String uri = request.getRequestURI();
		String uriWithoutMethod = uri.substring(0, uri.lastIndexOf("/"));
		String methodReq = uri.substring(uri.lastIndexOf("/") + 1);
		
		String classReq = uriWithoutMethod.substring(uriWithoutMethod.lastIndexOf("/") + 1);
		
		if (uriWithoutMethod.contains(REQUEST_POINT_TO_COMMON_PKG_FROM_BE)) {
			classReq = getCommonPackage(classReq) + classReq;
		} else {
			classReq = getPackage(classReq) + classReq;
		}
		
		log.debug("URI requested: " + uri + " Class: " + classReq + " Method: " + methodReq + " sessionId: " + request.getSession().getId());

		if (log.isTraceEnabled()) {
			Enumeration<String> headerNames = request.getHeaderNames();
			String headerName = null;
			String ls = System.getProperty("line.separator");
			StringBuffer sb = new StringBuffer(ls);
			while (headerNames.hasMoreElements()) {
				headerName = headerNames.nextElement();
				sb.append(headerName).append(": ").append(request.getHeader(headerName)).append(ls);
			}
			log.trace(sb.toString());
		}
		
		Object result = null;
		try {
			Gson gson = new GsonBuilder().serializeNulls().create();
			Class<?> serviceClass = Class.forName(classReq);			
			Method[] allMethods = serviceClass.getMethods();
			boolean isFoundMethod = false;
		    for (Method m : allMethods) {
		    	if(m.getName().equals(methodReq)){
		    		isFoundMethod = true;
		    		Parameter[] methodParams = m.getParameters();
					Object[] requestParams = new Object[methodParams.length];					
					for (int i = 0; i < methodParams.length; i++) {
						Class<?> paramClass = methodParams[i].getType();
	                    if (paramClass == HttpServletRequest.class) {
	                        requestParams[i] = request;
						} else {
							requestParams[i] = gson.fromJson(new InputStreamReader(request.getInputStream(), "UTF-8"), methodParams[i].getType());
						}
	                    //set IP
						if (requestParams[i] instanceof MethodRequest) {
							((MethodRequest) requestParams[i]).setIp(CommonUtil.getIPAddress(request));
						}
					}
					
					String jsonResponse = "";					
					if(!isWriterInSession(request) && isMethodNeedWriterSessionToBeBind(m)){
						log.error("Writer not in session!");
						jsonResponse = "{\"errorCode\" : \"" + ERROR_CODE_WRITER_IS_NOT_IN_SESSION + "\"}";					
					}else if(!isHavePermission(m, request) && isMethodNeedWriterSessionToBeBind(m)){
						log.error("No permission for method " + m.getName());
						jsonResponse = "{\"errorCode\" : \"" + ERROR_CODE_USER_NOT_HAVE_PERMISSION + "\"}";
					} else {
						result = m.invoke(null, requestParams);
						jsonResponse = gson.toJson(result);
					}				
					
					if (null != jsonResponse && jsonResponse.length() > 0) {
						if(!stopPrintDebugLog(m)){
							log.debug(jsonResponse);
						}
						byte[] data = jsonResponse.getBytes("UTF-8");
						OutputStream os = response.getOutputStream();
						response.setHeader("Content-Type", "application/json");
						response.setHeader("Content-Length", String.valueOf(data.length));
						os.write(data, 0, data.length);
						os.flush();
						os.close();
					}
		    	} 		    	
		    }
		    
	    	if(!isFoundMethod) {
	    		log.error("Can't find method:" + methodReq + " in Class:" + classReq);
	    	}
		    
		} catch (Exception e) {
			log.error("Problem executing " + methodReq + " Method! ", e);
		}
	}
	
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) {
		log.debug("doGet");
		String uri = request.getRequestURI();
		String uriWithoutMethod = uri.substring(0, uri.lastIndexOf("/"));
		String methodReq = uri.substring(uri.lastIndexOf("/") + 1);
		String classReq = uriWithoutMethod.substring(uriWithoutMethod.lastIndexOf("/") + 1);
		classReq = getPackage(classReq) + classReq;
		log.debug("URI requested: " + uri + " Class: " + classReq + " Method: " + methodReq + " sessionId: "
				+ request.getSession().getId());
		Object result = null;
		try {
			Gson gson = new GsonBuilder().serializeNulls().create();
			Class<?> serviceClass = Class.forName(classReq);
			Method[] allMethods = serviceClass.getMethods();
			boolean isFoundMethod = false;
			for (Method m : allMethods) {
				if (m.getName().equals(methodReq)) {
					isFoundMethod = true;
					Parameter[] methodParams = m.getParameters();
					Object[] requestParams = new Object[methodParams.length];
					requestParams[0] = request;
					String jsonResponse = "";
					result = m.invoke(null, requestParams);
					jsonResponse = gson.toJson(result);
					if (null != jsonResponse && jsonResponse.length() > 0) {
							if (!stopPrintDebugLog(m)) {
								log.debug(jsonResponse);
							}
							byte[] data = jsonResponse.getBytes("UTF-8");
							OutputStream os = response.getOutputStream();
							response.setHeader("Content-Type", "application/json");
							response.setHeader("Content-Length", String.valueOf(data.length));
							os.write(data, 0, data.length);
							os.flush();
							os.close();
						}
					}
				}
				if (!isFoundMethod) {
					log.error("Can't find method:" + methodReq + " in Class:" + classReq);
				}
		} catch (Exception e) {
			log.error("Problem executing " + methodReq + " Method! ", e);
		}
			
	}
	
	private boolean isMethodNeedWriterSessionToBeBind(Method m) {
		boolean flag = true;
		if (m.getName().equals("resetPassword")) {
			flag = false;
		}
		return flag;
	}

	@Override
	public void destroy() {

	}
	
	protected String getPackage(String className){
		String pkg = "com.anyoption.common.service." + className.substring(0, className.indexOf("Service")) + ".";
		return pkg.toLowerCase();
	}
	
	private String getCommonPackage(String className){
		String pkg = "com.anyoption.common.service." + className.substring(0, className.indexOf("Service")) + ".";
		return pkg.toLowerCase();
	}
	
	protected boolean isHavePermission(Method method, HttpServletRequest request) {
		return true;		
	}
	
	protected boolean isWriterInSession(HttpServletRequest request) {
		return true;		
	}
	
	private boolean stopPrintDebugLog(Method method) {
		boolean res = false;
		PrintLogAnnotations annotation = method.getAnnotation(PrintLogAnnotations.class);
		if (annotation != null) {			
			res = annotation.stopPrintDebugLog();
		}
		return res;
	}
}