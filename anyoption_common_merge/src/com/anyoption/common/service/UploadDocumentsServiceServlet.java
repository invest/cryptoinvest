package com.anyoption.common.service;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.OutputStream;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.User;
import com.anyoption.common.beans.Writer;
import com.anyoption.common.service.results.MethodResult;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;


public class UploadDocumentsServiceServlet extends HttpServlet {

	private static final long serialVersionUID = 7256494852272342630L;
	private static final Logger log = Logger.getLogger(UploadDocumentsServiceServlet.class);
	private static final String FILE_PART_NAME = "myFile";
	private static final String FILE_TYPE_PARAMETER = "fileType";
	private static final String FILE_NAME_PARAMETER = "fileName";
	private static final String CREDIT_CARD_ID_PARAMETER = "ccId";
	private static final String WRITER_ID_PARAMETER = "writerId";

	protected void doPost(HttpServletRequest request, HttpServletResponse response) {
		log.debug("UploadDocumentsServiceServlet.doPost");

		MethodResult result = new MethodResult();
		User user = null;
		user = (User) request.getSession().getAttribute("user");
		if (user == null) {
			log.debug("User not in session");
			result.setErrorCode(CommonJSONService.ERROR_CODE_SESSION_EXPIRED); 
			result.addErrorMessage("", "User login failed"); 
			response.setStatus(HttpServletResponse.SC_FORBIDDEN);
		}
		String partName = null;
		String writerIdStr = request.getParameter(WRITER_ID_PARAMETER);
		String fileTypeStr = request.getParameter(FILE_TYPE_PARAMETER);
		String fileName = request.getParameter(FILE_NAME_PARAMETER);
		String ccIdStr = request.getParameter(CREDIT_CARD_ID_PARAMETER);
		if (writerIdStr == null || fileTypeStr == null || Long.valueOf(fileTypeStr) < 1 || fileName == null) {
			log.debug("Not valid request. Given parameters are: writerIdStr: "+ writerIdStr + ", fileTypeStr: " + fileTypeStr
						+ ", fileName: " + fileName + ", ccIdStr: " + ccIdStr);
			result.setErrorCode(CommonJSONService.ERROR_CODE_INVALID_INPUT);
		} else {
			int writerId = Integer.valueOf(writerIdStr);
			if (writerId != Writer.WRITER_ID_MOBILE 
					&& writerId != Writer.WRITER_ID_COPYOP_MOBILE
					&& writerId != Writer.WRITER_ID_BUBBLES_MOBILE
					&& writerId != Writer.WRITER_ID_BUBBLES_DEDICATED_APP
							) {
				try {
					for (Part part : request.getParts()) {
						log.debug("PART-->" + part.getName() + " size:"  + part.getSize());
						if ((part.getName().equals(FILE_PART_NAME) || part.getName().toLowerCase().equals("file")) && part.getContentType() != null) {
							partName = part.getName();
						}
					}
				} catch (IllegalStateException e) {
					log.error("Unable to upload file", e);
					result.setErrorCode(CommonJSONService.ERROR_CODE_UNKNOWN);
				} catch (IOException e) {
					log.error("Unable to upload file", e);
					result.setErrorCode(CommonJSONService.ERROR_CODE_UNKNOWN);
				} catch (ServletException e) {
					log.error("Unable to upload file", e);
					result.setErrorCode(CommonJSONService.ERROR_CODE_UNKNOWN);
				}
			}
			try {
				if (result.getErrorCode() == CommonJSONService.ERROR_CODE_SUCCESS) {
					BufferedInputStream bis;
					if (partName == null) { // mobile client upload
						bis = new BufferedInputStream(request.getInputStream());
					} else { // web client upload
						bis = new BufferedInputStream(request.getPart(partName).getInputStream());
					}
					long fileType = Long.valueOf(fileTypeStr);
					long ccId = (ccIdStr != null) ? Long.valueOf(ccIdStr) : 0l;
					UploadDocumentsService.uploadFile(bis, user, fileType, fileName, ccId, writerId, result);
				}
			} catch (IllegalStateException e) {
				log.error("Unable to read file stream", e);
				result.setErrorCode(CommonJSONService.ERROR_CODE_UNKNOWN);
			} catch (IOException e) {
				log.error("Unable to read file stream", e);
				result.setErrorCode(CommonJSONService.ERROR_CODE_UNKNOWN);
			} catch (ServletException e) {
				log.error("Unable to read file stream", e);
				result.setErrorCode(CommonJSONService.ERROR_CODE_UNKNOWN);
			}
		}

		try {
			Gson gson = new GsonBuilder().serializeNulls().create();
	    	String jsonResponse = gson.toJson(result);
	        if (null != jsonResponse && jsonResponse.length() > 0) {
	        	log.debug(jsonResponse);
	        	byte[] data = jsonResponse.getBytes("UTF-8");
	        	OutputStream os = response.getOutputStream();
	        	response.setHeader("Content-Type", "application/json");
	        	response.setHeader("Content-Length", String.valueOf(data.length));
	        	os.write(data, 0, data.length);
	        	os.flush();
	        	os.close();
	        }
		} catch (IOException e) {
			log.error("Unable to serialize response", e);
		}
	}
}