package com.anyoption.common.service;

import java.io.StringWriter;
import java.sql.SQLException;
import java.text.Collator;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Properties;
import java.util.Random;
import java.util.Set;
import java.util.SortedSet;
import java.util.TimeZone;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.Velocity;

import com.anyoption.common.annotations.PrintLogAnnotations;
import com.anyoption.common.beans.Country;
import com.anyoption.common.beans.Investment;
import com.anyoption.common.beans.LimitationDeposits;
import com.anyoption.common.beans.Market;
import com.anyoption.common.beans.MarketRate;
import com.anyoption.common.beans.Opportunity;
import com.anyoption.common.beans.Skin;
import com.anyoption.common.beans.Transaction;
import com.anyoption.common.beans.User;
import com.anyoption.common.beans.UserMigration;
import com.anyoption.common.beans.Writer;
import com.anyoption.common.beans.base.ApiErrorCode;
import com.anyoption.common.beans.base.CurrenciesRules;
import com.anyoption.common.beans.base.DeeplinkParamsToUrl;
import com.anyoption.common.beans.base.DepositBonusBalanceBase;
import com.anyoption.common.beans.base.MailBoxTemplate;
import com.anyoption.common.beans.base.MarketGroup;
import com.anyoption.common.beans.base.QuestionnaireAnswer;
import com.anyoption.common.beans.base.QuestionnaireGroup;
import com.anyoption.common.beans.base.QuestionnaireQuestion;
import com.anyoption.common.beans.base.QuestionnaireUserAnswer;
import com.anyoption.common.beans.base.TermsPartFile;
import com.anyoption.common.beans.base.UserRegulationBase;
import com.anyoption.common.charts.ChartHistoryCache;
import com.anyoption.common.enums.CopyOpInvTypeEnum;
import com.anyoption.common.enums.SkinGroup;
import com.anyoption.common.jms.WLCOppsCacheBean;
import com.anyoption.common.jms.WebLevelLookupBean;
import com.anyoption.common.jms.WebLevelsCache;
import com.anyoption.common.managers.BonusManagerBase;
import com.anyoption.common.managers.CountryManagerBase;
import com.anyoption.common.managers.CurrenciesManagerBase;
import com.anyoption.common.managers.CurrenciesRulesManagerBase;
import com.anyoption.common.managers.CurrencyRatesManagerBase;
import com.anyoption.common.managers.DeeplinkParamsToUrlManagerBase;
import com.anyoption.common.managers.DepositBonusBalanceManagerBase;
import com.anyoption.common.managers.InvestmentsManagerBase;
import com.anyoption.common.managers.IssuesManagerBase;
import com.anyoption.common.managers.LanguagesManagerBase;
import com.anyoption.common.managers.LimitationDepositsManagerBase;
import com.anyoption.common.managers.LimitsManagerBase;
import com.anyoption.common.managers.MarketsManagerBase;
import com.anyoption.common.managers.QuestionnaireManagerBase;
import com.anyoption.common.managers.SkinsManagerBase;
import com.anyoption.common.managers.TermsManagerBase;
import com.anyoption.common.managers.TransactionsManagerBase;
import com.anyoption.common.managers.UserRegulationManager;
import com.anyoption.common.managers.UsersManagerBase;
import com.anyoption.common.managers.WritersManagerBase;
import com.anyoption.common.service.requests.CancelInvestmentCheckboxStateMethodRequest;
import com.anyoption.common.service.requests.CancelInvestmentMethodRequest;
import com.anyoption.common.service.requests.ChartDataRequest;
import com.anyoption.common.service.requests.CrossSaleRequest;
import com.anyoption.common.service.requests.DeeplinkRequest;
import com.anyoption.common.service.requests.DepositBonusBalanceMethodRequest;
import com.anyoption.common.service.requests.EmailValidatorRequest;
import com.anyoption.common.service.requests.InsertInvestmentMethodRequest;
import com.anyoption.common.service.requests.MethodRequest;
import com.anyoption.common.service.requests.TermsFileMethodRequest;
import com.anyoption.common.service.requests.UpdateUserQuestionnaireAnswersRequest;
import com.anyoption.common.service.requests.UserMethodRequest;
import com.anyoption.common.service.results.ChartDataResult;
import com.anyoption.common.service.results.CountriesListMethodResult;
import com.anyoption.common.service.results.DeeplinkResult;
import com.anyoption.common.service.results.DepositBonusBalanceMethodResult;
import com.anyoption.common.service.results.EmailValidatorResult;
import com.anyoption.common.service.results.InvestmentMethodResult;
import com.anyoption.common.service.results.InvestmentsMethodResult;
import com.anyoption.common.service.results.MarketsGroupsMethodResult;
import com.anyoption.common.service.results.MarketsMethodResult;
import com.anyoption.common.service.results.MarketsNamesMethodResult;
import com.anyoption.common.service.results.MethodResult;
import com.anyoption.common.service.results.SkinMethodResult;
import com.anyoption.common.service.results.TermsPartFilesMethodResult;
import com.anyoption.common.service.results.UserFPResult;
import com.anyoption.common.service.results.UserMethodResult;
import com.anyoption.common.service.results.UserQuestionnaireResult;
import com.anyoption.common.service.results.WithdrawBonusRegulationStateMethodResult;
import com.anyoption.common.service.results.WithdrawBonusRegulationStateMethodResult.WithdrawBonusStateEnum;
import com.anyoption.common.service.util.EmailValidatorUtil;
import com.anyoption.common.util.CommonUtil;
import com.anyoption.common.util.ConstantsBase;
import com.anyoption.common.util.InvestmentHelper;
import com.anyoption.common.util.InvestmentValidatorDynamics;
import com.anyoption.common.util.InvestmentValidatorParams;
import com.anyoption.common.util.MessageToFormat;
import com.anyoption.common.util.OpportunityCache;
import com.anyoption.common.util.OpportunityCacheBean;
import com.anyoption.common.util.Sha1;
import com.anyoption.common.util.Utils;


/**
 * @author kirilim
 */
public class CommonJSONService {

    private static final Logger log = Logger.getLogger(CommonJSONService.class);

    public static final int ERROR_CODE_SUCCESS          				 				= 0;
    public static final int ERROR_CODE_WITHDRAWAL_CLOSE        			 				= 110;
    public static final int ERROR_CODE_FAIL_TO_WITHDRAW        		 	 				= 111;
    public static final int ERROR_CODE_WITHDRAWAL_WRONG_STATUS			 				= 112;
    public static final int ERROR_CODE_WITHDRAWAL_NEGATIVE_AMOUNT	     				= 113;
    public static final int ERROR_CODE_WITHDRAWAL_AMOUNT_HIGHER_THAN_ALLOWED 			= 114;
    public static final int ERROR_CODE_WITHDRAWAL_UNAUTHORIZED_TRANSACTION_TYPE	     	= 115;
    public static final int ERROR_CODE_WITHDRAWAL_EMPTY_CHEQUE					     	= 116;
    public static final int ERROR_CODE_WITHDRAWAL_FLAGGED						     	= 117;
    public static final int ERROR_CODE_WITHDRAWAL_NO_FLAG_SUBJECT				     	= 118;
    public static final int ERROR_CODE_WITHDRAWAL_NO_BANK_SELECTED						= 119;
    public static final int ERROR_CODE_WITHDRAWAL_NO_BANK_WIRE_ID						= 120;
    public static final int ERROR_CODE_WITHDRAWAL_NO_EXPECTED_PROVIDER					= 121;
    public static final int ERROR_CODE_RC_UNIQUE_CONSTRAINT_VIOLATED					= 122;
    public static final int ERROR_CODE_INVALID_INPUT    				 				= 200;
    public static final int ERROR_CODE_INVALID_ACCOUNT  				 				= 201;
    public static final int ERROR_CODE_DUP_REQUEST      				 				= 202;
    public static final int ERROR_CODE_MISSING_PARAM    				 				= 203;
    public static final int ERROR_CODE_LOGIN_FAILED					     				= 205;
    public static final int ERROR_CODE_INV_VALIDATION					 				= 206;
    public static final int ERROR_CODE_GENERAL_VALIDATION				 				= 207;
    public static final int ERROR_CODE_VALIDATION_WITHOUT_FIELD			 				= 208;
    public static final int ERROR_CODE_VALIDATION_LOW_BALANCE			 				= 209;
    public static final int ERROR_CODE_TRANSACTION_FAILED				 				= 210;
    public static final int ERROR_CODE_CC_NOT_SUPPORTED					 				= 211;
    public static final int ERROR_CODE_INVALID_CAPTCH					 				= 212;
    public static final int ERROR_CODE_NO_CASH_FOR_NIOU					 				= 213;
    public static final int ERROR_CODE_EMAIL_IN_USE           			 				= 214;
    public static final int ERROR_CODE_LOGIN_FAILED_OB_BLOCKED_COUNTRY   				= 215;
    public static final int ERROR_CODE_LOGIN_WITH_AO_ACCOUNT			   				= 216;
    public static final int ERROR_CODE_REGULATION_MISSING_QUESTIONNAIRE	 				= 300;
    public static final int ERROR_CODE_REGULATION_SUSPENDED				 				= 301;
    public static final int ERROR_CODE_REGULATION_SUSPENDED_DOCUMENTS	 				= 302;
    public static final int ERROR_CODE_USER_NOT_REGULATED				 				= 303;
    public static final int ERROR_CODE_REGULATION_USER_RESTRICTED		 				= 304;
    public static final int ERROR_CODE_REG_SUSPENDED_QUEST_INCORRECT	 				= 305;
    public static final int ERROR_CODE_SHOW_WARNING_DUE_DOCUMENTS		 				= 306;
    public static final int ERROR_CODE_REGULATION_USER_PEP_PROHIBITED	 				= 307;
    public static final int ERROR_CODE_REGULATION_USER_IS_TRESHOLD_BLOCK 				= 308;
    public static final int ERROR_CODE_VALIDATION_LOW_CASH_BALANCE		 				= 309;
    public static final int ERROR_CODE_VALIDATION_EXPOSURE	 			 				= 310;
    public static final int ERROR_CODE_REGULATION_CAN_NOT_FINAL_APPROVE	 				= 311;
    public static final int ERROR_CODE_USER_DOC_IS_ALREADY_LOCKED    	 				= 312;
    public static final int ERROR_CODE_USER_CNMV_SUSPENDE          	 				    = 313;
    public static final int ERROR_CODE_SUSPENDED_MIGRATE_INVEST				    		= 314;
    public static final int ERROR_CODE_SUSPENDED_MIGRATE_WEBTRADE						= 315;
    
    public static final int ERROR_CODE_QUESTIONNAIRE_NOT_FULL			= 400;
    public static final int ERROR_CODE_QUESTIONNAIRE_NOT_SUPPORTED		= 401;
    public static final int ERROR_CODE_QUESTIONNAIRE_ALREADY_DONE		= 402;
    public static final int ERROR_CODE_SESSION_EXPIRED					= 419;
    public static final int ERROR_CODE_OPP_NOT_EXISTS					= 500;
    public static final int ERROR_CODE_FILE_TOO_LARGE					= 600;
    public static final int ERROR_CODE_USER_REMOVED					                = 800;
    public static final int ERROR_CODE_USER_BLOCKED					                = 801;
    public static final int ERROR_CODE_BACKEND_MAX_MIN_SPREAD_REACHED			    = 830;
    public static final int ERROR_CODE_BACKEND_NIGHT_SCHIFT_SPREAD_LIMIT_REACHED	= 831;
    public static final int ERROR_CODE_UNKNOWN						= 999;
    
    public static final int ERROR_CODE_ET_REGULATION_ADDITIONAL_INFO			    = 700;
    public static final int ERROR_CODE_NOT_ACCEPTED_TERMS				            = 701;
    public static final int ERROR_CODE_ET_REGULATION_KNOWLEDGE_CONFIRM			    = 702;
    public static final int ERROR_CODE_ET_REGULATION_CAPITAL_QUESTIONNAIRE_NOT_DONE	= 703;
    public static final int ERROR_CODE_ET_REGULATION_SUSPENDED_LOW_SCORE	     	= 704;
    public static final int ERROR_CODE_ET_REGULATION_SUSPENDED_LOW_TRESHOLD		    = 705;
    public static final int ERROR_CODE_ET_REGULATION_SUSPENDED_MEDIUM_TRESHOLD		= 706;
    
    public static final int ERROR_CODE_COPYOP_MAX_COPIERS				= 1001;

    public static final int ERROR_CODE_ROLE_REQUIRED					= 2000;

    public static final String BUBBLES_REQUEST_PARAMETER_SIGNATURE_SEPARATOR 		= "5u2v3i11a17c3"; // surveillance
    private static final int ERROR_CODE_BUBBLES_INVALID_SIGNATURE			= 3000;
    
    public static final int ERROR_CODE_INVALID_PERMISSIONS = 4000;
    
    public static final int ERROR_CODE_FAIL_TO_INSERT = 5000;
    public static final int ERROR_CODE_FAIL_TO_UPDATE = 5001;
    public static final int ERROR_CODE_FAIL_TO_LOAD_WRITERS = 5002;
    public static final int ERROR_CODE_WRITER_USERNAME_OR_EMAIL_INCORRECT = 5003;
    public static final int ERROR_CODE_WRITER_IS_NOT_ACTIVE = 5004;
    public static final int ERROR_CODE_WRITER_RESET_PASS_SUCCESSFULLY_SEND = 5005;
    public static final int ERROR_CODE_FAIL_TO_LOAD_TRANSACTIONS = 5502;
    public static final int ERROR_CODE_MAINTENANCE_FEE_ALREADY_CANCELED = 5503;
    
    public static final long WEB_ERITER = 1;
    
    public static final int VALIDATE_REGULATION_OTHER = 0;
    public static final int VALIDATE_REGULATION_INVESTMENT = 1;
    public static final int VALIDATE_REGULATION_DEPOSIT = 2;
    public static final int VALIDATE_REGULATION_COPYOP_WATCH = 3;
    
    protected static final int ANSWER_YES_IDX = 0;
    protected static final int ANSWER_NO_IDX = 1;
	protected static final int QUESTION_FUNDS_ORIGIN_IDX = 4;
	protected static final int QUESTION_PEP_IDX = 5;
	protected static final int QUESTION_TRADE_WISH_IDX = 6;
	protected static final int QUESTION_INCOME_SOURCE_IDX = 5;
	
	protected static final int SINGLE_QUEST_3_1 = 95;
	protected static final int SINGLE_QUEST_3_2 = 96;
	protected static final int SINGLE_QUEST_3_3 = 97;
	
	public static final int ERROR_CODE_MISS_SUPPORT_REJETC_REASON_PERMISSIONS = 4001;
	public static final int ERROR_CODE_MISS_CONTROL_REJETC_REASON_PERMISSIONS = 4002;
	public static final int ERROR_CODE_MISS_SUPPORT_REJETC_COMMENT = 4003;
	public static final int ERROR_CODE_MISS_FILE = 4004;
	public static final int ERROR_CODE_MISS_EXP_DATE = 4005;
	public static final int ERROR_CODE_MISS_CONTROL_REJETC_COMMENT = 4006;
	public static final int ERROR_CODE_SUPPORT_APPROVED_AND_REJETC = 4007;
	public static final int ERROR_CODE_CONTROL_APPROVED_AND_REJETC = 4008;
	public static final int ERROR_CODE_MISS_CC_ID = 4009;
	public static final int ERROR_CODE_MISS_FILE_ID = 4010;
	public static final int ERROR_CODE_VALIDATE_DATE_RANGE = 4011;
	public static final int ERROR_CODE_UNABLE_SENT_KEESING_FILE = 4012;
	public static final int ERROR_CODE_NO_FOUND_FILE = 4013;
	public static final int ERROR_CC_DOCUMENT_SENT_UPDATED = 4014;
	public static final int ERROR_EXIST_UPLOAD_FILE = 4015;
	
	
	public static final int ERROR_CODE_END_DATE_EARLIER_START_DATE = 4500;
	
	public static final int ERROR_CODE_ODDS_GROUP_ODDS_TYPES = 4700;
	
	public static final int ERROR_CODE_FAIL_TO_INSERT_BANNER_SLIDERS = 6001;
	public static final int ERROR_CODE_NOT_ALLOWED_EXTENSION_SLIDER_IMAGE = 6002;
	public static final int ERROR_CODE_WRONG_DIMENSIONS_SLIDER_IMAGE = 6003;
	public static final int ERROR_CODE_FAIL_TO_DELETE_BANNER_SLIDER = 6004;
	public static final int ERROR_CODE_FAIL_TO_DISABLE_BANNER_SLIDER = 6005;
	public static final int ERROR_CODE_FAIL_TO_ENABLE_BANNER_SLIDER = 6006;
	public static final int ERROR_CODE_FAIL_TO_UPDATE_BANNER_SLIDER_IMAGE = 6007;
	public static final int ERROR_CODE_MISSING_BANNER_SLIDER_IMAGE_LINK = 6008;
	public static final int ERROR_CODE_UNABLE_TO_GET_SLIDER_IMAGES = 6009;
	public static final int ERROR_CODE_UNABLE_TO_UPDATE_SLIDER_POSITION = 6010;
	public static final int ERROR_CODE_FAIL_TO_LOAD_BANNER_SLIDERS = 6011;
	
	public static final int ERROR_CODE_MISSING_FILE_NAME = 6050;
	public static final int ERROR_CODE_INSERTING_FILE = 6051;
	public static final int ERROR_CODE_NOT_ALLOWED_EXTENSION = 6052;
	
	public static final int ERROR_CODE_FAIL_TO_GET_COUNTRY_GROUPS = 7001;
	public static final int ERROR_CODE_FAIL_TO_UPDATE_COUNTRY_GROUPS = 7002;

	private static final int ERROR_CODE_AMOUNT_HIGHER_THAN_ALLOWED = 7003;

	protected static void validateBubblesRequest(String signature, String requestSignature, long writerId, MethodResult result) {
		try {
			signature += BUBBLES_REQUEST_PARAMETER_SIGNATURE_SEPARATOR + WritersManagerBase.getWriterPassword(writerId);
		} catch (SQLException e) {
			log.error("Unable to load writer information, writerId: " + writerId, e);
		}
		try {
			if (!Sha1.encode(signature).equals(requestSignature)) {
				log.error("Bubbles request invalid! Real signature: " + signature);
				result.setErrorCode(ERROR_CODE_BUBBLES_INVALID_SIGNATURE);
			}
		} catch (Exception e) {
			log.error("Unable to encode with Sha1", e);
			result.setErrorCode(ERROR_CODE_UNKNOWN);
		}
	}

    public static String concatBubblesRequestParams(Object... params) {
		int remaining = params.length;
		StringBuilder sb = new StringBuilder();
		for (Object param : params) {
			sb.append(String.valueOf(param));
			remaining--;
			if (remaining > 0) {
				sb.append(BUBBLES_REQUEST_PARAMETER_SIGNATURE_SEPARATOR);
			}
		}
		return sb.toString();
	}
    
    protected static DepositBonusBalanceMethodResult getDepositBonusBalanceBaseBase(DepositBonusBalanceMethodRequest request){
    	DepositBonusBalanceMethodResult db = new DepositBonusBalanceMethodResult();
    	DepositBonusBalanceBase depositBonusBalanceBase = new DepositBonusBalanceBase();
		try {
			depositBonusBalanceBase = DepositBonusBalanceManagerBase.getDepositBonusBalance(request);
		} catch (Exception e) {
			log.error("Unable to get bonusBalance ", e);
			db.setErrorCode(ERROR_CODE_UNKNOWN);
		}
    	db.setDepositBonusBalanceBase(depositBonusBalanceBase);
    	return db;
    }
    
    protected static UserMethodResult updateIsKnowledgeQuestion(long userId, boolean isKnowledgeQuestion, long writerId){
    	UserMethodResult res = new UserMethodResult();
		res.setErrorCode(ERROR_CODE_SUCCESS);
		try {
			if(isKnowledgeQuestion){
				 UserRegulationBase userRegulation = new UserRegulationBase();
				 userRegulation.setUserId(userId);
				 UserRegulationManager.getUserRegulation(userRegulation);
				 if(userRegulation.getScoreGroup() == UserRegulationBase.AO_REGULATION_RESTRICTED_FAILED_Q_SCORE_GROUP_ID) {
					 IssuesManagerBase.insertIssueUserRegulationRestricted(userId, IssuesManagerBase.ISSUE_ACTION_TYPE_SUCCESS_AFTER_FAILED_QUESTIONNAIRE, writerId);
				 } else {
					 IssuesManagerBase.insertIssueUserRegulationRestricted(userId, IssuesManagerBase.ISSUE_ACTION_TYPE_RESTRICTED_QUESTION_YES, writerId);
				 }

			} else {
				IssuesManagerBase.insertIssueUserRegulationRestricted(userId, IssuesManagerBase.ISSUE_ACTION_TYPE_RESTRICTED_QUESTION_NO, writerId);
			}
		} catch (Exception e) {
			log.error("Unable to updateIsKnowledgeQuestion ", e);
			res.setErrorCode(ERROR_CODE_UNKNOWN);
		}		
    	return res;
    }

	protected static void getWithdrawBonusRegulationState(	WithdrawBonusRegulationStateMethodResult result, long userId, long skinId,
															long transactionTypeId, long ccId, long writerId, String utcOffset, String ip) {
		if (BonusManagerBase.hasOpenedBonuses(userId)) {
			if (SkinsManagerBase.getSkin(skinId).getBusinessCaseId() != Skin.SKIN_BUSINESS_OUROBOROS) {
				result.setState(WithdrawBonusStateEnum.NOT_REGULATED_OPEN_BONUSES);
				TransactionsManagerBase.insertWithdrawCancel(	userId, transactionTypeId, ccId, writerId, utcOffset,
																WithdrawBonusStateEnum.NOT_REGULATED_OPEN_BONUSES, ip);
			} else {
				result.setState(WithdrawBonusStateEnum.REGULATED_OPEN_BONUSES);
			}
		} else {
			result.setState(WithdrawBonusStateEnum.NO_OPEN_BONUSES);
		}
	}

	protected static void insertWithdrawCancel(	MethodResult result, long userId, long skinId, long transactionTypeId, long ccId,
												long writerId, String utcOffset, String ip) {
		if (BonusManagerBase.hasOpenedBonuses(userId)
				&& SkinsManagerBase.getSkin(skinId).getBusinessCaseId() == Skin.SKIN_BUSINESS_OUROBOROS) {
			TransactionsManagerBase.insertWithdrawCancel(	userId, transactionTypeId, ccId, writerId, utcOffset,
															WithdrawBonusStateEnum.REGULATED_OPEN_BONUSES, ip);
		} else {
			log.warn("Unsuccessful attempt to cancel withdraw for user with id: "+ userId + ". The user is not in correct state - "
						+ WithdrawBonusStateEnum.REGULATED_OPEN_BONUSES);
			result.setErrorCode(ERROR_CODE_UNKNOWN);
		}
	}
	
	protected static void getUserInvestments(InvestmentsMethodResult result,
				User user, Calendar from, Calendar to, boolean isSettled, long marketId, long skinId, long writerId,
				int offset, int period, int startPage, int pageSize) throws SQLException {
			Date fromTime = null;
			Date toTime = null;
			if (null != from) {
				fromTime = from.getTime();
			}
			if (null != to) {
				toTime = to.getTime();
			}
			if (period == 0) {
				period = 3; // in case request is from copyop web
			}

			ArrayList<Investment> investList = InvestmentsManagerBase.getInvestmentsByUser(user.getId(), fromTime,
					toTime, isSettled, 0l, new Long(marketId), skinId, startPage, pageSize, writerId, period);
			String utcOffset = CommonUtil.formatJSUTCOffsetToString(offset);
			Investment[] arr = new Investment[investList.size()];
			long sumInvAmount = 0;
			for (int i = 0; i < arr.length; i++) {
				arr[i] = investList.get(i);
				InvestmentHelper.initInvestment(arr[i], skinId, utcOffset);
				sumInvAmount += arr[i].getAmount();
			}
			result.setInvestments(arr);
			result.setSumAllInvAmount(CommonUtil.formatCurrencyAmountAO(sumInvAmount, true, user.getCurrencyId()));
	}

    protected static UserMethodResult unblockUserForThreshold(long userId, long writerId) {
    	UserMethodResult res = new UserMethodResult();
		res.setErrorCode(ERROR_CODE_SUCCESS);
		try {
			boolean success = IssuesManagerBase.insertUserRegulationUnblockIssueAction(userId, writerId);
			if (!success) {
				log.error("Unable to unblock user for threshold ");
				res.setErrorCode(ERROR_CODE_UNKNOWN);
			}
		} catch (Exception e) {
			log.error("Unable to unblock user for threshold ", e);
			res.setErrorCode(ERROR_CODE_UNKNOWN);
		}		
    	return res;
    }
    
    public static void validateUserRegulationAndNonReg(com.anyoption.common.beans.User user, MethodResult result, int validateAction) {
 		UserRegulationBase ur = new UserRegulationBase();
 		ur.setUserId(user.getId());
 		try {
 			UserRegulationManager.getUserRegulation(ur);			
 			if(ur.getApprovedRegulationStep() != null){
 				//AO Regulation
 					if(validateAction == VALIDATE_REGULATION_INVESTMENT 
 							|| validateAction == VALIDATE_REGULATION_DEPOSIT
 								|| validateAction == VALIDATE_REGULATION_COPYOP_WATCH) {
 						if (UserRegulationBase.REGULATION_FIRST_DEPOSIT == ur.getApprovedRegulationStep() 
 								&& (ur.isQualified() || ur.getRegulationVersion() == UserRegulationBase.REGULATION_USER_VERSION)) {
 							log.debug("The questionnaire should be filled before further trading or deposit. Error code: ERROR_CODE_REGULATION_MISSING_QUESTIONNAIRE: " + ur );
 							result.setErrorCode(ERROR_CODE_REGULATION_MISSING_QUESTIONNAIRE);
 							result.addUserMessage(null, CommonUtil.getMessage(user.getLocale(), "error.investment.mandatory.questionnaire", null));						
 						} else if(UserRegulationManager.isRestrictedAO(ur, user.getSkinId())){
 							log.debug("The user isRestricted. Error code: ERROR_CODE_REGULATION_MISSING_QUESTIONNAIRE: " + ur);
 							result.setErrorCode(ERROR_CODE_REGULATION_USER_RESTRICTED);
 						}else if(ur.isQuestionaireIncorrect()){
 							log.debug("The user isQuestionaireIncorrect. Error code: ERROR_CODE_USER_CNMV_SUSPENDE: " + ur);
 							result.setErrorCode(ERROR_CODE_USER_CNMV_SUSPENDE);
 						} else if(ur.isSuspendedCNMVDocuments()){
 							log.debug("The user CNMV Suspended. Error code: ERROR_CODE_REGULATION_MISSING_QUESTIONNAIRE: " + ur);
 							result.setErrorCode(ERROR_CODE_USER_CNMV_SUSPENDE);
 						}else if(validateAction == VALIDATE_REGULATION_INVESTMENT 
 								&& UserRegulationManager.isPepProhibited(ur, user.getSkinId())){
 						log.debug("The user isPepProhibited. Error code: ERROR_CODE_REGULATION_USER_PEP_PROHIBITED: " + ur);
 						result.setErrorCode(ERROR_CODE_REGULATION_USER_PEP_PROHIBITED);
 						} else if(ur.isTresholdBlock() && validateAction == VALIDATE_REGULATION_INVESTMENT){
 							log.debug("The user isTresholdBlock. Error code: ERROR_CODE_REGULATION_USER_IS_TRESHOLD_BLOCK: " + ur);
 							result.setErrorCode(ERROR_CODE_REGULATION_USER_IS_TRESHOLD_BLOCK);
 							result.addErrorMessage("tresholdSum", ur.getThresholdBlock() + "");
 						} else if(ur.isSuspendedDueDocuments()){
 							result.setErrorCode(ERROR_CODE_REGULATION_SUSPENDED_DOCUMENTS);
 							log.debug("The user isSuspendedDueDocuments. Error code: ERROR_CODE_REGULATION_SUSPENDED_DOCUMENTS: " + ur);
 						} else if(ur.isSuspended()){
 							log.debug("The user isSuspended. Error code: ERROR_CODE_REGULATION_SUSPENDED: " + ur);
 							result.setErrorCode(ERROR_CODE_REGULATION_SUSPENDED);					
 							result.addUserMessage(null, CommonUtil.getMessage(user.getLocale(), "error.investment.user.suspended", null));
 						}
 												
 						
 					//CHck if is done 	
 					}else if(ur.getApprovedRegulationStep() == UserRegulationBase.REGULATION_MANDATORY_QUESTIONNAIRE_DONE && ur.isKnowledgeQuestion()){
 						result.setErrorCode(ERROR_CODE_ET_REGULATION_KNOWLEDGE_CONFIRM);
 					}					
 			}else {
 				if(UsersManagerBase.isNonRegUserSuspend(user.getId())){
						log.debug("The user is manual Suspended. Error code: ERROR_CODE_REGULATION_SUSPENDED: " + ur);
						result.setErrorCode(ERROR_CODE_REGULATION_SUSPENDED);					
						result.addUserMessage(null, CommonUtil.getMessage(user.getLocale(), "error.investment.user.suspended", null));
 				}
 			}
 		} catch (SQLException e) {
 			log.error("Unable to load user regulation status", e);
 			result.setErrorCode(ERROR_CODE_UNKNOWN);
 		}
     }
   
    @PrintLogAnnotations(stopPrintDebugLog = true) 
    public static ChartDataResult getChartDataCommon(ChartDataRequest request, OpportunityCache oc, ChartHistoryCache chc, WebLevelsCache wlc, HttpServletRequest httpRequest) {
    	log.debug("getChartData: " + request);
   	 	log.debug("HttpServletRequest: " + httpRequest.toString());
       	ChartDataResult result = new ChartDataResult();
       	
       	long marketId = request.getMarketId();
       	long opportunityTypeId = request.getOpportunityTypeId();
       	long skinId = request.getSkinId();
       	
		try {
			if (marketId == 0) {
				if (0 <= request.getBox() && request.getBox() <= 3) {
					if (opportunityTypeId == Opportunity.TYPE_REGULAR) {
						log.debug("marketId is 0. Taking home page 1st market.");
						marketId = wlc.getMarketsOnHomePage(skinId)[request.getBox()];
					} else if (opportunityTypeId == Opportunity.TYPE_DYNAMICS) {// Dynamics
						marketId = getDynamicsMarketId(skinId).get(request.getBox());
					} else {// Option +
						marketId = CommonJSONService.getOptionPlusMarketId(skinId);
					}
				} else {
					log.warn("Invalid box number: " + request.getBox());
				}
			}
			result.setMarketId(marketId);
		} catch (Exception e) {
			log.warn("Can't parse marketId - " + e.getMessage());
		}
       	
       	// get Market
      // 	Market m = MarketsManagerBase.getMarkets().get(marketId);
       	// get Market 
       	
       	// get history param
   		int history = 0;// 1 current hour, 2 - one hour before 
   		// get history param
   		
       	// get User from session
       	User user  = (User) httpRequest.getSession().getAttribute("user");
       	// get User from session
       	
       	// get list of investments
       	List<Investment> investments;
   		try {
   			if (user != null && user.getId() > 0) {
   				long marketIdForInvestments = (opportunityTypeId != Opportunity.TYPE_OPTION_PLUS) ? marketId : 0l;
				investments = InvestmentsManagerBase.getOpenedInvestmentsByUser(user.getId(), 0l, request.getOpportunityTypeId(),
																				marketIdForInvestments);
   			} else {
   				investments = new ArrayList<Investment>();
   			}
   		} catch (SQLException e) {
   			log.warn("Unable to load investments", e);
   			investments = new ArrayList<Investment>();
   		}
   		//get list of investments
   		
   	SortedSet<WLCOppsCacheBean> marketOpportunities = wlc.getMarketOpportunities(marketId);
   	WLCOppsCacheBean opp = null;
   	if (marketOpportunities != null && !marketOpportunities.isEmpty()) {
   		Iterator<WLCOppsCacheBean> marketOpportunitiesIterator = wlc.getMarketOpportunities(marketId).iterator();
   		opp = marketOpportunitiesIterator.next();
   		if (opp.getState() > Opportunity.STATE_LAST_10_MIN) {
   			boolean shouldChangeToNewOpp = false;
       		WLCOppsCacheBean newOpp = null;
       		if (marketOpportunitiesIterator.hasNext()) {
       			newOpp = marketOpportunitiesIterator.next();
       			if (newOpp.getSchedule() == 1) {
       				shouldChangeToNewOpp = true;
       			}
       		}
   			// investments check
   			Iterator<Investment> investmentsIterator = investments.iterator();
   			boolean changeOpp = true;
   			while (investmentsIterator.hasNext()) {
   				Investment investment = investmentsIterator.next();
   				if (investment.getOpportunityId() == opp.getOpportunityId()) {
   					changeOpp = false;
   					break;
   				}
   			}
   			if (changeOpp && shouldChangeToNewOpp) {
   				opp = newOpp;
   			}
   		}
   	}
   	// chartData begin
   	OpportunityCacheBean opcb = null;
		if (opp != null) {
			try {
				opcb = oc.getOpportunity(opp.getOpportunityId());
			} catch (SQLException e) {
				log.warn("Can not load OpportunityCacheBean due to: ", e);
			}
		} else {
			log.warn("WLCOppsCacheBean is null...........");
		}


   	result.setEventLevel(opcb != null ? opcb.getCurrentLevel() : 0D);

   	result.setServerTime(String.valueOf(System.currentTimeMillis()));

   	result.setTimezoneOffset(String.valueOf(CommonUtil.getUtcOffsetInMin(CommonUtil.getUtcOffset(httpRequest.getSession(), httpRequest))));

   	//result.setTimeEstClose(null != opp ? String.valueOf(opp.getTimeEstClosing().getTime()) : "''");
   	
   	result.setTimeEstClosing(opcb != null ? opcb.getTimeEstClosing() : null);

   	result.setTimeFirstInvest(opcb != null ? opcb.getTimeFirstInvest() : null);
   	
   	result.setTimeLastInvest(opcb != null ? opcb.getTimeLastInvest() : null);

   	result.setScheduled(opcb != null ? opcb.getScheduled() : 0);

   	result.setDecimalPoint(opcb != null ? opcb.getMarketDecimalPoint() : 0);

   	// 'balance' begin
   	try {

   		if((user != null && user.getId() > 0)){
   			result.setBalance(user.getBalance());
   		}else{
   			result.setBalance(Double.valueOf(CommonUtil.displayAmount(0l, false, SkinsManagerBase.getSkin(skinId).getDefaultCurrencyId(),false)));
   		}
   	} catch (Exception e) {
   		log.debug("Can't get balance due to: ", e);
   	}
   	// 'balance' end

   	result.setOpportunityTypeId(opcb != null ? opcb.getOpportunityTypeId() : 0l);

   	result.setOpportunityId(opcb != null ? opcb.getId() : 0);


   	// 'hasPreviousHour' begin
   	if (opp != null) {
   		Map<SkinGroup, List<MarketRate>> ratesMap = chc.getMarketHistory(marketId);
   		List<MarketRate> l = null;
   		if (ratesMap != null) {
   			l = ratesMap.get(SkinsManagerBase.getSkin(skinId).getSkinGroup());
   		}
   		log.debug("rates from cache: " + (null != l ? l.size() : 0));
   		if (null != l && l.size() > 0) {
   			// Calculate current hour start time by the opp est closing time - 1 hour
   			// because day opps are considered hour opps in the end of the day and their
   			// opening time is in the morning
   			long chst = opp.getTimeEstClosing().getTime() - 60 * 60 * 1000;
   			while (chst > System.currentTimeMillis()) {
   				chst -= 60 * 60 * 1000;
   			}
   			result.setHasPreviousHour(l.get(0).getRateTime().getTime() < chst ? true : false);
   			// 'hasPreviousHour' end
   			
   			//'investments' begin

   			if (user != null && user.getId() > 0) {
               	String utcOffset = CommonUtil.formatJSUTCOffsetToString(request.getUtcOffset());
               	Investment[] investmentArr = new Investment[investments.size()];
                   for (int i = 0 ; i < investments.size() ; i++) {
                       investmentArr[i] = investments.get(i);
                       InvestmentHelper.initInvestment(investmentArr[i], skinId, utcOffset);
                   }
                   result.setInvestments(investmentArr);
                   log.debug("Got " + investmentArr.length + " investments for user " + user.getUserName());
   			}
   			// 'investments' end
   			
   			//'rates' begin
   			int sent = 0;
   			MarketRate mr = null;
   			ArrayList<Long> ratesTimes = new ArrayList<Long>();
   			ArrayList<Double> ratesValues = new ArrayList<Double>();
   			ArrayList<Double> lasts = new ArrayList<Double>();
   			ArrayList<Double> asks = new ArrayList<Double>();
   			ArrayList<Double> bids = new ArrayList<Double>();
   			
   			for (int i = 0; i < l.size(); i++) {
   				mr = l.get(i);
   					if ((history == 0 || (history == 1 && mr.getRateTime().getTime() >= chst) || (history == 2 && mr.getRateTime().getTime() < chst)) && 
   					   ((opp.getSchedule() == 1 && mr.getGraphRate().doubleValue() != 0) || (opp.getSchedule() != 1 && mr.getDayRate().doubleValue() != 0))) {
   						
   						ratesTimes.add(mr.getRateTime().getTime());
   						// level begin
   						if (opp.getSchedule() == 1) {
   							ratesValues.add(mr.getGraphRate().doubleValue());
   						} else {
   							ratesValues.add(mr.getDayRate().doubleValue());
   						}
   						if (mr.getLast() != null) {
   							lasts.add(mr.getLast().doubleValue());
   						}
   						if (mr.getAsk() != null) {
   							asks.add(mr.getAsk().doubleValue());
   						}
   						if (mr.getBid() != null) {
   							bids.add(mr.getBid().doubleValue());
   						}
   						// level end
   						sent++;
   					}
   			}
   			
               result.setRatesTimes(ratesTimes);
               result.setRates(ratesValues);
               if(lasts.size() > 0) {
            	   result.setLasts(lasts);
               }
               if (asks.size() > 0) {
            	   result.setAsks(asks);
               }
               if (bids.size() > 0) {
            	   result.setBids(bids);
               }
   			// 'rates' end
   			log.debug("rates sent: " + sent);
   		}
   	} 
   	
   	return result;
   } 
    
   	public static long pickRandomMarket(long skinId, boolean onlyOpened) throws SQLException {
   		ArrayList<Long> l = MarketsManagerBase.getCurrentMarketsByFeedName(skinId, onlyOpened, ConstantsBase.FEED_NAME_OPTION_PLUS);
   		if (l.size() > 0) {
   			Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("GMT+0"));
   			int h = cal.get(Calendar.HOUR_OF_DAY);
   			int m = cal.get(Calendar.MINUTE);
   			cal.set(Calendar.HOUR_OF_DAY, 0);
   			cal.set(Calendar.MINUTE, 0);
   			cal.set(Calendar.SECOND, 0);
   			cal.set(Calendar.MILLISECOND, 0);
   			long seed = cal.getTimeInMillis() + (h * 60 + m) / 5;
   			if (log.isDebugEnabled()) {
   				log.debug("cal: " + cal.getTimeInMillis() + " h: " + h + " m: " + m + " seed: " + seed);
   			}
   			/**
   			 * The "random" number generated by the Random class depends on the seed. With the above calculation we make sure that on all
   			 * web servers we get the same seed for every 5 min period starting from 00:00 GMT (thus the same "random" number).
   			 * 
   			 * NOTE: large groups of consecutive seed numbers generate the same random number for a small interval
   			 */
   			Random rnd1 = new Random(seed);
   			Random rnd2 = new Random(rnd1.nextLong());
   			return l.get(rnd2.nextInt(l.size()));
   		} else {
   			return 0;
   		}
   	}
   	
   	public static long getOptionPlusMarketId(long skinId) {
   		log.debug("marketId is 0. Calculating Option+ market for skin " + skinId);
   		long marketId;
   		try {
   			marketId = pickRandomMarket(skinId, true);
   			if (0 == marketId) {
   				marketId = pickRandomMarket(skinId, false);
   				if (0 == marketId) {
   					log.debug("No opened markets.");
   				}
   			}
   		} catch (SQLException e) {
   			log.debug("Can't get market", e);
   			return 0l;
   		}

   		return marketId;
   	}
   	
   	public static ArrayList<Long> getDynamicsMarketId(long skinId) {
   		log.debug("marketId is 0. Calculating Dynamics market for skin " + skinId);
   		ArrayList<Long> marketIds = new ArrayList<Long>();
   		try {
   			 marketIds = MarketsManagerBase.getCurrentMarketsByFeedName(skinId, true, ConstantsBase.FEED_NAME_DYNAMICS);//opened markets
   			if(marketIds.isEmpty()){
   				 marketIds = MarketsManagerBase.getCurrentMarketsByFeedName(skinId, false, ConstantsBase.FEED_NAME_DYNAMICS); // close markets
   	   				if (marketIds.isEmpty()) {
   	   					log.debug("No opened markets.");
   	   				}
   			}
   		} catch (SQLException e) {
   			log.debug("Can't get market", e);
   		}

   		return marketIds;
   	}
	  /**
     * insert new Dynamic Investment.
     *
     * @param request
     * @return InvestmentMethodResult.
     
    public static InvestmentMethodResult sellDynamicsInvestment(SellDynamicsInvestmentRequest request, OpportunityCache oc, HttpServletRequest httpRequest) {
    	log.debug("Insert Investment: " + request);
    	InvestmentMethodResult result = new InvestmentMethodResult();
    	try {
	      		HttpSession session = httpRequest.getSession();
	            
	    		// get user
	    		User user = (User) session.getAttribute("user");
	    		// return error - no user logged
	            if (null == user || user.getId() == 0) {
	            	result.setErrorCode(ERROR_CODE_SESSION_EXPIRED);
	                return result;
	            }
	            if(!user.getProductViewFlags().get(ConstantsBase.HAS_DYNAMICS_FLAG)) {
	            	log.error("user :" + user.getId() + " is not dynamics enabled !");
	            	result.setErrorCode(ERROR_CODE_UNKNOWN);
	                return result;
	            }
	            	
	            if (CommonUtil.isUserRegulated(user) || user.isETRegulation()) {
		            validateUserRegulation(user, result, VALIDATE_REGULATION_INVESTMENT);
		            if (result.getErrorCode() != ERROR_CODE_SUCCESS) {
		                return result;
		            }
	            }
	            
	    		result = (InvestmentMethodResult) initUser(result, user, request.getUtcOffset(),request.getWriterId());
		} catch (Exception e) {
			log.error("Error in insertInvestment.", e);
			result.setErrorCode(ERROR_CODE_UNKNOWN);
		}
		return result;
	}
    */	
 
   	
	  /**
     * insert new Dynamic Investment.
     *
     * @param request
     * @return InvestmentMethodResult.
     */
    public static InvestmentMethodResult insertInvestmentDynamics(InsertInvestmentMethodRequest request, OpportunityCache oc, User user, WebLevelsCache levelsCache, long loginId, String sessionId) {
    	log.debug("Insert Investment: " + request);
    	InvestmentMethodResult result = new InvestmentMethodResult();
    	result.setCancelSeconds(Integer.parseInt(Utils.getProperty("cancel.seconds.client")));
    	try {

    		// To set the user default amount value, because we get the user from db
            user.setDefaultAmountValue(request.getDefaultAmountValue());
            OpportunityCacheBean opportunity = oc.getOpportunity(request.getOpportunityId());
            if (!InvestmentsManagerBase.validateOpportunityType(opportunity, Opportunity.TYPE_DYNAMICS)) {
            	result.setErrorCode(ERROR_CODE_INVALID_INPUT);
            	return result;
            }

            Investment inv = new Investment();
            inv.setOpportunityId(request.getOpportunityId());
            inv.setAmount(new Double(request.getRequestAmount() * 100).longValue());
            inv.setCurrentLevel(request.getPageLevel());
            inv.setEventLevel(request.getPageLevel());
            inv.setOddsWin(request.getPageOddsWin());
            inv.setOddsLose(request.getPageOddsLose());
            inv.setTypeId(request.getChoice());
            inv.setUtcOffsetCreated(CommonUtil.formatJSUTCOffsetToString(request.getUtcOffset()));
            inv.setApiExternalUserId(request.getApiExternalUserId());
            inv.setCopyopType(CopyOpInvTypeEnum.SELF);
            inv.setPrice(request.getPrice());
            inv.setOpportunityTypeId(Opportunity.TYPE_DYNAMICS);
            String ip = request.getIp();
            if (!CommonUtil.IsParameterEmptyOrNull(request.getIpAddress())) {
            	ip = request.getIpAddress();
            }
            inv.setIp(ip);
            inv.setCurrencyId(user.getCurrencyId());
            inv.setTimeEstClosing(opportunity.getTimeEstClosing());
            InvestmentValidatorParams ivp = new InvestmentValidatorParams();
            Market market = MarketsManagerBase.getMarket(opportunity.getMarketId());

            ivp.setSecBetweenInvestmentsSameIp(market.getSecBetweenInvestmentsSameIpMobile());
            ivp.setSecBetweenInvestments(market.getSecBetweenInvestmentsMobile());
            InvestmentValidatorDynamics iv = new InvestmentValidatorDynamics(ivp);

            // submit investment
            MessageToFormat m = InvestmentsManagerBase.submitDynamicsInvestment(user, inv, opportunity, request.getWriterId(), sessionId, levelsCache, iv, loginId);
            Locale locale = new Locale(LanguagesManagerBase.getLanguage(SkinsManagerBase.getSkin(request.getSkinId()).getDefaultLanguageId()).getCode());
            String utcOffset = CommonUtil.formatJSUTCOffsetToString(request.getUtcOffset());
            if (null == m) {
                inv.setAmount(inv.getAmount() + inv.getOptionPlusFee());
                InvestmentHelper.initInvestment(inv, request.getSkinId(), utcOffset);
                result.setInvestment(inv);
                // TODO 
                //refreshUserInSession(user.getId(), httpRequest);
            } else {
            	String apiErrorCode = null;
            	int errorCode = ERROR_CODE_INV_VALIDATION;
                if (m.getKey() != null && m.getKey().equalsIgnoreCase("error.investment.nomoney")) {
                	apiErrorCode = ApiErrorCode.API_ERROR_CODE_VALIDATION_BALANCE_LOWER_REQ_INV_AMOUNT;
                	errorCode = ERROR_CODE_VALIDATION_LOW_BALANCE;
                } else if (m.getKey() != null && m.getKey().equalsIgnoreCase("error.investment.limit.min")) {
                	apiErrorCode = ApiErrorCode.API_ERROR_CODE_VALIDATION_AMOUNT_BELOW_MIN_INVESTMENT;
                } else if (m.getKey() != null && m.getKey().equalsIgnoreCase("error.investment.limit.max")) {
                	apiErrorCode = ApiErrorCode.API_ERROR_CODE_VALIDATION_AMOUNT_ABOVE_MAX_INVESTMENT;
                } else if (m.getKey() != null && m.getKey().equalsIgnoreCase("error.investment.notopened")) {
                	apiErrorCode =  ApiErrorCode.API_ERROR_CODE_VALIDATION_OPTION_CURRENTLY_NOT_AVAILABLE;
                } else if (m.getKey() != null && m.getKey().equalsIgnoreCase("error.investment.expired")) {
                	apiErrorCode = ApiErrorCode.API_ERROR_CODE_VALIDATION_OPTION_NO_LONGER_AVAILABLE;
                } else if (m.getKey() != null && m.getKey().startsWith(ConstantsBase.NO_CASH_FOR_NIOU_MSG)) {
                	errorCode = ERROR_CODE_NO_CASH_FOR_NIOU;
                	result.setUserCashBalance(m.getKey().split("=")[1]); //Quick fix...
                }
                result.setErrorCode(errorCode);
                result.addUserMessage("", CommonUtil.getMessage(locale, m.getKey(), m.getParams()), apiErrorCode);
            }
            result = (InvestmentMethodResult) initUser(result, user, request.getUtcOffset(),request.getWriterId());
    	} catch (Exception e) {
    		log.error("Error in insertInvestment.", e);
    		result.setErrorCode(ERROR_CODE_UNKNOWN);
		}
    	return result;
    }
    
    /**
     * return result with the user set and all the data he need.
     *
     * @param result the result we send back to client
     * @param user user we got from db
     * @return result with the user in that have all the needed data
     * @throws SQLException
     */
    public static UserMethodResult initUser(UserMethodResult result, User user, int utcOffset, long writerId) throws SQLException {
        user.setHasBonus(UsersManagerBase.userHasBonus(user.getId()));
        user.setHasSeenBonuses(UsersManagerBase.isBonusSeen(user.getId()));

        if (writerId != Writer.WRITER_ID_BUBBLES_DEDICATED_APP) {
	        LimitationDeposits ld = LimitationDepositsManagerBase.getLimitByUserId(user.getId());
	        if (ld.getDefaultDeposit() != 0) {
	        	user.setPredefinedDepositAmount(Long.toString(ld.getDefaultDeposit()/100));
	        }
        } else {
        	user.setPredefinedDepositAmount(String.valueOf(LimitsManagerBase.getBDAPredefinedDepositAmount(user.getCurrencyId()) / 100));
        }
        result.setUser(user);
        user.isBonusTabDisplayAllow();
        user.setOpenedInvestmentsCount(InvestmentsManagerBase.getUserOpenedInvestmentsCount(user.getId(), writerId));
        setUserRegulationParameters(result, user);

        return result;
    }
    
	/**
	 * @param result
	 * @param user
	 * @throws SQLException
	 */
	protected static void setUserRegulationParameters(UserMethodResult result,
														User user) throws SQLException {
		/*
		 * User regulation check and status get.
		 * If UserRegulationBase is already loaded, don't load it again.
		 */
		if ((result.getUserRegulation() == null && CommonUtil.isUserRegulated(user)) || user.isETRegulation()) {
			UserRegulationBase ur = new UserRegulationBase();
			ur.setUserId(user.getId());
			UserRegulationManager.getUserRegulation(ur);
			result.setUserRegulation(ur);
		}
	}

	public static UserFPResult getForgetPasswordContacts(UserMethodRequest request, HttpServletRequest httpRequest) {
		UserFPResult result = new UserFPResult();

		User user = new User() ;
		
		if(request.getUserName() != null) {
			try {
				UsersManagerBase.loadUserContacts(request.getUserName().toUpperCase(), user);
			} catch (SQLException e) {
				log.error("can't load user contacts :", e);
				result.setErrorCode(ERROR_CODE_UNKNOWN);
				return result;
			}
			
			String email = user.getEmail();
			String phone = user.getMobilePhone();
			if(email!=null) {
				int i = email.indexOf("@");
				if(i<0 ) {
					log.error("Invalid e-mail:"+email);
				} else if( i < 3) {
					String asterix = email.substring(0, i); 
					result.setObfuscatedEmail(email.replaceFirst( asterix ,  "***"));
				} else {
					String asterix = email.substring(0, i-2); 
					result.setObfuscatedEmail(email.replaceFirst( asterix ,  "***"));
				}
				if(phone != null && phone.length() > 0) {
					String phoneAsterix = phone.substring(0, phone.length()-2);
					result.setObfuscatedPhone(user.getMobilePhonePrefix() + " " + phone.replaceFirst(phoneAsterix, "***"));
				}
				httpRequest.getSession().setAttribute("emailForgotPass", email);
				httpRequest.getSession().setAttribute("phoneForgotPass", phone);
				httpRequest.getSession().setAttribute("prefixForgotPass", user.getMobilePhonePrefix());
				return result;
			} else { 
				result.setErrorCode(ERROR_CODE_INVALID_ACCOUNT);
			}
		} else {
			result.setErrorCode(ERROR_CODE_INVALID_ACCOUNT);
		}
		
		return result;
	}
	
	 @PrintLogAnnotations(stopPrintDebugLog = true)
	public static MarketsGroupsMethodResult getMarketGroups(MethodRequest request, WebLevelsCache wlc) {
        MarketsGroupsMethodResult result = new MarketsGroupsMethodResult();
        
        log.info("Action: getMarketGroups: skinId: " + request.getSkinId());
        try {
              Hashtable<Long, MarketGroup> marketsGroups = SkinsManagerBase.getSkinGroupsMarkets(request.getSkinId());
              Map<Long, Set<Long>> marketsByGroup = wlc.getMarketsByGroup(request.getSkinId());
              com.anyoption.common.beans.base.MarketGroup[] arr = new com.anyoption.common.beans.base.MarketGroup[marketsGroups.size()];
              Locale locale = new Locale(LanguagesManagerBase.getLanguage(SkinsManagerBase.getSkin(request.getSkinId()).getDefaultLanguageId()).getCode());
              int i = 0;
              for (MarketGroup item : marketsGroups.values()) {
	        	  MarketGroup temp = (MarketGroup) item.clone();
	        	  log.debug("group id = " + item.getId() + " group name " + item.getName());
	        	  if (temp.getId() != ConstantsBase.MARKET_GROUP_OPTION_PLUS_ID && temp.getId() != ConstantsBase.MARKET_GROUP_DYNAMICS_ID) { //if not option + translate
	        		  temp.setName(CommonUtil.getMessage(locale, temp.getName(), null));
	        	  }
	        	  Set<Long> markets = marketsByGroup != null ? marketsByGroup.get(temp.getId()) : null;
	              for (com.anyoption.common.beans.base.Market m : temp.getMarkets()) {
	                  m.setDisplayName(MarketsManagerBase.getMarketName(request.getSkinId(), m.getId()));
	                  m.setTopMarketForGroup(markets != null && markets.contains(m.getId()));
	                  m.setNewMarket(MarketsManagerBase.isNewMarket(m));
	              }
	              if (request.getSkinId() == Skin.SKIN_ETRADER) {
	            	  int j = i + 2;
	            	  if (temp.getId() == ConstantsBase.MARKET_GROUP_OPTION_PLUS_ID) {
	            		  j = 1;
	            	  }
	            	  arr[marketsGroups.size() - j] = temp;
	              } else {
	                  arr[i] = temp;
	              }
	              i++;
              }
              result.setGroups(arr);
        } catch (Exception e) {
              log.error("Error in getMarketGroups.", e);
              //result.setErrorCode(ERROR_CODE_UNKNOWN);
        }
        return result;
    }
	
	/**
     * Get all binary markets for trading
     * @param request
     * @return
     */
     public static MarketsMethodResult getAllBinaryMarkets(MethodRequest request) {
     	log.info("Action: getAllBinaryMarkets: skinId: " + request.getSkinId());
     	MarketsMethodResult result = new MarketsMethodResult();
 	   	try {
 	   		Hashtable<Long, Market> binaryHourlyMarkets = SkinsManagerBase.getAllBinaryMarketsForSkinHM(request.getSkinId());
         	ArrayList<Market> marketsClone = new ArrayList<Market>();
            Locale locale = new Locale(LanguagesManagerBase.getLanguage(SkinsManagerBase.getSkin(request.getSkinId()).getDefaultLanguageId()).getCode());
            for (Iterator<Long> iter = binaryHourlyMarkets.keySet().iterator(); iter.hasNext();) {
            	 	Long marketId = iter.next();
             		Market marketClone = binaryHourlyMarkets.get(marketId).clone();
             		marketClone.setDisplayName(MarketsManagerBase.getMarketName(request.getSkinId(), marketClone.getId()));
             		marketClone.setGroupName(CommonUtil.getMessage(locale, marketClone.getGroupName(), null));
                 	marketsClone.add(marketClone);
            }
	         class AlphaComparator implements Comparator<Market> {
	         	private Locale locale;
	         	public AlphaComparator(Locale locale) {
	         		this.locale = locale;
	         	}
	             @Override
				public int compare(Market a, Market b) {
	                 String aLabel = a.getDisplayName();
	                 String bLabel = b.getDisplayName();
	                 int returnVal = 0;
	                 try {
	                     Collator collator = Collator.getInstance(locale);
	                     returnVal = collator.compare(aLabel, bLabel);
	                 } catch (Exception e) {
	                 }
	     			return returnVal;
	             }
	        }
            Collections.sort(marketsClone, new AlphaComparator(locale));
 	        result.setMarkets(marketsClone.toArray(new Market[marketsClone.size()]));
         } catch (Exception e) {
        	 log.error("Error in getAllBinaryMarkets.", e);
             result.setErrorCode(ERROR_CODE_UNKNOWN);
         }
 	   	return result;
    }
     
 	public static MarketsNamesMethodResult getMarketsNames(MethodRequest request, HttpServletRequest httpRequest) {
		log.debug("getMarketsNames: " + request);
		long beginTime = System.currentTimeMillis();
		MarketsNamesMethodResult result = new MarketsNamesMethodResult();
		try {
			Hashtable<Long, Market> table = MarketsManagerBase.getAll();
			HashMap<Long, String> marketList = new HashMap<Long, String>();
			Set<Long> keys = table.keySet();
			Iterator<Long> iter = keys.iterator();
			while ( iter.hasNext() ) {
				Long marketId = (Long)iter.next();
				marketList.put(marketId, table.get(marketId).getName());
			}
			result.setMarketsList(marketList);
			
		} catch (SQLException ex) {
			log.error("Can not get market names: ", ex);
			result.setErrorCode(1);
			return result;
		}
		long endTime = System.currentTimeMillis();
		if (log.isDebugEnabled()) {
			log.debug("TIME: getMarketsNames " + (endTime - beginTime) + "ms");
		}
		
		return result;
	}
 	
    public static EmailValidatorResult validateEmail(EmailValidatorRequest request) {
    	EmailValidatorResult result = new EmailValidatorResult();
    	result.setReturnCode(EmailValidatorUtil.validate(request.getEmail()));
    	return result;
    }
    
	public static TermsPartFilesMethodResult getTermsFilesBase(TermsFileMethodRequest request, boolean isPreview) {
		TermsPartFilesMethodResult result = new TermsPartFilesMethodResult();
		String pathFile = CommonUtil.getProperty(ConstantsBase.TERMS_FILES_PATH);
		try {
			ArrayList<TermsPartFile> filesList = null; 
			if(isPreview){
				pathFile = CommonUtil.getProperty(ConstantsBase.TERMS_FILES_PATH_PREVIEW);
				TermsPartFile filePreview = TermsManagerBase.getTermsFile(request.getPlatformIdFilter(), request.getSkinIdFilter(), request.getPartIdFilter());
				filesList = new ArrayList<>();
				filesList.add(filePreview);
			} else {
				filesList = TermsManagerBase.getTermsList(request.getPlatformIdFilter(), request.getSkinIdFilter(), request.getTermsType());
			}			
			
			HashMap<String, String> params = TermsManagerBase.getParams(request.getCurrencyId(), request.getSkinIdFilter(), request.getPlatformIdFilter());
			for (TermsPartFile termsPartFile : filesList) {
				org.apache.velocity.Template templateFile = getTemplate(termsPartFile.getFileName(), pathFile, isPreview);				
				VelocityContext context = new VelocityContext();
		        String paramName = "";
				for (Iterator<String> keys = params.keySet().iterator(); keys.hasNext();) {
					paramName = (String) keys.next();
					context.put(paramName, params.get(paramName));
				}
				StringWriter sw = new StringWriter();
				templateFile.merge(context, sw);
				
				termsPartFile.setHtml(sw.toString());
			}
			result.setFilesList(filesList);
		} catch (Exception e) {
			log.error("Unable to get getTermsFile", e);
			result.setErrorCode(ERROR_CODE_UNKNOWN);
		}
		return result;
	}
	
	private static Template getTemplate(String fileName, String filePath, boolean isPreview) throws Exception {
		try {
			Properties props = new Properties();
			props.setProperty("file.resource.loader.path", filePath);
			props.setProperty("resource.loader","file");
			props.setProperty("file.resource.loader.class","org.apache.velocity.runtime.resource.loader.FileResourceLoader");
			props.setProperty("file.resource.loader.cache",String.valueOf(isPreview));
			Velocity.init(props);
				return Velocity.getTemplate(fileName,"UTF-8");
		} catch (Exception ex) {
			log.fatal("ERROR! Cannot find Report template : " + ex.getMessage());
			throw ex;
		}
	}

	public static boolean cancelInvestment(CancelInvestmentMethodRequest request, User user, OpportunityCache oc, WebLevelsCache levelsCache, HttpSession session, long loginId, long cancelTimeCumulative) throws SQLException {
		log.debug(request.toString());
		long investmentId = request.getInvestmentId();
		if(request.getInvestmentId() == 0) {
			OpportunityCacheBean opportunity = oc.getOpportunity(request.getOpportunityId());
			if(opportunity == null) {
				log.error("Empty opportunity id ! Cannot create inv for cancel :" + request.getOpportunityId());
				return false;
			}
			if(Calendar.getInstance().getTimeInMillis() > opportunity.getTimeLastInvest().getTime()) {
				log.error("Opportunity is closed for investment : Cannot create inv for cancel :" + request.getOpportunityId());
				return false;
			}
			WebLevelLookupBean wllb = new WebLevelLookupBean();
		    wllb.setSkinGroup(SkinsManagerBase.getSkin(user.getSkinId()).getSkinGroup());	            
	        Market market = MarketsManagerBase.getMarket(opportunity.getMarketId());
	        wllb.setOppId(request.getOpportunityId());
	        wllb.setFeedName(market.getFeedName());
			levelsCache.getLevels(wllb);
			double rate = CurrencyRatesManagerBase.getUSDRate(user.getCurrencyId());
			// validate against the session paramValues
			if(!validateCancelRequest(session, request, request.getOpportunityId())) {
				return false;
			} else {
				session.removeAttribute(ConstantsBase.REQUEST_PARAMETER_ID + request.getOpportunityId());
			}
			
			investmentId = InvestmentsManagerBase.insertInvestment(
																		user,
																		opportunity,
																		request.getAmount(),
																		request.getChoice(),
																		Double.parseDouble(request.getPageLevel()),
																		request.getIp(),
																		wllb.getRealLevel(),
																		wllb.getDevCheckLevel(),
																		CommonUtil.formatJSUTCOffsetToString(request.getUtcOffset()),
																		rate,
																		request.getFromGraph(),
																		request.getWriterId(),
																		0l,		// optionPlusFee
																		user.getCountryId(),
																		0l,		// apiExternalUserID
																		Double.parseDouble(request.getOddWin()),
																		Double.parseDouble(request.getOddLose()),
																		0l,		// copyopInvId
																		CopyOpInvTypeEnum.SELF,
																		wllb.isClosingFlagClosest(),
																		0l,		// destUserId
																		null,	// bubbleStartTime
																		null,	// bubbleEndTime
																		0d,		//bubleLowLevel
																		0d,		// bubbleHighLevel
																		loginId,
																		0d
																	);
		}
		Investment i = InvestmentsManagerBase.getById(investmentId);
		return InvestmentsManagerBase.userCancelInvestment(i, request.getWriterId(), levelsCache, user, cancelTimeCumulative, false);
	}

	protected static void changeCancelInvestmentCheckboxState(MethodResult result, CancelInvestmentCheckboxStateMethodRequest request, User user) {
		boolean res = UsersManagerBase.changeCancelInvestmentCheckboxState(user.getId(), request.isShowCancelInvestment());
		if (res) {
			user.setShowCancelInvestment(request.isShowCancelInvestment());
		} else {
			log.warn("User state not updated. User show cancel investment state: " + user.isShowCancelInvestment());
		}
	}
	
	public static boolean validateCancelRequest(HttpSession session, CancelInvestmentMethodRequest request, long oppId) {
		if (session.getAttribute(ConstantsBase.REQUEST_PARAMETER_ID + oppId) instanceof Map<?, ?>) {
    		Map<?, ?> sessionParams = (Map<?, ?>) session.getAttribute(ConstantsBase.REQUEST_PARAMETER_ID + oppId);
    		synchronized (sessionParams) {
    			if (session.getAttribute(ConstantsBase.REQUEST_PARAMETER_ID + oppId) != null) {
        			if (sessionParams.get(ConstantsBase.REQUEST_PARAMETER_AMOUNT) != null
        					&& ((Double) sessionParams.get(ConstantsBase.REQUEST_PARAMETER_AMOUNT)).doubleValue() == request.getAmount()
        					&& sessionParams.get(ConstantsBase.REQUEST_PARAMETER_PAGE_LEVEL) != null
        					/*&& ((Double) sessionParams.get(ConstantsBase.REQUEST_PARAMETER_PAGE_LEVEL)).doubleValue() == Double.parseDouble(request.getPageLevel())*/
        					&& sessionParams.get(ConstantsBase.REQUEST_PARAMETER_PAGE_ODDS_WIN) != null
        					/*&& sessionParams.get(ConstantsBase.REQUEST_PARAMETER_PAGE_ODDS_WIN) == request.getOddWin()*/
        					&& sessionParams.get(ConstantsBase.REQUEST_PARAMETER_PAGE_ODDS_LOSE) != null
        					/*&& sessionParams.get(ConstantsBase.REQUEST_PARAMETER_PAGE_ODDS_LOSE) == request.getOddLose()*/
        					&& sessionParams.get(ConstantsBase.REQUEST_PARAMETER_CHOISE) != null
        					/*&& sessionParams.get(ConstantsBase.REQUEST_PARAMETER_CHOISE) == Integer.toString(request.getChoice())*/
        					&& sessionParams.get(ConstantsBase.REQUEST_PARAMETER_TIME_BEGIN) != null) {
        				return true;
        			}
    			} 
    		}
    	}
		log.debug("validateCancelRequest failed. sessionParams: " + session.getAttribute(ConstantsBase.REQUEST_PARAMETER_ID + oppId));
		return false;
	}
	
	 public static Skin defineSkinAndSetInSession(HttpServletRequest httpRequest, MethodRequest request, boolean isFirtsOpening, boolean isSkinUpdate, Long skinIdByLocale){
    	Skin skin = null;
    	try {			
	    	Long sessionSkinId = (Long) httpRequest.getSession().getAttribute(ConstantsBase.SESSION_SKIN);
            Long deviceSavedSkinId = null;
	    	if(!isFirtsOpening && request.getSkinId() > 0){
	    		deviceSavedSkinId = request.getSkinId();
	    	}

	    	Long newSelectedSkinId = null;
	    	if(isSkinUpdate && request.getSkinId() > 0){
	    		newSelectedSkinId = request.getSkinId();
	    	}

	    	Long countryIdDetectByIp = null;
	    	if(sessionSkinId == null){
	    		countryIdDetectByIp = getCountryFromIp(request.getIp(), httpRequest);
	    	}

	    	Long subdomain = newSelectedSkinId;
	    	//Only now for copyopWeb
	    	if(skinIdByLocale != null){
	    		subdomain = skinIdByLocale;
	    	}	    	
			skin = SkinsManagerBase.defineSkinNoUser(null, newSelectedSkinId, subdomain, null, deviceSavedSkinId, countryIdDetectByIp, sessionSkinId);
		} catch (Exception e) {
			log.error("When define Skin ", e);
		}
    	if(skin == null){
            log.debug("Can't define skin, get default");
            long skinId = Long.valueOf(Utils.getProperty("skin.default"));
    		skin = SkinsManagerBase.getSkin(skinId);
    	}
    	//Set in session
    	httpRequest.getSession().setAttribute(ConstantsBase.SESSION_SKIN, new Long(skin.getId()));
    	return skin;
    }
    
    public static Long getCountryFromIp(String ip, HttpServletRequest httpRequest){
		Long countryId = (Long) httpRequest.getSession().getAttribute(ConstantsBase.SESSION_COUNTRY_ID);
		if(countryId == null){
			countryId = getCountryFromIp(ip);
			if(countryId != null){
				httpRequest.getSession().setAttribute(ConstantsBase.SESSION_COUNTRY_ID, new Long(countryId));
			}
		}		
		return countryId;
	}
        
    private static Long getCountryFromIp(String ip){
		Long countryId = null;
		try {    	
			String countryCode = CommonUtil.getCountryCodeByIp(ip); 
			if(!CommonUtil.isParameterEmptyOrNull(countryCode)){
				countryId = CountryManagerBase.getIdByCode(countryCode);
			}			
		} catch (Exception e) {
			log.error("Can't get countryId from IP", e);
		}
		return countryId;
	}
    
    @PrintLogAnnotations(stopPrintDebugLog = true)
	public static CountriesListMethodResult getSortedCountries (MethodRequest request, HttpServletRequest httpRequest) {
		log.debug("getSortedCountries:" + request);
		Long skinId = (Long) httpRequest.getSession().getAttribute(ConstantsBase.SESSION_SKIN);
		if(skinId == null){
			skinId = request.getSkinId();
		}
		Skin s = SkinsManagerBase.getSkin(skinId);		
		CountriesListMethodResult result = new CountriesListMethodResult();
		result.setIpDetectedCountryId(getCountryFromIp(request.getIp(), httpRequest));
		result.setSkinDefautlCointryId(s.getDefaultCountryId());
		result.setCountriesMap(CountryManagerBase.getSortedCountriesMap(s.getId()));
		return result;
	}
    
    public static UserQuestionnaireResult getUserSingleQuestionnaireDynamic(User user, long writerId) {
    	
    	UserQuestionnaireResult result = new UserQuestionnaireResult();
    	try {

			List<QuestionnaireQuestion> cache = getQuestionnaireQuestions( QuestionnaireGroup.REGULATION_SINGLE_QUESTIONNAIRE_NAME);
			List<QuestionnaireQuestion> questions = new ArrayList<QuestionnaireQuestion>(cache.size());
			questions.addAll(cache);
			for(QuestionnaireQuestion question: questions) {
				question.setTranslatation(translateQA(question.getName(), user.getSkinId(), writerId));
				List<QuestionnaireAnswer> questionAnswers = question.getAnswers();
				for(QuestionnaireAnswer answer: questionAnswers) {
					answer.setTranslation(translateQA(answer.getName(), user.getSkinId(), writerId));
				}
			}
			List<QuestionnaireUserAnswer> userAnswers = QuestionnaireManagerBase.getUserAnswers(user.getId(), questions, QuestionnaireGroup.REGULATION_SINGLE_QUESTIONNAIRE_ID);

			result.setQuestions(questions);
			result.setUserAnswers(userAnswers);
		
		} catch (Exception e) {
			log.error("Error in get user questionnaire [" +  QuestionnaireGroup.REGULATION_SINGLE_QUESTIONNAIRE_NAME + "]", e);
			result.setErrorCode(ERROR_CODE_UNKNOWN);
		}
    	return result;
    }
    
    protected static List<QuestionnaireQuestion> getQuestionnaireQuestions(String questionnaireName) throws SQLException {
    	List<QuestionnaireQuestion> questions = QuestionnaireManagerBase.getAllQuestionnaires().get(questionnaireName).getQuestions();
    	if (QuestionnaireGroup.REGULATION_OPTIONAL_QUESTIONNAIRE_NAME.equals(questionnaireName)) {
    		QuestionnaireQuestion multipleAnswerQuestion = null;
			multipleAnswerQuestion = questions.get(QUESTION_INCOME_SOURCE_IDX);
			questions = new ArrayList<QuestionnaireQuestion> (questions);
			questions.remove(multipleAnswerQuestion);
			questions.add(multipleAnswerQuestion);
			questions.add(multipleAnswerQuestion);
			questions.add(multipleAnswerQuestion);
			questions.add(multipleAnswerQuestion);
    	}
    	return questions;
    }
    
    private static String translateQA(String key, long skinId, long writerId) {
    	// TODO - russian on copyop is english
		Locale locale = new Locale(LanguagesManagerBase.getLanguage(SkinsManagerBase.getSkin(skinId).getDefaultLanguageId()).getCode());
    	return CommonUtil.getMessage(locale, key, null);
    }
    // TODO - merge json & web
    public static UserMethodResult updateUserQuestionnaireAnswers(User user, UpdateUserQuestionnaireAnswersRequest request, UserQuestionnaireResult result) throws Exception {
			Collection<QuestionnaireGroup> groups = QuestionnaireManagerBase.getAllQuestionnaires().values();
			String questionnaireName = null;
			for (QuestionnaireGroup questionnaireGroup : groups) {
				if (questionnaireGroup.getId() == request.getUserAnswers().get(0).getGroupId()) {
					questionnaireName = questionnaireGroup.getName();
				}
			}
			UserRegulationBase ur = new UserRegulationBase();
				if ((!CommonUtil.isUserRegulated(user) && user.getSkinId() != Skin.SKIN_ETRADER) 
					|| (!user.isETRegulation() && user.getSkinId() == Skin.SKIN_ETRADER)){
					if (QuestionnaireGroup.REGULATION_MANDATORY_QUESTIONNAIRE_NAME.equals(questionnaireName)
							|| QuestionnaireGroup.REGULATION_SINGLE_QUESTIONNAIRE_NAME.equals(questionnaireName)
							|| QuestionnaireGroup.REGULATION_OPTIONAL_QUESTIONNAIRE_NAME.equals(questionnaireName)
							|| QuestionnaireGroup.REGULATION_CAPITAL_QUESTIONNAIRE_NAME.equals(questionnaireName)) {
						log.error("Requesting regulation questionnaire of a non-regulated user: " + request.getUserName());
						result.setErrorCode(ERROR_CODE_USER_NOT_REGULATED);
						return result;
				} /* else {
					OK
				} */
			} else {
				validateUserRegulationAndNonReg(user, result, VALIDATE_REGULATION_OTHER);
				if (result.getErrorCode() != ERROR_CODE_SUCCESS) {
	                return result;
	            }
				
				
				
				ur.setUserId(user.getId());
				UserRegulationManager.getUserRegulation(ur);
	
				
				if(!user.isETRegulation()){
					//AO Regulation
					if(!(QuestionnaireGroup.REGULATION_MANDATORY_QUESTIONNAIRE_NAME.equals(questionnaireName) 
							|| QuestionnaireGroup.REGULATION_OPTIONAL_QUESTIONNAIRE_NAME.equals(questionnaireName) 
							|| QuestionnaireGroup.REGULATION_SINGLE_QUESTIONNAIRE_NAME.equals(questionnaireName))){
						log.error("Wrong QUESTIONNAIRE_NAME! " + questionnaireName);
						result.setErrorCode(ERROR_CODE_QUESTIONNAIRE_NOT_SUPPORTED);
						return result;
					}
					if (((QuestionnaireGroup.REGULATION_MANDATORY_QUESTIONNAIRE_NAME.equals(questionnaireName) || QuestionnaireGroup.REGULATION_SINGLE_QUESTIONNAIRE_NAME.equals(questionnaireName))
							&& ur.getApprovedRegulationStep() >= UserRegulationBase.REGULATION_MANDATORY_QUESTIONNAIRE_DONE)
						|| (QuestionnaireGroup.REGULATION_OPTIONAL_QUESTIONNAIRE_NAME.equals(questionnaireName)
							&& ur.isOptionalQuestionnaireDone())) {
					log.error("Trying to update answers of questionnaire [" + questionnaireName + "] that is already submitted");
					result.setErrorCode(ERROR_CODE_QUESTIONNAIRE_ALREADY_DONE);
					return result;
					}
				} else {
					// ET Regulation
					if(!QuestionnaireGroup.REGULATION_CAPITAL_QUESTIONNAIRE_NAME.equals(questionnaireName)){
						log.error("Wrong QUESTIONNAIRE_NAME! " + questionnaireName);
						result.setErrorCode(ERROR_CODE_QUESTIONNAIRE_NOT_SUPPORTED);
						return result;
					}
					if((QuestionnaireGroup.REGULATION_CAPITAL_QUESTIONNAIRE_NAME.equals(questionnaireName)
							&& ur.getApprovedRegulationStep() >= UserRegulationBase.ET_CAPITAL_MARKET_QUESTIONNAIRE)){
						log.error("Trying to update answers of questionnaire [" + questionnaireName + "] that is already submitted");
						result.setErrorCode(ERROR_CODE_QUESTIONNAIRE_ALREADY_DONE);
					}					
				}
	
				// TODO Remove this if statement when question 5 QUESTION_FUNDS_ORIGIN is viewable again.
				if (QuestionnaireGroup.REGULATION_MANDATORY_QUESTIONNAIRE_NAME.equals(questionnaireName)) {
					request
						.getUserAnswers()
						.get(QUESTION_FUNDS_ORIGIN_IDX)
						.setAnswerId(
								getQuestionnaireQuestions(QuestionnaireGroup.REGULATION_MANDATORY_QUESTIONNAIRE_NAME)
									.get(QUESTION_FUNDS_ORIGIN_IDX)
									.getAnswers()
									.get(ANSWER_YES_IDX)
									.getId());
				}
	
				result.setUserRegulation(ur);
			}
				
			QuestionnaireGroup questionnaireCapital = null;
			if (QuestionnaireGroup.REGULATION_CAPITAL_QUESTIONNAIRE_NAME.equals(questionnaireName)) {
				questionnaireCapital = QuestionnaireManagerBase.getAllQuestionnaires().get(QuestionnaireGroup.REGULATION_CAPITAL_QUESTIONNAIRE_NAME);
			}
			
			for (QuestionnaireUserAnswer userAnswer : request.getUserAnswers()) {
				userAnswer.setWriterId(request.getWriterId());
				if (QuestionnaireGroup.REGULATION_CAPITAL_QUESTIONNAIRE_NAME.equals(questionnaireName)) {
					userAnswer.setScore(QuestionnaireManagerBase.getAnswertScore(questionnaireCapital, userAnswer));
				}
			}
			QuestionnaireManagerBase.updateUserAnswers(request.getUserAnswers(), user.isETRegulation());			
	
			initUser(result, user, request.getUtcOffset(), request.getWriterId());

		return result;
    }

    public static UserMethodResult updateQuestionnaireDone(User user, long writerId, int utcOffset, String questionnaireName, HttpServletRequest httpRequest) throws Exception {

    	UserMethodResult result = new UserMethodResult();

			if ((!CommonUtil.isUserRegulated(user) && user.getSkinId() != Skin.SKIN_ETRADER) 
					|| (!user.isETRegulation() && user.getSkinId() == Skin.SKIN_ETRADER)){
				if (QuestionnaireGroup.REGULATION_MANDATORY_QUESTIONNAIRE_NAME.equals(questionnaireName)
						|| QuestionnaireGroup.REGULATION_SINGLE_QUESTIONNAIRE_NAME.equals(questionnaireName)
						|| QuestionnaireGroup.REGULATION_OPTIONAL_QUESTIONNAIRE_NAME.equals(questionnaireName)
						|| QuestionnaireGroup.REGULATION_CAPITAL_QUESTIONNAIRE_NAME.equals(questionnaireName)) {
					log.error("Requesting regulation questionnaire of a non-regulated user: " + user.getUserName());
					result.setErrorCode(ERROR_CODE_USER_NOT_REGULATED);
					return result;
				} /* else {
					OK
				} */
			} else {
				validateUserRegulationAndNonReg(user, result, VALIDATE_REGULATION_OTHER);
				if (result.getErrorCode() != ERROR_CODE_SUCCESS) {
	                return result;
	            }

				UserRegulationBase ur = new UserRegulationBase();
				ur.setUserId(user.getId());
				UserRegulationManager.getUserRegulation(ur);
				result.setUserRegulation(ur);
				
				if(!user.isETRegulation()){
					//AO Regulation
					if(!(QuestionnaireGroup.REGULATION_MANDATORY_QUESTIONNAIRE_NAME.equals(questionnaireName) 
							|| QuestionnaireGroup.REGULATION_OPTIONAL_QUESTIONNAIRE_NAME.equals(questionnaireName)
							|| QuestionnaireGroup.REGULATION_SINGLE_QUESTIONNAIRE_NAME.equals(questionnaireName))){
						log.error("Wrong QUESTIONNAIRE_NAME! " + questionnaireName);
						result.setErrorCode(ERROR_CODE_QUESTIONNAIRE_NOT_SUPPORTED);
						return result;
					}
					if (((QuestionnaireGroup.REGULATION_MANDATORY_QUESTIONNAIRE_NAME.equals(questionnaireName) || QuestionnaireGroup.REGULATION_SINGLE_QUESTIONNAIRE_NAME.equals(questionnaireName))
							&& ur.getApprovedRegulationStep() >= UserRegulationBase.REGULATION_MANDATORY_QUESTIONNAIRE_DONE)
						|| (QuestionnaireGroup.REGULATION_OPTIONAL_QUESTIONNAIRE_NAME.equals(questionnaireName)
							&& ur.isOptionalQuestionnaireDone())) {
					log.error("Trying to submit questionnaire [" + questionnaireName + "] that is already submitted");
					result.setErrorCode(ERROR_CODE_QUESTIONNAIRE_ALREADY_DONE);
					return result;
					}
				} else {
					// ET Regulation
					if(!QuestionnaireGroup.REGULATION_CAPITAL_QUESTIONNAIRE_NAME.equals(questionnaireName)){
						log.error("Wrong QUESTIONNAIRE_NAME! " + questionnaireName);
						result.setErrorCode(ERROR_CODE_QUESTIONNAIRE_NOT_SUPPORTED);
						return result;
					}
					if ((QuestionnaireGroup.REGULATION_CAPITAL_QUESTIONNAIRE_NAME.equals(questionnaireName)
							&& ur.getApprovedRegulationStep() >= UserRegulationBase.ET_CAPITAL_MARKET_QUESTIONNAIRE)) {
					log.error("Trying to submit questionnaire [" + questionnaireName + "] that is already submitted");
					result.setErrorCode(ERROR_CODE_QUESTIONNAIRE_ALREADY_DONE);
					return result;
					}
				}
				
			}

			List<QuestionnaireQuestion> questions = getQuestionnaireQuestions(questionnaireName);
			List<QuestionnaireUserAnswer> userAnswers = QuestionnaireManagerBase.getUserAnswers(user.getId(), questions, writerId);
			UserRegulationBase ur = result.getUserRegulation();
			if (ur != null) {
				if (QuestionnaireGroup.REGULATION_MANDATORY_QUESTIONNAIRE_NAME.equals(questionnaireName)
						|| QuestionnaireGroup.REGULATION_SINGLE_QUESTIONNAIRE_NAME.equals(questionnaireName)) {
					for (QuestionnaireUserAnswer userAnswer : userAnswers) {
						if (userAnswer.getAnswerId() == null) {
							log.error("Questionnaire [" + questionnaireName + "] not full - cannot mark it as [DONE]");
							result.setErrorCode(ERROR_CODE_QUESTIONNAIRE_NOT_FULL);
							return result;
						}
					}
					
					if(QuestionnaireGroup.REGULATION_SINGLE_QUESTIONNAIRE_NAME.equals(questionnaireName) 
							&& ur.getApprovedRegulationStep() < UserRegulationBase.REGULATION_FIRST_DEPOSIT){
						log.error("Can't submit SINGLE_QUESTIONNAIRE, user must be make First Deposit!");
						result.setErrorCode(ERROR_CODE_UNKNOWN);
						return result;
					}

					// TODO Remove this line when question 5 QUESTION_FUNDS_ORIGIN is viewable again.
					userAnswers.get(QUESTION_FUNDS_ORIGIN_IDX)
									.setAnswerId(questions.get(QUESTION_FUNDS_ORIGIN_IDX).getAnswers().get(ANSWER_YES_IDX).getId());
					log.warn("User [" + user.getId() + "] answered correct, moving to optional questionnaire...");
					ur.setApprovedRegulationStep(UserRegulationBase.REGULATION_MANDATORY_QUESTIONNAIRE_DONE);
					ur.setWriterId(writerId);
					UserRegulationManager.updateRegulationStep(ur);
					log.warn("User [" + user.getId() + "] create new issue for regulation documents...");

					long tempCCId = -1l;
					Transaction tran = TransactionsManagerBase.getTransaction(user.getFirstDepositId());
		    	     if(null !=tran) {
		    	    	 if (tran.getTypeId() == 1) {
		    	    		 tempCCId = tran.getCreditCardId().longValue();
		    	    	 }
		    		 }  else {
		    			 log.warn("First transaction is null :"+user.getFirstDepositId());
		    		 }
					
					if (QuestionnaireGroup.REGULATION_SINGLE_QUESTIONNAIRE_NAME.equals(questionnaireName)){
						//Check Answers
						long restQuest = QuestionnaireManagerBase.isSingelQuestionnaireRestrictedFilled(user.getId());
						if(QuestionnaireManagerBase.isSingelQuestionnaireIncorrectFilled(user.getId())){
							ur.setScoreGroup(UserRegulationBase.AO_REGULATION_RESTRICTED_FAILED_Q_SCORE_GROUP_ID);
							QuestionnaireManagerBase.updateUserScoreGroupOnly(ur);
							result.setErrorCode(ERROR_CODE_REG_SUSPENDED_QUEST_INCORRECT);
							IssuesManagerBase.insertIssueUserRegulationRestricted(user.getId(), IssuesManagerBase.ISSUE_ACTION_TYPE_RESTRICTED_FAILED_QUESTIONNAIRE, Writer.WRITER_ID_AUTO);
							//UsersManagerBase.sendMailAfterQuestAORegulation(ur);
							log.debug("User " + user.getId() + " was suspended(" + ur.getSuspendedReasonId() + "): Incorrect Filled answers");
						} else if(restQuest > 0){
							ur.setScoreGroup(restQuest);
							QuestionnaireManagerBase.updateUserScoreGroupOnly(ur);
							result.setErrorCode(ERROR_CODE_REGULATION_USER_RESTRICTED);
							log.debug("User " + user.getId() + " was restricted and try to send mail");
							//UsersManagerBase.sendMailAfterQuestAORegulation(ur);
						} else {
							//Suspend user from Spain if missing CNMF file
							UserRegulationManager.CNMVFileSuspend(user, writerId);
							//Check due documents Limit
							UserRegulationManager.checkUserDueDocumentsDepLimit(ur);							
						}
						if(ur.getPepState() == null || ur.getPepState() == UserRegulationBase.PEP_NON_PROHIBITED){
							Date timeAnswearCreated = QuestionnaireManagerBase.isUserAnswerNoOnPEPQuestion(user.getId());
							if(timeAnswearCreated != null ){
								log.debug("The userId:" + user.getId() + " is answered NO on PEP! Create Issue, Auto Prohibited, Send Emails");
								//TO DO Create IssueAction
								ur.setPepState(UserRegulationBase.PEP_AUTO_PROHIBITED);
								QuestionnaireManagerBase.updateUserPepAnswer(ur);
								// Filling action fields
								String writerUserName = "";
								if(writerId == Writer.WRITER_ID_WEB){
									writerUserName = "WEB";
								} else if (writerId == Writer.WRITER_ID_MOBILE){
									writerUserName = "Mobile";
								} else if (writerId == Writer.WRITER_ID_COPYOP_WEB){
									writerUserName = "Copyop WEB";
								} else if (writerId == Writer.WRITER_ID_COPYOP_MOBILE){
									writerUserName = "Copyop Mobile";
								}	
								IssuesManagerBase.insertUserRegulationPEPRestrictedIssue(user.getId(), writerId, writerUserName, false);
								try {
									log.debug("Try to send support PEP Mail");
									QuestionnaireManagerBase.sendEmailInCaseUserAnswearNoOnPEPQuestion(user, timeAnswearCreated, false);
									if(ur.isQuestionaireIncorrect()){
										log.debug("The user is faild quest. and not will send user Pep Mail: "+ ur);
									}else{
										log.debug("Try to send User Pep Mail");
										//UsersManagerBase.sendMailTemplateFile(ConstantsBase.TEMPLATE_REGULATION_PEP, writerId, user, null, null, null, null);
									}
								} catch (Exception e) {
									log.error("When send PEP mails ", e);
								}
							}else {
								ur.setPepState(UserRegulationBase.PEP_NON_PROHIBITED);
								QuestionnaireManagerBase.updateUserPepAnswer(ur);
							}

						}
					}
					
				} else if (QuestionnaireGroup.REGULATION_OPTIONAL_QUESTIONNAIRE_NAME.equals(questionnaireName)) {
					List<QuestionnaireUserAnswer> nonTickableAnswers = userAnswers.subList(0, userAnswers.size() - 4);
					for (QuestionnaireUserAnswer userAnswer : nonTickableAnswers) {
						if (userAnswer.getAnswerId() == null) {
							log.error("Questionnaire [" + questionnaireName + "] not full - cannot mark it as [DONE]");
							result.setErrorCode(ERROR_CODE_QUESTIONNAIRE_NOT_FULL);
							return result;
						}
					}
					List<QuestionnaireUserAnswer> tickableAnswers = userAnswers.subList(userAnswers.size() - 4, userAnswers.size());
					boolean atLeastOneTicked = false;
					for (QuestionnaireUserAnswer userAnswer : tickableAnswers) {
						if (userAnswer.getAnswerId() != null) {
							atLeastOneTicked = true;
							break;
						}
					}
					if (!atLeastOneTicked) {
						log.error("Questionnaire [" + questionnaireName + "] not full - cannot mark it as [DONE]");
						result.setErrorCode(ERROR_CODE_QUESTIONNAIRE_NOT_FULL);
						return result;
					}

					ur.setOptionalQuestionnaireDone(true);
					ur.setWriterId(Writer.WRITER_ID_MOBILE);
					UserRegulationManager.updateOptionalQuestionnaireStatus(ur);
															
				} else if (QuestionnaireGroup.REGULATION_CAPITAL_QUESTIONNAIRE_NAME.equals(questionnaireName)) {
//					if(!QuestionnaireManagerBase.isAllCapitalAnswFilled(user.getId())){
//						log.error("Questionnaire [" + questionnaireName + "] not full - cannot mark it as [DONE]");
//						result.setErrorCode(ERROR_CODE_QUESTIONNAIRE_NOT_FULL);
//						return result;
//					}
//					long scoreGroup = QuestionnaireManagerBase.updateUserAnswersScoreGroup(user.getId());
//										
//					ur.setApprovedRegulationStep(UserRegulationBase.ET_CAPITAL_MARKET_QUESTIONNAIRE);
//					ur.setWriterId(Writer.WRITER_ID_MOBILE);
//					UserRegulationManager.updateRegulationStep(ur);
//						
//					if(scoreGroup == UserRegulationBase.ET_REGULATION_HIGH_SCORE_GROUP_ID){
//						ur.setApprovedRegulationStep(UserRegulationBase.ET_REGULATION_DONE);
//						ur.setWriterId(Writer.WRITER_ID_MOBILE);
//						UserRegulationManager.updateRegulationStep(ur);
//					} else if(scoreGroup == UserRegulationBase.ET_REGULATION_LOW_SCORE_GROUP_ID){
//						if(UsersManagerBase.sendActivationMailForSuspend(user.getId(), false, user.getSkinId())){							
//							initUser(result, user, utcOffset, writerId);
//							log.debug("Send activation mail after LOW GROUP Score userId" + user.getId());
//						} else {
//							log.error("Can't send Activation Mail For Suspend userId:" + user.getId());
//						}
//					}
					
				} else {
					result.setErrorCode(ERROR_CODE_QUESTIONNAIRE_NOT_SUPPORTED);
					log.error("Questionnaire [" + questionnaireName + "] not supported by JSON service.");
					return result;
				}
			} else {
				log.warn("Questionnaire [" + questionnaireName + "] is not a regulation one");
			}
		UserRegulationManager.getUserRegulation(ur);
		result.setUserRegulation(ur);	
		initUser(result, user, utcOffset, writerId);
    	return result;
    }
    
	public static DeeplinkResult getDeeplink(DeeplinkRequest request) {
		DeeplinkResult result = new DeeplinkResult();
		if (request == null || request.getDpn() == null || request.getDpn().equals("")) {
			result.setErrorCode(ERROR_CODE_INVALID_INPUT);
			return result;
		}

		try {
			DeeplinkParamsToUrl deeplinkParamsToUrl = DeeplinkParamsToUrlManagerBase.getAllCache().get(request.getDpn());
			if (deeplinkParamsToUrl == null || deeplinkParamsToUrl.getCopyopUrl() == null) {// validate if copyop URL exists
				result.setErrorCode(ERROR_CODE_INVALID_INPUT);
				return result;
			}
			result.setDeeplink(deeplinkParamsToUrl);
			result.setErrorCode(ERROR_CODE_SUCCESS);
			return result;
		} catch (SQLException e) {
			log.error("Unable to get deeplinkParamsToUrl with DPN param = " + request.getDpn(), e);
			result.setErrorCode(ERROR_CODE_INVALID_INPUT);
			return result;
		}
	}
	
	public static SkinMethodResult defineCopyopWebSkin(MethodRequest request, HttpServletRequest httpRequest) {
		log.debug("defineCopyopWebSkin: " + request);
		SkinMethodResult result = new SkinMethodResult();
		Calendar calendar = Calendar.getInstance();		
		Skin skin = null;
		com.anyoption.common.beans.base.Currency currency = null;
		try {			
			User user = (User) httpRequest.getSession().getAttribute("user");
			if (user != null && user.getId() > 0) {				
				result.setLoggedIn(true);
				skin = SkinsManagerBase.getSkin(user.getSkinId());
				currency = CurrenciesManagerBase.getCurrency(user.getCurrencyId());
				log.debug("Find user[" + user.getId() + "] in session, return skinId[" + user.getSkinId() + "]" );
			} else {
				boolean isFirtsOpeningWithLocale = false;
				Long skinIdByLocale = null;
				if(!CommonUtil.isParameterEmptyOrNull(request.getLocale())){
					isFirtsOpeningWithLocale = true;
					if (SkinsManagerBase.getSkinIdByLocale().get(request.getLocale().toLowerCase()) != null) {
						skinIdByLocale = (long) SkinsManagerBase.getSkinIdByLocale().get(request.getLocale().toLowerCase()).getId();
						Long defineCountryIdByIp = CommonJSONService.getCountryFromIp(request.getIp(), httpRequest);
						Country country = CountryManagerBase.getById(defineCountryIdByIp);
						if(!country.isRegulated()){
							skinIdByLocale = com.anyoption.common.managers.SkinsManagerBase.getNonRegulatedSkinIdByRegulatedSkinId(skinIdByLocale);
						}
						
					} 
				}
				skin = CommonJSONService.defineSkinAndSetInSession(httpRequest, request, isFirtsOpeningWithLocale, false, skinIdByLocale);
				
				CurrenciesRules currenciesRules = new CurrenciesRules();
				Long defineCountryIdByIp = CommonJSONService.getCountryFromIp(request.getIp(), httpRequest);
				currenciesRules.setCountryId(defineCountryIdByIp == null ? skin.getDefaultCountryId() : defineCountryIdByIp);
				currenciesRules.setSkinId(skin.getId());
				Long defaultCurrencyId = CurrenciesRulesManagerBase.getDefaultCurrencyId(currenciesRules);
				currency = CurrenciesManagerBase.getCurrency(defaultCurrencyId);
			}
		} catch (Exception e) {
			log.error("When define copyopWebSkin ", e);
		}
		
    	if(skin == null){
            log.debug("Can't define skin, get default");
            long skinId = Long.valueOf(Utils.getProperty("skin.default"));
    		skin = SkinsManagerBase.getSkin(skinId);
    	}
    	
    	if(currency == null){
    		log.debug("Can't define currency, get default for Skin:" + skin.getId());
    		currency = CurrenciesManagerBase.getCurrency(skin.getDefaultCurrencyId());
    	}						
		result.setSkinId(new Long(skin.getId()));
		result.setRegulated(skin.isRegulated());
		result.setCurrencyId(currency.getId());
		result.setSkinGroupId(skin.getSkinGroup().getId());
		result.setCurrentTime(System.currentTimeMillis());
		result.setCurrencyLeftSymbol(currency.getIsLeftSymbolBool());
		result.setCurrencySymbol(currency.getSymbol());
		result.setDstOffset(String.valueOf(calendar.get(Calendar.DST_OFFSET)));
		return result;
	}
	
	public static MethodResult saveUserMigration(CrossSaleRequest request, WebLevelsCache wlc, HttpServletRequest httpRequest){
		MethodResult result = new MethodResult();
		if(request == null || wlc == null || httpRequest == null) {
			result.setErrorCode(ERROR_CODE_UNKNOWN);
			log.debug("Cross sale error " );
			return result;
		}
		long start = System.currentTimeMillis();
		HttpSession session = httpRequest.getSession();
		// get user
		User user = (User) session.getAttribute("user");
		UserMigration userMigration = (UserMigration) session.getAttribute(ConstantsBase.BIND_USER_MIGRATION);
		long loginId = 0L;
		try {
			if (session.getAttribute(ConstantsBase.LOGIN_ID) != null) {
				loginId = (long) session.getAttribute(ConstantsBase.LOGIN_ID);
			}
		} catch (Exception e) {
			log.error("Cannot get login ID from session!", e);
			loginId = 0L;
		}
		if(user!= null && userMigration != null) {
			log.debug("Save cross sale " + request.toString()+ " for user " + user.getId() );	
			boolean partial = false;
			
			long amountToTransfer = (long) (request.getAmount() * 100);
			if(amountToTransfer >= 0 && amountToTransfer < 10000) {
				log.error("Invalid amount to transfer(<100): " + amountToTransfer);
				result.setErrorCode(ERROR_CODE_AMOUNT_HIGHER_THAN_ALLOWED);
				return result;
			}
			if( amountToTransfer > user.getBalancePlus() || amountToTransfer < 0 ) {
				log.error("Invalid amount to transfer: " + amountToTransfer);
				result.setErrorCode(ERROR_CODE_INVALID_INPUT);
				return result;
			} 
			if (amountToTransfer == user.getBalancePlus() || amountToTransfer == 0 ){
				partial = false;
			} else {
				partial = true;
			} 
			
			userMigration.setAcceptedMailing(request.isAcceptMail());
			userMigration.setAcceptedTransfer(true);
			userMigration.setFirstName(user.getFirstName());
			userMigration.setLastName(user.getLastName());
			userMigration.setEmail(user.getEmail());
			userMigration.setPhone("+"+user.getCountry().getPhoneCode() + user.getMobilePhone());
			userMigration.setLanguage(LanguagesManagerBase.getLanguage(user.getLanguageId()).getCode());
			userMigration.setCurrency(user.getCurrency().getCode());
			userMigration.setCountry(user.getCountry().getName());
			userMigration.setPartial(partial);
			
			try {
				if(amountToTransfer > 0) {
					ArrayList<Investment> investments = InvestmentsManagerBase.getOpenedInvestmentsByUser(user.getId(), 0, 0, 0);
					for(Investment i:investments) {
						InvestmentsManagerBase.userCancelInvestment(i, Writer.WRITER_ID_WEB, wlc, user, Long.MAX_VALUE, true);
						log.debug("Cross sale user cancel inv " + i.getId());
					}
					
					ArrayList<Transaction> pendingWithdrawals = TransactionsManagerBase.getPendingWithdraws(user.getId());
					TransactionsManagerBase.reverseWithdraws(pendingWithdrawals, user, Writer.WRITER_ID_WEB, user.getIp(), loginId);
					log.debug("Cross sale after pending wd");
					
					if(!partial) {
						// suspend user
						UserRegulationBase ur = new UserRegulationBase();
						ur.setUserId(user.getId());
						UserRegulationManager.getUserRegulation(ur);
						ur.setSuspendedReasonId(UserRegulationBase.SUSPENDED_INV_MIGRATION);
						ur.setComments("Migration IDC");
						ur.setWriterId(Writer.WRITER_ID_WEB);
						UserRegulationManager.updateSuspended(ur);
						
						// create issue for suspend
						UserRegulationManager.insertIssueToUsers(user.getId(), amountToTransfer);
					} else {
						log.debug("Cross sale partial migration - no suspend : "+ user.getId());
					}
					
					// create withdraw
					UserRegulationManager.insertAdminWithdraw(amountToTransfer, user.getId() );
					userMigration.setBalance(amountToTransfer); // the amount the be transferred
				}

				// update DATE_PROCESSED in INVEST_MIGRATION ( so the first popup won't show )		
				UsersManagerBase.updateUserMigration(user.getId());
				
				// save to INVEST_MIGRATION_USERS ( for the job to process it )	
				UsersManagerBase.insertUserMigrationJob(userMigration);

				// send mail
				MailBoxTemplate template = null;
				if(userMigration.getTargetPlatform() == 1) {
					template = UsersManagerBase.getTemplateByTypeSkinLanguage(60, Skin.SKIN_REG_EN, 2);
				} else if( userMigration.getTargetPlatform() == 2) {
					template = UsersManagerBase.getTemplateByTypeSkinLanguage(61, Skin.SKIN_REG_EN, 2);
				}
				String from = CommonUtil.getProperty("email.uname");
				String body = template.getTemplate().replaceAll("%%First_Name%%", user.getFirstName());
				String subject = template.getSubject();
				new Thread(){
					public void run() {
						CommonUtil.sendEmailMsgHtml(from, user.getEmail(), subject, body);
					}
				}.start();
				
				session.removeAttribute(ConstantsBase.BIND_USER_MIGRATION);
				long end = System.currentTimeMillis();
				log.debug("Cross sale time:" + ( end - start) + " ms");
			} catch (SQLException e) {
				log.error("Cross sale problem while processing migration data:" + user.getId(), e);
				result.setErrorCode(ERROR_CODE_UNKNOWN);
				return result;
			}
		}
		result.setErrorCode(ERROR_CODE_SUCCESS);
		return result;
	}
}

