package com.anyoption.common.service;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Enumeration;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class RedirectServlet extends HttpServlet { 
	/**
	 * 
	 */
	private static final long serialVersionUID = 9197744979391919617L;

	private static String redirectServlet;
	private static String redirectServletTo;
	
	@Override
	public void init() {
		redirectServlet = getServletContext().getInitParameter("redirect.servlet.suffix");
		redirectServletTo = getServletContext().getInitParameter("redirect.servlet.to.suffix");
	}
	
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, java.io.IOException {
		super.doGet(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws UnsupportedEncodingException {
		Enumeration<String> paramNames = request.getParameterNames();
		
		StringBuffer site = new StringBuffer(request.getRequestURL().toString().replace(redirectServlet, redirectServletTo));
		site.append("?");
		while(paramNames.hasMoreElements()) {
			String paramName = paramNames.nextElement();
			site.append(paramName).append("=").append(URLEncoder.encode(request.getParameter(paramName), "UTF-8")).append("&");	
		}
		
		response.setStatus(HttpServletResponse.SC_MOVED_TEMPORARILY);
		response.setHeader("Location", site.toString());    
 	}
}
