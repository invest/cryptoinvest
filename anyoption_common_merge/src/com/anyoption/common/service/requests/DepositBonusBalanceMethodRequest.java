package com.anyoption.common.service.requests;


public class DepositBonusBalanceMethodRequest extends UserMethodRequest {

	private long userId;
	private long investmentId;	
	private int balanceCallType;
	private boolean isRequestedFromBE;
	
	public final static int CALL_TOTAL_AMOUNT_BALANCE		= 1;
	public final static int CALL_OPEN_INVESTMENTS_AMOUNT_BALANCE		= 2;
	public final static int CALL_SETTLED_INVESTMENTS_AMOUNT_BALANCE		= 3;
	public final static int CALL_SETTLED_INVESTMENTS_RETURN_BALANCE		= 4;
	
	public long getUserId() {
		return userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}

	public long getInvestmentId() {
		return investmentId;
	}

	public void setInvestmentId(long investmentId) {
		this.investmentId = investmentId;
	}
	

	public int getBalanceCallType() {
		return balanceCallType;
	}

	public void setBalanceCallType(int balanceCallType) {
		this.balanceCallType = balanceCallType;
	}

	
	public boolean isRequestedFromBE() {
		return isRequestedFromBE;
	}

	public void setRequestedFromBE(boolean isRequestedFromBE) {
		this.isRequestedFromBE = isRequestedFromBE;
	}

	public String toString() {
		String ls = System.getProperty("line.separator");
		 return ls + "DepositBonusBalanceMethodRequest: " + ls
				 + super.toString() + ls
				 + "userId: " + userId + ls
				 + "investmentId: " + investmentId + ls
				 + "balanceCallType: " + balanceCallType + ls
				 + "isRequestedFromBE: " + isRequestedFromBE + ls;
	}
}