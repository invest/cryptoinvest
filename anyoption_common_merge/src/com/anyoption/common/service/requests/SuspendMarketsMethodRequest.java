package com.anyoption.common.service.requests;


public class SuspendMarketsMethodRequest {

	private boolean isOn;
	private boolean isAllOn;
	private long marketId;
	
	public boolean isOn() {
		return isOn;
	}
	
	public void setOn(boolean isOn) {
		this.isOn = isOn;
	}
	
	public boolean isAllOn() {
		return isAllOn;
	}
	
	public void setAllOn(boolean isAllOn) {
		this.isAllOn = isAllOn;
	}
	
	public long getMarketId() {
		return marketId;
	}
	
	public void setMarketId(long marketId) {
		this.marketId = marketId;
	}
		
	public String toString() {
		String ls = System.getProperty("line.separator");
		 return ls + "SuspendMarketsMethodRequest: " + ls
				 + super.toString() + ls
				 + "are all markets on: " + isAllOn + ls
				 + "is market on: " + isOn + ls
				 + "market id: " + marketId + ls;
	}
}
