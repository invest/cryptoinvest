package com.anyoption.common.service.requests;

import com.anyoption.common.beans.base.CreditCard;

/**
 *
 * @author KobiM
 *
 */
public class CardMethodRequest extends UserMethodRequest {
	private CreditCard card;

	/**
	 * @return the card
	 */
	public CreditCard getCard() {
		return card;
	}

	/**
	 * @param card the card to set
	 */
	public void setCard(CreditCard card) {
		this.card = card;
	}
	
    public String toString() {
        String ls = System.getProperty("line.separator");
        return ls + "CardMethodRequest: "
            + super.toString()
            + "card: " + card + ls;
    }

}
