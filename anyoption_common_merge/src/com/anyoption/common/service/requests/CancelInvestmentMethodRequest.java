package com.anyoption.common.service.requests;


public class CancelInvestmentMethodRequest extends UserMethodRequest {
	
	long investmentId;
	long opportunityId;
	long amount;
	String pageLevel;
	String oddWin;
	String oddLose;
	int choice;
    boolean fromGraph;
    long loginId;

	public long getInvestmentId() {
		return investmentId;
	}
	public void setInvestmentId(long investmentId) {
		this.investmentId = investmentId;
	}
	public long getAmount() {
		return amount;
	}
	public void setAmount(long amount) {
		this.amount = amount;
	}
	public long getOpportunityId() {
		return opportunityId;
	}
	public void setOpportunityId(long opportunityId) {
		this.opportunityId = opportunityId;
	}
	public int getChoice() {
		return choice;
	}
	public void setChoice(int choice) {
		this.choice = choice;
	}
	public boolean getFromGraph() {
		return fromGraph;
	}
	public void setFromGraph(boolean fromGraph) {
		this.fromGraph = fromGraph;
	}
	public long getLoginId() {
		return loginId;
	}
	public void setLoginId(long loginId) {
		this.loginId = loginId;
	}
	public String getPageLevel() {
		return pageLevel;
	}
	public void setPageLevel(String pageLevel) {
		this.pageLevel = pageLevel;
	}
	public String getOddWin() {
		return oddWin;
	}
	public void setOddWin(String oddWin) {
		this.oddWin = oddWin;
	}
	public String getOddLose() {
		return oddLose;
	}
	public void setOddLose(String oddLose) {
		this.oddLose = oddLose;
	}
	
	@Override
	public String toString() {
		return super.toString() + "CancelInvestmentMethodRequest [investmentId=" + investmentId + ", opportunityId=" + opportunityId
				+ ", amount=" + amount + ", pageLevel=" + pageLevel + ", oddWin=" + oddWin + ", oddLose=" + oddLose
				+ ", choice=" + choice + ", utcOffset=" + utcOffset + ", fromGraph=" + fromGraph + ", loginId="
				+ loginId + "]";
	}
}
