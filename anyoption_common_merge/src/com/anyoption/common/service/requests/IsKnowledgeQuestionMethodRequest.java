package com.anyoption.common.service.requests;


public class IsKnowledgeQuestionMethodRequest extends UserMethodRequest {

	private long userId;
	private boolean isKnowledge;
	
	public long getUserId() {
		return userId;
	}
	public void setUserId(long userId) {
		this.userId = userId;
	}
	public boolean isKnowledge() {
		return isKnowledge;
	}
	public void setKnowledge(boolean isKnowledge) {
		this.isKnowledge = isKnowledge;
	}

	
	public String toString() {
		String ls = System.getProperty("line.separator");
		 return ls + "IsKnowledgeQuestionMethodRequest: " + ls
				 + super.toString() + ls
				 + "userId: " + userId + ls
				 + "isKnowledge: " + isKnowledge + ls;
	}
}