package com.anyoption.common.service.requests;

public class CrossSaleRequest extends MethodRequest {
	boolean acceptMail;
	double amount;

	public boolean isAcceptMail() {
		return acceptMail;
	}

	public void setAcceptMail(boolean acceptMail) {
		this.acceptMail = acceptMail;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	@Override
	public String toString() {
		return "CrossSaleRequest [acceptMail=" + acceptMail + ", amount=" + amount + "]";
	}
	
	
	
}
