package com.anyoption.common.service.requests;

import com.anyoption.common.beans.base.User;

/**
 * set and get the user.
 * 
 * @author liors
 *
 */
public class UpdateUserMethodRequest extends UserMethodRequest {

    protected User user;
    
    protected boolean isRegularReportMail;
    
    public UpdateUserMethodRequest() {
    }

	/**
	 * @return the user
	 */
	public User getUser() {
		return user;
	}

	/**
	 * @param user the user to set
	 */
	public void setUser(User user) {
		this.user = user;
	}
	
    public boolean isRegularReportMail() {
		return isRegularReportMail;
	}

	public void setRegularReportMail(boolean isRegularReportMail) {
		this.isRegularReportMail = isRegularReportMail;
	}

	public String toString() {
        String ls = System.getProperty("line.separator");
        return ls + "UpdateUserMethodRequest: "
            + super.toString()
            + "user: " + user + ls
            + "isRegularReportMail: " + isRegularReportMail + ls;
    }
}