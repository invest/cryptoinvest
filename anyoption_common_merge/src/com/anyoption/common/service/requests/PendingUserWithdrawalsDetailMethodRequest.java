package com.anyoption.common.service.requests;


public class PendingUserWithdrawalsDetailMethodRequest extends UserMethodRequest {

	private long userId;	
	
	public long getUserId() {
		return userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}
}