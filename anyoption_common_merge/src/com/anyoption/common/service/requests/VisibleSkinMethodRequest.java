/**
 * 
 */
package com.anyoption.common.service.requests;

import com.anyoption.common.service.requests.MethodRequest;

public class VisibleSkinMethodRequest extends MethodRequest {
	
	private String uri;

	public String getUri() {
		return uri;
	}

	public void setUri(String uri) {
		this.uri = uri;
	}
	
}
