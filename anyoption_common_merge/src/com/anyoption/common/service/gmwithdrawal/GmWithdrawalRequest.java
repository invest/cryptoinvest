/**
 * 
 */
package com.anyoption.common.service.gmwithdrawal;

import com.anyoption.common.service.requests.MethodRequest;

/**
 * @author pavel.tabakov
 *
 */
public class GmWithdrawalRequest extends MethodRequest {
	
	private String beneficiaryName;
	private String swiftBicCode;// EPS and iDeal only
	private String iban;//EPS and iDeal only
	private long accountNumber;
	private String bankName;
	private long branchNumber;
	private String branchAddress;
	private String amount;
	private long gmType; // EPS value->61, Giropay value->60, Direct24 value->59, iDeal value->63
	private long userId;
	private double rate;//internal
	private long convertedRequestAmount;//internal
	private long allDepositsAmount;//internal
	private String accountInfo;
	private long trnStatusId;//internal
	private String trnFailDesc;//internal
	private Long answerId;
	private String textAnswer;
	private boolean exemptFee;//BE only
	private long allWithdrawalAmount;
	
	public String getBeneficiaryName() {
		return beneficiaryName;
	}
	public void setBeneficiaryName(String beneficiaryName) {
		this.beneficiaryName = beneficiaryName;
	}
	public String getSwiftBicCode() {
		return swiftBicCode;
	}
	public void setSwiftBicCode(String swiftBicCode) {
		this.swiftBicCode = swiftBicCode;
	}
	public String getIban() {
		return iban;
	}
	public void setIban(String iban) {
		this.iban = iban;
	}
	public long getAccountNumber() {
		return accountNumber;
	}
	public void setAccountNumber(long accountNumber) {
		this.accountNumber = accountNumber;
	}
	public String getBankName() {
		return bankName;
	}
	public void setBankName(String bankName) {
		this.bankName = bankName;
	}
	public long getBranchNumber() {
		return branchNumber;
	}
	public void setBranchNumber(long branchNumber) {
		this.branchNumber = branchNumber;
	}
	public String getBranchAddress() {
		return branchAddress;
	}
	public void setBranchAddress(String branchAddress) {
		this.branchAddress = branchAddress;
	}
	public String getAmount() {
		return amount;
	}
	public void setAmount(String amount) {
		this.amount = amount;
	}
	public long getGmType() {
		return gmType;
	}
	public void setGmType(long gmType) {
		this.gmType = gmType;
	}
	
	public long getUserId() {
		return userId;
	}
	public void setUserId(long userId) {
		this.userId = userId;
	}
	
	public double getRate() {
		return rate;
	}
	public void setRate(double rate) {
		this.rate = rate;
	}
	public long getConvertedRequestAmount() {
		return convertedRequestAmount;
	}
	public void setConvertedRequestAmount(long convertedRequestAmount) {
		this.convertedRequestAmount = convertedRequestAmount;
	}
	
	public long getAllDepositsAmount() {
		return allDepositsAmount;
	}
	public void setAllDepositsAmount(long allDepositsAmount) {
		this.allDepositsAmount = allDepositsAmount;
	}
	
	public String getAccountInfo() {
		return accountInfo;
	}
	public void setAccountInfo(String accountInfo) {
		this.accountInfo = accountInfo;
	}
	
	public long getTrnStatusId() {
		return trnStatusId;
	}
	public void setTrnStatusId(long trnStatusId) {
		this.trnStatusId = trnStatusId;
	}
	
	public String getTrnFailDesc() {
		return trnFailDesc;
	}
	public void setTrnFailDesc(String trnFailDesc) {
		this.trnFailDesc = trnFailDesc;
	}
	
	public String getTextAnswer() {
		return textAnswer;
	}
	public void setTextAnswer(String textAnswer) {
		this.textAnswer = textAnswer;
	}
	
	public Long getAnswerId() {
		return answerId;
	}
	public void setAnswerId(Long answerId) {
		this.answerId = answerId;
	}
	
	public boolean isExemptFee() {
		return exemptFee;
	}
	
	public void setExemptFee(boolean exemptFee) {
		this.exemptFee = exemptFee;
	}
	
	public long getAllWithdrawalAmount() {
		return allWithdrawalAmount;
	}
	public void setAllWithdrawalAmount(long allWithdrawalAmount) {
		this.allWithdrawalAmount = allWithdrawalAmount;
	}
	
    public String toString() {
        String ls = System.getProperty("line.separator");
        return ls + "GmWithdrawalRequest: "
            + super.toString()
            + "beneficiaryName: " + beneficiaryName + ls
            + "swiftBicCode: " + swiftBicCode + ls
            + "iban: " + iban + ls
            + "accountNumber: " + accountNumber + ls
            + "bankName: " + bankName + ls
            + "branchNumber: " + branchNumber + ls
            + "branchAddress: " + branchAddress + ls
            + "amount: " + amount + ls
            + "gmType: " + gmType + ls
            + "userId: " + userId + ls
            + "rate: " + rate + ls
            + "convertedRequestAmount: " + convertedRequestAmount + ls
            + "allDepositsAmount: " + allDepositsAmount + ls
            + "accountInfo: " + accountInfo + ls
            + "trnStatusId: " + trnStatusId + ls
            + "trnFailDesc: " + trnFailDesc + ls
            + "textAnswer: " + textAnswer + ls
            + "answerId: " + answerId + ls
            + "exemptFee: " + exemptFee + ls
            + "allWithdrawalAmount: " + allWithdrawalAmount + ls;
    }
}
