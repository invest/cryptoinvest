package com.anyoption.common.service.assetindex;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.base.MarketAssetIndex;
import com.anyoption.common.managers.LanguagesManagerBase;
import com.anyoption.common.managers.MarketAssetIndexManager;
import com.anyoption.common.managers.SkinsManagerBase;
import com.anyoption.common.service.CommonJSONService;
import com.anyoption.common.service.requests.MethodRequest;
import com.anyoption.common.util.CommonUtil;

/**
 * 
 * @author Ivan Petkov
 *
 */

public class AssetIndexServices {
	
	private static final Logger log = Logger.getLogger(AssetIndexServices.class);
	
	public static AssetIndexMarketsResult getAssetIndexMarketsPerSkin(MethodRequest request, HttpServletRequest httpRequest) {
		log.debug("getAssetIndexMarketsPerSkin" + request);
		long beginTime = System.currentTimeMillis();
		AssetIndexMarketsResult result = new AssetIndexMarketsResult();
		
		try {
			Locale locale = new Locale(LanguagesManagerBase.getLanguage(SkinsManagerBase.getSkin(request.getSkinId()).getDefaultLanguageId()).getCode());
			ArrayList<MarketAssetIndex> marketList = new ArrayList<MarketAssetIndex>();
			Map<Long, MarketAssetIndex> marketAssetIndicesBySkin = MarketAssetIndexManager.getMarketAssetIndicesBySkin(request.getSkinId());
			Iterator it = marketAssetIndicesBySkin.entrySet().iterator();
			while (it.hasNext()) {
				marketList.add((MarketAssetIndex)((Map.Entry)it.next()).getValue());
			}
			result.setMarketList(marketList);
			result.setAssetIndexFormulasBySkin(MarketAssetIndexManager.getAssetIndexFormulasBySkin(request.getSkinId()));
			result.setTooltipData(CommonUtil.getMessage(locale, "assetIndex.tooltipdata", null));
			result.setTooltipDataUnderTable(CommonUtil.getMessage(locale, "assetIndex.tooltipdataUnderTable", null));
		} catch (Exception e) {
			log.error("Cannot get asset index markets per skin! " + e);
			result.setErrorCode(CommonJSONService.ERROR_CODE_UNKNOWN);
		}
		
		long endTime = System.currentTimeMillis();
		if (log.isDebugEnabled()) {
			log.debug("TIME: getAssetIndexMarketsPerSkin " + (endTime - beginTime) + "ms");
		}
		return result;
	}
	
	public static AssetIndexMarketsResult getAssetIndexMarketPerSkin(MarketRequest request) {
		log.debug("getAssetIndexMarketPerSkin" + request);
		long beginTime = System.currentTimeMillis();
		AssetIndexMarketsResult result = new AssetIndexMarketsResult();
		
		try {
			ArrayList<MarketAssetIndex> marketList = new ArrayList<MarketAssetIndex>();
			marketList.add(MarketAssetIndexManager.getAssetIndexMarketBySkinAndID(request.getSkinId(), request.getMarketId()));
			result.setMarketList(marketList);
		} catch(SQLException sqle) {
			log.error("Cannot get asset index market per skin! " + sqle);
			result.setErrorCode(CommonJSONService.ERROR_CODE_UNKNOWN);
		}
		
		long endTime = System.currentTimeMillis();
		if (log.isDebugEnabled()) {
			log.debug("TIME: getAssetIndexMarketPerSkin " + (endTime - beginTime) + "ms");
		}
		return result;
	}
}
