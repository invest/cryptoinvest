package com.anyoption.common.service.academyarticles;

import java.util.ArrayList;

import com.anyoption.common.beans.base.AcademyArticle;
import com.anyoption.common.service.requests.MethodRequest;

public class AcademyArticlesMethodRequest extends MethodRequest {

	private String imageUrl;
	private String html;
	private Long homePagePosition;
	private boolean isActive;
	private boolean isDeleted;
	private ArrayList<String> topics;
	private ArrayList<Long> topicIds;
	private AcademyArticle article;
	private String articleLocale;
	private long page;
	private long rowsPerPage;
	private String topicString;

	public String getImage() {
		return imageUrl;
	}

	public void setImage(String image) {
		this.imageUrl = image;
	}

	public String getHtml() {
		return html;
	}

	public void setHtml(String html) {
		this.html = html;
	}

	public Long getHomePagePosition() {
		return homePagePosition;
	}

	public void setHomePagePosition(Long homePagePosition) {
		this.homePagePosition = homePagePosition;
	}

	public boolean isActive() {
		return isActive;
	}

	public void setActive(boolean isActive) {
		this.isActive = isActive;
	}

	public boolean isDeleted() {
		return isDeleted;
	}

	public void setDeleted(boolean isDeleted) {
		this.isDeleted = isDeleted;
	}

	public ArrayList<String> getTopics() {
		return topics;
	}

	public void setTopics(ArrayList<String> topics) {
		this.topics = topics;
	}

	public ArrayList<Long> getTopicIds() {
		return topicIds;
	}

	public void setTopicIds(ArrayList<Long> topicIds) {
		this.topicIds = topicIds;
	}

	public AcademyArticle getArticle() {
		return article;
	}

	public void setArticle(AcademyArticle article) {
		this.article = article;
	}

	public String getArticleLocale() {
		return articleLocale;
	}

	public void setArticleLocale(String articleLocale) {
		this.articleLocale = articleLocale;
	}

	public long getPage() {
		return page;
	}

	public void setPage(long page) {
		this.page = page;
	}

	public long getRowsPerPage() {
		return rowsPerPage;
	}

	public void setRowsPerPage(long rowsPerPage) {
		this.rowsPerPage = rowsPerPage;
	}

	public String getTopicString() {
		return topicString;
	}

	public void setTopicString(String topicString) {
		this.topicString = topicString;
	}	
}
