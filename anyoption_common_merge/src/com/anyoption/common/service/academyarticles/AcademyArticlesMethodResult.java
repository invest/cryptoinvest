package com.anyoption.common.service.academyarticles;

import java.util.ArrayList;

import com.anyoption.common.beans.base.AcademyArticle;
import com.anyoption.common.beans.base.AcademyArticleTopic;
import com.anyoption.common.service.results.MethodResult;

public class AcademyArticlesMethodResult extends MethodResult {

	private AcademyArticle article;
	private ArrayList<AcademyArticleTopic> topics;
	private ArrayList<AcademyArticle> homePageArticles;
	private ArrayList<AcademyArticle> searchedArticles;

	public AcademyArticle getArticle() {
		return article;
	}

	public void setArticle(AcademyArticle article) {
		this.article = article;
	}

	public ArrayList<AcademyArticleTopic> getTopics() {
		return topics;
	}

	public void setTopics(ArrayList<AcademyArticleTopic> topics) {
		this.topics = topics;
	}

	public ArrayList<AcademyArticle> getHomePageArticles() {
		return homePageArticles;
	}

	public void setHomePageArticles(ArrayList<AcademyArticle> homePageArticles) {
		this.homePageArticles = homePageArticles;
	}

	public ArrayList<AcademyArticle> getSearchedArticles() {
		return searchedArticles;
	}

	public void setSearchedArticles(ArrayList<AcademyArticle> searchedArticles) {
		this.searchedArticles = searchedArticles;
	}	
}
