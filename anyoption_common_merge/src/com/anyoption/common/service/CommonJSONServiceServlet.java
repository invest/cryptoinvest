package com.anyoption.common.service;

import java.io.InputStreamReader;
import java.io.OutputStream;
import java.lang.reflect.Method;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.anyoption.common.annotations.PrintLogAnnotations;
import com.anyoption.common.charts.ChartHistoryCache;
import com.anyoption.common.charts.ChartsUpdater;
import com.anyoption.common.charts.LevelHistoryCache;
import com.anyoption.common.jms.WebLevelsCache;
import com.anyoption.common.managers.CurrencyRatesManagerBase;
import com.anyoption.common.service.gmwithdrawal.GmWithdrawalPermissionToDisplayRequest;
import com.anyoption.common.service.gmwithdrawal.GmWithdrawalRequest;
import com.anyoption.common.service.requests.ChartDataRequest;
import com.anyoption.common.service.requests.CrossSaleRequest;
import com.anyoption.common.service.requests.DeeplinkRequest;
import com.anyoption.common.service.requests.EmailValidatorRequest;
import com.anyoption.common.service.requests.MethodRequest;
import com.anyoption.common.util.CommonUtil;
import com.anyoption.common.util.ConstantsBase;
import com.anyoption.common.util.OpportunityCache;
import com.anyoption.common.util.PastExpiriesCache;
import com.anyoption.json.util.JsonExclusionStrategy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/**
 * @author kirilim
 */
public class CommonJSONServiceServlet extends HttpServlet {

	private static final long serialVersionUID = 7576965430775553517L;
	private static final Logger log = Logger.getLogger(CommonJSONServiceServlet.class);
	protected static Map<String, Method> methods;

	protected static WebLevelsCache levelsCache;

	@Override
	@SuppressWarnings("unchecked")
	public void init() {
		log.info("CommonJSONServiceServlet starting...");
		HashMap<String, Object> objectShare = null;
		try {
			Context initCtx = new InitialContext();
			Context envCtx = (Context) initCtx.lookup("java:comp/env");
			objectShare = (HashMap<String, Object>) envCtx.lookup("anyoption/share");
			log.info("Lookedup object share: " + objectShare);
		} catch (Exception e) {
			log.fatal("Cannot lookup object share", e);
		}
		CurrencyRatesManagerBase.loadInvestmentRartesCache();
		methods = new HashMap<>();
		try {
			Class<?> serviceClass = Class.forName("com.anyoption.common.service.CommonJSONService");
			methods.put("getChartDataCommon", serviceClass.getMethod(	"getChartDataCommon",
																		new Class[] {	ChartDataRequest.class, OpportunityCache.class,
																						ChartHistoryCache.class, WebLevelsCache.class,
																						HttpServletRequest.class}));
			methods.put("getMarketGroups", serviceClass.getMethod(	"getMarketGroups", new Class[] {MethodRequest.class, WebLevelsCache.class}));
			methods.put("getAllBinaryMarkets", serviceClass.getMethod("getAllBinaryMarkets", new Class[]{MethodRequest.class}));
			methods.put("getMarketsNames", serviceClass.getMethod(	"getMarketsNames", new Class[] {MethodRequest.class, HttpServletRequest.class}));
			methods.put("validateEmail", serviceClass.getMethod("validateEmail", new Class[]{EmailValidatorRequest.class}));
			methods.put("getSortedCountries", serviceClass.getMethod("getSortedCountries", new Class[] {MethodRequest.class, HttpServletRequest.class}));
			methods.put("getDeeplink", serviceClass.getMethod("getDeeplink", new Class[] {DeeplinkRequest.class}));
			methods.put("defineCopyopWebSkin", serviceClass.getMethod("defineCopyopWebSkin", new Class[] {MethodRequest.class, HttpServletRequest.class}));
			Class<?> serviceClassAssetIndex = Class.forName("com.anyoption.common.service.asset.index.AssetIndex");
			methods.put("getAssetIndexMarketsPerSkin", serviceClassAssetIndex.getMethod("getAssetIndexMarketsPerSkin", new Class[] {MethodRequest.class, HttpServletRequest.class}));
			Class<?> gmWithdrawal = Class.forName("com.anyoption.common.service.gmwithdrawal.GmWithdrawalServices");
			methods.put("getPermissionsToDisplay", gmWithdrawal.getMethod("getPermissionsToDisplay", new Class[] {GmWithdrawalPermissionToDisplayRequest.class, HttpServletRequest.class}));
			methods.put("insertGmWithdraw", gmWithdrawal.getMethod("insertGmWithdraw", new Class[] {GmWithdrawalRequest.class, HttpServletRequest.class}));
			methods.put("saveUserMigration",
					serviceClass.getMethod(	"saveUserMigration",
							new Class[] {CrossSaleRequest.class, WebLevelsCache.class, HttpServletRequest.class}));
		} catch (ClassNotFoundException | NoSuchMethodException | SecurityException e) {
			log.fatal("Cannot load service methods! ", e);
		}
	}

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, java.io.IOException {
		super.doGet(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) {
		String uri = request.getRequestURI();
		String methodReq = uri.substring(uri.lastIndexOf("/") + 1);
		log.debug("URI requested: " + uri + " Method: " + methodReq + " sessionId: " + request.getSession().getId());
		if (log.isTraceEnabled()) {
			Enumeration<String> headerNames = request.getHeaderNames();
			String headerName = null;
			String ls = System.getProperty("line.separator");
			StringBuffer sb = new StringBuffer(ls);
			while (headerNames.hasMoreElements()) {
				headerName = headerNames.nextElement();
				sb.append(headerName).append(": ").append(request.getHeader(headerName)).append(ls);
			}
			log.trace(sb.toString());
		}

		Object result = null;
		try {
			Method m = methods.get(methodReq);
			if (m != null) {
				Gson gson = new GsonBuilder().setExclusionStrategies(new JsonExclusionStrategy()).serializeNulls().create();
				Class<?>[] params = m.getParameterTypes();
				Object[] requestParams = new Object[params.length];
				requestParams[0] = gson.fromJson(new InputStreamReader(request.getInputStream(), "UTF-8"), params[0]);
				for (int i = 1; i < params.length; i++) {			
					if (params[i] == HttpServletRequest.class) {
						requestParams[i] = request;
					}
				}
				if (requestParams[0] instanceof MethodRequest) {
					((MethodRequest) requestParams[0]).setIp(CommonUtil.getIPAddress(request));
				}
				result = m.invoke(null, requestParams);
				
				String jsonResponse = gson.toJson(result);
				if (null != jsonResponse && jsonResponse.length() > 0) {					
					if(!stopPrintDebugLog(m)){
						log.debug(jsonResponse);
					}
					byte[] data = jsonResponse.getBytes("UTF-8");
					OutputStream os = response.getOutputStream();
					response.setHeader("Content-Type", "application/json");
					response.setHeader("Content-Length", String.valueOf(data.length));
					os.write(data, 0, data.length);
					os.flush();
					os.close();
				}
			} else {
				log.warn("Method: " + methodReq + " not found!");
			}
		} catch (Exception e) {
			log.error("Problem executing " + methodReq + " Method! ", e);
		}
	}	
	
	private boolean stopPrintDebugLog(Method method) {
		boolean res = false;
		PrintLogAnnotations annotation = method.getAnnotation(PrintLogAnnotations.class);
		if (annotation != null) {			
			res = annotation.stopPrintDebugLog();
		}
		return res;
	}	

	public void destroy() {
		methods = null;
	}

	public static WebLevelsCache getLevelsCache() {
		return null;
	}
}