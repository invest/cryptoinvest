package com.anyoption.common.server.pixel;

import java.sql.SQLException;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.Transaction;
import com.anyoption.common.managers.ServerPixelsManagerBase;
import com.anyoption.common.managers.TransactionsManagerBase;


public class ServerPixelHandlerDepositWithinXdays extends ServerPixelHandler {
	public static final Logger log = Logger.getLogger(ServerPixelHandlerDepositWithinXdays.class);
    
	private int numberOfDays;
	
    public ServerPixelHandlerDepositWithinXdays() {
	}

	@Override
	public boolean checkCondition() {
		try {
			return ServerPixelsManagerBase.isHasOpenAppPixel(serverSidePixelEvent.getPublisherId(), serverSidePixelEvent.getDuid(), numberOfDays);
		} catch (SQLException e) {
			log.debug("cant check isHasOpenAppPixel ", e);
		}
		return false;
	}


	@Override
	public void prepareInsertPixelToDB(String pixel) {
		insertPixelToDB(serverSidePixelEvent.getTransactionId(), pixel);
	}
	
	@Override
	public String prepareReplaceParams() {
		try {
			Transaction t = TransactionsManagerBase.getTransactionInfo(serverSidePixelEvent.getTransactionId());
			serverSidePixelEvent.setTransactionBaseAmount(String.valueOf(t.getBaseAmount()/100));
		} catch (SQLException e) {
			log.debug("cant get transaction base amount for transaction id " + serverSidePixelEvent.getTransactionId(), e);
		}
    	return replaceParams();
    }


	public int getNumberOfDays() {
		return numberOfDays;
	}


	public void setNumberOfDays(int numberOfDays) {
		this.numberOfDays = numberOfDays;
	}    
}