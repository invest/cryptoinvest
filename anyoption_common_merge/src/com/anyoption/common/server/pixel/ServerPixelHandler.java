package com.anyoption.common.server.pixel;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.ServerPixel;
import com.anyoption.common.beans.ServerSidePixelEvent;
import com.anyoption.common.managers.ServerPixelsManagerBase;
import com.anyoption.common.util.CommonUtil;

public abstract class ServerPixelHandler {
	
    public static final Logger log = Logger.getLogger(ServerPixelHandler.class);
        
    public final static String PARAM_DUID = "$duid";
    public final static String PARAM_PUBLISHER_DUID = "$publisherDuid";
    public final static String PARAM_BASE_TRANSACTION_AMOUNT = "$transactionBaseAmount";
    public final static String PARAM_ORDER_ID = "$orderId";
    
    protected ServerSidePixelEvent serverSidePixelEvent;

    public ServerPixelHandler() {
		super();
	}
    
    /**
     * if u need new parameter take override it in the handler and add it
     */
    public String prepareReplaceParams() {
    	return replaceParams();
    }
    
    public String replaceParams() {
    	String pixel = serverSidePixelEvent.getPixelUrl();
    	if (serverSidePixelEvent.getDuid() != null) {
    		pixel = pixel.replace(PARAM_DUID, serverSidePixelEvent.getDuid());
    	}
    	if (serverSidePixelEvent.getPublisherUID() != null) {
    		pixel = pixel.replace(PARAM_PUBLISHER_DUID, serverSidePixelEvent.getPublisherUID());
    	}
    	if (serverSidePixelEvent.getTransactionBaseAmount() != null) {
    		pixel = pixel.replace(PARAM_BASE_TRANSACTION_AMOUNT, serverSidePixelEvent.getTransactionBaseAmount());
    	}
    	if (serverSidePixelEvent.getOrderId() != null) {
    		pixel = pixel.replace(PARAM_ORDER_ID, serverSidePixelEvent.getOrderId());
    	}
    	return pixel;
    }
    
    public abstract boolean checkCondition();
    
	public void run() {
        try {
			if (checkCondition()) {
				String pixel = prepareReplaceParams(); //and replace;
				if (CommonUtil.hitPage(pixel)) {
					prepareInsertPixelToDB(pixel);//and insert;
				}
			}
		} catch (Exception e) {
			log.error("cant send appsflyer event", e);
		}
    }
	
	
	public abstract void prepareInsertPixelToDB(String pixel);
	
	public void insertPixelToDB(long refrenceId, String pixel) {
		try {
			ServerPixelsManagerBase.insert(new ServerPixel(serverSidePixelEvent.getPixelTypeId(), refrenceId, serverSidePixelEvent.getPublisherId(), pixel));
		} catch (Exception e) {
			log.error("cant insert to pixels server", e);
		}
	}

	public ServerSidePixelEvent getServerSidePixelEvent() {
		return serverSidePixelEvent;
	}


	public void setServerSidePixelEvent(ServerSidePixelEvent serverSidePixelEvent) {
		this.serverSidePixelEvent = serverSidePixelEvent;
	}    
}