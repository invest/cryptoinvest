/**
 * 
 */
package com.anyoption.common.managers;

import java.sql.Connection;
import java.sql.SQLException;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.InvestmentRejects;
import com.anyoption.common.daos.InvestmentRejectsDAOBase;

/**
 * @author pavelhe
 *
 */
public class InvestmentRejectsManagerBase extends BaseBLManager {
	protected static Logger log = Logger.getLogger(InvestmentRejectsManagerBase.class);
	
	public static void insertInvestmentReject(InvestmentRejects inv) {
        Connection conn = null;
        try {
            conn = getConnection();
            InvestmentRejectsDAOBase.insertInvestmentRejects(conn, inv);
        } catch (SQLException e) {
        	log.debug("can't insert Investment Rejects into db " , e);
        } finally {
            closeConnection(conn);
        }
	}
}
