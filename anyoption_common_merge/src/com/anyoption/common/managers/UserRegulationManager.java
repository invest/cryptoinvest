package com.anyoption.common.managers;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.faces.model.SelectItem;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import com.anyoption.common.beans.ActivationMail;
import com.anyoption.common.beans.FileType;
import com.anyoption.common.beans.Issue;
import com.anyoption.common.beans.IssueAction;
import com.anyoption.common.beans.Transaction;
import com.anyoption.common.beans.User;
import com.anyoption.common.beans.UserMigration;
import com.anyoption.common.beans.UsersRegulationDueDocumentsDepositLimit;
import com.anyoption.common.beans.Writer;
import com.anyoption.common.beans.base.Skin;
import com.anyoption.common.beans.base.UserRegulationActivationMail;
import com.anyoption.common.beans.base.UserRegulationBase;
import com.anyoption.common.daos.GeneralDAO;
import com.anyoption.common.daos.IssuesDAOBase;
import com.anyoption.common.daos.TransactionsDAOBase;
import com.anyoption.common.daos.UsersDAOBase;
import com.anyoption.common.daos.UsersRegulationDAOBase;
import com.anyoption.common.service.CommonJSONService;
import com.anyoption.common.service.userdocuments.UserDocumentsManager;
import com.anyoption.common.util.CommonUtil;
import com.anyoption.common.util.ConstantsBase;
public class UserRegulationManager extends BaseBLManager {
	
	private static final Logger log = Logger.getLogger(UserRegulationManager.class);
	public static final int SUSPEND_ID_DEFAULT_VALUE = -1;		
    public static void updateRegulationStep(UserRegulationBase ur) throws SQLException {
    	
    	Connection conn = null;
        try {
        	conn = getConnection();
        	UsersRegulationDAOBase.updateRegulationStep(conn, ur); 
        	//after control approved
        	if(ur.getApprovedRegulationStep() == UserRegulationBase.REGULATION_CONTROL_APPROVED_USER){
        		UsersRegulationDAOBase.unsuspendUserAfterControlApproved(conn, ur.getUserId());       
        		UsersRegulationDAOBase.setControlApprovedDate(conn, ur.getUserId());
        	}
        	if(ur.getApprovedRegulationStep() == UserRegulationBase.REGULATION_MANDATORY_QUESTIONNAIRE_DONE){
        		UsersRegulationDAOBase.setQuestionnaireDoneDate(conn, ur.getUserId());
        	}
        } finally {
            closeConnection(conn);
        }
    }
    
    public static void updateSuspended(UserRegulationBase ur) throws SQLException {
    	
    	Connection conn = null;
        try {
        	conn = getConnection();
        	UsersRegulationDAOBase.updateSuspended(conn, ur);           
        } finally {
            closeConnection(conn);
        }
    }
    
    public static void updateOptionalQuestionnaireStatus(UserRegulationBase ur) throws SQLException {
    	
    	Connection conn = null;
        try {
        	conn = getConnection();
        	UsersRegulationDAOBase.updateOptionalQuestionnaireStatus(conn, ur);           
        } finally {
            closeConnection(conn);
        }
    }
    
    public static void updateFilledPersonalInformation(UserRegulationBase ur) throws SQLException {    	
    	Connection conn = null;
        try {
        	conn = getConnection();
        	UsersRegulationDAOBase.updateFilledPersonalInformation(conn, ur);           
        } finally {
            closeConnection(conn);
        }
    }
    
    public static void getUserRegulation(UserRegulationBase userRegulation) throws SQLException {
    	Connection conn = null;
        try {
        	conn = getConnection();
        	conn.setAutoCommit(false);
    		UsersRegulationDAOBase.getUserRegulation(conn, userRegulation);
    		conn.commit();
        } catch(SQLException e) {
        	conn.rollback();
        	log.error("Unable to load user regulation", e);
        	throw e;
    	} finally {
    		conn.setAutoCommit(true);
    		closeConnection(conn);
    	}
    }
    
    public static void getUserRegulationBackend(UserRegulationBase userRegulation) throws SQLException {
    	Connection conn = null;
        try {
        	conn = getConnection();
        	conn.setAutoCommit(false);
    		UsersRegulationDAOBase.getUserRegulationBackend(conn, userRegulation);
    		conn.commit();
        } catch(SQLException e) {
        	conn.rollback();
        	log.error("Unable to load user regulation", e);
        	throw e;
    	} finally {
    		conn.setAutoCommit(true);
    		closeConnection(conn);
    	}
    }
    
    public static ArrayList<SelectItem> getRegulationStepsList(long userId, long writerId) throws SQLException{
    	Connection con = null;
    	try {
    		con = getConnection();
    		return UsersRegulationDAOBase.getRegulationStepsList(con, userId,writerId);
    	}finally {
    		closeConnection(con);
    	}
    }
    
    public static int updateRegulationStepWithCheck(UserRegulationBase ur) throws SQLException {
    	
    	Connection conn = null;
        try {
        	conn = getConnection();
        return	UsersRegulationDAOBase.updateRegulationStepWithCheck(conn, ur);           
        } finally {
            closeConnection(conn);
        }
    }
    
    public static boolean isNeedAdditionalInfo(User u) {
    	    	
    	if(u.getSkinId() == Skin.SKIN_ETRADER  
	    	&& ((u.getIdNum().contains(ConstantsBase.ID_NUM_DEFAULT_VALUE)) 
	    		|| u.getTimeBirthDate() == null
	    		|| CommonUtil.isParameterEmptyOrNull(u.getCityName())
	    		|| CommonUtil.isParameterEmptyOrNull(u.getStreet())
	    		|| CommonUtil.isParameterEmptyOrNull(u.getStreetNo())
	    		|| CommonUtil.isParameterEmptyOrNull(u.getZipCode()))){
    		log.debug("Need additiona info for user: " + u.getId());
    		return true;    		
    	}else{
    		return false;
    	}
    }
    
    public static boolean isNeedShowBinaryOptionsKnowledge(UserRegulationBase userRegulation, long userSkinId) {    	    	
    	if(userSkinId == Skin.SKIN_ETRADER && userRegulation.getApprovedRegulationStep() != null
    			&& (userRegulation.getApprovedRegulationStep() == userRegulation.REGULATION_FIRST_DEPOSIT 
    				||
    				(userRegulation.getApprovedRegulationStep() == userRegulation.ET_REGULATION_KNOWLEDGE_QUESTION_USER && !userRegulation.isKnowledgeQuestion()))){
    		return true;    		
    	}else{
    		return false;
    	}
    }
    
    public static boolean isNeedShowCapitalQuestionnaire(UserRegulationBase userRegulation, long userSkinId) {    	    	
    	if(userSkinId == Skin.SKIN_ETRADER && userRegulation.getApprovedRegulationStep() != null
    			&& userRegulation.getApprovedRegulationStep() == userRegulation.ET_REGULATION_KNOWLEDGE_QUESTION_USER 
    			&& userRegulation.isKnowledgeQuestion()){
    		return true;    		
    	}else{
    		return false;
    	}
    }
    
    public static boolean isSuspendAfterCapitalQuestionnaire(UserRegulationBase userRegulation, long userSkinId) {    	    	
    	if(userSkinId == Skin.SKIN_ETRADER && userRegulation.getApprovedRegulationStep() != null
    			&& userRegulation.getApprovedRegulationStep() == userRegulation.ET_CAPITAL_MARKET_QUESTIONNAIRE 
    			&& userRegulation.getSuspendedReasonId() == UserRegulationBase.ET_SUSPEND_LOW_SCORE_GROUP){
    		return true;    		
    	}else{
    		return false;
    	}
    }
    
    public static boolean isSuspendTresholdLowGroup(UserRegulationBase userRegulation, long userSkinId) {    	    	
    	if(userSkinId == Skin.SKIN_ETRADER && userRegulation.getApprovedRegulationStep() != null
    			&& userRegulation.getApprovedRegulationStep() == userRegulation.ET_CAPITAL_MARKET_QUESTIONNAIRE 
    			&& userRegulation.getSuspendedReasonId() == UserRegulationBase.ET_SUSPEND_TRESHEHOLD_LOW_SCORE_GROUP){
    		return true;    		
    	}else{
    		return false;
    	}
    }
    	
    public static boolean isSuspendTresholdMediumGroup(UserRegulationBase userRegulation, long userSkinId) {    	    	
        	if(userSkinId == Skin.SKIN_ETRADER && userRegulation.getApprovedRegulationStep() != null
        			&& userRegulation.getApprovedRegulationStep() == userRegulation.ET_CAPITAL_MARKET_QUESTIONNAIRE 
        			&& userRegulation.isSuspended()
        			&& userRegulation.getSuspendedReasonId() == UserRegulationBase.ET_SUSPEND_TRESHEHOLD_MEDIUM_SCORE_GROUP){
        		return true;    		
        	}else{
        		return false;
        	}
    }
    
    public static boolean isRestrictedAO(UserRegulationBase userRegulation, long userSkinId) {    	    	
    	if(userSkinId != Skin.SKIN_ETRADER && userRegulation.getApprovedRegulationStep() != null
    			&& userRegulation.isRestricted()){
    		return true;    		
    	}else{
    		return false;
    	}
    }
    
	public static int canWithdraw(long userId) throws SQLException {
		int errCode = CommonJSONService.ERROR_CODE_SUCCESS;
		User user = UsersManagerBase.getUserById(userId);
		
		UserRegulationBase ur = new UserRegulationBase();
		ur.setUserId(userId);	
		UserRegulationManager.getUserRegulation(ur);
		
		if(ur.getApprovedRegulationStep() != null){
			if (UserRegulationBase.REGULATION_FIRST_DEPOSIT == ur.getApprovedRegulationStep() 
					&& (ur.isQualified() || ur.getRegulationVersion() == UserRegulationBase.REGULATION_USER_VERSION)){
				log.debug("Can't make Withdraw! Regulation missing questionnaire " + ur);
				errCode = CommonJSONService.ERROR_CODE_REGULATION_MISSING_QUESTIONNAIRE;
			} else if(UserRegulationManager.isRestrictedAO(ur, user.getSkinId())) {
				errCode = CommonJSONService.ERROR_CODE_REGULATION_USER_RESTRICTED;
				log.debug("Can't make Withdraw! Regulation users is restricted " + ur);
			}
		}
		return errCode;
	}
	
	public static boolean isCanWithdraw(long userId) throws SQLException {
		return canWithdraw(userId) == CommonJSONService.ERROR_CODE_SUCCESS;
	}
	
    public static boolean isPepProhibited(UserRegulationBase userRegulation, long userSkinId) {    	    	
    	if(userSkinId != Skin.SKIN_ETRADER && userRegulation.getApprovedRegulationStep() != null
    			&& (userRegulation.getPepState() == UserRegulationBase.PEP_AUTO_PROHIBITED 
    					|| userRegulation.getPepState() == UserRegulationBase.PEP_WRITER_PROHIBITED)){
    		return true;    		
    	}else{
    		return false;
    	}
}
    
    public static void updateRegularReportMail(UserRegulationBase ur) throws SQLException {
    	
    	Connection conn = null;
        try {
        	conn = getConnection();
        	UsersRegulationDAOBase.updateRegularReportMail(conn, ur);           
        } finally {
            closeConnection(conn);
        }
    }
    
	public static void insertHashKey(long userId, String hashKey, long hashType) throws SQLException {
		Connection conn = null;
		try {
			conn = getConnection();
			UsersRegulationDAOBase.insertHashKey(conn, userId, hashKey, hashType);
		} finally {
			closeConnection(conn);
		}
	}
	
	public static Issue getSuspendedIssueId(long userId) throws SQLException {
		Connection conn = null;
		Issue issue = null;
		try {
			conn = getConnection();
			issue = UsersRegulationDAOBase.getSuspendedIssueId(conn, userId);
		} finally {
			closeConnection(conn);
		}
		return issue;
	}
    
    public static ArrayList<UserRegulationActivationMail> getUserRegulationActivationMail() throws SQLException {    	
    	Connection conn = null;
        try {
        	conn = getConnection();
        	return UsersRegulationDAOBase.getUserRegulationActivationMail(conn);           
        } finally {
            closeConnection(conn);
        }
    }
    
    public static void updateUserRegulationActivationMail(List<UserRegulationActivationMail> activationMail) throws SQLException {    	
    	Connection conn = null;
        try {
        	conn = getConnection();
        	UsersRegulationDAOBase.updateUserRegulationActivationMail(conn, activationMail);           
        } finally {
            closeConnection(conn);
        }
    }
    
    public static void updateErrorUserRegulationActivationMail(List<UserRegulationActivationMail> activationMail) throws SQLException {    	
    	Connection conn = null;
        try {
        	conn = getConnection();
        	UsersRegulationDAOBase.updateErrorUserRegulationActivationMail(conn, activationMail);           
        } finally {
            closeConnection(conn);
        }
    }
    
    public static Date getTimeWhenQuestionnaireFinallySubmited(long userId) throws SQLException {    	
    	Connection conn = null;
        try {
        	conn = getConnection();
        	return UsersRegulationDAOBase.getTimeWhenQuestionnaireFinallySubmited(conn, userId, UserRegulationBase.ET_CAPITAL_MARKET_QUESTIONNAIRE);           
        } finally {
            closeConnection(conn);
        }
    }
    
    public static void updateRegulationStepKnowledgeQuestion(UserRegulationBase ur) throws SQLException {    	
    	Connection conn = null;
        try {
        	conn = getConnection();
        	UsersRegulationDAOBase.updateRegulationStepKnowledgeQuestion(conn, ur);           
        } finally {
            closeConnection(conn);
        }
    }
    
    public static void updateIsKnowledgeQuestion(long userId, boolean isKnowledgeQuestion, long writerId) throws SQLException {    	
    	Connection conn = null;
        try {
        	conn = getConnection();
        	UsersRegulationDAOBase.updateIsKnowledgeQuestion(conn, userId, isKnowledgeQuestion, writerId);         	
        } finally {
            closeConnection(conn);
        }
    	if(isKnowledgeQuestion){
    		//Suspend user from Spain if missing CNMF file
    		User u = UsersManagerBase.getUserById(userId);        		
    		CNMVFileSuspend(u, u.getWriterId());
        	//AO Check Deposit Limits Due Documents
    		UserRegulationBase ur = new UserRegulationBase();
    		ur.setUserId(userId);
    		getUserRegulation(ur);
    		checkUserDueDocumentsDepLimit(ur);        		        		
    	}
    }
    
    public static void unsuspendUserViaActivationLink(String hashKeyParam) throws SQLException {
		
		long userId = 0;
		long suspendedId;
		Connection conn = null;
		boolean isUnsuspend = false;
		
		try {
			conn = getConnection();
			conn.setAutoCommit(false);
			
			userId = UsersRegulationDAOBase.getUserIdByHashKey(conn, hashKeyParam, UserRegulationBase.HASH_TYPE_ET_UNSUSPEND);

			if (userId == 0) {
				log.debug("Hash key does not exist in DB!");
			} else {
				suspendedId = UsersRegulationDAOBase.getRegulationUserSuspendedId(conn, userId);
				
				if (suspendedId != UserRegulationBase.NOT_SUSPENDED && suspendedId != SUSPEND_ID_DEFAULT_VALUE) {
					
					Issue issue = UsersRegulationDAOBase.getSuspendedIssueId(conn, userId);
					if (issue != null) {
						isUnsuspend = IssuesDAOBase.insertLinkIssueAction(conn, issue, userId, "User account was activated", String.valueOf(IssuesManagerBase.ISSUE_ACTION_TYPE_REGULATION_ACTIVATION), String.valueOf(IssuesManagerBase.ISSUE_CHANNEL_ACTIVIY), suspendedId, String.valueOf(IssuesManagerBase.ISSUE_PRIORITY_LOW));
					}
				}
			}
			
			if (isUnsuspend) {
				conn.commit();
			} else {
				conn.rollback();
			}
				
			
		} catch (SQLException ex) {
			log.error("Unable to unsuspend user with id: " + userId, ex);
			conn.rollback();
		} finally {
			if (conn != null) {
				try {
					conn.setAutoCommit(true);
				} catch (SQLException e) {
					log.error("Unable to turn on Auto Commit for connection: "+ conn, e);
				}
			}
			closeConnection(conn);
		}
	}
    
    public static ActivationMail getUserHashTypeAndIdByHashKey(String hashKey) throws SQLException {
    	Connection conn = null;
    	ActivationMail activation = null;
        try {
        	conn = getConnection();
        	activation = UsersRegulationDAOBase.getUserHashTypeAndIdByHashKey(conn, hashKey);
        	return activation;
        }  finally {
			if (conn != null) {
				try {
					conn.setAutoCommit(true);
				} catch (SQLException e) {
					log.error("Unable to turn on Auto Commit for connection: "+ conn, e);
				}
			}
			closeConnection(conn);
		}
    }
    
    public static long getUserIdByHashKey(String hashKey, long hashType) throws SQLException {
    	Connection conn = null;
    	long userId = 0L;
        try {
        	conn = getConnection();
        	userId = UsersRegulationDAOBase.getUserIdByHashKey(conn, hashKey, hashType);
        	return userId;
        }  finally {
			if (conn != null) {
				try {
					conn.setAutoCommit(true);
				} catch (SQLException e) {
					log.error("Unable to turn on Auto Commit for connection: "+ conn, e);
				}
			}
			closeConnection(conn);
		}
    }
    
    public static ArrayList<Issue> getOpenedPendingDocsIssuesByUserId(long userId) {
    	Connection con = null;
    	try {
    		con = getConnection();
    		return UsersRegulationDAOBase.getOpenedPendingDocsIssuesByUserId(con, userId);
    	} catch (SQLException e) {
			log.error("Unable to get issues for user.", e);
			return null;
		} finally {
    		closeConnection(con);
    	}
    }
    
    public static void checkUserDueDocumentsDepLimit(UserRegulationBase ur) throws SQLException {    	
    	UsersRegulationDueDocumentsDepositLimit res = new UsersRegulationDueDocumentsDepositLimit();
    	log.debug("checkUserDueDocumentsDepLimit for userRegaultion:" + ur);
    		if(ur.getApprovedRegulationStep() != null && ur.getApprovedRegulationStep() >= UserRegulationBase.REGULATION_MANDATORY_QUESTIONNAIRE_DONE 
    				&& ur.getApprovedRegulationStep() < UserRegulationBase.REGULATION_CONTROL_APPROVED_USER
    					&& !ur.isSuspendedDueDocuments()
    						&& !ur.isSuspendedCNMVDocuments()
    							&& !ur.isRestricted()){    			    			
            	Connection conn = null;
                try {
                	conn = getConnection();
                	res = UsersRegulationDAOBase.getUserDueDocumentsDepLimit(conn, ur.getUserId());
                    
                	log.debug("AO Regulation Check UserDueDocuments Deposit Limit: " + res);
                    if(res.getId() == UsersRegulationDueDocumentsDepositLimit.SUSPEND_LIMIT){
    					ur.setSuspendedReasonId(UserRegulationBase.SUSPENDED_DUE_DOCUMENTS);
    					ur.setComments("User suspended due to documents, the deposits are :" + res.getDepositSum() + " and limit:" + res.getDepositLimit());
    					ur.setQualifiedAmount(res.getDepositLimit());
                    	ur.setQualified(true);
    					updateSuspended(ur);					
    					UsersRegulationDAOBase.updateQualified(conn, ur);    					
                    	log.debug("User suspended due to documents, the deposits are :" + res.getDepositSum() + " and limit:" + res.getDepositLimit());
                    	UsersRegulationDAOBase.insertSentMailAfterLimitDeposit(conn, ur.getUserId(), ur.getSuspendedReasonId());
                    } else if(res.getId() == UsersRegulationDueDocumentsDepositLimit.WARNING_LIMIT && !ur.showWarningDueDocuments()){
                    	ur.setQualifiedAmount(res.getDepositLimit());
                    	ur.setQualified(false);
                    	UsersRegulationDAOBase.updateQualified(conn, ur);
                    	log.debug("User must be Show popUp for due to documents, the deposits are :" + res.getDepositSum() + " and limit:" + res.getDepositLimit());
                    	UsersRegulationDAOBase.insertSentMailAfterLimitDeposit(conn, ur.getUserId(), 0);
                    }
                } finally {
                    closeConnection(conn);
                }                
    		}
    }
    
    public static void CNMVFileSuspend(User u, long writerId) throws SQLException {
    	UserRegulationBase ur = new UserRegulationBase();
    	ur.setUserId(u.getId());
    	UserRegulationManager.getUserRegulation(ur);
    	ur.setWriterId(writerId);
    	log.debug("CNMVFile Suspend for userRegaultion:" + ur);
    		if(u.getCountryId() == ConstantsBase.COUNTRY_ID_SPAIN && !UserDocumentsManager.isExistFileType(u.getId(), FileType.CNMV_FILE, 0l)){
    			ur.setWriterId(writerId);
    			ur.setSuspendedReasonId(UserRegulationBase.SUSPENDED_CNMV_RESTRICTED);
    			ur.setComments("User from Spain and suspended due to CNMV File documents");
    			updateSuspended(ur);    
    			IssuesManagerBase.insertUserRegulationCNMVIssue(u.getId(), writerId, false);
    			log.debug("AO Regulation Suspend CNMV user from Spain and missing CNMV File: " + ur);
    		}
    }
    
    public static void CNMVFileUnSuspend(User u, long writerId) throws SQLException {    	    	
    	UserRegulationBase ur = new UserRegulationBase();
    	ur.setUserId(u.getId());
    	UserRegulationManager.getUserRegulation(ur);
    	ur.setWriterId(writerId);
    	log.debug("Try CNMVFile UnSuspend for userRegulation:" + ur);
    		if(u.getCountryId() == ConstantsBase.COUNTRY_ID_SPAIN 
    				&& ur.getSuspendedReasonId() == UserRegulationBase.SUSPENDED_CNMV_RESTRICTED
    					&& UserDocumentsManager.isExistFileType(u.getId(), FileType.CNMV_FILE, 0l)){    			
    			ur.setWriterId(writerId);
    			ur.setSuspendedReasonId(UserRegulationBase.NOT_SUSPENDED);
    			ur.setComments("User from Spain was UnSuspended after upload CNMV File documents");
    			updateSuspended(ur);    
    			IssuesManagerBase.insertUserRegulationCNMVIssue(u.getId(), writerId, true);
    			log.debug("AO Regulation UnSuspend CNMV user from Spain" + ur);
    		} else {
    			log.debug("Can't CNMVFile UnSuspend for userRegulation:" + ur);
    		}
    }
    
	public static HashMap<String, String> getAORegulationState(UserRegulationBase userRegulation, long skinId) {
		HashMap<String, String> hmReturnMsg = null;
		if(userRegulation.getSuspendedReasonId() == UserRegulationBase.SUSPENDED_INV_MIGRATION) {
			UserMigration userMigration  = new UserMigration();
			try {
				UsersManagerBase.getUserMigration(userRegulation.getUserId(), userMigration, true);
			} catch (SQLException e) {
				log.error("Unable to load user migration", e);
			}

			if(userMigration.getTargetPlatform() == 1 ) {
				hmReturnMsg = new HashMap<String, String>();
				hmReturnMsg.put("CODE_ID",  CommonJSONService.ERROR_CODE_SUSPENDED_MIGRATE_INVEST + "");
				hmReturnMsg.put("ERROR_MSG", "regulation.user.migration");
			} else if ( userMigration.getTargetPlatform() == 2 ) {
				hmReturnMsg = new HashMap<String, String>();
				hmReturnMsg.put("CODE_ID",  CommonJSONService.ERROR_CODE_SUSPENDED_MIGRATE_WEBTRADE + "");
				hmReturnMsg.put("ERROR_MSG", "regulation.user.migration.webtrade");
			}

		} else if (UserRegulationBase.REGULATION_FIRST_DEPOSIT == userRegulation.getApprovedRegulationStep() 
				&& (userRegulation.isQualified() || userRegulation.getRegulationVersion() == UserRegulationBase.REGULATION_USER_VERSION)) {			
			hmReturnMsg = new HashMap<String, String>();
			hmReturnMsg.put("CODE_ID", CommonJSONService.ERROR_CODE_REGULATION_MISSING_QUESTIONNAIRE + "");
			hmReturnMsg.put("ERROR_MSG", "regulation.missing.questionnaire");
			log.debug("The questionnaire should be filled before further trading or deposit. Error code: ERROR_CODE_REGULATION_MISSING_QUESTIONNAIRE: " + userRegulation );						
		} else if(UserRegulationManager.isRestrictedAO(userRegulation, skinId)){
			hmReturnMsg = new HashMap<String, String>();
			hmReturnMsg.put("CODE_ID", CommonJSONService.ERROR_CODE_REGULATION_USER_RESTRICTED + "");
			hmReturnMsg.put("ERROR_MSG", "regulation.user.restricted");
			log.debug("The user isRestricted. Error code: ERROR_CODE_REGULATION_MISSING_QUESTIONNAIRE: " + userRegulation);
		}else if(userRegulation.isQuestionaireIncorrect()){
			hmReturnMsg = new HashMap<String, String>();
			hmReturnMsg.put("CODE_ID", CommonJSONService.ERROR_CODE_REG_SUSPENDED_QUEST_INCORRECT + "");
			hmReturnMsg.put("ERROR_MSG", "regulation.incorrect.fill.questionnaire");
			log.debug("The user isQuestionaireIncorrect. Error code: ERROR_CODE_REG_SUSPENDED_QUEST_INCORRECT: " + userRegulation);
		} else if(userRegulation.isSuspendedCNMVDocuments()){
			hmReturnMsg = new HashMap<String, String>();
			hmReturnMsg.put("CODE_ID", CommonJSONService.ERROR_CODE_USER_CNMV_SUSPENDE + "");
			hmReturnMsg.put("ERROR_MSG", "regulation.suspended.due.documents");
			log.debug("The user is CNMV File Suspended. Error code: ERROR_CODE_USER_CNMV_SUSPENDE: " + userRegulation);
		} else if(userRegulation.isSuspendedDueDocuments()){
			hmReturnMsg = new HashMap<String, String>();
			hmReturnMsg.put("CODE_ID", CommonJSONService.ERROR_CODE_REGULATION_SUSPENDED_DOCUMENTS + "");
			hmReturnMsg.put("ERROR_MSG", "regulation.suspended.due.documents");
			log.debug("The user isSuspendedDueDocuments. Error code: ERROR_CODE_REGULATION_SUSPENDED_DOCUMENTS: " + userRegulation);
		} else if(userRegulation.isSuspended()){
			hmReturnMsg = new HashMap<String, String>();
			hmReturnMsg.put("CODE_ID", CommonJSONService.ERROR_CODE_REGULATION_SUSPENDED + "");
			hmReturnMsg.put("ERROR_MSG", "regulation.user.suspended");
			log.debug("The user isSuspended. Error code: ERROR_CODE_REGULATION_SUSPENDED: " + userRegulation);									
		} else if(userRegulation.showPopUpWarningDueDocuments()){
			hmReturnMsg = new HashMap<String, String>();
			hmReturnMsg.put("CODE_ID", CommonJSONService.ERROR_CODE_SHOW_WARNING_DUE_DOCUMENTS + "");
			hmReturnMsg.put("ERROR_MSG", "regulation.user.warning.due.documents");
			log.debug("Show to user warning Due Documents. Error code: ERROR_CODE_SHOW_WARNING_DUE_DOCUMENTS: " + userRegulation);
		}
		return hmReturnMsg;
	}
	
	public static boolean isRegulationSuspended(long userId) throws SQLException {
		Connection conn = null;
        try {
        	conn = getConnection();
			return UsersRegulationDAOBase.isRegulationSuspended(conn, userId);
		} finally {
            closeConnection(conn);
        }    
	}
	
	public static void insertIssueToUsers(long userId, long balance) throws SQLException{
		Connection con = null;
		try {
			con = getConnection();
			Issue issue = new Issue();
			
			// Filling issue fields
			issue.setSubjectId(String.valueOf(IssuesManagerBase.ISSUE_SUBJ_MIGRATION_SUSPEND));
			issue.setStatusId(String.valueOf(IssuesManagerBase.ISSUE_STATUS_FINISHED));
			issue.setUserId(userId);
			issue.setPriorityId(String.valueOf(IssuesManagerBase.ISSUE_PRIORITY_HIGH));
			issue.setType(ConstantsBase.ISSUE_TYPE_REGULAR);
			IssuesDAOBase.insertIssue(con, issue);
			// Filling action fields
			IssueAction issueAction = new IssueAction();
			issueAction.setChannelId(String.valueOf(IssuesManagerBase.ISSUE_CHANNEL_ACTIVITY));
			issueAction.setActionTypeId(String.valueOf(IssuesManagerBase.ISSUE_ACTION_TYPE_MIGRATION_SUSPEND));
			issueAction.setWriterId(40000); // invest migration writer id
			issueAction.setSignificant(true);
			issueAction.setActionTime(new Date());
			issueAction.setActionTimeOffset("GMT+00:00");
			issueAction.setComments("User suspended after transfer. User's balance "+ balance/100);
			issueAction.setIssueId(issue.getId());
			IssuesDAOBase.insertAction(con, issueAction);
		} finally {
			closeConnection(con);
		}
	}

	public static boolean insertAdminWithdraw(long balance, long userId) throws SQLException {

		if (log.isEnabledFor(Level.INFO)) {
			String ls = System.getProperty("line.separator");
			log.log(Level.INFO, "Insert Admin Withdraw: " + ls + "userId: "
					+ userId + ", balance" + balance + ls);
		}
		Connection connection =  null;
		try {
			connection = getConnection();
			connection.setAutoCommit(false);

			Transaction tran = new Transaction();
			tran.setAmount(balance);
			tran.setComments("Withdawal for balance transfer to Invest.com account");
			tran.setIp(CommonUtil.getIPAddress());
			tran.setProcessedWriterId(Writer.MIGRATE_WRITER_ID);
			tran.setTimeSettled(new Date());
			tran.setTimeCreated(new Date());
			tran.setUtcOffsetCreated("GMT-00:00");
			tran.setUtcOffsetSettled("GMT-00:00");
			tran.setTypeId(TransactionsManagerBase.TRANS_TYPE_ADMIN_WITHDRAW);
			tran.setUserId(userId);
			tran.setWriterId(Writer.MIGRATE_WRITER_ID);
			tran.setStatusId(TransactionsManagerBase.TRANS_STATUS_SUCCEED);
			tran.setChequeId(null);
			tran.setWireId(null);
			tran.setChargeBackId(null);
			tran.setReceiptNum(null);
			tran.setCreditCardId(null);
			tran.setDescription(null);
			
			UsersDAOBase.addToBalance(connection, userId, -balance);
			TransactionsDAOBase.insert(connection, tran);
			GeneralDAO.insertBalanceLog(connection,
						Writer.MIGRATE_WRITER_ID, tran.getUserId(),
						ConstantsBase.TABLE_TRANSACTIONS, tran.getId(),
						ConstantsBase.LOG_BALANCE_ADMIN_WITHDRAW,
						"GMT-00:00");
			
			if (log.isEnabledFor(Level.INFO)) {
				String ls = System.getProperty("line.separator");
				log.log(Level.INFO, "Admin Withdraw insert successfully! " + ls
						+ tran.toString());
			}

			connection.commit();
		} catch (SQLException e) {
			log.log(Level.ERROR, "insertAdminWithdraw: ", e);
			rollback(connection);
			throw e;
		} finally {
			setAutoCommitBack(connection);
			closeConnection(connection);
		}
		return true;
	}
}
