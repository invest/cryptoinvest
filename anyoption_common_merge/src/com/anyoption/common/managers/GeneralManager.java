package com.anyoption.common.managers;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

import org.apache.log4j.Logger;

import com.anyoption.common.daos.GeneralDAO;

public class GeneralManager extends BaseBLManager {
	
	private static final Logger log = Logger.getLogger(GeneralManager.class);
	
	public static HashSet<String> allowedIP;
	public static HashMap<Long, String> predefinedDepositAmount;
	public static Map<Long, String> bdaPredefinedDepositAmount;
	public static String jsonPredefinedDepositMap;
	
	public static HashSet<String> getAllowIPList() {
		Connection con = null;
		try {
			con = getConnection();
			if (allowedIP == null || allowedIP.isEmpty()){
				allowedIP = GeneralDAO.getAllowIPList(con);
			}
			return allowedIP;
		} catch (SQLException e) {
			log.debug("Failed to load allowed IPs", e);
			return null;
		} finally {
			closeConnection(con);
		}
	}
	
	public static void setAllowedIP(HashSet<String> allowedIP) {
		GeneralManager.allowedIP = allowedIP;
	}
	
	public static HashMap<Long, String> getPredefinedDepositAmount() {
		if (predefinedDepositAmount == null) {
			Connection con = null;
			try {
				con = getConnection();
				predefinedDepositAmount = GeneralDAO.getAllPredefinedDepositAmounts(con);
			} catch (SQLException ex) {
				log.error("Can't lazy load predefinedDepositAmount", ex);
			} finally {
				closeConnection(con);
			}
		}
		return predefinedDepositAmount;
	}
	
	public static String getPredefinedDepositAmountJsonMap() {
		if (jsonPredefinedDepositMap == null) {
			HashMap<Long, String> predefinedDepositAmountMap = new HashMap<Long, String>();
			predefinedDepositAmountMap = getPredefinedDepositAmount();
			jsonPredefinedDepositMap = "{";
			for (Map.Entry<Long, String> entry : predefinedDepositAmountMap.entrySet()) {
				jsonPredefinedDepositMap += "" + entry.getKey() + ":" + entry.getValue() + ",";
			}
			jsonPredefinedDepositMap = jsonPredefinedDepositMap.substring(0, jsonPredefinedDepositMap.length());
			jsonPredefinedDepositMap += "}";
		}
		return jsonPredefinedDepositMap;
	}

	public static Map<Long, String> getBDAPredefinedDepositAmounts() {
		if (bdaPredefinedDepositAmount == null) {
			Connection con = null;
			try {
				con = getConnection();
				bdaPredefinedDepositAmount = GeneralDAO.getBDAPredefinedDepositAmounts(con);
			} catch (SQLException ex) {
				log.error("Unable to load bdaPredefinedDepositAmount", ex);
			} finally {
				closeConnection(con);
			}
		}
		return bdaPredefinedDepositAmount;
	}
}
