package com.anyoption.common.managers;

import java.sql.Connection;
import java.sql.SQLException;

import org.apache.log4j.Logger;

import com.anyoption.common.daos.RewardUserPlatformsDAO;

/**
 * @author liors
 *
 */
public class RewardUserPlatformsManager extends BaseBLManager {
	private static final Logger logger = Logger.getLogger(RewardUserPlatformsManager.class);

	/**
	 * @param taskUser
	 * @return
	 * @throws SQLException
	 */
	public static boolean isTaskDone(long userId, long taskId) throws SQLException {
		Connection connection = getConnection();
        try {            
        	return RewardUserPlatformsDAO.isTaskDone(connection, userId, taskId);
        } finally {
        	closeConnection(connection);
        }
	}	
}
