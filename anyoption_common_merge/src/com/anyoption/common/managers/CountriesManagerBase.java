package com.anyoption.common.managers;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Hashtable;

import com.anyoption.common.beans.Country;
import com.anyoption.common.beans.base.Skin;
import com.anyoption.common.daos.CountriesDAOBase;

public class CountriesManagerBase extends BaseBLManager {
    /**
     * Get all countries
     *
     * @param con connection to Db
     * @return <code>Hashtable<Long, Country></code>
     * @throws SQLException
     */
    public static Hashtable<Long, Country> getAll() throws SQLException {
        Connection conn = null;
        try {
            conn = getConnection();
            return CountriesDAOBase.getAll(conn);
        } finally {
            closeConnection(conn);
        }
    }
    
    public static Hashtable<Long, Country> getAllAllowedCountries() throws SQLException {
        Connection conn = null;
        try {
            conn = getConnection();
            return CountriesDAOBase.getAllAllowedCountries(conn);
        } finally {
            closeConnection(conn);
        }
    }

    /**
     * Get country by code
     *
     * @param countryCode country code
     * @return long countryId
     * @throws SQLException
     */
    public static long getCountryIdByCode(String countryCode) throws SQLException {
        Connection conn = null;
        try {
            conn = getConnection();
            return CountriesDAOBase.getCountryIdByCountryCode(conn, countryCode);
        } finally {
            closeConnection(conn);
        }
    }

    /**
     * Get skin by country id
     *
     * @param countryId country id
     * @return long skin id
     * @throws SQLException
     */
    public static long getSkinIdByCountryId(long countryId) throws SQLException {
        Connection conn = null;
        long res = 0l;
        try {
            conn = getConnection();
            res = CountriesDAOBase.getSkinIdByCountryId(conn, countryId);
            if(res == 0){
            	Country c = CountryManagerBase.getCountry(countryId);
            	if(c.isRegulated()){
            		res = Skin.SKIN_REG_EN;
            	} else {
            		res = Skin.SKIN_ENGLISH_NON_REG;
            	}
            }
            return res;
        } finally {
            closeConnection(conn);
        }
    }
}