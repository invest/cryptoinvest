/**
 * 
 */
package com.anyoption.common.managers;

import java.sql.Connection;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.Transaction;
import com.anyoption.common.beans.base.QuestionnaireAnswer;
import com.anyoption.common.beans.base.QuestionnaireGroup;
import com.anyoption.common.beans.base.QuestionnaireQuestion;
import com.anyoption.common.beans.base.QuestionnaireUserAnswer;
import com.anyoption.common.beans.base.User;
import com.anyoption.common.beans.base.UserRegulationBase;
import com.anyoption.common.daos.QuestionnaireDAOBase;
import com.anyoption.common.daos.TransactionsDAOBase;
import com.anyoption.common.util.CommonUtil;

/**
 * @author pavelhe
 *
 */
public class QuestionnaireManagerBase extends BaseBLManager {
	private static final Logger log = Logger.getLogger(QuestionnaireManagerBase.class);
	
	private static Map<String, QuestionnaireGroup> questionnairesMap;
	
	/**
	 * @return
	 * @throws SQLException
	 */
	public static Map<String, QuestionnaireGroup> getAllQuestionnaires() throws SQLException {
		if (questionnairesMap == null) {
			Connection conn = null;
			try {
				// TODO This should be done with a cursor...
				conn = getConnection();
				List<QuestionnaireGroup> groups = QuestionnaireDAOBase.getAllGroups(conn);
				questionnairesMap = new LinkedHashMap<String, QuestionnaireGroup>();
				for (QuestionnaireGroup group : groups) {
					questionnairesMap.put(group.getName(), group);
					group.setQuestions(QuestionnaireDAOBase.getAllQuestionsByGroup(conn, group.getId()));
					for (QuestionnaireQuestion question : group.getQuestions()) {
						question.setAnswers(QuestionnaireDAOBase.getAllAnswersByQuestion(conn, question.getId()));
					}
				}
			} finally {
				closeConnection(conn);
			}
		}
		return questionnairesMap;
	}
	
	/**
	 * @param userId
	 * @param questions
	 * @return
	 * @throws SQLException
	 */
	public static List<QuestionnaireUserAnswer> getUserAnswers(
													long userId,
													List<QuestionnaireQuestion> questions,
													long writerId) throws SQLException  {
		if (questions == null || questions.size() == 0) {
			log.error("Cannot get answers - questions list is empty [" + questions + "]");
			return null;
		}
		
		List<QuestionnaireUserAnswer> userAnswers = null;
		Connection conn = null;
		try {
			conn = getConnection();
			userAnswers = QuestionnaireDAOBase.getUserAnswers(conn, userId, questions.get(0).getGroupId());
			
			if (userAnswers == null || userAnswers.size() == 0 || userAnswers.size() != questions.size()) {
				log.debug("User answers for user [" + userId
							+ "] from group [" + questions.get(0).getGroupId()
							+ "] do not exist in DB, creating them...");
				userAnswers = createUserAnswers(conn, userAnswers, userId, questions, writerId);
			}
		} finally {
			closeConnection(conn);
		}
		
		return userAnswers;
	}
	
	/**
	 * @param conn
	 * @param userId
	 * @param questions
	 * @return
	 * @throws SQLException
	 */
	private static List<QuestionnaireUserAnswer> createUserAnswers(
													Connection conn, List<QuestionnaireUserAnswer> userAnswers,
													long userId,
													List<QuestionnaireQuestion> questions,
													long writerId) throws SQLException {
		if( userAnswers == null) {
			userAnswers = new ArrayList<QuestionnaireUserAnswer>();
		}
		
		for (QuestionnaireQuestion question : questions) {
			boolean exists = false;
			for(QuestionnaireUserAnswer userAnswer: userAnswers) {
				if(userAnswer.getQuestionId() == question.getId()) {
					exists = true;
					break;
				}
			}
			if(!exists) {
				QuestionnaireUserAnswer answer = new QuestionnaireUserAnswer();
				answer.setGroupId(question.getGroupId());
				answer.setQuestionId(question.getId());
				answer.setUserId(userId);
				answer.setTimeCreated(new Date());
				answer.setWriterId(writerId);
				log.debug("Inserting user answer [" + answer + "]");
				QuestionnaireDAOBase.insertUserAnswer(conn, answer, false);
				userAnswers.add(answer);
			}
		}
		
		return userAnswers;
	}
	
	/**
	 * @param userAnswers
	 * @throws SQLException
	 */
	public static void updateUserAnswers(List<QuestionnaireUserAnswer> userAnswers, boolean setScore) throws SQLException {
		Connection conn = null;
		try {
			conn = getConnection();
			log.debug("Updating user answers [" + userAnswers + "] ...");
			QuestionnaireDAOBase.updateUserAnswers(conn, userAnswers, setScore);
		} finally {
			closeConnection(conn);
		}
	}
	
	/**
	 * @param userAnswerId
	 * @return
	 * @throws SQLException
	 */
	public static QuestionnaireGroup getGroupByUserAnswerId(long userAnswerId) throws SQLException {
		Connection conn = null;
		try {
			conn = getConnection();
			long groupId = QuestionnaireDAOBase.getGroupIdByUserAnswerId(conn, userAnswerId);
			for (Map.Entry<String, QuestionnaireGroup> entries : getAllQuestionnaires().entrySet()) {
				if (entries.getValue().getId() == groupId) {
					return entries.getValue();
				}
			}
		} finally {
			closeConnection(conn);
		}
		return null;
	}
	public static long updateUserAnswersScoreGroup(long userId) throws SQLException {
		Connection conn = null;
		long res = -1;
		try {
			conn = getConnection();
			Long score = QuestionnaireDAOBase.getScoreUserAnswers(conn, userId);
			if(score != null){
				res = QuestionnaireDAOBase.updateUserAnswersScoreGroup(conn, score, userId);
				log.debug("User answers Score/Group was updated for userID:" + userId);
			} else {
				log.debug("Can not update user answers Score/Group. The score is NULL!!!");
			}
		} finally {
			closeConnection(conn);
		}
		
		return res;
	}
	
	public static Long getAnswertScore( QuestionnaireGroup questionnaire, QuestionnaireUserAnswer userAnswer) throws SQLException {		
		Long score = null;			
		for(QuestionnaireQuestion question : questionnaire.getQuestions()){
			for(QuestionnaireAnswer answer : question.getAnswers()){
				if(userAnswer.getAnswerId() != null){
					if(answer.getId() == userAnswer.getAnswerId()){
						score = answer.getScore();
					}
				}
			}
		}
		return score;
	}
	
	public static boolean isAllCapitalAnswFilled(long userId) throws SQLException {
		Connection conn = null;
		try {
			conn = getConnection();
			return QuestionnaireDAOBase.isAllCapitalAnswFilled(conn, userId);
		} finally {
			closeConnection(conn);
		}
	}
	
	public static boolean isSingelQuestionnaireIncorrectFilled(long userId) throws SQLException {
		Connection conn = null;
		boolean res =  false;
		try {
			conn = getConnection();
			HashMap<Integer, Boolean> answersHM = QuestionnaireDAOBase.getUserIncorrectAnswers(conn, userId);
			if(answersHM.size() > 0){
				//Check questions wrong out of the 4 questions from list: Q#1(a), Q#6(e), Q#8(a), Q#9(b). 
				if(answersHM.get(10) && answersHM.get(40) && answersHM.get(60) && answersHM.get(70)){
					res = true;
					log.debug("User " + userId + " is with IncorrectAnswers: " + answersHM.toString());
				}
			}
		} finally {
			closeConnection(conn);
		}		
		
		return res;
	}

	public static long isSingelQuestionnaireRestrictedFilled(long userId) throws SQLException {
		Connection conn = null;
		long res = 0;
		int br = 0;
		try {
			conn = getConnection();
			HashMap<Integer, Boolean> answersHM = QuestionnaireDAOBase.getUserIncorrectAnswers(conn, userId);
			if(answersHM.size() > 0){
				//Check 3 questions wrong out of the 4 questions from list: Q#1(a), Q#6(e), Q#8(a), Q#9(b). 
				if(answersHM.get(10)){
					br ++;
				}				
				if(answersHM.get(40)){
					br ++;
				}				
				if(answersHM.get(60)){
					br ++;
				}				
				if(answersHM.get(70)){
					br ++;
				}
				if(br == 3){
					res = UserRegulationBase.AO_REGULATION_RESTRICTED_LOW_X_SCORE_GROUP_ID;
					log.debug("User (with low X) " + userId + " is with RestrictedAnswers: " + answersHM.toString());
				}				
				//Check  Q#1(a) AND {answered Q#8(a) OR Q#9(b)} 
				if(res == 0){
					if(answersHM.get(10) && (answersHM.get(60) || answersHM.get(70))){
						res = UserRegulationBase.AO_REGULATION_RESTRICTED_HIGH_Y_SCORE_GROUP_ID;
						log.debug("User (with high Y)" + userId + " is with RestrictedAnswers: " + answersHM.toString());
					}
				}
			}
		} finally {
			closeConnection(conn);
		}		
		
		return res;
	}
	
	public static void updateUserScoreGroupOnly(UserRegulationBase ur) throws SQLException {
		Connection conn = null;
		try {
			conn = getConnection();			
			QuestionnaireDAOBase.updateUserScoreGroupOnly(conn, ur);
			log.debug("User scoreGroup (" + ur.getScoreGroup() + ") was updated for userID:" + ur.getUserId());
		} finally {
			closeConnection(conn);
		}		
	}
	
	public static  Date isUserAnswerNoOnPEPQuestion(long userId) throws SQLException {
		Connection conn = null;
		try {
			conn = getConnection();	
			log.debug("Check PEP answer for userId" + userId);
			return QuestionnaireDAOBase.isUserAnswerNoOnPEPQuestion(conn, userId);			
		} finally {
			closeConnection(conn);
		}		
	}
	
	public static void sendEmailInCaseUserAnswearNoOnPEPQuestion(User u, Date timeAnswearCreated, boolean isAfterGBG)throws SQLException {
		Connection conn = null;
		try {
			conn = getConnection();
			if (timeAnswearCreated != null || isAfterGBG) {
				Transaction transaction = TransactionsDAOBase.getFTDInfoById(conn, u.getFirstDepositId());
				String from = CommonUtil.getConfig("email.from");
				String to = CommonUtil.getConfig("email.to.for.pep.answer");
				String subject = "User " + u.getId() + " Identified as PEP";
		        String body = "User ID: " + u.getId() + "\r\n" +
		        			  "SKIN: " + u.getSkinId() + "\r\n" +
		        			  "Country: " + u.getCountryId() + "\r\n" +
		        			  "User first name: " + u.getFirstName() + "\r\n" +
		        			  "User last name: " + u.getLastName() + "\r\n" +
		        			  "User email: " + u.getEmail() + "\r\n" +
		        			  "Registration date: " + u.getTimeCreated() + "\r\n" ;
		        if(isAfterGBG){
		  		  	body += "After GBG response: " + timeAnswearCreated + "\r\n";
		        } else {
		        	body += "Questionnaire date: " + timeAnswearCreated + "\r\n";
		        }
		        	if (transaction != null) {
		        		body += "FTD: " + transaction.getAmount()/100+ u.getCurrency().getCode() + "\r\n" + 
		        				"FTD: " + transaction.getBaseAmount()/100 + " USD \r\n";
		        	}
				CommonUtil.sendEmailMsg(from, to, subject, body);
			}
		} finally {
			closeConnection(conn);
		}
	}
	
	public static void sendEmailInCaseFailedQouestinnaire(User u) {
		log.debug("sending email alert for user who failed questionnaire. user id " + u.getId());
		Connection conn = null;
		DecimalFormat sd = new DecimalFormat("###,###.##", new DecimalFormatSymbols(Locale.US));
		try {
			conn = getConnection();
			Transaction transaction = TransactionsDAOBase.getFTDInfoById(conn, u.getFirstDepositId());
			String from = CommonUtil.getConfig("email.from");
			String to = CommonUtil.getConfig("email.to.failed.questionnaire");
			
			String skinName;
			try {
				Locale locale = new Locale("en");
				skinName = CommonUtil.getMessage(locale, SkinsManagerBase.getSkin(u.getSkinId()).getDisplayName(), null);
			} catch (Exception e) {
				log.error("cant get skin name will use skin id");
				skinName = String.valueOf(u.getSkinId());
			}
			SimpleDateFormat formatDate = new SimpleDateFormat("dd.MM.yy HH:mm");
			String subject = "User (" + u.getId() + ") (" + skinName + ") Failed questionnaire FTD: " + (transaction != null ? transaction.getBaseAmount()/100 : 0) + "$";
	        String body = "User ID: " + u.getId() + "\r\n" +
	        			  "User email: " + u.getEmail() + "\r\n" +
	        			  "SKIN: " + skinName + "\r\n" +
	        			  "phone: " + u.getMobilePhone() + "\r\n";
	        if (transaction != null) {
    			  body += "FTD: " + sd.format(transaction.getAmount()/100D) + u.getCurrency().getCode() + "\r\n" + 
    					  "FTD: " + sd.format(transaction.getBaseAmount()/100D) + "$ \r\n" +
  	        			  "FTD time: " + formatDate.format(transaction.getTimeCreated()) + " \r\n";
  	        }
	        	  body += "Registration date: " + formatDate.format(u.getTimeCreated()) + "\r\n" +
	        	  		  "Questionnaire date: " + formatDate.format(new Date()) + "\r\n";
			CommonUtil.sendEmailMsg(from, to, subject, body);
		} catch (SQLException e) {
			log.error("cant Create email alert for user who failed questionnaire. user id " + u.getId());
		} finally {
			closeConnection(conn);
		}
	}
	
	public static void sendEmailInCaseQuestionnaireRestrictedBlocked(long userId, String from , String to, String skinName) {
		log.debug("sending email alert for user who questionnaire restricted and blocked due to loss threshold. user id " + userId);
		Connection conn = null;
		DecimalFormat sd = new DecimalFormat("###,###.##", new DecimalFormatSymbols(Locale.US));
		try {
			conn = getConnection();
			User user = UsersManagerBase.getUserById(userId);
			long sumAmountUSD = TransactionsDAOBase.getSumOfRealDepositsUSD(conn, user.getId());
			SimpleDateFormat formatDate = new SimpleDateFormat("dd/MM/yyyy HH:mm");
			String subject = "User (" + user.getId() + ") (" + skinName + ") Blocked from trading (questionnaire restricted and reached loss threshold) ";
			String body = "User ID: " + user.getId() + "\r\n" + 
						  "User email: " + user.getEmail() + "\r\n" + 
						  "SKIN: " + skinName + "\r\n" +
						  "phone: " + user.getMobilePhone() + "\r\n" +
						  "Total deposits USD: " + sd.format(sumAmountUSD / 100D) + "$ \r\n" +
						  "Time trading block "  + formatDate.format(new Date()) + " GMT \r\n";
			CommonUtil.sendEmailMsg(from, to, subject, body);
		} catch (SQLException e) {
			log.error("Error sending email alert for user who questionnaire restricted and blocked due to loss threshold. user id " + userId);
		} finally {
			closeConnection(conn);
		}
	}

	
	public static void updateUserPepAnswer(UserRegulationBase ur) throws SQLException {
		Connection conn = null;
		try {
			conn = getConnection();			
			QuestionnaireDAOBase.updateUserPepAnswer(conn, ur);			
		} finally {
			closeConnection(conn);
		}		
	}
	
	public static void updateCheckedPepAnswer(long userId) throws SQLException {
		Connection conn = null;
		try {
			conn = getConnection();			
			QuestionnaireDAOBase.updateCheckedPepAnswer(conn, userId);			
		} finally {
			closeConnection(conn);
		}		
	}
	
	/**
	 * This method returns the answer as HashMap (with 2 keys - answerName and textAnswer) of the withdraw survey to be shown in Backend 
	 * @param transactionId
	 * @return withdrawSurveyAnswer
	 * @throws SQLException
	 */
	public static  HashMap<String, String> getWithdrawSurveyAnswer(long transactionId) throws SQLException {
		Connection conn = null;
		HashMap<String, String> withdrawSurveyAnswer = null;
		try {
			conn = getConnection();			
			withdrawSurveyAnswer = QuestionnaireDAOBase.getWithdrawSurveyAnswer(conn, transactionId);			
		} finally {
			closeConnection(conn);
		}	
		return withdrawSurveyAnswer;
	}

	public static List<QuestionnaireUserAnswer> getAllUserAnswers(long userId, long groupId) throws SQLException {
		Connection conn = null;
		try {
			conn = getConnection();
			return QuestionnaireDAOBase.getAllUserAnswers(conn, userId, groupId);
		} finally {
			closeConnection(conn);
		}
	}
}
