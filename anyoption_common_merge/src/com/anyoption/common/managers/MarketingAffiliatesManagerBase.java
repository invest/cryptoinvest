package com.anyoption.common.managers;

import java.sql.Connection;
import java.sql.SQLException;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.MarketingAffiliate;
import com.anyoption.common.daos.MarketingAffiliatesDAOBase;

/**
 * @author eranl
 *
 */
public class MarketingAffiliatesManagerBase extends BaseBLManager {

	private static final Logger logger = Logger.getLogger(MarketingAffiliatesManagerBase.class);
	public static final long AFFILIATE_MANAGER_ALL = 0;
	public static final String ENUM_MARKETING_AFFILIATE_KEY_ENUMERATOR = "marketing_affiliate_key";
	public static final String ENUM_MARKETING_AFFILIATE_KEY_SEGMOB_CODE = "marketing_affiliate_key_segmob";
	
	
	/**
	 * Get affiliate by Key
	 * @param affiliateKey
	 * @throws SQLException
	 */
	public static MarketingAffiliate getAffiliateByKey(long affiliateKey) throws SQLException {
		Connection con = getConnection();
		try {
			return MarketingAffiliatesDAOBase.getAffiliateByKey(con, affiliateKey);
		} finally {
			closeConnection(con);
		}
	}
	
}
