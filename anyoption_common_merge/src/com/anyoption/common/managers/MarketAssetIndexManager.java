package com.anyoption.common.managers;

import java.sql.Connection;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Locale;
import java.util.Map;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.MarketTranslations;
import com.anyoption.common.beans.base.AssetIndexFormula;
import com.anyoption.common.beans.base.MarketAssetIndex;
import com.anyoption.common.daos.MarketAssetIndexDAO;
import com.anyoption.common.util.CommonUtil;

public class MarketAssetIndexManager extends BaseBLManager {

	private static final Logger logger = Logger.getLogger(BestHotDataManager.class);
	
	public static long EXPIRY_TYPE_HOURLY = 1;
	public static long EXPIRY_TYPE_DAILY = 2;
	public static long EXPIRY_TYPE_LONG_TERM = 3;
	
	private static LocalDate dateMarketsBySkin;
	private static LocalDate mobileDateMarketsBySkin;
	private static LocalDate dateFormulasBySkin;
	private static LocalDate dateFormulas;
	
	public static HashMap<Long, Map<Long, MarketAssetIndex>> assetIndexMarketsBySkin;
	public static HashMap<Long, Map<Long, MarketAssetIndex>> mobileAssetIndexMarketsBySkin;
	public static HashMap<Long, ArrayList<AssetIndexFormula>> assetIndexFormulasBySkin;
	public static ArrayList<AssetIndexFormula> assetIndexFormulas;
	
	public static Map<Long, MarketAssetIndex> getMarketAssetIndicesBySkin(long skinId) throws SQLException {
		Connection con = null;
		if (assetIndexMarketsBySkin == null) {
			try {
				con = getConnection();
				Map<Long, MarketAssetIndex> map = MarketAssetIndexDAO.getMarketAssetIndicesBySkin(con, skinId);
				assetIndexMarketsBySkin = new HashMap<Long, Map<Long, MarketAssetIndex>>();
				assetIndexMarketsBySkin.put(skinId, map);
				dateMarketsBySkin = LocalDateTime.now().toLocalDate();
			} catch (SQLException sqle) {
				logger.error("Cannot load asset index markets for skin ID " + skinId, sqle);
				throw sqle;
			} finally {
				closeConnection(con);
			}
		} else {
			LocalDate currentDate = LocalDateTime.now().toLocalDate();
			synchronized (assetIndexMarketsBySkin) {
				if ((assetIndexMarketsBySkin.get(skinId) == null) || currentDate.isAfter(dateMarketsBySkin)) {
					try {
						con = getConnection();
						if (currentDate.isAfter(dateMarketsBySkin)) {
							assetIndexMarketsBySkin.clear();
						}
						Map<Long, MarketAssetIndex> map = MarketAssetIndexDAO.getMarketAssetIndicesBySkin(con, skinId);
						assetIndexMarketsBySkin.put(skinId, map);
						dateMarketsBySkin = LocalDateTime.now().toLocalDate();
					} catch (SQLException sqle) {
						logger.error("Cannot load asset index markets for skin ID " + skinId, sqle);
						throw sqle;
					} finally {
						closeConnection(con);
					}
				}
			}
		}
		return assetIndexMarketsBySkin.get(skinId);
	}
	
	public static MarketAssetIndex getAssetIndexMarketBySkinAndID(long skinId, long marketId) throws SQLException {
		Connection con = null;
		if(mobileAssetIndexMarketsBySkin == null) {
			try {
				con = getConnection();
				Map<Long, MarketAssetIndex> map = MarketAssetIndexDAO.mobileAssetIndexMarketsBySkin(con, skinId);
				mobileAssetIndexMarketsBySkin = new HashMap<Long, Map<Long, MarketAssetIndex>>();
				mobileAssetIndexMarketsBySkin.put(skinId, map);
				mobileDateMarketsBySkin = LocalDateTime.now().toLocalDate();
			} catch (SQLException sqle) {
				logger.error("Cannot load mobile asset index markets for skin ID " + skinId, sqle);
				throw sqle;
			} finally {
				closeConnection(con);
			}
		} else {
			LocalDate currentDate = LocalDateTime.now().toLocalDate();
			if (currentDate.isAfter(mobileDateMarketsBySkin) || (mobileAssetIndexMarketsBySkin.get(skinId) == null || mobileAssetIndexMarketsBySkin.get(skinId).get(marketId) == null)) {
				try {
					con = getConnection();
					Map<Long, MarketAssetIndex> map = MarketAssetIndexDAO.mobileAssetIndexMarketsBySkin(con, skinId);
					mobileAssetIndexMarketsBySkin.put(skinId, map);
					mobileDateMarketsBySkin = LocalDateTime.now().toLocalDate();
				} catch (SQLException sqle) {
					logger.error("Cannot load mobile asset index markets for skin ID " + skinId, sqle);
					throw sqle;
				} finally {
					closeConnection(con);
				}
			}
		}
		return mobileAssetIndexMarketsBySkin.get(skinId).get(marketId);
	}
	
	public static ArrayList<AssetIndexFormula> getAssetIndexFormulasBySkin(long skinId) throws SQLException {
		Connection con = null;
		Locale locale = new Locale(LanguagesManagerBase.getLanguage(SkinsManagerBase.getSkin(skinId).getDefaultLanguageId()).getCode());
		if (assetIndexFormulasBySkin == null) {
			try {
				con = getConnection();
				ArrayList<AssetIndexFormula> formulasList = getAssetIndexFormulas();
				ArrayList<AssetIndexFormula> formulasListCopy = new ArrayList<AssetIndexFormula>();
				for (AssetIndexFormula formulas : formulasList) {
					AssetIndexFormula formulasCopy = new AssetIndexFormula();
					formulasCopy.setFormulaId(formulas.getFormulaId());
					formulasCopy.setFormulaKey(formulas.getFormulaKey());
					formulasCopy.setFormulaTranslation(CommonUtil.getMessage(locale, formulas.getFormulaKey(), null));
					formulasListCopy.add(formulasCopy);
				}
				assetIndexFormulasBySkin = new HashMap<Long, ArrayList<AssetIndexFormula>>();
				assetIndexFormulasBySkin.put(skinId, formulasListCopy);
				dateFormulasBySkin = LocalDateTime.now().toLocalDate();
			} catch (SQLException sqle) {
				logger.error("Cannot load asset index market formulas for skin ID " + skinId, sqle);
				throw sqle;
			} finally {
				closeConnection(con);
			}
		} else {
			LocalDate currentDate = LocalDateTime.now().toLocalDate();
			if (currentDate.isAfter(dateFormulasBySkin) || (assetIndexFormulasBySkin.get(skinId) == null)) {
				try {
					con = getConnection();
					ArrayList<AssetIndexFormula> formulasList = getAssetIndexFormulas();
					ArrayList<AssetIndexFormula> formulasListCopy = new ArrayList<AssetIndexFormula>();
					for (AssetIndexFormula formulas : formulasList) {
						AssetIndexFormula formulasCopy = new AssetIndexFormula();
						formulasCopy.setFormulaId(formulas.getFormulaId());
						formulasCopy.setFormulaKey(formulas.getFormulaKey());
						formulasCopy.setFormulaTranslation(CommonUtil.getMessage(locale, formulas.getFormulaKey(), null));
						formulasListCopy.add(formulasCopy);
					}
					assetIndexFormulasBySkin.put(skinId, formulasListCopy);
					dateFormulasBySkin = LocalDateTime.now().toLocalDate();
				} catch (SQLException sqle) {
					logger.error("Cannot load asset index market formulas for skin ID " + skinId, sqle);
					throw sqle;
				} finally {
					closeConnection(con);
				}
			}
		}
		return assetIndexFormulasBySkin.get(skinId);
	}

	public static ArrayList<AssetIndexFormula> getAssetIndexFormulas() throws SQLException {
		Connection con = null;
		LocalDate currentDate = LocalDateTime.now().toLocalDate();
		if (assetIndexFormulas == null) {
			try {
				dateFormulas = LocalDateTime.now().toLocalDate();
				con = getConnection();
				assetIndexFormulas = MarketAssetIndexDAO.getAssetIndexFormulas(con);
			} catch (SQLException sqle) {
				logger.error("Cannot load asset index market formulas! ", sqle);
				throw sqle;
			} finally {
				closeConnection(con);
			}
		} else if (currentDate.isAfter(dateFormulas)) {
			try {
				con = getConnection();
				assetIndexFormulas = MarketAssetIndexDAO.getAssetIndexFormulas(con);
				dateFormulas = LocalDateTime.now().toLocalDate();
			} catch (SQLException sqle) {
				logger.error("Cannot load asset index market formulas! ", sqle);
				throw sqle;
			} finally {
				closeConnection(con);
			}
		}
		return assetIndexFormulas;
	}
	
	public static MarketTranslations getMarketTranslationsByMarketId(long marketId) throws SQLException {
		Connection con = null;
		try {
			con = getConnection();
			return MarketAssetIndexDAO.getMarketTranslationsByMarketId(con, marketId);
		} finally {
			closeConnection(con);
		}
	}
	
	public static void updateMarketTranslations(MarketTranslations marketTranslations, long marketId) throws SQLException {
		Connection con = null;
		try {
			con = getConnection();
			updateAllMarketTranslations(con, marketId, marketTranslations);
		} finally {
			closeConnection(con);
		}
	}
	
	public static void updateAllMarketTranslations(Connection con, long marketId, MarketTranslations translations) throws SQLException {
		if (translations.getMarketName().keySet().equals(translations.getMarketShortName().keySet())
				&& translations.getMarketName().keySet().equals(translations.getMarketDescription().keySet())
				&& translations.getMarketName().keySet().equals(translations.getMarketDescription().keySet())
				&& translations.getMarketName().keySet().equals(translations.getAdditionalText().keySet())
				&& translations.getMarketName().keySet().equals(translations.getPageDescription().keySet())
				&& translations.getMarketName().keySet().equals(translations.getPageKeywords().keySet())
				&& translations.getMarketName().keySet().equals(translations.getPageTitle().keySet())
				&& translations.getMarketName().keySet().equals(translations.getDescriptionInPage().keySet())) {

			Iterator<Long> it = translations.getMarketName().keySet().iterator();
			while (it.hasNext()) {
				long languageId = it.next();
				MarketAssetIndexDAO.updateMarketTranslations(con, marketId, languageId,
						translations.getMarketName().get(languageId),
						translations.getMarketShortName().get(languageId),
						translations.getAdditionalText().get(languageId),
						translations.getMarketDescription().get(languageId),
						translations.getPageDescription().get(languageId),
						translations.getPageKeywords().get(languageId),
						translations.getPageTitle().get(languageId),
						translations.getDescriptionInPage().get(languageId));
			}
		}
	}
}