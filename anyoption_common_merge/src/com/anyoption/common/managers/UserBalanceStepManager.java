/**
 * 
 */
package com.anyoption.common.managers;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.BalanceStep;
import com.anyoption.common.beans.User;
import com.anyoption.common.beans.UserActiveData;
import com.anyoption.common.beans.Writer;
import com.anyoption.common.beans.base.BalanceStepPredefValue;
import com.anyoption.common.daos.BalanceStepsDAOBase;
import com.anyoption.common.daos.UserBalanceStepDAO;

/**
 * Manager for user balance step
 * 
 * @author kirilim
 */
public class UserBalanceStepManager extends BalanceStepsManagerBase {

	private static final Logger logger = Logger.getLogger(UserBalanceStepManager.class);
	private static final double BALANCE_STEP_DEFAULT_AMOUNT_MULTIPLIER = 1.5;
	private static final int BDA_MULTIPLIER_STEP_ONE = 1;
	private static final int BDA_MULTIPLIER_STEP_TWO = 2;
	private static final int BDA_MULTIPLIER_STEP_THREE = 3;
	private static final int BDA_MULTIPLIER_STEP_FOUR = 5;
	private static Map<Long, BalanceStep> logoutCurrencyDefaultBalanceStep;

	private static void loadUserBalanceStep(User user, boolean hasDeposits) {
		Connection con = null;
		try {
			con = getConnection();
			// One click algorithm
			if (hasDeposits) {
				Long userDefaultAmountValue = null;
				// determines whether the default amount value is changeable
				boolean rewritable = false;
				List<BalanceStepPredefValue> userPredefValues = null;
				UserActiveData uad = UserBalanceStepDAO.getUserActiveData(con, user.getId(), user.getCurrencyId());

				if (uad == null) { // this is in case no user active data to set
					BalanceStep balanceStep = getLogoutCurrencyDefaultBalanceStep().get(user.getCurrencyId());
					if (balanceStep != null) {
						user.setDefaultAmountValue(balanceStep.getDefaultAmountValue());
						user.setPredefValues(balanceStep.getPredefValues());
					} else {
						user.setDefaultAmountValue(0l);
						user.setPredefValues(new ArrayList<BalanceStepPredefValue>());
					}
					return;
				}

				if (uad.getCustomAmount() != null && uad.getCustomAmount() != 0) {
					// add the custom set amount as users default amount
					userDefaultAmountValue = uad.getCustomAmount();
				} else if (uad.getDefaultLastInvAmount() != null && uad.getDefaultLastInvAmount() != 0) {
					// add the last user investment as users default amount
					userDefaultAmountValue = uad.getDefaultLastInvAmount();
				} else if ((uad.getCustomStepId() != null && uad.getCustomStepId() != 0)) {
					// add the default amount from the custom balance step as users default amount
					userDefaultAmountValue = uad.getDefaultStepAmount();
				} else { // take it from the balance step of the user
					userDefaultAmountValue = uad.getDefaultStepAmount();
					rewritable = true;
				}

				if (uad.getAvgInvestment() > uad.getDefaultStepAmount()) { // avg investment is higher than the determined default amount
					// here comes the algorithm
					long valueToRound = Math.round(uad.getAvgInvestment() * BALANCE_STEP_DEFAULT_AMOUNT_MULTIPLIER);
					List<BalanceStep> balanceSteps = UserBalanceStepManager.getBalanceSteps(user.getCurrencyId());
					long closestValue = Long.MAX_VALUE;
					BalanceStepPredefValue closestPredefValue = null;
					BalanceStep closestBalanceStep = null;
					for (BalanceStep balanceStep : balanceSteps) { // searching through all steps and all predefined values
						Iterator<BalanceStepPredefValue> predefValueIterator = balanceStep.getPredefValues().iterator();
						while (predefValueIterator.hasNext()) {
							BalanceStepPredefValue predefValue = predefValueIterator.next();
							if (!predefValueIterator.hasNext()) { 	// we aren't taking the last predefined
								break;								// value as default amount value
							}
							Long value = predefValue.getPredefValue();
							long difference = Math.abs(valueToRound - value);
							if (difference < closestValue) { 		// if we find closer value
								closestValue = difference;	 		// we take it's step and
								closestPredefValue = predefValue; 	// will use as default
								closestBalanceStep = balanceStep; 	// amount
							} else if (difference == closestValue) { 								// if the value is on the same distance
								if (predefValue.getPriority() < closestPredefValue.getPriority()) { // we check priority
									closestPredefValue = predefValue;
									closestBalanceStep = balanceStep;
								} else if (predefValue.getPriority() == closestPredefValue.getPriority()) { // if the priority is equal
									if (balanceStep.getId() < closestBalanceStep.getId()) {					// then we have amounts from
										closestPredefValue = predefValue;									// different steps and we take
										closestBalanceStep = balanceStep;									// the smaller step
									}
								}
							}
						}
					}
					// add found default amount if we are able to change it and balance step
					userDefaultAmountValue = rewritable ? closestPredefValue.getPredefValue() : userDefaultAmountValue;
					userPredefValues = closestBalanceStep.getPredefValues();
				}
				// add calculated parameters to user bean
				user.setDefaultAmountValue(userDefaultAmountValue);
				// if we haven't run the algorithm the predefined values are not set
				if (userPredefValues == null) {
					userPredefValues = uad.getPredefValues();
				}
				user.setPredefValues(userPredefValues);
			} else {
				// add here the non-depositor step
				BalanceStep balanceStep = UserBalanceStepDAO.getNonDepositorsDefaultStep(con, user.getCurrencyId());
				user.setDefaultAmountValue(balanceStep.getDefaultAmountValue());
				user.setPredefValues(balanceStep.getPredefValues());
			}
		} catch (SQLException e) {
			logger.error("Can't get balance steps", e);
			return;
		} catch (Exception e) {
			logger.error("Can't get balance steps. Check balance steps to this currency: " + user.getCurrencyId(), e);
			return;
		} finally {
			closeConnection(con);
		}
	}

	public static void loadUserBalanceStep(User user, boolean hasDeposits, long writerId) {
		if (writerId != Writer.WRITER_ID_BUBBLES_DEDICATED_APP) {
			loadUserBalanceStep(user, hasDeposits);
		} else {
			loadBDAUserBalanceStep(user);
		}
	}

	private static void loadBDAUserBalanceStep(User user) {
		long defInvAmnt = LimitsManagerBase.getBDADefaultInvestmentAmount(user.getCurrencyId());
		user.setDefaultAmountValue(defInvAmnt);
		List<BalanceStepPredefValue> predefValues = new ArrayList<>();
		user.setPredefValues(predefValues);
		predefValues.add(new BalanceStepPredefValue(defInvAmnt * BDA_MULTIPLIER_STEP_ONE));
		predefValues.add(new BalanceStepPredefValue(defInvAmnt * BDA_MULTIPLIER_STEP_TWO));
		predefValues.add(new BalanceStepPredefValue(defInvAmnt * BDA_MULTIPLIER_STEP_THREE));
		predefValues.add(new BalanceStepPredefValue(defInvAmnt * BDA_MULTIPLIER_STEP_FOUR));
	}

	/**
	 * @return the logoutDefaultStep
	 * @throws SQLException
	 */
	public static Map<Long, BalanceStep> getLogoutCurrencyDefaultBalanceStep() throws SQLException {
		if (logoutCurrencyDefaultBalanceStep == null || logoutCurrencyDefaultBalanceStep.isEmpty()) {
			Connection con = getConnection();
			logoutCurrencyDefaultBalanceStep = BalanceStepsDAOBase.getCurrencyLogoutDefaultStep(con);
		}
		return logoutCurrencyDefaultBalanceStep;
	}

	/**
	 * @param logoutDefaultStep the logoutDefaultStep to set
	 */
	public static void setLogoutCurrencyDefaultBalanceStep(Map<Long, BalanceStep> setLogoutCurrencyDefaultBalanceStep) {
		UserBalanceStepManager.logoutCurrencyDefaultBalanceStep = setLogoutCurrencyDefaultBalanceStep;
	}
}