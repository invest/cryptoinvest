package com.anyoption.common.managers;

import java.net.InetAddress;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.base.ProvidersIP;
import com.anyoption.common.daos.ProvidersDAO;

/**
 * @author liors
 *
 */
public class ProvidersManager extends BaseBLManager {
	private static final Logger log = Logger.getLogger(ProvidersManager.class);
	private static ArrayList<ProvidersIP> providersIP;
	
	/**
	 * @return
	 * @throws Exception 
	 */
	public static ArrayList<ProvidersIP> loadProvidersIPs() {		
		Connection conn = null;
		try {
			conn = getConnection();
			if (providersIP == null || providersIP.isEmpty()) {
				providersIP = ProvidersDAO.loadProvidersIP(conn); 
			}			
		} catch (Exception e) {
			log.error("ERROR! loadProvidersIP", e);
			providersIP = null;
		} finally {
			closeConnection(conn);
		}
		return providersIP;
	}
	
	/**
	 * @param providerGroup
	 * @param ip
	 * @return
	 * @throws SQLException
	 */
	public static boolean isAllowProviderIPs(int providerGroup, String ip) throws SQLException {
		boolean allowIP = false;
		loadProvidersIPs();
		for (ProvidersIP providers : providersIP) {
			if (providers.getProviderGroupId() == providerGroup && 
					isValidRange(providers.getFromIp(), providers.getToIp(), ip) &&
					providers.isAllow()) {				
				return allowIP = true;
			}
		}	
		return allowIP;
	}
	
	/**
	 * @param ip
	 * @return
	 */
	public static long ipToLong(InetAddress ip) {
		byte[] octets = ip.getAddress();
		long result = 0;
		for (byte octet : octets) {
			result <<= 8;
			result |= octet & 0xff;
		}
		return result;
	}
	
	/**
	 * @param fromIP
	 * @param toIP
	 * @param checkIP
	 * @return
	 */
	public static boolean isValidRange(String fromIP, String toIP, String checkIP) {
		try {
			long ipLow = ipToLong(InetAddress.getByName(fromIP));
			long ipHigh = ipToLong(InetAddress.getByName(toIP));
			long ipToCheck = ipToLong(InetAddress.getByName(checkIP));
			return (ipToCheck >= ipLow && ipToCheck <= ipHigh);
		} catch (Exception e) {
			log.error("ERROR! failed to validate range ip", e);
			return false;
		}
	}
}
