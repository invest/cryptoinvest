/**
 *
 */
package com.anyoption.common.managers;


import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Locale;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import com.anyoption.common.beans.AppsflyerTrade;
import com.anyoption.common.beans.Bonus;
import com.anyoption.common.beans.CompanyProfitBonusWagering;
import com.anyoption.common.beans.ExposureBean;
import com.anyoption.common.beans.Investment;
import com.anyoption.common.beans.InvestmentLimitsGroup;
import com.anyoption.common.beans.InvestmentRejects;
import com.anyoption.common.beans.Market;
import com.anyoption.common.beans.Opportunity;
import com.anyoption.common.beans.OpportunityMiniBean;
import com.anyoption.common.beans.OpportunityOddsType;
import com.anyoption.common.beans.OpportunityType;
import com.anyoption.common.beans.Skin;
import com.anyoption.common.beans.Transaction;
import com.anyoption.common.beans.User;
import com.anyoption.common.beans.ValidatorParamNIOU;
import com.anyoption.common.beans.base.BonusUsers;
import com.anyoption.common.beans.base.DepositBonusBalanceBase;
import com.anyoption.common.beans.base.OpportunityOddsPair;
import com.anyoption.common.bl_vos.InvestmentLimit;
import com.anyoption.common.bl_vos.InvestmentType;
import com.anyoption.common.bonus.BonusHandlerBase;
import com.anyoption.common.bonus.BonusHandlerFactory;
import com.anyoption.common.bonus.BonusHandlersException;
import com.anyoption.common.daos.BonusDAOBase;
import com.anyoption.common.daos.GeneralDAO;
import com.anyoption.common.daos.InvestmentsDAOBase;
import com.anyoption.common.daos.TransactionsDAOBase;
import com.anyoption.common.daos.UsersDAOBase;
import com.anyoption.common.enums.CopyOpInvTypeEnum;
import com.anyoption.common.enums.SkinGroup;
import com.anyoption.common.jms.WebLevelLookupBean;
import com.anyoption.common.jms.WebLevelsCache;
import com.anyoption.common.service.requests.DepositBonusBalanceMethodRequest;
import com.anyoption.common.settlement.DynamicsSettlementHandler;
import com.anyoption.common.settlement.SettlementHandler;
import com.anyoption.common.settlement.SettlementHandlerFactory;
import com.anyoption.common.settlement.SettlementHandlerResult;
import com.anyoption.common.util.BonusFormulaDetails;
import com.anyoption.common.util.CommonUtil;
import com.anyoption.common.util.ConstantsBase;
import com.anyoption.common.util.InvestmentValidatorAbstract;
import com.anyoption.common.util.MessageToFormat;
import com.anyoption.common.util.OpportunityCacheBean;
//import com.copyop.common.dto.CopiedInvestment;
//import com.copyop.common.jms.CopyOpAsyncEventSender;
//import com.copyop.common.jms.events.InvCreateEvent;
//import com.copyop.common.managers.ProfilesOnlineManager;
/**
 * @author pavelhe
 *
 */
public class InvestmentsManagerBase extends BaseBLManager {
	
	private static final Logger log = Logger.getLogger(InvestmentsManagerBase.class);
	private static final float LIMITED_HIGH_AMOUNT_EXPOSURE_COEFFICIENT = .9f;
	public static final int INVESTMENT_TYPE_CALL = 1;
	public static final int INVESTMENT_TYPE_PUT = 2;
	public static final int INVESTMENT_TYPE_ONE = 3;
	public static final int INVESTMENT_TYPE_BUY = 4;
	public static final int INVESTMENT_TYPE_SELL = 5;
	public static final int INVESTMENT_TYPE_DYNAMICS_BUY = 7;
	public static final int INVESTMENT_TYPE_DYNAMICS_SELL = 8;

	 public static MessageToFormat submitDynamicsInvestment(
	            User user,
	            Investment inv,
	            OpportunityCacheBean opportunity,
	            long writerId,
	            String sessionId,
	            WebLevelsCache levelsCache,
	            InvestmentValidatorAbstract investmentValidator,
	            long loginId) {
	        if (log.isDebugEnabled()) {
	            log.debug("try to submit dynamics - " +
	                    " id: " + inv.getOpportunityId() +
	                    " amount: " + inv.getAmount() +
	                    " Time to be Created " + inv.getTimeCreated() +
	                    " pageLevel: " + inv.getCurrentLevel() +
	                    " pageOddsWin: " + inv.getOddsWin() +
	                    " pageOddsLose: " + inv.getOddsLose() +
	                    " choice: " + inv.getTypeId() +
	                    " UTC offset: " + inv.getUtcOffsetCreated() +
	                    " user: " + user.getUserName() +
	                    " sessionId: " + sessionId +
	                    " writerId: " + writerId +
	                    " price " + inv.getPrice() +
	                    " ip: " + inv.getIp());
	        }
	        MessageToFormat msg = null;
	        Connection conn = null;
	        try {
	        	double rate = CurrencyRatesManagerBase.getUSDRate(user.getCurrencyId());
	        	if (rate == 0D) {
	        		log.error("Rejecting investment, because the rate is 0");
	        		msg = new MessageToFormat("error.investment", null);
	        	} else {
		            double convertAmount = (inv.getAmount() / 100 )* rate;
		            long optionPlusFee = inv.getOptionPlusFee();
		            Float  pageOddsLose = (float)inv.getOddsLose();
		            if (opportunity.getOpportunityTypeId() != Opportunity.TYPE_REGULAR) {
		                pageOddsLose = null;
		            }
		            InvestmentRejects invRej = new InvestmentRejects(inv.getOpportunityId(), inv.getCurrentLevel(), sessionId, inv.getAmount(), user.getId(), rate, (float) inv.getOddsWin(), pageOddsLose, (int) inv.getTypeId(), ConstantsBase.OPPORTUNITIES_TYPE_DEFAULT, writerId, inv.getApiExternalUserId());
		            inv.setMarketId(opportunity.getMarketId());

		            conn = getConnection();
		            WebLevelLookupBean wllb = new WebLevelLookupBean();
		            wllb.setSkinGroup(SkinsManagerBase.getSkin(user.getSkinId()).getSkinGroup());	            
		            msg = investmentValidator.validate(conn, user, inv, opportunity, sessionId, levelsCache, wllb, convertAmount, invRej, rate, inv.getAmount());

		            if (null == msg) {
		                inv.setId(
		                		InvestmentsManagerBase.insertInvestment(user,
		                												opportunity,
		                												inv.getAmount() + optionPlusFee,
		                												(int) inv.getTypeId(),
		                												inv.getCurrentLevel(),
		                												inv.getIp(),
		                												wllb.getRealLevel(),
		                												wllb.getDevCheckLevel(),
		                												inv.getUtcOffsetCreated(),
		                												rate,
		                												false,
		                												writerId,
		                												optionPlusFee,
		                												user.getCountryId(),
		                												inv.getApiExternalUserId(),
		                												inv.getOddsWin() - 1,
		                												1 - inv.getOddsLose(),
		                												inv.getCopyopInvId(),
		                												inv.getCopyopType(),
		                												wllb.isClosingFlagClosest(),
		                												-1,
		                												inv.getBubbleStartTime(),
		                												inv.getBubbleEndTime(),
		                												inv.getBubbleLowLevel(),
		                												inv.getBubbleHighLevel(),
		                												loginId,
		                												inv.getPrice()
		                												)
		                		);
		                inv.setTimeCreated(new Date());
		                if (user.getClassId() != ConstantsBase.USER_CLASS_TEST) {
		            		double ifAbove = 0;
		            		double ibBelow = 0;
	            			if (inv.getTypeId() == InvestmentType.DYNAMICS_BUY) {
	            				ifAbove = -1D * (100D - inv.getPrice()) * DynamicsSettlementHandler.getDynamicsNumberOfContracts(inv).doubleValue() * rate * 100D;
	            				ibBelow = convertAmount;
	            			} else {
	            				ifAbove = convertAmount;
	            				ibBelow = -1D * inv.getPrice() * DynamicsSettlementHandler.getDynamicsNumberOfContracts(inv).doubleValue() * rate * 100D;
	            			}
		            		levelsCache.notifyForInvestment(
		            				opportunity.getId(),
		            				inv.getId(),
		            				convertAmount ,
		            				inv.getTypeId(),
									"0",
									"0",
									0l,
									inv.getMarketId(),
									OpportunityType.PRODUCT_TYPE_DYNAMICS,
									user.getSkinId(),
									inv.getCurrentLevel(),
									System.currentTimeMillis(),
									"0",
									"0",
									ifAbove,
									ibBelow,
									0l,
									CopyOpInvTypeEnum.SELF);
		                }
		            }
	        	}
	        } catch (Exception e) {
	            log.error("Failed to insert investment - user: " + user.getUserName() + " sessionId: " + sessionId, e);
	            msg = new MessageToFormat("error.investment", null);
	        } finally {
	            closeConnection(conn);
	        }
	        return msg;
	    }

	/**
	 * @param validator
	 * @param invRej
	 * @return
	 * @throws SQLException
	 */
	public static String validateNIOUTypeWithCashBalance(ValidatorParamNIOU validator, InvestmentRejects invRej) throws SQLException {
		log.info("\nabout to validate NIOU with cash balance: \n " + validator.toString());
		String resultMsg = null;
		Connection conn = null;
		boolean isSelectedDeafultOdds = true;
		boolean isFree = false;
        
		try {
			//NIOU
			if(validator.getOddsWin() != validator.getOppOddsWin() || validator.getOddsLose() != validator.getOppOddsLose()){
	        	isSelectedDeafultOdds =  false;
	        }		
            conn = getConnection();
            String bonusId_typeId = BonusDAOBase.hasNextInvestOnUsWithBonusType(conn, validator.getUser().getId(), isSelectedDeafultOdds);
            String[] tmpArry = bonusId_typeId.split("_");
            long nextInvestOnUsId = Long.parseLong(tmpArry[0]);
            if (Integer.parseInt(tmpArry[0]) > 0) {
            	HashMap<String, Long> freeInvValues = BonusDAOBase.getMinAndMaxFreeInv(conn, nextInvestOnUsId);
            	isFree = freeInvValues.get(ConstantsBase.FREE_INVEST_MIN).longValue() <= validator.getInvAmount() &&
			                freeInvValues.get(ConstantsBase.FREE_INVEST_MAX).longValue() >= validator.getInvAmount() &&
			                validator.getOppTypeId() == Opportunity.TYPE_REGULAR;
            	
            }
            
            if (isFree) {
            	//Cash
                DepositBonusBalanceMethodRequest dbbRequest = new DepositBonusBalanceMethodRequest();
                dbbRequest.setUserId(validator.getUser().getId());
                dbbRequest.setBalanceCallType(DepositBonusBalanceMethodRequest.CALL_TOTAL_AMOUNT_BALANCE);
                DepositBonusBalanceBase dbb = DepositBonusBalanceManagerBase.getDepositBonusBalance(dbbRequest);
                
                if (dbb.getDepositCashBalance() < validator.getInvAmount()) {
                	log.info("\nIt's NIOU but \nvalidate NIOU with cash balance:\n CashBalance: " +  dbb.getDepositCashBalance() + " InvAmount: " + validator.getInvAmount());
                	resultMsg = ConstantsBase.NO_CASH_FOR_NIOU_MSG + "=" + dbb.getDepositCashBalance();
                	invRej.setRejectAdditionalInfo("Insufficient cash balance. cash balance: " + dbb.getDepositCashBalance());
                	invRej.setRejectTypeId(ConstantsBase.INVESTMENT_REJECT_NIOU_NO_CASH_BALANCE);
                	InvestmentRejectsManagerBase.insertInvestmentReject(invRej);
                }   
            }          
		} catch (Exception e) {
			log.error("ERROR! validateNIOUTypeWithCashBalance ", e);
		} finally {
            closeConnection(conn);
        }
		log.info("\ndone validate NIOU with cash balance for: "
				+ validator.toString()
				+ "\nresult (null it's a PASS): " + resultMsg + "\n");
		return resultMsg;
	}
	
	public static Long getOriginalInvestmentUser(long copiedInvId) throws SQLException {
		Connection conn = null;
        try {
            conn = getConnection();
            return InvestmentsDAOBase.getOriginalInvestmentUser(conn, copiedInvId);
        } finally {
            closeConnection(conn);
        }
	}

	/**
     * @param opportunityId
     * @param amount
     * @param typeId
     * @param invRej
     * @param skinGroup
     * @param marketId
     * @param locale
     * @param currencyId
     * @param scheduled
     * @return the error message or null if there is no error
     * @throws SQLException
     */
    public static ExposureBean checkInvestmentExposure(long opportunityId, long invAmount, double convertAmount, long typeId, InvestmentRejects invRej, SkinGroup skinGroup, long marketId, Locale locale, long currencyId, int scheduled, long skinId) throws SQLException {
        Connection conn = null;
        try {
            conn = getConnection();
            ExposureBean exposureBean = InvestmentsDAOBase.getInvestmentExposure(conn, opportunityId, convertAmount, typeId, invRej, skinGroup);
            Double exposure = exposureBean.getExposure();
            Double maxExposure = exposureBean.getMaxExposure();
            if (exposure == null || maxExposure == null) {
            	invRej.setRejectTypeId(ConstantsBase.INVESTMENT_REJECT_EXPOSURE);
                invRej.setRejectAdditionalInfo("DB query did not return exposure record");
                InvestmentRejectsManagerBase.insertInvestmentReject(invRej);
            	log.error("DB query did not return exposure record for opportunity [" + opportunityId + "], skin group [" + skinGroup + "]");
            	exposureBean.setErrorMessage(CommonUtil.getMessage(locale, "error.investment.deviation", null));
                exposureBean.setErrorMessageKey("error.investment.deviation");
            } else if (Math.abs(exposure + ((typeId == Investment.TYPE_BUBBLE || typeId == 1) ? convertAmount : -convertAmount)) > maxExposure) {
                invRej.setRejectTypeId(ConstantsBase.INVESTMENT_REJECT_EXPOSURE);
                invRej.setRejectAdditionalInfo("Exposure Reached:, exposure: " + exposure + " maxExposure: " + maxExposure);
                InvestmentRejectsManagerBase.insertInvestmentReject(invRej);
                exposureBean.setMaxExposureReached(true);
                exposureBean.setErrorMessage(CommonUtil.getMessage(locale, "error.investment.deviation", null));
                exposureBean.setErrorMessageKey("error.investment.deviation");
            } else {
            	Market m = MarketsManagerBase.getMarketLimitFromAmount(marketId);
            	if (m.isLimitedForHighAmounts()) {
            		if (Math.abs(exposure) > maxExposure * LIMITED_HIGH_AMOUNT_EXPOSURE_COEFFICIENT) {
						long investmentHighAmountLimit = m.getLimitedFromAmount() * getExposureHighAmountLimitCoefficient(	conn,
																															InvestmentLimitsGroup.HIGH_EXPOSURE_INVESTMENT_LIMIT,
																															currencyId);
            			if (invAmount > investmentHighAmountLimit) {
            				invRej.setRejectTypeId(ConstantsBase.INVESTMENT_REJECT_HIGH_EXPOSURE_LIMIT);
                            invRej.setRejectAdditionalInfo(LIMITED_HIGH_AMOUNT_EXPOSURE_COEFFICIENT * 100 + "% exposure Reached:, exposure: " + exposure + " maxExposure: " + maxExposure);
                            InvestmentRejectsManagerBase.insertInvestmentReject(invRej);
                            exposureBean.setErrorMessage(CommonUtil.getMessage(locale, "error.investment.exposure.limit", new Object[] {investmentHighAmountLimit / 100}));
                            exposureBean.setErrorMessageKey("error.investment.exposure.limit");
                            exposureBean.setErrorMessageParams(new Object[] {investmentHighAmountLimit / 100});
            			}
            		} else if (Math.abs(exposure + ((typeId == Investment.TYPE_BUBBLE || typeId == 1) ? convertAmount : -convertAmount)) > maxExposure * LIMITED_HIGH_AMOUNT_EXPOSURE_COEFFICIENT) {
            		    String marketsTypeSchedule;
						switch (scheduled) {
						case Opportunity.SCHEDULED_HOURLY:
							marketsTypeSchedule = "Hourly";
							break;

						case Opportunity.SCHEDULED_DAYLY:
							marketsTypeSchedule = "Daily";
							break;

						case Opportunity.SCHEDULED_WEEKLY:
							marketsTypeSchedule = "Weekly";
							break;

						case Opportunity.SCHEDULED_MONTHLY:
							marketsTypeSchedule = "Monthly";
							break;

						case Opportunity.SCHEDULED_QUARTERLY:
							marketsTypeSchedule = "Quarterly";
							break;

						default:
							marketsTypeSchedule = "";
							break;
						}
            		    sendExposureNotifyMail(opportunityId, MarketsManagerBase.getMarketName(Skin.SKIN_REG_EN, marketId), marketsTypeSchedule);
            		}
            	}
            }

            if (log.isDebugEnabled()) {
                log.debug("exposure: " + exposure + " maxExposure: " + maxExposure + " amount: " + convertAmount + " type: " + typeId);
            }
            return exposureBean;
        } finally {
            closeConnection(conn);
        }
    }

	private static long getExposureHighAmountLimitCoefficient(Connection conn, int highExposureInvestmentLimitGroup, long currencyId) {
		try {
			return InvestmentsDAOBase.getExposureHighAmountLimitCoefficient(conn, highExposureInvestmentLimitGroup, currencyId);
		} catch (SQLException e) {
			log.error("Can't get high exposure investment amount limit", e);
			return 0l;
		}
	}

	public static Boolean isValidateOpportunityOddsGroup(String oddsGroup, long pageWin, long pageLose) {
		boolean returnV=false;
		ArrayList<OpportunityOddsPair> returnRefunSelectorStatic = new ArrayList<OpportunityOddsPair>();
		returnRefunSelectorStatic = OpportunitiesManagerBase.getOpportunityOddsPairSelector();

		String[] oddsGroupID=oddsGroup.split(" ");
		for (String groupID : oddsGroupID) {
			if (groupID.contains("d")) {
				groupID = groupID.replace("d", "");
			}
			for (OpportunityOddsPair oddsPair : returnRefunSelectorStatic) {
				if (oddsPair.getSelectorID() == Long.parseLong(groupID)) {
					if ((oddsPair.getReturnSelector() == pageWin)
							&& ((100 - oddsPair.getRefundSelector()) == pageLose)) {
						returnV = true;
					}
				}
			}
		}
		return returnV;
	}

	public static boolean sendD3Alert(long userId, long opportunityId, double rate, int amount) throws SQLException {
		Connection conn = null;
        try {
            conn = getConnection();
            return InvestmentsDAOBase.sendD3Alert(conn, userId, opportunityId, rate, amount);
        } finally {
            closeConnection(conn);
        }
	}
	
//	public static ArrayList<CopiedInvestment> getCopiedInvFromLastLogout(long userId) throws SQLException {				
//		Date lastLogoutnDateTime = ProfilesOnlineManager.getLastLogoutDate(userId);		
//		
//		ArrayList<CopiedInvestment> list = new ArrayList<CopiedInvestment>();
//		if(lastLogoutnDateTime != null){
//			Connection conn = null;
//	        try {
//	        	conn = getConnection();
//	        	list = InvestmentsDAOBase.getCopiedInvFromLastLogout(conn, lastLogoutnDateTime, userId);
//	        } finally {
//	            closeConnection(conn);
//	        }
//		}		
//		return list;
//	}
	
	public static void insertAppsflyerInvestmentHouseResult(int interval, String users) throws SQLException {
		Connection con = null;
		try {
			con = getConnection();
			InvestmentsDAOBase.insertAppsflyerInvestmentHouseResult(con, interval, users);
		} finally {
			closeConnection(con);
		}
	}

	public static HashMap<Long, AppsflyerTrade> getAppsflyerInvestmentHouseResult(int numberOfRecords) throws SQLException {
		HashMap<Long, AppsflyerTrade> list = new HashMap<Long , AppsflyerTrade>();
		Connection con = null;
		try {
			con = getConnection();
			list = InvestmentsDAOBase.getAppsflyerInvestmentHouseResult(con, numberOfRecords);
		} finally {
			closeConnection(con);
		}
		return list;
	}

	public static void updateAppsflyerEventsStatus(ArrayList<Long> idForUpdate) throws SQLException {
		Connection con = null;
		try {
			con = getConnection();
			InvestmentsDAOBase.updateAppsflyerEventsStatus(con, idForUpdate);
		} finally {
			closeConnection(con);
		}
		
	}

	public static void afterInvestmentSuccess(Connection conn, long userId, long balance, long investmentId, long investmentAmount, long writerId, boolean isFree, long opportunityTypeId, long loginId) throws SQLException {
		afterInvestmentSuccessTouchBonuses(conn, userId, balance, opportunityTypeId);
		afterInvestmentSuccessActivate(conn, userId, investmentId, investmentAmount, writerId, isFree, loginId, opportunityTypeId);
		afterInvestmentSuccessWagering(conn, userId, !isFree ? investmentAmount : 0, investmentId, opportunityTypeId);
		UsersManagerBase.updateLastDepositOrInvestDate(conn, userId);
	}
	
	protected static void afterInvestmentSuccessTouchBonuses(Connection conn, long userId, long balance, long opportunityTypeId) throws SQLException {
        long totalBonusAmount = UsersDAOBase.getUserTotalBonusesAmount(conn, userId);
        if (balance < totalBonusAmount) {
            long amountLeft = totalBonusAmount - balance;
            ArrayList<BonusUsers> bonuses = BonusDAOBase.getActiveAndUsed(conn, userId); //no next invest on us
            BonusUsers bu = null;
            for (int i = 0; i < bonuses.size() && amountLeft > 0; i++) {
                bu = bonuses.get(i);

                BonusHandlerBase bh = BonusHandlerFactory.getInstance(bu.getTypeId());

                if (null != bh){
                	try {
                		amountLeft = bh.touchBonusesAfterInvestmentSuccess(conn, bu, amountLeft, opportunityTypeId);
					} catch (BonusHandlersException e) {
						log.error("Error in touchBonusesAfterInvestmentSuccess for user bonus id " + bu.getId(),e);
						throw new SQLException();
					}
                }
            }
        }
    }
	
    /**
     * @param conn
     * @param userId
     * @param investmentAmount
     * @param investmentId
     * @param opportunityTypeId
     * @throws SQLException
     */
    protected static void afterInvestmentSuccessWagering(Connection conn, long userId, long investmentAmount , long investmentId, long opportunityTypeId) throws SQLException {
        ArrayList<BonusUsers> bonuses = BonusDAOBase.getActiveAndUsed(conn, userId); // next invest ?
        BonusUsers bu = null;
        double factor = InvestmentsDAOBase.getTurnoverFactor(conn, opportunityTypeId);
        long amountLeft = BigDecimal.valueOf(investmentAmount * factor).setScale(0, RoundingMode.HALF_UP).longValue();
        long amountLeftBase = amountLeft;
        long amountLeftBefore = amountLeft;

        for (int i = 0; i < bonuses.size()/* && amountLeft > 0*/; i++) { // we want the wagering to be triggered for all bonuses despite the investment amount. The key point is to get NIOU to state done immediately
            bu = bonuses.get(i);
            BonusHandlerBase bh = BonusHandlerFactory.getInstance(bu.getTypeId());
            if (null != bh) {
            	try {           		           		
            		amountLeft = bh.wageringAfterInvestmentSuccess(conn, bu, amountLeft, investmentId);  
            		log.info("afterInvestmentSuccessWagering. \n"
            				+ " ratio: " + amountLeftBefore + " - " + amountLeft + " \\ " + amountLeftBase);
            		if (amountLeftBefore > 0) {
	            		BonusDAOBase.insertBonusWageringInvestments(conn, bu, investmentId, 
	            				BigDecimal.valueOf(amountLeftBefore).subtract(new BigDecimal(amountLeft))
	            				.divide(new BigDecimal(amountLeftBase), 2, RoundingMode.HALF_UP).setScale(2, RoundingMode.HALF_UP).doubleValue(),
	            				amountLeftBefore - amountLeft);            		
	            		amountLeftBefore = amountLeft;
            		}
				} catch (BonusHandlersException e) {
					log.error("Error in wageringAfterInvestmentSuccess for bonus user id " + bu.getId(),e);
					throw new SQLException();
				}
            }
        }
    }
    
	protected static void afterInvestmentSuccessActivate(Connection conn, long userId, long investmentId, long investmentAmount, long writerId, boolean isFree, long loginId, long opportunityTypeId) throws SQLException {
        ArrayList<BonusUsers> bonuses = BonusDAOBase.getBonusesForActivation(conn, userId);
        BonusUsers bu = null;
        long amountLeft = investmentAmount;
        boolean isInvWasCountForActivation = false;
        boolean isAmountUsed;

        for (int i = 0; i < bonuses.size() ; i++) {
            bu = bonuses.get(i);
            BonusHandlerBase bh = BonusHandlerFactory.getInstance(bu.getTypeId());

            if (null != bh){
            	if (amountLeft > 0){
            		try {
						isAmountUsed = bh.activateBonusAfterInvestmentSuccessByInvAmount(	conn, bu, amountLeft, userId, investmentId,
																							writerId, isFree, loginId, opportunityTypeId);
            			if (isAmountUsed){
            				 // Amount decrease should happen whether the bonus got activated ot not.
            		        amountLeft = amountLeft - (bu.getSumInvQualify() - bu.getSumInvQualifyReached());
            			}
    				} catch (BonusHandlersException e) {
    					log.error("Error in activateBonusAfterInvestmentSuccessByInvAmount for bonus user id " + bu.getId(),e);
    					throw new SQLException();
    				}
            	}

            	if (!isInvWasCountForActivation){
            		try {
						isInvWasCountForActivation = bh.activateBonusAfterInvestmentSuccessByInvCount(	conn, bu, isInvWasCountForActivation,
																										investmentId, opportunityTypeId);
    				} catch (BonusHandlersException e) {
    					log.error("Error in activateBonusAfterInvestmentSuccessByInvCount for bonus user id " + bu.getId(),e);
    					throw new SQLException();
    				}
            	}
            }
        }
    }
	
    public static void afterInvestmentSuccess(User user, long balance, long investmentId, long investmentAmount, long writerId, boolean isFree, long opportunityTypeId, long optionPlusFee, int insuranceType, long loginId) throws SQLException {
    	// update one click user field
		if (insuranceType == 0 && (opportunityTypeId == Opportunity.TYPE_REGULAR || opportunityTypeId == Opportunity.TYPE_OPTION_PLUS)) {
			user.setDefaultAmountValue(investmentAmount - optionPlusFee);
		}
    	Connection conn = null;
        try {
            conn = getConnection();
            conn.setAutoCommit(false);
            afterInvestmentSuccess(conn, user.getId(), balance, investmentId, investmentAmount, writerId, isFree, opportunityTypeId, loginId);
            conn.commit();
        } catch (SQLException sqle) {
            try {
                conn.rollback();
            } catch (SQLException sqlie) {
                log.error("Failed to rollback.", sqlie);
            }
            throw sqle;
        } finally {
            try {
                conn.setAutoCommit(true);
            } catch (SQLException sqle) {
                log.error("Can't return connection back to autocommit.", sqle);
            }
            closeConnection(conn);
        }
    }
	
    public static void afterTransactionSuccess(long userId) throws SQLException {
    	Connection conn = null;
        try {
            conn = getConnection();
            conn.setAutoCommit(false);
            afterInvestmentSuccessWagering(conn, userId, 0, 0, Opportunity.TYPE_REGULAR);
            conn.commit();
        } catch (SQLException sqle) {
            try {
                conn.rollback();
            } catch (SQLException sqlie) {
                log.error("Failed to rollback.", sqlie);
            }
            throw sqle;
        } finally {
            try {
                conn.setAutoCommit(true);
            } catch (SQLException sqle) {
                log.error("Can't return connection back to autocommit.", sqle);
            }
            closeConnection(conn);
        }
    }
    
    public static ArrayList<Investment> getInvestmentsByUser(long userId, Date from, Date to, boolean isSettled, Long groupId, Long marketId, long skinId, Integer startRow, Integer pageSize, long writerId, int period) throws SQLException {
        Connection conn = null;
        try {
            conn = getConnection();
            return InvestmentsDAOBase.getByUser(conn, userId, from, to, isSettled, groupId, marketId, skinId, startRow, pageSize, writerId, period);
        } finally {
            closeConnection(conn);
        }
    }

    public static long getSumAllActiveInvestments(long oppId, long userId) {
    	Connection con = null;
    	try {
    		con = getConnection();
    		return InvestmentsDAOBase.getSumAllActiveInvestments(con, oppId, userId);
    	} catch (SQLException e) {
    		log.error("Unable to return sum of all investments", e);
    		return 0l;
    	} finally {
			closeConnection(con);
		}
    }
    
//	public static List<CopiedInvestment> getCopiedInvestmentsForUser(long userId) throws SQLException {
//    	Connection conn = null;
//    
//    	try {
//    		conn = getConnection();
//    		return InvestmentsDAOBase.getCopiedInvestmentsForUser(conn, userId);
//       	} finally {
//    		closeConnection(conn);
//    	}
//    
//    }

    public static void sendExposureNotifyMail(long opportunityId, String marketName, String marketsTypeSchedule){
    	Hashtable<String, String> serverProperties = new Hashtable<String, String>();
		serverProperties.put("url", CommonUtil.getProperty("email.server"));
		serverProperties.put("auth", "true");
		serverProperties.put("user", CommonUtil.getProperty("email.uname"));
		serverProperties.put("pass", CommonUtil.getProperty("email.pass"));

		Hashtable<String, String> emailProperties = new Hashtable<String, String>();

		emailProperties.put("from", CommonUtil.getProperty("email.from"));
		emailProperties.put("to",CommonUtil.getProperty("exposure.mailgroup"));
		emailProperties.put("subject", CommonUtil.getProperty("exposure.90.reach", "Opportunity reached 90% exposure"));
		String body= "Opportunity ID: " + opportunityId + "<br/>" + "Market name: " + marketName + "<br/>" + "Market type: " + marketsTypeSchedule;
		emailProperties.put("body",body);

		CommonUtil.sendEmail(serverProperties, emailProperties, null);
    }
    
    /**
     * BMS.
	 * This method handle bonus management system logic for insert investment phase.
	 * This method calculate the adjusted amount to clean from user balance when needed.
	 * settle investment will complete the mechanism logic
	 * This method don't handle binary0100 logic - there is no binary0100 implementation in this env.
	 *
	 * @param investmentId unique number, generate by investment mechanism
	 * @param userId unique user identifier
	 * @param userBalance after investment
	 * @param investmentAmount amount of user investment expressed in the smallest unit of the specified currency.
	 * @throws SQLException
	 */
	public static void insertBonusInvestment(long investmentId, long userId, long userBalance, long investmentAmount) throws SQLException {
		String ls = System.getProperty("line.separator");
		Connection conn = null;
		try {
        	conn = getConnection();
        	conn.setAutoCommit(false);
        	//userTotalAdjustedAmount: contain sum of adjusted amount that the user have. user can have more than 1 bonus, therefore more than 1 adjusted amount.
            long userTotalAdjustedAmount = BonusDAOBase.getUserTotalAdjustedAmount(conn, userId);
            if (userBalance < userTotalAdjustedAmount) {
        		log.info(ls + "BMS, start insertBonusInvestment:" + ls +
                        "investmentId: " + investmentId + ls +
                        "userId: " + userId + ls +
                        "userBalance: " + userBalance + ls +
                        "investmentAmount: " + investmentAmount + ls);
            	ArrayList<BonusUsers> bonuses = BonusDAOBase.getBonusUsed(conn, userId);
    	        BonusUsers bu = null;
    	        long bonusUsed = userTotalAdjustedAmount - userBalance; //The part of the amount that the user touch the bonus.
    	        long adjustmentAmountUsed = -1;
    	        for (Iterator<BonusUsers> i = bonuses.iterator(); i.hasNext() && bonusUsed > 0 && adjustmentAmountUsed < 0;) {
    	        	bu = i.next();
    	            BonusHandlerBase bh = BonusHandlerFactory.getInstance(bu.getTypeId());
    	            if (bh != null) {
    	            	try {
    	            		adjustmentAmountUsed = bu.getAdjustedAmount() - bonusUsed;
    	            		bh.insertBonusInvestments(conn, bu, investmentId, adjustmentAmountUsed < 0 ? bu.getAdjustedAmount() : bonusUsed,
	            					adjustmentAmountUsed, ConstantsBase.BONUS_INVESTMENTS_TYPE_INSERT_INV);
    	            		bonusUsed = bh.updateAdjustedAmount(conn, bu, adjustmentAmountUsed);
    	                 } catch (BonusHandlersException e) {
    	                	 log.error("BMS, Error in insert for bonus investment manager ", e);
    	                	 throw new SQLException();
    	                 }
    	            }
    			}
    	        conn.commit();
            }
    		log.info(ls + "BMS, end insertBonusInvestment, success:" + ls +
                    "investmentId: " + investmentId + ls);
		} catch (SQLException esql) {
            try {
                conn.rollback();
            } catch (SQLException e_sql) {
                log.error("BMS, Failed to rollback.", e_sql);
            }
            throw esql;
        } finally {
        	try {
                conn.setAutoCommit(true);
            } catch (SQLException esql) {
                log.error("BMS, Can't return connection back to autocommit.", esql);
            }
            closeConnection(conn);
        }
    }

	/**
	 * @param conn
	 * @param bonusUser
	 * @return
	 * @throws SQLException
	 */
	public static boolean isCompanyProfitHigher(Connection conn, BonusUsers bonusUser) throws SQLException {
		boolean isHigher = false;  
	    try {
	    	CompanyProfitBonusWagering profit = InvestmentsDAOBase.getBonusWageringInvestmentCalc(conn, bonusUser);
	    	
	    	if ((profit.getSumInvAmount() * ConstantsBase.BONUS_MANAGEMENT_PROFIT_PERCENT + bonusUser.getBonusAmount())
	    			< profit.getSumHouseResult()) {
	    		isHigher = true;
	    	}
	    } catch (SQLException sqle) {
	    	log.error("BMS Error!!! in isCompanyProfitHigher. ", sqle);
	    	throw sqle;
	    }
	    return isHigher;
	}

	public static boolean hasInvestments(Connection conn, long userId, boolean settled) throws SQLException {
		try {
			return InvestmentsDAOBase.isHasInvestments(conn, userId, settled);
		} catch (SQLException sqle) {
			log.error("Error!!! in hasInvestments. ", sqle);
			throw sqle;
		}
		
	}
	
	public static ArrayList<Long> getBubbleInvestmentsForClose(long addTimeToBubbleEndTime, long addDaysToSearchPeriod, long queryRowLimit) throws SQLException{
		Connection con = null;
		try{
			con = getConnection();
			return InvestmentsDAOBase.getBubbleInvestmentsForClose(con, addTimeToBubbleEndTime, addDaysToSearchPeriod, queryRowLimit);
		}finally{
			closeConnection(con);
		}
	}

	public static void settleInvestment(Connection con, long investmentId, long writerId, long GMInsuranceAmount, Investment inv,
										long optionPlusePrice, int balanceLogCmd, long loginId) throws SQLException {
		if (inv == null) {
			inv = InvestmentsDAOBase.getInvestmentToSettle(con, investmentId, true, false, null);
		}
		SettlementHandler sh = SettlementHandlerFactory.getInstance(inv.getOpportunityTypeId());
		if (sh == null) {
			log.error("No handler! Settlement cannot proceed");
			return;
		}
		User u = UsersManagerBase.getUserById(inv.getUserId());
		SettlementHandlerResult sHRslt = sh.calculate( con, inv, GMInsuranceAmount, optionPlusePrice, u);
		if (sHRslt == null) {
			log.error("There is a problem with the calculation of handler " + sh.toString());
			return;
		}

		boolean isOuterAutoCommit = con.getAutoCommit();
		try {
			if (isOuterAutoCommit) {
        		con.setAutoCommit(false);
        	}
			InvestmentsDAOBase.settleInvestmentUpdateUserBalances(	con, inv.getUserId(), sHRslt.getResult(), sHRslt.getTax(),
																	sHRslt.getWinLose());
			boolean shouldUpdateTimeSettled = inv.getTimeSettled() == null;
			InvestmentsDAOBase.settleInvestment(con, inv.getUserId(), investmentId, sHRslt.getResult(), sHRslt.isVoidBet(),
												GMInsuranceAmount, inv.getWin(), inv.getLose(), inv.getClosingLevel(), inv.getTimeQuoted(),
												new Date(), shouldUpdateTimeSettled);
			GeneralDAO.insertBalanceLog(con, writerId, inv.getUserId(), ConstantsBase.TABLE_INVESTMENTS, inv.getId(), balanceLogCmd,
										u.getUtcOffset());
			sh.handleBonusManagementSystemActions(	con, inv, sHRslt.isWin(), sHRslt.isNextInvestOnUs(), sHRslt.getBonusAmount(),
													sHRslt.getNetResult(), optionPlusePrice, writerId, loginId);
			if (isOuterAutoCommit) {
            	con.commit();
            }
        	sh.handleBalanceLogCmd(balanceLogCmd, inv, sHRslt.getResult(), GMInsuranceAmount, u);
		} catch (Exception e) {
            try {
            	if (isOuterAutoCommit) {
            		con.rollback();
            	}
            } catch (Exception ie) {
                log.error("Can't rollback.", ie);
            }
            SQLException sqle = new SQLException();
            sqle.initCause(e);
            throw sqle;
        } finally {
            try {
            	if (isOuterAutoCommit) {
            		con.setAutoCommit(true);
            	}
            } catch (Exception e) {
                log.error("Can't set back to autocommit.", e);
            }
        }

		sh.handleSMSOnSettlement(con, inv, u, sHRslt, GMInsuranceAmount);
	}

    public static boolean isUpInvestment(Investment i) {
        return i.getOneTouchUpDown() == Investment.INVESTMENT_ONE_TOUCH_UP;
    }

    public static boolean isDownInvestment(Investment i) {
        return i.getOneTouchUpDown() == Investment.INVESTMENT_ONE_TOUCH_DOWN;
    }

    public static boolean getIsOneTouchInv(Investment i) {
    	return i.getTypeId() == Investment.INVESTMENT_TYPE_ONE;
    }

	public static boolean getIsCallOption(Investment i) {
		return (i.getTypeId() == Investment.INVESTMENT_TYPE_CALL || i.getTypeId() == Investment.INVESTMENT_TYPE_DYNAMICS_BUY) ;
	}

    public static boolean isOptionPlusOpportunityTypeId(Investment i) {
        return (i.getOpportunityTypeId() == Opportunity.TYPE_OPTION_PLUS);
    }

	public static boolean isRolledUp(Investment i) {
		return i.getIsCanceled() == 1 && i.getRolledInvId() > 0 ? true : false;
	}

    public static boolean isRollUpBought(Investment i) {
        return i.getReferenceInvestmentId() > 0 && i.getInsuranceAmountRU() > 0 ? true : false;
    }

    public static boolean isGmBought(Investment i) {
        return null != i.getInsuranceFlag() && i.getInsuranceFlag().intValue() == 1 && i.getInsuranceAmountGM() > 0 ? true : false;
    }

	public static long getRefund(Investment i) {
		return i.getAmount() + i.getWin() - i.getLose();
	}

    public static long getBaseRefund(Investment i) {
        return i.getBaseAmount() + i.getBaseWin() - i.getBaseLose();
    }

    public static long getAmountWithFees(Investment i) {
        return i.getAmount() + getInsuranceAmount(i);
    }

    // TODO the optionplusfee wasn't present in mobile, why?
    public static long getAmountWithoutFees(Investment i) {
        return i.getAmount() - i.getInsuranceAmountGM() - i.getInsuranceAmountRU() - i.getOptionPlusFee();
    }

	public static long getBaseAmountWithoutFees(Investment i) {
		return i.getBaseAmount()- Math.round(i.getInsuranceAmountGM() * i.getRate()) - Math.round(i.getInsuranceAmountRU() * i.getRate())
				- Math.round(i.getOptionPlusFee() * i.getRate());
	}

    public static long getInsuranceAmount(Investment i) {
        return i.getInsuranceAmountGM() + i.getInsuranceAmountRU();
    }

	public static long getRefundUp(Investment i) {
		
		if(i.getTypeId() == Investment.INVESTMENT_TYPE_DYNAMICS_BUY) {
			return (new BigDecimal(10000).multiply(DynamicsSettlementHandler.getDynamicsNumberOfContracts(i))).longValue();
		}
		
		if(i.getTypeId() == Investment.INVESTMENT_TYPE_DYNAMICS_SELL) {
			return 0;
		}
		Double win = new Double(getAmountWithoutFees(i) + getAmountWithoutFees(i) * i.getOddsWin());
		Double lose = new Double(getAmountWithoutFees(i) - getAmountWithoutFees(i) * i.getOddsLose());
		if (i.getTypeId() == Investment.INVESTMENT_TYPE_PUT && i.getBonusOddsChangeTypeId() == 1) {  // lose & is free
			return i.getAmount();
		}
		if (i.getTypeId() == Investment.INVESTMENT_TYPE_CALL || (getIsOneTouchInv(i) && isUpInvestment(i))) { // win
			return Math.round(win);
		}
		return Math.round(lose);
	}

	public static long getBaseRefundUp(Investment i) {
		Double win = new Double(getBaseAmountWithoutFees(i) + getBaseAmountWithoutFees(i) * i.getOddsWin());
		Double lose = new Double(getBaseAmountWithoutFees(i) - getBaseAmountWithoutFees(i) * i.getOddsLose());
		if (i.getTypeId() == Investment.INVESTMENT_TYPE_PUT && i.getBonusOddsChangeTypeId() == 1) {
			return i.getBaseAmount();
		}
		if (i.getTypeId() == Investment.INVESTMENT_TYPE_CALL || (getIsOneTouchInv(i) && isUpInvestment(i))) {
			return Math.round(win);
		}
		return Math.round(lose);
	}

	public static long getRefundDown(Investment i) {
		if(i.getTypeId() == Investment.INVESTMENT_TYPE_DYNAMICS_BUY) {
			return 0;			
		}
		
		if(i.getTypeId() == Investment.INVESTMENT_TYPE_DYNAMICS_SELL) {
			return (new BigDecimal(10000).multiply(DynamicsSettlementHandler.getDynamicsNumberOfContracts(i))).longValue();
		}
		
		Double win = new Double(getAmountWithoutFees(i) + getAmountWithoutFees(i) * i.getOddsWin());
		Double lose = new Double(getAmountWithoutFees(i) - getAmountWithoutFees(i) * i.getOddsLose());
		if (i.getTypeId() == Investment.INVESTMENT_TYPE_CALL && i.getBonusOddsChangeTypeId() == 1){ // lose & is free
			return i.getAmount();
		}
		if (i.getTypeId() == Investment.INVESTMENT_TYPE_PUT || (getIsOneTouchInv(i) && isDownInvestment(i))) { // win
			return Math.round(win);
		}
		return Math.round(lose);
	}

	public static long getBaseRefundDown(Investment i) {
		Double win=new Double(getBaseAmountWithoutFees(i) + getBaseAmountWithoutFees(i) * i.getOddsWin());
		Double lose=new Double(getBaseAmountWithoutFees(i) - getBaseAmountWithoutFees(i) * i.getOddsLose());
		if (i.getTypeId() == Investment.INVESTMENT_TYPE_CALL && i.getBonusOddsChangeTypeId() == 1){
			return i.getBaseAmount();
		}
		if (Investment.INVESTMENT_TYPE_PUT == i.getTypeId() || (getIsOneTouchInv(i) && isDownInvestment(i))) {
			return Math.round(win);
		}
		return Math.round(lose);
	}

	public static long getRefundSort(Investment investment) {
		if (investment.getIsCanceled() == 1) {
			return Long.MAX_VALUE;
		}
		return investment.getAmount() + investment.getWin() - investment.getLose();
	}

	public static String getBonusName(Investment i, Locale locale) {
		if (i.getBonusOddsChangeTypeId() > 0) {
			return CommonUtil.getMessage(locale, i.getBonusDisplayName(), null);
		}
		return null;
	}

	public static ArrayList<Investment> getOpenedInvestmentsByUser(long userId, long opportunityId, long typeId, long marketId) throws SQLException {
        ArrayList<Investment> l = null;
        Connection conn = null;
        try {
            conn = getConnection();
            l = InvestmentsDAOBase.getOpenedByUser(conn, userId, opportunityId, typeId, marketId);
        } finally {
            closeConnection(conn);
        }
        return l;
    }
    
    /**
     * Check if this user can make this investment in this opportunity.
     *
     * @param user the user who wants to make investment (the balance should be up to date)
     * @param slipEntry the slip entry of the investment
     * @param pageOddsWin the current odds win displayed on the client page
     * @param pageOddsLose the current odds lose displayed on the client page
     * @param invRej the reject investment detaiis
     * @return <code>null</code> if check is ok else error message.
     * @throws SQLException
     */
    public static MessageToFormat validateOpportunity(User user, OpportunityCacheBean o, OpportunityMiniBean oppMiniBean, double pageOddsWin, double pageOddsLose, InvestmentRejects invRej) throws SQLException {
        if (System.currentTimeMillis() < o.getTimeFirstInvest().getTime()) {
            if (log.isDebugEnabled()) {
                log.debug("Opportunity not opened.");
            }
            return new MessageToFormat("error.investment.notopened", null);
        }
        if (System.currentTimeMillis() > o.getTimeLastInvest().getTime()) {
            if (log.isDebugEnabled()) {
                log.debug("Opportunity expired.");
            }
            return new MessageToFormat("error.investment.expired", null);
        }
        if (o.getOpportunityTypeId() != Opportunity.TYPE_BUBBLES && o.getOpportunityTypeId() != Opportunity.TYPE_DYNAMICS) {
        	OpportunityOddsType oddsType = com.anyoption.common.managers.OpportunitiesManagerBase.getOpportunityOddsType(oppMiniBean.getOddsTypeId());
            long dbWin = Math.round(oddsType.getOverOddsWin() * 100);
            long dbLose = Math.round(oddsType.getOverOddsLose() * 100);
            long pageWin = Math.round((pageOddsWin - 1) * 100);
            long pageLose = Math.round((1 - pageOddsLose) * 100);

			if (o.getOpportunityTypeId() == Opportunity.TYPE_REGULAR) {
				if (!isValidateOpportunityOddsGroup(oppMiniBean.getOddsGroup(), pageWin, pageLose)) {
					if (log.isDebugEnabled()) {
						log.debug("db OddsGroupID: " + oppMiniBean.getOddsGroup() + " page win: " + pageWin + " page lose: " + pageLose);
					}
					invRej.setRejectAdditionalInfo("Odds Change:, dbwin: " + dbWin + " , pagewin: " + pageWin + " , db OddsGroupID: "
													+ oppMiniBean.getOddsGroup());
					invRej.setRejectTypeId(ConstantsBase.INVESTMENT_REJECT_ODDS_CHANGE);
					InvestmentRejectsManagerBase.insertInvestmentReject(invRej);
					return new MessageToFormat("error.investment.odds.change", null);
				}
			} else {
				if (dbWin != pageWin || dbLose != pageLose) {
					if (log.isDebugEnabled()) {
						log.debug("db win: " + dbWin + " db lose: " + dbLose + " page win: " + pageWin + " page lose: " + pageLose);
					}
					invRej.setRejectAdditionalInfo("Odds Change:, dbwin: " + dbWin + " , pagewin: " + pageWin + " , dblose: " + dbLose
													+ " , pagelose: " + pageLose);
					invRej.setRejectTypeId(ConstantsBase.INVESTMENT_REJECT_ODDS_CHANGE);
					InvestmentRejectsManagerBase.insertInvestmentReject(invRej);
					return new MessageToFormat("error.investment.odds.change", null);
				}
			}
        }
        return null;
    }

    /**
     * Insert investment.
     *
     * @param user the user to insert investment to (reloaded by user name)
     * @param level the current level of the opp
     * @param ip the IP from which the invest request came from
     * @param realLevel the current real level
     * @param wwwLevel the level the investment was checked against for acceptable deviation
     * @param utcOffset the UTC offset of the user when he did the investment (eg. GMT+02:00)
     * @param optionPlusFee TODO
     * @param copyopInvId
     * @param bubbleStartTime 
     * @param bubbleEndTime 
     * @param bubbleLowLevel 
     * @param bubbleHighLevel 
     * @param copyopTypeId
     * @return The id of the new investment.
     * @throws SQLException
     */
    public static long insertInvestment(User user,
    										OpportunityCacheBean opportunity,
    										long amount,
    										int choice,
    										double level,
    										String ip,
    										double realLevel,
    										double wwwLevel,
    										String utcOffset,
    										double rate,
    										boolean fromGraph,
    										long writerId,
    										long optionPlusFee,
    										long countryId,
    										long apiExternalUserId,
    										double oddsWin,
    										double oddsLose,
    										long copyopInvId,
    										CopyOpInvTypeEnum copyopType,
    										boolean closingFlagClosest,
    										long destUserId,
    										Date bubbleStartTime,
    										Date bubbleEndTime,
    										double bubbleLowLevel,
    										double bubbleHighLevel,
    										long loginId,
    										double price
    										) throws SQLException {

        if (log.isTraceEnabled()) {
            log.trace("Enter insertInvestment - user: " + user + " level: " + level + " ip: " + ip + " realLevel: " + realLevel + " Rate: " +rate);
        }
        long id = 0;
        Connection conn = getConnection();
        conn.setAutoCommit(false);
        long s = System.currentTimeMillis();
        long isFreeTypeId = 0;
        long niouBuId = 0;
        long userId = user.getId();
        try {
        	BonusUsers niouBuIdAndType = null;
        	if (choice != Investment.TYPE_BUBBLE) {
        		niouBuIdAndType = BonusDAOBase.getNextInvestOnUsBonusUserIdAndType(conn, userId);
        	}
            
            user.setBalance(user.getBalance() - amount);
            UsersDAOBase.addToBalance(conn, user.getId(), -amount);

            if (niouBuIdAndType != null) {
            	isFreeTypeId = niouBuIdAndType.getTypeId();

                //all the next invest on us types should mark as 1
                if (isFreeTypeId == Bonus.TYPE_INSTANT_NEXT_INVEST_ON_US ||
                		isFreeTypeId == Bonus.TYPE_NEXT_INVEST_ON_US_AFTER_WAGERING ||
                		isFreeTypeId == Bonus.TYPE_NEXT_INVEST_ON_US_AFTER_X_INVS) {
    				isFreeTypeId = 1;
                }

                niouBuId = niouBuIdAndType.getId();
                HashMap<String, Long> freeInvValues = BonusDAOBase.getMinAndMaxFreeInv(conn, niouBuId);

            	boolean isFree = freeInvValues.get(ConstantsBase.FREE_INVEST_MIN).longValue() <= amount &&
			                freeInvValues.get(ConstantsBase.FREE_INVEST_MAX).longValue() >= amount &&
			                opportunity.getOpportunityTypeId() == Opportunity.TYPE_REGULAR;

                if (isFree) {
		            isFree = BonusDAOBase.useNextInvestOnUs(conn, niouBuId, amount) > 0;
		        }

                // check again that we realy update the rows in db if not or if its not good amount set the type to 0
                if (!isFree) {
                	isFreeTypeId = 0;
	            }
            }

            id = InvestmentsDAOBase.insertInvestment(conn,
            											userId,
            											opportunity,
            											choice,
            											amount,
            											level,
            											ip,
            											realLevel,
            											wwwLevel,
            											isFreeTypeId,
            											utcOffset,
            											rate,
            											niouBuId,
            											fromGraph,
            											optionPlusFee,
            											writerId,
            											countryId,
            											apiExternalUserId,
            											user.getDefaultAmountValue(),
            											oddsWin,
            											oddsLose,
            											copyopInvId,
            											copyopType.getCode(),
            											closingFlagClosest,
            											bubbleStartTime,
            											bubbleEndTime,
            											bubbleLowLevel,
            											bubbleHighLevel,
            											loginId,
            											price
            											);
            UsersDAOBase.insertBalanceLog(conn, writerId, userId, ConstantsBase.TABLE_INVESTMENTS, id, ConstantsBase.LOG_BALANCE_INSERT_INVESTMENT, utcOffset);
            conn.commit();

            long etime = System.currentTimeMillis();
			log.log(Level.DEBUG, "TIME: insert investment + update user balance " + (etime - s));
        } catch (Throwable t) {
            try {
                conn.rollback();
            } catch (Throwable it) {
                log.error("Can't rollback.", it);
            }
            if (log.isTraceEnabled()) {
                log.trace("Leave insertInvestment with exception.");
            }
            SQLException sqle = new SQLException();
            sqle.initCause(t);
            throw sqle;
        } finally {
            try {
                conn.setAutoCommit(true);
            } catch (Throwable t) {
                log.error("Can't set back to autocommit.", t);
            }
            closeConnection(conn);
        }
        if (log.isTraceEnabled()) {
            log.trace("Leave insertInvestment - id: " + id);
        }

        try {
        	long opportunityTypeId = 0;

        	if (null != opportunity){
    			opportunityTypeId = opportunity.getOpportunityTypeId();
    		}

        	if (opportunityTypeId == Opportunity.TYPE_OPTION_PLUS) {
    		    isFreeTypeId = 0;
    		}

            afterInvestmentSuccess(user, user.getBalance(), id, amount, writerId, isFreeTypeId > 0, opportunityTypeId, optionPlusFee, 0, loginId);
//	            if((CommonUtil.getProperty("copyop.isEnabled").equalsIgnoreCase("true"))) {
//	            	if(opportunityTypeId == Opportunity.TYPE_REGULAR ) {
//            			if(user.getSkinId() != Skin.SKIN_ETRADER && copyopType != CopyOpInvTypeEnum.COPY) {
//		            		CopyOpAsyncEventSender.sendEvent(new InvCreateEvent(opportunity.getId(),
//							            										id,
//							            										choice,
//							            										new BigDecimal(amount).divide(new BigDecimal(100)).doubleValue(),
//							            										level,
//							            										oddsWin,
//							            										oddsLose,
//							            										userId,
//							            										opportunity.getMarketId(),
//							            										rate,
//							            										((float)amount/(amount+user.getBalance())),
//							            										copyopType,
//							            										destUserId, // destination user ID
//							            										Writer.WRITER_ID_COPYOP_MOBILE,
//							            										user.getCurrencyId(),
//							            										opportunity.getTimeLastInvest().getTime(),
//							            										opportunity.getTimeEstClosing().getTime(),
//							            										opportunity.getScheduled()));
//		            										
//		            		log.debug("Sent to copyop:"+id);
//           			}
//	           	}
//	        }
        } catch (Exception e) {
            log.error("Problem processing bonuses after successful investment.", e);
        }
        try {
            insertBonusInvestment(id, userId, user.getBalance(), amount);
        } catch (Exception e) {
            log.error("BMS, Problem processing bonus investments.", e);
        }

        return id;
    }

	public static long getUserWinLose(Investment i) {
		return (i.getWin() - i.getLose())* i.getInvestmentsCount()
				/ ((i.getTypeId() == INVESTMENT_TYPE_BUY || i.getTypeId() == INVESTMENT_TYPE_SELL) && i.getContractsStep() != 0
																																? i.getContractsStep()
																																: 1);
	}

	public static long getBaseUserWinLose(Investment i) {
		return (i.getBaseWin() - i.getBaseLose())* i.getInvestmentsCount()
				/ ((i.getTypeId() == INVESTMENT_TYPE_BUY || i.getTypeId() == INVESTMENT_TYPE_SELL) && i.getContractsStep() != 0
																																? i.getContractsStep()
																																: 1);
	}

	public static double getCurrentLevel0100Total(Investment i) {
		double currentLeve = 0;
		if (i.getTypeId() == InvestmentsManagerBase.INVESTMENT_TYPE_BUY) {
			currentLeve = i.getAmount() / (double) i.getInvestmentsCount();
		} else if (i.getTypeId() == InvestmentsManagerBase.INVESTMENT_TYPE_SELL) {
			currentLeve = (100 - ((i.getAmount() / (double) i.getInvestmentsCount()) / 100)) * 100;
		}
		return currentLeve;
	}
	/**
	 * Returns the count of all opened investments by the user
	 *
	 * @param userId id of the user
	 * @return count of all opened investments by the user
	 */
	public static long getUserOpenedInvestmentsCount(long userId, long writerId) {
		Connection con = null;
		try {
			con = getConnection();
			return InvestmentsDAOBase.getUserOpenedInvestmentsCount(con, userId, writerId);
		} catch (SQLException e) {
			log.debug("Can't get user investments, returning 0", e);
			return 0l;
		} finally {
			closeConnection(con);
		}
	}
	
	public static long  getUserOpenedInvestmentsCount(long userId) {
		Connection con = null;
		try {
			con = getConnection();
			return InvestmentsDAOBase.getUserOpenedInvestmentsCount(con, userId);
		} catch (SQLException e) {
			log.debug("Can't get user investments, returning 0", e);
			return 0l;
		} finally {
			closeConnection(con);
		}
	}
	
	public static ArrayList<Long> getUserOpenedInvestments(long userId, long oppId, long invTypeId) {
		Connection con = null;
		try {
			con = getConnection();
			if(invTypeId != 0 ) {
				return InvestmentsDAOBase.getUserOpenedInvestments(con, userId, oppId, invTypeId);
			} else {
				return InvestmentsDAOBase.getUserOpenedInvestments(con, userId, oppId);
			}
		} catch (SQLException e) {
			log.error("Can't get user investments", e);
			return null;
		} finally {
			closeConnection(con);
		}
	}	
	
	
	public static Investment getById(long investmentId) throws SQLException {
		Connection con = null;
		try {
			con = getConnection();
			return InvestmentsDAOBase.getById(con, investmentId);
		} finally {
			closeConnection(con);
		}

	}
	
	public static boolean sellDynamicsInvestment(long investmentId, long amountToSell, double offer, double bid, long writerId, long loginId, User user, Double closingLevel, WebLevelsCache wlc) {
		Connection connection = null;
		try {
				log.debug("Sell Dynamics Investment: InvestmentId: " + investmentId + " bid: " + bid + " offer: " + offer);
	            connection = getConnection();
				connection.setAutoCommit(false);
		
		        Investment investmentToSell = InvestmentsDAOBase.getById(connection, investmentId);
		        // validate price is not zero
		        if(investmentToSell.getTypeId() == Investment.INVESTMENT_TYPE_DYNAMICS_BUY ) {
		        	if( bid == 0 ) { 
		        		log.error("ZERO BID FOR BUY Investment: " + investmentId + " bid: " + bid );
		        		return false;
		        	}
		        }
		        
		        if(investmentToSell.getTypeId() == Investment.INVESTMENT_TYPE_DYNAMICS_SELL ) {
		        	if((100 - offer) == 0 || offer == 0) {
		        		log.error("ZERO OFFER FOR SELL Investment: " + investmentId + " offer: " + offer+" (100-offer): " + (100 - offer));
		        		return false;
		        	}
		        }
		        // initial amount if this is partial close
		        long initialAmount = investmentToSell.getAmount() / 100 ;
				if(amountToSell == 0 ) {
					amountToSell = initialAmount;
				}

		        long parentInvestmentId = investmentToSell.getId();
		        if(investmentToSell.getReferenceInvestmentId() != 0) {
		        	Investment parentInv = InvestmentsDAOBase.getById(connection, investmentToSell.getReferenceInvestmentId());
		        	initialAmount  = parentInv.getAmount();
		        	parentInvestmentId = parentInv.getId();
		        }

		        if(amountToSell > initialAmount) {
		        	log.error("Trying to sell more than initial investment ! requested amount:"+amountToSell + " initialAmount:" + initialAmount + " invID:"+investmentId);
		        	return false;
		        }
		        String utcoffset = UsersDAOBase.getUtcOffset(connection, user.getId());
		        
		        investmentToSell.setClosingLevel(closingLevel);
		        
		        Investment settledInvestment = investmentToSell;
		        long settledInvestmentId = investmentToSell.getId();
		        // if the amount is less the original amount - make partial sell
		        if( amountToSell < initialAmount ) {
			        long amountToOpen = initialAmount - amountToSell;
	
			        InvestmentsDAOBase.cancelInvestment(connection, investmentId, writerId, false);
			        boolean isLikeHourly = true;

	        		InvestmentsDAOBase.insertInvestment(	
	        												connection,//Connection conn,
	        												user.getId(),//long userId,
	        												investmentToSell.getOpportunityId(),//long oppId,
	        												investmentToSell.getTypeId(),//long choice,
	        												amountToOpen,//long amount,
	        												investmentToSell.getCurrentLevel(),//double level,
	        												user.getIp(),//String ip,
	        												investmentToSell.getWwwLevel().doubleValue(),//double realLevel,
	        												investmentToSell.getWwwLevel().doubleValue(),//double wwwLevel,
	        												investmentToSell.getBonusOddsChangeTypeId(),//long bonusOddsChangeTypeId,
	        												utcoffset,//String utcOffsetCreated,
	        												investmentToSell.getRate(),//double rate,
	        												investmentToSell.getBonusUserId(),//long bonusUsersId,
	        												parentInvestmentId,//long referenceInvestmentId,
															isLikeHourly,
            												loginId,
            												writerId
            											);
			        
			        
			        settledInvestmentId =
			        		InvestmentsDAOBase.insertInvestment(	
			        												connection,//Connection conn,
			        												user.getId(),//long userId,
			        												investmentToSell.getOpportunityId(),//long oppId,
			        												investmentToSell.getTypeId(),//long choice,
			        												amountToSell,//long amount,
			        												investmentToSell.getCurrentLevel(),//double level,
			        												user.getIp(),//String ip,
			        												investmentToSell.getWwwLevel().doubleValue(),//double realLevel,
			        												investmentToSell.getWwwLevel().doubleValue(),//double wwwLevel,
			        												investmentToSell.getBonusOddsChangeTypeId(),//long bonusOddsChangeTypeId,
			        												utcoffset,//String utcOffsetCreated,
			        												investmentToSell.getRate(),//double rate,
			        												investmentToSell.getBonusUserId(),//long bonusUsersId,
			        												parentInvestmentId,//long referenceInvestmentId,
																	isLikeHourly,
	                												loginId,
	                												writerId
	                											);
			        settledInvestment.setId(settledInvestmentId);
			        settledInvestment.setAmount(amountToSell);

		        }
		        double quote = bid; // Investment.INVESTMENT_TYPE_DYNAMICS_BUY
		        if(investmentToSell.getTypeId() == Investment.INVESTMENT_TYPE_DYNAMICS_SELL ) {
		        	quote = (100 - offer) ;
		        }
		        
		        BigDecimal numberOfContracts = DynamicsSettlementHandler.getDynamicsNumberOfContracts(investmentToSell);
		        BigDecimal price = new BigDecimal(quote).multiply(numberOfContracts).setScale(2, RoundingMode.HALF_UP).multiply(new BigDecimal(100)).setScale(2, RoundingMode.HALF_UP) ;
		        
		        settleInvestment(connection, settledInvestmentId, writerId, 0l, settledInvestment, price.longValue(), ConstantsBase.LOG_BALANCE_SETTLE_INVESTMENT, loginId);
		        connection.commit();
		        
				if (user.getClassId() != ConstantsBase.USER_CLASS_TEST) {
                	
                	double rate = CurrencyRatesManagerBase.getUSDRate(user.getCurrencyId());
    	        	if (rate == 0D) {
    	        		log.error("Will not notify level service because the rate is 0");
    	        	} 
    	        	double convertAmount = investmentToSell.getAmount() * rate;
            		wlc.notifyForInvestment(
            									investmentToSell.getOpportunityId(),
							            		-numberOfContracts.doubleValue(),
							            		-(double)(convertAmount / quote) / 100,
							            		investmentToSell.getTypeId(),
					    						"0",
					    						"0",
					    						0l,
					    						investmentToSell.getMarketId(),
					    						OpportunityType.PRODUCT_TYPE_DYNAMICS,
					    						user.getSkinId(),
					    						investmentToSell.getCurrentLevel(),
					    						System.currentTimeMillis(),
					    						"0",
					    						"0",
					    						investmentToSell.getTypeId() == Investment.INVESTMENT_TYPE_DYNAMICS_BUY ? -numberOfContracts.doubleValue() : 0, // aboveTotal
					    						investmentToSell.getTypeId() == Investment.INVESTMENT_TYPE_DYNAMICS_SELL ? 0 : -numberOfContracts.doubleValue(), // belowTotal
												0l,
												CopyOpInvTypeEnum.SELF
											);
		        }

				log.debug("Sell Dynamics Investment done");
				return true;
		} catch(SQLException e) {
			try {
				connection.rollback();
			} catch (SQLException e1) {
				log.error("rollback error :", e);
			}
			log.error("Unavble to sell dynamics investment:", e);
			return false;
		} finally {
			setAutoCommitBack(connection);
			closeConnection(connection);
		}

	}


	/**
	 * This method grants a bonus transaction to the user
	 * 
	 * @param con - connection to the DB
	 * @param i - investment to cancel
	 * @return - succeed
	 * @throws SQLException
	 */
	public static void grantBonus(	Connection con, com.anyoption.common.beans.Investment investment, long writerId, long amount,
									long loginId) throws SQLException {
		Transaction bonus = getBonusTransaction(investment, amount, loginId);
		// add bonus transaction to user
		TransactionsDAOBase.insertFromService(con, bonus);
		if (log.isEnabledFor(Level.INFO)) {
			String ls = System.getProperty("line.separator");
			log.log(Level.INFO, "Bonus transaction inserted. TransactionId: " + bonus.getId() + " Amount: " + bonus.getAmount() + ls);
		}
		UsersDAOBase.addToBalance(con, bonus.getUserId(), bonus.getAmount());
		String table = ConstantsBase.TABLE_TRANSACTIONS;
		int command = ConstantsBase.LOG_BALANCE_BONUS_DEPOSIT;
		GeneralDAO.insertBalanceLog(con, writerId, investment.getUserId(), table, bonus.getId(), command, bonus.getUtcOffsetCreated());
		String tlog = "Bonus was granted to User.";
		if (log.isEnabledFor(Level.DEBUG)) {
			String ls = System.getProperty("line.separator");
			log.log(Level.DEBUG, tlog + ls);
		}
	}

	/**
	 * This method receives an investment and create a bonus transaction according to that investment.
	 * 
	 * @param i - the investment to create a transaction (towards the customer) upon
	 * @return
	 */
	private static Transaction getBonusTransaction(Investment i, long amount, long loginId) {
		Transaction bonus = new Transaction();
		bonus.setUserId(i.getUserId());
		bonus.setAmount(amount);
		bonus.setTypeId(TransactionsManagerBase.TRANS_TYPE_BONUS_DEPOSIT);
		bonus.setStatusId(TransactionsManagerBase.TRANS_STATUS_SUCCEED);
		bonus.setCreditCardId(null);
		bonus.setComments("investment id: " + i.getId());
		bonus.setDescription("log.balance.bonus.deposit");
		bonus.setIp("IP NOT FOUND!");
		bonus.setChequeId(null);
		bonus.setTimeSettled(new Date());
		bonus.setTimeCreated(new Date());
		bonus.setUtcOffsetCreated(i.getUtcOffsetSettled());
		bonus.setUtcOffsetSettled(i.getUtcOffsetSettled());
		bonus.setWriterId(i.getWriterId());
		bonus.setWireId(null);
		bonus.setChargeBackId(null);
		bonus.setBonusUserId(i.getBonusUserId());
		bonus.setLoginId(loginId);
		return bonus;
	}

	public static long getOneTouchInvestmentUpDown(Connection con, long investmentId) throws SQLException {
		return InvestmentsDAOBase.getOneTouchInvestmentUpDown(con, investmentId);
	}

	public static int getBinary0100NumberOfContracts(Investment i) {
		int numOfContracts = (int) Math.round((i.getAmount() - i.getOptionPlusFee()) / (i.getCurrentLevel() * 100));
		if (numOfContracts == 0) {
			numOfContracts = 1;
		}
		return numOfContracts;
	}
	
	public static InvestmentLimit getUserLimit(long userId, long userCurrencyId, long opportunityTypeId) {
    	Connection conn = null;
    	try {
    		conn = getConnection();
    		return InvestmentsDAOBase.getInvestmentLimit(conn, opportunityTypeId, 0, 0, userCurrencyId, userId);
       	} catch (SQLException e) {
       		log.debug("user limit :", e);
       		return null;
		} finally {
    		closeConnection(conn);
    	}
	}

	public static boolean userCancelInvestment(Investment i, long writerId, WebLevelsCache wlc, User user, long cancelTimeCumulativeMilis, boolean waiveTax) {
		log.debug("User cancel Investment: InvestmentId:" + i.getId() + " userId:" + user.getId());
		
		long timePassed = Calendar.getInstance().getTime().getTime() - i.getTimeCreated().getTime() ;
		if(timePassed > cancelTimeCumulativeMilis ) {
			log.debug("User cancel time's up:" + timePassed);
			return false;
		}
		
		Connection con = null;
		try {
			if (i.getIsCanceled() == 1) {
				return false;
			}
	
			if (user.getId() > 0 && i.getUserId() != user.getId()) {
				log.warn("Investment user id is different than the given user id, investment user id = " + i.getUserId() + ", user id : " + user.getId());
				return false;
			}
			
			if (i.getIsSettled() == 1) {
				log.debug("User cancel Investment: settled" + i.getId());
				return false;
			}

			BigDecimal bd = new BigDecimal(i.getAmount() - i.getOptionPlusFee());
			long tax = bd.divide(new BigDecimal(10), 2, RoundingMode.HALF_UP).longValue();

			if(waiveTax) {
				i.setLose(0);
			} else {
				i.setLose(tax);
			}
			
			con = getConnection();
			con.setAutoCommit(false);

			// reverse niou bonus
			if (BonusManagerBase.reverseNIOUBonus(con, i.getId())) {
				log.info("NIOU bonus reversed");
			} else {
				log.info("NIOU bonus not reversed");
			}

			//reverse bonus wagering		
			ArrayList<BonusFormulaDetails> bonusFormulaD = BonusDAOBase.getBonusAmounts(con, i.getId(), false);
			long adjustedAmount;
			BonusDAOBase.reverseBonusWagering(con, i.getId(), writerId, user.getId());
			
			// update balance
			UsersDAOBase.addToBalance(con, i.getUserId(), i.getAmount()-i.getLose());
			// update investment status
			InvestmentsDAOBase.cancelInvestment(con, i.getId(), writerId, false);
			BigDecimal taxBd = new BigDecimal(tax);
			double cancelReturnPercentage = 1 - taxBd.divide(bd, 2, RoundingMode.HALF_UP).doubleValue();
			if (!bonusFormulaD.isEmpty()) {
				for (BonusFormulaDetails item : bonusFormulaD) {
					log.debug("bonus amount = " + item.getAmount());
					long returnAmount = new BigDecimal(item.getAmount() * cancelReturnPercentage).longValue();
					adjustedAmount = item.getAdjustedAmount() + returnAmount;
					log.debug("new adjustedAmount = " + adjustedAmount);
					BonusDAOBase.updateAdjustedAmount(con, item.getBonusUsersId(), adjustedAmount);
					BonusDAOBase.insertBonusInvestments(con, i.getId(), item.getBonusUsersId(), returnAmount, adjustedAmount,
														ConstantsBase.BONUS_INVESTMENTS_TYPE_SETTLED_INV);
				}
			}
			
			// update investment
			InvestmentsDAOBase.updateInvestmentLose(con, i.getId(), tax);
			// notify level service
			if (user.getClassId() != ConstantsBase.USER_CLASS_TEST) {
	        	wlc.notifyForInvestment(    i.getOpportunityId(),
	        								i.getId(),
				        					(-1d) * i.getAmount() * i.getRate(),
				        					i.getTypeId()
	        							);
			}
	
			//update balance history
			String table = ConstantsBase.TABLE_INVESTMENTS;
			int	command = ConstantsBase.LOG_BALANCE_CANCEL_INVESTMENT;
			String utcoffset =UsersDAOBase.getUtcOffset(con, i.getUserId());
			GeneralDAO.insertBalanceLog(con, writerId, i.getUserId(), table, i.getId(), command, utcoffset);
	
			// Loyalty reverse points
			// TODO - merge TiersDAOBase, TierUserHistory
			//	long investPoints = TiersDAOBase.getInvetPointsByInvestmentId(con, i.getId());
			//	if (investPoints > 0) {
			//		log.debug("Reverse LoyaltyPoints, investPoints: " +investPoints);
			//		TierUser tierU = TiersDAOBase.getTierUserByUserId(con, i.getUserId());
			//		long updatedPoints = tierU.getPoints() - investPoints;
			//		log.debug("Going to update tierUser points, currentPoints: " + tierU.getPoints() + ", " +
			//					"UpdatedPoints: " + updatedPoints);
			//		TiersManagerBase.updateTierUser(con, tierU.getId(), writerId, updatedPoints);
			//    	TierUserHistory h = TiersManagerBase.getTierUserHisIns(tierU.getTierId(), i.getUserId(), TiersManagerBase.TIER_ACTION_REVERSE_POINTS,
			//   							writer,	updatedPoints, -investPoints, i.getId(), ConstantsBase.TABLE_INVESTMENTS, utcoffset);
			//		log.debug("Going to insert reverse action, " + h.toString());
			//    	TiersDAOBase.insertIntoTierHistory(con, h);
			//	}
			con.commit();
			log.debug("Cancel Investment finished successfully.");
			return true;
    	} catch (SQLException e) {
       		log.error("Error in userCancelInvestment :", e);
       		rollback(con);
       		return false;
		} finally {
			setAutoCommitBack(con);
    		closeConnection(con);
    	}
	}

	public static boolean investmentIsCancelled(long investmentId) {
    	Connection conn = null;
    	try {
    		conn = getConnection();
    		return InvestmentsDAOBase.investmentIsCancelled(conn, investmentId);
       	} catch (SQLException e) {
       		log.error("is cancelled :", e);
       		return false;
		} finally {
    		closeConnection(conn);
    	}
	}
	
	public static boolean updateBubblesClosingLevel(long investmentId, double closingLevel) throws SQLException {
		Connection conn = null;
    	try {
    		conn = getConnection();
    		return InvestmentsDAOBase.updateBubblesClosingLevel(conn, investmentId, closingLevel);
       	} catch (SQLException e) {
       		log.error("updateBubblesClosingLevel failed :", e);
       		return false;
		} finally {
    		closeConnection(conn);
    	}
	}

	/**
	 * Validates the type of the opportunity against the given valid opportunity types. The type should be validated, because an investment
	 * may be submitted with wrong opportunity type, for instance bubble investment with regular(binary) opportunity type id. The method
	 * receives an array of long values for {@code validOpportunityTypeIds}, because some investments are made through common logic(binary
	 * and option+). Returns {@code true} if {@code validOpportunityTypeIds} contains the type of the given opportunity.
	 * 
	 * @param opportunity the opportunity that should be validated
	 * @param validOpportunityTypeIds array of the valid opportunity types
	 * @return {@code true} if the type of the given opportunity is contained in {@code validOpportunityTypeIds}, {@code false} otherwise
	 */
	public static boolean validateOpportunityType(Opportunity opportunity, long... validOpportunityTypeIds) {
		return validateOpportunityType(opportunity.getOpportunityTypeId(), validOpportunityTypeIds);
	}

	/**
	 * Validates the type of the opportunity against the given valid opportunity types. The type should be validated, because an investment
	 * may be submitted with wrong opportunity type, for instance bubble investment with regular(binary) opportunity type id. The method
	 * receives an array of long values for {@code validOpportunityTypeIds}, because some investments are made through common logic(binary
	 * and option+). Returns {@code true} if {@code validOpportunityTypeIds} contains the type of the given opportunity.
	 * 
	 * @param opportunity the opportunity that should be validated
	 * @param validOpportunityTypeIds array of the valid opportunity types
	 * @return {@code true} if the type of the given opportunity is contained in {@code validOpportunityTypeIds}, {@code false} otherwise
	 */
	public static boolean validateOpportunityType(OpportunityCacheBean opportunity, long... validOpportunityTypeIds) {
		return validateOpportunityType(opportunity.getOpportunityTypeId(), validOpportunityTypeIds);
	}

	private static boolean validateOpportunityType(long opportunityTypeId, long... validOpportunityTypeIds) {
		for (long validOpportunityTypeId : validOpportunityTypeIds) {
			if (opportunityTypeId == validOpportunityTypeId) {
				return true;
			}
		}
		log.error("Validate opportunity type failed. Given opportunity has type id of "	+ opportunityTypeId
					+ ", but trying to manipulate it as " + validOpportunityTypeIds);
		return false;
	}

	/**
	 * Validates the current(page) level of the investment. The level should be above 0 to be valid.
	 * 
	 * @param currentLevel the current(page) level of the investment
	 * @return {@code true} if the level is not 0, {@code false} otherwise
	 */
	public static boolean validateCurrentLevel(double currentLevel) {
		if (currentLevel > 0) {
			return true;
		}
		return false;
	}
}