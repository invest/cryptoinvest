package com.anyoption.common.managers;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.RewardTier;
import com.anyoption.common.beans.RewardUser;
import com.anyoption.common.daos.RewardDAO;

/**
 * @author liors
 *
 */
public class RewardManager extends BaseBLManager {
	private static final Logger logger = Logger.getLogger(RewardManager.class);
	public static final int TASK = 1;
	public static final int BENEFIT = 2;
	
	public static HashMap<Integer, RewardTier> getTiers() throws SQLException {
		Connection connection = getConnection();
		try {
			return RewardDAO.getTiers(connection);
		} finally {
			closeConnection(connection);
		}
	}
	
	/**
	 * Get tier id by experience points 
	 * @param experiencePoints
	 * @return
	 * @throws SQLException
	 */
	public static int getTierIdByExperiencePoints(double experiencePoints) throws SQLException {
		Connection connection = getConnection();
		try {
			return RewardDAO.getTierIdByExperiencePoints(connection, experiencePoints);
		} finally {
			closeConnection(connection);
		}
	}
	
	/**
	 * Get Reward User After Tier Change if null there is no need to change tier
	 * @param userId
	 * @return
	 * @throws SQLException
	 */
	public static RewardUser getRewardUserAfterTierChange(long userId) throws SQLException {
		Connection connection = getConnection();
		try {
			return RewardDAO.getRewardUserAfterTierChange(connection, userId);
		} finally {
			closeConnection(connection);
		}	
	}
	
	/**
	 * Get Reward Users After Tier Change
	 * @return
	 * @throws SQLException
	 */
	public static ArrayList<RewardUser> getRewardUsersAfterTierChange() throws SQLException {
		Connection connection = getConnection();
		try {
			return RewardDAO.getRewardUsersAfterTierChange(connection); 
		} finally {
			closeConnection(connection);
		}	
	}
	
	/**
	 * Check whether the customer has this benefit
	 * @param rwdBenefitId
	 * @param userId
	 * @return true if the customer has this benefit
	 * @throws SQLException
	 */
	public static boolean isHasBenefit(String rwdBenefitsId, long userId) throws SQLException {
		Connection connection = getConnection();
		try {
			return RewardDAO.isHasBenefit(connection, rwdBenefitsId, userId);
		} finally {
			closeConnection(connection);
		}
	}
}
