/**
 *
 */
package com.anyoption.common.managers;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.Issue;
import com.anyoption.common.beans.IssueAction;
import com.anyoption.common.beans.Transaction;
import com.anyoption.common.beans.Writer;
import com.anyoption.common.beans.base.User;
import com.anyoption.common.clearing.ClearingManagerConstants;
import com.anyoption.common.util.ConstantsBase;


public class RiskAlertsManagerCommonBase extends BaseBLManager {
    
	private static final Logger logger = Logger.getLogger(RiskAlertsManagerCommonBase.class);
	private static HashMap<Long, ArrayList<String>> providerStolenErrCodes;
	private static ArrayList<String> stolenComments;
	
	private static final long FROM_ISSUERES=1;
	private static final long FROM_WIRECARD=2;
	private static final long FROM_CREDIT_GUARD=3;
    
    public static boolean isCreditCardStolen(String errorCode, Transaction tran){		
		try {
	    	//Check for all providers
	    	if(getProviderStolenErrCodes().get(FROM_ISSUERES) != null && 
					getProviderStolenErrCodes().get(FROM_ISSUERES).contains(errorCode)){
				logger.debug("Stolen Credit Cards with transactionId:" + tran.getId() + " and errorCode:" + errorCode);
				return true;
			} 

	    	//Check for WireCard
	    	if((tran.getClearingProviderId() == ClearingManagerConstants.WC_PROVIDER_ID ||  tran.getClearingProviderId() == ClearingManagerConstants.WC_PROVIDER_ID)
	    			&& getProviderStolenErrCodes().get(FROM_WIRECARD) != null && 
	    				getProviderStolenErrCodes().get(FROM_WIRECARD).contains(errorCode)) {
	    		logger.debug("Stolen Wire Credit Cards with transactionId:" + tran.getId() + " and errorCode:" + errorCode);
	    		return true;
			}
	    	
	    	//Check without WireCard and Inatec
	    	if((tran.getClearingProviderId() != ClearingManagerConstants.WC_PROVIDER_ID 
	    			||  tran.getClearingProviderId() != ClearingManagerConstants.WC_PROVIDER_ID
	    				||  tran.getClearingProviderId() != ClearingManagerConstants.INATEC_PC21
	    					||  tran.getClearingProviderId() != ClearingManagerConstants.INATEC_OUROBOROS_CC)
	    			&& getProviderStolenErrCodes().get(FROM_CREDIT_GUARD) != null && 
	    				getProviderStolenErrCodes().get(FROM_CREDIT_GUARD).contains(errorCode)) {
	    		logger.debug("Stolen Wire Credit Cards with transactionId:" + tran.getId() + " and errorCode:" + errorCode);
	    		return true;
			}
				
	    	for (String comment : getStolenComments()) {
	    		if(tran.getComments() != null && tran.getComments().toUpperCase().contains(comment.toUpperCase())){
	    			logger.debug("Stolen Credit Cards with transactionId:" + tran.getId() + " and comment:" + comment);
	    			return true;
	    		}
	    	}							

		} catch (Exception e) {
			logger.error("Can't check isStolen", e);
		}
		return false;
    }

	public static HashMap<Long, ArrayList<String>> getProviderStolenErrCodes() {
		if(providerStolenErrCodes == null){
			try {
				providerStolenErrCodes = CreditCardsManagerBase.getCCStolenProviderErrCodes();
			} catch (SQLException e) {
				logger.error("Can't get providerStolenErrCodes", e);
			}
		}
		return providerStolenErrCodes;
	}

	public static ArrayList<String> getStolenComments() {
		if(stolenComments == null){
			stolenComments = new ArrayList<>();
			stolenComments.add("pickup");
			stolenComments.add("pick up");
			stolenComments.add("retain");
			stolenComments.add("hot card");
			stolenComments.add("manipulation");
			stolenComments.add("fraud");
			stolenComments.add("stolen");
			stolenComments.add("confiscate");			
		}
		return stolenComments;
	}
	
    public static void createIssueForRegulation(User user, long creditCardId, boolean isMainRegulationIssue)
			throws SQLException {
    	Connection con = null;
    	try {
    		con = getConnection();
    		createIssueForRegulation(con, user, creditCardId, isMainRegulationIssue);
    	}finally {
    		closeConnection(con);
    	}
	}
	
	public static void createIssueForRegulation(Connection conn, User user, long creditCardId, boolean isMainRegulationIssue) {
		try {
			
		    Issue issue = null;
		    IssueAction issueAction = null;
		    
			issue = new Issue();
			issue.setUserId(user.getId());
			issue.setStatusId(Integer.toString(IssuesManagerBase.ISSUE_STATUS_ACTION_REQUIRED));
			issue.setPriorityId(Integer.toString(IssuesManagerBase.ISSUE_PRIORITY_LOW));
			issue.setSubjectId(Integer.toString(IssuesManagerBase.ISSUE_SUBJECT_REG_DOC));
			issue.setMainRegulationIssue(isMainRegulationIssue);
		   
	
			issueAction = new IssueAction();
			issueAction.setActionTypeId(String.valueOf(IssuesManagerBase.ISSUE_ACTION_PENDING_DOC));
			issueAction.setChannelId(String.valueOf(IssuesManagerBase.ISSUE_CHANNEL_ACTIVITY));
			issueAction.setSignificant(false);
			issueAction.setComments("Please ask for docs");
			issueAction.setRiskReasonId(Integer.toString(IssuesManagerBase.ISSUE_REASON_TYPE_REG_DOC));
			IssuesManagerBase.insertIssue(issue, issueAction, ConstantsBase.OFFSET_GMT, Writer.WRITER_ID_MOBILE, user, 0, creditCardId, conn);
		   
      
			logger.info(" Successfully insert regulation document need issue for user id: "
					+ user.getId());
		} catch (Exception e) {
			logger.error(
					"couldn't insert regulation document need issue for user id: "
							+ user.getId(), e);
		}
	}
	
}
