package com.anyoption.common.managers;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.JmxServerConfig;
import com.anyoption.common.daos.JmxServerConfigDAOBase;
import com.anyoption.common.util.CommonUtil;

/**
 * @author EranL
 *
 */
public class JmxServerConfigManager extends BaseBLManager {	
	private static final Logger log = Logger.getLogger(JmxServerConfigManager.class);

	
    public static ArrayList<JmxServerConfig> getJmxServersConfig() throws SQLException {    	
    	Connection con = null;
    	ArrayList<JmxServerConfig> list = new ArrayList<JmxServerConfig>();;
        try {
        	if(CommonUtil.getServerNameUnix().contains("Local")){
        		JmxServerConfig jmxServerConfig = new JmxServerConfig();
        		jmxServerConfig.setId(0L);
        		jmxServerConfig.setAddress("localhost");
        		jmxServerConfig.setPort(8181);
        		list.add(jmxServerConfig);
        	} else {
            	con = getConnection();
            	list = JmxServerConfigDAOBase.getJmxServersConfig(con);
        	}
        	return list;            
        } finally {
            closeConnection(con);
        }
    }
    
}