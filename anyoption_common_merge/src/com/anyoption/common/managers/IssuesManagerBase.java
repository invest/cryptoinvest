/**
 * 
 */
package com.anyoption.common.managers;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashSet;
import java.util.List;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import com.anyoption.common.beans.File;
import com.anyoption.common.beans.FileType;
import com.anyoption.common.beans.Issue;
import com.anyoption.common.beans.IssueAction;
import com.anyoption.common.beans.Transaction;
import com.anyoption.common.beans.Writer;
import com.anyoption.common.beans.base.User;
import com.anyoption.common.beans.base.UserRegulationBase;
import com.anyoption.common.daos.FilesDAOBase;
import com.anyoption.common.daos.IssuesDAOBase;
import com.anyoption.common.daos.UsersDAOBase;
import com.anyoption.common.daos.UsersRegulationDAOBase;
import com.anyoption.common.util.ConstantsBase;
//import com.copyop.common.enums.base.UserStateEnum;
//import com.copyop.common.managers.BlockUserManager;
//import com.copyop.common.managers.ProfileLinksManager;
//import com.copyop.common.managers.ProfileManager;

/**
 * @author kirilim
 *
 */
public class IssuesManagerBase extends BaseBLManager {

	private static final Logger logger = Logger.getLogger(IssuesManagerBase.class);
	
	public static final int ISSUE_CHANNEL_ACTIVIY = 7;
	public static final long ISSUE_SUBJECT_FIVE_NO_DOC = 36;
	public static final int ISSUE_SUBJECTS_BONUS = 50;
	public static final long ISSUE_SUBJ_CC_CHANGED = 51;
	public static final long ISSUE_SUBJECT_PENDING_DOC = 56;
	public static final String ISSUE_SUBJ_PENDING_DOCS = "56";
	public static final long ISSUE_SUBJECTS_INTERNAL = 67;
	
	public static final int ISSUE_STATUS_NEW = 4;
	public static final int ISSUE_STATUS_DONE_SUPPORT = 8;
	
	public static final int ISSUE_ACTION_BONUS_CANCELLATION_REGULATION = 65;
	public static final int ISSUE_ACTION_TYPE_PENDING_DOCS = 44;
	public static final int ISSUE_ACTION_TYPE_BINARY_QUESTION_NO = 62;
	public static final int ISSUE_ACTION_TYPE_BINARY_QUESTION_YES = 63;
	public static final int ISSUE_ACTION_TYPE_REGULATION_ACTIVATION = 66;
	public static final int ISSUE_ACTION_TYPE_REGULATION_LOW_QUESTIONNARE_SCORE = 67;
	public static final int ISSUE_ACTION_TYPE_REGULATION_LOSS_THRESHOLD_SUSPEND = 68;
	public static final int ISSUE_ACTION_TYPE_LOSS_THRESHOLD_SUSPEND_EMAIL = 69;
	public static final int ISSUE_SUBJECT_REGULATION = 70;
	public static final int ISSUE_ACTION_TYPE_RESTRICTED_QUESTION_NO = 70;
	public static final int ISSUE_ACTION_TYPE_RESTRICTED_QUESTION_YES = 71;
	public static final int ISSUE_ACTION_TYPE_RESTRICTED_SCORE_GROUP = 72;
	public static final int ISSUE_ACTION_TYPE_RESTRICTED_SUCCESS_AFTER_REG_LINK = 73;
	public static final int ISSUE_ACTION_TYPE_REGULATION_PEP_ACCOUNT_TEMPORARY_SUSPENDED = 74;
	public static final int ISSUE_ACTION_TYPE_REGULATION_PEP_APPROVED_COMPLIANCE_UNSUSPENDED = 75;
	public static final int ISSUE_ACTION_TYPE_REGULATION_PEP_FALSE_CLASSIFICATION_UNSUSPENDED = 76;
	public static final int ISSUE_ACTION_TYPE_BLOCK_CC_DEPS = 79;
	public static final int ISSUE_ACTION_TYPE_ENABLE_CC_DEPS = 80;	
	public static final int ISSUE_ACTION_TYPE_MAKE_VIP = 81;
	public static final int ISSUE_ACTION_TYPE_REVOKE_VIP = 82;
	public static final int ISSUE_ACTION_TYPE_RESTRICT_VIP = 83;
	public static final int ISSUE_ACTION_TYPE_UNRESTRICT_VIP = 84;
	public static final int ISSUE_ACTION_TYPE_RESTRICTED_FAILED_QUESTIONNAIRE = 85;
	public static final int ISSUE_ACTION_TYPE_SUCCESS_AFTER_FAILED_QUESTIONNAIRE = 86;
	public static final int ISSUE_ACTION_TYPE_SKIPED_BANK_DETAILS = 87;
	public static final int ISSUE_ACTION_TYPE_SUSPEND_NON_REGULATION = 88;
	public static final int ISSUE_ACTION_TYPE_REMOVE_SUSPEND_NON_REGULATION = 89;
	public static final int ISSUE_ACTION_TYPE_CAP_SUSPEND = 90;
	public static final int ISSUE_ACTION_TYPE_CAP_WD = 91;
	public static final int ISSUE_ACTION_TYPE_CNMV_SUSPEND = 92;
	public static final int ISSUE_ACTION_TYPE_CNMV_UNSUSPEND = 93;
	public static final int ISSUE_ACTION_TYPE_MIGRATION_SUSPEND = 94;
	
	//risk status
	public static final int RISK_STATUS_NEW_ISSUE = 4;
	public static final int RISK_STATUS_IN_PROGRESS = 5;
	public static final int RISK_STATUS_ADDITIONAL_REQUEST = 6;
	public static final int RISK_STATUS_DONE_SUPPORT_UNREACHED = 7;
	public static final int RISK_STATUS_DONE_SUPPORT = 8;
	public static final int RISK_STATUS_NEED_APPROVE = 9;
	public static final int RISK_STATUS_CLOSED = 10;
	public static final int RISK_STATUS_DONE_UNCOOPERATIVE = 11;
	
	public static final long ISSUE_REACHED_STATUS_NOT_REACHED = 1;
	public static final long ISSUE_REACHED_STATUS_REACHED = 2;
	public static final long ISSUE_REACHED_STATUS_FALSE_ACCOUNT = 3;
	public static final long ISSUE_SUBJECT_BLANK_TEMPLATE = 74;

	public static final long ISSUE_REACTION_NO_ANSWER = 1;
	public static final long ISSUE_REACTION_ALREADY_DEPOSIT = 2;
	public static final long ISSUE_REACTION_NOT_COOPERATIVE = 3;
	public static final long ISSUE_REACTION_CALL_BACK = 4;
	public static final long ISSUE_REACTION_CUT_OFF_CALLS = 5;
	public static final long ISSUE_REACTION_DIF_LANGUAGE = 6;
	public static final long ISSUE_REACTION_LINE_BUSY = 7;
	public static final long ISSUE_REACTION_NOT_INTERESTED = 8;
	public static final long ISSUE_REACTION_THIRD_PARTY = 9;
	public static final long ISSUE_REACTION_WRONG_NUMBER = 10;
	public static final long ISSUE_REACTION_DEPOSIT_DURING_CALL = 11;
	public static final long ISSUE_REACTION_COOPERATION = 12;
	public static final long ISSUE_REACTION_DENIED_ACCOUNT_OPENING = 13;
	public static final long ISSUE_REACTION_CANT_TALK_NOW = 14;
	public static final long ISSUE_REACTION_WILL_DEPOSIT_LATER = 15;
	public static final long ISSUE_REACTION_NOT_INTERESTED_NOW = 16;
	public static final long ISSUE_REACTION_NOT_SURE = 17;
	public static final long ISSUE_REACTION_BURN = 18;
	public static final long ISSUE_REACTION_QUERY_ANSWERED = 19;
	public static final long ISSUE_REACTION_OPENED_ACCOUNT = 20;

	// reaction's deposit search type
	public static final int DEPOSIT_SEARCH_TYPE_NONE = 0;
	public static final int DEPOSIT_SEARCH_TYPE_DEPOSIT_DURING_CALL = 1;
	public static final int DEPOSIT_SEARCH_TYPE_FUTURE_DEPOSIT = 2;

	public static final int ISSUE_STATUS_ACTION_REQUIRED = 1;
	public static final int ISSUE_STATUS_ON_PROGRESS = 2;
	public static final int ISSUE_STATUS_FINISHED = 3;
	public static final int ISSUE_STATUS_CLOSED = 10;

	// files risk status
	public static final int FILES_RISK_STATUS_NEW_ISSUE = 4;
	public static final int FILES_RISK_STATUS_IN_PROGRESS = 5;
	public static final int FILES_RISK_STATUS_ADDITIONAL_REQUEST = 6;
	public static final int FILES_RISK_STATUS_DONE_SUPPORT_UNREACHED = 7;
	public static final int FILES_RISK_STATUS_DONE_SUPPORT = 8;
	public static final int FILES_RISK_STATUS_NEED_APPROVE = 9;
	public static final int FILES_RISK_STATUS_CLOSED = 10;
	public static final int FILES_RISK_STATUS_DONE_UNCOOPERATIVE = 11;

	public static final int ISSUE_PRIORITY_LOW = 1;
	public static final int ISSUE_PRIORITY_MEDIUM = 2;
	public static final int ISSUE_PRIORITY_HIGH =3;

	// Risk issue subject
	public static final long ISSUE_SUBJ_DOCUMENTS_REQUIRED = 17;
	public static final long ISSUE_SUBJ_GENERAL = 5;
	public static final long ISSUE_SUBJ_CLOSE_ACCOUNT = 6;
	public static final long ISSUE_SUBJ_POPULATION = 31;
	public static final long ISSUE_SUBJ_SMS = 34;
	public static final long ISSUE_SUBJ_CHARGEBACK = 19;
	public static final long ISSUE_SUBJ_BONUS = 50;
	public static final String ISSUE_SUBJECT_RISK_ISSUE = "47";
	public static final int ISSUE_SUBJECT_REG_DOC = 56;
	public static final long ISSUE_SUBJ_MOVE_SKIN = 57;
	public static final long ISSUE_SUBJECT_DOCUMENT_REQUEST1 = 58;
	public static final long ISSUE_SUBJECT_DOCUMENT_REQUEST2 = 59;
	public static final long ISSUE_SUBJECT_DOCUMENT_REQUEST3 = 60;
	public static final long ISSUE_SUBJECT_DOCUMENT_REQUEST4 = 61;
	public static final long ISSUE_SUBJECT_5DEPOSIT_MORE = 62;
	public static final long ISSUE_SUBJECT_QUESTIONNAIRE = 63;
	public static final long ISSUE_SUBJECT_MAINTENANCE_FEE_EMAIL1 = 64;
	public static final long ISSUE_SUBJECT_MAINTENANCE_FEE_EMAIL2 = 65;
	public static final int ISSUE_SUBJECT_EMAIL_CHANGED = 66;
	public static final int ISSUE_SUBJECT_REGISTRATION_SMS = 71;
	public static final long ISSUE_SUBJECT_DOCS_UPDATE = 72;
	public static final long ISSUE_SUBJECT_VIP = 77;
	public static final long ISSUE_SUBJ_CAP_SUSPEND = 81;
	public static final long ISSUE_SUBJ_MIGRATION_SUSPEND = 82;
	
	public static final int ISSUE_CHANNEL_CALL = 1;
	public static final int ISSUE_CHANNEL_SMS = 2;
	public static final int ISSUE_CHANNEL_EMAIL = 3;
	public static final int ISSUE_CHANNEL_COMMENT = 4;
	public static final int ISSUE_CHANNEL_FALSE_ACCOUNT = 5;
	public static final int ISSUE_CHANNEL_DISABLE_PHONE_CONTACT = 6;
	public static final int ISSUE_CHANNEL_ACTIVITY = 7;
	public static final int ISSUE_CHANNEL_CHAT = 8;
	public static final int ISSUE_CHANNEL_AUTOMATIC_DIALER = 9;
	public static final int ISSUE_CHANNEL_PUSH_NOTIFICATION = 10;

	public static final int ISSUE_ACTIVITY_FALSE_ACCOUNT = 1;
	public static final int ISSUE_ACTIVITY_CANCEL_PHONE_CONTACT = 2;
	public static final int ISSUE_ACTIVITY_CANCEL_PHONE_CONTACT_DAA = 3;
	public static final int ISSUE_ACTIVITY_CANCEL_CALL_BACK = 4;
	public static final int ISSUE_ACTIVITY_RETURN_TO_SALES = 5;
	public static final int ISSUE_ACTIVITY_REMOVE_FROM_POPULATION = 6;
	public static final int ISSUE_ACTIVITY_REMOVE_FROM_SALES = 7;
	public static final int ISSUE_ACTIVITY_DISPLAY_MARKETING_USERS = 8;
	public static final int ISSUE_ACTIVITY_FRAUD = 9;
	public static final int ISSUE_ACTIVITY_CHB_CLOSE = 10;
	public static final int ISSUE_ACTIVITY_DUPLICATE_ACCOUNT = 11;
	public static final int ISSUE_ACTIVITY_ACTIVATE_ACCOUNT = 12;
	public static final int ISSUE_ACTIVITY_DISACTIVATE_ACCOUNT = 13;

	/* ACTION TYPES */

	// Activities
	public static final int ISSUE_ACTION_FALSE_ACCOUNT = 1;
	public static final int ISSUE_ACTION_CANCEL_PHONE_CONTACT = 2;
	public static final int ISSUE_ACTION_CANCEL_PHONE_CONTACT_DAA = 3;
	public static final int ISSUE_ACTION_CANCEL_CALL_BACK = 4;
	public static final int ISSUE_ACTION_RETURN_TO_SALES = 5;
	public static final int ISSUE_ACTION_REMOVE_FROM_POPULATION = 6;
	public static final int ISSUE_ACTION_REMOVE_FROM_SALES = 7;
	public static final int ISSUE_ACTION_DISPLAY_MARKETING_USERS = 8;
	public static final int ISSUE_ACTION_FRAUD = 9;
	public static final int ISSUE_ACTION_CHB_CLOSE = 10;
	public static final int ISSUE_ACTION_DUPLICATE_ACCOUNT = 11;
	public static final int ISSUE_ACTION_ACTIVATE_ACCOUNT = 12;
	public static final int ISSUE_ACTION_DEACTIVATE_ACCOUNT = 13;
	public static final int ISSUE_ACTION_ALLOWED_CREDIT_CARD = 42;
	public static final int ISSUE_ACTION_BONUS_ABUSER = 43;
	public static final int ISSUE_ACTION_VELOCITY_WAIVE = 45;
	public static final int ISSUE_ACTION_VELOCITY_ACTIVATE = 46;
	public static final int ISSUE_ACTION_SUSPEND_REGULATION = 47;
	public static final int ISSUE_ACTION_REMOVE_SUSPEND_REGULATION = 48;
	public static final int ISSUE_ACTION_REMOVE_FROM_TREATMENT = 49;

	// Call Reactions
	public static final int ISSUE_ACTION_CANT_TALK_NOW = 15;
	public static final int ISSUE_ACTION_WILL_DEPOSIT_LATER = 16;
	public static final int ISSUE_ACTION_NOT_INTERESTED_NOW = 17;
	public static final int ISSUE_ACTION_NOT_SURE = 18;
	public static final int ISSUE_ACTION_BURN = 19;
	public static final int ISSUE_ACTION_QUERY_ANSWERED = 20;
	public static final int ISSUE_ACTION_NO_ANSWER = 21;
	public static final int ISSUE_ACTION_ALREADY_DEPOSIT = 22;
	public static final int ISSUE_ACTION_NOT_COOPERATIVE = 23;
	public static final int ISSUE_ACTION_CALL_BACK = 24;
	public static final int ISSUE_ACTION_CUT_OFF_CALLS = 25;
	public static final int ISSUE_ACTION_DIFFERENT_LANGUAGE = 26;
	public static final int ISSUE_ACTION_LINE_BUSY = 27;
	public static final int ISSUE_ACTION_NOT_INTERESTED = 28;
	public static final int ISSUE_ACTION_THIRD_PARTY = 29;
	public static final int ISSUE_ACTION_WRONG_NUMBER = 30;
	public static final int ISSUE_ACTION_DEPOSIT_DURING_CALL = 31;
	public static final int ISSUE_ACTION_COOPERATION = 32;
	public static final int ISSUE_ACTION_DENIED_ACCOUNT_OPENING = 33;
	public static final int ISSUE_ACTION_OPENED_ACCOUNT = 34;
	public static final int ISSUE_ACTION_LOGIN_FAIL_CLOSE = 39;
	public static final int ISSUE_ACTION_RISK_CONTACT_USER = 40;
	public static final int ISSUE_ACTION_RISK_NEED_DOCUMENT = 41;
	public static final int ISSUE_ACTION_PENDING_DOC = 44;
	public static final int ISSUE_ACTION_NOT_INTERESTED_DONT_HAVE_MONEY = 50;
	public static final int ISSUE_ACTION_NOT_INTERESTED_DONT_WANT_TO_TRADE = 51;
	public static final int ISSUE_ACTION_NOT_INTERESTED_NOT_RELEVANT = 52;
	public static final int ISSUE_ACTION_DORMANT_CUSTOMER_CONVERSATION = 53;
	public static final int ISSUE_ACTION_EMAIL_CHANGED = 56;

	// Channels Action Types
	public static final int ISSUE_ACTION_SMS = 35;
	public static final int ISSUE_ACTION_EMAIL = 36;
	public static final int ISSUE_ACTION_COMMENT = 37;
	public static final int ISSUE_ACTION_CHAT = 38;
	public static final int ISSUE_ACTION_AUTOMATIC_DIALER = 54;
	public static final int ISSUE_ACTION_PUSH_NOTIFICATION = 57;
	
	

	// Issue action reason type

	public static final int ISSUE_REASON_TYPE_REG_DOC = 14;

	public static void insertLoginFailureLockIssue(Connection conn, String username, long userId) throws SQLException {
		logger.info("insert insertLoginFailureLockIssue for user " + username);
		try {
			// Create issue
			Issue issue = new Issue();
			IssueAction issueAction = new IssueAction();
			// Filling issue fields
			issue.setSubjectId(String.valueOf(ISSUE_SUBJ_CLOSE_ACCOUNT));
			issue.setStatusId(String.valueOf(ISSUE_STATUS_FINISHED));
			issue.setUserId(userId);
			issue.setPriorityId(String.valueOf(ISSUE_PRIORITY_LOW));
			issue.setType(ConstantsBase.ISSUE_TYPE_REGULAR);
			// Filling action fields
			issueAction.setActionTypeId(String.valueOf(ISSUE_ACTION_LOGIN_FAIL_CLOSE));
			issueAction.setWriterId(Writer.WRITER_ID_AUTO);
			issueAction.setSignificant(false);
			issueAction.setActionTime(new Date());
			issueAction.setActionTimeOffset(ConstantsBase.OFFSET_GMT);
			issueAction.setComments("User has been locked after several login attempts");
			IssuesDAOBase.insertIssue(conn, issue);
			issueAction.setIssueId(issue.getId());
			IssuesDAOBase.insertAction(conn, issueAction);
			logger.info("Successfully insert login fail close issue for user id: "+ userId);
		} catch (SQLException e) {
			logger.error("couldn't insert login fail close issue for user id: "+ userId,e);
			throw e;
		}
	}
	
	public static boolean insertCopyopBlockRemoveIssue(Connection conn, long userId, Issue issueCurrent, long writerId, String issueActionTypeId) throws SQLException {
		
		try {
			// Create issue
			
			Issue issue = new Issue();
			IssueAction issueAction = new IssueAction();
			
			// Filling issue fields
			issue.setStatusId(String.valueOf(ISSUE_STATUS_NEW));
			issue.setUserId(userId);
			issue.setPriorityId(issueCurrent.getPriorityId());
			issue.setType(RISK_STATUS_NEW_ISSUE);
			
			// Filling action fields
			issueAction.setActionTypeId(issueActionTypeId);
			issueAction.setWriterId(writerId);
			issueAction.setSignificant(false);
			issueAction.setActionTime(new Date());
			issueAction.setActionTimeOffset(ConstantsBase.OFFSET_GMT);
			
			if (issueActionTypeId.equals(IssueAction.ISSUE_ACTION_USER_COPYOP_BLOCK)) {
				issueAction.setComments("Block Copyop user");
				issue.setSubjectId(String.valueOf(Issue.ISSUE_SUBJECTS_COPYOP_BLOCK));
			} else if (issueActionTypeId.equals(IssueAction.ISSUE_ACTION_USER_COPYOP_REMOVE)) {
				issueAction.setComments("Remove Copyop user");
				issue.setSubjectId(String.valueOf(Issue.ISSUE_SUBJECTS_COPYOP_REMOVE));
			}
			
			IssuesDAOBase.insertIssue(conn, issue);
			issueAction.setIssueId(issue.getId());
			IssuesDAOBase.insertAction(conn, issueAction);
			logger.info("Successfully insert Issue for user id " + userId);
		} catch (SQLException e) {
			logger.error("couldn't insert copyop blocked/removed issue for user id: "+ userId,e);
			return false;
		}
		return true;
	}
	
	public static boolean insertCopyopUnblockUnremoveIssueAction(Connection conn, long userId, Issue currentIssue, long writerId, String issueActionTypeId)throws SQLException{
	
		try {
			IssueAction issueAction = new IssueAction();
			// Filling action fields
			issueAction.setActionTypeId(issueActionTypeId);
			issueAction.setWriterId(writerId);
			issueAction.setSignificant(false);
			issueAction.setActionTime(new Date());
			issueAction.setActionTimeOffset(ConstantsBase.OFFSET_GMT);
			issueAction.setComments(currentIssue.getLastComments());
			issueAction.setIssueId(currentIssue.getId());
			currentIssue.setStatusId(String.valueOf(IssuesManagerBase.RISK_STATUS_CLOSED));
			IssuesDAOBase.insertAction(conn, issueAction);
			IssuesDAOBase.updateIssue(conn, currentIssue);
			logger.info("Successfully insert action for issue with id " + currentIssue.getId());
		} catch (SQLException e) {
			logger.error("couldn't insert copyop unblocked/unremoved action for issue with id: "+ currentIssue.getId(),e);
			return false;
		}
		return true;
	}
	
//	public static void updateCopyopUserStatus(String username, long userId, Issue issueCurrent, long writerId, String issueActionTypeId) throws SQLException {
//		Connection conn = null;
//		
//		try {
//			conn = getConnection();
//			conn.setAutoCommit(false);
//			boolean isCopyopUserBlockedRemoved = false;
//			boolean isUpdateCopyopUserState = false;
//			boolean isCopyopUserUnblockedUnremoved = false;
//			
//			
//			if(issueActionTypeId.equals(IssueAction.ISSUE_ACTION_USER_COPYOP_UNBLOCK)){
//				
//				isCopyopUserUnblockedUnremoved = insertCopyopUnblockUnremoveIssueAction(conn, userId, issueCurrent, writerId, issueActionTypeId);
//				BlockUserManager.unBlockUser(userId);// unblock user from Cassandra DB
//				isUpdateCopyopUserState = UsersDAOBase.updateCopyopUserState(conn, userId, issueActionTypeId);
//				
//			}else if(issueActionTypeId.equals(IssueAction.ISSUE_ACTION_USER_COPYOP_UNREMOVED)){
//				
//				isCopyopUserUnblockedUnremoved = insertCopyopUnblockUnremoveIssueAction(conn, userId, issueCurrent, writerId, issueActionTypeId);
//				ProfileManager.updateUserStateBS(userId, UserStateEnum.STATE_REGULAR);// method for update user state to unremoved in Cassandra DB
//				isUpdateCopyopUserState = UsersDAOBase.updateCopyopUserState(conn, userId, issueActionTypeId);
//				
//			}else if(issueActionTypeId.equals(IssueAction.ISSUE_ACTION_USER_COPYOP_BLOCK)){
//				
//				isCopyopUserBlockedRemoved = insertCopyopBlockRemoveIssue(conn, userId, issueCurrent, writerId, issueActionTypeId);
//				BlockUserManager.blockUser(userId);
//				isUpdateCopyopUserState = UsersDAOBase.updateCopyopUserState(conn, userId, issueActionTypeId);
//				
//			}else if(issueActionTypeId.equals(IssueAction.ISSUE_ACTION_USER_COPYOP_REMOVE)){
//				
//				isCopyopUserBlockedRemoved = insertCopyopBlockRemoveIssue(conn, userId, issueCurrent, writerId, issueActionTypeId);
//				ProfileLinksManager.deleteAllProfileLinks(userId);// method remove user links from Cassandra DB
//				ProfileManager.updateUserStateBS(userId, UserStateEnum.STATE_REMOVED);// method for update user state to removed in Cassandra DB
//				isUpdateCopyopUserState = UsersDAOBase.updateCopyopUserState(conn, userId, issueActionTypeId);
//				
//			}
//			
//			
//			if ((isCopyopUserUnblockedUnremoved && isUpdateCopyopUserState) || (isCopyopUserBlockedRemoved && isUpdateCopyopUserState)) {
//				conn.commit();
//			} else {
//				conn.rollback();	
//			}
//		
//		}catch(Exception ex){
//			logger.error("couldn't update copyop user state with id: "+ userId, ex);
//			conn.rollback();
//		} finally {
//			if (conn != null) {
//	        	try {
//					conn.setAutoCommit(true);
//				} catch (SQLException e) {
//					logger.error("Unable to turn on Auto Commit for connection: " + conn, e);
//				}
//        	}
//			closeConnection(conn);
//		}
//	}

//	public static boolean updateCopyopUserStatus(String username, long userId, long writerId, String issueActionTypeId) throws SQLException {
//		Issue issue = getIssueForUpdateCopyopUserStatus(userId);
//		if (issue == null) {
//			logger.debug("Issue is null, userId: " + userId);
//			return false;
//		}
//		updateCopyopUserStatus(username, userId, issue, writerId, issueActionTypeId);
//		return true;
//	}

//	private static Issue getIssueForUpdateCopyopUserStatus(long userId) throws SQLException {
//		Connection con = getConnection();
//	    try {
//	    	return IssuesDAOBase.getIssueForUpdateCopyopUserStatus(con, userId);
//	    }
//	    finally {
//	    	closeConnection(con);
//	    }
//	}

	/**
	 * @param conn
	 * @param issueSubjectId
	 * @param issueStatusId
	 * @param userId
	 * @param issuePriorityId
	 * @param issueType
	 * @param issueActionTypeId
	 * @param writerId
	 * @param issueActionIsSignificant
	 * @param issueActionTime
	 * @param issueActionTimeOffset
	 * @param issueActionComments
	 * @throws SQLException
	 */
	public static void insertIssue(Connection conn, String issueSubjectId, String issueStatusId, long userId, String issuePriorityId, int issueType,
			String issueActionTypeId, long writerId, boolean issueActionIsSignificant, Date issueActionTime, String issueActionTimeOffset, String issueActionComments) throws SQLException {
		logger.info("insert Issue for user id " + userId + " with comment: " + issueActionComments);
		try {
			// Create issue
			Issue issue = new Issue();
			IssueAction issueAction = new IssueAction();
			// Filling issue fields
			issue.setSubjectId(issueSubjectId);//String.valueOf(ISSUE_SUBJ_CLOSE_ACCOUNT));
			issue.setStatusId(issueStatusId);//String.valueOf(ISSUE_STATUS_FINISHED));
			issue.setUserId(userId);
			issue.setPriorityId(issuePriorityId);//String.valueOf(ISSUE_PRIORITY_LOW));
			issue.setType(issueType);//ConstantsBase.ISSUE_TYPE_REGULAR);
			// Filling action fields
			issueAction.setActionTypeId(issueActionTypeId);//String.valueOf(ISSUE_ACTION_LOGIN_FAIL_CLOSE));
			issueAction.setWriterId(writerId);//Writer.WRITER_ID_AUTO);
			issueAction.setSignificant(issueActionIsSignificant);//false);
			issueAction.setActionTime(issueActionTime);//new Date());
			issueAction.setActionTimeOffset(issueActionTimeOffset);//ConstantsBase.OFFSET_GMT);
			issueAction.setComments(issueActionComments);//"User has been locked after several login attempts");
			IssuesDAOBase.insertIssue(conn, issue);
			issueAction.setIssueId(issue.getId());
			IssuesDAOBase.insertAction(conn, issueAction);
			logger.info("Successfully insert Issue for user id " + userId + " with comment: " + issueActionComments);
		} catch (SQLException e) {
			logger.error("couldn't insert Issue for user id " + userId + " with comment: " + issueActionComments, e);
			throw e;
		}
	}
	
	public static void insertIssueAndIssueAction(Connection conn, Issue issue, IssueAction issueAction) throws SQLException {
		try {
			if(issue.getId() == 0){
				IssuesDAOBase.insertIssue(conn, issue);
			}
			issueAction.setIssueId(issue.getId());
			IssuesDAOBase.insertAction(conn, issueAction);
			logger.info("Successfully insert Issue for user id " + issue.getUserId());
		} catch (SQLException e) {
			logger.error("couldn't insert Issue for user id " + issue.getUserId(), e);
			throw e;
		}
	}
	//START AO Regulation insert Issue and Issue actions
	public static boolean insertUserRegulationRestrictedIssue(Connection conn, long userId, String issuePriorityId, String issueActionTypeId, String issueActionComments, boolean isSignificant, boolean isYesClicked, long writerId) throws SQLException {
		try {
			// Create issue
			Issue issue = new Issue();
			IssueAction issueAction = new IssueAction();
			// Filling issue fields
			issue.setSubjectId(String.valueOf(ISSUE_SUBJECT_REGULATION));
			issue.setStatusId(String.valueOf(RISK_STATUS_CLOSED));
			issue.setUserId(userId);
			issue.setPriorityId(issuePriorityId);
			issue.setType(ConstantsBase.ISSUE_TYPE_REGULAR);
			// Filling action fields
			issueAction.setActionTypeId(issueActionTypeId);
			issueAction.setWriterId(Writer.WRITER_ID_AUTO);
			issueAction.setSignificant(isSignificant);
			issueAction.setActionTime(new Date());
			issueAction.setActionTimeOffset(UsersManagerBase.getUserUtcOffset(userId));
			issueAction.setComments(issueActionComments);
			
			insertIssueAndIssueAction(conn, issue, issueAction);
			
			if(isYesClicked){
				UserRegulationManager.updateIsKnowledgeQuestion(userId, isYesClicked, writerId);
			}
			
			logger.info("Successfully insert user regulation issue for user with id: "+ userId);
		} catch (SQLException e) {
			logger.error("Couldn't insert user regulation issue for user with id: "+ userId, e);
			return false;
		}
		return true;
	}
	
	public static boolean insertUserRegulationIssueAction(long userId, String issueActionTypeId,
			String issueActionComments, String issuePriorityId, boolean isSignificant, Issue issue,
			boolean isYesClicked) {
		Connection con = null;

		try {
			con = getConnection();
			return insertUserRegulationRestrictedIssueAction(con, userId, issueActionTypeId, issueActionComments,
					issuePriorityId, isSignificant, issue, isYesClicked);
		} catch (SQLException e) {
			logger.error("Couldn't insert user regulation action for issue with id: " + issue.getId(), e);
			return false;
		}
	}
	
	public static boolean insertUserRegulationRestrictedIssueAction(Connection conn, long userId, String issueActionTypeId, String issueActionComments, String issuePriorityId, boolean isSignificant, Issue issue, boolean isYesClicked)throws SQLException{
		
		try {
			IssueAction issueAction = new IssueAction();
			// Filling action fields
			issueAction.setActionTypeId(issueActionTypeId);
			issueAction.setWriterId(Writer.WRITER_ID_AUTO);
			issueAction.setSignificant(isSignificant);
			issueAction.setActionTime(new Date());
			issueAction.setActionTimeOffset(UsersManagerBase.getUserUtcOffset(userId));
			issueAction.setComments(issueActionComments);
			issueAction.setIssueId(issue.getId());
			issue.setPriorityId(issuePriorityId);
			
			IssuesDAOBase.insertAction(conn, issueAction);
			IssuesDAOBase.updateIssue(conn, issue);
			
			if(isYesClicked){
				UserRegulationManager.updateIsKnowledgeQuestion(userId, isYesClicked, Writer.WRITER_ID_AUTO);
			}
			
			logger.info("Successfully insert action for issue with id " + issue.getId());
		} catch (SQLException e) {
			logger.error("Couldn't insert user regulation action for issue with id: "+ issue.getId(),e);
			return false;
		}
		return true;
	}
	
	public static void insertIssueUserRegulationRestricted(long userId, int issueActionType, long writerId) throws SQLException{
		Connection conn = null;
		String issuePriorityId = "";
		String issueActionComments = "";
		String issueActionTypeId = "";
		boolean isSignificant = false;
		boolean isIssueInserted = false;
		boolean isIssueActionInserted = false;
		boolean isNeedToBeUnrestricted = false;
		try {
			conn = getConnection();
			conn.setAutoCommit(false);
			
			if(userId != 0){
				if (!IssuesDAOBase.isUserUnrestricted(conn, userId)) {
					
					if (issueActionType == ISSUE_ACTION_TYPE_RESTRICTED_QUESTION_YES) {
						issuePriorityId = String.valueOf(IssuesManagerBase.ISSUE_PRIORITY_LOW);
						issueActionComments = "Customer clicked YES on restricted pop-up.";
						issueActionTypeId = String.valueOf(IssuesManagerBase.ISSUE_ACTION_TYPE_RESTRICTED_QUESTION_YES);
						isNeedToBeUnrestricted = true;
					} else if (issueActionType == ISSUE_ACTION_TYPE_RESTRICTED_SUCCESS_AFTER_REG_LINK) {
						issuePriorityId = String.valueOf(IssuesManagerBase.ISSUE_PRIORITY_LOW);
						issueActionComments = "Customer become unrestricted via activation link.";
						issueActionTypeId = String.valueOf(IssuesManagerBase.ISSUE_ACTION_TYPE_RESTRICTED_SUCCESS_AFTER_REG_LINK);
						isNeedToBeUnrestricted = true;
					} else if (issueActionType == ISSUE_ACTION_TYPE_RESTRICTED_SCORE_GROUP) {
						issuePriorityId = String.valueOf(IssuesManagerBase.ISSUE_PRIORITY_HIGH);
						issueActionComments = "Customer is restricted automaticaly based on wrong answers.";
						issueActionTypeId = String.valueOf(IssuesManagerBase.ISSUE_ACTION_TYPE_RESTRICTED_SCORE_GROUP);
					} else if (issueActionType == ISSUE_ACTION_TYPE_RESTRICTED_QUESTION_NO) {
						issuePriorityId = String.valueOf(IssuesManagerBase.ISSUE_PRIORITY_HIGH);
						issueActionComments = "Customer clicked NO on restricted pop-up.";
						issueActionTypeId = String.valueOf(IssuesManagerBase.ISSUE_ACTION_TYPE_RESTRICTED_QUESTION_NO);
						isSignificant = true;
					} else if( issueActionType == IssuesManagerBase.ISSUE_ACTION_TYPE_RESTRICTED_FAILED_QUESTIONNAIRE) {
						issuePriorityId = String.valueOf(IssuesManagerBase.ISSUE_PRIORITY_HIGH);
						issueActionComments = "User have failed the questionnaire";
						issueActionTypeId = String.valueOf(IssuesManagerBase.ISSUE_ACTION_TYPE_RESTRICTED_FAILED_QUESTIONNAIRE);
						isSignificant = true;
					}  else if( issueActionType == IssuesManagerBase.ISSUE_ACTION_TYPE_SUCCESS_AFTER_FAILED_QUESTIONNAIRE) {
						issuePriorityId = String.valueOf(IssuesManagerBase.ISSUE_PRIORITY_HIGH);
						issueActionComments = "User have activated his account after failing the questionnaire";
						issueActionTypeId = String.valueOf(IssuesManagerBase.ISSUE_ACTION_TYPE_SUCCESS_AFTER_FAILED_QUESTIONNAIRE);
						isSignificant = true;
						isNeedToBeUnrestricted = true;
					}
					
						List<Integer> issueActionTypes = new ArrayList<Integer>();
						issueActionTypes.add(IssuesManagerBase.ISSUE_ACTION_TYPE_RESTRICTED_QUESTION_NO);
						issueActionTypes.add(IssuesManagerBase.ISSUE_ACTION_TYPE_RESTRICTED_QUESTION_YES);
						issueActionTypes.add(IssuesManagerBase.ISSUE_ACTION_TYPE_RESTRICTED_SCORE_GROUP);
						issueActionTypes.add(IssuesManagerBase.ISSUE_ACTION_TYPE_RESTRICTED_SUCCESS_AFTER_REG_LINK);
						issueActionTypes.add(IssuesManagerBase.ISSUE_ACTION_TYPE_RESTRICTED_FAILED_QUESTIONNAIRE);
						Issue issue = IssuesDAOBase.getUserIssueByIssueActionType(conn, userId, issueActionTypes);
						
						if(issue == null) {
							isIssueInserted = insertUserRegulationRestrictedIssue(conn, userId, issuePriorityId, issueActionTypeId, issueActionComments, isSignificant, isNeedToBeUnrestricted, writerId);
						} else {
							isIssueActionInserted = insertUserRegulationRestrictedIssueAction(conn, userId, issueActionTypeId, issueActionComments, issuePriorityId, isSignificant, issue, isNeedToBeUnrestricted);
						}
						
						if (isIssueInserted || isIssueActionInserted) {
							conn.commit();
							if(isIssueInserted){
								logger.info("Successfully insert AO Regulation restricted issue for user with id: " + userId);
							} else {
								logger.info("Successfully insert AO Regulation restricted issue Action for user with id: " + userId);
							}
						} else {
							conn.rollback();	
						}
				}
		} else {
			logger.error("insertIssueUserRegulationRestricted userId is 0 !!!");
		}
		
		} catch (Exception ex) {
			logger.error("Couldn't insert AO restricted issue for user with id: " + userId, ex);
			conn.rollback();
		} finally {
			if (conn != null) {
	        	try {
					conn.setAutoCommit(true);
				} catch (SQLException e) {
					logger.error("Unable to turn on Auto Commit for connection: " + conn, e);
				}
        	}
			closeConnection(conn);
		}
		
	}
	
	//END AO Regulation insert Issue and Issue actions
	
	//START ET Regulation insert Issue and Issue actions
		public static boolean insertUserRegulationIssue(Connection conn, long userId, String issuePriorityId, String issueActionTypeId, String issueActionComments, boolean isSignificant, boolean isYesClicked) throws SQLException {
			try {
				// Create issue
				Issue issue = new Issue();
				IssueAction issueAction = new IssueAction();
				// Filling issue fields
				issue.setSubjectId(String.valueOf(ISSUE_SUBJECT_REGULATION));
				issue.setStatusId(String.valueOf(RISK_STATUS_CLOSED));
				issue.setUserId(userId);
				issue.setPriorityId(issuePriorityId);
				issue.setType(ConstantsBase.ISSUE_TYPE_REGULAR);
				// Filling action fields
				issueAction.setActionTypeId(issueActionTypeId);
				issueAction.setWriterId(Writer.WRITER_ID_AUTO);
				issueAction.setSignificant(isSignificant);
				issueAction.setActionTime(new Date());
				issueAction.setActionTimeOffset(UsersManagerBase.getUserUtcOffset(userId));
				issueAction.setComments(issueActionComments);
				
				IssuesDAOBase.insertIssue(conn, issue);
				issueAction.setIssueId(issue.getId());
				IssuesDAOBase.insertAction(conn, issueAction);
				
				IssuesDAOBase.updateKnowledgeQuestionAnswer(conn, userId, isYesClicked);
				
				logger.info("Successfully insert user regulation issue for user with id: "+ userId);
			} catch (SQLException e) {
				logger.error("Couldn't insert user regulation issue for user with id: "+ userId, e);
				return false;
			}
			return true;
		}
		
		public static boolean insertUserRegulationIssueAction(Connection conn, long userId, String issueActionTypeId, String issueActionComments, String issuePriorityId, boolean isSignificant, Issue issue)throws SQLException{
			
			try {
				IssueAction issueAction = new IssueAction();
				// Filling action fields
				issueAction.setActionTypeId(issueActionTypeId);
				issueAction.setWriterId(Writer.WRITER_ID_AUTO);
				issueAction.setSignificant(isSignificant);
				issueAction.setActionTime(new Date());
				issueAction.setActionTimeOffset(UsersManagerBase.getUserUtcOffset(userId));
				issueAction.setComments(issueActionComments);
				issueAction.setIssueId(issue.getId());
				issue.setPriorityId(issuePriorityId);
				
				IssuesDAOBase.insertAction(conn, issueAction);
				IssuesDAOBase.updateIssue(conn, issue);
				
				logger.info("Successfully insert action for issue with id " + issue.getId());
			} catch (SQLException e) {
				logger.error("Couldn't insert user regulation action for issue with id: "+ issue.getId(),e);
				return false;
			}
			return true;
		}
		
		public static void insertIssueUserRegulationKnowledgeQuestion(long userId, int answerId) throws SQLException{
			Connection conn = null;
			String issuePriorityId = "";
			String issueActionComments = "";
			String issueActionTypeId = "";
			boolean isSignificant = false;
			boolean isIssueInserted = false;
			boolean isIssueActionInserted = false;
			boolean isYesClicked = false;
			
			try {
				conn = getConnection();
				conn.setAutoCommit(false);
				
				if(IssuesDAOBase.isKnowledgeQuestionAnswerNO(conn, userId)){
					
					UserRegulationBase userRegulation = new UserRegulationBase();
					userRegulation.setUserId(userId);
					UserRegulationManager.getUserRegulation(userRegulation);
					 
					if (answerId == ConstantsBase.BO_KNOWLEDGE_QUESTION_ANSWER_IS_YES) {
						
						issuePriorityId = String.valueOf(IssuesManagerBase.ISSUE_PRIORITY_LOW);
						issueActionComments = "Customer declared knowledge in BO trading";
						issueActionTypeId = String.valueOf(IssuesManagerBase.ISSUE_ACTION_TYPE_BINARY_QUESTION_YES);
						isYesClicked = true;
						
						userRegulation.setApprovedRegulationStep(UserRegulationBase.ET_REGULATION_KNOWLEDGE_QUESTION_USER);
						userRegulation.setKnowledgeQuestion(true);
					} else if (answerId == ConstantsBase.BO_KNOWLEDGE_QUESTION_ANSWER_IS_NO) {
						
						issuePriorityId = String.valueOf(IssuesManagerBase.ISSUE_PRIORITY_HIGH);
						issueActionComments = "Customer declared NO knowledge in BO trading";
						issueActionTypeId = String.valueOf(IssuesManagerBase.ISSUE_ACTION_TYPE_BINARY_QUESTION_NO);
						isSignificant = true;
						
						userRegulation.setApprovedRegulationStep(UserRegulationBase.ET_REGULATION_KNOWLEDGE_QUESTION_USER);
						userRegulation.setKnowledgeQuestion(false);
						
					}
						
						Issue issue = IssuesDAOBase.getUserRegIssue(conn, userId);
						
						if(issue == null) {
							isIssueInserted = insertUserRegulationIssue(conn, userId, issuePriorityId, issueActionTypeId, issueActionComments, isSignificant, isYesClicked);
						} else {
							if(userRegulation.getScoreGroup() == UserRegulationBase.AO_REGULATION_RESTRICTED_FAILED_Q_SCORE_GROUP_ID) {
								// insert issue action when the user activate his account via popup after failing the questionnaire. 
								isIssueActionInserted = insertUserRegulationIssueAction(conn, userId, String.valueOf(IssuesManagerBase.ISSUE_ACTION_TYPE_SUCCESS_AFTER_FAILED_QUESTIONNAIRE),
										"User have activated his account after failing the questionnaire", issuePriorityId, isSignificant, issue);
							} else {
								isIssueActionInserted = insertUserRegulationIssueAction(conn, userId, issueActionTypeId, issueActionComments, issuePriorityId, isSignificant, issue);
							}
						}
						
						UsersRegulationDAOBase.updateRegulationStepKnowledgeQuestion(conn, userRegulation);
						if (isIssueInserted || isIssueActionInserted) {
							conn.commit();
							if(isIssueInserted){
								logger.info("Successfully insert ET Regulation Knowledge Question issue for userId " + userId);
							} else {
								logger.info("Successfully insert ET Regulation Knowledge Question issue Action with id " + issue.getId());
							}
						} else {
							conn.rollback();	
						}
				}
			
			} catch (Exception ex) {
				logger.error("Couldn't insert ET Regulation Knowledge Question issue for user with id: " + userId, ex);
				conn.rollback();
			} finally {
				if (conn != null) {
		        	try {
						conn.setAutoCommit(true);
					} catch (SQLException e) {
						logger.error("Unable to turn on Auto Commit for connection: " + conn, e);
					}
	        	}
				closeConnection(conn);
			}
		}
		
		public static void insertLinkIssueAction(long userId, Issue issue, String comments, String issueActionTypeId, String channelId, long suspendedId, String priorityId) throws SQLException {
			Connection conn = null;
			try {
				conn = getConnection();
				IssuesDAOBase.insertLinkIssueAction(conn, issue, userId, comments, issueActionTypeId, channelId, suspendedId, priorityId);
			} finally {
				closeConnection(conn);
			}
		}
		
		public static boolean insertUserRegulationPEPRestrictedIssue(long userId, long writerId, String writerUserName, boolean isAfterGBG) throws SQLException {
			Connection conn = null;
			try {
				conn = getConnection();
				conn.setAutoCommit(false);
				// Create issue
				Issue issue = new Issue();
				IssueAction issueAction = new IssueAction();
				// Filling issue fields
				issue.setSubjectId(String.valueOf(ISSUE_SUBJECT_REGULATION));
				issue.setStatusId(String.valueOf(RISK_STATUS_CLOSED));
				issue.setUserId(userId);
				issue.setPriorityId(String.valueOf(IssuesManagerBase.ISSUE_PRIORITY_HIGH));
				if(isAfterGBG){
					issue.setPriorityId(String.valueOf(IssuesManagerBase.ISSUE_PRIORITY_LOW));
				}
				issue.setType(ConstantsBase.ISSUE_TYPE_REGULAR);
													
				String issueActionComments = "User classified PEP by " + writerUserName + ", temporarily suspended";	
				if(isAfterGBG){
					issueActionComments = "Client is PEP";
				}
				issueAction.setActionTypeId(String.valueOf(IssuesManagerBase.ISSUE_ACTION_TYPE_REGULATION_PEP_ACCOUNT_TEMPORARY_SUSPENDED));
				issueAction.setWriterId(writerId);
				issueAction.setActionTime(new Date());
				issueAction.setActionTimeOffset(UsersManagerBase.getUserUtcOffset(userId));
				issueAction.setComments(issueActionComments);				
				insertIssueAndIssueAction(conn, issue, issueAction);			
				logger.info("Successfully insert user regulation pep issue for userId: "+ userId);
			} catch (SQLException e) {
				logger.error("Couldn't insert user regulation issue for userId: "+ userId, e);
				conn.rollback();
				return false;
			} finally {
				if (conn != null) {
		        	try {
						conn.setAutoCommit(true);
					} catch (SQLException e) {
						logger.error("Unable to turn on Auto Commit for connection: " + conn, e);
					}
	        	}
				closeConnection(conn);
			}
			return true;
		}
		
	public static boolean insertUserRegulationUnblockIssueAction(long userId, long writerId) throws SQLException {
		Connection conn = null;
		try {
			UserRegulationBase ur = new UserRegulationBase();
			ur.setUserId(userId);
			UserRegulationManager.getUserRegulation(ur);
			if (ur.isTresholdBlock()) {
				conn = getConnection();
				conn.setAutoCommit(false);
				Issue issue = new Issue();
				issue = IssuesDAOBase.getUserRegBlockedIssue(conn, userId);
				issue.setPriorityId(String.valueOf(IssuesManagerBase.ISSUE_PRIORITY_LOW));
				String issueActionComments = "User account was activated ";
				IssueAction issueAction = new IssueAction();
				// Filling action fields
				issueAction.setActionTypeId(String.valueOf(IssuesManagerBase.ISSUE_ACTION_TYPE_REGULATION_ACTIVATION));
				issueAction.setWriterId(writerId);
				issueAction.setSignificant(false);
				issueAction.setActionTime(new Date());
				issueAction.setActionTimeOffset(UsersManagerBase.getUserUtcOffset(userId));
				issueAction.setComments(issueActionComments);
				issueAction.setIssueId(issue.getId());

				IssuesDAOBase.insertAction(conn, issueAction);
				IssuesDAOBase.updateIssue(conn, issue);
				logger.info("Successfully insert action for issue with id " + issue.getId());
				try {
					UsersRegulationDAOBase.unblockUser(conn, userId);
				} catch (SQLException e) {
					logger.error("Could NOT unblock user with id: " + userId, e);
					return false;
				}
				logger.info("Successfully unblock user regulation unblock issue for userId: " + userId);
				conn.commit();
			} else {
				logger.error("User is not blocked, userId: " + userId);
			}
		} catch (SQLException e) {
			logger.error("Couldn't insert user regulation issue for userId: " + userId, e);
			conn.rollback();
			return false;
		} finally {
			if (conn != null) {
				try {
					conn.setAutoCommit(true);
				} catch (SQLException e) {
					logger.error("Unable to turn on Auto Commit for connection: " + conn, e);
				}
			}
			closeConnection(conn);
		}
		return true;
	}
	
	public static boolean insertUserRegulationCNMVIssue(long userId, long writerId, boolean isUnSuspendAction) throws SQLException {
		Connection conn = null;
		try {
			conn = getConnection();
			conn.setAutoCommit(false);
			Issue issue = null;
			if(isUnSuspendAction){
				List<Integer> issueActionTypes = new ArrayList<Integer>();
				issueActionTypes.add(IssuesManagerBase.ISSUE_ACTION_TYPE_CNMV_SUSPEND);
				issue = IssuesDAOBase.getUserIssueByIssueActionType(conn, userId, issueActionTypes);
			}
			
			// Create issue
			if(issue == null){
				issue = new Issue();			
				// Filling issue fields
				issue.setSubjectId(String.valueOf(ISSUE_SUBJECT_REGULATION));
				issue.setStatusId(String.valueOf(RISK_STATUS_CLOSED));
				issue.setUserId(userId);
				issue.setPriorityId(String.valueOf(IssuesManagerBase.ISSUE_PRIORITY_HIGH));
				issue.setType(ConstantsBase.ISSUE_TYPE_REGULAR);
			}												
			
			String issueActionComments = "Customer is required to provide signed CNMV document";	
			if(isUnSuspendAction){
				issueActionComments = "Customer has provided CNMV document";
			}			
			IssueAction issueAction = new IssueAction();
			issueAction.setActionTypeId(String.valueOf(IssuesManagerBase.ISSUE_ACTION_TYPE_CNMV_SUSPEND));
			if(isUnSuspendAction){
				issueAction.setActionTypeId(String.valueOf(IssuesManagerBase.ISSUE_ACTION_TYPE_CNMV_UNSUSPEND));
			}
			issueAction.setWriterId(writerId);
			issueAction.setActionTime(new Date());
			issueAction.setActionTimeOffset(UsersManagerBase.getUserUtcOffset(userId));
			issueAction.setComments(issueActionComments);				
			insertIssueAndIssueAction(conn, issue, issueAction);			
			logger.info("Successfully insert user regulation CNMV issue for userId: "+ userId);
		} catch (SQLException e) {
			logger.error("Couldn't insert user regulation CNMV issue for userId: "+ userId, e);
			conn.rollback();
			return false;
		} finally {
			if (conn != null) {
	        	try {
					conn.setAutoCommit(true);
				} catch (SQLException e) {
					logger.error("Unable to turn on Auto Commit for connection: " + conn, e);
				}
        	}
			closeConnection(conn);
		}
		return true;
	}
	
	public static Issue getIssueIdForBlankTemplate(long userId) throws SQLException {
		Connection conn = null;
		try {
			conn = getConnection();
			return IssuesDAOBase.getIssueIdForBlankTemplate(conn, userId);
		} finally {
			closeConnection(conn);
		}
	}
	
	public static void insertAutoBlockedCCIssue(long userId, String issueActionTimeOffset, String ccLastFourDigits, long creditCardID, ArrayList<User> users) throws SQLException {
		Connection conn = null;
		try {
			conn = getConnection();
			conn.setAutoCommit(false);
			Issue issue = null;
			long issueId = 0;
			issueId = getIssueIdByCreditCardId(creditCardID);
			if (issueId == 0) {
				issue = new Issue();
				issue.setSubjectId(String.valueOf(IssuesManagerBase.ISSUE_SUBJ_CC_CHANGED));
				issue.setStatusId(String.valueOf(IssuesManagerBase.ISSUE_STATUS_FINISHED));
				issue.setUserId(userId);
				issue.setPriorityId(String.valueOf(ISSUE_PRIORITY_HIGH));
				issue.setType(ConstantsBase.ISSUE_TYPE_REGULAR);
				if (creditCardID > 0) {
					issue.setCreditCardId(creditCardID);
				}
				IssuesDAOBase.insertIssue(conn, issue);
			}
			
			IssueAction issueAction = new IssueAction();
			issueAction.setActionTypeId(String.valueOf(IssuesManagerBase.ISSUE_ACTION_ALLOWED_CREDIT_CARD));
			issueAction.setWriterId(Writer.WRITER_ID_AUTO);
			issueAction.setSignificant(true);
			issueAction.setActionTime(new Date());
			issueAction.setActionTimeOffset(issueActionTimeOffset);
			issueAction.setChannelId(String.valueOf(IssuesManagerBase.ISSUE_CHANNEL_COMMENT));
			StringBuilder usersStringBuilder = null;
			if (users != null && users.size() > 0) {
				usersStringBuilder = new StringBuilder(String.valueOf(users.get(0).getId()));
				if (users.size() > 2) {
					usersStringBuilder.append(", ").append(String.valueOf(users.get(1).getId())).append(", ").append(String.valueOf(users.get(2).getId()));
				} if(users.size() == 2) {
					usersStringBuilder.append(", ").append(String.valueOf(users.get(1).getId()));
				}
			}
			String comments = "Blocked CC: " + ccLastFourDigits + ", used in other accounts: " + usersStringBuilder.toString() + " CardID: " + creditCardID;
			issueAction.setComments(comments);
			issueAction.setIssueId(issue.getId());
			IssuesDAOBase.insertAction(conn, issueAction);
			
			logger.info("Successfully insert Issue for user id " + userId + " with comment: " + comments);
			conn.commit();
		} catch (SQLException e) {
			logger.error("Unable insert auto blocked CC issue for user id " + userId + e);
			conn.rollback();
		} finally {
			if (conn != null) {
				try {
					conn.setAutoCommit(true);
				} catch (SQLException e) {
					logger.error("Unable to turn on Auto Commit for connection: " + conn, e);
				}
			}
			closeConnection(conn);
		}
	}
	
	public static Issue getUserIssueByIssueActionType(long userId, List<Integer> issueActionTypes) throws SQLException {
		Connection conn = null;
		try {
			conn = getConnection();
			return IssuesDAOBase.getUserIssueByIssueActionType(conn, userId, issueActionTypes);
		} finally {
			closeConnection(conn);
		}
	}
	
	public static long getIssueIdByCreditCardId(long creditCardId) throws SQLException {
		Connection con = null;
		try {
			con = getConnection();
			return IssuesDAOBase.getIssueIdByCreditCardId(con, creditCardId);
		} finally {
			closeConnection(con);
		}
	}
	
	public static Long insertSkipedWhtdBankUserDetailsIssueAction(long userId, long writerId) throws SQLException {
		Connection conn = null;
		Long res;
		try {
			conn = getConnection();
			conn.setAutoCommit(false);
			//Issue
			Issue issue = new Issue();
			issue.setSubjectId(String.valueOf(IssuesManagerBase.ISSUE_SUBJECT_REGULATION));
			issue.setStatusId(String.valueOf(IssuesManagerBase.ISSUE_STATUS_CLOSED));
			issue.setUserId(userId);
			issue.setPriorityId(String.valueOf(ISSUE_PRIORITY_LOW));
			issue.setType(ConstantsBase.ISSUE_TYPE_REGULAR);
			IssuesDAOBase.insertIssue(conn, issue);
			
			// Filling action fields
			IssueAction issueAction = new IssueAction();
			issueAction.setActionTypeId(String.valueOf(IssuesManagerBase.ISSUE_ACTION_TYPE_SKIPED_BANK_DETAILS));
			issueAction.setWriterId(writerId);
			issueAction.setSignificant(false);
			issueAction.setActionTime(new Date());
			issueAction.setActionTimeOffset(UsersManagerBase.getUserUtcOffset(userId));
			issueAction.setComments("User skipped \"the bank\\Skrill details\" popup");
			issueAction.setIssueId(issue.getId());

			IssuesDAOBase.insertAction(conn, issueAction);
			IssuesDAOBase.updateIssue(conn, issue);
			res = issueAction.getActionId();
			logger.info("Successfully insert action for skiped Bank User Details with id " + res);
			conn.commit();		
		} catch (SQLException e) {
			logger.error("Couldn't insert user regulation issue for userId: " + userId, e);
			conn.rollback();
			res = null;
		} finally {
			if (conn != null) {
				try {
					conn.setAutoCommit(true);
				} catch (SQLException e) {
					logger.error("Unable to turn on Auto Commit for connection: " + conn, e);
				}
			}
			closeConnection(conn);
		}
		return res;
	}

    public static boolean isPendingDocIssuesForUser(long userId) throws SQLException {
    	Connection con = null;
    	boolean result = false;
    	try {
    		con = getConnection();
    		result = IssuesDAOBase.isPendingDocIssuesForUser(con,userId);
 
    	}finally {
          closeConnection(con);    		
    	}
    	return result;
    }
    
	public static void createPendingDocsFiles(Connection con, IssueAction action, User user, Issue i, long creditCardId) {
		logger.debug("createPendingDocsFiles creditCardId:"+creditCardId);
		
		LinkedHashSet<Long> fileTypeHashMap = null;
    	fileTypeHashMap = new LinkedHashSet<Long>(); 
    	
		    fileTypeHashMap.add( FileType.USER_ID_COPY ); 
		    fileTypeHashMap.add( FileType.PASSPORT ); 
		    fileTypeHashMap.add( FileType.DRIVER_LICENSE ); 
		    fileTypeHashMap.add( FileType.UTILITY_BILL );
		    fileTypeHashMap.add( FileType.CC_COPY_FRONT );  
			fileTypeHashMap.add( FileType.CC_COPY_BACK );
	       
	    try {
	    	
	    	// check for existing Issue wit subject_id 56
	    	// second deposit
	    	if(IssuesManagerBase.isPendingDocIssuesForUser(user.getId())) {
	    		fileTypeHashMap.remove( FileType.USER_ID_COPY); 
	 		    fileTypeHashMap.remove( FileType.PASSPORT); 
	 		    fileTypeHashMap.remove( FileType.DRIVER_LICENSE); 
	 		    fileTypeHashMap.remove( FileType.UTILITY_BILL);
	 		    
	    		 if(!TransactionsManagerBase.isFirstDepositWithCC(user.getId(), creditCardId)) {
	 				 fileTypeHashMap.remove( FileType.CC_COPY_FRONT);
	    			 fileTypeHashMap.remove( FileType.CC_COPY_BACK);
	 			  } 
	    	
	    	}else {
	    		ArrayList<File> fileList = FilesDAOBase.getUserFilesByIdNoIssue(con, user.getId());
	    		if(null != fileList) {
	    			for(File f : fileList) {
	    				if(f.getFileStatusId() == File.STATUS_IN_PROGRESS) {
	    						FilesDAOBase.updateFileRecords(con, f, action.getActionId());
	    						IssuesManagerBase.InsertIssueRiskDocReq(action.getActionId(), ConstantsBase.RISK_DOC_REQ_REGULATION,0);
	    						fileTypeHashMap.remove(f.getFileTypeId());
	    			   }
	    			}
	    		 }
	    		  
	    		 long firstDepositId = UsersDAOBase.getFirstTransactionId(con, user.getId());
	    		 Transaction tran = TransactionsManagerBase.getTransaction(firstDepositId);
	    	     if(null !=tran) {
	    		 if (tran.getTypeId() != 1) {
	    			 fileTypeHashMap.remove( FileType.CC_COPY_FRONT);
	    			 fileTypeHashMap.remove( FileType.CC_COPY_BACK);
	    		 }
	    	  }
    	}
	    	
//		 for (Long key :  fileTypeHashMap) {		 
//			 if(key == FileType.CC_COPY_FRONT || key == FileType.CC_COPY_BACK){
//				 FilesDAOBase.insertFile(con,action.getActionId(), creditCardId, key, user.getId(), File.STATUS_REQUESTED, null, action.getWriterId());
//				 IssuesManagerBase.InsertIssueRiskDocReq(action.getActionId(), ConstantsBase.RISK_DOC_REQ_REGULATION, creditCardId);
//			 }else {
//				 FilesDAOBase.insertFile(con,action.getActionId(), 0, key, user.getId(), File.STATUS_REQUESTED, null, action.getWriterId());
//				 IssuesManagerBase.InsertIssueRiskDocReq(action.getActionId(), ConstantsBase.RISK_DOC_REQ_REGULATION,0);
//			 }
//		 }
		 
		 logger.debug("Try to set UsersPendicDocs");
		 UsersPendingDocsManagerBase.insertUsersPendingDocs(i.getUserId(), i.getId(), i.isMainRegulationIssue());
		 
		 logger.info(" Successfully insert regulation document need issue for user id: " + user.getId());
	    }catch(Exception ex) {
	    	
	    	logger.error(
					"couldn't insert regulation document need issue for user id: "
							+ user.getId(), ex);
	    }
		
	}
	
   public static boolean InsertIssueRiskDocReq(long issueActionId, String type, long ccId) throws SQLException {
	   Connection con = getConnection();
       try {
           if(IssuesDAOBase.InsertIssueRiskDocReq(con, issueActionId,type,ccId)) {
        	   return true;
           } 
       } catch (Exception e) {
           logger.log(Level.ERROR, "Can't insertDocRequest: " + issueActionId, e);
           return false;
       } finally {
           closeConnection(con);
       }
       return true;
   }

   public static void insertIssue(Issue issue, IssueAction issueAction,String utcOffset,long writerId, User user, int screenId, long creditCardId, Connection conn) throws Exception {
        // Insert Issue
        IssuesDAOBase.insertIssue(conn, issue);

        issueAction.setIssueId(issue.getId());
        issueAction.setWriterId(writerId);
        issueAction.setActionTime(new Date());
        issueAction.setActionTimeOffset(utcOffset);
        validateAndInsertAction(conn, issueAction, user, issue, screenId, creditCardId);
    }
   
	public static boolean validateAndInsertAction(Connection con, IssueAction action, User user, Issue i, int screenId, long creditCardId) throws Exception{
        boolean res = true;
        IssuesDAOBase.insertAction(con, action);
        if ( ISSUE_SUBJ_PENDING_DOCS.equals(i.getSubjectId()) ) {
        	createPendingDocsFiles(con, action, user, i, creditCardId);
        }
        return res;
  }
	   
}
