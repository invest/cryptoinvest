/**
 *
 */
package com.anyoption.common.managers;

import java.sql.Connection;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.EmirReport;
import com.anyoption.common.beans.Market;
import com.anyoption.common.beans.MarketBean;
import com.anyoption.common.beans.MarketConfig;
import com.anyoption.common.beans.MarketFormula;
import com.anyoption.common.beans.MarketNameBySkin;
import com.anyoption.common.beans.MarketPauseDetails;
import com.anyoption.common.beans.MarketPriority;
import com.anyoption.common.beans.base.MarketGroup;
import com.anyoption.common.daos.MarketsDAOBase;
import com.anyoption.common.util.CommonUtil;

/**
 * @author pavelhe
 *
 */
public class MarketsManagerBase extends BaseBLManager {
	private static final Logger log = Logger.getLogger(MarketsManagerBase.class);

	private static Hashtable<Long, Market> markets = new Hashtable<Long, Market>();
	protected static HashMap<Long, HashMap<Long, MarketNameBySkin>> marketNamesBySkin = new HashMap<Long, HashMap<Long, MarketNameBySkin>>();
	private static Map<Integer, List<MarketFormula>> marketFormulas;
	private static HashMap<Long, Hashtable<Long, MarketGroup>> skinGroupsMarkets = new HashMap<Long, Hashtable<Long, MarketGroup>>();
	private static HashMap<Long, LocalDate> skinsDates = new HashMap<Long, LocalDate>();

	private static LocalDate marketNamesBySkinDate;
	private static LocalDate dateMarket;
	
	public static final Long MARKET_DATA_SOURCE_REUTERS = 1L;
	
	public static final String FILTER_MARKETS = "markets";

	public static Hashtable<Long, Market> getAll() throws SQLException {
        Connection conn = null;
        try {
            conn = getConnection();
            return MarketsDAOBase.getAll(conn);
        } finally {
            closeConnection(conn);
        }
    }

	public static ArrayList<MarketPriority> getMarketsByPriority(long skinId) {
		Connection conn = null;
		try {
			conn = getConnection();
			return MarketsDAOBase.getMarketsByPriority(conn, skinId);
		} catch (SQLException sqle) {
			log.error("Can't load markets by priority: ", sqle);
		} finally {
			closeConnection(conn);
		}
		return null;
	}

	public static void updateMarketPriority(ArrayList<Long> skinMarketGroupMarketIds) {
		Connection conn = null;
		try {
			conn = getConnection();
			conn.setAutoCommit(false);
			for (int i = 0; i < skinMarketGroupMarketIds.size(); i++) {
				MarketsDAOBase.updatetMarketPriority(conn, i + 1, skinMarketGroupMarketIds.get(i));
			}
			conn.commit();
		} catch (SQLException sqle) {
			rollback(conn);
			log.error("Can't update markets by priority: ", sqle);
		} finally {
			setAutoCommitBack(conn);
			closeConnection(conn);
		}
	}
	
	public static void insertMarketPriority(ArrayList<Long> newMarketsIds, long selectedSkinId, long homePagePriority) {
		Connection conn = null;
		try {
			conn = getConnection();
			MarketsDAOBase.insertMarketPriority(conn, selectedSkinId, newMarketsIds, homePagePriority);
		} catch (SQLException sqle) {
			log.error("Can't insert new markets priorities: ", sqle);
		} finally {
			closeConnection(conn);
		}
	}

	public static Market getMarket(long id) {
		LocalDate currentDate = LocalDateTime.now().toLocalDate();
		if (dateMarket == null || currentDate.isAfter(dateMarket) || (markets.get(id) == null)) {
			synchronized (markets) {
				if (dateMarket == null || currentDate.isAfter(dateMarket) || (markets.get(id) == null)) {
					try {
						Hashtable<Long, Market> tmp = MarketsManagerBase.getAll();
						markets.clear();
						markets.putAll(tmp);
						dateMarket = currentDate;
					} catch (SQLException sqle) {
						log.error("Can't load markets.", sqle);
					}
				}
			}
		}
		return markets.get(id);
	}
	
	public static ArrayList<Market> getAllBubblesMarkets() throws SQLException {
        Connection conn = null;
        try {
            conn = getConnection();
            return MarketsDAOBase.getMarketsByType(conn, Market.PRODUCT_TYPE_BUBBLES);
        } finally {
            closeConnection(conn);
        }
    }

	public static Market getMarketLimitFromAmount(long marketId) {
		 Connection con = null;
	        try {
	            try {
					con = getConnection();
					return MarketsDAOBase.getMarketLimitFromAmount(con, marketId);
				} catch (SQLException e) {
					log.error("Unable to load market exposure limits for high amount", e);
					return new Market();
				}
	        } finally {
	        	closeConnection(con);
	        }
	}
	
//	public static HashMap<Long, Market> getMarkets() {
//		if(marketss == null) {
//			Connection con = null;
//			try {
//				con = getConnection();		
//				marketss = new HashMap<Long, Market>();
//				ArrayList<Market> list = MarketsDAOBase.getMarkets(con);
//				for (int i = 0; i < list.size(); i++) {
//					Market m = (Market) list.get(i);
//					marketss.put(new Long(m.getId()), m);
//				}
//			}catch(SQLException ex) {
//				log.error("Can't lazzy load markets", ex );
//			} finally {
//				closeConnection(con);
//			}
//		}
//		return marketss;
//	}
	
    public static ArrayList<Long> getCurrentMarketsByFeedName(long skinId, boolean onlyOpened, String marketFeedName) throws SQLException {
        Connection conn = null;
        try {
            conn = getConnection();
            return MarketsDAOBase.getCurrentMarketsByFeedName(conn, skinId, onlyOpened, marketFeedName);
        } finally {
            closeConnection(conn);
        }
    }
    
	public static ArrayList<Market> getMarkets(long typeId) {
        Connection conn = null;
        try {
            conn = getConnection();
            return MarketsDAOBase.getMarketsByType(conn, typeId);
        } catch (SQLException e) {
			log.error("Can't load markets:"+ typeId, e);
			return null;
		} finally {
            closeConnection(conn);
        }
    }
	

	public static Hashtable<Long, MarketGroup> getSkinGroupsMarkets(long skinId) throws SQLException {
		LocalDate currentDate = LocalDateTime.now().toLocalDate();
		if (null == skinsDates.get(skinId)
				|| currentDate.isAfter(skinsDates.get(skinId))
				|| null == skinGroupsMarkets.get(skinId)) {
			synchronized (skinGroupsMarkets) {
				if (null == skinsDates.get(skinId)
						|| currentDate.isAfter(skinsDates.get(skinId))
						|| null == skinGroupsMarkets.get(skinId)) {
					Connection con = null;
					try {
						con = getConnection();
						skinGroupsMarkets.put(skinId, MarketsDAOBase.getSkinGroupsMarkets(con, skinId));
						skinsDates.put(skinId, LocalDateTime.now().toLocalDate());
					} finally {
						closeConnection(con);
					}
				}				
			}
		}
		return skinGroupsMarkets.get(skinId);
	}

    /**
     * if market time created is less then 3 months return true else false
     * @return true if its new else false
     */
    public static boolean isNewMarket(com.anyoption.common.beans.base.Market m) {
        Calendar createTime = Calendar.getInstance();
        createTime.setTime(m.getTimeCreated());
        Calendar monthsAgo = createTime;
        monthsAgo.set(Calendar.MONTH, createTime.get(Calendar.MONTH) + 3);
        Calendar timeNow = Calendar.getInstance();
        if (monthsAgo.after(timeNow)) {
            return true;
        }
        return false;
    }
    
    public static Map<Long, Map<Long, String>> getMarketNamesByTypeAndSkin(long skinId) throws SQLException {
    	Connection conn = null;
        try {
            conn = getConnection();
            return MarketsDAOBase.getMarketNamesByTypeAndSkin(conn, skinId);
        } finally {
            closeConnection(conn);
        }
    }
    
    public static long copyMarket(long marketId, long writerId) throws SQLException {
    	Connection conn = null;
        try {
            conn = getConnection();
            return MarketsDAOBase.copyMarket(conn, marketId, writerId);
        } finally {
            closeConnection(conn);
        }
    }
    
    public static MarketBean getMarketById(long marketId) throws SQLException {
    	Connection conn = null;
        try {
            conn = getConnection();
            return MarketsDAOBase.getMarketById(conn, marketId);
        } finally {
            closeConnection(conn);
        }
    }
    
    public static void updateMarketById(MarketBean market) throws SQLException {
    	Connection conn = null;
        try {
            conn = getConnection();
            MarketsDAOBase.updateMarketById(conn, market);
        } finally {
            closeConnection(conn);
        }
    }
    
    public static MarketConfig getMarketConfigFormulas(long marketId, long dataSourceId) throws SQLException {
    	Connection conn = null;
        try {
            conn = getConnection();
            return MarketsDAOBase.getMarketConfigFormulas(conn, marketId, dataSourceId);
        } finally {
            closeConnection(conn);
        }
    }

	/**
	 * Updates market configuration formulas and formula parameters. The method will not update the configuration if the id of the formula
	 * is invalid.
	 * 
	 * @param marketConfig the configuration that should be updated
	 * @param marketId the id of the configuration's market
	 * @param dataSourceId the id of the data provider
	 * @return {@code true} if the update was successful, {@code false} otherwise
	 * @throws SQLException if database error occurs
	 */
	public static boolean updateMarketConfigFormulas(MarketConfig marketConfig, long marketId, long dataSourceId) throws SQLException {
		Connection conn = null;
		try {
			if (marketConfig.getHourLevelFormulaId() <= 0	|| marketConfig.getHourClosingFormulaId() <= 0
					|| marketConfig.getDayClosingFormulaId() <= 0
					|| (marketConfig.getLongTermFormulaId() != null && marketConfig.getLongTermFormulaId() < 0)
					|| marketConfig.getRealLevelFormulaId() <= 0) {
				log.debug("Received negative or zero for formula id");
				return false;
			} else if (marketConfig.getLongTermFormulaId() == null || marketConfig.getLongTermFormulaId() == 0L) {
				// some markets don't have long term formula
				marketConfig.setLongTermFormulaId(null);
				marketConfig.setLongTermFormulaParams(null);
			}
			conn = getConnection();
			MarketsDAOBase.updateMarketConfigFormulas(conn, marketConfig, marketId, dataSourceId);
			return true;
		} finally {
			closeConnection(conn);
		}
	}
    
    public static HashMap<Long, HashMap<Long, MarketNameBySkin>> getMarketNamesBySkin() {
    	if (marketNamesBySkin.isEmpty()
    			|| LocalDateTime.now().toLocalDate().isAfter(marketNamesBySkinDate)) {
    		synchronized (marketNamesBySkin) {
    	    	if (marketNamesBySkin.isEmpty()
    	    			|| LocalDateTime.now().toLocalDate().isAfter(marketNamesBySkinDate)) {
    	    		Connection con = null;
    	    		try {
    	    			con = getConnection();
    	    			HashMap<Long, HashMap<Long, MarketNameBySkin>> tmp = MarketsDAOBase.getMarketNamesBySkin(con);
						marketNamesBySkin.clear();    			
						marketNamesBySkin.putAll(tmp);
						marketNamesBySkinDate = LocalDateTime.now().toLocalDate();
    	    		} catch (SQLException sqle) {
    					log.error("Cannot load market names! ", sqle);
    				} finally {
    					closeConnection(con);
    				}
    	    	}    			
    		}
    	}
		return marketNamesBySkin;
    }
    
	public static String getMarketName(long skinId, long marketId) {
		HashMap<Long, MarketNameBySkin> m = getMarketNamesBySkin().get(marketId);
		if (m != null
				&& m.get(skinId) != null
				&& m.get(skinId).getName() != null) {
			return m.get(skinId).getName();
		} else {
			return "market.id" + marketId + ".skinId" + skinId;
		}
	}
    
	public static String getMarketShortName(long skinId, long marketId) {
		HashMap<Long, MarketNameBySkin> m = getMarketNamesBySkin().get(marketId);
		if (m != null
				&& m.get(skinId) != null
				&& m.get(skinId).getShortName() != null) {
			return m.get(skinId).getShortName();
		} else {
			return "market.id" + marketId + ".skinId" + skinId;
		}
	}

	/**
	 * Returns a map containing the market formulas. The formulas are mapped by their type. For now the {@code type = 3}, which is
	 * {@code dynamics/0100} formula type is not included.
	 * 
	 * @param locale the locale which to be used for translating the formula description
	 * @return mapping of the market formulas by formula type
	 */
	public static Map<Integer, List<MarketFormula>> getMarketFormulas(Locale locale) {
		if (marketFormulas == null) {
			Connection con = null;
			try {
				con = getConnection();
				marketFormulas = MarketsDAOBase.getMarketFormulas(con);
				for (List<MarketFormula> formulas : marketFormulas.values()) {
					Iterator<MarketFormula> iterator = formulas.iterator();
					while (iterator.hasNext()) {
						MarketFormula formula = iterator.next();
						formula.setDescription(CommonUtil.getMessage(locale, formula.getDescriptionKey(), null));
					}
				}
			} catch (SQLException e) {
				log.debug("Unable to load market formulas", e);
				return new HashMap<>();
			} finally {
				closeConnection(con);
			}
		}
		return marketFormulas;
	}
	
	public static ArrayList<MarketPauseDetails> getMarketPausesByMarketId(long marketId) throws SQLException {
		Connection con = null;
		ArrayList<MarketPauseDetails> marketPauseDetailsList = null;
		try {
			con = getConnection();
			marketPauseDetailsList = MarketsDAOBase.getMarketPausesByMarketId(con, marketId);
		} finally {
			closeConnection(con);
		}
		return marketPauseDetailsList;
	}
	
	public static MarketPauseDetails getMarketPauseById(long marketPauseId) throws SQLException {
		Connection con = null;
		MarketPauseDetails marketPause = null;
		try {
			con = getConnection();
			marketPause = MarketsDAOBase.getMarketPauseById(con, marketPauseId);
		} finally {
			closeConnection(con);
		}
		return marketPause;
	}
	
	public static void updateInsertMarketPause(MarketPauseDetails marketPauseDetails) throws SQLException {
		Connection con = null;
		try {
			con = getConnection();
			MarketsDAOBase.updateInsertMarketPause(con, marketPauseDetails);
		} finally {
			closeConnection(con);
		}
	}

	public static void insertMarketForEmirReport(EmirReport emirReport) throws SQLException {
		Connection con = null;
		try {
			con = getConnection();
			MarketsDAOBase.insertMarketForEmirReport(con, emirReport);
		} finally {
			closeConnection(con);
		}
		
	}

	public static EmirReport getMarketEmirReportById(Long marketId) throws SQLException {
		Connection con = null;
		EmirReport emirReport = null;
		try {
			con = getConnection();
			emirReport = MarketsDAOBase.getMarketEmirReportById(con, marketId);
		} finally {
			closeConnection(con);
		}
		return emirReport;
	}
}