package com.anyoption.common.managers;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;

import org.apache.log4j.Logger;

import com.anyoption.common.daos.WriterPermisionsDAOBase;

public class WriterPermisionsManagerBase extends BaseBLManager {

	private static final Logger logger = Logger.getLogger(WriterPermisionsManagerBase.class);	
	
	public static ArrayList<String> getWriterScreenPermissions(String screenName, long writerId) throws SQLException {
		logger.debug("Get setings for Writer with id: " + writerId);		
		Connection conn = null;
		try {
			conn = getConnection();
			return WriterPermisionsDAOBase.getWriterScreenPermissions(conn, writerId, screenName);
		} finally {
			closeConnection(conn);
		}
	}	
}
