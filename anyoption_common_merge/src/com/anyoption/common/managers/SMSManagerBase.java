package com.anyoption.common.managers;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Date;
import java.util.Locale;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.Issue;
import com.anyoption.common.beans.IssueAction;
import com.anyoption.common.beans.User;
import com.anyoption.common.beans.base.User.MobileNumberValidation;
import com.anyoption.common.daos.SMSDAOBase;
import com.anyoption.common.sms.SMS;
import com.anyoption.common.sms.SMSException;
import com.anyoption.common.util.CommonUtil;
import com.anyoption.common.util.ConstantsBase;
import com.anyoption.common.util.GenerateShortenURL;


/**
 * @author kirilim
 */
public class SMSManagerBase extends BaseBLManager {

	private static final Logger log = Logger.getLogger(SMSManagerBase.class);
	public static final long SMS_KEY_TYPE_INVESTMENT = 1;
    public static final long SMS_KEY_TYPE_USERID = 2;
    public static final long SMS_KEY_TYPE_SMARTPHONE = 3;
    public static final long SMS_KEY_TYPE_CONTACT = 4;
    public static final long SMS_DEST_PORT = -1;
    public static final String SMS_VERIFY_PHONE_DELIMITER = "P4N";
    
    public static void sendTextMessage(String sender, String senderNumber, String phone, String message, Connection con, long keyValue, long keyType, long providerId, long descriptionId, MobileNumberValidation mobileNumberValidation) throws SMSException {
        sendMessage(SMS.TYPE_TEXT, sender, senderNumber, phone, message, null, -1, con, keyValue, keyType, providerId, descriptionId, mobileNumberValidation);
    }

	public static long sendMessage(	long typeId, String sender, String senderNumber, String phone, String message, String wapPushURL,
									long dstPort, Connection conn, long keyValue, long keyType, long providerId,
									long descriptionId, MobileNumberValidation mobileNumberValidation) throws SMSException {
		String ls = System.getProperty("line.separator");
		if (log.isTraceEnabled()) {
			log.trace("going to add queued sms, typeId:"+ typeId + ls + "phone: " + phone + ls + "message: " + message + ls
						+ "wapPushURL: " + wapPushURL + ls + "dstPort: " + dstPort + ls);
		}
		try {
			long initialStatus = SMS.STATUS_QUEUED;
			if(providerId == ConstantsBase.MOBIVATE_PROVIDER) {
				if(mobileNumberValidation == MobileNumberValidation.NOTVALIDATED) {
					initialStatus = SMS.STATUS_NOT_VERIFIED;
				} else if( mobileNumberValidation == MobileNumberValidation.INVALID ) {
					log.error("Abort sending sms to invalid number"  );
					return 0 ;
				}
			}
			return SMSDAOBase.queueMessage(	conn, typeId, sender, senderNumber, phone, message, wapPushURL, dstPort, keyValue, keyType,
											providerId, descriptionId, initialStatus);
		} catch (Exception e) {
			throw new SMSException("Failed to queue message.", e);
		}
	}
	
	public static long sendTextMessage(String sender, String senderNumber, String phone, String message, long keyValue, long keyType, long providerId, long descriptionId, MobileNumberValidation mobileNumberValidation) throws SMSException {
	  return sendMessage(SMS.TYPE_TEXT, sender, senderNumber, phone, message, null, -1, keyValue, keyType, providerId, descriptionId, mobileNumberValidation);
	}

	public static void sendWAPPushMessage(String sender, String senderNumber, String phone, String message, String wapPushURL, long keyValue, long keyType, long providerId, long descriptionId, MobileNumberValidation mobileNumberValidation) throws SMSException {
	  sendMessage(SMS.TYPE_WAP_PUSH, sender, senderNumber, phone, message, wapPushURL, -1, keyValue, keyType, providerId, descriptionId, mobileNumberValidation);
	}

	public static long sendMessage(long typeId, String sender, String senderNumber, String phone, String message, String wapPushURL,
			long dstPort, long keyValue, long keyType, long providerId, long descriptionId, MobileNumberValidation mobileNumberValidation) throws SMSException {
	  Connection conn = null;
	  try {
	      conn = getConnection();
	      return sendMessage(typeId, sender, senderNumber, phone, message, wapPushURL, dstPort, conn, keyValue, keyType, providerId, descriptionId, mobileNumberValidation);
	  } catch (Exception e) {
	      throw new SMSException("Failed to queue message.", e);
	  } finally {
	      closeConnection(conn);
	  }
	}
	
	public static Long getUserIdByMid(long mid) {
		Connection conn = null;
		  try {
		      conn = getConnection();
		      return SMSDAOBase.getUserIdByMid(conn, mid);
		  } catch (Exception e) {
		      return null;
		  } finally {
		      closeConnection(conn);
		  }
	}

	public static boolean sendPhoneNumberVerificationMessage(com.anyoption.common.beans.base.User user, Locale locale, int platformId) {
		if (user == null || user.getId() == 0) {
			log.error("User is null. Verification message can not be send");
			return false;
		}
		try {
			String senderNumber = CountryManagerBase.getById(user.getCountryId()).getSupportPhone();
			String phoneCode = CountryManagerBase.getPhoneCodeById(user.getCountryId());
			String senderName;
			String message;
			String longUrl = CommonUtil.getProperty("sms.verify.phone.link")	+ user.getId() + SMS_VERIFY_PHONE_DELIMITER
								+ user.getMobilePhone() + SMS_VERIFY_PHONE_DELIMITER + locale.getLanguage();
			String shortUrl = GenerateShortenURL.shorten(longUrl);
			// Produces something like 'https://goo.gl/8wXhJS', 'https://goo.gl/OpA9eC' - 21 chars
			if (shortUrl == null) {
				log.error("Got null from com.anyoption.common.util.GenerateShortenURL.shorten(String)");
				return false;
			}
			switch (platformId) {
			case ConstantsBase.PLATFORM_ID_ANYOPTION:
				senderName = "anyoption";
				message = CommonUtil.getMessage(locale, "sms.verify.phone", new Object[] {shortUrl});
				break;
			case ConstantsBase.PLATFORM_ID_COPYOP:
				senderName = "copyop";
				message = CommonUtil.getMessage(locale, "sms.verify.phone.copyop", new Object[] {shortUrl});
				break;
			default:
				senderName = "backend";
				message = CommonUtil.getMessage(locale, "sms.verify.phone", new Object[] {shortUrl});
				break;
			}
			if (message == null || message.equals("")) {
				log.error("Unable to get message from bundle");
				return false;
			}
			long smsId = sendTextMessage(	senderName, senderNumber, phoneCode + user.getMobilePhone(), message, user.getId(),
											SMSManagerBase.SMS_KEY_TYPE_USERID, ConstantsBase.MOBIVATE_PROVIDER, SMS.DESCRIPTION_WELCOME,
											MobileNumberValidation.NOTVALIDATED);
			if (smsId == 0) {
				log.error("Message id is 0");
				return false;
			}
			return true;
		} catch (SQLException | SMSException e) {
			log.error("Unable to send verification message for user: " + user.getId(), e);
			return false;
		}
	}

	public static void insertIssueForSMS(User user, Date timeSend, String msg, long smsId) {
		Connection conn = null;
		Issue issue = new Issue();
		issue.setSubjectId(String.valueOf(IssuesManagerBase.ISSUE_SUBJECT_REGISTRATION_SMS));
		issue.setStatusId(String.valueOf(IssuesManagerBase.ISSUE_STATUS_CLOSED));
		issue.setPriorityId(String.valueOf(IssuesManagerBase.ISSUE_PRIORITY_LOW));
		issue.setUserId(user.getId());
		issue.setContactId(user.getContactId());
		// issue.setType(ConstantsBase.ISSUE_TYPE_REGULAR);
		// Filling action fields
		IssueAction issueAction = new IssueAction();
		issueAction.setChannelId(String.valueOf(IssuesManagerBase.ISSUE_CHANNEL_SMS)); // SMS
		issueAction.setActionTime(timeSend);
		issueAction.setActionTypeId(String.valueOf(IssuesManagerBase.ISSUE_ACTION_SMS));
		issueAction.setSignificant(false);
		issueAction.setComments(msg);
		issueAction.setSmsId(smsId);
		issueAction.setWriterId(user.getWriterId());
		issueAction.setCallDirectionId(IssueAction.ISSUE_DIRECTION_OUT_BOUND_PHONE_CALL);
		issueAction.setActionTimeOffset(user.getUtcOffset());
		try {
			// Insert Issue & action
			conn = getConnection();
			com.anyoption.common.managers.IssuesManagerBase.insertIssueAndIssueAction(conn, issue, issueAction);
		} catch (SQLException e) {
			log.error("Unable to insert issue for user with id : " + user.getId(), e);
		} finally {
			closeConnection(conn);
		}
	}

	public static int getMobileNumberValidation(long userId) {
		Connection con = null;
		try {
			con = getConnection();
			return SMSDAOBase.getMobileNumberValidation(con, userId);
		} catch (SQLException e) {
			log.debug("Unable to load mobile number validation for user with id " + userId + ". Returning 0", e);
			return 0;
		} finally {
			closeConnection(con);
		}
	}
}