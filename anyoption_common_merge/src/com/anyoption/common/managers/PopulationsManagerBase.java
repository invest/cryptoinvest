package com.anyoption.common.managers;


import java.sql.Connection;
import java.sql.SQLException;

import org.apache.log4j.Logger;

import com.anyoption.common.bl_vos.PopulationEntryBase;
import com.anyoption.common.daos.PopulationsDAOBase;

/**
 * PopulationsManagerBase class
 * 		used also for keeping all population handlers
 * @author Eyal g
 *
 */
public abstract class PopulationsManagerBase extends BaseBLManager {
	private static final Logger log = Logger.getLogger(PopulationsManagerBase.class);

	public static final int POP_ENT_HIS_STATUS_MOVING_UPGRADE_TO_RETENTION = 654;
	
	public static PopulationEntryBase getPopulationUserByUserId(long userId) throws SQLException {
		Connection con = null;
		try {
			con = getConnection();
			return PopulationsDAOBase.getPopulationUserByUserId(con, userId);
		} finally {
			closeConnection(con);
		}
	}
}