package com.anyoption.common.managers;

import java.sql.Connection;
import java.sql.SQLException;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.ServerPixelsPublisherConfig;
import com.anyoption.common.daos.ServerPixelsPublisherConfigDAOBase;

public class ServerPixelsPublisherConfigManagerBase extends BaseBLManager {
	private static final Logger logger = Logger.getLogger(ServerPixelsPublisherConfigManagerBase.class);
	
	/**
	 * 
	 * @param sppc
	 * @throws SQLException
	 */
	public static void getServerPixelsPublisherConfig(ServerPixelsPublisherConfig sppc) throws SQLException {
		if (null == sppc) {
			throw new NullPointerException("The object of ServerPixelsPublisherConfig is null");
		}
		Connection conn = getConnection();
		try {
			ServerPixelsPublisherConfigDAOBase.getServerPixelsPublisherConfig(conn, sppc);
		} finally {
			closeConnection(conn);
		}
	}
}
