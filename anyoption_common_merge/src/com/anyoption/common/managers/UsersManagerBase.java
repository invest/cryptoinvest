/**
 *
 */
package com.anyoption.common.managers;

import java.io.UnsupportedEncodingException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.bouncycastle.crypto.CryptoException;

import com.anyoption.common.beans.BubblesToken;
import com.anyoption.common.beans.File;
import com.anyoption.common.beans.InvestmentRejects;
import com.anyoption.common.beans.MailBoxTemplate;
import com.anyoption.common.beans.ServerConfiguration;
import com.anyoption.common.beans.Transaction;
import com.anyoption.common.beans.User;
import com.anyoption.common.beans.UserMigration;
import com.anyoption.common.beans.base.Currency;
import com.anyoption.common.beans.base.MarketingInfo;
import com.anyoption.common.beans.base.Skin;
import com.anyoption.common.beans.base.User.MobileNumberValidation;
import com.anyoption.common.beans.base.UsersDetailsHistory;
import com.anyoption.common.daos.FilesDAOBase;
import com.anyoption.common.daos.MailBoxTemplatesDAOBase;
import com.anyoption.common.daos.TransactionsDAOBase;
import com.anyoption.common.daos.UsersDAOBase;
import com.anyoption.common.util.AESUtil;
import com.anyoption.common.util.CommonUtil;
import com.anyoption.common.util.ConstantsBase;
//import com.copyop.common.beans.UserUniqueDetails;

/**
 * @author kirilim
 *
 */
public class UsersManagerBase extends BaseBLManager {
	private static final Logger logger = Logger.getLogger(UsersManagerBase.class);

	public static final long USERS_DETAILS_HISTORY_TYPE_FIRST_NAME = 1;
	public static final long USERS_DETAILS_HISTORY_TYPE_LAST_NAME = 2;
	public static final long USERS_DETAILS_HISTORY_TYPE_ID_NUM = 3;
	public static final long USERS_DETAILS_HISTORY_TYPE_GENDER = 4;
	public static final long USERS_DETAILS_HISTORY_TYPE_EMAIL = 5;
	public static final long USERS_DETAILS_HISTORY_TYPE_BIRTHDATE = 6;
	public static final long USERS_DETAILS_HISTORY_TYPE_MOBILE = 7;
	public static final long USERS_DETAILS_HISTORY_TYPE_PHONE = 8;
	public static final long USERS_DETAILS_HISTORY_TYPE_COUNTRY = 9;
	public static final long USERS_DETAILS_HISTORY_TYPE_LANGUAGE = 10;
	public static final long USERS_DETAILS_HISTORY_TYPE_RETENTION_REP = 11;
	public static final long USERS_DETAILS_HISTORY_TYPE_PASSWORD = 12;
	public static final long USERS_DETAILS_HISTORY_TYPE_USER_TYPE = 13;
	public static final long USERS_DETAILS_HISTORY_TYPE_CREDIT_CARD_NUMBER = 14;
	public static final long USERS_DETAILS_HISTORY_TYPE_CONTACT_BY_EMAIL = 15;
	public static final long USERS_DETAILS_HISTORY_TYPE_CONTACT_BY_SMS = 16;
	public static final long USERS_DETAILS_HISTORY_TYPE_STREET_ADDRESS = 17;
	public static final long USERS_DETAILS_HISTORY_TYPE_CITY = 18;
	public static final long USERS_DETAILS_HISTORY_TYPE_SKIN = 19;
	public static final long USERS_DETAILS_HISTORY_TYPE_STATE = 20;
	public static final long USERS_DETAILS_HISTORY_TYPE_SEND_DEPOSIT_DOCUMENTS = 21;
	public static final long USERS_DETAILS_HISTORY_TYPE_EMAIL_AUTHORIZED = 22;
	public static final long USERS_DETAILS_HISTORY_TYPE_RISKY_USER = 23;
	public static final long USERS_DETAILS_HISTORY_TYPE_USER_DISABLE_ACTIVE = 24;
	public static final long USERS_DETAILS_HISTORY_TYPE_SPECIAL_CARE = 25;
	public static final long USERS_DETAILS_HISTORY_TYPE_STOP_REVERSE_WITHDRAW_OPTION = 26;
	public static final long USERS_DETAILS_HISTORY_TYPE_IMMEDIATE_WITHDRAW = 27;
	public static final long USERS_DETAILS_HISTORY_TYPE_WAIVE_DEPOSITS_VELOCITY_CHECK = 28;
	public static final long USERS_DETAILS_HISTORY_TYPE_USER_DISABLE_DEV3 = 29;
	public static final long USERS_DETAILS_HISTORY_TYPE_USERNAME = 30;
	public static final long USERS_DETAILS_HISTORY_TYPE_USER_IS_ACTIVE = 31;
	public static final long USERS_DETAILS_HISTORY_TYPE_USER_BUBBLES_PRICING_LEVEL = 32;

//	public static final String USERS_DETAILS_TYPE_ID = "type";
//    public static final String USERS_DETAILS_FIELD_BEFORE = "before";
//    public static final String USERS_DETAILS_FIELD_AFTER = "after";

	public static final long USER_STATUS_ALL = 0;
	public static final long USER_STATUS_ACTIVE = 1;
    public static final long USER_STATUS_SLEEPER = 2;
    public static final long USER_STATUS_COMA = 3;
    
    public static final long USER_DEFAULT_BUBBLES_PRICING_LEVEL = 0;

    /**
     * This method loads user info specifically for the login logic
     * @param name
     * @param user
     * @throws SQLException
     */
    public static void loadUserByName(String name, User user) throws SQLException {
    	if (user == null) {
    		throw new NullPointerException("The user parameter cannot be null");
    	}
        Connection conn = getConnection();
        try {
            UsersDAOBase.loadUserByName(conn, name, user);
        } finally {
            closeConnection(conn);
        }
    }

	public static boolean isEmailInUse(String email, long userId) throws SQLException {
		Connection con = getConnection();
		try {
			return UsersDAOBase.isEmailInUse(con, email, userId);
		} finally {
			closeConnection(con);
		}
	}

	public static boolean isEmailInUseIgnoreCase(String email) throws SQLException {
		Connection con = getConnection();
		try {
			return UsersDAOBase.isEmailInUseIgnoreCase(con, email);
		} finally {
			closeConnection(con);
		}
	}
	
	
	public static long updateUserFields(long userId, 
										long currencyId,
										String streetAddr, 
										String zipCode, 
										String cityName, 
										long skinId,
										Date birthDate) throws SQLException {
		Connection conn = getConnection();
		try {
			return UsersDAOBase.updateUserFields(conn, 
													userId, 
													currencyId,
													streetAddr, 
													zipCode, 
													cityName, 
													skinId, 
													birthDate);
		} finally {
			closeConnection(conn);
		}
	}


	public static long updateUserFields(long userId,
											long currencyId,
											String streetAddr,
											String streetNo,
											String zipCode,
											String cityName,
											long skinId,
											Date birthDate,
											String idNum,
											String gender,
											long cityId) throws SQLException {
		Connection conn = getConnection();
		try {
			return UsersDAOBase.updateUserFields(conn,
													userId,
													currencyId,
													streetAddr,
													streetNo,
													zipCode,
													cityName,
													skinId,
													birthDate,
													idNum,
													gender,
													cityId);
		} finally {
			closeConnection(conn);
		}
	}

    /**
     * return user balance from the data base by given userId.
     * @param conn
     * @param userId
     * @return
     * @throws SQLException
     */
    public static long getUserBalanceFromDB(Connection conn, long userId) throws SQLException {
	if (conn == null) {
	    conn = getConnection();
	}
	try {
	    return UsersDAOBase.getUserBalance(conn, userId);
	} catch (SQLException sqle) {
	    logger.log(Level.ERROR, "Error! problem to get user balance from db.", sqle);
	    throw sqle;
	} finally {
	    closeConnection(conn);
	}
    }

    /**
	 * Get user by login token
     * @param userId
     * @return
     * @throws SQLException
     * @throws CryptoException
     * @throws NoSuchPaddingException 
     * @throws NoSuchAlgorithmException 
     * @throws BadPaddingException 
     * @throws IllegalBlockSizeException 
     * @throws InvalidKeyException 
     * @throws InvalidAlgorithmParameterException 
     */
	public static User getUserByLoginToken(String uuid, long userId)	throws SQLException, CryptoException, InvalidKeyException,
																		IllegalBlockSizeException, BadPaddingException,
																		NoSuchAlgorithmException, NoSuchPaddingException,
																		InvalidAlgorithmParameterException {
        Connection conn = getConnection();
        try {
        	return UsersDAOBase.getUserByLoginToken(conn, uuid, userId);
        } finally {
            closeConnection(conn);
        }
    }

    public static boolean updateUserSkin(long userId, long newSkinId) throws SQLException {
        Connection con = getConnection();
        try {
        	return UsersDAOBase.updateUserSkin(con, userId, newSkinId);
        } finally {
            closeConnection(con);
        }
    }

    /**
	 * insert to users details history when the field changed.
	 * @param user
	 * @return true - if user details changed, else - false.
	 */
	public static boolean insertUsersDetailsHistoryOneField(long writerId, long userId, String userName, String classId, long typeId, String oldData, String newData) {
	    ArrayList<UsersDetailsHistory> usersDetailsHistoryList = new ArrayList<UsersDetailsHistory>();
	    UsersManagerBase.checkUserDetailsChange(typeId, oldData, newData, usersDetailsHistoryList);
	    if (!usersDetailsHistoryList.isEmpty()) {
	        UsersManagerBase.insertUsersDetailsHistory(writerId, userId, userName, usersDetailsHistoryList, classId);
          return true;
	    }
	    return false;
	}

    /**
     * check if user details changed,
     * if so, insert the changes to USERS_DETAILS_HISTORY.
     * @param userId
     * @param oldData
     * @param newData
     * @return
     */
    public static boolean checkUserDetailsChange(long usersDetailsHistoryTypeId, String oldData, String newData, ArrayList<UsersDetailsHistory> usersDetailsHistoryList){
		if (!CommonUtil.isParameterEmptyOrNull(newData)) {
			if (!newData.equals(oldData)) {
				populateUsersDetailsHistoryList(usersDetailsHistoryTypeId, oldData, newData, usersDetailsHistoryList);
				return true;
			}
		} else if (CommonUtil.isParameterEmptyOrNull(newData) && !CommonUtil.isParameterEmptyOrNull(oldData)) {
			populateUsersDetailsHistoryList(usersDetailsHistoryTypeId, oldData, newData, usersDetailsHistoryList);
			return true;
		}
		return false;
	}
    
    public static void populateUsersDetailsHistoryList(long usersDetailsHistoryTypeId, String oldData, String newData, ArrayList<UsersDetailsHistory> usersDetailsHistoryList) {
    	UsersDetailsHistory usersDetailsHistory = new UsersDetailsHistory();
		usersDetailsHistory.setTypeId(usersDetailsHistoryTypeId);
		usersDetailsHistory.setFieldBefore(oldData);
		usersDetailsHistory.setFieldAfter(newData);
		usersDetailsHistoryList.add(usersDetailsHistory);
    }

    /**
     * insert Users details history.
     * @param writerId
     * @param userId
     * @param userName
     * @param concatenationTypeId
     * @param concatenationOldData
     * @param concatenationNewData
     * @param classId
     */
    public static void insertUsersDetailsHistory(long writerId, long userId, String userName, ArrayList<UsersDetailsHistory> usersDetailsHistoryList, String classId) {
    	logger.log(Level.INFO, "START insertUsersDetailsHistory.");
    	Connection con = null;
    	try {
    		con = getConnection();
    		if (!CommonUtil.isParameterEmptyOrNull(userName)) {
    			if (logger.isEnabledFor(Level.DEBUG)) {
    				logger.log(Level.DEBUG, "Going to insert into users_details_history: \n WriterId = " + writerId + ", UserId = " + userId + ", UserName = " + userName);
    			}
    			long seqUsersDetailsHistoryId = UsersDAOBase.getSequenceNextVal(con, "SEQ_USERS_DETAILS_HISTORY");
        		UsersDAOBase.insertUsersDetailsHistory(con, writerId, userId, userName, usersDetailsHistoryList, classId, seqUsersDetailsHistoryId);
        		if (usersDetailsHistoryList.get(0).getTypeId() == UsersManagerBase.USERS_DETAILS_HISTORY_TYPE_CREDIT_CARD_NUMBER) {
        			UsersDAOBase.insertUsersDetailsHistoryChangesCC(con, userId, usersDetailsHistoryList.get(0), seqUsersDetailsHistoryId);
        		} else {
        			UsersDAOBase.insertUsersDetailsHistoryChanges(con, userId, usersDetailsHistoryList, seqUsersDetailsHistoryId);
        		}
    		}
    	} catch (Exception e) {
    		logger.log(Level.ERROR, "Error! problem in insert users details history.", e);
		} finally {
    		closeConnection(con);
    	}
    	logger.log(Level.INFO, "END insertUsersDetailsHistory.");
    }

    /**
     * get user details HM
     * @return HashMap<Long, String>
     * @throws Exception
     */
    public static HashMap<Long, String> getUserDetailsHM(long skinId, String street, long countryId, String gender, String email, String mobilePhone, String landLinePhone, boolean isContactByEmail,
            boolean isContactBySMS, String birthDay, String birthMonth, String birthYear, String cityName, String idNum, Long stateId, String firstName, String lastName, String password, Date birthDate) throws Exception {
        return UsersManagerBase.getUserDetailsHM(skinId, street, countryId, gender, email, mobilePhone, landLinePhone, isContactByEmail,
                isContactBySMS, birthDay, birthMonth, birthYear, cityName, idNum, stateId, firstName, lastName, password, birthDate,
                ConstantsBase.NON_SELECTED, null, null, false, false, (int)ConstantsBase.NON_SELECTED, false, false, false, false, null, false, USER_DEFAULT_BUBBLES_PRICING_LEVEL);
    }

    /**
     * get user details HM
     * @return userDetailsHM as HashMap<Long, String>
     * @throws Exception
     */
    public static HashMap<Long, String> getUserDetailsHM(long skinId, String street, long countryId, String gender, String email, String mobilePhone, String landLinePhone, boolean isContactByEmail,
            boolean isContactBySMS, String birthDay, String birthMonth, String birthYear, String cityName, String idNum, Long stateId, String firstName, String lastName, String password, Date birthDate,
            long languageId, String classId, String isIdDocVerify, boolean isAuthorizedMail, boolean isRisky, int specialCareManualy, boolean isStopReverseWithdrawOption, boolean isImmediateWithdraw,
            boolean isUserDisableDev3, boolean isUserDisableActive, String userName, boolean isUserActive, long bubblesPricingLevel) throws Exception {
        HashMap<Long, String> userDetailsHM = new HashMap<Long, String>();
        userDetailsHM.put(UsersManagerBase.USERS_DETAILS_HISTORY_TYPE_USERNAME, userName);
        userDetailsHM.put(UsersManagerBase.USERS_DETAILS_HISTORY_TYPE_STREET_ADDRESS, street);
        userDetailsHM.put(UsersManagerBase.USERS_DETAILS_HISTORY_TYPE_COUNTRY, String.valueOf(countryId));
        userDetailsHM.put(UsersManagerBase.USERS_DETAILS_HISTORY_TYPE_GENDER, gender);
        userDetailsHM.put(UsersManagerBase.USERS_DETAILS_HISTORY_TYPE_EMAIL, email);
        userDetailsHM.put(UsersManagerBase.USERS_DETAILS_HISTORY_TYPE_MOBILE, mobilePhone);
        userDetailsHM.put(UsersManagerBase.USERS_DETAILS_HISTORY_TYPE_PHONE, landLinePhone);
        userDetailsHM.put(UsersManagerBase.USERS_DETAILS_HISTORY_TYPE_CONTACT_BY_EMAIL, CommonUtil.getBooleanAsString(isContactByEmail));
        userDetailsHM.put(UsersManagerBase.USERS_DETAILS_HISTORY_TYPE_CONTACT_BY_SMS, CommonUtil.getBooleanAsString(isContactBySMS));
        userDetailsHM.put(UsersManagerBase.USERS_DETAILS_HISTORY_TYPE_CITY, cityName);
        userDetailsHM.put(UsersManagerBase.USERS_DETAILS_HISTORY_TYPE_FIRST_NAME, firstName);
        userDetailsHM.put(UsersManagerBase.USERS_DETAILS_HISTORY_TYPE_LAST_NAME, lastName);
        userDetailsHM.put(UsersManagerBase.USERS_DETAILS_HISTORY_TYPE_LANGUAGE, String.valueOf(languageId));
        userDetailsHM.put(UsersManagerBase.USERS_DETAILS_HISTORY_TYPE_USER_TYPE, classId);
        userDetailsHM.put(UsersManagerBase.USERS_DETAILS_HISTORY_TYPE_SKIN, String.valueOf(skinId));
        userDetailsHM.put(UsersManagerBase.USERS_DETAILS_HISTORY_TYPE_SEND_DEPOSIT_DOCUMENTS, isIdDocVerify);
        userDetailsHM.put(UsersManagerBase.USERS_DETAILS_HISTORY_TYPE_EMAIL_AUTHORIZED, CommonUtil.getBooleanAsString(isAuthorizedMail));
        userDetailsHM.put(UsersManagerBase.USERS_DETAILS_HISTORY_TYPE_RISKY_USER, CommonUtil.getBooleanAsString(isRisky));
        userDetailsHM.put(UsersManagerBase.USERS_DETAILS_HISTORY_TYPE_SPECIAL_CARE, String.valueOf(specialCareManualy));
        userDetailsHM.put(UsersManagerBase.USERS_DETAILS_HISTORY_TYPE_STOP_REVERSE_WITHDRAW_OPTION, CommonUtil.getBooleanAsString(isStopReverseWithdrawOption));
        userDetailsHM.put(UsersManagerBase.USERS_DETAILS_HISTORY_TYPE_IMMEDIATE_WITHDRAW, CommonUtil.getBooleanAsString(isImmediateWithdraw));
        userDetailsHM.put(UsersManagerBase.USERS_DETAILS_HISTORY_TYPE_USER_DISABLE_DEV3, CommonUtil.getBooleanAsString(isUserDisableDev3));
        userDetailsHM.put(UsersManagerBase.USERS_DETAILS_HISTORY_TYPE_USER_DISABLE_ACTIVE, CommonUtil.getBooleanAsString(isUserDisableActive));
        userDetailsHM.put(UsersManagerBase.USERS_DETAILS_HISTORY_TYPE_USER_IS_ACTIVE, CommonUtil.getBooleanAsStringYesNo(isUserActive));
        userDetailsHM.put(UsersManagerBase.USERS_DETAILS_HISTORY_TYPE_USER_BUBBLES_PRICING_LEVEL, String.valueOf(bubblesPricingLevel));

        // password
        String encryptedPassword = null;
        if (null != password) {
            if(skinId == Skin.SKIN_ETRADER){
                password = password.toUpperCase();
            }
			try {
				encryptedPassword = AESUtil.encrypt(password);
			} catch (	CryptoException | InvalidKeyException | NoSuchAlgorithmException | NoSuchPaddingException | IllegalBlockSizeException
						| BadPaddingException | UnsupportedEncodingException e) {
				throw new Exception("Unable to encrypt String" + e.getMessage());
			}
        }
        userDetailsHM.put(UsersManagerBase.USERS_DETAILS_HISTORY_TYPE_PASSWORD, encryptedPassword);
        // birth date.
        String birthDateFormat = null;
        if (birthDate != null) {
            birthDateFormat = CommonUtil.formatDate(birthDate, null, "dd/MM/yyyy hh:mm:ss");
        } else {
            Calendar calendar = null;
            if (!CommonUtil.isParameterEmptyOrNull(birthYear) && !CommonUtil.isParameterEmptyOrNull(birthMonth) && !CommonUtil.isParameterEmptyOrNull(birthDay)) {
                calendar = new GregorianCalendar(Integer.parseInt(birthYear),Integer.parseInt(birthMonth) - 1,Integer.parseInt(birthDay),0,1);
            }
            if(calendar != null){
                Date date = new Date(calendar.getTimeInMillis());
                birthDateFormat = CommonUtil.formatDate(date, null, "dd/MM/yyyy hh:mm:ss");

            }
        }
        userDetailsHM.put(UsersManagerBase.USERS_DETAILS_HISTORY_TYPE_BIRTHDATE, birthDateFormat);
        // ET: id_num, AO: state.
        if (skinId == Skin.SKIN_ETRADER) {
            userDetailsHM.put(UsersManagerBase.USERS_DETAILS_HISTORY_TYPE_ID_NUM, idNum);
        } else {
            String state = "0";
            if (stateId != null) {
                state = String.valueOf(stateId);
            }
            userDetailsHM.put(UsersManagerBase.USERS_DETAILS_HISTORY_TYPE_STATE, state);
        }
        return userDetailsHM;
    }

    public static ArrayList<UsersDetailsHistory> checkUserDetailsChange(HashMap<Long, String> userDetailsBeforeChangedHM, HashMap<Long, String> userDetailsAfterChangedHM) {
        ArrayList<UsersDetailsHistory> usersDetailsHistoryList = new ArrayList<UsersDetailsHistory>();
        Iterator<Long> iter = userDetailsAfterChangedHM.keySet().iterator();
        long usersDetailsHistoryTypeId;
        while (iter.hasNext()) {
            usersDetailsHistoryTypeId = iter.next();
            UsersManagerBase.checkUserDetailsChange(usersDetailsHistoryTypeId, userDetailsBeforeChangedHM.get(usersDetailsHistoryTypeId), userDetailsAfterChangedHM.get(usersDetailsHistoryTypeId), usersDetailsHistoryList);
        }
        if (!usersDetailsHistoryList.isEmpty()) {
            return usersDetailsHistoryList;
        }
        return null;
    }

    public static void updateUserLastLoginId(long userId, long lastLoginId) {
    	Connection con = null;
    	try {
    		con = getConnection();
    		if (userId> 0 && lastLoginId>0){
    			UsersDAOBase.updateUserLastLoginId(con, userId, lastLoginId);
    	}
	    } catch (Exception e) {
	    	logger.log(Level.ERROR, "Error! problem with updating user's last login id.", e);
		} finally {
			closeConnection(con);
	    }
    }

    public static long getUserCurrencyId(long userId) throws SQLException{
		Connection con = getConnection();
		try {
			return UsersDAOBase.getUser(con, userId).getCurrencyId();
		} finally {
			closeConnection(con);
		}
    }
    
    
    public static User getUserById(long userId) throws SQLException{
		Connection con = getConnection();
		try {
			return UsersDAOBase.getUser(con, userId);
		} finally {
			closeConnection(con);
		}
    }
    
    public static String getUserUtcOffset(long userId) throws SQLException{
		Connection con = getConnection();
		try {
			return UsersDAOBase.getUser(con, userId).getUtcOffset();
		} finally {
			closeConnection(con);
		}
    }

    public static HashMap<Long, User> getUserFirstLastName(List<Long> userIds) throws SQLException{
		Connection con = getConnection();
		try {
			return UsersDAOBase.getUserFirstLastName(con, userIds);
		} finally {
			closeConnection(con);
		}
    }

//    public static HashMap<Long, UserUniqueDetails> getUserUniqueDetails(StringBuilder usersIdsParse) {
//		Connection connection = null;
//		HashMap<Long, UserUniqueDetails> userUniqueDetails = new HashMap<Long, UserUniqueDetails>();
//		try {
//			if (usersIdsParse.length() > 0) {
//				connection =  getConnection();
//				userUniqueDetails = UsersDAOBase.getUserUniqueDetails(connection, usersIdsParse);
//			}
//		} catch (Exception e) {
//			logger.error("Error! problem with getUserUniqueDetails", e);
//		} finally {
//			closeConnection(connection);
//		}
//		return userUniqueDetails;
//    }

    /**
     * move user into upgrade only if he only if he meets the conditions
     * 1. sum deposit more then 1000
     * 2. not lock
     * 3. not in upgrade already
     * @param userId the user to move
     * @throws SQLException
     * @throws NoSuchMethodException
     * @throws SecurityException
     * @throws InvocationTargetException
     * @throws IllegalAccessException
     * @throws IllegalArgumentException
     */
    public static void moveUserToUpgrade(long userId, long writerId, Class<?> PopulationsManagerBaseClass, int countRealDeposit) throws SQLException, SecurityException, NoSuchMethodException, IllegalArgumentException, IllegalAccessException, InvocationTargetException {
    	logger.debug("moveUserToUpgrade started");
    	Connection con = getConnection();
		try {
			if (UsersDAOBase.isUserNeedToMoveToUpgrade(con, userId)) {
				Method cancelAssignAndMoveToUpgradeMethod = PopulationsManagerBaseClass.getMethod("cancelAssignAndMoveToUpgrade", Long.class, Long.class);
				cancelAssignAndMoveToUpgradeMethod.invoke(null, userId, writerId);
				if (countRealDeposit > TransactionsManagerBase.FIRST_REAL_SUCCESS_DEPOSIT) {
					logger.debug("Load data before sending email.");
					User user = UsersDAOBase.getUserDetails(con, userId);
					Currency currency = CurrenciesManagerBase.getCurrency(Currency.CURRENCY_USD_ID);
					String formatSumDeposits = CommonUtil.formatCurrencyAmount((user.getUserActiveData().getSumDeposits() / 100.00), true, currency);
					String from = CommonUtil.getConfig("email.from");
					String to = CommonUtil.getConfig("email.retention.managers");
					String subject = "Customer " + userId + " reached sum deposits of over $1000";
			        String body = "User ID: " + userId + "\r\n" +
			        			  "Username: " + user.getUserName() + "\r\n" +
			        			  "Skin: " + user.getSkinId() + "\r\n" +
			        			  "Customer's sum deposits: " + formatSumDeposits + "\r\n";
			        logger.debug("Sending email started, from: " + from + ", to: " + to);
					CommonUtil.sendEmailMsg(from, to, subject, body);
					logger.debug("Sending email end.");
				}
			}
		} finally {
			closeConnection(con);
		}
		logger.debug("moveUserToUpgrade end");
    }

	public static void updateAcceptTerms(long userId) throws SQLException {
		Connection con = getConnection();
		try {
			UsersDAOBase.updateAcceptTerms(con, userId);
		} finally {
			closeConnection(con);
		}
	}

	public static String getMobileDeviceOS(String deviceUniqueId) {
		String devOS = "";
		if (deviceUniqueId != null) {
			if(!deviceUniqueId.contains("-")) {
				devOS = ConstantsBase.MOBILE_DES_NEW_ANDROID_APP;
			} else {
				devOS = ConstantsBase.MOBILE_DES_NEW_IOS_APP;
			}
		}
		return devOS;
	}
	public static List<Long> areTestUsers(List<Long> usersId) throws SQLException {
		Connection con = getConnection();
		try {
			return UsersDAOBase.areTestUsers(con, usersId);
		} finally {
			closeConnection(con);
		}
	}
	
    public static HashMap<Long, Integer> getUsersCopyopStatus(List<Long> userIds) throws SQLException{
		Connection con = getConnection();
		try {
			return UsersDAOBase.getUsersCopyopStatus(con, userIds);
		} finally {
			closeConnection(con);
		}
    }
    
	public static boolean isUserEngagedBonus(long userId) throws SQLException {
		Connection conn = getConnection();
		try {
			return UsersDAOBase.isUserEngagedBonus(conn, userId);
		} finally {
		    closeConnection(conn);
		}
	}
	
	public static void insertAffSub(long userId, String affSub1, String affSub2, String affSub3) throws SQLException {
		Connection conn = getConnection();
		try {
			UsersDAOBase.insertAffSub(conn, userId, affSub1, affSub2, affSub3);
		} finally {
		    closeConnection(conn);
		}
	}
	
	public static long getUserDefInvAmount(long userId) throws SQLException{
		Connection con = getConnection();
		try {
			return UsersDAOBase.getUserDefInvAmount(con, userId);
		} finally {
			closeConnection(con);
		}
    }

	public static User getUserDetails(long userId) throws SQLException{
		Connection con = getConnection();
		try {
			return UsersDAOBase.getUserDetails(con, userId);
		} finally {
			closeConnection(con);
		}
    }
	
	/**
	 * Get marketing users
	 * @param affiliateKey
	 * @param dateRequest
	 * @return MarketingInfo
	 * @throws SQLException
	 * @throws ParseException
	 */
	public static MarketingInfo getMarketingUsers(long affiliateKey, String dateRequest) throws SQLException, ParseException{
		Connection con = getConnection();
		try {
			MarketingInfo mi = new MarketingInfo();
			UsersDAOBase.getMarketingUsers(con, mi, affiliateKey, dateRequest);
			TransactionsDAOBase.getMarketingFtds(con, mi, affiliateKey, dateRequest);
			return mi;
		} finally {
			closeConnection(con);
		}
    }


	/**
	 * This method returns the user files from types utility bill, id, passport, driver's license,
	 * credit card, bankwire confirmation.
	 * @param userId
	 * @return a list of user files, null if there is a problem
	 */
	public static List<File> getUserFiles(long userId, Locale locale) {
		Connection con = null;
		try {
			con = getConnection();
			return FilesDAOBase.getUserFiles(con, userId, locale);
		} catch (	SQLException | InvalidKeyException | IllegalBlockSizeException | BadPaddingException | NoSuchAlgorithmException
					| NoSuchPaddingException | InvalidAlgorithmParameterException e) {
			logger.error("Can't get user files", e);
		} finally {
			closeConnection(con);
		}
		return null;
	}

	public static com.anyoption.common.beans.File getUserFileByType(long userId, long type, Locale locale) {
		return getUserFileByTypeAndCc(userId, type, 0, locale);
	}

	public static com.anyoption.common.beans.File getUserFileByTypeAndCc(long userId, long type, long ccId, Locale locale) {
		Connection con = null;
		try {
			con = getConnection();
			return FilesDAOBase.getUserFileByType(con, userId, type, ccId, locale);
		} catch (	SQLException | InvalidKeyException | IllegalBlockSizeException | BadPaddingException | NoSuchAlgorithmException
					| NoSuchPaddingException | InvalidAlgorithmParameterException e) {
			logger.error("Can't get user file", e);
		} finally {
			closeConnection(con);
		}
		return null;
	}
	
	public static void updateUserNeedChangePass(long userId, boolean isNeedChangePass) throws SQLException {
		Connection conn = getConnection();
		try {
			UsersDAOBase.updateUserNeedChangePass(conn, userId, isNeedChangePass);
		} finally {
		    closeConnection(conn);
		}
	}

	public static void saveToken(String token, String serverId, long userId, long loginId) throws SQLException {
		Connection conn = getConnection();
		try {
			UsersDAOBase.saveToken(conn, token, serverId, userId, loginId);
		} finally {
		    closeConnection(conn);
		}
	}
	
	public static BubblesToken tokenExists(String token, long userId) throws SQLException {
		Connection conn = getConnection();
		try {
			return UsersDAOBase.tokenExists(conn, token, userId);
		} finally {
		    closeConnection(conn);
		}
	}
	
	public static void deleteToken(String token) throws SQLException {
		Connection conn = getConnection();
		try {
			UsersDAOBase.deleteToken(conn, token);
		} finally {
		    closeConnection(conn);
		}
	}
	
	public static void cleanAllTokens() throws SQLException {
		Connection conn = getConnection();
		String serverId = ServerConfiguration.getInstance().getServerName();
		try {
			UsersDAOBase.cleanAllTokens(conn, serverId);
		} finally {
		    closeConnection(conn);
		}
	}
	
	public static long getCurrencyIdByUserName(String userName) throws SQLException {
		Connection con = null;
		long currencyId;
		try {
			con = getConnection();
			currencyId = UsersDAOBase.getCurrencyIdByUserName(con, userName);
		} finally {
			closeConnection(con);
		}
		return currencyId;
	}

	public static boolean hasTheSameInvestmentInTheLastSecSameIp(String ip, int sec, InvestmentRejects invRej) throws SQLException {
		logger.debug("Checking submitting investment twice in less than " + sec + " sec from ip " + ip );
        boolean has = false;
        Connection conn = getConnection();
        try {
            has = UsersDAOBase.hasTheSameInvestmentInTheLastSecSameIp(conn, ip, sec, invRej);
        } finally {
            closeConnection(conn);
        }
        return has;
    }
	
    public static boolean hasTheSameInvestmentInTheLastSec(long userId, long oppId, long typeId, InvestmentRejects invRej) throws SQLException {
        boolean has = false;
        Connection conn = getConnection();
        try {
            has = UsersDAOBase.hasTheSameInvestmentInTheLastSec(conn, userId, oppId, typeId,invRej);
        } finally {
            closeConnection(conn);
        }
        return has;
    }

    /**
     * Check if there is disabled record for the specified user / market covering the current moment.
     *
     * @param userId
     * @param marketId
     * @return
     * @throws SQLException
     */
    public static boolean isUserMarketDisabled(long userId, long marketId, int scheduled, boolean isDev3, long apiExternalUserId) throws SQLException {
        boolean b = false;
        Connection conn = getConnection();
        try {
            b = UsersDAOBase.isUserMarketDisabled(conn, userId, marketId, scheduled, isDev3, apiExternalUserId);
        } finally {
            closeConnection(conn);
        }
        return b;
    }
	public static boolean userHasBonus(long userId) throws SQLException {
		logger.debug("Checking if user:" + userId + " has bonus");
        boolean has = false;
        Connection conn = getConnection();
        try {
            has = UsersDAOBase.isHaveBonus(conn, userId);
        } finally {
            closeConnection(conn);
        }
        return has;
    }
	public static boolean isBonusSeen(long userId) {
	    logger.debug("Checking if user:" + userId + " has bonuses not seen");
	    boolean isBonusSeen = false;
	    try {
	    	isBonusSeen = BonusManagerBase.isBonusNotSeen(userId);
	    } catch (SQLException e) {
	    	logger.debug("Can not check if user has bonuses not seen with exception: ", e);
	    }
	    return isBonusSeen;
	}
	
	public static void loadUserContacts(String userName, User user) throws SQLException {
		logger.debug("Loading users email and phone :" + userName);
        Connection conn = getConnection();
        try {
            UsersDAOBase.loadUserContacts(conn, userName, user);
        } finally {
            closeConnection(conn);
        }
	}

    /**
     * @param skinId
     * @param platformId
     * @return
     * @throws SQLException
     */
    public static int getEPGProductId(int skinId, int platformId) throws SQLException {
        Connection connection = null;
        try {
        	connection = getConnection();
            return UsersDAOBase.getEPGProductId(connection, skinId, platformId);  
        } finally {
            closeConnection(connection);
        }
    }


	public static boolean changeCancelInvestmentCheckboxState(long userId, boolean showCancelInvestment) {
		Connection con = null;
		try {
			con = getConnection();
			return UsersDAOBase.changeCancelInvestmentCheckboxState(con, userId, showCancelInvestment);
		} catch (SQLException e) {
			logger.error("Cannot change cancel investment checkbox state", e);
			return false;
		} finally {
			closeConnection(con);
		}
	}
	
	public static boolean updateUserPhoneValidated(Long id, MobileNumberValidation validation) {
		Connection con = null;
		try {
			con = getConnection();
			return UsersDAOBase.updateUserPhoneValidated(con, id, validation) ;
		} catch (SQLException e) {
			logger.error("Cannot Update User Phone Validation Status", e);
			return false;
		} finally {
			closeConnection(con);
		}
	}

	/**
	 * Adds the specified amount to the user's balance. If the amount is negative it's subtracted. If the balance would turn negative,
	 * another transaction is created to fix the negative balance, bringing it to 0. Returns 0 if no negative balance fix transaction is
	 * made, otherwise returns the id of the transaction
	 * 
	 * @param con
	 * @param userId id of the user
	 * @param amount the amount to be added to the balance(if negative the amount will be subtracted)
	 * @param rate the rate of the amount in USD
	 * @param utcOffset
	 * @return 0 if the balance is not negative, or the id of the transaction that fixed the user's balance, bringing it to 0
	 * @throws SQLException if a database access error occurs
	 */
	public static long addToBalanceNonNeg(Connection con, long userId, long amount, double rate, String utcOffset) throws SQLException {
		return UsersDAOBase.addToBalanceNonNeg(	con, userId, amount, TransactionsManagerBase.TRANS_TYPE_FIX_NEGATIVE_BALANCE, rate,
												"log.balance.fix.negative.balance.deposit", utcOffset,
												TransactionsManagerBase.NEGATIVE_BALANCE_FIX_COMMENT);
	}
	
	public static List<com.anyoption.common.beans.File> getUserFilesByType(long userId, long fileType, Locale locale) {
		Connection con = null;
		try {
			con = getConnection();
			return FilesDAOBase.getUserFilesByType(con, userId, fileType, locale);
		} catch (	SQLException | InvalidKeyException | IllegalBlockSizeException | BadPaddingException | NoSuchAlgorithmException
					| NoSuchPaddingException | InvalidAlgorithmParameterException e) {
			logger.error("Can't get user files", e);
		} finally {
			closeConnection(con);
		}
		return null;
	}

	public static boolean isTestUser(long userId) throws SQLException {
		Connection con = null;
		try {
			con = getConnection();
			return UsersDAOBase.isTestUser(con, userId);
		} finally {
			closeConnection(con);
		}		
	}
	
	public static long getUserIdByEmail(String email) throws SQLException {
		Connection con = null;
		try {
			con = getConnection();
			return UsersDAOBase.getUserIdByEmail(con, email);
		} finally {
			closeConnection(con);
		}
	}
	
	public static void setUserLastDecline(long userId, boolean isDeclined, Transaction tran) throws SQLException {
		Connection con = null;
		try {
			con = getConnection();
			UsersDAOBase.setUserLastDecline(con, userId, isDeclined, tran);
		} finally {
			closeConnection(con);
		}
	}
	
	public static void updateLastDepositOrInvestDate(long userId) throws SQLException {
		Connection con = null;		
		try {
			con = getConnection();
			UsersDAOBase.updateLastDepositOrInvestDate(con, userId);
		} catch (Exception e) {
			logger.error("Can't update last deposit or investment Date for userId[" + userId + "] ", e );
		} finally {
			closeConnection(con);
		}
	}
	
	public static void updateLastDepositOrInvestDate(Connection con, long userId) throws SQLException {
			UsersDAOBase.updateLastDepositOrInvestDate(con, userId);
	}
	
	public static HashMap<String, User> checkForExistingEmails(ArrayList<String> emailsForCheck) throws SQLException {
		Connection con = null;
		try {
			con = getConnection();
			return UsersDAOBase.checkForExistingEmails(con, emailsForCheck);
		} finally {
			closeConnection(con);
		}
	}
	
	public static boolean isNonRegUserSuspend(long userId) throws SQLException {
        boolean res = false;
        Connection conn = getConnection();
        try {
        	res = UsersDAOBase.isNonRegUserSuspend(conn, userId);
        } finally {
            closeConnection(conn);
        }
        return res;
    }
	
	public static UserMigration getUserMigration(long userId) throws SQLException {
        Connection conn = getConnection();
        try {
        	return UsersDAOBase.getUserMigration(conn, userId);
        } finally {
            closeConnection(conn);
        }
    }

	public static void getUserMigration(long userId, UserMigration um, boolean processed) throws SQLException {
        Connection conn = getConnection();
        try {
        	UsersDAOBase.getUserMigration(conn, userId, um, processed);
        } finally {
            closeConnection(conn);
        }
    }
	
	public static void updateUserMigration(long userId) throws SQLException {
        Connection conn = getConnection();
        try {
        	UsersDAOBase.updateUserMigration(conn, userId);
        } finally {
            closeConnection(conn);
        }
    }
	
	public static void insertUserMigrationJob(UserMigration um) throws SQLException {
        Connection conn = getConnection();
        try {
        	UsersDAOBase.insertUserMigrationJob(conn, um);
        } finally {
            closeConnection(conn);
        }
    }
	
	public static MailBoxTemplate getTemplateByTypeSkinLanguage(long typeId, long skinId, long languageId) throws SQLException {
		Connection con = getConnection();
		try {
			return MailBoxTemplatesDAOBase.getTemplateByTypeSkinLanguage(con, typeId, skinId, languageId);
		} finally {
			closeConnection(con);
		}
	}
	
}