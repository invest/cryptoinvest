package com.anyoption.common.managers;

import java.net.URLDecoder;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;

import org.apache.commons.lang.StringEscapeUtils;
import org.apache.log4j.Logger;

import com.anyoption.common.beans.base.MarketingTracking;
import com.anyoption.common.beans.base.MarketingTrackingCookieStatic;
import com.anyoption.common.daos.MarketingTrackingDAOBase;


public class MarketingTrackingManager extends BaseBLManager{
	
    private static final Logger log = Logger.getLogger(MarketingTrackingManager.class);
    
    public static boolean insertMarketingTracking(ArrayList<MarketingTracking> tracking) throws SQLException{
        
		Connection con = getConnection();
		boolean result = false;
		
		try{
			
		     for (MarketingTracking mt : tracking) {

		         try{
		             //Html and Url Decode to http_referer
		             if (mt.getHttpReferer() != null ){
		                 String afterHtmlDecoding = StringEscapeUtils.unescapeHtml(mt.getHttpReferer());
		                 String afterUrlDecode =  URLDecoder.decode(afterHtmlDecoding, "UTF-8");
		                 mt.setHttpReferer(afterUrlDecode);
		             }
                     
		             if (mt.getHttpRefererDynamic() != null ){
		                  String afterHtmlDecodingDynamic = StringEscapeUtils.unescapeHtml(mt.getHttpRefererDynamic());
		                  String afterUrlDecodeDynamic =  URLDecoder.decode(afterHtmlDecodingDynamic, "UTF-8");
		                  mt.setHttpRefererDynamic(afterUrlDecodeDynamic);
		             }
		             		             
		         } catch (Exception e){
		             log.info("Marketing Tracker can't html decode to http_refer:" + mt.getHttpRefererDynamic() + "and http_refer_dyn:"
		                     + mt.getHttpRefererDynamic() + " Exc: ", e);
		         }
		     }
		    
			result = MarketingTrackingDAOBase.insertMarketingTracking(con, tracking);
			
		}finally{
			
			closeConnection(con);
		}
		return result;
    }

    public static String getMidByUserId(long userId) throws SQLException{
    	
    	Connection con = getConnection();
    	String mId = "";
    	try{
    		mId = MarketingTrackingDAOBase.getMidByUserId(con, userId);
    	}finally{
    		closeConnection(con);
    	}
    	return mId;
    }
    
    public static long getUserIdByMid(String mid) throws SQLException{
    	
    	Connection con = getConnection();
    	long userId = 0;
    	try{
    		userId = MarketingTrackingDAOBase.getUserIdByMid(con, mid);
    	}finally{
    		closeConnection(con);
    	}
    	return userId;
    }
    
    public static long getContactIdByMid(String mid) throws SQLException{
    	
    	Connection con = getConnection();
    	long contactId = 0;
    	try{
    		contactId = MarketingTrackingDAOBase.getContactIdByMid(con, mid);
    	}finally{
    		closeConnection(con);
    	}
    	return contactId;
    }
    
    public static String getMidByContactDetailsInsertingUser(String email, String mobilePhone, String landLinePhone, long contactId, long userId) throws SQLException{
        
        Connection con = getConnection();
        String mId = "";
        try{
            mId = MarketingTrackingDAOBase.getMidByContactDetailsInsertingUser(con, email, mobilePhone, landLinePhone, contactId, userId);
        }finally{
            closeConnection(con);
        }
        return mId;
    }
    
    public static String getMidByContactDetail(String email, String phone) throws SQLException{
        
        Connection con = getConnection();
        String mId = "";
        try{
            mId = MarketingTrackingDAOBase.getMidByContactDetails(con, email, phone);
        }finally{
            closeConnection(con);
        }
        return mId;
    }
    
    public static boolean isHaveActivity(String mid) throws SQLException{
    	
    	Connection con = getConnection();
    	boolean result = false;
    	try{
    		result = MarketingTrackingDAOBase.isHaveActivity(con, mid);
    	}finally{
    		closeConnection(con);
    	}
    	return result;
    }
    
	public static MarketingTrackingCookieStatic getMarketingTrackingCookieStatic(String mId) throws SQLException {
		Connection con = getConnection();
		try {
			return MarketingTrackingDAOBase.getMarketingTrackingCookieStatic(con, mId);
		} finally {
			closeConnection(con);
		}
	}
	
	public static void updateContactMid (long contactId, String mId) throws SQLException{
	    Connection con = getConnection();
        try {
            MarketingTrackingDAOBase.updateContactMid(con, contactId, mId);
        } finally {
            closeConnection(con);
        }
	}
	
    public static void updateUserMid (long userId, String mId) throws SQLException{
        Connection con = getConnection();
        try {
            MarketingTrackingDAOBase.updateUserMid(con, userId, mId);
        } finally {
            closeConnection(con);
        }
    }	
	
    public static String getEtsMidByContactDetail(String email, String phone) throws SQLException{
        
        Connection con = getConnection();
        String mId = "";
        try{
            mId = MarketingTrackingDAOBase.getEtsMidByContactDetails(con, email, phone);
        }finally{
            closeConnection(con);
        }
        return mId;
    }
    
    public static String getEtsMidByContactDetail(String email, String mobilePhone, String landLinePhone, long contactId, long userId) throws SQLException{
        
        Connection con = getConnection();
        String mId = "";
        try{
            mId = MarketingTrackingDAOBase.getEtsMidByContactDetails(con, email, mobilePhone, landLinePhone, contactId, userId);
        }finally{
            closeConnection(con);
        }
        return mId;
    }

    public static ArrayList<String> getEtsUsersFirstDeposit() throws SQLException{
        
        Connection con = getConnection();
        try{
            return MarketingTrackingDAOBase.getEtsUsersFirstDeposit(con);
        }finally{
            closeConnection(con);
        }
    }

    public static void updateEtsUserFDState (ArrayList<String> users) throws SQLException{
        Connection con = getConnection();
        try {
            MarketingTrackingDAOBase.updateEtsUserFDState(con, users);
        } finally {
            closeConnection(con);
        }
    }
    
}
