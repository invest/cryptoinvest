package com.anyoption.common.managers;

import java.sql.Connection;
import java.sql.SQLException;

import com.anyoption.common.daos.UrlsDAO;

public class UrlsManagerBase extends BaseBLManager {
	
	public static int getIdByUrl(String url) throws SQLException {
		Connection conn = getConnection();
		try {
			return UrlsDAO.getIdByUrl(conn, url);
		} finally {
			closeConnection(conn);
		}
	}
}