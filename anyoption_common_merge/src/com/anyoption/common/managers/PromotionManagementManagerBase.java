package com.anyoption.common.managers;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import org.apache.log4j.Logger;

import com.anyoption.common.bl_vos.PromotionBannerSlider;
import com.anyoption.common.daos.PromotionManagementDAOBase;

public class PromotionManagementManagerBase extends BaseBLManager { 
	private static final Logger logger = Logger.getLogger(PromotionManagementManagerBase.class);
	
	private static HashMap<String, HashMap<Date, ArrayList<PromotionBannerSlider>>> cachedSliders = new HashMap<String, HashMap<Date, ArrayList<PromotionBannerSlider>>>();
	private static final int refreshIntervalMinutes = 60;

	public static HashMap<Date, ArrayList<PromotionBannerSlider>> getActiveSliders(String bannerPlace) {
		int minutesBetween = 0;
		
		if(!cachedSliders.containsKey(bannerPlace)){
			cachedSliders.put(bannerPlace, null);
		}
		HashMap<Date, ArrayList<PromotionBannerSlider>> slidersByPlace = cachedSliders.get(bannerPlace);
		if(slidersByPlace != null && !slidersByPlace.isEmpty()){
			Date now = new Date();
			Date lastRefresh = slidersByPlace.keySet().iterator().next();
			minutesBetween = (int) (now.getTime() - lastRefresh.getTime()) / (1000 * 60);
		}
		if (minutesBetween > refreshIntervalMinutes || (slidersByPlace == null || slidersByPlace.isEmpty())) {
			if(slidersByPlace == null){
				cachedSliders.put(bannerPlace, new HashMap<Date, ArrayList<PromotionBannerSlider>>());
				slidersByPlace = cachedSliders.get(bannerPlace);
			}
			slidersByPlace.clear();
			Connection con = null;
			try {
				con = getConnection();
				
				ArrayList<PromotionBannerSlider> activeSliders = PromotionManagementDAOBase.getActiveSliders(con, bannerPlace);	
				slidersByPlace.put(new Date(), activeSliders);
				return slidersByPlace;
			} catch (SQLException ex) {
				logger.error("Can't load banner sliders: ", ex);
			} finally {
				closeConnection(con);
			}
		}
		return slidersByPlace;
	}
}
