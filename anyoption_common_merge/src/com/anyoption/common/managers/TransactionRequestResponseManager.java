package com.anyoption.common.managers;

import java.sql.Connection;
import java.sql.SQLException;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.TransactionRequestResponse;
import com.anyoption.common.daos.TransactionRequestResponseDAO;

/**
 * TransactionRequestResponseManager
 * @author eyalo
 */
public class TransactionRequestResponseManager extends BaseBLManager {
	private static final Logger log = Logger.getLogger(TransactionRequestResponseManager.class);
	
	/**
	 * insert into TransactionRequestResponse
	 * @param vo
	 * @throws SQLException
	 */
	public static void insert(TransactionRequestResponse vo) throws SQLException {
		log.info("Insert into TransactionRequestResponse - START.");
		log.info(vo.toString());
		Connection con = null;
		try {
			con = getConnection();
			TransactionRequestResponseDAO.insert(con, vo);
		} finally {
			closeConnection(con);
		}
		log.info("Insert into TransactionRequestResponse - END.");
	}
	
	/**
	 * Update TransactionRequestResponse
	 * @param vo
	 * @throws SQLException
	 */
	public static void update(TransactionRequestResponse vo) throws SQLException {
		log.info("Update TransactionRequestResponse - START.");
		log.info(vo.toString());
		Connection con = null;
		try {
			con = getConnection();
			TransactionRequestResponseDAO.update(con, vo);
		} finally {
			closeConnection(con);
		}
		log.info("Update TransactionRequestResponse - END.");
	}
	
	/**
	 * get TransactionRequestResponse by the transaction id
	 * @param transactionId
	 * @throws SQLException
	 */
	public static TransactionRequestResponse getTranId(long transactionId) throws SQLException {
		log.debug("get Tran Id - START. " + transactionId);
		Connection con = null;
		TransactionRequestResponse transactionRequestResponse = null;
		try {
			con = getConnection();
			transactionRequestResponse = TransactionRequestResponseDAO.getByTranId(con, transactionId);
		} finally {
			closeConnection(con);
		}
		log.debug("get Tran Id - END. " + transactionId);
		return transactionRequestResponse;
	}
}
