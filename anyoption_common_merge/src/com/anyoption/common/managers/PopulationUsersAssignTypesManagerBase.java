package com.anyoption.common.managers;

import org.apache.log4j.Logger;

public class PopulationUsersAssignTypesManagerBase extends BaseBLManager {
	private static final Logger log = Logger.getLogger(PopulationUsersAssignTypesManagerBase.class);
	
	public static final int TYPE_ASSIGN = 1;
	public static final int TYPE_CANCEL_ASSIGN = 2;
	public static final int TYPE_REASSIGN = 3;
	public static final int TYPE_BULK_REASSIGN = 4;
	
	public static final int OPTION_UNASSIGN = -1;
	
	// Enumerators
	public static final String ENUM_LAST_BULK_REASSIGN_ENUMERATOR = "bulk_reassign";
	public static final String ENUM_LAST_BULK_REASSIGN_CODE = "last_hours_bulk_reassign";
}
