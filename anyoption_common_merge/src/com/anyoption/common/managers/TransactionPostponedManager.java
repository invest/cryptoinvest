package com.anyoption.common.managers;

import java.sql.Connection;
import java.sql.SQLException;

import org.jboss.logging.Logger;

import com.anyoption.common.beans.TransactionPostponed;
import com.anyoption.common.daos.TransactionPostponedDAO;

/**
 * @author liors
 *
 */
public class TransactionPostponedManager extends BaseBLManager {
	private static final Logger logger = Logger.getLogger(TransactionPostponedManager.class);
	
	public static void insert(TransactionPostponed transactionPostponed) throws SQLException {
		Connection connection = null;
		try {
			connection = getConnection();
			TransactionPostponedDAO.insert(connection, transactionPostponed);
		} finally {
			closeConnection(connection);
		}
	}
	
	public static void updateNumOfDays(TransactionPostponed transactionPostponed) throws SQLException {
		Connection connection = null;
		try {
			connection = getConnection();
			TransactionPostponedDAO.updateNumOfDays(connection, transactionPostponed);
		} finally {
			closeConnection(connection);
		}
	}
	
	public static void insertOrUpdateNumOfDays(TransactionPostponed transactionPostponed) throws SQLException {
		Connection connection = null;
		try {
			connection = getConnection();
			if (transactionPostponed.getId() > 0) {
				logger.debug("insertOrUpdateNumOfDays; about to updateNumOfDays"); 
				TransactionPostponedDAO.updateNumOfDays(connection, transactionPostponed);
			} else {
				logger.debug("insertOrUpdateNumOfDays; about to insert"); 
				TransactionPostponedDAO.insert(connection, transactionPostponed);
			}
		} finally {
			closeConnection(connection);
		}
	}
}
