package com.anyoption.common.managers;

import java.sql.SQLException;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.sql.DataSource;

import org.apache.log4j.Logger;

/**
 * DB utils.
 * 
 * @author Tony
 */
public class DBUtil {
    private static Logger log = Logger.getLogger(DBUtil.class);
    
    private static DataSource dataSource;
    private static DataSource dataSecondSource;
    
    /**
     * @return The <code>DataSource</code> to obtain db connection from. Create
     *      it if not created.
     * @throws SQLException
     */
    public static DataSource getDataSource() throws SQLException {
        if (null != dataSource) {
            return dataSource;
        }

        try {
            Context initCtx = new InitialContext();
            try {
                dataSource = (DataSource) initCtx.lookup("java:/DefaultDS");
            } catch (Exception e) {
                log.debug("JBoss DataSource not found.");
            }
            if (null == dataSource) {
                Context envCtx = (Context) initCtx.lookup("java:comp/env");
                dataSource = (DataSource) envCtx.lookup("jdbc/anyoptiondb");
            }
        } catch (Exception e) {
            log.fatal("Cannot lookup datasource", e);
        }

        return dataSource;
    }
    
    public static DataSource getSecondDataSource() throws SQLException {
        if (null != dataSecondSource) {
            return dataSecondSource;
        }

        try {
            Context initCtx = new InitialContext();
            try {
            	dataSecondSource = (DataSource) initCtx.lookup("java:DefaultDS");
            } catch (Exception e) {
                log.debug("JBoss DataSource not found.");
            }
            if (null == dataSecondSource) {
                Context envCtx = (Context) initCtx.lookup("java:comp/env");
                dataSecondSource = (DataSource) envCtx.lookup("jdbc/seconddb");
            }
        } catch (Exception e) {
            log.fatal("Cannot lookup datasource", e);
        }

        return dataSecondSource;
    }
}