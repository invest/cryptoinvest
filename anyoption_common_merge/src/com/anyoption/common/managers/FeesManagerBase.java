package com.anyoption.common.managers;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Map;

import org.apache.log4j.Logger;

import com.anyoption.common.daos.FeesDAOBase;

public class FeesManagerBase extends BaseBLManager {

	private static final Logger logger = Logger.getLogger(FeesManagerBase.class);
	public static Map<Long, Map<Long, Long>> fees;
	public static final long MAINTENANCE_FEE = 1;
	public static final long OPTION_PLUS_COMMISSION_FEE = 2;
	public static final long ZERO_ONE_HUNDRED_COMMISSION_FEE = 3;

	
	private static Map<Long, Map<Long, Long>> getAllFeesMap() {
		Connection con = null;
		try {
			con = getConnection();	
			return FeesDAOBase.getAllFeesMap(con);	
		} catch (SQLException e) {
			logger.debug("Failed to load All Fees Map", e);
			return null;
		} finally {
			closeConnection(con);
		}
	}
	
    public static Map<Long, Map<Long, Long>> getFees() {
    	if (fees == null || fees.isEmpty()) {
    		fees = getAllFeesMap();
    	}
		return fees;
	}
    
	public static long getFee(long feeId, long currencyId) throws SQLException{
		long amount = 0;
		amount = FeesManagerBase.getFees().get(feeId).get(currencyId);
		return amount;
	}
}