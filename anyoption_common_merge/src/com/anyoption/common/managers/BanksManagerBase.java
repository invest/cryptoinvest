package com.anyoption.common.managers;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Set;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.base.BankBranches;
import com.anyoption.common.bl_vos.BankBase;
import com.anyoption.common.daos.BanksDAOBase;

public class BanksManagerBase extends BaseBLManager {
	
	private static final Logger log = Logger.getLogger(BanksManagerBase.class);
	
	public static HashMap<Long, BankBase> banks;
	public static HashMap<Long, String> bankNames;
	
    public static Hashtable<Long, BankBase> getBanks() throws SQLException {
        Connection conn = null;
        try {
            conn = getConnection();
            return BanksDAOBase.getBanks(conn);
        } finally {
            closeConnection(conn);
        }
    }
    
    public static HashMap<Long, BankBase> getBanksET(int bankType, long businessCase) throws SQLException {
        Connection conn = null;
        try {
            conn = getConnection();
            return BanksDAOBase.getBanksET(conn, bankType, businessCase);
        } finally {
            closeConnection(conn);
        }
    }
    
    public static ArrayList<BankBranches> getBanksBranches() throws SQLException {
        Connection conn = null;
        try {
            conn = getConnection();
            return BanksDAOBase.getBanksBranches(conn);
        } finally {
            closeConnection(conn);
        }
    }
    
    public static HashMap<Long,BankBase> getBanks(int bankType, long businessCase) throws SQLException {
    	if (banks == null) {
    		Connection conn = null;
    		try {
    			conn = getConnection();
    			banks = new HashMap<>();
    			banks = BanksDAOBase.getBanks(conn, bankType, businessCase);
    		} finally {
                closeConnection(conn);
            }
    	}
    	return banks;
    }
    
    public static HashMap<Long,String> getBankNames(int bankType, long businessCase) {
    	if (bankNames == null) {
    		try {
    			HashMap<Long, BankBase> bankMap = getBanks(bankType, businessCase);
    			bankNames = new HashMap<Long, String>();
    			Set<Long> bankKeys = bankMap.keySet();
    			for (Long key : bankKeys) {
    				bankNames.put(key, bankMap.get(key).getName());
    			}
    		} catch (SQLException sqle) {
    			log.error("Failed to load bank names : " + sqle);
    		}
    	}
    	return bankNames;
    }
}