/**
 * 
 */
package com.anyoption.common.managers;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;

import com.anyoption.common.beans.base.AssetIndexBase;
import com.anyoption.common.daos.AssetIndexMarketDAO;
public class AssetIndexMarketManagerBase extends BaseBLManager {
		
	public static HashMap<Long, HashMap<Long, AssetIndexBase>> getAssetyByMarketHM() throws SQLException {       	
    	HashMap<Long, HashMap<Long, AssetIndexBase>> assetyByMarketHM = new HashMap<Long, HashMap<Long,AssetIndexBase>>();		
    	Connection con = null;
    	
        try {
            con = getConnection();
           	for(Long skinId: SkinsManagerBase.getSkinsId()){
        		assetyByMarketHM.put(skinId, AssetIndexMarketDAO.getAssetyByMarketHM(con, skinId));
        	}
        } finally {
            closeConnection(con);
        }        
        return assetyByMarketHM;
    }
}