package com.anyoption.common.managers;

import java.sql.Connection;
import java.sql.SQLException;

import org.apache.log4j.Logger;

import com.anyoption.common.daos.UsersPendingDocsDAOBase;


public class UsersPendingDocsManagerBase extends BaseBLManager {

	private static final Logger logger = Logger.getLogger(UsersPendingDocsManagerBase.class);
	public static final long EMPTY_PARAM = -1; 

    public static void insertUsersPendingDocs(long userId, long issueId, boolean isMainRegulationIssue) throws SQLException {
    	Connection con = null;
    	try {
    		con = getConnection();
    		if(UsersPendingDocsDAOBase.getPendicDocsId(con, issueId) == 0){
    			UsersPendingDocsDAOBase.insertUsersPendingDocs(con, userId, issueId, isMainRegulationIssue);
    			logger.debug("Inserted UsersPendicDocs for userId: " + userId + " with issueId: " + issueId);
    		}
    		UsersPendingDocsDAOBase.calculatePendingDocs(con, EMPTY_PARAM, issueId);
    	} finally {
        	closeConnection(con);
        }
    }
    
    public static void calculatePendingDocs(long fileId, long issueId) throws SQLException {
    	Connection con = null;
    	try {
    		con = getConnection();
    		UsersPendingDocsDAOBase.calculatePendingDocs(con, fileId, issueId);    		    		
    	} finally {
        	closeConnection(con);
        }
    }
    
    public static void updateLastWriterActionTime(long issueId) throws SQLException {
    	Connection con = null;
    	try {
    		con = getConnection();
    		UsersPendingDocsDAOBase.updateLastWriterActionTime(con, issueId);    		    		
    	} finally {
        	closeConnection(con);
        }
    }
    
    public static void updateLastUploadFileTime(long fileId) throws SQLException {
    	Connection con = null;
    	try {
    		con = getConnection();
    		UsersPendingDocsDAOBase.updateLastUploadFileTime(con, fileId);    		    		
    	} finally {
        	closeConnection(con);
        }
    }
}