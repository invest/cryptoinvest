/**
 * 
 */
package com.anyoption.common.managers;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.BalanceStep;
import com.anyoption.common.daos.BalanceStepsDAOBase;

/**
 * Manager for balance steps
 * 
 * @author kirilim
 */
public class BalanceStepsManagerBase extends BaseBLManager {

	private static final Logger logger = Logger.getLogger(BalanceStepsManagerBase.class);

	public static List<BalanceStep> getBalanceSteps(long currencyId) {
		Connection con = null;
		try {
			con = getConnection();
			return BalanceStepsDAOBase.getBalanceSteps(con, currencyId);
		} catch(SQLException e) {
			logger.error("Can't get balance steps", e);
			return null;
		} finally {
			closeConnection(con);
		}
	}
}