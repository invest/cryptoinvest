package com.anyoption.common.managers;

import java.io.FileNotFoundException;
import java.io.UnsupportedEncodingException;
import java.sql.Connection;
import java.sql.SQLException;

import com.anyoption.common.daos.TxtLocalDAO;


public class TxtLocalManager extends BaseBLManager {

	   public static String exportUsers(String filePath, String groupId) throws SQLException, UnsupportedEncodingException, FileNotFoundException {
		   Connection connection = getConnection();
		   return TxtLocalDAO.exportUsers(connection, filePath, groupId);
	   }

}
