package com.anyoption.common.managers;

import java.sql.Connection;
import java.sql.SQLException;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.RewardUserHistory;
import com.anyoption.common.daos.RewardUsersHistoryDAO;

/**
 * @author liors
 *
 */
public class RewardUsersHistoryManager extends BaseBLManager {
	private static final Logger logger = Logger.getLogger(RewardUsersHistoryManager.class);
	
	public static final int REWARD_GRANTED = 1;	
	public static final int REWARD_REMOVED = 2;
	public static final int PROMOTE_TIER = 3;
	public static final int DEMOTE_TIER = 4;
	public static final int TASK_COMPLETION = 5;
	public static final int REDUCE_EXPERIENCE_POINTS = 6;	
	public static final int FIX_EXPERIENCE_POINTS = 7;
	public static final int ADD_EXPERIENCE_POINTS = 8;
	
	public static void insertHistory(RewardUserHistory rewardUserHistory) throws SQLException {
		Connection connection = getConnection();
        try {
        	RewardUsersHistoryDAO.insertValueObject(connection, rewardUserHistory);
        } finally {
            closeConnection(connection);
        }
	}
	
	/**
	 * @param userId
	 * @param tierId
	 * @throws SQLException
	 */
	public static void updateHistoryRemoveRewards(long userId, int tierId, int writerId) throws SQLException {
		Connection connection = getConnection();
        try {
        	RewardUsersHistoryDAO.updateHistoryRemoveRewards(connection, userId, tierId, writerId);
        } finally {
            closeConnection(connection);
        }
	}
}
