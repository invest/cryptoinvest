package com.anyoption.common.managers;

import java.sql.Connection;
import java.sql.SQLException;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.Task;
import com.anyoption.common.beans.TaskGroup;
import com.anyoption.common.daos.TaskDAO;
import com.anyoption.common.util.OracleUtil;

/**
 * @author LioR SoLoMoN
 *
 */
public abstract class TaskManager extends BaseBLManager {
    private static final Logger logger = Logger.getLogger(TaskManager.class);
	
	/**
	 * @param task
	 * @throws SQLException
	 */
	public static void insertTask(Task task) throws SQLException {
		Connection connection = null;
		try {
			connection = getConnection();
			TaskDAO.insertTask(connection, task);
		} finally {
			OracleUtil.closeConnection(connection);
		}
	}
	
	public static void insertTaskGroup(TaskGroup taskGroup) throws SQLException {
		Connection connection = null;
		try {
			connection = getConnection();
			TaskDAO.insertTaskGroup(connection, taskGroup);
		} finally {
			OracleUtil.closeConnection(connection);
		}
	}
}