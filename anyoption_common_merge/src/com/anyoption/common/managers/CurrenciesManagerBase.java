/**
 *
 */
package com.anyoption.common.managers;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.base.Currency;
import com.anyoption.common.daos.CurrenciesDAOBase;
import com.anyoption.common.util.ConstantsBase;

/**
 * @author pavelhe
 *
 */
public class CurrenciesManagerBase extends BaseBLManager {

	private static final Logger log = Logger.getLogger(CurrenciesManagerBase.class);
	protected static Hashtable<Long, Currency> currencies;
	protected static Hashtable<String, Currency> currencyByCode;

	private static final String AMOUNT_FORMAT_PREFIX = "###,###,##0";
	private static final String INPUT_AMOUNT_FORMAT_PREFIX = "###########0";
	private static Map<Integer, String> decPointToAmountFormat = new ConcurrentHashMap<Integer, String>(2);
	private static Map<Integer, String> decPointToInputAmountFormat = new ConcurrentHashMap<Integer, String>(2);

    public static Hashtable<Long, Currency> getAll() throws SQLException {
        Connection conn = null;
        try {
            conn = getConnection();
            return CurrenciesDAOBase.getAll(conn);
        } finally {
            closeConnection(conn);
        }
    }

    public static Currency getCurrency(long id) {
    	getCurrencies();
        return currencies.get(id);
    }

	public static Hashtable<Long, Currency> getCurrencies() {
		if (null == currencies) {
			try {
				currencies = CurrenciesManagerBase.getAll();
				currencyByCode = new Hashtable<String, Currency>();
				Currency c = null;
				for (Iterator<Long> i = currencies.keySet().iterator(); i.hasNext();) {
					Long currencyId = i.next();
					c = currencies.get(currencyId);
	    	        currencyByCode.put(c.getCode(), c);
				}
			} catch (SQLException sqle) {
				log.error("Can't load currencies.", sqle);
			}
		}
		return currencies;
	}

	public static String getAmountFormat(Currency currency, boolean isForInput) {
		int decPointDigits = 2;
    	if (null != currency) {
    	    decPointDigits = currency.getDecimalPointDigits();
    	} else {
    	    try {
    	        decPointDigits = getCurrency(ConstantsBase.CURRENCY_BASE_ID).getDecimalPointDigits();
    	    } catch (Exception e) {
    	        log.error("Can't get decimal points to format amount.", e);
    	    }
    	}
    	Map<Integer, String> formatMap = (isForInput ? decPointToInputAmountFormat : decPointToAmountFormat);
    	String format = formatMap.get(decPointDigits);
    	if (format == null) {
    		format = (isForInput ? INPUT_AMOUNT_FORMAT_PREFIX : AMOUNT_FORMAT_PREFIX);
    		if (decPointDigits > 0) {
    			StringBuilder sb = new StringBuilder(format);
    			sb.append(".")
    				.append(String.format(String.format("%%0%dd", decPointDigits), 0));
    			format = sb.toString();
    		}

    		if (decPointDigits < 0) {
    			log.warn("Digits after the decimapl point are [" + decPointDigits
    						+ "] - will use the default format [" + format + "]");
    		} else {
    			formatMap.put(decPointDigits, format);
    		}
    	}
    	return format;
    }

	/**
	 * @return the currencyByCode
	 */
	public static Hashtable<String, Currency> getCurrencyByCode() {
		getCurrencies();
		return currencyByCode;
	}

	/**
	 * @param currencyByCode the currencyByCode to set
	 */
	public static void setCurrencyByCode(Hashtable<String, Currency> currencyByCode) {
		CurrenciesManagerBase.currencyByCode = currencyByCode;
	}

}
