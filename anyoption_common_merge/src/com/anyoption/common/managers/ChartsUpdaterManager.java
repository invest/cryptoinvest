package com.anyoption.common.managers;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import com.anyoption.common.beans.ChartsUpdaterMarket;
import com.anyoption.common.beans.MarketRate;
import com.anyoption.common.daos.ChartsUpdaterDAO;
import com.anyoption.common.enums.SkinGroup;

public class ChartsUpdaterManager extends BaseBLManager {
    /**
     * Liad market ids.
     *
     * @return <code>ArrayList<Long></code>.
     * @throws SQLException
     */
    public static ArrayList<ChartsUpdaterMarket> getMarkets(long opportunityTypeId) throws SQLException {
        ArrayList<ChartsUpdaterMarket> l = null;
        Connection conn = null;
        try {
            conn = getConnection();
            l = ChartsUpdaterDAO.getChartsUpdaterMarkets(conn, opportunityTypeId);
        } finally {
            closeConnection(conn);
        }
        return l;
    }

    /**
     * Load market rates.
     *
     * @param marketId the market for which to load the rates for
     * @param marketDecimalPoint decimal point digit rounding for the corresponding market
     * @param after if <code>null</code> load all. else load rates after that time
     * @return <code>ArrayList<MarketRate></code>
     * @throws SQLException
     */
    public static Map<SkinGroup, List<MarketRate>> getMarketRates(long marketId, int marketDecimalPoint, Date after) throws SQLException {
        Connection conn = null;
        try {
            conn = getConnection();
            return ChartsUpdaterDAO.getMarketRates(conn, marketId, marketDecimalPoint, after);
        } finally {
            closeConnection(conn);
        }
    }
}