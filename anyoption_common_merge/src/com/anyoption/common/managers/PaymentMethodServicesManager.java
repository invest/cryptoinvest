package com.anyoption.common.managers;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;

import org.jboss.logging.Logger;

import com.anyoption.common.beans.PaymentMethodService;
import com.anyoption.common.daos.PaymentMethodServicesDAO;

/**
 * PaymentMethodServicesManager
 * @author eyalo
 */
public class PaymentMethodServicesManager extends BaseBLManager {
	private static final Logger log = Logger.getLogger(PaymentMethodServicesManager.class);

	/**
	 * Get Services
	 * @return HashMap<Long, HashMap<Long, String>>
	 * @throws SQLException
	 */
	public static ArrayList<PaymentMethodService> getAllServices() throws SQLException {
		Connection con = null;
		try {
			con = getConnection();
			return  PaymentMethodServicesDAO.getAllServices(con);
		} finally {
			closeConnection(con);
		}
	}
	
	/**
	 * Get Service
	 * @param pms
	 * @return String
	 * @throws SQLException
	 */
	public static String getService(PaymentMethodService pms) throws SQLException {
		Connection con = null;
		try {
			con = getConnection();
			return  PaymentMethodServicesDAO.getService(con, pms);
		} finally {
			closeConnection(con);
		}
	}
}
