package com.anyoption.common.managers;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import com.anyoption.common.beans.Fee;
import com.anyoption.common.beans.Skin;
import com.anyoption.common.beans.base.Platform;
import com.anyoption.common.beans.base.TermsPartFile;
import com.anyoption.common.beans.base.TermsParts;
import com.anyoption.common.daos.TermsDAOBase;
import com.anyoption.common.service.requests.TermsFileMethodRequest;
import com.anyoption.common.util.CommonUtil;

public class TermsManagerBase extends BaseBLManager {
	
	private static final String MIN_FIRST_DEPOSIT = "min_first_deposit";
	private static final String MAINTENANCE_FEE = "maintenance_fee";
	private static final String OPTION_PLUS_COMMISSION_FEE = "option_plus_commission_fee";
	private static final String SLIP_1T_MAX_INV_AMOUNT = "slip_1t_max_inv_amount";
	
	private static final String SUPPORT_EMAIL = "support_email";
	private static final String COMPLIANCE_EMAIL = "compliance_email";
	private static final String COMPANY_NAME = "company_name";
	
	private static HashMap<String, String> params;

    public static Map<Long, Map<Long, ArrayList<TermsParts>>> getTermsFilter() throws SQLException {
        Connection conn = null;
        try {
            conn = getConnection();
            return TermsDAOBase.getTermsFilter(conn);
        } finally {
            closeConnection(conn);
        }
    }
    
    public static TermsPartFile getTermsFile(long platformId, long skinId, long partId) throws SQLException {
        Connection conn = null;
        try {
            conn = getConnection();
            return TermsDAOBase.getTermsFile(conn, platformId, skinId, partId);
        } finally {
            closeConnection(conn);
        }
    }
    
    public static ArrayList<TermsPartFile> getTermsList(long platformId, long skinId, long typeId) throws SQLException {
        Connection conn = null;
        try {
            conn = getConnection();
            return TermsDAOBase.getTermsList(conn, platformId, skinId, typeId);
        } finally {
            closeConnection(conn);
        }
    }
	
	public static void updateFile(long fileId, TermsFileMethodRequest request) throws SQLException {
		Connection con = getConnection();
		try {
			TermsDAOBase.updateFile(request, fileId, con);
		} finally {
			closeConnection(con);
		}
	}
	
	public static HashMap<String,String> getParams(long currencyId, long skinId, long platformId) throws SQLException {
		if(currencyId == 0){
			Skin skin =  SkinsManagerBase.getSkin(skinId);
			currencyId = skin.getDefaultCurrencyId();
		}
		return setTermsParams(currencyId, skinId, platformId);
	}
	
	private static HashMap<String,String> setTermsParams(long currencyId, long skinId, long platformId) throws SQLException {
		params = new HashMap<String, String>();
		
		params.put(MIN_FIRST_DEPOSIT, getMinFirstDepositText(currencyId, skinId));
		params.put(MAINTENANCE_FEE, getMaintenanceFeeText(currencyId));
		params.put(OPTION_PLUS_COMMISSION_FEE, getOptionPlusCommissionFeeText(currencyId));
		params.put(SLIP_1T_MAX_INV_AMOUNT, (get1TInvestLimit(currencyId, skinId) / 100) + "");
		
		params.put(SUPPORT_EMAIL, getSupportMail(platformId));
		params.put(COMPLIANCE_EMAIL, getComplianceMail(platformId));
		params.put(COMPANY_NAME, getCompanyName(skinId));
		return params;
	}
	
    private static Long get1TInvestLimit(long currencyId, long skinId) throws SQLException {
        Connection conn = null;
        try {
            conn = getConnection();
            return TermsDAOBase.get1TInvestLimit(conn, currencyId, skinId);
        } finally {
            closeConnection(conn);
        }
    }
    
    private static Long getMinFD(long currencyId, long skinId) throws SQLException {
        Connection conn = null;
        try {
            conn = getConnection();
            return TermsDAOBase.getMinFD(conn, currencyId, skinId);
        } finally {
            closeConnection(conn);
        }
    }
    
	private static String getMinFirstDepositText(long currencyId, long skinId) throws SQLException {
		long amnt = getMinFD(currencyId, skinId);
		String s =  CommonUtil.displayAmount(amnt, true, currencyId, false);
		Locale locale = new Locale(LanguagesManagerBase.getLanguage(SkinsManagerBase.getSkin(skinId).getDefaultLanguageId()).getCode());
		return CommonUtil.getMessage(locale, "CMS.anyoption_agreement.text.text49", new Object[]{s} );
	}
	
	private static String getMaintenanceFeeText(long currencyId) throws SQLException{
		long feeAmount = FeesManagerBase.getFee(Fee.MAINTENANCE_FEE, currencyId);
		return CommonUtil.displayAmount(feeAmount, true, currencyId, false);
	}
	
	private static String getOptionPlusCommissionFeeText(long currencyId) throws SQLException{
		long feeAmount = FeesManagerBase.getFee(Fee.OPTION_PLUS_COMMISSION_FEE, currencyId);
		return CommonUtil.displayAmount(feeAmount, true, currencyId, false);
	}
	
	private static String getSupportMail(long platformId){
		String email = "";
		if(platformId == Platform.ANYOPTION){
			email = "Support@anyoption.com";
		} else if (platformId == Platform.COPYOP) {
			email = "Support@copyop.com";
		}
		return email;
	}	
	
	private static String getComplianceMail(long platformId){
		String email = "";
		if(platformId == Platform.ANYOPTION){
			email = "compliance@anyoption.com";
		} else if (platformId == Platform.COPYOP) {
			email = "compliance@copyop.com";
		}
		return email;
	}
	
	private static String getCompanyName(long skinId){
		Skin skin = SkinsManagerBase.getSkin(skinId);
		String companyName = "";
		if(skin.isRegulated()){
			companyName = "Ouroboros Derivatives Trading Ltd";
		} else if(skin.getBusinessCaseId() == Skin.SKIN_BUSINESS_GOLDBEAM) {
			companyName = "Goldbeam International Inc.";
		} else {
			companyName = "AOPS Ltd.";
		}
		return companyName;
	}
}