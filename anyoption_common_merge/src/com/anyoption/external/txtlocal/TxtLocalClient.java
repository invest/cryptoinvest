package com.anyoption.external.txtlocal;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;

import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;
import com.jcraft.jsch.SftpException;
import com.jcraft.jsch.SftpProgressMonitor;

public class TxtLocalClient {
	private static final Logger log = Logger.getLogger(TxtLocalClient.class);
	
	String host;
	String user;
	String pass;
	String pathToUpload;
	File file;
	
	public TxtLocalClient(String host, String user, String pass, String pathToUpload, File file) {

		if(file == null) {
			throw new IllegalArgumentException("File is null");
		}
		this.host = host;
		this.user = user;
		this.pass = pass;
		this.pathToUpload = pathToUpload;
		this.file = file;
	}
		
	public void upload() throws JSchException, SftpException, FileNotFoundException {
		JSch jsch = new JSch();

        Session session = jsch.getSession(user, host );
        java.util.Properties config = new java.util.Properties();
        config.put("StrictHostKeyChecking", "no");
        config.put("PreferredAuthentications","password");
        session.setConfig(config);

        session.setPassword( pass );
        session.connect( 60000 );
        Channel channel = session.openChannel("sftp");
        ChannelSftp sftp = ( ChannelSftp ) channel;
        try {
	        sftp.connect( 60000 );
	        sftp.cd(pathToUpload);
	
	        InputStream is = new BufferedInputStream(new FileInputStream(file));
	        PutProgressMonitor pm = new PutProgressMonitor();
	        pm.init(0, file.getName(), "", file.length());
	        sftp.put(is, file.getName(), pm, ChannelSftp.OVERWRITE);
	        file.delete();
        } finally {
	        sftp.disconnect();
	        sftp.exit();
        }
	}
	
	  public static class PutProgressMonitor implements SftpProgressMonitor {
		    long count=0;
		    long max=0;
		    long percent=-1;
		    
		    long startTime = 0;
		    
		    public void init(int op, String src, String dest, long max) {
		      log.debug("Start uploading " + src + " " + max);
		      startTime = System.nanoTime();
		      this.max = max;
		      count=0;
		      percent=-1;
		    }

		    public boolean count(long count) {
		      this.count+=count;

		      if(percent>=this.count*100/max){ 
		    	  return true; 
		   	  }
		      percent=this.count*100/max;
		      
		      if(percent % 10 == 0) {
		    	  log.debug("progress "+ percent+" %");
		      }
		      return true;
		    }
		    
		    public void end(){
		    	log.debug("Took " + TimeUnit.NANOSECONDS.toSeconds((System.nanoTime()-startTime))+" seconds");
		    }
		  }
}
