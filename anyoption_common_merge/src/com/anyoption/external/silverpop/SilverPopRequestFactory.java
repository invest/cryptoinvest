package com.anyoption.external.silverpop;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import com.anyoption.external.silverpop.ws.Body;
import com.anyoption.external.silverpop.ws.EXPORTCOLUMNS;
import com.anyoption.external.silverpop.ws.Envelope;
import com.anyoption.external.silverpop.ws.ExportList;
import com.anyoption.external.silverpop.ws.GetJobStatus;
import com.anyoption.external.silverpop.ws.GetLists;
import com.anyoption.external.silverpop.ws.ImportList;
import com.anyoption.external.silverpop.ws.Login;

public class SilverPopRequestFactory {
	
	public static final String DATE_FORMAT = "dd/MM/yyyy HH:mm:ss";

	public static Envelope createLoginRequest(String userName, String password){
		Login login = new Login();
		login.setUSERNAME(userName);
		login.setPASSWORD(password);
		Body body = new Body();
		body.setLogin(login);
		Envelope env = new Envelope();
		env.setBody(body);
		return env;
	}
	
	public static Envelope createGetListRequest() {
		GetLists gl = new GetLists();
		gl.setLISTTYPE((byte) 5);
		gl.setVISIBILITY((byte) 1);
		Body body = new Body();
		body.setGetLists(gl);
		Envelope env = new Envelope();
		env.setBody(body);
		return env;
		
	}
	
	public static Envelope createExportListRequest(int listId, Date from, boolean contacts){
		SimpleDateFormat dt = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
		EXPORTCOLUMNS columns = new EXPORTCOLUMNS();
		if(contacts) {
			columns.getCOLUMN().add("contact_ID");
		} else { // users
			columns.getCOLUMN().add("user_ID");
		}
		columns.getCOLUMN().add("Opted Out Date");
		
		ExportList ex = new ExportList();
		ex.setLISTID(listId);
		ex.setLISTDATEFORMAT(DATE_FORMAT);
		ex.setEXPORTFORMAT("CSV");
		ex.setEXPORTTYPE("OPT_OUT");
		ex.setDATESTART(dt.format(from));
		ex.setDATEEND(dt.format(Calendar.getInstance().getTime()));
		ex.setEXPORTCOLUMNS(columns);
		
		Body body = new Body();
		body.setExportList(ex);
		Envelope env = new Envelope();
		env.setBody(body);
		return env;
	}
	
	public static Envelope createImportListRequest(String sourceFile, String mapFile, String email){
		ImportList il = new ImportList();
		il.setMAPFILE(mapFile);
		il.setSOURCEFILE(sourceFile);
		il.setEMAIL(email);
		
		Body body = new Body();
		body.setImportList(il);
		Envelope env = new Envelope();
		env.setBody(body);
		return env;
	}
	
	public static Envelope createLogoutRequest() {
		Body body = new Body();
		body.setLogout("");
		Envelope env = new Envelope();
		env.setBody(body);
		return env;
	}
	
	
	public static Envelope createGetJobStatusRequest(int jobId){
		GetJobStatus gjs = new GetJobStatus();
		gjs.setJOBID(jobId);
		Body body = new Body();
		body.setGetJobStatus(gjs);
		Envelope env = new Envelope();
		env.setBody(body);
		return env;	
	}
}
