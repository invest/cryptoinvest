package com.invest.common.service.cryptotransaction;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.anyoption.common.daos.DAOBase;
import com.invest.common.enums.OrderState;

/**
 * @author Kiril.m
 */
public class CryptoTransactionsDAOBase extends DAOBase {

	public static boolean updateCryptoTransactionState(Connection con, long transactionId, OrderState state) throws SQLException {
		String sql = "update crypto_trade set status = ? where transaction_id = ?";
		try (PreparedStatement ps = con.prepareStatement(sql)) {
			ps.setInt(1, state.getId());
			ps.setLong(2, transactionId);
			return ps.executeUpdate() == 1;
		}
	}

	public static boolean isDepositDisabled(Connection con) throws SQLException {
		String sql = "select config_value from crypto_config where config_key = ?";
		try (PreparedStatement ps = con.prepareStatement(sql)) {
			ps.setString(1, new String("DEPOSIT_DISABLED"));
			ResultSet rs = ps.executeQuery();
			if (rs.next()) {
				return rs.getBoolean("config_value");
			}
		}
		return true;
	}
}