package com.invest.common.service.cryptotransaction;

import java.sql.Connection;
import java.sql.SQLException;

import org.apache.log4j.Logger;

import com.anyoption.common.managers.BaseBLManager;
import com.invest.common.enums.OrderState;

/**
 * @author Kiril.m
 */
public class CryptoTransactionsManagerBase extends BaseBLManager {

	private static final Logger log = Logger.getLogger(CryptoTransactionsManagerBase.class);
	public static final String ASSIGNEE_FINANCE = "Finance";
	public static final String ASSIGNEE_SUPPORT = "Support";
	public static final String ASSIGNEE_RISK = "Risk";

	public static boolean updateCryptoTransactionState(Connection con, long transactionId, OrderState state) throws SQLException {
		return CryptoTransactionsDAOBase.updateCryptoTransactionState(con, transactionId, state);
	}

	public static boolean isDepositDisabled() {
		Connection con = null;
		try {
			con = getConnection();
			return CryptoTransactionsDAOBase.isDepositDisabled(con);
		} catch (SQLException e) {
			log.debug("Unable to determine disabled deposits", e);
			return true;
		} finally {
			closeConnection(con);
		}
	}
}