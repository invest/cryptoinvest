/**
 * 
 */
package com.invest.common.currencyrates;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.apache.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import com.anyoption.common.beans.Job;
import com.anyoption.common.jobs.ScheduledJob;
import com.anyoption.common.managers.CurrencyRatesManagerBase;

/**
 * @author pavel.t
 *
 */
public class CurrencyRatesECBJob implements ScheduledJob {

	private static final String CURRENCY = "currency";
	private static final String CUBE_NODE = "//Cube/Cube/Cube";
	private static final String RATE = "rate";
	private static final String ECB_ENDPOINT = "http://www.ecb.europa.eu/stats/eurofxref/eurofxref-daily.xml";
	private static final Logger log = Logger.getLogger(CurrencyRatesECBJob.class);

	@Override
	public void setJobInfo(Job job) {
		// TODO Auto-generated method stub

	}

	@Override
	public boolean run() {

		boolean result = true;
		ArrayList<CurrencyRatesECB> currRateList = new ArrayList<>();
		DocumentBuilderFactory builderFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = null;
		
		try {
			
			builder = builderFactory.newDocumentBuilder();
			Document document = null;
			String spec = ECB_ENDPOINT;
			URL url = new URL(spec);
			InputStream is = url.openStream();
			document = builder.parse(is);

			XPathFactory xPathfactory = XPathFactory.newInstance();
			XPath xpath = xPathfactory.newXPath();
			String xPathString = CUBE_NODE;
			XPathExpression expr = xpath.compile(xPathString);
			NodeList nl = (NodeList) expr.evaluate(document, XPathConstants.NODESET);
			for (int i = 0; i < nl.getLength(); i++) {
				Node node = nl.item(i);
				NamedNodeMap attribs = node.getAttributes();
				if (attribs.getLength() > 0) {
					Node currencyAttrib = attribs.getNamedItem(CURRENCY);
					if (currencyAttrib != null) {
						String currencyTxt = currencyAttrib.getNodeValue();
						String rateTxt = attribs.getNamedItem(RATE).getNodeValue();
						currRateList.add(new CurrencyRatesECB(currencyTxt, rateTxt));
					}
				}
			}
			
			currRateList.add(new CurrencyRatesECB("EUR", "1"));//add EUR, because ECB do not provided in the CUBE
			CurrencyRatesManagerBase.updateCurrencyRatesToEur(currRateList); 
			
		} catch (SAXException | IOException | XPathExpressionException | ParserConfigurationException | SQLException | ParseException e) {
			log.error("Enable to get currency rates from ECB due to: ", e);
			result = false;
		}
		
		return result;
	}

	@Override
	public void stop() {
		log.debug(this.getClass() + " stopped");

	}

}
