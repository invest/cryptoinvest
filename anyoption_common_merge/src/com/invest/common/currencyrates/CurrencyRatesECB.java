/**
 * 
 */
package com.invest.common.currencyrates;

/**
 * @author pavel.t
 *
 */
public class CurrencyRatesECB {

	private String currency;
	private String rate;

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "CurrencyRatesECB [currency=" + currency + ", rate=" + rate + "]";
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public String getRate() {
		return rate;
	}

	public void setRate(String rate) {
		this.rate = rate;
	}

	public CurrencyRatesECB(String currency, String rate) {
		super();
		this.currency = currency;
		this.rate = rate;
	}

}
