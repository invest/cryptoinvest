package com.invest.common.enums;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Kiril.m
 */
public enum OrderState {
						READY_FOR_MANUAL_PROCESSING(1), READY_FOR_PROCESSING(10), LOCKED(20), PROCESSING_FAILED(-20), PROCESSED(30),
						CLOSED(40);

	private final int id;
	private static final Map<Integer, OrderState> ID_STATE_MAP = new HashMap<>(OrderState.values().length);
	static {
		for (OrderState state : OrderState.values()) {
			ID_STATE_MAP.put(state.getId(), state);
		}
	}

	private OrderState(int id) {
		this.id = id;
	}

	public int getId() {
		return id;
	}

	public static OrderState getById(int id) {
		OrderState state = ID_STATE_MAP.get(id);
		if (state == null) {
			throw new IllegalArgumentException("No order state with id: " + id);
		} else {
			return state;
		}
	}
}