package com.invest.common.email.beans;

/**
 * @author Kiril.m
 */
public class EmailProvider {

	private long id;
	private String providerClass;
	private String name;
	private String apiKey;

	public EmailProvider(long id, String providerClass, String name, String apiKey) {
		this.id = id;
		this.providerClass = providerClass;
		this.name = name;
		this.apiKey = apiKey;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getProviderClass() {
		return providerClass;
	}

	public void setProviderClass(String providerClass) {
		this.providerClass = providerClass;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getApiKey() {
		return apiKey;
	}

	public void setApiKey(String apiKey) {
		this.apiKey = apiKey;
	}

	@Override
	public String toString() {
		String ls = System.lineSeparator();
		return ls + this.getClass().getName() + ls
				+ super.toString() + ls
				+ "id: " + id + ls
				+ "providerClass: " + providerClass + ls
				+ "name: " + name + ls;
	}
}