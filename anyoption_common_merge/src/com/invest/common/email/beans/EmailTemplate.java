package com.invest.common.email.beans;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Kiril.m
 */
public class EmailTemplate {

	public static final String TEMPLATE_PARAMETER_VERIFICATION_LINK = "-verificationLink-";
	public static final String TEMPLATE_PARAMETER_FIRST_NAME = "{%first name%}";
	public static final String TEMPLATE_PARAMETER_FORGOT_PASSWORD_LINK = "{%RESET_YOUR_PASSWORD%}";
	public static final String TEMPLATE_PARAMETER_SUPPORT_MAIL = "{%SUPPORT_MAIL%}";
	public static final String TEMPLATE_PARAMETER_CRYPTO_CURRENCY = "{%Cryptocurrency%}";
	public static final String TEMPLATE_PARAMETER_TRANSACTION_ID = "{%TRANSACTION ID%}";
	public static final String TEMPLATE_PARAMETER_ACCOUNT_NUMBER = "{%Account number%}";
	public static final String TEMPLATE_PARAMETER_DEPOSIT_AMOUNT = "{%Deposit amount%}";
	public static final String TEMPLATE_PARAMETER_DEPOSIT_CURRENCY = "{%Deposit currency%}";
	public static final String TEMPLATE_PARAMETER_COIN = "{%Coin%}";
	public static final String TEMPLATE_PARAMETER_DOMAIN = "{%domain%}";
	public static final String TEMPLATE_PARAMETER_REJECT_REASON = "{%rejection reason%}";
	public static final String TEMPLATE_PARAMETER_COINSHOP = "{%COINSHOP%}";
	public static final String TEMPLATE_PARAMETER_DOCUMENT_TYPE = "{%document type%}";

	private long id;
	private long userId;
	private String from;
	private String to;
	private String subject;
	private int typeId;
	private String providerId;
	private Map<String, String> params;
	private String paramsStr;
	private Date timeCreated;
	private Date timeSent;
	private boolean success;

	private EmailTemplate() {
		this.params = new HashMap<>();
	}

	public EmailTemplate(long id, long userId, String from, String to, String subject, int typeId, String providerId, String paramsStr) {
		this();
		this.id = id;
		this.userId = userId;
		this.from = from;
		this.to = to;
		this.subject = subject;
		this.typeId = typeId;
		this.providerId = providerId;
		this.paramsStr = paramsStr;
	}

	public EmailTemplate(long userId, String from, String to, String subject, int typeId) {
		this();
		this.userId = userId;
		this.from = from;
		this.to = to;
		this.subject = subject;
		this.typeId = typeId;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public long getUserId() {
		return userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}

	public String getFrom() {
		return from;
	}

	public void setFrom(String from) {
		this.from = from;
	}

	public String getTo() {
		return to;
	}

	public void setTo(String to) {
		this.to = to;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public int getTypeId() {
		return typeId;
	}

	public void setTypeId(int typeId) {
		this.typeId = typeId;
	}

	public String getProviderId() {
		return providerId;
	}

	public void setProviderId(String providerId) {
		this.providerId = providerId;
	}

	public Map<String, String> getParams() {
		return params;
	}

	public void setParams(Map<String, String> params) {
		this.params = params;
	}

	public String getParamsStr() {
		return paramsStr;
	}

	public void setParamsStr(String paramsStr) {
		this.paramsStr = paramsStr;
	}

	public Date getTimeCreated() {
		return timeCreated;
	}

	public void setTimeCreated(Date timeCreated) {
		this.timeCreated = timeCreated;
	}

	public Date getTimeSent() {
		return timeSent;
	}

	public void setTimeSent(Date timeSent) {
		this.timeSent = timeSent;
	}

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	@Override
	public String toString() {
		String ls = System.lineSeparator();
		return ls + this.getClass().getName() + ls
				+ super.toString() + ls
				+ "id: " + id + ls
				+ "userId: " + userId + ls
				+ "from: " + from + ls
				+ "to: " + to + ls
				+ "subject: " + subject + ls 
				+ "typeId: " + typeId + ls
				+ "providerId: " + providerId + ls
				+ "params: " + params + ls
				+ "paramsStr: " + paramsStr + ls
				+ "timeCreated: " + timeCreated + ls
				+ "timeSent: " + timeSent + ls
				+ "success: " + success + ls;
	}
}