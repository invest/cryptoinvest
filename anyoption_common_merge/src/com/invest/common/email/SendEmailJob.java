package com.invest.common.email;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.Job;
import com.anyoption.common.jobs.ScheduledJob;
import com.invest.common.email.api.EmailProviderSenderIfc;
import com.invest.common.email.beans.EmailTemplate;
import com.invest.common.email.factories.EmailProviderFactory;

/**
 * @author Kiril.m
 */
public class SendEmailJob implements ScheduledJob {

	private static final Logger log = Logger.getLogger(SendEmailJob.class);
	private static final String CONFIG_DELIMITER = ",";
	private static final String PARAM_DELIMITER = "=";
	private static final String BATCH_PROCESSING_LIMIT = "batch_processing_limit";
	private static final Map<String, Integer> jobConfig = new HashMap<>();
	static {
		jobConfig.put(BATCH_PROCESSING_LIMIT, null);
	}

	@Override
	public boolean run() {
		log.debug(this.getClass().getSimpleName() + " start");
		for (String key : jobConfig.keySet()) {
			if (jobConfig.get(key) == null) {
				log.error("Incorrect configuration");
				return false;
			}
		}
		List<EmailTemplate> emails = EmailManager.getScheduledEmails(jobConfig.get(BATCH_PROCESSING_LIMIT));
		EmailProviderSenderIfc emailProvider = EmailProviderFactory.getEmailProvider();
		boolean res = true;
		for (EmailTemplate email : emails) {
			if (!emailProvider.sendTemplateEmail(email)) {
				log.debug("Email for user " + email.getUserId() + " with type " + email.getTypeId() + " was not sent successfully");
				res = false;
			}
		}
		log.debug(this.getClass().getSimpleName() + " ends with result " + res);
		return res;
	}

	@Override
	public void setJobInfo(Job job) {
		if (job.getConfig() != null && !job.getConfig().trim().isEmpty()) {
			String[] config = job.getConfig().split(CONFIG_DELIMITER);
			for (String param : config) {
				String[] paramSplit = param.split(PARAM_DELIMITER);
				if (paramSplit.length == 2 && jobConfig.containsKey(paramSplit[0])) {
					try {
						jobConfig.put(paramSplit[0], Integer.valueOf(paramSplit[1]));
					} catch (NumberFormatException e) {
						log.error("Unable to parse to integer: " + paramSplit[1], e);
					}
				}
			}
		}
	}

	@Override
	public void stop() {
		log.debug(this.getClass() + " is stopping");
		for (String key : jobConfig.keySet()) {
			jobConfig.put(key, null);
		}
		log.debug(this.getClass() + " stopped");
	}
}