package com.invest.common.email.providers;

import java.io.IOException;
import java.util.Map;

import org.apache.log4j.Logger;

import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.invest.common.email.EmailManager;
import com.invest.common.email.api.EmailProviderSender;
import com.invest.common.email.beans.EmailProvider;
import com.invest.common.email.beans.EmailTemplate;
import com.sendgrid.Email;
import com.sendgrid.Mail;
import com.sendgrid.Method;
import com.sendgrid.Personalization;
import com.sendgrid.Request;
import com.sendgrid.Response;
import com.sendgrid.SendGrid;

/**
 * @author Kiril.m
 */
public class SendGridProviderSender extends EmailProviderSender {

	private static final Logger log = Logger.getLogger(SendGridProviderSender.class);
	private static EmailProvider provider;

	@Override
	public boolean sendTemplateEmail(EmailTemplate template) {
		Map<String, String> paramsMap = convertParams(template.getParamsStr());
		Mail mail = new Mail();
		mail.setFrom(new Email(template.getFrom()));
		if (template.getSubject() != null) {
			mail.setSubject(template.getSubject());
		}
		Personalization personalization = new Personalization();
		personalization.addTo(new Email(template.getTo()));
		mail.addPersonalization(personalization);
		for (String key : paramsMap.keySet()) {
			mail.getPersonalization().get(0).addSubstitution(key, paramsMap.get(key));
		}
		mail.setTemplateId(template.getProviderId());
		SendGrid sg = new SendGrid(getApiKey());
		Request request = new Request();
		boolean res;
		try {
			request.setMethod(Method.POST);
			request.setEndpoint("mail/send");
			request.setBody(mail.build());
			Response response = sg.api(request);
			log.debug(response.getStatusCode());
			log.debug(response.getBody());
			log.debug(response.getHeaders());
			res = true;
		} catch (IOException e) {
			log.debug("Unable to send template email " + template.getProviderId() + " for user " + template.getUserId(), e);
			res = false;
		}
		res = EmailManager.updateSendingResult(template.getId(), res);
		return res;
	}

	private String getApiKey() {
		if (provider == null) {
			provider = EmailManager.getEmailProvider(EmailManager.SENDGRID_PROVIDER_ID);
			if (provider == null) {
				log.warn("Provider " + EmailManager.SENDGRID_PROVIDER_ID + " was not initialized. Key will be set to null");
				return null;
			}
		}
		return provider.getApiKey();
	}

	@Override
	protected String convertParams(Map<String, String> params) {
		return new GsonBuilder().serializeNulls().create().toJson(params);
	}

	@Override
	protected Map<String, String> convertParams(String params) {
		return new GsonBuilder().serializeNulls().create().fromJson(params, new TypeToken<Map<String, String>>() {}.getType());
	}
}