package com.invest.common.email;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.anyoption.common.daos.DAOBase;
import com.invest.common.email.beans.EmailProvider;
import com.invest.common.email.beans.EmailTemplate;

/**
 * @author Kiril.m
 */
public class EmailDAO extends DAOBase {

	public static boolean scheduleTemplateEmail(Connection con, long userId, long templateId, String params, String from, String to,
												String subject) throws SQLException {
		String sql = "insert into email_templates (id, user_id, type_id, params, time_created, email_from, email_to, subject) "
						+ "values (?, ?, ?, ?, sysdate, ?, ?, ?)";
		try (PreparedStatement ps = con.prepareStatement(sql)) {
			long id = getSequenceNextVal(con, "SEQ_EMAIL_TEMPLATES");
			ps.setLong(1, id);
			ps.setLong(2, userId);
			ps.setLong(3, templateId);
			ps.setString(4, params);
			ps.setString(5, from);
			ps.setString(6, to);
			ps.setString(7, subject);
			return ps.executeUpdate() == 1;
		}
	}

	public static List<EmailTemplate> getScheduledEmails(Connection con, int limit) throws SQLException {
		String sql = "select et.id, et.user_id, et.email_from, et.email_to, et.subject, et.type_id, ett.provider_id, et.params "
					+ "from email_templates et "
						+ "join email_template_types ett on et.type_id = ett.id "
					+ "where success is null and rownum <= ? "
					+ "order by priority";
		List<EmailTemplate> result = new ArrayList<>();
		try (PreparedStatement ps = con.prepareStatement(sql)) {
			ps.setInt(1, limit);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				EmailTemplate email = new EmailTemplate(rs.getLong("id"), rs.getLong("user_id"), rs.getString("email_from"),
														rs.getString("email_to"), rs.getString("subject"), rs.getInt("type_id"),
														rs.getString("provider_id"), rs.getString("params"));
				result.add(email);
			}
		}
		return result;
	}

	public static EmailProvider getEmailProvider(Connection con, long id) throws SQLException {
		String sql = "select id, provider_class, name, api_key from email_providers where id = ?";
		try (PreparedStatement ps = con.prepareStatement(sql)) {
			ps.setLong(1, id);
			ResultSet rs = ps.executeQuery();
			if (rs.next()) {
				return new EmailProvider(rs.getLong("id"), rs.getString("provider_class"), rs.getString("name"), rs.getString("api_key"));
			}
		}
		return null;
	}

	public static boolean updateSendingResult(Connection con, long templateId, boolean result) throws SQLException {
		String sql = "update email_templates set success = ?, time_sent = sysdate where id = ?";
		try (PreparedStatement ps = con.prepareStatement(sql)) {
			ps.setBoolean(1, result);
			ps.setLong(2, templateId);
			return ps.executeUpdate() == 1;
		}
	}
}