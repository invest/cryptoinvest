package com.invest.common.email.api;

import java.util.Map;

import com.invest.common.email.EmailManager;
import com.invest.common.email.beans.EmailTemplate;

/**
 * @author Kiril.m
 */
public abstract class EmailProviderSender implements EmailProviderSenderIfc {

	@Override
	public boolean scheduleTemplateEmail(EmailTemplate template) {
		return EmailManager.scheduleTemplateEmail(	template.getUserId(), template.getTypeId(), convertParams(template.getParams()),
													template.getFrom(), template.getTo(), template.getSubject());
	}

	protected abstract String convertParams(Map<String, String> params);

	protected abstract Map<String, String> convertParams(String params);
}