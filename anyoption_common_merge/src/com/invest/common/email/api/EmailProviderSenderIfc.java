package com.invest.common.email.api;

import com.invest.common.email.beans.EmailTemplate;

/**
 * @author Kiril.m
 */
public interface EmailProviderSenderIfc {

	boolean scheduleTemplateEmail(EmailTemplate template);

	boolean sendTemplateEmail(EmailTemplate template);
}