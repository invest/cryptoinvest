package com.invest.common.email.factories;

import com.invest.common.email.api.EmailProviderSenderIfc;
import com.invest.common.email.providers.SendGridProviderSender;

/**
 * @author Kiril.m
 */
public class EmailProviderFactory {

	public static EmailProviderSenderIfc getEmailProvider() {
		// here factory logic should load the right email provider
		return new SendGridProviderSender();
	}
}