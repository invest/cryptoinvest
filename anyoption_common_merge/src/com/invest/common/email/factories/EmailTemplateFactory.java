package com.invest.common.email.factories;

import com.invest.common.email.EmailManager;
import com.invest.common.email.beans.EmailTemplate;

/**
 * @author Kiril.m
 */
public class EmailTemplateFactory {

	public static EmailTemplate getVerificationTemplate(long userId, String from, String to, String subject, String verificationLink,
														String homepage, String supportEmail) {
		EmailTemplate template = new EmailTemplate(userId, from, to, subject, EmailManager.TEMPLATE_TYPE_VERIFICATION);
		template.getParams().put(EmailTemplate.TEMPLATE_PARAMETER_VERIFICATION_LINK, verificationLink);
		addCommonParameters(template, homepage, supportEmail);
		return template;
	}

	public static EmailTemplate getForgotPasswordTemplate(	long userId, String from, String to, String subject, String name, String link,
															String homepage, String supportEmail) {
		EmailTemplate template;
		if (name == null) {
			template = new EmailTemplate(userId, from, to, subject, EmailManager.TEMPLATE_TYPE_FORGOT_PASSWORD_GENERAL);
		} else {
			template = new EmailTemplate(userId, from, to, subject, EmailManager.TEMPLATE_TYPE_FORGOT_PASSWORD);
			template.getParams().put(EmailTemplate.TEMPLATE_PARAMETER_FIRST_NAME, name);
		}
		template.getParams().put(EmailTemplate.TEMPLATE_PARAMETER_FORGOT_PASSWORD_LINK, link);
		addCommonParameters(template, homepage, supportEmail);
		return template;
	}

	public static EmailTemplate getEditProfileTemplate(	long userId, String from, String to, String subject, String name, String homepage,
														String supportEmail) {
		EmailTemplate template;
		if (name == null) {
			template = new EmailTemplate(userId, from, to, subject, EmailManager.TEMPLATE_TYPE_EDIT_PROFILE_GENERAL);
		} else {
			template = new EmailTemplate(userId, from, to, subject, EmailManager.TEMPLATE_TYPE_EDIT_PROFILE);
			template.getParams().put(EmailTemplate.TEMPLATE_PARAMETER_FIRST_NAME, name);
		}
		addCommonParameters(template, homepage, supportEmail);
		return template;
	}

	public static EmailTemplate getBankDetailsTemplate(	long userId, String from, String to, String subject, String name,
														String cryptoCurrency, long transactionId, double depositAmount,
														String depositCurrency, String homepage, String supportEmail) {
		EmailTemplate template = new EmailTemplate(userId, from, to, subject, EmailManager.TEMPLATE_TYPE_BANK_DETAILS);
		template.getParams().put(EmailTemplate.TEMPLATE_PARAMETER_FIRST_NAME, name);
		template.getParams().put(EmailTemplate.TEMPLATE_PARAMETER_CRYPTO_CURRENCY, cryptoCurrency);
		template.getParams().put(EmailTemplate.TEMPLATE_PARAMETER_TRANSACTION_ID, String.valueOf(transactionId));
		template.getParams().put(EmailTemplate.TEMPLATE_PARAMETER_ACCOUNT_NUMBER, String.valueOf(userId));
		template.getParams().put(EmailTemplate.TEMPLATE_PARAMETER_DEPOSIT_AMOUNT, String.valueOf(depositAmount));
		template.getParams().put(EmailTemplate.TEMPLATE_PARAMETER_DEPOSIT_CURRENCY, depositCurrency);
		addCommonParameters(template, homepage, supportEmail);
		return template;
	}

	public static EmailTemplate getCCTransactionApprovedTemplate(	long userId, String from, String to, String subject, String name,
																	String cryptoCurrency, long transactionId, String domain,
																	double depositAmount, String depositCurrency, String coin,
																	String homepage, String supportEmail) {
		EmailTemplate template = new EmailTemplate(userId, from, to, subject, EmailManager.TEMPLATE_TYPE_CC_TRANSACTION_APPROVED);
		template.getParams().put(EmailTemplate.TEMPLATE_PARAMETER_FIRST_NAME, name);
		template.getParams().put(EmailTemplate.TEMPLATE_PARAMETER_CRYPTO_CURRENCY, cryptoCurrency);
		template.getParams().put(EmailTemplate.TEMPLATE_PARAMETER_TRANSACTION_ID, String.valueOf(transactionId));
		template.getParams().put(EmailTemplate.TEMPLATE_PARAMETER_DOMAIN, domain);
		template.getParams().put(EmailTemplate.TEMPLATE_PARAMETER_DEPOSIT_AMOUNT, String.valueOf(depositAmount));
		template.getParams().put(EmailTemplate.TEMPLATE_PARAMETER_DEPOSIT_CURRENCY, depositCurrency);
		template.getParams().put(EmailTemplate.TEMPLATE_PARAMETER_COIN, coin);
		addCommonParameters(template, homepage, supportEmail);
		return template;
	}

	public static EmailTemplate getCCTransactionFailedTemplate(	long userId, String from, String to, String subject, String name,
																String cryptoCurrency, long transactionId, String domain,
																String rejectReason, String homepage, String supportEmail) {
		EmailTemplate template = new EmailTemplate(userId, from, to, subject, EmailManager.TEMPLATE_TYPE_CC_TRANSACTION_FAILED);
		template.getParams().put(EmailTemplate.TEMPLATE_PARAMETER_FIRST_NAME, name);
		template.getParams().put(EmailTemplate.TEMPLATE_PARAMETER_CRYPTO_CURRENCY, cryptoCurrency);
		template.getParams().put(EmailTemplate.TEMPLATE_PARAMETER_TRANSACTION_ID, String.valueOf(transactionId));
		template.getParams().put(EmailTemplate.TEMPLATE_PARAMETER_DOMAIN, domain);
		template.getParams().put(EmailTemplate.TEMPLATE_PARAMETER_REJECT_REASON, rejectReason);
		addCommonParameters(template, homepage, supportEmail);
		return template;
	}

	public static EmailTemplate getWireTransactionApprovedTemplate(	long userId, String from, String to, String subject, String name,
																	String cryptoCurrency, long transactionId, String domain, String coin,
																	String homepage, String supportEmail) {
		EmailTemplate template = new EmailTemplate(userId, from, to, subject, EmailManager.TEMPLATE_TYPE_WIRE_TRANSACTION_APPROVED);
		template.getParams().put(EmailTemplate.TEMPLATE_PARAMETER_FIRST_NAME, name);
		template.getParams().put(EmailTemplate.TEMPLATE_PARAMETER_CRYPTO_CURRENCY, cryptoCurrency);
		template.getParams().put(EmailTemplate.TEMPLATE_PARAMETER_TRANSACTION_ID, String.valueOf(transactionId));
		template.getParams().put(EmailTemplate.TEMPLATE_PARAMETER_DOMAIN, domain);
		template.getParams().put(EmailTemplate.TEMPLATE_PARAMETER_COIN, coin);
		addCommonParameters(template, homepage, supportEmail);
		return template;
	}

	public static EmailTemplate getWireTransactionFailedTemplate(	long userId, String from, String to, String subject, String name,
																	long transactionId, String domain, String rejectReason, String homepage,
																	String supportEmail) {
		EmailTemplate template = new EmailTemplate(userId, from, to, subject, EmailManager.TEMPLATE_TYPE_WIRE_TRANSACTION_FAILED);
		template.getParams().put(EmailTemplate.TEMPLATE_PARAMETER_FIRST_NAME, name);
		template.getParams().put(EmailTemplate.TEMPLATE_PARAMETER_TRANSACTION_ID, String.valueOf(transactionId));
		template.getParams().put(EmailTemplate.TEMPLATE_PARAMETER_DOMAIN, domain);
		template.getParams().put(EmailTemplate.TEMPLATE_PARAMETER_REJECT_REASON, rejectReason);
		addCommonParameters(template, homepage, supportEmail);
		return template;
	}

	public static EmailTemplate getDocumentRejectedTemplate(long userId, String from, String to, String subject, String name,
															String documentType, String rejectReason, String homepage,
															String supportEmail) {
		EmailTemplate template = new EmailTemplate(userId, from, to, subject, EmailManager.TEMPLATE_TYPE_DOCUMENT_REJECTED);
		template.getParams().put(EmailTemplate.TEMPLATE_PARAMETER_FIRST_NAME, name);
		template.getParams().put(EmailTemplate.TEMPLATE_PARAMETER_DOCUMENT_TYPE, documentType);
		template.getParams().put(EmailTemplate.TEMPLATE_PARAMETER_REJECT_REASON, rejectReason);
		addCommonParameters(template, homepage, supportEmail);
		return template;
	}

	public static EmailTemplate getDocumentsApprovedTemplate(	long userId, String from, String to, String subject, String name,
																String homepage, String supportEmail) {
		EmailTemplate template = new EmailTemplate(userId, from, to, subject, EmailManager.TEMPLATE_TYPE_DOCUMENTS_APPROVED);
		template.getParams().put(EmailTemplate.TEMPLATE_PARAMETER_FIRST_NAME, name);
		addCommonParameters(template, homepage, supportEmail);
		return template;
	}

	public static EmailTemplate getDocumentsReceivedTemplate(	long userId, String from, String to, String subject, String name,
																String homepage, String supportEmail) {
		EmailTemplate template = new EmailTemplate(userId, from, to, subject, EmailManager.TEMPLATE_TYPE_DOCUMENTS_RECEIVED);
		template.getParams().put(EmailTemplate.TEMPLATE_PARAMETER_FIRST_NAME, name);
		addCommonParameters(template, homepage, supportEmail);
		return template;
	}

	private static void addCommonParameters(EmailTemplate template, String homepage, String supportEmail) {
		template.getParams().put(EmailTemplate.TEMPLATE_PARAMETER_COINSHOP, homepage);
		template.getParams().put(EmailTemplate.TEMPLATE_PARAMETER_SUPPORT_MAIL, supportEmail);
	}
}