package com.invest.common.email;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.anyoption.common.managers.BaseBLManager;
import com.invest.common.email.beans.EmailProvider;
import com.invest.common.email.beans.EmailTemplate;

/**
 * @author Kiril.m
 */
public class EmailManager extends BaseBLManager {

	private static final Logger log = Logger.getLogger(EmailManager.class);

	public static final int TEMPLATE_TYPE_VERIFICATION = 1;
	public static final int TEMPLATE_TYPE_FORGOT_PASSWORD = 2;
	public static final int TEMPLATE_TYPE_FORGOT_PASSWORD_GENERAL = 3;
	public static final int TEMPLATE_TYPE_EDIT_PROFILE = 4;
	public static final int TEMPLATE_TYPE_EDIT_PROFILE_GENERAL = 5;
	public static final int TEMPLATE_TYPE_BANK_DETAILS = 6;
	public static final int TEMPLATE_TYPE_CC_TRANSACTION_APPROVED = 7;
	public static final int TEMPLATE_TYPE_CC_TRANSACTION_FAILED = 8;
	public static final int TEMPLATE_TYPE_WIRE_TRANSACTION_APPROVED = 9;
	public static final int TEMPLATE_TYPE_WIRE_TRANSACTION_FAILED = 10;
	public static final int TEMPLATE_TYPE_CRYPTO_SENT = 11;
	public static final int TEMPLATE_TYPE_DOCUMENT_REJECTED = 12;
	public static final int TEMPLATE_TYPE_DOCUMENTS_APPROVED = 13;
	public static final int TEMPLATE_TYPE_DOCUMENTS_RECEIVED = 14;

	public static final long SENDGRID_PROVIDER_ID = 1L;

	public static boolean scheduleTemplateEmail(long userId, long typeId, String params, String from, String to, String subject) {
		Connection con = null;
		try {
			con = getConnection();
			return EmailDAO.scheduleTemplateEmail(con, userId, typeId, params, from, to, subject);
		} catch (SQLException e) {
			log.debug("Unable to schedule email for user" + userId, e);
			return false;
		} finally {
			closeConnection(con);
		}
	}

	public static List<EmailTemplate> getScheduledEmails(int limit) {
		Connection con = null;
		try {
			con = getConnection();
			return EmailDAO.getScheduledEmails(con, limit);
		} catch (SQLException e) {
			log.debug("Unable to load scheduled emails", e);
			return new ArrayList<>();
		} finally {
			closeConnection(con);
		}
	}

	public static EmailProvider getEmailProvider(long id) {
		Connection con = null;
		try {
			con = getConnection();
			return EmailDAO.getEmailProvider(con, id);
		} catch (SQLException e) {
			log.debug("Unable to load provider " + id, e);
			return null;
		} finally {
			closeConnection(con);
		}
	}

	public static boolean updateSendingResult(long templateId, boolean result) {
		Connection con = null;
		try {
			con = getConnection();
			return EmailDAO.updateSendingResult(con, templateId, result);
		} catch (SQLException e) {
			log.debug("Unable to update result for template: " + templateId);
			return false;
		} finally {
			closeConnection(con);
		}
	}
}