package com.invest.common.documents;

import java.util.Map;

import com.anyoption.common.beans.File;
import com.anyoption.common.beans.FileType;

public class DocumentsHelper {

	public static boolean isPassportApproved(long typeId, Map<Long, File> documents) {
		return isPassportType(typeId) && isApprovedDocuments(	documents.get(FileType.PASSPORT_FRONT), documents.get(FileType.PASSPORT_BACK),
																documents.get(FileType.PASSPORT_SELFIE));
	}

	public static boolean isIdApproved(long typeId, Map<Long, File> documents) {
		return isIdType(typeId) && isApprovedDocuments(	documents.get(FileType.NATIONAL_ID_FRONT), documents.get(FileType.NATIONAL_ID_BACK),
														documents.get(FileType.NATIONAL_ID_SELFIE));
	}

	public static boolean isPassReceived(long typeId, Map<Long, File> documents) {
		return isPassportType(typeId) && isReceivedDocuments(	documents.get(FileType.PASSPORT_FRONT), documents.get(FileType.PASSPORT_BACK),
																documents.get(FileType.PASSPORT_SELFIE));
	}

	public static boolean isIdReceived(long typeId, Map<Long, File> documents) {
		return isIdType(typeId) && isReceivedDocuments(	documents.get(FileType.NATIONAL_ID_FRONT), documents.get(FileType.NATIONAL_ID_BACK),
														documents.get(FileType.NATIONAL_ID_SELFIE));
	}

	public static boolean isApprovedDocuments(File front, File back, File selfie) {
		if (front == null || back == null || selfie == null) {
			return false;
		}
		return front.isApproved() && back.isApproved() && selfie.isApproved();
	}

	public static boolean isReceivedDocuments(File front, File back, File selfie) {
		if (front == null || back == null || selfie == null) {
			return false;
		}
		return !front.isSupportReject() && !back.isSupportReject() && !selfie.isSupportReject();
	}

	public static boolean isPassportType(long typeId) {
		return typeId == FileType.PASSPORT_FRONT || typeId == FileType.PASSPORT_BACK || typeId == FileType.PASSPORT_SELFIE;
	}

	public static boolean isIdType(long typeId) {
		return typeId == FileType.NATIONAL_ID_FRONT || typeId == FileType.NATIONAL_ID_BACK || typeId == FileType.NATIONAL_ID_SELFIE;
	}
}