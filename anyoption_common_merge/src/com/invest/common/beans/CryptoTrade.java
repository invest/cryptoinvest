/**
 * 
 */
package com.invest.common.beans;

import java.io.Serializable;
import java.util.Date;

/**
 * @author pavel.t
 *
 */
public class CryptoTrade implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7625605404767667197L;
	
	private int id;
	private String market;
	private String fromCurrency;
	private String toCurrency;
	private String clientCurrency;
	private Double quantity;
	private Double quantityClient;
	private long amount;
	private long originalAmount;
	private Double fee;
	private Double clearanceFee;
	private Double rate;
	private Double rateMarketClient;
	private Double rateMarket;
	private int status;
	private int transactionId;
	private String uuid;
	private String comments;
	private Date timeCreated;
	private Date timeOpened;
	private Date timeDone;
	private String wallet;
	private int userId;
	private boolean isCurrencyNeedToBeSet;
	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}
	/**
	 * @return the market
	 */
	public String getMarket() {
		return market;
	}
	/**
	 * @param market the market to set
	 */
	public void setMarket(String market) {
		this.market = market;
	}
	/**
	 * @return the fromCurrency
	 */
	public String getFromCurrency() {
		return fromCurrency;
	}
	/**
	 * @param fromCurrency the fromCurrency to set
	 */
	public void setFromCurrency(String fromCurrency) {
		this.fromCurrency = fromCurrency;
	}
	/**
	 * @return the toCurrency
	 */
	public String getToCurrency() {
		return toCurrency;
	}
	/**
	 * @param toCurrency the toCurrency to set
	 */
	public void setToCurrency(String toCurrency) {
		this.toCurrency = toCurrency;
	}
	/**
	 * @return the clientCurrency
	 */
	public String getClientCurrency() {
		return clientCurrency;
	}
	/**
	 * @param clientCurrency the clientCurrency to set
	 */
	public void setClientCurrency(String clientCurrency) {
		this.clientCurrency = clientCurrency;
	}
	/**
	 * @return the quantity
	 */
	public Double getQuantity() {
		return quantity;
	}
	/**
	 * @param quantity the quantity to set
	 */
	public void setQuantity(Double quantity) {
		this.quantity = quantity;
	}
	/**
	 * @return the quantityClient
	 */
	public Double getQuantityClient() {
		return quantityClient;
	}
	/**
	 * @param quantityClient the quantityClient to set
	 */
	public void setQuantityClient(Double quantityClient) {
		this.quantityClient = quantityClient;
	}
	/**
	 * @return the fee
	 */
	public Double getFee() {
		return fee;
	}
	/**
	 * @param fee the fee to set
	 */
	public void setFee(Double fee) {
		this.fee = fee;
	}
	/**
	 * @return the clearanceFee
	 */
	public Double getClearanceFee() {
		return clearanceFee;
	}
	/**
	 * @param clearanceFee the clearanceFee to set
	 */
	public void setClearanceFee(Double clearanceFee) {
		this.clearanceFee = clearanceFee;
	}
	/**
	 * @return the rate
	 */
	public Double getRate() {
		return rate;
	}
	/**
	 * @param rate the rate to set
	 */
	public void setRate(Double rate) {
		this.rate = rate;
	}
	/**
	 * @return the rateMarketClient
	 */
	public Double getRateMarketClient() {
		return rateMarketClient;
	}
	/**
	 * @param rateMarketClient the rateMarketClient to set
	 */
	public void setRateMarketClient(Double rateMarketClient) {
		this.rateMarketClient = rateMarketClient;
	}
	/**
	 * @return the rateMarket
	 */
	public Double getRateMarket() {
		return rateMarket;
	}
	/**
	 * @param rateMarket the rateMarket to set
	 */
	public void setRateMarket(Double rateMarket) {
		this.rateMarket = rateMarket;
	}
	/**
	 * @return the status
	 */
	public int getStatus() {
		return status;
	}
	/**
	 * @param status the status to set
	 */
	public void setStatus(int status) {
		this.status = status;
	}
	/**
	 * @return the transactionId
	 */
	public int getTransactionId() {
		return transactionId;
	}
	/**
	 * @param transactionId the transactionId to set
	 */
	public void setTransactionId(int transactionId) {
		this.transactionId = transactionId;
	}
	/**
	 * @return the uuid
	 */
	public String getUuid() {
		return uuid;
	}
	/**
	 * @param uuid the uuid to set
	 */
	public void setUuid(String uuid) {
		this.uuid = uuid;
	}
	/**
	 * @return the comments
	 */
	public String getComments() {
		return comments;
	}
	/**
	 * @param comments the comments to set
	 */
	public void setComments(String comments) {
		this.comments = comments;
	}
	/**
	 * @return the timeCreated
	 */
	public Date getTimeCreated() {
		return timeCreated;
	}
	/**
	 * @param timeCreated the timeCreated to set
	 */
	public void setTimeCreated(Date timeCreated) {
		this.timeCreated = timeCreated;
	}
	/**
	 * @return the timeOpened
	 */
	public Date getTimeOpened() {
		return timeOpened;
	}
	/**
	 * @param timeOpened the timeOpened to set
	 */
	public void setTimeOpened(Date timeOpened) {
		this.timeOpened = timeOpened;
	}
	/**
	 * @return the timeDone
	 */
	public Date getTimeDone() {
		return timeDone;
	}
	/**
	 * @param timeDone the timeDone to set
	 */
	public void setTimeDone(Date timeDone) {
		this.timeDone = timeDone;
	}
	/**
	 * @return the wallet
	 */
	public String getWallet() {
		return wallet;
	}
	/**
	 * @param wallet the wallet to set
	 */
	public void setWallet(String wallet) {
		this.wallet = wallet;
	}
	
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	public long getAmount() {
		return amount;
	}
	public void setAmount(long amount) {
		this.amount = amount;
	}
	public long getOriginalAmount() {
		return originalAmount;
	}
	public void setOriginalAmount(long originalAmount) {
		this.originalAmount = originalAmount;
	}
	public boolean isCurrencyNeedToBeSet() {
		return isCurrencyNeedToBeSet;
	}
	public void setCurrencyNeedToBeSet(boolean isCurrencyNeedToBeSet) {
		this.isCurrencyNeedToBeSet = isCurrencyNeedToBeSet;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "CryptoTrade [id=" + id + ", market=" + market + ", fromCurrency=" + fromCurrency + ", toCurrency="
				+ toCurrency + ", clientCurrency=" + clientCurrency + ", quantity=" + quantity + ", quantityClient="
				+ quantityClient + ", amount=" + amount + ", originalAmount=" + originalAmount + ", fee=" + fee
				+ ", clearanceFee=" + clearanceFee + ", rate=" + rate + ", rateMarketClient=" + rateMarketClient
				+ ", rateMarket=" + rateMarket + ", status=" + status + ", transactionId=" + transactionId + ", uuid="
				+ uuid + ", comments=" + comments + ", timeCreated=" + timeCreated + ", timeOpened=" + timeOpened
				+ ", timeDone=" + timeDone + ", wallet=" + wallet + ", userId=" + userId + ", isCurrencyNeedToBeSet="
				+ isCurrencyNeedToBeSet + "]";
	}
}
