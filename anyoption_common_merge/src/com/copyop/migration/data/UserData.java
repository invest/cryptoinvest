package com.copyop.migration.data;

import java.util.Date;

public class UserData {
	long id;
	String firstName;
	String lastName;
	String email;
	String gender;
	boolean test;
	boolean frozen;
	int bestStreak;
	int bestStreakAmount;
	long bestAsset;
	Date timeCreated;
	
	public UserData() {
		
	}
	public UserData(long id, String firstName, String lastName, String email, boolean test, int bestStreak, Date timeCreated) {
		super();
		this.id = id;
		this.firstName = firstName;
		this.lastName = lastName;
		this.email = email;
		this.test = test;
		this.bestStreak = bestStreak;
		this.timeCreated = timeCreated;
	}
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public int getBestStreak() {
		return bestStreak;
	}
	public boolean isTest() {
		return test;
	}
	public void setTest(boolean test) {
		this.test = test;
	}
	public boolean isFrozen() {
		return frozen;
	}
	public void setFrozen(boolean frozen) {
		this.frozen = frozen;
	}
	public void setBestStreak(int bestStreak) {
		this.bestStreak = bestStreak;
	}
	public int getBestStreakAmount() {
		return bestStreakAmount;
	}
	public void setBestStreakAmount(int bestStreakAmount) {
		this.bestStreakAmount = bestStreakAmount;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public Date getTimeCreated() {
		return timeCreated;
	}
	public void setTimeCreated(Date timeCreated) {
		this.timeCreated = timeCreated;
	}
	public long getBestAsset() {
		return bestAsset;
	}
	public void setBestAsset(long bestAsset) {
		this.bestAsset = bestAsset;
	}
	@Override
	public String toString() {
		return "UserData [id=" + id + ", firstName=" + firstName
				+ ", lastName=" + lastName + ", email=" + email + ", gender="
				+ gender + ", test=" + test + ", frozen=" + frozen
				+ ", bestStreak=" + bestStreak + ", bestStreakAmount="
				+ bestStreakAmount + ", timeCreated=" + timeCreated + "]";
	}
	
}
