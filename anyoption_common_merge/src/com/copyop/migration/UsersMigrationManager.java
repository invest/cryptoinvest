package com.copyop.migration;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;

import com.anyoption.common.managers.BaseBLManager;
import com.copyop.common.dto.Profile;
import com.copyop.common.managers.ManagerBase;
import com.copyop.common.managers.ProfileManager;
import com.copyop.migration.data.UserData;
import com.datastax.driver.core.BoundStatement;
import com.datastax.driver.core.Session;

public class UsersMigrationManager extends BaseBLManager
{
	private static final Logger logger = Logger.getLogger(UsersMigrationManager.class);

	private static final String oneUserSQL = 
		"SELECT " + 
			"u.ID duid, " + 
			"u.first_name, " +
			"u.last_name, " +
			"u.gender, " +
			"u.email, " +
			"u.time_created, " +
			"u.id useridA, " +
			"u.class_id " +
		"FROM " +
			"users u " +
		"WHERE " + 
			" u.id = ? " ;
	
	private static String getEligibleUsersSQL(Date userCreated) { 
		String sql = 
		"SELECT " +
					" * " +
		"FROM " +
					"( " +
						"SELECT " +
									"DISTINCT(u.ID) AS duid, " +
									"((TRUNC(SYSDATE) - TRUNC(MAX(i.time_created)))) AS days_since_last_inv, " +
									"u.first_name, " +
									"u.last_name, " +
									"u.email , " +
									"u.gender, " +
									"u.time_created, " +
									"u.class_id " +	
						"FROM " +
									"users u, " +
									"investments i, " +
									"opportunities o " +
						"WHERE " +
									"u.first_deposit_id		IS NOT NULL " +
									"AND u.skin_Id NOT IN	(1, 4) " + // not from etrader 
									"AND u.country_id != 220 " + // not with country US
									"AND u.class_id			!= 0 " +
									"AND (o.scheduled = 1 OR i.is_like_hourly  = 1 ) " +
									"AND o.opportunity_type_id = 1 " +
									"AND i.opportunity_id	= o.ID " +
									"AND i.user_id			= u.ID " +
									"AND i.is_canceled		= 0 " +
									"AND i.time_created		>= u.time_created ";
						if(userCreated != null) {
							sql += "AND u.time_created > ? " ;
						}
						sql += 
						"GROUP BY " +
									"u.ID, " +
									"u.first_name, " +
									"u.last_name, " +
									"u.gender, " +
									"u.time_created, " +
								    "u.class_id, " +
									"u.email " +
						"HAVING COUNT(i.ID) > 2 " +
					") " +
			"WHERE days_since_last_inv < 30 " ;
		
		return sql;
	}
	
	static String getSqlForUsersExport(Long userId, Date userCreated) {
		
		String exportUsersSql =
				"SELECT * " +
						"FROM " +
							"(" ;
									if(userId != null) {
										exportUsersSql += oneUserSQL;
									} else {
										exportUsersSql += getEligibleUsersSQL(userCreated);
									}
		exportUsersSql +=									
							") A " + 
						"LEFT JOIN " +
							"( " +
								"WITH stp AS " +
											"(" +
												"SELECT " +
															"userid, " +
															"SUM(am) streakAmount, " +
															"COUNT(*) AS streak " +
												"FROM " +
														"( " +
																"SELECT " +
																			"am, " +
																			"userid, " +
																			"rez, " +
																			"SUM( CASE WHEN rez <> 'Y' THEN 1 END) OVER (PARTITION BY userid ORDER BY iid ROWS UNBOUNDED PRECEDING) AS dummy " +
																"  FROM " +
																			"(" +
																					"SELECT " +
																						"i.id iid, " +
																						"i.user_id userid, " +
																						"(win - lose) am, " +
																						"DECODE(SIGN(win-lose), 1, 'Y', ' ') rez " +
																					"FROM " +
																						"investments i, " +
																						"opportunities o, " +
																						"users u " +
																					"WHERE " +
																							"u.ID = i.user_id " +
																							"AND i.is_canceled = 0 " +
																							"AND i.copyop_type_id != 1 " +
																							"AND (o.scheduled = 1 OR i.is_like_hourly  = 1 ) " +
																							"AND o.opportunity_type_id = 1 " + // binary
																							"AND i.opportunity_id = o.id ";
																		if(userId != null){
																			exportUsersSql+= 
																							"AND U.id = ? ";
																		}
																		exportUsersSql +=																								
																					"ORDER BY iid " +
																			") " +
															") " +
												"WHERE rez = 'Y' " +
												"GROUP BY userid, dummy " +
												"ORDER BY userid, streak " +
											") " +
											"SELECT " +
													" * " +
											"FROM " +
													"stp u " +
											"WHERE " +
													"u.streak = " +
													"( " +
														"SELECT MAX(streak) FROM stp WHERE UserID = u.UserID " +
													") " +
													" AND ROWNUM = 1 " +
													"ORDER BY userID asc, streakamount desc " +
							") B " +
						"ON " +
							"A.duid = B.userid " +
						"JOIN " +
						 		"(" +
						 			"SELECT user_id, is_frozen FROM copyop_frozen" +
						 		") cf " +
			 			"ON A.duid = cf.user_Id";
		return exportUsersSql;
 	}
	

	
    // initial migration
    public static void exportUsers() throws SQLException {
    	exportUser(null, null);
    }
    
    // single user from json
    public static void exportUser(Long userId) throws SQLException {
    	exportUser(userId, null);
    }
    
    // single user from job
    public static void exportUser(Date usersCreatedSince) throws SQLException {
    	exportUser(null, usersCreatedSince);
    }
    
	public static void exportUser(Long userId, Date userCreated) throws SQLException {
		Connection connection = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
				connection = getConnection();
				long startTime = System.nanoTime();
				String exportUsersSql = getSqlForUsersExport(userId, userCreated);
				ps = connection.prepareStatement(exportUsersSql);
				// prepare parameters
				if(userId != null && userCreated == null) { // register user from json
					ps.setLong(1, userId);
					ps.setLong(2, userId);
				}
				if(userId == null && userCreated != null) { // migrate from job
					ps.setDate(1, new java.sql.Date(userCreated.getTime()));
				}
				if(userId != null && userCreated != null) { // safety  
					ps.setDate(1, new java.sql.Date(userCreated.getTime()));
					ps.setLong(2, userId);
					ps.setLong(3, userId);
				}
				
				if(userId == null) {
					int totalCount = count(connection, exportUsersSql, userCreated);
					logger.info("Start migrating: "+totalCount + " users");
				} else {
					logger.info("Start migrating: "+userId);
				}
				rs = ps.executeQuery();
				int count = 0;
				
//			    BlockingQueue<Runnable> blockingQueue = new ArrayBlockingQueue<Runnable>(10);
//			    RejectedExecutionHandler rejectedExecutionHandler = new ThreadPoolExecutor.CallerRunsPolicy();
//			    ExecutorService executorService =  new ThreadPoolExecutor(3, 10, 0L, TimeUnit.MILLISECONDS, blockingQueue, rejectedExecutionHandler);
				
				while (rs.next()) {
					UserData user = new UserData();
					user.setId(rs.getLong("duid"));
					user.setFirstName(rs.getString("first_name"));
					user.setEmail(rs.getString("email")); 
					user.setLastName(rs.getString("last_name"));
					user.setFrozen(rs.getInt("is_frozen")==1);
					user.setBestStreak(rs.getInt("streak"));
					user.setBestStreakAmount(rs.getInt("streakAmount"));
					user.setGender(rs.getString("gender"));
					user.setTimeCreated(rs.getDate("time_created"));
					user.setTest(rs.getLong("class_id")==0);
					Profile profile = ProfileManager.getProfile(user.getId());
					if(profile == null) {
						ProfilesMigrationManager.importProfile(user);
						//executorService.submit(new runnableMigration(user));
						count ++;
					} else {
						logger.info("Existing profile in copyop:"+user.getId());
					}
					
					if(count%500 == 0) {
						logger.info("Migrated "+count+" users. Time :"+TimeUnit.NANOSECONDS.toSeconds((System.nanoTime()-startTime))+" seconds");
					}
				}
				long endTime = System.nanoTime();
				long timeNanos = endTime - startTime;
				logger.info("Migration done. "+count+" users. Took :"+ TimeUnit.NANOSECONDS.toMinutes(timeNanos)+" minutes" );
		} finally {
			closeConnection(connection);
		}
	}

	static class runnableMigration extends Thread {
		UserData user;
		public runnableMigration(UserData user) {
			setName(String.valueOf(user.getId()));
			this.user = user;
		}
		public void run() {
			logger.info("running "+getName());
			try {
				sleep(800);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			//ProfilesMigrationManager.importProfile(user);
		}
	}
	
	private static int count(Connection conn, String sql, Date date) throws SQLException {
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
        		String countsql = "SELECT count(distinct(duid)) FROM ( " + sql + ")";
	            ps = conn.prepareStatement(countsql);
	            if(date!= null) {
	            	ps.setDate(1, new java.sql.Date(date.getTime()));
	            }
	            rs = ps.executeQuery();
	            if(rs.next()) {
	            	return rs.getInt(1);
	            }
        }catch(Exception e){
        	e.printStackTrace();
        } finally {
            rs.close();
        }
        return 0;
	}
	
//	public static void generatePassword() throws SQLException {
//		Connection connection = null;
//		PreparedStatement ps = null;
//		ResultSet rs = null;
//		try {
//				connection = getConnection();
//				long startTime = System.nanoTime();
//				System.out.println("Generation startL at: "+ TimeUnit.NANOSECONDS.toMinutes(startTime)+" minutes" );
//				String exportUsersSql = "select * from etrader.password_reset where password_revert is null ";
//				ps = connection.prepareStatement(exportUsersSql);
//				// prepare parameters
//				
//				rs = ps.executeQuery();
//				int count = 0;
//				
//				while (rs.next()) {
//					PreparedStatement ps2 = null;
//					String updateUsersSql = "update etrader.password_reset set PASSWORD = ?, password_revert = ? where user_id = ? ";
//					ps2 = connection.prepareStatement(updateUsersSql);
//					
//					try {
//						String encryptedPassword = Encryptor.encryptStringToString(rs.getString("password_back"));
//						ps2.setString(1, encryptedPassword);
//						String decryptedPassword = Encryptor.decryptStringToString(encryptedPassword);
//						ps2.setString(2, decryptedPassword);
//					} catch (CryptoException ce) {
//						throw new SQLException("CryptoException: " + ce.getMessage());
//					}
//					
//					ps2.setLong(3, rs.getLong("user_id"));
//					
//					ps2.executeUpdate();
//					ps2.close();
//					count++;
//				}
//				long endTime = System.nanoTime();
//				long timeNanos = endTime - startTime;
//				System.out.println("Generation done. Took: " + TimeUnit.NANOSECONDS.toMinutes(timeNanos)+" minutes, count: " + count );
//		} finally {
//			closeConnection(connection);
//		}
//	}
	
	public static void generateAvatar() {
		Session session = ManagerBase.getSession();
		com.datastax.driver.core.PreparedStatement ps = null;
		com.datastax.driver.core.PreparedStatement psRow = null;
			
		String cql =
					"SELECT user_id, avatar FROM profiles ";
			ps = session.prepare(cql);
			
		String cqlRow =
					"UPDATE profiles SET avatar = ? where user_id = ? ";
			psRow = session.prepare(cqlRow);	

		BoundStatement boundStatement = new BoundStatement(ps);
		BoundStatement boundStatementRow = new BoundStatement(psRow);
		
		com.datastax.driver.core.ResultSet rs = session.execute(boundStatement);
		int i=0;
		while (!rs.isExhausted()) {
			com.datastax.driver.core.Row oneRow = rs.one();
			String avatar = oneRow.getString("avatar");
			long userId = oneRow.getLong("user_id");
		try {	
			if (!avatar.isEmpty() && avatar != null && avatar.startsWith("http://www.copyop.com")) {
				System.out.println(avatar);
				avatar = avatar.replaceFirst("http://", "https://");
				boundStatementRow.bind(avatar, userId);
				session.execute(boundStatementRow);
				i++;
			}
		} catch (Exception E) {
			E.printStackTrace();
			System.out.println("error: " + avatar);
		}
		}
		System.out.println("count: " + i);

}
	
}
