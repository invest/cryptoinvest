/**
 *
 */
package com.copyop.common;



/**
 * @author pavelhe
 *
 */
public interface Constants {

	public static final long HOUR_MILLIS = 60L*60L*1000L;
	public static final long MINUTE_MILLIS = 60L*1000L;
	public static final long NO_TTL = -1L;

	public static final String JNDI_NAME_ACTIVEMQ = "java:/AMQConnectionFactory";
	public static final String JNDI_NAME_ACTIVEMQ_TOMCAT = "java:/comp/env/AMQConnectionFactory";

	/*
	 * 	3 constants copied from activemq-client-5.9.1.jar
	 *  org.apache.activemq.ScheduledMessage
	 *  in order to avoid direct dependency from
	 *  ActiveMQ API
	 */
	//set a message to wait with an initial delay (milliseconds)
	public static final String AMQ_SCHEDULED_DELAY = "AMQ_SCHEDULED_DELAY";
	//waiting N milliseconds between each re-delivery:
	public static final String AMQ_SCHEDULED_PERIOD = "AMQ_SCHEDULED_PERIOD";
	// repeat delivery N times
	public static final String AMQ_SCHEDULED_REPEAT = "AMQ_SCHEDULED_REPEAT";

	public static final String INBOX_PERIOD_FORMAT = "yyyy-MM";

	/*
	 * the format for date used to store count in trades history map
	 */
	public static final String TRADES_HISTORY_DATE_FORMAT = "yyyy.MM.dd";

	public static final String BIND_SESSION_PROFILE = "copyopProfile";
	public static final String BIND_SESSION_PROFILE_LINK = "copyopProfileLink";
	
	public static final String AFF_SUB1 = "aff_sub1";
	public static final String AFF_SUB2 = "aff_sub2";
	public static final String AFF_SUB3 = "aff_sub3";
	public static final String GCLID = "gclid";
	public static final String DPN = "dpn";
	public static final long USER_ID_ZERO = 0;
	
	public static final long UNICELL_PROVIDER_ET = 1;
	public static final long UNICELL_PROVIDER_AO = 2;
	//public static final long INFORU_PROVIDER_AO = 3;
	public static final long MBLOX_PROVIDER = 4;
	public static final int MAX_CHARACTERS_ALLOWED_BY_MBLOX_PROVIDER = 156;
	public static final long TEXTLOCAL_PROVIDER = 5;
	public static final int MAX_CHARACTERS_ALLOWED_BY_MOBIVATE_PROVIDER = 160;
	public static final long MOBIVATE_PROVIDER = 6;
	
	public static final String OFFSET_GMT = "GMT+00:00";
}
