package com.copyop.common.dto.base;

import java.io.Serializable;
import java.util.Date;
import java.util.Map;
import java.util.UUID;

public class FeedMessage implements Serializable  {


	private static final long serialVersionUID = 236371345496713941L;

	public static long HOT_SYSTEM_FEED_USER_ID = -1;
	public static final String PROPERTY_KEY_USER_ID						= "cpop_userId";
	public static final String PROPERTY_KEY_HIT_RATE					= "cpop_hitRate";
	public static final String PROPERTY_KEY_PROFIT						= "cpop_profit";
	public static final String PROPERTY_KEY_ASSET_CREATE_DATE_TIME 		= "cpop_assetCreateDateTime";
	public static final String PROPERTY_KEY_ASSET_LAST_TRADING_HOURS 	= "cpop_lastTradinghours";
	public static final String PROPERTY_KEY_COPIED_COUNT 				= "cpop_copiedCount";
	public static final String PROPERTY_KEY_RANKED_IN_TOP				= "cpop_ranked_in_top";
	public static final String PROPERTY_KEY_TOP_RANK					= "cpop_top_ranke";
	public static final String PROPERTY_KEY_AMOUNT						= "cpop_amount";
	public static final String PROPERTY_KEY_ASSET_TREND			= "cpop_trend";
	public static final String PROPERTY_KEY_INVESTMENT_TYPE		= "cpop_investmentType";
	public static final String PROPERTY_KEY_OPPORTUNITY_EXPIRE  = "cpop_opportunityExpire";
	public static final String PROPERTY_KEY_OPPORTUNITY_ID		= "cpop_opportunityId";
	public static final String PROPERTY_KEY_MARKET_ID			= "cpop_marketId";
	public static final String PROPERTY_KEY_SCHEDULED 			= "cpop_scheduled";

	public static final String PROPERTY_KEY_INV_ID			= "cpop_invId";
	public static final String PROPERTY_KEY_LEVEL			= "cpop_level";
	public static final String PROPERTY_KEY_AMOUNT_IN_CURR	= "cpop_amount_in_curr_";
	public static final String PROPERTY_KEY_DIRECTION		= "cpop_dir";
	public static final String PROPERTY_KEY_SRC_USER_ID		= "cpop_srcUserId";
	public static final String PROPERTY_KEY_SRC_NICKNAME	= "cpop_srcNickname";
	public static final String PROPERTY_KEY_SRC_AVATAR		= "cpop_srcAvatar";
	public static final String PROPERTY_KEY_DEST_USER_ID	= "cpop_destUserId";
	public static final String PROPERTY_KEY_DEST_NICKNAME	= "cpop_destNickname";
	public static final String PROPERTY_KEY_TIME_SETTLED	= "cpop_time_settled";
	public static final String PROPERTY_KEY_ODDS_WIN		= "cpop_oddsWin";
	public static final String PROPERTY_KEY_ODDS_LOSE		= "cpop_oddsLose";
	public static final String PROPERTY_KEY_TIME_CREATED	= "cpop_time_created";
	public static final String PROPERTY_KEY_COPIERS			= "cpop_copiers";
	public static final String PROPERTY_KEY_COPYING			= "cpop_copying";
	public static final String PROPERTY_KEY_WATCHERS		= "cpop_watchers";
	public static final String PROPERTY_KEY_WATCHING		= "cpop_watching";
	public static final String PROPERTY_KEY_NICKNAME		= "cpop_nickname";
	public static final String PROPERTY_KEY_OLD_NICKNAME	= "cpop_old_nickname";
	public static final String PROPERTY_KEY_AVATAR			= "cpop_avatar";
	public static final String PROPERTY_KEY_COINS_BALANCE	= "cpop_coins_balance";
	public static final String PROPERTY_KEY_UPDATE_TYPE		= "cpop_update_type";
	public static final String PROPERTY_KEY_FIRST_NAME		= "cpop_first_name";
	public static final String PROPERTY_KEY_LAST_NAME		= "cpop_last_name";
	public static final String PROPERTY_KEY_SEATS_LEFT		= "cpop_seats_left";
	public static final String PROPERTY_KEY_PUSH_SKIN_ID	= "cpop_push_skin_id";
	public static final String PROPERTY_KEY_PUSH_CURRENCY_ID= "cpop_push_currency_id";
	public static final String PROPERTY_KEY_PUSH_UTC_OFFSET	= "cpop_push_utc_offset";
	public static final String PROPERTY_BONUS_TYPE_ID		= "cpop_bonus_type_id";
	public static final String PROPERTY_BONUS_DESCRIPTION	= "cpop_bonus_description";
	public static final String PROPERTY_KEY_USER_FROZEN		= "cpop_user_frozen";
	public static final String PROPERTY_KEY_DEST_USER_FROZEN= "cpop_dest_user_frozen";
	public static final String PROPERTY_KEY_UUID			= "cpop_uuid";
	public static final String PROPERTY_KEY_INHERITED_UUID	= "cpop_inherited_uuid";
	public static final String PROPERTY_KEY_TIME_LAST_INVEST = "cpop_time_last_invest";

	public static final String NEWS_FEED_CAPACITY = "NEWS_FEED_CAPACITY";
	public static final String EXPLORE_FEED_CAPACITY = "EXPLORE_FEED_CAPACITY";

	public static final int INBOX_FEED_MESSAGE_PAGE_SIZE		= 15;
	public static final String INBOX_FEED_MESSAGE_PERIOD_KEY 	= "Inbox_date_period";
	public static final String INBOX_FEED_MESSAGE_TIMEUUID_KEY 	= "Inbox_timeuuid";
	
	public static final String PUSH_SCREEN_DPL = "screen_dpl";
	public static final String PUSH_SCREEN_USER_PROFILE = "user_profile";
	public static final String PUSH_USER_ID_DPL = "user_id_dpl";
	
	protected long userId;
	protected int queueType;
	protected Date timeCreated;
	protected UUID timeCreatedUUID;
	protected int msgType;
	protected Map<String, String> properties;

	public FeedMessage() {
		// Nothing to do
	}

	public long getUserId() {
		return userId;
	}
	public void setUserId(long userId) {
		this.userId = userId;
	}
	public int getQueueType() {
		return queueType;
	}
	public void setQueueType(int queueType) {
		this.queueType = queueType;
	}
	public Date getTimeCreated() {
		return timeCreated;
	}
	/**
	 * @param timeCreated the timeCreated to set
	 */
	public void setTimeCreated(Date timeCreated) {
		this.timeCreated = timeCreated;
	}
	/**
	 * @return the timeCreatedUUID
	 */
	public UUID getTimeCreatedUUID() {
		return timeCreatedUUID;
	}
	/**
	 * @param timeCreatedUUID the timeCreatedUUID to set
	 */
	public void setTimeCreatedUUID(UUID timeCreatedUUID) {
		this.timeCreatedUUID = timeCreatedUUID;
	}
	public int getMsgType() {
		return msgType;
	}
	public void setMsgType(int msgType) {
		this.msgType = msgType;
	}
	public Map<String, String> getProperties() {
		return properties;
	}
	public void setProperties(Map<String, String> properties) {
		this.properties = properties;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("FeedMessage [userId=").append(userId)
				.append(", queueType=").append(queueType)
				.append(", timeCreated=").append(timeCreated)
				.append(", timeCreatedUUID=").append(timeCreatedUUID)
				.append(", msgType=").append(msgType)
				.append(", properties=").append(properties).append("]");
		return builder.toString();
	}
}