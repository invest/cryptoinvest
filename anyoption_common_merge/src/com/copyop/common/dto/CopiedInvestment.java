package com.copyop.common.dto;

import java.util.Date;

public class CopiedInvestment {

	long userId;
	long copyFromUserId;
	long marketId;
	int copiedCount;
	int copiedCountSuccess;
	float successRate;
	long profit;
	boolean isAfterLastLogOff;
	Date timeCreated;
	
	public CopiedInvestment(){
		
	}
	
	public CopiedInvestment(long userId, long copyFromUserId, long marketId) {
		this.marketId = marketId;
		this.userId = userId;
		this.copyFromUserId = copyFromUserId;
		this.copiedCount = 0;
	}
	
	public CopiedInvestment(long userId, long copyFromUserId, int copiedCount, long profit, boolean isAfterLastLogOff) {
		this.userId = userId;
		this.copyFromUserId = copyFromUserId;
		this.copiedCount = copiedCount;
		this.profit = profit;
		this.isAfterLastLogOff = isAfterLastLogOff;
	}
	
	public long getUserId() {
		return userId;
	}
	public void setUserId(long userId) {
		this.userId = userId;
	}
	public long getCopyFromUserId() {
		return copyFromUserId;
	}
	public void setCopyFromUserId(long copyFromUserId) {
		this.copyFromUserId = copyFromUserId;
	}
	public long getMarketId() {
		return marketId;
	}
	public void setMarketId(long marketId) {
		this.marketId = marketId;
	}
	public int getCopiedCount() {
		return copiedCount;
	}
	public void setCopiedCount(int copiedCount) {
		this.copiedCount = copiedCount;
	}
	public long getProfit() {
		return profit;
	}
	public void setProfit(long profit) {
		this.profit = profit;
	}
	
	public void increaseCount(){
		this.copiedCount++;
	}
	public int getCopiedCountSuccess() {
		return copiedCountSuccess;
	}
	public void setCopiedCountSuccess(int copiedCountSuccess) {
		this.copiedCountSuccess = copiedCountSuccess;
	}
	public float getSuccessRate() {
		return successRate;
	}
	public void setSuccessRate(float successRate) {
		this.successRate = successRate;
	}
	
	public boolean isAfterLastLogOff() {
		return isAfterLastLogOff;
	}

	public void setAfterLastLogOff(boolean isAfterLastLogOff) {
		this.isAfterLastLogOff = isAfterLastLogOff;
	}
	
	public Date getTimeCreated() {
		return timeCreated;
	}

	public void setTimeCreated(Date timeCreated) {
		this.timeCreated = timeCreated;
	}

	@Override
	public String toString() {
		return "CopiedInvestment [userId=" + userId + ", copyFromUserId="
				+ copyFromUserId + ", marketId=" + marketId + ", copiedCount="
				+ copiedCount + ", copiedCountSuccess=" + copiedCountSuccess
				+ ", successRate=" + successRate + ", profit=" + profit + "]";
	}

}
