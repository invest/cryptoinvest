/**
 *
 */
package com.copyop.common.enums.base;

import java.util.HashMap;
import java.util.Map;

/**
 * @author pavelhe
 *
 */
public enum UpdateFamilyEnum {
	FACEBOOK_FRIENDS(1), WATCHINGS(2), COPYINGS(3), SYSTEM(4), SPECIAL(5), ABOUT_ME(6), HOT(7);

	private static final Map<Integer, UpdateFamilyEnum> ID_TO_ENUM_VALUE_MAP = new HashMap<Integer, UpdateFamilyEnum>(UpdateFamilyEnum.values().length);
	static {
		for (UpdateFamilyEnum enumVal : UpdateFamilyEnum.values()) {
			ID_TO_ENUM_VALUE_MAP.put(enumVal.getId(), enumVal);
		}
	}

	private final int id;

	UpdateFamilyEnum(int id) {
		this.id = id;
	}

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	public static UpdateFamilyEnum getById(int id) {
		UpdateFamilyEnum enumVal = ID_TO_ENUM_VALUE_MAP.get(id);
		if (enumVal == null) {
			throw new IllegalArgumentException("No enum value with ID: " + id);
		} else {
			return enumVal;
		}
	}
}
