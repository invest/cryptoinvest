package com.copyop.common.enums.base;

import java.util.EnumSet;
import java.util.HashMap;
import java.util.Map;

public enum UpdateTypeEnum {
	FB_NEW_FRIEND(1),
	FB_NEW_WATCHING(2),
	FB_NEW_COPYING(3),
	FB_NEW_INVESTMENT(4),
	FB_SHOW_OFF(5),
	FB_IN_MONEY_EXPIRY(6),
	W_NEW_INVESTMENT(7),
	W_SHOW_OFF(8),
	W_NEARLY_FULL(9),
	W_100_EXPIRY(10),
	W_NOW_AVAILABLE(11),
	CP_NEW_COPYING(12),
	CP_SHOW_OFF(13),
	CP_FROZE(14),
	SYS_24H_MOST_COPIED(15),
	SYS_8H_HIGHEST_SR(16),
	SYS_8H_MOST_PROFITABLE(17),
	SYS_1H_TRENDIEST(18),
	SYS_ANY_NEW_INV(19),
	SYS_ANY_SHOW_OFF(20),
	SYS_ANY_IN_MONEY_EXPIRY(21),
	SYS_NICKNAME_CHANGED(22),
	SP_TODAYS_OFFER(23),
	AB_BALANCE_LESS_MIN_WARN(24),
	AB_BALANCE_LESS_MIN_COPIED(25),
	AB_BALANCE_LESS_MIN_COPYING(26),
	AB_IM_FULLY_COPIED(27),
	AB_IM_TOP_PROFITABLE(28),
	AB_I_SELF_INVESTED(29),
	AB_I_COPIED_INVESTMENT(30),
	AB_FAILURE_SELF_INVEST(31),
	AB_MY_EXPIRY(32),
	AB_REACHED_COINS(33),

	//Hot Feed Msgs
	BEST_TRADERS(61),
	BEST_TRADERS_ONE_WEEK(611),
	BEST_TRADERS_ONE_MOTNH(612),
	BEST_TRADERS_TRHEE_MOTNH(613),
	
	BEST_TRADES(62),
	BEST_TRADES_ONE_WEEK(621),
	BEST_TRADES_ONE_MOTNH(622),
	BEST_TRADES_TRHEE_MOTNH(623),
	
	BEST_COPIERS(63),
	BEST_COPIERS_ONE_WEEK(631),
	BEST_COPIERS_ONE_MOTNH(632),
	BEST_COPIERS_TRHEE_MOTNH(633),
	
	BEST_ASSET_SPECIALISTS(64),
	BEST_ASSET_SPECIALISTS_ONE_WEEK(641),
	BEST_ASSET_SPECIALISTS_ONE_MOTNH(642),
	BEST_ASSET_SPECIALISTS_TRHEE_MOTNH(643),
	;

	private static final Map<Integer, UpdateTypeEnum> ID_TO_UPDATE_TYPE_MAP = new HashMap<Integer, UpdateTypeEnum>(UpdateTypeEnum.values().length);
	static {
		for (UpdateTypeEnum updateType : UpdateTypeEnum.values()) {
			ID_TO_UPDATE_TYPE_MAP.put(updateType.getId(), updateType);
		}
	}

	private final int id;
	private PriorityEnum priority;
	private long ttlMillis;
	private UpdateFamilyEnum updateFamily;
	private EnumSet<QueueTypeEnum> wherePosted;

	UpdateTypeEnum(int id) {
		this.id = id;
	}

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param ttlMillis the ttlMillis to set
	 */
	public void setTtlMillis(long ttlMillis) {
		this.ttlMillis = ttlMillis;
	}

	/**
	 * @return the ttlMillis
	 */
	public long getTtlMillis() {
		return ttlMillis;
	}

	/**
	 * @param updateFamily the updateFamily to set
	 */
	public void setUpdateFamily(UpdateFamilyEnum updateFamily) {
		this.updateFamily = updateFamily;
	}

	/**
	 * @return the updateFamily
	 */
	public UpdateFamilyEnum getUpdateFamily() {
		return updateFamily;
	}

	/**
	 * @param wherePosted the wherePosted to set
	 */
	public void setWherePosted(EnumSet<QueueTypeEnum> wherePosted) {
		this.wherePosted = wherePosted;
	}

	/**
	 * @return the wherePosted
	 */
	public EnumSet<QueueTypeEnum> getWherePosted() {
		return wherePosted;
	}

	/**
	 * @param priority the priority to set
	 */
	public void setPriority(PriorityEnum priority) {
		this.priority = priority;
	}

	/**
	 * @return the priority
	 */
	public PriorityEnum getPriority() {
		return priority;
	}

	public static UpdateTypeEnum getById(int id) {
		UpdateTypeEnum updateType = ID_TO_UPDATE_TYPE_MAP.get(id);
		if (updateType == null) {
			throw new IllegalArgumentException("No update type with ID: " + id);
		} else {
			return updateType;
		}
	}

}
