/**
 *
 */
package com.copyop.common.enums.base;

import java.util.HashMap;
import java.util.Map;

/**
 * @author pavelhe
 *
 */
public enum QueueTypeEnum {
	NEWS(1),
	EXPLORE(2),
	INBOX(3),
	MOBILE_NOTIFICATION(4),
	IN_APP_STRIP(5),
	HOT(6),
	SYSTEM(7),
	WEB_POPUP(8);

	private static final Map<Integer, QueueTypeEnum> ID_TO_QUEUE_TYPE = new HashMap<Integer, QueueTypeEnum>(QueueTypeEnum.values().length);
	static {
		for (QueueTypeEnum queueType : QueueTypeEnum.values()) {
			ID_TO_QUEUE_TYPE.put(queueType.getId(), queueType);
		}
	}

	private final int id;

	QueueTypeEnum(int id) {
		this.id = id;
	}

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	public static QueueTypeEnum getById(int id) {
		QueueTypeEnum queueType = ID_TO_QUEUE_TYPE.get(id);
		if (queueType == null) {
			throw new IllegalArgumentException("No queue type with ID: " + id);
		} else {
			return queueType;
		}
	}

}
