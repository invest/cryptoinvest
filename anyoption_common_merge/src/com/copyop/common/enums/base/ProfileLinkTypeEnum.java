package com.copyop.common.enums.base;

public enum ProfileLinkTypeEnum {
	COPY(1), WATCH(2), COPIED_BY(3), WATCHED_BY(4);
	
	private int code;
	 
	private ProfileLinkTypeEnum(int c) {
		code = c;
	}

	public int getCode() {
		return code;
	}
	
	public static ProfileLinkTypeEnum of(int linkType) {

	    switch (linkType) {
	        case 1: 
        		return COPY;
	        case 2:
	        	return WATCH;
	        case 3:
	        	return COPIED_BY;
	        case 4:
	        	return WATCHED_BY;
	        default: 
	        	return null;

	    }
	}
	
}
