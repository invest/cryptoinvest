package com.copyop.common.enums.base;


public enum UserStateEnum {
	
	STATE_REGULAR(0), STATE_BLOCKED(1), STATE_REMOVED(2);
	
	private final int id;

	private UserStateEnum(int id) {
		this.id = id;
	}

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}
	
	public static UserStateEnum of(int state) {

	    switch (state) {
	        case 0: 
        		return STATE_REGULAR;
	        case 1:
	        	return STATE_BLOCKED;
	        case 2:
	        	return STATE_REMOVED;
	        default: 
	        	return null;

	    }
	}
}
