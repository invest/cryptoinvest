package com.copyop.common.managers;

import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Comparator;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.UUID;

import org.apache.log4j.Logger;

import com.anyoption.common.managers.UsersManagerBase;
import com.copyop.common.dao.FeedDAO;
import com.copyop.common.dto.base.FeedMessage;
import com.copyop.common.enums.base.QueueTypeEnum;
import com.copyop.common.enums.base.UpdateTypeEnum;
import com.copyop.common.enums.base.UserStateEnum;
import com.datastax.driver.core.Session;
import com.datastax.driver.core.utils.UUIDs;

public class CopiedManager extends ManagerBase {

	private static final Logger log = Logger.getLogger(CopiedManager.class);

	public static void updateCopiedCounters(long userId) {
		Date timeInsert = new Date();
		SimpleDateFormat formatterHour = new SimpleDateFormat("dd/MM/yyyy HH");
		try {
			timeInsert = formatterHour.parse(formatterHour.format(timeInsert));
		} catch (ParseException e) {
			log.error("When format date ", e);
		}
		updateCopiedCounters(timeInsert, userId);
	}
	
	public static void updateUncopiedCounters(long userId) {
		Date timeInsert = new Date();
		SimpleDateFormat formatterHour = new SimpleDateFormat("dd/MM/yyyy HH");
		try {
			timeInsert = formatterHour.parse(formatterHour.format(timeInsert));
		} catch (ParseException e) {
			log.error("When format date ", e);
		}
		updateUncopiedCounters(timeInsert, userId);
	}

	public static void updateCopiedCounters(Date timeInsert, long userId) {
		Session session = getSession();
		FeedDAO.updateCopiedCounters(session, timeInsert, userId);
	}
	
	public static void updateUncopiedCounters(Date timeInsert, long userId) {
		Session session = getSession();
		FeedDAO.updateUncopiedCounters(session, timeInsert, userId);
	}

	public static void deleteCopiedCounters(int lessHoursBack) {
		GregorianCalendar gc = new GregorianCalendar();
		gc.add(Calendar.HOUR, (0 - (lessHoursBack)));
		SimpleDateFormat formatterHour = new SimpleDateFormat("dd/MM/yyyy HH");
		Session session = getSession();
		int br = 0;
		while (br < 10){
			try {
				gc.add(Calendar.HOUR, (-1));
				Date dt = gc.getTime();
				dt = formatterHour.parse(formatterHour.format(dt));
				FeedDAO.deleteCopiedCounters(session, dt);
				br ++;
				log.debug(dt.toString());
			} catch (ParseException e) {
				log.error("When get date in back for delete old data", e);
			}
		}
	}

	public static ArrayList<FeedMessage> getCopiedCounters(long lessHoursBack, long returnRecords ) {
		GregorianCalendar gc = new GregorianCalendar();
		SimpleDateFormat formatterHour = new SimpleDateFormat("dd/MM/yyyy HH");
		Session session = getSession();
		int br = 0;
		Map<Long, Integer> insertedUserMsg = FeedManager.getFeedPostedUsersMsgNow();
		Map<String, Long> copiedCountersHM = new HashMap<String, Long>();
		while (br < lessHoursBack){
			try {
				gc.add(Calendar.HOUR, (-1));
				Date dt = gc.getTime();
				dt = formatterHour.parse(formatterHour.format(dt));
				Map<Long, Long> copiedHours = FeedDAO.getCopiedCounters(session, dt, insertedUserMsg);

				for (Map.Entry<Long, Long> copied : copiedHours.entrySet()) {
					long userId = 0;
					long count = 0;
					if(copiedCountersHM.get(String.valueOf(copied.getKey())) != null){
						userId = copied.getKey();
						count = copiedCountersHM.get(String.valueOf(copied.getKey())) + copied.getValue();
					} else {
						userId = copied.getKey();
						count = copied.getValue();
					}

					copiedCountersHM.put(String.valueOf(userId), count);
				}

				br ++;
			} catch (ParseException e) {
				log.error("When get date in back for delete old data", e);
			}

		}

        ValueComparator vc =  new ValueComparator(copiedCountersHM);
        TreeMap<String, Long> copiedCountersSorted = new TreeMap<String, Long>(vc);
        copiedCountersSorted.putAll(copiedCountersHM);

		return getMostCopied(lessHoursBack, returnRecords, copiedCountersSorted);
	}

	private static ArrayList<FeedMessage> getMostCopied(long lessHoursBack, long returnRecords, Map<String, Long> copiedCountersHM) {
		int i = 0;
		ArrayList<FeedMessage> list = new ArrayList<FeedMessage>();
		long minCopiers = new Long(ConfigurationCopyopManager.getCopyopPropertiesValue("SYSTEM_MOST_COPIED_MIN_COPIERS", "5"));
		
		//Get CopyopStatus
		HashMap<Long, Integer> usersCopyopStatus = new HashMap<Long, Integer>();
		List<Long> userIds = new ArrayList<Long>();
		for(Map.Entry<String, Long> copiedCount : copiedCountersHM.entrySet()){
			long userId =  new Long(copiedCount.getKey());
			userIds.add(userId);
		}
		try {
			usersCopyopStatus = UsersManagerBase.getUsersCopyopStatus(userIds);
		} catch (SQLException e) {
			log.error("Can't get CopyopStatus", e);
		}
		
		
			for (Map.Entry<String, Long> copiedCount : copiedCountersHM.entrySet()) {
				if (i <= returnRecords - 1) {
					FeedMessage mostCopied = new FeedMessage();

					mostCopied.setUserId(FeedMessage.HOT_SYSTEM_FEED_USER_ID);
					mostCopied.setQueueType(QueueTypeEnum.SYSTEM.getId());
					UUID timeCreatedUUID = UUIDs.timeBased();
					mostCopied.setTimeCreatedUUID(timeCreatedUUID);
					mostCopied.setTimeCreated(new Date(UUIDs.unixTimestamp(timeCreatedUUID)));
					mostCopied.setMsgType(UpdateTypeEnum.SYS_24H_MOST_COPIED.getId());

					Map<String, String> properties = new HashMap<String, String>();
					properties.put(FeedMessage.PROPERTY_KEY_USER_ID, copiedCount.getKey());
					properties.put(FeedMessage.PROPERTY_KEY_COPIED_COUNT, String.valueOf(copiedCount.getValue()));
					properties.put(FeedMessage.PROPERTY_KEY_ASSET_LAST_TRADING_HOURS, String.valueOf(lessHoursBack));
					mostCopied.setProperties(properties);

					if(copiedCount.getValue() >= minCopiers
							&& !ProfileManager.isFrozenProfile(new Long(copiedCount.getKey()))
							&& usersCopyopStatus.get(new Long(copiedCount.getKey())) == UserStateEnum.STATE_REGULAR.getId()){
						list.add(mostCopied);
						i++;
					}
			}
		}
		return list;
	}
}


class ValueComparator implements Comparator<String> {

    Map<String, Long> base;
    public ValueComparator(Map<String, Long> base) {
        this.base = base;
    }

    @Override
	public int compare(String a, String b) {
        if (base.get(a) >= base.get(b)) {
            return -1;
        } else {
            return 1;
        }
    }
}
