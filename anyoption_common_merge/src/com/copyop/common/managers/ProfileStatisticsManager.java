package com.copyop.common.managers;

import java.util.Date;
import java.util.List;

import com.copyop.common.dao.ProfileStatisticsDAO;
import com.copyop.common.dto.ProfileInvResultCopy;
import com.copyop.common.dto.ProfileInvResultSelf;
import com.copyop.common.enums.base.UserStateEnum;
import com.datastax.driver.core.Session;

public class ProfileStatisticsManager  extends ManagerBase {

	public static ProfileInvResultSelf getProfileInvResultSelf(long userId, int limit, UserStateEnum state) {
		Session session = getSession();
		return ProfileStatisticsDAO.getProfileInvResultSelf(session, userId, limit, state == UserStateEnum.STATE_BLOCKED);
	}

	public static void deleteProfileInvResultSelf(long userId) {
		Session session = getSession();
		ProfileStatisticsDAO.deleteProfileInvResultSelf(session, userId);
	}
	
	public static List<Long> getLastSelfInvMarkets(long userId) {
		Session session = getSession();
		return ProfileStatisticsDAO.getLastSelfInvMarkets(session, userId);
	}
	
	public static ProfileInvResultCopy getProfileInvResultCopy(long userId, int limit) {
		Session session = getSession();
		return ProfileStatisticsDAO.getProfileInvResultCopy(session, userId, limit);
	}

	public static void insertProfileInvResultCopy(boolean result, long userId) {
		Session session = getSession();
		ProfileStatisticsDAO.insertProfileInvResultCopy(session, result, userId);
	}
	
	public static void insertProfileInvResultCopy(boolean result, long userId, Date timeCreated) {
		Session session = getSession();
		ProfileStatisticsDAO.insertProfileInvResultCopy(session, result, userId, timeCreated);
	}

	public static void insertProfileInvResultSelf(boolean result, long marketId, long userId) {
		Session session = getSession();
		ProfileStatisticsDAO.insertProfileInvResultSelf(session, result, marketId, userId);
	}

	public static long getSelfInvestmentsCount(long userId, int limit, boolean getBlockedData) {
		Session session = getSession();
		return ProfileStatisticsDAO.getSelfInvestmentsCount(session, userId, limit, getBlockedData);
	}
}