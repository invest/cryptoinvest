package com.copyop.common.managers;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;

import org.apache.log4j.Logger;

import com.copyop.common.dao.CurrenciesCacheDAO;
import com.copyop.common.dto.base.FeedMessage;

public class CurrenciesCacheManager extends com.anyoption.common.managers.BaseBLManager {
	private static final Logger logger = Logger.getLogger(CurrenciesCacheManager.class);
	
	static HashMap<Long, Double> currenciesRatesToUSD;
	static Date lastUpdated;
	
	static HashMap<Long, Double> loadCurrenciesRatesToUSD() throws SQLException {
		Connection connection = null;
		try{
			connection = getConnection();
			HashMap<Long, Double> rates = CurrenciesCacheDAO.loadCurrenciesRatesToUSD(connection);
			lastUpdated = Calendar.getInstance().getTime();
			return rates;
		} finally {
			closeConnection(connection);
		}
	}

	public static synchronized HashMap<Long, Double> getCurrenciesRatesToUSD() {

		if(currenciesRatesToUSD == null || !sameDay()) {
			try {
				logger.debug("Loading currencies cache from DB");
				currenciesRatesToUSD = loadCurrenciesRatesToUSD();
			} catch (SQLException e) {
				currenciesRatesToUSD = new HashMap<Long, Double>();
				lastUpdated = null;
				logger.debug("Can't load currencies rates to USD", e);
			}
		}
		return currenciesRatesToUSD;
	}
	
	public static HashMap<String, String> getConvertedAmounts(double amountInUSD) {
		HashMap<String, String> convertedAmounts = new HashMap<String, String>();
		Iterator<Long> iterator = getCurrenciesRatesToUSD().keySet().iterator();
		while(iterator.hasNext()) {
			long currencyId = iterator.next();
			double rate = currenciesRatesToUSD.get(currencyId);
			BigDecimal convertedAmount = new BigDecimal(amountInUSD).multiply(new BigDecimal(rate)).setScale(2, RoundingMode.CEILING);
			convertedAmounts.put(FeedMessage.PROPERTY_KEY_AMOUNT_IN_CURR+currencyId, ""+convertedAmount.doubleValue());
		}
		convertedAmounts.put(FeedMessage.PROPERTY_KEY_AMOUNT_IN_CURR+"2", ""+amountInUSD);
		return convertedAmounts;
	}
	
	public static void reset() {
		currenciesRatesToUSD = null;
		lastUpdated = null;
	}
	
	static boolean sameDay(){
		if(lastUpdated == null){
			return false;
		}
		Calendar cal1 = Calendar.getInstance();
		cal1.setTime(lastUpdated);
		Calendar cal2 = Calendar.getInstance();
		cal2.setTime(new Date());
		
		return cal1.get(Calendar.YEAR) == cal2.get(Calendar.YEAR) &&
                cal1.get(Calendar.DAY_OF_YEAR) == cal2.get(Calendar.DAY_OF_YEAR);
	}
}
