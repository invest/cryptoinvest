package com.copyop.common.managers;

import java.util.List;

import org.apache.log4j.Logger;

import com.copyop.common.dao.ProfileCountersDAO;
import com.copyop.common.dao.ProfileDAO;
import com.copyop.common.dao.ProfileLinksDAO;
import com.copyop.common.dto.ProfileLink;
import com.copyop.common.enums.base.ProfileLinkTypeEnum;
import com.datastax.driver.core.Session;

public class FixCountersManager extends ManagerBase {
	
	private static final Logger log = Logger.getLogger(FixCountersManager.class);
	
	public static void counterFix(long userId){
		Session session = getSession();
		counterFix(session, userId);
	}
	
	public static boolean counterFix(Session session, long userId){
		boolean isTest = ProfileDAO.isTestProfile(session, userId);
		if(!isTest) {
			int copy  = ProfileLinksDAO.getProfileLinksByType(session, userId, ProfileLinkTypeEnum.COPY).size();
			List<ProfileLink> copiedLinks = ProfileLinksDAO.getProfileLinksByType(session, userId, ProfileLinkTypeEnum.COPIED_BY);
			int copied = copiedLinks.size();
			for(ProfileLink link:copiedLinks) {
				if(ProfileDAO.isTestProfile(session, link.getDestUserId())) {
					copied--;
				}
			}
			int watch = ProfileLinksDAO.getProfileLinksByType(session, userId, ProfileLinkTypeEnum.WATCH).size();
			List<ProfileLink> watchedLinks = ProfileLinksDAO.getProfileLinksByType(session, userId, ProfileLinkTypeEnum.WATCHED_BY);
			int watched = watchedLinks.size();
			for(ProfileLink link:watchedLinks) {
				if(ProfileDAO.isTestProfile(session, link.getDestUserId())) {
					watched--;
				}
			}
			List<Long> currentCounters = ProfileCountersDAO.loadCounters(session, userId);
			if(currentCounters.size() > 0) {
				log.debug("user id: "+userId);
				log.debug("old values "+ currentCounters);
				log.debug("new values "+ copy + " " + copied + " " + watch + " " + watched);
				log.debug("-----------------------");
				ProfileCountersDAO.decreaseCounters(session, userId, currentCounters.get(0), currentCounters.get(1), currentCounters.get(2), currentCounters.get(3));
				ProfileCountersDAO.increaseCounters(session, userId, copy, copied, watch, watched);
			} else {
				log.debug("user id:" +userId + " no records to fix");
			}
			return true;
		} else {
			return false;
		}
	}

}
