/**
 *
 */
package com.copyop.common.managers;

import org.apache.log4j.Logger;

import com.copyop.common.dao.BlockUserDAO;
import com.datastax.driver.core.Session;

/**
 * @author pavelhe
 *
 */
public class BlockUserManager extends ManagerBase {
	private static final Logger log = Logger.getLogger(BlockUserManager.class);

	public static void blockUser(long userId) {
		Session session = getSession();
		BlockUserDAO.blockUser(session, userId);
	}

	public static void unBlockUser(long userId) {
		Session session = getSession();
		BlockUserDAO.unBlockUser(session, userId);
	}

}
