/**
 *
 */
package com.copyop.common.managers;

import java.util.List;

import org.apache.log4j.Logger;

import com.copyop.common.dao.InboxDAO;
import com.copyop.common.dto.base.FeedMessage;
import com.datastax.driver.core.Session;

/**
 * @author pavelhe
 *
 */
public class InboxManager extends ManagerBase {

	private static final Logger log = Logger.getLogger(InboxManager.class);

	public static List<FeedMessage> getInboxFeed(long userId, String period, String timeUUID, int inboxFeedMessagePageSize) {
		Session session = getSession();
		List<FeedMessage> result;
		long beginTime = 0;
		long endTime = 0;
		if (log.isTraceEnabled()) {
			beginTime = System.currentTimeMillis();
		}
		if (timeUUID != null) {
			result = InboxDAO.getInboxFeed(session, userId, period, timeUUID, inboxFeedMessagePageSize);
		} else {
			result = InboxDAO.getInboxFeed(session, userId, period, inboxFeedMessagePageSize);
		}
		if (log.isTraceEnabled()) {
			endTime = System.currentTimeMillis();
			log.trace("TIME: get inbox feed " + (endTime - beginTime) + "ms");
		}
		return result;
	}
}