package com.copyop.common.managers;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.apache.log4j.Logger;

import com.copyop.common.dao.FeedDAO;
import com.copyop.common.dto.base.FeedMessage;
import com.copyop.common.enums.base.QueueTypeEnum;
import com.copyop.common.enums.base.UpdateTypeEnum;
import com.datastax.driver.core.Session;
import com.datastax.driver.core.utils.UUIDs;

/**
 * @author kirilim
 */
public class FeedManager extends ManagerBase {

	private static final Logger log = Logger.getLogger(FeedManager.class);
	protected static final int ONE_DAY_IN_SEC = 86400;

	private static String getDateDDMMYYYY(Date timeInsert) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(timeInsert);
		String timeInserStr = String.valueOf(calendar.get(Calendar.DAY_OF_MONTH)) + String.valueOf(calendar.get(Calendar.MONTH)) + String.valueOf(calendar.get(Calendar.YEAR));
		return timeInserStr;
	}


	private static String getDateDDMMYYYYHH(Date timeInsert) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(timeInsert);
		String timeInserStr = String.valueOf(calendar.get(Calendar.DAY_OF_MONTH)) + String.valueOf(calendar.get(Calendar.MONTH)) + String.valueOf(calendar.get(Calendar.YEAR)
													+ String.valueOf(calendar.get(Calendar.HOUR)));
		return timeInserStr;
	}

	public static List<FeedMessage> getFeed(long userId, QueueTypeEnum type, Date timeCreated, int limit) {
		Session session = getSession();
		long beginTime = 0;
		long endTime = 0;
		if (log.isTraceEnabled()) {
			beginTime = System.currentTimeMillis();
		}
		List<FeedMessage> feed = FeedDAO.getFeed(session, userId, type, timeCreated, limit);
		if (log.isTraceEnabled()) {
			endTime = System.currentTimeMillis();
			log.trace("TIME: get feed " + (endTime - beginTime) + "ms");
		}
		return feed;
	}

	public static void insertFeedMessage(ArrayList<FeedMessage> feedList) {
		SimpleDateFormat formatterHour = new SimpleDateFormat("dd/MM/yyyy HH");
		SimpleDateFormat formatterDay = new SimpleDateFormat("dd/MM/yyyy");
		Date now = new Date();
		int br = 0;
		try {
			Date nowH = formatterHour.parse(formatterHour.format(now));
			Date nowD = formatterDay.parse(formatterDay.format(now));

			for (FeedMessage f : feedList) {
				insertFeedHotLastMsgsDateTime(br, f);
				insertFeedMessage(f);

				insertTrendiestTrendsAssets(nowH, f);
				log.debug("Insert into postedUser with:" + getDateDDMMYYYY(nowD) + ". NOW is:" + now + " with feed:" + f);
				insertPostedUsers(nowD, f);
				br++;
			}

		} catch (ParseException e) {
			e.printStackTrace();
		}
	}

	private static void insertPostedUsers(Date nowH, FeedMessage f) {
		if(f.getQueueType() == QueueTypeEnum.SYSTEM.getId()
				&& (f.getMsgType() == UpdateTypeEnum.SYS_24H_MOST_COPIED.getId()
					|| f.getMsgType() == UpdateTypeEnum.SYS_8H_HIGHEST_SR.getId()
						|| f.getMsgType() == UpdateTypeEnum.SYS_8H_MOST_PROFITABLE.getId())){
			insertFeedPostedUsers(nowH, new Long(f.getProperties().get(FeedMessage.PROPERTY_KEY_USER_ID)), f.getMsgType());
		}
	}

	private static void insertFeedHotLastMsgsDateTime(int br, FeedMessage f) {
		if(br == 0 && f.getQueueType() == QueueTypeEnum.HOT.getId()){
			insertFeedLastInsertMsgs(f.getQueueType(), f.getMsgType());
		}
	}

	private static void insertTrendiestTrendsAssets(Date nowH, FeedMessage f) {
		if(f.getQueueType() == QueueTypeEnum.SYSTEM.getId() && f.getMsgType() == UpdateTypeEnum.SYS_1H_TRENDIEST.getId()){
			insertFeedPostedAsset(nowH, new Integer(f.getProperties().get(FeedMessage.PROPERTY_KEY_MARKET_ID)));
		}
	}

	public static void insertFeedMessage(FeedMessage f, QueueTypeEnum queueType) {
		Session session = getSession();
		long beginTime = 0;
		int ttl = new Integer(ConfigurationCopyopManager.getCopyopPropertiesValue("FEED_TABLE_TTL", "0"));

		if (log.isTraceEnabled()) {
			beginTime = System.currentTimeMillis();
		}

		UUID insertUUID = UUIDs.timeBased();
		log.info("Inserting message [" + f.getTimeCreatedUUID() + "] with new UUID [" + insertUUID + "]");

		if(ttl > 0){
			FeedDAO.insertFeedMessage(session, f, queueType, ttl * ONE_DAY_IN_SEC, insertUUID);
		} else {
			FeedDAO.insertFeedMessage(session, f, queueType, insertUUID);
		}

		if (log.isTraceEnabled()) {
			log.trace("TIME: insert feed message " + (System.currentTimeMillis() - beginTime) + "ms");
		}
	}

	public static void insertFeedMessage(FeedMessage f) {
		insertFeedMessage(f, null);
	}

	public static void insertFeedPostedAsset(Date timeInsert, int marketId) {
		Session session = getSession();
		log.debug("Insert feed Posted Asset for:" + timeInsert + "and DDMMYYYYHH:" + getDateDDMMYYYYHH(timeInsert));
		FeedDAO.insertFeedPostedAsset(session, getDateDDMMYYYYHH(timeInsert), marketId);
	}

	public static HashSet<Integer> getFeedPostedAsset(Date timeInsert) {
		Session session = getSession();
		log.debug("GET feed Posted Asset for:" + timeInsert + "and DDMMYYYYHH:" + getDateDDMMYYYYHH(timeInsert));
		return FeedDAO.getFeedPostedAsset(session, getDateDDMMYYYYHH(timeInsert));
	}

	public static void insertFeedLastInsertMsgs(int queueType, int msgType) {
		int ttl = new Integer(ConfigurationCopyopManager.getCopyopPropertiesValue("FEED_TABLE_TTL", "0"));
		Session session = getSession();
		if(ttl > 0){
			FeedDAO.insertFeedLastInsertMsgs(session, queueType, msgType, ttl*ONE_DAY_IN_SEC);
		} else {
			FeedDAO.insertFeedLastInsertMsgs(session, queueType, msgType);
		}
	}

	public static void insertFeedPostedUsers(Date timeInsert, long userId, int msgTyp) {
		String timeInserStr = getDateDDMMYYYY(timeInsert);
		Session session = getSession();
		FeedDAO.insertFeedPostedUsers(session, timeInserStr, userId, msgTyp);
	}

	public static List<FeedMessage> getLastHotTableData(UpdateTypeEnum msgType) {
		Session session = getSession();
		long beginTime = 0;
		long endTime = 0;
		if (log.isTraceEnabled()) {
			beginTime = System.currentTimeMillis();
		}
		List<FeedMessage> fml = FeedDAO.getLastHotTableData(session, msgType);
		if (log.isTraceEnabled()) {
			endTime = System.currentTimeMillis();
			log.trace("TIME: insert feed message " + (endTime - beginTime) + "ms");
		}
		return fml;
	}

	public static boolean isHaveHotBestRecodrs(UpdateTypeEnum msgType){
		Session session = getSession();
		List<FeedMessage> fml = FeedDAO.getLastHotTableData(session, msgType);

		if(fml.size() == 0){
			return false;
		}
		return true;
	}

	public static List<Long> getProfileIdsFromFeedMessage(List<FeedMessage> feed) {
		List<Long> userIds = new ArrayList<Long>();
		for (FeedMessage feedMessage : feed) {
			Map<String, String> properties = feedMessage.getProperties();
			String userIdStr = properties.get(FeedMessage.PROPERTY_KEY_USER_ID);
			if (userIdStr != null) {
				try {
					userIds.add(new Long(userIdStr));
				} catch (NumberFormatException nfe) {
					log.warn("Unable to parse to long string = " + userIdStr, nfe);
				}
			}
		}
		return userIds;
	}

	public static Map<Long, Integer> getFeedPostedUsersMsgNow() {

		SimpleDateFormat formatterDay = new SimpleDateFormat("dd/MM/yyyy");
		Date now = new Date();
		Date nowD = null;
		try {
			nowD = formatterDay.parse(formatterDay.format(now));
		} catch (ParseException e) {
			log.error("When format DateTime", e);
		}
		return getFeedPostedUsersMsg(nowD);
	}

	public static Map<Long, Integer> getFeedPostedUsersMsg(Date timeInsert) {
		Session session = getSession();
		return FeedDAO.getFeedPostedUsersMsg(session,  getDateDDMMYYYY(timeInsert));
	}
}