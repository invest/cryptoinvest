/**
 *
 */
package com.copyop.common.managers;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.Writer;
import com.anyoption.common.managers.UsersManagerBase;
import com.anyoption.common.util.CommonUtil;
import com.anyoption.common.util.ConstantsBase;
import com.copyop.common.beans.AppoxeeResponse;
import com.copyop.common.beans.AppoxeeUserSegment;
import com.copyop.common.dao.AppoxeeDAO;
import com.datastax.driver.core.Session;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/**
 * @author pavelhe
 *
 */
public class AppoxeeManager extends ManagerBase {

	private static final Logger log = Logger.getLogger(AppoxeeManager.class);

	private static Gson gson = new GsonBuilder().serializeNulls().disableHtmlEscaping().create();

	public static void createSegment(long writerId, String deviceId, long userId) {
		if (Writer.WRITER_ID_COPYOP_MOBILE == (int) writerId) {
			String deviceOS = UsersManagerBase.getMobileDeviceOS(deviceId);
			if (ConstantsBase.MOBILE_DES_NEW_ANDROID_APP.equals(deviceOS)) {
				createSegment(userId, ConfigurationCopyopManager.getCopyopPropertiesValue(
																				"APPOXEE_APP_ID_ANDROID", "104270"));
			} else if (ConstantsBase.MOBILE_DES_NEW_IOS_APP.equals(deviceOS)) {
				createSegment(userId, ConfigurationCopyopManager.getCopyopPropertiesValue(
																				"APPOXEE_APP_ID_IOS", "104258"));
			} else {
				log.error("Unknown OS for device with ID: " + deviceId);
			}
		} else {
			log.trace("Writer [" + writerId + "] is not copyop mobile [" + Writer.WRITER_ID_COPYOP_MOBILE
																			+ "]. Will not create appoxee segment");
		}
	}

	private static void createSegment(long userId, String applicationId) {
		try {
			long lApplicationId = Long.valueOf(applicationId);
			Session session = getSession();
			Long segmentId = AppoxeeDAO.getUserSegment(session, userId, lApplicationId);
			boolean userTemplateAPI = Boolean.parseBoolean(ConfigurationCopyopManager.getCopyopPropertiesValue(
																							"APPOXEE_USE_TEMPLATE_API",
																							"true"));
			if (segmentId == null) {
				log.info("Segment for user [" + userId + "] application [" + applicationId + "] does not exist. Creating one");
				/*
				 * Using the template API does not require segment creation. In this case we use table
				 * appoxee_user_segments only for marking if the user has Android or iOS device (or both).
				 */
				segmentId = (userTemplateAPI ? -1L : createAppoxeeSegment(userId, lApplicationId));
				if (segmentId != null) {
					log.info("Inserting segment [" + segmentId + "]");
					AppoxeeDAO.insertUserSegment(session, userId, lApplicationId, segmentId);
				}
			} else if (segmentId == -1L && !userTemplateAPI) {
				segmentId = createAppoxeeSegment(userId, lApplicationId);
				if (segmentId != null) {
					log.info("Updating segment [" + segmentId + "]");
					AppoxeeDAO.updateUserSegmentIf(session, userId, lApplicationId, segmentId, -1L);
				}
			} else {
				log.trace("Segment [" + segmentId + "] already exists for user [" + userId + "] application [" + applicationId + "]");
			}
		} catch (Exception e) {
			log.error("Unable create segment for user [" + userId + "] application [" + applicationId + "]", e);
		}
	}

	private static Long createAppoxeeSegment(long userId, long lApplicationId) {
		Long segmentId = null;
		String response = sendPostRequest(
									ConfigurationCopyopManager.getCopyopPropertiesValue(
																				"APPOXEE_SEGMENT_URL",
																				"https://saas.appoxee.com/api/segment"),
									gson.toJson(new AppoxeeUserSegment(
														CommonUtil.getProperty("environment.name") + "_" + userId,
														lApplicationId)));
		if (response != null) {
			AppoxeeResponse objResponse = gson.fromJson(response, AppoxeeResponse.class);
			segmentId = objResponse.getPayload().getId();
		}
		return segmentId;
	}

	public static String sendPostRequest(String requestUrl, String requestToSend) {
		String response = null;
		try {
			log.debug("Sending appoxee post request: " + requestToSend);
			URL url = new URL(requestUrl);
			HttpURLConnection con = (HttpURLConnection) url.openConnection();

			// Add request header
			con.setRequestMethod("POST");
			con.setRequestProperty("Content-Type", "application/json");
			con.setRequestProperty("Content-Length", String.valueOf(requestToSend.length()));
			con.setRequestProperty("X-ACCOUNT_CODE", ConfigurationCopyopManager.getCopyopPropertiesValue("APPOXEE_ACCOUNT_CODE", "marketing@anyoption.com"));
			con.setRequestProperty("X-USERNAME", ConfigurationCopyopManager.getCopyopPropertiesValue("APPOXEE_USERNAME", "marketing@anyoption.com"));
			con.setRequestProperty("X-PASSWORD", ConfigurationCopyopManager.getCopyopPropertiesValue("APPOXEE_PASSWORD", "Anyoption2o14"));

			// Send post request
			con.setDoOutput(true);
			OutputStream os = con.getOutputStream();
			os.write(requestToSend.getBytes());
			os.flush();
			os.close();

			if (HttpURLConnection.HTTP_OK == con.getResponseCode()) {
				log.info("Request successful");
			} else {
				log.error("Request unsuccessful: " + con.getResponseMessage());
			}

			BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
			String inputLine;
			StringBuilder sb = new StringBuilder();
			while ((inputLine = in.readLine()) != null) {
				sb.append(inputLine);
			}
			in.close();
			response = sb.toString();
			log.info("Appoxee response body: " + response);
		} catch(Exception e) {
			log.error("Exception on appoxee post request", e);
		}
		return response;
	}

}
