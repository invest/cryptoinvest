package com.copyop.common.managers;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;

import com.copyop.common.dao.ProfileDAO;
import com.copyop.common.dao.ProfileLinksDAO;
import com.copyop.common.dto.ProfileLink;
import com.copyop.common.dto.ProfileLinkHistory;
import com.copyop.common.dto.base.CopyConfig;
import com.copyop.common.enums.ProfileLinkChangeReasonEnum;
import com.copyop.common.enums.base.ProfileLinkTypeEnum;
import com.datastax.driver.core.Session;

/**
 * @author kirilim
 */
public class ProfileLinksManager extends ManagerBase {

	private static final Logger log = Logger.getLogger(ProfileLinksManager.class);
	private static final int HOW_MANY_TIME_TRY_TO_LOCK_PROFILE = 4;


	public static List<ProfileLink> getProfileLinksByType(long userId, ProfileLinkTypeEnum type) {
		Session session = getSession();
		long beginTime = 0;
		long endTime = 0;
		if (log.isTraceEnabled()) {
			beginTime = System.currentTimeMillis();
		}
		List<ProfileLink> pl = ProfileLinksDAO.getProfileLinksByType(session, userId, type);
		if (log.isTraceEnabled()) {
			endTime = System.currentTimeMillis();
			log.trace("TIME: get profile links by type " + (endTime - beginTime) + "ms");
		}
		return pl;
	}

	public static List<Long> getUserIdLinksByType(long userId, ProfileLinkTypeEnum type) {
		Session session = getSession();
		long beginTime = 0;
		long endTime = 0;
		if (log.isTraceEnabled()) {
			beginTime = System.currentTimeMillis();
		}
		List<Long> userIds = ProfileLinksDAO.getUserIdLinksByType(session, userId, type);
		if (log.isTraceEnabled()) {
			endTime = System.currentTimeMillis();
			log.trace("TIME: get profile links by type " + (endTime - beginTime) + "ms");
		}
		return userIds;
	}

	public static List<Long> getProfileLinkIdsByTypes(long profileUserId, List<ProfileLinkTypeEnum> types) {
		Session session = getSession();
		long beginTime = 0;
		long endTime = 0;
		if (log.isTraceEnabled()) {
			beginTime = System.currentTimeMillis();
		}
		List<Integer> typeIds = new ArrayList<Integer>();
		for (ProfileLinkTypeEnum type : types) {
			typeIds.add(type.getCode());
		}
		List<Long> ids = ProfileLinksDAO.getProfileLinkIdsByTypes(session, profileUserId, typeIds);
		if (log.isTraceEnabled()) {
			endTime = System.currentTimeMillis();
			log.trace("TIME: get profile link ids by types " + (endTime - beginTime) + "ms");
		}
		return ids;
	}

	public static ArrayList<ProfileLink> getProfileLinks(long userId) {
		Session session = getSession();
		long beginTime = 0;
		long endTime = 0;
		if (log.isTraceEnabled()) {
			beginTime = System.currentTimeMillis();
		}
		ArrayList<ProfileLink> pll = ProfileLinksDAO.getProfileLinks(session, userId);
		if (log.isTraceEnabled()) {
			endTime = System.currentTimeMillis();
			log.trace("TIME: get profile links " + (endTime - beginTime) + "ms");
		}
		return pll;
	}

	public static List<ProfileLink> getProfileLinks(long userId, long destUserId, List<ProfileLinkTypeEnum> types) {
		long beginTime = 0;
		long endTime = 0;
		if (log.isTraceEnabled()) {
			beginTime = System.currentTimeMillis();
		}
		Session session = getSession();
		List<ProfileLink> pll = ProfileLinksDAO.getProfileLinks(session, userId, destUserId, types);
		if (log.isTraceEnabled()) {
			endTime = System.currentTimeMillis();
			log.trace("TIME: insert profile link " + (endTime - beginTime) + "ms");
		}
		return pll;
	}

	public static void deleteProfileLink(long userId, long destUserId, ProfileLinkTypeEnum linkType) {
		Session session = getSession();
		long beginTime = 0;
		long endTime = 0;
		if (log.isTraceEnabled()) {
			beginTime = System.currentTimeMillis();
		}
		ProfileLinksDAO.deleteProfileLink(session, userId, destUserId, linkType);
		if (log.isTraceEnabled()) {
			endTime = System.currentTimeMillis();
			log.trace("TIME: delete profile link " + (endTime - beginTime) + "ms");
		}
	}
	
	public static void deleteAllProfileLinks(long userId) {
		Session session = getSession();
		List<ProfileLink> copyList		= ProfileLinksDAO.getProfileLinksByType(session, userId, ProfileLinkTypeEnum.COPY);
		for(ProfileLink copy:copyList) {
			deleteCopyProfileLinkBatch(copy.getUserId(), copy.getDestUserId(), 
											ProfileDAO.isTestProfile(session, copy.getUserId()), 
											ProfileDAO.isTestProfile(session, copy.getDestUserId()));
		}
		List<ProfileLink> copiedList	= ProfileLinksDAO.getProfileLinksByType(session, userId, ProfileLinkTypeEnum.COPIED_BY);
		for(ProfileLink copied:copiedList) {
			deleteCopyProfileLinkBatch(copied.getDestUserId(), copied.getUserId(), 
											ProfileDAO.isTestProfile(session, copied.getDestUserId()),
											ProfileDAO.isTestProfile(session, copied.getUserId()));
		}
		List<ProfileLink> watchList		= ProfileLinksDAO.getProfileLinksByType(session, userId, ProfileLinkTypeEnum.WATCH);
		for(ProfileLink watch:watchList) {
			deleteWatchProfileLink(watch.getUserId(), watch.getDestUserId(),
										watch.isWatchingPushNotification(), ProfileLinkChangeReasonEnum.STOP_WATCH,
										ProfileDAO.isTestProfile(session, watch.getUserId()),
										ProfileDAO.isTestProfile(session, watch.getDestUserId()));
		}
		List<ProfileLink> watchedList	= ProfileLinksDAO.getProfileLinksByType(session, userId, ProfileLinkTypeEnum.WATCHED_BY);
		for(ProfileLink watched:watchedList) {
			deleteWatchProfileLink( watched.getDestUserId(), watched.getUserId(),
										watched.isWatchingPushNotification(), ProfileLinkChangeReasonEnum.STOP_WATCH,
										ProfileDAO.isTestProfile(session, watched.getDestUserId()),
										ProfileDAO.isTestProfile(session, watched.getUserId()));
		}
		
	}
	
	public static boolean isWithMaxCopiers(long userId){
		long copiersCount = ProfileCountersManager.getProfileCopiers(userId);
		int maxCopiersPerAccount = Integer.valueOf(ConfigurationCopyopManager.getCopyopPropertiesValue("MAX_COPIERS_PER_ACCOUNT", "" + 100));
		if(copiersCount >= maxCopiersPerAccount){
			log.debug("UserId:" + userId +" have max copiers:" + copiersCount);
			return true;
		} else{
			return false;
		}
	}

	public static boolean insertCopyProfileLink(long userId, CopyConfig config, boolean isInsert, boolean isChangedCountDown, boolean testUser, boolean testProfile, long writerId) {				
		int br = 0;
		boolean isLockedUser = true;
		while (br < HOW_MANY_TIME_TRY_TO_LOCK_PROFILE){
			if(!ProfileManager.lockProfile(config.getDestUserId())) {
				isLockedUser = false;
				log.debug("Can't lock profile : " + config.getDestUserId() + ". Sleep and try again." );
				br ++;
				try {
					Thread.sleep(500);
				} catch (InterruptedException e) {
					log.error("Can't sleep thread", e);
					return false;
				}				
			} else {
				isLockedUser = true;
				br = HOW_MANY_TIME_TRY_TO_LOCK_PROFILE;
				log.debug("Lock profile : " + config.getDestUserId() + " for COPY" );
			}
		}
		
		if(!isLockedUser){
			log.debug("Can't lock profile : " + config.getDestUserId() + ". Return FALSE!" );
			return false;
		}
		
		try {	//When have new records(isInsert) or user change copy  inv. count(isChangedCountDown) then reachedCount = 0 and count = request.count
				// else not change(updateCopyProfileLinkWithoutCountBatch) the inv. count
			if(isInsert){
				insertCopyProfileLinkBatch(userId, config.getDestUserId(), config.getAmount(), config.getCount(), config.getAssets(), writerId);
				CopyHistoryManager.insertCopyHistory(userId, config.getDestUserId());
			} else{
				if(isChangedCountDown){
					insertCopyProfileLinkBatch(userId, config.getDestUserId(), config.getAmount(), config.getCount(), config.getAssets(), writerId);
				} else{
					updateCopyProfileLinkWithoutCountBatch(userId, config.getDestUserId(), config.getAmount(), config.getAssets(), writerId);
				}
			}
			if (isInsert
					&& ((testUser && testProfile)
							|| (!testUser && !testProfile))) {
				ProfileCountersManager.increaseMutualCountsCopy(userId, config.getDestUserId());
			}

			// save profile_link history
			ProfileLink link = ProfileLinksManager.getProfileLink(userId, config.getDestUserId(), ProfileLinkTypeEnum.COPY);				
			ProfileLink revLink = ProfileLinksManager.getProfileLink(config.getDestUserId(),userId, ProfileLinkTypeEnum.COPIED_BY);
			
			insertProfileLinkHistory(link, ProfileLinkChangeReasonEnum.START_COPY);
			insertProfileLinkHistory(revLink, ProfileLinkChangeReasonEnum.START_COPY);
			return true;
		} finally {
			ProfileManager.unlockProfile(config.getDestUserId());
			log.debug("Unlock profile : " + config.getDestUserId() + " after COPY" );
		}
	}
	
	private static void insertCopyProfileLinkBatch(long userId, long destUserId, int amount, int maxInv, Set<Long> assets, long writerId) {
		Session session = getSession();
		ProfileLinksDAO.insertCopyProfileLinkBatch(session, userId, destUserId, amount, maxInv, assets, writerId);
	}
	
	private static void updateCopyProfileLinkWithoutCountBatch(long userId, long destUserId, int amount, Set<Long> assets, long writerId) {
		Session session = getSession();
		ProfileLinksDAO.updateCopyProfileLinkWithoutCountBatch(session, userId, destUserId, amount, assets, writerId);
	}

	public static void deleteCopyProfileLink(long userId, CopyConfig config, boolean testUser, boolean testProfile) {
		deleteCopyProfileLinkBatch(userId, config.getDestUserId(), testUser, testProfile);
	}
	
	public static void deleteCopyProfileLinkBatch(long userId, long destUserId, boolean testUser, boolean testProfile) {
		Session session = getSession();
		ProfileLink link = ProfileLinksManager.getProfileLink(userId, destUserId, ProfileLinkTypeEnum.COPY);
		ProfileLink revlink = ProfileLinksManager.getProfileLink(destUserId, userId, ProfileLinkTypeEnum.COPIED_BY);
		
		ProfileLinksDAO.deleteCopyProfileLinkBatch(session, userId, destUserId);
		if ((testUser && testProfile) || (!testUser && !testProfile)) {
			ProfileCountersManager.decreaseMutualCountsCopy(userId, destUserId);
			CopiedManager.updateUncopiedCounters(destUserId);
		}
		
		// save profile_link history
		insertProfileLinkHistory(link, ProfileLinkChangeReasonEnum.STOP_COPY);
		insertProfileLinkHistory(revlink, ProfileLinkChangeReasonEnum.STOP_COPY);
	}

	public static void insertProfileLinkHistory(ProfileLink link, ProfileLinkChangeReasonEnum reason) {
		Session session = getSession();
		ProfileLinksDAO.insertProfileLinkHistory(session, link, reason);
	}

	public static Map<Long, Boolean> getWatchersIds(long userId) {
		Session session = getSession();
		return ProfileLinksDAO.getWatchersIds(session, userId);
	}

	public static List<Long> getListWatchersIds(long userId) {
		Session session = getSession();
		return ProfileLinksDAO.getListWatchersIds(session, userId);
	}
	
	public static List<Long> getCopiersIds(long userId) {
		Session session = getSession();
		return ProfileLinksDAO.getCopiersIds(session, userId);
	}

	public static List<Long> getCopyingIds(long userId) {
		Session session = getSession();
		long beginTime = 0;
		long endTime = 0;
		if (log.isTraceEnabled()) {
			beginTime = System.currentTimeMillis();
		}
		List<Long> ids = ProfileLinksDAO.getCopyingIds(session, userId);
		if (log.isTraceEnabled()) {
			endTime = System.currentTimeMillis();
			log.trace("TIME: get copying ids " + (endTime - beginTime) + "ms");
		}
		return ids;
	}

	public static ProfileLink getProfileLink(long userId, long destUserId, ProfileLinkTypeEnum type) {
		Session session = getSession();
		return ProfileLinksDAO.getProfileLink(session, userId, destUserId, type);
	}

	public static void updateReachedCound(ProfileLink link) {
		Session session = getSession();
		ProfileLinksDAO.updateReachedCound(session, link);
	}
	
	public static void insertWatchProfileLink(long userId, long destUserId, boolean testUser, boolean testProfile) {
		insertUpdateWatchProfileLink(userId, destUserId, false, true, testUser, testProfile);
	}

	public static void insertUpdateWatchProfileLink(long userId, long destUserId, boolean isWatchingPushNotification, boolean isInsert,
													boolean testUser, boolean testProfile) {
		insertWatchProfileLinkBatch(userId, destUserId, isWatchingPushNotification);
		if (isInsert
				&& ((testUser && testProfile)
						|| (!testUser && !testProfile))) {
			ProfileCountersManager.increaseMutualCountsWatch(userId, destUserId);
		}
		// History
		insertProfileLinkHistory(	new ProfileLink(userId, destUserId, ProfileLinkTypeEnum.WATCH, isWatchingPushNotification),
									ProfileLinkChangeReasonEnum.START_WATCH);
		insertProfileLinkHistory(	new ProfileLink(destUserId, userId, ProfileLinkTypeEnum.WATCHED_BY, isWatchingPushNotification),
									ProfileLinkChangeReasonEnum.START_WATCH);
	}

	public static void deleteWatchProfileLink(long userId, long destUserId, boolean isWatchingPushNotification,
												ProfileLinkChangeReasonEnum reason, boolean testUser, boolean testProfile) {
		ProfileLinksManager.deleteWatchProfileLinkBatch(userId, destUserId);
		if ((testUser && testProfile) || (!testUser && !testProfile)) {
			ProfileCountersManager.decreaseMutualWatchers(userId, destUserId);
		}
		// History
		insertProfileLinkHistory(new ProfileLink(userId, destUserId, ProfileLinkTypeEnum.WATCH, isWatchingPushNotification), reason);
		insertProfileLinkHistory(new ProfileLink(destUserId, userId, ProfileLinkTypeEnum.WATCHED_BY, isWatchingPushNotification), reason);
	}
	
	private static void deleteWatchProfileLinkBatch(long userId, long destUserId) {
		Session session = getSession();
		ProfileLinksDAO.deleteWatchProfileLinkBatch(session, userId, destUserId);
	}

	private static void insertWatchProfileLinkBatch(long userId, long destUserId, boolean isWatchingPushNotification) {
		Session session = getSession();
		ProfileLinksDAO.insertWatchProfileLinkBatch(session, userId, destUserId, isWatchingPushNotification);
	}

	public static ArrayList<ProfileLinkHistory> getProfileLinksHistory(long userId, ProfileLinkTypeEnum linkType) {
		Session session = getSession();
		return ProfileLinksDAO.getProfileLinksHistory(session, userId, ProfileLinkTypeEnum.COPY);
	}

	public static void updateReachedCound(int countReached, long userId, long destUserId, ProfileLinkTypeEnum type) {
		Session session = getSession();
		ProfileLinksDAO.updateReachedCound(session, countReached, userId, destUserId, type);
	}
}