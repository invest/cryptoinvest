package com.copyop.common.managers;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.time.StopWatch;
import org.apache.log4j.Logger;

import com.copyop.common.dao.ProfileCountersDAO;
import com.copyop.common.dao.ProfileDAO;
import com.copyop.common.dto.ProfileRelations;
import com.copyop.common.dto.base.FeedMessage;
import com.copyop.common.dto.base.Profile;
import com.datastax.driver.core.Session;

public class ProfileCountersManager extends ManagerBase {

	private static final Logger log = Logger.getLogger(ProfileCountersManager.class);
	private final static String traceTime = "Time in ms: ";	

	public static long getProfileCopiers(long userId) {
		StopWatch stopWatch = new StopWatch();
		stopWatch.start();
		Session session = getSession();
		long result = ProfileCountersDAO.getProfileCopiers(session, userId);
		stopWatch.stop();
		log.trace(traceTime + stopWatch.getTime());
		return result;
	}

	public static long getProfileCopying(long userId) {
		Session session = getSession();
		long result = ProfileCountersDAO.getProfileCopying(session, userId);
		return result;
	}

	public static long getProfileCoinsBalance(long userId) {
		Session session = getSession();
		long result = ProfileCountersDAO.getProfileCoinsBalance(session, userId);
		return result;
	}

	public static void loadAllProfileRelations(long userId, FeedMessage message) {
		loadAllProfileRelations(userId, message.getProperties());
	}

	public static void loadAllProfileRelations(long userId, Map<String, String> props) {
		ProfileRelations pr = getAllProfileRelations(userId);
		props.put(FeedMessage.PROPERTY_KEY_COPIERS, String.valueOf(pr.getCopiers()));
		props.put(FeedMessage.PROPERTY_KEY_COPYING, String.valueOf(pr.getCopying()));
		props.put(FeedMessage.PROPERTY_KEY_WATCHERS, String.valueOf(pr.getWatchers()));
		props.put(FeedMessage.PROPERTY_KEY_WATCHING, String.valueOf(pr.getWatching()));
	}

	public static void loadAllProfileRelations(Profile profile) {
		ProfileRelations pr = getAllProfileRelations(profile.getUserId());
		profile.setCopiers(pr.getCopiers());
		profile.setCopying(pr.getCopying());
		profile.setWatchers(pr.getWatchers());
		profile.setWatching(pr.getWatching());
		profile.setShare(pr.getShare());
		profile.setRate(pr.getRate());
		profile.setCopiedInvestments(pr.getCopiedInvestments());
	}

	private static ProfileRelations getAllProfileRelations(long userId) {
		StopWatch stopWatch = new StopWatch();
		stopWatch.start();
		Session session = getSession();
		ProfileRelations pr = new ProfileRelations(userId);
		ProfileCountersDAO.loadAllProfileRelations(session, userId, pr);
		stopWatch.stop();
		if (log.isTraceEnabled()) {
			log.trace(traceTime + stopWatch.getTime());
		}
		return pr;
	}
	
	/*
	*	insert profile coins history after this method
	*/ 
	public static void increaseProfileCoinsBalance(long userId, long amount) {
		Session session = getSession();
		ProfileCountersDAO.updateProfileCoinsBalance(session, userId, amount);
	}

	public static long resetProfileCoinsBalance(long userId) {
		Session session = getSession();
		long amount = ProfileCountersDAO.getProfileCoinsBalance(session, userId);
		ProfileCountersDAO.updateProfileCoinsBalance(session, userId, -amount);
		ProfileDAO.resetTimeConvertCoins(session, userId);
		return amount;
	}

	public static void increaseMutualCountsCopy(long userId, long destUserId) {
		StopWatch stopWatch = new StopWatch();
		stopWatch.start();
		Session session = getSession();
		ProfileCountersDAO.increaseMutualCountsCopy(session, userId, destUserId);
		stopWatch.stop();
		log.trace(traceTime + stopWatch.getTime());
	}

	public static void increaseMutualCountsWatch(long userId, long destUserId) {
		StopWatch stopWatch = new StopWatch();
		stopWatch.start();
		Session session = getSession();
		ProfileCountersDAO.increaseMutualCountsWatch(session, userId, destUserId);
		stopWatch.stop();
		log.trace(traceTime + stopWatch.getTime());
	}

	public static void decreaseMutualCountsCopy(long userId, long destUserId) {
		Session session = getSession();
		ProfileCountersDAO.decreaseMutualCountsCopy(session, userId, destUserId);
	}

	public static void decreaseMutualWatchers(long userId, long destUserId) {
		Session session = getSession();
		ProfileCountersDAO.decreaseMutualWatchers(session, userId, destUserId);
	}

	public static void setProfileCopiersWatchers(List<Profile> profiles, List<Long> userIds) {
		Session session = getSession();
		HashMap<Long, ProfileRelations> hm = ProfileCountersDAO.getCopiersWathcerProfileRelations(session, userIds);
		for(Profile p : profiles){
			long copiers = 0l;
			long watchers = 0l;

			if(hm.containsKey(p.getUserId())){
				copiers = hm.get(p.getUserId()).getCopiers();
				watchers = hm.get(p.getUserId()).getWatchers();
			}
			p.setCopiers(copiers);
			p.setWatchers(watchers);
		}
	}

	public static void increaseProfileShare(long userId) {
		Session session = getSession();
		ProfileCountersDAO.increaseProfileShare(session, userId);
	}

	public static void increaseProfileRate(long userId) {
		Session session = getSession();
		ProfileCountersDAO.increaseProfileRate(session, userId);
	}
	
	public static void increaseCopiedInvestmntsCounters(long userId) {
		Session session = getSession();
		ProfileCountersDAO.increaseCopiedInvestmntsCounters(session, userId);
	}

	public static long getProfileProfileShare(long userId) {
		Session session = getSession();
		long result = ProfileCountersDAO.getProfileProfileShare(session, userId);
		return result;
	}

	public static long getProfileProfileRate(long userId) {
		Session session = getSession();
		long result = ProfileCountersDAO.getProfileProfileRate(session, userId);
		return result;
	}


	/*
	public static void increaseProfileCopiers(long userId) {
		StopWatch stopWatch = new StopWatch();
		stopWatch.start();
		Session session = getSession();
		ProfileCountersDAO.increaseProfileCopiers(session, userId);
		stopWatch.stop();
		log.trace(traceTime + stopWatch.getTime());
	}

	public static void increaseProfileCopying(long userId) {
		StopWatch stopWatch = new StopWatch();
		stopWatch.start();
		Session session = getSession();
		ProfileCountersDAO.increaseProfileCopying(session, userId);
		stopWatch.stop();
		log.trace(traceTime + stopWatch.getTime());
	}

	public static void increaseProfileWatchers(long userId) {
		StopWatch stopWatch = new StopWatch();
		stopWatch.start();
		Session session = getSession();
		ProfileCountersDAO.increaseProfileWatchers(session, userId);
		stopWatch.stop();
		log.trace(traceTime + stopWatch.getTime());
	}

	public static void increaseProfileWatching(long userId) {
		StopWatch stopWatch = new StopWatch();
		stopWatch.start();
		Session session = getSession();
		ProfileCountersDAO.increaseProfileWatching(session, userId);
		stopWatch.stop();
		log.trace(traceTime + stopWatch.getTime());
	}

	public static void decreaseProfileCopiers(long userId) {
		StopWatch stopWatch = new StopWatch();
		stopWatch.start();
		Session session = getSession();
		ProfileCountersDAO.decreaseProfileCopiers(session, userId);
		stopWatch.stop();
		log.trace(traceTime + stopWatch.getTime());
	}

	public static void decreaseProfileCopying(long userId) {
		StopWatch stopWatch = new StopWatch();
		stopWatch.start();
		Session session = getSession();
		ProfileCountersDAO.decreaseProfileCopying(session, userId);
		stopWatch.stop();
		log.trace(traceTime + stopWatch.getTime());

	}

	public static void decreaseProfileWatchers(long userId) {
		StopWatch stopWatch = new StopWatch();
		stopWatch.start();
		Session session = getSession();
		ProfileCountersDAO.decreaseProfileWatchers(session, userId);
		stopWatch.stop();
		log.trace(traceTime + stopWatch.getTime());
	}

	public static void decreaseProfileWatching(long userId) {
		StopWatch stopWatch = new StopWatch();
		stopWatch.start();
		Session session = getSession();
		ProfileCountersDAO.decreaseProfileWatching(session, userId);
		stopWatch.stop();
		log.trace(traceTime + stopWatch.getTime());
	}*/
}