package com.copyop.common.managers;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import com.copyop.common.dao.FbDAO;
import com.datastax.driver.core.Session;

public class FbManager extends ManagerBase {
	
	private static final Logger log = Logger.getLogger(FbManager.class);
	
	public static final String LAST_FETCHED_FB_ID = "lastFetchedFbIsd";
	
	public static ArrayList<Long> getFbFriendsUserIds(long userId) {
		Session session = getSession();
		List<String> friends = FbDAO.getFbFriends(session, userId);
		return FbDAO.getUserIdsForFbIds(session, friends);
	}
	
	public static void insertFbId(long userId, String fbId) {
		Session session = getSession();
		
		long existUserId = FbDAO.getUserIdForFbId(session, fbId);
		if(existUserId != userId){
			log.debug("Found fbId:" + fbId + " for userId:" + existUserId+ ". Will remove data{FbMap;FbFriends;ProfileFbID} and paste to userId:" + userId);
			FbDAO.deleteFbMap(session, fbId);
			FbDAO.deleteFbFrinds(session, existUserId);
			FbDAO.updateProfileFbId(session, existUserId, null);
			log.debug("Remove data for fbId:" + fbId + " and userId:" + existUserId);
		}
		
		FbDAO.insertFbMap(session, userId, fbId);
		FbDAO.updateProfileFbId(session, userId,  fbId);
	}
	
	public static void insertFbFrinds(long userId, List<String> fbIdFriends) {
		Session session = getSession();
		FbDAO.insertFbFrindsBatch(session, userId, fbIdFriends);
	}
	
	
	public static ArrayList<Long> getFbFriendsUserIds(long userId, HttpServletRequest httpRequest) {
		
		HttpSession httpSession = httpRequest.getSession();		
		String lastFetchFbId = (String) httpSession.getAttribute(FbManager.LAST_FETCHED_FB_ID);

		
		long limitSize = new Long(ConfigurationCopyopManager.getCopyopPropertiesValue("FACEBOOK_FRIEND_FETCH_LIMIT", "10"));
		Session session = getSession();		
		List<String> friends = FbDAO.getFbFriendsLimit(session, userId, lastFetchFbId, limitSize);
		
		if (friends != null && !friends.isEmpty()) {
			lastFetchFbId = friends.get(friends.size()-1);
			httpSession.setAttribute(FbManager.LAST_FETCHED_FB_ID, lastFetchFbId);
			}
		return FbDAO.getUserIdsForFbIds(session, friends);
	}	
}
