package com.copyop.common.managers;

import org.apache.log4j.Logger;

import com.copyop.common.dao.DAOBase;
import com.datastax.driver.core.Cluster;
import com.datastax.driver.core.Metadata;
import com.datastax.driver.core.Session;

/**
 * @author kirilim
 */
public class ManagerBase {

	private static final Logger log = Logger.getLogger(ManagerBase.class);
	private static Cluster cluster;
	private static Session session;
	private static String[] contactPoints;
	private static String keyspace;

	public static void init(String[] contactPoints, String keyspace) {
		// TODO add other configurations
		log.info("Initializing " + ManagerBase.class);
		ManagerBase.contactPoints = contactPoints;
		ManagerBase.keyspace = keyspace;
	}

	private static Cluster getCluster() {
		if (cluster == null || cluster.isClosed()) {
			if (cluster == null) {
				log.info("Cluster is null. Connecting to contact points"); // log the contact points
			} else {
				log.info("Cluster is closed. Connecting to contact points"); // log the contact points
			}
			connectCluster();
		}
		return cluster;
	}
	
	public static Session getSessionOld() {
		if (session == null || session.isClosed()) {
			session = getCluster().connect(keyspace);
			log.info("Session connected");
		}
		return session;
	}

	public static Session getSession() {
		if (session == null || session.isClosed()) {
			if (session == null) {
				log.info("Session is null. Connecting cluster"); // log the contact points
			} else {
				log.info("Session is closed. Connecting cluster"); // log the contact points
			}
			session = getCluster().connect(keyspace); // configure the keyspace
			log.info("Session connected");
		}
		try {
			if (!DAOBase.selectDual(session)) {
				log.info("Connection with server is lost. Going to reset the connection"); // log reset attempt
				closeSession();
				closeCluster();
				DAOBase.clearMap();
				session = getCluster().connect(keyspace); // configure the keyspace
			}
		} catch (Exception e) { 
			log.info("Connection with server is lost. Going to reset the connection"); // log reset attempt
			closeSession();
			closeCluster();
			DAOBase.clearMap();
			session = getCluster().connect(keyspace); // configure the keyspace
			log.error(e.toString());
		}
		return session;
	}

	/**
	 * @return the contactPoints
	 */
	public static String[] getContactPoints() {
		return contactPoints;
	}

	/**
	 * @return the keyspace
	 */
	public static String getKeyspace() {
		return keyspace;
	}

	public static void connectCluster() {
		// TODO add additional configurations
		log.warn("Cluster will be build with default configurations");
		cluster = Cluster.builder().addContactPoints(contactPoints).build();
		Metadata metadata = cluster.getMetadata();
		log.debug("Connected to cluster: " + metadata.getClusterName());
	}

	public static boolean closeSession() {
		if (session.isClosed()) {
			log.warn("Session already closed");
			return false;
		}
		log.info("Initiating session closing");
		session.close();
		log.info("Session closed successfully");
		return true;
	}

	public static boolean closeCluster() {
		if (!session.isClosed()) {
			log.info("Session is not closed. Closing session");
			boolean result = closeSession();
			log.info("Session closing result: " + result);
		}
		if (cluster.isClosed()) {
			log.warn("Cluster already closed");
			return false;
		}
		log.info("Initiating cluster closing");
		cluster.close();
		log.info("Cluster closed successfully");
		return true;
	}
}