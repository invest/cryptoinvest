package com.copyop.common.managers;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;

import com.anyoption.common.managers.BaseBLManager;
import com.copyop.common.dao.RiskAppetiteDAO;

public class RiskAppetiteManager extends BaseBLManager {
	
	public static float calculateUserRiskAppetite(long userId) throws SQLException {
		Connection connection = null;
		try{
			connection = getConnection();
			return RiskAppetiteDAO.calculateUserRiskAppetite(connection, userId);
		} finally {
			closeConnection(connection);
		}
	}
	
	public static ArrayList<String> getUsersForCalculate(int returnRecords) throws SQLException {
		Connection connection = getConnection();
		try {
			return RiskAppetiteDAO.getUsersForCalculate(connection, returnRecords); 
		} finally {
			closeConnection(connection);
		}	
	}
	
	public static void updateRiskAppetite(float riskAppetite, long userId) throws SQLException {
        Connection conn = getConnection();
        try {
        	RiskAppetiteDAO.updateRiskAppetite(conn, riskAppetite, userId);
        } finally {
            closeConnection(conn);
        }
    }
	
	public static void insertRiskAppetite(long userId) throws SQLException {
        Connection conn = getConnection();
        try {
        	RiskAppetiteDAO.insertRiskAppetite(conn, userId);
        } finally {
            closeConnection(conn);
        }
    }
}
