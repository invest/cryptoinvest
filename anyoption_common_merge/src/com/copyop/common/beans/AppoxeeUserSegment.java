/**
 *
 */
package com.copyop.common.beans;

import java.util.Collections;

/**
 * @author pavelhe
 *
 */
public class AppoxeeUserSegment extends AppoxeeSegment {
	/**
	 *
	 */
	private static final long serialVersionUID = 1L;

	private static final String SEGMENT_NAME_PREFIX = "user_segment_";

	public AppoxeeUserSegment(String userAlias, Long applicationId) {
		super(SEGMENT_NAME_PREFIX + userAlias,
				"User specific segment",
				applicationId,
				Collections.singletonList(new AppoxeeAliasRule(userAlias)));
	}

	public AppoxeeUserSegment(String userAlias) {
		this(userAlias, null);
	}

}
