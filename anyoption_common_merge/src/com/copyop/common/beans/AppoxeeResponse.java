/**
 *
 */
package com.copyop.common.beans;

import java.io.Serializable;

/**
 * @author pavelhe
 *
 */
public class AppoxeeResponse implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;

	protected AppoxeeResponsePayload payload;

	/**
	 * @return the payload
	 */
	public AppoxeeResponsePayload getPayload() {
		return payload;
	}

	/**
	 * @param payload the payload to set
	 */
	public void setPayload(AppoxeeResponsePayload payload) {
		this.payload = payload;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("AppoxeeResponse [payload=").append(payload).append("]");
		return builder.toString();
	}

}
