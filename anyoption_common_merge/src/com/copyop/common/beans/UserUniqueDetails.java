package com.copyop.common.beans;

/**
 * @author liors
 *
 */
public class UserUniqueDetails {
	private long userId;
	private String nickname;
	private boolean frozen;
	private long turnOver;
	private long balance;
	private long copyTradingProfit;//The SUM of the profit\loss generated only out of copied investments.
	private int coinsGenerated; //
	private int currencyId;
	private int skinId;
	
	/**
	 * @return the nickname
	 */
	public String getNickname() {
		return nickname;
	}
	
	/**
	 * @param nickname the nickname to set
	 */
	public void setNickname(String nickname) {
		this.nickname = nickname;
	}
	
	/**
	 * @return the turnOver
	 */
	public long getTurnOver() {
		return turnOver;
	}
	
	/**
	 * @param turnOver the turnOver to set
	 */
	public void setTurnOver(long turnOver) {
		this.turnOver = turnOver;
	}
	
	/**
	 * @return the balance
	 */
	public long getBalance() {
		return balance;
	}
	
	/**
	 * @param balance the balance to set
	 */
	public void setBalance(long balance) {
		this.balance = balance;
	}
	
	/**
	 * @return the frozen
	 */
	public boolean isFrozen() {
		return frozen;
	}
	
	/**
	 * @param frozen the frozen to set
	 */
	public void setFrozen(boolean frozen) {
		this.frozen = frozen;
	}
	
	/**
	 * @return the copyTradingProfit
	 */
	public long getCopyTradingProfit() {
		return copyTradingProfit;
	}

	/**
	 * @param copyTradingProfit the copyTradingProfit to set
	 */
	public void setCopyTradingProfit(long copyTradingProfit) {
		this.copyTradingProfit = copyTradingProfit;
	}

	/**
	 * @return the userId
	 */
	public long getUserId() {
		return userId;
	}

	/**
	 * @param userId the userId to set
	 */
	public void setUserId(long userId) {
		this.userId = userId;
	}

	/**
	 * @return the currencyId
	 */
	public int getCurrencyId() {
		return currencyId;
	}

	/**
	 * @param currencyId the currencyId to set
	 */
	public void setCurrencyId(int currencyId) {
		this.currencyId = currencyId;
	}

	/**
	 * @return the coinsGenerated
	 */
	public int getCoinsGenerated() {
		return coinsGenerated;
	}

	/**
	 * @param coinsGenerated the coinsGenerated to set
	 */
	public void setCoinsGenerated(int coinsGenerated) {
		this.coinsGenerated = coinsGenerated;
	}

	/**
	 * @return the skinId
	 */
	public int getSkinId() {
		return skinId;
	}

	/**
	 * @param skinId the skinId to set
	 */
	public void setSkinId(int skinId) {
		this.skinId = skinId;
	}
}
