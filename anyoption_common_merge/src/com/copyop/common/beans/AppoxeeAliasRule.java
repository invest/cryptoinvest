/**
 *
 */
package com.copyop.common.beans;

import java.util.Collections;

/**
 * @author pavelhe
 *
 */
public class AppoxeeAliasRule extends AppoxeeRule {

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;

	public AppoxeeAliasRule(String userAlias) {
		super("alias", "=", Collections.singletonList(userAlias));
	}

}
