package com.copyop.common.dao;

import org.apache.log4j.Logger;

import com.copyop.common.dto.CoinsBalanceHistory;
import com.datastax.driver.core.BoundStatement;
import com.datastax.driver.core.PreparedStatement;
import com.datastax.driver.core.ResultSet;
import com.datastax.driver.core.Session;

public class CoinsBalanceHistoryDAO extends DAOBase {

	private static final Logger log = Logger.getLogger(CoinsBalanceHistoryDAO.class);
	
	public static void insertCoinsBalanceHistory(Session session, CoinsBalanceHistory coinsBalance) {
		PreparedStatement ps = methodsPreparedStatement.get("insertCoinsBalanceHistory");
		if (ps == null) {
			String cql = 
					"INSERT into " +
					" coins_balance_history  (user_id, time_created, action_type, action_type_be, coins_balance, amount, " +
											 " comment, is_re_granted, writer_id ) " +
					" values" +
											 " (?, now(), ?, ?, ?, ?, " +
											  " ? ,? ,?);"; 

			ps = session.prepare(cql);
			methodsPreparedStatement.put("insertCoinsBalanceHistory", ps); 
		}
		BoundStatement boundStatement = new BoundStatement(ps);
		if (log.isTraceEnabled()) {
			boundStatement.enableTracing();
		}
		ResultSet rs = session.execute(boundStatement.bind(coinsBalance.getUserId(),
														   coinsBalance.getActionType(),
														   coinsBalance.isActionTypeBE(),
														   coinsBalance.getCoinsBalance(),
														   coinsBalance.getAmount(),
														   coinsBalance.getComment(),
														   coinsBalance.isRegranted(),
														   coinsBalance.getWriterId()));
		if (log.isTraceEnabled()) {
			traceLog(rs.getExecutionInfo(), log);
		}
	}
}
