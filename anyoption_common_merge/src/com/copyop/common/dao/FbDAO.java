package com.copyop.common.dao;

import java.util.ArrayList;
import java.util.List;

import com.anyoption.common.util.CommonUtil;
import com.datastax.driver.core.BatchStatement;
import com.datastax.driver.core.BoundStatement;
import com.datastax.driver.core.PreparedStatement;
import com.datastax.driver.core.ResultSet;
import com.datastax.driver.core.Row;
import com.datastax.driver.core.Session;

public class FbDAO extends DAOBase {
	
	/*
	 * get the facebook id for the given user ID
	 */
	public static List<String> getFbFriends(Session session, long userId) {
		PreparedStatement statement = methodsPreparedStatement.get("getFbFriends");
		if(statement == null ){
			String cql = "SELECT fb_id FROM fb_friends WHERE user_id = ?";
			statement = session.prepare(cql);
			methodsPreparedStatement.put("getFbFriends", statement);
		}
		
		ArrayList<String> result = new ArrayList<String>();
		BoundStatement boundStatement = new BoundStatement(statement);
		ResultSet rs = session.execute(boundStatement.bind(userId));
		while(!rs.isExhausted()) {
			Row row = rs.one();
			result.add(row.getString(0));
		}
		return result;
	}
	/*
	 * for each facebook id find the userId
	 */
	public static ArrayList<Long> getUserIdsForFbIds(Session session, List<String> friends) {
		PreparedStatement statement = methodsPreparedStatement.get("getUserIdsForFbIds");
		if(statement == null ){
			String cql = "SELECT user_id FROM fb_user_map WHERE fb_id IN :list";
			statement = session.prepare(cql);
			methodsPreparedStatement.put("getUserIdsForFbIds", statement);
		}
		ArrayList<Long> result = new ArrayList<Long>();
		BoundStatement boundStatement = new BoundStatement(statement);
		boundStatement.setList("list", friends);
		ResultSet rs = session.execute(boundStatement);
		while(!rs.isExhausted()) {
			Row row = rs.one();
			result.add(row.getLong(0));
		}
		return result;
	}
	
	public static Long getUserIdForFbId(Session session, String fbId) {
		PreparedStatement statement = methodsPreparedStatement.get("getUserIdForFbId");
		if(statement == null ){
			String cql = "SELECT user_id FROM fb_user_map WHERE fb_id = ? ";
			statement = session.prepare(cql);
			methodsPreparedStatement.put("getUserIdForFbId", statement);
		}
		Long result = 0l;
		BoundStatement boundStatement = new BoundStatement(statement);
		ResultSet rs = session.execute(boundStatement.bind(fbId));
		while(!rs.isExhausted()) {
			Row row = rs.one();
			result = row.getLong(0);
		}
		return result;
	}
	
	public static void updateProfileFbId(Session session, long userId, String fbId) {
		PreparedStatement ps = methodsPreparedStatement.get("updateProfileFBId");
		if (ps == null) {
			String cql = " UPDATE profiles SET " + 
							" fb_id = ? " +
							" WHERE user_id = ? ";
			ps = session.prepare(cql);
			methodsPreparedStatement.put("updateProfileFBId", ps);
		}
		BoundStatement boundStatement = new BoundStatement(ps);
		boundStatement.bind(fbId, userId);
		session.execute(boundStatement);
	}
	
	public static void insertFbMap(Session session, long userId, String fbId) {
		PreparedStatement ps = methodsPreparedStatement.get("insertFBMap");
		if (ps == null) {
			String cql = " insert into fb_user_map " + 
							" (fb_id, user_id) " +
						 " values " +
							" (?, ?) ";
			ps = session.prepare(cql);
			methodsPreparedStatement.put("insertFBMap", ps);
		}
		BoundStatement boundStatement = new BoundStatement(ps);
		boundStatement.bind(fbId, userId);
		session.execute(boundStatement);
	}
	
	public static void deleteFbMap(Session session, String fbId) {
		PreparedStatement ps = methodsPreparedStatement.get("deleteFbMap");
		if (ps == null) {
			String cql = " delete from fb_user_map " + 
							" where " +
						 " fb_id = ? ";
			ps = session.prepare(cql);
			methodsPreparedStatement.put("deleteFbMap", ps);
		}
		BoundStatement boundStatement = new BoundStatement(ps);
		boundStatement.bind(fbId);
		session.execute(boundStatement);
	}
	
	public static void insertFbFrindsBatch(Session session, long userId, List<String> fbIdFriends) {
		PreparedStatement ps = methodsPreparedStatement.get("insertFbFrinds");
		if (ps == null) {
			String cql = " insert into fb_friends " + 
							" (fb_id, user_id) " +
						 " values " +
							" (?, ?) ";
			ps = session.prepare(cql);
			methodsPreparedStatement.put("insertFbFrinds", ps);
		}
		BatchStatement batch = new BatchStatement();
		for(String fbId : fbIdFriends){
			batch.add(ps.bind(fbId, userId));			
		}
		session.execute(batch);
	}
	
	public static void deleteFbFrinds(Session session, long userId) {
		PreparedStatement ps = methodsPreparedStatement.get("deleteFbFrinds");
		if (ps == null) {
			String cql = " delete from fb_friends " + 
							" where " +
						 " user_id = ? ";
			ps = session.prepare(cql);
			methodsPreparedStatement.put("deleteFbFrinds", ps);
		}
		BoundStatement boundStatement = new BoundStatement(ps);
		boundStatement.bind(userId);
		session.execute(boundStatement);
	}
	
	public static List<String> getFbFriendsLimit(Session session, long userId, String lastFetchFbId, long limitSize) {
		
		String cqlName;
		String cql;
		if(CommonUtil.isParameterEmptyOrNull(lastFetchFbId)){
			cqlName = "getFbFriendsLimit";
			cql =  "SELECT fb_id FROM fb_friends WHERE user_id = ? LIMIT " + limitSize;
		} else {
			cqlName = "getFbFriendsLimitWithLastFB";
			cql =  "SELECT fb_id FROM fb_friends WHERE user_id = ? and fb_id > ? LIMIT " + limitSize;
		}
		
		PreparedStatement statement = methodsPreparedStatement.get(cqlName);
		if(statement == null ){
			statement = session.prepare(cql);
			methodsPreparedStatement.put(cqlName, statement);
		}
		
		ArrayList<String> result = new ArrayList<String>();
		BoundStatement boundStatement = new BoundStatement(statement);
		ResultSet rs;
		if(CommonUtil.isParameterEmptyOrNull(lastFetchFbId)){
			rs = session.execute(boundStatement.bind(userId));
		} else {
			rs = session.execute(boundStatement.bind(userId, lastFetchFbId));
		}
		while(!rs.isExhausted()) {
			Row row = rs.one();
			result.add(row.getString(0));
		}
		return result;
	}
}
