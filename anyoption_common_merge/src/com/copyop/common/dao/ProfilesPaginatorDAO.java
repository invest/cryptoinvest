package com.copyop.common.dao;

import java.util.ArrayList;
import java.util.List;

import com.datastax.driver.core.PreparedStatement;
import com.datastax.driver.core.ResultSet;
import com.datastax.driver.core.Row;
import com.datastax.driver.core.Session;


public class ProfilesPaginatorDAO extends DAOBase{

	String startCQL = "SELECT user_id, token(user_id), user_state FROM profiles limit ?";
	String pageCQL  = "SELECT user_id, token(user_id), user_state FROM profiles WHERE token(user_id) > ? limit ?";
	
	Session session;
	private boolean started = false;
	private boolean hasMore = true;
	private int pageSize = 100;
	private long token;

	public ProfilesPaginatorDAO(Session session, int pageSize){
		this.session = session;
		this.pageSize = pageSize;
	}

	public List<Long> startPage() {
		started = true;
		
		PreparedStatement ps = methodsPreparedStatement.get("ProfilesPaginatorStart");
		if (ps == null) {
			ps = session.prepare(startCQL);
			methodsPreparedStatement.put("ProfilesPaginatorStart", ps);
		}
		ResultSet rs = session.execute(ps.bind(pageSize));
		List<Row> rows =  rs.all();
		if(rows.size() < pageSize) {
			hasMore = false;
		}
		ArrayList<Long> rez = new ArrayList<Long>();
		for(Row row:rows) {
			if(row.getInt(2)==0) {
				rez.add(row.getLong(0));
				token = row.getLong(1);
			}
		}
		return rez;
	}
	
	public List<Long> nextPage() {
		if(!started) {
			throw new IllegalStateException("Pagination not started");
		}
		
		PreparedStatement ps = methodsPreparedStatement.get("ProfilesPaginatorNext");
		if (ps == null) {
			ps = session.prepare(pageCQL);
			methodsPreparedStatement.put("ProfilesPaginatorNext", ps);
		}
		ResultSet rs = session.execute(ps.bind(token, pageSize));
		List<Row> rows =  rs.all();
		if(rows.size() < pageSize) {
			hasMore = false;
		}
		ArrayList<Long> rez = new ArrayList<Long>();
		for(Row row:rows){
			if(row.getInt(2) == 0) { // UserStateEnum = 0 (Non blocked)
				rez.add(row.getLong(0));
				token = row.getLong(1);
			}
		}
		return rez;
	}
	
	public boolean hasMore() {
		return hasMore;
	}
}
