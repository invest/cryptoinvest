package com.copyop.common.dao;

import java.util.HashMap;
import java.util.List;

import org.apache.log4j.Logger;

import com.copyop.common.dto.CopiedInvestment;
import com.datastax.driver.core.BoundStatement;
import com.datastax.driver.core.PreparedStatement;
import com.datastax.driver.core.ResultSet;
import com.datastax.driver.core.Row;
import com.datastax.driver.core.Session;

/**
 * @author liors
 *
 */
public class CopiedInvestmentDAO extends DAOBase {
	private static final Logger log = Logger.getLogger(CopiedInvestmentDAO.class);
	
	/**
	 * @param session
	 * @param userId - user that copy
	 * @param destinationUsersId - userId copy from destinationUsersId
	 * @return - user and his sum profit. 
	 */
	public static HashMap<Long, CopiedInvestment> getCopiedInvestmentSum(Session session, long userId, List<Long> destinationUsersId) {
		//HashMap<Long, Long> usersProfit = new HashMap<Long, Long>();
		HashMap<Long, CopiedInvestment> copiedInvestment = new HashMap<Long, CopiedInvestment>();
		PreparedStatement ps = methodsPreparedStatement.get("getCopiedInvestmentProfitSum");
		if (ps == null) {
			String cql = 
					"SELECT " +
					"	copy_from_user_id" +
					"	,profit" +
					"	,copied_count_success " +
					"FROM " +
					"	copied_investments " +
					"WHERE " +
					"	user_id = ?" +
					"	AND copy_from_user_id IN :list";
					/*"	user_id IN :list" +
					"	AND copy_from_user_id = ?";*/					
			ps = session.prepare(cql);
			methodsPreparedStatement.put("getCopiedInvestmentProfitSum", ps);
		}
				
		BoundStatement boundStatement = new BoundStatement(ps);
		boundStatement.bind(userId);
		boundStatement.setList("list", destinationUsersId);
		ResultSet rs = session.execute(boundStatement);
		while(!rs.isExhausted()) {
			Row row = rs.one();	
			CopiedInvestment sum = new CopiedInvestment();				
			long copyFromUserId = row.getLong("copy_from_user_id");
			long profit = row.getLong("profit");
			int copiedCountSuccess = row.getInt("copied_count_success");
			
			if (copiedInvestment.containsKey(copyFromUserId)) {
				profit += copiedInvestment.get(copyFromUserId).getProfit();
				copiedCountSuccess +=  copiedInvestment.get(copyFromUserId).getCopiedCountSuccess();
			}
			
			sum.setProfit(profit);
			sum.setCopiedCountSuccess(copiedCountSuccess);
			sum.setCopyFromUserId(copyFromUserId);
			
			copiedInvestment.put(sum.getCopyFromUserId(), sum);						
		}
		return copiedInvestment;
	}
}