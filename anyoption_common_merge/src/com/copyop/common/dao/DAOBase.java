package com.copyop.common.dao;

import java.text.SimpleDateFormat;
import java.util.HashMap;

import org.apache.log4j.Logger;

import com.datastax.driver.core.BoundStatement;
import com.datastax.driver.core.ExecutionInfo;
import com.datastax.driver.core.Host;
import com.datastax.driver.core.PreparedStatement;
import com.datastax.driver.core.QueryTrace;
import com.datastax.driver.core.ResultSet;
import com.datastax.driver.core.Session;

/**
 * @author kirilim
 */
public class DAOBase {

	// LWT check success
	protected static final String APPLIED = "[applied]";
	/**
	 * key - method name
	 * value - cql after Prepared Statement
	 */
	protected static HashMap<String, PreparedStatement> methodsPreparedStatement = new HashMap<String, PreparedStatement>();


	protected static void traceLog(ExecutionInfo executionInfo, Logger log) {
		if (log.isTraceEnabled()) {
			try {
				log.trace("Host (queried): " + executionInfo.getQueriedHost().toString());
				for (Host host : executionInfo.getTriedHosts()) {
					log.trace("Host (tried): " + host.toString());
				}
				QueryTrace queryTrace = executionInfo.getQueryTrace();
				log.trace("Trace id: " + queryTrace.getTraceId());
				log.trace("");
				log.trace("EVENT | TIMESTAMP OF THE EVENT | SOURCE | SOURCE ELAPSED TIME (in microseconds)");
				log.trace("-------------------------------------------------------------------------------");
				for (QueryTrace.Event event : queryTrace.getEvents()) {
					log.trace(event.getDescription() + " | " + millis2Date(event.getTimestamp()) + " | " + event.getSource() + " | "
								+ event.getSourceElapsedMicros());
				}
			} catch (Exception e) {
				log.error("Unable to trace log", e);
			}
		}
	}

	private static String millis2Date(long timestamp) {
		SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss.SSS");
		return sdf.format(timestamp);
	}
	
	public static void clearMap() {
		methodsPreparedStatement.clear();
	}
	
	public static boolean selectDual(Session session) {
		boolean returnDual = false;
		PreparedStatement ps = methodsPreparedStatement.get("selectDual");
		if(ps == null) {
			String query = " select * from dual ";
        	ps = session.prepare(query);
			methodsPreparedStatement.put("selectDual", ps);
		}
		BoundStatement boundStatement = new BoundStatement(ps);
		ResultSet rs = session.execute(boundStatement);
		while(!rs.isExhausted()) {
			rs.all();
			returnDual = true;
		}
		return returnDual;
	}
	
}