package com.copyop.common.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.TreeMap;

import org.apache.log4j.Logger;

import com.copyop.common.enums.base.PriorityEnum;
import com.copyop.common.enums.base.QueueTypeEnum;
import com.copyop.common.enums.base.UpdateFamilyEnum;
import com.copyop.common.enums.base.UpdateTypeEnum;

import oracle.jdbc.OracleTypes;

public class ConfigurationCopyopDao extends com.anyoption.common.daos.DAOBase {
    protected static final Logger logger = Logger.getLogger(ConfigurationCopyopDao.class);

	public static void initPriorityEnum(Connection con) throws SQLException {

		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
            String sql = " select * from copyop.cfg_update_priority";

			ps = con.prepareStatement(sql);
			rs = ps.executeQuery();
			while(rs.next()) {				
				
				PriorityEnum.getById(rs.getInt("id")).setPriority(rs.getInt("priority"));
				PriorityEnum.getById(rs.getInt("id")).setMessageTimeLimit(rs.getInt("message_time_limit"));
				PriorityEnum.getById(rs.getInt("id")).setMessageCountLimit(rs.getInt("message_count_limit"));
				PriorityEnum.getById(rs.getInt("id")).setMessageWaitPeriod(rs.getLong("message_wait_period"));
			}			
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
	}
	
	public static void initUpdateTypeEnum(Connection con) throws SQLException {

		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
            String sql = " select * from copyop.cfg_update_msg ";

			ps = con.prepareStatement(sql);
			rs = ps.executeQuery();
			while(rs.next()) {				
				
				UpdateTypeEnum.getById(rs.getInt("id")).setPriority(PriorityEnum.getById(rs.getInt("priority_id")));																									
				UpdateTypeEnum.getById(rs.getInt("id")).setTtlMillis(rs.getLong("ttl"));
				UpdateTypeEnum.getById(rs.getInt("id")).setUpdateFamily(UpdateFamilyEnum.getById(rs.getInt("group_id")));
				UpdateTypeEnum.getById(rs.getInt("id")).setWherePosted(getWherePosted(con, rs.getInt("id")));
			}			
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
	}
	
	public static EnumSet<QueueTypeEnum> getWherePosted(Connection con, int updateTypeEnumId) throws SQLException {

		PreparedStatement ps = null;
		ResultSet rs = null;
		EnumSet<QueueTypeEnum> wherePostedSet = EnumSet.noneOf(QueueTypeEnum.class);
		try {
            String sql = " select * from copyop.cfg_update_queue_types " +
            		  		" where " +
            		  		" is_active =1 " +
            		  		" and update_id = " + updateTypeEnumId;

			ps = con.prepareStatement(sql);
			rs = ps.executeQuery();
			while(rs.next()) {								
				wherePostedSet.add(QueueTypeEnum.getById(rs.getInt("queue_type_id")));
			}			
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		
		return wherePostedSet;
	}
	
	public static HashMap<String, String> getCopyopPropertiesConfig(Connection con)throws SQLException {

		PreparedStatement ps = null;
		ResultSet rs = null;
		HashMap<String, String> cfgHM = new HashMap<String, String>();
		try {
            String sql = " select * from copyop.cfg_properties ";

			ps = con.prepareStatement(sql);
			rs = ps.executeQuery();
			while(rs.next()) {								
				cfgHM.put(rs.getString("key"), rs.getString("value"));
			}			
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		
		return cfgHM;
	}
	
	public static HashMap <Long, TreeMap <Long, Long> > getCopyopCoinStepsPerCurrencyCfg(Connection con) throws SQLException {
	    PreparedStatement ps = null;
	    ResultSet rs = null;
	    HashMap<Long, TreeMap<Long, Long>> cfgHM = new HashMap<Long, TreeMap<Long, Long>>();
        	try {
        	    String sql = " select * from copyop.cfg_coins_config coins order by currency_id, coins";
        
        	    ps = con.prepareStatement(sql);
        	    rs = ps.executeQuery();
        	    long currencyId = -1;
        	    while (rs.next()) {
        		currencyId = rs.getLong("currency_id");
        		if (!cfgHM.containsKey(currencyId)) {
        		    cfgHM.put(currencyId, getCoinsPerInvStepsCfg(con, currencyId));
        		}
        	    }
            	} finally {
        	    closeResultSet(rs);
        	    closeStatement(ps);
        	}
    		return cfgHM;
	}
    
        private static TreeMap<Long, Long> getCoinsPerInvStepsCfg(Connection con, long currencyId) throws SQLException {
            TreeMap<Long, Long> conversionRates = new TreeMap<Long, Long>();
            PreparedStatement ps = null;
            ResultSet rs = null;
            try {
        	String sql = " select * from copyop.cfg_coins_config coins where currency_id = " + currencyId;
        	ps = con.prepareStatement(sql);
        	rs = ps.executeQuery();
        	while (rs.next()) {
        	    conversionRates.put(rs.getLong("up_to_money"), rs.getLong("coins"));
        	}
            } finally {
        	closeResultSet(rs);
        	closeStatement(ps);
            }
            return conversionRates;
        }
	
	public static HashMap<Long, TreeMap<Long, Long>> getCopyopCoinsConversionRatesCfg(Connection con) throws SQLException{
		PreparedStatement ps = null;
		ResultSet rs = null;
		HashMap<Long, TreeMap<Long, Long>> cfgHM = new HashMap<Long, TreeMap<Long, Long>>();
		try {
            String sql = " select * from copyop.cfg_copyop_coins coins order by currency_id, coins";

			ps = con.prepareStatement(sql);
			rs = ps.executeQuery();
			long currencyId = -1;
			while(rs.next()) {	
				currencyId = rs.getLong("currency_id");				
				if(!cfgHM.containsKey(currencyId)){
					cfgHM.put(currencyId, getAmountCoinsCfg(con, currencyId));
				}				
			}			
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}		
		return cfgHM;
	}

	private static TreeMap<Long, Long> getAmountCoinsCfg(Connection con, long currencyId) throws SQLException {
		TreeMap<Long, Long> conversionRates = new TreeMap<Long, Long>();
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
            String sql = " select * from copyop.cfg_copyop_coins coins where currency_id = " + currencyId;
			ps = con.prepareStatement(sql);
			rs = ps.executeQuery();
			while(rs.next()) {	
				conversionRates.put(rs.getLong("amount"),rs.getLong("coins"));				
			}			
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}		
		return conversionRates;
	}
	
	public static HashMap<Long, HashMap<Long, ArrayList<Long>>> getCopyoAmountsCfg(Connection con) throws SQLException{
		PreparedStatement ps = null;
		ResultSet rs = null;
		HashMap<Long, HashMap<Long, ArrayList<Long>>> cfgHM = new HashMap<Long, HashMap<Long, ArrayList<Long>>>();
		HashMap<Long, ArrayList<Long>> hm = null;
		try {
            String sql = " select * from copyop.cfg_copy_amounts  order by registration_date_type_id, currency_id, amount ";
            
			ps = con.prepareStatement(sql);
			rs = ps.executeQuery();
			while(rs.next()) {
				long typeId = 0L;
				typeId = rs.getLong("registration_date_type_id");
				if (!cfgHM.containsKey(typeId)){
					hm = new HashMap<Long, ArrayList<Long>>();
					ArrayList<Long> list = new ArrayList<Long>();
					list.add(rs.getLong("amount"));
					hm.put(rs.getLong("currency_id"), list);
					cfgHM.put(typeId, hm);
				} else {
					hm = cfgHM.get(typeId);
					if(!hm.containsKey(rs.getLong("currency_id"))){
						ArrayList<Long> list = new ArrayList<Long>();
						list.add(rs.getLong("amount"));
						hm.put(rs.getLong("currency_id"), list);
					} else {
						ArrayList<Long> list = hm.get(rs.getLong("currency_id"));
						list.add(rs.getLong("amount"));
					}
				}
			}			
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}		
		return cfgHM;
	}
	
	public static HashMap<Long, ArrayList<Date>> getCopyopRegistrationDateType(Connection con) throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		HashMap<Long, ArrayList<Date>> cfgHM = new HashMap<Long, ArrayList<Date>>();
		try {
			String sql = " select * from copyop.cfg_user_reg_date_type order by id ";

			ps = con.prepareStatement(sql);
			rs = ps.executeQuery();
			while (rs.next()) {
				ArrayList<Date> list = new ArrayList<Date>();
				list.add(rs.getDate("from_date"));
				list.add(rs.getDate("to_date"));
				cfgHM.put(rs.getLong("id"), list);
			}
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		return cfgHM;
	}
	
	public static HashMap<Long, Long> getCopyopIncreaseAmountBtnShowCfg(Connection con)throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		HashMap<Long, Long> cfgHM = new HashMap<Long, Long>();
		try {
            String sql = " select * from copyop.CFG_INCREASE_AMOUNT_BTN_SHOW ";

			ps = con.prepareStatement(sql);
			rs = ps.executeQuery();
			while(rs.next()) {								
				cfgHM.put(rs.getLong("currency_id"), rs.getLong("amount"));
			}			
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}		
		return cfgHM;
	}

	public static double[] getCopyopInvOdds(Connection con, Date linkDate) throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
            String sql = "select * from copyop.cfg_copied_inv_odds where ? between period_start and period_end";

			ps = con.prepareStatement(sql);
			ps.setDate(1, new java.sql.Date(linkDate.getTime()));
			rs = ps.executeQuery();
			
			if(rs.next()) {
				double[] odds = {rs.getDouble("odds_win"), rs.getDouble("odds_lose")};
				return odds;
			} else {
				logger.error("Can't load copyop odds !");
				throw new SQLException("Can't load copyop odds !");
			}
			
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}		
	}
	
	
	public static double[] getCopyopLastInvOdds(Connection conn) throws SQLException {
		CallableStatement cstmt = null;
		ResultSet rs = null;
		try {
			cstmt = conn.prepareCall("{call COPYOP.GET_COPYOP_INV_ODDS(o_data => ?)}");
			cstmt.registerOutParameter(1, OracleTypes.CURSOR);

			cstmt.executeQuery();

			rs = (ResultSet) cstmt.getObject(1);
			if (rs.next()) {
				double[] odds = { rs.getDouble("odds_win"), rs.getDouble("odds_lose") };
				return odds;
			} else {
				throw new SQLException();
			}
		} finally {
			closeStatement(cstmt);
			closeResultSet(rs);
		}
	}
}
