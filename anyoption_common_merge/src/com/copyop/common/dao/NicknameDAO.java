package com.copyop.common.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.anyoption.common.daos.DAOBase;

public class NicknameDAO extends DAOBase {

	public static String getUnunsedNicknameMarkAsUsed(Connection conn, long userId) throws SQLException{
		String nickName = getUnunsedNickname(conn);
		markAsUsed(conn, nickName, userId);
		return nickName;
	}
	
	public static String getUnunsedNickname(Connection conn) throws SQLException {
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			String sql = "SELECT nickname FROM copyop_nicknames WHERE user_id IS NULL AND ROWNUM = 1 ORDER BY ID";
			pstmt = conn.prepareStatement(sql);
			rs = pstmt.executeQuery();
			if (rs.next()) {
				return rs.getString("nickname");
			}
		} finally {
			closeResultSet(rs);
			closeStatement(pstmt);
		}
		return null;
	}

	public static void markAsUsed(Connection conn, String nickName, long userId) throws SQLException {
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			String sql = "UPDATE copyop_nicknames SET user_id = ? WHERE nickname = ? ";
			pstmt = conn.prepareStatement(sql);
			pstmt.setLong(1, userId);
			pstmt.setString(2, nickName);
			rs = pstmt.executeQuery();
		} finally {
			closeResultSet(rs);
			closeStatement(pstmt);
		}
	}
}
