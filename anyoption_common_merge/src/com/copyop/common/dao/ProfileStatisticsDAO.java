package com.copyop.common.dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import com.copyop.common.dto.ProfileInvResultCopy;
import com.copyop.common.dto.ProfileInvResultSelf;
import com.datastax.driver.core.BoundStatement;
import com.datastax.driver.core.PreparedStatement;
import com.datastax.driver.core.ResultSet;
import com.datastax.driver.core.Row;
import com.datastax.driver.core.Session;
import com.datastax.driver.core.utils.UUIDs;

public class ProfileStatisticsDAO extends DAOBase {

	public static ProfileInvResultSelf getProfileInvResultSelf(Session session, long userId, int limit, boolean blocked) {
		ProfileInvResultSelf pStats = new ProfileInvResultSelf(userId, limit);
		String statementName = blocked ? "getProfileInvResultSelfOfBlocked" : "getProfileInvResultSelf";
		PreparedStatement ps = methodsPreparedStatement.get(statementName);
		if(ps == null) {
			String query = "SELECT user_id, dateOf(time_created) as time_created, time_created as time_uuid_created, market_id, result"
							+ " FROM " + (blocked ? "blocked_" : "") + "profiles_self_inv_result WHERE user_id = ? ORDER BY time_created DESC LIMIT ?";
        	ps = session.prepare(query);
			methodsPreparedStatement.put(statementName, ps);
		}
		BoundStatement boundStatement = new BoundStatement(ps);
		boundStatement.bind(userId, limit);
		ResultSet rs = session.execute(boundStatement);
		while(!rs.isExhausted()) {
			Row row = rs.one();
			pStats.getMarketIds().add(row.getLong("market_id"));
			pStats.getResults().add(row.getBool("result"));
			pStats.getTimeCreated().add(row.getDate("time_created"));
			pStats.getTimeUUIDCreated().add(row.getUUID("time_uuid_created"));
		}
		return pStats;
	}

	public static void deleteProfileInvResultSelf(Session session, long userId) {
		PreparedStatement ps = methodsPreparedStatement.get("deleteProfileInvResultSelf");
		if(ps == null) {
			String query = "DELETE FROM " +
										"profiles_self_inv_result " +
							"WHERE " +
										"user_id = ?";
        	ps = session.prepare(query);
			methodsPreparedStatement.put("deleteProfileInvResultSelf", ps);
		}
		BoundStatement boundStatement = new BoundStatement(ps);
		boundStatement.bind(userId );
		session.execute(boundStatement);
	}
	
	public static List<Long> getLastSelfInvMarkets(Session session, long userId) {
		PreparedStatement ps = methodsPreparedStatement.get("selectProfileInvResultSelf");
		if(ps == null) {
			String query = "SELECT market_id FROM " +
										"profiles_self_inv_result " +
							"WHERE " +
										"user_id = ?";
        	ps = session.prepare(query);
			methodsPreparedStatement.put("selectProfileInvResultSelf", ps);
		}
		List<Long> lastMarkets = new ArrayList<Long>();
		BoundStatement boundStatement = new BoundStatement(ps);
		boundStatement.bind(userId );
		session.execute(boundStatement);
		
		ResultSet rs = session.execute(boundStatement);
		while(!rs.isExhausted()) {
			Row row = rs.one();
			lastMarkets.add(row.getLong("market_id"));
		}
		return lastMarkets;
	}
	
	public static ProfileInvResultCopy getProfileInvResultCopy(Session session, long userId, int limit) {
		ProfileInvResultCopy pStats = new ProfileInvResultCopy(userId, limit);
		PreparedStatement ps = methodsPreparedStatement.get("getProfileInvResultCopy");
		if(ps == null) {
			String query = "SELECT * FROM profiles_copy_inv_result WHERE user_id = ? ORDER BY time_created DESC LIMIT ?";
        	ps = session.prepare(query);
			methodsPreparedStatement.put("getProfileInvResultCopy", ps);
		}
		BoundStatement boundStatement = new BoundStatement(ps);
		boundStatement.bind(userId, limit);
		ResultSet rs = session.execute(boundStatement);
		while(!rs.isExhausted()) {
			Row row = rs.one();
			pStats.getResults().add(row.getBool("result"));
			pStats.getTimeCreated().add(row.getUUID("time_created"));
		}
		return pStats;
	}

	public static void insertProfileInvResultSelf(Session session, boolean result, long marketId, long userId) {
		PreparedStatement ps = methodsPreparedStatement.get("insertProfileInvResultSelf");
		if(ps==null){
			String query = "INSERT into profiles_self_inv_result (user_id, result, market_id, time_created) VALUES ( ?, ?, ?, now())";
			ps = session.prepare(query);
			methodsPreparedStatement.put("insertProfileInvResultSelf", ps);
		}
		BoundStatement boundStatement = new BoundStatement(ps);
		boundStatement.bind(userId, result, marketId);
		session.execute(boundStatement);
	}

	public static BoundStatement getInsertBlockedProfileInvResultSelfBS(Session session,
																		long userId,
																		UUID timeCreated,
																		long marketId,
																		boolean result) {
		PreparedStatement ps = methodsPreparedStatement.get("insertBlockedProfileInvResultSelf");
		if(ps == null) {
			String query = "INSERT INTO blocked_profiles_self_inv_result (user_id, time_created, market_id, result) VALUES (?, ?, ?, ?)";
			ps = session.prepare(query);
			methodsPreparedStatement.put("insertBlockedProfileInvResultSelf", ps);
		}
		BoundStatement boundStatement = new BoundStatement(ps);
		return boundStatement.bind(userId, timeCreated, marketId, result);
	}

	public static BoundStatement getDeleteBlockedProfileInvResultSelfBS(Session session, long userId) {
		PreparedStatement ps = methodsPreparedStatement.get("deleteBlockedProfileInvResultSelf");
		if (ps == null) {
			String query = "DELETE FROM blocked_profiles_self_inv_result WHERE user_id = ?";
			ps = session.prepare(query);
			methodsPreparedStatement.put("deleteBlockedProfileInvResultSelf", ps);
		}
		BoundStatement boundStatement = new BoundStatement(ps);
		return boundStatement.bind(userId);
	}

	public static void insertProfileInvResultCopy(Session session, boolean result, long userId) {
		PreparedStatement ps = methodsPreparedStatement.get("insertProfileInvResultCopy");
		if(ps==null){
			String query = "INSERT into profiles_copy_inv_result (user_id, result, time_created) VALUES ( ?, ?, now())";
			ps = session.prepare(query);
			methodsPreparedStatement.put("insertProfileInvResultCopy", ps);
		}
		BoundStatement boundStatement = new BoundStatement(ps);
		boundStatement.bind(userId, result);
		session.execute(boundStatement);
	}
	
	public static void insertProfileInvResultCopy(Session session, boolean result, long userId, Date timeCreated) {
		PreparedStatement ps = methodsPreparedStatement.get("insertProfileInvResultCopy");
		if(ps==null){
			String query = "INSERT into profiles_copy_inv_result (user_id, result, time_created) VALUES ( ?, ?, ?)";
			ps = session.prepare(query);
			methodsPreparedStatement.put("insertProfileInvResultCopy", ps);
		}
		BoundStatement boundStatement = new BoundStatement(ps);
		boundStatement.bind(userId, result, UUIDs.startOf(timeCreated.getTime()));
		session.execute(boundStatement);
	}

	public static BoundStatement getInsertBlockedProfileInvResultCopyBS(Session session,
																		long userId,
																		UUID timeCreated,
																		boolean result) {
		PreparedStatement ps = methodsPreparedStatement.get("insertBlockedProfileInvResultCopy");
		if (ps==null) {
			String query = "INSERT INTO blocked_profiles_copy_inv_result (user_id, time_created, result) VALUES (?, ?, ?)";
			ps = session.prepare(query);
			methodsPreparedStatement.put("insertBlockedProfileInvResultCopy", ps);
		}
		BoundStatement boundStatement = new BoundStatement(ps);
		return boundStatement.bind(userId, timeCreated, result);
	}

	public static BoundStatement getDeleteBlockedProfileInvResultCopyBS(Session session, long userId) {
		PreparedStatement ps = methodsPreparedStatement.get("deleteBlockedProfileInvResultCopy");
		if (ps == null) {
			String query = "DELETE FROM blocked_profiles_copy_inv_result WHERE user_id = ?";
			ps = session.prepare(query);
			methodsPreparedStatement.put("deleteBlockedProfileInvResultCopy", ps);
		}
		BoundStatement boundStatement = new BoundStatement(ps);
		return boundStatement.bind(userId);
	}

	public static long getSelfInvestmentsCount(Session session, long userId, int limit, boolean getBlockedData) {
		String statementName = getBlockedData ? "getBlockedSelfInvestmentsCount" : "getSelfInvestmentsCount";
		PreparedStatement ps = methodsPreparedStatement.get(statementName);
		if (ps == null) {
			String cql = "SELECT count(*) from "
							+ (getBlockedData ? "blocked_" : "")
							+ "profiles_self_inv_result WHERE user_id = ? LIMIT ?";
			ps = session.prepare(cql);
			methodsPreparedStatement.put(statementName, ps);
		}
		BoundStatement boundStatement = new BoundStatement(ps);
		ResultSet rs = session.execute(boundStatement.bind(userId, limit));
		if (!rs.isExhausted()) {
			return rs.one().getLong(0);
		}
		return 0l;
	}

/**
 *
 	public static boolean updateProfileStatisticsTombstone(Session session, ProfileStatistics pstats) {
		PreparedStatement ps = methodsPreparedStatement.get("updateProfileStatistics");
		if(ps == null) {
			String query =
					"UPDATE profiles_sr SET " +
							"hit_rate_copy_inv_success = ?, " +//40
							"hit_rate_inv_markets = ?, " +//120 marketId
							"hit_rate_inv_success = ?, " +//120 result (true/false)
							"time_last_change = now() " +
					"WHERE user_id = ? ";// +
					//"IF time_last_change = ? ";
			ps = session.prepare(query);
			ps.setConsistencyLevel(ConsistencyLevel.ANY);
			methodsPreparedStatement.put("updateProfileStatistics", ps);
		}
		BoundStatement boundStatement = new BoundStatement(ps);
		boundStatement.bind(
								pstats.getHitRateCopyInvSuccess(),
								pstats.getHitRateInvMarkets(),
								pstats.getHitRateInvSuccess(),
								pstats.getUserId()//,
								//pstats.getTimeLastChanged()
							);
		Row row = session.execute(boundStatement).one();
		return true;//row.getBool(APPLIED);
	}
*/

}
