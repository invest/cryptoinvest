package com.copyop.common.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.anyoption.common.bl_vos.ACHDepositBase;
import com.anyoption.common.daos.DAOBase;


public class ACHDepositDAOBase extends DAOBase{

	/**
	 * Insert ACH Deposit
	 * @param con
	 * @param vo
	 * @throws SQLException
	 */
	  public static void insert(Connection con,ACHDepositBase vo) throws SQLException {
		  PreparedStatement ps = null;
		  ResultSet rs = null;
		  try {
			  String sql="insert into ach_deposit(id,transaction_id,payee_email,payee_address,payee_city," +
			  		"payee_state,payee_zip,payee_phone,routing_no,account_no" +
			  		") values(seq_ach_deposit.nextval,?,?,?,?,?,?,?,?,?)";

			  ps = con.prepareStatement(sql);

			  ps.setLong(1, vo.getTransactionId());
			  ps.setString(2, vo.getPayeeEmail());
			  ps.setString(3,vo.getPayeeAddress());
			  ps.setString(4,vo.getPayeeCity());
			  ps.setString(5, vo.getPayeeState());
			  ps.setString(6, vo.getPayeeZip());
			  ps.setString(7, vo.getPayeePhone());
			  ps.setString(8, vo.getRoutingNo());
			  ps.setString(9, vo.getAccountNo());

			  ps.executeUpdate();
			  vo.setId(getSeqCurValue(con,"seq_ach_deposit"));
		  }
		  finally
		  {
			  closeStatement(ps);
			  closeResultSet(rs);
		  }
	  }


	  public static ACHDepositBase get(Connection con,long id) throws SQLException {

		  PreparedStatement ps=null;
		  ResultSet rs=null;

		  try {
			  String sql="select ach_deposit.* from ach_deposit where transaction_id=?";
			  ps = con.prepareStatement(sql);
			  ps.setLong(1, id);
			  rs=ps.executeQuery();
			  if (rs.next()) {
				  ACHDepositBase a = getVO(rs);
				  return a;
			  }
		  }
		  finally
		  {
			  closeResultSet(rs);
			  closeStatement(ps);
		  }
		  return null;
	  }

	  private static ACHDepositBase getVO(ResultSet rs) throws SQLException{
		  ACHDepositBase vo = new ACHDepositBase();

		  vo.setId(rs.getLong("id"));
		  vo.setTransactionId(rs.getLong("transaction_id"));
		  vo.setPayeeEmail(rs.getString("payee_email"));
		  vo.setPayeeAddress(rs.getString("payee_address"));
		  vo.setPayeeCity(rs.getString("payee_city"));
		  vo.setPayeeState(rs.getString("payee_state"));
		  vo.setPayeeZip(rs.getString("payee_zip"));
		  vo.setPayeePhone(rs.getString("payee_phone"));
		  vo.setRoutingNo(rs.getString("routing_no"));
		  vo.setAccountNo(rs.getString("account_no"));
		  return vo;
	  }
}

