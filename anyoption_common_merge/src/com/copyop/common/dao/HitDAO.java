package com.copyop.common.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import com.anyoption.common.daos.DAOBase;

public class HitDAO extends DAOBase{

	public static void insertHitRate(Connection con, long userId, float hitRateSelf, float hitRateCopied) throws SQLException {
		PreparedStatement ps = null;
		try{
			String sql = "Begin copyop.INSERT_HITS(?, ?, ?);  End;";
			ps = con.prepareStatement(sql);
			ps.setLong(1, userId);
			ps.setFloat(2, hitRateSelf);
			ps.setFloat(3, hitRateCopied);
			ps.executeUpdate();
		} finally {
			closeStatement(ps);
		}
	}
	
}
