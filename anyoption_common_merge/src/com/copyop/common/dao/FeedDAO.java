package com.copyop.common.dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.apache.log4j.Logger;

import com.anyoption.common.daos.BestHotDataDAO;
import com.copyop.common.dto.base.FeedMessage;
import com.copyop.common.enums.base.QueueTypeEnum;
import com.copyop.common.enums.base.UpdateTypeEnum;
import com.copyop.common.enums.base.UserStateEnum;
import com.datastax.driver.core.BoundStatement;
import com.datastax.driver.core.PreparedStatement;
import com.datastax.driver.core.ResultSet;
import com.datastax.driver.core.Row;
import com.datastax.driver.core.Session;
import com.datastax.driver.core.utils.UUIDs;

/**
 * @author kirilim
 */
public class FeedDAO extends DAOBase {

	private static final Logger log = Logger.getLogger(FeedDAO.class);

	public static List<FeedMessage> getFeed(Session session, long userId, QueueTypeEnum type, Date timeCreated, int limit) {
		List<FeedMessage> feed = new ArrayList<FeedMessage>();
		PreparedStatement ps = methodsPreparedStatement.get("getFeed");
		if (ps == null) {
			String cql = "SELECT * FROM feed WHERE user_id = ? AND queue_type = ? AND time_created < minTimeuuid(?) LIMIT ?";
			ps = session.prepare(cql);
			methodsPreparedStatement.put("getFeed", ps);
		}
		BoundStatement boundStatement = new BoundStatement(ps);
		if (log.isTraceEnabled()) {
			boundStatement.enableTracing();
		}
		ResultSet rs = session.execute(boundStatement.bind(userId, type.getId(), timeCreated, limit));
		if (log.isTraceEnabled()) {
			traceLog(rs.getExecutionInfo(), log);
		}
		while (!rs.isExhausted()) {
			FeedMessage fm = loadMessage(rs.one());
			// check if users involved in this message are not removed
			String[] keys = {FeedMessage.PROPERTY_KEY_USER_ID, FeedMessage.PROPERTY_KEY_SRC_USER_ID, FeedMessage.PROPERTY_KEY_DEST_USER_ID};
			boolean add = true;
			for(int i =0;i<keys.length;i++) {
				try{
					long id = Long.parseLong(fm.getProperties().get(keys[i]));
					add = add && (ProfileDAO.getUserState(session, id) != UserStateEnum.STATE_REMOVED);
				}catch(NumberFormatException ex) {
				}
			}
			if(add) {
				feed.add(fm);
			}
		}
		return feed;
	}

	public static void insertFeedMessage(Session session, FeedMessage f, QueueTypeEnum queueType, UUID insertUUID) {
		PreparedStatement ps = getInsertFeedMessagePS(session);
		BoundStatement boundStatement = new BoundStatement(ps);
		if (log.isTraceEnabled()) {
			boundStatement.enableTracing();
		}
		ResultSet rs = session.execute(boundStatement.bind(f.getUserId(),
															queueType == null ? f.getQueueType() : queueType.getId(),
															insertUUID,
															f.getMsgType(),
															f.getProperties()));
		if (log.isTraceEnabled()) {
			traceLog(rs.getExecutionInfo(), log);
		}
	}

	public static void insertFeedMessage(Session session, FeedMessage f, QueueTypeEnum queueType, int ttl, UUID insertUUID) {
		PreparedStatement ps = getInsertFeedMessageWithTTLPS(session);
		BoundStatement boundStatement = new BoundStatement(ps);
		if (log.isTraceEnabled()) {
			boundStatement.enableTracing();
		}
		ResultSet rs = session.execute(boundStatement.bind(f.getUserId(),
															queueType == null ? f.getQueueType() : queueType.getId(),
															insertUUID,
															f.getMsgType(),
															f.getProperties(),
															ttl));
		if (log.isTraceEnabled()) {
			traceLog(rs.getExecutionInfo(), log);
		}
	}

	public static void insertFeedPostedAsset(Session session, String timeInsert, int marketId) {
		PreparedStatement ps = methodsPreparedStatement.get("insertFeedPostedAsset");
		if (ps == null) {
			String cql =
					"INSERT INTO " +
							"feed_posted_asset " +
									"(time_insert, market_id) " +
					"VALUES " +
					 				"(?, ?)  ";
			ps = session.prepare(cql);
			methodsPreparedStatement.put("insertFeedPostedAsset", ps);
		}
		BoundStatement boundStatement = new BoundStatement(ps);
		session.execute(boundStatement.bind(
				timeInsert,
				marketId));
	}

	public static HashSet<Integer> getFeedPostedAsset(Session session, String dateTime) {
		HashSet<Integer> result = new HashSet<Integer>();
		PreparedStatement ps = methodsPreparedStatement.get("getFeedPostedAsset");
		if (ps == null) {
			String cql =
					"SELECT " +
								" * " +
					"FROM " +
							"feed_posted_asset " +
					"WHERE " +
								" time_insert = ? ";
			ps = session.prepare(cql);
			methodsPreparedStatement.put("getFeedPostedAsset", ps);
		}
		BoundStatement boundStatement = new BoundStatement(ps);
		ResultSet rs = session.execute(boundStatement.bind(dateTime));
		while (!rs.isExhausted()) {
			result.add(rs.one().getInt("market_id"));
		}
		return result;
	}


	public static void insertFeedLastInsertMsgs(Session session, int queueType, int msgType) {
		PreparedStatement ps = methodsPreparedStatement.get("insertFeedLastInsertMsgs");
		if (ps == null) {
			String cql =
					"INSERT INTO " +
							"feed_last_insert_msgs " +
									"( queue_type, msg_type, time_insert) " +
					"VALUES " +
					 				"(?, ?, now()) ";
			ps = session.prepare(cql);
			methodsPreparedStatement.put("insertFeedLastInsertMsgs", ps);
		}
		BoundStatement boundStatement = new BoundStatement(ps);
		session.execute(boundStatement.bind(
				queueType,
				msgType));
	}

	public static void insertFeedLastInsertMsgs(Session session, int queueType, int msgType, int ttl) {
		PreparedStatement ps = methodsPreparedStatement.get("insertFeedLastInsertMsgsWTTL");
		if (ps == null) {
			String cql =
					"INSERT INTO " +
							"feed_last_insert_msgs " +
									"( queue_type, msg_type, time_insert) " +
					"VALUES " +
					 				"(?, ?, now()) " +
					" USING TTL ? ";
			ps = session.prepare(cql);
			methodsPreparedStatement.put("insertFeedLastInsertMsgsWTTL", ps);
		}
		BoundStatement boundStatement = new BoundStatement(ps);
		session.execute(boundStatement.bind(
				queueType,
				msgType,
				ttl));
	}

	public static void insertFeedPostedUsers(Session session, String timeInsert, long userId, int msgType) {
		PreparedStatement ps = methodsPreparedStatement.get("insertFeedPostedUsers");
		if (ps == null) {
			String cql =
					"INSERT INTO " +
							"feed_posted_users " +
									"( time_insert, user_id, msg_type) " +
					"VALUES " +
					 				"(?, ?, ?) ";
			ps = session.prepare(cql);
			methodsPreparedStatement.put("insertFeedPostedUsers", ps);
		}
		BoundStatement boundStatement = new BoundStatement(ps);
		session.execute(boundStatement.bind(
				timeInsert,
				userId,
				msgType));
	}

	public static Map<Long, Integer> getFeedPostedUsersMsg(Session session, String dateTime) {
		Map<Long, Integer> result = new HashMap<Long, Integer>();
		PreparedStatement ps = methodsPreparedStatement.get("getFeedPostedUsersMsg");
		if (ps == null) {
			String cql = "SELECT * FROM feed_posted_users where time_insert = ?";
			ps = session.prepare(cql);
			methodsPreparedStatement.put("getFeedPostedUsersMsg", ps);
		}
		BoundStatement boundStatement = new BoundStatement(ps);
		boundStatement.enableTracing();
		ResultSet rs = session.execute(boundStatement.bind(dateTime));
		while (!rs.isExhausted()) {
			Row row = rs.one();
			result.put(row.getLong("user_id"), row.getInt("msg_type"));
		}
		traceLog(rs.getExecutionInfo(), log);
		return result;
	}

	public static void updateCopiedCounters(Session session, Date timeInser, long userId) {
		PreparedStatement ps = methodsPreparedStatement.get("updateCopiedCounters");
		if (ps == null) {
			String cql = "UPDATE " +
							" copied_counters " +
						 " SET " +
						 	" copied_count = copied_count + 1 " +
						 	" where user_id  = ? " +
						 	" and time_copied = ? ";
			ps = session.prepare(cql);
			methodsPreparedStatement.put("updateCopiedCounters", ps);
		}
		BoundStatement boundStatement = new BoundStatement(ps);
		session.execute(boundStatement.bind(
				userId,
				timeInser
				));
	}
	
	public static void updateUncopiedCounters(Session session, Date timeInser, long userId) {
		PreparedStatement ps = methodsPreparedStatement.get("updateUncopiedCounters");
		if (ps == null) {
			String cql = "UPDATE " +
							" copied_counters " +
						 " SET " +
						 	" uncopied_count = uncopied_count - 1 " +
						 	" where user_id  = ? " +
						 	" and time_copied = ? ";
			ps = session.prepare(cql);
			methodsPreparedStatement.put("updateUncopiedCounters", ps);
		}
		BoundStatement boundStatement = new BoundStatement(ps);
		session.execute(boundStatement.bind(
				userId,
				timeInser
				));
	}

	public static void deleteCopiedCounters(Session session, Date timeInser) {
		PreparedStatement ps = methodsPreparedStatement.get("deleteCopiedCounters");
		if (ps == null) {
			String cql = "DELETE  FROM " +
							" copied_counters " +
						 " WHERE " +
						 	" time_copied = ? ";
			ps = session.prepare(cql);
			methodsPreparedStatement.put("deleteCopiedCounters", ps);
		}
		BoundStatement boundStatement = new BoundStatement(ps);
		session.execute(boundStatement.bind(
				timeInser
				));
	}

	public static Map<Long, Long> getCopiedCounters(Session session, Date dateTime, Map<Long, Integer> insertedUserMsg) {
		Map<Long, Long> result = new HashMap<Long, Long>();
		PreparedStatement ps = methodsPreparedStatement.get("getCopiedCounters");
		if (ps == null) {
			String cql = "SELECT * FROM " +
							" copied_counters " +
						 " WHERE " +
						 	" time_copied = ? ";
			ps = session.prepare(cql);
			methodsPreparedStatement.put("getCopiedCounters", ps);
		}
		BoundStatement boundStatement = new BoundStatement(ps);
		boundStatement.enableTracing();
		ResultSet rs = session.execute(boundStatement.bind(dateTime));
		while (!rs.isExhausted()) {
			Row row = rs.one();
			long userId = row.getLong("user_id");
			if (BestHotDataDAO.canBeInsertUser(userId, UpdateTypeEnum.SYS_24H_MOST_COPIED.getId(), insertedUserMsg)) {
				result.put(row.getLong("user_id"), row.getLong("copied_count") + row.getLong("uncopied_count"));
			}
		}
		traceLog(rs.getExecutionInfo(), log);
		return result;
	}

	private static FeedMessage loadMessage(Row row) {
		FeedMessage msg = new FeedMessage();
		msg.setUserId(row.getLong("user_id"));
		msg.setQueueType(row.getInt("queue_type"));
		msg.setMsgType(row.getInt("msg_type"));
		msg.setTimeCreatedUUID(row.getUUID("time_created"));
		msg.setTimeCreated(new Date(UUIDs.unixTimestamp(msg.getTimeCreatedUUID())));
		// cassandra returns unmodifiable collection
		msg.setProperties(new HashMap<String, String>(row.getMap("properties", String.class, String.class)));
		return msg;
	}

	public static List<FeedMessage> getLastHotTableData(Session session, UpdateTypeEnum msgType) {
		List<FeedMessage> feed = new ArrayList<FeedMessage>();
		if (msgType != UpdateTypeEnum.BEST_COPIERS && msgType != UpdateTypeEnum.BEST_TRADERS && msgType != UpdateTypeEnum.BEST_TRADES && msgType != UpdateTypeEnum.BEST_ASSET_SPECIALISTS
				&& msgType != UpdateTypeEnum.BEST_COPIERS_ONE_WEEK && msgType != UpdateTypeEnum.BEST_TRADERS_ONE_WEEK && msgType != UpdateTypeEnum.BEST_TRADES_ONE_WEEK && msgType != UpdateTypeEnum.BEST_ASSET_SPECIALISTS_ONE_WEEK
				&& msgType != UpdateTypeEnum.BEST_COPIERS_ONE_MOTNH && msgType != UpdateTypeEnum.BEST_TRADERS_ONE_MOTNH && msgType != UpdateTypeEnum.BEST_TRADES_ONE_MOTNH && msgType != UpdateTypeEnum.BEST_ASSET_SPECIALISTS_ONE_MOTNH
				&& msgType != UpdateTypeEnum.BEST_COPIERS_TRHEE_MOTNH && msgType != UpdateTypeEnum.BEST_TRADERS_TRHEE_MOTNH && msgType != UpdateTypeEnum.BEST_TRADES_TRHEE_MOTNH & msgType != UpdateTypeEnum.BEST_ASSET_SPECIALISTS_TRHEE_MOTNH) {
			log.warn("msgType param is not from hot types msgType = " + msgType);
			return feed;
		}
		PreparedStatement ps = methodsPreparedStatement.get("getLastHotTableData");
		if (ps == null) {
			String cql = "SELECT time_insert FROM feed_last_insert_msgs where queue_type = ? and msg_type = ?";
			ps = session.prepare(cql);
			methodsPreparedStatement.put("getLastHotTableData", ps);
		}
		BoundStatement boundStatement = new BoundStatement(ps);
		if (log.isTraceEnabled()) {
			boundStatement.enableTracing();
		}
		ResultSet rs = session.execute(boundStatement.bind(QueueTypeEnum.HOT.getId(), msgType.getId()));
		if (log.isTraceEnabled()) {
			traceLog(rs.getExecutionInfo(), log);
		}
		UUID timeLastInserted = null;
		if (!rs.isExhausted()) {
			timeLastInserted = rs.one().getUUID("time_insert");
		}
		if (timeLastInserted != null) {
			ps = methodsPreparedStatement.get("getLatestFeedByTime");
			if (ps == null) {
				String cql = "SELECT * FROM feed where user_id = ? and queue_type = ? and time_created > ?";
				ps = session.prepare(cql);
				methodsPreparedStatement.put("getLatestFeedByTime", ps);
			}
			boundStatement = new BoundStatement(ps);
			if (log.isTraceEnabled()) {
				boundStatement.enableTracing();
			}
			rs = session.execute(boundStatement.bind(FeedMessage.HOT_SYSTEM_FEED_USER_ID, QueueTypeEnum.HOT.getId(), timeLastInserted));
			if (log.isTraceEnabled()) {
				traceLog(rs.getExecutionInfo(), log);
			}
			while (!rs.isExhausted()) {
				Row row = rs.one();
				if (row.getInt("msg_type") == msgType.getId()) {
					feed.add(loadMessage(row));
				}
			}
		}
		return feed;
	}

	public static void deleteUserPostAlgorithmData(Session session, long userId) {
		PreparedStatement ps = methodsPreparedStatement.get("deleteUserPostAlgorithmData");
		if (ps == null) {
			StringBuilder sb = new StringBuilder(" DELETE FROM feed_post_algorithm_data WHERE user_id = ? AND queue_type IN (");
			sb.append(QueueTypeEnum.NEWS.getId())
				.append(", ")
				.append(QueueTypeEnum.EXPLORE.getId())
				.append("); ");
			ps = session.prepare(sb.toString());
			methodsPreparedStatement.put("deleteUserPostAlgorithmData", ps);
		}

		BoundStatement boundStatement = new BoundStatement(ps);
		if (log.isTraceEnabled()) {
			boundStatement.enableTracing();
		}
		ResultSet rs = session.execute(boundStatement.bind(userId));
		if (log.isTraceEnabled()) {
			traceLog(rs.getExecutionInfo(), log);
		}
	}

	protected static PreparedStatement getInsertFeedMessageWithTTLPS(Session session) {
		PreparedStatement ps = methodsPreparedStatement.get("insertFeedMessageWTTL");
		if (ps == null) {
			String cql =
					"INSERT INTO " +
							"feed " +
									"(user_id, queue_type, time_created, msg_type, properties) " +
					"VALUES " +
					 				"(?, ?, ?, ?, ?) " +

					" USING TTL ? ";
			ps = session.prepare(cql);
			methodsPreparedStatement.put("insertFeedMessageWTTL", ps);
		}
		return ps;
	}

	protected static PreparedStatement getInsertFeedMessagePS(Session session) {
		PreparedStatement ps = methodsPreparedStatement.get("insertFeedMessage");
		if (ps == null) {
			String cql =
					"INSERT INTO " +
							"feed " +
									"(user_id, queue_type, time_created, msg_type, properties) " +
					"VALUES " +
					 				"(?, ?, ?, ?, ?) ";
			ps = session.prepare(cql);
			methodsPreparedStatement.put("insertFeedMessage", ps);
		}
		return ps;
	}
}