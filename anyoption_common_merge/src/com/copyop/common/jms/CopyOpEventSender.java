package com.copyop.common.jms;

import java.io.Serializable;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.DeliveryMode;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.MessageProducer;
import javax.jms.ObjectMessage;
import javax.jms.Session;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import org.apache.log4j.Logger;

import com.anyoption.common.util.CommonUtil;
import com.copyop.common.Constants;
import com.copyop.common.jms.events.CopyOpEvent;
import com.copyop.common.jms.events.InvCreateEvent;
import com.copyop.common.jms.events.InvSettleEvent;
import com.copyop.common.jms.updates.UpdateMessage;


public class CopyOpEventSender {

	private static final Logger log = Logger.getLogger(CopyOpEventSender.class);
	private static ConnectionFactory connectionFactory;

	private static final String DESTINATOIN_PEFIX = "";

	public static final String FEED_DESTINATION_NAME			= DESTINATOIN_PEFIX + "copyOp.jb.client.feed";
	public static final String LS_TOPIC_DESTINATION_NAME 		= DESTINATOIN_PEFIX + "copyOp.ls.client.feed";
	public static final String INV_CREATE_DESTINATION_NAME		= DESTINATOIN_PEFIX + "copyOp.investments.create";
	public static final String INV_SETTLE_DESTINATION_NAME		= DESTINATOIN_PEFIX + "copyOp.investments.settle";
	public static final String PROFILE_MANAGE_DESTINATION_NAME	= DESTINATOIN_PEFIX + "copyOp.profile.manage";
	public static final String USER_DELAY_SYSTEM_MSG			= DESTINATOIN_PEFIX + "copyOp.user.delay.system.msg";
	public static final String MONITOR_PING						= DESTINATOIN_PEFIX + "monitor.ping";
	public static final String TOURNAMENT_INVESTMENT			= DESTINATOIN_PEFIX + "tournament.investment";

	public static void sendEvent(CopyOpEvent event, String queueName) {
		sendObjectMessage(event, queueName, true, 0, 0, 4);
	}

	public static void sendEvent(CopyOpEvent event, String queueName, long delay) {
		sendObjectMessage(event, queueName, true, delay, 0, 4);
	}

	public static void sendUpdate(UpdateMessage update, String queueName) {
		sendObjectMessage(update, queueName, true, 0, update.getUpdateType().getTtlMillis(), update.getUpdateType().getPriority().getPriority());
	}

	public static void sendUpdateToTopic(UpdateMessage update, String topicName) {
		sendObjectMessage(update, topicName, false, 0, 0, update.getUpdateType().getPriority().getPriority());
	}

	/**
	 *
	 * @param update - the UpdateMessage to be processed
	 * @param queueName -  the name identifying the destination Queue
	 * @param delay - The time in milliseconds that a message will wait before being scheduled to be delivered by the broker
	 * @param timeToLive - The message's lifetime (in milliseconds)
	 */
	public static void sendUpdate(UpdateMessage update, String queueName, long delay, long timeToLive) {
		sendObjectMessage(update, queueName, true, delay, timeToLive, update.getUpdateType().getPriority().getPriority());
	}

	public static boolean sendMessage(CopyOpEvent event) {
		String queueName = "";
		long delay = 0;
		if(event instanceof InvCreateEvent) {
			queueName = INV_CREATE_DESTINATION_NAME;
    		delay = (Integer.parseInt(CommonUtil.getProperty("cancel.seconds.client")) + Integer.parseInt(CommonUtil.getProperty("cancel.seconds.server"))) * 1000;
		} else if(event instanceof InvSettleEvent) {
			queueName = INV_SETTLE_DESTINATION_NAME;
		} else {
			log.error("Unsupported message type:"+event);
			return false;
		}
		return sendObjectMessage(event, queueName, true, delay, 0, 4);
	}
	private static boolean sendObjectMessage(Serializable object,
											String destinationName,
											boolean queue,
											long delay,
											long timeToLive,
											int priority) {
		MessageProducer producer = null;
		Session session = null;
		Connection connection = null;
		try {
			connection = getConnectionFactory().createConnection();
			session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
			Destination destination = null;
			if (queue) {
				destination = session.createQueue(destinationName);
			} else {
				destination = session.createTopic(destinationName);
			}
			producer = session.createProducer(destination);
			ObjectMessage msg = session.createObjectMessage();
			msg.setObject(object);
			//ActiveMQ specific parameter
			if(delay > 0) {
				msg.setLongProperty(Constants.AMQ_SCHEDULED_DELAY, delay);
			}

			if(timeToLive > 0) {
				producer.send(msg, DeliveryMode.NON_PERSISTENT, priority, timeToLive);
			} else {
				msg.setJMSPriority(priority);
				if(FEED_DESTINATION_NAME.equals(destination)) {
					msg.setJMSDeliveryMode(DeliveryMode.NON_PERSISTENT);
				}
				producer.send(msg);
			}
			return true;
		} catch(NamingException e) {
			log.error("JNDI problem :", e);
			return false;
		} catch (JMSException e) {
			log.error("AMQ problem", e);
			return false;
		} catch (Throwable t) {
			log.error("Runtime error", t);
			return false;
		} finally {
			try {
				connection.close();
			} catch (Exception e) {/*ignore*/}
		}
	}

	private static ConnectionFactory getConnectionFactory() throws NamingException {
		if(connectionFactory == null) {
			InitialContext initCtx = new InitialContext();
			try {
				connectionFactory = (ConnectionFactory) initCtx.lookup(Constants.JNDI_NAME_ACTIVEMQ);
			} catch(NamingException ex){
				connectionFactory = (ConnectionFactory) initCtx.lookup(Constants.JNDI_NAME_ACTIVEMQ_TOMCAT);
			}
		}
		return connectionFactory;
	}
}
