package com.copyop.common.jms;
import java.util.Properties;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.JMSException;
import javax.jms.MessageProducer;
import javax.jms.Queue;
import javax.jms.Session;
import javax.jms.TextMessage;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import org.apache.log4j.Logger;

import com.copyop.common.managers.ConfigurationCopyopManager;


public class LevelServiceSender {
	private static final Logger log = Logger.getLogger(LevelServiceSender.class);
	
	private static ConnectionFactory connectionFactory = null;
	private static Queue queue;

    static ConnectionFactory getConnectionFactory() throws NamingException {
    	if(connectionFactory == null) {
    		log.debug("loading level service jms properties - start");
    		Properties p = new Properties();
    		p.put(Context.INITIAL_CONTEXT_FACTORY, "org.jnp.interfaces.NamingContextFactory");
    		p.put(Context.URL_PKG_PREFIXES, "org.jboss.naming:org.jnp.interfaces");
    		p.put(Context.PROVIDER_URL, ConfigurationCopyopManager.getCopyopPropertiesValue("LEVEL_SERVICE_JNDI_PROVIDER_URL", "jnp://192.168.100.16:1099"));
    		InitialContext context = new InitialContext(p);
    		connectionFactory = (ConnectionFactory)context.lookup(ConfigurationCopyopManager.getCopyopPropertiesValue("LEVEL_SERVICE_JNDI_FACTORY_NAME", "/ConnectionFactory"));
    		queue =  (Queue) context.lookup(ConfigurationCopyopManager.getCopyopPropertiesValue("LEVEL_SERVICE_JNDI_QUEUE_NAME", "queue/etSubscribtionQueue"));
    		context.close();
    		log.debug("loading level service jms properties - end");
    	}
    	return connectionFactory;
    }

	public static void sendNotifyForInvestment(String msg) {
		log.trace("Sending to Level Service"+msg);
		Connection connection = null;
        try {
        	connection = getConnectionFactory().createConnection();
            Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
            MessageProducer publisher = session.createProducer(queue);
            TextMessage message = session.createTextMessage(msg);
            publisher.send(message);
        } catch (JMSException e) {
        	log.debug("Level Service JMS problem", e);
		} catch (NamingException e) {
			log.debug("Level service JNDI problem", e);
		} finally {
        	try {
				connection.close();
			} catch (Exception e) {/*ignore*/}
        }
	}
}
