package com.copyop.common.jms.events;

public class TournamentInvestmentEvent implements CopyOpEvent{
	/**
	 * 
	 */
	private static final long serialVersionUID = 8182524847741597145L;
	private long opportunity_product_type_id;
	private double baseAmount; //in $
	private long userId;
	private long userSkin;
	
	public TournamentInvestmentEvent(long opportunity_product_type_id, double baseAmount, long userId, long userSkin) {
		super();
		this.opportunity_product_type_id = opportunity_product_type_id;
		this.baseAmount = baseAmount;
		this.userId = userId;
		this.userSkin = userSkin;
	}

	public long getOpportunity_product_type_id() {
		return opportunity_product_type_id;
	}
	
	public void setOpportunity_product_type_id(long opportunity_product_type_id) {
		this.opportunity_product_type_id = opportunity_product_type_id;
	}
	
	public double getBaseAmount() {
		return baseAmount;
	}
	
	public void setBaseAmount(double baseAmount) {
		this.baseAmount = baseAmount;
	}
	
	public long getUserId() {
		return userId;
	}
	
	public void setUserId(long userId) {
		this.userId = userId;
	}
	
	public long getUserSkin() {
		return userSkin;
	}
	
	public void setUserSkin(long userSkin) {
		this.userSkin = userSkin;
	}	
}
