package com.copyop.common.jms.events;

import com.anyoption.common.enums.CopyOpInvTypeEnum;

public class InvCreateEvent implements CopyOpEvent{
	/**
	 * 
	 */
	private static final long serialVersionUID = 8182524847741597145L;
	long oppId;
	long invId;
	int dir; // call/put
	double level;
	double oddsWin;
	double oddsLose;
	long userId;
	long destUserId; // userId of the followed investment
	long currencyId;
	long marketId;
	double amount; // the real amount ( divided by 100 )
	double rate;
	float riskAppetite;
	CopyOpInvTypeEnum copyopType;
	long writerId;
	long timeLastInvest; // timestamp GMT
	long timeClose; // closing time for the investment
	int scheduled;

	public InvCreateEvent(	long oppId, long invId, int dir, double amount, double level, double oddsWin, double oddsLose, long userId,
							long marketId, double rate, float riskAppetite, CopyOpInvTypeEnum copyopType, long destUserId, int writerId,
							long currencyId, long timeLastInvest, long timeClose, int scheduled) {
		super();
		this.oppId = oppId;
		this.invId = invId;
		this.level = level;
		this.oddsWin = oddsWin;
		this.oddsLose = oddsLose;
		this.userId = userId;
		this.marketId = marketId;
		this.rate = rate;
		this.dir = dir;
		this.riskAppetite = riskAppetite;
		this.copyopType = copyopType;
		this.destUserId = destUserId;
		this.writerId = writerId;
		this.amount = amount;
		this.currencyId = currencyId;
		this.timeLastInvest = timeLastInvest;
		this.timeClose = timeClose;
		this.scheduled = scheduled;
	}

	public long getOppId() {
		return oppId;
	}

	public long getInvId() {
		return invId;
	}

	public int getDir() {
		return dir;
	}

	public double getLevel() {
		return level;
	}

	public double getOddsWin() {
		return oddsWin;
	}

	public double getOddsLose() {
		return oddsLose;
	}

	public long getUserId() {
		return userId;
	}

	public long getDestUserId() {
		return destUserId;
	}

	public long getMarketId() {
		return marketId;
	}

	public double getAmount() {
		return amount;
	}

	public double getRate() {
		return rate;
	}

	public float getRiskAppetite() {
		return riskAppetite;
	}

	public CopyOpInvTypeEnum getCopyopType() {
		return copyopType;
	}

	public long getWriterId() {
		return writerId;
	}

	public long getCurrencyId() {
		return currencyId;
	}

	public long getTimeLastInvest() {
		return timeLastInvest;
	}

	public long getTimeClose() {
		return timeClose;
	}

	public int getScheduled() {
		return scheduled;
	}

	@Override
	public String toString() {
		return "InvCreateEvent [oppId=" + oppId + ", invId=" + invId + ", dir=" + dir + ", level=" + level
				+ ", oddsWin=" + oddsWin + ", oddsLose=" + oddsLose + ", userId=" + userId + ", destUserId="
				+ destUserId + ", currencyId=" + currencyId + ", marketId=" + marketId + ", amount=" + amount
				+ ", rate=" + rate + ", riskAppetite=" + riskAppetite + ", copyopType=" + copyopType + ", writerId="
				+ writerId + ", timeLastInvest=" + timeLastInvest + ", timeClose=" + timeClose + ", scheduled=" + scheduled + "]";
	}
	
}
