package com.copyop.common.jms;

public interface CopyOpAsyncEventSenderMBean {

	public int getPendingMsgSize();
}
