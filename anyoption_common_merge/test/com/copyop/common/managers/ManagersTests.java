package com.copyop.common.managers;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ 
					FbManagerTest.class
					,ProfileLinksManagerTest.class
					,ProfileManagerTest.class
					,ProfileStatisticsManagerTest.class
					,CurrenciesCacheManagerTest.class
					,ProfileCountersManagerTest.class
					,InvestmentManagerTest.class
					,NicknameManagerTest.class
				})
public class ManagersTests {
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
    	ManagerBase.init(new String[]{"192.168.100.22"}, "copyop");
    	ManagerBase.getSession();
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
    	ManagerBase.closeSession();
    	ManagerBase.closeCluster();
	}
}
