package com.copyop.common.managers;
import static org.junit.Assert.fail;

import java.sql.SQLException;

import junit.framework.Assert;

import org.junit.Test;

public class NicknameManagerTest {

	@Test
	public void test() {
		try {
			String nickname = NicknameManager.getUnunsedNicknameFor(1);
			Assert.assertNotNull(nickname);
		} catch(SQLException ex) {
			fail(ex.getMessage());
		}
	}

}
