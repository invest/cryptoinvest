package com.copyop.common.managers;

import java.util.HashMap;

import junit.framework.Assert;

import org.junit.BeforeClass;
import org.junit.Test;

import com.copyop.common.util.JndiMocker;

public class CurrenciesCacheManagerTest {
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		JndiMocker.setUpJndiDataSource("dbtestbg.anyoption.com", "etrader",  "superman#1", "etrader" );
	}


	@Test
	public void testGetCurrenciesRatesToUSD() {
		HashMap<String, String> result = CurrenciesCacheManager.getConvertedAmounts(100.00);
		HashMap<String, String> result1 = CurrenciesCacheManager.getConvertedAmounts(100.00);
		System.out.println(result);
		Assert.assertNotNull(result);
		Assert.assertEquals(result, result1);
	}

}
