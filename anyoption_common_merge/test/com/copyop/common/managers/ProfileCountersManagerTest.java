package com.copyop.common.managers;

import junit.framework.Assert;

import org.junit.Test;

public class ProfileCountersManagerTest {

	@Test
	public void testProfileCopiers() {
		long userId1 = 1;
		long userId2 = 2;
		long userID1copiers = ProfileCountersManager.getProfileCopiers(userId1);
		long userId2copying = ProfileCountersManager.getProfileCopying(userId2);

		ProfileCountersManager.increaseMutualCountsCopy(userId1, userId2);
		
		Assert.assertEquals(userID1copiers+1, ProfileCountersManager.getProfileCopiers(userId1));
		Assert.assertEquals(userId2copying+1, ProfileCountersManager.getProfileCopying(userId2));
	}
	
	@Test
	public void testCoinsBalance() {
		long userId = 1;
		long coins = ProfileCountersManager.getProfileCoinsBalance(userId);
		ProfileCountersManager.increaseProfileCoinsBalance(userId, 5);
		Assert.assertEquals(coins + 5, ProfileCountersManager.getProfileCoinsBalance(userId));
	}
}
