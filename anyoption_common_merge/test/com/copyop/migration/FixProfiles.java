package com.copyop.migration;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;

import com.anyoption.common.managers.InvestmentsManagerBase;
import com.copyop.common.dao.ProfilesPaginatorDAO;
import com.copyop.common.dto.CopiedInvestment;
import com.copyop.common.managers.ManagerBase;
import com.copyop.common.util.JndiMocker;
import com.datastax.driver.core.Session;

 
public class FixProfiles {
	
	private static final Logger logger = Logger.getLogger(FixProfiles.class);
	
	static volatile int profilesCounter = 0;
	static volatile int investmentCounter = 0;
	static int pageSizeInt;
	
	public static void main(String args[]) {
		
		Properties props = new Properties();
		String propFileName = "copyopMigration.properties";
		InputStream inputStream = null;
		if(args.length > 0 && args[0] != null && args[0].length()>0 ) {
			try {
				inputStream = new FileInputStream(new File(args[0]));
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			}
		} else {
			inputStream = FixProfiles.class.getClassLoader().getResourceAsStream(propFileName);
		}
		
		if (inputStream == null) {
			System.out.println("Properties file :"+ propFileName+ " Not found in class path !");
		}		
		try {
			props.load(inputStream);
			String cassandraUrlsCsv		= props.getProperty("cassandra.urls.csv");
			String cassandraKeyspace	= props.getProperty("cassandra.keyspace");
			String oracleUrl			= props.getProperty("oracle.url");
			String oracleUsername		= props.getProperty("oracle.username");
			String oraclePassword		= props.getProperty("oracle.password");
			String oracleServiceName	= props.getProperty("oracle.service.name");
			String threadPoolSize		= props.getProperty("threadpool.size");
			String pageSize				= props.getProperty("page.size");
			String fullCalcParam		= props.getProperty("calculate.full");
			
			boolean valid = (cassandraUrlsCsv != null) && 
							(cassandraKeyspace != null) && 
							(oracleUrl != null) && 
							(oracleUsername != null) && 
							(oraclePassword != null) && 
							(oracleServiceName != null ) &&
							(threadPoolSize != null) && 
							(pageSize != null) &&
							(fullCalcParam != null) ;
			
			if(!valid) {
				System.out.println("Unsuficient info in properties file :");
				System.out.println(props);
			}
			System.out.println("Page size:"+pageSize);
			Boolean callculateAll = Boolean.parseBoolean(fullCalcParam);
			
			ManagerBase.init(cassandraUrlsCsv.split(","), cassandraKeyspace);
			Session session = ManagerBase.getSession();
			ExecutorService executor = Executors.newFixedThreadPool(Integer.parseInt(threadPoolSize));
	        
			JndiMocker.setUpJndiDataSource(oracleUrl, oracleUsername, oraclePassword, oracleServiceName);
			Integer page = 0;
			pageSizeInt = Integer.parseInt(pageSize);
			ProfilesPaginatorDAO profilesPaginator = new ProfilesPaginatorDAO(session, pageSizeInt);
			List<Long> profiles = profilesPaginator.startPage();
			executor.execute(new FixProfilesTask(callculateAll, page++, profiles));
			
			while(profilesPaginator.hasMore()) {
				profiles = profilesPaginator.nextPage();
				executor.execute(new FixProfilesTask(callculateAll, page++, profiles));
				System.out.println("scheduled page:" + page);
			}
			executor.shutdown();
	        while (!executor.isTerminated()) {
	        }
	        logger.info("profiles: "+ profilesCounter);
	        logger.info("investments" + investmentCounter);
		} catch (IOException e) {
			System.out.println("Error while loading properties file");
			e.printStackTrace();
		} catch(Throwable e) {
			e.printStackTrace();
			System.out.println("general error");
		}	finally {
			ManagerBase.closeSession();
			ManagerBase.closeCluster();
		}
	}
}

class FixProfilesTask implements Runnable {
	private static final Logger logger = Logger.getLogger(FixProfilesTask.class);
	List<Long> profiles;
	int id;
	boolean full;
	
	public FixProfilesTask( boolean full, int id, List<Long> profiles) {
		this.id = id;
		this.profiles = profiles;
		this.full = full;
	}
	
	public void run() {
		logger.info("Task [" + id + "] Start working " +profiles.size() );
		long startTime = System.nanoTime();
		int i =0;
		int k =0;
		try {		
			for(i =0 ;i<profiles.size();i++) {
				FixProfiles.profilesCounter++;

				if(full) {
					UserStatisticsManager.updateUserStatistics(profiles.get(i));
				}
				long userId = profiles.get(i);
				List<CopiedInvestment> cis = InvestmentsManagerBase.getCopiedInvestmentsForUser(userId);
				for(k =0;k<cis.size();k++) {
					CopiedInvestment  ci = cis.get(k);
					ProfilesMigrationManager.updateCopiedInvestmentStats(ci);
					FixProfiles.investmentCounter++;
				}
				if(i%(FixProfiles.pageSizeInt/2)==0) {
					logger.info("Task [" + id + "] at " + i + " of " + profiles.size());
				}
			}
		} catch (Exception e) {
			logger.error("Error while processing Task [" + id + "] at profile N: " + i +" at inv N: " +k, e);
			try {
				PrintWriter writer = new PrintWriter(" Task_" + id+".txt", "UTF-8");
				writer.println("profileN="+i);
				writer.println("investmentN="+k);
				writer.close();
				return;
			} catch (Exception ex) {
				logger.error("Can't create log", ex);
			}
		}
		long endTime = System.nanoTime();
		long timeNanos = endTime - startTime;
		logger.info("Task [" + id + "] Done; Time:" +TimeUnit.NANOSECONDS.toSeconds(timeNanos)+" seconds");
	}
}