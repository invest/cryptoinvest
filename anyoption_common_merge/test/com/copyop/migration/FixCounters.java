package com.copyop.migration;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;

import com.copyop.common.dao.ProfilesPaginatorDAO;
import com.copyop.common.managers.FixCountersManager;
import com.copyop.common.managers.ManagerBase;
import com.datastax.driver.core.Session;

 
public class FixCounters {
	static volatile int totalCounter = 0;
	public static void main(String args[]) {
		
		Properties props = new Properties();
		String propFileName = "copyopMigration.properties";
 
		InputStream inputStream = FixProfiles.class.getClassLoader().getResourceAsStream(propFileName);
		if (inputStream == null) {
			System.out.println("Properties file"+ propFileName+ " Not found in class path !");
		}		
		try {
			props.load(inputStream);
			String cassandraUrlsCsv		= props.getProperty("cassandra.urls.csv");
			String cassandraKeyspace	= props.getProperty("cassandra.keyspace");
			String threadPoolSize		= props.getProperty("threadpool.size");
			String pageSize				= props.getProperty("page.size");

			boolean valid = (cassandraUrlsCsv != null) && 
							(cassandraKeyspace != null) &&
							(threadPoolSize != null) && 
							(pageSize != null);
			
			if(!valid) {
				System.out.println("Unsuficient info in properties file :");
				System.out.println(props);
			}
			ExecutorService executor = Executors.newFixedThreadPool(Integer.parseInt(threadPoolSize));

			ManagerBase.init(cassandraUrlsCsv.split(","), cassandraKeyspace);
			Session session = ManagerBase.getSession();
			Integer page = 0;
			ProfilesPaginatorDAO profilesPaginator = new ProfilesPaginatorDAO(session, Integer.parseInt(pageSize));
			List<Long> profiles = profilesPaginator.startPage();
			executor.execute(new FixCountersTask(page++, profiles));
			
			while(profilesPaginator.hasMore()) {
				profiles = profilesPaginator.nextPage();
				executor.execute(new FixCountersTask(page++, profiles));
				System.out.println("scheduled page:" + page);
			}
			executor.shutdown();
	        while (!executor.isTerminated()) {
	        }
	        System.out.println(totalCounter);
			
		} catch (IOException e) {
			System.out.println("Error while loading propertis file");
			e.printStackTrace();
		} catch(Throwable e) {
			e.printStackTrace();
			System.out.println("general error");
		}	finally {
			ManagerBase.closeSession();
			ManagerBase.closeCluster();
		}
	}
}	
	 class FixCountersTask implements Runnable {
		private static final Logger logger = Logger.getLogger(FixCountersTask.class);
		List<Long> profiles;
		int id;
		public FixCountersTask( int id, List<Long> profiles) {
			this.id = id;
			this.profiles = profiles;
		}
		
		public void run() {
			logger.info("Task [" + id + "] Start working");
			long startTime = System.nanoTime();
			for(int i =0 ;i<profiles.size();i++) {
				FixCounters.totalCounter++;
				FixCountersManager.counterFix(profiles.get(i));
			}
			long endTime = System.nanoTime();
			long timeNanos = endTime - startTime;
			logger.info("Task [" + id + "] Done; Time:" +TimeUnit.NANOSECONDS.toSeconds(timeNanos)+" seconds");
		}
	}

