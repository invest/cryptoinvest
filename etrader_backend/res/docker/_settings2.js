var server = 'production';

settings.jsonLink = '/AnyoptionService/';
settings.backendJsonLink = '/BackendService/';
settings.commonJsonLink = '/BackendService/CommonService/';
settings.backendFileUploadLink = '/UploadBEDocumentsService/';
settings.commonFileUploadLink = '/UploadDocumentsService/';
settings.backendPromotionsUploadLink = '/UploadBannerSliderImageService/';
settings.backendPromotionsImagesLink = '/bannerSliderImages/';
settings.jsonImagegLink = '/';

settings.isLive = true;
settings.logIt_type = 4;