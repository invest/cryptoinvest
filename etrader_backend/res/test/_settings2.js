var server = 'test';

settings.jsonLink = '/anybackend/AnyoptionService/';
settings.backendJsonLink = '/anybackend/BackendService/';
settings.commonJsonLink = '/BackendService/CommonService/';
settings.backendFileUploadLink = '/anybackend/UploadBEDocumentsService/';
settings.commonFileUploadLink = '/anybackend/UploadDocumentsService/';
settings.backendPromotionsUploadLink = '/anybackend/UploadBannerSliderImageService/';
settings.backendPromotionsImagesLink = '/anybackend/bannerSliderImages/';
settings.jsonImagegLink = '/anybackend/';

settings.isLive = false;
settings.logIt_type = 4;