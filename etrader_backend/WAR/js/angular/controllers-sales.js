backendApp.controller('SalesCtr', ['$rootScope', '$scope', '$http', 'Pagination', function($rootScope, $scope, $http, Pagination) {

}]);
	
backendApp.controller('SalesInvestmentsCtr', ['$rootScope', '$scope', '$http', '$timeout', 'Pagination', function($rootScope, $scope, $http, $timeout, Pagination) {
	$scope.ready = false;
	$scope.action = '';
	$scope.filter = {};
	$scope.filter.startDate = 0;
	$scope.filter.endDate = 0;
	
	$scope.salesInvestments = [];
	$scope.totalAmount = '';
	
	$scope.selectedRepresentative = null;
	
	$scope.isManager = false;
	
	$scope.changePage = function(page){
		$scope.getAll(page);
	}
	$scope.resultsPerPage = 20;
	$scope.pagination = new Pagination.InstanceClass({callback: $scope.changePage, hideGroupNavigation: true});
	
	$scope.selectedFilter = null;
	
	
	$scope.filter.dateRangeDays = 32;
	$scope.filter.openDates = {
        startDate: false,
        endDate: false
    };
	$scope.openCalendar = function(e, date) {
        $scope.filter.openDates[date] = true;
		if (e.target.tagName.toLowerCase() == 'input') {
			$timeout(function() {
				e.target.focus();
			}, 100);
		}
    };
	$scope.initDates = function(){
		$scope.currentDate = new Date();
		$scope.hoursOffset = $scope.currentDate.getTimezoneOffset()/60 + $scope.writer.utcOffsetHours;
		$scope.currentDate.setHours($scope.currentDate.getHours() + $scope.hoursOffset);
		
		$scope.defaultStartDate = new Date();
		$scope.defaultStartDate.setDate(1);
		
		$scope.filter.startDate = $scope.defaultStartDate;
		$scope.filter.endDate = $scope.currentDate;
		if($scope.filter.endDate.getTime() - $scope.filter.startDate.getTime() > ($scope.filter.dateRangeDays - 1)*24*60*60*1000){
			$scope.filter.startDate.setTime($scope.filter.endDate.getTime() - ($scope.filter.dateRangeDays - 1)*24*60*60*1000);
		}
		
		$scope.filter.minDate = new Date();
		$scope.filter.minDate.setTime($scope.filter.endDate.getTime() - 20*365*24*60*60*1000 + $scope.hoursOffset*60*60*1000);
		$scope.filter.maxDate = new Date();
		$scope.filter.maxDate.setTime(new Date().getTime() + $scope.hoursOffset*60*60*1000);
	}
	$scope.initDates();
	
	$scope.dateOptions = {
        showWeeks: false,
        startingDay: 0
    };
	
	
	
	$scope.initFiltersOnReady = function(){
		$scope.filter.representatives = [{id: -1, name: $rootScope.getMsgs('all')}];
		$scope.filter.representative = $scope.filter.representatives[0];
	}
	
	//Init the controller
	var initDependencies = {name: 'initList', dependencies: {getSettings: false}};
	$scope.initScreenCtr($scope, initDependencies, function(){$scope.getSettings('initList');});
	
	$scope.checkAction = function(){
		$scope.getAll();
	}
	
	$scope.getSettings = function(dependencyName){
		var MethodRequest = {};
		$http.post(settings.backendJsonLink + 'SalesInvestmentsManagerServices/getSettings', MethodRequest)
			.then(function(data) {
				$rootScope.resetGlobalErrorMsg();
				for(var i = 0; i < data.permissionsList.length; i++){
					$rootScope.permissions.allow(data.permissionsList[i]);
					if(data.permissionsList[i] == "isManager"){
						$scope.isManager = true;
					}
				}
				if(data.screenFilters){
					$rootScope.fillFilter({arr: $scope.filter.representatives, data: data, filter: 'writers'});
				}
			})
			.catch(function(data) {
				$rootScope.handleErrors(data, 'getSettings')
			})
			.finally(function() {
				$rootScope.markDependencyDone($scope, dependencyName, 'getSettings');
			})
	}
	
	$scope.getAll = function(page){
		if((page = $scope.pagination.validatePage(page)) === false){
			return;
		}
		
		showLoading();
		
		var service = 'SalesInvestmentsRepresentativesServices/getSalesInvestmentsRep';
		if($scope.isManager && $scope.filter.representative.id < 0){
			service = 'SalesInvestmentsManagerServices/getSalesInvestmentsManager';
		}
		
		$scope.selectedRepresentative = $scope.filter.representative.id >= 0 ? $scope.filter.representative : null;
		
		var methodRequest = {};
		methodRequest.pageNumber = page;
		methodRequest.rowsPerPage = $scope.resultsPerPage;
		methodRequest.fromDate = $rootScope.dateParse({date: $scope.filter.startDate, startOfDay: true, timestamp: true});
		methodRequest.toDate = $rootScope.dateParse({date: $scope.filter.endDate, endOfDay: true, timestamp: true});
		if($scope.isManager && $scope.filter.representative.id >= 0){
			methodRequest.writerId = $scope.filter.representative.id;
		}
		
		$http.post(settings.backendJsonLink + service, methodRequest)
			.then(function(data) {
				$rootScope.resetGlobalErrorMsg();
				$scope.totalAmount = data.totalAmount;
				$scope.salesInvestments = data.salesInvestments;
				for(var i = 0; i < $scope.salesInvestments.length; i++){
					$scope.salesInvestments[i].traderValue = (parseInt($scope.salesInvestments[i].traderValue*100))/100;
				}
				$scope.pagination.setPages(page, data.totalCount, $scope.resultsPerPage);
			})
			.catch(function(data) {
				$rootScope.handleErrors(data, 'getAll')
			})
			.finally(function() {
				hideLoading();
			})
	}
}]);

backendApp.controller('CountryConfigurationCtr', ['$rootScope', '$scope', '$http', 'SingleSelect', 'Pagination', function($rootScope, $scope, $http, SingleSelect, Pagination) {
	$scope.ready = false;
	$scope.action = '';
	$scope.filter = {};
	$scope.filter.countries = [];
	$scope.countriesGroupTypes = {};
	$scope.countriesGroupTiers = {};
	
	$scope.countries = [];
	
	$scope.selectedFilter = null;
	
	
	$scope.initFiltersOnReady = function(){
		//
	}
	
	//Init the controller
	var initDependencies = {name: 'initList', dependencies: {getCountryScreenSettings: false}};
	$scope.initScreenCtr($scope, initDependencies, function(){$scope.getCountryScreenSettings('initList');});
	
	$scope.checkAction = function(){
		$scope.getCountryGroups();
	}
	
	$scope.getCountryScreenSettings = function(dependencyName){
		var MethodRequest = {};
		$http.post(settings.backendJsonLink + 'CountryServices/getCountryScreenSettings', MethodRequest)
			.then(function(data) {
				$rootScope.resetGlobalErrorMsg();
				for(var i = 0; i < data.permissionsList.length; i++){
					$rootScope.permissions.allow(data.permissionsList[i]);
				}
				if(data.screenFilters){
					$rootScope.fillFilter({arr: $scope.filter.countries, data: data, filter: 'countries'});
					$scope.countriesGroupTypes = data.screenFilters.countries_group_types;
					$scope.countriesGroupTiers = data.screenFilters.countries_group_tiers;
				}
			})
			.catch(function(data) {
				$rootScope.handleErrors(data, 'getCountryScreenSettings')
			})
			.finally(function() {
				$rootScope.markDependencyDone($scope, dependencyName, 'getCountryScreenSettings');
			})
	}
	
	$scope.getCountryGroups = function(page){
		showLoading();
		
		var methodRequest = {};
		$http.post(settings.backendJsonLink + 'CountryServices/getCountryGroups', methodRequest)
			.then(function(data) {
				$rootScope.resetGlobalErrorMsg();
				$scope.countries = data.countryGroups;
			})
			.catch(function(data) {
				$rootScope.handleErrors(data, 'getCountryGroups')
			})
			.finally(function() {
				hideLoading();
			})
	}
	
	$scope.updateCountryGroup = function(country, form){
		showLoading();
		if(form){
			form.$setPristine();
		}
		var methodRequest = {};
		methodRequest.countryGroup = country;
		$http.post(settings.backendJsonLink + 'CountryServices/updateCountryGroup', methodRequest)
			.then(function(data) {
				$rootScope.resetGlobalErrorMsg();
			})
			.catch(function(data) {
				$rootScope.handleErrors(data, 'updateCountryGroup')
			})
			.finally(function() {
				hideLoading();
			})
	}
}]);

backendApp.controller('CallStatusCtr', ['$rootScope', '$scope', '$http', '$timeout', 'Pagination', function($rootScope, $scope, $http, $timeout, Pagination) {
	$scope.ready = false;
	$scope.action = '';


	$scope.filter = {};
	$scope.filter.startDate = 0;
	$scope.filter.endDate = 0;
	
	$scope.representatives = [];
	
	$scope.changePage = function(page){
		$scope.getSalesCallStatusRepresentatives(page);
	}
	$scope.resultsPerPage = 20;
	$scope.pagination = new Pagination.InstanceClass({callback: $scope.changePage, hideGroupNavigation: true});
	
	$scope.selectedFilter = null;
	
	
	$scope.filter.dateRangeDays = 32;
	$scope.filter.openDates = {
        startDate: false,
        endDate: false
    };
	$scope.openCalendar = function(e, date) {
        $scope.filter.openDates[date] = true;
		if (e.target.tagName.toLowerCase() == 'input') {
			$timeout(function() {
				e.target.focus();
			}, 100);
		}
    };
	$scope.initDates = function(){
		$scope.currentDate = new Date();
		$scope.hoursOffset = $scope.currentDate.getTimezoneOffset()/60 + $scope.writer.utcOffsetHours;
		$scope.currentDate.setHours($scope.currentDate.getHours() + $scope.hoursOffset);
		
		$scope.defaultStartDate = new Date();
		$scope.defaultStartDate.setDate(1);
		
		$scope.filter.startDate = $scope.defaultStartDate;
		$scope.filter.endDate = $scope.currentDate;
		if($scope.filter.endDate.getTime() - $scope.filter.startDate.getTime() > ($scope.filter.dateRangeDays - 1)*24*60*60*1000){
			$scope.filter.startDate.setTime($scope.filter.endDate.getTime() - ($scope.filter.dateRangeDays - 1)*24*60*60*1000);
		}
		
		$scope.filter.minDate = new Date();
		$scope.filter.minDate.setTime($scope.filter.endDate.getTime() - 20*365*24*60*60*1000 + $scope.hoursOffset*60*60*1000);
		$scope.filter.maxDate = new Date();
		$scope.filter.maxDate.setTime(new Date().getTime() + $scope.hoursOffset*60*60*1000);
	}
	$scope.initDates();
	
	$scope.dateOptions = {
        showWeeks: false,
        startingDay: 0
    };
	
	
	
	$scope.initFiltersOnReady = function(){
		$scope.filter.representatives = [{id: -1, name: $rootScope.getMsgs('all')}];
		$scope.filter.representative = $scope.filter.representatives[0];

		$scope.filter.userRanks = [{id: -1, name: $rootScope.getMsgs('all')}];
		$scope.filter.userRank = $scope.filter.userRanks[0];

		$scope.filter.skins = [{id: -1, name: $rootScope.getMsgs('all')}];
		$scope.filter.skin = $scope.filter.skins[0];
	}
	
	//Init the controller
	var initDependencies = {name: 'initList', dependencies: {getSalesCallStatusScreenSettings: false}};
	$scope.initScreenCtr($scope, initDependencies, function(){$scope.getSalesCallStatusScreenSettings('initList');});
	
	$scope.checkAction = function(){
		
	}
	
	$scope.getSalesCallStatusScreenSettings = function(dependencyName){
		var MethodRequest = {};
		$http.post(settings.backendJsonLink + 'SalesCallStatusManagerServices/getSalesCallStatusScreenSettings', MethodRequest)
			.then(function(data) {
				$rootScope.resetGlobalErrorMsg();
				for(var i = 0; i < data.permissionsList.length; i++){
					$rootScope.permissions.allow(data.permissionsList[i]);
				}
				if(data.screenFilters){
					$rootScope.fillFilter({arr: $scope.filter.representatives, data: data, filter: 'representatives'});
					$rootScope.fillFilter({arr: $scope.filter.userRanks, data: data, filter: 'userRanks', translate: true});
					$rootScope.fillFilter({arr: $scope.filter.skins, data: data, filter: 'skins'});
				}
			})
			.catch(function(data) {
				$rootScope.handleErrors(data, 'getSalesCallStatusScreenSettings')
			})
			.finally(function() {
				$rootScope.markDependencyDone($scope, dependencyName, 'getSalesCallStatusScreenSettings');
			})
	}
	
	$scope.getSalesCallStatusRepresentatives = function(page){
		if((page = $scope.pagination.validatePage(page)) === false){
			return;
		}
		
		showLoading();
		
		var service = 'SalesCallStatusManagerServices/getSalesCallStatusRepresentatives';
		
		var methodRequest = {};
		methodRequest.pageNumber = page;
		methodRequest.rowsPerPage = $scope.resultsPerPage;
		methodRequest.fromDate = $rootScope.dateParse({date: $scope.filter.startDate, startOfDay: true, timestamp: true});
		methodRequest.toDate = $rootScope.dateParse({date: $scope.filter.endDate, endOfDay: true, timestamp: true});
		
		$scope.filter.userRank.id > 0 ? methodRequest.userRank = $scope.filter.userRank.id : '';
		$scope.filter.skin.id > 0 ? methodRequest.skinId = $scope.filter.skin.id : '';
		if($rootScope.permissions.get('callStatus_manager') && $scope.filter.representative.id > 0){
			methodRequest.writerId = $scope.filter.representative.id;
		}else if(!$rootScope.permissions.get('callStatus_manager')){
			methodRequest.writerId = $rootScope.writer.id;
		}

		$http.post(settings.backendJsonLink + service, methodRequest)
			.then(function(data) {
				$rootScope.resetGlobalErrorMsg();
				$scope.representatives = data.representatives;
				var totalCount = 0;
				if(data.representatives && data.representatives[0]){
					totalCount = data.representatives[0].totalCount;
				}
				$scope.pagination.setPages(page, totalCount, $scope.resultsPerPage);
			})
			.catch(function(data) {
				$rootScope.handleErrors(data, 'getSalesCallStatusRepresentatives')
			})
			.finally(function() {
				hideLoading();
			})
	}
}]);
