backendApp.controller('forgotPasswordController', ['$rootScope', '$scope', '$http', function($rootScope, $scope, $http) {
	$scope.form = {};
	$scope.submitDisabled = false;
	$scope.baseUrl = baseUrl;
	$scope.resetPassword = function(_form) {
		if (_form.$valid) {
			$scope.submitDisabled = true;
			var WriterResetPasswordMethodRequest = {
				email: $scope.form.email,
				userName: $scope.form.userName
			}
			
			if (!$scope.loading) {
				$scope.loading = true;
				$http.post(settings.backendJsonLink + 'WriterServices/resetPassword', WriterResetPasswordMethodRequest)
					.then(function(data) {
						$rootScope.resetGlobalErrorMsg();
						$('#globalErrorField').html($rootScope.getMsgs('password-reset-msgs'));
						$scope.submitDisabled = true;
					})
					.catch(function(data) {
						$scope.submitDisabled = false;
						$scope.handleErrors(data, 'getForgetPasswordContacts');
					})
				.finally(function() {
					$scope.loading = false;
				})
			}
		}
	}
}]);