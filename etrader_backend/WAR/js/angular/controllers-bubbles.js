backendApp.controller('BubblesCtr', ['$rootScope', '$scope', '$http', function($rootScope, $scope, $http) {
	$scope.form = {};
	$scope.form.just_updated = false;
	$scope.form.bubbles_on_off = "on";
	$scope.form.marketsData = [];
	$scope.form.market = {id: '0', name: ''};
	$scope.form.market_options = [{id: '0', name: ''}];
	
	$scope.initWatch = $scope.$watch(function(){return $rootScope.ready;}, function(){
		if($rootScope.ready){
			$scope.initWatch();
			$scope.init();
		}
	});
	
	$scope.init = function(){
		$scope.loadMarketData();
	}
	
	$scope.loadMarketData = function(){
		$scope.form.marketsData = [];
		$scope.form.market_options = [{id: '0', name: ''}];
		var MethodRequest = {};
		$http.post(settings.jsonLink + 'getBubblesMarkets', MethodRequest)
			.then(function(data) {
				for(var i = 0; i < data.markets.length; i++){
					$scope.form.market_options.push({id: data.markets[i].id, name: data.markets[i].name});
					var onOff = data.markets[i].suspended == 0 ? "on" : "off";
					$scope.form.marketsData[data.markets[i].id] = {market_on_off: onOff};
				}
			})
			.catch(function(data) {
				//
			})
	}
	
	
	$scope.saveBubblesStatus = function(){
		console.log($scope.form.bubbles_on_off);
		var isAllOn = $scope.form.bubbles_on_off == 'on' ? true : false;
		var SuspendMarketsMethodRequest = {'isOn' : isAllOn, 'isAllOn' : isAllOn, 'marketId' : 0}
		$http.post(settings.jsonLink + 'suspendBubblesMarkets', SuspendMarketsMethodRequest)
			.then(function(data) {
				$scope.form.just_updated = true;
			})
			.catch(function(data) {
				//
			})
	}
	
	$scope.saveMarketData = function(){
		console.log($scope.form.marketsData[$scope.form.market.id]);
		var isAllOn = $scope.form.marketsData[$scope.form.market.id].market_on_off == 'on' ? true : false;
		var SuspendMarketsMethodRequest = {'isOn' : isAllOn, 'isAllOn' : isAllOn, 'marketId' : $scope.form.market.id}
		$http.post(settings.jsonLink + 'suspendBubblesMarkets', SuspendMarketsMethodRequest)
			.then(function(data) {
				$scope.form.just_updated = true;
			})
			.catch(function(data) {
				//
			})
	}
	
	$scope.removeMessage = function(){
		$scope.form.just_updated = false;
	}
}]);