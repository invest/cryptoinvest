backendApp.controller('UsersCtr', ['$rootScope', '$scope', '$http', 'Pagination', function($rootScope, $scope, $http, Pagination) {

}]);
	
backendApp.controller('FilesCtr', ['$sce', '$rootScope', '$scope', '$http', '$filter', '$upload', '$timeout', '$uibModal', 'utilsService', 'SingleSelect', 'Pagination', function($sce, $rootScope, $scope, $http, $filter, $upload, $timeout, $uibModal, utilsService, SingleSelect, Pagination) {
	var FILE_STATUS_NEEDED = 1;
	var FILE_STATUS_NOT_NEEDED = 5;
	var FILE_TYPE_GBG = 36;
	
	$scope.ready = false;
	$scope.action = '';
	$scope.isSingle = true;
	if($scope.filesList){
		$scope.listFileId = $scope.filesList.fileId;
		$scope.userId = $scope.filesList.userId;
		$scope.userSkinId = $scope.filesList.skinId;
		$scope.showPreviousNext = $scope.filesList.showPreviousNext;
		$scope.saveCallback = $scope.filesList.saveCallback;
	}
	
	$scope.selectedFile = {
		isCurrentDisable: true
	};
	
	$scope.filter = {};
	$scope.filter.fileTypes = [];
	$scope.filter.fileType = null;
	
	$scope.filter.notNeeded = false;
	
	$scope.keesingStatuses = {
		0: 'full',
		1: 'difference',
		2: 'updated',
		3: 'dismissed'
	}
	$scope.keesingSelectedFlags = {};
	
	$scope.filter.creditCards = new SingleSelect.InstanceClass({aggregateOption: {id: 0, name: ''}, externalModel: {objCallback: function(){return $scope.selectedFile}, key: 'ccId'}});
	
	$scope.filter.countries = new SingleSelect.InstanceClass({aggregateOption: {id: -1, name: ''}, externalModel: {objCallback: function(){return $scope.selectedFile}, key: 'countryId'}});
	
	$scope.fileId = 0;
	$scope.currentFileType = 0;
	$scope.timeCreated = '';
	
	$scope.filter.expDate = 0;
	$scope.filter.openDates = {
        expDate: false,
		userDOB: false
    };
	$scope.openCalendar = function(e, date) {
		$scope.initDates(date);
        $scope.filter.openDates[date] = true;
		if (e.target.tagName.toLowerCase() == 'input') {
			$timeout(function() {
				e.target.focus();
			}, 100);
		}
    };
	$scope.defaultDate = new Date();
	$scope.initDates = function(input){
		if(!$scope.selectedFile[input]){
			$scope.selectedFile[input] = $scope.defaultDate;
		}
	}
	$scope.dateOptions = {
        showWeeks: false,
        startingDay: 0
    };
	
	$scope.hasPrevious = true;
	$scope.hasNext = true;
	
	
	$scope.previewControls = {};
	$scope.previewControls.zoomLevels = [];
	$scope.previewControls.rotationStates = [];
	
	$scope.initFiltersOnReady = function(){
		$scope.previewControls.zoomLevels.push({id: 0, name: $rootScope.getMsgs('auto'), value: 'zoom-auto'});
		$scope.previewControls.zoomLevels.push({id: 1, name: '25%', value: 'zoom-25'});
		$scope.previewControls.zoomLevels.push({id: 2, name: '50%', value: 'zoom-50'});
		$scope.previewControls.zoomLevels.push({id: 3, name: '75%', value: 'zoom-75'});
		$scope.previewControls.zoomLevels.push({id: 4, name: '100%', value: 'zoom-100'});
		$scope.previewControls.zoomLevels.push({id: 5, name: '150%', value: 'zoom-150'});
		$scope.previewControls.zoomLevels.push({id: 6, name: '200%', value: 'zoom-200'});
		$scope.previewControls.zoomLevels.push({id: 7, name: '400%', value: 'zoom-400'});
		$scope.previewControls.zoom = $scope.previewControls.zoomLevels[0];
		
		$scope.previewControls.rotationStates.push('rotate-auto');
		$scope.previewControls.rotationStates.push('rotate-90');
		$scope.previewControls.rotationStates.push('rotate-180');
		$scope.previewControls.rotationStates.push('rotate-270');
		$scope.previewControls.rotationState = 0;
	}
	
	$scope.previewRotate = function(right){
		if(right){
			if($scope.previewControls.rotationState == ($scope.previewControls.rotationStates.length - 1)){
				$scope.previewControls.rotationState = 0;
			}else{
				$scope.previewControls.rotationState++;
			}
		}else{
			if($scope.previewControls.rotationState == 0){
				$scope.previewControls.rotationState = ($scope.previewControls.rotationStates.length - 1);
			}else{
				$scope.previewControls.rotationState--;
			}
		}
		//Fix a bug where overflow doesn't create a scroll after rotation
		$timeout(function(){
				var zoom = $scope.previewControls.zoom;
				$scope.previewControls.zoom = null;
				$timeout(function(){
						$scope.previewControls.zoom = zoom;
					}, 0);
			}, 0);
	}
	$scope.previewImageClass = function(){
		var result = '';
		if($scope.previewControls.zoom){
			result += ' ' + $scope.previewControls.zoom.value;
		}else{
			result += ' hidden';
		}
		if($scope.previewControls.rotationStates[$scope.previewControls.rotationState]){
			result += ' ' + $scope.previewControls.rotationStates[$scope.previewControls.rotationState];
		}
		return result;
	}
	
	//Init the controller
	var initDependencies = {name: 'initList', dependencies: {getFileScreenSettings: false}};
	$scope.initScreenCtr($scope, initDependencies, function(){$scope.getFileScreenSettings('initList');});
	
	$scope.checkAction = function(){
		if($rootScope.$stateParams.fileId){
			$scope.action = 'edit';
			$scope.fileId = $rootScope.$stateParams.fileId;
			$scope.getFile($scope.fileId);
		}else if($scope.popupFileId){
			$scope.action = 'edit';
			$scope.userId = $scope.popupUserId;
			$scope.fileId = $scope.popupFileId;
			$scope.getFile($scope.fileId);
		}else if($scope.listFileId){
			$scope.action = 'edit';
			$scope.isSingle = false;
			$scope.fileId = $scope.listFileId;
			$scope.getFile($scope.fileId);
		}else if($rootScope.$state.includes("ln.users.files.add")){
			$scope.action = 'add';
			$scope.fileId = 0;
			$scope.filter.fileType = $scope.filter.fileTypes[0];
			$scope.initSelectedFile();
			$scope.getUserCC();
		}else if($scope.popupFileAdd){
			$scope.action = 'add';
			$scope.userId = $scope.popupUserId;
			$scope.fileId = 0;
			$scope.filter.fileType = $scope.filter.fileTypes[0];
			$scope.initSelectedFile();
			$scope.getUserCC();
		}else{
			$scope.action = '';
			$scope.fileId = 0;
			$scope.selectedFile = {};
			$scope.keesingSelectedFlags = {};
		}
	}
	
	$scope.getFileScreenSettings = function(dependencyName){
		var methodRequest = {
			skinId: (!isUndefined($scope.userSkinId)) ? $scope.userSkinId : userSkinId
		};
		$http.post(settings.backendJsonLink + 'FilesServices/getFileScreenSettings', methodRequest)
			.then(function(data) {
				$rootScope.resetGlobalErrorMsg($scope.localErrorMsgs);
				$scope.showControl = data.showControl;
				for(var i = 0; i < data.permissionsList.length; i++){
					$rootScope.permissions.allow(data.permissionsList[i]);
				}
				angular.forEach(data['fileRejectReasons'], function(value, key) {//just in case "not.rejected" is not setted in the database
					if (value[0][0] != 'not.rejected') {
						value.push({0: "not.rejected"});
					}
				})
				
				var fileTypes = {};
				for(var el in data['fileTypes']){
					if(data['fileTypes'].hasOwnProperty(el)){
						fileTypes[el] = data['fileTypes'][el];
						if(data['fileRejectReasons'][el]){
							var arr = [];
							for(var i = 0; i < data['fileRejectReasons'][el].length; i++){
								for(var subEl in data['fileRejectReasons'][el][i]){
									if(data['fileRejectReasons'][el][i].hasOwnProperty(subEl)){
										var name = data['fileRejectReasons'][el][i][subEl];
										arr.push({id: subEl, name: name});
									}
								}
							}
							arr = $rootScope.translateJsonKeyInArray(arr, 'name');
							fileTypes[el]['rejectReasons'] = new SingleSelect.InstanceClass({options: arr, externalModel: {objCallback: function(){return $scope.selectedFile}, key: 'rejectReason'}});
							fileTypes[el]['controlRejectReasons'] = new SingleSelect.InstanceClass({options: arr, externalModel: {objCallback: function(){return $scope.selectedFile}, key: 'controlRejectReason'}});
						}
						if(data['filesAdditionalInfo'][el]){
							fileTypes[el]['filesAdditionalInfo'] = data['filesAdditionalInfo'][el];
						}
						$scope.filter.fileTypes.push(fileTypes[el]);
					}
				}
				$scope.filter.fileTypes = $rootScope.sortFilter($rootScope.translateJsonKeyInArray($scope.filter.fileTypes, 'name'));
				
				if(data.screenFilters){
					$scope.filter.countries.fillOptions(data.screenFilters.countries);
				}
			})
			.catch(function(data) {
				$rootScope.handleErrors(data, 'getFileScreenSettings', $scope.localErrorMsgs)
			})
			.finally(function() {
				$rootScope.markDependencyDone($scope, dependencyName, 'getFileScreenSettings');
			})
	}
	
	
	$scope.getUserCC = function(){
		showLoading();
		var methodRequest = {};
		methodRequest.userId = $scope.userId;
		$http.post(settings.backendJsonLink + 'FilesServices/getUserCC', methodRequest)
			.then(function(data) {
				$rootScope.resetGlobalErrorMsg($scope.localErrorMsgs);
				var ccOptions = [];
				for(creditCard in data.creditCards){
					if(data.creditCards.hasOwnProperty(creditCard)){
						var name = '';
						var expiryDate = '';
						for(details in data.creditCards[creditCard]){
							if(data.creditCards[creditCard].hasOwnProperty(details)){
								name = details;
								expiryDate = data.creditCards[creditCard][details];
							}
						}
						ccOptions.push({id: creditCard, name: name, expiryDate: expiryDate});
					}
				}
				$scope.filter.creditCards.fillOptions(ccOptions);
			})
			.catch(function(data) {
				$rootScope.handleErrors(data, 'getUserCC', $scope.localErrorMsgs)
			})
			.finally(function() {
				hideLoading();
			})
	}
	
	
	$scope.initSelectedFile = function(){
		$scope.fileName = '';
		$scope.filter.currentIsControlReject = false;
		$scope.filter.currentIsSupportReject = false;
		$scope.selectedFile = {
			isCurrentDisable: true
		};
		/*if($scope.fileId){
			$scope.selectedFile.id = $scope.fileId;
		}*/
		$scope.fileId = 0;
		$scope.selectedFile.id = 0;
		$scope.selectedFile.ccId = $scope.filter.creditCards.options[0].id;
		$scope.selectedFile.rejectReason = 0;
		$scope.selectedFile.controlRejectReason = 0;
		$scope.selectedFile.ksStatusId = 0;
		$scope.filter.notNeeded = false;
	}
	
	
	$scope.getFile = function(fileId, isPreviousNext){
		showLoading();
		$scope.getUserFields();
		var methodRequest = {};
		methodRequest.file = {};
		methodRequest.file.id = fileId;
		methodRequest.previousNext = 0;
		if(isPreviousNext){
			methodRequest.previousNext = isPreviousNext;
		}else{
			$scope.hasPrevious = true;
			$scope.hasNext = true;
		}
		methodRequest.isLoadedFile = false;
		
		$http.post(settings.backendJsonLink + 'FilesServices/getFile', methodRequest)
			.then(function(data) {
				$rootScope.resetGlobalErrorMsg($scope.localErrorMsgs);
				if(isPreviousNext == 1){
					$scope.hasNext = true;
				}else{
					$scope.hasPrevious = true;
				}
				
				$scope.selectedFile = data.file;
				$scope.fileId = data.file.id;
				
				if ($scope.selectedFile.fileName == null || $scope.selectedFile.isCurrent) {
					$scope.selectedFile.isCurrentDisable = true;
				} else {
					$scope.selectedFile.isCurrentDisable = false;
				}
				
				if (!isUndefined($scope.selectedFile.filesKeesingResponse) && $scope.selectedFile.filesKeesingResponse.expireDate) {
					$scope.selectedFile.filesKeesingResponse.expireDateDsp = new Date($scope.selectedFile.filesKeesingResponse.expireDate);
				}
				
				if (!isUndefined($scope.selectedFile.filesKeesingMatch) && $scope.selectedFile.filesKeesingMatch.userDOB) {
					$scope.selectedFile.filesKeesingMatch.userDOBUpdate = new Date($scope.selectedFile.filesKeesingMatch.userDOB);
					$scope.selectedFile.filesKeesingMatch.userDOBOld = new Date($scope.selectedFile.filesKeesingMatch.userDOB);
				}
				
				if(parseInt($scope.selectedFile.expDay) && parseInt($scope.selectedFile.expMonth) && parseInt($scope.selectedFile.expYear)){
					$scope.selectedFile.expDate = $scope.defaultDate;
					$scope.selectedFile.expDate.setDate(parseInt($scope.selectedFile.expDay));
					$scope.selectedFile.expDate.setMonth(parseInt($scope.selectedFile.expMonth) - 1);
					$scope.selectedFile.expDate.setYear(parseInt($scope.selectedFile.expYear));
				}
				
				$scope.keesingSelectedFlags = {};
				$scope.clearTempFileData();
				
				$scope.filter.currentIsSupportReject = $scope.selectedFile.isSupportReject;
				$scope.filter.currentIsControlReject = $scope.selectedFile.isControlReject;
				
				var ccOptions = [];
				for(creditCard in data.creditCards){
					if(data.creditCards.hasOwnProperty(creditCard)){
						var name = '';
						var expiryDate = '';
						for(details in data.creditCards[creditCard]){
							if(data.creditCards[creditCard].hasOwnProperty(details)){
								name = details;
								expiryDate = data.creditCards[creditCard][details];
							}
						}
						ccOptions.push({id: creditCard, name: name, expiryDate: expiryDate});
					}
				}
				$scope.filter.creditCards.fillOptions(ccOptions);
				
				$scope.filter.fileType = $rootScope.getFilterElById($scope.filter.fileTypes, data.file.fileTypeId);
				
				if($scope.filter.countries.getOptionById(data.file.countryId)){
					$scope.filter.countries.setModel($scope.filter.countries.getOptionById(data.file.countryId));
				}
				
				$scope.fileName = data.file.fileName;
				$scope.fileExtension = '';
				if(data.file.fileName && data.file.fileName.lastIndexOf('.') != -1){
					$scope.fileExtension = data.file.fileName.slice(data.file.fileName.lastIndexOf('.') + 1);
				}
				$scope.fileIsImage = false;
				if($scope.fileExtension.toLocaleLowerCase() == 'jpg' || $scope.fileExtension.toLocaleLowerCase() == 'jpeg' || $scope.fileExtension.toLocaleLowerCase() == 'png' || $scope.fileExtension.toLocaleLowerCase() == 'gif' || $scope.fileExtension.toLocaleLowerCase() == 'bmp'){
					$scope.fileIsImage = true;
				}
				$scope.filePath = settings.jsonImagegLink + 'docs/' + data.file.fileName + '?isNewBe=1';
				if(!$scope.fileIsImage){
					if(data.file.fileTypeId == FILE_TYPE_GBG){
						$scope.filePath = settings.jsonImagegLink + 'previewer.html' + $scope.$state.href('ln.users.files-previewers-gbg', {}) + $scope.fileId;
					}else{
						$scope.filePath = $sce.trustAsResourceUrl($scope.filePath);
					}
				}
				
				if($scope.fileForm){
					$scope.fileForm.$setPristine();
				}
			})
			.catch(function(data) {
				if (isPreviousNext != 0 && data.errorCode == errorCodeMap.no_found_file) {
					if (isPreviousNext == 1) {
						$scope.hasPrevious = false;
					} else {
						$scope.hasNext = false;
					}
				} else {
					$rootScope.handleErrors(data, 'getFile', $scope.localErrorMsgs);
				}
			})
			.finally(function() {
				hideLoading();
			})
	}
	
	$scope.getUserFields = function() {
		var UserIdRequest = {
			userId: $scope.userId
		}
		$http.post(settings.backendJsonLink + 'UserFieldsService/getUserAddress', UserIdRequest)
			.then(function(data) {
				$rootScope.resetGlobalErrorMsg($scope.localErrorMsgs);
				$scope.userAddress = data.address;
			})
			.catch(function(data) {
				$rootScope.handleErrors(data, 'getUserAddress', $scope.localErrorMsgs);
			})
	}
	
	$scope.getPreviousFile = function(){
		if($scope.hasPrevious){
			$scope.getFile($scope.fileId, 1);
		}
	}
	$scope.getNextFile = function(){
		if($scope.hasNext){
			$scope.getFile($scope.fileId, 2);
		}
	}
	
	//Update the reject checkbox
	$scope.toggleSupportReject = function(){
		if($scope.filter.fileType.rejectReasons.getId() > 0){
			$scope.selectedFile.isSupportReject = true;
		}else{
			$scope.selectedFile.isSupportReject = false;
		}
		$scope.toggleApproveReject($scope.selectedFile.isSupportReject, 'isApproved');
	}
	
	
	$scope.sendKeesingFile = function(){
		showLoading();
		var methodRequest = {};
		methodRequest.file = {};
		methodRequest.file.id = $scope.fileId;
		$http.post(settings.backendJsonLink + 'FilesServices/sendKeesingFile', methodRequest)
			.then(function(data) {
				$rootScope.resetGlobalErrorMsg($scope.localErrorMsgs);
				$scope.selectedFile.filesKeesingMatch = data.file.filesKeesingMatch;
				$scope.selectedFile.filesKeesingResponse = data.file.filesKeesingResponse;
				$scope.selectedFile.ksStatusDate = data.file.ksStatusDate;
				$scope.selectedFile.ksStatusId = data.file.ksStatusId;
				
				if (!isUndefined($scope.selectedFile.filesKeesingResponse) && $scope.selectedFile.filesKeesingResponse.expireDate) {
					$scope.selectedFile.filesKeesingResponse.expireDateDsp = new Date($scope.selectedFile.filesKeesingResponse.expireDate);
				}
				if (!isUndefined($scope.selectedFile.filesKeesingMatch) && $scope.selectedFile.filesKeesingMatch.userDOB) {
					$scope.selectedFile.filesKeesingMatch.userDOBUpdate = new Date($scope.selectedFile.filesKeesingMatch.userDOB);
				}
			})
			.catch(function(data) {
				$rootScope.handleErrors(data, 'sendKeesingFile', $scope.localErrorMsgs)
			})
			.finally(function() {
				hideLoading();
			})
	}
	
	$scope.keesingGetClass = function(flag){
		if($scope.selectedFile.filesKeesingMatch){
			var selectedFlag = $scope.selectedFile.filesKeesingMatch[flag];
			if(typeof $scope.keesingSelectedFlags[flag] != 'undefined'){
				selectedFlag = $scope.keesingSelectedFlags[flag];
			}
			if(typeof selectedFlag != 'undefined' && typeof $scope.keesingStatuses[selectedFlag] != 'undefined'){
				return $scope.keesingStatuses[selectedFlag];
			}
		}
	}
	$scope.keesingGetFullMatchClass = function(){
		if(($scope.selectedFile.filesKeesingMatch.flagCountry == 0 || $scope.selectedFile.filesKeesingMatch.flagCountry == 2) && 
		($scope.selectedFile.filesKeesingMatch.flagDOB == 0 || $scope.selectedFile.filesKeesingMatch.flagDOB == 2) && 
		($scope.selectedFile.filesKeesingMatch.flagFirstName == 0 || $scope.selectedFile.filesKeesingMatch.flagFirstName == 2) && 
		($scope.selectedFile.filesKeesingMatch.flagGender == 0 || $scope.selectedFile.filesKeesingMatch.flagGender == 2) && 
		($scope.selectedFile.filesKeesingMatch.flagLastName == 0 || $scope.selectedFile.filesKeesingMatch.flagLastName == 2)){
			return 'full-match';
		}
	}
	$scope.keesingUpdate = function(flag){
		if(typeof $scope.keesingSelectedFlags[flag] == 'undefined'){
			$scope.keesingSelectedFlags[flag] = 2;
		}
	}
	$scope.keesingDismiss = function(flag){
		if(typeof $scope.keesingSelectedFlags[flag] == 'undefined'){
			$scope.keesingSelectedFlags[flag] = 3;
		}
	}
	$scope.isKeesingUpdated = function(){
		return Object.keys($scope.keesingSelectedFlags).length > 0;
	}
	
	
	$scope.saveFile = function(){
		$rootScope.resetGlobalErrorMsg($scope.localErrorMsgs);
		$scope.updateUserData();
		showLoading();
		if($scope.selectedFiles && $scope.selectedFiles.length > 0){
			for(var i = 0; i < $scope.selectedFiles.length; i++){
				$scope.progress[i] = -1;
				$scope.start(i);
			}
		}else{
			$scope.saveFileDetails(false);
		}
	}
	
	$scope.saveFileDetails = function(fileUploaded){
		var service = 'FilesServices/updateInsertFile';
		if(!$scope.isSingle){
			service = 'IdsFilesServices/saveIdsFile';
		}
		
		var methodRequest = {};
		methodRequest.previousNext = 0;
		methodRequest.isLoadedFile = fileUploaded ? true : false;
		methodRequest.file = cloneObj($scope.selectedFile);
		methodRequest.file.fileTypeId = $scope.filter.fileType.id;
		if($scope.selectedFile.expDate){
			methodRequest.file.expDate = null;
			methodRequest.file.expDay = $scope.selectedFile.expDate.getDate();
			methodRequest.file.expMonth = $scope.selectedFile.expDate.getMonth() + 1;
			methodRequest.file.expYear = $scope.selectedFile.expDate.getFullYear();
		}else{
			methodRequest.file.expDay = null;
			methodRequest.file.expMonth = null;
			methodRequest.file.expYear = null;
		}
		if($scope.filter.notNeeded){
			methodRequest.file.fileStatusId = FILE_STATUS_NOT_NEEDED;
		}else{
			methodRequest.file.fileStatusId = FILE_STATUS_NEEDED;
		}
		if($scope.action == 'add' || !methodRequest.file.userId){
			methodRequest.file.userId = $scope.userId;
		}
		if(!fileUploaded && Object.keys($scope.keesingSelectedFlags).length > 0){
			methodRequest.file.filesKeesingMatch = cloneObj($scope.keesingSelectedFlags);
			methodRequest.file.filesKeesingMatch.ksResponseId = $scope.selectedFile.filesKeesingMatch.ksResponseId;
		}
		$http.post(settings.backendJsonLink + service, methodRequest)
			.then(function(data) {
				$rootScope.resetGlobalErrorMsg($scope.localErrorMsgs);
				if($scope.action == 'add'){
					$scope.action = 'edit';
				}
				$scope.fileId = data.file.id;
				$scope.getFile($scope.fileId);
				$scope.saveCallback();
			})
			.catch(function(data) {
				$rootScope.handleErrors(data, 'saveFile', $scope.localErrorMsgs)
			})
			.finally(function() {
				hideLoading();
			})
	}
	
	$scope.updateUserData = function() {
		if (!isUndefined($scope.selectedFile.filesKeesingMatch) && $scope.selectedFile.filesKeesingMatch.userDOBUpdate) {
			var DOB = new Date($scope.selectedFile.filesKeesingMatch.userDOBUpdate.setMinutes($scope.selectedFile.filesKeesingMatch.userDOBUpdate.getMinutes() - $scope.selectedFile.filesKeesingMatch.userDOBUpdate.getTimezoneOffset()));
			var UserDataRequest = {
				userId: $scope.userId,
				writerId: writerId,
				dateOfBirth: DOB.getTime(),
				firstName: $scope.selectedFile.filesKeesingMatch.userFirstName,
				lastName: $scope.selectedFile.filesKeesingMatch.userLastName
			};
			$http.post(settings.backendJsonLink + 'UserFieldsService/updateUserData', UserDataRequest)
				.then(function(data) {
					$rootScope.resetGlobalErrorMsg($scope.localErrorMsgs);
				})
				.catch(function(data) {
					$rootScope.handleErrors(data, 'updateUserData', $scope.localErrorMsgs)
				})
				.finally(function() {
					hideLoading();
				})
			}
	}
	
	$scope.close = function(forceClose){
		if(!forceClose && ($scope.fileForm.$dirty || $scope.isKeesingUpdated() || ($scope.selectedFiles && $scope.selectedFiles.length > 0))){
			$scope.openClosePopup();
		}else{
			if(!isNewBackend){
				window.location = top.document.getElementById('content').src;
			}else{
				try{
					$scope.$dismiss('cancel');
				}catch(e){}
			}
		}
	}
	
	$scope.openClosePopup = function(){
		var previewModal = $uibModal.open({
			templateUrl: folderPrefix + 'components/files-close-popup.html',
			scope: $scope,
			windowClass: 'files-close-popup'
		});
	}
	$scope.hideClosePopup = function(successCallback){
		successCallback();
		$scope.close(true);
	}
	
	
	$scope.clearTempFileData = function(){
		$scope.uploadedFileName = '';
		$scope.uploadedImage = null;
		$scope.currentFileType = 0;
		$scope.selectedFiles = [];
		$scope.progress = [];
	}
	
	//Handle file upload
	$scope.abort = function(index) {
		$scope.upload[index].abort(); 
		$scope.upload[index] = null;
	};
	
	$scope.onFileSelect = function($files, fileType) {
		//Check if the file is too big, alert and return in case it is
		var hasLargerFile = false;
		$scope.fileTooBig = false;
		for ( var i = 0; i < $files.length; i++) {
			if($files[i].size / 1000000 >= settings.documentSizeLimit){
				hasLargerFile = true;
			}
		}
		if(hasLargerFile){
			//alert($rootScope.getMsgs('file-too-big'));
			$scope.fileTooBig = true;
			$timeout(function(){$scope.fileTooBig = false;}, 3000);
			return;
		}
		
		$scope.clearTempFileData();
		
		if($scope.action != 'add' && $scope.fileName){
			$scope.initSelectedFile();
		}
		
		$scope.currentFileType = fileType;
		
		if ($scope.upload && $scope.upload.length > 0) {
			for (var i = 0; i < $scope.upload.length; i++) {
				if ($scope.upload[i] != null) {
					$scope.upload[i].abort();
				}
			}
		}
		$scope.upload = [];
		$scope.uploadResult = [];
		$scope.selectedFiles = $files;
		$scope.dataUrls = [];
		for ( var i = 0; i < $files.length; i++) {
			var $file = $files[i];
			if ($scope.fileReaderSupported && $file.type.indexOf('image') > -1) {
				var fileReader = new FileReader();
				fileReader.readAsDataURL($files[i]);
				var loadFile = function(fileReader, index) {
					fileReader.onload = function(e) {
						$timeout(function() {
							$scope.dataUrls[index] = e.target.result;
							if (index == 0) {
								$scope.uploadedImage = $scope.dataUrls[index];
							}
						});
					}
				}(fileReader, i);
			}else if($scope.fileReaderSupported){
				$scope.uploadedFileName = $file.name;
			}
			//$scope.progress[i] = -1;
			//$scope.start(i);
		}
	};
	
	$scope.start = function(index) {
		if ($scope.selectedFiles[index].size / 1000000  < settings.documentSizeLimit) {
			$scope.progress[index] = 0;
			$scope.errorMsg = null;
			//$upload.upload()
			var params = '?';
			params += 'fileName=' + $scope.selectedFiles[index].name;
			params += '&fileType=' + $scope.currentFileType;
			params += '&fileId=' + $scope.fileId;
			params += '&userId=' + $scope.userId;
			params = encodeURI(params);
			$scope.upload[index] = $upload.upload({
				url: settings.backendFileUploadLink + params,
				method: 'POST',
				file: $scope.selectedFiles[index],
				fileFormDataName: 'myFile',
				data: {
					fileType: $scope.currentFileType,
					fileName: $scope.selectedFiles[index].name,
					test: $scope.filter.freeTextFilter
				}
			});
			$scope.upload[index].then(function(response) {
				$timeout(function() {
					$scope.uploadResult.push(response);
					$scope.uploadComplete(response);
				});
			}, function(response) {
				if (response.status > 0){
					hideLoading();
					$rootScope.addGlobalErrorMsg($rootScope.getMsgs('error.unknown'));
				}
			}, function(evt) {
				// Math.min is to fix IE which reports 200% sometimes
				$scope.$parent.progress = $scope.progress[index] = Math.min(100, parseInt(100.0 * evt.loaded / evt.total));
			});
			$scope.upload[index].xhr(function(xhr){
	//				xhr.upload.addEventListener('abort', function() {console.log('abort complete')}, false);
			});
		} else {
			hideLoading();
			$rootScope.addGlobalErrorMsg($rootScope.getMsgs('file-too-big'));
		}
	};
	
	$scope.uploadComplete = function(data) {
		if (!$rootScope.handleErrors(data, 'UploadDocumentsService', $scope.localErrorMsgs)) {
			if(data.userMessages){
				for(var i = 0; i < data.userMessages.length; i++){
					if(data.userMessages[i].field == 'fileName'){
						$scope.selectedFile.fileName = data.userMessages[i].message;
						$scope.saveFileDetails(true);
					}
				}
			}
		}
	}
	
	if (localStorage) {
		$scope.s3url = localStorage.getItem("s3url");
		$scope.AWSAccessKeyId = localStorage.getItem("AWSAccessKeyId");
		$scope.acl = localStorage.getItem("acl");
		$scope.success_action_redirect = localStorage.getItem("success_action_redirect");
		$scope.policy = localStorage.getItem("policy");
		$scope.signature = localStorage.getItem("signature");
	}
	$scope.success_action_redirect = $scope.success_action_redirect || window.location.protocol + "//" + window.location.host;
	$scope.jsonPolicy = $scope.jsonPolicy || '{\n  "expiration": "2020-01-01T00:00:00Z",\n  "conditions": [\n    {"bucket": "angular-file-upload"},\n    ["starts-with", "$key", ""],\n    {"acl": "private"},\n    ["starts-with", "$Content-Type", ""],\n    ["starts-with", "$filename", ""],\n    ["content-length-range", 0, 524288000]\n  ]\n}';
	$scope.acl = $scope.acl || 'private';
	
	$scope.send = function(){
		for ( var i = 0; i < $scope.selectedFiles.length; i++) {
			$scope.progress[i] = -1;
			$scope.start(i);
		}
	}
	
	
	$scope.toggleApproveReject = function(target, toggled){
		if(target != false){
			$scope.selectedFile[toggled] = false;
		}
	}

	$scope.disableBrowse = function() {
		if ($scope.selectedFile.expDate) {
			var expDate = new Date($scope.selectedFile.expDate);
			//14 day ago
			var date = new Date();
			date.setHours(23);
			date.setMinutes(59);
			date.setDate(date.getDate() - 1);
			var dateFuture = new Date(date);
			dateFuture.setDate(dateFuture.getDate() + 14);
			
			if (dateFuture.getTime() < expDate.getTime()) {
				return $scope.selectedFile.isApproved;
			} else {
				return false;
			}
		} else {
			return $scope.selectedFile.isApproved;
		}
	}
}]);
	
backendApp.controller('ComplaintsFilesController', ['$rootScope', '$scope', '$http', '$filter', '$upload', '$timeout', '$uibModal', function($rootScope, $scope, $http, $filter, $upload, $timeout, $uibModal) {
	$scope.userId = userId;
	$scope.ready = false;
	$scope.action = 'loading';//add,edit
	$scope.selectedFile = {};
	var initDependencies = {name: 'initList', dependencies: {getComplaintFilesScreenSettings: false}};
	$scope.initScreenCtr($scope, initDependencies, function(){$scope.getComplaintFilesScreenSettings('initList');});
	
	$scope.checkAction = function(){
		if($rootScope.$state.includes("ln.users.complaintsFiles.add")){
			$scope.action = 'add';
		}else{
			$scope.action = '';
			$scope.getAllComplaintFiles();
		}
	}
	
	$scope.getComplaintFilesScreenSettings = function(dependencyName) {
		var UserIdRequest = {
			userId: $scope.userId
		};
		$http.post(settings.backendJsonLink + 'ComplaintFilesServices/getComplaintFilesScreenSettings', UserIdRequest)
			.then(function(data) {
				$rootScope.resetGlobalErrorMsg();
				for(var i = 0; i < data.permissionsList.length; i++){
					$rootScope.permissions.allow(data.permissionsList[i]);
				}
			})
			.catch(function(data) {
				$rootScope.handleErrors(data, 'getComplaintFilesScreenSettings')
			})
			.finally(function() {
				$rootScope.markDependencyDone($scope, dependencyName, 'getComplaintFilesScreenSettings');
			})
	}
	
	$scope.getAllComplaintFiles = function() {
		var UserIdRequest = {
			userId: $scope.userId
		};
		$http.post(settings.backendJsonLink + 'ComplaintFilesServices/getAllComplaintFiles', UserIdRequest)
			.then(function(data) {
				$scope.allFiles = data.allFiles;
			})
			.catch(function(data) {
				handleNetworkError(data);
			})
	}
	
	$scope.changeView = function(view) {
		if (view == 'add') {
			$rootScope.$state.go('ln.users.complaintsFiles.add',{ln: $rootScope.$state.params.ln});
		} else {
			$rootScope.$state.go('ln.users.complaintsFiles',{ln: $rootScope.$state.params.ln});
		}
	}
	
	$scope.previewFile = function(fileName) {
		$scope.fileName_preview = fileName;
		$scope.fileIsImage_preview = false;
		var extension = fileName.substring(fileName.lastIndexOf(".")+1,fileName.length);
		if(extension.toLocaleLowerCase() == 'jpg' || extension.toLocaleLowerCase() == 'jpeg' || extension.toLocaleLowerCase() == 'png' || extension.toLocaleLowerCase() == 'gif' || extension.toLocaleLowerCase() == 'bmp'){
			$scope.fileIsImage_preview = true;
		}
		$scope.filePath_preview = settings.jsonImagegLink + 'docs/' + fileName + '?isNewBe=1';
	}
	
	$scope.uploadTheFile = function() {
		$scope.showLoading();
		if (!isUndefined($scope.selectedFiles) && $scope.selectedFiles.length > 0) {
			$scope.start(0);
		} else {
			$scope.hideLoading();
			showToast(2, $rootScope.getMsgs('file.alert.no.document.selected'));
		}
	}
	
	$scope.insertFile = function() {
		var FileRequest = {
			file: angular.merge($scope.selectedFile,{
				userId: $scope.userId
			})
		};
		$http.post(settings.backendJsonLink + 'ComplaintFilesServices/insertFile', FileRequest)
			.then(function(data) {
				$rootScope.resetGlobalErrorMsg();
				$scope.changeView('');
				showToast(1, $rootScope.getMsgs('file.upload.successful',{fileName: $scope.selectedFiles[0].name}));
				$scope.clearTempFileData();
			})
			.catch(function(data) {
				$rootScope.markDependencyDone($scope, dependencyName, 'insertFile');
				$rootScope.handleErrors(data, 'insertFile')
			})
			.finally(function(){
				$scope.hideLoading();
			})
	}

	$scope.clearTempFileData = function(){
		$scope.uploadedFileName = '';
		$scope.uploadedImage = null;
		$scope.currentFileType = 0;
		$scope.selectedFiles = [];
		$scope.progress = [];
		$scope.selectedFile.reference = '';
		$scope.selectedFile.comment = '';
	}
	

	
	//Handle file upload
	$scope.progress = [];
	$scope.abort = function(index) {
		$scope.upload[index].abort(); 
		$scope.upload[index] = null;
	};
	
	$scope.onFileSelect = function($files) {
		//Check if the file is too big, alert and return in case it is
		var hasLargerFile = false;
		$scope.fileTooBig = false;
		for ( var i = 0; i < $files.length; i++) {
			if($files[i].size / 1000000 >= settings.documentSizeLimit){
				hasLargerFile = true;
			}
		}
		if(hasLargerFile){
			//alert($rootScope.getMsgs('file-too-big'));
			$scope.fileTooBig = true;
			$timeout(function(){$scope.fileTooBig = false;}, 3000);
			return;
		}
		
		if ($scope.upload && $scope.upload.length > 0) {
			for (var i = 0; i < $scope.upload.length; i++) {
				if ($scope.upload[i] != null) {
					$scope.upload[i].abort();
				}
			}
		}
		$scope.upload = [];
		$scope.uploadResult = [];
		$scope.selectedFiles = $files;
		$scope.dataUrls = [];
		
		for ( var i = 0; i < $files.length; i++) {
			var $file = $files[i];
			if ($scope.fileReaderSupported && $file.type.indexOf('image') > -1) {
				var fileReader = new FileReader();
				fileReader.readAsDataURL($files[i]);
				var loadFile = function(fileReader, index) {
					fileReader.onload = function(e) {
						$timeout(function() {
							$scope.dataUrls[index] = e.target.result;
							if (index == 0) {
								$scope.uploadedImage = $scope.dataUrls[index];
							}
						});
					}
				}(fileReader, i);
			}else if($scope.fileReaderSupported){
				$scope.uploadedFileName = $file.name;
			}
			//$scope.progress[i] = -1;
			//$scope.start(i);
		}
	};
	
	$scope.start = function(index) {
		if ($scope.selectedFiles[index].size / 1000000  < settings.documentSizeLimit) {
			$scope.progress[index] = 0;
			$scope.errorMsg = null;
			//$upload.upload()
			var params = '?';
			params += 'fileName=' + $scope.selectedFiles[index].name;
			// params += '&fileType=' + $scope.currentFileType;
			// params += '&fileId=' + $scope.fileId;
			params += '&isComplaint=1';
			params = encodeURI(params);
			
			$scope.upload[index] = $upload.upload({
				url: settings.backendFileUploadLink + params,
				method: 'POST',
				file: $scope.selectedFiles[index],
				fileFormDataName: 'myFile',
				data: {
					fileName: $scope.selectedFiles[index].name,
					isComplaint:1
				}
			});
			$scope.upload[index].then(function(response) {
				$timeout(function() {
					$scope.uploadResult.push(response);
					$scope.uploadComplete(response);
				});
			}, function(response) {
				if (response.status > 0){
					hideLoading();
					$rootScope.addGlobalErrorMsg($rootScope.getMsgs('error.unknown'));
				}
			}, function(evt) {
				// Math.min is to fix IE which reports 200% sometimes
				$scope.$parent.progress = $scope.progress[index] = Math.min(100, parseInt(100.0 * evt.loaded / evt.total));
			});
			$scope.upload[index].xhr(function(xhr){
	//				xhr.upload.addEventListener('abort', function() {console.log('abort complete')}, false);
			});
		} else {
			// hideLoading();
			$rootScope.addGlobalErrorMsg($rootScope.getMsgs('file-too-big'));
		}
	};
	
	$scope.uploadComplete = function(data) {
		if (!$rootScope.handleErrors(data, 'UploadDocumentsService', $scope.localErrorMsgs)) {
			if(data.userMessages){
				for(var i = 0; i < data.userMessages.length; i++){
					if(data.userMessages[i].field == 'fileName'){
						$scope.selectedFile.fileName = data.userMessages[i].message;
						$scope.insertFile(data);
					}
				}
			}
		} else {
			$scope.hideLoading();
		}
	}
	
	if (localStorage) {
		$scope.s3url = localStorage.getItem("s3url");
		$scope.AWSAccessKeyId = localStorage.getItem("AWSAccessKeyId");
		$scope.acl = localStorage.getItem("acl");
		$scope.success_action_redirect = localStorage.getItem("success_action_redirect");
		$scope.policy = localStorage.getItem("policy");
		$scope.signature = localStorage.getItem("signature");
	}
	$scope.success_action_redirect = $scope.success_action_redirect || window.location.protocol + "//" + window.location.host;
	$scope.jsonPolicy = $scope.jsonPolicy || '{\n  "expiration": "2020-01-01T00:00:00Z",\n  "conditions": [\n    {"bucket": "angular-file-upload"},\n    ["starts-with", "$key", ""],\n    {"acl": "private"},\n    ["starts-with", "$Content-Type", ""],\n    ["starts-with", "$filename", ""],\n    ["content-length-range", 0, 524288000]\n  ]\n}';
	$scope.acl = $scope.acl || 'private';
}]);

backendApp.controller('WithdrawalsCtr', ['$rootScope', '$scope', '$http', '$state', function($rootScope, $scope, $http, $state) {
	
}]);

backendApp.controller('InsertRecordCtr', ['$rootScope', '$scope', '$http', '$state', function($rootScope, $scope, $http, $state) {
	$scope.ready = false;
	$scope.action = 'loading';
	$scope.filter = {};
	
	$scope.filter.withdrawTypes = [];// model -> dropdown content
	$scope.filter.withdrawType = null;// controller selected value from dropdown
	
	$scope.filter.amount = '';
	$scope.filter.comment = '';
	
	$scope.showConfirmationMessage = false;
	
	$scope.initFiltersOnReady = function(){
		//
	}
	
	//Init the controller
	var initDependencies = {name: 'initList', dependencies: {getWithdrawScreenSettings: false}};
	$scope.initScreenCtr($scope, initDependencies, function(){$scope.getWithdrawScreenSettings('initList');});
	
	$scope.checkAction = function(){
		//
	}
	
	$scope.getWithdrawScreenSettings = function(dependencyName){
		$rootScope.resetGlobalErrorMsg();
		showLoading();
		var methodRequest = {};
		$http.post(settings.backendJsonLink + 'WithdrawServices/getWithdrawScreenSettings', methodRequest)
			.then(function(data) {
				$rootScope.resetGlobalErrorMsg();
				for(var i = 0; i < data.permissionsList.length; i++){
					$rootScope.permissions.allow(data.permissionsList[i]);
				}
				//withdrawTypes
				$rootScope.fillFilter({arr: $scope.filter.withdrawTypes, data: data, filter: 'transactionTypesGermanWithdraw', translate: true});
				if($scope.filter.withdrawTypes.length > 0){
					$scope.filter.withdrawType = $scope.filter.withdrawTypes[0];
				}
			})
			.catch(function(data) {
				$rootScope.handleErrors(data, 'getWithdrawScreenSettings')
			})
			.finally(function() {
				$rootScope.markDependencyDone($scope, dependencyName, 'getWithdrawScreenSettings');
			})
	}
	
	
	$scope.insertRecord = function(){
		$scope.showConfirmationMessage = false;
		$rootScope.resetGlobalErrorMsg();
		showLoading();
		var methodRequest = {};
		methodRequest.withdrawTypeId = $scope.filter.withdrawType.id;
		methodRequest.amount = parseFloat($scope.filter.amount);
		methodRequest.comment = $scope.filter.comment;
		methodRequest.userId = $scope.userId;
		$http.post(settings.backendJsonLink + 'WithdrawServices/withdrawInsertRecord', methodRequest)
			.then(function(data) {
				$rootScope.resetGlobalErrorMsg();
				$scope.showConfirmationMessage = true;
			})
			.catch(function(data) {
				$rootScope.handleErrors(data, 'insertRecord')
			})
			.finally(function() {
				hideLoading();
			})
	}
}]);
////// This GmWithdrawalController will become obsolete, since the screen was merged in APM withdrawals //////
backendApp.controller('GmWithdrawalController', ['$rootScope', '$scope', '$http', function($rootScope, $scope, $http) {
	$scope.withdrawTypes = {
		direct24: 59,
		giropay: 60,
		eps: 61,
		ideal: 63
	};
	$scope.form = {}
	$scope.ready = true;
	$scope.state = 0;//0-form, 1-confirm fee, 2-success, 3-cancel, 4-confirm no fee, 
	
	var initDependencies = {name: 'init', dependencies: {getPermissionsToDisplay: false, getScreenSettings: false}};
	$scope.initScreenCtr($scope, initDependencies, function(){$scope.getPermissionsToDisplay('init');$scope.getScreenSettings('init');});
	
	$scope.checkAction = function(){
		//
	}
	
	$scope.getScreenSettings = function(dependencyName) {
		$rootScope.resetGlobalErrorMsg();
		showLoading();
		var methodRequest = {
			writerId: writerId
		};
		$http.post(settings.commonJsonLink + 'GmWithdrawalServices/getScreenSettings', methodRequest)
			.then(function(data) {
				$rootScope.resetGlobalErrorMsg();
				for(var i = 0; i < data.permissionsList.length; i++){
					$rootScope.permissions.allow(data.permissionsList[i]);
				}
			})
			.catch(function(data) {
				$rootScope.handleErrors(data, 'getScreenSettings')
			})
			.finally(function() {
				$rootScope.markDependencyDone($scope, dependencyName, 'getScreenSettings');
			})
	}
	
	$scope.getPermissionsToDisplay = function(dependencyName) {
		var GmWithdrawalPermissionToDisplayRequest = {
			userId: userId
		};
		
		$http.post(settings.commonJsonLink + 'GmWithdrawalServices/getPermissionsToDisplay', GmWithdrawalPermissionToDisplayRequest)
			.then(function(data) {
				$rootScope.resetGlobalErrorMsg();
				$scope.displayDirect24 = data.displayDirect24;
				$scope.displayEPS = data.displayEPS;
				$scope.displayGiropay = data.displayGiropay;
				$scope.displayIdeal = data.displayIdeal;

				$scope.count = 0;
				if ($scope.displayDirect24) {$scope.count++;}
				if ($scope.displayGiropay) {$scope.count++;}
				if ($scope.displayEPS) {$scope.count++;}
				if ($scope.displayIdeal) {$scope.count++;}

				if ($scope.count == 1) {
					if ($scope.displayDirect24) {$scope.form.gmType = $scope.withdrawTypes.direct24;}
					if ($scope.displayGiropay) {$scope.form.gmType = $scope.withdrawTypes.giropay;}
					if ($scope.displayEPS) {$scope.form.gmType = $scope.withdrawTypes.eps;}
					if ($scope.displayIdeal) {$scope.form.gmType = $scope.withdrawTypes.ideal;}
				}
			})
			.catch(function(data) {
				$rootScope.handleErrors(data, 'getPermissionsToDisplay');
			})
			.finally(function() {
				$rootScope.markDependencyDone($scope, dependencyName, 'getPermissionsToDisplay');
			})
	}
	
	$scope.submitWithdraw = function(_form) {
		if (_form.$valid) {
			if (isUndefined($scope.form.gmType)) {
				showToast(2, $rootScope.getMsgs('no-provider'));
				return;
			}
			$scope.GmWithdrawalRequest = {
				beneficiaryName: $scope.form.beneficiaryName,
				swiftBicCode: ($scope.form.gmType == $scope.withdrawTypes.eps || $scope.form.gmType == $scope.withdrawTypes.ideal) ? $scope.form.swiftBicCode : null,// EPS only
				iban: ($scope.form.gmType == $scope.withdrawTypes.eps || $scope.form.gmType == $scope.withdrawTypes.ideal) ? $scope.form.iban : null,//EPS only
				accountNumber: ($scope.form.gmType != $scope.withdrawTypes.ideal) ? parseInt($scope.form.accountNumber) : null,
				bankName: ($scope.form.gmType != $scope.withdrawTypes.ideal) ? $scope.form.bankName : null,
				branchNumber: ($scope.form.gmType != $scope.withdrawTypes.ideal) ? parseInt($scope.form.branchNumber) : null,
				branchAddress: ($scope.form.gmType != $scope.withdrawTypes.ideal) ? $scope.form.branchAddress : null,
				amount: parseInt($scope.form.amount),
				gmType: $scope.form.gmType, // EPS value->61, Giropay value->60, Direct24 value->59, ideal value->63
				userId: userId,
				exemptFee: $scope.form.exemptFee,
				writerId: writerId
			};
			
			if ($scope.form.exemptFee) {
				$scope.state = 4;
				return;
			}
			
			$http.post(settings.commonJsonLink + '/GmWithdrawalServices/validateGMWithdraw', $scope.GmWithdrawalRequest)
				.then(function(data) {
					$rootScope.resetGlobalErrorMsg();
					$scope.state = 1;
					$scope.approveMsgs = $rootScope.getMsgs('bank.wire.approve.message');
					$scope.approveMsgs = $scope.approveMsgs.replace('{0}', formatAmountSimple(data.fee/100, 0));
				})
				.catch(function(data) {
					$rootScope.handleErrors(data, 'validateGMWithdraw');
				})
		} else {
			logIt({'type':3,'msg':$scope._form.$error});
		}
	}
	
	$scope.insertGmWithdraw = function(_form) {
		$http.post(settings.commonJsonLink + 'GmWithdrawalServices/insertGmWithdraw', $scope.GmWithdrawalRequest)
			.then(function(data) {
				$rootScope.resetGlobalErrorMsg();
				$scope.state = 2;
			})
			.catch(function(data) {
				$rootScope.handleErrors(data, 'insertGmWithdraw');
			})
	}
	
	$scope.cancelWithdraw = function() {
		$scope.state = 3;
	}
}]);

backendApp.controller('UserDetailsCtr', ['$rootScope', '$scope', '$http', '$state', function($rootScope, $scope, $http, $state) {
	
}]);

backendApp.controller('BankWireSkrillController', ['$rootScope', '$scope', '$http', '$timeout', '$upload', function($rootScope, $scope, $http, $timeout, $upload) {
	$scope.form = {
		type: 2
	}
	$scope.ready = true;
	$scope.success = false;
	$scope.selectedFile = {};
	$scope.selectedFiles = [];
	
	var initDependencies = {name: 'init', dependencies: {getScreenSettings: false,getWithdrawUserDetails: false}};
	$scope.initScreenCtr($scope, initDependencies, function(){$scope.getScreenSettings('init');$scope.getWithdrawUserDetails('init');});
	
	$scope.checkAction = function(){
		//
	}
	
	$scope.getScreenSettings = function(dependencyName) {
		$rootScope.resetGlobalErrorMsg();
		showLoading();
		var methodRequest = {
			writerId: writerId
		};
		$http.post(settings.commonJsonLink + 'WithdrawUserDetailsServices/getScreenSettings', methodRequest)
			.then(function(data) {
				$rootScope.resetGlobalErrorMsg();
				for(var i = 0; i < data.permissionsList.length; i++){
					$rootScope.permissions.allow(data.permissionsList[i]);
				}
			})
			.catch(function(data) {
				$rootScope.handleErrors(data, 'getScreenSettings')
			})
			.finally(function() {
				$rootScope.markDependencyDone($scope, dependencyName, 'getScreenSettings');
			})
	}
	
	$scope.getWithdrawUserDetails = function(dependencyName) {
		var WithdrawUserDetailsMethodRequest  = {
			withdrawUserDetails: {
				userId: userId
			}
		};
		
		$http.post(settings.commonJsonLink + 'WithdrawUserDetailsServices/getWithdrawUserDetails', WithdrawUserDetailsMethodRequest )
			.then(function(data) {
				$rootScope.resetGlobalErrorMsg();
				$scope.form = jsonClone($scope.form, data.withdrawUserDetails);
				$scope.updateId = null;
				$scope.filePath_preview = '';
				if (!isUndefined(data.withdrawUserDetails)) {
					$scope.updateId = data.withdrawUserDetails.id;
					if (data.withdrawUserDetails.fileName != null) {
						$scope.filePath_preview = settings.jsonImagegLink + 'docs/' + data.withdrawUserDetails.fileName + '?isNewBe=1';
					}
				}
			})
			.catch(function(data) {
				$rootScope.handleErrors(data, 'getWithdrawUserDetails');
			})
			.finally(function() {
				$rootScope.markDependencyDone($scope, dependencyName, 'getWithdrawUserDetails');
			})
	}
	
	$scope.insertUpdateWithdrawUserDetailas = function(_form) {
		if($scope.isSubmitting){
			return false;
		}
		if (_form.$valid) {
			$scope.isSubmitting = true;
			$scope.showLoading();
			$scope.form.userId = userId;
			if ($scope.form.accountNum == '') {$scope.form.accountNum = null;}
			if ($scope.form.branchNumber == '') {$scope.form.branchNumber = null;}
			var WithdrawUserDetailsMethodRequest = {
				withdrawUserDetails: $scope.form,
				withdrawUserDetailsType: $scope.form.type,
				writerId: writerId
			};
			
			var url = ($scope.updateId != null) ? 'updateWithdrawUserDetils' : 'insertWithdrawUserDetailas';
			$http.post(settings.commonJsonLink  + '/WithdrawUserDetailsServices/'+url, WithdrawUserDetailsMethodRequest)
				.then(function(data) {
					$rootScope.resetGlobalErrorMsg();
					if ($scope.form.type == 1) {
						if ($scope.selectedFiles.length > 0) {
							$scope.start(0);
						} else {
							$scope.isSubmitting = false;
							$scope.success = true;
							$scope.hideLoading();
						}
					} else {
						$scope.isSubmitting = false;
						$scope.success = true;
						$scope.hideLoading();
					}
				})
				.catch(function(data) {
					$rootScope.handleErrors(data, url);
					$scope.hideLoading();
					$scope.isSubmitting = false;
				})
		} else {
			logIt({'type':3,'msg':_form.$error});
		}
	}
	
	//Handle file upload
	$scope.progress = [];
	$scope.abort = function(index) {
		$scope.upload[index].abort(); 
		$scope.upload[index] = null;
	};
	
	$scope.onFileSelect = function($files) {
		//Check if the file is too big, alert and return in case it is
		var hasLargerFile = false;
		$scope.fileTooBig = false;
		for ( var i = 0; i < $files.length; i++) {
			if($files[i].size / 1000000 >= settings.documentSizeLimit){
				hasLargerFile = true;
			}
		}
		if(hasLargerFile){
			//alert($rootScope.getMsgs('file-too-big'));
			$scope.fileTooBig = true;
			$timeout(function(){$scope.fileTooBig = false;}, 3000);
			return;
		}
		
		if ($scope.upload && $scope.upload.length > 0) {
			for (var i = 0; i < $scope.upload.length; i++) {
				if ($scope.upload[i] != null) {
					$scope.upload[i].abort();
				}
			}
		}
		$scope.upload = [];
		$scope.uploadResult = [];
		$scope.selectedFiles = $files;
		$scope.dataUrls = [];
		
		for ( var i = 0; i < $files.length; i++) {
			var $file = $files[i];
			if ($scope.fileReaderSupported && $file.type.indexOf('image') > -1) {
				var fileReader = new FileReader();
				fileReader.readAsDataURL($files[i]);
				var loadFile = function(fileReader, index) {
					fileReader.onload = function(e) {
						$timeout(function() {
							$scope.dataUrls[index] = e.target.result;
							if (index == 0) {
								$scope.uploadedImage = $scope.dataUrls[index];
							}
						});
					}
				}(fileReader, i);
			}else if($scope.fileReaderSupported){
				$scope.uploadedFileName = $file.name;
			}
			//$scope.progress[i] = -1;
			//$scope.start(i);
		}
	};
	
	$scope.start = function(index) {
		if ($scope.selectedFiles[index].size / 1000000  < settings.documentSizeLimit) {
			$scope.progress[index] = 0;
			$scope.errorMsg = null;
			//$upload.upload()
			var params = '?';
			params += 'fileType=35&';
			params += 'writerId='+writerId+'&';
			params += 'fileName=' + $scope.selectedFiles[index].name;
			params = encodeURI(params);
			
			$scope.upload[index] = $upload.upload({
				url: settings.commonFileUploadLink + params,
				method: 'POST',
				file: $scope.selectedFiles[index],
				fileFormDataName: 'myFile',
				data: {
					fileName: $scope.selectedFiles[index].name,
					fileType: 35,
					writerId: writerId
				}
			});
			$scope.upload[index].then(function(response) {
				$timeout(function() {
					$scope.uploadResult.push(response);
					$scope.uploadComplete(response);
				});
			}, function(response) {
				if (response.status > 0 || isUndefined(response.status)){
					$scope.hideLoading();
					$scope.isSubmitting = false;
					$rootScope.addGlobalErrorMsg($rootScope.getMsgs('error.unknown'));
				}
			}, function(evt) {
				// Math.min is to fix IE which reports 200% sometimes
				$scope.$parent.progress = $scope.progress[index] = Math.min(100, parseInt(100.0 * evt.loaded / evt.total));
			});
			$scope.upload[index].xhr(function(xhr){
	//				xhr.upload.addEventListener('abort', function() {console.log('abort complete')}, false);
			});
		} else {
			// hideLoading();
			$rootScope.addGlobalErrorMsg($rootScope.getMsgs('file-too-big'));
		}
	};
	
	$scope.uploadComplete = function(data) {
		if (!$rootScope.handleErrors(data, 'UploadDocumentsService', $scope.localErrorMsgs)) {
			$scope.selectedFile.fileName = $scope.selectedFiles[0].name;
			$scope.isSubmitting = false;
			$scope.success = true;
			$scope.hideLoading();
		} else {
			$scope.hideLoading();
		}
	}
	
	if (localStorage) {
		$scope.s3url = localStorage.getItem("s3url");
		$scope.AWSAccessKeyId = localStorage.getItem("AWSAccessKeyId");
		$scope.acl = localStorage.getItem("acl");
		$scope.success_action_redirect = localStorage.getItem("success_action_redirect");
		$scope.policy = localStorage.getItem("policy");
		$scope.signature = localStorage.getItem("signature");
	}
	$scope.success_action_redirect = $scope.success_action_redirect || window.location.protocol + "//" + window.location.host;
	$scope.jsonPolicy = $scope.jsonPolicy || '{\n  "expiration": "2020-01-01T00:00:00Z",\n  "conditions": [\n    {"bucket": "angular-file-upload"},\n    ["starts-with", "$key", ""],\n    {"acl": "private"},\n    ["starts-with", "$Content-Type", ""],\n    ["starts-with", "$filename", ""],\n    ["content-length-range", 0, 524288000]\n  ]\n}';
	$scope.acl = $scope.acl || 'private';
}]);

backendApp.controller('AccountTransactionsCtr', ['$rootScope', '$scope', '$http', 'Pagination', function($rootScope, $scope, $http, Pagination) {

}]);

backendApp.controller('DepositsSummaryCtr', ['$rootScope', '$scope', '$http', 'Pagination', function($rootScope, $scope, $http, Pagination) {
	$scope.ready = false;
	$scope.action = 'loading';
	$scope.filter = {};

	$scope.depositSummaryListSelected = null;

	$scope.initFiltersOnReady = function(){
		//
	}
	
	//Init the controller
	var initDependencies = {name: 'initList', dependencies: {getDepositsSummaryScreenSettings: false}};
	$scope.initScreenCtr($scope, initDependencies, function(){$scope.getDepositsSummaryScreenSettings('initList');});
	
	$scope.checkAction = function(){
		$scope.getDepositSummaryDetails();
	}

	$scope.getDepositsSummaryScreenSettings = function(dependencyName){
		var MethodRequest = {};
		$http.post(settings.backendJsonLink + 'DepositsServices/getDepositsSummaryScreenSettings', MethodRequest)
			.then(function(data) {
				$rootScope.resetGlobalErrorMsg();
				for(var i = 0; i < data.permissionsList.length; i++){
					$rootScope.permissions.allow(data.permissionsList[i]);
				}
			})
			.catch(function(data) {
				$rootScope.handleErrors(data, 'getDepositsSummaryScreenSettings');
			})
			.finally(function() {
				$rootScope.markDependencyDone($scope, dependencyName, 'getDepositsSummaryScreenSettings');
			})
	}
	
	$scope.getDepositSummaryDetails = function(){
		if ($scope.depositSummaryListSelected == null) {
			showLoading();
			var methodRequest = {};
			$http.post(settings.backendJsonLink + 'DepositsServices/getDepositSummaryDetails', methodRequest)
				.then(function(data) {
					$rootScope.resetGlobalErrorMsg();
					$scope.depositSummaryListSelected = data.depositSummaryList.map(function(current, index) {
						if (index > 0) {
							if (current.providerName == null) {
									current.rowTotal = true;
								} else {
									current.rowTotal = false;
								}
						} else {
							current.rowTotal = true;
						}
						return current;
					});
				})
				.catch(function(data) {
					$rootScope.handleErrors(data, 'getDepositSummaryDetails')
				})
				.finally(function() {
					hideLoading();
				})
		}
	}
}]);

backendApp.controller('userSingleQuestionnaireCtr', ['$rootScope', '$scope', '$http', function($rootScope, $scope, $http) {
	$scope.ready = false;
	$scope.success = false;
	$scope.activeScreen = 0;
	$scope.hasError = true;
	$scope.disabled = true;
	
	//Init the controller
	var initDependencies = {name: 'initList', dependencies: {getQuestionnaireScreenSettings: false}};
	$scope.initScreenCtr($scope, initDependencies, function(){$scope.getQuestionnaireScreenSettings('initList');});

	$scope.checkAction = function(){
		$scope.getUserSingleQuestionnaireDynamic();
	}
	
	$scope.getQuestionnaireScreenSettings = function(dependencyName){
		var methodRequest = {
			skinId: (!isUndefined($scope.userSkinId)) ? $scope.userSkinId : userSkinId,
			userName: userName,
			writerId: writerId
		};
		$http.post(settings.backendJsonLink + 'QuestionnaireServices/getQuestionnaireScreenSettings', methodRequest)
			.then(function(data) {
				$rootScope.resetGlobalErrorMsg($scope.localErrorMsgs);
				for(var i = 0; i < data.permissionsList.length; i++){
					$rootScope.permissions.allow(data.permissionsList[i]);
				}
			})
			.catch(function(data) {
				$rootScope.handleErrors(data, 'getQuestionnaireScreenSettings', $scope.localErrorMsgs)
			})
			.finally(function() {
				$rootScope.markDependencyDone($scope, dependencyName, 'getQuestionnaireScreenSettings');
			})
	}

	$scope.isRegulationQuestionaireCalled = false;
	$scope.getUserSingleQuestionnaireDynamic = function() {
		if($scope.isRegulationQuestionaireCalled){
			return;
		}
		$scope.isRegulationQuestionaireCalled = true;
		
		var UserMethodRequest = {
			skinId: (!isUndefined($scope.userSkinId)) ? $scope.userSkinId : userSkinId,
			userName: userName,
			writerId: writerId
		}
		$http.post(settings.backendJsonLink + 'QuestionnaireServices/getUserSingleQuestionnaireDynamic', UserMethodRequest)
			.then(function(data) {
				$rootScope.resetGlobalErrorMsg();
				$scope.ready = true;
				$scope.disabled = !$rootScope.permissions.get('questionnaire_save') || !data.isBEedit;
				if (data.errorCode == errorCodeMap.regulation_suspended) {
					$scope.errorMsg = data.userMessages[0].message;
				} else {
					$scope.hasError = false;
					
					$scope.questions = [];
					$scope.userAnswers = data.userAnswers;
					
					for (var i = 0; i < data.questions.length; i++) {
						if (data.questions[i].questionType == 'DROPDOWN') {
							data.questions[i].answers.unshift({
								id: null, 
								translation: $rootScope.getMsgs('select')
							})
						} else if (data.questions[i].questionType == 'CHECKBOX') {
							if (data.questions[i].answers[0].name.search('no') > -1) {//if no is the first answer
								var el = data.questions[i].answers.pop();//remove the last one (yes)
								data.questions[i].answers.unshift(el);//and add it in the begining, before no
							}
						}
						
						var index = searchJsonKeyInArray(data.userAnswers, 'questionId', data.questions[i].id);
						data.questions[i].index = i;
						
						var indexAnswer = searchJsonKeyInArray(data.questions[i].answers, 'id', data.userAnswers[index].answerId);
						
						var indexAnswerReal = 0;
						if (index > -1 && data.userAnswers[index].answerId != null && indexAnswer > -1) {
							indexAnswerReal = indexAnswer;
						} else if (data.questions[i].defaultAnswerId == 0) {
							indexAnswerReal = 0;
						} else {
							indexAnswerReal = searchJsonKeyInArray(data.questions[i].answers, 'id', data.questions[i].defaultAnswerId);
						}
						
						if (data.questions[i].questionType == 'DROPDOWN') {
							data.questions[i].userAnswer = data.questions[i].answers[indexAnswerReal];
							data.questions[i].userAnswer.textAnswer = data.userAnswers[index].textAnswer;
						} else if (data.questions[i].questionType == 'CHECKBOX') {
							data.questions[i].userAnswer = {
								id: data.questions[i].answers[indexAnswerReal].id
							}
						}
						
						$scope.questions.push(data.questions[i]);
					}
					
					$scope.checkStatus(false);
				}
			})
			.catch(function(data) {
				$rootScope.handleErrors(data, 'getUserSingleQuestionnaireDynamic');
			})
	}
	
	$scope.checkStatus = function(showErrors) {
		var questionsAnswered = 0;
		var questionsTotal = 0;
		for (var n = 0; n < $scope.questions.length; n++) {
			if ($scope.questions[n].mandatory) {
				questionsTotal++;
			}
			if (
				$scope.questions[n].mandatory && (
					($scope.questions[n].questionType == 'DROPDOWN' && $scope.questions[n].userAnswer.id != null) || 
					($scope.questions[n].questionType == 'CHECKBOX' && $scope.questions[n].userAnswer.id == $scope.questions[n].answers[0].id)
				)
			) {
				questionsAnswered++;
				$scope.questions[n].hasError = false;
			} else if (showErrors && $scope.questions[n].mandatory) {
				$scope.questions[n].hasError = true;
			} else {
				$scope.questions[n].hasError = false;
			}
			
			var index = searchJsonKeyInArray($scope.userAnswers, 'questionId', $scope.questions[n].id);
			if (index > -1) {
				$scope.userAnswers[index].answerId = $scope.questions[n].userAnswer.id;
				$scope.userAnswers[index].textAnswer = $scope.questions[n].userAnswer.textAnswer;
			}
		}
		if (questionsTotal == questionsAnswered) {
			$scope.questionsComplete = true;
		} else {
			$scope.questionsComplete = false;
		}
	}
	
	$scope.submitSQuestionnaire = function(submit, _form) {
		if (submit) {
			$scope.checkStatus(true);
		} else {
			$scope.checkStatus(false);
		}
		$scope.loading = true;
		
		var UpdateUserQuestionnaireAnswersRequest = {
			userName: userName, 
			writerId: writerId,
			userAnswers: $scope.userAnswers
		}
		$http.post(settings.backendJsonLink + 'QuestionnaireServices/updateUserQuestionnaireAnswers', UpdateUserQuestionnaireAnswersRequest)
			.then(function(data) {
				$rootScope.resetGlobalErrorMsg();
				$scope.loading = false;
				if (submit && $scope.questionsComplete) {
					$scope.updateSingleQuestionnaireDone(_form);
				}
			})
			.catch(function(data) {
				$rootScope.handleErrors(data, 'updateUserQuestionnaireAnswers')
			})
			.finally(function() {
				$scope.loading = false;
			})
	}
	
	var UserMethodRequest = {
		skinId: (!isUndefined($scope.userSkinId)) ? $scope.userSkinId : userSkinId,
		userName: userName,
		writerId: writerId
	}
	$scope.updateSingleQuestionnaireDone = function(_form) {
		if (!$scope.loading) {
			$scope.loading = true;
			
			$http.post(settings.backendJsonLink + 'QuestionnaireServices/updateSingleQuestionnaireDone', UserMethodRequest)
				.then(function(data) {
					$rootScope.resetGlobalErrorMsg();
					$scope.success = true;
				})
				.catch(function(data) {
					$rootScope.handleErrors(data, 'updateSingleQuestionnaireDone')
					if (data.errorCode >= 300 && data.errorCode <= 308) {
						$scope.success = true;
					}
				})
				.finally(function() {
					$scope.loading = false;
				})
		}
	}

}]);
