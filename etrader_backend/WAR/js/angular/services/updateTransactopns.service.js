/* service::updateTransactopnsService */
(function() {
	'use strict';

	angular.module('backendApp').service('updateTransactopnsService', updateTransactopnsService);

	updateTransactopnsService.$inject = ['$http', '$q'];
	function updateTransactopnsService($http, $q) {
		var _this = this;
		_this.service = 'UpdateTransactionServices/';
		
		_this.get = get;
		_this.update = update;
		
		
		function get(request) {
			var deferred = $q.defer();
			
			$http.post(settings.backendJsonLink + _this.service + 'getTransaction', request)
				.then(function(data) {
					deferred.resolve(data);
				})
				.catch(function(data) {
					deferred.reject(data);
				})
				
			return deferred.promise;
		}
		
		function update(request) {
			var deferred = $q.defer();
			
			$http.post(settings.backendJsonLink + _this.service + 'updateTransaction', request)
				.then(function(data) {
					deferred.resolve(data);
				})
				.catch(function(data) {
					deferred.reject(data);
				})
				
			return deferred.promise;
		}
	}
})();