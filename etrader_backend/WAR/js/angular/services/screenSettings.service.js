/* service::screenSettingsService */
(function() {
	'use strict';

	angular.module('backendApp').service('screenSettingsService', screenSettingsService);

	screenSettingsService.$inject = ['$http', '$q', 'utilsService'];
	function screenSettingsService($http, $q, utilsService) {
		var _this = this;
		
		_this.history = [];
	
		_this.getSettings = getSettings;
		
		
		function getSettings(serviceName, request, servicePrefix) {
			var deferred = $q.defer();
			
			if (!_this.history[serviceName]) {
				if (!request) {
					request = {};
				}
				if (!servicePrefix) {
					servicePrefix = settings.backendJsonLink;
				}
				
				$http.post(servicePrefix + serviceName, request)
					.then(function(data) {
						_this.history[serviceName] = data;
						
						if (data.permissionsList) {
							for (var i = 0; i < data.permissionsList.length; i++) {
								utilsService.permissions.allow(data.permissionsList[i]);
							}
						}
						
						deferred.resolve(data);
					})
					.catch(function(data) {
						deferred.reject(data);
					})
			} else {
				deferred.resolve(_this.history[serviceName]);
			}
			
			return deferred.promise;
		}
	};
})();

function resolveScreenSettings(serviceName, request, servicePrefix){
	return ['screenSettingsService', function(screenSettingsService){
		return screenSettingsService.getSettings(serviceName, request, servicePrefix);
	}];
}
