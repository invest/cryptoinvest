/* service::chargebackService */
(function() {
	'use strict';

	angular.module('backendApp').service('CryptoDepositService', CryptoDepositService);

	CryptoDepositService.$inject = ['$http', '$q'];
	function CryptoDepositService($http, $q) {
		var _this = this;
		
		_this.savedRequests = [];
		
		_this.get = get;
		_this.set = set;
		
		
		function get(serviceName, request) {
			var deferred = $q.defer();
			$http.post(settings.backendJsonLink + 'TransactionServices/' + serviceName, request)
				.then(function(data) {
					deferred.resolve(data);
				})
				.catch(function(data) {
					deferred.reject(data);
				})
				
			return deferred.promise;
		}
		
		function set(serviceName, request) {
			var deferred = $q.defer();
			
			$http.post(settings.backendJsonLink + 'TransactionServices/' + serviceName, request)
				.then(function(data) {
					deferred.resolve(data);
				})
				.catch(function(data) {
					deferred.reject(data);
				})
				
			return deferred.promise;
		}
	}
})();
