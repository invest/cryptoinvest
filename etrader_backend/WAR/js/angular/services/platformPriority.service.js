/* service::platformPriorityService */
(function() {
	'use strict';

	angular.module('backendApp').service('platformPriorityService', platformPriorityService);

	platformPriorityService.$inject = ['$http', '$q'];
	function platformPriorityService($http, $q) {
		var _this = this;
		
		_this.get = get;
		_this.update = update;
		
		
		function get() {
			var deferred = $q.defer();
			
			var request = {
				writerId: writerId
			}
			
			$http.post(settings.jsonLink + 'getLoginProducts', request)
				.then(function(data) {
					deferred.resolve(data);
				})
				.catch(function(data) {
					deferred.reject(data);
				})
				
			return deferred.promise;
		}
		
		function update(request) {
			var deferred = $q.defer();
			
			$http.post(settings.jsonLink + 'updateLoginProducts', request)
				.then(function(data) {
					deferred.resolve(data);
				})
				.catch(function(data) {
					deferred.reject(data);
				})
				
			return deferred.promise;
		}
	}
})();

platformPriorityPrepService.$inject = ['platformPriorityService'];
function platformPriorityPrepService(screenSettingsService) {
	return screenSettingsService.get();
}
