/* service::marketOptionPlusPricesService */
(function() {
	'use strict';

	angular.module('backendApp').service('marketOptionPlusPricesService', marketOptionPlusPricesService);

	marketOptionPlusPricesService.$inject = ['$http', '$q'];
	function marketOptionPlusPricesService($http, $q) {
		var _this = this;
		
		_this.getMarketName = getMarketName;
		_this.getOptionPlusPrices = getOptionPlusPrices;
		_this.updatePrice = updatePrice;
		
		
		function getMarketName(marketId, optionPlusMarkets){
			var result = '';
			
			for(var market in optionPlusMarkets){
				if(optionPlusMarkets.hasOwnProperty(market)){
					if(market == marketId){
						result = optionPlusMarkets[market];
						break;
					}
				}
			}
			
			return result;
		}
		
		function getOptionPlusPrices(request) {
			var deferred = $q.defer();
			
			$http.post(settings.backendJsonLink + 'OptionPlusPricesServices/getOptionPlusPrices', request)
				.then(function(data) {
					deferred.resolve(data);
				})
				.catch(function(data) {
					deferred.reject(data);
				})
				
			return deferred.promise;
		}
		
		function updatePrice(request) {
			var deferred = $q.defer();
			
			$http.post(settings.backendJsonLink + 'OptionPlusPricesServices/updatePrice', request)
				.then(function(data) {
					deferred.resolve(data);
				})
				.catch(function(data) {
					deferred.reject(data);
				})
				
			return deferred.promise;
		}
	}
})();
