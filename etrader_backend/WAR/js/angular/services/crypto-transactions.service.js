/* service::chargebackService */
(function() {
	'use strict';

	angular.module('backendApp').service('cryptoTransactionsService', cryptoTransactionsService);

	cryptoTransactionsService.$inject = ['$http', '$q'];
	function cryptoTransactionsService($http, $q) {
		var _this = this;
		
		_this.transactionTypes = {
			2: 'Bank transfer',
			70: 'Cardpay deposit'
		};
		
		_this.transactionStatus = {
			1: 'Started',
			2: 'Succed',
			3: 'Failed',
			7: 'Pending'
		};
		
		_this.cryptoStatuses = {
			0: 'Initiate',
			1: 'Ready for manual processing',
			10: 'Ready for processing',
			20: 'Locked',
			'-20': 'Processing failed',
			30: 'Processed',
			40: 'Closed'
		}
		
		_this.resolutionStatus = [
			{
				id: 1,
				name: 'Pending'
			}, {
				id: 2,
				name: 'Approved'
			}, {
				id: 3,
				name: 'Rejected'
			}
		];
		
		_this.rejectReasons = [
			{
				id: 0,
				value: "not.rejected"
			}, {
				id: 49,
				value: "file.rejected.third.party.deposit"
			}, {
				id: 50,
				value: "file.rejected.min.deposit"
			}, {
				id: 51,
				value: "file.rejected.max.daily.deposit"
			}, {
				id: 52,
				value: "file.rejected.max.monthly.deposit"
			}
		]

		_this.get = get;
		_this.set = set;
		_this.updateInsertFile = updateInsertFile;
		
		
		function get(serviceName, request) {
			var deferred = $q.defer();
			$http.post(settings.backendJsonLink + 'CryptoTransactionServices/' + serviceName, request)
				.then(function(data) {
					deferred.resolve(data);
				})
				.catch(function(data) {
					deferred.reject(data);
				})
				
			return deferred.promise;
		}
		
		function set(serviceName, request) {
			var deferred = $q.defer();
			
			$http.post(settings.backendJsonLink + 'CryptoTransactionServices/' + serviceName, request)
				.then(function(data) {
					deferred.resolve(data);
				})
				.catch(function(data) {
					deferred.reject(data);
				})
				
			return deferred.promise;
		}
		
		function updateInsertFile(request) {
			request.file.ccId = 0;
			request.file.controlRejectReason = 0;
			request.file.expDay = null;
			request.file.expMonth = null;
			request.file.expYear = null;
			request.file.fileStatusId = 1;
			request.file.id = 0;
			request.file.isCurrentDisable = true;
			request.file.ksStatusId = 0;
			request.file.rejectReason = 0;
			request.isLoadedFile = true;
			request.previousNext = 0;

			return $http.post(settings.backendJsonLink + 'FilesServices/updateInsertFile', request);
		}
	}
})();
