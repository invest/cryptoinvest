/* service::apmWithdrawalsService */
(function() {
	'use strict';

	angular.module('backendApp').service('apmWithdrawalsService', apmWithdrawalsService);

	apmWithdrawalsService.$inject = ['$http', '$q'];
	function apmWithdrawalsService($http, $q) {
		var _this = this;
		
		_this.withdrawTypes = {
			skrill: 36,
			direct24: 59,
			giropay: 60,
			eps: 61,
			ideal: 63
		};
		
		
		_this.submitWithdraw = submitWithdraw;
		_this.approveWithdraw = approveWithdraw;
		
		
		function submitWithdraw(request, withdrawType) {
			if(withdrawType == _this.withdrawTypes.skrill){
				return submitSkrillWithdraw(request);
			}else{
				return submitGMWithdraw(request);
			}
		}
		
		function submitSkrillWithdraw(request) {
			var deferred = $q.defer();
			
			$http.post(settings.backendJsonLink + 'APMWithdrawalsServices/insertMoneybookersWithdraw', request)
				.then(function(data) {
					deferred.resolve(data);
				})
				.catch(function(data) {
					deferred.reject(data);
				})
				
			return deferred.promise;
		}
		
		function submitGMWithdraw(request) {
			var deferred = $q.defer();
			
			$http.post(settings.commonJsonLink + 'GmWithdrawalServices/validateGMWithdraw', request)
				.then(function(data) {
					deferred.resolve(data);
				})
				.catch(function(data) {
					deferred.reject(data);
				})
				
			return deferred.promise;
		}
		
		function approveWithdraw(request, withdrawType) {
			if(withdrawType == _this.withdrawTypes.skrill){
				return approveSkrillWithdraw(request);
			}else{
				return insertGmWithdraw(request);
			}
		}
		
		function approveSkrillWithdraw(request) {
			var deferred = $q.defer();
			
			$http.post(settings.backendJsonLink + 'APMWithdrawalsServices/moneybookersApprove', request)
				.then(function(data) {
					deferred.resolve(data);
				})
				.catch(function(data) {
					deferred.reject(data);
				})
				
			return deferred.promise;
		}
		
		function insertGmWithdraw(request) {
			var deferred = $q.defer();
			
			$http.post(settings.commonJsonLink + 'GmWithdrawalServices/insertGmWithdraw', request)
				.then(function(data) {
					deferred.resolve(data);
				})
				.catch(function(data) {
					deferred.reject(data);
				})
				
			return deferred.promise;
		}
	}
})();
