/* service::tradersActionsService */
(function() {
	'use strict';

	angular.module('backendApp').service('tradersActionsService', tradersActionsService);

	tradersActionsService.$inject = ['$http', '$q'];
	function tradersActionsService($http, $q) {
		var _this = this;
		
		_this.getList = getList;
		
		
		function getList(request) {
			var deferred = $q.defer();
			
			$http.post(settings.backendJsonLink + 'TradersActionsServices/getTradersActions', request)
				.then(function(data) {
					deferred.resolve(data);
				})
				.catch(function(data) {
					deferred.reject(data);
				})
				
			return deferred.promise;
		}
	}
})();
