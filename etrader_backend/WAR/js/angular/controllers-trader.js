backendApp.controller('HomePagePriorityCtr', ['$rootScope', '$scope', '$http', function($rootScope, $scope, $http) {
	$scope.filter = {};
	$scope.filter.skins = [];
	for(var skin in skinMap){
		if(skinMap.hasOwnProperty(skin)){
			if(skin != 1 && skinMap[skin].isActive){
				$scope.filter.skins.push({id: skin, name: $rootScope.getMsgs(skinMap[skin].skinTranslation)});
			}
		}
	}
	$scope.filter.skin = $scope.filter.skins[0];
	$scope.selectedSkin = '';
	$scope.markets = [];
	$scope.marketIds = [];
	
	$scope.initWatch = $scope.$watch(function(){return $rootScope.ready;}, function(){
		if($rootScope.ready){
			$scope.initWatch();
			$scope.init();
		}
	});
	
	$scope.init = function(){
		//$scope.getSkinMarketsPriorities();
	}
	
	
	$scope.getSkinMarketsPriorities = function(){
		showLoading();
		var MethodRequest = {};
		MethodRequest.skinId = $scope.filter.skin.id;
		$http.post(settings.jsonLink + 'getSkinMarketsPriorities', MethodRequest)
			.then(function(data) {
				$rootScope.resetGlobalErrorMsg();
				$scope.markets = data.markets;
				$scope.selectedSkin = $scope.filter.skin;
			})
			.catch(function(data) {
				$rootScope.handleErrors(data, 'getSkinMarketsPriorities');
			})
			.finally(function() {
				hideLoading();
			})
	}
	
	$scope.updateMarketsPriorities = function(){
		showLoading();
		var MethodRequest = {};
		$scope.marketIds = [];
		for(var i = 0; i < $scope.markets.length; i++){
			$scope.marketIds.push($scope.markets[i].skinMarketGroupMarketId);
		}
		MethodRequest.skinMarketGroupMarketIds = $scope.marketIds;
		$http.post(settings.jsonLink + 'updateMarketsPriorities', MethodRequest)
			.then(function(data) {
				$rootScope.resetGlobalErrorMsg();
				showToast(TOAST_TYPES.info, $rootScope.getMsgs('changes.saved.successfully'));
			})
			.catch(function(data) {
				showToast(TOAST_TYPES.error, $rootScope.getMsgs('changes.saved.error'));
				$rootScope.handleErrors(data, 'updateMarketsPriorities');
			})
			.finally(function() {
				hideLoading();
			})
	}
	
}]);