/*shorted document.getElementById() it's only g() in order to be used very easy even inline*/
function g(id){return document.getElementById(id);}


if (!Array.prototype.indexOf) {
  Array.prototype.indexOf = function (searchElement /*, fromIndex */ ) {
	'use strict';
	if (this == null) {
	  throw new TypeError();
	}
	var n, k, t = Object(this),
		len = t.length >>> 0;

	if (len === 0) {
	  return -1;
	}
	n = 0;
	if (arguments.length > 1) {
	  n = Number(arguments[1]);
	  if (n != n) { // shortcut for verifying if it's NaN
		n = 0;
	  } else if (n != 0 && n != Infinity && n != -Infinity) {
		n = (n > 0 || -1) * Math.floor(Math.abs(n));
	  }
	}
	if (n >= len) {
	  return -1;
	}
	for (k = n >= 0 ? n : Math.max(len - Math.abs(n), 0); k < len; k++) {
	  if (k in t && t[k] === searchElement) {
		return k;
	  }
	}
	return -1;
  };
}

function cloneObj(obj){
	if(obj == null || typeof(obj) != 'object'){
		return obj;
	}
	if(obj instanceof Date){
		return new Date(obj);
	}

	var temp = obj.constructor(); // changed

	for(var key in obj){
		temp[key] = cloneObj(obj[key]);
	}
	return temp;
}

//check where ever something is undefined or null
function isUndefined(el) {
	return (typeof el == 'undefined' || el == null);
}

//use ?logType=num to load page with loggin on
//0:do not log,1:log info,2:log warnings,3:log errors,4:log all
var logIt_type_timer = ''; 
function logIt(params){//type,msg
	if((params.type === 1) && (settings.logIt_type === 1 || settings.logIt_type === 4)){//info
		console.info(params.msg);
	}
	else if((params.type === 2) && (settings.logIt_type === 2 || settings.logIt_type === 4)){//warnings
		console.warn(params.msg);
	}
	else if((params.type == 3) && (settings.logIt_type === 3 || settings.logIt_type === 4)){//errors
		console.error(params.msg);
	}
	// console.log('test.log') - just info
	// console.debug('test.debug') - just info
	// console.info('test.info') - with info ico
	// console.warn('test.warn') - with warning info
	// console.error('test.error') - with error ico
	if(logIt_type_timer == null){ 
		logIt_type_timer = setInterval(function(){console.clear()},1000*60*5);
	}
}

var error_stack = [];
function handleErrors(data, name) {//returns true on error
	angular.element('#backendAppHolder').scope().requestsStatus[data.serviceName] = 1;
	if (data == '') {
		logIt({'type':3,'msg':name});
		logIt({'type':3,'msg':data});
		alert('Response is empty string. Check eclipse console for exeptions.');
		return true;
	} else if (data.errorCode == null) {
		logIt({'type':3,'msg':name});
		logIt({'type':3,'msg':data});
		alert('Error code is null. Check eclipse console for exeptions. ');
		return true;
	} else if (data.errorCode > errorCodeMap.success) {//0
		var defMsg = angular.element('#backendAppHolder').scope().getMsgs('error-' + data.errorCode);
		logIt({'type':3,'msg':name + " (" + defMsg + ")"});
		logIt({'type':3,'msg':data});
		var globalErrorField;
		if (typeof data.globalErrorField != 'undefined') {
			globalErrorField = g(data.globalErrorField);
		} else if (g('globalErrorField') != null) {
			globalErrorField = g('globalErrorField');
		} else {
			globalErrorField = document.createElement('span');
			globalErrorField.id = 'globalErrorField';
			document.body.appendChild(globalErrorField);
		}
		globalErrorField.innerHTML = '';
		resetErrorMsgs(data);
		if (data.errorCode == errorCodeMap.session_expired) { //session expired 6999
			if (isUndefined(data.loginPage)) {
				try {
					/*angular.element('#backendAppHolder').scope().changeHeader(false);
					angular.element('#backendAppHolder').scope().$state.go('ln.login', {ln: settings.skinLanguage});
					angular.element('#backendAppHolder').scope().initSettings();*/
					window.location.hash = '';
					window.location.replace(window.location.pathname.replace(/index.html/, ''));
				} catch(e) {}
			}
		} else if(data.errorCode == errorCodeMap.date_range_too_large){
			addGlobalErrorMsg(angular.element('#backendAppHolder').scope().getMsgs('sales.deposits.date.range'));
		} else if(data.errorCode == errorCodeMap.role_name_exists){
			addGlobalErrorMsg(angular.element('#backendAppHolder').scope().getMsgs('permissions.role.name.exists'));
		} else {
			if (data.userMessages != null) {
				for (var i = 0; i < data.userMessages.length; i++) {
					if (data.userMessages[i].field == '' || data.userMessages[i].field == null) {
						globalErrorField.innerHTML += "<span>" + data.userMessages[i].message + "</span>";
					} else {
						var input = g(data.userMessages[i].field);
						if (input != null) {
							input.className += " error";
						}
						var error_el_id = data.userMessages[i].field + '-error';
						error_stack.push(data.userMessages[i].field);
						var err_place = g(error_el_id);
						if (err_place != null) {
							err_place.innerHTML = data.userMessages[i].message;
						} else {
							var field_place = g(data.userMessages[i].field + '-container');
							if (field_place != null) {
								var el = document.createElement('span');
								el.id = error_el_id;
								el.className = 'error-message icon';
								el.innerHTML = data.userMessages[i].message;
								field_place.appendChild(el);
							} else {
								globalErrorField.innerHTML += "<span>" + data.userMessages[i].message + "</span>";
							}
						}
					}
				}
			} else {
				logIt({'type':3,'msg':'unhandled errorCode: ' + data.errorCode});
				globalErrorField.innerHTML += "<span>" + defMsg + "</span>";
			}
		}
		return true;
	} else {
		logIt({'type':1,'msg':data});
		return false;
	}
}

function handleSuccess(data) {
	if (data.userMessages != null && data.userMessages[0].message != null) {
		alert(data.userMessages[0].message);
	} else {
		alert('Great, it works!')
	}
	logIt({'type':1,'msg':data});
}

function handleNetworkError(data) {
	// alert('Network error (aka 404, 500 etc)');
	logIt({'type':1,'msg':data});
}

function resetErrorMsgs(data) {
	if (typeof data.stopClear == 'undefined' || !data.stopClear) {
		for (var i = 0; i < error_stack.length; i++) {
			var error_holder = g(error_stack[i]);
			if (error_holder != null) {
				g(error_stack[i]).className = g(error_stack[i]).className.replace(/ error/g, '');
			}
			var error_holder = g(error_stack[i] + '-error');
			if (error_holder != null) {
				g(error_stack[i] + '-error').remove();
			}
		}
		error_stack = [];
	}
}

function addGlobalErrorMsg(data){
	$('[data-global-errors]').html('<span class="error">' + data + '</span>');
}

function resetGlobalErrorMsg(){
	$('[data-global-errors]').html('');
}

function searchJsonKeyInArray(arr, key, val) {
	if (!isUndefined(arr)) {
		for (var i = 0; i < arr.length; i++) {
			if (arr[i][key] == val) {
				return i;
			}
		}
	}
	return -1;
}
function searchJsonKeyInJson(obj, key, val) {
	if (!isUndefined(obj)) {
		for (var el in obj) {
			if (obj[el][key] == val) {
				return el;
			}
		}
	}
	return -1;
}
function formatAmount(params) {//centsPart: 1-upper span,2-no cents if 00,0-default show full format with .00,3- upper span without 00
	var amount = params.amount;
	var currency = params.currency;
	var centsPart = params.centsPart;
	var withoutDecimalPoint = params.withoutDecimalPoint;
	var round = params.round;
	try{
		if (!isNaN(amount)) {
			if(typeof amountDivideBy100 != 'undefined' && amountDivideBy100){
				amount = amount/100;
			}
			var rtn = '';
			if (amount.toString().charAt(0) == '-') {
				rtn = '-';
				amount *= -1;
			}
			if (typeof withoutDecimalPoint != 'undefined' && withoutDecimalPoint) {
				amount = amountToFloat(amount);
			}
			if (typeof round == 'undefined' || !round || amount < 1000 ) {
				amount = Math.round(amount*100)/100;
				var tmp = amount.toString().split('.');
				var decimal = parseInt(tmp[0]);
				var cents = (typeof tmp[1] != 'undefined')?tmp[1]:0;
				if((cents === '') || (cents === 0)){cents = '00';}
				else if(cents.length == '1'){cents = cents + '0';}
				else if(cents.length == '2' && parseInt(cents) < 10){cents = '0' + parseInt(cents);}
				
				if(currency.currencyLeftSymbol){
					rtn += currency.currencySymbol;
				}
				rtn += numberWithCommas(decimal);
				if(currency.decimalPointDigits == 0){
					//Absolutly nothing. Just chill!
				}
				else if((centsPart == 1) || ((centsPart == 3) && (parseInt(cents) != 0))){
					rtn += "<span>"+cents+"</span>";
				}
				else if((centsPart == 3) && (parseInt(cents) == 0)){
					//Absolutly nothing. Just chill!
				}
				else if((centsPart == 2) && (parseInt(cents) == 0)){
					//Absolutly nothing. Just chill!
				}
				else{
					rtn += "."+cents;
				}
				if(!currency.currencyLeftSymbol){
					rtn += currency.currencySymbol;
				}
			} else {
				if(currency.currencyLeftSymbol){
					rtn += currency.currencySymbol;
				}
				rtn += (Math.round(amount / 100)) / 10 + "K";
				if(!currency.currencyLeftSymbol){
					rtn += currency.currencySymbol;
				}
			}
			return rtn;
		} else {
			return ' ';
		}
	} catch(e) {
		logIt({'type':3,'msg':e});
	}
}

function numberWithCommas(x) {//convert value with camas every 1000
	x = x.toString().replace(/,/g,'');
	var tmp = x.split('.');
	var rtn = tmp[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
	if(typeof tmp[1] != 'undefined'){
		rtn += '.' + tmp[1];
	}
	return rtn;
}

function checkOwnProperty(obj, property){
	if(obj.hasOwnProperty(property) && obj[property]){
		return true;
	}else{
		return false;
	}
}

var loadingOverlayId = 'loadingOverlay';
function showLoading(globalOverlay){
	if(document.getElementById('tab-content') && !document.getElementById(loadingOverlayId)){
		var overlay = document.createElement('div');
		overlay.id = loadingOverlayId;
		overlay.className = 'loading-overlay';
		if(globalOverlay){
			overlay.className += ' loading-overlay-global';
		}
		document.getElementById('tab-content').appendChild(overlay);
	}
}
function hideLoading(){
	if(document.getElementById(loadingOverlayId)){
		document.getElementById(loadingOverlayId).parentNode.removeChild(document.getElementById(loadingOverlayId));
	}
}

function showToast(type, message){
	var toast = document.createElement('div');
	toast.className = 'toast';
	if(type == TOAST_TYPES.error){
		toast.className += ' toast_error';
	}
	toast.innerHTML = message;
	toast.onclick = function(){removeToast(toast, true);};
	document.body.appendChild(toast);
	setTimeout(function(){removeToast(toast, false);}, 3000);
}
function removeToast(toast, forceClose){
	var removeDelay = 0.5;
	if(toast && !toast.isBeingRemoved){
		toast.isBeingRemoved = true;
		$(toast).css('visibility', 'hidden');
		$(toast).css('opacity', '0');
		$(toast).css('transition', 'visibility 0s ' + removeDelay + 's, opacity ' + removeDelay + 's linear');
		$(toast).on('transitionend msTransitionEnd webkitTransitionEnd',function(){
			if(toast){
				toast.parentNode.removeChild(toast);
				toast = null;
			}
		});
		//Backup for browsers where transitionend didn't fire
		setTimeout(function(){
			if(toast){
				if(toast.parentNode){
					toast.parentNode.removeChild(toast);
				}
				toast = null;
			}
		}, (removeDelay + 1)*1000);
	}else if(toast && toast.isBeingRemoved && forceClose){
		toast.parentNode.removeChild(toast);
		toast = null;
	}
}
function detectIE() {
    var ua = window.navigator.userAgent;

    var msie = ua.indexOf('MSIE ');
    if (msie > 0) {
        // IE 10 or older => return version number
        return parseInt(ua.substring(msie + 5, ua.indexOf('.', msie)), 10);
    }

    var trident = ua.indexOf('Trident/');
    if (trident > 0) {
        // IE 11 => return version number
        var rv = ua.indexOf('rv:');
        return parseInt(ua.substring(rv + 3, ua.indexOf('.', rv)), 10);
    }

    var edge = ua.indexOf('Edge/');
    if (edge > 0) {
       // Edge (IE 12+) => return version number
       return parseInt(ua.substring(edge + 5, ua.indexOf('.', edge)), 10);
    }

    // other browser
    return false;
}
function exportSearch(tableId) {
	var dataToExport = '<table>';
	var hiddenColumns = [];
	
	$('#' + tableId + ' > thead').each(function() {
		dataToExport += "<thead>";
		$(this).find('tr').each(function() {
			dataToExport += "<tr>";
			
			$(this).find('th,td').each(function(index) {
				if ($(this).hasClass('_noExport')) {
					hiddenColumns.push(index);
				} else {
					dataToExport += "<th>" + $(this).text() + "</th>";
				}
			});
			
			dataToExport += "</tr>";
		});
	    
		dataToExport += "</thead>";
	});
	
	$('#' + tableId + ' > tbody').each(function() {
		dataToExport += "<tbody>";
		$(this).find('tr').each(function() {
			if ($(this).css("display") != "none") {
				dataToExport += "<tr>";
				
				$(this).find('th,td').each(function(index) {
					if ($.inArray(index, hiddenColumns) === -1) {
						dataToExport += "<th>" + $(this).text() + "</th>";
					}
				});
				
				dataToExport += "</tr>";
			}
		});
	    
		dataToExport += "</tbody>";
	});
    
    dataToExport += '</table>';
    
	download(dataToExport, 'download.xls', 'application/vnd.ms-excel')
}
function exportSearchCSV(tableId) {
	var dataToExport = '"sep=;"\n';
	var hiddenColumns = [];
	
	$('#' + tableId + ' > thead').each(function() {
		$(this).find('>tr').each(function() {
			$(this).find('>th,>td').each(function(index) {
				if ($(this).hasClass('_noExport')) {
					hiddenColumns.push(index);
				} else {
					dataToExport += $(this).text().trim() + ";";
				}
			});
			dataToExport += '\n';
		});
	});
	
	$('#' + tableId + ' > tbody').each(function() {
		$(this).find('>tr').each(function() {
			if ($(this).css("display") != "none") {
				$(this).find('>th,>td').each(function(index) {
					if ($.inArray(index, hiddenColumns) === -1) {
						dataToExport += $(this).text().trim() + ";";
					}
				});
				dataToExport += '\n';
			}
		});
	});
    
    download(dataToExport, 'download.csv', 'data:text/csv;charset=utf-8')
}
function exportListCSV(listId) {
	var dataToExport = '"sep=;"\n';
	
	$('#' + listId + ' > li').each(function() {
		dataToExport += $(this).text().trim() + ";";
		dataToExport += '\n';
	});
    
    download(dataToExport, 'download.csv', 'data:text/csv;charset=utf-8')
}
function download(strData, strFileName, strMimeType) {
    var D = document,
        a = D.createElement("a");
        strMimeType= strMimeType || "application/octet-stream";


    if (navigator.msSaveBlob) { // IE10
        return navigator.msSaveBlob(new Blob([strData], {type: strMimeType}), strFileName);
    } /* end if(navigator.msSaveBlob) */


    if ('download' in a) { //html5 A[download]
        a.href = "data:" + strMimeType + "," + encodeURIComponent(strData);
        a.setAttribute("download", strFileName);
        a.innerHTML = "downloading...";
        D.body.appendChild(a);
        setTimeout(function() {
            a.click();
            D.body.removeChild(a);
        }, 66);
        return true;
    } /* end if('download' in a) */


    //do iframe dataURL download (old ch+FF):
    var f = D.createElement("iframe");
    D.body.appendChild(f);
    f.src = "data:" +  strMimeType   + "," + encodeURIComponent(strData);

    setTimeout(function() {
        D.body.removeChild(f);
    }, 333);
    return true;
}

function detectCCtype(card, fullCheck){
	var cardTypes = {
		unknown: 'unknown',
		visa: 'visa',
		mastercard: 'mastercard',
		solo: 'solo',
		switch_type: 'switch',
		maestro: 'maestro',
		amex: 'amex',
		discover: 'discover',
		diners: 'diners',
		isracard: 'isracard',
		allcards: 'allcards',
		cup: 7
	}
	card = card.toString().replace(/-/g,'').replace(/ /g,'');
	//visa - starts with 4
	var part = parseInt(card.substr(0, 1));
	if(part == 4){
		if(fullCheck){
			if((card.length == 13 || card.length == 16) && luhnValidate(card)){
				return cardTypes.visa;
			}
			else{
				return cardTypes.unknown;
			}
		}
		return cardTypes.visa;
	}
	//Mastercard - starts with 51,52,53,54,55
	var part = parseInt(card.substr(0, 2));
	if(part >= 51 && part <= 55){
		if(fullCheck){
			if(card.length == 16 && luhnValidate(card)){
				return cardTypes.mastercard;
			}
			else{
				return cardTypes.unknown;
			}
		}
		return cardTypes.mastercard;
	}
	//Solo - starts with 6334,6767
	var part = parseInt(card.substr(0, 4));
	if(part == 6334 || part == 6767){
		if(fullCheck){
			if(card.length >= 16 && card.length <= 19 && luhnValidate(card)){
				return cardTypes.solo;
			}
			else{
				return cardTypes.unknown;
			}
		}
		return cardTypes.solo;
	}
	//Switch - starts with 4903,4905,4911,4936,6333,6759 or 564182,633110
	var part_arr = [4903,4905,4911,4936,6333,6759];
	var part2_arr = [564182,633110];
	var part = parseInt(card.substr(0, 4));
	var part2 = parseInt(card.substr(0, 6));
	if(part_arr.indexOf(part) > -1 || part2_arr.indexOf(part2) > -1){
		if(fullCheck){
			if(card.length >= 16 && card.length <= 19 && luhnValidate(card)){
				return cardTypes.switch_type;
			}
			else{
				return cardTypes.unknown;
			}
		}
		return cardTypes.switch_type;
	}
	//Maestro - starts with 50,56,57,58,59,60,62,63,64,67,90
	var part_arr = [50,56,57,58,59,60,62,63,64,67,90];
	var part = parseInt(card.substr(0, 2));
	if(part_arr.indexOf(part) > -1){
		if(fullCheck){
			if(card.length >= 16 && card.length <= 18 && luhnValidate(card)){
				return cardTypes.maestro;
			}
			else{
				return cardTypes.unknown;
			}
		}
		return cardTypes.maestro;
	}
	//Amex - starts with 34,37
	var part_arr = [34,37];
	var part = parseInt(card.substr(0, 2));
	if(part_arr.indexOf(part) > -1){
		if(fullCheck){
			if(card.length == 15 && luhnValidate(card)){
				return cardTypes.amex;
			}
			else{
				return cardTypes.unknown;
			}
		}
		return cardTypes.amex;
	}
	//Discover - starts with 6011
	var part_arr = [6011];
	var part = parseInt(card.substr(0, 4));
	if(part_arr.indexOf(part) > -1){
		if(fullCheck){
			if(card.length == 16 && luhnValidate(card)){
				return cardTypes.discover;
			}
			else{
				return cardTypes.unknown;
			}
		}
		return cardTypes.discover;
	}
	//Diners - starts with 300,301,302,303,304,305 or 36
	var part_arr = [300,301,302,303,304,305];
	var part2_arr = [36];
	var part = parseInt(card.substr(0, 3));
	var part2 = parseInt(card.substr(0, 2));
	if(part_arr.indexOf(part) > -1 || part2_arr.indexOf(part2) > -1){
		if(fullCheck){
			if(card.length == 14 && luhnValidate(card)){
				return cardTypes.diners;
			}
			else{
				return cardTypes.unknown;
			}
		}
		return cardTypes.diners;
	}
	//CUP - starts with 62
	var part_arr = [62];
	var part = parseInt(card.substr(0, 2));
	if(part_arr.indexOf(part) > -1){
		if(fullCheck){
			if(card.length >= 16 && card.length <= 19 && luhnValidate(card)){
				return cardTypes.cup;
			}
			else{
				return cardTypes.unknown;
			}
		}
		return cardTypes.diners;
	}
	//Isracard - complicated :)
	if(fullCheck){
		var cardIsr = card;
		if(cardIsr.length == 8){
			cardIsr = "0" + cardIsr;
		}
		else if(cardIsr.length > 9){
			cardIsr = cardIsr.substr(0, 9);
		}
		
		if(cardIsr.length == 9){
			var diff = "987654321",
				sum = 0,
				a = 0,
				b = 0,
				c = 0;
			for(var i=1;i<9;i++){
				sum += parseInt(diff.substr(i,1))*parseInt(cardIsr.substr(i,1));
			}
			if(sum % 11 == 0){
				return cardTypes.isracard;
			}
		}
	}
	//allcard
	if(card.length >= 12 && card.length <= 20 && luhnValidate(card)){
		return cardTypes.allcards;
	}
	return cardTypes.unknown;
}

// The Luhn algorithm is basically a CRC type
// system for checking the validity of an entry.
// All major credit cards use numbers that will
// pass the Luhn check. Also, all of them are based
// on MOD 10.

function luhnValidate(value) {
  // accept only digits, dashes or spaces
	if (/[^0-9-\s]+/.test(value)) return false;
 
	// The Luhn Algorithm. It's so pretty.
	var nCheck = 0, nDigit = 0, bEven = false;
	value = value.replace(/\D/g, "");
 
	for (var n = value.length - 1; n >= 0; n--) {
		var cDigit = value.charAt(n),
			  nDigit = parseInt(cDigit, 10);
 
		if (bEven) {
			if ((nDigit *= 2) > 9) nDigit -= 9;
		}
 
		nCheck += nDigit;
		bEven = !bEven;
	}
 
	return (nCheck % 10) == 0;
}
function jsonClone(o1, o2) {
	var json = cloneObj(o1);
	for (var key in o2) {
		json[key] = o2[key];
	}
	return json;
}
function amountToFloat(amount) {
	return (amount/Math.pow(10, currencyDecimalPoint));
}