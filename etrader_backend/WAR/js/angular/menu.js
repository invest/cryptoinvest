var menu = [{
	title: 'transactions',
	nodes: [
		{
			permission: 'transactions_view',
			link: 'ln.transaction.transactions',
			title: 'transactions',	
		},{
			permission: 'updateTransaction_view',
			link: 'ln.transaction.update-transactions',
			title: 'update-transaction',	
		},{
			title: 'pendingWithdrawals',
			nodes: [
					{
						permission: 'pendingWithdrawalFirstApprove_view',
						link: 'ln.transaction.pendingWithdrawals_firstApprove',
						title: 'firstApprove'
					},
					{
						permission: 'pendingWithdrawalSecondApprove_view',
						link: 'ln.transaction.pendingWithdrawals_secondApprove',
						title: 'secondApprove'
					},
					{
						permission: 'pendingWithdrawalSupport_view',
						link: 'ln.transaction.pendingWithdrawals_support',
						title: 'support'
					}
				]
		},{
			permission: 'cryptoDeposits_view',
			link: 'ln.transaction.crypto-deposit',
			title: 'crypto-deposits',	
		}
	]
},{
	title: 'content',
	nodes: [
		{
			permission: 'terms_view',
			link: 'ln.content.terms',
			title: 'terms'
		},{
			permission: 'academyFiles_view',
			link: 'ln.content.academy-articles',
			title: 'academy-articles'
		}
	]
},/*{
	title: 'users',
	nodes: [
		{
			permission: 'file_view',
			link: 'ln.users.files',
			title: 'files'
		}
	]
},*/{
	title: 'documents',
	nodes: [
		/*{
			permission: 'idsFile_view',
			link: 'ln.documents.ids_files',
			title: 'ids'
		},{
			permission: 'callCenter_view',
			link: 'ln.documents.call_center',
			title: 'call-center'
		},{
			permission: 'backOffice_view',
			link: 'ln.documents.back_office',
			title: 'back-office'
		},{
			permission: 'fromInbox_view',
			link: 'ln.documents.from-inbox',
			title: 'from-inbox'
		},*/{
			permission: 'fromInbox_view',
			link: 'ln.documents.user-documents',
			title: 'user-documents'
		}
	]
},{
	title: 'administrator',
	nodes: [
		{
			permission: 'writers_view',
			link: 'ln.admin.writers',
			title: 'writers'
		},{
			permission: 'permissions_view',
			link: 'ln.admin.permissions',
			title: 'permissions'
		}
	]
},{
	title: 'risk',
	nodes: [
		{
			permission: 'chargebacks_view',
			link: 'ln.risk.chargebacks',
			title: 'chargebacks'
		}
	]
},{
	title: 'menu.trader',
	nodes: [
		{
			title: 'markets',
			nodes: [
				{
					permission: 'createMarket_view',
					link: 'ln.admin.markets.create_market',
					title: 'create-market'
				},{
					permission: 'marketFields_view',
					link: 'ln.admin.markets.market_fields',
					title: 'market-fields'
				},{
					permission: 'marketFormulas_view',
					link: 'ln.admin.markets.market_formulas',
					title: 'market-formulas'
				},{
					permission: 'marketTranslations_view',
					link: 'ln.admin.markets.market_translations',
					title: 'market-translations'
				},{
					permission: 'marketSchedule_view',
					link: 'ln.admin.markets.market_opportunity_templates',
					title: 'market-opportunity-templates'
				},{
					permission: 'marketPriorities_view',
					link: 'ln.admin.markets.market_priorities',
					title: 'market-priorities'
				},{
					permission: 'marketPauses_view',
					link: 'ln.admin.markets.market_pauses',
					title: 'market-pauses'
				},{
					permission: 'dynamicsQuoteParams_view',
					link: 'ln.admin.markets.market_dynamics_quotes',
					title: 'market-dynamics-quotes'
				},{
					permission: 'optionPlusPrices_view',
					link: 'ln.admin.markets.market-option-plus-prices',
					title: 'market-option-plus-prices'
				}
			]
		},{
			permission: 'tradersActions_view',
			link: 'ln.trader.traders-actions',
			title: 'traders-actions',	
		}
	]

},{
	title: 'users',
	nodes: [
		{
			permission: 'cryptoLeads_view',
			link: 'ln.users.crypto-leads',
			title: 'crypto-leads'
		}
	]
},{
	title: 'menu.postman',
	nodes: [
		{
			permission: 'permissions_view',
			link: 'ln.postman.post',
			title: 'menu.postman'
		}
	]
}];