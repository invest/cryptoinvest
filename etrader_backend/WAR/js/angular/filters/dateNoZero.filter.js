/* filter::dateNoZero */
(function() {
	'use strict';

	angular.module('backendApp').decorator('dateFilter', dateNoZero);

	dateNoZero.$inject = ['$delegate'];
	function dateNoZero($delegate) {
		return extendsFilter;
		
		function extendsFilter() {
			if (arguments[0] == 0) {
				arguments[0] = null;
			} else if (typeof arguments[0] === 'string') {
				var testString = new Date(arguments[0]);
				if (isNaN(testString.getTime())) {
					arguments[0] = null;
				} else {
					arguments[0] = testString;
				}
			}
			if (arguments[1] == 'defaultDateTime') {
				arguments[1] = 'HH:mm dd/MM/yyyy';
			} else if (arguments[1] == 'defaultDate') {
				arguments[1] = 'dd/MM/yyyy';
			}
			return $delegate.apply(this, arguments);
		}
	}
})();