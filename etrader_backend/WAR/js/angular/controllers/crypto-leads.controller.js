/* controller::CryptoLeadsController */
(function() {
	'use strict';

	angular.module('backendApp').controller('CryptoLeadsController', CryptoLeadsController);

	CryptoLeadsController.$inject = ['screenSettingsPrepService', 'utilsService', 'CryptoLeadsService', '$rootScope', '$window'];
	function CryptoLeadsController(screenSettingsPrepService, utilsService, CryptoLeadsService, $rootScope, $window) {
		var _this = this;
		_this._u = utilsService;

		this.amountSettings = {centsPart: 2};
		this.action = '';

		var screenSettings = screenSettingsPrepService;
		_this.assignees = screenSettings.screenFilters.assignees;
		_this.transactionStatuses = screenSettings.screenFilters.transactionStatuses;
		_this.transactionTypes = screenSettings.screenFilters.transactionTypes;

		_this.results = null;

		//filters
		this.filter = {
			fromDate: _this._u.setDateNoSec({month: -1}),
			toDate: _this._u.setDateNoSec(),
			userId: '',
			email: '',
			firstName: '',
			lastName: '',
			mobilePhone: '',
			searchBy: new _this._u.SingleSelect.InstanceClass({
				aggregateOption: {id: -1, name: 'all'},
				options: {
					1: 'Registered',
					2: 'Last note'
				}
			}),
			countries: new _this._u.Multiselect.InstanceClass({
				aggregateOption: {id: -1, name: 'all'},
				mutateCallback: _this._u.getMsg,
				isSmart: true,
				options: screenSettings.screenFilters.countries,
				iconCallback: function(option){return settings.jsonImagegLink + 'images/flags/' + option.id + '.png';}
			}),
			kycStatus: new _this._u.SingleSelect.InstanceClass({
				aggregateOption: {id: -1, name: 'all'},
				options: screenSettings.screenFilters.kycStatus,
			}),
			depositsMade: new _this._u.SingleSelect.InstanceClass({
				aggregateOption: {id: -1, name: 'all'},
				options: screenSettings.screenFilters.depositMade,
			}),
			resultsPerPage: new _this._u.SingleSelect.InstanceClass({
				sortById: true,
				options: _this._u.resultsPerPage
			})
		};

		//set default loaded
		this.filter.resultsPerPage.setModel(this.filter.resultsPerPage.getOptionById(15));

		//options
		_this.minDate = _this._u.setDateNoSec({offset: _this._u.writer.utcOffsetHours, setYears: -20});
//		_this.maxDate = _this._u.setDateNoSec({offset: _this._u.writer.utcOffsetHours});//BAC-4114

		_this.openDates = {
			fromDate: false,
			toDate: false
		}

		_this.dateOptions = {
			showWeeks: false,
			startingDay: 0,
			defaultTime: '00:00:00'
		}

		this.pagination = new _this._u.Pagination.InstanceClass({
			callback: changePage,
			hideGroupNavigation: true
		});


		_this.getCryptoLeads = getCryptoLeads;
		_this.changePage = changePage;
		_this.openCalendar = openCalendar;

		//functions
		function getCryptoLeads(_form, page) {
			var paging = false;
			if (!isUndefined(_form)) {
				_this._form = _form;
			} else {
				_form = _this._form;
				paging = true;
			}

			if((page = _this.pagination.validatePage(page)) === false){
				return;
			}

			if (_form.$valid || paging) {
				_this.currentPage = page;
				_this._u.resetGlobalErrorMsg();
				_this._u.showLoading();

				var request = {
					from: _this.filter.fromDate.getTime(),
					to: _this.filter.toDate.getTime(),
					searchBy: _this._u.checkNull(_this.filter.searchBy.getId(), 'id'),
					countries:  _this._u.checkNull(_this.filter.countries.getCheckedIds(), 'ids'),
					kycStatus: _this._u.checkNull(_this.filter.kycStatus.getId(), 'id'),
					depositsMade: _this._u.checkNull(_this.filter.depositsMade.getId(), 'id'),
					userId: _this._u.checkNull(_this.filter.userId),
					email: _this._u.checkNull(_this.filter.email),
					firstName: _this._u.checkNull(_this.filter.firstName),
					lastName: _this._u.checkNull(_this.filter.lastName),
					mobilePhone: _this._u.checkNull(_this.filter.lastName),
					page: page,
					rowsPerPage: _this.filter.resultsPerPage.getId()
				}

				CryptoLeadsService.get('getCryptoLeads', request)
					.then(function(data) {
						_this._u.resetGlobalErrorMsg();
						_this.results = data.cryptoLeads;

						_this.pagination.setPages(page, data.totalCount, _this.filter.resultsPerPage.getId());
					})
					.catch(function(data) {
						_this._u.handleErrors(data, 'getCryptoLeads');
					})
					.finally(function(data) {
						_this._u.hideLoading();
					})
			} else {
				showToast(2, _this._u.getMsg('error.general_invalid_form'));
			}
		}

		function changePage(page) {
			getCryptoLeads(null, page);
		}

		function openCalendar(e, date) {
			_this.openDates[date] = true;
			if (e.target.tagName.toLowerCase() == 'input') {
				setTimeout(function() {
					e.target.focus();
				}, 100);
			}
		}

	}
})();
