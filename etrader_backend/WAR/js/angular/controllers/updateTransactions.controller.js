/* controller::UpdateTransactionController */
(function() {
	'use strict';

	angular.module('backendApp').controller('UpdateTransactionController', UpdateTransactionController);

	UpdateTransactionController.$inject = ['screenSettingsPrepService', 'utilsService', 'updateTransactopnsService'];
	function UpdateTransactionController(screenSettingsPrepService, utilsService, updateTransactopnsService) {
		var _this = this;
		_this._u = utilsService;
		
		_this.TRANSACTION_TYPE_CC_DEPOSIT = 1;
		_this.TRANSACTION_TYPE_INTERNAL_CREDIT = 16;
		
		_this.result = null;
		_this.hasResult = true;
		
		_this.hasStatuses = false;
		_this.hasTypes = false;
		
		_this.amountSettings = {centsPart: 0};
		
		var screenSettings = screenSettingsPrepService;

		//filters
		_this.filter = {
			transactionId: ''
		};
		_this.form = {};
		_this._form;
		
		
		_this.getList = getList;
		_this.update = update;
		_this.calculateJ4Postpone = calculateJ4Postpone;
	
		
		//functions
		function getList(_form) {
			if (!isUndefined(_form)) {
				_this._form = _form;
			} else {
				_form = _this._form;
			}
			if (_form.$valid) {
				_this._u.resetGlobalErrorMsg();
				_this._u.showLoading();
				
				var request = {
					transactionId: _this.filter.transactionId,
					trnIds: [_this.filter.transactionId],
					from: new Date().getTime(),
					to: new Date().getTime(),
					page: 1,
					rowsPerPage: 15,
					searchBy: 1
				}
				
				updateTransactopnsService.get(request)
					.then(function(data) {
						_this._u.resetGlobalErrorMsg();
						
						_this.result = data.transaction;
						if (_this.result) {
							_this.hasResult = true;
							
							//Add j4 postpone dates
							_this.calculateJ4Postpone(_this.result);
							
							_this.result.statusName = _this._u.getMsg(_this.result.statusName);
							_this.result.typeName = _this._u.getMsg(_this.result.typeName);
							
							_this.form = {
								radioType: '0'
							}

							if (!$.isEmptyObject(data.statuses)) {
								_this.hasStatuses = true;
								
								_this.form.statuses = new _this._u.SingleSelect.InstanceClass({
									options: data.statuses
								});
							} else {
								_this.hasStatuses = false;
							}
							
							if (!$.isEmptyObject(data.types)) {
								_this.hasTypes = true;
								
								_this.form.types = new _this._u.SingleSelect.InstanceClass({
									options: data.types
								})
							} else {
								_this.hasTypes = false;
							}
						} else {
							_this.hasResult = false;
						}
					})
					.catch(function(data) {
						_this._u.handleErrors(data, 'getStuff');
					})
					.finally(function(data) {
						_this._u.hideLoading();
					})
			} else {
				showToast(2, _this._u.getMsg('error.mandatory'));
			}
		}
		
		function update() {
			var from = '';
			var to = '';
			var message
			if (_this.form.radioType == 1) {
				from = _this.result.typeName;
				to = _this.form.types.getTextValue();
				message = _this._u.getMsg('update-transaction-confirm-type', {from: from, to: to});
			} else if (_this.form.radioType == 2) {
				from = _this.result.statusName;
				to = _this.form.statuses.getTextValue();
				message = _this._u.getMsg('update-transaction-confirm-status', {from: from, to: to});
			}
			
			var buttonTexts = {
				ok: _this._u.getMsg('yes'),
				cancel: _this._u.getMsg('no')
			}
			_this._u.confirm(message, updateAction, null, buttonTexts);
		}
		
		function updateAction() {
			if (_this.form.radioType != '0') {
				var request = {
					transactionId: _this.filter.transactionId,
					writerId: writerId 
				}
				
				if (_this.form.radioType == 1) {
					request.typeId = _this._u.checkNull(_this.form.types.getId(), 'id');
				} else if (_this.form.radioType == 2) {
					request.statusId = _this._u.checkNull(_this.form.statuses.getId(), 'id');
				}
				
				
				updateTransactopnsService.update(request)
					.then(function(data) {
						showToast(1, _this._u.getMsg('transaction-updated-successfully'));
						getList();
					})
			} else {
				showToast(2, _this._u.getMsg('error.general_invalid_form'));
			}
		}
		
		function calculateJ4Postpone(transaction) {
			var postponeIndex = 0;
			var postponeDates = [];
			var j = 0;
			var currentDate = new Date();
			var daysAddition = 0;
			if(currentDate.getHours() == 23){
				daysAddition = 1;
			}
			for(j; j < 3; j++){
				var d = new Date(transaction.timeCreated);
				d.setDate(d.getDate() + j + daysAddition + 1);
				d.setHours(23);
				d.setMinutes(0);
				postponeDates.push({name: _this._u.dateGetSimple(d), numberOfDays: j, value: d});
				if(transaction.transactionPostponed.numberOfDays == j){
					postponeIndex = j;
				}
			}
			transaction.postponeDates = postponeDates;
			transaction.postponeDate = transaction.postponeDates[postponeIndex];
			//it is ugly, i know, ask Tabakov :D
			//changes for direct24, EPS, Giropay
			if (transaction.paymentTypeId > 0) {
				transaction.typeIdView = '-' + transaction.typeId + transaction.paymentTypeId;  
			} else {
				transaction.typeIdView = transaction.typeId;
			}
			return transaction;
		}
	}
})();