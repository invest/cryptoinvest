/* controller::FromInboxController */
(function() {
	'use strict';

	angular.module('backendApp').controller('FromInboxController', FromInboxController);

	FromInboxController.$inject = ['screenSettingsPrepService', 'utilsService', 'fromInboxService'];
	function FromInboxController(screenSettingsPrepService, utilsService, fromInboxService) {
		var _this = this;
		_this._u = utilsService;
		
		var screenSettings = screenSettingsPrepService;
		
		_this.resultsList = null;
		
		//filters
		_this.filter = {
			fromDate: _this._u.setDateNoSec(),
			toDate: _this._u.setDateNoSec(),
			userId: '',
			username: '',
			classUsers: new _this._u.SingleSelect.InstanceClass({
				aggregateOption: {id: -1, name: 'all'},
				mutateCallback: _this._u.getMsg,
				options: screenSettings.screenFilters.classUsers
			}),
			skins: new _this._u.Multiselect.InstanceClass({
				aggregateOption: {id: -1, name: 'all'},
				mutateCallback: _this._u.getMsg,
				isSmart: true,
				noSort: true,
				options: screenSettings.screenFilters.skins
			}),
			countries: new _this._u.Multiselect.InstanceClass({
				aggregateOption: {id: -1, name: 'all'},
				mutateCallback: _this._u.getMsg,
				isSmart: true,
				iconCallback: function(option){return settings.jsonImagegLink + 'images/flags/' + option.id + '.png';},
				options: screenSettings.screenFilters.countries
			}),
			resultsPerPage: new _this._u.SingleSelect.InstanceClass({
				sortById: true,
				options: _this._u.resultsPerPage
			})
		};
		
		this.filter.resultsPerPage.setModel(this.filter.resultsPerPage.getOptionById(15));
		
		this.dateManager = new _this._u.Utils.DateManager();
		
		this.pagination = new _this._u.Pagination.InstanceClass({
			callback: changePage, 
			hideGroupNavigation: true
		});
		
		_this.getList = getList;
		_this.changePage = changePage;
		_this.fileSaved = fileSaved;
		_this.getSkinName = fromInboxService.getSkinName;
		
		//functions
		function getList(_form, page, allowCurrentPage) {
			var paging = false;
			if (!isUndefined(_form)) {
				_this._form = _form;
			} else {
				_form = _this._form;
				paging = true;
			}
			
			if((page = _this.pagination.validatePage(page, allowCurrentPage)) === false){
				return;
			}
			
			if (_form.$valid || paging) {
				_this._u.resetGlobalErrorMsg();
				_this._u.showLoading();
				
				var request = {
					lastMatchFrom: _this._u.setDateNoSec({date: _this.filter.fromDate, checkNull: true, startOfDay: true, timestamp: true}),
					lastMatchTo: _this._u.setDateNoSec({date: _this.filter.toDate, checkNull: true, endOfDay: true, timestamp: true}),
					userId: _this._u.checkNull(_this.filter.userId),
					userName: _this._u.checkNull(_this.filter.username),
					userClasses: _this._u.checkNull(_this.filter.classUsers.getId(), 'id'),
					skins: _this._u.checkNull(_this.filter.skins.getCheckedIds(), 'ids'),
					countries: _this._u.checkNull(_this.filter.countries.getCheckedIds(), 'ids'),
					page: page,
					rowsPerPage: _this.filter.resultsPerPage.getId()
				}
				
				fromInboxService.getList(request)
					.then(function(data) {
						_this._u.resetGlobalErrorMsg();
						if(!_this._u.handleErrors(data, 'getList')){
							_this.resultsList = data.filesList;
							_this.totalCount = data.totalCount;
							_this.pagination.setPages(page, _this.totalCount, _this.filter.resultsPerPage.getId());
						}
					})
					.catch(function(data) {
						_this._u.handleErrors(data, 'getList');
					})
					.finally(function(data) {
						_this._u.hideLoading();
					})
			} else {
				showToast(2, _this._u.getMsg('error.general_invalid_form'));
			}
		}
		
		function changePage(page) {
			getList(null, page);
		}
		
		function fileSaved(result){
			result.visible = false;
			_this.getList(null, _this.pagination.currentPage, true);
		}
		
	}
})();