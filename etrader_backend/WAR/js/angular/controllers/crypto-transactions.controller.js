/* controller::userCryptoTransactionsCtr */
(function() {
	'use strict';

	angular.module('backendApp').controller('CryptoTransactionsController', CryptoTransactionsController);

	CryptoTransactionsController.$inject = ['screenSettingsPrepService', 'utilsService', 'cryptoTransactionsService', '$rootScope', '$upload', '$timeout', '$scope'];
	function CryptoTransactionsController(screenSettingsPrepService, utilsService, cryptoTransactionsService, $rootScope, $upload, $timeout, $scope) {
		var _this = this;
		_this._u = utilsService;
		_this.transactionsList = [];
		_this.userCurrencyId;
		_this.centPart = 100;
		_this.disabled = true;
		_this.loadedTransaction;
		_this.loadedAssignee;
		_this.resolution;
		_this.currencies = screenSettingsPrepService.currencies;
		
		_this.transactionTypes = cryptoTransactionsService.transactionTypes;
		_this.transactionStatus = cryptoTransactionsService.transactionStatus;
		_this.cryptoStatuses = cryptoTransactionsService.cryptoStatuses;
		_this.resolutionStatus = cryptoTransactionsService.resolutionStatus;
		_this.rejectReasons = [];
		angular.copy(cryptoTransactionsService.rejectReasons, _this.rejectReasons);
		$scope.withdrawalToCustomerWalletDone = false;
		
		_this.form = {
			method: '',
			amount: '',
			currencyId: '',
			date: '',
			comments: ''
		};
		
		_this.formAssignee = {
			status: '',
			comment: '',
			reason: ''
		}
		
		//options
		_this.minDate = _this._u.setDateNoSec({offset: _this._u.writer.utcOffsetHours, setYears: -20});
		
		_this.openDates = {
			reviewDate: false
		}
		
		_this.dateOptions = {
			showWeeks: false,
			startingDay: 0,
			defaultTime: '00:00:00'
		}
		
		_this.getTransactions = getTransactions;
		_this.openCalendar = openCalendar;
		_this.loadTransaction = loadTransaction;
		_this.updateInsertFile = updateInsertFile;
		_this.submit = submit;
		_this.updateTransaction = updateTransaction;
		_this.calcAmounts = calcAmounts;
		_this.openApprovel = openApprovel;
		_this.submitAssignee = submitAssignee;
		_this.showFiles = showFiles;
		_this.getFilePath = getFilePath;
		_this.withdrawToUserWallet = withdrawToUserWallet;
		_this.closeStatus = closeStatus;
		
		_this.getTransactions();
		
		
		//functions
		function getTransactions() {
			cryptoTransactionsService.get('searchTransactions', {})
				.then(function(data) {
					_this.transactionsList = data.cryptoTransactions;
					_this.userCurrencyId = data.userCurrencyId;
					var index = searchJsonKeyInArray(screenSettingsPrepService.currencies, 'id', data.userCurrencyId);
					_this.userCurrencyCode =  screenSettingsPrepService.currencies[index].code;
					
					_this.calcAmounts();
				})
		}
		
		function calcAmounts(current) {
			_this.transactionsList.map(function(current) {
				current.transaction.timeCreated = new Date(current.transaction.timeCreated);
				
				current.cryptoTrade.rateMarketClient = Math.round(current.cryptoTrade.rateMarketClient) / _this.centPart;
				current.cryptoTrade.rateMarket = Math.round(current.cryptoTrade.rateMarket) / _this.centPart;
				
				current.transaction.total = current.transaction.amount / _this.centPart;
				current.transaction.totalUSD = current.transaction.total * current.cryptoTrade.rate;
				
				current.cryptoTrade.fees = (current.cryptoTrade.fee + current.cryptoTrade.clearanceFee) /  _this.centPart;
				current.cryptoTrade.feesUSD = current.cryptoTrade.fees * current.cryptoTrade.rate;
				
				current.transaction.subtotal = current.transaction.total - current.cryptoTrade.fees;
				current.transaction.subtotalUSD  = current.transaction.subtotal * current.cryptoTrade.rate;
				return current;
			});
		}
		
		function loadTransaction(transaction) {
			_this.loadedTransaction = transaction;
			_this.disabled = transaction.transaction.assignee && transaction.transaction.assignee != 'Finance';  
			
			var index = searchJsonKeyInArray(_this.currencies, 'id', _this.userCurrencyId);
			
			var date = new Date(); 
			if (transaction.transaction.timeSettled) {
				date = new Date(transaction.transaction.timeSettled);
			}
			
			_this.form = {
				method: transaction.transaction.typeId,
				amount: transaction.transaction.total,
				currencyId: _this.currencies[index],
				date: date.getTime(),
				comments: '',
				commentsOld: transaction.transaction.comments
			}
			
			cryptoTransactionsService.get('getTransactionResolution', {id: transaction.transaction.id})
				.then(function(data) {
					_this.resolution = data.resolution;
					
//					_this.resolution.map(function(current) {
//						var index = searchJsonKeyInArray(_this.resolutionStatus, 'id', current.status);
//						current.statusObj = _this.resolutionStatus[index];
						
//						var index = searchJsonKeyInArray(_this.rejectReasons, 'id', current.rejectReason);
//						current.rejectReasonObj = _this.rejectReasons[index]
						
//						return current;
//					})
				});
		}
		
		function updateInsertFile(file) {
			var request = {
				file: {
					fileName: file.message,
					fileTypeId: 35,
					userId: userId,
					transactionId: _this.loadedTransaction.transaction.id
				}
			};
			
			cryptoTransactionsService.updateInsertFile(request)
				.then(function(data) {
					_this.updateTransaction();
				});
		}
		
		function updateTransaction() {
			var request = {
				 id: _this.loadedTransaction.transaction.id,
			     amount: _this.form.amount * 100,
			     currencyId: _this.form.currencyId.id,
			     reviewDate: _this.form.date,
			     comments: _this.form.comments
			};
			
			cryptoTransactionsService.set('updateTransaction', request)
				.then(function(data) {
					showToast(1, 'Transaction saved successfully');
					_this.loadedTransaction.transaction.amount = data.amount;
					_this.loadedTransaction.transaction.fee = data.fee;
					_this.loadedTransaction.transaction.clearanceFee= data.clearanceFee;
					_this.loadedTransaction.transaction.assignee = data.assignee;
					_this.loadedTransaction.transaction.comments = _this.loadedTransaction.transaction.comments + data.comments;

					_this.calcAmounts();
					
					_this.loadTransaction(_this.loadedTransaction);
					_this.loading = false;
				});
		}
		
		function submit(_form) {
			if (!_this.loadedTransaction.transaction.assignee && _this.selectedFiles.length == 0) {
//				showToast(2, _this._u.getMsg('error.general_invalid_form'));
				showToast(2, 'No file selected');
			} else {
				if (_this.selectedFiles.length > 0){
					_this.loading = true;
					_this.send();
				} else {
					_this.updateTransaction();
				}
			}
		}
		
		function openApprovel(assignee) {
			_this.loadedAssignee = assignee;
			
			angular.copy(cryptoTransactionsService.rejectReasons, _this.rejectReasons);
			_this.rejectReasons = $rootScope.translateJsonKeyInArray( _this.rejectReasons, "value");

			_this.formAssignee = {
				status: _this.resolutionStatus[0],
				comment: '',
				reason: _this.rejectReasons[0]
			}
			
			$('#status-change').fadeIn();
		}
		
		function closeStatus() {
			$('#status-change').fadeOut();
		}
		
		function submitAssignee() {
			var request = {
				 id: _this.loadedTransaction.transaction.id,
				 status: _this.formAssignee.status.id,
				 comment: _this.formAssignee.comment,
				 rejectReasonId: _this.formAssignee.reason.id,
				 rejectReason: _this.formAssignee.reason.value
			};
			
			cryptoTransactionsService.set('changeResolutionBy' + _this.loadedAssignee, request)
				.then(function(data) {
					showToast(1, 'Status changed successfully');
					$('#status-change').fadeOut();
					
					if (_this.formAssignee.status.id == 2) {
						switch(_this.loadedAssignee) {
							case 'Finance': _this.loadedTransaction.transaction.assignee =  'Support'; break;
							case 'Support': _this.loadedTransaction.transaction.assignee =  'Risk'; break;
							case 'Risk': _this.loadedTransaction.transaction.statusId = 2; break;
						}
					}
					_this.resolution[_this.loadedAssignee].comment = _this.formAssignee.comment;
					_this.resolution[_this.loadedAssignee].status = _this.formAssignee.status.id;
				});
		}
	
		function openCalendar(e, date) {
			_this.openDates[date] = true;
			if (e.target.tagName.toLowerCase() == 'input') {
				setTimeout(function() {
					e.target.focus();
				}, 100);
			}
		}
		
		function showFiles() {
			var request = {
				id: _this.loadedTransaction.transaction.id
			};

			cryptoTransactionsService.set('viewUserFiles', request)
				.then(function(data) {
					_this.loadedTransaction.files = data.files;
				});
		}
		
		function getFilePath(fileName){
			return settings.jsonImagegLink + 'docs/' + fileName;
		}
		
		function withdrawToUserWallet() {
			var request = {
				 id: _this.loadedTransaction.transaction.id
			};
			

			cryptoTransactionsService.set('withdrawToUserWallet', request)
				.then(function(data) {
					showToast(1, 'Withdraw to user wallet successful');
					$scope.withdrawalToCustomerWalletDone = true;
				});
		}
		
		//Handle file upload
		_this.selectedFiles = [];
		_this.selectedSlideKey = null;
		_this.progress = [];
		_this.abort = function(index) {
			_this.upload[index].abort(); 
			_this.upload[index] = null;
		};
		
		_this.onFileSelect = function($files) {
			$rootScope.resetGlobalErrorMsg();
			//Check if the file is too big, alert and return in case it is
			var hasLargerFile = false;
			_this.fileTooBig = false;
			for ( var i = 0; i < $files.length; i++) {
				if($files[i].size / 1000000 >= settings.documentSizeLimit){
					hasLargerFile = true;
				}
			}
			if(hasLargerFile){
				//alert($rootScope.getMsgs('file-too-big'));
				_this.fileTooBig = true;
				$timeout(function(){_this.fileTooBig = false;}, 3000);
				return;
			}
			
			if (_this.upload && _this.upload.length > 0) {
				for (var i = 0; i < _this.upload.length; i++) {
					if (_this.upload[i] != null) {
						_this.upload[i].abort();
					}
				}
			}
			_this.upload = [];
			_this.uploadResult = [];
			_this.selectedFiles = $files;
			_this.dataUrls = [];
			for ( var i = 0; i < $files.length; i++) {
				var $file = $files[i];
				_this.uploadedFileName = $file.name;
//				_this.send();
			}
		};
		
		_this.start = function(index) {
			if (_this.selectedFiles[index].size / 1000000  < settings.documentSizeLimit) {
				_this.progress[index] = 0;
				_this.errorMsg = null;
				//$upload.upload()
				var params = '?';
				params += 'fileName=' + _this.selectedFiles[index].name;
				params += '&fileType=35';//public static final long BANK_WIRE_PROTOCOL_FILE = 35;
				params += '&transactionId=' + _this.loadedTransaction.transaction.id;
				params = encodeURI(params);
				_this.upload[index] = $upload.upload({
					url: settings.backendFileUploadLink + params,
					method: 'POST',
					file: _this.selectedFiles[index],
					fileFormDataName: 'myFile',
					data: {
						fileName: _this.selectedFiles[index].name
					}
				});
				_this.upload[index].then(function(response) {
					$timeout(function() {
						_this.uploadResult.push(response);
						_this.uploadComplete(response);
					});
				}, function(response) {
					if (response.status > 0){
						hideLoading();
						_this.selectedSlide.sliderImages[_this.selectedSlideKey].uploadedImage = null;
						$rootScope.addGlobalErrorMsg($rootScope.getMsgs('error.unknown'));
					}
				}, function(evt) {
					// Math.min is to fix IE which reports 200% sometimes
//					_this.$parent.progress = _this.progress[index] = Math.min(100, parseInt(100.0 * evt.loaded / evt.total));
				});
				_this.upload[index].xhr(function(xhr){
		//				xhr.upload.addEventListener('abort', function() {console.log('abort complete')}, false);
				});
			} else {
				hideLoading();
				_this.selectedSlide.sliderImages[_this.selectedSlideKey].uploadedImage = null;
				$rootScope.addGlobalErrorMsg($rootScope.getMsgs('file-too-big'));
			}
		};
		
		_this.uploadComplete = function(data) {
			hideLoading();
			if (!$rootScope.handleErrors(data, 'UploadService')) {
				if(data.userMessages){
					_this.updateInsertFile(data.userMessages[0]);
				}
			} else {
				_this.selectedSlide.sliderImages[_this.selectedSlideKey].uploadedImage = null;
			}
		}
		
		if (localStorage) {
			_this.s3url = localStorage.getItem("s3url");
			_this.AWSAccessKeyId = localStorage.getItem("AWSAccessKeyId");
			_this.acl = localStorage.getItem("acl");
			_this.success_action_redirect = localStorage.getItem("success_action_redirect");
			_this.policy = localStorage.getItem("policy");
			_this.signature = localStorage.getItem("signature");
		}
		_this.success_action_redirect = _this.success_action_redirect || window.location.protocol + "//" + window.location.host;
		_this.jsonPolicy = _this.jsonPolicy || '{\n  "expiration": "2020-01-01T00:00:00Z",\n  "conditions": [\n    {"bucket": "angular-file-upload"},\n    ["starts-with", "$key", ""],\n    {"acl": "private"},\n    ["starts-with", "$Content-Type", ""],\n    ["starts-with", "$filename", ""],\n    ["content-length-range", 0, 524288000]\n  ]\n}';
		_this.acl = _this.acl || 'private';
		
		_this.send = function(){
			showLoading();
			for ( var i = 0; i < _this.selectedFiles.length; i++) {
				_this.progress[i] = -1;
				_this.start(i);
			}
		}

	}
})();
