/* controller::AcademyArticlesController */
(function() {
	'use strict';

	angular.module('backendApp').controller('AcademyArticlesController', AcademyArticlesController);

	AcademyArticlesController.$inject = ['screenSettingsPrepService', 'utilsService', 'academyArticlesService', 'SingleSelect', '$sce', '$timeout'];
	function AcademyArticlesController(screenSettingsPrepService, utilsService, academyArticlesService, SingleSelect, $sce, $timeout) {
		var _this = this;
		_this._u = utilsService;
		
		var screenSettings = screenSettingsPrepService;
		
		_this.defaultLocale = 'en';
		
		_this.form = {}
		_this.ready = true;
		_this.totalCount = 0;
		_this.resultsList = null;
		_this.languages = _this._u.validateLanguages(screenSettingsPrepService.languages);
		_this.articleLoaded = false;
		_this.article = null;
		var minSuggestedTopicsAttemptTimeout = 300;
		
		_this.filter = {
			resultsPerPage: new _this._u.SingleSelect.InstanceClass({
				sortById: true,
				options: _this._u.resultsPerPage
			})
		};
		
		this.filter.resultsPerPage.setModel(this.filter.resultsPerPage.getOptionById(15));
		
		this.pagination = new _this._u.Pagination.InstanceClass({
			callback: changePage, 
			hideGroupNavigation: true
		});
		
		_this.tinymceParams = [
			
		];
		_this.tinymceOptions = null;
		_this.tinymceReady = false;
		
		_this.articleId = _this._u.getStateParams().articleId;
		_this.action = '';
		if(_this.articleId){
			_this.action = 'edit';
			getArticle(_this.articleId);
		}else if(_this._u.$state.includes('ln.content.academy-articles.add')){
			_this.action = 'add';
			_this.article = normalizeArticle({
				id: 0,
				homePagePosition: 0,
				isActive: false,
				isDeleted: false
			});
			_this.articleLoaded = true;
			initTinymce();
		}
		if(_this.action == 'edit' || _this.action == 'add'){
			var homePagePositionOptions = [{id: 0, name: ''}];
			for(var i = 0; i < 13; i++){
				homePagePositionOptions.push({id: (i + 1), name: (i + 1) + ''});
			}
			_this.homePagePosition = new _this._u.SingleSelect.InstanceClass({
				sortById: true,
				options: homePagePositionOptions,
				externalModel: {objCallback: function(){return _this.article}, key: 'homePagePosition'}
			});
		}
		
		
		_this.getList = getList;
		_this.changePage = changePage;
		_this.getArticle = getArticle;
		_this.saveArticle = saveArticle;
		_this.deleteArticle = deleteArticle;
		_this.getImageUrl = getImageUrl;
		_this.removeItemFromArr = removeItemFromArr;
		_this.addTopic = addTopic;
		_this.getSuggestedTopics = getSuggestedTopics;
		_this.useSuggestedTopic = useSuggestedTopic;
		_this.hideSuggestedTopics = hideSuggestedTopics;
		_this.initTinymce = initTinymce;
		
		//functions
		
		function getList(_form, page, allowCurrentPage) {
			var paging = false;
			if (!isUndefined(_form)) {
				_this._form = _form;
			} else {
				_form = _this._form;
				paging = true;
			}
			
			if((page = _this.pagination.validatePage(page, allowCurrentPage)) === false){
				return;
			}
			
			if (_form.$valid || paging) {
				_this._u.resetGlobalErrorMsg();
				_this._u.showLoading();
				
				var request = {
					page: page,
					rowsPerPage: _this.filter.resultsPerPage.getId()
				};
				
				academyArticlesService.getList(request)
					.then(function(data) {
						_this._u.resetGlobalErrorMsg();
						_this.resultsList = data.articles;
						_this.totalCount = data.totalCount;
						_this.pagination.setPages(page, _this.totalCount, _this.filter.resultsPerPage.getId());
					})
					.catch(function(data) {
						_this._u.handleErrors(data, 'getList');
					})
					.finally(function(data) {
						_this._u.hideLoading();
					})
			} else {
				showToast(2, _this._u.getMsg('error.general_invalid_form'));
			}
		}
		
		function changePage(page) {
			getList(null, page);
		}
		
		function getArticle(){
			_this._u.showLoading();
			var request = {
				article: {
					id: _this.articleId
				}
			};
			
			academyArticlesService.getArticle(request)
				.then(function(data) {
					_this.articleLoaded = true;
					_this._u.resetGlobalErrorMsg();
					_this.article = normalizeArticle(data.article);
					initTinymce();
				})
				.catch(function(data) {
					_this._u.handleErrors(data, 'getArticle');
				})
				.finally(function(data) {
					_this._u.hideLoading();
				})
		}
		
		function saveArticle(){
			_this._u.showLoading();
			_this.article = prepareArticleForSave(_this.article);
			var request = {
				article: _this.article,
				writerId: writerId
			};
			
			if(_this.action == 'edit'){
				editArticle(request);
			}else if(_this.action == 'add'){
				addArticle(request);
			}
		}
		
		function editArticle(request){
			academyArticlesService.editArticle(request)
				.then(function(data) {
					_this._u.resetGlobalErrorMsg();
					_this._u.$state.go('ln.content.academy-articles', {ln: _this._u.$state.params.ln})
				})
				.catch(function(data) {
					_this._u.handleErrors(data, 'getArticle');
				})
				.finally(function(data) {
					_this._u.hideLoading();
				})
		}
		
		function addArticle(request){
			academyArticlesService.addArticle(request)
				.then(function(data) {
					_this._u.resetGlobalErrorMsg();
					_this._u.$state.go('ln.content.academy-articles', {ln: _this._u.$state.params.ln})
				})
				.catch(function(data) {
					_this._u.handleErrors(data, 'getArticle');
				})
				.finally(function(data) {
					_this._u.hideLoading();
				})
		}
		
		function deleteArticle(article){
			_this._u.confirm(_this._u.getMsg('confirm.article-delete'), function(){
				_this._u.showLoading();
				var request = {
					article: {
						id: article.id
					}
				};
				academyArticlesService.deleteArticle(request)
					.then(function(data) {
						_this._u.resetGlobalErrorMsg();
						if(_this.action == 'edit'){
							_this._u.$state.go('ln.content.academy-articles', {ln: _this._u.$state.params.ln});
						}else if(_this.action == 'add'){
							_this._u.$state.go('ln.content.academy-articles', {ln: _this._u.$state.params.ln});
						}else{
							getList(null, _this.pagination.currentPage, true);
						}
					})
					.catch(function(data) {
						_this._u.handleErrors(data, 'deleteArticle');
					})
					.finally(function(data) {
						_this._u.hideLoading();
					})
			});
		}
		
		function normalizeArticle(article){
			return normalizeArticleHomePagePosition(normalizeArticleTopics(normalizeArticleTranslations(article)));
		}
		
		function normalizeArticleHomePagePosition(article){
			return article;
		}
		
		function normalizeArticleTopics(article){
			if(!article || !article.translations){
				return;
			}
			for(var i = 0; i < article.translations.length; i++){
				if(!article.translations[i].topics){
					article.translations[i].topics = [];
				}
				var arr = [];
				for(var j = 0; j < article.translations[i].topics.length; j++){
					arr.push({title: article.translations[i].topics[j]});
				}
				article.translations[i].topics = arr;
				
				article.translations[i].newTopics = {
					newTopic: '',
					suggestedTopics: [],
					lastSuggestedTopicsAttempt: null
				};
			}
			
			return article;
		}
		
		function normalizeArticleTranslations(article){
			if(!article){
				return;
			}
			if(!article.translations){
				article.translations = [];
			}
			for(var key in _this.languages){
				if(_this.languages.hasOwnProperty(key)){
					var hasTranslation = false;
					for(var i = 0; i < article.translations.length; i++){
						if(_this.languages[key].code == article.translations[i].locale){
							hasTranslation = true;
						}
					}
					if(!hasTranslation){
						article.translations.push({
							locale: _this.languages[key].code,
							topics: [],
							author: '',
							title: '',
							subtitle: '',
							html: ''
						});
					}
				}
			}
			article.translations.sort(function(a, b){
				if(a['locale'] < b['locale']){
					return -1;
				}else if(a['locale'] > b['locale']){
					return 1;
				}
				return 0;
			});
			return article;
		}
		
		function prepareArticleForSave(article){
			if(!article){
				return;
			}
			if(!article.translations){
				article.translations = [];
			}
			for(var i = 0; i < article.translations.length; i++){
				if(!article.translations[i].topics){
					article.translations[i].topics = [];
				}
				var arr = [];
				for(var j = 0; j < article.translations[i].topics.length; j++){
					arr.push(article.translations[i].topics[j].title);
				}
				article.translations[i].topics = arr;
			}
			
			if(_this.homePagePosition.getId() > 0){
				article.homePagePosition = _this.homePagePosition.getId();
			}
			
			return article;
		}
		
		function getImageUrl(url){
			return $sce.trustAsResourceUrl(url);
		}
		
		function removeItemFromArr(arr, index){
			arr.splice(index, 1);
		}
		
		function addTopic(topics, newTopic){
			if(!newTopic){
				return;
			}
			if(!topics){
				topics = [];
			}
			var hasTopic = false
			for(var i = 0; i < topics.length; i++){
				if(topics[i].title == newTopic){
					hasTopic = true;
				}
			}
			if(!hasTopic){
				topics.push({title: newTopic});
				translation.newTopics.newTopic = '';
			}
		}
		
		function getSuggestedTopics(translation){
			translation.newTopics.lastSuggestedTopicsAttempt = new Date().getTime();
			setAttemptTimeout(minSuggestedTopicsAttemptTimeout);
			function setAttemptTimeout(delay){
				$timeout(function(){
					if(translation.newTopics.lastSuggestedTopicsAttempt && new Date().getTime() < translation.newTopics.lastSuggestedTopicsAttempt + minSuggestedTopicsAttemptTimeout){
						return;
					}else{
						getSuggestedTopicsInner();
					}
				}, delay);
			}
			
			function getSuggestedTopicsInner(){
				translation.newTopics.suggestedTopics = [];
				if(translation.newTopics.newTopic == ''){
					return;
				}
				
				var request = {
					topicString: translation.newTopics.newTopic,
					articleLocale: translation.locale
				};
				academyArticlesService.getSuggestedTopics(request)
					.then(function(data) {
						_this._u.resetGlobalErrorMsg();
						translation.newTopics.suggestedTopics = data.topics;
					})
					.catch(function(data) {
						_this._u.handleErrors(data, 'getSuggestedTopics');
					})
					.finally(function(data) {
						_this._u.hideLoading();
					})
			}
		}
		
		function useSuggestedTopic(translation, suggestedTopic){
			translation.newTopics.newTopic = suggestedTopic.title;
			translation.newTopics.suggestedTopics = [];
		}
		
		function hideSuggestedTopics(newTopics){
			newTopics.suggestedTopics = [];
		}
		
		function initTinymce(){
			var cssLink = 'https://www.bgtestenv.anyoption.com/css/styles_ao.css?' +  + new Date().getTime();
			_this.tinymceOptions = {
				height: 350,
				body_class: '.anyoption-ng .academy .academy-section',
				selector: 'textarea#annex',
				menubar: 'view edit insert table format tools',
				plugins: 'advlist autolink link image lists table paste searchreplace charmap fullscreen noneditable code',
				content_css: cssLink + '?version=' + new Date().getTime(),
				content_style: 'body{background:none!important}',
				paste_convert_word_fake_lists: true,
				paste_remove_styles_if_webkit: true,
				paste_remove_styles: true,
				paste_strip_class_attributes: true,
				table_appearance_options: false,
				/*invalid_elements : 'table,thead,tbody,tr,td',*/
				style_formats: [
					{ title: 'Title block', block : 'div', classes : 'header-box', exact : true, wrapper: true },
					{ title: 'H1 - Title', block : 'h1', classes : '', exact : true },
					{ title: 'H2 - Title', block : 'h2', classes : '', exact : true },
					{ title: 'Yellow title text', inline : 'span', classes : '', exact : true },
					{ title: 'sub-item-title', inline : 'span', classes : 'sub-item-title', exact : true },
					{ title: 'No indentation', selector: 'ul', classes : 'no-indent' },
					{ title: 'Half column', block: 'div', wrapper: true, classes: 'half-col', exact: true }
				],
				formats: {
				},
				setup: function (editor) {
					for(var i = 0; i < _this.tinymceParams.length; i++){
						var insertParam = function(param){
							var content = '<span class="mceNonEditable">${' + param.name + '}</span>';
							if(param.display == 'block'){
								content = '<div class="mceNonEditable">${' + param.name + '}</div><br/>';
							}
							editor.addMenuItem('insert' + param.name, {
								text: param.title,
								context: 'insert',
								onclick: function () {
									editor.insertContent(content);
								}
							});
						}(_this.tinymceParams[i]);
					}
					editor.addMenuItem('custominsertseparator', {
						text: '-',
						context: 'insert'
					});
				}
			};
			_this.tinymceReady = true;
		}
	}
})();