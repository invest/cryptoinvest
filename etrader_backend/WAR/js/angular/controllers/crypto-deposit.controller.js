/* controller::CryptoDepositController */
(function() {
	'use strict';

	angular.module('backendApp').controller('CryptoDepositController', CryptoDepositController);

	CryptoDepositController.$inject = ['screenSettingsPrepService', 'utilsService', 'CryptoDepositService', '$rootScope'];
	function CryptoDepositController(screenSettingsPrepService, utilsService, CryptoDepositService, $rootScope) {
		var _this = this;
		_this._u = utilsService;
		
		this.amountSettings = {centsPart: 2};
		this.action = '';
		
		var screenSettings = screenSettingsPrepService;
		_this.assignees = screenSettings.screenFilters.assignees;
		_this.transactionStatuses = screenSettings.screenFilters.transactionStatuses;
		_this.transactionTypes = screenSettings.screenFilters.transactionTypes;

		_this.results = null;
		
		//filters
		this.filter = {
			fromDate: _this._u.setDateNoSec({month: -1}),
			toDate: _this._u.setDateNoSec(),
			userId: '',
			transactionId: '',
			email: '',
			firstName: '',
			lastName: '',
			searchBy: new _this._u.SingleSelect.InstanceClass({
				options: {
					1: 'Time created',
					2: 'Time execution'
				}
			}),
			countries: new _this._u.Multiselect.InstanceClass({
				aggregateOption: {id: -1, name: 'all'}, 
				mutateCallback: _this._u.getMsg, 
				isSmart: true, 
				options: screenSettings.screenFilters.countries,
				iconCallback: function(option){return settings.jsonImagegLink + 'images/flags/' + option.id + '.png';}
			}),
			transactionTypes: new _this._u.Multiselect.InstanceClass({
				aggregateOption: {id: -1, name: 'all'}, 
				mutateCallback: _this._u.getMsg, 
				isSmart: true, 
				options: screenSettings.screenFilters.transactionTypes,
			}),
			assignees: new _this._u.Multiselect.InstanceClass({
				aggregateOption: {id: -1, name: 'all'}, 
				isSmart: true, 
				options: screenSettings.screenFilters.assignees,
			}),
			statuses: new _this._u.Multiselect.InstanceClass({
				aggregateOption: {id: -1, name: 'all'}, 
				mutateCallback: _this._u.getMsg, 
				isSmart: true, 
				options: screenSettings.screenFilters.transactionStatuses
			}),
			resultsPerPage: new _this._u.SingleSelect.InstanceClass({
				sortById: true,
				options: _this._u.resultsPerPage
			})
		};
		
		//set default loaded
		this.filter.resultsPerPage.setModel(this.filter.resultsPerPage.getOptionById(15));
		this.filter.searchBy.setModel(this.filter.searchBy.getOptionById(1));
		
		//options
		_this.minDate = _this._u.setDateNoSec({offset: _this._u.writer.utcOffsetHours, setYears: -20});
//		_this.maxDate = _this._u.setDateNoSec({offset: _this._u.writer.utcOffsetHours});//BAC-4114

		_this.openDates = {
			fromDate: false,
			toDate: false
		}
		
		_this.dateOptions = {
			showWeeks: false,
			startingDay: 0,
			defaultTime: '00:00:00'
		}
		
		this.pagination = new _this._u.Pagination.InstanceClass({
			callback: changePage, 
			hideGroupNavigation: true
		});
		
		
		_this.getAllCrytpoDeposits = getAllCrytpoDeposits;
		_this.changePage = changePage;
		_this.openCalendar = openCalendar;
		
		//functions
		function getAllCrytpoDeposits(_form, page) {
			var paging = false;
			if (!isUndefined(_form)) {
				_this._form = _form;
			} else {
				_form = _this._form;
				paging = true;
			}
			
			if((page = _this.pagination.validatePage(page)) === false){
				return;
			}
			
			if (_form.$valid || paging) {
				_this.currentPage = page;
				_this._u.resetGlobalErrorMsg();
				_this._u.showLoading();
				
				var request = {
					from: _this.filter.fromDate.getTime(),
					to: _this.filter.toDate.getTime(),
					searchBy: _this._u.checkNull(_this.filter.searchBy.getId(), 'id'),
					countries:  _this._u.checkNull(_this.filter.countries.getCheckedIds(), 'ids'),
					transactionTypes: _this._u.checkNull(_this.filter.transactionTypes.getCheckedIds(), 'ids'),
					assignees: _this._u.checkNull(_this.filter.assignees.getCheckedIds(), 'ids'),
					statuses: _this._u.checkNull(_this.filter.statuses.getCheckedIds(), 'ids'),
					userId: _this._u.checkNull(_this.filter.userId),
					transactionId: _this._u.checkNull(_this.filter.transactionId),
					email: _this._u.checkNull(_this.filter.email),
					firstName: _this._u.checkNull(_this.filter.firstName),
					lastName: _this._u.checkNull(_this.filter.lastName),
					page: page,
					rowsPerPage: _this.filter.resultsPerPage.getId()
				}
				
				CryptoDepositService.get('getAllCrytpoDeposits', request)
					.then(function(data) {
						_this._u.resetGlobalErrorMsg();
						_this.results = data.cryptoDeposits;
						
						_this.pagination.setPages(page, data.totalCount, _this.filter.resultsPerPage.getId());
					})
					.catch(function(data) {
						_this._u.handleErrors(data, 'getAllCrytpoDeposits');
					})
					.finally(function(data) {
						_this._u.hideLoading();
					})
			} else {
				showToast(2, _this._u.getMsg('error.general_invalid_form'));
			}
		}
		
		function changePage(page) {
			getAllCrytpoDeposits(null, page);
		}
		
		function openCalendar(e, date) {
			_this.openDates[date] = true;
			if (e.target.tagName.toLowerCase() == 'input') {
				setTimeout(function() {
					e.target.focus();
				}, 100);
			}
		}

	}
})();