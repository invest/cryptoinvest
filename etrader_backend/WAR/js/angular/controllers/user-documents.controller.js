/* controller::UserDocumentsController */
(function() {
	'use strict';

	angular.module('backendApp').controller('UserDocumentsController', UserDocumentsController);

	UserDocumentsController.$inject = ['screenSettingsPrepService', 'utilsService', 'UserDocumentsService', '$rootScope', '$uibModal', '$scope'];
	function UserDocumentsController(screenSettingsPrepService, utilsService, UserDocumentsService, $rootScope, $uibModal, $scope) {
		var _this = this;
		_this._u = utilsService;
		
		this.amountSettings = {centsPart: 2};
		this.action = '';
		this.editAddMode = false;
		this.formDetails = {}
		
		var screenSettings = screenSettingsPrepService;
		_this.languages = screenSettings.screenFilters.languages;

		_this.results = null;
		
		//filters
		this.filter = {
			userId: '',
			email: '',
			firstName: '',
			lastName: '',
			isTester: new _this._u.SingleSelect.InstanceClass({
				aggregateOption: {id: -1, name: 'all'}, 
				mutateCallback: _this._u.getMsg, 
				options: screenSettings.screenFilters.classUsers
			}),
			countries: new _this._u.Multiselect.InstanceClass({
				aggregateOption: {id: -1, name: 'all'}, 
				mutateCallback: _this._u.getMsg, 
				isSmart: true, 
				options: screenSettings.screenFilters.countries,
				iconCallback: function(option){return settings.jsonImagegLink + 'images/flags/' + option.id + '.png';}
			}),
			languages: new _this._u.SingleSelect.InstanceClass({
				aggregateOption: {id: -1, name: 'all'}, 
				mutateCallback: _this._u.getMsg, 
				options: screenSettings.screenFilters.languages
			}),
			active: new _this._u.SingleSelect.InstanceClass({
				aggregateOption: {id: -1, name: 'all'}, 
				options: screenSettings.screenFilters.userActiveFilter
			}),
			suspended: new _this._u.SingleSelect.InstanceClass({
				aggregateOption: {id: -1, name: 'all'}, 
				options: screenSettings.screenFilters.userSuspendedFilter,
			}),
			isRegulationApproved: new _this._u.SingleSelect.InstanceClass({
				aggregateOption: {id: -1, name: 'all'}, 
				options: screenSettings.screenFilters.complianceApprovedFilter,
			}),
			docType1: new _this._u.SingleSelect.InstanceClass({
				aggregateOption: {id: -1, name: 'all'}, 
				options: screenSettings.screenFilters.docTypeFilter,
			}),
			docType2: new _this._u.SingleSelect.InstanceClass({
				aggregateOption: {id: -1, name: 'all'}, 
				options: screenSettings.screenFilters.docTypeFilter,
			}),
			resolutionStatus1: new _this._u.Multiselect.InstanceClass({
				aggregateOption: {id: -1, name: 'all'}, 
				mutateCallback: _this._u.getMsg, 
				mutateOnlyAggregate: true,
				options: screenSettings.screenFilters.resolutionFilter,
				isSmart: true
			}),
			resolutionStatus2: new _this._u.Multiselect.InstanceClass({
				aggregateOption: {id: -1, name: 'all'}, 
				mutateCallback: _this._u.getMsg, 
				mutateOnlyAggregate: true,
				options: screenSettings.screenFilters.resolutionFilter,
				isSmart: true
			}),
			resultsPerPage: new _this._u.SingleSelect.InstanceClass({
				sortById: true,
				options: _this._u.resultsPerPage
			})
		};
		
		//set default loaded
		this.filter.resultsPerPage.setModel(this.filter.resultsPerPage.getOptionById(15));
//		this.filter.isTester.setModel(this.filter.isTester.getOptionById(12));

		_this.openDates = {
			fromDate: false,
			toDate: false,
			chargebackDate: false,
			disputeDate: false
		}
		
		_this.dateOptions = {
			showWeeks: false,
			startingDay: 0,
			defaultTime: '00:00:00'
		}
		
		this.pagination = new _this._u.Pagination.InstanceClass({
			callback: changePage, 
			hideGroupNavigation: true
		});
		
		
		_this.getUsersDocuments = getUsersDocuments;
		_this.showHideSeconndResSearch = showHideSeconndResSearch;
		
		_this.changePage = changePage;
		_this.showRow = showRow;
		_this.openCustomerInformation = openCustomerInformation;
		_this.addFile = addFile;
		_this.openFilePopup = openFilePopup;
		_this.saveComments = saveComments;
		_this.openComment = openComment;
		_this.regulationApprove = regulationApprove;
		_this.regulationApproveManager = regulationApproveManager;
		
		_this.showMoreDoctype = false;
		function showHideSeconndResSearch() {
			_this.showMoreDoctype = !_this.showMoreDoctype;
		}
		
		function openCustomerInformation(userId){
			window.open($rootScope.getUserStripUrl(userId));
		}
		
		function addFile(result){
			//Clear edit data
			$scope.popupFileId = null;
			$scope.popupUserId = null;
			
			$scope.popupFileAdd = true;
			$scope.popupUserId = result.userId;
			var previewModal = $uibModal.open({
				templateUrl: folderPrefix + 'components/documents-back-office-file-popup.html',
				scope: $scope,
				windowClass: 'documents-back-office-file-popup'
			});
			previewModal.result.then(function () {
				//success
				_this.showRow(result, true);
			}, function () {
				//cancel
				_this.showRow(result, true);
			});
		}
		
		function openFilePopup(result, file){
			//Clear add data
			$scope.popupFileAdd = null;
			$scope.popupUserId = null;
			
			$scope.popupFileId = file.fileId;
			$scope.popupUserId = result.userId;
			var previewModal = $uibModal.open({
				templateUrl: folderPrefix + 'components/documents-back-office-file-popup.html',
				scope: $scope,
				windowClass: 'documents-back-office-file-popup'
			});
			previewModal.result.then(function () {
				//success
				_this.showRow(result, true);
			}, function () {
				//cancel
				_this.showRow(result, true);
			});
		}
		
		function saveComments(results) {
			if (isUndefined(results.issueActionComments) || results.issueActionComments == '') {
				showToast(TOAST_TYPES.error, $rootScope.getMsgs('error.mandatory'));
				return false;
			}
			
			var request = {
				userId: results.userId,
				comments: results.issueActionComments
			};
			
			showLoading();
			UserDocumentsService.set('saveComments', request)
				.then(function(data) {
					$rootScope.resetGlobalErrorMsg();
					showToast(TOAST_TYPES.info, $rootScope.getMsgs('comment-was-successfully-added'));
					results.issueActionComments = '';
				})
				.catch(function(data) {
					$rootScope.handleErrors(data, 'saveComments')
				})
				.finally(function() {
					hideLoading();
				})
		}
		
		_this.commentPopupType = '';
		_this.commentpopupEl = '';
		function openComment($event, type) {
			_this.commentPopupType = type;
			var el = $($event.currentTarget);
			var elPos = el.position();
			_this.commentPopupEl = el.parent().find('#commentsDiv');
			
			_this.commentPopupEl.css({
				left: elPos.left +  (el.width() / 2) - (_this.commentPopupEl.width() / 2) + 'px',
				top: elPos.top +  (el.height() / 2) + 30 + 'px',
			})
			
			_this.commentPopupEl.fadeToggle();
		}
		_this.closeComment = function() {
			_this.commentPopupEl.fadeToggle();
		}
		
		function regulationApprove(result){
			if(!result.comments){
				showToast(TOAST_TYPES.error, $rootScope.getMsgs('error.mandatory'));
				return false;
			}
			var methodRequest = {};
			methodRequest.userId = result.userId;
			methodRequest.comments = result.comments;
			
			UserDocumentsService.set('regulationCryptoApprove', methodRequest)
				.then(function(data) {
					$rootScope.resetGlobalErrorMsg();
					_this.showRow(result, true);
					_this.closeComment();
					showToast(TOAST_TYPES.info, $rootScope.getMsgs('info.documents-back-office-successfully-approved'));
				})
				.catch(function(data) {
					$rootScope.handleErrors(data, 'getDocuments')
				})
				.finally(function() {
					hideLoading();
				})
		}
		
		function regulationApproveManager(result){
			if(!result.comments){
				showToast(TOAST_TYPES.error, $rootScope.getMsgs('error.mandatory'));
				return false;
			}
			var methodRequest = {};
			methodRequest.userId = result.userId;
			methodRequest.comments = result.comments;
			UserDocumentsService.set('regulationCryptoApproveManager', methodRequest)
				.then(function(data) {
					$rootScope.resetGlobalErrorMsg();
					_this.showRow(result, true);
					_this.closeComment();
					showToast(TOAST_TYPES.info, $rootScope.getMsgs('info.documents-back-office-successfully-approved'));
				})
				.catch(function(data) {
					$rootScope.handleErrors(data, 'getDocuments')
				})
				.finally(function() {
					hideLoading();
				})
		}

		
		function showRow(result, keepOpen) {
			if (!keepOpen) {
				result.show = result.show ? false : true;
			}
			
			var methodRequest = {};
			methodRequest.userId = result.userId;
			
			UserDocumentsService.get('getUsersDocumentsData', methodRequest)
				.then(function(data) {
					result._additionalInfo = data;
					if (result._additionalInfo.lastCallTime == 0) {result._additionalInfo.lastCallTime == null;}
					if (result._additionalInfo.lastReachedTime == 0) {result._additionalInfo.lastReachedTime == null;}
					result.showAdditionalInfo = true;
					data.files.map(function(current, index){
						var typeIdArr = [1,2,22];
						if ($.inArray(current.fileType, typeIdArr) > -1 && current.expDate) {
							var expDate = new Date(current.expDate);
							if (!isNaN(expDate.getTime())) {
								var date = new Date();
								date.setHours(23);
								date.setMinutes(59);
								date.setDate(date.getDate() - 1);
								var dateFuture = new Date(date);
								dateFuture.setDate(dateFuture.getDate() + 14);
								
								if (date.getTime() > expDate.getTime()) {
									current.expClass = 'red';
								} else if (dateFuture.getTime() > expDate.getTime()) {
									current.expClass = 'orange';
								}
							}
						}
						return current;
					})
				});
		}
		
		//functions
		function getUsersDocuments(_form, page) {
			var paging = false;
			if (!isUndefined(_form)) {
				_this._form = _form;
			} else {
				_form = _this._form;
				paging = true;
			}
			
			if((page = _this.pagination.validatePage(page)) === false){
				return;
			}
			
			if (_form.$valid || paging) {
				_this.currentPage = page;
				_this._u.resetGlobalErrorMsg();
				_this._u.showLoading();
				
				var request = {
					userId: _this._u.checkNull(_this.filter.userId),
					firstName: _this._u.checkNull(_this.filter.firstName),
					lastName: _this._u.checkNull(_this.filter.lastName),
					email: _this._u.checkNull(_this.filter.email),
					countries: _this._u.checkNull(_this.filter.countries.getCheckedIds(), 'ids'),
					languageId: _this._u.checkNull(_this.filter.languages.getId(), 'id'),
					isTester: _this._u.checkNull(_this.filter.isTester.getId(), 'id'),
					suspended: _this._u.checkNull(_this.filter.suspended.getId(), 'id'),
					active: _this._u.checkNull(_this.filter.active.getId(), 'id'),
					isRegulationApproved: _this._u.checkNull(_this.filter.isRegulationApproved.getId(), 'id'),
					docType1: _this._u.checkNull(_this.filter.docType1.getId(), 'id'),
					docType2: _this._u.checkNull(_this.filter.docType2.getId(), 'id'),
					resolutionStatus1: _this._u.checkNull(_this.filter.resolutionStatus1.getCheckedIds(), 'ids'),
					resolutionStatus2: _this._u.checkNull(_this.filter.resolutionStatus2.getCheckedIds(), 'ids'),
					page: page,
					rowsPerPage: _this.filter.resultsPerPage.getId()
				}
				
				UserDocumentsService.get('getUsersDocuments', request)
					.then(function(data) {
						_this._u.resetGlobalErrorMsg();
						_this.results = data.usersDocuments;
						
						_this.pagination.setPages(page, data.totalCount, _this.filter.resultsPerPage.getId());
					})
					.catch(function(data) {
						_this._u.handleErrors(data, 'getUsersDocuments');
					})
					.finally(function(data) {
						_this._u.hideLoading();
					})
			} else {
				showToast(2, _this._u.getMsg('error.general_invalid_form'));
			}
		}
		
		function changePage(page) {
			getUsersDocuments(null, page);
		}
	}
})();