/* controller::MarketOptionPlusPricesController */
(function() {
	'use strict';

	angular.module('backendApp').controller('MarketOptionPlusPricesController', MarketOptionPlusPricesController);

	MarketOptionPlusPricesController.$inject = ['screenSettingsPrepService', 'utilsService', 'marketOptionPlusPricesService'];
	function MarketOptionPlusPricesController(screenSettingsPrepService, utilsService, marketOptionPlusPricesService) {
		var _this = this;
		_this._u = utilsService;
		
		var screenSettings = screenSettingsPrepService;
		
		_this.action = '';
		
		_this.marketId = null;
		
		_this.resultList = null;
		
		//filters
		_this.filter = {
			markets: [{id: -1, name: ''}]
		};
		
		_this.filter.market = _this.filter.markets[0];
		
		for(var market in screenSettings.optionPlusMarkets){
			if(screenSettings.optionPlusMarkets.hasOwnProperty(market)){
				_this.filter.markets.push({id: market, name: screenSettings.optionPlusMarkets[market]});
			}
		}
		
		_this.getMarketName = getMarketName;
		_this.getOptionPlusPrices = getOptionPlusPrices;
		_this.updatePrice = updatePrice;
		
		//functions
		function getMarketName(){
			return marketOptionPlusPricesService.getMarketName(_this.marketId, screenSettings.optionPlusMarkets);
		}
		
		function getOptionPlusPrices(marketId) {
			_this.marketId = marketId;
			
			_this._u.resetGlobalErrorMsg();
			_this._u.showLoading();
			
			var request = {
				marketId: _this.marketId
			};
			
			marketOptionPlusPricesService.getOptionPlusPrices(request)
				.then(function(data) {
					_this._u.resetGlobalErrorMsg();
					if(!_this._u.handleErrors(data, 'getOptionPlusPrices')){
						_this.resultList = data.optionPlusMatrix;
					}
				})
				.catch(function(data) {
					_this._u.handleErrors(data, 'getOptionPlusPrices');
				})
				.finally(function(data) {
					_this._u.hideLoading();
				})
		}
		
		function updatePrice(item) {
			_this._u.resetGlobalErrorMsg();
			_this._u.showLoading();
			
			var request = {
				marketId: _this.marketId,
				newPrice: item.value,
				item: {
					id: item.id
				}
			};
			
			marketOptionPlusPricesService.updatePrice(request)
				.then(function(data) {
					_this._u.resetGlobalErrorMsg();
					if(!_this._u.handleErrors(data, 'updatePrice')){
						showToast(TOAST_TYPES.info, _this._u.getMsg('general.update.success'));
					}
				})
				.catch(function(data) {
					_this._u.handleErrors(data, 'updatePrice');
				})
				.finally(function(data) {
					_this._u.hideLoading();
				})
		}
		
	}
})();