backendApp.service('Utils', [function(){
	var parseResponse = function(response){
		if(!response || !response.data){
			return null;
		}
		return response.data.data;
	}
	
	var DateManager = function(params){
		if(!params){
			params = {};
		}
		
		this.openDates = {};
		if(params.openDates && Array.isArray(params.openDates)){
			for(var i = 0; i < params.openDates.length; i++){
				this.openDates[params.openDates[i]] = false;
			}
		}
		this.format = params.format;
		this.onOpenCallback = params.onOpenCallback;
		
		if(!this.format){
			this.format = 'dd/MM/yyyy';
		}
		
		this.dateOptions = {
			showWeeks: false,
			startingDay: 0,
			defaultTime: '00:00:00'
			/*,
			ngModelOptions: {
				timezone: 'UTC'
			}*/
		};
		
		this.openCalendar = function(e, date){
			if(this.onOpenCallback){
				this.onOpenCallback();
			}
			this.openDates[date] = true;
		}
		
		this.save = function(date){
			return date;
		}
	}
	
	var loadingCounter = [];
	function showLoading(globalOverlay){
		loadingCounter.push(globalOverlay);
		$('.content').addClass('loading');
		if(globalOverlay){
			$('.content').addClass('loading-global');
		}
	}
	function hideLoading(){
		loadingCounter.pop();
		if(loadingCounter.length == 0){
			$('.content').removeClass('loading');
		}
		var hasGlobal = false;
		for(var i = 0; i < loadingCounter.length; i++){
			if(loadingCounter[i]){
				hasGlobal = true;
			}
		}
		if(!hasGlobal){
			$('.content').removeClass('loading-global');
		}
	}
	
	return {
		settings: settings,
		parseResponse: parseResponse,
		DateManager: DateManager,
		showLoading: showLoading,
		hideLoading: hideLoading
	};
}]);


//Class definitions
backendApp.service('Permissions', [function(){
	var Permissions = function(permissions){
		var rawPermissions = cloneObj(permissions);
		
		var parsePermissions = function(tree, list){
			if(typeof list == "undefined"){
				list = [];
			}
			
			if(tree.nodes){
				for(var i = 0; i < tree.nodes.length; i++){
					parsePermissions(tree.nodes[i], list);
				}
			}else if(Array.isArray(tree)){
				for(var i = 0; i < tree.length; i++){
					parsePermissions(tree[i], list);
				}
			}else{
				list.push({id: tree.id, allowed: false});
			}
			
			return list;
		}
		
		this.permissionsList = parsePermissions(rawPermissions);
		rawPermissions = null;
		
		this.get = function(permission){
			for(var i = 0; i < this.permissionsList.length; i++){
				if(this.permissionsList[i].id == permission){
					return this.permissionsList[i].allowed;
				}
			}
			return false;
		}
		
		this.allow = function(permissionId){
			for(var i = 0; i < this.permissionsList.length; i++){
				if(this.permissionsList[i].id == permissionId){
					this.permissionsList[i].allowed = true;
				}
			}
		}
	}
	return {
		InstanceClass: Permissions
	}
}]);


backendApp.service('BaseTree', [function(){
	var BaseTree = function(tree, active){
		var that = this;
		
		this.root = cloneObj(tree);
		this.active = (typeof active == "undefined") ? false : active;
		
		function initTree(tree, parent){
			if(typeof parent == "undefined"){
				parent = null;
			}
			if(Array.isArray(tree)){
				for(var i = 0; i < tree.length; i++){
					tree[i].parent = parent;
					tree[i].root = that.root;
					if(!tree[i].active){
						tree[i].active = that.active;
					}
					if(!tree[i].collapsed){
						tree[i].collapsed = false;
					}
					if(tree[i].nodes){
						initTree(tree[i].nodes, tree[i]);
					}
				}
			}
		}
		
		initTree(this.root);
		
		this.getTreeTop = function(node){
			if(node.nodes && node.parent){
				return getTreeTop(node.parent);
			}else if(node.parent == null){
				return node;
			}else{
				return null;
			}
		}
	}
	
	BaseTree.toggleNode = function(node){
		node.collapsed = !node.collapsed;
	}
	
	return {
		InstanceClass: BaseTree
	}
}]);

backendApp.service('MenuTree', ['BaseTree', function(BaseTree){
	var MenuTree = function(tree, active, permissions){
		//Inherit BaseTree
		BaseTree.InstanceClass.apply(this, arguments);
		
		var that = this;
		
		this.uncollapseCurrent = function(tree){
			if(Array.isArray(tree)){
				for(var i = 0; i < tree.length; i++){
					if(tree[i].current){
						var node = tree[i].parent;
						while(node != null){
							if(node.hasOwnProperty('collapsed')){
								node.current = true;
								node.collapsed = false;
							}
							node = node.parent;
						}
						parent = null;
					}
					if(tree[i].nodes){
						this.uncollapseCurrent(tree[i].nodes);
					}
				}
			}
		}
		
		this.uncollapseCurrent(this.root);
		
		this.updateMenuNodes = function(validateCallback){
			updateMenuNodesRecursive(this.root, validateCallback);
			this.uncollapseCurrent(this.root);
		}
		
		function updateMenuNodesRecursive(tree, validateCallback){
			for(var i = 0; i < tree.length; i++){
				if(validateCallback(tree[i].link)){
					tree[i].current = true;
				}else{
					tree[i].current = false;
				}
				if(tree[i].nodes){
					tree[i].collapsed = true;
					updateMenuNodesRecursive(tree[i].nodes, validateCallback);
				}else{
					if(!permissions.get(tree[i].permission)){
						tree.splice(i, 1);
						i--;
					}
				}
			}
		}
	}
	
	//Inherit static members
	for(staticMember in BaseTree.InstanceClass){
		if(BaseTree.InstanceClass.hasOwnProperty(staticMember)){
			MenuTree[staticMember] = BaseTree.InstanceClass[staticMember];
		}
	}
	
	return {
		InstanceClass: MenuTree
	}
}]);

backendApp.service('CheckboxTree', ['BaseTree', function(BaseTree){
	var CheckboxTree = function(tree, active){
		//Inherit BaseTree
		BaseTree.InstanceClass.apply(this, arguments);
		
		var that = this;
		
		function initTree(tree, parent){
			if(typeof parent == "undefined"){
				parent = null;
			}
			if(Array.isArray(tree)){
				for(var i = 0; i < tree.length; i++){
					if(!tree[i].nodes){
						if(!tree[i].checked){
							tree[i].checked = false;
						}
					}else{
						if(!tree[i].checkedStatus){
							tree[i].checkedStatus = 0;
						}
						initTree(tree[i].nodes, tree[i]);
					}
				}
			}
		}
		
		initTree(this.root);
		
		this.normalizeCheckedStatus = function(){
			this.normalizeCheckedStatusRecursive(this.root);
		}
		
		this.normalizeCheckedStatusRecursive = function(tree){
			if(tree.nodes){
				var numLeaves = 0;
				var countUnchecked = 0;
				var countSemiChecked = 0;
				var countChecked = 0;
				for(var i = 0; i < tree.nodes.length; i++){
					if(!tree.nodes[i].nodes){
						numLeaves++;
						if(tree.nodes[i].checked){
							countChecked++;
						}else{
							countUnchecked++;
						}
					}else{
						var temp = this.normalizeCheckedStatusRecursive(tree.nodes[i]);
						if(temp == 2){
							countChecked++;
						}else if(temp == 1){
							countSemiChecked++;
						}else{
							countUnchecked++;
						}
					}
				}
				if(countSemiChecked == 0 && countChecked == 0){
					tree.checkedStatus = 0;
				}else if(countUnchecked == 0 && countSemiChecked == 0){
					tree.checkedStatus = 2;
				}else{
					tree.checkedStatus = 1;
				}
				return tree.checkedStatus;
			}else if(Array.isArray(tree)){
				for(var i = 0; i < tree.length; i++){
					this.normalizeCheckedStatusRecursive(tree[i]);
				}
			}else{
				return;
			}
		}
		
		this.checkNode = function(tree, value){
			if(tree.name && tree.name == value){
				tree.checked = true;
			}
			if(tree.nodes){
				for(var i = 0; i < tree.nodes.length; i++){
					this.checkNode(tree.nodes[i], value);
				}
			}else if(Array.isArray(tree)){
				for(var i = 0; i < tree.length; i++){
					this.checkNode(tree[i], value);
				}
			}
		}
		
		this.uncheckNode = function(tree, value){
			if(tree.name && tree.name == value){
				tree.checked = false;
			}
			if(tree.nodes){
				for(var i = 0; i < tree.nodes.length; i++){
					this.uncheckNode(tree.nodes[i], value);
				}
			}else if(Array.isArray(tree)){
				for(var i = 0; i < tree.length; i++){
					this.uncheckNode(tree[i], value);
				}
			}
		}
		
		this.getCheckedNodes = function(onlyChanged, tree, list){
			if(typeof list == "undefined"){
				list = [];
			}
			if(typeof tree == "undefined"){
				tree = this.root;
			}
			
			if(tree.nodes){
				for(var i = 0; i < tree.nodes.length; i++){
					this.getCheckedNodes(onlyChanged, tree.nodes[i], list);
				}
			}else if(Array.isArray(tree)){
				for(var i = 0; i < tree.length; i++){
					this.getCheckedNodes(onlyChanged, tree[i], list);
				}
			}else{
				if(tree.checked && (!onlyChanged || (tree.changed && tree.checked != tree.originalChecked))){
					list.push(tree.id);
				}
			}
			
			return list;
		}
		
		this.getUncheckedNodes = function(onlyChanged, tree, list){
			if(typeof list == "undefined"){
				list = [];
			}
			if(typeof tree == "undefined"){
				tree = this.root;
			}
			
			if(tree.nodes){
				for(var i = 0; i < tree.nodes.length; i++){
					this.getUncheckedNodes(onlyChanged, tree.nodes[i], list);
				}
			}else if(Array.isArray(tree)){
				for(var i = 0; i < tree.length; i++){
					this.getUncheckedNodes(onlyChanged, tree[i], list);
				}
			}else{
				if(!tree.checked && (!onlyChanged || (tree.changed && tree.checked != tree.originalChecked))){
					list.push(tree.id);
				}
			}
			
			return list;
		}
	}
	
	//Inherit static members
	for(staticMember in BaseTree.InstanceClass){
		if(BaseTree.InstanceClass.hasOwnProperty(staticMember)){
			CheckboxTree[staticMember] = BaseTree.InstanceClass[staticMember];
		}
	}
	
	CheckboxTree.updateTree = function(node){
		if(node.active){
			CheckboxTree.updateTreeCheckedDown(node);
			CheckboxTree.updateTreeCheckedUp(node);
		}
	}
	
	CheckboxTree.updateTreeCheckedDown = function(node){
		var checkedStatus = CheckboxTree.getCheckedStatus(node);
		if(checkedStatus == 0){
			CheckboxTree.updateNodeStatusDown(node, 2);
		}else if(checkedStatus == 1){
			CheckboxTree.updateNodeStatusDown(node, 2);
		}else{
			CheckboxTree.updateNodeStatusDown(node, 0);
		}
	}
	
	CheckboxTree.updateNodeStatusDown = function(node, status){
		if(node.nodes){
			node.checkedStatus = status;
			for(var i = 0; i < node.nodes.length; i++){
				if(node.nodes[i].nodes){
					node.nodes[i].checkedStatus = status;
					CheckboxTree.updateNodeStatusDown(node.nodes[i], status);
				}else{
					if(!node.nodes[i].changed){
						node.nodes[i].changed = true;
						node.nodes[i].originalChecked = node.nodes[i].checked;
					}
					node.nodes[i].checked = (status == 0) ? false : true;
				}
			}
		}else{
			if(!node.changed){
				node.changed = true;
				node.originalChecked = node.checked;
			}
			node.checked = (status == 0) ? false : true;
		}
	}
	
	CheckboxTree.updateTreeCheckedUp = function(node){
		var checkedStatus = CheckboxTree.getCheckedStatus(node);
		if(node.parent){
			if(checkedStatus == 0){
				CheckboxTree.updateNodeStatusUp(node.parent, 2);
			}else if(checkedStatus == 1){
				CheckboxTree.updateNodeStatusUp(node.parent, 2);
			}else{
				CheckboxTree.updateNodeStatusUp(node.parent, 0);
			}
		}
	}
	
	CheckboxTree.updateNodeStatusUp = function(node){
		if(node.nodes){
			var status = 0;
			var countUnchecked = 0;
			var countSemiChecked = 0;
			var countChecked = 0;
			for(var i = 0; i < node.nodes.length; i++){
				if(node.nodes[i].nodes){
					if(node.nodes[i].checkedStatus == 0){
						countUnchecked++;
					}else if(node.nodes[i].checkedStatus == 1){
						countSemiChecked++;
					}else if(node.nodes[i].checkedStatus == 2){
						countChecked++;
					}
				}else{
					if(node.nodes[i].checked){
						countChecked++;
					}else{
						countUnchecked++;
					}
				}
			}
			if(countSemiChecked == 0 && countChecked == 0){
				status = 0;
			}else if(countUnchecked == 0 && countSemiChecked == 0){
				status = 2;
			}else{
				status = 1;
			}
			node.checkedStatus = status;
			if(node.parent){
				CheckboxTree.updateNodeStatusUp(node.parent);
			}
		}
	}
	
	CheckboxTree.getCheckedStatus = function(node){
		var checkedStatus = 0;
		if(node.nodes){
			checkedStatus = node.checkedStatus;
		}else{
			checkedStatus = node.checked ? 2 : 0;
		}
		return checkedStatus;
	}
	
	return {
		InstanceClass: CheckboxTree
	}
}]);


backendApp.service('Pagination', [function(){
	var Pagination = function(params){
		var that = this;
		
		this.pages = [];
		this.currentPage = 1;
		this.maxPage = 1;
		this.totalCount = 0;
		this.resultsPerPage = 20;
		
		this.callback = params.callback;
		this.hideSummary = params.hideSummary;
		this.hideGroupNavigation = params.hideGroupNavigation;
		
		this.setPages = function(currentPage, totalCount, resultsPerPage){
			this.currentPage = currentPage;
			this.maxPage = Math.ceil(totalCount/resultsPerPage);
			this.totalCount = totalCount;
			this.resultsPerPage = resultsPerPage;
			this.pages = [];
			for(var i = Math.max(0, currentPage - 4); i < Math.min(this.maxPage, Math.max(0, currentPage - 4) + 10); i++){
				this.pages.push(i+1);
			}
		}
		
		this.validatePage = function(page, allowCurrentPage){
			if(typeof page != 'undefined' && page == this.currentPage && !allowCurrentPage){
				return false;
			}
			if(typeof page == 'undefined'){
				page = 1;
			}
			return page;
		}
		
		this.setPages(this.currentPage, this.maxPage);
		
		this.previousPage = function(){
			return Math.max(this.currentPage - 1, 1);
		}
		this.nextPage = function(){
			return Math.min(this.currentPage + 1, this.maxPage);
		}
		this.previousPageGroup = function(){
			return Math.max(this.currentPage - 10, 1);
		}
		this.nextPageGroup = function(){
			return Math.min(this.currentPage + 10, this.maxPage);
		}
	}
	
	return {
		InstanceClass: Pagination
	}
}]);


backendApp.service('SingleSelect', ['$timeout', '$rootScope', function($timeout, $rootScope){
	var SingleSelect = function(params){
		var that = this;
		
		if(!params){
			params = {};
		}
		
		this.visible = false;
		
		this.externalModel = params.externalModel;
		if(this.externalModel){
			if(this.externalModel.bindById !== false){
				this.externalModel.bindById = true;
			}
			if(this.externalModel.bindByIdInt !== false){
				this.externalModel.bindByIdInt = true;
			}
		}
		this.validateExternalModel = function(){
			if(this.externalModel && typeof this.externalModel.objCallback == 'function' && (typeof this.externalModel.objCallback() != 'undefined' && this.externalModel.objCallback() != null)){
				return true;
			}
			return false;
		}
		$timeout(function(){
			$rootScope.$watch(function(){
				return that.validateExternalModel() ? that.externalModel.objCallback()[that.externalModel.key] : '';
			}, function(newValue, oldValue){
				if(!that.validateExternalModel()){
					return;
				}
				if(that.externalModel.bindById){
					if(that.model && that.externalModel.objCallback()[that.externalModel.key] == that.model.id){
						return;
					}
					that.model = null;
					for(var i = 0; i < that.options.length; i++){
						if(that.externalModel.objCallback()[that.externalModel.key] == that.options[i].id){
							that.model = (typeof that.model == 'string') ? that.options[i] + '' : that.options[i];
						}
					}
				}else{
					if(that.model && that.externalModel.objCallback()[that.externalModel.key] == that.model){
						return;
					}
					that.model = null;
					for(var i = 0; i < that.options.length; i++){
						if(that.externalModel.objCallback()[that.externalModel.key] == that.options[i]){
							that.model = that.options[i];
						}
					}
				}
			}, true);
		});
		
		this.isSmart = params.isSmart;
		this.noSort = params.noSort;
		this.sortById = params.sortById;
		this.sortByNameInt = params.sortByNameInt;
		this.mutateCallback = params.mutateCallback;
		this.mutateOnlyAggregate = params.mutateOnlyAggregate;
		this.iconCallback = params.iconCallback;
		
		this.iconClosed = params.iconClosed;
		this.iconOpen = params.iconOpen;
		
		this.filterText = '';
		this.filterCallback = params.filterCallback;
		
		this.filterPlaceholder = params.filterPlaceholder;
		
		this.model = null;
		this.options = [];
		this.aggregateOption = params.aggregateOption;
		
		this.setModel = function(option){
			this.model = option;
			if(this.externalModel && typeof this.externalModel.objCallback == 'function'){
				if(this.externalModel.bindById){
					this.externalModel.objCallback()[this.externalModel.key] = this.externalModel.bindByIdInt ? parseInt(this.model.id) : (this.model.id ? this.model.id : null);
				}else{
					this.externalModel.objCallback()[this.externalModel.key] = this.model;
				}
			}
		}
		
		if(this.aggregateOption){
			this.aggregateOption.isAggregate = true;
			this.options.push(this.aggregateOption);
			this.setModel(this.aggregateOption);
		}
		
		this.fillOptions = function(options, setModelToFirst){
			this.options = [];
			if(this.aggregateOption){
				this.options.push(this.aggregateOption);
			}
			if(options && Array.isArray(options)){
				for(var i = 0; i < options.length; i++){
					this.options.push(options[i]);
				}
			}else if(options){
				for(var el in options){
					if(options.hasOwnProperty(el)){
						var name = options[el];
						this.options.push({id: el, name: name});
					}
				}
			}
			if(setModelToFirst && this.options.length > 0){
				this.setModel(this.options[0]);
			}
			this.filter();
		}
		
		this.setFilterCallback = function(callback){
			this.filterCallback = callback;
		}
		
		
		this.filteredOptions = [];
		this.filter = function(){
			that.filteredOptions = that.options.filter(that.filterFunction);
			if(!that.noSort){
				that.filteredOptions.sort(that.sortFunction);
			}
		}
		this.filterFunction = function(option){
			var filterTextMatch = true;
			if(that.mutateCallback && (!that.mutateOnlyAggregate || option.isAggregate)){
				filterTextMatch = that.mutateCallback(option.name).search(new RegExp(that.filterText, "i")) != -1;
			}else{
				filterTextMatch = option.name.search(new RegExp(that.filterText, "i")) != -1;
			}
			if(that.filterCallback && typeof that.filterCallback == 'function' && !option.isAggregate){
				var filterCallbackResult = that.filterCallback(option);
				if(!filterCallbackResult){
					if(that.model == option){
						//TODO - perhaps find the first option that would not be filtered by filterCallback and set the model to that option
						if(that.aggregateOption){
							that.model = that.aggregateOption;
						}else{
							that.model = null;
						}
					}
				}
				return filterTextMatch && filterCallbackResult;
			}
			return filterTextMatch;
		}
		
		this.sortFunction = function(a, b){
			if(a == that.aggregateOption){
				return -1;
			}
			if(b == that.aggregateOption){
				return 1;
			}
			if(that.sortById){
				return parseInt(a.id) - parseInt(b.id);
			}
			if(that.sortByNameInt){
				return parseInt(a.name) - parseInt(b.name);
			}
			aName = a.name;
			bName = b.name
			if(that.mutateCallback && (!that.mutateOnlyAggregate || a == that.aggregateOption)){
				aName = that.mutateCallback(aName);
			}
			if(that.mutateCallback && (!that.mutateOnlyAggregate || b == that.aggregateOption)){
				bName = that.mutateCallback(bName);
			}
			aName = aName.trim();
			bName = bName.trim();
			if(aName < bName){
				return -1;
			}else if(aName > bName){
				return 1;
			}else{
				return 0;
			}
		}
		
		this.sort = function(option){
			if(that.noSort){
				return '';
			}
			if(that.sortById){
				return option == that.aggregateOption ? -1000 : parseInt(option.id);
			}
			if(that.sortByNameInt){
				return option == that.aggregateOption ? -1000 : parseInt(option.name);
			}
			if(that.mutateCallback && (!that.mutateOnlyAggregate || option == that.aggregateOption)){
				return option == that.aggregateOption ? '' : that.mutateCallback(option.name).trim();
			}
			return option == that.aggregateOption ? '' : option.name.trim();
		}
		
		if(params.options){
			this.fillOptions(params.options);
		}
		
		this.getTextValue = function(){
			if(this.mutateCallback && (!this.mutateOnlyAggregate || this.options[i] == this.aggregateOption)){
				this.model != null ? this.mutateCallback(this.model.name) : '';
			}
			return this.model != null ? this.model.name : '';
		}
		
		this.getId = function(){
			if(!this.model){
				return null;
			}
			return this.model.id;
		}
		
		this.getOptionById = function(id){
			var val = (id || id == 0) ? this.options[searchJsonKeyInArray(this.options, 'id', id)] : null;
			if(!val){
				return null;
			}
			return val;
		}
		
		this.getElNameById = function(id){
			var val = (id || id == 0) ? this.options[searchJsonKeyInArray(this.options, 'id', id)] : null;
			if(!val){
				return '';
			}
			if(this.mutateCallback && (!this.mutateOnlyAggregate || val == this.aggregateOption)){
				this.mutateCallback(val.name);
			}
			return val.name;
		}
		
		this.setById = function(id){
			this.setModel(this.getOptionById(id));
		}
		
		this.getPreviousOption = function(){
			if(this.model.index == 0){
				return null;
			}
			for(var i = 0; i < this.options.length; i++){
				if(this.options[i].index == this.model.index - 1){
					return this.options[i];
				}
			}
			return null;
		}
		this.getNextOption = function(){
			if(this.model.index == this.options.length - 1){
				return null;
			}
			for(var i = 0; i < this.options.length; i++){
				if(this.options[i].index == this.model.index + 1){
					return this.options[i];
				}
			}
			return null;
		}
		this.getNextOptionByKey = function(key){
			var firstMatch = null;
			var nextMatch = null;
			for(var i = 0; i < this.options.length; i++){
				if(this.options[i].index != this.model.index){
					var name = this.options[i].name;
					if(this.mutateCallback && (!this.mutateOnlyAggregate || this.options[i] == this.aggregateOption)){
						name = this.mutateCallback(name);
					}
					if(name && name.substring(0, 1).localeCompare(key, undefined, {sensitivity: 'base'}) == 0){
						if(this.options[i].index < this.model.index && (firstMatch == null || firstMatch.index > this.options[i].index)){
							firstMatch = this.options[i];
						}
						if(this.options[i].index > this.model.index && (nextMatch == null || nextMatch.index > this.options[i].index)){
							nextMatch = this.options[i];
						}
					}
				}
			}
			if(nextMatch){
				return nextMatch;
			}else if(firstMatch){
				return firstMatch;
			}
			return null;
		}
		
		this.clone = function(){
			var clone = new SingleSelect(cloneObj(params));
			var options = [];
			var modelIndex = 0;
			for(var i = 0; i < this.options.length; i++){
				if(this.options[i] == this.model){
					modelIndex = i;
				}
				if(!this.options[i].isAggregate){
					options.push(angular.copy(this.options[i]));
				}
			}
			clone.fillOptions(options);
			clone.setModel(clone.options[modelIndex]);
			return clone;
		}
	}
	
	return {
		InstanceClass: SingleSelect
	}
}]);


backendApp.service('Multiselect', [function(){
	var Multiselect = function(params){
		var that = this;
		
		this.visible = false;
		this.isSmart = params.isSmart;
		this.noSort = params.noSort;
		this.sortById = params.sortById;
		this.mutateCallback = params.mutateCallback;
		this.mutateOnlyAggregate = params.mutateOnlyAggregate;
		this.iconCallback = params.iconCallback;
		
		this.filterText = '';
		this.filterCallback = params.filterCallback;
		
		this.options = [];
		this.aggregateOption = params.aggregateOption;
		if(this.aggregateOption){
			this.aggregateOption.isAggregate = true;
			this.aggregateOption.checked = true;
			this.aggregateOption.visible = true;
			this.options.push(this.aggregateOption);
		}
		
		this.fillOptions = function(options){
			this.options = [];
			if(this.aggregateOption){
				this.options.push(this.aggregateOption);
			}
			if(options && Array.isArray(options)){
				for(var i = 0; i < options.length; i++){
					if(!options[i].checked){
						options[i].checked = false;
					}
					if(!options[i].visible){
						options[i].visible = true;
					}
					this.options.push(options[i]);
				}
			}else if(options){
				for(var el in options){
					if(options.hasOwnProperty(el)){
						this.options.push({id: el, name: options[el], checked: false, visible: true});
					}
				}
			}
		}
		
		if(params.options){
			this.fillOptions(params.options);
		}
		
		this.setFilterCallback = function(callback){
			this.filterCallback = callback;
		}
		
		this.filter = function(option){
			var filterTextMatch = true;
			if(that.mutateCallback && (!that.mutateOnlyAggregate || option.isAggregate)){
				filterTextMatch = that.mutateCallback(option.name).search(new RegExp(that.filterText, "i")) != -1;
			}else{
				filterTextMatch = option.name.search(new RegExp(that.filterText, "i")) != -1;
			}
			if(that.filterCallback && typeof that.filterCallback == 'function' && !option.isAggregate){
				var filterCallbackResult = that.filterCallback(option);
				if(!filterCallbackResult){
					if(option.checked){
						that.toggle(option);
					}
				}
				return filterTextMatch && filterCallbackResult;
			}
			return filterTextMatch;
		}
		
		this.sort = function(option){
			if(that.noSort){
				return '';
			}
			if(that.sortById){
				return option.isAggregate ? -1000 : parseInt(option.id);
			}
			if(that.mutateCallback && (!that.mutateOnlyAggregate || option.isAggregate)){
				return option.isAggregate ? '' : that.mutateCallback(option.name).trim();
			}
			return option.isAggregate ? '' : option.name.trim();
		}
		
		this.toggle = function(option){
			if(option.isAggregate){
				if(option.checked){
					return;
				}
				option.checked = !option.checked;
				for(i = 0; i < this.options.length; i++){
					if(this.options[i].isAggregate){
						continue;
					}
					this.options[i].checked = false;
				}
			}else{
				option.checked = !option.checked;
				if(this.aggregateOption){
					this.aggregateOption.checked = false;
				}
				var checkedCount = 0;
				for(i = 0; i < this.options.length; i++){
					if(this.options[i].checked){
						checkedCount++;
					}
				}
				if(checkedCount == 0){
					if(this.aggregateOption){
						this.aggregateOption.checked = true;
					}
				}else if((!this.aggregateOption && checkedCount == this.options.length) || (this.aggregateOption && checkedCount == this.options.length - 1)){
					if(this.aggregateOption){
						this.aggregateOption.checked = true;
						for(i = 0; i < this.options.length; i++){
							if(!this.options[i].isAggregate){
								this.options[i].checked = false;
							}
						}
					}
				}
			}
		}
		
		this.getCheckedIds = function(includeAggregate){
			var arr = [];
			for(var i = 0; i < this.options.length; i++){
				if((includeAggregate || !this.options[i].isAggregate) && this.options[i].checked){
					arr.push(this.options[i].id);
				}
			}
			return arr;
		}
		
		this.setSelectedOptions = function(ids){
			for(var i = 0; i < ids.length; i++){
				var key = -1;
				if((key = searchJsonKeyInArray(this.options, 'id', ids[i])) > -1){
					this.toggle(this.options[key]);
				}
			}
		}
		
		this.getOptionById = function(id){
			var val = (id || id == 0) ? this.options[searchJsonKeyInArray(this.options, 'id', id)] : null;
			if(!val){
				return null;
			}
			return val;
		}
		
		this.getElNameById = function(id){
			var val = (id || id == 0) ? this.options[searchJsonKeyInArray(this.options, 'id', id)] : null;
			if(!val){
				return '';
			}
			if(this.mutateCallback && (!this.mutateOnlyAggregate || val.isAggregate)){
				return this.mutateCallback(val.name);
			}
			return val.name;
		}
	}
	
	return {
		InstanceClass: Multiselect
	}
}]);