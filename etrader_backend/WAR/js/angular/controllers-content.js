backendApp.controller('ContentCtr', ['$rootScope', '$scope', '$http', function($rootScope, $scope, $http) {
	
}]);

backendApp.controller('TermsCtr', ['$rootScope', '$scope', '$http', '$uibModal', function($rootScope, $scope, $http, $uibModal) {
	$scope.filter = {};
	$scope.filter.platforms = [{id: 2, name: 'ANYOPTION', skins: []}, {id: 3, name: 'COPYOP', skins: []}];
	$scope.filter.platform = $scope.filter.platforms[0];
	$scope.filter.skin = {};
	$scope.filter.part = {};
	$scope.filterBy = [2];
	
	$scope.file = {};
	$scope.file.title = "";
	$scope.file.content = "";
	$scope.file.selectedType = 1;
	$scope.tinymceReady = false;
	
	$scope.selectedPlatform = '';
	$scope.selectedPlatformName = '';
	$scope.selectedSkin = '';
	$scope.selectedSkinName = '';
	$scope.selectedPart = '';
	$scope.selectedPartName = '';
	
	$scope.previewTerms = {};
	$scope.previewTerms.content = '';
	
	$scope.tinymceParams = [
		{name: 'assets', display: 'block', title: 'Insert assets'},
		{name: 'fee', display: 'inline', title: 'Insert fee'},
		{name: 'support_email', display: 'inline', title: 'Insert support email'},
		{name: 'privacy_link', display: 'inline', title: 'Insert privacy link'},
		{name: 'company_name', display: 'inline', title: 'Insert company name'},
		{name: 'compliance_email', display: 'inline', title: 'Insert compliance email'},
		{name: 'index_page', display: 'inline', title: 'Insert index page link'}
	];
	
	$scope.initWatch = $scope.$watch(function(){return $rootScope.ready;}, function(){
		if($rootScope.ready){
			$scope.initWatch();
			$scope.init();
		}
	});
	
	$scope.init = function(){
		//$scope.getTermsFilters();
		$scope.getTermsPermisionsScreenSettings();
		$scope.initTinymce();
	}
	
	$scope.setDefaultSkin = function(){
		$scope.filter.skin = $scope.filter.platform.skins[0];
		$scope.setDefaultPart();
	}
	$scope.setDefaultPart = function(){
		$scope.filter.part = $scope.filter.skin.parts[0];
	}
	
	$scope.getTermsPermisionsScreenSettings = function(){
		var methodRequest = {};
		$http.post(settings.backendJsonLink + 'TermsServices/getTermsPermisionsScreenSettings', methodRequest)
			.then(function(data) {
				$rootScope.resetGlobalErrorMsg();
				for(var i = 0; i < data.permissionsList.length; i++){
					$rootScope.permissions.allow(data.permissionsList[i]);
				}
				for(var i = 0; i < $scope.filter.platforms.length; i++){
					for(var skin in skinMap){
						if(skinMap.hasOwnProperty(skin)){
							if(typeof data.filters[$scope.filter.platforms[i].id] != "undefined" && typeof data.filters[$scope.filter.platforms[i].id][skin] != "undefined"){
								var parts = [];
								for(j = 0; j < data.filters[$scope.filter.platforms[i].id][skin].length; j++){
									parts.push(data.filters[$scope.filter.platforms[i].id][skin][j]);
								}
								$scope.filter.platforms[i].skins.push({
									id: skin, 
									name: $rootScope.getMsgs(skinMap[skin].skinTranslation), 
									parts: parts,
									platforms: skinMap[skin].platforms
								});
							}
						}
					}
				}
				$scope.filter.skin = $scope.filter.platform.skins[0];
				$scope.filter.part = $scope.filter.skin.parts[0];
			})
			.catch(function(data) {
				$rootScope.handleErrors(data, 'getTermsPermisionsScreenSettings');
			})
	}
	
	$scope.initTinymce = function(){
		var cssLink = 'https://www.bgtestenv.anyoption.com/css/styles_ao.css';
		if($scope.filter.platform.id == 3){
			cssLink = '//fonts.googleapis.com/css?family=Roboto,https://www.copyop.com/css/style.css';
		}
		$scope.tinymceOptions = {
			selector: 'textarea#annex',
			menubar: 'view edit insert table format tools',
			plugins: 'advlist autolink link image lists table paste searchreplace charmap fullscreen noneditable code',
			content_css: cssLink + '?version=' + new Date().getTime(),
			content_style: 'body{background:none!important}',
			paste_convert_word_fake_lists: true,
			paste_remove_styles_if_webkit: true,
			paste_remove_styles: true,
			paste_strip_class_attributes: true,
			table_appearance_options: false,
			/*invalid_elements : 'table,thead,tbody,tr,td',*/
			style_formats: [
				{ title: 'title', block : 'h1', classes : 'general_terms_h1', exact : true },
				{ title: 'sub-title', block : 'h2', classes : 'general_terms_h2', exact : true },
				{ title: 'item-title', inline : 'span', classes : 'item-title', exact : true },
				{ title: 'sub-item-title', inline : 'span', classes : 'sub-item-title', exact : true },
				{ title: 'item-no', inline : 'span', classes : 'item-no', exact : true },
				{ title: 'No indentation', selector: 'ul', classes : 'no-indent' },
				{ title: 'Half column', block: 'div', wrapper: true, classes: 'half-col', exact: true }
			],
			formats: {
			},
			setup: function (editor) {
				for(var i = 0; i < $scope.tinymceParams.length; i++){
					var insertParam = function(param){
						var content = '<span class="mceNonEditable">${' + param.name + '}</span>';
						if(param.display == 'block'){
							content = '<div class="mceNonEditable">${' + param.name + '}</div><br/>';
						}
						editor.addMenuItem('insert' + param.name, {
							text: param.title,
							context: 'insert',
							onclick: function () {
								editor.insertContent(content);
							}
						});
					}($scope.tinymceParams[i]);
				}
				editor.addMenuItem('custominsertseparator', {
					text: '-',
					context: 'insert'
				});
			}
		};
		$scope.tinymceReady = true;
	}
	
	$scope.getTermsFile = function(){
		showLoading();
		var methodRequest = {};
		methodRequest.platformIdFilter = $scope.filter.platform.id;
		methodRequest.skinIdFilter = $scope.filter.skin.id;
		methodRequest.partIdFilter = $scope.filter.part.partsId;
		$scope.initTinymce();
		$scope.$broadcast('$tinymce:refresh');
		$http.post(settings.backendJsonLink + 'TermsServices/getTermsFile', methodRequest)
			.then(function(data) {
				$rootScope.resetGlobalErrorMsg();
				$scope.file.title = data.termsPartFile.title;
				$scope.file.content = data.termsPartFile.html;
				$scope.file.selectedType = data.termsPartFile.typeId;
				$scope.file.isActive = data.termsPartFile.isActive;
				$scope.selectedPlatform = $scope.filter.platform.id;
				$scope.selectedPlatformName = $scope.filter.platform.name;
				$scope.selectedSkin = $scope.filter.skin.id;
				$scope.selectedSkinName = $scope.filter.skin.name;
				$scope.selectedPart = $scope.filter.part.partsId;
				$scope.selectedPartName = $scope.filter.part.partsName;
			})
			.catch(function(data) {
				$rootScope.handleErrors(data, 'getTermsFile');
			})
			.finally(function() {
				hideLoading();
			})
	}
	
	$scope.showPreview = function(){
		showLoading(true);
		var previewDependencies = {name: 'previewDependencies', dependencies: {saveTermsFile: false, getTermsFiles: false}};
		$rootScope.addDependency($scope, previewDependencies, function(){hideLoading();});
		
		$scope.saveTermsFile(previewDependencies.name);
	}
	
	$scope.saveTermsFile = function(dependencyName){
		var methodRequest = {};
		methodRequest.platformIdFilter = $scope.selectedPlatform;
		methodRequest.skinIdFilter = $scope.selectedSkin;
		methodRequest.partIdFilter = $scope.selectedPart;
		methodRequest.title = $scope.file.title;
		methodRequest.html = $scope.file.content;
		$http.post(settings.backendJsonLink + 'TermsServices/saveTermsFile', methodRequest)
			.then(function(data) {
				$rootScope.resetGlobalErrorMsg();
				$scope.openPreviewPopup();
				$scope.getTermsFiles(dependencyName);
			})
			.catch(function(data) {
				showToast(TOAST_TYPES.error, $rootScope.getMsgs('changes.saved.error'));
				$rootScope.handleErrors(data, 'saveTermsFile')
			})
			.finally(function() {
				$rootScope.markDependencyDone($scope, dependencyName, 'saveTermsFile');
			})
	}
	
	$scope.openPreviewPopup = function(){
		var previewModal = $uibModal.open({
			templateUrl: folderPrefix + 'components/terms-preview-popup.html',
			scope: $scope,
			windowClass: 'terms-preview-popup'
		});
	}
	
	$scope.getTermsFiles = function(dependencyName){
		var methodRequest = {};
		methodRequest.platformIdFilter = $scope.selectedPlatform;
		methodRequest.skinIdFilter = $scope.selectedSkin;
		methodRequest.termsType = $scope.file.selectedType;
		methodRequest.partIdFilter = $scope.selectedPart;
		$http.post(settings.backendJsonLink + 'TermsServices/getTermsFiles', methodRequest)
			.then(function(data) {
				$rootScope.resetGlobalErrorMsg();
				function filterTitle(str){
					var result = "";
					if(str){
						result = str.replace(/\"/g, "");
					}
					return result;
				}
				var container = document.createElement("div");
				data.filesList.sort(function(a, b) {
					return parseInt(a.orderNum) - parseInt(b.orderNum);
				});
				
				var div = document.createElement("div");
				for(var i = 0; i < data.filesList.length; i++){
					var anchor = document.createElement("a");
					anchor.className = "anchor";
					anchor.name = data.filesList[i].title ? filterTitle(data.filesList[i].title) : data.filesList[i].id;
					div.appendChild(anchor);
					anchor = null;
					var part = document.createElement("div");
					part.innerHTML = data.filesList[i].html;
					div.appendChild(part);
					part = null;
				}
				container.appendChild(div);
				div = null;
				var cssLink = 'https://www.anyoption.com/css/styles_ao.css';
				if($scope.selectedPlatform == 3){
					cssLink = 'https://www.copyop.com/css/style.css';
				}
				document.getElementById('previewContentIframe').contentDocument.body.innerHTML = '<link rel="stylesheet" type="text/css" href="' + cssLink + '"></link>' + container.innerHTML;
				document.getElementById('previewContentIframe').contentDocument.body.style.background = 'none';
				document.getElementById('previewContentIframe').contentDocument.body.style.padding = '0px 20px';
			})
			.catch(function(data) {
				$rootScope.handleErrors(data, 'getTermsFiles')
			})
			.finally(function() {
				$rootScope.markDependencyDone($scope, dependencyName, 'getTermsFiles');
			})
	}
	
	$scope.copyToLive = function(successCallback){
		showLoading(true);
		var methodRequest = {};
		methodRequest.platformIdFilter = $scope.selectedPlatform;
		methodRequest.skinIdFilter = $scope.selectedSkin;
		methodRequest.partIdFilter = $scope.selectedPart;
		methodRequest.isActive = $scope.file.isActive;
		methodRequest.title = $scope.file.title;
		$http.post(settings.backendJsonLink + 'TermsServices/copyTermsFileLiveFolder', methodRequest)
			.then(function(data) {
				$rootScope.resetGlobalErrorMsg();
				successCallback();
				showToast(TOAST_TYPES.info, $rootScope.getMsgs('changes.saved.successfully'));
			})
			.catch(function(data) {
				showToast(TOAST_TYPES.error, $rootScope.getMsgs('changes.saved.error'));
				$rootScope.handleErrors(data, 'copyTermsFileLiveFolder');
			})
			.finally(function() {
				hideLoading();
			})
	}
	
}]);