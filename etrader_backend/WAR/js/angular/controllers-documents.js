backendApp.controller('DocumentsCtr', ['$rootScope', '$scope', '$http', 'Pagination', function($rootScope, $scope, $http, Pagination) {

}]);

backendApp.controller('IDsCtr', ['$rootScope', '$scope', '$http', 'Multiselect', 'Pagination', function($rootScope, $scope, $http, Multiselect, Pagination) {
	$scope.ready = false;
	$scope.action = '';
	$scope.filter = {};
	
	$scope.filter.skins = new Multiselect.InstanceClass({aggregateOption: {id: -1, name: 'all'}, mutateCallback: $rootScope.getMsgs});
	$scope.filter.countries = new Multiselect.InstanceClass({aggregateOption: {id: -1, name: 'all'}, mutateCallback: $rootScope.getMsgs, isSmart: true});
	$scope.filter.userId = '';
	$scope.filter.userName = '';

	$scope.USERS_PRIVATE_COMPANY = 12;
	
	$scope.initFiltersOnReady = function(){
		$scope.filter.uploadedBy = new Multiselect.InstanceClass({aggregateOption: {id: -1, name: 'all'}, mutateCallback: $rootScope.getMsgs, mutateOnlyAggregate: true, sortById: true});
		$scope.filter.businessCases = [{id: -1, name: $rootScope.getMsgs('all')}];
		$scope.filter.businessCase = $scope.filter.businessCases[0];
		$scope.filter.ksStatuses = [{id: -1, name: $rootScope.getMsgs('all')}];
		$scope.filter.ksStatus = $scope.filter.ksStatuses[0];
		$scope.filter.classUsers = [{id: -1, name: $rootScope.getMsgs('all')}];
		$scope.filter.classUser = $scope.filter.classUsers[0];
	}
	
	$scope.changePage = function(page){
		$scope.getFiles(page);
	}
	$scope.resultsPerPage = 20;
	$scope.pagination = new Pagination.InstanceClass({callback: $scope.changePage, hideGroupNavigation: true});
	
	$scope.results = [];
	
	//Init the controller
	var initDependencies = {name: 'initList', dependencies: {getIdsFileScreenSettings: false}};
	$scope.initScreenCtr($scope, initDependencies, function(){$scope.getIdsFileScreenSettings('initList');});
	
	$scope.checkAction = function(){
		//
	}
	
	$scope.getIdsFileScreenSettings = function(dependencyName){
		var MethodRequest = {};
		$http.post(settings.backendJsonLink + 'IdsFilesServices/getIdsFileScreenSettings', MethodRequest)
			.then(function(data) {
				$rootScope.resetGlobalErrorMsg();
				for(var i = 0; i < data.permissionsList.length; i++){
					$rootScope.permissions.allow(data.permissionsList[i]);
				}
				if(data.screenFilters){
					var systemWriters = [{id: '-1', name: $rootScope.getMsgs('backend').toUpperCase()}];
					for(key in data.screenFilters.systemWriters){
						if(data.screenFilters.systemWriters.hasOwnProperty(key)){
							systemWriters.push({id: key, name: data.screenFilters.systemWriters[key], checked: true});
						}
					}
					$scope.filter.uploadedBy.fillOptions(systemWriters);
					$scope.filter.uploadedBy.aggregateOption.checked = false;
					$rootScope.fillFilter({arr: $scope.filter.businessCases, data: data, filter: 'platforms', translate: true});
					$scope.filter.skins.fillOptions(data.screenFilters.skins);
					$scope.filter.countries.fillOptions(data.screenFilters.countries);
					$rootScope.fillFilter({arr: $scope.filter.ksStatuses, data: data, filter: 'ksStatus'});
					$rootScope.fillFilter({arr: $scope.filter.classUsers, data: data, filter: 'classUsers', translate: true});
					for(var i = 0; i < $scope.filter.classUsers.length; i++){
						$scope.filter.classUsers[i].name = $scope.filter.classUsers[i].name.replace(/\\\\/, '\\');
						if($scope.filter.classUsers[i].id == $scope.USERS_PRIVATE_COMPANY){
							$scope.filter.classUser = $scope.filter.classUsers[i];
						}
					}
				}
			})
			.catch(function(data) {
				$rootScope.handleErrors(data, 'getIdsFileScreenSettings');
			})
			.finally(function() {
				$rootScope.markDependencyDone($scope, dependencyName, 'getIdsFileScreenSettings');
			})
	}
	
	$scope.getFiles = function(page){
		if((page = $scope.pagination.validatePage(page)) === false){
			return;
		}
		
		showLoading();
		
		var methodRequest = {};
		methodRequest.page = page;
		methodRequest.rowPerPage = $scope.resultsPerPage;
		
		$scope.filter.uploadedBy.getCheckedIds().length > 0 ? methodRequest.uploadedBy = $scope.filter.uploadedBy.getCheckedIds() : '';
		methodRequest.uploadedByBackend = 0;
		if(methodRequest.uploadedBy){
			for(var i = 0; i < methodRequest.uploadedBy.length; i++){
				if(methodRequest.uploadedBy[i] == -1){
					methodRequest.uploadedByBackend = 1;
				}
			}
		}
		$scope.filter.businessCase.id >= 0 ? methodRequest.businessCase = $scope.filter.businessCase.id : '';
		$scope.filter.skins.getCheckedIds().length > 0 ? methodRequest.skins = $scope.filter.skins.getCheckedIds() : '';
		$scope.filter.countries.getCheckedIds().length > 0 ? methodRequest.countries = $scope.filter.countries.getCheckedIds() : '';
		$scope.filter.ksStatus.id >= 0 ? methodRequest.ksStatus = $scope.filter.ksStatus.id : '';
		$scope.filter.userId != '' ? methodRequest.userId = $scope.filter.userId : '';
		$scope.filter.userName != '' ? methodRequest.userName = $scope.filter.userName : '';
		$scope.filter.classUser.id >= 0 ? methodRequest.userClasses = $scope.filter.classUser.id : '';
		
		$http.post(settings.backendJsonLink + 'IdsFilesServices/getFiles', methodRequest)
			.then(function(data) {
				$rootScope.resetGlobalErrorMsg();
				$scope.results = data.idsFiles;
				var totalCount = 0;
				if(data.idsFiles.length > 0){
					totalCount = data.idsFiles[0].totalCount;
				}
				$scope.pagination.setPages(page, totalCount, $scope.resultsPerPage);
			})
			.catch(function(data) {
				$rootScope.handleErrors(data, 'getFiles')
			})
			.finally(function() {
				hideLoading();
			})
	}
}]);

backendApp.controller('DocumentsFilesCtr', ['$rootScope', '$scope', '$http', '$uibModal', 'SingleSelect', 'Multiselect', 'Pagination', 'utilsService', function($rootScope, $scope, $http, $uibModal, SingleSelect, Multiselect, Pagination, utilsService) {
	$scope.USERS_PRIVATE_COMPANY = 12;
	$scope.PENDING_WITHDRAWAL_OPTION_NO = 0;
	$scope.COMPLIANCE_DOCUMENTS_2 = 2;
	$scope.REGULATION_APPROVED_NO = 0;
	$scope.DAYS_LEFT_1_3 = '1-3';
	
	$scope.SORT_BY_DAYS_LEFT = 1;
	$scope.SORT_BY_CALL_ATTEMPTS = 2;
	
	$scope.ready = false;
	$scope.action = '';
	$scope.filter = {};
	$scope.filter.startDate = 0;
	$scope.filter.endDate = 0;
	
	$scope.filter.displayAllWithOpenWithdrawals = false;
	$scope.filter.sortAscDesc = 1;
	$scope.filter.sortedBy = -1;
	
	/*$scope.filter.docTypeFilter = [];
	$scope.filter.documentsType = null;*/
	$scope.filter.skinBusinessCases = new Multiselect.InstanceClass({aggregateOption: {id: -1, name: 'all'}, mutateCallback: $rootScope.getMsgs, isSmart: false});
	$scope.filter.skins = new Multiselect.InstanceClass({aggregateOption: {id: -1, name: 'all'}, mutateCallback: $rootScope.getMsgs, isSmart: true});
	$scope.filter.countries = new Multiselect.InstanceClass({aggregateOption: {id: -1, name: 'all'}, mutateCallback: $rootScope.getMsgs, isSmart: true, iconCallback: function(option){return settings.jsonImagegLink + 'images/flags/' + option.id + '.png';}});
	$scope.filter.resolutionStatus1 = new Multiselect.InstanceClass({aggregateOption: {id: -1, name: 'all'}, mutateCallback: $rootScope.getMsgs, isSmart: true, mutateOnlyAggregate: true});
	$scope.filter.resolutionStatus2 = new Multiselect.InstanceClass({aggregateOption: {id: -1, name: 'all'}, mutateCallback: $rootScope.getMsgs, isSmart: true, mutateOnlyAggregate: true});
	
	$scope.initFiltersOnReady = function(){
		$scope.filter.classUsers = new SingleSelect.InstanceClass({aggregateOption: {id: -1, name: 'all'}, mutateCallback: $rootScope.getMsgs, mutateOnlyAggregate: true});
		
		$scope.filter.complianceApproved = new SingleSelect.InstanceClass({aggregateOption: {id: -1, name: 'all'}, mutateCallback: $rootScope.getMsgs, mutateOnlyAggregate: true});
		$scope.filter.complianceDocUploaded = new SingleSelect.InstanceClass({aggregateOption: {id: -1, name: 'all'}, mutateCallback: $rootScope.getMsgs, mutateOnlyAggregate: true, noSort: true});
		
		$scope.filter.pendingWithdrawal = new SingleSelect.InstanceClass({aggregateOption: {id: -1, name: 'all'}, mutateCallback: $rootScope.getMsgs, mutateOnlyAggregate: true});
		$scope.filter.userSuspended = new SingleSelect.InstanceClass({aggregateOption: {id: -1, name: 'all'}, mutateCallback: $rootScope.getMsgs, mutateOnlyAggregate: true});
		$scope.filter.userActive = new SingleSelect.InstanceClass({aggregateOption: {id: -1, name: 'all'}, mutateCallback: $rootScope.getMsgs, mutateOnlyAggregate: true});
		$scope.filter.daysLeft = new Multiselect.InstanceClass({aggregateOption: {id: -1, name: 'all'}, mutateCallback: $rootScope.getMsgs, mutateOnlyAggregate: true, noSort: true});
		$scope.filter.totalDeposit = new SingleSelect.InstanceClass({aggregateOption: {id: -1, name: 'all'}, mutateCallback: $rootScope.getMsgs, mutateOnlyAggregate: true, noSort: true});
		
		$scope.filter.docUploaded = new SingleSelect.InstanceClass({aggregateOption: {id: -1, name: 'all'}, mutateCallback: $rootScope.getMsgs, mutateOnlyAggregate: true});
		$scope.filter.callAttempts = new SingleSelect.InstanceClass({aggregateOption: {id: -1, name: 'all'}, mutateCallback: $rootScope.getMsgs, mutateOnlyAggregate: true});
		
		// $scope.filter.documentsType = new SingleSelect.InstanceClass({aggregateOption: {id: -1, name: 'all'}, mutateCallback: $rootScope.getMsgs, mutateOnlyAggregate: true});
		$scope.filter.docType1 = new SingleSelect.InstanceClass({aggregateOption: {id: -1, name: 'all'}, mutateCallback: $rootScope.getMsgs, mutateOnlyAggregate: true});
		$scope.filter.docType2 = new SingleSelect.InstanceClass({aggregateOption: {id: -1, name: 'all'}, mutateCallback: $rootScope.getMsgs, mutateOnlyAggregate: true});
		
		$scope.filter.expireDoc = new SingleSelect.InstanceClass({aggregateOption: {id: -1, name: 'all'}, mutateCallback: $rootScope.getMsgs, mutateOnlyAggregate: true});
	}
	
	$scope.filter.resultsPerPage = new SingleSelect.InstanceClass({sortById: true});
	$scope.filter.resultsPerPage.fillOptions(utilsService.resultsPerPage);
	$scope.filter.resultsPerPage.setModel($scope.filter.resultsPerPage.getOptionById(15));
	
	$scope.filter.openDates = {
        startDate: false,
        endDate: false
    };
	
	$scope.dateOptions = {
        showWeeks: false,
        startingDay: 0,
		defaultTime: '00:00:00'
    };
	
	$scope.openCalendar = function(e, date) {
		$scope.filter.openDates[date] = true;
		if (e.target.tagName.toLowerCase() == 'input') {
			$timeout(function() {
				e.target.focus();
			}, 100);
		}
    };
	
	$scope.initDates = function(){
		$scope.currentDate = new Date();
		
		$scope.hoursOffset = $scope.currentDate.getTimezoneOffset()/60 + $scope.writer.utcOffsetHours;
		$scope.currentDate.setHours($scope.currentDate.getHours() + $scope.hoursOffset);

		var startDate = new Date();
		startDate.setMilliseconds(0);
		startDate.setSeconds(0);
		startDate.setMinutes(0);
		startDate.setHours(0);
		startDate.setDate(5);
		startDate.setMonth(11);
		startDate.setFullYear(2016);
		$scope.defaultStartDate = startDate;

		$scope.filter.startDate = $scope.defaultStartDate;
		$scope.filter.endDate = $scope.currentDate;
		// if($scope.filter.endDate.getTime() - $scope.filter.startDate.getTime() > ($scope.filter.dateRangeDays - 1)*24*60*60*1000){
			// $scope.filter.startDate.setTime($scope.filter.endDate.getTime() - ($scope.filter.dateRangeDays - 1)*24*60*60*1000);
		// }
		
		$scope.filter.minDate = new Date();
		$scope.filter.minDate.setTime($scope.filter.endDate.getTime() - 20*365*24*60*60*1000 + $scope.hoursOffset*60*60*1000);
		$scope.filter.maxDate = new Date();
		$scope.filter.maxDate.setTime(new Date().getTime() + $scope.hoursOffset*60*60*1000);
	}
	$scope.initDates();
	
	$scope.changePage = function(page){
		$scope.getDocuments(null, page);
	}
	$scope.resultsPerPage = 20;
	$scope.pagination = new Pagination.InstanceClass({callback: $scope.changePage, hideGroupNavigation: true});
	
	$scope.results = null;

	$scope.currentRequest = null;
	
	//Init the controller
	var initDependencies = {name: 'initList', dependencies: {getDocumentsFilesScreenSettings: false}};
	$scope.initScreenCtr($scope, initDependencies, function(){$scope.setScreen();$scope.getDocumentsFilesScreenSettings('initList');});
	
	$scope.checkAction = function(){
		//
	}
	
	$scope.setScreen = function(){
		if($rootScope.$state.includes("ln.documents.call_center")){
			$scope.screen = 'callCenter';
		}else if($rootScope.$state.includes("ln.documents.back_office")){
			$scope.screen = 'backOffice';
		}
	}
	
	$scope.getDocumentsFilesScreenSettings = function(dependencyName){
		var serviceName = 'UserDocumentsScreensServices/getCallCenterScreenSettings';
		if($scope.screen == 'backOffice'){
			serviceName = 'UserDocumentsScreensServices/getBackOfficeScreenSettings';
		}
		var methodRequest = {};
		$http.post(settings.backendJsonLink + serviceName, methodRequest)
			.then(function(data) {
				$rootScope.resetGlobalErrorMsg();
				for(var i = 0; i < data.permissionsList.length; i++){
					$rootScope.permissions.allow(data.permissionsList[i]);
				}
				if(data.screenFilters){
					/*$rootScope.fillFilter({arr: $scope.filter.docTypeFilter, data: data, filter: 'docTypeFilter'});
					$scope.filter.docTypeFilter.push({id: -1, name: $rootScope.getMsgs('all')});
					$scope.filter.documentsType = -1;*/
					
					var classUsersOptions = [];
					for(key in data.screenFilters.classUsers){
						if(data.screenFilters.classUsers.hasOwnProperty(key)){
							var name = $rootScope.getMsgs(data.screenFilters.classUsers[key]);
							name = name.replace(/\\\\/, '\\');
							classUsersOptions.push({id: key, name: name});
						}
					}
					$scope.filter.classUsers.fillOptions(classUsersOptions);
					for(var i = 0; i < $scope.filter.classUsers.options.length; i++){
						if($scope.filter.classUsers.options[i].id == $scope.USERS_PRIVATE_COMPANY){
							$scope.filter.classUsers.setModel($scope.filter.classUsers.options[i]);
							break;
						}
					}
					
					$scope.filter.skinBusinessCases.fillOptions(data.screenFilters.skinBusinessCases);
					$scope.filter.skins.fillOptions(data.screenFilters.skins);
					$scope.filter.countries.fillOptions(data.screenFilters.countries);
					$scope.filter.resolutionStatus1.fillOptions(data.screenFilters.resolutionFilter);
					$scope.filter.resolutionStatus2.fillOptions(data.screenFilters.resolutionFilter);
					
					$scope.filter.complianceApproved.fillOptions(data.screenFilters.complianceApprovedFilter);
					$scope.filter.complianceApproved.setModel($scope.filter.complianceApproved.getOptionById($scope.REGULATION_APPROVED_NO));
					$scope.filter.complianceDocUploaded.fillOptions(data.screenFilters.complianceDocUploadedFilter);
					$scope.filter.complianceDocUploaded.setModel($scope.filter.complianceDocUploaded.getOptionById($scope.COMPLIANCE_DOCUMENTS_2));
					
					$scope.filter.docType1.fillOptions(data.screenFilters.docTypeFilter);
					$scope.filter.docType1.setModel($scope.filter.docType1.getOptionById(-1));
					$scope.filter.docType2.fillOptions(data.screenFilters.docTypeFilter);
					$scope.filter.docType2.setModel($scope.filter.docType2.getOptionById(-1));
					
					$scope.filter.expireDoc.fillOptions(data.screenFilters.expireDoc);
					$scope.filter.expireDoc.setModel($scope.filter.expireDoc.getOptionById(-1));

					$scope.filter.pendingWithdrawal.fillOptions(data.screenFilters.pendingWithdrawlFilter);
					$scope.filter.userSuspended.fillOptions(data.screenFilters.userSuspendedFilter);
					$scope.filter.userActive.fillOptions(data.screenFilters.userActiveFilter);
					
					$scope.daysLeftFilters = data.daysLeftFilters.daysLeftFilter;
					
					var daysLeftOptions = [];
					for(key in data.daysLeftFilters.daysLeftFilter){
						if(data.daysLeftFilters.daysLeftFilter.hasOwnProperty(key)){
							daysLeftOptions.push({id: key, name: key, values: data.daysLeftFilters.daysLeftFilter[key]});
						}
					}
					$scope.filter.daysLeft.fillOptions(daysLeftOptions);
					//$scope.filter.daysLeft.setModel($scope.filter.daysLeft.getOptionById($scope.DAYS_LEFT_1_3));
					var totalDepositOptions = [];
					totalDepositOptions.push({id: 1, name: '0-€2,000', values: [0, 2000*100]});
					totalDepositOptions.push({id: 2, name: '€2,001+', values: [2000*100 + 1, null]});
					$scope.filter.totalDeposit.fillOptions(totalDepositOptions, true);
					
					
					$scope.filter.docUploaded.fillOptions(data.screenFilters.docUploadedFilter);
					$scope.filter.callAttempts.fillOptions(data.screenFilters.callAttempts);
					
					// $scope.filter.documentsType.fillOptions(data.screenFilters.docTypeFilter);
				}
			})
			.catch(function(data) {
				$rootScope.handleErrors(data, 'getDocumentsFilesScreenSettings')
			})
			.finally(function() {
				$rootScope.markDependencyDone($scope, dependencyName, 'getDocumentsFilesScreenSettings');
			})
	}
	
	$scope.checkDisplayAllWithOpenWithdrawals = function(){
		if($scope.filter.pendingWithdrawal.getId() == -1 || $scope.filter.pendingWithdrawal.getId() == $scope.PENDING_WITHDRAWAL_OPTION_NO){
			$scope.filter.displayAllWithOpenWithdrawals = false;
			return true;
		}
		return false;
	}
	
	$scope.getSkinName = function(skinId){
		return skinMap[skinId] ? $rootScope.getMsgs(skinMap[skinId].skinTranslation) : '';
	}
	
	$scope.getDocuments = function(_form, page){
		var serviceName = 'UserDocumentsScreensServices/getCallCenterDocuments';
		if($scope.screen == 'backOffice'){
			serviceName = 'UserDocumentsScreensServices/getBackOfficeDocuments';
		}
		
		var paging = false;
		if (!isUndefined(_form)) {
			$scope._form = _form;
		} else {
			_form = $scope._form;
			paging = true;
		}
		if((page = $scope.pagination.validatePage(page)) === false){
			return;
		}
		if (_form.$valid || paging) {
			$scope.currentPage = page;
			$rootScope.resetGlobalErrorMsg();
			$scope.showLoading();
			
			var methodRequest = {
				page: page,
				rowsPerPage: $scope.filter.resultsPerPage.getId()
			};
			
			$scope.filter.userId != '' ? methodRequest.userId = $scope.filter.userId : '';
			$scope.filter.userName != '' ? methodRequest.userName = $scope.filter.userName : '';
			
			if ($scope.screen == 'backOffice' || !$scope.filter.displayAllWithOpenWithdrawals) {
				methodRequest.firstEngagementDateFrom = $scope.filter.startDate.getTime();
				methodRequest.firstEngagementDateTo = $rootScope.dateParse({
					date: $scope.filter.endDate,
					endOfDay: true,
					timestamp: true
				});
			}
			$scope.filter.sortedBy >= 0 ? methodRequest.orderById = $scope.filter.sortedBy : '';
			
			if(!$scope.filterCollapsed){
				methodRequest.ascDesc = $scope.filter.sortAscDesc;
				
				$scope.filter.classUsers.getId() >= 0 ? methodRequest.userClasses = $scope.filter.classUsers.getId() : '';
				$scope.filter.skinBusinessCases.getCheckedIds().length > 0 ? methodRequest.businessCases = $scope.filter.skinBusinessCases.getCheckedIds() : '';
				$scope.filter.skins.getCheckedIds().length > 0 ? methodRequest.skins = $scope.filter.skins.getCheckedIds() : '';
				$scope.filter.countries.getCheckedIds().length > 0 ? methodRequest.countries = $scope.filter.countries.getCheckedIds() : '';
				
				$scope.filter.complianceApproved.getId() >= 0 ? methodRequest.complianceApproved = $scope.filter.complianceApproved.getId() : '';
				$scope.filter.complianceDocUploaded.getId() >= 0 ? methodRequest.complianceDocUploaded = $scope.filter.complianceDocUploaded.getId() : '';
				$scope.filter.pendingWithdrawal.getId() >= 0 ? methodRequest.pendingWithdrawal = $scope.filter.pendingWithdrawal.getId() : '';
				$scope.filter.userSuspended.getId() >= 0 ? methodRequest.suspended = $scope.filter.userSuspended.getId() : '';
				$scope.filter.userActive.getId() >= 0 ? methodRequest.active = $scope.filter.userActive.getId() : '';
				
				if ($scope.filter.daysLeft.getCheckedIds().length > 0) {
					methodRequest.daysLeft = [];
					
					$scope.filter.daysLeft.getCheckedIds().map(function(current) {
						methodRequest.daysLeft = methodRequest.daysLeft.concat($scope.daysLeftFilters[current]); 
					})
				}
				
				$scope.filter.docType1.getId() != -1 ? methodRequest.docType1 = $scope.filter.docType1.getId() : null;
				$scope.filter.resolutionStatus1.getCheckedIds().length > 0 ? methodRequest.resolutionStatus1 = $scope.filter.resolutionStatus1.getCheckedIds() : null;
				
				if ($scope.showMoreDoctype) {
					$scope.filter.docType2.getId() != -1 ? methodRequest.docType2 = $scope.filter.docType2.getId() : null;
					$scope.filter.resolutionStatus2.getCheckedIds().length > 0 ? methodRequest.resolutionStatus2 = $scope.filter.resolutionStatus2.getCheckedIds() : null;
				}
				
				if ($scope.filter.totalDeposit.getId() != -1) {
					methodRequest.fromDeposits = $scope.filter.totalDeposit.model.values[0];
					methodRequest.toDeposits = $scope.filter.totalDeposit.model.values[1];
				}
				
				$scope.filter.expireDoc.getId() != -1 ? methodRequest.expireDoc = $scope.filter.expireDoc.getId() : null;
				
				//screen check backOffice
//				if($scope.screen == 'backOffice'){
//					$scope.filter.documentsType.getId() >= 0 ? methodRequest.docType = $scope.filter.documentsType.getId() : '';
//				}
				//screen check callCenter
				if($scope.screen == 'callCenter'){
					$scope.filter.docUploaded.getId() >= 0 ? methodRequest.docUploaded = $scope.filter.docUploaded.getId() : '';
					$scope.filter.callAttempts.getId() >= 0 ? methodRequest.callAttempts = $scope.filter.callAttempts.getId() : '';
					
					if(!$scope.filter.displayAllWithOpenWithdrawals){
						var startDate = new Date();
						startDate.setMilliseconds(0);
						startDate.setSeconds(0);
						startDate.setMinutes(0);
						startDate.setHours(0);
						startDate.setDate(5);
						startDate.setMonth(11);
						startDate.setFullYear(2016);
						methodRequest.ftdDateFrom = $rootScope.dateParse({date: startDate, startOfDay: true, timestamp: true});
					}
				}
			}
			
			$scope.currentRequest = methodRequest;
			
			$http.post(settings.backendJsonLink + serviceName, methodRequest)
				.then(function(data) {
					$rootScope.resetGlobalErrorMsg();
					$scope.results = data.UserDocuments;
					
					var totalCount = 0;
					if($scope.results.length > 0){
						totalCount = $scope.results[0].totalCount;
					}
					$scope.pagination.setPages(page, totalCount, $scope.filter.resultsPerPage.getId());
				})
				.catch(function(data) {
					$rootScope.handleErrors(data, 'getDocuments')
				})
				.finally(function() {
					$scope.hideLoading();
				})
		} else {
			showToast(2, $rootScope.getMsgs('error.general_invalid_form'));
		}
	}
	
	$scope.sortBy = function(colId){
		if($scope.filter.sortedBy == colId){
			$scope.filter.sortAscDesc = (-1)*$scope.filter.sortAscDesc;
		}else{
			$scope.filter.sortedBy = colId;
			$scope.filter.sortAscDesc = 1;
		}
		$scope.getDocuments();
	}
	
	$scope.toggleAdditionalInfo = function(result){
		if(!result.showAdditionalInfo){
			for(var i = 0; i < $scope.results.length; i++){
				$scope.results[i].showAdditionalInfo = false;
			}
			$scope.loadAdditionalInfo(result);
		}else{
			result.showAdditionalInfo = false;
			//Unlock user
			$scope.unlockUser();
		}
	}
	
	$scope.unlockUser = function() {
		var methodRequest = {};
		var unlockService = 'UserDocumentsScreensServices/unlockCallcenter';
		if($scope.screen == 'backOffice'){
			unlockService = 'UserDocumentsScreensServices/unlockBackoffice';
		}
		$http.post(settings.backendJsonLink + unlockService, methodRequest);
	}
	
	$scope.loadAdditionalInfo = function(result){
		$scope.showLoading();
		var methodRequest = {};
		methodRequest.userId = result.userId;
		var serviceName = 'getUserDocumentsUserDataCallCenter';
		if($scope.screen == 'backOffice'){
			serviceName = 'getUserDocumentsUserDataBackOffice';
		}
		$http.post(settings.backendJsonLink + 'UserDocumentsScreensServices/' + serviceName, methodRequest)
			.then(function(data) {
				$rootScope.resetGlobalErrorMsg();
				result._additionalInfo = data;
				if (result._additionalInfo.lastCallTime == 0) {result._additionalInfo.lastCallTime == null;}
				if (result._additionalInfo.lastReachedTime == 0) {result._additionalInfo.lastReachedTime == null;}
				result.showAdditionalInfo = true;
				data.files.map(function(current, index){
					var typeIdArr = [1,2,22];
					if ($.inArray(current.fileType, typeIdArr) > -1 && current.expDate) {
						var expDate = new Date(current.expDate);
						if (!isNaN(expDate.getTime())) {
							var date = new Date();
							date.setHours(23);
							date.setMinutes(59);
							date.setDate(date.getDate() - 1);
							var dateFuture = new Date(date);
							dateFuture.setDate(dateFuture.getDate() + 14);
							
							if (date.getTime() > expDate.getTime()) {
								current.expClass = 'red';
							} else if (dateFuture.getTime() > expDate.getTime()) {
								current.expClass = 'orange';
							}
						}
					}
					return current;
				})
			})
			.catch(function(data) {
				$rootScope.handleErrors(data, 'getDocuments')
			})
			.finally(function() {
				$scope.hideLoading();
			})
	}
	
	$scope.openFilePopup = function(result, file){
		//Clear add data
		$scope.popupFileAdd = null;
		$scope.popupUserId = null;
		
		$scope.popupFileId = file.fileId;
		$scope.popupUserId = result.userId;
		var previewModal = $uibModal.open({
			templateUrl: folderPrefix + 'components/documents-back-office-file-popup.html',
			scope: $scope,
			windowClass: 'documents-back-office-file-popup'
		});
		previewModal.result.then(function () {
			//success
			$scope.loadAdditionalInfo(result);
		}, function () {
			//cancel
			$scope.loadAdditionalInfo(result);
		});
	}
	
	$scope.addFile = function(result){
		//Clear edit data
		$scope.popupFileId = null;
		$scope.popupUserId = null;
		
		$scope.popupFileAdd = true;
		$scope.popupUserId = result.userId;
		var previewModal = $uibModal.open({
			templateUrl: folderPrefix + 'components/documents-back-office-file-popup.html',
			scope: $scope,
			windowClass: 'documents-back-office-file-popup'
		});
		previewModal.result.then(function () {
			//success
			$scope.loadAdditionalInfo(result);
		}, function () {
			//cancel
			$scope.loadAdditionalInfo(result);
		});
	}
	
	$scope.openCustomerInformation = function(userId){
		window.open($rootScope.getUserStripUrl(userId));
	}
	
	$scope.regulationApprove = function(result){
		if(!result.comments){
			showToast(TOAST_TYPES.error, $rootScope.getMsgs('error.mandatory'));
			return false;
		}
		var methodRequest = {};
		methodRequest.userId = result.userId;
		methodRequest.comments = result.comments;
		$http.post(settings.backendJsonLink + 'UserDocumentsScreensServices/regulationApprove', methodRequest)
			.then(function(data) {
				$rootScope.resetGlobalErrorMsg();
				$scope.loadAdditionalInfo(result);
				$scope.closeComment();
				showToast(TOAST_TYPES.info, $rootScope.getMsgs('info.documents-back-office-successfully-approved'));
			})
			.catch(function(data) {
				$rootScope.handleErrors(data, 'getDocuments')
			})
			.finally(function() {
				hideLoading();
			})
	}
	
	$scope.regulationApproveManager = function(result){
		if(!result.comments){
			showToast(TOAST_TYPES.error, $rootScope.getMsgs('error.mandatory'));
			return false;
		}
		var methodRequest = {};
		methodRequest.userId = result.userId;
		methodRequest.comments = result.comments;
		$http.post(settings.backendJsonLink + 'UserDocumentsScreensServices/regulationApproveManager', methodRequest)
			.then(function(data) {
				$rootScope.resetGlobalErrorMsg();
				$scope.loadAdditionalInfo(result);
				$scope.closeComment();
				showToast(TOAST_TYPES.info, $rootScope.getMsgs('info.documents-back-office-successfully-approved'));
			})
			.catch(function(data) {
				$rootScope.handleErrors(data, 'getDocuments')
			})
			.finally(function() {
				hideLoading();
			})
	}
	
	$scope.showMoreDoctype = false;
	$scope.showHideSeconndResSearch = function() {
		$scope.showMoreDoctype = !$scope.showMoreDoctype;
	}

	$scope.commentPopupType = '';
	$scope.commentpopupEl = '';
	$scope.openComment = function($event, type) {
		$scope.commentPopupType = type;
		var el = $($event.currentTarget);
		var elPos = el.position();
		$scope.commentPopupEl = el.parent().find('#commentsDiv');
		
		$scope.commentPopupEl.css({
			left: elPos.left +  (el.width() / 2) - ($scope.commentPopupEl.width() / 2) + 'px',
			top: elPos.top +  (el.height() / 2) + 30 + 'px',
		})
		
		$scope.commentPopupEl.fadeToggle();
	}
	$scope.closeComment = function() {
		$scope.commentPopupEl.fadeToggle();
	}

	$scope.saveComments = function(results) {
		if (isUndefined(results.issueActionComments) || results.issueActionComments == '') {
			showToast(TOAST_TYPES.error, $rootScope.getMsgs('error.mandatory'));
			return false;
		}
		
		var request = {
			userId: results.userId,
			comments: results.issueActionComments
		};
		
		showLoading();
		$http.post(settings.backendJsonLink + 'UserDocumentsScreensServices/saveComments', request)
			.then(function(data) {
				$rootScope.resetGlobalErrorMsg();
				showToast(TOAST_TYPES.info, $rootScope.getMsgs('comment-was-successfully-added'));
				results.issueActionComments = '';
			})
			.catch(function(data) {
				$rootScope.handleErrors(data, 'saveComments')
			})
			.finally(function() {
				hideLoading();
			})
	}
}]);
