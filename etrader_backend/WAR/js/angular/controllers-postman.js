backendApp.controller('PostmanCtr', ['$rootScope', '$scope', '$http', 'utilsService', function($rootScope, $scope, $http, utilsService) {

	$scope.form = {
		url: localStorage.getItem("postwoman-url") || settings.backendJsonLink,
		postJson: localStorage.getItem("postwoman-postJson") || ''
	}
	
	$scope.result = '';
	
	$scope.post = function(_form) {
		$scope.result = "Loading...";
		
		$http.post($scope.form.url, $scope.form.postJson)
			.then(function(data) {
				$scope.result = data;
				window.localStorage.setItem("postwoman-url", $scope.form.url);
				window.localStorage.setItem("postwoman-postJson", $scope.form.postJson);
				console.log(data);
			})
			.catch(function(data) {
				$rootScope.handleErrors(data, 'postman')
			})
			.finally(function() {
				hideLoading();
			});
	};
}]);
