
			var SUBSCR_FLD_KEY							= "key";
			var SUBSCR_FLD_COMMAND						= "command";
			var SUBSCR_FLD_CALLS_AMOUNT					= "callsAmount";
			var SUBSCR_FLD_PUTS_AMOUNT					= "putsAmount";
			var SUBSCR_FLD_CALLS_AMOUNT_SUB_PUTS_AMOUNT	= "callsAmountSubPutsAmount";
			var SUBSCR_FLD_CALLS_AMOUNT_ADD_PUTS_AMOUNT	= "callsAmountAddPutsAmount";
			var SUBSCR_FLD_CALLS						= "calls";
			var SUBSCR_FLD_PUTS							= "puts";
			var SUBSCR_FLD_TIME_LAST_INVEST				= "timeLastInvest";
			var SUBSCR_FLD_MARKET_ID					= "marketId";
			var SUBSCR_FLD_MARKET_GROUP_ID				= "marketGroupId";
			var SUBSCR_FLD_SCHEDULED					= "scheduled";
			var SUBSCR_FLD_OPP_TYPE						= "oppType";
			var SUBSCR_FLD_SHIFT_PARAMETER 				= "shiftParameter";
			var	SUBSCR_FLD_AUTO_SHIFT_PARAMETER 		= "autoShiftParameter";
			var SUBSCR_FLD_IS_DISABLED_TRADER 			= "isDisabledTrader";
			var SUBSCR_FLD_FEED_NAME 					= "feedName";
			var SUBSCR_FLD_IS_SUSPENDED 				= "isSuspended";
			var SUBSCR_FLD_SUSPENDED_MESSAGE 			= "suspendedMessage";
			var SUBSCR_FLD_STATE 						= "state";
			var SUBSCR_FLD_TIME_EST_CLOSING				= "timeEstClosing";
			var SUBSCR_FLD_MAX_EXPOSURE 				= "maxExposure_1";
			var SUBSCR_FLD_DECIMAL_POINT				= "decimalPoint";
			var SUBSCR_FLD_ET_LEVEL						= "etLevel";
			var SUBSCR_FLD_AO_LEVEL						= "aoLevel";
			var SUBSCR_FLD_SHIFT_PARAMETER_AO			= "shiftParameterAO";

			var SUBSCR_FLD_BZ_EVENT						= "BZ_EVENT";
			var SUBSCR_FLD_CALLSSUBPUTS					= "callsSubPuts";
			var SUBSCR_FLD_BZ_BID						= "BZ_BID";
			var SUBSCR_FLD_BZ_OFFER						= "BZ_OFFER";
//			var SUBSCR_FLD_PARAMETER_C					= "parameter_c";
//			var SUBSCR_FLD_PARAMETER_Q					= "parameter_q";
			var SUBSCR_FLD_PARAMETER_SP					= "parameter_sp";
			var SUBSCR_FLD_PARAMETER_T					= "parameter_t";
			var SUBSCR_FLD_PARAMETERS					= "parameters";
			var SUBSCR_FLD_PARAMETER_Z					= "parameter_z";
			var SUBSCR_FLD_PARAMETER_M					= "parameter_m";
			
			// variables generated on update 
			var SUBSCR_FLD_MARKET_NAME							= "marketName";
			var SUBSCR_FLD_SCHEDULED_NAME						= "scheduledName";
			var SUBSCR_FLD_OPP_TYPE_NAME						= "oppTypeName";
			var SUBSCR_FLD_STATE_CHANGED 						= "stateChanged";
			var FORMATED_TIME_EST_CLOSING						= "formatedTimeEstClosing";
			var FORMATED_CALLS_AMOUNT							= "formatedCallsAmount";
			var FORMATED_PUTS_AMOUNT							= "formatedPutsAmount";
			var FORMATED_CALLS_AMOUNT_SUB_PUTS_AMOUNT			= "formatedCallsAmountSubPutsAmount";
			var FORMATED_CALLS_AMOUNT_ADD_PUTS_AMOUNT   		= "formatedcallsamountaddputsamount";
			var FORMATED_MAX_EXPOSURE							= "formatedMaxExposure_1";
			var FORMATED_CALLS_CONTRACTS						= "formatedCallsContracts";
			var FORMATED_PUTS_CONTRACTS							= "formatedPutsContracts";
			var FORMATED_CALLS_SUB_PUTS_CONTRACTS 				= "formatedCallsSubPutsContracts";

			var OPPORTUNITY_STATE_OPEN 					= 1;
			var OPPORTUNITY_WAITING_TO_EXPIRE 			= 2;
			var OPPORTUNITY_SUSPENDED					= 3;
			var OPPORTUNITY_DISABLE						= 4;

			var OPPORTUNITY_TYPE_DEFAULT 				= 1;
			var OPPORTUNITY_TYPE_BINARY_0_100_ABOVE		= 4;
			var OPPORTUNITY_TYPE_BINARY_0_100_BELOW		= 5;

			//constants for the row for each opp every td will have constants
			var TD_OPP_ID 									= 1;
			var TD_MARKET_NAME  							= 2;
			var TD_BZ_EVENT									= 3;
			var TD_ET_LEVEL  								= 4;
			var TD_FORMATED_CALLS_CONTRACTS 				= 5;
			var TD_FORMATED_PUTS_CONTRACTS 					= 6;
			var TD_FORMATED_CALLS_SUB_PUTS_CONTRACTS		= 7;
			var TD_FORMATED_CALLS_AMOUNT 					= 8;
			var TD_FORMATED_PUTS_AMOUNT 					= 9;
			var TD_SUBSCR_FLD_BZ_BID 						= 10;
			var TD_SUBSCR_FLD_BZ_OFFER 						= 11;
			var TD_FORMATED_TIME_EST_CLOSING 				= 12;
			var TD_DISABLE_BUTTON 							= 13;
			var TD_SUSPEND_BUTTON 							= 14;
			var TD_MARKET_ID 								= 15;
			var TD_SUBSCR_FLD_MARKET_GROUP_ID 				= 16;
			var TD_SCHEDULED 								= 17;
			var TD_OPP_TYPE_ID 								= 18;
			var TD_SUBSCR_FLD_FEED_NAME 					= 19;
			var TD_SUBSCR_FLD_SUSPENDED_MESSAGE 			= 20;
			var TD_SUBSCR_FLD_DECIMAL_POINT 				= 21;
			var TD_SETTLE_BUTTON 							= 22;
			var TD_SUBSCR_FLD_MAX_EXPOSURE 					= 23;
			var TD_PARAMETERS								= 24;
			var TD_PARAMETERS_C								= 25;
			var TD_PARAMETERS_Q								= 26;
			var TD_PARAMETERS_T								= 27;
			var TD_PARAMETERS_M								= 28;

			var classAtr = navigator.userAgent.toLowerCase().indexOf("msie") != -1 ? 'className' : 'class';

			// last field and direction (asc or desc)
			var last_sort_field = null;
			var last_sort_direction = null;

			//for highlight to remmber what was the last color when mouseout
			var last_highlight_row_bgColor = null;
			var last_highlight_row_oppId = null;

			//if we show alert mark it as true so we will not popup again only after C changed
			var exposureAlertArry = new Array();

	function updateItem(key,updateInfo,domNode) {
try{

	//add filed to change display name
	if (updateInfo.getChangedFieldValue(SUBSCR_FLD_MARKET_ID)!=null) {
		for (var i=0; i<marketsDiaplayName.length; i++) {
			if (marketsDiaplayName[i][0] == updateInfo.getChangedFieldValue(SUBSCR_FLD_MARKET_ID)) {
				updateInfo.setCellValue(SUBSCR_FLD_MARKET_NAME, marketsDiaplayName[i][1]);
				break;
			}
		}
	}

//	if (updateInfo.getChangedFieldValue(SUBSCR_FLD_SCHEDULED)!=null) {
//		updateInfo.setCellValue(SUBSCR_FLD_SCHEDULED_NAME , scheduledTxt[new Number(updateInfo.getChangedFieldValue(SUBSCR_FLD_SCHEDULED)) - 1]);
//	}

	//format the est_close_time according to client windows offset
	if (updateInfo.getChangedFieldValue(SUBSCR_FLD_TIME_EST_CLOSING)!=null) {
		var est_date = new Date(updateInfo.getChangedFieldValue(SUBSCR_FLD_TIME_EST_CLOSING));
		updateInfo.setCellValue(FORMATED_TIME_EST_CLOSING, formatDate(est_date));
	}

	if (updateInfo.getChangedFieldValue(SUBSCR_FLD_CALLS)!=null) {
		updateInfo.setCellValue(FORMATED_CALLS_CONTRACTS, addCommas(updateInfo.getChangedFieldValue(SUBSCR_FLD_CALLS)));
	}

	if (updateInfo.getChangedFieldValue(SUBSCR_FLD_PUTS)!=null) {
		updateInfo.setCellValue(FORMATED_PUTS_CONTRACTS, addCommas(updateInfo.getChangedFieldValue(SUBSCR_FLD_PUTS)));
	}

	if (updateInfo.getChangedFieldValue(SUBSCR_FLD_CALLSSUBPUTS)!=null) {
		updateInfo.setCellValue(FORMATED_CALLS_SUB_PUTS_CONTRACTS, addCommas(updateInfo.getChangedFieldValue(SUBSCR_FLD_CALLSSUBPUTS)));
	}

	if (updateInfo.getChangedFieldValue(SUBSCR_FLD_CALLS_AMOUNT)!=null) {
		updateInfo.setCellValue(FORMATED_CALLS_AMOUNT , "$" + addCommas(updateInfo.getChangedFieldValue(SUBSCR_FLD_CALLS_AMOUNT)));
	}
	
	if (updateInfo.getChangedFieldValue(SUBSCR_FLD_PUTS_AMOUNT)!=null) {
		updateInfo.setCellValue(FORMATED_PUTS_AMOUNT, "$" + addCommas(updateInfo.getChangedFieldValue(SUBSCR_FLD_PUTS_AMOUNT)));
	}

	if (updateInfo.getChangedFieldValue(SUBSCR_FLD_MAX_EXPOSURE)!=null) {
		updateInfo.setCellValue(FORMATED_MAX_EXPOSURE, "$" + addCommas(updateInfo.getChangedFieldValue(SUBSCR_FLD_MAX_EXPOSURE)));
	}
	

	if (updateInfo.getChangedFieldValue(SUBSCR_FLD_STATE) != null) {
			state = updateInfo.getChangedFieldValue(SUBSCR_FLD_STATE);
			if (state == OPPORTUNITY_DISABLE) {
				domNode.setAttribute('bgColor', '#9D9D9D');
				domNode.setAttribute("style", "row_disable");
			} else if (state == OPPORTUNITY_SUSPENDED) {
				domNode.setAttribute('bgColor', '#FFD700');
				domNode.setAttribute("style", "row_suspend");
			} else if (state == OPPORTUNITY_WAITING_TO_EXPIRE) {
				domNode.setAttribute('bgColor', '#11b000');
				domNode.setAttribute("style", "row_waiting");
			} else {
				domNode.setAttribute('bgColor', '#FFFFFF');
				domNode.setAttribute("style", "row_other");
			}
	
			//if this row is highlight now, then change the last bg color to the new one
			if (last_highlight_row_oppId == updateInfo.getChangedFieldValue(SUBSCR_FLD_KEY)) {
				last_highlight_row_bgColor = domNode.getAttribute('bgColor');
			}
	}
	if (updateInfo.getChangedFieldValue("oppOnlySuspended") != null) {
		var suspended = updateInfo.getChangedFieldValue("oppOnlySuspended");
		if (suspended == 1) {
			domNode.setAttribute('bgColor', '#7EC0EE');
			domNode.setAttribute("style", "row_suspend");
		}
	}

		var td = null;
		var button = null;
		var delta;
		if(updateInfo.getChangedFieldValue(SUBSCR_FLD_CALLSSUBPUTS)!=null) {
			delta = new Number(updateInfo.getChangedFieldValue(SUBSCR_FLD_CALLSSUBPUTS));
		}
//		var z;
//		if(updateInfo.getChangedFieldValue(SUBSCR_FLD_PARAMETER_Z)!=null) {
//			z = new Number(updateInfo.getChangedFieldValue(SUBSCR_FLD_PARAMETER_Z));
//		}
		var expusre;
		if(updateInfo.getChangedFieldValue(SUBSCR_FLD_MAX_EXPOSURE)!=null) {
			expusre = new Number(updateInfo.getChangedFieldValue(SUBSCR_FLD_MAX_EXPOSURE));
		}

		td = getChildOfType(domNode, "TD", TD_PARAMETERS_C);
		button = getChildOfType(td, "INPUT", 1);
		if (Math.abs(expusre) * 0.75 <= Math.abs(delta)/* / z*/) {
			button.style.background = '#FF0000';
			if (exposureAlertArry[updateInfo.getChangedFieldValue(SUBSCR_FLD_KEY)] == null || !exposureAlertArry[updateInfo.getChangedFieldValue(SUBSCR_FLD_KEY)]) {
				exposureAlertArry[updateInfo.getChangedFieldValue(SUBSCR_FLD_KEY)] = true;
				openActionWindowChangeParamOnUpdate(button, 'c', updateInfo.getChangedFieldValue(SUBSCR_FLD_KEY), updateInfo.getChangedFieldValue(SUBSCR_FLD_MARKET_NAME), updateInfo.getChangedFieldValue(SUBSCR_FLD_PARAMETERS), updateInfo.getChangedFieldValue(SUBSCR_FLD_MARKET_ID));
			}
		} else {
			button.style.background = '#F0F0F0';
			exposureAlertArry[updateInfo.getChangedFieldValue(SUBSCR_FLD_KEY)] = false;
		}

		var isDisabled = updateInfo.getChangedFieldValue(SUBSCR_FLD_IS_DISABLED_TRADER);
		if (isDisabled != null) {
			updateInfo.setCellValue(SUBSCR_FLD_IS_DISABLED_TRADER, isDisabled);
		}
		var isSuspended = updateInfo.getChangedFieldValue(SUBSCR_FLD_IS_SUSPENDED);
		if (isSuspended != null) {
			updateInfo.setCellValue(SUBSCR_FLD_IS_SUSPENDED, isSuspended);
		}
//		var cParam = updateInfo.getChangedFieldValue(SUBSCR_FLD_PARAMETER_C);
//		if ( cParam != null) {
//			updateInfo.setCellValue(SUBSCR_FLD_PARAMETER_C, cParam);
//		}
//		var qParam = updateInfo.getChangedFieldValue(SUBSCR_FLD_PARAMETER_Q);
//		if (qParam != null) {
//			updateInfo.setCellValue(SUBSCR_FLD_PARAMETER_Q, qParam);
//		}
//		var tParam = updateInfo.getChangedFieldValue(SUBSCR_FLD_PARAMETER_T);
//		if (tParam != null) {
//			updateInfo.setCellValue(SUBSCR_FLD_PARAMETER_T, tParam);
//		}
//		var mParam = updateInfo.getChangedFieldValue(SUBSCR_FLD_PARAMETER_M);
//		if (mParam!= null) {
//			updateInfo.setCellValue(SUBSCR_FLD_PARAMETER_M, mParam);
//		}

		if (updateInfo.getChangedFieldValue(SUBSCR_FLD_OPP_TYPE) != null) {
			td = getChildOfType(domNode, "TD", TD_BZ_EVENT);
			var img = getChildOfType(td, "IMG", 1);
			//set the value of the max exposure button (the opp max exposure value);
			if (updateInfo.getChangedFieldValue(SUBSCR_FLD_OPP_TYPE) == OPPORTUNITY_TYPE_BINARY_0_100_ABOVE) {
				img.src = img.src.replace('down', 'up');
			}
		}
		var bzBid = updateInfo.getChangedFieldValue(SUBSCR_FLD_BZ_BID);
		if (bzBid != null) {
			updateInfo.setCellValue(SUBSCR_FLD_BZ_BID, bzBid);
		}

		var bzOffer = updateInfo.getChangedFieldValue(SUBSCR_FLD_BZ_OFFER)
		if (bzOffer!= null) {
			updateInfo.setCellValue(SUBSCR_FLD_BZ_OFFER, bzOffer);
		}
}catch(e){
	console.log(e);
}
	}

/**
	when changeing filters update the table according to the filters (which row to show and which to hide)
**/
function updateMarketsSelection() {
	var tbl = document.getElementById("oppsTable");
	var tbody = getChildOfType(tbl, "TBODY", 1);

	var i = 3;
	do {
		var row = getChildOfType(tbody, "TR", i);
		if (null != row) {

			var market_id = "0," + getOppValue(row, TD_MARKET_ID);
			var row_group_id = getOppValue(row, TD_SUBSCR_FLD_MARKET_GROUP_ID);
			var row_scheduled = getOppValue(row, TD_SCHEDULED);
			var row_oppType = getOppValue(row, TD_OPP_TYPE_ID);

			filters(row, market_id, row_group_id, row_scheduled, row_oppType);

		} else {
			break;
		}
		i = i + 1;
	} while (true);
}


//update table according to market group selection and then update market select list
function updateMarketGroupSelection(selectList) {
	updateMarketsSelection();
	var marketGroupIds = getSelectedItems(selectList);
	if (marketGroupIds.indexOf(',') > -1) {
		marketGroupIds = marketGroupIds.substring(0, marketGroupIds.length - 1);
	} else {
		marketGroupIds = "0";
	}
	marketGroupChangeAjaxCall(marketGroupIds);
}

function getChildOfType(node, childType, order) {
	var pos = 0;
	for (var i = 0; i < node.childNodes.length; i++) {
		if (node.childNodes[i].nodeName == childType) {
			pos++;
			if (pos == order) {
				return node.childNodes[i];
			}
		}
	}
}

function getOppValue(opp, field) {
	var td = getChildOfType(opp, "TD", field);
	var div = getChildOfType(td, "DIV", 1);
	return div.innerHTML;
}

function getOppValueShift(opp, field, divNum) {
	var td = getChildOfType(opp, "TD", field);
	var div = getChildOfType(td, "DIV", divNum);
	div = getChildOfType(div, "DIV", 1);
	return div.innerHTML;
}

function openActionWindow(button, link) {
	var row = button.parentNode.parentNode;
	var row_oppId = getOppValue(row, TD_OPP_ID);
	var row_suspendMsg = getOppValue(row, TD_SUBSCR_FLD_SUSPENDED_MESSAGE);
	var row_feedName = encodeURIComponent(getOppValue(row, TD_SUBSCR_FLD_FEED_NAME));
	var row_markeName = escape(getOppValue(row, TD_MARKET_NAME).replace('&amp;', '&'));
	var actionName = button.getAttribute('value');
	var popupWin = window.open(context_path + "/jsp/trader/traderToolAction" + link + ".jsf?action=" + actionName + "&oppId=" + row_oppId + "&suspendMsg=" + row_suspendMsg + "&feedName=" + row_feedName + "&marketName=" + row_markeName ,'actionWindow' + row_oppId, 'width=400,height=420,left=' + document.body.clientWidth + ',top=0');
	//window.top.location.href = context_path + "/jsp/trader/traderToolAction" + link + ".jsf?action=" + button.getAttribute('value') + "&oppId=" + row_oppId + "&suspendMsg=" + row_suspendMsg + "&spread=" + row_spread + "&feedName=" + row_feedName + "&marketName=" + row_markeName + "&maxExposure=" + row_maxExposure
   if (window.focus) {
   		popupWin.focus();
   	}
}

function openActionWindowSettle(button, link) {
	var row = button.parentNode.parentNode;
	var row_oppId = getOppValue(row, TD_OPP_ID);
	var row_decimalPoint = getOppValue(row, TD_SUBSCR_FLD_DECIMAL_POINT);
	var row_markeName = escape(getOppValue(row, TD_MARKET_NAME).replace('&amp;', '&'));
	var popupWin = window.open(context_path + "/jsp/trader/traderToolAction" + link + ".jsf?action=" + button.getAttribute('value') + "&oppId=" + row_oppId + "&decimalPoint=" + row_decimalPoint + "&marketName=" + row_markeName ,'actionWindow','width=400,height=240');
    if (window.focus) {
    	popupWin.focus();
    }
}

function openActionWindowChangeParam(button, action) {
	var row = button.parentNode.parentNode;
	var row_oppId = getOppValue(row, TD_OPP_ID);
	var row_parameters = getOppValue(row, TD_PARAMETERS);
	var row_markeName = escape(getOppValue(row, TD_MARKET_NAME).replace('&amp;', '&'));
	var row_markeId = getOppValue(row, TD_MARKET_ID);
	var popupWin = window.open("http://" + window.location.host + context_path + "/jsp/trader/traderToolActionChangeParam.jsf?action=" + action + "&paramValue=" + button.getAttribute('value') + "&oppId=" + row_oppId + "&marketName=" + row_markeName + "&params=" + row_parameters + "&marketId=" + row_markeId,'actionWindow','width=400,height=280');
    if (window.focus) {
    	popupWin.focus();
    }
}

function openActionWindowChangeParamOnUpdate(button, action, oppId, marketName, params, marketId) {
	var row = button.parentNode.parentNode;
	var row_oppId = oppId;
	var row_parameters = params;
	var row_markeName = escape(marketName.replace('&amp;', '&'));
	var popupWin = window.open("http://" + window.location.host + context_path + "/jsp/trader/traderToolActionChangeParam.jsf?action=" + action + "&paramValue=" + button.getAttribute('value') + "&oppId=" + row_oppId + "&marketName=" + row_markeName + "&params=" + row_parameters + "&marketId=" + marketId,'actionWindow','width=400,height=280');
    if (window.focus) {
    	popupWin.focus();
    }
}


/**
* sort the table according to the culomn clicked
*@field - a field descriptor object for the field to be used as sort field, or null to disable sorting. A String field name or a Number representing a field position can also be used directly, instead of a field descriptor object.
*@direction - [Optional] true or false to perform descending or ascending sort. This parameter is optional; if missing or null, then ascending sort is performed.
*@numeric - [Optional] true or false to perform numeric or alphabetical sort. This parameter is optional; if missing or null, then alphabetical sort is performed.
**/
function sort(field, direction, numeric) {
	try{
		if (field == last_sort_field) {
			if (last_sort_direction == true) {
				last_sort_direction = false;
			} else {
				last_sort_direction = true;
			}
		} else {
			last_sort_field = field;
			last_sort_direction = direction;
		}
		dynaGrid.setSort(field, last_sort_direction, numeric);
	}catch(e){
		console.log(e);
	}
}

// check if the row should be visible according to filters
function filters(row_row, row_market_id, row_group_id, row_scheduled, row_oppType) {
	var mSel = document.getElementById("select_market");
	var market = mSel.options[mSel.selectedIndex].value;

	var sSel = document.getElementById("select_scheduled");
	var i;
	var scheduled = getSelectedItems(sSel);

	var gSel = document.getElementById("select_group");
	var group_id = getSelectedItems(gSel);

	var tSel = document.getElementById("select_oppType");
	var oppType_id = tSel.options[tSel.selectedIndex].value;
	//	document.getElementById("log").innerHTML += oppType_id + ", " + row_oppType + "<br/>";
	var row = row_row;
	if (((market == row_market_id) || (market == '0,0')) && ((scheduled.indexOf(row_scheduled) > -1) || (scheduled.indexOf('0') > -1)) && ((group_id.indexOf(row_group_id) > -1) || (group_id.indexOf('0') > -1)) && ((oppType_id == row_oppType) || (oppType_id == '0'))) {
		row.style.display = navigator.userAgent.toLowerCase().indexOf("msie") != -1 ? 'block' : 'table-row';
	} else {
		row.style.display = 'none';
	}
}


function adjustFromUTCToLocal(toAdj) {
	var adjM = toAdj.getMinutes() - new Date().getTimezoneOffset();
	var adjH = 0;
	if (adjM >= 0) {
		adjH = Math.floor(adjM / 60);
		adjM = adjM % 60;
	} else {
		adjH = Math.floor(Math.abs(adjM) / 60);
		if (Math.abs(adjM) % 60 != 0) {
			adjH += 1;
			adjM = 60 - Math.abs(adjM) % 60;
		} else {
			adjM = Math.abs(adjM) % 60;
		}
		adjH = -adjH;
	}
	toAdj.setHours(toAdj.getHours() + adjH);
	toAdj.setMinutes(adjM);
	return toAdj;
}

function formatDate(est_date) {
	est_date = adjustFromUTCToLocal(est_date);

	var currdate = new Date();
	var min = est_date.getMinutes();
	if (est_date.getDate() == currdate.getDate() && est_date.getMonth() == currdate.getMonth() && est_date.getFullYear() == currdate.getFullYear()) {
		if (new Number(est_date.getMinutes()) < new Number("10")) {
			min = "0" + est_date.getMinutes();
		}
		est_date = est_date.getHours() + ":" + min + " " + bundle_msg_today;
	} else {
		if (new Number(est_date.getMinutes()) < new Number("10")) {
			min = "0" + est_date.getMinutes();
		}
		var year = new String(est_date.getFullYear());
		est_date = est_date.getHours() + ":" + min + ", " + est_date.getDate() + "." + (est_date.getMonth()+1) + "." + year.substring(2);
	}
	return est_date;
}

function changeRowStyle(row, styleClass) {
	var td = null;
	for (var i = 1; i < 15; i++) {
		td = getChildOfType(row, "TD", i);
		td.setAttribute(classAtr, styleClass);
	}
}

// add the commas and round the number
//parm amount: the amount
function addCommas(amount) {
	amount += '';
	x = amount.split('.');
	x1 = x[0];
	x2 = x.length > 1 ? '.' + x[1] : '';
	var rgx = /(\d+)(\d{3})/;
	while (rgx.test(x1)) {
		x1 = x1.replace(rgx, '$1' + ',' + '$2');
	}
	return x1 + x2;
}

/** highlight Row when mouseover and back to real color on mouse out
*@parm row the row to hightlight
*@parm direction over if mouse over out if mouse out
**/
function highlightRow(rowP, dirction) {
	//for IE Bug
	var row = rowP
	var colorStyle = "#EE82EE";
	if (dirction == "over") {
		last_highlight_row_bgColor = row.getAttribute('bgColor');
		last_highlight_row_oppId = getOppValue(row, 1);
	} else {
		colorStyle = last_highlight_row_bgColor;
	}
	row.setAttribute('bgColor', colorStyle);
}


//ajax requset
function AJAXInteraction(url, callback) {

   	var req = init();
    req.onreadystatechange = processRequest;

    function init() {
   		if (window.XMLHttpRequest) {
        	return new XMLHttpRequest();
        } else if (window.ActiveXObject) {
        	return new ActiveXObject("Microsoft.XMLHTTP");
        }
    }

    function processRequest () {
      // readyState of 4 signifies request is complete
      if (req.readyState == 4) {
	// status of 200 signifies sucessful HTTP call
        if (req.status == 200) {
          if (callback) {
          	callback(req.responseXML);
          }
        }
      }
    }

    this.doGet = function() {
      // make a HTTP GET request to the URL asynchronously
      req.open("GET", url, true);
      req.send(null);
   	}
}

//build new select list with values from ajax callback
function buildSelect(nodeName, selectId, responseXML) {
	var currArr = responseXML.documentElement; //<marketsSelect>
	var markets = currArr.getElementsByTagName("market");
	var len = markets.length;
	var currSel = document.getElementById(selectId);
	var marketName = null;

	//safty check
	if (!currSel) {
		return null;
	}
	currSel.options.length = 0;
	currSel.options.add(new Option("All","0,0"));
	for (var i=0; i<len; i++) {
    	marketName = markets[i].getElementsByTagName("name")[0].firstChild.data;
    	marketName = unescape(marketName.replace(/\+/g," "));
	    currSel.options.add(new Option(marketName, markets[i].getElementsByTagName("value")[0].firstChild.data));
	}
	currSel.selectedIndex = 0;

}

function marketGroupChangeAjaxCall(MarketGroupId) {
    var url = "ajax.jsf?marketGroupId=" + encodeURIComponent(MarketGroupId);
    var ajax = new AJAXInteraction(url, changeMarketsSelect);
    ajax.doGet();
}

function changeMarketsSelect(responseXML) {
	buildSelect("marketsSelect", "select_market", responseXML);
}

function getSelectedItems(list) {
	var temp = "";
	for (var i = 0; i < list.length; i++) {
		if (list.options[i].selected) {
			temp += list.options[i].value + ",";
		}
	}
	return temp;
}

function changeTParam(button, type) {
	var row = button.parentNode.parentNode;
	var row_oppType = getOppValue(row, TD_OPP_TYPE_ID);
	var row_oppId = getOppValue(row, TD_OPP_ID);
	var row_parameters = getOppValue(row, TD_PARAMETERS);
	var row_marketId = getOppValue(row, TD_MARKET_ID);
	var value = 1;
	if ((type == 'bid' && row_oppType == 4) || (type == 'offer' && row_oppType == 5) || (type == 'bid' && row_oppType == 7)) {
		value = -1;
	}
	var url = "ajax.jsf?changeTParam=" + value + "&params=" + row_parameters + "&oppId=" + row_oppId + "&marketId=" + row_marketId;
    var ajax = new AJAXInteraction(url, changeTParamCallback);
	ajax.doGet();
}

function changeTParamCallback(responseXML) {
	  var ans = responseXML.getElementsByTagName("response")[0].firstChild.nodeValue;
	  if (ans == "false") {
		  alert("can't change T param");
	  }
}