var OPPORTUNITY_STATE_OPEN 					= 1;
var OPPORTUNITY_WAITING_TO_EXPIRE 			= 2;
var OPPORTUNITY_SUSPENDED					= 3;
var OPPORTUNITY_DISABLE						= 4;

var OPPORTUNITY_TYPE_DEFAULT 				= 1;

define("dynaGrid", ["DynaGrid"],function(DynaGrid) {
		dynaGrid = new DynaGrid("opps",true);
		dynaGrid.setNodeTypes(["div", "INPUT"]);
		dynaGrid.setAutoCleanBehavior(true, false);
		dynaGrid.addListener({
			onVisualUpdate: function(key, visualUpdate, domNode){
				
				var shiftParameter_2 = visualUpdate.getChangedFieldValue("shiftParameter_2");
				if(shiftParameter_2 !== null) {
					domNode.setAttribute("shiftParameter_2", shiftParameter_2);
				}
				
				if(visualUpdate.getChangedFieldValue("command") == "ADD") {
					var oppType = visualUpdate.getChangedFieldValue("oppType");
					if(oppType !== null) {
						domNode.setAttribute("oppType", oppType);
					}
					var marketGroupId = visualUpdate.getChangedFieldValue("marketGroupId");
					if(marketGroupId !== null) {
						domNode.setAttribute("marketGroupId", marketGroupId);
					}
					var decimalPoint = visualUpdate.getChangedFieldValue("decimalPoint");
					if(decimalPoint !== null) {
						domNode.setAttribute("decimalPoint", decimalPoint);
					}
					var key = visualUpdate.getChangedFieldValue("key");
					if(key !== null) {
						domNode.setAttribute("oppId", key);
					}
					var suspendedMessage = visualUpdate.getChangedFieldValue("suspendedMessage");
					if(marketGroupId !== null) {
						domNode.setAttribute("suspendedMessage", suspendedMessage);
					}
					var feedName = visualUpdate.getChangedFieldValue("feedName");
					if(feedName !== null) {
						domNode.setAttribute("feedName", feedName);
					}
					// change marketID to market name
					var mId = visualUpdate.getChangedFieldValue("marketId");
					if(mId !== null) {
						var mName = marketsDisplayName[mId];
						if (mName !== null) {
							visualUpdate.setCellValue("marketId", mName);
							domNode.setAttribute("marketId", mId);
						}
					}
					try{
						filter(domNode);
					}catch(e){
						console.log(e);
					}
				}
				try{
					// Format opportunity state 
					var state = visualUpdate.getChangedFieldValue("state");
					if(state !== null) {
						var td = domNode.childNodes;
						
						if (state == OPPORTUNITY_DISABLE) {
							domNode.style.backgroundColor="#9D9D9D";
							for(i=0; i<td.length; i++) {
								td[i].className = "row_disable";
							}
						} else if (state == OPPORTUNITY_SUSPENDED) {
							domNode.style.backgroundColor="#FFD700";
							for(i=0; i<td.length; i++) {
								td[i].className = "row_suspend";
							}
						} else if (state == OPPORTUNITY_WAITING_TO_EXPIRE) {
							domNode.style.backgroundColor="#11b000";
							for(i=0; i<td.length; i++) {
								td[i].className = "row_waiting";
							}
						} else {
							var oppType = visualUpdate.getChangedFieldValue("oppType");
							if(oppType == null) {
								oppType = domNode.getAttribute("oppType");
							}
							if(oppType != null) {
								if (oppType == OPPORTUNITY_TYPE_DEFAULT) {
									domNode.style.backgroundColor="#FFFFFF";
								} else {
									domNode.style.backgroundColor="#B2E6D1";
								}
							}
							for(i=0; i<td.length; i++) {
								td[i].className = "row_other";
							}
						}
					}
					if (visualUpdate.getChangedFieldValue("oppOnlySuspended") != null) {
						var suspended = visualUpdate.getChangedFieldValue("oppOnlySuspended");
						if (suspended == 1) {
							domNode.setAttribute('bgColor', '#7EC0EE');
							domNode.setAttribute("style", "row_suspend");
						}
					}
					// Format Calls amount
					var callsAmount = visualUpdate.getChangedFieldValue("callsAmount");
					if(callsAmount !== null) {
						var formatedAmountCallsAmount = "$" + formatMoney(callsAmount);
						visualUpdate.setCellValue("callsAmount", formatedAmountCallsAmount);
					}
					// Format Puts amount
					var putsAmount = visualUpdate.getChangedFieldValue("putsAmount");
					if(putsAmount !== null) {
						var formatedAmountPutsAmount = "$" + formatMoney(putsAmount);
						visualUpdate.setCellValue("putsAmount", formatedAmountPutsAmount);
					}
					// Format Delta
					var delta = visualUpdate.getChangedFieldValue("callsAmountSubPutsAmount");
					if(delta !== null) {
						var formatedAmountDelta = "$" + formatMoney(delta);
						domNode.setAttribute("delta", delta);
						visualUpdate.setCellValue("callsAmountSubPutsAmount", formatedAmountDelta);
					}
					// set delta color
					if(delta != null || state != null) {
						var keepDelta = domNode.getAttribute("delta");
						var deltaNumber = new Number(keepDelta);
						if (deltaNumber > 0) {
							domNode.childNodes[23].childNodes[0].style.color = "blue";
						} else if (deltaNumber < 0){
							domNode.childNodes[23].childNodes[0].style.color = "red";
						}
					}
					
					// Format Volume
					var volume = visualUpdate.getChangedFieldValue("callsAmountAddPutsAmount");6
					if(volume !== null) {
						var formatedAmountVolume = "$" + formatMoney(volume);
						visualUpdate.setCellValue("callsAmountAddPutsAmount", formatedAmountVolume);
					}
					// Format Max exposure
					var maxExposure1 = visualUpdate.getChangedFieldValue("maxExposure_1");
					if(maxExposure1 !== null) {
						var formatedAmount = "$" + maxExposure1;
						visualUpdate.setCellValue("maxExposure_1", formatedAmount);
						domNode.setAttribute("maxExposure_1", maxExposure1);
					}

					// format exposure button ( background red on condition )
					var d = new Number(domNode.getAttribute("delta"));
					var me1 = new Number(domNode.getAttribute("maxExposure_1"));
					if(d!=null && d >0 && me1 != null ) {
						if (Math.abs(me1) * 0.75 < d) {
							domNode.getElementsByTagName("input")[0].style.color='#FF0000';
						} else {
							domNode.getElementsByTagName("input")[0].style.color='#000000';
						}
						
						if (Math.abs(me1) * 0.9 < d) {
							domNode.style.backgroundColor='#FF0000';
						}
					}
					
					// Format date of closing
					if(visualUpdate.getChangedFieldValue("timeEstClosing")!== null) {
						var timeEstClosing = new Date(visualUpdate.getChangedFieldValue("timeEstClosing"));
						var formatedAmount = formatDate(timeEstClosing);
						visualUpdate.setCellValue("timeEstClosing", formatedAmount);
					}
					// Format opportunity schedule
					var scheduled = visualUpdate.getChangedFieldValue("scheduled");
					if(scheduled !== null) {
						var formatedTxt = scheduledTxt[scheduled-1];
						visualUpdate.setCellValue("scheduled", formatedTxt);
						domNode.setAttribute("scheduled", scheduled);
					}

					var disabledTrader = visualUpdate.getChangedFieldValue("isDisabledTrader");
					if(disabledTrader !== null) {
						visualUpdate.setCellValue("isDisabledTrader", disabledTrader);
					}
					var suspended = visualUpdate.getChangedFieldValue("isSuspended");
					if(suspended != null) {
						visualUpdate.setCellValue("isSuspended", suspended);
					}
				} catch(e) {
					console.log(e);
				}				
			}
		});
		return dynaGrid;
});

function changeSort(sortOn) {
    var sortedBy = dynaGrid.getSortField();
    if (sortOn == sortedBy) {
      if (direction == false) {
        direction = true;
        document.getElementById("img_" + sortOn).src = "../../images/down.gif";
      } else if (direction == true) {
        direction = null;
        document.getElementById("img_" + sortOn).src = "../../images/spacer.gif";
      } else {
        direction = false;
        document.getElementById("img_" + sortOn).src = "../../images/up.gif";
      }
    } else {
      direction = false;
      if (sortedBy != null) {
        document.getElementById("img_" + sortedBy).src = "../../images/spacer.gif";
      }
      document.getElementById("img_" + sortOn).src = "../../images/up.gif";
    }

    if (direction == null) {
      dynaGrid.setSort(null);
    } else {
        dynaGrid.setSort(sortOn, direction, true, false);
    }
}
//use 1000 separator, remove the digits after the decimal point
function formatMoney(x) {
    var parts = x.toString().split(".");
    parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    //return parts.join(".");
    return parts[0];
}

function formatDate(est_date) {
	est_date = adjustFromUTCToLocal(est_date);

	var currdate = new Date();
	var min = est_date.getMinutes();
	if (est_date.getDate() == currdate.getDate() && est_date.getMonth() == currdate.getMonth() && est_date.getFullYear() == currdate.getFullYear()) {
		if (new Number(est_date.getMinutes()) < new Number("10")) {
			min = "0" + est_date.getMinutes();
		}
		est_date = est_date.getHours() + ":" + min + " " + bundle_msg_today;
	} else {
		if (new Number(est_date.getMinutes()) < new Number("10")) {
			min = "0" + est_date.getMinutes();
		}
		var year = new String(est_date.getFullYear());
		est_date = est_date.getHours() + ":" + min + ", " + est_date.getDate() + "." + (est_date.getMonth()+1) + "." + year.substring(2);
	}
	return est_date;
}

function adjustFromUTCToLocal(toAdj) {
	var adjM = toAdj.getMinutes() - new Date().getTimezoneOffset();
	var adjH = 0;
	if (adjM >= 0) {
		adjH = Math.floor(adjM / 60);
		adjM = adjM % 60;
	} else {
		adjH = Math.floor(Math.abs(adjM) / 60);
		if (Math.abs(adjM) % 60 != 0) {
			adjH += 1;
			adjM = 60 - Math.abs(adjM) % 60;
		} else {
			adjM = Math.abs(adjM) % 60;
		}
		adjH = -adjH;
	}
	toAdj.setHours(toAdj.getHours() + adjH);
	toAdj.setMinutes(adjM);
	return toAdj;
}


function openActionWindowSettle(button) {
	var row = button.parentNode.parentNode;
	var row_oppId = row.childNodes[1].firstChild.innerHTML;
	var row_decimalPoint = row.getAttribute("decimalPoint");
	var row_markeName = escape(row.childNodes[5].firstChild.innerHTML).replace('&amp;', '&');
	var popupWin = window.open(context_path + "/jsp/trader/traderToolActionSettle.jsf?action=" + button.getAttribute('value') + "&oppId=" + row_oppId + "&decimalPoint=" + row_decimalPoint + "&marketName=" + row_markeName ,'actionWindow','width=400,height=240');
    if (window.focus) {
    	popupWin.focus();
    }
}

function openActionWindow(button, link) {
	var row = button.parentNode.parentNode;
	var row_oppId = row.childNodes[1].firstChild.innerHTML;
	var row_suspendMsg = row.getAttribute("suspendedMessage");
	
	var row_spread_et = row.childNodes[31].childNodes[0].firstChild.innerHTML;
	var row_spread_ao = row.childNodes[31].childNodes[2].childNodes[3].innerHTML;
	var row_spread_zh = row.childNodes[35].childNodes[0].firstChild.innerHTML;
	var row_spread_tr = row.childNodes[35].childNodes[2].firstChild.innerHTML;
	
	var row_feedName = encodeURIComponent( row.getAttribute("feedName"));
	var row_markeName = escape(row.childNodes[5].firstChild.innerHTML).replace('&amp;', '&');
	var row_maxExposure = row.getAttribute("maxExposure_1");
	var actionName = button.getAttribute('value');
	if (link == 'MaxExposure') {
		actionName = 'change max exposure';
	}
	var popupWin = window.open(context_path + "/jsp/trader/traderToolAction" + link + ".jsf?action=" + actionName + "&oppId=" + row_oppId + "&suspendMsg=" + row_suspendMsg + "&feedName=" + row_feedName + "&marketName=" + row_markeName + "&maxExposure=" + row_maxExposure + "&spread1=" + row_spread_et+"&spread2=" + row_spread_ao+"&spread3=" + row_spread_zh+"&spread4=" + row_spread_tr,'actionWindow' + row_oppId, 'width=400,height=420,scrollbars=yes,left=' + document.body.clientWidth + ',top=0');
	//window.top.location.href = context_path + "/jsp/trader/traderToolAction" + link + ".jsf?action=" + button.getAttribute('value') + "&oppId=" + row_oppId + "&suspendMsg=" + row_suspendMsg + "&spread=" + row_spread + "&feedName=" + row_feedName + "&marketName=" + row_markeName + "&maxExposure=" + row_maxExposure
   if (window.focus) {
   		popupWin.focus();
   	}
}
