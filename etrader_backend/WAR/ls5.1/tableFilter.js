var filters		= new Array();
var filterIds	= new Array();

var TableFilter = (function(Arr) {
	function _onInputEvent(e) {
		var table = document.getElementById("table");
		Arr.forEach.call(table.rows, _filter);
	}
	
	function _filter(row) {
		getFilterValues();
		filter(row);
	}

	return {
		init: function() {
			var inputs = document.getElementsByTagName("select");
			Arr.forEach.call(inputs, function(input) {
				input.onchange = _onInputEvent;
			});
			getFilterValues();
		}
	};
})(Array.prototype);

(function(document) {
	document.addEventListener('readystatechange', function() {
		if (document.readyState === 'complete') {
			TableFilter.init();
		}
	});

})(document);

function getSelectedItems(list) {
	var temp = new Array();
	var k = 0;
	for (var i = 0; i < list.length; i++) {
		if (list.options[i].selected) {
			var comma = list.options[i].value.indexOf(",") + 1;
			temp[k] = list.options[i].value.substring(comma) ;
			k++;
		}
	}
	//temp = temp.slice(0,-1);
	return temp;
}


function getFilterValues(){
	var inputs = document.getElementsByTagName("select");

	for(var i=0;i<inputs.length;i++) {
		filters[i] = getSelectedItems(inputs[i]);
		filterIds[i] = inputs[i].id;
	}
}
// TODO - rewrite !
function filter(row) {
	try{
		var marketid = row.getAttribute("marketid");
		for(var i=0;i<filters.length-1;i++) {
			var rowValue = row.getAttribute(filterIds[i]);
			if(rowValue!=null) {
				if(filters[i] == "0" || filters[i] == "0,0" || filters[i].indexOf(rowValue)>-1){
					row.style.display = navigator.userAgent.toLowerCase().indexOf("msie") != -1 ? 'block' : 'table-row';
				} else {
					row.style.display = 'none';
					return;
				}
			}
		}
	
		var i = filters.length-1;
		if(s[filters[i]].indexOf(parseInt(marketid))>-1) {
			row.style.display = 'none';
			return;
		}
	}catch(e){
		//alert(e);
	}
}