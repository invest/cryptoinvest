package com.anyoption.backend.encryption.migration;

import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import org.apache.log4j.Logger;
import org.bouncycastle.crypto.CryptoException;
import org.bouncycastle.crypto.DataLengthException;

import com.anyoption.backend.encryption.migration.EncryptionMigrationManager.EncriptionMigrationCreditCard;
import com.anyoption.backend.encryption.migration.EncryptionMigrationManager.EncryptionMigrationUser;
import com.anyoption.common.beans.Job;
import com.anyoption.common.jobs.ScheduledJob;
import com.anyoption.common.util.AESUtil;

/**
 * @author kiril.mutafchiev
 */
public class EncryptionMigrationJob implements ScheduledJob {

	private static final Logger log = Logger.getLogger(EncryptionMigrationJob.class);
	private static final String CONFIG_DELIMITER = ",";
	private static final String PARAM_DELIMITER = "=";
	private static final String BATCH_PROCESSING_LIMIT = "batch_processing_limit";
	private static final String NUMBER_OF_THREADS = "number_of_threads";
	private static final Map<String, Integer> jobConfig = new HashMap<>();
	static {
		jobConfig.put(BATCH_PROCESSING_LIMIT, null);
		jobConfig.put(NUMBER_OF_THREADS, null);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.anyoption.common.jobs.ScheduledJob#run()
	 */
	// TODO revise and decide what should be logged and on what level
	@Override
	public boolean run() {
		log.debug(this.getClass() + " start");
		for (String key : jobConfig.keySet()) {
			if (jobConfig.get(key) == null) {
				log.error("Incorrect configuration. Configuration is: " + jobConfig);
				return false;
			}
		}
		Connection con = null;
		try {
			con = EncryptionMigrationManager.getSingleConnection();
			List<EncryptionMigrationUser> users = EncryptionMigrationManager.getUsersForMigration(jobConfig.get(BATCH_PROCESSING_LIMIT));
			log.debug("Begin migrating users. First batch " + users.size());
			if (!users.isEmpty()) {
				if (jobConfig.get(NUMBER_OF_THREADS) <= 1) {
					if (!migrateUsers(con, users)) {
						log.debug("No users migrated");
					}
				} else {
					// TODO process by multiple threads
				}
				log.debug("Users batch processing completed");
				// This is removed, because the job will run with only one batch
				// if (users.size() < jobConfig.get(BATCH_PROCESSING_LIMIT)) { // another call will result in empty collection
				// break;
				// } else {
				// users = EncryptionMigrationManager.getUsersForMigration(jobConfig.get(BATCH_PROCESSING_LIMIT));
				// log.debug("Fetched another " + users.size() + " users for processing");
				// }
			}
			List<EncriptionMigrationCreditCard> cards = EncryptionMigrationManager.getCreditCardsForMigration(jobConfig.get(BATCH_PROCESSING_LIMIT));
			log.debug("Begin migrating cards. First batch " + cards.size());
			if (!cards.isEmpty()) {
				if (jobConfig.get(NUMBER_OF_THREADS) <= 1) {
					if (!migrateCards(con, cards)) {
						log.debug("No cards migrated");
					}
				} else {
					// TODO process by multiple threads
				}
				log.debug("Cards batch processing completed");
				// This is removed, because the job will run with only one batch
				// if (cards.size() < jobConfig.get(BATCH_PROCESSING_LIMIT)) { // another call will result in empty collection
				// break;
				// } else {
				// cards = EncryptionMigrationManager.getCreditCardsForMigration(jobConfig.get(BATCH_PROCESSING_LIMIT));
				// log.debug("Fetched another " + cards.size() + " cards for processing");
				// }
			}
		} catch (SQLException e) {
			log.error("Unable to initialize connection", e);
			return false;
		} finally {
			EncryptionMigrationManager.closeConnection(con);
		}
		
		log.debug(this.getClass() + " end");
		return true;
	}

	@Override
	public void setJobInfo(Job job) {
		if (job.getConfig() != null && !job.getConfig().trim().isEmpty()) {
			String[] config = job.getConfig().split(CONFIG_DELIMITER);
			for (String param : config) {
				String[] paramSplit = param.split(PARAM_DELIMITER);
				if (paramSplit.length == 2 && jobConfig.containsKey(paramSplit[0])) {
					try {
						jobConfig.put(paramSplit[0], Integer.valueOf(paramSplit[1]));
					} catch (NumberFormatException e) {
						log.error("Unable to parse to integer: " + paramSplit[1], e);
					}
				} else {
					log.warn("Incorect configuration parameter. Unexpected: " + param);
				}
			}
		}
	}

	@Override
	public void stop() {
		log.debug(this.getClass() + " is stopping");
		for (String key : jobConfig.keySet()) {
			jobConfig.put(key, null);
		}
		log.debug(this.getClass() + " stopped");
	}

	private boolean migrateUsers(Connection con, List<EncryptionMigrationUser> users) {
		boolean migrated = false;
		for (EncryptionMigrationUser user : users) {
			try {
				migrateEncryption(user);
				EncryptionMigrationManager.migrate(con, user);
				migrated = true;
			} catch (	InvalidKeyException | NoSuchAlgorithmException | NoSuchPaddingException | IllegalBlockSizeException
						| BadPaddingException | UnsupportedEncodingException | CryptoException e) {
				log.error("Unable to migrate user with id: " + user.getId(), e);
			} catch (DataLengthException e) {
				log.error("Unable to decrypt fields of user with id: " + user.getId(), e);
			} catch (Exception e) {
				log.error("Unexpected error when migrating user with id: " + user.getId(), e);
			}
		}
		return migrated;
	}

	private boolean migrateCards(Connection con, List<EncriptionMigrationCreditCard> cards) {
		boolean migrated = false;
		for (EncriptionMigrationCreditCard card : cards) {
			try {
				migrateEncryption(card);
				EncryptionMigrationManager.migrate(con, card);
				migrated = true;
			} catch (	InvalidKeyException | NoSuchAlgorithmException | NoSuchPaddingException | IllegalBlockSizeException
						| BadPaddingException | UnsupportedEncodingException | CryptoException e) {
				log.error("Unable to migrate credit card with id: " + card.getId(), e);
			} catch (DataLengthException e) {
				log.error("Unable to decrypt credit card with id: " + card.getId(), e);
			} catch (Exception e) {
				log.error("Unexpected error when migrating credit card with id: " + card.getId(), e);
			}
		}
		return migrated;
	}

	private void migrateEncryption(EncryptionMigrationUser user)	throws InvalidKeyException, NoSuchAlgorithmException,
																	NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException,
																	UnsupportedEncodingException, CryptoException,
																	InvalidAlgorithmParameterException {
		user.setNewPassword(AESUtil.migrateEncryption(user.getOldPassword()));
		user.setNewIdNum(AESUtil.migrateEncryption(user.getOldIdNum()));
	}

	private void migrateEncryption(EncriptionMigrationCreditCard card)	throws InvalidKeyException, NoSuchAlgorithmException,
																		NoSuchPaddingException, IllegalBlockSizeException,
																		BadPaddingException, UnsupportedEncodingException, CryptoException,
																		InvalidAlgorithmParameterException {
		card.setNewCcNum(AESUtil.migrateEncryption(card.getOldCcNum()));
	}
}