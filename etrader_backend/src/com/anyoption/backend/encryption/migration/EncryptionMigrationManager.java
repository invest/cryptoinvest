package com.anyoption.backend.encryption.migration;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.anyoption.common.managers.BaseBLManager;

/**
 * @author kiril.mutafchiev
 */
class EncryptionMigrationManager extends BaseBLManager {

	private static final Logger log = Logger.getLogger(EncryptionMigrationManager.class);
	static EncryptionMigrationManager eMManager;

	EncryptionMigrationManager() {
		if (eMManager == null) {
			eMManager = this;
		}
	}

	static EncryptionMigrationManager getInstance() {
		if (eMManager == null) {
			new EncryptionMigrationManager();
		}
		return eMManager;
	}

	static Connection getSingleConnection() throws SQLException {
		return getConnection();
	}

	static List<EncryptionMigrationUser> getUsersForMigration(int limit) {
		Connection con = null;
		try {
			con = getConnection();
			return EncryptionMigrationDAO.getUsersForMigration(con, limit, getInstance());
		} catch (SQLException e) {
			log.debug("Unable to load users for migration", e);
			return new ArrayList<>();
		} finally {
			closeConnection(con);
		}
	}

	static List<EncriptionMigrationCreditCard> getCreditCardsForMigration(int limit) {
		Connection con = null;
		try {
			con = getConnection();
			return EncryptionMigrationDAO.getCreditCardsForMigration(con, limit, getInstance());
		} catch (SQLException e) {
			log.debug("Unable to load credit cards for migration", e);
			return new ArrayList<>();
		} finally {
			closeConnection(con);
		}
	}

	static void migrate(Connection con, EncryptionMigrationUser user) {
		try {
			EncryptionMigrationDAO.migrate(con, user);
		} catch (SQLException e) {
			log.error("Unable to migrate user with id: " + user.getId(), e);
		}
	}

	static void migrate(Connection con, EncriptionMigrationCreditCard card) {
		try {
			EncryptionMigrationDAO.migrate(con, card);
		} catch (SQLException e) {
			log.error("Unable to migrate credit card with id: " + card.getId(), e);
		}
	}

	class EncryptionMigrationUser {

		private long id;
		private String oldPassword;
		private String newPassword;
		private String oldIdNum;
		private String newIdNum;
		private boolean invalidated;

		long getId() {
			return id;
		}

		void setId(long id) {
			this.id = id;
		}

		String getOldPassword() {
			return oldPassword;
		}

		void setOldPassword(String oldPassword) {
			this.oldPassword = oldPassword;
		}

		String getNewPassword() {
			return newPassword;
		}

		void setNewPassword(String newPassword) {
			this.newPassword = newPassword;
		}

		String getOldIdNum() {
			return oldIdNum;
		}

		void setOldIdNum(String oldIdNum) {
			this.oldIdNum = oldIdNum;
		}

		String getNewIdNum() {
			return newIdNum;
		}

		void setNewIdNum(String newIdNum) {
			this.newIdNum = newIdNum;
		}

		boolean isInvalidated() {
			return invalidated;
		}

		void setInvalidated(boolean invalidated) {
			this.invalidated = invalidated;
		}

		@Override
		public String toString() {
			String ls = System.getProperty("line.separator");
			return ls + "EncryptionMigrationUser: " + ls
					+ super.toString() + ls
					+ "id: "	+ id + ls
					+ "oldPassword: " + oldPassword + ls
					+ "newPassword: " + newPassword + ls
					+ "oldIdNum: " + oldIdNum + ls
					+ "newIdNum: " + newIdNum + ls
					+ "invalidated:" + invalidated + ls;
		}
	}

	class EncriptionMigrationCreditCard {

		private long id;
		private String oldCcNum;
		private String newCcNum;

		long getId() {
			return id;
		}

		void setId(long id) {
			this.id = id;
		}

		String getOldCcNum() {
			return oldCcNum;
		}

		void setOldCcNum(String oldCcNum) {
			this.oldCcNum = oldCcNum;
		}

		String getNewCcNum() {
			return newCcNum;
		}

		void setNewCcNum(String newCcNum) {
			this.newCcNum = newCcNum;
		}

		@Override
		public String toString() {
			String ls = System.getProperty("line.separator");
			return ls + "EncriptionMigrationCreditCard: " + ls
					+ super.toString() + ls
					+ "id: " + id + ls
					+ "oldCcNum: " + oldCcNum + ls
					+ "newCcNum: " + newCcNum + ls;
		}
	}
}