/**
 * 
 */
package com.anyoption.backend.service.user;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;

import org.apache.log4j.Logger;

import com.anyoption.common.daos.DAOBase;

import oracle.jdbc.OracleTypes;

/**
 * @author pavel.t
 *
 */
public class UserDao extends DAOBase{
	
	private static final Logger logger = Logger.getLogger(UserDao.class);
	
	public static ArrayList<CryptoLead> getCryptoLeads(Connection conn, GetCryptoLeadsRequest request, GetCryptoLeadsResponse response) throws SQLException {
		ArrayList<CryptoLead> leadsList = new ArrayList<>();
		CallableStatement cstmt = null;
		ResultSet rs = null;
		int index = 1;
		try {
			
			cstmt = conn.prepareCall("{call pkg_transactions_backend.GET_CRYPTO_LEADS(o_leads => ? "+
																					   ",i_fromdate => ? "+
																					   ",i_todate => ? "+
																					   ",i_datetype => ? "+
																				       ",i_countries => ? "+
																					   ",i_kyc_status => ? "+
																					   ",i_deposit_made => ? "+
																					   ",i_user_id => ? "+
																					   ",i_email => ? "+
																					   ",i_first_name => ? "+
																					   ",i_last_name => ? "+
																					   ",i_mobile_phone => ? "+
																					   ",i_page => ? "+ 
																					   ",i_rows_per_page => ?)} ");
			
			cstmt.registerOutParameter(index++, OracleTypes.CURSOR);
			
			setStatementValue(new Timestamp(request.getFrom() == null ? 0 : request.getFrom() + 1), index++, cstmt);
			setStatementValue(new Timestamp(request.getTo() == null ? 0 : request.getTo() + 1), index++, cstmt);
			setStatementValue(request.getSearchBy(), index++, cstmt);
			setArrayVal(request.getCountries(), index++, cstmt, conn);
			setStatementValue(request.getKycStatus(), index++, cstmt);
			setStatementValue(request.getDepositsMade(), index++, cstmt);
			setStatementValue(request.getUserId(), index++, cstmt);
			setStatementValue(request.getEmail(), index++, cstmt);
			setStatementValue(request.getFirstName(), index++, cstmt);
			setStatementValue(request.getLastName(), index++, cstmt);
			setStatementValue(request.getMobilePhone(), index++, cstmt);
			setStatementValue(request.getPage(), index++, cstmt);
			setStatementValue(request.getRowsPerPage(), index++, cstmt);
			
			cstmt.execute();
			
			rs = (ResultSet) cstmt.getObject(1);
			int count = 0;
			while (rs.next()) {
				if (count == 0) {
					response.setTotalCount(rs.getLong("total_count"));
					count++;
				}
				
				CryptoLead vo = new CryptoLead();
				loadCryptoLeads(rs, vo);
				
				leadsList.add(vo);
			}
		} finally {
			closeStatement(cstmt);
			closeResultSet(rs);
		}
		return leadsList;
	}
	
	public static void setBittrexConfirmationFlag(Connection con, SetBittrexConfirmationFlagRequest request) throws SQLException {
		String sql = "update crypto_config set config_value = ? where config_key = '" + UserManager.SKIP_BUY_LIMIT + "'";
		try (PreparedStatement ps = con.prepareStatement(sql)) {
			ps.setBoolean(1, request.isSkipBuyLimit());
		    ps.executeUpdate();
		}
	}
	
	private static void loadCryptoLeads(ResultSet rs, CryptoLead leads) throws SQLException {
		
		leads.setUserId(rs.getLong("user_id"));
		leads.setName(rs.getString("name"));
		leads.setEmail(rs.getString("email"));
		leads.setCountry(rs.getString("country"));
		leads.setKycStatus(rs.getString("kyc_status"));
		leads.setTimeCreated(rs.getTimestamp("created") == null ? 0 : rs.getTimestamp("created").getTime());
		leads.setDepositMade(rs.getLong("deposits_made"));

	}
	
}
