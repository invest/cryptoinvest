/**
 * 
 */
package com.anyoption.backend.service.user;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import com.anyoption.backend.managers.WriterPermisionsManager;
import com.anyoption.backend.service.writerpermissions.ScreenSettingsResponse;
import com.anyoption.common.backend.annotations.BackendPermission;
import com.anyoption.common.managers.CountryManagerBase;
import com.anyoption.common.service.CommonJSONService;
import com.anyoption.common.service.results.MethodResult;

import il.co.etrader.backend.bl_vos.WriterWrapper;
import il.co.etrader.util.ConstantsBase;

/**
 * @author pavel.t
 *
 */
public class UserServices {
	
	private static final Logger log = Logger.getLogger(UserServices.class);
	private static final String SCREEN_NAME_USERS = "cryptoLeads";
	
	public static ScreenSettingsResponse getUserScreenSettings(HttpServletRequest httpRequest) {
		ScreenSettingsResponse response = new ScreenSettingsResponse();
		HashMap<String, HashMap<Long, String>> filters = new HashMap<>();
		WriterWrapper writer = (WriterWrapper) httpRequest.getSession().getAttribute(ConstantsBase.BIND_SESSION_WRITER_WRAPPER);
		
		WriterPermisionsManager.setWriterScreenPermissionsByScreenName(SCREEN_NAME_USERS, response, httpRequest);
		//FILTERS
		
		HashMap<Long, String> countries = CountryManagerBase.getAllCountriesFilter();
		filters.put(CountryManagerBase.FILTER_COUNTRIES, countries);
		

		HashMap<Long, String> kycStatus = UserManager.getKycStatus();
		filters.put(UserManager.FILTER_KYC_STATUS, kycStatus);
		

		HashMap<Long, String> depositMade = UserManager.getDepositMade();
		filters.put(UserManager.FILTER_DEPOSIT_MADE, depositMade);
		
		response.setScreenFilters(filters);
		return response;
	}
	
	
	@BackendPermission(id = "cryptoLeads_view")
	public static GetCryptoLeadsResponse getCryptoLeads(GetCryptoLeadsRequest request, HttpServletRequest httpRequest) {
		GetCryptoLeadsResponse response = new GetCryptoLeadsResponse();

		if (request == null) {
			response.setResponseCode(CommonJSONService.ERROR_CODE_INVALID_INPUT);
			return response;
		}

		WriterWrapper writer = (WriterWrapper) httpRequest.getSession().getAttribute(ConstantsBase.BIND_SESSION_WRITER_WRAPPER);
		long writerId = writer.getWriter().getId();
		request.setWriterUtcOffset(writer.getUtcOffset());
		request.setWriterId(writerId);

		try {
			ArrayList<CryptoLead> cryptoLeads = UserManager.getCryptoLeads(request, response);
			response.setCryptoLeads(cryptoLeads);
		} catch (SQLException e) {
			log.error("Enable to get crypto leads due to: " + e);
			response.setResponseCode(CommonJSONService.ERROR_CODE_UNKNOWN);
		}
		return response;
	}
	
	@BackendPermission(id = "cryptoLeads_buyLimitSkip")
	public static MethodResult setBittrexConfirmationFlag(SetBittrexConfirmationFlagRequest request, HttpServletRequest httpRequest) {
		MethodResult response = new MethodResult();

		if (request == null) {
			response.setResponseCode(CommonJSONService.ERROR_CODE_INVALID_INPUT);
			return response;
		}

		try {
			UserManager.setBittrexConfirmationFlag(request);
		} catch (SQLException e) {
			log.error("Enable to get crypto leads due to: " + e);
			response.setResponseCode(CommonJSONService.ERROR_CODE_UNKNOWN);
		}
		return response;
	}
}
