/**
 * 
 */
package com.anyoption.backend.service.user;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;

import org.apache.log4j.Logger;

import com.anyoption.common.managers.BaseBLManager;

/**
 * @author pavel.tabakov
 *
 */
public class UserManager extends BaseBLManager {
	
	private static final Logger logger = Logger.getLogger(UserManager.class);
	
	public static final String FILTER_COUNTRIES = "countries";
	public static final String FILTER_KYC_STATUS = "kycStatus";
	public static final String FILTER_DEPOSIT_MADE = "depositMade";
	static final String SKIP_BUY_LIMIT = "SKIP_BITREX_CONFIRMATION";
	
	public static final String[] filterConstants = {"", FILTER_COUNTRIES, FILTER_KYC_STATUS, FILTER_DEPOSIT_MADE};
	
	public static HashMap<Long, String> kycStatus;
	public static HashMap<Long, String> depositMade;
	
	// FILTERS START	
	public static HashMap<Long, String> getKycStatus() {
		kycStatus = new HashMap<>();
		kycStatus.put(1l, "Yes");
		kycStatus.put(0l, "No");
		return kycStatus;
	}
	
	public static HashMap<Long, String> getDepositMade() {
		depositMade = new HashMap<>();
		depositMade.put(1l, "Yes");
		depositMade.put(0l, "No");
		return depositMade;
	}
	// FILTERS END
	
	public static ArrayList<CryptoLead> getCryptoLeads(GetCryptoLeadsRequest request, GetCryptoLeadsResponse response) throws SQLException {
		Connection conn = null;
		try {
			conn = getConnection();
			return UserDao.getCryptoLeads(conn, request, response);
		} finally {
			closeConnection(conn);
		}
	}
	
	public static void setBittrexConfirmationFlag(SetBittrexConfirmationFlagRequest request) throws SQLException {
		Connection con = null;
		try {
			con = getConnection();
			UserDao.setBittrexConfirmationFlag(con, request);
		} finally {
			closeConnection(con);
		}
	}
}
