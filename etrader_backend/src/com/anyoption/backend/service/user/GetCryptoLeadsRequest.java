/**
 * 
 */
package com.anyoption.backend.service.user;

import java.util.ArrayList;

import com.anyoption.common.service.requests.MethodRequest;

/**
 * @author pavel.t
 *
 */
public class GetCryptoLeadsRequest extends MethodRequest {

	private Long from;
	private Long to;
	private Long searchBy; // radio button registered = 1, last_note = 2
	private ArrayList<Integer> countries;
	private Long userId;
	private String mobilePhone;
	private String email;
	private String firstName;
	private String lastName;
	private Long kycStatus;
	private Long depositsMade;
	private Long page;
	private Long rowsPerPage;
	private String writerUtcOffset;
	/**
	 * @return the from
	 */
	public Long getFrom() {
		return from;
	}
	/**
	 * @param from the from to set
	 */
	public void setFrom(Long from) {
		this.from = from;
	}
	/**
	 * @return the to
	 */
	public Long getTo() {
		return to;
	}
	/**
	 * @param to the to to set
	 */
	public void setTo(Long to) {
		this.to = to;
	}
	/**
	 * @return the searchBy
	 */
	public Long getSearchBy() {
		return searchBy;
	}
	/**
	 * @param searchBy the searchBy to set
	 */
	public void setSearchBy(Long searchBy) {
		this.searchBy = searchBy;
	}
	/**
	 * @return the countries
	 */
	public ArrayList<Integer> getCountries() {
		return countries;
	}
	/**
	 * @param countries the countries to set
	 */
	public void setCountries(ArrayList<Integer> countries) {
		this.countries = countries;
	}
	/**
	 * @return the userId
	 */
	public Long getUserId() {
		return userId;
	}
	/**
	 * @param userId the userId to set
	 */
	public void setUserId(Long userId) {
		this.userId = userId;
	}
	/**
	 * @return the mobilePhone
	 */
	public String getMobilePhone() {
		return mobilePhone;
	}
	/**
	 * @param mobilePhone the mobilePhone to set
	 */
	public void setMobilePhone(String mobilePhone) {
		this.mobilePhone = mobilePhone;
	}
	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}
	/**
	 * @param email the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}
	/**
	 * @return the firstName
	 */
	public String getFirstName() {
		return firstName;
	}
	/**
	 * @param firstName the firstName to set
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	/**
	 * @return the lastName
	 */
	public String getLastName() {
		return lastName;
	}
	/**
	 * @param lastName the lastName to set
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	/**
	 * @return the writerUtcOffset
	 */
	public String getWriterUtcOffset() {
		return writerUtcOffset;
	}
	/**
	 * @param writerUtcOffset the writerUtcOffset to set
	 */
	public void setWriterUtcOffset(String writerUtcOffset) {
		this.writerUtcOffset = writerUtcOffset;
	}
	public Long getKycStatus() {
		return kycStatus;
	}
	public void setKycStatus(Long kycStatus) {
		this.kycStatus = kycStatus;
	}
	public Long getDepositsMade() {
		return depositsMade;
	}
	public void setDepositsMade(Long depositsMade) {
		this.depositsMade = depositsMade;
	}
	public Long getPage() {
		return page;
	}
	public void setPage(Long page) {
		this.page = page;
	}
	public Long getRowsPerPage() {
		return rowsPerPage;
	}
	public void setRowsPerPage(Long rowsPerPage) {
		this.rowsPerPage = rowsPerPage;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "GetCryptoLeadsRequest [from=" + from + ", to=" + to + ", searchBy=" + searchBy + ", countries="
				+ countries + ", userId=" + userId + ", mobilePhone=" + mobilePhone + ", email=" + email
				+ ", firstName=" + firstName + ", lastName=" + lastName + ", kycStatus=" + kycStatus + ", depositsMade="
				+ depositsMade + ", page=" + page + ", rowsPerPage=" + rowsPerPage + ", writerUtcOffset="
				+ writerUtcOffset + "]";
	}
}
