/**
 * 
 */
package com.anyoption.backend.service.user;

import com.anyoption.common.service.requests.MethodRequest;

/**
 * @author pavel.t
 *
 */
public class SetBittrexConfirmationFlagRequest extends MethodRequest {
	
	private boolean skipBuyLimit;

	/**
	 * @return the skipBuyLimit
	 */
	public boolean isSkipBuyLimit() {
		return skipBuyLimit;
	}

	/**
	 * @param skipBuyLimit the skipBuyLimit to set
	 */
	public void setSkipBuyLimit(boolean skipBuyLimit) {
		this.skipBuyLimit = skipBuyLimit;
	}
	
	@Override
	public String toString() {
		return "SetBittrexConfirmationFlagRequest [skipBuyLimit=" + skipBuyLimit + "]";
	}

}
