/**
 * 
 */
package com.anyoption.backend.service.user;

import java.io.Serializable;

/**
 * @author pavel.t
 *
 */
public class CryptoLead implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 9145819613767824913L;
	private long userId;
	private String name;
	private String email;
	private String country;
	private String kycStatus;
	private long timeCreated;
	private Long depositMade;// tell Dimandiev for this change
	/**
	 * @return the userId
	 */
	public long getUserId() {
		return userId;
	}
	/**
	 * @param userId the userId to set
	 */
	public void setUserId(long userId) {
		this.userId = userId;
	}
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}
	/**
	 * @param email the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}
	/**
	 * @return the country
	 */
	public String getCountry() {
		return country;
	}
	/**
	 * @param country the country to set
	 */
	public void setCountry(String country) {
		this.country = country;
	}
	/**
	 * @return the kycStatus
	 */
	public String getKycStatus() {
		return kycStatus;
	}
	/**
	 * @param kycStatus the kycStatus to set
	 */
	public void setKycStatus(String kycStatus) {
		this.kycStatus = kycStatus;
	}
	/**
	 * @return the depositMode
	 */
	public Long getDepositMade() {
		return depositMade;
	}
	/**
	 * @param depositMode the depositMode to set
	 */
	public void setDepositMade(Long depositMade) {
		this.depositMade = depositMade;
	}
	public long getTimeCreated() {
		return timeCreated;
	}
	public void setTimeCreated(long timeCreated) {
		this.timeCreated = timeCreated;
	}
}
