/**
 * 
 */
package com.anyoption.backend.service.user;

import java.util.ArrayList;

import com.anyoption.common.service.results.MethodResult;

/**
 * @author pavel.t
 *
 */
public class GetCryptoLeadsResponse extends MethodResult {

	private ArrayList<CryptoLead> cryptoLeads;
	private long totalCount;

	public long getTotalCount() {
		return totalCount;
	}

	public void setTotalCount(long totalCount) {
		this.totalCount = totalCount;
	}

	public ArrayList<CryptoLead> getCryptoLeads() {
		return cryptoLeads;
	}

	public void setCryptoLeads(ArrayList<CryptoLead> cryptoLeads) {
		this.cryptoLeads = cryptoLeads;
	}

}
