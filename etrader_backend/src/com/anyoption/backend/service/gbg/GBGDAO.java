/**
 * 
 */
package com.anyoption.backend.service.gbg;

import java.io.StringReader;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;

import org.apache.log4j.Logger;

import com.anyoption.common.daos.GBGDAOBase;

import il.co.etrader.util.CommonUtil;
import oracle.jdbc.OracleTypes;


public class GBGDAO extends GBGDAOBase {
	
	private static final Logger logger = Logger.getLogger(GBGDAO.class);

	public static void insertResponse(long userId, Connection conn) throws SQLException {				
		CallableStatement cstmt = null;
		int index = 1;
		try {
			cstmt = conn.prepareCall("{call pkg_userfiles.create_gbg_record(i_user_id => ? )}");							
			cstmt.setLong(index++, userId);					
			cstmt.execute();			
		} finally {
			closeStatement(cstmt);
		}
	}
	
	  public static ArrayList<GBGRequest> loadGBGUsersForSend(Connection con) throws SQLException {		  	
			CallableStatement cstmt = null;
			ResultSet rs = null;
			int index = 1;
			 ArrayList<GBGRequest> gbgUsers = new ArrayList<>();
			try {
				String sql = "{call pkg_userfiles.load_gbg_users_for_send(?)}";
				cstmt = con.prepareCall(sql);
				cstmt.registerOutParameter(index++, OracleTypes.CURSOR);				
				cstmt.executeQuery();
				rs = (ResultSet) cstmt.getObject(1);
				while (rs.next()) {
					GBGRequest gbg = new GBGRequest(rs.getLong("id"), 
													rs.getLong("user_id"), 
													rs.getString("profile_id"), 
													0, 
													rs.getString("first_name"), 
													rs.getString("last_name"), 
													rs.getString("gender"), 
													rs.getString("DOBDay"), 
													rs.getString("DOBMonth"), 
													rs.getString("DOBYear"), 
													rs.getString("country_name"), 
													rs.getString("street"), 
													rs.getString("city_name"), 
													rs.getString("zip_code"), 
													rs.getString("street_no"));
					gbgUsers.add(gbg);
				}
			} finally {
				closeStatement(cstmt);			
			}
			return gbgUsers;
	  }
	  
		public static void updateGBGStatus(long id, long statusId, Connection conn) throws SQLException {				
			CallableStatement cstmt = null;
			int index = 1;
			try {
				cstmt = conn.prepareCall("{call pkg_userfiles.update_gbg_status_id(i_gbg_id => ? " +
						                                                        ", i_status_id => ? )}");							
				cstmt.setLong(index++, id);
				cstmt.setLong(index++, statusId);
				cstmt.execute();			
			} finally {
				closeStatement(cstmt);
			}
		}
		
		public static long updateGBGResponse(Connection conn, GBGResponse response) throws SQLException {
			CallableStatement cstmt = null;
			int index = 1;
			long resByScore = 0;
			Long score = null;
			try {
				if(!CommonUtil.isParameterEmptyOrNull(response.getScore())){
					score = new Long( response.getScore());
				}
			} catch (Exception e) {
				logger.error("Can't cast to long score:" + response.getScore());
			}
			try {
				String sql = "{call pkg_userfiles.update_gbg_response(?, ?, ?, ?, ?, ?, ?, ?)}";
				cstmt = conn.prepareCall(sql);
		
				cstmt.registerOutParameter(index++, Types.INTEGER);
				cstmt.setLong(index++, response.getId());				
				setStatementValue(score, index++, cstmt);
				cstmt.setNull(index++, OracleTypes.NUMBER);
				setStatementValue(response.getErrorCode(), index++, cstmt);
				setStatementValue(response.getErrorReason(), index++, cstmt);
				cstmt.setCharacterStream(index++, new StringReader(response.getXmlRequest()), response.getXmlRequest().length());
				cstmt.setCharacterStream(index++, new StringReader(response.getXmlResponse()), response.getXmlResponse().length());
				
				cstmt.executeQuery();	
				resByScore = cstmt.getInt(1);
			} finally {
				closeStatement(cstmt);
			}			
			return resByScore;
		}
		
		public static void updateGBGFileId(long id, long fileId, Connection conn) throws SQLException {				
			CallableStatement cstmt = null;
			int index = 1;
			try {
				cstmt = conn.prepareCall("{call pkg_userfiles.update_gbg_file_id(i_gbg_id => ? " +
						                                                        ", i_file_id => ? )}");							
				cstmt.setLong(index++, id);
				cstmt.setLong(index++, fileId);
				cstmt.execute();			
			} finally {
				closeStatement(cstmt);
			}
		}
		
		public static GBGResponse getGBGResult(long fileId, Connection conn) throws SQLException {				
			CallableStatement cstmt = null;
			ResultSet rs = null;
			GBGResponse res = new GBGResponse();
			res.setErrorCode("0");
			int index = 1;
			try {
				cstmt = conn.prepareCall("{call pkg_userfiles.get_gbg_result(o_result => ? " +
						                                                  ", i_file_id => ? )}");							
				cstmt.registerOutParameter(index++, OracleTypes.CURSOR);
				cstmt.setLong(index++, fileId);
				cstmt.execute();
				rs = (ResultSet) cstmt.getObject(1);
				if (rs.next()) {
					res.setId(rs.getLong("id"));
					res.setXmlRequest(CommonUtil.clobToString(rs.getClob(("request_xml"))));
					res.setXmlResponse(CommonUtil.clobToString(rs.getClob(("response_xml"))));					
				}
			return res;
			
			} finally {
				closeStatement(cstmt);
			}
		}
}
