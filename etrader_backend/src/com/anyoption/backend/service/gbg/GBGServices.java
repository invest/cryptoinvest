/**
 * 
 */
package com.anyoption.backend.service.gbg;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

import javax.xml.namespace.QName;
import javax.xml.soap.MessageFactory;
import javax.xml.soap.MimeHeaders;
import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPConnection;
import javax.xml.soap.SOAPConnectionFactory;
import javax.xml.soap.SOAPElement;
import javax.xml.soap.SOAPEnvelope;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPHeader;
import javax.xml.soap.SOAPHeaderElement;
import javax.xml.soap.SOAPMessage;
import javax.xml.soap.SOAPPart;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Document;

import com.anyoption.common.service.CommonJSONService;
import com.anyoption.common.util.ClearingUtil;
import com.anyoption.common.util.CommonUtil;

/**
 * @author pavel.tabakov
 *
 */
public class GBGServices {

	public static GBGResponse authenticateSP(GBGRequest r) {
		GBGResponse response = new GBGResponse();

		if (r == null || r.getProfileId() == null || r.getProfileId().equals("")) {
			response.setErrorReason("Prifile ID is null or empty!");
			response.setErrorCode(String.valueOf(CommonJSONService.ERROR_CODE_INVALID_INPUT));
			return response;
		}

		try {

			SOAPConnectionFactory soapConnectionFactory = SOAPConnectionFactory.newInstance();
			SOAPConnection soapConnection = soapConnectionFactory.createConnection();
			SOAPMessage soapResponse = soapConnection.call(createSoapRequest(r), CommonUtil.getProperty("gbg.url"));
			
			// include SOAP request to response object
			response.setXmlRequest(soapMessageToXML(createSoapRequest(r)));

			// manage soap response
			getGBGResponse(soapResponse, response);

		} catch (Exception ex) {
			response.setErrorReason("Can NOT execute authenticateSP due to: ", ex);
			response.setErrorCode(String.valueOf(CommonJSONService.ERROR_CODE_UNKNOWN));
		}

		return response;
	}

	private static SOAPMessage createSoapRequest(GBGRequest r) throws SOAPException, IOException {

		// construct SOAP message
		SOAPMessage message = MessageFactory.newInstance().createMessage();
		SOAPPart part = message.getSOAPPart();

		// envelope
		SOAPEnvelope envelope = part.getEnvelope();
		envelope.addNamespaceDeclaration("ns", "http://www.id3global.com/ID3gWS/2013/04");
		// envelope

		// construct header
		SOAPHeader header = envelope.getHeader();
		header.addNamespaceDeclaration("wsa", "http://www.w3.org/2005/08/addressing");

		QName security = new QName("http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd", "Security", "wsse");
		SOAPHeaderElement securityHeader = header.addHeaderElement(security);
		securityHeader.addNamespaceDeclaration("wsu", "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd");
		securityHeader.setMustUnderstand(true);
		SOAPElement usernameToken = securityHeader.addChildElement("UsernameToken", "wsse");
		SOAPElement username = usernameToken.addChildElement("Username", "wsse");
		username.addTextNode(CommonUtil.getProperty("gbg.username"));
		SOAPElement password = usernameToken.addChildElement("Password", "wsse");
		password.addTextNode(CommonUtil.getProperty("gbg.password"));

		// construct body
		SOAPBody body = envelope.getBody();
		SOAPElement authenticateSP = body.addChildElement("AuthenticateSP", "ns");

		SOAPElement profileIDVersion = authenticateSP.addChildElement("ProfileIDVersion", "ns");

		SOAPElement id = profileIDVersion.addChildElement("ID", "ns");
		id.addTextNode(r.getProfileId());
		SOAPElement version = profileIDVersion.addChildElement("Version", "ns");
		version.addTextNode(String.valueOf(r.getVersion()));

		SOAPElement inputData = authenticateSP.addChildElement("InputData", "ns");
		SOAPElement personal = inputData.addChildElement("Personal", "ns");
		SOAPElement personalDetails = personal.addChildElement("PersonalDetails", "ns");
		
		// customer personal details info
		addTag(r.getFirstName(), personalDetails, "Forename");
		addTag(r.getLastName() , personalDetails, "Surname");
		addTag(r.getGender(), personalDetails, "Gender");
		addTag(r.getDOBDay(), personalDetails, "DOBDay");
		addTag(r.getDOBMonth(), personalDetails, "DOBMonth");
		addTag(r.getDOBYear(), personalDetails, "DOBYear");

		// client address info
		SOAPElement addresses = inputData.addChildElement("Addresses", "ns");
		SOAPElement currentAddress = addresses.addChildElement("CurrentAddress", "ns");
		
		addTag(r.getCountryName(), currentAddress, "Country");
		addTag(r.getStreet(), currentAddress, "Street");
		addTag(r.getCity(), currentAddress, "City");
		addTag(r.getZipCode(), currentAddress, "ZipPostcode");
		addTag(r.getStreetNo(), currentAddress, "Building");

		MimeHeaders headers = message.getMimeHeaders();
		headers.addHeader("SOAPAction", "http://www.id3global.com/ID3gWS/2013/04/IGlobalAuthenticate/AuthenticateSP");

		message.saveChanges();

		return message;
	}

	private static void getGBGResponse(SOAPMessage soapResponse, GBGResponse response) throws Exception {

		String xml = soapMessageToXML(soapResponse);
		response.setXmlResponse(xml);

		Document doc = ClearingUtil.parseXMLToDocument(xml);
		XPath xPath = XPathFactory.newInstance().newXPath();

		String errorCode = xPath.compile("//Fault//faultcode").evaluate(doc);
		String errorReason = xPath.compile("//Fault//faultstring").evaluate(doc);
		if (errorCode == null || errorCode.equals("")) {
			response.setScore(xPath.compile("//AuthenticateSPResponse//AuthenticateSPResult//Score").evaluate(doc));
		} else {
			response.setErrorCode(errorCode);
			response.setErrorReason(errorReason);
		}
	}
	
	private static String soapMessageToXML(SOAPMessage message) throws Exception {
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		message.writeTo(out);
		return new String(out.toByteArray());
	}
	
	private static void addTag(String requestValue, SOAPElement el, String tagName) throws SOAPException {
		if (requestValue != null && !requestValue.equals("")) {
			SOAPElement e = el.addChildElement(tagName, "ns");
			e.addTextNode(requestValue);
		}
	}
}
