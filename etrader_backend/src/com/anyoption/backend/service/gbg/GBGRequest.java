/**
 * 
 */
package com.anyoption.backend.service.gbg;

import com.anyoption.common.service.requests.UserMethodRequest;

/**
 * @author pavel.tabakov
 *
 */
public class GBGRequest extends UserMethodRequest {
	
	
	private long id;
	private long userId;
	// authentication
	private String profileId;// profileIDVersion --> gbg_countries table
	private long version;// always 0 (zero)
	// personal details
	private String firstName;// forename
	private String lastName;// surname
	private String gender;// gender
	private String DOBDay;// DOBDay
	private String DOBMonth;// DOBMonth
	private String DOBYear;// DOBYear
	// addresses
	private String countryName;// country
	private String street;// street
	private String city;// city;
	private String zipCode;// zipPostcode
	private String streetNo;// building;
	
	public GBGRequest(long id, long userId, String profileId, long version, String firstName, String lastName,
			String gender, String dOBDay, String dOBMonth, String dOBYear, String countryName, String street,
			String city, String zipCode, String streetNo) {
		super();
		this.id = id;
		this.userId = userId;
		this.profileId = profileId;
		this.version = version;
		this.firstName = firstName;
		this.lastName = lastName;
		this.gender = gender;
		DOBDay = dOBDay;
		DOBMonth = dOBMonth;
		DOBYear = dOBYear;
		this.countryName = countryName;
		this.street = street;
		this.city = city;
		this.zipCode = zipCode;
		this.streetNo = streetNo;
	}
	
	public GBGRequest() {		
	}

	public long getVersion() {
		return version;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getDOBDay() {
		return DOBDay;
	}

	public void setDOBDay(String dOBDay) {
		DOBDay = dOBDay;
	}

	public String getDOBMonth() {
		return DOBMonth;
	}

	public void setDOBMonth(String dOBMonth) {
		DOBMonth = dOBMonth;
	}

	public String getDOBYear() {
		return DOBYear;
	}

	public void setDOBYear(String dOBYear) {
		DOBYear = dOBYear;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getCountryName() {
		return countryName;
	}

	public void setCountryName(String countryName) {
		this.countryName = countryName;
	}

	public String getStreetNo() {
		return streetNo;
	}

	public void setStreetNo(String streetNo) {
		this.streetNo = streetNo;
	}

	public String getZipCode() {
		return zipCode;
	}

	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}

	public String getStreet() {
		return street;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	public String getAddressLine1() {
		return street + " " + streetNo;
	}

	public String getProfileId() {
		return profileId;
	}

	public void setProfileId(String profileId) {
		this.profileId = profileId;
	}
	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}
	

	public long getUserId() {
		return userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}

	@Override
	public String toString() {
		return "GBGRequest [Id=" + id + " userId=" + userId + " profileId=" + profileId + ", version=" + version + ", firstName=" + firstName
				+ ", lastName=" + lastName + ", gender=" + gender + ", DOBDay=" + DOBDay + ", DOBMonth=" + DOBMonth
				+ ", DOBYear=" + DOBYear + ", countryName=" + countryName + ", street=" + street + ", city=" + city
				+ ", zipCode=" + zipCode + ", streetNo=" + streetNo + "getAddressLine1=" + getAddressLine1() + "]";
	}
}
