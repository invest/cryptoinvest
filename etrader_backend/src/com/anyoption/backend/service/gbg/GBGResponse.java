/**
 * 
 */
package com.anyoption.backend.service.gbg;

import java.io.PrintWriter;
import java.io.StringWriter;

/**
 * @author pavel.tabakov
 *
 */
public class GBGResponse {
	
	private long id;
	private String score;
	private String xmlRequest;
	private String xmlResponse;
	private String errorCode;
	private String errorReason;

	public String getScore() {
		return score;
	}

	public void setScore(String score) {
		this.score = score;
	}

	public String getErrorReason() {
		return errorReason;
	}

	public void setErrorReason(String errorReason) {
		this.errorReason = errorReason;
	}

	public void setErrorReason(String errorReason, Exception ex) {
		StringWriter errors = new StringWriter();
		ex.printStackTrace(new PrintWriter(errors));
		String er = errorReason + errors.toString();
		if (er.length() > 4000) {//check stackTrace length, because of VARCHAR2 type -> 4000 chars constraint
			this.errorReason = er.substring(0, 4000);
		} else {
			this.errorReason = er;
		}
	}

	public String getXmlResponse() {
		return xmlResponse;
	}

	public void setXmlResponse(String xmlResponse) {
		this.xmlResponse = xmlResponse;
	}

	public String getXmlRequest() {
		return xmlRequest;
	}

	public void setXmlRequest(String xmlRequest) {
		this.xmlRequest = xmlRequest;
	}

	public String getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}
	
	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	@Override
	public String toString() {
		return "GBGResponse [id=" + id + " score=" + score + ", xmlResponse=" + xmlResponse + ", xmlRequest=" + xmlRequest
				+ ", errorCode=" + errorCode + ", errorReason=" + errorReason + "]";
	}
}
