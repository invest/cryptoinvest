/**
 * 
 */
package com.anyoption.backend.service.keesing;


import java.sql.Date;

import com.anyoption.common.service.results.MethodResult;

/**
 * @author pavel.tabakov
 *
 */
public class KeesingResponse extends MethodResult {
	
	private long fileId;
	private Long checkStatusValue;
	private String checkStatusText;
	private Long reasonNotOkValue;
	private String reasonNotOkText;
	private Long persID;
	private String lastname;
	private String firstnames;
	private String nationality;
	private String documentNr;
	private Date expireDate;
	private String issuingCountry;
	private Long docID;
	private Date dateOfBirth;
	private String gender;
	private String xml;	

	public String toString() {
		String ls = System.getProperty("line.separator");
		return ls + "KeesingResponse: " + ls + super.toString() + "fileId" + fileId + ls + "checkStatusValue: " + checkStatusValue + ls
				+ "checkStatusText: " + checkStatusText + ls + "reasonNotOkValue: " + reasonNotOkValue + ls
				+ "reasonNotOkText: " + reasonNotOkText + ls + "persID: " + persID + ls + "lastname: " + lastname + ls
				+ "firstnames: " + firstnames + ls + "nationality: " + nationality + ls + "documentNr: " + documentNr
				+ ls + "expireDate: " + expireDate + ls + "issuingCountry: " + issuingCountry + ls + "docID: " + docID
				+ ls + "dateOfBirth: " + dateOfBirth + ls + ls + "gender: " + gender + ls + ls + "xml: " + xml + ls;
	}

	public Long getCheckStatusValue() {
		return checkStatusValue;
	}

	public void setCheckStatusValue(Long checkStatusValue) {
		this.checkStatusValue = checkStatusValue;
	}

	public String getCheckStatusText() {
		return checkStatusText;
	}

	public void setCheckStatusText(String checkStatusText) {
		this.checkStatusText = checkStatusText;
	}

	public Long getReasonNotOkValue() {
		return reasonNotOkValue;
	}

	public void setReasonNotOkValue(Long reasonNotOkValue) {
		this.reasonNotOkValue = reasonNotOkValue;
	}

	public String getReasonNotOkText() {
		return reasonNotOkText;
	}

	public void setReasonNotOkText(String reasonNotOkText) {
		this.reasonNotOkText = reasonNotOkText;
	}

	public Long getPersID() {
		return persID;
	}

	public void setPersID(Long persID) {
		this.persID = persID;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public String getFirstnames() {
		return firstnames;
	}

	public void setFirstnames(String firstnames) {
		this.firstnames = firstnames;
	}

	public String getNationality() {
		return nationality;
	}

	public void setNationality(String nationality) {
		this.nationality = nationality;
	}

	public String getDocumentNr() {
		return documentNr;
	}

	public void setDocumentNr(String documentNr) {
		this.documentNr = documentNr;
	}

	public Date getExpireDate() {
		return expireDate;
	}

	public void setExpireDate(Date expireDate) {
		this.expireDate = expireDate;
	}

	public String getIssuingCountry() {
		return issuingCountry;
	}

	public void setIssuingCountry(String issuingCountry) {
		this.issuingCountry = issuingCountry;
	}

	public Long getDocID() {
		return docID;
	}

	public void setDocID(Long docID) {
		this.docID = docID;
	}

	public String getXml() {
		return xml;
	}

	public void setXml(String xml) {
		this.xml = xml;
	}

	public Date getDateOfBirth() {
		return dateOfBirth;
	}

	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public long getFileId() {
		return fileId;
	}

	public void setFileId(long fileId) {
		this.fileId = fileId;
	}

}
