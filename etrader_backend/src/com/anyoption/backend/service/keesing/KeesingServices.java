/**
 * 
 */
package com.anyoption.backend.service.keesing;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Base64;
import java.util.Iterator;

import javax.xml.soap.MessageFactory;
import javax.xml.soap.Node;
import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPBodyElement;
import javax.xml.soap.SOAPConnection;
import javax.xml.soap.SOAPConnectionFactory;
import javax.xml.soap.SOAPElement;
import javax.xml.soap.SOAPEnvelope;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;
import javax.xml.soap.SOAPPart;

import org.apache.commons.io.FilenameUtils;
import org.apache.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import com.anyoption.backend.managers.KeesingManager;
import com.anyoption.common.service.CommonJSONService;
import com.anyoption.common.util.ClearingUtil;
import com.anyoption.common.util.CommonUtil;
import com.itextpdf.text.Image;
import com.itextpdf.text.pdf.PdfWriter;

/**
 * @author pavel.tabakov
 *
 */
public class KeesingServices {

	private static final Logger log = Logger.getLogger(KeesingServices.class);

	public static KeesingResponse checkIDDocument(KeesingRequest request) {
		KeesingResponse response = new KeesingResponse();
		response.setFileId(request.getFileId());

		if (request == null || request.getFilePath().equals("") || request.getFileName().equals("") || request.getFilePath() == null || request.getFileName() == null) {
			response.setErrorCode(CommonJSONService.ERROR_CODE_INVALID_INPUT);
			return response;
		}
		
		if (request.getFileId() < 1) {
			response.setErrorCode(CommonJSONService.ERROR_CODE_INVALID_INPUT);
			return response;
		}

		try {
			
			SOAPConnectionFactory soapConnectionFactory = SOAPConnectionFactory.newInstance();
			SOAPConnection soapConnection = soapConnectionFactory.createConnection();
			SOAPMessage soapResponse = soapConnection.call(createSoapRequest(request.getFilePath(), request.getFileName()), CommonUtil.getProperty("keesing.url"));
			
			//manage soap response
			getKeesingSoapResponse(soapResponse, response);
			
			//insert message to DB
			KeesingManager.insertResponse(response, request);
			
		} catch (Exception ex) {
			log.error("Can NOT check ID Document due to: ", ex);
			response.setErrorCode(CommonJSONService.ERROR_CODE_UNKNOWN);
		}
		
		return response;
	}
	

	 private static SOAPMessage createSoapRequest(String imagePath, String imageName) throws SOAPException, IOException {
		 
			// start convert to PDF , supported formats ->> .gif, .jpg, .png
			com.itextpdf.text.Document document = new com.itextpdf.text.Document();
			String input = imagePath + imageName;
			String extension = FilenameUtils.getExtension(imageName).toLowerCase();
			String output = "";
			if (!extension.equals("pdf")) {
				String imageNameNoExt = FilenameUtils.removeExtension(imageName);
				output = imagePath + imageNameNoExt + ".pdf";
				try {
					FileOutputStream fos = new FileOutputStream(output);
					PdfWriter writer = PdfWriter.getInstance(document, fos);
					writer.open();
					document.open();
					Image img = Image.getInstance(input);
					img.scaleToFit(595, 842);
					img.setAbsolutePosition(0, 0);
					document.add(img);
					document.close();
					writer.close();
				} catch (Exception e) {
					log.error("Problem while converting to PDF due to: ", e);
				}
				input = output;
			}
			// end convert to PDF

		String fileEncoded = "";
		try {
			fileEncoded = encodeFileToBase64Binary(input);
		} catch (IOException ex) {
			log.error("Fail to encode the file due to: ", ex);
		}

		// make sure to delete the file
		if (!output.equals("")) {
			File file = new File(output);
			if (file.delete()) {
				log.debug("File with name" + file.getName() + " is deleted successfully");
			} else {
				log.debug("For file with name" + file.getName() + ", delete operation is failed");
			}
		}
			
			// construct SOAP message
	    	   SOAPMessage message = MessageFactory.newInstance().createMessage();
	    	   SOAPPart part = message.getSOAPPart();
	    	   SOAPEnvelope envelope = part.getEnvelope();
	    	   envelope.addNamespaceDeclaration("xsd", "http://www.w3.org/2001/XMLSchema");
	    	   SOAPBody body = envelope.getBody();
	    	   SOAPBodyElement checkIDDocument = body.addBodyElement(envelope.createName("checkIDDocument"));
	    	   SOAPElement requestParams = checkIDDocument.addChildElement("RequestParams");
	    	   requestParams.addAttribute(envelope.createName("href"), "#1");
	    	   SOAPElement iDDocImage = checkIDDocument.addChildElement("IDDocImage");
	    	   iDDocImage.addTextNode(fileEncoded);
	    	   SOAPElement tRequestParams = body.addChildElement("TRequestParams");
	    	   tRequestParams.addAttribute(envelope.createName("id"), "1");
	    	   SOAPElement accountname = tRequestParams.addChildElement("Accountname");
	    	   accountname.addTextNode("AnyOption");
	    	   SOAPElement Username = tRequestParams.addChildElement("Username");
	    	   Username.addTextNode(CommonUtil.getProperty("keesing.username"));
	    	   SOAPElement accountCountry = tRequestParams.addChildElement("AccountCountry");
	    	   accountCountry.addTextNode("BG");
	    	   SOAPElement number = tRequestParams.addChildElement("Number");
	    	   number.addTextNode(CommonUtil.getProperty("keesing.password"));
	    	   SOAPElement checkKMAR = tRequestParams.addChildElement("CheckKMAR");
	    	   checkKMAR.addTextNode("false");
	    	   SOAPElement checkVIS = tRequestParams.addChildElement("CheckVIS");
	    	   checkVIS.addTextNode("false");
	    	   SOAPElement checkNrOfPages = tRequestParams.addChildElement("CheckNrOfPages");
	    	   checkNrOfPages.addTextNode("false");
	    	   SOAPElement checkBWCopy = tRequestParams.addChildElement("CheckBWCopy");
	    	   checkBWCopy.addTextNode("false");
	    	   message.saveChanges();
	           
	    	   return message;
	    	}
	 
	private static String encodeFileToBase64Binary(String fileFullPath) throws IOException {
		File originalFile = new File(fileFullPath);
		String encodedBase64 = null;
		
		try {
			FileInputStream fileInputStreamReader = new FileInputStream(originalFile);
			byte[] bytes = new byte[(int) originalFile.length()];
			fileInputStreamReader.read(bytes);
			encodedBase64 = new String(Base64.getEncoder().encode(bytes));
			fileInputStreamReader.close();
		} catch (FileNotFoundException e) {
			log.error("Can not encode file with filepath: " + fileFullPath + "due to: ", e);
		} catch (IOException e) {
			log.error("Can not encode file with filepath: " + fileFullPath + "due to: ", e);
		}
		
		return encodedBase64;
	}
		
	private static void getKeesingSoapResponse(SOAPMessage soapResponse, KeesingResponse response) throws Exception {
		
		Iterator itr = soapResponse.getSOAPBody().getChildElements();
		
		String xml = "";
		while (itr.hasNext()) {
			Node node = (Node) itr.next();
			if (node.getNodeType() == Node.ELEMENT_NODE) {
				Element ele = (Element) node;
				xml = ele.getTextContent();
			}
		}

		xml = xml.replace("\n", "");
		xml = xml.trim();
		
		log.debug("Keesing SOAP response XML: " + xml + "with FileId: " + response.getFileId());
		// set XML response
		response.setXml(xml);

		Document doc = ClearingUtil.parseXMLToDocument(xml);
		doc.getDocumentElement().normalize();

		String checkStatusValue = "";
		String checkStatusText = "";
		NodeList nList = doc.getElementsByTagName("checkstatus");
		for (int temp = 0; temp < nList.getLength(); temp++) {
			org.w3c.dom.Node nNode = nList.item(temp);
			if (nNode.getNodeType() == Node.ELEMENT_NODE) {
				Element eElement = (Element) nNode;
				checkStatusValue = eElement.getElementsByTagName("value").item(0).getTextContent();
				checkStatusText = eElement.getElementsByTagName("text").item(0).getTextContent();
			}
		}
		
		response.setCheckStatusValue(Long.valueOf(checkStatusValue));
		response.setCheckStatusText(checkStatusText);

		String reasonNotOkValue = "";
		String reasonNotOkText = "";
		NodeList nListReason = doc.getElementsByTagName("reasonnotok");
		for (int temp = 0; temp < nListReason.getLength(); temp++) {
			org.w3c.dom.Node nNode = nListReason.item(temp);
			if (nNode.getNodeType() == Node.ELEMENT_NODE) {
				Element eElement = (Element) nNode;
				reasonNotOkValue = eElement.getElementsByTagName("value").item(0).getTextContent();
				reasonNotOkText = eElement.getElementsByTagName("text").item(0).getTextContent();
			}
		}
		
		response.setReasonNotOkValue(Long.valueOf(reasonNotOkValue));
		response.setReasonNotOkText(reasonNotOkText);

		String persID = "";
		String lastname = "";
		String firstnames = "";
		String nationality = "";
		String documentNr = "";
		String expireDate = "";
		String issuingCountry = "";
		String docID = "";
		String dateOfBirth = "";
		String gender = "";
		NodeList nListRecord = doc.getElementsByTagName("record");
		for (int temp = 0; temp < nListRecord.getLength(); temp++) {
			org.w3c.dom.Node nNode = nListRecord.item(temp);
			if (nNode.getNodeType() == Node.ELEMENT_NODE) {
				Element eElement = (Element) nNode;
				persID = eElement.getElementsByTagName("PersID").item(0).getTextContent();
				lastname = eElement.getElementsByTagName("Lastname").item(0).getTextContent();
				firstnames = eElement.getElementsByTagName("Firstnames").item(0).getTextContent();
				nationality = eElement.getElementsByTagName("Nationality").item(0).getTextContent();
				documentNr = eElement.getElementsByTagName("DocumentNr").item(0).getTextContent();
				expireDate = eElement.getElementsByTagName("ExpireDate").item(0).getTextContent();
				issuingCountry = eElement.getElementsByTagName("IssuingCountry").item(0).getTextContent();
				docID = eElement.getElementsByTagName("DocID").item(0).getTextContent();
				dateOfBirth = eElement.getElementsByTagName("DateOfBirth").item(0).getTextContent();
				gender = eElement.getElementsByTagName("Gender").item(0).getTextContent();
			}
		}
		
		if (!persID.equals("")) {
			response.setPersID(Long.valueOf(persID));
		}
		response.setLastname(lastname);
		response.setFirstnames(firstnames);
		response.setNationality(nationality);
		response.setDocumentNr(documentNr);
		response.setExpireDate(convertStringToJavaSqlDate(expireDate));
		response.setIssuingCountry(issuingCountry);
		if (!docID.equals("")) {
			response.setDocID(Long.valueOf(docID));
		}
		response.setDateOfBirth(convertStringToJavaSqlDate(dateOfBirth));
		response.setGender(gender);

	}

	private static Date convertStringToJavaSqlDate(String dateInString) {
		if (dateInString.equals("") || dateInString.equals("00000000")) {
			return null;
		}
		SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd");
		java.util.Date date = null;
		try {

			date = formatter.parse(dateInString);

		} catch (ParseException e) {
			log.error("Can not convert string: " + dateInString + "to java.sql.Date");
		}
		Date sqlDate = new Date(date.getTime());
		return sqlDate;
	}
	
}
