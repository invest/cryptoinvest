package com.anyoption.backend.service.promotionmanagement;

import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import com.anyoption.backend.managers.PromotionManagementManager;
import com.anyoption.backend.managers.WriterPermisionsManager;
import com.anyoption.common.backend.annotations.BackendPermission;
import com.anyoption.common.bl_vos.PromotionBannerSlider;
import com.anyoption.common.bl_vos.PromotionBannerSliderImage;
import com.anyoption.common.managers.LanguagesManagerBase;
import com.anyoption.common.service.CommonJSONService;
import com.anyoption.common.service.requests.UserMethodRequest;
import com.anyoption.common.service.results.MethodResult;

import il.co.etrader.backend.bl_vos.WriterWrapper;
import il.co.etrader.util.ConstantsBase;

public class PromotionManagementServices {
	private static final Logger log = Logger.getLogger(PromotionManagementServices.class);
	private static final String SCREEN_NAME_PROMOTION_MANAGEMENT = "promotionManagement";
	
	@BackendPermission(id = "promotionManagement_view")
	public static PromotionManagementFiltersResponse getPromotionManagementScreenSettings(UserMethodRequest request, HttpServletRequest httpRequest) {
		PromotionManagementFiltersResponse response = new PromotionManagementFiltersResponse();
		HashMap<String, HashMap<Long, String>> filters = new HashMap<>();
		
		WriterPermisionsManager.setWriterScreenPermissionsByScreenName(SCREEN_NAME_PROMOTION_MANAGEMENT, response, httpRequest);

		HashMap<Long, String> bannersList = PromotionManagementManager.getBannersList();
		filters.put(PromotionManagementManager.FILTER_BANNERS, bannersList);
		
		HashMap<Long, String> bannerSliderTypesList = PromotionManagementManager.getBannerSliderTypes();
		filters.put(PromotionManagementManager.FILTER_BANNER_SLIDER_TYPES, bannerSliderTypesList);

		HashMap<Long, String> isActiveList = PromotionManagementManager.getIsActive();
		filters.put(PromotionManagementManager.FILTER_IS_ACTIVE, isActiveList);
		
		response.setLanguages(LanguagesManagerBase.getLanguages());
		response.setScreenFilters(filters);
		return response;
	}
	
	@BackendPermission(id = "promotionManagement_view")
	public static PromotionBannerSlidersGetAllMethodResponse getPromotionBannerSliders(PromotionBannerSlidersGetAllMethodRequest request) {
		PromotionBannerSlidersGetAllMethodResponse response = new PromotionBannerSlidersGetAllMethodResponse();
		if (request == null) {
			response.setErrorCode(CommonJSONService.ERROR_CODE_INVALID_INPUT);
			return response;
		}
		ArrayList<PromotionBannerSlider> bannerSliders = PromotionManagementManager.getBannerSliders(request, response);
		if (bannerSliders == null) {
			return response;
		}
		response.setBannerSliders(bannerSliders);
		return response;
	}
	
	@BackendPermission(id = "promotionManagement_view")
	public static MethodResult insertPromotionBannerSlider(PromotionBannerSliderInsertUpdateRequest request, HttpServletRequest httpRequest) {
		MethodResult response = new MethodResult();
		WriterWrapper writer = (WriterWrapper) httpRequest.getSession().getAttribute(ConstantsBase.BIND_SESSION_WRITER_WRAPPER);
		long writerId = writer.getWriter().getId();
		if (request == null) {
			response.setErrorCode(CommonJSONService.ERROR_CODE_INVALID_INPUT);
			return response;
		}
		request.setWriterId(writerId);
		PromotionManagementManager.insertBannerSliderImages(request, response);		
		return response;
	}
	
	@BackendPermission(id = "promotionManagement_view")
	public static MethodResult deletePromotionBannerSlider(PromotionBannerSliderInsertUpdateRequest request, HttpServletRequest httpRequest) {
		MethodResult response = new MethodResult();
		WriterWrapper writer = (WriterWrapper) httpRequest.getSession().getAttribute(ConstantsBase.BIND_SESSION_WRITER_WRAPPER);
		long writerId = writer.getWriter().getId();
		if (request == null) {
			response.setErrorCode(CommonJSONService.ERROR_CODE_INVALID_INPUT);
			return response;
		}
		request.setWriterId(writerId);
		PromotionManagementManager.deleteBannerSliders(request, response);	
		return response;
	}
	
	@BackendPermission(id = "promotionManagement_view")
	public static MethodResult disablePromotionBannerSlider(PromotionBannerSliderInsertUpdateRequest request, HttpServletRequest httpRequest) {
		MethodResult response = new MethodResult();
		WriterWrapper writer = (WriterWrapper) httpRequest.getSession().getAttribute(ConstantsBase.BIND_SESSION_WRITER_WRAPPER);
		long writerId = writer.getWriter().getId();
		if (request == null) {
			response.setErrorCode(CommonJSONService.ERROR_CODE_INVALID_INPUT);
			return response;
		}
		request.setWriterId(writerId);
		PromotionManagementManager.disableBannerSliders(request, response);	
		return response;
	}
	
	@BackendPermission(id = "promotionManagement_view")
	public static MethodResult enablePromotionBannerSlider(PromotionBannerSliderInsertUpdateRequest request, HttpServletRequest httpRequest) {
		MethodResult response = new MethodResult();
		WriterWrapper writer = (WriterWrapper) httpRequest.getSession().getAttribute(ConstantsBase.BIND_SESSION_WRITER_WRAPPER);
		long writerId = writer.getWriter().getId();
		if (request == null) {
			response.setErrorCode(CommonJSONService.ERROR_CODE_INVALID_INPUT);
			return response;
		}
		request.setWriterId(writerId);
		PromotionManagementManager.enableBannerSliders(request, response);
		return response;
	}
	
	@BackendPermission(id = "promotionManagement_view")
	public static PromotionBannerSlidersGetImageResponse getPromotionBannerSliderImages(PromotionBannerSliderInsertUpdateRequest request) {
		PromotionBannerSlidersGetImageResponse response = new PromotionBannerSlidersGetImageResponse();
		ArrayList<PromotionBannerSliderImage> sliderImages = new ArrayList<PromotionBannerSliderImage>();
		PromotionBannerSlider slider = new PromotionBannerSlider();
		if (request == null) {
			response.setErrorCode(CommonJSONService.ERROR_CODE_INVALID_INPUT);
			return response;
		}
		HashMap<PromotionBannerSlider, ArrayList<PromotionBannerSliderImage>> hm = PromotionManagementManager.getBannerSliderImages(request, response);
		sliderImages = hm.values().iterator().next();
		slider = hm.keySet().iterator().next();
		
		response.setBannerId(slider.getBannerId());
		response.setBannerSliderImages(sliderImages);
		response.setLinkTypeId(slider.getLinkTypeId());
		return response;
	}
	
	@BackendPermission(id = "promotionManagement_view")
	public static PromotionBannerSlidersBankResponse getBannerSliderImageFromBank(UserMethodRequest request) {
		PromotionBannerSlidersBankResponse response = new PromotionBannerSlidersBankResponse();
		if (request == null) {
			response.setErrorCode(CommonJSONService.ERROR_CODE_INVALID_INPUT);
			return response;
		}
		ArrayList<String> imagesFromBank = PromotionManagementManager.getBannerSliderImagesFromBank();
		response.setImagesFromBank(imagesFromBank);
		return response;
	}
	
	@BackendPermission(id = "promotionManagement_view")
	public static MethodResult updateBannerSliderImages(PromotionBannerSliderInsertUpdateRequest request, HttpServletRequest httpRequest) {
		MethodResult response = new MethodResult();
		WriterWrapper writer = (WriterWrapper) httpRequest.getSession().getAttribute(ConstantsBase.BIND_SESSION_WRITER_WRAPPER);
		long writerId = writer.getWriter().getId();
		if (request == null) {
			response.setErrorCode(CommonJSONService.ERROR_CODE_INVALID_INPUT);
			return response;
		}
		request.setWriterId(writerId);
		PromotionManagementManager.updateBannerSliderImages(request, response);
		return response;
	}
	
	@BackendPermission(id = "promotionManagement_view")
	public static MethodResult updatePromotionBannerSliderPosition(PromotionBannerSliderUpdatePositionRequest request, HttpServletRequest httpRequest) {
		MethodResult response = new MethodResult();
		WriterWrapper writer = (WriterWrapper) httpRequest.getSession().getAttribute(ConstantsBase.BIND_SESSION_WRITER_WRAPPER);
		long writerId = writer.getWriter().getId();
		if (request == null) {
			response.setErrorCode(CommonJSONService.ERROR_CODE_INVALID_INPUT);
			return response;
		}
		request.setWriterId(writerId);
		PromotionManagementManager.updateBannerSliderPosition(request, response);	
		return response;
	}
	
}
