package com.anyoption.backend.service.promotionmanagement;

import java.util.ArrayList;

import com.anyoption.common.bl_vos.PromotionBannerSlider;
import com.anyoption.common.service.results.MethodResult;

public class PromotionBannerSlidersGetAllMethodResponse extends MethodResult{
	
	private ArrayList<PromotionBannerSlider> bannerSliders;

	public ArrayList<PromotionBannerSlider> getBannerSliders() {
		return bannerSliders;
	}

	public void setBannerSliders(ArrayList<PromotionBannerSlider> bannerSliders) {
		this.bannerSliders = bannerSliders;
	}
	
	

}
