package com.anyoption.backend.service.promotionmanagement;

import java.util.ArrayList;

import com.anyoption.common.bl_vos.PromotionBannerSliderImage;
import com.anyoption.common.service.results.MethodResult;

public class PromotionBannerSlidersGetImageResponse extends MethodResult{
	private ArrayList<PromotionBannerSliderImage> sliderImages;
	private long linkTypeId;
	private long bannerId;

	public ArrayList<PromotionBannerSliderImage> getBannerSliderImages() {
		return sliderImages;
	}

	public void setBannerSliderImages(ArrayList<PromotionBannerSliderImage> sliderImages) {
		this.sliderImages = sliderImages;
	}

	public long getLinkTypeId() {
		return linkTypeId;
	}

	public void setLinkTypeId(long linkTypeId) {
		this.linkTypeId = linkTypeId;
	}

	public long getBannerId() {
		return bannerId;
	}

	public void setBannerId(long bannerId) {
		this.bannerId = bannerId;
	}
	
	
}
