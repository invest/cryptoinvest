package com.anyoption.backend.service.promotionmanagement;

import java.util.Hashtable;

import com.anyoption.backend.service.writerpermissions.ScreenSettingsResponse;
import com.anyoption.common.beans.base.Language;

public class PromotionManagementFiltersResponse extends ScreenSettingsResponse {
	private Hashtable<Long, Language> languages;
	private Hashtable<Long, Language> linkTypes;

	public Hashtable<Long, Language> getLanguages() {
		return languages;
	}

	public void setLanguages(Hashtable<Long, Language> languages) {
		this.languages = languages;
	}

	public Hashtable<Long, Language> getLinkTypes() {
		return linkTypes;
	}

	public void setLinkTypes(Hashtable<Long, Language> linkTypes) {
		this.linkTypes = linkTypes;
	}
	
}
