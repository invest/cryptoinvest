package com.anyoption.backend.service.promotionmanagement;

import com.anyoption.common.service.requests.UserMethodRequest;

public class PromotionBannerSlidersGetAllMethodRequest extends UserMethodRequest{
	
	private Long bannerId;
	private Long isActive;
	
	public Long getBannerId() {
		return bannerId;
	}
	public void setBannerId(Long bannerId) {
		this.bannerId = bannerId;
	}
	public Long isActive() {
		return isActive;
	}
	public void setActive(Long isActive) {
		this.isActive = isActive;
	}

}
