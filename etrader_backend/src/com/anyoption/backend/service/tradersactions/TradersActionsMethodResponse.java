package com.anyoption.backend.service.tradersactions;

import java.util.ArrayList;

import com.anyoption.common.service.results.MethodResult;

import il.co.etrader.backend.bl_vos.TradersActions;

public class TradersActionsMethodResponse extends MethodResult {
	
private ArrayList<TradersActions> tradersActions;
	
	public ArrayList<TradersActions> getTradersActions() {
		return tradersActions;
	}

	public void setTradersActions(ArrayList<TradersActions> tradersActions) {
		this.tradersActions = tradersActions;
	}
}
