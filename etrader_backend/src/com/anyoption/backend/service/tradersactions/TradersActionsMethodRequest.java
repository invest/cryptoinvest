package com.anyoption.backend.service.tradersactions;

import com.anyoption.common.service.requests.MethodRequest;

public class TradersActionsMethodRequest extends MethodRequest {

	private Long actionId;
	private Long assetId;
	private Long traderId;
	private long fromDate;
	private long toDate;
	private long pageNumber;
	private long rowsPerPage;
	private long column;
	private long orderType;
	
	public Long getActionId() {
		return actionId;
	}

	public void setActionId(Long actionId) {
		this.actionId = actionId;
	}

	public Long getAssetId() {
		return assetId;
	}

	public void setAssetId(Long assetId) {
		this.assetId = assetId;
	}

	public long getFromDate() {
		return fromDate;
	}

	public void setFromDate(long fromDate) {
		this.fromDate = fromDate;
	}

	public long getToDate() {
		return toDate;
	}

	public void setToDate(long toDate) {
		this.toDate = toDate;
	}

	public long getWriterId() {
		return writerId;
	}

	public void setWriterId(long writerId) {
		this.writerId = writerId;
	}

	public long getSkinId() {
		return skinId;
	}

	public void setSkinId(long skinId) {
		this.skinId = skinId;
	}

	public long getPageNumber() {
		return pageNumber;
	}

	public void setPageNumber(long pageNumber) {
		this.pageNumber = pageNumber;
	}

	public long getRowsPerPage() {
		return rowsPerPage;
	}

	public void setRowsPerPage(long rowsPerPage) {
		this.rowsPerPage = rowsPerPage;
	}

	public long getColumn() {
		return column;
	}

	public void setColumn(long column) {
		this.column = column;
	}

	public long getOrderType() {
		return orderType;
	}

	public void setOrderType(long orderType) {
		this.orderType = orderType;
	}

	public Long getTraderId() {
		return traderId;
	}

	public void setTraderId(Long traderId) {
		this.traderId = traderId;
	}		
}
