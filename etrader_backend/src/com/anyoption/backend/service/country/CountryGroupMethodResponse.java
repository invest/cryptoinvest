package com.anyoption.backend.service.country;

import il.co.etrader.bl_vos.CountryGroup;

import java.util.ArrayList;

import com.anyoption.common.service.results.MethodResult;

public class CountryGroupMethodResponse extends MethodResult {
	private ArrayList<CountryGroup> countryGroups;

	/**
	 * @return the countryGroups
	 */
	public ArrayList<CountryGroup> getCountryGroups() {
		return countryGroups;
	}

	/**
	 * @param countryGroups the countryGroups to set
	 */
	public void setCountryGroups(ArrayList<CountryGroup> countryGroups) {
		this.countryGroups = countryGroups;
	}
}
