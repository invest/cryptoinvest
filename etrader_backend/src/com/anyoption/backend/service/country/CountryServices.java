package com.anyoption.backend.service.country;

import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import com.anyoption.backend.managers.CountryManager;
import com.anyoption.backend.managers.WriterPermisionsManager;
import com.anyoption.backend.service.writerpermissions.ScreenSettingsResponse;
import com.anyoption.common.backend.annotations.BackendPermission;
import com.anyoption.common.service.CommonJSONService;
import com.anyoption.common.service.requests.UserMethodRequest;
import com.anyoption.common.service.results.MethodResult;

import il.co.etrader.backend.bl_vos.WriterWrapper;
import il.co.etrader.bl_vos.CountryGroup;
import il.co.etrader.util.ConstantsBase;

/**
 * 
 * @author eyal.ohana
 *
 */
public class CountryServices {
	private static final Logger log = Logger.getLogger(CountryServices.class);
	private static final String SCREEN_NAME_COUNTRY = "countries";
	
	/**
	 * Get country screen settings
	 * @param request
	 * @param httpRequest
	 * @return ScreenSettingsResponse
	 */
	public static ScreenSettingsResponse getCountryScreenSettings(UserMethodRequest request, HttpServletRequest httpRequest) {
		ScreenSettingsResponse response = new ScreenSettingsResponse();
		HashMap<String, HashMap<Long, String>> filters = new HashMap<>();
		WriterPermisionsManager.setWriterScreenPermissionsByScreenName(SCREEN_NAME_COUNTRY, response, httpRequest);
		//Countries group types
		HashMap<Long, String> countriesGroupTypes = CountryManager.getCountriesGroupTypes();
		filters.put(CountryManager.FILTER_COUNTRIES_GROUP_TYPES, countriesGroupTypes);
		
		//Countries group tiers
		HashMap<Long, String> countriesGroupTiers = CountryManager.getCountriesGroupTiers();
		filters.put(CountryManager.FILTER_COUNTRIES_GROUP_TIERS, countriesGroupTiers);
		
		//Countries
		HashMap<Long, String> countries = CountryManager.getCountries();
		filters.put(CountryManager.FILTER_COUNTRIES, countries);
		
		response.setScreenFilters(filters);
		return response;
	}
	
	@BackendPermission(id = "countryGroup_view")
	public static CountryGroupMethodResponse getCountryGroups(UserMethodRequest request) {
		CountryGroupMethodResponse response = new CountryGroupMethodResponse();
		ArrayList<CountryGroup> countryGroups = CountryManager.getCountryGroups(response);
		response.setCountryGroups(countryGroups);
		return response;
	}
	
	@BackendPermission(id = "countryGroup_view")
	public static MethodResult updateCountryGroup(CountryGroupMethodRequest request, HttpServletRequest httpRequest) {
		MethodResult response = new MethodResult();
		WriterWrapper writer = (WriterWrapper) httpRequest.getSession().getAttribute(ConstantsBase.BIND_SESSION_WRITER_WRAPPER);
		long writerId = writer.getWriter().getId();
		if (request == null) {
			response.setErrorCode(CommonJSONService.ERROR_CODE_INVALID_INPUT);
			return response;
		}
		request.setWriterId(writerId);
		CountryManager.updateCountryGroup(request, response);
		return response;
	}
}
