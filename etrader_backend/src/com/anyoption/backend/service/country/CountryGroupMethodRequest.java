package com.anyoption.backend.service.country;

import com.anyoption.common.service.requests.UserMethodRequest;

import il.co.etrader.bl_vos.CountryGroup;

public class CountryGroupMethodRequest extends UserMethodRequest {
	private CountryGroup countryGroup;

	/**
	 * @return the countryGroup
	 */
	public CountryGroup getCountryGroup() {
		return countryGroup;
	}

	/**
	 * @param countryGroup the countryGroup to set
	 */
	public void setCountryGroup(CountryGroup countryGroup) {
		this.countryGroup = countryGroup;
	}
	
}
