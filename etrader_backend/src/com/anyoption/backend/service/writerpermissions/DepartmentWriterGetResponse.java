/**
 * 
 */
package com.anyoption.backend.service.writerpermissions;

import java.util.ArrayList;

import com.anyoption.common.service.results.MethodResult;

import il.co.etrader.bl_vos.WriterBase;

public class DepartmentWriterGetResponse extends MethodResult {
	
	private ArrayList<WriterBase> writers;
	
	public ArrayList<WriterBase> getWriters() {
		return writers;
	}
	public void setWriters(ArrayList<WriterBase> writers) {
		this.writers = writers;
	}		
}
