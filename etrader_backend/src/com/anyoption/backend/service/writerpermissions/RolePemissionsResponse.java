/**
 * 
 */
package com.anyoption.backend.service.writerpermissions;

import java.util.ArrayList;

import com.anyoption.common.service.results.MethodResult;

import il.co.etrader.backend.bl_vos.ExistWriterRole;

public class RolePemissionsResponse extends MethodResult {
	
	private ArrayList<ExistWriterRole> existWriterRoleList;

	public ArrayList<ExistWriterRole> getExistWriterRoleList() {
		return existWriterRoleList;
	}

	public void setExistWriterRoleList(ArrayList<ExistWriterRole> existWriterRoleList) {
		this.existWriterRoleList = existWriterRoleList;
	}
	
}
