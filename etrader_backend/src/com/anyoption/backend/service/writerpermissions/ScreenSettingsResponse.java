/**
 * 
 */
package com.anyoption.backend.service.writerpermissions;

import java.util.Collection;
import java.util.HashMap;

import com.anyoption.common.beans.base.Currency;

public class ScreenSettingsResponse extends PermissionsGetResponse {

	private HashMap<String, HashMap<Long, String>> screenFilters;
	private Collection<Currency> currencies;
	private boolean depositDisabled;

	public Collection<Currency> getCurrencies() {
		return currencies;
	}

	public void setCurrencies(Collection<Currency> currencies) {
		this.currencies = currencies;
	}

	public HashMap<String, HashMap<Long, String>> getScreenFilters() {
		return screenFilters;
	}

	public void setScreenFilters(HashMap<String, HashMap<Long, String>> screenFilters) {
		this.screenFilters = screenFilters;
	}

	public boolean isDepositDisabled() {
		return depositDisabled;
	}

	public void setDepositDisabled(boolean depositDisabled) {
		this.depositDisabled = depositDisabled;
	}
}