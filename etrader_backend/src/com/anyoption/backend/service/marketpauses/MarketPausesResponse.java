package com.anyoption.backend.service.marketpauses;

import java.util.ArrayList;

import com.anyoption.backend.service.writerpermissions.PermissionsGetResponse;
import com.anyoption.common.beans.MarketPauseDetails;

public class MarketPausesResponse extends PermissionsGetResponse {
	
	private ArrayList<MarketPauseDetails> marketPauses;

	public ArrayList<MarketPauseDetails> getMarketPauses() {
		return marketPauses;
	}

	public void setMarketPauses(ArrayList<MarketPauseDetails> marketPauses) {
		this.marketPauses = marketPauses;
	}
}
