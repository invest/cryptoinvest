package com.anyoption.backend.service.marketpauses;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import com.anyoption.backend.managers.WriterPermisionsManager;
import com.anyoption.common.backend.annotations.BackendPermission;
import com.anyoption.common.beans.MarketPauseDetails;
import com.anyoption.common.beans.Opportunity;
import com.anyoption.common.beans.base.Skin;
import com.anyoption.common.managers.MarketsManagerBase;
import com.anyoption.common.service.CommonJSONService;
import com.anyoption.common.service.assetindex.MarketRequest;
import com.anyoption.common.service.requests.UserMethodRequest;
import com.anyoption.common.service.results.MethodResult;

import il.co.etrader.bl_managers.InvestmentsManagerBase;
import il.co.etrader.util.CommonUtil;

public class MarketPausesServices {
	
	private static final String SCREEN_NAME_PERMISSIONS_MARKET_PAUSES = "marketPauses";
	private static final Logger log = Logger.getLogger(MarketPausesServices.class);

	@BackendPermission(id = "marketPauses_view")
	public static MarketPausesFiltersResponse getMarketPausesScreenSettings(UserMethodRequest request,
																					HttpServletRequest httpRequest) {
		log.debug("getMarketPausesScreenSettings: " + request);
		MarketPausesFiltersResponse response = new MarketPausesFiltersResponse();
		WriterPermisionsManager.setWriterScreenPermissionsByScreenName(SCREEN_NAME_PERMISSIONS_MARKET_PAUSES,
				response, httpRequest);
		try {
			Map<Long, Map<Long, String>> map = MarketsManagerBase.getMarketNamesByTypeAndSkin(Skin.SKIN_REG_EN);
			Map<String, Map<Long, Map<Long, String>>> mapFilters = new HashMap<String, Map<Long, Map<Long, String>>>();
			if (map != null) {
				mapFilters.put(MarketsManagerBase.FILTER_MARKETS, map);
			}
			response.setFilters(mapFilters);
			Map<Long, String> scheduledMap = new LinkedHashMap<Long, String>();
			// TODO switch constants to Opportunity ones
			scheduledMap.put((long) InvestmentsManagerBase.SCHEDULED_HOURLY, 
													CommonUtil.getMessage("opportunities.templates.hourly", null));
			scheduledMap.put((long) InvestmentsManagerBase.SCHEDULED_DAILY, 
													CommonUtil.getMessage("opportunities.templates.daily", null));
			scheduledMap.put((long) InvestmentsManagerBase.SCHEDULED_WEEKLY, 
													CommonUtil.getMessage("opportunities.templates.weekly", null));
			scheduledMap.put((long) InvestmentsManagerBase.SCHEDULED_MONTHLY, 
													CommonUtil.getMessage("opportunities.templates.monthly", null));
			scheduledMap.put((long) InvestmentsManagerBase.SCHEDULED_ONE_TOUCH, 
													CommonUtil.getMessage("opportunities.templates.oneTouch", null));
			scheduledMap.put((long) Opportunity.SCHEDULED_QUARTERLY, CommonUtil.getMessage("opportunities.templates.quarterly", null));
			response.setScheduled(scheduledMap);
		} catch (SQLException sqle) {
			log.error("Unable to load filters for Market pauses! ", sqle);
			response.setErrorCode(CommonJSONService.ERROR_CODE_UNKNOWN);
		}
		return response;
	}
	
	@BackendPermission(id = "marketPauses_view")
	public static MarketPausesResponse getMarketPausesByMarketId(MarketRequest request) {
		log.debug("getMarketPausesByMarketId: " + request);
		MarketPausesResponse response = new MarketPausesResponse();
		try {
			ArrayList<MarketPauseDetails> marketPauses = MarketsManagerBase.
																getMarketPausesByMarketId(request.getMarketId());
			response.setMarketPauses(marketPauses);
		} catch (SQLException sqle) {
			log.error("Unable to load market pauses by marketId! ", sqle);
			response.setErrorCode(CommonJSONService.ERROR_CODE_UNKNOWN);
		}
		return response;
	}
	
	@BackendPermission(id = "marketPauses_view")
	public static MarketPausesResponse getMarketPausesByMarketPauseId(MarketPausesRequest request) {
		log.debug("getMarketPausesByMarketPauseId: " + request);
		MarketPausesResponse response = new MarketPausesResponse();
		try {
			MarketPauseDetails marketPause = MarketsManagerBase.getMarketPauseById(
																	request.getMarketPauseDetails().getMarketPauseId());
			ArrayList<MarketPauseDetails> marketPauses = new ArrayList<MarketPauseDetails>();
			marketPauses.add(marketPause);
			response.setMarketPauses(marketPauses);
		} catch (SQLException sqle) {
			log.error("Unable to load market pause by id! ", sqle);
			response.setErrorCode(CommonJSONService.ERROR_CODE_UNKNOWN);
		}
		return response;
	}
	
	@BackendPermission(id = "marketPauses_view")
	public static MethodResult editInsertMarketPause(MarketPausesRequest request) {
		log.debug("editInsertMarketPause: " + request);
		MethodResult result = new MethodResult();
		try {
			MarketsManagerBase.updateInsertMarketPause(request.getMarketPauseDetails());
		} catch (SQLException sqle) {
			log.error("Unable to update or insert market pause! ", sqle);
			result.setErrorCode(CommonJSONService.ERROR_CODE_UNKNOWN);
		}
		return result;
	}
}
