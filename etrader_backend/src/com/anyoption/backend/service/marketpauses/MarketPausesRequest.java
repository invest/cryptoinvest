package com.anyoption.backend.service.marketpauses;

import com.anyoption.common.beans.MarketPauseDetails;
import com.anyoption.common.service.requests.MethodRequest;

public class MarketPausesRequest extends MethodRequest {
	
	protected MarketPauseDetails marketPauseDetails;

	public MarketPauseDetails getMarketPauseDetails() {
		return marketPauseDetails;
	}

	public void setMarketPauseDetails(MarketPauseDetails marketPauseDetails) {
		this.marketPauseDetails = marketPauseDetails;
	} 
}
