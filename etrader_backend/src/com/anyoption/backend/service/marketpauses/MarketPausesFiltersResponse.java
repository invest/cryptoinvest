package com.anyoption.backend.service.marketpauses;

import java.util.Map;

import com.anyoption.backend.service.createmarket.CopyMarketResponse;

public class MarketPausesFiltersResponse extends CopyMarketResponse {

	protected Map<Long, String> scheduled;

	public Map<Long, String> getScheduled() {
		return scheduled;
	}

	public void setScheduled(Map<Long, String> scheduled) {
		this.scheduled = scheduled;
	}
}
