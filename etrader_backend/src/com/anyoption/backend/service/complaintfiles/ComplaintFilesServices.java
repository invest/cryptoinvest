package com.anyoption.backend.service.complaintfiles;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import com.anyoption.backend.managers.WriterPermisionsManager;
import com.anyoption.backend.service.files.FileRequest;
import com.anyoption.backend.service.files.FilesServices;
import com.anyoption.backend.service.files.UserIdRequest;
import com.anyoption.backend.service.writerpermissions.ScreenSettingsResponse;
import com.anyoption.common.backend.annotations.BackendPermission;
import com.anyoption.common.beans.File;
import com.anyoption.common.beans.FileType;
import com.anyoption.common.managers.UsersManagerBase;
import com.anyoption.common.service.CommonJSONService;
import com.anyoption.common.service.requests.MethodRequest;
import com.anyoption.common.service.results.MethodResult;
import com.anyoption.common.service.userdocuments.UserDocumentsManager;
import com.anyoption.common.util.CommonUtil;

import il.co.etrader.backend.bl_managers.FilesManager;
import il.co.etrader.backend.bl_vos.WriterWrapper;
import il.co.etrader.util.ConstantsBase;

public class ComplaintFilesServices extends FilesServices{
	private static final Logger log = Logger.getLogger(ComplaintFilesServices.class);
	
	private static final String SCREEN_NAME_COMPLAINT_FILES = "complaintFiles";
	
	@BackendPermission(id = "complaintFiles_view")
	public static ScreenSettingsResponse getComplaintFilesScreenSettings(MethodRequest request, HttpServletRequest httpRequest) {
		log.debug("getComplaintFilesScreenSettings:" + request);		
		ScreenSettingsResponse response = new ScreenSettingsResponse();
	
		WriterPermisionsManager.setWriterScreenPermissionsByScreenName(SCREEN_NAME_COMPLAINT_FILES, response, httpRequest);

		return response;
	}
	
	@BackendPermission(id = "complaintFiles_view")
	public static ComplaintFilesResponse getAllComplaintFiles(UserIdRequest request, HttpServletRequest httpRequest) {
		log.debug("getAllComplaintFiles:" + request);		
		ComplaintFilesResponse response = new ComplaintFilesResponse();
		
		long userId = request.getUserId();
		List<File> allFiles = new ArrayList<File>();
		WriterWrapper writer = (WriterWrapper) httpRequest.getSession().getAttribute(ConstantsBase.BIND_SESSION_WRITER_WRAPPER);
		allFiles = UsersManagerBase.getUserFilesByType(userId, FileType.COMPLAINT_FILE, writer.getLocalInfo().getLocale());
		response.setAllFiles(allFiles);
		
		return response;
	}
	
	@BackendPermission(id = "complaintFiles_view")
	public static MethodResult insertFile(FileRequest request, HttpServletRequest httpRequest) {		
		log.debug("insertFile:" + request);
		WriterWrapper writer = (WriterWrapper) httpRequest.getSession().getAttribute(ConstantsBase.BIND_SESSION_WRITER_WRAPPER);
		long writerId = writer.getWriter().getId();
		MethodResult response = new MethodResult();
		
		if (request == null) {
			response.setErrorCode(CommonJSONService.ERROR_CODE_INVALID_INPUT);
			return response;
		}

		try {
			File file = request.getFile();
			file.setWriterId(writerId);
			file.setRejectReason(File.REJECT_REASON_NOT_REJECTED);
			file.setFileTypeId(FileType.COMPLAINT_FILE);
			if (CommonUtil.isParameterEmptyOrNull(file.getFileName())) {
				log.error("Missing file name!");
				response.setErrorCode(CommonJSONService.ERROR_CODE_MISSING_FILE_NAME);
				return response;
			}  else {
				long isInserted = FilesManager.insertFileBE(file);
				if (isInserted == 0) {
					response.setErrorCode(CommonJSONService.ERROR_CODE_INSERTING_FILE);
					return response;
				}
			}		
		} catch (Exception e) {
			log.error("Unable to insertFile: " + request + " " , e);
			response.setErrorCode(CommonJSONService.ERROR_CODE_UNKNOWN);
		}
		return response; 
	}
	
}
