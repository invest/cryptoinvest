package com.anyoption.backend.service.chargebacks;

import com.anyoption.common.service.requests.UserMethodRequest;

public class ChargebackRequest extends UserMethodRequest {

	private long transactionId;

	public long getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(long transactionId) {
		this.transactionId = transactionId;
	}
}