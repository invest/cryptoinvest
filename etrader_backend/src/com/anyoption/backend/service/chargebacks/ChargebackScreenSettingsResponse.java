package com.anyoption.backend.service.chargebacks;

import java.util.ArrayList;

import com.anyoption.backend.service.writerpermissions.ScreenSettingsResponse;

public class ChargebackScreenSettingsResponse extends ScreenSettingsResponse {
	
	ArrayList<ChargebackReasonCode> allReasonCodes;

	public ArrayList<ChargebackReasonCode> getAllReasonCodes() {
		return allReasonCodes;
	}

	public void setAllReasonCodes(ArrayList<ChargebackReasonCode> allReasonCodes) {
		this.allReasonCodes = allReasonCodes;
	}
}