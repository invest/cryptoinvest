package com.anyoption.backend.service.chargebacks;

import java.sql.SQLException;
import java.sql.SQLIntegrityConstraintViolationException;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import com.anyoption.backend.managers.TransactionManager;
import com.anyoption.backend.managers.WriterPermisionsManager;
import com.anyoption.common.backend.annotations.BackendPermission;
import com.anyoption.common.bl_vos.ChargeBack;
import com.anyoption.common.managers.ChargeBacksManagerBase;
import com.anyoption.common.managers.CountryManagerBase;
import com.anyoption.common.service.CommonJSONService;
import com.anyoption.common.service.requests.UserMethodRequest;
import com.anyoption.common.service.results.MethodResult;

import il.co.etrader.backend.bl_vos.WriterWrapper;
import il.co.etrader.bl_managers.SkinsManagerBase;
import il.co.etrader.util.ConstantsBase;

public class ChargebacksServices {
	
	private static final Logger logger = Logger.getLogger(ChargebacksServices.class);
	
	private static final String SCREEN_NAME_CHARGEBACKS = "chargebacks";
	
	@BackendPermission(id = "chargebacks_view")
	public static ChargebackScreenSettingsResponse getChargebacksScreenSettings(UserMethodRequest request,
											HttpServletRequest httpRequest) {
		ChargebackScreenSettingsResponse response = new ChargebackScreenSettingsResponse();
		HashMap<String, HashMap<Long, String>> filters = new HashMap<>();
		WriterWrapper writer = (WriterWrapper) httpRequest.getSession()
																.getAttribute(ConstantsBase.BIND_SESSION_WRITER_WRAPPER);

		WriterPermisionsManager.setWriterScreenPermissionsByScreenName(SCREEN_NAME_CHARGEBACKS, response, httpRequest);

		//FILTERS
		//Users
		HashMap<Long, String> usersClass = TransactionManager.getUserClasses();
		if (usersClass.containsKey(3L)) {	//API
			usersClass.remove(3L);
		}
		filters.put(TransactionManager.FILTER_CLASS_USERS, usersClass);
		
		//business case
		HashMap<Long, String> skinBusinessCases = TransactionManager.getSkinBusinessCases();
		filters.put(TransactionManager.FILTER_SKIN_BUSINESS_CASES, skinBusinessCases);
		
		//skins
		HashMap<Long, String> skins = WriterPermisionsManager.filterSkinsBasedOnWriterAttribution(
																	SkinsManagerBase.getAllSkinsFilter(), writer, logger);
		filters.put(SkinsManagerBase.FILTER_SKINS, skins);
		
		//countries
		HashMap<Long, String> countries = CountryManagerBase.getAllCountriesFilter();
		filters.put(CountryManagerBase.FILTER_COUNTRIES, countries);
		
		//currencies
		HashMap<Long, String> currencies = TransactionManager.getAllCurrencies();
		filters.put(TransactionManager.FILTER_CURRENCIES, currencies);
		
		//Chargeback statuses
		try {
			HashMap<Long,ChargeBack> statuses = ChargeBacksManagerBase.getAllStatuses();
			HashMap<Long, String> filterStatuses = new HashMap<Long, String>();
			for(Long key : statuses.keySet()) {
				filterStatuses.put(key, statuses.get(key).getStatusName());
			}
			filters.put(ChargeBacksManager.FILTER_STATUSES, filterStatuses);
		} catch (SQLException e) {
			logger.error("Error loading chargeback statuses : " + e);
		}
		
		//Chargeback providers
		HashMap<Long, String> providers = new HashMap<Long, String>();
		try {
			providers = ChargeBacksManager.getChargebackProviders();
		} catch (SQLException e) {
			logger.error("Error loading chargeback providers : " + e);
		}
		filters.put(ChargeBacksManager.FILTER_PROVIDERS, providers);
		
		//Chargeback Reason Codes
		ArrayList<ChargebackReasonCode> reasonCodes = new ArrayList<ChargebackReasonCode>();
		try {
			reasonCodes = ChargeBacksManager.getAllReasonCodes();
		} catch (SQLException e) {
			logger.error("Error loading chargeback reason codes : " + e);
		}

		response.setAllReasonCodes(reasonCodes);
		response.setScreenFilters(filters);
		return response;
	}
	
	@BackendPermission(id = "chargebacks_view")
	public static ChargebacksResponse getChargebacks(ChargebackFiltersRequest request, HttpServletRequest httpRequest) {
		ChargebacksResponse response = new ChargebacksResponse();
		try {
			WriterWrapper writerWrapper = (WriterWrapper) httpRequest.getSession().getAttribute(
																				ConstantsBase.BIND_SESSION_WRITER_WRAPPER);
			ArrayList<Integer> skinsList = request.getChargebackFilters().getSkins();
			skinsList = WriterPermisionsManager.setPermitedSkinsForWriter(skinsList, writerWrapper);
			request.getChargebackFilters().setSkins(skinsList);
			ArrayList<Chargeback> chargebacks = ChargeBacksManager.getChargebacks(request.getChargebackFilters());
			response.setChargebacksList(chargebacks);
		} catch (SQLException e) {
			logger.error("Unable to get chargebacks! ", e);
			response.setErrorCode(CommonJSONService.ERROR_CODE_UNKNOWN);
		}
		return response;
	}
	
	@BackendPermission(id = "chargebacks_view")
	public static ChargebackDetailsResponse getChargebackDetails(ChargebackRequest request) {
		ChargebackDetailsResponse response = new ChargebackDetailsResponse();
		try {
			ChargebackDetails chargebackDetails = ChargeBacksManager.getChargebackDetails(request.getTransactionId());
			response.setChargebackDetails(chargebackDetails);
		} catch (SQLException sqle) {
			logger.error("Unable to get chargeback details! ", sqle);
			response.setErrorCode(CommonJSONService.ERROR_CODE_UNKNOWN);
		}
		return response;
	}
	
	@BackendPermission(id = "chargebacks_add")
	public static MethodResult insertChargeback(ChargebackDetailsRequest request, HttpServletRequest httpRequest) {
		MethodResult result = new MethodResult();
		try {
			WriterWrapper writerWrapper = (WriterWrapper) httpRequest.getSession()
															.getAttribute(ConstantsBase.BIND_SESSION_WRITER_WRAPPER);
			ChargeBacksManager.insertChargeback(request.getChargebackDetails(), writerWrapper);
		} catch (SQLIntegrityConstraintViolationException sicve) {
			logger.error("Unable to insert chargeback! Unique constraint violated! ", sicve);
			result.setErrorCode(CommonJSONService.ERROR_CODE_RC_UNIQUE_CONSTRAINT_VIOLATED);
		} catch (SQLException sqle) {
			logger.error("Unable to insert chargeback! ", sqle);
			result.setErrorCode(CommonJSONService.ERROR_CODE_UNKNOWN);
		}
		return result;
	}
	
	@BackendPermission(id = "chargebacks_edit")
	public static MethodResult updateChargeback(ChargebackDetailsRequest request, HttpServletRequest httpRequest) {
		MethodResult result = new MethodResult();
		try {
			WriterWrapper writerWrapper = (WriterWrapper) httpRequest.getSession().getAttribute(
					ConstantsBase.BIND_SESSION_WRITER_WRAPPER);
			Long updatingWriterId = writerWrapper.getWriter().getId();
			request.getChargebackDetails().setUpdatingWriterId(updatingWriterId);
			ChargeBacksManager.updateChargeback(request.getChargebackDetails(), writerWrapper);
		} catch (SQLIntegrityConstraintViolationException sicve) {
			logger.error("Unable to update chargeback! Unique constraint violated! ", sicve);
			result.setErrorCode(CommonJSONService.ERROR_CODE_RC_UNIQUE_CONSTRAINT_VIOLATED);
		} catch (SQLException sqle) {
			logger.error("Unable to update chargeback! ", sqle);
			result.setErrorCode(CommonJSONService.ERROR_CODE_UNKNOWN);
		}
		return result;
	}
	
}