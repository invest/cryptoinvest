package com.anyoption.backend.service.chargebacks;

import com.anyoption.common.service.results.MethodResult;

public class ChargebackDetailsResponse extends MethodResult {
	
	private ChargebackDetails chargebackDetails;

	public ChargebackDetails getChargebackDetails() {
		return chargebackDetails;
	}

	public void setChargebackDetails(ChargebackDetails chargebackDetails) {
		this.chargebackDetails = chargebackDetails;
	}
}