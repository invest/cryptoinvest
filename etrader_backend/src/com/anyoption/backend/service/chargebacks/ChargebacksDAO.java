package com.anyoption.backend.service.chargebacks;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;

import com.anyoption.backend.daos.TransactionDAO;
import com.anyoption.common.beans.CreditCard;
import com.anyoption.common.daos.DAOBase;

import il.co.etrader.util.ConstantsBase;
import oracle.jdbc.OracleTypes;

public class ChargebacksDAO extends DAOBase {
	
	public static HashMap<Long, String> getChargebackProviders(Connection con) throws SQLException {
		HashMap<Long, String> providers = new HashMap<Long, String>();
		CallableStatement cstmt = null;
		ResultSet rs = null;
		int index = 1;
		
		try {
			cstmt = con.prepareCall("{call pkg_risk.get_chargeback_providers(o_providers => ? )}");
			
			cstmt.registerOutParameter(index++, OracleTypes.CURSOR);
			cstmt.execute();
			rs = (ResultSet) cstmt.getObject(1);
			while (rs.next()) {
				providers.put(rs.getLong("id"), rs.getString("name"));
			}
		} finally {
			closeStatement(cstmt);
			closeResultSet(rs);
		}
		return providers;
	}
	
	public static ArrayList<ChargebackReasonCode> getAllReasonCodes(Connection con) throws SQLException {
		ArrayList<ChargebackReasonCode> reasonCodes = new ArrayList<ChargebackReasonCode>();
		CallableStatement cstmt = null;
		ResultSet rs = null;
		int index = 1;
		
		try {
			cstmt = con.prepareCall("{call pkg_risk.get_all_reason_codes(o_reason_codes => ? )}");
			
			cstmt.registerOutParameter(index++, OracleTypes.CURSOR);
			cstmt.execute();
			rs = (ResultSet) cstmt.getObject(1);
			while (rs.next()) {
				ChargebackReasonCode chargebackReasonCode = new ChargebackReasonCode();
				chargebackReasonCode.setRc(rs.getString("rc"));
				chargebackReasonCode.setReasonCode(rs.getString("reason_code"));
				chargebackReasonCode.setType(rs.getLong("type"));
				reasonCodes.add(chargebackReasonCode);
			}
		} finally {
			closeStatement(cstmt);
			closeResultSet(rs);
		}
		return reasonCodes;
	}
	
	public static ArrayList<Chargeback> getChargebacks(Connection con, ChargebackFilters chargebackFilters)
																						throws SQLException {
		ArrayList<Chargeback> chargebacks = new ArrayList<Chargeback>();
		CallableStatement cstmt = null;
		ResultSet rs = null;
		int index = 1;
		
		try {
			cstmt = con.prepareCall("{call pkg_risk.get_chargebacks(o_chargebacks => ? " +
																	" , i_from_date => ? " + 
																	" , i_to_date => ? " +
																	" , i_transaction_id => ? " +
																	" , i_user_id => ? " +
																	" , i_arn => ? " +
																	" , i_last_four_digits => ? " +
																	" , i_user_classes => ? " +
																	" , i_business_cases => ? " +
																	" , i_skins => ? " +
																	" , i_countries => ? " +
																	" , i_currencies => ? " +
																	" , i_statuses => ? " +
																	" , i_providers => ? " +
																	" , i_rc => ? " +
																	" , i_page => ? " +
																	" , i_rows_per_page => ? " +
																	" , i_other => ? )}");
			
			cstmt.registerOutParameter(index++, OracleTypes.CURSOR);
			setStatementValue(new Timestamp(chargebackFilters.
											getFromDate() == null ? 0 : chargebackFilters.getFromDate() + 1),
							index++, cstmt);
			setStatementValue(new Timestamp(chargebackFilters.
											getToDate() == null ? 0 : chargebackFilters.getToDate() + 1),
							index++, cstmt);
			setStatementValue(chargebackFilters.getTransactionId(), index++, cstmt);
			setStatementValue(chargebackFilters.getUserId(), index++, cstmt);
			setStatementValue(chargebackFilters.getArn(), index++, cstmt);
			setStatementValue(chargebackFilters.getLast4Digits(), index++, cstmt);
			setStatementValue(chargebackFilters.getUserClasses(), index++, cstmt);
			TransactionDAO.setArrayVal(chargebackFilters.getBusinessCases(), index++, cstmt, con);
			TransactionDAO.setArrayVal(chargebackFilters.getSkins(), index++, cstmt, con);
			TransactionDAO.setArrayVal(chargebackFilters.getCountries(), index++, cstmt, con);
			TransactionDAO.setArrayVal(chargebackFilters.getCurrencies(), index++, cstmt, con);
			TransactionDAO.setArrayVal(chargebackFilters.getStatuses(), index++, cstmt, con);
			TransactionDAO.setArrayVal(chargebackFilters.getProviders(), index++, cstmt, con);
			TransactionDAO.setArrayVal(chargebackFilters.getRc(), index++, cstmt, con);
			setStatementValue(chargebackFilters.getPage(), index++, cstmt);
			setStatementValue(chargebackFilters.getRowsPerPage(), index++, cstmt);
			cstmt.setLong(index++, chargebackFilters.isOtherRC() ? 1 : 0);
			
			cstmt.execute();
			rs = (ResultSet) cstmt.getObject(1);
			while (rs.next()) {
				Chargeback chargeback = new Chargeback();
				chargeback.setChargebackId(rs.getLong("id"));
				chargeback.setUserId(rs.getLong("user_id"));
				chargeback.setStatusName(rs.getString("status"));
				chargeback.setAmount(rs.getLong("amount"));
				chargeback.setCurrencyId(rs.getLong("currency_id"));
				if (rs.getTimestamp("date_chb") != null) {
					chargeback.setChargebackDate(convertToDate(rs.getTimestamp("date_chb")).getTime());
				}
				chargeback.setCaseId(rs.getLong("case_id"));
				chargeback.setRc(String.valueOf(rs.getLong("reason_code")));
				chargeback.setWriterName(rs.getString("updated_by_writer"));
				if (rs.getTimestamp("dispute_deadline") != null) {
					chargeback.setDeadline(convertToDate(rs.getTimestamp("dispute_deadline")).getTime());
				}
				chargeback.setProvider(rs.getString("provider_name"));
				chargeback.setTotalCount(rs.getLong("total_count"));
				chargeback.setTransactionId(rs.getLong("transaction_id"));
				chargeback.setIs3d(rs.getBoolean("is_3d"));
				chargebacks.add(chargeback);
			}
		} finally {
			closeStatement(cstmt);
			closeResultSet(rs);
		}
		return chargebacks;
	}

	public static ChargebackDetails getChargebackDetails(Connection con, long transactionId) throws SQLException {
		ChargebackDetails chargebackDetails = null;
		CallableStatement cstmt = null;
		ResultSet rs = null;
		int index = 1;
		
		try {
			cstmt = con.prepareCall("{call pkg_risk.get_chargeback_details(o_chargeback_details => ? " +
																		   " , i_trans_id => ? )}");
			
			cstmt.registerOutParameter(index++, OracleTypes.CURSOR);
			cstmt.setLong(index++, transactionId);
			
			cstmt.execute();
			
			rs = (ResultSet) cstmt.getObject(1);
			while (rs.next()) {
				chargebackDetails = new ChargebackDetails();
				chargebackDetails.setId(rs.getLong("id"));
				chargebackDetails.setUserName(rs.getString("user_name"));
				chargebackDetails.setFirstName(rs.getString("first_name"));
				chargebackDetails.setLastName(rs.getString("last_name"));
				chargebackDetails.setUserId(rs.getLong("user_id"));
				chargebackDetails.setTransactionId(rs.getLong("transaction_id"));
				chargebackDetails.setCurrencyId(rs.getLong("currency_id"));
				chargebackDetails.setAmount(rs.getLong("amount"));
				chargebackDetails.setAffiliateKey(rs.getLong("affiliate_key"));
				chargebackDetails.setAccountManager(rs.getString("account_manager"));
				chargebackDetails.setAmountUSD(rs.getLong("amount_usd"));
				chargebackDetails.setProvider(rs.getString("clearing_provider"));
				chargebackDetails.setChargebackId(rs.getLong("chargeback_id"));
				chargebackDetails.setIssueId(rs.getLong("issue_id"));
				if (rs.getTimestamp("time_created") != null) {
					chargebackDetails.setTimeCreated(convertToDate(rs.getTimestamp("time_created")).getTime());
				}
				if (rs.getTimestamp("time_updated") != null) {
					chargebackDetails.setTimeUpdated(convertToDate(rs.getTimestamp("time_updated")).getTime());
				}
				if (rs.getTimestamp("time_chb") != null) {
					chargebackDetails.setChargebackDate(convertToDate(rs.getTimestamp("time_chb")).getTime());
				}
				chargebackDetails.setStatus(rs.getLong("status_id"));
				chargebackDetails.setUpdatingWriterName(rs.getString("updating_writer"));
				CreditCard creditCard = new CreditCard();
				creditCard.setTypeName(rs.getString("cc_name"));
				creditCard.setTypeByBin(rs.getString("cc_type_by_bin"));
				creditCard.setTypeIdByBin(rs.getLong("cc_type_id_by_bin"));
				chargebackDetails.setCreditCard(creditCard);
				chargebackDetails.setCaseId(rs.getString("case_id"));
				chargebackDetails.setArn(rs.getString("arn"));
				chargebackDetails.setRc(rs.getLong("rc"));
				chargebackDetails.setRcNew(rs.getLong("new_rc"));
				chargebackDetails.setReasonCodeNew(rs.getString("reason_code"));
				if (rs.getTimestamp("dispute_deadline") != null) {
					chargebackDetails.setDisputeDate(convertToDate(rs.getTimestamp("dispute_deadline")).getTime());
				}
				chargebackDetails.setComments(rs.getString("comments"));
				chargebackDetails.setTransactionTypeId(rs.getLong("transaction_type_id"));
				chargebackDetails.setIs3d(rs.getBoolean("is_3d"));
			}
		} finally {
			closeStatement(cstmt);
			closeResultSet(rs);
		}
		return chargebackDetails;
	}

	public static void insertChargeback(Connection con, ChargebackDetails chargebackDetails) throws SQLException {
		CallableStatement cstmt = null;
		int index = 1;
		
		try {
			cstmt = con.prepareCall("{call pkg_risk.insert_chargeback(i_transaction_id => ? " +
																	" , i_issue_id => ? " +
																	" , i_chb_date => ? " +
																	" , i_status => ? " +
																	" , i_case_id => ? " +
																	" , i_arn => ? " +
																	" , i_rc => ? " +
																	" , i_rc_new => ? " +
																	" , i_reason_code_new => ? " +
																	" , i_dispute_date => ? " +
																	" , i_comments => ? " +
																	" , i_writer_id => ? " +
					   												" , i_state => ? )}");
			
			cstmt.setLong(index++, chargebackDetails.getTransactionId());
			cstmt.setLong(index++, chargebackDetails.getIssueId());
			setStatementValue(new Timestamp(chargebackDetails.
											getChargebackDate() == null ? 0 : chargebackDetails.getChargebackDate() + 1),
								index++, cstmt);
			setStatementValue(chargebackDetails.getStatus(), index++, cstmt);
			setStatementValue(chargebackDetails.getCaseId(), index++, cstmt);
			setStatementValue(chargebackDetails.getArn(), index++, cstmt);
			setStatementValue(chargebackDetails.getRc(), index++, cstmt);
			setStatementValue(chargebackDetails.getRcNew(), index++, cstmt);
			setStatementValue(chargebackDetails.getReasonCodeNew(), index++, cstmt);
			setStatementValue(new Timestamp(chargebackDetails.
											getDisputeDate() == null ? 0 : chargebackDetails.getDisputeDate() + 1),
								index++, cstmt);
			setStatementValue(chargebackDetails.getComments(), index++, cstmt);
			setStatementValue(chargebackDetails.getWriterId(), index++, cstmt);
			cstmt.setLong(index++, ConstantsBase.STATE_CHARGEBACK);
			
			cstmt.executeUpdate();
			
			chargebackDetails.setId(getSeqCurValue(con, "seq_charge_backs"));
		} finally {
			closeStatement(cstmt);
		}
	}

	public static void updateChargeback(Connection con, ChargebackDetails chargebackDetails) throws SQLException {
		CallableStatement cstmt = null;
		int index = 1;
		
		try {
			cstmt = con.prepareCall("{call pkg_risk.update_chargeback(i_id => ? " +
																	" ,i_updated_by => ? " +
																	" ,i_chb_date => ? " +
																	" ,i_status => ? " +
																	" ,i_case_id => ? " +
																	" ,i_arn => ? " +
																	" ,i_rc => ? " +
																	" ,i_rc_new => ? " +
																	" ,i_reason_code_new => ? " +
																	" ,i_dispute_date => ? " +
																	" ,i_comments => ? )}");
			
			cstmt.setLong(index++, chargebackDetails.getId());
			setStatementValue(chargebackDetails.getUpdatingWriterId(), index++, cstmt);
			setStatementValue(new Timestamp(chargebackDetails.
											getChargebackDate() == null ? 0 : chargebackDetails.getChargebackDate() + 1),
								index++, cstmt);
			setStatementValue(chargebackDetails.getStatus(), index++, cstmt);
			setStatementValue(chargebackDetails.getCaseId(), index++, cstmt);
			setStatementValue(chargebackDetails.getArn(), index++, cstmt);
			setStatementValue(chargebackDetails.getRc(), index++, cstmt);
			setStatementValue(chargebackDetails.getRcNew(), index++, cstmt);
			setStatementValue(chargebackDetails.getReasonCodeNew(), index++, cstmt);
			setStatementValue(new Timestamp(chargebackDetails.
											getDisputeDate() == null ? 0 : chargebackDetails.getDisputeDate() + 1),
								index++, cstmt);
			setStatementValue(chargebackDetails.getComments(), index++, cstmt);

			cstmt.executeUpdate();
		} finally {
			closeStatement(cstmt);
		}
		
	}
}