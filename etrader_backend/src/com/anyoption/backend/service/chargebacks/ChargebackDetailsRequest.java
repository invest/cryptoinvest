package com.anyoption.backend.service.chargebacks;

import com.anyoption.common.service.requests.UserMethodRequest;

public class ChargebackDetailsRequest extends UserMethodRequest {
	
	private ChargebackDetails chargebackDetails;

	public ChargebackDetails getChargebackDetails() {
		return chargebackDetails;
	}

	public void setChargebackDetails(ChargebackDetails chargebackDetails) {
		this.chargebackDetails = chargebackDetails;
	}
}