package com.anyoption.backend.service.chargebacks;

public class ChargebackReasonCode {
	
	private String rc;
	private String reasonCode;
	private long type;
	

	public String getRc() {
		return rc;
	}
	
	public void setRc(String rc) {
		this.rc = rc;
	}
	
	public String getReasonCode() {
		return reasonCode;
	}
	
	public void setReasonCode(String reasonCode) {
		this.reasonCode = reasonCode;
	}
	
	public long getType() {
		return type;
	}
	
	public void setType(long type) {
		this.type = type;
	}
}