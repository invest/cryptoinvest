package com.anyoption.backend.service.chargebacks;

import java.util.ArrayList;

import com.anyoption.common.service.results.MethodResult;

public class ChargebacksResponse extends MethodResult {
	
	private ArrayList<Chargeback> chargebacksList;

	public ArrayList<Chargeback> getChargebacksList() {
		return chargebacksList;
	}

	public void setChargebacksList(ArrayList<Chargeback> chargebacksList) {
		this.chargebacksList = chargebacksList;
	}
}