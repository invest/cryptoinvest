package com.anyoption.backend.service.chargebacks;

import java.io.Serializable;

import com.anyoption.common.beans.base.CreditCard;

public class ChargebackDetails implements Serializable {

	private static final long serialVersionUID = 1L;

	private long id;
	private String userName;
	private String firstName;
	private String lastName;
	private Long writerId;
	private Long affiliateKey;
	private String accountManager;
	private long userId;
	private long transactionId;
	private long amount;
	private long amountUSD;
	private String provider;
	private long currencyId;
	private long chargebackId;
	private long issueId;
	private Long timeCreated;
	private Long timeUpdated;
	private Long updatingWriterId;
	private String updatingWriterName;
	private Long chargebackDate;
	private Long status;
	private String caseId;
	private String arn;
	private Long rc;
	private Long rcNew;
	private String reasonCodeNew;
	private Long disputeDate;
	private String comments;
	private String utcOffsetCreated;
	private CreditCard creditCard;
	private Long transactionTypeId;
	private boolean is3d;
	
	public String getUserName() {
		return userName;
	}
	
	public void setUserName(String userName) {
		this.userName = userName;
	}
	
	public String getFirstName() {
		return firstName;
	}
	
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	
	public String getLastName() {
		return lastName;
	}
	
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	
	public long getUserId() {
		return userId;
	}
	
	public void setUserId(long userId) {
		this.userId = userId;
	}
	
	public long getTransactionId() {
		return transactionId;
	}
	
	public void setTransactionId(long transactionId) {
		this.transactionId = transactionId;
	}
	
	public long getAmount() {
		return amount;
	}
	
	public void setAmount(long amount) {
		this.amount = amount;
	}
	
	public long getCurrencyId() {
		return currencyId;
	}
	
	public void setCurrencyId(long currencyId) {
		this.currencyId = currencyId;
	}
	
	public Long getChargebackDate() {
		return chargebackDate;
	}
	
	public void setChargebackDate(Long chargebackDate) {
		this.chargebackDate = chargebackDate;
	}
	
	public Long getStatus() {
		return status;
	}
	
	public void setStatus(Long status) {
		this.status = status;
	}
	
	public String getCaseId() {
		return caseId;
	}
	
	public void setCaseId(String caseId) {
		this.caseId = caseId;
	}
	
	public String getArn() {
		return arn;
	}
	
	public void setArn(String arn) {
		this.arn = arn;
	}
	
	public Long getRc() {
		return rc;
	}
	
	public void setRc(Long rc) {
		this.rc = rc;
	}
	
	public Long getRcNew() {
		return rcNew;
	}
	
	public void setRcNew(Long rcNew) {
		this.rcNew = rcNew;
	}
	
	public String getReasonCodeNew() {
		return reasonCodeNew;
	}
	
	public void setReasonCodeNew(String reasonCodeNew) {
		this.reasonCodeNew = reasonCodeNew;
	}
	
	public Long getDisputeDate() {
		return disputeDate;
	}
	
	public void setDisputeDate(Long disputeDate) {
		this.disputeDate = disputeDate;
	}
	
	public String getComments() {
		return comments;
	}
	
	public void setComments(String comments) {
		this.comments = comments;
	}

	public long getAmountUSD() {
		return amountUSD;
	}

	public void setAmountUSD(long amountUSD) {
		this.amountUSD = amountUSD;
	}

	public Long getAffiliateKey() {
		return affiliateKey;
	}

	public void setAffiliateKey(Long affiliateKey) {
		this.affiliateKey = affiliateKey;
	}

	public String getAccountManager() {
		return accountManager;
	}

	public void setAccountManager(String accountManager) {
		this.accountManager = accountManager;
	}

	public String getProvider() {
		return provider;
	}

	public void setProvider(String provider) {
		this.provider = provider;
	}

	public long getChargebackId() {
		return chargebackId;
	}

	public void setChargebackId(long chargebackId) {
		this.chargebackId = chargebackId;
	}

	public long getIssueId() {
		return issueId;
	}

	public void setIssueId(long issueId) {
		this.issueId = issueId;
	}

	public Long getTimeCreated() {
		return timeCreated;
	}

	public void setTimeCreated(Long timeCreated) {
		this.timeCreated = timeCreated;
	}

	public Long getTimeUpdated() {
		return timeUpdated;
	}

	public void setTimeUpdated(Long timeUpdated) {
		this.timeUpdated = timeUpdated;
	}

	public Long getUpdatingWriterId() {
		return updatingWriterId;
	}

	public void setUpdatingWriterId(Long updatingWriterId) {
		this.updatingWriterId = updatingWriterId;
	}

	public String getUpdatingWriterName() {
		return updatingWriterName;
	}

	public void setUpdatingWriterName(String updatingWriterName) {
		this.updatingWriterName = updatingWriterName;
	}

	public Long getWriterId() {
		return writerId;
	}

	public void setWriterId(Long writerId) {
		this.writerId = writerId;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getUtcOffsetCreated() {
		return utcOffsetCreated;
	}

	public void setUtcOffsetCreated(String utcOffsetCreated) {
		this.utcOffsetCreated = utcOffsetCreated;
	}

	public CreditCard getCreditCard() {
		return creditCard;
	}

	public void setCreditCard(CreditCard creditCard) {
		this.creditCard = creditCard;
	}

	public Long getTransactionTypeId() {
		return transactionTypeId;
	}

	public void setTransactionTypeId(Long transactionTypeId) {
		this.transactionTypeId = transactionTypeId;
	}

	public boolean isIs3d() {
		return is3d;
	}

	public void setIs3d(boolean is3d) {
		this.is3d = is3d;
	}
}