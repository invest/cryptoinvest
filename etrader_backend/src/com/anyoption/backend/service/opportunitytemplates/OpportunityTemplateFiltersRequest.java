package com.anyoption.backend.service.opportunitytemplates;

import com.anyoption.common.service.assetindex.MarketRequest;

public class OpportunityTemplateFiltersRequest extends MarketRequest {
	
	private long active;

	public long getActive() {
		return active;
	}

	public void setActive(long active) {
		this.active = active;
	}
}
