package com.anyoption.backend.service.opportunitytemplates;

import com.anyoption.backend.service.createmarket.CopyMarketResponse;

import il.co.etrader.bl_vos.OpportunityTemplate;

public class MarketOpportunityResponse extends CopyMarketResponse {
	
	private OpportunityTemplate opportunityTemplate;
	private String writerName;

	public OpportunityTemplate getOpportunityTemplate() {
		return opportunityTemplate;
	}

	public void setOpportunityTemplate(OpportunityTemplate opportunityTemplate) {
		this.opportunityTemplate = opportunityTemplate;
	}

	public String getWriterName() {
		return writerName;
	}

	public void setWriterName(String writerName) {
		this.writerName = writerName;
	}
}
