package com.anyoption.backend.service.opportunitytemplates;

import il.co.etrader.bl_vos.OpportunityTemplate;

public class OpportunityTemplateRequest extends OpportunityTemplateFiltersRequest {
	
	private long opportunityTemplateId;
	private OpportunityTemplate opportunityTemplate;

	public long getOpportunityTemplateId() {
		return opportunityTemplateId;
	}

	public void setOpportunityTemplateId(long opportunityTemplateId) {
		this.opportunityTemplateId = opportunityTemplateId;
	}

	public OpportunityTemplate getOpportunityTemplate() {
		return opportunityTemplate;
	}

	public void setOpportunityTemplate(OpportunityTemplate opportunityTemplate) {
		this.opportunityTemplate = opportunityTemplate;
	}
}