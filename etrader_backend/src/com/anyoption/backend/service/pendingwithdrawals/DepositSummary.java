package com.anyoption.backend.service.pendingwithdrawals;

public class DepositSummary {
	
	private long typeId;
	private String description;
	private String cardDetails;
	private String lastFourDigitsCC;
	private String providerName;
	private long depositCount;
	private long amount;
	private long amountToCredit;
	private long maxDate;
	private long currencyId;
	private String expDate;
	
	public long getTypeId() {
		return typeId;
	}
	
	public void setTypeId(long typeId) {
		this.typeId = typeId;
	}
	
	public String getDescription() {
		return description;
	}
	
	public void setDescription(String description) {
		this.description = description;
	}
	
	public String getCardDetails() {
		return cardDetails;
	}
	
	public void setCardDetails(String cardDetails) {
		this.cardDetails = cardDetails;
	}
	
	public String getLastFourDigitsCC() {
		return lastFourDigitsCC;
	}
	
	public void setLastFourDigitsCC(String lastFourDigitsCC) {
		this.lastFourDigitsCC = lastFourDigitsCC;
	}
	
	public String getProviderName() {
		return providerName;
	}
	
	public void setProviderName(String providerName) {
		this.providerName = providerName;
	}
	
	public long getDepositCount() {
		return depositCount;
	}
	
	public void setDepositCount(long depositCount) {
		this.depositCount = depositCount;
	}
	
	public long getAmount() {
		return amount;
	}
	
	public void setAmount(long amount) {
		this.amount = amount;
	}
	
	public long getAmountToCredit() {
		return amountToCredit;
	}
	
	public void setAmountToCredit(long amountToCredit) {
		this.amountToCredit = amountToCredit;
	}
	
	public long getMaxDate() {
		return maxDate;
	}
	
	public void setMaxDate(long maxDate) {
		this.maxDate = maxDate;
	}

	public long getCurrencyId() {
		return currencyId;
	}

	public void setCurrencyId(long currencyId) {
		this.currencyId = currencyId;
	}

	public String getExpDate() {
		return expDate;
	}

	public void setExpDate(String expDate) {
		this.expDate = expDate;
	}
	
}
