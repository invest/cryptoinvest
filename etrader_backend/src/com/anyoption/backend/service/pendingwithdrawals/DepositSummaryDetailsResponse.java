package com.anyoption.backend.service.pendingwithdrawals;

import java.util.ArrayList;

import com.anyoption.common.service.results.MethodResult;

public class DepositSummaryDetailsResponse extends MethodResult {
	
	ArrayList<DepositSummary> depositSummaryList;

	public ArrayList<DepositSummary> getDepositSummaryList() {
		return depositSummaryList;
	}

	public void setDepositSummaryList(ArrayList<DepositSummary> depositSummaryList) {
		this.depositSummaryList = depositSummaryList;
	}
}
