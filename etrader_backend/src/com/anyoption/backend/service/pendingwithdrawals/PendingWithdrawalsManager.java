package com.anyoption.backend.service.pendingwithdrawals;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import com.anyoption.common.beans.PaymentMethod;
import com.anyoption.common.beans.Transaction;
import com.anyoption.common.bl_vos.WireBase;
import com.anyoption.common.clearing.ClearingException;
import com.anyoption.common.daos.WiresDAOBase;
import com.anyoption.common.managers.BaseBLManager;
import com.anyoption.common.managers.TransactionsManagerBase;

import il.co.etrader.backend.bl_managers.TransactionsManager;
import il.co.etrader.backend.bl_managers.UsersManager;
import il.co.etrader.backend.bl_vos.User;
import il.co.etrader.backend.bl_vos.WriterWrapper;
import il.co.etrader.backend.dao_managers.TransactionsDAO;
import il.co.etrader.bl_managers.ClearingManager;
import il.co.etrader.bl_vos.Cheque;
import il.co.etrader.clearing.EnvoyClearingProvider;
import il.co.etrader.clearing.EnvoyInfo;
import il.co.etrader.dao_managers.ChequeDAO;
import il.co.etrader.dao_managers.LimitsDAO;
import il.co.etrader.util.CommonUtil;

public class PendingWithdrawalsManager extends BaseBLManager {
	
	private static final Logger logger = Logger.getLogger(PendingWithdrawalsManager.class);

	public static final String FILTER_USER_INVESTED = "userInvested";
	public static final String FILTER_PAST_WITHDRAWS = "pastWithdraws";
	public static final String FILTER_WITHDRAW_STATUS = "withdrawStatus";
	public static final String FILTER_FLAG_SUBJECTS = "flagSubjects";
	public static final String FILTER_CLEARING_PROVIDERS = "clearingProviders";
	public static final String FILTER_BANKS = "banks";
	
	public static final long PENDING_WITHDRAW_STATUS_REQUESTED = 4l;
	public static final long PENDING_WITHDRAW_STATUS_FIRST_APPROVED = 9l;
	
	public static final long PENDING_WITHDRAW_SCREEN_FIRST_APPROVE = 1l;
	public static final long PENDING_WITHDRAW_SCREEN_SECOND_APPROVE = 2l;
	public static final long PENDING_WITHDRAW_SCREEN_SUPPORT = 3l;
	
	public static HashMap<Long, String> userInvested;
	public static HashMap<Long, String> pastWithdraws;
	public static HashMap<Long, String> withdrawStatus;
	public static HashMap<Long, String> flagSubjects;
	
	public static HashMap<Long, String> getUserInvestedFilter() {
		if (null == userInvested) {
			userInvested = new HashMap<Long, String>();
			userInvested.put(1l, "Yes");
			userInvested.put(0l, "No");
		}
		return userInvested;
	}
	
	public static HashMap<Long, String> getPastWithdrawsFilter() {
		if (null == pastWithdraws) {
			pastWithdraws = new HashMap<Long, String>();
			pastWithdraws.put(1l, "Yes");
			pastWithdraws.put(0l, "No");
		}
		return pastWithdraws;
	}
	
	public static HashMap<Long, String> getWithdrawStatusFilter() {
		if (null == withdrawStatus) {
			withdrawStatus = new HashMap<Long, String>();
			withdrawStatus.put(9l, "Approved");
			withdrawStatus.put(4l, "Requested");
		}
		return withdrawStatus;
	}
	
	public static HashMap<Long, String> getFlagSubjectsFilter() throws SQLException {
		if (null == flagSubjects) {
			Connection conn = null;
			try {
				conn = getConnection();
				flagSubjects = new HashMap<Long, String>();
				flagSubjects = PendingWithdrawsDAO.getFlagSubjects(conn);
			} finally {
				closeConnection(conn);
			}
		}
		return flagSubjects;
	}
	
	public static ArrayList<PendingWithdraw> getPendingWithdraws(PendingWithdrawFilters pendingWithdraw, boolean isSecondApproval) throws SQLException {
		ArrayList<PendingWithdraw> pendingWithdrawsList = null;
		Connection conn = null;
		try {
			conn = getConnection();
			pendingWithdrawsList = PendingWithdrawsDAO.getPendingWithdraws(conn, pendingWithdraw, isSecondApproval);
		} finally {
			closeConnection(conn);
		}
		return pendingWithdrawsList;
	}
	
	public static UserDetails getUserDetails(long transactionId) throws SQLException {
		UserDetails userDetails = null;
		Connection conn = null;
		try {
			conn = getConnection();
			userDetails = PendingWithdrawsDAO.getUserDetails(conn, transactionId);
		} finally {
			closeConnection(conn);
		}
		return userDetails;
	}
	
	public static TransactionDetails getTransactionDetails(long transactionId) throws SQLException {
		TransactionDetails transactionDetails = null;
		Connection conn = null;
		try {
			conn = getConnection();
			transactionDetails = PendingWithdrawsDAO.getTransactionDetails(conn, transactionId);
		} finally {
			closeConnection(conn);
		}
		return transactionDetails;
	}
	
	public static ArrayList<DepositSummary> getDepositSummaryDetails(long transactionId, long userId) throws SQLException {
		ArrayList<DepositSummary> depositSummaryDetailsList = null;
		Connection conn = null;
		try {
			conn = getConnection();
			depositSummaryDetailsList = PendingWithdrawsDAO.getDepositSummaryDetails(conn, transactionId, userId);
		} finally {
			closeConnection(conn);
		}
		return depositSummaryDetailsList;
	}
	
	public static boolean approveCcWithdraw(Transaction t, long fee, boolean accounting, String processedWriterName,
			StringBuffer changedProviderComment, WriterWrapper writerWrapper, boolean tradersReviewed,
			boolean flagAccountingOk, long accountingOkWriter, Date accountingOkDate) throws SQLException {

		if (logger.isEnabledFor(Level.INFO)) {
			String ls = System.getProperty("line.separator");
			logger.log(Level.INFO, "Approve Credit card Withdraw: " + ls + t.toString() + ls + " ccNumber: " + t.getCc4digit() + ls
					+ " fee: " + fee + ls + " admin: " + accounting + ls);
		}
		Connection con = null;
		try {
			con = getConnection();
			con.setAutoCommit(false);
			t.setProcessedWriterId(writerWrapper.getWriter().getId());
			String comments = t.getComments().isEmpty() ? "" : t.getComments() + "|";
			SimpleDateFormat formatDate = new SimpleDateFormat("dd.MM.yy HH:mm");
			if (accounting) {  // from accounting tab
				t.setAccountingApproved(true);
				// Necessary because we treat cup and baropay withdrawals as cc withdrawals
				if (t.getTypeId() == TransactionsManagerBase.TRANS_TYPE_CC_WITHDRAW) {
					t.setStatusId(TransactionsManagerBase.TRANS_STATUS_SECOND_APPROVAL);
				}
				comments += "Second approved at " + formatDate.format(Calendar.getInstance().getTime()) + " by " + processedWriterName;
				t.setComments(comments);
				TransactionsDAO.updateAfterAccouningApproved(con, t);
				t.setUtcOffsetSettled(UsersManager.getUserDetails(t.getUserId(), writerWrapper.getSkinsToString()).getUtcOffset());
			}
			else {  // admin
				t.setStatusId(TransactionsManagerBase.TRANS_STATUS_APPROVED);
				comments += "First approved at " + formatDate.format(Calendar.getInstance().getTime()) + " by " + processedWriterName
							+ " | " + changedProviderComment.toString();
				t.setComments(comments);
				setApproveChanges(con, t, writerWrapper.getWriter().getId(), flagAccountingOk, accountingOkWriter, accountingOkDate);
			}
			TransactionsDAO.update(con, t);

			if (logger.isEnabledFor(Level.INFO)) {
				String ls = System.getProperty("line.separator");
				logger.log(Level.INFO, " approve credit card Withdraw finished successfully! " + ls);
			}
			PendingWithdrawalsManager.updateTradersReviewedFlag(con, tradersReviewed,
					writerWrapper.getWriter().getId(), t.getUserId());
			con.commit();
		} catch (SQLException e) {
			logger.log(Level.ERROR, "approveCcWithdraw: ", e);
			try {
				con.rollback();
			} catch (SQLException ie) {
				logger.log(Level.ERROR, "Can't rollback.", ie);
			}
			throw e;
		} finally {
			con.setAutoCommit(true);
			closeConnection(con);
		}
		return true;
	}
	
	public static boolean approveEnvoyWithdraw(Transaction t, long fee, boolean account, long writerId,
			WriterWrapper writerWrapper, boolean tradersReviewed, boolean flagAccountingOk, long accountingOkWriter,
			Date accountingOkDate) throws SQLException {
		if (logger.isEnabledFor(Level.INFO)) {
			String ls = System.getProperty("line.separator");
			logger.log(Level.INFO, "Approve Envoy Withdraw: " + ls + t.toString() + ls + "Amount: " + t.getAmount() + ls
					+ " fee: " + fee + ls + " admin: " + account + ls);
		}
		Connection con = null;
		try {
			con = getConnection();
			con.setAutoCommit(false);
			t.setProcessedWriterId(writerWrapper.getWriter().getId());

			if (!account) {  // admin
				t.setStatusId(TransactionsManager.TRANS_STATUS_APPROVED);
				setApproveChanges(con, t, writerId, flagAccountingOk, accountingOkWriter, accountingOkDate);
			} else {  // from accounting tab
				User user = UsersManager.getUserDetails(t.getUserId());

				t.setAccountingApproved(true);
				TransactionsDAO.updateAfterAccouningApproved(con, t);
				t.setUtcOffsetSettled(user.getUtcOffset());

				EnvoyInfo info = new EnvoyInfo(user,t);
				PaymentMethod pm = il.co.etrader.bl_managers.
									TransactionsManagerBase.getPaymentMethodById(t.getPaymentTypeId());
		        info.setRefundCountryCode(pm.getEnvoyWithdrawCountryCode());
		        info.setAccountNumber(t.getEnvoyAccountNum());
		        info.setRequestReference(String.valueOf(t.getId()));
		        info.setRefundDestination(EnvoyClearingProvider.REFUND_DEST_SERVICE);

		        if (!t.isFeeCancel()){
	 	            fee = LimitsDAO.getFeeByTranTypeId(con, user.getLimitIdLongValue(),
	 	            								   TransactionsManagerBase.TRANS_TYPE_ENVOY_WITHDRAW);
		            if (fee > 0){
		            	info.setAmount(t.getAmount() - fee);
		            }
	            }
		        try{
		        	ClearingManager.withdraw(info);
		        	if(info.isSuccessful()) {
		        		t.setStatusId(TransactionsManagerBase.TRANS_STATUS_SUCCEED);
		        	}
		        }catch (ClearingException e) {
		        	logger.error("Problem with sending withdraw to envot for tansaction: " + t.getId(),e);
				}
			}
			TransactionsDAO.update(con, t);

			if (logger.isEnabledFor(Level.INFO)) {
				String ls = System.getProperty("line.separator");
				logger.log(Level.INFO, " approve Envoy Withdraw finished successfully! " + ls);
			}
			PendingWithdrawalsManager.updateTradersReviewedFlag(con, tradersReviewed,
					writerWrapper.getWriter().getId(), t.getUserId());
			con.commit();
		} catch (SQLException e) {
			logger.log(Level.ERROR, "approveEnvoyWithdraw: ", e);
			try {
				con.rollback();
			} catch (SQLException ie) {
				logger.log(Level.ERROR, "Can't rollback.", ie);
			}
			throw e;
		} finally {
			con.setAutoCommit(true);
			closeConnection(con);
		}
		return true;
	}
	
	public static boolean approveGeneralWithdraw(Transaction t, long fee, boolean accounting,
			WriterWrapper writerWrapper, boolean tradersReviewed, boolean flagAccountingOk, long accountingOkWriter,
			Date accountingOkDate) throws SQLException {
		if (logger.isEnabledFor(Level.INFO)) {
			String ls = System.getProperty("line.separator");
			logger.log(Level.INFO, "Approve general withdraw: " + ls + t.toString() + ls + "Amount: " + t.getAmount() + ls
					+ " fee: " + fee + ls + " admin: " + accounting + ls);
		}
		Connection con = null;
		try {
			con = getConnection();
			con.setAutoCommit(false);
			t.setProcessedWriterId(writerWrapper.getWriter().getId());
			if (!accounting) {  // admin
				t.setStatusId(TransactionsManager.TRANS_STATUS_APPROVED);
				setApproveChanges(con, t, writerWrapper.getWriter().getId(), flagAccountingOk, accountingOkWriter, accountingOkDate);
			} else {  // from accounting tab
				t.setAccountingApproved(true);
				TransactionsDAO.updateAfterAccouningApproved(con, t);
				t.setUtcOffsetSettled(UsersManager.getUserDetails(t.getUserId(), writerWrapper.getSkinsToString()).getUtcOffset());
				t.setTimeSettled(new Date());
				t.setStatusId(TransactionsManager.TRANS_STATUS_SUCCEED);
			}
			TransactionsDAO.update(con, t);
			if (logger.isEnabledFor(Level.INFO)) {
				String ls = System.getProperty("line.separator");
				logger.log(Level.INFO, " approve general withdraw finished successfully! " + ls);
			}
			PendingWithdrawalsManager.updateTradersReviewedFlag(con, tradersReviewed,
					writerWrapper.getWriter().getId(), t.getUserId());
			con.commit();
		} catch (SQLException e) {
			logger.log(Level.ERROR, "approve general withdraw: " , e);
			try {
				con.rollback();
			} catch (SQLException ie) {
				logger.log(Level.ERROR, "Can't rollback.", ie);
			}
			throw e;
		} finally {
			con.setAutoCommit(true);
			closeConnection(con);
		}
		return true;
	}
	
	public static boolean approveWithdraw(Transaction t, Cheque c, long fee, boolean accounting,
			WriterWrapper writerWrapper, String ip, boolean tradersReviewed, boolean flagAccountingOk,
			long accountingOkWriter, Date accountingOkDate) throws SQLException {
		if (logger.isEnabledFor(Level.INFO)) {
			String ls = System.getProperty("line.separator");
			logger.log(Level.INFO, "Approve Withdraw: " + ls + t.toString() + ls + " cheque: " + c.toString() + ls
					+ " fee: " + fee + ls + " admin: " + accounting + ls);
		}
		Connection con = null;
		try {
			con = getConnection();
			con.setAutoCommit(false);

			t.setProcessedWriterId(writerWrapper.getWriter().getId());
			t.setUtcOffsetSettled(UsersManager.getUserDetails(t.getUserId(), writerWrapper.getSkinsToString()).getUtcOffset());

			if (accounting) {
				t.setAmount(t.getAmount() - fee);
				t.setTimeSettled(new Date());
				t.setStatusId(TransactionsManager.TRANS_STATUS_SUCCEED);
			} else {
				t.setStatusId(TransactionsManager.TRANS_STATUS_APPROVED);
				setApproveChanges(con, t, writerWrapper.getWriter().getId(), flagAccountingOk, accountingOkWriter, accountingOkDate);
			}

			TransactionsDAO.update(con, t);

			if (fee > 0 && accounting) {
				long withdrawId = t.getId();

				t.setIp(ip);
				t.setDescription(null);
				t.setComments(null);
				t.setAmount(fee);
				t.setProcessedWriterId(writerWrapper.getWriter().getId());
				t.setWriterId(writerWrapper.getWriter().getId());
				t.setTimeSettled(new Date());
				t.setStatusId(TransactionsManager.TRANS_STATUS_SUCCEED);
				t.setTypeId(TransactionsManager.TRANS_TYPE_HOMO_FEE);
				t.setReferenceId(new BigDecimal(withdrawId));
				t.setChequeId(null);
				t.setLoginId(0L);

				TransactionsDAO.insert(con, t);

				if (logger.isEnabledFor(Level.INFO)) {
					String ls = System.getProperty("line.separator");
					logger.log(Level.INFO, " fee transaction inserted: " + ls + t.toString());
				}

				TransactionsDAO.updateRefId(con, withdrawId, t.getId());
			}

			ChequeDAO.update(con, c);

			if (logger.isEnabledFor(Level.INFO)) {
				String ls = System.getProperty("line.separator");
				logger.log(Level.INFO, " approve Withdraw finished successfully! " + ls);
			}
			PendingWithdrawalsManager.updateTradersReviewedFlag(con, tradersReviewed,
					writerWrapper.getWriter().getId(), t.getUserId());
			con.commit();

		} catch (SQLException e) {
			logger.log(Level.ERROR, "approveWithdraw: ", e);
			try {
				con.rollback();
			} catch (SQLException ie) {
				logger.log(Level.ERROR, "Can't rollback.", ie);
			}
			throw e;
		} finally {
			con.setAutoCommit(true);
			closeConnection(con);
		}
		return true;
	}
	
	public static boolean approveBankWireWithdraw(Transaction t, WireBase w, long fee, boolean accounting,
			WriterWrapper writerWrapper, long chosenBankID, String ip, boolean tradersReviewed,
			boolean flagAccountingOk, long accountingOkWriter, Date accountingOkDate) throws SQLException {
		if (logger.isEnabledFor(Level.INFO)) {
			String ls = System.getProperty("line.separator");
			logger.log(Level.INFO, "Approve bank wire Withdraw: " + ls + t.toString() + ls + " bank wire: "
						+ w.toString() + ls + " fee: " + fee + ls + " accounting: " + accounting + ls);
		}
		Connection con = null;
		try {
			con = getConnection();
			con.setAutoCommit(false);
			t.setProcessedWriterId(writerWrapper.getWriter().getId());
			t.setUtcOffsetSettled(UsersManager.getUserDetails(t.getUserId(), writerWrapper.getSkinsToString()).getUtcOffset());
			if (accounting) {
				t.setAmount(t.getAmount() - fee);
				t.setTimeSettled(new Date());
				t.setStatusId(TransactionsManager.TRANS_STATUS_SUCCEED);
				//save the amount before taking the fee
				t.setWireAmountAfterFee(CommonUtil.displayAmount(t.getAmount(), t.getCurrency().getId()));
			} else { // admin
				setApproveChanges(con, t, writerWrapper.getWriter().getId(), flagAccountingOk, accountingOkWriter, accountingOkDate);
				t.setStatusId(TransactionsManager.TRANS_STATUS_APPROVED);
			}
			TransactionsDAO.update(con, t);
			if (accounting && w.getBankFeeAmount() > 0) {
				WiresDAOBase.updateBankFeeAmount(con, w.getId(), w.getBankFeeAmount());
			}
			WiresDAOBase.updateBankWireWithdrawal(con, w.getId(), chosenBankID);
			if (fee > 0 && accounting) {
				long withdrawId = t.getId();
				t.setIp(ip);
				t.setDescription(null);
				t.setComments(null);
				t.setAmount(fee);
				t.setProcessedWriterId(writerWrapper.getWriter().getId());
				t.setWriterId(writerWrapper.getWriter().getId());
				t.setTimeSettled(new Date());
				t.setStatusId(TransactionsManager.TRANS_STATUS_SUCCEED);
				t.setTypeId(TransactionsManager.TRANS_TYPE_HOMO_FEE);
				t.setReferenceId(new BigDecimal(withdrawId));
				t.setChequeId(null);
				t.setLoginId(0L);
				TransactionsDAO.insert(con, t);
				if (logger.isEnabledFor(Level.INFO)) {
					String ls = System.getProperty("line.separator");
					logger.log(Level.INFO, " fee transaction for bank wire inserted: " + ls + t.toString());
				}
				TransactionsDAO.updateRefId(con, withdrawId, t.getId());
			}
			if (logger.isEnabledFor(Level.INFO)) {
				String ls = System.getProperty("line.separator");
				logger.log(Level.INFO, " approve bank wire Withdraw finished successfully! " + ls);
			}
			PendingWithdrawalsManager.updateTradersReviewedFlag(con, tradersReviewed,
					writerWrapper.getWriter().getId(), t.getUserId());
			con.commit();
		} catch (SQLException e) {
			logger.log(Level.ERROR, "approveBankWireWithdraw: ", e);
			try {
				con.rollback();
			} catch (SQLException ie) {
				logger.log(Level.ERROR, "Can't rollback.", ie);
			}
			throw e;
		} finally {
			con.setAutoCommit(true);
			closeConnection(con);
		}
		return true;
	}
	
	public static boolean approvePayPalWithdraw(Transaction t, long fee, boolean accounting, String paypalTransactionId,
			WriterWrapper writerWrapper, boolean tradersReviewed, boolean flagAccountingOk, long accountingOkWriter,
			Date accountingOkDate) throws SQLException {
		if (logger.isEnabledFor(Level.INFO)) {
			String ls = System.getProperty("line.separator");
			logger.log(Level.INFO, "Approve PayPal Withdraw: " + ls + t.toString() + ls	+ "Amount: " + t.getAmount() + ls
						+ " fee: " + fee + ls + " accounting: " + accounting + ls);
		}
		Connection con = null;
		try {
			con = getConnection();
			con.setAutoCommit(false);
			t.setProcessedWriterId(writerWrapper.getWriter().getId());

			if (accounting) {
				t.setAccountingApproved(true);
				TransactionsDAO.updateAfterAccouningApproved(con, t);

				t.setXorIdAuthorize(paypalTransactionId);
				t.setXorIdCapture(paypalTransactionId);
				t.setAmount(t.getAmount() - fee);
				t.setTimeSettled(new Date());
				t.setUtcOffsetSettled(UsersManager.getUserDetails(t.getUserId(),
									  writerWrapper.getSkinsToString()).getUtcOffset());
				t.setStatusId(TransactionsManager.TRANS_STATUS_SUCCEED);
			} else {  // admin
				setApproveChanges(con, t, writerWrapper.getWriter().getId(), flagAccountingOk, accountingOkWriter, accountingOkDate);
				t.setStatusId(TransactionsManager.TRANS_STATUS_APPROVED);
			}
			TransactionsDAO.update(con, t);

			if (logger.isEnabledFor(Level.INFO)) {
				String ls = System.getProperty("line.separator");
				logger.log(Level.INFO, " approve PayPal Withdraw finished successfully! " + ls);
			}
			PendingWithdrawalsManager.updateTradersReviewedFlag(con, tradersReviewed,
					writerWrapper.getWriter().getId(), t.getUserId());
			con.commit();
		} catch (SQLException e) {
			logger.log(Level.ERROR, "approvePPWithdraw: ", e);
			try {
				con.rollback();
			} catch (SQLException ie) {
				logger.log(Level.ERROR, "Can't rollback.", ie);
			}
			throw e;
		} finally {
			con.setAutoCommit(true);
			closeConnection(con);
		}
		return true;
	}
	
	public static void saveChequeWithdraw(Transaction t, Cheque c, boolean updateFlagChanges,
			WriterWrapper writerWrapper, long flagSubjectId, boolean flag, boolean tradersReviewed,
			boolean flagAccountingOk, long accountingOkWriter, Date accountingOkDate) throws SQLException {
		Connection con = null;
		try {
			con = getConnection();
			con.setAutoCommit(false);
			TransactionsDAO.update(con, t);
			ChequeDAO.update(con, c);
			if (updateFlagChanges) {
				setFlagChanges(con, t, writerWrapper.getWriter().getId(), flagSubjectId, flag, flagAccountingOk,
						accountingOkWriter, accountingOkDate);
			}
			PendingWithdrawalsManager.updateTradersReviewedFlag(con, tradersReviewed,
					writerWrapper.getWriter().getId(), t.getUserId());
			con.commit();
		} catch (SQLException e) {
			logger.log(Level.ERROR, "saveChequeWithdraw: ", e);
			try {
				con.rollback();
			} catch (SQLException ie) {
				logger.log(Level.ERROR, "Can't rollback.", ie);
			}
			throw e;
		} finally {
			con.setAutoCommit(true);
			closeConnection(con);
		}
	}

	public static void saveWithdraw(Transaction t, boolean updateFlagChanges, WriterWrapper writerWrapper,
			long flagSubjectId, boolean flag, boolean tradersReviewed, boolean flagAccountingOk,
			long accountingOkWriter, Date accountingOkDate) throws SQLException {
		Connection con = null;
		try {
			con = getConnection();
			con.setAutoCommit(false);
			TransactionsDAO.update(con, t);
			if (updateFlagChanges) {
				setFlagChanges(con, t, writerWrapper.getWriter().getId(), flagSubjectId, flag, flagAccountingOk,
						accountingOkWriter, accountingOkDate);
			}
			PendingWithdrawalsManager.updateTradersReviewedFlag(con, tradersReviewed,
					writerWrapper.getWriter().getId(), t.getUserId());
			con.commit();
		} catch (SQLException e) {
			logger.log(Level.ERROR, "saveWithdraw: ", e);
			try {
				con.rollback();
			} catch (SQLException ie) {
				logger.log(Level.ERROR, "Can't rollback.", ie);
			}
			throw e;
		} finally {
			con.setAutoCommit(true);
			closeConnection(con);
		}
	}
	
	public static void setApproveChanges(Connection con, Transaction t, long writerId, boolean flagAccountingOk,
			long accountingOkWriter, Date accountingOkDate) throws SQLException {
		FlagApproveChange flagApproveChange = new FlagApproveChange();
		if (t.getWithdrawalChangesId() > 0) {
			flagApproveChange = getFlagApproveChange(con, t.getWithdrawalChangesId());
		}
		flagApproveChange.setApproveChangeDate(Calendar.getInstance().getTime());
		flagApproveChange.setApproveWriterId(writerId);
		flagApproveChange.setTransactionId(t.getId());
		flagApproveChange.setFlagAccountingOk(flagAccountingOk);
		if (accountingOkWriter == 0) {
			flagApproveChange.setAccountingOkDate(null);		
		} else {
			flagApproveChange.setAccountingOkDate(accountingOkDate);
		}
		updateFlagApproveChange(con, flagApproveChange);
	}
	
	public static void setFlagChanges(Connection con, Transaction t, long writerId, long flagSubjectId, boolean flag,
			boolean flagAccountingOk, long accountingOkWriter, Date accountingOkDate) throws SQLException {
		FlagApproveChange flagApproveChange = new FlagApproveChange();
		if (t.getWithdrawalChangesId() > 0) {
			flagApproveChange = getFlagApproveChange(con, t.getWithdrawalChangesId());
		}
		flagApproveChange.setFlagChangeDate(Calendar.getInstance().getTime());
		flagApproveChange.setTransactionId(t.getId());
		if (flagSubjectId != 0) {
			flagApproveChange.setFlagWriterId(writerId);
			flagApproveChange.setFlagSubjectId(flagSubjectId);
			flagApproveChange.setFlag(flag);
		}
		flagApproveChange.setFlagAccountingOk(flagAccountingOk);
		flagApproveChange.setAccountingOkWriterId(accountingOkWriter);
		if (accountingOkWriter == 0) {
			flagApproveChange.setAccountingOkDate(null);		
		} else {
			flagApproveChange.setAccountingOkDate(accountingOkDate);
		}
		updateFlagApproveChange(con, flagApproveChange);
	}
	
	public static FlagApproveChange getFlagApproveChange(Connection con, long withdrawChangeId) throws SQLException {
		return PendingWithdrawsDAO.getFlagApproveChange(con, withdrawChangeId);
	}
	
	public static void updateFlagApproveChange(Connection con, FlagApproveChange flagApproveChange) throws SQLException {
		PendingWithdrawsDAO.updateFlagApproveChange(con, flagApproveChange);
	}

	public static void updateTradersReviewedFlag(Connection con, boolean tradersReviewed, long writerId,
												 long userId) throws SQLException {
			PendingWithdrawsDAO.updateTradersReviewedFlag(con, tradersReviewed, writerId, userId);
	}
}
