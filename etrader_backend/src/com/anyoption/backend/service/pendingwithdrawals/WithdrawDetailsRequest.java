package com.anyoption.backend.service.pendingwithdrawals;

import com.anyoption.common.service.requests.UserMethodRequest;

public class WithdrawDetailsRequest extends UserMethodRequest {
	
	private long transactionId;

	public long getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(long transactionId) {
		this.transactionId = transactionId;
	}
}
