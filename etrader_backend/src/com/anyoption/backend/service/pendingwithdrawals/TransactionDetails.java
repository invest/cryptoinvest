package com.anyoption.backend.service.pendingwithdrawals;

public class TransactionDetails {

	private long balance;
	private long winLose;
	private boolean controlApproved;
	private long controlApprovedDate;
	private long investmentsCount;
	private long bonusReceived;
	private long bonusUsed;
	private long activeBonuses;
	private boolean wageringWaved;
	private boolean tradersReviewed;
	private long tradersReviewedDate;
	private long tradersReviewedWriterId;
	private String tradersReviewedWriter;
	private long successfulWithdrawsCount;
	private long sumWithdraws;
	private boolean approvedId;
	private boolean approvedUtilityBill;
	private boolean approvedCreditCard;
	private boolean displayCC;
	private long signedFormDate;
	private String signedFormName;
	private long totalDeposits;
	private long totalDepositsUSD;
	private boolean userRegulated;
	private boolean isHavePendingDeposit;
	
	public long getBalance() {
		return balance;
	}
	
	public void setBalance(long balance) {
		this.balance = balance;
	}
	
	public long getWinLose() {
		return winLose;
	}
	
	public void setWinLose(long winLose) {
		this.winLose = winLose;
	}
	
	public boolean isControlApproved() {
		return controlApproved;
	}
	
	public void setControlApproved(boolean controlApproved) {
		this.controlApproved = controlApproved;
	}
	
	public long getControlApprovedDate() {
		return controlApprovedDate;
	}
	
	public void setControlApprovedDate(long controlApprovedDate) {
		this.controlApprovedDate = controlApprovedDate;
	}
	
	public long getInvestmentsCount() {
		return investmentsCount;
	}
	
	public void setInvestmentsCount(long investmentsCount) {
		this.investmentsCount = investmentsCount;
	}
	
	public long getBonusReceived() {
		return bonusReceived;
	}
	
	public void setBonusReceived(long bonusReceived) {
		this.bonusReceived = bonusReceived;
	}
	
	public long getBonusUsed() {
		return bonusUsed;
	}
	
	public void setBonusUsed(long bonusUsed) {
		this.bonusUsed = bonusUsed;
	}
	
	public long getActiveBonuses() {
		return activeBonuses;
	}
	
	public void setActiveBonuses(long activeBonuses) {
		this.activeBonuses = activeBonuses;
	}
	
	public boolean isWageringWaved() {
		return wageringWaved;
	}
	
	public void setWageringWaved(boolean wageringWaved) {
		this.wageringWaved = wageringWaved;
	}
	
	public boolean isTradersReviewed() {
		return tradersReviewed;
	}

	public void setTradersReviewed(boolean tradersReviewed) {
		this.tradersReviewed = tradersReviewed;
	}

	public long getTradersReviewedDate() {
		return tradersReviewedDate;
	}

	public void setTradersReviewedDate(long tradersReviewedDate) {
		this.tradersReviewedDate = tradersReviewedDate;
	}

	public long getTradersReviewedWriterId() {
		return tradersReviewedWriterId;
	}

	public void setTradersReviewedWriterId(long tradersReviewedWriterId) {
		this.tradersReviewedWriterId = tradersReviewedWriterId;
	}

	public String getTradersReviewedWriter() {
		return tradersReviewedWriter;
	}

	public void setTradersReviewedWriter(String tradersReviewedWriter) {
		this.tradersReviewedWriter = tradersReviewedWriter;
	}

	public long getSuccessfulWithdrawsCount() {
		return successfulWithdrawsCount;
	}
	
	public void setSuccessfulWithdrawsCount(long successfulWithdrawsCount) {
		this.successfulWithdrawsCount = successfulWithdrawsCount;
	}
	
	public long getSumWithdraws() {
		return sumWithdraws;
	}
	
	public void setSumWithdraws(long sumWithdraws) {
		this.sumWithdraws = sumWithdraws;
	}
	
	public boolean isApprovedId() {
		return approvedId;
	}

	public void setApprovedId(boolean approvedId) {
		this.approvedId = approvedId;
	}

	public boolean isApprovedUtilityBill() {
		return approvedUtilityBill;
	}

	public void setApprovedUtilityBill(boolean approvedUtilityBill) {
		this.approvedUtilityBill = approvedUtilityBill;
	}

	public boolean isApprovedCreditCard() {
		return approvedCreditCard;
	}

	public void setApprovedCreditCard(boolean approvedCreditCard) {
		this.approvedCreditCard = approvedCreditCard;
	}

	public boolean isDisplayCC() {
		return displayCC;
	}

	public void setDisplayCC(boolean displayCC) {
		this.displayCC = displayCC;
	}

	public long getSignedFormDate() {
		return signedFormDate;
	}
	
	public void setSignedFormDate(long signedFormDate) {
		this.signedFormDate = signedFormDate;
	}
	
	
	public long getTotalDeposits() {
		return totalDeposits;
	}
	
	public void setTotalDeposits(long totalDeposits) {
		this.totalDeposits = totalDeposits;
	}
	
	public long getTotalDepositsUSD() {
		return totalDepositsUSD;
	}
	
	public void setTotalDepositsUSD(long totalDepositsUSD) {
		this.totalDepositsUSD = totalDepositsUSD;
	}

	public boolean isUserRegulated() {
		return userRegulated;
	}

	public void setUserRegulated(boolean userRegulated) {
		this.userRegulated = userRegulated;
	}

	public String getSignedFormName() {
		return signedFormName;
	}

	public void setSignedFormName(String signedFormName) {
		this.signedFormName = signedFormName;
	}

	public boolean isHavePendingDeposit() {
		return isHavePendingDeposit;
	}

	public void setHavePendingDeposit(boolean isHavePendingDeposit) {
		this.isHavePendingDeposit = isHavePendingDeposit;
	}
}
