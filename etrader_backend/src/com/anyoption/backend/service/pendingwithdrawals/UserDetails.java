package com.anyoption.backend.service.pendingwithdrawals;

public class UserDetails {
	
	private long currencyId;
	private String userName;
	private boolean activeUser;
	private String userFirstName;
	private String userLastName;
	private String userAddress;
	private String countryName;
	private long countryId;
	private String mobilePhone;
	private String landlinePhone;
	private String lastFourDigitsCCnumber;
	private String bin;
	private String beneficiaryName;
	private String swift;
	private String iban;
	private long accountNumber;
	private String bankName;
	private String branchAddress;
	private String branchNumber;
	private String skrillEmail;
	private String paypalEmail;
	private String source;
	private String firstApproveWriter;
	private long firstApproveDate;
	private long amount;
	private long amountUSD;
	private long feeAmount;
	private long clearingProviderId;
	private String flagWriter;
	private long flagSubject;
	private long flagDate;
	private boolean flag;
	private String comments;
	private Boolean feeCancel;
	private boolean accountingOkFlag;
	private long accountingOkWriterId;
	private String accountingOkWriter;
	private long accountingOkDate;

	public long getCurrencyId() {
		return currencyId;
	}
	
	public void setCurrencyId(long currencyId) {
		this.currencyId = currencyId;
	}
	
	public String getUserName() {
		return userName;
	}
	
	public void setUserName(String userName) {
		this.userName = userName;
	}
	
	public boolean isActiveUser() {
		return activeUser;
	}
	
	public void setActiveUser(boolean activeUser) {
		this.activeUser = activeUser;
	}
	
	public String getUserFirstName() {
		return userFirstName;
	}
	
	public void setUserFirstName(String userFirstName) {
		this.userFirstName = userFirstName;
	}
	
	public String getUserLastName() {
		return userLastName;
	}
	
	public void setUserLastName(String userLastName) {
		this.userLastName = userLastName;
	}
	
	public String getUserAddress() {
		return userAddress;
	}
	
	public void setUserAddress(String userAddress) {
		this.userAddress = userAddress;
	}
	
	public String getCountryName() {
		return countryName;
	}
	
	public void setCountryName(String countryName) {
		this.countryName = countryName;
	}
	
	public String getMobilePhone() {
		return mobilePhone;
	}
	
	public void setMobilePhone(String mobilePhone) {
		this.mobilePhone = mobilePhone;
	}
	
	public String getLandlinePhone() {
		return landlinePhone;
	}
	
	public void setLandlinePhone(String landlinePhone) {
		this.landlinePhone = landlinePhone;
	}
	
	public String getLastFourDigitsCCnumber() {
		return lastFourDigitsCCnumber;
	}
	
	public void setLastFourDigitsCCnumber(String lastFourDigitsCCnumber) {
		this.lastFourDigitsCCnumber = lastFourDigitsCCnumber;
	}
	
	public String getBeneficiaryName() {
		return beneficiaryName;
	}
	
	public void setBeneficiaryName(String beneficiaryName) {
		this.beneficiaryName = beneficiaryName;
	}
	
	public String getSwift() {
		return swift;
	}
	
	public void setSwift(String swift) {
		this.swift = swift;
	}
	
	public String getIban() {
		return iban;
	}
	
	public void setIban(String iban) {
		this.iban = iban;
	}
	
	public long getAccountNumber() {
		return accountNumber;
	}
	
	public void setAccountNumber(long accountNumber) {
		this.accountNumber = accountNumber;
	}
	
	public String getBankName() {
		return bankName;
	}
	
	public void setBankName(String bankName) {
		this.bankName = bankName;
	}
	
	public String getSkrillEmail() {
		return skrillEmail;
	}
	
	public void setSkrillEmail(String skrillEmail) {
		this.skrillEmail = skrillEmail;
	}
	
	public String getPaypalEmail() {
		return paypalEmail;
	}

	public void setPaypalEmail(String paypalEmail) {
		this.paypalEmail = paypalEmail;
	}

	public String getSource() {
		return source;
	}
	
	public void setSource(String source) {
		this.source = source;
	}
	
	public String getFirstApproveWriter() {
		return firstApproveWriter;
	}

	public void setFirstApproveWriter(String firstApproveWriter) {
		this.firstApproveWriter = firstApproveWriter;
	}

	public long getFirstApproveDate() {
		return firstApproveDate;
	}

	public void setFirstApproveDate(long firstApproveDate) {
		this.firstApproveDate = firstApproveDate;
	}

	public long getAmount() {
		return amount;
	}
	
	public void setAmount(long amount) {
		this.amount = amount;
	}
	
	public long getAmountUSD() {
		return amountUSD;
	}
	
	public void setAmountUSD(long amountUSD) {
		this.amountUSD = amountUSD;
	}
	
	public long getFeeAmount() {
		return feeAmount;
	}

	public void setFeeAmount(long feeAmount) {
		this.feeAmount = feeAmount;
	}

	public long getClearingProviderId() {
		return clearingProviderId;
	}

	public void setClearingProviderId(long clearingProviderId) {
		this.clearingProviderId = clearingProviderId;
	}

	public String getFlagWriter() {
		return flagWriter;
	}

	public void setFlagWriter(String flagWriter) {
		this.flagWriter = flagWriter;
	}

	public long getFlagSubject() {
		return flagSubject;
	}

	public void setFlagSubject(long flagSubject) {
		this.flagSubject = flagSubject;
	}

	public long getFlagDate() {
		return flagDate;
	}

	public void setFlagDate(long flagDate) {
		this.flagDate = flagDate;
	}

	public String getComments() {
		return comments;
	}
	
	public void setComments(String comments) {
		this.comments = comments;
	}
	
	public String getBin() {
		return bin;
	}

	public void setBin(String bin) {
		this.bin = bin;
	}

	public Boolean isFeeCancel() {
		return feeCancel;
	}

	public void setFeeCancel(Boolean feeCancel) {
		this.feeCancel = feeCancel;
	}

	public long getCountryId() {
		return countryId;
	}

	public void setCountryId(long countryId) {
		this.countryId = countryId;
	}

	public boolean isFlag() {
		return flag;
	}

	public void setFlag(boolean flag) {
		this.flag = flag;
	}

	public String getBranchAddress() {
		return branchAddress;
	}

	public void setBranchAddress(String branchAddress) {
		this.branchAddress = branchAddress;
	}

	public String getBranchNumber() {
		return branchNumber;
	}

	public void setBranchNumber(String branchNumber) {
		this.branchNumber = branchNumber;
	}

	public boolean isAccountingOkFlag() {
		return accountingOkFlag;
	}

	public void setAccountingOkFlag(boolean accountingOkFlag) {
		this.accountingOkFlag = accountingOkFlag;
	}

	public long getAccountingOkWriterId() {
		return accountingOkWriterId;
	}

	public void setAccountingOkWriterId(long accountingOkWriterId) {
		this.accountingOkWriterId = accountingOkWriterId;
	}

	public String getAccountingOkWriter() {
		return accountingOkWriter;
	}

	public void setAccountingOkWriter(String accountingOkWriter) {
		this.accountingOkWriter = accountingOkWriter;
	}

	public long getAccountingOkDate() {
		return accountingOkDate;
	}

	public void setAccountingOkDate(long accountingOkDate) {
		this.accountingOkDate = accountingOkDate;
	}
}