package com.anyoption.backend.service.pendingwithdrawals;

import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import com.anyoption.backend.managers.TransactionManager;
import com.anyoption.backend.managers.WriterPermisionsManager;
import com.anyoption.backend.service.writerpermissions.ScreenSettingsResponse;
import com.anyoption.common.backend.annotations.BackendPermission;
import com.anyoption.common.beans.Transaction;
import com.anyoption.common.bl_vos.WireBase;
import com.anyoption.common.managers.CountryManagerBase;
import com.anyoption.common.payments.WithdrawEntryInfo;
import com.anyoption.common.payments.WithdrawEntryResult;
import com.anyoption.common.payments.WithdrawUtil;
import com.anyoption.common.service.CommonJSONService;
import com.anyoption.common.service.requests.UserMethodRequest;
import com.anyoption.common.service.results.MethodResult;

import il.co.etrader.backend.bl_managers.AdminManager;
import il.co.etrader.backend.bl_managers.PopulationsManager;
import il.co.etrader.backend.bl_managers.TransactionsManager;
import il.co.etrader.backend.bl_vos.User;
import il.co.etrader.backend.bl_vos.WriterWrapper;
import il.co.etrader.backend.util.Constants;
import il.co.etrader.bl_managers.ClearingManager;
import il.co.etrader.bl_managers.SkinsManagerBase;
import il.co.etrader.bl_managers.TransactionsManagerBase;
import il.co.etrader.bl_managers.UsersManagerBase;
import il.co.etrader.bl_vos.Cheque;
import il.co.etrader.bl_vos.Template;
import il.co.etrader.bl_vos.UserBase;
import il.co.etrader.bl_vos.Writer;
import il.co.etrader.util.CommonUtil;
import il.co.etrader.util.ConstantsBase;

/**
 * 
 * @author ivan.petkov
 *
 */
public class PendingWithdrawalsServices {

	private static final Logger logger = Logger.getLogger(PendingWithdrawalsServices.class);
	private static final String SCREEN_NAME_FIRST_APPROVE = "pendingWithdrawalFirstApprove";
	private static final String SCREEN_NAME_SECOND_APPROVE = "pendingWithdrawalSecondApprove";
	private static final String SCREEN_NAME_SUPPORT = "pendingWithdrawalSupport";
	private static final String FILTER_ACCOUNTING_OK = "accountingOk";
	private static final String FILTER_YES = "Yes";
	private static final String FILTER_NO = "No";	
	
	public static ScreenSettingsResponse getPendingWithdrawalsScreenSettings(UserMethodRequest request, HttpServletRequest httpRequest, ScreenSettingsResponse response, String screenName) {
		HashMap<String, HashMap<Long, String>> filters = new HashMap<>();
		WriterWrapper writer = (WriterWrapper) httpRequest.getSession().getAttribute(ConstantsBase.BIND_SESSION_WRITER_WRAPPER);
		
		WriterPermisionsManager.setWriterScreenPermissionsByScreenName(screenName, response, httpRequest);
		
		//FILTERS
		//business case
		HashMap<Long, String> skinBusinessCases = TransactionManager.getSkinBusinessCases();
		filters.put(TransactionManager.FILTER_SKIN_BUSINESS_CASES, skinBusinessCases);
		
		//skins
		HashMap<Long, String> skins = WriterPermisionsManager.filterSkinsBasedOnWriterAttribution(SkinsManagerBase.getAllSkinsFilter(), writer, logger);
		filters.put(SkinsManagerBase.FILTER_SKINS, skins);
		
		//countries
		HashMap<Long, String> countries = CountryManagerBase.getAllCountriesFilter();
		filters.put(CountryManagerBase.FILTER_COUNTRIES, countries);
		
		//Users
		HashMap<Long, String> usersClass = TransactionManager.getUserClasses();
		if (usersClass.containsKey(3L)) {	//API
			usersClass.remove(3L);
		}
		filters.put(TransactionManager.FILTER_CLASS_USERS, usersClass);
		
		//currencies
		HashMap<Long, String> currencies = TransactionManager.getAllCurrencies();
		filters.put(TransactionManager.FILTER_CURRENCIES, currencies);
		
		//types
		HashMap<Long, String> pendingWithdrawTypes = TransactionManager.getPendingWithdrawTypes();
		filters.put(TransactionManager.FILTER_PENDING_WITHDRAW_TYPES, pendingWithdrawTypes);
		
		//User invested?
		HashMap<Long, String> userInvested = PendingWithdrawalsManager.getUserInvestedFilter();
		filters.put(PendingWithdrawalsManager.FILTER_USER_INVESTED, userInvested);
		
		//Past Withdraws
		HashMap<Long, String> pastWithdraws = PendingWithdrawalsManager.getPastWithdrawsFilter();
		filters.put(PendingWithdrawalsManager.FILTER_PAST_WITHDRAWS, pastWithdraws);
		
		//Withdraw status
		HashMap<Long, String> withdrawStatuses = PendingWithdrawalsManager.getWithdrawStatusFilter();
		filters.put(PendingWithdrawalsManager.FILTER_WITHDRAW_STATUS, withdrawStatuses);
		
		try {
			//flag subject
			HashMap<Long, String> flagSubjects = PendingWithdrawalsManager.getFlagSubjectsFilter();
			filters.put(PendingWithdrawalsManager.FILTER_FLAG_SUBJECTS, flagSubjects);
		} catch (SQLException sqle) {
			logger.error("Unable to get flag subjects! ", sqle);
			response.setErrorCode(CommonJSONService.ERROR_CODE_UNKNOWN);
		}
		//Clearing providers
		HashMap<Long, String> clearingProviders = ClearingManager.getClearingProviderCCNames();
		filters.put(PendingWithdrawalsManager.FILTER_CLEARING_PROVIDERS, clearingProviders);

		HashMap<Long, String> accountingOk = new HashMap<Long, String>();
		accountingOk.put(new Long (ConstantsBase.FILTER_YES), FILTER_YES);
		accountingOk.put(new Long (ConstantsBase.FILTER_NO), FILTER_NO);
		filters.put(FILTER_ACCOUNTING_OK, accountingOk);
		
		response.setScreenFilters(filters);
		return response;
	}
	
	@BackendPermission(id = "pendingWithdrawalFirstApprove_view")
	public static ScreenSettingsResponse getFirstApproveScreenSettings(UserMethodRequest request, HttpServletRequest httpRequest) {
		ScreenSettingsResponse response = new ScreenSettingsResponse();
		getPendingWithdrawalsScreenSettings(request, httpRequest, response, SCREEN_NAME_FIRST_APPROVE);
		return response;
	}
	
	@BackendPermission(id = "pendingWithdrawalSecondApprove_view")
	public static ScreenSettingsResponse getSecondApproveScreenSettings(UserMethodRequest request, HttpServletRequest httpRequest) {
		ScreenSettingsResponse response = new ScreenSettingsResponse();
		getPendingWithdrawalsScreenSettings(request, httpRequest, response, SCREEN_NAME_SECOND_APPROVE);
		return response;
	}
	
	@BackendPermission(id = "pendingWithdrawalSupport_view")
	public static ScreenSettingsResponse getSupportScreenSettings(UserMethodRequest request, 
																						HttpServletRequest httpRequest) {
		ScreenSettingsResponse response = new ScreenSettingsResponse();
		getPendingWithdrawalsScreenSettings(request, httpRequest, response, SCREEN_NAME_SUPPORT);
		return response;
	}
	
	public static void searchPendingWithdraws(PendingWithdrawsMethodRequest request, HttpServletRequest httpRequest, PendingWithdrawsMethodResponse response, boolean isSecondApproval) {
		try {
			WriterWrapper writerWrapper = (WriterWrapper) httpRequest.getSession().getAttribute(ConstantsBase.BIND_SESSION_WRITER_WRAPPER);
			ArrayList<Integer> skinsList = request.getPendingWithdraw().getSkins();
			skinsList = WriterPermisionsManager.setPermitedSkinsForWriter(skinsList, writerWrapper);
			request.getPendingWithdraw().setSkins(skinsList);
			ArrayList<PendingWithdraw> pendingWithdraws = PendingWithdrawalsManager.getPendingWithdraws(request.getPendingWithdraw(), isSecondApproval);
			response.setPendingWithdraw(pendingWithdraws);
		} catch (SQLException e) {
			logger.error("Unable to get pending withdraws! ", e);
			response.setErrorCode(CommonJSONService.ERROR_CODE_UNKNOWN);
		}
	}
	
	@BackendPermission(id = "pendingWithdrawalFirstApprove_view")
	public static PendingWithdrawsMethodResponse searchFirstApproveWithdraws(PendingWithdrawsMethodRequest request, 
																					HttpServletRequest httpRequest) {
		logger.debug("searchFirstApproveWithdraws: " + request);
		PendingWithdrawsMethodResponse response = new PendingWithdrawsMethodResponse();
		request.getPendingWithdraw().setStatus(PendingWithdrawalsManager.PENDING_WITHDRAW_STATUS_REQUESTED);
		request.getPendingWithdraw().setScreen(PendingWithdrawalsManager.PENDING_WITHDRAW_SCREEN_FIRST_APPROVE);
		searchPendingWithdraws(request, httpRequest, response, false);
		return response;
	}
	
	@BackendPermission(id = "pendingWithdrawalSecondApprove_view")
	public static PendingWithdrawsMethodResponse searchSecondApproveWithdraws(PendingWithdrawsMethodRequest request, 
																					HttpServletRequest httpRequest) {
		logger.debug("searchSecondApproveWithdraws: " + request);
		PendingWithdrawsMethodResponse response = new PendingWithdrawsMethodResponse();
		request.getPendingWithdraw().setStatus(PendingWithdrawalsManager.PENDING_WITHDRAW_STATUS_FIRST_APPROVED);
		request.getPendingWithdraw().setScreen(PendingWithdrawalsManager.PENDING_WITHDRAW_SCREEN_SECOND_APPROVE);
		searchPendingWithdraws(request, httpRequest, response, true);
		return response;
	}
	
	@BackendPermission(id = "pendingWithdrawalSupport_view")
	public static PendingWithdrawsMethodResponse searchSupportWithdraws(PendingWithdrawsMethodRequest request, 
																					HttpServletRequest httpRequest) {
		logger.debug("searchSupportWithdraws: " + request);
		PendingWithdrawsMethodResponse response = new PendingWithdrawsMethodResponse();
		request.getPendingWithdraw().setScreen(PendingWithdrawalsManager.PENDING_WITHDRAW_SCREEN_SUPPORT);
		searchPendingWithdraws(request, httpRequest, response, false);
		return response;
	}
	
	public static UserDetailsResponse getUserDetails(WithdrawDetailsRequest request, UserDetailsResponse response) {
		try {
			UserDetails userDetails = PendingWithdrawalsManager.getUserDetails(request.getTransactionId());
			response.setUserDetails(userDetails);
		} catch (SQLException e) {
			logger.error("Unable to get user details! ", e);
			response.setErrorCode(CommonJSONService.ERROR_CODE_UNKNOWN);
		}
		return response;
	}
	
	@BackendPermission(id = "pendingWithdrawalFirstApprove_view")
	public static UserDetailsResponse getFirstApproveUserDetails(WithdrawDetailsRequest request) {
		logger.debug("getFirstApproveWithdrawDetails: " + request);
		UserDetailsResponse response = new UserDetailsResponse();
		getUserDetails(request, response);
		return response;
	}
	
	@BackendPermission(id = "pendingWithdrawalSecondApprove_view")
	public static UserDetailsResponse getSecondApproveUserDetails(WithdrawDetailsRequest request) {
		logger.debug("getFirstApproveWithdrawDetails: " + request);
		UserDetailsResponse response = new UserDetailsResponse();
		getUserDetails(request, response);
		return response;
	}
	
	@BackendPermission(id = "pendingWithdrawalSupport_view")
	public static UserDetailsResponse getSupportUserDetails(WithdrawDetailsRequest request) {
		logger.debug("getFirstApproveWithdrawDetails: " + request);
		UserDetailsResponse response = new UserDetailsResponse();
		getUserDetails(request, response);
		return response;
	}
	
	public static TransactionDetailsResponse getTransactionDetails(WithdrawDetailsRequest request,
																	TransactionDetailsResponse response) {
		try {
			TransactionDetails transactionDetails =
								PendingWithdrawalsManager.getTransactionDetails(request.getTransactionId());
			response.setTransactionDetails(transactionDetails);
		} catch (SQLException e) {
			logger.error("Unable to get transaction details! ", e);
			response.setErrorCode(CommonJSONService.ERROR_CODE_UNKNOWN);
		}
		return response;
	}
	
	@BackendPermission(id = "pendingWithdrawalFirstApprove_view")
	public static TransactionDetailsResponse getFirstApproveTransactionDetails(WithdrawDetailsRequest request) {
		logger.debug("getFirstApproveWithdrawDetails: " + request);
		TransactionDetailsResponse response = new TransactionDetailsResponse();
		getTransactionDetails(request, response);
		return response;
	}
	
	@BackendPermission(id = "pendingWithdrawalSecondApprove_view")
	public static TransactionDetailsResponse getSecondApproveTransactionDetails(WithdrawDetailsRequest request) {
		logger.debug("getFirstApproveWithdrawDetails: " + request);
		TransactionDetailsResponse response = new TransactionDetailsResponse();
		getTransactionDetails(request, response);
		return response;
	}
	
	@BackendPermission(id = "pendingWithdrawalSupport_view")
	public static TransactionDetailsResponse getSupportTransactionDetails(WithdrawDetailsRequest request) {
		logger.debug("getFirstApproveWithdrawDetails: " + request);
		TransactionDetailsResponse response = new TransactionDetailsResponse();
		getTransactionDetails(request, response);
		return response;
	}
	
	public static DepositSummaryDetailsResponse getDepositSummaryDetails(WithdrawDetailsRequest request, DepositSummaryDetailsResponse response) {
		try {
			ArrayList<DepositSummary> depositSummaryDetails = PendingWithdrawalsManager.getDepositSummaryDetails(request.getTransactionId(), 0);
			response.setDepositSummaryList(depositSummaryDetails);
		} catch (SQLException e) {
			logger.error("Unable to get deposit summary details! ", e);
			response.setErrorCode(CommonJSONService.ERROR_CODE_UNKNOWN);
		}
		return response;
	}
	
	@BackendPermission(id = "pendingWithdrawalFirstApprove_view")
	public static DepositSummaryDetailsResponse getFirstApproveDepositSummaryDetails(WithdrawDetailsRequest request) {
		logger.debug("getFirstApproveWithdrawDetails: " + request);
		DepositSummaryDetailsResponse response = new DepositSummaryDetailsResponse();
		getDepositSummaryDetails(request, response);
		return response;
	}
	
	@BackendPermission(id = "pendingWithdrawalSecondApprove_view")
	public static DepositSummaryDetailsResponse getSecondApproveDepositSummaryDetails(WithdrawDetailsRequest request) {
		logger.debug("getFirstApproveWithdrawDetails: " + request);
		DepositSummaryDetailsResponse response = new DepositSummaryDetailsResponse();
		getDepositSummaryDetails(request, response);
		return response;
	}
	
	@BackendPermission(id = "pendingWithdrawalSupport_view")
	public static DepositSummaryDetailsResponse getSupportDepositSummaryDetails(WithdrawDetailsRequest request) {
		logger.debug("getFirstApproveWithdrawDetails: " + request);
		DepositSummaryDetailsResponse response = new DepositSummaryDetailsResponse();
		getDepositSummaryDetails(request, response);
		return response;
	}
	
	private static boolean isCCWithdraw(Transaction transaction) {
		return (transaction.getTypeId() == TransactionsManagerBase.TRANS_TYPE_CC_WITHDRAW);
	}
	
	private static boolean isWireWithdraw(Transaction transaction) {
		return (transaction.getTypeId() == TransactionsManagerBase.TRANS_TYPE_BANK_WIRE_WITHDRAW);
	}
	
	private static boolean isPayPal(Transaction transaction) {
		return (transaction.getTypeId() == TransactionsManagerBase.TRANS_TYPE_PAYPAL_WITHDRAW);
	}

	private static boolean isMoneyBookers(Transaction transaction) {
		return (transaction.getTypeId() == TransactionsManagerBase.TRANS_TYPE_MONEYBOOKERS_WITHDRAW);
	}

	private static boolean isDeltaPAyChinaWithdraw(Transaction transaction) {
		return (transaction.getTypeId() == TransactionsManagerBase.TRANS_TYPE_CUP_CHINA_WITHDRAW);
	}
	private static boolean isCUPWithdraw(Transaction transaction) {
		return (transaction.getTypeId() == TransactionsManagerBase.TRANS_TYPE_CUP_CHINA_WITHDRAW);
	}

	private static boolean isWebMoney(Transaction transaction) {
		return (transaction.getTypeId() == TransactionsManagerBase.TRANS_TYPE_WEBMONEY_WITHDRAW);
	}

	private static boolean isEnvoy(Transaction transaction) {
		return (transaction.getTypeId() == TransactionsManagerBase.TRANS_TYPE_ENVOY_WITHDRAW) ;
	}
	
	private static boolean isBaroPay(Transaction transaction) {
		return (transaction.getTypeId() == TransactionsManagerBase.TRANS_TYPE_BAROPAY_WITHDRAW) ;
	}

	private static boolean isChequeWithdrawl(Transaction transaction) {
		return (transaction.isChequeWithdraw());
	}
	
	private static boolean isDirect24(Transaction transaction) {
		return (transaction.getTypeId() == TransactionsManagerBase.TRANS_TYPE_DIRECT24_WITHDRAW) ;
	}
	
	private static boolean isGiropay(Transaction transaction) {
		return (transaction.getTypeId() == TransactionsManagerBase.TRANS_TYPE_GIROPAY_WITHDRAW) ;
	}
	
	private static boolean isEPS(Transaction transaction) {
		return (transaction.getTypeId() == TransactionsManagerBase.TRANS_TYPE_EPS_WITHDRAW) ;
	}
	
	private static boolean isIdeal(Transaction transaction) {
		return (transaction.getTypeId() == TransactionsManagerBase.TRANS_TYPE_IDEAL_WITHDRAW) ;
	}
	
	static com.anyoption.common.beans.Transaction createFeeTransaction(com.anyoption.common.beans.Transaction mainTransaction, WriterWrapper  w, long feeAmount, int feeType) {
		com.anyoption.common.beans.Transaction feeTransaction = new com.anyoption.common.beans.Transaction();
		feeTransaction.setUserId(mainTransaction.getUserId());
		feeTransaction.setTypeId(feeType);
		feeTransaction.setTimeCreated(new Date());
		feeTransaction.setStatusId(TransactionsManagerBase.TRANS_STATUS_SUCCEED);
		feeTransaction.setWriterId(w.getWriter().getId());
		feeTransaction.setIp(mainTransaction.getIp());
		feeTransaction.setTimeSettled(new Date());
		feeTransaction.setProcessedWriterId(w.getWriter().getId());
		feeTransaction.setReferenceId(new BigDecimal(mainTransaction.getId()));
		feeTransaction.setAuthNumber(mainTransaction.getAuthNumber());
       	feeTransaction.setAmount(feeAmount);
    	feeTransaction.setUtcOffsetCreated(w.getUtcOffset());
    	feeTransaction.setUtcOffsetSettled(w.getUtcOffset());
    	return feeTransaction;
	}
	
	@BackendPermission(id = "pendingWithdrawalFirstApprove_approveWithdraw")
	public static MethodResult firstApproveWithdraw(ApproveWithdrawRequest request, HttpServletRequest httpRequest) {
		logger.debug("firstApproveWithdraw: " + request);
		MethodResult result = new MethodResult();
		approveWithdraw(request, httpRequest, result, false);
		return result;
	}
	
	@BackendPermission(id = "pendingWithdrawalSecondApprove_approveWithdraw")
	public static MethodResult secondApproveWithdraw(ApproveWithdrawRequest request, HttpServletRequest httpRequest) {
		logger.debug("firstApproveWithdraw: " + request);
		MethodResult result = new MethodResult();
		approveWithdraw(request, httpRequest, result, true);
		return result;
	}
	
	@BackendPermission(id = "pendingWithdrawalSecondApprove_cyApprove")
	public static MethodResult cySecondApproveWithdraw(ApproveWithdrawRequest request, HttpServletRequest httpRequest) {
		logger.debug("firstApproveWithdraw: " + request);
		MethodResult result = new MethodResult();
		approveWithdraw(request, httpRequest, result, true);
		return result;
	}
	
	public static MethodResult approveWithdraw(ApproveWithdrawRequest request, HttpServletRequest httpRequest,
												MethodResult result, boolean accounting) {
		if (request.isFlagChecked()) {
			logger.error("Cannot approve flagged pending withdraw! ");
			result.setErrorCode(CommonJSONService.ERROR_CODE_WITHDRAWAL_FLAGGED);
			return result;
		}
		if (request.getClearingProviderId() == 0) {
			logger.error("Cannot approve pending withdraw without clearing provider! ");
			result.setErrorCode(CommonJSONService.ERROR_CODE_WITHDRAWAL_NO_EXPECTED_PROVIDER);
			return result;
		}
		StringBuffer changedProviderComment = new StringBuffer();
		boolean res = false;
		try {
			Transaction transaction = TransactionsManagerBase.getTransaction(request.getTransactionId());
			long fee = 0l;
			Date accountingOkDate = null;
			if (request.getAccountingOkDate() != 0) {
				accountingOkDate = new Date(request.getAccountingOkDate());
			}
			if (!request.isExemptFee()) {
				fee = TransactionsManagerBase.getTransactionHomoFee(transaction.getAmount(), transaction.getCurrencyId(),
																	transaction.getTypeId());
			}
			// Disable negative transaction amount (after fee)
			if ((transaction.getAmount() - fee) <= 0) {
				logger.error("Negative transaction amount!");
				result.setErrorCode(CommonJSONService.ERROR_CODE_WITHDRAWAL_NEGATIVE_AMOUNT);
				return result;
			}
			long sId = transaction.getStatusId();
			transaction.setFeeCancel(request.isExemptFee());
			transaction.setComments(request.getComments());
			if (request.isExpectedProviderChange()) {
				transaction.setClearingProviderId(request.getClearingProviderId());
			}
			WriterWrapper writerWrapper = (WriterWrapper) httpRequest.getSession()
																.getAttribute(ConstantsBase.BIND_SESSION_WRITER_WRAPPER);
			Writer writer = writerWrapper.getWriter();
			String writerName = writer.getUserName();
			long writerId = writer.getId();
			String ip = CommonUtil.getIPAddress(httpRequest);
			User user = (User) httpRequest.getSession().getAttribute(ConstantsBase.BIND_SESSION_USER);
			if (sId == TransactionsManager.TRANS_STATUS_SECOND_APPROVAL	|| sId == TransactionsManager.TRANS_STATUS_SUCCEED) {
				logger.error("Withdraw not in correct status! ");
				result.setErrorCode(CommonJSONService.ERROR_CODE_WITHDRAWAL_WRONG_STATUS);
				return result;
			} else {
				if (isCCWithdraw(transaction) || isCUPWithdraw(transaction) || isBaroPay(transaction)) {
					if (request.isExpectedProviderChange()
							&& request.getClearingProviderId() != transaction.getClearingProviderId()) {
						String providerName = "";
						providerName = ClearingManager.getClearingProviderNames().get(transaction.getClearingProviderId());
						changedProviderComment.append("provider changed from " + providerName);
					}
					
					res = PendingWithdrawalsManager.approveCcWithdraw(transaction, fee, accounting, writerName,
															changedProviderComment, writerWrapper, request.isTradersReviewed(),
															request.isFlagAccountingOk(), request.getAccountingOkWriter(),
															accountingOkDate);
				} else if (isWireWithdraw(transaction) || isDirect24(transaction) || isEPS(transaction)
																				  || isGiropay(transaction)
																				  || isIdeal(transaction)) {
					if (transaction.getWireId() == null || transaction.getWireId().equals("")) {
						logger.error("No wire id for bank withdraw! ");
						result.setErrorCode(CommonJSONService.ERROR_CODE_WITHDRAWAL_NO_BANK_WIRE_ID);
						return result;
					}
					WireBase wire = TransactionsManagerBase.getWire(transaction.getWireId());
					res = PendingWithdrawalsManager.approveBankWireWithdraw(transaction, wire, fee, accounting,
													writerWrapper, request.getBankId(), ip, request.isTradersReviewed(),
													request.isFlagAccountingOk(), request.getAccountingOkWriter(),
													accountingOkDate);
					if (transaction.getStatusId() == TransactionsManagerBase.TRANS_STATUS_SUCCEED) {
						// sending mail to the client
						String ls = System.getProperty("line.separator");
						logger.info("Going to send withdrawal email: " + ls + "userId: " + transaction.getUserId() + ls
								+ "trxId: " + transaction.getId() + ls);
						Template template = new Template();
						template.setSubject("withdrawal.user.email");
						template.setFileName(Constants.TEMPLATE_WITHDRAW_BW_SUCCEED);

						Template t = AdminManager.getTemplateById(Template.WIRE_WITHDRAWAL_SUCCESS_MAIL_ID);
						template.setMailBoxEmailType(t.getMailBoxEmailType());

						// send email to user and to us for tracking
						try {
							UserBase u = new UserBase();
							u.setUserName(UsersManagerBase.getUserNameById(transaction.getUserId()));
							String[] splitTo = CommonUtil.getProperty("succeed.email.to").split(";");
							UsersManagerBase.sendMailTemplateFile(template, writerId, u,
									false, transaction.getCc4digit(), null, transaction.getWireAmountAfterFee(),
									transaction.getWire().getBankNameTxt(), null, splitTo, transaction.getId(), null, 0,
									null, null, null, null, false, null, null, null, null, null, null);
						} catch (Exception e) {
							logger.error("Error, Can't send withdrawal email!", e);
						}
					}
				} else if (isPayPal(transaction)) {
					res = PendingWithdrawalsManager.approvePayPalWithdraw(transaction, fee, accounting,
											request.getPaypalTransactionId(), writerWrapper, request.isTradersReviewed(),
											request.isFlagAccountingOk(), request.getAccountingOkWriter(),
											accountingOkDate);
				} else if (isEnvoy(transaction)) {
					res = PendingWithdrawalsManager.approveEnvoyWithdraw(transaction, fee, accounting, writerId,
																		writerWrapper, request.isTradersReviewed(),
																		request.isFlagAccountingOk(), request.getAccountingOkWriter(),
																		accountingOkDate);
				} else if (isMoneyBookers(transaction) || isWebMoney(transaction)) {
					int feeType = TransactionsManagerBase.TRANS_TYPE_HOMO_FEE;
					if (fee > 0 && !transaction.isFeeCancel() && !transaction.isFeeCancelByAdmin() && accounting) {
						transaction.setAmount(transaction.getAmount() - fee);
						res = PendingWithdrawalsManager.approveGeneralWithdraw(transaction, fee, accounting,
																			   writerWrapper, request.isTradersReviewed(),
																				request.isFlagAccountingOk(), request.getAccountingOkWriter(),
																				accountingOkDate);
						com.anyoption.common.beans.Transaction feeTransaction = createFeeTransaction(transaction,
																						writerWrapper, fee, feeType);
						TransactionsManagerBase.insertFeeTransaction(feeTransaction, transaction.getId());
					} else { // no fee
						res = PendingWithdrawalsManager.approveGeneralWithdraw(transaction, fee, accounting,
																			   writerWrapper, request.isTradersReviewed(),
																				request.isFlagAccountingOk(), request.getAccountingOkWriter(),
																				accountingOkDate);
					}
				} else { // cheqeue withdrawal transaction type
					Cheque cheque = TransactionsManager.getChequeDetails(transaction.getChequeId().longValue());
					if ((cheque.getChequeId() == null || cheque.getChequeId().equals(""))) {
						logger.error("Cheque empty!");
						result.setErrorCode(CommonJSONService.ERROR_CODE_WITHDRAWAL_EMPTY_CHEQUE);
						return result;
					}
					cheque.setFeeCancel(request.isExemptFee());
					TransactionsManager.updateCheque(cheque);
					transaction.setFeeCancelByAdmin(transaction.isFeeCancel());
					TransactionsManager.updateFeeCancelByAdmin(cheque.getFeeCancel(), transaction.getId());
					res = PendingWithdrawalsManager.approveWithdraw(transaction, cheque, fee, accounting,
																	writerWrapper, ip, request.isTradersReviewed(),
																	request.isFlagAccountingOk(), request.getAccountingOkWriter(),
																	accountingOkDate);
				}
				if (transaction.getStatusId() == TransactionsManagerBase.TRANS_STATUS_SUCCEED) {
					WithdrawEntryResult finalWithdrawEntryResult = WithdrawUtil.finalWithdrawEntry(
																	new WithdrawEntryInfo(user, transaction.getId()));
				}
				if (res == false) {
					return result;
				}
				if (accounting) {
					PopulationsManager.removeFromOpenWithdrawPop(transaction.getUserId());
				}
			}
		} catch (SQLException sqle) {
			logger.error("Error approving withdraw! " + sqle);
			result.setErrorCode(CommonJSONService.ERROR_CODE_UNKNOWN);
			return result;
		}
		return result;
	}
	
	@BackendPermission(id = "pendingWithdrawalFirstApprove_saveWithdraw")
	public static MethodResult firstSaveWithdraw(ApproveWithdrawRequest request, HttpServletRequest httpRequest) {
		logger.debug("firstSaveWithdraw: " + request);
		MethodResult result = new MethodResult();
		saveWithdraw(request, result, httpRequest, false);
		return result;
	}
	
	@BackendPermission(id = "pendingWithdrawalSecondApprove_saveWithdraw")
	public static MethodResult secondSaveWithdraw(ApproveWithdrawRequest request, HttpServletRequest httpRequest) {
		logger.debug("secondSaveWithdraw: " + request);
		MethodResult result = new MethodResult();
		saveWithdraw(request, result, httpRequest, false);
		return result;
	}
	
	@BackendPermission(id = "pendingWithdrawalSupport_saveWithdraw")
	public static MethodResult supportSaveWithdraw(ApproveWithdrawRequest request, HttpServletRequest httpRequest) {
		logger.debug("supportSaveWithdraw: " + request);
		MethodResult result = new MethodResult();
		saveWithdraw(request, result, httpRequest, true);
		return result;
	}
	
	public static MethodResult saveWithdraw(ApproveWithdrawRequest request, MethodResult result, HttpServletRequest httpRequest, boolean support) {
		if (!support && request.isFlagChecked() && request.getFlagSubjectId() == 0) {
			logger.error("No flag subject has been selected!");
			result.setErrorCode(CommonJSONService.ERROR_CODE_WITHDRAWAL_NO_FLAG_SUBJECT);
			return result;
		}
		try {
			Transaction transaction = TransactionsManagerBase.getTransaction(request.getTransactionId());
			WriterWrapper writerWrapper = (WriterWrapper) httpRequest.getSession()
								.getAttribute(ConstantsBase.BIND_SESSION_WRITER_WRAPPER);
			boolean updateFlagChanges = false;
			if (support) {
				updateFlagChanges = true;
			} else {
				if (request.isFlagChecked() || request.isAccountingChecked()) {
					updateFlagChanges = true;
				}
			}
			if (request.isExpectedProviderChange()) {
				transaction.setClearingProviderId(request.getClearingProviderId());
			}
			Date tmp = null;
			if (request.getAccountingOkDate() == 0 && request.isAccountingChecked()) {
				tmp = new Date();
			} else {
				tmp = new Date(request.getAccountingOkDate());
			}
			
			transaction.setComments(request.getComments());
			transaction.setFeeCancel(request.isExemptFee());
			if (isChequeWithdrawl(transaction)) {
				Cheque cheque = TransactionsManager.getChequeDetails(transaction.getChequeId().longValue());
				if ((cheque.getChequeId() == null || cheque.getChequeId().equals(""))) {
					logger.error("Cheque empty!");
					result.setErrorCode(CommonJSONService.ERROR_CODE_WITHDRAWAL_EMPTY_CHEQUE);
					return result;
				}
				cheque.setFeeCancel(request.isExemptFee());
				TransactionsManager.updateCheque(cheque);
				transaction.setFeeCancelByAdmin(transaction.isFeeCancel());
				TransactionsManager.updateFeeCancelByAdmin(cheque.getFeeCancel(), transaction.getId());
				PendingWithdrawalsManager.saveChequeWithdraw(transaction, cheque, updateFlagChanges, writerWrapper,
															request.getFlagSubjectId(), request.isFlagChecked(),
															request.isTradersReviewed(), request.isFlagAccountingOk(), 
															request.getAccountingOkWriter(), tmp);
			} else {
				PendingWithdrawalsManager.saveWithdraw(transaction, updateFlagChanges, writerWrapper,
														request.getFlagSubjectId(), request.isFlagChecked(),
														request.isTradersReviewed(), request.isFlagAccountingOk(), 
														request.getAccountingOkWriter(), tmp);
			}
		} catch (SQLException sqle) {
			logger.error("Error saving withdraw! " + sqle);
			result.setErrorCode(CommonJSONService.ERROR_CODE_UNKNOWN);
			return result;
		}
		return result;
	}
}
