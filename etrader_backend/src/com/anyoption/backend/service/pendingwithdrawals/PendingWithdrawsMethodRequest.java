package com.anyoption.backend.service.pendingwithdrawals;

import com.anyoption.common.service.requests.UserMethodRequest;

/**
 * 
 * @author ivan.petkov
 *
 */
public class PendingWithdrawsMethodRequest extends UserMethodRequest {
	
	private PendingWithdrawFilters pendingWithdraw;

	public PendingWithdrawFilters getPendingWithdraw() {
		return pendingWithdraw;
	}

	public void setPendingWithdraw(PendingWithdrawFilters pendingWithdraw) {
		this.pendingWithdraw = pendingWithdraw;
	}
}