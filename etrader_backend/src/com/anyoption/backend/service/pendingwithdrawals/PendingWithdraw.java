package com.anyoption.backend.service.pendingwithdrawals;

public class PendingWithdraw {
	
	private Long transactionId;
	private Long userId;
	private String userName;
	private String skinName;
	private String countryName;
	private Long timeCreated;
	private String typeName;
	private Long typeId;
	private Long amount;
	private Long amountInUSD;
	private boolean invested;
	private boolean pastWithdraws;
	private Long totalCount;
	private Long status;
	private Long currencyId;
	private boolean transactionFlagged;
	private String flagSubject;
	private boolean accountingOkFlag;
	private Long clearingProviderId;
	
	public Long getTransactionId() {
		return transactionId;
	}
	
	public void setTransactionId(Long transactionId) {
		this.transactionId = transactionId;
	}
	
	public Long getUserId() {
		return userId;
	}
	
	public void setUserId(Long userId) {
		this.userId = userId;
	}
	
	public String getUserName() {
		return userName;
	}
	
	public void setUserName(String userName) {
		this.userName = userName;
	}
	
	public String getSkinName() {
		return skinName;
	}
	
	public void setSkinName(String skinName) {
		this.skinName = skinName;
	}
	
	public String getCountryName() {
		return countryName;
	}
	
	public void setCountryName(String countryName) {
		this.countryName = countryName;
	}
	
	public Long getTimeCreated() {
		return timeCreated;
	}
	
	public void setTimeCreated(Long timeCreated) {
		this.timeCreated = timeCreated;
	}
	
	public String getTypeName() {
		return typeName;
	}
	
	public void setTypeName(String typeName) {
		this.typeName = typeName;
	}
	
	public Long getAmount() {
		return amount;
	}
	
	public void setAmount(Long amount) {
		this.amount = amount;
	}
	
	public Long getAmountInUSD() {
		return amountInUSD;
	}
	
	public void setAmountInUSD(Long amountInUSD) {
		this.amountInUSD = amountInUSD;
	}
	
	public boolean isInvested() {
		return invested;
	}
	
	public void setInvested(boolean invested) {
		this.invested = invested;
	}
	
	public boolean isPastWithdraws() {
		return pastWithdraws;
	}
	
	public void setPastWithdraws(boolean pastWithdraws) {
		this.pastWithdraws = pastWithdraws;
	}
	
	public Long getTotalCount() {
		return totalCount;
	}
	
	public void setTotalCount(Long totalCount) {
		this.totalCount = totalCount;
	}
	
	public Long getStatus() {
		return status;
	}
	
	public void setStatus(Long status) {
		this.status = status;
	}
	
	public Long getCurrencyId() {
		return currencyId;
	}

	public void setCurrencyId(Long currencyId) {
		this.currencyId = currencyId;
	}

	public Long getTypeId() {
		return typeId;
	}

	public void setTypeId(Long typeId) {
		this.typeId = typeId;
	}
	
	public boolean isTransactionFlagged() {
		return transactionFlagged;
	}

	public void setTransactionFlagged(boolean transactionFlagged) {
		this.transactionFlagged = transactionFlagged;
	}

	public String getFlagSubject() {
		return flagSubject;
	}

	public void setFlagSubject(String flagSubject) {
		this.flagSubject = flagSubject;
	}

	public boolean isAccountingOkFlag() {
		return accountingOkFlag;
	}

	public void setAccountingOkFlag(boolean accountingOkFlag) {
		this.accountingOkFlag = accountingOkFlag;
	}

	public Long getClearingProviderId() {
		return clearingProviderId;
	}

	public void setClearingProviderId(Long clearingProviderId) {
		this.clearingProviderId = clearingProviderId;
	}

	public String toString() {
        String ls = System.getProperty("line.separator");
        return ls + "PendingWithdrawsMethodRequest: "
            + super.toString() + ls
            + "transactionId: " + transactionId + ls
            + "userId: " + userId + ls
            + "userName: " + userName + ls
            + "skinName: " + skinName + ls
            + "countryName: " + countryName + ls
            + "timeCreated: " + timeCreated + ls
            + "typeName: " + typeName + ls
            + "typeId: " + typeId + ls
            + "amount: " + amount + ls
            + "amountInUSD: " + amountInUSD + ls
            + "invested: " + invested + ls
            + "pastWithdraws: " + pastWithdraws + ls
            + "totalCount: " + totalCount + ls
            + "status: " + status + ls
            + "currencyId: " + currencyId + ls
            + "transactionFlagged: " + transactionFlagged + ls
            + "flagSubject: " + flagSubject + ls
            + "accountingOkFlag: " + accountingOkFlag + ls
        	+ "clearingProviderId: " + clearingProviderId + ls;
	}
}
