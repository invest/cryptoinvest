package com.anyoption.backend.service.pendingwithdrawals;

import com.anyoption.common.service.results.MethodResult;

public class TransactionDetailsResponse extends MethodResult {
	
	private TransactionDetails transactionDetails;

	public TransactionDetails getTransactionDetails() {
		return transactionDetails;
	}

	public void setTransactionDetails(TransactionDetails transactionDetails) {
		this.transactionDetails = transactionDetails;
	}
}
