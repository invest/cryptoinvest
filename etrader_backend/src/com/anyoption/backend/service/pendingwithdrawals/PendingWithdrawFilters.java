package com.anyoption.backend.service.pendingwithdrawals;

import java.util.ArrayList;

public class PendingWithdrawFilters {
	
	private Long from;
	private Long to;
	private Long userId;
	private Long transactionId;
	private Long amountUSDFrom;
	private Long amountUSDTo;
	private ArrayList<Integer> businessCases;
	private ArrayList<Integer> skins;
	private ArrayList<Integer> countries;
	private Long userClasses;
	private ArrayList<Integer> currencies;
	private ArrayList<Integer> transactionTypes;
	private Long invested;
	private Long pastWithdraws;
	private ArrayList<Integer> flagSubjects;
	private Long page;
	private long rowsPerPage;
	private Long status;
	private long screen;
	private long flagAccountingOk;
	
	public Long getFrom() {
		return from;
	}
	
	public void setFrom(Long from) {
		this.from = from;
	}
	
	public Long getTo() {
		return to;
	}
	
	public void setTo(Long to) {
		this.to = to;
	}
	
	public Long getUserId() {
		return userId;
	}
	
	public void setUserId(Long userId) {
		this.userId = userId;
	}
	
	public Long getTransactionId() {
		return transactionId;
	}
	
	public void setTransactionId(Long transactionId) {
		this.transactionId = transactionId;
	}
	
	public Long getAmountUSDFrom() {
		return amountUSDFrom;
	}
	
	public void setAmountUSDFrom(Long amountUSDFrom) {
		this.amountUSDFrom = amountUSDFrom;
	}
	
	public Long getAmountUSDTo() {
		return amountUSDTo;
	}
	
	public void setAmountUSDTo(Long amountUSDTo) {
		this.amountUSDTo = amountUSDTo;
	}
	
	public ArrayList<Integer> getBusinessCases() {
		return businessCases;
	}
	
	public void setBusinessCases(ArrayList<Integer> businessCases) {
		this.businessCases = businessCases;
	}
	
	public ArrayList<Integer> getSkins() {
		return skins;
	}
	
	public void setSkins(ArrayList<Integer> skins) {
		this.skins = skins;
	}
	
	public ArrayList<Integer> getCountries() {
		return countries;
	}
	
	public void setCountries(ArrayList<Integer> countries) {
		this.countries = countries;
	}
	
	public Long getUserClasses() {
		return userClasses;
	}
	
	public void setUserClasses(Long userClasses) {
		this.userClasses = userClasses;
	}
	
	public ArrayList<Integer> getCurrencies() {
		return currencies;
	}
	
	public void setCurrencies(ArrayList<Integer> currencies) {
		this.currencies = currencies;
	}
	
	public ArrayList<Integer> getTransactionTypes() {
		return transactionTypes;
	}
	
	public void setTransactionTypes(ArrayList<Integer> transactionTypes) {
		this.transactionTypes = transactionTypes;
	}
	
	public Long getInvested() {
		return invested;
	}
	
	public void setInvested(Long invested) {
		this.invested = invested;
	}
	
	public Long getPastWithdraws() {
		return pastWithdraws;
	}
	
	public void setPastWithdraws(Long pastWithdraws) {
		this.pastWithdraws = pastWithdraws;
	}
	
	public ArrayList<Integer> getFlagSubject() {
		return flagSubjects;
	}
	
	public void setFlagSubject(ArrayList<Integer> flagSubject) {
		this.flagSubjects = flagSubject;
	}
	
	public Long getPage() {
		return page;
	}

	public void setPage(Long page) {
		this.page = page;
	}

	public long getRowsPerPage() {
		return rowsPerPage;
	}

	public void setRowsPerPage(long rowsPerPage) {
		this.rowsPerPage = rowsPerPage;
	}

	public Long getStatus() {
		return status;
	}

	public void setStatus(Long status) {
		this.status = status;
	}
	
	public ArrayList<Integer> getFlagSubjects() {
		return flagSubjects;
	}

	public void setFlagSubjects(ArrayList<Integer> flagSubjects) {
		this.flagSubjects = flagSubjects;
	}

	public long getScreen() {
		return screen;
	}

	public void setScreen(long screen) {
		this.screen = screen;
	}

	public long getFlagAccountingOk() {
		return flagAccountingOk;
	}

	public void setFlagAccountingOk(long flagAccountingOk) {
		this.flagAccountingOk = flagAccountingOk;
	}

	public String toString() {
        String ls = System.getProperty("line.separator");
        return ls + "PendingWithdrawsMethodRequest: "
            + super.toString()
            + "from: " + from + ls
            + "to: " + to + ls
            + "userId: " + userId + ls
            + "transactionId: " + transactionId + ls
            + "amountUSDFrom: " + amountUSDFrom + ls
            + "amountUSDTo: " + amountUSDTo + ls
            + "businessCases: " + businessCases + ls
            + "skins: " + skins + ls
            + "countries: " + countries + ls
            + "userClasses: " + userClasses + ls
            + "currencies: " + currencies + ls
            + "transactionTypes: " + transactionTypes + ls
            + "invested: " + invested + ls
            + "pastWithdraws: " + pastWithdraws + ls
            + "flagSubjects: " + flagSubjects + ls
            + "page: " + page + ls
            + "rowsPerPage: " + rowsPerPage + ls
            + "status: " + status + ls
            + "flagAccountingOk: " + flagAccountingOk + ls;
	}
}