/**
 * 
 */
package com.anyoption.backend.service.transaction;

import com.anyoption.common.service.requests.UserMethodRequest;

/**
 * @author pavel.tabakov
 *
 */
public class TransactionGetByReferenceId extends UserMethodRequest {

	private long referenceId;

	public long getReferenceId() {
		return referenceId;
	}

	public void setReferenceId(long referenceId) {
		this.referenceId = referenceId;
	}

	public String toString() {
		String ls = System.getProperty("line.separator");
		return ls + "TransactionGetByReferenceId: " + super.toString()
		+ "referenceId: " + referenceId + ls;
	}
}
