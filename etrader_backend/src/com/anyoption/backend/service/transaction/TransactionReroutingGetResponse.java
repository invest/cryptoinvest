/**
 * 
 */
package com.anyoption.backend.service.transaction;

import java.util.ArrayList;

import com.anyoption.common.service.results.MethodResult;

public class TransactionReroutingGetResponse extends MethodResult {
	
	private long typeId;  //1-REROUTE_FROM_TYPE; 2-REROUTE_IDS_TYPE
	private ArrayList<Long> reroutingTransactionId;
	
	public long getTypeId() {
		return typeId;
	}
	public void setTypeId(long typeId) {
		this.typeId = typeId;
	}
	public ArrayList<Long> getReroutingTransactionId() {
		return reroutingTransactionId;
	}
	public void setReroutingTransactionId(ArrayList<Long> reroutingTransactionId) {
		this.reroutingTransactionId = reroutingTransactionId;
	}
		
}
