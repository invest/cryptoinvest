/**
 * 
 */
package com.anyoption.backend.service.transaction;

import java.util.ArrayList;

import com.anyoption.common.beans.Transaction;
import com.anyoption.common.service.requests.UserMethodRequest;

/**
 * @author pavel.tabakov
 *
 */
public class TransactionGetAllMethodRequest extends UserMethodRequest {
	
	private Long from;
	private Long to;
	private Long searchBy; // radio button time created = 1, time settled = 2
	private Long userClasses;
	private ArrayList<Integer> businessCases;
	private ArrayList<Integer> skins;
	private ArrayList<Integer> countries;
	private ArrayList<Integer> currencies; 
	private ArrayList<Integer> statuses;
	private ArrayList<Integer> ccProviders;
	private ArrayList<Integer> transactionTypes;
	private ArrayList<Integer> transactionClassTypes;// not into db procedure
	private Long gateway;
	private Long campaign;
	private ArrayList<Integer> systemWriters;
	private Long ccBin;
	private String ccLast4Digits;
	private Long hasBalance;
	private Long wasRerouted;
	private Long reference;// ex EPG_ID
	private Long page;
	private Long userId;
	private Long transactionId;
	private String receiptNumber;// not into db procedure
	private Long J4postponed;// not into db procedure
	private long isIndividualWritersNeedToBeInclude;
	private long rowsPerPage;
	private Long salesTypes;
	private Long depositSource;
	private Long amountFrom;
	private Long amountTo;
	private ArrayList<Integer> trnIds;// need to set from Client including mother transaction (transactionId property)
	private String writerUtcOffset;
	private Transaction transaction;
	private Long isThreeD; // null ALL, 0 No, 1 Yes
	private ArrayList<Integer> creditCardCountries;


	public ArrayList<Integer> getBusinessCases() {
		return businessCases;
	}

	public void setBusinessCases(ArrayList<Integer> businessCases) {
		this.businessCases = businessCases;
	}

	public ArrayList<Integer> getSkins() {
		return skins;
	}

	public void setSkins(ArrayList<Integer> skins) {
		this.skins = skins;
	}

	public ArrayList<Integer> getCountries() {
		return countries;
	}

	public void setCountries(ArrayList<Integer> countries) {
		this.countries = countries;
	}

	public ArrayList<Integer> getCurrencies() {
		return currencies;
	}

	public void setCurrencies(ArrayList<Integer> currencies) {
		this.currencies = currencies;
	}

	public ArrayList<Integer> getStatuses() {
		return statuses;
	}

	public void setStatuses(ArrayList<Integer> statuses) {
		this.statuses = statuses;
	}

	public ArrayList<Integer> getCcProviders() {
		return ccProviders;
	}

	public void setCcProviders(ArrayList<Integer> ccProviders) {
		this.ccProviders = ccProviders;
	}

	public ArrayList<Integer> getSystemWriters() {
		return systemWriters;
	}

	public void setSystemWriters(ArrayList<Integer> systemWriters) {
		this.systemWriters = systemWriters;
	}

	public ArrayList<Integer> getTransactionTypes() {
		return transactionTypes;
	}

	public Long getFrom() {
		return from;
	}

	public void setFrom(Long from) {
		this.from = from;
	}

	public Long getTo() {
		return to;
	}

	public void setTo(Long to) {
		this.to = to;
	}

	public Long getSearchBy() {
		return searchBy;
	}

	public void setSearchBy(Long searchBy) {
		this.searchBy = searchBy;
	}

	public Long getGateway() {
		return gateway;
	}

	public void setGateway(Long gateway) {
		this.gateway = gateway;
	}

	public Long getCampaign() {
		return campaign;
	}

	public void setCampaign(Long campaign) {
		this.campaign = campaign;
	}

	public Long getCcBin() {
		return ccBin;
	}

	public void setCcBin(Long ccBin) {
		this.ccBin = ccBin;
	}

	public String getCcLast4Digits() {
		return ccLast4Digits;
	}

	public void setCcLast4Digits(String ccLast4Digits) {
		this.ccLast4Digits = ccLast4Digits;
	}

	public Long getHasBalance() {
		return hasBalance;
	}

	public void setHasBalance(Long hasBalance) {
		this.hasBalance = hasBalance;
	}

	public Long getWasRerouted() {
		return wasRerouted;
	}

	public void setWasRerouted(Long wasRerouted) {
		this.wasRerouted = wasRerouted;
	}

	public Long getPage() {
		return page;
	}

	public void setPage(Long page) {
		this.page = page;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public Long getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(Long transactionId) {
		this.transactionId = transactionId;
	}

	public Long getJ4postponed() {
		return J4postponed;
	}

	public void setJ4postponed(Long j4postponed) {
		J4postponed = j4postponed;
	}

	public ArrayList<Integer> getTransactionClassTypes() {
		return transactionClassTypes;
	}

	public void setTransactionClassTypes(ArrayList<Integer> transactionClassTypes) {
		this.transactionClassTypes = transactionClassTypes;
	}

	public String getReceiptNumber() {
		return receiptNumber;
	}

	public void setReceiptNumber(String receiptNumber) {
		this.receiptNumber = receiptNumber;
	}

	public Long getUserClasses() {
		return userClasses;
	}

	public void setUserClasses(Long userClasses) {
		this.userClasses = userClasses;
	}

	public long getIsIndividualWritersNeedToBeInclude() {
		return isIndividualWritersNeedToBeInclude;
	}

	public void setIsIndividualWritersNeedToBeInclude(long isIndividualWritersNeedToBeInclude) {
		this.isIndividualWritersNeedToBeInclude = isIndividualWritersNeedToBeInclude;
	}
	
	public long getRowsPerPage() {
		return rowsPerPage;
	}

	public void setRowsPerPage(long rowsPerPage) {
		this.rowsPerPage = rowsPerPage;
	}
	
	public Long getDepositSource() {
		return depositSource;
	}

	public void setDepositSource(Long depositSource) {
		this.depositSource = depositSource;
	}
	
	public Long getSalesTypes() {
		return salesTypes;
	}

	public void setSalesTypes(Long salesTypes) {
		this.salesTypes = salesTypes;
	}
	
	public Long getReference() {
		return reference;
	}

	public void setReference(Long reference) {
		this.reference = reference;
	}
	
	public Long getAmountFrom() {
		return amountFrom;
	}

	public void setAmountFrom(Long amountFrom) {
		this.amountFrom = amountFrom;
	}

	public Long getAmountTo() {
		return amountTo;
	}

	public void setAmountTo(Long amountTo) {
		this.amountTo = amountTo;
	}
	
	public ArrayList<Integer> getTrnIds() {
		return trnIds;
	}

	public void setTrnIds(ArrayList<Integer> trnIds) {
		this.trnIds = trnIds;
	}
	
	public String getWriterUtcOffset() {
		return writerUtcOffset;
	}

	public void setWriterUtcOffset(String writerUtcOffset) {
		this.writerUtcOffset = writerUtcOffset;
	}
	
	public Transaction getTransaction() {
		return transaction;
	}

	public void setTransaction(Transaction transaction) {
		this.transaction = transaction;
	}

    public Long getIsThreeD() {
		return isThreeD;
	}

	public void setIsThreeD(Long isThreeD) {
		this.isThreeD = isThreeD;
	}
	
	public ArrayList<Integer> getCreditCardCountries() {
		return creditCardCountries;
	}

	public void setCreditCardCountries(ArrayList<Integer> creditCardCountries) {
		this.creditCardCountries = creditCardCountries;
	}

	public String toString() {
        String ls = System.getProperty("line.separator");
        return ls + "TransactionGetAllMethodRequest: "
            + super.toString()
            + "from: " + from + ls
            + "to: " + to + ls
            + "searchBy: " + searchBy + ls
            + "gateway: " + gateway + ls
            + "campaign: " + campaign + ls
            + "ccBin: " + ccBin + ls
            + "ccLast4Digits: " + ccLast4Digits + ls
            + "hasBalance: " + hasBalance + ls
            + "page: " + page + ls
            + "userId: " + userId + ls
            + "transactionId: " + transactionId + ls
            + "receiptNumber: " + receiptNumber + ls
            + "J4postponed: " + J4postponed + ls
            + "encrypt: " + encrypt + ls
            + "isIndividualsWritersNeedToBeInclude: " + isIndividualWritersNeedToBeInclude + ls
            + "rowsPerPage: " + rowsPerPage + ls
            + "salesType: " + salesTypes + ls
            + "depositSource: " + depositSource + ls
            + "reference: " + reference + ls
            + "amountFrom: " + amountFrom + ls
            + "amountTo: " + amountTo + ls
            + "trnIds: " + trnIds + ls
            + "isThreeD: " + isThreeD + ls
            + "writerUtcOffset: " + writerUtcOffset + ls;
    }
}
