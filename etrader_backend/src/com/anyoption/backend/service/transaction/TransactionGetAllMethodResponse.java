/**
 * 
 */
package com.anyoption.backend.service.transaction;

import java.util.ArrayList;

import com.anyoption.common.beans.Transaction;
import com.anyoption.common.service.results.MethodResult;

/**
 * @author pavel.tabakov
 *
 */
public class TransactionGetAllMethodResponse extends MethodResult {
	private ArrayList<Transaction> transactions;
	private long totalCount;
	private long totalSumDeposits;
	private long totalSumWithdraws;
	private long totalCountDeposits;
	private long totalCountWithdraws;
	private long pageTotalDeposits;
	private long pageTotalWithdraws;

	public ArrayList<Transaction> getTransactions() {
		return transactions;
	}

	public void setTransactions(ArrayList<Transaction> transactions) {
		this.transactions = transactions;
	}

	public long getTotalCount() {
		return totalCount;
	}

	public void setTotalCount(long totalCount) {
		this.totalCount = totalCount;
	}

	public long getTotalSumDeposits() {
		return totalSumDeposits;
	}

	public void setTotalSumDeposits(long totalSumDeposits) {
		this.totalSumDeposits = totalSumDeposits;
	}

	public long getTotalSumWithdraws() {
		return totalSumWithdraws;
	}

	public void setTotalSumWithdraws(long totalSumWithdraws) {
		this.totalSumWithdraws = totalSumWithdraws;
	}

	public long getTotalCountDeposits() {
		return totalCountDeposits;
	}

	public void setTotalCountDeposits(long totalCountDeposits) {
		this.totalCountDeposits = totalCountDeposits;
	}

	public long getTotalCountWithdraws() {
		return totalCountWithdraws;
	}

	public void setTotalCountWithdraws(long totalCountWithdraws) {
		this.totalCountWithdraws = totalCountWithdraws;
	}

	public long getPageTotalDeposits() {
		return pageTotalDeposits;
	}

	public void setPageTotalDeposits(long pageTotalDeposits) {
		this.pageTotalDeposits = pageTotalDeposits;
	}

	public long getPageTotalWithdraws() {
		return pageTotalWithdraws;
	}

	public void setPageTotalWithdraws(long pageTotalWithdraws) {
		this.pageTotalWithdraws = pageTotalWithdraws;
	}
	
    public String toString() {
        String ls = System.getProperty("line.separator");
        return ls + "TransactionGetAllMethodResponse: " + ls
        	+ super.toString() + ls
            + "totalCount: " + totalCount + ls
            + "totalSumDeposits: " + totalSumDeposits + ls
            + "totalSumWithdraws: " + totalSumWithdraws + ls
            + "totalCountDeposits: " + totalCountDeposits + ls
            + "totalCountWithdraws: " + totalCountWithdraws + ls
            + "pageTotalDeposits: " + pageTotalDeposits + ls
            + "pageTotalWithdraws: " + pageTotalWithdraws  + ls;
    }
}
