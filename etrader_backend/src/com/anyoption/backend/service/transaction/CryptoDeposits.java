/**
 * 
 */
package com.anyoption.backend.service.transaction;

import java.io.Serializable;

/**
 * @author pavel.t
 *
 */
public class CryptoDeposits implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5471928210446455877L;
	
	  private String name;
      private String  country;
      private long timeCreated;
      private long depositAmount;
      private double depositRate;
      private String currency;
      private long executionDate;
      private long executionAmount;
      private double executionRate;
      private double fee;
      private double clearanceFee;
      private String lang;// ex: "en", "bg"
      private int paymentMethod;
      private long totalDepositUsd;
      private long totalDepositAccCurr;
      private long transactionStatus; // ex: TRANS_STATUS_PENDING
      private String assignee;
      private long cryptoDepositStatus;// --?? number --> Joro must add at front end human readable explanations
      private long cryptoWstatus;
      private String email;
      private long userId;
      private long transactionId;
      private String txId;
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * @return the country
	 */
	public String getCountry() {
		return country;
	}
	/**
	 * @param country the country to set
	 */
	public void setCountry(String country) {
		this.country = country;
	}
	/**
	 * @return the timeCreated
	 */
	public long getTimeCreated() {
		return timeCreated;
	}
	/**
	 * @param timeCreated the timeCreated to set
	 */
	public void setTimeCreated(long timeCreated) {
		this.timeCreated = timeCreated;
	}
	/**
	 * @return the depositAmount
	 */
	public long getDepositAmount() {
		return depositAmount;
	}
	/**
	 * @param depositAmount the depositAmount to set
	 */
	public void setDepositAmount(long depositAmount) {
		this.depositAmount = depositAmount;
	}
	/**
	 * @return the depositRate
	 */
	public double getDepositRate() {
		return depositRate;
	}
	/**
	 * @param depositRate the depositRate to set
	 */
	public void setDepositRate(double depositRate) {
		this.depositRate = depositRate;
	}
	/**
	 * @return the currency
	 */
	public String getCurrency() {
		return currency;
	}
	/**
	 * @param currency the currency to set
	 */
	public void setCurrency(String currency) {
		this.currency = currency;
	}
	/**
	 * @return the executionDate
	 */
	public long getExecutionDate() {
		return executionDate;
	}
	/**
	 * @param executionDate the executionDate to set
	 */
	public void setExecutionDate(long executionDate) {
		this.executionDate = executionDate;
	}
	/**
	 * @return the executionAmount
	 */
	public long getExecutionAmount() {
		return executionAmount;
	}
	/**
	 * @param executionAmount the executionAmount to set
	 */
	public void setExecutionAmount(long executionAmount) {
		this.executionAmount = executionAmount;
	}
	/**
	 * @return the executionRate
	 */
	public double getExecutionRate() {
		return executionRate;
	}
	/**
	 * @param executionRate the executionRate to set
	 */
	public void setExecutionRate(double executionRate) {
		this.executionRate = executionRate;
	}
	/**
	 * @return the fee
	 */
	public double getFee() {
		return fee;
	}
	/**
	 * @param fee the fee to set
	 */
	public void setFee(double fee) {
		this.fee = fee;
	}
	/**
	 * @return the clearanceFee
	 */
	public double getClearanceFee() {
		return clearanceFee;
	}
	/**
	 * @param clearanceFee the clearanceFee to set
	 */
	public void setClearanceFee(double clearanceFee) {
		this.clearanceFee = clearanceFee;
	}
	/**
	 * @return the lang
	 */
	public String getLang() {
		return lang;
	}
	/**
	 * @param lang the lang to set
	 */
	public void setLang(String lang) {
		this.lang = lang;
	}
	/**
	 * @return the paymentMethod
	 */
	public int getPaymentMethod() {
		return paymentMethod;
	}
	/**
	 * @param paymentMethod the paymentMethod to set
	 */
	public void setPaymentMethod(int paymentMethod) {
		this.paymentMethod = paymentMethod;
	}
	/**
	 * @return the totalDepositUsd
	 */
	public long getTotalDepositUsd() {
		return totalDepositUsd;
	}
	/**
	 * @param totalDepositUsd the totalDepositUsd to set
	 */
	public void setTotalDepositUsd(long totalDepositUsd) {
		this.totalDepositUsd = totalDepositUsd;
	}
	/**
	 * @return the totalDepositAccCurr
	 */
	public long getTotalDepositAccCurr() {
		return totalDepositAccCurr;
	}
	/**
	 * @param totalDepositAccCurr the totalDepositAccCurr to set
	 */
	public void setTotalDepositAccCurr(long totalDepositAccCurr) {
		this.totalDepositAccCurr = totalDepositAccCurr;
	}
	/**
	 * @return the transactionStatus
	 */
	public long getTransactionStatus() {
		return transactionStatus;
	}
	/**
	 * @param transactionStatus the transactionStatus to set
	 */
	public void setTransactionStatus(long transactionStatus) {
		this.transactionStatus = transactionStatus;
	}
	/**
	 * @return the assignee
	 */
	public String getAssignee() {
		return assignee;
	}
	/**
	 * @param assignee the assignee to set
	 */
	public void setAssignee(String assignee) {
		this.assignee = assignee;
	}
	/**
	 * @return the cryptoDepositStatus
	 */
	public long getCryptoDepositStatus() {
		return cryptoDepositStatus;
	}
	/**
	 * @param cryptoDepositStatus the cryptoDepositStatus to set
	 */
	public void setCryptoDepositStatus(long cryptoDepositStatus) {
		this.cryptoDepositStatus = cryptoDepositStatus;
	}
	/**
	 * @return the cryptoWstatus
	 */
	public long getCryptoWstatus() {
		return cryptoWstatus;
	}
	/**
	 * @param cryptoWstatus the cryptoWstatus to set
	 */
	public void setCryptoWstatus(long cryptoWstatus) {
		this.cryptoWstatus = cryptoWstatus;
	}
	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}
	/**
	 * @param email the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}
	/**
	 * @return the userId
	 */
	public long getUserId() {
		return userId;
	}
	/**
	 * @param userId the userId to set
	 */
	public void setUserId(long userId) {
		this.userId = userId;
	}
	/**
	 * @return the transactionId
	 */
	public long getTransactionId() {
		return transactionId;
	}
	/**
	 * @param transactionId the transactionId to set
	 */
	public void setTransactionId(long transactionId) {
		this.transactionId = transactionId;
	}
	public String getTxId() {
		return txId;
	}
	public void setTxId(String txId) {
		this.txId = txId;
	}
	
	@Override
	public String toString() {
		return "CryptoDeposits [name=" + name + ", country=" + country + ", timeCreated=" + timeCreated
				+ ", depositAmount=" + depositAmount + ", depositRate=" + depositRate + ", currency=" + currency
				+ ", executionDate=" + executionDate + ", executionAmount=" + executionAmount + ", executionRate="
				+ executionRate + ", fee=" + fee + ", clearanceFee=" + clearanceFee + ", lang=" + lang
				+ ", paymentMethod=" + paymentMethod + ", totalDepositUsd=" + totalDepositUsd + ", totalDepositAccCurr="
				+ totalDepositAccCurr + ", transactionStatus=" + transactionStatus + ", assignee=" + assignee
				+ ", cryptoDepositStatus=" + cryptoDepositStatus + ", cryptoWstatus=" + cryptoWstatus + ", email="
				+ email + ", userId=" + userId + ", transactionId=" + transactionId + ", txId=" + txId + "]";
	}
}
