/**
 * 
 */
package com.anyoption.backend.service.transaction;

import java.util.ArrayList;

import com.anyoption.common.service.requests.MethodRequest;

/**
 * @author pavel.tabakov
 *
 */
public class GetAllCryptoDepsitsRequest extends MethodRequest {
	
	//filters
	private Long from;
	private Long to;
	private Long searchBy; // radio button time_created = 1, time_execution = 2
	private ArrayList<Integer> countries;
	private ArrayList<Integer> transactionTypes; // ex 9 --> TRANS_TYPE_CASH_DEPOSIT
	private ArrayList<Integer> assignees;
	private ArrayList<Integer> statuses;
	private Long userId;
	private Long transactionId;
	private String email;
	private String firstName;
	private String lastName;
	private Long page;
	private Long rowsPerPage;
	private String writerUtcOffset;
	/**
	 * @return the from
	 */
	public Long getFrom() {
		return from;
	}
	/**
	 * @param from the from to set
	 */
	public void setFrom(Long from) {
		this.from = from;
	}
	/**
	 * @return the to
	 */
	public Long getTo() {
		return to;
	}
	/**
	 * @param to the to to set
	 */
	public void setTo(Long to) {
		this.to = to;
	}
	/**
	 * @return the countries
	 */
	public ArrayList<Integer> getCountries() {
		return countries;
	}
	/**
	 * @param countries the countries to set
	 */
	public void setCountries(ArrayList<Integer> countries) {
		this.countries = countries;
	}
	/**
	 * @return the searchBy
	 */
	public Long getSearchBy() {
		return searchBy;
	}
	/**
	 * @param searchBy the searchBy to set
	 */
	public void setSearchBy(Long searchBy) {
		this.searchBy = searchBy;
	}
	/**
	 * @return the transactionTypes
	 */
	public ArrayList<Integer> getTransactionTypes() {
		return transactionTypes;
	}
	/**
	 * @param transactionTypes the transactionTypes to set
	 */
	public void setTransactionTypes(ArrayList<Integer> transactionTypes) {
		this.transactionTypes = transactionTypes;
	}
	/**
	 * @return the statuses
	 */
	public ArrayList<Integer> getStatuses() {
		return statuses;
	}
	/**
	 * @param statuses the statuses to set
	 */
	public void setStatuses(ArrayList<Integer> statuses) {
		this.statuses = statuses;
	}
	/**
	 * @return the userId
	 */
	public Long getUserId() {
		return userId;
	}
	/**
	 * @param userId the userId to set
	 */
	public void setUserId(Long userId) {
		this.userId = userId;
	}
	/**
	 * @return the transactionId
	 */
	public Long getTransactionId() {
		return transactionId;
	}
	/**
	 * @param transactionId the transactionId to set
	 */
	public void setTransactionId(Long transactionId) {
		this.transactionId = transactionId;
	}
	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}
	/**
	 * @param email the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}
	/**
	 * @return the writerUtcOffset
	 */
	public String getWriterUtcOffset() {
		return writerUtcOffset;
	}
	/**
	 * @param writerUtcOffset the writerUtcOffset to set
	 */
	public void setWriterUtcOffset(String writerUtcOffset) {
		this.writerUtcOffset = writerUtcOffset;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public Long getPage() {
		return page;
	}
	public void setPage(Long page) {
		this.page = page;
	}
	public Long getRowsPerPage() {
		return rowsPerPage;
	}
	public void setRowsPerPage(Long rowsPerPage) {
		this.rowsPerPage = rowsPerPage;
	}
	public ArrayList<Integer> getAssignees() {
		return assignees;
	}
	public void setAssignees(ArrayList<Integer> assignees) {
		this.assignees = assignees;
	}

}
