/**
 * 
 */
package com.anyoption.backend.service.transaction;

import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import com.anyoption.backend.managers.TransactionManager;
import com.anyoption.backend.managers.WriterPermisionsManager;
import com.anyoption.backend.service.writerpermissions.ScreenSettingsResponse;
import com.anyoption.common.backend.annotations.BackendPermission;
import com.anyoption.common.beans.Transaction;
import com.anyoption.common.managers.CountryManagerBase;
import com.anyoption.common.service.CommonJSONService;
import com.anyoption.common.service.requests.UserMethodRequest;
import com.anyoption.common.service.results.MethodResult;

import il.co.etrader.backend.bl_managers.WritersManager;
import il.co.etrader.backend.bl_vos.WriterWrapper;
import il.co.etrader.bl_managers.SkinsManagerBase;
import il.co.etrader.bl_managers.TransactionsManagerBase;
import il.co.etrader.util.CommonUtil;
import il.co.etrader.util.ConstantsBase;

/**
 * @author pavel.tabakov
 *
 */
public class TransactionServices {
	
	private static final Logger log = Logger.getLogger(TransactionServices.class);
	private static final String SCREEN_NAME_TRANSACTION = "transactions";
	private static final String PERMISSION_CC_NUM = "transactions_ccNum";

	public static ScreenSettingsResponse getTransactionScreenSettings(UserMethodRequest request, HttpServletRequest httpRequest) {
		ScreenSettingsResponse response = new ScreenSettingsResponse();
		HashMap<String, HashMap<Long, String>> filters = new HashMap<>();
		WriterWrapper writer = (WriterWrapper) httpRequest.getSession().getAttribute(ConstantsBase.BIND_SESSION_WRITER_WRAPPER);
		
		WriterPermisionsManager.setWriterScreenPermissionsByScreenName(SCREEN_NAME_TRANSACTION, response, httpRequest);
		//FILTERS
		//Users
		HashMap<Long, String> usersClass = TransactionManager.getUserClasses();
		filters.put(TransactionManager.FILTER_CLASS_USERS, usersClass);
		
		//platforms
		HashMap<Long, String> platforms = TransactionManager.getPlatforms();
		filters.put(TransactionManager.FILTER_PLATFORMS, platforms);
		
		//business case
		HashMap<Long, String> skinBusinessCases = TransactionManager.getSkinBusinessCases();
		filters.put(TransactionManager.FILTER_SKIN_BUSINESS_CASES, skinBusinessCases);
		
		//skins
		HashMap<Long, String> skins = WriterPermisionsManager.filterSkinsBasedOnWriterAttribution(SkinsManagerBase.getAllSkinsFilter(), writer, log);
		filters.put(SkinsManagerBase.FILTER_SKINS, skins);
		
		//countries
		HashMap<Long, String> countries = CountryManagerBase.getAllCountriesFilter();
		filters.put(CountryManagerBase.FILTER_COUNTRIES, countries);
		
		//currencies
		HashMap<Long, String> currencies = TransactionManager.getAllCurrencies();
		filters.put(TransactionManager.FILTER_CURRENCIES, currencies);
		
		
		//assignees
		HashMap<Long, String> assignees = TransactionManager.getAssignees();
		filters.put(TransactionManager.FILTER_ASSIGNEES, assignees);
		
		//status
		HashMap<Long, String> transactionStatuses = TransactionManager.getAllTransactionStatuses();
		filters.put(TransactionManager.FILTER_TRANSACTION_STATUSES, transactionStatuses);
		
		//transaction class types
		HashMap<Long, String> transactionClassTypes = TransactionManager.getAllTransactionClassTypes();
		filters.put(TransactionManager.FILTER_TRANSACTION_CLASS_TYPES, transactionClassTypes);
		
		//transaction types
		HashMap<Long, String> transactionTypes = TransactionManager.getAllTransactionTypes();// depend on FILTER_TRANSACTION_CLASS_TYPES 
		filters.put(TransactionManager.FILTER_TRANSACTION_TYPES, transactionTypes);
		
		//transaction types and class types mapping
		HashMap<Long, String> transactionsClassAndTypesMapping = TransactionManager.getTransactionsAndClassTypeTablesMapping(); 
		filters.put(TransactionManager.FILTER_TRANSACTIONS_CLASS_TYPE_MAPPING, transactionsClassAndTypesMapping);
		
		//CC Provider
		HashMap<Long, String> clearingProviders = TransactionManager.getAllClearingProviders();
		filters.put(TransactionManager.FILTER_CLEARING_PROVIDERS, clearingProviders);
		
		// Gateway --> hide check BAC-2233 for further explanation :)
		//HashMap<Long, String> gateway = TransactionManager.getGateway();
		//filters.put(TransactionManager.FILTER_GATEWAY, gateway);
		
		// Campaign
		HashMap<Long, String> campaigns = TransactionManager.getCampaigns();
		filters.put(TransactionManager.FILTER_MARKETING_CAMPAIGNS, campaigns);
		
		// writer
		HashMap<Long, String> systemWriters = WritersManager.getSystemWriters();
		systemWriters.put(new Long(987), "BACKEND");
		filters.put(WritersManager.FILTER_SYSTEM_WRITERS, systemWriters);
		
		//Has balance getHasBalance
		HashMap<Long, String> hasBalance = TransactionManager.getHasBalance();
		filters.put(TransactionManager.FILTER_HAS_BALANCE, hasBalance);
		
		//Was rerouted getWasRerouted
		HashMap<Long, String> wasRerouted = TransactionManager.getWasRerouted();
		filters.put(TransactionManager.FILTER_WAS_REROUTED, wasRerouted);
		
		// J4 postponed
		HashMap<Long, String> j4postponed = TransactionManager.getJ4postponed();
		filters.put(TransactionManager.FILTER_J4_POSTPONED, j4postponed);
		
		// Deposit Types --> FTD or Non FTD
		HashMap<Long, String> salesTypes = TransactionManager.getSalesType();
		filters.put(TransactionManager.FILTER_SALES_TYPES, salesTypes);
		
		// Deposit Source --> Independent or Sales
		HashMap<Long, String> depositSource = TransactionManager.getDepositSource();
		filters.put(TransactionManager.FILTER_DEPOSIT_SOURCE, depositSource);
		
		// status class mapping
		HashMap<Long, String> trnStatusClassMapping= TransactionManager.getTrnStatusClassMapping(); 
		filters.put(TransactionManager.FILTER_STATUS_CLASS_MAPPING, trnStatusClassMapping);
		
		//Was processed with 3d verification process
		HashMap<Long, String> isThreeD = TransactionManager.getIsThreeD();
		filters.put(TransactionManager.FILTER_IS_3D, isThreeD);
		
		response.setScreenFilters(filters);
		return response;
	}
	
	@BackendPermission(id = "transactions_view")
	public static TransactionGetAllMethodResponse getAll(TransactionGetAllMethodRequest request, HttpServletRequest httpRequest){
		TransactionGetAllMethodResponse response = new TransactionGetAllMethodResponse();
		WriterWrapper writer = (WriterWrapper) httpRequest.getSession().getAttribute(ConstantsBase.BIND_SESSION_WRITER_WRAPPER);
		long writerId = writer.getWriter().getId();
		ArrayList<String> permissionList = null;
		if (request == null) {
			response.setErrorCode(CommonJSONService.ERROR_CODE_INVALID_INPUT);
			return response;
		}
		ArrayList<Integer> skinsList = request.getSkins();
		skinsList = WriterPermisionsManager.setPermitedSkinsForWriter(skinsList, writer);
		request.setSkins(skinsList);
		ArrayList<Transaction> transactions = TransactionManager.getAll(request, response);
		if(transactions == null){
			return response;
		}
		
		try {
			for (Transaction t : transactions) {// only last 4
				if (t.getCreditCard() != null) {
					t.getCreditCard().setCcNumberXXXX(t.getCreditCard().getCcNumberLast4NotMasked());
				}
			}
			permissionList = WriterPermisionsManager.getWriterScreenPermissions(SCREEN_NAME_TRANSACTION, httpRequest);
			if (!permissionList.isEmpty()) {
				if (permissionList.contains(PERMISSION_CC_NUM)) {

					for (Transaction t : transactions) {// first 6 and last 4
						if (t.getCreditCard() != null) {
							t.getCreditCard().setBinXXXXXXLastFour(t.getCreditCard().getCcNumberXXXX());
						}
					}
				}
			}

			for (Transaction t : transactions) {// set cc_number to 0
				if (t.getCreditCard() != null) {
					t.getCreditCard().setCcNumber(0);
				}
			}

		} catch (SQLException ex) {
			log.error("Enable to get permissions for writer with id: " + writerId);
			response.setErrorCode(CommonJSONService.ERROR_CODE_UNKNOWN);
		}
		response.setTransactions(transactions);
		return response;
	}
	
	@BackendPermission(id = "cryptoDeposits_view")
	public static GetAllCryptoDepositsResponse getAllCrytpoDeposits(GetAllCryptoDepsitsRequest request, HttpServletRequest httpRequest) {
		GetAllCryptoDepositsResponse response = new GetAllCryptoDepositsResponse();

		if (request == null) {
			response.setResponseCode(CommonJSONService.ERROR_CODE_INVALID_INPUT);
			return response;
		}
		
		setWriterIdFromSession(request, httpRequest);
		try {
			ArrayList<CryptoDeposits> deposits = TransactionManager.getAllCrytpoDeposits(request, response);
			response.setCryptoDeposits(deposits);
		} catch (SQLException e) {
			log.error("Unable to get all deposits", e);
			response.setResponseCode(CommonJSONService.ERROR_CODE_UNKNOWN);
		}
		return response;
	}
	
	@BackendPermission(id = "transactions_approveWireDeposit")
	public static TransactionGetAllMethodResponse approveDeposit(TransactionGetAllMethodRequest request, HttpServletRequest httpRequest) {
		TransactionGetAllMethodResponse response = new TransactionGetAllMethodResponse();
		
		if (!isValid(request)) {
			response.setErrorCode(CommonJSONService.ERROR_CODE_INVALID_INPUT);
			return response;
		}
		
		setWriterIdFromSession(request, httpRequest);
		TransactionManager.approveDeposit(request.getTransaction(), request.getWriterId(), response);
		if (response.getErrorCode() > 0) {
			return response;
		}
		return getAll(request, httpRequest);
	}
	
	@BackendPermission(id = "transactions_cancelPendingDeposit")
	public static TransactionGetAllMethodResponse cancelDeposit(TransactionGetAllMethodRequest request, HttpServletRequest httpRequest) {
		TransactionGetAllMethodResponse response = new TransactionGetAllMethodResponse();

		if (!isValid(request)) {
			response.setErrorCode(CommonJSONService.ERROR_CODE_INVALID_INPUT);
			return response;
		}
		setWriterIdFromSession(request, httpRequest);
		int cancelStatus = TransactionsManagerBase.TRANS_STATUS_CANCELED_ETRADER;
		if (request.getTransaction().isWireDeposit() || request.getTransaction().isCashDeposit()) {
			cancelStatus = TransactionsManagerBase.TRANS_STATUS_CANCEL_M_N_R;
		}
		TransactionManager.cancelDeposit(request.getTransaction(), request.getWriterId(), cancelStatus, response);
		if (response.getErrorCode() > 0) {
			return response;
		}
		
		return getAll(request, httpRequest);
	}
	
	@BackendPermission(id = "transactions_fraudCancelPendingDeposit")
	public static TransactionGetAllMethodResponse cancelDepositFraud(TransactionGetAllMethodRequest request, HttpServletRequest httpRequest) {
		TransactionGetAllMethodResponse response = new TransactionGetAllMethodResponse();

		if (!isValid(request)) {
			response.setErrorCode(CommonJSONService.ERROR_CODE_INVALID_INPUT);
			return response;
		}
		setWriterIdFromSession(request, httpRequest);

		TransactionManager.cancelDeposit(request.getTransaction(), request.getWriterId(), TransactionsManagerBase.TRANS_STATUS_CANCELED_FRAUDS, response);
		if (response.getErrorCode() > 0) {
			return response;
		}
		
		return getAll(request, httpRequest);

	}
	
	@BackendPermission(id = "transactions_cancelBankWireMistake")
	public static TransactionGetAllMethodResponse cancelDepositMistake(TransactionGetAllMethodRequest request, HttpServletRequest httpRequest) {
		TransactionGetAllMethodResponse response = new TransactionGetAllMethodResponse();

		if (!isValid(request)) {
			response.setErrorCode(CommonJSONService.ERROR_CODE_INVALID_INPUT);
			return response;
		}
		setWriterIdFromSession(request, httpRequest);

		TransactionManager.cancelDeposit(request.getTransaction(), request.getWriterId(), TransactionsManagerBase.TRANS_STATUS_CANCEL_S_M, response);
		if (response.getErrorCode() > 0) {
			return response;
		}

		return getAll(request, httpRequest);
	}
	
	@BackendPermission(id = "transactions_cancelMaintenanceFee")
	public static TransactionGetAllMethodResponse cancelMaintenanceFee(TransactionGetAllMethodRequest request, HttpServletRequest httpRequest) {
		TransactionGetAllMethodResponse response = new TransactionGetAllMethodResponse();

		if (!isValid(request)) {
			response.setErrorCode(CommonJSONService.ERROR_CODE_INVALID_INPUT);
			return response;
		}
		setWriterIdFromSession(request, httpRequest);
		try {
			if (!TransactionsManagerBase.isMaintenanceFeeCanceled(request.getTransactionId())) {
				Transaction cancelTrn = createCancelFeeTransaction(request);
				TransactionsManagerBase.cancelMaintenanceFee(cancelTrn, request.getWriterId());
			} else {
				log.error("This Maintenance fee was already cancelled ! ID: " + request.getTransaction().getId());
				response.setErrorCode(CommonJSONService.ERROR_CODE_MAINTENANCE_FEE_ALREADY_CANCELED);
				return response;
			}
		} catch (SQLException ex) {
			log.error("Cannot cancel maintenance fee due to : ", ex);
			response.setErrorCode(CommonJSONService.ERROR_CODE_UNKNOWN);
			return response;
		}
		return getAll(request, httpRequest);
	}
	
	@BackendPermission(id = "transactions_view")
	public static TransactionReroutingGetResponse getReroutingTransactions(TransactionGetByReferenceId request) {
		TransactionReroutingGetResponse response = new TransactionReroutingGetResponse();
		try {
			return TransactionManager.getReroutingTransactions(request.getReferenceId());
		} catch (SQLException e) {
			log.error("Enable to get getReroutingTransactions for transactionId: " + request.getReferenceId());
			response.setErrorCode(CommonJSONService.ERROR_CODE_UNKNOWN);
		}
		return response;
	}
	
	private static Transaction createCancelFeeTransaction(TransactionGetAllMethodRequest request) {
		Transaction tran = new Transaction();
		tran.setAmount(request.getTransaction().getAmount());
		tran.setDescription("maintenance.fee.cancellation");
		tran.setCurrency(request.getTransaction().getCurrency());
		tran.setIp(CommonUtil.getIPAddress());
		tran.setProcessedWriterId(request.getWriterId());
		tran.setTimeSettled(new Date());
		tran.setTimeCreated(new Date());
		tran.setUtcOffsetCreated(request.getWriterUtcOffset());
		tran.setUtcOffsetSettled(request.getWriterUtcOffset());
		tran.setTypeId(TransactionsManagerBase.TRANS_TYPE_MAINTENANCE_FEE_CANCEL);
		tran.setUserId(request.getTransaction().getUserId());
		tran.setWriterId(request.getWriterId());
		tran.setStatusId(TransactionsManagerBase.TRANS_STATUS_SUCCEED);
		tran.setComments("Manually canceled from backend");
		tran.setSkin(request.getTransaction().getSkin());
		tran.setReferenceId(new BigDecimal(request.getTransaction().getId()));
		tran.setLoginId(request.getTransaction().getLoginId());
		return tran;
	}
	
	public static MethodResult manageTransactionPostponed(TransactionPostponedRequest request, HttpServletRequest httpRequest) {

		WriterWrapper writer = (WriterWrapper) httpRequest.getSession().getAttribute(ConstantsBase.BIND_SESSION_WRITER_WRAPPER);
		long writerId = writer.getWriter().getId();
		request.setWriterId(writerId);

		return TransactionManager.manageTransactionPostponed(request);
	}

	private static boolean isValid(TransactionGetAllMethodRequest request) {
		if (request == null || request.getTransaction() == null) {
			return false;
		}
		return true;
	}

	private static void setWriterIdFromSession(TransactionGetAllMethodRequest request, HttpServletRequest httpRequest) {
		WriterWrapper writer = (WriterWrapper) httpRequest.getSession().getAttribute(ConstantsBase.BIND_SESSION_WRITER_WRAPPER);
		long writerId = writer.getWriter().getId();
		request.setWriterUtcOffset(writer.getUtcOffset());
		request.setWriterId(writerId);
	}
	
	private static void setWriterIdFromSession(GetAllCryptoDepsitsRequest request, HttpServletRequest httpRequest) {
		WriterWrapper writer = (WriterWrapper) httpRequest.getSession().getAttribute(ConstantsBase.BIND_SESSION_WRITER_WRAPPER);
		long writerId = writer.getWriter().getId();
		request.setWriterUtcOffset(writer.getUtcOffset());
		request.setWriterId(writerId);
	}
	
}
