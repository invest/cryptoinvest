/**
 * 
 */
package com.anyoption.backend.service.transaction;

import java.util.ArrayList;

import com.anyoption.common.service.results.MethodResult;

/**
 * @author pavel.tabakov
 *
 */
public class GetAllCryptoDepositsResponse extends MethodResult {
	private ArrayList<CryptoDeposits> cryptoDeposits;
	private long totalCount;

	public long getTotalCount() {
		return totalCount;
	}

	public void setTotalCount(long totalCount) {
		this.totalCount = totalCount;
	}

	public ArrayList<CryptoDeposits> getCryptoDeposits() {
		return cryptoDeposits;
	}

	public void setCryptoDeposits(ArrayList<CryptoDeposits> cryptoDeposits) {
		this.cryptoDeposits = cryptoDeposits;
	}
}
