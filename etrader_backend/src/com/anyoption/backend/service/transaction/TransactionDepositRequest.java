/**
 * 
 */
package com.anyoption.backend.service.transaction;

import com.anyoption.common.beans.Transaction;
import com.anyoption.common.service.requests.UserMethodRequest;

/**
 * @author pavel.tabakov
 *
 */
public class TransactionDepositRequest extends UserMethodRequest{

	private Transaction transaction;
	private String writerUtcOffset;

	public Transaction getTransaction() {
		return transaction;
	}

	public void setTransaction(Transaction transaction) {
		this.transaction = transaction;
	}

	public String getWriterUtcOffset() {
		return writerUtcOffset;
	}

	public void setWriterUtcOffset(String writerUtcOffset) {
		this.writerUtcOffset = writerUtcOffset;
	}
}
