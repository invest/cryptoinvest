package com.anyoption.backend.service.apmwithdrawals;

import com.anyoption.common.service.results.MethodResult;

public class APMSkrillMethodResponse extends MethodResult {

	private long fee;

	public long getFee() {
		return fee;
	}

	public void setFee(long fee) {
		this.fee = fee;
	}
}
