package com.anyoption.backend.service.apmwithdrawals;

import com.anyoption.common.service.requests.MethodRequest;

public class APMWithdrawalsMethodRequest extends MethodRequest {

	private long userId;

	public long getUserId() {
		return userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}	
}
