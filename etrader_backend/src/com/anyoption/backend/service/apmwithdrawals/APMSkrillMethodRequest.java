package com.anyoption.backend.service.apmwithdrawals;

import com.anyoption.common.service.requests.MethodRequest;

public class APMSkrillMethodRequest extends MethodRequest {

	private long userId;
	private String amount;
	private boolean exemptFee;
	private String moneybookersEmail;
	private boolean approved;

	public long getUserId() {
		return userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public boolean isExemptFee() {
		return exemptFee;
	}

	public void setExemptFee(boolean exemptFee) {
		this.exemptFee = exemptFee;
	}

	public String getMoneybookersEmail() {
		return moneybookersEmail;
	}

	public void setMoneybookersEmail(String moneybookersEmail) {
		this.moneybookersEmail = moneybookersEmail;
	}

	public boolean isApproved() {
		return approved;
	}

	public void setApproved(boolean approved) {
		this.approved = approved;
	}	
}
