package com.anyoption.backend.service.apmwithdrawals;

import java.util.HashMap;

import com.anyoption.backend.service.writerpermissions.ScreenSettingsResponse;

public class APMWithdrawalsScreenSettings extends ScreenSettingsResponse {

	private HashMap<String, HashMap<String, Boolean>> screenFilter;

	public HashMap<String, HashMap<String, Boolean>> getScreenFilter() {
		return screenFilter;
	}

	public void setScreenFilter(HashMap<String, HashMap<String, Boolean>> screenFilter) {
		this.screenFilter = screenFilter;
	}
}
