/**
 * 
 */
package com.anyoption.backend.service.writer;

import java.security.SecureRandom;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import com.anyoption.backend.managers.WriterManager;
import com.anyoption.backend.managers.WriterPermisionsManager;
import com.anyoption.backend.service.writerpermissions.ScreenSettingsResponse;
import com.anyoption.common.backend.annotations.BackendPermission;
import com.anyoption.common.beans.Writer;
import com.anyoption.common.service.CommonJSONService;
import com.anyoption.common.service.requests.UserMethodRequest;
import com.anyoption.common.service.results.MethodResult;
import com.anyoption.common.util.CommonUtil;

import il.co.etrader.bl_managers.SkinsManagerBase;

/**
 * @author pavel.tabakov
 *
 */
public class WriterServices {

	private static final Logger log = Logger.getLogger(WriterServices.class);
	private static final String SCREEN_NAME_WRITER = "writers";
	private static final String AB = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
	private static SecureRandom rnd = new SecureRandom();
	private static final int RANDOM_CHARACTERS = 8;
	
	// SERVICES START
	@BackendPermission(id = "writers_view")
	public static ScreenSettingsResponse getWriterScreenSettings(UserMethodRequest request, HttpServletRequest httpRequest) {
		ScreenSettingsResponse response = new ScreenSettingsResponse();
		HashMap<String, HashMap<Long, String>> filters = new HashMap<>();

		WriterPermisionsManager.setWriterScreenPermissionsByScreenName(SCREEN_NAME_WRITER, response, httpRequest);

		//skins
		HashMap<Long, String> skins = SkinsManagerBase.getAllSkinsFilter();
		filters.put(WriterManager.FILTER_SKINS, skins);

		// Department filter
		HashMap<Long, String> departments = WriterManager.getDepartments();
		filters.put(WriterManager.FILTER_DEPARTMENT, departments);
		
		// writer site possible values (Bulgaria, Cyprus, Germany, Israel)
		HashMap<Long, String> site = WriterManager.getSite();
		filters.put(WriterManager.FILTER_SITE, site);

		// Roles filter
		try{
			HashMap<Long, String> roles = WriterPermisionsManager.getRoles();
			filters.put(WriterManager.FILTER_ROLE, roles);
		}catch(SQLException ex){
			log.error("Unable to roles filter due to: ", ex);
			response.setErrorCode(CommonJSONService.ERROR_CODE_UNKNOWN);
		}

		// is active filter
		HashMap<Long, String> isActiveList = WriterManager.getIsActive();
		filters.put(WriterManager.FILTER_IS_ACTIVE, isActiveList);
		
		// sales types filter
		HashMap<Long, String> salesTypeList = WriterManager.getSalesType();
		filters.put(WriterManager.FILTER_SALES_TYPE, salesTypeList);

		response.setScreenFilters(filters);
		return response;
	}
	
	@BackendPermission(id = "writers_view")
	public static WriterGetAllMethodResponse getAll(WriterGetAllMethodRequest request) {
		WriterGetAllMethodResponse response = new WriterGetAllMethodResponse();
		if (request == null) {
			response.setErrorCode(CommonJSONService.ERROR_CODE_INVALID_INPUT);
			return response;
		}
		ArrayList<Writer> writers = WriterManager.getAll(request, response);
		if (writers == null) {
			return response;
		}
		response.setWriters(writers);
		return response;
	}
	
	@BackendPermission(id = "writers_addWriter")
	public static MethodResult insertWriter(WriterInsertMethodRequest request) {
		WriterInsertMethodResponse response = new WriterInsertMethodResponse();
		if (request == null) {
			response.setErrorCode(CommonJSONService.ERROR_CODE_INVALID_INPUT);
			return response;
		}
		// check if MUST fields are populated correctly
		if (!WriterManager.isValid(request.getFirstName())
				|| !WriterManager.isValid(request.getLastName())
				|| !WriterManager.isValid(request.getEmail())
				|| request.getDepartmentId() == null
				|| request.getSite() == null) {
			response.setErrorCode(CommonJSONService.ERROR_CODE_INVALID_INPUT);
			return response;
		}
		// random pass
		String randomPass = randomString(RANDOM_CHARACTERS);
		// generate username
		request.setUserName(generateWriterUserName(request.getFirstName().toUpperCase(), request.getLastName().toUpperCase()));

		WriterManager.insert(request, response, randomPass);

		if (response.getErrorCode() == CommonJSONService.ERROR_CODE_FAIL_TO_INSERT) {
			return response;
		} else {
			response.setErrorCode(CommonJSONService.ERROR_CODE_SUCCESS);
		}
		sendWriterWelcomeEmail(request, randomPass);// send email
		return response;
	}

	@BackendPermission(id = "writers_editWriter")
	public static MethodResult updateWriter(WriterUpdateMethodRequest request, HttpServletRequest httpRequest) {
		MethodResult response = new MethodResult();
		
		if (request == null || request.getId() == null || request.getId() < 0) {
			response.setErrorCode(CommonJSONService.ERROR_CODE_INVALID_INPUT);
			return response;
		}
		
		WriterManager.update(request, response);
		if (response.getErrorCode() != CommonJSONService.ERROR_CODE_FAIL_TO_UPDATE) {
			response.setErrorCode(CommonJSONService.ERROR_CODE_SUCCESS);
		}
		return response;
	}
	
	public static MethodResult resetPassword(WriterResetPasswordMethodRequest request, HttpServletRequest httpRequest) {
		MethodResult response = new MethodResult();
		if (request.getUserName() == null || request.getEmail() == null || request.getUserName().equals("") || request.getEmail().equals("")) {
			response.setErrorCode(CommonJSONService.ERROR_CODE_INVALID_INPUT); //MESSAGE: The writer name and\or email are incorrect
			return response;
		}
		Writer writer = WriterManager.isWriterExists(request);
		 if(writer == null) {
				response.setErrorCode(CommonJSONService.ERROR_CODE_WRITER_USERNAME_OR_EMAIL_INCORRECT); //MESSAGE: The writer name and\or email are incorrect
				return response;
		 } else if (writer.getIsActive() < 1) {
				response.setErrorCode(CommonJSONService.ERROR_CODE_WRITER_IS_NOT_ACTIVE); //MESSAGE: Can't reset password. Please contact your manager
				return response;
		 }
		 String pass = randomString(RANDOM_CHARACTERS);
		 sendWriterResetPasswordEmail(writer, response, pass);
		 response.setErrorCode(CommonJSONService.ERROR_CODE_SUCCESS); //MESSAGE: A new password was sent to your email
		return response;
	}
	// SERVICES END
	
	
	private static String randomString(int len) {
		StringBuilder sb = new StringBuilder(len);
		for (int i = 0; i < len; i++){
			sb.append(AB.charAt(rnd.nextInt(AB.length())));
		}
		return sb.toString();
	}
	
	private static String generateWriterUserName(String firstName, String lastName) {
		String username = firstName + lastName.substring(0, 1);
		try {
			ArrayList<String> usernames = WriterManager.getUsernamesToCompare(username);
			if (usernames.isEmpty()) {
				return username;
			} else {
				int index = 2;
				int count = 0;// add number at the end writer user name in case combination (first name + last name) already exists
				int lastNameLength = lastName.length();
				for (int i = 0; i < usernames.size() + 1; i++) {
					username = firstName + lastName.substring(0, index);
					if (usernames.contains(username)) {
						if (index < lastNameLength) {
							index++;
						} else {
							++count;
							username = username + count;
						}
					} else {
						return username;
					}
				}
			}
		} catch (SQLException ex) {
			log.info("Can not get other writers usernames to compare due to: ", ex);
			return username;
		}
		return username;
	}
	
	private static void sendWriterWelcomeEmail(WriterInsertMethodRequest request, String password) {
		Hashtable<String, String> serverProperties = new Hashtable<String, String>();
		serverProperties.put("url", CommonUtil.getProperty("email.server"));
		serverProperties.put("auth", "true");
		serverProperties.put("user", CommonUtil.getProperty("email.uname"));
		serverProperties.put("pass", CommonUtil.getProperty("email.pass"));

		Hashtable<String, String> emailProperties = new Hashtable<String, String>();

		emailProperties.put("from", CommonUtil.getProperty("email.from"));
		emailProperties.put("to", request.getEmail());
		emailProperties.put("subject", "Welcome to Backend");
		String body = "Hi " + request.getFirstName() + ",<br/>" + "These are your credentials to the Backend:" + "<br/>"
				+ "URL: <a href=\"" + CommonUtil.getProperty("login.url") + "\">" + CommonUtil.getProperty("login.url") + "</a><br/>" + "User name: " + request.getUserName() + "<br/>" + "Password: "
				+ password + "<br/>" + "The password should be changed as soon as possible.";
		emailProperties.put("body", body);

		CommonUtil.sendEmail(serverProperties, emailProperties, null);
	}
	
	private static void sendWriterResetPasswordEmail(Writer wr, MethodResult result, String pass) {
		Hashtable<String, String> serverProperties = new Hashtable<String, String>();
		serverProperties.put("url", CommonUtil.getProperty("email.server"));
		serverProperties.put("auth", "true");
		serverProperties.put("user", CommonUtil.getProperty("email.uname"));
		serverProperties.put("pass", CommonUtil.getProperty("email.pass"));

		Hashtable<String, String> emailProperties = new Hashtable<String, String>();

		emailProperties.put("from", CommonUtil.getProperty("email.from.backend"));
		emailProperties.put("to", wr.getEmail());
		emailProperties.put("subject", "Your password was reset");
		String body =  "Dear " + wr.getUserName() + "<br/>" + "Your new temporary Backend password is " + pass
					 + "<br/>" + "Please login and then change your password, as soon as possible.";
		emailProperties.put("body", body);

		CommonUtil.sendEmail(serverProperties, emailProperties, null);
		
		WriterManager.resetWriterPassAndFailCount(wr.getId(), result, pass);
	}
    
}
