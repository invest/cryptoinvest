/**
 * 
 */
package com.anyoption.backend.service.writer;

import java.util.ArrayList;

import com.anyoption.common.service.requests.UserMethodRequest;

/**
 * @author pavel.tabakov
 *
 */
public class WriterUpdateMethodRequest extends UserMethodRequest{
	
	private Long id;
	private String firstName;
	private String lastName;
	private String email;
	private String mobilePhone;
	private String nickNameFirst;
	private String nickNameLast;
	private Long employeeId;
	private Long departmentId;
	private Long roleId;
	private Long isActive;
	private Long salesType;
	private Long site;
	
	private ArrayList<Integer> skins;
	
	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getMobilePhone() {
		return mobilePhone;
	}

	public void setMobilePhone(String mobilePhone) {
		this.mobilePhone = mobilePhone;
	}

	public String getNickNameFirst() {
		return nickNameFirst;
	}

	public void setNickNameFirst(String nickNameFirst) {
		this.nickNameFirst = nickNameFirst;
	}

	public String getNickNameLast() {
		return nickNameLast;
	}

	public void setNickNameLast(String nickNameLast) {
		this.nickNameLast = nickNameLast;
	}

	public Long getEmployeeId() {
		return employeeId;
	}

	public void setEmployeeId(Long employeeId) {
		this.employeeId = employeeId;
	}

	public Long getDepartmentId() {
		return departmentId;
	}

	public void setDepartmentId(Long departmentId) {
		this.departmentId = departmentId;
	}

	public Long getRoleId() {
		return roleId;
	}

	public void setRoleId(Long roleId) {
		this.roleId = roleId;
	}

	public Long getIsActive() {
		return isActive;
	}

	public void setIsActive(Long isActive) {
		this.isActive = isActive;
	}

	public ArrayList<Integer> getSkins() {
		return skins;
	}

	public void setSkins(ArrayList<Integer> skins) {
		this.skins = skins;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getSalesType() {
		return salesType;
	}
	public void setSalesType(Long salesType) {
		this.salesType = salesType;
	}
	public Long getSite() {
		return site;
	}
	public void setSite(Long site) {
		this.site = site;
	}
	
	public String toString() {
	    String ls = System.getProperty("line.separator");
	    return ls + "WriterUpdateMethodRequest: " + ls
	    	+ super.toString() + ls
	        + "firstName: " + firstName + ls
	        + "lastName: " + lastName + ls
	        + "email: " + email + ls
            + "mobilePhone: " + mobilePhone + ls
            + "nickNameFirst: " + nickNameFirst + ls
            + "nickNameLast: " + nickNameLast + ls 
		    + "employeeId: " + employeeId + ls
		    + "departmentId: " + departmentId + ls
		    + "roleId: " + roleId + ls
		    + "salesType: " + salesType + ls
		    + "isActive: " + isActive + ls
		    + "site: " + site + ls;
	}

}
