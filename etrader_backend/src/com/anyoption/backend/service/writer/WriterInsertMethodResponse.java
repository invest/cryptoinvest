/**
 * 
 */
package com.anyoption.backend.service.writer;

import com.anyoption.common.service.results.MethodResult;

/**
 * @author pavel.tabakov
 *
 */
public class WriterInsertMethodResponse extends MethodResult{
	
	private long writerId;

	public long getWriterId() {
		return writerId;
	}

	public void setWriterId(long writerId) {
		this.writerId = writerId;
	}
}
