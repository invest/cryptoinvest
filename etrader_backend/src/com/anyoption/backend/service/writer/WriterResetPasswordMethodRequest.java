/**
 * 
 */
package com.anyoption.backend.service.writer;

import com.anyoption.common.service.requests.UserMethodRequest;

/**
 * @author pavel.tabakov
 *
 */
public class WriterResetPasswordMethodRequest extends UserMethodRequest {

	private String email;

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String toString() {
		String ls = System.getProperty("line.separator");
		return ls + "WriterResetPasswordMethodRequest: " + super.toString() + "email: " + email + ls;
	}
}
