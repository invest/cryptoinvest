package com.anyoption.backend.service.questionnaire;

import java.sql.SQLException;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import com.anyoption.backend.managers.WriterPermisionsManager;
import com.anyoption.backend.service.writerpermissions.PermissionsGetResponse;
import com.anyoption.common.backend.annotations.BackendPermission;
import com.anyoption.common.beans.base.QuestionnaireGroup;
import com.anyoption.common.beans.base.UserRegulationBase;
import com.anyoption.common.managers.UserRegulationManager;
import com.anyoption.common.service.CommonJSONService;
import com.anyoption.common.service.requests.UpdateUserQuestionnaireAnswersRequest;
import com.anyoption.common.service.requests.UserMethodRequest;
import com.anyoption.common.service.results.UserMethodResult;
import com.anyoption.common.service.results.UserQuestionnaireResult;

import il.co.etrader.backend.bl_managers.UsersManager;
import il.co.etrader.backend.bl_vos.User;
import il.co.etrader.bl_managers.UsersManagerBase;
import il.co.etrader.bl_vos.Writer;
import il.co.etrader.util.ConstantsBase;

public class QuestionnaireServices {	         
	private static final Logger log = Logger.getLogger(QuestionnaireServices.class);
	
	private static final String SCREEN_NAME_QUEST = "questionnaire";
	
	@BackendPermission(id = "questionnaire_view")
	public static PermissionsGetResponse getQuestionnaireScreenSettings(UserMethodRequest request, HttpServletRequest httpRequest) {
		log.debug("getQuestionnaireScreenSettings:" + request);		
		PermissionsGetResponse response = new PermissionsGetResponse();		
		WriterPermisionsManager.setWriterScreenPermissionsByScreenName(SCREEN_NAME_QUEST, response, httpRequest);	
		return response;
	}
	
	@BackendPermission(id = "questionnaire_view")
	public static UserQuestionnaireResult getUserSingleQuestionnaireDynamic(UserMethodRequest request, HttpServletRequest httpRequest) {
		log.debug("getUserSingleQuestionnaireDynamic:" + request);
		User user;
		UserQuestionnaireResult  res = new UserQuestionnaireResult();
		try {
			user = UsersManager.getUserByOnlyUserName(request.getUserName());
			res = CommonJSONService.getUserSingleQuestionnaireDynamic(user, request.getWriterId());
			
			UserRegulationBase userRegulation = new UserRegulationBase();
		   	userRegulation.setUserId(user.getId());
		   	UserRegulationManager.getUserRegulation(userRegulation);
		   	if (userRegulation.getApprovedRegulationStep() == UserRegulationBase.REGULATION_FIRST_DEPOSIT){
		   		res.setBEedit(true);
		   	}
		   	return res;
		} catch (SQLException e) {
			log.error("Error in update getUserSingleQuestionnaireDynamic", e);
			res.setErrorCode(CommonJSONService.ERROR_CODE_UNKNOWN);
			return res;
		}		
    }
	
	@BackendPermission(id = "questionnaire_save")
    public static UserMethodResult updateUserQuestionnaireAnswers(UpdateUserQuestionnaireAnswersRequest request, HttpServletRequest httpRequest) {
    	log.debug("updateUserQuestionnaireAnswers: " + request);
    	UserQuestionnaireResult result = new UserQuestionnaireResult();
    	try {
    		User user = UsersManager.getUserByOnlyUserName(request.getUserName());
			return CommonJSONService.updateUserQuestionnaireAnswers(user, request, result);
		} catch (Exception e) {
			log.error("Error in update questionnaire answers", e);
			result.setErrorCode(CommonJSONService.ERROR_CODE_UNKNOWN);
			return result;
		}    	
    }
	
	@BackendPermission(id = "questionnaire_save")
    public static UserMethodResult updateSingleQuestionnaireDone(UserMethodRequest request, HttpServletRequest httpRequest) {
    	UserMethodResult result = new UserMethodResult();
    	try {
	    	log.debug("updateQuestionnaireDone: " + request);
	    	il.co.etrader.bl_vos.UserBase user = UsersManager.getUserByOnlyUserName(request.getUserName());				    	
	    	
			UserRegulationBase userRegulation = new UserRegulationBase();
		   	userRegulation.setUserId(user.getId());
		   	UserRegulationManager.getUserRegulation(userRegulation);
		   	if (userRegulation.getApprovedRegulationStep() != UserRegulationBase.REGULATION_FIRST_DEPOSIT){
				log.error("Error in update for User[" + user.getId() + "] Regyulation step is wrong: " + userRegulation);
				result.setErrorCode(CommonJSONService.ERROR_CODE_UNKNOWN);
				return result;
		   	}
	    	
	    	result = CommonJSONService.updateQuestionnaireDone(user, user.getWriterId(), request.getUtcOffset(), QuestionnaireGroup.REGULATION_SINGLE_QUESTIONNAIRE_NAME, httpRequest);			
			if(result.getErrorCode() == CommonJSONService.ERROR_CODE_REG_SUSPENDED_QUEST_INCORRECT ||
				 result.getErrorCode() == CommonJSONService.ERROR_CODE_REGULATION_USER_RESTRICTED) {
					UsersManagerBase.sendMailAfterQuestAORegulation(result.getUserRegulation());
			}
			
			if(result.getUserRegulation().getPepState() == UserRegulationBase.PEP_AUTO_PROHIBITED) {
				UsersManagerBase.sendMailTemplateFile(ConstantsBase.TEMPLATE_REGULATION_PEP, (long)Writer.WRITER_ID_WEB, user, null, new String(), new String(), null);
			}
			return result;
	    } catch (Exception e) {
			log.error("Error in update [" + QuestionnaireGroup.REGULATION_SINGLE_QUESTIONNAIRE_NAME + "] questionnaire status", e);
			result.setErrorCode(CommonJSONService.ERROR_CODE_UNKNOWN);
			return result;
		}
    }	
}