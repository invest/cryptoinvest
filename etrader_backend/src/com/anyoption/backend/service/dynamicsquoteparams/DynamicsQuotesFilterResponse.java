package com.anyoption.backend.service.dynamicsquoteparams;

import java.util.Map;

import com.anyoption.backend.service.writerpermissions.PermissionsGetResponse;

public class DynamicsQuotesFilterResponse extends PermissionsGetResponse {
	
	private Map<Long, String> dynamicsMarkets;

	public Map<Long, String> getDynamicsMarkets() {
		return dynamicsMarkets;
	}

	public void setDynamicsMarkets(Map<Long, String> dynamicsMarkets) {
		this.dynamicsMarkets = dynamicsMarkets;
	}
	
}
