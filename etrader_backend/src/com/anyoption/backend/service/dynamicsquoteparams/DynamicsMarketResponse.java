package com.anyoption.backend.service.dynamicsquoteparams;

import com.anyoption.common.beans.Market;
import com.anyoption.common.service.results.MethodResult;

public class DynamicsMarketResponse extends MethodResult {
	
	private Market market;

	public Market getMarket() {
		return market;
	}

	public void setMarket(Market market) {
		this.market = market;
	}
}
