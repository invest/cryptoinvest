package com.anyoption.backend.service.dynamicsquoteparams;

import com.anyoption.common.beans.MarketDynamicsQuoteParams;
import com.anyoption.common.service.requests.MethodRequest;

public class DynamicsMarketRequest extends MethodRequest {
	
	private long marketId;
	private MarketDynamicsQuoteParams quoteParams;
	
	public long getMarketId() {
		return marketId;
	}
	
	public void setMarketId(long marketId) {
		this.marketId = marketId;
	}

	public MarketDynamicsQuoteParams getQuoteParams() {
		return quoteParams;
	}

	public void setQuoteParams(MarketDynamicsQuoteParams quoteParams) {
		this.quoteParams = quoteParams;
	}
}