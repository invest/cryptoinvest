package com.anyoption.backend.service.marketpriorities;

import java.util.ArrayList;

import com.anyoption.common.service.requests.MethodRequest;

public class MarketsHPPRequest extends MethodRequest {
	ArrayList<Long> skinMarketGroupMarketIds;
	ArrayList<Long> newMarketsIds;
	long selectedSkinId;

	public ArrayList<Long> getSkinMarketGroupMarketIds() {
		return skinMarketGroupMarketIds;
	}

	public void setSkinMarketGroupMarketIds(ArrayList<Long> skinMarketGroupMarketId) {
		this.skinMarketGroupMarketIds = skinMarketGroupMarketId;
	}

	public ArrayList<Long> getNewMarketsIds() {
		return newMarketsIds;
	}

	public void setNewMarketsIds(ArrayList<Long> newMarketsIds) {
		this.newMarketsIds = newMarketsIds;
	}

	public long getSelectedSkinId() {
		return selectedSkinId;
	}

	public void setSelectedSkinId(long selectedSkinId) {
		this.selectedSkinId = selectedSkinId;
	}

	@Override
	public String toString() {
		return "MarketsHPPRequest [skinMarketGroupMarketIds=" + skinMarketGroupMarketIds + "]";
	}

}
