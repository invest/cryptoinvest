package com.anyoption.backend.service.marketpriorities;

import java.util.ArrayList;

import com.anyoption.common.beans.MarketPriority;
import com.anyoption.common.service.results.MethodResult;

public class MarketsHPPResponse extends MethodResult {
	ArrayList<MarketPriority> markets;

	public ArrayList<MarketPriority> getMarkets() {
		return markets;
	}
	public void setMarkets(ArrayList<MarketPriority> markets) {
		this.markets = markets;
	}

	@Override
	public String toString() {
		return "MarketsHPPResult [markets=" + markets + "]";
	}
	
}
