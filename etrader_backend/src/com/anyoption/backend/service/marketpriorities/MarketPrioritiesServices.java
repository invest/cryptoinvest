package com.anyoption.backend.service.marketpriorities;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import com.anyoption.backend.managers.WriterPermisionsManager;
import com.anyoption.backend.service.createmarket.CreateMarketServices;
import com.anyoption.common.backend.annotations.BackendPermission;
import com.anyoption.common.beans.MarketPriority;
import com.anyoption.common.beans.base.Skin;
import com.anyoption.common.jms.BackendLevelsCache;
import com.anyoption.common.managers.MarketsManagerBase;
import com.anyoption.common.managers.SkinsManagerBase;
import com.anyoption.common.service.CommonJSONService;
import com.anyoption.common.service.requests.UserMethodRequest;
import com.anyoption.common.service.results.MethodResult;
import com.anyoption.common.util.CommonUtil;

import il.co.etrader.backend.bl_managers.WritersManager;
import il.co.etrader.backend.bl_vos.WriterWrapper;
import il.co.etrader.bl_vos.Writer;
import il.co.etrader.util.ConstantsBase;

public class MarketPrioritiesServices {
	
	private static final Logger log = Logger.getLogger(CreateMarketServices.class);
	private static final String SCREEN_NAME_PERMISSIONS_MARKET_PRIORITIES = "marketPriorities";
	
	@BackendPermission(id = "marketPriorities_view")
	public static MarketPrioritiesResponse getMarketPrioritiesScreenSettings(UserMethodRequest request, HttpServletRequest httpRequest) {
		log.debug("getMarketPrioritiesScreenSettings: " + request);
		MarketPrioritiesResponse response = new MarketPrioritiesResponse();
		WriterPermisionsManager.setWriterScreenPermissionsByScreenName(SCREEN_NAME_PERMISSIONS_MARKET_PRIORITIES, response, httpRequest);
		WriterWrapper writerWrapper = (WriterWrapper) httpRequest.getSession().getAttribute(ConstantsBase.BIND_SESSION_WRITER_WRAPPER);
		Writer writer = writerWrapper.getWriter();
		Locale locale = new Locale("en");
		try {
			ArrayList<Integer> writerSkins = WritersManager.getWriterSkins(writer.getId());
			writerSkins.sort((p1, p2) -> p1.compareTo(p2));
			Map<Long, String> skins = new LinkedHashMap<Long, String>();
			for (int skinId : writerSkins) {
				skins.put((long) skinId, CommonUtil.getMessage(locale, SkinsManagerBase.getSkin(skinId).getDisplayName(), null));
			}
			Map<String, Map<Long, String>> filters = new HashMap<String, Map<Long, String>>();
			Map<Long, Map<Long, String>> markets = MarketsManagerBase.getMarketNamesByTypeAndSkin(Skin.SKIN_REG_EN);
			if (markets != null) {
				response.setMarkets(markets);
			}
			filters.put("writerSkins", skins);
			response.setFilters(filters);
		} catch (SQLException sqle) {
			log.error("Unable to select writer skins list! ", sqle);
			response.setErrorCode(CommonJSONService.ERROR_CODE_UNKNOWN);
			return response;
		}
		return response;
	}
	
	@BackendPermission(id = "marketPriorities_view")
    public static MarketsHPPResponse getSkinMarketsPriorities(UserMethodRequest umr) {
		log.debug("getSkinMarketsPriorities: " + umr);
    	MarketsHPPResponse result = new MarketsHPPResponse();
    	long skinId = umr.getSkinId();
    	if(skinId == 0 ) {
    		result.setErrorCode(CommonJSONService.ERROR_CODE_MISSING_PARAM);
    		return result;
    	}
    	ArrayList<MarketPriority> markets = MarketsManagerBase.getMarketsByPriority(skinId);
    	if(markets == null ) {
    		result.setErrorCode(CommonJSONService.ERROR_CODE_MISSING_PARAM);
    		return result;
    	}

    	result.setMarkets(markets);
    	return result;
    }

	@BackendPermission(id = "marketPriorities_view")
    public static MethodResult updateMarketsPriorities(MarketsHPPRequest mr, HttpServletRequest request) {
		log.debug("updateMarketsPriorities: " + mr);
    	MethodResult result = new MethodResult();
    	if(mr.getSkinMarketGroupMarketIds() == null || mr.getSkinMarketGroupMarketIds().size() == 0) {
    		result.setErrorCode(CommonJSONService.ERROR_CODE_MISSING_PARAM);
    		return result;
    	}
    	MarketsManagerBase.updateMarketPriority(mr.getSkinMarketGroupMarketIds());
    	if (mr.getNewMarketsIds() != null && mr.getNewMarketsIds().size() > 0) {
    		MarketsManagerBase.insertMarketPriority(mr.getNewMarketsIds(), mr.getSelectedSkinId(), mr.getSkinMarketGroupMarketIds().size());
    	}
    	BackendLevelsCache wlc = (BackendLevelsCache) request.getServletContext().getAttribute("levelsCache");
    	wlc.updateMarketPriority();
    	return result;
    }
}
