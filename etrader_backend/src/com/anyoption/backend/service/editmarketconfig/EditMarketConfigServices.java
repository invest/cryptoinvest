package com.anyoption.backend.service.editmarketconfig;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import com.anyoption.backend.managers.WriterPermisionsManager;
import com.anyoption.backend.service.createmarket.CopyMarketResponse;
import com.anyoption.common.backend.annotations.BackendPermission;
import com.anyoption.common.beans.MarketConfig;
import com.anyoption.common.beans.base.Skin;
import com.anyoption.common.managers.MarketsManagerBase;
import com.anyoption.common.service.CommonJSONService;
import com.anyoption.common.service.assetindex.MarketRequest;
import com.anyoption.common.service.requests.UserMethodRequest;
import com.anyoption.common.service.results.MethodResult;

public class EditMarketConfigServices {
	
	private static final String SCREEN_NAME_PERMISSIONS_MARKET_CONFIG = "marketFormulas";
	private static final Logger log = Logger.getLogger(EditMarketConfigServices.class);
	
	@BackendPermission(id = "marketFormulas_view")
	public static CopyMarketResponse getMarketConfigFieldsScreenSettings(UserMethodRequest request, HttpServletRequest httpRequest) {
		log.debug("getMarketFieldsScreenSettings:" + request);
		MarketConfigSettingsResponse response = new MarketConfigSettingsResponse();
		WriterPermisionsManager.setWriterScreenPermissionsByScreenName(SCREEN_NAME_PERMISSIONS_MARKET_CONFIG, response, httpRequest);
		try {
			Map<Long, Map<Long, String>> map = MarketsManagerBase.getMarketNamesByTypeAndSkin(Skin.SKIN_REG_EN);
			Map<String, Map<Long, Map<Long, String>>> mapFilters = new HashMap<String, Map<Long, Map<Long, String>>>();
			if (map != null) {
				mapFilters.put(MarketsManagerBase.FILTER_MARKETS, map);
			}
			response.setFilters(mapFilters);
			response.setFormulas(MarketsManagerBase.getMarketFormulas(new Locale("en")));
		} catch (SQLException e) {
			log.error("Unable to select markets list! ", e);
			response.setErrorCode(CommonJSONService.ERROR_CODE_UNKNOWN);
		}
		return response;
	}
	
	@BackendPermission(id = "marketFormulas_view")
	public static MarketConfigResponse getMarketConfigFields(MarketRequest request, HttpServletRequest httpRequest) {
		log.debug("getMarketConfigFields:" + request);
		MarketConfigResponse response = new MarketConfigResponse();
		try {
			MarketConfig marketConfig = MarketsManagerBase.getMarketConfigFormulas(request.getMarketId(), MarketsManagerBase.MARKET_DATA_SOURCE_REUTERS);
			response.setMarketConfig(marketConfig);
		} catch (SQLException e) {
			log.error("Unable to select market config fields! ", e);
			response.setErrorCode(CommonJSONService.ERROR_CODE_UNKNOWN);
		}
		return response;
	}
	
	@BackendPermission(id = "marketFormulas_view")
	public static MethodResult updateMarketConfigFields(MarketConfigRequest request, HttpServletRequest httpRequest) {
		log.debug("updateMarketConfigFields:" + request);
		MethodResult response = new MethodResult();
		try {
			if (!MarketsManagerBase.updateMarketConfigFormulas(	request.getMarketConfig(), request.getMarketId(),
																MarketsManagerBase.MARKET_DATA_SOURCE_REUTERS)) {
				response.setErrorCode(CommonJSONService.ERROR_CODE_FAIL_TO_UPDATE);
			}
		} catch (SQLException e) {
			log.error("Unable to update market config formulas! ", e);
			response.setErrorCode(CommonJSONService.ERROR_CODE_UNKNOWN);
		}
		return response;
	}
}
