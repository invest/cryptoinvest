package com.anyoption.backend.service.editmarketconfig;

import com.anyoption.common.beans.MarketConfig;
import com.anyoption.common.service.results.MethodResult;

/**
 * 
 * @author ivan.petkov
 *
 */
public class MarketConfigResponse extends MethodResult {
	
	private MarketConfig marketConfig;

	public MarketConfig getMarketConfig() {
		return marketConfig;
	}

	public void setMarketConfig(MarketConfig marketConfig) {
		this.marketConfig = marketConfig;
	}
}