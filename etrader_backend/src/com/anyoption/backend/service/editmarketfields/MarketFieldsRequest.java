package com.anyoption.backend.service.editmarketfields;

import com.anyoption.common.beans.EmirReport;
import com.anyoption.common.beans.MarketBean;
import com.anyoption.common.service.requests.MethodRequest;

import il.co.etrader.bl_vos.AssetIndex;

public class MarketFieldsRequest extends MethodRequest {

	private MarketBean market;
	private AssetIndex assetIndex;
	private EmirReport emirReport;
	private long marketId;

	public MarketBean getMarket() {
		return market;
	}

	public void setMarket(MarketBean market) {
		this.market = market;
	}

	public AssetIndex getAssetIndex() {
		return assetIndex;
	}

	public void setAssetIndex(AssetIndex assetIndex) {
		this.assetIndex = assetIndex;
	}

	public long getMarketId() {
		return marketId;
	}

	public void setMarketId(long marketId) {
		this.marketId = marketId;
	}

	public EmirReport getEmirReport() {
		return emirReport;
	}

	public void setEmirReport(EmirReport emirReport) {
		this.emirReport = emirReport;
	}
}