package com.anyoption.backend.service.editmarketfields;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import com.anyoption.backend.managers.WriterPermisionsManager;
import com.anyoption.backend.service.createmarket.CopyMarketResponse;
import com.anyoption.common.backend.annotations.BackendPermission;
import com.anyoption.common.beans.EmirReport;
import com.anyoption.common.beans.Exchange;
import com.anyoption.common.beans.InvestmentLimitsGroup;
import com.anyoption.common.beans.MarketBean;
import com.anyoption.common.beans.MarketGroup;
import com.anyoption.common.beans.base.Skin;
import com.anyoption.common.managers.BubblesManager;
import com.anyoption.common.managers.MarketsManagerBase;
import com.anyoption.common.service.CommonJSONService;
import com.anyoption.common.service.assetindex.MarketRequest;
import com.anyoption.common.service.requests.UserMethodRequest;
import com.anyoption.common.service.results.MethodResult;

import il.co.etrader.backend.bl_managers.OpportunitiesManager;
import il.co.etrader.backend.bl_vos.WriterWrapper;
import il.co.etrader.bl_managers.AssetIndexManagerBase;
import il.co.etrader.bl_managers.InvestmentsManagerBase;
import il.co.etrader.bl_managers.MarketsManager;
import il.co.etrader.bl_vos.AssetIndex;
import il.co.etrader.bl_vos.Writer;
import il.co.etrader.util.CommonUtil;
import il.co.etrader.util.ConstantsBase;

public class EditMarketFieldsServices {
	
	private static final String SCREEN_NAME_PERMISSIONS_MARKET_FIELDS = "marketFields";
	private static final Logger log = Logger.getLogger(EditMarketFieldsServices.class);
	
	@BackendPermission(id = "marketFields_view")
	public static CopyMarketResponse getMarketFieldsScreenSettings(UserMethodRequest request, HttpServletRequest httpRequest) {
		log.debug("getMarketFieldsScreenSettings:" + request);
		CopyMarketResponse response = new CopyMarketResponse();
		WriterPermisionsManager.setWriterScreenPermissionsByScreenName(SCREEN_NAME_PERMISSIONS_MARKET_FIELDS, response, httpRequest);
		try {
			Map<Long, Map<Long, String>> map = MarketsManagerBase.getMarketNamesByTypeAndSkin(Skin.SKIN_REG_EN);
			Map<String, Map<Long, Map<Long, String>>> mapFilters = new HashMap<String, Map<Long, Map<Long, String>>>();
			if (map != null) {
				mapFilters.put(MarketsManagerBase.FILTER_MARKETS, map);
			}
			response.setFilters(mapFilters);
		} catch (SQLException e) {
			log.error("Unable to select markets list! ", e);
			response.setErrorCode(CommonJSONService.ERROR_CODE_UNKNOWN);
		}
		return response;
	}
	
	@BackendPermission(id = "marketFields_view")
	public static MarketResponse getMarketFields(MarketRequest request, HttpServletRequest httpRequest) {
		log.debug("getMarketFields:" + request);
		MarketResponse response = new MarketResponse();
		try {
			MarketBean market = new MarketBean();
			market = MarketsManagerBase.getMarketById(request.getMarketId());
			Map<String, ArrayList<String>> fieldTypes = new HashMap<String, ArrayList<String>>();
			ArrayList<String> listDisabledFields = new ArrayList<String>();
			ArrayList<String> listDropdownFields = new ArrayList<String>();
			ArrayList<String> listCheckboxFields = new ArrayList<String>();
			ArrayList<String> listTimestampFields = new ArrayList<String>();
			ArrayList<String> listAmountFields = new ArrayList<String>();

			listDisabledFields.add("id");
			listDisabledFields.add("timeCreated");
			listDisabledFields.add("writerId");
			listDisabledFields.add("instantSpread");
			
			listDropdownFields.add("marketGroupId");
			listDropdownFields.add("exchangeId");
			listDropdownFields.add("investmentLimitGroupId");
			listDropdownFields.add("typeOfShifting");
			listDropdownFields.add("bubblesPricingLevel");

			listCheckboxFields.add("isHasFutureAgreement");
			listCheckboxFields.add("isNextAgreementSameDay");
			listCheckboxFields.add("useForFakeInvestments");
			listCheckboxFields.add("limitedHighAmount");
			listCheckboxFields.add("isSuspended");
			listCheckboxFields.add("isRollUp");
			listCheckboxFields.add("isGoldenMinutes");
			
			listTimestampFields.add("timeCreated");
			
			listAmountFields.add("exposureReachedAmount");
			listAmountFields.add("raiseExposureUpAmount");
			listAmountFields.add("raiseExposureDownAmount");
			listAmountFields.add("limitedFromAmount");

			fieldTypes.put("disabled", listDisabledFields);
			fieldTypes.put("dropdowns", listDropdownFields);
			fieldTypes.put("checkboxes", listCheckboxFields);
			fieldTypes.put("timestamp", listTimestampFields);
			fieldTypes.put("amounts", listAmountFields);

			Map<Long, MarketGroup> marketGroups = new HashMap<Long, MarketGroup>();
			marketGroups = MarketsManager.getMarketGroups();

			Map<Long, Exchange> exchanges = new HashMap<Long, Exchange>();
			exchanges = OpportunitiesManager.getExchanges();

			Map<Long, InvestmentLimitsGroup> investmentLimitGroups = new HashMap<Long, InvestmentLimitsGroup>();
			investmentLimitGroups = InvestmentsManagerBase.getInvestmentLimitGroupsMap();

			Map<Long, String> typeOfShifting = new LinkedHashMap<Long, String>();
			typeOfShifting.put(0L, CommonUtil.getMessage("market.shifting.type.fixed", null));
			typeOfShifting.put(1L, CommonUtil.getMessage("market.shifting.type.average", null));
			typeOfShifting.put(2L, CommonUtil.getMessage("market.shifting.type.max", null));
			
			Map<Long, String> bubblesPricingLevels = BubblesManager.getBubbleLevels();
			AssetIndex assetIndex = AssetIndexManagerBase.getAssetIndexByMarketId(request.getMarketId());
			EmirReport emirReport = MarketsManagerBase.getMarketEmirReportById(request.getMarketId());
			
			response.setEmirReport(emirReport);
			if (assetIndex == null) {
				assetIndex = new AssetIndex();
			}
			response.setAssetIndex(assetIndex);
			response.setBubblesPricingLevels(bubblesPricingLevels);
			response.setTypeOfShifting(typeOfShifting);
			response.setInvestmentLimitGroups(investmentLimitGroups);
			response.setExchanges(exchanges);
			response.setMarketGroups(marketGroups);
			response.setMarketFieldTypes(fieldTypes);
			response.setMarket(market);
		} catch (SQLException e) {
			log.error("Unable to select market fields! ", e);
			response.setErrorCode(CommonJSONService.ERROR_CODE_UNKNOWN);
		}
		return response;
	}
	
	@BackendPermission(id = "marketFields_view")
	public static MethodResult updateMarketFields(MarketFieldsRequest request, HttpServletRequest httpRequest) {
		log.debug("getMarketFields:" + request);
		MethodResult response = new MethodResult();
		try {
			MarketBean market = request.getMarket();
			WriterWrapper writerWrapper = (WriterWrapper) httpRequest.getSession().getAttribute(ConstantsBase.BIND_SESSION_WRITER_WRAPPER);
			Writer writer = writerWrapper.getWriter();
			long writerId = writer.getId();
			market.setWriterId(writerId);
			MarketsManagerBase.updateMarketById(market);
			EmirReport emirReport = request.getEmirReport();
			emirReport.setAssetId(market.getId());
			emirReport.setUait(market.getName());
			MarketsManagerBase.insertMarketForEmirReport(emirReport);
			AssetIndexManagerBase.updateAssetIndexByMarketId(request.getAssetIndex(), request.getMarketId());
			
		} catch (SQLException e) {
			log.error("Unable to update market! ", e);
			response.setErrorCode(CommonJSONService.ERROR_CODE_UNKNOWN);
		}
		return response;
	}
}