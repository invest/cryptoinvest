package com.anyoption.backend.service.salescallstatusmanager;

import java.util.ArrayList;

import com.anyoption.common.service.results.MethodResult;

import il.co.etrader.backend.bl_vos.CallsRepresentative;

public class SalesCallStatusManagerMethodResponse extends MethodResult {
	
	private ArrayList<CallsRepresentative> representatives;
	
	public ArrayList<CallsRepresentative> getRepresentatives() {
		return representatives;
	}

	public void setRepresentatives(ArrayList<CallsRepresentative> representatives) {
		this.representatives = representatives;
	}
}
