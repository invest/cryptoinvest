package com.anyoption.backend.service.salescallstatusmanager;

import com.anyoption.common.service.requests.MethodRequest;

public class SalesCallStatusManagerMethodRequest extends MethodRequest {
	
	private long fromDate;
	private long toDate;
	private long userRank;
	private long pageNumber;
	private long rowsPerPage;
	
	public long getFromDate() {
		return fromDate;
	}
	
	public void setFromDate(long fromDate) {
		this.fromDate = fromDate;
	}
	
	public long getToDate() {
		return toDate;
	}
	
	public void setToDate(long toDate) {
		this.toDate = toDate;
	}
	
	public long getUserRank() {
		return userRank;
	}

	public void setUserRank(long userRank) {
		this.userRank = userRank;
	}

	public long getWriterId() {
		return writerId;
	}

	public void setWriterId(long writerId) {
		this.writerId = writerId;
	}

	public long getSkinId() {
		return skinId;
	}

	public void setSkinId(long skinId) {
		this.skinId = skinId;
	}

	public long getPageNumber() {
		return pageNumber;
	}

	public void setPageNumber(long pageNumber) {
		this.pageNumber = pageNumber;
	}

	public long getRowsPerPage() {
		return rowsPerPage;
	}

	public void setRowsPerPage(long rowsPerPage) {
		this.rowsPerPage = rowsPerPage;
	}
	
	
}
