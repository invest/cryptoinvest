package com.anyoption.backend.service.copyopusercopiers;

import java.util.Date;

import com.anyoption.common.service.requests.MethodRequest;

public class CopyopUserCopiersMethodRequest extends MethodRequest {

	private long fromDate;
	private long toDate;
	private long userId;
	private long userLeaderId;
	private long isStillCopying;
	private long isFrozen;
	private long userOffsetId;
	private Date timeCreatedOffset;
	private boolean isNext;
	private long rowsPerPage;
	
	public long getFromDate() {
		return fromDate;
	}
	
	public void setFromDate(long fromDate) {
		this.fromDate = fromDate;
	}
	
	public long getToDate() {
		return toDate;
	}
	
	public void setToDate(long toDate) {
		this.toDate = toDate;
	}
	
	public long getUserId() {
		return userId;
	}
	
	public void setUserId(long userId) {
		this.userId = userId;
	}
	
	public long getUserLeaderId() {
		return userLeaderId;
	}
	
	public void setUserLeaderId(long userLeaderId) {
		this.userLeaderId = userLeaderId;
	}
	
	public long isStillCopying() {
		return isStillCopying;
	}
	
	public void setStillCopying(long isStillCopying) {
		this.isStillCopying = isStillCopying;
	}
	
	public long isFrozen() {
		return isFrozen;
	}
	
	public void setFrozen(long isFrozen) {
		this.isFrozen = isFrozen;
	}

	public long getUserOffsetId() {
		return userOffsetId;
	}

	public void setUserOffsetId(long userOffsetId) {
		this.userOffsetId = userOffsetId;
	}

	public Date getTimeCreatedOffset() {
		return timeCreatedOffset;
	}

	public void setTimeCreatedOffset(Date timeCreatedOffset) {
		this.timeCreatedOffset = timeCreatedOffset;
	}

	public boolean isNext() {
		return isNext;
	}

	public void setNext(boolean isNext) {
		this.isNext = isNext;
	}

	public long getRowsPerPage() {
		return rowsPerPage;
	}

	public void setRowsPerPage(long rowsPerPage) {
		this.rowsPerPage = rowsPerPage;
	}
}
