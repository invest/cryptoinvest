package com.anyoption.backend.service.copyopusercopiers;

import com.anyoption.common.beans.CopyopUserCopy;
import com.anyoption.common.service.requests.MethodRequest;

public class CopyopStopUserRequest extends MethodRequest {
	
	private CopyopUserCopy copyopUser;
	private long userId;
	private long userClassId;

	public CopyopUserCopy getCopyopUser() {
		return copyopUser;
	}

	public void setCopyopUser(CopyopUserCopy copyopUser) {
		this.copyopUser = copyopUser;
	}

	public long getUserId() {
		return userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}

	public long getUserClassId() {
		return userClassId;
	}

	public void setUserClassId(long userClassId) {
		this.userClassId = userClassId;
	}	
}
