
package com.anyoption.backend.service.idsfiles;

import java.util.ArrayList;

import com.anyoption.common.service.requests.UserMethodRequest;

public class IdsFileRequest extends UserMethodRequest {
	
	private ArrayList<Integer> uploadedBy;
	private int uploadedByBackend;
	private Long  businessCase;
	private ArrayList<Integer> skins;
	private ArrayList<Integer> countries;
	private Long ksStatus;
	private Long userId;
	private String userName;
	private Long page;
	private Long rowPerPage;
	private Long userClasses;
	
	public ArrayList<Integer> getUploadedBy() {
		return uploadedBy;
	}
	public void setUploadedBy(ArrayList<Integer> uploadedBy) {
		this.uploadedBy = uploadedBy;
	}
	public Long getBusinessCase() {
		return businessCase;
	}
	public void setBusinessCase(Long businessCase) {
		this.businessCase = businessCase;
	}
	public ArrayList<Integer> getSkins() {
		return skins;
	}
	public void setSkins(ArrayList<Integer> skins) {
		this.skins = skins;
	}
	public ArrayList<Integer> getCountries() {
		return countries;
	}
	public void setCountries(ArrayList<Integer> countries) {
		this.countries = countries;
	}
	public Long getKsStatus() {
		return ksStatus;
	}
	public void setKsStatus(Long ksStatus) {
		this.ksStatus = ksStatus;
	}
	public Long getUserId() {
		return userId;
	}
	public void setUserId(Long userId) {
		this.userId = userId;
	}
	
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}	

    public Long getPage() {
		return page;
	}
	public void setPage(Long page) {
		this.page = page;
	}
	
    public String toString() {
        String ls = System.getProperty("line.separator");
        return ls + "IdsFileRequest: "
            + super.toString()
            + "uploadedBy: " + uploadedBy + ls
            + "businessCase: " + businessCase + ls
            + "skins: " + skins +ls
        	+ "countries: " + countries +ls
        	+ "ksStatus: " + ksStatus +ls
        	+ "userId: " + userId +ls
        	+ "userName: " + userName +ls
        	+ "userClassType: " + userClasses +ls
        	+ "page: " + page +ls;
    }
	public Long getRowPerPage() {
		return rowPerPage;
	}
	public void setRowPerPage(Long rowPerPage) {
		this.rowPerPage = rowPerPage;
	}
	public int getUploadedByBackend() {
		return uploadedByBackend;
	}
	public void setUploadedByBackend(int uploadedByBackend) {
		this.uploadedByBackend = uploadedByBackend;
	}
	public Long getUserClasses() {
		return userClasses;
	}
	public void setUserClasses(Long userClasses) {
		this.userClasses = userClasses;
	}

}
