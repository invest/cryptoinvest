package com.anyoption.backend.service.markettranslations;

import com.anyoption.common.beans.MarketTranslations;
import com.anyoption.common.service.results.MethodResult;

public class MarketTranslationsResponse extends MethodResult {
	
	private MarketTranslations marketTranslations;

	public MarketTranslations getMarketTranslations() {
		return marketTranslations;
	}

	public void setMarketTranslations(MarketTranslations marketTranslations) {
		this.marketTranslations = marketTranslations;
	}
}
