package com.anyoption.backend.service.userdocumentsscreens;

import com.anyoption.common.service.requests.MethodRequest;

public class UserFileCommentsRequest extends MethodRequest {

	private long userId;
	private String comments;

	public long getUserId() {
		return userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	} 
}
