/**
 * 
 */
package com.anyoption.backend.service.userdocumentsscreens;

import java.util.ArrayList;

import com.anyoption.common.service.results.MethodResult;

/**
 * @author pavel.t
 *
 */
public class UsersDocumentsResponse extends MethodResult {

	private ArrayList<UsersDocuments> usersDocuments;
	private long totalCount;

	public ArrayList<UsersDocuments> getUsersDocuments() {
		return usersDocuments;
	}

	public void setUsersDocuments(ArrayList<UsersDocuments> usersDocuments) {
		this.usersDocuments = usersDocuments;
	}

	public long getTotalCount() {
		return totalCount;
	}

	public void setTotalCount(long totalCount) {
		this.totalCount = totalCount;
	}

}
