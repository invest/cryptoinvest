package com.anyoption.backend.service.userdocumentsscreens;


import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import com.anyoption.common.beans.Issue;
import com.anyoption.common.beans.IssueAction;
import com.anyoption.common.beans.ServerConfiguration;
import com.anyoption.common.daos.IssuesDAOBase;
import com.anyoption.common.managers.BaseBLManager;
import com.anyoption.common.util.ConstantsBase;

public class UserDocumentsScreenManager extends BaseBLManager {
	
	public static long DOC_STATUS_MISSING = 1;
	public static long DOC_STATUS_PENDING = 2;
	public static long DOC_STATUS_APPROVED = 3;
	
	public static HashMap<Long, String> userActive;
	public static HashMap<Long, String> complianceApproved;
	public static HashMap<Long, String> resolution;
	public static HashMap<Long, String> suspended;
	public static HashMap<Long, String> docType;
	
	
	public static HashMap<Long, String> getUserActive() {
		userActive = new HashMap<>();
		userActive.put(1l, "Yes");
		userActive.put(0l, "No");
		return userActive;
	}

	public static HashMap<Long, String> getSuspended() {
		suspended = new HashMap<>();
		suspended.put(1l, "Yes");
		suspended.put(0l, "No");
		return suspended;
	}

	public static HashMap<Long, String> getComplianceApproved() {
		complianceApproved = new HashMap<>();
		complianceApproved.put(1l, "Yes");
		complianceApproved.put(0l, "No");
		return complianceApproved;
	}

	public static HashMap<Long, String> getResolution() {
		resolution = new HashMap<>();
		resolution.put(1l, "Missing");
		resolution.put(2l, "Pending");
		resolution.put(3l, "Approved");
		resolution.put(4l, "Rejected");
		return resolution;
	}

	public static HashMap<Long, String> getDocType() {
		docType = new HashMap<>();
		docType.put(1l, "Proof of Identity(POI)");
		docType.put(2l, "Proof of Residence(POR)");
		docType.put(3l, "Selfie Verification");
		return docType;
	}	
	
	public static ArrayList<UserDocuments> getCallCenterDocuments(UserDocumentsScreenGetRequest request, String writerUtcOffset) throws SQLException {
		Connection conn = null;		 
		try {
			conn = getConnection();
			return UserDocumentsScreenDao.getCallCenterDocuments(conn, request, writerUtcOffset);
		} finally {
			closeConnection(conn);
		}		
	}
	
	public static ArrayList<UserDocuments> getBackOfficeDocuments(UserDocumentsScreenGetRequest request, String writerUtcOffset) throws SQLException {
		Connection conn = null;		 
		try {
			conn = getConnection();
			return UserDocumentsScreenDao.getBackOfficeDocuments(conn, request, writerUtcOffset);
		} finally {
			closeConnection(conn);
		}		
	}
	
	public static UsersDocumentsResponse getUsersDocuments(UsersDocumentsRequest request) throws SQLException {
		Connection conn = null;
		try {
			conn = getConnection();
			return UserDocumentsScreenDao.getUsersDocuments(request, conn);
		} finally {
			closeConnection(conn);
		}
	}
	
	public static UserDocumentsUserDataGetResponse getUserDocumentsUserData(UserDocumentsUserDataGetRequest request) throws SQLException {
		Connection conn = null;		 
		try {
			conn = getConnection();
			return UserDocumentsScreenDao.getUserDocumentsUserData(conn, request);
		} finally {
			closeConnection(conn);
		}		
	}
	
	public static UserDocumentsDataResponse getUsersDocumentsData(UsersDocumentsRequest request) throws SQLException {
		Connection conn = null;
		try {
			conn = getConnection();
			return UserDocumentsScreenDao.getUsersDocumentsData(conn, request);
		} finally {
			closeConnection(conn);
		}
	}
	
	public static boolean isCanRegulationApprove(long userId) throws SQLException {
		Connection conn = null;		 
		try {
			conn = getConnection();
			return UserDocumentsScreenDao.isCanRegulationApprove(conn, userId);
		} finally {
			closeConnection(conn);
		}		
	}
	
	public static boolean isCanCryptoRegApprove(long userId) throws SQLException {
		Connection conn = null;
		try {
			conn = getConnection();
			return UserDocumentsScreenDao.isCanCryptoRegApprove(conn, userId);
		} finally {
			closeConnection(conn);
		}
	}
	
	public static Long getDocumentStatus(long userId) throws SQLException {
		Connection conn = null;		 
		try {
			conn = getConnection();
			return UserDocumentsScreenDao.getDocumentStatus(conn, userId);
		} finally {
			closeConnection(conn);
		}		
	}
	
	public static String getDocumentStatusTxt(long userId) throws SQLException {
		String res = "userstrip.doc.status.na"; 
		long status= getDocumentStatus(userId);
		if(status == DOC_STATUS_MISSING){
			return res = "userstrip.doc.status.missing";
		}
		if(status == DOC_STATUS_PENDING){
			return res = "userstrip.doc.status.pending";
		}
		if(status == DOC_STATUS_APPROVED){
			return res = "userstrip.doc.status.approved";
		}
		return res;
	}
	
	public static String lockDocumentStatus(String sessionId, long writerId, long userId, long screenId) throws SQLException {
		Connection conn = null;		 
		try {
			conn = getConnection();
			return UserDocumentsScreenDao.lockDocumentStatus(conn, sessionId, writerId, userId, screenId);
		} finally {
			closeConnection(conn);
		}		
	}
	
	public static void unlockDocumentStatusBySession(String sessionId) throws SQLException {
		unlockDocumentStatus(sessionId, null, null, null);
	}
	
	public static void unlockDocumentStatusByWriter(long writerId, long screenId) throws SQLException {
		unlockDocumentStatus(null, writerId, null, screenId);
	}
	
	public static void unlockDocumentStatusByServer() throws SQLException {
		String serverId = ServerConfiguration.getInstance().getServerName();
		unlockDocumentStatus(null, null, serverId, null);
	}	
	
	public static void saveComments(long userId, long writerId, String comments, String writerUtcOffset) throws SQLException {
		Connection con = null;
		try {
			con = getConnection();
			if (IssuesDAOBase.isPendingDocIssuesForUser(con, userId)) {
				IssueAction vo = new IssueAction();
				vo.setIssueId(IssuesDAOBase.getPendingDocsIssueId(con, userId));
				vo.setActionTypeId(ConstantsBase.ISSUE_ACTION_TYPE_COMMENT);
				vo.setWriterId(writerId);
				vo.setActionTime(new Date());
				vo.setActionTimeOffset(writerUtcOffset);
				vo.setComments(comments);
				vo.setSignificant(false);
				vo.setTransactionId(0);
				IssuesDAOBase.insertAction(con, vo);
			} else {
				Issue vo = new Issue();
				vo.setUserId(userId);
				vo.setSubjectId("" + ConstantsBase.ISSUE_SUBJECT_PENDING_DOC + "");
				vo.setPriorityId("" + ConstantsBase.ISSUE_PRIORITY_LOW +"");
				vo.setStatusId("" + ConstantsBase.ISSUE_STATUS_PENDING + "");
				vo.setComponentId(ConstantsBase.ISSUE_COMPONENT_ETRADER_ANYOPTION);
				IssuesDAOBase.insertIssue(con, vo);
				
				IssueAction ia = new IssueAction();
				ia.setIssueId(IssuesDAOBase.getPendingDocsIssueId(con, userId));
				ia.setActionTypeId(ConstantsBase.ISSUE_ACTION_TYPE_COMMENT);
				ia.setWriterId(writerId);
				ia.setActionTime(new Date());
				ia.setActionTimeOffset(writerUtcOffset);
				ia.setComments(comments);
				ia.setSignificant(false);
				ia.setTransactionId(0);
				IssuesDAOBase.insertAction(con, ia);
			}
		} finally {
			closeConnection(con);
		}
	}
	
	private static void unlockDocumentStatus(String sessionId, Long writerId, String serverId, Long screenId) throws SQLException {
		Connection conn = null;		 
		try {
			conn = getConnection();
			UserDocumentsScreenDao.unlockDocumentStatus(conn, sessionId, writerId, serverId, screenId);
		} finally {
			closeConnection(conn);
		}		
	}
	
}
