
package com.anyoption.backend.service.userdocumentsscreens;

import java.util.ArrayList;

import com.anyoption.common.service.requests.UserMethodRequest;

public class UserDocumentsScreenGetRequest extends UserMethodRequest {
	
	private Long ftdDateFrom;
	private Long ftdDateTo;
	private Long firstEngagementDateFrom;
	private Long firstEngagementDateTo;
	private Long userId;
	private String userName;
	private Long userClasses;
	private ArrayList<Integer> skins;
	private ArrayList<Integer> countries;
	private Long pendingWithdrawal;
	private ArrayList<Integer> daysLeft;
	private Long suspended;
	private Long active;
	private Long complianceApproved;
	private Long docUploaded;
	private Long fromDeposits;
	private Long toDeposits;
	private Long callAttempts;
	private Long page;
	private Long rowsPerPage;
	private Long orderById;
	private Long ascDesc;
	private Long docType1;
	private ArrayList<Integer> resolutionStatus1;
	private Long docType2;
	private ArrayList<Integer> resolutionStatus2;
	private Long expireDoc;
	private ArrayList<Integer> businessCases;
		
	
	public Long getUserId() {
		return userId;
	}
	public void setUserId(Long userId) {
		this.userId = userId;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public Long getUserClasses() {
		return userClasses;
	}
	public void setUserClasses(Long userClasses) {
		this.userClasses = userClasses;
	}
	public ArrayList<Integer> getSkins() {
		return skins;
	}
	public void setSkins(ArrayList<Integer> skins) {
		this.skins = skins;
	}
	public ArrayList<Integer> getCountries() {
		return countries;
	}
	public void setCountries(ArrayList<Integer> countries) {
		this.countries = countries;
	}
	public Long getPendingWithdrawal() {
		return pendingWithdrawal;
	}
	public void setPendingWithdrawal(Long pendingWithdrawal) {
		this.pendingWithdrawal = pendingWithdrawal;
	}
	public ArrayList<Integer> getDaysLeft() {
		return daysLeft;
	}
	public void setDaysLeft(ArrayList<Integer> daysLeft) {
		this.daysLeft = daysLeft;
	}
	public Long getSuspended() {
		return suspended;
	}
	public void setSuspended(Long suspended) {
		this.suspended = suspended;
	}
	public Long getActive() {
		return active;
	}
	public void setActive(Long active) {
		this.active = active;
	}
	public Long getComplianceApproved() {
		return complianceApproved;
	}
	public void setComplianceApproved(Long complianceApproved) {
		this.complianceApproved = complianceApproved;
	}
	public Long getDocUploaded() {
		return docUploaded;
	}
	public void setDocUploaded(Long docUploaded) {
		this.docUploaded = docUploaded;
	}
	public Long getFromDeposits() {
		return fromDeposits;
	}
	public void setFromDeposits(Long fromDeposits) {
		this.fromDeposits = fromDeposits;
	}
	public Long getToDeposits() {
		return toDeposits;
	}
	public void setToDeposits(Long toDeposits) {
		this.toDeposits = toDeposits;
	}

	public Long getPage() {
		return page;
	}
	public void setPage(Long page) {
		this.page = page;
	}
	public Long getRowsPerPage() {
		return rowsPerPage;
	}
	public void setRowsPerPage(Long rowsPerPage) {
		this.rowsPerPage = rowsPerPage;
	}	
	
	public Long getOrderById() {
		return orderById;
	}
	public void setOrderById(Long orderById) {
		this.orderById = orderById;
	}
	public Long getAscDesc() {
		return ascDesc;
	}
	public void setAscDesc(Long ascDesc) {
		this.ascDesc = ascDesc;
	}
	public Long getFtdDateTo() {
		return ftdDateTo;
	}
	public void setFtdDateTo(Long ftdDateTo) {
		this.ftdDateTo = ftdDateTo;
	}
	public Long getFtdDateFrom() {
		return ftdDateFrom;
	}
	public void setFtdDateFrom(Long ftdDateFrom) {
		this.ftdDateFrom = ftdDateFrom;
	}
	
    public Long getFirstEngagementDateFrom() {
		return firstEngagementDateFrom;
	}
    
	public void setFirstEngagementDateFrom(Long firstEngagementDateFrom) {
		this.firstEngagementDateFrom = firstEngagementDateFrom;
	}
	
	public Long getFirstEngagementDateTo() {
		return firstEngagementDateTo;
	}
	
	public void setFirstEngagementDateTo(Long firstEngagementDateTo) {
		this.firstEngagementDateTo = firstEngagementDateTo;
	}
	
    public Long getDocType1() {
		return docType1;
	}
	public void setDocType1(Long docType1) {
		this.docType1 = docType1;
	}
	public ArrayList<Integer> getResolutionStatus1() {
		return resolutionStatus1;
	}
	public void setResolutionStatus1(ArrayList<Integer> resolutionStatus1) {
		this.resolutionStatus1 = resolutionStatus1;
	}
	public Long getDocType2() {
		return docType2;
	}
	public void setDocType2(Long docType2) {
		this.docType2 = docType2;
	}
	public ArrayList<Integer> getResolutionStatus2() {
		return resolutionStatus2;
	}
	public void setResolutionStatus2(ArrayList<Integer> resolutionStatus2) {
		this.resolutionStatus2 = resolutionStatus2;
	}
	public Long getCallAttempts() {
		return callAttempts;
	}
	public void setCallAttempts(Long callAttempts) {
		this.callAttempts = callAttempts;
	}
	public Long getExpireDoc() {
		return expireDoc;
	}
	public void setExpireDoc(Long expireDoc) {
		this.expireDoc = expireDoc;
	}
	
	public ArrayList<Integer> getBusinessCases() {
		return businessCases;
	}
	public void setBusinessCases(ArrayList<Integer> businessCases) {
		this.businessCases = businessCases;
	}
	public String toString() {
        String ls = System.getProperty("line.separator");
        return ls + "UserDocumentsScreenGetRequest: "
            + super.toString()
            + "ftdDateFrom: " + ftdDateFrom + ls
            + "ftdDateTo: " + ftdDateTo + ls
            + "firstEngagementDateFrom: " + firstEngagementDateFrom + ls
            + "firstEngagementDateTo: " + firstEngagementDateTo + ls
            + "userId" + userId + ls
            + "userName: " + userName + ls
            + "userClasses: " + userClasses + ls
            + "skins: " + skins + ls
            + "countries: " + countries + ls
            + "pendingWithdrawal: " + pendingWithdrawal + ls
            + "daysLeft: " + daysLeft + ls
            + "suspended: " + suspended + ls
            + "active: " + active + ls
            + "complianceApproved: " + complianceApproved + ls
            + "docUploaded: " + docUploaded + ls
            + "fromDeposits: " + fromDeposits + ls
            + "toDeposits: " + toDeposits + ls
            + "rowsPerPage: " + rowsPerPage + ls
            + "page: " + page + ls
            + "rowsPerPage: " + rowsPerPage + ls
        	+ "orderById: " + orderById + ls
        	+ "ascDesc: " + ascDesc + ls
        	+ "docType1: " + docType1 + ls
        	+ "resolutionStatus1: " + resolutionStatus1 + ls
        	+ "docType2: " + docType2 + ls
        	+ "resolutionStatus2: " + resolutionStatus2 + ls
        	+ "expireDoc: " + expireDoc + ls;
        }
}
