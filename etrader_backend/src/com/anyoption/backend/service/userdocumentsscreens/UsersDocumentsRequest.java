/**
 * 
 */
package com.anyoption.backend.service.userdocumentsscreens;

import java.util.ArrayList;

import com.anyoption.common.service.requests.MethodRequest;

/**
 * @author pavel.t
 *
 */
public class UsersDocumentsRequest extends MethodRequest {
	

	private Long userId;
	private String firstName;
	private String lastName;
	private String email;
	private ArrayList<Integer> countries;
	private Long languageId;
	private Long isTester;
	private Long suspended;
	private Long active;
	private Long isRegulationApproved;
	private Long page;
	private Long rowsPerPage;
	private Long docType1;
	private ArrayList<Integer> resolutionStatus1;
	private Long docType2;
	private ArrayList<Integer> resolutionStatus2;
	private String comments;
	/**
	 * @return the userId
	 */
	public Long getUserId() {
		return userId;
	}
	/**
	 * @param userId the userId to set
	 */
	public void setUserId(Long userId) {
		this.userId = userId;
	}
	/**
	 * @return the firstName
	 */
	public String getFirstName() {
		return firstName;
	}
	/**
	 * @param firstName the firstName to set
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	/**
	 * @return the lastName
	 */
	public String getLastName() {
		return lastName;
	}
	/**
	 * @param lastName the lastName to set
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}
	/**
	 * @param email the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}
	/**
	 * @return the countries
	 */
	public ArrayList<Integer> getCountries() {
		return countries;
	}
	/**
	 * @param countries the countries to set
	 */
	public void setCountries(ArrayList<Integer> countries) {
		this.countries = countries;
	}
	/**
	 * @return the languageId
	 */
	public Long getLanguageId() {
		return languageId;
	}
	/**
	 * @param languageId the languageId to set
	 */
	public void setLanguageId(Long languageId) {
		this.languageId = languageId;
	}
	/**
	 * @return the isTester
	 */
	public Long getIsTester() {
		return isTester;
	}
	/**
	 * @param isTester the isTester to set
	 */
	public void setIsTester(Long isTester) {
		this.isTester = isTester;
	}
	/**
	 * @return the suspended
	 */
	public Long getSuspended() {
		return suspended;
	}
	/**
	 * @param suspended the suspended to set
	 */
	public void setSuspended(Long suspended) {
		this.suspended = suspended;
	}
	/**
	 * @return the active
	 */
	public Long getActive() {
		return active;
	}
	/**
	 * @param active the active to set
	 */
	public void setActive(Long active) {
		this.active = active;
	}
	/**
	 * @return the isRegulationApproved
	 */
	public Long getIsRegulationApproved() {
		return isRegulationApproved;
	}
	/**
	 * @param isRegulationApproved the isRegulationApproved to set
	 */
	public void setIsRegulationApproved(Long isRegulationApproved) {
		this.isRegulationApproved = isRegulationApproved;
	}
	/**
	 * @return the page
	 */
	public Long getPage() {
		return page;
	}
	/**
	 * @param page the page to set
	 */
	public void setPage(Long page) {
		this.page = page;
	}
	/**
	 * @return the rowsPerPage
	 */
	public Long getRowsPerPage() {
		return rowsPerPage;
	}
	/**
	 * @param rowsPerPage the rowsPerPage to set
	 */
	public void setRowsPerPage(Long rowsPerPage) {
		this.rowsPerPage = rowsPerPage;
	}
	/**
	 * @return the docType1
	 */
	public Long getDocType1() {
		return docType1;
	}
	/**
	 * @param docType1 the docType1 to set
	 */
	public void setDocType1(Long docType1) {
		this.docType1 = docType1;
	}
	/**
	 * @return the resolutionStatus1
	 */
	public ArrayList<Integer> getResolutionStatus1() {
		return resolutionStatus1;
	}
	/**
	 * @param resolutionStatus1 the resolutionStatus1 to set
	 */
	public void setResolutionStatus1(ArrayList<Integer> resolutionStatus1) {
		this.resolutionStatus1 = resolutionStatus1;
	}
	/**
	 * @return the docType2
	 */
	public Long getDocType2() {
		return docType2;
	}
	/**
	 * @param docType2 the docType2 to set
	 */
	public void setDocType2(Long docType2) {
		this.docType2 = docType2;
	}
	/**
	 * @return the resolutionStatus2
	 */
	public ArrayList<Integer> getResolutionStatus2() {
		return resolutionStatus2;
	}
	/**
	 * @param resolutionStatus2 the resolutionStatus2 to set
	 */
	public void setResolutionStatus2(ArrayList<Integer> resolutionStatus2) {
		this.resolutionStatus2 = resolutionStatus2;
	}
	public String getComments() {
		return comments;
	}
	public void setComments(String comments) {
		this.comments = comments;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "UsersDocumentsRequest [userId=" + userId + ", firstName=" + firstName + ", lastName=" + lastName
				+ ", email=" + email + ", countries=" + countries + ", languageId=" + languageId + ", isTester="
				+ isTester + ", suspended=" + suspended + ", active=" + active + ", isRegulationApproved="
				+ isRegulationApproved + ", page=" + page + ", rowsPerPage=" + rowsPerPage + ", docType1=" + docType1
				+ ", resolutionStatus1=" + resolutionStatus1 + ", docType2=" + docType2 + ", resolutionStatus2="
				+ resolutionStatus2 + "]";
	}
}
