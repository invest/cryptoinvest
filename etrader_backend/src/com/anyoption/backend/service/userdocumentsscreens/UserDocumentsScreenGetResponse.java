/**
 * 
 */
package com.anyoption.backend.service.userdocumentsscreens;

import java.util.ArrayList;

import com.anyoption.common.service.results.MethodResult;

public class UserDocumentsScreenGetResponse extends MethodResult {
	
	private ArrayList<UserDocuments> UserDocuments;

	public ArrayList<UserDocuments> getUserDocuments() {
		return UserDocuments;
	}

	public void setUserDocuments(ArrayList<UserDocuments> userDocuments) {
		UserDocuments = userDocuments;
	}
}
