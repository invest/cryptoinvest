
package com.anyoption.backend.service.userdocumentsscreens;

import com.anyoption.common.service.requests.UserMethodRequest;

public class UserDocumentsUserDataGetRequest extends UserMethodRequest {
	
	private Long userId;
	private String comments;
		
	public Long getUserId() {
		return userId;
	}
	public void setUserId(Long userId) {
		this.userId = userId;
	}	
	
	public String getComments() {
		return comments;
	}
	public void setComments(String comments) {
		this.comments = comments;
	}
	
	public String toString() {
        String ls = System.getProperty("line.separator");
        return ls + "UserDocumentsUserDataGetRequest: "
            + super.toString()
            + "userId" + userId + ls;
        }
}
