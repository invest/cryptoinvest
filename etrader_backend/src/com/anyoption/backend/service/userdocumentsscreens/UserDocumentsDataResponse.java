/**
 * 
 */
package com.anyoption.backend.service.userdocumentsscreens;

import java.util.ArrayList;

import com.anyoption.common.service.results.MethodResult;

/**
 * @author pavel.t
 *
 */
public class UserDocumentsDataResponse extends MethodResult {
	
	private long userId;
	private Long registrionDate;
	private Long engagementDate;
	private String accountManagerName;
	private Long balance;
	private Long balanceCurrencyId;
	private String phoneNumber;
	private Long lastLoginDate;
	private boolean isEnableRegulationApprove;
	private boolean isEnableRegulationApproveManager;		
	private ArrayList<UserDocumentsFile> files;
	private Long callAttempts;
	private Long lastCallTime;
	private Long lastReachedTime;
	private String comments;
	/**
	 * @return the userId
	 */
	public long getUserId() {
		return userId;
	}
	/**
	 * @param userId the userId to set
	 */
	public void setUserId(long userId) {
		this.userId = userId;
	}
	/**
	 * @return the registrionDate
	 */
	public Long getRegistrionDate() {
		return registrionDate;
	}
	/**
	 * @param registrionDate the registrionDate to set
	 */
	public void setRegistrionDate(Long registrionDate) {
		this.registrionDate = registrionDate;
	}
	/**
	 * @return the engagementDate
	 */
	public Long getEngagementDate() {
		return engagementDate;
	}
	/**
	 * @param engagementDate the engagementDate to set
	 */
	public void setEngagementDate(Long engagementDate) {
		this.engagementDate = engagementDate;
	}
	/**
	 * @return the accountManagerName
	 */
	public String getAccountManagerName() {
		return accountManagerName;
	}
	/**
	 * @param accountManagerName the accountManagerName to set
	 */
	public void setAccountManagerName(String accountManagerName) {
		this.accountManagerName = accountManagerName;
	}
	/**
	 * @return the balance
	 */
	public Long getBalance() {
		return balance;
	}
	/**
	 * @param balance the balance to set
	 */
	public void setBalance(Long balance) {
		this.balance = balance;
	}
	/**
	 * @return the balanceCurrencyId
	 */
	public Long getBalanceCurrencyId() {
		return balanceCurrencyId;
	}
	/**
	 * @param balanceCurrencyId the balanceCurrencyId to set
	 */
	public void setBalanceCurrencyId(Long balanceCurrencyId) {
		this.balanceCurrencyId = balanceCurrencyId;
	}
	/**
	 * @return the phoneNumber
	 */
	public String getPhoneNumber() {
		return phoneNumber;
	}
	/**
	 * @param phoneNumber the phoneNumber to set
	 */
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	/**
	 * @return the lastLoginDate
	 */
	public Long getLastLoginDate() {
		return lastLoginDate;
	}
	/**
	 * @param lastLoginDate the lastLoginDate to set
	 */
	public void setLastLoginDate(Long lastLoginDate) {
		this.lastLoginDate = lastLoginDate;
	}
	/**
	 * @return the isEnableRegulationApprove
	 */
	public boolean isEnableRegulationApprove() {
		return isEnableRegulationApprove;
	}
	/**
	 * @param isEnableRegulationApprove the isEnableRegulationApprove to set
	 */
	public void setEnableRegulationApprove(boolean isEnableRegulationApprove) {
		this.isEnableRegulationApprove = isEnableRegulationApprove;
	}
	/**
	 * @return the isEnableRegulationApproveManager
	 */
	public boolean isEnableRegulationApproveManager() {
		return isEnableRegulationApproveManager;
	}
	/**
	 * @param isEnableRegulationApproveManager the isEnableRegulationApproveManager to set
	 */
	public void setEnableRegulationApproveManager(boolean isEnableRegulationApproveManager) {
		this.isEnableRegulationApproveManager = isEnableRegulationApproveManager;
	}
	/**
	 * @return the files
	 */
	public ArrayList<UserDocumentsFile> getFiles() {
		return files;
	}
	/**
	 * @param files the files to set
	 */
	public void setFiles(ArrayList<UserDocumentsFile> files) {
		this.files = files;
	}
	/**
	 * @return the callAttempts
	 */
	public Long getCallAttempts() {
		return callAttempts;
	}
	/**
	 * @param callAttempts the callAttempts to set
	 */
	public void setCallAttempts(Long callAttempts) {
		this.callAttempts = callAttempts;
	}
	/**
	 * @return the lastCallTime
	 */
	public Long getLastCallTime() {
		return lastCallTime;
	}
	/**
	 * @param lastCallTime the lastCallTime to set
	 */
	public void setLastCallTime(Long lastCallTime) {
		this.lastCallTime = lastCallTime;
	}
	/**
	 * @return the lastReachedTime
	 */
	public Long getLastReachedTime() {
		return lastReachedTime;
	}
	/**
	 * @param lastReachedTime the lastReachedTime to set
	 */
	public void setLastReachedTime(Long lastReachedTime) {
		this.lastReachedTime = lastReachedTime;
	}
	/**
	 * @return the comments
	 */
	public String getComments() {
		return comments;
	}
	/**
	 * @param comments the comments to set
	 */
	public void setComments(String comments) {
		this.comments = comments;
	}

}
