/**
 * 
 */
package com.anyoption.backend.service.userdocumentsscreens;

import java.io.Serializable;

/**
 * @author pavel.t
 *
 */
public class UsersDocuments implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4368706429248964746L;
	
    private long userId;
    private String firstName;
    private String lastName;
    private String email;
    private String countryName;
    private long languageId;
    private long isRegulationApproved;
    private long complDocsUpload;
	/**
	 * @return the userId
	 */
	public long getUserId() {
		return userId;
	}
	/**
	 * @param userId the userId to set
	 */
	public void setUserId(long userId) {
		this.userId = userId;
	}
	/**
	 * @return the firstName
	 */
	public String getFirstName() {
		return firstName;
	}
	/**
	 * @param firstName the firstName to set
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	/**
	 * @return the lastName
	 */
	public String getLastName() {
		return lastName;
	}
	/**
	 * @param lastName the lastName to set
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}
	/**
	 * @param email the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}
	/**
	 * @return the countryName
	 */
	public String getCountryName() {
		return countryName;
	}
	/**
	 * @param countryName the countryName to set
	 */
	public void setCountryName(String countryName) {
		this.countryName = countryName;
	}
	/**
	 * @return the languageId
	 */
	public long getLanguageId() {
		return languageId;
	}
	/**
	 * @param languageId the languageId to set
	 */
	public void setLanguageId(long languageId) {
		this.languageId = languageId;
	}
	/**
	 * @return the isRegulationApproved
	 */
	public long getIsRegulationApproved() {
		return isRegulationApproved;
	}
	/**
	 * @param isRegulationApproved the isRegulationApproved to set
	 */
	public void setIsRegulationApproved(long isRegulationApproved) {
		this.isRegulationApproved = isRegulationApproved;
	}
	/**
	 * @return the complDocsUpload
	 */
	public long getComplDocsUpload() {
		return complDocsUpload;
	}
	/**
	 * @param complDocsUpload the complDocsUpload to set
	 */
	public void setComplDocsUpload(long complDocsUpload) {
		this.complDocsUpload = complDocsUpload;
	}

}
