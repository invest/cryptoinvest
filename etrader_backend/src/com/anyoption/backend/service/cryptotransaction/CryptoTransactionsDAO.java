package com.anyoption.backend.service.cryptotransaction;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.anyoption.common.beans.Transaction;
import com.anyoption.common.daos.DAOBase;
import com.invest.common.beans.CryptoTrade;
import com.invest.common.enums.OrderState;

/**
 * @author Kiril.m
 */
public class CryptoTransactionsDAO extends DAOBase {

	public static Transaction getTransaction(Connection con, long id, long userId) throws SQLException {
		String sql = "select id, amount, assignee, type_id, status_id from transactions where id = ? and user_id = ?";
		try (PreparedStatement ps = con.prepareStatement(sql)) {
			ps.setLong(1, id);
			ps.setLong(2, userId);
			ResultSet rs = ps.executeQuery();
			if (rs.next()) {
				Transaction t = new Transaction();
				t.setId(rs.getLong("id"));
				t.setUserId(userId);
				t.setAmount(rs.getLong("amount"));
				t.setAssignee(rs.getString("assignee"));
				t.setTypeId(rs.getLong("type_id"));
				t.setStatusId(rs.getLong("status_id"));
				return t;
			}
		}
		return null;
	}

	public static boolean updateTransaction(Connection con, long id, long amount, String comments, long reviewDate,
											long writerId, String assignee) throws SQLException {
		String sql = "update transactions set amount = ?, comments = concat(comments, ?), time_settled = ?, processed_writer_id = ?,"
						+ " assignee = ? "
					+ "where id = ?";
		try (PreparedStatement ps = con.prepareStatement(sql)) {
			ps.setLong(1, amount);
			ps.setString(2, comments);
			ps.setTimestamp(3, new Timestamp(reviewDate));
			ps.setLong(4, writerId);
			ps.setString(5, assignee);
			ps.setLong(6, id);
			return ps.executeUpdate() == 1;
		}
	}

	public static boolean changeTransactionAssignee(Connection con, long id, String assignee, long writerId) throws SQLException {
		String sql = "update transactions set assignee = ?, processed_writer_id = ? where id = ?";
		try (PreparedStatement ps = con.prepareStatement(sql)) {
			ps.setString(1, assignee);
			ps.setLong(2, writerId);
			ps.setLong(3, id);
			return ps.executeUpdate() == 1;
		}
	}

	public static boolean updateAssigneeResolution(Connection con, long transactionId, String assignee, int status, int rejectReasonId, String comment, long writerId) throws SQLException {
		String sql = "update crypto_transaction_resolutions set status = ?, reject_reason_id = ?, writer_comment = ?, writer_id = ?, "
						+ "time_updated = sysdate "
					+ "where transaction_id = ? and assignee = ?";
		try (PreparedStatement ps = con.prepareStatement(sql)) {
			ps.setInt(1, status);
			ps.setInt(2, rejectReasonId);
			ps.setString(3, comment);
			ps.setLong(4, writerId);
			ps.setLong(5, transactionId);
			ps.setString(6, assignee);
			return ps.executeUpdate() == 1;
		}
	}

	public static Map<String, TransactionResolution> getTransactionResolution(Connection con, long transactionId) throws SQLException {
		String sql = "select transaction_id, assignee, status, writer_comment, reject_reason_id from crypto_transaction_resolutions "
					+ "where transaction_id = ?";
		Map<String, TransactionResolution> result = new HashMap<>();
		try (PreparedStatement ps = con.prepareStatement(sql)) {
			ps.setLong(1, transactionId);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				result.put(	rs.getString("assignee"),
							new TransactionResolution(	rs.getLong("transaction_id"), rs.getString("assignee"), rs.getInt("status"),
														rs.getString("writer_comment"), rs.getInt("reject_reason_id")));
			}
		}
		return result;
	}

	public static boolean updateTransactionStatus(Connection con, long id, long writerId, int statusId) throws SQLException {
		String sql = "update transactions set status_id = ?, processed_writer_id = ? where id = ?";
		try (PreparedStatement ps = con.prepareStatement(sql)) {
			ps.setInt(1, statusId);
			ps.setLong(2, writerId);
			ps.setLong(3, id);
			return ps.executeUpdate() == 1;
		}
	}

	public static List<CryptoTransaction> searchTransactions(Connection con, long userId, int writerId) throws SQLException {
		String sql = "select t.id, t.user_id, t.amount, t.assignee, t.time_created, t.type_id, t.status_id, t.time_settled, t.comments, "
						+ "ct.to_currency, ct.quantity, ct.quantity_client, ct.rate_market, ct.rate_market_client, ct.fee, "
						+ "ct.clearance_fee, ct.rate, ct.status, ct.time_done, ct.wallet, cw.status as withdraw_status, "
						+ "cwh.tx_id as withdraw_id "
					+ "from transactions t "
						+ "join crypto_trade ct on t.id = ct.transaction_id "
						+ "left join crypto_withdraw cw on t.id = cw.transaction_id "
						+ "left join crypto_withdrawal_history cwh on cw.uuid = cwh.payment_uuid "
					+ "where t.user_id = ? and t.writer_id = ? "
					+ "order by t.id desc";
		List<CryptoTransaction> result = new ArrayList<>();
		try (PreparedStatement ps = con.prepareStatement(sql)) {
			ps.setLong(1, userId);
			ps.setInt(2, writerId);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				Transaction t = new Transaction();
				t.setId(rs.getLong("id"));
				t.setUserId(rs.getLong("user_id"));
				t.setAmount(rs.getLong("amount"));
				t.setAssignee(rs.getString("assignee"));
				t.setTimeCreated(convertToDate(rs.getTimestamp("time_created")));
				t.setTimeSettled(convertToDate(rs.getTimestamp("time_settled")));
				t.setTypeId(rs.getLong("type_id"));
				t.setStatusId(rs.getLong("status_id"));
				t.setComments(rs.getString("comments"));
				CryptoTrade ct = new CryptoTrade();
				ct.setToCurrency(rs.getString("to_currency"));
				ct.setQuantity(rs.getDouble("quantity"));
				ct.setQuantityClient(rs.getDouble("quantity_client"));
				ct.setRateMarket(rs.getDouble("rate_market"));
				ct.setRateMarketClient(rs.getDouble("rate_market_client"));
				ct.setFee(rs.getDouble("fee"));
				ct.setClearanceFee(rs.getDouble("clearance_fee"));
				ct.setRate(rs.getDouble("rate"));
				ct.setStatus(rs.getInt("status"));
				ct.setTimeDone(convertToDate(rs.getTimestamp("time_done")));
				ct.setWallet(rs.getString("wallet"));
				result.add(new CryptoTransaction(t, ct, rs.getInt("withdraw_status"), rs.getString("withdraw_id")));
			}
		}
		return result;
	}

	public static boolean updateCryptoWithdrawTransaction(Connection con, long transactionId, OrderState state) throws SQLException {
		String sql = "update crypto_withdraw set status = ?, time_updated = sysdate where transaction_id = ? and status < ?";
		try (PreparedStatement ps = con.prepareStatement(sql)) {
			ps.setInt(1, state.getId());
			ps.setLong(2, transactionId);
			ps.setInt(3, OrderState.READY_FOR_PROCESSING.getId());
			return ps.executeUpdate() == 1;
		}
	}

	public static double getBankwireFeePercentage(Connection con, long currencyId) throws SQLException {
		String sql = "select bank_wire_fee from limits where id = ?";
		try (PreparedStatement ps = con.prepareStatement(sql)) {
			ps.setLong(1, currencyId);
			ResultSet rs = ps.executeQuery();
			if (rs.next()) {
				return rs.getDouble("bank_wire_fee");
			}
		}
		return 0d;
	}

	public static double getCCFeePercentage(Connection con, long currencyId) throws SQLException {
		String sql = "select cc_fee from limits where id = ?";
		try (PreparedStatement ps = con.prepareStatement(sql)) {
			ps.setLong(1, currencyId);
			ResultSet rs = ps.executeQuery();
			if (rs.next()) {
				return rs.getDouble("cc_fee");
			}
		}
		return 0d;
	}

	public static boolean updateCryptoTransaction(Connection con, long transactionId, long amount, long amountUSD, long fee, double rate) throws SQLException {
		String sql = "update crypto_trade set original_amount = ?, amount = ?, fee = ?, rate = ? where transaction_id = ?";
		try (PreparedStatement ps = con.prepareStatement(sql)) {
			ps.setLong(1, amount);
			ps.setLong(2, amountUSD);
			ps.setLong(3, fee);
			ps.setDouble(4, rate);
			ps.setLong(5, transactionId);
			return ps.executeUpdate() == 1;
		}
	}

	public static double getCryptoTransactionRate(Connection con, long transactionId) throws SQLException {
		String sql = "select rate from crypto_trade where transaction_id = ?";
		try (PreparedStatement ps = con.prepareStatement(sql)) {
			ps.setLong(1, transactionId);
			ResultSet rs = ps.executeQuery();
			if (rs.next()) {
				return rs.getDouble("rate");
			}
		}
		return 0d;
	}

	public static String getCryptoTransactionCurrency(Connection con, long id) throws SQLException {
		String sql = "select to_currency from crypto_trade where transaction_id = ?";
		try (PreparedStatement ps = con.prepareStatement(sql)) {
			ps.setLong(1, id);
			ResultSet rs = ps.executeQuery();
			if (rs.next()) {
				return rs.getString("to_currency");
			}
		}
		return null;
	}

	public static boolean updateDepositDisabled(Connection con, boolean depositDisabled) throws SQLException {
		String sql = "update crypto_config set config_value = ? where config_key = ?";
		try (PreparedStatement ps = con.prepareStatement(sql)) {
			ps.setBoolean(1, depositDisabled);
			ps.setString(2, new String("DEPOSIT_DISABLED"));
			return ps.executeUpdate() == 1;
		}
	}
}