package com.anyoption.backend.service.cryptotransaction;

import java.util.Map;

import com.anyoption.common.service.results.MethodResult;

/**
 * @author Kiril.m
 */
public class TransactionResolutionResult extends MethodResult {

	private Map<String, TransactionResolution> resolution;

	public Map<String, TransactionResolution> getResolution() {
		return resolution;
	}

	public void setResolution(Map<String, TransactionResolution> resolution) {
		this.resolution = resolution;
	}
}