package com.anyoption.backend.service.cryptotransaction;

import com.anyoption.common.service.requests.MethodRequest;

/**
 * @author Kiril.m
 */
public class ChangeTransactionResolutionRequest extends MethodRequest {

	private long id;
	private int status;
	private String comment;
	private int rejectReasonId;
	private String rejectReason;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public int getStatus() {
		return status;
	}
	
	public void setStatus(int status) {
		this.status = status;
	}
	
	public String getComment() {
		return comment;
	}
	
	public void setComment(String comment) {
		this.comment = comment;
	}

	public int getRejectReasonId() {
		return rejectReasonId;
	}

	public void setRejectReasonId(int rejectReasonId) {
		this.rejectReasonId = rejectReasonId;
	}

	public String getRejectReason() {
		return rejectReason;
	}

	public void setRejectReason(String rejectReason) {
		this.rejectReason = rejectReason;
	}

	@Override
	public String toString() {
		String ls = System.lineSeparator();
		return ls + this.getClass().getName() + ls
				+ super.toString() + ls
				+ "id: " + id + ls
				+ "status: " + status + ls
				+ "comment: " + comment + ls
				+ "rejectReasonId: " + rejectReasonId + ls
				+ "rejectReason: " + rejectReason + ls;
	}
}