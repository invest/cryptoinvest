package com.anyoption.backend.service.cryptotransaction;

import com.anyoption.common.service.requests.MethodRequest;

/**
 * @author Kiril.m
 */
public class UpdateTransactionRequest extends MethodRequest {

	private long id;
	private long amount;
	private int currencyId;
	private long reviewDate;
	private String comments;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public long getAmount() {
		return amount;
	}

	public void setAmount(long amount) {
		this.amount = amount;
	}

	public int getCurrencyId() {
		return currencyId;
	}

	public void setCurrencyId(int currencyId) {
		this.currencyId = currencyId;
	}

	public long getReviewDate() {
		return reviewDate;
	}

	public void setReviewDate(long reviewDate) {
		this.reviewDate = reviewDate;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	@Override
	public String toString() {
		String ls = System.lineSeparator();
		return ls + this.getClass().getName() + ls
				+ super.toString() + ls
				+ "id: " + id + ls
				+ "amount: " + amount + ls 
				+ "currencyId: " + currencyId + ls
				+ "reviewDate : " + reviewDate + ls
				+ "comments: " + comments + ls;
	}
}