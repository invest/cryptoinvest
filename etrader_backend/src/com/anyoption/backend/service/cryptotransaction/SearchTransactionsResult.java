package com.anyoption.backend.service.cryptotransaction;

import java.util.List;

import com.anyoption.common.service.results.MethodResult;

/**
 * @author Kiril.m
 */
public class SearchTransactionsResult extends MethodResult {

	private List<CryptoTransaction> cryptoTransactions;
	private long userCurrencyId;

	public List<CryptoTransaction> getCryptoTransactions() {
		return cryptoTransactions;
	}

	public void setCryptoTransactions(List<CryptoTransaction> cryptoTransactions) {
		this.cryptoTransactions = cryptoTransactions;
	}

	public long getUserCurrencyId() {
		return userCurrencyId;
	}

	public void setUserCurrencyId(long userCurrencyId) {
		this.userCurrencyId = userCurrencyId;
	}
}