package com.anyoption.backend.service.cryptotransaction;

/**
 * @author Kiril.m
 */
public class TransactionResolution {

	private long transactionId;
	private String assignee;
	private int status;
	private String comment;
	private int rejectReasonId;

	public TransactionResolution(long transactionId, String assignee, int status, String comment, int rejectReasonId) {
		this.transactionId = transactionId;
		this.assignee = assignee;
		this.status = status;
		this.comment= comment;
		this.rejectReasonId = rejectReasonId;
	}

	public long getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(long transactionId) {
		this.transactionId = transactionId;
	}

	public String getAssignee() {
		return assignee;
	}

	public void setAssignee(String assignee) {
		this.assignee = assignee;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public int getRejectReasonId() {
		return rejectReasonId;
	}

	public void setRejectReasonId(int rejectReasonId) {
		this.rejectReasonId = rejectReasonId;
	}

	@Override
	public String toString() {
		String ls = System.lineSeparator();
		return ls + this.getClass().getName() + ls
				+ super.toString() + ls
				+ "transactionId: " + transactionId + ls
				+ "assignee: " + assignee + ls
				+ "status: " + status + ls
				+ "comment: " + comment + ls
				+ "rejectReasonId: " + rejectReasonId + ls;
	}
}