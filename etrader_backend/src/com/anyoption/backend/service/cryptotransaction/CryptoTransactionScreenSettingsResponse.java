package com.anyoption.backend.service.cryptotransaction;

import java.util.ArrayList;
import java.util.HashMap;

import com.anyoption.backend.service.writerpermissions.ScreenSettingsResponse;

/**
 * @author Kiril.m
 */
public class CryptoTransactionScreenSettingsResponse extends ScreenSettingsResponse {

	private HashMap<Long, ArrayList<HashMap<Long, String>>> fileRejectReasons;

	public HashMap<Long, ArrayList<HashMap<Long, String>>> getFileRejectReasons() {
		return fileRejectReasons;
	}

	public void setFileRejectReasons(HashMap<Long, ArrayList<HashMap<Long, String>>> fileRejectReasons) {
		this.fileRejectReasons = fileRejectReasons;
	}
}