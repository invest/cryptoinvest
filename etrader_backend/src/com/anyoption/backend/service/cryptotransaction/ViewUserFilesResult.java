package com.anyoption.backend.service.cryptotransaction;

import java.util.List;

import com.anyoption.common.beans.File;
import com.anyoption.common.service.results.MethodResult;

/**
 * @author Kiril.m
 */
public class ViewUserFilesResult extends MethodResult {

	private List<File> files;

	public List<File> getFiles() {
		return files;
	}

	public void setFiles(List<File> files) {
		this.files = files;
	}
}