package com.anyoption.backend.service.cryptotransaction;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.Transaction;
import com.anyoption.common.managers.CurrenciesManagerBase;
import com.anyoption.common.managers.TransactionsManagerBase;
import com.invest.common.email.beans.EmailTemplate;
import com.invest.common.email.factories.EmailProviderFactory;
import com.invest.common.email.factories.EmailTemplateFactory;
import com.invest.common.enums.OrderState;
import com.invest.common.service.cryptotransaction.CryptoTransactionsManagerBase;

import il.co.etrader.backend.util.Utils;

/**
 * @author Kiril.m
 */
public class CryptoTransactionsManager extends CryptoTransactionsManagerBase {

	private static final Logger log = Logger.getLogger(CryptoTransactionsManager.class);
	public static final int RESOLUTION_STATUS_PENDING = 1;
	public static final int RESOLUTION_STATUS_APPROVED = 2;
	public static final int RESOLUTION_STATUS_REJECTED = 3;

	public static Transaction getTransaction(long id, long userId) {
		Connection con = null;
		try {
			con = getConnection();
			return CryptoTransactionsDAO.getTransaction(con, id, userId);
		} catch (SQLException e) {
			log.debug("Unable to load transaction with id " + id + ", returning null", e);
			return null;
		} finally {
			closeConnection(con);
		}
	}

	public static boolean updateTransaction(long id, long amount, long amountUSD, long fee, double rate, String comments, long reviewDate,
											long writerId, String assignee) {
		Connection con = null;
		try {
			con = getConnection();
			con.setAutoCommit(false);
			boolean res = false;
			if (CryptoTransactionsDAO.updateTransaction(con, id, amount, comments, reviewDate, writerId, assignee)) {
				res = CryptoTransactionsDAO.updateCryptoTransaction(con, id, amount, amountUSD, fee, rate);
			}
			if (res) {
				con.commit();
			} else {
				con.rollback();
			}
			return res;
		} catch (SQLException e) {
			log.debug("Unable to update transaction with id " + id + ", returning false", e);
			try {
				con.rollback();
			} catch (SQLException e1) {
				log.debug("Cannot rollback connection", e1);
			}
			return false;
		} finally {
			try {
				con.setAutoCommit(true);
			} catch (SQLException e) {
				log.debug("Cannot return connection back to autocommit", e);
			}
			closeConnection(con);
		}
	}

	public static boolean updateResolution(	long id, String currentAssignee, String newAssignee, int status, String comment, long writerId,
											int transactionStatus, int rejectReasonId, String rejectReason, long userId, String userEmail,
											String name, long transactionTypeId, long amount, long currencyId) {
		if (status == RESOLUTION_STATUS_PENDING) {
			return updatePendingTransaction(id, currentAssignee, status, 0, comment, writerId);
		} else if (status == RESOLUTION_STATUS_APPROVED) {
			return approveTransaction(	id, currentAssignee, newAssignee, status, comment, writerId, transactionStatus, userId, userEmail,
										name, transactionTypeId, amount, currencyId);
		} else if (status == RESOLUTION_STATUS_REJECTED) {
			return rejectTransaction(	id, currentAssignee, status, rejectReasonId, rejectReason, comment, writerId, userId, userEmail, name,
										transactionTypeId);
		}
		return false;
	}

	public static boolean approveTransaction(	long id, String currentAssignee, String newAssignee, int status, String comment, long writerId,
												int transactionStatus, long userId, String userEmail, String name, long transactionTypeId,
												long amount, long currencyId) {
		Connection con = null;
		try {
			con = getConnection();
			con.setAutoCommit(false);
			boolean res = false;
			if (CryptoTransactionsDAO.changeTransactionAssignee(con, id, newAssignee, writerId)) {
				res = CryptoTransactionsDAO.updateAssigneeResolution(con, id, currentAssignee, status, 0, comment, writerId);
				if ((transactionStatus > 0) && res) {
					res = CryptoTransactionsDAO.updateTransactionStatus(con, id, writerId, transactionStatus);
					if (transactionStatus == TransactionsManagerBase.TRANS_STATUS_SUCCEED) {
						res = updateCryptoTransactionState(con, id, OrderState.READY_FOR_PROCESSING);
						if (res) {
							String cryptoCurrency = getCryptoTransactionCurrency(con, id);
							EmailTemplate template = null;
							if (transactionTypeId == TransactionsManagerBase.TRANS_TYPE_WIRE_DEPOSIT) {
								template = EmailTemplateFactory.getWireTransactionApprovedTemplate(	userId, Utils.getConfig("email.from"),
																									userEmail, null, name, cryptoCurrency,
																									id,
																									Utils.getConfig("email.template.domain"),
																									cryptoCurrency,
																									Utils.getConfig("email.template.homepage"),
																									Utils.getConfig("email.support"));
							} else if (transactionTypeId == TransactionsManagerBase.TRANS_TYPE_CARDPAY_DEPOSIT) {
								template = EmailTemplateFactory.getCCTransactionApprovedTemplate(	userId, Utils.getConfig("email.from"),
																									userEmail, null, name, cryptoCurrency,
																									id,
																									Utils.getConfig("email.template.domain"),
																									amount / 100,
																									CurrenciesManagerBase	.getCurrency(currencyId)
																															.getDefaultSymbol(),
																									cryptoCurrency,
																									Utils.getConfig("email.template.homepage"),
																									Utils.getConfig("email.support"));
							}
							if (template != null) {
								EmailProviderFactory.getEmailProvider().scheduleTemplateEmail(template);
							}
						}
					}
				}
			}
			if (res) {
				con.commit();
			} else {
				con.rollback();
			}
			return res;
		} catch (SQLException e) {
			log.debug("Unable to change assignee for transaction " + id, e);
			try {
				con.rollback();
			} catch (SQLException e1) {
				log.debug("Cannot rollback connection", e1);
			}
			return false;
		} finally {
			try {
				con.setAutoCommit(true);
			} catch (SQLException e) {
				log.debug("Cannot return connection back to autocommit", e);
			}
			closeConnection(con);
		}
	}

	private static String getCryptoTransactionCurrency(Connection con, long id) throws SQLException {
		return CryptoTransactionsDAO.getCryptoTransactionCurrency(con, id);
	}

	public static Map<String, TransactionResolution> getTransactionResolution(long transactionId) {
		Connection con = null;
		try {
			con = getConnection();
			return CryptoTransactionsDAO.getTransactionResolution(con, transactionId);
		} catch (SQLException e) {
			log.debug("Unable to load resolution for transaction " + transactionId + ", returning null", e);
			return null;
		} finally {
			closeConnection(con);
		}
	}

	public static boolean rejectTransaction(long id, String assignee, int status, int rejectReasonId, String rejectReason, String comment,
											long writerId, long userId, String userEmail, String name, long transactionTypeId) {
		Connection con = null;
		try {
			con = getConnection();
			con.setAutoCommit(false);
			boolean res = false;
			if (CryptoTransactionsDAO.updateTransactionStatus(con, id, writerId, TransactionsManagerBase.TRANS_STATUS_FAILED)) {
				res = CryptoTransactionsDAO.updateAssigneeResolution(con, id, assignee, status, rejectReasonId, comment, writerId);
				EmailTemplate template = null;
				if (transactionTypeId == TransactionsManagerBase.TRANS_TYPE_WIRE_DEPOSIT) {
					template = EmailTemplateFactory.getWireTransactionFailedTemplate(	userId, Utils.getConfig("email.from"), userEmail, null,
																						name, id, Utils.getConfig("email.template.domain"),
																						rejectReason,
																						Utils.getConfig("email.template.homepage"),
																						Utils.getConfig("email.support"));
				} else if (transactionTypeId == TransactionsManagerBase.TRANS_TYPE_CARDPAY_DEPOSIT) {
					String cryptoCurrency = getCryptoTransactionCurrency(con, id);
					template = EmailTemplateFactory.getCCTransactionFailedTemplate(	userId, Utils.getConfig("email.from"), userEmail, null,
																					name, cryptoCurrency, id,
																					Utils.getConfig("email.template.domain"), rejectReason,
																					Utils.getConfig("email.template.homepage"),
																					Utils.getConfig("email.support"));
				}
				if (template != null) {
					EmailProviderFactory.getEmailProvider().scheduleTemplateEmail(template);
				}
			}
			if (res) {
				con.commit();
			} else {
				con.rollback();
			}
			return res;
		} catch (SQLException e) {
			log.debug("Unable to change reject transaction " + id, e);
			try {
				con.rollback();
			} catch (SQLException e1) {
				log.debug("Cannot rollback connection", e1);
			}
			return false;
		} finally {
			try {
				con.setAutoCommit(true);
			} catch (SQLException e) {
				log.debug("Cannot return connection back to autocommit", e);
			}
			closeConnection(con);
		}
	}

	public static boolean updatePendingTransaction(	long transactionId, String assignee, int status, int rejectReasonId, String comment,
													long writerId) {
		Connection con = null;
		try {
			con = getConnection();
			return CryptoTransactionsDAO.updateAssigneeResolution(con, transactionId, assignee, status, 0, comment, writerId);
		} catch (SQLException e) {
			log.debug("Unable to update pending transaction", e);
			return false;
		} finally {
			closeConnection(con);
		}
	}

	public static List<CryptoTransaction> searchTransactions(long userId, int writerId) {
		Connection con = null;
		try {
			con = getConnection();
			return CryptoTransactionsDAO.searchTransactions(con, userId, writerId);
		} catch (SQLException e) {
			log.debug("Unable to load transactons, returning null", e);
			return null;
		} finally {
			closeConnection(con);
		}
	}

	public static boolean updateCryptoWithdrawTransaction(long transactionId, long userId, OrderState state) {
		Connection con = null;
		try {
			con = getConnection();
			Transaction transaction = getTransaction(transactionId, userId);
			if (transaction != null && transaction.getStatusId() == TransactionsManagerBase.TRANS_STATUS_SUCCEED) {
				return CryptoTransactionsDAO.updateCryptoWithdrawTransaction(con, transactionId, state);
			}
			return false;
		} catch (SQLException e) {
			log.debug("Unable to update crypto withdraw transaction with transaction id " + transactionId);
			return false;
		} finally {
			closeConnection(con);
		}
	}

	public static double getFeePercentage(long currencyId, long transactionTypeId) {
		if (transactionTypeId == TransactionsManagerBase.TRANS_TYPE_WIRE_DEPOSIT) {
			return getBankwireFeePercentage(currencyId);
		} else if (transactionTypeId == TransactionsManagerBase.TRANS_TYPE_CARDPAY_DEPOSIT) {
			return getCCFeePercentage(currencyId);
		}
		return 0d;
	}

	private static double getBankwireFeePercentage(long currencyId) {
		Connection con = null;
		try {
			con = getConnection();
			return CryptoTransactionsDAO.getBankwireFeePercentage(con, currencyId);
		} catch (SQLException e) {
			log.debug("Unable to load bankwire fee percentage", e);
			return 0d;
		} finally {
			closeConnection(con);
		}
	}

	private static double getCCFeePercentage(long currencyId) {
		Connection con = null;
		try {
			con = getConnection();
			return CryptoTransactionsDAO.getCCFeePercentage(con, currencyId);
		} catch (SQLException e) {
			log.debug("Unable to load bankwire fee percentage", e);
			return 0d;
		} finally {
			closeConnection(con);
		}
	}

	public static double getCryptoTransactionRate(long transactionId) {
		Connection con = null;
		try {
			con = getConnection();
			return CryptoTransactionsDAO.getCryptoTransactionRate(con, transactionId);
		} catch (SQLException e) {
			log.debug("Unable to get crypto transaction rate for transaction" + transactionId, e);
			return 0d;
		} finally {
			closeConnection(con);
		}
	}

	public static boolean updateDepositDisabled(boolean depositDisabled) {
		Connection con = null;
		try {
			con = getConnection();
			return CryptoTransactionsDAO.updateDepositDisabled(con, depositDisabled);
		} catch (SQLException e) {
			log.debug("Unable to update deposit disabled", e);
			return false;
		} finally {
			closeConnection(con);
		}
	}
}