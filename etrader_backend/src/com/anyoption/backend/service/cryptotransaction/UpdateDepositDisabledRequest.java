package com.anyoption.backend.service.cryptotransaction;

import com.anyoption.common.service.requests.MethodRequest;

/**
 * @author Kiril.m
 */
public class UpdateDepositDisabledRequest extends MethodRequest {

	private boolean depositDisabled;

	public boolean isDepositDisabled() {
		return depositDisabled;
	}

	public void setDepositDisabled(boolean depositDisabled) {
		this.depositDisabled = depositDisabled;
	}

	@Override
	public String toString() {
		String ls = System.lineSeparator();
		return ls + this.getClass().getName() + ls
				+ super.toString() + ls
				+ "depositDisabled: " + depositDisabled + ls;
	}
}