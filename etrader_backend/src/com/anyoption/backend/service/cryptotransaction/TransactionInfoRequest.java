package com.anyoption.backend.service.cryptotransaction;

import com.anyoption.common.service.requests.MethodRequest;

/**
 * @author Kiril.m
 */
public class TransactionInfoRequest extends MethodRequest {

	private long id;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	@Override
	public String toString() {
		String ls = System.lineSeparator();
		return ls + this.getClass().getName() + ls
				+ super.toString() + ls
				+ "id: " + id + ls;
	}
}