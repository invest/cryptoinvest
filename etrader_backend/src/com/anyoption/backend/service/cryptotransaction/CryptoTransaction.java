package com.anyoption.backend.service.cryptotransaction;

import com.anyoption.common.beans.Transaction;
import com.invest.common.beans.CryptoTrade;

/**
 * @author Kiril.m
 */
public class CryptoTransaction {

	private Transaction transaction;
	private CryptoTrade cryptoTrade;
	private int withdrawStatus;
	private String cryptoWithdrawId;

	public CryptoTransaction(Transaction transaction, CryptoTrade cryptoTrade, int withdrawStatus, String cryptoWithdrawId) {
		this.transaction = transaction;
		this.cryptoTrade = cryptoTrade;
		this.withdrawStatus = withdrawStatus;
		this.cryptoWithdrawId = cryptoWithdrawId;
	}

	public Transaction getTransaction() {
		return transaction;
	}

	public void setTransaction(Transaction transaction) {
		this.transaction = transaction;
	}

	public CryptoTrade getCryptoTrade() {
		return cryptoTrade;
	}

	public void setCryptoTrade(CryptoTrade cryptoTrade) {
		this.cryptoTrade = cryptoTrade;
	}

	public int getWithdrawStatus() {
		return withdrawStatus;
	}

	public void setWithdrawStatus(int withdrawStatus) {
		this.withdrawStatus = withdrawStatus;
	}

	public String getCryptoWithdrawId() {
		return cryptoWithdrawId;
	}

	public void setCryptoWithdrawId(String cryptoWithdrawId) {
		this.cryptoWithdrawId = cryptoWithdrawId;
	}
}