package com.anyoption.backend.service.cryptotransaction;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import com.anyoption.backend.managers.WriterPermisionsManager;
import com.anyoption.common.backend.annotations.BackendPermission;
import com.anyoption.common.beans.File;
import com.anyoption.common.beans.FileType;
import com.anyoption.common.beans.Transaction;
import com.anyoption.common.beans.Writer;
import com.anyoption.common.managers.CurrenciesManagerBase;
import com.anyoption.common.service.CommonJSONService;
import com.anyoption.common.service.requests.MethodRequest;
import com.anyoption.common.service.results.MethodResult;
import com.invest.common.enums.OrderState;
import com.invest.common.service.cryptotransaction.CryptoTransactionsManagerBase;

import il.co.etrader.backend.bl_managers.FilesManager;
import il.co.etrader.backend.bl_vos.User;
import il.co.etrader.backend.bl_vos.WriterWrapper;
import il.co.etrader.bl_managers.TransactionsManagerBase;
import il.co.etrader.util.CommonUtil;
import il.co.etrader.util.ConstantsBase;

/**
 * @author Kiril.m
 */
public class CryptoTransactionServices {

	private static final Logger log = Logger.getLogger(CryptoTransactionServices.class);
	private static final String SCREEN_NAME_CRYPTO_TRANSACTIONS = "cryptoTransactions";
	private static final int RESPONSE_CODE_INCORRECT_ASSIGNEE = 12001;
	private static final int RESPONSE_CODE_FAIL_TO_LOAD = 12002;
	private static final int RESPONSE_CODE_INCORRECT_STATUS = 12003;

	public static CryptoTransactionScreenSettingsResponse getScreenSettings(MethodRequest request, HttpServletRequest httpRequest) {
		log.info("getCryptoTransactionsScreenSettings: " + request);
		CryptoTransactionScreenSettingsResponse response = new CryptoTransactionScreenSettingsResponse();
		HashMap<Long, ArrayList<HashMap<Long, String>>> rejectReasons = new HashMap<>();
		try {
			rejectReasons.put(FileType.BANK_WIRE_PROTOCOL_FILE, FilesManager.getFileRejectReasons().get(FileType.BANK_WIRE_PROTOCOL_FILE));
			rejectReasons.put(FileType.CC_COPY, FilesManager.getFileRejectReasons().get(FileType.BANK_WIRE_PROTOCOL_FILE));
			response.setFileRejectReasons(rejectReasons);
		} catch (SQLException e) {
			log.debug("Unable to load reject reasons", e);
		}
		WriterPermisionsManager.setWriterScreenPermissionsByScreenName(SCREEN_NAME_CRYPTO_TRANSACTIONS, response, httpRequest);
		response.setCurrencies(CurrenciesManagerBase.getCurrencies().values());
		response.setDepositDisabled(CryptoTransactionsManagerBase.isDepositDisabled());
		return response;
	}

	@BackendPermission(id = "cryptoTransactions_view")
	public static SearchTransactionsResult searchTransactions(MethodRequest request, HttpServletRequest httpRequest) {
		log.info("searchTransactions: " + request);
		SearchTransactionsResult result = new SearchTransactionsResult();
		User user = (User) httpRequest.getSession().getAttribute(ConstantsBase.BIND_SESSION_USER);
		List<CryptoTransaction> transactions = CryptoTransactionsManager.searchTransactions(user.getId(), Writer.WRITER_ID_WEB);
		if (transactions == null) {
			result.setResponseCode(RESPONSE_CODE_FAIL_TO_LOAD);
			return result;
		}
		result.setCryptoTransactions(transactions);
		result.setUserCurrencyId(user.getCurrencyId());
		return result;
	}

	@BackendPermission(id = "cryptoTransactions_finance")
	public static UpdateTransactionResult updateTransaction(UpdateTransactionRequest request, HttpServletRequest httpRequest) {
		log.info("updateTransaction: " + request);
		UpdateTransactionResult result = new UpdateTransactionResult();
		WriterWrapper writer = (WriterWrapper) httpRequest.getSession().getAttribute(ConstantsBase.BIND_SESSION_WRITER_WRAPPER);
		User user = (User) httpRequest.getSession().getAttribute(ConstantsBase.BIND_SESSION_USER);
		List<File> userFiles = FilesManager.getFiles(request.getId());
		String assignee = null;
		if (userFiles != null) {
			assignee = CryptoTransactionsManager.ASSIGNEE_FINANCE;
		}
		Transaction transaction = CryptoTransactionsManager.getTransaction(request.getId(), user.getId()); // create new get by id
		if (transaction == null) {
			result.setResponseCode(CommonJSONService.ERROR_CODE_INVALID_INPUT);
			return result;
		}
		long amountNew = transaction.getAmount();
		String comments = request.getComments();
		long amountUSDRnd = amountNew;
		double precision = Math.pow(10, ConstantsBase.RATE_PRECISISON);
		double feePercentage = CryptoTransactionsManager.getFeePercentage(user.getCurrencyId(), transaction.getTypeId()) / 100d;
		double rate = CryptoTransactionsManager.getCryptoTransactionRate(transaction.getId());
		Date date = new Date();
		if (user.getCurrencyId() != request.getCurrencyId()) {
			Double amountUSD = CommonUtil.convertToBaseAmount(request.getAmount(), request.getCurrencyId(), date);
			amountNew = new Double(Math.round(amountUSD / CommonUtil.convertToBaseAmount(1L, user.getCurrencyId(), date) * precision)
									/ precision).longValue();
			rate = Math.round(amountUSD / request.getAmount() * precision) / precision;
			amountUSDRnd = Math.round(amountUSD);
			comments += " | "	+ request.getAmount() + " in " + request.getCurrencyId() + ", converted to " + amountNew + " in "
						+ user.getCurrencyId();
		} else if (transaction.getAmount() != request.getAmount()) {
			amountNew = request.getAmount();
			amountUSDRnd = request.getAmount();
		}
		comments += " | ";
		long fee = new Double(Math.round(amountNew * feePercentage * precision) / precision).longValue();
		if (!CryptoTransactionsManager.updateTransaction(	request.getId(), amountNew, amountUSDRnd, fee, rate, comments,
															request.getReviewDate(), writer.getWriter().getId(), assignee)) {
			result.setResponseCode(CommonJSONService.ERROR_CODE_FAIL_TO_UPDATE);
			return result;
		}
		result.setAmount(amountNew);
		result.setComments(comments);
		result.setAssignee(assignee);
		result.setFee(fee);
		return result;
	}

	@BackendPermission(id = "cryptoTransactions_view")
	public static TransactionResolutionResult getTransactionResolution(TransactionInfoRequest request, HttpServletRequest httpRequest) {
		log.info("getTransactionResolution: " + request);
		TransactionResolutionResult result = new TransactionResolutionResult();
		Map<String, TransactionResolution> resolution = CryptoTransactionsManager.getTransactionResolution(request.getId());
		if (resolution == null) {
			result.setResponseCode(RESPONSE_CODE_FAIL_TO_LOAD);
			return result;
		}
		result.setResolution(resolution);
		return result;
	}

	@BackendPermission(id = "cryptoTransactions_finance")
	public static MethodResult changeResolutionByFinance(ChangeTransactionResolutionRequest request, HttpServletRequest httpRequest) {
		log.info("changeResolutionByFinance: " + request);
		return changeResolution(request, httpRequest, CryptoTransactionsManager.ASSIGNEE_FINANCE,
								CryptoTransactionsManager.ASSIGNEE_SUPPORT, 0);
	}

	@BackendPermission(id = "cryptoTransactions_support")
	public static MethodResult changeResolutionBySupport(ChangeTransactionResolutionRequest request, HttpServletRequest httpRequest) {
		log.info("changeResolutionBySupport: " + request);
		return changeResolution(request, httpRequest, CryptoTransactionsManager.ASSIGNEE_SUPPORT, CryptoTransactionsManager.ASSIGNEE_RISK,
								0);
	}

	@BackendPermission(id = "cryptoTransactions_risk")
	public static MethodResult changeResolutionByRisk(ChangeTransactionResolutionRequest request, HttpServletRequest httpRequest) {
		log.info("changeResolutionByRisk: " + request);
		return changeResolution(request, httpRequest, CryptoTransactionsManager.ASSIGNEE_RISK, CryptoTransactionsManager.ASSIGNEE_RISK,
								TransactionsManagerBase.TRANS_STATUS_SUCCEED);
	}

	private static MethodResult changeResolution(	ChangeTransactionResolutionRequest request, HttpServletRequest httpRequest,
													String currentAssignee, String newAssignee, int transactionStatus) {
		MethodResult result = new MethodResult();
		WriterWrapper writer = (WriterWrapper) httpRequest.getSession().getAttribute(ConstantsBase.BIND_SESSION_WRITER_WRAPPER);
		User user = (User) httpRequest.getSession().getAttribute(ConstantsBase.BIND_SESSION_USER);
		Transaction transaction = CryptoTransactionsManager.getTransaction(request.getId(), user.getId());
		if (transaction == null) {
			result.setResponseCode(CommonJSONService.ERROR_CODE_INVALID_INPUT);
			return result;
		}
		if (!transaction.getAssignee().equals(currentAssignee)) {
			result.setResponseCode(RESPONSE_CODE_INCORRECT_ASSIGNEE);
			return result;
		}
		if (transaction.getStatusId() == TransactionsManagerBase.TRANS_STATUS_STARTED) {
			result.setResponseCode(RESPONSE_CODE_INCORRECT_STATUS);
			return result;
		}
		if (!CryptoTransactionsManager.updateResolution(request.getId(), currentAssignee, newAssignee, request.getStatus(),
														request.getComment(), writer.getWriter().getId(), transactionStatus,
														request.getRejectReasonId(), request.getRejectReason(), user.getId(),
														user.getEmail(), user.getFirstName(), transaction.getTypeId(),
														transaction.getAmount(), user.getCurrencyId())) {
			result.setResponseCode(CommonJSONService.ERROR_CODE_FAIL_TO_UPDATE);
			return result;
		}
		return result;
	}

	@BackendPermission(id = "cryptoTransactions_view")
	public static ViewUserFilesResult viewUserFiles(TransactionInfoRequest request, HttpServletRequest httpRequest) {
		log.info("viewUserFiles: " + request);
		ViewUserFilesResult result = new ViewUserFilesResult();
		result.setFiles(FilesManager.getFiles(request.getId()));
		return result;
	}

	@BackendPermission(id = "cryptoTransactions_withdraw")
	public static MethodResult withdrawToUserWallet(TransactionInfoRequest request, HttpServletRequest httpRequest) {
		log.info("withdrawToUserWallet: " + request);
		MethodResult result = new MethodResult();
		User user = (User) httpRequest.getSession().getAttribute(ConstantsBase.BIND_SESSION_USER);
		if (!CryptoTransactionsManager.updateCryptoWithdrawTransaction(request.getId(), user.getId(), OrderState.READY_FOR_PROCESSING)) {
			result.setResponseCode(CommonJSONService.ERROR_CODE_FAIL_TO_UPDATE);
			return result;
		}
		return result;
	}

	@BackendPermission(id = "cryptoTransactions_disableDeposit")
	public static MethodResult updateDepositDisabled(UpdateDepositDisabledRequest request) {
		log.info("updateDepositDisabled: " + request);
		MethodResult result = new MethodResult();
		if (!CryptoTransactionsManager.updateDepositDisabled(request.isDepositDisabled())) {
			result.setResponseCode(CommonJSONService.ERROR_CODE_FAIL_TO_UPDATE);
		}
		return result;
	}
}