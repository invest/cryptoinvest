package com.anyoption.backend.service.cryptotransaction;

import com.anyoption.common.service.results.MethodResult;

/**
 * @author Kiril.m
 */
public class UpdateTransactionResult extends MethodResult {

	private long amount;
	private String comments;
	private String assignee;
	private long fee;
	private long clearanceFee;

	public long getAmount() {
		return amount;
	}

	public void setAmount(long amount) {
		this.amount = amount;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public String getAssignee() {
		return assignee;
	}

	public void setAssignee(String assignee) {
		this.assignee = assignee;
	}

	public long getFee() {
		return fee;
	}

	public void setFee(long fee) {
		this.fee = fee;
	}

	public long getClearanceFee() {
		return clearanceFee;
	}

	public void setClearanceFee(long clearanceFee) {
		this.clearanceFee = clearanceFee;
	}
}