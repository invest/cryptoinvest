package com.anyoption.backend.service.updatetransaction;

import com.anyoption.common.service.requests.MethodRequest;

public class UpdateTransactionMethodRequest extends MethodRequest {
	
	private Long transactionId;
	private Long typeId;
	private Long statusId;
	private Long userId;
	
	public Long getTransactionId() {
		return transactionId;
	}
	
	public void setTransactionId(Long transactionId) {
		this.transactionId = transactionId;
	}
	
	public Long getTypeId() {
		return typeId;
	}
	
	public void setTypeId(Long typeId) {
		this.typeId = typeId;
	}
	
	public Long getStatusId() {
		return statusId;
	}
	
	public void setStatusId(Long statusId) {
		this.statusId = statusId;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}
}
