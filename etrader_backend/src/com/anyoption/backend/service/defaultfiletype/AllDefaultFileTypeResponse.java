package com.anyoption.backend.service.defaultfiletype;

import java.util.ArrayList;

import com.anyoption.common.service.results.MethodResult;

public class AllDefaultFileTypeResponse extends MethodResult {
	
	ArrayList<AllDefaultFileType> filesList;
	long totalCount;

	public ArrayList<AllDefaultFileType> getFilesList() {
		return filesList;
	}

	public void setFilesList(ArrayList<AllDefaultFileType> filesList) {
		this.filesList = filesList;
	}

	public long getTotalCount() {
		return totalCount;
	}

	public void setTotalCount(long totalCount) {
		this.totalCount = totalCount;
	}
}
