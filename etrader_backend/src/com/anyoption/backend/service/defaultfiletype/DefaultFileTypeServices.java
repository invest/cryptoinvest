package com.anyoption.backend.service.defaultfiletype;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import com.anyoption.backend.managers.TransactionManager;
import com.anyoption.backend.managers.WriterPermisionsManager;
import com.anyoption.backend.service.writerpermissions.ScreenSettingsResponse;
import com.anyoption.common.backend.annotations.BackendPermission;
import com.anyoption.common.managers.CountryManagerBase;
import com.anyoption.common.service.CommonJSONService;
import com.anyoption.common.service.requests.UserMethodRequest;

import il.co.etrader.backend.bl_vos.WriterWrapper;
import il.co.etrader.bl_managers.SkinsManagerBase;
import il.co.etrader.util.ConstantsBase;

public class DefaultFileTypeServices {
	
	private static final Logger logger = Logger.getLogger(DefaultFileTypeServices.class);
	private static final String SCREEN_NAME_FROM_INBOX = "fromInbox";
	
	@BackendPermission(id = "fromInbox_view")
	public static ScreenSettingsResponse getDefaultFileTypeScreenSettings(UserMethodRequest request,
																		  HttpServletRequest httpRequest) {
		logger.debug("getDefaultFileTypeScreenSettings: " + request);
		HashMap<String, HashMap<Long, String>> filters = new HashMap<>();
		ScreenSettingsResponse response = new ScreenSettingsResponse();
		WriterWrapper writer = (WriterWrapper) httpRequest.getSession()
															.getAttribute(ConstantsBase.BIND_SESSION_WRITER_WRAPPER);
		
		WriterPermisionsManager.setWriterScreenPermissionsByScreenName(SCREEN_NAME_FROM_INBOX, response, httpRequest);
		
		//skins
		HashMap<Long, String> skins = WriterPermisionsManager.filterSkinsBasedOnWriterAttribution(
																	SkinsManagerBase.getAllSkinsFilter(), writer, logger);
		filters.put(SkinsManagerBase.FILTER_SKINS, skins);
		
		//countries
		HashMap<Long, String> countries = CountryManagerBase.getAllCountriesFilter();
		filters.put(CountryManagerBase.FILTER_COUNTRIES, countries);
		
		//Users
		HashMap<Long, String> usersClass = TransactionManager.getUserClasses();
		if (usersClass.containsKey(3L)) {	//API
			usersClass.remove(3L);
		}
		filters.put(TransactionManager.FILTER_CLASS_USERS, usersClass);
			
		response.setScreenFilters(filters);
		return response;
	}
	
	@BackendPermission(id = "fromInbox_view")
	public static AllDefaultFileTypeResponse getAllDefaultFiles(AllDefaultFileTypeRequest request,
																HttpServletRequest httpRequest) {
		logger.debug("getAllDefaultFiles: " + request);
		AllDefaultFileTypeResponse response = new AllDefaultFileTypeResponse();
		try {
			WriterWrapper writerWrapper = (WriterWrapper) httpRequest.getSession().
																getAttribute(ConstantsBase.BIND_SESSION_WRITER_WRAPPER);
			ArrayList<Integer> skinsList = request.getSkins();
			skinsList = WriterPermisionsManager.setPermitedSkinsForWriter(skinsList, writerWrapper);
			request.setSkins(skinsList);
			AutomaticMailProcessManager.getAllDefaultTypeFiles(request, response);
		} catch (SQLException e) {
			logger.error("Unable to get default files list! ", e);
			response.setErrorCode(CommonJSONService.ERROR_CODE_UNKNOWN);
		}
		return response;
	}

}
