package com.anyoption.backend.service.defaultfiletype;

import java.util.ArrayList;

import com.anyoption.common.service.requests.UserMethodRequest;

public class AllDefaultFileTypeRequest extends UserMethodRequest {
	
	private Long lastMatchFrom;
	private Long lastMatchTo;
	private Long userId;
	private String userName;
	private Long userClasses;
	private ArrayList<Integer> skins;
	private ArrayList<Integer> countries;
	private Long page;
	private long rowsPerPage;
	
	public Long getLastMatchFrom() {
		return lastMatchFrom;
	}
	
	public void setLastMatchFrom(Long lastMatchFrom) {
		this.lastMatchFrom = lastMatchFrom;
	}
	
	public Long getLastMatchTo() {
		return lastMatchTo;
	}
	
	public void setLastMatchTo(Long lastMatchTo) {
		this.lastMatchTo = lastMatchTo;
	}
	
	public Long getUserId() {
		return userId;
	}
	
	public void setUserId(Long userId) {
		this.userId = userId;
	}
	
	public String getUserName() {
		return userName;
	}
	
	public void setUserName(String userName) {
		this.userName = userName;
	}
	
	public Long getUserClasses() {
		return userClasses;
	}
	
	public void setUserClasses(Long userClasses) {
		this.userClasses = userClasses;
	}
	
	public ArrayList<Integer> getSkins() {
		return skins;
	}
	
	public void setSkins(ArrayList<Integer> skins) {
		this.skins = skins;
	}
	
	public ArrayList<Integer> getCountries() {
		return countries;
	}
	
	public void setCountries(ArrayList<Integer> countries) {
		this.countries = countries;
	}
	
	public Long getPage() {
		return page;
	}
	
	public void setPage(Long page) {
		this.page = page;
	}
	
	public long getRowsPerPage() {
		return rowsPerPage;
	}
	
	public void setRowsPerPage(long rowsPerPage) {
		this.rowsPerPage = rowsPerPage;
	}

	public String toString() {
        String ls = System.getProperty("line.separator");
        return ls + "AllDefaultFileTypeRequest: "
        + "lastMatchFrom: " + lastMatchFrom + ls
        + "lastMatchTo: " + lastMatchTo + ls
        + "userId: " + userId + ls
        + "userName: " + userName + ls
        + "userClasses: " + userClasses + ls
        + "skins: " + skins + ls
        + "countries: " + countries + ls
        + "page: " + page + ls
        + "rowsPerPage: " + rowsPerPage + ls;
	}
}
