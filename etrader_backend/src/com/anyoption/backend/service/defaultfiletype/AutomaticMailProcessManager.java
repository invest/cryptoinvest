package com.anyoption.backend.service.defaultfiletype;

import java.sql.Connection;
import java.sql.SQLException;

import com.anyoption.common.managers.BaseBLManager;

public class AutomaticMailProcessManager extends BaseBLManager {
	
	public static void getAllDefaultTypeFiles(AllDefaultFileTypeRequest request,
											AllDefaultFileTypeResponse response) throws SQLException {
		Connection con = null;
		try {
			con = getConnection();
			AutomaticMailProcessDAO.getAllDefaultTypeFiles(con, request, response);
		} finally {
			closeConnection(con);
		}
	}

}
