package com.anyoption.backend.service.defaultfiletype;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;

import org.apache.log4j.Logger;

import com.anyoption.common.daos.DAOBase;

import oracle.jdbc.OracleTypes;

public class AutomaticMailProcessDAO extends DAOBase {
	
	private static final Logger log = Logger.getLogger(AutomaticMailProcessDAO.class);
	
	public static void getAllDefaultTypeFiles(Connection con, AllDefaultFileTypeRequest request,
											AllDefaultFileTypeResponse response) throws SQLException {
		log.debug("Trying to fetch all users with default type files. ");
		ArrayList<AllDefaultFileType> filesList = new ArrayList<AllDefaultFileType>();
		CallableStatement cstmt = null;
		ResultSet rs = null;
		int index = 1;
		try {
			cstmt = con.prepareCall("{call PKG_USER_DOCUMENTS.GET_DEFAULT_FILES( O_DEFAULT_FILES => ? " +
																				 " ,I_MATCH_FROM => ? " +
																				 " ,I_MATCH_TO => ? " +
																				 " ,I_USER_ID => ? " +
																				 " ,I_USER_NAME => ? " +
																				 " ,I_USER_CLASS => ? " +
																				 " ,I_SKINS => ? " +
																				 " ,I_COUNTRIES => ? " +
																				 " ,I_PAGE => ? " +
																			     " ,I_ROWS_PER_PAGE => ? )}");
			
		    cstmt.registerOutParameter(index++, OracleTypes.CURSOR);
			setStatementValue(new Timestamp(request.getLastMatchFrom() == null ? 0 : request.getLastMatchFrom() + 1), index++, cstmt);
			setStatementValue(new Timestamp(request.getLastMatchTo() == null ? 0 : request.getLastMatchTo() + 1), index++, cstmt);
			setStatementValue(request.getUserId(), index++, cstmt);
			setStatementValue(request.getUserName(), index++, cstmt);
			setStatementValue(request.getUserClasses(), index++, cstmt);
			setArrayVal(request.getSkins(), index++, cstmt, con);
			setArrayVal(request.getCountries(), index++, cstmt, con);
			setStatementValue(request.getPage(), index++, cstmt);
			setStatementValue(request.getRowsPerPage(), index++, cstmt);
			
			cstmt.execute();
			
			rs = (ResultSet) cstmt.getObject(1);
			long count = 0;
			while (rs.next()) {
				if (count == 0) {
					response.setTotalCount(rs.getLong("total_count"));
					count++;
				}
				AllDefaultFileType file = new AllDefaultFileType();
				file.setUserId(rs.getLong("userId"));
				file.setUserName(rs.getString("userName"));
				file.setFirstName(rs.getString("firstName"));
				file.setLastName(rs.getString("lastName"));
				file.setCountry(rs.getString("country"));
				file.setSkinId(rs.getLong("skinId"));
				file.setDefaultDocumentsCount(rs.getLong("defaultDocumentsCount"));
				file.setLastDefaultDocumentId(rs.getLong("lastDocumentId"));
				file.setLastMatchDate(rs.getTimestamp("lastMatchDate").getTime());
				filesList.add(file);
			}
			response.setFilesList(filesList);
		} finally {
			closeStatement(cstmt);
		}
	}
}
