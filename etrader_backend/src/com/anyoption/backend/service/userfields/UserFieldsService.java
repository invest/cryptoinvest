package com.anyoption.backend.service.userfields;

import java.sql.SQLException;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import com.anyoption.backend.service.files.UserDataRequest;
import com.anyoption.backend.service.files.UserIdRequest;
import com.anyoption.common.service.CommonJSONService;
import com.anyoption.common.service.results.MethodResult;

import il.co.etrader.backend.bl_managers.UsersManager;

public class UserFieldsService {

	private static final Logger logger = Logger.getLogger(UserFieldsService.class);

	public static AddressResponse getUserAddress(UserIdRequest request) {
		logger.debug("getUserAddress: " + request);
		AddressResponse response = new AddressResponse();
		try {
			Address address = UsersManager.getAddressByUserId(request.getUserId());
			response.setAddress(address);
		} catch (SQLException sqle) {
			logger.error("Unable to get user address! ", sqle);
			response.setErrorCode(CommonJSONService.ERROR_CODE_UNKNOWN);
		}
		return response;
	}
	
	public static MethodResult updateUserData(UserDataRequest request, HttpServletRequest httpRequest) {
		logger.debug("updateUserData:" + request);		
		MethodResult response = new MethodResult();		
		if (request.getFirstName() == null || request.getLastName() == null) {
			response.setErrorCode(CommonJSONService.ERROR_CODE_INVALID_INPUT);
			return response;
		}
		try {
			UsersManager.updateUserData(request.getUserId(), request.getDateOfBirth(), request.getFirstName(),
					request.getLastName(), request.getWriterId());
		} catch (SQLException e) {
			logger.error("Unable to updateUserData", e);
			response.setErrorCode(CommonJSONService.ERROR_CODE_UNKNOWN);
		}
		return response;		
	}
}