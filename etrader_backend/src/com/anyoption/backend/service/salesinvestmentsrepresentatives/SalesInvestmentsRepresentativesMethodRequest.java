package com.anyoption.backend.service.salesinvestmentsrepresentatives;

import com.anyoption.common.service.requests.MethodRequest;

public class SalesInvestmentsRepresentativesMethodRequest extends MethodRequest {
	private long fromDate;
	private long toDate;
	private Integer pageNumber;
	private int rowsPerPage;
	
	public long getFromDate() {
		return fromDate;
	}
	
	public void setFromDate(long fromDate) {
		this.fromDate = fromDate;
	}
	
	public long getToDate() {
		return toDate;
	}
	
	public void setToDate(long toDate) {
		this.toDate = toDate;
	}
	
	public Integer getPageNumber() {
		return pageNumber;
	}
	
	public void setPageNumber(Integer pageNumber) {
		this.pageNumber = pageNumber;
	}
	
	public int getRowsPerPage() {
		return rowsPerPage;
	}
	
	public void setRowsPerPage(int rowsPerPage) {
		this.rowsPerPage = rowsPerPage;
	}
}
