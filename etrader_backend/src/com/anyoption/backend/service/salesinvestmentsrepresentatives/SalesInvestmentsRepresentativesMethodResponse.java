package com.anyoption.backend.service.salesinvestmentsrepresentatives;

import java.util.ArrayList;

import com.anyoption.common.beans.SalesInvestments;
import com.anyoption.common.service.results.MethodResult;

public class SalesInvestmentsRepresentativesMethodResponse extends MethodResult {
	
	private ArrayList<SalesInvestments> salesInvestments;
	private long totalCount;
	private double totalAmount;
	
	public ArrayList<SalesInvestments> getSalesInvestments() {
		return salesInvestments;
	}
	
	public void setSalesInvestments(ArrayList<SalesInvestments> salesInvestments) {
		this.salesInvestments = salesInvestments;
	}
	
	public long getTotalCount() {
		return totalCount;
	}
	
	public void setTotalCount(long totalCount) {
		this.totalCount = totalCount;
	}

	public double getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(double totalAmount) {
		this.totalAmount = totalAmount;
	}
	
	
	
}
