package com.anyoption.backend.service.optionplusprices;

import com.anyoption.common.service.assetindex.MarketRequest;

import il.co.etrader.backend.bl_vos.OptionPlusPriceMatrixItem;

public class OptionPlusPriceRequest extends MarketRequest {
	
	private OptionPlusPriceMatrixItem item;
	private double newPrice;
	
	public OptionPlusPriceMatrixItem getItem() {
		return item;
	}
	public void setItem(OptionPlusPriceMatrixItem item) {
		this.item = item;
	}
	public double getNewPrice() {
		return newPrice;
	}
	public void setNewPrice(double newPrice) {
		this.newPrice = newPrice;
	}
}
