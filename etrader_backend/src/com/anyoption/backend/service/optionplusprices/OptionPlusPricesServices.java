package com.anyoption.backend.service.optionplusprices;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import com.anyoption.backend.managers.WriterPermisionsManager;
import com.anyoption.common.backend.annotations.BackendPermission;
import com.anyoption.common.service.CommonJSONService;
import com.anyoption.common.service.assetindex.MarketRequest;
import com.anyoption.common.service.requests.UserMethodRequest;
import com.anyoption.common.service.results.MethodResult;

import il.co.etrader.backend.bl_managers.OptionPlusManager;
import il.co.etrader.backend.bl_vos.OptionPlusPriceMatrixItem;
import il.co.etrader.backend.dao_managers.MarketsManager;

public class OptionPlusPricesServices {
	
	private static final String SCREEN_NAME_PERMISSIONS_OPTION_PLUS_PRICES = "optionPlusPrices";
	private static final Logger log = Logger.getLogger(OptionPlusPricesServices.class);
	
	@BackendPermission(id = "optionPlusPrices_view")
	public static OptionPlusPricesFilterResponse getOptionPlusPricesScreenSettings(UserMethodRequest request,
			HttpServletRequest httpRequest) {
		log.debug("getOptionPlusPricesScreenSettings:" + request);
		OptionPlusPricesFilterResponse response = new OptionPlusPricesFilterResponse();
		WriterPermisionsManager.setWriterScreenPermissionsByScreenName(SCREEN_NAME_PERMISSIONS_OPTION_PLUS_PRICES,
																		response, httpRequest);
		try {
			Map<Long, String> map = MarketsManager.getOptionPlusMarkets();
			response.setOptionPlusMarkets(map);
		} catch (Exception e) {
			log.error("Unable to get option plus markets list! ", e);
			response.setErrorCode(CommonJSONService.ERROR_CODE_UNKNOWN);
		}
		return response;
	}
	
	@BackendPermission(id = "optionPlusPrices_view")
	public static OptionPlusPricesResponse getOptionPlusPrices(MarketRequest request) {
		log.debug("getOptionPlusPrices:" + request);
		OptionPlusPricesResponse response = new OptionPlusPricesResponse();
		try {
			ArrayList<OptionPlusPriceMatrixItem> matrix = OptionPlusManager.buildOptionPlusPriceMatrix(request.getMarketId());
			response.setOptionPlusMatrix(matrix);
		} catch (SQLException e) {
			log.error("Unable to get option plus matrix! ", e);
			response.setErrorCode(CommonJSONService.ERROR_CODE_UNKNOWN);
		}
		return response;
	}
	
	@BackendPermission(id = "optionPlusPrices_view")
	public static MethodResult updatePrice(OptionPlusPriceRequest request) {
		log.debug("updatePrice:" + request);
		MethodResult response = new MethodResult();
		try {
			OptionPlusManager.updatePrice(request.getItem().getId(), request.getNewPrice()/100);
		} catch (SQLException e) {
			log.error("Unable to update option plus price! ", e);
			response.setErrorCode(CommonJSONService.ERROR_CODE_UNKNOWN);
		}
		return response;
	}
}
