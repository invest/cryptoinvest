/**
 * 
 */
package com.anyoption.backend.service.deposits;

import java.sql.SQLException;
import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import com.anyoption.backend.service.pendingwithdrawals.DepositSummary;
import com.anyoption.backend.service.pendingwithdrawals.DepositSummaryDetailsResponse;
import com.anyoption.backend.service.pendingwithdrawals.PendingWithdrawalsManager;
import com.anyoption.common.backend.annotations.BackendPermission;
import com.anyoption.common.managers.WriterPermisionsManagerBase;
import com.anyoption.common.service.CommonJSONService;
import com.anyoption.common.service.requests.UserMethodRequest;
import com.anyoption.common.service.results.PermissionsGetResponseBase;

import il.co.etrader.backend.bl_vos.User;
import il.co.etrader.util.ConstantsBase;

/**
 * @author pavel.tabakov
 *
 */
public class DepositsServices {
	
	private static final String SCREEN_NAME = "depositsSummary";
	private static final Logger log = Logger.getLogger(DepositsServices.class);
	
	
	@BackendPermission(id = "depositsSummary_view")
	public static PermissionsGetResponseBase getDepositsSummaryScreenSettings(UserMethodRequest request, HttpServletRequest httpRequest) {
		log.debug("getScreenSettings:" + request);		
		PermissionsGetResponseBase response = new PermissionsGetResponseBase();		
		try {
			response.setPermissionsList(WriterPermisionsManagerBase.getWriterScreenPermissions(SCREEN_NAME, request.getWriterId()));
		} catch (SQLException e) {
			log.error("Can't getScreenSettings for depositsSummary" , e);
		}
		return response;
	}
	
	
	public static DepositSummaryDetailsResponse getDepositSummaryDetails(UserMethodRequest request, HttpServletRequest httpRequest) {
		DepositSummaryDetailsResponse response = new DepositSummaryDetailsResponse();

		User user = (User) httpRequest.getSession().getAttribute(ConstantsBase.BIND_SESSION_USER);
		if (user == null || user.getId() < 1) {
			log.error("Unable to get user id from session!");
			response.setErrorCode(CommonJSONService.ERROR_CODE_UNKNOWN);
		} else {
			try {
				ArrayList<DepositSummary> depositSummaryDetails = PendingWithdrawalsManager.getDepositSummaryDetails(0, user.getId());
				response.setDepositSummaryList(depositSummaryDetails);
			} catch (SQLException e) {
				log.error("Unable to get deposit summary details! ", e);
				response.setErrorCode(CommonJSONService.ERROR_CODE_UNKNOWN);
			}
		}
		return response;
	}
	
}
