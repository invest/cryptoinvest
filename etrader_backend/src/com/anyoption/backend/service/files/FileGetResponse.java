/**
 * 
 */
package com.anyoption.backend.service.files;

import java.util.HashMap;

import com.anyoption.common.beans.File;
import com.anyoption.common.service.results.MethodResult;

public class FileGetResponse extends MethodResult {
	
	private File file;
	private HashMap<Long, HashMap<String, String>> creditCards;

	public File getFile() {
		return file;
	}

	public void setFile(File file) {
		this.file = file;
	}

	public HashMap<Long, HashMap<String, String>> getCreditCards() {
		return creditCards;
	}

	public void setCreditCards(HashMap<Long, HashMap<String, String>> creditCards) {
		this.creditCards = creditCards;
	}
		
}
