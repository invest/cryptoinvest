/**
 * 
 */
package com.anyoption.backend.service.files;

import com.anyoption.common.beans.File;
import com.anyoption.common.service.requests.UserMethodRequest;

public class FileRequest extends UserMethodRequest {
	
	private File file;
	private long previousNext;
	private boolean isLoadedFile;
	private boolean isIDsScreen;

	public long getPreviousNext() {
		return previousNext;
	}

	public void setPreviousNext(long previousNext) {
		this.previousNext = previousNext;
	}

	public File getFile() {
		return file;
	}

	public void setFile(File file) {
		this.file = file;
	}	

	public boolean isLoadedFile() {
		return isLoadedFile;
	}

	public void setLoadedFile(boolean isLoadedFile) {
		this.isLoadedFile = isLoadedFile;
	}
	
    public String toString() {
        String ls = System.getProperty("line.separator");
        return ls + "FileRequest: "
            + super.toString()
            + "File: " + file + ls
            + "previousNext: " + previousNext
            + "isLoadedFile: " + isLoadedFile;
    }

	public boolean isIDsScreen() {
		return isIDsScreen;
	}

	public void setIDsScreen(boolean isIDsScreen) {
		this.isIDsScreen = isIDsScreen;
	}
}
