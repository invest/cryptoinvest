package com.anyoption.backend.managers;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;

import org.apache.log4j.Logger;

import com.anyoption.backend.daos.WriterDAO;
import com.anyoption.backend.service.writer.WriterGetAllMethodRequest;
import com.anyoption.backend.service.writer.WriterGetAllMethodResponse;
import com.anyoption.backend.service.writer.WriterInsertMethodRequest;
import com.anyoption.backend.service.writer.WriterInsertMethodResponse;
import com.anyoption.backend.service.writer.WriterResetPasswordMethodRequest;
import com.anyoption.backend.service.writer.WriterUpdateMethodRequest;
import com.anyoption.common.beans.Writer;
import com.anyoption.common.managers.BaseBLManager;
import com.anyoption.common.service.CommonJSONService;
import com.anyoption.common.service.results.MethodResult;

public class WriterManager extends BaseBLManager {

	private static final Logger logger = Logger.getLogger(WriterManager.class);
	public static final String FILTER_SKINS = "skins";// writer get from ApplicationData
	public static final String FILTER_DEPARTMENT = "departments";
	public static final String FILTER_IS_ACTIVE = "isActive";
	public static final String FILTER_ROLE = "roles";
	public static final String FILTER_SALES_TYPE = "salesType";
	public static final String FILTER_SITE = "site";
	
	public static HashMap<Long, String> isActive;
	public static HashMap<Long, String> departments;
	public static HashMap<Long, String> site;
	//TODO: need Ignite Cache
	//all the retention Writers
	public static HashMap<Long, String> retentionWriters;	
	public static HashMap<Long, String> salesType;
	
	public static final String[] filterConstants = {"", FILTER_DEPARTMENT, FILTER_SITE};

	public static HashMap<Long, String> getIsActive() {
		isActive = new HashMap<>();
		isActive.put(1l, "Yes");
		isActive.put(0l, "No");
		return isActive;
	}
	
	public static HashMap<Long, String> getDepartments() {
		if (departments == null) {
			Connection con = null;
			try {
				con = getConnection();
				departments = WriterDAO.getFilters(con).get(FILTER_DEPARTMENT);
			} catch (SQLException ex) {
				logger.error("Can't lazzy load departments: ", ex);
			} finally {
				closeConnection(con);
			}
		}
		return departments;
	}
	
	public static HashMap<Long, String> getSite() {
		if (site == null) {
			Connection con = null;
			try {
				con = getConnection();
				site = WriterDAO.getFilters(con).get(FILTER_SITE);
			} catch (SQLException ex) {
				logger.error("Can't lazzy load site: ", ex);
			} finally {
				closeConnection(con);
			}
		}
		return site;
	}
	
	public static HashMap<Long, String> getSalesType() {
		salesType = new HashMap<>();
		salesType.put(1l, "Conversion");
		salesType.put(2l, "Retention");
		salesType.put(3l, "Both");
		return salesType;
	}
	
	public static void insert(WriterInsertMethodRequest request, WriterInsertMethodResponse response, String randomPass) {
		Connection conn = null;
		try {
			conn = getConnection();
			WriterDAO.insert(request, conn, randomPass, response);
		} catch (SQLException ex) {
			logger.info("Fail to insert writer with username: " + request.getUserName(), ex);
			response.setErrorCode(CommonJSONService.ERROR_CODE_FAIL_TO_INSERT);
		} finally {
			closeConnection(conn);
		}
	}

	public static void update(WriterUpdateMethodRequest request, MethodResult result) {
		Connection conn = null;
		try {
			conn = getConnection();
			WriterDAO.update(request, conn);
		} catch (SQLException ex) {
			logger.info("Fail to update writer with ID: " + request.getWriterId(), ex);
			result.setErrorCode(CommonJSONService.ERROR_CODE_FAIL_TO_UPDATE);
		} finally {
			closeConnection(conn);
		}
	}

	public static ArrayList<Writer> getAll(WriterGetAllMethodRequest request, WriterGetAllMethodResponse response) {
		Connection conn = null;
		try {
			conn = getConnection();
			return WriterDAO.getAll(conn, request, response);
		} catch (SQLException ex) {
			logger.info("Fail to get all writers due to: ", ex);
			response.setErrorCode(CommonJSONService.ERROR_CODE_FAIL_TO_LOAD_WRITERS);
			return null;
		} finally {
			closeConnection(conn);
		}
	}
	
	public static ArrayList<String> getUsernamesToCompare(String username) throws SQLException {
		Connection conn = null;
		try {
			conn = getConnection();
			return WriterDAO.getUsernamesToCompare(conn, username);
		} finally {
			closeConnection(conn);
		}
	}
	
	public static Writer isWriterExists(WriterResetPasswordMethodRequest request) {
		Connection conn = null;
		try {
			conn = getConnection();
			return WriterDAO.isWriterExists(conn, request);
		}catch(SQLException ex){
			logger.info("Fail to get all writer due to: ", ex);
			return null;
		} finally {
			closeConnection(conn);
		}
	}
	
	public static void resetWriterPassAndFailCount(long writerId, MethodResult result, String pass) {
		Connection conn = null;
		try {
			conn = getConnection();
			WriterDAO.resetWriterPassAndFailCount(writerId, conn, pass);
		} catch (SQLException ex) {
			logger.debug("Fail to reset fail count and password for writer with ID: " + writerId, ex);
			result.setErrorCode(CommonJSONService.ERROR_CODE_FAIL_TO_UPDATE);
		} finally {
			closeConnection(conn);
		}
	}
	
	public static boolean isValid(String i){
		if(i == null || i.equals("")){
			return false;
		}
		return true;
	}
	
	public static HashMap<Long, String> getRetentionWriters(long WriterId) {
		if (retentionWriters == null) {
			Connection con = null;
			try {
				con = getConnection();
				retentionWriters = WriterDAO.getWriters(con, WriterId);
			} catch (SQLException ex) {
				logger.error("Can't lazzy load Writers: ", ex);
			} finally {
				closeConnection(con);
			}
		}
		return retentionWriters;
	}
}
