/**
 * 
 */
package com.anyoption.backend.managers;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import com.anyoption.backend.daos.TransactionDAO;
import com.anyoption.backend.service.transaction.CryptoDeposits;
import com.anyoption.backend.service.transaction.GetAllCryptoDepositsResponse;
import com.anyoption.backend.service.transaction.GetAllCryptoDepsitsRequest;
import com.anyoption.backend.service.transaction.TransactionGetAllMethodRequest;
import com.anyoption.backend.service.transaction.TransactionGetAllMethodResponse;
import com.anyoption.backend.service.transaction.TransactionPostponedRequest;
import com.anyoption.backend.service.transaction.TransactionReroutingGetResponse;
import com.anyoption.backend.service.updatetransaction.UpdateTransactionMethodRequest;
import com.anyoption.backend.service.updatetransaction.UpdateTransactionMethodResponse;
import com.anyoption.backend.service.withdraw.WithdrawInsertMethodRequest;
import com.anyoption.common.beans.Transaction;
import com.anyoption.common.beans.base.Currency;
import com.anyoption.common.clearing.ClearingException;
import com.anyoption.common.clearing.ClearingInfo;
import com.anyoption.common.clearing.ClearingProvider;
import com.anyoption.common.daos.GeneralDAO;
import com.anyoption.common.managers.CurrenciesManagerBase;
import com.anyoption.common.managers.TransactionsManagerBase;
import com.anyoption.common.service.CommonJSONService;
import com.anyoption.common.service.results.MethodResult;

import il.co.etrader.backend.bl_managers.UsersManager;
import il.co.etrader.backend.bl_vos.User;
import il.co.etrader.backend.dao_managers.UsersDAO;
import il.co.etrader.backend.util.Constants;
import il.co.etrader.bl_managers.ClearingManager;
import il.co.etrader.bl_vos.UserBase;
import il.co.etrader.dao_managers.TransactionsDAOBase;
import il.co.etrader.dao_managers.UsersDAOBase;
import il.co.etrader.util.CommonUtil;
import il.co.etrader.util.ConstantsBase;

/**
 * @author pavel.tabakov
 *
 */
public class TransactionManager extends TransactionsManagerBase {
	
	private static final Logger logger = Logger.getLogger(TransactionManager.class);
	
	public static final String FILTER_SKINS = "skins";// not used in filterConstants
	public static final String FILTER_CLASS_USERS = "classUsers";
	public static final String FILTER_PLATFORMS = "platforms";
	public static final String FILTER_SKIN_BUSINESS_CASES = "skinBusinessCases";
	public static final String FILTER_COUNTRIES = "countries";
	public static final String FILTER_CURRENCIES = "currencies";// not used in filterConstants
	public static final String FILTER_ASSIGNEES = "assignees";// not used in filterConstants
	public static final String FILTER_TRANSACTION_STATUSES = "transactionStatuses";
	public static final String FILTER_TRANSACTION_CLASS_TYPES = "transactionClassTypes";
	public static final String FILTER_TRANSACTION_TYPES = "transactionTypes";
	public static final String FILTER_CLEARING_PROVIDERS = "clearingProviders";
	public static final String FILTER_HAS_BALANCE = "hasBalance";// not used in filterConstants
	public static final String FILTER_WAS_REROUTED = "wasRerouted";
	public static final String FILTER_IS_3D = "isThreeD";
	public static final String FILTER_TRANSACTIONS_CLASS_TYPE_MAPPING = "transactionsAndClassTypeTablesMapping";
	public static final String FILTER_MARKETING_CAMPAIGNS = "campaigns";
	public static final String FILTER_GATEWAY = "gateway";// not used in filterConstants
	public static final String FILTER_SYSTEM_WRITERS = "systemWriters";
	public static final String FILTER_J4_POSTPONED = "j4postponed";// not used in filterConstants
	public static final String FILTER_PENDING_WITHDRAW_TYPES = "pendingWithdrawTypes";
	public static final String FILTER_SALES_TYPES = "salesTypes";// not used in filterConstants
	public static final String FILTER_DEPOSIT_SOURCE = "depositSource";// not used in filterConstants
	public static final String FILTER_STATUS_CLASS_MAPPING = "statusClassMapping";// not used in filterConstants
	public static final String FILTER_LANGUAGES = "languages";
	
	public static final String[] filterConstants = {"", FILTER_PLATFORMS, FILTER_SKIN_BUSINESS_CASES,
			FILTER_CLASS_USERS, FILTER_COUNTRIES, FILTER_TRANSACTION_STATUSES,
			FILTER_TRANSACTION_CLASS_TYPES, FILTER_CLEARING_PROVIDERS, FILTER_TRANSACTION_TYPES,
			FILTER_TRANSACTIONS_CLASS_TYPE_MAPPING, FILTER_MARKETING_CAMPAIGNS, FILTER_SYSTEM_WRITERS,
			FILTER_PENDING_WITHDRAW_TYPES, FILTER_LANGUAGES};
	
	public static HashMap<Long, String> platforms;
	public static HashMap<Long, String> skinBusinessCases;
	public static HashMap<Long, String> userClasses;
	public static HashMap<Long, String> currencies;
	public static HashMap<Long, String> transactionStatuses;
	public static HashMap<Long, String> transactionClassTypes;
	public static HashMap<Long, String> transactionTypes;
	public static HashMap<Long, String> clearingProviders;
	public static HashMap<Long, String> hasBalance;
	public static HashMap<Long, String> wasRerouted;
	public static HashMap<Long, String> isThreeD;
	public static HashMap<Long, String> transactionsAndClassTypeTablesMapping;
	public static HashMap<Long, String> campaigns;
	public static HashMap<Long, String> gateway;
	public static HashMap<Long, String> j4postponed;
	public static HashMap<Long, String> pendingWithdrawTypes;
	public static HashMap<Long, String> salesType;
	public static HashMap<Long, String> depositSource;
	public static HashMap<Long, String> statusClassMapping;
	public static HashMap<Integer, Integer> typesAndBalanceMapping;
	public static HashMap<Long, String> excludedTypes;
	public static HashMap<Long, String> excludedStatuses;
	public static HashMap<Long, String> languages;
	public static HashMap<Long, String> assignees;
	
	// FILTERS START
	public static HashMap<Long, String> getPlatforms() {
		if (platforms == null) {
			Connection con = null;
			try {
				con = getConnection();
				platforms = TransactionDAO.getFilters(con).get(FILTER_PLATFORMS);
			} catch (SQLException ex) {
				logger.error("Can't lazzy load Platforms: ", ex);
			} finally {
				closeConnection(con);
			}
		}
		return platforms;
	}

	public static HashMap<Long, String> getSkinBusinessCases() {
		if (skinBusinessCases == null) {
			Connection con = null;
			try {
				con = getConnection();
				skinBusinessCases = TransactionDAO.getFilters(con).get(FILTER_SKIN_BUSINESS_CASES);
			} catch (SQLException ex) {
				logger.error("Can't lazzy load SkinBusinessCases: ", ex);
			} finally {
				closeConnection(con);
			}
		}
		return skinBusinessCases;
	}

	public static HashMap<Long, String> getUserClasses() {
		if (userClasses == null) {
			Connection con = null;
			try {
				con = getConnection();
				userClasses = TransactionDAO.getFilters(con).get(FILTER_CLASS_USERS);
				userClasses.put((long)12, "user.class.privatecompany");
			} catch (SQLException ex) {
				logger.error("Can't lazzy load user classes: ", ex);
			} finally {
				closeConnection(con);
			}
		}
		return userClasses;
	}
	
	public static HashMap<Long, String> getAllCurrencies() {
		if (currencies == null) {
			Hashtable<Long, Currency> currList = CurrenciesManagerBase.getCurrencies();
			currencies = new HashMap<>();
			for (Long key : currList.keySet()) {
				currencies.put(currList.get(key).getId(), currList.get(key).getDisplayName());
			}
		}
		return currencies;
	}	
	
	
	public static HashMap<Long, String> getAllTransactionStatuses() {
		if (transactionStatuses == null) {
			Connection con = null;
			try {
				con = getConnection();
				transactionStatuses = TransactionDAO.getFilters(con).get(FILTER_TRANSACTION_STATUSES);
			} catch (SQLException ex) {
				logger.error("Can't lazzy load transaction statuses: ", ex);
			} finally {
				closeConnection(con);
			}
		}
		return transactionStatuses;
	}
	
	public static HashMap<Long, String> getAllTransactionClassTypes() {
		if (transactionClassTypes == null) {
			Connection con = null;
			try {
				con = getConnection();
				transactionClassTypes = TransactionDAO.getFilters(con).get(FILTER_TRANSACTION_CLASS_TYPES);
			} catch (SQLException ex) {
				logger.error("Can't lazzy load transaction class types: ", ex);
			} finally {
				closeConnection(con);
			}
		}
		return transactionClassTypes;
	}
	
	public static HashMap<Long, String> getAllLanguages() {
		if (languages == null) {
			Connection con = null;
			try {
				con = getConnection();
				languages = TransactionDAO.getFilters(con).get(FILTER_LANGUAGES);
			} catch (SQLException ex) {
				logger.error("Can't lazzy load transaction class types: ", ex);
			} finally {
				closeConnection(con);
			}
		}
		return languages;
	}
	
	public static HashMap<Long, String> getAllClearingProviders() {
		if (clearingProviders == null) {
			Connection con = null;
			try {
				con = getConnection();
				clearingProviders = TransactionDAO.getFilters(con).get(FILTER_CLEARING_PROVIDERS);
			} catch (SQLException ex) {
				logger.error("Can't lazzy load clearing providers: ", ex);
			} finally {
				closeConnection(con);
			}
		}
		return clearingProviders;
	}
	
	public static HashMap<Long, String> getHasBalance() {
		hasBalance = new HashMap<>();
		hasBalance.put(1l, "Yes");
		hasBalance.put(0l, "No");
		return hasBalance;
	}
	
	public static HashMap<Long, String> getWasRerouted() {
		wasRerouted = new HashMap<>();
		wasRerouted.put(1l, "Yes");
		wasRerouted.put(0l, "No");
		return wasRerouted;
	}
	
	public static HashMap<Long, String> getIsThreeD() {
		isThreeD = new HashMap<>();
		isThreeD.put(1l, "Yes");
		isThreeD.put(0l, "No");
		return isThreeD;
	}
	
	public static HashMap<Long, String> getGateway() {
		gateway = new HashMap<>();
		gateway.put(1l, "Direct");
		gateway.put(0l, "EPG");
		return gateway;
	}
	
	public static HashMap<Long, String> getAssignees() {
		assignees = new HashMap<>();
		assignees.put(1l, "Finance");
		assignees.put(2l, "Support");
		assignees.put(3l, "Risk");
		return assignees;
	}
	
	public static HashMap<Long, String> getJ4postponed() {
		j4postponed = new HashMap<>();
		j4postponed.put(1l, "Yes");
		j4postponed.put(0l, "No");
		return j4postponed;
	}
	
	public static HashMap<Long, String> getAllTransactionTypes() {
		if (transactionTypes == null) {
			Connection con = null;
			try {
				con = getConnection();
				transactionTypes = TransactionDAO.getFilters(con).get(FILTER_TRANSACTION_TYPES);
			} catch (SQLException ex) {
				logger.error("Can't lazzy load transaction types: ", ex);
			} finally {
				closeConnection(con);
			}
		}
		return transactionTypes;
	}
	
	public static HashMap<Long, String> getTransactionsAndClassTypeTablesMapping() {
		if (transactionsAndClassTypeTablesMapping == null) {
			Connection con = null;
			try {
				con = getConnection();
				transactionsAndClassTypeTablesMapping = TransactionDAO.getFilters(con).get(FILTER_TRANSACTIONS_CLASS_TYPE_MAPPING);
				transactionsAndClassTypeTablesMapping.put(-172l, "1");
				transactionsAndClassTypeTablesMapping.put(-173l, "1");
				transactionsAndClassTypeTablesMapping.put(-174l, "1");
			} catch (SQLException ex) {
				logger.error("Can't lazzy load transaction types mapping: ", ex);
			} finally {
				closeConnection(con);
			}
		}
		return transactionsAndClassTypeTablesMapping;
	}
	
	public static HashMap<Long, String> getCampaigns() {
		if (campaigns == null) {
			Connection con = null;
			try {
				con = getConnection();
				campaigns = TransactionDAO.getFilters(con).get(FILTER_MARKETING_CAMPAIGNS);
			} catch (SQLException ex) {
				logger.error("Can't lazzy load campaigns: ", ex);
			} finally {
				closeConnection(con);
			}
		}
		return campaigns;
	}

	public static HashMap<Long, String> getPendingWithdrawTypes() {
		if (pendingWithdrawTypes == null) {
			Connection con = null;
			try {
				con = getConnection();
				pendingWithdrawTypes = TransactionDAO.getFilters(con).get(FILTER_PENDING_WITHDRAW_TYPES);
			} catch (SQLException ex) {
				logger.error("Can't lazzy load pending withdraw types due to: ", ex);
			} finally {
				closeConnection(con);
			}
		}
		return pendingWithdrawTypes;
	}
	
	public static HashMap<Long, String> getSalesType() {
		salesType = new HashMap<>();
		salesType.put((long)ConstantsBase.SALES_DEPOSIT_TYPE_CONVERSION, "sales.deposit.sales.deposits.conversion.new.BE");
		salesType.put((long)ConstantsBase.SALES_DEPOSIT_TYPE_RETENTION, "sales.deposit.sales.deposits.retention.new.BE");
		return salesType;
	}
	
	public static HashMap<Long, String> getDepositSource() {
		depositSource = new HashMap<>();
		depositSource.put((long)Constants.SALES_DEPOSIT_SOURCE_SALES, "sales.deposit.source.sales");
		depositSource.put((long)Constants.SALES_DEPOSIT_SOURCE_INDEPENDENT, "sales.deposit.source.independent");
		return depositSource;
	}
	// FILTERS END
	
	public static ArrayList<Transaction> getAll(TransactionGetAllMethodRequest request, TransactionGetAllMethodResponse response) {
		Connection conn = null;
		try {
			conn = getConnection();
			conn.setAutoCommit(false);
			return TransactionDAO.getAll(conn, request, response);
		} catch (SQLException ex) {
			logger.info("Fail to get all transaction due to: ", ex);
			response.setErrorCode(CommonJSONService.ERROR_CODE_FAIL_TO_LOAD_TRANSACTIONS);
			return null;
		} finally {
			try {
				conn.commit();// to clean up temporary table used in get_transactions DB procedure
			} catch (SQLException e) {
				logger.error("Can not commit due to: ", e);
			}
			try {
				conn.setAutoCommit(true);
			} catch (SQLException e) {
				logger.error("Can't set back to autocommit due to: ", e);
			}
			closeConnection(conn);
		}
	}
	
	public static ArrayList<CryptoDeposits> getAllCrytpoDeposits(GetAllCryptoDepsitsRequest request, GetAllCryptoDepositsResponse response) throws SQLException {
		Connection conn = null;
		try {
			conn = getConnection();
			return TransactionDAO.getAllCrytpoDeposits(conn, request, response);
		} finally {
			closeConnection(conn);
		}
	}
	
	public static TransactionReroutingGetResponse getReroutingTransactions(long trasnactionId) throws SQLException {
		Connection conn = null;
		try {
			conn = getConnection();
			return TransactionDAO.getReroutingTransactions(conn, trasnactionId);
		} finally {
			closeConnection(conn);
		}
	}

	public static void approveDeposit(Transaction t, long writerId, MethodResult response) {
		
		Connection con = null;
		try {
			con = getConnection();
			TransactionDAO.approveDeposit(con, t.getId(), writerId);
			response.setErrorCode(CommonJSONService.ERROR_CODE_SUCCESS);
		} catch (SQLException ex) {
			logger.error("Exception during approve deposit due to: " + ex);
			response.setErrorCode(CommonJSONService.ERROR_CODE_UNKNOWN);
		}

	}
	
	public static void cancelDeposit(Transaction t, long writerId, int cancelStatus, MethodResult response) {
		Connection con = null;

		try {
			con = getConnection();
			con.setAutoCommit(false);
			
			TransactionDAO.cancelDeposit(con, t.getId(), cancelStatus, writerId);

			ClearingProvider p = ClearingManager.getClearingProviders().get(t.getClearingProviderId());
			if (null != p) {
				User user = TransactionManager.getUser(t.getUserId());
				p.cancel(new ClearingInfo(user, t.getAmount(), t.getTypeId(), t.getId(), t.getIp(), t.getAcquirerResponseId()));
			}
			
			response.setErrorCode(CommonJSONService.ERROR_CODE_SUCCESS);
			con.commit();

		} catch (SQLException e) {
			try {
				con.rollback();
			} catch (SQLException e1) {
				logger.log(Level.ERROR, "Can't rollback.", e1);
			}
			logger.error("Exception during Cancel Deposit due to: " + e);
			response.setErrorCode(CommonJSONService.ERROR_CODE_UNKNOWN);
		} catch (ClearingException e) {
			try {
				con.rollback();
			} catch (SQLException e1) {
				logger.log(Level.ERROR, "Can't rollback.", e1);
			}
			logger.error("Exception during clearing cancel deposit due to: " + e);
			response.setErrorCode(CommonJSONService.ERROR_CODE_UNKNOWN);
		} finally {
			try {
				con.setAutoCommit(true);
			} catch (SQLException e) {
				logger.log(Level.ERROR, "Can't set auto commit true due to: ", e);
				response.setErrorCode(CommonJSONService.ERROR_CODE_UNKNOWN);
			}
			closeConnection(con);
		}
	}
	
	
	public static User getUser(long userId) throws SQLException {
		Connection con = null;
		try {
			con = getConnection();
			return UsersDAO.getUserById(con, userId);
		} finally {
			closeConnection(con);
		}
	}
	
	public static MethodResult manageTransactionPostponed(TransactionPostponedRequest request) {
		MethodResult response = new MethodResult();
		Connection con = null;
		try {
			con = getConnection();
			Transaction t = TransactionsDAOBase.getById(con, request.getTransactionId());
			if(t.isCcDepositAndPending()){
				TransactionDAO.manageTransactionPostponed(con, request);
			} else {
				logger.error("Can't Transaction Postponed, it is not CcDeposit And Pending:" + t);
				response.setErrorCode(CommonJSONService.ERROR_CODE_UNKNOWN);
				response.addUserMessage(null, "J4 Postpone options should only be available for PENDING CC deposits");
			}
		} catch (SQLException ex) {
			logger.error("Can't manageTransactionPostponed due to: ", ex);
			response.setErrorCode(CommonJSONService.ERROR_CODE_UNKNOWN);
		}

		return response;
	}
	
	public static MethodResult insertWithdraw(WithdrawInsertMethodRequest request) {
		MethodResult response = new MethodResult();
		Connection con = null;
		try {
			con = getConnection();
			TransactionDAO.insertWithdraw(con, request);
			response.setErrorCode(CommonJSONService.ERROR_CODE_SUCCESS);
		} catch (SQLException ex) {
			logger.error("Can't insertWithdraw due to: ", ex);
			response.setErrorCode(CommonJSONService.ERROR_CODE_UNKNOWN);
		}

		return response;
	}
	
	public static Date getFirstDepositWithCC(long userId, long ccId) {
		Connection con = null;
		try {
			con = getConnection();
			return TransactionsDAOBase.getFirstDepositWithCC(con, userId, ccId);
		} catch (SQLException ex) {
			logger.error("Can't find firstDepositWithCC", ex );
			return null;
		}
	}
	
	public static void getTransactionChanges(TransactionGetAllMethodRequest request, UpdateTransactionMethodResponse response) {
		Connection con = null;
		try {
			con = getConnection();
			TransactionDAO.getTransactionChanges(con, request, response);
		} catch (SQLException sqle) {
			logger.error("Can't get transactionChanges", sqle);
		} finally {
			closeConnection(con);
		}
	}
	
	public static void updateTransactionChange(UpdateTransactionMethodRequest request, Transaction t) {
		Connection con = null;
		try {
			con = getConnection();
			con.setAutoCommit(false);
			String comment = t.getComments() == null ? "" : t.getComments() + "|";
			if (null != request.getTypeId() && t.getTypeId() != request.getTypeId().longValue()) {
				t.setComments(comment + "Changed from " + CommonUtil.getMessage(t.getTypeName(), null) + " by "
						+ CommonUtil.getWriterName(request.getWriterId()) + " "
						+ Timestamp.valueOf(LocalDateTime.now()));
				if (t.getTypeId() == 17 && request.getTypeId().longValue() != 17) {
					t.setPaymentTypeId(0);
				}
				t.setTypeId(request.getTypeId().longValue());
			} else {
				t.setComments(comment + "Changed from " + transactionStatuses.get(t.getStatusId()) + " by "
						+ il.co.etrader.util.CommonUtil.getWriterName(request.getWriterId()) + " "
						+ Timestamp.valueOf(LocalDateTime.now()));

				UserBase user = UsersManager.getUserById(t.getUserId());
				if (t.getStatusId() == TransactionsManagerBase.TRANS_STATUS_FAILED
						&& request.getStatusId() == TransactionsManagerBase.TRANS_STATUS_SUCCEED) {
					if (t.getClassType() == TransactionsManagerBase.TRANS_CLASS_DEPOSIT
							|| t.getClassType() == TransactionsManagerBase.TRANS_CLASS_ADMIN_DEPOSITS) {
						updateBalance(con, user, t, true);
					}
					if (t.getClassType() == TransactionsManagerBase.TRANS_CLASS_WITHDRAW
							|| t.getClassType() == TransactionsManagerBase.TRANS_CLASS_ADMIN_WITHDRAWALS) {
						updateBalance(con, user, t, false);
					}
				}
				if (t.getStatusId() == TransactionsManagerBase.TRANS_STATUS_PENDING
						&& request.getStatusId() == TransactionsManagerBase.TRANS_STATUS_FAILED) {
					if (t.getClassType() == TransactionsManagerBase.TRANS_CLASS_DEPOSIT
							|| t.getClassType() == TransactionsManagerBase.TRANS_CLASS_ADMIN_DEPOSITS) {
						updateBalance(con, user, t, false);
					}
				}
				if (t.getStatusId() == TransactionsManagerBase.TRANS_STATUS_APPROVED
						&& request.getStatusId() == TransactionsManagerBase.TRANS_STATUS_FAILED) {
					if (t.getClassType() == TransactionsManagerBase.TRANS_CLASS_WITHDRAW
							|| t.getClassType() == TransactionsManagerBase.TRANS_CLASS_ADMIN_WITHDRAWALS) {
						updateBalance(con, user, t, true);
					}
				}
				t.setStatusId(request.getStatusId().longValue());
			}
			t.setWriterId(request.getWriterId());
			TransactionsDAOBase.update(con, t);
			con.commit();
		} catch (Exception e) {
			logger.error("Exception in success deposit Transaction! ", e);
			try {
				con.rollback();
			} catch (Throwable it) {
				logger.error("Can't rollback.", it);
			}
		} finally {
			try {
				con.setAutoCommit(true);
			} catch (Exception e) {
				logger.error("Can't set back to autocommit.", e);
			}
		}
	}
	
	public static HashMap<Long, String> getTrnStatusClassMapping() {
		statusClassMapping = new HashMap<>();
		statusClassMapping.put(1l, "1");
		//classStatusMapping.put(3l, "1");//
		statusClassMapping.put(7l, "1");
		statusClassMapping.put(8l, "1");
		statusClassMapping.put(10l, "1");
		statusClassMapping.put(12l, "1");
		statusClassMapping.put(13l, "1");
		statusClassMapping.put(14l, "1");
		statusClassMapping.put(15l, "1");
		statusClassMapping.put(16l, "1");
		//classStatusMapping.put(3l, "2");//
		statusClassMapping.put(4l, "2");
		statusClassMapping.put(5l, "2");
		statusClassMapping.put(6l, "2");
		statusClassMapping.put(9l, "2");
		statusClassMapping.put(11l, "2");
		statusClassMapping.put(17l, "2");
		statusClassMapping.put(18l, "2");
		statusClassMapping.put(19l, "2");
		//classStatusMapping.put(3l, "3");//
		return statusClassMapping;
	}
	
	public static HashMap<Integer, Integer> getTrnTypesBalanceMapping() {
		typesAndBalanceMapping = new HashMap<Integer, Integer>();
		typesAndBalanceMapping.put(TransactionsManagerBase.TRANS_TYPE_CC_DEPOSIT, ConstantsBase.LOG_BALANCE_CC_DEPOSIT);
		typesAndBalanceMapping.put(TransactionsManagerBase.TRANS_TYPE_WIRE_DEPOSIT, ConstantsBase.LOG_BALANCE_WIRE_DEPOSIT);
		typesAndBalanceMapping.put(TransactionsManagerBase.TRANS_TYPE_ADMIN_DEPOSIT, ConstantsBase.LOG_BALANCE_ADMIN_DEPOSIT);
		typesAndBalanceMapping.put(TransactionsManagerBase.TRANS_TYPE_ADMIN_WITHDRAW, ConstantsBase.LOG_BALANCE_ADMIN_WITHDRAW);
		typesAndBalanceMapping.put(TransactionsManagerBase.TRANS_TYPE_CASH_DEPOSIT, ConstantsBase.LOG_BALANCE_CASH_DEPOSIT);
		typesAndBalanceMapping.put(TransactionsManagerBase.TRANS_TYPE_CC_WITHDRAW, ConstantsBase.LOG_BALANCE_CC_WITHDRAW);
		typesAndBalanceMapping.put(TransactionsManagerBase.TRANS_TYPE_BANK_WIRE_WITHDRAW, ConstantsBase.LOG_BALANCE_BANK_WIRE);
		typesAndBalanceMapping.put(TransactionsManagerBase.TRANS_TYPE_DIRECT_BANK_DEPOSIT, ConstantsBase.LOG_BALANCE_DIRECT_DEPOSIT);
		typesAndBalanceMapping.put(TransactionsManagerBase.TRANS_TYPE_DEPOSIT_BY_COMPANY, ConstantsBase.LOG_BALANCE_DEPOSIT_BY_COMPANY);
		typesAndBalanceMapping.put(TransactionsManagerBase.TRANS_TYPE_PAYPAL_DEPOSIT, ConstantsBase.LOG_BALANCE_PAYPAL_DEPOSIT);
		typesAndBalanceMapping.put(TransactionsManagerBase.TRANS_TYPE_PAYPAL_WITHDRAW, ConstantsBase.LOG_BALANCE_PAYPAL_WITHDRAW);
		typesAndBalanceMapping.put(TransactionsManagerBase.TRANS_TYPE_MONEYBOOKERS_DEPOSIT, ConstantsBase.LOG_BALANCE_MONEYBOOKERS_DEPOSIT);
		typesAndBalanceMapping.put(TransactionsManagerBase.TRANS_TYPE_WEBMONEY_DEPOSIT, ConstantsBase.LOG_BALANCE_WEBMONEY_DEPOSIT);
		typesAndBalanceMapping.put(TransactionsManagerBase.TRANS_TYPE_MONEYBOOKERS_WITHDRAW, ConstantsBase.LOG_BALANCE_MONEYBOOKERS_WITHDARW);
		typesAndBalanceMapping.put(TransactionsManagerBase.TRANS_TYPE_WEBMONEY_WITHDRAW, ConstantsBase.LOG_BALANCE_WEBMONEY_WITHDRAW);
		typesAndBalanceMapping.put(TransactionsManagerBase.TRANS_TYPE_CUP_CHINA_DEPOSIT, ConstantsBase.LOG_BALANCE_DELTA_PAY_CHINA_DEPOSIT);
		typesAndBalanceMapping.put(TransactionsManagerBase.TRANS_TYPE_CUP_CHINA_WITHDRAW, ConstantsBase.LOG_BALANCE_DELTA_PAY_CHINA_WITHDRAW);
		typesAndBalanceMapping.put(TransactionsManagerBase.TRANS_TYPE_MAINTENANCE_FEE_CANCEL, ConstantsBase.LOG_BALANCE_MAINTENANCE_FEE_CANCEL);
		typesAndBalanceMapping.put(TransactionsManagerBase.TRANS_TYPE_INATEC_IFRAME_DEPOSIT, ConstantsBase.LOG_BALANCE_INATEC_IFRAME_DEPOSIT);
		typesAndBalanceMapping.put(TransactionsManagerBase.TRANS_TYPE_EPG_CHECKOUT_DEPOSIT, ConstantsBase.LOG_BALANCE_EPG_CHECKOUT_DEPOSIT);
		typesAndBalanceMapping.put((int)TransactionsManagerBase.TRANS_TYPE_DIRECT24_WITHDRAW, ConstantsBase.LOG_BALANCE_DIRECT24_WITHDRAW);
		typesAndBalanceMapping.put((int)TransactionsManagerBase.TRANS_TYPE_GIROPAY_WITHDRAW, ConstantsBase.LOG_BALANCE_GIROPAY_WITHDRAW);
		typesAndBalanceMapping.put((int)TransactionsManagerBase.TRANS_TYPE_EPS_WITHDRAW, ConstantsBase.LOG_BALANCE_EPS_WITHDRAW);
		return typesAndBalanceMapping;
	}
	
	public static HashMap<Long, String> getExcludedTypes() {
		excludedTypes = new HashMap<Long, String>();
		excludedTypes.put(new Long(8), "transactions.withdrawfee"); //TRANS_TYPE_WITHDRAW_FEE
		excludedTypes.put(new Long(11), "transactions.cc.withdrawfee"); //TRANS_TYPE_CC_WITHDRAW_FEE
		excludedTypes.put(new Long(15), "transactions.bank.wirefee"); //TRANS_TYPE_BANK_WIRE_FEE
		excludedTypes.put(new Long(27), "transactions.paypal.withdrawfee"); //TRANS_TYPE_PAYPAL_FEE
		excludedTypes.put(new Long(31), "transactions.envoy.withdrawfee"); //TRANS_TYPE_ENVOY_WITHDRAW_FEE
		excludedTypes.put(new Long(47), "transactions.maintenance.fee"); //TRANS_TYPE_MAINTENANCE_FEE
		excludedTypes.put(new Long(50), "transactions.moneybookers.withdraw.fee"); //Trans_Type_MB_Withdraw_Fee
		excludedTypes.put(new Long(51), "transactions.webmoney.withdraw.fee"); //Trans_Type_WM_Withdraw_Fee
		excludedTypes.put(new Long(55), "transactions.type.low.amt.withdraw.fee"); //TRANS_TYPE_LOW_AMOUNT_WITHDRAW_FEE
		excludedTypes.put(new Long(62), "transaction.homo.fee"); //TRANS_TYPE_HOMO_FEE
		excludedTypes.put((long)TransactionsManagerBase.TRANS_TYPE_BONUS_DEPOSIT, "transactions.bonus.deposit"); 
		excludedTypes.put((long)TransactionsManagerBase.TRANS_TYPE_POINTS_TO_CASH, "transactions.points.to.cash"); 
		excludedTypes.put((long)TransactionsManagerBase.TRANS_TYPE_COPYOP_COINS_TO_CASH, "transactions.copyop.coins.to.cash"); 
		excludedTypes.put((long)TransactionsManagerBase.TRANS_TYPE_BONUS_WITHDRAW, "transactions.bonus.withdraw"); 
		excludedTypes.put((long)TransactionsManagerBase.TRANS_TYPE_REVERSE_WITHDRAW, "transactions.bonus.withdraw"); 
		excludedTypes.put((long)TransactionsManagerBase.TRANS_TYPE_FIX_BALANCE_DEPOSIT, "transactions.fix.balance.deposit"); 
		excludedTypes.put((long)TransactionsManagerBase.TRANS_TYPE_FIX_BALANCE_WITHDRAW, "transactions.fix.balance.withdraw"); 
		excludedTypes.put((long)TransactionsManagerBase.TRANS_TYPE_FIX_NEGATIVE_BALANCE, "transactions.fix.negative.balance"); 
		return excludedTypes;
	}
	
	
	public static HashMap<Long, String> getExcludedStatuses() {
		excludedStatuses = new HashMap<Long, String>();
		excludedStatuses.put((long)TransactionsManagerBase.TRANS_STATUS_REVERSE_WITHDRAW, "trans.status.reverse");
		excludedStatuses.put((long)TransactionsManagerBase.TRANS_STATUS_CANCELED_ETRADER, "trans.status.canceledbyetrader");
		excludedStatuses.put((long)TransactionsManagerBase.TRANS_STATUS_CANCELED_FRAUDS, "trans.status.canceledbyfrauds"); 
		excludedStatuses.put((long)TransactionsManagerBase.TRANS_STATUS_CANCEL_S_DEPOSIT, "trans.status.cancel.succ.deposit");
		excludedStatuses.put((long)TransactionsManagerBase.TRANS_STATUS_CHARGE_BACK, "trans.status.charge.back"); 
		excludedStatuses.put((long)TransactionsManagerBase.TRANS_STATUS_CANCEL_S_M, "trans.status.cancel.s.m"); 
		excludedStatuses.put((long)TransactionsManagerBase.TRANS_STATUS_CANCEL_M_N_R, "trans.status.cancel.m.n.r"); 
		excludedStatuses.put(new Long(18), "trans.status.partial"); //TRANS_STATUS_PARTIAL 	 
		excludedStatuses.put(new Long(19), "trans.status.cancel.survey"); //TRANS_STATUS_CANCEL_SURVEY
		return excludedStatuses;
	}
	
	private static void updateBalance(Connection con, UserBase user, Transaction t, boolean isPositive) throws SQLException {
		if (isPositive) {
			UsersDAOBase.addToBalance(con, user.getId(), t.getAmount());
		} else {
			UsersDAOBase.addToBalance(con, user.getId(), -t.getAmount());
		}
		GeneralDAO.insertBalanceLog(con, t.getWriterId(), t.getUserId(), ConstantsBase.TABLE_TRANSACTIONS, t.getId(),
				getTrnTypesBalanceMapping().get((int)t.getTypeId()), user.getUtcOffset());
	}
}
