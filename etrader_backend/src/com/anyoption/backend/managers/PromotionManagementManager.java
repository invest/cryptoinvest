package com.anyoption.backend.managers;

import java.awt.image.BufferedImage;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.imageio.ImageIO;

import org.apache.log4j.Logger;

import com.anyoption.backend.daos.PromotionManagementDAO;
import com.anyoption.backend.service.promotionmanagement.PromotionBannerSliderInsertUpdateRequest;
import com.anyoption.backend.service.promotionmanagement.PromotionBannerSliderUpdatePositionRequest;
import com.anyoption.backend.service.promotionmanagement.PromotionBannerSlidersGetAllMethodRequest;
import com.anyoption.backend.service.promotionmanagement.PromotionBannerSlidersGetAllMethodResponse;
import com.anyoption.backend.service.promotionmanagement.PromotionBannerSlidersGetImageResponse;
import com.anyoption.common.bl_vos.PromotionBannerSlider;
import com.anyoption.common.bl_vos.PromotionBannerSliderImage;
import com.anyoption.common.managers.PromotionManagementManagerBase;
import com.anyoption.common.service.CommonJSONService;
import com.anyoption.common.service.results.MethodResult;
import com.anyoption.common.util.CommonUtil;
import com.anyoption.common.util.ConstantsBase;

import il.co.etrader.backend.bl_vos.PromotionBanner;

public class PromotionManagementManager extends PromotionManagementManagerBase{

	private static final Logger logger = Logger.getLogger(PromotionManagementManager.class);
	public static final int HOMEPAGE_SLIDERI_ID = 1;
	
	public static final String FILTER_IS_ACTIVE = "isActive";
	public static final String FILTER_BANNERS = "banners";
	public static final String FILTER_BANNER_SLIDER_TYPES = "bannerSliderTypes";
	
	public static HashMap<Long, String> isActive;
	public static HashMap<Long, String> banners;
	public static HashMap<Long, String> bannerSliderTypes;
	public static HashMap<Long, PromotionBanner> bannerDimensions;

	
	public static HashMap<Long, String> getIsActive() {
		isActive = new HashMap<>();
		isActive.put(1l, "Active");
		isActive.put(0l, "Inactive");
		return isActive;
	}
	
	public static HashMap<Long, String> getBannersList() {
		if (banners == null) {	
			Connection con = null;
			try {
				con = getConnection();
				return PromotionManagementDAO.getBannersList(con);
			} catch (SQLException ex) {
				logger.error("Can't load banners: ", ex);
			} finally {
				closeConnection(con);
			}
		}
		return banners;
	}
	
	public static HashMap<Long, PromotionBanner> getBannerDimensions() {
		if (bannerDimensions == null) {	
			Connection con = null;
			try {
				con = getConnection();
				bannerDimensions = PromotionManagementDAO.getBannerDimensions(con);
				return bannerDimensions;
			} catch (SQLException ex) {
				logger.error("Can't load banners: ", ex);
			} finally {
				closeConnection(con);
			}
		}
		return bannerDimensions;
	}
	
	public static HashMap<Long, String> getBannerSliderTypes() {
		if (bannerSliderTypes == null) {	
			Connection con = null;
			try {
				con = getConnection();
				return PromotionManagementDAO.getBannerSliderTypeList(con);
			} catch (SQLException ex) {
				logger.error("Can't load slider types: ", ex);
			} finally {
				closeConnection(con);
			}
		}
		return bannerSliderTypes;
	}
	
	public static ArrayList<PromotionBannerSlider> getBannerSliders(PromotionBannerSlidersGetAllMethodRequest request, PromotionBannerSlidersGetAllMethodResponse response) {
		Connection conn = null;
		try {
			conn = getConnection();
			return PromotionManagementDAO.getAllSliders(conn, request);
		} catch (SQLException ex) {
			logger.info("Fail to get all bannerSliders due to: ", ex);
			response.setErrorCode(CommonJSONService.ERROR_CODE_FAIL_TO_LOAD_BANNER_SLIDERS); 
			return null;
		} finally {
			closeConnection(conn);
		}
	}
	
	public static void deleteBannerSliders(PromotionBannerSliderInsertUpdateRequest request, MethodResult response) {
		Connection conn = null;
		try {
			conn = getConnection();
			PromotionManagementDAO.deleteBannerSlider(request, conn);
		} catch (SQLException ex) {
			logger.info("Fail to delete bannerSliders due to: ", ex);
			response.setErrorCode(CommonJSONService.ERROR_CODE_FAIL_TO_DELETE_BANNER_SLIDER); 
		} finally {
			closeConnection(conn);
		}
	}
	
	public static void disableBannerSliders(PromotionBannerSliderInsertUpdateRequest request, MethodResult response) {
		Connection conn = null;
		try {
			conn = getConnection();
			PromotionManagementDAO.disableBannerSlider(request, conn);
		} catch (SQLException ex) {
			logger.info("Fail to delete bannerSliders due to: ", ex);
			response.setErrorCode(CommonJSONService.ERROR_CODE_FAIL_TO_DISABLE_BANNER_SLIDER); 
		} finally {
			closeConnection(conn);
		}
	}
	
	public static void enableBannerSliders(PromotionBannerSliderInsertUpdateRequest request, MethodResult response) {
		Connection conn = null;
		try {
			conn = getConnection();
			PromotionManagementDAO.enableBannerSlider(request, conn);
		} catch (SQLException ex) {
			logger.info("Fail to delete bannerSliders due to: ", ex);
			response.setErrorCode(CommonJSONService.ERROR_CODE_FAIL_TO_ENABLE_BANNER_SLIDER); 
		} finally {
			closeConnection(conn);
		}
	}
	
	public static void insertBannerSliderImages(PromotionBannerSliderInsertUpdateRequest request, MethodResult response) {
		Connection conn = null;
		try {
			conn = getConnection();
			conn.setAutoCommit(false);
			
			PromotionBannerSlider bannerSlider = PromotionManagementDAO.insertSlider(request, conn);

			ArrayList<PromotionBannerSliderImage> sliderImages = request.getBannerSliderImages();
			if (sliderImages != null) {
				for(PromotionBannerSliderImage sliderImage : sliderImages){
					sliderImage.setBannerSliderId(bannerSlider.getId());
					sliderImage.setWriterId(request.getWriterId());
					if (!CommonUtil.isParameterEmptyOrNull(sliderImage.getImageUrl()) && !CommonUtil.isParameterEmptyOrNull(sliderImage.getImageName())) {
						PromotionManagementDAO.insertSliderImage(sliderImage, conn);
					} 
					if ((CommonUtil.isParameterEmptyOrNull(sliderImage.getImageUrl()) && !CommonUtil.isParameterEmptyOrNull(sliderImage.getImageName())) ||
							(!CommonUtil.isParameterEmptyOrNull(sliderImage.getImageUrl()) && CommonUtil.isParameterEmptyOrNull(sliderImage.getImageName()))) {
						logger.info("Fail insert image because of empty image url");
						response.setErrorCode(CommonJSONService.ERROR_CODE_MISSING_BANNER_SLIDER_IMAGE_LINK); 
					}	
				}
			}
		} catch (SQLException ex) {
			try {
				conn.rollback();
			} catch (Throwable it) {
				logger.error("Can't rollback.", it);
			}
			logger.info("Fail to insert banner slider due to: ", ex);
			response.setErrorCode(CommonJSONService.ERROR_CODE_FAIL_TO_INSERT_BANNER_SLIDERS); 
		} finally {
			try {
				conn.setAutoCommit(true);
			} catch (SQLException e) {
				logger.error("Can't set back to autocommit.", e);
			}
			closeConnection(conn);
		}
	}
	
	public static void updateBannerSliderImages(PromotionBannerSliderInsertUpdateRequest request, MethodResult response){
		Connection conn = null;
		try {
			conn = getConnection();
			conn.setAutoCommit(false);
			
			PromotionManagementDAO.updateBannerSliderType(request, conn);

			ArrayList<PromotionBannerSliderImage> sliderImages = request.getBannerSliderImages();

			if (sliderImages != null) {
				for(PromotionBannerSliderImage sliderImage : sliderImages){
					if (!CommonUtil.isParameterEmptyOrNull(sliderImage.getImageUrl()) && !CommonUtil.isParameterEmptyOrNull(sliderImage.getImageName())) {
						sliderImage.setWriterId(request.getWriterId());
						if (sliderImage.getId() > 0){
							PromotionManagementDAO.updateSliderImage(sliderImage, conn);
						} else {
							PromotionManagementDAO.insertSliderImage(sliderImage, conn);
						}
					} if ((CommonUtil.isParameterEmptyOrNull(sliderImage.getImageUrl()) && !CommonUtil.isParameterEmptyOrNull(sliderImage.getImageName())) ||
							(!CommonUtil.isParameterEmptyOrNull(sliderImage.getImageUrl()) && CommonUtil.isParameterEmptyOrNull(sliderImage.getImageName()))) {
						logger.info("Fail insert image because of empty image url");
						response.setErrorCode(CommonJSONService.ERROR_CODE_MISSING_BANNER_SLIDER_IMAGE_LINK); 
						break;
					}					
				}
			}
		} catch (SQLException ex) {
			try {
				conn.rollback();
			} catch (Throwable it) {
				logger.error("Can't rollback.", it);
			}
			logger.info("Fail to insert banner slider due to: ", ex);
			response.setErrorCode(CommonJSONService.ERROR_CODE_FAIL_TO_UPDATE_BANNER_SLIDER_IMAGE); 
		} finally {
			try {
				conn.setAutoCommit(true);
			} catch (SQLException e) {
				logger.error("Can't set back to autocommit.", e);
			}
			closeConnection(conn);
		}
	}
	
	public static HashMap<PromotionBannerSlider, ArrayList<PromotionBannerSliderImage>> getBannerSliderImages(PromotionBannerSliderInsertUpdateRequest request, PromotionBannerSlidersGetImageResponse response) {
		Connection conn = null;
		try {
			conn = getConnection();
			HashMap<PromotionBannerSlider, ArrayList<PromotionBannerSliderImage>> hm = PromotionManagementDAO.getAllSliderImages(request, conn);

			return hm;
		} catch (SQLException ex) {
			logger.info("Fail to insert banner slider due to: ", ex);
			response.setErrorCode(CommonJSONService.ERROR_CODE_UNABLE_TO_GET_SLIDER_IMAGES); 
			return null;
		} finally {
			closeConnection(conn);
		}
	}
	
	public static boolean saveFile(String fileName, BufferedInputStream bis, Long bannerId) throws IOException {
		String filePath = CommonUtil.getProperty(ConstantsBase.BANNER_SLIDER_IMAGES_PATH) + "/" + fileName;
		File targetFile = new File(filePath); 
		BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(targetFile), 4096);
		int bt;
		
		while ((bt = bis.read()) != -1) {
			bos.write(bt);
		}

		InputStream in = new FileInputStream(targetFile);
		in.read();
		in.close();
		
		bos.close();
		bis.close();
		
		BufferedImage bimg = ImageIO.read(new File(filePath));
		int width = bimg.getWidth();
		int height = bimg.getHeight();
		if (width != getBannerDimensions().get(bannerId).getWidth() || height != getBannerDimensions().get(bannerId).getHeight()) {
			logger.info("Wrong slider images dimensions");
			targetFile.delete();
			return false;
		}
		
		return true;
	}
	
	public static void deleteImageByName(String fileName) {
		if (fileName == null || fileName.isEmpty()) {
			return;
		}
		File targetFile = new File(CommonUtil.getProperty(ConstantsBase.BANNER_SLIDER_IMAGES_PATH) + fileName);
		boolean result = targetFile.delete();
		if (!result) {
			logger.warn("Couldn't delete file!");
		}
	}
	
	public static boolean validateImageExtension(String fileName) {
		boolean result = false;
		if (fileName != null && !fileName.isEmpty()) {
			Pattern regex = Pattern.compile(ConstantsBase.ALLOWED_SLIDER_IMAGE_EXTENSIONS_REGEX,
	    			Pattern.CANON_EQ);
	    	Matcher matcher = regex.matcher(fileName.toLowerCase());
	    	if (matcher.matches()) {
	    			result=true;
	    	}
		}
    	return result;
	}
	
	public static ArrayList<String> getBannerSliderImagesFromBank(){
		ArrayList<String> imagesFromBank = new ArrayList<String>();
		
		File path = new File(CommonUtil.getProperty(ConstantsBase.BANNER_SLIDER_IMAGES_PATH));
		File[] listOfFiles = path.listFiles();
		
		if (listOfFiles != null && listOfFiles.length > 0) {
			for (File image : listOfFiles){
				if (image.isFile()){
					if (PromotionManagementManager.validateImageExtension(image.getName())) {
						imagesFromBank.add(image.getName());
					}
				}
			}
		}
		return imagesFromBank;
	}
	
	public static void updateBannerSliderPosition(PromotionBannerSliderUpdatePositionRequest request, MethodResult response) {
		Connection conn = null;
		try {
			conn = getConnection();
			conn.setAutoCommit(false);
			ArrayList<Long> sliderOrderedIds = request.getSlideIds();
			Long writerId = request.getWriterId();
			for (Long id : sliderOrderedIds) {
				PromotionManagementDAO.updateBannerSliderPosition(id, sliderOrderedIds.indexOf(id) + 1, writerId, conn);
			}
		} catch (SQLException ex) {
			try {
				conn.rollback();
			} catch (Throwable it) {
				logger.error("Can't rollback.", it);
			}
			logger.info("Fail to update bannerSlider position due to: ", ex);
			response.setErrorCode(CommonJSONService.ERROR_CODE_UNABLE_TO_UPDATE_SLIDER_POSITION); 
		} finally {
			try {
				conn.setAutoCommit(true);
			} catch (SQLException e) {
				logger.error("Can't set back to autocommit.", e);
			}
			closeConnection(conn);
		}
	}


}
