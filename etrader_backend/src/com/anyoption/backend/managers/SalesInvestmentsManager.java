package com.anyoption.backend.managers;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;

import org.apache.log4j.Logger;

import com.anyoption.backend.daos.SalesInvestmentsDAO;
import com.anyoption.backend.service.salesinvestmentsmanager.SalesInvestmentsManagerMethodRequest;
import com.anyoption.backend.service.salesinvestmentsmanager.SalesInvestmentsManagerMethodResponse;
import com.anyoption.backend.service.salesinvestmentsrepresentatives.SalesInvestmentsRepresentativesMethodRequest;
import com.anyoption.backend.service.salesinvestmentsrepresentatives.SalesInvestmentsRepresentativesMethodResponse;
import com.anyoption.common.beans.SalesInvestments;
import com.anyoption.common.managers.BaseBLManager;

/**
 * @author Eyal Goren
 *
 */
public class SalesInvestmentsManager extends BaseBLManager {

	private static final Logger logger = Logger.getLogger(SalesInvestmentsManager.class);
		
	public static ArrayList<SalesInvestments> getSalesInvestmentsRep(SalesInvestmentsRepresentativesMethodRequest request,
			SalesInvestmentsRepresentativesMethodResponse response) throws SQLException {
		Connection conn = null;
		try {			
			conn = getConnection();
			return SalesInvestmentsDAO.getSalesInvestmentsRep(conn, request, response);
		} finally {
			closeConnection(conn);
		}
	}
	
	public static ArrayList<SalesInvestments> getSalesInvestmentsManager(SalesInvestmentsManagerMethodRequest request,
			SalesInvestmentsManagerMethodResponse response) throws SQLException {
		Connection conn = null;
		try {
			conn = getConnection();
			return SalesInvestmentsDAO.getSalesInvestmentsManager(conn, request, response);
		} finally {
			closeConnection(conn);
		}
	}
}
