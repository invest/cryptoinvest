package com.anyoption.backend.managers;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;

import com.anyoption.backend.daos.PhoneConversionCallsHistoryDAO;
import com.anyoption.backend.service.phoneconversioncallshistory.PhoneConversionCallsHistoryMethodRequest;
import com.anyoption.common.managers.BaseBLManager;

public class PhoneConversionCallsHistoryManager extends BaseBLManager {

	public static HashMap<String, Object> getCallsHistory(PhoneConversionCallsHistoryMethodRequest request)
			throws SQLException {
		Connection conn = null;
		try {
			conn = getConnection();
			return PhoneConversionCallsHistoryDAO.getCallsHistory(conn, request);
		} finally {
			closeConnection(conn);
		}
	}
}
