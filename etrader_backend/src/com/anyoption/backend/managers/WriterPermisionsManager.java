package com.anyoption.backend.managers;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import com.anyoption.backend.daos.WriterPermisionsDAO;
import com.anyoption.backend.service.writerpermissions.PermissionsGetResponse;
import com.anyoption.common.managers.WriterPermisionsManagerBase;
import com.anyoption.common.service.CommonJSONService;

import il.co.etrader.backend.bl_vos.ExistWriterRole;
import il.co.etrader.backend.bl_vos.WriterWrapper;
import il.co.etrader.bl_vos.WriterBase;
import il.co.etrader.util.ConstantsBase;

public class WriterPermisionsManager extends WriterPermisionsManagerBase {

	private static final Logger logger = Logger.getLogger(WriterPermisionsManager.class);
	
	private static Map<Long, ArrayList<String>> rolesPermissions;
	
	public static ArrayList<String> getWriterPermissions(long writerId) throws SQLException {
		Connection conn = null;
		try {
			conn = getConnection();
			return WriterPermisionsDAO.getWriterPermissions(conn, writerId);
		} finally {
			closeConnection(conn);
		}
	}
	
	public static void setWriterScreenPermissionsByScreenName(String screenName, PermissionsGetResponse response, HttpServletRequest httpRequest) {
		
		WriterWrapper writer = (WriterWrapper) httpRequest.getSession().getAttribute(ConstantsBase.BIND_SESSION_WRITER_WRAPPER);
		long writerId = writer.getWriter().getId();
		logger.debug("Get setings for Writer with id: " + writerId);

		ArrayList<String> permissionsList = null;
		Connection conn = null;
		try {
			conn = getConnection();
			permissionsList = WriterPermisionsDAO.getWriterScreenPermissions(conn, writerId, screenName);
			response.setPermissionsList(permissionsList);
		} catch (SQLException ex) {
			logger.error("Unable to set writer permisions screen settings due to: ", ex);
			response.setErrorCode(CommonJSONService.ERROR_CODE_UNKNOWN);
		} finally {
			closeConnection(conn);
		}
	}
	
	public static ArrayList<String> getWriterScreenPermissions(String screenName, HttpServletRequest httpRequest) throws SQLException {
		WriterWrapper writer = (WriterWrapper) httpRequest.getSession().getAttribute(ConstantsBase.BIND_SESSION_WRITER_WRAPPER);
		long writerId = writer.getWriter().getId();
		logger.debug("Get setings for Writer with id: " + writerId);
		
		Connection conn = null;
		try {
			conn = getConnection();
			return WriterPermisionsDAO.getWriterScreenPermissions(conn, writerId, screenName);
		} finally {
			closeConnection(conn);
		}
	}
	
	public static HashMap<Long, String> getRoles() throws SQLException {
		Connection conn = null;
		try {
			conn = getConnection();
			return WriterPermisionsDAO.getRoles(conn);
		} finally {
			closeConnection(conn);
		}
	}
	
	public static ArrayList<WriterBase> getDepartmentWriters(Long roleId) throws SQLException {
		Connection conn = null;
		try {
			conn = getConnection();
			return WriterPermisionsDAO.getDepartmentWriters(conn, roleId);
		} finally {
			closeConnection(conn);
		}
	}
	
	public static ArrayList<String> getRolePermissions(long roleId) throws SQLException {
		Connection conn = null;
		try {
			conn = getConnection();
			return WriterPermisionsDAO.getRolePermissions(conn, roleId);
		} finally {
			closeConnection(conn);
		}
	}
	
	public static ArrayList<ExistWriterRole> editRolePermissions(long roleId, String roleName, ArrayList<String> permissions, ArrayList<Long> writers, ArrayList<Long> unsetWriters) throws SQLException {
		Connection conn = null;
		ArrayList<ExistWriterRole> res = new ArrayList<>();
		try {
			conn = getConnection();
			conn.setAutoCommit(false);
			
			WriterPermisionsDAO.updateRoleName(conn, roleId, roleName);
			if(permissions != null){
				WriterPermisionsDAO.insertRolePermission(conn, roleId, permissions);
			}
			if(unsetWriters != null){
				WriterPermisionsDAO.unsetWritersRole(conn, unsetWriters);
			}
			if(writers != null){
				res = WriterPermisionsDAO.setWritersRole(conn, roleId, writers);
			}
			
			conn.commit();
			//Refresh cache
			loadRolePermission();
		} catch (SQLException e) {
			logger.error("Exception on Edit Role Permissions!!! " + e);
			try {
				conn.rollback();
			} catch (SQLException ie) {
				logger.log(Level.ERROR, "Can't rollback.", ie);
			}
			throw e;	
		} finally {;
        	conn.setAutoCommit(true);
            closeConnection(conn);
		}
		
		return res;
	}
	
	public static ArrayList<ExistWriterRole> addRolePermissions(String roleName, ArrayList<String> permissions, ArrayList<Long> writers) throws SQLException {
		Connection conn = null;
		ArrayList<ExistWriterRole> res = new ArrayList<>();
		long roleId = 0;
		try {
			conn = getConnection();
			conn.setAutoCommit(false);
			
			roleId = WriterPermisionsDAO.insertRole(conn, roleName);
			if(permissions != null){
				WriterPermisionsDAO.insertRolePermission(conn, roleId, permissions);
			}
			if(writers != null){
				res = WriterPermisionsDAO.setWritersRole(conn, roleId, writers);
			}
			
			conn.commit();
			//Refresh cache
			loadRolePermission();
		} catch (SQLException e) {
			logger.error("Exception on Add Role Permissions!!! " + e);
			try {
				conn.rollback();
			} catch (SQLException ie) {
				logger.log(Level.ERROR, "Can't rollback.", ie);
			}
			throw e;	
		} finally {;
        	conn.setAutoCommit(true);
            closeConnection(conn);
		}
		
		return res;
	}

	public static Map<Long, ArrayList<String>> getRolesPermissions() {
		if (rolesPermissions == null){
			loadRolePermission();
		}		
		return rolesPermissions;
	}

	private static void loadRolePermission() {
		if(rolesPermissions != null){
			rolesPermissions.clear();
		}		
		Connection conn = null;
		try {
			conn = getConnection();
			rolesPermissions = WriterPermisionsDAO.getAllRolePermissions(conn);
		} catch (Exception e) {
			logger.error("Can't load rolesPermissions", e);
		} finally {
			closeConnection(conn);
		}
	}
	
	public static boolean isExistPermission(long roleId, String permmission){
		logger.debug("isExistPermission for roleId:" + roleId + " and permmission:" + permmission);
		boolean res = false;
		if (WriterPermisionsManager.getRolesPermissions().get(roleId) != null &&
				WriterPermisionsManager.getRolesPermissions().get(roleId).contains(permmission)) {
			res = true;
		}
		if(!res){
			logger.debug("NOT permission for roleId:" + roleId + " and permmission:" + permmission
					+ " and WriterPermisionsManager.getRolesPermissions():.. ");
		}
		return res;
	}
	
	public static HashMap<Long, String> filterSkinsBasedOnWriterAttribution(HashMap<Long, String> skinsMap, WriterWrapper writer, Logger log) {
		if (writer == null) {
			log.error("Writer object can NOT be found in session");
			return skinsMap;
		}

		if (skinsMap.isEmpty()) {
			log.error("Skins Map filter is empty");
			return skinsMap;
		}
		
		HashMap<Long, String> newSkinsMap = new HashMap<>();
		for (Integer i : writer.getSkins()) {
			if (skinsMap.containsKey(i.longValue())) {
				newSkinsMap.put(i.longValue(), skinsMap.get(i.longValue()));
			}
		}
		return newSkinsMap;
	}
	
	/**
	 * This method checks writer skins from request. This is needed in case no skin is selected from filter,
	 * the skin parameter is null or empty and every query with skin filter is searching for ALL skins,
	 * which is incorrect. In Backend we need to search only for users with skin which the specific writer
	 * is allowed to see. It also checks the request for unauthorized skins. 
	 * @param skinsList The skins from the request
	 * @param writer The writer from the session
	 * @return writerSkins The correct skins for query filtering
	 */
	public static ArrayList<Integer> setPermitedSkinsForWriter(ArrayList<Integer> skinsList, WriterWrapper writer) {
		ArrayList<Integer> writerSkins = new ArrayList<Integer>();
		if (null == skinsList || skinsList.isEmpty()) {
			writerSkins = writer.getSkins();
		} else {
			for(Integer i : skinsList) {
				if (writer.getSkins().contains(i)) {
					writerSkins.add(i);
				}
			}
		}
		return writerSkins;
	}
}
