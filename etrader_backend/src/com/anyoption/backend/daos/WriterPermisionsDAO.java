package com.anyoption.backend.daos;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;

import com.anyoption.common.daos.WriterPermisionsDAOBase;

import il.co.etrader.backend.bl_vos.ExistWriterRole;
import il.co.etrader.bl_vos.WriterBase;
import oracle.jdbc.OracleTypes;

public class WriterPermisionsDAO extends WriterPermisionsDAOBase {

	private static final Logger logger = Logger.getLogger(WriterPermisionsDAO.class);

	public static ArrayList<String> getWriterPermissions(Connection conn, long writerId) throws SQLException {
		ArrayList<String> writersPermissionsList = new ArrayList<>();
		CallableStatement cstmt = null;
		ResultSet rs = null;
		int index = 1;
		try {
			String sql = "{call pkg_writers.get_writer_screens(?, ?)}";
			cstmt = conn.prepareCall(sql);
			cstmt.registerOutParameter(index++, OracleTypes.CURSOR);
			cstmt.setLong(index++, writerId);
			cstmt.executeQuery();
			
			rs = (ResultSet) cstmt.getObject(1);
			while (rs.next()) {
				writersPermissionsList.add(rs.getString("screenname") + "_" + rs.getString("permission"));
			}
		} finally {
			closeStatement(cstmt);
		}
		return writersPermissionsList;
	}
	
	
	public static HashMap<Long, String> getRoles(Connection conn) throws SQLException {
		HashMap<Long, String> rolesHM = new HashMap<Long, String>();
		CallableStatement cstmt = null;
		ResultSet rs = null;
		int index = 1;
		try {
			String sql = "{call pkg_writers.get_roles(?)}";
			cstmt = conn.prepareCall(sql);
			cstmt.registerOutParameter(index++, OracleTypes.CURSOR);
			cstmt.executeQuery();
			
			rs = (ResultSet) cstmt.getObject(1);
			while (rs.next()) {
				rolesHM.put(rs.getLong("id"), rs.getString("rolename"));
			}
		} finally {
			closeStatement(cstmt);
		}
		return rolesHM;
	}
	
	
	public static ArrayList<WriterBase> getDepartmentWriters(Connection conn, Long roleId) throws SQLException {
		ArrayList<WriterBase> writerList = new ArrayList<>();
		CallableStatement cstmt = null;
		ResultSet rs = null;
		int index = 1;
		try {			
			String sql = "{call pkg_writers.get_department_writers(?)}";
			if(roleId != null){
				sql = "{call pkg_writers.get_department_writers(?, ?)}";
			}
			cstmt = conn.prepareCall(sql);
			cstmt.registerOutParameter(index++, OracleTypes.CURSOR);
			if(roleId != null){
				cstmt.setLong(index++, roleId);
			}
			cstmt.executeQuery();
			
			rs = (ResultSet) cstmt.getObject(1);
			while (rs.next()) {				
				WriterBase w = new WriterBase();
				w.setId(rs.getLong("writer_id"));
				w.setUserName(rs.getString("user_name"));
				w.setDeptId(rs.getLong("dept_id"));
				w.setDepartmentName(rs.getString("dept_name"));
				w.setMultitude(rs.getBoolean("multitude"));	
				w.setRoleId(rs.getLong("role_id"));
				
				writerList.add(w);				
			}
		} finally {
			closeStatement(cstmt);
		}
		return writerList;
	}
	
	public static ArrayList<String> getRolePermissions(Connection conn, long roleId) throws SQLException {
		ArrayList<String> rolePermissionsList = new ArrayList<>();
		CallableStatement cstmt = null;
		ResultSet rs = null;
		int index = 1;
		try {
			String sql = "{call pkg_writers.get_role_permissions(?, ?)}";
			cstmt = conn.prepareCall(sql);
			cstmt.registerOutParameter(index++, OracleTypes.CURSOR);
			cstmt.setLong(index++, roleId);
			cstmt.executeQuery();
			
			rs = (ResultSet) cstmt.getObject(1);
			while (rs.next()) {
				rolePermissionsList.add(rs.getString("screenname") + "_" + rs.getString("permission"));
			}
		} finally {
			closeStatement(cstmt);
		}
		return rolePermissionsList;
	}
	
	public static Map<Long, ArrayList<String>> getAllRolePermissions(Connection conn) throws SQLException {
		Map<Long, ArrayList<String>> hm = new HashMap<>();
		CallableStatement cstmt = null;
		ResultSet rs = null;
		int index = 1;
		try {
			String sql = "{call pkg_writers.get_role_permissions(?)}";
			cstmt = conn.prepareCall(sql);
			cstmt.registerOutParameter(index++, OracleTypes.CURSOR);
			cstmt.executeQuery();
			
			rs = (ResultSet) cstmt.getObject(1);
			while (rs.next()) {
				long roleId = rs.getLong("role_id");
				String perm = rs.getString("screenname") + "_" + rs.getString("permission");
				if (hm.get(roleId) == null) {
					hm.put(roleId, new ArrayList<String>());
				}
				ArrayList<String> arr = hm.get(roleId);
				arr.add(perm);
				hm.put(roleId, arr);				
			}
		} finally {
			closeStatement(cstmt);
		}
		return hm;
	}
	
	public static void updateRoleName(Connection conn, long roleId, String roleName) throws SQLException {
		CallableStatement cstmt = null;
		int index = 1;
		try {
			String sql = "{call pkg_writers.update_role(?, ?)}";
			cstmt = conn.prepareCall(sql);
			cstmt.setLong(index++, roleId);
			cstmt.setString(index++, roleName);
			cstmt.executeQuery();
			logger.debug("The roleId:" + roleId + " was updated name with:" + roleName);
		} finally {
			closeStatement(cstmt);
		}
	}
		
		public static void insertRolePermission(Connection conn, long roleId, ArrayList<String> permissionList) throws SQLException {
			CallableStatement cstmt = null;
			
			if(permissionList.size() > 0){				
				int index = 1;
				ArrayList<String> screnList = new ArrayList<>();
				ArrayList<String> permList = new ArrayList<>();
				
				for (String pls : permissionList) {
					String[] parts = pls.split("_");
					screnList.add(parts[0]);
					permList.add(parts[1]);
				}
				
				try {
					String sql = "{call pkg_writers.update_role_permissions(?, ?, ?)}";
					cstmt = conn.prepareCall(sql);
					cstmt.setArray(index++, getPreparedSqlArray(conn, screnList));
					cstmt.setArray(index++, getPreparedSqlArray(conn, permList));
					cstmt.setLong(index++, roleId);
					cstmt.executeQuery();
					logger.debug("The roleId:" + roleId + " was inserted  with permissions:" + permissionList);				
				} finally {
					closeStatement(cstmt);
				}
			}
	}
		
		public static void deleteRolePermission(Connection conn, long roleId, ArrayList<String> permissionList) throws SQLException {
			CallableStatement cstmt = null;
			
			if(permissionList.size() > 0){				
				int index = 1;
				ArrayList<String> screnList = new ArrayList<>();
				ArrayList<String> permList = new ArrayList<>();
				
				for (String pls : permissionList) {
					String[] parts = pls.split("_");
					screnList.add(parts[0]);
					permList.add(parts[1]);
				}
				
				try {
					String sql = "{call pkg_writers.delete_role_permission(?, ?, ?)}";
					cstmt = conn.prepareCall(sql);
					cstmt.setArray(index++, getPreparedSqlArray(conn, screnList));
					cstmt.setArray(index++, getPreparedSqlArray(conn, permList));
					cstmt.setLong(index++, roleId);
					cstmt.executeQuery();
					logger.debug("The roleId:" + roleId + " was deleted  with permissions:" + permissionList);				
				} finally {
					closeStatement(cstmt);
				}
			}
	}
		
		public static void unsetWritersRole(Connection conn, ArrayList<Long> writers) throws SQLException {
			CallableStatement cstmt = null;
			int index = 1;
			try {
				String sql = "{call pkg_writers.unset_writers_role(?)}";
				cstmt = conn.prepareCall(sql);
				cstmt.setArray(index++, getPreparedSqlArray(conn, writers));
				cstmt.executeQuery();
				logger.debug("The writers:" + writers + " was unset");
			} finally {
				closeStatement(cstmt);
			}
		}
		
		
		public static ArrayList<ExistWriterRole> setWritersRole(Connection conn, long roleId, ArrayList<Long> writers) throws SQLException {
			CallableStatement cstmt = null;
			ResultSet rs = null;
			int index = 1;			
			ArrayList<ExistWriterRole> res = new ArrayList<>();
			try {
				String sql = "{call pkg_writers.set_writers_role(?, ?, ?)}";
				cstmt = conn.prepareCall(sql);
				cstmt.registerOutParameter(index++, OracleTypes.CURSOR);
				cstmt.setArray(index++, getPreparedSqlArray(conn, writers));
				cstmt.setLong(index++, roleId);
				cstmt.executeQuery();
				
				rs = (ResultSet) cstmt.getObject(1);
				while (rs.next()) {
					res.add(new ExistWriterRole(rs.getString("user_name"), rs.getString("dept_name"), rs.getString("rolename")));
				}				
				logger.debug("The roleId:" + roleId + " was set writers where  without:" + res);
			} finally {
				closeStatement(cstmt);
			}
			
			return res;
		}
		
		public static long insertRole(Connection conn, String roleName) throws SQLException {
			CallableStatement cstmt = null;
			int index = 1;
			long roleId = 0;
			try {
				String sql = "{call pkg_writers.create_role(?, ?)}";
				cstmt = conn.prepareCall(sql);
				cstmt.registerOutParameter(index++, Types.INTEGER);
				cstmt.setString(index++, roleName);
				cstmt.executeQuery();				
				
				roleId = cstmt.getInt(1);	
				
				logger.debug("The roleId:" + roleId + " was inserted with name:" + roleName);
			} finally {
				closeStatement(cstmt);
			}			
			return roleId;
		}
}
