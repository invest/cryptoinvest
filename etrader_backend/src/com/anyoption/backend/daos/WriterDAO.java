/**
 * 
 */
package com.anyoption.backend.daos;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;

import org.apache.log4j.Logger;

import com.anyoption.backend.managers.WriterManager;
import com.anyoption.backend.service.writer.WriterGetAllMethodRequest;
import com.anyoption.backend.service.writer.WriterGetAllMethodResponse;
import com.anyoption.backend.service.writer.WriterInsertMethodRequest;
import com.anyoption.backend.service.writer.WriterInsertMethodResponse;
import com.anyoption.backend.service.writer.WriterResetPasswordMethodRequest;
import com.anyoption.backend.service.writer.WriterUpdateMethodRequest;
import com.anyoption.common.beans.Writer;
import com.anyoption.common.daos.DAOBase;

import oracle.jdbc.OracleTypes;

/**
 * @author pavel.tabakov
 *
 */
public class WriterDAO extends DAOBase {

	private static final Logger logger = Logger.getLogger(WriterDAO.class);
	
	public static HashMap<String, HashMap<Long, String>> getFilters(Connection conn) throws SQLException {
		HashMap<String, HashMap<Long, String>> filtersList = new HashMap<>();
		CallableStatement cstmt = null;
		ResultSet rs = null;
		int index = 1;
		try {
			cstmt = conn.prepareCall("{call pkg_writers.load_filters(o_departments => ? "+
																   ",o_writers_site => ?)}");
			
			cstmt.registerOutParameter(index++, OracleTypes.CURSOR);
			cstmt.registerOutParameter(index++, OracleTypes.CURSOR);
			
			cstmt.executeQuery();
			
			TransactionDAO.loadFilters(rs, cstmt, filtersList, WriterManager.filterConstants);
			
		} finally {
			closeStatement(cstmt);
			closeResultSet(rs);
		}
		return filtersList;
	}
	
	public static ArrayList<Writer> getAll(Connection conn, WriterGetAllMethodRequest request, WriterGetAllMethodResponse response) throws SQLException {
		ArrayList<Writer> writersList = new ArrayList<>();
		CallableStatement cstmt = null;
		ResultSet rs = null;
		int index = 1;
		long rowsPerPage = 20;
		try {
			cstmt = conn.prepareCall("{call pkg_writers.get_writers(o_writers => ? "+
																   ",i_page_number => ? "+
																   ",i_rows_per_page => ? "+
																   ",i_departments => ? "+
																   ",i_roles => ? "+
																   ",i_skins => ? "+
																   ",i_text_filter => ? "+
																   ",i_is_active => ? " + 
																   ",i_writer_id => ? " +
																   ",i_site => ?)}");
			cstmt.registerOutParameter(index++, OracleTypes.CURSOR);
			
			if (request.getPage() == null) {
				cstmt.setLong(index++, 1);
			} else {
				cstmt.setLong(index++, request.getPage());
			}
			
			
			cstmt.setLong(index++, rowsPerPage);
			
			TransactionDAO.setArrayVal(request.getDepartments(), index++, cstmt, conn);
			TransactionDAO.setArrayVal(request.getRoles(), index++, cstmt, conn);
			TransactionDAO.setArrayVal(request.getSkins(), index++, cstmt, conn);
			
			cstmt.setString(index++, request.getFreeTextFilter());
			
			setStatementValue(request.getIsActive(), index++, cstmt);
			
			if (request.getWriterId() <= 0) {
				cstmt.setNull(index++, OracleTypes.INTEGER);
			} else {
				cstmt.setLong(index++, request.getWriterId());
			}
			
			TransactionDAO.setArrayVal(request.getSite(), index++, cstmt, conn);
			
			cstmt.executeQuery();
			rs = (ResultSet) cstmt.getObject(1);
			int count = 0;
			while (rs.next()) {
				if(count == 0){
				 response.setTotalCount(rs.getLong("total_count"));
				 count ++;
				}
				Writer vo = new Writer();
				loadWriter(rs, vo);
				writersList.add(vo);
			}
		} finally {
			closeStatement(cstmt);
			closeResultSet(rs);
		}
		return writersList;
	}

	public static void insert(WriterInsertMethodRequest request, Connection conn, String randomPass, WriterInsertMethodResponse response) throws SQLException {
		CallableStatement cstmt = null;
		int index = 1;
		try {
			cstmt = conn.prepareCall("{call pkg_writers.create_writer(o_writer_id => ? "+
																	  ",i_user_name => ? "+
																	  ",i_first_name => ? "+
																	  ",i_last_name => ? "+
																	  ",i_email => ? "+
																	  ",i_mobile_phone => ? "+
																	  ",i_dept_id => ? "+
																	  ",i_nick_name_first => ? "+
																	  ",i_nick_name_last => ? "+
																	  ",i_emp_id => ? "+
																	  ",i_role_id => ? "+
																	  ",i_password => ? "+
																	  ",i_skins => ? " +
																	  ",i_sales_type => ? " +
																	  ",i_site => ? )}");
			cstmt.registerOutParameter(index++, OracleTypes.INTEGER);
			cstmt.setString(index++, request.getUserName());// must
			cstmt.setString(index++, request.getFirstName());// must
			cstmt.setString(index++, request.getLastName());// must
			cstmt.setString(index++, request.getEmail());// must
			
			setStatementValue(request.getMobilePhone(), index++, cstmt);
			
			cstmt.setLong(index++, request.getDepartmentId());// must
			
			setStatementValue(request.getNickNameFirst(), index++, cstmt);
			setStatementValue(request.getNickNameLast(), index++, cstmt);
			
			setStatementValue(request.getEmployeeId(), index++, cstmt);
			setStatementValue(request.getRoleId(), index++, cstmt);

			cstmt.setString(index++, randomPass);
			
			TransactionDAO.setArrayVal(request.getSkins(), index++, cstmt, conn);
			setStatementValue(request.getSalesType(), index++, cstmt);
			setStatementValue(request.getSite(), index++, cstmt);
			
			cstmt.execute();

			response.setWriterId(cstmt.getLong(1));

			
		} finally {
			closeStatement(cstmt);
		}
	}

	public static void update(WriterUpdateMethodRequest request, Connection conn) throws SQLException {
		CallableStatement cstmt = null;
		int index = 1;
		try {
			cstmt = conn.prepareCall("{call pkg_writers.update_writer(i_writer_id => ? "+
																	  ",i_first_name => ? "+
																	  ",i_last_name => ? "+
																	  ",i_email => ? "+
																	  ",i_mobile_phone => ? "+
																	  ",i_nick_name_first => ? "+
																	  ",i_nick_name_last => ? "+
																	  ",i_emp_id => ? "+
																	  ",i_dept_id => ? "+
																	  ",i_role_id => ? "+
																	  ",i_is_active => ? "+
																	  ",i_skins => ?" +
																	  ",i_sales_type => ? " +
																	  ",i_site => ?)}");
			
			cstmt.setLong(index++, request.getId());// must
			
			setStatementValue(request.getFirstName(), index++, cstmt);
			setStatementValue(request.getLastName(), index++, cstmt);
			setStatementValue(request.getEmail(), index++, cstmt);
			setStatementValue(request.getMobilePhone(), index++, cstmt);
			setStatementValue(request.getNickNameFirst(), index++, cstmt);
			setStatementValue(request.getNickNameLast(), index++, cstmt);
			
			setStatementValue(request.getEmployeeId(), index++, cstmt);
			setStatementValue(request.getDepartmentId(), index++, cstmt);
			setStatementValue(request.getRoleId(), index++, cstmt);
			setStatementValue(request.getIsActive(), index++, cstmt);
			
			TransactionDAO.setArrayVal(request.getSkins(), index++, cstmt, conn);
			setStatementValue(request.getSalesType(), index++, cstmt);
			setStatementValue(request.getSite(), index++, cstmt);
			
			cstmt.executeUpdate();
			
		} finally {
			closeStatement(cstmt);
		}
	}

	public static ArrayList<String> getUsernamesToCompare(Connection conn, String username) throws SQLException {
		ArrayList<String> writersUsernameList = new ArrayList<>();
		CallableStatement cstmt = null;
		ResultSet rs = null;
		int index = 1;
		try {
			cstmt = conn.prepareCall("{call pkg_writers.get_existing_usernames(o_usernames => ? "+
																			  ",i_lookup_string => ?)}");
			cstmt.registerOutParameter(index++, OracleTypes.CURSOR);
			cstmt.setString(index++, username);
			cstmt.executeQuery();
			
			rs = (ResultSet) cstmt.getObject(1);
			while (rs.next()) {
				writersUsernameList.add(rs.getString("user_name"));
			}
		} finally {
			closeStatement(cstmt);
			closeResultSet(rs);
		}
		return writersUsernameList;
	}
	
	public static ArrayList<Integer> getWriterSkins(Connection conn, long writerId) throws SQLException {
		ArrayList<Integer> skinList = new ArrayList<>();
		CallableStatement cstmt = null;
		ResultSet rs = null;
		int index = 1;
		try {
			String sql = "{call pkg_writers.get_writers(o_writers => ? " +
													  ",i_writer_id => ?)}";
			cstmt = conn.prepareCall(sql);
			cstmt.registerOutParameter(index++, OracleTypes.CURSOR);
			cstmt.setLong(index++, writerId);
			cstmt.executeQuery();

			rs = (ResultSet) cstmt.getObject(1);
			if (rs.next()) {
				ResultSet result = (ResultSet) rs.getObject("skins");
				while (result.next()) {
					skinList.add(result.getInt("id"));
				}
				closeResultSet(result);
			}
		} finally {
			closeStatement(cstmt);
			closeResultSet(rs);
		}
		return skinList;
	}
	
	private static void loadWriter(ResultSet rs, Writer writer) throws SQLException {
		writer.setId(rs.getLong("writer_id"));
		writer.setUserName(rs.getString("user_name"));
		writer.setFirstName(rs.getString("first_name"));
		writer.setLastName(rs.getString("last_name"));
		writer.setDepartmentName(rs.getString("dept_name"));
		writer.setDepartmentId(rs.getLong("dept_id"));
		writer.setEmployeeId(rs.getLong("emp_id"));
		ArrayList<Integer> skins = new ArrayList<>();
		ResultSet result = (ResultSet) rs.getObject("skins");
		while (result.next()) {
			skins.add(result.getInt("id"));
		}
		writer.setSkins(skins);
		closeResultSet(result);
		writer.setEmail(rs.getString("email"));
		writer.setMobilePhone(rs.getString("mobile_phone"));
		writer.setIsActive(rs.getLong("is_active"));
		writer.setNickNameFirst(rs.getString("nick_name_first"));
		writer.setNickNameLast(rs.getString("nick_name_last"));
        writer.setRoleName(rs.getString("rolename"));
        writer.setRoleId(rs.getLong("role_id"));
        writer.setSalesType(rs.getInt("sales_type"));
        writer.setSite(rs.getInt("site"));
	}
	
	public static HashMap<Long, String> getWriters(Connection conn, long writerId) throws SQLException {
		HashMap<Long, String> writers = new HashMap<Long, String>();

		CallableStatement cstmt = null;
		ResultSet rs = null;
		int index = 1;
		try {			   
			cstmt = conn.prepareCall("{call pkg_writers_legacy.get_retention_writers(o_writers => ? " + 
																					",i_writer_id => ?)}");			
			cstmt.registerOutParameter(index++, OracleTypes.CURSOR);			
			cstmt.setLong(index++, writerId);
						
			cstmt.executeQuery();
			rs = (ResultSet) cstmt.getObject(1);			

			while (rs.next()) {
				writers.put(rs.getLong("id"), rs.getString("user_name"));
			}
		} finally {
			closeStatement(cstmt);
			closeResultSet(rs);
		}
		return writers;
	}
	
	public static Writer isWriterExists(Connection conn, WriterResetPasswordMethodRequest request) throws SQLException {
		Writer wr = null;
		CallableStatement cstmt = null;
		ResultSet rs = null;
		int index = 1;
		try {			   
			cstmt = conn.prepareCall("{call pkg_writers.get_writer_by_username(o_writer => ? " + 
																			  ",i_username => ? "+
																			  ",i_email => ?)}");			
			cstmt.registerOutParameter(index++, OracleTypes.CURSOR);			
			cstmt.setString(index++, request.getUserName());
			cstmt.setString(index, request.getEmail());
						
			cstmt.executeQuery();
			rs = (ResultSet) cstmt.getObject(1);			

			if (rs.next()) {
				wr = new Writer();
				wr.setId(rs.getLong("id"));
				wr.setUserName(rs.getString("user_name"));
				wr.setIsActive(rs.getLong("is_active"));
				wr.setFailedCount(rs.getInt("failed_count"));
				wr.setEmail(rs.getString("email"));
			}
		} finally {
			closeStatement(cstmt);
			closeResultSet(rs);
		}
		return wr;
	}
	
	public static void resetWriterPassAndFailCount(long writerId, Connection conn, String pass) throws SQLException {
		CallableStatement cstmt = null;
		int index = 1;
		try {
			cstmt = conn.prepareCall("{call pkg_writers.reset_writer_pass_fail_count(i_writer_id => ?" +
																				    ",i_password => ?)}");

			cstmt.setLong(index++, writerId);
			cstmt.setString(index, pass);

			cstmt.executeUpdate();

		} finally {
			closeStatement(cstmt);
		}
	}

}
