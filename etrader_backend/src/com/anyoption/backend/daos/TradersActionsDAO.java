package com.anyoption.backend.daos;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import org.apache.log4j.Logger;

import com.anyoption.backend.managers.TradersActionsManager;
import com.anyoption.backend.service.tradersactions.TradersActionsMethodRequest;
import com.anyoption.common.daos.DAOBase;

import il.co.etrader.backend.bl_vos.TradersActions;
import il.co.etrader.util.CommonUtil;
import oracle.jdbc.OracleTypes;

public class TradersActionsDAO extends DAOBase {
	
	private static final Logger logger = Logger.getLogger(TradersActionsDAO.class);

	private static final long ONE_DAY =  86400000; //one day in milis
	
	public static HashMap<String, HashMap<Long, String>> getFilters(Connection conn) throws SQLException {
		logger.info("Traders Actions DAO getFilters");
		HashMap<String, HashMap<Long, String>> filtersList = new HashMap<>();
		CallableStatement cstmt = null;
		ResultSet rs = null;
		int index = 1;
		try {
			cstmt = conn.prepareCall("{call pkg_traders_actions.load_filters(o_traders => ? " +
																   			",o_actions => ?)}");
			cstmt.registerOutParameter(index++, OracleTypes.CURSOR);
			cstmt.registerOutParameter(index++, OracleTypes.CURSOR);
			
			cstmt.executeQuery();
			
			TransactionDAO.loadFilters(rs, cstmt, filtersList, TradersActionsManager.filterConstants);
			
		} finally {
			closeStatement(cstmt);
			closeResultSet(rs);
		}
		return filtersList;
	}
	
	public static ArrayList<TradersActions> getTradersActions(Connection conn, ArrayList<Integer> commands,
			TradersActionsMethodRequest request) throws SQLException {
		logger.info("Traders Actions DAO getTradersActions");
		ArrayList<TradersActions> tradersActionsList = new ArrayList<TradersActions>();
		CallableStatement cstmt = null;
		ResultSet rs = null;
		int index = 1;
		try {			   
			cstmt = conn.prepareCall("{call pkg_traders_actions.get_traders_actions(o_traders_actions => ? " +
																   ",i_fromdate => ? " +
																   ",i_todate => ? " +
																   ",i_trader_id => ? " +
																   ",i_action_command_id => ? " +  
																   ",i_asset_id => ? " + 
																   ",i_page_number => ? " + 
																   ",i_rows_per_page => ? " +
																   ",i_order_column => ? " +
																   ",i_order_type => ? " +
																   ",i_commands => ?)}");
			cstmt.registerOutParameter(index++, OracleTypes.CURSOR);
			cstmt.setTimestamp(index++, new Timestamp(request.getFromDate()));
			cstmt.setTimestamp(index++, new Timestamp(request.getToDate() + ONE_DAY));
			setStatementValue(request.getTraderId(), index++, cstmt);
			setStatementValue(request.getActionId(), index++, cstmt);
			setStatementValue(request.getAssetId(), index++, cstmt);
		    cstmt.setLong(index++, request.getPageNumber());
			cstmt.setLong(index++, request.getRowsPerPage());
			
			if (request.getColumn() == 0) {
				cstmt.setLong(index++, 1);
			} else {
				cstmt.setLong(index++, request.getColumn());
			}
			
			if (request.getOrderType() == 0) {
				cstmt.setLong(index++, 1);
			} else {
				cstmt.setLong(index++, request.getOrderType());
			}
			TransactionDAO.setArrayVal(commands, index++, cstmt, conn);
			
			cstmt.execute();
			
			rs = (ResultSet) cstmt.getObject(1);
			while (rs.next()) {
				TradersActions traderAction = new TradersActions();
				String action = null;
				String asset = null;
				traderAction.setTraderId(rs.getLong("trader_id"));
				traderAction.setTraderName(CommonUtil.getWriterName(traderAction.getTraderId()));
				action = rs.getString("action");
				String[] items = action.split(":");
				List<String> itemList = Arrays.asList(items);
				traderAction.setAction(itemList.get(1));
				traderAction.setActionDate(convertToDate(rs.getTimestamp("time_created")).getTime());
				traderAction.setTotalCount(rs.getLong("total_count"));
				asset = rs.getString("table_name");
				if (asset.equalsIgnoreCase("markets")) {
					asset = CommonUtil.getMarketName(rs.getLong("asset_id"));
				} 
				if (asset.equalsIgnoreCase("opportunities")) {
					asset = "Opportunity " + rs.getString("name");
				}
				if (asset.equalsIgnoreCase("investment_limits")) {
					asset = CommonUtil.getMarketName(rs.getLong("asset_id"));
				} 
				traderAction.setAssetType(asset);
				tradersActionsList.add(traderAction);
			}
		} finally {
			closeStatement(cstmt);
			closeResultSet(rs);
		}
		
		return tradersActionsList;
	}
}
