/**
 * 
 */
package com.anyoption.backend.daos;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.log4j.Logger;

import com.anyoption.backend.managers.TransactionManager;
import com.anyoption.backend.service.transaction.CryptoDeposits;
import com.anyoption.backend.service.transaction.GetAllCryptoDepositsResponse;
import com.anyoption.backend.service.transaction.GetAllCryptoDepsitsRequest;
import com.anyoption.backend.service.transaction.TransactionGetAllMethodRequest;
import com.anyoption.backend.service.transaction.TransactionGetAllMethodResponse;
import com.anyoption.backend.service.transaction.TransactionPostponedRequest;
import com.anyoption.backend.service.transaction.TransactionReroutingGetResponse;
import com.anyoption.backend.service.updatetransaction.UpdateTransactionMethodResponse;
import com.anyoption.backend.service.withdraw.WithdrawInsertMethodRequest;
import com.anyoption.common.beans.Bins;
import com.anyoption.common.beans.CreditCard;
import com.anyoption.common.beans.SpecialCare;
import com.anyoption.common.beans.Transaction;
import com.anyoption.common.beans.TransactionPostponed;
import com.anyoption.common.bl_vos.WireBase;
import com.anyoption.common.daos.DAOBase;
import com.anyoption.common.managers.TransactionsManagerBase;
import com.anyoption.common.util.AESUtil;

import il.co.etrader.backend.util.Constants;
import il.co.etrader.util.CommonUtil;
import oracle.jdbc.OracleTypes;

/**
 * @author pavel.tabakov
 *
 */
public class TransactionDAO extends DAOBase {
	
	private static final Logger logger = Logger.getLogger(TransactionDAO.class);
	private static final long DEPOSIT = 1;
	private static final long WITHDRAW = 2;
	private static final ArrayList<Integer> WITHDRAWAL_CLASS_TYPES = new ArrayList<>(Arrays.asList(2,6,8,9));
	private static final ArrayList<Integer> WITHDRAWAL_SUCCESS_FIRST_OR_SECOND_APPROVE = new ArrayList<>(Arrays.asList(2,9,17));
	
	
	public static HashMap<String, HashMap<Long, String>> getFilters(Connection conn) throws SQLException {
		HashMap<String, HashMap<Long, String>> filtersList = new HashMap<>();
		CallableStatement cstmt = null;
		ResultSet rs = null;
		int index = 1;
		try {
			cstmt = conn.prepareCall("{call pkg_transactions_backend.load_filters(o_platforms => ? "+
																   				  ",o_skin_business_cases => ? "+
																   				  ",o_user_classes => ? "+
																   				  ",o_countries => ? "+
																   				  ",o_transaction_statuses => ? "+
																   				  ",o_transaction_class_types => ? "+
																   				  ",o_clearing_providers => ? "+
																   				  ",o_transaction_types => ? "+
																   				  ",o_transaction_type_class => ? "+
																   				  ",o_compaigns => ? " +
																   				  ",o_system_writers => ? " +
																   				  ",o_pending_withdraw_types => ? " +
																   				  ",o_languages => ?)}");
			cstmt.registerOutParameter(index++, OracleTypes.CURSOR);
			cstmt.registerOutParameter(index++, OracleTypes.CURSOR);
			cstmt.registerOutParameter(index++, OracleTypes.CURSOR);
			cstmt.registerOutParameter(index++, OracleTypes.CURSOR);
			cstmt.registerOutParameter(index++, OracleTypes.CURSOR);
			cstmt.registerOutParameter(index++, OracleTypes.CURSOR);
			cstmt.registerOutParameter(index++, OracleTypes.CURSOR);
			cstmt.registerOutParameter(index++, OracleTypes.CURSOR);
			cstmt.registerOutParameter(index++, OracleTypes.CURSOR);
			cstmt.registerOutParameter(index++, OracleTypes.CURSOR);
			cstmt.registerOutParameter(index++, OracleTypes.CURSOR);
			cstmt.registerOutParameter(index++, OracleTypes.CURSOR);
			cstmt.registerOutParameter(index++, OracleTypes.CURSOR);
			
			cstmt.executeQuery();
			
			loadFilters(rs, cstmt, filtersList, TransactionManager.filterConstants);
			
		} finally {
			closeStatement(cstmt);
			closeResultSet(rs);
		}
		return filtersList;
	}
	
	public static ArrayList<Transaction> getAll(Connection conn, TransactionGetAllMethodRequest request, TransactionGetAllMethodResponse response) throws SQLException {
		ArrayList<Transaction> transactionList = new ArrayList<>();
		CallableStatement cstmt = null;
		ResultSet rs = null;
		int index = 1;
		try {
			
			cstmt = conn.prepareCall("{call pkg_transactions_backend.get_transactions(o_transactions => ? "+
																   ",i_fromdate => ? "+
																   ",i_todate => ? "+
																   ",i_datetype => ? "+
																   ",i_transaction_id => ? "+
																   ",i_user_id => ? "+
																   ",i_user_classes => ? "+
																   ",i_business_cases => ? "+
																   ",i_countries => ? "+
																   ",i_currencies => ? "+
																   ",i_compaigns => ? "+
																   ",i_transaction_statuses => ? "+
																   ",i_system_writers => ? "+
																   ",i_has_balance => ? "+
																   ",i_clearing_providers => ? " +
																   ",i_skins => ? " +
																   ",i_page_number => ? " +
																   ",i_rows_per_page => ? " +
																   ",i_transaction_types => ? "+
																   ",i_gateway => ? "+// not understand
																   ",i_cc_bin => ? "+
																   ",i_cc_last4 => ? "+
																   ",i_epg_id => ? "+// reference property
																   ",i_trans_class_types => ? " +
																   ",i_receipt_number => ? " +
																   ",i_j4postponed => ? " +
																   ",i_include_be_writers => ? " +
																   ",i_sales_type => ? " +
																   ",i_deposit_source => ? " +
																   ",i_amount_from => ? " +
																   ",i_amount_to => ? " +
																   ",i_tran_ids => ? " +
																   ",i_was_rerouted => ? " +
																   ",i_is_three_d => ? " +  
																   ",i_cc_countries => ?)} ");
			
			cstmt.registerOutParameter(index++, OracleTypes.CURSOR);
			
			if (request.getTrnIds() != null && !request.getTrnIds().isEmpty()) {
				cstmt.setNull(index++, OracleTypes.TIMESTAMP);
				cstmt.setNull(index++, OracleTypes.TIMESTAMP);
				cstmt.setLong(index++, 1);
			} else {
				cstmt.setTimestamp(index++, new Timestamp(request.getFrom()));// must
				cstmt.setTimestamp(index++, new Timestamp(request.getTo()));// must
				cstmt.setLong(index++, request.getSearchBy());// must
			}
			setStatementValue(request.getTransactionId(), index++, cstmt);
			setStatementValue(request.getUserId(), index++, cstmt);
			
			setStatementValue(request.getUserClasses(), index++, cstmt);
			setArrayVal(request.getBusinessCases(), index++, cstmt, conn);
			setArrayVal(request.getCountries(), index++, cstmt, conn);
			setArrayVal(request.getCurrencies(), index++, cstmt, conn);
			
			setStatementValue(request.getCampaign(), index++, cstmt);
			
			setArrayVal(request.getStatuses(), index++, cstmt, conn);
			setArrayVal(request.getSystemWriters(), index++, cstmt, conn);
			
			setStatementValue(request.getHasBalance(), index++, cstmt);
			
			setArrayVal(request.getCcProviders(), index++, cstmt, conn);
			setArrayVal(request.getSkins(), index++, cstmt, conn);

			if (request.getPage() == null) {
				cstmt.setLong(index++, 1);
			} else {
				cstmt.setLong(index++, request.getPage());
			}
			
			cstmt.setLong(index++, request.getRowsPerPage());
			
			setArrayVal(request.getTransactionTypes(), index++, cstmt, conn);
			
			setStatementValue(request.getGateway(), index++, cstmt);
			setStatementValue(request.getCcBin(), index++, cstmt);
			
			setStatementValue(request.getCcLast4Digits(), index++, cstmt);
			
			setStatementValue(request.getReference(), index++, cstmt);
			setArrayVal(request.getTransactionClassTypes(), index++, cstmt, conn);
			setStatementValue(request.getReceiptNumber(), index++, cstmt);
			setStatementValue(request.getJ4postponed(), index++, cstmt);
			cstmt.setLong(index++, request.getIsIndividualWritersNeedToBeInclude());
			setStatementValue(request.getSalesTypes(), index++, cstmt);
			setStatementValue(request.getDepositSource(), index++, cstmt);
			setStatementValue(request.getAmountFrom(), index++, cstmt);
			setStatementValue(request.getAmountTo(), index++, cstmt);
			setArrayVal(request.getTrnIds(), index++, cstmt, conn);
			setStatementValue(request.getWasRerouted(), index++, cstmt);
			setStatementValue(request.getIsThreeD(), index++, cstmt);
			setArrayVal(request.getCreditCardCountries(), index++, cstmt, conn);
			cstmt.execute();
			
			rs = (ResultSet) cstmt.getObject(1);
			int count = 0;
			double pageTotalDeposits = 0;
			double pageTotalWithdraws = 0;
			while (rs.next()) {
				if (count == 0) {
					response.setTotalCount(rs.getLong("total_count"));
					response.setTotalSumDeposits(Math.round(rs.getDouble("sum_deposits")));
					response.setTotalSumWithdraws(Math.round(rs.getDouble("sum_withdraws")));
					response.setTotalCountDeposits(rs.getLong("count_deposits"));
					response.setTotalCountWithdraws(rs.getLong("count_withdraws"));
					count++;
				}
				
				Transaction vo = new Transaction();
				loadTransaction(rs, vo);
				
				
				if (vo.getDepositOrWithdraw() == DEPOSIT) {
					pageTotalDeposits += (vo.getAmount() * vo.getRate());
				} else if (vo.getDepositOrWithdraw() == WITHDRAW) {// fees are interpreted as withdrawals also
					pageTotalWithdraws += (vo.getAmount() * vo.getRate());
				}
				
				setActionStatuses(vo);
				
				transactionList.add(vo);
			}

			response.setPageTotalDeposits(Math.round(pageTotalDeposits));
			response.setPageTotalWithdraws(Math.round(pageTotalWithdraws));
		} finally {
			closeStatement(cstmt);
			closeResultSet(rs);
		}
		return transactionList;
	}
	
	public static TransactionReroutingGetResponse getReroutingTransactions(Connection conn, long trasnactionId) throws SQLException {
		CallableStatement cstmt = null;
		ResultSet rs = null;
		int index = 1;
		TransactionReroutingGetResponse res = new TransactionReroutingGetResponse();
		 ArrayList<Long> ids = new ArrayList<>();
		try {
			cstmt = conn.prepareCall("{call pkg_transactions_backend.get_rerouting_trans(o_transactions => ? "+																					   
																   					   ",i_transaction_id => ? )}");
			
			cstmt.registerOutParameter(index++, OracleTypes.CURSOR);
		    cstmt.setLong(index++, trasnactionId);
			cstmt.execute();
			
			rs = (ResultSet) cstmt.getObject(1);
			while (rs.next()) {
				ids.add(rs.getLong("id"));
				res.setTypeId(rs.getLong("rec_type"));
			}
		} finally {
			closeStatement(cstmt);
			closeResultSet(rs);
		}
		res.setReroutingTransactionId(ids);
		return res;
	}
	
	public static void cancelDeposit(Connection conn, long transactionId, int cancelStatus, long writerId) throws SQLException{
			
		CallableStatement cstmt = null;
		int index = 1;
		 try {
				cstmt = conn.prepareCall("{call pkg_transactions_backend.cancel_deposit(i_transaction_id => ? "+
						   												   			  ",i_writer_id => ? " +
																					  ",i_status_id => ?)}");
				cstmt.setLong(index++, transactionId);
				cstmt.setLong(index++, writerId);
				cstmt.setInt(index++, cancelStatus);
				
				cstmt.executeUpdate();
				
		 } finally {
				closeStatement(cstmt);
		 }
	}
	
	public static void approveDeposit(Connection conn, long transactionId, long writerId) throws SQLException {

		CallableStatement cstmt = null;
		int index = 1;
		try {
			cstmt = conn.prepareCall("{call pkg_transactions_backend.approve_deposit(i_transaction_id => ? " + 
																					",i_writer_id => ?)}");
			cstmt.setLong(index++, transactionId);
			cstmt.setLong(index++, writerId);

			cstmt.executeUpdate();

		} finally {
			closeStatement(cstmt);
		}
	}
	
	private static void loadTransaction(ResultSet rs, Transaction transaction) throws SQLException{
		
		transaction.setId(rs.getLong("transaction_id"));
		transaction.setUserId(rs.getLong("user_id"));
		transaction.setUserName(rs.getString("user_name"));// users table
		transaction.setCountryId(rs.getLong("user_country_id"));// country table
        transaction.setTimeCreated(convertToDate(rs.getTimestamp("time_created")));
        transaction.setTimeSettled(convertToDate(rs.getTimestamp("time_settled")));
		// credit card
		if (rs.getString("cc_number") != null) {

			CreditCard c = new CreditCard();
			try {
				c.setCcNumber(Long.valueOf(AESUtil.decrypt(rs.getString("cc_number"))));// credit_card table
			} catch (Exception e) {
				logger.info("Can not decrypt credit card number due to: ", e);
			}
			c.setId(rs.getLong("credit_card_id"));// from transactions table
			c.setTypeName(rs.getString("cc_name"));// type id
			c.setCountryName(rs.getString("cc_country_name"));// country name

			if (rs.getLong("cc_is_allowed") == CreditCard.NOT_ALLOWED) {
				c.setPermission(CreditCard.NOT_ALLOWED);
			} else if (rs.getLong("cc_is_allowed") == CreditCard.ALLOWED) {
				c.setPermission(CreditCard.ALLOWED);
				c.setCcAllowed(true);
			} else if (rs.getLong("cc_is_allowed") == CreditCard.NOT_ALLOWED_DEPOSIT) {
				c.setPermission(CreditCard.NOT_ALLOWED_DEPOSIT);
				c.setCcAllowed(true);
				c.setCcNotAllowedDepositOnly(true);
			}
			
			c.setUtcOffsetCreated(rs.getString("cc_utc_offset_created"));
			c.setUtcOffsetModified(rs.getString("cc_utc_offset_modified"));
			c.setExpYear(rs.getString("cc_exp_year"));
			c.setHolderName(rs.getString("cc_holder_name"));
			c.setHolderIdNum(rs.getString("cc_holder_id_num"));
			c.setTimeModified(convertToDate(rs.getTimestamp("cc_time_modified")));
			c.setTimeCreated(convertToDate(rs.getTimestamp("cc_time_created")));
			c.setExpMonth(rs.getString("cc_exp_month"));
			c.setTypeIdByBin(rs.getLong("cc_type_id_by_bin"));
			c.setTypeByBin(rs.getString("cc_type_by_bin"));
			c.setVisible(rs.getLong("cc_is_visible") == 1 ? true : false);
			
			// bins info
			Bins bins = new Bins();
			bins.setCategory(rs.getString("bins_category"));
			bins.setBank(rs.getString("bins_bank"));
			bins.setCardPaymentType(rs.getString("bins_card_payment_type"));
			c.setCcBins(bins);
			
			transaction.setCreditCard(c);
			
		}
    	// end credit card
        transaction.setTypeId(rs.getLong("type_id"));
        transaction.setStatusId(rs.getLong("status_id"));
        transaction.setAmount(rs.getLong("amount"));
        transaction.setWriterId(rs.getLong("writer_id"));
        transaction.setProcessedWriterId(rs.getLong("processed_writer_id"));
		if (WITHDRAWAL_SUCCESS_FIRST_OR_SECOND_APPROVE.contains(rs.getInt("status_id")) && WITHDRAWAL_CLASS_TYPES.contains(rs.getInt("class_type"))) {
			transaction.setWriterWithdrawalApprove(rs.getString("processed_writer_name"));// approved by
		}
        transaction.setWriterUserName(rs.getString("writer_name"));// from writers table source
        transaction.setReferenceId(rs.getBigDecimal("reference_id"));
        transaction.setIp(rs.getString("ip"));
        transaction.setComments(replaceComments(rs.getString("comments"), "Split to trx", "SplittedTrx to"));
        transaction.setChequeId(rs.getBigDecimal("cheque_id"));
        transaction.setWireId(rs.getBigDecimal("wire_id"));
        transaction.setChargeBackId(rs.getBigDecimal("charge_back_id"));
        transaction.setReceiptNum(rs.getBigDecimal("receipt_num"));
        transaction.setAuthNumber(rs.getString("auth_number"));

        transaction.setXorIdAuthorize(rs.getString("xor_id_authorize"));// EPG Trx ID
        transaction.setXorIdCapture(rs.getString("xor_id_capture"));
        transaction.setUtcOffsetCreated(rs.getString("utc_offset_created"));
        transaction.setUtcOffsetSettled(rs.getString("utc_offset_settled"));
        transaction.setClearingProviderId(rs.getLong("clearing_provider_id"));
        transaction.setEnvoyAccountNum(rs.getString("envoy_account_num"));
        transaction.setPaymentTypeId(rs.getLong("payment_type_id"));
		if (rs.getLong("payment_type_id") > 0) {
			transaction.setDescription(rs.getString("payment_type_desc"));
		} else {
			transaction.setDescription(rs.getString("description"));
		}
        transaction.setIsRerouted(rs.getLong("is_rerouted") == 1 ? true : false);
        transaction.setReroutingTranId(rs.getLong("rerouting_transaction_id"));
        transaction.setManualRouted(rs.getLong("is_manual_routing"));
        transaction.setSelectorId(rs.getInt("selector_id"));
        transaction.setDepositOrWithdraw(rs.getLong("deposit_or_withdraw"));// credit or debit
        transaction.setUserFirstName(rs.getString("first_name"));
        transaction.setUserLastName(rs.getString("last_name"));
        transaction.setBusinessCaseName(rs.getString("business_case_desc"));
        transaction.setSkin(rs.getString("user_skin_name"));
        transaction.setCurrencyId(rs.getLong("user_currency_id"));
        transaction.setRate(rs.getDouble("rate"));
        transaction.setHaveCredits(rs.getLong("have_withdrawals") > 0 ? true : false);
        transaction.setCreditAmount(rs.getLong("credit_amount"));
        transaction.setSplittedReferenceId(rs.getLong("splitted_reference_id"));
        transaction.setDepositReferenceId(rs.getLong("deposit_reference_id"));
        transaction.setCreditWithdrawal(rs.getLong("is_credit_withdrawal") == 1 ? true : false);
        transaction.setPayPalEmail(rs.getString("paypal_email"));
        transaction.setClearingProviderName(rs.getString("clearing_provider_name"));
        transaction.setAcquirerResponseId(rs.getString("acquirer_response_id"));
        
        // transaction postponed
        transaction.setTransactionPostponed(new TransactionPostponed());
        transaction.getTransactionPostponed().setId(rs.getLong("ppn_id"));
        transaction.getTransactionPostponed().setNumberOfDays(rs.getInt("ppn_number_of_days"));
        transaction.getTransactionPostponed().setWriterId(rs.getLong("ppn_writer_id"));
        transaction.getTransactionPostponed().setTimeUpdated(convertToDate(rs.getTimestamp("ppn_time_updated")));
        
        // amount in $
        transaction.setAmountInUsd(transaction.getAmount() * transaction.getRate());
        
        //Transaction Splitted as:
        long splitted = transaction.getHowTransactionSplitted();
		if (splitted == 1) {
			transaction.setTransactionSplittedAs("transactions.credit.withdrawal");
		} else if (splitted == 2) {
			transaction.setTransactionSplittedAs("transactions.CFT.withdrawal");
		} else if (splitted == 3) {
			transaction.setTransactionSplittedAs("transaction.split.between");
		}
		
		transaction.isCFTWithdrawalInternal();
		transaction.isCreditWithdrawalInternal();
		// wire transaction info
		if (transaction.getWireId() != null && transaction.getWireId().intValue() > 0) {
			WireBase wire = new WireBase();
			wire.setId(transaction.getWireId().longValue());
			wire.setBeneficiaryName(rs.getString("beneficiary_name"));
			wire.setAccountNum(rs.getString("account_num"));
			wire.setSwift(rs.getString("swift"));
			wire.setIban(rs.getString("iban"));
			wire.setBankName(rs.getString("bank_name"));
			wire.setBranch(rs.getString("branch"));
			wire.setBranchName(rs.getString("branch_name"));
			wire.setBranchAddress(rs.getString("branch_address"));
			transaction.setWire(wire);
		}
		
		//Special care
		SpecialCare specialCare = new SpecialCare();
        specialCare.setTimeScTurnover(convertToDate(rs.getTimestamp("time_turnover")));
        specialCare.setTimeScHouseResult(convertToDate(rs.getTimestamp("time_house_result")));
        specialCare.setTimeScManual(convertToDate(rs.getTimestamp("time_manualy")));
        transaction.setSpecialCare(specialCare);
		
		// Skrill transactions
		if(transaction.getTypeId() == Constants.TRANSACTION_TYPE_SKRILL_DEPOSIT ||
		   transaction.getTypeId() == Constants.TRANSACTION_TYPE_SKRILL_WITHDRAW){
			//MoneybookersInfo info = transaction.getMoneybookersDetails();
			transaction.setMoneybookersEmail(rs.getString("moneybookers_email"));
		}
		
		// maintenance fee cancel
		
		if (transaction.getTypeId() == Constants.TRANS_TYPE_MAINTENANCE_FEE) {
			transaction.setMaintenanceFeeCancel(!TransactionsManagerBase.isMaintenanceFeeCanceled(transaction.getId()));
		}
		transaction.setIs3D(rs.getBoolean("is_3d"));
		// Servey withdrawal answers
		transaction.setQmaAnswerName(rs.getString("qma_answer_name"));
		transaction.setQmTextAnswer(rs.getString("qm_text_answer"));
	}
	
   private static void loadCryptoDeposits(ResultSet rs, CryptoDeposits deposits) throws SQLException {

		deposits.setName(rs.getString("Name"));
		deposits.setCountry(rs.getString("Country"));
		deposits.setTimeCreated(rs.getTimestamp("Created") == null ? 0 : rs.getTimestamp("Created").getTime());
		deposits.setDepositAmount(rs.getLong("Deposit_amount"));
		deposits.setDepositRate(rs.getDouble("Deposit_rate"));
		deposits.setCurrency(rs.getString("Currency"));
		deposits.setExecutionDate(rs.getTimestamp("Execution_date") == null ? 0 : rs.getTimestamp("Execution_date").getTime());
		deposits.setExecutionAmount(rs.getLong("Execution_amount"));
		deposits.setExecutionRate(rs.getDouble("Execution_rate"));
		deposits.setFee(rs.getDouble("Fee"));
		deposits.setClearanceFee(rs.getDouble("Clearance_fee"));
		deposits.setLang(rs.getString("Lang"));
		deposits.setPaymentMethod(rs.getInt("Payment_method"));
		deposits.setTotalDepositUsd(rs.getLong("Total_Deposit_usd"));
		deposits.setTotalDepositAccCurr(rs.getLong("Total_Deposit_acc_curr"));
		deposits.setTransactionStatus(rs.getLong("Transaction_Status"));
		deposits.setAssignee(rs.getString("Assignee"));
		deposits.setCryptoDepositStatus(rs.getLong("CryptoDepositStatus"));
		deposits.setCryptoWstatus(rs.getLong("CryptoWstatus"));
		deposits.setEmail(rs.getString("Email"));
		deposits.setUserId(rs.getLong("User_Id"));
		deposits.setTransactionId(rs.getLong("Transaction_Id"));
		deposits.setTxId(rs.getString("tx_id"));
	}
	
	public static void manageTransactionPostponed(Connection conn, TransactionPostponedRequest request) throws SQLException{
		
		CallableStatement cstmt = null;
		int index = 1;
		try {
			cstmt = conn.prepareCall("{call pkg_transactions_backend.manage_trans_postponed(i_ppn_id => ? " + 
																						  ",i_ppn_num_of_days => ? " +
																						  ",i_ppn_trans_id => ? " +
																						  ",i_writer_id => ?)}");
			setStatementValue(request.getId(), index++, cstmt);
			cstmt.setLong(index++, request.getNumberOfDays());
			cstmt.setLong(index++, request.getTransactionId());
			cstmt.setLong(index++, request.getWriterId());

			cstmt.executeUpdate();

		} finally {
			closeStatement(cstmt);
		}
	}
	
	
	public static void insertWithdraw(Connection conn, WithdrawInsertMethodRequest request) throws SQLException{
		
		CallableStatement cstmt = null;
		int index = 1;
		try {
			cstmt = conn.prepareCall("{call pkg_withdraw_backend.create_withdraw(i_user_id => ? " + 
																				 ",i_amount => ? " +
																				 ",i_writer_id => ? " +
																				 ",i_withdraw_type_id => ? " +
																				 ",i_comments => ? " +
																				 ",i_rate => ?)}");

			cstmt.setLong(index++, request.getUserId());
			cstmt.setDouble(index++, request.getConvertedAmount());
			cstmt.setLong(index++, request.getWriterId());
			cstmt.setLong(index++, request.getWithdrawTypeId());
			cstmt.setString(index++, request.getComment());
			cstmt.setDouble(index++, request.getRate());

			cstmt.executeUpdate();

		} finally {
			closeStatement(cstmt);
		}
	}

	public static void getTransactionChanges(Connection con, TransactionGetAllMethodRequest request,
			UpdateTransactionMethodResponse response) throws SQLException {
		CallableStatement cstmt = null;
		ResultSet rs = null;
		int index = 1;
		try {
			cstmt = con.prepareCall("{call pkg_transactions.GET_TRANSACTION_CHANGES(I_TRANSACTION_ID => ? " + 
																				 	",O_TYPES => ? " +
																					",O_STATUSES => ?)}");

			cstmt.setLong(index++, request.getTransactionId());
			cstmt.registerOutParameter(index++, OracleTypes.CURSOR);
			cstmt.registerOutParameter(index++, OracleTypes.CURSOR);
			cstmt.execute();
			
			rs = (ResultSet) cstmt.getObject(2);
			LinkedHashMap<Long, String> types = new LinkedHashMap<Long, String>();
			HashMap<Long, String> map = new HashMap<Long, String>();
			while (rs.next()) {
				map.put(rs.getLong("id"), CommonUtil.getMessage(rs.getString("description"), null));
			}
			map.entrySet().stream().sorted(Map.Entry.<Long, String>comparingByValue())
	         .forEachOrdered(entry -> types.put(entry.getKey(), entry.getValue()));
			response.setTypes(types);
			
			rs = (ResultSet) cstmt.getObject(3);
			LinkedHashMap<Long, String> statuses = new LinkedHashMap<Long, String>();
			map.clear();
			while (rs.next()) {
				map.put(rs.getLong("id"), CommonUtil.getMessage(rs.getString("description"), null));
			}
			map.entrySet().stream().sorted(Map.Entry.<Long, String>comparingByValue())
	         .forEachOrdered(entry -> statuses.put(entry.getKey(), entry.getValue()));
			response.setStatuses(statuses);
		} finally {
			closeStatement(cstmt);
		}
	}
	
	public static ArrayList<CryptoDeposits> getAllCrytpoDeposits(Connection conn, GetAllCryptoDepsitsRequest request, GetAllCryptoDepositsResponse response) throws SQLException {
		ArrayList<CryptoDeposits> depositsList = new ArrayList<>();
		CallableStatement cstmt = null;
		ResultSet rs = null;
		int index = 1;
		try {
			
			cstmt = conn.prepareCall("{call pkg_transactions_backend.GET_DEPOSIT_HISTORY(o_deposits => ? "+
																					   ",i_fromdate => ? "+
																					   ",i_todate => ? "+
																					   ",i_datetype => ? "+
																				       ",i_countries => ? "+
																					   ",i_transaction_types => ? "+
																					   ",i_assignees => ? "+
																					   ",i_user_id => ? "+
																					   ",i_transaction_id => ? "+
																					   ",i_email => ? "+
																					   ",i_first_name => ? "+
																					   ",i_last_name => ? "+
																					   ",i_page => ? "+ 
																					   ",i_rows_per_page => ?)} ");
			
			cstmt.registerOutParameter(index++, OracleTypes.CURSOR);
			
			setStatementValue(new Timestamp(request.getFrom() == null ? 0 : request.getFrom() + 1), index++, cstmt);
			setStatementValue(new Timestamp(request.getTo() == null ? 0 : request.getTo() + 1), index++, cstmt);
			setStatementValue(request.getSearchBy(), index++, cstmt);
			setArrayVal(request.getCountries(), index++, cstmt, conn);
			setArrayVal(request.getTransactionTypes(), index++, cstmt, conn);
			setArrayVal(request.getAssignees(), index++, cstmt, conn);
			setStatementValue(request.getUserId(), index++, cstmt);
			setStatementValue(request.getTransactionId(), index++, cstmt);
			setStatementValue(request.getEmail(), index++, cstmt);
			setStatementValue(request.getFirstName(), index++, cstmt);
			setStatementValue(request.getLastName(), index++, cstmt);
			setStatementValue(request.getPage(), index++, cstmt);
			setStatementValue(request.getRowsPerPage(), index++, cstmt);
			
			cstmt.execute();
			
			rs = (ResultSet) cstmt.getObject(1);
			int count = 0;
			while (rs.next()) {
				if (count == 0) {
					response.setTotalCount(rs.getLong("total_count"));
					count++;
				}
				
				CryptoDeposits vo = new CryptoDeposits();
				loadCryptoDeposits(rs, vo);
				
				depositsList.add(vo);
			}
		} finally {
			closeStatement(cstmt);
			closeResultSet(rs);
		}
		return depositsList;
	}
	
	// Util start	
	public static void setActionStatuses(Transaction t) {
		// mistake
		if (t.isWireDeposit() && t.getStatusId() == TransactionsManagerBase.TRANS_STATUS_PENDING) {
			t.setAllowedCancelDepositMistakeAction(true);
		}
		// cancel fraud deposit, cancel deposit
		if (t.isCcDepositAndPending() || (t.isWireDeposit() && t.getStatusId() == TransactionsManagerBase.TRANS_STATUS_PENDING)) {
			t.setAllowedCancelDepositFraudAction(true);
			t.setAllowedCancelDepositAction(true);
		}

		// approve deposit
		if ((t.isWireDeposit() && t.getStatusId() == TransactionsManagerBase.TRANS_STATUS_PENDING)) {
			t.setAllowedApproveDepositAction(true);
		}
		// chargeback edit
		if (t.isChargeBackEdit()) {
			t.setAllowedChargebackEditAction(true);
		}
		
		// chargeback add
		if (t.isChargeBackAdd()) {
			t.setAllowedChargebackAddAction(true);
		}
	}
	
	private static String replaceComments(String comment, String replacement, String regex) {

		if (comment == null || comment.equals("")) {
			return null;
		}

		Pattern p = Pattern.compile(regex);
		Matcher m = p.matcher(comment);
		StringBuffer sb = new StringBuffer();

		while (m.find()) {
			m.appendReplacement(sb, replacement);
		}

		m.appendTail(sb);

		return sb.toString();
	}
	//Util end
	
	public static void loadFilters(ResultSet rs, CallableStatement cstmt, HashMap<String, HashMap<Long, String>> filtersList, String[] filterConstants) throws SQLException {
		
		for (int i = 1; i < filterConstants.length; i++) {
			rs = (ResultSet) cstmt.getObject(i);
			HashMap<Long, String> map = new HashMap<>();
			while (rs.next()) {
				map.put(rs.getLong("id"), rs.getString("value"));
			}
			filtersList.put(filterConstants[i], map);
			rs.close();
		}
	}
}
