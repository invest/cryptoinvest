/**
 * 
 */
package com.anyoption.backend.daos;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;

import org.apache.log4j.Logger;

import com.anyoption.backend.service.salesinvestmentsmanager.SalesInvestmentsManagerMethodRequest;
import com.anyoption.backend.service.salesinvestmentsmanager.SalesInvestmentsManagerMethodResponse;
import com.anyoption.backend.service.salesinvestmentsrepresentatives.SalesInvestmentsRepresentativesMethodRequest;
import com.anyoption.backend.service.salesinvestmentsrepresentatives.SalesInvestmentsRepresentativesMethodResponse;
import com.anyoption.common.beans.SalesInvestments;
import com.anyoption.common.daos.DAOBase;

import oracle.jdbc.OracleTypes;

/**
 * @author Eyal Goren
 *
 */
public class SalesInvestmentsDAO extends DAOBase {

	private static final Logger logger = Logger.getLogger(SalesInvestmentsDAO.class);
	
	
	public static ArrayList<SalesInvestments> getSalesInvestmentsRep(Connection conn, SalesInvestmentsRepresentativesMethodRequest request,
			SalesInvestmentsRepresentativesMethodResponse response) throws SQLException {
		
		ArrayList<SalesInvestments> salesInvestmentsList = new ArrayList<SalesInvestments>();
		CallableStatement cstmt = null;
		ResultSet rs = null;
		int index = 1;
		try {			  
			cstmt = conn.prepareCall("{call pkg_writers_comm_reports.get_data_by_user(o_users_data => ? "+
																   ",i_writer_id => ? "+
																   ",i_from_date => ? "+
																   ",i_to_date => ? "+
																   ",i_page_number => ? "+
																   ",i_rows_per_page => ?)}");
			
			cstmt.registerOutParameter(index++, OracleTypes.CURSOR);			
			cstmt.setLong(index++, request.getWriterId());
			cstmt.setTimestamp(index++, new Timestamp(request.getFromDate()));
			cstmt.setTimestamp(index++, new Timestamp(request.getToDate()));
			
			if (request.getPageNumber() == null) {
				cstmt.setInt(index++, 1);
			} else {
				cstmt.setInt(index++, request.getPageNumber());
			}
						
			cstmt.setInt(index++, request.getRowsPerPage());			
			cstmt.executeQuery();
			rs = (ResultSet) cstmt.getObject(1);
			
			boolean isFirstTime = true;
			while (rs.next()) {				
				if (isFirstTime) {
					response.setTotalCount(rs.getLong("total_count"));
					response.setTotalAmount(rs.getDouble("total_amount"));
					isFirstTime = false;
				}				
				SalesInvestments SalesInvestment = new SalesInvestments();				
				SalesInvestment.setName(rs.getString("user_id"));
				SalesInvestment.setTotalInv(rs.getDouble("total_inv"));
				SalesInvestment.setTraderValue(rs.getDouble("trader_value"));				
				salesInvestmentsList.add(SalesInvestment);
			}
		} finally {
			closeStatement(cstmt);
			closeResultSet(rs);
		}
		return salesInvestmentsList;
	}
	
	public static ArrayList<SalesInvestments> getSalesInvestmentsManager(Connection conn, SalesInvestmentsManagerMethodRequest request,
			SalesInvestmentsManagerMethodResponse response) throws SQLException {
		
		ArrayList<SalesInvestments> salesInvestmentsList = new ArrayList<SalesInvestments>();
		CallableStatement cstmt = null;
		ResultSet rs = null;
		int index = 1;
		try {			   
			cstmt = conn.prepareCall("{call pkg_writers_comm_reports.get_data_by_writer(o_writers_data => ? "+
																   ",i_from_date => ? "+
																   ",i_to_date => ? "+
																   ",i_page_number => ? "+
																   ",i_rows_per_page => ?)}");			
			cstmt.registerOutParameter(index++, OracleTypes.CURSOR);			
			cstmt.setTimestamp(index++, new Timestamp(request.getFromDate()));
			cstmt.setTimestamp(index++, new Timestamp(request.getToDate()));
			
			if (request.getPageNumber() == null) {
				cstmt.setInt(index++, 1);
			} else {
				cstmt.setInt(index++, request.getPageNumber());
			}						
			cstmt.setInt(index++, request.getRowsPerPage());			
			cstmt.executeQuery();
			rs = (ResultSet) cstmt.getObject(1);			
			boolean isFirstTime = true;
			while (rs.next()) {				
				if (isFirstTime) {
					response.setTotalCount(rs.getLong("total_count"));
					response.setTotalAmount(rs.getDouble("total_amount"));
					isFirstTime = false;
				}				
				SalesInvestments SalesInvestment = new SalesInvestments();
				SalesInvestment.setName(rs.getString("writer_name"));
				SalesInvestment.setTotalInv(rs.getDouble("total_inv"));
				SalesInvestment.setTraderValue(rs.getDouble("trader_value"));
				salesInvestmentsList.add(SalesInvestment);
			}
		} finally {
			closeStatement(cstmt);
			closeResultSet(rs);
		}
		return salesInvestmentsList;
	}

}
