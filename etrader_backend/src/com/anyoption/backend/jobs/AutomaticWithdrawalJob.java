package com.anyoption.backend.jobs;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.HashMap;
import java.util.stream.IntStream;

import org.apache.log4j.Logger;

import com.anyoption.backend.managers.TransactionManager;
import com.anyoption.common.beans.CreditCard;
import com.anyoption.common.beans.Issue;
import com.anyoption.common.beans.IssueAction;
import com.anyoption.common.beans.Job;
import com.anyoption.common.beans.Transaction;
import com.anyoption.common.beans.base.CreditCardType;
import com.anyoption.common.beans.base.UserRegulationBase;
import com.anyoption.common.bl_vos.WireBase;
import com.anyoption.common.daos.CreditCardsDAOBase;
import com.anyoption.common.daos.TransactionsDAOBase;
import com.anyoption.common.daos.UsersRegulationDAOBase;
import com.anyoption.common.daos.WiresDAOBase;
import com.anyoption.common.jobs.ScheduledJob;
import com.anyoption.common.managers.BaseBLManager;
import com.anyoption.common.managers.CurrencyRatesManagerBase;
import com.anyoption.common.service.gmwithdrawal.GmWithdrawalRequest;
import com.anyoption.common.util.CommonUtil;

import il.co.etrader.backend.bl_managers.TransactionsManager;
import il.co.etrader.bl_managers.IssuesManagerBase;
import il.co.etrader.bl_managers.TransactionsManagerBase;
import il.co.etrader.bl_vos.UserBase;
import il.co.etrader.dao_managers.IssuesDAOBase;
import il.co.etrader.dao_managers.UsersDAOBase;
import il.co.etrader.dao_managers.WritersDAO;
import il.co.etrader.util.ConstantsBase;

public class AutomaticWithdrawalJob extends BaseBLManager implements ScheduledJob {
    private static Logger log = Logger.getLogger(CapInternalMailerJob.class);
    
    HashMap<String, String> jobProperties;
    
    private boolean isTestRun = false;
    private long daysSinceQuestionary = 14;
    private long writerId = 28000;

    
	@Override
	public void setJobInfo(Job job) {
    	jobProperties = new HashMap<String, String>();
		if (null != job.getConfig() && !job.getConfig().trim().equals("")) {
            String[] ps = job.getConfig().split(",");
            for (int i = 0; i < ps.length; i++) {
            	String[] p = ps[i].split("=");
            	jobProperties.put(p[0], p[1]);
            }
		}
		String test = jobProperties.get("test");
		try {
			daysSinceQuestionary = Long.parseLong(jobProperties.get("daysSinceQuestionary"));
		} catch (Exception e) {
			daysSinceQuestionary = 14;
		}
		
		try {
			writerId = Long.parseLong(jobProperties.get("writerId"));
		} catch (Exception e) {
			writerId = 28000;
		}

		isTestRun = test != null && test.equalsIgnoreCase("true");
	}

	@Override
	public boolean run() {
		log.info("Starting Automatic Withdrawal job.");
		boolean r =  awd();
		log.info("End Automatic Withdrawal job.");
		return r;
	}

	@Override
	public void stop() {
		// TODO Auto-generated method stub
		
	}

	private boolean awd() {
		Connection connection = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try{
			connection = getConnection();
			
			String sql = 
				"SELECT * FROM ( " + 
						                "SELECT " +  
						                        "u.balance balance, " + 
						                        "u.id usr, " + 
						                        "TRUNC(SYSDATE) - TRUNC(questionnaire_done_date) AS days_since_quest, " +
						                        "sum(t.amount) total_deposit " +  
						                "FROM " + 
						                        "users_regulation ur, " + 
						                        "users u, " + 
						                        "transactions t " + 
						                "WHERE " + 
						                        "u.id = ur.user_id " + 
						                        "AND u.is_active = 1  "; // user is active
						                        if(isTestRun) {
						                        	sql += "AND u.class_id = 0 "; // test account
						                        } else {
						                        	sql += "AND u.class_id <> 0 "; // not test account
						                        }
						                        sql += 
						                        "AND ur.suspended_reason_id = 0  " + // not suspended 
						                        "AND ur.questionnaire_done_date IS NOT NULL " + 
						                        "AND ur.approved_regulation_step = 3 " +
						                        "AND t.type_id IN(1,2,9,17,38,32,58,54) " +  
						                        "AND t.status_id in (2, 7) " + 
						                        "AND t.user_id = u.ID " + 
						               "GROUP BY balance,  u.id, TRUNC(SYSDATE) - TRUNC(questionnaire_done_date) " + 
				")"
               + " WHERE days_since_quest = ? "; 

			String eligibleSql = 
				" SELECT COUNT(*) as eligible FROM DUAL WHERE " + 
				" NOT EXISTS ( SELECT ID FROM investments WHERE user_id = ? AND is_settled = 0 AND is_canceled = 0 AND time_created > (SYSDATE - 14)) AND " +  // NOT Have open investments  
				" NOT EXISTS ( SELECT ID FROM bonus_users WHERE bonus_state_id in (1, 2) AND user_id = ? ) AND " +  // NOT Have a bonus in active state 
				" NOT EXISTS ( SELECT ID FROM transactions WHERE type_id IN(10, 14, 26, 37, 36, 39, 41, 59, 60, 61) AND status_id IN ( 4, 7, 9, 17) AND user_id = ?) AND" +  // NOT Have pending withdrawal 
				" NOT EXISTS ( SELECT ID FROM transactions WHERE type_id in (9, 54) AND status_id IN (2, 4, 7) AND user_id = ?) " +  //NOT Have a succ. Cash deposit or IDEAL
				" AND (SELECT count(unique(nvl(credit_card_id, type_id))) FROM transactions WHERE type_id IN(1, 2, 25, 17, 38, 32, 33, 40, 58, 54) AND status_id IN (2, 7, 9) AND user_id = ?) < 2 " + // Have only one type of deposit, 1 cc deposit
				" AND (SELECT count(DISTINCT(clearing_provider_id)) FROM transactions WHERE type_id =1 AND  status_id IN (2, 7, 9) AND user_id = ?) < 2 " + // only 1 clearing provider used
				" AND EXISTS (SELECT u.id FROM users u, withdraw_fees wf WHERE u.balance > wf.fee_min_amount AND u.currency_id = wf.currency_id AND u.id = ?) " ;
			
			pstmt = connection.prepareStatement(sql);

			pstmt.setLong(1, daysSinceQuestionary);
			rs = pstmt.executeQuery();

			while (rs.next()) {
				long userId = rs.getLong("usr");
				double balance = (double) rs.getLong("balance")/100l;
				double totalDeposit = (double) rs.getLong("total_deposit")/100l;

				log.info("Start processing userId :"+userId + " balance:"+ balance+ " ttlDeposits:" + totalDeposit);

				UserBase user = new UserBase();
				UsersDAOBase.getByUserId(connection, userId, user);
				
				PreparedStatement pstmt1 = connection.prepareStatement(eligibleSql);
				pstmt1.setLong(1, userId);
				pstmt1.setLong(2, userId);
				pstmt1.setLong(3, userId);
				pstmt1.setLong(4, userId);
				pstmt1.setLong(5, userId);
				pstmt1.setLong(6, userId);
				pstmt1.setLong(7, userId);
				ResultSet rs1 = pstmt1.executeQuery();
				rs1.next();
				int eligible = rs1.getInt("eligible");
				String trx = "";
				
				if(eligible != 0) {
					log.info("user:" +userId +" id eligible");
					
					Transaction t = TransactionsDAOBase.getLastUserDepositEver(connection, userId);
					if(t != null) {
						long transactionId = t.getId(); 
						long depositType =	t.getTypeId();
						
						
						
						if (depositType == TransactionManager.TRANS_TYPE_WIRE_DEPOSIT ) {
							double minWithdrawal = (double) TransactionsManagerBase.getWireMinWithdraw(user.getCurrencyId()) / 100l;
							if(balance > minWithdrawal) {
								log.info("(1) full wd for trx:"+ transactionId + " userID:"+ userId + " balance:"+balance+" minWD:"+minWithdrawal +" bc wire ");
								Long wireId = TransactionsDAOBase.getWireByTransactionId(connection, transactionId);
								WireBase wire = WiresDAOBase.getById(connection, wireId);
								wire.setAmount(""+balance);
								TransactionsManagerBase.insertBankWireWithdraw(wire, null, user, writerId, true, false, 0l, null);
								
								trx = String.valueOf(com.anyoption.common.managers.TransactionsManagerBase.getLastTransactionsId(user.getId()));
							} else {
								log.info("(2) no wd for trx:"+ transactionId + " balance:"+balance+" is lower than minWD:"+minWithdrawal + " AutoWD is cancelled");
								eligible = 0;
							}
						}
						
						if( depositType == TransactionManager.TRANS_TYPE_MONEYBOOKERS_DEPOSIT) {
							log.info("(3) full wd for trx:"+ transactionId + " userID:"+ userId + " bc MB ");
							String moneybookersEmail = TransactionsDAOBase.getLastMoneybookersEmail(connection, userId);
							TransactionsManagerBase.insertMoneybookersWithdraw(String.valueOf(balance), moneybookersEmail, null, user, writerId, true);
							
							trx = String.valueOf(com.anyoption.common.managers.TransactionsManagerBase.getLastTransactionsId(user.getId()));
						}
						
						if (depositType == TransactionManager.TRANS_TYPE_DIRECT_BANK_DEPOSIT ) {
							double minWithdrawal = (double) TransactionsManagerBase.getWireMinWithdraw(user.getCurrencyId()) / 100l;
							
							Long paymentType = TransactionsDAOBase.getPaymentTypeIdByTransactionId(connection, transactionId);
							Long wdType = 0l;
							if(paymentType == 2) { // Sofort
								wdType = TransactionManager.TRANS_TYPE_DIRECT24_WITHDRAW;
							} else if(paymentType == 3) { // Giropay
								wdType = TransactionManager.TRANS_TYPE_GIROPAY_WITHDRAW;
							} else if(paymentType == 4) { // Eps
								wdType = TransactionManager.TRANS_TYPE_EPS_WITHDRAW;
							} 
							
							Long wireId = TransactionsDAOBase.getWireByTransactionId(connection, transactionId);
							WireBase wire = WiresDAOBase.getById(connection, wireId);
							if(wire == null) {
								log.info("Can't load wire for transaction" +transactionId+ " continuing with empty wire");
								wire = new WireBase();
							}
							GmWithdrawalRequest request = new GmWithdrawalRequest();
							request.setUserId(userId);
							request.setConvertedRequestAmount(new BigDecimal(balance).multiply(new BigDecimal(100)).longValue());
							request.setWriterId(writerId);
							request.setGmType(wdType);
							request.setRate(CurrencyRatesManagerBase.getUSDRate(user.getCurrencyId()));
							request.setBeneficiaryName(wire.getBeneficiaryName());
							request.setSwiftBicCode(wire.getSwift());
							request.setIban(wire.getIban());
							request.setAccountNumber(CommonUtil.isParameterEmptyOrNull(wire.getAccountNum()) ? 0 : Long.parseLong(wire.getAccountNum()));
							request.setBankName(wire.getBankName());
							request.setBranchNumber(CommonUtil.isParameterEmptyOrNull(wire.getBranch()) ? 0 : Long.parseLong(wire.getBranch()));
							request.setBranchAddress(wire.getBranchAddress());
							request.setAccountInfo(wire.getAccountInfo());
							request.setTrnStatusId(TransactionsManagerBase.TRANS_STATUS_REQUESTED);
							request.setIp("0.0.0.0");
							request.setExemptFee(true);
							
							if(balance > totalDeposit ) {
								if(( (balance - totalDeposit) > minWithdrawal)) {
									log.info("(4) split wd for trx:"+ transactionId + " userID:"+ userId + " bc CFT ("+(balance - totalDeposit)+") is > minWD :"+ minWithdrawal);
									// SPLIT WD
									wire.setAmount(String.valueOf(balance - totalDeposit));
									TransactionsManagerBase.insertBankWireWithdraw(wire, null, user, writerId, true, false, 0l, null);
									trx = String.valueOf(com.anyoption.common.managers.TransactionsManagerBase.getLastTransactionsId(user.getId()));
									request.setConvertedRequestAmount(new BigDecimal(totalDeposit).multiply(new BigDecimal(100)).longValue());									
									TransactionsDAOBase.insertGmWithdraw(request, connection);
									
									trx = trx + ", "+com.anyoption.common.managers.TransactionsManagerBase.getLastTransactionsId(user.getId());
								} else {
									log.info("(5) no wd for trx:"+ transactionId + " userID:"+ userId +" cft:"+(balance - totalDeposit)+" is lower than minWD"+minWithdrawal + " AutoWD is cancelled");
									eligible = 0;
								}
								
							} else {
								if(balance > minWithdrawal) {
									TransactionsDAOBase.insertGmWithdraw(request, connection);
									trx = String.valueOf(com.anyoption.common.managers.TransactionsManagerBase.getLastTransactionsId(user.getId()));
								} else {
									log.info("(6) no wd for trx:"+ transactionId + " balance:"+balance+" is lower than minWD:"+minWithdrawal + " AutoWD is cancelled");
									eligible = 0;
								}
							}
						}
						
						if (depositType == TransactionManager.TRANS_TYPE_CC_DEPOSIT) {
							Long[] details = TransactionsDAOBase.getDetailsByTransactionId(connection, transactionId);
							double minWithdrawal = (double) TransactionsManagerBase.getCreditCardMinWithdraw(user.getCurrencyId()) / 100l;
								
							Long ccId = details[0];
							Long clearingProviderId = details[1]; 
							CreditCard card = CreditCardsDAOBase.getById(connection, ccId);
	
							if (t.getClearingProviderId() > 0) {
								card.setClearingProviderId(t.getClearingProviderId());
								card.setCftClearingProviderId(t.getClearingProviderId());
							}
							int[] transact = {61,52,46,45,44,43,33,23};
							int[] trustPay = {51,50,40,39,38};
							
							boolean isTransact = IntStream.of(transact).anyMatch(x -> x == clearingProviderId);
							boolean isTrustPay = IntStream.of(trustPay).anyMatch(x -> x == clearingProviderId);
							
							if(balance > totalDeposit ) {
								if(isTrustPay || (isTransact && card.getType().getId() == CreditCardType.CC_TYPE_MASTERCARD) ) {
									if((balance - totalDeposit) > minWithdrawal ) {
										log.info("(7) split wd for trx:"+ transactionId + " userID:"+ userId + " bc CFT > minWD ");
										// SPLIT WD
										TransactionsManager.insertCreditCardWithdraw(String.valueOf(totalDeposit), card, card.getHolderName(), null, user, writerId, true, null, 0L, null);
										trx = String.valueOf(com.anyoption.common.managers.TransactionsManagerBase.getLastTransactionsId(user.getId()));
										
										WireBase wire = new WireBase();
										wire.setAmount(String.valueOf(balance - totalDeposit));
										TransactionsManagerBase.insertBankWireWithdraw(wire, null, user, writerId, true, false, 0l, null);
										trx += trx + ", " + String.valueOf(com.anyoption.common.managers.TransactionsManagerBase.getLastTransactionsId(user.getId()));
									} else {
										log.info("(8) no wd for trx:"+ transactionId + " userID:"+ userId + " bc isTrustPay || (isTransact && mastercard) and amount < minWD ");
										eligible = 0;
									}
								} else {
									// TOTAL WD
									log.info("(9) full wd for trx:"+ transactionId + " userID:"+ userId + " for other CC ");
									TransactionsManager.insertCreditCardWithdraw(String.valueOf(balance), card, card.getHolderName(), null, user, writerId, true, null, 0L, null);
									trx = String.valueOf(com.anyoption.common.managers.TransactionsManagerBase.getLastTransactionsId(user.getId()));
								}
							} else {
								// TOTAL WD
								if(balance > minWithdrawal) {
									log.info("(10) full wd for trx:"+ transactionId + " userID:"+ userId + " bc balance <= totalDeposit ");
									TransactionsManager.insertCreditCardWithdraw(String.valueOf(balance), card, card.getHolderName(), null, user, writerId, true, null, 0L, null);
									trx = String.valueOf(com.anyoption.common.managers.TransactionsManagerBase.getLastTransactionsId(user.getId()));
								} else {
									log.info("(11) no wd for trx:"+ transactionId + " balance:"+balance+" is lower than minWD:"+minWithdrawal + " AutoWD is cancelled");
									eligible = 0;
								}
							}
						} 
					} else {
						log.error("Can't load deposit transaction for user:"+userId);
					}
				} else {
					log.info("user:" +userId +" id is NOT eligible");
				}

				// For a safe, clean process, we will have ALL users without full KYC suspended. 
				UserRegulationBase urb = new UserRegulationBase();
				urb.setComments("End of grace period");
				urb.setSuspendedReasonId(UserRegulationBase.SUSPENDED_AUTO_WD);
				urb.setUserId(userId);
				urb.setWriterId(writerId);
				UsersRegulationDAOBase.updateSuspended(connection, urb);
				insertIssueToUsers(connection, user, balance, writerId, eligible != 0, trx);
				log.info("End processing :"+ userId) ;
			}

			return true;
		} catch(Exception e) {
			log.error("Automatic withdrawal failed:", e);
			return false;
		} finally {
			closeConnection(connection);
		}
	}
	
	private static void insertIssueToUsers(Connection con, UserBase user, double balance, long writerId, boolean eligible, String trx) throws SQLException{
		Issue issue = new Issue();
		String withdrawn = " should be withdrawn.";
		// Filling issue fields
		issue.setSubjectId(String.valueOf(IssuesManagerBase.ISSUE_SUBJ_CAP_SUSPEND));
		issue.setStatusId(String.valueOf(IssuesManagerBase.ISSUE_STATUS_FINISHED));
		issue.setUserId(user.getId());
		issue.setContactId(user.getContactId());
		issue.setPriorityId(String.valueOf(IssuesManagerBase.ISSUE_PRIORITY_HIGH));
		issue.setType(ConstantsBase.ISSUE_TYPE_REGULAR);
		IssuesDAOBase.insertIssue(con, issue);
		if(eligible) {
			IssueAction issueActionWD = new IssueAction();
			// Filling action fields
			issueActionWD.setChannelId(String.valueOf(IssuesManagerBase.ISSUE_CHANNEL_ACTIVITY));
			issueActionWD.setActionTypeId(String.valueOf(IssuesManagerBase.ISSUE_ACTION_TYPE_CAP_WD));
			issueActionWD.setWriterId(writerId);
			issueActionWD.setSignificant(false);
			issueActionWD.setActionTime(new Date());
			issueActionWD.setActionTimeOffset(WritersDAO.getWriterUtcOffset(con, writerId));
			issueActionWD.setComments("Withdrawal " + trx + " initiated at end of grace period." );
			issueActionWD.setIssueId(issue.getId());
			IssuesDAOBase.insertAction(con, issueActionWD);
			withdrawn = " was withdrawn.";
		}
		// Filling action fields
		IssueAction issueAction = new IssueAction();
		issueAction.setChannelId(String.valueOf(IssuesManagerBase.ISSUE_CHANNEL_ACTIVITY));
		issueAction.setActionTypeId(String.valueOf(IssuesManagerBase.ISSUE_ACTION_TYPE_CAP_SUSPEND));
		issueAction.setWriterId(writerId);
		issueAction.setSignificant(true);
		issueAction.setActionTime(new Date());
		issueAction.setActionTimeOffset(WritersDAO.getWriterUtcOffset(con, writerId));
		issueAction.setComments("Grace period ended, user suspended. User's balance "+ balance + withdrawn);
		issueAction.setIssueId(issue.getId());
		IssuesDAOBase.insertAction(con, issueAction);
	}
}
