package com.anyoption.backend.jobs.bonus;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.anyoption.backend.jobs.bonus.BonusCleanup.CleanupDetails;
import com.anyoption.backend.jobs.bonus.BonusCleanup.CleanupDetailsReport;
import com.anyoption.backend.jobs.bonus.BonusExpiration.ExpirationBonus;
import com.anyoption.common.beans.Writer;
import com.anyoption.common.beans.base.BonusUsers;
import com.anyoption.common.beans.base.Currency;
import com.anyoption.common.beans.base.Skin;
import com.anyoption.common.daos.DAOBase;
import com.anyoption.common.managers.TransactionsManagerBase;

import il.co.etrader.bl_vos.UserBase;
import il.co.etrader.dao_managers.BonusDAOBase;
import il.co.etrader.util.CommonUtil;
import il.co.etrader.util.ConstantsBase;

/**
 * @author kiril.mutafchiev
 */
class BonusJobsDAO extends DAOBase {

	static void insertAllUsersInTemporaryTable(Connection con, int period, int days, String startDate) throws SQLException {
    	PreparedStatement ps = null;
    	try {
    		String sql = 	"CREATE TABLE " + BonusCleanup.TEMPORARY_TABLE + " AS " +
	    					"SELECT " +
	    					"	bu.*, " +
	    					"	u.id as u_user_id, " +
		    				"	u.user_name," +
	                        "	u.first_name," +
	                        "	u.last_name," +
	                        "	u.skin_id," +
	                        "	u.email," +
	                        "	u.utc_offset, " +
	                        "	u.language_id, " +
	                        "	u.is_contact_by_email, " +
	                        "   u.writer_id as u_writer_id, " +
	    					//"	t.id as transaction_id, " +
	    					"	bt.name as bonus_type_name, " +
	    					//"	t.status_id as transaction_status_id, " +
	    					"	c.id as currency_id, " +
	    					"	c.symbol as currency_symbol, " +
	    					"	c.is_left_symbol as currency_is_left_symbol, " +
	    					"	c.code as currency_code, " +
	    					"	c.name_key as currency_name_key, " +
	    					"	c.display_name as currency_display_name, " +
	    					"	c.dec_pnt_digits as currency_dec_pnt_digits, " +
	    					"  	CASE WHEN EXISTS (SELECT 1 FROM investments i WHERE i.user_id = bu.user_id AND i.is_settled = 0) THEN 1 ELSE 0 END as open_investment, " +
	    					"	CASE WHEN EXISTS (SELECT 1 FROM transactions t WHERE t.bonus_user_id = bu.id) THEN 1 ELSE 0 END as has_bonus_tran, " +
	    					"	i.win as inv_win, " +
	    					"	i.bonus_odds_change_type_id, " +
	    					"	i.house_result " + 					
	    					"FROM " +
	    					"	bonus_users bu " +
	    					"		left join investments i on i.bonus_user_id = bu.id, " +
	    					"	bonus_types bt, " +
	    					//"	transactions t, " +
	    					"	users u, " +
	    					"	currencies c " +
	    					"WHERE " +
	    					"	u.id = bu.user_id AND " +
	    					//"	bu.id = t.bonus_user_id AND " +
	    					"	bt.id = bu.type_id AND " +
	    					"	c.id = u.currency_id AND " +
	    					"	bu.wagering_parameter > 0 AND " +
	    					//"	t.type_id = " + TransactionsManagerBase.TRANS_TYPE_BONUS_DEPOSIT + " AND " +
	    					"	bu.bonus_state_id in (" + ConstantsBase.BONUS_STATE_ACTIVE + ", " + ConstantsBase.BONUS_STATE_USED + ") AND " +
							" 	bu.time_activated <= case when bu.bonus_id = 99999991 then TRUNC(SYSDATE) - 7 else " +
							"   add_months(TRUNC(SYSDATE), -(decode(bu.type_id, 5, 1, " + period + "))) + " + days + " end AND " + //period month are over.   	
							" 	bu.sum_invest_withdraw_reached < bu.sum_invest_withdraw AND " + //didn't reach the required wagering.
							" 	bu.start_date >= to_date(" + "'" + startDate + "'" + ",'DD/MM/YYYY') ";
	    			sql +=  " AND u.class_id <> " + ConstantsBase.USER_CLASS_TEST + " ";
//	    					//run on specific user
//	    					if (!CommonUtil.isParameterEmptyOrNull(CommonUtil.getProperty("user.id", ConstantsBase.EMPTY_STRING))) {
//	    						sql+=	" AND u.id = " + CommonUtil.getProperty("user.id") + " ";
//	    					}
	    					
    				sql+=	"ORDER BY " +
    						"	bu.time_activated desc";
    		
    		ps = con.prepareStatement(sql);    		
			ps.executeUpdate();
    	} finally {
        	closeStatement(ps);			
		}	
	}

	static void dropTemporaryTable(Connection con) throws SQLException {
		PreparedStatement ps = null;
		try {
			String sql = "DROP TABLE " + BonusCleanup.TEMPORARY_TABLE;
			ps = con.prepareStatement(sql);
			ps.executeUpdate();
		} finally {
			closeStatement(ps);
		}
	}

	static List<CleanupDetailsReport> prepareCleanupReport(Connection con, BonusCleanup bonusCleanup) throws SQLException {
		PreparedStatement ps = null;
    	ResultSet rs = null;		
		List<CleanupDetailsReport> reportList = new ArrayList<>(); 	
    	try {
    		String sql = 
    					"SELECT " +
    					"	u.id as user_id, " +
    					"	u.user_name, " +
    					"	(t.amount - nvl(t1.amount,0))*t.rate  as amount, " +
    					"	bu.id as bonus_users_id, " +
    					"	bu.bonus_state_id, " +
    					"	bu.bonus_id as bonus_id " + 							
    					"FROM " +
    					"	transactions t " +
    					"		left join transactions t1 on t.bonus_user_id = t1.bonus_user_id and t1.type_id = " + TransactionsManagerBase.TRANS_TYPE_FIX_NEGATIVE_BALANCE + ", " +
    					"	users u, " +
    					"	bonus_users bu " +   				
    					"WHERE " +
    					"	t.user_id = u.id AND " +
    					"	t.bonus_user_id = bu.id AND " +
    					"	bu.user_id = u.id AND " +
    					"	t.type_id = " + TransactionsManagerBase.TRANS_TYPE_BONUS_WITHDRAW + " AND " +
    					"	t.status_id = " + TransactionsManagerBase.TRANS_STATUS_SUCCEED + " AND " +
    					"	t.bonus_user_id in (" +
    					"						SELECT " +
    					"								id " +
    					"						FROM " + 
    					"							 " + BonusCleanup.TEMPORARY_TABLE + 
    											") ";
    		if (CommonUtil.getProperty("only.live.users").equals("1")) {
    			sql +=  " 	AND u.class_id <> " + ConstantsBase.USER_CLASS_TEST + " ";
    		}
    		//run on specific user
			if (!CommonUtil.isParameterEmptyOrNull(CommonUtil.getProperty("user.id", ConstantsBase.EMPTY_STRING))) {
				sql+=	" 	AND u.id = ? ";
			}
    		ps = con.prepareStatement(sql);
    		if (!CommonUtil.isParameterEmptyOrNull(CommonUtil.getProperty("user.id", ConstantsBase.EMPTY_STRING))) {
    			ps.setString(1, CommonUtil.getProperty("user.id"));
    		}
    		rs = ps.executeQuery();
    		while (rs.next()) {
    			CleanupDetailsReport reportContent = bonusCleanup. new CleanupDetailsReport();
    			reportContent.setBonusId(rs.getLong("bonus_id"));
    			reportContent.setBonusStateId(rs.getLong("bonus_state_id"));
    			reportContent.setBonusUsersId(rs.getLong("bonus_users_id"));
    			reportContent.setDeductedAmount(rs.getDouble("amount"));
    			reportContent.setUserId(rs.getLong("user_id"));
    			reportContent.setUserName(rs.getString("user_name"));
    			
    			reportList.add(reportContent);
    		}
    	} finally {
			closeResultSet(rs);
        	closeStatement(ps);
		}
    	return reportList;
	}

	static List<CleanupDetails> getBonusUsersToCleanup(Connection conn, int period, boolean isNotification, int checkDays, BonusCleanup bonusCleanup) throws SQLException  {
		PreparedStatement ps = null;
    	ResultSet rs = null;
    	List<CleanupDetails> list = new ArrayList<>();
    	try {
			
			String sql =
					"SELECT " +
					"	* " +
					"FROM " +
					"	" + BonusCleanup.TEMPORARY_TABLE + " " +
					"WHERE" + 
					" 	time_activated <= case when bonus_id = 99999991 then TRUNC(SYSDATE) - 7 + 7 else " +
					"   add_months(TRUNC(SYSDATE), -(decode(type_id, 5, 1, " + period + "))) + " + checkDays + " end "; //period month are over.   
				if (isNotification) {
					sql +=
					"	AND case when bonus_id = 99999991 then TRUNC(SYSDATE) - 7 + 7 else add_months(TRUNC(SYSDATE), -(decode(type_id, 5, 1, " + period + "))) end <= time_activated " +
					" 	AND is_cleanup_email_sent = 0 ";
				}
			
			ps = conn.prepareStatement(sql);
			
    		rs = ps.executeQuery();
    		//The query include users with open investments.
    		while (rs.next()) {
    			CleanupDetails cd = bonusCleanup. new CleanupDetails();
    			cd.setBonusUsers(BonusDAOBase.getBonusUsersVO(rs, conn));
    			//
    			if ((rs.getInt("bonus_odds_change_type_id") > 0) && (rs.getLong("house_result") < 0) && rs.getInt("has_bonus_tran") == 0 && cd.getBonusUsers().getWageringParam() > 0) {
    				cd.getBonusUsers().setBonusAmount(rs.getLong("skin_id") == Skin.SKIN_ETRADER ? (Math.round(rs.getLong("inv_win") * (1 - ConstantsBase.TAX_PERCENTAGE))) : rs.getLong("inv_win"));
    			}
    			cd.setHasOpenInvestment(rs.getInt("open_investment") == 0 ? false : true);
    			//cd.setTransactionId(rs.getLong("transaction_id"));
    			//cd.setTransactionStatusId(rs.getInt("transaction_status_id"));
    			cd.getBonusUsers().setUserId(rs.getLong("user_id"));
    			cd.getBonusUsers().setUtcOffsetUser(rs.getString("utc_offset"));
    			cd.getBonusUsers().setBonusTypeName(rs.getString("bonus_type_name"));
    			cd.getBonusUsers().setWriterIdCancel(Writer.WRITER_ID_AUTO);
    			Currency currency = new Currency();
    			currency.setId(rs.getLong("currency_id"));
    			currency.setSymbol(rs.getString("currency_symbol"));
    			currency.setIsLeftSymbol(rs.getInt("currency_is_left_symbol"));
    			currency.setCode(rs.getString("currency_code"));
    			currency.setNameKey(rs.getString("currency_name_key"));
    			currency.setDisplayName(rs.getString("currency_display_name"));
    			currency.setDecimalPointDigits(rs.getInt("currency_dec_pnt_digits"));
    			cd.getBonusUsers().setCurrency(currency);
    			UserBase user = new UserBase();
				user.setId(rs.getLong("u_user_id"));
				user.setFirstName(rs.getString("first_name"));
				user.setLastName(rs.getString("last_name"));
				user.setEmail(rs.getString("email"));
				user.setSkinId(rs.getLong("skin_id"));
				user.setUtcOffset(rs.getString("utc_offset"));
				user.setUserName(rs.getString("user_name"));
				user.setLanguageId(rs.getLong("language_id"));
				user.setIsContactByEmail(rs.getInt("is_contact_by_email"));
				user.setWriterId(rs.getLong("u_writer_id"));
				user.setCurrency(currency);
    			cd.setUser(user);
    			list.add(cd);
    		}
    	} finally {
			closeResultSet(rs);
        	closeStatement(ps);
		}
    	return list;
	}

	static List<ExpirationBonus> retrieveExpirationBonus(Connection con, BonusExpiration bonusExpiration) throws SQLException {
        PreparedStatement ps = null;
        ResultSet rs = null;
        List<ExpirationBonus> list = new ArrayList<>();

        try {
		    String sql = "SELECT " +
		    				"bu.* , " +
		    				"u.user_name," +
                            "u.first_name," +
                            "u.last_name," +
                            "u.skin_id," +
                            "u.email," +
                            "u.utc_offset, " +
                            "u.id user_id," +
                            "u.language_id, " +
                            "u.is_contact_by_email, " +
                            "u.writer_id as u_writer_id, " +
		    				"bt.class_type_id, " +
		    				"(CASE WHEN sysdate > bu.end_date THEN 1 ELSE 0 END) AS is_expired " +
		    			 "FROM " +
		    			 	"bonus_users bu, " +
		    			 	"users u, " +
		    			 	"bonus_types bt " +
		    		     "WHERE " +
		    		     	"u.id = bu.user_id AND " +
                            "u.class_id > 0 AND " +
                            "((sysdate > bu.end_date) " +
                            	"OR " +
                               "(TRUNC(sysdate) = TRUNC((bu.end_date - " + ConstantsBase.BONUS_EMAIL_BEFORE_EXP_DAYS + ")) AND " +
                               "bu.is_exp_email_sent = 0) " +  // user didn't get email yet...
                            ") AND " +
		    		     	"bt.id = bu.type_id AND " +
		    		     	"( bu.bonus_state_id IN (" + ConstantsBase.BONUS_STATE_GRANTED + ", " + ConstantsBase.BONUS_STATE_PENDING + " ) OR " +
                               "( bu.bonus_state_id IN (" +  ConstantsBase.BONUS_STATE_ACTIVE + " ) AND " +
                                 " bt.IS_CAN_EXPIRE_ON_ACTIVE = 1)) " +
                        "ORDER BY " +
                        	"is_expired, " +
                        	"user_name ";

		    ps = con.prepareStatement(sql);
			rs = ps.executeQuery();

			while (rs.next()) {
				ExpirationBonus eb = bonusExpiration. new ExpirationBonus();
				list.add(eb);
				BonusUsers vo = BonusDAOBase.getBonusUsersVO(rs, con);
				vo.setUserName(rs.getString("user_name"));
				vo.setBonusClassType(rs.getLong("class_type_id"));
				vo.setUserSkinId(rs.getLong("skin_id"));
				vo.setWriterIdCancel(Writer.WRITER_ID_AUTO);
				boolean isExpired = rs.getLong("is_expired") == 1 ? true : false;
				eb.setExpiredBonus(isExpired);
				eb.setBonusUsers(vo);					
				if (!isExpired) {  // email notification
					UserBase user = new UserBase();
					user.setId(rs.getLong("user_id"));
					user.setFirstName(rs.getString("first_name"));
					user.setLastName(rs.getString("last_name"));
					user.setEmail(rs.getString("email"));
					user.setSkinId(rs.getLong("skin_id"));
					user.setUtcOffset(rs.getString("utc_offset"));
					user.setUserName(rs.getString("user_name"));
					user.setLanguageId(rs.getLong("language_id"));
					user.setIsContactByEmail(rs.getInt("is_contact_by_email"));
					user.setWriterId(rs.getLong("u_writer_id"));
					eb.setUserToNotify(user);
//					expirationBonus.getUsersToNotify().add(user);
				}
			}
         } finally {
        	 closeStatement(ps);
        	 closeResultSet(rs);
        }
        return list;
	}
}