package com.anyoption.backend.jobs.bonus;

import java.io.IOException;
import java.io.StringWriter;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.exception.MethodInvocationException;
import org.apache.velocity.exception.ParseErrorException;
import org.apache.velocity.exception.ResourceNotFoundException;

import com.anyoption.backend.jobs.bonus.BonusCleanup.CleanupDetails;
import com.anyoption.backend.jobs.bonus.BonusCleanup.CleanupDetailsReport;
import com.anyoption.backend.jobs.bonus.BonusExpiration.ExpirationBonus;
import com.anyoption.common.beans.Issue;
import com.anyoption.common.beans.IssueAction;
import com.anyoption.common.beans.base.BonusUsers;
import com.anyoption.common.daos.IssuesDAOBase;
import com.anyoption.common.managers.BaseBLManager;

import il.co.etrader.bl_managers.BonusManagerBase;
import il.co.etrader.bl_vos.Writer;
import il.co.etrader.dao_managers.BonusDAOBase;
import il.co.etrader.dao_managers.TemplatesDAO;
import il.co.etrader.util.CommonUtil;
import il.co.etrader.util.ConstantsBase;

/**
 * @author kiril.mutafchiev
 */
class BonusJobsManager extends BaseBLManager {

	private static final Logger log = Logger.getLogger(BonusJobsManager.class);

	static Connection getSingleConnection() throws SQLException {
		return getConnection();
	}

	static void insertAllUsersInTemporaryTable(Connection con, int period, int days, String startDate) throws SQLException {
		try {
			BonusJobsDAO.insertAllUsersInTemporaryTable(con, period, days, startDate);
		} catch (SQLException e) {
			log.error(BonusCleanup.LOG_PREFIX_MSG + "ERROR in insertAllUsersInTemporaryTable", e);
			throw e;
		}
	}

	static void dropTemporaryTable(Connection con) {
		if (con == null) {
			log.error(BonusCleanup.LOG_PREFIX_MSG	+ "ERROR! Failed to DROP temporary table " + BonusCleanup.TEMPORARY_TABLE
						+ ". Connection is null");
			return;
		}
		try {
			BonusJobsDAO.dropTemporaryTable(con);
			log.info(BonusCleanup.LOG_PREFIX_MSG + "DROP TABLE " + BonusCleanup.TEMPORARY_TABLE);
		} catch (SQLException e) {
			log.error(BonusCleanup.LOG_PREFIX_MSG + "ERROR! Failed to DROP temporary table " + BonusCleanup.TEMPORARY_TABLE, e);
		} finally {
			closeConnection(con);
		}
	}

	static List<CleanupDetailsReport> prepareCleanupReport(Connection con, BonusCleanup bonusCleanup) throws SQLException {
		try {
			return BonusJobsDAO.prepareCleanupReport(con, bonusCleanup);
		} catch (SQLException e) {
			log.error(BonusCleanup.LOG_PREFIX_MSG + "ERROR in prepareCleanupReport", e);
			throw e;
		}
	}

	static void insertIssue(Connection con, Issue issue) throws SQLException {
		IssuesDAOBase.insertIssue(con, issue);
	}

	static void insertAction(Connection con, IssueAction issueAction, long issueId) throws SQLException {
		issueAction.setIssueId(issueId);
		IssuesDAOBase.insertAction(con, issueAction);
	}

	static List<CleanupDetails> getBonusUsersToCleanup(	Connection con, int period, boolean isNotification,
														int checkDays, BonusCleanup bonusCleanup) throws SQLException {
		try {
			return BonusJobsDAO.getBonusUsersToCleanup(con, period, isNotification, checkDays, bonusCleanup);
		} catch (SQLException e) {
			log.error(BonusCleanup.LOG_PREFIX_MSG + "ERROR in getBonusUsersToCleanup", e);
			throw e;
		}
	}

	static void sendEmail(String body, String emailSubject, String emailTo) {
		sendEmail(body, emailSubject, emailTo, CommonUtil.getProperty("email.from.anyoption"));
	}

	static void sendEmail(String body, String emailSubject, String emailTo, long writerId) {
		String emailFrom = CommonUtil.getProperty("email.from.anyoption");
		if (writerId == com.anyoption.common.beans.Writer.WRITER_ID_COPYOP_WEB
				|| writerId == com.anyoption.common.beans.Writer.WRITER_ID_COPYOP_MOBILE
				|| writerId == com.anyoption.common.beans.Writer.WRITER_ID_CO_MINISITE) {
			emailFrom = emailFrom.replaceAll("anyoption", "copyop");
		}
		sendEmail(body, emailSubject, emailTo, emailFrom);
	}

    static void sendEmail(String body, String emailSubject, String emailTo, String emailFrom) {
    	//Server configuration
	    Hashtable<String,String> server = new Hashtable<String,String>();
	    server.put("url", CommonUtil.getProperty("email.server"));
	    server.put("auth", "true");
	    server.put("user", CommonUtil.getProperty("email.uname"));
	    server.put("pass", CommonUtil.getProperty("email.pass"));
	    server.put("contenttype", "text/html; charset=UTF-8");

	    //email configuration
	    Hashtable<String, String> email = new Hashtable<String, String>();
	    email.put("subject", emailSubject);
	    email.put("to", emailTo);
	    email.put("from", emailFrom);
	    email.put("body", body);
	    CommonUtil.sendEmail(server, email, null);
    }

	static String getEmailBody(Template velocityMailTemplate, Map<String, String> params) {
		VelocityContext context = new VelocityContext();
		String paramName;
		for (Iterator<String> keys = params.keySet().iterator(); keys.hasNext();) {
			paramName = (String) keys.next();
			context.put(paramName, params.get(paramName));
		}
		StringWriter sw = new StringWriter();
		try {
			velocityMailTemplate.merge(context, sw);
		} catch (Exception e) { //TODO fix after Jenkins valocity.jar is updated
		//catch (ResourceNotFoundException | ParseErrorException | MethodInvocationException e) { 
			log.debug("Unable to get email body", e);
		}
		return sw.toString();
	}

	static il.co.etrader.bl_vos.Template getTemplate(Connection con, int templateId) throws SQLException {
		return TemplatesDAO.get(con, templateId);
	}

	static void updateCleanupEmailSent(Connection con, long bonusUserId, boolean isCleanupEmailSent) throws SQLException {
		BonusDAOBase.updateCleanupEmailSent(con, bonusUserId, isCleanupEmailSent);
	}

	static List<ExpirationBonus> retrieveExpirationBonus(BonusExpiration bonusExpiration) {
		Connection con = null;
		try {
			con = getConnection();
			return BonusJobsDAO.retrieveExpirationBonus(con, bonusExpiration);
		} catch (SQLException e) {
			log.debug("Unable to retrieve expiration bonus users", e);
			return new ArrayList<>();
		} finally {
			closeConnection(con);
		}
	}

	static void updateExpEmailSent(Connection con, long bonusUsersId, long isExpEmailSent) throws SQLException {
		BonusDAOBase.updateExpEmailSent(con, bonusUsersId, isExpEmailSent);
	}

	static void updateBonusMissed(BonusUsers bonus) {
		Connection con = null;
		try {
			con = getConnection();
			BonusManagerBase.cancelBonusToUser(con, bonus, ConstantsBase.BONUS_STATE_MISSED, "GMT+00:00", Writer.WRITER_ID_AUTO, bonus.getUserSkinId(), 0L, "job");
		} catch (SQLException e) {
			log.debug("Unable to cancel bonus with id " + bonus.getId(), e);
		} finally {
			closeConnection(con);
		}
	}
}