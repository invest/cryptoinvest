package com.anyoption.backend.jobs.bonus;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import com.anyoption.common.beans.Issue;
import com.anyoption.common.beans.IssueAction;
import com.anyoption.common.beans.Job;
import com.anyoption.common.beans.Writer;
import com.anyoption.common.beans.base.BonusUsers;
import com.anyoption.common.jobs.ScheduledJob;
import com.anyoption.common.managers.LanguagesManagerBase;

import il.co.etrader.bl_managers.BonusManagerBase;
import il.co.etrader.bl_managers.IssuesManagerBase;
import il.co.etrader.bl_managers.UsersManagerBase;
import il.co.etrader.bl_vos.Template;
import il.co.etrader.bl_vos.UserBase;
import il.co.etrader.util.CommonUtil;
import il.co.etrader.util.ConstantsBase;
import il.co.etrader.util.SendTemplateEmail;

/**
 * @author liors
 * 
 * The following explanation is written here to give a general aspect and explanation to this Job (In high level).
 * 
 * This class represent and operates as a Job to handle BMS (Bonus Management System) logic.
 * The main & general Purpose of the BonusCleanup Job is to deduct X amount from the user balance.
 * The X amount will be (In general) the bonus amount that the user got from anyoption and all the profit that the user
 * made from the bonus benefit.
 * There are cases which the user balance will not be deducted.
 * BonusCleanup Job create several reports.
 * BonusCleanup Job can create and send inner message to the user.
 * 
 * Tax - in case user's skin_id =1 (ET) - because of tax issues , we will not deduct amount that is higher than original bonus amount.
 * 
 * Three months pass and the NIOU bonus is still in status 2 or 3, for the NIOU we will withdraw the MAX{BONUS_USERS.adjusted_amount , NIOU INVESTMENTS.win}
 * For ET we will simply withdraw NIOU INVESTMENTS.win - TAX
 */
public class BonusCleanup implements ScheduledJob {

    private static final Logger log = Logger.getLogger(BonusCleanup.class);
    private static final String BONUS_CLEANUP_CONFIG_DELIMITER = ",";
    private static final String BONUS_CLEANUP_CONFIG_PARAM_DELIMITER = "=";
    private static final String MAILBOX_NOTIFICATION_DAYS = "mailbox_notification_days";
    private static final String CLEANUP_EMAIL_TITLE = "cleanup_email_title";
    private static final String CLEANUP_EMAIL_SUBTITLE = "cleanup_email_subtitle";
    private static final String CLEANUP_EMAIL_SUBJECT = "cleanup_email_subject";
    private static final String CLEANUP_EMAIL_TO = "cleanup_email_to";
    private static final String NO_CLEANUP_EMAIL_TITLE = "no_cleanup_email_title";
    private static final String NO_CLEANUP_EMAIL_SUBTITLE = "no_cleanup_email_subtitle";
    private static final String NO_CLEANUP_EMAIL_SUBJECT = "no_cleanup_email_subject";
    private static final String NO_CLEANUP_EMAIL_TO = "no_cleanup_email_to";
    private static final String JOB_START_MONTHS_BACK = "start_months_back";
    private static final Map<String, String> jobConfig = new HashMap<>();
    static {
    	jobConfig.put(MAILBOX_NOTIFICATION_DAYS, null);
    	jobConfig.put(CLEANUP_EMAIL_TITLE, null);
    	jobConfig.put(CLEANUP_EMAIL_SUBTITLE, null);
    	jobConfig.put(CLEANUP_EMAIL_SUBJECT, null);
    	jobConfig.put(CLEANUP_EMAIL_TO, null);
    	jobConfig.put(NO_CLEANUP_EMAIL_TITLE, null);
    	jobConfig.put(NO_CLEANUP_EMAIL_SUBTITLE, null);
    	jobConfig.put(NO_CLEANUP_EMAIL_SUBJECT, null);
    	jobConfig.put(NO_CLEANUP_EMAIL_TO, null);
    	jobConfig.put(JOB_START_MONTHS_BACK, null);
    }

    private static Template mailTemplate;
    private static Map<String, org.apache.velocity.Template> velocityMailTemplates;

    static final String LOG_PREFIX_MSG = "Bonus Cleanup Job: ";
    static String TEMPORARY_TABLE = "bonus_cleanup_" + System.currentTimeMillis(); /*bonus_cleanup_1389518266808*/

    public boolean run() {
    	TEMPORARY_TABLE = "bonus_cleanup_" + System.currentTimeMillis(); // Tony: quick and durty fix for CORE-4380
		for (String key : jobConfig.keySet()) {
			if (jobConfig.get(key) == null) { // Note that if the configuration is wrong mail may not be sent
				log.debug("Incorrect configuration, sending mail and exiting. Configuraton is " + jobConfig);
				try {
					BonusJobsManager.sendEmail(	"Incorrect configuration for com.anyoption.backend.jobs.bonus.BonusExpiration job",
												jobConfig.get(CLEANUP_EMAIL_SUBJECT), jobConfig.get(CLEANUP_EMAIL_TO));
				} catch (Exception e) {
					log.debug("Unable to send mail");
				}
				return false;
			}
		}
    	log.info(LOG_PREFIX_MSG + " Start");
        long countCleanups = 0;
        long countNotCleanups = 0;
        long countSentMail = 0;
        String reportCleaned = "";
        String reportNotCleaned = "";
        Connection conn = null;
        try {
        	if (log.isEnabledFor(Level.INFO)) {
            	log.info(LOG_PREFIX_MSG + "about to active 'get Users to Cleanup' module\ntemporary table: " + TEMPORARY_TABLE);
            }
        	int days = Integer.valueOf(jobConfig.get(MAILBOX_NOTIFICATION_DAYS));
        	int months = Integer.valueOf(jobConfig.get(JOB_START_MONTHS_BACK));
        	Calendar cal = Calendar.getInstance();
        	cal.add(Calendar.MONTH, -months);
        	SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
			conn = BonusJobsManager.getSingleConnection();
        	BonusJobsManager.insertAllUsersInTemporaryTable(conn, ConstantsBase.BONUS_MANAGEMENT_TIME_FRAME, days, sdf.format(cal.getTime()));
            List<CleanupDetails> usersToClean = BonusJobsManager.getBonusUsersToCleanup(conn, ConstantsBase.BONUS_MANAGEMENT_TIME_FRAME, false, 0, this); //get users to clean. this list have also users with open investment.
            List<CleanupDetailsReport> usersCouldNotCleanup = null; //This list contain users that can't be cleaned due to open investment and bonus status used.
            if (!usersToClean.isEmpty()) {
            	usersCouldNotCleanup = fillBonusNotCleanedup(usersToClean); //users that has open investments and bonus is in status used => could not be clean.
            }
            List<CleanupDetails> usersToSendMailsNotification = BonusJobsManager.getBonusUsersToCleanup(conn, ConstantsBase.BONUS_MANAGEMENT_TIME_FRAME, true, days, this);
        	log.info(LOG_PREFIX_MSG + "about to active 'Mailbox + external email Notification' module");
        	mailTemplate = BonusJobsManager.getTemplate(conn, Template.BONUS_CLEANUP_MAIL_ID);
            for (CleanupDetails cleanupDetails : usersToSendMailsNotification) {
            	//External email.
            	HashMap<String,String> params = new HashMap<String, String>();
            	DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
            	params.put(SendTemplateEmail.PARAM_USER_FIRST_NAME, cleanupDetails.getUser().getFirstName());
            	params.put(SendTemplateEmail.PARAM_USER_NAME, cleanupDetails.getUser().getUserName());
            	params.put("dateBonusGranted", df.format(cleanupDetails.getBonusUsers().getTimeCreated()));
            	params.put("bonusType", cleanupDetails.getBonusUsers().getBonusTypeName());
                if (cleanupDetails.getUser().getIsContactByEmail() == 1) {
                	sendEmailWithIssueCreate(conn, mailTemplate.getFileName(), mailTemplate.getSubject(), cleanupDetails.getUser(), params);
                }
            	//MailBoxNotification.
                //TODO This section should be atomic. SendToMailBox method not return indication!
        		UsersManagerBase.SendToMailBox(cleanupDetails.getUser(), mailTemplate, Writer.WRITER_ID_AUTO, cleanupDetails.getUser().getLanguageId(), null, params, conn, 0);
        		BonusJobsManager.updateCleanupEmailSent(conn, cleanupDetails.getBonusUsers().getId(), true);
        		//TODO This section should be atomic. SendToMailBox method not return indication!
        		countSentMail++;
			}
        	log.info(LOG_PREFIX_MSG + " About to active 'Cleanup' module");
            StringBuilder reportContent = new StringBuilder();
        	for (CleanupDetails userToClean : usersToClean) {
        		//BonusCleanup main functionality.
        		boolean res = bonusCleanup(userToClean);
        		if (!res) {
					log.debug("Bonus with id "	+ userToClean.getBonusUsers().getId() + " for user with id " + userToClean.getUser().getId()
								+ " was not cleaned up");
        		}
			}
        	log.info(LOG_PREFIX_MSG + " About to Build Reports");
        	if (usersToClean != null && usersToClean.size() > 0) {
            	List<CleanupDetailsReport> reportDeductedDetails = BonusJobsManager.prepareCleanupReport(conn, this);
            	for (CleanupDetailsReport report : reportDeductedDetails) {
    				if (report.getBonusStateId() == ConstantsBase.BONUS_STATE_WITHDRAWN) {
    					countCleanups++;
    					reportContent.append(buildReportContent(report, true, countCleanups));
    				}
    			}   
            	reportCleaned = buildReport(reportContent, jobConfig.get(CLEANUP_EMAIL_TITLE), jobConfig.get(CLEANUP_EMAIL_SUBTITLE), true);
        	}
        	//Open investments - build report content
        	if (usersCouldNotCleanup != null && usersCouldNotCleanup.size() > 0) {
            	reportContent.delete(0, reportContent.length());
            	for (CleanupDetailsReport cleanupDetails : usersCouldNotCleanup) {
            		countNotCleanups++;
            		reportContent.append(buildReportContent(cleanupDetails, false, countNotCleanups));
    			}
            	reportNotCleaned = buildReport(reportContent, jobConfig.get(NO_CLEANUP_EMAIL_TITLE), jobConfig.get(NO_CLEANUP_EMAIL_SUBTITLE), false);
        	}
        } catch (Exception e) {
            log.error(LOG_PREFIX_MSG + "ERROR! Bonus Cleanup Job Failed.", e);
        } finally {
        	//Critical section
        	BonusJobsManager.dropTemporaryTable(conn);
        	BonusJobsManager.closeConnection(conn);
        }

        //send reports via email.
    	log.info(LOG_PREFIX_MSG + " About to send Reports if there are");
        if (!CommonUtil.isParameterEmptyOrNull(reportCleaned) && countCleanups != 0) {
			BonusJobsManager.sendEmail(reportCleaned, jobConfig.get(CLEANUP_EMAIL_SUBJECT), jobConfig.get(CLEANUP_EMAIL_TO));
			log.info(LOG_PREFIX_MSG	+ " Report was sent:\n" + "subject: " + jobConfig.get(CLEANUP_EMAIL_SUBJECT) + "to: "
						+ jobConfig.get(CLEANUP_EMAIL_TO) + "from: " + CommonUtil.getProperty("email.from.anyoption"));
        }
        if (!CommonUtil.isParameterEmptyOrNull(reportNotCleaned) && countNotCleanups != 0) {
        	BonusJobsManager.sendEmail(reportNotCleaned, jobConfig.get(NO_CLEANUP_EMAIL_SUBJECT), jobConfig.get(NO_CLEANUP_EMAIL_TO));
			log.info(LOG_PREFIX_MSG	+ " Report was sent:\n" + "subject: " + jobConfig.get(NO_CLEANUP_EMAIL_SUBJECT) + "to: "
						+ jobConfig.get(NO_CLEANUP_EMAIL_TO) + "from: " + CommonUtil.getProperty("email.from.anyoption"));
        }
        if (log.isEnabledFor(Level.INFO)) {
        	log.log(Level.INFO, LOG_PREFIX_MSG + "Summary:\n" +
        			"Cleanups: " + countCleanups + "\n" +
        			"Open investments (Not Cleaned): " + countNotCleanups + "\n" +
        			"Mails: " + countSentMail);
	        log.log(Level.INFO, LOG_PREFIX_MSG + "complete.\n");
	    }
        return true;
    }

	@Override
	public void setJobInfo(Job job) {
		velocityMailTemplates = new HashMap<>();
		if (job.getConfig() != null && !job.getConfig().trim().isEmpty()) {
			String[] config = job.getConfig().split(BONUS_CLEANUP_CONFIG_DELIMITER);
			for (String param : config) {
				String[] paramSplt = param.split(BONUS_CLEANUP_CONFIG_PARAM_DELIMITER);
				if (paramSplt.length == 2 && jobConfig.containsKey(paramSplt[0])) {
					jobConfig.put(paramSplt[0], paramSplt[1]);
				} else {
					log.warn("Incorect configuration parameter. Unexpected: " + param);
				}
			}
		}
	}

	@Override
	public void stop() {
		log.debug("BonusCleanup is stopping");
		velocityMailTemplates.clear();
		for (String key : jobConfig.keySet()) {
			jobConfig.put(key, null);
		}
		mailTemplate = null;
		log.debug("BonusCleanup stopped");
	}

	/**
	 * @param details
	 * @param isCleaned
	 * @param countCleanups
	 * @return
	 */
	private static StringBuffer buildReportContent(CleanupDetailsReport details, boolean isCleaned, long countCleanups) {
		StringBuffer report = new StringBuffer();
		String rowColor = "";
		if (countCleanups % 2 == 0) {
			rowColor = "#CEDEF4";
		}
        report.append("<tr bgcolor=" + rowColor + ">" +
        		"<td>" + countCleanups + "</td>" + 
        		"<td>" + details.getUserId() + "</td>" + 
        		"<td>" + details.getUserName() + "</td>" +
        		"<td>" + details.getBonusUsersId() + "</td>" +
        		"<td>" + details.getBonusId() + "</td>");
    	if (isCleaned) {
    		report.append("<td>" +  BigDecimal.valueOf(details.getDeductedAmount() / 100).setScale(2, RoundingMode.HALF_UP) + "</td>");
    	}
		report.append("</tr>");        	
        
		return report;		
	}

	/**
	 * @param reportContent
	 * @param title
	 * @param subTitle
	 * @param isCleaned
	 * @return
	 */
	private static String buildReport(StringBuilder reportContent, String title, String subTitle, boolean isCleaned) {
		StringBuffer reportHead = new StringBuffer();
        reportHead.append("<html><body>");
        reportHead.append("<h1>" + title + "</h1>");
		reportHead.append("<h4 style=\"color:#C0C0C0;\">" + subTitle + "</h4>");
		reportHead.append("<table border=\"1\">");        
		reportHead.append("<tr bgcolor=\"#3399FF\">" +
				"<th>#</th>" +
				"<th>User Id</th>" +
				"<th>User Name</th>" +
				"<th>Bonus Users Id</th>" +
				"<th>Bonus Id</th>");
		if (isCleaned) {
    		reportHead.append("<th>Deducted Amount($)</th>");
    	}
		reportHead.append("</tr>");
		
		StringBuffer reportTail = new StringBuffer();
		reportTail.append("</table>");
		reportTail.append("</body></html>");
		
		String report = reportHead + reportContent.toString() + reportTail;
		return report;		
	}

	private List<CleanupDetailsReport> fillBonusNotCleanedup(List<CleanupDetails> userList) {	
		List<CleanupDetailsReport> reportList = new ArrayList<BonusCleanup.CleanupDetailsReport>();
		
		for (CleanupDetails it : userList) {
			if (it.isHasOpenInvestment() && it.getBonusUsers().getBonusStateId() == ConstantsBase.BONUS_STATE_USED) {
				CleanupDetailsReport reportContent = new CleanupDetailsReport();
				reportContent.setBonusId(it.getBonusUsers().getBonusId());
				reportContent.setBonusUsersId(it.getBonusUsers().getId());
				reportContent.setUserId(it.getUser().getId());
				reportContent.setUserName(it.getUser().getUserName());
				reportList.add(reportContent);
			}
		}	
		return reportList;
	}

	private static boolean bonusCleanup(CleanupDetails userToClean) {
		boolean cancelSucc = false;
		String ls = System.getProperty("line.separator");
		try {
			if (log.isEnabledFor(Level.INFO)) {
				log.info(LOG_PREFIX_MSG + " About to Clean: " + ls + userToClean.toString());
			}
			String msg = BonusManagerBase.cancelBonus(	userToClean.getBonusUsers(), userToClean.getUser().getUtcOffset(),
														Writer.WRITER_ID_AUTO, userToClean.getUser().getSkinId(), userToClean.getUser(), 0L,
														"job");
			String[] msgParts = msg.split("_");
			if (!CommonUtil.isParameterEmptyOrNull(msgParts[0]) && msgParts[0].equalsIgnoreCase("A")) {
				cancelSucc = true;
			}
		} catch (SQLException e) {
			log.error(ls	+ LOG_PREFIX_MSG + "ERROR in bonusCleanup" + ls + "UserId: " + userToClean.getBonusUsers().getUserId() + ls
						+ "bonus_users_id: " + userToClean.getBonusUsers().getId() + ls, e);
		}
		return cancelSucc;
	}

    private void sendEmailWithIssueCreate(Connection con, String templateFileName, String subjectKey, UserBase user, Map<String, String> params) throws Exception {
    	String lngCode = LanguagesManagerBase.getLanguage(user.getLanguageId()).getCode();
    	String key = lngCode + "_" + user.getSkinId();
    	org.apache.velocity.Template velocityMailTemplate = velocityMailTemplates.get(key);
    	if (velocityMailTemplate == null) {
    		velocityMailTemplate = SendTemplateEmail.getEmailTemplate(templateFileName, lngCode, user.getSkinId(), CommonUtil.getProperty("templates.path"), user.getWriterId(), 0);
    		velocityMailTemplates.put(key, velocityMailTemplate);
    	}
    	String body = BonusJobsManager.getEmailBody(velocityMailTemplate, params);
    	BonusJobsManager.sendEmail(body, CommonUtil.getMessage(new Locale(lngCode), subjectKey, null), user.getEmail(), user.getWriterId());

		Issue issue = new Issue();
		IssueAction issueAction = new IssueAction();
		issue.setSubjectId(String.valueOf(IssuesManagerBase.ISSUE_SUBJ_GENERAL));
		issue.setStatusId(String.valueOf(IssuesManagerBase.ISSUE_STATUS_FINISHED));
		issue.setUserId(user.getId());
		issue.setContactId(user.getContactId());
		issue.setContactId(user.getContactId());
		issue.setPriorityId(String.valueOf(IssuesManagerBase.ISSUE_PRIORITY_LOW));
		issue.setType(ConstantsBase.ISSUE_TYPE_REGULAR);
		issueAction.setChannelId(String.valueOf(IssuesManagerBase.ISSUE_CHANNEL_EMAIL));
		issueAction.setActionTypeId(String.valueOf(IssuesManagerBase.ISSUE_ACTION_EMAIL));
		issueAction.setWriterId(Writer.WRITER_ID_AUTO);
		issueAction.setSignificant(false);
		issueAction.setActionTime(new Date());
		issueAction.setActionTimeOffset(user.getUtcOffset());
		issueAction.setComments(CommonUtil.getMessage(new Locale(lngCode), subjectKey, null));
		issueAction.setTemplateId(mailTemplate.getId());
		BonusJobsManager.insertIssue(con, issue);
		BonusJobsManager.insertAction(con, issueAction, issue.getId());
    }

    class CleanupDetails {
    	private BonusUsers bonusUsers;
    	private UserBase user;
    	private boolean hasOpenInvestment;

    	public CleanupDetails() {
    		bonusUsers = new BonusUsers();
    	}

		public BonusUsers getBonusUsers() {
			return bonusUsers;
		}

		public void setBonusUsers(BonusUsers bonusUsers) {
			this.bonusUsers = bonusUsers;
		}

		public boolean isHasOpenInvestment() {
			return hasOpenInvestment;
		}

		public void setHasOpenInvestment(boolean hasOpenInvestment) {
			this.hasOpenInvestment = hasOpenInvestment;
		}

		public UserBase getUser() {
			return user;
		}

		public void setUser(UserBase user) {
			this.user = user;
		}

		@Override
		public String toString() {
			return "[bonusUsersId=" + bonusUsers.getId()
					+ ", bonusId=" + bonusUsers.getBonusId()
					+ ", bonusTypeId=" + bonusUsers.getTypeId()
					+ ", bonusStateId=" + bonusUsers.getBonusStateId()
					+ ", bonusAmount=" + bonusUsers.getBonusAmount()
					+ ", adjustedAmount=" + bonusUsers.getAdjustedAmount()
					+ ", timeActivated=" + bonusUsers.getTimeActivated()
					+ ", startDate=" + bonusUsers.getStartDate()
					+ ", userId=" + bonusUsers.getUserId()
					+ ", userSkinId=" + user.getSkinId()
					+ ", hasOpenInvestment=" + hasOpenInvestment + "]";
		}
    }
    
    class CleanupDetailsReport {
    	private long userId;
    	private long bonusUsersId;
    	private String userName;
    	private long bonusId;
    	private double deductedAmount;
    	private long bonusStateId;

		public long getUserId() {
			return userId;
		}

		public void setUserId(long userId) {
			this.userId = userId;
		}

		public long getBonusUsersId() {
			return bonusUsersId;
		}

		public void setBonusUsersId(long bonusUsersId) {
			this.bonusUsersId = bonusUsersId;
		}

		public String getUserName() {
			return userName;
		}

		public void setUserName(String userName) {
			this.userName = userName;
		}

		public long getBonusId() {
			return bonusId;
		}

		public void setBonusId(long bonusId) {
			this.bonusId = bonusId;
		}

		public double getDeductedAmount() {
			return deductedAmount;
		}

		public void setDeductedAmount(double deductedAmount) {
			this.deductedAmount = deductedAmount;
		}

		public long getBonusStateId() {
			return bonusStateId;
		}

		public void setBonusStateId(long bonusStateId) {
			this.bonusStateId = bonusStateId;
		}
    }
}