package com.anyoption.backend.jobs.bonus;

import java.sql.Connection;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.Job;
import com.anyoption.common.beans.base.BonusUsers;
import com.anyoption.common.jobs.ScheduledJob;
import com.anyoption.common.managers.LanguagesManagerBase;

import il.co.etrader.bl_managers.UsersManagerBase;
import il.co.etrader.bl_vos.Template;
import il.co.etrader.bl_vos.UserBase;
import il.co.etrader.bl_vos.Writer;
import il.co.etrader.util.CommonUtil;
import il.co.etrader.util.ConstantsBase;
import il.co.etrader.util.SendTemplateEmail;

/**
 * bonusExpirationJob class. This class represent the bonus Expiration job. We need to update bonus Expiration of users that didnt used the
 * bonus and thier bonus expired The trigger for the update is "end_date < current_date" field in bonus_users table. Email notification sent
 * to the user 3 days before the bonus cancelation
 *
 * @author Eyal
 */
public class BonusExpiration implements ScheduledJob {

	private static final Logger log = Logger.getLogger(BonusExpiration.class);
	private static Map<String, org.apache.velocity.Template> velocityMailTemplates;
	private static final String BONUS_EXPIRATION_EMAIL_SUBJECT = "bonus_email_subject";
	private static final String BONUS_EXPIRATION_EMAIL_TO = "bonus_email_to";
	private static final String BONUS_EXPIRATION_EMAIL_FROM = "bonus_email_from";
	private static final String BONUS_EXPIRATION_CONFIG_DELIMITER = ",";
	private static final String BONUS_EXPIRATION_CONFIG_PARAM_DELIMITER = "=";
	private static final Map<String, String> jobConfig = new HashMap<>();
	static {
		jobConfig.put(BONUS_EXPIRATION_EMAIL_SUBJECT, null);
		jobConfig.put(BONUS_EXPIRATION_EMAIL_TO, null);
		jobConfig.put(BONUS_EXPIRATION_EMAIL_FROM, null);
	}

	public boolean run() {
		for (String key : jobConfig.keySet()) {
			if (jobConfig.get(key) == null) { // Note that if the configuration is wrong mail may not be sent
				log.debug("Incorrect configuration, sending mail and exiting. Configuraton is " + jobConfig);
				try {
					BonusJobsManager.sendEmail(	"Incorrect configuration for com.anyoption.backend.jobs.bonus.BonusExpiration job",
												jobConfig.get(BONUS_EXPIRATION_EMAIL_SUBJECT), jobConfig.get(BONUS_EXPIRATION_EMAIL_TO),
												jobConfig.get(BONUS_EXPIRATION_EMAIL_FROM));
				} catch (Exception e) {
					log.debug("Unable to send mail");
				}
				return false;
			}
		}
		log.info("Starting the bonus Expiration job");
		log.info("Starting to retrieve Expiration bonuses that need to be update");
		List<ExpirationBonus> expirationBonusList = BonusJobsManager.retrieveExpirationBonus(this);
		String report;
		try {
			report = buildReportAndNotifyUsers(expirationBonusList);
		} catch (Exception e) {
			log.debug("Cannot build report and notify users, exiting", e);
			return false;
		}
		log.info("Starting to update bonuses");
		// Update users columns to lock the users
		for (ExpirationBonus expirationBonus : expirationBonusList) {
			if (expirationBonus.isExpiredBonus()) {
				BonusJobsManager.updateBonusMissed(expirationBonus.getBonusUsers());
			}
		}
		// build the report
		String reportBody = "<html><body>" + report + "</body></html>";
		log.info("Sending report email: " + reportBody);
		BonusJobsManager.sendEmail(	reportBody, jobConfig.get(BONUS_EXPIRATION_EMAIL_SUBJECT), jobConfig.get(BONUS_EXPIRATION_EMAIL_TO),
									jobConfig.get(BONUS_EXPIRATION_EMAIL_FROM));
		log.info("bonusExpirationJob completed, bonus was updated");
		return true;
	}

	@Override
	public void setJobInfo(Job job) {
		velocityMailTemplates = new HashMap<>();
		if (job.getConfig() != null && !job.getConfig().trim().isEmpty()) {
			String[] config = job.getConfig().split(BONUS_EXPIRATION_CONFIG_DELIMITER);
			for (String param : config) {
				String[] paramSplt = param.split(BONUS_EXPIRATION_CONFIG_PARAM_DELIMITER);
				if (paramSplt.length == 2 && jobConfig.containsKey(paramSplt[0])) {
					jobConfig.put(paramSplt[0], paramSplt[1]);
				} else {
					log.warn("Incorect configuration parameter. Unexpected: " + param);
				}
			}
		}
	}

	@Override
	public void stop() {
		log.debug("BonusExpiration is stopping");
		velocityMailTemplates.clear();
		for (String key : jobConfig.keySet()) {
			jobConfig.put(key, null);
		}
		log.debug("BonusExpiration stopped");
	}

	private String buildReportAndNotifyUsers(List<ExpirationBonus> expirationBonusList) throws Exception {
		Connection con = null;
		StringBuilder report = new StringBuilder("<br><b>bonusExpirationJob Information:</b><table border=\"1\">");
		try {
			report.append("<tr><td><b>Bonus Users Id</b></td><td><b>User Id</b></td><td><b>Action</b></td><td><b>Bonus Amount</b></td><td><b>Start Date</b></td></tr>");
			con = BonusJobsManager.getSingleConnection();
			Template mailTemplate = BonusJobsManager.getTemplate(con, Template.BONUS_CLEANUP_MAIL_ID);
			for (ExpirationBonus expirationBonus : expirationBonusList) {
				String actionStr;
				if (expirationBonus.isExpiredBonus()) {
					actionStr = "Cancel bonus";
				} else {
					actionStr = "Email";
					if (expirationBonus.getUserToNotify().getIsContactByEmail() == 1) {
						HashMap<String, String> params = new HashMap<String, String>();
						params.put(SendTemplateEmail.PARAM_USER_FIRST_NAME, expirationBonus.getUserToNotify().getFirstName());
						params.put(SendTemplateEmail.PARAM_USER_LAST_NAME, expirationBonus.getUserToNotify().getLastName());
						params.put(SendTemplateEmail.PARAM_BONUS_EXP_DAYS, String.valueOf(ConstantsBase.BONUS_EMAIL_BEFORE_EXP_DAYS));

						String lngCode = LanguagesManagerBase.getLanguage(expirationBonus.getUserToNotify().getLanguageId()).getCode();
						String key = lngCode + "_" + expirationBonus.getUserToNotify().getSkinId();
						org.apache.velocity.Template velocityMailTemplate = velocityMailTemplates.get(key);
						if (velocityMailTemplate == null) {
							velocityMailTemplate = SendTemplateEmail.getEmailTemplate(	mailTemplate.getFileName(), lngCode,
																						expirationBonus.getUserToNotify().getSkinId(),
																						CommonUtil.getProperty("templates.path"),
																						expirationBonus.getUserToNotify().getWriterId(), 0);
							velocityMailTemplates.put(key, velocityMailTemplate);
						}
						String body = BonusJobsManager.getEmailBody(velocityMailTemplate, params);
						BonusJobsManager.sendEmail(	body, CommonUtil.getMessage(new Locale(lngCode), mailTemplate.getSubject(), null),
													expirationBonus.getUserToNotify().getEmail(),
													expirationBonus.getUserToNotify().getWriterId());
						UsersManagerBase.SendToMailBox(	expirationBonus.getUserToNotify(), mailTemplate, Writer.WRITER_ID_AUTO,
														expirationBonus.getUserToNotify().getLanguageId(), null, con, 0);
						BonusJobsManager.updateExpEmailSent(con, expirationBonus.getBonusUsers().getId(), 1);
					}
				}
				report.append("<tr>");
				report.append("<td>").append(expirationBonus.getBonusUsers().getId()).append("</td>");
				report.append("<td>").append(expirationBonus.getBonusUsers().getUserId()).append("</td>");
				report.append("<td>").append(actionStr).append("</td>");
				report.append("<td>").append(expirationBonus.getBonusUsers().getBonusAmount() / 100).append("</td>");
				report.append("<td>").append(expirationBonus.getBonusUsers().getStartDate()).append("</td>");
				report.append("</tr>");
			}
		} catch (Exception e) {
			log.debug("Unable to build report and send mails", e);
			throw e;
		} finally {
			BonusJobsManager.closeConnection(con);
		}
		return report.toString();
	}

	class ExpirationBonus {

		private BonusUsers bonusUsers;
		private UserBase userToNotify;
		private boolean expiredBonus;

		public BonusUsers getBonusUsers() {
			return bonusUsers;
		}

		public void setBonusUsers(BonusUsers bonusUsers) {
			this.bonusUsers = bonusUsers;
		}

		public UserBase getUserToNotify() {
			return userToNotify;
		}

		public void setUserToNotify(UserBase userToNotify) {
			this.userToNotify = userToNotify;
		}

		public boolean isExpiredBonus() {
			return expiredBonus;
		}

		public void setExpiredBonus(boolean expiredBonus) {
			this.expiredBonus = expiredBonus;
		}
	}
}