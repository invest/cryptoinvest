package com.anyoption.backend.jobs;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Properties;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import javax.activation.DataHandler;
import javax.activation.FileDataSource;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import com.anyoption.common.beans.Issue;
import com.anyoption.common.beans.IssueAction;
import com.anyoption.common.beans.Job;
import com.anyoption.common.beans.Transaction;
import com.anyoption.common.beans.UserMigration;
import com.anyoption.common.daos.GeneralDAO;
import com.anyoption.common.daos.InvestmentsDAOBase;
import com.anyoption.common.jobs.ScheduledJob;
import com.anyoption.common.managers.BaseBLManager;
import com.anyoption.common.managers.InvestmentsManagerBase;
import com.anyoption.common.managers.TransactionsManagerBase;

import il.co.etrader.backend.dao_managers.TransactionsDAO;
import il.co.etrader.backend.dao_managers.UsersDAO;
import il.co.etrader.backend.util.Constants;
import il.co.etrader.bl_managers.IssuesManagerBase;
import il.co.etrader.dao_managers.IssuesDAOBase;
import il.co.etrader.util.CommonUtil;
import il.co.etrader.util.ConstantsBase;

public class InvestMigrationJob extends BaseBLManager implements ScheduledJob {
	private static Logger log = Logger.getLogger(InvestMigrationJob.class);
    HashMap<String, String> jobProperties;
    
    private boolean isTestRun = false;
    private String emails ;
    private String fileName = "usersTranfer";
    
    
	@Override
	public void setJobInfo(Job job) {
		jobProperties = new HashMap<String, String>();
		if (null != job.getConfig() && !job.getConfig().trim().equals("")) {
            String[] ps = job.getConfig().split(",");
            for (int i = 0; i < ps.length; i++) {
            	String[] p = ps[i].split("=");
            	jobProperties.put(p[0], p[1]);
            }
		}
		String test = jobProperties.get("test");
		emails = jobProperties.get("emails");
		fileName = jobProperties.get("fileName");
		isTestRun = test != null && test.equalsIgnoreCase("true");
	}

	@Override
	public boolean run() {
		log.info("Starting Invest Migration Job.");
		boolean r =  migrate();
		log.info("End Invest Migration Job.");
		return r;	}

	@Override
	public void stop() {
		// TODO Auto-generated method stub
		
	}

	private boolean migrate() {
		Connection connection = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			connection = getConnection();
			
			String sql = " SELECT imu.*, im.TARGET_PLATFORM, u.balance ub, (u.STREET || ' ' || u.STREET_NO ) AS address, u.CITY_NAME, u.STATE_CODE, u.ZIP_CODE, u.TIME_CREATED, u.GENDER, u.ip FROM INVEST_MIGRATION_USERS imu, users u, INVEST_MIGRATION im where imu.TIME_PROCESSED is null AND imu.ACCEPTED_TRANSFER = 1 AND imu.USER_ID = u.id AND u.id = im.USER_ID";
			
			pstmt = connection.prepareStatement(sql);

			rs = pstmt.executeQuery();
			// create file
			String suffix = new SimpleDateFormat("yyyyMMddHHmm'.csv'").format(new Date());
			String filePath1 = CommonUtil.getProperty(Constants.FILES_SERVER_PATH) + fileName + "_invest_" + suffix ;
			String filePath2 = CommonUtil.getProperty(Constants.FILES_SERVER_PATH) + fileName + "_webtrade_" +suffix ;
			
			PrintWriter writer1 = new PrintWriter(new File(filePath1), "UTF-8");
			PrintWriter writer2 = new PrintWriter(new File(filePath2), "UTF-8");
			
			boolean file1 = false;
			boolean file2 = false;
			
			long start = System.nanoTime();
			writer1.println(UserMigration.headerCsv());
			writer2.println(UserMigration.headerCsv());
			while (rs.next()) {
				try{
					UserMigration um = new UserMigration(
															rs.getLong("user_id"),
															rs.getLong("balance"), 
															rs.getInt("accepted_transfer")==1,
															rs.getString("first_name"),
															rs.getString("last_name"),
															rs.getString("email"),
															rs.getString("phone"),
															rs.getString("country"),
															rs.getString("currency")
														);
					um.setLanguage(rs.getString("language"));
					um.setAddress(rs.getString("address"));
					um.setCity(rs.getString("CITY_NAME"));
					um.setGender(rs.getString("gender"));
					um.setState(rs.getString("STATE_CODE"));
					um.setZip(rs.getString("zip_code"));
					um.setRegistered(rs.getDate("time_created").toString());
					um.setIp(rs.getString("ip"));
					um.setAcceptedMailing(rs.getInt("accepted_mailing")==1);
					um.setTargetPlatform(rs.getInt("TARGET_PLATFORM"));
					
					// save to file
					if(um.getTargetPlatform() == 1) {
						writer1.println(um.toCsv());
						file1 = true;
					} else if (um.getTargetPlatform() == 2) {
						writer2.println(um.toCsv());
						file2 = true;
					} else {
						log.error("UNRECOGNIZED TARGET PLATFORM:"+um.getTargetPlatform());
					}
					
					// update TIME_PROCESSED
					updateTimeProcessed(um.getUserId(), um.getBalance());
				}catch(SQLException ex) {
					log.error("Unable to process user", ex);
				}
			}
			// close file
			writer1.close();
			writer2.close();
			// zip file
			//String zip = zipFile(filePath);
			//File[] f = {new File(zip)};

			// send file to emails
			if(file1 && file2) {
				sendEmail("Transfer csv file", "Anyoption to IDC", emails, new File[]{new File(filePath1), new File(filePath2)});
			} else if( file1) {
				sendEmail("Transfer csv file", "Anyoption to IDC", emails, new File[]{new File(filePath1)});
				log.error("Webtrade File is empty ");
			} else if( file2) {
				sendEmail("Transfer csv file", "Anyoption to IDC", emails, new File[]{new File(filePath2)});
				log.error("Invest File is empty ");
			} else {
				log.error("Files are empty nothing to send");
			}
			long end = System.nanoTime();
			double seconds = (double)(end - start) / 1000000000.0;
			log.info("Time elapsed (seconds):" + seconds); 
			return true;
		} catch(Throwable e) {
			log.error("Automatic withdrawal failed:", e);
			try {
				sendEmail("Error in migration job", e.getMessage(), "pavlin.m@invest.com", null);
			} catch (Exception e1) {}
			return false;
		} finally {
			closeConnection(connection);
		}
					
	}
	private static void updateTimeProcessed(long user_id, long currentBalance) throws SQLException {
		Connection connection = null;
		try	{
			connection = getConnection();
			String updateSql = "UPDATE INVEST_MIGRATION_USERS set TIME_PROCESSED=SYSDATE, balance = ? where user_id = ?";
			PreparedStatement pstmt = connection.prepareStatement(updateSql);
			pstmt.setLong(1, currentBalance);
			pstmt.setLong(2, user_id);
			pstmt.executeUpdate();
		} finally {
			closeConnection(connection);
		}
	}
	
    private static String zipFile(String filePath) {
        try {
            File file = new File(filePath);
            String zipFileName = file.getName().concat(".zip");
 
            FileOutputStream fos = new FileOutputStream(zipFileName);
            ZipOutputStream zos = new ZipOutputStream(fos);
 
            zos.putNextEntry(new ZipEntry(file.getName()));
 
            byte[] bytes = Files.readAllBytes(Paths.get(filePath));
            zos.write(bytes, 0, bytes.length);
            zos.closeEntry();
            zos.close();
            return zipFileName;
        } catch (FileNotFoundException ex) {
            System.err.format("The file %s does not exist", filePath);
        } catch (IOException ex) {
            System.err.println("I/O error: " + ex);
        }
        return null;
    }



	public static void sendEmail(String body, String subject, String to, File[] attachment) throws Exception {
			final String username = CommonUtil.getProperty("email.uname");
			final String password = CommonUtil.getProperty("email.pass");
	
			Properties serverProperties = new Properties();
			serverProperties.put("mail.smtp.auth", "true");
			serverProperties.put("mail.smtp.starttls.enable", "false");
			serverProperties.put("mail.smtp.host", CommonUtil.getProperty("email.server"));
	
			Session session = Session.getInstance(serverProperties, new javax.mail.Authenticator() {
				@Override
				protected PasswordAuthentication getPasswordAuthentication() {
					return new PasswordAuthentication(username, password);
				}
			});

	        try {

	            Message message = new MimeMessage(session);
	            message.setFrom(new InternetAddress("bgjob@invest.com"));
	            message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(to));
	            message.setSubject(subject);
	            //message.setText(body);

				// set the email Text
				MimeBodyPart messagePart = new MimeBodyPart();
				messagePart.setContent(body, "text/html; charset=UTF-8");
				
				Multipart multipart = new MimeMultipart();
				// set the email attachment file
				if(attachment != null) {
					for(int i=0;i<attachment.length;i++) {
						MimeBodyPart attachmentPart;
		
						FileDataSource fileDataSource = new FileDataSource(attachment[i]) {
							@Override
							public String getContentType() {
								return "application/octet-stream";
							}
						};
						attachmentPart = new MimeBodyPart();
						attachmentPart.setDataHandler(new DataHandler(fileDataSource));
						attachmentPart.setFileName(attachment[i].getName());
						multipart.addBodyPart(attachmentPart);
					}
				}
				multipart.addBodyPart(messagePart);


				message.setContent(multipart);
	            
	            Transport.send(message);

	            log.debug("Email sent.");

	        } catch (MessagingException e) {
	            throw new RuntimeException(e);
	        }
    }
	public static void main(String args[]) {
		try {
			InvestMigrationJob.sendEmail("test", "tst sbj", "pavlin.m@invest.com", new File[]{new File("C:/docs/ao2idc201710111312.txt"), new File("C:/docs/ao2idc201710111314.txt")});
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}