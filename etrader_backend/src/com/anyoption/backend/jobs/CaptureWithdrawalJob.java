package com.anyoption.backend.jobs;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Locale;
import java.util.Map;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.velocity.VelocityContext;

import com.anyoption.common.beans.Job;
import com.anyoption.common.beans.Transaction;
import com.anyoption.common.beans.base.Skin;
import com.anyoption.common.clearing.ClearingException;
import com.anyoption.common.clearing.ClearingInfo;
import com.anyoption.common.clearing.ClearingManagerConstants;
import com.anyoption.common.daos.LanguagesDAOBase;
import com.anyoption.common.jobs.ScheduledJob;
import com.anyoption.common.managers.BaseBLManager;
import com.anyoption.common.managers.CurrenciesManagerBase;
import com.anyoption.common.managers.LanguagesManagerBase;
import com.anyoption.common.util.AESUtil;
import com.anyoption.common.util.ClearingUtil;
import com.anyoption.common.util.SendTemplateEmail;

import il.co.etrader.bl_managers.ClearingManager;
import il.co.etrader.bl_managers.TransactionsManagerBase;
import il.co.etrader.bl_managers.UsersManagerBase;
import il.co.etrader.bl_vos.Template;
import il.co.etrader.bl_vos.UserBase;
import il.co.etrader.bl_vos.Writer;
import il.co.etrader.clearing.jobs.ReportSummary;
import il.co.etrader.dao_managers.SkinsDAOBase;
import il.co.etrader.dao_managers.TemplatesDAO;
import il.co.etrader.dao_managers.TransactionsDAOBase;
import il.co.etrader.dao_managers.UsersDAOBase;
import il.co.etrader.util.CommonUtil;
import il.co.etrader.util.ConstantsBase;
/**
 * Go over the pending transactions from the previous day and make "capture" on them.
 */
public class CaptureWithdrawalJob extends BaseBLManager implements ScheduledJob {
    private static Logger log = Logger.getLogger(CaptureWithdrawalJob.class);

    private String lastCurrencyCode = null;
    private long newProviderId = 0;

    private boolean isTestRun = false;
    private boolean hasMissedTrx = false;
    
    // Constants
    private static final int RUN_CFT	= 1;
    private static final int RUN_CREDIT	= 2;  // (credit/bookBack)
    private static final int DONT_RUN	= 0;

    private StringBuilder missedTransactions;
    private StringBuilder report;
    private ReportSummary summary;
    Map<String, org.apache.velocity.Template> velocityMailTemplates;
    
    HashMap<String, String> jobProperties;
    
    @Override
    public void setJobInfo(Job job) {
    	if(job.getRunning() == 1 ) {
    		hasMissedTrx = gatherMissedTransacations(job.getLastRunTime());
    	}
    	summary = new ReportSummary();
    	report = new StringBuilder();
    	velocityMailTemplates = new HashMap<>();
    	
    	jobProperties = new HashMap<String, String>();
		if (null != job.getConfig() && !job.getConfig().trim().equals("")) {
            String[] ps = job.getConfig().split(",");
            for (int i = 0; i < ps.length; i++) {
            	String[] p = ps[i].split("=");
            	jobProperties.put(p[0], p[1]);
            }
		}
		String test = jobProperties.get("test");
		isTestRun = test != null && test.equalsIgnoreCase("true");
    }
    
	@Override
	public void stop() {
		
	}

    /**
     * Capture Withdrawal Job.
     * @param args
     */
	@Override
	public boolean run() {
        log.info("Starting CaptureWithdrawalJob job.");
        
        captureWithdrawalTransactions();
        sendReport();
        
        log.info("CaptureWithdrawal Job completed.");
        return true;
    }
	
    private String getProperty(String key, String defaultValue) {
    	String value = jobProperties.get(key); 
    	return value == null ? defaultValue : value;
    }
    
  
	/**
	 * Goes over the approved (by accounting) transactions in the db 
	 * from the previous day and make a "capture" request for each of them
	 */
	private void captureWithdrawalTransactions() {
		String reportHead = "<br><b>Capture withdraw transactions:</b><table border=\"1\">";
		if(hasMissedTrx) {
			report = new StringBuilder(missedTransactions);
			report.append(reportHead);
		} else {
			report = new StringBuilder(reportHead);
		}
		report.append("<tr><td>User id</td><td>Transaction id</td><td>Provider</td><td>Status</td>" +
					  "<td>Description</td><td>Amount</td><td>Currency</td><td>Type</td></tr>");
		Connection conn1 = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		String utcOffset = ConstantsBase.EMPTY_STRING;

		String transactionId = getProperty("transaction.id", ConstantsBase.EMPTY_STRING);		
		String inatecFormer =	ClearingManagerConstants.INATEC_PROVIDER_ID_EUR + "," +
								ClearingManagerConstants.INATEC_PROVIDER_ID_USD + "," +
								ClearingManagerConstants.INATEC_PROVIDER_ID_GBP + "," +
								ClearingManagerConstants.INATEC_PROVIDER_ID_TRY ;

		try {
			conn1 = getConnection();
            ClearingManager.loadClearingProviders(conn1);
			String sql =
            	"SELECT " +
                    "c.holder_id_num idnum, " +
                    "t.user_id userid, " +
                    "t.id tid, " +
                    "c.cc_number ccnumber, " +
                    "c.cc_pass cvv, " +
                    "c.exp_month expmonth, " +
                    "curr.code, "+
                    "c.exp_year expyear, " +
                    "c.type_id type_id, " +
                    "c.holder_name, " +
                    "c.id ccId, " +
                    "t.amount amount, " +
                    "t.auth_number auth_number, " +
                    "w.user_name writer, " +
                    "u.user_name user_name, " +
                    "t.fee_cancel, " +
                    "s.default_xor_id, " +
                    "u.skin_id, " +
                    "u.utc_offset, " +
                    "u.currency_id, " +
                    
                    "u.first_name, " + 
                    "u.last_name, " +
                    "u.street, " +
                    "u.zip_code, " +
                    "u.city_name, " +
                    "u.email, " +
                    
                    "l.cc_fee, " +
                    "cntr.id AS country_id, " +
                    "cntr.a2 , " +
                    "t.ip, " +
                    "t.is_credit_withdrawal, " +
                    "t.deposit_reference_id, " +
                    "t.clearing_provider_id, " +
                    "t.type_id transaction_type_id, " +
                    "t.internals_amount, " +
                    "c.type_id_by_bin," +
                    "tu.time_created as transferred_user_time, " +
                    "tu.is_first_dep_made as transferred_user_depositor, " +
                    "l.amt_for_low_withdraw, " +
                    "t.time_created, " +
                    "t.utc_offset_created " +
            	"FROM " +
                    "transactions t, " +
                    "credit_cards c, " +
                    "writers w, " +
                    "users u left join transferred_users tu on u.id = tu.user_id, " +
                    "skins s, " +
                    "currencies curr, " +
                    "limits l, " +
                    "countries cntr, " +
                    "clearing_providers cp " +
            	"WHERE " +
            		"t.clearing_provider_id = cp.id AND " +
                    "t.user_id = u.id AND " +
                    "t.credit_card_id = c.id AND " +
                    "t.writer_id = w.id AND " +
                    "s.id = u.skin_id AND " +
                    "curr.id = u.currency_id AND " +
                    "u.limit_id = l.id AND " +
                    "u.country_id = cntr.id AND " +
                    
                    "t.status_id = ? AND " +
                    "t.is_accounting_approved = ? AND " +
					"cp.payment_group_id <> " + TransactionsManagerBase.PAYMENT_GROUP_EPG + " AND " ;


                	// Add an option to run job with specific transaction id
                    if (CommonUtil.isParameterEmptyOrNull(transactionId)) {
                       	sql +=
                            "t.type_id = ? AND " +
                       		"t.time_created < sysdate ";

                        if (log.isInfoEnabled()) {
                            log.info("No transaction.id specified");
                        }
                    } else {
                    		sql +=
                    			"t.id in ( " + transactionId + " ) AND " +
                    			"(t.type_id = ? OR  t.type_id = ? ) ";
                    }
                    
                    if(isTestRun) {
                    	sql += " AND u.class_id = 0  ";
                    	sql += " AND t.clearing_provider_id = " + ClearingManagerConstants.TESTING_PROVIDER_ID;
                    	sql += " AND rownum < 10 ";
                    } else {
                    	sql += " AND u.class_id <> 0 ";
                    }

                sql += "ORDER BY " +
                    	"curr.id, userid ,tid";

			pstmt = conn1.prepareStatement(sql);

			pstmt.setLong(1, TransactionsManagerBase.TRANS_STATUS_SECOND_APPROVAL);
			pstmt.setLong(2, ConstantsBase.ACCOUNTING_APPROVED_YES);
			pstmt.setLong(3, TransactionsManagerBase.TRANS_TYPE_CC_WITHDRAW);
			if (!CommonUtil.isParameterEmptyOrNull(transactionId)) {
				pstmt.setLong(4, TransactionsManagerBase.TRANS_TYPE_INTERNAL_CREDIT);
			}

			rs = pstmt.executeQuery();

			while (rs.next()) {
				newProviderId = 0;
                ClearingInfo info = new ClearingInfo();
                info.setTransactionId(rs.getInt("tid"));
                info.setCcn(AESUtil.decrypt(rs.getString("ccnumber")));
                info.setExpireMonth(rs.getString("expmonth"));
                info.setExpireYear(rs.getString("expyear"));
                info.setCvv(rs.getString("cvv"));
                info.setCcTypeId(rs.getInt("type_id"));
                info.setOwner(rs.getString("holder_name"));
                String currentCurrencyCode = rs.getString("code");
                info.setCurrencySymbol(currentCurrencyCode);
                info.setCurrencyId(rs.getInt("currency_id"));
                info.setAuthNumber(rs.getString("auth_number"));
                info.setAmount(rs.getLong("amount"));
                info.setUserId(rs.getLong("userid"));
                info.setUserName(rs.getString("user_name"));
                info.setSkinId(rs.getLong("skin_id"));
                
                info.setFirstName(rs.getString("first_name"));
                info.setLastName(rs.getString("last_name"));
                info.setAddress(rs.getString("street"));
                info.setZip(rs.getString("zip_code"));
                info.setCity(rs.getString("city_name"));
                info.setEmail(rs.getString("email"));

                if (CommonUtil.isHebrewSkin(info.getSkinId())) {
                	info.setCardUserId(rs.getString("idnum"));
                } else {  // Ao users(no id number)
                	info.setCardUserId(ConstantsBase.NO_ID_NUM);
                }
                info.setCountryId(rs.getLong("country_id"));
                info.setCountryA2(rs.getString("a2"));
                info.setIp(rs.getString("ip"));
                //info.setTransactionType(TransactionsManagerBase.TRANS_TYPE_CC_WITHDRAW);
                info.setTransactionType(rs.getLong("transaction_type_id"));
                utcOffset = rs.getString("utc_offset"); //save user utc offset
                info.setProviderId(rs.getLong("clearing_provider_id"));
                long internalsAmount = rs.getLong("internals_amount");  // saved for failed splitted transactions
                //set fee indicator and amount in case transaction has fee
                boolean hasFee = rs.getInt("fee_cancel") == 0;

                long amount = rs.getLong("amount");
                Date transactionTimeCreated = rs.getDate("time_created");
                String transactionUtcOffsetCreated = rs.getString("utc_offset_created");
                long feeAmount = 0l;

				if (hasFee) { 
					feeAmount = TransactionsManagerBase.getTransactionHomoFee(conn1, amount, info.getCurrencyId(), info.getTransactionType());
				}
				
                if (feeAmount > 0) {
                	info.setAmount(amount - feeAmount);
                } else {
                	hasFee = false;
                }

               
                // CFT / Credit logic
                ArrayList<Transaction> depositsList = new ArrayList<Transaction>();
                long creditAmountAllow = 0;
                boolean cftRequestNeeded = true;
                boolean creditProblem = false;
                boolean splitIsEnabled = false;
                long ccId = rs.getLong("ccId");
                long depositRefId = rs.getLong("deposit_reference_id");
                boolean cftEnabled = rs.getLong("is_credit_withdrawal") == 0 ? true : false;
                int transferredUserDepositorInt = rs.getInt("transferred_user_depositor");
                boolean transferredUserDepositor = false;
                if (transferredUserDepositorInt == 1) { // depositor
                	transferredUserDepositor = true;
                }

                // build deposits list
                if (info.getProviderId() != ClearingManagerConstants.INATEC_PROVIDER_ID_CFT && cftEnabled || info.getSkinId() == Skin.SKIN_ETRADER) {
                	depositsList = checkingFormerProviderToCredit(TransactionsDAOBase.getDepositTrxForCredit(conn1, info.getUserId(), ccId, String.valueOf(info.getProviderId()), true),
                			info.getAmount(), transferredUserDepositor, ccId, info.getUserId());
                	if(null == depositsList){
                		depositsList = TransactionsDAOBase.getDepositTrxForCredit(conn1, info.getUserId(), ccId, String.valueOf(info.getProviderId()), false);
                	}
                } else {  // marked manually to inatec CFT
                	if(!cftEnabled) { // request for Inatec credit via Backend
                    		ArrayList<Transaction> deposits = TransactionsDAOBase.getDepositTrxForCredit(conn1, info.getUserId(), ccId, null, false);
                    		for (Transaction t : deposits) { // take just inatec deposit providers
                    			if (t.getClearingProviderId() == ClearingManagerConstants.INATEC_PROVIDER_ID_USD ||
                    					t.getClearingProviderId() == ClearingManagerConstants.INATEC_PROVIDER_ID_EUR ||
                    					t.getClearingProviderId() == ClearingManagerConstants.INATEC_PROVIDER_ID_GBP ||
                    					t.getClearingProviderId() == ClearingManagerConstants.INATEC_PROVIDER_ID_TRY) {

                    				depositsList.add(t);
                    			}
                    		}
                    	} else { // CFT, need to check if we can credit via former Provider
                    		depositsList = checkingFormerProviderToCredit(TransactionsDAOBase.getDepositTrxForCredit(conn1, info.getUserId(), ccId, inatecFormer, true), 
                    				info.getAmount(), transferredUserDepositor, ccId, info.getUserId());
                    		if(null == depositsList){
                    			depositsList = new ArrayList<Transaction>();
                    		}
                    	}
                }

                // calculate creditAmountAllow
                for (Transaction t : depositsList) {
                	creditAmountAllow += t.getCreditAmount();
                }

                if (internalsAmount > 0) {  // then it's credit withdrawal + internals
                	cftEnabled = false;
                }

                if (cftEnabled) {
                	if (info.getAmount() <= creditAmountAllow) {
                		cftRequestNeeded = false;
                		if (info.getProviderId() == ClearingManagerConstants.INATEC_PROVIDER_ID_CFT) {
                			log.info("Inatec CFT request, going to run via Formere Provider");
                		} else {
                			splitIsEnabled = true;
                		}
                	} else {// cannot operate credit
                		if(creditAmountAllow > 0 ){
                			splitIsEnabled = true;
                		}
                		cftRequestNeeded = true;
                	}
                } else {  // credit
                	cftRequestNeeded = false;
                	//	check sum deposits in case it's cft disabled trx
                	long trxAmount = info.getAmount();
                	if (internalsAmount > 0) {
                		trxAmount = info.getAmount() - internalsAmount;
                		if (trxAmount <= 0) {
                            log.error("Failed to capture withdrawal transaction(Credit), internals amount >= transaction amount!, " +
                            		"amount: " + info.getAmount() + " ,internalsAmount: " + internalsAmount );
                            info.setSuccessful(false);
                            info.setResult("999");
                            info.setMessage("Problem with creditWithdrawal, internalsAmount(" + internalsAmount + ") >= transaction amount(" + info.getAmount() + ").");
                            creditProblem = true;
                		}
                	}
            		if (!creditProblem && trxAmount > creditAmountAllow) {
            			splitIsEnabled = true;            			
            		}
                }

                boolean res = true;

                if (cftRequestNeeded && !splitIsEnabled) {
					res = captureWithdrawalSingleTrx(	RUN_CFT, info, currentCurrencyCode, rs, utcOffset, hasFee, feeAmount, false,
														transactionTimeCreated, transactionUtcOffsetCreated);
                } else {  // credit (bookBack) needed
                	if (creditProblem) {  // cannot operate credit action
						captureWithdrawalSingleTrx(	DONT_RUN, info, currentCurrencyCode, rs, utcOffset, hasFee, feeAmount, false,
													transactionTimeCreated, transactionUtcOffsetCreated);
                	} else {
                		splitIsEnabled = false;
                		Transaction depTrx = null;
                		if (internalsAmount == 0) { // regular cases without failed internals
	                		if (depositRefId > 0) {  // credit action from backend to specific deposit
	                			depTrx = TransactionsDAOBase.getDepositTrxForCreditById(conn1, depositRefId);
	                		}
                		}
	        			if (null != depTrx) { // without split
	        				info.setOriginalDepositId(depTrx.getId());
	        				info.setProviderDepositId(depTrx.getXorIdCapture());
	        				info.setAuthNumber(depTrx.getAuthNumber());
	        				info.setCaptureNumber(depTrx.getCaptureNumber());
	        				info.setProviderId(depTrx.getClearingProviderId());
							res = captureWithdrawalSingleTrx(	RUN_CREDIT, info, currentCurrencyCode, rs, utcOffset, hasFee,
																feeAmount, false, transactionTimeCreated,
																transactionUtcOffsetCreated);
	        				if (res) {
	        					updateAfterCredit(info, depTrx.getId(), cftEnabled);
	        				}
	        			} else {  // split needed, cannot credit with 1 deposit trx(all trx with availableAmount<amount)  / recapture for failed internals
	        				ArrayList<Transaction> internalTrx = new ArrayList<Transaction>();
	        				long creditAmount = info.getAmount();

	        				ArrayList<Transaction> depositTrxBefore120Days = new ArrayList<Transaction>();
	        				ArrayList<Transaction> depositTrxAfter120Days = new ArrayList<Transaction>();
	        				Calendar cal = Calendar.getInstance();
	        				cal.add(Calendar.DAY_OF_MONTH, -120);
	        				log.debug("Time-120: " + cal.getTime().toString());

	        				for (Transaction t : depositsList) {
	        					Calendar time = Calendar.getInstance();
	        					time.setTime(t.getTimeSettled());
	        					log.debug("Time-trx: " + time.getTime().toString());
	        					if (time.after(cal)) {
	        						depositTrxBefore120Days.add(t);
	        					} else {
	        						depositTrxAfter120Days.add(t);
	        					}
	        				}

	        				if (internalsAmount > 0) {  // reduce internals amount
	        					creditAmount = info.getAmount() - internalsAmount;
	        				}

	        				for (Transaction t : depositTrxBefore120Days) {///need to split here
	        					long availableAmount = t.getCreditAmount();
	        					if (availableAmount > creditAmount) {   //  the amount that left for the withdrawal < availableAmount in trx
	        						availableAmount = creditAmount;
	        					}
	        					t.setCreditAmount(availableAmount);
	        					internalTrx.add(t);
	        					creditAmount -= availableAmount;
	        					if (creditAmount == 0) {
	        						break;
	        					}
	        				}

	        				if (creditAmount > 0) {  // perform split on the rest amount	        					
		        				for (Transaction t : depositTrxAfter120Days) {
		        					long availableAmount = t.getCreditAmount();
		        					if (availableAmount > creditAmount) {   //  the amount that left for the withdrawal < availableAmount in trx
		        						availableAmount = creditAmount;
		        					}
		        					t.setCreditAmount(availableAmount);
		        					internalTrx.add(t);
		        					creditAmount -= availableAmount;
		        					if (creditAmount == 0) {
		        						break;
		        					}
		        				}
	        				}
	        				if(creditAmount > 0){// not enough deposit to credit then split
	        					splitIsEnabled = true;
	        				}

	        				// create the internal credit transactions
	        				boolean resInternalsTrx = true;
	        				HashMap<Long,Transaction> updateTrxList = new HashMap<Long,Transaction>(); // <withdrawalTrxId, internalTrxKey>
	        				ClearingInfo splittedInfo = null;    // save info after each innerTrx
	        				long sumInternals = 0;

	        				for (Transaction t : internalTrx) {
	        					long availableAmount = t.getCreditAmount();
	        					Transaction trx = createInternalTrx(info, utcOffset, ccId, availableAmount);
	        					if (null == trx) {
	        						resInternalsTrx = false;
	        						break;
	        					} else {
		        					ClearingInfo innerInfo = createInternalInfo(info, trx, t);
									res = captureWithdrawalSingleTrx(	RUN_CREDIT, innerInfo, currentCurrencyCode, rs, utcOffset,
																		false, 0, false, transactionTimeCreated,
																		transactionUtcOffsetCreated);
		            				// save last run
		            				splittedInfo = new ClearingInfo();
		            				splittedInfo.setProviderId(innerInfo.getProviderId());
		            				splittedInfo.setResult(innerInfo.getResult());
		            				splittedInfo.setMessage(innerInfo.getMessage());
		            				splittedInfo.setSuccessful(innerInfo.isSuccessful());
		            				if (res) {
		            					updateAfterCredit(innerInfo, t.getId(), false);
		            					updateTrxList.put(trx.getId(), t);
		            					sumInternals += innerInfo.getAmount();
		            				} else {
		            					resInternalsTrx = false;
		            					break;
		            				}
	        					}
	        				}
	        				 if (splitIsEnabled && sumInternals <= info.getAmount()){	        					 
	        					 long amountToCFT = info.getAmount() - sumInternals;
	        					 Transaction trx = createInternalTrx(info, utcOffset, ccId, amountToCFT);
	        					 ClearingInfo innerInfo = createInternalInfo(info, trx, null);
	        					 innerInfo.setAmount(amountToCFT);
	        					 innerInfo.setProviderId(info.getProviderId());
	        					 res = captureWithdrawalSingleTrx(	RUN_CFT, innerInfo, currentCurrencyCode, rs, utcOffset, false,
																	feeAmount, true, transactionTimeCreated,
																	transactionUtcOffsetCreated);
	        					 if(res){
	        						 updateTrxList.put(trx.getId(), trx);
	        					 } else {
	        						 resInternalsTrx = false; 
	        					 }
	        				 }
	        				

	        				// set run info in the splitted trx
	        				if (null != splittedInfo) {
		        				info.setProviderId(splittedInfo.getProviderId());
		        				info.setResult(splittedInfo.getResult());
		        				info.setMessage(splittedInfo.getMessage());
		        				info.setSuccessful(splittedInfo.isSuccessful());
	        				}

	        				// update splitted transaction
   				        	addSplittedTrxToReport(info, currentCurrencyCode, rs.getString("writer"));  // add splittedTrx to report + summary
							updateTransaction(	info, hasFee, feeAmount, utcOffset, resInternalsTrx, true, cftEnabled,
												updateTrxList.keySet().toString(), sumInternals, splitIsEnabled, 
												transactionTimeCreated, transactionUtcOffsetCreated);
	        			}
                	}
                }
                // Send email to user
                if(info.isSuccessful() &&
                		(cftRequestNeeded ||
                		(!cftRequestNeeded && info.getTransactionType() == TransactionsManagerBase.TRANS_TYPE_CC_WITHDRAW))) {
                   	String ls = System.getProperty("line.separator");
    				log.info("Going to send withdrawal email: " + ls +
    								"userId: " + info.getUserId() + ls +
    								"trxId: " + info.getTransactionId() + ls);
    		        try {
	 				       UserBase uBase = new UserBase();
	 				       uBase.setUserName(info.getUserName());
	 				       UsersDAOBase.getByUserName(conn1,uBase.getUserName(),uBase, true);
	 				       
    				       long langId = SkinsDAOBase.getById(conn1, (int)info.getSkinId()).getDefaultLanguageId();
    				       String langCode = LanguagesDAOBase.getCodeById(conn1, langId);
    				       log.debug("skinId: " + info.getSkinId() + " , langId: " + langId + ", langCode: " + langCode);

							// email attributes
    						String body = composeMailBody(uBase, info.getCurrencyId(), info.getAmount(), info.getCc4Digits());
    						String emailSubject = CommonUtil.getMessage("withdrawal.user.email", null, new Locale(langCode));
							String emailFrom = CommonUtil.getProperty("email.from.anyoption");
    						if (uBase.getWriterId() == com.anyoption.common.beans.Writer.WRITER_ID_COPYOP_WEB
    								|| uBase.getWriterId() == com.anyoption.common.beans.Writer.WRITER_ID_COPYOP_MOBILE
    								|| uBase.getWriterId() == com.anyoption.common.beans.Writer.WRITER_ID_CO_MINISITE) {
    							emailFrom = emailFrom.replaceAll("anyoption", "copyop");
    						}
    						
    						// send email for tracking
    						sendEmail(body, emailSubject, emailFrom, jobProperties.get("succeed.email.to"));
							// send email to user							
							sendEmail(body, emailSubject, emailFrom, uBase.getEmail()); 

    					   // add to user MailBox
						   if (uBase.getSkinId() != Skin.SKIN_ETRADER) {
							   Template t = TemplatesDAO.get(conn1, Template.CC_WITHDRAWAL_SUCCESS_MAIL_ID);
							   UsersManagerBase.SendToMailBox(uBase, t, Writer.WRITER_ID_AUTO, langId, info.getTransactionId(), conn1, 0);
						   }

    		        } catch (Exception e) {
    		        	log.error("Error, Can't send withdrawal email! " + e);
    				}
                } else {
                	log.info("trx failed, withdrawal email not sent!");
                }
             }
        } catch (Exception e) {
        	String failedMail = jobProperties.get("failed.email.to");
        	if(failedMail != null) {
        		StringWriter sw = new StringWriter();
        		e.printStackTrace(new PrintWriter(sw));
        		try {
					sendEmail(sw.toString(), "Error in Capture Withdrawal", "support@anyoption.com", failedMail);
				} catch (Exception e1) {
					log.error("Error, Can't send exception trace", e);
				}
        	}
            log.error("Error while capturing pending transactions.", e);
        } finally {
            try {
                rs.close();
                pstmt.close();
                conn1.close();
            } catch (Exception e) {
                log.log(Level.ERROR, "Can't close", e);
            }
        }
        report.append("</table>");
    }


	private ArrayList<Transaction> checkingFormerProviderToCredit(
			ArrayList<Transaction> depositsList, long amount, boolean transferredUserDepositor, long cardId, long userId) {

		HashMap<Long, ArrayList<Transaction>> depositsHM = new HashMap<Long, ArrayList<Transaction>>();
		boolean canBeCredited = false;	
		long creditAmount = 0;
		
		for(Transaction trn : depositsList){
			ArrayList<Transaction> arr = null;
			if (trn.getClearingProviderId()!= ClearingManagerConstants.INATEC_PROVIDER_ID_EUR && trn.getClearingProviderId()!= ClearingManagerConstants.INATEC_PROVIDER_ID_USD &&
				trn.getClearingProviderId()!= ClearingManagerConstants.INATEC_PROVIDER_ID_TRY && trn.getClearingProviderId()!= ClearingManagerConstants.INATEC_PROVIDER_ID_GBP &&
						ClearingManager.getClearingProviders().get(trn.getClearingProviderId()).isActive()){
					
				Calendar cal = Calendar.getInstance();
				cal.add(Calendar.DAY_OF_MONTH, -120);
				Calendar time = Calendar.getInstance();
				time.setTime(trn.getTimeSettled());
				if(time.after(cal)){
					if(depositsHM.containsKey(trn.getClearingProviderId())){
						arr = depositsHM.get(trn.getClearingProviderId());
					} else {
						arr = new ArrayList<Transaction>();
					}
					arr.add(trn);			
					depositsHM.put(trn.getClearingProviderId(), arr);
				}
			}			
		}
		ArrayList<Transaction> tempList = null;
		
		while(!depositsHM.isEmpty() && !canBeCredited){
			tempList = checkByMoreTransaction(depositsHM);
			if(null == tempList){
				return null;
			}
			for (Transaction t : tempList) {
            	creditAmount += t.getCreditAmount();
            }
			if(amount <= creditAmount){
				canBeCredited = true;
			} else if (creditAmount > 0 && transferredUserDepositor && !isHaveSuccessDepositAfterTransferedUser(userId, cardId)) { //DEV-7201 case 9. check if we have success deposit after transferred user. If no we want to change CFT provider id to be former provider.
				long provider = tempList.get(0).getClearingProviderId();
				if (provider == ClearingManagerConstants.INATEC_PROVIDER_ID_USD || provider == ClearingManagerConstants.INATEC_PROVIDER_ID_EUR || provider == ClearingManagerConstants.INATEC_PROVIDER_ID_GBP) {
					provider = ClearingManagerConstants.INATEC_PROVIDER_ID_CFT;
				}
				newProviderId = provider;
				continue;
			} else {
				depositsHM.remove(tempList.get(0).getClearingProviderId());
				tempList = null;
				creditAmount = 0;
			}
		}
		return tempList;
	}


	private ArrayList<Transaction> checkByMoreTransaction(
			HashMap<Long, ArrayList<Transaction>> depositsHM) {
		Long maxDepositsProviderId = (long) 0;
		for(Iterator<Long> iter = depositsHM.keySet().iterator(); iter.hasNext();){
			Long providerId = iter.next();
			if(maxDepositsProviderId == 0 ){
				maxDepositsProviderId = providerId;
			} else if(depositsHM.get(providerId).size() > depositsHM.get(maxDepositsProviderId).size()){
				maxDepositsProviderId = providerId;
			}
		}		
		return depositsHM.get(maxDepositsProviderId);
	}

	private com.anyoption.common.beans.Transaction getFeeTransaction(ClearingInfo info) {
		com.anyoption.common.beans.Transaction t = new com.anyoption.common.beans.Transaction();
    	t.setUserId(info.getUserId());
    	t.setTypeId(TransactionsManagerBase.TRANS_TYPE_HOMO_FEE);
    	t.setTimeCreated(new Date());
    	t.setStatusId(TransactionsManagerBase.TRANS_STATUS_SUCCEED);
    	t.setWriterId(Writer.WRITER_ID_AUTO);
    	t.setIp("IP NOT FOUND!");
    	t.setTimeSettled(new Date());
    	t.setProcessedWriterId(Writer.WRITER_ID_AUTO);
    	t.setReferenceId(new BigDecimal(info.getTransactionId()));
    	t.setAuthNumber(info.getAuthNumber());
    	t.setXorIdCapture(info.getProviderTransactionId());
    	return t;
	}

	/**
	 * withdrawal action to 1 trx.
	 * @param type withdrawal type(cft/credit)
	 * @param info ClearingInfo instance of the trx
	 * @param currentCurrencyCode currency code of the trx
	 * @param utcOffset trx offset
	 * @param hasFee in case we need to take fee
	 * @param feeAmount fee amount
	 * @param conn1 db connection
	 * @param rs ResultSet instance
	 * @return true in case the process completed successfully
	 * @throws SQLException
	 */
	private boolean captureWithdrawalSingleTrx(int type, ClearingInfo info, String currentCurrencyCode,
														ResultSet rs, String utcOffset, boolean hasFee, long feeAmount,
														boolean splitEnabled, Date transactionTimeCreated,
														String transactionUtcOffsetCreated) throws SQLException {

		String providerName = "";
		String withdrawalType = "";
		boolean captureRes = true;

		if (type == RUN_CFT) {
			withdrawalType = "(CFT)";
		} else if (type == RUN_CREDIT) {
			withdrawalType = "(Credit)";
		}
		
    	if (newProviderId > 0 && type == RUN_CFT) {
    		log.debug("Transfer user case 9. DEV-7201. Change CFT provider id from " + info.getProviderId() + " to " + newProviderId);
    		info.setProviderId(newProviderId);
    	}

        if (log.isEnabledFor(Level.INFO)) {
        	if (type == RUN_CFT || type == RUN_CREDIT) {
        		log.log(Level.INFO, "Going to capture transaction" + withdrawalType + ": " + info.toString());
        	} else {
        		log.log(Level.INFO, "Don't going to capture transaction, capture cannot proceed!" + info.toString());
        	}
        }
        try {
        	
	        	if (type == RUN_CFT) {
	        		ClearingManager.withdraw(info);
	        	} else if (type == RUN_CREDIT) {
	        		ClearingManager.bookback(info);
	        	}
        } catch (ClearingException ce) {
            log.log(Level.ERROR, "Failed to capture withdrawal transaction" + withdrawalType + ": " + info.toString(), ce);
            info.setResult("999");
            info.setMessage(ce.getMessage());
            captureRes = false;
        }

        // currency code display
        if (null == lastCurrencyCode || !lastCurrencyCode.equals(currentCurrencyCode)) {
        	lastCurrencyCode = currentCurrencyCode;
            report.append("<tr><td colspan=\"7\" align=\"center\" valign=\"middle\"><b>").append(lastCurrencyCode).append("</b></td></tr>");
        }

        if (info.isSuccessful()) {
            report.append("<tr>");
        } else {
        	if (type == RUN_CREDIT && info.getTransactionType() == TransactionsManagerBase.TRANS_TYPE_INTERNAL_CREDIT) {
        		report.append("<tr style=\" color: #C24641; font-weight: bold;\">");
        	} else {
        		report.append("<tr style=\" color: #FF0000; font-weight: bold;\">");
        	}
        }

        providerName = ClearingManager.getProviderName(info.getProviderId());

        report.append("<td>").append(info.getUserId()).append("</td><td>");

        String actionType = "";
        String splittedId = "";  // display splittedId for internal credit trx
        if (type == RUN_CREDIT && info.getTransactionType() == TransactionsManagerBase.TRANS_TYPE_INTERNAL_CREDIT) {
        	splittedId = "(splittedTrx:" + String.valueOf(rs.getInt("tid")) + ")";
        }
        report.append(info.getTransactionId()).append(splittedId).append("</td><td>");
        report.append(providerName).append("</td><td>");
        report.append(info.getResult()).append("</td><td>");
        report.append(info.getMessage()).append("</td><td>");
        report.append(ClearingUtil.formatAmount(info.getAmount())).append("</td><td>");
        report.append(currentCurrencyCode).append("</td><td>");
        if(type == RUN_CFT && info.getTransactionType() == TransactionsManagerBase.TRANS_TYPE_INTERNAL_CREDIT) {
        	actionType = "CFT(internal)";
        } else if(type == RUN_CFT) {
        	actionType = "CFT";
        } else {
        	if (type == RUN_CREDIT && info.getTransactionType() == TransactionsManagerBase.TRANS_TYPE_INTERNAL_CREDIT) {
        		actionType = "Credit(internal)";
        	} else {
        		actionType = "Credit";
        	}
        }
        report.append(actionType).append("</td></tr>\n");
        if(!splitEnabled){
        	if ((!(type == RUN_CREDIT && info.getTransactionType() == TransactionsManagerBase.TRANS_TYPE_INTERNAL_CREDIT))
        		&& type != DONT_RUN) {
        	summary.addTransaction(rs.getString("writer"), providerName, info.getAmount(), currentCurrencyCode, info.isSuccessful());
        	}
        }

        if (info.isSuccessful()) {
        	captureRes = true;
        } else {
        	captureRes = false;
        }

		updateTransaction(	info, hasFee, feeAmount, utcOffset, info.isSuccessful(), false, false, null, 0, false, 
							transactionTimeCreated, transactionUtcOffsetCreated);

        return captureRes;

    }

	/**
	 * Update transaction after Credit action.
	 * Need to update deposit_reference_id on the withdrawal trx and to update
	 * credit_amount on the proper deposit trx.
	 * @param info	ClearingInfo instance
	 * @param depositTrxId the proper deposit transaction for the withdrawal
	 */
	private void updateAfterCredit(ClearingInfo info, long depositTrxId, boolean cftTrx) {

		Connection con = null;
		PreparedStatement ps = null;
		PreparedStatement ps2 = null;

		try {
			con = getConnection();
			con.setAutoCommit(false);

			String sql = "UPDATE " +
						 	"transactions " +
						 "SET " +
						 	"deposit_reference_id = ? ";

						 if (cftTrx) {
							 sql += ", is_credit_withdrawal = 1 ";
						 }

						 sql += "WHERE id = ? ";

			ps = con.prepareStatement(sql);
			ps.setLong(1, depositTrxId);
			ps.setLong(2, info.getTransactionId());
			ps.executeUpdate();

			String sql2 = "UPDATE " +
						  	 "transactions " +
						  "SET " +
						  	 "credit_amount = credit_amount + ? " +
						  "WHERE " +
						  	 "id = ? ";

			ps2 = con.prepareStatement(sql2);
			ps2.setLong(1, info.getAmount());
			ps2.setLong(2, depositTrxId);
			ps2.executeUpdate();
			con.commit();
    		ps.close();
    		ps2.close();
		} catch (Exception e) {
				rollback(con);
		} finally {
				setAutoCommitBack(con);
        		closeConnection(con);
		}
	}


	/**
	 * Create an internal withdrawal transaction
	 * @param info ClearingInfo instance of the original withdrawal trx
	 * @param utcOffset user offset
	 * @param ccId creditCard id of the original trx
	 * @param userId userId of the original trx
	 * @return
	 */
	private com.anyoption.common.beans.Transaction createInternalTrx(ClearingInfo info, String utcOffset, long ccId, long amount) {
		com.anyoption.common.beans.Transaction tran = new com.anyoption.common.beans.Transaction();
		tran.setAmount(amount);
		tran.setComments(null);
		tran.setCreditCardId(new BigDecimal(ccId));
		tran.setIp(info.getIp());
		tran.setProcessedWriterId(Writer.WRITER_ID_AUTO);
		tran.setTimeSettled(null);
		tran.setTimeCreated(new Date());
		tran.setUtcOffsetCreated(utcOffset);
		tran.setTypeId(TransactionsManagerBase.TRANS_TYPE_INTERNAL_CREDIT);
		tran.setUserId(info.getUserId());
		tran.setWriterId(Writer.WRITER_ID_AUTO);
		tran.setWireId(null);
		tran.setChargeBackId(null);
		tran.setReceiptNum(null);
		tran.setChequeId(null);
		tran.setFeeCancel(true);
		tran.setSplittedReferenceId(info.getTransactionId());
		tran.setDescription(null);
		tran.setStatusId(TransactionsManagerBase.TRANS_STATUS_SECOND_APPROVAL);
		tran.setAccountingApproved(true);
		tran.setCreditWithdrawal(true);

		Connection con = null;
		try {
			con = getConnection();
			TransactionsDAOBase.insert(con, tran);
		} catch (Exception e) {
			log.log(Level.ERROR, "ERROR! Can't create internalCreditTrx for trxId: " + tran.getSplittedReferenceId() + ", " +  e);
			tran = null;
		} finally {
       		closeConnection(con);
		}
		return tran;
	}

	/**
	 * Create and return internal clearingInfo instance
	 * @param info ClearingInfo instance of the original transaction
	 * @param trx  Internal withdrawal transaction
	 * @param depoTrx Deposit transaction instance
	 * @return
	 */
	private ClearingInfo createInternalInfo(ClearingInfo info, Transaction trx, Transaction depoTrx) {

		ClearingInfo innerInfo = new ClearingInfo();
		innerInfo.setTransactionId(trx.getId());
		innerInfo.setCcn(info.getCcn());
		innerInfo.setExpireMonth(info.getExpireMonth());
		innerInfo.setExpireYear(info.getExpireYear());
		innerInfo.setCvv(info.getCvv());
		innerInfo.setCcTypeId(info.getCcTypeId());
		innerInfo.setOwner(info.getOwner());
        innerInfo.setCurrencySymbol(info.getCurrencySymbol());
        innerInfo.setCurrencyId(info.getCurrencyId());
        innerInfo.setAmount(trx.getAmount());
        innerInfo.setUserId(trx.getUserId());
        innerInfo.setSkinId(info.getSkinId());
        innerInfo.setCountryId(info.getCountryId());
        innerInfo.setCountryA2(info.getCountryA2());
        innerInfo.setIp(trx.getIp());
        innerInfo.setTransactionType(trx.getTypeId());
        innerInfo.setCardUserId(info.getCardUserId());

        if(depoTrx != null){
        	innerInfo.setOriginalDepositId(depoTrx.getId());
        	innerInfo.setProviderDepositId(depoTrx.getXorIdCapture());
        	innerInfo.setAuthNumber(depoTrx.getAuthNumber());
        	innerInfo.setCaptureNumber(depoTrx.getCaptureNumber());
        	innerInfo.setProviderId(depoTrx.getClearingProviderId());
        }
        
        return innerInfo;
	}

	/**
	 * Add splitted trx to the report and to summary
	 * @param info ClearingInfo instance of the original trx
	 * @param currentCurrencyCode currency code
	 */
	private void addSplittedTrxToReport(ClearingInfo info, String currentCurrencyCode, String writer) {
        if (info.isSuccessful()) {
            report.append("<tr>");
        } else {
            report.append("<tr style=\" color: #FF0000; font-weight: bold;\">");
        }

        String providerName = ClearingManager.getProviderName(info.getProviderId());

        report.append("<td>").append(info.getUserId()).append("</td><td>");
        String splittedStr = "(splittedTrx)";
        report.append(info.getTransactionId()).append(splittedStr).append("</td><td>");
        report.append(providerName).append("</td><td>");
        report.append(info.getResult()).append("</td><td>");
        report.append(info.getMessage()).append("</td><td>");
        report.append(ClearingUtil.formatAmount(info.getAmount())).append("</td><td>");
        report.append(currentCurrencyCode).append("</td><td>");
        report.append("Credit").append("</td></tr>\n");

        summary.addTransaction(writer, providerName, info.getAmount(), currentCurrencyCode, info.isSuccessful());
	}


	/**
	 * Update transaction after captureWithdrawal
	 * @param info ClearingInfo instance of the current trx that captured
	 * @param hasFee true in case we need to create fee transaction
	 * @param feeAmount fee amount to take
	 * @param utcOffset user utcOffset
	 * @throws SQLException
	 */
	private void updateTransaction(ClearingInfo info, boolean hasFee, long feeAmount, String utcOffset, boolean isSuccessful,
											boolean splittedUpdate, boolean cftEnabled, String internalTransactions, long internalsAmount,
											boolean isSplittedCFT,  Date transactionTimeCreated,
											String transactionUtcOffsetCreated) throws SQLException {

		Connection conn = null;
		PreparedStatement ps = null;
		int index = 1;
        try {
        	conn = getConnection();
            String updateSql;
        	if (isSuccessful) { //handle success from provider
              	//update transaction status to success and amount to the amount after fee (in case there was fee)
        		updateSql =
                    "UPDATE " +
                        "transactions " +
                    "SET " +
                        "time_settled = sysdate, " +
                        "status_id =  ?, ";
        				if(!splittedUpdate) {
        					updateSql += "comments = ? || '|' || ? || '|' || ? || '|' || ? , " +
        								 "xor_id_capture = ? , ";
        				} else {
        					if(!isSplittedCFT){
        						updateSql += "comments =  ? || '|' || ? || '|' || ? || '| SplittedTrx to : ' || ? ,";
        					} else {
        						updateSql += "comments =  ? || '|' || ? || '|' || ? || '| SplittedTrx to CREDIT : ' || ? || '| SplittedTrx to CFT:' || ? ,";        					
        					}
        					
        				}

        				updateSql += "amount =  ?, " +
               						 "utc_offset_settled = ?, " +
               						 "clearing_provider_id = ? ";

						 if (splittedUpdate && cftEnabled) {  // in case we update spllited trx(not after capture) and the trx was not credit on creation.
							 updateSql += ",is_credit_withdrawal = 1 ";
						 }

						 updateSql += "WHERE " +
						 				"id = ? ";

						 ps = conn.prepareStatement(updateSql);
						 ps.setInt(index++, TransactionsManagerBase.TRANS_STATUS_SUCCEED);
						 ps.setString(index++, info.getResult());
						 ps.setString(index++, info.getMessage());
						 ps.setString(index++, info.getUserMessage());
						 if (!splittedUpdate) {
							 ps.setString(index++, info.getProviderTransactionId());
							 ps.setString(index++, info.getProviderTransactionId());
							 ps.setLong(index++, info.getAmount());
							 ps.setString(index++, utcOffset);
							 ps.setLong(index++, info.getProviderId());
							 ps.setLong(index++, info.getTransactionId());

						 } else {
							 if(!isSplittedCFT){
								 ps.setString(index++, internalTransactions);
							 }else {
								 String[] a = internalTransactions.split(",");								 
								 String credit ="";
								 String cft = "";
								 int size = a.length;
								 a[0] = a[0].substring(1);
								 a[size-1] = a[size -1].substring(0, a[size-1].length()-1);
								 
								 Arrays.sort(a, new Comparator<String>()
										    {
										      public int compare(String s1, String s2)
										      {
										        return Integer.valueOf(s1.trim()).compareTo(Integer.valueOf(s2.trim()));
										      }
										    });
								 for(String s: a){
									 if(!s.equalsIgnoreCase(a[size-1])){
										 credit += s + ",";
									 }else {
										 cft += s;
									 }
								 }
								 if (credit.length() > 0) {
									 credit = credit.substring(1, credit.length()-1);
								 }
								 cft = cft.substring(0, cft.length());
								 ps.setString(index++, credit);
								 ps.setString(index++, cft);
							 }
							 ps.setLong(index++, info.getAmount());
							 ps.setString(index++, utcOffset);
							 ps.setLong(index++, info.getProviderId());
							 ps.setLong(index++, info.getTransactionId());
						 }

				//in order to support roll back once and update or an insert was not successful
        		conn.setAutoCommit(false); 

               	//insert new fee transaction
               	if (hasFee) {
               		com.anyoption.common.beans.Transaction feeTransaction;
               		feeTransaction = getFeeTransaction(info);
					feeTransaction.setAmount(feeAmount);
                	feeTransaction.setUtcOffsetCreated(utcOffset);
                	feeTransaction.setUtcOffsetSettled(utcOffset);
    				TransactionsDAOBase.insert(conn, feeTransaction);
    				if (log.isInfoEnabled()) {
    					String ls = System.getProperty("line.separator");
    					log.info("fee transaction inserted: " + ls + feeTransaction.toString());
    				}

    				//update Transaction reference
    				TransactionsDAOBase.updateRefId(conn, info.getTransactionId(), feeTransaction.getId());
               	}
        	} else { //handle failed from provider
        		//update transaction status to failed
        		updateSql =
                    "UPDATE " +
                        "transactions " +
                    "SET " +
                        "time_settled = sysdate, " +
                        "status_id = ? , " +
      	 				"comments = ? || '|' || ? || '|' || ? || '|' || ? , " +
      	 				"description = ? , ";
        				if (!splittedUpdate) {
        					updateSql += "xor_id_capture = ?, ";
        				}
        				if (splittedUpdate) {
        					updateSql += "internals_amount = internals_amount + " + internalsAmount + ", ";
        				}
        				updateSql +=  "utc_offset_settled = ?, " +
                        			  "clearing_provider_id = ? " +
                        			  "WHERE " +
                        			  		"id = ? ";

       				 ps = conn.prepareStatement(updateSql);
					 ps.setInt(1, TransactionsManagerBase.TRANS_STATUS_FAILED);
					 ps.setString(2, info.getResult());
					 ps.setString(3, info.getMessage());
					 ps.setString(4, info.getUserMessage());
					 ps.setString(5, info.getProviderTransactionId());
					 ps.setString(6, ConstantsBase.ERROR_FAILED_CAPTURE);
					 if (!splittedUpdate) {
						 ps.setString(7, info.getProviderTransactionId());
						 ps.setString(8, utcOffset);
						 ps.setLong(9, info.getProviderId());
						 ps.setLong(10, info.getTransactionId());
					 } else {
						 ps.setString(7, utcOffset);
						 ps.setLong(8, info.getProviderId());
						 ps.setLong(9, info.getTransactionId());
					 }

        	}
        	ps.executeUpdate();
        	conn.commit();
        } catch (Exception e) {
			log.error("ERROR! Can't update transaction!. ", e);
			rollback(conn);
		} finally {
			setAutoCommitBack(conn);
			closeConnection(conn);
		}
	}

	private void sendEmail(String body, String subject, String from, String to) throws Exception {
    	// server configuration
	    Hashtable<String,String> server = new Hashtable<String,String>();
	    server.put("url", CommonUtil.getProperty("email.server"));
	    server.put("auth", CommonUtil.getProperty("email.auth"));
	    server.put("user", CommonUtil.getProperty("email.uname"));
	    server.put("pass", CommonUtil.getProperty("email.pass"));
	    server.put("contenttype", "text/html; charset=UTF-8");

	    // email configuration
	    Hashtable<String, String> email = new Hashtable<String, String>();
	    email.put("subject", subject);
	    email.put("to", to);
	    email.put("from", from);
	    email.put("body", body.toString());
	    // send email
	    CommonUtil.sendEmail(server, email, null);
	}
	
	/* 
	 * composes the body for the success withdraw email
	 * load templates from file system and replace parameters
	 * with values from user and transaction
	 * returns the actual String to be send as mail body to client
	 */
	private String composeMailBody(UserBase user, long currencyId, long amount, String cc4Digits) throws Exception {
		// prepare template
		String lngCode = LanguagesManagerBase.getLanguage(user.getLanguageId()).getCode();
		String vmTemplatesKey = lngCode + "_" + user.getSkinId();
		
		org.apache.velocity.Template velocityMailTemplate = velocityMailTemplates.get(vmTemplatesKey);
		if (velocityMailTemplate == null) {
			velocityMailTemplate = SendTemplateEmail.getEmailTemplate(ConstantsBase.TEMPLATE_WITHDRAW_CC_SUCCEED, lngCode, user.getSkinId(), CommonUtil.getProperty("templates.path"), user.getWriterId(), 0);
			velocityMailTemplates.put(vmTemplatesKey, velocityMailTemplate);
		} 
		
		// prepare body
		String currencySymbol = CurrenciesManagerBase.getCurrency(currencyId).getSymbol();
		String amountTxt = currencySymbol + ClearingUtil.formatAmount(amount);
		HashMap<String,String> params = new HashMap<String, String>();
		params.put(SendTemplateEmail.PARAM_USER_ID,String.valueOf(user.getId()));
		params.put(SendTemplateEmail.PARAM_USER_FIRST_NAME,user.getFirstName());
		params.put(SendTemplateEmail.PARAM_USER_LAST_NAME, user.getLastName());
		params.put(SendTemplateEmail.PARAM_USER_ADDRESS_NUMBER, user.getStreetNo());
		params.put(SendTemplateEmail.PARAM_USER_ADDRESS_STREET, user.getStreet());
		params.put(SendTemplateEmail.PARAM_USER_ADDRESS_CITY, user.getCityName());
		params.put(SendTemplateEmail.PARAM_USER_ADDRESS_ZIP_CODE, user.getZipCode());
		params.put(SendTemplateEmail.PARAM_TRANSACTION_AMOUNT, amountTxt);
		params.put(SendTemplateEmail.PARAM_TRANSACTION_PAN, cc4Digits);
		Date date = new Date();
		params.put(SendTemplateEmail.PARAM_DATE_DMMYYYY, new SimpleDateFormat("dd.MM.yy").format(date));
		params.put(SendTemplateEmail.PARAM_DATE_MMMMMDYYYY, new SimpleDateFormat("dd/MM/yyyy").format(date));
		
		if (null != user.getGender()) {
			String genderTxt = user.getGenderForTemplateEnHeUse();
			params.put(SendTemplateEmail.PARAM_GENDER_DESC_EN_OR_HE, CommonUtil.getMessage(genderTxt, null, new Locale(lngCode)));
		}
		VelocityContext context = new VelocityContext(params);
		
		StringWriter stringWriter = new StringWriter();
		velocityMailTemplate.merge(context, stringWriter);
		String body = stringWriter.toString();
		return body;
	}
	
	private void sendReport() {
        String reportBody = "<html><body>" +
                summary.getSummery() +
                report.toString() +
                "</body></html>";
       
        log.info("Sending report email: " + reportBody);

        String subject	=	jobProperties.get("report.email.subject");
        String to		=	jobProperties.get("report.email.to");
        String from		=	jobProperties.get("report.email.from");
        
        try {
			sendEmail(reportBody, subject, from, to);
		} catch (Exception e) {
			log.error("Can't send capture withdraw report", e);
		}
        	
	}
    
    /**
     * DEV-7201 case 9. check if we have success deposit from current provider. If no we want to change CFT provider id to be former provider.
     * @param userId
     * @param cardId
     * @return
     */
    private boolean isHaveSuccessDepositAfterTransferedUser(long userId, long cardId) { 
        Connection conn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;    
        boolean res = false;
        try {
            conn = getConnection();
            String sql =" SELECT " +
            			"	t.* " +
            			" FROM " +
            			"	users u, " +
            			"   transferred_users tu, " +
            			"   transactions t, " +
            			"   transaction_types tt " +
            			" WHERE " +
            			"   u.id = tu.user_id " +
            			"   AND t.user_id = u.id " +
            			"   AND t.type_id = tt.id " +
            			"   AND t.type_id = ? " + // cc
            			"   AND tt.class_type = ? " + // real
            			"   AND t.time_settled > tu.time_created " +
            			"   AND t.status_id = ? " +
            			"   AND t.credit_card_id = ? " +
            			"   AND u.id = ? " +
						"   AND t.time_settled > sysdate - 120 ";
            ps = conn.prepareStatement(sql);
            ps.setLong(1, TransactionsManagerBase.TRANS_TYPE_CC_DEPOSIT);
            ps.setLong(2, TransactionsManagerBase.TRANSACTION_CLASS_TYPE_REAL_DEPOSIT);
            ps.setLong(3, TransactionsManagerBase.TRANS_STATUS_SUCCEED);
            ps.setLong(4, cardId);
            ps.setLong(5, userId);
            
            rs = ps.executeQuery();
            if (rs.next()) {          
                res = true;
            }
        } catch (Exception e) {
            log.error("isHaveSuccessDepositAfterTransferedUser exception, ", e);
        } finally {
        	try{
	        	ps.close();
	        	rs.close();
        	} catch( Exception e) {/* ignore */}
        	closeConnection(conn);
        }	
        return res;
    }

    /*
     * IF the last execution was stopped before sending report
     * THEN gather all success transaction since the last success run
     * AND append to the current report
     */
    private boolean gatherMissedTransacations(Date date) {
    	 Connection conn = null;
         PreparedStatement ps = null;
         ResultSet rs = null;    
         boolean res = false;
         missedTransactions = new StringBuilder("<br><b>Transactions from failed run</br></b><table border=\"1\">");
         missedTransactions.append("<tr><td>User ID</td><td>Transaction ID</td><td>Clearing provider<td>Time settled</td><td>Description</td><td>Amount</td></tr>");
         try {
             conn = getConnection();
             String sql =" SELECT user_id, id, clearing_provider_id, time_settled, description, amount FROM transactions WHERE time_settled >  ? ";
             ps = conn.prepareStatement(sql);
             ps.setDate(1, new java.sql.Date(date.getTime()));
             
             rs = ps.executeQuery();
             
             while (rs.next()) {
            	 res = true;
            	 missedTransactions.append("<tr>");
            	 missedTransactions.append("<td>").append(rs.getLong("user_id")).append("</td>");
            	 missedTransactions.append("<td>").append(rs.getLong("id")).append("</td>");
            	 missedTransactions.append("<td>").append(rs.getLong("clearing_provider_id")).append("</td>");
            	 missedTransactions.append("<td>").append(rs.getDate("time_settled")).append("</td>");
            	 missedTransactions.append("<td>").append(rs.getString("description")).append("</td>");
            	 missedTransactions.append("<td>").append(rs.getLong("amount")).append("</td>");
            	 missedTransactions.append("</tr>");            	 
             }
             missedTransactions.append("</table><br/>");
             return res;
         } catch (SQLException e) {
        	 log.error("Error gathering missed transactions", e);
        	 return false;
         } finally {
         	try{
 	        	ps.close();
 	        	rs.close();
         	} catch( Exception e) {/* ignore */}
         	closeConnection(conn);
         }
    }
}