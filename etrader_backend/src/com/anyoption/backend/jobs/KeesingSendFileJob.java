package com.anyoption.backend.jobs;


import org.apache.log4j.Logger;

import com.anyoption.backend.managers.KeesingManager;
import com.anyoption.backend.service.keesing.KeesingRequest;
import com.anyoption.backend.service.keesing.KeesingResponse;
import com.anyoption.backend.service.keesing.KeesingServices;
import com.anyoption.common.beans.File;
import com.anyoption.common.beans.Job;
import com.anyoption.common.jobs.ScheduledJob;
import com.anyoption.common.service.CommonJSONService;
import com.anyoption.common.util.CommonUtil;
import com.anyoption.common.util.ConstantsBase;

public class KeesingSendFileJob implements ScheduledJob {

   private static final Logger log = Logger.getLogger(KeesingSendFileJob.class); 
   private static final int RECORD_COUNT = 5;

    public void setJobInfo(Job job) {
        // do nothing
    }

    public boolean run() {
    	try {    	
    		long beginTime = System.currentTimeMillis();    		
        	log.debug("============ BEGIN KEESING Send File Job ====================="); 
        	int i = 1;
        	boolean notFoundRec = false;
        	while (i <= RECORD_COUNT && !notFoundRec) {							
	        	File f = KeesingManager.loadKeesingFileForSend(0l);
	        	i++;
	        	if(f != null){
	        		long fileId = f.getId();
	        		String filePath = CommonUtil.getProperty(ConstantsBase.FILES_PATH);
	        		String fileName = f.getFileName();
	        		log.debug(i + ". Try to Send KEESING FileID|:" + fileId + " and file:" + filePath + fileName);
	        		KeesingRequest request = new KeesingRequest(fileId, filePath, fileName);
	        		KeesingResponse response =  KeesingServices.checkIDDocument(request);
	        		if(response.getErrorCode() != CommonJSONService.ERROR_CODE_SUCCESS){
	        			log.error("Unsuccessful sent file to Keesing, mark file ks failed");
	        			KeesingManager.markKeesingFileFailed(fileId);
	        		}
	        		log.debug("After Sent KEESING Response: " + response);
	        	} else {
	        		log.debug("Not FOUND file for send KEESING");
	        		notFoundRec = true;
	        	}        	
        	}
        	
        	long endTime = System.currentTimeMillis();   	
        	log.debug("============ FINISHED KEESING Job Send Files:" + i + " in:" + (endTime - beginTime) + "ms =====================");
            return true;
			
		} catch (Exception e) {
			log.error("When run Keesing Send FileJob Job",e);
			return false;
		}
    }
    public void stop() {
        log.info("KeesongSendFileJob...");
    }
}