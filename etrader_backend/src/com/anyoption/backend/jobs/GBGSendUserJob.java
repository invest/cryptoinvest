package com.anyoption.backend.jobs;


import java.util.ArrayList;

import org.apache.log4j.Logger;

import com.anyoption.backend.service.gbg.GBGManager;
import com.anyoption.backend.service.gbg.GBGRequest;
import com.anyoption.backend.service.gbg.GBGResponse;
import com.anyoption.backend.service.gbg.GBGServices;
import com.anyoption.common.beans.Job;
import com.anyoption.common.jobs.ScheduledJob;

public class GBGSendUserJob implements ScheduledJob {

   private static final Logger log = Logger.getLogger(GBGSendUserJob.class); 
    public void setJobInfo(Job job) {
        // do nothing
    }

    public boolean run() {
    	try {    	
    		long beginTime = System.currentTimeMillis();    		
        	log.debug("============ BEGIN GBG Send Users Job ====================="); 
        	int i = 1;
        	ArrayList<GBGRequest> gbgList = GBGManager.loadGBGUsersForSend();
        	if(gbgList.size() > 0){
        		log.debug("Found[" + gbgList.size() + "] users for GBG send");
        		for (GBGRequest gbgRequest : gbgList) {
        			sendGBG(gbgRequest);
					i++;
				}
        	} else {
        		log.debug("Not FOUND User for send GBG");
        	}
        	
        	long endTime = System.currentTimeMillis();   	
        	log.debug("============ FINISHED KGBG Send Users:" + i + " in:" + (endTime - beginTime) + "ms =====================");
            return true;
			
		} catch (Exception e) {
			log.error("When run GBG Send Users Job",e);
			return false;
		}
    }
    
    private void sendGBG(GBGRequest gbg){
      	try {
	    	GBGManager.updateGBGStatus(gbg.getId(), GBGManager.GBG_INSERTED_INPROCESS_ID);
	    	log.debug("GBG Response in Procces and try to sent GBGServices.authenticateSP[" + gbg + "]");
	    	GBGResponse response = GBGServices.authenticateSP(gbg);
	    	response.setId(gbg.getId());
	    	long stateAfterGBG = GBGManager.updateGBGResponse(response);
	    	log.debug("GBG State after Response is[" + stateAfterGBG + "]");
	    	//Save FILE
	    	if(stateAfterGBG == GBGManager.GBG_PEP_SUSPEND || 
	    			stateAfterGBG == GBGManager.GBG_LOW_SCORE || 
	    				stateAfterGBG == GBGManager.GBG_APPROVE_FILE){
		    	GBGManager.uploadGBGFileId(response, gbg.getUserId(), stateAfterGBG);
	    	}
		} catch (Exception e) {
			log.error("Can't send and save GBG File :" + gbg + " with error", e);
		}
    }
    public void stop() {
        log.info("Stop GBG Job...");
    }
}