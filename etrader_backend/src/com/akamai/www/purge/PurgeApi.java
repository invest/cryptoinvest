/**
 * PurgeApi.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.akamai.www.purge;

public interface PurgeApi extends java.rmi.Remote {
    public com.akamai.www.purge.PurgeResult purgeRequest(java.lang.String name, java.lang.String pwd, java.lang.String network, java.lang.String[] opt, java.lang.String[] uri) throws java.rmi.RemoteException;
}
