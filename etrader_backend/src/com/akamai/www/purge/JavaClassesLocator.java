/**
 * JavaClassesLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.akamai.www.purge;

public class JavaClassesLocator extends org.apache.axis.client.Service implements com.akamai.www.purge.JavaClasses {

/**
 * Provides programmatic purge access
 */

    public JavaClassesLocator() {
    }


    public JavaClassesLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public JavaClassesLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for PurgeApi
    private java.lang.String PurgeApi_address = "https://ccuapi.akamai.com:443/soap/servlet/soap/purge";

    public java.lang.String getPurgeApiAddress() {
        return PurgeApi_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String PurgeApiWSDDServiceName = "PurgeApi";

    public java.lang.String getPurgeApiWSDDServiceName() {
        return PurgeApiWSDDServiceName;
    }

    public void setPurgeApiWSDDServiceName(java.lang.String name) {
        PurgeApiWSDDServiceName = name;
    }

    public com.akamai.www.purge.PurgeApi getPurgeApi() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(PurgeApi_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getPurgeApi(endpoint);
    }

    public com.akamai.www.purge.PurgeApi getPurgeApi(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            com.akamai.www.purge.PurgeApiSOAPBindingStub _stub = new com.akamai.www.purge.PurgeApiSOAPBindingStub(portAddress, this);
            _stub.setPortName(getPurgeApiWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setPurgeApiEndpointAddress(java.lang.String address) {
        PurgeApi_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (com.akamai.www.purge.PurgeApi.class.isAssignableFrom(serviceEndpointInterface)) {
                com.akamai.www.purge.PurgeApiSOAPBindingStub _stub = new com.akamai.www.purge.PurgeApiSOAPBindingStub(new java.net.URL(PurgeApi_address), this);
                _stub.setPortName(getPurgeApiWSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("PurgeApi".equals(inputPortName)) {
            return getPurgeApi();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://www.akamai.com/purge", "JavaClasses");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://www.akamai.com/purge", "PurgeApi"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("PurgeApi".equals(portName)) {
            setPurgeApiEndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
