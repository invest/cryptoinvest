package il.co.etrader.backend.cms;

import il.co.etrader.backend.cms.CMSConstants.fileType;

public class FilePathConvertor {

	private final static String pathForImage = "webapps/webapp_anyoption/ROOT";
	private final static String pathForProperty = "webapps/webapp_anyoption/ROOT/WEB-INF/classes";
	
	public static String convertCvsToFtpPath(String cvsPath, fileType type) {
		if ( type.equals(fileType.property) ) {
			return pathForProperty;
		} else if (type.equals(fileType.image)) {
			String[] path = cvsPath.split("/");
			String toReturn = pathForImage;
			for (int i = 2; i < path.length; i++) {
				toReturn += "/" + path[i];
			}
			return toReturn;
		} else {
			return null;
		}
	}
}
