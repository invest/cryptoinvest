package il.co.etrader.backend.cms;

import il.co.etrader.backend.cms.common.CommonCMSBean;
import il.co.etrader.backend.cms.common.PropertyTypeEnum;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.faces.model.SelectItem;
import javax.servlet.http.HttpSession;

import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;

/**
 * Interface for accessing all message bundle CMS functionalities. The implementing instance should be registered as
 * managed session bean.
 *
 */
public interface BundleSelection {
    
	/**
	 * Searches for all properties that comply with the given criteria.
	 * 
	 * @param skinId
	 * @param page
	 * @param type
	 * @param key
	 * @return list of {@link CmsDataObjects}. Each member represents a single property of a skin.
	 */
	public ArrayList<CmsDataObjects> getList(int skinId, String page, PropertyTypeEnum type, String key);
	
	/**
	 * @return list of all active skins
	 */
	public ArrayList<SelectItem> getSkinList();
	
	/**
	 * @return list of all pages that have been adapted for CMS.
	 */
	public ArrayList<SelectItem> getPageList();
	
	/**
	 * @param onlyAltKeys if <code>true</code> then it will return only alt text keys. If <code>false</code>, it will
	 * return all keys except the alt text keys.
	 * @return list of either all alt text keys or all other keys.
	 */
	public ArrayList<SelectItem> getKeyList(boolean onlyAltKeys);
	
	/**
	 * @param skinId
	 * @return the path to the given skin properties file
	 */
	public String getSkinFilePath(int skinId);
	
	/**
	 * @param skinId
	 * @return the name of the images folder for the given skin
	 */
	public String getSkinImagesFolder(int skinId);
		
	/**
	 * Sets a new value to the property defined with the given parameters. All input parameters shall not be <code>null</code>.
	 * 
	 * @param skinId the ID of the skin which property is about to be set
	 * @param page the name of the page to which the property belongs
	 * @param type the type of the property, see {@link PropertyTypeEnum}
	 * @param key the actual key of the property
	 * @param value the value to set
	 * @throws ConfigurationException - in case something is wrong with the {@link PropertiesConfiguration}
	 * @throws IOException - in case something is wrong with the properties file.
	 */
	public void setValue(int skinId,
	                        String page,
	                        PropertyTypeEnum type,
	                        String key,
	                        String value) throws ConfigurationException, IOException;
	
	/**
	 * This method should be called only once per session. It checks out all properties files from CVS and loads them
	 * in the managed bean. It, also, initializes the {@link CommonCMSBean} and puts it in the {@link HttpSession} map.
	 * @throws SQLException 
	 * @throws IOException 
	 * @throws ConfigurationException 
	 */
	public void load() throws ConfigurationException, IOException, SQLException;
	
	/**
	 * This method loads the properties of the given skin. It should be called right before trying to edit a property
	 * in order to have an up-to-date property value.
	 * 
	 * @param skinId the skin ID
	 * @throws IOException - in case something is wrong with the {@link PropertiesConfiguration}  
	 * @throws ConfigurationException - in case something is wrong with the properties file. 
	 */
	public void load(int skinId) throws ConfigurationException, IOException;
	
	/**
	 * This method loads the properties of a skin with the given properties file path. It should be called right before
	 * trying to edit a property in order to have an up-to-date property value.
	 * 
	 * @param filePath the file path to the properties file for the corresponding skin
	 * @throws IOException - in case something is wrong with the {@link PropertiesConfiguration} 
	 * @throws ConfigurationException - in case something is wrong with the properties file. 
	 */
	public void load(String filePath) throws ConfigurationException, IOException;
	
}
