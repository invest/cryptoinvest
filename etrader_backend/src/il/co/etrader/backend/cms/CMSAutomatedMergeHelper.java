/**
 * 
 */
package il.co.etrader.backend.cms;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;


/**
 * This class is executed from CMS_automated_merge script and it's purpose is to checkout from cvs and commit files back to cvs after the merge
 * 
 * @author kirilim
 *
 */
public class CMSAutomatedMergeHelper {
	
	private static String cvs_root_dev;
	private static String cvs_root_cms;
	private static String server_host_dev;
	private static String server_host_cms;
	private static int server_port_dev;
	private static int server_port_cms;
	private static String repo_root;
	private static String commit_message;
	private static String cvsDevUser;
	private static String cvsDevPassword;
	private static String cvsCmsUser;
	private static String cvsCmsPassword;
	private static String dir1;
	private static String dir2;
	private static String dirCheckoutCommit;
	private static String roboLog1;
	private static String roboLog2;
	private static final String NEW_FILE_LINE = "New File";
	private static final String NEW_DIR_LINE = "New Dir";
	
	private enum cvsCommand {
		checkout, commit;
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		System.out.println("Start of java code");
		try {
			Cvs cvs = null;			
            switch (cvsCommand.valueOf(args[0])) {
			case checkout:
				System.out.println("Executing checkout command");
				parseArguments(args);
				cvs = new CVSImpl(cvs_root_dev, server_host_dev, server_port_dev, repo_root, commit_message);
				System.out.println("Checkout from cvs to " + dir1);
				cvs.checkoutDir(dir1, dirCheckoutCommit, cvsDevUser, cvsDevPassword);
				cvs = new CVSImpl(cvs_root_cms, server_host_cms, server_port_cms, repo_root, commit_message);
				System.out.println("Checkout from cvs to " + dir2);
				cvs.checkoutDir(dir2, dirCheckoutCommit, cvsCmsUser, cvsCmsPassword);
				break;
			case commit:
				System.out.println("Executing add command and commit command.");
				parseArguments(args);
				
				ArrayList<String> fileNames1 = parseRoboLog(roboLog1, NEW_FILE_LINE, dir1 + "/" + dirCheckoutCommit + "/", dir2 + "/" + dirCheckoutCommit + "/");
				ArrayList<String> dirNames1 = parseRoboLog(roboLog1, NEW_DIR_LINE, dir1 + "/" + dirCheckoutCommit + "/", dir2 + "/" + dirCheckoutCommit + "/");
				cvs = new CVSImpl(cvs_root_dev, server_host_dev, server_port_dev, repo_root, commit_message);
				System.out.println("Adding directories from " + dir1 + "/" + dirCheckoutCommit + "/" + " to cvs");
				cvs.add(dir1 + "/" + dirCheckoutCommit + "/", dirNames1, cvsDevUser, cvsDevPassword, false);
				System.out.println("cvs message after add dirs command -> " + cvs.getGet_message());
				System.out.println("Adding files from " + dir1 + "/" + dirCheckoutCommit + "/" + " to cvs");
				cvs.add(dir1 + "/" + dirCheckoutCommit + "/", fileNames1, cvsDevUser, cvsDevPassword, true);
				System.out.println("cvs message after add files command -> " + cvs.getGet_message());
				cvs.commit(dir1, dir1 + "/" + dirCheckoutCommit, cvsDevUser, cvsDevPassword);
				System.out.println("cvs message after commit command -> " + cvs.getGet_message());

				ArrayList<String> fileNames2 = parseRoboLog(roboLog2, NEW_FILE_LINE, dir2 + "/" + dirCheckoutCommit + "/", dir1 + "/" + dirCheckoutCommit + "/");
				ArrayList<String> dirNames2 = parseRoboLog(roboLog2, NEW_DIR_LINE, dir2 + "/" + dirCheckoutCommit + "/", dir1 + "/" + dirCheckoutCommit + "/");
				cvs = new CVSImpl(cvs_root_cms, server_host_cms, server_port_cms, repo_root, commit_message);
				System.out.println("Adding directories from " + dir2 + "/" + dirCheckoutCommit + "/" + " to cvs");
				cvs.add(dir2 + "/" + dirCheckoutCommit + "/", dirNames2, cvsCmsUser, cvsCmsPassword, false);
				System.out.println("cvs message after add dirs command -> " + cvs.getGet_message());
                System.out.println("Adding files from " + dir2 + "/" + dirCheckoutCommit + "/" + " to cvs");
                cvs.add(dir2 + "/" + dirCheckoutCommit + "/", fileNames2, cvsCmsUser, cvsCmsPassword, true);
                System.out.println("cvs message after add files command -> " + cvs.getGet_message());
                cvs.commit(dir2, dir2 + "/" + dirCheckoutCommit, cvsCmsUser, cvsCmsPassword);
                System.out.println("cvs message after commit command -> " + cvs.getGet_message());
				break;
			}
		} catch (IllegalArgumentException e) {
			System.out.println("Wrong command parameter. The first argument should be either chekout or commit");
			return;
		}
		
		System.out.println("End of java code");
	}

	private static ArrayList<String> parseRoboLog(String roboLog, String regExLine, String toReplaceWith, String toBeReplaced) {
	    ArrayList<String> fileNames = new ArrayList<String>();
	    File log = new File(roboLog);
        Scanner scan = null;
        try {
            scan = new Scanner(log);
            while (scan.hasNextLine()) {
                String line = scan.nextLine();
                if (line.contains(regExLine)) {
                    String fileName = line.substring(line.lastIndexOf('\t'), line.length()).trim();
                    fileName = fileName.replace("\\", "/");
                    toBeReplaced = toBeReplaced.replace("\\", "/");
                    toReplaceWith = toReplaceWith.replace("\\", "/");
                    fileName = fileName.replace(toBeReplaced, toReplaceWith);
                    fileNames.add(fileName);
                }
            }
        } catch (FileNotFoundException e) {
            System.out.println(e.toString());
        } finally {
            scan.close();
        }
        
        return fileNames;
    }

    private static void parseArguments(String[] args) {
		cvs_root_dev = args[1];
		cvs_root_cms = args[2];
		server_host_dev = args[3];
		server_host_cms =args[4];
		server_port_dev = Integer.parseInt(args[5]);
		server_port_cms = Integer.parseInt(args[6]);
		repo_root = args[7];
		commit_message = args[8];
		cvsDevUser = args[9];
		cvsDevPassword = args[10];
		cvsCmsUser = args[11];
        cvsCmsPassword = args[12];
		dir1 = args[13];
		dir2 = args[14];
		dirCheckoutCommit = args[15];
		roboLog1 = args[16];
		roboLog2 = args[17];
	}
}
