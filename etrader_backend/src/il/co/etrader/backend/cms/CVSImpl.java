package il.co.etrader.backend.cms;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;

import javax.faces.context.FacesContext;
import javax.net.SocketFactory;

import org.apache.log4j.Logger;
import org.netbeans.lib.cvsclient.CVSRoot;
import org.netbeans.lib.cvsclient.Client;
import org.netbeans.lib.cvsclient.admin.StandardAdminHandler;
import org.netbeans.lib.cvsclient.command.BuildableCommand;
import org.netbeans.lib.cvsclient.command.CommandAbortedException;
import org.netbeans.lib.cvsclient.command.CommandException;
import org.netbeans.lib.cvsclient.command.GlobalOptions;
import org.netbeans.lib.cvsclient.command.KeywordSubstitutionOptions;
import org.netbeans.lib.cvsclient.command.add.AddCommand;
import org.netbeans.lib.cvsclient.command.checkout.CheckoutCommand;
import org.netbeans.lib.cvsclient.command.commit.CommitCommand;
import org.netbeans.lib.cvsclient.command.edit.EditCommand;
import org.netbeans.lib.cvsclient.command.editors.EditorsCommand;
import org.netbeans.lib.cvsclient.command.unedit.UneditCommand;
import org.netbeans.lib.cvsclient.event.CVSAdapter;
import org.netbeans.lib.cvsclient.event.MessageEvent;
import org.netbeans.modules.versioning.system.cvss.SSHConnection;

public class CVSImpl implements Cvs,Serializable {

    /**
	 * 
	 */
	private static final long serialVersionUID = -5447217691318536615L;
	private static Logger log = Logger.getLogger(CVSImpl.class);
    private FacesContext context;
    private String cvs_root;
    private String server_host;
    private int server_port;
    private String repo_root;
    private String commit_message;
    private String get_message = null;
    static final GlobalOptions glop = new GlobalOptions();

    public CVSImpl() {
    	context = FacesContext.getCurrentInstance();
        cvs_root = context.getExternalContext().getInitParameter("il.co.etrader.backend.cms.CVSImpl.cvs_root");
        server_host = context.getExternalContext().getInitParameter("il.co.etrader.backend.cms.CVSImpl.server_host");
        server_port = Integer.parseInt(context.getExternalContext().getInitParameter("il.co.etrader.backend.cms.CVSImpl.server_port"));
        repo_root = context.getExternalContext().getInitParameter("il.co.etrader.backend.cms.CVSImpl.repo_root");
        commit_message = context.getExternalContext().getInitParameter("il.co.etrader.backend.cms.CVSImpl.commit_message");
    }
    
    public CVSImpl(String cvs_root, String server_host, int server_port, String repo_root, String commit_message) {
    	this.cvs_root = cvs_root;
    	this.server_host = server_host;
    	this.server_port = server_port;
    	this.repo_root = repo_root;
    	this.commit_message = commit_message;
    }
    
    public String getGet_message() {
        return get_message;
    }

    private class BasicListener extends CVSAdapter {
        /**
         * This class is responsible for CVS return message handling
         */
        private final StringBuffer taggedLine = new StringBuffer();

        public void messageSent(MessageEvent e) {
            String line = e.getMessage();

            if (e.isTagged()) {
                String message = MessageEvent.parseTaggedMessage(taggedLine, line);
                if (message != null && message.trim().isEmpty()) {
                    get_message = get_message + "\n" + message;
                }
            } else {
                if (line != null && !line.trim().isEmpty()) {
                    get_message = get_message + "\n" + line;
                }
            }
        }
    }

    private void initCVS(BuildableCommand CMD, String localPath, String uName, String pWord) {

        this.get_message = null;
        SSHConnection c = new SSHConnection(SocketFactory.getDefault(), this.server_host, this.server_port, uName, pWord);
        System.setProperty("CVS_SERVER", this.repo_root);
        c.setRepository(this.repo_root);
        try {
            c.open();
        } catch (CommandAbortedException e) {
            log.error("error open connection: " + e.toString());
            this.get_message = "error check log";
        } catch (org.netbeans.lib.cvsclient.connection.AuthenticationException e1) {
            log.error("error open connection: " + e1.toString());
            this.get_message = "error check log";
        }

        Client client = new Client(c, new StandardAdminHandler());
        client.getEventManager().addCVSListener(new BasicListener());
        client.setLocalPath(localPath);
        client.getEventManager().addCVSListener(new BasicListener());

        try {
//            c.open();
            CVSImpl.glop.setCVSRoot(CVSRoot.parse(this.cvs_root).toString());
            boolean cvsCommandResult = client.executeCommand(CMD, glop);
            log.trace("cvsCommandResult: " + cvsCommandResult);
//            System.out.println("cvsCommandResult: "+ cvsCommandResult);
            try {
                c.close();
            } catch (IOException e) {
                log.error("error close connection: " + e.toString());
                this.get_message = "error check log";
            }
        } catch (CommandAbortedException e) {
            log.error("error execute command: " + e.toString());
            this.get_message = "error check log";
        } catch (CommandException e1) {
            log.error("error execute command: " + e1.toString());
            this.get_message = "error check log";
        } catch (org.netbeans.lib.cvsclient.connection.AuthenticationException e2) {
            log.error("error execute command: " + e2.toString());
            this.get_message = "error check log";
        }
    }

    @Override
    public void checkoutDir(String baseDir, String source, String userName, String password) {

        CheckoutCommand cvnCmd = new CheckoutCommand();
        cvnCmd.setBuilder(null);
        cvnCmd.setRecursive(true);
        cvnCmd.setModule(source);
        cvnCmd.setPruneDirectories(true);
        initCVS(cvnCmd, baseDir, userName, password);

    }

    @Override
    public void checkoutFile(String baseDir, String source, String userName, String password) {
        CheckoutCommand cvnCmd = new CheckoutCommand();
        cvnCmd.setBuilder(null);
        cvnCmd.setRecursive(true);
        cvnCmd.setModule(source);
        cvnCmd.setPruneDirectories(true);
        initCVS(cvnCmd, baseDir, userName, password);

    }

    @Override
    public String editors(String source, String userName, String password) {
        EditorsCommand cvnCmd = new EditorsCommand();
        cvnCmd.setBuilder(null);
        cvnCmd.setRecursive(true);
        File listFile[] = { new File(source) };
        cvnCmd.setFiles(listFile);
        initCVS(cvnCmd, "", userName, password);
        return this.get_message;
    }

    @Override
    public void edit(String source, String userName, String password) {
        EditCommand cvnCmd = new EditCommand();
        cvnCmd.setBuilder(null);
        cvnCmd.setRecursive(true);
        File listFile[] = { new File(source) };
        cvnCmd.setFiles(listFile);
        initCVS(cvnCmd, "", userName, password);

    }

    @Override
    public void commit(String baseDir, String source, String userName, String password) {
        CommitCommand cvnCmd = new CommitCommand();
        cvnCmd.setBuilder(null);
        cvnCmd.setRecursive(true);
        File listFile[] = { new File(source) };
        cvnCmd.setFiles(listFile);
        cvnCmd.setMessage(this.commit_message);
        initCVS(cvnCmd, baseDir, userName, password);

    }
    
    @Override
    public void add(String baseDir, ArrayList<String> fileNames, String userName, String password, boolean isBinary) {
        if (!fileNames.isEmpty()) {
            AddCommand cvnCmd = new AddCommand();
            File listFiles[] = new File[fileNames.size()];
            int index = 0;
            for (String fileName : fileNames) {
                listFiles[index++] = new File(fileName);
            }
            cvnCmd.setFiles(listFiles);
            if (isBinary) {
            	// if the files are binary we must specifically add this option 
            	cvnCmd.setKeywordSubst(KeywordSubstitutionOptions.BINARY);
            }
            initCVS(cvnCmd, baseDir, userName, password);
        } else {
            System.out.println("Empty list of names, so we don't execute an add command");
        }
    }

    @Override
    public void unedit(String source, String userName, String password) {
        UneditCommand cvnCmd = new UneditCommand();
        cvnCmd.setBuilder(null);
        cvnCmd.setRecursive(true);
        File listFile[] = { new File(source) };
        cvnCmd.setFiles(listFile);
        initCVS(cvnCmd, "", userName, password);

    }

}
