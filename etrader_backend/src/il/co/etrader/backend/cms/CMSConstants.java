package il.co.etrader.backend.cms;

/**
 * A place to store all CMS specific constants.
 * 
 * @author pavelhe
 *
 */
public interface CMSConstants {
    
    /** EL expression for accessing {@link Cvs} session bean. */
    public static final String BIND_CVS = "#{CVS}";
    /** EL expression for accessing {@link BundleSelection} session bean. */
    public static final String BIND_BUNDLE_SELECTION = "#{cmsBundleSelection}";
    /** This is the name of the common CMS bean object stored in session. */
    public static final String COMMON_CMS_BEAN_NAME = "commonCMSBean";
    
    public static final String CONTENT_PATH_PROPERTY_NAME = "il.co.etrader.backend.cms.tmp_content_base_path";

	public enum fileType {
		property, image;
	}
	
}
