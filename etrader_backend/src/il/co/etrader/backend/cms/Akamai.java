package il.co.etrader.backend.cms;

public interface Akamai {

	public void refresh(String baseDir, String destination);
	public int getGet_code();
}
