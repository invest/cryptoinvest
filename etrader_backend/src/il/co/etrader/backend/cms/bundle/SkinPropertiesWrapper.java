/**
 * 
 */
package il.co.etrader.backend.cms.bundle;

import org.apache.commons.configuration.Configuration;
import org.apache.commons.configuration.PropertiesConfiguration;


/**
 * Wrapper bean for skin related properties and basi skin information used in CMS module.
 * 
 * @author pavelhe
 *
 */
public class SkinPropertiesWrapper {
    
    private int id;
    private String name;
    private String loacale;
    private String configurationFilePath;
    private PropertiesConfiguration propertiesConfiguration;
    private Configuration cmsDecoratedConfiguration;
    /**
     * @return the skin id
     */
    public int getId() {
        return id;
    }
    /**
     * @param id the skin id to set
     */
    public void setId(int id) {
        this.id = id;
    }
    /**
     * @return the skin name
     */
    public String getName() {
        return name;
    }
    /**
     * @param name the skin name to set
     */
    public void setName(String name) {
        this.name = name;
    }
    /**
     * @return the loacale
     */
    public String getLoacale() {
        return loacale;
    }
    /**
     * @param loacale the loacale to set
     */
    public void setLoacale(String loacale) {
        this.loacale = loacale;
    }
    /**
     * @return the configurationFilePath
     */
    public String getConfigurationFilePath() {
        return configurationFilePath;
    }
    /**
     * @param configurationFilePath the configurationFilePath to set
     */
    public void setConfigurationFilePath(String configurationFilePath) {
        this.configurationFilePath = configurationFilePath;
    }
    /**
     * @return the propertiesConfiguration
     */
    public PropertiesConfiguration getPropertiesConfiguration() {
        return propertiesConfiguration;
    }
    /**
     * @param propertiesConfiguration the propertiesConfiguration to set
     */
    public void setPropertiesConfiguration(PropertiesConfiguration propertiesConfiguration) {
        this.propertiesConfiguration = propertiesConfiguration;
    }
    /**
     * @return the cmsDecoratedConfiguration
     */
    public Configuration getCmsDecoratedConfiguration() {
        return cmsDecoratedConfiguration;
    }
    /**
     * @param cmsDecoratedConfiguration the cmsDecoratedConfiguration to set
     */
    public void setCmsDecoratedConfiguration(Configuration cmsDecoratedConfiguration) {
        this.cmsDecoratedConfiguration = cmsDecoratedConfiguration;
    }
    
    @Override
    public String toString() {
        return "SkinPropertiesWrapper [id=" + id + ", name=" + name
                                        + ", loacale=" + loacale
                                        + ", configurationFilePath=" + configurationFilePath
                                        + ", propertiesConfiguration=" + propertiesConfiguration
                                        + ", cmsDecoratedConfiguration=" + cmsDecoratedConfiguration + "]";
    }

}
