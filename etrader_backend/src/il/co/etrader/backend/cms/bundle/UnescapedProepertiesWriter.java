/**
 * 
 */
package il.co.etrader.backend.cms.bundle;

import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.configuration.PropertiesConfiguration.PropertiesWriter;
import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.lang.StringUtils;

/**
 * This class has almost the same behavior as {@link PropertiesWriter} except
 * that it does not escape UNICODE characters when writing a property's value.  
 * 
 * @author pavelhe
 *
 */
public class UnescapedProepertiesWriter extends PropertiesWriter {
	
	/** Constant for the initial size when creating a string buffer. */
    private static final int BUF_SIZE = 8;
    
    /** Constant for the escaping character.*/
    private static final String ESCAPE = "\\";

    /** Constant for the escaped escaping character.*/
    private static final String DOUBLE_ESC = ESCAPE + ESCAPE;
    
    /** The list of possible key/value separators */
    private static final char[] SEPARATORS = new char[] {'=', ':'};
    
    /** The white space characters used as key/value separators. */
    private static final char[] WHITE_SPACE = new char[]{' ', '\t', '\f'};
    
    /** The delimiter for multi-valued properties.*/
    private char delimiter;
    
	/**
	  * Constructor.
      *
      * @param writer a Writer object providing the underlying stream
      * @param delimiter the delimiter character for multi-valued properties
      */
	public UnescapedProepertiesWriter(Writer writer, char delimiter) {
		super(writer, delimiter);
		this.delimiter = delimiter;
	}

	/**
     * Writes the given property and its value. If the value happens to be a
     * list, the {@code forceSingleLine} flag is evaluated. If it is
     * set, all values are written on a single line using the list delimiter
     * as separator.
     *
     * @param key the property key
     * @param value the property value
     * @param forceSingleLine the &quot;force single line&quot; flag
     * @throws IOException if an error occurs
     * @since 1.3
     */
	@Override
    public void writeProperty(String key, Object value,
            boolean forceSingleLine) throws IOException
    {
        String v;

        if (value instanceof List)
        {
            List<?> values = (List<?>) value;
            if (forceSingleLine)
            {
                v = makeSingleLineValue(values);
            }
            else
            {
                writeProperty(key, values);
                return;
            }
        }
        else
        {
            v = escapeValue(value, false);
        }

        write(escapeKey(key));
        write(fetchSeparator(key, value));
        write(v);

        writeln(null);
    }
	
	 /**
     * Performs the escaping of backslashes in the specified properties
     * value. Because a double backslash is used to escape the escape
     * character of a list delimiter, double backslashes also have to be
     * escaped if the property is part of a (single line) list. Then, in all
     * cases each backslash has to be doubled in order to produce a valid
     * properties file.<p>
     * 
     * Bear in mind that this method is almost the same as the one in
     * {@link PropertiesWriter} except that it does not use the standard
     * Apache-Lang {@link StringEscapeUtils#escapeJavaScript(String)}. This is
     * done to prevent UNICODE character escaping for writers that use UTF encoding.
     *
     * @param value the value to be escaped
     * @param inList a flag whether the value is part of a list
     * @return the value with escaped backslashes as string
     */
    private String handleBackslashs(Object value, boolean inList)
    {
        String strValue = String.valueOf(value);

        if (inList && strValue.indexOf(DOUBLE_ESC) >= 0)
        {
            char esc = ESCAPE.charAt(0);
            StringBuilder buf = new StringBuilder(strValue.length() + BUF_SIZE);
            for (int i = 0; i < strValue.length(); i++)
            {
                if (strValue.charAt(i) == esc && i < strValue.length() - 1
                        && strValue.charAt(i + 1) == esc)
                {
                    buf.append(DOUBLE_ESC).append(DOUBLE_ESC);
                    i++;
                }
                else
                {
                    buf.append(strValue.charAt(i));
                }
            }

            strValue = buf.toString();
        }

        return escapeJava(strValue);
    }
    
    /**
     * Copied from {@link PropertiesWriter}.
     * 
     * Escapes the given property value. Delimiter characters in the value
     * will be escaped.
     *
     * @param value the property value
     * @param inList a flag whether the value is part of a list
     * @return the escaped property value
     * @since 1.3
     */
    private String escapeValue(Object value, boolean inList)
    {
        String escapedValue = handleBackslashs(value, inList);
        if (delimiter != 0)
        {
            escapedValue = StringUtils.replace(escapedValue, String.valueOf(delimiter), ESCAPE + delimiter);
        }
        return escapedValue;
    }
    
    /**
     * Copied from {@link PropertiesWriter}.
     * 
     * Transforms a list of values into a single line value.
     *
     * @param values the list with the values
     * @return a string with the single line value (can be <b>null</b>)
     * @since 1.3
     */
    private String makeSingleLineValue(List<?> values)
    {
        if (!values.isEmpty())
        {
            Iterator<?> it = values.iterator();
            String lastValue = escapeValue(it.next(), true);
            StringBuilder buf = new StringBuilder(lastValue);
            while (it.hasNext())
            {
                // if the last value ended with an escape character, it has
                // to be escaped itself; otherwise the list delimiter will
                // be escaped
                if (lastValue.endsWith(ESCAPE) && (countTrailingBS(lastValue) / 2) % 2 != 0)
                {
                    buf.append(ESCAPE).append(ESCAPE);
                }
                buf.append(delimiter);
                lastValue = escapeValue(it.next(), true);
                buf.append(lastValue);
            }
            return buf.toString();
        }
        else
        {
            return null;
        }
    }
    
    /**
     * Copied from {@link PropertiesWriter}.
     * 
     * Escape the separators in the key.
     *
     * @param key the key
     * @return the escaped key
     * @since 1.2
     */
    private String escapeKey(String key)
    {
        StringBuilder newkey = new StringBuilder();

        for (int i = 0; i < key.length(); i++)
        {
            char c = key.charAt(i);

            if (ArrayUtils.contains(SEPARATORS, c) || ArrayUtils.contains(WHITE_SPACE, c))
            {
                // escape the separator
                newkey.append('\\');
                newkey.append(c);
            }
            else
            {
                newkey.append(c);
            }
        }

        return newkey.toString();
    }

    
    /**
     * Copied from {@link PropertiesWriter}.
     * 
     * Returns the number of trailing backslashes. This is sometimes needed for
     * the correct handling of escape characters.
     *
     * @param line the string to investigate
     * @return the number of trailing backslashes
     */
    private static int countTrailingBS(String line)
    {
        int bsCount = 0;
        for (int idx = line.length() - 1; idx >= 0 && line.charAt(idx) == '\\'; idx--)
        {
            bsCount++;
        }

        return bsCount;
    }
    
    /**
     * Escapes special java characters but does not escape UNICODE symbols.
     * 
     * @param str String to escape values in, may be null
     */
    private String escapeJava(String str) {
    	if (str == null) {
    		return "";
    	}
    	StringWriter writer = new StringWriter(str.length() * 2);
    	
        int sz;
        sz = str.length();
        for (int i = 0; i < sz; i++) {
            char ch = str.charAt(i);

            switch (ch) {
                case '\b':
                    writer.write('\\');
                    writer.write('b');
                    break;
                case '\n':
                    writer.write('\\');
                    writer.write('n');
                    break;
//                case '\t':
//                    writer.write('\\');
//                    writer.write('t');
//                    break;
                case '\f':
                    writer.write('\\');
                    writer.write('f');
                    break;
                case '\r':
                    writer.write('\\');
                    writer.write('r');
                    break;
                case '\'':
                    writer.write('\'');
                    break;
// Should not be escaped                    
//                case '"':
//                    writer.write('\\');
//                    writer.write('"');
//                    break;
                case '\\':
                    writer.write('\\');
                    writer.write('\\');
                    break;
// Should not be escaped                    
//                case '/':
//                    writer.write('\\');
//                    writer.write('/');
//                    break;
                default :
                    writer.write(ch);
                    break;
            }
                
        }
        
        return writer.toString();
    }
    
}
