package il.co.etrader.backend.cms;

import il.co.etrader.backend.cms.CMSConstants.fileType;
import il.co.etrader.backend.cms.common.CMSUtil;
import il.co.etrader.backend.cms.common.CommonCMSBean;
import il.co.etrader.backend.cms.common.PropertyTypeEnum;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpSession;

import org.apache.commons.configuration.ConfigurationException;
import org.apache.log4j.Logger;

import com.anyoption.common.beans.JmxServerConfig;
import com.anyoption.common.managers.JmxServerConfigManager;
public class CmsGuiBindingsManager implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -6380268964127668249L;
	// the name of the application which is edited example: anyoption, etrader, backend
	private static final String ANYOPTION = "anyoption";
	private static final Logger logger = Logger.getLogger(CmsGuiBindingsManager.class);
	private String editors;
    private String userName;
	private String password;
	private String source="etrader_ant_scripts/test/test.txt";
	private String baseDir;
	private String cvsMessage;
	private String sessionId;
	private String uploadImgPath;
	private Cvs interfaceCvs;
	private Akamai  interfaceAkamai;
	private BundleSelection ineterfaceBundleSelection;
	private Ftp interfaceFtp;
	private CacheReloader interfaceCacheReloader;
	private CommonCMSBean commonCmsBean;
	
	@SuppressWarnings("deprecation")
	public CmsGuiBindingsManager()  throws SQLException{
		
		FacesContext facesContext = FacesContext.getCurrentInstance();
		
		interfaceCvs = (Cvs)facesContext.getApplication().createValueBinding("#{CVS}").getValue(facesContext);
		interfaceAkamai = (Akamai)facesContext.getApplication().createValueBinding("#{Akamai}").getValue(facesContext);
		ineterfaceBundleSelection = (BundleSelection)facesContext.getApplication().createValueBinding("#{cmsBundleSelection}").getValue(facesContext);
		commonCmsBean = (CommonCMSBean) CMSUtil.getSessionObject(CMSConstants.COMMON_CMS_BEAN_NAME);
		interfaceFtp = (Ftp) facesContext.getApplication().createValueBinding("#{Ftp}").getValue(facesContext); 
		interfaceCacheReloader = (CacheReloader)facesContext.getApplication().createValueBinding("#{Jmx}").getValue(facesContext); 
				
		HttpSession session = (HttpSession) facesContext.getExternalContext().getSession(false);
		setSessionId(session.getId());
		
		userName = commonCmsBean.getCvsUserName();
		password = commonCmsBean.getCvsPassword();
		baseDir=commonCmsBean.getCmsCheckoutDir();
	}
	
	public CmsGuiBindingsManager(String bindingValue)  throws SQLException{
		
		FacesContext facesContext = FacesContext.getCurrentInstance();
		
		interfaceCacheReloader = (CacheReloader)facesContext.getApplication().createValueBinding(bindingValue).getValue(facesContext); 
	}

	// for property files
	public boolean edit(CmsDataObjects cmsDataObjects){
		source=getSource(cmsDataObjects);
		
		try {
			deleteFile(baseDir+source);
		} catch (Exception e) {
			cvsMessage=e.toString();
			return false;
		}
		
		interfaceCvs.checkoutFile(baseDir,source, userName, password);
		cvsMessage=interfaceCvs.getGet_message();
		
		commonCmsBean.setCurrentCmsPropertyStage(CommonCMSBean.CmsStage.EDITORS);
		commonCmsBean.setCurrentCvsPropertyPath(source);
		
		if(!isExistFile(baseDir+source)){
			cvsMessage="File:"+source+" is not chekout!"+cvsMessage;
			return false;
		}
		editors=interfaceCvs.editors(source, userName, password);
		cvsMessage=interfaceCvs.getGet_message();
		
        commonCmsBean.setCurrentCmsPropertyStage(CommonCMSBean.CmsStage.EDITORS);
        commonCmsBean.setCurrentCvsPropertyPath(source);
        
		if (editors==null){
			interfaceCvs.edit(baseDir+source, userName, password);
			try {
				ineterfaceBundleSelection.load(cmsDataObjects.getSkin());
			}  catch (Exception e) {				
				 cvsMessage=e.getMessage();
				 return false;
			}
			
			commonCmsBean.setCurrentCmsPropertyStage(CommonCMSBean.CmsStage.EDIT);
	        commonCmsBean.setCurrentCvsPropertyPath(baseDir+source);
	        
	        cvsMessage="File is in EDIT mode!";			
		}
		else {
			cvsMessage=getEditorsMsg(editors);
			return false;
		}
		return true;
	}
	
	public boolean editImg(CmsDataObjects cmsDataObjects){		
		source=getSource(cmsDataObjects);
		
		try {
			deleteFile(baseDir+source);
		} catch (Exception e) {
			cvsMessage=e.toString();
			return false;
		}
		
		interfaceCvs.checkoutFile(baseDir,source, userName, password);
		cvsMessage=interfaceCvs.getGet_message();
		
		commonCmsBean.setCurrentCmsImageStage(CommonCMSBean.CmsStage.EDITORS);
		commonCmsBean.setCurrentCvsImagePath(source);
		
		if(!isExistFile(baseDir+source)){
			cvsMessage="File:"+source+" is not chekout!"+cvsMessage;
			return false;
		}
		editors=interfaceCvs.editors(source, userName, password);
        commonCmsBean.setCurrentCmsImageStage(CommonCMSBean.CmsStage.EDITORS);
        commonCmsBean.setCurrentCvsImagePath(source);
        
		if (editors==null){
			interfaceCvs.edit(baseDir+source, userName, password);
	        commonCmsBean.setCurrentCmsImageStage(CommonCMSBean.CmsStage.EDIT);
	        commonCmsBean.setCurrentCvsImagePath(baseDir+source);
	        
	        uploadImgPath=baseDir+source;
			cvsMessage="File is in EDIT mode!";
		}
		else {
			cvsMessage=getEditorsMsg(editors);
			return false;
		}
		return true;
	}
	
		public boolean preview(CmsDataObjects cmsDataObjects) throws ConfigurationException, IOException{
		   if(cmsDataObjects.getType().equals(PropertyTypeEnum.IMAGE)){
			   interfaceFtp.preview(baseDir, source, fileType.image, userName, password);
		      
			   commonCmsBean.setCurrentCmsImageStage(CommonCMSBean.CmsStage.PREVIEW);
		       commonCmsBean.setCurrentCvsImagePath(source);
	
		       cvsMessage = "Ftp result: " + interfaceFtp.getMessage();
		   }
		   else{
			  ineterfaceBundleSelection.setValue(cmsDataObjects.getSkin(), cmsDataObjects.getPage(), cmsDataObjects.getType(), cmsDataObjects.getKey(), cmsDataObjects.getValue());
			  interfaceFtp.preview(baseDir, source, fileType.property, userName, password);
			   commonCmsBean.setCurrentCmsPropertyStage(CommonCMSBean.CmsStage.PREVIEW);
		       commonCmsBean.setCurrentCvsPropertyPath(source);
	
		       cvsMessage = "Ftp result: " + interfaceFtp.getMessage();
		       
		       interfaceCacheReloader.clearTestEnvCache(ANYOPTION);		      
		   }	
			return true;
		}
		
	public boolean initAssetIndexMarket(String application, ArrayList<String> serverResult) throws SQLException {		
		boolean res = true;		
		int i = 0;
		for(JmxServerConfig serves : JmxServerConfigManager.getJmxServersConfig()){
			i++;
			if(!interfaceCacheReloader.initAssetIndexMarketsCache(application, serves)){
				res = false;
			}
			serverResult.add( i + ".Server:" + serves.getAddress() + " Is Refresh:" + res);
		}
		return res;
	}
	
	public boolean rollback(CmsDataObjects cmsDataObjects ){
		interfaceCvs.unedit(baseDir+source, userName, password);
	   
		if(cmsDataObjects.getType().equals(PropertyTypeEnum.IMAGE)){
		   commonCmsBean.setCurrentCmsImageStage(CommonCMSBean.CmsStage.UNEDIT);
		   commonCmsBean.setCurrentCvsImagePath(source);
		} else {
		   commonCmsBean.setCurrentCmsPropertyStage(CommonCMSBean.CmsStage.UNEDIT);
		   commonCmsBean.setCurrentCvsPropertyPath(source);
		}
		
		try {
			deleteFile(baseDir+source);
		} catch (Exception e) {
			cvsMessage=e.toString();
			return false;
		}
		
		if(isExistFile(baseDir+source)){
			cvsMessage="File:"+source+" is not delete";
			return false;
		}
				
		interfaceCvs.checkoutFile(baseDir,source, userName, password);
		cvsMessage=interfaceCvs.getGet_message();
		try {
			ineterfaceBundleSelection.load(cmsDataObjects.getSkin());
		} catch (Exception e) {
			e.printStackTrace();
			cvsMessage="Error when load properties files "+e.toString();
		} 
		
	    if(cmsDataObjects.getType().equals(PropertyTypeEnum.IMAGE)){
			   interfaceFtp.preview(baseDir, source, fileType.image, userName, password);
			   cvsMessage = "Ftp result: " + interfaceFtp.getMessage();
			   
			   commonCmsBean.setCurrentCmsImageStage(CommonCMSBean.CmsStage.CHECKOUT);
			   commonCmsBean.setCurrentCvsImagePath(source);
			   
		    }else{
			   interfaceFtp.preview(baseDir, source, fileType.property, userName, password);
			   cvsMessage = "Ftp result: " + interfaceFtp.getMessage();
			   interfaceCacheReloader.clearTestEnvCache(ANYOPTION);
			   
			   commonCmsBean.setCurrentCmsPropertyStage(CommonCMSBean.CmsStage.CHECKOUT);
			   commonCmsBean.setCurrentCvsPropertyPath(source);
		    }
		
	    if(!isExistFile(baseDir+source)){
			cvsMessage="File:"+source+" is not chekout whenr ROLLBACK!"+cvsMessage;
			return false;
		}

		cvsMessage="File is ROLLBACK and in UNEDIT mode!";
		return true;
	}
	
	public boolean commit(CmsDataObjects cmsDataObjects){
		
	    if(cmsDataObjects.getType().equals(PropertyTypeEnum.IMAGE)){
				interfaceCvs.commit(baseDir, baseDir+source, userName, password);
				cvsMessage=interfaceCvs.getGet_message();
   			    interfaceCvs.unedit(source, userName, password);
			    interfaceFtp.publish(baseDir, source, fileType.image, userName, password);
    		    interfaceAkamai.refresh("http://images.anyoption.com/", cmsDataObjects.getAkamaiDestination());
				if( !checkAkamai(interfaceAkamai.getGet_code())){
					cvsMessage="File is COMMIT and ERROR Akamai";
					return false;
				}
			    
			    commonCmsBean.setCurrentCmsImageStage(CommonCMSBean.CmsStage.END);
			    commonCmsBean.setCurrentCvsImagePath(source);
				
		    }else{
				interfaceCvs.commit(baseDir, baseDir+source, userName, password);
				cvsMessage=interfaceCvs.getGet_message();
   			    interfaceCvs.unedit(source, userName, password);
			    interfaceFtp.publish(baseDir, source, fileType.property, userName, password);
				cvsMessage = "Ftp result: " + interfaceFtp.getMessage();
				interfaceCacheReloader.clearProdEnvCache(ANYOPTION);
				
			    commonCmsBean.setCurrentCmsPropertyStage(CommonCMSBean.CmsStage.END);
			    commonCmsBean.setCurrentCvsPropertyPath(source);
			    
		    }
		return true;
	}
	
    private boolean checkAkamai (int code){
    	String strCgode=Integer.toString(code);
    	if(strCgode.substring(1, 1).contains("3") || strCgode.substring(1, 1).contains("4") ){
    		return false;
    	}
    	return true;    	
    }
	
	public ArrayList<CmsDataObjects> search(int skinId,String page,PropertyTypeEnum type,String key){
		return ineterfaceBundleSelection.getList(skinId, page, type, key);
	}
	
	public ArrayList<SelectItem> getPageList(){
	   return ineterfaceBundleSelection.getPageList();
	}
	
	public ArrayList<SelectItem> getSkinList(){				
	   return ineterfaceBundleSelection.getSkinList();
	}
	
	public ArrayList<SelectItem> getKeyList(boolean isAlt){
	   return ineterfaceBundleSelection.getKeyList(isAlt );
	}
	
	public String getCvsMessage(){
		return cvsMessage;
	}

	public String getSessionId() {
		return sessionId;
	}

	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}
	
	public String getUploadImagePath() {
		return uploadImgPath;
	}
	
	private boolean isExistFile(String filePath){
		File fileCheck=new File(filePath);
		return fileCheck.exists();
	}
	
	private String getEditorsMsg(String editors){
		String[] splitText = editors.split("\t");
		return "The file is edit from:"+splitText[1]+" DateTime:" + splitText[2]+" PC:" +splitText[3];
	}
	
    private static void deleteFile(String filePath) throws IOException {
    	
    	File file=new File(filePath);
    	logger.debug("Begin deleting");
    	boolean test=   file.delete();
         System.out.print("Deleted:"+ test+ "file:"+filePath);
         logger.debug("End deleting:"+test+ " file:"+filePath);             
    }

	private String getSource(CmsDataObjects cmsDataObjects){
	  if(cmsDataObjects.getType().equals(PropertyTypeEnum.IMAGE)){	
		return FacesContext.getCurrentInstance().getExternalContext()
                .getInitParameter("il.co.etrader.backend.cms.image_default_path")
                + ineterfaceBundleSelection.getSkinImagesFolder(cmsDataObjects.getSkin())+"/" + getParseKey(cmsDataObjects.getKey());
	  }else{
		  String tmpStr = ineterfaceBundleSelection.getSkinFilePath(cmsDataObjects.getSkin()).replace(baseDir, "");
		    
		    return tmpStr;
	  }
	}
	
	private String getParseKey(String key){
		String parsedKey="";
		String[] temp;
		String delimiter = "\\.";
		  temp = key.split(delimiter);
		  for(int i =0; i < temp.length ; i++){
			 if(i==temp.length-2){
				 parsedKey=parsedKey+(temp[i])+'.';
			 }else if(i==temp.length-1){
				 parsedKey=parsedKey+(temp[i]);
			 }else{
				 parsedKey=parsedKey+(temp[i])+'/'; 
			 }			  
		  }
		return parsedKey;
	}
}
