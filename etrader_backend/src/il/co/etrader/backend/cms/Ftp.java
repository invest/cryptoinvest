package il.co.etrader.backend.cms;

import il.co.etrader.backend.cms.CMSConstants.fileType;

public interface Ftp {

	public void preview(String baseDir, String destination, fileType type, String cvsUser, String cvsPass) ;

	public void publish(String baseDir, String destination, fileType type, String cvsUser, String cvsPass);

	public String getMessage();
}
