package il.co.etrader.backend.bl_managers;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;

import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;

import org.apache.log4j.Logger;

import com.anyoption.backend.daos.TransactionDAO;

import il.co.etrader.backend.bl_vos.PopulationEntry;
import il.co.etrader.backend.bl_vos.User;
import il.co.etrader.backend.bl_vos.WriterWrapper;
import il.co.etrader.backend.dao_managers.PopulationEntriesDAO;
import il.co.etrader.backend.util.Constants;
import il.co.etrader.backend.util.Utils;
import il.co.etrader.bl_managers.WritersManagerBase;
import il.co.etrader.bl_vos.RankStatus;
import il.co.etrader.bl_vos.Writer;
import il.co.etrader.bl_vos.WriterMaxAssignment;
import il.co.etrader.dao_managers.WritersDAO;
import il.co.etrader.util.ConstantsBase;


public class WritersManager extends WritersManagerBase {

	private static final Logger logger = Logger.getLogger(User.class);
	private static HashMap<Long, String> systemWriters;
	public static final String FILTER_SYSTEM_WRITERS = "systemWriters";

	/**
	 * Returns the writers skin
	 * @param writerId  - writer ID
	 * @return list of all writer skins
	 * @throws SQLException
	 */
	public static ArrayList<Integer> getWriterSkins(long writerId) throws SQLException {
		Connection con = getConnection();
		ArrayList<Integer> list = null;
		try {
			list = WritersDAO.getWriterSkins(con, writerId);
		}
		finally {
			closeConnection(con);
		}
		return list;
	}

	/**
	 * Returns a select items list of the writer's skins
	 * @param writerId - the writer id
	 * @return Select Items list of all writers skins
	 * @throws SQLException
	 */
	public static ArrayList<SelectItem> getWriterSkinsSI(long writerId) throws SQLException{
		Connection con = getConnection();
		ArrayList<SelectItem> list = null;
		try {
			list = WritersDAO.getWriterSkinsSI(con, writerId);
		}
		finally {
			closeConnection(con);
		}
		return list;
	}
	
	/**
	 * Returns a select items list of the writer's regulated skins
	 * @param writerId - the writer id
	 * @return Select Items list of all writers skins
	 * @throws SQLException
	 */
	public static ArrayList<SelectItem> getWriterSkinsRegulatedSI(long writerId) throws SQLException{
		Connection con = getConnection();
		ArrayList<SelectItem> list = null;
		try {
			list = WritersDAO.getWriterSkinsRegulatedSI(con, writerId);
		}
		finally {
			closeConnection(con);
		}
		return list;
	}


	/**
	 * Retunrs a Select Items List of the writers's currencies
	 * @param writerId - the writer id
	 * @param locale - the writer locale
	 * @return Select Items list of all writers currencies
	 * @throws SQLException
	 */
	public static ArrayList<SelectItem> getWriterCurrenciesSI(long writerId, Locale locale) throws SQLException{
		Connection con = getConnection();
		ArrayList<SelectItem> list = null;
		try {
			list = WritersDAO.getWriterCurrenciesSI(con, writerId, locale);
		}
		finally {
			closeConnection(con);
		}
		return list;
	}

	/**
	 * Retunrs a Select Items List of the writers's languages
	 * @param writerId - the writer id
	 * @return Select Items list of all writers languages
	 * @throws SQLException
	 */
	public static ArrayList<Long> getWriterLanguages(int writerId) throws SQLException{
		Connection con = getConnection();
		ArrayList<Long> list = null;
		try {
			list = WritersDAO.getWriterLanguages(con, writerId);
		}
		finally {
			closeConnection(con);
		}
		return list;
	}

	/**
	 * Returns the writers UtcOffset
	 * @param writerId - the writer's Id
	 * @return a String representing the writers UTC Offset
	 * @throws SQLException
	 */
	public static String getWriterUtcOffset(long writerId) throws SQLException{
		Connection con = getConnection();
		String utcOffset = ConstantsBase.EMPTY_STRING;
		try {
			utcOffset = WritersDAO.getWriterUtcOffset(con, writerId);
		}
		finally {
			closeConnection(con);
		}
		return utcOffset;
	}

	/**
	 * Get writers overView details
	 * @return
	 * @throws SQLException
	 */
	public static ArrayList<Writer> getWritersOverAllView(long skinId,long writerFilter, long salesType) throws SQLException{
		Connection con = getSecondConnection();
		ArrayList<Writer> list = null;
		try {
			list = WritersDAO.getWritersOverAllView(con, skinId,writerFilter, Utils.getWriter().getSkinsToString(), salesType);
		}
		finally {
			closeConnection(con);
		}
		return list;
	}

	/**
	 * Get writers todayView details
	 * @return
	 * @throws SQLException
	 */
	public static ArrayList<Writer> getWritersTodayView(long skinId,long writerFilter) throws SQLException{
		Connection con = getSecondConnection();
		ArrayList<Writer> list = null;
		try {
			FacesContext context = FacesContext.getCurrentInstance();
			WriterWrapper wr = (WriterWrapper) context.getApplication().createValueBinding(Constants.BIND_WRITER_WRAPPER).getValue(context);
			list = WritersDAO.getWritersTodayView(con, skinId,writerFilter,wr.getSkinsToString());
			Calendar c = Calendar.getInstance();
			Date from = c.getTime();
			c.set(Calendar.HOUR_OF_DAY, 23);
			c.set(Calendar.MINUTE, 59);
			c.set(Calendar.SECOND, 59);
			Date to = c.getTime();
			for (Writer w : list) {
				ArrayList<PopulationEntry> callBacks =  PopulationEntriesDAO.getPopulationEntriesByEntryType(con, 0, w.getId(), Constants.POP_ENTRY_TYPE_CALLBACK, from,
					 to,null,null, 0, false, 0, null, 0, null, null, wr, 0, 0, null, 0, "0", null, false, false, 0, 0, 0, ConstantsBase.ALL_FILTER_ID, null);
//				ArrayList<PopulationEntry> callBacks =  PopulationEntriesDAO.getPopulationEntries(con, 0, 0, 0, 0,
//						 w.getId(), Constants.POP_ENTRY_TYPE_CALLBACK, 0, 0, c.getTime(), c.getTime(), 0, 0,
//							false, 0, null, 0, 0, null, false, null, null, from, to, 0);
				w.setCallBacksRecords(callBacks.size());
			}
		}
		finally {
			closeConnection(con);
		}
		return list;
	}


	 public static HashMap<Long,Writer> getAllWritersMap()  throws SQLException{
		  Connection con = getConnection();
   	      HashMap<Long,Writer> map=new HashMap<Long,Writer>();

	   	   try {
	   		map = WritersDAO.getAllWritersMap(con);
			}
			finally {
				closeConnection(con);
			}
			return map;
	}

    /**
     * Set writers number of CMS open tabs
     * 
     * @param direction
     *            -1 or 1 depending of login or logout
     * @param writerId
     * @throws SQLException
     */
    public static void setWritersCMS(int direction, Long writerId) throws SQLException {
        Connection con = getConnection();

        try {
            WritersDAO.setWritersCMS(con, direction, writerId);
        } finally {
            closeConnection(con);
        }
    }

    /**
     * Get writers number of CMS open tabs
     * 
     * @param writerId
     * @return number of open tabs
     * @throws SQLException
     */
    public static int getWritersCMS(Long writerId) throws SQLException {
        Connection con = getConnection();
        int writerCMSCount;

        try {
            writerCMSCount = WritersDAO.getWritersCMS(con, writerId);
        } finally {
            closeConnection(con);
        }
        return writerCMSCount;
    }
    
    /**
     * Reset CMS user counter
     * 
     * @param userName
     * @return number of open tabs
     * @throws SQLException
     */
    public static boolean resetWritersCMS(String userName) throws SQLException {
        Connection con = getConnection();
        int rowsUpdated;

        try {
            rowsUpdated = WritersDAO.resetWritersCMS(con, 0, userName);
        } finally {
            closeConnection(con);
        }
        
        if (rowsUpdated < 1) {
            return false;
        } else {
            return true;
        }
    }

    public static ArrayList<RankStatus> getRetentionRepresentativeAssignments(long writerID) throws SQLException {
    	Connection con = getConnection();
    	ArrayList<RankStatus> retval = null;
        try {
        	retval = WritersDAO.getRetentionRepresentativeAssignments(con, writerID);
        } finally {
            closeConnection(con);
        }
        
        return retval;
    }
    
    public static ArrayList<WriterMaxAssignment> getWriterMaxAutoAssignment(long writerId) throws SQLException {
    	Connection con = getConnection();
    	ArrayList<WriterMaxAssignment> retval = null;
        try {
        	retval = WritersDAO.getWriterMaxAutoAssignment(con, writerId);
        } finally {
            closeConnection(con);
        }
        
        return retval;
    	
    }
    
    public static ArrayList<SelectItem> getWriterSkinsNotRegulatedSI(long writerId)  throws SQLException{
    	
    	Connection con = getConnection();
		ArrayList<SelectItem> list = null;
		try {
			list = WritersDAO.getWriterSkinsNotRegulatedSI(con, writerId);
		}
		finally {
			closeConnection(con);
		}
		return list;
    	
    }

	public static HashMap<Long, Writer> getChatWriters() throws SQLException {
		Connection con = null;
		HashMap<Long, Writer> list = null;
		try {
			con = getConnection();
			list = WritersDAO.getChatWriters(con);
		} finally {
			closeConnection(con);
		}
		return list;
	}
	
	/**
	 * Get Writer Skins Group By Language SI
	 * @param writerId
	 * @return list as ArrayList<SelectItem>
	 * @throws SQLException
	 */
	public static ArrayList<SelectItem> getWriterSkinsGroupByLanguageSI(long writerId) throws SQLException {
		Connection con = getConnection();
		ArrayList<SelectItem> list = null;
		try {
			list = WritersDAO.getWriterSkinsGroupByLanguageSI(con, writerId);
		}
		finally {
			closeConnection(con);
		}
		return list;
	}
	
	public static HashMap<Long, String> getSystemWriters() {
		if (systemWriters == null) {
			Connection con = null;
			try {
				con = getConnection();
				systemWriters = TransactionDAO.getFilters(con).get(FILTER_SYSTEM_WRITERS);
			} catch (SQLException ex) {
				logger.error("Can't lazzy load system writers: ", ex);
			} finally {
				closeConnection(con);
			}
		}
		return systemWriters;
	}
	
	public static ArrayList<SelectItem> getUrlSkins() {
		ArrayList<SelectItem> skins = new ArrayList<SelectItem>();
		Connection con = null;
		try {
			con = getConnection();
			skins = WritersDAO.getUrlSkins(con);
		} catch (SQLException ex) {
			logger.error("Can't get url skins: ", ex);
		} finally {
			closeConnection(con);
		}
		return skins;
	}
}
