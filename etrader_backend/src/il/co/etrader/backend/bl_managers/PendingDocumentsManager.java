package il.co.etrader.backend.bl_managers;

import il.co.etrader.backend.bl_vos.PendingDocument;
import il.co.etrader.backend.dao_managers.PendingDocumentsDAO;
import il.co.etrader.backend.dao_managers.PendingDocumentsDAOFilter;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;

import com.anyoption.common.managers.BaseBLManager;

public class PendingDocumentsManager extends BaseBLManager {
	
	public static ArrayList<PendingDocument> getPendingDocumentsNotRegulated(PendingDocumentsDAOFilter p) throws SQLException {
	  	Connection con = getConnection();
	  	try {
	  		return PendingDocumentsDAO.getPendingDocumentsNotRegulated(con, p);
	  	} finally {
	  		closeConnection(con);
	  	}
	}
	
	public static ArrayList<PendingDocument> getPendingDocumentsRegulated(PendingDocumentsDAOFilter p) throws SQLException {
	  	Connection con = getConnection();
	  	try {
	  		return PendingDocumentsDAO.getPendingDocumentsRegulated(con, p);
	  	} finally {
	  		closeConnection(con);
	  	}
	}

	public static int getPendingDocumentsNotRegulatedCount(PendingDocumentsDAOFilter p) throws SQLException {
	  	Connection con = getConnection();
	  	try {
	  		return PendingDocumentsDAO.getPendingDocumentsNotRegulatedCount(con, p);
	  	} finally {
	  		closeConnection(con);
	  	}
	}
	
	public static int getPendingDocumentsRegulatedCount(PendingDocumentsDAOFilter p) throws SQLException {
	  	Connection con = getConnection();
	  	try {
	  		return PendingDocumentsDAO.getPendingDocumentsRegulatedCount(con, p);
	  	} finally {
	  		closeConnection(con);
	  	}
	}
	
	public static long getPendingDocsNum(long userId) throws SQLException {
	  	Connection con = getConnection();
	  	try {
	  		return PendingDocumentsDAO.getPendingDocsNum(con, userId);
	  	} finally {
	  		closeConnection(con);
	  	}
	}
}