package il.co.etrader.backend.bl_managers;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;

import javax.faces.model.SelectItem;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import com.anyoption.common.beans.Bonus;

import il.co.etrader.backend.dao_managers.BonusDAO;
import il.co.etrader.bl_managers.BonusManagerBase;
import il.co.etrader.util.CommonUtil;

public class BonusManager extends BonusManagerBase {

    private static final Logger logger = Logger.getLogger(BonusManager.class);

	public static ArrayList<Bonus> getAllBonuses() throws SQLException {
		Connection con = getConnection();
		try {
			return BonusDAO.getAll(con);
		} finally {
			closeConnection(con);
		}
	}

    public static void insertUpdateBonus(Bonus bonus) throws SQLException {
        if (logger.isEnabledFor(Level.DEBUG)) {
            String ls = System.getProperty("line.separator");
            logger.log(Level.DEBUG,"Insert/update bonus: " + ls + bonus.toString());
         }

        Connection con = getConnection();

        try {
            if ( bonus.getId() == 0 ) {  // insert
                BonusDAO.insert(con, bonus);
            } else { // update
                BonusDAO.update(con, bonus);
            }

        } finally {
            closeConnection(con);
        }

    }

    /**
     * Get bonus list by writer group and population id
     * @param writerGroupId writer group id
     * @param populationTypeId population type id
     * @return
     * @throws SQLException
     */
	public static ArrayList getBonusByWriterGroupAPopulation(long writerGroupId, long populationTypeId, String skins, long skinId) throws SQLException {
		Connection con = null;
		try {
			con = getConnection();
			ArrayList list = new ArrayList();
			list = BonusDAO.getBonusByWriterGroupAPopulation(con, writerGroupId, populationTypeId, skins, skinId);
			Collections.sort(list, new CommonUtil.selectItemComparator());
			return list;
		} finally {
			closeConnection(con);
		}
	}

	/**
     * Get bonus list by skin
     * @param skinId
     * @return
     * @throws SQLException
     */
	public static ArrayList<SelectItem> getBonusBySkin(long skinId) throws SQLException {
		Connection con = null;
		try {
			con = getConnection();
			return BonusDAO.getBonusBySkin(con, skinId);
		} finally {
			closeConnection(con);
		}
	}

    public static void updateBonusWageringParam(long bonusUsersId, long wageringParam) throws SQLException {
        if (logger.isEnabledFor(Level.DEBUG)) {
            String ls = System.getProperty("line.separator");
            logger.log(Level.DEBUG,ls + "update bonus user id: " + bonusUsersId + ", set wagering param to " + wageringParam);
         }
        Connection con = getConnection();

        try {
        	con.setAutoCommit(false);

        	BonusDAO.updateBonusWagering(con, bonusUsersId, wageringParam);

        	// if wageringParam is 0 set bonus as Done
            if ( 0 == wageringParam ) {  // insert
                BonusDAO.doneBonusUsers(con, bonusUsersId,false, 0);
            }
            con.commit();
        } finally {
        	con.setAutoCommit(true);
            closeConnection(con);
        }
    }
}
