package il.co.etrader.backend.bl_managers;


import il.co.etrader.backend.bl_vos.MergeTransaction;
import il.co.etrader.backend.bl_vos.SalesDepositsDetails;
import il.co.etrader.backend.bl_vos.SalesDepositsTotals;
import il.co.etrader.backend.bl_vos.SalesTurnoversDetails;
import il.co.etrader.backend.bl_vos.TransactionsIssues;
import il.co.etrader.backend.dao_managers.TransactionsIssuesDAO;
import il.co.etrader.bl_managers.TransactionsIssuesManagerBase;

import java.sql.Connection;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;


public class TransactionsIssuesManager extends TransactionsIssuesManagerBase {

	/**
	 * Returns SalesDepositsContent list
	 * @param from date
	 * @param to date
	 * @param writer Id
	 * @throws SQLException
	 * @throws ParseException 
	 */
	public static ArrayList<SalesDepositsTotals> getSalesDeposits(Date from, Date to, long writerId, long currencyId, long skinId) throws SQLException, ParseException {
		Connection con = getSecondConnection();
		ArrayList<SalesDepositsTotals> list = null;

		try {
			list = TransactionsIssuesDAO.getSalesDeposits(con, from, to, writerId,currencyId,skinId);
		}
		finally {
			closeConnection(con);
		}
		return list;
	}

	/**
	 * Returns SalesDepositsContent list
	 * @param from date
	 * @param to date
	 * @param writer Id
	 * @throws SQLException
	 */
	public static ArrayList<SalesDepositsDetails> getSalesDepositsDetails(long userId, long transactioId, Date from, Date to, long writerId, long currencyId,
			long skinId, String dateFilterType, int qualifiedFilter, int firstDepositFilter) throws SQLException {
		Connection con = getSecondConnection();
		ArrayList<SalesDepositsDetails> list = null;

		try {
			list = TransactionsIssuesDAO.getSalesDepositsDetails(con, userId, transactioId, from, to, writerId,currencyId,
					skinId,dateFilterType,qualifiedFilter,firstDepositFilter);
		}
		finally {
			closeConnection(con);
		}
		return list;
	}

	public static ArrayList<SalesTurnoversDetails> getSaledDepositsDetailsOfRetention(Date from, Date to, long writerId, long currencyId, long skinId, long salesTypeDepId, long tranClassTypeId) throws SQLException{
		Connection conn = getSecondConnection();
		try{
			return TransactionsIssuesDAO.getSaledDepositsDetailsOfRetention(conn, from, to, writerId, currencyId, skinId, salesTypeDepId, tranClassTypeId);
		} finally {
			closeConnection(conn);
		}
	}
	
	/**
	 * Search matching transaction for user's issue action
	 * @param userId
	 * @throws SQLException
	 */

	public static long getTransactionForActionByUserId(long userId, long issueId) throws SQLException{
		Connection con = getConnection();
		try {
			return TransactionsIssuesDAO.getTransactionForActionByUserId(con, userId, issueId);
		} finally {
			closeConnection(con);
		}
	}

	/**
	 * Get sales deposit views
	 * @param period
	 * @param isEtrader
	 * @return
	 * @throws SQLException
	 */
	public static ArrayList<SalesDepositsTotals> getSalesWriterDepositsTv(int period, boolean isEtrader, boolean isAllSkins, boolean etraderOnly) throws SQLException {
		Connection con = getConnection();
		try {
			return TransactionsIssuesDAO.getSalesWriterDepositsTv(con, period, isEtrader, isAllSkins, etraderOnly);

		} finally {
			closeConnection(con);
		}
	}


	/**
	 * Check if transaction or issue action exists on TransactionIssue table
	 * @param tranId
	 * @param issueActionId
	 * @return
	 * @throws SQLException
	 */
	public static ArrayList<TransactionsIssues> isTranOnTiTable(long tranId,long issueActionId)throws SQLException {
		Connection con = getConnection();
		try {
			return TransactionsIssuesDAO.getIsTranOnTiTable(con,tranId,issueActionId);
		} finally {
			closeConnection(con);
		}
	}

	/**
	 * Get conversion deposit views
	 * @param period
	 * @param isEtrader
	 * @return
	 * @throws SQLException
	 */
	public static ArrayList<SalesDepositsTotals> getConversionWriterDepositsTv(boolean isEtrader,
			boolean isAllSkins, boolean isDaily, boolean fromConversion, long skinId) throws SQLException {
		Connection con = getConnection();
		try {
			return TransactionsIssuesDAO.getConversionWriterDepositsTv(con, isEtrader, isAllSkins, isDaily, fromConversion, skinId);

		} finally {
			closeConnection(con);
		}
	}

	/**
	 * add new sales deposits since last access to db to sales deposits list
	 * @param timeLastSalesTvDbCheck
	 * @throws SQLException
	 */
	public static void addNewDepositsForTvFromDb(Date timeLastSalesTvDbCheck) throws SQLException {
		Connection con = getConnection();
		try {
			TransactionsIssuesDAO.addNewDepositsForTvFromDb(con, timeLastSalesTvDbCheck);

		} finally {
			closeConnection(con);
		}
	}

	public static MergeTransaction getMergeTransactions(long trxId, boolean isFirstDeposit) throws SQLException {
		Connection con = getConnection();
		try {
			return TransactionsIssuesDAO.getMergeTransactions(con, trxId, isFirstDeposit);

		} finally {
			closeConnection(con);
		}
	}

	public static void mergeTransaction(long firstTxId, String transactionsStr, long writerId) throws SQLException {
		Connection con = getConnection();
		try {
			TransactionsIssuesDAO.mergeTransaction(con, firstTxId, transactionsStr, writerId);
		} finally {
			closeConnection(con);
		}
		
	}

}