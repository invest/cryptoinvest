package il.co.etrader.backend.bl_managers;

import il.co.etrader.backend.bl_vos.OptionPlusPrice;
import il.co.etrader.backend.bl_vos.OptionPlusPriceMatrixItem;
import il.co.etrader.backend.dao_managers.OptionPlusDAO;
import il.co.etrader.util.CommonUtil;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;

import org.apache.log4j.Logger;

import com.anyoption.common.managers.BaseBLManager;

/**
 * OptionPlusManager Manager class
 * @author Kobi
 *
 */
public class OptionPlusManager extends BaseBLManager {

    private static final Logger logger = Logger.getLogger(OptionPlusManager.class);

    public static ArrayList<OptionPlusPriceMatrixItem> buildOptionPlusPriceMatrix(long marketId) throws SQLException {
    	Connection con = null;
    	try {
    		con = getConnection();

    		ArrayList<OptionPlusPrice> list = OptionPlusDAO.getOptionPlusPrices(con, marketId);
    		ArrayList<OptionPlusPriceMatrixItem> matrixList = new ArrayList<OptionPlusPriceMatrixItem>();
    		ArrayList<OptionPlusPriceMatrixItem> firstRowItems = new ArrayList<OptionPlusPriceMatrixItem>();
			OptionPlusPriceMatrixItem row = new OptionPlusPriceMatrixItem();
			ArrayList<OptionPlusPriceMatrixItem> items = new ArrayList<OptionPlusPriceMatrixItem>();
    		boolean isFirstRow = true;
    		long lastPromileId = 0;

    		for (int i = 0 ; i < list.size() ; i++) {
    			OptionPlusPrice p = list.get(i);
    			if (lastPromileId == 0 || p.getPromileRangeId() != lastPromileId) {
    				lastPromileId = p.getPromileRangeId();
    				row.setItemType(OptionPlusPriceMatrixItem.TYPE_ROW_VALUE);
    				row.setValue(p.getPromileRangeStr());
    				items.add(new OptionPlusPriceMatrixItem(OptionPlusPriceMatrixItem.TYPE_MATRIX_VALUE, p.getPrice(), p.getId()));
    				if (isFirstRow) {
    					OptionPlusPriceMatrixItem minCol = new OptionPlusPriceMatrixItem();
    					minCol.setItemType(OptionPlusPriceMatrixItem.TYPE_COLUMN_VALUE);
    					minCol.setValue(p.getMinutesRangeStr());
    					firstRowItems.add(minCol);
    				}
    			} else {
    				items.add(new OptionPlusPriceMatrixItem(OptionPlusPriceMatrixItem.TYPE_MATRIX_VALUE, p.getPrice(), p.getId()));
    				if (isFirstRow) {
    					OptionPlusPriceMatrixItem minCol = new OptionPlusPriceMatrixItem();
    					minCol.setItemType(OptionPlusPriceMatrixItem.TYPE_COLUMN_VALUE);
    					minCol.setValue(p.getMinutesRangeStr());
    					firstRowItems.add(minCol);
    				}
    			}
    			if ((list.size() == i+1) ||
    					(null != list.get(i+1) && lastPromileId != list.get(i+1).getPromileRangeId())) {  // set the row into the matrix list
    																									  //when it's different promile / last row
    				if (isFirstRow) {  // handle first row
    					isFirstRow = false;
    					OptionPlusPriceMatrixItem firstRow = new OptionPlusPriceMatrixItem();
    					firstRow.setItemType(OptionPlusPriceMatrixItem.TYPE_ROW_HEADER);
    					firstRow.setValue(CommonUtil.getMessage("optionplus.prices.min", null));
    					firstRow.setItems(firstRowItems);

    					OptionPlusPriceMatrixItem secondRow = new OptionPlusPriceMatrixItem();
    					secondRow.setItemType(OptionPlusPriceMatrixItem.TYPE_COLUMN_HEADER);
    					secondRow.setValue(CommonUtil.getMessage("optionplus.prices.promile", null));
    					ArrayList<OptionPlusPriceMatrixItem> secondRowItems = new ArrayList<OptionPlusPriceMatrixItem>();
    					for (int z = 0; z < firstRowItems.size(); z++) {
    						secondRowItems.add(new OptionPlusPriceMatrixItem(OptionPlusPriceMatrixItem.TYPE_EMPLY_VALUE, 0, 0));
    					}
    					secondRow.setItems(secondRowItems);

    					matrixList.add(firstRow);
    					matrixList.add(secondRow);
    				}
    				row.setItems(items);
    				matrixList.add(row);
    				row = new OptionPlusPriceMatrixItem();
    				items = new ArrayList<OptionPlusPriceMatrixItem>();
    			}
    		}
    		return matrixList;
    	} finally {
    		closeConnection(con);
    	}
    }


    public static void updatePrice(long id, double price) throws SQLException {
    	Connection con = null;
    	try {
    		con = getConnection();
    		OptionPlusDAO.updatePrice(con, id, price);
    	} finally {
    		closeConnection(con);
    	}
    }

}
