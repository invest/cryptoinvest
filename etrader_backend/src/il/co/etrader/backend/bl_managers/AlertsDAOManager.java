package il.co.etrader.backend.bl_managers;

import il.co.etrader.backend.dao_managers.UsersDAO;
import il.co.etrader.bl_vos.AlertsBase;
import il.co.etrader.bl_vos.UserBase;
import il.co.etrader.dao_managers.AlertsDAOBase;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;

import org.apache.log4j.Logger;

import com.anyoption.common.managers.BaseBLManager;

public class AlertsDAOManager extends BaseBLManager {
    
    AlertsDAOBase adb = new AlertsDAOBase();

    private static final Logger logger = Logger.getLogger(UsersManager.class);
    
    public ArrayList<AlertsBase> getAlertsByUser(Long userId) throws SQLException{
        
        ArrayList<AlertsBase> al;
        Connection con = getConnection();
    
        try{
            
        al = adb.getAlertsByUser(con, userId);
        
        } finally {
            
            closeConnection(con);
        }
        return al;
        
    }
    
    
    public boolean insertAlertsBase(ArrayList<AlertsBase> bases) throws SQLException{
        
        Connection con = getConnection();
        boolean result = false;
        
        try{
            
            result = adb.insertAlertsBase(con, bases);
            
        }finally{
            
            closeConnection(con);
        }
        
        return result;
    }
    
   public boolean deleteAlertsBase(ArrayList<AlertsBase> bases) throws SQLException{
        
        Connection con = getConnection();
        boolean result = false;
        
        try{
            
            result = adb.deleteAlertsBase(con, bases);
            
        }finally{
            
            closeConnection(con);
        }
        
        return result;
    }
   
   public boolean deleteAlertsBaseAll(Long userId) throws SQLException{
       
       Connection con = getConnection();
       boolean result = false;
       
       try{
           
           result = adb.deleteAlertsBaseAll(con, userId);
           
       }finally{
           
           closeConnection(con);
       }
       
       return result;
   }
    
   public boolean getExistsUser(String name, Long id){
       Connection con = null;
       UserBase vo = new UserBase();
       try {
           con = getConnection();
           return UsersDAO.getByUserNameOrId(con, name, id, null, vo, null);    
       } catch (Exception e) {
           logger.error("Error get risk alert list!" + e.getMessage() + e.getStackTrace());
       } finally {
           closeConnection(con);
       }
   return false;
   }
   
}
