package il.co.etrader.backend.bl_managers;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Font.FontFamily;
import com.itextpdf.text.Image;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.ColumnText;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfGState;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfStamper;

public class TransparentWatermark {
//	public static final String SRC = "C:/docs/POA_153439_59477.pdf";
//	public static final String DEST = "C:/docs/POA_153439_59477_WATER.pdf";
//	public static final String IMG = CommonUtil.getProperty("host.url.https") +  "/images/ouroboros_logo_Watermark.png";
	
	public TransparentWatermark(String src, String dest, String waterMarkImg) {
		super();
		this.srcFile = src;
		this.destNewFile = dest;
		this.waterMarkImg = waterMarkImg;
	}

	private String srcFile;
	private String destNewFile;
	private String waterMarkImg;
	
	public File putWaterMArkImg() throws IOException, DocumentException {
		File file = new File(destNewFile);
		file.getParentFile().mkdirs();
		//new TransparentWatermark().manipulatePdf(src, dest);
		manipulatePdf(srcFile, destNewFile);
		return file;
	}

	public void manipulatePdf(String src, String dest) throws IOException, DocumentException {
		PdfReader reader = new PdfReader(src);
		int n = reader.getNumberOfPages();
		PdfStamper stamper = new PdfStamper(reader, new FileOutputStream(dest));
		// text watermark
		Font f = new Font(FontFamily.HELVETICA, 30);
		Phrase p = new Phrase("", f);
		// image watermark
		Image img = Image.getInstance(waterMarkImg);
		float w = img.getScaledWidth();
		float h = img.getScaledHeight();
		// transparency
		PdfGState gs1 = new PdfGState();
		gs1.setFillOpacity(0.2f);
		// properties
		PdfContentByte over;
		Rectangle pagesize;
		float x, y;
		// loop over every page
		for (int i = 1; i <= n; i++) {
			pagesize = reader.getPageSizeWithRotation(i);
			x = (pagesize.getLeft() + pagesize.getRight()) / 2;
			y = (pagesize.getTop() + pagesize.getBottom()) / 2;
			over = stamper.getOverContent(i);
			over.saveState();
			over.setGState(gs1);
			over.addImage(img, w, 0, 0, h, x - (w / 2), y - (h / 2));
			if (i % 2 == 1)
				ColumnText.showTextAligned(over, Element.ALIGN_CENTER, p, x, y, 0);
			else
				over.addImage(img, w, 0, 0, h, x - (w / 2), y - (h / 2));
			over.restoreState();
		}
		stamper.close();
		reader.close();
	}

}
