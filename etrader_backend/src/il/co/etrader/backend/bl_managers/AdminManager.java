package il.co.etrader.backend.bl_managers;

import java.io.BufferedInputStream;
import java.io.FileWriter;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import com.anyoption.common.beans.Market;
import com.anyoption.common.jms.BackendLevelsCache;

import il.co.etrader.backend.bl_vos.NewAccounts;
import il.co.etrader.backend.dao_managers.ClicksDAO;
import il.co.etrader.backend.dao_managers.MarketsDAO;
import il.co.etrader.backend.dao_managers.NewAccountsDAO;
import il.co.etrader.backend.dao_managers.OpportunitiesDAO;
import il.co.etrader.backend.mbeans.ChangeWriterPassForm;
import il.co.etrader.backend.util.Constants;
import il.co.etrader.backend.util.Utils;
import il.co.etrader.bl_managers.AdminManagerBase;
import il.co.etrader.bl_managers.MarketsManager;
import il.co.etrader.bl_vos.CcBlackList;
import il.co.etrader.bl_vos.Limit;
import il.co.etrader.bl_vos.Template;
import il.co.etrader.bl_vos.Writer;
import il.co.etrader.dao_managers.CcBlackListDAO;
import il.co.etrader.dao_managers.LimitsDAO;
import il.co.etrader.dao_managers.SkinCurrenciesDAOBase;
import il.co.etrader.dao_managers.TemplatesDAO;
import il.co.etrader.dao_managers.WritersDAO;
import il.co.etrader.util.CommonUtil;

public class AdminManager extends AdminManagerBase {
	private static final Logger logger = Logger.getLogger(AdminManager.class);

	public AdminManager() throws Exception {

	}

	public static ArrayList searchTemplates() throws SQLException {
		Connection con = getConnection();
		try {
			return TemplatesDAO.getAll(con);
		} finally {
			closeConnection(con);
		}
	}

	/**
	 * Get all templates by user skin id
	 * @param skinId
	 * 		user skin id
	 * @return
	 * @throws SQLException
	 */
	public static ArrayList<Template> searchTemplatesByUserSkin(long skinId) throws SQLException {
		Connection con = getConnection();
		try {
			return TemplatesDAO.getAllBySkinId(con, skinId);
		} finally {
			closeConnection(con);
		}
	}

	/**
	 * Get all templates by writer skins
	 * @param skins
	 * 		skins list of the writer
	 * @return
	 * @throws SQLException
	 */
	public static ArrayList<Template> searchTemplatesByWriterSkins(String skins) throws SQLException {
		Connection con = getConnection();
		try {
			return TemplatesDAO.getAllBySkins(con, skins);
		} finally {
			closeConnection(con);
		}
	}

	public static ArrayList searchWriters() throws SQLException {
		Connection con = getConnection();
		try {
			return WritersDAO.getAll(con);
		} finally {
			closeConnection(con);
		}
	}

	public static ArrayList searchLimits(int skinId, long currencyId) throws SQLException {
		Connection con = getConnection();
		try {
			return LimitsDAO.getBySkinCurrency(con, skinId, currencyId, Utils.getWriter().getSkinsToString());
		} finally {
			closeConnection(con);
		}
	}

	public static ArrayList searchCcBlackList(Date from, Date to) throws SQLException {
		Connection con = getConnection();
		try {
			return CcBlackListDAO.search(con,from,to);
		} finally {
			closeConnection(con);
		}
	}

	public static ArrayList searchMarkets(String name, long groupId) throws SQLException {
		Connection con = getConnection();
		try {
			return MarketsDAO.searchMarkets(con, name, groupId);
		} finally {
			closeConnection(con);
		}
	}

	public static ArrayList searchClicks(Date from, Date to, long campaignId) throws SQLException {
		Connection con = getConnection();
		try {
			return ClicksDAO.search(con,from,to, campaignId);
		} finally {
			closeConnection(con);
		}
	}

	public static ArrayList<NewAccounts> searchNewAccounts(Date from, Date to, long campaignId, String skins,int yesNo, long countryId, long skinBusinessCaseId,
            long writerId, Date arabicStartDate, int verifiedUsers, long currencyId, String amountAbove, String amountBelow) throws SQLException{
		Connection con = getSecondConnection();
		try{
			return NewAccountsDAO.getNewAccounts(con, from, to, campaignId, skins,yesNo, countryId, skinBusinessCaseId, writerId, arabicStartDate,
                    verifiedUsers, currencyId, amountAbove, amountBelow);
		} finally {
			closeConnection(con);
		}
	}



	public static boolean updateInsertCcBlackList(CcBlackList f) throws SQLException {
		if (logger.isEnabledFor(Level.DEBUG)) {
  			String ls = System.getProperty("line.separator");
  			logger.log(Level.DEBUG,"Insert/update Credit Card black list: "+ls+f.toString());
  		 }
		FacesContext context = FacesContext.getCurrentInstance();
        
		Connection con = getConnection();
		try {
			if (f.getId() == 0) {
				// insert
				CcBlackListDAO.insert(con,f);
				FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_INFO,CommonUtil.getMessage("ccblacklist.insert.success",null, Utils.getWriterLocale(context)),null);
				context.addMessage(null,fm);
                
			} else {
				// update
				CcBlackListDAO.update(con,f);
				FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_INFO,CommonUtil.getMessage("ccblacklist.update.success",null, Utils.getWriterLocale(context)),null);
				context.addMessage(null,fm);
                
			}
			if (logger.isEnabledFor(Level.DEBUG)) {
	  			String ls = System.getProperty("line.separator");
	  			logger.log(Level.DEBUG,"Insert/update Credit Card black finished ok. "+ls);
	  		 }
		} finally {
			closeConnection(con);
		}
		return true;
	}

	public static boolean updateInsertMarket(Market f, Long oddsTypeId) throws SQLException {
		if (logger.isEnabledFor(Level.DEBUG)) {
  			String ls = System.getProperty("line.separator");
  			logger.log(Level.DEBUG,"Insert/update Market: "+ls+f.toString());
  		 }
		FacesContext context = FacesContext.getCurrentInstance();
        
		BackendLevelsCache levelsCache = (BackendLevelsCache) context.getExternalContext().getApplicationMap().get("levelsCache");
	    if (levelsCache.notifyForMarketCfgChange(f.getFeedName(),
						     f.getUpdatesPause(), f.getSignificantPercentage(), f.getRealUpdatesPause(), f.getRealSignificantPercentage(),
						     f.getRandomCeiling(), f.getRandomFloor(), f.getFirstShiftParameter(),
						     f.getPercentageOfAverageInvestments(), f.getFixedAmountForShifting(), f.getTypeOfShifting().intValue(),
						     f.getDisableAfterPeriod(), f.getQuoteParams())) {

			Connection con = getConnection();
			try {
				con.setAutoCommit(false);

				Market market2 = MarketsDAO.get(con, f.getId(), true);
				f.setDisplayName(f.getDisplayNameKey());
				if (f.getId() == 0) {
					// insert
					MarketsDAO.insert(con, f);
					FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_INFO,CommonUtil.getMessage("market.insert.success",null, Utils.getWriterLocale(context)),null);
					context.addMessage(null,fm);
                    
				} else {
					// update
					MarketsDAO.update(con, f);
					MarketsManager.changeFXALParameter(con, f.getQuoteParams(), f.getId());

					if(market2.getMax_exposure() != f.getMax_exposure()){
						OpportunitiesDAO.updateOpportunitiesMaxExposure(con, f.getId(), f.getMax_exposure());
					}
					
					if (market2.getWorstCaseReturn() != f.getWorstCaseReturn()) {
						OpportunitiesDAO.updateOpportunitiesWorstCaseReturn(con, f.getId(), f.getWorstCaseReturn());
					}
					
					if (oddsTypeId!=null && !oddsTypeId.equals("")) {
						
						//Check if market in Binary (opportunity_type_id=1) and missing oddsPairGroup about Return Selector for new oddsTypeId
						String oddsPairGroup = OpportunitiesDAO.getOpportunityPairsGroup(con,oddsTypeId);
						if (MarketsDAO.isExistMarketInBinary(con, f) && oddsPairGroup.contains("null") ){
							
							logger.debug("not update for market "+ f.getId()+" and OddsType "+ oddsTypeId +"! Missing OddsPairGroup for Return Selector!");
							try {
								con.rollback();
							} catch (SQLException ie) {
								logger.log(Level.ERROR,"Can't rollback.",ie);
							}
							
							FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_INFO,CommonUtil.getMessage("market.update.faild.oddsgroup",null, Utils.getWriterLocale(context)),null);
							context.addMessage(null,fm);
							return false;
						}
						
						
						OpportunitiesDAO.updateOppTemplateAllMarketsOdds(con, f.getId(), oddsTypeId, oddsPairGroup );
						AdminManagerBase.addToLog(AdminManager.getWriterId(), "markets", f.getId(), Constants.LOG_COMMANDS_CHANGE_OPP_ODS, "Odds Type ID: "+oddsTypeId);
					}
					FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_INFO,CommonUtil.getMessage("market.update.success",null, Utils.getWriterLocale(context)),null);
					context.addMessage(null,fm);
                    
				}



				HashMap hm = new HashMap();
				hm.put("groupName",null);
				hm.put("exchangeName",null);
				hm.put("investmentLimitsGroupName",null);
				if (null == market2) {  // in a case market2 doesn't exsists
					logger.debug("not insert to Log , new market creation situations! ");
				} else {
					AdminManager.addToLog(AdminManager.getWriterId(), "markets", f.getId(), Constants.LOG_COMMANDS_UPDATE_MARKET, CommonUtil.compareObjects(market2, f, hm));
				}

	            logger.debug("levels cache notified successfully on markets change!");

				if (logger.isEnabledFor(Level.DEBUG)) {
		  			String ls = System.getProperty("line.separator");
		  			logger.log(Level.DEBUG,"Insert/update Market finished ok. "+ls);
		  		 }

				con.commit();
			} catch (SQLException e) {
				logger.error("Exception on Insert/update Market!!!",e);
				try {
					con.rollback();
				} catch (SQLException ie) {
					logger.log(Level.ERROR,"Can't rollback.",ie);
				}
				throw e;

			} finally {
				con.setAutoCommit(true);
				closeConnection(con);
			}
			return true;
	    }
	    FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_INFO,CommonUtil.getMessage("market.update.faild",null, Utils.getWriterLocale(context)),null);
		context.addMessage(null,fm);
        
	    return false;
	}

	/**
	 * Update / Insert Limit
	 * @param f	 - Limit instance
	 * @return
	 * @throws SQLException
	 */
	public static boolean updateInsertLimit(Limit f) throws SQLException {

		if (logger.isEnabledFor(Level.DEBUG)) {
  			String ls = System.getProperty("line.separator");
  			logger.log(Level.DEBUG,"Insert/update Limit: "+ls+f.toString());
  		 }

		FacesContext context = FacesContext.getCurrentInstance();
        
		Connection con = getConnection();

		try {

			con.setAutoCommit(false);

			if (f.getId() == 0) {
				// insert
				LimitsDAO.insert(con,f);
				SkinCurrenciesDAOBase.insert(con, f);
				FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_INFO,CommonUtil.getMessage("limits.insert.success",null, Utils.getWriterLocale(context)),null);
				context.addMessage(null,fm);
                
			} else {
				// update
				LimitsDAO.update(con,f);
				SkinCurrenciesDAOBase.updateLimit(con, f);
				FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_INFO,CommonUtil.getMessage("limits.update.success",null, Utils.getWriterLocale(context)),null);
				context.addMessage(null,fm);
                
			}

			if (logger.isEnabledFor(Level.DEBUG)) {
	  			logger.log(Level.DEBUG,"Insert/update Limit finished ok.");
	  		}

			con.commit();

		} catch(Exception e) {
			logger.error("Exception in insert/update Limit! ", e);
			try {
				con.rollback();
			} catch (Throwable it) {
				logger.error("Can't rollback.", it);
			}
		} finally {
			con.setAutoCommit(true);
			closeConnection(con);
		}
		return true;
	}
	public static Template getTemplateById(long id) throws SQLException{
		Connection con = getConnection();
		Template template = TemplatesDAO.get(con, id);
		return template;
	}
	public static boolean updateInsertTemplate(Template f) throws SQLException {

		if (logger.isEnabledFor(Level.DEBUG)) {
			String ls = System.getProperty("line.separator");
			logger.log(Level.DEBUG,"Insert/update Template: " + ls + f.toString());
		}

		FacesContext context = FacesContext.getCurrentInstance();

		if (f.getId() == 0 && f.getFile() == null) {
			// insert
			FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR,CommonUtil.getMessage("error.mandatory",null, Utils.getWriterLocale(context)),null);
			context.addMessage("templatesForm:file",fm);
			return false;
		}

		Connection con = getConnection();
		try {

			if (f.getFile() != null) {

				String name = f.getFile().getName();
				while(name.indexOf("\\") != -1)
					name = name.substring(name.indexOf("\\") + 1);

				f.setFileName(name);

				InputStream in = new BufferedInputStream(f.getFile().getInputStream());
				FileWriter w = new FileWriter(CommonUtil.getProperty(Constants.TEMPLATES_PATH) + name);

				try {
					int i;
					while((i = in.read()) != -1) {
						w.write(i);
					}
				} finally {
					in.close();
					w.close();
				}
			}

			if (f.getId() == 0) {
				// insert
				TemplatesDAO.insert(con,f);
			} else {
				// update
				TemplatesDAO.update(con,f);
			}

			if (f.getId() == 0) {
				// insert
				FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_INFO,CommonUtil.getMessage("templates.insert.success",null, Utils.getWriterLocale(context)),null);
				context.addMessage(null,fm);
                
			} else {
				// update
				FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_INFO,CommonUtil.getMessage("templates.update.success",null, Utils.getWriterLocale(context)),null);
				context.addMessage(null,fm);
                
			}
		} catch (Exception e) {
			logger.log(Level.ERROR,"Error on insert/update template!",e);
			FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR,CommonUtil.getMessage("templates.error",null, Utils.getWriterLocale(context)),null);
			context.addMessage(null,fm);
            
			return false;
		} finally {
			closeConnection(con);
		}
		if (logger.isEnabledFor(Level.DEBUG)) {
			String ls = System.getProperty("line.separator");
			logger.log(Level.DEBUG,"Insert/update Template finished ok. " + ls);
		}
		return true;
	}

	public static ArrayList getGroupsSI() throws SQLException {
		Connection con = getConnection();
		try {
			return MarketsDAO.getGroupsSelectItems(con);
		} finally {
			closeConnection(con);
		}
	}


	public static ArrayList<SelectItem> getLimits() throws SQLException {
		Connection con = getConnection();
		ArrayList<SelectItem> limits = new ArrayList<SelectItem>();
		try {
			ArrayList list = LimitsDAO.getAll(con);
			for (int i = 0; i < list.size(); i++) {
				Limit c = (Limit) list.get(i);
				limits.add(new SelectItem(String.valueOf(c.getId()),c.getName()));
			}
		} finally {
			closeConnection(con);
		}
		return limits;
	}

	/**
	 * Get limits by writer skins
	 * @return
	 * @throws SQLException
	 */
	public static ArrayList<SelectItem> getLimitsBySkins() throws SQLException {
		Connection con = getConnection();
		ArrayList<SelectItem> limits = new ArrayList<SelectItem>();
		try {
			ArrayList list = LimitsDAO.getByWriterSkins(con,Utils.getWriter().getSkinsToString());
			for (int i = 0; i < list.size(); i++) {
				Limit c = (Limit) list.get(i);
				limits.add(new SelectItem(String.valueOf(c.getId()),c.getName()));
			}
			Collections.sort(limits, new CommonUtil.selectItemComparator());
		} finally {
			closeConnection(con);
		}
		return limits;
	}

	public static long getWriterId() throws SQLException {
		FacesContext context = FacesContext.getCurrentInstance();

		Connection con = getConnection();
		try {

			// Find the writer ID
			String remoteUser = context.getExternalContext().getRemoteUser();
			return WritersDAO.getByUserName(con,remoteUser).getId();

		} finally {
			closeConnection(con);
		}

	}

	public static Writer getWriterByUserName(String remoteUser) throws SQLException {

		Connection con = getConnection();
		try {
			return WritersDAO.getByUserName(con, remoteUser);

		} finally {
			closeConnection(con);
		}
	}

    public static boolean changeWriterPass(ChangeWriterPassForm passForm, Writer writer) throws SQLException {

		if (logger.isEnabledFor(Level.DEBUG)) {
			String ls = System.getProperty("line.separator");
			logger.log(Level.DEBUG, "Changing writer password.  " + ls + "Writer ID:" + writer.getId() +
																	ls + "new password:" + passForm.getNewPassword() + ls);
		}
   	FacesContext context=FacesContext.getCurrentInstance();

		if (!(writer.getPassword().toUpperCase()).equals((passForm.getOldPassword()).toUpperCase())) {

			FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR, CommonUtil.getMessage("error.changePass.wrongPass", null, Utils.getWriterLocale(context)), null);
			context.addMessage("changeWriterPassForm:oldpassword", fm);
			return false;
		}
   	
		if (!(passForm.getNewPassword().toUpperCase()).equals((passForm.getVerifyPass()).toUpperCase())) {
	       	FacesMessage fm=new FacesMessage(FacesMessage.SEVERITY_ERROR, CommonUtil.getMessage("error.changePass.wrongVerifyPass",null, Utils.getWriterLocale(context)),null);
	       	context.addMessage("changeWriterPassForm:password", fm);
	       	return false;
		}
		
		if ((passForm.getNewPassword().toUpperCase()).equals((passForm.getOldPassword()).toUpperCase())) {
	       	FacesMessage fm=new FacesMessage(FacesMessage.SEVERITY_ERROR, CommonUtil.getMessage("error.changePass.new.equals.new.pass",null, Utils.getWriterLocale(context)),null);
	       	context.addMessage("changeWriterPassForm:password", fm);
	       	return false;
		}

   	Connection con=getConnection();

       try {

    	   writer.setPassword(passForm.getNewPassword());
    	   WritersDAO.updatePassword(con, writer);
           if (logger.isEnabledFor(Level.DEBUG)) {
    			logger.log(Level.DEBUG," password updated successfully!");
    		}

       } finally {
           con.setAutoCommit(true);
           closeConnection(con);
       }
       FacesMessage fm=new FacesMessage(FacesMessage.SEVERITY_INFO,CommonUtil.getMessage("password.changed",null, Utils.getWriterLocale(context)),null);
       context.addMessage(null, fm);
       
       return true;

	}

    /**
     * return the display name by market id
     * for one touch opportunity messages creation
     * @param marketId
     * @return
     * @throws SQLException
     */
    public static String getDisplayNameById(long marketId) throws SQLException {

		Connection con = getConnection();
		try {
			return MarketsDAO.getDisplayNameById(con, marketId);

		} finally {
			closeConnection(con);
		}
	}

}