package il.co.etrader.backend.bl_managers;

import il.co.etrader.backend.bl_vos.RewardUserTaskExpPoint;
import il.co.etrader.backend.bl_vos.RewardUserTierDistribution;
import il.co.etrader.backend.dao_managers.RewardsDAO;
import il.co.etrader.util.DataPage;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;

import javax.faces.model.SelectItem;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.RewardTier;
import com.anyoption.common.beans.RewardUser;
import com.anyoption.common.beans.RewardUserBenefit;
import com.anyoption.common.beans.RewardUserHistory;
import com.anyoption.common.beans.RewardUserPlatform;
import com.anyoption.common.managers.BaseBLManager;

/**
 * @author EranL
 *
 */
public class RewardsManager extends BaseBLManager {
	private static final Logger logger = Logger.getLogger(RewardsManager.class);
		
	/**
	 * Get reward tiers 
	 * @return
	 * @throws SQLException
	 */
	public static ArrayList<RewardTier> getRewardTiers() throws SQLException {
		Connection con = null;
		try {
			con = getSecondConnection();
			return RewardsDAO.getRewardTiers(con);
		} finally {
			closeConnection(con);
		}		
	}
	
	/**
	 * Get rewards user
	 * @param userId
	 * @return
	 * @throws SQLException
	 */
	public static RewardUser getRewardsUser(long userId) throws SQLException {
		Connection con = null;
		try {
			con = getSecondConnection();
			return RewardsDAO.getRewardsUser(con, userId);
		} finally {
			closeConnection(con);
		}
	}
	
	/**
	 * Get reward user task experience points distribution
	 * @param userId
	 * @return
	 * @throws SQLException
	 */
	public static ArrayList<RewardUserTaskExpPoint> getRewardUserTaskExpPoints(long userId) throws SQLException {
		Connection con = null;
		try {
			con = getSecondConnection();
			return RewardsDAO.getRewardUserTaskExpPoints(con, userId);
		} finally {
			closeConnection(con);
		}
	}
	
	/**
	 * Get reward user platforms (task experience points distribution) 
	 * @param userId
	 * @return
	 * @throws SQLException
	 */
	public static ArrayList<RewardUserPlatform> getRewardUserPlatforms(long userId) throws SQLException {
		Connection con = null;
		try {
			con = getSecondConnection();
			return RewardsDAO.getRewardUserPlatforms(con, userId);
		} finally {
			closeConnection(con);
		}
	}	
		
	/**
	 * Get Rewards User Benefits
	 * @param userId
	 * @return
	 * @throws SQLException
	 */
	public static ArrayList<RewardUserBenefit> getRewardsUserBenefits(long userId) throws SQLException {
		Connection con = null;
		try {
			con = getSecondConnection();
			return RewardsDAO.getRewardsUserBenefits(con, userId);
		} finally {
			closeConnection(con);
		}
	}

	/**
	 * Get Reward User History
	 * @param userId
	 * @param startRow
	 * @param modelSize
	 * @return
	 * @throws SQLException
	 */
	public static DataPage<RewardUserHistory> getRewardUserHistory(long userId, int startRow, int pageSize, Date from, Date to, long tierId, long typeId, String actionId) throws SQLException {
		DataPage<RewardUserHistory> dataPage = null;
        ArrayList<RewardUserHistory> list = null;
        Connection con = null;
        try {
        	con = getSecondConnection();
        	list = RewardsDAO.getRewardUserHistory(con, startRow, pageSize, userId, from, to, tierId, typeId, actionId);        	
        	int modelSize = RewardsDAO.getRewardUserHistoryCount(con, startRow, pageSize, userId, from, to, tierId, typeId, actionId);   
            dataPage = new DataPage<RewardUserHistory>(modelSize, startRow, list);
        } finally {
            closeConnection(con);
        }
        return dataPage;
	}
	
	/**
	 * Get Rewards Actions (Benefit or Reward)
	 * @return
	 * @throws SQLException
	 */
	public static ArrayList<SelectItem> getRewardActions() throws SQLException {
		Connection con = null;
		try {
			con = getSecondConnection();
			return RewardsDAO.getRewardActions(con);
		} finally {
			closeConnection(con);
		}
	}
	
	/**
	 * Update rewards tier
	 * @param con
	 * @param rewardTiers
	 * @throws SQLException
	 */
	public static void updateTiers(ArrayList<RewardTier> rewardTiers, long writerId) throws SQLException {
		Connection con = null;
		try {
			con = getConnection();
			RewardsDAO.updateTiers(con, rewardTiers, writerId);
		} finally {
			closeConnection(con);
		}
	}
	
	/**
	 * Get Rewards Users Distribution By Tier
	 * @return
	 * @throws SQLException
	 */
	public static ArrayList<RewardUserTierDistribution> getRewardsUsersDistributionByTier() throws SQLException {
		Connection con = null;
		try {
			con = getConnection();
			return RewardsDAO.getRewardsUsersDistributionByTier(con);
		} finally {
			closeConnection(con);
		}
	}
	

	/**
	 * Get estimated rewards users 
	 * @param rewardTier
	 * @return
	 * @throws SQLException
	 */
	public static ArrayList<RewardUserTierDistribution> getEstimateRewardsUsersDistributionByTier(ArrayList<RewardTier> rewardTier) throws SQLException {
		Connection con = null;
		try {
			con = getConnection(); 
			return RewardsDAO.getEstimateRewardsUsersDistributionByTier(con, rewardTier);
		} finally {
			closeConnection(con);
		}
	}
	
	
	/**
	 * Count estimated rewards users that we change after tier change\
	 * @return
	 * @throws SQLException
	 */
	public static int countEstimatedRewardsUsersChanged(ArrayList<RewardTier> rewardTier) throws SQLException {
		Connection con = null;
		try {
			con = getConnection(); 
			return RewardsDAO.countEstimatedRewardsUsersChanged(con, rewardTier);
		} finally {
			closeConnection(con);
		}
	}
	
	/**
	 * Check if the document previously approved
	 * @param userId
	 * @param fileId
	 * @return true if the document previously approved
	 * @throws SQLException
	 */
	public static boolean isDocPreviouslyApproved(long userId, long fileId) throws SQLException {
		Connection connection = getConnection();
		try {
			return RewardsDAO.isDocPreviouslyApproved(connection, userId, fileId);
		} finally {
			closeConnection(connection);
		}
	}
}