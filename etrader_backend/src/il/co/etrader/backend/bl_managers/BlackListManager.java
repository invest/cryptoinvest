package il.co.etrader.backend.bl_managers;

import il.co.etrader.backend.bl_vos.BlackList;
import il.co.etrader.backend.dao_managers.BlackListDAO;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;

import org.apache.log4j.Logger;

import com.anyoption.common.managers.BaseBLManager;


public class BlackListManager extends BaseBLManager {

    private static final Logger logger = Logger.getLogger(BlackListManager.class);


	public static ArrayList<BlackList> getBlackList(String userIdStr,
													String userName,
													String userFirstName,
													String userLastName,
													String phone,
													String email,
													String password,
													String ip,
													long cityId,
													long publisherId,
													long actionTypeId) throws SQLException {
		Connection con = getConnection();
		try {
			return BlackListDAO.getBlackList(con, userIdStr, userName, userFirstName, userLastName, phone, email, password, ip, cityId, publisherId, actionTypeId);
		} finally {
			closeConnection(con);
		}
	}

}
