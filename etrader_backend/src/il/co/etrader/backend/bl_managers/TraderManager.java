package il.co.etrader.backend.bl_managers;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashSet;
import java.util.List;

import javax.faces.model.SelectItem;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import com.anyoption.common.beans.Investment;
import com.anyoption.common.beans.Market;
import com.anyoption.common.beans.Opportunity;
import com.anyoption.common.daos.GeneralDAO;
import com.anyoption.common.enums.SkinGroup;
import com.anyoption.common.managers.BaseBLManager;

import il.co.etrader.backend.bl_vos.WriterWrapper;
import il.co.etrader.backend.dao_managers.InvestmentsDAO;
import il.co.etrader.backend.dao_managers.MarketsDAO;
import il.co.etrader.backend.dao_managers.OpportunitiesDAO;
import il.co.etrader.backend.util.Constants;
import il.co.etrader.backend.util.Utils;
import il.co.etrader.bl_managers.InvestmentsManagerBase;
import il.co.etrader.bl_vos.ServiceConfig;
import il.co.etrader.dao_managers.BonusDAOBase;
import il.co.etrader.dao_managers.OpportunitiesDAOBase;
import il.co.etrader.dao_managers.ServiceConfigDAO;
import il.co.etrader.dao_managers.UsersDAOBase;
import il.co.etrader.util.ConstantsBase;


/**
 * trader manager.
 *
 * @author Shachar
 */
public class TraderManager extends BaseBLManager {

	private static final Logger logger = Logger.getLogger(TraderManager.class);
	private static final List<Long> resettleOpportunities = Arrays.asList(	Opportunity.TYPE_REGULAR, Opportunity.TYPE_OPTION_PLUS,
																			Opportunity.TYPE_DYNAMICS, Opportunity.TYPE_BINARY_0_100_ABOVE,
																			Opportunity.TYPE_BINARY_0_100_BELOW);

	/**
	 * insert shift parameter to DB opp table and shifting table
	 * @param opId
	 * @param spreadsChanged 
	 * @param shift
	 * @param time
	 * @param writerId
	 * @throws SQLException
	 */
	public static void insertShift(long opId, BigDecimal shift, SkinGroup sg, Date time, long writerId) throws SQLException {
		Connection con = getConnection();
		try {
				con.setAutoCommit(false);
				OpportunitiesDAO.updateOpportunityShiftParameter(con, opId, sg, shift.doubleValue(), writerId);
				OpportunitiesDAO.insertShifting(con, opId, shift.abs(), shift.doubleValue() > 0, writerId, sg);
				addToLog(con, writerId, "opportunities", opId, ConstantsBase.LOG_COMMANDS_UPDATE_SHIFT_PERC, shift.toString());
				con.commit();

		} catch (SQLException e) {
			try {
				con.rollback();
			} catch (SQLException ie) {
				logger.log(Level.ERROR, "Can't rollback.", ie);
			}
			throw e;
		} finally {
			con.setAutoCommit(true);
			closeConnection(con);
		}
	}

	/**
	 * add the command to log table
	 * @param writerId
	 * @param table
	 * @param key
	 * @param command
	 * @param desc
	 * @throws SQLException
	 */
	public static void addToLog(long writerId, String table, long key, int command, String desc) throws SQLException {
		Connection con = getConnection();
		try {
			addToLog(con, writerId, table, key, command, desc);
		} finally {
			closeConnection(con);
		}
	}

	/**
	 * add the command to log table
	 * @param writerId
	 * @param table
	 * @param key
	 * @param command
	 * @param desc
	 * @throws SQLException
	 */
	public static void addToLog(Connection con, long writerId, String table, long key, int command, String desc) throws SQLException{
		GeneralDAO.insertLog(con, writerId, table, key, command, desc);
	}

	/**
	 * disable or enable opp by trader
	 * @param opId opp id to enable
	 * @throws SQLException
	 */
	public static void disableEnableOppByTrader(long opId, int disable) throws SQLException {
		Connection con = getConnection();
		try {
			OpportunitiesDAO.disableEnableOppByTrader(con, opId, disable);
		} finally {
			closeConnection(con);
		}
	}

	/**
	 * check if opp disable by service
	 * @param opId
	 * @return true if disable false if enable
	 * @throws SQLException
	 */
	public static boolean isDisableByService(long opId) throws SQLException {
		Connection con = getConnection();
		boolean disable = true;
		try {
			disable = OpportunitiesDAO.isDisableByService(con, opId);
		} finally {
			closeConnection(con);
		}
		return disable;
	}

	/**
	 * suspend market or resume
	 * @param id
	 * @param suspended
	 * @param msg
	 * @throws SQLException
	 */
	public static void updateSuspendedMarket(String feedName, boolean suspended, String msg) throws SQLException {

		Connection con = getConnection();
		try {
			MarketsDAO.updateSuspendedMarket(con, feedName, suspended, msg);
		} finally {
			closeConnection(con);
		}
	}
	
	public static void updateSuspendedMarketByType(long type, boolean suspended, String msg) throws SQLException {

		Connection con = getConnection();
		try {
			MarketsDAO.updateSuspendedMarketByType(con, type, suspended, msg);
		} finally {
			closeConnection(con);
		}
	}

	/**
	 * get the service config
	 * @return the service config
	 * @throws SQLException
	 */
	public static ServiceConfig getServiceConfig() throws SQLException {
		Connection con = getConnection();
		ServiceConfig temp = null;
		try {
			temp = ServiceConfigDAO.getServiceConfig(con);
		} finally {
			closeConnection(con);
		}
		return temp;
	}

	public static void updateParameter(String market, double value) throws SQLException {
		Connection con = getConnection();
		try {
			ServiceConfigDAO.updateParameter(con, market, value);
		} finally {
			closeConnection(con);
		}
	}

	/**
	 * resettle opp
	 * @param opId opp id
	 * @param closingLevel the closing level
	 * @param writer
	 * @throws SQLException
	 * @returns list of unique users
	 */
	public static HashSet<Long> resettleOpp(long opId, double closingLevel, long writer, long oppTypeId) throws SQLException {
		Connection con = getConnection();
		try {
			con.setAutoCommit(false);

			OpportunitiesDAO.updateOppNewLevel(con, opId, closingLevel, writer);

			HashSet<Long> listOfUsers = new HashSet<Long>();
			ArrayList<Investment> list = InvestmentsManagerBase.getAllInvestmentsByOppId(opId, con);
			for (int i = 0; i < list.size(); i++) {
				Investment inv = list.get(i);

				if (inv.getIsCanceled() == 0) {

					boolean canceled = InvestmentsManagerBase.cancelInvestment(inv.getId(), writer, con);
					if (canceled) {  // if canceled done
						logger.debug("Cancel invId: " + inv.getId());
						//BMS
						BonusDAOBase.updateAdjustedAmountUponResettle(con, inv.getId());
						//deduct the investment amount from the balance as if the user re-invested
						long negFixTransactionId = UsersManager.addToBalanceNonNeg(	con, inv.getUserId(), -inv.getAmount(), inv.getRate(),
																					Utils.getUser(inv.getUserId()).getUtcOffset());
						InvestmentsDAO.prepareInvestmentToSettle(con, inv.getId());
						if (resettleOpportunities.contains(oppTypeId)) {
							InvestmentsManagerBase.settleInvestment(con, inv.getId(), writer, 0, null, ConstantsBase.REGULAR_SETTLEMNT, ConstantsBase.LOG_BALANCE_RESETTLE_INVESTMENT, 0L);
							if (negFixTransactionId > 0) {
								GeneralDAO.insertBalanceLog(con, AdminManager.getWriterId(), inv.getUserId(),
															ConstantsBase.TABLE_INVESTMENTS, inv.getId(),
															Constants.LOG_BALANCE_FIX_NEGATIVE_BALANCE_DEPOSIT,
															Utils.getUser(inv.getUserId()).getUtcOffset());
							}
						}
						// for copyop recalculate profile statistics
						listOfUsers.add(inv.getUserId());
					} else {
						logger.warn("Cancel for invId: " + inv.getId() + " allready done!");
					}
				}
			}
			con.commit();
			return listOfUsers;
		} catch (SQLException e) {
			try {
				con.rollback();
			} catch (SQLException ie) {
				logger.log(Level.ERROR, "Can't rollback.", ie);
			}
			throw e;
		} finally {
			con.setAutoCommit(true);
			closeConnection(con);
		}

	}

	 /**
     * Close one touch opportunity and set the highest\lowest level.
     *
     * @param oppId the id of the opp to close
     * @param closingLevel the closing level
     * @param closeLevelTxt the closing level formula as txt
     * @throws SQLException
     */
    public static void closeOneTouchOpportunity(long oppId, double closingLevel, String closeLevelTxt, long writerId, String typeIdTxt) throws SQLException {
        Connection con = null;
        try {
            con = getConnection();
            OpportunitiesDAOBase.closeOpportunity(con, oppId, closingLevel, closeLevelTxt);
            addToLog(writerId, "opportunities", oppId, ConstantsBase.LOG_COMMANDS_MANUAL_SETTLE, typeIdTxt + " Level: " + closingLevel);
        } catch (SQLException sqle) {
            throw sqle;
        } catch (Throwable t) {
            logger.log(Level.FATAL, "Can't clos one touch opp.", t);
        } finally {
        	closeConnection(con);
        }
    }

    /**
     * update opp max exposure (from TT)
     * @param oppId opp id to change
     * @param maxExposure the new value
     * @throws SQLException
     */
	public static void updateMaxExposureAllSkinGroup(long oppId, int maxExposure) throws SQLException {
		Connection con = getConnection();
		try {
			for(SkinGroup sg:SkinGroup.NON_AUTO_SET) {
				OpportunitiesDAO.updateOpportunityMaxExposure(con, oppId, maxExposure, sg);
			}
		} finally {
			closeConnection(con);
		}

	}

	/**
	 * get markets list by group market id
	 * @param marketGroupId
	 * @return list of markets
	 */
	public static ArrayList<SelectItem> getMarketsListById(String marketGroupId) throws SQLException {
		Connection con = getConnection();
		ArrayList<SelectItem> markets = null;
		try {
			markets = MarketsDAO.getMarketsBygroupId(con, marketGroupId);
		} finally {
			closeConnection(con);
		}
		return markets;
	}

	/**
	 * get list of market name and oppid to shift them togther from TT
	 * @param oppId the opp id we will shift
	 * @return translated list of markets name and opp id
	 * @throws SQLException
	 */
	public static ArrayList<SelectItem> getShiftGroupSI(long oppId) throws SQLException {
		Connection con = getConnection();
		ArrayList<SelectItem> shiftGroup = null;
		try {
			shiftGroup = MarketsDAO.getShiftGroupSI(con, oppId);
		} finally {
			closeConnection(con);
		}
		return shiftGroup;
	}

	/**
	 * get list of market name and oppid to shift them togther from TT
	 * @param oppId the opp id we will shift
	 * @param date from
	 * @return translated list of markets name and opp id
	 * @throws SQLException
	 */
	public static ArrayList<SelectItem> getShiftGroupSINonPublished(long oppId, Date date) throws SQLException {
		Connection con = getConnection();
		ArrayList<SelectItem> shiftGroup = null;
		try {
			shiftGroup = MarketsDAO.getShiftGroupSIPublished(con, oppId, date);
		} finally {
			closeConnection(con);
		}
		return shiftGroup;
	}
	
	
	public static void updateBinary0100opp(long oppId, String params) throws SQLException {
		Connection con = getConnection();
		try {
			OpportunitiesDAO.updateBinary0100opp(con, oppId, params);
		} finally {
			closeConnection(con);
		}
	}

	public static double getNightShiftLimitByOppId(long oppId) throws SQLException {
		Connection con = getConnection();
		double limit;
		try {
			limit = OpportunitiesDAO.getNightShiftLimitByOppId(con, oppId);
		} finally {
			closeConnection(con);
		}
		return limit;
	}
	
	public static Market getDynamicsMarketQuoteParams(long marketId) throws SQLException {
		Connection con = null;
		try {
			con = getConnection();
			return MarketsDAO.getDynamicsMarketQuoteParams(con, marketId);
		} finally {
			closeConnection(con);
		}
	}
}