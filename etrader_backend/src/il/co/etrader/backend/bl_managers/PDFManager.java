package il.co.etrader.backend.bl_managers;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Locale;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import com.anyoption.backend.managers.TransactionManager;
import com.anyoption.common.beans.CreditCard;
import com.anyoption.common.beans.Transaction;
import com.anyoption.common.beans.base.Skin;
import com.anyoption.common.managers.BaseBLManager;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.ColumnText;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfPageEventHelper;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfStamper;
import com.itextpdf.text.pdf.PdfWriter;

import il.co.etrader.backend.dao_managers.MailBoxUsersDAO;
import il.co.etrader.backend.util.Constants;
import il.co.etrader.backend.util.Utils;
import il.co.etrader.bl_managers.TransactionsManagerBase;
import il.co.etrader.bl_vos.UserBase;
import il.co.etrader.util.CommonUtil;


	/**
	 * PDF manager class using iText for PDF file creation
	 *
	 * @author Kobi
	 *
	 */
	public class PDFManager extends BaseBLManager {

		private static final Logger logger = Logger.getLogger(PDFManager.class);

		public static final String FONT_DEFAULT = "D";

		public static final String FONT_BOLD = "B";

		public static final String FONT_UNDERLINE_BLUE = "UB";
		
		public static final String FONT_BOLD_SMALL_CENTR = "BS";
		
		private static BaseFont arialFont = null;

		private static Font font_AO = null;

		private static Font fontBold_AO = null;

		private static Font font_ET = null;

		private static Font fontBold_ET = null;
		
		private static Font underLineBlueUrl = null; 
		
		private static Font fontBoldSmall_AO = null;

		static {
			try {
				HttpServletRequest request = (HttpServletRequest) FacesContext
						.getCurrentInstance().getExternalContext().getRequest();
				ServletContext servletContext = request.getSession()
						.getServletContext();
				arialFont = BaseFont.createFont(servletContext
						.getRealPath("/WEB-INF/resources/ARIALUNI.TTF"),
						BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
				BaseColor aoColor = new BaseColor(9, 55, 85);
				BaseColor etColor = new BaseColor(75, 92, 89);
				font_AO = new Font(arialFont, 12);
				font_AO.setColor(aoColor);
				fontBold_AO = new Font(arialFont, 12, Font.BOLD, aoColor);
				font_ET = new Font(arialFont, 12);
				font_ET.setColor(etColor);
				fontBold_ET = new Font(arialFont, 12, Font.BOLD, etColor);
				
				underLineBlueUrl = new Font(arialFont, 12, Font.UNDERLINE);
				underLineBlueUrl.setColor(0, 0, 255);
				
				fontBoldSmall_AO = new Font(arialFont, 8, Font.BOLD, aoColor);
			} catch (Exception e) {
				logger.error("Problem loading fonts for PDF creator. ", e);
			}
		}

		/**
		 * Create text paragraph and put it in a table call
		 *
		 * @param text
		 * @param skinid
		 * @return table cell
		 */
		public static PdfPCell createParagraph(ArrayList<PdfMessage> messages,
				long skinId) {
			int align = Paragraph.ALIGN_LEFT;
			int direction = PdfWriter.RUN_DIRECTION_LTR;
			Font font = font_AO;
			Font fontBold = fontBold_AO;

			// choose cell align & directon
			if (skinId == Skin.SKIN_ETRADER || skinId == Skin.SKIN_ARABIC) {

				align = Paragraph.ALIGN_RIGHT;
				direction = PdfWriter.RUN_DIRECTION_RTL;
			}

			if (skinId == Skin.SKIN_ETRADER) {
				font = font_ET;
				fontBold = fontBold_ET;
			}

			Paragraph p = new Paragraph();

			p.setAlignment(align);
			boolean is_center = false;
			for (PdfMessage msg : messages) {
				String message = msg.getMessage();
				String fontM = msg.getFont();
				if (fontM.equals(FONT_DEFAULT)) {
					p.setFont(font);
				} else if (fontM.equals(FONT_UNDERLINE_BLUE)){
					p.setFont(underLineBlueUrl);
				} else if (fontM.equals(FONT_BOLD_SMALL_CENTR)){
					p.setFont(fontBoldSmall_AO);
					p.setAlignment(Paragraph.ALIGN_MIDDLE);
					is_center = true;
				} else {
					p.setFont(fontBold);
					
				}	
				p.add(message);
				
			}
			PdfPCell cell = new PdfPCell(p);
			cell.setRunDirection(direction);
			cell.setBorder(PdfPCell.NO_BORDER);
			cell.setColspan(3);
			if (is_center){
			cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
			cell.setHorizontalAlignment(Element.ALIGN_CENTER);
			}
			cell.setLeading(5.0f, 1.0f);
			return cell;
		}

		/**
		 * Message class each message contains the font
		 *
		 * @author Kobi
		 *
		 */
		public class PdfMessage {
			private String message;

			private String font;

			public PdfMessage(String message, String font) {
				this.message = message;
				this.font = font;
			}

			public String getFont() {
				return font;
			}

			public void setFont(String font) {
				this.font = font;
			}

			public String getMessage() {
				return message;
			}

			public void setMessage(String message) {
				this.message = message;
			}

		}

		/**
		 * Create iText Image
		 *
		 * @param skinId
		 * @return
		 * @throws Exception
		 */
		public static Image getHeader(long skinId) throws Exception {
			int align = Image.ALIGN_LEFT;
			String imagePath = "http://picbase.etrader.co.il/images/ao_blue.jpg";
			if (skinId == Skin.SKIN_ETRADER) {
				align = Image.ALIGN_RIGHT;
				imagePath = "http://picbase.etrader.co.il/images/et_logo.jpg";
			} else if (skinId == Skin.SKIN_CHINESE){//chinese header
                imagePath = "http://picbase.etrader.co.il/images/ao_blue_zh.jpg";
            }
			Image header = Image.getInstance(imagePath);
			header.setAlignment(align);
			// header.scaleAbsolute(400f, 84f);
			return header;
		}
		
		public static Image getHeaderPAOReg(long skinId) throws Exception {
			int align = Image.ALIGN_CENTER;
			//CommonUtil.getProperty("host.url.https") +  "/images/ouroboros.png";
			String imagePath = "http://picbase.etrader.co.il/images/ouroboros_logo_small.png";
			Image header = Image.getInstance(imagePath);
			header.setAlignment(align);
		    //header.scaleAbsolute(200f, 84f);
			return header;
		}

		/**
		 * Create and send first withdrawal email with PDF attachment
		 *
		 * @param user
		 * @param cardId
		 * @param templateId
		 * @return
		 * @throws Exception
		 */
		public static String sendAndCreateFirstWithdrawalPDF(UserBase user, String cardId, String templateId, String mailToSupport, Date from, Date to) throws Exception {
			// take the withdrawal transaction by selected card id
			Transaction withdrawal = TransactionsManager.getFirstWithdrawal(user.getId(), Long.parseLong(cardId));
			if (null == withdrawal) { // withdrawal not found
				FacesContext context = FacesContext.getCurrentInstance();
				FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_INFO, CommonUtil.getMessage("templates.withdrawal.not.found", null, Utils.getWriterLocale(context)), null);
				context.addMessage(null, fm);

				return "templates.withdrawal.not.found";
			}

			Document document = new Document();
			
			String filePathSrc = CommonUtil.getProperty(Constants.FILES_PATH) + "withdrawal_" + user.getId() + "_" + cardId + "(1)"+".pdf";
			String filePathDest = CommonUtil.getProperty(Constants.FILES_PATH) + "withdrawal_" + user.getId() + "_" + cardId + ".pdf";
			
			File fileTmp = new File(filePathSrc);
			FileOutputStream fos = new FileOutputStream(fileTmp);
			PdfWriter writer = PdfWriter.getInstance(document, fos);

			// use for new line
			Phrase phrase = new Phrase("");
			phrase.add("\n");

			document.open();
		
			int langId = user.getSkin().getDefaultLanguageId();
			String langCode = ApplicationData.getLanguage(langId).getCode();
			Locale locale = new Locale(langCode);
			
			PdfFooterSignature event = new PdfFooterSignature(locale);
			writer.setPageEvent(event);
			
			document.add(getHeader(user.getSkinId()));
			document.add(phrase);

			PdfPTable table = new PdfPTable(1);
			table.setWidthPercentage(100);
			ArrayList<PdfMessage> messages = new ArrayList<PdfMessage>();
			PDFManager pdfManager = new PDFManager(); // create pdf manager
			// instance

			String[] params = new String[1];
			params[0] = CommonUtil.getDateFormat(new Date());
			
			if (user.getSkin().isRegulated()) {
				messages.add(pdfManager.new PdfMessage(CommonUtil.getMessage("pdf.withdrawal.ph.1.reg", params, locale), PDFManager.FONT_BOLD));
			} else {
				messages.add(pdfManager.new PdfMessage(CommonUtil.getMessage("pdf.withdrawal.ph.1", params, locale), PDFManager.FONT_BOLD));
			}
			
			table.addCell(PDFManager.createParagraph(messages, user.getSkinId()));

			messages = new ArrayList<PdfMessage>();
			params[0] = String.valueOf(user.getId());
			messages.add(pdfManager.new PdfMessage(CommonUtil.getMessage("pdf.withdrawal.ph.2", params, locale), PDFManager.FONT_BOLD));
			table.addCell(PDFManager.createParagraph(messages, user.getSkinId()));

			messages = new ArrayList<PdfMessage>();
			messages.add(pdfManager.new PdfMessage(CommonUtil.getMessage("pdf.withdrawal.ph.3", null, locale), PDFManager.FONT_BOLD));
			table.addCell(PDFManager.createParagraph(messages, user.getSkinId()));

			messages = new ArrayList<PdfMessage>();
			messages.add(pdfManager.new PdfMessage("1. ", PDFManager.FONT_BOLD));
			messages.add(pdfManager.new PdfMessage(CommonUtil.getMessage("pdf.withdrawal.ph.4", null, locale),PDFManager.FONT_DEFAULT));
			table.addCell(PDFManager.createParagraph(messages, user.getSkinId()));

			HashMap<CreditCard, ArrayList<Transaction>> ccTransactions = TransactionsManager.getCreditCardDeposits(user.getId(), from, to);
			int idx = 2;

			if (!ccTransactions.isEmpty()) {
				for (Iterator<CreditCard> iter = ccTransactions.keySet().iterator(); iter.hasNext();) {
					CreditCard c = iter.next();
					ArrayList<Transaction> transactions = ccTransactions.get(c);
					params = new String[3];
					params[0] = CommonUtil.getMessage(c.getType().getName(), null);
					params[1] = c.getCcNumberLast4();
					params[2] = c.getHolderName();

					messages = new ArrayList<PdfMessage>();
					messages.add(pdfManager.new PdfMessage("\n"	+ String.valueOf(idx) + ". ", PDFManager.FONT_BOLD));
					messages.add(pdfManager.new PdfMessage(CommonUtil.getMessage("pdf.withdrawal.ph.5", params, locale),PDFManager.FONT_DEFAULT));
					table.addCell(PDFManager.createParagraph(messages, user.getSkinId()));

					for (Transaction t : transactions) {
						t.setCurrency(user.getCurrency());
						params = new String[3];
						params[0] = CommonUtil.getDateFormat(t.getTimeCreated());
						params[1] = CommonUtil.displayAmount(t.getAmount(), t.getCurrency().getId());
						params[2] = String.valueOf(t.getId());
						messages = new ArrayList<PdfMessage>();
						messages.add(pdfManager.new PdfMessage(CommonUtil.getMessage("pdf.withdrawal.ph.6", params, locale),PDFManager.FONT_DEFAULT));
						table.addCell(PDFManager.createParagraph(messages, user.getSkinId()));
					}
					idx++;
				}

				messages = new ArrayList<PdfMessage>();
				if (user.getSkin().isRegulated()) {
					messages.add(pdfManager.new PdfMessage(CommonUtil.getMessage("pdf.withdrawal.ph.7.reg", null, locale), PDFManager.FONT_DEFAULT));
				} else {
					messages.add(pdfManager.new PdfMessage(CommonUtil.getMessage("pdf.withdrawal.ph.7", null, locale), PDFManager.FONT_DEFAULT));
				}
				table.addCell(PDFManager.createParagraph(messages, user.getSkinId()));
			}
			
			ArrayList<Transaction> nonCreditCardTransactions = TransactionsManager.getNonCreditCardDepositsByUserId(user.getId(), from, to);
			
			if (!nonCreditCardTransactions.isEmpty()) {
				
				messages = new ArrayList<PdfMessage>();
				messages.add(pdfManager.new PdfMessage("\n"	+ String.valueOf(idx) + ". ", PDFManager.FONT_BOLD));
				messages.add(pdfManager.new PdfMessage(CommonUtil.getMessage("pdf.confirmation.ph.15", null, locale),PDFManager.FONT_DEFAULT));
				table.addCell(PDFManager.createParagraph(messages, user.getSkinId()));
				
				for (Transaction t : nonCreditCardTransactions) {
					t.setCurrency(user.getCurrency());
					params = new String[4];
					params[0] = CommonUtil.getDateFormat(t.getTimeCreated());
					params[1] = CommonUtil.displayAmount(t.getAmount(), t.getCurrency().getId());
					params[2] = String.valueOf(t.getId());
					params[3] = CommonUtil.getMessage(t.getDescription(), null, locale);
					messages = new ArrayList<PdfMessage>();
					messages.add(pdfManager.new PdfMessage(CommonUtil.getMessage("pdf.confirmation.ph.14", params, locale), PDFManager.FONT_DEFAULT));
					table.addCell(PDFManager.createParagraph(messages, user.getSkinId()));
				}
			}
			
			params = new String[3];
			params[0] = CommonUtil.displayAmount(withdrawal.getAmount(), user.getCurrencyId());
			params[1] = CommonUtil.getMessage(withdrawal.getCreditCard().getType().getName(), null);
			params[2] = withdrawal.getCreditCard().getCcNumberLast4(); // just for
			// etrader

			messages = new ArrayList<PdfMessage>();
			messages.add(pdfManager.new PdfMessage(CommonUtil.getMessage("pdf.withdrawal.ph.8", params, locale),PDFManager.FONT_DEFAULT));
			table.addCell(PDFManager.createParagraph(messages, user.getSkinId()));

			messages = new ArrayList<PdfMessage>();
			messages.add(pdfManager.new PdfMessage(CommonUtil.getMessage("pdf.withdrawal.ph.9", null, locale), PDFManager.FONT_BOLD));
			table.addCell(PDFManager.createParagraph(messages, user.getSkinId()));

			messages = new ArrayList<PdfMessage>();
			messages.add(pdfManager.new PdfMessage(CommonUtil.getMessage("pdf.withdrawal.ph.10", null, locale)+ " " + user.getFirstName() + " " + user.getLastName()+ "\n\n", PDFManager.FONT_BOLD));
			table.addCell(PDFManager.createParagraph(messages, user.getSkinId()));

			messages = new ArrayList<PdfMessage>();
			String param = String.valueOf(user.getId());
			if (user.getSkinId() == Skin.SKIN_ETRADER) {
				param = user.getIdNum();
			}
			messages.add(pdfManager.new PdfMessage(CommonUtil.getMessage("pdf.withdrawal.ph.11", null, locale)+ " " + param + "\n\n", PDFManager.FONT_BOLD));
			table.addCell(PDFManager.createParagraph(messages, user.getSkinId()));

			messages = new ArrayList<PdfMessage>();
			messages.add(pdfManager.new PdfMessage(CommonUtil.getMessage("pdf.withdrawal.ph.12", null, locale)+ "\n\n", PDFManager.FONT_BOLD));
			table.addCell(PDFManager.createParagraph(messages, user.getSkinId()));

			/*messages = new ArrayList<PdfMessage>();
			messages.add(pdfManager.new PdfMessage(CommonUtil.getMessage("pdf.withdrawal.ph.13", null, locale)+ "\n\n", PDFManager.FONT_BOLD));
			table.addCell(PDFManager.createParagraph(messages, user.getSkinId()));*/
			
			document.add(table);   
			document.close();
			
			addPaging(filePathSrc, filePathDest);
			
			if (fileTmp.delete()) {
				logger.debug("File with name" + fileTmp.getName() + " is deleted successfully");
			} else {
				logger.debug("For file with name" + fileTmp.getName() + ", delete operation is failed");
			}
			// Save the attachment in DB
			File file = new File(filePathDest);
			Connection con = null;
			long attachmentId = 0;
			try {
				con = getConnection();
				attachmentId = MailBoxUsersDAO.insertAttachmentAsBLOB(con, file);
			} finally {
				closeConnection(con);
			}
			File[]attachments = {file};
		  
			UsersManager.sendTemplate(Long.valueOf(templateId).longValue(), user, attachments, withdrawal.getCreditCard().getCcNumberLast4(), null, attachmentId, mailToSupport);
			return null;
		}

		/**
		 * Create and send just confirmation email with PDF attachment
		 *
		 * @param user
		 * @param cardId
		 * @param templateId
		 * @return
		 * @throws Exception
		 */
		public static String sendAndCreateJustConfirmationPDF(UserBase user, String templateId, String mailToSupport, Date from, Date to) throws Exception {
			// take the deposit transaction by all cards
			HashMap<CreditCard, ArrayList<Transaction>> ccTransactions = TransactionsManager.getCreditCardDeposits(user.getId(), from, to);
			if (null == ccTransactions) { // withdrawal not found
				FacesContext context = FacesContext.getCurrentInstance();
				FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_INFO, CommonUtil.getMessage("templates.confirmation.not.found", null, Utils.getWriterLocale(context)), null);
				context.addMessage(null, fm);

				return "templates.confirmation.not.found";
			}
			
			ArrayList<Transaction> nonCreditCardTransactions = TransactionsManager.getNonCreditCardDepositsByUserId(user.getId(), from, to);
			
			Document document = new Document();
			
			String filePathSrc = CommonUtil.getProperty(Constants.FILES_PATH) + "confirmation_" + user.getId() + "(1)" + ".pdf";
			String filePathDest = CommonUtil.getProperty(Constants.FILES_PATH) + "confirmation_" + user.getId() + ".pdf";

			File fileTmp = new File(filePathSrc);
			FileOutputStream fos = new FileOutputStream(fileTmp);
			PdfWriter writer = PdfWriter.getInstance(document, fos);

			// use for new line
			Phrase phrase = new Phrase("");
			phrase.add("\n");

			document.open();
			
			int langId = user.getSkin().getDefaultLanguageId();
			String langCode = ApplicationData.getLanguage(langId).getCode();
			Locale locale = new Locale(langCode);

			PdfFooterSignature event = new PdfFooterSignature(locale);
			writer.setPageEvent(event);
			
			document.add(getHeader(user.getSkinId()));
			document.add(phrase);

			PdfPTable table = new PdfPTable(1);
			table.setWidthPercentage(100);
			ArrayList<PdfMessage> messages = new ArrayList<PdfMessage>();
			PDFManager pdfManager = new PDFManager(); // create pdf manager instance

			String[] params = new String[1];
			params[0] = CommonUtil.getDateFormat(new Date());
			if (user.getSkin().isRegulated()) {
				messages.add(pdfManager.new PdfMessage(CommonUtil.getMessage("pdf.confirmation.ph.1.reg", params, locale),PDFManager.FONT_BOLD));
			} else {
				messages.add(pdfManager.new PdfMessage(CommonUtil.getMessage("pdf.confirmation.ph.1", params, locale),PDFManager.FONT_BOLD));
			}
			table.addCell(PDFManager.createParagraph(messages, user.getSkinId()));

			messages = new ArrayList<PdfMessage>();
			params[0] = String.valueOf(user.getId());
			messages.add(pdfManager.new PdfMessage(CommonUtil.getMessage("pdf.confirmation.ph.2", params, locale),PDFManager.FONT_BOLD));
			table.addCell(PDFManager.createParagraph(messages, user.getSkinId()));

			messages = new ArrayList<PdfMessage>();
			messages.add(pdfManager.new PdfMessage(CommonUtil.getMessage("pdf.confirmation.ph.3", null, locale), PDFManager.FONT_BOLD));
			table.addCell(PDFManager.createParagraph(messages, user.getSkinId()));
			
			messages = new ArrayList<PdfMessage>();
			messages.add(pdfManager.new PdfMessage("1. ", PDFManager.FONT_BOLD));
			messages.add(pdfManager.new PdfMessage(CommonUtil.getMessage("pdf.confirmation.ph.4", null, locale),PDFManager.FONT_DEFAULT));
			table.addCell(PDFManager.createParagraph(messages, user.getSkinId()));

			int idx = 2;
			for (Iterator<CreditCard> iter = ccTransactions.keySet().iterator(); iter.hasNext();) {
				CreditCard c = iter.next();
				ArrayList<Transaction> transactions = ccTransactions.get(c);
				params = new String[3];
				params[0] = CommonUtil.getMessage(c.getType().getName(), null);
				params[1] = c.getCcNumberLast4();
				params[2] = c.getHolderName();

				messages = new ArrayList<PdfMessage>();
				messages.add(pdfManager.new PdfMessage("\n"	+ String.valueOf(idx) + ". ", PDFManager.FONT_BOLD));
				messages.add(pdfManager.new PdfMessage(CommonUtil.getMessage("pdf.confirmation.ph.5", params, locale), PDFManager.FONT_DEFAULT));
				table.addCell(PDFManager.createParagraph(messages, user.getSkinId()));

				for (Transaction t : transactions) {
					t.setCurrency(user.getCurrency());
					params = new String[3];
					params[0] = CommonUtil.getDateFormat(t.getTimeCreated());
					params[1] = CommonUtil.displayAmount(t.getAmount(), t.getCurrency().getId());
					params[2] = String.valueOf(t.getId());
					messages = new ArrayList<PdfMessage>();
					messages.add(pdfManager.new PdfMessage(CommonUtil.getMessage("pdf.confirmation.ph.6", params, locale), PDFManager.FONT_DEFAULT));
					table.addCell(PDFManager.createParagraph(messages, user.getSkinId()));
				}
				idx++;
			}
			
			if (!nonCreditCardTransactions.isEmpty()) {
				
				messages = new ArrayList<PdfMessage>();
				messages.add(pdfManager.new PdfMessage("\n"	+ String.valueOf(idx) + ". ", PDFManager.FONT_BOLD));
				messages.add(pdfManager.new PdfMessage(CommonUtil.getMessage("pdf.confirmation.ph.15", null, locale),PDFManager.FONT_DEFAULT));
				table.addCell(PDFManager.createParagraph(messages, user.getSkinId()));
				
				for (Transaction t : nonCreditCardTransactions) {
					t.setCurrency(user.getCurrency());
					params = new String[4];
					params[0] = CommonUtil.getDateFormat(t.getTimeCreated());
					params[1] = CommonUtil.displayAmount(t.getAmount(), t.getCurrency().getId());
					params[2] = String.valueOf(t.getId());
					params[3] = CommonUtil.getMessage(t.getDescription(), null, locale);
					messages = new ArrayList<PdfMessage>();
					messages.add(pdfManager.new PdfMessage(CommonUtil.getMessage("pdf.confirmation.ph.14", params, locale), PDFManager.FONT_DEFAULT));
					table.addCell(PDFManager.createParagraph(messages, user.getSkinId()));
				}
				
			}
			
			messages = new ArrayList<PdfMessage>();
			if (user.getSkin().isRegulated()) {
				messages.add(pdfManager.new PdfMessage(CommonUtil.getMessage("pdf.confirmation.ph.7.reg", null, locale), PDFManager.FONT_DEFAULT));
			} else {
				messages.add(pdfManager.new PdfMessage(CommonUtil.getMessage("pdf.confirmation.ph.7", null, locale), PDFManager.FONT_DEFAULT));
			}
			table.addCell(PDFManager.createParagraph(messages, user.getSkinId()));

			messages = new ArrayList<PdfMessage>();
			messages.add(pdfManager.new PdfMessage(CommonUtil.getMessage("pdf.confirmation.ph.9", null, locale), PDFManager.FONT_BOLD));
			table.addCell(PDFManager.createParagraph(messages, user.getSkinId()));

			messages = new ArrayList<PdfMessage>();
			messages.add(pdfManager.new PdfMessage(CommonUtil.getMessage("pdf.confirmation.ph.10", null, locale) + " " + user.getFirstName() + " " + user.getLastName() + "\n\n", PDFManager.FONT_BOLD));
			table.addCell(PDFManager.createParagraph(messages, user.getSkinId()));

			messages = new ArrayList<PdfMessage>();
			String param = String.valueOf(user.getId());
			
			if (user.getSkinId() == Skin.SKIN_ETRADER) {
				param = user.getIdNum();
			}
			
			messages.add(pdfManager.new PdfMessage(CommonUtil.getMessage("pdf.confirmation.ph.11", null, locale) + " " + param + "\n\n", PDFManager.FONT_BOLD));
			table.addCell(PDFManager.createParagraph(messages, user.getSkinId()));

			messages = new ArrayList<PdfMessage>();
			messages.add(pdfManager.new PdfMessage(CommonUtil.getMessage("pdf.confirmation.ph.12", null, locale) + "\n\n\n", PDFManager.FONT_BOLD));
			table.addCell(PDFManager.createParagraph(messages, user.getSkinId()));
			
			/*messages = new ArrayList<PdfMessage>();
			messages.add(pdfManager.new PdfMessage(CommonUtil.getMessage("pdf.confirmation.ph.13", null, locale) + "\n\n", PDFManager.FONT_BOLD));
			table.addCell(PDFManager.createParagraph(messages, user.getSkinId()));*/
			
			document.add(table);
            document.close();
			
			addPaging(filePathSrc, filePathDest);
           
			
			if (fileTmp.delete()) {
				logger.debug("File with name" + fileTmp.getName() + " is deleted successfully");
			} else {
				logger.debug("For file with name" + fileTmp.getName() + ", delete operation is failed");
			}
			
			File file = new File(filePathDest);
			
			// Save the attachment in DB
			Connection con = null;
			long attachmentId = 0;
			try {
				con = getConnection();
				attachmentId = MailBoxUsersDAO.insertAttachmentAsBLOB(con, file);
			} finally {
				closeConnection(con);
			}
			File[]attachments = {file};
			  
			UsersManager.sendTemplate(Long.valueOf(templateId).longValue(), user, attachments, null, null, attachmentId, mailToSupport);
			return null;
		}	
		
	public static String sendAndCreateClosingAccountPDF(UserBase user,
														String templateId, String mailToSupport) throws Exception {
		// take the deposit transaction by all cards
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.DATE, -180);
		Date from = cal.getTime();
		Date to = new Date();
		HashMap<CreditCard, ArrayList<Transaction>> ccTransactions = TransactionsManager
				.getCreditCardDeposits(user.getId(), from, to);
		if (null == ccTransactions) { // withdrawal not found
			FacesContext context = FacesContext.getCurrentInstance();
			FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_INFO,
					CommonUtil.getMessage("templates.confirmation.not.found", null, Utils.getWriterLocale(context)),
					null);
			context.addMessage(null, fm);

			return "templates.confirmation.not.found";
		}

		ArrayList<Transaction> nonCreditCardTransactions = TransactionsManager
				.getNonCreditCardDepositsByUserId(user.getId(), from, to);

		Document document = new Document();

		String filePathSrc = CommonUtil.getProperty(Constants.FILES_PATH) + "Close_Account_" + user.getId() + "(1)"
				+ ".pdf";
		String filePathDest = CommonUtil.getProperty(Constants.FILES_PATH) + "Close_Account_" + user.getId() + ".pdf";

		File fileTmp = new File(filePathSrc);
		FileOutputStream fos = new FileOutputStream(fileTmp);
		PdfWriter writer = PdfWriter.getInstance(document, fos);

		// use for new line
		Phrase phrase = new Phrase("");
		phrase.add("\n");

		document.open();

		int langId = user.getSkin().getDefaultLanguageId();
		String langCode = ApplicationData.getLanguage(langId).getCode();
		Locale locale = new Locale(langCode);

		PdfFooterSignature event = new PdfFooterSignature(locale);
		writer.setPageEvent(event);

		document.add(getHeader(user.getSkinId()));
		document.add(phrase);

		PdfPTable table = new PdfPTable(1);
		table.setWidthPercentage(100);
		ArrayList<PdfMessage> messages = new ArrayList<PdfMessage>();
		PDFManager pdfManager = new PDFManager(); // create pdf manager instance

		messages.add(pdfManager.new PdfMessage(CommonUtil.getMessage("pdf.closure.of.account.header", null, locale),
											   PDFManager.FONT_BOLD));
		table.addCell(PDFManager.createParagraph(messages, user.getSkinId()));

		String[] params = new String[3];
		messages = new ArrayList<PdfMessage>();
		params[0] = user.getFirstName();
		params[1] = user.getLastName();
		params[2] = CommonUtil.getDateFormat(new Date());
		messages.add(pdfManager.new PdfMessage(CommonUtil.getMessage("pdf.closure.of.account.ph.1", params, locale),
				PDFManager.FONT_BOLD));
		table.addCell(PDFManager.createParagraph(messages, user.getSkinId()));

		messages = new ArrayList<PdfMessage>();
		messages.add(pdfManager.new PdfMessage(CommonUtil.getMessage("pdf.closure.of.account.ph.2", null, locale),
				PDFManager.FONT_BOLD));
		table.addCell(PDFManager.createParagraph(messages, user.getSkinId()));

		int idx = 1;
		for (Iterator<CreditCard> iter = ccTransactions.keySet().iterator(); iter.hasNext();) {
			CreditCard c = iter.next();
			ArrayList<Transaction> transactions = ccTransactions.get(c);
			params = new String[3];
			params[0] = CommonUtil.getMessage(c.getType().getName(), null);
			params[1] = c.getCcNumberLast4();
			params[2] = c.getHolderName();

			messages = new ArrayList<PdfMessage>();
			messages.add(pdfManager.new PdfMessage("\n" + String.valueOf(idx) + ". ", PDFManager.FONT_BOLD));
			messages.add(pdfManager.new PdfMessage(CommonUtil.getMessage("pdf.closure.of.account.ph.3", params, locale),
					PDFManager.FONT_DEFAULT));
			table.addCell(PDFManager.createParagraph(messages, user.getSkinId()));

			for (Transaction t : transactions) {
				t.setCurrency(user.getCurrency());
				params = new String[3];
				params[0] = CommonUtil.getDateFormat(t.getTimeCreated());
				params[1] = CommonUtil.displayAmount(t.getAmount(), t.getCurrency().getId());
				params[2] = String.valueOf(t.getId());
				messages = new ArrayList<PdfMessage>();
				messages.add(pdfManager.new PdfMessage(CommonUtil.getMessage("pdf.closure.of.account.ph.4", params, locale),
						PDFManager.FONT_DEFAULT));
				table.addCell(PDFManager.createParagraph(messages, user.getSkinId()));
			}
			idx++;
		}

		if (!nonCreditCardTransactions.isEmpty()) {
			messages = new ArrayList<PdfMessage>();
			messages.add(pdfManager.new PdfMessage("\n" + String.valueOf(idx) + ". ", PDFManager.FONT_BOLD));
			messages.add(pdfManager.new PdfMessage(CommonUtil.getMessage("pdf.closure.of.account.ph.5", null, locale),
					PDFManager.FONT_DEFAULT));
			table.addCell(PDFManager.createParagraph(messages, user.getSkinId()));

			for (Transaction t : nonCreditCardTransactions) {
				t.setCurrency(user.getCurrency());
				params = new String[4];
				params[0] = CommonUtil.getDateFormat(t.getTimeCreated());
				params[1] = CommonUtil.displayAmount(t.getAmount(), t.getCurrency().getId());
				params[2] = String.valueOf(t.getId());
				params[3] = CommonUtil.getMessage(t.getDescription(), null, locale);
				messages = new ArrayList<PdfMessage>();
				messages.add(pdfManager.new PdfMessage(CommonUtil.getMessage("pdf.closure.of.account.ph.6", params, locale),
						PDFManager.FONT_DEFAULT));
				table.addCell(PDFManager.createParagraph(messages, user.getSkinId()));
			}
		}

		messages = new ArrayList<PdfMessage>();
		messages.add(pdfManager.new PdfMessage(CommonUtil.getMessage("pdf.closure.of.account.ph.7", null, locale),
				PDFManager.FONT_BOLD));
		table.addCell(PDFManager.createParagraph(messages, user.getSkinId()));

		messages = new ArrayList<PdfMessage>();
		messages.add(pdfManager.new PdfMessage(CommonUtil.getMessage("pdf.closure.of.account.ph.8", null, locale),
				PDFManager.FONT_BOLD));
		table.addCell(PDFManager.createParagraph(messages, user.getSkinId()));

		messages = new ArrayList<PdfMessage>();
		messages.add(pdfManager.new PdfMessage(CommonUtil.getMessage("pdf.closure.of.account.ph.9", null, locale),
				PDFManager.FONT_BOLD));
		table.addCell(PDFManager.createParagraph(messages, user.getSkinId()));

		document.add(table);
		document.close();

		addPaging(filePathSrc, filePathDest);

		if (fileTmp.delete()) {
			logger.debug("File with name" + fileTmp.getName() + " is deleted successfully");
		} else {
			logger.debug("For file with name" + fileTmp.getName() + ", delete operation is failed");
		}

		File file = new File(filePathDest);

		// Save the attachment in DB
		Connection con = null;
		long attachmentId = 0;
		try {
			con = getConnection();
			attachmentId = MailBoxUsersDAO.insertAttachmentAsBLOB(con, file);
		} finally {
			closeConnection(con);
		}
		File[] attachments = { file };

		long ccMinWithdraw = TransactionsManagerBase.getCreditCardMinWithdraw(user.getCurrencyId());
		String minWithdraw = CommonUtil.displayAmount(ccMinWithdraw, true, user.getCurrencyId());
		UsersManager.sendTemplate(Long.valueOf(templateId).longValue(), user, attachments, null, null, attachmentId,
				mailToSupport, minWithdraw);
		return null;
	}
		
		// Helper Class which has methods which executed after event - when page started and when page is in the end
		
	static class TableFooter extends PdfPageEventHelper {
			
			int pagenumber = 1;

			
	        // count the number of pages
	        public void onStartPage(PdfWriter writer, Document document) {
	            pagenumber++;
	        }

	        // write the number of page in the footer
	        public void onEndPage(PdfWriter writer, Document document)
	        {
	            com.itextpdf.text.Rectangle rect = new Rectangle(36, 54, 559, 788);
	            ColumnText.showTextAligned(writer.getDirectContent(), Element.ALIGN_CENTER, new Phrase(String.format(" %d", pagenumber)), (rect.getLeft() + rect.getRight()) / 2, rect.getBottom() - 18, 0);
	        }
	        
	    }	
		 
	static class PdfFooterSignature extends PdfPageEventHelper {

		Locale locale;

		public PdfFooterSignature(Locale locale) {
			this.locale = locale;
		}

		public void onEndPage(PdfWriter writer, Document document) {
			com.itextpdf.text.Rectangle rect = new Rectangle(36, 54, 1100, 1700);
			ColumnText.showTextAligned(writer.getDirectContent(), Element.ALIGN_RIGHT, new Phrase(CommonUtil.getMessage("pdf.signature", null, locale)), (rect.getLeft() + rect.getRight()) / 2, rect.getBottom() - 18, 0);
		}
	}	
		 

		/**
		 * Create and send non cc withdrawal email with PDF attachment
		 *
		 * @param user
		 * @param templateId
		 * @return
		 * @throws Exception
		 */
		public static String sendAndCreateNonCcWithdrawalPDF(UserBase user, String templateId, String mailToSupport, Date from, Date to) throws Exception {
			// take the deposit transaction by all cards
			Transaction withdraw = TransactionsManagerBase.getFirstWireWithdrawalByUser(user.getId());
			if (null == withdraw) { // withdrawal not found
				FacesContext context = FacesContext.getCurrentInstance();
				FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_INFO, CommonUtil.getMessage("templates.noncc.withdrawal.not.found", null, Utils.getWriterLocale(context)), null);
				context.addMessage(null, fm);

				return "templates.noncc.withdrawal.not.found";
			}
			 Document document = new Document(PageSize.A4, 36, 36, 54, 36);
		        // step 2
				
			String filePathSrc = CommonUtil.getProperty(Constants.FILES_PATH) + "nonCcWithdrawal_" + user.getId() + "(1)"+".pdf";
			String filePathDest = CommonUtil.getProperty(Constants.FILES_PATH) + "nonCcWithdrawal_" + user.getId() + ".pdf";
			
			File fileTmp = new File(filePathSrc);
		      
			FileOutputStream fos = new FileOutputStream(fileTmp);
			PdfWriter writer = PdfWriter.getInstance(document, fos);

			// use for new line
			Phrase phrase = new Phrase("");
			phrase.add("\n");

			document.open();
			
			int langId = user.getSkin().getDefaultLanguageId();
			String langCode = ApplicationData.getLanguage(langId).getCode();
			Locale locale = new Locale(langCode);
			
			PdfFooterSignature event = new PdfFooterSignature(locale);
			writer.setPageEvent(event);
			

			document.add(getHeader(user.getSkinId()));
			document.add(phrase);

			PdfPTable table = new PdfPTable(1);
			table.setWidthPercentage(100);
			ArrayList<PdfMessage> messages = new ArrayList<PdfMessage>();
			PDFManager pdfManager = new PDFManager(); // create pdf manager instance

			String[] params = new String[1];
			params[0] = CommonUtil.getDateFormat(new Date());
			if(user.getSkin().isRegulated()){
				messages.add(pdfManager.new PdfMessage(CommonUtil.getMessage("pdf.noncc.withdrawal.ph.1.reg", params, locale), PDFManager.FONT_BOLD));
			}else{
				messages.add(pdfManager.new PdfMessage(CommonUtil.getMessage("pdf.noncc.withdrawal.ph.1", params, locale), PDFManager.FONT_BOLD));
				
			}
			table.addCell(PDFManager.createParagraph(messages, user.getSkinId()));

			messages = new ArrayList<PdfMessage>();
			params[0] = String.valueOf(user.getId());
			messages.add(pdfManager.new PdfMessage(CommonUtil.getMessage("pdf.noncc.withdrawal.ph.2", params, locale), PDFManager.FONT_BOLD));
			table.addCell(PDFManager.createParagraph(messages, user.getSkinId()));

			messages = new ArrayList<PdfMessage>();
			messages.add(pdfManager.new PdfMessage(CommonUtil.getMessage("pdf.noncc.withdrawal.ph.3", null, locale), PDFManager.FONT_BOLD));
			table.addCell(PDFManager.createParagraph(messages, user.getSkinId()));
			
			messages = new ArrayList<PdfMessage>();
			messages.add(pdfManager.new PdfMessage("1. ", PDFManager.FONT_BOLD));
			messages.add(pdfManager.new PdfMessage(CommonUtil.getMessage("pdf.noncc.withdrawal.ph.4", null, locale),PDFManager.FONT_DEFAULT));
			table.addCell(PDFManager.createParagraph(messages, user.getSkinId()));

			Transaction tr = TransactionsManager.getNonCcDeposits(user.getId());
				int idx = 2;
				if (null != tr) {
					params = new String[3];
					params[0] = CommonUtil.getDateFormat(tr.getDepositsFrom());
					params[1] = CommonUtil.getDateFormat(tr.getDepositsTo());
					params[2] = CommonUtil.displayAmount(tr.getAmount(), user.getCurrencyId());
					messages = new ArrayList<PdfMessage>();
					messages.add(pdfManager.new PdfMessage(String.valueOf(idx) + ". " + CommonUtil.getMessage("pdf.noncc.withdrawal.ph.5", params, locale), PDFManager.FONT_DEFAULT));
					table.addCell(PDFManager.createParagraph(messages, user.getSkinId()));
					idx++;
				}

		HashMap<CreditCard, ArrayList<Transaction>> ccTransactions = TransactionsManager.getCreditCardDeposits(user.getId(), from, to);
					
		if (!ccTransactions.isEmpty()) {
			for (Iterator<CreditCard> iter = ccTransactions.keySet().iterator(); iter.hasNext();) {
				CreditCard c = iter.next();
				ArrayList<Transaction> transactions = ccTransactions.get(c);
				params = new String[3];
				params[0] = CommonUtil.getMessage(c.getType().getName(), null);
				params[1] = c.getCcNumberLast4();
				params[2] = c.getHolderName();

				messages = new ArrayList<PdfMessage>();
				messages.add(pdfManager.new PdfMessage("\n" + String.valueOf(idx) + ". ", PDFManager.FONT_BOLD));
				messages.add(pdfManager.new PdfMessage(CommonUtil.getMessage("pdf.noncc.withdrawal.ph.6", params, locale), PDFManager.FONT_DEFAULT));
				table.addCell(PDFManager.createParagraph(messages, user.getSkinId()));

				for (Transaction t : transactions) {
					t.setCurrency(user.getCurrency());
					params = new String[3];
					params[0] = CommonUtil.getDateFormat(t.getTimeCreated());
					params[1] = CommonUtil.displayAmount(t.getAmount(), t.getCurrency().getId());
					params[2] = String.valueOf(t.getId());
					messages = new ArrayList<PdfMessage>();
					messages.add(pdfManager.new PdfMessage(CommonUtil.getMessage("pdf.noncc.withdrawal.ph.7", params, locale), PDFManager.FONT_DEFAULT));
					table.addCell(PDFManager.createParagraph(messages, user.getSkinId()));
				}
				idx++;
			}
		}
			
		ArrayList<Transaction> nonCreditCardTransactions = TransactionsManager.getNonCreditCardDepositsByUserId(user.getId(), from, to);
			
			if (!nonCreditCardTransactions.isEmpty()) {
				
				messages = new ArrayList<PdfMessage>();
				messages.add(pdfManager.new PdfMessage("\n"	+ String.valueOf(idx) + ". ", PDFManager.FONT_BOLD));
				messages.add(pdfManager.new PdfMessage(CommonUtil.getMessage("pdf.confirmation.ph.15", null, locale),PDFManager.FONT_DEFAULT));
				table.addCell(PDFManager.createParagraph(messages, user.getSkinId()));
				
				for (Transaction t : nonCreditCardTransactions) {
					t.setCurrency(user.getCurrency());
					params = new String[4];
					params[0] = CommonUtil.getDateFormat(t.getTimeCreated());
					params[1] = CommonUtil.displayAmount(t.getAmount(), t.getCurrency().getId());
					params[2] = String.valueOf(t.getId());
					params[3] = CommonUtil.getMessage(t.getDescription(), null, locale);
					messages = new ArrayList<PdfMessage>();
					messages.add(pdfManager.new PdfMessage(CommonUtil.getMessage("pdf.confirmation.ph.14", params, locale), PDFManager.FONT_DEFAULT));
					table.addCell(PDFManager.createParagraph(messages, user.getSkinId()));
				}
			}
			
			messages = new ArrayList<PdfMessage>();
			if(user.getSkin().isRegulated()){
				messages.add(pdfManager.new PdfMessage(CommonUtil.getMessage("pdf.noncc.withdrawal.ph.8.reg", null, locale), PDFManager.FONT_DEFAULT));
			}else{
				messages.add(pdfManager.new PdfMessage(CommonUtil.getMessage("pdf.noncc.withdrawal.ph.8", null, locale), PDFManager.FONT_DEFAULT));
				
			}
			table.addCell(PDFManager.createParagraph(messages, user.getSkinId()));

			if (withdraw.getTypeId() != TransactionsManagerBase.TRANS_TYPE_MONEYBOOKERS_WITHDRAW) {
				params = new String[1];
				params[0] = CommonUtil.displayAmount(withdraw.getAmount(), user.getCurrencyId());
				messages = new ArrayList<PdfMessage>();
				messages.add(pdfManager.new PdfMessage(CommonUtil.getMessage("pdf.noncc.withdrawal.ph.9", params, locale), PDFManager.FONT_BOLD));
				table.addCell(PDFManager.createParagraph(messages, user.getSkinId()));
				
				messages = new ArrayList<PdfMessage>();
				messages.add(pdfManager.new PdfMessage("\n" + CommonUtil.getMessage("pdf.noncc.withdrawal.ph.10", null,locale) + " " + user.getFirstName() + " " + user.getLastName() + "\n\n", PDFManager.FONT_BOLD));
				table.addCell(PDFManager.createParagraph(messages, user.getSkinId()));
				
				messages = new ArrayList<PdfMessage>();
				messages.add(pdfManager.new PdfMessage(CommonUtil.getMessage("pdf.noncc.withdrawal.ph.11", null, locale) + "\n\n", PDFManager.FONT_BOLD));
				table.addCell(PDFManager.createParagraph(messages, user.getSkinId()));
				
				messages = new ArrayList<PdfMessage>();
				messages.add(pdfManager.new PdfMessage(CommonUtil.getMessage("pdf.noncc.withdrawal.ph.12", null, locale) + "\n\n", PDFManager.FONT_BOLD));
				table.addCell(PDFManager.createParagraph(messages, user.getSkinId()));
				
				messages = new ArrayList<PdfMessage>();
				messages.add(pdfManager.new PdfMessage(CommonUtil.getMessage("pdf.noncc.withdrawal.ph.13", null, locale) + "\n\n", PDFManager.FONT_BOLD));
				table.addCell(PDFManager.createParagraph(messages, user.getSkinId()));
				
				messages = new ArrayList<PdfMessage>();
				messages.add(pdfManager.new PdfMessage(CommonUtil.getMessage("pdf.noncc.withdrawal.ph.14", null, locale) + "\n\n", PDFManager.FONT_BOLD));
				table.addCell(PDFManager.createParagraph(messages, user.getSkinId()));
				
				messages = new ArrayList<PdfMessage>();
				messages.add(pdfManager.new PdfMessage(CommonUtil.getMessage("pdf.noncc.withdrawal.ph.15", null, locale) + "\n\n", PDFManager.FONT_BOLD));
				table.addCell(PDFManager.createParagraph(messages, user.getSkinId()));
				
				messages = new ArrayList<PdfMessage>();
				messages.add(pdfManager.new PdfMessage(CommonUtil.getMessage("pdf.noncc.withdrawal.ph.16", null, locale) + "\n\n", PDFManager.FONT_BOLD));
				table.addCell(PDFManager.createParagraph(messages, user.getSkinId()));
				
				messages = new ArrayList<PdfMessage>();
				messages.add(pdfManager.new PdfMessage(CommonUtil.getMessage("pdf.noncc.withdrawal.ph.17", null, locale) + "\n\n", PDFManager.FONT_BOLD));
				table.addCell(PDFManager.createParagraph(messages, user.getSkinId()));
				
//				messages = new ArrayList<PdfMessage>();
//				messages.add(pdfManager.new PdfMessage(CommonUtil.getMessage("pdf.noncc.withdrawal.ph.18", null, locale) + "\n\n\n", PDFManager.FONT_BOLD));
//				table.addCell(PDFManager.createParagraph(messages, user.getSkinId()));
				
				messages = new ArrayList<PdfMessage>();
				messages.add(pdfManager.new PdfMessage(CommonUtil.getMessage("pdf.noncc.withdrawal.ph.19", null, locale) + "\n\n\n", PDFManager.FONT_BOLD));
				table.addCell(PDFManager.createParagraph(messages, user.getSkinId()));
				
			  /*messages = new ArrayList<PdfMessage>();
				messages.add(pdfManager.new PdfMessage(CommonUtil.getMessage("pdf.noncc.withdrawal.ph.20", null, locale) + "\n\n", PDFManager.FONT_BOLD));
				table.addCell(PDFManager.createParagraph(messages, user.getSkinId()));*/
			} else {
				params = new String[1];
				params[0] = CommonUtil.displayAmount(withdraw.getAmount(), user.getCurrencyId());
				messages = new ArrayList<PdfMessage>();
				messages.add(pdfManager.new PdfMessage(CommonUtil.getMessage("pdf.noncc.withdrawal.ph.21", params, locale), PDFManager.FONT_BOLD));
				table.addCell(PDFManager.createParagraph(messages, user.getSkinId()));
				
				messages = new ArrayList<PdfMessage>();
				messages.add(pdfManager.new PdfMessage("\n" + CommonUtil.getMessage("pdf.noncc.withdrawal.ph.10", null,locale) + " " + user.getFirstName() + " " + user.getLastName() + "\n\n", PDFManager.FONT_BOLD));
				table.addCell(PDFManager.createParagraph(messages, user.getSkinId()));
				
				messages = new ArrayList<PdfMessage>();
				messages.add(pdfManager.new PdfMessage(CommonUtil.getMessage("pdf.noncc.withdrawal.ph.22", null, locale) + " " + withdraw.getMoneybookersEmail() + "\n\n", PDFManager.FONT_BOLD));
				table.addCell(PDFManager.createParagraph(messages, user.getSkinId()));
				
				messages = new ArrayList<PdfMessage>();
				messages.add(pdfManager.new PdfMessage(CommonUtil.getMessage("pdf.noncc.withdrawal.ph.19", null, locale) + "\n\n", PDFManager.FONT_BOLD));
				table.addCell(PDFManager.createParagraph(messages, user.getSkinId()));
			}
			
			document.add(table);
			document.close();
			
			addPaging(filePathSrc, filePathDest);
           
			
			if (fileTmp.delete()) {
				logger.debug("File with name" + fileTmp.getName() + " is deleted successfully");
			} else {
				logger.debug("For file with name" + fileTmp.getName() + ", delete operation is failed");
			}
			
			File file = new File(filePathDest);
			
			// Save the attachment in DB
			Connection con = null;
			long attachmentId = 0;
			try {
				con = getConnection();
				attachmentId = MailBoxUsersDAO.insertAttachmentAsBLOB(con, file);
			} finally {
				closeConnection(con);
			}
			File[]attachments = {file};
			  
			UsersManager.sendTemplate(Long.valueOf(templateId).longValue(), user, attachments, null, null, attachmentId, mailToSupport);
			return null;
		}

		/**
		 * Create and send POA email with PDF attachment
		 *
		 * @param user
		 * @param cardId
		 * @param templateId
		 * @return
		 * @throws Exception
		 */
		public static String sendAndCreatePOAPDF(UserBase user, String cardId,
				String templateId, String mailToSupport) throws Exception {
			Document document = new Document();

			File file = new File(CommonUtil.getProperty(Constants.FILES_PATH)
					+ "POA_" + user.getId() + "_" + cardId + ".pdf");
			FileOutputStream fos = new FileOutputStream(file);
			PdfWriter.getInstance(document, fos);

			// use for new line
			Phrase phrase = new Phrase("");
			phrase.add("\n");

			document.open();

			document.add(getHeader(user.getSkinId()));
			document.add(phrase);

			int langId = user.getSkin().getDefaultLanguageId();
			String langCode = ApplicationData.getLanguage(langId).getCode();
			Locale locale = new Locale(langCode);

			PdfPTable table = new PdfPTable(1);
			table.setWidthPercentage(100);
			ArrayList<PdfMessage> messages = new ArrayList<PdfMessage>();
			PDFManager pdfManager = new PDFManager(); // create pdf manager
			// instance

			String[] params = new String[1];
			params[0] = CommonUtil.getDateFormat(new Date());
			messages = new ArrayList<PdfMessage>();
			messages.add(pdfManager.new PdfMessage(CommonUtil.getMessage(
					"pdf.poa.ph.1", params, locale), PDFManager.FONT_BOLD));
			table.addCell(PDFManager.createParagraph(messages, user.getSkinId()));

			messages = new ArrayList<PdfMessage>();
			messages.add(pdfManager.new PdfMessage(CommonUtil.getMessage(
					"pdf.poa.ph.2", null, locale), PDFManager.FONT_BOLD));
			table.addCell(PDFManager.createParagraph(messages, user.getSkinId()));

			CreditCard c = TransactionsManagerBase.getCreditCard(new BigDecimal(cardId));
			params = new String[6];
			params[0] = c.getHolderName();
			params[1] = user.getFirstName() + " " + user.getLastName();
			if (user.getSkinId() != Skin.SKIN_ETRADER) {
				params[2] = String.valueOf(user.getId());
			} else {
				params[2] = c.getHolderIdNum();
			}
			params[3] = c.getType().getName();
			params[4] = c.getCcNumberLast4();
			params[5] = user.getIdNum(); // for etrader
			messages = new ArrayList<PdfMessage>();
			messages.add(pdfManager.new PdfMessage(CommonUtil.getMessage(
					"pdf.poa.ph.3", params, locale), PDFManager.FONT_DEFAULT));
			table.addCell(PDFManager.createParagraph(messages, user.getSkinId()));
			params = new String[3];
			params[0] = c.getHolderName();
			params[1] = user.getFirstName() + " " + user.getLastName();
			params[2] = c.getType().getName();

			messages = new ArrayList<PdfMessage>();
			messages.add(pdfManager.new PdfMessage(CommonUtil.getMessage(
					"pdf.poa.ph.4", params, locale), PDFManager.FONT_DEFAULT));
			table.addCell(PDFManager.createParagraph(messages, user.getSkinId()));

			messages = new ArrayList<PdfMessage>();
			messages.add(pdfManager.new PdfMessage(CommonUtil.getMessage(
					"pdf.poa.ph.5", null, locale), PDFManager.FONT_BOLD));
			table.addCell(PDFManager.createParagraph(messages, user.getSkinId()));

			messages = new ArrayList<PdfMessage>();
			messages.add(pdfManager.new PdfMessage(CommonUtil.getMessage(
					"pdf.poa.ph.6", null, locale)
					+ c.getHolderName(), PDFManager.FONT_BOLD));
			table.addCell(PDFManager.createParagraph(messages, user.getSkinId()));

			messages = new ArrayList<PdfMessage>();
			messages.add(pdfManager.new PdfMessage(CommonUtil.getMessage(
					"pdf.poa.ph.7", null, locale), PDFManager.FONT_BOLD));
			table.addCell(PDFManager.createParagraph(messages, user.getSkinId()));

			messages = new ArrayList<PdfMessage>();
			messages.add(pdfManager.new PdfMessage(CommonUtil.getMessage(
					"pdf.poa.ph.8", null, locale), PDFManager.FONT_BOLD));
			table.addCell(PDFManager.createParagraph(messages, user.getSkinId()));

			document.add(table);
			document.close();

			// Save the attachment in DB
			Connection con = null;
			long attachmentId = 0;
			try {
				con = getConnection();
				attachmentId = MailBoxUsersDAO.insertAttachmentAsBLOB(con, file);
			} finally {
				closeConnection(con);
			}
			File[]attachments = {file};
			  
			UsersManager.sendTemplate(Long.valueOf(templateId).longValue(), user,
					attachments, c.getCcNumberLast4(), null, attachmentId, mailToSupport);
			return null;
		}
		
		
		public static String sendAndCreatePOAPDFReg(UserBase user, String cardId, String templateId, String mailToSupport) throws Exception {
			Document document = new Document();
			String tempFilePath = CommonUtil.getProperty(Constants.FILES_PATH) + "POA_TEMP" + user.getId() + "_" + cardId + ".pdf";
			String paoFilePath = CommonUtil.getProperty(Constants.FILES_PATH) + "POA_" + user.getId() + "_" + cardId + ".pdf";
			String waterMarkImgPath = "http://picbase.etrader.co.il/images/ouroboros_logo_symbol_small.png";
			File tempFile = new File(tempFilePath);
			FileOutputStream fos = new FileOutputStream(tempFile);
			PdfWriter.getInstance(document, fos);

			// use for new line
			Phrase phrase = new Phrase("");
			phrase.add("\n");

			document.open();

			document.add(getHeaderPAOReg(user.getSkinId()));
			document.add(phrase);

			int langId = user.getSkin().getDefaultLanguageId();
			String langCode = ApplicationData.getLanguage(langId).getCode();
			Locale locale = new Locale(langCode);

			PdfPTable table = new PdfPTable(1);
			table.setWidthPercentage(100);
			ArrayList<PdfMessage> messages = new ArrayList<PdfMessage>();
			PDFManager pdfManager = new PDFManager(); // create pdf manager
			// instance

			messages = new ArrayList<PdfMessage>();
			messages.add(pdfManager.new PdfMessage(CommonUtil.getMessage(
					"pdf.poa.reg.ph.1", null, locale), PDFManager.FONT_BOLD));
			table.addCell(PDFManager.createParagraph(messages, user.getSkinId()));
			
			String company = CommonUtil.getMessage("pdf.poa.reg.ph.AO", null, locale);
			if(CommonUtil.isCopyopWriter(user.getWriterId())){
				company = CommonUtil.getMessage("pdf.poa.reg.ph.CO", null, locale);
			}
			CreditCard cc = TransactionsManagerBase.getCreditCard(new BigDecimal(cardId));
			Date firstDepositWithCC = TransactionManager.getFirstDepositWithCC(user.getId(), Long.parseLong(cardId));
			String[] params = new String[6];
			params[0] = CommonUtil.getDateFormat(new Date());
			params[1] = user.getFirstName() + " " + user.getLastName();
			params[2] = company;
			params[3] = String.valueOf(user.getId());
			params[4] = user.getUserName();
			params[5] = cc.getCcNumberLast4();			
			messages = new ArrayList<PdfMessage>();
			messages.add(pdfManager.new PdfMessage(CommonUtil.getMessage("pdf.poa.reg.ph.2", params, locale), PDFManager.FONT_DEFAULT));
			table.addCell(PDFManager.createParagraph(messages, user.getSkinId()));
			
			
			messages = new ArrayList<PdfMessage>();
			messages.add(pdfManager.new PdfMessage(CommonUtil.getMessage("pdf.poa.reg.ph.3", null, locale), PDFManager.FONT_DEFAULT));
			table.addCell(PDFManager.createParagraph(messages, user.getSkinId()));

			
			String companyGMUrl = CommonUtil.getMessage("pdf.poa.reg.ph.gm.AO", null, locale);
			if(CommonUtil.isCopyopWriter(user.getWriterId())){
				companyGMUrl = CommonUtil.getMessage("pdf.poa.reg.ph.gm.CO", null, locale);
			}
			
			messages = new ArrayList<PdfMessage>();
			messages.add(pdfManager.new PdfMessage(CommonUtil.getMessage("pdf.poa.reg.ph.4", null, locale), PDFManager.FONT_DEFAULT));
			messages.add(pdfManager.new PdfMessage(companyGMUrl, PDFManager.FONT_UNDERLINE_BLUE));
			messages.add(pdfManager.new PdfMessage(CommonUtil.getMessage("pdf.poa.reg.ph.4b", null, locale), PDFManager.FONT_DEFAULT));	
			
			table.addCell(PDFManager.createParagraph(messages, user.getSkinId()));
			
			messages = new ArrayList<PdfMessage>();
			messages.add(pdfManager.new PdfMessage(CommonUtil.getMessage("pdf.poa.reg.ph.5", null, locale), PDFManager.FONT_DEFAULT));
			table.addCell(PDFManager.createParagraph(messages, user.getSkinId()));
			
			messages = new ArrayList<PdfMessage>();
			messages.add(pdfManager.new PdfMessage(CommonUtil.getMessage("pdf.poa.reg.ph.6", null, locale), PDFManager.FONT_DEFAULT));
			table.addCell(PDFManager.createParagraph(messages, user.getSkinId()));
			
			messages = new ArrayList<PdfMessage>();
			messages.add(pdfManager.new PdfMessage(CommonUtil.getMessage("pdf.poa.reg.ph.7", null, locale), PDFManager.FONT_BOLD));
			table.addCell(PDFManager.createParagraph(messages, user.getSkinId()));
			
			messages = new ArrayList<PdfMessage>();
			messages.add(pdfManager.new PdfMessage(CommonUtil.getMessage("pdf.poa.reg.ph.8", null, locale), PDFManager.FONT_DEFAULT));
			table.addCell(PDFManager.createParagraph(messages, user.getSkinId()));
			
			messages = new ArrayList<PdfMessage>();
			messages.add(pdfManager.new PdfMessage(CommonUtil.getMessage("pdf.poa.reg.ph.9", null, locale), PDFManager.FONT_DEFAULT));
			table.addCell(PDFManager.createParagraph(messages, user.getSkinId()));

			messages = new ArrayList<PdfMessage>();
			messages.add(pdfManager.new PdfMessage(CommonUtil.getMessage("pdf.poa.reg.ph.10", null, locale), PDFManager.FONT_BOLD_SMALL_CENTR));
			table.addCell(PDFManager.createParagraph(messages, user.getSkinId()));			
			document.add(table);
			document.close();
			
			TransparentWatermark trWater = new TransparentWatermark(tempFilePath, paoFilePath, waterMarkImgPath);
			File file = trWater.putWaterMArkImg();
			tempFile.delete();

			// Save the attachment in DB
			Connection con = null;
			long attachmentId = 0;
			try {
				con = getConnection();
				attachmentId = MailBoxUsersDAO.insertAttachmentAsBLOB(con, file);
			} finally {
				closeConnection(con);
			}
			File[]attachments = {file};
			
			UsersManager.sendTemplate(Long.valueOf(templateId).longValue(), user,
					attachments, cc.getCcNumberLast4(), CommonUtil.getDateTimeFormat(firstDepositWithCC), attachmentId, mailToSupport);
			return null;
		}
		
	   private static void addPaging(String src, String dest) throws IOException, DocumentException {
	    	
	        PdfReader reader = new PdfReader(src);
	        int n = reader.getNumberOfPages();
	        PdfStamper stamper = new PdfStamper(reader, new FileOutputStream(dest));
	        PdfContentByte pagecontent;
	        for (int i = 0; i < n;) {
	            pagecontent = stamper.getOverContent(++i);
	            com.itextpdf.text.Rectangle rect = new Rectangle(36, 54, 559, 788);
	            ColumnText.showTextAligned(pagecontent, Element.ALIGN_CENTER, new Phrase(String.format("%s/%s", i, n)), (rect.getLeft() + rect.getRight()) / 2, rect.getBottom() - 18, 0);
	        }
	        stamper.close();
	        reader.close();
	    }

	}
