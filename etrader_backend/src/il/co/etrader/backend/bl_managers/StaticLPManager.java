package il.co.etrader.backend.bl_managers;

import il.co.etrader.backend.bl_vos.StaticLandingPage;
import il.co.etrader.backend.bl_vos.StaticLandingPageLanguage;
import il.co.etrader.backend.dao_managers.StaticLPDAO;
import il.co.etrader.bl_vos.MarketingCombination;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.faces.model.SelectItem;

import com.anyoption.common.managers.BaseBLManager;

/**
 * @author EranL
 *
 */
public class StaticLPManager extends BaseBLManager {

	public static final long STATIC_LANDING_PAGE_PATH_ET 				= 1;
	public static final long STATIC_LANDING_PAGE_PATH_AO_NONE_REGULATED = 2;
	public static final long STATIC_LANDING_PAGE_PATH_AO_REGULATED 		= 3;
	public static final long STATIC_LANDING_PAGE_PATH_MO				= 4; // MasterOption
	
	public static ArrayList<StaticLandingPageLanguage> getStaticLandigPages(long type, long languageId, long staticLandingPageId, 
			long writerIdForSkin, String staticLandingPageName, long staticLandingPagePathId) throws SQLException {
		Connection con = getConnection();
	  	try {
	  		return StaticLPDAO.getStaticLandigPages(con, type, languageId, staticLandingPageId, writerIdForSkin, staticLandingPageName, staticLandingPagePathId);
	  	} finally {
	  		closeConnection(con);
	  	}
	}

	public static ArrayList<SelectItem> getStaticLandingPages() throws SQLException {
		Connection con = getConnection();
	  	try {
	  		return StaticLPDAO.getStaticLandingPages(con);
	  	} finally {
	  		closeConnection(con);
	  	}
	}
	
	public static ArrayList<SelectItem> getStaticLandingPagesByWriterSkinIdSI(long writerIdForSkin) throws SQLException {
		Connection con = getConnection();
	  	try {
	  		return StaticLPDAO.getStaticLandingPagesByWriterSkinIdSI(con, writerIdForSkin);
	  	} finally {
	  		closeConnection(con);
	  	}
	}

	public static ArrayList<MarketingCombination> getStaticCombination(long combId, long skinId, long landingPage, long writerIdForSkin, long fetchCount, long pathId) throws SQLException {
		Connection con = getSecondConnection();
	  	try {
	  		return StaticLPDAO.getStaticCombination(con, combId, skinId, landingPage, writerIdForSkin, fetchCount, pathId);
	  	} finally {
	  		closeConnection(con);
	  	}
	}
	
	public static void insertStaticLandingPage(StaticLandingPage slp) throws SQLException {
		Connection con = getConnection();
	  	try {
	  		StaticLPDAO.insertStaticLandingPage(con, slp);
	  	} finally {
	  		closeConnection(con);
	  	}
	}
	
	public static void insertStaticLandingPageLanguage(StaticLandingPageLanguage slpl) throws SQLException {
		Connection con = getConnection();
	  	try {
	  		StaticLPDAO.insertStaticLandingPageLanguage(con, slpl);
	  	} finally {
	  		closeConnection(con);
	  	}
	}
	
	/**
	 * Get static lp names SI
	 * @param landingPageType
	 * @param pathId
	 * @return ArrayList<SelectItem>
	 * @throws SQLException
	 */
	public static ArrayList<SelectItem> getStaticLpNamesSI(long landingPageType, long pathId) throws SQLException {
		Connection con = getSecondConnection();
		try {
			return StaticLPDAO.getStaticLpNamesSI(con, landingPageType, pathId);
		} finally {
			closeConnection(con);
		}
	}
	
}