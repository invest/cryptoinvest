package il.co.etrader.backend.bl_managers;

import java.sql.Connection;
import java.sql.SQLException;

import javax.faces.context.FacesContext;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.Transaction;
import com.anyoption.common.bl_vos.ChargeBack;
import com.anyoption.common.daos.ChargeBacksDAO;
import com.anyoption.common.daos.GeneralDAO;
import com.anyoption.common.managers.ChargeBacksManagerBase;

import il.co.etrader.backend.bl_vos.User;
import il.co.etrader.backend.util.Constants;
import il.co.etrader.backend.util.Utils;
import il.co.etrader.dao_managers.TransactionsDAOBase;

public class ChargeBacksManager extends ChargeBacksManagerBase {
	private static Logger log = Logger.getLogger(ChargeBacksManager.class);

	public static void insertChargeBack(ChargeBack cb) throws Exception {
		Connection con = getConnection();
		long cbTranId = cb.getTransactionId();

		con.setAutoCommit(false);
		try {
			// first insert cb issue.
			IssuesManager.insertChargeBackIssue(con,cb);

			// only if cb issue  was successfully inserted, insert cb
			ChargeBacksDAO.insert(con, cb);

			TransactionsDAOBase.updateTransactionChargeBack(con, cbTranId, cb.getId(), cb.isChbState());

			// Only if it's a chargeback deduct user's balance.
			if (cb.isChbState()){
				Transaction trans = TransactionsDAOBase.getById(con, cbTranId);
				long negFixTransactionId = UsersManager.addToBalanceNonNeg(	con, trans.getUserId(), -trans.getAmount(), trans.getRate(),
																			Utils.getUser(trans.getUserId()).getUtcOffset());
				GeneralDAO.insertBalanceLog(con, AdminManager.getWriterId(), trans
						.getUserId(), Constants.TABLE_TRANSACTIONS, trans.getId(),
						Constants.LOG_BALANCE_CHARGE_BACK, Utils.getUser(trans.getUserId()).getUtcOffset());
				if (negFixTransactionId > 0) {
					GeneralDAO.insertBalanceLog(con, AdminManager.getWriterId(), trans.getUserId(), Constants.TABLE_TRANSACTIONS,
												trans.getId(), Constants.LOG_BALANCE_FIX_NEGATIVE_BALANCE_DEPOSIT,
												Utils.getUser(trans.getUserId()).getUtcOffset());
				}

				FacesContext context = FacesContext.getCurrentInstance();
				User user = (User)context.getApplication().createValueBinding(Constants.BIND_USER).getValue(context);
				if (user.isSupportSelected() || user.isRetentionSelected() || user.isRetentionMSelected()) {
					UsersManager.loadUserStripFields(user, true, null);
				}
			}
			con.commit();
		} catch (SQLException e) {
    		log.error("Exception inserting chargback. ", e);
    		try {
    			con.rollback();
    		} catch (Throwable it) {
    			log.error("Can't rollback.", it);
    		}
    		throw e;
    	}finally {
    		try {
    			con.setAutoCommit(true);
    		} catch (Exception e) {
    			log.error("Can't set back to autocommit.", e);
    		}
			closeConnection(con);
		}
	}

	public static void updateChargeBack(ChargeBack cb) throws Exception {
		Connection con = getConnection();
		long cbTranId = cb.getTransactionId();

		con.setAutoCommit(false);
		try {
			// first insert cb issue action.
			IssuesManager.insertChargeBackIssueAction(con,cb);

			// only if cb issue action was successfully inserted, update cb
			ChargeBacksDAO.update(con, cb);

			// Only if it's a chargeback deduct user's balance.
			if (cb.isStateChanged()){
				TransactionsDAOBase.updateTransactionChargeBack(con, cbTranId, cb.getId(),cb.isChbState());
				Transaction trans = TransactionsDAOBase.getById(con, cbTranId);
				long negFixTransactionId = UsersManager.addToBalanceNonNeg(	con, trans.getUserId(), -trans.getAmount(), trans.getRate(),
																			Utils.getUser(trans.getUserId()).getUtcOffset());
				GeneralDAO.insertBalanceLog(con, AdminManager.getWriterId(), trans
						.getUserId(), Constants.TABLE_TRANSACTIONS, trans.getId(),
						Constants.LOG_BALANCE_CHARGE_BACK, Utils.getUser(trans.getUserId()).getUtcOffset());
				if (negFixTransactionId > 0) {
					GeneralDAO.insertBalanceLog(con, AdminManager.getWriterId(), trans.getUserId(), Constants.TABLE_TRANSACTIONS,
												trans.getId(), Constants.LOG_BALANCE_FIX_NEGATIVE_BALANCE_DEPOSIT,
												Utils.getUser(trans.getUserId()).getUtcOffset());
				}
				FacesContext context = FacesContext.getCurrentInstance();
				User user = (User)context.getApplication().createValueBinding(Constants.BIND_USER).getValue(context);
				if (user.isSupportSelected() || user.isRetentionSelected() || user.isRetentionMSelected()) {
					UsersManager.loadUserStripFields(user, true, null);
				}
			}
			con.commit();
		} catch (SQLException e) {
    		log.error("Exception updating chargback. ", e);
    		try {
    			con.rollback();
    		} catch (Throwable it) {
    			log.error("Can't rollback.", it);
    		}
    		throw e;
    	}finally {
    		try {
    			con.setAutoCommit(true);
    		} catch (Exception e) {
    			log.error("Can't set back to autocommit.", e);
    		}
			closeConnection(con);
		}
	}

}
