package il.co.etrader.backend.bl_managers;

import il.co.etrader.backend.dao_managers.MailBoxUsersDAO;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import com.anyoption.common.beans.MailBoxTemplate;
import com.anyoption.common.beans.MailBoxUser;
import com.anyoption.common.managers.BaseBLManager;

public class MailBoxUsersManager extends BaseBLManager {

    private static final Logger logger = Logger.getLogger(MailBoxUsersManager.class);

	public static ArrayList<MailBoxUser> getAllEmails(long userId) throws SQLException {
		Connection con = getConnection();
		try {
			return MailBoxUsersDAO.getAll(con, userId);
		} finally {
			closeConnection(con);
		}
	}

	/**
	 * Insert new email to user inbox
	 * @param email
	 * @throws SQLException
	 */
    public static void insertEmail(MailBoxUser email) throws SQLException {
        if (logger.isEnabledFor(Level.DEBUG)) {
            String ls = System.getProperty("line.separator");
            logger.log(Level.DEBUG,"Insert mailBoxEmail: " + ls + email.toString());
         }
        Connection con = getConnection();
        try {
              MailBoxUsersDAO.insert(con, email);
        } finally {
            closeConnection(con);
        }
    }

    public static void insertEmail(MailBoxTemplate template, long writerId, long promotion_id) throws SQLException{
    	Connection conn = null;
    	try{
    		conn = getConnection();	   
    		//conn = DriverManager.getConnection ("jdbc:oracle:thin:@dbtestbg.anyoption.com:1521/etrader", "etrader_web", "w3tr4d3r");
    		MailBoxUser email = new MailBoxUser();
	    	email.setWriterId(writerId);
	    	email.setTemplateId(template.getId());
	    	email.setSenderId(template.getSenderId());
			email.setIsHighPriority(template.getIsHighPriority());
			email.setSubject(template.getSubject());
			email.setTemplate(template);
			email.setPopupTypeId(template.getPopupTypeId());
	
	    	MailBoxUsersDAO.insertEmailInBatch(conn, email, promotion_id);
	    	
    	} finally {
    		closeConnection(conn);
    	}
    }
}


//    	
//    	
//    	
//    	
//   	 for(UserBase user : usersList){
//   		 	email.setWriterId(writerId);
//	    	email.setUserId(user.getId());
//	    	email.setTemplateId(template.getId());
//	    	email.setSenderId(template.getSenderId());
//			email.setIsHighPriority(template.getIsHighPriority());
//			email.setSubject(template.getSubject());
//			email.setTemplate(template);
//			email.setPopupTypeId(template.getPopupTypeId());
//
//			if (logger.isEnabledFor(Level.DEBUG)) {
//	            String ls = System.getProperty("line.separator");
//	            logger.log(Level.DEBUG,"Insert mailBoxEmail: " + ls + email.toString());
//	         } try {
//	        	 MailBoxUsersDAO.insert(conn, email);
//	        	 PromotionsDAO.updatePromotionEntriesStatus(conn, Constants.PROMOTION_STATUS_ID_SENDED, user.getId(), promotion_id);
//			} catch (SQLException eq) {
//				eq.printStackTrace();
//				logger.warn("Faild to send email to user id " + user.getId(), eq);
//			}
//			} try {
//		  		conn.commit();
//		  	} catch (SQLException sqle) {
//		       try {
//		          conn.rollback();
//		      } catch (SQLException sqlie) {
//		          logger.error("Failed to rollback.", sqlie);
//		      }
//		      throw sqle;
//			  }finally{
//			  	conn.setAutoCommit(true);
//			  	closeConnection(conn);
//			  	}
//			  }
//    }