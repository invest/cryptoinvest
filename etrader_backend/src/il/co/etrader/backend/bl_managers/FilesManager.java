package il.co.etrader.backend.bl_managers;



import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.File;
import com.anyoption.common.beans.FileType;
import com.anyoption.common.beans.FilesAdditionalInfo;
import com.anyoption.common.beans.FilesKeesingMatch;
import com.anyoption.common.beans.User;
import com.anyoption.common.managers.FilesManagerBase;
import com.anyoption.common.managers.UserRegulationManager;
import com.anyoption.common.managers.UsersManagerBase;
import com.anyoption.common.service.userdocuments.UserDocumentsDAO;
import com.invest.common.documents.DocumentsHelper;
import com.invest.common.email.beans.EmailTemplate;
import com.invest.common.email.factories.EmailProviderFactory;
import com.invest.common.email.factories.EmailTemplateFactory;

import il.co.etrader.backend.dao_managers.FilesDAO;
import il.co.etrader.backend.util.Utils;

public class FilesManager extends FilesManagerBase {
	private static final Logger log = Logger.getLogger(FilesDAO.class);	
	private static HashMap<Long, FileType> fileTypes;
	private static HashMap<Long, String> rejectReasons;
	private static HashMap<Long, ArrayList<HashMap<Long, String>>> fileRejectReasons;
	private static HashMap<Long, FilesAdditionalInfo> filesAdditionalInfo;
	private static final Integer FILE_KEESING_STATUS_DONE = 3;
	
	public static final int FILE_REJECT_REASON_OTHER = 44;
	public static final String FILTER_FOR_REJECT_REASONST = "rejectReasons";

	public static HashMap<Long, FileType> getFileTypes() throws SQLException {
		if(fileTypes == null){
			Connection con = getConnection();
			try {
				fileTypes = FilesDAO.getFileTypes(con);
				setAdditionalInfo();
			} finally {
				closeConnection(con);
			}
		}
		return fileTypes;
	}

	private static void setAdditionalInfo() throws SQLException {
		for (Entry<Long, FileType> entry : fileTypes.entrySet()) {
		    Long fileTypeId = entry.getKey();
		    FileType value = entry.getValue();
		    if(getFilesAdditionalInfo().containsKey(fileTypeId)){
			    value.setFilesAdditionalInfo(getFilesAdditionalInfo().get(fileTypeId));
		    }
		}
	}

	public static HashMap<Long, String> getRejectReasons() throws SQLException {
		if(rejectReasons == null){
			Connection con = getConnection();
			try {
				rejectReasons = FilesDAO.getRejectReasons(con);
			} finally {
				closeConnection(con);
			}
		}
		return rejectReasons;
	}

	public static HashMap<Long, ArrayList<HashMap<Long, String>>> getFileRejectReasons() throws SQLException {		
		if(fileRejectReasons == null){
			Connection con = getConnection();
			try {
				fileRejectReasons = FilesDAO.getFileRejectReasons(con);
			} finally {
				closeConnection(con);
			}
		}
		return fileRejectReasons;
	}
	
	public static File getFileById(long fileId, Locale locale, long previousNext) throws SQLException {
	    Connection con = getConnection();
		try {
			 File f = FilesDAO.getFileById(con, fileId, locale, previousNext);
			 if(f != null && f.getKsStatusId() == FILE_KEESING_STATUS_DONE){
				 f.setFilesKeesingMatch(FilesDAO.getFilesKeesingMatch(con, f.getId()));
				 log.debug("loaded Keesing Match");
				 f.setFilesKeesingResponse(FilesDAO.getFilesKeesingResponse(con, f.getId()));
				 log.debug("loaded Keesing Response");
			 }
			 return f;
		} finally {
			closeConnection(con);
		}
	}
	
	public static void updateFileKeesingMatch(FilesKeesingMatch fkm, long writerId) throws SQLException {
	    Connection con = getConnection();
		try {
			FilesDAO.updateFileKeesingMatch(con, fkm, writerId);	
			log.debug("Update KS Match");
		} finally {
			closeConnection(con);
		}
	}

	public static HashMap<Long, FilesAdditionalInfo> getFilesAdditionalInfo() throws SQLException {
		if(filesAdditionalInfo == null){
			Connection con = getConnection();
			try {
				filesAdditionalInfo =  FilesDAO.getFilesAdditionalInfo(con);
			} finally {
				closeConnection(con);
			}
		}
		return filesAdditionalInfo;
	}
	
	public static long insertFileBE(File file) {
		long res = 0l;
		Connection con = null;
		try {
			con = getConnection();
			con.setAutoCommit(false);
			res = FilesDAO.insertFileBE(con, file);
			UserDocumentsDAO.setCurrentFlagDocs(con, file.getUserId(), file.getFileTypeId(), file.getCcId());
			con.commit();
		} catch (SQLException e) {
			rollback(con);
			log.error("Error inserting file " + e);
		} finally {
			setAutoCommitBack(con);
			closeConnection(con);
		}
		
		if(file.getFileTypeId() == FileType.CNMV_FILE){
			User user = null;
			try {
				user = UsersManagerBase.getUserById(file.getUserId());
				UserRegulationManager.CNMVFileUnSuspend(user, file.getUploaderId());
			} catch (SQLException e) {			
				log.error("When CNMVFileUnSuspend " + e);
			}			
		}		
		return res;
	}
	
	public static boolean updateFileBE(File file) {
		Connection con = null;
		try {
			con = getConnection();
			il.co.etrader.dao_managers.FilesDAOBase.updateBE(con, file);
			if(file.getFilesKeesingMatch() != null && file.getFilesKeesingMatch().getKsResponseId() != null){
				updateFileKeesingMatch(file.getFilesKeesingMatch(), file.getWriterId());
			}
			return true;
		} catch (SQLException e) {
			log.debug("Cannot update file", e);
			return false;
		} finally {
			closeConnection(con);
		}
	}

	public static List<File> getFiles(long transactionId) {
		Connection con = null;
		try {
			con = getConnection();
			return FilesDAO.getFiles(con, transactionId);
		} catch (SQLException e) {
			log.debug("Unable to load files for transaction " + transactionId, e);
			return null;
		} finally {
			closeConnection(con);
		}
	}

	public static void checkDocumentsStatus(User user, File file) throws SQLException {
		if (!isCheckDocumentsStatus(file.getFileTypeId())) {
			return;
		}
		if (file.getFileStatusId() == File.STATUS_REJECTED) {
			String rejectReason = Utils.getMessage(FilesManager.getRejectReasons().get(new Long(file.getRejectReason())), null);
			EmailTemplate template = EmailTemplateFactory.getDocumentRejectedTemplate(	user.getId(), Utils.getConfig("email.from"),
																						user.getEmail(), null, user.getFirstName(),
																						file.getFileTypeName(), rejectReason,
																						Utils.getConfig("email.template.homepage"),
																						Utils.getConfig("email.support"));
			EmailProviderFactory.getEmailProvider().scheduleTemplateEmail(template);
		} else {
			Map<Long, File> documents = getDocuments(user.getId());
			if (DocumentsHelper.isPassportApproved(file.getFileTypeId(), documents)
					|| DocumentsHelper.isIdApproved(file.getFileTypeId(), documents)) {
				EmailTemplate template = EmailTemplateFactory.getDocumentsApprovedTemplate(	user.getId(), Utils.getConfig("email.from"),
																							user.getEmail(), null, user.getFirstName(),
																							Utils.getConfig("email.template.homepage"),
																							Utils.getConfig("email.support"));
				EmailProviderFactory.getEmailProvider().scheduleTemplateEmail(template);
			} else if (!file.isApproved() && (DocumentsHelper.isPassReceived(file.getFileTypeId(), documents)
												|| DocumentsHelper.isIdReceived(file.getFileTypeId(), documents))) {
				EmailTemplate template = EmailTemplateFactory.getDocumentsReceivedTemplate(	user.getId(), Utils.getConfig("email.from"),
																							user.getEmail(), null, user.getFirstName(),
																							Utils.getConfig("email.template.homepage"),
																							Utils.getConfig("email.support"));
				EmailProviderFactory.getEmailProvider().scheduleTemplateEmail(template);
			}
		}
	}
}