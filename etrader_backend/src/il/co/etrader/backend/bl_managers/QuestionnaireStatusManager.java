package il.co.etrader.backend.bl_managers;

import il.co.etrader.backend.bl_vos.QuestionnaireStatus;
import il.co.etrader.backend.dao_managers.QuestionnaireStatusDAO;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;

import com.anyoption.common.managers.BaseBLManager;

public class QuestionnaireStatusManager extends BaseBLManager {
    
    public static ArrayList<QuestionnaireStatus> getQuestionnaireNotFilled(Long userId, String userName, int interested, int daysCondition, int reached, int firstRow, int rowsPerPage, int qualified, int daysConditionQualified, int skinId) throws SQLException {
        Connection con = getConnection();
        try {
            return QuestionnaireStatusDAO.getQuestionnaireNotFilled(con, userId, userName, interested, daysCondition, reached, firstRow, rowsPerPage, qualified, daysConditionQualified, skinId);
        } finally {
            closeConnection(con);
        }
    }
    
    public static int getQuestionnaireNotFilledCount(Long userId, String userName, int interested, int daysCondition, int reached, int qualified, int daysConditionQualified, int skinId) throws SQLException {
        Connection con = getConnection();
        try {
            return QuestionnaireStatusDAO.getQuestionnaireNotFilledCount(con, userId, userName, interested, daysCondition, reached, qualified, daysConditionQualified, skinId);
        } finally {
            closeConnection(con);
        }
    }
    
    public static ArrayList<QuestionnaireStatus> getQuestionnaireFilledIncorrectly(Long userId, String userName, int interested, int daysCondition, int reached, int firstRow, int rowsPerPage, int qualified, int daysConditionQualified, int skinId) throws SQLException {
        Connection con = getConnection();
        try {
            return QuestionnaireStatusDAO.getQuestionnaireFilledIncorrectly(con, userId, userName, interested, daysCondition, reached, firstRow, rowsPerPage, qualified, daysConditionQualified, skinId);
        } finally {
            closeConnection(con);
        }
    }
    
    public static int getQuestionnaireFilledIncorrectlyCount(Long userId, String userName, int interested, int daysCondition, int reached, int qualified, int daysConditionQualified, int skinId) throws SQLException {
        Connection con = getConnection();
        try {
            return QuestionnaireStatusDAO.getQuestionnaireFilledIncorrectlyCount(con, userId, userName, interested, daysCondition, reached, qualified, daysConditionQualified, skinId);
        } finally {
            closeConnection(con);
        }
    }

}
