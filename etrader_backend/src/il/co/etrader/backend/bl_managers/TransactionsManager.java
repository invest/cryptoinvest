package il.co.etrader.backend.bl_managers;

import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.bouncycastle.crypto.CryptoException;

import com.anyoption.common.beans.Bonus;
import com.anyoption.common.beans.CreditCard;
import com.anyoption.common.beans.PaymentMethod;
import com.anyoption.common.beans.Transaction;
import com.anyoption.common.beans.base.BonusUsers;
import com.anyoption.common.beans.base.Currency;
import com.anyoption.common.beans.base.UserRegulationBase;
import com.anyoption.common.bl_vos.BonusCurrency;
import com.anyoption.common.bl_vos.PopulationEntryBase;
import com.anyoption.common.bl_vos.TransactionReport;
import com.anyoption.common.bl_vos.WireBase;
import com.anyoption.common.clearing.ClearingException;
import com.anyoption.common.clearing.ClearingInfo;
import com.anyoption.common.clearing.ClearingProvider;
import com.anyoption.common.daos.ChargeBacksDAO;
import com.anyoption.common.daos.GeneralDAO;
import com.anyoption.common.daos.WiresDAOBase;
import com.anyoption.common.managers.UserRegulationManager;
import com.anyoption.common.service.CommonJSONService;
import com.anyoption.common.util.AnyOptionCreditCardValidator;

import il.co.etrader.backend.bl_vos.CaptureDailyInfo;
import il.co.etrader.backend.bl_vos.DepositsSummary;
import il.co.etrader.backend.bl_vos.FeeCurrencyMap;
import il.co.etrader.backend.bl_vos.ReroutingLimit;
import il.co.etrader.backend.bl_vos.User;
import il.co.etrader.backend.dao_managers.TransactionsDAO;
import il.co.etrader.backend.dao_managers.UsersDAO;
import il.co.etrader.backend.mbeans.DepositForm;
import il.co.etrader.backend.mbeans.WithdrawForm;
import il.co.etrader.backend.util.Constants;
import il.co.etrader.backend.util.Utils;
import il.co.etrader.bl_managers.BonusManagerBase;
import il.co.etrader.bl_managers.ClearingManager;
import il.co.etrader.bl_managers.PopulationsManagerBase;
import il.co.etrader.bl_managers.TransactionsManagerBase;
import il.co.etrader.bl_vos.Cheque;
import il.co.etrader.bl_vos.Skins;
import il.co.etrader.bl_vos.UserBase;
import il.co.etrader.clearing.EnvoyClearingProvider;
import il.co.etrader.clearing.EnvoyInfo;
import il.co.etrader.dao_managers.ChequeDAO;
import il.co.etrader.dao_managers.CreditCardsDAO;
import il.co.etrader.dao_managers.LimitsDAO;
import il.co.etrader.dao_managers.TransactionTypesDAO;
import il.co.etrader.dao_managers.TransactionsDAOBase;
import il.co.etrader.dao_managers.UsersDAOBase;
import il.co.etrader.util.CommonUtil;
import il.co.etrader.util.ConstantsBase;

public class TransactionsManager extends TransactionsManagerBase {
	
	public static Map<Long, String> transactionTypes;

	private static final Logger log = Logger
			.getLogger(TransactionsManager.class);

	public static com.anyoption.common.bl_vos.ChargeBack getChargeBack(long id) throws SQLException {
		Connection con = getConnection();
		try {
			return ChargeBacksDAO.getById(con, id);
		} finally {
			closeConnection(con);
		}
	}

	public static User getById(long id) throws SQLException {
		Connection con = getConnection();
		try {
			return UsersDAO.getById(con, id);
		} finally {
			closeConnection(con);
		}
	}

	public static Cheque getChequeDetails(long id) throws SQLException {
		Connection con = getSecondConnection();
		try {
			return ChequeDAO.getById(con, id);
		} finally {
			closeConnection(con);
		}
	}

	public static long getTransactionsSummary(long userId, String types,
			String status, boolean isUSD) throws SQLException {
		Connection con = getSecondConnection();
		try {
			return TransactionsDAO.getSummary(con, userId, types, status, isUSD);
		} finally {
			closeConnection(con);
		}
	}

	public static String getLastSalesDepositDetails(long userId) throws SQLException {
		Connection con = getSecondConnection();
		try {
			return TransactionsDAO.getLastSalesDeposit(con, userId);
		} finally {
			closeConnection(con);
		}
	}

	public static Transaction insertWireDeposit(WireBase w, long userId)
			throws SQLException {

		if (log.isEnabledFor(Level.INFO)) {
			String ls = System.getProperty("line.separator");
			log.log(Level.INFO, "Insert wire deposit: " + ls + w.toString());
		}

		FacesContext context = FacesContext.getCurrentInstance();

        long writerId = AdminManager.getWriterId();
		Transaction tran = null;
		Connection con = getConnection();
		try {
			UserBase user = UsersDAO.getById(con, userId);

			con.setAutoCommit(false);

			if(CommonUtil.isHebrewSkin(user.getSkinId())){
				WiresDAOBase.insert(con, w);
			} else { //for anyoption
				WiresDAOBase.insertBankWire(con, w);
			}

			tran = new Transaction();
			tran.setAmount(CommonUtil.calcAmount(w.getDeposit()));
			tran.setCurrency(new Currency(Utils.getUser(userId).getCurrency()));
			tran.setComments(null);
			tran.setCreditCardId(null);
			tran.setDescription(null);
			tran.setIp(CommonUtil.getIPAddress());
			tran.setProcessedWriterId(writerId);
			tran.setChequeId(null);
			tran.setTimeSettled(null);
			tran.setTimeCreated(new Date());
			tran.setTimeCreatedTxt(CommonUtil.getTimeAndDateFormat(tran.getTimeCreated(), Utils.getUser(userId).getUtcOffset()));
			tran.setUtcOffsetCreated(Utils.getUser(userId).getUtcOffset());
			tran.setUtcOffsetSettled(null);
			tran.setTypeId(TRANS_TYPE_WIRE_DEPOSIT);
			tran.setUserId(user.getId());
			tran.setWriterId(writerId);
			tran.setWireId(new BigDecimal(w.getId()));
			tran.setChargeBackId(null);
			tran.setStatusId(TRANS_STATUS_PENDING);
			tran.setReceiptNum(new BigDecimal(TransactionsDAOBase.getNextReceipt(con)));

			UsersDAO.addToBalance(con, user.getId(), CommonUtil.calcAmount(w
					.getDeposit()));
			TransactionsDAO.insert(con, tran);
			GeneralDAO.insertBalanceLog(con, writerId, tran
					.getUserId(), Constants.TABLE_TRANSACTIONS, tran.getId(),
					Constants.LOG_BALANCE_WIRE_DEPOSIT, Utils.getUser(userId).getUtcOffset());

			con.commit();

			try {
				PopulationsManagerBase.deposit(userId, true, writerId, tran);
			} catch (Exception e) {
				log.warn("Problem with population succeed deposit event " + e);
			}

			String[] params = new String[1];
			params[0] = CommonUtil.displayAmount(tran.getAmount(), user.getCurrency().getId());
			FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_INFO,
					CommonUtil.getMessage("wire.deposit.success", params, Utils.getWriterLocale(context)), null);
			context.addMessage(null, fm);


			if (log.isEnabledFor(Level.INFO)) {
				log.log(Level.INFO, "Wire deposit inserted successfully!");
			}
            try {
                Utils.alertOverDepositLimitByEmail(tran, user);
            } catch (RuntimeException e) {
                log.error("Exception sending email alert to traders! ", e);
            }
		} catch (SQLException e) {
			log.log(Level.ERROR, "insertWireDeposit: ", e);
			try {
				con.rollback();
			} catch (SQLException ie) {
				log.log(Level.ERROR, "Can't rollback.", ie);
			}
			throw e;
		} finally {
			con.setAutoCommit(true);
			closeConnection(con);
		}

        try {
        	afterDepositAction(Utils.getUser(), tran, TransactionSource.TRANS_FROM_BACKEND);
            afterTransactionSuccess(tran.getId(), userId, tran.getAmount(), writerId, 0L);
        } catch (Exception e) {
            log.error("Problem processing bonuses after success transaction", e);
        }

        return tran;
	}




    public static Transaction insertEFTDeposit(WireBase w, long userId)
            throws SQLException {

        if (log.isEnabledFor(Level.INFO)) {
            String ls = System.getProperty("line.separator");
            log.log(Level.INFO, "Insert EFT deposit: " + ls + w.toString());
        }

        FacesContext context = FacesContext.getCurrentInstance();

        long writerId = AdminManager.getWriterId();
        Transaction tran = null;
        Connection con = getConnection();
        try {
            UserBase user = UsersDAO.getById(con, userId);

            con.setAutoCommit(false);

            if(CommonUtil.isHebrewSkin(user.getSkinId())){
                WiresDAOBase.insert(con, w);
            } else { //for anyoption
                WiresDAOBase.insertBankWire(con, w);
            }

            tran = new Transaction();
            tran.setAmount(CommonUtil.calcAmount(w.getDeposit()));
            tran.setCurrency(new Currency(Utils.getUser(userId).getCurrency()));
            tran.setComments(null);
            tran.setCreditCardId(null);
            tran.setDescription(null);
            tran.setIp(CommonUtil.getIPAddress());
            tran.setProcessedWriterId(writerId);
            tran.setChequeId(null);
            tran.setTimeSettled(null);
            tran.setTimeCreated(new Date());
            tran.setTimeCreatedTxt(CommonUtil.getTimeAndDateFormat(tran.getTimeCreated(), Utils.getUser(userId).getUtcOffset()));
            tran.setUtcOffsetCreated(Utils.getUser(userId).getUtcOffset());
            tran.setUtcOffsetSettled(null);
            tran.setTypeId(TRANS_TYPE_EFT_DEPOSIT);
            tran.setUserId(user.getId());
            tran.setWriterId(writerId);
            tran.setWireId(new BigDecimal(w.getId()));
            tran.setChargeBackId(null);
            tran.setStatusId(TRANS_STATUS_PENDING);
            tran.setReceiptNum(new BigDecimal(TransactionsDAOBase.getNextReceipt(con)));

            UsersDAO.addToBalance(con, user.getId(), CommonUtil.calcAmount(w
                    .getDeposit()));
            TransactionsDAO.insert(con, tran);
            GeneralDAO.insertBalanceLog(con, writerId, tran
                    .getUserId(), Constants.TABLE_TRANSACTIONS, tran.getId(),
                    Constants.LOG_BALANCE_EFT_DEPOSIT, Utils.getUser(userId).getUtcOffset());

            con.commit();

            try {
                PopulationsManagerBase.deposit(userId, true, writerId, tran);
            } catch (Exception e) {
                log.warn("Problem with population succeed deposit event " + e);
            }

            String[] params = new String[1];
            params[0] = CommonUtil.displayAmount(tran.getAmount(), user.getCurrency().getId());
            FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_INFO,
                    CommonUtil.getMessage("wire.deposit.success", params, Utils.getWriterLocale(context)), null);
            context.addMessage(null, fm);


            if (log.isEnabledFor(Level.INFO)) {
                log.log(Level.INFO, "EFT deposit inserted successfully!");
            }
            try {
                Utils.alertOverDepositLimitByEmail(tran, user);
            } catch (RuntimeException e) {
                log.error("Exception sending email alert to traders! ", e);
            }
        } catch (SQLException e) {
            log.log(Level.ERROR, "insertEFTDeposit: ", e);
            try {
                con.rollback();
            } catch (SQLException ie) {
                log.log(Level.ERROR, "Can't rollback.", ie);
            }
            throw e;
        } finally {
            con.setAutoCommit(true);
            closeConnection(con);
        }

        try {
        	afterDepositAction(Utils.getUser(), tran, TransactionSource.TRANS_FROM_BACKEND);
            afterTransactionSuccess(tran.getId(), userId, tran.getAmount(), writerId, 0L);
        } catch (Exception e) {
            log.error("Problem processing bonuses after success transaction", e);
        }

        return tran;
        }




	public static boolean insertBonusDeposit(DepositForm f,
			long userId, String userName, int transTypeId, long bonusUserId) throws SQLException {

		if (log.isEnabledFor(Level.INFO)) {
			String ls = System.getProperty("line.separator");
			log.log(Level.INFO, "Insert Bonus deposit: " + ls + "user:"
					+ userName + ls + f.toString());
		}

		FacesContext context = FacesContext.getCurrentInstance();
		Connection con = getConnection();
		try {

			Transaction tran = null;

			con.setAutoCommit(false);

			tran = new Transaction();
			tran.setAmount(CommonUtil.calcAmount(f.getDeposit()));
			tran.setCurrency(new Currency(Utils.getUser(userId).getCurrency()));
			tran.setComments(f.getComments());
			tran.setCreditCardId(null);
			tran.setDescription("log.balance.bonus.deposit");
			tran.setIp(CommonUtil.getIPAddress());
			tran.setProcessedWriterId(AdminManager.getWriterId());
			tran.setChequeId(null);
			tran.setTimeSettled(new Date());
			tran.setTimeCreated(new Date());
			tran.setUtcOffsetCreated(Utils.getUser(userId).getUtcOffset());
			tran.setUtcOffsetSettled(Utils.getUser(userId).getUtcOffset());
			tran.setTypeId(transTypeId);
			tran.setUserId(userId);
			tran.setWriterId(AdminManager.getWriterId());
			tran.setWireId(null);
			tran.setChargeBackId(null);
			tran.setStatusId(TRANS_STATUS_SUCCEED);
			tran.setReceiptNum(null);
			tran.setBonusUserId(bonusUserId);

			UsersDAO.addToBalance(con, userId, CommonUtil.calcAmount(f
					.getDeposit()));
			TransactionsDAO.insert(con, tran);
			GeneralDAO.insertBalanceLog(con, AdminManager.getWriterId(),
					tran.getUserId(), Constants.TABLE_TRANSACTIONS, tran
							.getId(), Constants.LOG_BALANCE_BONUS_DEPOSIT, Utils.getUser(userId).getUtcOffset());

			con.commit();

			String[] params = new String[1];
			params[0] = CommonUtil.displayAmount(tran.getAmount(), tran.getCurrencyId());
			FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_INFO,
					CommonUtil.getMessage("deposit.success", params, Utils.getWriterLocale(context)), null);
			context.addMessage(null, fm);


			if (log.isEnabledFor(Level.INFO)) {
				log.log(Level.INFO, "Bonus deposit inserted successfully!");
			}

			return true;

		} catch (SQLException e) {
			log.log(Level.ERROR, "insertBonusDeposit: ", e);
			try {
				con.rollback();
			} catch (SQLException ie) {
				log.log(Level.ERROR, "Can't rollback.", ie);
			}
			throw e;
		} finally {
			con.setAutoCommit(true);
			closeConnection(con);
		}

	}

	public static boolean insertBonusWithdraw(Connection prevCon,
			WithdrawForm f, long userId, int transTypeId) throws SQLException {

		if (log.isEnabledFor(Level.INFO)) {
			String ls = System.getProperty("line.separator");
			log.log(Level.INFO, "Insert Bonus Withdraw: " + ls + "userId: "
					+ userId + ls + f.toString() + ls);
		}

		FacesContext context = FacesContext.getCurrentInstance();

		Connection con = getConnection();
		try {

			con.setAutoCommit(false);

			Transaction tran = new Transaction();
			tran.setAmount(CommonUtil.calcAmount(f.getAmount()));
			tran.setCurrency(new Currency(Utils.getUser(userId).getCurrency()));
			tran.setComments(f.getComments());
			tran.setCreditCardId(null);
			tran.setDescription("log.balance.bonus.withdraw");
			tran.setIp(CommonUtil.getIPAddress());
			tran.setProcessedWriterId(AdminManager.getWriterId());
			tran.setTimeSettled(new Date());
			tran.setTimeCreated(new Date());
			tran.setUtcOffsetCreated(Utils.getUser(userId).getUtcOffset());
			tran.setUtcOffsetSettled(Utils.getUser(userId).getUtcOffset());
			tran.setTypeId(transTypeId);
			tran.setUserId(userId);
			tran.setWriterId(AdminManager.getWriterId());
			tran.setStatusId(TRANS_STATUS_SUCCEED);
			tran.setChequeId(null);
			tran.setWireId(null);
			tran.setChargeBackId(null);
			tran.setReceiptNum(null);

			if (prevCon != null) {
				UsersDAO.addToBalance(prevCon, userId, -CommonUtil.calcAmount(f
						.getAmount()));
				TransactionsDAO.insert(prevCon, tran);
				GeneralDAO.insertBalanceLog(prevCon,
						AdminManager.getWriterId(), tran.getUserId(),
						Constants.TABLE_TRANSACTIONS, tran.getId(),
						Constants.LOG_BALANCE_BONUS_WITHDRAW, Utils.getUser(userId).getUtcOffset());
			} else {
				UsersDAO.addToBalance(con, userId, -CommonUtil.calcAmount(f
						.getAmount()));
				TransactionsDAO.insert(con, tran);
				GeneralDAO.insertBalanceLog(con, AdminManager.getWriterId(),
						tran.getUserId(), Constants.TABLE_TRANSACTIONS, tran
								.getId(), Constants.LOG_BALANCE_BONUS_WITHDRAW, Utils.getUser(userId).getUtcOffset());
			}

			if (log.isEnabledFor(Level.INFO)) {
				String ls = System.getProperty("line.separator");
				log.log(Level.INFO, "Bonus Withdraw insert successfully! " + ls
						+ tran.toString());
			}

			con.commit();
			String[] params = new String[1];
			params[0] = CommonUtil.displayAmount(tran.getAmount(), tran.getCurrencyId());
			FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_INFO,
					CommonUtil.getMessage("withdraw.admin.success", params, Utils.getWriterLocale(context)),
					null);
			context.addMessage(null, fm);
		} catch (SQLException e) {
			log.log(Level.ERROR, "insertBonusWithdraw: ", e);
			try {
				con.rollback();
			} catch (SQLException ie) {
				log.log(Level.ERROR, "Can't rollback.", ie);
			}
			throw e;
		} finally {
			con.setAutoCommit(true);
			closeConnection(con);
		}
		return true;
	}


	public static boolean insertAdminDeposit(Connection prevCon, DepositForm f,
			long userId, String userName, int transTypeId) throws SQLException {

		if (log.isEnabledFor(Level.INFO)) {
			String ls = System.getProperty("line.separator");
			log.log(Level.INFO, "Insert Admin deposit: " + ls + "user:"
					+ userName + ls + f.toString());
		}

		FacesContext context = FacesContext.getCurrentInstance();
        Transaction tran = null;
		Connection con = getConnection();
		try {
			con.setAutoCommit(false);

			tran = new Transaction();
			tran.setAmount(CommonUtil.calcAmount(f.getDeposit()));
			tran.setCurrency(Utils.getUser(userId).getCurrency());
			tran.setComments(f.getComments());
			tran.setCreditCardId(null);
			tran.setDescription(null);
			tran.setIp(CommonUtil.getIPAddress());
			tran.setProcessedWriterId(AdminManager.getWriterId());
			tran.setChequeId(null);
			tran.setTimeSettled(new Date());
			tran.setTimeCreated(new Date());
			tran.setUtcOffsetCreated(Utils.getUser(userId).getUtcOffset());
			tran.setUtcOffsetSettled(Utils.getUser(userId).getUtcOffset());
			tran.setTypeId(transTypeId);
			tran.setUserId(userId);
			tran.setWriterId(AdminManager.getWriterId());
			tran.setWireId(null);
			tran.setChargeBackId(null);
			tran.setStatusId(TRANS_STATUS_SUCCEED);
			tran.setReceiptNum(null);

			if (prevCon != null) {
				UsersDAO.addToBalance(prevCon, userId, CommonUtil.calcAmount(f
						.getDeposit()));
				TransactionsDAO.insert(prevCon, tran);
				GeneralDAO.insertBalanceLog(prevCon,
						AdminManager.getWriterId(), tran.getUserId(),
						Constants.TABLE_TRANSACTIONS, tran.getId(),
						Constants.LOG_BALANCE_ADMIN_DEPOSIT, Utils.getUser(userId).getUtcOffset());
			} else {
				UsersDAO.addToBalance(con, userId, CommonUtil.calcAmount(f
						.getDeposit()));
				TransactionsDAO.insert(con, tran);
				GeneralDAO.insertBalanceLog(con, AdminManager.getWriterId(),
						tran.getUserId(), Constants.TABLE_TRANSACTIONS, tran
								.getId(), Constants.LOG_BALANCE_ADMIN_DEPOSIT, Utils.getUser(userId).getUtcOffset());
			}

			con.commit();

			String[] params = new String[1];
			params[0] = CommonUtil.displayAmount(tran.getAmount(), tran.getCurrencyId());
			FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_INFO,
					CommonUtil.getMessage("deposit.success", params, Utils.getWriterLocale(context)), null);
			context.addMessage(null, fm);


			if (log.isEnabledFor(Level.INFO)) {
				log.log(Level.INFO, "Admin deposit inserted successfully!");
			}
            try {
                UserBase user = getById(userId);
                Utils.alertOverDepositLimitByEmail(tran, user);
            } catch (RuntimeException e) {
                log.error("Exception sending email alert to traders! ", e);
            }
		} catch (SQLException e) {
			log.log(Level.ERROR, "insertAdminDeposit: ", e);
			try {
				con.rollback();
			} catch (SQLException ie) {
				log.log(Level.ERROR, "Can't rollback.", ie);
			}
			throw e;
		} finally {
			con.setAutoCommit(true);
			closeConnection(con);
		}

        return true;
	}

	public static boolean insertCashDeposit(String deposit, long userId)
			throws SQLException {

		if (log.isEnabledFor(Level.INFO)) {
			String ls = System.getProperty("line.separator");
			log.log(Level.INFO, "Insert Cash deposit: " + ls + "userId:"
					+ userId + ls + "amount: " + deposit);
		}

		FacesContext context = FacesContext.getCurrentInstance();
        long writerId = AdminManager.getWriterId();
        Transaction tran = null;
		Connection con = getConnection();
		try {
			con.setAutoCommit(false);

			tran = new Transaction();
			tran.setAmount(CommonUtil.calcAmount(deposit));
			tran.setCurrency(new Currency(Utils.getUser(userId).getCurrency()));
			tran.setComments(null);
			tran.setCreditCardId(null);
			tran.setDescription(null);
			tran.setIp(CommonUtil.getIPAddress());
			tran.setProcessedWriterId(writerId);
			tran.setChequeId(null);
			tran.setTimeSettled(new Date());
			tran.setTimeCreated(new Date());
			tran.setUtcOffsetCreated(Utils.getUser(userId).getUtcOffset());
			tran.setUtcOffsetSettled(Utils.getUser(userId).getUtcOffset());
			tran.setTypeId(TRANS_TYPE_CASH_DEPOSIT);
			tran.setUserId(userId);
			tran.setWriterId(writerId);
			tran.setWireId(null);
			tran.setChargeBackId(null);
			tran.setStatusId(TRANS_STATUS_PENDING);
			tran.setReceiptNum(new BigDecimal(TransactionsDAOBase.getNextReceipt(con)));

			UsersDAO.addToBalance(con, userId, CommonUtil.calcAmount(deposit));
			TransactionsDAO.insert(con, tran);
			GeneralDAO.insertBalanceLog(con, writerId, tran
					.getUserId(), Constants.TABLE_TRANSACTIONS, tran.getId(),
					Constants.LOG_BALANCE_CASH_DEPOSIT, Utils.getUser(userId).getUtcOffset());

			con.commit();

			try {
				PopulationsManagerBase.deposit(userId, true, writerId, tran);
			} catch (Exception e) {
				log.warn("Problem with population succeed deposit event " + e);
			}

			String[] params = new String[1];
			params[0] = CommonUtil.displayAmount(tran.getAmount(), tran.getCurrencyId());
			FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_INFO,
					CommonUtil.getMessage("deposit.success", params, Utils.getWriterLocale(context)), null);
			context.addMessage(null, fm);


			if (log.isEnabledFor(Level.INFO)) {
				log.log(Level.INFO, "Cash deposit inserted successfully!");
			}
            try {
                UserBase user = getById(userId);
                Utils.alertOverDepositLimitByEmail(tran, user);
            } catch (RuntimeException e) {
                log.error("Exception sending email alert to traders! ", e);
            }
		} catch (SQLException e) {
			log.log(Level.ERROR, "insertCashDeposit: ", e);
			try {
				con.rollback();
			} catch (SQLException ie) {
				log.log(Level.ERROR, "Can't rollback.", ie);
			}
			throw e;
		} finally {
			con.setAutoCommit(true);
			closeConnection(con);
		}

        try {
        	afterDepositAction(Utils.getUser(), tran, TransactionSource.TRANS_FROM_BACKEND);
            afterTransactionSuccess(tran.getId(), userId, tran.getAmount(), writerId, 0L);
        } catch (Exception e) {
            log.error("Problem processing bonuses after success transaction", e);
        }

        return true;
	}

	public static void deleteCards(ArrayList cards) throws Exception {

		if (log.isEnabledFor(Level.INFO)) {
			log.log(Level.INFO, " deleting cards :");
		}

		FacesContext context = FacesContext.getCurrentInstance();

		Connection con = getConnection();

		CreditCard c = null;
		try {
			con.setAutoCommit(false);
			int count = 0;
			Iterator iter = cards.iterator();
			while (iter.hasNext()) {

				c = (CreditCard) iter.next();

				if (c.isSelected()) {
					deleteCard(con, c);
					count++;
				}

			}
			if (count == 0) {
				FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR,
						CommonUtil.getMessage("error.card.nonselected", null, Utils.getWriterLocale(context)),
						null);
				context.addMessage(null, fm);

				return;
			}

			con.commit();

		} catch (	SQLException | InvalidKeyException | NoSuchAlgorithmException | NoSuchPaddingException | IllegalBlockSizeException
					| BadPaddingException | UnsupportedEncodingException e) {
			log.log(Level.ERROR, "deleteCards: ", e);
			try {
				con.rollback();
			} catch (SQLException ie) {
				log.log(Level.ERROR, "Can't rollback.", ie);
			}
			throw e;
		} finally {
			con.setAutoCommit(true);
			closeConnection(con);
		}

		FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_INFO,
				CommonUtil.getMessage("creditcards.delete.success", null, Utils.getWriterLocale(context)), null);
		context.addMessage(null, fm);


	}

	private static void deleteCard(Connection con, CreditCard c)	throws SQLException, InvalidKeyException, NoSuchAlgorithmException,
																	NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException,
																	UnsupportedEncodingException, InvalidAlgorithmParameterException {
		if (log.isEnabledFor(Level.INFO)) {
			log.log(Level.INFO, " about to delete card: "
					+ c.toString());
		}
		c.setVisible(false);
		c.setUtcOffsetModified(Utils.getUser(c.getUserId()).getUtcOffset());
		CreditCardsDAO.update(con, c);

		if (log.isEnabledFor(Level.INFO)) {
			log.log(Level.INFO, " card deleted sucessfuly!");
		}
	}

	public static boolean deleteCard(CreditCard c) throws SQLException{
		Connection con = getConnection();

		try {
			deleteCard(con, c);
			return true;
		} catch (	SQLException | InvalidKeyException | NoSuchAlgorithmException | NoSuchPaddingException | IllegalBlockSizeException
					| BadPaddingException | UnsupportedEncodingException | InvalidAlgorithmParameterException e) {
			log.log(Level.ERROR, "can't deleteCard: ", e);
		} finally {
			closeConnection(con);
		}
		return false;
	}

	public static boolean reverseWithdraw(ArrayList tranList, long userId)
			throws SQLException {

		FacesContext context = FacesContext.getCurrentInstance();
		String[] params = new String[1];
		long totalAmount = 0;
		Connection con = getConnection();

		try {

			UserBase user = UsersDAO.getById(con, userId);
			Transaction tran = null;
			con.setAutoCommit(false);
			int count = 0;
			Iterator iter = tranList.iterator();
			while (iter.hasNext()) {
				tran = (Transaction) iter.next();

				if (tran.isSelected()) {

					if (log.isEnabledFor(Level.INFO)) {
						String ls = System.getProperty("line.separator");
						log.log(Level.INFO, "Insert ReverseWithdraw: " + ls
								+ tran.toString() + ls);
					}

					if (TransactionsManagerBase.isStillPendingWithdraw(tran.getId())){

						tran.setTimeSettled(new Date());
						tran.setUtcOffsetSettled(Utils.getUser(userId).getUtcOffset());
						tran.setProcessedWriterId(AdminManager.getWriterId());
						tran.setStatusId(TRANS_STATUS_REVERSE_WITHDRAW);

						TransactionsDAO.updateReverseWithdraw(con, tran);

						tran.setReferenceId(new BigDecimal(tran.getId()));
						tran.setIp(CommonUtil.getIPAddress());
						tran.setTypeId(TRANS_TYPE_REVERSE_WITHDRAW);
						tran.setStatusId(TRANS_STATUS_SUCCEED);
						tran.setTimeSettled(new Date());
						tran.setWriterId(AdminManager.getWriterId());

						UsersDAOBase.addToBalance(con, user.getId(), tran
								.getAmount());
						GeneralDAO.insertBalanceLog(con,
								AdminManager.getWriterId(), tran.getUserId(),
								Constants.TABLE_TRANSACTIONS, tran.getId(),
								Constants.LOG_BALANCE_REVERSE_WITHDRAW, Utils.getUser(userId).getUtcOffset());
						totalAmount += tran.getAmount();

						TransactionsDAOBase.insert(con, tran);

						if (log.isEnabledFor(Level.INFO)) {
							String ls = System.getProperty("line.separator");
							log.log(Level.INFO,
									"ReverseWithdraw insert successfully! " + ls
											+ tran.toString());
						}
					}
					count++;
				}
			}

			if (count == 0) {
				FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR,
						CommonUtil.getMessage("error.transaction.nonselected", null, Utils.getWriterLocale(context)), null);
				context.addMessage(null, fm);

				return false;
			}
			con.commit();

			if (tranList.size() == count) {
				PopulationEntryBase pe = PopulationsManagerBase.getPopulationUserByUserId(user.getId());
				if(pe.getCurrPopualtionTypeId() == PopulationsManagerBase.POP_TYPE_OPEN_WITHDRAW &&
						pe.getEntryTypeId() != PopulationsManagerBase.ENTRY_TYPE_REMOVE_PERMANENTLY) {
					PopulationsManagerBase.insertIntoPopulation(0, 0, user.getId(), 
							pe.getCurrPopulationName(), pe.getAssignWriterId(), PopulationsManagerBase.POP_TYPE_RETENTION, pe);
				}
			}
			
			params[0] = CommonUtil.displayAmount(totalAmount, tran.getCurrencyId());
			FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_INFO,
					CommonUtil.getMessage("reverseWithdraw.success", params, Utils.getWriterLocale(context)),	null);
			context.addMessage(null, fm);

			return true;

		} catch (SQLException e) {
			log.log(Level.ERROR, "reverseWithdraw: ", e);
			try {
				con.rollback();
			} catch (SQLException ie) {
				log.log(Level.ERROR, "Can't rollback.", ie);
			}
			throw e;
		} finally {
			con.setAutoCommit(true);
			closeConnection(con);
		}

	}

	public static boolean insertAdminWithdraw(Connection prevCon,
			WithdrawForm f, long userId, int transTypeId) throws SQLException {

		if (log.isEnabledFor(Level.INFO)) {
			String ls = System.getProperty("line.separator");
			log.log(Level.INFO, "Insert Admin Withdraw: " + ls + "userId: "
					+ userId + ls + f.toString() + ls);
		}

		FacesContext context = FacesContext.getCurrentInstance();

		Connection con = getConnection();
		try {

			con.setAutoCommit(false);

			Transaction tran = new Transaction();
			tran.setAmount(CommonUtil.calcAmount(f.getAmount()));
			tran.setCurrency(Utils.getUser(userId).getCurrency());
			tran.setComments(f.getComments());
			tran.setCreditCardId(null);
			tran.setDescription(null);
			tran.setIp(CommonUtil.getIPAddress());
			tran.setProcessedWriterId(AdminManager.getWriterId());
			tran.setTimeSettled(new Date());
			tran.setTimeCreated(new Date());
			tran.setUtcOffsetCreated(Utils.getUser(userId).getUtcOffset());
			tran.setUtcOffsetSettled(Utils.getUser(userId).getUtcOffset());
			tran.setTypeId(transTypeId);
			tran.setUserId(userId);
			tran.setWriterId(AdminManager.getWriterId());
			tran.setStatusId(TRANS_STATUS_SUCCEED);
			tran.setChequeId(null);
			tran.setWireId(null);
			tran.setChargeBackId(null);
			tran.setReceiptNum(null);

			if (prevCon != null) {
				UsersDAO.addToBalance(prevCon, userId, -CommonUtil.calcAmount(f
						.getAmount()));
				TransactionsDAO.insert(prevCon, tran);
				GeneralDAO.insertBalanceLog(prevCon,
						AdminManager.getWriterId(), tran.getUserId(),
						Constants.TABLE_TRANSACTIONS, tran.getId(),
						Constants.LOG_BALANCE_ADMIN_WITHDRAW, Utils.getUser(userId).getUtcOffset());
			} else {
				UsersDAO.addToBalance(con, userId, -CommonUtil.calcAmount(f
						.getAmount()));
				TransactionsDAO.insert(con, tran);
				GeneralDAO.insertBalanceLog(con, AdminManager.getWriterId(),
						tran.getUserId(), Constants.TABLE_TRANSACTIONS, tran
								.getId(), Constants.LOG_BALANCE_ADMIN_WITHDRAW, Utils.getUser(userId).getUtcOffset());
			}

			if (log.isEnabledFor(Level.INFO)) {
				String ls = System.getProperty("line.separator");
				log.log(Level.INFO, "Admin Withdraw insert successfully! " + ls
						+ tran.toString());
			}

			con.commit();
			String[] params = new String[1];
			params[0] = CommonUtil.displayAmount(tran.getAmount(), tran.getCurrencyId());
			if(context != null) {
				FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_INFO,
						CommonUtil.getMessage("withdraw.admin.success", params, Utils.getWriterLocale(context)),
						null);
				context.addMessage(null, fm);
			}
		} catch (SQLException e) {
			log.log(Level.ERROR, "insertAdminWithdraw: ", e);
			try {
				con.rollback();
			} catch (SQLException ie) {
				log.log(Level.ERROR, "Can't rollback.", ie);
			}
			throw e;
		} finally {
			con.setAutoCommit(true);
			closeConnection(con);
		}
		return true;
	}

	public static ArrayList getChargeBacks(long id, long statusId, String userName, long classId, String tranId, String arn, int state, long currencyId, long providerId)
			throws SQLException {
		Connection con = getConnection();
		try {
			return ChargeBacksDAO.search(con, id, statusId, userName, classId, tranId, arn, state, currencyId, providerId);
		} finally {
			closeConnection(con);
		}
	}

	public static String[] getAverageDepositWithdraw(long userId)
			throws SQLException {
		String[] l = null;
		Connection conn = null;
		try {
			conn = getSecondConnection();
			l = TransactionsDAO.getAverageDepositWithdraw(conn, userId);
		} finally {
			closeConnection(conn);
		}
		return l;
	}

	public static void cancelDeposit(Transaction trans, int status) throws SQLException {
		if (log.isEnabledFor(Level.INFO)) {
			String ls = System.getProperty("line.separator");
			log.log(Level.INFO, "Cancel Deposit: " + ls + "transaction: "
					+ trans + ls);
		}
		FacesContext context = FacesContext.getCurrentInstance();
		Connection con = getConnection();

		try {
			con.setAutoCommit(false);

			TransactionsDAO
					.updateStatusId(
							con,
							trans.getId(),
							status,
							true,AdminManager.getWriterId());
			long negFixTransactionId = UsersManager.addToBalanceNonNeg(	con, trans.getUserId(), -trans.getAmount(), trans.getRate(),
																		Utils.getUser(trans.getUserId()).getUtcOffset());
			GeneralDAO.insertBalanceLog(con, AdminManager.getWriterId(), trans
					.getUserId(), Constants.TABLE_TRANSACTIONS, trans.getId(),
					Constants.LOG_BALANCE_CANCEL_DEPOSIT, Utils.getUser(trans.getUserId()).getUtcOffset());
			if (negFixTransactionId > 0) {
				GeneralDAO.insertBalanceLog(con, AdminManager.getWriterId(), trans.getUserId(), Constants.TABLE_TRANSACTIONS, trans.getId(),
											Constants.LOG_BALANCE_FIX_NEGATIVE_BALANCE_DEPOSIT,
											Utils.getUser(trans.getUserId()).getUtcOffset());
			}
			con.commit();
			 ClearingProvider p = ClearingManager.getClearingProviders().get(trans.getClearingProviderId());
		        if (null != p) {
					p.cancel(new ClearingInfo(Utils.getUser(trans.getUserId()), trans.getAmount(), trans.getTypeId(), trans.getId(), trans.getIp(), trans.getAcquirerResponseId()));
		        } 
			if (log.isEnabledFor(Level.INFO)) {
				String ls = System.getProperty("line.separator");
				log.log(Level.INFO, "Cancel Deposit finished successfully."
						+ ls);
			}

		} catch (SQLException e) {
			log.error("Exception during Cancel Deposit!! " + e);
			try {
				con.rollback();
			} catch (SQLException ie) {
				log.log(Level.ERROR, "Can't rollback.", ie);
			}
			throw e;
		}  catch (ClearingException e) {
			log.error("Exception during Cancel Deposit!! " + e);
			try {
				con.rollback();
			} catch (SQLException ie) {
				log.log(Level.ERROR, "Can't rollback.", ie);
			}
		} finally {
			con.setAutoCommit(true);
			closeConnection(con);
		}
		FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_INFO,
				CommonUtil.getMessage("transactions.cancel.success", null, Utils.getWriterLocale(context)), null);
		context.addMessage(null, fm);

	}

	public static ArrayList<Transaction> getPendingWithdraws(Date from, Date to,
			String userId, String userIdNum, boolean admin, String userName, long classId, String skins, long currencyId, int skinId, long skinBusinessCaseId, long withdrawTypeId) throws SQLException {
		Connection con = getSecondConnection();
		try {

			return TransactionsDAO.getPendingWithdraws(con, from, to, userId,
					userIdNum, admin, userName, classId, skins, currencyId, skinId, skinBusinessCaseId, withdrawTypeId);

		} finally {
			closeConnection(con);
		}
	}

	public static boolean approveWithdraw(Transaction t, Cheque c, long fee,
			boolean admin) throws SQLException {

		if (log.isEnabledFor(Level.INFO)) {
			String ls = System.getProperty("line.separator");
			log.log(Level.INFO, "Approve Withdraw: " + ls + t.toString() + ls
					+ " cheque: " + c.toString() + ls + " fee: " + fee + ls
					+ " admin: " + admin + ls);
		}

		FacesContext context = FacesContext.getCurrentInstance();

		if (!admin) {
			if (c.getChequeId() == null || c.getChequeId().equals("")) {
				FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR,
						CommonUtil.getMessage("error.mandatory", null, Utils.getWriterLocale(context)), null);
				context.addMessage("pendingWithdrawForm:chequeId", fm);
				return false;
			}
		}

		Connection con = getConnection();

		try {
			con.setAutoCommit(false);

			t.setProcessedWriterId(AdminManager.getWriterId());
			t.setUtcOffsetSettled(Utils.getUser(t.getUserId()).getUtcOffset());

			if (!admin) {
				t.setAmount(t.getAmount() - fee);
				t.setTimeSettled(new Date());
				t.setStatusId(TransactionsManager.TRANS_STATUS_SUCCEED);
			} else {
				t.setStatusId(TransactionsManager.TRANS_STATUS_APPROVED);
			}

			TransactionsDAO.update(con, t);

			if (fee > 0 && !admin) {
				long withdrawId = t.getId();

				t.setIp(CommonUtil.getIPAddress());
				t.setDescription(null);
				t.setComments(null);
				t.setAmount(fee);
				t.setProcessedWriterId(AdminManager.getWriterId());
				t.setWriterId(AdminManager.getWriterId());
				t.setTimeSettled(new Date());
				t.setStatusId(TransactionsManager.TRANS_STATUS_SUCCEED);
				t.setTypeId(TransactionsManager.TRANS_TYPE_HOMO_FEE);
				t.setReferenceId(new BigDecimal(withdrawId));
				t.setChequeId(null);
				t.setLoginId(0L);

				TransactionsDAO.insert(con, t);

				if (log.isEnabledFor(Level.INFO)) {
					String ls = System.getProperty("line.separator");
					log.log(Level.INFO, " fee transaction inserted: " + ls
							+ t.toString());
				}

				TransactionsDAO.updateRefId(con, withdrawId, t.getId());
			}

			ChequeDAO.update(con, c);

			if (log.isEnabledFor(Level.INFO)) {
				String ls = System.getProperty("line.separator");
				log.log(Level.INFO, " approve Withdraw finished successfully! "
						+ ls);
			}

			con.commit();

		} catch (SQLException e) {
			log.log(Level.ERROR, "approveWithdraw: ", e);
			try {
				con.rollback();
			} catch (SQLException ie) {
				log.log(Level.ERROR, "Can't rollback.", ie);
			}
			throw e;
		} finally {
			con.setAutoCommit(true);
			closeConnection(con);
		}

		FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_INFO,
				CommonUtil.getMessage("approve.withdraw.success", null, Utils.getWriterLocale(context)), null);
		context.addMessage(null, fm);

		return true;

	}

	/**
	 * Apptove credit card withdrawal
	 * @param t
	 * 		transaction
	 * @param fee
	 * 		fee on this transaction
	 * @param admin
	 * @param changedProviderComment 
	 * @return
	 * @throws SQLException
	 */
	public static boolean approveCcWithdraw(Transaction t, long fee, boolean admin, String processedWriterName, StringBuffer changedProviderComment) throws SQLException {

		if (log.isEnabledFor(Level.INFO)) {
			String ls = System.getProperty("line.separator");
			log.log(Level.INFO, "Approve Credit card Withdraw: " + ls + t.toString() + ls
					+ " ccNumber: " + t.getCc4digit() + ls + " fee: " + fee + ls
					+ " admin: " + admin + ls);
		}

		FacesContext context = FacesContext.getCurrentInstance();

		Connection con = getConnection();

		try {
			con.setAutoCommit(false);

			t.setProcessedWriterId(Utils.getWriter().getWriter().getId());

			if ( !admin ) {  // from accounting tab
				t.setAccountingApproved(true);
				// Necessary because we treat cup and baropay withdrawals as cc withdrawals
				if (t.getTypeId() == TRANS_TYPE_CC_WITHDRAW) {
					t.setStatusId(TransactionsManager.TRANS_STATUS_SECOND_APPROVAL);
				}
				String comments = t.getComments().isEmpty() ? "" : t.getComments() + "|";
				SimpleDateFormat formatDate = new SimpleDateFormat("dd.MM.yy HH:mm");
				comments += "Second approved at " + formatDate.format(Calendar.getInstance().getTime()) + " by " + processedWriterName;
				t.setComments(comments);
				TransactionsDAO.updateAfterAccouningApproved(con, t);
				t.setUtcOffsetSettled(Utils.getUser(t.getUserId()).getUtcOffset());
			}
			else {  // admin
				t.setStatusId(TransactionsManager.TRANS_STATUS_APPROVED);
				String comments = t.getComments().isEmpty() ? "" : t.getComments() + "|";
				SimpleDateFormat formatDate = new SimpleDateFormat("dd.MM.yy HH:mm");
				comments += "First approved at " + formatDate.format(Calendar.getInstance().getTime()) + " by " + processedWriterName + " | " + changedProviderComment.toString();
				t.setComments(comments);
			}
			TransactionsDAO.update(con, t);

			if (log.isEnabledFor(Level.INFO)) {
				String ls = System.getProperty("line.separator");
				log.log(Level.INFO, " approve credit card Withdraw finished successfully! "
						+ ls);
			}

			con.commit();

		} catch (SQLException e) {
			log.log(Level.ERROR, "approveCcWithdraw: ", e);
			try {
				con.rollback();
			} catch (SQLException ie) {
				log.log(Level.ERROR, "Can't rollback.", ie);
			}
			throw e;
		} finally {
			con.setAutoCommit(true);
			closeConnection(con);
		}

		FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_INFO,
				CommonUtil.getMessage("approve.withdraw.success", null, Utils.getWriterLocale(context)), null);
		context.addMessage(null, fm);

		return true;

	}


	/**
	 * Apptove bank wire withdrawal
	 * @param t
	 * 		Transaction to approve
	 * @param w
	 * 		wire object
	 * @param fee
	 * 		fee amount
	 * @param admin
	 * 		if we came from admin or accounting
	 * @param lowWithdrawFee 
	 * @return
	 * @throws SQLException
	 */
	public static boolean approveBankWireWithdraw(Transaction t, WireBase w, long fee,
			boolean admin, long chosenBankID) throws SQLException {

		if (log.isEnabledFor(Level.INFO)) {
			String ls = System.getProperty("line.separator");
			log.log(Level.INFO, "Approve bank wire Withdraw: " + ls + t.toString() + ls
					+ " bank wire: " + w.toString() + ls + " fee: " + fee + ls
					+ " admin: " + admin + ls);
		}

		FacesContext context = FacesContext.getCurrentInstance();

		if (!admin) {
			if (t.getWireId() == null || t.getWireId().equals("")) {
				FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR,
						CommonUtil.getMessage("error.mandatory", null, Utils.getWriterLocale(context)), null);
				context.addMessage("pendingWithdrawForm:wireId", fm);
				return false;
			}
		}

		Connection con = getConnection();

		try {
			t.setProcessedWriterId(Utils.getWriter().getWriter().getId());
			t.setUtcOffsetSettled(Utils.getUser(t.getUserId()).getUtcOffset());

			if (!admin) {  // accounting
				t.setAmount(t.getAmount() - fee);
				t.setTimeSettled(new Date());
				t.setStatusId(TransactionsManager.TRANS_STATUS_SUCCEED);
				t.setWireAmountAfterFee(CommonUtil.displayAmount(t.getAmount(), t.getCurrency().getId()));  // save the amount before taking the fee
			} else {  // admin
				t.setStatusId(TransactionsManager.TRANS_STATUS_APPROVED);
			}

			con.setAutoCommit(false);
			TransactionsDAO.update(con, t);

			if (!admin && w.getBankFeeAmount() > 0){
				WiresDAOBase.updateBankFeeAmount(con,w.getId(),w.getBankFeeAmount());
			}
            WiresDAOBase.updateBankWireWithdrawal(con,w.getId(), chosenBankID);

			if (fee > 0 && !admin) {
				long withdrawId = t.getId();

				t.setIp(CommonUtil.getIPAddress());
				t.setDescription(null);
				t.setComments(null);
				t.setAmount(fee);
				t.setProcessedWriterId(Utils.getWriter().getWriter().getId());
				t.setWriterId(Utils.getWriter().getWriter().getId());
				t.setTimeSettled(new Date());
				t.setStatusId(TransactionsManager.TRANS_STATUS_SUCCEED);
				t.setTypeId(TransactionsManager.TRANS_TYPE_HOMO_FEE);
				t.setReferenceId(new BigDecimal(withdrawId));
				t.setChequeId(null);
				t.setLoginId(0L);

				TransactionsDAO.insert(con, t);

				if (log.isEnabledFor(Level.INFO)) {
					String ls = System.getProperty("line.separator");
					log.log(Level.INFO, " fee transaction for bank wire inserted: " + ls
							+ t.toString());
				}

				TransactionsDAO.updateRefId(con, withdrawId, t.getId());
			}

			if (log.isEnabledFor(Level.INFO)) {
				String ls = System.getProperty("line.separator");
				log.log(Level.INFO, " approve bank wire Withdraw finished successfully! "
						+ ls);
			}

			con.commit();

		} catch (SQLException e) {
			log.log(Level.ERROR, "approveBankWireWithdraw: ", e);
			try {
				con.rollback();
			} catch (SQLException ie) {
				log.log(Level.ERROR, "Can't rollback.", ie);
			}
			throw e;
		} finally {
			con.setAutoCommit(true);
			closeConnection(con);
		}

		FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_INFO,
				CommonUtil.getMessage("approve.withdraw.success", null, Utils.getWriterLocale(context)), null);
		context.addMessage(null, fm);

		return true;

	}


	/**
	 * Apptove payPal withdrawal
	 * @param t
	 * 		Transaction to approve
	 * @param fee
	 * 		fee amount
	 * @param admin
	 * 		if we came from admin or accounting
	 * @return
	 * @throws SQLException
	 */
	public static boolean approvePayPalWithdraw(Transaction t, long fee, boolean admin, String paypalTransactionId) throws SQLException {
		if (log.isEnabledFor(Level.INFO)) {
			String ls = System.getProperty("line.separator");
			log.log(Level.INFO, "Approve PayPal Withdraw: " + ls + t.toString() + ls
					+ "Amount: " + t.getAmount() + ls + " fee: " + fee + ls
					+ " admin: " + admin + ls);
		}
		FacesContext context = FacesContext.getCurrentInstance();
		Connection con = getConnection();
		try {
			con.setAutoCommit(false);
			t.setProcessedWriterId(Utils.getWriter().getWriter().getId());

			if (!admin) {  // from accounting tab
				t.setAccountingApproved(true);
				TransactionsDAO.updateAfterAccouningApproved(con, t);

				t.setXorIdAuthorize(paypalTransactionId);
				t.setXorIdCapture(paypalTransactionId);
				t.setAmount(t.getAmount() - fee);
				t.setTimeSettled(new Date());
				t.setUtcOffsetSettled(Utils.getUser(t.getUserId()).getUtcOffset());
				t.setStatusId(TransactionsManager.TRANS_STATUS_SUCCEED);
			} else {  // admin
				t.setStatusId(TransactionsManager.TRANS_STATUS_APPROVED);
			}
			TransactionsDAO.update(con, t);

			if (log.isEnabledFor(Level.INFO)) {
				String ls = System.getProperty("line.separator");
				log.log(Level.INFO, " approve PayPal Withdraw finished successfully! "
						+ ls);
			}
			con.commit();
		} catch (SQLException e) {
			log.log(Level.ERROR, "approvePPWithdraw: ", e);
			try {
				con.rollback();
			} catch (SQLException ie) {
				log.log(Level.ERROR, "Can't rollback.", ie);
			}
			throw e;
		} finally {
			con.setAutoCommit(true);
			closeConnection(con);
		}

		FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_INFO,
				CommonUtil.getMessage("approve.withdraw.success", null, Utils.getWriterLocale(context)), null);
		context.addMessage(null, fm);

		return true;
	}

	public static boolean rejectWithdraw(Transaction t) throws SQLException {

		if (log.isEnabledFor(Level.INFO)) {
			String ls = System.getProperty("line.separator");
			log.log(Level.INFO, "Reject Withdraw: " + ls + t.toString());
		}

		FacesContext context = FacesContext.getCurrentInstance();

		Connection con = getConnection();
		String[] params = new String[1];

		try {
			con.setAutoCommit(false);

			UserBase user = UsersDAO.getById(con, t.getUserId());
			t.setProcessedWriterId(AdminManager.getWriterId());
			t.setTimeSettled(new Date());
			t.setUtcOffsetSettled(Utils.getUser(t.getUserId()).getUtcOffset());
			t.setStatusId(TransactionsManager.TRANS_STATUS_CANCELED);

			UsersDAO.addToBalance(con, user.getId(), t.getAmount());
			TransactionsDAO.update(con, t);
			GeneralDAO.insertBalanceLog(con, AdminManager.getWriterId(), t
					.getUserId(), Constants.TABLE_TRANSACTIONS, t.getId(),
					Constants.LOG_BALANCE_REJECT_WITHDRAW, Utils.getUser(t.getUserId()).getUtcOffset());

			params[0] = CommonUtil.displayAmount(t.getAmount(), t.getCurrencyId());

			con.commit();

			if (log.isEnabledFor(Level.INFO)) {
				String ls = System.getProperty("line.separator");
				log.log(Level.INFO, " Reject Withdraw finised successfully! "
						+ ls + t.toString());
			}

		} catch (SQLException e) {
			log.log(Level.ERROR, "rejectWithdraw: ", e);

			try {
				con.rollback();
			} catch (SQLException ie) {
				log.log(Level.ERROR, "Can't rollback.", ie);
			}
			throw e;
		} finally {
			con.setAutoCommit(true);
			closeConnection(con);
		}

		FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_INFO,
				CommonUtil.getMessage("cancel.withdraw.success", params, Utils.getWriterLocale(context)), null);
		context.addMessage(null, fm);

		return true;

	}

	public static ArrayList<CreditCard> getAllCreditCardsByUser(long id)
			throws SQLException {
		Connection con = getConnection();
		try {
			return CreditCardsDAO.getByUserId(con, id);
		} finally {
			closeConnection(con);
		}
	}

	public static ArrayList getCreditCardsByUserIdAndFilters(long user_id, int documentStatusFilter,int depositsStatusFilter, int documentsRequiredFilter) throws SQLException {
		Connection con = getConnection();
		try {
			return CreditCardsDAO.getByUserIdAndFilters(con, user_id, documentStatusFilter, depositsStatusFilter, documentsRequiredFilter);
		} finally {
			closeConnection(con);
		}
	}
	
	public static ArrayList getAttachedFilesForCC(long user_id, ArrayList<CreditCard> cardList) throws SQLException {
		Connection con = getConnection();
		try {
			return CreditCardsDAO.getAttachedFilesForCC(con, user_id, cardList);
		} finally {
			closeConnection(con);
		}
	}
	
	public static ArrayList<CreditCard> setCreditCardBinInfo(long user_id, ArrayList<CreditCard> cardList) throws SQLException {
		Connection con = getConnection();
		try {
			return CreditCardsDAO.setCreditCardBinInfo(con, user_id, cardList);
		} finally {
			closeConnection(con);
		}
	}
	
	public static void updateCheque(Cheque cheque) throws SQLException {
		FacesContext context = FacesContext.getCurrentInstance();
		Connection con = getConnection();
		try {
			ChequeDAO.update(con, cheque);
			FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_INFO,
					CommonUtil.getMessage("card.cheque.success", null, Utils.getWriterLocale(context)), null);
			context.addMessage(null, fm);

		} finally {
			closeConnection(con);
		}
	}


	/**
	 * Update fee cancle
	 * @param chequeId
	 * 			cheque id
	 * @param feeCancle
	 * 			true in case we want to cancle the fee
	 * @throws SQLException
	 */
	public static void updateFeeCheque(long chequeId, boolean feeCancel) throws SQLException {
		Connection con = getConnection();
		try {
			ChequeDAO.updateFee(con, chequeId, feeCancel);
		} finally {
			closeConnection(con);
		}
	}


	/**
	 * Update feeCancle field after transaction editing
	 * @param transaction
	 * @throws SQLException
	 */
	public static void updateFeeInTransaction(Transaction transaction, boolean displayMsg) throws SQLException {
		FacesContext context = FacesContext.getCurrentInstance();
		Connection con = getConnection();
		try {
			TransactionsDAO.updateFeeCancel(con, transaction);
			if (displayMsg) {
				FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_INFO,
						CommonUtil.getMessage("update.withdrawal.success", null, Utils.getWriterLocale(context)), null);
				context.addMessage(null, fm);

			}
		} finally {
			closeConnection(con);
		}
	}

	/**
	 * Update feeCancle by admin field after transaction editing
	 * @param feeCancel
	 * @param id  transaction id
	 * @throws SQLException
	 */
	public static void updateFeeCancelByAdmin(boolean feeCancel, long id) throws SQLException {
		Connection con = getConnection();
		try {
			TransactionsDAO.updateFeeCancelByAdmin(con, feeCancel, id);
		} finally {
			closeConnection(con);
		}
	}

	public static void updateIsCreditWithdrawal(boolean isCreditWithdrawal, long id) throws SQLException {
		Connection con = getConnection();
		try {
			TransactionsDAO.updateIsCreditWithdrawal(con, isCreditWithdrawal, id);
		} finally {
			closeConnection(con);
		}
	}

	public static void updateCreditCard(CreditCard c)	throws SQLException, InvalidKeyException, NoSuchAlgorithmException,
														NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException,
														UnsupportedEncodingException, InvalidAlgorithmParameterException {
		FacesContext context = FacesContext.getCurrentInstance();
		Connection con = getConnection();
		try {
			log.debug("Update card details, writerId: " + Utils.getWriter().getWriter().getUserName() + ", " + c.toString());
			CreditCardsDAO.update(con, c);
			FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_INFO, CommonUtil.getMessage("card.update.success", null, Utils.getWriterLocale(context)), null);
			context.addMessage(null, fm);

		} finally {
			closeConnection(con);
		}
	}

	public static ArrayList<Transaction> getTransactions(long userId, Date from, Date to,
			long typeId, long statusId, String searchBy, String receiptNum,
			String ccNum, boolean timeSearch, String userName, long classId,String skins,
			long countryId, long skinBusinessCaseId, long hasInvestment, long startHour, long endHour,
			long currencyId, long campaignId, long writerId,long providerId,boolean byPhone, Date arabicStartDate,
			long paymentMethodId, int hasBalance, int salesDepositsType, int salesDepositsSource, long chosenBankWireId, 
			long clearingProvider, int ccBin, long wasRerouted, long isManualRouting, long transactionId, int isPostponed, long providerTransactionId,
			String affiliatesKeys, long is3d) throws SQLException {
		Connection con = getSecondConnection();
		try {
			return TransactionsDAO.getTransactions(con, userId, from, to,
					typeId, statusId, searchBy, receiptNum, ccNum, timeSearch, userName, classId, skins, 
					countryId, skinBusinessCaseId, hasInvestment, startHour, endHour, currencyId, campaignId, writerId,providerId,byPhone, 
					arabicStartDate, paymentMethodId, hasBalance, salesDepositsType, salesDepositsSource, chosenBankWireId, clearingProvider, 
					ccBin, wasRerouted, isManualRouting, transactionId, isPostponed, providerTransactionId,
					affiliatesKeys, is3d);
		} finally {
			closeConnection(con);
		}
	}

	public static ArrayList<Transaction> getTransactions(long id1, BigDecimal id2)
			throws SQLException {
		Connection con = getConnection();
		try {
			return TransactionsDAO.getTransactions(con, id1, id2);
		} finally {
			closeConnection(con);
		}
	}

	public static long getNumberOfDeposits(long userId) throws SQLException {

		Connection conn = null;
		try {
			conn = getSecondConnection();

			return TransactionsDAO.getNumberOfDeposits(conn, userId);

		} finally {
			closeConnection(conn);
		}

	}

	public static long getNumberOfWithdraws(long userId) throws SQLException {

		Connection conn = null;
		try {
			conn = getSecondConnection();

			return TransactionsDAO.getNumberOfWithdraws(conn, userId);

		} finally {
			closeConnection(conn);
		}

	}

	/**
	 * Get all transactions that gonna capture today
	 * @return
	 * @throws SQLException
	 */
	public static ArrayList<CaptureDailyInfo> getCaptureDailyInfo(long investStatus) throws SQLException {
		Connection con = null;
		try {
			con = getConnection();
			return TransactionsDAO.getCaptureDailyInfo(con, investStatus);
		} finally {
			closeConnection(con);
		}

	}

	/**
	 * Insert bonus to user with dynamic amount
	 *
	 * @param wagering wagering available to sAdmin
	 */
	public static boolean insertBonusDeposit(UserBase user, long bonusUserId,
			long writerId, long amount, String comments, long wagering, boolean sAdmin) throws SQLException {

		Connection con = getConnection();
		try {
			// create bonusUser instance by amount and wagering
			BonusUsers bonusUser = new BonusUsers();
			bonusUser.setUserId(user.getId());
			bonusUser.setBonusId(ConstantsBase.BONUS_INSTANT_DYNAMIC_AMOUNT);
			bonusUser.setWriterId(writerId);
			bonusUser.setComments(comments);

			Bonus b = BonusManagerBase.getBonusById(con, bonusUser.getBonusId());
			if (sAdmin) {
				b.setWageringParameter(wagering);
			}

			BonusCurrency bc = new BonusCurrency();
			bc.setBonusAmount(amount);

			return BonusManagerBase.insertBonusUser(bonusUser, user, writerId, con, bc, b, 0, 0, false);

		} finally {
			closeConnection(con);
		}
	}

	/**
	 * Insert transaction by typeId
	 * @param f depositForm instance
	 * @param userId
	 * @param userName
	 * @param transTypeId transaction type id for insert
	 * @return
	 * @throws SQLException
	 */
	public static boolean insertTransactionByType(String amount, String comments, long userId, String userName,
			 int transTypeId, int logBalanceId, boolean isDeposit) throws SQLException {

		if (log.isEnabledFor(Level.INFO)) {
			String ls = System.getProperty("line.separator");
			log.log(Level.INFO, "Insert transaction: " + ls
					+ "typeId: " + transTypeId + ls
					+ "user: " + userName + ls
					+ "amount:" + amount);
		}

		FacesContext context = FacesContext.getCurrentInstance();
        Transaction tran = null;
		Connection con = getConnection();
		try {
			con.setAutoCommit(false);

			tran = new Transaction();
			tran.setAmount(CommonUtil.calcAmount(amount));
			tran.setCurrency(Utils.getUser(userId).getCurrency());
			tran.setComments(comments);
			tran.setCreditCardId(null);
			tran.setDescription(null);
			tran.setIp(CommonUtil.getIPAddress());
			tran.setProcessedWriterId(AdminManager.getWriterId());
			tran.setChequeId(null);
			tran.setTimeSettled(new Date());
			tran.setTimeCreated(new Date());
			tran.setUtcOffsetCreated(Utils.getUser(userId).getUtcOffset());
			tran.setUtcOffsetSettled(Utils.getUser(userId).getUtcOffset());
			tran.setTypeId(transTypeId);
			tran.setUserId(userId);
			tran.setWriterId(AdminManager.getWriterId());
			tran.setWireId(null);
			tran.setChargeBackId(null);
			tran.setStatusId(TRANS_STATUS_SUCCEED);
			tran.setReceiptNum(null);

			long amountVal = CommonUtil.calcAmount(amount);
			String msgKey = "transaction.insert.success.deposit";
			if (!isDeposit) {
				amountVal = -amountVal;
				msgKey = "transaction.insert.success.withdrawal";
			}
			UsersDAO.addToBalance(con, userId, amountVal);
			TransactionsDAO.insert(con, tran);
			GeneralDAO.insertBalanceLog(con, AdminManager.getWriterId(),
					tran.getUserId(), Constants.TABLE_TRANSACTIONS, tran
							.getId(), logBalanceId, Utils.getUser(userId).getUtcOffset());

			con.commit();
			String[] params = new String[1];
			params[0] = CommonUtil.displayAmount(tran.getAmount(), tran.getCurrencyId());
			FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_INFO,
					CommonUtil.getMessage(msgKey, params, Utils.getWriterLocale(context)), null);
			context.addMessage(null, fm);


			if (log.isEnabledFor(Level.INFO)) {
				log.log(Level.INFO, "Transaction inserted successfully!");
			}
            try {
                UserBase user = getById(userId);
                Utils.alertOverDepositLimitByEmail(tran, user);
            } catch (RuntimeException e) {
                log.error("Exception sending email alert to traders! ", e);
            }
		} catch (SQLException e) {
			log.log(Level.ERROR, "insertTransaction: ", e);
			try {
				con.rollback();
			} catch (SQLException ie) {
				log.log(Level.ERROR, "Can't rollback.", ie);
			}
			throw e;
		} finally {
			con.setAutoCommit(true);
			closeConnection(con);
		}

        return true;
	}

	/**
	 * Get trx by id for cancel
	 * @param id
	 * @return
	 * @throws SQLException
	 */
	public static Transaction getByIdForCancel(long id) throws SQLException {
		Connection con = getConnection();
		try {
			return TransactionsDAO.getByIdForCancel(con, id);
		} finally {
			closeConnection(con);
		}
	}

	public static void approveDeposit(Transaction trans, int status) throws SQLException {

		String depositType = "wire";
		String depositMsg =  "transactions.wire.deposit.approve.success";

		if (trans.isCashDeposit()){ //cash deposit
			depositType = "cash";
			depositMsg =  "transactions.cash.deposit.approve.success";
		}else if(trans.isPayPalDeposit()){
			depositType = "paypal";
			depositMsg =  "transactions.paypal.deposit.approve.success";
		}

		if (log.isEnabledFor(Level.INFO)) {
			String ls = System.getProperty("line.separator");
			log.log(Level.INFO, "Approve "+ depositType +" Deposit: " + ls + "transaction: "					+ trans + ls);
		}

		FacesContext context = FacesContext.getCurrentInstance();
		Connection con = getConnection();

		TransactionsDAO.updateStatusId(
							con,
							trans.getId(),
							status,
							true,Utils.getWriter().getWriter().getId());

		FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_INFO,
				CommonUtil.getMessage(depositMsg, null, Utils.getWriterLocale(context)), null);
		context.addMessage(null, fm);

	}

	public static ArrayList<Transaction> getDepositTrxForCredit(long userId, long ccId, String clearingProviderId, boolean isNot) throws SQLException {
		Connection con = getConnection();
		try {
			return TransactionsDAOBase.getDepositTrxForCredit(con, userId, ccId, clearingProviderId, false);
		} finally {
			closeConnection(con);
		}
	}

	public static void updateClearingProviderId(long tranId, long clearingProviderId) throws SQLException {
		Connection con = getConnection();
		try {
			TransactionsDAO.updateClearingProviderId(con, tranId, clearingProviderId);
		} finally {
			closeConnection(con);
		}
	}

	/**
	 * Validate inatec CFT withdraw
	 * @param amount
	 * @param card
	 * @param writerId
	 * @param feeCancel
	 * @param user
	 * @return
	 * @throws SQLException
	 */
	public static String validateInatecWithdraw(String amount, CreditCard card, long writerId , boolean feeCancel, User user) throws SQLException{

		FacesContext context = FacesContext.getCurrentInstance();
		String errorMsg = null;
		// prevent withdraw from maestro or diners cards
		AnyOptionCreditCardValidator.Maestro maestro = new  AnyOptionCreditCardValidator.Maestro();
		AnyOptionCreditCardValidator.Diners diners = new  AnyOptionCreditCardValidator.Diners();
		if (maestro.matches(String.valueOf(card.getCcNumber())) || diners.matches(String.valueOf(card.getCcNumber()))){
			errorMsg = "error.creditcard.diners.maestro";
			FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR,CommonUtil.getMessage(errorMsg, null),null);
			context.addMessage(null, fm);

			return errorMsg;
		}

		//check if card is black listed
		boolean isCCBlackList = AdminManager.isCCBlackListedWithdraw(card.getCcNumber());
		if (isCCBlackList){
			errorMsg = "error.creditcard.blacklisted";
			FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR,CommonUtil.getMessage(errorMsg, null),null);
			context.addMessage(null, fm);

			return errorMsg;
		}

		//check if card is bin black listed
		boolean isBinBlackListed = AdminManager.isBINBlacklisted(card, false, user);
		if (isBinBlackListed){
			errorMsg = "error.creditcard.binblacklisted";
			FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR,CommonUtil.getMessage(errorMsg, null),null);
			context.addMessage(null, fm);

			return errorMsg;
		}

		return errorMsg;
	}

	public static transactionUpdateStatus updateTransaction(long trxId, long writerId, long typeId, long statusId, long providerId, String capturedProviderId, long transactionClassId) throws SQLException {
		Connection con = getConnection();
		try {
			return TransactionsDAO.updateTransaction(con, trxId, writerId, typeId, statusId, providerId, capturedProviderId, transactionClassId);
		} finally {
			closeConnection(con);
		}
	}
	
	public static transactionUpdateStatus updateTransaction(long trxId, long writerId, long typeId, long statusId, String capturedProviderId, long transactionClassId) throws SQLException {
		return updateTransaction(trxId, writerId, typeId, statusId, 0, capturedProviderId, transactionClassId);
	}


	/**
	 * approveEnvoyWithdraw
	 * @param t
	 * 		Transaction to approve
	 * @param fee
	 * 		fee amount
	 * @param admin
	 * 		if we came from admin or accounting
	 * @return
	 * @throws SQLException
	 */
	public static boolean approveEnvoyWithdraw(Transaction t, long fee, boolean admin) throws SQLException {
		if (log.isEnabledFor(Level.INFO)) {
			String ls = System.getProperty("line.separator");
			log.log(Level.INFO, "Approve Envoy Withdraw: " + ls + t.toString() + ls
					+ "Amount: " + t.getAmount() + ls + " fee: " + fee + ls
					+ " admin: " + admin + ls);
		}
		FacesContext context = FacesContext.getCurrentInstance();
		Connection con = getConnection();
		try {
			con.setAutoCommit(false);
			t.setProcessedWriterId(Utils.getWriter().getWriter().getId());

			if (admin) {  // admin
				t.setStatusId(TransactionsManager.TRANS_STATUS_APPROVED);
			} else {  // from accounting tab
				User user = Utils.getUser(t.getUserId());

				t.setAccountingApproved(true);
				TransactionsDAO.updateAfterAccouningApproved(con, t);
				t.setUtcOffsetSettled(user.getUtcOffset());

				EnvoyInfo info = new EnvoyInfo(user,t);
				PaymentMethod pm = getPaymentMethodById(t.getPaymentTypeId());
		        info.setRefundCountryCode(pm.getEnvoyWithdrawCountryCode());
		        info.setAccountNumber(t.getEnvoyAccountNum());
		        info.setRequestReference(String.valueOf(t.getId()));
		        info.setRefundDestination(EnvoyClearingProvider.REFUND_DEST_SERVICE);

		        if (!t.isFeeCancel()){
	 	            fee = LimitsDAO.getFeeByTranTypeId(con, user.getLimitIdLongValue(), TRANS_TYPE_ENVOY_WITHDRAW);
		            if (fee > 0){
		            	info.setAmount(t.getAmount() - fee);
		            }
	            }

		        try{
		        	ClearingManager.withdraw(info);
		        	if(info.isSuccessful()) {
		        		t.setStatusId(TRANS_STATUS_SUCCEED);
		        	}
		        }catch (ClearingException e) {
					log.error("Problem with sending withdraw to envot for tansaction: " + t.getId(),e);
				}
			}
			TransactionsDAO.update(con, t);

			if (log.isEnabledFor(Level.INFO)) {
				String ls = System.getProperty("line.separator");
				log.log(Level.INFO, " approve Envoy Withdraw finished successfully! "
						+ ls);
			}
			con.commit();
		} catch (SQLException e) {
			log.log(Level.ERROR, "approveEnvoyWithdraw: ", e);
			try {
				con.rollback();
			} catch (SQLException ie) {
				log.log(Level.ERROR, "Can't rollback.", ie);
			}
			throw e;
		} finally {
			con.setAutoCommit(true);
			closeConnection(con);
		}

		FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_INFO,
				CommonUtil.getMessage("approve.withdraw.success", null, Utils.getWriterLocale(context)), null);
		context.addMessage(null, fm);

		return true;
	}

	/**
	 * Approve General Withdraw (for now we are using for Moneybookers)
	 * @param t
	 * 		Transaction to approve
	 * @param fee
	 * 		fee amount
	 * @param admin
	 * 		if we came from admin or accounting
	 * @return
	 * @throws SQLException
	 */
	public static boolean approveGeneralWithdraw(Transaction t, long fee, boolean admin) throws SQLException {
		if (log.isEnabledFor(Level.INFO)) {
			String ls = System.getProperty("line.separator");
			log.log(Level.INFO, "Approve general withdraw: " + ls + t.toString() + ls
					+ "Amount: " + t.getAmount() + ls + " fee: " + fee + ls
					+ " admin: " + admin + ls);
		}
		FacesContext context = FacesContext.getCurrentInstance();
		Connection con = getConnection();
		try {
			con.setAutoCommit(false);
			t.setProcessedWriterId(Utils.getWriter().getWriter().getId());
			if (admin) {  // admin
				t.setStatusId(TransactionsManager.TRANS_STATUS_APPROVED);
			} else {  // from accounting tab
				User user = Utils.getUser(t.getUserId());
				t.setAccountingApproved(true);
				TransactionsDAO.updateAfterAccouningApproved(con, t);
				t.setUtcOffsetSettled(user.getUtcOffset());
				t.setTimeSettled(new Date());
				t.setUtcOffsetSettled(Utils.getUser(t.getUserId()).getUtcOffset());
				t.setStatusId(TransactionsManager.TRANS_STATUS_SUCCEED);
			}
			TransactionsDAO.update(con, t);
			if (log.isEnabledFor(Level.INFO)) {
				String ls = System.getProperty("line.separator");
				log.log(Level.INFO, " approve general withdraw finished successfully! "
						+ ls);
			}
			con.commit();
		} catch (SQLException e) {
			log.log(Level.ERROR, "approve general withdraw: " , e);
			try {
				con.rollback();
			} catch (SQLException ie) {
				log.log(Level.ERROR, "Can't rollback.", ie);
			}
			throw e;
		} finally {
			con.setAutoCommit(true);
			closeConnection(con);
		}

		FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_INFO,
				CommonUtil.getMessage("approve.withdraw.success", null, Utils.getWriterLocale(context)), null);
		context.addMessage(null, fm);

		return true;
	}



	/**
	 * Get credit cards details for risk issues
	 * @param user_id
	 * @return
	 * @throws SQLException
	 */
	public static ArrayList<CreditCard> getCCByUserId(long user_id, long issueId, long reasonId) throws SQLException {
		Connection con = getSecondConnection();
		try {
			return CreditCardsDAO.getCCByUserId(con, user_id, issueId, reasonId);
		} finally {
			closeConnection(con);
		}
	}

	public static HashMap<CreditCard, ArrayList<Transaction>> getCreditCardDeposits(long userId, Date from, Date to) throws SQLException{
		Connection con = getConnection();
		try {
			return TransactionsDAO.getCreditCardDeposits(con, userId, from, to);
		} finally {
			closeConnection(con);
		}
	}
	
	public static ArrayList<Transaction> getNonCreditCardDepositsByUserId(long userId, Date from, Date to) throws SQLException {
		Connection con = getConnection();
		try {
			return TransactionsDAO.getNonCreditCardDepositsByUserId(con, userId, from, to);
		} finally {
			closeConnection(con);
		}
	}

	public static Transaction getNonCcDeposits(long userId) throws SQLException{
		Connection con = getConnection();
		try {
			return TransactionsDAO.getNonCcDeposits(con, userId);
		} finally {
			closeConnection(con);
		}
	}
	

	public static String getWebMoneyDetailsByPurseNumber(String purseNumber) throws SQLException{
		Connection con = getSecondConnection();
		try {
			return TransactionsDAO.getWebMoneyDetailsByPurseNumber(con, purseNumber);
		} finally {
			closeConnection(con);
		}
	}

    public static User getNoneIssueTransactionByTime() throws SQLException {
		Connection con = getConnection();
		try {
			return TransactionsDAO.getNoneIssueTransactionByTime(con);
		} finally {
			closeConnection(con);
		}
    }

	public static long getSumDepositByDateRangeAndUserId(long userId, Date sumOfDepositsDateFrom, Date sumOfDepositsDateTo) throws Exception {
		Connection con = getConnection();
		try {
			return TransactionsDAO.getSumDepositByDateRangeAndUserId(con, userId, sumOfDepositsDateFrom, sumOfDepositsDateTo);
		} finally {
			closeConnection(con);
		}
	}

	public static boolean userMadeDepositsInLastXDays(long userId, long lastXDays) throws SQLException {
		Connection con = getConnection();
		try {
			return TransactionsDAO.userMadeDepositsInLastXDays(con, userId, new Date(new Date().getTime() - (lastXDays *24*60*60*1000)));
		} finally {
			closeConnection(con);
		}
	}
	
	public static long getSumBonuses(long userId) throws SQLException {
		Connection con = getSecondConnection();
		try {
			return TransactionsDAO.getSumBonusesTransactions(con, userId);
		} finally {
			closeConnection(con);
		}
	}
	
	/**
	 * The function return all transaction that were inserted mentally to TRANSACTION_ISSUES.  
	 * @param startDate
	 * @param endDate
	 * @param writerFilter
	 * @return
	 */
	public static ArrayList<TransactionReport> getTransactionInsertedManually(Date startDate, Date endDate, String writerFilter) {
		ArrayList<TransactionReport> vo = new ArrayList<TransactionReport>();
		Connection con = null;
		try {
			con = getConnection();
			return TransactionsDAO.getTransactionInsertedManually(con, startDate, endDate, writerFilter);
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			closeConnection(con);
		}
		return vo;
	}
	
	public static ArrayList<ReroutingLimit> getReroutingLimit() throws SQLException {
		Connection con = getConnection();
        try {
            return TransactionsDAO.getReroutingLimit(con);
        } finally {
            closeConnection(con);
        }
	}
	
	public static boolean setReroutingLimit(ReroutingLimit reroutingLimit) throws SQLException {		
		boolean res = false;
		Connection con = getConnection();
        try {
        	res = TransactionsDAO.setReroutingLimit(con, reroutingLimit,Constants.INATEC);
            if(res){
            	res = TransactionsDAO.setReroutingLimit(con, reroutingLimit,Constants.WIRE_CARD);
            }
            if(res){
            	res = TransactionsDAO.setReroutingLimit(con, reroutingLimit,Constants.TBI);
            }
            if(res){
            	res = TransactionsDAO.setReroutingLimit(con, reroutingLimit,Constants.WIRE_CARD_3D);
            }
            if(res){
            	res = TransactionsDAO.setReroutingLimit(con, reroutingLimit,Constants.WALPAY);
            }            
            return res;
        } finally {
            closeConnection(con);
        }
	}
	
	public static ArrayList<SelectItem> getFeeList() throws SQLException{
		Connection con = getConnection();
		try {
			return TransactionsDAO.getFeeList(con);
		} finally {
			closeConnection(con);
		}
	}
	
	public static ArrayList<FeeCurrencyMap> getFeeCurrencyList() throws SQLException{
		Connection con = getConnection();
		try {
			return TransactionsDAO.getFeeCurrencyList(con);
		} finally {
			closeConnection(con);
		}
	}
	
	public static void updateFeeCurrencyMap(FeeCurrencyMap map) throws SQLException {
		Connection con = getConnection();
		try {
			TransactionsDAO.updateFeeCurrencyMap(con, map);
		} finally {
			closeConnection(con);
		}
	}
	
	public static ArrayList getWithdrawlsList() throws SQLException {
		Connection con = getConnection();
		try {
			return TransactionsDAO.getWithdrawls(con);
		} finally {
			closeConnection(con);
		}
		
	}
	
	public static long getFirstWithdrawls(Date firstDayOfMonth, Date dateTransaction, long userId) throws SQLException {
		Connection con = getConnection();
		try {
			return TransactionsDAO.getFirstWithdrawls(con, firstDayOfMonth, dateTransaction, userId);
		} finally {
			closeConnection(con);
		}
		
	}
	public static String checkUserStatus(User user) throws SQLException {
		if(user.getSkin().isRegulated() && user.getSkin().getId() != Skins.SKIN_ETRADER) {
			UserRegulationBase userRegulation = new UserRegulationBase();
			userRegulation.setUserId(user.getId());
			UserRegulationManager.getUserRegulationBackend(userRegulation);
			HashMap<String, String> hmReturnMsg = new HashMap<String, String>();
			hmReturnMsg = UserRegulationManager.getAORegulationState(userRegulation, user.getSkinId());
			if(hmReturnMsg != null && !hmReturnMsg.isEmpty()) {
				if(hmReturnMsg.get("CODE_ID") != CommonJSONService.ERROR_CODE_SHOW_WARNING_DUE_DOCUMENTS + "") {
					return hmReturnMsg.get("ERROR_MSG");
				}
			}
		}
		return null;
	}
		
	public static ArrayList<DepositsSummary> getDepositsSummaryList(long userId) throws Exception{
		Connection con = getConnection();
		try {
			return TransactionsDAO.getDepositsSummaryList(con, userId);
		} finally {
			closeConnection(con);
		}
	}
	
	public static Map<Long, String> getAllVisibleTransactionTypesList() throws SQLException {
		Connection con = getConnection();
		try {
			if (transactionTypes == null || transactionTypes.isEmpty()) {
				transactionTypes = TransactionTypesDAO.getAllVisible(con);
			}
			return transactionTypes;
		} finally {
			closeConnection(con);
		}
	}
	
	public static	ArrayList<CreditCard>
			getCcHolderNameHistory(ArrayList<CreditCard> cardList)	throws SQLException, CryptoException, InvalidKeyException,
																	NoSuchAlgorithmException, NoSuchPaddingException,
																	IllegalBlockSizeException, BadPaddingException,
																	UnsupportedEncodingException, InvalidAlgorithmParameterException {
		Connection con = getConnection();
		try {
			return CreditCardsDAO.getCcHolderNameHistory(con, cardList);
		} finally {
			closeConnection(con);
		}
	}
	
	public static boolean isDisplaySkrill(long userId) throws SQLException {
		Connection con = getConnection();
		try {
			return TransactionsDAO.isDisplaySkrill(con, userId);
		} finally {
			closeConnection(con);
		}
	}
}