package il.co.etrader.backend.bl_managers;

import il.co.etrader.backend.dao_managers.TiersDAO;
import il.co.etrader.bl_managers.TiersManagerBase;
import il.co.etrader.bl_vos.TierUserHistory;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;

import org.apache.log4j.Logger;

public class TiersManager extends TiersManagerBase {

    private static final Logger logger = Logger.getLogger(TiersManager.class);

    /**
     * Get user points history
     * @return
     * @throws SQLException
     */
	public static ArrayList<TierUserHistory> getUserPointsHistory (long userId) throws SQLException {
		Connection con = getConnection();
		try {
			return TiersDAO.getTierUserHistory(con, userId);
		} finally {
			closeConnection(con);
		}
	}

	/**
	 * Update userTier
	 * @param userId
	 * @param tierId
	 * @param writerId
	 * @return
	 * @throws SQLException
	 */
	public static boolean updateUserTier(long userId, long tierId, long writerId, long qualifDifPoints) throws SQLException {
		Connection con = null;
		boolean res = false;
		logger.debug("Going to update tierUser, userId: " + userId + " , newTier: " + tierId);
		try {
			con = getConnection();
			TiersDAO.updateUserTier(con, userId, tierId, writerId, qualifDifPoints);
			res = true;
		} finally {
			closeConnection(con);
		}
		return res;
	}


}
