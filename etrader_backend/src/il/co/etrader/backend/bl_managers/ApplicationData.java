package il.co.etrader.backend.bl_managers;
import il.co.etrader.backend.bl_vos.User;
import il.co.etrader.backend.dao_managers.ApiIssuesDAO;
import il.co.etrader.backend.dao_managers.BonusDAO;
import il.co.etrader.backend.dao_managers.FilesDAO;
import il.co.etrader.backend.dao_managers.FraudsDAO;
import il.co.etrader.backend.dao_managers.IssuesDAO;
import il.co.etrader.backend.dao_managers.MailBoxTemplatesDAO;
import il.co.etrader.backend.dao_managers.MarketingAffilatesDAO;
import il.co.etrader.backend.dao_managers.MarketingCampaignsDAO;
import il.co.etrader.backend.dao_managers.MarketingPaymentTypesDAO;
import il.co.etrader.backend.dao_managers.MarketingPixelsDAO;
import il.co.etrader.backend.dao_managers.MarketsDAO;
import il.co.etrader.backend.dao_managers.OpportunitiesDAO;
import il.co.etrader.backend.dao_managers.PopulationsDAO;
import il.co.etrader.backend.dao_managers.RiskAlertsDAO;
import il.co.etrader.backend.dao_managers.StaticLPDAO;
import il.co.etrader.backend.dao_managers.TargetsDAO;
import il.co.etrader.backend.dao_managers.TournamentDAO;
import il.co.etrader.backend.dao_managers.UsersDAO;
import il.co.etrader.backend.mbeans.AnalForm;
import il.co.etrader.backend.mbeans.DailyInvestmentsForm;
import il.co.etrader.backend.mbeans.InvestmentsForm;
import il.co.etrader.backend.mbeans.MasterSearchForm;
import il.co.etrader.backend.mbeans.PopulationsEntriesForm;
import il.co.etrader.backend.mbeans.RewardsUserHistoryForm;
import il.co.etrader.backend.mbeans.SalesDepositsForm;
import il.co.etrader.backend.mbeans.SalesDepositsTVForm;
import il.co.etrader.backend.mbeans.SalesTurnoversForm;
import il.co.etrader.backend.util.Constants;
import il.co.etrader.backend.util.Utils;
import il.co.etrader.bl_managers.ApplicationDataBase;
import il.co.etrader.bl_managers.PopulationsManagerBase;
import il.co.etrader.bl_managers.TiersManagerBase;
import il.co.etrader.bl_vos.PopulationType;
import il.co.etrader.bl_vos.Skins;
import il.co.etrader.bl_vos.Writer;
import il.co.etrader.dao_managers.ClearingDAO;
import il.co.etrader.dao_managers.CreditCardTypesDAO;
import il.co.etrader.dao_managers.MarketingCombinationsDAOBase;
import il.co.etrader.dao_managers.MarketsDAOBase;
import il.co.etrader.dao_managers.OpportunityOddsTypesDAO;
import il.co.etrader.dao_managers.SkinsDAOBase;
import il.co.etrader.dao_managers.WritersDAO;
import il.co.etrader.issueActions.IssueActionType;
import il.co.etrader.util.CommonUtil;
import il.co.etrader.util.CommonUtil.selectItemComparator;
import il.co.etrader.util.ConstantsBase;

import java.sql.Connection;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;

import javax.el.ValueExpression;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;

import com.anyoption.common.beans.Country;
import com.anyoption.common.beans.Enumerator;
import com.anyoption.common.beans.FileType;
import com.anyoption.common.beans.IssueStatus;
import com.anyoption.common.beans.Market;
import com.anyoption.common.beans.SkinCurrency;
import com.anyoption.common.beans.Tier;
import com.anyoption.common.beans.base.CreditCardType;
import com.anyoption.common.beans.base.Currency;
import com.anyoption.common.beans.base.Skin;
import com.anyoption.common.daos.ApiDAOBase;
import com.anyoption.common.daos.BanksDAOBase;
import com.anyoption.common.daos.CountryDAOBase;
import com.anyoption.common.daos.CurrenciesDAOBase;
import com.anyoption.common.daos.GeneralDAO;
import com.anyoption.common.daos.LanguagesDAOBase;
import com.anyoption.common.enums.CopyOpInvTypeEnum;
import com.anyoption.common.jms.BackendLevelsCache;
import com.anyoption.common.jms.WebLevelLookupBean;
import com.anyoption.common.managers.MarketsManagerBase;
import com.copyop.common.enums.CoinsActionTypeEnum;
//import il.co.etrader.bl_vos.Enumerator; //TODO: remove it after 2 deploys

/**
 * @author liors
 *
 */
public class ApplicationData extends ApplicationDataBase {
	//Application Objects HM
	public static HashMap<String, HashMap> applicationObjectsHm;

	//Application SI's
	public static HashMap<String, ArrayList<SelectItem>> applicationSIHm;

    protected long defaultFee;
    protected String writerOffset;
    public static HashMap<Long, Writer> retentionWriters;
    public static ArrayList<Writer> retentionWritersList;
    public static HashMap<Long,Writer> writersHM;
    public static ArrayList<IssueActionType> actionTypesList;
    public static LinkedHashMap<Long,IssueActionType> actionTypesHm;
    public static ArrayList<SelectItem> riskAlertsType;
    public static ArrayList<SelectItem> fileRejectReasons;
    public static HashMap<Long, ArrayList<SelectItem>> rejectReasonsByType;
    public static HashMap<Long, PopulationType> populationsTypes;
    public static HashMap<Long,FileType> fileTypes;
    public static HashMap<Integer, ArrayList<String>> callCenterHours;
    private static ArrayList<SelectItem> prioritySI;
    private static ArrayList<SelectItem> mediaBuyersWriters;
    public static Map<Integer, String> skinBusinessCases;
    public static Map<Integer, String> clearingProviders;
    public static Map<Integer, Skin> skinsMap;
    public static Map<Integer, Currency> currenciesMap;
    public static Map<Integer, Country> countriesMap;
    public static Map<Integer, CreditCardType> creditCardTypesMap;
    private static ArrayList<SelectItem> customerReportTypeSI;
    private static Map<Integer, String> ccClearingProviders;


	public static Date timeLastSalesTvDbCheck;

    public static void init() throws SQLException {
        ApplicationDataBase.init();

		Connection con = getConnection();
    	try {
    		//Application Objects
    		//init HashMaps
    		applicationObjectsHm = new HashMap<String, HashMap>();
    		applicationSIHm = new HashMap<String, ArrayList<SelectItem>>();

    		HashMap hm = null;

    		//issue subjects
       		hm = IssuesDAO.getIssueSubjects(con, Constants.ISSUE_SUBJECTS);
    		applicationObjectsHm.put("issueSubjects", hm);
    		applicationSIHm.put("issueSubjectsSI", CommonUtil.hashMap2SelectListString(hm, "getId", "getName"));

    		//risk reasons
       		hm = IssuesDAO.getRiskReasons(con);
    		applicationObjectsHm.put("riskReason", hm);
    		applicationSIHm.put("riskReasonSI", CommonUtil.hashMap2SelectListString(hm, "getId", "getName"));

    		//issue reached statuses
       		hm = IssuesDAO.getIssueReachedStatuses(con);
    		applicationObjectsHm.put("issueReachedStatuses", hm);
    		applicationSIHm.put("issueReachedStatusesSI", CommonUtil.hashMap2SelectListString(hm, "getId", "getName"));

    		// issue actionTypes
    		actionTypesHm = IssuesDAO.getIssueActionTypes(con);
    		applicationObjectsHm.put("issueActionTypes", actionTypesHm);
    		applicationSIHm.put("issueActionTypesSI", CommonUtil.hashMap2SelectListString(actionTypesHm, "getId", "getName"));

    		actionTypesList = IssuesDAO.getIssueActionTypesList(con);

    		//issue priorities
       		hm = IssuesDAO.getIssuePrioritues(con);
    		applicationObjectsHm.put("issuePriorities", hm);
    		applicationSIHm.put("issuePrioritiesSI", CommonUtil.hashMap2SelectListString(hm, "getId", "getName"));

    		//issue statuses
       		hm = IssuesDAO.getIssueStatus(con, IssueStatus.ISSUE_STATUS_TYPE_ISSUE);
    		applicationObjectsHm.put("issueStatuses", hm);
    		applicationSIHm.put("issueStatusesSI", CommonUtil.hashMap2SelectListString(hm, "getId", "getName"));

    		//files risk statuses
       		hm = IssuesDAO.getFilesRiskStatus(con);
    		applicationObjectsHm.put("riskStatuses", hm);
    		applicationSIHm.put("riskStatusesSI", CommonUtil.hashMap2SelectListString(hm, "getId", "getName"));
    		
    		// file reject reasons
    		fileRejectReasons = FilesDAO.getFileRejectReasonList(con);
    		rejectReasonsByType = FilesDAO.getFileRejectReasonsByFileType(con);
    		
    		//issue channels
       		hm = IssuesDAO.getIssueChannels(con);
    		applicationObjectsHm.put("issueChannels", hm);
    		applicationSIHm.put("issueChannelsSI", CommonUtil.hashMap2SelectListString(hm, "getId", "getName"));

    		//file types
    		fileTypes = FilesDAO.getFileTypes(con);
    		hm = fileTypes;
    		applicationObjectsHm.put("fileTypes", hm);
    		applicationSIHm.put("fileTypesSI", CommonUtil.hashMap2SelectListString(hm, "getId", "getName"));

    		//fraud type
       		hm = FraudsDAO.getFraudTypes(con);
    		applicationObjectsHm.put("fraudTypes", hm);
    		applicationSIHm.put("fraudTypesSI", CommonUtil.hashMap2SelectListString(hm, "getId", "getName"));

       		//markets and groups SI (special SI)
    		//applicationSIHm.put("marketsAndGroupsSI", MarketsDAO.getMarketsAndGroupsSI(con));

    		//active markets and groups SI (special SI)
            //applicationSIHm.put("activeMarketsAndGroupsSI", MarketsDAO.getActiveMarketsAndGroupsSI(con));

//          active binary 0100 markets and groups SI (special SI)
            //applicationSIHm.put("activeBinary0100MarketsAndGroupsSI", MarketsDAO.getActiveBinary0100MarketsAndGroupsSI(con));

//          active markets and groups SI (special SI)
            //applicationSIHm.put("activeMarketsAndGroupsWithOneTouchSI", MarketsDAO.getActiveMarketsAndGroupsWithOneTouchSI(con));

//          active option plus markets SI
            //applicationSIHm.put("activeOptionPlusMarketsSI", MarketsDAO.getActiveMarketsSI(con, Opportunity.TYPE_OPTION_PLUS));

    		//markets by agrrement
//       	hm = MarketsDAO.getMarketsByAgreement(con);
//    		applicationObjectsHm.put("marketsByAgreement", hm);
//    		applicationSIHm.put("marketsByAgreementSI", CommonUtil.hashMap2SelectList(hm, "getId", "getDisplayNameKey"));

       		//markets
//       	hm = MarketsDAO.getAllMarkets(con);
//    		applicationObjectsHm.put("markets", hm);
//    		applicationSIHm.put("marketsSI", CommonUtil.hashMap2SelectList(hm, "getId", "getDisplayNameKey"));

       		//market groups
//    		hm = MarketsDAOBase.getMarketGroups(con);
//    		applicationObjectsHm.put("marketGroups", hm);
//    		applicationSIHm.put("marketGroupsSI", addAllOption(CommonUtil.hashMap2SelectList(hm, "getId", "getDisplayName")));

    		//markets zeroOneHundred
    		hm = MarketsDAO.getAllZeroOneHundredMarkets(con); // TODO
    		applicationObjectsHm.put("marketsZeroOneHundred", hm);
    		applicationSIHm.put("marketsZeroOneHundredSI", addAllOption(CommonUtil.hashMap2SelectList(hm, "getId", "getDisplayNameKey")));

       		//investment statuses
    		applicationObjectsHm.put("investmentStatuses", null);
    		applicationSIHm.put("investmentStatusesSI", CommonUtil.enumSI(ConstantsBase.INVESTMENT_CLASS_STATUS_DROPDOWN));

       		//transaction types
    		applicationObjectsHm.put("transactionTypes", null);
    		applicationSIHm.put("transactionTypesSI", CommonUtil.enumSI(ConstantsBase.XOR_TRANSACTION_FORM_CLASS_TYPE_DROPDOWN));

       		//currencies
    		hm = CurrenciesDAOBase.getAllCurrencies(con);
    		applicationObjectsHm.put("currencies", hm);
    		applicationSIHm.put("currenciesSI", CommonUtil.hashMap2SelectList(hm, "getId", "getDisplayName"));

       		//languages
    		hm = LanguagesDAOBase.getAllLanguagess(con);
    		applicationObjectsHm.put("languages", hm);
    		applicationSIHm.put("languagesSI", CommonUtil.hashMap2SelectList(hm, "getId", "getDisplayName"));

       		//odds types
    		hm = OpportunityOddsTypesDAO.getTypes(con);
    		applicationObjectsHm.put("oddsTypes", hm);
    		applicationSIHm.put("oddsTypesSI", CommonUtil.hashMap2SelectList(hm, "getId", "getName"));

       		//exchanges
    		hm = OpportunitiesDAO.getExchanges(con);
    		applicationObjectsHm.put("exchanges", hm);
    		applicationSIHm.put("exchangesSI", CommonUtil.hashMap2SelectList(hm, "getId", "getName"));

       		//investment limit groups
    		hm = invLimitisGroupsMap;
    		applicationObjectsHm.put("investmentLimitGroups", hm);
    		applicationSIHm.put("investmentLimitGroupsSI", CommonUtil.hashMap2SelectList(hm, "getId", "getGroupNameDisplay"));

       		//opportunity types
    		hm = OpportunitiesDAO.getOpportunityTypes(con);
    		applicationObjectsHm.put("opportunityTypes", hm);
    		applicationSIHm.put("opportunityTypesSI", CommonUtil.hashMap2SelectList(hm, "getId", "getDescription"));

    		//Product Type
//    		hm = OpportunitiesDAO.getOpportunityProductTypes(con);
//    		applicationObjectsHm.put("opportunityProductTypes", hm);
//    		applicationSIHm.put("opportunityProductTypesSI", CommonUtil.hashMap2SelectListString(hm, "getId", "getName"));

       		//timezone types
    		applicationObjectsHm.put("timezoneTypes", null);
    		applicationSIHm.put("timezoneTypesSI", getTimeZoneTypes());

    		//marketing payment types
    		applicationSIHm.put("marketingPaymentTypes", MarketingPaymentTypesDAO.getList(con));
    		
    		//marketing channels
    		applicationSIHm.put("marketingChannelsTypes", MarketingCampaignsDAO.getMarketingChannels(con));

    		//population types
    		populationsTypes = PopulationsManager.getPopulationTypes(con);
    		applicationObjectsHm.put("populationTypes", populationsTypes);
    		applicationSIHm.put("populationTypesSI", CommonUtil.hashMap2SelectList(populationsTypes, "getId", "getName"));

    		ArrayList<SelectItem> popTypeDecline = new ArrayList<SelectItem>();
    		popTypeDecline.add(new SelectItem(new Long(PopulationsManagerBase.POP_TYPE_DECLINE_LAST),"population.type.decline.last"));

    		applicationSIHm.put("populationTypesDeclineSI", popTypeDecline);

    		retentionWriters = PopulationsDAO.getRetentionWriters(con);

    		retentionWritersList = PopulationsDAO.getRetentionWritersList(con);

    		//population types
    		hm = MarketingAffilatesDAO.getAffiliatesHM(con);
    		applicationObjectsHm.put("marketingAffiliates", hm);
    		applicationSIHm.put("marketingAffiliatesSI", CommonUtil.hashMap2SelectList(populationsTypes, "getId", "getName"));

    		//bonus types
            applicationSIHm.put("bonusTypesSI", BonusDAO.getAllBonusTypes(con));

            //bonus states
            applicationSIHm.put("bonusStatesSI", BonusDAO.getAllBonusStates(con));

            // pixel types
            applicationSIHm.put("pixelTypesSI", MarketingPixelsDAO.getAllPixelTypes(con));

            // skin business cases
            skinBusinessCases = SkinsDAOBase.getSkinBusinessCasesMap(con);
            applicationSIHm.put("skinBusinessCasesSI", CommonUtil.map2SelectList(skinBusinessCases));

			// providers
            clearingProviders = ClearingDAO.getProvidersNameMaping(con);
            applicationSIHm.put("providersSI", CommonUtil.map2SelectList(clearingProviders));
            
            // cc providers
            ccClearingProviders = ClearingDAO.getCCProvidersNameMaping(con);
            applicationSIHm.put("ccProvidersSI", CommonUtil.map2SelectList(getCCClearingProviders()));

            // countriesGroup
            applicationSIHm.put("countriesGroup", CountryDAOBase.getCountriesGroupType(con));

            //user actions
            //applicationSIHm.put("userActionsSI", BonusDAO.getAllUserActions(con));

            //risk alert type
            riskAlertsType = RiskAlertsDAO.getRiskAlertDescription(con);

            // mailBox popup types
            applicationSIHm.put("mailboxPopupTypes", MailBoxTemplatesDAO.getPopupTypes(con));

            // marketing affiliates keys
            applicationSIHm.put("affiliatesNamesAndKeysSI", MarketingAffilatesDAO.getAffiliatesNamesAndKeysSI(con));

            // callCenterHours
            callCenterHours = IssuesDAO.getCallCenterHours(con);

            timeLastSalesTvDbCheck = new Date();
            
            currenciesMap = CurrenciesDAOBase.getCurrenciesMaping(con); 
            
            skinsMap = SkinsDAOBase.getSkinsMaping(con);

            countriesMap = CountryDAOBase.getCountriesMaping(con);
            
            creditCardTypesMap = CreditCardTypesDAO.getCreditCardTypesMaping(con);
            
            createPrioritySI();

            //bank wire withdrawal etrader
            hm = BanksDAOBase.getBanks(con, ConstantsBase.BANK_TYPE_WITHDRAWAL, Skin.SKIN_BUSINESS_ETRADER);
            applicationObjectsHm.put("wirebanksEtrader", hm);
            applicationSIHm.put("wirebanksEtraderSI", CommonUtil.hashMap2SelectList(hm, "getId", "getName"));

//          bank wire withdrawal anyoption
            hm = BanksDAOBase.getBanks(con, ConstantsBase.BANK_TYPE_WITHDRAWAL, Skin.SKIN_BUSINESS_ANYOPTION);
            applicationObjectsHm.put("wirebanksAnyoption", hm);
            applicationSIHm.put("wirebanksAnyoptionSI", CommonUtil.hashMap2SelectList(hm, "getId", "getName"));


            mediaBuyersWriters = TargetsDAO.getMediaBuyers(con);

            applicationSIHm.put("salesRepsSI", PopulationsDAO.getSalesReps(con));
            applicationSIHm.put("salesRepsConversionDepSI", PopulationsDAO.getSalesRepsConversionDep(con));

//          api users
            applicationSIHm.put("apiUsersTypeExternalSI", ApiDAOBase.getApiUsersTypeExternalSI(con));

             // API issues lists include sorting
            ArrayList<SelectItem> ApiIssueChannelSI = ApiIssuesDAO.getApiIssueChannelSI(con);
            Collections.sort(ApiIssueChannelSI, new CommonUtil.selectItemComparator());
            ArrayList<SelectItem> ApiIssueSubjecSI = ApiIssuesDAO.getApiIssueSubjectSI(con);
            Collections.sort(ApiIssueSubjecSI, new CommonUtil.selectItemComparator());
            ArrayList<SelectItem> ApiIssueStatusSI = ApiIssuesDAO.getApiIssueStatusSI(con);
            Collections.sort(ApiIssueStatusSI, new CommonUtil.selectItemComparator());            
            applicationSIHm.put("apiIssueChannelSI", ApiIssueChannelSI) ;
            applicationSIHm.put("apiIssueSubjectSI", ApiIssueSubjecSI);
            applicationSIHm.put("apiIssueStatusSI", ApiIssueStatusSI);
            // users details history types
            applicationSIHm.put("usersDetailsHistoryTypes", UsersDAO.getUsersDetailsHistoryTypes(con));
            
            //static landing page types
            ArrayList<SelectItem> staticLPTypesSI = StaticLPDAO.getStaticLandingPageTypesSI(con);
            Collections.sort(staticLPTypesSI, new CommonUtil.selectItemComparator()); 
            applicationSIHm.put("staticLPTypesSI", staticLPTypesSI);
            
            applicationSIHm.put("marketingUrlSourceSI", MarketingCombinationsDAOBase.getAllUrlSoursecTypes(con));
//            // rewards 
//            ArrayList<SelectItem> rewardTiersSI =  RewardsDAO.getRewardTiersSI(con);
//            Collections.sort(rewardTiersSI, new CommonUtil.selectItemComparator()); 
//            applicationSIHm.put("rewardTiersSI", rewardTiersSI);
//            ArrayList<SelectItem> rewardHistoryTypesSI =  RewardsDAO.getRewardHistoryTypesSI(con);
//            Collections.sort(rewardHistoryTypesSI, new CommonUtil.selectItemComparator()); 
//            applicationSIHm.put("rewardHistoryTypesSI", rewardHistoryTypesSI);
    		//issue component
       		hm = IssuesDAO.getIssueComponent(con);
    		applicationObjectsHm.put("issueComponent", hm);
    		applicationSIHm.put("issueComponentSI", CommonUtil.hashMap2SelectListString(hm, "getId", "getName"));
    		// copyop coins state    		
    		ArrayList<SelectItem> copyopCoinsStateSI = getCopyopCoinsStateList();
    		Collections.sort(copyopCoinsStateSI, new CommonUtil.selectItemComparator());
    		applicationSIHm.put("copyopCoinsStateSI", copyopCoinsStateSI);		
    		// copyop investment types
    		ArrayList<SelectItem> copyopInvTypeSI = getCopyopInvTypeSI();
    		Collections.sort(copyopInvTypeSI, new CommonUtil.selectItemComparator());
    		applicationSIHm.put("copyopInvTypeSI", copyopInvTypeSI);		
    		writersHM = WritersDAO.getAllWritersMap(con);
    		// investment writers with copyop
    		ArrayList<SelectItem> invWritersSI = getWritersGroupSI(writersHM, true);
       		Collections.sort(invWritersSI, new CommonUtil.selectItemComparator());
    		applicationSIHm.put("invWritersSI", invWritersSI);
    		// writers group
    		ArrayList<SelectItem> writersGroupsSI = getWritersGroupSI(writersHM, false);
       		Collections.sort(writersGroupsSI, new CommonUtil.selectItemComparator());
    		applicationSIHm.put("writersGroupsSI", writersGroupsSI);    		
    		// Static Landing Page Path
    		hm = StaticLPDAO.getStaticLandingPagePath(con);
    		applicationObjectsHm.put("staticLandingPagePath", hm);
    		applicationSIHm.put("staticLandingPagePathSI", CommonUtil.hashMap2SelectListString(hm, "getId", "getName"));
    		    		
    		customerReportTypeSI = new ArrayList<SelectItem>();
    		customerReportTypeSI.add(new SelectItem(Constants.CUSTOMER_REPORT_ALL, "All"));
    		customerReportTypeSI.add(new SelectItem(Constants.CUSTOMER_REPORT_FORTNIGHTLY, CommonUtil.getMessage("customer.report.fortnightly", null)));
    		customerReportTypeSI.add(new SelectItem(Constants.CUSTOMER_REPORT_MONTHLY, CommonUtil.getMessage("customer.report.monthly", null)));

    		applicationSIHm.put("tournamentNumberOfUserSI", TournamentDAO.getNumberOfUsersSI(con));
    		applicationSIHm.put("tournamentTypesSI", TournamentDAO.getTypes(con));
    		

    		applicationSIHm.put("userHistoryFieldsSI", UsersDAO.getUserHistoryFieldsSI(con));
    	} finally {
    		closeConnection(con);
    	}
	}

    public static ArrayList<SelectItem> getTimeZoneTypes() throws SQLException {
        ArrayList l = CommonUtil.getEnum(Constants.ENUM_TIMEZONE);
        ArrayList<SelectItem> out = new ArrayList<SelectItem>();
        for (int i = 0; i < l.size(); i++) {
            Enumerator e = (Enumerator)l.get(i);
            out.add(new SelectItem(e.getValue(),e.getValue()));
        }
        Collections.sort(out, new CommonUtil.selectItemComparator());
        return out;
    }

	public static HashMap getApplicationSIHm() {
		return applicationSIHm;
	}

	/**
	 * Return the context WRITERS !!! locale (this methods name was determined to override the name in ApplicationDataBase)
	 */
	@Override
	public Locale getUserLocale() throws SQLException {
		return Utils.getWriter().getLocalInfo().getLocale();
	}

	/**
	 * Return the context writer's locale
	 */
	public Locale getWriterLocale() throws SQLException {
		return Utils.getWriter().getLocalInfo().getLocale();
	}

	/**
	 * This method returns the skin id of the user that exists on the session scope
	 * it was written on the application data to overrode the getSkinId() on ApplicationDataBase so it could be used along with ApplicationData.getSkinId() on the web
	 */
	@Override
	public Long getSkinId() {
		long skinId = Skin.SKIN_ETRADER;
		User user = Utils.getUser();
    	if (user.isSupportSelected() || user.isRetentionSelected()) {
    		skinId = user.getSkinId();
    	}
    	return skinId;
	}

    /**
     * Get UtcOffset with GMT+/-HH:MM format.
     * The UtcOffset is either BE writer derived or a WEB user derived
     * in case a utcOffset is provided and support is selected this would be the utcOffset to be used
     * @param - the utcOffset of the investments\transaction ect
     * @return
     * 		The passed UTC offset in format GMT+/-HH:MM.
     */
    @Override
    public String getUtcOffset(String utcOffset) {
    	//returns User timezone in case of support
    	User user = Utils.getUser();
    	if (user.isSupportSelected()) {
    		if (null != utcOffset && !utcOffset.isEmpty()) {
    			return utcOffset;
    		} else {
    			return user.getUtcOffset();
    		}
    	}

    	//returns writer timezone in case of other then support
    	return Utils.getWriter().getUtcOffset();

    }

    /**
     * Get UtcOffset with GMT+/-HH:MM format.
     * The UtcOffset is either BE writer derived or a WEB user derived
     * @return
     * 		The passed UTC offset in format GMT+/-HH:MM.
     */
    @Override
    public String getUtcOffset() {
    	//returns User timezone in case of support
    	User user = Utils.getUser();
    	if (user.isSupportSelected()) {
    		return user.getUtcOffset();
    	}

    	//returns writer timezone in case of other then support
    	return Utils.getWriter().getUtcOffset();

    }

    public String getWriterOffset(){
    	return Utils.getWriter().getUtcOffset();
    }

    public void setWriterOffset(){
    	this.writerOffset = Utils.getWriter().getUtcOffset();
    }

	/**
	 * This method adds an empty item to a select list
	 * @param list
	 * @return
	 */
	private static ArrayList<SelectItem> addAllOption(ArrayList<SelectItem> list) {
		SelectItem allItem = new SelectItem(ConstantsBase.ALL_FILTER_ID, CommonUtil.getMessage(ConstantsBase.ALL_FILTER_KEY, null));
		list.add(0, allItem);
		return list;
	}

	/**
	 * Get countries list
	 * @return
	 * 		List of countries
	 * @throws SQLException
	 */
    public ArrayList<SelectItem> getCountriesList() throws SQLException {
		ArrayList<SelectItem> translatedList = new ArrayList<SelectItem>();
		Locale locale = getUserLocale();
		for (SelectItem c : countriesList) {
			Long value = (Long) c.getValue();
			String lable = CommonUtil.getMessage(c.getLabel(), null, locale);
			translatedList.add(new SelectItem(value, lable));
		}
		return translatedList;
    }
    
    /**
	 * Get regulated countries list
	 * @return
	 * 		List of regulated countries
	 * @throws SQLException
	 */
    public ArrayList<SelectItem> getCountriesRegulatedList() throws SQLException {
		ArrayList<SelectItem> translatedList = new ArrayList<SelectItem>();
		Locale locale = getUserLocale();
		for (SelectItem c : countriesRegulatedList) {
			Long value = (Long) c.getValue();
			String lable = CommonUtil.getMessage(c.getLabel(), null, locale);
			translatedList.add(new SelectItem(value, lable));
		}
		return translatedList;
    }
 
    /**
	 * Get non regulated countries list
	 * @return
	 * 		List of non regulated countries
	 * @throws SQLException
	 */   
    public ArrayList<SelectItem> getCountriesNotRegulatedList() throws SQLException {
		ArrayList<SelectItem> translatedList = new ArrayList<SelectItem>();
		Locale locale = getUserLocale();
		for (SelectItem c : countriesNotRegulatedList) {
			Long value = (Long) c.getValue();
			String lable = CommonUtil.getMessage(c.getLabel(), null, locale);
			translatedList.add(new SelectItem(value, lable));
		}
		return translatedList;
    }

	/**
	 * Get countries All list
	 * @return
	 * 		List of countries
	 * @throws SQLException
	 */
    public ArrayList<SelectItem> getAllCountriesList() throws SQLException {
		ArrayList<SelectItem> countriesList = new ArrayList<SelectItem>();
		countriesList.add(new SelectItem(new Long(0),"All"));
		countriesList.addAll(getCountriesList());
	return countriesList;
    }

    /**
     * initialize investmentsForm bean
     * @throws SQLException
     * @throws ParseException
     */
    public String getInitInvestmentsForm() throws SQLException, ParseException {
    	FacesContext context = FacesContext.getCurrentInstance();
    	ValueExpression ve = context.getApplication().getExpressionFactory().createValueExpression(context.getELContext(), Constants.BIND_INVESTMENTS_FORM, InvestmentsForm.class);
    	InvestmentsForm invForm = (InvestmentsForm) ve.getValue(context.getELContext());
    	if (null != invForm) { // create a new instance
    		invForm = new InvestmentsForm();
    		ve.setValue(context.getELContext(), invForm);
    	}
    	return null;
    }
    
    /**
     * initialize dailyInvestmentsForm bean
     * @throws SQLException
     * @throws ParseException
     */
    public String getInitDailyInvestmentsForm() throws SQLException, ParseException {
    	FacesContext context = FacesContext.getCurrentInstance();
    	ValueExpression ve = context.getApplication().getExpressionFactory().createValueExpression(context.getELContext(), Constants.BIND_DAILY_INVESTMENTS_FORM, DailyInvestmentsForm.class);
    	DailyInvestmentsForm dayInvForm = (DailyInvestmentsForm) ve.getValue(context.getELContext());
    	if (null != dayInvForm) { // create a new instance
    	    	dayInvForm = new DailyInvestmentsForm();
    		ve.setValue(context.getELContext(), dayInvForm);
    	}
    	return null;
    }

    /**
     * initialize analForm bean
     * @throws SQLException
     */
    public String getInitAnalForm() throws SQLException {
    	FacesContext context = FacesContext.getCurrentInstance();
    	ValueExpression ve = context.getApplication().getExpressionFactory().createValueExpression(context.getELContext(), Constants.BIND_ANAL_FORM, AnalForm.class);
    	AnalForm analForm = (AnalForm) ve.getValue(context.getELContext());
    	if (null != analForm) {   // create a new instance
    		analForm = new AnalForm();
    		ve.setValue(context.getELContext(), analForm);
    	}
    	return null;
    }

    /**
     * initialize PopulationsEntriesForm bean
     * @throws SQLException
     */
    public String getInitPopulationsEntriesForm() throws SQLException {
    	FacesContext context = FacesContext.getCurrentInstance();
    	ValueExpression ve = context.getApplication().getExpressionFactory().createValueExpression(context.getELContext(), Constants.BIND_POPULATION_ENTRIES_FORM, PopulationsEntriesForm.class);
    	PopulationsEntriesForm populationsEntriesForm = (PopulationsEntriesForm) ve.getValue(context.getELContext());
    	if (null != populationsEntriesForm) { // create a new instance
    		populationsEntriesForm.initPopulationsEntriesForm();
    		ve.setValue(context.getELContext(), populationsEntriesForm);
    	}
    	return null;
    }

    /**
     * initialize salesDepositsForm (details screen) bean
     * @throws SQLException
     */
    public String getInitSalesDepositsFormDetails() throws SQLException {
    	FacesContext context = FacesContext.getCurrentInstance();
    	ValueExpression ve = context.getApplication().getExpressionFactory().createValueExpression(context.getELContext(), Constants.BIND_SALES_DEPOSITS_FORM, SalesDepositsForm.class);
    	SalesDepositsForm salesDepositsForm = (SalesDepositsForm) ve.getValue(context.getELContext());
    	if (null != salesDepositsForm) { // create a new instance
    		salesDepositsForm.initSalesDepositsDetails();
    		ve.setValue(context.getELContext(), salesDepositsForm);
    	}
    	return null;
    }

    /**
     * initialize salesDepositsForm (details screen) bean
     * @throws SQLException
     */
    public String getInitSalesTurnoversFormDetails() throws SQLException {
    	FacesContext context = FacesContext.getCurrentInstance();
    	ValueExpression ve = context.getApplication().getExpressionFactory().createValueExpression(context.getELContext(), Constants.BIND_SALES_TURNOVERS_FORM, SalesTurnoversForm.class);
    	SalesTurnoversForm salesTurnoversForm = (SalesTurnoversForm) ve.getValue(context.getELContext());
    	if (null != salesTurnoversForm) { // create a new instance
    		salesTurnoversForm.initSalesTurnoversDetails();
    		ve.setValue(context.getELContext(), salesTurnoversForm);
    	}
    	return null;
    }

    /**
     * initialize PopulationsEntriesForm bean
     * @throws SQLException
     */
    public String getInitMasterSearchForm() throws SQLException {
    	FacesContext context = FacesContext.getCurrentInstance();
    	ValueExpression ve = context.getApplication().getExpressionFactory().createValueExpression(context.getELContext(), Constants.BIND_MASTER_SEARCH_FORM, MasterSearchForm.class);
    	MasterSearchForm masterSearchForm = (MasterSearchForm) ve.getValue(context.getELContext());
    	if (null != masterSearchForm) { // create a new instance
    		masterSearchForm.initMasterSearchForm();
    		ve.setValue(context.getELContext(), masterSearchForm);
    	}
    	return null;
    }

    /**
     * initialize TV deposits Form bean
     * @return
     * @throws SQLException
     */
    public String getInitTVForm() throws SQLException {
    	FacesContext context = FacesContext.getCurrentInstance();
    	ValueExpression ve = context.getApplication().getExpressionFactory().createValueExpression(context.getELContext(), Constants.BIND_SALES_DEPOSITS_TV_FORM, SalesDepositsTVForm.class);
    	SalesDepositsTVForm form = (SalesDepositsTVForm) ve.getValue(context.getELContext());
    	if (null != form) {
    		form.initSalesDepositsTVForm();
    		ve.setValue(context.getELContext(), form);
    	}
    	return null;
    }


    /**
	 * Get all markets by user skin.
	 * @return
	 * 		String[][] - array that contains id,display_name for each market
	 * 		in this skin
	 * @throws SQLException
	 */
	public String getDisplayMarkets() throws SQLException {
		long key;
		ArrayList<Market> marketList = new ArrayList<Market>();
		StringBuffer st = new StringBuffer();
		st.append("[");

		for (Iterator<Long> i = markets.keySet().iterator(); i.hasNext();) {
			key = i.next();
			marketList.add(markets.get(key));
		}
		for (int i = 0; i < marketList.size(); i++) {
			st.append("[\"" + marketList.get(i).getId()).append("\" , \"");
			st.append(marketList.get(i).getDisplayNameKey()).append("\"], ");
		}
		int ind = st.lastIndexOf(",");
		if (ind != -1) {
			st.deleteCharAt(ind);
		}
		st.append("]");

		return st.toString();
	}
	
	/**
	 * Get trader tool published url
	 * @return
	 */
	public String getTraderToolUrl() {
		return CommonUtil.getProperty("trader.tool.url");
	}

    /**
     * Get trader tool published url
     * @return
     */
    public String getTraderTool0100Url() {
        return CommonUtil.getProperty("trader.tool0100.url");
    }

	/**
	 * @return the retentionWriters
	 */
	public static HashMap<Long, Writer> getRetentionWriters() {
		return retentionWriters;
	}

	/**
	 * @param retentionWriters the retentionWriters to set
	 */
	public static void setRetentionWriters(HashMap<Long, Writer> retentionWriters) {
		ApplicationData.retentionWriters = retentionWriters;
	}

	/**
	 * Get all tiers
	 * @return
	 */
	public ArrayList<SelectItem> getTiersSI() {
		ArrayList<SelectItem> list = new ArrayList<SelectItem>();
		ArrayList<Long> l = new ArrayList<Long>();
		for (Iterator<Long> iter = TiersManagerBase.getTiersList().keySet().iterator(); iter.hasNext();) {
			l.add(iter.next());
		}
		Collections.sort(l);
		for (Long tierId : l) {
			Tier t = TiersManagerBase.getTiersList().get(tierId);
			list.add(new SelectItem(tierId, CommonUtil.getMessage(t.getDescription(), null)));
		}
		return list;
	}

	/**
	 * @return the retentionWritersList
	 */
	public static ArrayList<Writer> getRetentionWritersList() {
		return retentionWritersList;
	}

    public static HashMap<Long, Writer> getWritersHM() {
		return writersHM;
	}

	public String getRunWlcCheck()  {
        FacesContext context = FacesContext.getCurrentInstance();
        BackendLevelsCache levelsCache = (BackendLevelsCache) context.getExternalContext().getApplicationMap().get("levelsCache");
        WebLevelLookupBean wllb = new WebLevelLookupBean();
        wllb.setOppId(-1);
        wllb.setFeedName("WLCCHECK");
        levelsCache.getLevels(wllb);
        if (wllb.getDevCheckLevel() != 0 && wllb.getRealLevel() != 0) {
            logger.debug("Wlc check: ok");
            return "wlcok";
        } else {
            logger.debug("Wlc check: not ok");
        }
        return "";
    }

	/**
	 * @return the actionTypesHm
	 */
	public static LinkedHashMap<Long, IssueActionType> getActionTypesHm() {
		return actionTypesHm;
	}

	/**
	 * @return the actionTypesList
	 */
	public static ArrayList<IssueActionType> getActionTypesList() {
		return actionTypesList;
	}

	/**
	 * @return the riskAlertsType
	 */
	public static ArrayList<SelectItem> getRiskAlertsType() {
		return riskAlertsType;
	}

	/**
	 * @param riskAlertsType the riskAlertsType to set
	 */
	public static void setRiskAlertsType(ArrayList<SelectItem> riskAlertsType) {
		ApplicationData.riskAlertsType = riskAlertsType;
	}

	/**
	 * @return the populationsTypes
	 */
	public static HashMap<Long, PopulationType> getPopulationsTypes() {
		return populationsTypes;
	}

	public static HashMap<Long, FileType> getFileTypes() {
		return fileTypes;
	}

	/**
	 * @return the callCenterHours
	 */
	public static HashMap<Integer, ArrayList<String>> getCallCenterHours() {
		return callCenterHours;
	}

	public static void getNewSalesDepositsFromDb(){
		GregorianCalendar gc = new GregorianCalendar();

		gc.add(GregorianCalendar.MINUTE, -Constants.SALES_TV_DB_CHECK_MINUTES);
		// if SALES_TV_DB_CHECK_MINUTES minutes past since last sales db check, check db for new deposits
		if (timeLastSalesTvDbCheck.before(gc.getTime())){
			try{
				TransactionsIssuesManager.addNewDepositsForTvFromDb(timeLastSalesTvDbCheck);
				timeLastSalesTvDbCheck = new Date();
			}catch (Exception e) {
				logger.error("Couldn't get NewDepositsForTvFromDb ",e);
			}
		}

	}

	private static void createPrioritySI() {
		prioritySI = new ArrayList<SelectItem>();
		prioritySI.add(new SelectItem(Constants.PRIORITY_LOW, "LOW"));
		prioritySI.add(new SelectItem(Constants.PRIORITY_MED, "MED"));
		prioritySI.add(new SelectItem(Constants.PRIORITY_HIGH, "HIGH"));
	}

	/**
	 * @return the prioritySI
	 */
	public ArrayList<SelectItem> getPrioritySI() {
		return prioritySI;
	}
	
    public static ArrayList<SelectItem> getCustomerReportTypeSI() {
		return customerReportTypeSI;
	}

	/**
	 * @return the expYears
	 */
	public ArrayList<SelectItem> getExpYears() {
		return expYears;
	}

    /**
     * @return the mediaBuyersWriters
     */
    public static ArrayList<SelectItem> getMediaBuyersWriters() {
        return mediaBuyersWriters;
    }

    /**
     * @param mediaBuyersWriters the mediaBuyersWriters to set
     */
    public static void setMediaBuyersWriters(
            ArrayList<SelectItem> mediaBuyersWriters) {
        ApplicationData.mediaBuyersWriters = mediaBuyersWriters;
    }
    
    /**
     * Get real currency id, first search in the session and after that in the cookies.
     * @return
     *      currencyId
     */
    public long getRealCurrencyId() {
        User user = Utils.getUser();
        long skinId = user.getSkinId(); 
        long defultCurrencyId = ConstantsBase.CURRENCY_USD_ID;
        if (skinId == Skin.SKIN_ETRADER || skinId == Skin.SKIN_TLV) {
            defultCurrencyId = ConstantsBase.CURRENCY_ILS_ID;
        }
        String currencyId = Long.toString(user.getCurrencyId());
        if (null != currencyId) {
            return Long.parseLong(currencyId);
        }    
        return defultCurrencyId;
    }
    
    public int getCurrencyDecimalPointDigits() {
        return getCurrencyById(getRealCurrencyId()).getDecimalPointDigits();
    }
    
    public String getInputFieldPattern() {
        String pattern = "\\d*.\\d{0," + getCurrencyDecimalPointDigits() + "}";
        return pattern;
    }
    
	public String getRunDbCheck(int connectionNumber)  {
		Connection con = null;
		long timeBefore = 0;
		long timeAfter = 0;

		try {
			 timeBefore = System.currentTimeMillis();
			 if (connectionNumber == 2) {
				 con = getSecondConnection();
			 } else {
				 con = getConnection();	 
			 }			 
			 GeneralDAO.getTestQuery(con);
			 timeAfter = System.currentTimeMillis();
			 logger.debug("Db check: ok, time: " + String.valueOf(timeAfter-timeBefore) + "ms");
			 return "dbok";

		} catch (Exception e) {
			logger.debug("Db check: not ok, time: " + String.valueOf(timeAfter-timeBefore) + "ms");

		} finally {
			try {
			closeConnection(con);
			} catch (Exception e){}
		}
		return "";
	}
	
	//var map = {"key1":"value1","key2":"value2"}
	public String getJSMarketMap() {
		StringBuffer jsMap = new StringBuffer();
		jsMap.append("{");
		for (Entry<Long, Market> entry : markets.entrySet()) {
			Long marketId = entry.getKey();
			Market market = entry.getValue();
			jsMap.append("\"");
			jsMap.append(marketId);
			jsMap.append("\"");
			jsMap.append(":");
			jsMap.append("\"");
			jsMap.append(com.anyoption.common.daos.MarketsDAOBase.getMarketNameExtention(String.valueOf(marketId), MarketsManagerBase.getMarketName(Skins.SKIN_REG_EN, marketId), "hh", market.getTypeId()));
			jsMap.append("\"");
			jsMap.append(",");
		}
		jsMap.append("}");
		return jsMap.toString();
	}
	
	/**
     * initialize rewardsForm bean
     * @throws SQLException
     * @throws ParseException
     */
    public String getInitRewardsUserHistoryForm() throws SQLException, ParseException {
    	FacesContext context = FacesContext.getCurrentInstance();
    	ValueExpression ve = context.getApplication().getExpressionFactory().createValueExpression(context.getELContext(), Constants.BIND_REWARDS_USER_HISTORY_FORM, RewardsUserHistoryForm.class);
    	RewardsUserHistoryForm form = (RewardsUserHistoryForm) ve.getValue(context.getELContext());
    	if (null != form) { // create a new instance
    		form = new RewardsUserHistoryForm();
    		ve.setValue(context.getELContext(), form);
    	}
    	return null;
    }

	/**
	 * Build SI of Copyop coins state 
	 * @return
	 */
	public static ArrayList<SelectItem> getCopyopCoinsStateList() {
		ArrayList<SelectItem> list = new ArrayList<SelectItem>() ;
	    for(CoinsActionTypeEnum item: CoinsActionTypeEnum.values()) {
	    	if (item.isBackendAction()) {
	    		list.add(new SelectItem(item.getId(), item.getDisplayName()));
	    	}
	    }
	    return list;
	}
	
	/**
	 * Build SI of Copyop investment type 
	 * @return
	 */
	public static ArrayList<SelectItem> getCopyopInvTypeSI() {
		ArrayList<SelectItem> list = new ArrayList<SelectItem>() ;
	    for(CopyOpInvTypeEnum item: CopyOpInvTypeEnum.values()) {
	    	list.add(new SelectItem(item.getCode(), item.getDisplayName()));
	    }
	    return list;
	}
	
	/**
	 * Build group SI for writers 
	 * 1. AO/ET WEB   
	 * 2. AO/ET MOBILE 
	 * 3. COPYOP WEB
	 * 4. COPYOP MOBILE
	 * 5. API_GATEWAY
	 * @return
	 */
	public static ArrayList<SelectItem> getWritersGroupSI(HashMap<Long,Writer> allWritersMap, boolean includeAPI) {
		ArrayList<SelectItem> writers = new ArrayList<SelectItem>();
		Writer webWriter = allWritersMap.get(1L); // AO/ET WEB
		Writer mobileWriter = allWritersMap.get(200L); // AO/ET MOBILE
		Writer copyopWriterMobile = allWritersMap.get(10000L); // COPYOP MOBILE
		Writer copyopWriterWeb = allWritersMap.get(20000L); // COPYOP WEB
        Writer apiGatewayWriter = allWritersMap.get(868L); // api_gateway        
                                                  
        // AO/ET WEB
        writers.add(new SelectItem(String.valueOf(webWriter.getId())        										         							
        										, "AO/ET " + webWriter.getUserName()));
        // AO/ET MOBILE
        writers.add(new SelectItem(String.valueOf(mobileWriter.getId())         							
												, "AO/ET " + mobileWriter.getUserName()));
        // COPYOP WEB
        writers.add(new SelectItem(String.valueOf(copyopWriterWeb.getId())         							
												, copyopWriterWeb.getUserName()));
        // COPYOP MOBILE
        writers.add(new SelectItem(String.valueOf(copyopWriterMobile.getId())         							
												, copyopWriterMobile.getUserName()));
        if (includeAPI) {
        	//API gateway  
        	writers.add(new SelectItem(String.valueOf(apiGatewayWriter.getId()), apiGatewayWriter.getUserName()));
        } 
		return writers;
	}
		
	
	
	public ArrayList<SelectItem> getCurrenciesBySkin() throws SQLException {

        User user = Utils.getUser();
                  
        ArrayList<SkinCurrency> skinCurrencies = UsersManager.getCurrenciesBySkin(user.getSkinId());
        
		ArrayList<SelectItem> currencies = new ArrayList<SelectItem>();

		for (SkinCurrency sc : skinCurrencies) {
			currencies.add(new SelectItem(Long.valueOf(sc.getCurrencyId()),
					CommonUtil.getMessage(CommonUtil.getCurrencySymbolAndCode(sc.getCurrencyId()), null)));
		}
		Collections.sort(currencies, new selectItemComparator());
		
        
        return currencies;
        		
	}
	
	/**
	 * @return the clearingProviders
	 */
	public Map<Integer, String> getClearingProviders() {
		return clearingProviders;
	}
	
	public static Map<Integer, String> getClearingProvidersStatic() {
		return clearingProviders;
	}
	
	public static Map<Integer, Currency> getCurrenciesMap() {
		return currenciesMap;
	}
	
	public static Map<Integer, Skin> getSkinsMap() {
		return skinsMap;
	}
	
	public static Map<Integer, Country> getCountriesMap() {
		return countriesMap;
	}
	
	public static Map<Integer, CreditCardType> getCreditCardTypesMap() {
		return creditCardTypesMap;
	}

	/**
	 * @return the ccClearingProviders
	 */
	public static Map<Integer, String> getCCClearingProviders() {
		return ccClearingProviders;
	}

	/**
	 * @param ccClearingProviders the ccClearingProviders to set
	 */
	public static void setCcClearingProviders(Map<Integer, String> ccClearingProviders) {
		ApplicationData.ccClearingProviders = ccClearingProviders;
	}
} 