package il.co.etrader.backend.bl_managers;

import il.co.etrader.backend.bl_vos.ApiIssue;
import il.co.etrader.backend.dao_managers.ApiIssuesDAO;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;

import org.apache.log4j.Logger;

import com.anyoption.common.managers.BaseBLManager;

/**
 * @author EranL
 *
 */
public class ApiIssuesManager extends BaseBLManager {

	private static final Logger log = Logger.getLogger(ApiIssuesManager.class);
			
	/**
	 * Insert API issue 
	 * @param apiIssue
	 * @throws SQLException
	 */
	public static void insertApiIssue(ApiIssue apiIssue) throws SQLException {
        Connection con = getConnection();
        try {
        	log.debug("Going to insert API Issue");
            ApiIssuesDAO.insertApiIssue(con, apiIssue);
            log.debug("Inserted API issue successfully: " + apiIssue.toString());
        } finally {
            closeConnection(con);
        }
    }
	
	/**
	 * Update API issue 
	 * @param apiIssue
	 * @throws SQLException
	 */
	public static void updateApiIssue(ApiIssue apiIssue) throws SQLException {
        Connection con = getConnection();
        try {
        	log.debug("Going to update API Issue" );
            ApiIssuesDAO.updateApiIssue(con, apiIssue);
            log.debug("Updated API issue successfully: " + apiIssue.toString());
        } finally {
            closeConnection(con);
        }
    }
	
	/**
	 * Search API issues for external user
	 * @param apiIssue
	 * @throws SQLException
	 */
	public static ArrayList<ApiIssue> searchApiIssues(long apiExternalUserId, Date from, Date to, long statusId, long channelId, long subjectId) throws SQLException {
        Connection con = getSecondConnection();
        try {
        	log.debug("Going to get API IssueS for apiExternalUserId:" + apiExternalUserId );
            return ApiIssuesDAO.searchApiIssues(con, apiExternalUserId, from, to, statusId, channelId, subjectId);            
        } finally {
            closeConnection(con);
        }
    }
}

