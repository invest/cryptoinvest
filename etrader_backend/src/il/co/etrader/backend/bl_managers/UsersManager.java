package il.co.etrader.backend.bl_managers;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Locale;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import com.anyoption.backend.service.userfields.Address;
import com.anyoption.common.beans.CreditCard;
import com.anyoption.common.beans.File;
import com.anyoption.common.beans.FileType;
import com.anyoption.common.beans.IssueStatus;
import com.anyoption.common.beans.LimitationDeposits;
import com.anyoption.common.beans.SkinCurrency;
import com.anyoption.common.beans.SkinLanguage;
import com.anyoption.common.beans.base.PendingUserWithdrawalsDetailsBase;
import com.anyoption.common.beans.base.Skin;
import com.anyoption.common.beans.base.UserRegulationBase;
import com.anyoption.common.beans.base.UsersDetailsHistory;
import com.anyoption.common.daos.CurrenciesDAOBase;
import com.anyoption.common.daos.LanguagesDAOBase;
import com.anyoption.common.managers.FilesManagerBase;
import com.anyoption.common.managers.UserRegulationManager;
import com.anyoption.common.sms.RegisterSMSThread;
import com.anyoption.common.util.MarketingTrackerBase;
//import com.copyop.common.dao.FrozenCopyopDao;
//import com.copyop.common.dto.Frozen;
//import com.copyop.common.dto.Profile;
//import com.copyop.common.managers.ProfileManager;

import il.co.etrader.backend.bl_vos.TCAccessToken;
import il.co.etrader.backend.bl_vos.TCAccessTokenSite;
import il.co.etrader.backend.bl_vos.User;
import il.co.etrader.backend.bl_vos.UserDetailsHistoryChanges;
import il.co.etrader.backend.bl_vos.UserFiles;
import il.co.etrader.backend.bl_vos.UserRegulationHistory;
import il.co.etrader.backend.bl_vos.WriterWrapper;
import il.co.etrader.backend.dao_managers.FilesDAO;
import il.co.etrader.backend.dao_managers.FilesDAOFilter;
import il.co.etrader.backend.dao_managers.FraudsDAO;
import il.co.etrader.backend.dao_managers.InvestmentsDAO;
import il.co.etrader.backend.dao_managers.IssuesDAO;
import il.co.etrader.backend.dao_managers.TransactionsDAO;
import il.co.etrader.backend.dao_managers.UsersDAO;
import il.co.etrader.backend.dao_managers.UsersMarketDisableDAO;
import il.co.etrader.backend.mbeans.DepositForm;
import il.co.etrader.backend.mbeans.EditUserForm;
import il.co.etrader.backend.mbeans.FraudsForm;
import il.co.etrader.backend.mbeans.RegisterForm;
import il.co.etrader.backend.mbeans.WithdrawForm;
import il.co.etrader.backend.util.Constants;
import il.co.etrader.backend.util.Utils;
import il.co.etrader.bl_managers.ApplicationDataBase;
import il.co.etrader.bl_managers.ContactsManager;
import il.co.etrader.bl_managers.IssuesManagerBase;
import il.co.etrader.bl_managers.PopulationsManagerBase;
import il.co.etrader.bl_managers.TaxHistoryManagerBase;
import il.co.etrader.bl_managers.TransactionsManagerBase;
import il.co.etrader.bl_managers.UsersManagerBase;
import il.co.etrader.bl_vos.Contact;
import il.co.etrader.bl_vos.Fraud;
import il.co.etrader.bl_vos.TaxHistory;
import il.co.etrader.bl_vos.Template;
import il.co.etrader.bl_vos.UserBase;
import il.co.etrader.bl_vos.UserMarketDisable;
import il.co.etrader.bl_vos.Writer;
import il.co.etrader.dao_managers.ContactsDAO;
import il.co.etrader.dao_managers.CreditCardsDAO;
import il.co.etrader.dao_managers.FilesDAOBase;
import il.co.etrader.dao_managers.LimitsDAO;
import il.co.etrader.dao_managers.SkinCurrenciesDAOBase;
import il.co.etrader.dao_managers.SkinLanguagesDAOBase;
import il.co.etrader.dao_managers.TemplatesDAO;
import il.co.etrader.dao_managers.UsersDAOBase;
import il.co.etrader.dao_managers.WritersDAO;
import il.co.etrader.util.CommonUtil;
import il.co.etrader.util.ConstantsBase;
import il.co.etrader.util.SendDepositErrorEmail;
import il.co.etrader.util.SendEmailChangedNoficationEmail;
import il.co.etrader.util.SendReceiptEmail;

public class UsersManager extends UsersManagerBase {

	private static final Logger logger = Logger.getLogger(UsersManager.class);
	private static List<TCAccessTokenSite> tcAccessSites;

	private static boolean validateRegisterForm(UserBase u) throws SQLException {

		FacesContext context = FacesContext.getCurrentInstance();

		boolean validate = true;
		Connection con = getConnection();
		try {
			if (u.getBirthYear() == null || u.getBirthYear().equals("") || u.getBirthMonth() == null || u.getBirthMonth().equals("") || u.getBirthDay() == null || u.getBirthDay().equals("")) {
				validate = false;
				FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR,CommonUtil.getMessage("error.mandatory", null, Utils.getWriterLocale(context)),null);
				context.addMessage("userForm:birthDay", fm);
			} else {
				try {
					Calendar cal = Calendar.getInstance();
					cal.clear();
					cal.set(Integer.parseInt(u.getBirthYear()), Integer.parseInt(u.getBirthMonth()), Integer.parseInt(u.getBirthDay()));
					cal.add(Calendar.YEAR, 18);
					Calendar curCal = Calendar.getInstance();
					curCal.add(Calendar.MONTH, 1);
					// if the current day is NOT after the 18th birthdate, ergo can not make deposit since < 18
					if (!(curCal).after(cal)) {
						validate = false;
						FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR,CommonUtil.getMessage("error.register.birthdate", null, Utils.getWriterLocale(context)),null);
						context.addMessage("userForm:birth", fm);
					}
				} catch (Exception e) {
				}
			}

			if (UsersDAO.isUserNameInUse(con, u.getUserName(), true, null)) {
				validate = false;
				FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR,CommonUtil.getMessage("error.register.username.inuse", null, Utils.getWriterLocale(context)),null);
				context.addMessage("userForm:userName", fm);
			}

			if (CommonUtil.isHebrewSkin(u.getSkinId()) && UsersDAOBase.isUserIdNumberInUse(con, u.getIdNum(), u.getSkinId())) {
				validate = false;
				FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR,CommonUtil.getMessage("error.register.useridnum.inuse", null, Utils.getWriterLocale(context)),null);
				context.addMessage("userForm:idNum", fm);
			}

			if (CommonUtil.isHebrewSkin(u.getSkinId())) {
				try {
					CommonUtil.validateHebrewOnly(null, null, u.getCityName());
				} catch (Exception e) {
					validate = false;
					FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR,CommonUtil.getMessage("error.city.nothebrew", null, Utils.getWriterLocale(context)),null);
					context.addMessage("userForm:cityId", fm);
				}
				if (u.getCityId() == 0) {
					validate = false;
					FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR,CommonUtil.getMessage("error.register.city", null, Utils.getWriterLocale(context)),null);
					context.addMessage("userForm:cityId", fm);
				}
				if (u.getMobilePhone() == null || u.getMobilePhone().equals("")) {

					FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR,CommonUtil.getMessage("error.mandatory", null, Utils.getWriterLocale(context)),null);
					context.addMessage("userForm:mobilePhone", fm);
					return false;
				}
				
				if (CommonUtil.isParameterEmptyOrNull(u.getStreet())) {

					FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR,CommonUtil.getMessage("error.mandatory", null, Utils.getWriterLocale(context)),null);
					context.addMessage("userForm:street", fm);
					return false;
				}
				
				if (CommonUtil.isParameterEmptyOrNull(u.getStreetNo())) {

					FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR,CommonUtil.getMessage("error.mandatory", null, Utils.getWriterLocale(context)),null);
					context.addMessage("userForm:streetno", fm);
					return false;
				}
				
				if (CommonUtil.isParameterEmptyOrNull(u.getZipCode())) {

					FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR,CommonUtil.getMessage("error.mandatory", null, Utils.getWriterLocale(context)),null);
					context.addMessage("userForm:zipCode", fm);
					return false;
				}
				
				if (CommonUtil.isParameterEmptyOrNull(u.getGender())) {

					FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR,CommonUtil.getMessage("error.mandatory", null, Utils.getWriterLocale(context)),null);
					context.addMessage("userForm:gender", fm);
					return false;
				}
				
				if (CommonUtil.isHebrewSkin(u.getSkinId()) && u.getMobilePhone().length() != ConstantsBase.MOBILE_DIGITS_NUMBER_ON_ETRADER) {

					FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR,CommonUtil.getMessage("error.register.mobile.digit.number", null, Utils.getWriterLocale(context)),null);
					context.addMessage("userForm:mobilePhone", fm);
					return false;
				}
			} else {  // anyoption
				if (u.getMobilePhone() == null || u.getMobilePhone().equals("") &&
							(u.getSkinId() != Skin.SKIN_GERMAN ||
							(u.isContactBySMS() && u.getSkinId() == Skin.SKIN_GERMAN))) {  // allow to insert empty mobile phone for German skin
					FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR,CommonUtil.getMessage("error.mandatory", null, Utils.getWriterLocale(context)),null);
					context.addMessage("userForm:mobilePhone", fm);
					return false;
				}

    			if (UsersDAOBase.isEmailInUse(con, u.getEmail(), 0)) {
    				FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR,
    						CommonUtil.getMessage("error.register.email.inuse",	null, Utils.getWriterLocale(context)), null);
    				context.addMessage("userForm:email", fm);
    				return false;
    			}
			}

		} finally {
			closeConnection(con);
		}
		return validate;

	}

	private static boolean validateEditUserForm(long userId, boolean oldTaxExemption, EditUserForm u, long balance) throws SQLException {

		FacesContext context = FacesContext.getCurrentInstance();

		boolean validate = true;
		Connection con = getConnection();
		try {

			if (balance < 0) {
				validate = false;
				FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR,CommonUtil.getMessage("error.register.negativebalance", null, Utils.getWriterLocale(context)),null);
				context.addMessage("userForm:taxExemption", fm);
			}

			if (u.isTaxExemption() == true && oldTaxExemption == false && InvestmentsDAO.hasInvestments(con, userId, false)) {
				validate = false;
				FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR,CommonUtil.getMessage("error.register.openinvestments", null, Utils.getWriterLocale(context)),null);
				context.addMessage("userForm:taxExemption", fm);
			}

			if (u.getBirthYear() != null && !u.getBirthYear().equals("") && u.getBirthMonth() != null && !u.getBirthMonth().equals("") && u.getBirthDay() != null && !u.getBirthDay().equals("")) {
				try {
					Calendar cal = Calendar.getInstance();
					cal.clear();
					cal.set(Integer.parseInt(u.getBirthYear()), Integer.parseInt(u.getBirthMonth()), Integer.parseInt(u.getBirthDay()));
					cal.add(Calendar.YEAR, 18);
					Calendar curCal = Calendar.getInstance();
					curCal.add(Calendar.MONTH, 1);
					// if the current day is NOT after the 18th birthdate, ergo can not make deposit since < 18
					if (!(curCal).after(cal)) {
						validate = false;
						FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR,CommonUtil.getMessage("error.register.birthdate", null, Utils.getWriterLocale(context)),null);
						context.addMessage("userForm:birthDay", fm);
					}
				} catch (Exception e) {
				}
			}

			//City validation is done for etrader
			if (u.isEtrader()) {
				if (u.getCityName() != null && !u.getCityName().isEmpty()) {
					try {
						CommonUtil.validateHebrewOnly(null, null, u.getCityName());
					} catch (Exception e) {
						FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR,CommonUtil.getMessage("error.city.nothebrew", null, Utils.getWriterLocale(context)),null);
						context.addMessage("userForm:cityId", fm);
						return false;
					}

					if (u.getCityId() == 0) {
						FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR,CommonUtil.getMessage("error.register.city", null, Utils.getWriterLocale(context)),null);
						context.addMessage("userForm:cityId", fm);
						return false;
					}
				}
			}
			if (u.getMobilePhone() == null || u.getMobilePhone().equals("")) {
				if (u.getSkinId() != Skin.SKIN_GERMAN ||
						(u.isContactBySMS() && u.getSkinId() == Skin.SKIN_GERMAN)) {  // allow to insert empty mobile phone for German skin
					FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR,CommonUtil.getMessage("error.mandatory", null, Utils.getWriterLocale(context)),null);
					context.addMessage("userForm:mobilePhone", fm);
					return false;
				}
			}
			if (u.isEtrader() && u.getMobilePhone().length() != ConstantsBase.MOBILE_DIGITS_NUMBER_ON_ETRADER) {

				FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR,CommonUtil.getMessage("error.register.mobile.digit.number", null, Utils.getWriterLocale(context)),null);
				context.addMessage("userForm:mobilePhone", fm);
				return false;
			}
			
			if (CommonUtil.validateIsraeliIdNumber(u.getIdNum())) {
				FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR, CommonUtil.getMessage("error.register.useridnum.invalid.id", null, Utils.getWriterLocale(context)),null);
				context.addMessage("userForm:idNum", fm);
				return false;
			}
			
			if (u.isEtrader() && UsersDAOBase.isUserIdNumForOtherUserId(con, u.getIdNum(), userId, u.getUserName())) {
				FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR, CommonUtil.getMessage("error.register.useridnum.inuse", null, Utils.getWriterLocale(context)),null);
				context.addMessage("userForm:idNum", fm);
				return false;
			}

			if (UsersDAOBase.isEmailInUseIgnoreCase(con, u.getEmail(), userId)) {
				FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR, CommonUtil.getMessage("error.register.email.inuse", null, Utils.getWriterLocale(context)), null);
				context.addMessage("userForm:email", fm);
				return false;
			}
			
			if (!CommonUtil.isEmailValidUnix(u.getEmail())) {
				FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR, CommonUtil.getMessage("error.register.email.invalid", null, Utils.getWriterLocale(context)), null);
				context.addMessage("userForm:email", fm);
				return false;
			}
			
			if (UsersDAOBase.isUserNameInUseIgnoreCase(con, u.getUserName(), userId)) {
				FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR, CommonUtil.getMessage("error.register.username.inuse", null, Utils.getWriterLocale(context)), null);
				context.addMessage("userForm:userName", fm);
				return false;
			}
			
//			// copyop nickname
//			if (!CommonUtil.isParameterEmptyOrNull(u.getCopyopNickname())) {
//				String newNickname = u.getCopyopNickname();
//				String oldNickname = ProfileManager.getProfileNickname(userId);
//				if (!newNickname.equalsIgnoreCase(oldNickname)) { 				
//					String pattern = CommonUtil.getMessage("general.validate.copyop.nickname", null);										
//					if (!u.getCopyopNickname().matches(pattern) || u.getCopyopNickname().trim().equals("")) {
//						FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR, CommonUtil.getMessage("copyop.nickname.validation.err", null, Utils.getWriterLocale(context)), null);
//						context.addMessage("userForm:nickname", fm);
//						return false;
//					}
//				}				
//			}
			
		} finally {
			closeConnection(con);
		}
		return validate;

	}

	/**
	 * Add user from backend and role for him to the db.
	 */

	public static boolean insertUser(UserBase user) throws SQLException {

		if (logger.isEnabledFor(Level.DEBUG)) {
			String ls = System.getProperty("line.separator");
			logger.log(Level.DEBUG, "Insert new user by writerId: " + Utils.getWriter().getWriter().getId() +  " " + ls + user.toString());
		}

		FacesContext context = FacesContext.getCurrentInstance();

		if (!validateRegisterForm(user))
			return false;

		//set tax exemption true in case of anyoption
		else{
			user.setTaxExemption(true);
		}

		user.setKeyword("");
		user.setIp(CommonUtil.getIPAddress());
		user.setBalance(0);
		user.setTaxBalance(0);
		user.setComments("");
		Calendar c = new GregorianCalendar(Integer.parseInt(user.getBirthYear()),Integer.parseInt(user.getBirthMonth()) - 1,Integer.parseInt(user.getBirthDay()),0,1);
		user.setTimeBirthDate(new Date(c.getTimeInMillis()));
		user.setUtcOffsetCreated(Utils.getWriter().getUtcOffset());
		user.setUtcOffsetModified(Utils.getWriter().getUtcOffset());
		user.setWriterId(Utils.getWriter().getWriter().getId());
		Connection con = getConnection();

		long contactId = user.getContactId();
		if(contactId != 0 )	{   // insert contact
			Contact cont = ContactsDAO.getById(con, contactId);
			if (null == cont) {  // dont exist
				FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_INFO,CommonUtil.getMessage("error.contactId", null, Utils.getWriterLocale(context)),null);
				context.addMessage("userForm:contactId", fm);
				return false;
			} else {  // found contact
				user.setCombinationId(cont.getCombId());
				user.setDynamicParam(cont.getDynamicParameter());
			}
		}

		user.setAcceptedTerms(false);

		user.setAuthorizedMail(false);
		if (CommonUtil.isHebrewSkin(user.getSkinId())) {
			user.setAuthorizedMail(false);
		} else {
			user.setAuthorizedMail(true);
		}

        // for setting the skin by us country(if is choosen) in 	registration
	    if (user.getCountryId() == ConstantsBase.COUNTRY_ID_US &&
	             user.getSkinId() != Skin.SKIN_EN_US &&
	             user.getSkinId() != Skin.SKIN_ES_US) {
	         if (user.getSkinId() == Skin.SKIN_SPAIN) {
	             user.setSkinId(Skin.SKIN_ES_US);
	         } else {
	             user.setSkinId(Skin.SKIN_EN_US);
	         }
	         user.setCurrencyId(ConstantsBase.CURRENCY_USD_ID);
	    }
 	    user.setLimitId(LimitsDAO.getBySkinAndCurrency(con, user.getSkinId(), user.getCurrencyId()));

		try {
			con.setAutoCommit(false);

			// Insert new user with contactId check.
			// Also, insert user in users_regulation if the user should be regulated.
			UsersManagerBase.insert(con, user, contactId);
			con.commit();

			try {
    			MarketingTrackerBase.insertMarketingTrackerUserCreatedBE(user.getEmail(), user.getMobilePhone(), user.getLandLinePhone(),
    			        user.getContactId(), user.getId(), user.getSkinId());
    			// Get the contact Id from the user in case it was change in the insert.
    			contactId = user.getContactId();
			} catch (Exception e) {
                logger.error("Marketing Tracker when try to insertMarketingTrackerUserCreatedBE for contactId: "+ contactId , e);
            }

//			try {
//				MarketingETS.etsInsertUserBackend(user.getEmail(), user.getMobilePhone(), user.getLandLinePhone(),
//    			        user.getContactId(), user.getId(), user.getSkinId());
//			} catch (Exception e) {
//				logger.error("Marketing ETS Tracker when trying to insertMarketingTrackerUserCreatedBE for ETS for contactId: " + contactId , e);
//			}

			long userId=0;
			try {
				if (contactId > 0) {
					userId = user.getId();
					ContactsManager.updateContactRequest(contactId, userId);
					PopulationsManagerBase.updateContactPopulation(contactId, userId, Utils.getWriter().getWriter().getId());
					String[] affsub =  com.anyoption.common.daos.UsersDAOBase.getAffSubForContact(con, contactId);
			    	com.anyoption.common.daos.UsersDAOBase.insertAffSub(con, user.getId(), affsub[0], affsub[1], affsub[2]);
			    	con.commit();
				}
			} catch (Exception e) {
				logger.warn("Problem with population insertUser event for contactId: "+ contactId
						+ " for userId: " + userId + e);
			}

			try {
				UsersManagerBase.sendMailTemplateFile(Template.WELCOME_MAIL_ID, CommonUtil.getWebWriterId(), user, null, new String(), new String(), null);
			} catch (Exception e2) {
				logger.error("can't send registration mail!! ", e2);
			}

			FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_INFO,CommonUtil.getMessage("register.success", null, Utils.getWriterLocale(context)),null);
			context.addMessage(null, fm);


			if (logger.isEnabledFor(Level.DEBUG)) {
				String ls = System.getProperty("line.separator");
				logger.log(Level.DEBUG, "Insert user successfully. " + ls);
			}

			/*
			 * The skin id in the session/cookie may not be the same as the
			 * user's, so we should check the user skin.
			 */
			Skin userSkin = ApplicationDataBase.getSkinById(user.getSkinId());
			if (userSkin.isRegulated()) {
				// WC
				UserRegulationBase ur = new UserRegulationBase();
				ur.setUserId(user.getId());
				UserRegulationManager.getUserRegulation(ur);
				ur.setApprovedRegulationStep(UserRegulationBase.REGULATION_USER_REGISTERED);
				ur.setWriterId(user.getWriterId());
			}
			new RegisterSMSThread(user, user.getLocale(), user.getPlatformId(), user.getWriterId()).start();
		} catch (SQLException e) {
			try {
				con.rollback();
			} catch (SQLException ie) {
				logger.log(Level.ERROR, "Can't rollback.", ie);
			}
			throw e;
		} finally {
			con.setAutoCommit(true);
			closeConnection(con);
		}

		return true;

	}

	public static boolean updateUserDetails(User user, EditUserForm edForm) throws SQLException {
		FacesContext context = FacesContext.getCurrentInstance();

		if (logger.isEnabledFor(Level.DEBUG)) {
			String ls = System.getProperty("line.separator");
			logger.log(Level.DEBUG, "Updating user Details:  " + ls + edForm.toString() + ls);
		}

		if (!validateEditUserForm(user.getId(), user.isTaxExemption(), edForm, user.getBalance())) {
			return false;
		}
		boolean classIdChanged = false;

		// change email handle
		String oldEmail = user.getEmail();
		String newEmail = edForm.getEmail();
		String oldUserName = user.getUserName();
		String newUserName = edForm.getUserName();
		WriterWrapper wr = (WriterWrapper) context.getApplication().createValueBinding(Constants.BIND_WRITER_WRAPPER).getValue(context);
		ApplicationData ap = (ApplicationData) context.getApplication().createValueBinding(Constants.BIND_APPLICATION_DATA).getValue(context);
		String eWriterName = wr.getWriter().getUserName();
		long eUserId = user.getId();
		boolean emailChangedNotif = false;
		boolean userNamChangedNotif = false;
		if (!oldEmail.equalsIgnoreCase(newEmail)) {
				emailChangedNotif = true;
				if(oldEmail.equalsIgnoreCase(oldUserName)) {
					edForm.setUserName(newEmail);
					newUserName = edForm.getUserName().toUpperCase();
					userNamChangedNotif = true;
				}
		}
		
		//Handle change of sent deposit document
		boolean isChangeSentDepositDocuments = false;
		if((user.isIdDocVerify() && edForm.getSendDepositDocs() == 0) || 
					(!user.isIdDocVerify()) && edForm.getSendDepositDocs() == 1){
			isChangeSentDepositDocuments = true;
		}
			
		// Check skin and phones change in order to update pop user if relevant
		long newSkinId = edForm.getSkinId();
		UsersManager.checkDetailsChange(eUserId, String.valueOf(user.getSkinId()), String.valueOf(newSkinId), ConstantsBase.USER_CHANGE_DETAILS_SKIN);

		String newMobilePhone = edForm.getMobilePhone();
		String newLandLinePhone = edForm.getLandLinePhone();
		UsersManager.checkDetailsChange(eUserId, user.getMobilePhone() + user.getLandLinePhone(),
				newMobilePhone + newLandLinePhone, ConstantsBase.USER_CHANGE_DETAILS_PHONE_NUMBER);
		user.setUserName(edForm.getUserName());
		user.setTmpUserName(edForm.getUserName());
		user.setSkinId(newSkinId);
		user.setMobilePhone(newMobilePhone);
		user.setLandLinePhone(newLandLinePhone);
		
		Calendar c = null;	
		if(null != edForm.getBirthYear() && !edForm.getBirthYear().equals("") 
				&& null != edForm.getBirthMonth() && !edForm.getBirthMonth().equals("") 
					&& null != edForm.getBirthDay() && !edForm.getBirthDay().equals("")){
			c = new GregorianCalendar(Integer.parseInt(edForm.getBirthYear()), Integer.parseInt(edForm.getBirthMonth()) - 1, Integer.parseInt(edForm.getBirthDay()), 0, 1);
		}
		if(c!= null){
			user.setTimeBirthDate(new Date(c.getTimeInMillis()));
		}		
		user.setIsActive(edForm.getIsActive());
		user.setFirstName(CommonUtil.capitalizeFirstLetters(edForm.getFirstName()));
		user.setLastName(CommonUtil.capitalizeFirstLetters(edForm.getLastName()));
		user.setStreet(edForm.getStreet());
		user.setStreetNo(edForm.getStreetNo());
		user.setCityId(edForm.getCityId());
		user.setCityName(edForm.getCityName());
		user.setZipCode(edForm.getZipCode());
		user.setEmail(edForm.getEmail());
		user.setContactByEmail(edForm.isContactByEmail());
		user.setGender(edForm.getGender());
		user.setIdNum(edForm.getIdNum());
		user.setComments(edForm.getComments());
		user.setCurrencyId(edForm.getCurrencyId());
		if (edForm.getClassId() != user.getClassId()) {
			classIdChanged = true;
		}
		user.setClassId(edForm.getClassId());
		user.setCombinationId(edForm.getCombId());
		user.setLimitId(edForm.getLimitId());
		user.setCountryId(edForm.getCountryId());
		user.setLanguageId(edForm.getLanguageId());

		user.setCompanyId(edForm.getCompanyId());
		user.setCompanyName(edForm.getCompanyName());

		//user.setIsNextInvestOnUs(Integer.valueOf(edForm.getIsNextInvestOnUs()));
		user.setContactBySMS((edForm.isContactBySMS()));
		user.setContactByPhone((edForm.isContactByPhone()));
		user.setFalseAccount(edForm.isFalseAccount());
		user.setWriterId(wr.getWriter().getId());

		if ( edForm.getSendDepositDocs() == 1 ) {
			user.setIdDocVerify(true);
		}
		else {
			user.setIdDocVerify(false);
		}

        if ( edForm.getSpecialCareManualy() == 1 && edForm.getOldSpecialCareManualy() == 0) {
            user.getSpecialCare().setTimeScManual(new Date());
        } else if (edForm.getSpecialCareManualy() == 1 && edForm.getOldSpecialCareManualy() == 1){
            user.getSpecialCare().setTimeScManual(user.getSpecialCare().getTimeScManual());
        } else {
            user.getSpecialCare().setTimeScManual(null);
        }
		user.setVip(edForm.isVip());
		user.setAuthorizedMail(edForm.isAuthorizedMail());
        user.setRisky(edForm.isRisky());
        user.setStopReverseWithdrawOption(edForm.isStopReverseWithdrawOption());
        user.setImmediateWithdraw(edForm.isImmediateWithdraw());
        long stateId = 0;
        if (edForm.getCountryId() == Constants.COUNTRY_ID_US) {
        	stateId = edForm.getState();
        }
        user.setState(stateId);
        user.setShowCancelInvestment(edForm.isShowCancelInvestment());

		long oldBalance = user.getBalance();
		long oldUserId = user.getId();

		Connection con = getConnection();
		try {
			con.setAutoCommit(false);

			if ( CommonUtil.isHebrewSkin(user.getSkinId())) {
				// check if tax empemtion is enabled and user has closed investments
				if (edForm.isTaxExemption() == true && user.isTaxExemption() == false && InvestmentsDAO.hasInvestments(con, user.getId(), true)) {

					user.setTaxExemption(true);

					logger.debug("updating user with closed investments to tax exemption");

					// copy user details
					user.setReferenceId(new BigDecimal(oldUserId));
					user.setBalance(0);
					user.setTaxBalance(0);

					// From backend accept terms always false.
					user.setAcceptedTerms(false);

					UsersDAO.insert(con, user);
					logger.debug("create new User: " + user.toString());
					
//					//Add copyop frozen when user is created
//					try { 
//						Frozen fr = new Frozen(user.getId(), true, Frozen.FROZEN_REASON_FRESH, user.getWriterId());
//						FrozenCopyopDao.insert(con, fr);
//					} catch (Exception e) {
//						logger.error("Can't insert frozen user", e);
//					}

					TransactionsDAO.copyCreditCards(con, user.getId(), oldUserId);

					if (oldBalance > 0) {

						WithdrawForm wf = new WithdrawForm();
						wf.setAmount(String.valueOf((double)oldBalance/(double)100));
						int transTypeIdWithdraw = TransactionsManagerBase.TRANS_TYPE_ADMIN_WITHDRAW;

						DepositForm df = new DepositForm();
						df.setDeposit(String.valueOf((double)oldBalance/(double)100));
						df.setSendReceipt(false);
						int transTypeIdDeposit = TransactionsManagerBase.TRANS_TYPE_ADMIN_DEPOSIT;

						TransactionsManager.insertAdminWithdraw(con,wf, oldUserId, transTypeIdWithdraw);
						TransactionsManager.insertAdminDeposit(con,df, user.getId(), user.getUserName(), transTypeIdDeposit);
					}

					user.setTaxExemption(false);
					user.setReferenceId(new BigDecimal(user.getId()));
					user.setId(oldUserId);
					user.setUserName(user.getUserName() + "_OLD");
					user.setIsActive("0");

					logger.debug("copy credit cards, update old User: " + user.toString());
				} else {

					user.setTaxExemption(edForm.isTaxExemption());
				}
			} else {  // Ao users always with tax exemption
				user.setTaxExemption(true);
			}

			/*
			 * if (user.getIsActive().equals("1")) {
			 * user.setLastFailedTime(null); user.setFailedCount(0); }
			 */

//			//	update copyop nickname
//			if (!CommonUtil.isParameterEmptyOrNull(edForm.getCopyopNickname())) {	
//				try {
//					String newNickname = edForm.getCopyopNickname();
//					String oldNickname = ProfileManager.getProfileNickname(user.getId());
//					if (!newNickname.equalsIgnoreCase(oldNickname)) { // update nickname
//						boolean res = ProfileManager.insertNicknameIfNotExists(newNickname, user.getId());
//						if (!res) {							
//							FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR,CommonUtil.getMessage("copyop.nickname.exists.err",
//									null, Utils.getWriterLocale(context)),null);
//							context.addMessage("userForm:nickname", fm);
//							return false;
//						}
//						ProfileManager.changeCopyopNickname(user.getId(), newNickname, oldNickname);
//					}
//				} catch (Exception e) {
//					logger.error("Problem during executing changeCopyopNickname", e);
//					return false;
//				}
//			}
//			
//			// copyop update class id 
//			if (classIdChanged) {
//				try {
//					Profile profile = ProfileManager.getProfile(user.getId());
//					if (profile != null) {
//						ProfileManager.updateProfileToTest(user.getId(), user.getClassId() == ConstantsBase.USER_CLASS_TEST);
//					}
//				} catch (Exception e) {
//					logger.error("Problem during update classId on profile for userId:" + user.getId(), e);
//				}
//			}
				
			UsersDAO.update(con, user);
			if (user.getUserActiveData().getBubblesPricingLevel() != edForm.getBubblesPricingLevel()) {
				UsersDAOBase.updateUserBubblesPricingLevel(con, user.getId(), edForm.getBubblesPricingLevel());
			}
			UsersDAOBase.changeCancelInvestmentCheckboxState(con, user.getId(), edForm.isShowCancelInvestment());

			try {
				// send emailUserNameChanged notification and create issue EmailChanges
				
				if (emailChangedNotif){
					RiskAlertsManager.createIssueForEmailChanged(user, oldEmail, eWriterName);
				}
				
				if (isChangeSentDepositDocuments){
					UsersManager.updateSentDepDocumentsWriter(con, user.getId(), wr.getWriter().getId());
				}
				
				if (emailChangedNotif && user.getClassId() != ConstantsBase.USER_CLASS_TEST &&
						com.anyoption.common.managers.TransactionsManagerBase.isPastDeposit(user.getId()))
				/*&& ap.getIsLive())*/ {
					logger.debug("emailChangedNotif:" + emailChangedNotif + ", userNamChangedNotif: " + userNamChangedNotif);
					HashMap<String, String> params = null;
					params = new HashMap<String, String>();
					params.put(SendEmailChangedNoficationEmail.PARAM_USER_ID, String.valueOf(user.getId()));
					params.put(SendEmailChangedNoficationEmail.PARAM_WRITER_NAME, eWriterName);
					if(emailChangedNotif){
						params.put(SendEmailChangedNoficationEmail.PARAM_OLD_EMAIL, oldEmail);
						params.put(SendEmailChangedNoficationEmail.PARAM_NEW_EMAIL, newEmail);
					}
					if(userNamChangedNotif){
						params.put(SendEmailChangedNoficationEmail.PARAM_OLD_USER_NAME, oldUserName);
						params.put(SendEmailChangedNoficationEmail.PARAM_NEW_USER_NAME, newUserName);
					}
					params.put(SendEmailChangedNoficationEmail.MAIL_SUBJECT , "User id(" + user.getId() + ") who has a past deposit changed his username/email ");
					String emails = CommonUtil.getProperty("change.username.email.notification", null);
					String[] splitTo = emails.split(";");
					for (int i = 0; i < splitTo.length; i++) {
						params.put(SendDepositErrorEmail.PARAM_EMAIL, splitTo[i]);
						params.put(SendDepositErrorEmail.MAIL_TO, splitTo[i]);
						new SendEmailChangedNoficationEmail(params).start();
						Thread.sleep(1000);
					}
				}
			} catch (Exception e) {
				logger.warn("Error, problem sending email changed notification and issue EmailChanged " + e);
			}

			if (logger.isEnabledFor(Level.DEBUG)) {
				String ls = System.getProperty("line.separator");
				logger.log(Level.DEBUG, "Update user details finished successfully.  " + ls);
			}
			con.commit();

		} catch (SQLException e) {
			try {
				con.rollback();
			} catch (SQLException ie) {
				logger.log(Level.ERROR, "Can't rollback.", ie);
			}
			throw e;
		} finally {
			con.setAutoCommit(true);
			closeConnection(con);
		}

		FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_INFO,CommonUtil.getMessage("user.update.success", null, Utils.getWriterLocale(context)),null);
		context.addMessage(null, fm);

		return true;

	}

	public static boolean changePass(RegisterForm f, UserBase user) throws Exception {
		if (logger.isEnabledFor(Level.DEBUG)) {
			String ls = System.getProperty("line.separator");
			logger.log(Level.DEBUG, "Changing user password.  " + ls + "User:" + user.getUserName() + ls + "new password:" + "*****" + ls);
		}

		FacesContext context = FacesContext.getCurrentInstance();
		Connection con = getConnection();
		try {
			user.setPassword(f.getPassword());
			UsersDAO.update(con, user);
			if (f.isSendPassword()) {
				sendMailTemplateFile(TEMPLATE_CONFIRM_MAIL_ID, AdminManager.getWriterId(), user, null, new String(), new String(), null);
			}

			if (logger.isEnabledFor(Level.DEBUG)) {
				logger.log(Level.DEBUG, " password updated successfully! ");
			}
		} finally {
			con.setAutoCommit(true);
			closeConnection(con);
		}
		FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_INFO,CommonUtil.getMessage("password.changed", null, Utils.getWriterLocale(context)),null);
		context.addMessage(null, fm);

		return true;

	}

	public static String getWriterPassword(String writer) throws SQLException {

		Connection con = getConnection();
		try {
			return WritersDAO.getByUserName(con, writer).getPassword();

		} finally {
			closeConnection(con);
		}

	}


	public static boolean updateInsertFile(File f, UserBase user, int fileStatusId) throws SQLException, IOException {
		if (logger.isEnabledFor(Level.DEBUG)) {
			String ls = System.getProperty("line.separator");
			logger.log(Level.DEBUG, "Update/Insert File:  " + ls + "User:" + user.getUserName() + ls + f.toString());
		}

		FacesContext context = FacesContext.getCurrentInstance();
		boolean isNewFile = false;
		if (f.getId() == 0 && f.getFile() == null) {
			// insert
			FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR,CommonUtil.getMessage("error.mandatory", null, Utils.getWriterLocale(context)),null);
			context.addMessage("filesForm:file", fm);
			return false;
		}

		Connection con = getConnection();
		try {

			if (f.getFile() != null) {
				String name = f.getFile().getName();
				name = modifyFileName(name);
//				while(name.indexOf(".") != -1)
//					name = name.substring(name.indexOf(".") + 1);

				SimpleDateFormat sd = new SimpleDateFormat("ddMMyyyy_HHmmss");
				name = "user_" + user.getId() + "_doc_" + f.getFileTypeId() + "_" + sd.format(new Date()) + "_" + name;
				
				if (!com.anyoption.common.util.Utils.validateFileExtension(name)) {
					logger.log(Level.ERROR, "File extension not allowed: " + name);
					FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR,CommonUtil.getMessage("files.error", null, Utils.getWriterLocale(context)),null);
					context.addMessage(null, fm);
					return false;
				} else {
					f.setFileName(name);
	
					BufferedInputStream bis = new BufferedInputStream(f.getFile().getInputStream(),4096);
					java.io.File targetFile = new java.io.File(CommonUtil.getProperty(Constants.FILES_PATH) + name); // destination
					BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(targetFile),4096);
					int theChar;
					while((theChar = bis.read()) != -1) {
						bos.write(theChar);
					}
					FileInputStream  in = new FileInputStream(targetFile);
					in.read();
					bos.close();
					bis.close();
					in.close();
				}
			}
			//searching for requested file in order to update his status to done
			File requestedFile = FilesDAO.getRequestedFileByUserId(con, user.getId(), f.getCcId(), Long.valueOf(f.getFileTypeId()));
			if (requestedFile != null){ //update requested file
				boolean needApprove = true;
				requestedFile.setWriterId(Utils.getWriter().getWriter().getId());
				requestedFile.setTimeUpdated(new Date());
				requestedFile.setFileStatusId(File.STATUS_DONE);
				requestedFile.setFileName(f.getFileName());
				requestedFile.setReference(f.getReference());
				requestedFile.setRejectReason(f.getRejectReason());
				requestedFile.setComment(f.getComment());
				FilesManagerBase.updateExpDate(f);
				FilesDAO.updateBE(con, requestedFile);
				//change file risk status to 'need approve'
				if (requestedFile.getFileTypeId() == FileType.USER_ID_COPY){ // user files
					IssuesDAO.updateUserDocStatus(con, user.getId(), IssuesManagerBase.FILES_RISK_STATUS_NEED_APPROVE);
					if (requestedFile.getIssueActionsId() != 0){
						//change risk status to 'need approve'
						//IssuesDAO.needChangeFileStatus
					}
				} else { // CC files
					needApprove = IssuesDAO.needFilesApprove(con, user.getId(), requestedFile.getCcId());
					if (needApprove){
						IssuesDAO.updateCCDocStatus(con, requestedFile.getCcId(), IssuesManagerBase.FILES_RISK_STATUS_NEED_APPROVE);
						if (requestedFile.getIssueActionsId() != 0){
							//change risk status to 'need approve'

						}
					}
				}
				isNewFile = true;
			}

			if (f.getId() == 0) {
				// insert
				f.setWriterId(Utils.getWriter().getWriter().getId());
				f.setTimeCreated(new Date());
				f.setUtcOffsetCreated(Utils.getUser(f.getUserId()).getUtcOffset());
				f.setUserId(user.getId());
				f.setFileStatusId(fileStatusId);
				f.setTimeUpdated(new Date());
				FilesManagerBase.updateExpDate(f);
				FilesDAO.insert(con, f);
				isNewFile = true;
			} else {
				// update
				
				f.setWriterId(Utils.getWriter().getWriter().getId());
				f.setTimeUpdated(new Date());
				f.setFileStatusId(fileStatusId);
				FilesDAO.updateBE(con, f);
				if(f.isSupportReject() || f.isControlReject()) {
					sendRejectMail(f);
				}
			}

			if (isNewFile) {
				// insert
				FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_INFO,CommonUtil.getMessage("files.insert.success", null, Utils.getWriterLocale(context)),null);
				context.addMessage(null, fm);

			} else {
				// update
				FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_INFO,CommonUtil.getMessage("files.update.success", null, Utils.getWriterLocale(context)),null);
				context.addMessage(null, fm);

			}

			if (logger.isEnabledFor(Level.DEBUG)) {
				logger.log(Level.DEBUG, "File Updated succesfuly!");
			}
		} catch (Exception e) {
			logger.log(Level.ERROR, "Error on insert/update file!", e);
			FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR,CommonUtil.getMessage("files.error", null, Utils.getWriterLocale(context)),null);
			context.addMessage(null, fm);

			return false;

		} finally {
			closeConnection(con);
		}
		return true;
	}

	public static boolean updateInsertFraud(Fraud f, UserBase user) throws SQLException {
		if (logger.isEnabledFor(Level.DEBUG)) {
			String ls = System.getProperty("line.separator");
			logger.log(Level.DEBUG, "Update/Insert Fraud:  " + ls + "User:" + user.getUserName() + ls + f.toString());
		}

		FacesContext context = FacesContext.getCurrentInstance();

		Connection con = getConnection();
		try {

			if (f.getId() == 0) {
				// insert
				f.setWriterId(AdminManager.getWriterId());
				f.setTimeCreated(new Date());
				f.setUtcOffsetCreated(Utils.getUser(f.getUserId()).getUtcOffset());
				f.setUserId(user.getId());
				FraudsDAO.insert(con, f);
				FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_INFO,CommonUtil.getMessage("frauds.insert.success", null, Utils.getWriterLocale(context)),null);
				context.addMessage(null, fm);


			} else {
				// update
				f.setWriterId(AdminManager.getWriterId());
				FraudsDAO.update(con, f);
				FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_INFO,CommonUtil.getMessage("frauds.insert.success", null, Utils.getWriterLocale(context)),null);
				context.addMessage(null, fm);

			}

			if (logger.isEnabledFor(Level.DEBUG)) {
				logger.log(Level.DEBUG, "Fraud Updated succesfuly!");
			}
		} finally {
			closeConnection(con);
		}
		return true;
	}

	public static ArrayList searchUser(RegisterForm f) throws SQLException {
		Connection con = getConnection();
		try {
			return UsersDAO.getUsersList(con, f.getUserName(), f.getFirstName(), f.getLastName(), f.getIdNum(), f.getEmail(), f.getUserId(), f.getPhoneSearch());
		} finally {
			closeConnection(con);
		}

	}


	public static ArrayList<User> searchUserAndContact(RegisterForm f, Long writerId) throws SQLException {
		Connection con = getConnection();
		try {
			return UsersDAO.getUsersListWithContactID(con, writerId, f.getUserName(), f.getFirstName(), f.getLastName(), f.getIdNum(), f.getEmail(), f.getUserId(), f.getPhoneSearch(),f.getContactId(),f.getMobilePhone(), f.getCountries(), f.getSkinId(), f.getFirstRow(), f.getRowsPerPage());
		} finally {
			closeConnection(con);
		}

	}

	public static int searchUserAndContactCount(RegisterForm f, Long writerId) throws SQLException {
		Connection con = getConnection();
		try {
			return UsersDAO.getUsersListWithContactIDCount(con,writerId, f.getUserName(), f.getFirstName(), f.getLastName(), f.getIdNum(), f.getEmail(), f.getUserId(), f.getPhoneSearch(),f.getContactId(),f.getMobilePhone(), f.getCountries(), f.getSkinId());
		} finally {
			closeConnection(con);
		}

	}


	public static ArrayList<File> searchFiles(FilesDAOFilter filter) throws SQLException {
		Connection con = getConnection();
		try {
			return FilesDAO.getByUserId(con, filter);

		} finally {
			closeConnection(con);
		}
	}

	public static User getUserDetails(long id, String writerSkinsToString) throws SQLException {
		Connection con = getSecondConnection();
		try {
			return UsersDAO.getById(con, id, writerSkinsToString);
		} finally {
			closeConnection(con);
		}
	}

	public static User getUserDetails(long id) throws SQLException {
		Connection con = getSecondConnection();
		try {
			return UsersDAO.getById(con, id);
		} finally {
			closeConnection(con);
		}
	}

	public static ArrayList searchFrauds(FraudsForm f, long id) throws SQLException {
		Connection con = getConnection();
		try {
			return FraudsDAO.search(con, f.getTypeId(), id);

		} finally {
			closeConnection(con);
		}
	}

//	/**
//	 * @param user
//	 * @param refreshUserStripPage
//	 * @return
//	 * @throws SQLException
//	 */
//	public static boolean loadUserCalcFields(User user, boolean refreshUserStripPage) throws SQLException {
//		FacesContext context = FacesContext.getCurrentInstance();
//
//		Connection con = getSecondConnection();
//		try {
//			boolean found = false;
//
//			found = UsersDAO.getByUserName(con, user.getTmpUserName(), user);
//
//			if (found) {
//				long userId = user.getId();
//				UserRegulationBase userRegulation = new UserRegulationBase();
//				userRegulation.setUserId(userId);
//				UserRegulationManager.getUserRegulationBackend(userRegulation);
//				user.setUserRegulation(userRegulation);
//				user.setTmpUserId(userId);
//				user.setTmpUserName(user.getUserName());
//				user.setTmpEmail(user.getEmail());
//
//				MarketingCombination comb = MarketingCombinationsDAO.getById(con, user.getCombinationId());
//				if (null != comb) {
//					user.setCampaignName(comb.getCampaignName());
//					user.setCampaignPriority(comb.getCampaignPriority());
//					user.setCampaignDesc(comb.getCampaignDesc());
//					user.setContentsName(comb.getContentName());
//					String campaignForStrip = Constants.EMPTY_STRING;
//				    campaignForStrip = comb.getCampaignForStrip();
//					user.setCampaignForStrip(campaignForStrip);
//				} else {
//					user.setCampaignName("Campign not found");
//					user.setCampaignPriority(0);
//					user.setCampaignDesc(ConstantsBase.EMPTY_STRING);
//					logger.error("Error! comb for user "+ userId);
//				}
//
//				user.setCampaignRemarketing(MarketingManager.getRemarketingFlag(user.getId()));
//				user.setCurrency(CurrenciesDAOBase.getById(con, user.getCurrencyId().intValue()));
//
//				long withdrawals =
//						TransactionsManager.getTransactionsSummary(userId,
//								String.valueOf(TransactionsManagerBase.TRANS_TYPE_ADMIN_WITHDRAW) + "," +
//								String.valueOf(TransactionsManagerBase.TRANS_TYPE_CHEQUE_WITHDRAW) + "," +
//								String.valueOf(TransactionsManagerBase.TRANS_TYPE_CC_WITHDRAW) + "," +
//								String.valueOf(TransactionsManagerBase.TRANS_TYPE_PAYPAL_WITHDRAW) + "," +
//								String.valueOf(TransactionsManagerBase.TRANS_TYPE_BANK_WIRE_WITHDRAW) + "," +
//                                String.valueOf(TransactionsManagerBase.TRANS_TYPE_EFT_WITHDRAW) + "," +
//                                String.valueOf(TransactionsManagerBase.TRANS_TYPE_MONEYBOOKERS_WITHDRAW) + "," +
//                                String.valueOf(TransactionsManagerBase.TRANS_TYPE_WEBMONEY_WITHDRAW) + "," +
//								String.valueOf(TransactionsManagerBase.TRANS_TYPE_ENVOY_WITHDRAW) + "," +
//								String.valueOf(TransactionsManagerBase.TRANS_TYPE_BAROPAY_WITHDRAW) + "," +
//								String.valueOf(TransactionsManagerBase.TRANS_TYPE_CUP_CHINA_WITHDRAW),
//								String.valueOf(TransactionsManagerBase.TRANS_STATUS_SUCCEED));
//
//				long pendingWithdrawals =
//						TransactionsManager.getTransactionsSummary(userId,
//								String.valueOf(TransactionsManagerBase.TRANS_TYPE_ADMIN_WITHDRAW) + "," +
//								String.valueOf(TransactionsManagerBase.TRANS_TYPE_CHEQUE_WITHDRAW) + "," +
//								String.valueOf(TransactionsManagerBase.TRANS_TYPE_CC_WITHDRAW) + "," +
//								String.valueOf(TransactionsManagerBase.TRANS_TYPE_BANK_WIRE_WITHDRAW) + "," +
//                                String.valueOf(TransactionsManagerBase.TRANS_TYPE_EFT_WITHDRAW) + "," +
//								String.valueOf(TransactionsManagerBase.TRANS_TYPE_PAYPAL_WITHDRAW) + "," +
//								String.valueOf(TransactionsManagerBase.TRANS_TYPE_MONEYBOOKERS_WITHDRAW) + "," +
//								String.valueOf(TransactionsManagerBase.TRANS_TYPE_WEBMONEY_WITHDRAW) + "," +
//								String.valueOf(TransactionsManagerBase.TRANS_TYPE_CUP_CHINA_WITHDRAW) + "," +
//								String.valueOf(TransactionsManagerBase.TRANS_TYPE_ENVOY_WITHDRAW) + "," +
//								String.valueOf(TransactionsManagerBase.TRANS_TYPE_BAROPAY_WITHDRAW),
//								String.valueOf(TransactionsManagerBase.TRANS_STATUS_REQUESTED) + "," +
//								String.valueOf(TransactionsManagerBase.TRANS_STATUS_APPROVED) + "," +
//								String.valueOf(TransactionsManagerBase.TRANS_STATUS_SECOND_APPROVAL));
//
//				long deposits =
//						TransactionsManager.getTransactionsSummary(userId,
//                                TransactionsManagerBase.TRANS_TYPS_ALL_DEPOSITS_TXT,
//								String.valueOf(TransactionsManagerBase.TRANS_STATUS_SUCCEED) + "," +
//								String.valueOf(TransactionsManagerBase.TRANS_STATUS_PENDING));
//
//				long totalFees=
//					TransactionsManager.getTransactionsSummary(userId,
//							String.valueOf(TransactionsManagerBase.TRANS_TYPE_BANK_WIRE_FEE) + "," +
//							String.valueOf(TransactionsManagerBase.TRANS_TYPE_CC_WITHDRAW_FEE) + "," +
//							String.valueOf(TransactionsManagerBase.TRANS_TYPE_PAYPAL_WITHDRAW_FEE) + "," +
//							String.valueOf(TransactionsManagerBase.TRANS_TYPE_WITHDRAW_FEE) + "," +
//							String.valueOf(TransactionsManagerBase.TRANS_TYPE_ENVOY_WITHDRAW_FEE),
//							String.valueOf(TransactionsManagerBase.TRANS_STATUS_SUCCEED));
//
//				user.setWithdrawals(withdrawals);
//				user.setPendingWithdrawals(pendingWithdrawals);
//				user.setDeposits(deposits);
//
//				long userValue = deposits - withdrawals - user.getBalance() - pendingWithdrawals;
//
//				user.setUserValue(userValue);
//
//				//user.setIsOpenIssue(IssuesDAO.checkOpenIssues(con, userId));
//				user.setIsOpenIssue(IssuesDAO.checkOpenRiskIssues(con, userId));
//
//				user.setChargeBacks(ChargeBacksDAO.checkChargeBacks(con, userId));
//
//				user.setFrauds(FraudsDAO.checkFrauds(con, userId));
//				user.setSignificant(IssuesDAO.checkSignificantIssues(con, (int)userId));
//				user.setBonusUsed(UsersDAO.getNumOfBonus(con, (int)userId,true));
//				user.setBonusReceived(UsersDAO.getNumOfBonus(con, (int)userId,false));
//
//				String[] totals = InvestmentsManager.getInvestmentsTotals(userId);
////				String winLose = InvestmentsManager.getUsersWinLose(userId);
//				String winLoseYear = InvestmentsManager.getInvestmentsWinLose(userId, ConstantsBase.WIN_LOSE_YEAR_PERIOD);
//				String winLoseHalfYear = InvestmentsManager.getInvestmentsWinLose(userId, ConstantsBase.WIN_LOSE_HALF_YEAR_PERIOD);
//				String[] totals2 = TransactionsManager.getAverageDepositWithdraw(userId);
//
//				user.setNumOfDeposits(CommonUtil.displayLong(TransactionsManager.getNumberOfDeposits(userId)));
//				user.setNumOfWithdraws(CommonUtil.displayLong(TransactionsManager.getNumberOfWithdraws(userId)));
//				user.setTotalInvestments(CommonUtil.displayAmount(totals[2], user.getCurrencyId().intValue()));
//				user.setNumOfCancelInvestments(CommonUtil.displayLong(InvestmentsManager.getNumOfCancelInvestments(userId)));
//
//				user.setTotal(totals[0]);
//				user.setAvgAmount(CommonUtil.displayAmount(totals[1],user.getCurrencyId().intValue()));
//				user.setWinLose(CommonUtil.displayAmount(user.getWinLoseHouse(), user.getCurrencyId().intValue()));
//				user.setWinLoseHalfYear((CommonUtil.displayAmount(winLoseHalfYear,user.getCurrencyId().intValue())));
//				user.setWinLoseYear(CommonUtil.displayAmount(winLoseYear,user.getCurrencyId().intValue()));
//				user.setAvgDeposit(CommonUtil.displayAmount(totals2[0],user.getCurrencyId().intValue()));
//				user.setAvgWithdraw(CommonUtil.displayAmount(totals2[1],user.getCurrencyId().intValue()));
//				user.setTotalFees(CommonUtil.displayAmount(totalFees, user.getCurrencyId().intValue()));
//
//				ApplicationData ap = (ApplicationData)context.getApplication().createValueBinding(Constants.BIND_APPLICATION_DATA).getValue(context);
//				user.setMobilePhonePrefix(ap.getCountryPhoneCodes(String.valueOf(user.getCountryId())));
//				user.setLandLinePhonePrefix(ap.getCountryPhoneCodes(String.valueOf(user.getCountryId())));
//				user.setHaveBonus(UsersDAO.isHaveBonus(con, userId));
//				UsersDAO.getMaxDepositAndDate(con, user);
//				String lastSalesDeposit = TransactionsManager.getLastSalesDepositDetails(userId);
//				if (null != lastSalesDeposit) {
//					String[] lastSalesDepositDetails = lastSalesDeposit.split("_");
//					user.setLastSalesDepositDate(new Date(Long.valueOf(lastSalesDepositDetails[0])));
//					user.setLastSalesDepositAmount(CommonUtil.displayAmount(Long.valueOf(lastSalesDepositDetails[1]), user.getCurrency().getId()));
//				}
//				else {
//					user.setLastSalesDepositDate(null);
//					user.setLastSalesDepositAmount(null);
//				}
//
//				if (refreshUserStripPage) {
//					Utils.refreshUserStrip();
//				}
//
//				// Handle mobile device data
//				DeviceUniqueIdSkin device = UsersDAO.getUserAgentFromLogins(con, user.getLastLoginId());
//				if (null != device) {
//					user.setDeviceUniqueId(device.getDuid());
//					user.setLoginsUserAgent(device.getUserAgent());
//				} else {
//					user.setLoginsUserAgent(null);
//				}
//
//				Long dCombId = null;
//				if (!CommonUtil.isParameterEmptyOrNull(user.getDeviceUniqueId())) {  // user registered from the device
//					dCombId = UsersDAO.getCombIdByDuid(con, user.getDeviceUniqueId());
//				} else {  // take from logins
//					if (null != device && device.getDuid() != null) {
//						dCombId = UsersDAO.getCombIdByDuid(con, device.getDuid());
//					}
//				}
//
//				// set device campaign
//				if (null != dCombId && dCombId != 0) {
//					MarketingCombination combD = MarketingCombinationsDAO.getById(con, dCombId);
//					if (null != combD){
//						user.setDeviceCampaign(combD.getCampaignForStrip());
//					} else {
//						user.setDeviceCampaign("Campign not found");
//					}
//				} else {
//					user.setDeviceCampaign("Campign not found");
//				}
//
//				// handle other skin case
////				ArrayList<Integer> writerSkins = Utils.getWriter().getSkins();
////				boolean flag = false;
////				for ( Integer w : writerSkins ) {
////					if ( w.equals((Integer.parseInt(String.valueOf(user.getSkinId())))) ) {
////						flag = true;
////						break;
////					}
////				}
////
////
////				if (!flag) {   // other skin
////					FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR,CommonUtil.getMessage("error.user.otherskin", null, Utils.getWriterLocale(context)),null);
////					context.addMessage(null, fm);
////					Utils.popupMessage("error.user.otherskin");
////					return false;
////				}
//				user.setRetentionRepName(PopulationsManagerBase.getRetentionRepNameByUserId(user.getId()));
//				try {
//	                LimitationDeposits ld = LimitationDepositsManager.getLimitByUserId(userId);
//	                user.setLimitTxt(ld.getMinimumFirstDeposit()/100 + "/" + ld.getMinimumDeposit()/100);
//				} catch (Exception e) {
//					logger.debug("cant load user deposit limit for user id " + userId);
//				}
//                user.setLastRepChat(UsersDAO.getLastWriterIdChat(con, userId));
////                user.setRewardUser(RewardsDAO.getRewardsUser(con, userId));
//                user.setRegOrig(UsersDAO.getRegisterOriginal(con, userId));
//				return true;
//
//			} else {
//					FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR,CommonUtil.getMessage("error.user.notfound", null, Utils.getWriterLocale(context)),null);
//					context.addMessage(null, fm);
//
//					Utils.popupMessage("error.user.notfound");
//					return false;
//			}
//
//		} finally {
//			closeConnection(con);
//		}
//	}

	/**
	 * @param user
	 * @param refreshUserStripPage
	 * @return
	 * @throws SQLException
	 */
	public static boolean loadUserStripFields(User user, boolean refreshUserStripPage, Date arabicStartDate) throws SQLException {
		FacesContext context = FacesContext.getCurrentInstance();
		Connection con = getSecondConnection();
		try {
			boolean found = false;
			boolean goFound = true;
			Contact c = null;
			/*
			 * new logic for searching user by contactId getUserIdByContact() checks whether userId stays behind the given contactId if yes
			 * function continue with searching by userId else returns false
			 */
			if (user.getContactId() != 0 && user.getTmpUserName() == null && user.getTmpUserId() == null && user.getTmpEmail() == null) {
				c = getUserIdByContact(user.getContactId(), con);
				if (c != null && c.getUserId() != 0) {
					user.cleanUserStrip();
					user.setTmpUserId(c.getUserId());
				} else if (c != null && c.getUserId() == 0) {
					goFound = false;
				} else {
					// to do error msg not found
					return false;
				}
			}

			if (goFound) {
				String tmpUserName = user.getTmpUserName();
				Long tmpUserId = user.getTmpUserId();
				String tmpEmail = user.getTmpEmail();
				user.cleanUserStrip();
				found = UsersDAO.getByUserNameOrId(con, tmpUserName, tmpUserId, tmpEmail, user, arabicStartDate);
			} else {
				user.setLoadUserStrip(false);
				user.setConContactId(c.getId());
				user.setConTypeId(getContactType(con,c.getId(),c.getType()));
				if (c.getMobilePhone() != null) {
					user.setConMobilePhonePrefix(ApplicationData.getCountryPhoneCodes(String.valueOf(c.getCountryId())));
					user.setConMobilePhone(c.getMobilePhone());
				} else {
					user.setConMobilePhone("");
					user.setConMobilePhonePrefix("");
				}
				if (c.getPhone() != null) {
					user.setConLandLinePhone(c.getPhone());
					user.setConLandLinePhonePrefix(ApplicationData.getCountryPhoneCodes(String.valueOf(c.getCountryId())));
				} else {
					user.setConLandLinePhone("");
					user.setConLandLinePhonePrefix("");
				}
				if (c.getEmail() != null) {
					user.setConEmail(c.getEmail());
				} else {
					user.setConEmail("");
				}
				if (c.getFirstName() != null) {
					user.setConFirstName(c.getFirstName());
				} else {
					user.setConFirstName("");
				}
				if (c.getLastName() != null) {
					user.setConLastName(c.getLastName());
				} else {
					user.setConLastName("");
				}
				return true;
			}

			if (found) {
				long userId = user.getId();
				user.setBonusAbuser(UsersDAO.isBonusAbuser(con, userId));
				UserRegulationBase userRegulation = new UserRegulationBase();
				userRegulation.setUserId(userId);
				user.setUserRegulation(userRegulation);
				UserRegulationManager.getUserRegulationBackend(userRegulation);
				user.setTmpUserId(userId);
				user.setTmpUserName(user.getUserName());
				user.setTmpEmail(user.getEmail());
				//user.setAffiliateKey(user.getAffiliateKey());
				//user.setAffSiteLink(UsersManager.getAffiliateByKey(user.getAffiliateKey()).getSiteLink());
				//user.setDocStatus(UserDocumentsScreenManager.getDocumentStatusTxt(userId));

//				MarketingCombination comb = MarketingCombinationsDAO.getById(con, user.getCombinationId());
//				if (comb != null) {
//					user.setCampaignName(comb.getCampaignName());
//					user.setCampaignDesc(comb.getCampaignDesc());
//					user.setContentsName(comb.getContentName());
//					String campaignForStrip = Constants.EMPTY_STRING;
//				    campaignForStrip = comb.getCampaignForStrip();
//					user.setCampaignForStrip(campaignForStrip);
//				} else {
//					user.setCampaignName("Campign not found");
//					user.setCampaignPriority(0);
//					user.setCampaignDesc(ConstantsBase.EMPTY_STRING);
//					logger.error("Error! comb for user "+ userId);
//				}
//				user.setCampaignRemarketing(MarketingManager.getRemarketingFlag(user.getId()));
				user.setCurrency(CurrenciesDAOBase.getById(con, user.getCurrencyId().intValue()));

				long withdrawals =
						TransactionsManager.getTransactionsSummary(userId,
								String.valueOf(TransactionsManagerBase.TRANS_TYPE_ADMIN_WITHDRAW) + "," +
								String.valueOf(TransactionsManagerBase.TRANS_TYPE_CHEQUE_WITHDRAW) + "," +
								String.valueOf(TransactionsManagerBase.TRANS_TYPE_CC_WITHDRAW) + "," +
								String.valueOf(TransactionsManagerBase.TRANS_TYPE_PAYPAL_WITHDRAW) + "," +
								String.valueOf(TransactionsManagerBase.TRANS_TYPE_BANK_WIRE_WITHDRAW) + "," +
                                String.valueOf(TransactionsManagerBase.TRANS_TYPE_EFT_WITHDRAW) + "," +
                                String.valueOf(TransactionsManagerBase.TRANS_TYPE_MONEYBOOKERS_WITHDRAW) + "," +
                                String.valueOf(TransactionsManagerBase.TRANS_TYPE_WEBMONEY_WITHDRAW) + "," +
                                String.valueOf(TransactionsManagerBase.TRANS_TYPE_CUP_CHINA_WITHDRAW) + "," +
                                String.valueOf(TransactionsManagerBase.TRANS_TYPE_BAROPAY_WITHDRAW) + "," +
                                String.valueOf(TransactionsManagerBase.TRANS_TYPE_DIRECT24_WITHDRAW) + "," +
                                String.valueOf(TransactionsManagerBase.TRANS_TYPE_GIROPAY_WITHDRAW) + "," +
                                String.valueOf(TransactionsManagerBase.TRANS_TYPE_EPS_WITHDRAW) + "," +
								String.valueOf(TransactionsManagerBase.TRANS_TYPE_ENVOY_WITHDRAW)  + "," +
                                String.valueOf(TransactionsManagerBase.TRANS_TYPE_IDEAL_WITHDRAW),
								String.valueOf(TransactionsManagerBase.TRANS_STATUS_SUCCEED), false);
				
				long realWithdrawalsUSD =
						TransactionsManager.getTransactionsSummary(userId,
								String.valueOf(TransactionsManagerBase.TRANS_TYPE_CHEQUE_WITHDRAW) + "," +
								String.valueOf(TransactionsManagerBase.TRANS_TYPE_CC_WITHDRAW) + "," +	
								String.valueOf(TransactionsManagerBase.TRANS_TYPE_BANK_WIRE_WITHDRAW) + "," +
								String.valueOf(TransactionsManagerBase.TRANS_TYPE_ENVOY_WITHDRAW) + "," +
								String.valueOf(TransactionsManagerBase.TRANS_TYPE_PAYPAL_WITHDRAW) + "," +
								String.valueOf(TransactionsManagerBase.TRANS_TYPE_EFT_WITHDRAW) + "," +
								String.valueOf(TransactionsManagerBase.TRANS_TYPE_MONEYBOOKERS_WITHDRAW) + "," +
								String.valueOf(TransactionsManagerBase.TRANS_TYPE_WEBMONEY_WITHDRAW) + "," +
								String.valueOf(TransactionsManagerBase.TRANS_TYPE_CUP_CHINA_WITHDRAW) + "," +
								String.valueOf(TransactionsManagerBase.TRANS_TYPE_FOCAL_PAYMENTS_WITHDRAW) + "," +
                                String.valueOf(TransactionsManagerBase.TRANS_TYPE_DIRECT24_WITHDRAW) + "," +
                                String.valueOf(TransactionsManagerBase.TRANS_TYPE_GIROPAY_WITHDRAW) + "," +
                                String.valueOf(TransactionsManagerBase.TRANS_TYPE_EPS_WITHDRAW) + "," +
								String.valueOf(TransactionsManagerBase.TRANS_TYPE_BAROPAY_WITHDRAW),
								String.valueOf(TransactionsManagerBase.TRANS_STATUS_SUCCEED), true);

				long pendingWithdrawals =
						TransactionsManager.getTransactionsSummary(userId,
								String.valueOf(TransactionsManagerBase.TRANS_TYPE_ADMIN_WITHDRAW) + "," +
								String.valueOf(TransactionsManagerBase.TRANS_TYPE_CHEQUE_WITHDRAW) + "," +
								String.valueOf(TransactionsManagerBase.TRANS_TYPE_CC_WITHDRAW) + "," +
								String.valueOf(TransactionsManagerBase.TRANS_TYPE_PAYPAL_WITHDRAW) + "," +
								String.valueOf(TransactionsManagerBase.TRANS_TYPE_BANK_WIRE_WITHDRAW) + "," +
                                String.valueOf(TransactionsManagerBase.TRANS_TYPE_EFT_WITHDRAW) + "," +
                                String.valueOf(TransactionsManagerBase.TRANS_TYPE_MONEYBOOKERS_WITHDRAW) + "," +
                                String.valueOf(TransactionsManagerBase.TRANS_TYPE_WEBMONEY_WITHDRAW) + "," +
                                String.valueOf(TransactionsManagerBase.TRANS_TYPE_CUP_CHINA_WITHDRAW) + "," +
                                String.valueOf(TransactionsManagerBase.TRANS_TYPE_BAROPAY_WITHDRAW) + "," +
                                String.valueOf(TransactionsManagerBase.TRANS_TYPE_DIRECT24_WITHDRAW) + "," +
                                String.valueOf(TransactionsManagerBase.TRANS_TYPE_GIROPAY_WITHDRAW) + "," +
                                String.valueOf(TransactionsManagerBase.TRANS_TYPE_EPS_WITHDRAW) + "," +
								String.valueOf(TransactionsManagerBase.TRANS_TYPE_ENVOY_WITHDRAW)  + "," +
								String.valueOf(TransactionsManagerBase.TRANS_TYPE_IDEAL_WITHDRAW),
								String.valueOf(TransactionsManagerBase.TRANS_STATUS_REQUESTED) + "," +
								String.valueOf(TransactionsManagerBase.TRANS_STATUS_APPROVED) + "," +
								String.valueOf(TransactionsManagerBase.TRANS_STATUS_SECOND_APPROVAL), false);

				long deposits =
						TransactionsManager.getTransactionsSummary(userId,
								TransactionsManagerBase.TRANS_TYPS_ALL_DEPOSITS_TXT,
								String.valueOf(TransactionsManagerBase.TRANS_STATUS_SUCCEED) + "," +
								String.valueOf(TransactionsManagerBase.TRANS_STATUS_PENDING), false);
				
				double netDeposit =
						 (double)(TransactionsManager.getSumOfRealDepositsUSD(userId) - realWithdrawalsUSD)/100;
				
				long totalFees=
					TransactionsManager.getTransactionsSummary(userId,
							// kept for backwards compatibility
							" 15," +	// TRANS_TYPE_BANK_WIRE_FEE = 15;
							" 11," +	// TRANS_TYPE_CC_WITHDRAW_FEE = 11;
							" 27," +	// TRANS_TYPE_PAYPAL_WITHDRAW_FEE = 27;
							" 8," +		// TRANS_TYPE_WITHDRAW_FEE = 8;	
							" 31, " +		//TRANS_TYPE_ENVOY_WITHDRAW_FEE = 31;
							//this one is real
							Integer.toString(TransactionsManagerBase.TRANS_TYPE_HOMO_FEE)
							, String.valueOf(TransactionsManagerBase.TRANS_STATUS_SUCCEED), false);

//				long sumBonuses=TransactionsManager.getSumBonuses(userId);
//				String lastSalesDeposit = TransactionsManager.getLastSalesDepositDetails(userId);
//				if (null != lastSalesDeposit) {
//					String[] lastSalesDepositDetails = lastSalesDeposit.split("_");
//					user.setLastSalesDepositDate(new Date(Long.valueOf(lastSalesDepositDetails[0])));
//					user.setLastSalesDepositAmount(CommonUtil.displayAmount(Long.valueOf(lastSalesDepositDetails[1]), user.getCurrency().getId()));
//				}
//				else {
//					user.setLastSalesDepositDate(null);
//					user.setLastSalesDepositAmount(null);
//				}
				user.setWithdrawals(withdrawals);
				user.setPendingWithdrawals(pendingWithdrawals);
				user.setDeposits(deposits);
				long userValue = deposits - withdrawals - user.getBalance() - pendingWithdrawals;
				user.setUserValue(userValue);
				//user.setIsOpenIssue(IssuesDAO.checkOpenIssues(con, userId));
				user.setIsOpenIssue(IssuesDAO.checkOpenRiskIssues(con, userId));
//				user.setChargeBacks(ChargeBacksDAO.checkChargeBacks(con, userId));
//				user.setFrauds(FraudsDAO.checkFrauds(con, userId));
//				user.setSignificant(IssuesDAO.checkSignificantIssues(con, (int)userId));
//				user.setBonusUsed(UsersDAO.getNumOfBonus(con,(int)userId,true));
//				user.setBonusReceived(UsersDAO.getNumOfBonus(con,(int)userId,false));

//				String[] totals = InvestmentsManager.getInvestmentsTotals(userId);
//				String winLose = InvestmentsManager.getUsersWinLose(userId);
//				String winLoseYear = InvestmentsManager.getInvestmentsWinLose(userId, ConstantsBase.WIN_LOSE_YEAR_PERIOD);
//				String winLoseHalfYear = InvestmentsManager.getInvestmentsWinLose(userId, ConstantsBase.WIN_LOSE_HALF_YEAR_PERIOD);
//				String[] totals2 = TransactionsManager.getAverageDepositWithdraw(userId);

				user.setNumOfDeposits(CommonUtil.displayLong(TransactionsManager.getNumberOfDeposits(userId)));
				user.setNumOfAvgDeposits(CommonUtil.displayLong(TransactionsManager.countRealDeposit(userId)));
				user.setNumOfWithdraws(CommonUtil.displayLong(TransactionsManager.getNumberOfWithdraws(userId)));
//				user.setTotalInvestments(CommonUtil.displayAmount(totals[2], user.getCurrencyId().intValue()));
//				user.setNumOfCancelInvestments(CommonUtil.displayLong(InvestmentsManager.getNumOfCancelInvestments(userId)));

//				user.setTotal(totals[0]);
//				user.setAvgAmount(CommonUtil.displayAmount(totals[1],user.getCurrencyId().intValue()));
//				user.setWinLose(CommonUtil.displayAmount(user.getWinLoseHouse(), user.getCurrencyId().intValue()));
//				user.setWinLoseHalfYear((CommonUtil.displayAmount(winLoseHalfYear,user.getCurrencyId().intValue())));
//				user.setWinLoseYear(CommonUtil.displayAmount(winLoseYear,user.getCurrencyId().intValue()));
//				user.setAvgDeposit(CommonUtil.displayAmount(totals2[0],user.getCurrencyId().intValue()));
//				user.setAvgWithdraw(CommonUtil.displayAmount(totals2[1],user.getCurrencyId().intValue()));
//				user.setTotalFees(CommonUtil.displayAmount(totalFees, user.getCurrencyId().intValue()));
//				user.setSumBonuses (CommonUtil.displayAmount(sumBonuses, user.getCurrencyId().intValue()));
				user.setNetDeposit(netDeposit);
				if (TransactionsManager.countRealDeposit(userId) > 0) {
					user.setAvgNetDeposit(new DecimalFormat("##.##").format(user.getNetDeposit()/TransactionsManager.countRealDeposit(userId)));
				} else {
					user.setAvgNetDeposit("0");
				}

				user.setMobilePhonePrefix(ApplicationData.getCountryPhoneCodes(String.valueOf(user.getCountryId())));
				user.setLandLinePhonePrefix(ApplicationData.getCountryPhoneCodes(String.valueOf(user.getCountryId())));
//				user.setHaveBonus(UsersDAO.isHaveBonus(con, userId));
				UsersDAO.getMaxDepositAndDate(con, user);
				if (refreshUserStripPage) {
					Utils.refreshUserStrip();
				}

				// Handle mobile device data
//				DeviceUniqueIdSkin device = UsersDAO.getUserAgentFromLogins(con, user.getLastLoginId());
//				if (null != device) {
//					user.setDeviceUniqueId(device.getDuid());
//					user.setLoginsUserAgent(device.getUserAgent());
//				} else {
//					user.setLoginsUserAgent(null);
//				}

//				Long dCombId = null;
//				if (!CommonUtil.isParameterEmptyOrNull(user.getDeviceUniqueId())) {  // user registered from the device
//					dCombId = UsersDAO.getCombIdByDuid(con, user.getDeviceUniqueId());
//				} else {  // take from logins
//					if (null != device && device.getDuid() != null) {
//						dCombId = UsersDAO.getCombIdByDuid(con, device.getDuid());
//					}
//				}

				// set device campaign
//				if (null != dCombId && dCombId != 0) {
//					MarketingCombination combD = MarketingCombinationsDAO.getById(con, dCombId);
//					if (null != combD){
//						user.setDeviceCampaign(combD.getCampaignForStrip());
//					} else {
//						user.setDeviceCampaign("Campign not found");
//					}
//				} else {
//					user.setDeviceCampaign("Campign not found");
//				}

				// handle other skin case
//				ArrayList<Integer> writerSkins = Utils.getWriter().getSkins();
//				boolean flag = false;
//				for ( Integer w : writerSkins ) {
//					if ( w.equals((Integer.parseInt(String.valueOf(user.getSkinId())))) ) {
//						flag = true;
//						break;
//					}
//				}
//
//
//				if (!flag) {   // other skin
//					FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR,CommonUtil.getMessage("error.user.otherskin", null, Utils.getWriterLocale(context)),null);
//					context.addMessage(null, fm);
//					Utils.popupMessage("error.user.otherskin");
//					return false;
//				}
//				user.setRetentionRepName(PopulationsManagerBase.getRetentionRepNameByUserId(user.getId()));
				try {
	                LimitationDeposits ld = LimitationDepositsManager.getLimitByUserId(userId);
	                if (ld != null) {
	                	user.setLimitTxt(ld.getMinimumFirstDeposit()/100 + "/" + ld.getMinimumDeposit()/100);
	                }
				} catch (Exception e) {
					logger.debug("cant load user deposit limit for user id " + userId);
				}
 //               user.setLastRepChat(UsersDAO.getLastWriterIdChat(con, userId));
//                user.setRewardUser(RewardsDAO.getRewardsUser(con, userId));
                user.setRegOrig(UsersDAO.getRegisterOriginal(con, userId));                
				try {
					if (user.getTimeBirthDate() != null) {
						CommonUtil.setUserBirthDayMonthYear(user);
					}
				} catch (Exception e) {
					logger.debug("Can't load user Birth Date/Month/Year " + userId);
				}
				return true;
			} else {
					FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR,CommonUtil.getMessage("error.user.notfound", null, Utils.getWriterLocale(context)),null);
					context.addMessage(null, fm);
					Utils.popupMessage("error.user.notfound");
					return false;
			}
		} finally {
			closeConnection(con);
		}
	}

	public static void sendTemplate(long templateId, UserBase user, java.io.File[] attachement, String trxPan, String trxDate, long attachmentId, String mailToSupport) throws Exception {
		FacesContext context = FacesContext.getCurrentInstance();
		
	    sendMailTemplateFile(templateId, AdminManager.getWriterId(), user, false, trxPan, trxDate, null, null, null, null, attachement, attachmentId, mailToSupport, null, null, null, null,
	    						null, null, null);
		FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_INFO,CommonUtil.getMessage("templates.send.success", null, Utils.getWriterLocale(context)), null);
		context.addMessage(null, fm);

	}
	
	public static void sendTemplate(long templateId, UserBase user, java.io.File[] attachement, String trxPan, String trxDate, long attachmentId, String mailToSupport, String minWithdraw) throws Exception {
		FacesContext context = FacesContext.getCurrentInstance();
		
	    sendMailTemplateFile(templateId, AdminManager.getWriterId(), user, false, trxPan, trxDate, null, null, null, null, attachement, attachmentId, mailToSupport, null, null, null, null,
	    						null, null, minWithdraw);
		FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_INFO,CommonUtil.getMessage("templates.send.success", null, Utils.getWriterLocale(context)), null);
		context.addMessage(null, fm);

	}

	public static void sendTemplate(long templateId, UserBase user, String mailToSupport, long year) throws Exception {
		FacesContext context = FacesContext.getCurrentInstance();

		// for income tax form
		if ( templateId == Constants.TEMPLATE_INCOME_TAX_ID ) {
			TaxHistory t1 = TaxHistoryManagerBase.get(user.getId(), year, ConstantsBase.TAX_JOB_PERIOD1);
			TaxHistory t2 = TaxHistoryManagerBase.get(user.getId(), year, ConstantsBase.TAX_JOB_PERIOD2);
			TaxHistory t = new TaxHistory();
			t.setTax(t1.getTax() + t2.getTax());
			t.setWin(t1.getWin() + t2.getWin());
			t.setLose(t1.getLose() + t2.getLose());
			t.setYear(year);
			
			t.setSumInvest(t1.getSumInvest());			
			if (year < 2012) {
				t.setSumInvest(t2.getSumInvest());
			}
			
			t.setPercent(ConstantsBase.TAX_PERCENTAGE_AFTER_2012);
			if (year < 2012) {
				t.setPercent(ConstantsBase.TAX_PERCENTAGE_BEFORE_2012);
			}
			sendMailTemplateFile(templateId, AdminManager.getWriterId(), user, t, context);
		}
		else {
			sendMailTemplateFile(templateId, AdminManager.getWriterId(), user, mailToSupport, null, null, null);
		}

		FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_INFO,CommonUtil.getMessage("templates.send.success", null, Utils.getWriterLocale(context)),null);
		context.addMessage(null, fm);


	}

	/**
	 * This method will send the receipt email to user
	 * @throws SQLException
	 */
	public static void sendReceiptEmail(String amount, String ddate, String footer, String receiptNum) throws SQLException {
		FacesContext context = FacesContext.getCurrentInstance();
		UserBase u = (UserBase) context.getApplication().createValueBinding(Constants.BIND_USER).getValue(context);
		HttpServletRequest req = (HttpServletRequest) context.getExternalContext().getRequest();
    	String langCode = "";
    	String subject = "";

    	Connection con = getConnection();
		try {
            // get language code for getting the correct template
            logger.debug("Get language code, for getting the correct template.");
    		long langId = u.getSkin().getDefaultLanguageId();
    		langCode = LanguagesDAOBase.getCodeById(con, langId);
    		if( CommonUtil.isParameterEmptyOrNull(langCode)) {   // take israel to default
    			langCode = ConstantsBase.ETRADER_LOCALE;
    		}

    		// get subject email
   			Locale locale = new Locale(u.getSkinId() == Skin.SKIN_CHINESE_VIP ? ConstantsBase.CHINESE_LOCALE_BACLEND : langCode);
    		subject = CommonUtil.getMessage("receipt.email.subject", null, locale);

			// collect the parameters we neeed for the email
			HashMap params = new HashMap();
			params.put(SendReceiptEmail.PARAM_AMOUNT, amount);
			params.put(SendReceiptEmail.PARAM_DATE, ddate);
			params.put(SendReceiptEmail.PARAM_EMAIL, u.getEmail());
			params.put(SendReceiptEmail.PARAM_FOOTER, footer);
			params.put(SendReceiptEmail.PARAM_RECEIPT_NUM, receiptNum);
			params.put(SendReceiptEmail.PARAM_USER_ADDRESS, u.getStreet() + " " + u.getStreetNo() + " " + u.getCityName() + " " + u.getZipCode());
			params.put(SendReceiptEmail.PARAM_USER_NAME, u.getFirstName() + " " + u.getLastName());

			new SendReceiptEmail(params,req, langCode, subject, u.getSkinId()).start();

			// mailBox
//			if (!CommonUtil.isHebrewSkin(u.getSkinId())) {
				Template t = TemplatesDAO.get(con, Template.CC_RECEIPT_MAIL_ID);
				SendToMailBox(u, t, Utils.getWriter().getWriter().getId(), langId, Long.valueOf(receiptNum), 0);
//			}
		} catch (Exception e) {
			logger.error("Could not send email.", e);
		} finally {
            closeConnection(con);
        }
	}

	public static void initValues(User u) throws SQLException {
		FacesContext context = FacesContext.getCurrentInstance();
		Connection con = getConnection();
		String remoteUser = context.getExternalContext().getRemoteUser();
		try {
			u.setIsContactByEmail(1);
			u.setIsActive("1");
			u.setCurrencyId(CurrenciesDAOBase.getDefault(con).getId());
			u.setClassId(UsersDAO.getDefaultClass(con).getId());
			u.setIp(CommonUtil.getIPAddress());
			// TODO Why the writer roles are loaded in the user, why don't get them from the writer bean?
			u.setRoles(WritersDAO.getWriterRoles(con, remoteUser));
			u.setSupportMOLocking(false);
			if (Utils.getWriter().isEtrader()) {
				u.setCombinationId(ApplicationDataBase.getSkinById(Skin.SKIN_ETRADER).getDefaultCombinationId());
				u.setCountryId(ConstantsBase.COUNTRY_ID_IL); //also determines skin_id israel
				u.setCurrencyId(ConstantsBase.CURRENCY_ILS_ID);
				u.setLimitId(LimitsDAO.getBySkinAndCurrency(con, u.getSkinId(), u.getCurrencyId()));
				if ((u.getLimitId() == 0)) {
					u.setLimitId(ConstantsBase.ETRADER_DEFAULT_LIMIT);
				}
				u.setState(0l); //no state
			} else {
				u.setCombinationId(ApplicationDataBase.getSkinById(Skin.SKINS_DEFAULT).getDefaultCombinationId());
				u.setCountryId(ConstantsBase.COUNTRY_ID_US); //also determines skin_id israel
				u.setCurrencyId(ConstantsBase.CURRENCY_BASE_ID);
				u.setLimitId(LimitsDAO.getBySkinAndCurrency(con, u.getSkinId(), u.getCurrencyId()));
				if ((u.getLimitId() == 0)) {
					u.setLimitId(ConstantsBase.AO_DEFAULT_LIMIT);
				}
				u.setState(1l);
			}
		} finally {
			closeConnection(con);
		}

	}

	public static boolean isUserNameInUse(String userName) throws SQLException {
		Connection con = getConnection();
		try {
			return UsersDAO.isUserNameInUse(con, userName, true, null);
		} finally {
			closeConnection(con);
		}
	}

	public static User getByUserName(String userName) throws SQLException {
		Connection con = getSecondConnection();
		try {
			return UsersDAO.getByUserName(con, userName);
		} finally {
			closeConnection(con);
		}
	}

	public static IssueStatus getIdDocumentStatusByUserId(long userId) throws SQLException {
		Connection con = getConnection();
		try {
			return IssuesDAO.getIdDocumentStatusByUserId(con, userId);
		} finally {
			closeConnection(con);
		}
	}

	public static ArrayList<SkinCurrency> getCurrenciesBySkin(long skinId) throws SQLException {

		Connection con = getConnection();
		try {
			return SkinCurrenciesDAOBase.getAllBySkin(con, skinId);

		} finally {
			closeConnection(con);
		}

	}

	public static ArrayList<SkinLanguage> getLangueagesBySkin(long skinId) throws SQLException {

		Connection con = getConnection();
		try {
			return SkinLanguagesDAOBase.getAllBySkin(con, (int)skinId);

		} finally {
			closeConnection(con);
		}

	}

    /**
     * update document_required field in a case the
	 * user sum deposits over the max, he need to send us documents.
	 *
     * @param userId
     * 		user id
     * @throws SQLException
     */
    public static void setFlagsAfterMaxDeposit(long userId) throws SQLException {
        Connection conn = getConnection();
        try {
        	UsersDAO.setFlagsAfterMaxDeposit(conn, userId);
        } finally {
            closeConnection(conn);
        }
    }


    /**
     * heck and return if user send documents for a bigger deposits
     * @param userId
     * 	user id
     * @return
     * 		true if ducuments send
     * @throws SQLException
     */
    public static boolean getIsSendIdDocuments(long userId) throws SQLException {
        Connection con = getConnection();
        try {
        	return UsersDAOBase.getIsSendIdDocuments(con, userId);
        } finally {
            closeConnection(con);
        }
    }


    /**
     * Get docs required field of the user
     * if flag true - user must to send us documents for deposit
     * @param userName
     * 		user name
     * @return
     * 		if true then user need to send documents
     * @throws SQLException
     */
    public static boolean getIsDocsRequired(String userName) throws SQLException {
        Connection con = getConnection();
        try {
        	int flag = UsersDAO.getIsDocsRequired(con, userName);
        	if ( flag == 1 ) {
        		return true;
        	}
        } finally {
            closeConnection(con);
        }
        return false;
    }


    /**
     * Get locked accounts
     * @return
     * 		users list
     * @throws SQLException
     */
    public static ArrayList getLockedAccounts()  throws SQLException {
        Connection con = getConnection();
        try {
        		return UsersDAO.getLockedAccounts(con);
        } finally {
            closeConnection(con);
        }
    }

   /**
    * Unlock user.
    * @param userId
    * 		user id for unlock
    * @throws SQLException
    */
    public static void unlockUser(long userId, boolean activate, long writerId) throws SQLException {
        Connection con = getConnection();
        try {
        		boolean isSendIdDocuments = UsersDAO.getIsSendIdDocuments(con, userId);
        		UsersDAO.unlockUser(con, userId, activate);        		
        		if(!isSendIdDocuments){
        			updateSentDepDocumentsWriter(con, userId, writerId);
        		}
        		
        } finally {
            closeConnection(con);
        }
    }


    /**
     * Get risky users
     * @return
     * @throws SQLException
     */
    public static ArrayList<SelectItem> getRiskyUsers() throws SQLException {
        Connection con = getConnection();
        try {
        		return UsersDAO.getRiskyUsers(con);
        } finally {
            closeConnection(con);
        }
    }


    /**
     * Get users market disable list
     * @param userId
     * 		userd id filter
     * @param from
     * 		from date filter
     * @param to
     * 		to date filter
     * @param apiExternalUserId
     * @return
     * 		users market disable list
     * @throws SQLException
     */
    public static ArrayList<UserMarketDisable> getUsersMarketDisableList(long userId, Date from, Date to,
    		long active, long marketId, long isDev3, long skinId, long apiExternalUserId) throws SQLException {

        Connection con = getConnection();
        try {
        	return UsersMarketDisableDAO.getUsersList(con, userId, from, to, active, marketId, isDev3, skinId, apiExternalUserId);
        } finally {
            closeConnection(con);
        }
    }


    /**
     * Update / Insert user market disable
     * @param um  userMarketDisable instance for update / insert
     * @return
     * @throws SQLException
     */
    public static boolean insertUpdateUsersMarketDisable(UserMarketDisable um) throws SQLException {

		if (logger.isEnabledFor(Level.DEBUG)) {
  			String ls = System.getProperty("line.separator");
  			logger.log(Level.DEBUG,"Insert/update userMarketDisable: " + ls + um.toString());
  		 }

    	Connection con = getConnection();

    	try {
    			if ( um.getId() == 0 ) {  // insert
    				UsersMarketDisableDAO.insert(con, um);
    			} else {
    				UsersMarketDisableDAO.update(con, um);   // update
    			}

    	} finally {
    		closeConnection(con);
    	}

    	return true;
    }

    /**
     * Get limitId bu currencyId and skinId
     * @return
     * @throws SQLException
     */
    public static long getLimitId(long skinId, long currencyId) throws SQLException {
        Connection con = getConnection();
        try {
        		return LimitsDAO.getBySkinAndCurrency(con, skinId, currencyId);
        } finally {
            closeConnection(con);
        }
    }


    /**
     * is have deposits
     * @param id  user id
     * @return
     * @throws SQLException
     */
    public static boolean isHaveDeposits(long id) throws SQLException {
    	Connection con = getConnection();
    	try {
    		return UsersDAO.isHaveDeposits(con, id);
    	} finally {
    		closeConnection(con);
    	}
    }

    /**
     * is have approved deposits
     * @param id  user id
     * @return
     * @throws SQLException
     */
    public static boolean isHaveApprovedDeposits(long id) throws SQLException {
    	Connection con = getConnection();
    	try {
    		return UsersDAO.isHaveApprovedDeposits(con, id);
    	} finally {
    		closeConnection(con);
    	}
    }
    
    public static boolean isHaveApprovedDeposits(long id, boolean allDepositClassTypes) throws SQLException {
        Connection con = getConnection();
        try {
            return UsersDAO.isHaveApprovedDeposits(con, id, allDepositClassTypes);
        } finally {
            closeConnection(con);
        }
    }

    /**
     * Get credit card instance by id
     * @param ccId
     * @return
     * @throws SQLException
     */
    public static CreditCard getById(long ccId) throws SQLException {
    	Connection con = getConnection();
    	try {
    		return CreditCardsDAO.getById(con, ccId);
    	} finally {
    		closeConnection(con);
    	}
    }

    /**
     * set false account
     * @param userId  user id
     * @return
     * @throws SQLException
     */
    public static void setFalseAccount(long userId) throws SQLException {
    	Connection con = getConnection();
    	try {
    		UsersDAO.setFalseAccount(con, userId, true);
    	} finally {
    		closeConnection(con);
    	}
    }

    /**
     * set Is Active
     * @param userId  user id
     * @return
     * @throws SQLException
     */
    public static void setIsActive(long userId, boolean isActive) throws SQLException {
    	Connection con = getConnection();
    	try {
    		UsersDAO.setIsActive(con, userId, isActive);
    	} finally {
    		closeConnection(con);
    	}
    }

    /**
     * set Disable Phone Contact
     * @param userId  user id
     * @return
     * @throws SQLException
     */
    public static void setDisablePhoneContact(long userId) throws SQLException {
    	Connection con = getConnection();
    	try {
    		UsersDAO.setDisablePhoneContact(con, userId);
    	} finally {
    		closeConnection(con);
    	}
    }

    /**
     * Get Calcalist Users
     * @param
     * @return
     *      User list
     * @throws SQLException
     */
    public static ArrayList<User> getCalcalistUsers(String userId,String userIdNum,Date from,Date to) throws SQLException {
    	Connection con = getConnection();
    	try {
    	 return	UsersDAO.getCalcalistUsers(con,userId,userIdNum,from,to);
    	} finally {
    		closeConnection(con);
    	}
    }

    /**
     * set Disable Phone Contact DAA
     * @param userId  user id
     * @return
     * @throws SQLException
     */
    public static void setDisablePhoneContactDAA(long userId) throws SQLException {
    	Connection con = getConnection();
    	try {
    		UsersDAO.setDisablePhoneContactDAA(con, userId);
    	} finally {
    		closeConnection(con);
    	}
    }

    public static ArrayList<User> getSMSUsers(Integer lastloginStart, Integer lastloginEnd, String includeCountries,
    		String skinId, String arrayToString, List<String> customerValue, String populationType, int hasSalesDeposit,
    		List<String> depositors, long userClassId, int sendType, int activeUsersFilter) throws Exception {
        Connection con = getConnection();
        ArrayList<User> list = new ArrayList<User>();
        try {
            list = UsersDAO.getSMSUsers(con, lastloginStart, lastloginEnd, includeCountries, skinId, arrayToString, customerValue, populationType, hasSalesDeposit, depositors, userClassId, sendType, activeUsersFilter);
        } finally {
            closeConnection(con);
        }
        return list;
    }

	public static ArrayList<User> getSMSUsersFromFile(String usersFromFile, int sendType) throws SQLException {
		Connection con = getConnection();
        ArrayList<User> list = new ArrayList<User>();
        try {
            list = UsersDAO.getSMSUsersFromFile(con, usersFromFile, sendType);
        } finally {
            closeConnection(con);
        }
        return list;
	}

	/**
	 * Get a specific file from files.
	 * @throws SQLException
	 */
	public static File getSpecificFile(long userId, long fileTypeId, long ccId, long actionId) throws SQLException {
		Connection con = getSecondConnection();
        try {
            return FilesDAO.getSpecificFile(con, userId, fileTypeId, ccId, actionId);
        } finally {
            closeConnection(con);
        }
	}

	/**
	 * Get user files from files.
	 * @throws SQLException
	 */
	public static UserFiles getUserFiles(long issueId, long reasonId) throws SQLException {
		Connection con = getSecondConnection();
		try {
			return FilesDAO.getUserFiles(con, issueId, reasonId);
		} finally {
			closeConnection(con);
		}
	}

	public static void updateUserBackgroundAndTip(long userId, String background, String tip, long tag) throws SQLException {
		Connection con = getConnection();
		try {
			UsersDAO.updateUserBackgroundAndTip(con, userId, background, tip, tag);
		} finally {
			closeConnection(con);
		}
	}

	public static void setUserToPromotionsTable(ArrayList<User> usersList, Long templateId, Connection conn, HttpSession session, int sendType) throws SQLException{
			UsersDAO.setUserToPromotionsTable( conn, usersList, session ,sendType);
	}

	public static void insertDataToPromotionTable(ArrayList<User> usersList, Long templateId, int sendType, String message, Writer writer, String promotionName, HttpSession session) throws SQLException{

		Connection conn = getConnection();
        try{
        	session.setAttribute(Constants.PROMOTION_TOTAL_AND_INSERTED_USERS, "0_0");
        	insertPromotion(conn, templateId, sendType, message, writer.getId(), promotionName);
        	setUserToPromotionsTable(usersList, templateId, conn, session, sendType);
        } finally {
			closeConnection(conn);
		}
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		 session.setAttribute(Constants.PROMOTION_TOTAL_AND_INSERTED_USERS, "0_0");


//	    try {
//	        conn = getConnection();
//	        conn.setAutoCommit(false);
//	        session.setAttribute(Constants.PROMOTION_TOTAL_AND_INSERTED_USERS, "0_0");
//	        insertPromotion(conn, templateId, sendType, message, writer.getId(), promotionName);
//	        double i=0;
//	        int countInserted = 0;
//	        String percentage = "";
//	        logger.debug(usersList.size());
//	        for(User user : usersList){
//	        	if(sendType == Constants.SEND_EMAIL || (sendType == Constants.SEND_SMS && user.getContactBySMS())){
//	        		setUserToPromotionsTable(user, templateId, conn);
//	        		countInserted++;
//	        	}
//	        	i++;
//	        	percentage = Double.toString(((i/sumOfUsersList)*100));
//	        	session.setAttribute(Constants.PROMOTION_TOTAL_AND_INSERTED_USERS,sumOfUsersList+ "_" + percentage.substring(0, percentage.indexOf(".")) );
//	        }
//	        conn.commit();
//	    } catch (SQLException sqle) {
//	        try {
//	            conn.rollback();
//	        } catch (SQLException sqlie) {
//	            logger.error("Failed to rollback.", sqlie);
//	        }
//	        throw sqle;
//	    } finally {
//	        try {
//	            conn.setAutoCommit(true);
//	        } catch (SQLException sqle) {
//	            logger.error("Can't return connection back to autocommit.", sqle);
//	        }
//	        closeConnection(conn);
//	        try {
//				Thread.currentThread().sleep(3000);
//			} catch (InterruptedException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
//	        session.setAttribute(Constants.PROMOTION_TOTAL_AND_INSERTED_USERS, "0_0");
//	    }
	}

	public static void insertPromotion(Connection conn, Long templateId, int sendType, String message, long writerId, String promotionName){
		try {
			UsersDAO.insertPromotion(conn, templateId, sendType, message, writerId, promotionName);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static ArrayList<UserBase> getUsersListByPromotionId(long promotionId, boolean smsAvailable) throws SQLException{
		Connection conn = getConnection();
		ArrayList<UserBase> list = null;

		try {
			list = UsersDAO.getUsersListByPromotionId(conn, promotionId, smsAvailable);
		} catch (Exception e) {
			// TODO: handle exception
		}finally{
			closeConnection(conn);
		}
		return list;
	}

	public static int getUsersListCountByPromotionId(long promotionId, boolean smsAvailable) throws SQLException{
		Connection conn = getConnection();

		try {
			return  UsersDAO.getUsersListCountByPromotionId(conn, promotionId, smsAvailable);
		} catch (Exception e) {
			return 0;
		}finally{
			closeConnection(conn);
		}
	}

	public static Contact getUserIdByContact(Long contactId,Connection con) throws SQLException{

		Contact c = null;
		try {
			con = getSecondConnection();
			c = ContactsDAO.getById(con, contactId);
			return c;
		} finally {
			c= null;
		}
	}

	public static String getContactType(Connection con, Long contactId, Long typeId) throws SQLException{

		String type = null;

		try{

			con= getConnection();
			type = ContactsDAO.getContactTypeById(con,contactId,typeId);

		    return type;

		}finally{}

	}

	public static void updateUserContactByEmail(User u) throws SQLException {
		Connection con = null;
		try {
		con = getConnection();
		UsersDAO.updateUserContactByEmail(con, u);
		}finally{

			closeConnection(con);
		}
	}

    public static boolean isWaiveDepositsVelocityCheck(long id) throws SQLException {
    	Connection con = getConnection();
    	try {
    		return UsersDAO.isWaiveDepositsVelocityCheck(con, id);
    	} finally {
    		closeConnection(con);
    	}
    }

    public static void sendRejectMail(File file)
	{
		try {
			HashMap<String, String> params = null;
			params = new HashMap<String, String>();
					String fromEmail = CommonUtil.getProperty("email.to.anyoption");
					String mailTo = CommonUtil.getProperty("email.to.anyoption");;

					params.put(SendDepositErrorEmail.PARAM_EMAIL, fromEmail);
					params.put(SendDepositErrorEmail.PARAM_USER_ID,"" + file.getUserId() + "");
					params.put(SendDepositErrorEmail.PARAM_FILE_ID,"" + file.getUserId() + "");
					params.put(SendDepositErrorEmail.MAIL_TO, mailTo);
					if(file.isSupportReject()) {
						params.put(SendDepositErrorEmail.MAIL_SUBJECT ,"File rejected from support. ");
					}
					if(file.isControlReject()) {
						params.put(SendDepositErrorEmail.MAIL_SUBJECT ,"File rejected from control. ");
					}
					new SendDepositErrorEmail(params).start();

	    } catch (Exception e) {
		    logger.error("Could not send control/support reject email.", e);
   }
}

	public static Hashtable<String, Integer> getFirstIssueFilesStatusByUserId(long userId) throws SQLException {
		Connection con = null;
		try {
			con = getConnection();
			return FilesDAOBase.getFirstIssueFilesStatusByUserId(con, userId);
		} finally {
			closeConnection(con);
		}
	}

	private static String modifyFileName(String name) {
		int lastChar = name.lastIndexOf('\\');
		if (lastChar != -1) {
			name = name.substring(lastChar + 1);
		}
		lastChar = name.lastIndexOf('/');
		if (lastChar != -1) {
			name = name.substring(lastChar + 1);
		}
		name = name.replaceAll("\\?", "");
		name = name.replaceAll(":", "");
		name = name.replaceAll("\\*", "");
		name = name.replaceAll("<", "");
		name = name.replaceAll(">", "");
		name = name.replaceAll("\"", "");
		name = name.replaceAll("|", "");
		name = name.replaceAll("\\+", "");
		name = name.replaceAll("\\s", ""); 
		name = name.replaceAll("[^a-zA-Z0-9.-]", "");

		return name;
	}

	public static void removeUsersMarketDisableByUserId(long userId) throws SQLException {
		removeUsersMarketDisableByUserId(userId, 0);
	}

	public static void removeUsersMarketDisableByUserId(long userId, long apiExternalUserId) throws SQLException {
		Connection con = getConnection();
		try {
			UsersMarketDisableDAO.removeUsersMarketDisableByUserId(con, userId, apiExternalUserId);
		} finally {
			closeConnection(con);
		}
	}

	/**
	 * get users details history.
	 * @param con
	 * @param from
	 * @param to
	 * @param userName
	 * @param email
	 * @param contactId
	 * @param writersId
	 * @param typesId
	 * @param isExcludeTesters
	 * @return ArrayList<UsersDetailsHistory>
	 * @throws SQLException
	 */
	public static ArrayList<UsersDetailsHistory> getUsersDetailsHistory(Date from, Date to, String userName, String email, long contactId, String writersId, String typesId, boolean isExcludeTesters, long userId) throws SQLException {
	    Connection con = null;
        try {
            con = getConnection();
            return UsersDAO.getUsersDetailsHistory(con, from, to, userName, email, contactId, writersId, typesId, isExcludeTesters, userId);
        } finally {
            closeConnection(con);
        }
	}
	
	/**
	 * Check if issue is a risk issue.
	 * @throws SQLException
	 */
	public static boolean verifyIsRiskIssue(long userId) throws SQLException {
		Connection con = getSecondConnection();
        try {
            return FilesDAO.checkIsRiskIssue(con,userId);
        } finally {
            closeConnection(con);
        }
	}
	
	public static ArrayList<UserRegulationHistory> getUserRegulationHistory(long userId, Date from, Date to) throws SQLException{
		 Connection con = null;
		 try{
			 con = getConnection();
			 return UsersDAO.getUserRegulationHistory(con, userId, from, to);
		 }finally{
			 closeConnection(con);
		 }
	}

	/**
	 * check if user came from media channel
	 * @param userId the user id
	 * @return true if he his from media channel false if not
	 * @throws SQLException 
	 */
	public static boolean isUserFromMediaChannel(long userId) throws SQLException {
		Connection con = null;
		try {
			con = getConnection();
			return UsersDAO.isUserFromMediaChannel(con, userId);
		} finally {
			 closeConnection(con);
		}
	}
	
	public static PendingUserWithdrawalsDetailsBase pendingUserWithdrawalsDetails(long userId) throws SQLException {
		Connection con = null;
		try {
			con = getConnection();
			return UsersDAO.pendingUserWithdrawalsDetails(con, userId);
		} finally {
			 closeConnection(con);
		}
	}
	
	public static ArrayList<UserDetailsHistoryChanges> getUserDetailsHistoryChangesList(long userId, Date from, Date to, long fieldId, long writerId) throws SQLException {
		Connection con = null;
		try {
			con = getConnection();
			return UsersDAO.getUserDetailsHistoryChangesList(con, userId, from, to, fieldId, writerId);
		} finally {
			 closeConnection(con);
		}
	}
	
	public static void updateSentDepDocumentsWriter(Connection con, long userId, long writerId) throws SQLException {
		UsersDAO.updateSentDepDocumentsWriter(con, userId, writerId);
	}
	
	public static Address getAddressByUserId(long userId) throws SQLException {
		Connection con = null;
		try {
			con = getConnection();
			return UsersDAO.getAddressByUserId(con, userId);
		} finally {
			 closeConnection(con);
		}
	}
	
	public static User getUserByOnlyUserName(String userName) throws SQLException {
		Connection con = getSecondConnection();
		try {
			return UsersDAO.getUserByOnlyUserName(con, userName);
		} finally {
			closeConnection(con);
		}
	}

	public static List<SelectItem> getTCSitesSI() {
		if (tcAccessSites == null) {
			Connection con = null;
			try {
				con = getConnection();
				tcAccessSites = UsersDAO.getTCSites(con);
			} catch (SQLException e) {
				logger.debug("Unable to load sites", e);
				return new ArrayList<>();
			} finally {
				closeConnection(con);
			}
		}
		List<SelectItem> sis = new ArrayList<>();
		sis.add(new SelectItem(0, "All"));
		for (TCAccessTokenSite site : tcAccessSites) {
			sis.add(new SelectItem(site.getId(), site.getName()));
		}
		return sis;
	}

	public static List<TCAccessToken> getTCAccess(long userId, Date from, Date to, String utcOffset, List<Integer> sites) {
		Connection con = null;
		try {
			con = getConnection();
			return UsersDAO.getTCAccess(con, userId, from, to, utcOffset, sites);
		} catch (SQLException e) {
			logger.debug("Unable to load TC access", e);
			return new ArrayList<>();
		} finally {
			 closeConnection(con);
		}
	}

	public static void cancelTCAccessToken(long tokenId) {
		Connection con = null;
		try {
			con = getConnection();
			UsersDAO.cancelTCAccessToken(con, tokenId);
		} catch (SQLException e) {
			logger.debug("Unable to cancel TC token with id " + tokenId, e);
		} finally {
			 closeConnection(con);
		}
	}

	public static boolean addTCAccessToken(int site, Date from, Date to, long userId, long writerId, String utcOffset) throws SQLException {
		Connection con = null;
		try {
			con = getConnection();
			con.setAutoCommit(false);
			boolean result = UsersDAO.addTCAccessToken(con, site, from, to, userId, writerId, utcOffset);
			con.commit();
			return result;
		} catch (SQLException e) {
			logger.debug("Unable to add TC access", e);
			try {
				con.rollback();
			} catch (SQLException e1) {
				logger.debug("Unable to rollback connection", e1);
				throw e1;
			}
			return false;
		} finally {
			con.setAutoCommit(true);
			closeConnection(con);
		}
	}
	
	public static void updateUserData(long userId, Long dob, String firstName, String lastName, long writerId) throws SQLException {
		Connection con = null;
		try {
			con = getConnection();
			UsersDAO.updateUserData(con, userId, dob, firstName, lastName, writerId);
		} finally {
			 closeConnection(con);
		}
	}
	
}