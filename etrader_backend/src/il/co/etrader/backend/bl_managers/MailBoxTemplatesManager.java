package il.co.etrader.backend.bl_managers;

import il.co.etrader.backend.dao_managers.MailBoxTemplatesDAO;
import il.co.etrader.bl_managers.MailBoxTemplateManagerBase;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.faces.model.SelectItem;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import com.anyoption.common.beans.MailBoxTemplate;

public class MailBoxTemplatesManager extends MailBoxTemplateManagerBase {

    private static final Logger logger = Logger.getLogger(MailBoxTemplatesManager.class);

	public static ArrayList<MailBoxTemplate> getAllTempaltes(int skinId, long typeId, long senderId) throws SQLException {
		Connection con = getConnection();
		try {
			return MailBoxTemplatesDAO.getAll(con, skinId, typeId, senderId);
		} finally {
			closeConnection(con);
		}
	}

    public static void insertUpdateTemplate(MailBoxTemplate template) throws SQLException {
        if (logger.isEnabledFor(Level.DEBUG)) {
            String ls = System.getProperty("line.separator");
            logger.log(Level.DEBUG,"Insert/update mailBoxTemplate: " + ls + template.toString());
         }

        Connection con = getConnection();

        try {
            if (template.getId() == 0) {  // insert
               MailBoxTemplatesDAO.insert(con, template);
            } else { // update
            	MailBoxTemplatesDAO.update(con, template);
            }
        } finally {
            closeConnection(con);
        }
    }

    /**
     * Get all template types
     * @return
     * @throws SQLException
     */
    public static ArrayList<SelectItem> getTemplateTypes() throws SQLException {
    	Connection con = null;
    	try {
    		con = getConnection();
    		return MailBoxTemplatesDAO.getTypes(con);
    	} finally {
    		closeConnection(con);
    	}
    }

    /**
     * Get senders
     * @return
     * @throws SQLException
     */
    public static ArrayList<SelectItem> getSenders() throws SQLException {
    	Connection con = null;
    	try {
    		con = getConnection();
    		return MailBoxTemplatesDAO.getSenders(con);
    	} finally {
    		closeConnection(con);
    	}
    }

    /**
     * Get all mailbox templates by writer skins and user
     * @return
     * @throws SQLException
     */
    public static ArrayList<SelectItem> getAllTemplatesByWriterAndUser() throws SQLException {
    	Connection con = null;
    	try {
    		con = getConnection();
    		return MailBoxTemplatesDAO.getTemplatesListByWriterAndUser(con);
    	} finally {
    		closeConnection(con);
    	}
    }

    /**
     * Get all mailbox templates by writer skins
     * @return
     * @throws SQLException
     */
    public static ArrayList<SelectItem> getAllTemplatesByWriter() throws SQLException {
    	Connection con = null;
    	try {
    		con = getConnection();
    		return MailBoxTemplatesDAO.getTemplatesListByWriter(con);
    	} finally {
    		closeConnection(con);
    	}
    }

    public static MailBoxTemplate getTemplateById(long templateId) throws SQLException {
    	Connection con = null;
    	try {
    		con = getConnection();
    		return MailBoxTemplatesDAO.getTemplateById(con, templateId);
    	} finally {
    		closeConnection(con);
    	}

    }
}
