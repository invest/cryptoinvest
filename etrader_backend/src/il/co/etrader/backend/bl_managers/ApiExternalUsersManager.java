package il.co.etrader.backend.bl_managers;

import il.co.etrader.backend.bl_vos.ApiExternalUser;
import il.co.etrader.backend.dao_managers.ApiExternalUsersDAO;

import java.sql.Connection;
import java.sql.SQLException;

import org.apache.log4j.Logger;

import com.anyoption.common.managers.BaseBLManager;


public class ApiExternalUsersManager extends BaseBLManager {

    private static final Logger logger = Logger.getLogger(ApiExternalUsersManager.class);


	public static ApiExternalUser getApiExternalUser(long id, String reference, long apiUserId) throws SQLException {
		Connection con = getConnection();
		try {
			return ApiExternalUsersDAO.getApiExternalUser(con, id, reference, apiUserId);
		} finally {
			closeConnection(con);
		}
	}

	/**
	 * update external user details currently we can update only is_risky
	 * @param apiExternalUser
	 * @return true if success
	 * @throws SQLException
	 */
	public static boolean updateUserDetails(ApiExternalUser apiExternalUser) throws SQLException {
		Connection con = getConnection();
		try {
			return ApiExternalUsersDAO.updateUserDetails(con, apiExternalUser);
		} finally {
			closeConnection(con);
		}
	}

}
