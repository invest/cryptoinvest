package il.co.etrader.backend.util;

import il.co.etrader.util.ConstantsBase;

public class Constants extends ConstantsBase {

	public static final String USER_DETAILS = "USER_DETAILS";
	public static final String BACKEND = "backend";
	public static final String WEB_ROLE = "web";
	public static final String TEMPLATES_PATH = "templates.path";
	public static final String FILES_SERVER_PATH = "files.server.path";
	public static final String TEMPLATES_SERVER_PATH = "templates.server.path";
	public static final String LP_UPLOAD_PATH = "lp.upload.path";

	public static final String BIND_CANCELED_INVESTMENTS_LIST = "#{canceledInvestmentsList}";
	public static final String BIND_INVESTMENTS_LIST_EXCEL = "#{investmentsExcelList}";
	public static final String BIND_CHARGE_BACK = "#{chargeBack}";
	public static final String BIND_TRANSACTION = "#{transactionsForm.transaction}";
	public static final String BIND_INVESTMENTS_FORM = "#{investmentsForm}";
	public static final String BIND_DAILY_INVESTMENTS_FORM = "#{dailyInvestmentsForm}";
	public static final String BIND_REWARDS_USER_HISTORY_FORM = "#{rewardsUserHistoryForm}";
	public static final String BIND_ANAL_FORM = "#{analForm}";
	public static final String BIND_POPULATION_ENTRIES_FORM = "#{populationsEntriesForm}";
	public static final String BIND_ASSIGN_WRITERS_FORM = "#{assignWritersForm}";
	public static final String BIND_MASTER_SEARCH_FORM = "#{masterSearchForm}";
	public static final String BIND_SALES_DEPOSITS_FORM = "#{salesDepositsForm}";
	public static final String BIND_SALES_TURNOVERS_FORM = "#{salesTurnoversForm}";
	public static final String BIND_SALES_DEPOSITS_TV_FORM = "#{salesDepositsTVForm}";

	public static final String NAV_EMPTY = "empty";
	public static final String NAV_PARENT_REFRESH = "empty_parent_refresh";
	public static final String NAV_EMPTY_SUBTABS = "emptysubtabs";
	public static final String NAV_SEND_CONFIRM = "SendSmsConfirm";
	public static final String NAV_SEND_SUCCESS = "SendSmsSuccess";
	public static final String NAV_SUPPORT_MAIN = "supportmain";
	public static final String NAV_REGISTER = "register";
	public static final String NAV_SUCCESS = "success";
	public static final String NAV_FIRST_DEPOSIT = "firstDeposit";
	public static final String NAV_OTHER_DEPOSIT = "otherDeposit";
	public static final String NAV_USER = "user";
	public static final String NAV_CHANGE_PASS = "changePass";
	public static final String NAV_NEW_CREDIT_CARD = "newCard";
	public static final String NAV_DEPOSIT = "deposit";
	public static final String NAV_WITHDRAW = "withdraw";
	public static final String NAV_CARDS = "cards";
	public static final String NAV_CARD = "card";
	public static final String NAV_REVERSE = "reverse";
	public static final String NAV_TRANSACTIONS = "transactions";
	public static final String NAV_ADMIN_TRANSACTIONS = "adminTransactions";
	public static final String NAV_RETENTION_TRANSACTIONS = "retentionTransactions";
	public static final String NAV_ISSUES = "issues";
	public static final String NAV_ISSUES_MORE_OPTIONS = "issuesMoreOptions";
	public static final String NAV_ISSUE = "issue";
	public static final String NAV_ISSUE_ACTIONS = "issueActions";
	public static final String NAV_FILES = "files";
	public static final String NAV_FILE = "file";
	public static final String NAV_FRAUDS = "frauds";
	public static final String NAV_FRAUD = "fraud";
	public static final String NAV_INVESTMENTS = "investments";
	public static final String NAV_WIRE = "wire";
	public static final String NAV_TRANS_INVESTMENTS = "transInvestments";
	public static final String NAV_ADMIN_TRANS_ISSUE = "transactionIssue";
	public static final String NAV_ADMIN_TRANS_INVESTMENTS = "adminTransInvestments";
	public static final String NAV_ADMIN_DEPOSIT = "adminDeposit";
	public static final String NAV_ADMIN_WITHDRAW = "adminWithdraw";
	public static final String NAV_CHARGE_BACKS = "chargeBacks";
	public static final String NAV_CHARGE_BACK = "chargeback";
	public static final String NAV_ADMIN_CHARGE_BACK = "adminChargeback";
	public static final String NAV_RETENTION_CHARGE_BACK = "retentionChargeback";
	public static final String NAV_SEND_TEMPLATE = "sendTemplate";
	public static final String NAV_TEMPLATES = "templates";
	public static final String NAV_TEMPLATE = "template";
	public static final String NAV_LIMITS = "limits";
	public static final String NAV_LIMIT = "limit";
	public static final String NAV_USER_MARKET_DISABLE = "userMarketDisable";
	public static final String NAV_USERS_MARKET_DISABLE = "usersMarketDisable";
	public static final String NAV_MARKETING_CAMPAIGN = "campaign";
	public static final String NAV_MARKETING_CAMPAIGNS = "campaigns";
	public static final String NAV_MARKETING_SOURCES = "sources";
	public static final String NAV_MARKETING_SOURCE = "source";
	public static final String NAV_MARKETING_PIXELS = "pixels";
	public static final String NAV_MARKETING_PIXEL = "pixel";
	public static final String NAV_MARKETING_MEDIUMS = "mediums";
	public static final String NAV_MARKETING_MEDIUM = "medium";
	public static final String NAV_MARKETING_CONTENTS = "contents";
	public static final String NAV_MARKETING_CONTENT = "content";
	public static final String NAV_MARKETING_SIZES = "sizes";
	public static final String NAV_MARKETING_SIZE = "size";
	public static final String NAV_MARKETING_TYPES = "types";
	public static final String NAV_MARKETING_TYPE = "type";
	public static final String NAV_MARKETING_LOCATION = "location";
	public static final String NAV_MARKETING_LOCATIONS = "locations";
	public static final String NAV_MARKETING_LANDING_PAGES = "landingPages";
	public static final String NAV_MARKETING_LANDING_PAGE = "landingPage";
	public static final String NAV_MARKETING_COMBINATION = "combination";
	public static final String NAV_MARKETING_COMBINATIONS = "combinations";
	public static final String NAV_MARKETING_COMBINATIONS_SERIES = "combinationsSeries";
	public static final String NAV_MARKETING_COMBINATIONS_SERIES_LIST = "combinationsSeriesList";
	public static final String NAV_MARKETING_PAYMENT_RECIPIENT = "paymentRecipient";
	public static final String NAV_MARKETING_PAYMENT_RECIPIENTS = "paymentRecipients";
	public static final String NAV_MARKETING_TERRITORY = "territory";
	public static final String NAV_MARKETING_TERRITORIES = "territories";
	public static final String NAV_GROUPS = "groups";
	public static final String NAV_GROUP = "group";
	public static final String NAV_MARKETS = "markets";
	public static final String NAV_MARKET = "market";
	public static final String NAV_WRITERS = "writers";
	public static final String NAV_WRITER = "writer";
	public static final String NAV_OPPORTUNITIES = "opportunities";
	public static final String NAV_OPPORTUNITY = "opportunity";
	public static final String NAV_RECEIPT = "receipt";
	public static final String NAV_ADMIN_PENDING_WITHDRAWS = "adminPendingWithdraws";
	public static final String NAV_ADMIN_PENDING_WITHDRAW = "adminPendingWithdraw";
	public static final String NAV_SUPPORT_PENDING_WITHDRAWS = "supportPendingWithdraws";
	public static final String NAV_SUPPORT_PENDING_WITHDRAW = "supportPendingWithdraw";
	public static final String NAV_ACCOUNT_PENDING_WITHDRAWS = "accountPendingWithdraws";
	public static final String NAV_ACCOUNT_PENDING_WITHDRAW = "accountPendingWithdraw";
	public static final String NAV_ADMIN_TABS = "admin";
	public static final String NAV_ACCOUNTING_TABS = "accounting";
	public static final String NAV_SUPPORT_TABS = "support";
	public static final String NAV_TRADER_TABS = "trader";
	public static final String NAV_RETENTION_TABS = "retention";
	public static final String NAV_RETENTION_MANAGER_TABS = "retentionM";
	public static final String NAV_PARTNER_TABS = "partner";
	public static final String NAV_SUPPORT_MORE_TABS = "support_more_options";
	public static final String NAV_SADMIN_TABS = "sadmin";
	public static final String NAV_CMS_TABS = "cms";
	public static final String NAV_MARKETING_TABS = "marketing";
	public static final String NAV_PRODUCT_TABS = "product";
	public static final String NAV_MANAGER_ENTRY_STRIP = "entryStripManager";
	public static final String NAV_WITHDRAW_APPROVE = "approveWithdraw";
	public static final String NAV_CC_WITHDRAW_APPROVE = "approveCcWithdraw";
	public static final String NAV_WITHDRAW_APPROVE_NO_FEE = "approveWithdrawNoFee";
	public static final String NAV_CREDIT_WITHDRAW_APPROVE = "creditApproveWithdraw";
	public static final String NAV_PAYPAL_WITHDRAW_APPROVE = "payPalApprove";
	public static final String NAV_BANK_WIRE_APPROVE = "bankWireApprove";
	public static final String NAV_BANK_WIRE_APPROVE_NO_FEE = "bankWireapproveNoFee";
	public static final String NAV_WITHDRAW_APPROVE_WEBMONEY = "withdrawApproveWebmoney";
	public static final String NAV_POPULATION = "population";
	public static final String NAV_POPULATIONS = "populations";
	public static final String NAV_POPULATIONS_STRIP = "entryStrip";
	public static final String NAV_RETENTION_MANAGER_ASSIGNM = "assignManager";
	public static final String NAV_RETENTION_CALLBACKS = "callbacks";
	public static final String NAV_RETENTION_MANAGER_ENTRIES_TAB = "phoneRetentionM";
	public static final String NAV_RETENTION_MANAGER_ENTRIES_TAB_RETENTION = "phoneContentRetentionM";
	public static final String NAV_RETENTION_MANAGER_ENTRIES_TAB_CONVERTION = "phoneContentConvertionM";
	public static final String NAV_RETENTION_MANAGER_ENTRIES_TAB_IMMEDIATE_TREATMENT = "immediateTreatment";
	public static final String NAV_RETENTION_OPEN_WITHDRAWS_TAB = "openWithdraws";
	public static final String NAV_MARKETING_AFFILATE = "affilate";
	public static final String NAV_MARKETING_AFFILATES = "affilates";
	public static final String NAV_MARKETING_SUB_AFFILATE = "subAffiliate";
	public static final String NAV_MARKETING_SUB_AFFILATES = "subAffiliates";
	public static final String NAV_RISK_ISSUE = "riskIssue";
	public static final String NAV_RISK_ISSUE_ACTIONS = "riskIssueActions";
	public static final String NAV_MONEYBOOKERS_WITHDRAW_APPROVE = "moneybookersApprove";
	public static final String NAV_ZERO_ONE_HUNDRED_UPDATE = "zeroOneHundred";
	public static final String NAV_ZERO_ONE_HUNDRED_UPDATE_BACK = "zeroOneHundredBack";
	public static final String NAV_REROUTING_PRIORITY_CHANGE = "reroutingPriorityChange";
	public static final String NAV_REROUTING_PRIORITY_CHANGE_BACK = "reroutingPriorityChangeBack";
	public static final String NAV_MAIN = "search";
	public static final String NAV_EXTERNAL_SUPPORT = "externalSupport";
	public static final String NAV_ASSET_INDEXES_MARKET = "assetIndexesMarket";
	public static final String NAV_ASSET_INDEX_MARKET = "assetIndexMarket";


	public static final String NAV_ODDS_TYPES = "oddsTypes";
	public static final String NAV_ODDS_TYPE = "oddsType";

	public static final String NAV_SHIFTINGS = "shiftings";

	public static final String NAV_OPP_TEMPLATES = "oppTemplates";
	public static final String NAV_OPP_TEMPLATE = "oppTemplate";

	public static final String NAV_CC_BLACK_LIST = "ccblacklist";
	public static final String NAV_CC_BLACK_LIST_EDIT = "ccblacklistedit";

	public static final String NAV_EXCHANGES = "exchanges";
	public static final String NAV_EXCHANGE = "exchange";

	public static final String NAV_EXCHANGE_HOLIDAYS = "exchangeHolidays";
	public static final String NAV_EXCHANGES_HOLIDAYS = "exchangesHolidays";

	public static final String NAV_INV_LIMITS = "invLimits";
	public static final String NAV_INV_LIMIT = "invLimit";

	public static final String NAV_INV_LIMIT_GROUPS = "invLimitGroups";
	public static final String NAV_INV_LIMIT_GROUP = "invLimitGroup";

	public static final String NAV_INV_LIM_GROUPS = "invLimGroups";
	public static final String NAV_INV_LIM_GROUP = "invLimGroup";

	public static final String NAV_OPTION_EXPIRATION = "optionExpiration";
	public static final String NAV_OPTIONS_EXPIRATIONS = "optionsExpirations";

	public static final String NAV_INSERT_ONE_TOUCH_SUCCEED = "insertOneTouchSucceed";
	public static final String NAV_INSERT_ONE_TOUCH_FAILED = "insertOneTouchFailed";

	public static final String NAV_LIABILITY_GRAPH = "liabilityGraph";

	public static final String NAV_BONUS_USERS_SUPPORT = "bonusUsersSupport";
	public static final String NAV_BONUS_USERS_RETENTION = "bonusUsersRerention";
	public static final String NAV_BONUS_USER = "bonusUser";

	public static final String NAV_SALES_DEPOSITS_DETAILS = "salesDepositsDetails";
	public static final String NAV_SALES_TURNOVERS_DETAILS_BY_DATE = "salesTurnoversDateDetails";

	public static final String NAV_ODDS_PAIRS = "oddsPair";
	public static final String NAV_ODDS_GROUP_PAIRS = "oddsGroupPair";
	public static final String NAV_ODDS_GROUPS_PAIRS = "oddsGroupsPair";

	public static final String NAV_LIVE_GLOBE_CITIES = "liveGlobeCities";
	public static final String NAV_LIVE_GLOBE_CITY = "liveGlobeCity";

	public static final String NAV_CONSTANT_INVESTMENTS_STREAMS = "constantInvesmentsStreams";
	public static final String NAV_CONSTANT_INVESTMENT_STREAM = "constantInvesmentStream";

	public static final String NAV_LIMITS_CONTENT = "limitsContent";

    public static final String NAV_LIMITATION_DEPOSITS_SEARCH = "limitationDeposits";
    public static final String NAV_LIMITATION_DEPOSITS_NEW = "limitationDeposit";
    public static final String NAV_LIMITATION_DEPOSITS_UPDATE = "limitationDepositUpdate";
    public static final String NAV_REWARD_USER_UPDATE_SUCCESS = "rewardUserUpdateSuccess";
    
    public static final String NAV_DEEPLINKK_PARAMS_TO_URL_SEARCH = "deeplinkParamsToUrls";
    public static final String NAV_DEEPLINKK_PARAMS_TO_URL_NEW = "deeplinkParamsToUrl";

    public static final String NAV_CLEARING_ROUTE = "clearingRoute";
    public static final String NAV_CLEARING_ROUTES = "clearingRoutes";
    public static final String NAV_ROUTE_CONFIRMATION = "confirmation";
    public static final String NAV_TOURNAMENTS = "tournaments";
    public static final String NAV_TOURNAMENT = "tournament";
    public static final String NAV_TOURNAMENTS_BANNERS = "tournamentsBanners";
    
    public static final String NAV_ADD_TC = "TCAccessAdd";
    public static final String NAV_TC = "TCAccess";
    
	public static final int MENU_BUTTON_SUPPORT = 0;
	public static final int MENU_BUTTON_ADMIN = 1;
	public static final int MENU_BUTTON_TRADER = 2;
	public static final int MENU_BUTTON_ACCOUNTING = 3;
	public static final int MENU_BUTTON_SADMIN = 4;
	public static final int MENU_BUTTON_RETENTION = 5;
	public static final int MENU_BUTTON_SUPPORT_MORE_OPTIONS = 6;
	public static final int MENU_BUTTON_MARKETING = 7;
	public static final int MENU_BUTTON_RETENTION_MANAGER = 8;
	public static final int MENU_BUTTON_PARTNER = 9;
	public static final int MENU_BUTTON_CMS = 10;
	public static final int MENU_BUTTON_CONTROL = 11;
	public static final int MENU_BUTTON_PRODUCT = 12;

	public static final String NAV_POPUP_MESSAGE= "/jsp/templates/pop_up_message";
	public static final String NAV_CANCEL_INVESTMENTS= "/jsp/trader/cancel_all_investments";

	public static final String ENUM_TIMEZONE = "time_zone";

	public static final String ENUM_MASTER_SEARCH_CODE = "master_password";
	public static final String ENUM_MASTER_SEARCH_ENUMERATOR = "master_search";

	public static final int PORT_HTTP = 80;

	public static final int PORT_HTTPS = 443;

	public static final int LANGUAGES_HEBREW = 1;
	public static final int LANGUAGES_ENGLISH = 2;

	public static final int TEMPLATE_INCOME_TAX_ID = 14;

	public static final String NAV_ISSUES_ADMIN = "issuesAdmin";
	public static final String NAV_ISSUES_RETENTION = "issuesRetention";
	public static final String NAV_ISSUES_RETENTIONM = "issuesRetentionM";
	public static final String NAV_ISSUES_PARTNER = "issuesPartner";
	public static final String NAV_CHANGE_WRITER_PASS = "changeWriterPass";

	public static final String TRADERTOOL_FAILED = "tradertool.faild";
	public static final String TRADERTOOL_SUCCESS = "tradertool.success";
	public static final String NAV_ET_QUESTIONNAIRE = "questionnaireET";

	public static final int TRADERTOOL_DISABLE_OPP = 1;
	public static final int TRADERTOOL_ENABLE_OPP = 0;

	public static final double TRADERTOOL_BTRADER_SPREAD_LIMIT = 0.1;
	public static final double TRADERTOOL_TRADER_SPREAD_LIMIT = 0.4;
	public static final double TRADERTOOL_ASTRADER_SPREAD_LIMIT = 9;

    public static final long TRADERTOOL_BTRADER_EXPOSURE_LIMIT = 2000;
	public static final long TRADERTOOL_TRADER_EXPOSURE_LIMIT = 25000;
	public static final long TRADERTOOL_ASTRADER_EXPOSURE_LIMIT = 250000;

	public static final double TRADERTOOL_TRADER_PARAM_Q_UP_LIMIT = 30;
	public static final double TRADERTOOL_TRADER_PARAM_Q_DOWN_LIMIT = 5.2;
	public static final double TRADERTOOL_TRADER_PARAM_T_UP_LIMIT = 20;
	public static final double TRADERTOOL_TRADER_PARAM_T_DOWN_LIMIT = -20;
	public static final double TRADERTOOL_TRADER_PARAM_C_UP_LIMIT = 15;
	public static final double TRADERTOOL_TRADER_PARAM_C_DOWN_LIMIT = 2;
	public static final double TRADERTOOL_TRADER_PARAM_C_UP_LIMIT_SHY = 30; //for SD & SB

	public static final String SETTLED = "1";
	public static final String NOT_SETTLED = "0";

	public static final String MARKETS_SELECT_TAG = "<marketsSelect>";
	public static final String MARKETS_SELECT_CLOSING_TAG = "</marketsSelect>";

	public static final String USER_ACTIVE = "1";
	public static final String USER_NOT_ACTIVE = "0";
	public static final int USER_ACTIVE_INT = 1;
	public static final int USER_NOT_ACTIVE_INT = 2;

	public static final String NAV_BONUS = "bonus";
	public static final String NAV_BONUSES = "bonuses";

	public static final String NAV_MAILBOX_TEMPLATE = "mailBoxTemplate";
	public static final String NAV_MAILBOX_TEMPLATES = "mailBoxTemplates";

    public static final String NAV_MEDIA_BUYER_TARGET = "mediaBuyerTarget";
    public static final String NAV_MEDIA_BUYER_TARGETS = "mediaBuyerTargets";

	public static final String NAV_MAILBOX_EMAIL = "mailBoxEmail";
	public static final String NAV_MAILBOX_EMAILS = "mailBoxEmails";

	public static final int CAPTURE_RUN_ISR_HOUR = 18;
	public static final long CAPTURE_INFO_INVEST = 1;
	public static final long CAPTURE_INFO_NOT_INVEST = 2;

	public static final int RETENTION_PAGE_SIZE = 10;

	public static final String SALES_OVERVIEW_FILTER = "1";
	public static final String SALES_TODAY_FILTER = "0";

	public static final int USER_MARKET_DISABLE_PERID = 5;
	public static final int USER_MARKET_LIMIT_PERID = 5;

	public static final String SALES_TIME_CREATED_FILTER = "0";
	public static final String SALES_TIME_SETTLED_FILTER = "1";

	public static final int SALES_QUAL_FILTER_QUALIFIED = 1;
	public static final int SALES_QUAL_FILTER_NOT_QUALIFIED = 2;

	public static final String SALES_DEPOSIT_TYPE_CONVERSION_TXT = "Conversion";
	public static final String SALES_DEPOSIT_TYPE_RETENTION_TXT = "Retention";

	public static final int SALES_DEPOSIT_SOURCE_SALES = 1;
	public static final int SALES_DEPOSIT_SOURCE_INDEPENDENT = 2;


	// Sales depositsTV pages
	//public static final int DAILY_DEPOSITS_ET = 0;
	//public static final int WEEKLY_DEPOSITS_ET = 1;
	//public static final int DAILY_DEPOSITS_AO = 2;
	//public static final int WEEKLY_DEPOSITS_AO = 3;
	//public static final int DEPOSIT_TV_FIRST_VIEWS = 4;
	//-------------------------------------------------
//	public static final int DAILY_DEPOSITS_ET = 0;
//	public static final int DAILY_DEPOSITS_AO = 1;
//	public static final int MONTHLY_DEPOSITS_ET = 2;
//	public static final int MONTHLY_DEPOSITS_AO = 3;

	public static final int DAILY_DEPOSITS = 0;
	public static final int MONTHLY_DEPOSITS = 1;
	public static final int DEPOSIT_TV_SECOND_VIEWS = 2;

	public static final int DAILY_DEPOSITS_CONVERSION = 0;
	public static final int MONTHLY_DEPOSITS_CONVERSION = 1;

	public static final int SALES_TV_DB_CHECK_MINUTES = 5;

	// pages periods
	public static final int SALES_DEPOSIT_DAILY_VIEW = 1;
	public static final int SALES_DEPOSIT_WEEKLY_VIEW = 2;
	public static final int SALES_DEPOSIT_MONTHLY_VIEW = 3;

	public static final long DEPARTMENT_SALES = 3;

	// Activities Roles for disply
	public static final int ISSUE_ACTIVITY_ROLE_SUPPORT = 1;
	public static final int ISSUE_ACTIVITY_ROLE_SALES = 2;
	public static final int ISSUE_ACTIVITY_ROLE_SALES_M = 3;
	public static final int ISSUE_ACTIVITY_ROLE_ADMIN = 4;

	public static final int CALL_BACK_INTERVAL = 10;
	public static final int CALL_BACK_INTERVAL_PARTNERS = 15;
	//Succeeded transaction
	public static final int SUCCEEDED_TRANSACTION = 1;
	public static final int NOT_SUCCEEDED_TRANSACTION = 0;

	//Marketing report
	public static final int MARKETING_REPORT_TYPE_BASIC = 1;
	public static final int MARKETING_REPORT_TYPE_HOUSE_WIN = 2;
	public static final int MARKETING_REPORT_TYPE_GOOGLE_DP = 3;
	public static final int MARKETING_REPORT_TYPE_GOOGLE_DATE = 4;
	public static final int MARKETING_REPORT_TYPE_GOOGLE_MONTH = 5;
	public static final int MARKETING_REPORT_TYPE_REG_LEADS_TOTAL = 6;
	public static final int MARKETING_REPORT_TYPE_REG_LEADS_DETAILS = 7;
	public static final int MARKETING_REPORT_TYPE_REMARKETING_REPORT = 8;
	public static final int MARKETING_REPORT_TYPE_HTTP_REF_REPORT  = 9;
	public static final int MARKETING_REPORT_TYPE_BASE_BY_DATE_REPORT = 10;
	public static final int MARKETING_REPORT_TYPE_DP_BY_DATE_REPORT  = 11;

	// Templates - using cc details attachments
	public static final int TEMPLATE_POA = 32;
	public static final int TEMPLATE_FIRST_WITHDRAWAL = 33;
	public static final int TEMPLATE_JUST_CONFIRMATION = 38;
	public static final int TEMPLATE_JUST_CONFIRMATION_OTHER = 39;
	public static final int TEMPLATE_NON_CC_WITHDRAWAL = 40;
	public static final int TEMPLATE_BANK_TRANSFER_DETAILS_USER = 52;
	public static final int TEMPLATE_BANK_TRANSFER_DETAILS_CONTACT = 53;
	public static final String TEMPLATE_BANK_TRANSFER_DETAILS_ETRADER = ".etrader.general";
	public static final String TEMPLATE_BANK_TRANSFER_DETAILS_AO = ".general";
	public static final int TEMPLATE_BEFORE_DEPOSIT = 67;
	public static final int TEMPLATE_AFTER_DEPOSIT = 66;
	public static final long TEMPLATE_ACTIVATION_MAIL_LOW_SCORE = 68;
	public static final long TEMPLATE_ACTIVATION_MAIL_TRESHOLD = 69;
	public static final long TEMPLATE_AO_ACTIVATION_MAIL_TRESHOLD = 76;
	public static final long TEMPLATE_CLOSING_OF_ACCOUNT = 83;
	

	//issue subject by type
	public static final int ISSUE_SUBJECTS = 1;
	public static final int RISK_SUBJECTS = 2;

	//Arabic partner writer
	public static final long PARTNER_ARABIC_ID = 332;
	public static final String ENUM_PARTNER_ARABIC_DATE_CODE = "partner_arabic_date";
	public static final String ENUM_PARTNER_ARABIC_DATE_ENUMERATOR = "partner_arabic";

	//Default comments for risk issue
	public static final String IA_CONTACT_USER_COMMENT = "Please contact user";
	public static final String IA_NEED_DUCUMENTS_COMMENT = "Please ask for documents";

	//Risk issue collapse/expand status
	public static final int RISK_ISSUES_COLLAPSE = 1;
	public static final int RISK_ISSUES_EXPAND = 2;

	//Doc request type
	public static final String RISK_DOC_REQ_ALL = "0";
	public static final String RISK_DOC_REQ_ID = "1";
	public static final String RISK_DOC_REQ_CC = "2";
	public static final String RISK_DOC_REQ_CONTACT_USER = "3";

	// Reg Leads Types
	public static final int REG_LEADS_TYPE_CAMPAIGN_ENTRIES = 1;
	public static final int REG_LEADS_TYPE_CALL_ME = 2;
	public static final int REG_LEADS_TYPE_REG_CALL_ME = 3;
	public static final int REG_LEADS_TYPE_SHORT_REG = 4;
	public static final int REG_LEADS_TYPE_REG_SHORT_REG = 5;
	public static final int REG_LEADS_TYPE_DIRECT_REG = 6;
	public static final int REG_LEADS_TYPE_USERS_NUM = 7;
	public static final int REG_LEADS_TYPE_FIRST_REG_DEP = 8;

	// email/sms types
	public static final int SEND_SMS = 1;
	public static final int SEND_EMAIL = 2;

	public static final long COUNTRY_ID_FRANCE = 73;
	public static final long POPULATION_TYPE_ID_FDD = 1;
	public static final long POPULATION_TYPE_ID_CALL_ME = 6;
	public static final long POPULATION_TYPE_ID_NO_DEPOSIT = 5;
	public static final long POPULATION_TYPE_ALL = 0;
    //Dep ID
    public static final long DEPARTMENT_ID_MARKETING = 8;

    ///population typesId
    public static final int POPULATION_TYPE_ID_NON = 0;
    public static final int POPULATION_TYPE_ID_CONVERSION = 1;
    public static final int POPULATION_TYPE_ID_RETENTION = 2;

    // priority of affiliate
    public static final int AFFILIATE_PRIORITY_LOW = 0;
    public static final int AFFILIATE_PRIORITY_MEDIUM = 1;
    public static final int AFFILIATE_PRIORITY_HIGH = 2;
    public static final int AFFILIATE_PRIORITY_ALL = 3;
    public static final String UPDATE_AFFILIATE_LIST = "updateAffiliateList";
    public static final String ADDED_AFFILIATE = "true";
    public static final String NOT_ADDED_AFFILIATE = "false";
    public static final String UPDATE_AFFILIATE_NAME_KEY_LIST = "updateAffiliateNameKeyList";

    //transaction type
    public static final long TRANSACTION_TYPE_STATUS_REQUESTED = 4;
    public static final long TRANSACTION_TYPE_STATUS_APPROVED = 9;
    //Skrill
    public static final long TRANSACTION_TYPE_SKRILL_DEPOSIT = 32;
    public static final long TRANSACTION_TYPE_SKRILL_WITHDRAW = 36;
    public static final long TRANS_TYPE_MAINTENANCE_FEE = 47;



    // enumerator for User last login.
	public static final String ENUM_LAST_LOGIN_LEVEL_ONE_CODE = "user.last.login.1";
	public static final String ENUM_LAST_LOGIN_LEVEL_TWO_CODE = "user.last.login.2";
	public static final String ENUM_LAST_LOGIN_ENUMERATOR = "last_login";
	public static final int MINUTES_IN_HOUR = 60;

    public static final int PROMOTION_STATUS_ID_CREATED = 1;
    public static final int PROMOTION_STATUS_ID_SENDED = 2;
    public static final int PROMOTION_STATUS_ID_CANCELED = 3;

    public static final String PROMOTION_ID_SEQUENCE = "SEQ_PROMOTION_ID";
    public static final String PROMOTION_TOTAL_AND_INSERTED_USERS = "promotionsTotalAndInsertedUsers";
    //public static final String PROMOTION_TOTAL_SENDED_TO_USERS = "promotionsSendedToClients";

    public static final long SALES_ACTIVE_REP = 1;
    public static final long SALES_NON_ACTIVE_REP = 0;

    public static final int TEMPLATES_SEND_CUSTOMER = 0;
    public static final int TEMPLATES_SEND_SUPPORT = 1;
    public static final int TEMPLATES_SEND_CUSTOMER_AND_SUPPORT = 2;

    public static final int TEMPLATE_PASSWORD_REMINDER = 3;

    //sales support report
    public static final String NAV_CONTROL_TABS = "control";
  	public static final int SALES_SUPPORT_REPORT_TYPE_ISSUE_ACTION = 1;
  	public static final int SALES_SUPPORT_REPORT_TYPE_CONVERSION_CALL = 2;
  	public static final int SALES_SUPPORT_REPORT_TYPE_TRANSACTION = 3;
  	public static final int TRANSACTION_ISSUE_INSERT_TYPE_MANUALLY = 2;

  	public static final int TRANSACTION_DEPOSIT_WAS_REROUTS_ALL = 0;
  	public static final int TRANSACTION_DEPOSIT_WAS_NOT_REROUTS = 1;
  	public static final int TRANSACTION_DEPOSIT_WAS_REROUTS = 2;
	public static final String MAX_ASSIGNMENT_REPORT = "maxAssignmentReport";
	public static final long TRANSACTION_DEPOSIT_IS_NOT_MANUAL_ROUTE = 0;
	public static final long TRANSACTION_DEPOSIT_IS_MANUAL_ROUTE = 1;

	public static final int INATEC = 1;
	public static final int WIRE_CARD = 2;
	public static final int TBI =  3;
	public static final int WIRE_CARD_3D = 4;
	public static final int WALPAY = 5;

	public static final long REROUTING_LIMIT_CURRENCY_ID = 1;
	public static final long REROUTING_LIMIT_CREDIT_CARD_TYPE_ID = 2;
	public static final long REROUTING_LIMIT_BIN_ID = 3;
	public static final long REROUTING_LIMIT_COUNRTY_ID = 4;

	public static final long SCREEN_TYPE_DATIKA = 1;
	public static final long SCREEN_TYPE_TT = 2;

	public static final long DEFAULT_WRITER_ID_FOR_SKIN = 0;
	public static final String SELECTED_ISSUE_SUBJECT_ID = "issueSubjectId";
	public static final String NAV_FEE_CONFIGURATION_EDIT = "feeConfigurationEditContent";
	public static final String NAV_FEE_CONFIGURATION = "feeConfiguration";

	public static final int IS_TESTER = 1;
	public static final int IS_NOT_TESTER = 0;
	public static final String USERS_DETAILS_CHECKED = "Checked";
	public static final String USERS_DETAILS_UN_CHECKED = "Un-checked";

	public static final long ETS_REQUEST_TYPE_CAMPAIGN = 1;
	public static final long ETS_REQUEST_TYPE_COMBINATION = 2;
	public static final long ETS_ACTION_TYPE_INSERT = 1;
	public static final long ETS_ACTION_TYPE_UPDATE = 2;

	public static final String IS_VOICE_SPIN = "1";
	public static final String IS_NOT_VOICE_SPIN = "0";
	public static final String RETENTION_ASSIGNMENTS_TAB = "0";
	public static final String RETENTION_ENTRY_STRIP_TAB = "5";
	public static final String RETENTION_ERROR_TAB = "6";

    public static final long ALL_FILTER_ID1 = -1;
    public static final long FILTER_NOT_ACTIVE = 0;
    public static final long FILTER_ACTIVE = 1;
    
    public static final String LOW_TIERS = "low_tiers";
    public static final String HIGH_TIERS = "high_tiers";
    
    public static final String HOST_URL_HTTPS = "host.url.https";
    public static final String COOKIE_WRITER_NAME = "be_wn";
    
    public static final String ENUM_VOICE_SPIN_CODE = "voice_spin";
	public static final String ENUM_ACCESS_TOKEN_ENUMERATOR = "access_token";
}
