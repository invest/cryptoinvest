package il.co.etrader.backend.util;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Method;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Locale;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.ValidatorException;

import org.apache.log4j.Logger;
import org.apache.myfaces.renderkit.html.util.AddResource;
import org.apache.myfaces.renderkit.html.util.AddResourceFactory;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFRichTextString;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import com.anyoption.common.annotations.MethodInfoAnnotations;
import com.anyoption.common.beans.base.Currency;
import com.anyoption.common.beans.base.Skin;
import com.anyoption.common.managers.CreditCardsManagerBase;

import il.co.etrader.backend.bl_managers.ApplicationData;
import il.co.etrader.backend.bl_managers.OpportunitiesManager;
import il.co.etrader.backend.bl_managers.UsersManager;
import il.co.etrader.backend.bl_vos.User;
import il.co.etrader.backend.bl_vos.WriterWrapper;
import il.co.etrader.bl_managers.ApplicationDataBase;
import il.co.etrader.bl_vos.UserBase;
import il.co.etrader.util.CommonUtil;
import il.co.etrader.util.ConstantsBase;

/**
 * Backend utils
 */
public class Utils extends CommonUtil {
	private static final Logger log = Logger.getLogger(Utils.class);

	public static boolean userSelected() {
		FacesContext context=FacesContext.getCurrentInstance();
		UserBase user= (UserBase)context.getApplication().createValueBinding(Constants.BIND_USER).getValue(context);
		if (user.getId()==0) {
			FacesMessage fm=new FacesMessage(FacesMessage.SEVERITY_ERROR,CommonUtil.getMessage("error.user.not.selected",null, getWriterLocale(context)),null);
        	context.addMessage(null, fm);
            
			return false;
		}

		return true;
	}

	public static void refreshUserStrip() {
		FacesContext facesContext = FacesContext.getCurrentInstance();
		String javaScriptText = "parent.document.frames['user_strip'].location.reload();";

	    // Add the Javascript to the rendered page's header for immediate execution
	    AddResource addResource = AddResourceFactory.getInstance(facesContext);
	    addResource.addInlineScriptAtPosition(facesContext, AddResource.HEADER_BEGIN, javaScriptText);

	}

	public String getUtcDifference() {
		return "0";
//    	FacesContext context = FacesContext.getCurrentInstance();
//    	User user = (User)context.getApplication().createValueBinding(ConstantsBase.BIND_USER).getValue(context);
//    	WriterWrapper w = (WriterWrapper) context.getApplication().createValueBinding(ConstantsBase.BIND_WRITER_WRAPPER).getValue(context);
//    	String UTC1 = w.getUtcOffset();
//    	String UTC2 = user.getUtcOffset();
//    	return getUtcDifference(UTC1, UTC2);
	}

	public static String getUtcDifference(String UTC1, String UTC2) {
    	int time1;
    	int time2;
    	boolean pos;
    	if(CommonUtil.isParameterEmptyOrNull(UTC1) ||
    			CommonUtil.isParameterEmptyOrNull(UTC2)){
    		return "";
    	}
    	try {
    		Pattern regex = Pattern.compile("GMT([+-])(\\d{2}):(\\d{2})",
    			Pattern.CANON_EQ);
    		Matcher matcher = regex.matcher(UTC1);
    		if (matcher.find()) {
    			pos = matcher.group(1).equals("+");
    			if (pos) {
    				time1 = Integer.valueOf(matcher.group(2))*60;
    				time1 += Integer.valueOf(matcher.group(3));
    			}
    			else {
    				time1 = -Integer.valueOf(matcher.group(2))*60;
    				time1 -= Integer.valueOf(matcher.group(3));
    			}
    		}
    		else {
    			return "";
    		}
    		matcher = regex.matcher(UTC2);
    		if (matcher.find()) {
    			pos = matcher.group(1).equals("+");
    			if (pos) {
    				time2 = Integer.valueOf(matcher.group(2))*60;
    				time2 += Integer.valueOf(matcher.group(3));
    			}
    			else {
    				time2 = -Integer.valueOf(matcher.group(2))*60;
    				time2 -= Integer.valueOf(matcher.group(3));
    			}
    		}
    		else {
    			return "";
    		}
    		int dist = time2 - time1;
    		String res = (dist<0 ? "-" : "+");
    		int min = dist % 60;
    		dist -= min;
    		int hour = Math.abs(dist) / 60;
    		NumberFormat formatter = new DecimalFormat("00");
    		return (dist<0 ? "-" : "+") + formatter.format(hour) + ":" + formatter.format(min);
    	} catch (PatternSyntaxException ex) {
    		return "";
    	}
    }


	public static void popupMessage(String msgCode) {

		FacesContext facesContext = FacesContext.getCurrentInstance();

		//User user= (User)facesContext.getApplication().createValueBinding(Constants.BIND_USER).getValue(facesContext);
		//user.setGeneralMessage(CommonUtil.getMessage(msgCode,null));

		//final String viewId = Constants.NAV_POPUP_MESSAGE;

	    // This is the proper way to get the view's url
	    //ViewHandler viewHandler = facesContext.getApplication().getViewHandler();
	    //String actionUrl = viewHandler.getActionURL(facesContext, viewId);

	    //String javaScriptText = "window.open('"+actionUrl+"', 'popupWindow', 'titlebar=no,scrollbars=no,directories=no,menubar=no,resizable=no,location=no,toolbar=no,status=no'); ";
		String javaScriptText = "alert('"+CommonUtil.getMessage(msgCode,null, getWriterLocale(facesContext))+"'); ";

	    // Add the Javascript to the rendered page's header for immediate execution
	    AddResource addResource = AddResourceFactory.getInstance(facesContext);
	    addResource.addInlineScriptAtPosition(facesContext, AddResource.HEADER_BEGIN, javaScriptText);

	}

	public static void popupMessage(String msgCode, String[] params) {

		FacesContext facesContext = FacesContext.getCurrentInstance();
		String javaScriptText = "alert('"+CommonUtil.getMessage(msgCode,params, getWriterLocale(facesContext))+"'); ";

	    // Add the Javascript to the rendered page's header for immediate execution
	    AddResource addResource = AddResourceFactory.getInstance(facesContext);
	    addResource.addInlineScriptAtPosition(facesContext, AddResource.HEADER_BEGIN, javaScriptText);

	}

	/*
	 * get the (session) writer locale (the session writer is an object the exists on the session scope
	 * to do: this function should be removed
	 */
	public static Locale getWriterLocale(FacesContext context) {
		return getWriter().getLocalInfo().getLocale();
	}

	/*
	 * get the (session) writer locale (the session writer is an object the exists on the session scope
	 */
	public static Locale getWriterLocale() {
		return getWriter().getLocalInfo().getLocale();
	}

	/**
	 * This method returns the context user if exists or DB user by id
	 * @return context user
	 * @throws SQLException
	 */
	public static User getUser(long userId) throws SQLException {
		FacesContext context = FacesContext.getCurrentInstance();
		User user= (User)context.getApplication().createValueBinding(Constants.BIND_USER).getValue(context);
		if (user.isSupportSelected()){
			return user;
		} else {
			return UsersManager.getUserDetails(userId);
		}
	}

	/**
	 * This method returns the context user
	 * @return context user
	 * @throws SQLException
	 */
	public static User getUser() {
		FacesContext context = FacesContext.getCurrentInstance();
		User user= (User)context.getApplication().createValueBinding(Constants.BIND_USER).getValue(context);
		return user;
	}

	/**
	 * This method returns the context writer
	 * @return context writer
	 */
	public static WriterWrapper getWriter() {
		FacesContext context = FacesContext.getCurrentInstance();
		WriterWrapper writer = (WriterWrapper)context.getApplication().createValueBinding(Constants.BIND_WRITER_WRAPPER).getValue(context);
		return writer;
	}
	
	public static boolean isWriterAllowedToViewCcFirst6Digits() {
		ArrayList<Long> writers = ApplicationDataBase.getWritersAllowedToViewCcFirst6Digits();
		long loggedWriterId = getWriter().getWriter().getId();
		if (writers != null && writers.contains(loggedWriterId)) {
			return true;
		}else{
			return false;
		}
	}
	
	public static int isSuspended(long id) throws SQLException {
	    return OpportunitiesManager.isSuspendedOpp(id);
	}

	/**
	 * Get applicationDatainstance
	 * @return
	 * 		appplicationData instance
	 */
	public static ApplicationData getAppData() {
		try {
			FacesContext context = FacesContext.getCurrentInstance();
			ApplicationData appData = (ApplicationData) context.getApplication().createValueBinding(ConstantsBase.BIND_APPLICATION_DATA).getValue(context);
			return appData;
		} catch (Exception npe) {
			return null;
		}
	}

	/**
	 *  validateStreetNo by user skin
	 */
	public static void validateStreetNo(FacesContext context, UIComponent comp, Object value) throws Exception {
		String v = (String) value;
		User user = (User)context.getApplication().createValueBinding(ConstantsBase.BIND_USER).getValue(context);
		Locale userLocale = user.getValidatorLocale();
		String pattern = CommonUtil.getMessage("general.validate.streetnum", null, userLocale);

		if (!v.matches(pattern)) {

			FacesMessage msg = new FacesMessage(CommonUtil.getMessage("error.streetnum", null));
			throw new ValidatorException(msg);
		}
	}

	/**
	 *  validateStreet by user skin
	 */
	public static void validateStreet(FacesContext context, UIComponent comp, Object value) throws Exception {
		String v = (String) value;
		User user = (User)context.getApplication().createValueBinding(ConstantsBase.BIND_USER).getValue(context);
		Locale userLocale = user.getValidatorLocale();
		String pattern = CommonUtil.getMessage("general.validate.street", null, userLocale);
		if (!v.matches(pattern) || v.trim().equals("")) {
			FacesMessage msg = new FacesMessage(CommonUtil.getMessage("error.street", null));
			throw new ValidatorException(msg);
		}
	}

	/**
	 *  validateLettersOnly by user skin
	 */
	public static void validateLettersOnly(FacesContext context, UIComponent comp, Object value) throws Exception {
		User user = (User)context.getApplication().createValueBinding(ConstantsBase.BIND_USER).getValue(context);
		Locale userLocale = user.getValidatorLocale();
		String pattern = CommonUtil.getMessage("general.validate.letters.only", null, userLocale);
		String v = (String) value;
		if (!v.matches(pattern) || v.trim().equals("")) {
			FacesMessage msg = new FacesMessage(CommonUtil.getMessage("error.letters.only", null));
			throw new ValidatorException(msg);
		}
	}

	/**
	 *  validateLettersAndNumbersOnly by user skin
	 */
	public static void validateLettersAndNumbersOnly(FacesContext context, UIComponent comp, Object value) throws UnsupportedEncodingException {
		String v = (String) value;
		User user = (User)context.getApplication().createValueBinding(ConstantsBase.BIND_USER).getValue(context);
		Locale userLocale = user.getValidatorLocale();
		String pattern = CommonUtil.getMessage("general.validate.letters.and.numbers", null, userLocale);
		if (!v.matches(pattern)) {
			FacesMessage msg = new FacesMessage(CommonUtil.getMessage("error.lettersandnumbers.only", null));
			throw new ValidatorException(msg);
		}
	}

	public static void validateNumbersAndCommasOnly(FacesContext context, UIComponent comp, Object value) throws Exception {
		String pattern = "^([0-9]([0-9]*[,]{0,1}[0-9]+)*)*$";
		String v = (String) value;
		if (!v.matches(pattern) || v.trim().equals("")) {
			FacesMessage msg = new FacesMessage(CommonUtil.getMessage("error.numbers.commas.only", null));
			throw new ValidatorException(msg);
		}
	}
	/**
	 *  validateCcPassCode
	 */
	public static void validateCcPassCode(FacesContext context, UIComponent comp, Object value) throws Exception {
		String v = (String) value;
		if (!v.equals("") && v != null) {
			String pattern = "^([0-9]([0-9]*{0,1}[0-9]+)*)*$";
			if (!v.matches(pattern)) {
				FacesMessage msg = new FacesMessage(CommonUtil.getMessage("error.numbers.only", null));
				throw new ValidatorException(msg);
			}
			if (v.length() != ConstantsBase.CCPASS_ALLOWED_VALUE) {
				FacesMessage msg = new FacesMessage(CommonUtil.getMessage("error.numbers.lenght", null));
				throw new ValidatorException(msg);
			}
		}
	}

	public static String navAfterUserOperation() {
		FacesContext context = FacesContext.getCurrentInstance();
		User user = (User)context.getApplication().createValueBinding(Constants.BIND_USER).getValue(context);
		if (user.isRetentionMSelected()) {
			return Constants.NAV_RETENTION_MANAGER_TABS;
		} else if (user.isRetentionSelected()) {
			return Constants.NAV_RETENTION_TABS;
		} else {
			return Constants.NAV_EMPTY_SUBTABS;
		}
	}

	public static String getAOHomePageUrlHttps() {
		return getProperty("anyoption.home.page.url.https");
	}

	public static String getHomePageUrlHttps(long skinId) {
		String key = ".home.page.url.https";
		if (skinId == Skin.SKIN_ETRADER) {
			key = "etrader" + key;
		} else if (skinId == Skin.SKIN_REG_IT) {
			key = "anyoption.it" + key;
		} else if (skinId == Skin.SKIN_CHINESE_VIP) {
			key = "vip168" + key;
		} else {
			key = "anyoption" + key;
		}
		return getProperty(key);
	}

    /**
     * check if the user is from israeli skin
     * @return return true if he his from skin 1 or 11
     */
    public boolean isHebrewSkin() {
        FacesContext context = FacesContext.getCurrentInstance();
        User user = (User)context.getApplication().createValueBinding(Constants.BIND_USER).getValue(context);
        return isHebrewSkin(user.getSkinId());
    }

	public static void validateCity(FacesContext context, UIComponent comp, Object value) throws Exception {
		User user = (User)context.getApplication().createValueBinding(ConstantsBase.BIND_USER).getValue(context);
		Locale userLocale = user.getValidatorLocale();
		String pattern = CommonUtil.getMessage("general.validate.city", null, userLocale);
		String v = (String) value;
		if (!v.matches(pattern) || v.trim().equals("")) {
			String[] params = getLablePropertyFromComponent(comp);
			FacesMessage msg = new FacesMessage(CommonUtil.getMessage("error.letters.only", params));
			throw new ValidatorException(msg);
		}
	}
	
	public static Boolean isWriterComaRetention(long writerId) {
		if (getWriter().getSalesReps().get(writerId).getSalesTypeDepartmentId() == Constants.SALES_TYPE_DEPARTMENT_COMA_RETENTION) {
			return true;
		}
		return false;
	}
	
	public static Boolean isWriterInitialStageRetention(long writerId) {
		if (getWriter().getSalesReps().get(writerId).getSalesTypeDepartmentId() == Constants.SALES_TYPE_DEPARTMENT_GENERAL_RETENTION || 
				getWriter().getSalesReps().get(writerId).getSalesTypeDepartmentId() == Constants.SALES_TYPE_DEPARTMENT_UPGRADE_RETENTION) {
			return true;
		}
		return false;
	}
	
	/**
	 * Get percent after dividing.
	 * @param numerator
	 * @param denominator
	 * @return percent after dividing as DecimalFormat
	 */
	public static String getPercentAfterDevided(double numerator, double denominator) {
		DecimalFormat sd = new DecimalFormat("#0.00%");
		if (denominator > 0) {
			return (sd.format(numerator/denominator));
		} else if (numerator > 0 && denominator == 0) {
			return (CommonUtil.getMessage("no.deposit.only.withdrawals", null));
		} else {
			return (ConstantsBase.EMPTY_STRING);
		}
	}
	
	/**
	 * Convert amount from one currency to an amount in base currency
	 * 
	 * @param amount - the amount to convert from
	 * @param showCurrency - display currency in the return string
	 * @param currencyId - the currency to convert from
	 * @return the new amount
	 */
	public static String displayBaseAmount(long amount, boolean showCurrency, long currencyId) {
		Double baseAmount = CommonUtil.convertToBaseAmount(amount, currencyId, new Date());
		return CommonUtil.displayAmount(baseAmount, true, Currency.CURRENCY_USD_ID);
	}
	
	public String getCCExpDateTxt(com.anyoption.common.beans.File file) {
		String date = "";
		if (file.getCcId() > 0) {
			try {
				date = CreditCardsManagerBase.getById(file.getCcId()).getExpDateStr();
			} catch (SQLException sqle) {
				return "";
			}
		}
		return date;
	}
	
	/**
     * Generate excel file.
     * The list of objects fill the content of the file.
     * The hash map contain:
     * key - column's title
     * value - name of the function 
     * 
     * @param fileName as String
     * @param sheetName as String
     * @param hmTitleToFunction as HashMap<String, String>
     * @param ObjList as ArrayList<?>
     * @return File[]
     */
    public static File[] generateListToXls(String fileName, String sheetName, HashMap<String, String> hmTitleToFunction, ArrayList<?> ObjList, String timeZone) {
    	log.info("Generate xls file by list - start.");
    	/* Initial all parameters */
    	HSSFWorkbook xlsWorkbook = new HSSFWorkbook();
		HSSFSheet xlsSheet = xlsWorkbook.createSheet(sheetName);;
		int rowCounter = 0;
		HSSFRow xlsRow = xlsSheet.createRow(rowCounter++);;
		int cellCounter = 0;
		HSSFCell xlsCell = null;
		Iterator it = hmTitleToFunction.entrySet().iterator();
		String filePath = CommonUtil.getProperty(ConstantsBase.FILES_PATH) + fileName;
		
		/* Column's titles */
		while (it.hasNext()) {
	        Map.Entry pairs = (Map.Entry)it.next();
	        String columnTitle = (String) pairs.getKey();
	        xlsCell = xlsRow.createCell(cellCounter++);
			xlsCell.setCellValue(new HSSFRichTextString(columnTitle));
		}
		
		/* Column's info */
		for (Object obj : ObjList) {
			cellCounter = 0;
			xlsRow = xlsSheet.createRow(rowCounter++);
			HSSFCellStyle cellStyle = xlsWorkbook.createCellStyle();
			cellStyle.setWrapText(true);
			it = hmTitleToFunction.entrySet().iterator();
			while (it.hasNext()) {
		        Map.Entry pairs = (Map.Entry)it.next();
		        String methodName = pairs.getValue().toString();
		        /* Reflection to method */
		        String info = null;
		        Method m;
		        try {
		        	m = obj.getClass().getMethod(methodName);
		        	Object val = m.invoke(obj); //reflection
		        	// checks if MethodReflection annotation is present for the method
		        	if (m.isAnnotationPresent(MethodInfoAnnotations.class)) {
		        		MethodInfoAnnotations methodAnnotation = m.getAnnotation(MethodInfoAnnotations.class);
		        		if (methodAnnotation.type().equalsIgnoreCase(MethodInfoAnnotations.TYPE_DATE)) {
		        			Date date = new Date((long) val);
		        			info = CommonUtil.getDateAndTimeFormat(date, timeZone);
		        		} else if (methodAnnotation.formatType() == MethodInfoAnnotations.FORMAT_TYPE_FROM_USD_SMALL_AMOUNT) {
		        			info = CommonUtil.displayAmount((long) val, true, ConstantsBase.CURRENCY_USD_ID, false);
		        		} else if (methodAnnotation.isMsgKey() == true) {
		        			info = CommonUtil.getMessage(new Locale(ConstantsBase.LOCALE_DEFAULT), val.toString(), null);
		        		}
		        	} else if (val instanceof Date) {
		        		info = CommonUtil.getDateAndTimeFormat((Date) val, timeZone);
		        	} else {
		        		info = val.toString();
		        	}
				} catch (Exception e) {
					log.fatal("Can't reflection to method: " + methodName, e);
				}
		        xlsCell = xlsRow.createCell(cellCounter++);
				xlsCell.setCellValue(info);       
		    }
		}
		if (xlsWorkbook != null) {
			log.info("writing to excel.");
			writeToExcel(xlsWorkbook, filePath);
		}
        File attachment = new File(filePath);
        File[] attachments = {attachment};
        log.info("Generate xls file by list - end.");
        return attachments;
    }
    
	public static void writeToExcel(HSSFWorkbook xlsWorkbook, String fileName){
		FileOutputStream fos = null;
		try {
            fos = new FileOutputStream(new File(fileName));
            xlsWorkbook.write(fos);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (fos != null) {
                try {
                    fos.flush();
                    fos.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
	}

}