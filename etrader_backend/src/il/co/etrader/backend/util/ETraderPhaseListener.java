package il.co.etrader.backend.util;

import il.co.etrader.backend.bl_managers.AdminManager;
import il.co.etrader.backend.bl_managers.ApplicationData;
import il.co.etrader.backend.bl_managers.BonusManager;
import il.co.etrader.backend.bl_managers.TraderManager;
import il.co.etrader.backend.bl_managers.UsersManager;
import il.co.etrader.backend.bl_vos.PopulationEntry;
import il.co.etrader.backend.bl_vos.User;
import il.co.etrader.backend.bl_vos.WriterWrapper;
import il.co.etrader.backend.mbeans.LoginForm;
import il.co.etrader.backend.mbeans.PopulationsEntriesForm;
import il.co.etrader.backend.mbeans.SalesDepositsTVForm;
import il.co.etrader.backend.mbeans.TraderToolActionForm;
import il.co.etrader.bl_managers.ApplicationDataBase;
import il.co.etrader.bl_managers.SkinsManagerBase;
import il.co.etrader.bl_managers.TiersManagerBase;
import il.co.etrader.bl_vos.Skins;
import il.co.etrader.bl_vos.Writer;
import il.co.etrader.bonus.BonusMessageHandler;
import il.co.etrader.util.CommonUtil;
import il.co.etrader.util.ConstantsBase;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Map;

import javax.el.ValueExpression;
import javax.faces.context.FacesContext;
import javax.faces.event.PhaseEvent;
import javax.faces.event.PhaseId;
import javax.faces.event.PhaseListener;
import javax.faces.model.SelectItem;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import com.anyoption.common.beans.Bonus;
import com.anyoption.common.beans.SkinCurrency;
import com.anyoption.common.beans.SkinLanguage;
import com.anyoption.common.beans.base.CurrenciesRules;
import com.anyoption.common.bl_vos.BonusCurrency;
import com.anyoption.common.bonus.BonusHandlerBase;
import com.anyoption.common.bonus.BonusHandlerFactory;
import com.anyoption.common.managers.CurrenciesRulesManagerBase;

public class ETraderPhaseListener implements PhaseListener {
	private static final Logger log = Logger.getLogger(ETraderPhaseListener.class);

	private static final String RESPONSE_XML_TAG = "<ResponseXml>";
	private static final String PHONE_CODE_TAG = "<phoneCode>";
	private static final String CURRENCIES_SELECT_TAG = "<currenciesSelect>";
	private static final String LANGUAGES_SELECT_TAG = "<languagesSelect>";
	private static final String CURRENCY_ID_TAG = "<currencyId>";

	private static final String RESPONSE_XML_CLOSING_TAG = "</ResponseXml>";
	private static final String PHONE_CODE_CLOSING_TAG = "</phoneCode>";
	private static final String CURRENCIES_SELECT_CLOSING_TAG = "</currenciesSelect>";
	private static final String LANGUAGES_SELECT_CLOSING_TAG = "</languagesSelect>";
	private static final String CURRENCY_ID_CLOSING_TAG = "</currencyId>";


	public PhaseId getPhaseId() {
		return PhaseId.ANY_PHASE;
	}

	public void beforePhase(PhaseEvent event) {
		String useHttps = "false";

		useHttps = CommonUtil.getProperty("use.https");

		if (useHttps.equals("true")) {

			FacesContext context = event.getFacesContext();

			if (event.getPhaseId() == PhaseId.RESTORE_VIEW) {

				HttpServletRequest request = (HttpServletRequest) context.getExternalContext().getRequest();
				HttpServletResponse response = (HttpServletResponse) context.getExternalContext().getResponse();

				String quer = request.getQueryString();
				if (quer == null) {
					quer = "";
				}
				String view = request.getRequestURL() + "?" + quer;

				int usingPort = 0;
				Enumeration e = request.getHeaderNames();
				while (e.hasMoreElements())
				{
					String header = (String)e.nextElement();
					log.debug(header + ": " + request.getHeader(header));
					if (header.equalsIgnoreCase("port")) {
						usingPort=Integer.parseInt(request.getHeader(header).trim());
						break;
					}
				}

				if (view.indexOf("tradertool.jsf") > -1 ||
				        view.indexOf("tradertoolBinary0100.jsf") > -1 ||
						view.indexOf("ajax.jsf?marketGroupId") > -1 ||
						view.indexOf("heapSize_check.jsf") > -1) {
	                   return;
				}

				if (view.indexOf("https") > -1 || usingPort == Constants.PORT_HTTPS){
					return;
				}

				StringBuffer url = request.getRequestURL();
				log.debug("url before replace " + url.toString());

				url.replace(0, "http".length(), "https");
				log.debug("url after replace " + url.toString());

				String query = "";
				if (request.getQueryString() != null) {
					query = "?" + request.getQueryString();
					String j_username = request.getParameter("j_username");
					if (j_username != null) {
						query += "&j_username=" + j_username;
					}
				}

				try {
					log.debug("redirecting to: " + url.toString() + query);
					response.sendRedirect(url.toString() + query);
					context.responseComplete();
					return;

				} catch (IOException io) {
					log.error("can't redirect to http/https!!! " + url, io);
				}

			}
		}

	}

	public void afterPhase(PhaseEvent event) {

		FacesContext context = event.getFacesContext();
        HttpSession session = (HttpSession) context.getExternalContext().getSession(true);
		Map sm = context.getExternalContext().getSessionMap();
		Map pm = context.getExternalContext().getRequestParameterMap();
		ApplicationData ap = (ApplicationData)context.getApplication().createValueBinding(Constants.BIND_APPLICATION_DATA).getValue(context);

		if (event.getPhaseId() == PhaseId.RESTORE_VIEW) {

			// handle market group filter change in trader tool
			if (null != pm.get("marketGroupId")) {
				log.debug("handle market Group change from TT page");
				HttpServletResponse response = (HttpServletResponse) context.getExternalContext().getResponse();
				response.setContentType("text/xml");
				response.setCharacterEncoding("utf-8");
				response.setHeader("Cache-Control", "no-cache");

				try {
					StringBuffer sb = new StringBuffer();
					sb.append(RESPONSE_XML_TAG);

					String marketGroupId = pm.get("marketGroupId").toString();

					ArrayList<SelectItem> marketsList = TraderManager.getMarketsListById(marketGroupId);
					String marketslistTxt = CommonUtil.arrayList2StringTags(marketsList, "getValue", "getLabel", ap.getWriterLocale(), "market");
					sb.append(Constants.MARKETS_SELECT_TAG + marketslistTxt + Constants.MARKETS_SELECT_CLOSING_TAG);
					
					sb.append(RESPONSE_XML_CLOSING_TAG);
					response.getWriter().write(sb.toString());
					log.trace(sb.toString());

				} catch (Exception e) {
					log.log(Level.ERROR,
							"Can't write reply for markets list!!.", e);
				}

				context.responseComplete();
				return;
			}

			// for phone code request from register page
			if (null != pm.get("countryId")) {
				log.debug("handle country id from registration page");
				HttpServletResponse response = (HttpServletResponse) context
						.getExternalContext().getResponse();
				response.setContentType("text/xml");
				response.setCharacterEncoding("utf-8");
				response.setHeader("Cache-Control", "no-cache");

				try {
					StringBuffer sb = new StringBuffer();
					sb.append(RESPONSE_XML_TAG);

					//handle country phone codes

					String out = ap.getCountryPhoneCodes((String) pm.get("countryId"));
					sb.append(PHONE_CODE_TAG + out + PHONE_CODE_CLOSING_TAG);

					long countryId = new Long((String)pm.get("countryId"));
					Skins skin = SkinsManagerBase.getNewUserSkin(countryId);

					//set the 'session scope user' skin
					//Utils.getUser().setSkinId(skin.getId());

					//handle currency select box
					ArrayList<SkinCurrency> currList = UsersManager.getCurrenciesBySkin(skin.getId());
					String currenciesSt = CommonUtil.arrayList2String(currList, "getCurrencyId", "getDisplayName", ap.getWriterLocale());
					sb.append(CURRENCIES_SELECT_TAG + currenciesSt + CURRENCIES_SELECT_CLOSING_TAG);

					//handle language select box
					ArrayList<SkinLanguage> langList = UsersManager.getLangueagesBySkin(skin.getId());
					String languagesSt = CommonUtil.arrayList2String(langList, "getLanguageId", "getDisplayName", ap.getWriterLocale());
					sb.append(LANGUAGES_SELECT_TAG + languagesSt + LANGUAGES_SELECT_CLOSING_TAG);
					
					//handle default currency
					CurrenciesRules currenciesRules = new CurrenciesRules();
					currenciesRules.setCountryId(Long.valueOf(pm.get("countryId").toString()));
					if (pm.get("crSkinId") != null && pm.get("crSkinId").toString().length() > 0) {
						currenciesRules.setSkinId(Long.valueOf(pm.get("crSkinId").toString()));
					}
					long currencyid = CurrenciesRulesManagerBase.getDefaultCurrencyId(currenciesRules);
					sb.append(CURRENCY_ID_TAG + currencyid + CURRENCY_ID_CLOSING_TAG);


					sb.append(RESPONSE_XML_CLOSING_TAG);
					response.getWriter().write(sb.toString());


				} catch (Exception e) {
					log.log(Level.ERROR,
							"Can't write reply for phoneCode request!!.", e);
				}
				context.responseComplete();
			}

			// for handling skin change request from register page
			if (null != pm.get("skinId")) {
				log.debug("handle s id from registration page");
				HttpServletResponse response = (HttpServletResponse) context
						.getExternalContext().getResponse();
				response.setContentType("text/xml");
				response.setCharacterEncoding("utf-8");
				response.setHeader("Cache-Control", "no-cache");

				try {
					StringBuffer sb = new StringBuffer();
					sb.append(RESPONSE_XML_TAG);

					//handle country phone codes
					Skins skin = ApplicationDataBase.getSkinById((Integer.parseInt((String)pm.get("skinId"))));

					//set the 'session scope user' skin
					Utils.getUser().setSkinId(skin.getId());

					//handle currency select box
					ArrayList<SkinCurrency> currList = UsersManager.getCurrenciesBySkin(skin.getId());
					String currenciesSt = CommonUtil.arrayList2String(currList, "getCurrencyId", "getDisplayName", ap.getWriterLocale());
					sb.append(CURRENCIES_SELECT_TAG + currenciesSt + CURRENCIES_SELECT_CLOSING_TAG);

					//handle language select box
					ArrayList<SkinLanguage> langList = UsersManager.getLangueagesBySkin(skin.getId());
					String languagesSt = CommonUtil.arrayList2String(langList, "getLanguageId", "getDisplayName", ap.getWriterLocale());
					sb.append(LANGUAGES_SELECT_TAG + languagesSt + LANGUAGES_SELECT_CLOSING_TAG);

					String out =  String.valueOf(UsersManager.getLimitId(skin.getId(), currList.get(0).getCurrencyId()));
					sb.append("<limitId>"+out+"</limitId>");
					
					//handle default currency
					CurrenciesRules currenciesRules = new CurrenciesRules();
					if (pm.get("crCountryId") != null && pm.get("crCountryId").toString().length() > 0) {
						currenciesRules.setCountryId(Long.valueOf(pm.get("crCountryId").toString()));
					}
					currenciesRules.setSkinId(Long.valueOf(pm.get("skinId").toString()));					
					long currencyid = CurrenciesRulesManagerBase.getDefaultCurrencyId(currenciesRules);
					sb.append(CURRENCY_ID_TAG + currencyid + CURRENCY_ID_CLOSING_TAG);


					sb.append(RESPONSE_XML_CLOSING_TAG);
					response.getWriter().write(sb.toString());


				} catch (Exception e) {
					log.log(Level.ERROR,
							"Can't write reply for registration page request!!.", e);
				}
				context.responseComplete();
			}

			// for currency code request from register page
			if (null != pm.get("currencyId")) {
				log.debug("handle currency id from change user details page");
				HttpServletResponse response = (HttpServletResponse) context
						.getExternalContext().getResponse();
				response.setContentType("text/xml");
				response.setCharacterEncoding("utf-8");
				response.setHeader("Cache-Control", "no-cache");

				try {
					long skinId = Utils.getUser().getSkinId();
					String currencyId = (String)pm.get("currencyId");
					String out =  String.valueOf(UsersManager.getLimitId(skinId, Long.valueOf(currencyId)));
					response.getWriter().write("<limitId>" + out + "</limitId>");
				} catch (Exception e) {
					log.log(Level.ERROR,
							"Can't write reply for currencyId request!!.", e);
				}
				context.responseComplete();
			}

			if(null != pm.get("bonusId") && pm.get("bonusId") != ""){
					log.debug("handle bonus id from change bonuses page");
					HttpServletResponse response = (HttpServletResponse) context
							.getExternalContext().getResponse();
					response.setContentType("text/xml");
					response.setCharacterEncoding("utf-8");
					response.setHeader("Cache-Control", "no-cache");

					String out = "";
					try {
						String bonusIdStr = (String)pm.get("bonusId");
						long bonusId = Long.parseLong(bonusIdStr);
						String currencyId = (String)pm.get("bonusCurrencyId");
						Bonus b = BonusManager.getBonusById(bonusId);
						BonusCurrency bc = BonusManager.getBonusCurrency(bonusId, Long.parseLong(currencyId));
						BonusHandlerBase bh = BonusHandlerFactory.getInstance(b.getTypeId());
						long popTypeId = 0;
						long popEnteyId = 0;
						int bonusPopLimitTypeId = 0;
						long userId = 0;

						out = "Bonus will be granted for default period of "+b.getDefaultPeriod()+" days <br/>";

						// Population limits
						if (b.isCheckLimit()){

							User user = (User)context.getApplication().createValueBinding(Constants.BIND_USER).getValue(context);
							if (user.isRetentionSelected() || user.isRetentionMSelected() || user.isSupportSelectedWithMOLocking()) {
								userId = user.getId();
								PopulationEntry entry = (PopulationEntry) context.getApplication().createValueBinding(Constants.BIND_POPULATION_ENTRY).getValue(context);
								if (null != entry){
									popTypeId = entry.getCurrPopualtionTypeId();

									if (popTypeId != 0){
										popEnteyId = entry.getCurrEntryId();
								    	WriterWrapper wr = Utils.getWriter();
								    	bonusPopLimitTypeId = wr.getBonusPopLimitType(popTypeId);
									}else{
										log.warn("Problem getting popTypeId. userId: " + userId);
									}
								}else{
									log.warn("Problem getting PopulationEntry for check bonus pop limits ");
								}
							}
						}

						if (null != bh){
							out+= BonusMessageHandler.getBonusMessage(b, bc, popEnteyId, bonusPopLimitTypeId, userId, 0, 0, 0);
						}

						response.getWriter().write("<desc><![CDATA[" + out + "]]><sumdeposit>"+ bc.getSumDeposits() + "</sumdeposit></desc>");
					} catch (Exception e) {
						log.log(Level.ERROR,
								"Can't write reply for bonus description request. bonusId="+(String)pm.get("bonusId"), e);
					}
					context.responseComplete();
			}

			if ( null != pm.get("pointsToConvert")) {
				log.debug("handle calculate amount by Loyalty points");
				HttpServletResponse response = (HttpServletResponse) context
						.getExternalContext().getResponse();
				response.setContentType("text/xml");
				response.setCharacterEncoding("utf-8");
				response.setHeader("Cache-Control", "no-cache");
				String out = "";
				try {
					User u = (User)context.getApplication().createValueBinding(Constants.BIND_USER).getValue(context);
					long points = Long.parseLong((String)pm.get("pointsToConvert"));
					long tierId = u.getTierUser().getTierId();
					double amount = TiersManagerBase.convertPointsToCash(points, tierId);
					out = CommonUtil.displayAmount(CommonUtil.calcAmount(amount), true, u.getCurrencyId());
					response.getWriter().write("<convertedAmount>" + out + "</convertedAmount>");
				} catch (Exception e) {
					log.log(Level.ERROR, "Can't write reply for amount by points request!!.", e);
				}
				context.responseComplete();
			}


			if ( null != pm.get("assignRequest")) {  // handel assign writer requests
				String assignReq = (String)pm.get("assignRequest");
				String out = "";
				HttpServletResponse response = (HttpServletResponse) context
				.getExternalContext().getResponse();
				response.setContentType("text/xml");
				response.setCharacterEncoding("utf-8");
				response.setHeader("Cache-Control", "no-cache");
				String checked =(String)pm.get("assignVal");
				boolean checkedB = checked.equals("true") ? true : false;
				try {
					String beanName = (String)pm.get("beanName");
					String beanKey = "#{" + beanName + "}";
					String className = Character.toUpperCase(beanName.charAt(0)) + beanName.substring(1);
					Class<?> beanClass = Class.forName("il.co.etrader.backend.mbeans." + className);
					if("true".equalsIgnoreCase((String) pm.get("Ret"))){
						beanKey = "#{populationsEntriesFormRetention}";
					}
				    ValueExpression ve = context.getApplication().getExpressionFactory().createValueExpression(context.getELContext(), beanKey, beanClass);
					PopulationsEntriesForm aForm = (PopulationsEntriesForm) ve.getValue(context.getELContext());
					if (assignReq.equals("assignAll")) {
						aForm.setAssignToAllSkins(checkedB);
						out = "ok";
					} else if (assignReq.equals("assignSkinAll")) {
						long skinId = Long.parseLong((String)pm.get("assignSkinId"));
						aForm.setAssignForSkin(checkedB, skinId);
						out = String.valueOf(skinId);
					} else if (assignReq.equals("assignWriter")) {
						long skinId = Long.parseLong((String)pm.get("assignSkinId"));
						long writerId = Long.parseLong((String)pm.get("assignWriterId"));
						aForm.setAssignForSkinAndWriter(checkedB, skinId, writerId);
						out = String.valueOf(skinId + "," + writerId);
					}
					ve.setValue(context.getELContext(), aForm);
					response.getWriter().write("<assignRes>" + out + "</assignRes>");
				} catch (Exception e) {
					log.log(Level.ERROR, "Can't write reply for assign writers request, req: " + assignReq, e);
				}
				context.responseComplete();
			}

			// handle TV updates
			if (null != pm.get("TVUpdate")) {
				String page = (String)pm.get("TVUpdate");
				String out = "";
				HttpServletResponse response = (HttpServletResponse) context.getExternalContext().getResponse();
				response.setContentType("text/xml");
				response.setCharacterEncoding("utf-8");
				response.setHeader("Cache-Control", "no-cache");
				try {
					// Get new Sales deposits from db is exists
					ApplicationData.getNewSalesDepositsFromDb();
					ValueExpression ve = context.getApplication().getExpressionFactory().createValueExpression(context.getELContext(), Constants.BIND_SALES_DEPOSITS_TV_FORM, SalesDepositsTVForm.class);
					SalesDepositsTVForm form = (SalesDepositsTVForm) ve.getValue(context.getELContext());
					if (page.equals("second")) {
						if (!ApplicationData.isGotUpdateForWriterDepositTV2()) {
							form.setViewAfterOnlineDeposit(-1);
							form.setRepName(null);
							form.setRepDepositAmount(null);
							form.setRepDepositType(0);
							out = "no";
						} else {
							ArrayList<Writer> list = ApplicationData.getDepositedWritersTV2();
							if (null != list && list.size() > 0) {
								Writer w = list.get(0);
								log.info("OnlineUpdate for secondTv, writerId: " +w.getId());
								//form.setViewAfterOnlineDeposit(Constants.DAILY_DEPOSITS);
								form.setRepName(w.getUserName().toLowerCase());
								form.setRepDepositAmount(w.getOnlineDepositAmount());
								form.setRepDepositType(w.getOnlineDepositType());
								out = "yes";
								ve.setValue(context.getELContext(), form);
								//	remove writer from deposits list
								ApplicationData.removeWriterAfterHandleTV2();
							} else {
								form.setViewAfterOnlineDeposit(-1);
								form.setRepName(null);
								form.setRepDepositAmount(null);
								form.setRepDepositType(0);
								out = "no";
							}
						}
					} else if (page.equals("daily")) {
						if (!ApplicationData.isGotUpdateForWriterDepositTV1()) {
							form.setViewAfterOnlineDeposit(-1);
							form.setRepName(null);
							form.setRepDepositAmount(null);
							form.setRepDepositType(0);
							out = "no";
						} else {
							ArrayList<Writer> list = ApplicationData.getDepositedWritersTV1();
							if (null != list && list.size() > 0) {
								Writer w = list.get(0);
								log.info("OnlineUpdate for dailyAllSkins , writerId: " +w.getId());
								out = "yes";
								form.setRepName(w.getUserName().toLowerCase());
								form.setRepDepositAmount(w.getOnlineDepositAmount());
								form.setRepDepositType(w.getOnlineDepositType());
								ve.setValue(context.getELContext(), form);
								// remove writer from deposits list
								ApplicationData.removeWriterAfterHandleTV1();
							} else {
								form.setViewAfterOnlineDeposit(-1);
								form.setRepName(null);
								form.setRepDepositAmount(null);
								form.setRepDepositType(0);
								out = "no";
							}
						}
					} else {
						log.info("Ajax TV call without page param");
						form.setViewAfterOnlineDeposit(-1);
						form.setRepName(null);
						form.setRepDepositAmount(null);
						ve.setValue(context.getELContext(), form);
					}
					response.getWriter().write("<tvRes>" + out + "</tvRes>");
				} catch (Exception e) {
					log.log(Level.ERROR, "Can't write reply for online deposit request, req: " + e);
				}
				context.responseComplete();
			}

			/**
			 * Ajax request that don't do nothing! just response complete for solving problems
			 * with window open & jsf
			 */
			if ( null != pm.get("renderResponseCall")) {
				log.trace("response complete request");
				HttpServletResponse response = (HttpServletResponse) context
						.getExternalContext().getResponse();
				response.setContentType("text/xml");
				response.setCharacterEncoding("utf-8");
				response.setHeader("Cache-Control", "no-cache");
				try {
					response.getWriter().write("<rrc> ok </rrc>");
				} catch (Exception e) {
					log.log(Level.ERROR, "Can't write reply for response complete request!!.", e);
				}
				context.responseComplete();
			}
			if(null != pm.get(ConstantsBase.PROMOTION_TOTAL_AND_INSERTED_USERS)){
				log.debug("request for sending sms/email");
				String out = (String) session.getAttribute(Constants.PROMOTION_TOTAL_AND_INSERTED_USERS);
				System.out.println(out);
				HttpServletResponse response = (HttpServletResponse)context.getExternalContext().getResponse();
				response.setContentType("text/xml");
				response.setCharacterEncoding("utf-8");
				response.setHeader("Cache-Control", "no-cache");
				try {
					response.getWriter().write("<total>" + out + "</total>");
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				context.responseComplete();
			}
			if(null != pm.get(ConstantsBase.PROMOTION_TOTAL_SENDED_TO_USERS)){
				log.debug("sending sms/email to clients");
				String out = (String) session.getAttribute(Constants.PROMOTION_TOTAL_SENDED_TO_USERS);
				System.out.println(out);
				HttpServletResponse response = (HttpServletResponse)context.getExternalContext().getResponse();
				response.setContentType("text/xml");
				response.setCharacterEncoding("utf-8");
				response.setHeader("Cache-Control", "no-cache");
				try {
					response.getWriter().write("<totalSended>" + out + "</totalSended>");
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				context.responseComplete();
			}
			if(null != pm.get("changeTParam")){
//				boolean reply = false;
				double changeToT = Double.valueOf(pm.get("changeTParam").toString());
//				BinaryZeroOneHundred binaryZeroOneHundred = new BinaryZeroOneHundred(pm.get("params").toString());
//				BinaryZeroOneHundred binaryZeroOneHundredOld = new BinaryZeroOneHundred(pm.get("params").toString());
				long oppId = Long.valueOf(pm.get("oppId").toString());
				long marketId = Long.valueOf(pm.get("marketId").toString());
//				binaryZeroOneHundred.setT(binaryZeroOneHundred.getT() + changeToT);
//				log.debug("t= " + changeToT + " params = " + binaryZeroOneHundred.toString() + " oppId = " + oppId + " marketId = " + marketId);
//				try {
//					reply = TraderToolActionForm.updateTparamAjax(binaryZeroOneHundred, oppId, marketId, Utils.getWriter(), binaryZeroOneHundredOld);
//				} catch (SQLException e) {
//					log.debug("cant change T param");
//				}
				boolean reply = TraderToolActionForm.updateDynamicsTParam(oppId, changeToT, marketId, Utils.getWriter());
				HttpServletResponse response = (HttpServletResponse) context.getExternalContext().getResponse();
				response.setContentType("text/xml");
				response.setCharacterEncoding("utf-8");
				response.setHeader("Cache-Control", "no-cache");
				try {
					response.getWriter().write("<response>" + reply + "</response>");
				} catch (IOException e) {
					// do nothing
				}
				context.responseComplete();
			}
			if (null != pm.get("autoLogin")) {
				boolean isAutoLogin = false;
				try {
					HttpServletRequest request = (HttpServletRequest) context.getExternalContext().getRequest();
			  		WriterWrapper sessionWriterWrapper = (WriterWrapper) request.getSession().getAttribute(Constants.BIND_SESSION_WRITER_WRAPPER);
					if (sessionWriterWrapper == null) {
						log.debug("Writer not found in session.");
						Cookie cookieWriterName = CommonUtil.getCookie(request, Constants.COOKIE_WRITER_NAME);
						if (null != cookieWriterName) {
							String writerName = cookieWriterName.getValue();
							log.debug("Found cookie writer name with value:" + writerName);
							Writer writer = AdminManager.getWriterByUserName(writerName);
							if (writer != null) {
								log.debug("autoLogin writer was found by userName:" + writerName);
								LoginForm loginForm = new LoginForm();
								loginForm.setUsername(writer.getUserName());
								loginForm.setPassword(writer.getPassword());
								loginForm.login();
								isAutoLogin = true;
							}
						}
					}
					HttpServletResponse response = (HttpServletResponse)context.getExternalContext().getResponse();
					response.setContentType("text/xml");
					response.setCharacterEncoding("utf-8");
					response.setHeader("Cache-Control", "no-cache");
					response.getWriter().write("<isAutoLogin>" + String.valueOf(isAutoLogin) + "</isAutoLogin>");
				} catch (Exception e) {
					log.error("Error while trying to do auto login", e);
				}
				context.responseComplete();
			}
			
		}
	}
}