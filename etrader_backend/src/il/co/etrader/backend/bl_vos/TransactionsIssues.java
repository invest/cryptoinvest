package il.co.etrader.backend.bl_vos;



import java.sql.SQLException;
import java.util.Date;

import com.anyoption.common.beans.IssueAction;
import com.anyoption.common.beans.Transaction;

public class TransactionsIssues implements java.io.Serializable{

	private static final long serialVersionUID = -9023094246932323640L;

	private long transactionId;
	private long issueActionId;
	private long cycleReached;
	private long lastInvestmentId;
	private Date timeSettled;
	private long lastInvestmentRemainder;
	private long callersNum;
	private Date timeInserted;
	private int insertType;
	private long writerId;
	private String writerName;
	private Transaction transaction;
	private IssueAction issueAction;

	public TransactionsIssues(){
		initValues();
	}

	public void initValues(){
		transactionId = 0;
		issueActionId = 0;
		cycleReached = 0;
		lastInvestmentId = 0;
		timeSettled = null;
		lastInvestmentRemainder = 0;
		callersNum = 0;
		timeInserted = null;
		insertType = 0;
		writerId = 0;
		writerName = null;
		transaction = new Transaction();
		issueAction = new IssueAction();
	}


	/**
	 * Constructs a <code>String</code> with all attributes
	 * in name = value format.
	 *
	 * @return a <code>String</code> representation
	 * of this object.
	 */
	public String toString()
	{
	    final String TAB = " \n ";

	    String retValue = "";

	    retValue = "IssueStatus ( "
	        + super.toString() + TAB
	        + "transactionId= " + this.transactionId + TAB
	        + "issueActionId= " + this.issueActionId + TAB
	        + "cycleReached= " + this.cycleReached + TAB
	        + "lastInvestmentId= " + this.lastInvestmentId + TAB
	        + "timeSettled= " + this.timeSettled + TAB
	        + "lastInvestmentRemainder= " + this.lastInvestmentRemainder + TAB
	        + "callersNum= " + this.callersNum + TAB
	        + "timeInserted= " + this.timeInserted + TAB
	        + "insertType= " + this.insertType + TAB
	        + "writerId= " + this.writerId + TAB
	        + "writerName= " + this.writerName + TAB
	        + " )";

	    return retValue;
	}


	/**
	 * @return the callersNum
	 */
	public long getCallersNum() {
		return callersNum;
	}


	/**
	 * @param callersNum the callersNum to set
	 */
	public void setCallersNum(long callersNum) {
		this.callersNum = callersNum;
	}


	/**
	 * @return the cycleReached
	 */
	public long getCycleReached() {
		return cycleReached;
	}


	/**
	 * @param cycleReached the cycleReached to set
	 */
	public void setCycleReached(long cycleReached) {
		this.cycleReached = cycleReached;
	}


	/**
	 * @return the insertType
	 */
	public int getInsertType() {
		return insertType;
	}


	/**
	 * @param insertType the insertType to set
	 */
	public void setInsertType(int insertType) {
		this.insertType = insertType;
	}


	/**
	 * @return the issueActionId
	 */
	public long getIssueActionId() {
		return issueActionId;
	}


	/**
	 * @param issueActionId the issueActionId to set
	 */
	public void setIssueActionId(long issueActionId) {
		this.issueActionId = issueActionId;
	}


	/**
	 * @return the lastInvestmentId
	 */
	public long getLastInvestmentId() {
		return lastInvestmentId;
	}


	/**
	 * @param lastInvestmentId the lastInvestmentId to set
	 */
	public void setLastInvestmentId(long lastInvestmentId) {
		this.lastInvestmentId = lastInvestmentId;
	}


	/**
	 * @return the lastInvestmentRemainder
	 */
	public long getLastInvestmentRemainder() {
		return lastInvestmentRemainder;
	}


	/**
	 * @param lastInvestmentRemainder the lastInvestmentRemainder to set
	 */
	public void setLastInvestmentRemainder(long lastInvestmentRemainder) {
		this.lastInvestmentRemainder = lastInvestmentRemainder;
	}


	/**
	 * @return the timeInserted
	 */
	public Date getTimeInserted() {
		return timeInserted;
	}


	/**
	 * @param timeInserted the timeInserted to set
	 */
	public void setTimeInserted(Date timeInserted) {
		this.timeInserted = timeInserted;
	}


	/**
	 * @return the timeSettled
	 */
	public Date getTimeSettled() {
		return timeSettled;
	}


	/**
	 * @param timeSettled the timeSettled to set
	 */
	public void setTimeSettled(Date timeSettled) {
		this.timeSettled = timeSettled;
	}


	/**
	 * @return the transactionId
	 */
	public long getTransactionId() {
		return transactionId;
	}


	/**
	 * @param transactionId the transactionId to set
	 */
	public void setTransactionId(long transactionId) {
		this.transactionId = transactionId;
	}


	/**
	 * @return the writerId
	 */
	public long getWriterId() {
		return writerId;
	}


	/**
	 * @param writerId the writerId to set
	 */
	public void setWriterId(long writerId) {
		this.writerId = writerId;
	}


	/**
	 * @return the writerName
	 * @throws SQLException
	 */
	public String getWriterName() throws SQLException {
		return writerName;
	}


	/**
	 * @param writerName the writerName to set
	 */
	public void setWriterName(String writerName) {
		this.writerName = writerName;
	}



	/**
	 * Format the insert type of the transaction issue
	 * @return
	 */
	public String getInsertTypeTxt(){
		switch (insertType) {
			case 0:
				 return "Auto";
			case 1:
				 return "Online";
			case 2:
				 return "Manual";
			}
		return null;
	}


	/**
	 * @return the issueAction
	 */
	public IssueAction getIssueAction() {
		return issueAction;
	}


	/**
	 * @param issueAction the issueAction to set
	 */
	public void setIssueAction(IssueAction issueAction) {
		this.issueAction = issueAction;
	}


	/**
	 * @return the transaction
	 */
	public Transaction getTransaction() {
		return transaction;
	}


	/**
	 * @param transaction the transaction to set
	 */
	public void setTransaction(Transaction transaction) {
		this.transaction = transaction;
	}




}
