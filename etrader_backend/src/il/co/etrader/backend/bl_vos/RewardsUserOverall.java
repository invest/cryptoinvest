package il.co.etrader.backend.bl_vos;

import java.io.Serializable;
import java.util.ArrayList;

import com.anyoption.common.beans.RewardUserBenefit;
import com.anyoption.common.beans.RewardUserPlatform;

/**
 * @author EranL
 *
 */
public class RewardsUserOverall implements Serializable {

	private static final long serialVersionUID = 1L;

	private long tierId;
	private double expPntsBalance;
	
	private ArrayList<RewardUserPlatform> rewardUserPlatformList;
	private ArrayList<RewardUserBenefit> rewardUserBenefitList;
	private ArrayList<RewardUserTaskExpPoint> rewardUserTaskExpPoints;
	
	public RewardsUserOverall() {
		rewardUserPlatformList = new ArrayList<RewardUserPlatform>();
		rewardUserBenefitList = new ArrayList<RewardUserBenefit>();
	}
	
	/**
	 * Get reward user benefit list size
	 * @return
	 */
	public int getRewardUserBenefitListSize() {
		return rewardUserBenefitList.size();
	}
	
	/**
	 * @return the tierId
	 */
	public long getTierId() {
		return tierId;
	}
	/**
	 * @param tierId the tierId to set
	 */
	public void setTierId(long tierId) {
		this.tierId = tierId;
	}

	public double getExpPntsBalance() {
		return expPntsBalance;
	}

	public void setExpPntsBalance(double expPntsBalance) {
		this.expPntsBalance = expPntsBalance;
	}

	/**
	 * @return the rewardUserPlatformList
	 */
	public ArrayList<RewardUserPlatform> getRewardUserPlatformList() {
		return rewardUserPlatformList;
	}

	/**
	 * @param rewardUserPlatformList the rewardUserPlatformList to set
	 */
	public void setRewardUserPlatformList(
			ArrayList<RewardUserPlatform> rewardUserPlatformList) {
		this.rewardUserPlatformList = rewardUserPlatformList;
	}

	/**
	 * @return the rewardUserBenefitList
	 */
	public ArrayList<RewardUserBenefit> getRewardUserBenefitList() {
		return rewardUserBenefitList;
	}

	/**
	 * @param rewardUserBenefitList the rewardUserBenefitList to set
	 */
	public void setRewardUserBenefitList(
			ArrayList<RewardUserBenefit> rewardUserBenefitList) {
		this.rewardUserBenefitList = rewardUserBenefitList;
	}

	/**
	 * @return the rewardUserTaskExpPoints
	 */
	public ArrayList<RewardUserTaskExpPoint> getRewardUserTaskExpPoints() {
		return rewardUserTaskExpPoints;
	}

	/**
	 * @param rewardUserTaskExpPoints the rewardUserTaskExpPoints to set
	 */
	public void setRewardUserTaskExpPoints(
			ArrayList<RewardUserTaskExpPoint> rewardUserTaskExpPoints) {
		this.rewardUserTaskExpPoints = rewardUserTaskExpPoints;
	}
	
}