package il.co.etrader.backend.bl_vos;

import java.io.Serializable;

/**
 * PopulationUsersAssignment
 * @author eyalo
 */
public class PopulationUsersAssignment implements Serializable {
	private static final long serialVersionUID = 3676826122438479835L;

	private int numOfLeads;
	private int numOfLeadsUpdated;
	private long assignToRepId;
	
	/**
	 * @return the numOfLeads
	 */
	public int getNumOfLeads() {
		return numOfLeads;
	}
	
	/**
	 * @param numOfLeads the numOfLeads to set
	 */
	public void setNumOfLeads(int numOfLeads) {
		this.numOfLeads = numOfLeads;
	}

	/**
	 * @return the numOfLeadsUpdated
	 */
	public int getNumOfLeadsUpdated() {
		return numOfLeadsUpdated;
	}

	/**
	 * @param numOfLeadsUpdated the numOfLeadsUpdated to set
	 */
	public void setNumOfLeadsUpdated(int numOfLeadsUpdated) {
		this.numOfLeadsUpdated = numOfLeadsUpdated;
	}

	/**
	 * @return the assignToRepId
	 */
	public long getAssignToRepId() {
		return assignToRepId;
	}

	/**
	 * @param assignToRepId the assignToRepId to set
	 */
	public void setAssignToRepId(long assignToRepId) {
		this.assignToRepId = assignToRepId;
	}
}
