package il.co.etrader.backend.bl_vos;

import java.io.Serializable;

/**
 * @author eranl
 *
 */
public class RewardUserTaskExpPoint implements Serializable {
 
	private static final long serialVersionUID = -1974070174967086079L;
	private String taskType;
	private long experiencePointsGained;
	private int rowType;
	
	public static final int ROW_TYPE_VAL = 0;
	public static final int ROW_TYPE_TOTAL = 1;
	
	/**
	 * 
	 */
	public RewardUserTaskExpPoint() {
		this.rowType = ROW_TYPE_VAL;
	}
	
	/**
	 * @param rowType
	 */
	public RewardUserTaskExpPoint(int rowType) {
		this.rowType = rowType;
	}
		
	/**
	 * @return the taskType
	 */
	public String getTaskType() {
		return taskType;
	}
	/**
	 * @param taskType the taskType to set
	 */
	public void setTaskType(String taskType) {
		this.taskType = taskType;
	}
	/**
	 * @return the experiencePointsGained
	 */
	public long getExperiencePointsGained() {
		return experiencePointsGained;
	}
	/**
	 * @param experiencePointsGained the experiencePointsGained to set
	 */
	public void setExperiencePointsGained(long experiencePointsGained) {
		this.experiencePointsGained = experiencePointsGained;
	}
	
	/**
	 * @return the rowType
	 */
	public int getRowType() {
		return rowType;
	}

	/**
	 * @param rowType the rowType to set
	 */
	public void setRowType(int rowType) {
		this.rowType = rowType;
	}

	/**
	 * @return
	 */
	public boolean getIsVal() {
		return ROW_TYPE_VAL == rowType;
	}

	/**
	 * @return
	 */
	public boolean getIsTotal() {
		return ROW_TYPE_TOTAL == rowType;
	}
	
}
