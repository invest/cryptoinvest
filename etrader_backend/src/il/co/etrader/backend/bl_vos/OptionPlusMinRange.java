package il.co.etrader.backend.bl_vos;

import java.io.Serializable;

/**
 * OptionPlusMinRange class.
 *
 * @author Kobi
 */
public class OptionPlusMinRange implements Serializable {
	private static final long serialVersionUID = 1L;

	private long id;
	private long min;
	private long max;

	/**
	 * @return the id
	 */
	public long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(long id) {
		this.id = id;
	}

	/**
	 * @return the max
	 */
	public long getMax() {
		return max;
	}

	/**
	 * @param max the max to set
	 */
	public void setMax(long max) {
		this.max = max;
	}

	/**
	 * @return the min
	 */
	public long getMin() {
		return min;
	}

	/**
	 * @param min the min to set
	 */
	public void setMin(long min) {
		this.min = min;
	}


}
