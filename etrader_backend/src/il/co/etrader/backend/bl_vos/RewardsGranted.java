package il.co.etrader.backend.bl_vos;

import java.io.Serializable;

/**
 * @author EranL
 *
 */
public class RewardsGranted implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private boolean isGranted;
	private long tierId;
	private String reward;
	
	/**
	 * @return the isGranted
	 */
	public boolean isGranted() {
		return isGranted;
	}
	/**
	 * @param isGranted the isGranted to set
	 */
	public void setGranted(boolean isGranted) {
		this.isGranted = isGranted;
	}
	/**
	 * @return the tierId
	 */
	public long getTierId() {
		return tierId;
	}
	/**
	 * @param tierId the tierId to set
	 */
	public void setTierId(long tierId) {
		this.tierId = tierId;
	}
	/**
	 * @return the reward
	 */
	public String getReward() {
		return reward;
	}
	/**
	 * @param reward the reward to set
	 */
	public void setReward(String reward) {
		this.reward = reward;
	}
	
}
