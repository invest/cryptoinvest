package il.co.etrader.backend.bl_vos;

import il.co.etrader.bl_vos.Writer;

import java.util.Date;

public class SalesDepositsInfo {
	private long writerId;
	private Date startDate;
	private Date endDate;
	private long userId;
	private Writer writer;
	private long skinId;
	
	/**
	 * @return the writerId
	 */
	public long getWriterId() {
		return writerId;
	}
	
	/**
	 * @param writerId the writerId to set
	 */
	public void setWriterId(long writerId) {
		this.writerId = writerId;
	}
	
	/**
	 * @return the startDate
	 */
	public Date getStartDate() {
		return startDate;
	}
	
	/**
	 * @param startDate the startDate to set
	 */
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}
	
	/**
	 * @return the endDate
	 */
	public Date getEndDate() {
		return endDate;
	}
	
	/**
	 * @param endDate the endDate to set
	 */
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	/**
	 * @return the userId
	 */
	public long getUserId() {
		return userId;
	}

	/**
	 * @param userId the userId to set
	 */
	public void setUserId(long userId) {
		this.userId = userId;
	}

	/**
	 * @return the writer
	 */
	public Writer getWriter() {
		return writer;
	}

	/**
	 * @param writer the writer to set
	 */
	public void setWriter(Writer writer) {
		this.writer = writer;
	}

	/**
	 * @return the skinId
	 */
	public long getSkinId() {
		return skinId;
	}

	/**
	 * @param skinId the skinId to set
	 */
	public void setSkinId(long skinId) {
		this.skinId = skinId;
	}
}
