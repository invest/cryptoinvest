package il.co.etrader.backend.bl_vos;


import il.co.etrader.backend.util.Constants;
import il.co.etrader.util.CommonUtil;

import java.net.URLEncoder;

import com.anyoption.common.beans.Opportunity;
import com.anyoption.common.enums.SkinGroup;

public class OpportunityTotal extends Opportunity implements java.io.Serializable {


	private static final long serialVersionUID = 1L;
	//sum of investes in money(centes)(put,calls,call-put,call+put)
	private double callsAmount;
	private double putsAmount;
	private double callsAmountSubPutsAmount;
	private double callsAmountAddPutsAmount;
	private double boughtAmountSubSoldAmount;
	private int boughtCountSubSoldCount;
	private boolean canSettle;
	private double boughtAmount;
	private double soldAmount;
	private int boughtCount;
	private int soldCount;

	public OpportunityTotal() {

	}

	/**
	 * @return the callsAmount
	 */
	public double getCallsAmount() {
		return callsAmount;
	}

	/**
	 * @param callsAmount the callsAmount to set
	 */
	public void setCallsAmount(double callsAmount) {
		this.callsAmount = callsAmount;
	}

	/**
	 * @return the callsAmountAddPutsAmount
	 */
	public double getCallsAmountAddPutsAmount() {
		return callsAmountAddPutsAmount;
	}

	/**
	 * @param callsAmountAddPutsAmount the callsAmountAddPutsAmount to set
	 */
	public void setCallsAmountAddPutsAmount(double callsAmountAddPutsAmount) {
		this.callsAmountAddPutsAmount = callsAmountAddPutsAmount;
	}

	/**
	 * @return the callsAmountSubPutsAmount
	 */
	public double getCallsAmountSubPutsAmount() {
		return callsAmountSubPutsAmount;
	}

	/**
	 * @param callsAmountSubPutsAmount the callsAmountSubPutsAmount to set
	 */
	public void setCallsAmountSubPutsAmount(double callsAmountSubPutsAmount) {
		this.callsAmountSubPutsAmount = callsAmountSubPutsAmount;
	}

	/**
	 * @return the putsAmount
	 */
	public double getPutsAmount() {
		return putsAmount;
	}

	/**
	 * @param putsAmount the putsAmount to set
	 */
	public void setPutsAmount(double putsAmount) {
		this.putsAmount = putsAmount;
	}

	/**
	 * @return the callsAmountTxt
	 */
	public String getCallsAmountTxt() {
		return CommonUtil.displayAmount(callsAmount, true, Constants.CURRENCY_BASE_ID);
	}

	/**
	 * @return the bought amount
	 */
	public String getBoughtAmountTxt(){
		return CommonUtil.displayAmount(boughtAmount, true, Constants.CURRENCY_BASE_ID);
	}

	/**
	 *
	 * @return the sold amount
	 */
	public String getSoldAmountTxt(){
		return CommonUtil.displayAmount(soldAmount, true, Constants.CURRENCY_BASE_ID);
	}

	/**
	 * @return the putsAmountTxt
	 */
	public String getPutsAmountTxt() {
		return CommonUtil.displayAmount(putsAmount, true, Constants.CURRENCY_BASE_ID);
	}

	/**
	 * @return the callsAmountSubPutsAmountTxt
	 */
	public String getCallsAmountSubPutsAmountTxt() {
		return CommonUtil.displayAmount(callsAmountSubPutsAmount, true, Constants.CURRENCY_BASE_ID);
	}

	/**
	 * @return the callsAmountAddPutsAmountTxt
	 */
	public String getCallsAmountAddPutsAmountTxt() {
		return CommonUtil.displayAmount(callsAmountAddPutsAmount, true, Constants.CURRENCY_BASE_ID);
	}

	public int getMaxExposure() {
		// TODO Get exposure according to the skin when exposure design is ready
		return getSkinGroupMappings().get(SkinGroup.ETRADER).getMaxExposure();
	}

	/**
	 * @return the maxExposureTxt
	 */
	public String getMaxExposureTxt() {
		// TODO Get exposure according to the skin when exposure design is ready
		return CommonUtil.displayDecimal(getSkinGroupMappings().get(SkinGroup.ETRADER).getMaxExposure());
	}

	public String getEncodingMarketName() {
		try {
			return URLEncoder.encode(CommonUtil.getMarketName(this.getMarketId()),"UTF-8");
		} catch (Exception e) {
			//TODO add loger
		}
		//if cant encode return market name as is
		return CommonUtil.getMarketName(this.getMarketId());
	}

	public String getEncodingFeedName() {
		try {
			return URLEncoder.encode(this.getMarket().getFeedName(),"UTF-8");
		} catch (Exception e) {
			//TODO add loger
		}
		//if cant encode return market name as is
		return this.getMarket().getFeedName();
	}

	/**
	 * @return the canSettle
	 */
	public boolean isCanSettle() {
		return canSettle;
	}

	/**
	 * @param canSettle the canSettle to set
	 */
	public void setCanSettle(boolean canSettle) {
		this.canSettle = canSettle;
	}

	public double getShiftParameter() {
		return getSkinGroupMappings().get(SkinGroup.ETRADER).getShiftParameter();
	}
	
    public double getShiftParameterMultiplyBy100() {
        return getSkinGroupMappings().get(SkinGroup.ETRADER).getShiftParameter() * 100;
    }

    public double getShiftParameterAOMultiplyBy100() {
    	return getSkinGroupMappings().get(SkinGroup.ANYOPTION).getShiftParameter() * 100;
    }

    public double getAutoShiftParameterMultiplyBy100() {
        return getAutoShiftParameter()*100;
    }

	public double getBoughtAmount() {
		return boughtAmount;
	}

	public double getBoughtAmountSubSoldAmount() {
		return boughtAmountSubSoldAmount;
	}

	public void setBoughtAmountSubSoldAmount(double boughtAmountSubSoldAmount) {
		this.boughtAmountSubSoldAmount = boughtAmountSubSoldAmount;
	}

	public void setBoughtAmount(double boughtAmount) {
		this.boughtAmount = boughtAmount;
	}

	public double getSoldAmount() {
		return soldAmount;
	}

	public void setSoldAmount(double soldAmount) {
		this.soldAmount = soldAmount;
	}

	public int getBoughtCount() {
		return boughtCount;
	}

	public void setBoughtCount(int boughtCount) {
		this.boughtCount = boughtCount;
	}

	public int getSoldCount() {
		return soldCount;
	}

	public void setSoldCount(int soldCount) {
		this.soldCount = soldCount;
	}

	public int getBoughtCountSubSoldCount() {
		return boughtCountSubSoldCount;
	}

	public void setBoughtCountSubSoldCount(int boughtCountSubSoldCount) {
		this.boughtCountSubSoldCount = boughtCountSubSoldCount;
	}

    public String getStatusTxt(){
        if (isSettled == 1)
            return CommonUtil.getMessage("opportunities.settled", null);
        if (isPublished == 1)
            return CommonUtil.getMessage("opportunities.published", null);

        return CommonUtil.getMessage("opportunities.notpublished", null);
    }
}
