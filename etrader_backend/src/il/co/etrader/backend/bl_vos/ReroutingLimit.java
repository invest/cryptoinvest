package il.co.etrader.backend.bl_vos;

import java.io.Serializable;

public class ReroutingLimit implements Serializable {
	private String limitDysplayName;
	private long limitTypeId;
	private String limitType;
	private long type;
	private boolean inatec;
	private boolean wireCard;
	private boolean tbiAnyoption;
	private boolean wireCard3d;
	private boolean wall;

	/**
	 * @return the limitDysplayName
	 */
	public String getLimitDysplayName() {
		return limitDysplayName;
	}

	/**
	 * @param limitDysplayName the limitDysplayName to set
	 */
	public void setLimitDysplayName(String limitDysplayName) {
		this.limitDysplayName = limitDysplayName;
	}

	/**
	 * @return the limitTypeId
	 */
	public long getLimitTypeId() {
		return limitTypeId;
	}

	/**
	 * @param limitTypeId the limitTypeId to set
	 */
	public void setLimitTypeId(long limitTypeId) {
		this.limitTypeId = limitTypeId;
	}

	/**
	 * @return the limitType
	 */
	public String getLimitType() {
		return limitType;
	}

	/**
	 * @param limitType the limitType to set
	 */
	public void setLimitType(String limitType) {
		this.limitType = limitType;
	}

	/**
	 * @return the type
	 */
	public long getType() {
		return type;
	}

	/**
	 * @param type the type to set
	 */
	public void setType(long type) {
		this.type = type;
	}

	/**
	 * @return the inatec
	 */
	public boolean isInatec() {
		return inatec;
	}

	/**
	 * @param inatec the inatec to set
	 */
	public void setInatec(boolean inatec) {
		this.inatec = inatec;
	}

	/**
	 * @return the wireCard
	 */
	public boolean isWireCard() {
		return wireCard;
	}

	/**
	 * @param wireCard the wireCard to set
	 */
	public void setWireCard(boolean wireCard) {
		this.wireCard = wireCard;
	}

	/**
	 * @return the tbiAnyoption
	 */
	public boolean isTbiAnyoption() {
		return tbiAnyoption;
	}

	/**
	 * @param tbiAnyoption the tbiAnyoption to set
	 */
	public void setTbiAnyoption(boolean tbiAnyoption) {
		this.tbiAnyoption = tbiAnyoption;
	}

	/**
	 * @return the wireCard3d
	 */
	public boolean isWireCard3d() {
		return wireCard3d;
	}

	/**
	 * @param wireCard3d the wireCard3d to set
	 */
	public void setWireCard3d(boolean wireCard3d) {
		this.wireCard3d = wireCard3d;
	}

	/**
	 * @return the wall
	 */
	public boolean isWall() {
		return wall;
	}

	/**
	 * @param wall the wall to set
	 */
	public void setWall(boolean wall) {
		this.wall = wall;
	}	
}
