package il.co.etrader.backend.bl_vos;

import java.io.Serializable;

public class EntryTypesDivision implements Serializable {

	private static final long serialVersionUID = 680152678535192438L;

	int entryTypesLeadsNum;
	int depositorsNum;
	int nonDepositorsNum;


	/**
	 * @return the depositorsNum
	 */
	public int getDepositorsNum() {
		return depositorsNum;
	}

	/**
	 * @param depositorsNum the depositorsNum to set
	 */
	public void setDepositorsNum(int depositorsNum) {
		this.depositorsNum = depositorsNum;
	}

	/**
	 * @param depositorsNum the depositorsNum to add
	 */
	public void addDepositorsNum(int depositorsNum) {
		this.depositorsNum += depositorsNum;
	}

	/**
	 * @return the entryTypesLeadsNum
	 */
	public int getEntryTypesLeadsNum() {
		return entryTypesLeadsNum;
	}

	/**
	 * @param entryTypesLeadsNum the entryTypesLeadsNum to set
	 */
	public void setEntryTypesLeadsNum(int entryTypesUsersNum) {
		this.entryTypesLeadsNum = entryTypesUsersNum;
	}

	/**
	 * @param entryTypesLeadsNum the entryTypesLeadsNum to add
	 */
	public void addEntryTypesLeadsNum(int entryTypesUsersNum) {
		this.entryTypesLeadsNum += entryTypesUsersNum;
	}


	/**
	 * @return the nonDepositorsNum
	 */
	public int getNonDepositorsNum() {
		return nonDepositorsNum;
	}

	/**
	 * @param nonDepositorsNum the nonDepositorsNum to set
	 */
	public void setNonDepositorsNum(int nonDepositorsNum) {
		this.nonDepositorsNum = nonDepositorsNum;
	}

	/**
	 * @param nonDepositorsNum the nonDepositorsNum to add
	 */
	public void addNonDepositorsNum(int nonDepositorsNum) {
		this.nonDepositorsNum += nonDepositorsNum;
	}

}


