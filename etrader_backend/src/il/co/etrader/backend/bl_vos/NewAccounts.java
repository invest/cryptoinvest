package il.co.etrader.backend.bl_vos;

import il.co.etrader.backend.util.Utils;
import il.co.etrader.util.CommonUtil;
import il.co.etrader.util.ConstantsBase;

import java.io.Serializable;
import java.sql.SQLException;
import java.util.Date;

import javax.faces.context.FacesContext;

import com.anyoption.common.beans.SpecialCare;

public class NewAccounts implements Serializable {
	private static final long serialVersionUID = 1L;
	private int userID;
	private String username;
	//protected boolean isSpecialCare;
	private Date registrationDate;
	private String campaignName;
	private long campaignId;
	private boolean hasDeposits;
	private boolean hasSuccessDeposits;
	private String skin;
	private String country;
	private long countryId;
	private long writerId;
    private long firstDepositAmount;
    private long highestFailedDeposit;
    private long currencyId;
    protected SpecialCare specialCare;

	/**
	 * @return the hasSuccessDeposits
	 */
	public boolean isHasSuccessDeposits() {
		return hasSuccessDeposits;
	}

	/**
	 * @param hasSuccessDeposits the hasSuccessDeposits to set
	 */
	public void setHasSuccessDeposits(boolean hasSuccessDeposits) {
		this.hasSuccessDeposits = hasSuccessDeposits;
	}

	public int getUserID()
	{
		return userID;
	}

	public String getUsername()
	{
		return username;
	}

	public Date getRegistrationDate()
	{
		return registrationDate;
	}

	public String getRegistrationDateTxt() throws SQLException {
		return CommonUtil.getDateTimeFormatDisplay(registrationDate, Utils.getWriter().getUtcOffset() );
	}

	public String getCampaignName()
	{
		return campaignName;
	}

	public long getCampaignId()
	{
		return campaignId;
	}

	public boolean isHasDeposits()
	{
		return this.hasDeposits;
	}

	public String getHasDepositsTxt(){
	   	FacesContext context=FacesContext.getCurrentInstance();

		if (this.hasDeposits == true) {
			if (this.hasSuccessDeposits == true) {
				return CommonUtil.getMessage("newaccounts.successDeposits", null, Utils.getWriterLocale(context));
			} else {
				return CommonUtil.getMessage("newaccounts.failedDeposits", null, Utils.getWriterLocale(context));
			}
		} else {
			return CommonUtil.getMessage("newaccounts.noDeposits", null, Utils.getWriterLocale(context));
		}
	}

	public int getHasDepositsStatus(){
		if (this.hasDeposits==true) {
			if (this.hasSuccessDeposits == true){
				return ConstantsBase.SUCCESS_DEPOSIT;
			}
			else{
				return ConstantsBase.FAILED_DEPOSIT;
			}
		} else {
			return ConstantsBase.NO_DEPOSIT;
		}
	}

	public void setUserID(int userID)
	{
		this.userID = userID;
	}

	public void setUsername(String username)
	{
		this.username = username;
	}

	public void setRegistrationDate(Date registrationDate)
	{
		this.registrationDate = registrationDate;
	}

	public void setCampaignName(String campaignName)
	{
		this.campaignName = campaignName;
	}

	public void setCampaignId(long campaignId)
	{
		this.campaignId = campaignId;
	}

	public void setHasDeposits(boolean hasDeposits)
	{
		this.hasDeposits = hasDeposits;
	}

	public String toString()
	{
		final String newLine = " \n ";
		String ans = "";

		ans = "NewAccount ( "
			+ "userID = " + this.userID + newLine
			+ "username = " + this.username + newLine
			+ "registrationDate = " + this.registrationDate + newLine
			+ "campaignName = " + this.campaignName + newLine
			+ "campaignId = " + this.campaignId + newLine
			+ "hasDeposits = " + this.hasDeposits + newLine
			+ "writerId = " + this.writerId + newLine
			+ " )";

		return ans;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String countryId) {
		this.country = countryId;
	}

	public String getSkin() {
		return skin;
	}

	public void setSkin(String skin) {
		this.skin = skin;
	}

	/**
	 * @return the countryId
	 */
	public long getCountryId() {
		return countryId;
	}

	/**
	 * @param countryId the countryId to set
	 */
	public void setCountryId(long countryId) {
		this.countryId = countryId;
	}

	/**
	 * @return the writerId
	 */
	public long getWriterId() {
		return writerId;
	}

	/**
	 * @param writerId the writerId to set
	 */
	public void setWriterId(long writerId) {
		this.writerId = writerId;
	}

    /**
     * @return the firstDepositAmount
     */
    public long getFirstDepositAmount() {
        return firstDepositAmount;
    }

    /**
     * @param firstDepositAmount the firstDepositAmount to set
     */
    public void setFirstDepositAmount(long firstDepositAmount) {
        this.firstDepositAmount = firstDepositAmount;
    }

    /**
     * @return the highestFailedDeposit
     */
    public long getHighestFailedDeposit() {
        return highestFailedDeposit;
    }

    /**
     * @param highestFailedDeposit the highestFailedDeposit to set
     */
    public void setHighestFailedDeposit(long highestFailedDeposit) {
        this.highestFailedDeposit = highestFailedDeposit;
    }

    /**
     * @return the currencyId
     */
    public long getCurrencyId() {
        return currencyId;
    }

    /**
     * @param currencyId the currencyId to set
     */
    public void setCurrencyId(long currencyId) {
        this.currencyId = currencyId;
    }

    public String getcurrencySymbol() {
        return CommonUtil.getMessage(CommonUtil.getCurrencySymbol(currencyId), null);
    }

    public long getDepositStatusForAmoutDisplay(){
        int depStatus = getHasDepositsStatus();
        long displayAmount = 0;
        switch (depStatus) {
            case 1:  displayAmount = 0;  break;
            case 2:  displayAmount = getHighestFailedDeposit();  break;
            case 3:  displayAmount = getFirstDepositAmount();  break;
        }
        return displayAmount/100;
    }

//	/**
//	 * @return the isSpecialCare
//	 */
//	public boolean isSpecialCare() {
//		return isSpecialCare;
//	}
//
//	/**
//	 * @param isSpecialCare the isSpecialCare to set
//	 */
//	public void setSpecialCare(boolean isSpecialCare) {
//		this.isSpecialCare = isSpecialCare;
//	}

    /**
     * @return the specialCare
     */
    public SpecialCare getSpecialCare() {
        return specialCare;
    }

    /**
     * @param specialCare the specialCare to set
     */
    public void setSpecialCare(SpecialCare specialCare) {
        this.specialCare = specialCare;
    }
}