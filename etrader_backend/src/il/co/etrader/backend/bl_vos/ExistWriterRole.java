package il.co.etrader.backend.bl_vos;

import java.io.Serializable;

/**
 * @author EranL
 *
 */
public class ExistWriterRole implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private String writerUserName;
	public ExistWriterRole(String writerUserName, String writerDepartment, String writerRole) {
		super();
		this.writerUserName = writerUserName;
		this.writerDepartment = writerDepartment;
		this.writerRole = writerRole;
	}
	private String writerDepartment;
	private String writerRole;
	
	public String getWriterUserName() {
		return writerUserName;
	}
	public void setWriterUserName(String writerUserName) {
		this.writerUserName = writerUserName;
	}
	public String getWriterDepartment() {
		return writerDepartment;
	}
	public void setWriterDepartment(String writerDepartment) {
		this.writerDepartment = writerDepartment;
	}
	public String getWriterRole() {
		return writerRole;
	}
	public void setWriterRole(String writerRole) {
		this.writerRole = writerRole;
	}
}
