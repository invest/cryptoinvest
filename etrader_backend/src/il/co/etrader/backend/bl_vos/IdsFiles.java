package il.co.etrader.backend.bl_vos;

import java.io.Serializable;
import java.util.Date;

import il.co.etrader.util.CommonUtil;

public class IdsFiles implements Serializable {
	
	public IdsFiles(long fileId, long userId, String userName, String firstName, String lastName, String countryName,
			String skin, Date uploadDate, long ksStatus, Long regulated, long totalCount, long skinId) {
		super();
		this.fileId = fileId;
		this.userId = userId;
		this.userName = userName;
		this.firstName = firstName;
		this.lastName = lastName;
		this.countryName = countryName;
		this.skin = skin;
		this.uploadDate = uploadDate;
		this.ksStatus = ksStatus;
		this.regulated = regulated;
		this.totalCount = totalCount;
		this.skinId = skinId;
	}

	private static final long serialVersionUID = 1L;
	
	private long fileId;
	private long userId;
	private String userName;
	private String firstName;
	private String lastName;
	private String countryName;
	private String skin;
	private long skinId;
	private Date uploadDate;
	private long ksStatus;
	private Long regulated;  //1-YES; 2-NO; 0-NULL;
	private long totalCount;
	
	public long getFileId() {
		return fileId;
	}
	public void setFileId(long fileId) {
		this.fileId = fileId;
	}
	public long getUserId() {
		return userId;
	}
	public void setUserId(long userId) {
		this.userId = userId;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public Date getUploadDate() {
		return uploadDate;
	}
	public void setUploadDate(Date uploadDate) {
		this.uploadDate = uploadDate;
	}
	public long getKsStatus() {
		return ksStatus;
	}
	public void setKsStatus(long ksStatus) {
		this.ksStatus = ksStatus;
	}
	public Long getRegulated() {
		return regulated;
	}
	public void setRegulated(Long regulated) {
		this.regulated = regulated;
	}
	
    public String toString() {
        String ls = System.getProperty("line.separator");
        return ls + "IdsFiles: "
            + super.toString()
            + "fileId: " + fileId + ls
            + "userId:" + userId + ls
            + "userName: " + userName + ls
            + "firstName: " + firstName + ls
            + "lastName: " + lastName + ls
            + "countryName: " + countryName + ls
            + "skin: " + skin + ls
            + "uploadDate: " + uploadDate + ls
            + "ksStatus: " + ksStatus + ls
            + "regulated: " + regulated + ls;
    }
	public long getTotalCount() {
		return totalCount;
	}
	public void setTotalCount(long totalCount) {
		this.totalCount = totalCount;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getCountryName() {
		return countryName;
	}
	public void setCountryName(String countryName) {
		this.countryName = countryName;
	}
	public String getSkin() {
		return skin;
	}
	public void setSkin(String skin) {
		this.skin = skin;
	}
	public long getSkinId() {
		return skinId;
	}
	public void setSkinId(long skinId) {
		this.skinId = skinId;
	}

}
