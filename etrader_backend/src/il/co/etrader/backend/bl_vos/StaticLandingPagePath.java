package il.co.etrader.backend.bl_vos;

import java.io.Serializable;

/**
 * @author eyalo
 * StaticLandingPagePath
 */
public class StaticLandingPagePath implements Serializable {

	private static final long serialVersionUID = 1L;
	private long id;
	private String name;
	private String folderName;
	private boolean isRegulated;
	
	/**
	 * @return the id
	 */
	public long getId() {
		return id;
	}
	
	/**
	 * @param id the id to set
	 */
	public void setId(long id) {
		this.id = id;
	}
	
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	
	/**
	 * @return the folderName
	 */
	public String getFolderName() {
		return folderName;
	}
	
	/**
	 * @param folderName the folderName to set
	 */
	public void setFolderName(String folderName) {
		this.folderName = folderName;
	}

	/**
	 * @return the isRegulated
	 */
	public boolean isRegulated() {
		return isRegulated;
	}

	/**
	 * @param isRegulated the isRegulated to set
	 */
	public void setRegulated(boolean isRegulated) {
		this.isRegulated = isRegulated;
	}
}
