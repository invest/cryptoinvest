package il.co.etrader.backend.bl_vos;

import java.io.Serializable;

/**
 * OptionPlusPrice class.
 *
 * @author Kobi
 */
public class OptionPlusPrice implements Serializable {
	private static final long serialVersionUID = 1L;

	protected long id;
	protected long minRangeId;
	protected long promileRangeId;
	protected double price;

	protected String minutesRangeStr;
	protected String promileRangeStr;

	/**
	 * @return the id
	 */
	public long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(long id) {
		this.id = id;
	}

	/**
	 * @return the minRangeId
	 */
	public long getMinRangeId() {
		return minRangeId;
	}

	/**
	 * @param minRangeId the minRangeId to set
	 */
	public void setMinRangeId(long minRangeId) {
		this.minRangeId = minRangeId;
	}

	/**
	 * @return the promileRangeId
	 */
	public long getPromileRangeId() {
		return promileRangeId;
	}

	/**
	 * @param promileRangeId the promileRangeId to set
	 */
	public void setPromileRangeId(long promileRangeId) {
		this.promileRangeId = promileRangeId;
	}

	/**
	 * @return the price
	 */
	public double getPrice() {
		return price;
	}

	/**
	 * @param price the price to set
	 */
	public void setPrice(double price) {
		this.price = price;
	}

	/**
	 * @return the minutesRangeStr
	 */
	public String getMinutesRangeStr() {
		return minutesRangeStr;
	}

	/**
	 * @param minutesRangeStr the minutesRangeStr to set
	 */
	public void setMinutesRangeStr(String minutesRangeStr) {
		this.minutesRangeStr = minutesRangeStr;
	}

	/**
	 * @return the promileRangeStr
	 */
	public String getPromileRangeStr() {
		return promileRangeStr;
	}

	/**
	 * @param promileRangeStr the promileRangeStr to set
	 */
	public void setPromileRangeStr(String promileRangeStr) {
		this.promileRangeStr = promileRangeStr;
	}

}
