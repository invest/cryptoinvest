package il.co.etrader.backend.bl_vos;

import java.io.Serializable;
import java.util.Date;

public class RiskAlertCCDiffCountriesDetails implements Serializable{

	private static final long serialVersionUID = 4193315795138629972L;

	private String ccCountries;
	private Date lastCCdepositDate;
	private String ccLast4digits;
	
	public String getCcCountries() {
		return ccCountries;
	}
	public void setCcCountries(String ccCountries) {
		this.ccCountries = ccCountries;
	}
	public Date getLastCCdepositDate() {
		return lastCCdepositDate;
	}
	public void setLastCCdepositDate(Date lastCCdepositDate) {
		this.lastCCdepositDate = lastCCdepositDate;
	}
	public String getCcLast4digits() {
		return ccLast4digits;
	}
	public void setCcLast4digits(String ccLast4digits) {
		this.ccLast4digits = ccLast4digits;
	}
}
