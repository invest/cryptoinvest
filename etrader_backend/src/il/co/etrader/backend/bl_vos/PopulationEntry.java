package il.co.etrader.backend.bl_vos;
 
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.Locale;

import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;

import com.anyoption.common.beans.SpecialCare;
import com.anyoption.common.bl_vos.PopulationEntryBase;

import il.co.etrader.backend.bl_managers.ApplicationData;
import il.co.etrader.backend.util.Constants;
import il.co.etrader.backend.util.Utils;
import il.co.etrader.bl_vos.Writer;
import il.co.etrader.util.CommonUtil;
import il.co.etrader.util.ConstantsBase;


public class PopulationEntry extends PopulationEntryBase {

	private static final long serialVersionUID = -3651059911821523394L;

	private String populationUserName;
	private long languageId;
	private String language;
	private String country;
	private long countryId;
	private String countryOffset;
	private long lastCallWriterId;
	private String lastCallWriter;
	private Date lastCallTime;
	private String lastCallTimeOffset;
	private long callsCounts;
	private long reachedCallsCounts;
	private long reachedCalls;
	private long notReachedCallsCounts;
	private Date callBackTime;
	private String callBackTimeOffset;
	private String rowColor;
	private String comments;
	private long lockingWriter;
	private long oldPeWriterId;
	private String oldPeWriter;
	private Date entryTypeActionTime;
	private Date cbLastCallTime;
	private String activityName;
	private String actionTypeName;
	private String lastSalesDepRep;
	private String lastChatRep;
	private long campaignId;
	private String campaignName;
	private double firstDeclineAmountUsd;
    
    private String dynamicParam;
    //private boolean isSpecialCare;
    private boolean fddPreviousCallIndicator;// if the last population type was no deposint or call me
    protected SpecialCare specialCare;

    private Date lastDepositTime;
    private long totalTurnover;
    private String totalTurnoverTxt;

	private Date dateOfWithdraw;
	private String withdrawAmountTxt;
	private String userRankTxt;
	private String userStatusTxt;
	private String mobilePhone;
	private String landLindPhone;
	private long   reachCustomerFlag;
	private String phone;
	private String firstDeposit;
	private String userBalance;
	private String countDeposit;
	private int rowColorId;
	private String affiliateKey;
	private long assignTypeId;
	private Date timeAssign;
	private String backgroundColor;
	private long lastLoginInXMinutes;

	/**
	 * @return the withdrawAmountTxt
	 */
	public String getWithdrawAmountTxt() {
		return withdrawAmountTxt;
	}

	/**
	 * @param withdrawAmountTxt the withdrawAmountTxt to set
	 */
	public void setWithdrawAmountTxt(String withdrawAmountTxt) {
		this.withdrawAmountTxt = withdrawAmountTxt;
	}

	/**
	 * @return the dateOfWithdraw
	 */
	public Date getDateOfWithdraw() {
		return dateOfWithdraw;
	}

	/**
	 * @param dateOfWithdraw the dateOfWithdraw to set
	 */
	public void setDateOfWithdraw(Date dateOfWithdraw) {
		this.dateOfWithdraw = dateOfWithdraw;
	}

	/**
	 * @return the lastDepositTime
	 */
	public Date getLastDepositTime() {
		return lastDepositTime;
	}

	/**
	 * @param lastDepositTime the lastDepositTime to set
	 */
	public void setLastDepositTime(Date lastDepositTime) {
		this.lastDepositTime = lastDepositTime;
	}

	public PopulationEntry (){
        specialCare = null;
    }

	/**
	 * @return the cbLastCallTime
	 */
	public Date getCbLastCallTime() {
		return cbLastCallTime;
	}

	/**
	 * @param cbLastCallTime the cbLastCallTime to set
	 */
	public void setCbLastCallTime(Date cbLastCallTime) {
		this.cbLastCallTime = cbLastCallTime;
	}

	public String getCbLastCallTimeTxt() {
		return getCbLastCallTimeTxt(Utils.getWriter().getUtcOffset(), "&nbsp;");
	}

	public String getCbLastCallTimeTxt(String utcOffSet, String delimeter) {
		return CommonUtil.getDateTimeFormatDisplay(cbLastCallTime, utcOffSet, delimeter);
	}

	/**
	 * @return the oldPeWriter
	 */
	public String getOldPeWriter() {
		return oldPeWriter;
	}

	/**
	 * @param oldPeWriter the oldPeWriter to set
	 */
	public void setOldPeWriter(String oldPeWriter) {
		this.oldPeWriter = oldPeWriter;
	}

	/**
	 * @return the oldPeWriterId
	 */
	public long getOldPeWriterId() {
		return oldPeWriterId;
	}

	/**
	 * @param oldPeWriterId the oldPeWriterId to set
	 */
	public void setOldPeWriterId(long oldPeWriterId) {
		this.oldPeWriterId = oldPeWriterId;
	}

	/**
	 * Load population entry
	 * @param entry
	 */
	public void loadEntry(PopulationEntry entry) {
		super.loadEntry(entry);
		this.populationUserId = entry.populationUserId;
		this.currPopulationName = entry.currPopulationName;
		this.language = entry.language;
		this.country = entry.country;
		this.lastCallWriter = entry.lastCallWriter;
		this.lastCallTime = entry.lastCallTime;
		this.lastCallTimeOffset = entry.lastCallTimeOffset;
		this.callsCounts = entry.callsCounts;
		this.callBackTime = entry.callBackTime;
		this.callBackTimeOffset = entry.callBackTimeOffset;
		this.rowColor = entry.rowColor;
		this.skinId = entry.skinId;
		this.assignWriterId = entry.assignWriterId;
		this.lockHistoryId = entry.lockHistoryId;
		this.comments = entry.comments;
		this.lockingWriter = entry.lockingWriter;
		this.dynamicParam = entry.dynamicParam;
	}

	/**
	 * @return the callBackTime
	 */
	public Date getCallBackTime() {
		return callBackTime;
	}

	/**
	 * @param callBackTime the callBackTime to set
	 */
	public void setCallBackTime(Date callBackTime) {
		this.callBackTime = callBackTime;
	}

	/**
	 * @return the callBackTimeOffset
	 */
	public String getCallBackTimeOffset() {
		return callBackTimeOffset;
	}

	/**
	 * @param callBackTimeOffset the callBackTimeOffset to set
	 */
	public void setCallBackTimeOffset(String callBackTimeOffset) {
		this.callBackTimeOffset = callBackTimeOffset;
	}

	/**
	 * @return the callsCounts
	 */
	public long getCallsCounts() {
		return callsCounts;
	}

	/**
	 * @param callsCounts the callsCounts to set
	 */
	public void setCallsCounts(long callsCounts) {
		this.callsCounts = callsCounts;
	}

	/**
	 * @return the country
	 */
	public String getCountry() {
		FacesContext context = FacesContext.getCurrentInstance();
		ApplicationData ap = (ApplicationData)context.getApplication().createValueBinding(Constants.BIND_APPLICATION_DATA).getValue(context);
		if (null == ap.getCountry(countryId)) {
			return "";
		}
		return ap.getCountry(countryId).getName();
	}

	/**
	 * @return the countryWithOffsetDiff
	 */
	public String getCountryWithOffsetDiff() {
		return getCountry() + Utils.getUtcDifference(Utils.getWriter().getUtcOffset(),
														getCountryOffset());
	}

	public String getCountryWithOffsetDiff(String utcOffSet) {
		return ApplicationData.getCountry(countryId).getName() + Utils.getUtcDifference(utcOffSet,getCountryOffset());
	}

	/**
	 * @return the countryId
	 */
	public long getCountryId() {
		return countryId;
	}

	/**
	 * @param countryId the countryId to set
	 */
	public void setCountryId(long countryId) {
		this.countryId = countryId;
	}

	/**
	 * @return the language
	 */

	public String getLanguage() {
		if (languageId > 0){
			FacesContext context = FacesContext.getCurrentInstance();
			ApplicationData ap = (ApplicationData)context.getApplication().createValueBinding(Constants.BIND_APPLICATION_DATA).getValue(context);
			return CommonUtil.getMessage(ap.getLanguage(languageId).getDisplayName(), null);
		}
		return ConstantsBase.EMPTY_STRING;
	}
	
	public String getUserBrand() {
		if (userBrandId > 0){
			FacesContext context = FacesContext.getCurrentInstance();
			ApplicationData ap = (ApplicationData)context.getApplication().createValueBinding(Constants.BIND_APPLICATION_DATA).getValue(context);
			return ap.getPlatformsMap().get(userBrandId);
		}
		return ConstantsBase.EMPTY_STRING;
	}

	/**
	 * @return the lastCallTime
	 */
	public Date getLastCallTime() {
		return lastCallTime;
	}

	/**
	 * @param lastCallTime the lastCallTime to set
	 */
	public void setLastCallTime(Date lastCallTime) {
		this.lastCallTime = lastCallTime;
	}

	/**
	 * @return the lastCallTimeOffset
	 */
	public String getLastCallTimeOffset() {
		return lastCallTimeOffset;
	}

	/**
	 * @param lastCallTimeOffset the lastCallTimeOffset to set
	 */
	public void setLastCallTimeOffset(String lastCallTimeOffset) {
		this.lastCallTimeOffset = lastCallTimeOffset;
	}

	/**
	 * @return the populationUserName
	 */
	public String getPopulationUserName() {
		return populationUserName;
	}

	/**
	 * @param populationUserName the populationUserName to set
	 */
	public void setPopulationUserName(String populationUserName) {
		this.populationUserName = populationUserName;
	}

	/**
	 * @return the rowColor
	 */
	public String getRowColor() {
		return rowColor;
	}

	/**
	 * @param rowColor the rowColor to set
	 */
	public void setRowColor(String rowColor) {
		this.rowColor = rowColor;
	}

	public String getQualificationTimeTxt() {
		return getQualificationTimeTxt(Utils.getWriter().getUtcOffset(), " ");
	}

	public String getQualificationTimeTxt(String utcOffSet, String delimiter) {
		return CommonUtil.getDateTimeFormatDisplay(qualificationTime, utcOffSet, delimiter);
	}

	public String getLastCallTimeTxt() {
		return getLastCallTimeTxt(Utils.getWriter().getUtcOffset(), "&nbsp;");
	}

	public String getLastCallTimeTxt(String utcOffset) {
		return CommonUtil.getDateTimeFormatDisplay(lastCallTime, utcOffset, "&nbsp;");
	}

	public String getLastCallTimeTxt(String utcOffset, String delimiter) {
		return CommonUtil.getDateTimeFormatDisplay(lastCallTime, utcOffset , delimiter);
	}

	public String getCallBackTimeTxt() {
		return CommonUtil.getDateTimeFormatDisplay(callBackTime, Utils.getWriter().getUtcOffset(), "&nbsp;");
	}

	public String getCallBackTimeTxt(String utcOffset, String delimiter) {
		return CommonUtil.getDateTimeFormatDisplay(callBackTime, utcOffset , delimiter);
	}
//	/**
//	 * @return the assignWriter Txt
//	 */
//	public String getAssignWriterTxt() {
//		if ( CommonUtil.IsParameterEmptyOrNull(assignWriter) ) {
//			return "";
//		}
//		return assignWriter;
//	}

	/**
	 * Get writers list for assign
	 * @return
	 */
	public ArrayList getWritersToAssign() {
		ArrayList<SelectItem> list = new ArrayList<SelectItem>();
		list.add(new SelectItem(new Long(0), new String()));
		list.addAll(Utils.getWriter().getSalesWriters(skinId, populationSalesTypeId, currPopualtionTypeId));
		Collections.sort(list, new CommonUtil.selectItemComparator());
		return list;
	}

	public boolean isAssignToWriter() {
		if(assignWriterId > 0 ) {
			return true;
		}
		return false;
	}

	/**
	 * @return the lastCallWriter
	 */
	public String getLastCallWriter() {
		return lastCallWriter;
	}

	/**
	 * @param lastCallWriter the lastCallWriter to set
	 */
	public void setLastCallWriter(String lastCallWriter) {
		this.lastCallWriter = lastCallWriter;
	}

	/**
	 * @return the lastCallWriterId
	 */
	public long getLastCallWriterId() {
		return lastCallWriterId;
	}

	/**
	 * @param lastCallWriterId the lastCallWriterId to set
	 */
	public void setLastCallWriterId(long lastCallWriterId) {
		this.lastCallWriterId = lastCallWriterId;
	}

	/**
	 * @return the comments
	 */
	public String getComments() {
		return comments;
	}

	/**
	 * @param comments the comments to set
	 */
	public void setComments(String comments) {
		this.comments = comments;
	}

	public String getAssignWriterName(){
		String writerName = ConstantsBase.EMPTY_STRING;
		if (this.assignWriterId != 0) {
		//	HashMap<Long, Writer> hm = ;
			Writer writer = ApplicationData.getRetentionWriters().get(this.assignWriterId);
			if (writer != null) {
				writerName = writer.getUserName();
			}
		}
		return writerName;
	}

	/**
	 * @return the entryTypeName
	 */
	public String getEntryTypeName() {
		FacesContext context = FacesContext.getCurrentInstance();
		ApplicationData ap = (ApplicationData)context.getApplication().createValueBinding(Constants.BIND_APPLICATION_DATA).getValue(context);
		String key = ap.populationEntryType.get(entryTypeId);
		return CommonUtil.getMessage(key,null);

	}

	/**
	 * @return the lockingWriter
	 */
	public String getLockingWriter() {
		if(this.lockingWriter != 0){
			FacesContext context = FacesContext.getCurrentInstance();
			WriterWrapper wr = (WriterWrapper)context.getApplication().createValueBinding(Constants.BIND_WRITER_WRAPPER).getValue(context);
			return wr.getAllWritersMap().get(this.lockingWriter).getUserName();
		}
		return "";
	}

	/**
	 * @param lockingWriter the lockingWriter to set
	 */
	public void setLockingWriter(long lockingWriter) {
		this.lockingWriter = lockingWriter;
	}

	/**
	 * @return the languageId
	 */
	public long getLanguageId() {
		return languageId;
	}

	/**
	 * @param languageId the languageId to set
	 */
	public void setLanguageId(long languageId) {
		this.languageId = languageId;
	}

	/**
	 * @return the countryOffset
	 */
	public String getCountryOffset() {
		return countryOffset;
	}

	/**
	 * @param countryOffset the countryOffset to set
	 */
	public void setCountryOffset(String countryOffset) {
		this.countryOffset = countryOffset;
	}

	/**
	 * @return the entryTypeActionTime
	 */
	public Date getEntryTypeActionTime() {
		return entryTypeActionTime;
	}

	/**
	 * @param entryTypeActionTime the entryTypeActionTime to set
	 */
	public void setEntryTypeActionTime(Date entryTypeActionTime) {
		this.entryTypeActionTime = entryTypeActionTime;
	}

	public String getEntryTypeActionTimeTxt(String utcOffSet, String delimiter) {
		return CommonUtil.getDateTimeFormatDisplay(entryTypeActionTime, utcOffSet, delimiter);
	}

	public String getEntryTypeActionTimeTxt() {
		return getEntryTypeActionTimeTxt(Utils.getWriter().getUtcOffset(), "&nbsp;");
	}

	/**
	 * @return the activityName
	 */
	public String getActivityName() {
		if (!CommonUtil.isParameterEmptyOrNull(activityName)){
			return  CommonUtil.getMessage(activityName, null);
		}
		return "";
	}

	/**
	 * @param activityName the activityName to set
	 */
	public void setActivityName(String activityName) {
		this.activityName = activityName;
	}


	/**
	 * @return the actionTypeName
	 */
	public String getActionTypeName() {
		return CommonUtil.getMessage(actionTypeName, null);
	}

	/**
	 * @return the actionTypeName
	 */
	public String getActionTypeName(Locale l) {
		return CommonUtil.getMessage(actionTypeName, null, l);
	}

	/**
	 * @param actionTypeName the actionTypeName to set
	 */
	public void setActionTypeName(String actionTypeName) {
		this.actionTypeName = actionTypeName;
	}

	/**
	 * @return the reachedCallsCounts
	 */
	public long getReachedCallsCounts() {
		return reachedCallsCounts;
	}

	/**
	 * @param reachedCallsCounts the reachedCallsCounts to set
	 */
	public void setReachedCallsCounts(long reachedCallsCounts) {
		this.reachedCallsCounts = reachedCallsCounts;
	}

	
	/**
	 * @return the reachedCalls
	 */
	public long getReachedCalls() {
		return reachedCalls;
	}

	/**
	 * @param reachedCalls the reachedCalls to set
	 */
	public void setReachedCalls(long reachedCalls) {
		this.reachedCalls = reachedCalls;
	}

	/**
	 * @return the lastSalesDepRep
	 */
	public String getLastSalesDepRep() {
		return lastSalesDepRep;
	}

	/**
	 * @param lastSalesDepRep the lastSalesDepRep to set
	 */
	public void setLastSalesDepRep(String lastSalesDepRep) {
		this.lastSalesDepRep = lastSalesDepRep;
	}

	/**
	 * @return the campaignId
	 */
	public long getCampaignId() {
		return campaignId;
	}

	/**
	 * @param campaignId the campaignId to set
	 */
	public void setCampaignId(long campaignId) {
		this.campaignId = campaignId;
	}

	/**
	 * @return the campaignName
	 */
	public String getCampaignName() {
		return campaignName;
	}

	/**
	 * @param campaignName the campaignName to set
	 */
	public void setCampaignName(String campaignName) {
		this.campaignName = campaignName;
	}

    /**
     * Get Campaign Priorty name By Id
     * @param campaignPriortyId
     * @return
     */
    public String getCampaignPriortyStringById(int campaignPriortyId, Locale locale){
        switch (campaignPriortyId) {
            case 1: return CommonUtil.getMessage("campaign.priorty.low", null, locale);
            case 2: return CommonUtil.getMessage("campaign.priorty.medium", null, locale);
            case 3: return CommonUtil.getMessage("campaign.priorty.high", null, locale);
        }
        return CommonUtil.getMessage("campaign.priorty.low", null, locale);
    }

    /**
     * Return CampaignId And Priorty
     * Add default locale because we are using this function also on EntriesReportSender
     * @return
     */
    public String getCampaignIdAndPriorty(){
        return campaignId + "_" + getCampaignPriortyStringById(campaignPriorityId, new Locale(Constants.LOCALE_DEFAULT));

    }

//	/**
//	 * @return the isSpecialCare
//	 */
//	public boolean isSpecialCare() {
//		return isSpecialCare;
//	}
//
//	/**
//	 * @param isSpecialCare the isSpecialCare to set
//	 */
//	public void setSpecialCare(boolean isSpecialCare) {
//		this.isSpecialCare = isSpecialCare;
//	}

	/**
	 * @return the fddPreviousCallIndicator
	 */
	public boolean isFddPreviousCallIndicator() {
		return fddPreviousCallIndicator;
	}

	/**
	 * @param fddPreviousCallIndicator the fddPreviousCallIndicator to set
	 */
	public void setFddPreviousCallIndicator(boolean fddPreviousCallIndicator) {
		this.fddPreviousCallIndicator = fddPreviousCallIndicator;
	}

    /**
     * @return the specialCare
     */
    public SpecialCare getSpecialCare() {
        return specialCare;
    }

    /**
     * @param specialCare the specialCare to set
     */
    public void setSpecialCare(SpecialCare specialCare) {
        this.specialCare = specialCare;
    }

	/**
	 * @return the dynamicParam
	 */
	public String getDynamicParam() {
		return dynamicParam;
	}

	/**
	 * @param dynamicParam the dynamicParam to set
	 */
	public void setDynamicParam(String dynamicParam) {
		this.dynamicParam = dynamicParam;
	}

	/**
	 * @return the timeLastLogin
	 */
	public String getTimeLastLoginTxt() {
		return getTimeLastLoginTxt(Utils.getWriter().getUtcOffset(), " ");
	}

	public String getTimeLastLoginTxt(String utcOffSet, String delimiter) {
		return CommonUtil.getDateTimeFormatDisplay(timeLastLogin, utcOffSet, delimiter);
	}

	/**
	 * @return the isLastLoginHighlight
	 */
	public boolean isLastLoginHighlight() {
		if (lastLoginInXMinutes != ConstantsBase.ALL_FILTER_ID) {
			return true;
		}
		return false;
	}

	/**
	 * @return the totalTurnover
	 */
	public long getTotalTurnover() {
		return totalTurnover;
	}

	/**
	 * @param totalTurnover the totalTurnover to set
	 */
	public void setTotalTurnover(long totalTurnover) {
		this.totalTurnover = totalTurnover;
	}

	/**
	 * @return the totalTurnoverTxt
	 */
	public String getTotalTurnoverTxt() {
		return totalTurnoverTxt;
	}

	/**
	 * @param totalTurnoverTxt the totalTurnoverTxt to set
	 */
	public void setTotalTurnoverTxt(String totalTurnoverTxt) {
		this.totalTurnoverTxt = totalTurnoverTxt;
	}

	/**
	 * @param language the language to set
	 */
	public void setLanguage(String language) {
		this.language = language;
	}

	/**
	 * @param country the country to set
	 */
	public void setCountry(String country) {
		this.country = country;
	}

	/**
	 * @return the notReachedCallsCounts
	 */
	public long getNotReachedCallsCounts() {
		return notReachedCallsCounts;
	}

	/**
	 * @param notReachedCallsCounts the notReachedCallsCounts to set
	 */
	public void setNotReachedCallsCounts(long notReachedCallsCounts) {
		this.notReachedCallsCounts = notReachedCallsCounts;
	}

	/**
	 * @return the userRankTxt
	 */
	public String getUserRankTxt() {
		return CommonUtil.getMessage(userRankTxt, null);
	}

	/**
	 * @param userRankTxt the userRankTxt to set
	 */
	public void setUserRankTxt(String userRankTxt) {
		this.userRankTxt = userRankTxt;
	}

	/**
	 * @return the userStatusTxt
	 */
	public String getUserStatusTxt() {
		return CommonUtil.getMessage(userStatusTxt, null);
	}

	/**
	 * @param userStatusTxt the userStatusTxt to set
	 */
	public void setUserStatusTxt(String userStatusTxt) {
		this.userStatusTxt = userStatusTxt;
	}

    /**
     * @return the mobilePhone
     */
    public String getMobilePhone() {
        return mobilePhone;
    }

    /**
     * @param mobilePhone the mobilePhone to set
     */
    public void setMobilePhone(String mobilePhone) {
        this.mobilePhone = mobilePhone;
    }

    /**
     * @return the landLindPhone
     */
    public String getLandLindPhone() {
        return landLindPhone;
    }

    /**
     * @param landLindPhone the landLindPhone to set
     */
    public void setLandLindPhone(String landLindPhone) {
        this.landLindPhone = landLindPhone;
    }

    /**
	 * @return the lastChatRep
	 */
	public String getLastChatRep() {
		return lastChatRep;
	}

	/**
	 * @param lastChatRep the lastChatRep to set
	 */
	public void setLastChatRep(String lastChatRep) {
		this.lastChatRep = lastChatRep;
	}
	
    /**
     * @return the reachCustomerFlag
     */
    public long getReachCustomerFlag() {
        return reachCustomerFlag;
    }

    /**
     * @param reachCustomerFlag the reachCustomerFlag to set
     */
    public void setReachCustomerFlag(long reachCustomerFlag) {
        this.reachCustomerFlag = reachCustomerFlag;
    }

	/**
     * Convert population entry base to population entry
     * @param populationEntryBase
     * @return populationEntry as PopulationEntry
     */
    public void convertBaseToPopulationEntry(PopulationEntryBase populationEntryBase) {
        // set entry base.
        setAssignWriterId(populationEntryBase.getAssignWriterId());
        setContactId(populationEntryBase.getContactId());
        setCurrEntryId(populationEntryBase.getCurrEntryId());
        setEntryTypeId(populationEntryBase.getEntryTypeId());
        setEntryTypeActionId(populationEntryBase.getEntryTypeActionId());
        setGroupId(populationEntryBase.getGroupId());
        setLockHistoryId(populationEntryBase.getLockHistoryId());
        setCurrPopualtionTypeId(populationEntryBase.getCurrPopualtionTypeId());
        setPopulationId(populationEntryBase.getPopulationId());
        setPopulationUserId(populationEntryBase.getPopulationUserId());
        setQualificationTime(populationEntryBase.getQualificationTime());
        setSkinId(populationEntryBase.getSkinId());
        setUserId(populationEntryBase.getUserId());
        setDelayId(populationEntryBase.getDelayId());
        setDisplayed(populationEntryBase.isDisplayed());
        setTimeJoinControlGroup(populationEntryBase.getTimeJoinControlGroup());
        setOldPopulationName(populationEntryBase.getOldPopulationName());
        setOldPopEntryId(populationEntryBase.getOldPopEntryId());
        setOldPopulationTypeId(populationEntryBase.getOldPopulationTypeId());
        setCurrPopualtionDeptId(populationEntryBase.getCurrPopualtionDeptId());
        setUserName(populationEntryBase.getUserName());
        setUserClassId(populationEntryBase.getUserClassId());
    }

	/**
	 * @return the phone
	 */
	public String getPhone() {
		long countryId = getCountryId();
        String prefix = Constants.EMPTY_STRING;
        if (countryId != 0) {
            prefix = CommonUtil.getPrefixFromCountry(countryId);
        }
        phone = getMobilePhone();
        if (!Utils.isParameterEmptyOrNull(phone) && !prefix.equals("972")){
            phone = prefix.concat(phone);
        }
        if (Utils.isParameterEmptyOrNull(phone)) {
        	phone = getLandLindPhone();
            if (!Utils.isParameterEmptyOrNull(phone) && !prefix.equals("972")){
                phone = prefix.concat(phone);
            }
        }
		return phone;
	}

	/**
	 * @param phone the phone to set
	 */
	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getFirstDeposit() {
		return firstDeposit;
	}

	public void setFirstDeposit(String firstDeposit) {
		this.firstDeposit = firstDeposit;
	}

	public String getCountDeposit() {
		return countDeposit;
	}

	public void setCountDeposit(String countDeposit) {
		this.countDeposit = countDeposit;
	}

	/**
	 * @return the rowColorId
	 */
	public int getRowColorId() {
		return rowColorId;
	}

	/**
	 * @param rowColorId the rowColorId to set
	 */
	public void setRowColorId(int rowColorId) {
		this.rowColorId = rowColorId;
	}

	public String getAffiliateKey() {
		return affiliateKey;
	}

	public void setAffiliateKey(String affiliateKey) {
		this.affiliateKey = affiliateKey;
	}
	
	public double getFirstDeclineAmountUsd() {
		return firstDeclineAmountUsd;
	}

	public void setFirstDeclineAmountUsd(double firstDeclineAmountUsd) {
		this.firstDeclineAmountUsd = firstDeclineAmountUsd;
	}
	
	public String getFirstDeclineAmountUsdTxt() {
		return CommonUtil.displayAmount(getFirstDeclineAmountUsd(), true, ConstantsBase.CURRENCY_USD_ID);
	}

	/**
	 * @return the assignTypeId
	 */
	public long getAssignTypeId() {
		return assignTypeId;
	}

	/**
	 * @param assignTypeId the assignTypeId to set
	 */
	public void setAssignTypeId(long assignTypeId) {
		this.assignTypeId = assignTypeId;
	}

	/**
	 * @return the timeAssign
	 */
	public Date getTimeAssign() {
		return timeAssign;
	}

	/**
	 * @param timeAssign the timeAssign to set
	 */
	public void setTimeAssign(Date timeAssign) {
		this.timeAssign = timeAssign;
	}

	/**
	 * @return the backgroundColor
	 */
	public String getBackgroundColor() {
		return backgroundColor;
	}

	/**
	 * @param backgroundColor the backgroundColor to set
	 */
	public void setBackgroundColor(String backgroundColor) {
		this.backgroundColor = backgroundColor;
	}

	/**
	 * @return the lastLoginInXMinutes
	 */
	public long getLastLoginInXMinutes() {
		return lastLoginInXMinutes;
	}

	/**
	 * @param lastLoginInXMinutes the lastLoginInXMinutes to set
	 */
	public void setLastLoginInXMinutes(long lastLoginInXMinutes) {
		this.lastLoginInXMinutes = lastLoginInXMinutes;
	}
	
	public String getUserBalance() {
		return userBalance;
	}

	public void setUserBalance(String userBalance) {
		this.userBalance = userBalance;
	}
	
	public String getLastLoginText() {
		long msgTime = lastLoginInXMinutes;
		String msgKey = "retention.last.login";
		if (lastLoginInXMinutes > Constants.MINUTES_IN_HOUR) {
			msgTime = lastLoginInXMinutes / Constants.MINUTES_IN_HOUR;
			msgKey = Constants.ENUM_LAST_LOGIN_LEVEL_TWO_CODE;
		}
		String[] params = new String[1];
		params[0] = String.valueOf(msgTime);
		return CommonUtil.getMessage(msgKey, params);
	}
	
	/**
	 * Get bind Application Data
	 * @return ap as ApplicationData
	 */
	public ApplicationData getBindAppData() {
		FacesContext context = FacesContext.getCurrentInstance();
		ApplicationData ap = (ApplicationData)context.getApplication().createValueBinding(Constants.BIND_APPLICATION_DATA).getValue(context);
		return ap;
	}
	
	/**
	 * PopulationEntry
	 * @param peb
	 */
	public PopulationEntry(PopulationEntryBase peb) {
		super(peb);
	}
}

