/**
 *
 */
package il.co.etrader.backend.bl_vos;

import java.io.Serializable;

/**
 * Assign writer
 *
 * @author Kobi
 *
 */
public class AssignWriter implements Serializable {
	private static final long serialVersionUID = -7252289266955658494L;

	private long writerId;
	private String writerName;
	private boolean assign;
	private Long tasks;

	/**
	 * @return the assign
	 */
	public boolean isAssign() {
		return assign;
	}

	/**
	 * @param assign the assign to set
	 */
	public void setAssign(boolean assign) {
		this.assign = assign;
	}

	/**
	 * @return the tasks
	 */
	public Long getTasks() {
		return tasks;
	}

	/**
	 * @param tasks the tasks to set
	 */
	public void setTasks(Long tasks) {
		this.tasks = tasks;
	}

	/**
	 * @return the writerId
	 */
	public long getWriterId() {
		return writerId;
	}

	/**
	 * @param writerId the writerId to set
	 */
	public void setWriterId(long writerId) {
		this.writerId = writerId;
	}

	/**
	 * @return the writerName
	 */
	public String getWriterName() {
		return writerName;
	}

	/**
	 * @param writerName the writerName to set
	 */
	public void setWriterName(String writerName) {
		this.writerName = writerName;
	}

}
