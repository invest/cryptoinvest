package il.co.etrader.backend.bl_vos;

import com.anyoption.common.beans.Transaction;



public class MergeTransaction extends Transaction{
	
	private static final long serialVersionUID = 4022941825108601421L;
	private boolean isValidToMerge;
	private String comment;

	/**
	 * @return the isValidToMerge
	 */
	public boolean isValidToMerge() {
		return isValidToMerge;
	}

	/**
	 * @param isValidToMerge the isValidToMerge to set
	 */
	public void setValidToMerge(boolean isValidToMerge) {
		this.isValidToMerge = isValidToMerge;
	}

	/**
	 * @return the comment
	 */
	public String getComment() {
		return comment;
	}

	/**
	 * @param comment the comment to set
	 */
	public void setComment(String comment) {
		this.comment = comment;
	}
	
	
}
