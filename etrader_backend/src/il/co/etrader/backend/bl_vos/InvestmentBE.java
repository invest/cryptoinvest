//package il.co.etrader.backend.bl_vos;
//
//
//
//import com.anyoption.common.beans.Investment;
//
//import il.co.etrader.util.CommonUtil;
//import il.co.etrader.util.ConstantsBase;
//
//
//
//public class InvestmentBE extends Investment {
//
//	public String getTimeCreatedTxt() {
//		return CommonUtil.getDateTimeFormatDisplay(timeCreated, CommonUtil.getUtcOffset(utcOffsetCreated));
//	}
//
//	public String getTimeEstClosingTxt() {
//		long offsetCreated = CommonUtil.getUtcOffsetValue(utcOffsetCreated);
//		long offsetEstSettled = CommonUtil.getUtcOffsetValue(utcOffsetEstClosing);
//		long offsetGain = 0;
//
//		if ( offsetCreated != -1 && offsetEstSettled != -1 ) {   // -1 : problem with getting offset value
//			offsetGain = Math.abs(offsetCreated - offsetEstSettled);
//		}
//
//		if ( offsetGain > ConstantsBase.OFFSET_GAIN_BETWEEN_CREATED_AND_SETTLED ) {
//			return CommonUtil.getDateTimeFormatDisplay(timeEstClosing, CommonUtil.getUtcOffset(utcOffsetEstClosing));
//		}
//
//		return CommonUtil.getDateTimeFormatDisplay(timeEstClosing, CommonUtil.getUtcOffset(utcOffsetEstClosing));
//	}
//
//	public String getTimeActClosingTxt() {
//		return CommonUtil.getDateTimeFormatDisplay(timeActClosing, CommonUtil.getUtcOffset(utcOffsetActClosing));
//	}
//	public String getTimeSettledTxt() {
//		return CommonUtil.getDateTimeFormatDisplay(timeSettled, CommonUtil.getUtcOffset(utcOffsetSettled));
//	}
//	public String getTimeCanceledTxt() {
//		return CommonUtil.getDateTimeFormatDisplay(timeCanceled, CommonUtil.getUtcOffset(utcOffsetCancelled));
//	}
//
//
//
//}
