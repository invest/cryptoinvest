package il.co.etrader.backend.bl_vos;

import il.co.etrader.backend.bl_managers.ApplicationData;
import il.co.etrader.backend.util.Constants;
import il.co.etrader.util.CommonUtil;

import java.io.Serializable;
import java.util.HashMap;

import javax.faces.context.FacesContext;


public class RegLeadsDetails implements Serializable {

	private static final long serialVersionUID = -875262580188452230L;

	private HashMap<Integer, String> populationEntryTypes;

	private long campaignId;
	private String campaignName;
	private long combId;
	private long userId;
	private long contactId;
	private String regPopName;
	private String currPopName;
	private boolean isDepositior;
	private boolean isSalesDep;
	private String salesDepRepName;
	private String entryType;
	private String entryTypeActionType;

	// Excel fields
	private String timeCreated;
	private String originalPop;
	private String currentPop;
	private String actionTime;
	private String channel;
	private String department;
	private String repName;
	private String reachedStatus;
	private String subject;
	private String actionType;
	private String comments;
	private long issueActionId;

	public RegLeadsDetails() {
		super();

		FacesContext context = FacesContext.getCurrentInstance();
		ApplicationData ap = (ApplicationData)context.getApplication().createValueBinding(Constants.BIND_APPLICATION_DATA).getValue(context);
		populationEntryTypes = ap.getPopulationEntryType();
	}

	/**
	 * @return the campaignId
	 */
	public long getCampaignId() {
		return campaignId;
	}

	/**
	 * @param campaignId the campaignId to set
	 */
	public void setCampaignId(long campaignId) {
		this.campaignId = campaignId;
	}

	/**
	 * @return the campaignName
	 */
	public String getCampaignName() {
		return campaignName;
	}

	/**
	 * @param campaignName the campaignName to set
	 */
	public void setCampaignName(String campaignName) {
		this.campaignName = campaignName;
	}

	/**
	 * @return the combId
	 */
	public long getCombId() {
		return combId;
	}

	/**
	 * @param combId the combId to set
	 */
	public void setCombId(long combId) {
		this.combId = combId;
	}

	/**
	 * @return the contactId
	 */
	public long getContactId() {
		return contactId;
	}

	/**
	 * @param contactId the contactId to set
	 */
	public void setContactId(long contactId) {
		this.contactId = contactId;
	}



	/**
	 * @return the currPopName
	 */
	public String getCurrPopName() {
		return currPopName;
	}

	/**
	 * @param currPopName the currPopName to set
	 */
	public void setCurrPopName(String currPopName) {
		this.currPopName = currPopName;
	}

	/**
	 * @return the isDepositior
	 */
	public boolean isDepositior() {
		return isDepositior;
	}

	/**
	 * @param isDepositior the isDepositior to set
	 */
	public void setDepositior(boolean isDepositior) {
		this.isDepositior = isDepositior;
	}

	/**
	 * @return the isDepositior Text
	 */
	public String getIsDepositiorTxt() {
		return isDepositior?"Yes":"No";
	}

	/**
	 * @return the isSalesDep
	 */
	public boolean isSalesDep() {
		return isSalesDep;
	}

	/**
	 * @param isSalesDep the isSalesDep to set
	 */
	public void setSalesDep(boolean isSalesDep) {
		this.isSalesDep = isSalesDep;
	}

	/**
	 * @return the isDepositior
	 */
	public String getIsSalesDepTxt() {
		return isSalesDep?"Yes":"No";
	}

	/**
	 * @return the populationEntryTypes
	 */
	public HashMap<Integer, String> getPopulationEntryTypes() {
		return populationEntryTypes;
	}

	/**
	 * @param populationEntryTypes the populationEntryTypes to set
	 */
	public void setPopulationEntryTypes(
			HashMap<Integer, String> populationEntryTypes) {
		this.populationEntryTypes = populationEntryTypes;
	}

	/**
	 * @return the regPopName
	 */
	public String getRegPopName() {
		return regPopName;
	}

	/**
	 * @param regPopName the regPopName to set
	 */
	public void setRegPopName(String regPopName) {
		this.regPopName = regPopName;
	}

	/**
	 * @return the salesDepRepName
	 */
	public String getSalesDepRepName() {
		return salesDepRepName;
	}

	/**
	 * @param salesDepRepName the salesDepRepName to set
	 */
	public void setSalesDepRepName(String salesDepRepName) {
		if (null == salesDepRepName){
			this.salesDepRepName ="";
		}else {
			this.salesDepRepName = salesDepRepName;
		}
	}

	/**
	 * @return the userId
	 */
	public long getUserId() {
		return userId;
	}

	/**
	 * @param userId the userId to set
	 */
	public void setUserId(long userId) {
		this.userId = userId;
	}


	/**
	 * @return the currEntryType Name
	 */
	public void setEntryType(int entryTypeId) {
		if (entryTypeId != 0){
			this.entryType = CommonUtil.getMessage(populationEntryTypes.get(entryTypeId),null);
		}else{
			this.entryType = "";
		}

	}

	/**
	 * @return the entryType
	 */
	public String getEntryType() {
		return entryType;
	}

	/**
	 * @return the actionTime
	 */
	public String getActionTime() {
		return actionTime;
	}

	/**
	 * @param actionTime the actionTime to set
	 */
	public void setActionTime(String actionTime) {
		if (null == actionTime){
			this.actionTime ="";
		}else {
			this.actionTime = actionTime;
		}
	}

	public void setActionType(String actionTypeKey) {
		if (CommonUtil.isParameterEmptyOrNull(actionTypeKey)){
			this.actionType = "";
		}else{
			this.actionType = CommonUtil.getMessage(actionTypeKey,null);
		}
	}

	public void setChannel(String channelKey) {
		if (CommonUtil.isParameterEmptyOrNull(channelKey)){
			this.channel =  "";
		}else {
			this.channel = CommonUtil.getMessage(channelKey,null);
		}
	}

	/**
	 * @return the comments
	 */
	public String getComments() {
		return comments;
	}

	/**
	 * @param comments the comments to set
	 */
	public void setComments(String comments) {
		if (null == comments){
			this.comments ="";
		}else {
			this.comments = comments;
		}
	}

	/**
	 * @return the currentPop
	 */
	public String getCurrentPop() {
		return currentPop;
	}

	/**
	 * @param currentPop the currentPop to set
	 */
	public void setCurrentPop(String currentPop) {
		if (null == currentPop){
			this.currentPop ="";
		}else {
			this.currentPop = currentPop;
		}
	}

	/**
	 * @return the department
	 */
	public String getDepartment() {
		return department;
	}

	/**
	 * @param department the department to set
	 */
	public void setDepartment(String department) {
		if (null == department){
			this.department ="";
		}else {
			this.department = department;
		}
	}

	/**
	 * @return the issueActionId
	 */
	public long getIssueActionId() {
		return issueActionId;
	}

	/**
	 * @param issueActionId the issueActionId to set
	 */
	public void setIssueActionId(long issueActionId) {
		this.issueActionId = issueActionId;
	}

	/**
	 * @return the originalPop
	 */
	public String getOriginalPop() {
		return originalPop;
	}

	/**
	 * @param originalPop the originalPop to set
	 */
	public void setOriginalPop(String originalPop) {
		this.originalPop = originalPop;
	}


	public void setReachedStatus(String reachedStatusKey) {
		if (CommonUtil.isParameterEmptyOrNull(reachedStatusKey)){
			this.reachedStatus = "";
		}else{
			this.reachedStatus = CommonUtil.getMessage(reachedStatusKey,null);
		}
	}

	/**
	 * @return the repName
	 */
	public String getRepName() {
		return repName;
	}

	/**
	 * @param repName the repName to set
	 */
	public void setRepName(String repName) {
		if (null == repName){
			this.repName ="";
		}else {
			this.repName = repName;
		}
	}

	/**
	 * @return the timeCreated
	 */
	public String getTimeCreated() {
		return timeCreated;
	}

	/**
	 * @param timeCreated the timeCreated to set
	 */
	public void setTimeCreated(String timeCreated) {
		this.timeCreated = timeCreated;
	}

	/**
	 * @return the subject
	 */
	public String getSubject() {
		return subject;
	}

	/**
	 * @param subject the subject to set
	 */
	public void setSubject(String subject) {
		this.subject = subject;
	}

	/**
	 * @return the actionType
	 */
	public String getActionType() {
		return actionType;
	}

	/**
	 * @return the channel
	 */
	public String getChannel() {
		return channel;
	}

	/**
	 * @return the reachedStatus
	 */
	public String getReachedStatus() {
		return reachedStatus;
	}

	/**
	 * @return the entryTypeActionType
	 */
	public String getEntryTypeActionType() {
		return entryTypeActionType;
	}

	/**
	 * @param entryTypeActionType the entryTypeActionType to set
	 */
	public void setEntryTypeActionType(String entryTypeActionTypeKey) {
		if (CommonUtil.isParameterEmptyOrNull(entryTypeActionTypeKey)){
			this.entryTypeActionType = "";
		}else{
			this.entryTypeActionType = CommonUtil.getMessage(entryTypeActionTypeKey,null);
		}
	}
}


