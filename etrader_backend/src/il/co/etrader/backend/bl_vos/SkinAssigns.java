/**
 *
 */
package il.co.etrader.backend.bl_vos;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Skin assigns
 *
 * @author Kobi
 *
 */
public class SkinAssigns implements Serializable {
	private static final long serialVersionUID = 5436390708967588068L;

	private long skinId;
	private String skinDescription;
	private ArrayList<AssignWriter> writers;
	private boolean assignAll;

	/**
	 * @return the skinDescription
	 */
	public String getSkinDescription() {
		return skinDescription;
	}

	/**
	 * @param skinDescription the skinDescription to set
	 */
	public void setSkinDescription(String skinDescription) {
		this.skinDescription = skinDescription;
	}

	/**
	 * @return the skinId
	 */
	public long getSkinId() {
		return skinId;
	}

	/**
	 * @param skinId the skinId to set
	 */
	public void setSkinId(long skinId) {
		this.skinId = skinId;
	}

	/**
	 * @return the writers
	 */
	public ArrayList<AssignWriter> getWriters() {
		return writers;
	}

	/**
	 * @param writers the writers to set
	 */
	public void setWriters(ArrayList<AssignWriter> writers) {
		this.writers = writers;
	}

	/**
	 * @return the assignAll
	 */
	public boolean isAssignAll() {
		return assignAll;
	}

	/**
	 * @param assignAll the assignAll to set
	 */
	public void setAssignAll(boolean assignAll) {
		this.assignAll = assignAll;
	}

	/**
	 * @param assignAll the assignAll to set
	 */
	public void setAssignAllWriters(boolean assignAll) {
		this.assignAll = assignAll;
		for (AssignWriter aw : writers) {
			aw.setAssign(assignAll);
		}
	}

	public boolean isHasAssignWriters() {
		for (AssignWriter aw : writers) {
			if (aw.isAssign()) {
				return true;
			}
		}
		return false;
	}

}
