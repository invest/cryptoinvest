package il.co.etrader.backend.bl_vos;

import il.co.etrader.backend.bl_managers.ApplicationData;
import il.co.etrader.backend.util.Constants;
import il.co.etrader.util.CommonUtil;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;

import javax.faces.context.FacesContext;


public class RegLeadsTotals implements Serializable {

	private static final long serialVersionUID = 8470665378624530897L;
	private final String NEW_LINE = " </br> ";
	private final String TAB = "&#160;&#160;&#160;";
	private HashMap<Integer, String> populationEntryTypes;
	private int entryTypesNum;

	private long campaignId;
	private String campaignName;
	private long combId;
	private long campaignEntries;
	private long callMeNum;
	private long regCallMeNum;
	private long shortRegMum;
	private long regShortRegNum;
	private long directRegNum;
	private long totalRegNum;
	private long firstRegDep;
	private ArrayList<EntryTypesDivision> shortRegEntryTypesDiv;
	private ArrayList<EntryTypesDivision> regShortRegEntryTypesDiv;
	private ArrayList<EntryTypesDivision> callMeEntryTypesDiv;
	private ArrayList<EntryTypesDivision> regCallMeEntryTypesDiv;
	private ArrayList<EntryTypesDivision> totalRegEntryTypesDiv;


	public RegLeadsTotals() {
		super();

		FacesContext context = FacesContext.getCurrentInstance();
		// Only for display entry types names on screen
		if (context != null){
			ApplicationData ap = (ApplicationData)context.getApplication().createValueBinding(Constants.BIND_APPLICATION_DATA).getValue(context);
			populationEntryTypes = ap.getPopulationEntryType();
			entryTypesNum =  populationEntryTypes.size() + 1;
		}

		shortRegEntryTypesDiv = new ArrayList<EntryTypesDivision>();
		regShortRegEntryTypesDiv = new ArrayList<EntryTypesDivision>();
		callMeEntryTypesDiv = new ArrayList<EntryTypesDivision>();
		regCallMeEntryTypesDiv = new ArrayList<EntryTypesDivision>();
		totalRegEntryTypesDiv = new ArrayList<EntryTypesDivision>();

		for (int i=0;i<entryTypesNum;++i){
			shortRegEntryTypesDiv.add(new EntryTypesDivision());
			regShortRegEntryTypesDiv.add(new EntryTypesDivision());
			callMeEntryTypesDiv.add(new EntryTypesDivision());
			regCallMeEntryTypesDiv.add(new EntryTypesDivision());
			totalRegEntryTypesDiv.add(new EntryTypesDivision());
		}
	}


	/**
	 * @return the callMeNum
	 */
	public long getCallMeNum() {
		return callMeNum;
	}
	/**
	 * @param callMeNum the callMeNum to set
	 */
	public void setCallMeNum(long callMeNum) {
		this.callMeNum = callMeNum;
	}
	/**
	 * @return the campaignEntries
	 */
	public long getCampaignEntries() {
		return campaignEntries;
	}
	/**
	 * @param campaignEntries the campaignEntries to set
	 */
	public void setCampaignEntries(long campaignEntries) {
		this.campaignEntries = campaignEntries;
	}

	/**
	 * @return the campaignId
	 */
	public long getCampaignId() {
		return campaignId;
	}
	/**
	 * @param campaignId the campaignId to set
	 */
	public void setCampaignId(long campaignId) {
		this.campaignId = campaignId;
	}
	/**
	 * @return the combId
	 */
	public long getCombId() {
		return combId;
	}
	/**
	 * @param combId the combId to set
	 */
	public void setCombId(long combId) {
		this.combId = combId;
	}
	/**
	 * @return the directRegNum
	 */
	public long getDirectRegNum() {
		return directRegNum;
	}
	/**
	 * @param directRegNum the directRegNum to set
	 */
	public void setDirectRegNum(long directRegNum) {
		this.directRegNum = directRegNum;
	}
	/**
	 * @return the firstRegDep
	 */
	public long getFirstRegDep() {
		return firstRegDep;
	}
	/**
	 * @param firstRegDep the firstRegDep to set
	 */
	public void setFirstRegDep(long firstRegDep) {
		this.firstRegDep = firstRegDep;
	}
	/**
	 * @return the regCallMeNum
	 */
	public long getRegCallMeNum() {
		return regCallMeNum;
	}
	/**
	 * @param regCallMeNum the regCallMeNum to set
	 */
	public void setRegCallMeNum(long regCallMeNum) {
		this.regCallMeNum = regCallMeNum;
	}

	/**
	 * @return the shortRegMum
	 */
	public long getShortRegMum() {
		return shortRegMum;
	}
	/**
	 * @param shortRegMum the shortRegMum to set
	 */
	public void setShortRegMum(long shortRegMum) {
		this.shortRegMum = shortRegMum;
	}
	/**
	 * @return the totalRegNum
	 */
	public long getTotalRegNum() {
		return totalRegNum;
	}
	/**
	 * @param totalRegNum the totalRegNum to set
	 */
	public void setTotalRegNum(long totalRegNum) {
		this.totalRegNum = totalRegNum;
	}
	/**
	 * @return the regShortRegNum
	 */
	public long getRegShortRegNum() {
		return regShortRegNum;
	}
	/**
	 * @param regShortRegNum the regShortRegNum to set
	 */
	public void setRegShortRegNum(long regShortRegNum) {
		this.regShortRegNum = regShortRegNum;
	}
	/**
	 * @return the campaignName
	 */
	public String getCampaignName() {
		return campaignName;
	}
	/**
	 * @param campaignName the campaignName to set
	 */
	public void setCampaignName(String campaignName) {
		this.campaignName = campaignName;
	}
	/**
	 * @return the callMeEntryTypesDiv
	 */
	public ArrayList<EntryTypesDivision> getCallMeEntryTypesDiv() {
		return callMeEntryTypesDiv;
	}
	/**
	 * @param callMeEntryTypesDiv the callMeEntryTypesDiv to set
	 */
	public void setCallMeEntryTypesDiv(
			ArrayList<EntryTypesDivision> callMeEntryTypesDiv) {
		this.callMeEntryTypesDiv = callMeEntryTypesDiv;
	}
	/**
	 * @return the shortRegEntryTypesDiv
	 */
	public ArrayList<EntryTypesDivision> getShortRegEntryTypesDiv() {
		return shortRegEntryTypesDiv;
	}
	/**
	 * @param shortRegEntryTypesDiv the shortRegEntryTypesDiv to set
	 */
	public void setShortRegEntryTypesDiv(
			ArrayList<EntryTypesDivision> shortRegEntryTypesDiv) {
		this.shortRegEntryTypesDiv = shortRegEntryTypesDiv;
	}
	/**
	 * @return the totalRegEntryTypesDiv
	 */
	public ArrayList<EntryTypesDivision> getTotalRegEntryTypesDiv() {
		return totalRegEntryTypesDiv;
	}
	/**
	 * @param totalRegEntryTypesDiv the totalRegEntryTypesDiv to set
	 */
	public void setTotalRegEntryTypesDiv(
			ArrayList<EntryTypesDivision> totalRegEntryTypesDiv) {
		this.totalRegEntryTypesDiv = totalRegEntryTypesDiv;
	}
	/**
	 * @return the regCallMeEntryTypesDiv
	 */
	public ArrayList<EntryTypesDivision> getRegCallMeEntryTypesDiv() {
		return regCallMeEntryTypesDiv;
	}
	/**
	 * @param regCallMeEntryTypesDiv the regCallMeEntryTypesDiv to set
	 */
	public void setRegCallMeEntryTypesDiv(
			ArrayList<EntryTypesDivision> regCallMeEntryTypesDiv) {
		this.regCallMeEntryTypesDiv = regCallMeEntryTypesDiv;
	}
	/**
	 * @return the regShortRegEntryTypesDiv
	 */
	public ArrayList<EntryTypesDivision> getRegShortRegEntryTypesDiv() {
		return regShortRegEntryTypesDiv;
	}
	/**
	 * @param regShortRegEntryTypesDiv the regShortRegEntryTypesDiv to set
	 */
	public void setRegShortRegEntryTypesDiv(
			ArrayList<EntryTypesDivision> regShortRegEntryTypesDiv) {
		this.regShortRegEntryTypesDiv = regShortRegEntryTypesDiv;
	}

	public String getCallMeNumTitle(){

		String regStr = "Registered Call Me leads: " + regCallMeNum + NEW_LINE;
		String nonRegStr = "Non Registered Call Me leads: " + (callMeNum - regCallMeNum) + NEW_LINE;
		String entryTypeName;

		for (int i=0; i<entryTypesNum;++i){

			if (i==0){
				entryTypeName = "No Entry Type";
			}else{
				entryTypeName = CommonUtil.getMessage(populationEntryTypes.get(i),null);
			}

			int regCallMeNumLeads = regCallMeEntryTypesDiv.get(i).getEntryTypesLeadsNum();
			int nonRegCallMeNumLeads = callMeEntryTypesDiv.get(i).getEntryTypesLeadsNum() - regCallMeNumLeads;

			if (regCallMeNumLeads > 0){
				regStr += TAB + entryTypeName + ": " + regCallMeNumLeads + NEW_LINE;
			}
			if (nonRegCallMeNumLeads > 0){
				nonRegStr += TAB + entryTypeName + ": " +  nonRegCallMeNumLeads + NEW_LINE;
			}
		}
		return regStr + NEW_LINE + nonRegStr;
	}

	public String getShortRegNumTitle(){

		String regStr = "Registered Short Reg leads: " + regShortRegNum + NEW_LINE;
		String nonRegStr = "Non Registered Short Reg leads: " + (shortRegMum - regShortRegNum) + NEW_LINE;
		String entryTypeName;

		for (int i=0; i<entryTypesNum;++i){

			if (i==0){
				entryTypeName = "No Entry Type";
			}else{
				entryTypeName = CommonUtil.getMessage(populationEntryTypes.get(i),null);
			}

			int regShortRegNumLeads = regShortRegEntryTypesDiv.get(i).getEntryTypesLeadsNum();
			int nonRegShortRegNumLeads = shortRegEntryTypesDiv.get(i).getEntryTypesLeadsNum() - regShortRegNumLeads;

			if (regShortRegNumLeads > 0){
				regStr += TAB + entryTypeName + ": " + regShortRegNumLeads + NEW_LINE;
			}
			if (nonRegShortRegNumLeads > 0){
				nonRegStr += TAB + entryTypeName + ": " +  nonRegShortRegNumLeads + NEW_LINE;
			}
		}
		return regStr + NEW_LINE + nonRegStr;
	}

	public String getUsersNumTitle(){

		String titleStr = "Users Num: " + totalRegNum + NEW_LINE;
		String entryTypeName;

		for (int i=0; i<entryTypesNum;++i){

			if (i==0){
				entryTypeName = "No Entry Type";
			}else{
				entryTypeName = CommonUtil.getMessage(populationEntryTypes.get(i),null);
			}

			EntryTypesDivision userNumEntryDiv = totalRegEntryTypesDiv.get(i);
			int usersNumLeads = userNumEntryDiv.getEntryTypesLeadsNum();

			if (usersNumLeads > 0){
				titleStr += TAB + entryTypeName + ": " + usersNumLeads + NEW_LINE;
				titleStr += TAB + TAB + "Depositors Num: " + userNumEntryDiv.getDepositorsNum() + NEW_LINE;
				titleStr += TAB + TAB + "Non Depositors Num: " + userNumEntryDiv.getNonDepositorsNum() + NEW_LINE;
			}

		}
		return titleStr;
	}
}


