package il.co.etrader.backend.bl_vos;

import il.co.etrader.backend.util.Constants;
import il.co.etrader.bl_vos.Writer;

import java.io.Serializable;
import java.util.Date;

import javax.faces.context.FacesContext;

import com.copyop.common.enums.CoinsActionTypeEnum;

/**
 * @author EranL
 *
 */
public class CopyopCoins implements Serializable {

	private static final long serialVersionUID = 1L;
	private String id;
	private long writerId;
	private Date timeCreated;
	private int actionTypeId;
	private boolean isReGranted;
	private long coins;
	private long amount;
	private String amountWF; 
		
	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}
	/**
	 * @return the writerId
	 */
	public long getWriterId() {
		return writerId;
	}
	/**
	 * @param writerId the writerId to set
	 */
	public void setWriterId(long writerId) {
		this.writerId = writerId;
	}
	/**
	 * @return the timeCreated
	 */
	public Date getTimeCreated() {
		return timeCreated;
	}
	/**
	 * @param timeCreated the timeCreated to set
	 */
	public void setTimeCreated(Date timeCreated) {
		this.timeCreated = timeCreated;
	}

	/**
	 * @return the actionTypeId
	 */
	public int getActionTypeId() {
		return actionTypeId;
	}
	/**
	 * @param actionTypeId the actionTypeId to set
	 */
	public void setActionTypeId(int actionTypeId) {
		this.actionTypeId = actionTypeId;
	}

	/**
	 * @return the isReGranted
	 */
	public boolean isReGranted() {
		return isReGranted;
	}
	/**
	 * @param isReGranted the isReGranted to set
	 */
	public void setReGranted(boolean isReGranted) {
		this.isReGranted = isReGranted;
	}
	/**
	 * @return the coins
	 */
	public long getCoins() {
		return coins;
	}
	/**
	 * @param coins the coins to set
	 */
	public void setCoins(long coins) {
		this.coins = coins;
	}	
	/**
	 * @return the amount
	 */
	public long getAmount() {
		return amount;
	}
	/**
	 * @param amount the amount to set
	 */
	public void setAmount(long amount) {
		this.amount = amount;
	}
	public String getActionTypeTxt() {
		return CoinsActionTypeEnum.getById(actionTypeId).getDisplayName();
	}
	
	/**
	 * @return the amountWF
	 */
	public String getAmountWF() {
		return amountWF;
	}
	/**
	 * @param amountWF the amountWF to set
	 */
	public void setAmountWF(String amountWF) {
		this.amountWF = amountWF;
	}
	public String getWriterNameById() {
		String writerName = Constants.EMPTY_STRING;
		FacesContext context = FacesContext.getCurrentInstance();
		WriterWrapper wr = (WriterWrapper)context.getApplication().createValueBinding(Constants.BIND_WRITER_WRAPPER).getValue(context);
		Writer writer = wr.getAllWritersMap().get(writerId);		
		if (writer != null) {
			writerName = writer.getUserName();
		}
		return writerName;
	}
	
}
