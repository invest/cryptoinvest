package il.co.etrader.backend.bl_vos;


public class OpportunityOddsGroup {
	private long oddsGroupID;
	private String oddsGroup;
	private String oddsGroupValue;
	private String oddsGroupDefault;
	private String defaultPair;
	/**
	 * @return the oddsGroupID
	 */
	public long getOddsGroupID() {
		return oddsGroupID;
	}
	/**
	 * @param oddsGroupID the oddsGroupID to set
	 */
	public void setOddsGroupID(long oddsGroupID) {
		this.oddsGroupID = oddsGroupID;
	}
	/**
	 * @return the oddsGroup
	 */
	public String getOddsGroup() {	    
		return oddsGroup;
	}
	/**
	 * @param oddsGroup the oddsGroup to set
	 */
	public void setOddsGroup(String oddsGroup) {
		this.oddsGroup = oddsGroup;
	}
	/**
	 * @return the oddsGroupValue
	 */
	public String getOddsGroupValue() {
		return oddsGroupValue;
	}
	/**
	 * @param oddsGroupValue the oddsGroupValue to set
	 */
	public void setOddsGroupValue(String oddsGroupValue) {
		this.oddsGroupValue = oddsGroupValue;
	}
	public String getOddsGroupDefault() {
		return oddsGroupDefault;
	}
	public void setOddsGroupDefault(String oddsGroupDefault) {
		this.oddsGroupDefault = oddsGroupDefault;
	}
	public String getDefaultPair() {
		return defaultPair;
	}
	public void setDefaultPair(String defaultPair) {
		this.defaultPair = defaultPair;
	}

}
