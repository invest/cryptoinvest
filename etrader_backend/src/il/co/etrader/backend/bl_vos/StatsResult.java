package il.co.etrader.backend.bl_vos;

import il.co.etrader.backend.util.Utils;
import il.co.etrader.util.CommonUtil;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class StatsResult {

	private String groupName;
	private String oppType;

	private long turnover;
	private long customerProfit;
	private double profitRatio;

	private long investmentsNumber;
	private long successInvestmentsNumber;
	private double successInvestmentsRatio;

	private long totalCustomerProfit;
	private long totalInvestmentsNumber;
	private Date date1;
	private Date date2;


	public String getCustomerProfitTxt() {
		return CommonUtil.displayAmount(customerProfit, Utils.getUser().getCurrencyId());
	}

	public void setCustomerProfit(long customerProfit) {
		this.customerProfit = customerProfit;
	}

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	public long getInvestmentsNumber() {
		return investmentsNumber;
	}

	public void setInvestmentsNumber(long investmentsNumber) {
		this.investmentsNumber = investmentsNumber;
	}

	public String getProfitRatioTxt() {
		NumberFormat format = new DecimalFormat("0%");
		return format.format(profitRatio);
	}

	public void setProfitRatio(double profitRatio) {
		this.profitRatio = profitRatio;
	}

	public long getSuccessInvestmentsNumber() {
		return successInvestmentsNumber;
	}

	public void setSuccessInvestmentsNumber(long successInvestmentsNumber) {
		this.successInvestmentsNumber = successInvestmentsNumber;
	}

	public String getSuccessInvestmentsRatioTxt() {
		NumberFormat format = new DecimalFormat("0%");
		return format.format(successInvestmentsRatio);
	}

	public void setSuccessInvestmentsRatio(double successInvestmentsRatio) {
		this.successInvestmentsRatio = successInvestmentsRatio;
	}

	public String getTurnoverTxt() {
		return CommonUtil.displayAmount(turnover, Utils.getUser().getCurrencyId());
	}

	public void setTurnover(long turnOver) {
		this.turnover = turnOver;
	}

	public String getDate1Txt() {
		SimpleDateFormat sd = new SimpleDateFormat("dd.MM.yyyy");
		return sd.format(date1);
	}

	public Date getDate1() {
		return date1;
	}

	public void setDate1(Date date1) {
		this.date1 = date1;
	}

	public String getDaysNumber() {
		return String.valueOf
			((CommonUtil.getStartOfDay(date2).getTime() - CommonUtil.getStartOfDay(date1).getTime())
					/ (1000 * 60 * 60 * 24) + 1);
	}

	public String getDate2Txt() {
		SimpleDateFormat sd = new SimpleDateFormat("dd.MM.yyyy");
		return sd.format(date2);
	}

	public Date getDate2() {
		return date2;
	}

	public void setDate2(Date date2) {
		this.date2 = date2;
	}

	public long getCustomerProfit() {
		return customerProfit;
	}

	public long getTotalCustomerProfit() {
		return totalCustomerProfit;
	}

	public void setTotalCustomerProfit(long totalCustomerProfit) {
		this.totalCustomerProfit = totalCustomerProfit;
	}

	public String getTotalCustomerProfitTxt() {
		return CommonUtil.displayAmount(totalCustomerProfit, Utils.getUser().getCurrencyId());
	}

	public long getTotalInvestmentsNumber() {
		return totalInvestmentsNumber;
	}

	public void setTotalInvestmentsNumber(long totalInvestmentsNumber) {
		this.totalInvestmentsNumber = totalInvestmentsNumber;
	}

	public String getOppType() {
		return oppType;
	}

	public void setOppType(String typeName) {
		this.oppType = typeName;
	}
}
