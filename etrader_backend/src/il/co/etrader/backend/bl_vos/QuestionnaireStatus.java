package il.co.etrader.backend.bl_vos;

import java.io.Serializable;
import java.util.Date;

public class QuestionnaireStatus implements Serializable {

    private static final long serialVersionUID = -8949100944949954658L;

    long userId;
    String userName;
    String mobilePhone;
    String landLinePhone;
    String skinName;
    Date firstDepositDate;
    int daysUntilSuspend;
    Date lastTimeReached;
    String lastCaller;
    boolean userNotInterested = false;
    Date lastTimeCall;

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getSkinName() {
        return skinName;
    }

    public void setSkinName(String skinName) {
        this.skinName = skinName;
    }

    public String getMobilePhone() {
        return mobilePhone;
    }

    public void setMobilePhone(String mobilePhone) {
        this.mobilePhone = mobilePhone;
    }

    public String getLandLinePhone() {
        return landLinePhone;
    }

    public void setLandLinePhone(String landLinePhone) {
        this.landLinePhone = landLinePhone;
    }

    public Date getFirstDepositDate() {
        return firstDepositDate;
    }

    public void setFirstDepositDate(Date firstDepositDate) {
        this.firstDepositDate = firstDepositDate;
    }

    public int getDaysUntilSuspend() {
        return daysUntilSuspend;
    }

    public void setDaysUntilSuspend(int daysUntilSuspend) {
        this.daysUntilSuspend = daysUntilSuspend;
    }

    public Date getLastTimeReached() {
        return lastTimeReached;
    }

    public void setLastTimeReached(Date lastTimeReached) {
        this.lastTimeReached = lastTimeReached;
    }

    public String getLastCaller() {
        return lastCaller;
    }

    public void setLastCaller(String lastCaller) {
        this.lastCaller = lastCaller;
    }

    public boolean isUserNotInterested() {
        return userNotInterested;
    }

    public void setUserNotInterested(boolean userNotInterested) {
        this.userNotInterested = userNotInterested;
    }

	public Date getLastTimeCall() {
		return lastTimeCall;
	}

	public void setLastTimeCall(Date lastTimeCall) {
		this.lastTimeCall = lastTimeCall;
	}
    
}
