package il.co.etrader.backend.bl_vos;

import java.util.Date;

import com.anyoption.common.beans.Investment;

import il.co.etrader.util.CommonUtil;
import il.co.etrader.util.ConstantsBase;
import il.co.etrader.util.InvestmentFormatter;

public class InvestmentTotal extends Investment {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8301735409014221909L;
	protected long totalAmount;
	protected long totalRefund;
	protected long baseTotalAmount;
	protected long baseTotalRefund;
	protected double totalPrice;
	protected long totalNumOfContracts;
	protected double totalAbove;
	protected double baseTotalAbove;
	protected double totalBelow;
	protected double baseTotalBelow;
	protected long totalWinLose;
	protected long baseTotalWinLose;
	
	public InvestmentTotal(int totalLine) {
		super(totalLine);
	}

	public InvestmentTotal(int totalLine, Date timeCreated, String utcffsetCreated, int id, long currencyId, long amount) {
		this.totalLine = totalLine;
		setTimeCreated(timeCreated);
		setUtcOffsetCreated(utcffsetCreated);
		setId(id);
		setCurrencyId(currencyId);
		setAmount(amount);
	}

	public String getTotalAmountTxt() {
		return CommonUtil.displayAmount(totalAmount, currencyId);
	}

	public String getTotalRefundTxt() {
		return CommonUtil.displayAmount(totalRefund, currencyId);
	}

	public String getTotalProfitTxt() {
		long totalProfit = totalAmount - totalRefund;
		return CommonUtil.displayAmount(totalProfit, currencyId);
	}

	public String getBaseTotalAmountTxt() {
		return CommonUtil.displayAmount(baseTotalAmount,ConstantsBase.CURRENCY_BASE_ID);
	}

	public String getBaseTotalRefundTxt() {
		return CommonUtil.displayAmount(baseTotalRefund,ConstantsBase.CURRENCY_BASE_ID);
	}

	public String getBaseTotalProfitTxt() {
		long totalProfit = baseTotalAmount- baseTotalRefund;
		return CommonUtil.displayAmount(totalProfit,ConstantsBase.CURRENCY_BASE_ID);
	}

	public boolean getIsProfitPositiveTxt() {
		return (totalAmount - totalRefund) > 0;
	}

	public String getStyleColor() {
		return InvestmentFormatter.getStyleColor(totalAmount - totalRefund);
	}

	public String getBaseStyleColor() {
		return InvestmentFormatter.getStyleColor(baseTotalAmount - baseTotalRefund);
	}
	
	public String getTotalWinLoseStyleColor() {
		return InvestmentFormatter.getStyleColor(totalWinLose);
	}
	
	public String getCustomerTotalWinLoseStyleColor() {
		return InvestmentFormatter.getStyleColor((totalWinLose * (-1)) + totalAmount );
	}

	public String getBaseTotalWinLoseStyleColor() {
		return InvestmentFormatter.getStyleColor(baseTotalWinLose);
	}
	
	public String getCustomerBaseTotalWinLoseStyleColor() {
		return InvestmentFormatter.getStyleColor((baseTotalWinLose * (-1)) + baseTotalAmount);
	}
	
	public String getTotalAboveTxt() {
		return CommonUtil.displayAmount(totalAbove, true, currencyId);
	}
	
	public String getBaseTotalAboveTxt() {
		return CommonUtil.displayAmount(baseTotalAbove, true, ConstantsBase.CURRENCY_BASE_ID);
	}
	
	public String getTotalBelowTxt() {
		return CommonUtil.displayAmount(totalBelow, true, currencyId);
	}
	
	public String getBaseTotalBelowTxt() {
		return CommonUtil.displayAmount(baseTotalBelow, true, ConstantsBase.CURRENCY_BASE_ID);
	}
	
	public String getTotalWinLoseTxt() {
		return CommonUtil.displayAmount(totalWinLose, currencyId);
	}
	
	public String getCustomerTotalWinLoseTxt() {
		return CommonUtil.displayAmount((totalWinLose * (-1)) + totalAmount, currencyId);
	}
	
	public String getBaseTotalWinLoseTxt() {
		return CommonUtil.displayAmount(baseTotalWinLose, currencyId);
	}
	
	public String getCustomerBaseTotalWinLoseTxt() {
		return CommonUtil.displayAmount((baseTotalWinLose * (-1)) + baseTotalAmount, currencyId);
	}

	/**
	 * @return the baseTotalAmount
	 */
	public long getBaseTotalAmount() {
		return baseTotalAmount;
	}

	/**
	 * @param baseTotalAmount the baseTotalAmount to set
	 */
	public void setBaseTotalAmount(long baseTotalAmount) {
		this.baseTotalAmount = baseTotalAmount;
	}

	/**
	 * @return the baseTotalRefund
	 */
	public long getBaseTotalRefund() {
		return baseTotalRefund;
	}

	/**
	 * @param baseTotalRefund the baseTotalRefund to set
	 */
	public void setBaseTotalRefund(long baseTotalRefund) {
		this.baseTotalRefund = baseTotalRefund;
	}

	/**
	 * @return the totalAmount
	 */
	public long getTotalAmount() {
		return totalAmount;
	}

	/**
	 * @param totalAmount the totalAmount to set
	 */
	public void setTotalAmount(long totalAmount) {
		this.totalAmount = totalAmount;
	}

	/**
	 * @return the totalRefund
	 */
	public long getTotalRefund() {
		return totalRefund;
	}

	/**
	 * @param totalRefund the totalRefund to set
	 */
	public void setTotalRefund(long totalRefund) {
		this.totalRefund = totalRefund;
	}

	/**
	 * @return the totalPrice
	 */
	public double getTotalPrice() {
		return totalPrice;
	}
	
	public String getTotalPriceTxt() {
		return CommonUtil.displayAmount(totalPrice,false,1);
	}

	/**
	 * @param totalPrice the totalPrice to set
	 */
	public void setTotalPrice(double totalPrice) {
		this.totalPrice = totalPrice;
	}

	/**
	 * @return the numOfContracts
	 */
	public long getTotalNumOfContracts() {
		return totalNumOfContracts;
	}

	/**
	 * @param numOfContracts the numOfContracts to set
	 */
	public void setTotalNumOfContracts(long totalNumOfContracts) {
		this.totalNumOfContracts = totalNumOfContracts;
	}

	/**
	 * @return the totalAbove
	 */
	public double getTotalAbove() {
		return totalAbove;
	}

	/**
	 * @param totalAbove the totalAbove to set
	 */
	public void setTotalAbove(double totalAbove) {
		this.totalAbove = totalAbove;
	}

	/**
	 * @return the baseTotalAbove
	 */
	public double getBaseTotalAbove() {
		return baseTotalAbove;
	}

	/**
	 * @param baseTotalAbove the baseTotalAbove to set
	 */
	public void setBaseTotalAbove(double baseTotalAbove) {
		this.baseTotalAbove = baseTotalAbove;
	}

	/**
	 * @return the totalBelow
	 */
	public double getTotalBelow() {
		return totalBelow;
	}

	/**
	 * @param totalBelow the totalBelow to set
	 */
	public void setTotalBelow(double totalBelow) {
		this.totalBelow = totalBelow;
	}

	/**
	 * @return the baseTotalBelow
	 */
	public double getBaseTotalBelow() {
		return baseTotalBelow;
	}

	/**
	 * @param baseTotalBelow the baseTotalBelow to set
	 */
	public void setBaseTotalBelow(double baseTotalBelow) {
		this.baseTotalBelow = baseTotalBelow;
	}

	/**
	 * @return the totalWinLose
	 */
	public long getTotalWinLose() {
		return totalWinLose;
	}

	/**
	 * @param totalWinLose the totalWinLose to set
	 */
	public void setTotalWinLose(long totalWinLose) {
		this.totalWinLose = totalWinLose;
	}

	/**
	 * @return the baseTotalWinLose
	 */
	public long getBaseTotalWinLose() {
		return baseTotalWinLose;
	}

	/**
	 * @param baseTotalWinLose the baseTotalWinLose to set
	 */
	public void setBaseTotalWinLose(long baseTotalWinLose) {
		this.baseTotalWinLose = baseTotalWinLose;
	}

}
