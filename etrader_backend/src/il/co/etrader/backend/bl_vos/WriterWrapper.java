package il.co.etrader.backend.bl_vos;

import java.io.Serializable;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.IssueAction;
import com.anyoption.common.beans.IssueComponent;
import com.anyoption.common.beans.PaymentMethod;
import com.anyoption.common.beans.base.Opportunity;
import com.anyoption.common.bl_vos.ChargeBack;
import com.anyoption.common.bl_vos.InvestmentType;
import com.anyoption.common.enums.AssetIndexGroupTypeByName;
import com.anyoption.common.managers.CountryManagerBase;

import il.co.etrader.backend.bl_managers.AdminManager;
import il.co.etrader.backend.bl_managers.ApplicationData;
import il.co.etrader.backend.bl_managers.BonusManager;
import il.co.etrader.backend.bl_managers.IssuesManager;
import il.co.etrader.backend.bl_managers.MailBoxTemplatesManager;
import il.co.etrader.backend.bl_managers.MarketingManager;
import il.co.etrader.backend.bl_managers.PopulationEntriesManager;
import il.co.etrader.backend.bl_managers.PopulationsManager;
import il.co.etrader.backend.bl_managers.TransactionsManager;
import il.co.etrader.backend.bl_managers.UsersManager;
import il.co.etrader.backend.bl_managers.WritersManager;
import il.co.etrader.backend.dao_managers.MarketsManager;
import il.co.etrader.backend.util.Constants;
import il.co.etrader.backend.util.Utils;
import il.co.etrader.bl_managers.CacheObjects;
import il.co.etrader.bl_managers.InvestmentsManagerBase;
import il.co.etrader.bl_managers.IssuesManagerBase;
import il.co.etrader.bl_managers.PopulationsManagerBase;
import il.co.etrader.bl_managers.RiskAlertsManagerBase;
import il.co.etrader.bl_managers.UsersManagerBase;
import il.co.etrader.bl_vos.FilesRiskStatus;
import il.co.etrader.bl_vos.MarketingAffilate;
import il.co.etrader.bl_vos.MarketingCampaign;
import il.co.etrader.bl_vos.MarketingCampaignManagers;
import il.co.etrader.bl_vos.MarketingContent;
import il.co.etrader.bl_vos.MarketingLandingPage;
import il.co.etrader.bl_vos.MarketingLocation;
import il.co.etrader.bl_vos.MarketingMedium;
import il.co.etrader.bl_vos.MarketingPixel;
import il.co.etrader.bl_vos.MarketingSize;
import il.co.etrader.bl_vos.MarketingSource;
import il.co.etrader.bl_vos.MarketingType;
import il.co.etrader.bl_vos.Population;
import il.co.etrader.bl_vos.PopulationType;
import il.co.etrader.bl_vos.Skins;
import il.co.etrader.bl_vos.Writer;
import il.co.etrader.issueActions.IssueActionType;
import il.co.etrader.util.CommonUtil;
import il.co.etrader.util.CommonUtil.selectItemComparator;
import il.co.etrader.util.ConstantsBase;

public class WriterWrapper implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7259837570140265518L;
	private static final Logger logger = Logger.getLogger(WriterWrapper.class);
	protected static CacheObjects cache;
	protected LocalInfo localInfo;
	protected ArrayList<Integer> skins;
	protected Writer writer;
	protected boolean isEtrader;
	private String UtcOffset;

	private HashMap<Long,Writer> allWritersMap;

	//Drop downs
	private ArrayList<SelectItem> issueSubjectsSI;
	private ArrayList<SelectItem> issueReachedStatusesSI;
	private ArrayList<SelectItem> issueActionTypesSI;
	private ArrayList<SelectItem> issuePrioritiesSI;
	private ArrayList<SelectItem> issueStatusesSI;
	private ArrayList<SelectItem> issueChannelsSI;
	private ArrayList<SelectItem> fileTypesSI;
	private ArrayList<SelectItem> fraudTypesSI;
	//private ArrayList<SelectItem> marketsAndGroupsSI;
    //private ArrayList<SelectItem> activeMarketsAndGroupsSI;
    //private ArrayList<SelectItem> activeBinary0100MarketsAndGroupsSI; 
    //private ArrayList<SelectItem> activeMarketsAndGroupsWithOneTouchSI; 
	//private ArrayList<SelectItem> marketsByAgreementSI; 
	private ArrayList<SelectItem> marketsByAgreementWithAllSI;
	//private ArrayList<SelectItem> marketsSI; 
	//private ArrayList<SelectItem> marketsZeroOneHundredSI;
	//private ArrayList<SelectItem> marketGroupsSI; 
	//private ArrayList<SelectItem> activeOptionPlusMarketsSI; 
	private ArrayList<SelectItem> skinsSI;
	private ArrayList<SelectItem> skinsRegulatedSI;
	private ArrayList<SelectItem> skinsNotRegulatedSI;
	private ArrayList<SelectItem> currencySI;
	private ArrayList<SelectItem> investmentStatusesSI;
	private ArrayList<SelectItem> transactionTypesSI;
	private ArrayList<SelectItem> currenciesSI;
	private ArrayList<SelectItem> languagesSI;
	private ArrayList<SelectItem> chargebackStatusesSI;
	private ArrayList<SelectItem> oddsTypesSI;
	private ArrayList<SelectItem> exchangesSI;
	private ArrayList<SelectItem> investmentLimitGroupsSI;
	private ArrayList<SelectItem> opportunityTypesSI;
	private ArrayList<SelectItem> userClassesSI;
	private ArrayList<SelectItem> userClassesStringSI; //used in forms where classId member is a String
	private ArrayList<SelectItem> timezoneTypesSI;
	private ArrayList<SelectItem> yesNoSI;
	private ArrayList<SelectItem> yesNoSIAll;
	private ArrayList<SelectItem> ccDepositsStatusSI;
	private ArrayList<SelectItem> depositsStatusSI;
	private ArrayList<SelectItem> marketingPaymentTypesSI;
	private ArrayList<SelectItem> marketingChannelsTypesSI;
	private ArrayList<SelectItem> populationTypesSI;
	private ArrayList<SelectItem> callDirectionSI;
	private ArrayList<SelectItem> populationEntryEntitySI;
	private ArrayList<SelectItem> populationsSI;
	private ArrayList<SelectItem> popEntriesWritersFilterSI;
    private ArrayList<SelectItem> bonusTypesSI;
    private ArrayList<SelectItem> bonusStatesSI;
    private ArrayList<SelectItem> pixelTypesSI;
    private ArrayList<SelectItem> skinBusinessCasesSI;
    private ArrayList<SelectItem> marketingSourcesSI;
    private ArrayList<SelectItem> providersSI;
    private ArrayList<SelectItem> ccProvidersSI;
    private ArrayList<SelectItem> timeZoneRetentionSI;
    private ArrayList<SelectItem> popTypeDecline;
    private ArrayList<SelectItem> salesDepositsTypes;
    private ArrayList<SelectItem> salesDepositsSources;
    private ArrayList<SelectItem> historyWriters;
    private ArrayList<SelectItem> salesQualifiedFilter;
    private ArrayList<SelectItem> wonLostAllSI;
    private ArrayList<SelectItem> countriesGroup;
    private HashMap<Long, MarketingAffilate> affiliatesHM;
    private HashMap<Long, Writer> salesReps;
    private HashMap<Long, Writer> salesConversionReps;
    private ArrayList<SelectItem> fetchRowNumCount;
    private ArrayList<SelectItem> customerReportTypeSI;

	private HashMap<Long, Writer> salesRetentionReps;
    private HashMap<Long, PopulationType> populationTypes;
    private HashMap<Integer, HashMap<Long, ArrayList<IssueActionType>>> IssueActionType;
    private HashMap<Long, FilesRiskStatus> filesRiskStatusHM;
    private ArrayList<SelectItem> mailBoxSenders;
    public static ArrayList<SelectItem> mailboxPopupTypes;
    private ArrayList<SelectItem> affiliatesNamesAndKeysSI;
    private ArrayList<SelectItem> riskReasonSI;
    private ArrayList<SelectItem> filesRiskStatusesSI;
    //private ArrayList<SelectItem> paymentMethodsSI;
    private ArrayList<SelectItem> issueTypesSI;
    private ArrayList<SelectItem> riskIssueTypesSI;
    private ArrayList<SelectItem> wirebanksEtraderSI;
    private ArrayList<SelectItem> wirebanksAnyoptionSI;
    //private ArrayList<SelectItem> marketingCombinationsSI;
    //private ArrayList<SelectItem> marketingCampaignsSI;
    //private ArrayList<SelectItem> marketingCampaignsSIByWriterID;
    //private ArrayList<SelectItem> marketingCampaignsBySkinsSI;
    //private ArrayList<SelectItem> marketingCampaignsSalesSI;
    private ArrayList<SelectItem> activeUsersSI;
    private ArrayList<SelectItem> mediaBuyersSI;
    private ArrayList<SelectItem> siteSI;

    private ArrayList<SelectItem> salesRepsConversionDepSI;
    private ArrayList<SelectItem> salesRepsSI;
    private ArrayList<SelectItem> withdrawalTypesSI;
    private ArrayList<SelectItem> rewardActions;

    private ArrayList<SelectItem> marketingUrlSourceSI;
    private ArrayList<SelectItem> assetIndexGroupsSI;
    private ArrayList<SelectItem> issueComponentSI;
    private HashMap<Long, IssueComponent> issueComponentsHM;
    private ArrayList<SelectItem> copyopCoinsStateSI;
    private ArrayList<SelectItem> copyopInvTypeSI;
    private ArrayList<SelectItem> invWritersSI;
    private ArrayList<SelectItem> writersGroupsSI;
    //private ArrayList<SelectItem> affiliatesSI;
    private ArrayList<SelectItem> skinsGroupByLanguageSI;
    private ArrayList<SelectItem> staticLandingPagePathSI;
    private HashMap<Long, StaticLandingPagePath> staticLandingPagePathHM;
    private ArrayList<SelectItem> lastLoginSI;
    
    private ArrayList<SelectItem> userHistoryFieldsSI;
        	
    protected boolean hasEtrader;

	private final int ONLY_ONE_LANGUAGE = 1;

	 public static Map<String, ArrayList<SelectItem>> selectItems ;

	
	/**
	 * This method returns true if the writer is an etrader writer. etrader writer will have only 1 skin which is hebrew.
	 * @return whether the writer is an etrader writer.
	 * @throws SQLException
	 */
	private boolean isEtraderWriter() throws SQLException {
		boolean res = false;
		ArrayList<Long> languagesList = WritersManager.getWriterLanguages((int)writer.getId());
		if (languagesList.size()==ONLY_ONE_LANGUAGE && languagesList.contains(new Long(Constants.LANGUAGES_HEBREW))) {
			res = true;
		}
		return res;
	}

	public boolean isSupportEnable() throws SQLException {
		return writer.isSupportEnable();
	}

	public WriterWrapper() throws SQLException {
		selectItems = new HashMap<>();
		
		FacesContext context=FacesContext.getCurrentInstance();
		String writerName = context.getExternalContext().getRemoteUser();
		cache =CommonUtil.getCacheObjects();
		writer = AdminManager.getWriterByUserName(writerName);
		skins = WritersManager.getWriterSkins(writer.getId());
		isEtrader = isEtraderWriter();
		hasEtrader = isHasEtraderWriter();
		localInfo = new LocalInfo(this);
		UtcOffset = WritersManager.getWriterUtcOffset(writer.getId());

		allWritersMap = WritersManager.getAllWritersMap();

		//set dropdowns according to users locale
		setUserDropDowns(localInfo.getLocale());

        //sort the oddsTypesSI
        Collections.sort(oddsTypesSI, new CommonUtil.selectItemComparator());
	}

	public void setUserDropDowns(Locale locale) throws SQLException{

		issueSubjectsSI = ApplicationData.applicationSIHm.get("issueSubjectsSI");
		issueReachedStatusesSI = ApplicationData.applicationSIHm.get("issueReachedStatusesSI");
		issueActionTypesSI = ApplicationData.applicationSIHm.get("issueActionTypesSI");
		issuePrioritiesSI = ApplicationData.applicationSIHm.get("issuePrioritiesSI");
		issueStatusesSI = ApplicationData.applicationSIHm.get("issueStatusesSI");
		issueChannelsSI = ApplicationData.applicationSIHm.get("issueChannelsSI");
		fileTypesSI = ApplicationData.applicationSIHm.get("fileTypesSI");
		fraudTypesSI = ApplicationData.applicationSIHm.get("fraudTypesSI");
		//marketsAndGroupsSI = ApplicationData.applicationSIHm.get("marketsAndGroupsSI"); 
        //activeMarketsAndGroupsSI = ApplicationData.applicationSIHm.get("activeMarketsAndGroupsSI"); 
        //activeBinary0100MarketsAndGroupsSI = ApplicationData.applicationSIHm.get("activeBinary0100MarketsAndGroupsSI"); 
        //activeMarketsAndGroupsWithOneTouchSI = ApplicationData.applicationSIHm.get("activeMarketsAndGroupsWithOneTouchSI"); 
        //activeOptionPlusMarketsSI = ApplicationData.applicationSIHm.get("activeOptionPlusMarketsSI"); 
		//marketsByAgreementSI = ApplicationData.applicationSIHm.get("marketsByAgreementSI");
		marketsByAgreementWithAllSI = (ArrayList<SelectItem>) (CommonUtil.map2SelectList(getMarketsByAgreement())).clone();
		//marketsSI = ApplicationData.applicationSIHm.get("marketsSI");
		//marketsZeroOneHundredSI = ApplicationData.applicationSIHm.get("marketsZeroOneHundredSI");
		//marketGroupsSI = ApplicationData.applicationSIHm.get("marketGroupsSI");
		investmentStatusesSI = ApplicationData.applicationSIHm.get("investmentStatusesSI");
		transactionTypesSI = ApplicationData.applicationSIHm.get("transactionTypesSI");
		currenciesSI = ApplicationData.applicationSIHm.get("currenciesSI");
		languagesSI = ApplicationData.applicationSIHm.get("languagesSI");
		chargebackStatusesSI = CacheObjects.applicationSIHm.get("chargebackStatusesSI");
		oddsTypesSI = ApplicationData.applicationSIHm.get("oddsTypesSI");
		exchangesSI = ApplicationData.applicationSIHm.get("exchangesSI");
		investmentLimitGroupsSI = ApplicationData.applicationSIHm.get("investmentLimitGroupsSI");
		opportunityTypesSI = ApplicationData.applicationSIHm.get("opportunityTypesSI");
		userClassesSI = (CacheObjects.applicationSIHm.get("userClassesSI"));
		userClassesStringSI = (CacheObjects.applicationSIHm.get("userClassesStringSI"));
		timezoneTypesSI = ApplicationData.applicationSIHm.get("timezoneTypesSI");
		marketingPaymentTypesSI = ApplicationData.applicationSIHm.get("marketingPaymentTypes");
		marketingChannelsTypesSI = ApplicationData.applicationSIHm.get("marketingChannelsTypes");
		populationTypesSI = ApplicationData.applicationSIHm.get("populationTypesSI");
        bonusTypesSI = ApplicationData.applicationSIHm.get("bonusTypesSI");
        bonusStatesSI = ApplicationData.applicationSIHm.get("bonusStatesSI");
        pixelTypesSI = ApplicationData.applicationSIHm.get("pixelTypesSI");
        skinBusinessCasesSI = ApplicationData.applicationSIHm.get("skinBusinessCasesSI");
        providersSI = ApplicationData.applicationSIHm.get("providersSI");
        ccProvidersSI = ApplicationData.applicationSIHm.get("ccProvidersSI");
        countriesGroup = ApplicationData.applicationSIHm.get("countriesGroup");
		populationTypes = ApplicationData.applicationObjectsHm.get("populationTypes");
		marketingSourcesSI = ApplicationData.applicationSIHm.get("marketingSourcesSI");
		popTypeDecline = ApplicationData.applicationSIHm.get("populationTypesDeclineSI");
		mailboxPopupTypes = ApplicationData.applicationSIHm.get("mailboxPopupTypes");
		affiliatesNamesAndKeysSI = ApplicationData.applicationSIHm.get("affiliatesNamesAndKeysSI");
		affiliatesHM = ApplicationData.applicationObjectsHm.get("marketingAffiliates");
		riskReasonSI = ApplicationData.applicationSIHm.get("riskReasonSI");
		filesRiskStatusesSI = ApplicationData.applicationSIHm.get("riskStatusesSI");
		filesRiskStatusHM = ApplicationData.applicationObjectsHm.get("riskStatuses");
        wirebanksEtraderSI = ApplicationData.applicationSIHm.get("wirebanksEtraderSI");
        wirebanksAnyoptionSI = ApplicationData.applicationSIHm.get("wirebanksAnyoptionSI");
        issueComponentSI = (ApplicationData.applicationSIHm.get("issueComponentSI"));
        issueComponentsHM = ApplicationData.applicationObjectsHm.get("issueComponent");       
        copyopCoinsStateSI = (ApplicationData.applicationSIHm.get("copyopCoinsStateSI"));   
        copyopInvTypeSI = (ApplicationData.applicationSIHm.get("copyopInvTypeSI")); 
        invWritersSI = (ApplicationData.applicationSIHm.get("invWritersSI"));
        writersGroupsSI = (ApplicationData.applicationSIHm.get("writersGroupsSI"));        
        staticLandingPagePathSI = ApplicationData.applicationSIHm.get("staticLandingPagePathSI");
        staticLandingPagePathHM = ApplicationData.applicationObjectsHm.get("staticLandingPagePath");
        userHistoryFieldsSI = ApplicationData.applicationSIHm.get("userHistoryFieldsSI");
        marketingUrlSourceSI = (ApplicationData.applicationSIHm.get("marketingUrlSourceSI"));
        salesRepsSI = ApplicationData.applicationSIHm.get("salesRepsSI");
        salesRepsConversionDepSI = ApplicationData.applicationSIHm.get("salesRepsConversionDepSI");
        
        //ignite cached
        setMarketingCombinationsLongSI();
        setMarketingCampaignsLongSI();
		setPaymentMethodsSI();
        initAffiliatesSI();
        initMarketingAffNameKeySI();
        initCountriesGroupTiersSI();
        
        //not cached
		setYesNoSI();
		setYesNoAllSI();
		setWonLostAllSI();
		setFetchRowNumCountSI();
		setDepositsStatusSI();
		setCcDepositsStatusSI();
		setSalesDepositsTypes();
		setSalesQualifiedFilter();
		setIssueTypesSI();
		setRiskIssueTypesSI();
		setSalesDepositsSources();
		setHistoryWriters();
        setMarketingCampaignsBySkinsLongSI();
        setActiveUsersSI();
        setSiteSI();
        setAssetIndexGroupsSI();
        setLastLoginSI();
		setCallDirectionSI();
		setPopEntriesWritersFilterSI();
		
		skinsSI = WritersManager.getWriterSkinsSI(writer.getId());
        Collections.sort(skinsSI, new selectItemComparator());
        
        skinsRegulatedSI = WritersManager.getWriterSkinsRegulatedSI(writer.getId());
        Collections.sort(skinsRegulatedSI, new selectItemComparator());
        
        skinsNotRegulatedSI = WritersManager.getWriterSkinsNotRegulatedSI(writer.getId());
        Collections.sort(skinsNotRegulatedSI, new selectItemComparator());
        
		currencySI = WritersManager.getWriterCurrenciesSI(writer.getId(), localInfo.getLocale());
        Collections.sort(currencySI, new selectItemComparator());
        
		String skinsToString = null;
		if (null != skins && skins.size() > 0) {
			skinsToString = getSkinsToString();
		}
		salesReps = PopulationsManager.getSalesWriters(skinsToString);
		salesConversionReps = PopulationsManager.getSalesWritersBySalesType(skinsToString, PopulationsManagerBase.SALES_TYPE_CONVERSION);
		salesRetentionReps = PopulationsManager.getSalesWritersBySalesType(skinsToString, PopulationsManagerBase.SALES_TYPE_RETENTION);
		IssueActionType = IssuesManager.getIssuesActionTypes(writer.getId(), issueChannelsSI);
		mailBoxSenders = MailBoxTemplatesManager.getSenders();
		withdrawalTypesSI = TransactionsManager.getWithdrawlsList();
		
		skinsGroupByLanguageSI = WritersManager.getWriterSkinsGroupByLanguageSI(writer.getId());
        Collections.sort(skinsGroupByLanguageSI, new selectItemComparator());
	}

	/**
	 * This method translates a select item list to the language of this writer
	 * @param list the list to translate
	 * @return translated list
	 */
	public static ArrayList<SelectItem> translateSI(ArrayList<SelectItem> list) {
		ArrayList<SelectItem> newList = new ArrayList<SelectItem>();
		for (SelectItem si : list) {
			String lable = CommonUtil.getMessage(si.getLabel(), null, new Locale("en"));
			newList.add(new SelectItem(si.getValue(), lable));
		}
		Collections.sort(newList, new selectItemComparator());
		return newList;
	}
	
	public static ArrayList<SelectItem> translateOneTouchSI(ArrayList<SelectItem> list) {
		ArrayList<SelectItem> newList = new ArrayList<SelectItem>();
		for (SelectItem si : list) {
			newList.add(new SelectItem(si.getValue(), si.getLabel()));
		}
		Collections.sort(newList, new selectItemComparator());
		return newList;
	}
	
	/**
	 * This method adds a suffix to market's name, adds option "All" and sorts the list
	 * @param list
	 * @param addIds
	 * @return sorted list
	 */
	public static ArrayList<SelectItem> translateOneTouchAndLongTermSI(ArrayList<SelectItem> list, boolean addIds) {
		ArrayList<SelectItem> newList = new ArrayList<SelectItem>();
		for (SelectItem si : list) {
            String[] parts = si.getValue().toString().split(",");
            long marketId =  new Long(parts[1]);
			if (addIds) {
				newList.add(new SelectItem(marketId, si.getLabel()));
			} else {
				newList.add(new SelectItem(si.getValue(), si.getLabel()));
			}
		}
    	Collections.sort(newList, new selectItemComparator());
		if (addIds) {
			newList.add(0, new SelectItem("0", CommonUtil.getMessage("general.all", null)));
		} else {
			newList.add(0, new SelectItem("0,0", CommonUtil.getMessage("general.all", null)));
		}

		return newList;
	}

	/**
	 * This method take the second value at each string in the list,
	 * Translates a select item to the language of this writer
	 * @param list the list to translate
	 * @return translated list
	 */
	public static ArrayList<SelectItem> splitTranslateSI(ArrayList<SelectItem> list) {
		ArrayList<SelectItem> newList = new ArrayList<SelectItem>();
		for (SelectItem si : list) {
			String[] splitItem = si.getLabel().split(" ");
			String lable = splitItem[0] + " ";
			lable += CommonUtil.getMessage(splitItem[1], null, new Locale("en"));
			newList.add(new SelectItem(si.getValue(), lable));
		}
		Collections.sort(newList, new selectItemComparator());
		return newList;
	}

	/**
	 * This method adds an empty item to a select list
	 * @param list
	 * @return
	 */
	private ArrayList<SelectItem> addAllOption(ArrayList<SelectItem> list) {
		ArrayList<SelectItem> si = new ArrayList<SelectItem>();
		si=(ArrayList<SelectItem>)list.clone();
		SelectItem allItem = new SelectItem(ConstantsBase.ALL_FILTER_ID, CommonUtil.getMessage(ConstantsBase.ALL_FILTER_KEY, null));
		si.add(0, allItem);
		return si;
	}

	/**
	 * This method adds an empty item to a select list
	 * @param list
	 * @return
	 */
	private ArrayList<SelectItem> addAllOptionInt(ArrayList<SelectItem> list) {
		ArrayList<SelectItem> si = new ArrayList<SelectItem>();
		si=(ArrayList<SelectItem>)list.clone();
		SelectItem allItem = new SelectItem((int)ConstantsBase.ALL_FILTER_ID, ConstantsBase.ALL_FILTER_KEY);
		si.add(0, allItem);
		return si;
	}

	/**
	 * This method adds an empty item to a select list
	 * @param list
	 * @return
	 */
	private ArrayList<SelectItem> addAllOptionNoKey(ArrayList<SelectItem> list) {
		ArrayList<SelectItem> si = new ArrayList<SelectItem>();
		si=(ArrayList<SelectItem>)list.clone();
		SelectItem allItem = new SelectItem(ConstantsBase.ALL_FILTER_ID, "All");
		si.add(0, allItem);
		return si;
	}
	/**
	 * translates special SI - markets and groups
	 * @param list
	 * @return translated list
	 */
	private ArrayList<SelectItem> translateSImarketsAndGroupsSI(ArrayList<SelectItem> list) {
		ArrayList<SelectItem> newList = new ArrayList<SelectItem>();
		for (SelectItem si : list) {
			String lable = CommonUtil.getMessage(si.getLabel(), null, localInfo.getLocale());
//			if (si.getLabel().endsWith(",0"))
//				lable = si.getLabel();
//			else
//				lable = "--"+lable;
			newList.add(new SelectItem(si.getValue(), lable));
		}
		return newList;
	}

	private ArrayList<SelectItem> setYesNoSI() {
		yesNoSI = new ArrayList<SelectItem>();
		SelectItem no = new SelectItem(0,CommonUtil.getMessage("no", null,localInfo.getLocale()));
		SelectItem yes = new SelectItem(1,CommonUtil.getMessage("yes", null,localInfo.getLocale()));
		yesNoSI.add(no);
        yesNoSI.add(yes);
		return yesNoSI;
	}
	
	private long getWriterIdForSkin() {
		FacesContext context = FacesContext.getCurrentInstance();
        User u = (User) context.getApplication().createValueBinding(Constants.BIND_USER).getValue(context);
        long writerIdForSkin = Constants.DEFAULT_WRITER_ID_FOR_SKIN;

        if ( u.isPartnerMarketingSelected()){
        	writerIdForSkin = Utils.getWriter().getWriter().getId();
        }
        
        return writerIdForSkin;
	}

	public ArrayList<SelectItem> setYesNoAllSI() {
		yesNoSIAll = new ArrayList<SelectItem>();
		SelectItem no = new SelectItem(ConstantsBase.GENERAL_NO,CommonUtil.getMessage("no", null,localInfo.getLocale()));
		SelectItem yes = new SelectItem(ConstantsBase.GENERAL_YES,CommonUtil.getMessage("yes", null,localInfo.getLocale()));
		SelectItem all = new SelectItem(0,CommonUtil.getMessage("general.all", null,localInfo.getLocale()));
		yesNoSIAll.add(all);
		yesNoSIAll.add(no);
        yesNoSIAll.add(yes);
		return yesNoSIAll;
	}

	public ArrayList<SelectItem> setWonLostAllSI() {
		wonLostAllSI = new ArrayList<SelectItem>();
		SelectItem won = new SelectItem(new Integer(1),"Won");
		SelectItem lost = new SelectItem(new Integer(-1),"Lost");
		SelectItem all = new SelectItem(new Integer(0),CommonUtil.getMessage("general.all", null,localInfo.getLocale()));
		wonLostAllSI.add(all);
		wonLostAllSI.add(lost);
		wonLostAllSI.add(won);
		return wonLostAllSI;
	}
	
	public ArrayList<SelectItem> setFetchRowNumCountSI() {
	    fetchRowNumCount = new ArrayList<SelectItem>();
        SelectItem all = new SelectItem(new Integer(0),CommonUtil.getMessage("general.all", null,localInfo.getLocale()));
        SelectItem thousand = new SelectItem(new Integer(1000),"1000");
        fetchRowNumCount.add(all);
        fetchRowNumCount.add(thousand);
        return fetchRowNumCount;
	}

	public ArrayList<SelectItem> getYesNoAllSI() {
		return yesNoSIAll;
	}

    /**
	 * @return the yesNoSI
	 */
	public ArrayList<SelectItem> getYesNoSI() {
		return yesNoSI;
	}

	public ArrayList<SelectItem> getWonLostAllSI() {
		return wonLostAllSI;
	}
	
	public ArrayList<SelectItem> getFetchRowNumCountSI() {
	    return fetchRowNumCount;
	}


	private ArrayList<SelectItem> setDepositsStatusSI() {
		depositsStatusSI = new ArrayList<SelectItem>();
		SelectItem noDeposits = new SelectItem(0,CommonUtil.getMessage("deposit.item.list.status.no.deposits", null,localInfo.getLocale()));
		SelectItem failedDeposits = new SelectItem(1,CommonUtil.getMessage("deposit.item.list.status.failed", null,localInfo.getLocale()));
		SelectItem successDeposits = new SelectItem(2,CommonUtil.getMessage("deposit.item.list.status.success", null,localInfo.getLocale()));
		depositsStatusSI.add(noDeposits);
		depositsStatusSI.add(failedDeposits);
		depositsStatusSI.add(successDeposits);
		return depositsStatusSI;
	}

	public ArrayList<SelectItem> getDepositsStatusAllSI() {
		ArrayList<SelectItem> depositsStatusSIAll = new ArrayList<SelectItem>();
		SelectItem noDeposits = new SelectItem(ConstantsBase.NO_DEPOSIT,CommonUtil.getMessage("deposit.item.list.status.no.deposits", null,localInfo.getLocale()));
		SelectItem failedDeposits = new SelectItem(ConstantsBase.FAILED_DEPOSIT,CommonUtil.getMessage("deposit.item.list.status.failed", null,localInfo.getLocale()));
		SelectItem successDeposits = new SelectItem(ConstantsBase.SUCCESS_DEPOSIT,CommonUtil.getMessage("deposit.item.list.status.success", null,localInfo.getLocale()));
		SelectItem all = new SelectItem(0,CommonUtil.getMessage("general.all", null));
		depositsStatusSIAll.add(all);
		depositsStatusSIAll.add(failedDeposits);
		depositsStatusSIAll.add(noDeposits);
		depositsStatusSIAll.add(successDeposits);
		return depositsStatusSIAll;
	}

    /**
	 * @return the depositsStatusSISI
	 */
	public ArrayList<SelectItem> getDepositsStatusSI() {
		return depositsStatusSI;
	}

	private void setCcDepositsStatusSI() {
		ccDepositsStatusSI = new ArrayList<SelectItem>();
		SelectItem all = new SelectItem(0,CommonUtil.getMessage("general.all", null,localInfo.getLocale()));
		SelectItem failedDeposits = new SelectItem(ConstantsBase.FAILED_DEPOSIT,CommonUtil.getMessage("deposit.item.list.status.failed", null,localInfo.getLocale()));
		SelectItem successDeposits = new SelectItem(ConstantsBase.SUCCESS_DEPOSIT,CommonUtil.getMessage("deposit.item.list.status.success", null,localInfo.getLocale()));
		ccDepositsStatusSI.add(all);
		ccDepositsStatusSI.add(failedDeposits);
		ccDepositsStatusSI.add(successDeposits);
	}

    /**
	 * @return the ccDepositsStatusSISI
	 */
	public ArrayList<SelectItem> getCcDepositsStatusSI() {
		return ccDepositsStatusSI;
	}

	private ArrayList<SelectItem> setCallDirectionSI() {
		callDirectionSI = new ArrayList<SelectItem>();
		SelectItem all = new SelectItem(ConstantsBase.ALL_FILTER_ID,ConstantsBase.EMPTY_STRING);
		SelectItem out = new SelectItem(IssueAction.ISSUE_DIRECTION_OUT_BOUND_PHONE_CALL,CommonUtil.getMessage("issues.actions.call.direction.out", null,localInfo.getLocale()));
		SelectItem mr = new SelectItem(IssueAction.ISSUE_DIRECTION_MEMBER_REQUESTED_PHONE_CALL,CommonUtil.getMessage("issues.actions.call.direction.mr", null,localInfo.getLocale()));
		SelectItem in = new SelectItem(IssueAction.ISSUE_DIRECTION_IN_BOUND_PHONE_CALL,CommonUtil.getMessage("issues.actions.call.direction.in", null,localInfo.getLocale()));
		callDirectionSI.add(all);
        callDirectionSI.add(in);
		callDirectionSI.add(mr);
        callDirectionSI.add(out);
		return callDirectionSI;
	}

    /**
	 * @return the callDirectionSI
	 */
	public ArrayList<SelectItem> getCallDirectionSI() {
		return callDirectionSI;
	}

    /**
	 * @return the populationEntryEntitySI
	 */
	public ArrayList<SelectItem> getPopulationEntryEntitySI() {
		return populationEntryEntitySI;
	}


	/**
     * Get Writer UtcOffset in format GMT+/-HH:MM.
     * The Writer UtcOffset in format GMT+/-HH:MM.
     * @return The Writer UtcOffset in format GMT+/-HH:MM.
     */
	public String getUtcOffset() {
		return UtcOffset;
	}

	/**
     * Get Writer UtcOffset or User UtcOffset if isSupportSelected
     * The UtcOffset in format GMT+/-HH:MM.
     * @return The Writer  or User UtcOffset in format GMT+/-HH:MM.
     */
	public String getCurrentPageUtcOffset() {
		User user = Utils.getUser();
    	if (user.isSupportSelected()) {
    			return user.getUtcOffset();
    		}
    	//returns writer timezone in case of other then support
    	return  getUtcOffset();
	}

	public ArrayList getFileTypesSI() {
		return translateSI(fileTypesSI);
	}


	public ArrayList getFraudTypesSI() {
		return translateSI(fraudTypesSI);
	}

	public ArrayList getIssueChannelsSI() {
		return translateSI(issueChannelsSI);
	}

	public ArrayList getIssuePrioritiesSI() {
		return translateSI(issuePrioritiesSI);
	}

	public ArrayList getIssueStatusesSI() {
		return translateSI(issueStatusesSI);
	}
	
	public ArrayList getCustomerReportTypeSI() {
		return translateSI(customerReportTypeSI);
	}

	public ArrayList getIssueSubjectsSI() {
		return translateSI(issueSubjectsSI);
	}

	public ArrayList getIssueReachedStatusesSI() {
		return translateSI(issueReachedStatusesSI);
	}

	public ArrayList getIssueActionTypesSI() {
		return translateSI(issueActionTypesSI);
	}

	public ArrayList<SelectItem> getIssueActionTypeFilterSI() {
		ArrayList<SelectItem> newList = new ArrayList<SelectItem>();
		ArrayList<IssueActionType> actionTypesList = ApplicationData.getActionTypesList();
		long channelId;
		for (IssueActionType actionType : actionTypesList) {
			channelId = actionType.getChannelId();
			if (IssuesManagerBase.ISSUE_CHANNEL_CALL == channelId ||
					IssuesManagerBase.ISSUE_CHANNEL_ACTIVITY == channelId){
				String lable = CommonUtil.getMessage(actionType.getName(), null, localInfo.getLocale());
				newList.add(new SelectItem(String.valueOf(actionType.getId()), lable));
			}
		}
		return newList;
	}

	public ArrayList<SelectItem> getIssueActionTypeFilterSIWithAll() {
		ArrayList<SelectItem> actionTypesList = new ArrayList<SelectItem>();
		actionTypesList.add(new SelectItem("All","All"));
		actionTypesList.addAll(getIssueActionTypeFilterSI());
		return actionTypesList;
	}

	public LocalInfo getLocalInfo() {
		return localInfo;
	}

	public Map<String, String> getMarketsAndGroups() { 
		LinkedHashMap<String, String> marketsAndGroups = new LinkedHashMap<String,String>();
		marketsAndGroups.putAll(MarketsManager.getMarketsAndGroups());
		return marketsAndGroups;
	}
	
    public ArrayList getMarketsAndGroupsWithoutUnnecessarySI(boolean addIds) {
        ArrayList marketsWithoutUnnecessary = new ArrayList<SelectItem>();
        for (SelectItem si : CommonUtil.map2SelectList(getActiveMarketsAndGroupsWithOneTouch()) ) { 
            String[] parts = si.getValue().toString().split(",");
            long marketId =  new Long(parts[1]);
//            if (isUnnecessaryMarketId(marketId)){               
                marketsWithoutUnnecessary.add(new SelectItem(si.getValue(), si.getLabel()));   
//            }
        }
        return translateOneTouchAndLongTermSI(marketsWithoutUnnecessary, addIds);
    }

//    private boolean isUnnecessaryMarketId(long marketId) {
//        return marketId != Constants.MARKET_ID_ABU_DHABI && marketId != Constants.MARKET_ID_AIR_CHINA_LIMITEDI && marketId != Constants.MARKET_ID_AFRICA &&
//        		marketId != Constants.MARKET_ID_AMERICA_MOVIL_UP && marketId != Constants.MARKET_ID_BRITISH_AIRWAYS &&
//                marketId != Constants.MARKET_ID_CMB && marketId != Constants.MARKET_ID_COCA_COLA && marketId != Constants.MARKET_ID_CONTROLADORA &&
//                marketId != Constants.MARKET_ID_FREDDIE_MAC && marketId != Constants.MARKET_ID_GENERAL_ELECTRIC &&
//                marketId != Constants.MARKET_ID_IBERDROLA && marketId != Constants.MARKET_ID_INDITEX && marketId != Constants.MARKET_ID_ISE30 &&
//                marketId != Constants.MARKET_ID_ISE100 && marketId != Constants.MARKET_ID_ISE30FUT && marketId != Constants.MARKET_ID_ISE_FINANCIAL &&
//                marketId != Constants.MARKET_ID_ISE_INDUSTRIAL && marketId != Constants.MARKET_ID_JOHNSON_JOHNSON && marketId != Constants.MARKET_ID_KLSE &&
//                marketId != Constants.MARKET_ID_KWAIT_SE_KSE && marketId != Constants.MARKET_ID_MAOF_INDEX && marketId != Constants.MARKET_ID_MARKS_SPENSER &&
//                marketId != Constants.MARKET_ID_NASDAQ100_INDEX && marketId != Constants.MARKET_ID_NIKE && marketId != Constants.MARKET_ID_PETROCHINA && 
//                marketId != Constants.MARKET_ID_PETROLEO_BRASILEIRO && marketId != Constants.MARKET_ID_REPSOL_YPF &&
//                marketId != Constants.MARKET_ID_TADAWUL && marketId != Constants.MARKET_ID_TEL_REALSTATE && marketId != Constants.MARKET_ID_TELMEX &&
//                marketId != Constants.MARKET_ID_VOLKSWAGEN && marketId != Constants.MARKET_ID_WAL_MART && marketId != Constants.MARKET_ID_WAL_MART_MX &&
//                marketId != Constants.MARKET_ID_ZYNGA;
//    }

    public Map<String, String> getActiveMarketsAndGroups() {
    	LinkedHashMap<String, String> activeMarketsAndGroups = new LinkedHashMap<String,String>();
    	activeMarketsAndGroups.putAll(MarketsManager.getActiveMarketsAndGroups());
		return activeMarketsAndGroups;
    }

    public Map<String, String> getActiveBinary0100MarketsAndGroups() {
    	LinkedHashMap<String, String> activeBinary0100 = new LinkedHashMap<String, String>();
    	activeBinary0100.putAll(MarketsManager.getActiveBinary0100MarketsAndGroups());
        return activeBinary0100;
    }

    public HashMap<String, String> getActiveMarketsAndGroupsWithOneTouch() {
    	HashMap<String, String> map = MarketsManager.getActiveMarketsAndGroupsWithOneTouch();
    	LinkedHashMap<String, String> activeOneTouch = new LinkedHashMap<String, String>();
    	 map.entrySet().stream().sorted(Map.Entry.<String, String>comparingByValue())
         .forEachOrdered(entry -> activeOneTouch.put(entry.getKey(), entry.getValue()));
    	return activeOneTouch;
    }
    
    public ArrayList getActiveMarketsAndGroupsWithOneTouchAndLongTermSI(boolean addIds) {
        return translateOneTouchAndLongTermSI(CommonUtil.map2SelectList(getActiveMarketsAndGroupsWithOneTouch()), addIds); 
    }

    public HashMap<Long, String> getActiveOptionPlusMarkets() {
    	LinkedHashMap<Long, String> activeMarkets = new LinkedHashMap<Long, String>();
    	activeMarkets.putAll(MarketsManager.getActiveMarkets(Opportunity.TYPE_OPTION_PLUS));
        return activeMarkets;
    }

	public HashMap<Long, String> getMarketsByAgreement() {
		LinkedHashMap<Long, String> marketsByAgreement = new LinkedHashMap<Long, String>();
		marketsByAgreement.putAll(MarketsManager.getMarketsByAgreement());
		return marketsByAgreement; 
	}

	public String getSkinsToString() {
		String list = ConstantsBase.EMPTY_STRING;
		for (int i=0; i<skins.size(); i++) {
			list += skins.get(i)+",";
		}
		return list.substring(0,list.length()-1);
	}

	public ArrayList<Integer> getSkins() {
		return skins;
	}

	public int getSkinsSize() {
		return skins.size();
	}

	public HashMap<Long, String> getMarkets() {
		LinkedHashMap<Long, String> markets = new LinkedHashMap<Long, String>();
		markets.putAll(MarketsManager.getAllMarkets());
		return markets;
	}
	
    public ArrayList getMarketsWithoutUnnecessarySI() {
        ArrayList marketsWithoutUnnecessary = new ArrayList<SelectItem>();
        for (SelectItem si : CommonUtil.map2SelectList(getMarkets())) {
            long marketId =  new Long((Long) si.getValue());
//            if (isUnnecessaryMarketId(marketId)){               
                marketsWithoutUnnecessary.add(new SelectItem(si.getValue(), si.getLabel()));   
//            }
        }
        return marketsWithoutUnnecessary;
    }	
	public HashMap<String, String> getMarketsZeroOneHundred(){
		LinkedHashMap<String, String> marketsZeroOneHundred = new LinkedHashMap<String, String>();
		marketsZeroOneHundred.put(String.valueOf(ConstantsBase.ALL_FILTER_ID), CommonUtil.getMessage(ConstantsBase.ALL_FILTER_KEY, null));
		HashMap<String, String> map = MarketsManager.getAllZeroOneHundredMarkets();
		map.entrySet().stream().sorted(Map.Entry.<String, String>comparingByValue(String.CASE_INSENSITIVE_ORDER))
		.forEachOrdered(entry -> marketsZeroOneHundred.put(entry.getKey(), entry.getValue()));
		
		return marketsZeroOneHundred; 
	}
	public ArrayList getSkinsSI() {
		return translateSI(skinsSI);
	}

	public ArrayList getSkinsAndEmptySI() {
		return translateSI(addAllOptionInt(skinsSI));
	}

	public ArrayList getSkinsAndEmptyLongSI() {
		ArrayList<SelectItem> list = new ArrayList<SelectItem>();
	
		for ( SelectItem s : skinsSI ) {
			list.add(new SelectItem(new Long((Integer)s.getValue()), s.getLabel()));
		}
        list = translateSI(list);
        list.add(0, new SelectItem(new Long(0), CommonUtil.getMessage("campaigns.all", null)));
		return list;
	}

	public ArrayList getSkinsLongSI() {
		ArrayList<SelectItem> list = new ArrayList<SelectItem>();
		for ( SelectItem s : skinsSI ) {
			list.add(new SelectItem(new Long((Integer)s.getValue()), s.getLabel()));
		}

		return translateSI(list);
	}
	
	public ArrayList getSkinsNoETLongSI() {
		ArrayList<SelectItem> list = new ArrayList<SelectItem>();
		for ( SelectItem s : skinsSI ) {
			if((Long)s.getValue() != Skins.SKIN_ETRADER) {				
				list.add(new SelectItem(new Long((Integer)s.getValue()), s.getLabel()));
			}
		}

		return translateSI(list);
	}

	public ArrayList getSkinsStringSI() {
		ArrayList<SelectItem> list = new ArrayList<SelectItem>();
		list.add(new SelectItem("0","general.all"));
		for ( SelectItem s : skinsSI ) {
			list.add(new SelectItem(s.getValue().toString(), s.getLabel()));
		}
		return translateSI(list);
	}

	public ArrayList getMarketGroupStringSI(){
		ArrayList<SelectItem> list = new ArrayList<SelectItem>();
		for ( SelectItem s : CommonUtil.map2SelectList(getMarketGroups()) ) {
				list.add(new SelectItem(s.getValue().toString(), s.getLabel()));
		}
		return list;
	}

	public ArrayList getMarketsStringSI(){
		ArrayList<SelectItem> list = new ArrayList<SelectItem>();

		for ( SelectItem s : CommonUtil.map2SelectList(getMarkets())) {
				list.add(new SelectItem(s.getValue().toString(), s.getLabel()));
		}

        //add all
        ArrayList<SelectItem> listSI = new ArrayList<SelectItem>();
        listSI.add(new SelectItem("0",CommonUtil.getMessage("general.all",null)));
        listSI.addAll(list);
        return listSI;
	}

	public ArrayList getMarketsZeroOneHundredStringSI(){
		ArrayList<SelectItem> list = new ArrayList<SelectItem>();

		for ( SelectItem s : CommonUtil.map2SelectList(getMarketsZeroOneHundred()) ) {
				list.add(new SelectItem(s.getValue().toString(), s.getLabel()));
		}

        //add all
        ArrayList<SelectItem> listSI = new ArrayList<SelectItem>();
        listSI.add(new SelectItem("0",CommonUtil.getMessage("general.all",null)));
        listSI.addAll(translateSI(list));
        return listSI;
	}

	public ArrayList getCurrencySI() {
		return currencySI;
	}

	public int getCurrenySIsize() {
		return currencySI.size();
	}

	public ArrayList getInvestmentStatusesSI() {
	/*	ArrayList<SelectItem> list = new ArrayList<SelectItem>();
		list.add(new SelectItem(new Long(2), CommonUtil.getMessage("users.market.disable.all", null)));
		ArrayList<SelectItem> tmp = (translateSI(investmentStatusesSI));
		tmp.remove(2);
		list.addAll(tmp);
		return list;*/
		return translateSI(investmentStatusesSI);
	}

	public ArrayList getTransactionTypesSI() {
		return translateSI(transactionTypesSI);
	}

	public boolean isEtrader() {
		return isEtrader;
	}

	public ArrayList getCurrenciesSI() {
		return translateSI(currenciesSI);
	}

	public ArrayList getLanguagesSI() {
		return translateSI(languagesSI);
	}

	public ArrayList getLanguagesAndEmptyLongSI() {
		ArrayList<SelectItem> list = new ArrayList<SelectItem>();
		list.add(new SelectItem(new Long(0), "languages.all"));

		for ( SelectItem s : languagesSI ) {
			list.add(new SelectItem((Long)s.getValue(), s.getLabel()));
		}

		return translateSI(list);
	}

	public ArrayList<SelectItem> getChargebackStatusesSI() {
		return translateSI(chargebackStatusesSI);
	}

	public ArrayList<SelectItem> getAllChargebackStatusesSI() {
		ArrayList<SelectItem> si = new ArrayList<SelectItem>();
		si.add(new SelectItem(ConstantsBase.ALL_FILTER_ID, ConstantsBase.ALL_FILTER_KEY));
		si.addAll(chargebackStatusesSI);
		return translateSI(si);
	}

	public ArrayList<SelectItem> getBanksSI() {
		return translateSI(cache.getBanksSI());
	}

	/**
	 * @return the oddsTypeSI
	 */
	public ArrayList<SelectItem> getOddsTypesSI() {
        return oddsTypesSI;
	}

    /**
     * For 1T option display only odds with percentage great or equals 100%
     * @return the list
     */
    public ArrayList<SelectItem> getOneTouchOddsTypesSI() {
        ArrayList<SelectItem> list = new ArrayList<SelectItem>();
        for (SelectItem s : oddsTypesSI) {
            int end = s.getLabel().indexOf("VS");
            if (end == -1) {
                end = s.getLabel().indexOf("vs");
            }
            String win = s.getLabel().substring(0, end);
            if (Integer.parseInt(win)>=100) {
                list.add(new SelectItem((Long)s.getValue(), s.getLabel()));
            }
        }
        Collections.sort(list, new CommonUtil.selectItemComparator());
        return list;
    }

	/**
	 * @return the exchangesSI
	 */
	public ArrayList<SelectItem> getExchangesSI() {
		return exchangesSI;
	}

	/**
	 * @return the investmentLimitGroupsSI
	 */
	public ArrayList<SelectItem> getInvestmentLimitGroupsSI() {
		return investmentLimitGroupsSI;
	}

	/**
	 * @return the opportunityTypesSI
	 */
	public ArrayList<SelectItem> getOpportunityTypesSI() {
		return opportunityTypesSI;
	}

	/**
	 * @return the opportunityTypesSI
	 */
	public ArrayList<SelectItem> getOpportunityTypesWithAllOptionSI() {
		return addAllOptionNoKey(opportunityTypesSI);
	}

	public ArrayList getOpportunityTypesStringSI() {
		ArrayList<SelectItem> list = new ArrayList<SelectItem>();
		list.add(new SelectItem("0",CommonUtil.getMessage("general.all", null)));
		for ( SelectItem s : opportunityTypesSI ) {
			list.add(new SelectItem(s.getValue().toString(), s.getLabel()));
		}
		return list;
	}

	/**
	 * @return the userClassesSI
	 */
	public ArrayList<SelectItem> getUserClassesSI() {
		return translateSI(userClassesSI);
	}

	/**
	 * @return the userClassesSI
	 */
	public ArrayList<SelectItem> getUserClassesNoEmptySI() {
		ArrayList<SelectItem> list = (ArrayList<SelectItem>)userClassesSI.clone();
		list.remove(0); //remove all option
		return translateSI(list);
	}

	/**
	 * @return the timeZoneTypesSI
	 */
	public ArrayList<SelectItem> getTimezoneTypesSI() {
		return timezoneTypesSI;
	}

	/**
	 * @return the marketGroupsSI
	 */
	public HashMap<String, String> getMarketGroups() {
		LinkedHashMap<String, String> marketGroups = new LinkedHashMap<String, String>();
		marketGroups.put(String.valueOf(ConstantsBase.ALL_FILTER_ID), CommonUtil.getMessage(ConstantsBase.ALL_FILTER_KEY, null));
		HashMap<String, String> map = MarketsManager.getMarketGroups();
		map.entrySet().stream().sorted(Map.Entry.<String, String>comparingByValue(String.CASE_INSENSITIVE_ORDER))
		.forEachOrdered(entry -> marketGroups.put(entry.getKey(), entry.getValue()));
		
		LinkedHashMap<String, String> marketGroupsTranslated = new LinkedHashMap<String, String>();
		for(Map.Entry<String, String> entry : marketGroups.entrySet()) {
			String label = CommonUtil.getMessage(entry.getValue(), null, new Locale("en"));
			marketGroupsTranslated.put(entry.getKey(), label);
		}
		return marketGroupsTranslated; 
	}

	/**
	 * @return the userClassesStringSI
	 */
	public ArrayList<SelectItem> getUserClassesStringSI() {
		return translateSI(userClassesStringSI);
	}

	/**
	 * @return the marketsByAgreementWithAllSI
	 */
	public ArrayList<SelectItem> getMarketsByAgreementWithAllSI() {
		return addAllOption(marketsByAgreementWithAllSI);
	}

	/**
	 * Get risky users list for market disable with all value
	 * @return
	 * 		list of risky users
	 * @throws SQLException
	 */
	public ArrayList<SelectItem> getRiskyUsersAllSI() throws SQLException {

		ArrayList<SelectItem> si = UsersManager.getRiskyUsers();
		si.add(0, new SelectItem(new String("0"), CommonUtil.getMessage("users.market.disable.all", null)));

		return si;
	}

	/**
	 * Get risky users list for market disable
	 * 		without all value
	 * @return
	 * 		list of risky users
	 * @throws SQLException
	 */
	public ArrayList<SelectItem> getRiskyUsersSI() throws SQLException {
		return UsersManager.getRiskyUsers();
	}

	/**
	 * Get risky users list for market disable
	 * 		without all value
	 * @return
	 * 		list of risky users
	 * @throws SQLException
	 */
	public ArrayList<SelectItem> getRiskyUsersSIAll() throws SQLException {

		return addAllOptionNoKey(getRiskyUsersSI());
	}

	/**
	 * Get scheduled list with all value
	 * @return
	 * @throws SQLException
	 */
	public ArrayList<SelectItem> getScheduledListAllSI() throws SQLException {
		ArrayList<SelectItem> si = new ArrayList<SelectItem>();
		// TODO switch constants to Opportunity ones
		si.add(new SelectItem(new Long(InvestmentsManagerBase.SCHEDULED_ALL), CommonUtil.getMessage("users.market.disable.all", null)));
		si.add(new SelectItem(new Long(InvestmentsManagerBase.SCHEDULED_HOURLY),CommonUtil.getMessage("opportunities.templates.hourly", null)));
		si.add(new SelectItem(new Long(InvestmentsManagerBase.SCHEDULED_DAILY),CommonUtil.getMessage("opportunities.templates.daily", null)));
		si.add(new SelectItem(new Long(InvestmentsManagerBase.SCHEDULED_WEEKLY),CommonUtil.getMessage("opportunities.templates.weekly", null)));
		si.add(new SelectItem(new Long(InvestmentsManagerBase.SCHEDULED_MONTHLY),CommonUtil.getMessage("opportunities.templates.monthly", null)));
		si.add(new SelectItem(new Long(InvestmentsManagerBase.SCHEDULED_ONE_TOUCH),CommonUtil.getMessage("opportunities.templates.oneTouch", null)));
		si.add(new SelectItem(new Long(Opportunity.SCHEDULED_QUARTERLY),CommonUtil.getMessage("opportunities.templates.quarterly", null)));
		return si;
	}
	
	/**
	 * Get suspended opportunity drop down list with all values
	 * @return ArrayList of SelectItem type to be displayed
	 * @throws SQLException
	 */
	public ArrayList<SelectItem> getSuspendedListAllSI() throws SQLException {
		ArrayList<SelectItem> si = new ArrayList<SelectItem>();
		si.add(new SelectItem(new Integer(InvestmentsManagerBase.SUSPENDED_ALL), CommonUtil.getMessage("opportunities.suspended.all", null)));
		si.add(new SelectItem(new Integer(InvestmentsManagerBase.SUSPENDED_TRUE),CommonUtil.getMessage("opportunities.suspended.true", null)));
		si.add(new SelectItem(new Integer(InvestmentsManagerBase.SUSPENDED_FALSE),CommonUtil.getMessage("opportunities.suspended.false", null)));
		return si;
	}

	/**
	 * Get active / inactive list
	 * @return
	 * @throws SQLException
	 */
	public ArrayList<SelectItem> getIsActiveAllSI() throws SQLException {

		ArrayList<SelectItem> si = new ArrayList<SelectItem>();
		si.add(new SelectItem(new Long(ConstantsBase.USERS_MARKET_DIS_ACTIVE_ALL), CommonUtil.getMessage("users.market.disable.all", null)));
		si.add(new SelectItem(new Long(ConstantsBase.USERS_MARKET_DIS_ACTIVE),CommonUtil.getMessage("users.market.disable.active", null)));
		si.add(new SelectItem(new Long(ConstantsBase.USERS_MARKET_DIS_NOT_ACTIVE),CommonUtil.getMessage("users.market.disable.Inactive", null)));

		return si;
	}


	/**
	 * Get markets SI with all value
	 * @return
	 * @throws SQLException
	 */
	public ArrayList<SelectItem> getMarketsAllSI() throws SQLException {

		ArrayList<SelectItem> si = new ArrayList<SelectItem>();
		si.add(new SelectItem(new Long(0), CommonUtil.getMessage("users.market.disable.all", null)));
		si.addAll(CommonUtil.map2SelectList(getMarkets()));

		return si;
	}
	
	   /**
     * Get markets SI without Unnecessary and with all value
     * @return
     * @throws SQLException
     */
    public ArrayList<SelectItem> getMarketsWithoutUnnecessaryAllSI() throws SQLException {

        ArrayList<SelectItem> si = new ArrayList<SelectItem>();
        si.add(new SelectItem(new Long(0), CommonUtil.getMessage("users.market.disable.all", null)));
        si.addAll(getMarketsWithoutUnnecessarySI());

        return si;
    }

	/**
	 * @return the writer
	 */
	public Writer getWriter() {
		return writer;
	}

	/**
	 * @param writer the writer to set
	 */
	public void setWriter(Writer writer) {
		this.writer = writer;
	}

	/**
	 * Get marketing payment types SI with all
	 * @return
	 */
	public ArrayList getMarketingPaymentTypesSI() {
		return translateSI(marketingPaymentTypesSI);
	}
	
	public ArrayList getMarketingChannelsTypesSI() {
		return translateSI(marketingChannelsTypesSI);
	}

	/**
	 * Get marketing payment types SI with all
	 * @return
	 */
	public ArrayList getMarketingPaymentTypesSIAll() {
		ArrayList<SelectItem> si = new ArrayList<SelectItem>();
		si.add(new SelectItem(new Long(0), CommonUtil.getMessage("campaigns.all", null)));
		ArrayList<SelectItem> siT = translateSI(marketingPaymentTypesSI);
		si.addAll(siT);
		return si;
	}
	
	public ArrayList getMarketingChannelsTypesSIAll() {
		ArrayList<SelectItem> si = new ArrayList<SelectItem>();
		si.add(new SelectItem(new Long(0), CommonUtil.getMessage("campaigns.all", null)));
		ArrayList<SelectItem> siT = translateSI(marketingChannelsTypesSI);
		si.addAll(siT);
		return si;
	}

	/**
	 * Get marketing territories SI with all
	 * @return
	 * @throws SQLException
	 */
	public ArrayList getMarketingTerritoriesSIAll() throws SQLException {
		ArrayList<SelectItem> si = new ArrayList<SelectItem>();
		si.add(new SelectItem(new Long(0), CommonUtil.getMessage("campaigns.all", null)));
		ArrayList<SelectItem> siT = MarketingManager.getMarketingTerritoriesSI();
		si.addAll(siT);
		return si;
	}
	
	/**
	 * Get marketing territories SI with all value by writerSkinId
	 * @return
	 * @throws SQLException
	 */
	public ArrayList getMarketingTerritoriesSIAllWriterSkinId() throws SQLException {
		ArrayList<SelectItem> si = new ArrayList<SelectItem>();
		si.add(new SelectItem(new Long(0), CommonUtil.getMessage("campaigns.all", null)));
		ArrayList<SelectItem> siT = MarketingManager.getMarketingTerritoriesSIWriterSkinId(writer.getId());
		si.addAll(siT);
		return si;
	}

	/**
	 * Get marketing payment recipients
	 * @return
	 * @throws SQLException
	 */
	public ArrayList<SelectItem> getMarketingPaymentRecipientsSI() throws SQLException {

		return MarketingManager.getMarketingPaymentRecipients();
	}

	/**
	 * Get marketing payment recipients with all value
	 * @return
	 * @throws SQLException
	 */
	public ArrayList<SelectItem> getMarketingPaymentRecipientsAllSI() throws SQLException {
		ArrayList<SelectItem> si = new ArrayList<SelectItem>();
		si.add(new SelectItem(new Long(0), CommonUtil.getMessage("campaigns.all", null)));
		ArrayList<SelectItem> siT = MarketingManager.getMarketingPaymentRecipients();
		si.addAll(siT);
		return si;
	}
	
	/**
	 * Get marketing payment recipients with all value by writerSkinId
	 * @return
	 * @throws SQLException
	 */
	public ArrayList<SelectItem> getMarketingPaymentRecipientsAllWriterSkinId() throws SQLException {
		ArrayList<SelectItem> si = new ArrayList<SelectItem>();
		si.add(new SelectItem(new Long(0), CommonUtil.getMessage("campaigns.all", null)));
		ArrayList<SelectItem> siT = MarketingManager.getMarketingPaymentRecipientsWriterSkinId(writer.getId());
		si.addAll(siT);
		return si;
	}

	/**
	 * Get marketing territories
	 * @return
	 * @throws SQLException
	 */
	public ArrayList<SelectItem> getMarketingTerritoriesSI() throws SQLException {

		return MarketingManager.getMarketingTerritoriesSI();
	}

	/**
	 * Get payment type lable by id
	 * @param id id of the payment type
	 * @return
	 */
	public String getPaymentTypeById(long id) {
		String lable = "";
		for ( SelectItem s : marketingPaymentTypesSI ) {
			if ( s.getValue().equals(id) ) {
				lable = s.getLabel();
			}
		}
		return lable;
	}
	
	public String getMarketingChannelsTypeById(long id) {
		String label = "";
		for(SelectItem s: marketingChannelsTypesSI) {
			if (s.getValue().equals(id)) {
				label = s.getLabel();
			}
		}
		return label;
	}

	/**
	 * Get marketing mediums SI (String,String)
	 * @return
	 * @throws SQLException
	 */
	public ArrayList<SelectItem> getMarketingMediumsSI() throws SQLException {

		ArrayList<MarketingMedium> list = MarketingManager.getMarketingMediums(null, getWriterIdForSkin());
		ArrayList<SelectItem> si = new ArrayList<SelectItem>();
		si.add(new SelectItem(new String(), CommonUtil.getMessage("campaigns.all", null)));

		for ( MarketingMedium m : list ) {
			si.add(new SelectItem(m.getName(), m.getName()));
		}

		return si;
	}

	/**
	 * Get marketing mediums SI (Long,String)
	 * @return
	 * @throws SQLException
	 */
	public ArrayList<SelectItem> getMarketingMediumsLongSI() throws SQLException {

		ArrayList<MarketingMedium> list = MarketingManager.getMarketingMediums(null, getWriterIdForSkin());
		ArrayList<SelectItem> si = new ArrayList<SelectItem>();
		si.add(new SelectItem(new Long(0), CommonUtil.getMessage("campaigns.all", null)));

		for ( MarketingMedium m : list ) {
			si.add(new SelectItem(new Long(m.getId()), m.getName()));
		}

		return si;
	}


	/**
	 * Get marketing contents SI(String,Sting)
	 * @return
	 * @throws SQLException
	 */
	public ArrayList<SelectItem> getMarketingContentsSI() throws SQLException {

		ArrayList<MarketingContent> list = MarketingManager.getMarketingContents(null, getWriterIdForSkin());
		ArrayList<SelectItem> si = new ArrayList<SelectItem>();
		si.add(new SelectItem(new String(), CommonUtil.getMessage("campaigns.all", null)));

		for ( MarketingContent m : list ) {
			si.add(new SelectItem(m.getName(), m.getName()));
		}

		return si;
	}

	/**
	 * Get marketing contents SI(String,Long)
	 * @return
	 * @throws SQLException
	 */
	public ArrayList<SelectItem> getMarketingContentsLongSI() throws SQLException {

		ArrayList<MarketingContent> list = MarketingManager.getMarketingContents(null, getWriterIdForSkin());
		ArrayList<SelectItem> si = new ArrayList<SelectItem>();
		si.add(new SelectItem(new Long(0), CommonUtil.getMessage("campaigns.all", null)));

		for ( MarketingContent m : list ) {
			si.add(new SelectItem(new Long(m.getId()), m.getName()));
		}

		return si;
	}

	/**
	 * Get marketing landing pages SI(String,String)
	 * @return
	 * @throws SQLException
	 */
	public ArrayList<SelectItem> getMarketingLandingPagesSI() throws SQLException {

		ArrayList<MarketingLandingPage> list = MarketingManager.getMarketingLandingPages(null, getWriterIdForSkin());
		ArrayList<SelectItem> si = new ArrayList<SelectItem>();
		HashSet<String> mlpIds = new HashSet<String>();
		si.add(new SelectItem(new String(), CommonUtil.getMessage("campaigns.all", null)));

		for ( MarketingLandingPage m : list ) {
			if (!mlpIds.contains(m.getName())) {
				mlpIds.add(m.getName());
				si.add(new SelectItem(m.getName(), m.getName()));
			}			
		}

		return si;
	}

	/**
	 * Get marketing landing pages SI(Long,String)
	 * @return
	 * @throws SQLException
	 */
	public ArrayList<SelectItem> getMarketingLandingPagesLongSI() throws SQLException {

		ArrayList<MarketingLandingPage> list = MarketingManager.getMarketingLandingPages(null, getWriterIdForSkin());
		ArrayList<SelectItem> si = new ArrayList<SelectItem>();
		si.add(new SelectItem(new Long(0), CommonUtil.getMessage("campaigns.all", null)));

		for ( MarketingLandingPage m : list ) {
			si.add(new SelectItem(new Long(m.getId()), m.getName()));
		}

		return si;
	}

	/**
	 * Get marketing landing pages SI(Long,String) that have marketing_combinations
	 * @return landing pages SI for the writer skin id
	 * @throws SQLException
	 */
	public ArrayList<SelectItem> getMarketingLandingPagesLongByWriterSkinsSI() throws SQLException {

		ArrayList<MarketingLandingPage> list = MarketingManager.getMarketingLandingPagesByWriterSkins();
		ArrayList<SelectItem> si = new ArrayList<SelectItem>();
		si.add(new SelectItem(new Long(0), CommonUtil.getMessage("campaigns.all", null)));

		for ( MarketingLandingPage m : list ) {
			si.add(new SelectItem(new Long(m.getId()), m.getName()));
		}

		return si;
	}

	/**
	 * set marketing campaigns pages SI(Long,String)
	 * @throws SQLException
	 */
	public void setMarketingCampaignsLongSI() throws SQLException {

		ArrayList<MarketingCampaign> list = MarketingManager.getMarketingCampaigns(null, null, 0, null, null, 0, 0, 0, 0);
		ArrayList<SelectItem> marketingCampaignsSI = new ArrayList<SelectItem>();
		
		ArrayList<SelectItem> marketingCampaignsSIByWriterID = new ArrayList<SelectItem>();
		ArrayList<Integer> skinsCampaignWriter = new ArrayList<Integer>();
		
		for ( MarketingCampaign m : list ) {
			marketingCampaignsSI.add(new SelectItem(new Long(m.getId()), m.getName()));
			
			skinsCampaignWriter = WritersManager.getWriterSkins(m.getWriterId());
			skinsCampaignWriter.retainAll(skins);
			if (skinsCampaignWriter.size() > 0) {
				marketingCampaignsSIByWriterID.add(new SelectItem(new Long(m.getId()), m.getName()));
			}
		}

        Collections.sort(marketingCampaignsSI, new CommonUtil.selectItemComparator());
        selectItems.put("marketingCampaignsSI", marketingCampaignsSI);
        marketingCampaignsSI.add(0, new SelectItem(ConstantsBase.ALL_CAMPAIGNS, CommonUtil.getMessage("campaigns.all", null)));
        selectItems.put("allMarketingCampaignsSI", marketingCampaignsSI);
        
        Collections.sort(marketingCampaignsSIByWriterID, new CommonUtil.selectItemComparator());
        selectItems.put("marketingCampaignsSIByWriterID", marketingCampaignsSIByWriterID);
        new SelectItem(ConstantsBase.ALL_CAMPAIGNS, CommonUtil.getMessage("campaigns.all", null));
        selectItems.put("allMarketingCampaignsSIByWriterID", marketingCampaignsSIByWriterID);
	}

	/**
	 * set marketing campaigns pages SI(Long,String)
	 * @throws SQLException
	 */
	public void setMarketingCampaignsBySkinsLongSI() throws SQLException {

		ArrayList<MarketingCampaign> list = MarketingManager.getMarketingCampaigns(null, null, 0, null, null, 0, writer.getId(), 0, 0);
		ArrayList<SelectItem> marketingCampaignsSalesSI = new ArrayList<SelectItem>();
		ArrayList<SelectItem> marketingCampaignsBySkinsSI = new ArrayList<SelectItem>();

		for ( MarketingCampaign m : list ) {
			marketingCampaignsSalesSI.add(new SelectItem(new Long(m.getId()), String.valueOf(m.getId()) + '_' + m.getName()));
			marketingCampaignsBySkinsSI.add(new SelectItem(new Long(m.getId()), m.getName()));
		}

        Collections.sort(marketingCampaignsSalesSI, new CommonUtil.selectItemComparator());
        marketingCampaignsSalesSI.add(0, new SelectItem(ConstantsBase.ALL_CAMPAIGNS, CommonUtil.getMessage("campaigns.all", null)));
        selectItems.put("marketingCampaignsSalesSI", marketingCampaignsSalesSI);
        
        Collections.sort(marketingCampaignsBySkinsSI, new CommonUtil.selectItemComparator());
        marketingCampaignsBySkinsSI.add(0, new SelectItem(ConstantsBase.ALL_CAMPAIGNS, CommonUtil.getMessage("campaigns.all", null)));
        selectItems.put("marketingCampaignsBySkinsSI", marketingCampaignsBySkinsSI);
	}

	/**
	 * Get marketing campaigns pages SI(Long,String)
	 * @return
	 * @throws SQLException
	 */
	public ArrayList<SelectItem> getMarketingCampaignsLongSI() throws SQLException {

		return selectItems.get("marketingCampaignsSI");
	}
	
	public ArrayList<SelectItem> getMarketingCampaignsLongSIByWriterID() throws SQLException {

		return selectItems.get("marketingCampaignsSIByWriterID");
	}

	/**
	 * Get marketing campaigns pages SI(Long,String) including an All field
	 * @return
	 * @throws SQLException
	 */
	public ArrayList<SelectItem> getAllMarketingCampaignsLongSI() throws SQLException {

//        ArrayList<SelectItem> listSI = new ArrayList<SelectItem>();
//        listSI.add(new SelectItem(ConstantsBase.ALL_CAMPAIGNS, CommonUtil.getMessage("campaigns.all", null)));
//        listSI.addAll(marketingCampaignsSI);
//
//        return listSI;
		return selectItems.get("allMarketingCampaignsSI");
	}
	
	public ArrayList<SelectItem> getAllMarketingCampaignsLongSIByWriterID() throws SQLException {

//        ArrayList<SelectItem> listSI = new ArrayList<SelectItem>();
//        listSI.add(new SelectItem(ConstantsBase.ALL_CAMPAIGNS, CommonUtil.getMessage("campaigns.all", null)));
//        listSI.addAll(marketingCampaignsSIByWriterID);
//
//        return listSI;
		return selectItems.get("allMarketingCampaignsSIByWriterID");
	}

	/**
	 * Get marketing campaigns pages SI(Long,String) including an All field
	 * @return
	 * @throws SQLException
	 */
	public ArrayList<SelectItem> getAllMarketingCampaignsBySkinsLongSI() throws SQLException {

//        ArrayList<SelectItem> listSI = new ArrayList<SelectItem>();
//        listSI.add(new SelectItem(ConstantsBase.ALL_CAMPAIGNS, CommonUtil.getMessage("campaigns.all", null)));
//        listSI.addAll(marketingCampaignsBySkinsSI);
//
//        return listSI;
		return selectItems.get("marketingCampaignsBySkinsSI");
	}

	/**
	 * Get marketing campaigns pages SI(Long,String) including an All field
	 * @return
	 * @throws SQLException
	 */
	public ArrayList<SelectItem> getAllSalesMarketingCampaignsLongSI() throws SQLException {

//        ArrayList<SelectItem> listSI = new ArrayList<SelectItem>();
//        listSI.add(new SelectItem(ConstantsBase.ALL_CAMPAIGNS, CommonUtil.getMessage("campaigns.all", null)));
//        listSI.addAll(marketingCampaignsSalesSI);
//
//        return listSI;
		
		return selectItems.get("marketingCampaignsSalesSI");
	}


	/**
	 * Get marketing campaigns pages SI(Long,String)
	 * @return
	 * @throws SQLException
	 */
	public void setMarketingCombinationsLongSI() throws SQLException {

		//ArrayList<MarketingCombination> list = MarketingManager.getMarketingCombinations(0, null, null, 0, 0, null, null, null, null, 0, 0);
//		ArrayList<MarketingCombination> list = MarketingManager.getMarketingCombinations();
//		marketingCombinationsSI = new ArrayList<SelectItem>();
//
//
//		for ( MarketingCombination m : list ) {
//			marketingCombinationsSI.add(new SelectItem(new Long(m.getId()), m.getId() + "_" + m.getCampaignName()));
//		}

		ArrayList<SelectItem> marketingCombinationsSI = MarketingManager.getCombinationIdWithCampaigns();

        Collections.sort(marketingCombinationsSI, new CommonUtil.selectItemComparator());
        marketingCombinationsSI.add(0, new SelectItem(new Long(0), CommonUtil.getMessage("campaigns.all", null)));
        selectItems.put("marketingCombinationsSI", marketingCombinationsSI);
	}

	/**
	 * Get marketing campaigns pages SI(Long,String)
	 * @return
	 * @throws SQLException
	 */
	public ArrayList<SelectItem> getMarketingCombinationsLongSI() throws SQLException {

//        ArrayList<SelectItem> listSI = new ArrayList<SelectItem>();
//        listSI.add(new SelectItem(new Long(0), CommonUtil.getMessage("campaigns.all", null)));
//        listSI.addAll(marketingCombinationsSI);
//
//		return listSI;
		return selectItems.get("marketingCombinationsSI");
	}


	/**
	 * Get marketing sources pages SI(Long,String)
	 * @return
	 * @throws SQLException
	 */
	public ArrayList<SelectItem> getMarketingSourcesLongSI() throws SQLException {

		ArrayList<MarketingSource> list = MarketingManager.getMarketingSources(null,true,  getWriterIdForSkin());
		ArrayList<SelectItem> si = new ArrayList<SelectItem>();

		for ( MarketingSource m : list ) {
			si.add(new SelectItem(new Long(m.getId()), m.getName()));
		}

        Collections.sort(si, new CommonUtil.selectItemComparator());

		return si;
	}

	/**
	 * Get marketing sources pages SI(Long,String)
	 * @return
	 * @throws SQLException
	 */
	public ArrayList<SelectItem> getMarketingSourcesSI() throws SQLException {

		ArrayList<MarketingSource> list = MarketingManager.getMarketingSources(null,false, getWriterIdForSkin());
		ArrayList<SelectItem> si = new ArrayList<SelectItem>();
		si.add(new SelectItem(new Long(0),"All"));
		for ( MarketingSource m : list ) {
			si.add(new SelectItem(new Long(m.getId()), m.getName()));
		}

		return si;
	}

	/**
	 * Get marketing sources pages SI(Long,String) by writer skin id and have marketing_combinations
	 * @return
	 * @throws SQLException
	 */
	public ArrayList<SelectItem> getMarketingSourcesByWriterSkinIdSI() throws SQLException {

		ArrayList<MarketingSource> list = MarketingManager.getMarketingSourcesByWriterSkinId();
		ArrayList<SelectItem> si = new ArrayList<SelectItem>();
		si.add(new SelectItem(new Long(0),"All"));
		for ( MarketingSource m : list ) {
			si.add(new SelectItem(new Long(m.getId()), m.getName()));
		}

		return si;
	}

	public ArrayList<SelectItem> getAllMarketingCampaignManagers() throws SQLException {

		ArrayList<MarketingCampaignManagers> list = MarketingManager.getAllMarketingCampaignManagers();
		ArrayList<SelectItem> si = new ArrayList<SelectItem>();
		si.add(new SelectItem(new Long(0),"All"));
		for ( MarketingCampaignManagers m : list ) {
			si.add(new SelectItem(new Long(m.getCampaignManagerId()), m.getCampaignManagerUserame()));
		}

		return si;
	}

	/**
	 * Get marketing mediums SI without all(String,String)
	 * @return
	 * @throws SQLException
	 */
	public ArrayList<SelectItem> getMarketingMediumsNoAllSI() throws SQLException {

		ArrayList<MarketingMedium> list = MarketingManager.getMarketingMediums(null, getWriterIdForSkin());
		ArrayList<SelectItem> si = new ArrayList<SelectItem>();

		for ( MarketingMedium m : list ) {
			si.add(new SelectItem(new Long(m.getId()), m.getName()));
		}

		return si;
	}

	/**
	 * Get marketing contents SI without all(String,String)
	 * @return
	 * @throws SQLException
	 */
	public ArrayList<SelectItem> getMarketingContentsNoAllSI() throws SQLException {

		ArrayList<MarketingContent> list = MarketingManager.getMarketingContents(null, getWriterIdForSkin());
		ArrayList<SelectItem> si = new ArrayList<SelectItem>();

		for ( MarketingContent m : list ) {
			si.add(new SelectItem(new Long(m.getId()), m.getName()));
		}

		return si;
	}

	/**
	 * Get marketing sizes SI(Long,String)
	 * @return
	 * @throws SQLException
	 */
	public ArrayList<SelectItem> getMarketingSizesLongSI() throws SQLException {

		ArrayList<MarketingSize> list = MarketingManager.getMarketingSizes(null, null, getWriterIdForSkin());
		ArrayList<SelectItem> si = new ArrayList<SelectItem>();
		si.add(new SelectItem(new Long(0), CommonUtil.getMessage("marketing.empty", null)));

		for ( MarketingSize m : list ) {
			si.add(new SelectItem(new Long(m.getId()), m.getVerticalSize() + "X" + m.getHorizontalSize()));
		}

		return si;
	}

	/**
	 * Get marketing types SI(Long,String)
	 * @return
	 * @throws SQLException
	 */
	public ArrayList<SelectItem> getMarketingTypesLongSI() throws SQLException {

		ArrayList<MarketingType> list = MarketingManager.getMarketingTypes(null, getWriterIdForSkin());
		ArrayList<SelectItem> si = new ArrayList<SelectItem>();
		si.add(new SelectItem(new Long(0), CommonUtil.getMessage("marketing.empty", null)));

		for ( MarketingType m : list ) {
			si.add(new SelectItem(new Long(m.getId()), m.getName()));
		}

		return si;
	}

	/**
	 * Get marketing locations SI(Long,String)
	 * @return
	 * @throws SQLException
	 */
	public ArrayList<SelectItem> getMarketingLocationsLongSI() throws SQLException {

		ArrayList<MarketingLocation> list = MarketingManager.getMarketingLocations(null, getWriterIdForSkin());
		ArrayList<SelectItem> si = new ArrayList<SelectItem>();
		si.add(new SelectItem(new Long(0), CommonUtil.getMessage("marketing.empty", null)));

		for ( MarketingLocation m : list ) {
			si.add(new SelectItem(new Long(m.getId()), m.getLocation()));
		}

		return si;
	}

	/**
	 * Get marketing landing pages SI without all(String,String)
	 * @return
	 * @throws SQLException
	 */
	public ArrayList<SelectItem> getMarketingLandingPagesNoAllSI() throws SQLException {

		ArrayList<MarketingLandingPage> list = MarketingManager.getMarketingLandingPages(null, getWriterIdForSkin());
		ArrayList<SelectItem> si = new ArrayList<SelectItem>();

		for ( MarketingLandingPage m : list ) {
			si.add(new SelectItem(new Long(m.getId()), m.getName()));
		}

		return si;
	}

	/**
	 * Get marketing pixels SI without all(long,String)
	 * @return
	 * @throws SQLException
	 */
	public ArrayList<SelectItem> getMarketingPixelsNoAllSI() throws SQLException {

		ArrayList<MarketingPixel> list = MarketingManager.getMarketingPixels(null, getWriterIdForSkin());
		ArrayList<SelectItem> si = new ArrayList<SelectItem>();

		for ( MarketingPixel m : list ) {
			si.add(new SelectItem(new Long(m.getId()), m.getName()));
		}
        Collections.sort(si, new CommonUtil.selectItemComparator());

		return si;
	}

	/**
	 * Get marketing pixels SI with all(long,String)
	 * @return
	 * @throws SQLException
	 */
	public ArrayList<SelectItem> getMarketingPixelsAllSI() throws SQLException {

		ArrayList<MarketingPixel> list = MarketingManager.getMarketingPixels(null, getWriterIdForSkin());
		ArrayList<SelectItem> si = new ArrayList<SelectItem>();

		for ( MarketingPixel m : list ) {
			si.add(new SelectItem(new Long(m.getId()), m.getName()));
		}

        Collections.sort(si, new CommonUtil.selectItemComparator());
        //add all
        ArrayList<SelectItem> listSI = new ArrayList<SelectItem>();
        listSI.add(new SelectItem(new Long(0), CommonUtil.getMessage("marketing.empty", null)));
        listSI.addAll(si);
		return listSI;
	}


	public ArrayList<SelectItem> getInvestmentTypesSI() throws SQLException {
		ArrayList<SelectItem> si = new ArrayList<SelectItem>();
		ArrayList<InvestmentType> investmentTypes = InvestmentsManagerBase.getAllInvestmentTypes();

		SelectItem tmp = null;
		for(InvestmentType invType : investmentTypes){
			tmp = new SelectItem(new Long(invType.getId()),CommonUtil.getMessage(invType.getDisplayName(), null));
			si.add(tmp);
		}
        Collections.sort(si, new CommonUtil.selectItemComparator());
        //add all
        ArrayList<SelectItem> listSI = new ArrayList<SelectItem>();
        listSI.add(new SelectItem(new Long(0),"All"));
        listSI.addAll(si);

		return listSI;
	}

	public ArrayList<SelectItem> getInvestmentClosingHoursSI() {
		ArrayList<SelectItem> si = new ArrayList<SelectItem>();
		SelectItem tmp = new SelectItem(new Long(99),"--");
		si.add(tmp);
		for(long i=0;i<24;i++){
			tmp = new SelectItem();
			tmp.setValue(new Long(i));
			if(i<10){
				tmp.setLabel("0"+i);
			}else{
				tmp.setLabel(String.valueOf(i));
			}
			si.add(tmp);
		}
		return si;

	}

	public ArrayList<SelectItem> getInvestmentClosingMinutesSI() {
		ArrayList<SelectItem> si = new ArrayList<SelectItem>();
		SelectItem tmp = new SelectItem(new Long(99),"--");
		si.add(tmp);
		tmp = new SelectItem(new Long(0),"00");
		si.add(tmp);
		tmp = new SelectItem(new Long(5),"05");
		si.add(tmp);
		tmp = new SelectItem(new Long(10),"10");
		si.add(tmp);
		tmp = new SelectItem(new Long(15),"15");
		si.add(tmp);
		tmp = new SelectItem(new Long(20),"20");
		si.add(tmp);
		tmp = new SelectItem(new Long(25),"25");
		si.add(tmp);
		tmp = new SelectItem(new Long(30),"30");
		si.add(tmp);
		tmp = new SelectItem(new Long(35),"35");
		si.add(tmp);
		tmp = new SelectItem(new Long(40),"40");
		si.add(tmp);
		tmp = new SelectItem(new Long(45),"45");
		si.add(tmp);
		tmp = new SelectItem(new Long(50),"50");
		si.add(tmp);
		tmp = new SelectItem(new Long(55),"55");
		si.add(tmp);
		return si;
	}

	/**
	 * @return the selected items list containing all investments writers (WEB, MOBILE or both)
	 */
	public List<SelectItem> getInvestmentWritersSI() {
		List<SelectItem> writers = new ArrayList<SelectItem>(3);
		Writer webWriter = allWritersMap.get(1L); // WEB
		Writer mobileWriter = allWritersMap.get(200L); // MOBILE
        Writer apiGatewayWriter = allWritersMap.get(868L); // api_gateway

		writers.add(new SelectItem(-1L, "All"));
		writers.add(new SelectItem(webWriter.getId(), webWriter.getUserName()));
		writers.add(new SelectItem(mobileWriter.getId(), mobileWriter.getUserName()));
        writers.add(new SelectItem(apiGatewayWriter.getId(), apiGatewayWriter.getUserName()));

		return writers;
	}
	
	/**
	 * One touch SI
	 * @return
	 * @throws SQLException
	 */
	public ArrayList<SelectItem> getOneTouchAllSI() throws SQLException {
		ArrayList<SelectItem> si = new ArrayList<SelectItem>();
		si.add(new SelectItem(new Long(-1),"All"));
        si.add(new SelectItem(new Long(0), CommonUtil.getMessage("opportunities.one.touch.down", null, Utils.getWriterLocale())));
		si.add(new SelectItem(new Long(1), CommonUtil.getMessage("opportunities.one.touch.up", null, Utils.getWriterLocale())));
		return si;
	}

	/**
	 * @return the populationTypesSI
	 */
	public ArrayList<SelectItem> getPopulationTypesSI() {
		return translateSI(populationTypesSI);
	}

	/**
	 * @return the populationTypesAllSI
	 */
	public ArrayList<SelectItem> getPopulationTypesAllSI() {
		ArrayList<SelectItem> si = new ArrayList<SelectItem>();
		si.add(new SelectItem(new Long(0),"opportunities.all"));
		si.addAll(populationTypesSI);
		return translateSI(si);
	}

	/**
	 * @return population types list by department
	 * @throws SQLException
	 */
	public ArrayList<SelectItem> getPopulationTypesByDept() throws SQLException {
		ArrayList<PopulationType> populationsTypes = PopulationsManager.getPopulationsTypesByDeps(getSkinsToString());


		ArrayList<SelectItem> si = new ArrayList<SelectItem>();
		si.add(new SelectItem(new Long(0),"general.all"));

		for (PopulationType pt : populationsTypes) {
			si.add(new SelectItem(pt.getId(),pt.getName()));
		}
		return translateSI(si);
	}

	/**
	 * @return population types list by department
	 * @throws SQLException
	 */
	public ArrayList<SelectItem> getPopulationTypesStrByDept() throws SQLException {
		ArrayList<PopulationType> populationsTypes = PopulationsManager.getPopulationsTypesByDeps(getSkinsToString());


		ArrayList<SelectItem> si = new ArrayList<SelectItem>();

		for (PopulationType pt : populationsTypes) {
			si.add(new SelectItem(String.valueOf(pt.getId()),pt.getName()));
		}
		return translateSI(si);
	}

	/**
	 * @return population types list by department
	 * @throws SQLException
	 */
	public ArrayList<SelectItem> getPopulationTypesStrBySalesType() throws SQLException {
		ArrayList<PopulationType> populationsTypes = PopulationsManager.getPopulationTypesStrBySalesType(getSkinsToString());


		ArrayList<SelectItem> si = new ArrayList<SelectItem>();

		for (PopulationType pt : populationsTypes) {
			si.add(new SelectItem(String.valueOf(pt.getId()),pt.getName()));
		}
		return translateSI(si);
	}

	/**
	 * @return population types list by department
	 * @throws SQLException
	 */
	public ArrayList<SelectItem> getPopulationTypesStrBySalesTypeForTracking() throws SQLException {
		ArrayList<PopulationType> populationsTypes = PopulationsManager.getPopulationTypesStrBySalesTypeForTracking(getSkinsToString());


		ArrayList<SelectItem> si = new ArrayList<SelectItem>();

		for (PopulationType pt : populationsTypes) {
			si.add(new SelectItem(String.valueOf(pt.getId()),pt.getName()));
		}
		return translateSI(si);
	}

	/**
	 * @return the populationsSI + All field
	 */
	public ArrayList getPopulationsForWriterAllSI() throws SQLException {
		ArrayList<SelectItem> list = new ArrayList<SelectItem>();
		ArrayList<Population> writerPopulations = PopulationsManager.getPopulationsForWriter();

		SelectItem item = null;
		for(Population population : writerPopulations){
			item = new SelectItem(new Long(population.getId()),population.getName());
			list.add(item);
		}

        Collections.sort(list, new CommonUtil.selectItemComparator());
        //add all
        ArrayList<SelectItem> listSI = new ArrayList<SelectItem>();
        listSI.add(new SelectItem(new Long(ConstantsBase.ALL_FILTER_ID), CommonUtil.getMessage("general.all", null,localInfo.getLocale())));
        listSI.addAll(list);

		return listSI;
	}

	/**
	 * @return the populationsSI
	 */
	public ArrayList getPopulationsForWriterSI() throws SQLException {
		ArrayList<SelectItem> list = new ArrayList<SelectItem>();
		ArrayList<Population> writerPopulations = PopulationsManager.getPopulationsForWriter();

		for(Population population : writerPopulations){
			SelectItem item = new SelectItem(String.valueOf(population.getId()),population.getName());
			list.add(item);
		}

        Collections.sort(list, new CommonUtil.selectItemComparator());
		return list;
	}


	public ArrayList<SelectItem> getSalesWriters(Long skinId, long populationType_typeId, long populationTypeId) {
		if (populationTypeId == PopulationsManagerBase.POP_TYPE_CALLME) {
				long writerSalesType = Utils.getWriter().writer.getSales_type();

				if (writerSalesType == PopulationsManagerBase.SALES_TYPE_RETENTION) {
					return getSalesRetentionWriters(skinId);
				} else
				if (writerSalesType == PopulationsManagerBase.SALES_TYPE_CONVERSION) {
					return getSalesConversionWriters(skinId);
				} else {
					return getSalesWriters(skinId);
				}
		} else if (populationType_typeId == PopulationsManagerBase.SALES_TYPE_RETENTION || populationType_typeId == PopulationsManagerBase.SALES_TYPE_IMMEDIATE_TREATMENT) {
			return getSalesWriters(skinId);
		} else if (populationType_typeId == PopulationsManagerBase.SALES_TYPE_CONVERSION) {
			return getSalesConversionWriters(skinId);
		} else {
			return getSalesWriters(skinId);
		}
	}

	/**
	 * Get Sales writers by skinId
	 * @param skinId
	 * @return
	 */
	public ArrayList<SelectItem> getSalesWriters(Long skinId) {
		ArrayList<SelectItem> list = new ArrayList<SelectItem>();
		Iterator<Long> iter = salesReps.keySet().iterator();
		while(iter.hasNext()) {
			Long key = iter.next();
			Writer w = salesReps.get(key);
			if(w.getSkins().contains(new Integer(skinId.intValue()))) {
				list.add(new SelectItem(new Long(w.getId()),w.getUserName()));
			}
		}
		Collections.sort(list, new CommonUtil.selectItemComparator());
		return list;
	}

	/**
	 * Get Sales writers by skinId
	 * @param skinId
	 * @return
	 */
	public ArrayList<SelectItem> getSalesConversionWriters(Long skinId) {
		ArrayList<SelectItem> list = new ArrayList<SelectItem>();
		Iterator<Long> iter = salesConversionReps.keySet().iterator();
		while(iter.hasNext()) {
			Long key = iter.next();
			Writer w = salesConversionReps.get(key);
			if(w.getSkins().contains(new Integer(skinId.intValue()))) {
				list.add(new SelectItem(new Long(w.getId()),w.getUserName()));
			}
		}
		Collections.sort(list, new CommonUtil.selectItemComparator());
		return list;
	}

	/**
	 * Get Sales writers by skinId
	 * @param skinId
	 * @return
	 */
	public ArrayList<SelectItem> getSalesRetentionWriters(Long skinId) {
		ArrayList<SelectItem> list = new ArrayList<SelectItem>();
		Iterator<Long> iter = salesRetentionReps.keySet().iterator();
		while(iter.hasNext()) {
			Long key = iter.next();
			Writer w = salesRetentionReps.get(key);
			if(w.getSkins().contains(new Integer(skinId.intValue()))) {
				list.add(new SelectItem(new Long(w.getId()),w.getUserName()));
			}
		}
		Collections.sort(list, new CommonUtil.selectItemComparator());
		return list;
	}


	/**
	 * Get retention writers by skinId
	 * @param skinId
	 * @return
	 */
	public ArrayList<SelectItem> getRetentionWriters(Long skinId) {
		ArrayList<SelectItem> list = new ArrayList<SelectItem>();
		Iterator<Long> iter = ApplicationData.getRetentionWriters().keySet().iterator();
		while(iter.hasNext()) {
			Long key = iter.next();
			Writer w = ApplicationData.getRetentionWriters().get(key);
			if(w.getSkins().contains(new Integer(skinId.intValue()))) {
				list.add(new SelectItem(new Long(w.getId()),w.getUserName()));
			}
		}
		Collections.sort(list, new CommonUtil.selectItemComparator());
		return list;
	}

	/**
	 * Get retention writers by skinId
	 * @param skinId
	 * @return
	 */
	public static ArrayList<SelectItem> getRetentionWritersSortedAll(Long skinId) {
		ArrayList<SelectItem> list = new ArrayList<SelectItem>();
		ArrayList<Writer> fullWritersList = ApplicationData.getRetentionWritersList();

		for(Writer w:fullWritersList) {
			if(w.getSkins().contains(new Integer(skinId.intValue()))) {
				list.add(new SelectItem(new Long(w.getId()),w.getUserName()));
			}
		}

        Collections.sort(list, new CommonUtil.selectItemComparator());
        //add all
        ArrayList<SelectItem> listSI = new ArrayList<SelectItem>();
        listSI.add(new SelectItem(new Long(ConstantsBase.ALL_FILTER_ID),
                CommonUtil.getMessage("general.all", null)));
        listSI.addAll(list);
		return listSI;
	}

	public ArrayList<SelectItem> getSalesWriters() {
		ArrayList<SelectItem> list = new ArrayList<SelectItem>();
		ArrayList<SelectItem> listTmp = new ArrayList<SelectItem>();
		Iterator<Long> iter = salesReps.keySet().iterator();
		Long key;
		Writer writer;
		while(iter.hasNext()) {
			key = iter.next();
			writer = salesReps.get(key);
			listTmp.add(new SelectItem(new Long(writer.getId()),writer.getUserName()));
		}
		Collections.sort(listTmp, new CommonUtil.selectItemComparator());
		list.addAll(listTmp);
		return list;
	}

	public ArrayList<SelectItem> getSalesConversionWriters() {
		ArrayList<SelectItem> list = new ArrayList<SelectItem>();
		ArrayList<SelectItem> listTmp = new ArrayList<SelectItem>();
		Iterator<Long> iter = salesConversionReps.keySet().iterator();
		Long key;
		Writer writer;
		while(iter.hasNext()) {
			key = iter.next();
			writer = salesConversionReps.get(key);
			listTmp.add(new SelectItem(new Long(writer.getId()),writer.getUserName()));
		}
		Collections.sort(listTmp, new CommonUtil.selectItemComparator());
		list.addAll(listTmp);
		return list;
	}

	public ArrayList<SelectItem> getSalesRetentionWriters() {
		ArrayList<SelectItem> list = new ArrayList<SelectItem>();
		ArrayList<SelectItem> listTmp = new ArrayList<SelectItem>();
		Iterator<Long> iter = salesRetentionReps.keySet().iterator();
		Long key;
		Writer writer;
		while(iter.hasNext()) {
			key = iter.next();
			writer = salesRetentionReps.get(key);
				listTmp.add(new SelectItem(new Long(writer.getId()),writer.getUserName()));
		}
		Collections.sort(listTmp, new CommonUtil.selectItemComparator());
		list.addAll(listTmp);
		return list;
	}

	public ArrayList<SelectItem> getRetentionWriters() {
		ArrayList<SelectItem> list = new ArrayList<SelectItem>();
		list.addAll(getSalesWriters());
		return list;
	}

	public ArrayList<SelectItem> getRetentionWritersAll() {
		ArrayList<SelectItem> list = new ArrayList<SelectItem>();
		list.add(new SelectItem(new Long(ConstantsBase.ALL_FILTER_ID),
				CommonUtil.getMessage("general.all", null,localInfo.getLocale())));
		list.add(new SelectItem(new Long(ConstantsBase.ALL_REPS_FILTER_ID),
				CommonUtil.getMessage("general.all.reps", null,localInfo.getLocale())));
		list.add(new SelectItem(new Long(ConstantsBase.NONE_FILTER_ID),
				CommonUtil.getMessage("general.none", null,localInfo.getLocale())));
		list.addAll(getSalesWriters());
		return list;
	}

	public ArrayList<SelectItem> getSalesRetentionWritersAll() {
		ArrayList<SelectItem> list = new ArrayList<SelectItem>();
		list.add(new SelectItem(new Long(ConstantsBase.ALL_FILTER_ID),
				CommonUtil.getMessage("general.all", null,localInfo.getLocale())));
		list.add(new SelectItem(new Long(ConstantsBase.ALL_REPS_FILTER_ID),
				CommonUtil.getMessage("general.all.reps", null,localInfo.getLocale())));
		list.addAll(getSalesRetentionWriters());
		return list;
	}

	public ArrayList<SelectItem> getSalesConversionWritersAll() {
		ArrayList<SelectItem> list = new ArrayList<SelectItem>();
		list.add(new SelectItem(new Long(ConstantsBase.ALL_FILTER_ID),
				CommonUtil.getMessage("general.all", null,localInfo.getLocale())));
		list.add(new SelectItem(new Long(ConstantsBase.ALL_REPS_FILTER_ID),
				CommonUtil.getMessage("general.all.reps", null,localInfo.getLocale())));
		list.addAll(getSalesConversionWriters());
		return list;
	}

	public ArrayList<SelectItem> getAssignedRepList(boolean isIncludeNone) throws SQLException {
		ArrayList<SelectItem> repSI = new ArrayList<SelectItem>();
		SelectItem all = new SelectItem(new Long(ConstantsBase.ALL_FILTER_ID),
				CommonUtil.getMessage("general.all", null,localInfo.getLocale()));
		if (isIncludeNone) {
			SelectItem noneItem = new SelectItem(ConstantsBase.NONE_FILTER_ID, CommonUtil.getMessage(ConstantsBase.NONE_FILTER_KEY, null));
			repSI.add(noneItem);
		}
		repSI.add(all);
		repSI.addAll(PopulationsManager.getAssignedRepList(false));
		return repSI;
	}
	
	public ArrayList<SelectItem> getAssignedRepList() throws SQLException {
		return getAssignedRepList(false);
	}
	
	public ArrayList<SelectItem> getAssignedRepListAndNone() throws SQLException {
		return getAssignedRepList(true);
	}

	public ArrayList<SelectItem> getAssignedRepListByDepartment() throws SQLException {
		ArrayList<SelectItem> repSI = new ArrayList<SelectItem>();
		SelectItem all = new SelectItem(new Long(ConstantsBase.ALL_FILTER_ID),
				CommonUtil.getMessage("general.all", null,localInfo.getLocale()));
		repSI.add(all);
		repSI.addAll(PopulationsManager.getAssignedRepList(true));
		return repSI;
	}

	public ArrayList<SelectItem> setPopEntriesWritersFilterSI() {
		popEntriesWritersFilterSI = new ArrayList<SelectItem>();
		SelectItem all = new SelectItem(new Long(ConstantsBase.ALL_FILTER_ID),
				CommonUtil.getMessage("general.all", null,localInfo.getLocale()));
		SelectItem locked = new SelectItem(new Long(ConstantsBase.POPULATION_ENTRIES_LOCKED_BY_WRITER),
				CommonUtil.getMessage("population.entry.writers.filter.locked", null,localInfo.getLocale()));
		SelectItem assigned = new SelectItem(new Long(ConstantsBase.POPULATION_ENTRIES_ASSIGNED_TO_WRITER),
				CommonUtil.getMessage("population.entry.writers.filter.assigned", null,localInfo.getLocale()));
		popEntriesWritersFilterSI.add(all);
		popEntriesWritersFilterSI.add(locked);
		popEntriesWritersFilterSI.add(assigned);
		return popEntriesWritersFilterSI;
	}

	public ArrayList<SelectItem> getAssignedRepListBySalesType(long salesType) throws SQLException {
		ArrayList<SelectItem> repSI = new ArrayList<SelectItem>();
		SelectItem all = new SelectItem(new Long(ConstantsBase.ALL_FILTER_ID),
				CommonUtil.getMessage("general.all", null,localInfo.getLocale()));
		repSI.add(all);
		repSI.addAll(PopulationsManager.getAssignedRepListBySalesType(salesType));
		return repSI;
	}

    /**
	 * @return the getPopEntriesWritersFilterSI
	 */
	public ArrayList<SelectItem> getPopEntriesWritersFilterSI() {
		return popEntriesWritersFilterSI;
	}

    /**
     * get bonuse type select list
     * @return ArrayList
     */
    public ArrayList getBonusTypeSI() {
        return bonusTypesSI;
    }

    /**
     * get bonuse states select list
     * @return ArrayList
     */
    public ArrayList getBonusStatesSI() {
        return bonusStatesSI;
    }

    /**
     * get user actions select list
     * @return ArrayList
     */
    /*public ArrayList getUserActionsSI() {
        return userActionsSI;
    }*/

    /**
     * get bonuse type select list with all option
     * @return ArrayList
     */
	public ArrayList<SelectItem> getBonusTypeSIAll() {
		ArrayList<SelectItem> si = new ArrayList<SelectItem>();
		si.add(new SelectItem(new Long(0),"All"));
		si.addAll(bonusTypesSI);
		return si;
	}

    /**
     * get bonuse states select list with all option
     * @return ArrayList
     */
	public ArrayList<SelectItem> getBonusStatesSIAll() {
		ArrayList<SelectItem> si = new ArrayList<SelectItem>();
		si.add(new SelectItem(new Long(0),"All"));
		si.addAll(bonusStatesSI);
		return si;
	}

    /**
     * get user actions select list with all option
     * @return ArrayList
     */
	/*public ArrayList<SelectItem> getUserActionsSIAll() {
		ArrayList<SelectItem> si = new ArrayList<SelectItem>();
		si.add(new SelectItem(new Long(0),"All"));
		si.addAll(userActionsSI);
		return si;
	}*/

	/**
	 * get bonus SI list by writer group
	 * @return
	 * @throws SQLException
	 */
	public ArrayList getBonusesByWriterGroup() throws SQLException {
		FacesContext context = FacesContext.getCurrentInstance();
		User user = (User) context.getApplication().createValueBinding(Constants.BIND_USER).getValue(context);
		return BonusManager.getBonusByWriterGroupAPopulation(writer.getGroupId(), 0, getSkinsToString(), user.getSkinId());
	}

	/**
	 * get bonus SI list by writer group and population type
	 * @return
	 * @throws SQLException
	 */
	public ArrayList getBonusesByWriterGroupPop() throws SQLException {
		FacesContext context = FacesContext.getCurrentInstance();
		PopulationEntry entry = (PopulationEntry) context.getApplication().createValueBinding(Constants.BIND_POPULATION_ENTRY).getValue(context);
		long currPopEntryId = entry.getCurrPopualtionTypeId();

		if (currPopEntryId > 0){
			return BonusManager.getBonusByWriterGroupAPopulation(writer.getGroupId(), currPopEntryId, getSkinsToString(), entry.getSkinId());
		}
		return new ArrayList<SelectItem>();
	}

	/**
	 * @return the pixelTypesSI
	 */
	public ArrayList<SelectItem> getPixelTypesSI() {
		return translateSI(pixelTypesSI);
	}

	/**
	 * @return the skinBusinessCasesSI
	 */
	public ArrayList<SelectItem> getSkinBusinessCasesSI() {
		ArrayList<SelectItem> si = new ArrayList<SelectItem>();
		si.add(new SelectItem(new Long(0),"All"));
		si.addAll(translateSI(skinBusinessCasesSI));
		return si;
	}
	
	/**
	 * @return the skinBusinessCasesSI
	 */
	public ArrayList<SelectItem> getSkinBusinessCases() {
		ArrayList<SelectItem> si = new ArrayList<SelectItem>();
		si.addAll(translateSI(skinBusinessCasesSI));
		return si;
	}

	/**
	 * @return the providersSI
	 */
	public ArrayList<SelectItem> getProvidersSI() {
		ArrayList<SelectItem> si = new ArrayList<SelectItem>();
		si.add(new SelectItem(new Long(0),"All"));
		si.addAll(providersSI);
		return si;
	}
	
	/**
	 * @return the providersSI
	 */
	public ArrayList<SelectItem> getProvidersSelectItem() {
		return providersSI;
	}
	
	/**
	 * @return the providersSI
	 */
	public ArrayList<SelectItem> getCCProvidersSI() {
		ArrayList<SelectItem> si = new ArrayList<SelectItem>();
		si.add(new SelectItem(new Long(0),"All"));
		si.addAll(ccProvidersSI);
		return si;
	}
	
	/**
	 * @return the providersSI
	 */
	public ArrayList<SelectItem> getCCProvidersSelectItem() {
		return ccProvidersSI;
	}

	/**
	 * @return the countriesGroup
	 */
	public ArrayList<SelectItem> getCountriesGroupSI() {
		ArrayList<SelectItem> si = new ArrayList<SelectItem>();
		si.add(new SelectItem("0","All"));
		si.addAll(countriesGroup);
		return si;
	}

	/**
	 * @return the HasInvestmentsSI
	 */


	public ArrayList<SelectItem> getHasInvestmentsSI() {
		ArrayList<SelectItem> list = new ArrayList<SelectItem>();
		list.add(new SelectItem(new Long(0), CommonUtil.getMessage("capture.daily.have.investments.all", null)));
		list.add(new SelectItem(new Long(Constants.CAPTURE_INFO_INVEST), CommonUtil.getMessage("capture.daily.have.investments", null)));
		list.add(new SelectItem(new Long(Constants.CAPTURE_INFO_NOT_INVEST), CommonUtil.getMessage("capture.daily.not.have.investments", null)));
		return list;
	}

	public ArrayList<SelectItem> getRetentionStatusSI() {
		ArrayList<SelectItem> list = new ArrayList<SelectItem>();
		SelectItem tmp = new SelectItem(new Integer(0).intValue(),"All");
		list.add(tmp);
		tmp = new SelectItem(ConstantsBase.COLOR_BLACK_ID,"BLACK");
		list.add(tmp);
		tmp = new SelectItem(ConstantsBase.COLOR_BLUE_ID,"BLUE");
		list.add(tmp);
		tmp = new SelectItem(ConstantsBase.COLOR_GREEN_ID,"GREEN");
		list.add(tmp);

		return list;
	}

	public ArrayList<SelectItem> getUserStatusSI() {
		ArrayList<SelectItem> list = new ArrayList<SelectItem>();
		SelectItem tmp = new SelectItem(new Long(0) ,"All");
		list.add(tmp);
		tmp = new SelectItem(UsersManagerBase.USER_STATUS_ACTIVE, CommonUtil.getMessage("user.status.active", null));
		list.add(tmp);
		tmp = new SelectItem(UsersManagerBase.USER_STATUS_SLEEPER, CommonUtil.getMessage("user.status.sleeper", null));
		list.add(tmp);
		tmp = new SelectItem(UsersManagerBase.USER_STATUS_COMA, CommonUtil.getMessage("user.status.coma", null));
		list.add(tmp);

		return list;
	}

	public ArrayList<SelectItem> getUserRankSI() {
		ArrayList<SelectItem> list = new ArrayList<SelectItem>();
		SelectItem tmp = new SelectItem(new Long(0) ,"All");
		list.add(tmp);
		tmp = new SelectItem(UsersManagerBase.USER_RANK_NEWBIES, CommonUtil.getMessage("user.newbies", null));
		list.add(tmp);
		tmp = new SelectItem(UsersManagerBase.USER_RANK_REGULARS, CommonUtil.getMessage("user.regulars", null));
		list.add(tmp);
		tmp = new SelectItem(UsersManagerBase.USER_RANK_BEGGINER_ROLLERS, CommonUtil.getMessage("user.beginner.rollers", null));
		list.add(tmp);
		tmp = new SelectItem(UsersManagerBase.USER_RANK_MEDUIM_ROLLERS, CommonUtil.getMessage("user.medium.rollers", null));
		list.add(tmp);
		tmp = new SelectItem(UsersManagerBase.USER_RANK_HIGH_ROLLERS, CommonUtil.getMessage("user.high.rollers", null));
		list.add(tmp);
		tmp = new SelectItem(UsersManagerBase.USER_RANK_GOLD_ROLLERS, CommonUtil.getMessage("user.gold.rollers", null));
		list.add(tmp);
		tmp = new SelectItem(UsersManagerBase.USER_RANK_PLATINUM_ROLLERS, CommonUtil.getMessage("user.platinum.rollers", null));
		list.add(tmp);

		return list;
	}

	public ArrayList<SelectItem> getEntryTypesForCollect() {
		ArrayList<SelectItem> list = new ArrayList<SelectItem>();
		list.add(new SelectItem(new Long(0), "All"));
		list.add(new SelectItem(new Long(ConstantsBase.POP_ENTRY_TYPE_GENERAL), CommonUtil.getMessage("population.enrty.type.general", null)));
		list.add(new SelectItem(new Long(ConstantsBase.POP_ENTRY_TYPE_CALLBACK), CommonUtil.getMessage("population.enrty.type.callback", null)));
		list.add(new SelectItem(new Long(ConstantsBase.POP_ENTRY_TYPE_TRACKING), CommonUtil.getMessage("population.enrty.type.tracking", null)));
		return list;
	}

	/**
	 * @return the popTypeDecline
	 */
	public ArrayList<SelectItem> getPopTypeDeclineSI() {
		ArrayList<SelectItem> si = new ArrayList<SelectItem>();
		si.add(new SelectItem(new Long(0),"opportunities.all"));
		si.addAll(popTypeDecline);
		return translateSI(si);
	}

	/**
	 * @return the allWritersMap
	 */
	public HashMap<Long, Writer> getAllWritersMap() {
		return allWritersMap;
	}

	/**
	 * @param allWritersMap the allWritersMap to set
	 */
	public void setAllWritersMap(HashMap<Long, Writer> allWritersMap) {
		this.allWritersMap = allWritersMap;
	}



	public boolean isWriterHasLockedEntry() {
		long writerId = writer.getId();
		try {
			FacesContext context = FacesContext.getCurrentInstance();
			PopulationEntry populationEntry = (PopulationEntry) context.getApplication().createValueBinding(Constants.BIND_POPULATION_ENTRY).getValue(context);
			if (null == populationEntry || populationEntry.getPopulationUserId() == 0) {
				PopulationEntry writerLockedEntry = PopulationEntriesManager.getWriterLockedEntry(writerId);
				if (null != writerLockedEntry){
					PopulationEntriesManager.loadEntry(writerLockedEntry);
					return true;
				}
			} else {  // allready in the session
				return true;
			}
		} catch (Exception e) {
			logger.error("Error in loading entry for writer with id " + writerId,e);
		}
		return false;
	}

	public void setSalesDepositsTypes() {
		salesDepositsTypes = new ArrayList<SelectItem>();
		SelectItem all = new SelectItem(ConstantsBase.ALL_FILTER_ID_INT,
				CommonUtil.getMessage("general.all", null,localInfo.getLocale()));
		SelectItem con = new SelectItem(ConstantsBase.SALES_DEPOSIT_TYPE_CONVERSION,
				CommonUtil.getMessage("sales.deposit.sales.deposits.conversion", null,localInfo.getLocale()));
		SelectItem ret = new SelectItem(ConstantsBase.SALES_DEPOSIT_TYPE_RETENTION,
				CommonUtil.getMessage("sales.deposit.sales.deposits.retention", null,localInfo.getLocale()));
		salesDepositsTypes.add(all);
		salesDepositsTypes.add(con);
		salesDepositsTypes.add(ret);
	}

	/**
	 * @return the salesDepositsTypes
	 */
	public ArrayList<SelectItem> getSalesDepositsTypes() {
		return salesDepositsTypes;
	}

	public void setSalesDepositsSources() {
		salesDepositsSources = new ArrayList<SelectItem>();
		SelectItem all = new SelectItem(ConstantsBase.ALL_FILTER_ID_INT,
				CommonUtil.getMessage("general.all", null,localInfo.getLocale()));
		SelectItem sales = new SelectItem(Constants.SALES_DEPOSIT_SOURCE_SALES,
				CommonUtil.getMessage("sales.deposit.source.sales", null,localInfo.getLocale()));
		SelectItem ind = new SelectItem(Constants.SALES_DEPOSIT_SOURCE_INDEPENDENT,
				CommonUtil.getMessage("sales.deposit.source.independent", null,localInfo.getLocale()));
		salesDepositsSources.add(all);
		salesDepositsSources.add(sales);
		salesDepositsSources.add(ind);
	}
	
	public void setHistoryWriters() {
		historyWriters = new ArrayList<SelectItem>();
		SelectItem all = new SelectItem(ConstantsBase.ALL_FILTER_ID_INT,
				CommonUtil.getMessage("general.all", null, localInfo.getLocale()));
		SelectItem web = new SelectItem(com.anyoption.common.beans.Writer.WRITER_ID_WEB, "WEB");
		SelectItem mob = new SelectItem(com.anyoption.common.beans.Writer.WRITER_ID_MOBILE, "Mobile");
		SelectItem copyopWeb = new SelectItem(com.anyoption.common.beans.Writer.WRITER_ID_COPYOP_WEB, "copyop_web");
		SelectItem copyopMob = new SelectItem(com.anyoption.common.beans.Writer.WRITER_ID_COPYOP_MOBILE, "copyop_mobile");
		SelectItem be = new SelectItem(-1, "BE");
		
		historyWriters.add(all);
		historyWriters.add(web);
		historyWriters.add(mob);
		historyWriters.add(copyopWeb);
		historyWriters.add(copyopMob);
		historyWriters.add(be);
	}

	/**
	 * @return the salesDepositsSources
	 */
	public ArrayList<SelectItem> getHistoryWriters() {
		return historyWriters;
	}
	
	public ArrayList<SelectItem> getSalesDepositsSources() {
		return salesDepositsSources;
	}

	public ArrayList<SelectItem> getSalesQualifiedFilter() {
		return salesQualifiedFilter;
	}

	public void setSalesQualifiedFilter() {
		salesQualifiedFilter = new ArrayList<SelectItem>();
		SelectItem all = new SelectItem(ConstantsBase.ALL_FILTER_ID_INT,
				CommonUtil.getMessage("general.all", null,localInfo.getLocale()));
		SelectItem qual = new SelectItem(Constants.SALES_QUAL_FILTER_QUALIFIED,
				CommonUtil.getMessage("sales.deposit.sales.qualified", null,localInfo.getLocale()));
		SelectItem notQual = new SelectItem(Constants.SALES_QUAL_FILTER_NOT_QUALIFIED,
				CommonUtil.getMessage("sales.deposit.sales.not.qualified", null,localInfo.getLocale()));
		salesQualifiedFilter.add(all);
		salesQualifiedFilter.add(qual);
		salesQualifiedFilter.add(notQual);
	}

	/**
	 * @return the marketingAffiliatesSi
	 * @throws SQLException
	 */
	public ArrayList<SelectItem> getMarketingAffiliatesSi() throws SQLException {
		 return MarketingManager.getMarketingAffiliates();
	}

    /**
     * @return the marketingAffiliatesSi all
     * @throws SQLException
     */
    public ArrayList<SelectItem> getMarketingAffiliatesAllSI() throws SQLException {
        ArrayList<SelectItem> list = new ArrayList<SelectItem>();
        list.add(new SelectItem(ConstantsBase.ALL_FILTER_ID,CommonUtil.getMessage(ConstantsBase.ALL_FILTER_KEY, null)));
        list.addAll(MarketingManager.getMarketingAffiliates());
        return list;
    }

	/**
	 * Get bonus population limit typeId by popTypeId
	 * @param popTypeId
	 * @return
	 */
	public int getBonusPopLimitType(Long popTypeId) {
		return populationTypes.get(popTypeId).getBonusLimitTypeId();
	}

	/**
	 * Get Risk Alerts Type Description Array list SI
	 * @return
	 */
	public ArrayList<SelectItem> getRiskAlertsTypeDescriptionSI(){
			ArrayList<SelectItem> riskAlertTypeSI = new ArrayList<SelectItem>();
			riskAlertTypeSI.add(new SelectItem(new Long(0),"All"));
			riskAlertTypeSI.addAll(translateSI(ApplicationData.getRiskAlertsType()));
		return riskAlertTypeSI;
	}
	
	/**
	 * Get Risk Alerts Type Description Array list SI
	 * @return
	 */
	public ArrayList<SelectItem> getRiskAlertsTypeDescriptionForAccountingSI() {
		ArrayList<SelectItem> riskAlertTypeSI = new ArrayList<SelectItem>();
		ArrayList<SelectItem> riskAlertTypeForAccountingSI = new ArrayList<SelectItem>();
		riskAlertTypeSI.add(new SelectItem(new Long(0), "All"));
		riskAlertTypeSI
				.addAll(translateSI(ApplicationData.getRiskAlertsType()));
		for (SelectItem item : riskAlertTypeSI) {
			long value = (Long) item.getValue();
			if (value != RiskAlertsManagerBase.RISK_TYPE_DEPOSIT_BALANCE_GREATER_THAN_MIN_INVEST
					&& value != RiskAlertsManagerBase.RISK_TYPE_WIRE_WITHDRAW_AMOUNT_GREATER_THAN_BALANCE
					&& value != RiskAlertsManagerBase.RISK_TYPE_CC_DEPOSIT_MORE_THAN_ALLOWED
					&& value != RiskAlertsManagerBase.RISK_TYPE_WITHDRAW_NO_INVESTMENTS) {
				riskAlertTypeForAccountingSI.add(item);
			}
		}
		return riskAlertTypeForAccountingSI;
	}

	/**
	 * @return the issueActionType
	 */
	public HashMap<Integer, HashMap<Long, ArrayList<IssueActionType>>> getIssueActionType() {
		return IssueActionType;
	}

	public ArrayList<SelectItem> getMailBoxTypes() throws SQLException {
		return MailBoxTemplatesManager.getTemplateTypes();
	}

	public ArrayList<SelectItem> getMailBoxTypesAll() throws SQLException {
		ArrayList<SelectItem> list = new ArrayList<SelectItem>();
		list.add(new SelectItem(new Long(0),CommonUtil.getMessage("", null,localInfo.getLocale())));
		list.addAll(getMailBoxTypes());
		return list;
	}

	/**
	 * Get Black list reason list
	 * @param channelId
	 * @return
	 */
	public ArrayList<SelectItem> getAccountingIssueActionTypesSI(){
		ArrayList<IssueActionType> list = getIssueActionType().get(ConstantsBase.SCREEN_ACCOUNTING).get(new Long(IssuesManagerBase.ISSUE_CHANNEL_ACTIVITY));
		ArrayList<SelectItem> si = new ArrayList<SelectItem>();
		for (IssueActionType iat: list){
				si.add(new SelectItem(String.valueOf(iat.getId()),CommonUtil.getMessage(iat.getName(), null)));
		}
		Collections.sort(si, new CommonUtil.selectItemComparator());
		return si;
	}

	public ArrayList<SelectItem> getMailBoxSenders() {
		Collections.sort(mailBoxSenders, new CommonUtil.selectItemComparator());
		return mailBoxSenders;
	}

	public ArrayList<SelectItem> getMailBoxSendersAll() {
		ArrayList<SelectItem> list = new ArrayList<SelectItem>();
		list.add(new SelectItem(new Long(0),CommonUtil.getMessage("", null,localInfo.getLocale())));
		list.addAll(getMailBoxSenders());
		return list;
	}


	public String getMailBoxSenderById(long sernderId) {
		for (int i = 0; i < mailBoxSenders.size(); i++) {
			if ((Long) mailBoxSenders.get(i).getValue() == sernderId) {
				return mailBoxSenders.get(i).getLabel();
			}
		}
		return "";
	}

	/**
	 * For support use
	 * @return
	 * @throws SQLException
	 */
	public ArrayList<SelectItem> getMailBoxTemplatesByWriterAndUser() throws SQLException {
		return MailBoxTemplatesManager.getAllTemplatesByWriterAndUser();
	}

	/**
	 * For none support use
	 * @return
	 * @throws SQLException
	 */
	public ArrayList<SelectItem> getMailBoxTemplatesByWriter() throws SQLException {
		ArrayList<SelectItem> list = new ArrayList<SelectItem>();
		list.add(new SelectItem(new Long(0),CommonUtil.getMessage("", null,localInfo.getLocale())));
		list.addAll(MailBoxTemplatesManager.getAllTemplatesByWriter());
		return list;
	}

	public ArrayList<SelectItem> getMailboxPopupTypes() {
		return mailboxPopupTypes;
	}

	/**
	 * Get action type list lists by channel id
	 * @param channelId
	 * @return
	 */
	public ArrayList<SelectItem> getAutoIssueActivityActionTypes(){
		ArrayList<IssueActionType> list = ApplicationData.getActionTypesList();
		ArrayList<SelectItem> si = new ArrayList<SelectItem>();
		for (IssueActionType iat: list){
			if (iat.getChannelId() == IssuesManagerBase.ISSUE_CHANNEL_ACTIVITY){
				si.add(new SelectItem(String.valueOf(iat.getId()),CommonUtil.getMessage(iat.getName(), null)));
			}
		}
		Collections.sort(si, new CommonUtil.selectItemComparator());
		return si;
	}
	
	public ArrayList<SelectItem> getAutoIssueActivityActionTypesWithoutVelocity(){
		ArrayList<IssueActionType> list = ApplicationData.getActionTypesList();
		ArrayList<SelectItem> si = new ArrayList<SelectItem>();
		for (IssueActionType iat: list){
			if (iat.getChannelId() == IssuesManagerBase.ISSUE_CHANNEL_ACTIVITY && 
					( iat.getId() != IssuesManagerBase.ISSUE_ACTION_VELOCITY_WAIVE && iat.getId() != IssuesManagerBase.ISSUE_ACTION_VELOCITY_ACTIVATE) ){
				si.add(new SelectItem(String.valueOf(iat.getId()),CommonUtil.getMessage(iat.getName(), null)));
			}
		}
		Collections.sort(si, new CommonUtil.selectItemComparator());
		return si;
	}

	/**
	 * Get Affiliates Array list SI
	 * @return
	 */
	public ArrayList<SelectItem> getAffiliateNameAndKeySI(){
		return splitTranslateSI(affiliatesNamesAndKeysSI);
	}

	/**
	 * @return the affiliatesHM
	 */
	public HashMap<Long, MarketingAffilate> getAffiliatesHM() {
		return affiliatesHM;
	}

	public ArrayList<SelectItem> getWithdrawalCasesStatuses() throws SQLException {
		ArrayList<SelectItem> list = new ArrayList<SelectItem>();
		list.add(new SelectItem(new Long(0),CommonUtil.getMessage("", null,localInfo.getLocale())));
		list.add(new SelectItem(new Long(TransactionsManager.TRANS_STATUS_REQUESTED),CommonUtil.getMessage("trans.status.requested", null,localInfo.getLocale())));
		list.add(new SelectItem(new Long(TransactionsManager.TRANS_STATUS_APPROVED),CommonUtil.getMessage("trans.status.approved", null,localInfo.getLocale())));
		list.add(new SelectItem(new Long(TransactionsManager.TRANS_STATUS_SECOND_APPROVAL),CommonUtil.getMessage("trans.status.second.approval", null,localInfo.getLocale())));
		list.add(new SelectItem(new Long(TransactionsManager.TRANS_STATUS_SUCCEED),CommonUtil.getMessage("trans.status.succeed", null,localInfo.getLocale())));
		return list;
	}

	public ArrayList<SelectItem> getChbStates(){
		ArrayList<SelectItem> list = new ArrayList<SelectItem>();
		list.add(new SelectItem(ChargeBack.STATE_POS,CommonUtil.getMessage("chargebacks.state.pos", null)));
		list.add(new SelectItem(ChargeBack.STATE_CHB,CommonUtil.getMessage("chargebacks.state.chb", null)));
		return list;
	}

	public ArrayList<SelectItem> getChbStatesWithAll(){
		ArrayList<SelectItem> list = new ArrayList<SelectItem>();
		ArrayList<SelectItem> temp = getChbStates();
		list.add(new SelectItem(ConstantsBase.ALL_FILTER_ID_INT,CommonUtil.getMessage(ConstantsBase.ALL_FILTER_KEY, null)));
		list.addAll(temp);
		return list;
	}


	/**
	 * @return the riskReasonSI
	 */
	public ArrayList<SelectItem> getRiskReasonSI() {
		return translateSI(riskReasonSI);
	}

	/**
	 * @return the riskStatusesSI
	 */
	public ArrayList<SelectItem> getAllRiskStatusesSI() {
		ArrayList<SelectItem> list = new ArrayList<SelectItem>();
		list.add(new SelectItem("0","All"));
		list.addAll(translateSI(filesRiskStatusesSI));
		return list;
	}

	/**
	 * Return all risk statuses for support personnel (risk issues screen)
	 * @return
	 */
	public ArrayList<SelectItem> getSupportRiskStatusesSI(){
		ArrayList<SelectItem> tmp = new ArrayList<SelectItem>();
		long itr;
		FilesRiskStatus frs;
		for (Iterator<Long> iter = filesRiskStatusHM.keySet().iterator(); iter.hasNext();) {
			itr = iter.next();
			frs = filesRiskStatusHM.get(itr);
			if (frs.isSupportStatus()){
				tmp.add(new SelectItem(String.valueOf(itr), CommonUtil.getMessage(frs.getName(), null)));
			}
		}
		Collections.sort(tmp, new selectItemComparator());
		ArrayList<SelectItem> list = new ArrayList<SelectItem>();
		list.add(new SelectItem("0","All"));
		list.addAll(tmp);
		return list;
	}

	/**
	 * @return the filesRiskStatusHM
	 */
	public HashMap<Long, FilesRiskStatus> getFilesRiskStatusHM() {
		return filesRiskStatusHM;
	}

	/**
	 * @param filesRiskStatusHM the filesRiskStatusHM to set
	 */
	public void setFilesRiskStatusHM(
			HashMap<Long, FilesRiskStatus> filesRiskStatusHM) {
		this.filesRiskStatusHM = filesRiskStatusHM;
	}

	/**
	 * @return the riskStatusesSI
	 */
	public ArrayList<SelectItem> getRiskStatusesSI() {
		return translateSI(filesRiskStatusesSI);
	}

	/**
	 * @param riskStatusesSI the riskStatusesSI to set
	 */
	public void setRiskStatusesSI(ArrayList<SelectItem> filesRiskStatusesSI) {
		this.filesRiskStatusesSI = filesRiskStatusesSI;
	}

	/**
	 * @return the paymentMethodsSI
	 */
	public ArrayList<SelectItem> getPaymentMethodsSI() {
		return selectItems.get("paymentMethodsSI");
	}

	/**
	 * @return the paymentMethodsSI
	 */
	public void setPaymentMethodsSI() throws SQLException {
		ArrayList<SelectItem> paymentMethodsSI = new ArrayList<SelectItem>();
		ArrayList<PaymentMethod> pmList= TransactionsManager.getTransactionsPaymentMethodList();

		paymentMethodsSI.add(new SelectItem(ConstantsBase.ALL_FILTER_ID,
				CommonUtil.getMessage("general.all", null,localInfo.getLocale())));
		for (PaymentMethod pm:pmList){
			paymentMethodsSI.add(new SelectItem(new Long(pm.getId()),pm.getName()));
		}
		
		selectItems.put("paymentMethodsSI", paymentMethodsSI);
	}

	/**
	 * @return the issueTypesSI
	 */
	public ArrayList<SelectItem> getIssueTypesSI() {
		return issueTypesSI;
	}

	/**
	 * @param issueTypesSI the issueTypesSI to set
	 */
	public void setIssueTypesSI() {
		issueTypesSI = new ArrayList<SelectItem>();
		issueTypesSI.add(new SelectItem(new Long(ConstantsBase.ALL_FILTER_ID), CommonUtil.getMessage("general.all", null,localInfo.getLocale())));
		issueTypesSI.add(new SelectItem(new Long(ConstantsBase.ISSUE_TYPE_REGULAR), CommonUtil.getMessage("issue.type.regular", null,localInfo.getLocale())));
		issueTypesSI.add(new SelectItem(new Long(ConstantsBase.ISSUE_TYPE_RISK), CommonUtil.getMessage("issue.type.risk", null,localInfo.getLocale())));
	}

	/**
	 * @return the riskIssueTypesSI
	 */
	public ArrayList<SelectItem> getRiskIssueTypesSI() {
		return riskIssueTypesSI;
	}

	/**
	 * @param riskIssueTypesSI the riskIssueTypesSI to set
	 */
	public void setRiskIssueTypesSI() {
		riskIssueTypesSI = new ArrayList<SelectItem>();
		riskIssueTypesSI.add(new SelectItem(new Long(ConstantsBase.ALL_FILTER_ID), CommonUtil.getMessage("general.all", null,localInfo.getLocale())));
		riskIssueTypesSI.add(new SelectItem(new Long(Long.valueOf(IssuesManagerBase.ISSUE_ACTION_RISK_CONTACT_USER)), CommonUtil.getMessage("issue.action.request.contact.user", null,localInfo.getLocale())));
		riskIssueTypesSI.add(new SelectItem(new Long(Long.valueOf(IssuesManagerBase.ISSUE_ACTION_RISK_NEED_DOCUMENT)), CommonUtil.getMessage("issue.action.request.documents", null,localInfo.getLocale())));
	}
	
	public void setAssetIndexGroupsSI() {
		assetIndexGroupsSI = new ArrayList<SelectItem>();
		assetIndexGroupsSI.add(new SelectItem(new Long(ConstantsBase.ALL_FILTER_ID), CommonUtil.getMessage("general.all", null, localInfo.getLocale())));
		assetIndexGroupsSI.add(new SelectItem(new Long(AssetIndexGroupTypeByName.INDICES.getId()), CommonUtil.getMessage("asset.index.market.groups.indices", null,localInfo.getLocale())));
		assetIndexGroupsSI.add(new SelectItem(new Long(AssetIndexGroupTypeByName.CURRENCY_PAIRS.getId()), CommonUtil.getMessage("asset.index.market.groups.currency", null,localInfo.getLocale())));
		assetIndexGroupsSI.add(new SelectItem(new Long(AssetIndexGroupTypeByName.COMMODITIES.getId()), CommonUtil.getMessage("asset.index.market.groups.commodities", null,localInfo.getLocale())));
		assetIndexGroupsSI.add(new SelectItem(new Long(AssetIndexGroupTypeByName.STOCKS.getId()), CommonUtil.getMessage("asset.index.market.groups.stocks", null,localInfo.getLocale())));
		assetIndexGroupsSI.add(new SelectItem(new Long(AssetIndexGroupTypeByName.OPTION_PL.getId()), CommonUtil.getMessage("asset.index.market.groups.type.option.pls", null,localInfo.getLocale())));
		assetIndexGroupsSI.add(new SelectItem(new Long(AssetIndexGroupTypeByName.BINARY_0_100.getId()), CommonUtil.getMessage("asset.index.market.groups.type.binary.0.100", null,localInfo.getLocale())));
	}

	public ArrayList<SelectItem> getEnvoyWebmoneyMonetaList() {
		ArrayList<SelectItem> envoyWebmoneyMonetaList = new ArrayList<SelectItem>();
		envoyWebmoneyMonetaList.add(new SelectItem("Webmoney","Webmoney"));
		envoyWebmoneyMonetaList.add(new SelectItem("Moneta","Moneta"));
		return envoyWebmoneyMonetaList;
	}

    public ArrayList<SelectItem> getBankWithdrawalList() throws SQLException {
        ArrayList<SelectItem> list = new ArrayList<SelectItem>();
        list.add(new SelectItem(new Long(0),CommonUtil.getMessage("", null,localInfo.getLocale())));
        list.add(new SelectItem(new Long(TransactionsManager.TRANS_STATUS_REQUESTED),CommonUtil.getMessage("trans.status.requested", null,localInfo.getLocale())));
        list.add(new SelectItem(new Long(TransactionsManager.TRANS_STATUS_APPROVED),CommonUtil.getMessage("trans.status.approved", null,localInfo.getLocale())));
        list.add(new SelectItem(new Long(TransactionsManager.TRANS_STATUS_SUCCEED),CommonUtil.getMessage("trans.status.succeed", null,localInfo.getLocale())));
        return list;
    }

    /**
     * @return the wirebanksSI
     */
    public ArrayList<SelectItem> getWireBanksAnyoptionSI() {
        return translateSI(wirebanksAnyoptionSI);
    }

    public ArrayList<SelectItem> getWireBanksAnyoptionAllSI() {
        ArrayList<SelectItem> list = new ArrayList<SelectItem>();
        list.add(new SelectItem("0", "general.all"));
        list.addAll(wirebanksAnyoptionSI);
        return translateSI(list);
    }

    public ArrayList<SelectItem> getWireBanksEtraderSI() {
        return translateSI(wirebanksEtraderSI);
    }

    public ArrayList<SelectItem> getWireBanksEtraderAllSI() {
        ArrayList<SelectItem> list = new ArrayList<SelectItem>();
        list.add(new SelectItem("0", "general.all"));
        list.addAll(wirebanksEtraderSI);
        return translateSI(list);
    }

	public ArrayList<SelectItem> getActiveUsersSI() {
		return activeUsersSI;
	}

	public void setActiveUsersSI() {
		activeUsersSI = new ArrayList<SelectItem>();

		activeUsersSI.add(new SelectItem(ConstantsBase.ALL_FILTER_ID_INT,
				CommonUtil.getMessage("general.all", null,localInfo.getLocale())));
		activeUsersSI.add(new SelectItem(Constants.USER_ACTIVE_INT,
				CommonUtil.getMessage("filter.users.active", null,localInfo.getLocale())));
		activeUsersSI.add(new SelectItem(Constants.USER_NOT_ACTIVE_INT,
				CommonUtil.getMessage("filter.users.inactive", null,localInfo.getLocale())));
	}

	public ArrayList<SelectItem> getMediaBuyersAll() throws SQLException {
        return addAllOptionNoKey(ApplicationData.getMediaBuyersWriters());
    }

    /**
     * @return the mediaBuyersSI
     */
    public ArrayList<SelectItem> getMediaBuyersSI() {
        return ApplicationData.getMediaBuyersWriters();
    }

    public void setSiteSI() {
    	siteSI = new ArrayList<SelectItem>();

    	siteSI.add(new SelectItem(ConstantsBase.ALL_FILTER_ID,
				CommonUtil.getMessage("general.all", null, localInfo.getLocale())));
    	siteSI.add(new SelectItem(1, "etrader"));
    	siteSI.add(new SelectItem(2, "anyoption"));
	}

    public ArrayList<SelectItem> getSiteSI() {
		return siteSI;
	}

	private boolean isHasEtraderWriter() throws SQLException {
		boolean res = false;
		ArrayList<Long> languagesList = WritersManager.getWriterLanguages((int)writer.getId());
		if (languagesList.contains(new Long(Constants.LANGUAGES_HEBREW))) {
			res = true;
		}
		return res;
	}

	
	public boolean isHasEtrader() {
		return hasEtrader;
	}
	
		public void setHasEtrader(boolean hasEtrader) {
		this.hasEtrader = hasEtrader;
	}

	public ArrayList<SelectItem> getOpportunityProductTypesWithAllOptionSI() {
		return addAllOptionNoKey(ApplicationData.getOpportunityProductTypes());
	}

	public ArrayList getOpportunityProductTypesStringSI() {
		ArrayList<SelectItem> list = new ArrayList<SelectItem>();
		list.add(new SelectItem("0",CommonUtil.getMessage("general.all", null)));
		for ( SelectItem s : ApplicationData.getOpportunityProductTypes() ) {
			list.add(new SelectItem(s.getValue().toString(), s.getLabel()));
		}
		return list;
	}

	private ArrayList<SelectItem> addAllOptionAndNoneNoKey(ArrayList<SelectItem> list) {
		ArrayList<SelectItem> si = new ArrayList<SelectItem>();
		si=(ArrayList<SelectItem>)list.clone();
		SelectItem allItem = new SelectItem(ConstantsBase.ALL_FILTER_ID, CommonUtil.getMessage(ConstantsBase.ALL_FILTER_KEY, null));
		SelectItem noneItem = new SelectItem(ConstantsBase.NONE_FILTER_ID, CommonUtil.getMessage(ConstantsBase.NONE_FILTER_KEY, null));
		si.add(0, noneItem);
		si.add(0, allItem);
		return si;
	}

	public ArrayList<SelectItem> getSalesRepsWithAllAndNoneOptionSI() {
		return addAllOptionAndNoneNoKey(salesRepsSI);
	}

	public ArrayList<SelectItem> getSalesRepsConversionDepWithAllAndNoneOptionSI() {
		return addAllOptionAndNoneNoKey(salesRepsConversionDepSI);
	}
	
	public ArrayList getSkinsRegulatedSI() {
		return translateSI(skinsRegulatedSI);
	}

	public ArrayList<SelectItem> getSkinsNotRegulatedSI() {
		return translateSI(skinsNotRegulatedSI);
	}

	/**
	 * @return the salesReps
	 */
	public HashMap<Long, Writer> getSalesReps() {
		return salesReps;
	}

	/**
	 * @param salesReps the salesReps to set
	 */
	public void setSalesReps(HashMap<Long, Writer> salesReps) {
		this.salesReps = salesReps;
	}

	public ArrayList<SelectItem> getApiUsersTypeExternaWithNoneSI() {
		ArrayList<SelectItem> si = new ArrayList<SelectItem>();
		si = ((ArrayList<SelectItem>)ApplicationData.applicationSIHm.get("apiUsersTypeExternalSI").clone());
		SelectItem noneItem = new SelectItem(ConstantsBase.NONE_FILTER_ID, CommonUtil.getMessage(ConstantsBase.NONE_FILTER_KEY, null));
		si.add(0, noneItem);
		return si;
	}

	/**
	 * check if this writer have that skin
	 * @param skinId the skin to check
	 * @return true if the writer have it else false
	 */
	public Boolean isWriterHaveSkin(long skinId) {
		for (long skin : skins) {
			if (skinId == skin) {
				return true;
			}
		}
		return false;
	}
	
	/**
	 * return API issue channel select item
	 * @return
	 */
	public ArrayList<SelectItem> getApiIssueChannelSI() {
		return ApplicationData.applicationSIHm.get("apiIssueChannelSI"); 
	}
	
	/**
	 * return API issue subject select item
	 * @return
	 */
	public ArrayList<SelectItem> getApiIssueSubjectSI() {
		return ApplicationData.applicationSIHm.get("apiIssueSubjectSI"); 
	}
	
	/**
	 * return API issue status select item
	 * @return
	 */
	public ArrayList<SelectItem> getApiIssueStatusSI() {
		return ApplicationData.applicationSIHm.get("apiIssueStatusSI"); 
	}
	
	/**
	 * get users details history types SI
	 * @return ArrayList<SelectItem>
	 */
    public ArrayList<SelectItem> getUsersDetailsHistoryTypesSI() {
        return ApplicationData.applicationSIHm.get("usersDetailsHistoryTypes"); 
    }
	
	/**
	 * Get all writers
	 * @return list as ArrayList<SelectItem>
	 */
	public ArrayList<SelectItem> getAllWriters() {
        ArrayList<SelectItem> list = new ArrayList<SelectItem>();
        list.add(new SelectItem(String.valueOf(ConstantsBase.ALL_FILTER_ID),
                CommonUtil.getMessage("general.all", null,localInfo.getLocale())));
        list.addAll(getAllWritersList());
        return list;
	}
	
	/**
	 * Get all writers list
	 * @return list as ArrayList<SelectItem>
	 */
	public ArrayList<SelectItem> getAllWritersList() {
	    ArrayList<SelectItem> list = new ArrayList<SelectItem>();
        ArrayList<SelectItem> listTmp = new ArrayList<SelectItem>();
        Iterator<Long> iter = allWritersMap.keySet().iterator();
        Long key;
        Writer writer;
        while(iter.hasNext()) {
            key = iter.next();
            writer = allWritersMap.get(key);
            listTmp.add(new SelectItem(String.valueOf(writer.getId()),writer.getUserName()));
        }
        Collections.sort(listTmp, new CommonUtil.selectItemComparator());
        list.addAll(listTmp);
        return list;
    }
	
	/**
	 * return static landing pages types select item
	 * @return
	 */
	public ArrayList<SelectItem> getStaticLPTypesSI() {
		return ApplicationData.applicationSIHm.get("staticLPTypesSI"); 
	}
	
    public ArrayList<SelectItem> getWithdrawalTypesSI() {
		return translateSI(withdrawalTypesSI);
	}
    
	/**
	 * Return reward tiers select item 
	 * @return
	 */
	public ArrayList<SelectItem> getRewardTiersSI() {
		return ApplicationData.applicationSIHm.get("rewardTiersSI"); 
	}
	
	/**
	 * Return reward history types select item 
	 * @return
	 */
	public ArrayList<SelectItem> getRewardHistoryTypesSI() {
		return ApplicationData.applicationSIHm.get("rewardHistoryTypesSI"); 
	}

	public ArrayList<SelectItem> getMarketingUrlSourceSI() {
		return marketingUrlSourceSI;
	}

	public ArrayList<SelectItem> getAssetIndexGroupsSI() {
		return assetIndexGroupsSI;
	}

	public void setAssetIndexGroupsSI(ArrayList<SelectItem> assetIndexGroupsSI) {
		this.assetIndexGroupsSI = assetIndexGroupsSI;
	}

	/**
	 * Return reward actions (rewards or benefits) select item
	 * @return the rewardActions
	 */
	public ArrayList<SelectItem> getRewardActions() {
		return rewardActions;
	}

	/**
	 * @param rewardActions the rewardActions to set
	 */
	public void setRewardActions(ArrayList<SelectItem> rewardActions) {
		this.rewardActions = rewardActions;
	}

	/**
	 * @return the issueComponentSI
	 */
	public ArrayList<SelectItem> getIssueComponentSI() {
		return issueComponentSI;
	}

	/**
	 * @return the issueComponentsHM
	 */
	public HashMap<Long, IssueComponent> getIssueComponentsHM() {
		return issueComponentsHM;
	}
	
	/**
	 * @return the copyopCoinsStateSI
	 */
	public ArrayList<SelectItem> getCopyopCoinsStateSI() {
		return copyopCoinsStateSI;
	}

	/**
	 * @param copyopCoinsStateSI the copyopCoinsStateSI to set
	 */
	public void setCopyopCoinsStateSI(ArrayList<SelectItem> copyopCoinsStateSI) {
		this.copyopCoinsStateSI = copyopCoinsStateSI;
	}
	
	/**
	 * @return the copyopInvTypeSI
	 */
	public ArrayList<SelectItem> getCopyopInvTypeSI() {
		return copyopInvTypeSI;
	}

	/**
	 * @param copyopInvTypeSI the copyopInvTypeSI to set
	 */
	public void setCopyopInvTypeSI(ArrayList<SelectItem> copyopInvTypeSI) {
		this.copyopInvTypeSI = copyopInvTypeSI;
	}
	
	/**
	 * @return the invWritersSI
	 */
	public ArrayList<SelectItem> getInvWritersSI() {
		return invWritersSI;
	}

	/**
	 * @param invWritersSI the invWritersSI to set
	 */
	public void setInvWritersSI(ArrayList<SelectItem> invWritersSI) {
		this.invWritersSI = invWritersSI;
	}

	/**
	 * Get list of retention teams.
	 * Based on the table WRITERS_SALES_TYPE_DEPARTMENT
	 * @return retentionTeamsAllSI as ArrayList<SelectItem>
	 */
	public ArrayList<SelectItem> getRetentionTeamsSI() {
		ArrayList<SelectItem> retentionTeamsAllSI = new ArrayList<SelectItem>();
		retentionTeamsAllSI.add(new SelectItem(ConstantsBase.ALL_FILTER_ID,CommonUtil.getMessage("general.all", null)));
		retentionTeamsAllSI.add(new SelectItem(WritersManager.RETENTION_TEAMS_RETENTION,CommonUtil.getMessage("retention.teams.retention", null)));
		retentionTeamsAllSI.add(new SelectItem(WritersManager.RETENTION_TEAMS_UPGRADE,CommonUtil.getMessage("retention.teams.upgrade", null)));
		return retentionTeamsAllSI;
	}
	
	 /**
	  * @return the businessCasesSI
	  */
	 public ArrayList<SelectItem> getBusinessCasesSI() {
		 return translateSI(skinBusinessCasesSI);
	 }
	 
	 /**
	 * Get Affiliates Array list SI
	 * @return
	 */
	public ArrayList<SelectItem> getAffiliatesSI(){
		FacesContext context = FacesContext.getCurrentInstance();
		HttpSession session = (HttpSession) context.getExternalContext().getSession(true);
		String updateAffiliate = (String)session.getAttribute(Constants.UPDATE_AFFILIATE_LIST);
		if (updateAffiliate == Constants.ADDED_AFFILIATE) {
			try {
				ArrayList<SelectItem> affiliatesSI = MarketingManager.getAffiliatesNamesAndKeys();
				selectItems.put("affiliatesSI", splitTranslateSI(affiliatesSI));
				session.setAttribute(Constants.UPDATE_AFFILIATE_LIST, Constants.NOT_ADDED_AFFILIATE);
			} catch (SQLException e) {
				logger.debug("MarketingManager.getAffiliatesNamesAndKeys failed");
			}
		}
		return selectItems.get("affiliatesSI");
	}

	public void initAffiliatesSI() {
		try {
			ArrayList<SelectItem> affiliatesSI = MarketingManager.getAffiliatesNamesAndKeys();
			selectItems.put("affiliatesSI", splitTranslateSI(affiliatesSI));
		} catch (SQLException e) {
			logger.debug("initAffiliatesSI, MarketingManager.getAffiliatesNamesAndKeys failed");
		}
	}
	
	/**
	 * Get Writer Skins Group By Language SI And All Option
	 * @return si as ArrayList<SelectItem>
	 */
	public ArrayList getWriterSkinsGroupByLanguageSIAll() {
		ArrayList<SelectItem> si = new ArrayList<SelectItem>();
		si.add(new SelectItem(ConstantsBase.ALL_FILTER_ID, ConstantsBase.ALL_FILTER_KEY));
		si.addAll(skinsGroupByLanguageSI);
		return translateSI(si);
	}

	/**
	 * @return the staticLandingPagePathSI
	 */
	public ArrayList<SelectItem> getStaticLandingPagePathSI() {
		return staticLandingPagePathSI;
	}

	/**
	 * @param staticLandingPagePathSI the staticLandingPagePathSI to set
	 */
	public void setStaticLandingPagePathSI(
			ArrayList<SelectItem> staticLandingPagePathSI) {
		this.staticLandingPagePathSI = staticLandingPagePathSI;
	}

	/**
	 * @return the staticLandingPagePathHM
	 */
	public HashMap<Long, StaticLandingPagePath> getStaticLandingPagePathHM() {
		return staticLandingPagePathHM;
	}

	/**
	 * @param staticLandingPagePathHM the staticLandingPagePathHM to set
	 */
	public void setStaticLandingPagePathHM(
			HashMap<Long, StaticLandingPagePath> staticLandingPagePathHM) {
		this.staticLandingPagePathHM = staticLandingPagePathHM;
	}

	/**
	 * @return the lastLoginSI
	 */
	public ArrayList<SelectItem> getLastLoginSI() {
		return lastLoginSI;
	}

	/**
	 * @param lastLoginSI the lastLoginSI to set
	 * @throws SQLException 
	 */
	public void setLastLoginSI() throws SQLException {
		this.lastLoginSI = translateSILastLogin(CommonUtil.enumSI(Constants.ENUM_LAST_LOGIN_ENUMERATOR));
	}
	
	/**
	 * This method translates a select item list to the language of this writer
	 * @param list the list to translate
	 * @return translated list
	 */
	public static ArrayList<SelectItem> translateSILastLogin(ArrayList<SelectItem> list) {
		ArrayList<SelectItem> newList = new ArrayList<SelectItem>();
		for (SelectItem si : list) {
			String[] params = new String[1];
			long val = (long) si.getValue();
			if (val > Constants.MINUTES_IN_HOUR) {
				val = val / Constants.MINUTES_IN_HOUR;
			}
			params[0] = String.valueOf(val);
			String lable = CommonUtil.getMessage(si.getLabel(), params, new Locale("en"));
			newList.add(new SelectItem(si.getValue(), lable));
		}
		Collections.sort(newList, new selectItemComparator());
		return newList;
	}

	public ArrayList<SelectItem> getUserHistoryFieldsSI() {		
		return userHistoryFieldsSI;
	}

	public void setUserHistoryFieldsSI(ArrayList<SelectItem> userHistoryFieldsSI) {
		this.userHistoryFieldsSI = userHistoryFieldsSI;
	}

	/**
	 * @return the writersGroupsSI
	 */
	public ArrayList<SelectItem> getWritersGroupsSI() {
		return writersGroupsSI;
	}

	/**
	 * @param writersGroupsSI the writersGroupsSI to set
	 */
	public void setWritersGroupsSI(ArrayList<SelectItem> writersGroupsSI) {
		this.writersGroupsSI = writersGroupsSI;
	}
	
	/**
	 * Get marketing affiliates names and keys
	 * @return ArrayList<SelectItem>
	 */
	public ArrayList<SelectItem> getMarketingAffNameKeySI(){
		FacesContext context = FacesContext.getCurrentInstance();
		HttpSession session = (HttpSession) context.getExternalContext().getSession(true);
		String updateAffiliate = (String)session.getAttribute(Constants.UPDATE_AFFILIATE_NAME_KEY_LIST);
		if (updateAffiliate == Constants.ADDED_AFFILIATE) {
			try {
				ArrayList<SelectItem> affiliatesSI = MarketingManager.getMarketingAffNameKey();
				Collections.sort(affiliatesSI, new CommonUtil.selectItemComparator());
				selectItems.put("affiliatesNameKeySI", affiliatesSI);
				session.setAttribute(Constants.UPDATE_AFFILIATE_NAME_KEY_LIST, Constants.NOT_ADDED_AFFILIATE);
			} catch (SQLException e) {
				logger.error("getAffiliatesNameAndKeySI failed", e);
			}
		}
		return selectItems.get("affiliatesNameKeySI");
	}

	/**
	 * Initial Marketing Affiliates Name and Key SI
	 */
	public void initMarketingAffNameKeySI() {
		try {
			ArrayList<SelectItem> affiliatesSI = MarketingManager.getMarketingAffNameKey();
			Collections.sort(affiliatesSI, new CommonUtil.selectItemComparator());
			selectItems.put("affiliatesNameKeySI", affiliatesSI);
		} catch (SQLException e) {
			logger.error("initAffiliatesNameAndKeySI failed", e);
		}
	}

	/**
	 * Initial countries group tiers SI
	 */
	public void initCountriesGroupTiersSI() {
		try {
			ArrayList<SelectItem> countriesGroupTiersSI = CountryManagerBase.getCountriesGroupTiersSI();
			selectItems.put("countriesGroupTiersSI", countriesGroupTiersSI);
		} catch (SQLException e) {
			logger.error("initCountriesGroupTiersSI failed", e);
		}
	}
	
	/**
	 * Get countries group tiers SI
	 * @return
	 */
	public ArrayList<SelectItem> getCountriesGroupTiersSI() {
		ArrayList<SelectItem> si = new ArrayList<SelectItem>();
		si.add(new SelectItem(ConstantsBase.ALL_FILTER_ID, ConstantsBase.ALL_FILTER_KEY));
		si.addAll(selectItems.get("countriesGroupTiersSI"));
		return translateSI(si);
	}
}
