package il.co.etrader.backend.bl_vos;

public class CallsRepresentative extends Representative {

	private static final long serialVersionUID = 1L;
	private long callsNotReached;
	private long callsReached;
	private long notCalled;
	private long dailyAverage;
	private long totalCount;
	
	public long getCallsNotReached() {
		return callsNotReached;
	}
	
	public void setCallsNotReached(long callsNotReached) {
		this.callsNotReached = callsNotReached;
	}
	
	public long getCallsReached() {
		return callsReached;
	}
	
	public void setCallsReached(long callsReached) {
		this.callsReached = callsReached;
	}
	
	public long getNotCalled() {
		return notCalled;
	}
	
	public void setNotCalled(long notCalled) {
		this.notCalled = notCalled;
	}
	
	
	public long getDailyAverage() {
		return dailyAverage;
	}
	
	public void setDailyAverage(long dailyAverage) {
		this.dailyAverage = dailyAverage;
	}
	
	public long getTotalCount() {
		return totalCount;
	}
	
	public void setTotalCount(long totalCount) {
		this.totalCount = totalCount;
	}
}
