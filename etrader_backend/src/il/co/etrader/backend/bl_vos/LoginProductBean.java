package il.co.etrader.backend.bl_vos;

import java.util.Date;

import il.co.etrader.bl_managers.LoginProductManager.LoginProductEnum;

/**
 * @author kiril.mutafchiev
 */
public class LoginProductBean {

	private long skinId;
	private LoginProductEnum loginProduct;
	private Date timeUpdated;
	private long writerId;
	private boolean enabled;

	public long getSkinId() {
		return skinId;
	}

	public void setSkinId(long skinId) {
		this.skinId = skinId;
	}

	public LoginProductEnum getLoginProduct() {
		return loginProduct;
	}

	public void setLoginProduct(LoginProductEnum loginProduct) {
		this.loginProduct = loginProduct;
	}

	public Date getTimeUpdated() {
		return timeUpdated;
	}

	public void setTimeUpdated(Date timeUpdated) {
		this.timeUpdated = timeUpdated;
	}

	public long getWriterId() {
		return writerId;
	}

	public void setWriterId(long writerId) {
		this.writerId = writerId;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	public String toString() {
		String ls = System.getProperty("line.separator");
		return ls + "LoginProductBean: " + ls
				+ super.toString() + ls
				+ "skinId: " + skinId + ls
				+ "loginProduct: " + loginProduct + ls
				+ "timeUpdated: " + timeUpdated + ls
				+ "writerId: " + writerId + ls
				+ "enabled: " + enabled + ls;
	}
}