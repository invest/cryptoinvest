package il.co.etrader.backend.bl_vos;

import il.co.etrader.util.CommonUtil;
import il.co.etrader.util.ConstantsBase;

import java.io.Serializable;
import java.util.Date;



// Regulation Backend
public class PendingDocument implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -2984993069379071545L;
	private static final String defaultColor = "#db9356";
	//the automatic issue that will pop up in backend-support-issue menu
	long issueId;
	long userId;
	String userName;
	String skinName;
	String mobilePhone;
	String landLinePhone;
	//the date the user completed his first deposit - from this date the 30 days begin
	Date firstDepositDate;
	//the time diff from GMT (for example in Israel GMT+2)
	String gmtOffset;
	//number of days since customer’s first deposit until customer’s first deposit+30
	int daysUntilSuspend;
	//date and time of the last call that was reached
	Date lastTimeReached;
	//writer user name of the last call that was reached
	String lastCaller;
	// there is an issue action with issue_action_type = 23 (not cooperative )
	boolean userNotInterested = false;
	// count of not approved documents (support)
	int pendingDocs;
	// count of not approved documents (control)
	int pendingDocsControl;
	// count of not approved documents and uploaded (support or control)
    int pendingDocsUploaded;
	// comment by control when approving the user
	String comment;
	// the date when the users sum of deposits reach 5000
	Date qualifiedTime;
	 //the date when user last call
	Date lastCall;
	
	Date issueLastUpdate;
   private long totalDepositAmount;
   // glow row yellow if true
   private boolean isHotUser;
   private Date lastUploadFileTime;
	
	public Date getQualifiedTime() {
		return qualifiedTime;
	}
	public void setQualifiedTime(Date qualifiedTime) {
		this.qualifiedTime = qualifiedTime;
	}
	public int getPendingDocsUploaded() {
        return pendingDocsUploaded;
    }
    public void setPendingDocsUploaded(int pendingDocsUploaded) {
        this.pendingDocsUploaded = pendingDocsUploaded;
    }
    public long getIssueId() {
		return issueId;
	}
	public void setIssueId(long issueId) {
		this.issueId = issueId;
	}
	public long getUserId() {
		return userId;
	}
	public void setUserId(long userId) {
		this.userId = userId;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getSkinName() {
		return skinName;
	}
	public void setSkinName(String skinName) {
		this.skinName = skinName;
	}
	public String getMobilePhone() {
		return mobilePhone;
	}
	public void setMobilePhone(String mobilePhone) {
		this.mobilePhone = mobilePhone;
	}
	public String getLandLinePhone() {
		return landLinePhone;
	}
	public void setLandLinePhone(String landLinePhone) {
		this.landLinePhone = landLinePhone;
	}
	public Date getFirstDepositDate() {
		return firstDepositDate;
	}
	public void setFirstDepositDate(Date firstDepositDate) {
		this.firstDepositDate = firstDepositDate;
	}
	public String getGmtOffset() {
		return gmtOffset;
	}
	public void setGmtOffset(String gmtOffset) {
		this.gmtOffset = gmtOffset;
	}
	public int getDaysUntilSuspend() {
		return daysUntilSuspend;
	}
	public void setDaysUntilSuspend(int daysUntilSuspend) {
		this.daysUntilSuspend = daysUntilSuspend;
	}
	public Date getLastTimeReached() {
		return lastTimeReached;
	}
	public void setLastTimeReached(Date lastTimeReached) {
		this.lastTimeReached = lastTimeReached;
	}
	public String getLastCaller() {
		return lastCaller;
	}
	public void setLastCaller(String lastCaller) {
		this.lastCaller = lastCaller;
	}
	public boolean isUserNotInterested() {
		return userNotInterested;
	}
	public void setUserNotInterested(boolean userNotInterested) {
		this.userNotInterested = userNotInterested;
	}
	public int getPendingDocs() {
		return pendingDocs;
	}
	public void setPendingDocs(int pending_docs) {
		this.pendingDocs = pending_docs;
	}
	public String getComment() {
		return comment;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}
	public String getColor() {
		if(daysUntilSuspend == 0) {
			return defaultColor;
		} else if (daysUntilSuspend <=4 ) {
			return "red";
		} else {
			return "black";
		}
	}
	public Date getLastCall() {
		return lastCall;
	}
	public void setLastCall(Date lastCall) {
		this.lastCall = lastCall;
	}
	public Date getIssueLastUpdate() {
		return issueLastUpdate;
	}
	public void setIssueLastUpdate(Date issueLastUpdate) {
		this.issueLastUpdate = issueLastUpdate;
	}
	
	public String getIssueLastUpdateTxt() {
		return  CommonUtil.getDateTimeFormatDisplay(issueLastUpdate);
	}
	public long getTotalDepositAmount() {
		return totalDepositAmount;
	}
	public void setTotalDepositAmount(long totalDepositAmount) {
		this.totalDepositAmount = totalDepositAmount;
	}
	
	public String getTotalDepositAmountTxt(){
		return CommonUtil.displayAmount(totalDepositAmount,ConstantsBase.CURRENCY_BASE_ID);
	}
	public boolean isHotUser() {
		return isHotUser;
	}
	public void setHotUser(boolean isHotUser) {
		this.isHotUser = isHotUser;
	}
	public Date getLastUploadFileTime() {
		return lastUploadFileTime;
	}
	public void setLastUploadFileTime(Date lastUploadFileTime) {
		this.lastUploadFileTime = lastUploadFileTime;
	}
	public int getPendingDocsControl() {
		return pendingDocsControl;
	}
	public void setPendingDocsControl(int pendingDocsControl) {
		this.pendingDocsControl = pendingDocsControl;
	}
}
