package il.co.etrader.backend.dao_managers;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import com.anyoption.common.beans.Exchange;
import com.anyoption.common.beans.Market;
import com.anyoption.common.beans.OneTouchFields;
import com.anyoption.common.beans.Opportunity;
import com.anyoption.common.beans.base.Investment;
import com.anyoption.common.beans.base.OpportunityOddsPair;
import com.anyoption.common.enums.SkinGroup;
import com.anyoption.common.managers.MarketsManagerBase;

import il.co.etrader.backend.bl_vos.OpportunityOddsGroup;
import il.co.etrader.backend.bl_vos.OpportunityOddsPairTemplate;
import il.co.etrader.backend.bl_vos.OpportunityTotal;
import il.co.etrader.backend.util.Constants;
import il.co.etrader.backend.util.Utils;
import il.co.etrader.bl_vos.ExchangesHolidays;
import il.co.etrader.bl_vos.OpportunityShifting;
import il.co.etrader.bl_vos.OpportunityTemplate;
import il.co.etrader.bl_vos.OptionExpiration;
import il.co.etrader.bl_vos.Skins;
import il.co.etrader.dao_managers.InvestmentsDAOBase;
import il.co.etrader.dao_managers.OpportunitiesDAOBase;
import il.co.etrader.util.CommonUtil;
import il.co.etrader.util.ConstantsBase;

/**
 * Opportunities DAO.
 */
public class OpportunitiesDAO extends OpportunitiesDAOBase {
	private static Logger log = Logger.getLogger(OpportunitiesDAO.class);



	/**
	 * Gets the opportunities for the list in the backend.
	 *
	 * @param conn
	 *            the db connection to use
	 * @param suspended 
	 * @return <code>ArrayList</code> of <code>Opportunity</code>s.
	 * @throws SQLException
	 */
	public static ArrayList<Opportunity> get(Connection conn, Date from, String publishedSettled, long groupId,
			long marketId, long marketTypeId, long statusId, long classId, long opportunityId, long scheduledId, int suspended) throws SQLException {
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		ArrayList<Opportunity> list = new ArrayList<Opportunity>();
		try {
			String sql = " SELECT o.*, " + 
					"o.is_suspended AS is_opp_suspended, " + getFormatTimesSQL() + ", m.market_group_id, ot.description typedesc,odt.name oddstype_name , ";

			if (publishedSettled.equals("1")) {
				sql += " (select sum(house_result*rate) from investments i, users u " +
						"where i.user_id=u.id and i.opportunity_id=o.id and i.is_settled=1 and i.is_void=0 ";

				if ( classId != ConstantsBase.USER_CLASS_ALL )   {
			  		if ( classId == ConstantsBase.USER_CLASS_COMPANY_OR_PRIVATE )  {
			  			sql += " and (u.class_id="+ConstantsBase.USER_CLASS_PRIVATE+""+
			  				  " or u.class_id="+ConstantsBase.USER_CLASS_COMPANY+")";
			  		}
			  		else {
			  				sql += " and u.class_id ="+classId;
			  		}
			  	}
				if ( statusId != ConstantsBase.INVESTMENT_CLASS_STATUS_CANCELLED_AND_NON_CANCELLED_INVESTMENT ) {
					int isCancelled = (statusId==0 ? 1 : 0);
					sql += " and i.is_canceled=" + isCancelled;
				}
				sql += ") winlose ";
			} else {
				sql += " 0 winlose ";
			}

			sql +=  "FROM opportunities o,markets m,opportunity_types ot,opportunity_odds_types odt, opportunity_one_touch oot " +
					"WHERE ";

			if (publishedSettled.equals("0")) {
				// published
				sql += " o.is_published = 1 ";
                if (marketTypeId == 2 && null != from) {
                    sql += " and trunc(o.TIME_EST_CLOSING,'DDD')=trunc(?,'DDD') ";
                }

			} else if (publishedSettled.equals("1")) {
				// settled
				sql += " o.is_settled=1 and trunc(o.TIME_EST_CLOSING,'DDD')=trunc(?,'DDD') ";

			} else if (publishedSettled.equals("2")) {
				// not published
				sql += " o.is_published=0 and o.is_settled=0 and trunc(o.TIME_EST_CLOSING,'DDD')=trunc(?,'DDD') ";
			}

			if (marketId != 0) {
				sql += " and o.market_id = " + marketId;
			}

			sql += " and o.market_id=m.id and o.odds_type_id=odt.id and o.OPPORTUNITY_TYPE_ID=ot.id ";

			if ( marketTypeId > 0 ) {
				sql += " and m.TYPE_ID = " + marketTypeId + " ";
			}

            if (opportunityId > 0) {
                sql += " and o.id = " + opportunityId + " ";
            }
            
            if (scheduledId > 0) {
            	sql += " and o.scheduled = " + scheduledId + " ";
            }

            sql += " and o.id = oot.opportunity_id(+) ";

            if(suspended != 3) {
        	sql += " and o.is_suspended = " + suspended + " ";
            }
            
            if (marketTypeId == 2) {
                sql += " order by m.name, oot.up_down, o.CURRENT_LEVEL";
            } else {
                sql += " order by o.time_est_closing ";
            }


			pstmt = conn.prepareStatement(sql);

			if (!publishedSettled.equals("0") || (marketTypeId == 2 && null != from)) {
				//pstmt.setTimestamp(1, CommonUtil.convertToTimeStamp(from));
				Date d = CommonUtil.getDateTimeFormaByTz(from, Utils.getAppData().getUtcOffset());
				pstmt.setTimestamp(1, CommonUtil.convertToTimeStamp(d));
			}

			rs = pstmt.executeQuery();
			while(rs.next()) {
				Opportunity o = getVO(rs);
				o.setOpenGraph(rs.getInt("is_open_graph")==1);
				// set up_down field for one touch opp
				if ( o.getOpportunityTypeId() == Opportunity.TYPE_ONE_TOUCH ) {
					OneTouchFields tempOneTouchFields = InvestmentsDAOBase.getOneTouchFields(conn, o.getId());
					o.setOneTouchUpDown(tempOneTouchFields.getUpDown());
					o.setDecimalPointOneTouch(tempOneTouchFields.getDecimalPoint());
				}

				Market m = new Market();
				m.setGroupId(rs.getLong("market_group_id"));
				o.setOpportunityTypeDesc(rs.getString("typedesc"));
				o.setOpportunityOddsTypeName(rs.getString("oddstype_name"));
				o.setMarket(m);
				o.setWinLose(rs.getLong("winlose"));
				o.setSkinGroupMappings(loadOpportunitySkingGroupMappings(conn, o.getId()));
				o.setOddsGroup(rs.getString("odds_group"));
				list.add(o);
			}
		} catch (SQLException sqle) {
 			log.log(Level.ERROR, "", sqle);
			throw sqle;
		} catch (Throwable t) {
			log.log(Level.ERROR, "", t);
		} finally {
 			closeResultSet(rs);
			closeStatement(pstmt);
		}
		return list;
	}

	public static ArrayList<OpportunityShifting> searchShiftings(Connection con, Date from, long groupId, long marketId, long shiftId) throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		ArrayList<OpportunityShifting> list = new ArrayList<OpportunityShifting>();
		try {
			String sql =
					"SELECT " +
						"os.*, " +
						"o.market_id, " +
						"to_char(o.time_est_closing, 'YYYY-MM-DD HH24:MI:SS.FF3 TZHTZM') AS time_est_closing, " +
						"o.scheduled " +
					"FROM " +
						"opportunity_shiftings os, " +
						"opportunities o, " +
						"markets m " +
					"WHERE " +
						"os.opportunity_id = o.id AND " +
						"m.id = o.market_id ";

			if (shiftId != 0) {
				sql += " and os.opportunity_id = " + shiftId;
			} else {
				sql += " and os.shifting_time >= ? and os.shifting_time <= ? ";
				if (marketId != 0) {
					sql += " and o.market_id = " + marketId;
				} // NO need for market groups now.
				/*else if (groupId != 0) { //
					//using skin_id=SKIN_ETRADER which is the etrader skinId do get the market.id group
					sql+= " and m.id in (select smgm.market_id " +
							"from skin_market_group_markets smgm , skin_market_groups smg " +
							"where smgm.skin_market_group_id=smg.id and smg.skin_id=" + ConstantsBase.SKIN_ETRADER +" and smg.market_group_id=" +groupId + ")";
				}*/
			}

			sql += " order by os.shifting_time desc";

			ps = con.prepareStatement(sql);

			if (shiftId == 0) {
				ps.setTimestamp(1, CommonUtil.convertToTimeStamp(CommonUtil.getDateTimeFormat(from, Utils.getAppData().getUtcOffset())));
				ps.setTimestamp(2, CommonUtil.convertToTimeStamp(CommonUtil.getDateTimeFormat(CommonUtil.addDay(from), Utils.getAppData().getUtcOffset())));
			}

			rs = ps.executeQuery();
			while (rs.next()) {
				list.add(getOppShiftingVO(rs));
			}
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		return list;
	}

	public static ArrayList<OptionExpiration> searchOptionsExpiration(Connection con, Long marketId, int month, int year) throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		ArrayList<OptionExpiration> list = new ArrayList<OptionExpiration>();
		try {
			String sql = " select oe.*, m.IS_NEXT_AGREEMENT_SAME_DAY ,m.display_name marketname,w.user_name username" +
			" from options_expirations oe,markets m,writers w " + " where oe.market_ID = m.id and oe.writer_id=w.id ";
			sql += " and to_char(oe.expiry_date,'mm')=" + month;
			sql += " and to_char(oe.expiry_date,'yyyy')=" + year;

			if (marketId != 0) {
				sql += " and m.id=" + marketId;
			}
			sql += " order by oe.market_id,oe.expiry_date";

			ps = con.prepareStatement(sql);
			rs = ps.executeQuery();
			while(rs.next()) {
				OptionExpiration oe = getOptionExpirationVO(rs);
				oe.setMarketName(MarketsManagerBase.getMarketName(Skins.SKIN_REG_EN, rs.getLong("market_id")));
				oe.setWriterName(rs.getString("username"));
				oe.setNextAgreementOnSameDay(rs.getInt("IS_NEXT_AGREEMENT_SAME_DAY")==1);

				list.add(oe);
			}
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		return list;
	}

	public static ArrayList<OpportunityTemplate> searchOppTemplates(Connection con, long groupId, long marketId, boolean isSAdmin,long active, long skinId) throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		ArrayList<OpportunityTemplate> list = new ArrayList<OpportunityTemplate>();
		try {
			String sql =
					" select ot.*,m.display_name market_name ,ods.name odds_name,types.description type_desc, ot.odds_group oddsGroup, get_opp_pair_group_pct(ot.odds_group) as oddsGroupValue " +
					" from opportunity_templates ot,markets m,OPPORTUNITY_ODDS_TYPES ods,OPPORTUNITY_TYPES types" +
					" where ot.market_id = m.id and ot.ODDS_TYPE_ID = ods.id and ot.OPPORTUNITY_TYPE_ID = types.id ";

			// NO MARKET GROUPS FOR NOW
			/*if (groupId > 0)
			//using skin_id=SKIN_ETRADER which is the etrader skinId do get the market.id group
			sql+= " and m.id in (select smgm.market_id " +
					"from skin_market_group_markets smgm , skin_market_groups smg " +
					"where smgm.skin_market_group_id=smg.id and smg.skin_id=" + ConstantsBase.SKIN_ETRADER +" and smg.market_group_id="+groupId+")";

			*/
			if (marketId > 0) {
				sql += " and m.id=" + marketId;
			}

			if (!isSAdmin) {
				sql += " and types.id = " + 2;
			}

			if (active > 0){
				if ( active == ConstantsBase.USERS_MARKET_DIS_ACTIVE ) {
		    		sql +=  " AND ot.IS_ACTIVE = 1 ";
		    	} else {
		    		sql +=  " AND ot.IS_ACTIVE = 0 ";
		    	}
			}
			sql += " order by m.id, ot.time_first_invest";
			ps = con.prepareStatement(sql);
			rs = ps.executeQuery();
			while(rs.next()) {
				OpportunityTemplate ot = getOppTemplatesVO(rs);
				ot.setMarketName(MarketsManagerBase.getMarketName(skinId, rs.getLong("market_id")));
				ot.setOddsTypeName(rs.getString("odds_name"));
				ot.setOpTypeDesc(rs.getString("type_desc"));
				ot.setOddsGroup(rs.getString("oddsGroup"));
				ot.setOddsGroupValue(rs.getString("oddsGroupValue"));
				list.add(ot);
			}
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		return list;
	}

	public static void insertOppTemplate(Connection con, OpportunityTemplate vo) throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		int index = 1;
		try {

			String sql =
					" insert into opportunity_templates(" +
					"ID,MARKET_ID,TIME_FIRST_INVEST," +
					" TIME_EST_CLOSING,TIME_LAST_INVEST,ODDS_TYPE_ID," +
					" WRITER_ID,OPPORTUNITY_TYPE_ID,is_active,scheduled, time_zone, " +
					"DEDUCT_WIN_ODDS, is_open_graph";
					if (vo.getOpportunityTypeId() == Opportunity.TYPE_ONE_TOUCH) {
						sql += ", ONE_TOUCH_DECIMAL_POINT, UP_DOWN";
					}
					if (vo.getOpportunityTypeId() == Opportunity.TYPE_REGULAR) {
						sql += ", ODDS_GROUP";
					}
					if (vo.getMaxInvAmountCoeffPerUser() > 0) {
						sql += ", MAX_INV_COEFF";
					}
					if (vo.getFullDayWeek() != null && vo.getHalfDayWeek() != null 
							&& vo.getFullDayWeek().length == 7 && vo.getHalfDayWeek().length == 7) {
						sql +=  " , SUN_F , MON_F , TUE_F , WED_F , THU_F , FRI_F , SAT_F , SUN_H , " +
								" MON_H , TUE_H , WED_H , THU_H , FRI_H , SAT_H ";
					}
					sql += " ,TIME_EST_CLOSING_HALFDAY , TIME_LAST_INVEST_HALFDAY "; 
					sql += ") values (SEQ_OPPORTUNITY_TEMPLATES.nextval,?,?,?,?,?,?,?,?,?,?, ?,?" ;
					if (vo.getOpportunityTypeId() == Opportunity.TYPE_ONE_TOUCH) {
						sql += ", ?, ?";
					}
					if (vo.getOpportunityTypeId() == Opportunity.TYPE_REGULAR) {
						sql += " , ?";
					}
					if (vo.getMaxInvAmountCoeffPerUser() > 0) {
						sql += ", ?";
					}
					if (vo.getFullDayWeek() != null && vo.getHalfDayWeek() != null 
							&& vo.getFullDayWeek().length == 7 && vo.getHalfDayWeek().length == 7) {
						sql+= " , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? ";
					}
					sql += ", ? , ? ) ";

			ps = con.prepareStatement(sql);
			ps.setLong(index++, vo.getMarketId());
			ps.setString(index++, vo.getTimeFirstInvest());
			ps.setString(index++, vo.getTimeEstClosing());
			ps.setString(index++, vo.getTimeLastInvest());
			ps.setLong(index++, vo.getOddsTypeId());
			ps.setLong(index++, vo.getWriterId());
			ps.setLong(index++, vo.getOpportunityTypeId());
			ps.setInt(index++, vo.isActive() ? 1 : 0);
			ps.setLong(index++, vo.getScheduled());
			ps.setString(index++, vo.getTimeZone());
//			ps.setInt(index++, vo.getFullDay());
//			ps.setInt(index++, vo.isHalfDay() ? 1 : 0);
			ps.setInt(index++, vo.isDeductWinOdds() ? 1 : 0);
			ps.setInt(index++, vo.isOpenGraph() ? 1 : 0);
			if (vo.getOpportunityTypeId() == Opportunity.TYPE_ONE_TOUCH) {
				ps.setLong(index++, vo.getOneTouchDecimalPoint());
				ps.setInt(index++, Integer.parseInt(vo.getUpDown()));
			}
			if (vo.getOpportunityTypeId() == Opportunity.TYPE_REGULAR) {
				ps.setString(index++, vo.getOddsGroup());				
			}
			if (vo.getMaxInvAmountCoeffPerUser() > 0) {
				ps.setDouble(index++, vo.getMaxInvAmountCoeffPerUser());
			}
			if (vo.getFullDayWeek() != null && vo.getHalfDayWeek() != null 
					&& vo.getFullDayWeek().length == 7 && vo.getHalfDayWeek().length == 7) {
				for (int i : vo.getFullDayWeek()) {
					ps.setLong(index++, i);
				}
				for (int j : vo.getHalfDayWeek()) {
					ps.setLong(index++, j);
				}
			}
			ps.setString(index++, vo.getTimeEstClosingHalfday());
			ps.setString(index++, vo.getTimeLastInvestHalfday());
			ps.executeUpdate();

			vo.setId(getSeqCurValue(con, "SEQ_OPPORTUNITY_TEMPLATES"));
		} finally {
			closeStatement(ps);
			closeResultSet(rs);
		}
	}

	public static void updateOppTemplateAllMarketsOdds(Connection con, long marketId, long oddsId, String oddsPairGroup) throws SQLException {

		updateQuery(con, "update opportunity_templates set ODDS_TYPE_ID=" + oddsId +
							", ODDS_GROUP = decode(opportunity_type_id,1,'"+ oddsPairGroup +"',ODDS_GROUP) "+ 
							" where market_id=" + marketId);

	}

	public static void updateOppTemplatesOdds(Connection con, OpportunityTemplate template) throws SQLException {
		String sql = "UPDATE opportunity_templates ot " +
				        " set " +
				            " ot.ODDS_TYPE_ID=" + template.getOddsTypeId() +
				            " ,ot.ODDS_GROUP = decode(ot.opportunity_type_id,1,'"+ template.getOddsGroup() +"',ot.ODDS_GROUP) "+
				     " WHERE ot.market_id = " + template.getMarketId();
		//change ALL templates in market
		if (!template.isChangeAll()) {
			sql += " AND ot.scheduled = " + template.getScheduled();
		}
		if (template.isChangeAll() && template.getScheduled()!=5) {
			sql += " AND ot.scheduled <> 5" ; // change all except 1T
		}
		if (template.isChangeAll() && template.getScheduled()==5) {
			sql += " AND ot.scheduled = 5" ; // change all except 1T
		}
		updateQuery(con, sql);
	}




	public static void updateOppTemplate(Connection con, OpportunityTemplate vo) throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		int index = 1;
		try {

			String sql =
					"update opportunity_templates " +
					"set " +
						"MARKET_ID = ?, " +
						"TIME_FIRST_INVEST = ?, " +
						"TIME_EST_CLOSING = ?, " +
						"TIME_LAST_INVEST = ?, " +
						"ODDS_TYPE_ID = ?, " +
						"WRITER_ID = ?, " +
						"OPPORTUNITY_TYPE_ID = ?, " +
						"is_active = ?, " +
						"scheduled = ?, " +
						"time_zone = ?, " +
//						"is_full_day = ?, " +
//						"is_half_day = ?, " +
						"DEDUCT_WIN_ODDS = ?," +
						"is_open_graph = ?, " +
						"odds_group = ? ";
						if (vo.getOpportunityTypeId() == Opportunity.TYPE_ONE_TOUCH) {
							sql += ", ONE_TOUCH_DECIMAL_POINT = ?, " +
								   "UP_DOWN = ?";
						}
						if (vo.getMaxInvAmountCoeffPerUser() > 0) {
							sql += ", MAX_INV_COEFF = ?";
						}
						if (vo.getFullDayWeek() != null && vo.getHalfDayWeek() != null 
								&& vo.getFullDayWeek().length == 7 && vo.getHalfDayWeek().length == 7) {
							sql+=
							" , SUN_F = ? , MON_F = ? , TUE_F = ? , WED_F = ? , THU_F = ? , FRI_F = ? , SAT_F = ? " +
							" , SUN_H = ? , MON_H = ? , TUE_H = ? , WED_H = ? , THU_H = ? , FRI_H = ? , SAT_H = ? ";
						}
						sql+=
							" , TIME_EST_CLOSING_HALFDAY = ? " +
							" , TIME_LAST_INVEST_HALFDAY = ? " +	
						" where " +
							" id = ? ";

			ps = con.prepareStatement(sql);

			ps.setLong(index++, vo.getMarketId());
			ps.setString(index++, vo.getTimeFirstInvest());
			ps.setString(index++, vo.getTimeEstClosing());
			ps.setString(index++, vo.getTimeLastInvest());
			ps.setLong(index++, vo.getOddsTypeId());
			ps.setLong(index++, vo.getWriterId());
			ps.setLong(index++, vo.getOpportunityTypeId());
			ps.setInt(index++, (vo.isActive() ? 1 : 0));
			ps.setLong(index++, vo.getScheduled());
			ps.setString(index++, vo.getTimeZone());
//			ps.setInt(index++, vo.getFullDay());
//			ps.setInt(index++, vo.isHalfDay() ? 1 : 0);
			ps.setInt(index++, vo.isDeductWinOdds() ? 1 : 0);
			ps.setInt(index++, vo.isOpenGraph() ? 1 : 0);
			ps.setString(index++, vo.getOddsGroup());
			if (vo.getOpportunityTypeId() == Opportunity.TYPE_ONE_TOUCH) {
				ps.setLong(index++, vo.getOneTouchDecimalPoint());
				ps.setInt(index++, Integer.parseInt(vo.getUpDown()));
			}
			if (vo.getMaxInvAmountCoeffPerUser() > 0) {
				ps.setDouble(index++, vo.getMaxInvAmountCoeffPerUser());
			}
			if (vo.getFullDayWeek() != null && vo.getHalfDayWeek() != null 
					&& vo.getFullDayWeek().length == 7 && vo.getHalfDayWeek().length == 7) {
				for (int i : vo.getFullDayWeek()) {
					ps.setLong(index++, i);
				}
				for (int j : vo.getHalfDayWeek()) {
					ps.setLong(index++, j);
				}
			}
			ps.setString(index++, vo.getTimeEstClosingHalfday());
			ps.setString(index++, vo.getTimeLastInvestHalfday());
			ps.setLong(index++, vo.getId());

			ps.executeUpdate();

		} finally {
			closeStatement(ps);
			closeResultSet(rs);
		}
	}

	public static void updateOpportunityOdds(Connection con, Opportunity vo, long oddsTypeId, String oddsPairGroup) throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		String sql = " UPDATE opportunities o "
				+ " SET "
					+ " o.odds_type_id=? ," 
					+ " odds_group = GET_WITH_DEFAULT_VALUE(?) "
				+ " WHERE o.id =?";
		try {
			ps = con.prepareStatement(sql);
			ps.setLong(1, oddsTypeId);
			ps.setString(2, oddsPairGroup == null ? "" : oddsPairGroup);
			ps.setLong(3, vo.getId());
			rs = ps.executeQuery();
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
	}

	public static void updateOpportunity(Connection con, Opportunity vo) throws SQLException {
		// This method is called by a form that is not used anymore.
	    	// ^^^^^^^^^^^^ 	/* DEV-1821 */
		// They do per CORE-2057 and CORE-2176 ....
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			String sql = " UPDATE opportunities "
					+ " SET "
						+ " is_disabled=?,"
						+ " odds_type_id=?, "
						+ " odds_group = ? ";

			if ( vo.getIsOneTouchOpp() ) {
				sql += ",current_level = ? "
					+ " ,is_published = ? ";
			}
				sql += " WHERE id=? ";

			ps = con.prepareStatement(sql);
			ps.setInt(1, (vo.isDisabled() ? 1 : 0));			
			ps.setLong(2, vo.getOddsTypeId());
			ps.setString(3, vo.getOddsGroup() == null ? "" : vo.getOddsGroup());

			if (vo.getIsOneTouchOpp()) {   // if one touch opp
				ps.setDouble(4, vo.getCurrentLevel());
				ps.setInt(5, vo.getIsPublished());
				ps.setLong(6, vo.getId());
			} else {
				ps.setLong(4, vo.getId());
			}
			ps.executeUpdate();
		} finally {
			closeStatement(ps);
			closeResultSet(rs);
		}
	}

	public static HashMap<Long,Exchange> getExchanges(Connection con) throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		HashMap<Long,Exchange> hm = new HashMap<Long,Exchange>();
		try {
			String sql = "select * from exchanges ";
			ps = con.prepareStatement(sql);
			rs = ps.executeQuery();
			while(rs.next()) {
				Exchange vo = new Exchange();
				vo.setId(rs.getLong("id"));
				vo.setName(rs.getString("name"));
				vo.setHalfDayClosingTime(rs.getString("half_day_close_time"));
				hm.put(rs.getLong("id"),vo);
			}
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		return hm;
	}

	public static ArrayList<Exchange> searchExchanges(Connection con) throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		ArrayList<Exchange> list = new ArrayList<Exchange>();
		try {
			String sql = "select * from exchanges ";
			ps = con.prepareStatement(sql);
			rs = ps.executeQuery();
			while(rs.next()) {
				Exchange exchange = new Exchange();
				exchange.setId(rs.getLong("id"));
				exchange.setName(rs.getString("name"));
				exchange.setHalfDayClosingTime(rs.getString("half_day_close_time"));
				list.add(exchange);
			}
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		return list;
	}

	public static boolean checkForDoubleMarketsPerMonth(Connection con,long marketId,Date expiryDate) throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			String sql = "select * from OPTIONS_EXPIRATIONS where market_id=? and to_char(expiry_date,'mm')=to_char(?,'mm') and to_char(expiry_date,'yyyy')=to_char(?,'yyyy')";
			ps = con.prepareStatement(sql);

			ps.setLong(1, marketId);
			ps.setTimestamp(2, CommonUtil.convertToTimeStamp(expiryDate));
			ps.setTimestamp(3, CommonUtil.convertToTimeStamp(expiryDate));

			rs = ps.executeQuery();
			if (rs.next()) {
				return true;
			}
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		return false;
	}

	public static ArrayList<ExchangesHolidays> searchExchangesHolidays(Connection con, Long exchangeId, Date from) throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		ArrayList<ExchangesHolidays> list = new ArrayList<ExchangesHolidays>();
		try {
			String sql = "select eh.*, e.name name " + " from exchange_holidays eh, exchanges e" + " where eh.exchange_id = e.id ";

			if (exchangeId != null) {
				sql += " and eh.exchange_id=" + exchangeId;
			}
			if (from != null) {
				sql += " and eh.holiday>=? and eh.holiday<=? ";
			}

			sql += " order by eh.exchange_id, eh.holiday";

			ps = con.prepareStatement(sql);

			if (from != null) {
				ps.setTimestamp(1, CommonUtil.convertToTimeStamp(CommonUtil.getDateTimeFormat(from, Utils.getAppData().getUtcOffset())));
				ps.setTimestamp(2, CommonUtil.convertToTimeStamp(CommonUtil.getDateTimeFormat(CommonUtil.addDay(from), Utils.getAppData().getUtcOffset())));
			}

			rs = ps.executeQuery();
			while(rs.next()) {
				ExchangesHolidays exchangesHolidays = new ExchangesHolidays();
				exchangesHolidays.setId(rs.getLong("id"));
				exchangesHolidays.setHoliday(convertToDate(rs.getTimestamp("holiday")));
				exchangesHolidays.setExchangeId(rs.getLong("exchange_id"));
				exchangesHolidays.setHalfDay(rs.getInt("is_half_day") == 1);
				exchangesHolidays.setExchangeName(rs.getString("name"));
				list.add(exchangesHolidays);
			}
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		return list;
	}

	public static void updateExchange(Connection con, Exchange vo) throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			String sql = " update exchanges set NAME=?, HALF_DAY_CLOSE_TIME=? where id=? ";
			ps = con.prepareStatement(sql);
			ps.setString(1, vo.getName());
			ps.setString(2, vo.getHalfDayClosingTime());
			ps.setLong(3, vo.getId());
			ps.executeUpdate();
		} finally {
			closeStatement(ps);
			closeResultSet(rs);
		}
	}

	public static void updateExchangeHoliday(Connection con, ExchangesHolidays vo) throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			String sql = " update EXCHANGE_HOLIDAYS set HOLIDAY=?, EXCHANGE_ID=?, IS_HALF_DAY=? where id=? ";
			ps = con.prepareStatement(sql);
			ps.setTimestamp(1, CommonUtil.convertToTimeStamp(vo.getHoliday()));
			ps.setLong(2, vo.getExchangeId());
			ps.setInt(3, vo.isHalfDay()==true?1:0);
			ps.setLong(4, vo.getId());
			ps.executeUpdate();
		} finally {
			closeStatement(ps);
			closeResultSet(rs);
		}
	}

	public static void insertExchangeHoliday(Connection con, ExchangesHolidays vo) throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			String sql = " insert into EXCHANGE_HOLIDAYS(id,hOLIDAY,EXCHANGE_ID,IS_HALF_DAY) values(SEQ_EXCHANGE_HOLIDAYS.nextval,?,?,?)";
			ps = con.prepareStatement(sql);
			ps.setTimestamp(1, CommonUtil.convertToTimeStamp(vo.getHoliday()));
			ps.setLong(2, vo.getExchangeId());
			ps.setInt(3, vo.isHalfDay()==true?1:0);
			ps.executeUpdate();
			vo.setId(getSeqCurValue(con, "SEQ_EXCHANGE_HOLIDAYS"));
		} finally {
			closeStatement(ps);
			closeResultSet(rs);
		}
	}

	public static void insertOptionExpiration(Connection con, OptionExpiration vo) throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			String sql = "INSERT INTO " +
                            "options_expirations(" +
                                "id, " +
                                "market_id, " +
                                "expiry_date, " +
                                "is_active, " +
                                "writer_id, " +
                                "time_created, " +
                                "expiry_date_end, " +
                                "ric_letter, " +
                                "ric_year) " +
					"VALUES(" +
                        "seq_options_expirations.nextval, " +
                        "?, " +
                        "?, " +
                        "?, " +
                        "?, " +
                        "sysdate, " +
                        "?, " +
                        "?, " +
                        "?)";

			ps = con.prepareStatement(sql);
			ps.setLong(1, vo.getMarketId());
			ps.setTimestamp(2, CommonUtil.convertToTimeStamp(vo.getExpiryDate()));
			ps.setInt(3, vo.isActive()==true?1:0);
			ps.setLong(4, vo.getWriterId());
            ps.setTimestamp(5, CommonUtil.convertToTimeStamp(vo.getExpiryDateEnd()));
            ps.setString(6, vo.getLetter());
            ps.setString(7, vo.getYear());
			ps.executeUpdate();
			vo.setId(getSeqCurValue(con, "SEQ_OPTIONS_EXPIRATIONS"));
		} finally {
			closeStatement(ps);
			closeResultSet(rs);
		}
	}

	public static void updateOptionExpiration(Connection con, OptionExpiration vo) throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			String sql = "UPDATE " +
                            "options_expirations " +
    					"SET " +
                            "market_id = ?, " +
                            "expiry_date = ?, " +
                            "is_active = ?, " +
                            "writer_id = ?, " +
                            "expiry_date_end = ?, " +
                            "ric_letter = ?, " +
                            "ric_year = ? " +
                        "WHERE " +
                            "id = ?";
			ps = con.prepareStatement(sql);
			ps.setLong(1, vo.getMarketId());
			ps.setTimestamp(2, CommonUtil.convertToTimeStamp(vo.getExpiryDate()));
			ps.setInt(3, vo.isActive()==true?1:0);
			ps.setLong(4, vo.getWriterId());
            ps.setTimestamp(5, CommonUtil.convertToTimeStamp(vo.getExpiryDateEnd()));
            ps.setString(6, vo.getLetter());
            ps.setString(7, vo.getYear());
			ps.setLong(8, vo.getId());
			ps.executeUpdate();
		} finally {
			closeStatement(ps);
			closeResultSet(rs);
		}
	}

	/**
	 * Insert one touch oppurtunity to Db
	 * First insert to opportunities table and second insert to
	 * opportunity_one_touch table.
	 * @param con
	 * @param vo
	 * @throws SQLException
	 */
	public static void insertOneTouchOpportunity(Connection con, Opportunity opportunity,
			Calendar firstInvCal,Calendar estClosingCal, Calendar lastInvalCal,
				String timeZone, String firstInv, String estClosing, String lastInval, long skinId ) throws SQLException {
		// This method is no longer used.
		
		PreparedStatement ps1 = null;
		PreparedStatement ps2 = null;
		try {
			con.setAutoCommit(false);

			String sql = "insert into opportunities " +
		            	  "(id, market_id, time_created, time_first_invest, time_est_closing, " +
		            	  "time_last_invest, is_published, is_settled, odds_type_id, writer_id, " +
		            	  "opportunity_type_id, is_disabled, scheduled) " +

		            	  "values (SEQ_OPPORTUNITIES.NEXTVAL,?,current_timestamp,"+
		            	  "to_timestamp_tz('" + firstInvCal.get(Calendar.YEAR) + "-" +
		            	  	(firstInvCal.get(Calendar.MONTH) + 1) + "-" + firstInvCal.get(Calendar.DAY_OF_MONTH) +
		            	  		" " + firstInv + " " + timeZone + "', 'yyyy-mm-dd hh24:mi TZR')," +

		              	  "to_timestamp_tz('" + estClosingCal.get(Calendar.YEAR) + "-" +
		          	  		(estClosingCal.get(Calendar.MONTH) + 1) + "-" + estClosingCal.get(Calendar.DAY_OF_MONTH) +
		          	  			" " + estClosing + " " + timeZone + "', 'yyyy-mm-dd hh24:mi TZR')," +


		                   "to_timestamp_tz('" + lastInvalCal.get(Calendar.YEAR) + "-" +
		                	(lastInvalCal.get(Calendar.MONTH) + 1) + "-" + lastInvalCal.get(Calendar.DAY_OF_MONTH) +
		                		" " + lastInval + " " + timeZone + "', 'yyyy-mm-dd hh24:mi TZR')," +

		                	"?,?,?,?,?,?,? ) ";

			ps1 = con.prepareStatement(sql);
			ps1.setLong(1, opportunity.getMarketId());
			ps1.setInt(2, opportunity.getIsPublished());
			ps1.setInt(3, opportunity.getIsSettled());
			ps1.setLong(4, opportunity.getOddsTypeId());
			ps1.setLong(5, opportunity.getWriterId());
			ps1.setLong(6, opportunity.getOpportunityTypeId());

			if ( opportunity.isDisabled() ) {
				ps1.setLong(7, 1);
			}
			else {
				ps1.setLong(7, 0);
			}

			ps1.setInt(8, opportunity.getScheduled());
			ps1.executeUpdate();

			// insert into opportunity_one_touch table
			sql = "insert into opportunity_one_touch " +
  			  	 "(id, opportunity_id, up_down, opportunity_msg, decimal_point) " +
			   	 "values(SEQ_OPPORTUNITY_ONE_TOUCH.NEXTVAL,SEQ_OPPORTUNITIES.CURRVAL,?,?,?) ";

			ps2 = con.prepareStatement(sql);
			ps2.setLong(1, opportunity.getOneTouchUpDown());
			ps2.setString(2, opportunity.getMsg());
			ps2.setLong(3, opportunity.getDecimalPointOneTouch());

			ps2.executeUpdate();

			con.commit();
			log.info("insert new One Touch opportunity finished successfully!.");
		} catch (SQLException e) {
			log.error("Error in insert new One Touch opportunity.", e);
			try {
	              con.rollback();
            } catch (Throwable it) {
                log.error("Can't rollback.", it);
            }
            throw e;
		} finally {
			con.setAutoCommit(true);
			closeStatement(ps1);
			closeStatement(ps2);
		}
	}

   /**
    * Update upDown field for one touch opp
    * @param con
    * @param id
    * 			opportunity id
    * @param upDown
    * 			upDown field of the opp
    * @throws SQLException
    */
	public static void updateOneTouchOpportunity(Connection con, long id, long upDown) throws SQLException {
		PreparedStatement ps = null;

		try {
			String sql;

			sql = "update opportunity_one_touch set up_down = ? " +
			  	  "where opportunity_id = ? ";

			ps = con.prepareStatement(sql);

			ps.setLong(1, upDown);
			ps.setLong(2, id);

			ps.executeUpdate();

		} finally {
			closeStatement(ps);
		}
	}
	
	public static void updateCurrentLevelOfOneTouchOpportunity(Connection con, long id, double currentLevel) throws SQLException {
		PreparedStatement ps = null;

		try {
			String sql;

			sql = "update opportunities set current_level = ? " +
			  	  "where id = ? ";

			ps = con.prepareStatement(sql);

			ps.setDouble(1, currentLevel);
			ps.setLong(2, id);

			ps.executeUpdate();

		} finally {
			closeStatement(ps);
		}
	}

	/**
	 * This method returns all opportunities by template
	 * (The key between opportunity_template and opportunities is market_id@@shceduled)
	 * @param con - connection string
	 * @param template - the opportunities template
	 * @param days - the number of days to ignore (i.e only opportunities with 'time_first_invest > sysdate + days' will be retreived)
	 * @param allOppsInMarket - retrive all opps in the template  market
	 * @param published - retrive only published opps
	 * @return
	 */
	public static ArrayList<Opportunity> getOpportunitiesByTemplate(Connection con, OpportunityTemplate template, int days, boolean allOppsInMarket, boolean published) throws SQLException  {
		PreparedStatement ps = null;
		ResultSet rs = null;
		ArrayList<Opportunity> list = new ArrayList<Opportunity>();


		String sql =	"SELECT " +
							"o.*, " + 
							"o.is_suspended AS is_opp_suspended, " +
							getFormatTimesSQL() +
						"FROM " +
							"opportunities o " +
						"WHERE " +
							//only retrive non settled opps
							"o.is_settled = 0 AND " +

							//only retrive opps with first investment of 'days' days ahead
							"trunc(o.time_first_invest) >= trunc(sysdate + " + days + ")" +

							//only for market_id
							" AND o.market_id = " + template.getMarketId();

							//retrive all opps for this market
							if (!allOppsInMarket) {
								sql += " AND o.scheduled = " + template.getScheduled();
							}

							//retrive only published opps
							if (!published) {
								sql += " AND o.published = 0";
							}

		try {
			ps = con.prepareStatement(sql);
			rs = ps.executeQuery();
			Opportunity o = null;
			while(rs.next()) {
				o = getVO(rs);
				o.setSkinGroupMappings(loadOpportunitySkingGroupMappings(con, o.getId()));
				list.add(o);
			}
		} catch (SQLException e) {
			log.error(e.toString());
		}finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		return list;
	}

	public static void updateOpportunitiesMaxExposure(Connection con, long marketId, int maxExposure) throws SQLException {
		PreparedStatement ps = null;

		try {
			String sql;
			sql = " UPDATE opportunity_skin_group_map SET max_exposure = ? " +
					" WHERE opportunity_id IN " +
						" (SELECT id FROM opportunities WHERE market_id = ? AND is_settled = 0 AND opportunity_type_id = 1) ";

			ps = con.prepareStatement(sql);

			ps.setInt(1, maxExposure);
			ps.setLong(2, marketId);

			ps.executeUpdate();

		} finally {
			closeStatement(ps);
		}
	}
	
	
	public static void updateOpportunitiesWorstCaseReturn(Connection con, long marketId, long worstCaseReturn) throws SQLException {
		PreparedStatement ps = null;

		try {
			String sql;
			sql = " UPDATE opportunity_skin_group_map SET worst_case_return = ? " +
					" WHERE opportunity_id IN " +
						" (SELECT id FROM opportunities WHERE market_id = ? AND is_settled = 0 AND opportunity_type_id = " + Investment.TYPE_BUBBLE + ") ";

			ps = con.prepareStatement(sql);

			ps.setLong(1, worstCaseReturn);
			ps.setLong(2, marketId);

			ps.executeUpdate();

		} finally {
			closeStatement(ps);
		}
	}

	/**
	 * disable or enable opp by trader
	 * @param con connection to db
	 * @param oppid opp id to disable or enable
	 * @param disable 1 to disable, 0 to enable
	 * @throws SQLException
	 */
	public static void disableEnableOppByTrader(Connection con, long oppid, int disable) throws SQLException {
		PreparedStatement ps = null;
		try {
			String sql;

		    sql =
                "UPDATE " +
                    "opportunities " +
                "SET " +
                    "is_disabled_trader = ?, ";
                    if (disable == 0) {
                    	sql += "is_disabled = CASE WHEN is_disabled_service = 0 THEN 0 ELSE 1 END ";
                    } else {
                    	sql += "is_disabled = 1 ";
                    }
            sql += "WHERE " +
                		"id = ? ";
			ps = con.prepareStatement(sql);
			ps.setInt(1, disable);
			ps.setLong(2, oppid);
			ps.executeUpdate();
		} finally {
			closeStatement(ps);
		}
	}

	/**
	 * check if opp is disable by service or not
	 * @param con
	 * @param opId
	 * @return true if disable false if not
	 * @throws SQLException
	 */
	public static boolean isDisableByService(Connection con, long opId) throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		boolean disable = true;
		try {
			String sql;

			sql = "select is_disabled_service from opportunities " +
			  	  "where id = ?";

			ps = con.prepareStatement(sql);

			ps.setLong(1, opId);

			rs = ps.executeQuery();

			while(rs.next()) {
				disable = rs.getInt("is_disabled_service") == 1 ? true : false;
			}

		} finally {
			closeStatement(ps);
		}
		return disable;
	}

	/**
	 * update opp new level for resettle
	 * @param con
	 * @param opId opp id
	 * @param level new level for update
	 * @param writer
	 * @throws SQLException
	 */
	public static void updateOppNewLevel(Connection con, long opId, double level, long writer) throws SQLException {

		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {

			String sql = "update opportunities o set TIME_SETTLED = sysdate, closing_level = ?, is_published = 0, is_settled = 1, " +
						 "writer_id = ? where id = ?";

			pstmt = con.prepareStatement(sql);
			pstmt.setDouble(1, level);
			pstmt.setLong(2, writer);
			pstmt.setLong(3, opId);

			pstmt.executeUpdate();

		} catch (SQLException sqle) {
			log.log(Level.ERROR, "", sqle);
			throw sqle;
		} catch (Throwable t) {
			log.log(Level.ERROR, "", t);
		} finally {
			closeResultSet(rs);
			closeStatement(pstmt);
		}
	}

	public static Opportunity getById(Connection con, long id) throws SQLException {
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            String sql =" select o.is_disabled_trader,o.is_disabled_service," +
                    " o.closing_level, o.current_level, o.id, o.is_disabled, o.is_published, o.is_settled, o.market_id, " +
                    " o.odds_type_id, o.opportunity_type_id, o.scheduled, o.writer_id, o.CLOSING_LEVEL_CALCULATION," +
                    " to_char(o.time_act_closing, 'YYYY-MM-DD HH24:MI:SS.FF3 TZHTZM') AS ftime_act_closing, " +
                    " to_char(o.time_created, 'YYYY-MM-DD HH24:MI:SS.FF3 TZHTZM') AS ftime_created, " +
                    " to_char(o.time_est_closing, 'YYYY-MM-DD HH24:MI:SS.FF3 TZHTZM') AS ftime_est_closing, " +
                    " to_char(o.time_first_invest, 'YYYY-MM-DD HH24:MI:SS.FF3 TZHTZM') AS ftime_first_invest, " +
                    " to_char(o.time_last_invest, 'YYYY-MM-DD HH24:MI:SS.FF3 TZHTZM') AS ftime_last_invest, " +
                    " to_char(o.time_settled, 'YYYY-MM-DD HH24:MI:SS.FF3 TZHTZM') AS ftime_settled, " +
                    " smg.market_group_id, m.display_name, m.feed_name, m.decimal_point, m.investment_limits_group_id, " +
                    " o.max_inv_coeff, o.is_suspended AS is_opp_suspended " +
//                  " m.home_page_priority, m.home_page_hour_first, m.group_priority " +
                    " from opportunities o, markets m , skin_market_group_markets smgm, skin_market_groups smg " +
                    " where o.id=? and m.id = o.market_id " +
                    " and smgm.skin_market_group_id = smg.id and m.id = smgm.market_id " +
            		"and smg.skin_id IN (" + Utils.getWriter().getSkinsToString() + ") ";

            ps = con.prepareStatement(sql);
            ps.setLong(1, id);

            rs = ps.executeQuery();
            while (rs.next()) {
                Opportunity opportunity = getVO(rs);
                Market market = new Market();
                market.setGroupId(new Long(rs.getLong("market_group_id")));
              //  market.setDisplayName(CommonUtil.getMessage(rs.getString("display_name"), null));
                market.setFeedName(rs.getString("feed_name"));
                market.setDecimalPoint(new Long(rs.getLong("decimal_point")));
                market.setInvestmentLimitsGroupId(new Long(rs.getLong("investment_limits_group_id")));
//                market.setHomePagePriority(rs.getInt("home_page_priority"));
//                market.setHomePageHourFirst(rs.getBoolean("home_page_hour_first"));
//                market.setGroupPriority(rs.getInt("group_priority"));
                market.setId(rs.getLong("market_id"));
                opportunity.setMarket(market);
                opportunity.setSkinGroupMappings(loadOpportunitySkingGroupMappings(con, opportunity.getId()));
                return opportunity;
            }
        } catch (Throwable t) {
            log.log(Level.ERROR, "", t);
        } finally {
            closeResultSet(rs);
            closeStatement(ps);
        }
        return null;
    }

	/**
	 * Get not published opportunities
	 * @param con db connection
	 * @param from time estimated closing filter
	 * @param settled settled filter
	 * @return
	 * @throws SQLException
	 * @throws ParseException
	 */
	public static ArrayList<OpportunityTotal> getTtOpportunities(Connection con, Date from, String settled,
			long marketId, long scheduled, long groupId, boolean isZeroOneHundred) throws SQLException, ParseException {

		PreparedStatement ps = null;
		ResultSet rs = null;
		ArrayList<OpportunityTotal> list = new ArrayList<OpportunityTotal>();

		try {
			String sql;

			sql = "select " +
			            "o.id, " +
			            "o.market_id, " +
			            "o.SCHEDULED, " +
			            "o.OPPORTUNITY_TYPE_ID, " +
			            "to_char(o.TIME_EST_CLOSING, 'YYYY-MM-DD HH24:MI:SS.FF3 TZHTZM') AS TIME_EST_CLOSING, " +
			            "o.AUTO_SHIFT_PARAMETER, " +
			            "o.IS_DISABLED, " +
			            "o.IS_DISABLED_TRADER, " +
			            "o.closing_level, " +
			            "o.current_level, " +
			            "o.is_settled, " +
			            "m.name, " +
			            "m.MARKET_GROUP_ID, " +
			            "m.IS_SUSPENDED, " +
			            "m.SUSPENDED_MESSAGE , " +
			            "m.FEED_NAME, " +
			            "m.decimal_point, " +
			            "o.quote_params, " +
			            (!isZeroOneHundred ? "(select sum( i.amount * i.rate) " +
								             "from investments i, users u " +
								             "where i.OPPORTUNITY_ID = o.id " +
					                         "and i.TYPE_ID = 2 " +
					                         "and u.id = i.user_id " +
						                     "and u.class_id > 0 " +
						                     "and o.opportunity_type_id <> 2  ) as PUT , " +

 					                        "(select sum( i.amount * i.rate) " +
 					                        "from investments i, users u " +
 					                        "where i.OPPORTUNITY_ID = o.id " +
 					                        "and i.TYPE_ID = 1 " +
 					                        "and u.id = i.user_id " +
 					                        "and u.class_id > 0 " +
 					                        "and o.opportunity_type_id <> 2  ) as CALL "

					                        :

											"(select sum( i.amount * i.rate) " +
 											"from investments i, users u " +
 											"where i.OPPORTUNITY_ID = o.id " +
 											"and i.TYPE_ID in (4," + Investment.INVESTMENT_TYPE_DYNAMICS_BUY +") " +
 											"and u.id = i.user_id " +
 											"and u.class_id > 0 ) as BOUGHT_SUM  , " +

 											"(select sum( i.amount * i.rate) " +
 											"from investments i, users u " +
 											"where i.OPPORTUNITY_ID = o.id " +
 											"and i.TYPE_ID in (5," + Investment.INVESTMENT_TYPE_DYNAMICS_SELL + ") " +
 											"and u.id = i.user_id " +
 											"and u.class_id > 0 ) as SOLD_SUM , " +

 											"(select count(i.id) " +
 											"from investments i, users u " +
 											"where i.OPPORTUNITY_ID = o.id " +
 											"and i.TYPE_ID in (4," + Investment.INVESTMENT_TYPE_DYNAMICS_BUY + ") " +
 											"and u.id = i.user_id " +
 											"and u.class_id > 0 ) as BOUGHT_COUNT  , " +

 											"(select count(i.id) " +
 											"from investments i, users u " +
 											"where i.OPPORTUNITY_ID = o.id " +
 											"and i.TYPE_ID in (5," + Investment.INVESTMENT_TYPE_DYNAMICS_SELL + ") " +
 											"and u.id = i.user_id " +
 											"and u.class_id > 0 ) as SOLD_COUNT " ) +

                 "from   opportunities o , markets  m " +
                 "where o.market_id = m.id " +
                 "and trunc(o.time_est_closing)=trunc(?) " +
                 (!isZeroOneHundred ? 	" and o.opportunity_type_id <> 2 "
                		 				:
                		 				"and o.opportunity_type_id in (4,5," + Opportunity.TYPE_DYNAMICS + ") ");

                if (settled.equalsIgnoreCase("2")) {
                    sql += "and o.is_published = 1 " +
                           "and o.is_settled = 0 ";
                } else {
                    sql += "and o.is_published = 0 " +
                           "and o.is_settled = " + settled + " ";
                }

				if ( marketId > 0 ) {
					sql += "and m.id = " + marketId + " ";
				}

				if ( scheduled > 0 && !isZeroOneHundred) {
					sql += "and o.SCHEDULED = " + scheduled + " ";
				}

				if ( groupId > 0 ) {
					sql += "and m.market_group_id = " + groupId + " ";
				}

				sql += "order by o.time_est_closing ";

			ps = con.prepareStatement(sql);

			Date d = CommonUtil.getDateTimeFormaByTz(from, Utils.getAppData().getUtcOffset());
			ps.setTimestamp(1, CommonUtil.convertToTimeStamp(d));
	//		ps.setString(2, settled);

			rs = ps.executeQuery();

			while(rs.next()) {

				OpportunityTotal o = new OpportunityTotal();
				Market m = new Market();
				o.setId(rs.getLong("id"));
				o.setMarketId(rs.getLong("market_id"));
				o.setScheduled(rs.getInt("scheduled"));
				o.setOpportunityTypeId(rs.getLong("opportunity_type_id"));
				SimpleDateFormat localDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS Z");
				o.setTimeEstClosing(localDateFormat.parse(rs.getString("TIME_EST_CLOSING")));
				o.setDisabled(rs.getBoolean("is_disabled"));
				o.setDisabledTrader(rs.getBoolean("IS_DISABLED_TRADER"));
				o.setClosingLevel(rs.getDouble("closing_level"));
				o.setCurrentLevel(rs.getDouble("current_level"));
				if (isZeroOneHundred) {
					o.setBoughtAmount(rs.getDouble("BOUGHT_SUM"));
					o.setSoldAmount(rs.getDouble("SOLD_SUM"));
					o.setBoughtCount(rs.getInt("BOUGHT_COUNT"));
					o.setSoldCount(rs.getInt("SOLD_COUNT"));
					o.setDecimalPointZeroOneHundred(rs.getLong("decimal_point"));
					o.setQuoteParams(rs.getString("quote_params"));
					o.setBoughtAmountSubSoldAmount(o.getBoughtAmount()- o.getSoldAmount());
					o.setBoughtCountSubSoldCount(o.getBoughtCount() - o.getSoldCount());
				} else {
					o.setCallsAmount(rs.getDouble("CALL"));
					o.setPutsAmount(rs.getDouble("PUT"));
					m.setDecimalPoint(rs.getLong("decimal_point"));
					o.setCallsAmountAddPutsAmount(o.getCallsAmount() + o.getPutsAmount());
					o.setCallsAmountSubPutsAmount(o.getCallsAmount() - o.getPutsAmount());
				}

				o.setIsSettled(rs.getInt("is_settled"));
				m.setId(o.getMarketId());
				m.setName(rs.getString("name"));
				m.setGroupId(rs.getLong("MARKET_GROUP_ID"));
				m.setSuspended(rs.getBoolean("is_suspended"));
				m.setSuspendedMessage(rs.getString("suspended_message"));
				m.setFeedName(rs.getString("feed_name"));
				m.setDecimalPoint(rs.getLong("decimal_point"));
				o.setMarket(m);
				o.setSkinGroupMappings(loadOpportunitySkingGroupMappings(con, o.getId()));

				list.add(o);
			}


		} finally {
			closeStatement(ps);
		}

		return list;
	}


	/**
	 * Get one touch opportunities
	 * @param con db connection
	 * @param from time estimated closing filter
	 * @param settled settled filter
	 * @return
	 * @throws SQLException
	 * @throws ParseException
	 */
	public static ArrayList<OpportunityTotal> getTtOneTouchOpportunities(Connection con, Date from, String settled,
			long marketId, long groupId, long upDown) throws SQLException, ParseException {

		PreparedStatement ps = null;
		ResultSet rs = null;
		ArrayList<OpportunityTotal> list = new ArrayList<OpportunityTotal>();

		try {
			String sql;

			sql = "select " +
			            "o.id, " +
			            "o.market_id, " +
			            "o.SCHEDULED, " +
			            "o.OPPORTUNITY_TYPE_ID, " +
			            "to_char(o.TIME_EST_CLOSING, 'YYYY-MM-DD HH24:MI:SS.FF3 TZHTZM') AS TIME_EST_CLOSING, " +
			            "o.AUTO_SHIFT_PARAMETER, " +
			            "o.IS_DISABLED, " +
			            "o.IS_DISABLED_TRADER, " +
			            "o.closing_level, " +
			            "o.current_level, " +
			            "o.is_published, " +
			            "o.is_settled, " +
			            "m.name, " +
			            "m.MARKET_GROUP_ID, " +
			            "m.IS_SUSPENDED, " +
			            "m.SUSPENDED_MESSAGE , " +
			            "m.FEED_NAME, " +
			            "m.decimal_point, " +
			            "op.decimal_point decimalPointone, " +
			            "op.up_down, " +
			            "odt.name oddstype_name, " +

			            "(select sum( i.amount * i.rate) " +
			            "from investments i, users u " +
			            "where i.OPPORTUNITY_ID = o.id " +
                        "and i.TYPE_ID = 3 " +
                        "and i.IS_CANCELED = 0 " +
                        "and u.id = i.user_id " +
                        "and u.class_id > 0 " +
                        "and o.opportunity_type_id = 2 ) as VOLUME " +

                 "from opportunities o , markets  m, opportunity_one_touch op, opportunity_odds_types odt " +
                 "where o.market_id = m.id and o.id = op.opportunity_id " +
                 "and o.odds_type_id=odt.id " +
                 "and trunc(o.time_est_closing)>=trunc(?) " +
                 "and o.is_settled = ? " +
                 "and o.opportunity_type_id = 2 ";

				if ( marketId > 0 ) {
					sql += "and m.id = " + marketId + " ";
				}

				if ( groupId > 0 ) {
					sql += "and m.market_group_id = " + groupId + " ";
				}

				if ( upDown > -1 ) {
					sql += "and op.up_down = " + upDown + " ";
				}

				sql += "order by o.time_est_closing ";

			ps = con.prepareStatement(sql);

			Date d = CommonUtil.getDateTimeFormaByTz(from, Utils.getAppData().getUtcOffset());
			ps.setTimestamp(1, CommonUtil.convertToTimeStamp(d));
			ps.setString(2, settled);

			rs = ps.executeQuery();

			while(rs.next()) {

				OpportunityTotal o = new OpportunityTotal();
				Market m = new Market();
				o.setId(rs.getLong("id"));
				o.setMarketId(rs.getLong("market_id"));
				o.setScheduled(rs.getInt("scheduled"));
				o.setOpportunityTypeId(rs.getLong("opportunity_type_id"));
				SimpleDateFormat localDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS Z");
				o.setTimeEstClosing(localDateFormat.parse(rs.getString("TIME_EST_CLOSING")));
				o.setDisabled(rs.getBoolean("is_disabled"));
				o.setDisabledTrader(rs.getBoolean("IS_DISABLED_TRADER"));
				o.setClosingLevel(rs.getDouble("closing_level"));
				o.setCurrentLevel(rs.getDouble("current_level"));
				o.setCallsAmountAddPutsAmount(rs.getDouble("VOLUME"));
				o.setIsSettled(rs.getInt("is_settled"));
				o.setIsPublished(rs.getInt("is_published"));
				m.setId(o.getMarketId());
				m.setName(rs.getString("name"));
				m.setGroupId(rs.getLong("MARKET_GROUP_ID"));
				m.setSuspended(rs.getBoolean("is_suspended"));
				m.setSuspendedMessage(rs.getString("suspended_message"));
				m.setFeedName(rs.getString("feed_name"));
				m.setDecimalPoint(rs.getLong("decimal_point"));
				o.setMarket(m);
				o.setOneTouchUpDown(rs.getLong("up_down"));
				o.setDecimalPointOneTouch(rs.getLong("decimalPointone"));
				o.setOpportunityOddsTypeName(rs.getString("oddstype_name"));

				GregorianCalendar gc = new GregorianCalendar();
				GregorianCalendar gcEs = new GregorianCalendar();
				gcEs.setTime(o.getTimeEstClosing());
				if ( gcEs.before(gc) ) {
					o.setCanSettle(true);
				} else {
					o.setCanSettle(false);
				}
				o.setSkinGroupMappings(loadOpportunitySkingGroupMappings(con, o.getId()));
				list.add(o);
			}


		} finally {
			closeStatement(ps);
		}

		return list;
	}

	public static void updateOpportunityMaxExposure(Connection con, long oppId, int maxExposure, SkinGroup sg) throws SQLException {
		PreparedStatement ps = null;

		try {
			String sql;

			sql = 
					"UPDATE " +
								"opportunity_skin_group_map" +
					" SET " +
								"max_exposure = ? " +
					" WHERE " +
			  	  				"opportunity_id = ? " +
					" AND " +
			  	  				"skin_group_id = ? ";

			ps = con.prepareStatement(sql);

			ps.setInt(1, maxExposure);
			ps.setLong(2, oppId);
			ps.setLong(3, sg.getId());

			ps.executeUpdate();

		} finally {
			closeStatement(ps);
		}
	}

	public static Opportunity getOppositeDirectionOpportunity(Connection con, Opportunity opportunity)throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		Opportunity vo = null;

		try {
			String sql;

			sql = " select o.*, " +
					"o.is_suspended AS is_opp_suspended, " +
					getFormatTimesSQL() + " " +
				  " from opportunities o , opportunity_one_touch oot " +
				  " where o.id = oot.opportunity_id " +
				    " and o.opportunity_type_id = ? " +
				  	" and o.market_id = ? " +
				  	" and o.time_first_invest = ? " +
				  	" and o.time_last_invest = ? " +
				  	" and o.odds_type_id = ? " +
				  	" and oot.up_down <> ? ";

			ps = con.prepareStatement(sql);

			ps.setLong(1, Opportunity.TYPE_ONE_TOUCH);
			ps.setLong(2, opportunity.getMarketId());
			ps.setTimestamp(3, CommonUtil.convertToTimeStamp(CommonUtil.getDateTimeFormat(opportunity.getTimeFirstInvest(),
					Utils.getAppData().getUtcOffset())));
			ps.setTimestamp(4, CommonUtil.convertToTimeStamp(CommonUtil.getDateTimeFormat(opportunity.getTimeLastInvest(),
					Utils.getAppData().getUtcOffset())));
			ps.setLong(5, opportunity.getOddsTypeId());
			ps.setLong(6, opportunity.getOneTouchUpDown());

			rs = ps.executeQuery();

			if(rs.next()){
				vo = getVO(rs);
				vo.setSkinGroupMappings(loadOpportunitySkingGroupMappings(con, vo.getId()));
			}

		} finally {
			closeStatement(ps);
		}

		return vo;
	}

	public static void updateBinary0100opp(Connection con, long oppId, String params) throws SQLException {
		PreparedStatement ps = null;

		try {
			String sql;

			sql = "UPDATE " +
					"opportunities " +
				  "SET " +
				  	"quote_params = ? " +
			  	  "WHERE " +
			  	  	"id = ?";

			ps = con.prepareStatement(sql);

			ps.setString(1, params);
			ps.setLong(2, oppId);

			ps.executeUpdate();

		} finally {
			closeStatement(ps);
		}
	}
	
	public static ArrayList<OpportunityOddsPairTemplate> getOpportunityOddsPairTemplate(Connection con, String oddsGroup) throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		ArrayList<OpportunityOddsPairTemplate> list = new ArrayList<OpportunityOddsPairTemplate>();
		boolean isSelected = false;
		try {
			String sql = " SELECT " +
					   " sop.selector_id,sop.refund,sop.return, 'Y' selected " + 
					 " FROM " + 
					   " opportunity_odds_pair sop " + 
					 " WHERE " +
					   " selector_id in (" + oddsGroup + ")" + 
					 " union all" +
					 " SELECT " +
					    " sop.selector_id,sop.refund,sop.return, 'N' selected" +
					 " FROM " + 
					    " opportunity_odds_pair sop " + 
					 " WHERE " + 
					    "selector_id not in (" + oddsGroup + ")" + 
					 " ORDER BY" +
					  " Selector_ID";

			ps = con.prepareStatement(sql);
			rs = ps.executeQuery();
			while (rs.next()) {
				OpportunityOddsPairTemplate vo = new OpportunityOddsPairTemplate();
				vo.setSelectorID(rs.getLong("selector_id"));
				vo.setReturnSelector(rs.getLong("return"));
				vo.setRefundSelector(rs.getLong("refund"));
				if (rs.getString("selected").contains("Y")) {
					isSelected = true;
				} else {
					isSelected = false;
				}
				vo.setIsSelected(isSelected);
				list.add(vo);
			}
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		return list;
	}
	  
	public static ArrayList<OpportunityOddsPair> getOpportunityOddsPair(Connection con) throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		ArrayList<OpportunityOddsPair> list = new ArrayList<OpportunityOddsPair>();
		try {
			String sql = " SELECT " + 
		                    "sop.selector_id,sop.refund,sop.return " + 
					     " FROM " + 
		                    " opportunity_odds_pair sop " + 
					     " ORDER BY" + 
					      " sop.Selector_ID, sop.Return";

			ps = con.prepareStatement(sql);
			rs = ps.executeQuery();
			while (rs.next()) {
				OpportunityOddsPair vo = new OpportunityOddsPair();
				vo.setSelectorID(rs.getLong("selector_id"));
				vo.setReturnSelector(rs.getLong("return"));
				vo.setRefundSelector(rs.getLong("refund"));
				list.add(vo);
			}
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		return list;
	}

	public static boolean isExistOpportunityOddsPair(Connection con, OpportunityOddsPair op) throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			String sql = " select * from Opportunity_Odds_pair where refund="
					+ op.getRefundSelector() + " and return="
					+ op.getReturnSelector();

			ps = con.prepareStatement(sql);
			rs = ps.executeQuery();
			if (rs.next()) {
				return true;
			}
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		return false;
	}

	public static boolean isExistOddsGoupsInOddsType(Connection con, String oddsGroup, long oddsTypeId) throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			String sql = "SELECT  'X' " + 
		                 "FROM " +      
					       "opportunity_odds_types oot " + 
		                 "WHERE " 
					       + "oot.id= "+ oddsTypeId +  
					       " and oot.over_odds_win*100 ||'vs'|| (100-((oot.over_odds_lose)*100)) in "+ 
		                    " (select  oop.return ||'vs'|| oop.refund from opportunity_odds_pair oop  where oop.selector_id in (" + oddsGroup + ")) ";

			ps = con.prepareStatement(sql);
			rs = ps.executeQuery();
			if (rs.next()) {
				return true;
			}
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		return false;
	}

	public static void insertOpportunityOddsPair(Connection con, OpportunityOddsPair vo) throws SQLException {
		PreparedStatement ps = null;
		try {
			String sql = " INSERT " +
					         "into Opportunity_Odds_pair " +
					         " (selector_id,refund,return) " + 
					       " Values " + 
					         " (SEQ_opportunity_odds_pair.NEXTVAL,?,?)";

			ps = con.prepareStatement(sql);
			ps.setLong(1, vo.getRefundSelector());
			ps.setLong(2, vo.getReturnSelector());
			ps.executeUpdate();
			vo.setSelectorID((getSeqCurValue(con, "SEQ_opportunity_odds_pair")));
		} finally {
			closeStatement(ps);
		}
	}

	public static ArrayList<OpportunityOddsGroup> getOpportunityOddsGroup(Connection con) throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		ArrayList<OpportunityOddsGroup> list = new ArrayList<OpportunityOddsGroup>();
		try {
			String sql = " SELECT " + 
		                   " oog.odds_group_id,oog.odds_group, get_opp_pair_group_pct(oog.odds_group) group_value, get_with_default_value(oog.odds_group) as odds_default, " +
		                   " 'Default: '|| oop.return || '%' || 'vs'|| oop.refund || '%' as default_pair " +
					     " FROM " + 
		                   " opportunity_odds_group oog, opportunity_odds_pair oop " +
		                   " WHERE oog.odds_group_default = oop.selector_id " +
					     "ORDER BY" +
					        " oog.odds_group_id";
			ps = con.prepareStatement(sql);
			rs = ps.executeQuery();
			while (rs.next()) {
				OpportunityOddsGroup vo = new OpportunityOddsGroup();
				vo.setOddsGroupID(rs.getLong("odds_group_id"));
				vo.setOddsGroup(rs.getString("odds_group"));
				vo.setOddsGroupValue(rs.getString("group_value"));
				vo.setOddsGroupDefault(rs.getString("odds_default"));
				vo.setDefaultPair(rs.getString("default_pair"));
				list.add(vo);
			}
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		return list;
	}

	public static String getOpportunityPairsGroupPct(Connection con, String oddsGroup) throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		String retrunValue = "";
		try {
			String sql = " SELECT " +
					       " get_opp_pair_group_pct(oog.odds_group) group_value " +
					     " FROM " + " " +
					       " opportunity_odds_group oog " + 
					     " WHERE " +
					       " oog.odds_group = ? ";

			ps = con.prepareStatement(sql);
			ps.setString(1, oddsGroup);
			rs = ps.executeQuery();
			while (rs.next()) {
				retrunValue = rs.getString("group_value");
			}
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		return retrunValue;
	}

	public static void insertOpportunityOddsGroup(Connection con, OpportunityOddsGroup vo, String oddsGroup, long writerId) throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			String[] oddsGroupDef = oddsGroup.split(" ");
			String sql = " insert into opportunity_odds_group (odds_group_id, odds_group, odds_group_default, writer_id) values(SEQ_opportunity_odds_group.nextval,?,?,?)";
			ps = con.prepareStatement(sql);
			ps.setString(1, oddsGroup);
			ps.setString(2, oddsGroupDef[0]);
			ps.setLong(3, writerId);
			ps.executeUpdate();
			vo.setOddsGroupID(getSeqCurValue(con, "SEQ_opportunity_odds_group"));
		} finally {
			closeStatement(ps);
			closeResultSet(rs);
		}
	}

	public static void updatetOpportunityOddsGroup(Connection con, OpportunityOddsGroup vo, String oddsGroup, long writerId, long defValue) throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		boolean isDefValInList = false;
		try {
			String[] oddsGroupArr = oddsGroup.split(" ");
			if (defValue == 0) {
				defValue = Long.valueOf(oddsGroupArr[0]);
			} else {
				for (String v : oddsGroupArr) {
					if (v.equals(String.valueOf(defValue))) {
						isDefValInList = true;
					}
				}
			}
			
			if (!isDefValInList && defValue != 0) {
				defValue = Long.valueOf(oddsGroupArr[0]);
			}
			
			String sql = " update opportunity_odds_group set odds_group=?, writer_id=?, odds_group_default=? where odds_group_id=? ";
			ps = con.prepareStatement(sql);
			ps.setString(1, oddsGroup);
			ps.setLong(2, writerId);
			ps.setLong(3, defValue);
			ps.setLong(4, vo.getOddsGroupID());
			ps.executeUpdate();
		} finally {
			closeStatement(ps);
			closeResultSet(rs);
		}
	}
	
	public static void updatetOpportunityOddsGroupDefaultValue(Connection con, long id, long defValue, long writerId) throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			String sql = " update opportunity_odds_group set odds_group_default=?, writer_id=? where odds_group_id=? ";
			ps = con.prepareStatement(sql);
			ps.setLong(1, defValue);
			ps.setLong(2, writerId);
			ps.setLong(3, id);
			ps.executeUpdate();
		} finally {
			closeStatement(ps);
			closeResultSet(rs);
		}
	}
	
	public static long getDefValueByOddsGroup(Connection con, String oddsGroup) throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		long oddsDefVal = 0;
		try {
			String sql = " SELECT odds_group_default " +
						 " FROM opportunity_odds_group " +
						 " WHERE odds_group = ? ";
			ps = con.prepareStatement(sql);
			ps.setString(1, oddsGroup);
			rs = ps.executeQuery();
			if(rs.next()){
				oddsDefVal = rs.getLong("odds_group_default");
			}
		} finally {
			closeStatement(ps);
			closeResultSet(rs);
		}
		return oddsDefVal;
	}
	
	public static String getOpportunityPairsGroup(Connection con, Long oddsTypeId) throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		String retrunOddsGroup = "null";
		try {
			String sql = " Select oog.odds_group as odds_group " + 
						 " From " + 
						 " opportunity_odds_group oog " +
						 " Where " + 
						 " replace( get_opp_pair_group_pct(oog.odds_group),'%','' ) like " +
						 	" (	Select to_char( '%'||oot.over_odds_win*100 ||'vs'||(100-((oot.over_odds_lose)*100) ||'%')) " +
						 		" From " + 
								" opportunity_odds_types oot " +
								" Where oot.id=" + oddsTypeId +") " +
						 " order by oog.odds_group_id ";

			ps = con.prepareStatement(sql);
			rs = ps.executeQuery();
			while (rs.next()) {
				retrunOddsGroup = rs.getString("odds_group");
			}
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		return retrunOddsGroup;
	}

	public static double getNightShiftLimitByOppId(Connection con, long oppId) throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		double limit = Constants.TRADERTOOL_BTRADER_SPREAD_LIMIT;
		try {
			String sql = " SELECT " +
						 "	  mg.night_shift_limit " +
						 " FROM " +
						 "	  opportunities o, " +
						 "	  market_groups mg, " +
						 "	  markets m " +
						 " WHERE " +
						 " 	  o.market_id = m.id AND " +
						 " 	  mg.id = m.market_group_id AND " +
						 "	  o.id = ?";
			ps = con.prepareStatement(sql);
			ps.setLong(1, oppId);
			rs = ps.executeQuery();
			if (rs.next()) {
				limit = rs.getDouble("night_shift_limit");
			}
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		return limit;
	}

    /**
     * It does as is, gets if for given opportunity value in DB is set to 0, 1 -> ergo suspended or not
     * 
     * @param conn	db connection
     * @param id	opportunity ID
     * @return 		0=no, 1=yes, 2= something went wrong....
     * @throws SQLException
     */
	public static int getOppIsSuspended(Connection conn, long id) throws SQLException {

		PreparedStatement pstmt = null;
		ResultSet rs = null;
		int isSuspended = 2;
		try {
			String sql = " SELECT is_suspended FROM opportunities WHERE id = ?";
			pstmt = conn.prepareStatement(sql);
			pstmt.setLong(1, id);
			rs = pstmt.executeQuery();
			rs.next();
			isSuspended = rs.getInt("is_suspended");
		} finally {
			closeResultSet(rs);
			closeStatement(pstmt);
		}
		return isSuspended;
	}
    
    /**
     * Sets in the DB for given opportunity to 0, 1 -> ergo suspended or not status
     * 
     * @param conn		db connection
     * @param oppID		opportunity ID
     * @param isSuspended	1 or 0 if suspended to set
     * @throws SQLException
     */
    public static void setOpportunitySuspended(Connection conn, long oppID, int isSuspended) throws SQLException {
		PreparedStatement ps = null;
		try {
		    String sql = 	
			"UPDATE " + 
			    "opportunities " +
			"SET " +
			    "is_suspended = ?" +
			"WHERE " +
			    "id = ?";
		    ps = conn.prepareStatement(sql);
		    ps.setInt(1, isSuspended);
		    ps.setLong(2, oppID);
		    ps.executeUpdate();
		} finally {
		    closeStatement(ps);
		}
    }
}