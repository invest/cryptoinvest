package il.co.etrader.backend.dao_managers;

import il.co.etrader.bl_vos.MarketingAffilate;
import il.co.etrader.bl_vos.MarketingSubAffiliate;
import il.co.etrader.dao_managers.MarketingAffiliatesDAOBase;
import il.co.etrader.util.CommonUtil;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.faces.model.SelectItem;

import com.anyoption.common.managers.MarketingAffiliatesManagerBase;

public class MarketingAffilatesDAO extends MarketingAffiliatesDAOBase {

	/**
	 * Get All Marketing Affiliates by SI
	 * @param con
	 * @return
	 * @throws SQLException
	 */
	public static ArrayList<SelectItem> getAllMarketingAffiliatesSI(Connection con) throws SQLException{

	  PreparedStatement ps = null;
	  ResultSet rs = null;
	  ArrayList<SelectItem> list = new ArrayList<SelectItem>();
	  try {
			String sql = "SELECT " +
							"ma.name," +
							"ma.id " +
						 "FROM " +
						 	"marketing_affilates ma " +
						 "ORDER BY " +
						 	"name";

			ps = con.prepareStatement(sql);
			rs = ps.executeQuery();
			while (rs.next()) {
				list.add(new SelectItem(rs.getLong("id"),rs.getString("name")));
			}
	  } finally {
		  closeResultSet(rs);
		  closeStatement(ps);
	  }
	  return list;
	}


	/**
	 * Insert a new affilate
	 * @param con  db connection
	 * @param vo MarketingLocation instance
	 * @throws SQLException
	 */
	public static void insert(Connection con,MarketingAffilate vo) throws SQLException {

		  PreparedStatement ps = null;
		  ResultSet rs = null;

		  try {
				String sql = "insert into marketing_affilates(id,name,writer_id,key,priority_id,time_created,active, site_link, aff_manager_writer_id) " +
						     "values(seq_m_affilates.nextval,?,?,?,?,sysdate,?,?, ?)";

				ps = con.prepareStatement(sql);

				ps.setString(1, vo.getName());
				ps.setLong(2, vo.getWriterId());
				ps.setString(3, vo.getKey());
                ps.setString(4, vo.getPriorityId());
                ps.setBoolean(5, vo.isActive());
                ps.setString(6, vo.getSiteLink());
                ps.setLong(7, vo.getAffiliateManagerId());
				ps.executeUpdate();
				vo.setId(getSeqCurValue(con,"seq_m_affilates"));
		  } finally {
				closeStatement(ps);
				closeResultSet(rs);
			}
	  }

	/**
	 * Insert a new affilate with special code
	 * @param con  db connection
	 * @param vo MarketingLocation instance
	 * @throws SQLException
	 */
	public static void insertMarketingCode(Connection con,MarketingAffilate vo) throws SQLException {

		  PreparedStatement ps = null;
		  ResultSet rs = null;

		  try {
				String sql = "insert into marketing_affilates_code(id,affiliate_key,special_code,time_created) " +
						     "values(seq_marketing_affilates_code.nextval,?,?,sysdate)";
				ps = con.prepareStatement(sql);
				ps.setString(1, vo.getKey());
                ps.setString(2, vo.getSpecialCode());
				ps.executeUpdate();
				vo.setId(getSeqCurValue(con,"seq_m_affilates"));

		  } finally {
				closeStatement(ps);
				closeResultSet(rs);
			}
	  }

	/**
	 * Insert a new sub affilate
	 * @param con  db connection
	 * @param vo MarketingLocation instance
	 * @throws SQLException
	 */
	public static void insertSubAffiliate(Connection con,MarketingSubAffiliate vo) throws SQLException {

		  PreparedStatement ps = null;
		  ResultSet rs = null;

		  try {
				String sql = "insert into marketing_sub_affiliates(id,name,writer_id,key,affiliate_id) " +
						     "values(seq_m_sub_affiliate.nextval,?,?,?,?)";

				ps = con.prepareStatement(sql);

				ps.setString(1, vo.getName());
				ps.setLong(2, vo.getWriterId());
				ps.setString(3, vo.getKey());
				ps.setLong(4, vo.getAffiliateId());
				ps.executeUpdate();
				vo.setId(getSeqCurValue(con,"seq_m_sub_affiliate"));
		  } finally {
				closeStatement(ps);
				closeResultSet(rs);
			}
	  }

	  /**
	   * Update marketing affilate
	   * @param con db connection
	   * @param vo  MarketingAffilate instance
	   * @throws SQLException
	   */
	  public static void update(Connection con,MarketingAffilate vo) throws SQLException {

		  PreparedStatement ps = null;
		  ResultSet rs = null;

		  try {
			  String sql = "update " +
			  					"marketing_affilates " +
			  				"set " +
			  					"name=?," +
			  					"writer_id=?, " +
			  					"key=?, " +
			  					"priority_id=?, " +
			  					"active=?, " +
			  					"site_link=?, " +
			  					" aff_manager_writer_id = ? " +
			  			   "where " +
			  			   		"id=?";

				ps = con.prepareStatement(sql);
				ps.setString(1, vo.getName());
				ps.setLong(2, vo.getWriterId());
				ps.setString(3, vo.getKey());
                ps.setString(4, vo.getPriorityId());
                ps.setBoolean(5, vo.isActive());
                ps.setString(6, vo.getSiteLink());
                ps.setLong(7, vo.getAffiliateManagerId());
				ps.setLong(8, vo.getId());
				ps.executeUpdate();
		  } finally	{
				closeStatement(ps);
				closeResultSet(rs);
			}
	  }

	  /**
	   * Update marketing affilate code
	   * @param con db connection
	   * @param vo  MarketingAffilate instance
	   * @throws SQLException
	   */
	  /*
	  public static void updateMarketingCode(Connection con,MarketingAffilate vo) throws SQLException {

		  PreparedStatement ps = null;
		  ResultSet rs = null;

		  try {
			  String sql = "update " +
			  					"marketing_affilates_code " +
			  				"set " +
			  					"affiliate_key=?" +
			  			   "where " +
			  			   		"( SELECT " +
			  								"ma.id " +
			  							  "FROM " +
			  							 	 "marketing_affilates ma " +
			  							 	 	"LEFT JOIN marketing_affilates_code mac ON ma.key = mac.affiliate_key, " +
			  							 	 "writers w " +
			  							  "WHERE " +
			  							 	 "ma.writer_id = w.id " +
			                   "AND " +
			                   "ma.id =? ) is not null";

				ps = con.prepareStatement(sql);
				ps.setString(1, vo.getKey());
                ps.setString(2, vo.getSpecialCode());
				ps.setLong(3, vo.getId());
				ps.executeUpdate();
		  } finally	{
				closeStatement(ps);
				closeResultSet(rs);
			}
	  }
	  */
	  
	  public static void updateMarketingCode(Connection con, MarketingAffilate vo) throws SQLException {

		  PreparedStatement ps = null;
		  ResultSet rs = null;

		  try {
			  String sql = " UPDATE " +
			  					 " marketing_affilates_code " +
			  			   " SET " +
			  					" special_code = ? " +
			  			   " WHERE " +
			  			   		" affiliate_key = ?";

				ps = con.prepareStatement(sql);
				ps.setString(1, vo.getSpecialCode());
				ps.setString(2, vo.getKey());
				ps.executeUpdate();
		  } finally	{
				closeStatement(ps);
				closeResultSet(rs);
			}
	  }

	  /**
	   * Update sub marketing affiliate
	   * @param con db connection
	   * @param vo  Sub Marketing Affiliate instance
	   * @throws SQLException
	   */
	  public static void updateMarketingSubAffiliate(Connection con,MarketingSubAffiliate vo) throws SQLException {

		  PreparedStatement ps = null;
		  ResultSet rs = null;

		  try {
			  String sql = "UPDATE " +
			  					"marketing_sub_affiliates " +
			  				"SET " +
			  					"name=?," +
			  					"writer_id=?, " +
			  					"key=?, " +
			  					"affiliate_id=? " +
			  			   "WHERE " +
			  			   		"id=?";

				ps = con.prepareStatement(sql);
				ps.setString(1, vo.getName());
				ps.setLong(2, vo.getWriterId());
				ps.setString(3, vo.getKey());
				ps.setLong(4, vo.getAffiliateId());
				ps.setLong(5, vo.getId());
				ps.executeUpdate();
		  } finally	{
				closeStatement(ps);
				closeResultSet(rs);
			}
	  }


	  /**
	   * Get all affilates
	   * @param con  db connection
	   * @param location  filter
	   * @return
	   *	ArrayList of MarketingAffilate
	   * @throws SQLException
	   */
	  public static ArrayList<MarketingAffilate> getAll(Connection con, String affName, String affKey, long affManagerId) throws SQLException {

		  PreparedStatement ps = null;
		  ResultSet rs = null;
		  ArrayList<MarketingAffilate> list = new ArrayList<MarketingAffilate>();
		  int index = 1;
		  try {
				String sql = " SELECT " +
								" ma.*, " +
								" w.user_name w_name, " +
								" mac.special_code " +
							 " FROM " +
							 	" marketing_affilates ma " +
							 	" 	LEFT JOIN marketing_affilates_code mac ON ma.key = mac.affiliate_key, " +
							 	" writers w " +
							 " WHERE " +
							 	" ma.writer_id = w.id ";

				if (!CommonUtil.isParameterEmptyOrNull(affName)) {
					sql += "and upper(ma.name) like '%" + affName.toUpperCase() + "%' ";
				}
	
				if (!CommonUtil.isParameterEmptyOrNull(affKey)) {
					sql += "and ma.key like '%" + affKey + "%' ";
				}
				if (affManagerId != MarketingAffiliatesManagerBase.AFFILIATE_MANAGER_ALL) {
					sql += " and ma.aff_manager_writer_id = ? ";
				}
				sql += "order by ma.name";

				ps = con.prepareStatement(sql);
				if (affManagerId != MarketingAffiliatesManagerBase.AFFILIATE_MANAGER_ALL) {
					ps.setLong(index++, affManagerId);
				}
				rs = ps.executeQuery();

				while (rs.next()) {
					MarketingAffilate vo = getVO(rs);
					vo.setSpecialCode(rs.getString("special_code"));
					vo.setWriterName(rs.getString("w_name"));
					vo.setPriorityId(rs.getString("priority_id"));
					vo.setActive(rs.getBoolean("active"));
					vo.setSiteLink(rs.getString("site_link"));
					vo.setPixelsSi(MarketingPixelsDAO.getPixelsByAffiliateSI(con, vo.getId()));
					vo.setAffiliateManagerId(rs.getLong("aff_manager_writer_id"));
					list.add(vo);
				}

			} finally {
					closeResultSet(rs);
					closeStatement(ps);
			}

			return list;
	  }

	  /**
	   * Get all sub affilates
	   * @param con  db connection
	   * @param location  filter
	   * @return
	   *	ArrayList of MarketingSubAffilate
	   * @throws SQLException
	   */
	  public static ArrayList<MarketingSubAffiliate> getAllSubAffiliates(Connection con, String subAffName, String affName) throws SQLException {

		  PreparedStatement ps = null;
		  ResultSet rs = null;
		  ArrayList<MarketingSubAffiliate> list = new ArrayList<MarketingSubAffiliate>();

		  try {
				String sql = "SELECT " +
								"msa.*, " +
								"ma.name aff_name, " +
								"w.user_name w_name " +
							 "FROM " +
							 	"marketing_sub_affiliates msa , " +
							 	"marketing_affilates ma, " +
							 	"writers w " +
							 "WHERE " +
							 	"msa.writer_id = w.id " +
							 	"AND ma.id=msa.affiliate_id ";

				if ( !CommonUtil.isParameterEmptyOrNull(subAffName) ) {
					sql += "and upper(msa.name) like '%" + subAffName.toUpperCase() + "%' ";
				}

				if ( !CommonUtil.isParameterEmptyOrNull(affName) ) {
					sql += "and upper(ma.name) like '%" + affName.toUpperCase() + "%' ";
				}

				sql += "order by msa.name";

				ps = con.prepareStatement(sql);
				rs = ps.executeQuery();

				while (rs.next()) {
					MarketingSubAffiliate vo = new MarketingSubAffiliate();
					vo.setId(rs.getLong("id"));
					vo.setName(rs.getString("name"));
					vo.setKey(rs.getString("key"));
					vo.setAffiliateId(rs.getLong("affiliate_id"));
					vo.setWriterId(rs.getLong("writer_id"));
					vo.setWriterName(rs.getString("w_name"));
					vo.setAffiliateName(rs.getString("aff_name"));
					vo.setPixelsSi(MarketingPixelsDAO.getPixelsBySubAffiliateSI(con, vo.getId()));
					list.add(vo);
				}

			} finally {
					closeResultSet(rs);
					closeStatement(ps);
			}

			return list;
	  }



	  /**
	   * Check if affName in use
	   * @param con   db connection
	   * @param name  affilate name
	   * @return true if name in use
	   * @throws SQLException
	   */
	  public static boolean isSpecialCodeInUse(Connection con, String affSpecialCode, long id) throws SQLException {

		  ResultSet rs = null;
		  PreparedStatement ps = null;

		  try {

			  String sql = "select * " +
			  			   "from marketing_affilates ma ," +
						 		"marketing_affilates_code mac " +
			  			   "where mac.affiliate_key = ma.key AND " +
			  			   		"mac.special_code like ? ";

			  if ( id > 0 ) {  // for update
				  sql += "and ma.id <> " + id;
			  }

			  ps = con.prepareStatement(sql);
			  ps.setString(1, affSpecialCode);

			  rs = ps.executeQuery();

			  if ( rs.next() ) {
				  return true;
			  }
		  } finally {
			  closeResultSet(rs);
			  closeStatement(ps);
		  }

		  return false;

	  }

	  /**
	   * Check if subAffName in use
	   * @param con   db connection
	   * @param name  subAffilate name
	   * @return true if name in use
	   * @throws SQLException
	   */
	  public static boolean isSubAffiliateNameInUse(Connection con, String subAffName, long id) throws SQLException {

		  ResultSet rs = null;
		  PreparedStatement ps = null;

		  try {

			  String sql = "select * " +
			  			   "from marketing_sub_affiliates " +
			  			   "where name like ? ";

			  if ( id > 0 ) {  // for update
				  sql += "and id <> " + id;
			  }

			  ps = con.prepareStatement(sql);
			  ps.setString(1, subAffName);

			  rs = ps.executeQuery();

			  if ( rs.next() ) {
				  return true;
			  }
		  } finally {
			  closeResultSet(rs);
			  closeStatement(ps);
		  }

		  return false;
	  }
	  
	  public static boolean isKeyInUse(Connection con, String key, long id) throws SQLException {
		  
		  ResultSet rs = null;
		  PreparedStatement ps = null;
		  
		  try {
			  String sql = "select * " +
		  			   "from marketing_affilates " +
		  			   "where key like ?";
			  
			  if ( id > 0 ) {  // for update
				  sql += "and id <> " + id;
			  }
			  
			  ps = con.prepareStatement(sql);
			  ps.setString(1, key);
			  
			  rs = ps.executeQuery();
			  
			  if ( rs.next() ) {
				  return true;
			  }			
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		  
		return false;
	  }



		/**
		 * Get All Marketing Affiliates keys and name to SI
		 * @param con
		 * @return
		 * @throws SQLException
		 */
		public static ArrayList<SelectItem> getAffiliatesNamesAndKeysSI(Connection con) throws SQLException{

		  PreparedStatement ps = null;
		  ResultSet rs = null;
		  ArrayList<SelectItem> list = new ArrayList<SelectItem>();
		  try {
				String sql = "SELECT " +
								"ma.key, " +
								"ip.name " +
							 "FROM " +
							 	"marketing_affilates ma, " +
							 	"issue_priorities ip " +
							 "WHERE " +
							 	"ip.id = ma.priority_id " +
							 "GROUP BY " +
							 	"ma.key," +
							 	"ip.name " +
							 "ORDER BY " +
							 	"key";

				ps = con.prepareStatement(sql);
				rs = ps.executeQuery();
				while (rs.next()) {
					list.add(new SelectItem(rs.getString("key"),rs.getString("key") + " " + rs.getString("name") ));
				}
		  } finally {
			  closeResultSet(rs);
			  closeStatement(ps);
		  }
		  return list;
		}

	/**
	 * Get marketing affiliates names and keys
	 * @param con
	 * @return ArrayList<SelectItem>
	 * @throws SQLException
	 */
	public static ArrayList<SelectItem> getMarketingAffNameKey(Connection con) throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		ArrayList<SelectItem> list = new ArrayList<SelectItem>();
		try {
			String sql = "SELECT " +
							" ma.key, " +
							" ma.name " +
						" FROM " +
							" marketing_affilates ma ";
			ps = con.prepareStatement(sql);
			rs = ps.executeQuery();
			while (rs.next()) {
				list.add(new SelectItem(rs.getLong("key"), rs.getString("key") + " " + rs.getString("name")));
			}
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		return list;
	}
}

