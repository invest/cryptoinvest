package il.co.etrader.backend.dao_managers;

import il.co.etrader.backend.util.Constants;
import il.co.etrader.backend.util.Utils;
import il.co.etrader.util.CommonUtil;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.LimitationDeposits;
import com.anyoption.common.daos.LimitationDepositsDAOBase;
import com.anyoption.common.util.ConstantsBase;

public class LimitationDepositsDAO extends LimitationDepositsDAOBase {

	private static final Logger logger = Logger.getLogger(LimitationDepositsDAO.class);

    public static long insert(Connection conn, long skin, long currency, long country, long paymentRecipient, long campaign, long affiliate, long minDeposit,
            long minFirstDeposit, long defaultDeposit, String comments, Long groupId, long remarketing) throws SQLException {
        PreparedStatement pstmt = null;
        ResultSet rs = null;

        try {
            String sql =
                    "INSERT INTO LIMITATION_DEPOSITS " +
                    "( " +
                        "minimum_deposit, " +
                        "comments, " +
                        "currency_id, " +
                        "writer_id, " +
                        "skin_id, " +
                        "minimum_first_deposit, " +
                        "country_id, " +
                        "campaign_id, " +
                        "id, " +
                        "affiliate_id, " +
                        "time_updated, " +
                        "payment_recipient_id, " +
                        "group_id, " +
                        "is_remarketing, " +
                        "default_deposit " +
                    ") " +
                    "VALUES " +
                    "( " +
                        "?, " +
                        "?, " +
                        "?, " +
                        "?, " +
                        "?, " +
                        "?, " +
                        "?, " +
                        "?, " +
                        "SEQ_LIMITATION_DEPOSITS.nextval, " +
                        "?, " +
                        "sysdate, " +
                        "?, " +
                        (null != groupId ? groupId : "SEQ_LIMITATION_DEPOSITS.currval") + ", " +
                        "?, " +
                        "?" +
                    ")";

            pstmt = conn.prepareStatement(sql);

            pstmt.setLong(1, minDeposit * 100);
            pstmt.setString(2, comments);

            if (currency != Constants.ALL_FILTER_ID) {
                pstmt.setLong(3, currency);
            } else {
                pstmt.setNull(3, Types.NUMERIC);
            }
            pstmt.setLong(4, Utils.getWriter().getWriter().getId());

            if (skin != Constants.ALL_FILTER_ID) {
                pstmt.setLong(5, skin);
            } else {
                pstmt.setNull(5, Types.NUMERIC);
            }
            pstmt.setLong(6, minFirstDeposit * 100);

            if (country != Constants.ALL_FILTER_ID) {
                pstmt.setLong(7, country);
            } else {
                pstmt.setNull(7, Types.NUMERIC);
            }

            if (campaign != Constants.ALL_FILTER_ID) {
                pstmt.setLong(8, campaign);
            } else {
                pstmt.setNull(8, Types.NUMERIC);
            }

            if (affiliate != Constants.ALL_FILTER_ID) {
                pstmt.setLong(9, affiliate);
            } else {
                pstmt.setNull(9, Types.NUMERIC);
            }
            if (paymentRecipient != Constants.ALL_FILTER_ID) {
                pstmt.setLong(10, paymentRecipient);
            } else {
                pstmt.setNull(10, Types.NUMERIC);
            }

            if (remarketing == Constants.USERS_MARKET_DIS_ACTIVE) {
                pstmt.setLong(11, remarketing);
            } else {
                pstmt.setNull(11, Types.NUMERIC);
            }
            if (defaultDeposit != 0) {
            	pstmt.setLong(12, defaultDeposit * 100);
            } else {
            	pstmt.setNull(12, Types.NUMERIC);
            }

            pstmt.executeUpdate();
        } finally {
            closeResultSet(rs);
            closeStatement(pstmt);
        }
        return (null != groupId ? groupId : getSeqCurValue(conn, "SEQ_LIMITATION_DEPOSITS"));
    }

    public static ArrayList<LimitationDeposits> search(Connection con, String skins, String currencies, String countries, String paymentRecipients,
            String campaigns, String affiliates, long isActive, boolean isValidate, List<Long> types, long remarketing) throws SQLException {
        PreparedStatement ps = null;
        ResultSet rs = null;
        ArrayList<LimitationDeposits> list = new ArrayList<LimitationDeposits>();
        try {
            String sql = "SELECT " +
                          "ld.*, " +
                          "s.display_name as skin_name, " +
                          "ma.name affilate_name, " +
                          "c.display_name currency_name, " +
                          "co.display_name as country_name, " +
                          "mpr.agent_name as payment_recipients_name, " +
                          "mc.name as campaign_name, " +
                          "w.user_name as writer_name " +
                        "FROM " +
                          "limitation_deposits ld " +
                              "LEFT JOIN skins s ON s.id = ld.skin_id " +
                              "LEFT JOIN marketing_affilates ma ON ld.affiliate_id = ma.id " +
                              "LEFT JOIN currencies c ON c.id = ld.currency_id " +
                              "LEFT JOIN countries co ON co.id = ld.country_id " +
                              "LEFT JOIN marketing_payment_recipients mpr ON mpr.id = ld.payment_recipient_id " +
                              "LEFT JOIN marketing_campaigns mc ON mc.id = ld.campaign_id, " +
                          "writers w " +
                        "WHERE " +
                            "w.id = ld.writer_id ";
                        if (null != paymentRecipients) {
                    sql += "AND ld.payment_recipient_id IN (" + paymentRecipients + ") ";
                        } else if (isValidate) {
                    sql += "AND ld.payment_recipient_id IS NULL ";
                        } else if (types.contains(LimitationDeposits.PAYMENT_RECIPIENT)) {
                    sql += "AND ld.payment_recipient_id IS NOT NULL ";
                        }
                        if (null != affiliates) {
                    sql += "AND ld.affiliate_id IN (" + affiliates + ") ";
                        } else if (isValidate) {
                    sql += "AND ld.affiliate_id IS NULL ";
                        } else if (types.contains(LimitationDeposits.AFFILATE)) {
                    sql += "AND ld.affiliate_id IS NOT NULL ";
                        }
                        if (null != campaigns) {
                    sql += "AND ld.campaign_id IN (" + campaigns + ") ";
                        } else if (isValidate) {
                    sql += "AND ld.campaign_id IS NULL ";
                        } else if (types.contains(LimitationDeposits.CAMPAIGN)) {
                    sql += "AND ld.campaign_id IS NOT NULL ";
                        }
                        if (null != countries) {
                    sql += "AND ld.country_id IN (" + countries + ") ";
                        } else if (isValidate) {
                    sql += "AND ld.country_id IS NULL ";
                        } else if (types.contains(LimitationDeposits.COUNTRY)) {
                    sql += "AND ld.country_id IS NOT NULL ";
                        }
                        if (null != currencies) {
                    sql += "AND  ld.currency_id IN (" + currencies + ") ";
                        } else if (isValidate) {
                    sql += "AND ld.currency_id IS NULL ";
                        } else if (types.contains(LimitationDeposits.CURRENCY)) {
                    sql += "AND ld.currency_id IS NOT NULL ";
                        }
                        if (null != skins) {
                    sql += "AND ld.skin_id IN (" + skins + ") ";
                        } else if (isValidate) {
                    sql += "AND ld.skin_id IS NULL ";
                        } else if (types.contains(LimitationDeposits.SKIN)) {
                    sql += "AND ld.skin_id IS NOT NULL ";
                        }

                        if (Constants.ALL_FILTER_ID1 != isActive) {
                    sql += "AND ld.is_active = ? ";
                        }

                        if (remarketing == Constants.USERS_MARKET_DIS_ACTIVE) {
                    sql += "AND ld.is_remarketing = " + remarketing + " ";
                        } else if (isValidate) {
                    sql += "AND ld.is_remarketing IS NULL ";
                        } else if (types.contains(LimitationDeposits.REMARKETING)) {
                    sql += "AND ld.skin_id IS NOT NULL ";
                        }

                    sql += "ORDER BY " +
                                "ld.time_updated desc ";
            int index = 1;
            ps = con.prepareStatement(sql);

            if (Constants.ALL_FILTER_ID1 != isActive) {
                ps.setLong(index++, isActive);
            }

            rs = ps.executeQuery();
            while (rs.next()) {
                LimitationDeposits ld = getVO(rs);
                ld = getVOExtended(rs, ld);
                list.add(ld);
            }
        } finally {
            closeResultSet(rs);
            closeStatement(ps);
        }
        return list;
    }

    private static LimitationDeposits getVOExtended(ResultSet rs, LimitationDeposits ld) throws SQLException {
        ld.setAffiliateTxt(rs.getString("affilate_name"));
        ld.setCampaignTxt(rs.getString("campaign_name"));
        ld.setCountryTxt(CommonUtil.getMessage(rs.getString("country_name"), null, new Locale("en")));
        ld.setCurrencyTxt(CommonUtil.getMessage(rs.getString("currency_name"), null, new Locale("en")));
        ld.setPaymentRecipientTxt(rs.getString("payment_recipients_name"));
        ld.setSkinTxt(CommonUtil.getMessage(rs.getString("skin_name"), null, new Locale("en")));
        ld.setWriterTxt(rs.getString("writer_name"));
        return ld;
    }

    private static LimitationDeposits getVO(ResultSet rs) throws SQLException {
        LimitationDeposits ld = new LimitationDeposits();
        ld.setActive(rs.getLong("is_active") == 1 ? true : false);
        ld.setAffiliateId(rs.getLong("affiliate_id"));
        ld.setCampaignId(rs.getLong("campaign_id"));
        ld.setComments(rs.getString("comments"));
        ld.setId(rs.getLong("id"));
        ld.setCountryId(rs.getLong("country_id"));
        ld.setCurrencyId(rs.getLong("currency_id"));
        ld.setMinimumDeposit(rs.getLong("minimum_deposit")/100);
        ld.setMinimumFirstDeposit(rs.getLong("minimum_first_deposit")/100);
        ld.setDefaultDeposit(rs.getLong("default_deposit")/100);
        ld.setPaymentRecipientId(rs.getLong("payment_recipient_id"));
        ld.setSkinId(rs.getLong("skin_id"));
        if (null != rs.getTimestamp("time_updated")) {
            ld.setTimeUpdated(convertToDate(rs.getTimestamp("time_updated")));
        }
        ld.setWriterId(rs.getLong("writer_id"));
        ld.setGroupId(rs.getLong("group_id"));
        ld.setRemarketing(rs.getLong("is_remarketing") == 1 ? true : false);
        return ld;
    }

    public static void update(Connection con, LimitationDeposits limitationDeposit) throws SQLException {
        PreparedStatement ps = null;

        try {
            String sql = "UPDATE " +
                             "limitation_deposits " +
                         "SET " +
                             "comments = ?, " +
                             "minimum_deposit = ?, " +
                             "minimum_first_deposit = ?, " +
                             "default_deposit = ?, " +
                             "writer_id = ?, " +
                             "is_active = ?, " +
                             "time_updated = sysdate ";
                 if (!limitationDeposit.isUpdateByGroup()) {
                      sql += ", group_id = seq_limitation_deposits.nextval ";
                 }
                  sql += "WHERE ";
                 if (limitationDeposit.isUpdateByGroup()) {
                     sql += "group_id = ? ";
                 } else {
                     sql += "id = ? ";
                 }
            ps = con.prepareStatement(sql);
            int index = 1;
            ps.setString(index++, limitationDeposit.getComments());
            ps.setLong(index++, limitationDeposit.getMinimumDeposit() * 100);
            ps.setLong(index++, limitationDeposit.getMinimumFirstDeposit() * 100);
            if (limitationDeposit.getDefaultDeposit() < 1) {
            	ps.setNull(index++, Types.NUMERIC);
            } else {
            	ps.setLong(index++, limitationDeposit.getDefaultDeposit() * 100);
            }
            ps.setLong(index++, Utils.getWriter().getWriter().getId());
            ps.setLong(index++, limitationDeposit.isActive() ? 1 : 0);
            if (limitationDeposit.isUpdateByGroup()) {
                ps.setLong(index++, limitationDeposit.getGroupId());
            } else {
                ps.setLong(index++, limitationDeposit.getId());
            }
            ps.executeUpdate();

        } finally {
            closeStatement(ps);
        }
    }

    /**
     * return list of roles for this group id or id;
     * @param con
     * @param id id or group id
     * @param isGroupId if true search by group id and not id
     * @return list of rules for this id
     * @throws SQLException
     */
    public static ArrayList<LimitationDeposits> searchById(Connection con, long id, boolean isGroupId) throws SQLException {
        PreparedStatement ps = null;
        ResultSet rs = null;
        ArrayList<LimitationDeposits> list = new ArrayList<LimitationDeposits>();
        try {
            String sql = "SELECT " +
                          "ld.*, " +
                          "s.display_name as skin_name, " +
                          "ma.name affilate_name, " +
                          "c.display_name currency_name, " +
                          "co.display_name as country_name, " +
                          "mpr.agent_name as payment_recipients_name, " +
                          "mc.name as campaign_name, " +
                          "w.user_name as writer_name " +
                        "FROM " +
                          "limitation_deposits ld " +
                              "LEFT JOIN skins s ON s.id = ld.skin_id " +
                              "LEFT JOIN marketing_affilates ma ON ld.affiliate_id = ma.id " +
                              "LEFT JOIN currencies c ON c.id = ld.currency_id " +
                              "LEFT JOIN countries co ON co.id = ld.country_id " +
                              "LEFT JOIN marketing_payment_recipients mpr ON mpr.id = ld.payment_recipient_id " +
                              "LEFT JOIN marketing_campaigns mc ON mc.id = ld.campaign_id, " +
                          "writers w " +
                        "WHERE " +
                            "w.id = ld.writer_id ";
                        if (isGroupId) {
                    sql += "AND ld.group_id = ? ";
                        } else {
                    sql += "AND ld.id = ? ";
                        }

            int index = 1;
            ps = con.prepareStatement(sql);
            ps.setLong(index++, id);

            rs = ps.executeQuery();
            while (rs.next()) {
                LimitationDeposits ld = getVO(rs);
                ld = getVOExtended(rs, ld);
                list.add(ld);
            }
        } finally {
            closeResultSet(rs);
            closeStatement(ps);
        }
        return list;
    }

	public static LimitationDeposits getLimitDeposits(Connection con, long skin, long currency, long country, long paymentRecipient, long campaign, long affiliate,
			boolean blankMinDeposit, boolean blankMinFirstDeposit, LimitationDeposits ld) throws SQLException {
		PreparedStatement ps = null;
        ResultSet rs = null;
        
        try {
            String sql = "SELECT " +
                             "ld.minimum_first_deposit as min_f_dep, " +
                             "ld.minimum_deposit as min_dep " +
                         "FROM " +
                             "limitation_deposits ld " +
                         "WHERE " +
                      		 "(ld.affiliate_id IS null OR ld.affiliate_id = ?) AND " +
                             "(ld.campaign_id IS null OR ld.campaign_id = ?) AND " +
                      		 "(ld.payment_recipient_id IS null OR ld.payment_recipient_id = ?) AND " +
                             "(ld.country_id IS null OR ld.country_id = ?) AND " +
                             "(ld.currency_id IS null OR ld.currency_id = ?) AND " +
                             "(ld.skin_id IS null OR ld.skin_id = ?) AND " +
                             "ld.is_active = " + ConstantsBase.STRING_TRUE +
                         "ORDER BY " +
                             "ld.is_remarketing, " +
                             "ld.affiliate_id, " +
                             "ld.campaign_id, " +
                             "ld.payment_recipient_id, " +
                             "ld.country_id, " +
                             "ld.currency_id, " +
                             "ld.skin_id ";
                        

            int index = 1;
            ps = con.prepareStatement(sql);
            ps.setLong(index++, affiliate);
            ps.setLong(index++, campaign);
            ps.setLong(index++, paymentRecipient);
            ps.setLong(index++, country);
            ps.setLong(index++, currency);
            ps.setLong(index++, skin);

            rs = ps.executeQuery();
            if (rs.next()) {
                if(blankMinDeposit) {
                	ld.setMinimumDeposit(rs.getLong("min_dep")/100);
                }
                if(blankMinFirstDeposit) {
                	ld.setMinimumFirstDeposit(rs.getLong("min_f_dep")/100);
                }
            }
        } finally {
            closeResultSet(rs);
            closeStatement(ps);
        }
		
		
		return ld;
		
		
	}
}
