package il.co.etrader.backend.dao_managers;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.TimeZone;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

import javax.faces.context.FacesContext;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import com.anyoption.common.beans.Investment;
import com.anyoption.common.beans.OneTouchFields;
import com.anyoption.common.beans.SpecialCare;
import com.anyoption.common.beans.base.Skin;
import com.anyoption.common.bl_vos.InvestmentType;
import com.anyoption.common.daos.MarketsDAOBase;
import com.anyoption.common.managers.MarketsManagerBase;

import il.co.etrader.backend.bl_managers.ApplicationData;
import il.co.etrader.backend.bl_vos.SalesTurnoversDetails;
import il.co.etrader.backend.bl_vos.SalesTurnoversTotals;
import il.co.etrader.backend.bl_vos.StatsResult;
import il.co.etrader.backend.bl_vos.User;
import il.co.etrader.backend.mbeans.AnalForm;
import il.co.etrader.backend.util.Constants;
import il.co.etrader.backend.util.Utils;
import il.co.etrader.bl_managers.InvestmentsManagerBase;
import il.co.etrader.bl_managers.TransactionsManagerBase;
import il.co.etrader.bl_vos.InvestmentLimit;
import il.co.etrader.bl_vos.InvestmentsGroup;
import il.co.etrader.bl_vos.Skins;
import il.co.etrader.bl_vos.Taxpayment;
import il.co.etrader.bl_vos.TranInvestment;
import il.co.etrader.dao_managers.InvestmentsDAOBase;
import il.co.etrader.dao_managers.UsersDAOBase;
import il.co.etrader.util.CommonUtil;
import il.co.etrader.util.ConstantsBase;

public class InvestmentsDAO extends InvestmentsDAOBase {
    protected static Logger log = Logger.getLogger(InvestmentsDAO.class);

    /**
     * Get investments by filters
     * @param startRow
     * 		the start row for rownum
     * @param pageSize
     * 		number of rows to take from startRow
     * @param isPaging TODO
     * @param sumInvestments TODO
     * @return
     * 		arrayList of investments
     * @throws SQLException
     */
    public static ArrayList<Investment> get(Connection con, long id, Date from, Date to, String isSettled,
    		String marketGroup, long scheduled, String timeType, String userName, long classId,
    			long statusId, long currencyId, String opportunities, long startRow, int pageSize,
    			String skins,long invType,long invAmount,long invAmountBelow, long invCloseHour,
    			long invCloseMin, long skinBusinessCaseId, String groups, String markets, int houseResult,
    			boolean hoursFilter, String orderType, boolean isPaging, Investment sumInvestments, Date arabicStartDate, 
    			String insertedFrom, long countryId, long apiExternalUserId, int copyopInvTypeId, long fromUserId, long invId) throws SQLException {
    	log.debug("id " + id + " from " + from + " to " + to + " isSettled " + isSettled +
    		" marketGroup " + marketGroup + " scheduled " + scheduled + " timeType " + timeType + " userName " + userName + " classId " + classId +
    			" statusId " + statusId + " currencyId " + currencyId + " opportunities " + opportunities + " startRow " + startRow + " pageSize " + pageSize +
    			" skins " + skins + " invType " + invType + " invAmount " + invAmount + " invAmountBelow " + invAmountBelow + " invCloseHour " + invCloseHour +
    			" invCloseMin " + invCloseMin + " skinBusinessCaseId " + skinBusinessCaseId + " groups " + groups + " markets " + markets + " houseResult " + houseResult +
    			" hoursFilter " + hoursFilter + " orderType " + orderType + " isPaging " + isPaging + " sumInvestments " + sumInvestments + " arabicStartDate " + arabicStartDate +
    			" insertedFrom " + insertedFrom + " apiExternalUserId " + apiExternalUserId + " copyopInvTypeId " + copyopInvTypeId + " fromUserId " + fromUserId + " invId " + invId);
		PreparedStatement ps = null;
		ResultSet rs = null;
		ArrayList<Investment> list = new ArrayList<Investment>();
		FacesContext context=FacesContext.getCurrentInstance();
		//ApplicationData app = (ApplicationData) context.getApplication().createValueBinding(Constants.BIND_APPLICATION_DATA).getValue(context);

		SimpleDateFormat tzDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS Z");
		//int market=Integer.valueOf(marketGroup.split(",")[1]);
		//int group=Integer.valueOf(marketGroup.split(",")[0]);

		long sumBaseAmount = 0;
		long sumBaseWin = 0;
		long sumBaseLose = 0;

		try {
			String sql =
					   " SELECT " +
					   		" A.*, " +
				   			" b.display_name bonus_display_name, " +
				   			" t.amount transaction_amount, " +
				   			" iref.id rolled_inv_id, " +
				   			" w.USER_NAME as writerUserName," +
				   			" aeu.reference as api_external_users_refrence " +
					   " FROM " +
					   		" (" +
						   		" select " +
						   			" X.* " +
						   	 	" from " +
									" (" +
										" select " +
											" row_number()over(order by i.time_created desc) as rownumIn, " +
											" i.*, " +
											" o.current_level as opp_current_level, " +
											" m.decimal_point, " +
											" m.display_name market_name, " +
											" m.id market_id, " +
											" int.display_name type_name," +
											" u.skin_id, " +
											" u.currency_id currency_id, " +
											" u.user_name username, " +
											" u.first_name firstname, " +
											" u.last_name lastname, " +
											" u.TIME_SC_TURNOVER, " +
											" u.TIME_SC_HOUSE_RESULT, " +
                                            " u.balance, " +
											" to_char(o.time_est_closing, 'YYYY-MM-DD HH24:MI:SS.FF3 TZHTZM') AS time_est_closing," +
											" to_char(o.time_act_closing, 'YYYY-MM-DD HH24:MI:SS.FF3 TZHTZM') AS time_act_closing, " +
											" (CASE WHEN i.time_settled < SYS_EXTRACT_UTC(o.time_est_closing) " +
												  " THEN CASE WHEN o.opportunity_type_id = 1 " +
												  			" THEN o.gm_level " +
												  			" ELSE i.settled_level END " +
												  " ELSE o.closing_level END) as closing_level, " +
							                " o.scheduled scheduled, " +
							                " c.a3 country_code, " +
							                " c.id as cid, " +
							                " c.country_name as cname, " +
							                " (CASE WHEN i.time_settled < SYS_EXTRACT_UTC(o.time_est_closing) AND o.opportunity_type_id = 3 " +
							                	  " THEN 1 " +
							                	  " ELSE 0 END) as is_option_plus, " +
							                " o.opportunity_type_id " +
											condString(id, from, to, isSettled, marketGroup, scheduled, timeType, userName, classId,
													statusId, currencyId, opportunities,skins,invType,invAmount,invAmountBelow,
													invCloseHour, invCloseMin, con, skinBusinessCaseId, groups, markets, houseResult, 
													arabicStartDate, insertedFrom, null == sumInvestments ? false : true, countryId, 
															apiExternalUserId, copyopInvTypeId, fromUserId, invId) +
										//" order by " + timeType + " " + orderType + " " +
									" )X " +
								   " WHERE " +
								   		" rownumIn > " + startRow + " and rownumIn <= " + (startRow+pageSize);

						sql +=	" )A " +
								" LEFT JOIN investments iref ON A.id = iref.reference_investment_id " +
								" LEFT JOIN transactions t ON t.bonus_user_id = A.bonus_user_id and A.bonus_odds_change_type_id > 0 " +
								" LEFT JOIN bonus_users bu ON bu.id = A.bonus_user_id and A.bonus_odds_change_type_id > 0 " +
                        		" LEFT JOIN bonus b ON bu.bonus_id = b.id " +
                        		" LEFT JOIN api_external_users aeu ON A.api_external_user_id = aeu.id " +
                        		" LEFT JOIN WRITERS w ON w.ID = A.WRITER_ID ";
			if (isPaging){
				sql +=
					   " WHERE " +
					   		" A.rownumIn > " + startRow + " and A.rownumIn <= " + (startRow+pageSize);
			}

			sql +=
					   " ORDER BY " +
					   		" A.rownumIn ";


			ps = con.prepareStatement(sql);
			if (!hoursFilter){
				ps.setTimestamp(1, CommonUtil.convertToTimeStamp(CommonUtil.getDateTimeFormat(CommonUtil.getStartOfDay(from), Utils.getWriter().getUtcOffset())));
				ps.setTimestamp(2, CommonUtil.convertToTimeStamp(CommonUtil.getDateTimeFormat(CommonUtil.getEndOfDay(to), Utils.getWriter().getUtcOffset())));
				if(null != arabicStartDate){
					ps.setTimestamp(3, CommonUtil.convertToTimeStamp(CommonUtil.getDateTimeFormat(arabicStartDate , Utils.getWriter().getUtcOffset())));
				}
			} else {
				ps.setTimestamp(1, CommonUtil.convertToTimeStamp(CommonUtil.getDateTimeFormat(from, Utils.getWriter().getUtcOffset())));
				ps.setTimestamp(2, CommonUtil.convertToTimeStamp(CommonUtil.getDateTimeFormat(to, Utils.getWriter().getUtcOffset())));
				if(null != arabicStartDate){
					ps.setTimestamp(3, CommonUtil.convertToTimeStamp(CommonUtil.getDateTimeFormat(arabicStartDate , Utils.getWriter().getUtcOffset())));
				}
			}

			rs = ps.executeQuery();


			while(rs.next()) {
				Investment i = getVO(rs);
                SpecialCare specialCare = new SpecialCare();

				i.setMarketName(MarketsDAOBase.getMarketNameExtention(rs.getString("market_id"),
						MarketsManagerBase.getMarketName(Skins.SKIN_REG_EN, rs.getLong("market_id")),
						rs.getString("market_name")));
				i.setMarketId(rs.getLong("market_id"));
				i.setTypeName(CommonUtil.getMessage(rs.getString("type_name"), null, Utils.getWriterLocale(context)));
				
				if(i.getTypeId() == Investment.INVESTMENT_TYPE_BUBBLES) {
					i.setClosingLevel(rs.getDouble("bubbles_closing_level"));
				} else {
					i.setClosingLevel(rs.getDouble("closing_level"));
				}
				i.setTimeEstClosing(tzDateFormat.parse(rs.getString("time_est_closing")));
				if (rs.getString("time_act_closing") != null) {
					i.setTimeActClosing(tzDateFormat.parse(rs.getString("time_act_closing")));
				}
				i.setScheduledId(rs.getLong("scheduled"));
				i.setUserName(rs.getString("username"));
				i.setUserFirstName(rs.getString("firstname"));
				i.setUserLastName(rs.getString("lastname"));
                specialCare.setTimeScTurnover(convertToDate(rs.getTimestamp("TIME_SC_TURNOVER")));
                specialCare.setTimeScHouseResult(convertToDate(rs.getTimestamp("TIME_SC_HOUSE_RESULT")));
                i.setSpecialCare(specialCare);
				//i.setSpecialCare(rs.getString("TIME_SC_TURNOVER") != null || rs.getString("TIME_SC_HOUSE_RESULT") != null);
				i.setBonusOddsChangeTypeId(rs.getInt("bonus_odds_change_type_id"));
				i.setUserCountryCode(rs.getString("country_code"));
				
				i.setCountryId(rs.getLong("cid"));
				i.setCountryName(rs.getString("cname"));
				
				i.setSkin(CommonUtil.getMessage(ApplicationData.getSkinById(rs.getInt("skin_id")).getDisplayName(),null));
                i.setRolledInvId(rs.getLong("rolled_inv_id"));
                i.setBonusDisplayName(rs.getString("bonus_display_name"));
                i.setTransactionAmount(rs.getLong("transaction_amount"));

				// set up_down field for one touch investments
				if ( i.getTypeId() == InvestmentsManagerBase.INVESTMENT_TYPE_ONE ) {
					OneTouchFields oneTouchAttribs = InvestmentsDAOBase.getOneTouchFields(con, i.getOpportunityId());
					i.setOneTouchUpDown(oneTouchAttribs.getUpDown());
					i.setOneTouchDecimalPoint(oneTouchAttribs.getDecimalPoint());
				}
                i.setOptionPlus(rs.getInt("is_option_plus") == 1);
                i.setOpportunityTypeId(rs.getLong("opportunity_type_id"));
                i.setOppCurrentLevel(rs.getDouble("opp_current_level"));
                i.setDecimalPoint(rs.getLong("decimal_point"));
                i.setInvestmentsCount(rs.getLong("contracts_count"));
                if (i.getTypeId() == InvestmentsManagerBase.INVESTMENT_TYPE_BUY || i.getTypeId() == InvestmentsManagerBase.INVESTMENT_TYPE_SELL) {
                    i.setInvestmentsCount(i.getInvestmentsCount() * i.getContractsStep());
                }
                i.setUserBalance(rs.getLong("balance"));
                i.setApiExternalUserReference(rs.getString("api_external_users_refrence"));
                i.setBubbleHighLevel(rs.getDouble("bubble_high_level"));
                i.setBubbleLowLevel(rs.getDouble("bubble_low_level"));
                i.setBubbleStartTime(convertToDate(rs.getTimestamp("bubble_start_time")));
                i.setBubbleEndTime(convertToDate(rs.getTimestamp("bubble_end_time")));
				list.add(i);

				if (CommonUtil.isParameterEmptyOrNull(isSettled) || !(isSettled.equalsIgnoreCase("0") && i.getIsSettled() == 1)) {
					sumBaseAmount += i.getBaseAmount();
					sumBaseWin += i.getBaseWin() ;
					sumBaseLose += i.getBaseLose();
				}
			}

			if (null != sumInvestments) {
				sumInvestments.setBaseAmount(sumBaseAmount);
				sumInvestments.setBaseWin(sumBaseWin);
				sumInvestments.setBaseLose(sumBaseLose);
			}
		} catch (Exception e) {
			log.error("", e);
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		return list;
	}

    /**
     * Create an sql conditions for get all investments function and getBaseTotals
     * @return
     * 		Sql String condition
     */
    public static String condString(long id, Date from, Date to, String isSettled,
    		String marketGroup, long scheduled, String timeType, String userName, long classId, long statusId,
    			long currencyId, String opportunities,String skins,long invType,long invAmount,
    			long invAmountBelow, long invCloseHour,long invCloseMin,Connection con,
    			long skinBusinessCaseId, String groups, String markets, int houseResult, Date arabicStartDate,
    			String insertedFrom, boolean isDaily, long countryId, long apiExternalUserId, int copyopInvTypeId, long fromUserId, long invId) {

		//SimpleDateFormat tzDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS Z");
		int market=Integer.valueOf(marketGroup.split(",")[1]);
		String invFilter = "";
		if (invId != 0) {
			invFilter = String.valueOf(invId); 
		}
		//int group=Integer.valueOf(marketGroup.split(",")[0]);
			String sql =
					" from " +
						" countries c, " +
	                    " opportunities o, " +
	                    " opportunity_types ot, " +
                        " markets m, " +
                        " investment_types int, " +
                        " users u, " +
                        " ( " +
	                        " select " +
	                        	" i.*, " +
	                        	" row_number()over" +
	                        	"(" +
	                        		"partition by " +
		                        		" i.user_id, " +
		                        		" i.opportunity_id, " +
		                        		" i.type_id, " +
		                        		" i.amount, " +
		                        		" i.current_level, " +
		                        		" i.time_created, " +
		                        		" i.time_settled, " +
		                        		" i.group_inv_id " +
		                        		" order by i.id asc " +
	                    		") id_order, " +
	                			" count(*)over" +
	                			"(" +
	                				"partition by " +
	                					" i.user_id, " +
	                					" i.opportunity_id, " +
	                					" i.type_id, " +
	                					" i.amount, " +
	                					" i.current_level, " +
	                					" i.time_created, " +
	                					" i.time_settled, " +
	                					" i.group_inv_id " +
	        					") contracts_count " +
	                		" from  " +
	                			" investments i, " +
	                			" opportunities o " +
	            			" where " +
	            				" 1=1  ";

								if (isSettled != null && !isSettled.equals("")) {
									sql += " and (i.is_settled = " + Integer.parseInt(isSettled);
								    if (Integer.parseInt(isSettled) == 0 && isDaily) {
								    	sql += " or (i.is_settled = 1 and " +
								    				"o.is_settled = 0 and " +
								    				"i.type_id in (4,5)) ";
								    }
								    sql += " ) ";
								}
			if ( statusId == 0 ) {
				sql += 			" and ( i.is_canceled= 1 AND NOT EXISTS (SELECT ID FROM investments ii WHERE ii.reference_investment_id = i.ID AND ii.time_created > i.time_created)  ) ";
			} else if (statusId == 1) {
				sql += 			" and i.is_canceled= 0 ";
			}
				//sql +=   		" and " + timeType + ">=? and " + timeType + "<=? ";
	            sql +=				" and i.opportunity_id = o.id " +
	            					" and nvl(i.id, 0) LIKE '%" + invFilter + "%'" +
	    				" )i " +

					" where " +
					 	" 1=1 and o.opportunity_type_id = ot.id and " +
					 	" id_order = 1 and " +
                        " i.type_id = int.id and " +
                        " u.id = i.user_id and " +
                        " u.country_id = c.id " ;
	            if(countryId >0) {
	            	sql+=" and u.country_id = " + countryId +" ";
	            }

		    sql+=" and u.skin_id in (" + skins + ")";

			if (id != 0) {
				sql += " and i.user_id=" + id;
			}

			if (currencyId!=ConstantsBase.CURRENCY_ALL_ID) {
				sql+=" and u.currency_id=" + currencyId;
			}

			sql += " and i.opportunity_id=o.id " +
				   " and o.market_id = m.id " +
				   " and " + timeType + ">=? and " + timeType + "<=? ";

			if (isSettled != null && !isSettled.equals("")) {
				sql += " and (i.is_settled = " + Integer.parseInt(isSettled);
			    if (Integer.parseInt(isSettled) == 0 && isDaily) {
			    	sql += " or (i.is_settled = 1 and " +
			    				"o.is_settled = 0 and " +
			    				"i.type_id in (4,5)) ";
			    }
			    sql += " ) ";
			}

			if (market>0) {
				sql += " and m.id=" + market;
			}

			// As there are no groups for market now,the below code is not needed.
			/*if (group>0) {

				//using skin_id=SKIN_ETRADER which is the etrader skinId do get the market.id group
				sql+= " and m.id in (select smgm.market_id " +
						"from skin_market_group_markets smgm , skin_market_groups smg " +
						"where smgm.skin_market_group_id=smg.id and smg.skin_id=" + ConstantsBase.SKIN_ETRADER +" and smg.market_group_id=" + group+")";
			}*/

			if ( userName != null && !userName.equals("") ) {
				sql += " and u.user_name='"+userName+"'";
			}

			if ( classId != ConstantsBase.USER_CLASS_ALL )   {

		  		if ( classId == ConstantsBase.USER_CLASS_COMPANY_OR_PRIVATE )  {
		  			sql += " and (u.class_id="+ConstantsBase.USER_CLASS_PRIVATE+""+
		  				  " or u.class_id="+ConstantsBase.USER_CLASS_COMPANY+")";
		  		}
		  		else {
		  				sql += " and u.class_id ="+classId;
		  		}
		  	}

			if ( statusId != ConstantsBase.INVESTMENT_CLASS_STATUS_CANCELLED_AND_NON_CANCELLED_INVESTMENT ) {
				long isCancelled = (statusId==0 ? 1 : 0);
				sql += " and i.is_canceled=" + isCancelled;
			}
			
			if ( scheduled > 0 ) {
				sql += " AND o.scheduled=" + scheduled + " ";
			}

			if (!CommonUtil.isParameterEmptyOrNull(opportunities) && !opportunities.contentEquals("0")){
				sql += " AND ot.product_type_id IN (" + opportunities +") ";
			}

			if (invAmount > 0){
				String convertedInvAmount;
				if( currencyId == 0){
					convertedInvAmount = "("+invAmount +"/i.rate) ";
				}else{
					convertedInvAmount = invAmount + " ";
				}
				sql += "and (i.amount * i.contracts_count) >= " + convertedInvAmount ;
			}

			if (invAmountBelow > 0){
				String convertedInvAmount;
				if ( currencyId == 0){
					convertedInvAmount = "("+invAmountBelow +"/i.rate) ";
				} else {
					convertedInvAmount = invAmountBelow + " ";
				}
				sql += "and (i.amount * i.contracts_count) < " + convertedInvAmount ;
			}


			if(invType > 0) {
				sql += " and i.TYPE_ID = " + invType + " ";
			}


			if( invCloseHour!= 99){ // use time closing parameter
				String closingHour = new String();
				TimeZone tz = TimeZone.getTimeZone(Utils.getWriter().getUtcOffset());

				invCloseHour = invCloseHour - tz.getRawOffset() / (1000*3600);

				if (invCloseHour < 0) { // prevents negative number
					invCloseHour += 24;
				}
				if(invCloseHour < 10){
					closingHour = "0" + invCloseHour;
				} else {
					closingHour = String.valueOf(invCloseHour);
				}
				if(invCloseMin == 0){
					closingHour += ":00";
				}else{
					closingHour += ":" + invCloseMin;
				}
				sql+= " and to_char(to_timestamp(sys_extract_utc(o.time_est_closing)),'HH24:MI') = '" + closingHour + "'";
			}

			if (skinBusinessCaseId > 0) {
				sql += "AND u.skin_id in (SELECT id FROM skins WHERE business_case_id = " +skinBusinessCaseId+ ") ";
			}

			if (!CommonUtil.isParameterEmptyOrNull(groups) && !groups.contentEquals("0")){
				sql += "AND m.market_group_id IN (" + groups + ") ";
			}

			if (!CommonUtil.isParameterEmptyOrNull(markets) && !markets.contentEquals("0")){
				sql += "AND m.id IN (" + markets +") ";
			}

			if (houseResult != 0){
				if (houseResult > 0){
					sql += "AND i.house_result > 0 ";
				} else {
					sql += "AND i.house_result < 0 ";
				}
			}

            if(null != arabicStartDate){
				 sql += "  AND u.time_created >= ? ";
			}

            if (!insertedFrom.equals("-1") /* not ALL */) {
            	sql += " AND i.writer_id IN (" + insertedFrom  + ") ";
            }

            if (apiExternalUserId != 0) {
            	sql += " AND i.api_external_user_id = " + apiExternalUserId;
            }
                        
            if (copyopInvTypeId != -1) {
            	sql += " AND i.copyop_type_id = " + copyopInvTypeId;
            }
            
            if (fromUserId != 0) {
            	sql += " AND i.copyop_inv_id in (SELECT " +
            			"							inv.id " +
            			"						FROM " +
            			"							investments inv " +
            			"						WHERE " +
            			"							inv.user_id =  " + fromUserId + " ) ";
            }
            
			return sql;
		}

    public static ArrayList<InvestmentsGroup> getInvestmentsGroups(Connection con, int groupBy,
    		Date fromDate, Date toDate, int skinId, long opportunityTypeId,String searchBy,
    		long skinBusinessCaseId,
    		long scheduledId,
    		long groupId,
    		long market,
    		long countryId,
    		String insertedFrom,
    		int copyopInvTypeId,
    		long fromUserId, 
    		long canceled
    		) {

		PreparedStatement ps = null;
		ResultSet rs = null;
		ArrayList<InvestmentsGroup> list = new ArrayList<InvestmentsGroup>();
		int index = 1;

		try {
			String sql;
			switch (groupBy) {
			case AnalForm.GROUP_BY_ASSET:
				sql =	" SELECT " +
							" m.name AS asset, ";
				break;
			case AnalForm.GROUP_BY_SKIN:
				sql =	" SELECT " +
							" s.id AS skin_id, ";
				break;
			default: //user or/and external user
				sql =	" SELECT " +
						" u.id user_id, " +
						" u.user_name AS user_name, " +
						" max(s.display_name) as skin, " +
						" avg(i.rate)*u.balance as balanceBase, " +
						" u.balance , "  +
						" u.currency_id, ";
				if (AnalForm.GROUP_BY_USER != groupBy) { //user and external user or external user
					sql += " aeu.reference, i.api_external_user_id, ";
				}
				break;
			}

			if (countryId > 0) {
					sql +=	" u.country_id as country_id,";
			} else {
					sql +=  " stringagg(DISTINCT  u.country_id) as country_ids,";
			}
			sql += 			" SUM(i.amount*i.rate) AS total_investments, " +
							" max(a3) as country, " +
							" SUM((i.amount+i.win-i.lose)*rate) AS total_return, " +
						    " SUM(case when i.house_result < 0 then 1 else 0 end) as hits, " +
				 			" COUNT(i.id) AS count " +
			 			" FROM " +
			 				" investments i " +
			 				" 	LEFT JOIN api_external_users aeu ON aeu.id = i.api_external_user_id, " +
			 				" users u, " +
			 				" countries c, " +
			 				" skins s, " +
			 				" opportunities o, " +
			 				" opportunity_types ot, " +
			 				" markets m " +
			 			"WHERE " +
			 				" o.opportunity_type_id = ot.id AND " +
			 				" i.user_id=u.id AND " +
			 				" u.country_id=c.id AND " +
			 				" u.skin_id=s.id AND " +
			 				" i.opportunity_id=o.id AND " +
			 				" o.market_id=m.id AND " +
			 				" u.class_id > 0 AND " +
			 				" i.is_settled = 1 AND " +
			 				" o.time_est_closing >= ? AND o.time_est_closing <= ? ";

			if (skinBusinessCaseId > 0) {
				sql +=		" AND s.business_case_id=" + skinBusinessCaseId + " ";
			}
			
			if (groupId > 0) {
				sql +=		" AND m.market_group_id=" + groupId + " ";
			}
			
			if (market > 0) {
				sql +=		" AND m.id=" + market + " ";
			}
			
			if(canceled == 0 ) {
				sql +=		" AND i.is_canceled = 1 ";
			} if(canceled == 1 ) { 
				sql +=		" AND i.is_canceled = 0 ";
			}
			
			if (countryId > 0) {
				sql +=		" AND c.id=" + countryId + " ";
			}
			
			//-------------------------
			if (skinId > 0) {
				sql +=		" AND s.id=" + skinId + " ";
			}

			if ( opportunityTypeId > 0 ) {
				sql += 		" AND ot.product_type_id = " + opportunityTypeId + " ";
			}
			if (searchBy.equals("0")) {
				 sql += " AND i.time_created >= ? "+
			 			" AND i.time_created <= ? ";

			} else {
				 sql += " AND i.time_settled >= ? "+
			 			" AND i.time_settled <= ? ";
			}
			
            if (!insertedFrom.equals("-1") /* not ALL */) {
            	sql += " AND i.writer_id IN (" + insertedFrom  + ") ";
            }

			if (AnalForm.GROUP_BY_EXTERNAL == groupBy) { //external user
				 sql += " AND i.api_external_user_id > 0 " ;
			}

			if (AnalForm.GROUP_BY_USER == groupBy) { //user
				 sql += " AND u.skin_id != " + Skin.SKIN_API ;
			}
			
            if (copyopInvTypeId != -1) {
            	sql += " AND i.copyop_type_id = " + copyopInvTypeId + " ";
            }
            
            if (scheduledId > 0) {
            	sql += " AND o.scheduled = " + scheduledId + " ";
            }
            
            if (fromUserId != 0) {
            	sql += " AND i.copyop_inv_id in (SELECT " +
            			"							inv.id " +
            			"						FROM " +
            			"							investments inv " +
            			"						WHERE " +
            			"							inv.user_id =  " + fromUserId + " ) ";
            }
			
			switch (groupBy) {
			case AnalForm.GROUP_BY_ASSET:
				sql +=	" GROUP BY m.name ";
				break;
			case AnalForm.GROUP_BY_SKIN:
				sql +=	" GROUP BY s.id ";
				break;
			default: //user or/and external user
				sql +=	" GROUP BY u.id, u.user_name, u.balance, u.currency_id ";
				if (AnalForm.GROUP_BY_USER != groupBy) { //user and external user or external user
					sql += ", aeu.reference, i.api_external_user_id ";
				}
				break;
			}
			if (countryId > 0) { // Group by country Id 
				sql +=	", u.country_id ";
			}

			ps = con.prepareStatement(sql);
			ps.setTimestamp(index++ , CommonUtil.convertToTimeStamp(fromDate));
			ps.setTimestamp(index++ , CommonUtil.convertToTimeStamp(toDate));
			ps.setTimestamp(index++ , CommonUtil.convertToTimeStamp(fromDate));
			ps.setTimestamp(index++ , CommonUtil.convertToTimeStamp(toDate));
			rs = ps.executeQuery();

			while(rs.next()) {
				InvestmentsGroup ig = new InvestmentsGroup();
				switch (groupBy) {
				case AnalForm.GROUP_BY_ASSET:
					ig.setAsset(rs.getString("asset"));
					break;
				case AnalForm.GROUP_BY_SKIN:
					ig.setSkinId(rs.getLong("skin_id"));
					break;
				default: //user or/and external user
					ig.setUserId(rs.getLong("user_id"));
					ig.setUserName(rs.getString("user_name"));

					ig.setSkin(rs.getString("skin"));
					ig.setUsersBalanceBase(rs.getLong("BALANCEBASE"));
					ig.setUsersBalance(rs.getLong("BALANCE"));
					ig.setUsersCurrencyId(rs.getLong("CURRENCY_ID"));
					if (AnalForm.GROUP_BY_USER_AND_EXTERNAL == groupBy || AnalForm.GROUP_BY_EXTERNAL == groupBy) {
						ig.setApiExternalUserReference(rs.getString("reference"));
						ig.setApiExternalUserId(rs.getLong("api_external_user_id"));
					}
					break;
				}
				ig.setCountry(rs.getString("country")); 
				if (countryId > 0) {
					ig.setCountryId(rs.getLong("country_id"));
				} else {
					ig.setCountryIds(rs.getString("country_ids"));
				}
					
				ig.setTotalInv(rs.getLong("total_investments"));
				ig.setTotalReturn(rs.getLong("total_return"));
				ig.setHits(rs.getLong("hits"));
				ig.setCount(rs.getLong("count"));

				list.add(ig);
			}
		} catch (Exception e) {
			log.log(Level.ERROR, "Cant load users for P&L ", e);
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		return list;
	}

    /**
     * Get totals for investments page
     * @return
     * 		ArrayList with all totals we need for page
     * @throws SQLException
     */
    public static ArrayList<Object> getBaseTotals(Connection con, long id, Date from, Date to, String isSettled,
    		String marketGroup, long scheduled, String timeType, String userName, long classId, long statusId,
    			long currencyId, String opportunities,String skins,long invType,long invAmount,
    			long invAmountBelow, long invCloseHour,long invCloseMin, long skinBusinessCaseId, String groups,
    			String markets, int houseResult, String orderBy, Date arabicStartDate, String insertedFrom, long apiExternalUserId, int copyopInvTypeId, long fromUserId, long invId) throws SQLException {

		PreparedStatement ps = null;
		ResultSet rs = null;
		ArrayList<Object> baseTotals = null;
		try {

			String sql =  "select sum(i.amount * i.contracts_count) total_amount, " +
						  " sum(i.amount+i.win-i.lose) total_refund, " +
						  " sum((i.amount * i.rate) * i.contracts_count) base_total_amount, " +
						  " sum((i.amount+i.win-i.lose) * i.rate) base_total_refund, " +
						  " sum(-1 * (i.win-i.lose) * i.contracts_count) total_win_lose, " +
						  " sum(-1 * (i.win-i.lose) * i.rate * i.contracts_count) base_total_win_lose, " +
						  " sum(CASE WHEN i.type_id = " + InvestmentType.BUY + " THEN ROUND(i.current_level * 100) " +
						  		" ELSE " +
						 		  "(CASE WHEN i.type_id = " + InvestmentType.SELL + " THEN (10000 - ROUND(i.current_level * 100)) " +
					 		  		"ELSE " +
					 		  		"0 END)END) total_price, " +
		 		  		  " sum(CASE WHEN i.type_id in (" + InvestmentType.BUY + "," + InvestmentType.SELL + ") THEN i.contracts_count * ROUND(i.amount / (i.current_level * 100)) END) total_contracts_count, " +
	  		  			  /*" sum() total_above, " +
	  		  			  " sum() total_below, " +
	  		  			  " sum() base_total_above, " +
	  		  			  " sum() base_total_below, " +*/
						  " count(*) count " +
						  condString(id, from, to, isSettled, marketGroup, scheduled, timeType, userName,
								  classId, statusId, currencyId, opportunities,skins,invType,invAmount,
								  invAmountBelow, invCloseHour,invCloseMin,con, skinBusinessCaseId, groups, 
								  markets, houseResult, arabicStartDate, insertedFrom, false, 0, apiExternalUserId, copyopInvTypeId, fromUserId, invId);

			ps = con.prepareStatement(sql);
			ps.setTimestamp(1, CommonUtil.convertToTimeStamp(CommonUtil.getDateTimeFormat(CommonUtil.getStartOfDay(from), Utils.getWriter().getUtcOffset())));
			ps.setTimestamp(2, CommonUtil.convertToTimeStamp(CommonUtil.getDateTimeFormat(CommonUtil.getEndOfDay(to), Utils.getWriter().getUtcOffset())));
			if(null != arabicStartDate){
				ps.setTimestamp(3, CommonUtil.convertToTimeStamp(CommonUtil.getDateTimeFormat(arabicStartDate , Utils.getWriter().getUtcOffset())));
			}

			rs = ps.executeQuery();
			while(rs.next()) {
				baseTotals = new ArrayList<Object>();
				baseTotals.add(rs.getLong("total_amount"));
				baseTotals.add(rs.getLong("total_refund"));
				baseTotals.add(Math.round(rs.getDouble("base_total_amount")));
				baseTotals.add(Math.round(rs.getDouble("base_total_refund")));
				baseTotals.add(rs.getInt("count"));
				baseTotals.add(rs.getDouble("total_price"));
				baseTotals.add(rs.getLong("total_contracts_count"));
				baseTotals.add(rs.getLong("total_win_lose"));
				baseTotals.add(rs.getLong("base_total_win_lose"));
				/*baseTotals.add(rs.getDouble("total_above"));
				baseTotals.add(rs.getDouble("total_below"));
				baseTotals.add(rs.getDouble("base_total_above"));
				baseTotals.add(rs.getDouble("base_total_below"));*/
			}
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
    	return baseTotals;
    }

    public static ArrayList<InvestmentLimit> searchInvLimits(Connection con, Date from, Date to, long userId, long active, long marketId) throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		ArrayList<InvestmentLimit> list = new ArrayList<InvestmentLimit>();
		try {
			String sql = " SELECT " +
						 	" il.*, " +
						 	" u.user_name, " +
						 	" m.DISPLAY_NAME market_key, " +
						 	" (CASE WHEN ot.description is null THEN 'All' " +
						 		  " ELSE to_char(ot.description) END) opp_type_name " +
						 " FROM " +
						   " investment_limits il " +
						   		" left join markets m on m.id = il.market_id " +
						   		" left join users u on u.id = il.user_id " +
						   		" left join opportunity_types ot on ot.id = il.opportunity_type_id " +
						 " WHERE " +
						 	" il.TIME_CREATED between ? AND ? ";
			if (userId > 0){
				sql +=   	" AND il.USER_ID = " + userId + " ";
			}
			if (marketId > 0){
				sql +=   	" AND il.MARKET_ID = " + marketId + " ";
			}
			if (active > 0){
				if ( active == ConstantsBase.USERS_MARKET_DIS_ACTIVE ) {
		    		sql +=  " AND il.IS_ACTIVE = 1 ";
		    	} else {
		    		sql +=  " AND il.IS_ACTIVE = 0 ";
		    	}
			}
			sql +=		 " ORDER BY " +
						 	" il.id ";


			ps = con.prepareStatement(sql);
			ps.setTimestamp(1, CommonUtil.convertToTimeStamp(CommonUtil.getDateTimeFormat(from, Utils.getAppData().getUtcOffset())));
			ps.setTimestamp(2, CommonUtil.convertToTimeStamp(CommonUtil.getDateTimeFormat(CommonUtil.addDay(to), Utils.getAppData().getUtcOffset())));

			rs = ps.executeQuery();
			while(rs.next()) {
				InvestmentLimit vo = getInvestmentLimitVo(rs);
				if (rs.getString("market_key") != null && rs.getString("market_key") != "") {
					vo.setMarketNameKey(MarketsManagerBase.getMarketName(Skins.SKIN_REG_EN, rs.getLong("MARKET_ID")));
				} else {
					vo.setMarketNameKey("");
				}
				vo.setUserName(rs.getString("user_name"));
				vo.setOpportunityTypeName(rs.getString("opp_type_name"));
				list.add(vo);
			}
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		return list;
	}


    public static boolean hasInvestments(Connection con, long userId, boolean settled) throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			String sql = " select * from INVESTMENTS where user_id="+userId+" and is_settled=" + (settled == true ? 1 : 0);

			ps = con.prepareStatement(sql);
			rs = ps.executeQuery();
			if (rs.next()) {
				return true;
			}
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		return false;
	}


    public static boolean hasNonCanceledInvestmentsByOppId(Connection con, long id) throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			String sql = " select * from INVESTMENTS where opportunity_id="+id+" and is_canceled=0";

			ps = con.prepareStatement(sql);
			rs = ps.executeQuery();
			if (rs.next()) {
				return true;
			}
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		return false;
	}

	public static String[] getInvestmentsTotals(Connection con, long id) throws SQLException {
		return getInvestmentsTotals(con, id, 0);
	}

	public static String[] getInvestmentsTotals(Connection con, long id, long apiExternalUserId) throws SQLException {

		String[] res = new String[3];
		PreparedStatement ps = null;
		ResultSet rs = null;

		try {
			String sql = "  SELECT " +
								"COUNT(*) total, " +
								"TRUNC(AVG(amount)) avgamount, " +
								"SUM(amount) totalamount " +
							"FROM " +
								" investments i " +
							"WHERE " +
							" is_canceled = 0 AND ";
					if (id > 0) {
						sql += " i.user_id = ? ";
					} else {
						sql += " i.api_external_user_id = ? ";
					}

			ps = con.prepareStatement(sql);
			if (id > 0) {
				ps.setLong(1, id);
			} else {
				ps.setLong(1, apiExternalUserId);
			}

			rs = ps.executeQuery();

			if (rs.next()) {
				res[0] = rs.getString("total");
				res[1] = rs.getString("avgamount");
				res[2] = rs.getString("totalamount");

				for (int i=0;i<res.length;i++) {
					if (res[i]==null) {
						res[i]="0";
					}
				}
				return res;
			}
		}

		finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		return res;
	}

	public static String getInvestmentsWinLose(Connection con, long id, int period) throws SQLException {

		String res = new String();
		PreparedStatement ps = null;
		ResultSet rs = null;

		String userOffset = UsersDAOBase.getUtcOffset(con, id);
		String dateDelta = "";
		if(userOffset.equals(ConstantsBase.UTCOFFSET_DEFAULT_PERIOD2)){
			dateDelta="+2/24";
		}
		if(userOffset.equals(ConstantsBase.UTCOFFSET_DEFAULT_PERIOD1)){
			dateDelta="+3/24";
		}

		try {
			String sql =
				"SELECT " +
					"sum(win)-sum(lose) winlose " +
				"FROM " +
					"investments i,opportunities o " +
				"WHERE i.user_id=? AND " +
					"is_canceled=0 AND " +
					"NOT (bonus_odds_change_type_id = 1 and lose > 0) AND"+
					" i.opportunity_id=o.id "; //ignore lossing next invest on us investments

			switch (period) {
				case ConstantsBase.WIN_LOSE_HALF_YEAR_PERIOD:
					//halfyear winlose
					sql += " AND EXTRACT(MONTH FROM o.time_est_closing"+dateDelta+") >= '" + ConstantsBase.TAX_JOB_PERIOD1_END_PART_MONTH+ "' ";
					break;
				case ConstantsBase.WIN_LOSE_YEAR_PERIOD:
					//year winlose
					sql += " AND EXTRACT(MONTH FROM o.time_est_closing"+dateDelta+") >= '" + ConstantsBase.TAX_JOB_PERIOD1_START_PART_MONTH+ "' ";
					break;
				case ConstantsBase.WIN_LOSE_TOTAL_PERIOD:	//total winlose
					break;

				default:
					break;
			}

			ps = con.prepareStatement(sql);
			ps.setLong(1, id);

			rs = ps.executeQuery();

			if (rs.next()) {
				res = rs.getString("winlose");
				if (res==null)
					res = "0";
				return res;
			}
		}

		finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		return res;
	}

	public static String getUsersWinLose(Connection con, long userId) throws SQLException {
		return getUsersWinLose(con, userId, 0);
	}

	public static String getUsersWinLose(Connection con, long userId, long apiExternalUserId) throws SQLException {

		String res = new String();
		PreparedStatement ps = null;
		ResultSet rs = null;

		try {
			String sql =
				" SELECT " +
					" SUM(-house_result) winlose " +
				" FROM " +
					" investments i " +
				" WHERE " +
					" is_canceled = 0 AND "; //ignore lossing next invest on us investments
			if (userId > 0) {
				sql += " i.user_id = ? ";
			} else {
				sql += " i.api_external_user_id = ? ";
			}

			ps = con.prepareStatement(sql);
			if (userId > 0) {
				ps.setLong(1, userId);
			} else {
				ps.setLong(1, apiExternalUserId);
			}

			rs = ps.executeQuery();

			if (rs.next()) {
				res = rs.getString("winlose");
				if (res == null)
					res = "0";
				return res;
			}
		}

		finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		return res;
	}


	/**
	 * The function get investments and transactions between 'from' and 'to' dates and another extra row before 'from' date if the row exist.
	 * Return extra row for calculate win/lose for last row.
	 *
	 * @param con
	 * @param id
	 * @param from
	 * @param to
	 * @param transactionTypeId 
	 * @return list as ArrayList<TranInvestment>
	 * @throws SQLException
	 */
	public static ArrayList<TranInvestment> getInvAndTransactions(Connection con, long id, Date from, Date to, 
			long invId, String bonusIdFreeTextFilter, String transIdFreeTextFilter, 
			int transactionTypeId) throws SQLException {
		FacesContext context = FacesContext.getCurrentInstance();
		User user = (User) context.getApplication().createValueBinding(Constants.BIND_USER).getValue(context);

		PreparedStatement ps = null;
		ResultSet rs = null;
		ArrayList<TranInvestment> list = new ArrayList<TranInvestment>();
		String invFilter = "";
		if (invId != 0) {
			invFilter = String.valueOf(invId); 
		}

		if (bonusIdFreeTextFilter == null) {
			bonusIdFreeTextFilter = "";
		}

		if (transIdFreeTextFilter == null) {
			transIdFreeTextFilter = "";
		}
		
		try {
			String sql =
			    "SELECT E.VALUE TYPE, " +
				       "BH.*, " +
				       "W.USER_NAME WRITER, " +
				       "I.IS_CANCELED INV_CANCELED, " +
				       "I2.ID ROLLED_INV_ID " +
				  "FROM BALANCE_HISTORY BH " +
				  "JOIN WRITERS W " +
				    "ON BH.WRITER_ID = W.ID " +
				  "JOIN ENUMERATORS E " +
				    "ON BH.COMMAND = E.CODE " +
				  "LEFT JOIN INVESTMENTS I " +
				    "ON BH.KEY_VALUE = I.ID " +
				   "AND BH.TABLE_NAME = 'investments' " +
				  "LEFT JOIN TRANSACTIONS TR " +
				    "ON BH.KEY_VALUE = TR.ID " +
				   "AND BH.TABLE_NAME = 'transactions' " +
				  "LEFT JOIN INVESTMENTS I2 " +
				    "ON I.ID = I2.REFERENCE_INVESTMENT_ID " +
				 "WHERE BH.USER_ID = ? " +
				   "AND E.ENUMERATOR = ? " +
				   "AND NVL(I.ID, 0) LIKE '%' || ? || '%' " +
				   "AND (NVL(TR.BONUS_USER_ID, 0) LIKE '%' || ? || '%' OR NVL(I.BONUS_USER_ID, 0) LIKE '%' || ? || '%') " +
				   "AND NVL(TR.ID, 0) LIKE '%' || ? || '%' " +
				   "AND BH.TIME_CREATED >= TO_DATE(?, 'DD.MM.YYYY HH24:MI:SS') " +
				   "AND BH.TIME_CREATED <= TO_DATE(?, 'DD.MM.YYYY HH24:MI:SS') " +
				   "AND 1 = DECODE(?, 0, 1, tr.type_id , 1, 0) " +
				 "ORDER BY BH.TIME_CREATED DESC, BH.ID DESC";


			ps = con.prepareStatement(sql);
			ps.setLong(1, id);
			ps.setString(2, ConstantsBase.ENUM_LOG_BALANCE);
			ps.setString(3, invFilter);
			ps.setString(4, bonusIdFreeTextFilter);
			ps.setString(5, bonusIdFreeTextFilter);
			ps.setString(6, transIdFreeTextFilter);
			SimpleDateFormat sd = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");
			ps.setString(7, sd.format(CommonUtil.getDateTimeFormat(from, Utils.getAppData().getUtcOffset())));
			ps.setString(8, sd.format(CommonUtil.getDateTimeFormat(to, Utils.getAppData().getUtcOffset())));
			ps.setInt(9, transactionTypeId);
			rs = ps.executeQuery();

			while(rs.next()) {
				TranInvestment i = new TranInvestment();

				i.setBalance(rs.getLong("balance"));
				i.setTaxBalance(rs.getLong("tax_balance"));
				i.setKey(rs.getLong("key_value"));
				i.setTableName(rs.getString("table_name"));
				i.setCurrencyId(user.getCurrencyId().intValue());

				i.setType(CommonUtil.getMessage(rs.getString("type"), null, Utils.getWriterLocale(context)));

				if (i.getTableName().equalsIgnoreCase(ConstantsBase.TABLE_INVESTMENTS)) {
					i.setInvTranType(TranInvestment.INVESTMENT);
					int isCanceled = rs.getInt("inv_canceled");
					long rolledInvId = rs.getLong("rolled_inv_id");
					if (isCanceled == 1 && rolledInvId > 0) {
						i.setRolledUpInvest(true);
					} else {
						i.setRolledUpInvest(false);
					}
				}
				if (i.getTableName().equalsIgnoreCase(ConstantsBase.TABLE_TRANSACTIONS)) {
					i.setInvTranType(TranInvestment.TRANSACTION);
				}

				i.setTimeCreated(convertToDate(rs.getTimestamp("time_created")));
				i.setUtcOffsetCreated(rs.getString("utc_offset"));
				i.setWriter(rs.getString("writer"));

				list.add(i);

			}
		}

		finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		return list;

	}

	public static long getNumOfCancelInvestments(Connection con, long userid) throws SQLException {
		return getNumOfCancelInvestments(con, userid, 0);
	}

	public static long getNumOfCancelInvestments(Connection con, long userId, long apiExternalUserId) throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;

		try {
			String sql =
				 "SELECT " +
				 	"(allCanceled.num - ruCanceled.num) as countCanceled " +
                 "FROM " +
                     "(SELECT " +
                             "count(i.id) as num " +
                      "FROM " +
                             "investments i " +
                     "WHERE " +
                     		 "i.is_canceled = 1 AND ";
         		 if (userId > 0) {
         			  sql += "i.user_id = ? ";
         		 } else {
         			  sql += "i.api_external_user_id = ? ";
         		 }
         		 		sql += ") allCanceled , " +

                     "(SELECT  " +
                             "count(i.id) as num " +
                      "FROM  " +
                             "INVESTMENTS i, " +
                             "(SELECT iref.reference_investment_id " +
                             " FROM investments iref  " +
                             " WHERE iref.reference_investment_id IS NOT null AND " +
                             "       iref.reference_investment_id > 0 AND  ";
                         if (userId > 0) {
                			  sql += "iref.user_id = ? ";
                		 } else {
                			  sql += "iref.api_external_user_id = ? ";
                		 }
                      sql += " ) refInvests  " +
                     "WHERE  ";
                 if (userId > 0) {
        			  sql += "i.user_id = ? AND ";
        		 } else {
        			  sql += "i.api_external_user_id = ? AND ";
        		 }
                      sql += "i.is_canceled = 1 AND " +
                             "i.insurance_flag = 2 AND " +
                             "i.id = refInvests.reference_investment_id) ruCanceled ";

			ps = con.prepareStatement(sql);
			long id = userId;
			if (userId == 0) {
				id = apiExternalUserId;
			}
			ps.setLong(1, id);
			ps.setLong(2, id);
			ps.setLong(3, id);

			rs = ps.executeQuery();

			if (rs.next()) {
				return rs.getLong(1);
			}
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		return 0;
	}

	public static ArrayList<Taxpayment> getTaxpayments(Connection con, int userId) throws SQLException {
		FacesContext context = FacesContext.getCurrentInstance();
		User user = (User) context.getApplication().createValueBinding(Constants.BIND_USER).getValue(context);
		ArrayList<Taxpayment> list = new ArrayList<Taxpayment>();
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {
	        String sql =
	                "SELECT " +
	                    "th.*, w.user_name writer " +
	                "FROM " +
	                    "tax_history th, writers w " +
	                "WHERE " +
	                    "th.writer_id = w.id AND " +
	                	"th.user_id = " + userId;

	        pstmt = con.prepareStatement(sql);
	        rs = pstmt.executeQuery(sql);

			while(rs.next()) {
				Taxpayment tp = new Taxpayment();
				tp.setId(rs.getInt("id"));
				tp.setPeriod(rs.getInt("period"));
				tp.setTax(rs.getLong("tax"));
				tp.setWin(rs.getLong("win"));
				tp.setLose(rs.getLong("lose"));
				tp.setTimeCreated(convertToDate(rs.getTimestamp("time_created")));
				tp.setUtcOffsetCreated("utc_offset_created");
				tp.setWriter(rs.getString("writer"));
				tp.setYear(rs.getInt("year"));
				tp.setCurrencyId(user.getCurrencyId().intValue());
				tp.setUtcOffsetCreated(rs.getString("utc_offset_created"));
				list.add(tp);
			}
		}finally {
			closeResultSet(rs);
			closeStatement(pstmt);
		}
		return list;
	}

	protected static Investment getVO(ResultSet rs) throws SQLException {
		Investment vo = InvestmentsDAO.getVOBE(rs);
		vo.setUtcOffsetEstClosing(rs.getString("utc_offset_created"));
		vo.setUtcOffsetActClosing(rs.getString("utc_offset_settled"));
		vo.setCurrencyId(rs.getLong("currency_id"));
		double rate = rs.getDouble("rate");
		vo.setBaseAmount(Math.round(rate * vo.getAmount()));
		vo.setBaseWin(Math.round(rate * vo.getWin()));
		vo.setBaseLose(Math.round(rate * vo.getLose()));
        vo.setInsuranceAmountGM(rs.getLong("insurance_Amount_GM"));
        vo.setInsuranceAmountRU(rs.getLong("insurance_Amount_RU"));
        vo.setReferenceInvestmentId(rs.getLong("REFERENCE_INVESTMENT_ID"));
        vo.setRate(rs.getDouble("rate"));
        vo.setInsuranceFlag(rs.getInt("insurance_flag"));
        vo.setOptionPlusFee(rs.getLong("option_plus_fee"));
        vo.setPrice(rs.getDouble("price"));
        if (rs.wasNull()) {
            vo.setInsuranceFlag(null);
        }
		return vo;
	}

	protected static Investment getVOBE(ResultSet rs) throws SQLException {
		Investment vo = new Investment();
		vo.setId(rs.getLong("id"));
		vo.setUserId(rs.getLong("user_id"));
		vo.setOpportunityId(rs.getLong("opportunity_Id"));
		vo.setTypeId(rs.getLong("type_Id"));
		vo.setTimeCreated(CommonUtil.getDateTimeToUTC(convertToDate(rs.getTimestamp("time_created")), Utils.getWriter().getUtcOffset()));
//		vo.setAmount(rs.getLong("amount") * rs.getLong("contracts_count"));
		vo.setAmount(rs.getLong("amount"));
        vo.setCurrentLevel(rs.getDouble("current_level"));
        vo.setOptionPlusFee(rs.getLong("option_plus_fee"));
		vo.setContractsStep(InvestmentsManagerBase.getBinary0100NumberOfContracts(vo));
		vo.setAmount(vo.getAmount() * rs.getLong("contracts_count"));
		vo.setRealLevel(rs.getDouble("real_level"));
		vo.setWwwLevel(rs.getBigDecimal("www_level"));
		vo.setOddsWin(rs.getDouble("odds_Win"));
		vo.setOddsLose(rs.getDouble("odds_Lose"));
		vo.setWriterId(rs.getLong("writer_id"));
		vo.setCanceledWriterId(rs.getBigDecimal("canceled_writer_id"));
		vo.setIp(rs.getString("ip"));
		vo.setWin(rs.getLong("win"));
		vo.setLose(rs.getLong("lose"));
		vo.setHouseResult(rs.getLong("house_Result"));
		vo.setTimeSettled(convertToDate(rs.getTimestamp("time_settled")));
		vo.setIsSettled(rs.getInt("is_settled"));
		vo.setIsCanceled(rs.getInt("is_canceled"));
		vo.setTimeCanceled(convertToDate(rs.getTimestamp("time_canceled")));
		vo.setIsVoid(rs.getInt("is_void"));
		vo.setBonusOddsChangeTypeId(rs.getInt("bonus_odds_change_type_id"));
        vo.setUtcOffsetCreated(rs.getString("utc_offset_created"));
        vo.setUtcOffsetSettled(rs.getString("utc_offset_settled"));
        vo.setUtcOffsetCancelled(rs.getString("utc_offset_cancelled"));
        vo.setFromGraph(rs.getInt("is_from_graph"));
        vo.setWriterUserName(rs.getString("writerUserName"));
        vo.setApiExternalUserId(rs.getLong("api_external_user_id"));
        vo.setCopyOpInvId(rs.getLong("copyop_inv_id"));
        vo.setCopyOpTypeId(rs.getInt("copyop_type_id"));
        
		return vo;
	}

	/**
	 * prepare to investment to settle
	 * @param con
	 * @param id investment id
	 * @throws SQLException
	 */
	public static void prepareInvestmentToSettle(Connection con, long id) throws SQLException {
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {
			String sql = " update investments i set IS_SETTLED = 0, IS_CANCELED = 0, TIME_CANCELED = null, UTC_OFFSET_CANCELLED = null, " +
						 "is_void = 0, CANCELED_WRITER_ID = null, WIN = 0, LOSE = 0, HOUSE_RESULT = 0 " +
						 "where id=?";

			pstmt = con.prepareStatement(sql);
			pstmt.setLong(1, id);
			pstmt.executeUpdate();
		} catch (SQLException sqle) {
			log.log(Level.ERROR, "", sqle);
			throw sqle;
		} catch (Throwable t) {
			log.log(Level.ERROR, "", t);
		} finally {
			closeResultSet(rs);
			closeStatement(pstmt);
		}
	}

	public static List<StatsResult> getStatsAssetProfit(Connection con, long userId, Date from, Date to, boolean allTypes) throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		List<StatsResult> list = new ArrayList<StatsResult>();

		try {
			String sql =
				"SELECT asset_name, opp_type, inv_num, turnover, customer_profit, ROUND(customer_profit/turnover, 2) as profit_ratio " +
				"FROM ( " +
				"	SELECT m.name asset_name, CASE WHEN (i.type_id = 3) THEN '1T' ELSE (CASE WHEN (o.opportunity_type_id = 3) THEN 'Option+' ELSE 'Binary' END) END opp_type, COUNT(i.id) inv_num, SUM(i.amount) turnover, SUM(-i.house_result) customer_profit " +
				"	FROM investments i, opportunities o, markets m, users u " +
				"	WHERE " +
				"		i.opportunity_id = o.id AND o.market_id = m.id AND i.user_id = u.id " +
				"		AND i.is_canceled = 0 AND i.is_settled = 1 " +
				"		AND i.user_id = ? ";
			if (!allTypes) {
				sql +=
				"		AND i.type_id <> 3 AND o.opportunity_type_id <> 3 ";
			}

			if (null != from) {
				sql +=
				"		AND o.time_est_closing >= to_date(?, 'DD.MM.YYYY-HH24:MI:SS') ";
			}

			if (null != to) {
				sql += "AND o.time_est_closing <= to_date(?, 'DD.MM.YYYY-HH24:MI:SS') ";
			}

			sql +=
				"	GROUP BY m.name, CASE WHEN (i.type_id = 3) THEN '1T' ELSE (CASE WHEN (o.opportunity_type_id = 3) THEN 'Option+' ELSE 'Binary' END) END " +
				"	HAVING SUM(-i.house_result) > 0 " +
				") " +
				"ORDER BY customer_profit DESC";

			ps = con.prepareStatement(sql);
			SimpleDateFormat sd = new SimpleDateFormat("dd.MM.yyyy-HH:mm:ss");

			ps.setLong(1, userId);
			if (null != from) {
				GregorianCalendar from1 = new GregorianCalendar();
				from1.setTime(CommonUtil.getStartOfDay(from));
				from1.add(Calendar.MINUTE, 1);
				ps.setString(2, sd.format(CommonUtil.getDateTimeFormat(from1.getTime(), Utils.getAppData().getUtcOffset())));
			}
			if (null != to) {
				GregorianCalendar to1 = new GregorianCalendar();
				to1.setTime(CommonUtil.getEndOfDay(to));
				to1.add(Calendar.MINUTE, 1);
				ps.setString(3, sd.format(CommonUtil.getDateTimeFormat(to1.getTime(), Utils.getAppData().getUtcOffset())));
			}
			rs = ps.executeQuery();
			while (rs.next()) {
				StatsResult result = new StatsResult();
				result.setGroupName(rs.getString("asset_name"));
				result.setOppType(rs.getString("opp_type"));
				result.setInvestmentsNumber(rs.getLong("inv_num"));
				result.setTurnover(rs.getLong("turnover"));
				result.setCustomerProfit(rs.getLong("customer_profit"));
				result.setProfitRatio(rs.getDouble("profit_ratio"));

				list.add(result);
			}
			if (list.size() == 0) {
				list = null;
			}
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		return list;
	}

	public static List<StatsResult> getStatsAssetSuccess(Connection con, long userId) throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		List<StatsResult> list = new ArrayList<StatsResult>();

		try {
			String sql =
				"SELECT asset_name, inv_num, success_inv, ROUND(success_inv/inv_num, 2) success_ratio " +
				"FROM ( " +
				"	SELECT m.name asset_name, COUNT(i.id) inv_num, SUM(CASE WHEN ((-i.house_result)> 0) THEN 1 ELSE 0 END) success_inv " +
				"	FROM investments i, opportunities o, markets m, users u " +
				"	WHERE " +
				"		i.opportunity_id = o.id AND o.market_id = m.id AND i.user_id = u.id " +
				"		AND i.is_canceled = 0 AND i.is_settled = 1 AND i.type_id <> 3 AND o.opportunity_type_id <> 3 " +
				"		AND i.user_id = ? " +
				"	GROUP BY m.name " +
				"	HAVING COUNT(i.id) > 10 " +
				") " +
				"WHERE success_inv > 0 " +
				"ORDER BY success_ratio DESC";

			ps = con.prepareStatement(sql);
			ps.setLong(1, userId);
			rs = ps.executeQuery();
			while (rs.next()) {
				StatsResult result = new StatsResult();
				result.setGroupName(rs.getString("asset_name"));
				result.setInvestmentsNumber(rs.getLong("inv_num"));
				result.setSuccessInvestmentsNumber(rs.getLong("success_inv"));
				result.setSuccessInvestmentsRatio(rs.getDouble("success_ratio"));

				list.add(result);
			}
			if (list.size() == 0) {
				list = null;
			}
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		return list;
	}


	public static List<StatsResult> getStatsAssetGroupProfit(Connection con, long userId) throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		List<StatsResult> list = new ArrayList<StatsResult>();

		try {
			String sql =
				"SELECT asset_group_name, inv_num, turnover, customer_profit, ROUND(customer_profit/turnover, 2) as profit_ratio " +
				"FROM ( " +
				"	SELECT CASE WHEN (u.skin_id=1) THEN mg.name ELSE (CASE WHEN (mg.name = 'Abroad Stocks') THEN 'Stocks' ELSE mg.name END) END asset_group_name, COUNT(i.id) inv_num, SUM(i.amount) turnover, SUM(-i.house_result) customer_profit " +
				"	FROM investments i, opportunities o, markets m, users u, market_groups mg " +
				"	WHERE " +
				"		i.opportunity_id = o.id AND o.market_id = m.id AND i.user_id = u.id AND m.market_group_id = mg.id " +
				"		AND i.is_canceled = 0 AND i.is_settled = 1 AND i.type_id <> 3 AND o.opportunity_type_id <> 3 " +
				"		AND u.id = ? " +
				"	GROUP BY CASE WHEN (u.skin_id=1) THEN mg.name ELSE (CASE WHEN (mg.name = 'Abroad Stocks') THEN 'Stocks' ELSE mg.name END) END " +
				"	HAVING SUM(-i.house_result) > 0 " +
				") " +
				"ORDER BY customer_profit DESC";

			ps = con.prepareStatement(sql);
			ps.setLong(1, userId);
			rs = ps.executeQuery();
			while (rs.next()) {
				StatsResult result = new StatsResult();
				result.setGroupName(rs.getString("asset_group_name"));
				result.setInvestmentsNumber(rs.getLong("inv_num"));
				result.setTurnover(rs.getLong("turnover"));
				result.setCustomerProfit(rs.getLong("customer_profit"));
				result.setProfitRatio(rs.getDouble("profit_ratio"));

				list.add(result);
			}
			if (list.size() == 0) {
				list = null;
			}
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		return list;
	}


	public static List<StatsResult> getStatsAssetGroupSuccess(Connection con, long userId) throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		List<StatsResult> list = new ArrayList<StatsResult>();

		try {
			String sql =
				"SELECT asset_group_name, inv_num, success_inv, ROUND(success_inv/inv_num, 2) success_ratio " +
				"FROM ( " +
				"	SELECT CASE WHEN (mg.name = 'Abroad Stocks') THEN 'Stocks' ELSE mg.name END asset_group_name, COUNT(i.id) inv_num, SUM(CASE  WHEN ((-i.house_result)> 0) THEN 1 ELSE 0 END) success_inv " +
				"	FROM investments i, opportunities o, markets m, users u, market_groups mg " +
				"	WHERE " +
				"		i.opportunity_id = o.id AND o.market_id = m.id AND i.user_id = u.id AND m.market_group_id = mg.id " +
				"		AND i.is_canceled = 0 AND i.is_settled = 1 AND i.type_id <> 3 AND o.opportunity_type_id <> 3 " +
				"		AND i.user_id = ? " +
				"	GROUP BY CASE WHEN (mg.name = 'Abroad Stocks') THEN 'Stocks' ELSE mg.name END " +
				"	HAVING COUNT(i.id) > 10 " +
				") " +
				"WHERE success_inv > 0 " +
				"ORDER BY success_ratio DESC";

			ps = con.prepareStatement(sql);
			ps.setLong(1, userId);
			rs = ps.executeQuery();
			while (rs.next()) {
				StatsResult result = new StatsResult();
				result.setGroupName(rs.getString("asset_group_name"));
				result.setInvestmentsNumber(rs.getLong("inv_num"));
				result.setSuccessInvestmentsNumber(rs.getLong("success_inv"));
				result.setSuccessInvestmentsRatio(rs.getDouble("success_ratio"));

				list.add(result);
			}
			if (list.size() == 0) {
				list = null;
			}
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		return list;
	}

	public static List<StatsResult> getStatsTimeProfit(Connection con, long userId) throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		List<StatsResult> list = new ArrayList<StatsResult>();

		try {
			String sql =
				"SELECT " +
				"	time_period, " +
				"	COUNT(time_period) inv_num, " +
				"	SUM(turnover_1) turnover, " +
				"	SUM(customer_profit_1) customer_profit, " +
				"	ROUND(SUM(customer_profit_1)/SUM(turnover_1), 2) profit_ratio " +
				"FROM ( " +
				"	SELECT " +
				"		CASE o.scheduled " +
				"			WHEN 1 THEN 'Hourly' " +
				"			WHEN 2 THEN 'Daily' " +
				"			WHEN 3 THEN 'Weekly' " +
				"			WHEN 4 THEN 'Monthly' " +
				"			ELSE 'default' " +
				"		END time_period, " +
				"		i.amount turnover_1, i.house_result*(-1) customer_profit_1 " +
				"	FROM investments i, opportunities o,  users u " +
				"	WHERE " +
				"		i.opportunity_id = o.id AND i.user_id = u.id " +
				"		AND i.is_canceled = 0 AND i.is_settled = 1 AND i.type_id <> 3 AND o.opportunity_type_id <> 3 " +
				"		AND i.user_id = ? " +
				") " +
				"GROUP BY time_period " +
				"HAVING SUM(customer_profit_1)>0 " +
				"ORDER BY customer_profit DESC";

			ps = con.prepareStatement(sql);
			ps.setLong(1, userId);
			rs = ps.executeQuery();
			while (rs.next()) {
				StatsResult result = new StatsResult();
				result.setGroupName(rs.getString("time_period"));
				result.setInvestmentsNumber(rs.getLong("inv_num"));
				result.setTurnover(rs.getLong("turnover"));
				result.setCustomerProfit(rs.getLong("customer_profit"));
				result.setProfitRatio(rs.getDouble("profit_ratio"));

				list.add(result);
			}
			if (list.size() == 0) {
				list = null;
			}
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		return list;
	}


	public static List<StatsResult> getStatsTimeSuccess(Connection con, long userId) throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		List<StatsResult> list = new ArrayList<StatsResult>();

		try {
			String sql =
				"SELECT " +
				"	time_period, " +
				"	COUNT(*) inv_num, " +
				"	SUM(success_inv_1) success_inv, " +
				"	ROUND(SUM(success_inv_1)/COUNT(*), 2) success_ratio " +
				"FROM ( " +
				"	SELECT " +
				"		CASE o.scheduled " +
				"			WHEN 1 THEN 'Hourly' " +
				"			WHEN 2 THEN 'Daily' " +
				"			WHEN 3 THEN 'Weekly' " +
				"			WHEN 4 THEN 'Monthly' " +
				"			ELSE 'default' " +
				"		END time_period , " +
				"		CASE  WHEN ((-i.house_result) > 0) THEN 1 ELSE 0 END success_inv_1 " +
				"	FROM investments i, opportunities o,  users u " +
				"	WHERE " +
				"		i.opportunity_id = o.id AND i.user_id = u.id " +
				"		AND i.is_canceled = 0 AND i.is_settled = 1 AND i.type_id <> 3 AND o.opportunity_type_id <> 3 " +
				"		AND i.user_id = ? " +
				") " +
				"GROUP BY time_period " +
				"HAVING COUNT(*) > 10 AND SUM(success_inv_1) > 0 " +
				"ORDER BY success_ratio DESC";

			ps = con.prepareStatement(sql);
			ps.setLong(1, userId);
			rs = ps.executeQuery();
			while (rs.next()) {
				StatsResult result = new StatsResult();
				result.setGroupName(rs.getString("time_period"));
				result.setInvestmentsNumber(rs.getLong("inv_num"));
				result.setSuccessInvestmentsNumber(rs.getLong("success_inv"));
				result.setSuccessInvestmentsRatio(rs.getDouble("success_ratio"));

				list.add(result);
			}
			if (list.size() == 0) {
				list = null;
			}
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		return list;
	}


	public static List<StatsResult> getStats1TouchProfit(Connection con, long userId) throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		List<StatsResult> list = new ArrayList<StatsResult>();

		try {
			String sql =
				"SELECT asset_name, inv_num, turnover, customer_profit, ROUND(customer_profit/turnover, 2) as profit_ratio " +
				"FROM ( " +
				"	SELECT m.name asset_name, COUNT(i.id) inv_num, SUM(i.amount) turnover, SUM(-i.house_result) customer_profit " +
				"	FROM investments i, opportunities o, markets m, users u " +
				"	WHERE " +
				"		i.opportunity_id = o.id AND o.market_id = m.id AND i.user_id = u.id " +
				"		AND i.is_canceled = 0 AND i.is_settled = 1 AND i.type_id = 3 " +
				"		AND i.user_id = ? " +
				"	GROUP BY m.name " +
				"	HAVING SUM(-i.house_result) > 0 " +
				") " +
				"ORDER BY customer_profit DESC";

			ps = con.prepareStatement(sql);
			ps.setLong(1, userId);
			rs = ps.executeQuery();
			while (rs.next()) {
				StatsResult result = new StatsResult();
				result.setGroupName(rs.getString("asset_name"));
				result.setInvestmentsNumber(rs.getLong("inv_num"));
				result.setTurnover(rs.getLong("turnover"));
				result.setCustomerProfit(rs.getLong("customer_profit"));
				result.setProfitRatio(rs.getDouble("profit_ratio"));

				list.add(result);
			}
			if (list.size() == 0) {
				list = null;
			}
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		return list;
	}


	public static List<StatsResult> getStats1TouchSuccess(Connection con, long userId) throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		List<StatsResult> list = new ArrayList<StatsResult>();

		try {
			String sql =
				"SELECT asset_name, inv_num, success_inv, ROUND(success_inv/inv_num, 2) success_ratio " +
				"FROM ( " +
				"      SELECT m.name asset_name, COUNT(i.id) inv_num, SUM(CASE  WHEN ((-i.house_result)> 0) THEN 1 ELSE 0 END) success_inv " +
				"      FROM investments i, opportunities o, markets m, users u " +
				"      WHERE " +
				"            i.opportunity_id = o.id AND o.market_id = m.id AND i.user_id = u.id " +
				"            AND i.is_canceled = 0 AND i.is_settled = 1 AND i.type_id = 3 " +
				"            AND i.user_id = ? " +
				"      GROUP BY m.name " +
				") " +
				"WHERE success_inv > 0 " +
				"ORDER BY success_ratio DESC";

			ps = con.prepareStatement(sql);
			ps.setLong(1, userId);
			rs = ps.executeQuery();
			while (rs.next()) {
				StatsResult result = new StatsResult();
				result.setGroupName(rs.getString("asset_name"));
				result.setInvestmentsNumber(rs.getLong("inv_num"));
				result.setSuccessInvestmentsNumber(rs.getLong("success_inv"));
				result.setSuccessInvestmentsRatio(rs.getDouble("success_ratio"));

				list.add(result);
			}
			if (list.size() == 0) {
				list = null;
			}
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		return list;
	}

	public static List<StatsResult> getStatsDayAndProfit(Connection con, long userId) throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		List<StatsResult> list = new ArrayList<StatsResult>();

		try {
			// get one minute before - 00:00 go to previues day
			String sql =
				"SELECT TO_CHAR(GET_TIME_BY_OFFSET(o.time_est_closing - 1/(24*60) , '" + Utils.getAppData().getUtcOffset() + "'), 'YYYY.MM.DD') time_settled, -SUM(house_result) customer_profit, count(i.id) inv_num " +
				"FROM investments i, opportunities o " +
				"WHERE i.opportunity_id = o.id AND i.is_canceled = 0 AND i.is_settled = 1 " +
				"AND i.user_id = ? " +
				"GROUP BY TO_CHAR(GET_TIME_BY_OFFSET(o.time_est_closing - 1/(24*60), '" + Utils.getAppData().getUtcOffset() + "'), 'YYYY.MM.DD') " +
				"ORDER BY 1";

			ps = con.prepareStatement(sql);
			ps.setLong(1, userId);
			rs = ps.executeQuery();
			while (rs.next()) {
				StatsResult result = new StatsResult();
				result.setDate1(CommonUtil.getStartOfDay(fixDateFormatToDate(rs.getString("time_settled"))));
				result.setCustomerProfit(rs.getLong("customer_profit"));
				result.setInvestmentsNumber(rs.getLong("inv_num"));

				list.add(result);
			}
			if (list.size() == 0) {
				list = null;
			}
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		return list;
	}

	/**
	 *  YYYY.MM.DD -> Date
	 */
	private static Date fixDateFormatToDate(String dateFormat) {
		Date res = null;
		try {
			Pattern regex = Pattern.compile("(\\d{2,4})\\.(\\d{1,2})\\.(\\d{1,2})",
				Pattern.CANON_EQ);
			Matcher matcher = regex.matcher(dateFormat);
			if (matcher.find()) {
				GregorianCalendar calendar = new GregorianCalendar();
				calendar.set(Integer.valueOf(matcher.group(1)),
						Integer.valueOf(matcher.group(2))-1, Integer.valueOf(matcher.group(3)));
				res = calendar.getTime();
			}
		} catch (PatternSyntaxException e) {
		}
		return res;
	}
	/**
	 * Insert new InvestmentLimit
	 * @param con db connection
	 * @param vo  InvestmentLimit instance for insert
	 * @throws SQLException
	 */
	public static void insertInvestmentLimit(Connection con,InvestmentLimit vo) throws SQLException {

	  PreparedStatement ps = null;

	  try {

			String sql = " Insert into investment_limits " +
							" (ID,MIN_LIMIT_GROUP_ID,MAX_LIMIT_GROUP_ID,LIMIT_TYPE_ID,"  +
							" OPPORTUNITY_TYPE_ID,SCHEDULED,USER_ID, MARKET_ID,START_DATE, " +
							" END_DATE,TIME_CREATED,WRITER_ID,IS_ACTIVE) " +
						 " Values " +
						 	" (SEQ_INV_LIMITS.NEXTVAL,?,?,?,?,?,?,?,?,?,sysdate,?,?)";

			ps = con.prepareStatement(sql);

			ps.setLong(1, vo.getMinAmountGroupId());
			ps.setLong(2, vo.getMaxAmountGroupId());
			ps.setInt(3, vo.getInvLimitTypeId());

			if (vo.getOpportunityTypeId() > 0){
				ps.setLong(4, vo.getOpportunityTypeId());
			}else{
				ps.setString(4, null);
			}

			if (vo.getScheduled() > 0){
				ps.setLong(5, vo.getScheduled());
			}else{
				ps.setString(5, null);
			}

			if (vo.getUserId() > 0){
				ps.setLong(6, vo.getUserId());
			}else{
				ps.setString(6, null);
			}

			if (vo.getMarketId() > 0){
				ps.setLong(7, vo.getMarketId());
			}else{
				ps.setString(7, null);
			}

			ps.setTimestamp(8, CommonUtil.convertToTimeStamp(CommonUtil.getDateTimeFormat(vo.getStartDate(), Utils.getAppData().getUtcOffset())));
			ps.setTimestamp(9, CommonUtil.convertToTimeStamp(CommonUtil.getDateTimeFormat(vo.getEndDate(), Utils.getAppData().getUtcOffset())));
			ps.setLong(10, vo.getWriterId());
			ps.setInt(11, vo.isActive()?1:0);

			ps.executeUpdate();
			vo.setId(getSeqCurValue(con,"SEQ_INV_LIMITS"));

		  }
			finally {
				closeStatement(ps);
			}

	  }

	/**
	 * Update user market disable table
	 * @param con db connection
	 * @param vo  user market disable instance for update
	 * @throws SQLException
	 */
	public static void updateInvestmentLimit(Connection con,InvestmentLimit vo) throws SQLException {

		PreparedStatement ps = null;

	  try {

			String sql = " Update investment_limits " +
						 " Set " +
						 	" opportunity_type_id = ?, " +
						 	" scheduled = ?, " +
						 	" start_date = ?, " +
						 	" end_date = ?, " +
						 	" time_created = sysdate, " +
						 	" writer_id = ?, " +
						 	" is_active = ? " +
						 " Where id = ? ";

			ps = con.prepareStatement(sql);

			if (vo.getOpportunityTypeId() > 0){
				ps.setLong(1, vo.getOpportunityTypeId());
			}else{
				ps.setString(1, null);
			}

			if (vo.getScheduled() > 0){
				ps.setLong(2, vo.getScheduled());
			}else{
				ps.setString(2, null);
			}

			ps.setTimestamp(3, CommonUtil.convertToTimeStamp(CommonUtil.getDateTimeFormat(vo.getStartDate(), Utils.getAppData().getUtcOffset())));
			ps.setTimestamp(4, CommonUtil.convertToTimeStamp(CommonUtil.getDateTimeFormat(vo.getEndDate(), Utils.getAppData().getUtcOffset())));
			ps.setLong(5, vo.getWriterId());
			ps.setInt(6, vo.isActive()?1:0);

			ps.setLong(7, vo.getId());

			ps.executeUpdate();

		  }	finally {
				closeStatement(ps);
			}
	  }


	public static long getSumInvestmentsByDateRangeAndUserId(Connection con, long userId, Date sumOfInvestmentsDateFrom, Date sumOfInvestmentsDateTo) throws Exception {
		PreparedStatement ps = null;
		ResultSet rs = null;
		long sumOfInvestments = 0;
		try {
			String sql = " SELECT " +
						 "		sum(i.amount) as sum_of_investments " +
						 " FROM " +
						 "		investments i " +
						 " WHERE " +
						 "		i.user_id = ? " +
						 "		AND i.time_created >= ? " +
					     " 		AND i.time_created <= ? ";

			ps = con.prepareStatement(sql);
			ps.setLong(1, userId);
			ps.setTimestamp(2, CommonUtil.convertToTimeStamp(CommonUtil.getDateTimeFormat(sumOfInvestmentsDateFrom,  Utils.getWriter().getUtcOffset())));
			ps.setTimestamp(3, CommonUtil.convertToTimeStamp(CommonUtil.getDateTimeFormat(sumOfInvestmentsDateTo,  Utils.getWriter().getUtcOffset())));
			rs = ps.executeQuery();
			if (rs.next()) {
				sumOfInvestments = rs.getLong("sum_of_investments");
			}
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		return sumOfInvestments;
	}


	public static ArrayList<String> getMarketAndSumOfInvestments(Connection con, long userId) throws Exception {
		PreparedStatement ps = null;
		ResultSet rs = null;
		ArrayList<String> list = new ArrayList<String>();
		try {
			String sql = " SELECT " +
						 "		* " +
						 " FROM " +
						 "		( " +
						 "		SELECT " +
						 "			 m.id, " +
						 "			 m.name, " +
						 "			 sum (i.amount) sum_of_investments, " +
						 "			 u.currency_id " +
						 " 		FROM " +
						 "			 investments i, " +
						 "			 opportunities o, " +
						 "			 markets m, " +
						 "			 users u " +
						 "		WHERE " +
						 "			 u.id = ? " +
						 "			 AND i.user_id = u.id " +
						 "			 AND i.opportunity_id = o.id " +
						 "			 AND o.market_id = m.id " +
						 "			 GROUP BY " +
						 "					m.id, " +
						 "					m.name, " +
						 "					u.currency_id " +
						 "			 ) " +
						 " ORDER BY " +
						 "			sum_of_investments DESC " ;

			ps = con.prepareStatement(sql);
			ps.setLong(1, userId);
			rs = ps.executeQuery();
			while (rs.next()) {
				list.add(rs.getString("name") + " " + CommonUtil.displayAmount(rs.getLong("sum_of_investments"), rs.getLong("currency_id")));
			}
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		return list;
	}


	public static ArrayList<SalesTurnoversTotals> getSalesTurnovers(Connection con, Date from, Date to, long writerId, long skinId, long currencyId, long salesTypeDepId) throws Exception {
		PreparedStatement ps = null;
		ResultSet rs = null;
		ArrayList<SalesTurnoversTotals> list = new ArrayList<SalesTurnoversTotals>();
		SalesTurnoversTotals stSum = new SalesTurnoversTotals();
		SalesTurnoversTotals vo;
//		FacesContext context = FacesContext.getCurrentInstance();
//		WriterWrapper w = (WriterWrapper) context.getApplication().createValueBinding(Constants.BIND_WRITER_WRAPPER).getValue(context);
//		long offset = Utils.getUtcOffsetValue(w.getUtcOffset());
//		DecimalFormat sd = new DecimalFormat("#0.00%");

//		long resCurrencyId;
//		String amountStr;
//		if (currencyId == ConstantsBase.ALL_FILTER_ID){
//			resCurrencyId = ConstantsBase.CURRENCY_USD_ID;
//			amountStr = " t.amount*t.rate ";
//		}else {
//			resCurrencyId = currencyId;
//			amountStr = " t.amount ";
//		}

		long retInvMadeCount = 0;
		long retInvMadeSum = 0;
		long retInvNumOfUsers = 0;

		long retTranMadeCount = 0;
		long retTranMadeSum = 0;
		long retTranNumOfUsers = 0;


		long retInvMadeCountTotal = 0;
		long retInvMadeSumTotal = 0;
		long retInvNumOfUsersTotal = 0;

		long retTranMadeCountTotal = 0;
		long retTranMadeSumTotal = 0;
		long retTranNumOfUsersTotal = 0;

		long retTranWithdrawMadeSum = 0;
		long retTranWithdrawMadeSumTotal = 0;
		
		try {
			String sql = "SELECT"  +
					 "		to_char(date_col, 'dd/mm/yyyy') day_date, " +
					 "		case when inv.sum_of_investments is null then 0 else inv.sum_of_investments end as sum_of_investments, " +
					 "		case when inv.num_of_investments is null then 0 else inv.num_of_investments end as num_of_investments, " +
					 "		case when inv.inv_num_of_users is null then 0 else inv.inv_num_of_users end as inv_num_of_users, " +
					 "		case when tran.sum_of_deposits is null then 0 else tran.sum_of_deposits end as sum_of_deposits, " +
					 "		case when tran.num_of_deposits is null then 0 else tran.num_of_deposits end as num_of_deposits, " +
					 "		case when tran.tran_num_of_users is null then 0 else tran.tran_num_of_users end as tran_num_of_users, " +
					 "		case when tran.sum_of_withdrawals is null then 0 else tran.sum_of_withdrawals end as sum_of_withdrawals " +
					 " FROM " +
					 "		(SELECT" +
					 "			 to_date(? ,'dd-mm-yyyy') + level - 1 as date_col " +
					 "			 from dual connect by level <= to_date(?,'dd/mm/yyyy') - to_date(?,'dd/mm/yyyy') + 1 " +
					 "		) dates_by_range " +
					 " LEFT JOIN " +
						 "		(SELECT " +
					 "			trunc(i.time_created) inv_date, " +
					 "			SUM(i.amount * i.rate * wci.factor) as sum_of_investments, " +
					 "			COUNT(i.id) as num_of_investments, " +
					 "			COUNT(distinct(i.user_id)) as inv_num_of_users " +
					 "		FROM " +
					 "			writers_commission_inv  wci, " +
					 "			investments i, " +
					 "			users u," +
					 "			writers w " +
					 "		WHERE " +
					 "			i.id = wci.investments_id AND " +
					 "			i.time_created >= ?  AND " +
					 "			i.time_created < ? + 1  AND " +
					 "			i.user_id = u.id AND " +
					 "			i.is_canceled = 0 AND " +
					 "          w.sales_type in (2,3) AND " +
					 "			w.id = wci.writer_id ";
		if (skinId > 0) {
			sql +=   "			AND u.skin_id = " + skinId ;
		}

		if (currencyId > 0) {
			sql +=   "			AND u.currency_id = " + currencyId;
		}

		if (writerId > 0) {
			sql +=   "			AND w.id = " + writerId;
		}
		
		if (salesTypeDepId > 0) {
			sql +=   "			AND w.sales_type_dept_id = " + salesTypeDepId;
		}
		sql +=		 " 		GROUP BY trunc(i.time_created) " +
					 "		) inv on (inv.inv_date = dates_by_range.date_col) " +
					 " LEFT JOIN 	" +
					 "		(SELECT " +
					 "			trunc(t.time_created) tran_date, " +
					 "			SUM( CASE WHEN tt.class_type = ? THEN (t.amount * t.rate) ELSE 0 END) AS sum_of_deposits, " +
					 "			COUNT(CASE WHEN tt.class_type = ? THEN t.id END) as num_of_deposits, " +
					 "			COUNT(distinct(CASE WHEN tt.class_type = ? THEN t.user_id END)) as tran_num_of_users, " +
					 "          SUM( CASE WHEN tt.class_type = ? THEN (t.amount * t.rate) ELSE 0 END) AS sum_of_withdrawals " +
					 "		FROM " +
					 "			writers_commission_dep  wcd, " +
					 "			transactions t, " +
					 "			transaction_types tt, " +
					 "			users u, " +
					 "			writers w " +
					 "		WHERE " +
					 "			t.id = wcd.transaction_id AND " +
					 "          t.id <> u.first_deposit_id  AND"+
					 "			t.time_created >= ?  AND " +
					 "			t.time_created < ? + 1  AND " +
					 "			t.user_id = u.id AND " +
					 "          t.status_id in (2,7,8) AND " +
					 "      	t.type_id = tt.id AND " +
					 "      	tt.class_type in (?,?) AND " +
					 "      	w.sales_type in (2,3) AND " +
					 "			w.id = wcd.writer_id ";
		if (skinId > 0) {
			sql +=   "			AND u.skin_id = " + skinId;
		}

		if (currencyId > 0) {
			sql +=   "			AND u.currency_id = " + currencyId;
		}

		if (writerId > 0) {
			sql +=   "			AND w.id = " + writerId;
		}
		
		if (salesTypeDepId > 0) {
			sql +=   "			AND w.sales_type_dept_id = " + salesTypeDepId;
		}
		sql +=		 "		GROUP BY trunc(t.time_created) " +
					 "		) tran  on  (tran.tran_date = dates_by_range.date_col) " +
					 " ORDER BY date_col ";

			ps = con.prepareStatement(sql);

			ps.setString(1, CommonUtil.getDateFormat(from));
			ps.setString(2, CommonUtil.getDateFormat(to));
			ps.setString(3, CommonUtil.getDateFormat(from));
			ps.setTimestamp(4, CommonUtil.convertToTimeStamp(from));
			ps.setTimestamp(5, CommonUtil.convertToTimeStamp(to));
			ps.setLong(6, TransactionsManagerBase.TRANS_CLASS_DEPOSIT);
			ps.setLong(7, TransactionsManagerBase.TRANS_CLASS_DEPOSIT);
			ps.setLong(8, TransactionsManagerBase.TRANS_CLASS_DEPOSIT);
			ps.setLong(9, TransactionsManagerBase.TRANS_CLASS_WITHDRAW);
			ps.setTimestamp(10, CommonUtil.convertToTimeStamp(from));
			ps.setTimestamp(11, CommonUtil.convertToTimeStamp(to));
			ps.setLong(12, TransactionsManagerBase.TRANS_CLASS_DEPOSIT);
			ps.setLong(13, TransactionsManagerBase.TRANS_CLASS_WITHDRAW);


			rs = ps.executeQuery();
			while (rs.next()) {
				retInvMadeCount = rs.getLong("num_of_investments");
				retInvMadeSum = rs.getLong("sum_of_investments");
				retInvNumOfUsers = rs.getLong("inv_num_of_users");
				retTranMadeCount = rs.getLong("num_of_deposits");
				retTranMadeSum = rs.getLong("sum_of_deposits");
				retTranNumOfUsers = rs.getLong("tran_num_of_users");
				retTranWithdrawMadeSum = rs.getLong("sum_of_withdrawals");

				vo = new SalesTurnoversTotals();
				vo.setDate(rs.getString("day_date"));

				if (retInvMadeCount > 0){
					vo.setRetInvMadeCount(String.valueOf(retInvMadeCount));
					retInvMadeCountTotal += retInvMadeCount;
				}else{
					vo.setRetInvMadeCount(ConstantsBase.EMPTY_STRING);
				}

				if (retInvMadeSum > 0){
					vo.setRetInvMadeSum(CommonUtil.displayAmount(retInvMadeSum, ConstantsBase.CURRENCY_USD_ID));
					retInvMadeSumTotal += retInvMadeSum;
				}else{
					vo.setRetInvMadeSum(ConstantsBase.EMPTY_STRING);
				}

				if (retInvNumOfUsers > 0){
					vo.setRetInvNumOfUsers(String.valueOf(retInvNumOfUsers));
					retInvNumOfUsersTotal += retInvNumOfUsers;
				}else{
					vo.setRetInvNumOfUsers(ConstantsBase.EMPTY_STRING);
				}

				if (retTranMadeCount > 0){
					vo.setRetTranMadeCount(String.valueOf(retTranMadeCount));
					retTranMadeCountTotal += retTranMadeCount;
				}else{
					vo.setRetTranMadeCount(ConstantsBase.EMPTY_STRING);
				}

				if (retTranMadeSum > 0){
					vo.setRetTranMadeSum(CommonUtil.displayAmount(retTranMadeSum, ConstantsBase.CURRENCY_USD_ID));
					retTranMadeSumTotal += retTranMadeSum;
				}else{
					vo.setRetTranMadeSum(ConstantsBase.EMPTY_STRING);
				}

				if (retTranNumOfUsers > 0){
					vo.setRetTranNumOfUsers(String.valueOf(retTranNumOfUsers));
					retTranNumOfUsersTotal += retTranNumOfUsers;
				}else{
					vo.setRetTranMadeSum(ConstantsBase.EMPTY_STRING);
				}
				// Sum of withdrawals
				if (retTranWithdrawMadeSum > 0) {
					vo.setRetTranWithdrawMadeSum(CommonUtil.displayAmount(retTranWithdrawMadeSum, ConstantsBase.CURRENCY_USD_ID));
					retTranWithdrawMadeSumTotal += retTranWithdrawMadeSum;
				} else {
					vo.setRetTranWithdrawMadeSum(ConstantsBase.EMPTY_STRING);
				}
				// sum withdrawals divided by sum deposits
				vo.setRetTranWithdrawDepMadeSum(Utils.getPercentAfterDevided(retTranWithdrawMadeSum, retTranMadeSum));
				list.add(vo);
			}

			stSum.setDate(Constants.EMPTY_STRING);
			stSum.setRetInvMadeCount(String.valueOf(retInvMadeCountTotal));
			stSum.setRetInvMadeSum(CommonUtil.displayAmount(retInvMadeSumTotal, ConstantsBase.CURRENCY_USD_ID));
			stSum.setRetInvNumOfUsers(String.valueOf(retInvNumOfUsersTotal));
			stSum.setRetTranMadeCount(String.valueOf(retTranMadeCountTotal));
			stSum.setRetTranMadeSum(CommonUtil.displayAmount(retTranMadeSumTotal, ConstantsBase.CURRENCY_USD_ID));
			stSum.setRetTranNumOfUsers(String.valueOf(retTranNumOfUsersTotal));
			stSum.setRetTranWithdrawMadeSum(CommonUtil.displayAmount(retTranWithdrawMadeSumTotal, ConstantsBase.CURRENCY_USD_ID));
			stSum.setRetTranWithdrawDepMadeSum(Utils.getPercentAfterDevided(retTranWithdrawMadeSumTotal, retTranMadeSumTotal));
			list.add(stSum);
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		return list;
	}


	public static ArrayList<SalesTurnoversDetails> getSalesTurnoverDetails(Connection con, Date fromDetails, Date toDetails, long currencyIdDetails, long writerIdDetails, long skinIdDetails, long salesTypeDepIdDetails) throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		ArrayList<SalesTurnoversDetails> list = new ArrayList<SalesTurnoversDetails>();
//		SalesTurnoversDetails stSum = new SalesTurnoversDetails();
		SalesTurnoversDetails vo;
//		FacesContext context = FacesContext.getCurrentInstance();
//		WriterWrapper w = (WriterWrapper) context.getApplication().createValueBinding(Constants.BIND_WRITER_WRAPPER).getValue(context);
//		long offset = Utils.getUtcOffsetValue(w.getUtcOffset());
		int index = 1 ;
		long totalTurnover = 0;
		long totalDeposits = 0;
		long totalWithdraw = 0;
//		DecimalFormat sd = new DecimalFormat("#0.00%");


		try {
			String sql = " SELECT * " +
						 "		FROM ( " +
						 "			SELECT " +
						 "				w2.user_name as writer_name, " +
						 "				u.skin_id user_skin_id, " +
						 "				u.id as user_id, " +
						 "				tran.sum_of_deposits, " +
						 "				tran.sum_of_withdrawals, " +
						 "				inv.sum_of_investments, " +
						 "				mca.name as campaign_name, " +
						 "				u.currency_id, " +
						 "				p.name as population_name, " +
						 "				ur.rank_name, " +
						 "				us.status_name " +
						 "			FROM " +
						 "				users u " +
						 "				LEFT JOIN " +
						 "						(SELECT " +
                         "    						SUM(i.amount * i.rate * wci.factor) as sum_of_investments, " +
                         "   						u.id as user_id, " +
                         "   						w.id as writer_id " +
                         "						 FROM " +
                         "							writers_commission_inv  wci, " +
                         "   						investments i, " +
                         "   						users u, " +
                         "   						writers w " +
                         "						 WHERE " +
                         "	 					    i.id = wci.investments_id AND " +
                         "   						i.time_created >= ? AND " +
                         "   						i.time_created < ? + 1 AND " +
                         "						    i.user_id = u.id AND " +
                         "   						i.is_canceled = 0 AND " +
                         "						    w.sales_type in (2,3) AND " +
                         "   						w.id = wci.writer_id " +
(skinIdDetails != 0 ?    " 							AND u.skin_id = ? ": "") +
(currencyIdDetails != 0 ?" 							AND u.currency_id = ? " : "" ) +
(writerIdDetails != 0 ?  " 							AND w.id = ? " : "" ) +
(salesTypeDepIdDetails > 0 ?"                       AND w.sales_type_dept_id = ? " : "" ) +
                         "   					 GROUP BY u.id, w.id) inv on inv.user_id = u.id " +
                         "				LEFT JOIN " +
                         "						(SELECT " +
                         "   						SUM( CASE WHEN tt.class_type = ? THEN (t.amount * t.rate) ELSE 0 END) AS sum_of_deposits, " +
    					 "          				SUM( CASE WHEN tt.class_type = ? THEN (t.amount * t.rate) ELSE 0 END) AS sum_of_withdrawals, " +
                         "   						u.id as user_id, " +
                         "						    w.id as writer_id " +
                         "						 FROM 			" +
                         "							writers_commission_dep wcd, " +
                         "   						transactions t, " +
                         "						    transaction_types tt, " +
                         "   						users u, " +
                         "   						writers w " +
                         "						 WHERE 		" +
                         "							t.id = wcd.transaction_id AND " +
                         "   						t.time_created >= ?  AND " +
                         "   						t.time_created < ? + 1 AND " +
                         "							t.user_id = u.id AND " +
                         "   						t.status_id in (2,7,8) AND " +
                         "						    t.type_id = tt.id AND " +
                         "   						tt.class_type in (?,?) AND " +
                         "   						w.sales_type in (2,3) AND " +
                         "						    w.id = wcd.writer_id " +
(skinIdDetails != 0 ?    " 							AND u.skin_id = ?  ": "") +
(currencyIdDetails != 0 ?" 							AND u.currency_id = ? " : "" ) +
(writerIdDetails != 0 ?  " 							AND w.id = ? " : "" ) +
(salesTypeDepIdDetails > 0 ?"                       AND w.sales_type_dept_id = ? " : "" ) +
                         "   					 GROUP BY u.id, w.id) tran ON tran.user_id = u.id " +
                         "				LEFT JOIN writers w2 on w2.id = tran.writer_id OR w2.id = inv.writer_id " +
                         "				LEFT JOIN users_rank ur ON u.rank_id = ur.id " +
                         "				LEFT JOIN users_status us ON u.status_id = us.id " +
                         "				LEFT JOIN marketing_combinations mco ON u.combination_id = mco.id " +
                         "				LEFT JOIN marketing_campaigns mca ON mco.campaign_id = mca.id " +
                         "				LEFT JOIN population_users pu ON pu.user_id = u.id " +
                         "				LEFT JOIN population_entries pe ON pe.id = pu.curr_population_entry_id " +
                         "				LEFT JOIN populations p ON p.id = pe.population_id " +
                         " 			WHERE u.first_deposit_id is not null) " +
                         "	WHERE " +
                         "		(sum_of_deposits is not null OR sum_of_investments is not null) " ;

			ps = con.prepareStatement(sql);
			ps.setTimestamp(index++, CommonUtil.convertToTimeStamp(fromDetails));
			ps.setTimestamp(index++, CommonUtil.convertToTimeStamp(toDetails));

			if(skinIdDetails != 0 ){
				ps.setLong(index++, skinIdDetails);
			}
			if(currencyIdDetails != 0){
				ps.setLong(index++, currencyIdDetails);
			}
			if(writerIdDetails != 0 ){
				ps.setLong(index++, writerIdDetails);
			}
			if (salesTypeDepIdDetails > 0) {
				ps.setLong(index++, salesTypeDepIdDetails);
			}

			ps.setLong(index++, TransactionsManagerBase.TRANS_CLASS_DEPOSIT);
			ps.setLong(index++, TransactionsManagerBase.TRANS_CLASS_WITHDRAW);
			ps.setTimestamp(index++, CommonUtil.convertToTimeStamp(fromDetails));
			ps.setTimestamp(index++, CommonUtil.convertToTimeStamp(toDetails));
			ps.setLong(index++, TransactionsManagerBase.TRANS_CLASS_DEPOSIT);
			ps.setLong(index++, TransactionsManagerBase.TRANS_CLASS_WITHDRAW);

			if(skinIdDetails != 0 ){
				ps.setLong(index++, skinIdDetails);
			}
			if(currencyIdDetails != 0){
				ps.setLong(index++, currencyIdDetails);
			}
			if(writerIdDetails != 0 ){
				ps.setLong(index++, writerIdDetails);
			}
			if (salesTypeDepIdDetails > 0) {
				ps.setLong(index++, salesTypeDepIdDetails);
			}
			rs = ps.executeQuery();

			while (rs.next()) {
				vo = new SalesTurnoversDetails();
				vo.setWriterName(rs.getString("writer_name"));
				vo.setSkinId(rs.getLong("user_skin_id"));
				vo.setUserId(rs.getLong("user_id"));
				vo.setSumOfDeposits(rs.getLong("sum_of_deposits"));
				vo.setSumOfTurnovers(rs.getLong("sum_of_investments"));
				vo.setCampaignId(rs.getString("campaign_name"));
				vo.setCurrencyId(rs.getLong("currency_id"));
				vo.setPopulationName(rs.getString("population_name"));
				vo.setUserRankName(rs.getString("rank_name"));
				vo.setUserStatusName(rs.getString("status_name"));
				long sumOfWithdraw = rs.getLong("sum_of_withdrawals");
				vo.setSumOfWithdrawals(sumOfWithdraw);
				vo.setWithdrawDepPercent(Utils.getPercentAfterDevided(sumOfWithdraw, rs.getLong("sum_of_deposits")));

				if(rs.getLong("sum_of_deposits") != 0){
					totalDeposits += rs.getLong("sum_of_deposits");
				}
				if(rs.getLong("sum_of_investments")!= 0){
					totalTurnover += rs.getLong("sum_of_investments");
				}
				if(sumOfWithdraw != 0){
					totalWithdraw += sumOfWithdraw;
				}

				list.add(vo);
			}
			vo = new SalesTurnoversDetails();
			if(currencyIdDetails != 0){
				vo.setCurrencyId(currencyIdDetails);
			} else {
				vo.setCurrencyId(Constants.CURRENCY_BASE_ID);
			}
			vo.setSumOfDeposits(totalDeposits);
			vo.setSumOfTurnovers(totalTurnover);
			vo.setSumOfWithdrawals(totalWithdraw);
			vo.setWithdrawDepPercent(Utils.getPercentAfterDevided(totalWithdraw, totalDeposits));
			
			list.add(vo);

		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		return list;
	}
}