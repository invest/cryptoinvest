package il.co.etrader.backend.dao_managers;

import il.co.etrader.bl_vos.MarketingTerritory;
import il.co.etrader.util.CommonUtil;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.faces.model.SelectItem;

import com.anyoption.common.daos.DAOBase;



public class MarketingTerritoriesDAO extends DAOBase {


	  /**
	   * Get territories for marketing
	   * @throws SQLException
	   */
	  public static ArrayList<SelectItem> getList(Connection con) throws SQLException {

		  PreparedStatement ps = null;
		  ResultSet rs = null;
		  ArrayList<SelectItem> list = new ArrayList<SelectItem>();

		  try {

				String sql = "select id, territory " +
							 "from marketing_territories " +
							 "order by upper(territory)";

				ps = con.prepareStatement(sql);
				rs = ps.executeQuery();

				while (rs.next()) {
					list.add(new SelectItem(new Long(rs.getLong("id")),rs.getString("territory")));
				}

			} finally {
					closeResultSet(rs);
					closeStatement(ps);
			}

			return list;
	  }
	  
	  /**
	   * Get territories for marketing by WriterSkinId
	   * @throws SQLException
	   */
	  public static ArrayList<SelectItem> getListWriterSkinId(Connection con, long writerIdForSkin) throws SQLException {

		  PreparedStatement ps = null;
		  ResultSet rs = null;
		  ArrayList<SelectItem> list = new ArrayList<SelectItem>();

		  try {

				String sql = "select id, territory " +
							 " from marketing_territories " +
							 " where writer_id in (select distinct writer_id from " +
								" writers_skin where skin_id in " +
								" (select skin_id from writers_skin where writer_id =" + writerIdForSkin + ")) " +
							 "order by upper(territory)";

				ps = con.prepareStatement(sql);
				rs = ps.executeQuery();

				while (rs.next()) {
					list.add(new SelectItem(new Long(rs.getLong("id")),rs.getString("territory")));
				}

			} finally {
					closeResultSet(rs);
					closeStatement(ps);
			}

			return list;
	  }

		/**
		 * Insert a new territory
		 * @param con  db connection
		 * @param vo MarketingTerritory instance
		 * @throws SQLException
		 */
		public static void insert(Connection con,MarketingTerritory vo) throws SQLException {

			  PreparedStatement ps = null;
			  ResultSet rs = null;

			  try {

					String sql = "insert into marketing_territories(id,territory,writer_id,time_created) " +
							     "values(seq_mar_territories.nextval,?,?,sysdate)";

					ps = con.prepareStatement(sql);

					ps.setString(1, vo.getTerritory());
					ps.setLong(2, vo.getWriterId());

					ps.executeUpdate();

					vo.setId(getSeqCurValue(con,"seq_mar_territories"));
			  }
			  finally {
					closeStatement(ps);
					closeResultSet(rs);
				}

		  }

		  /**
		   * Update territory
		   * @param con db connection
		   * @param vo  MarketingTerritory instance
		   * @throws SQLException
		   */
		  public static void update(Connection con,MarketingTerritory vo) throws SQLException {

			  PreparedStatement ps = null;
			  ResultSet rs = null;

			  try {

				  String sql = "update marketing_territories set territory=?,writer_id=? " +
				  			   "where id=?";

					ps = con.prepareStatement(sql);


					ps.setString(1, vo.getTerritory());
					ps.setLong(2, vo.getWriterId());
					ps.setLong(3, vo.getId());

					ps.executeUpdate();

			  }
			  finally	{
					closeStatement(ps);
					closeResultSet(rs);
				}

		  }


		  /**
		   * Get all territory
		   * @param con  db connection
		   * @param name  territory name filter
		   * @return
		   *	ArrayList of MarketingTerritory
		   * @throws SQLException
		   */
		  public static ArrayList<MarketingTerritory> getAll(Connection con, String name, long writerIdForSkin) throws SQLException {

			  PreparedStatement ps = null;
			  ResultSet rs = null;
			  ArrayList<MarketingTerritory> list = new ArrayList<MarketingTerritory>();

			  try {

					String sql = "select * " +
								 "from marketing_territories where 1=1 ";

					if ( !CommonUtil.isParameterEmptyOrNull(name) ) {
						sql += "and upper(territory) like '%" + name.toUpperCase() + "%' ";
					}
					
					if (writerIdForSkin > 0) { 
						sql += 	" and writer_id in (select distinct writer_id from " +
													" writers_skin where skin_id in " +
														" (select skin_id from writers_skin where writer_id ="+ writerIdForSkin +")) ";
					}

					sql += "order by id";

					ps = con.prepareStatement(sql);
					rs = ps.executeQuery();

					while (rs.next()) {
						MarketingTerritory vo = getVO(rs);
						list.add(vo);
					}

				} finally {
						closeResultSet(rs);
						closeStatement(ps);
				}

				return list;
		  }


		  /**
		   * Check if territory name in use
		   * @param con   db connection
		   * @param name  territory
		   * @return true if name in use
		   * @throws SQLException
		   */
		  public static boolean isNameInUse(Connection con, String name, long id) throws SQLException {

			  ResultSet rs = null;
			  PreparedStatement ps = null;

			  try {

				  String sql = "select * " +
				  			   "from marketing_territories " +
				  			   "where territory like ? ";

				  if ( id > 0 ) {  // for update
					  sql += "and id <> " + id;
				  }

				  ps = con.prepareStatement(sql);
				  ps.setString(1, name);

				  rs = ps.executeQuery();

				  if ( rs.next() ) {
					  return true;
				  }
			  } finally {
				  closeResultSet(rs);
				  closeStatement(ps);
			  }

			  return false;

		  }

		  /**
		   * Get VO
		   * @param rs
		   * 	Result set instance
		   * @return
		   * 	MarketingTerritory object
		   * @throws SQLException
		   */
		  private static MarketingTerritory getVO(ResultSet rs) throws SQLException {

			  MarketingTerritory vo = new MarketingTerritory();

				vo.setId(rs.getLong("id"));
				vo.setTerritory(rs.getString("territory"));
				vo.setTimeCreated(convertToDate(rs.getTimestamp("time_created")));
				vo.setWriterId(rs.getLong("writer_id"));

				return vo;
		  }



}

