package il.co.etrader.backend.dao_managers;

import il.co.etrader.bl_vos.ConstantInvestmentsStream;
import il.co.etrader.dao_managers.ConstantInvestmentsStreamDAOBase;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class ConstantInvestmentsStreamDAO extends ConstantInvestmentsStreamDAOBase{
	
	public static void insertConstantInvestmentsStream(Connection con,
			ConstantInvestmentsStream vo) throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
				String sql = " insert into CONSTANT_INVESTMENTS_STREAMS " +
						         "(id," +
						         " stream_name," +
						         " investment_min_amount, " +
						         " is_active, ";
						        if (vo.getSkinId()>0){
						        	sql +=" skin_id, ";
						        }
						        sql +=  " max_rows) " + 
			                  " values" +
			                    "( SEQ_CONSTANT_INVESTMENTS_STR.nextval," +
			                    "  ?, " +
			                    "  ?," +
			                    "  ?,";
			                    if (vo.getSkinId()>0){  
			                    	sql +="  ?,?)";
			                    }else{
			                    sql += "  ?) ";
			                    }
				ps = con.prepareStatement(sql);
				ps.setString(1, vo.getStreamName());		
				ps.setLong(2, vo.getInvestmentMinAmount());
				ps.setInt(3, vo.isActive() ? 1 : 0);
				 if (vo.getSkinId()>0){
					 ps.setLong(4, vo.getSkinId());
					 ps.setLong(5, vo.getMaxRows());
				 }else{
					 ps.setLong(4, vo.getMaxRows());
				 }
				ps.executeUpdate();
	
				vo.setId(getSeqCurValue(con, "SEQ_CONSTANT_INVESTMENTS_STR"));
			} finally {
			closeStatement(ps);
			closeResultSet(rs);
		}
	}

	public static void updateConstantInvestmentsStream(Connection con,
			ConstantInvestmentsStream vo) throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			String sql = " update CONSTANT_INVESTMENTS_STREAMS " + 
		                 " set " +
		                 " stream_name=? ," +
			               " investment_min_amount=? ," +
			              " max_rows=? , " +
			              " is_active=? " ;
		           if (vo.getSkinId()>0){
		        	   sql += ", skin_id=  " + vo.getSkinId();  
		           }else{
		        	   sql += ", skin_id= null ";  
		           }
		              sql +=  " where id=? ";
			ps = con.prepareStatement(sql);
			ps.setString(1, vo.getStreamName());			
			ps.setLong(2, vo.getInvestmentMinAmount());
			ps.setLong(3, vo.getMaxRows());
			ps.setInt(4, vo.isActive() ? 1 : 0);
			ps.setLong(5, vo.getId());
			ps.executeUpdate();
		} finally {
			closeStatement(ps);
			closeResultSet(rs);
		}
	}
	
	public static boolean isExistStreamName(Connection con,
			ConstantInvestmentsStream lc) throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;

		try {
			String sql = " select * from constant_investments_streams where upper(stream_name)= ? ";

			ps = con.prepareStatement(sql);
			ps.setString(1, lc.getStreamName().toUpperCase());
			rs = ps.executeQuery();
			if (rs.next()) {
				return true;
			}
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		return false;
	}
}