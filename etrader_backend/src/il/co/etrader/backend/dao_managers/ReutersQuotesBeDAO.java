package il.co.etrader.backend.dao_managers;

import il.co.etrader.backend.bl_vos.ReutersQuotesBE;
import il.co.etrader.bl_vos.Skins;
import il.co.etrader.util.CommonUtil;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;

import org.apache.log4j.Logger;

import com.anyoption.common.daos.DAOBase;
import com.anyoption.common.managers.MarketsManagerBase;

public class ReutersQuotesBeDAO extends DAOBase {
	//TODO: extends ReutersQuotesBeDAOBase
	private static final Logger logger = Logger.getLogger(ReutersQuotesBeDAO.class);


	public static ArrayList<ReutersQuotesBE> getAllData(Connection con, long marketId, long scheduledId, Date from, Date to) throws SQLException {
		int index = 1;
		PreparedStatement ps = null;
		ResultSet rs = null;
		ArrayList<ReutersQuotesBE> list = new ArrayList<ReutersQuotesBE>();

		try {
			String sql =	" SELECT " +
			  					" m.display_name AS display_name_market, " +
			  					" m.id AS market_id, " +
			  					" o.time_est_closing, " +
			  					" o.scheduled, " +
			  					" m.decimal_point, " +
			  					" rq.* " +
			  				" FROM " +
			  			  		" reuters_quotes rq, " +
			  			  		" opportunities o, " +
			  			  		" markets m " +
			  			  	" WHERE " +
			  			  		" rq.opportunity_id = o.id " +
			  			  		" AND o.market_id = m.id " +
			  			  		" AND SYS_EXTRACT_UTC(o.time_est_closing) between (?) and (?) ";
			  			if (marketId > 0) {
			  				sql +=	" AND m.id = ? ";
			  			}
			  			if (scheduledId > 0) {
			  				sql +=	" AND o.scheduled = ? ";
			  			}

		  			sql += " ORDER BY " +
			  					" o.time_est_closing DESC, " +
			  					" m.display_name, " +
			  					" rq.time ";
 			ps = con.prepareStatement(sql);
			ps.setTimestamp(index++, CommonUtil.convertToTimeStamp(from));
  			ps.setTimestamp(index++, CommonUtil.convertToTimeStamp(to));
			if (marketId > 0) {
				ps.setLong(index++, marketId);
  			}
  			if (scheduledId > 0) {
  				ps.setLong(index++, scheduledId);
  			}

			rs = ps.executeQuery();
			while (rs.next()) {
				ReutersQuotesBE reutersQuotesBE = new ReutersQuotesBE();
				reutersQuotesBE.setId(rs.getLong("id"));
				reutersQuotesBE.setAsk(rs.getBigDecimal("ask"));
				reutersQuotesBE.setBid(rs.getBigDecimal("bid"));
				reutersQuotesBE.setLast(rs.getBigDecimal("last"));
				reutersQuotesBE.setTime(rs.getTimestamp("time"));
				reutersQuotesBE.setOpportunityId(rs.getLong("opportunity_id"));
				reutersQuotesBE.setScheduled(rs.getLong("scheduled"));
				reutersQuotesBE.settimeEstClosing(rs.getTimestamp("time_est_closing"));
				reutersQuotesBE.setMarketName(MarketsManagerBase.getMarketName(Skins.SKIN_REG_EN, rs.getLong("market_id")));
				reutersQuotesBE.setDecimalPoint(rs.getInt("decimal_point"));
				list.add(reutersQuotesBE);
			}
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		  return list;
	}//getAllData.
}
