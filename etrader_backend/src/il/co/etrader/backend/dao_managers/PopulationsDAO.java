package il.co.etrader.backend.dao_managers;

import il.co.etrader.backend.bl_vos.PopulationEntry;
import il.co.etrader.backend.bl_vos.WriterWrapper;
import il.co.etrader.backend.util.Constants;
import il.co.etrader.backend.util.Utils;
import il.co.etrader.bl_managers.PopulationsManagerBase;
import il.co.etrader.bl_vos.Population;
import il.co.etrader.bl_vos.PopulationType;
import il.co.etrader.bl_vos.Writer;
import il.co.etrader.dao_managers.PopulationsDAOBase;
import il.co.etrader.dao_managers.WritersDAO;
import il.co.etrader.util.CommonUtil;
import il.co.etrader.util.ConstantsBase;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;

import javax.faces.model.SelectItem;

import org.apache.log4j.Logger;

public class PopulationsDAO extends PopulationsDAOBase {

	private static final Logger logger = Logger.getLogger(PopulationsDAO.class);


	public static void insert(Connection con, Population vo) throws SQLException {
		  PreparedStatement ps = null;
		  ResultSet rs = null;

		  try {

				String sql = "insert into populations(id, name, SKIN_ID, LANGUAGE_ID, start_date,end_date,THRESHOLD," +
						     "NUM_ISSUES,REFRESH_TIME_PERIOD, NUMBER_OF_MINUTES, NUMBER_OF_DAYS, writer_id, time_created," +
							 "BALANCE_AMOUNT, LAST_INVEST_DAY, POPULATION_TYPE_ID, IS_ACTIVE, number_of_deposits) " +
						     "values(seq_mar_campaigns.nextval,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

				ps = con.prepareStatement(sql);



				ps.setString(1, vo.getName());
				ps.setLong(2, vo.getSkinId());
				ps.setLong(3, vo.getLanguageId());
				ps.setTimestamp(4, CommonUtil.convertToTimeStamp(CommonUtil.getDateTimeFormat(vo.getStartDate(), Utils.getAppData().getUtcOffset())));
				if ( null == vo.getEndDate() ) {
					ps.setTimestamp(5, null);
				} else {
					ps.setTimestamp(5, CommonUtil.convertToTimeStamp(CommonUtil.getDateTimeFormat(vo.getEndDate(), Utils.getAppData().getUtcOffset())));
				}
				ps.setInt(6, vo.getThreshold());
				ps.setLong(7, vo.getNumIssues());
				ps.setLong(8, vo.getRefreshTimePeriod());
				ps.setInt(9, vo.getNumberOfMinutes());
				ps.setInt(10, vo.getNumberOfDays());
				ps.setInt(11, vo.getWriterId());
				ps.setTimestamp(12, CommonUtil.convertToTimeStamp(CommonUtil.getDateTimeFormat(vo.getTimeCreated(), Utils.getAppData().getUtcOffset())));
				ps.setLong(13, vo.getBalanceAmount());
				ps.setInt(14, vo.getLastInvestDay());
				ps.setLong(15, vo.getPopulationTypeId());
				ps.setBoolean(16, vo.getIsActive());
				ps.setInt(17, vo.getNumberOfDeposits());


				ps.executeUpdate();


		  }
		  finally {
				closeStatement(ps);
				closeResultSet(rs);
			}


	}

	public static void update(Connection con, Population vo)throws SQLException {
		  PreparedStatement ps = null;
		  ResultSet rs = null;

		  try {

				String sql = "update populations set name = ?, SKIN_ID = ?, LANGUAGE_ID = ?, start_date = ?, end_date = ?, THRESHOLD = ?," +
						     "NUM_ISSUES = ?, REFRESH_TIME_PERIOD = ?, NUMBER_OF_MINUTES = ?, NUMBER_OF_DAYS = ?, " +
							 "BALANCE_AMOUNT = ?, LAST_INVEST_DAY = ?, POPULATION_TYPE_ID = ?, IS_ACTIVE = ?, " +
						     "number_of_deposits = ? where id = ?";

				ps = con.prepareStatement(sql);



				ps.setString(1, vo.getName());
				ps.setLong(2, vo.getSkinId());
				ps.setLong(3, vo.getLanguageId());
				ps.setTimestamp(4, CommonUtil.convertToTimeStamp(CommonUtil.getDateTimeFormat(vo.getStartDate(), Utils.getAppData().getUtcOffset())));
				if ( null == vo.getEndDate() ) {
					ps.setTimestamp(5, null);
				} else {
					ps.setTimestamp(5, CommonUtil.convertToTimeStamp(CommonUtil.getDateTimeFormat(vo.getEndDate(), Utils.getAppData().getUtcOffset())));
				}
				ps.setInt(6, vo.getThreshold());
				ps.setLong(7, vo.getNumIssues());
				ps.setLong(8, vo.getRefreshTimePeriod());
				ps.setInt(9, vo.getNumberOfMinutes());
				ps.setInt(10, vo.getNumberOfDays());
				ps.setLong(11, vo.getBalanceAmount());
				ps.setInt(12, vo.getLastInvestDay());
				ps.setLong(13, vo.getPopulationTypeId());
				ps.setBoolean(14, vo.getIsActive());
				ps.setInt(15, vo.getNumberOfDeposits());
				ps.setLong(16, vo.getId());

				ps.executeUpdate();


		  }
		  finally {
				closeStatement(ps);
				closeResultSet(rs);
			}

	}

	/**
	 * get populationsType HashMap
	 * @param con
	 * @return
	 * @throws SQLException
	 */
	public static HashMap<Long, PopulationType> getPopulationTypesHash(Connection con) throws SQLException {
		HashMap<Long, PopulationType> list = new HashMap<Long, PopulationType>();
		PreparedStatement ps = null;
		ResultSet rs = null;

		try {

			String sql = "select * from population_types";

			ps = con.prepareStatement(sql);
			rs = ps.executeQuery();

			while (rs.next()) {
				PopulationType vo = getPopulationTypesVO(rs);
				vo.setBonusLimitTypeId(rs.getInt("bonus_limit_type_id"));
				list.put(rs.getLong("id"), vo);
			}

		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		return list;
	}

	/**
	 * get PopulationType object
	 *
	 * @param rs
	 *            ResultSet instance
	 * @return
	 * @throws SQLException
	 */
	protected static PopulationType getPopulationTypesVO(ResultSet rs) throws SQLException {

		PopulationType vo = new PopulationType();
		vo.setId(rs.getLong("ID"));
		vo.setName(rs.getString("NAME"));
		vo.setRefreshPeriod(rs.getInt("refresh_Period"));
		vo.setRefreshDays(rs.getInt("refresh_Days"));
		vo.setTypeId(rs.getInt("type_id"));

		return vo;
	}

	/**
	 * get populationsType
	 * @param con
	 * @return
	 * @throws SQLException
	 */
	public static ArrayList<Population> getPopulationsForWriter(Connection con) throws SQLException {
		ArrayList<Population> list = new ArrayList<Population>();
		PreparedStatement ps = null;
		ResultSet rs = null;

		try {
			String skins = Utils.getWriter().getSkinsToString();

			String sql =
				"SELECT * " +
				"FROM populations " +
				"WHERE " +
					"skin_id in (" + skins +") " +
					"AND is_active = 1 " +
				"ORDER BY skin_id, name ";

			ps = con.prepareStatement(sql);
			rs = ps.executeQuery();

			while (rs.next()) {
				list.add(getPopulationsVO(rs));
			}

		} catch (SQLException ex){
			logger.error("Error: ", ex);

		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		return list;
	}

	/**
	 * Get all retention users
	 * @param con
	 * @return
	 * @throws SQLException
	 */
	public static HashMap<Long, Writer> getRetentionWriters(Connection con) throws SQLException {
		HashMap<Long, Writer> list = new HashMap<Long, Writer>();
		PreparedStatement ps = null;
		ResultSet rs = null;

		try {
			String sql = "SELECT " +
							"distinct w.* " +
						"FROM " +
					        "writers w, roles r " +
					    "WHERE " +
					        "w.user_name = r.user_name AND " +
					        "w.is_active = 1 AND " +
					        "(r.role like ? or r.role like ?) " +
					    "ORDER BY w.user_name " ;

			ps = con.prepareStatement(sql);
			ps.setString(1, Constants.ROLE_RETENTION);
			ps.setString(2, Constants.ROLE_RETENTIONM);

			rs = ps.executeQuery();

			while (rs.next()) {
				Writer w = new Writer();
				w.setId(rs.getLong("id"));
				w.setUserName(rs.getString("user_name"));
				w.setSkins(WritersDAO.getWriterSkins(con, w.getId()));
				list.put(new Long(w.getId()), w);
			}
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}

		return list;
	}


	/**
	 * Get all retention users sorted as array list
	 * @param con
	 * @return
	 * @throws SQLException
	 */
	public static ArrayList<Writer> getRetentionWritersList(Connection con) throws SQLException {
		ArrayList<Writer> list = new ArrayList<Writer>();
		PreparedStatement ps = null;
		ResultSet rs = null;

		try {
			String sql = "SELECT " +
							"distinct w.* " +
						"FROM " +
					        "writers w, roles r " +
					    "WHERE " +
					        "w.user_name = r.user_name AND " +
					        "w.is_active = 1 AND " +
					        "(r.role like ? or r.role like ?) " +
					    "ORDER BY w.user_name " ;

			ps = con.prepareStatement(sql);
			ps.setString(1, Constants.ROLE_RETENTION);
			ps.setString(2, Constants.ROLE_RETENTIONM);

			rs = ps.executeQuery();

			while (rs.next()) {
				Writer w = new Writer();
				w.setId(rs.getLong("id"));
				w.setUserName(rs.getString("user_name"));
				w.setSkins(WritersDAO.getWriterSkins(con, w.getId()));
				list.add(w);
			}
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}

		return list;
	}

	/**
	 * Get all Sales users from RETENTION DEPARTMENT
	 * @param con
	 * @return
	 * @throws SQLException
	 */

	public static HashMap<Long, Writer> getSalesWriters(Connection con,String skins) throws SQLException {
		HashMap<Long, Writer> list = new HashMap<Long, Writer>();
		PreparedStatement ps = null;
		ResultSet rs = null;

		try {
			String sql ="SELECT " +
							"distinct w.* " +
						"FROM " +
					        "writers w, roles r " +
					    "WHERE " +
					        "w.user_name = r.user_name AND " +
					        "w.is_active = 1 AND " +
					        "(r.role like ? or r.role like ?) " +
					        "AND w.dept_id = ? ";

					        if (null != skins) {
						        sql += "AND EXISTS (SELECT " +
						        				"1 "+
	                                         "FROM " +
	                                         	"writers_skin ws " +
	                                         "WHERE " +
	                                         	"ws.writer_id = w.id " +
	                                         "AND " +
	                                         	"ws.skin_id IN (" +skins + ")) ";
					        }

						sql += "ORDER BY w.user_name ";

			ps = con.prepareStatement(sql);
			ps.setString(1, Constants.ROLE_RETENTION);
			ps.setString(2, Constants.ROLE_RETENTIONM);
			ps.setLong(3, Constants.DEPARTMENT_RETENTION );

			rs = ps.executeQuery();

			while (rs.next()) {
				Writer w = new Writer();
				w.setId(rs.getLong("id"));
				w.setUserName(rs.getString("user_name"));
				w.setSkins(WritersDAO.getWriterSkins(con, w.getId()));
				w.setSalesTypeDepartmentId(rs.getLong("sales_type_dept_id"));
				w.setSales_type(rs.getInt("sales_type"));
				list.put(new Long(w.getId()), w);
			}
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}

		return list;
	}



	/**
	 * get PopulationType object
	 *
	 * @param rs
	 *            ResultSet instance
	 * @return
	 * @throws SQLException
	 */
	protected static Population getPopulationsVO(ResultSet rs) throws SQLException {

		Population vo = new Population();
		vo.setId(rs.getLong("ID"));
		vo.setName(rs.getString("NAME"));
		vo.setSkinId(rs.getLong("SKIN_ID"));
		vo.setLanguageId(rs.getLong("LANGUAGE_ID"));
		vo.setStartDate(convertToDate(rs.getTimestamp("START_DATE")));
		vo.setEndDate(convertToDate(rs.getTimestamp("END_DATE")));
		vo.setThreshold(rs.getInt("THRESHOLD"));
		vo.setNumIssues(rs.getLong("NUM_ISSUES"));
		vo.setIsActive(rs.getInt("IS_ACTIVE")==1);
		vo.setRefreshTimePeriod(rs.getLong("REFRESH_TIME_PERIOD"));
		vo.setNumberOfMinutes(rs.getInt("NUMBER_OF_MINUTES"));
		vo.setNumberOfDays(rs.getInt("NUMBER_OF_DAYS"));
		vo.setWriterId(rs.getInt("WRITER_ID"));
		vo.setTimeCreated(convertToDate(rs.getTimestamp("TIME_CREATED")));
		vo.setBalanceAmount(rs.getLong("BALANCE_AMOUNT"));
		vo.setLastInvestDay(rs.getInt("LAST_INVEST_DAY"));
		vo.setPopulationTypeId(rs.getLong("POPULATION_TYPE_ID"));
		vo.setNumberOfDeposits(rs.getInt("number_of_deposits"));

		return vo;
	}

	public static ArrayList<SelectItem> getAssignedRepSI(Connection con, boolean isByDepartment) throws SQLException {
		ArrayList<SelectItem> list = new ArrayList<SelectItem>();
		PreparedStatement ps = null;
		ResultSet rs = null;
		WriterWrapper w = Utils.getWriter();
		try {
			String sql = "SELECT " +
							"distinct w.* " +
						"FROM " +
					        "writers w, roles r " +
					    "WHERE " +
					        "w.user_name = r.user_name AND " +
					        "w.is_active = 1 AND " ;

			if (isByDepartment) {
				sql += "(w.dept_id = " + ConstantsBase.DEPARTMENT_SUPPORT +
						 " OR w.dept_id = " + ConstantsBase.DEPARTMENT_RETENTION + ") AND ";
			}
			else {
				sql += "(r.role like ? or r.role like ?) AND " +
						"EXISTS (select 1 " +
			        		"from population_users pu " +
			        		"where curr_assigned_writer_id is not null AND " +
			        		"curr_assigned_writer_id = w.id) AND ";
			}

	        sql +=	"EXISTS (select 1 from " +
        				"writers_skin " +
        				"where writer_id = w.id and skin_id is not null and skin_id in (" + w.getSkinsToString() + ")) " +

				    "ORDER BY w.user_name " ;

			ps = con.prepareStatement(sql);
			if (!isByDepartment) {
				ps.setString(1, Constants.ROLE_RETENTION);
				ps.setString(2, Constants.ROLE_RETENTIONM);
			}
			rs = ps.executeQuery();

			while (rs.next()) {
				list.add(new SelectItem(new Long(rs.getLong("id")), rs.getString("user_name")));
			}
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}

		return list;
	}

	/**
	 * Returns population list
	 * @param list of population Ids
	 * @return a list of Populations
	 * @throws SQLException
	 */
	public static ArrayList<Population> getCountPopulations(Connection con,long skinId,long populationType) throws SQLException{
		PreparedStatement ps = null;
		ResultSet rs = null;
		ArrayList<Population> list = new ArrayList<Population>();
		WriterWrapper w = Utils.getWriter();
		try {

			String sql ="SELECT " +
							"p.name, count (*) AS population_count " +
						"FROM " +
							"population_users pu, population_entries pe, populations p, population_types pt " +
						"WHERE " +
							"pu.curr_population_entry_id = pe.id AND p.id= pe.population_id AND p.population_type_id = pt.id AND " +
							"skin_id is not null and skin_id in (" + w.getSkinsToString() + ") ";

						if (skinId != 0) {
							sql +="AND skin_id = " + skinId + " ";
						}
						if (populationType != 0){
							sql +="AND p.population_type_id = " + populationType + " ";
						}
						sql += "GROUP BY " +
									"p.name, pe.population_id, p.skin_id " +
							   "ORDER BY "+
							   		"p.skin_id";

			ps = con.prepareStatement(sql);
			rs = ps.executeQuery();
			while (rs.next()) {
				Population p = new Population();
				p.setCountPopulation(rs.getInt("population_count"));
				p.setName(rs.getString("name"));
				list.add(p);
			}
		} finally {
			closeStatement(ps);
		}

		return list;
	}


	/**
	 * Returns population types list by department
	 * @param skins
	 * @param dept id
	 * @return a list of Populations types
	 * @throws SQLException
	 */
	public static ArrayList<PopulationType> getPopulationsTypesByDeps(Connection con,String skins, int deptId) throws SQLException{
		PreparedStatement ps = null;
		ResultSet rs = null;
		ArrayList<PopulationType> list = new ArrayList<PopulationType>();

		try {

			String sql =" select " +
							" distinct pt.* " +
						" from " +
							" population_types pt, " +
							" populations p " +
						" where " +
							" p.population_type_id = pt.id " +
						    " and p.skin_id in (" + skins + " ) " +
						    " and p.dept_id = " + deptId + " " +
						    " and p.is_active = 1 " +
						" order by pt.name";


			ps = con.prepareStatement(sql);
			rs = ps.executeQuery();
			while (rs.next()) {
				list.add(getPopulationTypesVO(rs));
			}
		} finally {
			closeStatement(ps);
		}

		return list;
	}

	/**
	 * Returns population types list by department
	 * @param skins
	 * @param dept id
	 * @return a list of Populations types
	 * @throws SQLException
	 */
	public static ArrayList<PopulationType> getPopulationTypesStrBySalesType(Connection con,String skins, int deptId, long popTypeId) throws SQLException{
		PreparedStatement ps = null;
		ResultSet rs = null;
		ArrayList<PopulationType> list = new ArrayList<PopulationType>();

		try {

			String sql =" select " +
							" distinct pt.* " +
						" from " +
							" population_types pt, " +
							" populations p " +
						" where " +
							" p.population_type_id = pt.id " +
						    " and p.skin_id in (" + skins + " ) " +
						    " and p.dept_id = " + deptId + " " +
						    " and p.is_active = 1 ";
			if (popTypeId != 0) {
		    	if(popTypeId == Constants.POPULATION_TYPE_ID_CONVERSION) {
		    		 sql += " and (pt.type_id = " + popTypeId + " OR pt.id = 6)";
		    	} else {
				     sql += " and pt.type_id = " + popTypeId;
		    	}

	        }
			sql += " order by pt.name";


			ps = con.prepareStatement(sql);
			rs = ps.executeQuery();
			while (rs.next()) {
				list.add(getPopulationTypesVO(rs));
			}
		} finally {
			closeStatement(ps);
		}

		return list;
	}

	/**
	 * Returns population types list by department
	 * @param skins
	 * @param dept id
	 * @return a list of Populations types
	 * @throws SQLException
	 */
	public static ArrayList<PopulationType> getPopulationTypesStrBySalesTypeForTracking(Connection con,String skins) throws SQLException{
		PreparedStatement ps = null;
		ResultSet rs = null;
		ArrayList<PopulationType> list = new ArrayList<PopulationType>();

		try {

			String sql =" select " +
							" distinct pt.* " +
						" from " +
							" population_types pt, " +
							" populations p " +
						" where " +
							" p.population_type_id = pt.id " +
						    " and p.skin_id in (" + skins + " ) " +
						    " and p.is_active = 1 " +
							" and pt.type_id in ( " + PopulationsManagerBase.SALES_TYPE_RETENTION + "," + PopulationsManagerBase.SALES_TYPE_IMMEDIATE_TREATMENT + ") ";
			sql += " order by pt.name";


			ps = con.prepareStatement(sql);
			rs = ps.executeQuery();
			while (rs.next()) {
				list.add(getPopulationTypesVO(rs));
			}
		} finally {
			closeStatement(ps);
		}

		return list;
	}

	/**
	 * Get locked entry by lockHistoryId
	 * @param con db connection
	 * @param lockHistoryId
	 * @return
	 * @throws SQLException
	 */
	public static PopulationEntry getLockedEntryByLockHisId(Connection con, long lockHistoryId) throws SQLException {

		PreparedStatement ps = null;
		ResultSet rs = null;
		PopulationEntry vo = null;

		try {
			String sql = " SELECT " +
							" pu.id population_users_id, " +
						  	" pu.*, " +
						  	" pe.id curr_population_entry_id," +
						  	" pe.*," +
						  	" p.skin_id, " +
						  	" p.population_type_id, " +
						  	" p_old_pop.name p_old_name, " +
				            " p_old_pop.population_type_id pe_old_population_type_id, " +
				            " pe_old_pop.id pe_old_id, " +
				            " cn.id country_id, " +
				            " cn.gmt_offset country_offset " +
						 " FROM " +
							 " population_entries_hist peh "+
						 		" LEFT JOIN population_entries pe_old_pop on peh.population_entry_id = pe_old_pop.id " +
						 			" LEFT JOIN populations p_old_pop on pe_old_pop.population_id = p_old_pop.id " +
								 	" LEFT JOIN population_users pu on pe_old_pop.population_users_id = pu.id " +
									 	" LEFT JOIN contacts c on pu.contact_id = c.id " +
					             	 	" LEFT JOIN users u on pu.user_id = u.id " +" " +
					             	 			" LEFT JOIN countries cn on (u.country_id = cn.id OR (u.country_id is null AND c.country_id = cn.id)) " +
									 	" LEFT JOIN population_entries pe on pu.curr_population_entry_id = pe.id " +
							 				" LEFT JOIN populations p on pe.population_id = p.id " +
						 " WHERE " +
						 	" peh.id = ? ";


			ps = con.prepareStatement(sql);
			ps.setLong(1, lockHistoryId);

			rs = ps.executeQuery();

			if (rs.next()) {
				vo = new PopulationEntry();
				com.anyoption.common.daos.PopulationsDAOBase.getEntryBaseVO(rs, vo);
				vo.setOldPopulationName(rs.getString("p_old_name"));
				vo.setOldPopEntryId(rs.getLong("pe_old_id"));
				vo.setOldPopulationTypeId(rs.getLong("pe_old_population_type_id"));
				vo.setCountryId(rs.getLong("country_id"));
				vo.setCountryOffset(rs.getString("country_offset"));
			}
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		return vo;
	}

	public static HashMap<Integer, ArrayList<String>> getTypesIdAndValueArray(Connection con)throws SQLException{
		HashMap<Integer, ArrayList<String>> hm = new HashMap<Integer, ArrayList<String>>();
		PreparedStatement ps = null;
		ResultSet rs = null;
		ArrayList<String> non = new ArrayList<String>();
		ArrayList<String> retention = new ArrayList<String>();
		ArrayList<String> conversion = new ArrayList<String>();

		try {
			String sql =" SELECT " +
							" * " +
						" FROM " +
							" population_types " +
						" ORDER BY " +
							" type_id DESC";

			ps = con.prepareStatement(sql);
			rs = ps.executeQuery();

			while (rs.next()){
				if(rs.getInt("type_id") == Constants.POPULATION_TYPE_ID_NON) {
					non.add(rs.getString("id"));
				} else if(rs.getInt("type_id") == Constants.POPULATION_TYPE_ID_CONVERSION) {
					conversion.add(rs.getString("id"));
				} else if(rs.getInt("type_id") == Constants.POPULATION_TYPE_ID_RETENTION) {
					retention.add(rs.getString("id"));
				}
			}
			hm.put(Constants.POPULATION_TYPE_ID_NON, non);
			hm.put(Constants.POPULATION_TYPE_ID_CONVERSION, conversion);
			hm.put(Constants.POPULATION_TYPE_ID_RETENTION, retention);
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		return hm;
	}

	public static HashMap<Integer, ArrayList<String>> getKeyAndPriorityArray(Connection con)throws SQLException{
		HashMap<Integer, ArrayList<String>> hm = new HashMap<Integer, ArrayList<String>>();
		PreparedStatement ps = null;
		ResultSet rs = null;
		ArrayList<String> high = new ArrayList<String>();
		ArrayList<String> medium = new ArrayList<String>();
		ArrayList<String> low = new ArrayList<String>();

		try {
			String sql =" SELECT " +
							" * " +
						" FROM " +
							" marketing_affilates " +
						" ORDER BY " +
							" key DESC";

			ps = con.prepareStatement(sql);
			rs = ps.executeQuery();

			while (rs.next()){
				if(rs.getInt("priority_id") == Constants.AFFILIATE_PRIORITY_LOW) {
					low.add(rs.getString("id"));
				} else if(rs.getInt("priority_id") == Constants.AFFILIATE_PRIORITY_MEDIUM) {
					medium.add(rs.getString("id"));
				} else if(rs.getInt("priority_id") == Constants.AFFILIATE_PRIORITY_HIGH) {
					high.add(rs.getString("id"));
				}
			}
			hm.put(Constants.AFFILIATE_PRIORITY_LOW, low);
			hm.put(Constants.POPULATION_TYPE_ID_CONVERSION, medium);
			hm.put(Constants.POPULATION_TYPE_ID_RETENTION, high);
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		return hm;
	}

	public static ArrayList<Population> getPopulationListByUserId(Connection con, long userId) throws Exception {
		PreparedStatement ps = null;
		ResultSet rs = null;
		ArrayList<Population> populationList = new ArrayList<Population>();
		try {
			String sql = " SELECT " +
						 "		p.*," +
						 "		pu.time_control," +
						 "		pehs.id as status_id " +
						 " FROM " +
						 "		population_users pu, " +
						 "		population_entries pe, " +
						 "		population_entries_hist peh, " +
						 "		populations p, " +
						 "		population_entries_hist_status pehs " +
						 " WHERE " +
						 "		pu.id = pe.population_users_id " +
						 "		AND pe.id = peh.population_entry_id " +
						 "		AND pu.user_id = ? " +
						 "		AND p.id = pe.population_id " +
						 "		AND pehs.id= peh.status_id " +
						 "		AND pehs.id = ? " +
						 " ORDER BY peh.time_created desc ";

			ps = con.prepareStatement(sql);
			ps.setLong(1, userId);
			ps.setInt(2, PopulationsManagerBase.POP_ENT_HIS_STATUS_NEW);
			rs = ps.executeQuery();
			while (rs.next()) {
				Population vo = new Population();
				vo.setId(rs.getLong("ID"));
				vo.setName(rs.getString("NAME"));
				vo.setSkinId(rs.getLong("SKIN_ID"));
				vo.setLanguageId(rs.getLong("LANGUAGE_ID"));
				vo.setStartDate(convertToDate(rs.getTimestamp("start_date")));
				vo.setEndDate(convertToDate(rs.getTimestamp("end_date")));
				vo.setThreshold(rs.getInt("THRESHOLD"));
				vo.setNumIssues(rs.getLong("NUM_ISSUES"));
				vo.setRefreshTimePeriod(rs.getLong("REFRESH_TIME_PERIOD"));
				vo.setNumberOfMinutes(rs.getInt("NUMBER_OF_MINUTES"));
				vo.setNumberOfDays(rs.getInt("NUMBER_OF_DAYS"));
				vo.setWriterId(rs.getInt("writer_id"));
				vo.setTimeCreated(convertToDate(rs.getTimestamp("time_created")));
				vo.setBalanceAmount(rs.getLong("BALANCE_AMOUNT"));
				vo.setLastInvestDay(rs.getInt("LAST_INVEST_DAY"));
				vo.setPopulationTypeId(rs.getInt("POPULATION_TYPE_ID"));
				vo.setNumberOfDeposits(rs.getInt("number_of_deposits"));
				int isActive = rs.getInt("is_active");
				if (isActive == 1) {
					vo.setIsActive(true);
				} else {
					vo.setIsActive(false);
				}
				vo.setEntryStatusId(rs.getInt("status_id"));
				vo.setTimeControl(convertToDate(rs.getTimestamp("time_control")));
				populationList.add(vo);
			}
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		return populationList;
	}

	public static HashMap<Long, Writer> getSalesWritersBySalesType(Connection con, String skinsToString, int salesTypeConversion) throws SQLException {
		HashMap<Long, Writer> list = new HashMap<Long, Writer>();
		PreparedStatement ps = null;
		ResultSet rs = null;

		try {
			String sql ="SELECT " +
							"distinct w.* " +
						"FROM " +
					        "writers w " +
					    "WHERE " +
					        "w.is_active = 1 AND " +
					        "w.sales_type  in (? ,?) " +
					        "AND w.dept_id = ? ";

					        if (null != skinsToString) {
						        sql += "AND EXISTS (SELECT " +
						        				"1 "+
	                                         "FROM " +
	                                         	"writers_skin ws " +
	                                         "WHERE " +
	                                         	"ws.writer_id = w.id " +
	                                         "AND " +
	                                         	"ws.skin_id IN (" + skinsToString + ")) ";
					        }

						sql += "ORDER BY w.user_name ";

			ps = con.prepareStatement(sql);
			ps.setLong(1, ConstantsBase.SALES_DEPOSIT_TYPE_BOTH);
			ps.setInt(2, salesTypeConversion);
			ps.setLong(3, Constants.DEPARTMENT_RETENTION );

			rs = ps.executeQuery();

			while (rs.next()) {
				Writer w = new Writer();
				w.setId(rs.getLong("id"));
				w.setUserName(rs.getString("user_name"));
				w.setSkins(WritersDAO.getWriterSkins(con, w.getId()));
				list.put(new Long(w.getId()), w);
			}
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}

		return list;
	}

	/**
	 * @param con
	 * @param salesType
	 * @return list of Representative writers by sales type. conversion or retention.
	 * if a writer is both he will be in both lists.
	 * @throws SQLException
	 */
	public static ArrayList<SelectItem> getAssignedRepListBySalesType(Connection con, long salesType) throws SQLException {
		ArrayList<SelectItem> list = new ArrayList<SelectItem>();
		PreparedStatement ps = null;
		ResultSet rs = null;
		WriterWrapper w = Utils.getWriter();
		try {
			String sql =
						"SELECT " +
							"distinct w.* " +
						"FROM " +
					        "writers w, roles r " +
					    "WHERE " +
					        "w.user_name = r.user_name AND " +
					        "w.is_active = 1 ";

			 if (salesType == ConstantsBase.SALES_DEPOSIT_TYPE_CONVERSION) {
			    	sql += 	" AND w.sales_type in (1, 3) ";
		    } else if (salesType == ConstantsBase.SALES_DEPOSIT_TYPE_RETENTION) {
		    	sql += " AND w.sales_type in (2, 3) ";
		    }
			 sql += " AND (r.role like ? or r.role like ?) AND " +
					"EXISTS (select 1 " +
		        		"from population_users pu " +
		        		"where curr_assigned_writer_id is not null AND " +
		        		"curr_assigned_writer_id = w.id) AND ";

			 sql +=	"EXISTS (select 1 from " +
        				"writers_skin " +
        				"where writer_id = w.id and skin_id is not null and skin_id in (" + w.getSkinsToString() + ")) " +

				    "ORDER BY w.user_name " ;

			ps = con.prepareStatement(sql);
			ps.setString(1, Constants.ROLE_RETENTION);
			ps.setString(2, Constants.ROLE_RETENTIONM);
			rs = ps.executeQuery();

			while (rs.next()) {
				list.add(new SelectItem(new Long(rs.getLong("id")), rs.getString("user_name")));
			}
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}

		return list;
	}

	/**
	 * Get all sales reps from conversion department
	 * @param con
	 * @return HashMap<Long, String>
	 * @throws SQLException
	 */
	public static ArrayList<SelectItem> getSalesRepsConversionDep(Connection con) throws SQLException {
		ArrayList<SelectItem> list = new ArrayList<SelectItem>();
		PreparedStatement ps = null;
		ResultSet rs = null;

		try {
			String sql = "SELECT " +
							"id, " +
							"user_name " +
						 "FROM " +
						 	"writers w " +
						 "WHERE " +
						 	"w.sales_type IN (1,3) AND " +
						    "w.dept_id = 3 AND " +
						    "w.is_active = 1 " +
						  "ORDER BY " +
						  	"w.user_name";

			ps = con.prepareStatement(sql);

			rs = ps.executeQuery();

			while (rs.next()) {
				SelectItem temp = new SelectItem();
				temp.setValue(rs.getLong("id"));
				temp.setLabel(rs.getString("user_name"));
				list.add(temp);
			}
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}

		return list;
	}

	/**
	 * Get all sales reps
	 * @param con
	 * @return HashMap<Long, String>
	 * @throws SQLException
	 */
	public static ArrayList<SelectItem> getSalesReps(Connection con) throws SQLException {
		ArrayList<SelectItem> list = new ArrayList<SelectItem>();
		PreparedStatement ps = null;
		ResultSet rs = null;

		try {
			String sql = "SELECT " +
							"id, " +
							"user_name " +
						 "FROM " +
						 	"writers w " +
						 "WHERE " +
						 	"w.dept_id = 3 AND " +
						 	"w.is_active = 1 AND " +
						 	"w.sales_type IS NOT null " +
						 "ORDER BY " +
						  	"w.user_name";

			ps = con.prepareStatement(sql);
			rs = ps.executeQuery();

			while (rs.next()) {
				SelectItem temp = new SelectItem();
				temp.setValue(rs.getLong("id"));
				temp.setLabel(rs.getString("user_name"));
				list.add(temp);
			}
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}

		return list;
	}
}
