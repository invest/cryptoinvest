package il.co.etrader.backend.dao_managers;

import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;

import org.apache.log4j.Logger;
import org.bouncycastle.crypto.CryptoException;

import com.anyoption.backend.daos.TransactionDAO;
import com.anyoption.common.beans.CreditCard;
import com.anyoption.common.beans.SpecialCare;
import com.anyoption.common.beans.Transaction;
import com.anyoption.common.beans.TransactionPostponed;
import com.anyoption.common.beans.base.CreditCardType;
import com.anyoption.common.bl_vos.TransactionReport;
import com.anyoption.common.managers.TransactionsManagerBase.transactionUpdateStatus;
import com.anyoption.common.util.AESUtil;

import il.co.etrader.backend.bl_managers.ApplicationData;
import il.co.etrader.backend.bl_managers.TransactionsManager;
import il.co.etrader.backend.bl_vos.CaptureDailyInfo;
import il.co.etrader.backend.bl_vos.DepositsSummary;
import il.co.etrader.backend.bl_vos.FeeCurrencyMap;
import il.co.etrader.backend.bl_vos.ReroutingLimit;
import il.co.etrader.backend.bl_vos.User;
import il.co.etrader.backend.util.Constants;
import il.co.etrader.backend.util.Utils;
import il.co.etrader.bl_managers.TransactionsManagerBase;
import il.co.etrader.dao_managers.CreditCardsDAO;
import il.co.etrader.dao_managers.TransactionsDAOBase;
import il.co.etrader.util.CommonUtil;
import il.co.etrader.util.ConstantsBase;

public class TransactionsDAO extends TransactionsDAOBase {
	private static final Logger logger = Logger.getLogger(TransactionsDAO.class);
	private final static Integer CUP_CHINA_WITHDRAW = 10; 
	private final static Integer EFT= 3;
	private final static Integer ENVOY = 26;
	private final static Integer PAY_PAL= 19;
	private final static Integer SKRILL= 39;
	private final static Integer BYCHECK= 36;
	private final static Integer CC= 35;
	private final static Integer RUBLE = 37;
	private final static Integer IS_ACTIVE_TRUE = 1;
	private final static Integer FOCAL_PAYMENTS = 41;

	public static void updateRefId(Connection con, long tranId, long refId) throws SQLException {
		PreparedStatement ps = null;
		try {
			String sql = "update transactions set REFERENCE_ID=? where id=? ";

			ps = con.prepareStatement(sql);
			ps.setLong(1, refId);
			ps.setLong(2, tranId);
			ps.executeUpdate();
		} finally {
			closeStatement(ps);
		}
	}

	public static void copyCreditCards(Connection con, long newUserId, long oldUserId) throws SQLException {
		PreparedStatement ps = null;
		try {
			String sql =
					" INSERT into credit_cards (id,user_id,type_id,is_visible,cc_number,cc_number_back,cc_pass,time_created,time_modified, " +
					" exp_month,exp_year,holder_name,holder_id_num,is_allowed, utc_offset_created, utc_offset_modified, recurring_transaction, cc_number_last_4_digits) " +
					" (SELECT seq_credit_cards.nextval,?,type_id,is_visible,cc_number,cc_number_back,cc_pass,sysdate,sysdate, " +
					" exp_month,exp_year,holder_name,holder_id_num,is_allowed, utc_offset_created, utc_offset_modified, recurring_transaction, cc_number_last_4_digits " +
					" FROM credit_cards " +
					" WHERE user_id = ?) ";

			ps = con.prepareStatement(sql);
			ps.setLong(1, newUserId);
			ps.setLong(2, oldUserId);
			ps.executeUpdate();
		} finally {
			closeStatement(ps);
		}
	}

	public static long getSummary(Connection con, long userId, String types, String status, boolean isUSD) throws SQLException {

		PreparedStatement ps = null;
		ResultSet rs = null;

		try {
			String sql = "select ";
						if (isUSD) {
							sql += " sum(amount * rate) total from transactions ";
						} else {	
							sql += " sum(amount) total from transactions ";
							}
						    sql += " where user_id=? and type_id in (" + types + ") " +
								   " and status_id in (" + status + ") ";
			ps = con.prepareStatement(sql);
			ps.setLong(1, userId);

			rs = ps.executeQuery();
			if (rs.next()) {
				return rs.getLong("total");
			}
		}

		finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		return 0;

	}

	// return: DATE_AMOUNT
	public static String getLastSalesDeposit(Connection con, long userId) throws SQLException {

		PreparedStatement ps = null;
		ResultSet rs = null;

		try {
			String sql = "SELECT time_created, amount " +
							"FROM transactions, transactions_issues " +
							"WHERE " +
							"	user_id = ? AND " +
							"	transactions.id = transactions_issues.transaction_id AND " +
							"	transactions.status_id IN (2,7,8) " +
							"ORDER BY time_created DESC";
			ps = con.prepareStatement(sql);
			ps.setLong(1, userId);

			rs = ps.executeQuery();
			if (rs.next()) {
				return String.valueOf(rs.getDate("time_created").getTime()) + '_' + rs.getLong("amount");
			}
		}

		finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		return null;

	}

	public static String[] getAverageDepositWithdraw(Connection con, long id) throws SQLException {
		String[] res = new String[2];
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			String sql =
					"select nvl(trunc(avg(amount)),0) avgamount" + " " +
					"from transactions t,transaction_types tt " +
					"where t.user_id=? and t.type_id=tt.id and " +
					"tt.class_type in (?,?) and " +
					"t.status_id in (" + TransactionsManagerBase.TRANS_STATUS_SUCCEED +
					"," + TransactionsManagerBase.TRANS_STATUS_PENDING + "," + TransactionsManagerBase.TRANS_STATUS_REQUESTED + "," + TransactionsManagerBase.TRANS_STATUS_APPROVED + "," + TransactionsManagerBase.TRANS_STATUS_SECOND_APPROVAL + ")";
			ps = con.prepareStatement(sql);
			ps.setLong(1, id);
			ps.setLong(2, TransactionsManagerBase.TRANS_CLASS_DEPOSIT);
			ps.setLong(3, TransactionsManagerBase.TRANS_CLASS_ADMIN_DEPOSITS);
			rs = ps.executeQuery();
			if (rs.next()) {
				res[0] = rs.getString("avgamount");
			}
			closeResultSet(rs);
			closeStatement(ps);

			sql =
					" select nvl(trunc(avg(amount)),0) avgamount" + " " +
					"from transactions t,transaction_types tt " +
					"where t.user_id=? and t.type_id=tt.id and " +
					"t.status_id in (" + TransactionsManagerBase.TRANS_STATUS_SUCCEED + ") and " +
					"t.type_id in (" + TransactionsManagerBase.TRANS_TYPE_ADMIN_WITHDRAW + "," +
									TransactionsManagerBase.TRANS_TYPE_CHEQUE_WITHDRAW + "," +
									TransactionsManagerBase.TRANS_TYPE_CC_WITHDRAW + "," +
									TransactionsManagerBase.TRANS_TYPE_BANK_WIRE_WITHDRAW + "," +
                                    TransactionsManagerBase.TRANS_TYPE_EFT_WITHDRAW + "," +
                                    TransactionsManagerBase.TRANS_TYPE_MONEYBOOKERS_WITHDRAW + "," +
                                    TransactionsManagerBase.TRANS_TYPE_CUP_CHINA_WITHDRAW + "," +
                                    TransactionsManagerBase.TRANS_TYPE_WEBMONEY_WITHDRAW + "," +
                                    TransactionsManagerBase.TRANS_TYPE_BAROPAY_WITHDRAW + "," +
                                    TransactionsManagerBase.TRANS_TYPE_DIRECT24_WITHDRAW + "," +
                                    TransactionsManagerBase.TRANS_TYPE_GIROPAY_WITHDRAW + "," +
                                    TransactionsManagerBase.TRANS_TYPE_EPS_WITHDRAW + "," +	
									TransactionsManagerBase.TRANS_TYPE_ENVOY_WITHDRAW + "," +	
									TransactionsManagerBase.TRANS_TYPE_IDEAL_WITHDRAW + ")";
			ps = con.prepareStatement(sql);
			ps.setLong(1, id);
			rs = ps.executeQuery();
			if (rs.next()) {
				res[1] = rs.getString("avgamount");
			}
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		return res;
	}

	public static ArrayList<Transaction> getPendingWithdraws(Connection con, Date from, Date to, String userId,
			String userIdNum,boolean admin, String userName, long classId, String skins, long currencyId, int skinId, long skinBusinessCaseId, long withdrawTypeId) throws SQLException {

		PreparedStatement ps = null;
		ResultSet rs = null;
		ArrayList<Transaction> list = new ArrayList<Transaction>();

		try {
			String sql =
					"SELECT " +
						"t.*, " +
						"ty.description typename, " +
						"ty.class_type classtype, " +
						"u.user_name username, " +
						"w.user_name writername, " +
						" u.TIME_SC_TURNOVER, " +
						" u.TIME_SC_HOUSE_RESULT, " +
						" u.currency_id, " +
						" (last.time_settled) as last, " +
						" u.is_stop_reverse_withdraw, " +
						" s.is_regulated, " +
						" u.is_immediate_withdraw, " +
						" case when nvl(ur.approved_regulation_step,0) =  7 then 1 else 0 end as control " +
					"FROM " +
						"transaction_types ty, " +
						"users u, " +
						"skins s, " +
						"writers w, " +
						"transactions t " +
						" LEFT JOIN ( SELECT (max( t.time_settled )) as time_settled , t.user_id " +
								    " FROM transactions t , transaction_types tt " +
								    " WHERE t.type_id = tt.id AND " +
										  " tt.class_type = ? AND " +
										  " is_accounting_approved = ? " +
								    " GROUP BY t.user_id ) last on t.user_id = last.user_id " +
						"  LEFT JOIN  users_regulation ur " +
						    " on t.user_id = ur.user_id  "	+									  
					"WHERE " +
						"t.type_id = ty.id AND " +
						"ty.class_type = ? AND " +
						"t.user_id = u.id AND " +
						"u.skin_id = s.ID AND " + 
						"t.writer_id = w.id AND " +
						"( t.is_accounting_approved = 0 OR t.is_accounting_approved is null ) ";

			if (admin) {
				sql += " and t.status_id = " + TransactionsManager.TRANS_STATUS_REQUESTED;
			} else {
				sql += " and t.status_id in (" + TransactionsManager.TRANS_STATUS_APPROVED + "," + TransactionsManager.TRANS_STATUS_SECOND_APPROVAL + ")";
			}
			
			sql += " and t.time_created>=? " + " and t.time_created<=? ";

			sql+=" and u.skin_id in (" + skins + ")";

			if (skinId!=0) {
				sql+=" and u.skin_id=" + skinId;
			}

			if (skinBusinessCaseId > 0) {
				sql += "AND u.skin_id in (SELECT id FROM skins WHERE business_case_id = " +skinBusinessCaseId+ ") ";
			}

			if (currencyId!=0) {
				sql+=" and u.currency_id=" + currencyId;
			}

			if (userId != null && !userId.equals("")) {
				sql += " and u.id=" + userId;
			}

			if (userIdNum != null && !userIdNum.equals("")) {
				sql += " and u.id_num='" + AESUtil.encrypt(userIdNum) + "'";
			}

			if ( userName != null && !userName.equals("") ) {
				sql += " and u.user_name='"+userName+"'";
			}

		  	if ( classId != ConstantsBase.USER_CLASS_ALL )   {
		  		if ( classId == ConstantsBase.USER_CLASS_COMPANY_OR_PRIVATE )  {
		  			sql += " and (u.class_id="+ConstantsBase.USER_CLASS_PRIVATE+""+
		  				   " or u.class_id="+ConstantsBase.USER_CLASS_COMPANY+")";
		  		} else {
		  			sql += " and u.class_id ="+classId;
		  		}
		  	}
		  	if(withdrawTypeId!=0){
		  		sql+=" and ty.id ='"+withdrawTypeId+"'";  
		  	}
			sql += " order by t.time_created ";

			ps = con.prepareStatement(sql);

			ps.setLong(1, TransactionsManagerBase.TRANS_CLASS_WITHDRAW);
			ps.setInt(2, Constants.ACCOUNTING_APPROVED_YES);

			ps.setLong(3, TransactionsManagerBase.TRANS_CLASS_WITHDRAW);

			ps.setTimestamp(4, CommonUtil.convertToTimeStamp(CommonUtil.getDateTimeFormat(from,  Utils.getWriter().getUtcOffset())));
			ps.setTimestamp(5, CommonUtil.convertToTimeStamp(CommonUtil.getDateTimeFormat(CommonUtil.addDay(to),  Utils.getWriter().getUtcOffset())));

			rs = ps.executeQuery();

			while(rs.next()) {

				Transaction t = getVO(con, rs);
				t.setRegulated(rs.getInt("is_regulated")==1);
                SpecialCare specialCare = new SpecialCare();
				t.setUserName(rs.getString("username"));
				t.setPayPalEmail(rs.getString("paypal_email"));
				t.setMoneybookersEmail(rs.getString("moneybookers_email"));
				t.setWebMoneyPurse(rs.getString("webmoney_purse"));
                specialCare.setTimeScTurnover(convertToDate(rs.getTimestamp("TIME_SC_TURNOVER")));
                specialCare.setTimeScHouseResult(convertToDate(rs.getTimestamp("TIME_SC_HOUSE_RESULT")));
                t.setSpecialCare(specialCare);
                t.setLastWithdrawlSettled(rs.getTimestamp("LAST"));
                t.setUpdateByAdmin(rs.getLong("update_by_admin") == 0 ? false : true);
                //t.setSpecialCare(rs.getString("TIME_SC_TURNOVER") != null || rs.getString("TIME_SC_HOUSE_RESULT") != null);
				if (!t.isChequeWithdraw()) {

					// set fee_cancel field
					long feeCancel = rs.getLong("fee_cancel");
					if ( feeCancel == ConstantsBase.CC_FEE_CANCEL_NO ) {
						t.setFeeCancel(false);
					}
					else if ( feeCancel == ConstantsBase.CC_FEE_CANCEL_YES ) {
						t.setFeeCancel(true);
					}

					if ( t.getTypeId() == TransactionsManagerBase.TRANS_TYPE_CC_WITHDRAW ) {
						t.setCc4digit(CreditCardsDAO.getById(con, Long.parseLong(String.valueOf(t.getCreditCardId()))).getCcNumberLast4());
					}
				}

				long feeCancelByAdmin = rs.getLong("fee_cancel_by_admin");
				if ( feeCancelByAdmin == ConstantsBase.CC_FEE_CANCEL_NO ) {
					t.setFeeCancelByAdmin(false);
				}
				else if ( feeCancelByAdmin == ConstantsBase.CC_FEE_CANCEL_YES ) {
					t.setFeeCancelByAdmin(true);
				}
				
		        long isStopReverseWithdrawOption = rs.getLong("is_stop_reverse_withdraw");
		        if (isStopReverseWithdrawOption == 1 ) {
		            t.setUserStopReverseWithdrawOption(true);
		        }
		        else {
		            t.setUserStopReverseWithdrawOption(false);
		        }
		        
		        long isImmediateWithdraw = rs.getLong("is_immediate_withdraw");
		        if (isImmediateWithdraw == 1 ) {
		            t.setUserImmediateWithdraw(true);
		        } else {
		            t.setUserImmediateWithdraw(false);
		        }
		        t.setControl(rs.getBoolean("control"));
		        double rate = rs.getDouble("rate");
		        t.setBaseAmount(Math.round(rate * t.getAmount()));
				list.add(t);
			}
		} catch (	CryptoException | InvalidKeyException | NoSuchAlgorithmException | NoSuchPaddingException | IllegalBlockSizeException
					| BadPaddingException | UnsupportedEncodingException | InvalidAlgorithmParameterException ce) {
			throw new SQLException(ce.getMessage());
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		return list;
	}
	
	/*
	 *  WriterId = -1 means ALL
	 *  WtiterId = 0 means AUTO
	 */
	public static ArrayList<Transaction> getTransactions(Connection con, long id, Date from, Date to, long typeId,
			long statusId, String searchBy, String receiptNum, String ccNum,
				boolean timeSearch, String userName, long classId,String skins, long countryId, long skinBusinessCaseId,long hasInvestments,
                long startHour, long endHour, long currencyId, long campaignId, long writerId, long providerId,boolean byPhone, Date arabicStartDate, 
                long paymentMethodId, int hasBalance, int salesDepositsType, int salesDepositsSource, long chosenBankWireId, long clearingProvider, 
                int ccBin, long wasRerouted, long manualRouting, long transactionId, int isPostponed, long providerTransactionId, String affiliatesKeys, long is3d) throws SQLException {

		PreparedStatement ps = null;
		ResultSet rs = null;
		ArrayList<Transaction> list = new ArrayList<Transaction>();

		FacesContext context=FacesContext.getCurrentInstance();
		ApplicationData app = (ApplicationData) context.getApplication().createValueBinding(Constants.BIND_APPLICATION_DATA).getValue(context);
		if (receiptNum != null && !receiptNum.equals("")) {
			int pos = receiptNum.indexOf("-");
			if (pos > -1) {
				receiptNum = receiptNum.substring(pos + 1);
			}
			try {
				long l = Long.valueOf(receiptNum);
			} catch (Exception e) {
				return list;
			}
		}
		int index = 1;
		try {
			String sql =
					" select t.*,ty.description typename,ty.class_type classtype, " +
					" u.currency_id currency_id, u.user_name username,u.first_name firstname,u.last_name lastname,u.skin_id, " +
					" u.TIME_SC_TURNOVER, " +
					" u.TIME_SC_HOUSE_RESULT, " +
					" c.a3 country_code,c.display_name country, c.country_name cc_country, c.id countryId, cp.name provider_name, " +
					" NVL( (select count(*) " +
		                   " from transactions " +
		                   " where type_id = ? and user_id = u.id " +
		                   " and status_id in (" + TransactionsManagerBase.TRANS_STATUS_REQUESTED + "," + TransactionsManagerBase.TRANS_STATUS_APPROVED + "," + TransactionsManagerBase.TRANS_STATUS_SECOND_APPROVAL + ")" +
		                   " and credit_card_id = cc.id and is_credit_withdrawal = 1 " +
                    " ), 0 ) as HAVE_WITHDRAWALS, " +
                    " pt.description directProviderName, " +
                    " pt.name paymentMethodName, " +
                    " mc.name as campaign_name, " +
                    " sbc.description as business_case_name, " +
                    " w.withdrawal_source, " +
                    " tp.id tp_id, " +
                    " tp.number_of_days tp_number_of_days, " +
                    " tp.writer_id tp_writer_id, " +
                    " tp.time_updated as tp_time_updated, " +
                    " b.card_type, " +
                    " cct.description as cc_name" +
					" from " +
                        " transactions t, " +
                        " transaction_postponed tp, " +
                        " transactions_issues ti, " +
                        " transaction_types ty, " +
                        " credit_cards cc, " +
                        " users u, " +
                        " countries c, " +
                        " clearing_providers cp, " +
                        " payment_methods pt, " +
                        " marketing_campaigns mc, " +
                        " marketing_combinations mcb, " +
                        " skins s, " +
                        " skin_business_cases sbc, " +
                        " wires w, " +
                        " bins  b, " +
                        " credit_card_types cct " +
					" where " +
                        " t.user_id = u.id and " +
                        " t.type_id=ty.id and " +
                        " t.credit_card_id=cc.id (+) and " +
                        " cc.country_id = c.id (+) and " +
    					" t.clearing_provider_id=cp.id (+) and " +
                        " t.payment_type_id = pt.id (+) and " +
                        " mc.id = mcb.campaign_id and " +
                        " u.combination_id = mcb.id AND " +
                        " s.id = u.skin_id AND " +
                        " sbc.id = s.business_case_id AND " +
                        " t.wire_id = w.id(+) AND " +
                        " t.id = ti.transaction_id (+) AND " +
                        " t.id = tp.transaction_id (+) AND " +
                        " cc.type_id_by_bin = b.id(+) AND " +
                        " cc.type_id = cct.id(+) AND " +
                        " 1 = DECODE(?, 0, 1, t.xor_id_authorize, 1, 0) ";
			if (isPostponed == ConstantsBase.GENERAL_YES) {
				sql += " and tp.id is not null ";
			} else if (isPostponed == ConstantsBase.GENERAL_NO) {
				sql += " and tp.id is null ";
			}

			if (ccNum != null && !ccNum.equals(""))
				sql += " and cc.cc_number LIKE '" + AESUtil.encrypt(ccNum) + "' ";

			if (receiptNum!=null && !receiptNum.trim().equals(""))
				sql += " and t.RECEIPT_NUM=" + receiptNum + " ";

			if (salesDepositsType == ConstantsBase.SALES_DEPOSIT_TYPE_CONVERSION){
				sql += " and u.first_deposit_id = t.id ";
			}else if (salesDepositsType == ConstantsBase.SALES_DEPOSIT_TYPE_RETENTION){
				sql += " and u.first_deposit_id != t.id  ";
			}

			if (salesDepositsSource == Constants.SALES_DEPOSIT_SOURCE_SALES){
				sql += " and ti.transaction_id is not null ";
			}else if (salesDepositsSource == Constants.SALES_DEPOSIT_SOURCE_INDEPENDENT){
				sql += " and ti.transaction_id is null  ";
			}

            String timeField = "t.time_created";
			if (searchBy.equals("0")) {
				 sql += " AND t.time_created >= ? "+
			 			" AND t.time_created <= ? ";

			} else {
				 sql += " AND t.time_settled >= ? "+
			 			" AND t.time_settled <= ? ";
                timeField = "t.time_settled";
			}
			if (transactionId != 0){
				sql += " AND t.id = ? ";
			}
            if (chosenBankWireId != 0 )
                sql += " AND w.withdrawal_source = " + chosenBankWireId + " ";

      /*      if (startHour != 99) { // use time starting parameter
                String startHourTxt = new String();
                TimeZone tz = TimeZone.getTimeZone(Utils.getWriter().getUtcOffset());
                startHour = startHour - tz.getRawOffset() / (1000*3600);
                if (startHour < 0) {
                	startHour = 0;
                }
                if(startHour < 10){
                    startHourTxt = "0" + startHour;
                } else {
                    startHourTxt = String.valueOf(startHour);
                }
                startHourTxt += ":00";

                sql+= " and to_char(" + timeField + ",'HH24:MI') >= '" + startHourTxt + "'";
            }

            if (endHour != 99) { // use time closing parameter
                String endHourTxt = new String();
                TimeZone tz = TimeZone.getTimeZone(Utils.getWriter().getUtcOffset());
                endHour = endHour - tz.getRawOffset() / (1000*3600);
                if (endHour < 0) {
                	endHour = 0;
                }
                if(endHour < 10){
                    endHourTxt = "0" + endHour;
                } else {
                    endHourTxt = String.valueOf(endHour);
                }
                endHourTxt += ":00";

                sql+= " and to_char(" + timeField + ",'HH24:MI') <= '" + endHourTxt + "'";
            }

       	*/

			if (id != 0)
				sql += " and t.user_id=" + id;

		  	if ( classId != ConstantsBase.USER_CLASS_ALL )   {

		  		if ( classId == ConstantsBase.USER_CLASS_COMPANY_OR_PRIVATE )  {
		  			sql += " and (u.class_id="+ConstantsBase.USER_CLASS_PRIVATE +
		  				  " or u.class_id="+ConstantsBase.USER_CLASS_COMPANY+")";
		  		}
		  		else {
		  				sql += " and u.class_id =" + classId + " ";
		  		}
		  	}

	  		if (hasBalance == ConstantsBase.GENERAL_YES){
	  			sql += " and u.balance > 0 ";
	  		}else if (hasBalance == ConstantsBase.GENERAL_NO){
	  			sql += " and u.balance = 0 ";
	  		}

			if (typeId > 0) {
				if (typeId == TransactionsManager.TRANS_TYPE_ALL_DEPOSIT) {
					sql += " and ty.class_type=" + TransactionsManager.TRANS_CLASS_DEPOSIT + " and t.type_id<>" + TransactionsManager.TRANS_STATUS_REVERSE_WITHDRAW;
				} else if (typeId == TransactionsManager.TRANS_TYPE_ALL_WITHDRAW) {
					sql += " and (ty.class_type=" + TransactionsManager.TRANS_CLASS_WITHDRAW + " or " +
						          "ty.class_type=" + TransactionsManager.TRANS_CLASS_ADMIN_WITHDRAWALS + " or " +
						          "(ty.class_type=" + TransactionsManager.TRANS_CLASS_FIX_BALANCE + " AND ty.id= " + TransactionsManager.TRANS_TYPE_FIX_BALANCE_WITHDRAW  + ")) ";
                                  /*"ty.class_type=" + TransactionsManager.TRANS_CLASS_BONUS_WITHDRAWALS + ") "*/
				} else {
					sql += " and t.type_id=" + typeId + " ";
				}
			}

			if (paymentMethodId > 0){
				sql += " and t.payment_type_id = " +  paymentMethodId + " ";
			}

			if (statusId > 0) {
                if (statusId == TransactionsManager.TRANS_STATUS_PANDING_AND_SUCCESS) {
                    sql += " and t.status_id in (2,7) ";
                } else {
                    sql += " and t.status_id in (" + statusId + ") ";
                }
			}

			if ( userName != null && !userName.equals("") ) {
				sql += " and u.user_name='"+userName+"'";
			}

			if (countryId > 0) {
				sql += "AND c.id = " + countryId + " ";
			}

			if (skinBusinessCaseId > 0) {
				sql += "AND u.skin_id in (SELECT id FROM skins WHERE business_case_id = " +skinBusinessCaseId+ ") ";
			}

		//	 has investments

	        if (hasInvestments > 0) {
	        	String invSql = " AND ";
	        	if (hasInvestments == Constants.CAPTURE_INFO_INVEST) {
	        		invSql += "EXISTS ";
	        	} else if (hasInvestments == Constants.CAPTURE_INFO_NOT_INVEST) {
	        		invSql += "NOT EXISTS ";
	        	}
	        	invSql += "(SELECT 1 " +
	        			   "FROM investments i " +
	        			   "WHERE i.user_id = u.id AND " +
	        			   	 	"i.is_canceled = 0) ";
	        	sql += invSql;
	        }


			//writer permissions
		    sql += " AND u.skin_id in (" + skins + ")";

		    if (currencyId > 0) {
		        sql += " AND u.currency_id = " + currencyId;
            }

            if (campaignId > 0) {
                sql += " AND mc.id = " + campaignId;
            }

            if (writerId >= 0) {
                sql += " AND t.writer_id = " +  writerId;
            }

            if (providerId > 0) {
                sql += " AND cp.id = " +  providerId;
            }

            if (byPhone) {
            	sql += " AND " +
            			  "t.auth_number is not null " +
            		   " AND " +
            			  "t.xor_id_capture is not null " +
            		   " AND " +
            		   	  "t.xor_id_authorize is null " ;
            }

            if(null != arabicStartDate){
				 sql += " AND u.time_created >= ? ";
			}
            
            if (clearingProvider > 0) {
                sql += " AND cp.id = " + clearingProvider;
            }
            
            if (ccBin > 0) {
                sql += " AND cc.bin = " + ccBin;
            }
            
            if (wasRerouted == Constants.TRANSACTION_DEPOSIT_WAS_NOT_REROUTS) {
            	 sql += " AND t.is_rerouted =  0";
            } else if (wasRerouted == Constants.TRANSACTION_DEPOSIT_WAS_REROUTS){
            	sql += " AND t.is_rerouted =  1";
            }
            
            
            if (manualRouting == Constants.TRANSACTION_DEPOSIT_IS_NOT_MANUAL_ROUTE) {
            	sql += " AND t.is_manual_routing = 0";
           } else if (manualRouting == Constants.TRANSACTION_DEPOSIT_IS_MANUAL_ROUTE){
        	   sql += " AND t.is_manual_routing = 1";
           }
            
            if (!CommonUtil.isParameterEmptyOrNull(affiliatesKeys) && !affiliatesKeys.contentEquals("0")) {
            	sql += " AND u.affiliate_key in (" + affiliatesKeys + ") ";
            }
            
            if( is3d > -1) {
            	sql += " AND t.is_3d = " + is3d + " ";
            }

			sql += " order by " + timeField + " desc";

			ps = con.prepareStatement(sql);

			if (byPhone) {
				ps.setLong(index++, TransactionsManagerBase.TRANS_TYPE_CC_DEPOSIT);
			} else {
				ps.setLong(index++, TransactionsManagerBase.TRANS_TYPE_CC_WITHDRAW);
			}
			ps.setLong(index++, providerTransactionId);
			ps.setTimestamp(index++, CommonUtil.convertToTimeStamp(CommonUtil.getDateTimeFormat(from, Utils.getAppData().getUtcOffset())));
			if (endHour == 99){								//if we dont have end time field and we want to check for specific day.
				to = CommonUtil.addDay(to) ;
			}
			ps.setTimestamp(index++, CommonUtil.convertToTimeStamp(CommonUtil.getDateTimeFormat(to , Utils.getAppData().getUtcOffset())));
			if (transactionId!=0 && null != arabicStartDate) {
				ps.setLong(index++, transactionId);
				ps.setTimestamp(index++, CommonUtil.convertToTimeStamp(CommonUtil.getDateTimeFormat(arabicStartDate , Utils.getAppData().getUtcOffset())));
			}
			if (transactionId!=0 && null == arabicStartDate) {
				ps.setLong(index++, transactionId);
			}
			if(null != arabicStartDate && transactionId==0){
				ps.setTimestamp(index++, CommonUtil.convertToTimeStamp(CommonUtil.getDateTimeFormat(arabicStartDate , Utils.getAppData().getUtcOffset())));
			}
			
			rs = ps.executeQuery();
			while(rs.next()) {
				Transaction t = getVO(con, rs);
                SpecialCare specialCare = new SpecialCare();
				t.setUserName(rs.getString("username"));
				t.setUserFirstName(rs.getString("firstname"));
				t.setUserLastName(rs.getString("lastname"));
				t.setSkin(CommonUtil.getMessage(app.getSkinById(rs.getInt("skin_id")).getDisplayName(),null));
				t.setUserCountryCode(rs.getString("country_code"));
                specialCare.setTimeScTurnover(convertToDate(rs.getTimestamp("TIME_SC_TURNOVER")));
                specialCare.setTimeScHouseResult(convertToDate(rs.getTimestamp("TIME_SC_HOUSE_RESULT")));
                t.setSpecialCare(specialCare);
				if (t.getCreditCard() != null) {
					t.getCreditCard().setTypeNameByBin(rs.getString("card_type"));
					t.getCreditCard().setCountryName(rs.getString("cc_country"));
					t.getCreditCard().getType().setName(rs.getString("cc_name"));
				}
                //t.setSpecialCare(rs.getString("TIME_SC_TURNOVER") != null || rs.getString("TIME_SC_HOUSE_RESULT") != null);
				t.setClearingProviderName(rs.getString("provider_name"));
				long haveCreditsWithdrawals = rs.getLong("HAVE_WITHDRAWALS");
				if (haveCreditsWithdrawals > 0 ){
					t.setHaveCredits(true);
				} else {
					t.setHaveCredits(false);
				}
				t.setCreditAmount(rs.getLong("credit_amount"));
				t.setSplittedReferenceId(rs.getLong("splitted_reference_id"));
				t.setDepositReferenceId(rs.getLong("deposit_reference_id"));
				long creditWithdrawal = rs.getLong("is_credit_withdrawal");
				if (creditWithdrawal == 1) {
					t.setCreditWithdrawal(true);
				} else {
					t.setCreditWithdrawal(false);
				}
				t.setPaymentTypeDesc(rs.getString("directProviderName"));
				t.setPaymentTypeName(rs.getString("paymentMethodName"));
				t.setPaymentTypeId(rs.getLong("payment_type_id"));
				t.setCountryId(rs.getLong("countryId"));
				t.setCountryName(CommonUtil.getMessage(rs.getString("country"),null));
                t.setCampaignName(rs.getString("campaign_name"));
                t.setBusinessCaseName(rs.getString("business_case_name"));
                double rate = rs.getDouble("rate");
                t.setBaseAmount(Math.round(rate * t.getAmount()));
                t.setPayPalEmail(rs.getString("paypal_email"));
                t.setMoneybookersEmail(rs.getString("moneybookers_email"));
                t.setWebMoneyPurse(rs.getString("webmoney_purse"));
                t.setReroutingTranId(rs.getLong("rerouting_transaction_id"));
                t.setIsRerouted(rs.getBoolean("is_rerouted"));
                t.setTransactionPostponed(new TransactionPostponed());
                t.getTransactionPostponed().setId(rs.getLong("tp_id"));
                t.getTransactionPostponed().setNumberOfDays(rs.getInt("tp_number_of_days"));
                t.getTransactionPostponed().setWriterId(rs.getLong("tp_writer_id"));
                t.getTransactionPostponed().setTimeUpdated(convertToDate(rs.getTimestamp("tp_time_updated")));
                
                TransactionDAO.setActionStatuses(t);
				list.add(t);
			}
		} catch (	CryptoException | InvalidKeyException | NoSuchAlgorithmException | NoSuchPaddingException | IllegalBlockSizeException
					| BadPaddingException | UnsupportedEncodingException | InvalidAlgorithmParameterException ce) {
			throw new SQLException(ce.getMessage());
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		return list;

	}

	public static ArrayList<Transaction> getTransactions(Connection con, long id1, BigDecimal id2) throws SQLException {

		PreparedStatement ps = null;
		ResultSet rs = null;
		ArrayList<Transaction> list = new ArrayList<Transaction>();

		try {
			String sql = "select t.*,tt.description typename,tt.class_type classtype, u.currency_id " +
						 " from transactions t,transaction_types tt, users u where t.user_id=u.id and " +
						 "t.type_id=tt.id ";

			if (id2 != null)
				sql += " and (t.id=" + id1 + " or t.id=" + id2.longValue() + ")";
			else
				sql += " and t.id=" + id1;

			ps = con.prepareStatement(sql);

			rs = ps.executeQuery();
			while(rs.next()) {
				Transaction t = getVO(con, rs);
				list.add(t);
			}
		}

		finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		return list;

	}

	public static long getNumberOfDeposits(Connection con, long id) throws SQLException {

		PreparedStatement ps = null;
		ResultSet rs = null;

		try {
			String sql =
					" select count(*) total from transactions t" +
					" where t.user_id=? and " + 
					" t.status_id in (" + TransactionsManagerBase.TRANS_STATUS_SUCCEED + "," +
										TransactionsManagerBase.TRANS_STATUS_PENDING + "," + TransactionsManagerBase.TRANS_STATUS_REVERSE_WITHDRAW + ")"
					+ " and t.type_id in (" + TransactionsManagerBase.TRANS_TYPS_ALL_DEPOSITS_TXT + ")";


			ps = con.prepareStatement(sql);
			ps.setLong(1, id);

			rs = ps.executeQuery();

			if (rs.next()) {
				return rs.getLong("total");
			}
		}

		finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		return 0;

	}

	public static long getNumberOfWithdraws(Connection con, long id) throws SQLException {

		PreparedStatement ps = null;
		ResultSet rs = null;

		try {
			String sql =
					" SELECT " +
						" count(*) total " +
					" FROM " +
						" transactions t " +
					" WHERE " +
						" t.user_id = ? " +
						" AND " + " t.type_id in " +
						" ( " + TransactionsManagerBase.TRANS_TYPE_ADMIN_WITHDRAW + ","
							  + TransactionsManagerBase.TRANS_TYPE_CHEQUE_WITHDRAW + ","
							  + TransactionsManagerBase.TRANS_TYPE_CC_WITHDRAW + ","
							  + TransactionsManagerBase.TRANS_TYPE_BANK_WIRE_WITHDRAW + ","
							  + TransactionsManagerBase.TRANS_TYPE_EFT_WITHDRAW + ","
							  + TransactionsManagerBase.TRANS_TYPE_MONEYBOOKERS_WITHDRAW + ","
							  + TransactionsManagerBase.TRANS_TYPE_WEBMONEY_WITHDRAW + ","
							  + TransactionsManagerBase.TRANS_TYPE_CUP_CHINA_WITHDRAW + ","
							  + TransactionsManagerBase.TRANS_TYPE_BAROPAY_WITHDRAW + ","
							  + TransactionsManagerBase.TRANS_TYPE_DIRECT24_WITHDRAW + ","
							  + TransactionsManagerBase.TRANS_TYPE_GIROPAY_WITHDRAW + ","
							  + TransactionsManagerBase.TRANS_TYPE_EPS_WITHDRAW + ","						  
							  + TransactionsManagerBase.TRANS_TYPE_ENVOY_WITHDRAW + ","
							  + TransactionsManagerBase.TRANS_TYPE_IDEAL_WITHDRAW + ")" +
						" AND t.status_id in (" + TransactionsManagerBase.TRANS_STATUS_SUCCEED + ")";

			ps = con.prepareStatement(sql);
			ps.setLong(1, id);

			rs = ps.executeQuery();

			if (rs.next()) {
				return rs.getLong("total");
			}
		}

		finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		return 0;

	}

	public static ArrayList getByUserAndDates(Connection con,long id,Date from,Date to,String status) throws SQLException {

		  PreparedStatement ps=null;
		  ResultSet rs=null;
		  ArrayList<Transaction> list=new ArrayList<Transaction>();
		  try
			{
			    String sql=" select t.*,ty.description typename,ty.class_type classtype,ts.description statusdesc, u.currency_id currency_id " +
			    		" from transactions t,transaction_types ty,transaction_statuses ts, users u " +
			    		" where t.user_id=u.id and t.type_id=ty.id and t.status_id=ts.id and ";

			    if (id!=0)
			    		sql+="user_id="+id+" and ";

			    sql+=" trunc(time_created,'DDD')>=trunc(?,'DDD') and " +
			    	"  trunc(time_created,'DDD')<=trunc(?,'DDD') and " +
			    	"  status_id in ("+status+")" +
			    	"  order by time_created desc";

				ps = con.prepareStatement(sql);
				ps.setTimestamp(1, new Timestamp(from.getTime()));
				ps.setTimestamp(2, new Timestamp(to.getTime()));

				rs=ps.executeQuery();

				while (rs.next()) {
					Transaction t=getVO(con,rs);

				    list.add(t);
				}
			}

			finally
			{
				closeResultSet(rs);
				closeStatement(ps);
			}
			return list;

	  }


	/**
	 * Update feeCancle field after transaction editing
	 * @param con
	 * @param tran
	 * 		The transaction for update
	 * @throws SQLException
	 */
	public static void updateFeeCancel(Connection con, Transaction tran) throws SQLException {
		PreparedStatement ps = null;
		try {

			String sql = "update transactions set fee_cancel=? where id=? ";

			ps = con.prepareStatement(sql);

			boolean feeCancel = tran.isFeeCancel();

			if (feeCancel) {
				ps.setInt(1, ConstantsBase.CC_FEE_CANCEL_YES);
			}
			else {
				ps.setInt(1, ConstantsBase.CC_FEE_CANCEL_NO);
			}

			ps.setLong(2, tran.getId());

			ps.executeUpdate();

		}
		finally {
			closeStatement(ps);
		}
	}


	/**
	 * Update transaction after accounting approved
	 * @param con
	 * @param tran
	 * 			The transaction for update
	 * @throws SQLException
	 */
	public static void updateAfterAccouningApproved(Connection con, Transaction tran) throws SQLException {
		PreparedStatement ps = null;
		try {

			String sql = "update transactions set is_accounting_approved=? where id=? ";

			ps = con.prepareStatement(sql);

			boolean accountingApproved = tran.isAccountingApproved();

			if (accountingApproved) {
				ps.setInt(1, ConstantsBase.ACCOUNTING_APPROVED_YES);
			}
			else {
				ps.setInt(1, ConstantsBase.ACCOUNTING_APPROVED_NO);
			}

			ps.setLong(2, tran.getId());

			ps.executeUpdate();

		}
		finally {
			closeStatement(ps);
		}
	}

	/**
	 * Update fee_cancel_by_admin in case the admin prees the check box and want
	 * to cancel the fee for specific transaction
	 * @param con  db connection
	 * @param feeCancel admin fee cancel
	 * @param id transaction id
	 * @throws SQLException
	 */
	public static void updateFeeCancelByAdmin(Connection con, boolean feeCancel, long id) throws SQLException {
		PreparedStatement ps = null;
		try {

			String sql = "update transactions set fee_cancel_by_admin=?, update_by_admin = 1 where id=? ";

			ps = con.prepareStatement(sql);

			if (feeCancel) {
				ps.setInt(1, ConstantsBase.CC_FEE_CANCEL_YES);
			}
			else {
				ps.setInt(1, ConstantsBase.CC_FEE_CANCEL_NO);
			}

			ps.setLong(2, id);

			ps.executeUpdate();

		}
		finally {
			closeStatement(ps);
		}
	}

	/**
	 * Get capture daily information: all transactions that should capture today
	 * @param con
	 * @return
	 * @throws SQLException
	 */
	public static ArrayList<CaptureDailyInfo> getCaptureDailyInfo(Connection con, long investStatus) throws SQLException {
		  PreparedStatement ps = null;
		  ResultSet rs = null;
		  ArrayList<CaptureDailyInfo> list = new ArrayList<CaptureDailyInfo>();

		  try {
	        String sql =
	        	"SELECT " +
		            "t.user_id userid, " +
		            "u.user_name, " +
		            "t.id trxId, " +
		            "c.cc_number ccnumber, " +
		            "curr.code, " +
		            "t.amount amount, " +
		            "cp.name cpName " +
		        "FROM " +
	            	"transactions t, " +
	            	"credit_cards c, " +
	            	"users u, " +
	            	"currencies curr, " +
	            	"clearing_providers cp " +
	    	    "WHERE " +
		            "t.user_id = u.id AND " +
		            "t.credit_card_id = c.id AND " +
		            "curr.id = u.currency_id AND " +
		            "u.class_id <> 0 AND " +
		            "cp.id = t.clearing_provider_id AND " +
		            "t.status_id = " + TransactionsManagerBase.TRANS_STATUS_PENDING + " AND " +
		            "t.type_id = " + TransactionsManagerBase.TRANS_TYPE_CC_DEPOSIT + " AND " +
		            "t.time_created >= ? - 1 - 18/24 AND " +
                   	"t.time_created < ? - 18/24 ";

	        if (investStatus > 0) {
	        	String invSql = "AND ";
	        	if (investStatus == Constants.CAPTURE_INFO_INVEST) {
	        		invSql += "EXISTS ";
	        	} else if (investStatus == Constants.CAPTURE_INFO_NOT_INVEST) {
	        		invSql += "NOT EXISTS ";
	        	}
	        	invSql += "(SELECT 1 " +
	        			   "FROM investments i " +
	        			   "WHERE i.user_id = u.id AND " +
	        			   	 	"i.is_canceled = 0) ";
	        	sql += invSql;
	        }

	        sql += "ORDER BY " +
	        		  "curr.code, " +
	        		  "user_name, " +
	        		  "trxId";

	        	Calendar c = Calendar.getInstance();
	        	int hour = Constants.CAPTURE_RUN_ISR_HOUR;
	        	String writerUtcOffset = Utils.getWriter().getUtcOffset();
	        	hour -= CommonUtil.getUtcOffsetValue(writerUtcOffset);
	        	c.set(Calendar.HOUR_OF_DAY, hour);
	        	c.set(Calendar.MINUTE, 0);
	        	c.set(Calendar.SECOND, 0);

	        	ps = con.prepareStatement(sql);
	        	ps.setTimestamp(1, CommonUtil.convertToTimeStamp(c.getTime()));
	        	ps.setTimestamp(2, CommonUtil.convertToTimeStamp(c.getTime()));
	        	rs = ps.executeQuery();
	        	while (rs.next()) {
	                CaptureDailyInfo ci = new CaptureDailyInfo();
	                ci.setUserId(rs.getLong("userid"));
	                ci.setUserName(rs.getString("user_name"));
	                ci.setTransactionId(rs.getLong("trxId"));
	                ci.setCcNumber(AESUtil.decrypt(rs.getString("ccnumber")));
	                ci.setCurrencyCode(rs.getString("code"));
	                ci.setAmount(rs.getLong("amount"));
	                ci.setProviderName(rs.getString("cpName"));
	                list.add(ci);
	        	}
		} catch (	CryptoException | InvalidKeyException | IllegalBlockSizeException | BadPaddingException | NoSuchAlgorithmException
					| NoSuchPaddingException | InvalidAlgorithmParameterException ce) {
			// FIXME very bad way of handling exceptions
			throw new SQLException(ce.getMessage());
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
			return list;
	  }

	/**
	 * Get transaction for cancel action after success
	 * @param con
	 * @param id
	 * @return
	 * @throws SQLException
	 */
    public static Transaction getByIdForCancel(Connection con, long id) throws SQLException {
        PreparedStatement ps = null;
        ResultSet rs = null;
        Transaction vo = null;
        try {
            String sql =
                "SELECT " +
                    "t.*, " +
                    "ty.description typename, " +
                    "ty.class_type classtype, " +
                    "u.currency_id currency_id " +
                "FROM " +
                    "transactions t, " +
                    "transaction_types ty, " +
                    "users u " +
                "WHERE " +
                    "t.user_id = u.id AND " +
                    "t.type_id = ty.id AND " +
                    "t.type_id in (?,?,?) AND " +
                    "t.status_id = ? AND " +
                    "t.id = ?";
            ps = con.prepareStatement(sql);
            ps.setLong(1, TransactionsManagerBase.TRANS_TYPE_WIRE_DEPOSIT);
            ps.setLong(2, TransactionsManagerBase.TRANS_TYPE_EFT_DEPOSIT);
            ps.setLong(3, TransactionsManagerBase.TRANS_TYPE_CASH_DEPOSIT);
            ps.setLong(4, TransactionsManagerBase.TRANS_STATUS_SUCCEED);
            ps.setLong(5, id);
            rs = ps.executeQuery();
            if (rs.next()) {
            	vo = getVO(con, rs);
                return vo;
            }
        } finally {
            closeResultSet(rs);
            closeStatement(ps);
        }
        return vo;
    }

	public static void updateIsCreditWithdrawal(Connection con, boolean isCreditWithdrawal, long id) throws SQLException {
		PreparedStatement ps = null;
		try {
			String sql = "update transactions set is_credit_withdrawal=? where id=? ";

			ps = con.prepareStatement(sql);
			if (isCreditWithdrawal) {
				ps.setInt(1, 1);
			} else {
				ps.setInt(1, 0);
			}
			ps.setLong(2, id);

			ps.executeUpdate();
		} finally {
			closeStatement(ps);
		}
	}

	public static void updateClearingProviderId(Connection con, long tranId, long clearingProviderId) throws SQLException {
		PreparedStatement ps = null;
		try {
			String sql =
				"UPDATE " +
					"transactions " +
				"SET " +
					"clearing_provider_id = ? " +
				"WHERE " +
					"id = ? ";

			ps = con.prepareStatement(sql);
			ps.setLong(1, clearingProviderId);
			ps.setLong(2, tranId);
			ps.executeUpdate();
		} finally {
			closeStatement(ps);
		}
	}

	/**
	 * Update transaction
	 * Used for update withdrawal statuses in case of failure
	 * @param con
	 * @param tranId
	 * @param statusId
	 * @param typeId
	 * @param writerId 
	 * @param capturedProviderId
	 * @throws SQLException
	 */
	public static transactionUpdateStatus updateTransaction(Connection con, long tranId, long writerId, long typeId, long statusId, long providerId, String capturedProviderId, long transactionClassId) throws SQLException {
		PreparedStatement ps = null;
		con.setAutoCommit(false);
		transactionUpdateStatus updateStatus;
		try {
			String sql =
				"UPDATE " +
					"transactions t " +
				"SET " +
					"t.processed_writer_id = ? ";

			if (typeId > 0) {
				sql += ", t.type_id = " + typeId + " ";
				if (providerId > 0) {
					sql += ", t.clearing_provider_id = " + providerId + " ";
				}
			} else if (typeId < 0) {
				if (typeId == -172l) {
					sql += ", t.type_id = 17 ";
					sql += ", t.payment_type_id = 2 ";
				}
				if (typeId == -173l) {
					sql += ", t.type_id = 17 ";
					sql += ", t.payment_type_id = 3 ";
				}
				if (typeId == -174l) {
					sql += ", t.type_id = 17 ";
					sql += ", t.payment_type_id = 4 ";
				}
				if (providerId > 0) {
					sql += ", t.clearing_provider_id = " + providerId + " ";
				}
			}
				
			
				if (statusId > 0) {
					sql +=
						",t.status_id = " + statusId + " ";
				}

				if (statusId == TransactionsManager.TRANS_STATUS_SUCCEED) {
					sql +=
						",t.time_settled = sysdate " +
						",t.utc_offset_settled = (select utc_offset from users u where t.user_id = u.id) ";
				}

				if (statusId == TransactionsManager.TRANS_STATUS_REQUESTED) {  // reverse to the first status (on withdrawal request)
					sql +=
						", t.is_accounting_approved = 0 ";
				}

				if (statusId == TransactionsManager.TRANS_STATUS_SUCCEED ||
						statusId == TransactionsManager.TRANS_STATUS_APPROVED ||
						statusId == TransactionsManager.TRANS_STATUS_SECOND_APPROVAL) {   // remove failed description
					sql +=
						",t.description = NULL ";
				}

				if (!CommonUtil.isParameterEmptyOrNull(capturedProviderId)) {
					sql +=
						",t.xor_id_capture = '" + capturedProviderId + "' ";
				}

				sql +=
					"WHERE " +
//						"t.type_id = ? AND " +
						"t.id = ? AND " +
						"(select tt.class_type from transactions t  join transaction_types tt on tt.id = t.type_id where t.id = ?) = ?";
				

			ps = con.prepareStatement(sql);

			ps.setLong(1, writerId);
//			ps.setLong(2, TransactionsManager.TRANS_TYPE_CC_WITHDRAW); //TODO: think on other types as well...
			ps.setLong(2, tranId);
			ps.setLong(3, tranId);
			ps.setLong(4, transactionClassId);
			int res = ps.executeUpdate();
			if (res == 1) { // transaction update succeed
				updateStatus = transactionUpdateStatus.TRANS_UPDATED;
			} else { // transaction update failed
				updateStatus = transactionUpdateStatus.TRANS_FAILED;
			}
			
			if (typeId > 0 && updateStatus == transactionUpdateStatus.TRANS_UPDATED) { // when changing the type we must update balance history table
				String sqlBH = "UPDATE balance_history " + 
								"set command = (select command_id from balance_history_mapping where transaction_type = ?) " +
								"where key_value = ?";
				ps = con.prepareStatement(sqlBH);
				ps.setLong(1, typeId);
				ps.setLong(2, tranId);
				res = ps.executeUpdate();
				if (res == 1) { // balance update succeed
					updateStatus = transactionUpdateStatus.TRANS_BALANCE_UPDATED;
				} else { // balance update failed
					updateStatus = transactionUpdateStatus.TRANS_UPDATED_BALANCE_FAILED;
				}
			}			
			
			if (updateStatus == transactionUpdateStatus.TRANS_FAILED) { // if the update(s) was(were) unsuccessful we rollback
				con.rollback();
			} else { // if the update(s) was(were) successful we commit
				con.commit();
			}
			
			return updateStatus;
		} catch (SQLException e) {
			con.rollback();
			logger.debug(e);
			return transactionUpdateStatus.TRANS_FAILED;
		} finally {
			con.setAutoCommit(true);
			closeStatement(ps);
		}
	}

	public static HashMap<CreditCard, ArrayList<Transaction>> getCreditCardDeposits(Connection con, long userId, Date from, Date to) throws SQLException{
		  PreparedStatement ps = null;
		  ResultSet rs = null;
		  long ccId = 0;
		  CreditCard keyCC = null;
		  ArrayList<Transaction> trans = null;
		  HashMap<CreditCard , ArrayList<Transaction>> ccTransactions = new HashMap<CreditCard, ArrayList<Transaction>>();

		  try {
			  String sql =
				  "SELECT " +
			         "t.credit_card_id, " +
			         "cc.cc_number, " +
			         "cct.name, " +
			         "cc.holder_name, " +
			         "t.time_created, " +
			         "t.amount, " +
			         "t.id " +
					"FROM " +
				        "transactions t, " +
				        "credit_cards cc, " +
				        "credit_card_types cct " +
					"WHERE " +
				        "t.user_id = ? AND " +
				        "t.type_id = ? AND " +
				        "t.time_created  BETWEEN ? AND ? +1 AND " +
				        "t.status_id in (" + TransactionsManager.TRANS_STATUS_SUCCEED + "," +
										TransactionsManager.TRANS_STATUS_PENDING + "," +
										TransactionsManager.TRANS_STATUS_CANCELED_ETRADER + ") AND " +
				        "t.credit_card_id = cc.id AND " +
				        "cc.type_id = cct.id " +
					"ORDER BY " +
					 "t.credit_card_id, t.time_created DESC ";

			  ps = con.prepareStatement(sql);
			  ps.setLong(1, userId);
			  ps.setLong(2, TransactionsManager.TRANS_TYPE_CC_DEPOSIT);
			  ps.setTimestamp(3, CommonUtil.convertToTimeStamp(from));
			  ps.setTimestamp(4, CommonUtil.convertToTimeStamp(to));
			  rs = ps.executeQuery();

			  while (rs.next()) {
				  if(ccId != rs.getLong("credit_card_id")){
					  keyCC = new CreditCard();
					  trans = new ArrayList<Transaction>();
					  ccId = rs.getLong("credit_card_id");
					  keyCC.setId(rs.getLong("credit_card_id"));
					  keyCC.setHolderName(rs.getString("holder_name"));
					  keyCC.setSumDeposits(rs.getLong("amount"));
					  CreditCardType type = new CreditCardType();
					  type.setName(rs.getString("name"));
					  keyCC.setType(type);
					  try {
						  keyCC.setCcNumber(Long.valueOf(AESUtil.decrypt(rs.getString("cc_number"))));
					  } catch (Exception e) {
						  e.printStackTrace();
					  }
					  ccTransactions.put(keyCC, trans);
				  }
				  Transaction t = new Transaction();
				  t.setTimeCreated(rs.getDate("time_created"));
				  t.setId(rs.getLong("id"));
				  t.setAmount(rs.getLong("amount"));
				  trans.add(t);
			  }
		  } finally {
			  closeResultSet(rs);
			  closeStatement(ps);
		  }
		  return ccTransactions;
	}
	
	public static ArrayList<Transaction> getNonCreditCardDepositsByUserId(Connection con, long userId, Date from, Date to) throws SQLException {
		ArrayList<Transaction> nonCreditCardDepositsList = new ArrayList<>();

		PreparedStatement ps = null;
		ResultSet rs = null;

		try {
			 String sql = "SELECT T.TIME_CREATED, " +
					 			 "T.AMOUNT, " +
					 			 "T.ID, " +
					 			 "T.USER_ID, " +
					 			 "T.PAYMENT_TYPE_ID, " +
					 			 "T.TYPE_ID, " +
					 			 "TT.DESCRIPTION AS trans_desc, " +
					 			 "pm.description AS payment_desc " +
					 	    "FROM TRANSACTIONS T " +
					 	    "JOIN TRANSACTION_TYPES TT " +
					 	      "ON T.TYPE_ID = TT.ID " +
					 	    "LEFT JOIN PAYMENT_METHODS PM " +
					 	      "ON T.PAYMENT_TYPE_ID = PM.ID " +
					 	   "WHERE T.USER_ID = ? " +
					 	     "AND TT.CLASS_TYPE = 1 " +
					 	     "AND TT.ID <> 1 " +
					 	     "AND T.TIME_CREATED BETWEEN ? AND ? +1 " +
					 	     "AND T.STATUS_ID IN (2, 7, 8) " +
					 	   "ORDER BY T.TIME_CREATED DESC ";

			ps = con.prepareStatement(sql);
			ps.setLong(1, userId);
			ps.setTimestamp(2, CommonUtil.convertToTimeStamp(from));
			ps.setTimestamp(3, CommonUtil.convertToTimeStamp(to));
			rs = ps.executeQuery();
			while (rs.next()) {

				Transaction t = new Transaction();
				t.setTimeCreated(rs.getDate("time_created"));
				t.setAmount(rs.getLong("amount"));
				t.setId(rs.getLong("id"));
				
				if (rs.getString("payment_desc") == null) {
					t.setDescription(rs.getString("trans_desc"));
				} else {
					t.setDescription(rs.getString("payment_desc"));
				}
				
				nonCreditCardDepositsList.add(t);
			}

		} finally {

		}
		return nonCreditCardDepositsList;
	}


	 /**
	   * Get deposits information by date for withdrawal form that we are sending to our users
	   * @param con
	   * @param from taking all deposits before "from" date
	   * @param userId the transactions of the user id
	   * @return
	   * @throws SQLException
	   */
	  public static Transaction getNonCcDeposits(Connection con, long userId) throws SQLException {
		  PreparedStatement ps = null;
		  ResultSet rs = null;
		  Transaction t = null;
		  try {
			  String sql =
				  "SELECT " +
			         "SUM(t.amount) amount, " +
			         "MIN(GET_TIME_BY_OFFSET(t.time_created, t.utc_offset_created)) from_date, " +
			         "MAX(GET_TIME_BY_OFFSET(t.time_created, t.utc_offset_created)) to_date " +
					"FROM " +
				        "transactions t " +
					"WHERE " +
				        "t.user_id = ? AND " +
				        "t.time_created >= sysdate -180 AND " + // TODO "from" and "to" dates in case there is requirements for that
				        "t.type_id  = " + TransactionsManager.TRANS_TYPE_WIRE_DEPOSIT + " AND " +
				        "t.status_id in (" + TransactionsManager.TRANS_STATUS_SUCCEED + "," +
										TransactionsManager.TRANS_STATUS_PENDING + ") ";

			  ps = con.prepareStatement(sql);
			  ps.setLong(1, userId);
			  rs = ps.executeQuery();

			  if (rs.next()) {
				  if (rs.getLong("amount") != 0){
					  t = new Transaction();
					  t.setDepositsFrom(convertToDate(rs.getTimestamp("from_date")));
					  t.setDepositsTo(convertToDate( rs.getTimestamp("to_date")));
					  t.setAmount(rs.getLong("amount"));
				  }
			  }
		  } finally {
			  closeResultSet(rs);
			  closeStatement(ps);
		  }
		  return t;
	  }

    /**
     * Get WebMoney Details by purse number
	 * @param con
	 * @param purseNumber
	 * @return
	 * @throws SQLException
	 */
	public static String getWebMoneyDetailsByPurseNumber(Connection con, String purseNumber) throws SQLException {
		  PreparedStatement ps = null;
		  ResultSet rs = null;
		  String webMoneyId = null;
		  try {
			  String sql =" SELECT " +
				  				" wd.lmi_payer_wm " +
				  			" FROM " +
				  				" webmoney_deposit wd " +
				  			" WHERE " +
				  				" wd.lmi_payer_purse = ? ";
			  ps = con.prepareStatement(sql);
			  ps.setString(1, purseNumber);
			  rs = ps.executeQuery();

			  if (rs.next()) {
				  webMoneyId = rs.getString("LMI_PAYER_WM");
			  }
		  } finally {
			  closeResultSet(rs);
			  closeStatement(ps);
		  }
		  return webMoneyId;
	  }
	public static User getNoneIssueTransactionByTime(Connection con) throws SQLException{
		PreparedStatement ps = null;
		ResultSet rs = null;
		User user = null;
		try {
			String sql =" SELECT u.* " +
						" FROM users u " ;
		  ps = con.prepareStatement(sql);
		  rs = ps.executeQuery();

		  if(rs.next()) {
			  user = new User();
			  user.setId(rs.getLong("id"));
		  }

	  } finally {
		  closeResultSet(rs);
		  closeStatement(ps);
	  }
	  return user;
	}

	public static long getSumDepositByDateRangeAndUserId(Connection con, long userId, Date sumOfDepositsDateFrom, Date sumOfDepositsDateTo) throws Exception {
		PreparedStatement ps = null;
		ResultSet rs = null;
		long sumOfDeposits = 0;
		//SimpleDateFormat fmt = new SimpleDateFormat("dd/MM/yyyy");
		try {
			String sql = " SELECT " +
						 " 		sum(t.amount) as sum_of_deposits " +
						 " FROM " +
						 "		transactions t " +
						 " WHERE " +
						 "		t.user_id = ? " +
						 "		AND t.status_id in (?,?,?)" +
						 "		AND t.time_created >= ? " +
					     " 		AND t.time_created <= ? ";
//						 "		AND t.time_created between to_date('"+ fmt.format(sumOfDepositsDateFrom) + "','dd/MM/yyyy') " +
//											     " and to_date('"+ fmt.format(sumOfDepositsDateTo) + "','dd/MM/yyyy') +1) ";
		  ps = con.prepareStatement(sql);
		  ps.setLong(1, userId);
		  ps.setInt(2, TransactionsManagerBase.TRANS_STATUS_SUCCEED);
		  ps.setInt(3, TransactionsManagerBase.TRANS_STATUS_REVERSE_WITHDRAW);
		  ps.setInt(4, TransactionsManagerBase.TRANS_STATUS_PENDING);
		  ps.setTimestamp(5, CommonUtil.convertToTimeStamp(CommonUtil.getDateTimeFormat(sumOfDepositsDateFrom,  Utils.getWriter().getUtcOffset())));
		  ps.setTimestamp(6, CommonUtil.convertToTimeStamp(CommonUtil.getDateTimeFormat(sumOfDepositsDateTo,  Utils.getWriter().getUtcOffset())));
		  rs = ps.executeQuery();

		  if(rs.next()) {
			  sumOfDeposits = rs.getLong("sum_of_deposits");
		  }

	  } finally {
		  closeResultSet(rs);
		  closeStatement(ps);
	  }
	  return sumOfDeposits;
	}

	public static boolean userMadeDepositsInLastXDays(Connection con, long userId, Date date) throws SQLException {
		  PreparedStatement ps = null;
		  ResultSet rs = null;

		  try {
			  String sql =
				  "SELECT " +
			           " t.*," +
			           " tt.* " +
					"FROM " +
				        "transactions t, " +
				        "transaction_types tt " +
					"WHERE " +
				        "t.user_id = ? AND " +
				        "t.type_id = tt.id AND " +
				        "tt.class_type = ? AND " +
				        "t.time_created  >= ? AND " +
				        "t.status_id in (" + TransactionsManager.TRANS_STATUS_SUCCEED + "," +
										TransactionsManager.TRANS_STATUS_PENDING + "," +
										TransactionsManager.TRANS_STATUS_CANCELED_ETRADER + ")" +
					"ORDER BY " +
				        "t.time_created ";

			  ps = con.prepareStatement(sql);
			  ps.setLong(1, userId);
			  ps.setLong(2, TransactionsManager.TRANS_CLASS_DEPOSIT);
			  ps.setTimestamp(3, CommonUtil.convertToTimeStamp(date));
			  rs = ps.executeQuery();

			  if (rs.next()) {
				  return true;
			  }
		  } finally {
			  closeResultSet(rs);
			  closeStatement(ps);
		  }
		  return false;
	}
	
	public static long getSumBonusesTransactions(Connection con, long userId) throws SQLException {

		PreparedStatement ps = null;
		ResultSet rs = null;

		try {
			String sql = "select sum(tr.amount) total  " +
					     " from  " +
					     " transactions tr,  " +
					     " bonus_users bu  " +
					     " where  " +
					     " tr.bonus_user_id=bu.id  " +
					     " and bu.user_id=tr.user_id  " +
					     " and tr.type_id=12  " +
					     " and tr.status_id=2  " +
					     " and bu.user_id= ?  ";
			ps = con.prepareStatement(sql);
			ps.setLong(1, userId);

			rs = ps.executeQuery();
			if (rs.next()) {
				return rs.getLong("total");
			}
		}

		finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		return 0;

	}
	
	/**
	 * The function return all transaction that were inserted mentally to TRANSACTION_ISSUES.
	 * @param con
	 * @param startDate
	 * @param endDate
	 * @param offset
	 * @param writerFilter
	 * @return
	 * @throws SQLException
	 */
	public static ArrayList<TransactionReport> getTransactionInsertedManually(Connection con, Date startDate, Date endDate, String writerFilter) throws SQLException {

		  PreparedStatement ps=null;
		  ResultSet rs=null;

		  TransactionReport vo = null;
		  ArrayList<TransactionReport> transaction = new ArrayList<TransactionReport>();

		  try
			{
				String sql = " SELECT " +
								" t.id TransactionId, " +
								" t.time_created TransactionDate, " +
								" w.user_name Rep, " +
								" ia.id IssueActionId, " +
								" ti.time_inserted TimeInserted, " +
								" ia.comments IssueComment, " +
								" u.mobile_phone MobilePhone, " +
								" u.land_line_phone LandLinePhone " +
							" FROM " +
								" transactions_issues ti, " +
								" users u, " +
								" issue_actions ia, " +
								" transactions t, " +
								" writers w " +
							" WHERE " +
								" u.id = t.user_id " +
								" AND t.id = ti.transaction_id " +
								" AND ti.issue_action_id = ia.id " +
								" AND ia.writer_id = w.id " +
								" AND u.class_id <> 0 " +
								" AND ti.insert_type = " + Constants.TRANSACTION_ISSUE_INSERT_TYPE_MANUALLY;
				if (!writerFilter.equals(ConstantsBase.ALL_REP)){
					sql += " AND w.id in (" + writerFilter + ") ";
				}
				sql += "AND t.time_created  between ? and ? ";
			  
			  ps = con.prepareStatement(sql);
			  ps.setTimestamp(1, CommonUtil.convertToTimeStamp(startDate));
			  ps.setTimestamp(2 , CommonUtil.convertToTimeStamp(endDate));
			  rs=ps.executeQuery();
			  while (rs.next()) {
					vo = new TransactionReport();
					vo.setTransactionId(rs.getLong("TransactionId"));
					vo.setTransactionDate(rs.getDate("TransactionDate"));
					vo.setWriterName(rs.getString("Rep"));
					vo.setIssueActionId(rs.getLong("IssueActionId"));
					vo.setTimeInserted(rs.getDate("TimeInserted"));
					vo.setIssueComment(rs.getString("IssueComment"));
					vo.setUsersMobilePhone(rs.getString("MobilePhone"));
					vo.setUsersLandLinePhone(rs.getString("LandLinePhone"));					
					transaction.add(vo);
				}
			}
			finally
			{
				closeResultSet(rs);
				closeStatement(ps);
			}
			return transaction;
	  }
	
	public static ArrayList<ReroutingLimit> getReroutingLimit(Connection con) throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		ArrayList<ReroutingLimit> list = new ArrayList<ReroutingLimit>();
		try {
			String sql =
						"SELECT a.display_name as LIMIT_DYSPLAY_NAME, " +
								" a.limit_type_id, " + 
								" case when a.limit_type=1 then 'Currency' " + 
									 " when a.limit_type=2 then 'Card type' " + 
									 " when a.limit_type=3 then 'BIN' " + 
									 " when a.limit_type=4 then 'Country' end as LIMIT_TYPE , " +
							    " a.limit_type as TYPE, " +
							    " decode(a.Inatec,0,'+','-') as INATEC, " +
							    " decode(a.Wire_Card,0,'+','-') as Wire_Card, " +
							    " decode(a.TBI_Anyoption,0,'+','-') as TBI_Anyoption," +
							    " decode(a.Wire_Card_3D,0,'+','-') as Wire_Card_3D," +
							    " decode(a.WAll,0,'+','-') as WAll " +

 						" FROM " + 
 							" (select " +
 								" to_char(crr.name_key) as display_name, " +
 								" currs.currency_id as limit_type_id, " +
 								" 1 as limit_type," +
								" sum(currs.Inatec) as Inatec , " +
								" sum(currs.Wire_Card) as Wire_Card, " +
								" sum(currs.TBI_Anyoption) as TBI_Anyoption, " +
								" sum(currs.Wire_Card_3D) as Wire_Card_3D, " +
								" sum(currs.WAll) as WAll " +
							  " FROM " +
							  	" (select curr.currency_id, " +
							  		" nvl((select distinct 1 from  clearing_providers_groups t where t.group_id=1 and t.group_id=curr.clearing_provider_group_id),0) as Inatec, " +
									" nvl((select  distinct 1 from  clearing_providers_groups t where t.group_id=2 and t.group_id=curr.clearing_provider_group_id),0) as Wire_Card, " +
									" nvl((select distinct 1 from  clearing_providers_groups t where t.group_id=3 and t.group_id=curr.clearing_provider_group_id),0) as TBI_Anyoption, " +
									" nvl((select distinct 1 from  clearing_providers_groups t where t.group_id=4 and t.group_id=curr.clearing_provider_group_id),0) as Wire_Card_3D, " +
									" nvl((select distinct 1 from  clearing_providers_groups t where t.group_id=5 and t.group_id=curr.clearing_provider_group_id),0) as WAll " +
								   " FROM " + 
								     " clearing_routing_limitations curr " +
								   " WHERE " + 
									 " curr.currency_id is not null " +
									 " and curr.is_active=1 " +
								   " GROUP BY " +
									 " curr.currency_id, curr.clearing_provider_group_id " +

								" ) currs, " +
								" currencies crr" +
							   " WHERE " +
							   " currs.currency_id=crr.id " +
							   " GROUP BY currency_id,crr.name_key " +
						" UNION ALL " + 
						" SELECT " +
							" to_char(ct.description), " +
							" ccts.credit_card_type_id, " +
							" 2 as limit_type, " +
							" sum(ccts.Inatec) as Inatec , " +
							" sum(ccts.Wire_Card) as Wire_Card, " +
							" sum(ccts.TBI_Anyoption) as TBI_Anyoption, " +
							" sum(ccts.Wire_Card_3D) as Wire_Card_3D, " +
							" sum(ccts.WAll) as WAll " +
						" FROM " +
							" (" +
								" SELECT cct.credit_card_type_id, " +
									" nvl((select distinct 1 from  clearing_providers_groups t where t.group_id=1 and t.group_id=cct.clearing_provider_group_id),0) as Inatec, " +
									" nvl((select  distinct 1 from  clearing_providers_groups t where t.group_id=2 and t.group_id=cct.clearing_provider_group_id),0) as Wire_Card, " +
									" nvl((select distinct 1 from  clearing_providers_groups t where t.group_id=3 and t.group_id=cct.clearing_provider_group_id),0) as TBI_Anyoption, " +
									" nvl((select distinct 1 from  clearing_providers_groups t where t.group_id=4 and t.group_id=cct.clearing_provider_group_id),0) as Wire_Card_3D, " +
									" nvl((select distinct 1 from  clearing_providers_groups t where t.group_id=5 and t.group_id=cct.clearing_provider_group_id),0) as WAll " +
								" FROM " + 
									" clearing_routing_limitations cct " +
								" WHERE  " +
									" cct.credit_card_type_id is not null  " +
									" and cct.is_active=1 " +
								" GROUP BY cct.credit_card_type_id, cct.clearing_provider_group_id " +							
							" ) ccts, " +
						" credit_card_types ct " +
						" WHERE ccts.credit_card_type_id=ct.id " +
						" GROUP BY ct.description, ccts.credit_card_type_id " +
					" UNION ALL " +
					" SELECT " +
						" to_char(bins.bin_id), " +
						" bins.bin_id, " +
						" 3 as limit_type, " +
						" sum(bins.Inatec) as Inatec , " +
						" sum(bins.Wire_Card) as Wire_Card, " +
						" sum(bins.TBI_Anyoption) as TBI_Anyoption, " +
						" sum(bins.Wire_Card_3D) as Wire_Card_3D, " +
						" sum(bins.WAll) as WAll " +
					" FROM " +
					" (  " +
						" SELECT " + 
							" bi.bin_id, " +
							" nvl((select distinct 1 from  clearing_providers_groups t where t.group_id=1 and t.group_id=bi.clearing_provider_group_id),0) as Inatec, " +
							" nvl((select  distinct 1 from  clearing_providers_groups t where t.group_id=2 and t.group_id=bi.clearing_provider_group_id),0) as Wire_Card, " +
							" nvl((select distinct 1 from  clearing_providers_groups t where t.group_id=3 and t.group_id=bi.clearing_provider_group_id),0) as TBI_Anyoption, " +
							" nvl((select distinct 1 from  clearing_providers_groups t where t.group_id=4 and t.group_id=bi.clearing_provider_group_id),0) as Wire_Card_3D, " +
							" nvl((select distinct 1 from  clearing_providers_groups t where t.group_id=5 and t.group_id=bi.clearing_provider_group_id),0) as WAll " +
						" FROM  " +
							" clearing_routing_limitations bi " +
						" WHERE  " +
							" bi.bin_id is not null " +
							" and bi.is_active=1 " +
						" GROUP BY bi.bin_id, bi.clearing_provider_group_id " +						
					" ) bins  " +
					" GROUP BY bins.bin_id " +
					" UNION ALL " +
					" SELECT " +
						" to_char(cntr.country_name), " +
						" countr.country_id, " +
						" 4 as limit_type, " +
						" sum(countr.Inatec) as Inatec , " +
						" sum(countr.Wire_Card) as Wire_Card, " +
						" sum(countr.TBI_Anyoption) as TBI_Anyoption, " +
						" sum(countr.Wire_Card_3D) as Wire_Card_3D, " +
						" sum(countr.WAll) as WAll " +
					" FROM " +
					" ( " +
						" SELECT " + 
							" co.country_id, " +
							" nvl((select distinct 1 from  clearing_providers_groups t where t.group_id=1 and t.group_id=co.clearing_provider_group_id),0) as Inatec, " +
							" nvl((select  distinct 1 from  clearing_providers_groups t where t.group_id=2 and t.group_id=co.clearing_provider_group_id),0) as Wire_Card, " +
							" nvl((select distinct 1 from  clearing_providers_groups t where t.group_id=3 and t.group_id=co.clearing_provider_group_id),0) as TBI_Anyoption, " +
							" nvl((select distinct 1 from  clearing_providers_groups t where t.group_id=4 and t.group_id=co.clearing_provider_group_id),0) as Wire_Card_3D, " +
							" nvl((select distinct 1 from  clearing_providers_groups t where t.group_id=5 and t.group_id=co.clearing_provider_group_id),0) as WAll " +
						" FROM  " +
							" clearing_routing_limitations co " +
						" WHERE  " +
							" co.country_id is not null " +
							" and co.is_active=1 " +
						" GROUP BY co.country_id, co.clearing_provider_group_id " +					
					" ) countr, " +
					" countries cntr " +
					" WHERE  " +
						" countr.country_id=cntr.id " +
					" GROUP BY countr.country_id,cntr.country_name " +
					" ) a ";
				           
			ps = con.prepareStatement(sql);
			rs = ps.executeQuery();
			while (rs.next()) {
				ReroutingLimit vo = new ReroutingLimit();
				vo.setLimitDysplayName(rs.getString("LIMIT_DYSPLAY_NAME"));
				vo.setLimitTypeId(rs.getLong("limit_type_id"));
				vo.setLimitType(rs.getString("LIMIT_TYPE"));
				vo.setType(rs.getLong("TYPE"));
				if (rs.getString("INATEC").contains("+")) {
					vo.setInatec(true);
				} else {
					vo.setInatec(false);
				}
				if (rs.getString("Wire_Card").contains("+")) {
					vo.setWireCard(true);
				} else {
					vo.setWireCard(false);
				}
				if (rs.getString("TBI_Anyoption").contains("+")) {
					vo.setTbiAnyoption(true);
				} else {
					vo.setTbiAnyoption(false);
				}
				if (rs.getString("Wire_Card_3D").contains("+")) {
					vo.setWireCard3d(true);
				} else {
					vo.setWireCard3d(false);
				}
				if (rs.getString("WAll").contains("+")) {
					vo.setWall(true);
				} else {
					vo.setWall(false);
				}
				list.add(vo);
			}
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		return list;	
	}
		
	public static boolean setReroutingLimit(Connection con, ReroutingLimit re, int provider) throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		String sql = "";
		String limitType = "";
		int isActive=0;
		
		if(provider == Constants.INATEC){
			isActive=(re.isInatec()?0:1);
		}else if (provider == Constants.WIRE_CARD){
			isActive=(re.isWireCard()?0:1);
		}else if (provider == Constants.TBI){
			isActive=(re.isTbiAnyoption()?0:1);
		}else if (provider == Constants.WIRE_CARD_3D){
			isActive=(re.isWireCard3d()?0:1);
		}else if (provider == Constants.WALPAY){
			isActive=(re.isWall()?0:1);
		}
		
		if(re.getType() == Constants.REROUTING_LIMIT_CURRENCY_ID){
			limitType=" currency_id ";
        }else if(re.getType() == Constants.REROUTING_LIMIT_CREDIT_CARD_TYPE_ID){
        	limitType=" credit_card_type_id ";
        }else if(re.getType() == Constants.REROUTING_LIMIT_BIN_ID){
        	limitType=" bin_id ";
        }else if(re.getType() == Constants.REROUTING_LIMIT_COUNRTY_ID){
        	limitType=" country_id ";
        }
        
		if(isExistReroutingLimit(con,re,provider)){
			sql=" Update clearing_routing_limitations set is_active = "+ isActive +" where "+ limitType +" = " 
				+re.getLimitTypeId() + " and clearing_provider_group_id = "+ provider;
		}else{
			sql=" Insert into clearing_routing_limitations " +
					  " (id, clearing_provider_group_id, "+ limitType +", is_active) " +
				  " values "+ 
			           " (SEQ_CLEARING_ROUTING_LIMIT.nextval, "+ provider +", "+ re.getLimitTypeId() + ", "+ isActive +") ";
		}
	
		try {			
			ps = con.prepareStatement(sql);
			rs = ps.executeQuery();
		} finally {
			closeStatement(ps);
		}
		return true;
	}
	
	private static boolean isExistReroutingLimit(Connection con,ReroutingLimit re, int providerGroup) throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		String whereStatement="";
        
		if(re.getType() == Constants.REROUTING_LIMIT_CURRENCY_ID){
        	whereStatement=" currency_id = "+re.getLimitTypeId() + " and clearing_provider_group_id = " + providerGroup;
        }else if(re.getType() == Constants.REROUTING_LIMIT_CREDIT_CARD_TYPE_ID){
        	whereStatement=" credit_card_type_id = "+re.getLimitTypeId() + " and clearing_provider_group_id = " + providerGroup;
        }else if(re.getType() == Constants.REROUTING_LIMIT_BIN_ID){
        	whereStatement=" bin_id = "+re.getLimitTypeId() + " and clearing_provider_group_id = " + providerGroup;
        }else if(re.getType() == Constants.REROUTING_LIMIT_COUNRTY_ID){
        	whereStatement=" country_id = "+re.getLimitTypeId() + " and clearing_provider_group_id = " + providerGroup;
        }
		
		try {
			String sql = "select * from clearing_routing_limitations where " + whereStatement;

			ps = con.prepareStatement(sql);
			rs = ps.executeQuery();
			if (rs.next()) {
				return true;
			}
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		return false;
	}
	
	public static ArrayList<SelectItem> getFeeList(Connection con) throws SQLException{
		PreparedStatement ps = null;
		ResultSet rs = null;
		ArrayList<SelectItem> list = new ArrayList<SelectItem>();
		String sql = "SELECT * FROM fees";
		
 		try {
 			ps = con.prepareStatement(sql);
 			rs = ps.executeQuery();
		    while (rs.next()) {
		    	list.add(new SelectItem(rs.getLong("id"),rs.getString("name")));
		    }
		    return list;
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
	}
	
	public static ArrayList<FeeCurrencyMap> getFeeCurrencyList(Connection con) throws SQLException{
		PreparedStatement ps = null;
		ResultSet rs = null;
		ArrayList<FeeCurrencyMap> list = new ArrayList<FeeCurrencyMap>();
		String sql = " SELECT fc_map.id, fc_map.fee_id, f.name, fc_map.currency_id, c.code, fc_map.amount, fc_map.time_created, writer_id  " + 
                     " FROM fee_currency_map fc_map, currencies c, fees f " +
                     " WHERE fc_map.currency_id = c.id" +
                     " AND fc_map.fee_id = f.id ORDER BY fc_map.id desc";
		try {
			ps = con.prepareStatement(sql);
 			rs = ps.executeQuery();
		    while (rs.next()) {
		    	list.add(feeCurrencyMapObject(rs));
		    }
		    return list;
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
	} 
	
	public static void updateFeeCurrencyMap(Connection con, FeeCurrencyMap map) throws SQLException {
		PreparedStatement ps = null;
		String sql = "UPDATE " +
				     " fee_currency_map " +
				     " SET " +
				     " amount = ?," +
				     " writer_id = ?, " +
				     " time_created = sysdate " +
				     " WHERE  " +
				     " fee_id = ? " +
				     " AND " +
				     " currency_id = ?" ;
		try { 
			ps = con.prepareStatement(sql);
			ps.setLong(1, map.getAmount());
			ps.setLong(2, map.getWriterId());
			ps.setLong(3, map.getFeeId());
			ps.setLong(4, map.getCurrencyId());
			ps.executeUpdate();
		} finally {
			closeStatement(ps);
		}
	}
	
	public static FeeCurrencyMap feeCurrencyMapObject(ResultSet rs) throws SQLException {
		FeeCurrencyMap obj = new FeeCurrencyMap();
		obj.setId(rs.getLong("id"));
		obj.setCurrencyCode(rs.getString("code"));
		obj.setFeeName(rs.getString("name"));
		obj.setAmount(rs.getLong("amount"));
		obj.setWriterId(rs.getLong("writer_id"));
		obj.setTimeCreated(convertToDate(rs.getTimestamp("time_created")));
		obj.setFeeId(rs.getLong("fee_id"));
		obj.setCurrencyId(rs.getLong("currency_id"));
		return obj;
	}
	
	
	
	public static ArrayList getWithdrawls(Connection con) throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		ArrayList list = null;
		
		String sql = "SELECT * "
				+ " FROM transaction_types"
				+ " WHERE class_type = ? " 
				+ " AND is_active = ? "
				+ " AND id not in (?)";
		try {
			ps = con.prepareStatement(sql);
			ps.setLong(1, TransactionsManagerBase.TRANSACTION_CLASS_TYPE_REAL_WITHDRAWALS);
			ps.setLong(2, IS_ACTIVE_TRUE);
			ps.setLong(3,  FOCAL_PAYMENTS);
			
			rs = ps.executeQuery();
			list = new ArrayList();
			list.add(new SelectItem(new Long(0),"All"));
			while (rs.next()) {
				list.add(new SelectItem(new Long(rs.getLong("id")), rs.getString("description")));
			}
			return list;
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
	}
	
	public static long getFirstWithdrawls(Connection con, Date firstDayInMonth, Date dateTransaction, long userId) throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
	    String sql = "SELECT (min(t.id)) as id "
				+    " FROM transactions t, transaction_types tt"
				+ 	 " WHERE  ? <= t.time_created AND t.time_created <= ? AND " +
					 " t.type_id = tt.id AND " +
				     " tt.class_type = ? AND " +
				     //" is_accounting_approved = ? AND"+
				     " t.user_id =  ?";
		
	    ps = con.prepareStatement(sql);
	    ps.setTimestamp(1, CommonUtil.convertToTimeStamp(firstDayInMonth));
	    ps.setTimestamp(2, CommonUtil.convertToTimeStamp(dateTransaction));
		ps.setLong(3, TransactionsManagerBase.TRANS_CLASS_WITHDRAW);
		//ps.setInt(4, Constants.ACCOUNTING_APPROVED_NO);
		ps.setLong(4, userId);
		rs = ps.executeQuery();
		while (rs.next()) {
			return rs.getLong("id");
		}
		return 0;
	}	
	
	public static ArrayList<DepositsSummary> getDepositsSummaryList(Connection con, long userId) throws Exception {

		  PreparedStatement ps=null;
		  ResultSet rs=null;

		  DepositsSummary vo = null;
		  ArrayList<DepositsSummary> depositsList = new ArrayList<DepositsSummary>();

		  try
			{
				String sql = "SELECT " + 
								" tp.id type_id, " +
								" tp.description, " +
								" case when tr.type_id = 1 " +
									 " then to_char(cct.name) else  '' " +
							     " end as card_details, " +
							     " nvl(cc.cc_number,'X') cc_num, " +
							     " count(*) as count, " +
							     " sum(tr.amount) as amount, " + 
							     " u.currency_id, " +
							     " max(tr.time_created) as max_date " +       
							  " FROM " +
							     " transactions tr, " +
							     " transaction_types tp, " +
							     " credit_cards cc, " +
							     " credit_card_types cct, " +
							     " users u " +
							  " WHERE " + 
							     " tr.user_id = u.id " +
							     " and tr.time_created > u.time_created " +
							     " and tr.type_id = tp.id " +
							     " and tp.class_type in (1,5) " +
							     " and tr.status_id in (2,7) " +
							     " and tr.credit_card_id = cc.id(+) " +
							     " and cc.type_id = cct.id(+) " +
							     " and u.id = ? " +
							  " GROUP BY " +
							     " tp.id, " +
							     " tp.description, " +
							     " case when tr.type_id = 1 " +
							        " then to_char(cct.name) else  '' " +
							     " end , " +
							     " cc.cc_number," +
							     " u.currency_id " +
							   " ORDER BY " + 
							     " 6 desc ";
			  
			  ps = con.prepareStatement(sql);
			  ps.setLong(1, userId);
			  rs=ps.executeQuery();
			  while (rs.next()) {
					vo = new DepositsSummary();
					vo.setTypeId(rs.getLong("type_id"));
					vo.setDepositType(rs.getString("description"));
					vo.setCcName(rs.getString("card_details"));
					String cc_num = rs.getString("cc_num");
					if(!cc_num.contains("X")){
						cc_num = AESUtil.decrypt(rs.getString("cc_num"));
						vo.setCcLast4Num(cc_num.substring(cc_num.length() - 4, cc_num.length()));
					}
					vo.setCount(rs.getLong("count"));
					vo.setAmount(rs.getLong("amount"));
					vo.setCurrency(rs.getLong("currency_id"));
					vo.setLastDeposit(convertToDate(rs.getTimestamp("max_date")));
															
					depositsList.add(vo);
				}
			}
			finally
			{
				closeResultSet(rs);
				closeStatement(ps);
			}
			return depositsList;
	  }	
	
	public static boolean isDisplaySkrill(Connection con, long userId) throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs=null;
		int index = 1;
		try {
			String sql = "SELECT " + 
								" COUNT(*) CNT " +
						 " FROM " + 
								" TRANSACTIONS T " +		 
						 " WHERE " + 
								" T.USER_ID = ? " + 
								" AND T.TYPE_ID = ? " + 
								" AND T.STATUS_ID = ? ";
			
			ps = con.prepareStatement(sql);
			ps.setLong(index++, userId);
			ps.setLong(index++, TransactionsManagerBase.TRANS_TYPE_MONEYBOOKERS_DEPOSIT);
			ps.setLong(index++, TransactionsManagerBase.TRANS_STATUS_SUCCEED);
			rs = ps.executeQuery();
			
			if (rs.next()) {
				if (rs.getLong("CNT") > 0) {
					return true;
				}
			}
		} finally {
			closeStatement(ps);
			closeResultSet(rs);
		}
		return false;
	}
}
