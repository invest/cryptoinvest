package il.co.etrader.backend.dao_managers;

import il.co.etrader.backend.util.Utils;
import il.co.etrader.bl_vos.MarketingCampaign;
import il.co.etrader.bl_vos.MarketingCampaignManagers;
import il.co.etrader.util.CommonUtil;
import il.co.etrader.util.ConstantsBase;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import javax.faces.model.SelectItem;

import com.anyoption.common.daos.DAOBase;


public class MarketingCampaignsDAO extends DAOBase {

	/**
	 * Insert a new campaign
	 * @param con  db connection
	 * @param vo MarketingCampaigns instance
	 * @throws SQLException
	 */
	public static void insert(Connection con,MarketingCampaign vo) throws SQLException {

		  PreparedStatement ps = null;
		  ResultSet rs = null;

		  try {

				String sql = "insert into marketing_campaigns(id,name,payment_type_id,start_date,end_date,payment_amount,comments," +
						     "writer_id,payment_recipient_id, time_created, territory_id, source_id, priority, description, campaign_manager, marketing_channel_priority_id, unit_price) " +
						     "values(seq_mar_campaigns.nextval,?,?,?,?,?,?,?,?,sysdate,?,?,?,?,?,?,?)";

				ps = con.prepareStatement(sql);

				ps.setString(1, vo.getName());
				ps.setLong(2, vo.getPaymentTypeId());
				ps.setTimestamp(3, CommonUtil.convertToTimeStamp(CommonUtil.getDateTimeFormat(vo.getStartDate(), Utils.getAppData().getUtcOffset())));
				if ( null == vo.getEndDate() ) {
					ps.setTimestamp(4, null);
				} else {
					ps.setTimestamp(4, CommonUtil.convertToTimeStamp(CommonUtil.getDateTimeFormat(vo.getEndDate(), Utils.getAppData().getUtcOffset())));
				}
				ps.setLong(5, vo.getPaymentAmount());
				ps.setString(6, vo.getComments());
				ps.setLong(7, vo.getWriterId());
				ps.setLong(8, vo.getPaymentRecipientId());
				ps.setLong(9, vo.getTerritoryId());
				ps.setLong(10, vo.getSourceId());
				ps.setInt(11, vo.getPriority());
				ps.setString(12, vo.getDescription().replace("\r\n", "<br/>"));
                ps.setLong(13, vo.getCampaignManagerId());
                ps.setLong(14, vo.getMarketingChannelId());
                ps.setDouble(15, vo.getUnitPrice());

				ps.executeUpdate();

				vo.setId(getSeqCurValue(con,"seq_mar_campaigns"));
		  }
		  finally {
				closeStatement(ps);
				closeResultSet(rs);
			}

	  }

	  /**
	   * Update marketing campaign
	   * @param con db connection
	   * @param vo  marketingCampaign instance
	   * @throws SQLException
	   */
	  public static void update(Connection con,MarketingCampaign vo) throws SQLException {

		  PreparedStatement ps = null;
		  ResultSet rs = null;

		  try {

			  String sql = " UPDATE " +
                               " marketing_campaigns " +
			  			   " SET " +
			  			       " name = ?," +
                               " payment_type_id = ?, " +
                               " start_date = ?, " +
                               " end_date = ?, " +
			  			   	   " payment_amount = ?, " +
                               " comments = ?, " +
                               " writer_id = ?, " +
                               " payment_recipient_id = ?, " +
                               " territory_id = ?, " +
                               " source_id = ?, " +
                               " priority = ?, " +
                               " description = ?, " +
                               " campaign_manager = ?, " +
                               " last_updated_by = ?, " +
                               " marketing_channel_priority_id = ?," +
                               " unit_price = ?" +
			  			   " WHERE " +
			  			       " id = ? ";

				ps = con.prepareStatement(sql);


				ps.setString(1, vo.getName());
				ps.setLong(2, vo.getPaymentTypeId());
				ps.setTimestamp(3, CommonUtil.convertToTimeStamp(CommonUtil.getDateTimeFormat(vo.getStartDate(), Utils.getAppData().getUtcOffset())));
				if ( null == vo.getEndDate() ) {
					ps.setTimestamp(4, null);
				} else {
					ps.setTimestamp(4, CommonUtil.convertToTimeStamp(CommonUtil.getDateTimeFormat(vo.getEndDate(), Utils.getAppData().getUtcOffset())));
				}
				ps.setLong(5, vo.getPaymentAmount());
				ps.setString(6, vo.getComments());
				ps.setLong(7, vo.getWriterId());
				ps.setLong(8, vo.getPaymentRecipientId());
				ps.setLong(9, vo.getTerritoryId());
				ps.setLong(10, vo.getSourceId());
				ps.setInt(11, vo.getPriority());
				ps.setString(12, vo.getDescription().replace("\r\n", "<br/>"));
                ps.setLong(13, vo.getCampaignManagerId());
                ps.setLong(14, vo.getLastUpdatedBy());
                ps.setLong(15, vo.getMarketingChannelId());
                ps.setDouble(16, vo.getUnitPrice());
				ps.setLong(17, vo.getId());
				

				ps.executeUpdate();

		  }
		  finally	{
				closeStatement(ps);
				closeResultSet(rs);
			}

	  }


	  /**
	   * Update campaign name
	   * @param con
	   * @param vo MarketingCampaign instance
	   * @throws SQLException
	   */
	  public static void updateName(Connection con,MarketingCampaign vo) throws SQLException {

		  PreparedStatement ps = null;
		  ResultSet rs = null;

		  try {

			  String sql = "update marketing_campaigns set name=? " +
			  			   "where id=?";

				ps = con.prepareStatement(sql);

				ps.setString(1, vo.getName());
				ps.setLong(2, vo.getId());

				ps.executeUpdate();

		  }
		  finally	{
				closeStatement(ps);
				closeResultSet(rs);
			}

	  }
	  
	  public static boolean isUpdatePriorityId(Connection con, Long campaignId, Long priorityId) throws SQLException {
		  
		  PreparedStatement ps = null;
		  ResultSet rs = null;

		  try {

			  String sql = "select marketing_channel_priority_id from  marketing_campaigns " +
			  			   "where id=? and marketing_channel_priority_id=?";

				ps = con.prepareStatement(sql);

				ps.setLong(1, campaignId);
				ps.setLong(2, priorityId);

				rs = ps.executeQuery();
				
				if (rs.next()) {
					return false;
				}
				
		  }
		  finally	{
				closeStatement(ps);
				closeResultSet(rs);
			}
		  return true;
	  }

	  /**
	   * Get all campaigns
	   * @param con  db connection
	 * @param from  campaigns start date
	 * @param to	campaigns end date
	 * @param paymentType	  payment type filter
	 * @param campaignName  campaign name filter
	 * @param isNotFormMarketingTab TODO
	   * @return
	   *	ArrayList of MarketingCampaigns
	   * @throws SQLException
	   */
	  public static ArrayList<MarketingCampaign> getAll(Connection con, Date from, Date to, long paymentType,
				String campaignName, String campaignId, long paymentRecipient, long writerIdForSkin, long marketingChannelId, long marketingCampaignManager) throws SQLException {

		  PreparedStatement ps = null;
		  ResultSet rs = null;
		  ArrayList<MarketingCampaign> list = new ArrayList<MarketingCampaign>();

		  try {
				String sql = " SELECT " +
								" mc.*, " +
								" pt.description paymentTypeDesc, " +
								" pr.agent_name, " +
                                " w1.user_name created_by, " +
                                " w2.user_name manager, " +
                                " mac.description marketingChannel " + 
							 " FROM " +
							 	" marketing_campaigns mc " +
							 	"      LEFT JOIN writers w1 ON w1.id = mc.writer_id " +
							 	"      LEFT JOIN marketing_channel mac " +
							 	"      on mac.id = mc.marketing_channel_priority_id " +
                                "      LEFT JOIN writers w2 ON w2.id = mc.campaign_manager, " +
							 	" marketing_payment_types pt, " +
							 	" marketing_payment_recipients pr " +
							 " WHERE " +
							 	" mc.payment_type_id = pt.id " +
							 	" AND mc.payment_recipient_id = pr.id ";

				if (writerIdForSkin > 0) {  // don't check skin in the marketing pages
					sql += 	" AND mc.id in (select mco.campaign_id " +
											" from marketing_combinations mco " +
							 				" where mco.skin_id in (select ws.skin_id from writers_skin ws where ws.writer_id = ?)) ";
				}

				if ( null != from ) {
					sql += "AND mc.start_date >= ? ";
				}

				if ( null != to ) {
					sql += "AND (mc.end_date <= ? or mc.end_date is null) ";
				}

				if ( paymentType > 0 ) {
					sql += "AND mc.payment_type_id = " + paymentType;
				}

				if ( !CommonUtil.isParameterEmptyOrNull(campaignName) ) {
					sql += "AND upper(mc.name) like '%" + campaignName.toUpperCase() + "%' ";
				}
				
				if (marketingChannelId > 0) {
					sql+= "AND mc.marketing_channel_priority_id = " + marketingChannelId;
				}

				if ( !CommonUtil.isParameterEmptyOrNull(campaignId) ) {
					sql += "AND mc.id =" + campaignId + " ";
				}

				if ( paymentRecipient > 0 ) {
					sql += "AND mc.payment_recipient_id = " + paymentRecipient;
				}
				
				if ( marketingCampaignManager > 0 ) {
					sql += "AND w2.id = " + marketingCampaignManager;
				}

				sql += " ORDER BY upper(mc.name)";

				ps = con.prepareStatement(sql);
				int parameterIndex = 1;

				if (writerIdForSkin > 0){
					ps.setLong(parameterIndex++, writerIdForSkin);
				}

				if ( null != from ) {
					ps.setTimestamp(parameterIndex++, CommonUtil.convertToTimeStamp(CommonUtil.getDateTimeFormat(from, Utils.getAppData().getUtcOffset())));
				}

				if ( null != to ) {
					ps.setTimestamp(parameterIndex++, CommonUtil.convertToTimeStamp(CommonUtil.getDateTimeFormat(CommonUtil.addDay(to), Utils.getAppData().getUtcOffset())));
				}

				rs = ps.executeQuery();

				Locale l = new Locale(ConstantsBase.LOCALE_DEFAULT);
				while (rs.next()) {
					MarketingCampaign vo = getVO(rs);
					if (null != vo.getDescription()) {
						vo.setDescription(vo.getDescription().replace("<br/>", "\r\n"));
					}
					vo.setPaymentTypeDescription(CommonUtil.getMessage(rs.getString("paymentTypeDesc"),null,l));
					vo.setPaymentRecipientAgent(rs.getString("agent_name"));
                    vo.setCreatedByWriter(rs.getString("created_by"));
                    vo.setCampaignManagerTxt(rs.getString("manager"));
                    if (rs.getLong("marketing_channel_priority_id") > 0){
                    	vo.setMarketingChannelTxt(CommonUtil.getMessage(rs.getString("marketingChannel"),null,l));
                    } else {
                    	vo.setMarketingChannelTxt("");
                    }
					list.add(vo);
				}

			} finally {
					closeResultSet(rs);
					closeStatement(ps);
			}

			return list;
	  }

	  /**
	   * Check if campaign name in use
	   * @param con   db connection
	   * @param mc MarketingCampaign instance
	   * @return true if name in use
	   * @throws SQLException
	   */
	  public static boolean isNameInUse(Connection con, MarketingCampaign mc) throws SQLException {

		  ResultSet rs = null;
		  PreparedStatement ps = null;

		  try {

			  String sql = "select * " +
			  			   "from marketing_campaigns " +
			  			   "where payment_type_id = ? and payment_recipient_id = ? " +
			  			   "and territory_id = ? and source_id = ? ";

			  if ( mc.getId() > 0 ) {  // for update
				  sql += "and id <> " + mc.getId();
			  }

			  ps = con.prepareStatement(sql);
			  ps.setLong(1, mc.getPaymentTypeId());
			  ps.setLong(2, mc.getPaymentRecipientId());
			  ps.setLong(3, mc.getTerritoryId());
			  ps.setLong(4, mc.getSourceId());

			  rs = ps.executeQuery();

			  if ( rs.next() ) {
				  return true;
			  }
		  } finally {
			  closeResultSet(rs);
			  closeStatement(ps);
		  }

		  return false;

	  }

	  /**
	   * Get VO
	   * @param rs
	   * 	Result set instance
	   * @return
	   * 	MarketingCampaigns object
	   * @throws SQLException
	   */
	  private static MarketingCampaign getVO(ResultSet rs) throws SQLException {

		  MarketingCampaign vo = new MarketingCampaign();

			vo.setId(rs.getLong("id"));
			vo.setName(rs.getString("name"));
			vo.setPaymentTypeId(rs.getLong("payment_type_id"));
			vo.setStartDate(convertToDate(rs.getTimestamp("start_date")));
			if ( null == rs.getDate("end_date") ) {
				vo.setEndDate(null);
			} else {
				vo.setEndDate(convertToDate(rs.getTimestamp("end_date")));
			}
			vo.setTimeCreated(convertToDate(rs.getTimestamp("time_created")));
			vo.setPaymentAmount(rs.getLong("payment_amount"));
			vo.setComments(rs.getString("comments"));
			vo.setWriterId(rs.getLong("writer_id"));
			vo.setPaymentRecipientId(rs.getLong("payment_recipient_id"));
			vo.setTerritoryId(rs.getLong("territory_id"));
			vo.setSourceId(rs.getLong("source_id"));
			vo.setPriority(rs.getInt("priority"));
			vo.setDescription(rs.getString("description"));
            vo.setCampaignManagerId(rs.getLong("campaign_manager"));
            vo.setMarketingChannelId(rs.getLong("marketing_channel_priority_id"));
            vo.setUnitPrice(rs.getDouble("unit_price"));
			return vo;
	  }
	  
	  public static ArrayList<SelectItem> getMarketingChannels(Connection con) throws SQLException {
		  PreparedStatement ps = null;
		  ResultSet rs = null;
		  ArrayList<SelectItem> list = new ArrayList<SelectItem>();

		  try {

				String sql = "select * " +
							 "from marketing_channel ";


				ps = con.prepareStatement(sql);
				rs = ps.executeQuery();

				while (rs.next()) {
					list.add(new SelectItem(new Long(rs.getLong("id")),rs.getString("description")));
				}

			} finally {
					closeResultSet(rs);
					closeStatement(ps);
			}

			return list;
	  }

	public static MarketingCampaign getMarketingCampaignByUserId(Connection con, long combinationId) throws Exception {
		  ResultSet rs = null;
		  PreparedStatement ps = null;
		  MarketingCampaign vo = null;
		  try {

			  String sql = " SELECT " +
			  			   "      mc.* " +
			  			   " FROM " +
			  			   "	  users u, " +
			  			   "	  marketing_campaigns mc, " +
			  			   "	  marketing_combinations mco " +
			  			   " WHERE " +
			  			   "	  mco.id = u.combination_id " +
			  			   "      AND mco.campaign_id = mc.id " +
			  			   "      AND u.id = ? " ;

			  ps = con.prepareStatement(sql);
			  ps.setLong(1, combinationId);
			  rs = ps.executeQuery();

			  if (rs.next()) {
				  vo = new MarketingCampaign();
				  vo = getVO(rs);
			  }
		  } finally {
			  closeResultSet(rs);
			  closeStatement(ps);
		  }

		  return vo;
	}
	
	  public static ArrayList<MarketingCampaignManagers> getAllMarketingCampaignManagers(Connection con) throws SQLException {

		  PreparedStatement ps = null;
		  ResultSet rs = null;
		  ArrayList<MarketingCampaignManagers> list = new ArrayList<MarketingCampaignManagers>();

		  try {

				String sql = " select " +
								" w.id, w.user_name " +
							" from " +
								" marketing_campaigns ms, " +
								" writers w " +
							" where " +
								"  ms.campaign_manager=w.id " +
							" group by  " +
								" w.id, w.user_name " +
							" order by " +
								" 2"; 

				ps = con.prepareStatement(sql);
				rs = ps.executeQuery();

				while (rs.next()) {
					MarketingCampaignManagers vo = new MarketingCampaignManagers();
					vo.setCampaignManagerId(rs.getLong("id"));
					vo.setCampaignManagerUserame(rs.getString("user_name"));
					
					list.add(vo);
				}

			} finally {
					closeResultSet(rs);
					closeStatement(ps);
			}

			return list;
	  }

}

