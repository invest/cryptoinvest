package il.co.etrader.backend.dao_managers;

import il.co.etrader.backend.bl_vos.OpportunityQuotes0100;
import il.co.etrader.backend.util.Utils;
import il.co.etrader.util.CommonUtil;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.MarketDynamicsQuoteParams;
import com.anyoption.common.daos.DAOBase;
import com.anyoption.common.util.DynamicsUtil;

public class OpportunityQuotes0100DAO extends DAOBase {
	private static Logger log = Logger.getLogger(OpportunityQuotes0100.class);

	public static void insertOpportunityQuotes0100(Connection con, OpportunityQuotes0100 vo) throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			String sql = "INSERT INTO opportunity_quotes_0_100( " +
							 "id, ";
					if (null != vo.getOpportunityId()) {
					 sql +=  "opportunity_id, ";
					}
					 sql +=  "market_id, " +
							 "time_created, " +
							 "screen_type, " +
							 "writer_id, " +
							 "comments) " +
						"VALUES(" +
		                 	 "seq_opportunity_quotes_0_100.nextval, ";
		         if (null != vo.getOpportunityId()) {
		              sql += "?, ";
		         }
		         	  sql += "?, " +
		                 	 "sysdate, " +
		                 	 "?, " +
		                 	 "?, " +
		                 	 "?) ";
			ps = con.prepareStatement(sql);
			int i = 1;
			if (null != vo.getOpportunityId()) {
				ps.setLong(i++, vo.getOpportunityId());
			}
			ps.setLong(i++, vo.getMarketId());
			ps.setLong(i++, vo.getScreenType());
			ps.setLong(i++, vo.getWriterId());
			ps.setString(i++, vo.getComment());
			ps.executeUpdate();
		} finally {
			closeStatement(ps);
			closeResultSet(rs);
		}
	}

	public static ArrayList<OpportunityQuotes0100> updateList(Connection con, Date from, long marketId, long oppId) throws Exception {
		PreparedStatement ps = null;
		ResultSet rs = null;
		ArrayList<OpportunityQuotes0100> list = new ArrayList<OpportunityQuotes0100>();

		try {
			String sql =
					"SELECT " +
						"oq.*, " +
						"m.display_name, " +
						"to_char(o.time_est_closing, 'YYYY-MM-DD HH24:MI:SS.FF3 TZHTZM') AS time_est_closing, " +
						"w.user_name " +
					"FROM " +
						"opportunity_quotes_0_100 oq " +
							"left join opportunities o on o.id = oq.opportunity_id, " +
						"markets m, " +
						"writers w " +
					"WHERE " +
						"m.id = oq.market_id AND " +
						"w.id = oq.writer_id ";

			if (oppId != 0) {
				sql += " AND oq.opportunity_id = ? ";
			} else {
				sql += "AND oq.time_created >= ? " +
					   "AND oq.time_created <= ? ";
				if (marketId != 0) {
					sql += " AND oq.market_id = ? ";
				}
			}
			sql += " ORDER BY oq.time_created desc";

			ps = con.prepareStatement(sql);
			if (oppId == 0) {
				ps.setTimestamp(1, CommonUtil.convertToTimeStamp(CommonUtil.getDateTimeFormat(from, Utils.getAppData().getUtcOffset())));
				ps.setTimestamp(2, CommonUtil.convertToTimeStamp(CommonUtil.getDateTimeFormat(CommonUtil.addDay(from), Utils.getAppData().getUtcOffset())));
				if (marketId != 0) {
					ps.setLong(3, marketId);
				}
			} else {
				ps.setLong(1, oppId);
			}

			rs = ps.executeQuery();
			while (rs.next()) {
				OpportunityQuotes0100 opportunityQuotes0100 = getOpportunityQuotes0100VO(rs);
				try {
					SimpleDateFormat localDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS Z");
					opportunityQuotes0100.setTimeEstClosing(localDateFormat.parse(rs.getString("time_est_closing")));
				} catch (RuntimeException e) {
					log.debug("cant format time est close");
				}
				opportunityQuotes0100.setWriterName(rs.getString("user_name"));
				list.add(opportunityQuotes0100);
			}
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		return list;
	}

	private static OpportunityQuotes0100 getOpportunityQuotes0100VO(ResultSet rs) throws Exception {
		OpportunityQuotes0100 temp = new OpportunityQuotes0100();
		temp.setOpportunityId(rs.getLong("opportunity_id"));
		temp.setId(rs.getLong("id"));
		temp.setMarketId(rs.getLong("market_id"));
		String comment = rs.getString("comments");
		temp.setComment(comment);
		temp.setScreenType(rs.getLong("screen_type"));
		temp.setTimeCreated(convertToDate(rs.getTimestamp("time_created")));
		temp.setWriterId(rs.getLong("writer_id"));
		String quoteParamsStr = comment.substring(0, comment.indexOf("{", 2)); // the comment should be 2 JSON object concatenated. The second { should be the first char of the 2nd JSON
		MarketDynamicsQuoteParams quoteParams = DynamicsUtil.parseOpportunityQuoteParams(quoteParamsStr);
		temp.setQuoteParams(quoteParams);
		return temp;
	}
}