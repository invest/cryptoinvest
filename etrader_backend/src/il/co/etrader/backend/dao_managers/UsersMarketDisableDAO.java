
package il.co.etrader.backend.dao_managers;
import il.co.etrader.backend.util.Utils;
import il.co.etrader.bl_vos.UserMarketDisable;
import il.co.etrader.util.CommonUtil;
import il.co.etrader.util.ConstantsBase;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Date;

import org.apache.log4j.Logger;

import com.anyoption.common.daos.DAOBase;

public class UsersMarketDisableDAO extends DAOBase {

	private static final Logger logger = Logger.getLogger(UsersMarketDisableDAO.class);


	/**
	 * Get users market disabled
	 * @param con
	 * 		db connection
	 * @param userId
	 * 		userId to search
	 * @param from
	 * 		from date filter
	 * @param to
	 * 		to date filter
	 * @param isDev3
	 * @param apiExternalUserId
	 * @return
	 * 	List of all users market disable
	 * @throws SQLException
	 */
	public static ArrayList<UserMarketDisable> getUsersList(Connection con, long userId, Date from, Date to,
			long active, long marketId, long isDev3, long skinId, long apiExternalUserId)  throws SQLException {

  		  ArrayList<UserMarketDisable> list = new ArrayList<UserMarketDisable>();
  		  PreparedStatement ps = null;
		  ResultSet rs = null;

		  try {

			    String sql = "SELECT " +
                                "hr.total_hr, " +
                                "um.*, " +
                                "u.user_name, " +
                                "u.currency_id, " +
                                "m.name market_name, " +
                                "s.display_name AS skin_name, " +
                                "aeu.reference " +
			    			 "FROM " +
                                 "users u " +
                                 "JOIN skins s ON u.skin_id = s.id, " +
                                 "user_market_disable um " +
	                                 "LEFT JOIN (SELECT i.user_id, i.api_external_user_id, sum(i.house_result)/100 total_hr " +
	                                 				"FROM investments i " +
	                                 			 "GROUP BY i.user_id, i.api_external_user_id) hr ON um.user_id = hr.user_id AND " +
	                                 			 	"(um.api_external_user_id IS NULL OR " +
	                                 			 	" um.api_external_user_id = hr.Api_External_User_Id) " +
                                     "LEFT JOIN markets m ON um.market_id = m.id " +
                                     "LEFT JOIN api_external_users aeu ON aeu.id = um.api_external_user_id " +
			    			 "WHERE " +
                                 "u.id = um.user_id AND " +
                                 "um.time_created >= ? AND " +
                                 "um.time_created <= ? ";


			    if ( userId > 0 ) {
			    	sql += "and u.id = ? ";
			    }

			    if ( active > 0 ) {
			    	if ( active == ConstantsBase.USERS_MARKET_DIS_ACTIVE ) {
			    		sql += "and um.is_active = 1 ";
			    	} else {
			    		sql += "and um.is_active = 0 ";
			    	}
			    }

			    if ( marketId > 0 ) {
			    	sql += "and um.market_id = " + marketId + " ";
			    }
			    
			    if ( skinId > 0 ) {
			    	sql += "and s.id = " + skinId + " ";
			    }

                if ( isDev3 > 0 ) {
                    if ( isDev3 == ConstantsBase.USERS_MARKET_DIS_ACTIVE ) {
                        sql += "and um.is_dev3 = 1 ";
                    } else {
                        sql += "and um.is_dev3 = 0 ";
                    }
                }

                if (apiExternalUserId > 0) {
			    	sql += "and um.api_external_user_id = " + apiExternalUserId + " ";
			    }

			    sql += "order by um.time_created desc";

			    ps = con.prepareStatement(sql);

				ps.setTimestamp(1, CommonUtil.convertToTimeStamp(CommonUtil.getDateTimeFormat(from, Utils.getAppData().getUtcOffset())));
				ps.setTimestamp(2, CommonUtil.convertToTimeStamp(CommonUtil.getDateTimeFormat(CommonUtil.addDay(to), Utils.getAppData().getUtcOffset())));

				if ( userId > 0 ) {
				    ps.setLong(3, userId);
				}

				rs = ps.executeQuery();

				while (rs.next()) {
					UserMarketDisable vo = getVO(con,rs);
                    vo.setWinLose(rs.getDouble("total_hr"));
                    vo.setCurrencyId(rs.getLong("currency_id"));
                    vo.setApiExternalUserReference(rs.getString("reference"));
					list.add(vo);
				}

			} finally {
				closeResultSet(rs);
				closeStatement(ps);
			}

			return list;
	}


	/**
	 * Insert new user market disable
	 * @param con db connection
	 * @param vo  user market disable instance for insert
	 * @throws SQLException
	 */
	public static void insert(Connection con,UserMarketDisable vo) throws SQLException {

	  PreparedStatement ps = null;

	  try {

			String sql = "insert into user_market_disable(id, user_id, market_id, start_date, end_date, time_created," +
						 "writer_id, scheduled, is_active, is_dev3, api_external_user_id) " +
						 "values (SEQ_USER_MARKET_DISABLE.NEXTVAL,?,?,?,?,sysdate,?,?,?,?,?)";

			ps = con.prepareStatement(sql);

			ps.setLong(1, vo.getUserId());
			ps.setLong(2, vo.getMarketId());
			ps.setTimestamp(3, CommonUtil.convertToTimeStamp(CommonUtil.getDateTimeFormat(vo.getStartDate(), Utils.getWriter().getUtcOffset())));
			ps.setTimestamp(4, CommonUtil.convertToTimeStamp(CommonUtil.getDateTimeFormat(vo.getEndDate(), Utils.getWriter().getUtcOffset())));
			ps.setLong(5, vo.getWriterId());
			ps.setLong(6, vo.getScheduled());

			if ( vo.isActive() ) {
				ps.setLong(7, 1);
			} else {
				ps.setLong(7, 0);
			}

            ps.setInt(8, vo.isDev3() ? 1 : 0);
            if (vo.getApiExternalUserId() != 0) {
            	ps.setLong(9, vo.getApiExternalUserId());
            } else {
            	ps.setNull(9, Types.NUMERIC);
            }

			ps.executeUpdate();
			vo.setId(getSeqCurValue(con,"SEQ_USER_MARKET_DISABLE"));

		  }
			finally {
				closeStatement(ps);
			}

	  }

	/**
	 * Update user market disable table
	 * @param con db connection
	 * @param vo  user market disable instance for update
	 * @throws SQLException
	 */
	public static void update(Connection con,UserMarketDisable vo) throws SQLException {

		PreparedStatement ps = null;

	  try {

			String sql = " UPDATE " +
							" user_market_disable " +
						 " SET " +
						 	" user_id = ? , " +
						 	" market_id = ? , " +
						 	" start_date = ?, " +
						 	" end_date = ?, " +
						 	" time_created = sysdate, " +
						 	" writer_id = ?, " +
						 	" scheduled = ?, " +
						 	" is_active = ?, " +
						 	" is_dev3 = ? " +
						 "WHERE " +
						 	" id = ? AND ";
						 	if (vo.getApiExternalUserId() != 0) {
						 		sql += " api_external_user_id = ? ";
						 	} else {
						 		sql += " api_external_user_id is null ";
						 	}

			ps = con.prepareStatement(sql);

			ps.setLong(1, vo.getUserId());
			ps.setLong(2, vo.getMarketId());
			ps.setTimestamp(3, CommonUtil.convertToTimeStamp(CommonUtil.getDateTimeFormat(vo.getStartDate(), Utils.getAppData().getUtcOffset())));
			ps.setTimestamp(4, CommonUtil.convertToTimeStamp(CommonUtil.getDateTimeFormat(vo.getEndDate(), Utils.getAppData().getUtcOffset())));
			ps.setLong(5, vo.getWriterId());
			ps.setLong(6, vo.getScheduled());

			if ( vo.isActive() ) {
				ps.setLong(7, 1);
			} else {
				ps.setLong(7, 0);
			}

            ps.setInt(8, vo.isDev3() ? 1 : 0);

			ps.setLong(9, vo.getId());
			if (vo.getApiExternalUserId() != 0) {
            	ps.setLong(10, vo.getApiExternalUserId());
            }

			ps.executeUpdate();
			
  		}	finally {
			closeStatement(ps);
		}
	  }

	/**
	 * get UserMarketDisable object
	 * @param rs
	 * 		ResultSet instance
	 * @return
	 * @throws SQLException
	 */
	private static UserMarketDisable getVO(Connection con, ResultSet rs) throws SQLException {

		UserMarketDisable vo = new UserMarketDisable();
		vo.setId(rs.getLong("id"));
		vo.setUserId(rs.getLong("user_id"));
		vo.setMarketId(rs.getLong("market_id"));
		vo.setStartDate(convertToDate(rs.getTimestamp("start_date")));
		vo.setEndDate(convertToDate(rs.getTimestamp("end_date")));
		vo.setWriterId(rs.getLong("writer_id"));
		vo.setTimeCreated(convertToDate(rs.getTimestamp("time_created")));
		vo.setScheduled(rs.getLong("scheduled"));
		vo.setUserName(rs.getString("user_name"));
		vo.setMarketName(rs.getString("market_name"));
		vo.setSkinName(rs.getString("skin_name"));
		vo.setApiExternalUserId(rs.getLong("api_external_user_id"));
		vo.setUserIdAndApiExternalUserId(vo.getUserId() + "_" + vo.getApiExternalUserId());

		int isActive = rs.getInt("is_active");
		if (isActive == 1) {
			vo.setActive(true);
		} else {
			vo.setActive(false);
		}

        if (rs.getInt("is_dev3") == 1) {
            vo.setDev3(true);
        } else {
            vo.setDev3(false);
        }

		return vo;
	}


	public static void removeUsersMarketDisableByUserId(Connection con, long userId, long apiExternalUserId) throws SQLException {
		PreparedStatement ps = null;
		try {

			String sql = "UPDATE " +
							"user_market_disable " +
						 "SET " +
						 	"is_active = 0 " +
						 "WHERE " +
						 	"user_id = ? AND ";
				if (apiExternalUserId != 0) {
					sql += "api_external_user_id = ? AND ";
				} else {
					sql += "api_external_user_id is null AND ";
				}
						 	
				sql += "is_dev3 = 1";

			ps = con.prepareStatement(sql);

			ps.setLong(1, userId);
			if (apiExternalUserId != 0) {
				ps.setLong(2, apiExternalUserId);
            }
			

			ps.executeUpdate();

		} finally {
			closeStatement(ps);
		}
	}
}
