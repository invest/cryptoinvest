package il.co.etrader.backend.dao_managers;


import il.co.etrader.backend.bl_vos.CopyopCoins;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import org.apache.log4j.Logger;

import com.copyop.common.dao.CoinsBalanceHistoryDAO;
import com.copyop.common.enums.CoinsActionTypeEnum;
import com.datastax.driver.core.BoundStatement;
import com.datastax.driver.core.PreparedStatement;
import com.datastax.driver.core.ResultSet;
import com.datastax.driver.core.Row;
import com.datastax.driver.core.Session;

/**
 * @author eranl
 *
 */
public class CopyopCoinsBalanceHistoryDAO extends CoinsBalanceHistoryDAO  {

	private static final Logger log = Logger.getLogger(CopyopCoinsBalanceHistoryDAO.class);
	

	/**
	 * Get coins balance history by user
	 * @param session
	 * @param userId
	 * @param coinsStateId
	 * @param from
	 * @param to
	 * @return
	 */
	public static ArrayList<CopyopCoins> getCoinsBalanceHistoryByUser(Session session, long userId, int coinsStateId, Date from, Date to) { 
		PreparedStatement ps = null;
		ArrayList<CopyopCoins> list = new ArrayList<CopyopCoins>();
		int index = 0;
		String cql =" SELECT " +
					"	time_created as id, " +
					"	dateOf(time_created) as time_created, " +
					"	action_type, " +
					"	coins_balance, " +
					"	writer_id, " +
					"	is_re_granted, " +
					"	amount, " +
					"	re_granted_writer_id " +
					" FROM " +
					"	coins_balance_history " +
					" WHERE " +
					"	user_id = ? " +
					"	AND time_created > minTimeuuid(?) " +
					"	AND time_created < minTimeuuid(?) ";
		if (coinsStateId == 0) {					
			cql+= 	"	AND action_type_be = ? ";
		}					
		if (coinsStateId != 0) {
			cql+= 	"	AND action_type = ? ";
		}
				
		ps = session.prepare(cql);		
		BoundStatement boundStatement = new BoundStatement(ps); 		
		boundStatement.setLong(index++, userId);
		boundStatement.setDate(index++, from);
		boundStatement.setDate(index++, to);
		if (coinsStateId == 0) {
			boundStatement.setBool(index++, true);
		}
		if (coinsStateId != 0) {
			boundStatement.setInt(index++, coinsStateId);  
		}
		ResultSet resultSet = session.execute(boundStatement);
		List<Row> rows = resultSet.all();
		for (Row row : rows) {
			CopyopCoins cc = new CopyopCoins();			
			cc.setId(row.getUUID("id").toString());
			cc.setTimeCreated(row.getDate("time_created"));
			cc.setWriterId(row.getLong("writer_id"));
			cc.setCoins(Math.abs(row.getLong("coins_balance"))); // we want to display the coins as positive value
			cc.setActionTypeId(row.getInt("action_type"));
			cc.setAmount(row.getLong("amount"));
			boolean isReGrant = row.getBool("is_re_granted");			
			if (isReGrant) {
				cc.setActionTypeId(CoinsActionTypeEnum.RE_GRANT.getId());
				cc.setWriterId(row.getInt("re_granted_writer_id"));
			}	
			list.add(cc);
		}
		return list;
	}
	
	
						
	/**
	 * Sum coins by action type
	 * @param session
	 * @param userId
	 * @param actionTypeId
	 * @return
	 */
	public static long sumCoinsAmountByType(Session session, long userId, int actionTypeId) {				
		PreparedStatement ps = methodsPreparedStatement.get("sumCoinsAmountByType");
		long result = 0;
		if (ps == null) {
			String cql = 
					" SELECT " +
					"	* " +
					" FROM " +
					"	coins_balance_history " +
					" WHERE " +
					"	user_id = ? " +
					"	AND action_type = ? ";
			ps = session.prepare(cql);
			methodsPreparedStatement.put("sumCoinsAmountByType", ps);
		}
		BoundStatement boundStatement = new BoundStatement(ps);
		boundStatement.bind(userId, actionTypeId);
		ResultSet resultSet = session.execute(boundStatement);		
		List<Row> rows = resultSet.all();
		for (Row row : rows) {
			result += row.getLong("coins_balance");
		}
		return result;
	}	
	
	/**
	 * Sum missed coins with Re-Grant status	
	 * @param session
	 * @param userId
	 * @param actionTypeId
	 * @return
	 */
	public static long sumMissedCoinsRegrant(Session session, long userId) {				
		PreparedStatement ps = methodsPreparedStatement.get("sumCoinsAmountByType");
		long result = 0;
		if (ps == null) {
			String cql = 
					" SELECT " +
					"	* " +
					" FROM " +
					"	coins_balance_history " +
					" WHERE " +
					"	user_id = ? " +
					"	AND action_type = ? ";
			ps = session.prepare(cql);
			methodsPreparedStatement.put("sumCoinsAmountByType", ps);
		}
		BoundStatement boundStatement = new BoundStatement(ps);
		boundStatement.bind(userId, CoinsActionTypeEnum.RESET_COINS.getId());
		ResultSet resultSet = session.execute(boundStatement);		
		List<Row> rows = resultSet.all();
		for (Row row : rows) {
			boolean isReGrant = row.getBool("is_re_granted");
			if (isReGrant) {
				result += row.getLong("coins_balance");	
			}				
		}
		return result;
	}
	
	/**
	 * Sum missed coins without Re-Grant status 
	 * @param session
	 * @param userId
	 * @param actionTypeId
	 * @return
	 */
	public static long sumMissedCoinsWithoutRegrant(Session session, long userId) {				
		PreparedStatement ps = methodsPreparedStatement.get("sumCoinsAmountByType");
		long result = 0;
		if (ps == null) {
			String cql = 
					" SELECT " +
					"	* " +
					" FROM " +
					"	coins_balance_history " +
					" WHERE " +
					"	user_id = ? " +
					"	AND action_type = ? ";
			ps = session.prepare(cql);
			methodsPreparedStatement.put("sumCoinsAmountByType", ps);
		}
		BoundStatement boundStatement = new BoundStatement(ps);
		boundStatement.bind(userId, CoinsActionTypeEnum.RESET_COINS.getId());
		ResultSet resultSet = session.execute(boundStatement);		
		List<Row> rows = resultSet.all();
		for (Row row : rows) {
			boolean isReGrant = row.getBool("is_re_granted");
			if (!isReGrant) {
				result += row.getLong("coins_balance");	
			}				
		}
		return result;
	}
	
	/**
	 * Re-Grant coins to user 
	 * @param session
	 * @param userId
	 * @param id
	 * @param writerId
	 * @return
	 */
	public static boolean updateBalanceHistoryRegrant(Session session, long userId, UUID coinsBalanceHistoryId, int writerId) {
		PreparedStatement ps = methodsPreparedStatement.get("updateBalanceHistoryRegrant");		
		if (ps == null) {
			String cql =" UPDATE " +
						"	coins_balance_history " +
						" SET " +						
						"	re_granted_writer_id = ?, " +
						"	is_re_granted = true " +
						" WHERE " +
						"	user_id = ? " +							
						"	AND time_created = ? ";
			ps = session.prepare(cql);
			methodsPreparedStatement.put("updateBalanceHistoryRegrant", ps);
		}
		BoundStatement boundStatement = new BoundStatement(ps);
		session.execute(boundStatement.bind(writerId, userId, coinsBalanceHistoryId));
		return true;
	}
	
}
