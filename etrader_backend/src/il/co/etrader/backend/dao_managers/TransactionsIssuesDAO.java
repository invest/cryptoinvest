package il.co.etrader.backend.dao_managers;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;

import javax.faces.context.FacesContext;

import com.anyoption.common.beans.SpecialCare;
import com.anyoption.common.beans.base.Skin;

import il.co.etrader.backend.bl_vos.MergeTransaction;
import il.co.etrader.backend.bl_vos.SalesDepositsDetails;
import il.co.etrader.backend.bl_vos.SalesDepositsTotals;
import il.co.etrader.backend.bl_vos.SalesTurnoversDetails;
import il.co.etrader.backend.bl_vos.TransactionsIssues;
import il.co.etrader.backend.bl_vos.WriterWrapper;
import il.co.etrader.backend.util.Constants;
import il.co.etrader.backend.util.Utils;
import il.co.etrader.bl_managers.ApplicationDataBase;
import il.co.etrader.bl_managers.SkinsManagerBase;
import il.co.etrader.bl_vos.Skins;
import il.co.etrader.bl_vos.Writer;
import il.co.etrader.dao_managers.TransactionsIssuesDAOBase;
import il.co.etrader.util.CommonUtil;
import il.co.etrader.util.ConstantsBase;

public class TransactionsIssuesDAO extends TransactionsIssuesDAOBase{

		/**
		 * get salesDeposits list
		 * @param con db connection
		 * @return ArrayList of all salesDeposits
		 * @throws SQLException
		 * @throws ParseException 
		 */
		public static ArrayList<SalesDepositsTotals> getSalesDeposits(Connection con, Date from, Date to, long writerId, long currencyId, long skinId) throws SQLException, ParseException {

			PreparedStatement ps = null;
			ResultSet rs = null;
			ArrayList<SalesDepositsTotals> list = new ArrayList<SalesDepositsTotals>();
			SalesDepositsTotals sdcSum = new SalesDepositsTotals();
			FacesContext context = FacesContext.getCurrentInstance();
			WriterWrapper w = (WriterWrapper) context.getApplication().createValueBinding(Constants.BIND_WRITER_WRAPPER).getValue(context);
			long offset = Utils.getUtcOffsetValue(w.getUtcOffset());
			long resCurrencyId;
			String amountStr;

			long conTranMadeCountTotal = 0;
			long conTranMadeSumTotal = 0;
			long conTranMadeAvgTotal = 0;
			long conTranMadeQualCountTotal = 0;
			long conTranMadeQualSumTotal = 0;
			long conTranMadeQualAvgTotal = 0;
			long conTranQualCountTotal =0;
			long conTranQualSumTotal = 0;
			long conTranQualAvgTotal = 0;
//			long retTranMadeCountTotal = 0;
//			long retTranMadeSumTotal = 0;
//			long retTranMadeAvgTotal = 0;
//			long retTranMadeQualCountTotal = 0;
//			long retTranMadeQualSumTotal = 0;
//			long retTranMadeQualAvgTotal = 0;
//			long retTranQualCountTotal = 0;
//			long retTranQualSumTotal = 0;
//			long retTranQualAvgTotal = 0;

			if (currencyId == ConstantsBase.ALL_FILTER_ID){
				resCurrencyId = ConstantsBase.CURRENCY_USD_ID;
				amountStr = " GROUP_MERGED_TX_SUM_USD ";
			}else {
				resCurrencyId = currencyId;
				amountStr = " GROUP_MERGED_TX_SUM ";
			}

			try {

//				String sql =
//					" SELECT " +
//				    	" to_char(st.date1, 'dd/mm/yyyy') date_str, " +
//				    	" st.date1 date_value, " +
//				        " SUM (st.con_dep_made_count) con_dep_made_count, " +
//				        " SUM (st.con_dep_made_sum) con_dep_made_sum, " +
//				        " SUM (st.con_dep_made_qual_count) con_dep_made_qual_count, " +
//				        " SUM (st.con_dep_made_qual_sum) con_dep_made_qual_sum, " +
//				        " SUM (st.con_dep_qual_count) con_dep_qual_count, " +
//				        " SUM (st.con_dep_qual_sum) con_dep_qual_sum, " +
//				        " SUM (st.ret_dep_made_count) ret_dep_made_count, " +
//				        " SUM (st.ret_dep_made_sum) ret_dep_made_sum, " +
//				        " SUM (st.ret_dep_made_qual_count) ret_dep_made_qual_count, " +
//				        " SUM (st.ret_dep_made_qual_sum) ret_dep_made_qual_sum, " +
//				        " SUM (st.ret_dep_qual_count) ret_dep_qual_count, " +
//				        " SUM (st.ret_dep_qual_sum) ret_dep_qual_sum " +
//					" FROM " +
//						" (" +
//						" SELECT " +
//				        	" trunc(t.time_created  +(1/24)*" + offset + ") date1, " +
//				         	" SUM (CASE WHEN first_deposits.first_deposit_id is not null  THEN 1 ELSE 0 END) con_dep_made_count, " +
//				         	" SUM (CASE WHEN first_deposits.first_deposit_id is not null THEN " + amountStr + " ELSE 0 END) con_dep_made_sum, " +
//				         	" SUM (CASE WHEN first_deposits.first_deposit_id is not null AND ti.time_settled is not null THEN 1 ELSE 0 END) con_dep_made_qual_count, " +
//				         	" SUM (CASE WHEN first_deposits.first_deposit_id is not null AND ti.time_settled is not null THEN " + amountStr + " ELSE 0 END) con_dep_made_qual_sum, " +
//				         	" 0 con_dep_qual_count,  " +
//				         	" 0 con_dep_qual_sum,  " +
//				         	" SUM (CASE WHEN first_deposits.first_deposit_id is null  THEN 1 ELSE 0 END) ret_dep_made_count, " +
//				         	" SUM (CASE WHEN first_deposits.first_deposit_id is null  THEN " + amountStr + " ELSE 0 END) ret_dep_made_sum, " +
//				         	" SUM (CASE WHEN first_deposits.first_deposit_id is null AND ti.time_settled is not null THEN 1 ELSE 0 END) ret_dep_made_qual_count, " +
//				         	" SUM (CASE WHEN first_deposits.first_deposit_id is null AND ti.time_settled is not null THEN " + amountStr + " ELSE 0 END) ret_dep_made_qual_sum, " +
//				         	" 0 ret_dep_qual_count,  " +
//				         	" 0 ret_dep_qual_sum  " +
//				     	" FROM   " +
//				        	" transactions_issues ti, " +
//				           	" issue_actions ia,  " +
//				           	" users u,  " +
//				           	" transactions t left join users_first_deposit first_deposits on t.id = first_deposits.first_deposit_id " +
//				     	" WHERE " +
//				     		" ti.transaction_id = t.id  " +
//				       		" AND u.id = t.user_id  " +
//				       		" AND u.class_id > 0  " +
//				       		" AND t.status_id IN (" + TransactionsManagerBase.SUCCESS_DEPOSITS_TYPES + ") " +
//				       		" AND u.skin_id in (" + w.getSkinsToString() + ") " +
//				       		" AND ti.issue_action_id = ia.id  ";
//	            if (currencyId > ConstantsBase.ALL_FILTER_ID){
//	            	sql += 	" AND u.currency_id = " + currencyId + " ";
//	            }
//	            if (writerId > ConstantsBase.ALL_FILTER_ID){
//	            	sql += 	" AND ia.writer_id = " + writerId + " ";
//	            }
//	            if (skinId > ConstantsBase.ALL_FILTER_ID){
//	            	sql +=  " AND u.skin_id = " + skinId + " ";
//	            }
//				sql +=
//				       		" AND t.time_created +(1/24)*" + offset + " between ? and ? " +
//				    	" GROUP BY " +
//				    		" trunc(t.time_created  +(1/24)*" + offset + ") " +
//
//				    	" UNION " +
//
//				    	" SELECT  " +
//					        " trunc(ti.time_settled  +(1/24)*" + offset + ") date1, " +
//					        " 0 con_dep_made_count,  " +
//					        " 0 con_dep_made_sum, " +
//					        " 0 con_dep_made_qual_count, " +
//				         	" 0 con_dep_made_qual_sum, " +
//					        " SUM (CASE WHEN first_deposits.first_deposit_id  is not null  THEN 1 ELSE 0 END) con_dep_qual_count, " +
//					        " SUM (CASE WHEN first_deposits.first_deposit_id  is not null THEN " + amountStr + " ELSE 0 END) con_dep_qual_sum, " +
//					        " 0 ret_dep_made_count,  " +
//					        " 0 ret_dep_made_sum,  " +
//					        " 0 ret_dep_made_qual_count, " +
//				         	" 0 ret_dep_made_qual_sum, " +
//					        " SUM (CASE WHEN first_deposits.first_deposit_id  is null  THEN 1 ELSE 0 END) ret_dep_qual_count, " +
//					        " SUM (CASE WHEN first_deposits.first_deposit_id  is null  THEN " + amountStr + " ELSE 0 END) ret_dep_qual_sum " +
//				    	" FROM   " +
//				    		" transactions_issues ti, " +
//				    		" issue_actions ia, " +
//				    		" users u, " +
//				    		" transactions t left join users_first_deposit first_deposits on t.id = first_deposits.first_deposit_id " +
//				    	" WHERE " +
//				    		" ti.transaction_id = t.id  " +
//				    		" AND u.id = t.user_id  " +
//				    		" AND u.class_id > 0  " +
//				    		" AND t.status_id IN (" + TransactionsManagerBase.SUCCESS_DEPOSITS_TYPES + ") " +
//				    		" AND u.skin_id in (" + w.getSkinsToString() + ") " +
//				    		" AND ti.issue_action_id = ia.id  ";
//	            if (currencyId > ConstantsBase.ALL_FILTER_ID){
//	            	sql += 	" AND u.currency_id = " + currencyId + " ";
//	            }
//	            if (writerId > ConstantsBase.ALL_FILTER_ID){
//	            	sql += 	" AND ia.writer_id = " + writerId + " ";
//	            }
//	            if (skinId > ConstantsBase.ALL_FILTER_ID){
//	            	sql +=  " AND u.skin_id = " + skinId + " ";
//	            }
//				sql +=
//				    		" AND ti.time_settled +(1/24)*" + offset + " between ? and ? " +
//				    	" GROUP BY " +
//				    		" trunc(ti.time_settled  +(1/24)*" + offset + ") " +
//
//				    	" UNION " +
//
//				    	" SELECT " +
//					        " trunc(dates.column_value) date1, " +
//					        " 0 con_dep_made_count,  " +
//					        " 0 con_dep_made_sum,  " +
//					        " 0 con_dep_made_qual_count,  " +
//					        " 0 con_dep_made_qual_sum,  " +
//					        " 0 con_dep_qual_count,  " +
//					        " 0 con_dep_qual_sum,  " +
//					        " 0 ret_dep_made_count,  " +
//					        " 0 ret_dep_made_sum,  " +
//					        " 0 ret_dep_made_qual_count,  " +
//					        " 0 ret_dep_made_qual_sum,  " +
//					        " 0 ret_dep_qual_count,  " +
//					        " 0 ret_dep_qual_sum  " +
//					    " FROM   " +
//					        " TABLE (get_date_range(?, ?)) dates " +
//				    	" ) st " +
//				" GROUP BY st.date1 " +
//				" ORDER by st.date1 ";
				
				
				
				
				
				String sql =
						"SELECT st.date1 date_value, " +
								"  SUM (st.dep_and_qualif_cnt) dep_made_qual_count, " +
								"  SUM (st.dep_and_qualif_sum) dep_made_qual_sum, " +
								"  SUM (st.dep_qual_cnt) dep_made_count, " +
								"  SUM (st.dep_qual_sum) dep_made_sum, " +
								"  SUM (st.dep_qual_cnt1) dep_qual_count, " +
								"  SUM (st.dep_qual_sum1) dep_qual_sum " +
								"FROM " +
								"  (SELECT TRUNC(first_tx_time +(1/24)*" + offset + ") date1, " +
								"    SUM ( " +
								"    CASE " +
								"      WHEN TRUNC(first_tx_time) = TRUNC(tx_settled_time) " +
								"      THEN 1 " +
								"      ELSE 0 " +
								"    END) AS dep_and_qualif_cnt, " +
								"    SUM ( " +
								"    CASE " +
								"      WHEN TRUNC(first_tx_time) = TRUNC(tx_settled_time) " +
								"      THEN " + amountStr  +
								"      ELSE 0 " +
								"    END) AS dep_and_qualif_sum, " +
								"    SUM ( " +
								"    CASE " +
								"      WHEN TRUNC(first_tx_time) IS NOT NULL " +
								"      THEN 1 " +
								"      ELSE 0 " +
								"    END) AS dep_qual_cnt, " +
								"    SUM ( " +
								"    CASE " +
								"      WHEN TRUNC(first_tx_time) IS NOT NULL " +
								"      THEN " + amountStr +
								"      ELSE 0 " +
								"    END) AS dep_qual_sum, " +
								"    0 dep_qual_cnt1, " +
								"    0 dep_qual_sum1 " +
								"  FROM merged_sales_conversions " +
								"  WHERE first_tx_time BETWEEN ?  AND ? " ;
								 if (currencyId > ConstantsBase.ALL_FILTER_ID){
						            	sql += 	" AND currency_id = " + currencyId + " ";
						            }
						            if (writerId > ConstantsBase.ALL_FILTER_ID){
						            	sql += 	" AND writer_id = " + writerId + " ";
						            }
						            if (skinId > ConstantsBase.ALL_FILTER_ID){
						            	sql +=  " AND skin_id = " + skinId + " ";
						            }
								
								sql += "  GROUP BY TRUNC(first_tx_time +(1/24)*" + offset + ") " +
								"  UNION " +
								"  SELECT( " +
								"    TRUNC(tx_settled_time +(1/24)*" + offset + ")) date1, " +
								"    0 dep_and_qualif_cnt, " +
								"    0 dep_and_qualif_sum, " +
								"    0 dep_qual_cnt, " +
								"    0 dep_qual_sum, " +
								"    SUM ( " +
								"    CASE " +
								"      WHEN TRUNC(tx_settled_time) is not null " +
								"      THEN 1 " +
								"      ELSE 0 " +
								"    END) AS dep_qual_cnt1, " +
								"    SUM ( " +
								"    CASE " +
								"      WHEN TRUNC(tx_settled_time) is not null " +
								"      THEN " + amountStr +
								"      ELSE 0 " +
								"    END) AS dep_qual_sum1 " +
								"  FROM merged_sales_conversions " +
								"  WHERE (tx_settled_time BETWEEN ? AND ?) ";
								 if (currencyId > ConstantsBase.ALL_FILTER_ID){
						            	sql += 	" AND currency_id = " + currencyId + " ";
						            }
						            if (writerId > ConstantsBase.ALL_FILTER_ID){
						            	sql += 	" AND writer_id = " + writerId + " ";
						            }
						            if (skinId > ConstantsBase.ALL_FILTER_ID){
						            	sql +=  " AND skin_id = " + skinId + " ";
						            }
								sql += "  GROUP BY " +
								"    TRUNC(tx_settled_time +(1/24)*" + offset + ") " +
								"  UNION " +
								"  SELECT TRUNC(dates.column_value) date1, " +
								"    0 dep_and_qualif_cnt, " +
								"    0 dep_and_qualif_sum, " +
								"    0 dep_qual_cnt, " +
								"    0 dep_qual_sum, " +
								"    0 dep_qual_cnt1, " +
								"    0 dep_qual_sum1 " +
								"  FROM TABLE (get_date_range(?, ?)) dates " +
								"  ) st " +
								"GROUP BY st.date1 " +
								"ORDER BY st.date1";
				
				ps = con.prepareStatement(sql);
				
				ps.setTimestamp(1, CommonUtil.convertToTimeStamp(CommonUtil.getDateTimeFormat(from, Utils.getAppData().getUtcOffset())));
				ps.setTimestamp(2, CommonUtil.convertToTimeStamp(CommonUtil.getDateTimeFormat(CommonUtil.addDay(to), Utils.getAppData().getUtcOffset())));
				ps.setTimestamp(3, CommonUtil.convertToTimeStamp(CommonUtil.getDateTimeFormat(from, Utils.getAppData().getUtcOffset())));
				ps.setTimestamp(4, CommonUtil.convertToTimeStamp(CommonUtil.getDateTimeFormat(CommonUtil.addDay(to), Utils.getAppData().getUtcOffset())));
				ps.setTimestamp(5, CommonUtil.convertToTimeStamp(CommonUtil.getDateTimeFormat(from, Utils.getAppData().getUtcOffset())));
				ps.setTimestamp(6, CommonUtil.convertToTimeStamp(CommonUtil.getDateTimeFormat(to, Utils.getAppData().getUtcOffset())));

				rs = ps.executeQuery();

				while (rs.next()) {
					SalesDepositsTotals sdc = new SalesDepositsTotals();
					long conTranMadeCount = rs.getLong("dep_made_count");
					long conTranMadeSum = rs.getLong("dep_made_sum");
					long conTranMadeAvg = 0;
					if (conTranMadeCount > 0) {
						conTranMadeAvg = conTranMadeSum / conTranMadeCount;
					}
					long conTranMadeQualCount = rs.getLong("dep_made_qual_count");
					long conTranMadeQualSum = rs.getLong("dep_made_qual_sum");
					long conTranMadeQualAvg = 0;
					if (conTranMadeQualCount > 0) {
						conTranMadeQualAvg = conTranMadeQualSum / conTranMadeQualCount;
					}
					long conTranQualCount = rs.getLong("dep_qual_count");
					long conTranQualSum = rs.getLong("dep_qual_sum");
					long conTranQualAvg = 0;
					if (conTranQualCount > 0) {
						conTranQualAvg = conTranQualSum / conTranQualCount;
					}
//					long retTranMadeCount = rs.getLong("ret_dep_made_count");
//					long retTranMadeSum = rs.getLong("ret_dep_made_sum");
//					long retTranMadeAvg = 0;
//					if (retTranMadeCount > 0) {
//						retTranMadeAvg = retTranMadeSum / retTranMadeCount;
//					}
//					long retTranMadeQualCount = rs.getLong("ret_dep_made_qual_count");
//					long retTranMadeQualSum = rs.getLong("ret_dep_made_qual_sum");
//					long retTranMadeQualAvg = 0;
//					if (retTranMadeQualCount > 0) {
//						retTranMadeQualAvg = retTranMadeQualSum / retTranMadeQualCount;
//					}
//					long retTranQualCount = rs.getLong("ret_dep_qual_count");
//					long retTranQualSum = rs.getLong("ret_dep_qual_sum");
//					long retTranQualAvg = 0;
//					if (retTranQualCount > 0) {
//						retTranQualAvg = retTranQualSum / retTranQualCount;
//					}

					sdc.setDate(rs.getString("date_value"));
					sdc.setRealDate(convertToDate(rs.getTimestamp("date_value")));

					if (conTranMadeCount > 0){
						sdc.setConTranMadeCount(String.valueOf(conTranMadeCount));
						conTranMadeCountTotal += conTranMadeCount;
					}else{
						sdc.setConTranMadeCount(ConstantsBase.EMPTY_STRING);
					}

					if (conTranMadeSum > 0){
						sdc.setConTranMadeSum(CommonUtil.displayAmount(conTranMadeSum, resCurrencyId));
						conTranMadeSumTotal += conTranMadeSum;
					}else{
						sdc.setConTranMadeSum(ConstantsBase.EMPTY_STRING);
					}

					if (conTranMadeAvg > 0) {
						sdc.setConTranMadeAvg(CommonUtil.displayAmount(conTranMadeAvg, resCurrencyId));
						if (conTranMadeCountTotal > 0) {
							conTranMadeAvgTotal = conTranMadeSumTotal / conTranMadeCountTotal;
						}
					} else {
						sdc.setConTranMadeAvg(ConstantsBase.EMPTY_STRING);
					}

					if (conTranMadeQualCount > 0){
						sdc.setConTranMadeQualCount(String.valueOf(conTranMadeQualCount));
						conTranMadeQualCountTotal += conTranMadeQualCount;
					}else{
						sdc.setConTranMadeQualCount(ConstantsBase.EMPTY_STRING);
					}

					if (conTranMadeQualSum > 0){
						sdc.setConTranMadeQualSum(CommonUtil.displayAmount(conTranMadeQualSum, resCurrencyId));
						conTranMadeQualSumTotal += conTranMadeQualSum;
					}else{
						sdc.setConTranMadeQualSum(ConstantsBase.EMPTY_STRING);
					}

					if (conTranMadeQualAvg > 0){
						sdc.setConTranMadeQualAvg(CommonUtil.displayAmount(conTranMadeQualAvg, resCurrencyId));
						if (conTranMadeQualCountTotal > 0) {
							conTranMadeQualAvgTotal = conTranMadeQualSumTotal / conTranMadeQualCountTotal;
						}
					}else{
						sdc.setConTranMadeQualAvg(ConstantsBase.EMPTY_STRING);
					}

					if (conTranQualCount > 0){
						sdc.setConTranQualCount(String.valueOf(conTranQualCount));
						conTranQualCountTotal += conTranQualCount;
					}else{
						sdc.setConTranQualCount(ConstantsBase.EMPTY_STRING);
					}

					if (conTranQualSum > 0){
						sdc.setConTranQualSum(CommonUtil.displayAmount(conTranQualSum, resCurrencyId));
						conTranQualSumTotal += conTranQualSum;
					}else{
						sdc.setConTranQualSum(ConstantsBase.EMPTY_STRING);
					}

					if (conTranQualCountTotal > 0){
						sdc.setConTranQualAvg(CommonUtil.displayAmount(conTranQualAvg, resCurrencyId));
						conTranQualAvgTotal = conTranQualSum / conTranQualCountTotal;
					}else{
						sdc.setConTranQualAvg(ConstantsBase.EMPTY_STRING);
					}

//					if (retTranMadeCount > 0){
//						sdc.setRetTranMadeCount(String.valueOf(retTranMadeCount));
//						retTranMadeCountTotal += retTranMadeCount;
//					}else{
//						sdc.setRetTranMadeCount(ConstantsBase.EMPTY_STRING);
//					}
//
//					if (retTranMadeSum > 0){
//						sdc.setRetTranMadeSum(CommonUtil.displayAmount(retTranMadeSum, resCurrencyId));
//						retTranMadeSumTotal += retTranMadeSum;
//					}else{
//						sdc.setRetTranMadeSum(ConstantsBase.EMPTY_STRING);
//					}
//
//					if (retTranMadeAvg > 0){
//						sdc.setRetTranMadeAvg(CommonUtil.displayAmount(retTranMadeAvg, resCurrencyId));
//						if (retTranMadeCountTotal > 0) {
//							retTranMadeAvgTotal = retTranMadeSumTotal / retTranMadeCountTotal;
//						}
//					}else{
//						sdc.setRetTranMadeAvg(ConstantsBase.EMPTY_STRING);
//					}
//
//					if (retTranMadeQualCount > 0){
//						sdc.setRetTranMadeQualCount(String.valueOf(retTranMadeQualCount));
//						retTranMadeQualCountTotal += retTranMadeQualCount;
//					}else{
//						sdc.setRetTranMadeQualCount(ConstantsBase.EMPTY_STRING);
//					}
//
//					if (retTranMadeQualSum > 0){
//						sdc.setRetTranMadeQualSum(CommonUtil.displayAmount(retTranMadeQualSum, resCurrencyId));
//						retTranMadeQualSumTotal += retTranMadeQualSum;
//					}else{
//						sdc.setRetTranMadeQualSum(ConstantsBase.EMPTY_STRING);
//					}
//
//					if (retTranMadeQualAvg > 0){
//						sdc.setRetTranMadeQualAvg(CommonUtil.displayAmount(retTranMadeQualAvg, resCurrencyId));
//						if (retTranMadeQualCountTotal > 0) {
//							retTranMadeQualAvgTotal = retTranMadeQualSumTotal / retTranMadeQualCountTotal;
//						}
//					}else{
//						sdc.setRetTranMadeQualAvg(ConstantsBase.EMPTY_STRING);
//					}
//
//					if (retTranQualCount > 0){
//						sdc.setRetTranQualCount(String.valueOf(retTranQualCount));
//						retTranQualCountTotal += retTranQualCount;
//					}else{
//						sdc.setRetTranQualCount(ConstantsBase.EMPTY_STRING);
//					}
//
//					if (retTranQualSum > 0){
//						sdc.setRetTranQualSum(CommonUtil.displayAmount(retTranQualSum, resCurrencyId));
//						retTranQualSumTotal += retTranQualSum;
//					}else{
//						sdc.setRetTranQualSum(ConstantsBase.EMPTY_STRING);
//					}
//
//					if (retTranQualAvg > 0){
//						sdc.setRetTranQualAvg(CommonUtil.displayAmount(retTranQualAvg, resCurrencyId));
//						if (retTranQualCountTotal > 0) {
//							retTranQualAvgTotal = retTranQualSumTotal / retTranQualCountTotal;
//						}
//					}else{
//						sdc.setRetTranQualAvg(ConstantsBase.EMPTY_STRING);
//					}

					list.add(sdc);

				}

				sdcSum.setDate(Constants.EMPTY_STRING);
				sdcSum.setConTranMadeCount(String.valueOf(conTranMadeCountTotal));
				sdcSum.setConTranMadeSum(CommonUtil.displayAmount(conTranMadeSumTotal, resCurrencyId));
				sdcSum.setConTranMadeAvg(CommonUtil.displayAmount(conTranMadeAvgTotal, resCurrencyId));
				sdcSum.setConTranMadeQualCount(String.valueOf(conTranMadeQualCountTotal));
				sdcSum.setConTranMadeQualSum(CommonUtil.displayAmount(conTranMadeQualSumTotal, resCurrencyId));
				sdcSum.setConTranMadeQualAvg(CommonUtil.displayAmount(conTranMadeQualAvgTotal, resCurrencyId));
				sdcSum.setConTranQualCount(String.valueOf(conTranQualCountTotal));
				sdcSum.setConTranQualSum(CommonUtil.displayAmount(conTranQualSumTotal, resCurrencyId));
				sdcSum.setConTranQualAvg(CommonUtil.displayAmount(conTranQualAvgTotal, resCurrencyId));
//				sdcSum.setRetTranMadeCount(String.valueOf(retTranMadeCountTotal));
//				sdcSum.setRetTranMadeSum(CommonUtil.displayAmount(retTranMadeSumTotal, resCurrencyId));
//				sdcSum.setRetTranMadeAvg(CommonUtil.displayAmount(retTranMadeAvgTotal, resCurrencyId));
//				sdcSum.setRetTranMadeQualCount(String.valueOf(retTranMadeQualCountTotal));
//				sdcSum.setRetTranMadeQualSum(CommonUtil.displayAmount(retTranMadeQualSumTotal, resCurrencyId));
//				sdcSum.setRetTranMadeQualAvg(CommonUtil.displayAmount(retTranMadeQualAvgTotal, resCurrencyId));
//				sdcSum.setRetTranQualCount(String.valueOf(retTranQualCountTotal));
//				sdcSum.setRetTranQualSum(CommonUtil.displayAmount(retTranQualSumTotal, resCurrencyId));
//				sdcSum.setRetTranQualAvg(CommonUtil.displayAmount(retTranQualAvgTotal, resCurrencyId));
				list.add(sdcSum);

			} finally {
				closeResultSet(rs);
				closeStatement(ps);
			}
			return list;
		}

		/**
		 * get salesDepositsDetails list
		 * @param con db connection
		 * @return ArrayList of all salesDeposits
		 * @throws SQLException
		 */
		public static ArrayList<SalesDepositsDetails> getSalesDepositsDetails(Connection con, long userId, long transactionId, Date from, Date to, long writerId, long currencyId,
				long skinId, String dateFilterType, int qualifiedFilter, int firstDepositFilter) throws SQLException {

			PreparedStatement ps = null;
			ResultSet rs = null;
			ArrayList<SalesDepositsDetails> list = new ArrayList<SalesDepositsDetails>();
			SalesDepositsDetails sddSum = new SalesDepositsDetails();
			double depositAmountSum = 0;
			long ResCurrencyId;
			FacesContext context = FacesContext.getCurrentInstance();
			WriterWrapper w = (WriterWrapper) context.getApplication().createValueBinding(Constants.BIND_WRITER_WRAPPER).getValue(context);

			if (currencyId == ConstantsBase.ALL_FILTER_ID){
				ResCurrencyId = ConstantsBase.CURRENCY_USD_ID;
			}else {
				ResCurrencyId = currencyId;
			}

			try {

				String sql = " SELECT " +
									" w.user_name writer_name, " +
									" s.display_name skin, " +
									" u.id User_id, " +
									" u.TIME_SC_TURNOVER, " +
									" u.TIME_SC_HOUSE_RESULT, " +
									" (CASE WHEN " + currencyId + " = " + ConstantsBase.ALL_FILTER_ID + " THEN " +
											"  msc.group_merged_tx_sum_USD " +
										" ELSE " +
											"  msc.group_merged_tx_sum END) deposit_amount, " +
									" t.time_created, " +
									" t.id as tid, " +
									" ia.action_time, " +
									" msc.tx_settled_time as time_settled, " +
									" (CASE WHEN " + currencyId + " = " + ConstantsBase.ALL_FILTER_ID + " THEN " +
											" (t.amount - msc.cycle_reached_sum)*t.rate " +
										" ELSE " +
											" (t.amount - msc.cycle_reached_sum) END)Turnover_left, " +
					                " c.display_name currency, " +
					                " p.name population_name, " +
					                " (CASE WHEN first_deposits.id = t.id THEN 1 ELSE 0 END) is_first_deposit," +
					                " mco.campaign_id," +
					                " mca.name capmaign_name," +
					                " u.time_created reg_time, " +
					                " pe.qualification_time " +
							 " FROM " +
							 		" merged_sales_conversions msc, " +
							 		" transactions_issues ti, " +
							        " issue_actions ia, " +
							        " writers w, " +
							        " skins s, " +
							        " currencies c, " +
							        " transactions t, " +
							        " users u " +
							        	" left join transactions first_deposits on u.first_deposit_id = first_deposits.id, " +
							        " issues i " +
							        	" left join population_entries pe on i.population_entry_id = pe.id " +
							        		" left join populations p on pe.population_id = p.id," +
							        " marketing_combinations mco, " +
							        " marketing_campaigns mca " +
							  " WHERE " +
							  		" msc.group_tx_id = ti.transaction_id " +
							  		" and ti.transaction_id = t.id " +
							  		" and u.combination_id = mco.id " +
							  		" and mco.campaign_id = mca.id " +
								    " and t.user_id = u.id " +
								    " and w.id = ia.writer_id " +
								    " and ti.issue_action_id = ia.id " +
								    " and ia.issue_id = i.id " +
								    " and u.skin_id = s.id " +
								    " and u.currency_id = c.id " +
								    " AND t.status_id IN (2,7,8) " +
								    " and u.class_id > 0 " +
								    " and u.skin_id in (" + w.getSkinsToString() + ") ";

				if (userId != 0 ){
					sql += 			" and u.id = " + userId + " ";
				}

				if (transactionId != 0){
					sql += 			" and t.id = " + transactionId + " ";
				}


				if (dateFilterType.equals(Constants.SALES_TIME_SETTLED_FILTER)){
					sql +=		    " and ti.time_settled between ? and ? ";
				} else {
					sql += 			" and t.time_created between ? and ? ";

					if (Constants.SALES_QUAL_FILTER_QUALIFIED == qualifiedFilter){
						sql += 		" and ti.time_settled is not null ";
					} else if (Constants.SALES_QUAL_FILTER_NOT_QUALIFIED == qualifiedFilter){
						sql += 		" and ti.time_settled is null ";
					}
				}

				if (ConstantsBase.SALES_DEPOSIT_TYPE_CONVERSION == firstDepositFilter){
					sql += 			" and first_deposits.id = t.id ";
				} else if (ConstantsBase.SALES_DEPOSIT_TYPE_RETENTION == firstDepositFilter){
					sql += 			" and first_deposits.id != t.id ";
				}

	            if (currencyId > ConstantsBase.ALL_FILTER_ID){
	            	sql += 			" and u.currency_id = " + currencyId + " ";
	            }

	            if (skinId > ConstantsBase.ALL_FILTER_ID){
	            	sql += 			" and u.skin_id = " + skinId + " ";
	            }

	            if (writerId > ConstantsBase.ALL_FILTER_ID){
	            	sql += 			" and ia.writer_id = " + writerId + " ";
	            }

				sql +=		" ORDER BY t.time_created ";

				ps = con.prepareStatement(sql);

				ps.setTimestamp(1, CommonUtil.convertToTimeStamp(CommonUtil.getDateTimeFormat(from, Utils.getAppData().getUtcOffset())));
				ps.setTimestamp(2, CommonUtil.convertToTimeStamp(CommonUtil.getDateTimeFormat(CommonUtil.addDay(to), Utils.getAppData().getUtcOffset())));

				rs = ps.executeQuery();

				while (rs.next()) {
					SalesDepositsDetails sdd = new SalesDepositsDetails();
					SpecialCare specialCare = new SpecialCare();
					sdd.setWriterName(rs.getString("writer_name"));
					sdd.setSkin(rs.getString("skin"));
					sdd.setUserId(rs.getLong("user_id"));
                    specialCare.setTimeScTurnover(convertToDate(rs.getTimestamp("TIME_SC_TURNOVER")));
                    specialCare.setTimeScHouseResult(convertToDate(rs.getTimestamp("TIME_SC_HOUSE_RESULT")));
                    sdd.setSpecialCare(specialCare);
					//sdd.setSpecialCare(rs.getString("TIME_SC_TURNOVER") != null || rs.getString("TIME_SC_HOUSE_RESULT") != null);
					sdd.setTransactionId(rs.getLong("tid"));
					sdd.setRegTime(convertToDate(rs.getTimestamp("reg_time")));
					sdd.setCapmaignId(rs.getLong("campaign_id"));
					sdd.setCapmaignName(rs.getString("capmaign_name"));

					double depositAmount = rs.getDouble("deposit_amount");
					sdd.setDepositAmount(CommonUtil.displayAmount(depositAmount,true,ResCurrencyId));

					sdd.setDepositTime(convertToDate(rs.getTimestamp("time_created")));
					sdd.setActionTime(convertToDate(rs.getTimestamp("action_time")));
					sdd.setTranQualTime(convertToDate(rs.getTimestamp("time_settled")));
					sdd.setPopQualTime(convertToDate(rs.getTimestamp("qualification_time")));

					sdd.setTurnoverLeft(CommonUtil.displayAmount(rs.getDouble("turnover_left"),true,ResCurrencyId));
					sdd.setCurrency(rs.getString("currency"));
					sdd.setPopulationName(rs.getString("population_name"));

					if (rs.getInt("is_first_deposit")==0){
						sdd.setSalesDepositType(ConstantsBase.SALES_DEPOSIT_TYPE_RETENTION);
					}else {
						sdd.setSalesDepositType(ConstantsBase.SALES_DEPOSIT_TYPE_CONVERSION);
					}


					list.add(sdd);

					depositAmountSum += depositAmount;
				}
				sddSum.setDepositAmount(CommonUtil.displayAmount(depositAmountSum,true,ResCurrencyId));
				list.add(sddSum);

			} finally {
				closeResultSet(rs);
				closeStatement(ps);
			}
			return list;
		}

		public static ArrayList<SalesTurnoversDetails> getSaledDepositsDetailsOfRetention(Connection conn, Date from, Date to, long writerId, long currencyId, long skinId, long salesTypeDepId, long tranClassTypeId) throws SQLException{
			ArrayList<SalesTurnoversDetails> list =  new ArrayList<SalesTurnoversDetails>();
			ResultSet rs = null;
			PreparedStatement ps = null;
			int index = 1;
			long totalBaseAmount = 0;
			try{
				String sql = " SELECT " +  	
                                "  u.id as user_id, " +
                                "  w.USER_NAME as rep_name, " +
                                "  t.id as tran_id, " +
                                "  t.time_created as date_created, " +
                                "  t.amount as amount, " +
                                "  (t.amount * t.rate) as base_amount, " +
                                "  u.skin_id as skin, " +
                                "  u.currency_id as curr  " +
                            " FROM 	" +
                        		"							writers_commission_dep wcd, " + 			
                        		"   						transactions t, " + 
                        		"						    transaction_types tt, " +
                        		"   						users u, " + 			
                        		"   						writers w " + 		
                        		"						 WHERE 		" +
                        		"							t.id = wcd.transaction_id AND " +
                        		"   						t.time_created >= ?  AND " + 			
                        		"   						t.time_created < ? + 1 AND " + 			
                        		"							t.user_id = u.id AND " + 
                        		"   						t.status_id in (2,7,8) AND " +
                        		"						    t.type_id = tt.id AND " +
                        		"   						w.sales_type in (2,3) AND " +
                        		"						    w.id = wcd.writer_id " +
                        		(skinId != 0 ?    "  AND u.skin_id = ?  ": "") +
                        		(currencyId != 0 ? " AND u.currency_id = ? " : "" ) +
                        		(writerId != 0 ?  "  AND w.id = ? " : "" ) +
								(salesTypeDepId > 0 ? " AND w.sales_type_dept_id = ? " : "" ) +
								(tranClassTypeId > 0 ? " AND tt.class_type = ? " : "" );
				ps = conn.prepareStatement(sql);
				
				ps.setTimestamp(index++, CommonUtil.convertToTimeStamp(from));
				ps.setTimestamp(index++, CommonUtil.convertToTimeStamp(to));
				if(skinId != 0 ){
					ps.setLong(index++, skinId);
				}
				if(currencyId != 0){
					ps.setLong(index++, currencyId);
				}
				if(writerId != 0 ){
					ps.setLong(index++, writerId);
				}
				if (salesTypeDepId > 0) {
					ps.setLong(index++, salesTypeDepId);
				}
				if (tranClassTypeId > 0) {
					ps.setLong(index++, tranClassTypeId);
				}
				rs = ps.executeQuery();		
				
				while (rs.next()){
					SalesTurnoversDetails vo = new SalesTurnoversDetails();
					vo.setUserId(rs.getLong("user_id"));
					vo.setWriterName(rs.getString("rep_name"));
					vo.setTranId(rs.getLong("tran_id"));
					vo.setSkinId(rs.getLong("skin"));
					vo.setDate(rs.getString("date_created"));
					vo.setTranAmount(CommonUtil.displayAmount(rs.getLong("amount"), rs.getLong("curr")));
					vo.setBaseTranAmount(CommonUtil.displayAmount(rs.getLong("base_amount"), Constants.CURRENCY_BASE_ID));
					long baseAmount = rs.getLong("base_amount") ;
					if(baseAmount > 0){
						totalBaseAmount += baseAmount; 
					}					
					list.add(vo);
				}
				SalesTurnoversDetails vo = new SalesTurnoversDetails();
				vo.setBaseTranAmount(CommonUtil.displayAmount(totalBaseAmount, Constants.CURRENCY_BASE_ID));
				list.add(vo);
						
			} finally {
				closeResultSet(rs);
				closeStatement(ps);
			}
			return list;
		}
		
		/**
		 * Get sales deposits summary views
		 * @param con
		 * @param period daily, weekly, monthly
		 * @param isEtrader get etrader writers or anyoption
		 * @return
		 * @throws SQLException
		 */
		public static ArrayList<SalesDepositsTotals> getSalesWriterDepositsTv(Connection con, int period, boolean isEtrader, boolean isAllSkins, boolean etraderOnly) throws SQLException {
			PreparedStatement ps = null;
			ResultSet rs = null;
			ArrayList<SalesDepositsTotals> list = new ArrayList<SalesDepositsTotals>();
			SalesDepositsTotals emptyLine = null;
			long repNumb = 1;

			long currencyId = Constants.CURRENCY_USD_ID;
//			String amountTxt = "(A.amount * A.rate)";
			long totalCount = 0;
			long totalAmount = 0;
			long totalTurnover = 0;
			String periodToStringTran = "";
			String periodToStringInv = "";
			
			if (period == Constants.SALES_DEPOSIT_MONTHLY_VIEW) {				
				periodToStringInv = " AND i.time_created between trunc(sysdate,'mm') and sysdate ";
				periodToStringTran = " AND t.time_created between trunc(sysdate,'mm') and sysdate ";
			} else if (period == Constants.SALES_DEPOSIT_DAILY_VIEW) {
				periodToStringInv = " AND i.time_created between trunc(sysdate) and sysdate ";
				periodToStringTran = " AND t.time_created between trunc(sysdate) and sysdate ";
			}
			
			try { 
				String sql =" SELECT " +
								" w.first_name, w.last_name, w.id, " +
								" NVL(A.transactions_sum, 0) AS deposits_count, " +
								" NVL(A.transactions_amount, 0) as deposits_amount, " +
								" NVL(B.investments_amount, 0) as turnover_amount, " +
								" NVL(A.is_new_record, 0) as is_new_record, " +
								" w.is_active," +
								" w.sales_type " +
							" FROM " +
								( etraderOnly ? " writers_skin ws, " : "") +
								" writers w " +
								" LEFT JOIN ( SELECT " +
												" w.id as writer_id, " +
												" sum(i.amount * i.rate * wci.factor) as investments_amount, " +
												" count(i.id) as investments_sum " +
											 " FROM " +
											 	" writers_commission_inv wci, " +
											 	" investments i, " +
											 	" writers w " +
											 " WHERE " +
											 	" w.id = wci.writer_id AND " +
											 	" wci.investments_id = i.id AND " +
											 	" i.is_canceled = 0 AND " +
											 	" w.sales_type IN (" + Constants.SALES_TYPE_RETENTION + "," + 
											 						   Constants.SALES_TYPE_BOTH + ") " + 
											 	periodToStringInv +
											 " GROUP BY " +
											 	" w.id )  B ON B.writer_id = w.id " +
								" LEFT JOIN ( SELECT " +
												" w.id as writer_id, " +
												" sum(t.amount * t.rate) as transactions_amount, " +
												" SUM(CASE WHEN wcd.time_created >= (sysdate - interval '5' minute)  THEN 1 ELSE 0 END) AS is_new_record, " +
												" count(t.id) as transactions_sum " +
											 " FROM " +
											 	" writers_commission_dep wcd, " +
											 	" transactions t, " +
											 	" transaction_types tt," +
											 	" writers w " +
											 " WHERE " +
											 	" w.id = wcd.writer_id AND " +
											 	" wcd.transaction_id = t.id AND " +
											 	" w.sales_type IN (" + Constants.SALES_TYPE_RETENTION + "," + 
											 						   Constants.SALES_TYPE_BOTH + ") AND " + 			
                                                " t.status_id in (2,7,8) AND " +
                                                " t.type_id = tt.id AND " +
                                                " tt.class_type = 1 " +
											 	periodToStringTran +
											 " GROUP BY " +
											 	" w.id   )A ON A.writer_id = w.id " +
							" WHERE " +
								" w.is_active = " + Constants.SALES_ACTIVE_REP + 
								( etraderOnly ? " AND w.id = ws.writer_id AND ws.skin_id = ? " : "") +
								" AND w.dept_id = " + Constants.SALES_DEPARTMENT_ID +
								" AND w.sales_type IN (" + Constants.SALES_TYPE_RETENTION + "," + Constants.SALES_TYPE_BOTH + ") " + 	
							" ORDER BY " +
								" deposits_amount DESC, turnover_amount DESC, lower(w.first_name)";											


				ps = con.prepareStatement(sql);
				if (etraderOnly) {
					ps.setLong(1, Skin.SKIN_ETRADER);
				}
				rs = ps.executeQuery();

				while (rs.next()){
					SalesDepositsTotals t = new SalesDepositsTotals();
					t.setWriterName(rs.getString("first_name") + " " + rs.getString("last_name"));
					t.setWriterId(rs.getLong("id"));
					String depositCount = rs.getString("deposits_count");
					t.setRetTranMadeCount(depositCount);
					Long depositAmount = rs.getLong("deposits_amount");					
					t.setRetTranMadeSum(CommonUtil.displayAmount(depositAmount, currencyId));
					Long turnover = rs.getLong("turnover_amount");
					t.setRetTranTurnoverSum(CommonUtil.displayAmount(turnover, currencyId));
					int countNewRec = rs.getInt("is_new_record");

					if (null != depositCount) {
						totalCount += Long.valueOf(depositCount);
					}

					if (null != depositAmount) {
						totalAmount += depositAmount;
					}
					
					if(null != turnover){
						totalTurnover += turnover;
					}

					if (countNewRec > 0) {  // new record type
						t.setRowType(SalesDepositsTotals.ROW_TYPE_NEW);
					} else {
						t.setRowType(SalesDepositsTotals.ROW_TYPE_TOTAL);
					}
					if (t.getRetTranMadeCount().equals("0") && rs.getLong("deposits_amount") == 0) {
						if (null == emptyLine) {  // empty line
							emptyLine = new SalesDepositsTotals();
							emptyLine.setRowType(SalesDepositsTotals.ROW_TYPE_EMPTY);
							list.add(emptyLine);
						}
					}

					// display writer only if is active and has relevant sales type
					boolean isWriterActive = rs.getInt("is_active")==1?true:false;

					if (isWriterActive){
						int salesType = rs.getInt("sales_type");

						if (ConstantsBase.SALES_DEPOSIT_TYPE_CONVERSION == salesType ||
								ConstantsBase.SALES_DEPOSIT_TYPE_BOTH == salesType ||
								(ConstantsBase.SALES_DEPOSIT_TYPE_RETENTION == salesType && depositAmount > 0)){
							t.setRepNumb(repNumb++);
							list.add(t);
						}
					}
				}
				// Summary row
				SalesDepositsTotals total = new SalesDepositsTotals();
				total.setWriterName("Summary");
				total.setConTranMadeCount(String.valueOf(totalCount));
				total.setConTranMadeSum(CommonUtil.displayAmount(totalAmount, currencyId));
				total.setConTranMadeAvg(CommonUtil.displayAmount(totalTurnover, currencyId));
				list.add(total);
			} finally {
				closeResultSet(rs);
				closeStatement(ps);
			}
			return list;
		}


		/**
		 * Check if transaction or issue action exists on TransactionIssue table
		 * @param con
		 * @param tranId
		 * @param issueActionId
		 * @return
		 * @throws SQLException
		 */
		public static ArrayList<TransactionsIssues> getIsTranOnTiTable(Connection con, long tranId, long issueActionId) throws SQLException{
			ArrayList<TransactionsIssues> list = new ArrayList<TransactionsIssues>();
			PreparedStatement ps = null;
			ResultSet rs = null;
			try {

				String sql =" SELECT " +
								" ti.transaction_id, " +
								" ti.issue_action_id, " +
								" ti.time_inserted, " +
								" ti.insert_type, " +
								" t.user_id, " +
								" t.id TID, " +
								" ia.id IAID," +
								" ia.issue_id, " +
								" ia.action_time, " +
								" ia.transaction_id, " +
								" ia.action_time_offset, " +
								" ia.writer_id " +
							" FROM " +
								" transactions t " +
									" FULL OUTER JOIN transactions_issues ti on t.id=ti.transaction_id " +
										" FULL OUTER join issue_actions ia on ia.id=ti.issue_action_id " +
							" WHERE  " +
								" t.id=? OR ia.id=? ";


			ps = con.prepareStatement(sql);
			ps.setLong(1, tranId);
			ps.setLong(2, issueActionId);

			rs = ps.executeQuery();
			while(rs.next()){
				TransactionsIssues tiTmp = new TransactionsIssues();
				tiTmp.setTransactionId(rs.getLong("transaction_id"));
				tiTmp.setIssueActionId(rs.getLong("issue_action_id"));
				tiTmp.setTimeInserted(convertToDate(rs.getTimestamp("time_inserted")));
				tiTmp.setInsertType(rs.getInt("insert_type"));
				tiTmp.getTransaction().setId(rs.getLong("TID"));
				tiTmp.getTransaction().setUserId(rs.getLong("user_id"));
				tiTmp.getIssueAction().setActionId(rs.getLong("IAID"));
				tiTmp.getIssueAction().setIssueId(rs.getLong("issue_id"));
				tiTmp.getIssueAction().setActionTime(convertToDate(rs.getTimestamp("action_time")));
				tiTmp.getIssueAction().setTransactionId(rs.getLong("transaction_id"));
				tiTmp.getIssueAction().setActionTimeOffset(rs.getString("action_time_offset"));
				tiTmp.getIssueAction().setWriterId(rs.getLong("writer_id"));
				list.add(tiTmp);
			}

			}
			finally{
				closeStatement(ps);
				closeResultSet(rs);
			}

			return list;

		}

		/**
		 * Get Conversion deposits summary views
		 * @param con
		 * @param period daily, weekly, monthly
		 * @param isEtrader get etrader writers or anyoption
		 * @return
		 * @throws SQLException
		 */
		public static ArrayList<SalesDepositsTotals> getConversionWriterDepositsTv(Connection con,
				boolean isEtrader, boolean isAllSkins, boolean isDaily, boolean fromConversion, long skinId) throws SQLException {
			PreparedStatement ps = null;
			ResultSet rs = null;
			ArrayList<SalesDepositsTotals> list = new ArrayList<SalesDepositsTotals>();
			SalesDepositsTotals emptyLine = null;
			FacesContext context = FacesContext.getCurrentInstance();
			WriterWrapper w = (WriterWrapper) context.getApplication().createValueBinding(Constants.BIND_WRITER_WRAPPER).getValue(context);
			long offset = Utils.getUtcOffsetValue(w.getUtcOffset());
			long totalConTodayCount = 0;
			long totalConTodayAmount = 0;
			long totalConMontlyCount = 0;
			long totalConMontlyAmount = 0;
			long maxConTodayAmount = 0;
			long maxConMonthlyAmount = 0;
			long repNumb = 1;

			long currencyId = Constants.CURRENCY_USD_ID;
			String amountTxt = "(t.amount * t.rate)";

//			if (!isEtrader || isAllSkins) {
//				currencyId = Constants.CURRENCY_USD_ID;
//				amountTxt = "(t.amount * t.rate)";
//				skinTxt = "AND ws.skin_id <> " + ConstantsBase.SKIN_ETRADER;
//			}
//
//			if (isAllSkins) {  // without skins filter
//				skinTxt = "";
//			}

			try {
				String sql =
					"SELECT * FROM ( " +
					" SELECT " +
				        " w.first_name, " +
				        " w.last_name, " +
				        " w.id, " +
				        " NVL(wd.con_num_today,0) con_num_today, " +
				        " NVL(wd.con_amount_today,0) con_amount_today, " +
				        " NVL(wd.con_num_month,0) con_num_month, " +
				        " NVL(wd.con_amount_month,0) con_amount_month, " +
				        " NVL(wd.max_amount_today,0) max_amount_today, " +
				        " NVL(wd.max_amount_month,0) max_amount_month, " +
				        " NVL(is_new_record,0) is_new_record, " +
				        " w.is_active, " +
				        " w.sales_type " +
					" FROM " +
					"	  writers_skin ws, ";
				 sql += " writers w " +
					     	  " LEFT JOIN (SELECT " +
					     	  					" ia.writer_id writer_id, " +
					     	  					" sum(CASE WHEN trunc(t.time_created + (1/24)* " + offset + ") = trunc(sysdate) THEN 1 ELSE 0 END) con_num_today, " +
					     	  					" sum(CASE WHEN trunc(t.time_created + (1/24)* " + offset + ") = trunc(sysdate) THEN " + amountTxt + " ELSE 0 END) con_amount_today, " +
					     	  					" max(CASE WHEN trunc(t.time_created + (1/24)* " + offset + ") = trunc(sysdate) THEN " + amountTxt + " ELSE 0 END) max_amount_today, " +
					     	  					" count(*) con_num_month, " +
					     	  					" sum(" + amountTxt + ") con_amount_month, " +
					     	  					" max(" + amountTxt + ") max_amount_month, " +
					     	  					" SUM(CASE WHEN ti.time_inserted >= (sysdate - interval '5' minute)  THEN 1 ELSE 0 END) AS is_new_record " +
                                          " FROM " +
		                                         " issue_actions ia, " +
		                                         " transactions_issues ti, " +
		                                         " transactions t, " +
		                                         " users_first_deposit ufd, " +
		                                         " users u, " +
		                                         " skins s " +
		                                  " WHERE " +
		                                  		 " t.id = ti.transaction_id " +
		                                  	     " and u.id = t.user_id " +
		                                  	     " and u.class_id <> 0 " +
		                                  		 " and ti.issue_action_id = ia.id " +
		                                  	     " and t.time_created >= TRUNC(sysdate,'MM') - (1/24)* " + offset + " " +
		                                  		 " and t.id = ufd.first_deposit_id " +
		                                  		 " and u.skin_id = s.id " +
		                                  		 " and s.business_case_id != " + SkinsManagerBase.BUSINESS_CASE_PARTNERS + " " +
		                                  " GROUP BY " +
		                                  		 " ia.writer_id) wd on w.id = wd.writer_id ";
//					" WHERE " +
//					      " w.is_active = 1 " +
//					      " AND (w.sales_type in (" + Constants.SALES_DEPOSIT_TYPE_CONVERSION + "," +
//					      							 Constants.SALES_DEPOSIT_TYPE_BOTH  + ") " +
//					      		" OR " +
//					      		" (w.sales_type =  " + Constants.SALES_DEPOSIT_TYPE_RETENTION + " AND NVL(wd.con_num_month,0) > 0)" +
//					      		") " +
//					      " AND EXISTS (select 1  " +
//	 	                              " from " +
//	 	                              	" issue_actions ia, " +
//	 	                              	" transactions_issues ti " +
//					                  " where " +
//					                  	" and ti.issue_action_id = ia.id and " +
//					                    " r.role like ? ) " +
//					      " AND w.dept_id = ? " +
//					      " AND EXISTS (select 1 " +
//				          				" from writers_skin ws " +
//				          				" where ws.writer_id = w.id " + skinTxt +
//				          				" ) " +
//				          " AND NOT EXISTS (select 1 " +
//				                  	 		" from " +
//				                  	 			" writers_skin ws, " +
//				                  	 			" skins s " +
//				                  	 		" where " +
//				                  	 			" s.id = ws.skin_id " +
//				                  	 			" AND ws.writer_id = w.id " +
//				                  	 			" AND s.business_case_id = ?) ";
				sql += " WHERE w.id = ws.writer_id " +
					   " AND w.dept_id = ? ";
				if (skinId > 0) {
					sql += " AND ws.skin_id = ? ";
				} else {
					sql += " AND ws.skin_id <> ? ";
				}
				if (fromConversion) {
						if (isDaily) {
							sql +=
								" ORDER BY " +
									" NVL(wd.con_num_today,0) DESC, " +
									" w.first_name ";
						} else {
							sql +=
							" ORDER BY " +
								" NVL(wd.con_num_month,0) DESC, " +
								" w.first_name ";
						}
					} else {
						if (isDaily) {
							sql +=
								" ORDER BY " +
									" NVL(wd.con_amount_today,0) DESC, " +
									" w.first_name ";
						} else {
							sql +=
							" ORDER BY " +
								" NVL(wd.con_amount_month,0) DESC, " +
								" w.first_name ";
						}
					}
							sql += ") GROUP BY  first_name, last_name, id, con_num_today, con_amount_today, con_num_month, con_amount_month, max_amount_today, max_amount_month, is_new_record, is_active,sales_type " +
								   " ORDER BY  con_amount_today DESC, first_name ";
				ps = con.prepareStatement(sql);
				ps.setLong(1, Constants.SALES_DEPARTMENT_ID);
				if (skinId > 0) {
					ps.setLong(2, skinId);					
				} else {
					ps.setLong(2, Skins.SKIN_CHINESE);
				}

				rs = ps.executeQuery();

				while (rs.next()){
					SalesDepositsTotals t = new SalesDepositsTotals();
					t.setWriterName(rs.getString("first_name") + " " + rs.getString("last_name"));
					long repTodayCount = rs.getLong("con_num_today");
					long repTodayAmount = rs.getLong("con_amount_today");
					long repTodayAvg = 0;
					if (repTodayCount > 0) {
						repTodayAvg = repTodayAmount / repTodayCount;
					}

					t.setConTranMadeCount(String.valueOf(repTodayCount));
					t.setConTranMadeSum(CommonUtil.displayAmount(repTodayAmount, currencyId));
					t.setConTranMadeAvg(CommonUtil.displayAmount(repTodayAvg, currencyId));

					if (repTodayCount > 0){
						long dailyStrike = repTodayAmount / repTodayCount;
						t.setConDailyStrike(CommonUtil.displayAmount(dailyStrike, currencyId));
						totalConTodayAmount += repTodayAmount;
						maxConTodayAmount = Math.max(maxConTodayAmount, rs.getLong("max_amount_today"));
						totalConTodayCount += repTodayCount;
					} else {
						t.setConDailyStrike(CommonUtil.displayAmount(0, currencyId));
						// Add red line
						if (null == emptyLine) {  // empty line
							emptyLine = new SalesDepositsTotals();
							emptyLine.setRowType(SalesDepositsTotals.ROW_TYPE_EMPTY);
							list.add(emptyLine);
						}
					}

					long repMonthlyCount = rs.getLong("con_num_month");
					long repMonthlyAmount = rs.getLong("con_amount_month");

					t.setConTranMadeCountMonth(String.valueOf(repMonthlyCount));
					t.setConTranMadeSumMonth(CommonUtil.displayAmount(repMonthlyAmount, currencyId));
					long repMonthlyAvg = 0;
					if (repMonthlyCount > 0) {
						repMonthlyAvg = repMonthlyAmount / repMonthlyCount;
					}
					t.setConTranMadeAvgMonth(CommonUtil.displayAmount(repMonthlyAvg, currencyId));
					if (repMonthlyCount > 0){
						totalConMontlyCount += repMonthlyCount;
						totalConMontlyAmount += repMonthlyAmount;
						maxConMonthlyAmount = Math.max(maxConMonthlyAmount, rs.getLong("max_amount_month"));
						long montlyStrike = repMonthlyAmount / repMonthlyCount;
						t.setConMonthlyStrike(CommonUtil.displayAmount(montlyStrike, currencyId));
					}else{
						t.setConMonthlyStrike(CommonUtil.displayAmount(0, currencyId));
					}

					int countNewRec = rs.getInt("is_new_record");
					if (countNewRec > 0) {  // new record type
						t.setRowType(SalesDepositsTotals.ROW_TYPE_NEW);
					} else {
						t.setRowType(SalesDepositsTotals.ROW_TYPE_TOTAL);
					}

					// display writer only if is active and has relevant sales type
					boolean isWriterActive = rs.getInt("is_active")==1?true:false;

					if (isWriterActive){
						int salesType = rs.getInt("sales_type");

						if (ConstantsBase.SALES_DEPOSIT_TYPE_CONVERSION == salesType ||
								ConstantsBase.SALES_DEPOSIT_TYPE_BOTH == salesType ||
								(ConstantsBase.SALES_DEPOSIT_TYPE_RETENTION == salesType && repMonthlyAmount > 0)){
							t.setRepNumb(repNumb++);
							list.add(t);
						}
					}

				}
				SalesDepositsTotals totalToday = new SalesDepositsTotals();
				totalToday.setWriterName("Daily");
				totalToday.setConTranMadeCount(String.valueOf(totalConTodayCount));
				totalToday.setConTranMadeSum(CommonUtil.displayAmount(totalConTodayAmount, currencyId));
				long totalConTodayAvg = 0;
				if (totalConTodayCount > 0) {
					totalConTodayAvg = totalConTodayAmount / totalConTodayCount;
				}
				totalToday.setConTranMadeAvg(CommonUtil.displayAmount(totalConTodayAvg, currencyId));

				//Note: overall strike changed to max amount for day
				totalToday.setConMonthlyStrike(CommonUtil.displayAmount(maxConTodayAmount, currencyId));
				list.add(totalToday);

				SalesDepositsTotals totalMonthly = new SalesDepositsTotals();
				totalMonthly.setWriterName("Monthly");
				totalMonthly.setConTranMadeCount(String.valueOf(totalConMontlyCount));
				totalMonthly.setConTranMadeSum(CommonUtil.displayAmount(totalConMontlyAmount, currencyId));
				long totalConMontlyAvg = 0;
				if (totalConMontlyCount > 0) {
					totalConMontlyAvg = totalConMontlyAmount / totalConMontlyCount;
				}
				totalMonthly.setConTranMadeAvg(CommonUtil.displayAmount(totalConMontlyAvg, currencyId));

				//	Note: overall strike changed to max amount for month
				totalMonthly.setConMonthlyStrike(CommonUtil.displayAmount(maxConMonthlyAmount, currencyId));
				list.add(totalMonthly);
			} finally {
				closeResultSet(rs);
				closeStatement(ps);
			}
			return list;
		}

		/**
		 * add new sales deposits since last access to db to sales deposits list
		 * @param con
		 * @param timeLastSalesTvDbCheck
		 * @throws SQLException
		 */
		public static void addNewDepositsForTvFromDb(Connection con, Date timeLastSalesTvDbCheck) throws SQLException {
			PreparedStatement ps = null;
			ResultSet rs = null;

			try {

				String sql = " SELECT " +
								" u.skin_id user_skin_id, " +
								" u.currency_id user_currency_id, " +
								" t.amount," +
								" w.id writer_id," +
								" w.user_name writer_name," +
								" CASE WHEN u.first_deposit_id = t.id THEN 1 ELSE 0 END is_first_deposit " +
							 " FROM " +
							 	" transactions_issues ti, " +
							 	" issue_actions ia, " +
							 	" writers w, " +
							 	" transactions t," +
							 	" users u " +
							 " WHERE " +
							 	" ti.issue_action_id = ia.id " +
							 	" and ia.writer_id = w.id " +
							 	" and ti.transaction_id = t.id " +
							 	" and t.user_id = u.id " +
							 	" and ti.time_inserted > ? " +
							 	" and ti.insert_type != ? " +
					            " and w.sales_type is not null ";

				ps = con.prepareStatement(sql);
				ps.setTimestamp(1, CommonUtil.convertToTimeStamp(timeLastSalesTvDbCheck));
				ps.setInt(2, Constants.TRAN_ISSUES_INSERT_ONLINE);
				rs = ps.executeQuery();

				while (rs.next()){
					Writer w = new Writer();

					w.setId(rs.getLong("writer_id"));
					w.setUserName(rs.getString("writer_name"));
					w.setOnlineDepositSkinId(rs.getLong("user_skin_id"));
					String depositType = ConstantsBase.SALES_DEPOSIT_TYPE_RETENTION_STR;
					int depositTypeId = ConstantsBase.SALES_DEPOSIT_TYPE_RETENTION;

					if (rs.getInt("is_first_deposit") == 1) {
						depositType = ConstantsBase.SALES_DEPOSIT_TYPE_CONVERSION_STR;
						depositTypeId = ConstantsBase.SALES_DEPOSIT_TYPE_CONVERSION;
					}

					w.setOnlineDepositAmount(CommonUtil.displayAmount(rs.getLong("amount"), rs.getLong("user_currency_id")) + " " + depositType);
					w.setOnlineDepositType(depositTypeId);
					ApplicationDataBase.addWriterAfterSuccessDeposit(w);
				}

			} finally {
				closeResultSet(rs);
				closeStatement(ps);
			}
		}

		public static MergeTransaction getMergeTransactions(Connection con, long trxId, boolean isFirstDeposit) throws SQLException {
			PreparedStatement ps = null;
			ResultSet rs = null;

			try {

				String sql = " SELECT " +
						     "		t.id as transaction_id, " +
						     "		t.user_id as user_id, " +
						     "		ia.writer_id as writer_id " +
							 " FROM " +
							 " 		transactions t, " +
							 " 		transactions_issues ti, " +
							 "		issue_actions ia ";
				if (isFirstDeposit) {
					sql +=	 "      ,users u ";
				}
				sql +=		 " WHERE " +
							 "	  t.id = ti.transaction_id AND " +
							 "	  ia.id = ti.issue_action_id AND ";
				if (isFirstDeposit) {
					sql +=	 "	  u.first_deposit_id = t.id AND ";
				}			 
				sql +=		 "    t.id = ? ";

				ps = con.prepareStatement(sql);
				ps.setLong(1, trxId);
				rs = ps.executeQuery();

				while (rs.next()){
					MergeTransaction vo = new MergeTransaction();
					vo.setId(rs.getLong("transaction_id"));
					vo.setUserId(rs.getLong("user_id"));
					vo.setWriterId(rs.getLong("writer_id"));
					return vo;
				}
			} finally {
				closeResultSet(rs);
				closeStatement(ps);
			}	
			return null;
		}

		public static void mergeTransaction(Connection con, long firstTxId, String transactionsStr, long writerId) throws SQLException {
	        PreparedStatement ps = null;
	        try {
	            String sql =
	            	"UPDATE " +
	            		" transactions_issues " +
	            	"SET " +
	            		" transactions_refference_id = ?, " +
	            		" update_writer_id = ?, " +
	            		" time_updated = sysdate " +
	            	"WHERE " +
	            		" transaction_id in ( " + transactionsStr + ")";
	            ps = con.prepareStatement(sql);
	            ps.setLong(1, firstTxId);
	            ps.setLong(2, writerId);
	            ps.executeUpdate();
	        } finally {
	        	closeStatement(ps);
	        }
		}
}
