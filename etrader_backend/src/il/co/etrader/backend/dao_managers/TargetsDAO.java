package il.co.etrader.backend.dao_managers;


import il.co.etrader.backend.bl_managers.ApplicationData;
import il.co.etrader.backend.util.Constants;
import il.co.etrader.backend.util.Utils;
import il.co.etrader.bl_vos.MediaBuyerTarget;
import il.co.etrader.dao_managers.IssuesDAOBase;
import il.co.etrader.util.CommonUtil;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import javax.faces.model.SelectItem;

public class TargetsDAO extends IssuesDAOBase{
    public static boolean validateIsTargetCombinationExist(Connection con, MediaBuyerTarget mediaBuyer) throws SQLException {
        PreparedStatement ps = null;
          ResultSet rs = null;

          try{
              String sql=" SELECT " +
                         "      mmbt.assigned_media_buyer_id, " +
                         "      mmbt.skin_id, " +
                         "      mmbt.month_of_the_target " +
                         " FROM " +
                         "      marketing_media_buyer_target mmbt " +
                         " WHERE " +
                         "      mmbt.assigned_media_buyer_id = ? " +
                         "      AND mmbt.skin_id = ? " +
                         "      AND TO_CHAR(mmbt.month_of_the_target, 'MM') = ? " +
                         "      AND TO_CHAR(mmbt.month_of_the_target, 'YYYY') = ? ";

              ps = con.prepareStatement(sql);
              int parameterIndex = 1;
              // Set month and year to compare
              Calendar d = Calendar.getInstance();
              d.setTime(mediaBuyer.getMonthTarget());
              int monthSelected = d.get(Calendar.MONTH)+1;
              int yearSelected = d.get(Calendar.YEAR);

              ps.setLong(parameterIndex++, mediaBuyer.getMediaBuyerId());
              ps.setLong(parameterIndex++, mediaBuyer.getSkinId());
              ps.setLong(parameterIndex++, monthSelected);
              ps.setLong(parameterIndex++, yearSelected);

              rs = ps.executeQuery();
              if ( rs.next() ) {
                  return true;

              } else {
                  return false;
              }
          }
          finally
          {
              closeStatement(ps);
              closeResultSet(rs);
          }
    }

    public static void insertTarget(Connection con, MediaBuyerTarget mediaBuyer) throws SQLException {
		  PreparedStatement ps = null;
		  ResultSet rs = null;
		  try {

			  String sql=" INSERT INTO " +
			              "  marketing_media_buyer_target " +
                          "     (id, assigned_media_buyer_id, date_of_updating, month_of_the_target, skin_id, target_of_deposits, target_of_registrations, updated_by) " +
		                  "VALUES " +
		                  " (seq_marketing_media_buyer.nextval,?,sysdate,?,?,?,?,?) ";

				ps = con.prepareStatement(sql);

                int parameterIndex = 1;
				ps.setLong(parameterIndex++, mediaBuyer.getMediaBuyerId());
				ps.setTimestamp(parameterIndex++, CommonUtil.convertToTimeStamp(CommonUtil.getDateTimeFormat(mediaBuyer.getMonthTarget(), Utils.getAppData().getUtcOffset())));
                ps.setLong(parameterIndex++, mediaBuyer.getSkinId());
                ps.setLong(parameterIndex++, mediaBuyer.getDepTarget());
                ps.setLong(parameterIndex++, mediaBuyer.getRegTarget());
                ps.setLong(parameterIndex++, (int)Utils.getWriter().getWriter().getId());
                ps.executeUpdate();
                mediaBuyer.setId(getSeqCurValue(con,"seq_marketing_media_buyer"));
		  }
			finally
			{
				closeStatement(ps);
				closeResultSet(rs);
			}

	  }


      public static boolean updateTarget(Connection con, MediaBuyerTarget mediaBuyer) throws SQLException {
          PreparedStatement ps = null;
          ResultSet rs = null;

          try {

              String sql="UPDATE " +
                         "  marketing_media_buyer_target " +
                         "SET" +
                         "  date_of_updating = sysdate, " +
                         "  target_of_deposits = ?, " +
                         "  target_of_registrations = ? " +
                         "WHERE " +
                         "  id = ?" +
                         "  AND sysdate < month_of_the_target";

                ps = con.prepareStatement(sql);
                ps.setLong(1, mediaBuyer.getDepTarget());
                ps.setLong(2, mediaBuyer.getRegTarget());
                ps.setLong(3, mediaBuyer.getId());
                int isUpdated = ps.executeUpdate();

                if (isUpdated == 0) {
                    return false;
                } else {
                    return true;
                }
          } finally {
                closeStatement(ps);
                closeResultSet(rs);
            }
      }


      /**
         * get all templates
         * @param con db connection
         * @return ArrayList of all bonuses
         * @throws SQLException
         */
        public static ArrayList<MediaBuyerTarget> getAllTargets(Connection con, long mediaBuyerId, Date monthDate, long skinId, long skinBusinessCaseId) throws SQLException {
            ArrayList<MediaBuyerTarget> list = new ArrayList<MediaBuyerTarget>();
            PreparedStatement ps = null;
            ResultSet rs = null;
            try {
                String sql =
                    "SELECT " +
                    "   mmbt.*, " +
                    "   reg_users.regMade, " +
                    "   dep_users.depMade " +
                    "FROM " +
                    "   marketing_media_buyer_target mmbt " +

                    /*------------reg_users----------- */
                    "   LEFT JOIN " +
                    "       (SELECT " +
                    "           count(u.id) regMade, " +
                    "           mca.campaign_manager, " +
                    "           mcom.skin_id " +
                    "        FROM " +
                    "           users u, " +
                    "           marketing_combinations mcom, " +
                    "           marketing_campaigns mca " +
                    "        WHERE " +
                    "           u.combination_id = mcom.id " +
                    "           AND u.class_id <> 0  " +
                    "           AND mcom.campaign_id = mca.id " +
//                    "           AND u.time_created between ? and ? ";
                    "           AND u.time_created >= ? " +
                    "           AND u.time_created <= ? " ;

                    if (skinBusinessCaseId > 0) {
                        sql += " AND u.SKIN_ID in (SELECT id FROM skins WHERE business_case_id = ? ) ";
                    }

                    sql +="        GROUP BY " +
                    "           mca.campaign_manager, " +
                    "           mcom.skin_id )  reg_users on reg_users.campaign_manager = mmbt.assigned_media_buyer_id and reg_users.skin_id = mmbt.skin_id " +

                    /*------------dep_users----------- */
                    "   LEFT JOIN " +
                    "       (SELECT " +
                    "           count( u.id) depMade, " +
                    "           mca.campaign_manager, " +
                    "           mcom.skin_id " +
                    "        FROM " +
                    "           users u, " +
                    "           transactions t, " +
                    "           marketing_combinations mcom, " +
                    "           marketing_campaigns mca " +
                    "        WHERE " +
                    "           u.first_deposit_id = t.id  " +
//                    "           AND t.time_created between ? and ? " +
                    "           AND t.time_created >= ? " +
                    "           AND t.time_created <= ? " +
                    "           AND u.class_id <> 0  " +
                    "           AND u.combination_id = mcom.id  " +
                    "           AND mcom.campaign_id = mca.id ";

                    if (skinBusinessCaseId > 0) {
                        sql += " AND u.SKIN_ID in (SELECT id FROM skins WHERE business_case_id = ? ) ";
                    }

                    sql +="     GROUP BY " +
                    "           mca.campaign_manager, " +
                    "           mcom.skin_id  ) dep_users on mmbt.assigned_media_buyer_id = dep_users.campaign_manager and dep_users.skin_id = mmbt.skin_id " +
                    "WHERE " +
                    "   1=1 " +
                    "   AND extract (month from mmbt.MONTH_OF_THE_TARGET)  = ? " +
                    "   AND extract (year from mmbt.MONTH_OF_THE_TARGET)  = ? ";


                if (skinBusinessCaseId > 0) {
                    sql += " AND mmbt.SKIN_ID in (SELECT id FROM skins WHERE business_case_id = ? ) ";
                }

                if (mediaBuyerId > 0){
                    sql+="   AND mmbt.assigned_media_buyer_id = ? ";
                }

                if (skinId > 0){
                    sql+=" AND mmbt.skin_id = ? ";
                }

                sql +="ORDER BY mmbt.id ";

                //set dates
                GregorianCalendar gc = new GregorianCalendar();
                Calendar c = Calendar.getInstance();
                c.setTime(monthDate);
                c.set(Calendar.HOUR_OF_DAY, 00);
                c.set(Calendar.MINUTE, 00);
                c.set(Calendar.SECOND, 00);
                c.set(Calendar.MILLISECOND, 00);
                Timestamp startTs = CommonUtil.convertToTimeStamp(c.getTime());
                Timestamp endTs = null;
                //monthly search
                int daysInMonth = gc.getActualMaximum(GregorianCalendar.DAY_OF_MONTH);
                c.set(Calendar.DAY_OF_MONTH, daysInMonth);
                endTs = CommonUtil.convertToTimeStamp(c.getTime());

                int parameterIndex = 1;

                ps = con.prepareStatement(sql);
                //reg users
                ps.setTimestamp(parameterIndex++, startTs);
                ps.setTimestamp(parameterIndex++, endTs);

                if (skinBusinessCaseId > 0) {
                    ps.setLong(parameterIndex++, skinBusinessCaseId);
                }

                //dep users
                ps.setTimestamp(parameterIndex++, startTs);
                ps.setTimestamp(parameterIndex++, endTs);

                if (skinBusinessCaseId > 0) {
                    ps.setLong(parameterIndex++, skinBusinessCaseId);
                }

                // general
                c.setTime(monthDate);
                ps.setLong(parameterIndex++, c.get(Calendar.MONTH)+1);
                ps.setLong(parameterIndex++, c.get(Calendar.YEAR));


                if (skinBusinessCaseId > 0) {
                    ps.setLong(parameterIndex++, skinBusinessCaseId);
                }

                if (mediaBuyerId > 0){
                    ps.setLong(parameterIndex++, mediaBuyerId);
                }

                if (skinId > 0){
                    ps.setLong(parameterIndex++, skinId);
                }

                rs = ps.executeQuery();
                while ( rs.next() ) {
                    MediaBuyerTarget vo = new MediaBuyerTarget();
                    vo.setId(rs.getLong("id"));
                    vo.setMediaBuyerId(rs.getLong("assigned_media_buyer_id"));
                    vo.setSkinId(rs.getLong("skin_id"));
                    vo.setMonthTarget(rs.getDate("MONTH_OF_THE_TARGET"));
                    vo.setRegTarget(rs.getLong("TARGET_OF_REGISTRATIONS"));
                    vo.setDepTarget(rs.getLong("TARGET_OF_DEPOSITS"));
                    vo.setSkinName(ApplicationData.getSkinById(rs.getLong("skin_id")).getDisplayName());
                    vo.setRegMade(rs.getLong("REGMADE"));
                    vo.setDepMade(rs.getLong("DEPMADE"));
                    vo.setTotalTargetReg(MediaBuyerTarget.totalTargetReg + rs.getLong("TARGET_OF_REGISTRATIONS"));
                    vo.setTotalTargetDep(MediaBuyerTarget.totalTargetDep + rs.getLong("TARGET_OF_DEPOSITS"));
                    vo.setTotalRegMade(MediaBuyerTarget.totalRegMade + rs.getLong("REGMADE"));
                    vo.setTotalDepMade(MediaBuyerTarget.totalDepMade + rs.getLong("DEPMADE"));
                    list.add(vo);

                }
            } finally {
                closeResultSet(rs);
                closeStatement(ps);
            }
            return list;
        }

        public static ArrayList<MediaBuyerTarget> getAllDailyTargets(Connection con, Date monthDate) throws SQLException {
            ArrayList<MediaBuyerTarget> list = new ArrayList<MediaBuyerTarget>();
            PreparedStatement ps = null;
            ResultSet rs = null;
            try {
                String sql =
                    " SELECT " +
                    "   w.id as camp_manager, " +
                    "   mcomb.skin_id, " +
                    "   SUM(CASE WHEN to_char( u.time_created, 'yyyymmdd')=to_char( ? ,'yyyymmdd')  then 1 else 0 end) as REGMADE, " +
                    "   SUM(CASE WHEN u.first_deposit_id is not null and  to_char( t.time_created, 'yyyymmdd')=to_char( ? ,'yyyymmdd') then 1 else 0 end) as DEPMADE " +
                    " FROM" +
                    "   marketing_campaigns mc, " +
                    "   marketing_combinations mcomb, " +
                    "   writers w, " +
                    "   users u " +
                    "    left join transactions t on u.first_deposit_id = t.id " +
                    " WHERE " +
                    "   u.combination_id=mcomb.id " +
                    "   AND mc.campaign_manager=w.id " +
                    "   AND mcomb.campaign_id=mc.id " +
                    "   AND u.class_id<>0 " +
                    " GROUP BY " +
                    "   w.id, " +
                    "   mcomb.skin_id ";

                Timestamp dailyTs = CommonUtil.convertToTimeStamp(monthDate);
                ps = con.prepareStatement(sql);
                ps.setTimestamp(1, dailyTs);
                ps.setTimestamp(2, dailyTs);
                rs = ps.executeQuery();
                while ( rs.next() ) {
                    MediaBuyerTarget vo = new MediaBuyerTarget();
                    vo.setMediaBuyerId(rs.getLong("camp_manager"));
                    vo.setSkinId(rs.getLong("skin_id"));
                    vo.setSkinName(ApplicationData.getSkinById(rs.getLong("skin_id")).getDisplayName());
                    vo.setRegMade(rs.getLong("REGMADE"));
                    vo.setDepMade(rs.getLong("DEPMADE"));
                    vo.setTotalRegMade(MediaBuyerTarget.totalRegMade + rs.getLong("REGMADE"));
                    vo.setTotalDepMade(MediaBuyerTarget.totalDepMade + rs.getLong("DEPMADE"));
                    list.add(vo);
                }
            } finally {
                closeResultSet(rs);
                closeStatement(ps);
            }
            return list;
        }






        public static ArrayList<SelectItem> getMediaBuyers(Connection con) throws SQLException {
            ArrayList<SelectItem> list = new ArrayList<SelectItem>();
            PreparedStatement ps = null;
            ResultSet rs = null;
            try {
                String sql = " SELECT " +
                             "      w.* " +
                             " FROM " +
                             "      writers w " +
                             " WHERE " +
                             "      w.dept_id = ? " +
                             " ORDER BY " +
                             "      w.user_name " ;

                ps = con.prepareStatement(sql);
                ps.setLong(1, Constants.DEPARTMENT_ID_MARKETING);

                rs = ps.executeQuery();

                while (rs.next()) {
                    list.add(new SelectItem(new Long(rs.getLong("id")), rs.getString("user_name")));
                }
            } finally {
                closeResultSet(rs);
                closeStatement(ps);
            }

            return list;
        }



}
