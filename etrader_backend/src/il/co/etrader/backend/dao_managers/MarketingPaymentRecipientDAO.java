package il.co.etrader.backend.dao_managers;

import il.co.etrader.bl_vos.MarketingPaymentRecipient;
import il.co.etrader.util.CommonUtil;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.faces.model.SelectItem;

import com.anyoption.common.daos.DAOBase;



public class MarketingPaymentRecipientDAO extends DAOBase {


	  /**
	   * Get payment recipients for marketing
	   * @throws SQLException
	   */
	  public static ArrayList<SelectItem> getList(Connection con) throws SQLException {

		  PreparedStatement ps = null;
		  ResultSet rs = null;
		  ArrayList<SelectItem> list = new ArrayList<SelectItem>();

		  try {

				String sql = "select id, agent_name " +
							 "from marketing_payment_recipients " +
							 "order by upper(agent_name)";

				ps = con.prepareStatement(sql);
				rs = ps.executeQuery();

				while (rs.next()) {
					list.add(new SelectItem(new Long(rs.getLong("id")),rs.getString("agent_name")));
				}

			} finally {
					closeResultSet(rs);
					closeStatement(ps);
			}

			return list;
	  }
	  
	  public static ArrayList<SelectItem> getListWriterSkinId(Connection con, long writerIdForSkin) throws SQLException {

		  PreparedStatement ps = null;
		  ResultSet rs = null;
		  ArrayList<SelectItem> list = new ArrayList<SelectItem>();

		  try {

				String sql = "select id, agent_name " +
							 "from marketing_payment_recipients " +
							 " where writer_id in (select distinct writer_id from " +
													" writers_skin where skin_id in " +
													" (select skin_id from writers_skin where writer_id =" + writerIdForSkin + ")) " +
							 "order by upper(agent_name)";

				ps = con.prepareStatement(sql);
				rs = ps.executeQuery();

				while (rs.next()) {
					list.add(new SelectItem(new Long(rs.getLong("id")),rs.getString("agent_name")));
				}

			} finally {
					closeResultSet(rs);
					closeStatement(ps);
			}

			return list;
	  }

		/**
		 * Insert a new payment recipient
		 * @param con  db connection
		 * @param vo MarketingPaymentRecipient instance
		 * @throws SQLException
		 */
		public static void insert(Connection con,MarketingPaymentRecipient vo) throws SQLException {

			  PreparedStatement ps = null;
			  ResultSet rs = null;

			  try {

					String sql = "insert into marketing_payment_recipients(id,agent_name,writer_id,time_created) " +
							     "values(seq_mar_payment_recipients.nextval,?,?,sysdate)";

					ps = con.prepareStatement(sql);

					ps.setString(1, vo.getAgentName());
					ps.setLong(2, vo.getWriterId());

					ps.executeUpdate();

					vo.setId(getSeqCurValue(con,"seq_mar_payment_recipients"));
			  }
			  finally {
					closeStatement(ps);
					closeResultSet(rs);
				}

		  }

		  /**
		   * Update payment recipient
		   * @param con db connection
		   * @param vo  MarketingPaymentRecipient instance
		   * @throws SQLException
		   */
		  public static void update(Connection con,MarketingPaymentRecipient vo) throws SQLException {

			  PreparedStatement ps = null;
			  ResultSet rs = null;

			  try {

				  String sql = "update marketing_payment_recipients set agent_name=?,writer_id=? " +
				  			   "where id=?";

					ps = con.prepareStatement(sql);


					ps.setString(1, vo.getAgentName());
					ps.setLong(2, vo.getWriterId());
					ps.setLong(3, vo.getId());

					ps.executeUpdate();

			  }
			  finally	{
					closeStatement(ps);
					closeResultSet(rs);
				}

		  }


		  /**
		   * Get all payments recipients
		   * @param con  db connection
		   * @param name  agent name filter
		   * @return
		   *	ArrayList of MarketingPaymentRecipient
		   * @throws SQLException
		   */
		  public static ArrayList<MarketingPaymentRecipient> getAll(Connection con, String name, long writerIdForSkin) throws SQLException {

			  PreparedStatement ps = null;
			  ResultSet rs = null;
			  ArrayList<MarketingPaymentRecipient> list = new ArrayList<MarketingPaymentRecipient>();

			  try {

					String sql = "select * " +
								 "from marketing_payment_recipients where 1=1 ";

					if ( !CommonUtil.isParameterEmptyOrNull(name) ) {
						sql += " and upper(agent_name) like '%" + name.toUpperCase() + "%' ";
					}
					
					if (writerIdForSkin > 0) { 
						sql += 	" and writer_id in (select distinct writer_id from " +
													" writers_skin where skin_id in " +
														" (select skin_id from writers_skin where writer_id ="+ writerIdForSkin +")) ";
					}

					sql += "order by id";

					ps = con.prepareStatement(sql);
					rs = ps.executeQuery();

					while (rs.next()) {
						MarketingPaymentRecipient vo = getVO(rs);
						list.add(vo);
					}

				} finally {
						closeResultSet(rs);
						closeStatement(ps);
				}

				return list;
		  }


		  /**
		   * Check if agent name in use
		   * @param con   db connection
		   * @param name  agent name
		   * @return true if name in use
		   * @throws SQLException
		   */
		  public static boolean isNameInUse(Connection con, String name, long id) throws SQLException {

			  ResultSet rs = null;
			  PreparedStatement ps = null;

			  try {

				  String sql = "select * " +
				  			   "from marketing_payment_recipients " +
				  			   "where agent_name like ? ";

				  if ( id > 0 ) {  // for update
					  sql += "and id <> " + id;
				  }

				  ps = con.prepareStatement(sql);
				  ps.setString(1, name);

				  rs = ps.executeQuery();

				  if ( rs.next() ) {
					  return true;
				  }
			  } finally {
				  closeResultSet(rs);
				  closeStatement(ps);
			  }

			  return false;

		  }

		  /**
		   * Get VO
		   * @param rs
		   * 	Result set instance
		   * @return
		   * 	MarketingPaymentRecipient object
		   * @throws SQLException
		   */
		  private static MarketingPaymentRecipient getVO(ResultSet rs) throws SQLException {

			  MarketingPaymentRecipient vo = new MarketingPaymentRecipient();

				vo.setId(rs.getLong("id"));
				vo.setAgentName(rs.getString("agent_name"));
				vo.setTimeCreated(convertToDate(rs.getTimestamp("time_created")));
				vo.setWriterId(rs.getLong("writer_id"));

				return vo;
		  }



}

