package il.co.etrader.backend.dao_managers;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import javax.faces.model.SelectItem;

import com.anyoption.common.beans.Bonus;

import il.co.etrader.dao_managers.BonusDAOBase;
import il.co.etrader.util.CommonUtil;
import il.co.etrader.util.ConstantsBase;

/**
 * BonusDAOBase class.
 *
 * @author Eliran
 */
public class BonusDAO extends BonusDAOBase {

	/**
	 * get all bonuses
	 * @param con db connection
	 * @return ArrayList of all bonuses
	 * @throws SQLException
	 */
	public static ArrayList<Bonus> getAll(Connection con) throws SQLException {
		ArrayList<Bonus> list = new ArrayList<Bonus>();
		Statement ps = null;
		ResultSet rs = null;

		try	{

		    String sql = "SELECT " +
                            "b.*, " +
                            "bt.name as type_name, " +
                            "bt.class_type_id, " +
                            "bct.name as class_type_name " +
                         "FROM " +
                             "bonus b, " +
                             "bonus_types bt, " +
                             "bonus_class_types bct " +
                         "WHERE " +
                             "b.type_id = bt.id AND " +
                             "bct.id = bt.class_type_id " +
                         "ORDER BY " +
                             "time_created";

			ps = con.createStatement();
			rs = ps.executeQuery(sql);

			while ( rs.next() ) {
				Bonus vo = getVO(rs);
				vo.setTypeName(rs.getString("type_name"));
				vo.setClassType(rs.getLong("class_type_id"));
				vo.setClassTypeName(rs.getString("class_type_name"));
				list.add(vo);
			}
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		return list;
	}

	/**
	 * insert bonus
	 * @param con db connection
	 * @throws SQLException
	 */
	public static void insert(Connection con, Bonus bonus) throws SQLException {

		PreparedStatement ps = null;
		try {
			String sql="INSERT INTO " +
						  "bonus" +
						  		"(id, " +
                                "start_date, " +
                                "end_date, " +
                                "wagering_parameter, " +
								"default_period, " +
								"automatic_renew_ind, " +
								"renew_frequency, " +
								"type_id, " +
								"writer_id, " +
								"time_created, " +
								"number_of_actions, " +
								"name, " +
								"odds_win, " +
								"odds_lose) " +
						"VALUES" +
                                "(SEQ_BONUS.nextval,?,?,?,?,?,?,?,?,sysdate,?,?,?,?)";

			ps = con.prepareStatement(sql);

			ps.setTimestamp(1, CommonUtil.convertToTimeStamp(bonus.getStartDate()));
			ps.setTimestamp(2, CommonUtil.convertToTimeStamp(bonus.getEndDate()));
			ps.setLong(3, bonus.getWageringParameter());
			ps.setLong(4, bonus.getDefaultPeriod());
			ps.setLong(5, bonus.getAutoRenewInd());
			ps.setLong(6, bonus.getRenewFrequency());
			ps.setLong(7, bonus.getTypeId());
			ps.setLong(8, bonus.getWriterId());
			ps.setLong(9, bonus.getNumberOfActions());
            ps.setString(10, bonus.getName());
            ps.setDouble(11, bonus.getOddsWin());
            ps.setDouble(12, bonus.getOddsLose());

			ps.executeUpdate();

		} finally {
			closeStatement(ps);
		}
	}

    /**
     * insert bonus
     * @param con db connection
     * @throws SQLException
     */
    public static void update(Connection con, Bonus bonus) throws SQLException {
        PreparedStatement ps = null;
        try {
            String sql=	"UPDATE " +
            				"bonus " +
            			"SET " +
	                        "start_date = ?, end_date = ?, wagering_parameter = ?, " +
	                        "default_period = ?, automatic_renew_ind = ?, renew_frequency = ?, type_id = ?, " +
	                        "writer_id = ?, number_of_actions  = ?, name = ?, odds_win = ?, odds_lose = ? " +
                        "WHERE " +
                            "id = ?";

            ps = con.prepareStatement(sql);
            ps.setTimestamp(1, CommonUtil.convertToTimeStamp(bonus.getStartDate()));
            ps.setTimestamp(2, CommonUtil.convertToTimeStamp(bonus.getEndDate()));
            ps.setLong(3, bonus.getWageringParameter());
            ps.setLong(4, bonus.getDefaultPeriod());
            ps.setLong(5, bonus.getAutoRenewInd());
            ps.setLong(6, bonus.getRenewFrequency());
            ps.setLong(7, bonus.getTypeId());
            ps.setLong(8, bonus.getWriterId());
            ps.setLong(9, bonus.getNumberOfActions());
            ps.setString(10, bonus.getName());
            ps.setDouble(11, bonus.getOddsWin());
            ps.setDouble(12, bonus.getOddsLose());
            ps.setLong(13, bonus.getId());

            ps.executeUpdate();

        } finally {
            closeStatement(ps);
        }
    }

    /**
     * Get All Bonus Types into a ArrayList<SelectItem>
     * @param con connection to DB
     * @return Bonus Types
     * @throws SQLException
     */
    public static ArrayList<SelectItem> getAllBonusTypes(Connection con) throws SQLException {
        PreparedStatement ps = null;
        ResultSet rs = null;
        ArrayList<SelectItem> hm = new ArrayList<SelectItem>();
        try
        {
            String sql = "select * from bonus_types order by upper(name)";
            ps = con.prepareStatement(sql);
            rs = ps.executeQuery();
            while ( rs.next() ) {
                hm.add(new SelectItem(rs.getLong("id"), rs.getString("name")));
            }
        } finally {
            closeStatement(ps);
            closeResultSet(rs);
        }
        return hm;
    }

    /**
     * Get All Bonus Types into a ArrayList<SelectItem> by writer groups and population type id
     * @param con connection to DB
     * @return Bonus Types
     * @throws SQLException
     */
    public static ArrayList<SelectItem> getBonusByWriterGroupAPopulation(Connection con, long writerGroupId,
    		long populationTypeId, String skins, long skinId) throws SQLException {

        PreparedStatement ps = null;
        ResultSet rs = null;
        ArrayList<SelectItem> hm = new ArrayList<SelectItem>();
        try {
            String sql = "SELECT b.id, b.name " +
            			 "FROM " +
            			 	"bonus b, " +
            			 	"bonus_writer_groups bwg, " +
            			 	"writer_groups wg ";

            			 if (populationTypeId > 0) {
            				 sql += ",bonus_populations bp ";
            			 }

            		sql += "WHERE " +
	            			 	"b.id =  bwg.bonus_id AND " +
	            			 	"wg.id = bwg.writer_group_id AND " +
	            			 	"wg.id = ? AND " +
	            			 	"EXISTS (SELECT 1 " +
                                        "FROM bonus_skins bs " +
                                        "WHERE  bs.bonus_id = b.id AND " +
                                                "bs.skin_id IN (" + skins + ") ) AND " +
                                skinId + " IN (SELECT bs.skin_id " +
		                                 	   "FROM bonus_skins bs " +
		                                 	   "WHERE bs.bonus_id = b.id " +
		                                     " ) AND " +
		                         "b.type_id <> ? AND " +
		                         "sysdate >= b.start_date AND " +
		                         "sysdate <= b.end_date ";

            if (populationTypeId > 0) {
            	sql += "AND b.id = bp.bonus_id AND bp.population_type_id = " + populationTypeId;
            }

            ps = con.prepareStatement(sql);
            ps.setLong(1, writerGroupId);
            ps.setLong(2, ConstantsBase.BONUS_TYPE_CONVERT_POINTS_TO_CASH);
            rs = ps.executeQuery();
            while ( rs.next() ) {
                hm.add(new SelectItem(rs.getLong("id"), rs.getString("name")));
            }
        } finally {
            closeStatement(ps);
            closeResultSet(rs);
        }
        return hm;
    }

    /**
     * Get All Bonus states into a ArrayList<SelectItem>
     * @param con connection to DB
     * @return Bonus states
     * @throws SQLException
     */
    public static ArrayList<SelectItem> getAllBonusStates(Connection con) throws SQLException {
        PreparedStatement ps = null;
        ResultSet rs = null;
        ArrayList<SelectItem> hm = new ArrayList<SelectItem>();
        try
        {
            String sql = "select * from bonus_states order by name";
            ps = con.prepareStatement(sql);
            rs = ps.executeQuery();
            while ( rs.next() ) {
                hm.add(new SelectItem(rs.getLong("id"), rs.getString("name")));
            }
        } finally {
            closeStatement(ps);
            closeResultSet(rs);
        }
        return hm;
    }

    /**
     * Get All user actions into a ArrayList<SelectItem>
     * @param con connection to DB
     * @return user actions
     * @throws SQLException
     */
    public static ArrayList<SelectItem> getAllUserActions(Connection con) throws SQLException {
        PreparedStatement ps = null;
        ResultSet rs = null;
        ArrayList<SelectItem> hm = new ArrayList<SelectItem>();
        try
        {
            String sql = "select * from user_actions";
            ps = con.prepareStatement(sql);
            rs = ps.executeQuery();
            while ( rs.next() ) {
                hm.add(new SelectItem(rs.getLong("id"), rs.getString("name")));
            }
        } finally {
            closeStatement(ps);
            closeResultSet(rs);
        }
        return hm;
    }

	public static ArrayList<SelectItem> getBonusBySkin(Connection con, long skinId) throws SQLException {

        PreparedStatement ps = null;
        ResultSet rs = null;
        ArrayList<SelectItem> hm = new ArrayList<SelectItem>();
        try {

            String sql = "SELECT b.id, b.name " +
            			 "FROM " +
            			 	"bonus b, " +
            			 	"bonus_skins bs ";
            		sql += "WHERE " +
	            			 	"bs.bonus_id = b.id AND bs.skin_id=?";

            ps = con.prepareStatement(sql);
            ps.setLong(1, skinId);
            rs = ps.executeQuery();
            while ( rs.next() ) {
                hm.add(new SelectItem(rs.getLong("id"), rs.getString("name")));
            }
        } finally {
            closeStatement(ps);
            closeResultSet(rs);
        }
        return hm;
    }

    public static void updateBonusWagering(Connection conn, long bonusUsersId, long wageringParam) throws SQLException {
        PreparedStatement pstmt = null;
        try{
            String sql =
                    "UPDATE " +
                        "bonus_users " +
                    "SET " +
                        "wagering_parameter = ?, " +
                        "time_updated = current_date " +
                    "WHERE " +
                        "id = ?";
            pstmt = conn.prepareStatement(sql);
            pstmt.setLong(1, wageringParam);
            pstmt.setLong(2, bonusUsersId);
            pstmt.executeUpdate();
        } finally {
            closeStatement(pstmt);
        }
    }
}
