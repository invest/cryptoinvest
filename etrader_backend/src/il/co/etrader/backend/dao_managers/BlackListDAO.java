package il.co.etrader.backend.dao_managers;


import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import org.bouncycastle.crypto.CryptoException;

import com.anyoption.common.daos.DAOBase;
import com.anyoption.common.util.AESUtil;

import il.co.etrader.backend.bl_vos.BlackList;
import il.co.etrader.backend.util.Utils;
import il.co.etrader.util.CommonUtil;
import il.co.etrader.util.ConstantsBase;


/**
 * BonusDAOBase class.
 *
 * @author Eliran
 */
public class BlackListDAO extends DAOBase {

	/**
	 * get  black list
	 * @param con db connection
	 * @return ArrayList black list
	 * @throws SQLException
	 */
	public static ArrayList<BlackList> getBlackList(Connection con,
													String userIdStr,
													String userName,
													String userFirstName,
													String userLastName,
													String phone,
													String email,
													String password,
													String ip,
													long cityId,
													long publisherId,
													long actionTypeId) throws SQLException {


		ArrayList<BlackList> list = new ArrayList<BlackList>();
		Statement ps = null;
		ResultSet rs = null;

		try	{

		    String sql = " Select " +
		    				" u.id user_id, " +
		    				" u.user_name," +
		    				" mca.name publisher," +
		    				" iat.name action_type_name," +
		    				" ia.action_time," +
		    				" ia.action_time_offset " +
						 " From " +
						 		" marketing_combinations mco, " +
						 		" marketing_campaigns mca ," +
						 		" users u " +
							 		" left join (select " +
							 						" i.user_id, " +
							 						" max(ia.id) action_id " +
							                    " from " +
							                    	" issue_actions ia, " +
							                    	" issues i, " +
							                    	" issue_action_types iat, " +
							                    	" issue_action_roles iar "  +
							                    " where " +
							                    	" i.id = ia.issue_id " +
							                        " and ia.issue_action_type_id = iat.id " +
							                        " and iar.issue_action_type_id = iat.id " +
							                        " and iar.screen_id = " + ConstantsBase.SCREEN_ACCOUNTING + " " +
							                    " group by i.user_id) close_acc on u.id = close_acc.user_id " +
							               " left join issue_actions ia on close_acc.action_id = ia.id " +
							               		" left join issue_action_types iat on ia.issue_action_type_id = iat.id " +
						" Where " +
							" u.is_active = 0 " +
							" and u.class_id > 0 " +
							" and u.combination_id = mco.id " +
							" and mco.campaign_id = mca.id ";

		    if (!CommonUtil.isParameterEmptyOrNull(userIdStr)){
		    	sql +=      " and u.id = " + userIdStr + " ";
		    }

		    if (!CommonUtil.isParameterEmptyOrNull(userName)){
		    	sql +=      " and u.user_name = '" + userName.toUpperCase() + "' ";
		    }

		    if (!CommonUtil.isParameterEmptyOrNull(userFirstName)){
		    	sql +=      " and upper(u.first_name) = '" + userFirstName.toUpperCase() + "' ";
		    }

		    if (!CommonUtil.isParameterEmptyOrNull(userLastName)){
		    	sql +=      " and upper(u.last_name) = '" + userLastName.toUpperCase() + "' ";
		    }

		    if (!CommonUtil.isParameterEmptyOrNull(phone)){
		    	sql +=      " and (u.LAND_LINE_PHONE = '" + phone + "' or u.MOBILE_PHONE = '" + phone + "') ";
		    }

		    if (!CommonUtil.isParameterEmptyOrNull(email)){
		    	sql +=      " and upper(u.email) = '" + email.toUpperCase() + "' ";
		    }

		    if (!CommonUtil.isParameterEmptyOrNull(password)){
				try {
					password = AESUtil.encrypt(password);
				} catch (	CryptoException | InvalidKeyException | NoSuchAlgorithmException | NoSuchPaddingException
							| IllegalBlockSizeException | BadPaddingException | UnsupportedEncodingException
							| InvalidAlgorithmParameterException ce) {
					throw new SQLException("Unable to encrypt password: " + ce.getMessage());
				}
		    	sql +=      " and u.password = '" + password + "' ";
		    }

		    if (!CommonUtil.isParameterEmptyOrNull(ip)){
		    	sql +=      " and upper(u.ip) = '" + ip.toUpperCase() + "' ";
		    }

		    if (cityId > 0){
		    	sql +=      " and u.city_id = " + cityId + " ";
		    }

		    if (publisherId > 0){
		    	sql +=      " and mca.id = " + publisherId + " ";
		    }

		    if (actionTypeId > 0){
		    	sql +=      " and iat.id = " + actionTypeId + " ";
		    }

		    sql +=
						" Order by " +
							" ia.action_time desc nulls last, u.time_created desc";

			ps = con.createStatement();
			rs = ps.executeQuery(sql);

			while ( rs.next() ) {
				BlackList vo = new BlackList();
				vo.setUserId(rs.getLong("user_id"));
				vo.setUserName(rs.getString("user_name"));
				vo.setPublisher(rs.getString("publisher"));
				vo.setActionTime(convertToDate(rs.getTimestamp("action_time")));
				vo.setActionTimeOffset(rs.getString("action_time_offset"));
				vo.setActionTypeName(CommonUtil.getMessage(rs.getString("action_type_name"),null, Utils.getWriter().getLocalInfo().getLocale()));

				list.add(vo);
			}
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		return list;
	}

}
