package il.co.etrader.backend.dao_managers;

import il.co.etrader.bl_vos.MarketingLocation;
import il.co.etrader.util.CommonUtil;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.anyoption.common.daos.DAOBase;


public class MarketingLocationsDAO extends DAOBase {


	/**
	 * Insert a new location
	 * @param con  db connection
	 * @param vo MarketingLocation instance
	 * @throws SQLException
	 */
	public static void insert(Connection con,MarketingLocation vo) throws SQLException {

		  PreparedStatement ps = null;
		  ResultSet rs = null;

		  try {

				String sql = "insert into marketing_locations(id,location,writer_id,time_created) " +
						     "values(seq_mar_locations.nextval,?,?,sysdate)";

				ps = con.prepareStatement(sql);

				ps.setString(1, vo.getLocation());
				ps.setLong(2, vo.getWriterId());

				ps.executeUpdate();

				vo.setId(getSeqCurValue(con,"seq_mar_locations"));
		  }
		  finally {
				closeStatement(ps);
				closeResultSet(rs);
			}

	  }

	  /**
	   * Update marketing location
	   * @param con db connection
	   * @param vo  MarketingLocation instance
	   * @throws SQLException
	   */
	  public static void update(Connection con,MarketingLocation vo) throws SQLException {

		  PreparedStatement ps = null;
		  ResultSet rs = null;

		  try {

			  String sql = "update marketing_locations set location=?,writer_id=? " +
			  			   "where id=?";

				ps = con.prepareStatement(sql);


				ps.setString(1, vo.getLocation());
				ps.setLong(2, vo.getWriterId());
				ps.setLong(3, vo.getId());

				ps.executeUpdate();

		  }
		  finally	{
				closeStatement(ps);
				closeResultSet(rs);
			}

	  }


	  /**
	   * Get all locations
	   * @param con  db connection
	   * @param location  filter
	   * @return
	   *	ArrayList of MarketingLocation
	   * @throws SQLException
	   */
	  public static ArrayList<MarketingLocation> getAll(Connection con, String location, long writerIdForSkin) throws SQLException {

		  PreparedStatement ps = null;
		  ResultSet rs = null;
		  ArrayList<MarketingLocation> list = new ArrayList<MarketingLocation>();

		  try {

				String sql = "select * " +
							 "from marketing_locations where 1=1 ";

				if ( !CommonUtil.isParameterEmptyOrNull(location) ) {
					sql += " and upper(location) like '%" + location.toUpperCase() + "%' ";
				}
				
				if (writerIdForSkin > 0) { 
					sql += 	" and writer_id in (select distinct writer_id from " +
												" writers_skin where skin_id in " +
													" (select skin_id from writers_skin where writer_id ="+ writerIdForSkin +")) ";
				}

				sql += "order by upper(location)";

				ps = con.prepareStatement(sql);
				rs = ps.executeQuery();

				while (rs.next()) {
					MarketingLocation vo = getVO(rs);
					list.add(vo);
				}

			} finally {
					closeResultSet(rs);
					closeStatement(ps);
			}

			return list;
	  }


	  /**
	   * Check if location in use
	   * @param con   db connection
	   * @param name  medium name
	   * @return true if name in use
	   * @throws SQLException
	   */
	  public static boolean isNameInUse(Connection con, String location, long id) throws SQLException {

		  ResultSet rs = null;
		  PreparedStatement ps = null;

		  try {

			  String sql = "select * " +
			  			   "from marketing_locations " +
			  			   "where location like ? ";

			  if ( id > 0 ) {  // for update
				  sql += "and id <> " + id;
			  }

			  ps = con.prepareStatement(sql);
			  ps.setString(1, location);

			  rs = ps.executeQuery();

			  if ( rs.next() ) {
				  return true;
			  }
		  } finally {
			  closeResultSet(rs);
			  closeStatement(ps);
		  }

		  return false;

	  }

	  /**
	   * Get VO
	   * @param rs
	   * 	Result set instance
	   * @return
	   * 	MarketingLocation object
	   * @throws SQLException
	   */
	  private static MarketingLocation getVO(ResultSet rs) throws SQLException {

		  MarketingLocation vo = new MarketingLocation();

			vo.setId(rs.getLong("id"));
			vo.setLocation(rs.getString("location"));
			vo.setTimeCreated(convertToDate(rs.getTimestamp("time_created")));
			vo.setWriterId(rs.getLong("writer_id"));

			return vo;
	  }

}

