package il.co.etrader.backend.dao_managers;

import il.co.etrader.backend.util.Utils;
import il.co.etrader.bl_vos.MarketingSource;
import il.co.etrader.util.CommonUtil;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.anyoption.common.daos.DAOBase;

public class MarketingSourcesDAO extends DAOBase {


	/**
	 * Insert a new source
	 * @param con  db connection
	 * @param vo MarketingSources instance
	 * @throws SQLException
	 */
	public static void insert(Connection con,MarketingSource vo) throws SQLException {

		  PreparedStatement ps = null;
		  ResultSet rs = null;

		  try {

				String sql = "insert into marketing_sources(id,name,writer_id,time_created) " +
						     "values(seq_mar_sources.nextval,?,?,sysdate)";

				ps = con.prepareStatement(sql);

				ps.setString(1, vo.getName());
				ps.setLong(2, vo.getWriterId());

				ps.executeUpdate();

				vo.setId(getSeqCurValue(con,"seq_mar_sources"));
		  }
		  finally {
				closeStatement(ps);
				closeResultSet(rs);
			}

	  }

	  /**
	   * Update marketing source
	   * @param con db connection
	   * @param vo  MarketingSources instance
	   * @throws SQLException
	   */
	  public static void update(Connection con,MarketingSource vo) throws SQLException {

		  PreparedStatement ps = null;
		  ResultSet rs = null;

		  try {

			  String sql = "update marketing_sources set name=?,writer_id=? " +
			  			   "where id=?";

				ps = con.prepareStatement(sql);


				ps.setString(1, vo.getName());
				ps.setLong(2, vo.getWriterId());
				ps.setLong(3, vo.getId());

				ps.executeUpdate();

		  }
		  finally	{
				closeStatement(ps);
				closeResultSet(rs);
			}

	  }


	  /**
	   * Get all sources
	   * @param con  db connection
	   * @param sourceName  source name filter
	   * @return
	   *	ArrayList of MarketingSources
	   * @throws SQLException
	   */
	  public static ArrayList<MarketingSource> getAll(Connection con, String sourceName, boolean orderById, long writerIdForSkin) throws SQLException {

		  PreparedStatement ps = null;
		  ResultSet rs = null;
		  ArrayList<MarketingSource> list = new ArrayList<MarketingSource>();

		  try {

				String sql = "select * " +
							 "from marketing_sources where 1=1 ";

				if ( !CommonUtil.isParameterEmptyOrNull(sourceName) ) {
					sql += " and upper(name) like '%" + sourceName.toUpperCase() + "%' ";
				}
				
				if (writerIdForSkin > 0) { 
					sql += 	" and writer_id in (select distinct writer_id from " +
												" writers_skin where skin_id in " +
													" (select skin_id from writers_skin where writer_id ="+ writerIdForSkin +")) ";
				}

			   if(orderById){
				   sql += "order by id";
			   }
			   else{
				   sql += "order by upper(name)";
			   }

				ps = con.prepareStatement(sql);
				rs = ps.executeQuery();

				while (rs.next()) {
					MarketingSource vo = getVO(rs);
					list.add(vo);
				}

			} finally {
					closeResultSet(rs);
					closeStatement(ps);
			}

			return list;
	  }

	  /**
	   * Get all sources
	   * @param con  db connection
	   * @return ArrayList of MarketingSources for this writer that he can see
	   *
	   * @throws SQLException
	   */
	  public static ArrayList<MarketingSource> getMarketingSourcesByWriterSkinId(Connection con) throws SQLException {

		  PreparedStatement ps = null;
		  ResultSet rs = null;
		  ArrayList<MarketingSource> list = new ArrayList<MarketingSource>();

		  try {
			  	String skins = Utils.getWriter().getSkinsToString();

				String sql = "select " +
							 	"ms.* " +
							 "from " +
							 	"marketing_sources ms, " +
							 	"marketing_campaigns mcamp, " +
							 	"marketing_combinations mcom " +
							"where " +
								"ms.id = mcamp.source_id and " +
								"mcamp.id = mcom.campaign_id and " +
								"mcom.skin_id in (" + skins + ") " +
							"GROUP BY " +
								"ms.id, ms.name, ms.time_created, ms.writer_id, ms.is_keep_daily_data, ms.is_special_mail " +
							"ORDER BY " +
								"ms.name ";

				ps = con.prepareStatement(sql);
				rs = ps.executeQuery();

				while (rs.next()) {
					MarketingSource vo = getVO(rs);
					list.add(vo);
				}

			} finally {
					closeResultSet(rs);
					closeStatement(ps);
			}

			return list;
	  }


	  /**
	   * Get source by id
	   * @param con db connection
	   * @param id source id
	   * @return
	   * @throws SQLException
	   */
	  public static MarketingSource getById(Connection con, long id) throws SQLException {

		  PreparedStatement ps = null;
		  ResultSet rs = null;
		  MarketingSource source = null;

		  try {

				String sql = " select * " +
							 " from marketing_sources " +
							 " where id = ? ";

				ps = con.prepareStatement(sql);
				ps.setLong(1, id);

				rs = ps.executeQuery();

				if (rs.next()) {
					source = new MarketingSource();
					source.setName(rs.getString("name"));
					source.setId(id);
					source.setKeepDailyData(rs.getInt("is_keep_daily_data")==1?true:false);
				}

			} finally {
					closeResultSet(rs);
					closeStatement(ps);
			}

			return source;
	  }


	  /**
	   * Check if source name in use
	   * @param con   db connection
	   * @param name  source name
	   * @return true if name in use
	   * @throws SQLException
	   */
	  public static boolean isNameInUse(Connection con, String name, long id) throws SQLException {

		  ResultSet rs = null;
		  PreparedStatement ps = null;

		  try {

			  String sql = "select * " +
			  			   "from marketing_sources " +
			  			   "where name like ? ";

			  if ( id > 0 ) {  // for update
				  sql += "and id <> " + id;
			  }

			  ps = con.prepareStatement(sql);
			  ps.setString(1, name);

			  rs = ps.executeQuery();

			  if ( rs.next() ) {
				  return true;
			  }
		  } finally {
			  closeResultSet(rs);
			  closeStatement(ps);
		  }

		  return false;

	  }

	  /**
	   * Get VO
	   * @param rs
	   * 	Result set instance
	   * @return
	   * 	MarketingSources object
	   * @throws SQLException
	   */
	  private static MarketingSource getVO(ResultSet rs) throws SQLException {

		  MarketingSource vo = new MarketingSource();

			vo.setId(rs.getLong("id"));
			vo.setName(rs.getString("name"));
			vo.setTimeCreated(convertToDate(rs.getTimestamp("time_created")));
			vo.setWriterId(rs.getLong("writer_id"));
			vo.setSpecialMail(rs.getLong("is_special_mail") ==  0 ? false : true);

			return vo;
	  }

}

