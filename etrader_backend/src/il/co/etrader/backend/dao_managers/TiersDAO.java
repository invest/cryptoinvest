package il.co.etrader.backend.dao_managers;

import il.co.etrader.bl_vos.TierUserHistory;
import il.co.etrader.dao_managers.TiersDAOBase;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;




/**
 * TiersDAO class.
 *
 * @author Kobi
 */
public class TiersDAO extends TiersDAOBase {

	/**
	 * Get loyalty tier points history of the user
	 * @param con
	 * @param userId the points history of the user
	 * @return
	 * @throws SQLException
	 */
	public static ArrayList<TierUserHistory> getTierUserHistory(Connection con, long userId) throws SQLException {

		PreparedStatement ps = null;
		ResultSet rs = null;
		ArrayList<TierUserHistory> list = new ArrayList<TierUserHistory>();

		try {
			String sql =
				"SELECT " +
					"tuh.*, " +
					"w.user_name writer, " +
					"tha.description action_des, " +
					"t.description tier_des " +
				"FROM " +
					"tier_users_history tuh, " +
					"writers w, " +
					"tier_his_actions tha, " +
					"tiers t " +
				"WHERE " +
					"tuh.user_id = ? AND " +
					"tuh.tier_id = t.id AND " +
					"tuh.writer_id = w.id AND " +
					"tuh.tier_his_action = tha.id " +

				"ORDER BY " +
					"tuh.time_created desc, tuh.id desc";

			ps = con.prepareStatement(sql);
			ps.setLong(1, userId);
			rs = ps.executeQuery();

			while(rs.next()) {
				TierUserHistory h = new TierUserHistory();
				h.setId(rs.getLong("id"));
				h.setUserId(userId);
				h.setTierId(rs.getLong("tier_id"));
				h.setWriterId(rs.getLong("writer_id"));
				h.setPoints(rs.getLong("points"));
				h.setActionPoints(rs.getLong("action_points"));
				h.setKeyValue(rs.getLong("key_value"));
				h.setTableName(rs.getString("table_name"));
				h.setTierHistoryActionId(rs.getLong("tier_his_action"));
				h.setTierHisAction(rs.getString("action_des"));
				h.setTierName(rs.getString("tier_des"));
				h.setWriter(rs.getString("writer"));
				h.setTimeCreated(convertToDate(rs.getTimestamp("time_created")));
				h.setUtcOffset(rs.getString("utc_offset"));
				list.add(h);
			}
		}
		finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		return list;
	}

	/**
	 * Update userTier with SP
	 * @param conn db connection
	 * @param userId user for update his tier
	 * @param tierId the new tier
	 * @param writerId the writer that perform the action
	 * @throws SQLException
	 */
	public static void updateUserTier(Connection conn, long userId, long tierId, long writerId, long qualifDifPoints) throws SQLException {
		CallableStatement cstmt = null;
        try {
            cstmt = conn.prepareCall("BEGIN LOYALTY_UPDATE_USERS_TIER(?,?,?,?); END;");
            cstmt.setLong(1, userId);
            cstmt.setLong(2, tierId);
            cstmt.setLong(3, writerId);
            cstmt.setLong(4, qualifDifPoints);
            cstmt.execute();
        } finally {
            closeStatement(cstmt);
        }
	}


}
