package il.co.etrader.backend.dao_managers;

import il.co.etrader.backend.bl_vos.MarketingCombinationSeris;
import il.co.etrader.bl_vos.MarketingCombination;
import il.co.etrader.dao_managers.MarketingCombinationsDAOBase;
import il.co.etrader.util.CommonUtil;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.faces.model.SelectItem;

public class MarketingCombinationsDAO extends MarketingCombinationsDAOBase {


	/**
	 * Insert a new combination
	 * @param con  db connection
	 * @param vo MarketingCombination instance
	 * @throws SQLException
	 */
	public static long insert(Connection con,MarketingCombination vo) throws SQLException {

		  PreparedStatement ps = null;
		  ResultSet rs = null;

		  try {

				String sql = "insert into marketing_combinations(id,skin_id,campaign_id,medium_id,content_id,size_id," +
							 "type_id,location_id,landing_page_id,writer_id,time_created,marketing_url_source_type_id,dynamic_param) " +
						     "values(seq_mar_combinations.nextval,?,?,?,?,?,?,?,?,?,sysdate,?,?)";

				ps = con.prepareStatement(sql);

				ps.setLong(1, vo.getSkinId());
				ps.setLong(2, vo.getCampaignId());
				ps.setLong(3, vo.getMediumId());
				ps.setLong(4, vo.getContentId());
				if ( vo.getSizeId() == 0 ) {   // not required fields
					ps.setString(5, null);
				} else {
					ps.setLong(5, vo.getSizeId());
				}
				if ( vo.getTypeId() == 0 ) {
					ps.setString(6, null);
				} else {
					ps.setLong(6, vo.getTypeId());
				}
				if ( vo.getLocationId() == 0 ) {
					ps.setString(7, null);
				} else {
					ps.setLong(7, vo.getLocationId());
				}
				ps.setLong(8, vo.getLandingPageId());
				ps.setLong(9, vo.getWriterId());
				ps.setLong(10, vo.getUrlSourceTypeId());
				ps.setString(11, vo.getDynamicParam());
				ps.executeUpdate();

				vo.setId(getSeqCurValue(con,"seq_mar_combinations"));
				return vo.getId();
		  }
		  finally {
				closeStatement(ps);
				closeResultSet(rs);
			}

	  }
	
	 public static boolean isUpdateCampaign(Connection con, Long combinationId, Long campaignId) throws SQLException {
		  
		  PreparedStatement ps = null;
		  ResultSet rs = null;

		  try {

			  String sql = "select id, campaign_id from  marketing_combinations " +
			  			   "where id=? and campaign_id=?";

				ps = con.prepareStatement(sql);

				ps.setLong(1, combinationId);
				ps.setLong(2, campaignId);

				rs = ps.executeQuery();
				
				if (rs.next()) {
					return false;
				}
				
		  }
		  finally	{
				closeStatement(ps);
				closeResultSet(rs);
			}
		  return true;
	  }

	  /**
	   * Update marketing combination
	   * @param con db connection
	   * @param vo  MarketingCombination instance
	   * @throws SQLException
	   */
	  public static void update(Connection con,MarketingCombination vo) throws SQLException {

		  PreparedStatement ps = null;
		  ResultSet rs = null;

		  try {

			  String sql = "update marketing_combinations set skin_id=?,campaign_id=?," +
			  			   "medium_id=?,content_id=?,size_id=?,type_id=?,location_id=?," +
			  			   "landing_page_id=?,writer_id=?, marketing_url_source_type_id=?, dynamic_param=? " +
			  			   "where id=?";

				ps = con.prepareStatement(sql);

				ps.setLong(1, vo.getSkinId());
				ps.setLong(2, vo.getCampaignId());
				ps.setLong(3, vo.getMediumId());
				ps.setLong(4, vo.getContentId());
				if ( vo.getSizeId() == 0 ) { // not required fields
					ps.setString(5, null);
				} else {
					ps.setLong(5, vo.getSizeId());
				}
				if ( vo.getTypeId() == 0 ) {
					ps.setString(6, null);
				} else {
					ps.setLong(6, vo.getTypeId());
				}
				if ( vo.getLocationId() == 0 ) {
					ps.setString(7, null);
				} else {
					ps.setLong(7, vo.getLocationId());
				}
				ps.setLong(8, vo.getLandingPageId());
				ps.setLong(9, vo.getWriterId());
				ps.setLong(10, vo.getUrlSourceTypeId());
				ps.setString(11, vo.getDynamicParam());
				ps.setLong(12, vo.getId());

				ps.executeUpdate();

		  }
		  finally	{
				closeStatement(ps);
				closeResultSet(rs);
			}

	  }


	  /**
	   * Get all combinations
	   * @param con  db connection
	   * @param skinId  skin id filter
	   * @param campaign  campaign name filter
	   * @param source source name filter
	   * @param medium medium id filter
	   * @param content content id filter
	   * @param verticalSize vertical size filter
	   * @param horizontalSize horizontal size filter
	   * @param type  type name filter
	   * @param location location filter
	   * @param landingPage landing page id filter
	   * @return
	   * @throws SQLException
	   */
	  public static ArrayList<MarketingCombination> getAll(Connection con, long skinId, String campaign, String source,
			  			long medium, long content, String verticalSize, String horizontalSize,
			  					String type, String location, long landingPage, long combId, long writerIdForSkin, long fetchCount) throws SQLException {

		  PreparedStatement ps = null;
		  ResultSet rs = null;
		  ArrayList<MarketingCombination> list = new ArrayList<MarketingCombination>();

		  try {

				String sql = "select co.*, s.name skin_name, ca.name campaign_name, so.name source_name, me.name medium_name, " +
							 		"ct.name content_name, si.size_vertical, si.size_horizontal, ty.name type_name, lo.location, " +
							 		"lp.name landing_page_name, lp.url, lp.landing_page_type, " +
							 		" md.domain " +							 		
							 "from marketing_combinations co, skins s, marketing_campaigns ca, marketing_sources so, " +
						 	 	  "marketing_mediums me, marketing_contents ct, marketing_sizes si, marketing_types ty," +
						 	 	  "marketing_locations lo, marketing_landing_pages lp, " +
						 	 	  " marketing_combination_domains mcd, " +
						 	 	  " marketing_domains md " +
						 	 "where co.skin_id = s.id " +
						 	 	   "and co.campaign_id = ca.id " +
						 	 	   "and ca.source_id = so.id " +
						 	 	   "and co.medium_id = me.id " +
						 	 	   "and co.content_id = ct.id " +
						 	 	   "and co.size_id = si.id(+) " +
						 	 	   "and co.type_id = ty.id(+) " +
						 	 	   "and co.location_id = lo.id(+) " +
						 	 	   "and co.landing_page_id = lp.id " +
						 	 	   " AND co.id = mcd.combination_id " +
						 	 	   " AND mcd.domain_id = md.id ";

				if ( skinId > 0 ) {
					sql += "and co.skin_id = " + skinId + " ";
				}

				if ( !CommonUtil.isParameterEmptyOrNull(campaign)) {
					sql += "and upper(ca.name) like '%" + campaign.toUpperCase() + "%' ";
				}

				if ( !CommonUtil.isParameterEmptyOrNull(source)) {
					sql += "and upper(so.name) like '%" + source.toUpperCase() + "%' ";
				}

				if ( medium > 0 ) {
					sql += "and co.medium_id = " + medium + " ";
				}

				if ( content > 0 ) {
					sql += "and co.content_id = " + content + " ";
				}

				if ( !CommonUtil.isParameterEmptyOrNull(verticalSize)) {
					sql += "and si.size_vertical = " + verticalSize + " ";
				}

				if ( !CommonUtil.isParameterEmptyOrNull(horizontalSize)) {
					sql += "and si.size_horizontal = " + horizontalSize + " ";
				}

				if ( !CommonUtil.isParameterEmptyOrNull(type)) {
					sql += "and upper(ty.name) like '%" + type.toUpperCase() + "%' ";
				}

				if ( !CommonUtil.isParameterEmptyOrNull(location)) {
					sql += "and upper(lo.location) like '%" + location.toUpperCase() + "%' ";
				}

				if ( landingPage > 0 ) {
					sql += "and co.landing_page_id = " + landingPage + " ";
				}

				if ( combId > 0 ) {
					sql += "and co.id = " + combId + " ";
				}
				
				if (writerIdForSkin > 0) { 
					sql += 	" and co.skin_id in (select ws.skin_id from writers_skin ws where ws.writer_id ="+ writerIdForSkin +" ) ";
				}

				sql += "order by co.id desc";
				if (fetchCount > 0){
				    sql = rowLimit(sql, fetchCount);   
				}
				ps = con.prepareStatement(sql);
				rs = ps.executeQuery();

				while (rs.next()) {
					MarketingCombination vo = getVO(rs);
					vo.setSkinName(rs.getString("skin_name"));
					vo.setCampaignName(rs.getString("campaign_name"));
					vo.setSourceName(rs.getString("source_name"));
					vo.setMediumName(rs.getString("medium_name"));
					vo.setContentName(rs.getString("content_name"));
					vo.setTypeName(rs.getString("type_name"));
					vo.setLocation(rs.getString("location"));
					vo.setLandingPageName(rs.getString("landing_page_name"));
					vo.setLandingPageUrl(rs.getString("url"));
					vo.setVerticalSize(rs.getString("size_vertical"));
					vo.setHorizontalSizel(rs.getString("size_horizontal"));
					vo.setPixelsSi(MarketingPixelsDAO.getPixelsByCombIdSI(con, vo.getId()));
					vo.setLandingPageType(rs.getInt("landing_page_type"));
					vo.getMarketingDomain().setDomain(rs.getString("domain"));
					list.add(vo);
				}

			} finally {
					closeResultSet(rs);
					closeStatement(ps);
			}

			return list;
	  }
	  
	public static ArrayList<MarketingCombinationSeris> getAllSerries(Connection con, List<Long> mcId) throws SQLException {

		PreparedStatement ps = null;
		ResultSet rs = null;
		ArrayList<MarketingCombinationSeris> list = new ArrayList<MarketingCombinationSeris>();
		String mcIds = CommonUtil.getArrayToString(mcId);
		try {

			String sql = "select co.*, s.name skin_name, ca.name campaign_name, so.name source_name, me.name medium_name, "
					+ "ct.name content_name, si.size_vertical, si.size_horizontal, ty.name type_name, lo.location, "
					+ "lp.name landing_page_name, lp.url, lp.landing_page_type, "
					+ " md.domain "
					+ "from marketing_combinations co, skins s, marketing_campaigns ca, marketing_sources so, "
					+ "marketing_mediums me, marketing_contents ct, marketing_sizes si, marketing_types ty,"
					+ "marketing_locations lo, marketing_landing_pages lp, "
					+ " marketing_combination_domains mcd, "
			 	 	+ " marketing_domains md "
					+ "where co.skin_id = s.id "
					+ "and co.campaign_id = ca.id "
					+ "and ca.source_id = so.id "
					+ "and co.medium_id = me.id "
					+ "and co.content_id = ct.id "
					+ "and co.size_id = si.id(+) "
					+ "and co.type_id = ty.id(+) "
					+ "and co.location_id = lo.id(+) "
					+ "and co.landing_page_id = lp.id " +
					" and co.id in ( " + mcIds + " ) "
					+ " AND co.id = mcd.combination_id "
					+ " AND mcd.domain_id = md.id ";

			

			sql += "order by co.id ";
			ps = con.prepareStatement(sql);
			rs = ps.executeQuery();
			long i = 1;
			while (rs.next()) {
				MarketingCombination vo = getVO(rs);
				vo.setSkinName(rs.getString("skin_name"));
				vo.setCampaignName(rs.getString("campaign_name"));
				vo.setSourceName(rs.getString("source_name"));
				vo.setMediumName(rs.getString("medium_name"));
				vo.setContentName(rs.getString("content_name"));
				vo.setTypeName(rs.getString("type_name"));
				vo.setLocation(rs.getString("location"));
				vo.setLandingPageName(rs.getString("landing_page_name"));
				vo.setLandingPageUrl(rs.getString("url"));
				vo.setVerticalSize(rs.getString("size_vertical"));
				vo.setHorizontalSizel(rs.getString("size_horizontal"));
				vo.setPixelsSi(MarketingPixelsDAO.getPixelsByCombIdSI(con, vo.getId()));
				vo.setLandingPageType(rs.getInt("landing_page_type"));
				vo.getMarketingDomain().setDomain(rs.getString("domain"));
				list.add(new MarketingCombinationSeris(vo, i, null));				
			}

		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}

		return list;
	}	  

	  /**
	   * Check if combination already in db. if so, returns the existing combination id
	   * @param con   db connection
	   * @param vo  MarketingCombination instance
	   * @return combination id if combination in use, 0 o.w.
	   * @throws SQLException
	   */
	  public static long isInUse(Connection con, MarketingCombination vo) throws SQLException {

		  ResultSet rs = null;
		  PreparedStatement ps = null;

		  try {

			  String sql = "SELECT * " +
			  			   "FROM marketing_combinations " +
			  			   "WHERE skin_id" + 		setSqlLongValueOrIsNull(vo.getSkinId()) +
			  			   " AND campaign_id" + 		setSqlLongValueOrIsNull(vo.getCampaignId()) +
			  			   " AND medium_id" + 		setSqlLongValueOrIsNull(vo.getMediumId()) +
			  			   " AND content_id" + 		setSqlLongValueOrIsNull(vo.getContentId()) +
			  			   " AND size_id" + 			setSqlLongValueOrIsNull(vo.getSizeId()) +
			  			   " AND type_id" + 			setSqlLongValueOrIsNull(vo.getTypeId()) +
			  			   " AND location_id" + 		setSqlLongValueOrIsNull(vo.getLocationId()) +
			  			   " AND landing_page_id" +	setSqlLongValueOrIsNull(vo.getLandingPageId());

			  if ( vo.getId() > 0 ) {  // for update
				  sql += "AND id <> " + vo.getId();
			  }
			  ps = con.prepareStatement(sql);
			  rs = ps.executeQuery();
			  if ( rs.next() ) {
				  return rs.getLong("id");
			  }
		  } finally {
			  closeResultSet(rs);
			  closeStatement(ps);
		  }

		  return 0;
	  }
	  
	  
	/**
	 * Get marketing combination (for reg leads screen for now)
	 * @param con
	 * @return
	 * @throws SQLException
	 */
	public static ArrayList<SelectItem> getCombinationIdWithCampaigns(Connection con) throws SQLException {
		  
		  PreparedStatement ps = null;
		  ResultSet rs = null;
		  ArrayList<SelectItem> list = new ArrayList<SelectItem>(); 
		  try {
			  String sql = 	" SELECT " +
					  		"	co.id," +
					  		"	co.skin_id," +
					  		"	co.campaign_id campaign_id, " +
					  		"	ca.name campaign_name  " +
			  				" FROM " +			  					
							"	marketing_combinations co, " +
							"	marketing_campaigns ca " +
							" WHERE " +
							"	co.campaign_id = ca.id " +
							" ORDER BY " +
							"	co.id ";

			ps = con.prepareStatement(sql);
			rs = ps.executeQuery();

			while (rs.next()) {
				MarketingCombination vo = new MarketingCombination();
				vo.setId(rs.getLong("id"));
				vo.setCampaignId(rs.getLong("campaign_id"));
				vo.setCampaignName(rs.getString("campaign_name"));
				list.add(new SelectItem(new Long(vo.getId()), vo.getId() + "_" + vo.getCampaignName()));
			}
		  } finally {
			  closeResultSet(rs);
			  closeStatement(ps);
		  }
		  return list;
	  }


	
	/**
	 * Get marketing combination(s) for campaign 
	 * @param con
	 * @return
	 * @throws SQLException
	 */
	public static ArrayList<MarketingCombination> getCombinationsWithCampaignIDSourceID(Connection con, Long campaignID, Long sourceID, long writerIdForSkin) throws SQLException {
		  
		  PreparedStatement ps = null;
		  ResultSet rs = null;
		  ArrayList<MarketingCombination> result = new ArrayList<MarketingCombination>(); 
		  try {
			  String sql = 	" SELECT " +
					  		"	co.id," +
					  		"	co.skin_id," +
					  		"	co.campaign_id campaign_id, " +
					  		"	ca.name campaign_name,  " +
					  		"	so.name source_name " +
			  				" FROM " +			  					
							"	marketing_combinations co, " +
							"	marketing_campaigns ca, " +
							"	marketing_sources so " +
							" WHERE " ;
							if(campaignID != 0) {
								sql += "ca.id = "+campaignID+" and ";
							}
							
							if(sourceID != 0) {
								sql += "ca.source_id = "+sourceID+" and";
							}
							if (writerIdForSkin != 0) {  // don't check skin in the marketing pages
								sql += 	" co.skin_id in (select ws.skin_id from writers_skin ws where ws.writer_id = "+ writerIdForSkin +" ) and ";
							}
							
							sql +=	"	co.campaign_id = ca.id and " +
									"	ca.source_id = so.id " +
									"	ORDER BY co.id ";

			ps = con.prepareStatement(sql);
			
			rs = ps.executeQuery();

			while (rs.next()) {
				MarketingCombination vo = new MarketingCombination();
				vo.setId(rs.getLong("id"));
				vo.setCampaignId(rs.getLong("campaign_id"));
				vo.setCampaignName(rs.getString("campaign_name"));
				vo.setSourceName(rs.getString("source_name"));
				vo.setPixelsSi(MarketingPixelsDAO.getPixelsByCombIdSI(con, vo.getId()));
				result.add(vo);
			}
		  } finally {
			  closeResultSet(rs);
			  closeStatement(ps);
		  }
		  return result;
	  }
	
	
	
	  private static String setSqlLongValueOrIsNull(long value) throws SQLException {
		  if (0 == value) {
			  return " IS NULL";
		  }
		  else {
			  return " = " + String.valueOf(value);
		  }
	  }

}

