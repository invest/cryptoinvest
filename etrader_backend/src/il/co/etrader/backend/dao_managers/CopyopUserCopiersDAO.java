package il.co.etrader.backend.dao_managers;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;

import com.copyop.common.dao.DAOBase;
import com.copyop.common.dto.ProfileLink;
import com.copyop.common.dto.ProfileLinkHistory;
import com.copyop.common.enums.ProfileLinkChangeReasonEnum;
import com.copyop.common.enums.base.ProfileLinkTypeEnum;
import com.datastax.driver.core.BoundStatement;
import com.datastax.driver.core.PreparedStatement;
import com.datastax.driver.core.ResultSet;
import com.datastax.driver.core.Row;
import com.datastax.driver.core.Session;

public class CopyopUserCopiersDAO extends DAOBase {

	private static final Logger log = Logger.getLogger(CopyopUserCopiersDAO.class);

	public static List<ProfileLink> getProfileLinksByType(Session session, long userId, ProfileLinkTypeEnum type,
			long userOffsetId) {
		List<ProfileLink> result = new ArrayList<ProfileLink>();
		PreparedStatement ps = methodsPreparedStatement.get("getProfileLinksByType");
		if (ps == null) {
			String cql = "SELECT * FROM profile_links WHERE user_id = ? AND type = ? AND dest_user_id > ? limit 10";
			ps = session.prepare(cql);
			methodsPreparedStatement.put("getProfileLinksByType", ps);
		} 
		BoundStatement boundStatement = new BoundStatement(ps);
		if (log.isTraceEnabled()) {
			boundStatement.enableTracing();
		}

		ResultSet rs = session.execute(boundStatement.bind(userId, type.getCode(), userOffsetId));
		if (log.isTraceEnabled()) {
			traceLog(rs.getExecutionInfo(), log);
		}
		while (!rs.isExhausted()) {
			result.add(loadProfileLink(rs.one()));
		}
		return result;
	}

	public static ArrayList<ProfileLinkHistory> getProfileLinksHistory(Session session, long userId,
			ProfileLinkTypeEnum type, Date timeCreatedOffset, Date fromDate, Date toDate) {
		PreparedStatement ps = methodsPreparedStatement.get("getProfileLinksHistoryByType");
		ArrayList<ProfileLinkHistory> profileLinkHistory = new ArrayList<ProfileLinkHistory>();
		if (ps == null) {
			String cql = "SELECT " + "	* " + "FROM " + "	profile_links_history " + "WHERE " + "	user_id = ? "
					+ "	AND type = ? AND time_created < ? AND time_created > ? limit 10 ";
			ps = session.prepare(cql);
			methodsPreparedStatement.put("getProfileLinksHistoryByType", ps);
		}
		BoundStatement boundStatement = new BoundStatement(ps);
		
		ResultSet resultSet = null;
		if (timeCreatedOffset != null) {
			resultSet = session.execute(boundStatement.bind(userId, type.getCode(), timeCreatedOffset, fromDate));
		} else {
			resultSet = session.execute(boundStatement.bind(userId, type.getCode(), toDate, fromDate));
		}
		while (!resultSet.isExhausted()) {
			profileLinkHistory.add(getProfileLinkHistoryVO(resultSet.one()));
		}
		return profileLinkHistory;
	}

	private static ProfileLink loadProfileLink(Row row) {
		ProfileLink pl = new ProfileLink();
		pl.setUserId(row.getLong("user_id"));
		pl.setType(ProfileLinkTypeEnum.of(row.getInt("type")));
		pl.setDestUserId(row.getLong("dest_user_id"));
		pl.setTimeCreated(row.getDate("time_created"));
		pl.setCount(row.getInt("count"));
		pl.setCountReached(row.getInt("count_reached"));
		pl.setAmount(row.getInt("amount"));
		pl.setAssets(row.getSet("assets", Long.class));
		pl.setWriterId(row.getLong("writer_id"));
		pl.setWatchingPushNotification(row.getBool("watching_push_notification"));
		return pl;
	}

	private static ProfileLinkHistory getProfileLinkHistoryVO(Row row) {
		ProfileLinkHistory profileLinkHistory = new ProfileLinkHistory();
		profileLinkHistory.setUserId(row.getLong("user_id"));
		profileLinkHistory.setType(ProfileLinkTypeEnum.of(row.getInt("type")));
		profileLinkHistory.setTimeCreated(row.getDate("time_created"));
		profileLinkHistory.setAmount(row.getInt("amount"));
		profileLinkHistory.setAssets(row.getSet("assets", Long.class));
		profileLinkHistory.setCount(row.getInt("count"));
		profileLinkHistory.setDestUserId(row.getLong("dest_user_id"));
		profileLinkHistory.setResonDone(ProfileLinkChangeReasonEnum.of(row.getInt("reason_done")));
		profileLinkHistory.setTimeDone(row.getDate("time_done"));

		return profileLinkHistory;
	}
}
