package il.co.etrader.backend.dao_managers;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import com.anyoption.common.daos.DAOBase;

/**
 * MarketingCombinationDomainsDAO
 * @author eyalo
 */
public class MarketingCombinationDomainsDAO extends DAOBase {
	
	/**
	 * Insert into marketing combination domains.
	 * @param con
	 * @param combId
	 * @param domainId
	 * @throws SQLException
	 */
	public static void insert(Connection con, long combId, long domainId) throws SQLException {
		PreparedStatement ps = null;
		try {
			String sql = "INSERT INTO MARKETING_COMBINATION_DOMAINS (ID ,COMBINATION_ID ,DOMAIN_ID) " +
					" VALUES (SEQ_MARKETING_COMB_DOMAINS.nextval,?,?)";
			ps = con.prepareStatement(sql);
			ps.setLong(1, combId);
			ps.setLong(2, domainId);
			ps.executeUpdate();
		} finally {
			closeStatement(ps);
		}
	}
}
