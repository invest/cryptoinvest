package il.co.etrader.backend.dao_managers;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.base.DeeplinkParamsToUrl;

public class DeeplinkParamsToUrlDAO extends com.anyoption.common.daos.DeeplinkParamsToUrlDAO{
	
	private static final Logger logger = Logger.getLogger(DeeplinkParamsToUrlDAO.class);

	public static void update(Connection con, DeeplinkParamsToUrl deeplinkParamsToUrl) throws SQLException {
		PreparedStatement ps = null;
		int index = 1;
		try {							
			String sql =" UPDATE "
							+ "deeplink_params_to_url "
						+ "SET "
						  	+ "copyop_url         = ?, "
						  	+ "anyoption_url      = ?, "
						  	+ "parameter_name     = ?, "
						  	+ "writer_id          = ?, "
						  	+ "time_updated       = sysdate "
					  	+ "WHERE "
				  	  		+ "id = ? ";
			ps = con.prepareStatement(sql);
			ps.setString(index++, deeplinkParamsToUrl.getCopyopUrl());
			ps.setString(index++, deeplinkParamsToUrl.getAnyoptionUrl());
			ps.setString(index++, deeplinkParamsToUrl.getParameterName());				
			ps.setLong(index++, deeplinkParamsToUrl.getWriterId());
			ps.setInt(index++, deeplinkParamsToUrl.getId());
			ps.executeUpdate();	
		} finally {
			closeStatement(ps);	
		}
	}
  
	public static void insert(Connection con, DeeplinkParamsToUrl deeplinkParamsToUrl) throws SQLException {
		PreparedStatement ps = null;
		try {
			String sql = "INSERT INTO "
					   		+ "deeplink_params_to_url "
				   		+ "( "
		  			 		+ "time_created, "
		  			 		+ "copyop_url, "
		  			 		+ "anyoption_url, "
		  			 		+ "parameter_name, "
		  			 		+ "id, "
		  			 		+ "writer_id "
	  			 		+ ") "
	  			 		+ "VALUES "
	  			 		+ "( "
							+ "SYSDATE, "
						    + "?, "
						    + "?, "
						    + "?, "
						    + "seq_deeplink_params_to_url.nextval, "
						    + "? "
					    + ")";

			ps = con.prepareStatement(sql);
			int index = 1;
			ps.setString(index++, deeplinkParamsToUrl.getCopyopUrl());
			ps.setString(index++, deeplinkParamsToUrl.getAnyoptionUrl());
			ps.setString(index++, deeplinkParamsToUrl.getParameterName());
			ps.setLong(index++, deeplinkParamsToUrl.getWriterId());
		  
			ps.executeUpdate();
		} finally {
			closeStatement(ps);
		}
	}
}
