package il.co.etrader.backend.dao_managers;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.faces.model.SelectItem;

import org.apache.log4j.Logger;

import com.anyoption.common.daos.DAOBase;



public class MarketingPaymentTypesDAO extends DAOBase {

	private static final Logger logger = Logger.getLogger(MarketingPaymentTypesDAO.class);


	  /**
	   * Get payment types for marketing
	   * @throws SQLException
	   */
	  public static ArrayList<SelectItem> getList(Connection con) throws SQLException {

		  PreparedStatement ps = null;
		  ResultSet rs = null;
		  ArrayList<SelectItem> list = new ArrayList<SelectItem>();

		  try {

				String sql = "select * " +
							 "from marketing_payment_types ";


				ps = con.prepareStatement(sql);
				rs = ps.executeQuery();

				while (rs.next()) {
					list.add(new SelectItem(new Long(rs.getLong("id")),rs.getString("description")));
				}

			} finally {
					closeResultSet(rs);
					closeStatement(ps);
			}

			return list;
	  }


}

