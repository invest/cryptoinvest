package il.co.etrader.backend.dao_managers;

import il.co.etrader.bl_vos.MarketingContent;
import il.co.etrader.util.CommonUtil;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.anyoption.common.daos.DAOBase;


public class MarketingContentsDAO extends DAOBase {


	/**
	 * Insert a new content
	 * @param con  db connection
	 * @param vo MarketingContent instance
	 * @throws SQLException
	 */
	public static void insert(Connection con,MarketingContent vo) throws SQLException {

		  PreparedStatement ps = null;
		  ResultSet rs = null;

		  try {

				String sql = "insert into marketing_contents(id,name,writer_id,time_created,comments) " +
						     "values(seq_mar_contents.nextval,?,?,sysdate,?)";

				ps = con.prepareStatement(sql);

				ps.setString(1, vo.getName());
				ps.setLong(2, vo.getWriterId());
				ps.setString(3, vo.getComments());

				ps.executeUpdate();

				vo.setId(getSeqCurValue(con,"seq_mar_contents"));
		  }
		  finally {
				closeStatement(ps);
				closeResultSet(rs);
			}

	  }

	  /**
	   * Update marketing content
	   * @param con db connection
	   * @param vo  MarketingContent instance
	   * @throws SQLException
	   */
	  public static void update(Connection con,MarketingContent vo) throws SQLException {

		  PreparedStatement ps = null;
		  ResultSet rs = null;

		  try {

			  String sql = "update marketing_contents set name=?,writer_id=?,comments=? " +
			  			   "where id=?";

				ps = con.prepareStatement(sql);


				ps.setString(1, vo.getName());
				ps.setLong(2, vo.getWriterId());
				ps.setString(3, vo.getComments());
				ps.setLong(4, vo.getId());

				ps.executeUpdate();

		  }
		  finally	{
				closeStatement(ps);
				closeResultSet(rs);
			}

	  }


	  /**
	   * Get all content
	   * @param con  db connection
	   * @param contentName  source name filter
	   * @return
	   *	ArrayList of MarketingContent
	   * @throws SQLException
	   */
	  public static ArrayList<MarketingContent> getAll(Connection con, String contentName, long writerIdForSkin) throws SQLException {

		  PreparedStatement ps = null;
		  ResultSet rs = null;
		  ArrayList<MarketingContent> list = new ArrayList<MarketingContent>();

		  try {

				String sql = "select * " +
							 "from marketing_contents where 1=1 ";

				if ( !CommonUtil.isParameterEmptyOrNull(contentName) ) {
					sql += "and upper(name) like '%" + contentName.toUpperCase() + "%' ";
				}
				
				if (writerIdForSkin > 0) { 
					sql += 	" and writer_id in (select distinct writer_id from " +
												" writers_skin where skin_id in " +
													" (select skin_id from writers_skin where writer_id ="+ writerIdForSkin +")) ";
				}


				sql += "order by upper(name)";

				ps = con.prepareStatement(sql);
				rs = ps.executeQuery();

				while (rs.next()) {
					MarketingContent vo = getVO(rs);
					list.add(vo);
				}

			} finally {
					closeResultSet(rs);
					closeStatement(ps);
			}

			return list;
	  }


	  /**
	   * Check if content name in use
	   * @param con   db connection
	   * @param name  source name
	   * @return true if name in use
	   * @throws SQLException
	   */
	  public static boolean isNameInUse(Connection con, String name, long id) throws SQLException {

		  ResultSet rs = null;
		  PreparedStatement ps = null;

		  try {

			  String sql = "select * " +
			  			   "from marketing_contents " +
			  			   "where name like ? ";

			  if ( id > 0 ) {  // for update
				  sql += "and id <> " + id;
			  }

			  ps = con.prepareStatement(sql);
			  ps.setString(1, name);

			  rs = ps.executeQuery();

			  if ( rs.next() ) {
				  return true;
			  }
		  } finally {
			  closeResultSet(rs);
			  closeStatement(ps);
		  }

		  return false;

	  }

	  /**
	   * Get VO
	   * @param rs
	   * 	Result set instance
	   * @return
	   * 	MarketingContent object
	   * @throws SQLException
	   */
	  private static MarketingContent getVO(ResultSet rs) throws SQLException {

		  MarketingContent vo = new MarketingContent();

			vo.setId(rs.getLong("id"));
			vo.setName(rs.getString("name"));
			vo.setTimeCreated(convertToDate(rs.getTimestamp("time_created")));
			vo.setWriterId(rs.getLong("writer_id"));
			vo.setComments(rs.getString("comments"));

			return vo;
	  }

}

