package il.co.etrader.backend.dao_managers;

import il.co.etrader.bl_vos.MarketingMedium;
import il.co.etrader.util.CommonUtil;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.anyoption.common.daos.DAOBase;


public class MarketingMediumsDAO extends DAOBase {


	/**
	 * Insert a new medium
	 * @param con  db connection
	 * @param vo MarketingMedium instance
	 * @throws SQLException
	 */
	public static void insert(Connection con,MarketingMedium vo) throws SQLException {

		  PreparedStatement ps = null;
		  ResultSet rs = null;

		  try {

				String sql = "insert into marketing_mediums(id,name,writer_id,time_created) " +
						     "values(seq_mar_mediums.nextval,?,?,sysdate)";

				ps = con.prepareStatement(sql);

				ps.setString(1, vo.getName());
				ps.setLong(2, vo.getWriterId());

				ps.executeUpdate();

				vo.setId(getSeqCurValue(con,"seq_mar_mediums"));
		  }
		  finally {
				closeStatement(ps);
				closeResultSet(rs);
			}

	  }

	  /**
	   * Update marketing medium
	   * @param con db connection
	   * @param vo  MarketingMedium instance
	   * @throws SQLException
	   */
	  public static void update(Connection con,MarketingMedium vo) throws SQLException {

		  PreparedStatement ps = null;
		  ResultSet rs = null;

		  try {

			  String sql = "update marketing_mediums set name=?,writer_id=? " +
			  			   "where id=?";

				ps = con.prepareStatement(sql);


				ps.setString(1, vo.getName());
				ps.setLong(2, vo.getWriterId());
				ps.setLong(3, vo.getId());

				ps.executeUpdate();

		  }
		  finally	{
				closeStatement(ps);
				closeResultSet(rs);
			}

	  }


	  /**
	   * Get all mediums
	   * @param con  db connection
	   * @param mediumName  medium name filter
	   * @return
	   *	ArrayList of MarketingMedium
	   * @throws SQLException
	   */
	  public static ArrayList<MarketingMedium> getAll(Connection con, String mediumName, long writerIdForSkin) throws SQLException {

		  PreparedStatement ps = null;
		  ResultSet rs = null;
		  ArrayList<MarketingMedium> list = new ArrayList<MarketingMedium>();

		  try {

				String sql = "select * " +
							 "from marketing_mediums where 1=1 ";

				if ( !CommonUtil.isParameterEmptyOrNull(mediumName) ) {
					sql += " and upper(name) like '%" + mediumName.toUpperCase() + "%' ";
				}
				
				if (writerIdForSkin > 0) { 
					sql += 	" and writer_id in (select distinct writer_id from " +
												" writers_skin where skin_id in " +
													" (select skin_id from writers_skin where writer_id ="+ writerIdForSkin +")) ";
				}

				sql += "order by upper(name)";

				ps = con.prepareStatement(sql);
				rs = ps.executeQuery();

				while (rs.next()) {
					MarketingMedium vo = getVO(rs);
					list.add(vo);
				}

			} finally {
					closeResultSet(rs);
					closeStatement(ps);
			}

			return list;
	  }


	  /**
	   * Check if medium name in use
	   * @param con   db connection
	   * @param name  medium name
	   * @return true if name in use
	   * @throws SQLException
	   */
	  public static boolean isNameInUse(Connection con, String name, long id) throws SQLException {

		  ResultSet rs = null;
		  PreparedStatement ps = null;

		  try {

			  String sql = "select * " +
			  			   "from marketing_mediums " +
			  			   "where name like ? ";

			  if ( id > 0 ) {  // for update
				  sql += "and id <> " + id;
			  }

			  ps = con.prepareStatement(sql);
			  ps.setString(1, name);

			  rs = ps.executeQuery();

			  if ( rs.next() ) {
				  return true;
			  }
		  } finally {
			  closeResultSet(rs);
			  closeStatement(ps);
		  }

		  return false;

	  }

	  /**
	   * Get VO
	   * @param rs
	   * 	Result set instance
	   * @return
	   * 	MarketingMedium object
	   * @throws SQLException
	   */
	  private static MarketingMedium getVO(ResultSet rs) throws SQLException {

		  MarketingMedium vo = new MarketingMedium();

			vo.setId(rs.getLong("id"));
			vo.setName(rs.getString("name"));
			vo.setTimeCreated(convertToDate(rs.getTimestamp("time_created")));
			vo.setWriterId(rs.getLong("writer_id"));

			return vo;
	  }

}

