/**
 * 
 */
package il.co.etrader.backend.dao_managers;

import il.co.etrader.backend.util.Utils;
import il.co.etrader.util.CommonUtil;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.faces.model.SelectItem;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.Tournament;
import com.anyoption.common.beans.TournamentBannerFile;
import com.anyoption.common.beans.TournamentLanguageTabs;
import com.anyoption.common.beans.TournamentSkinsLanguages;
import com.anyoption.common.beans.TournamentsBanners;
import com.anyoption.common.daos.TournamentDAOBase;

/**
 * @author pavelt
 *
 */
public class TournamentDAO extends TournamentDAOBase {
	
	private static final Logger logger = Logger.getLogger(TournamentDAO.class);
	
	
	public static void insertTournament(Connection conn, Tournament tournament) throws SQLException{
	  	  PreparedStatement ps = null;
		  ResultSet rs = null;
		  logger.info("Going to insert to TOURNAMENT table....");
		  try {
			  String sql = " INSERT INTO TOURNAMENTS "+
			  			   " (ID, TYPE_ID, NAME, TIME_START, TIME_END, IS_ACTIVE, MIN_INV_AMOUNT, WRITER_ID, " +
			  			   " TIME_UPDATED, NUMBER_OF_USERS_ID, SKIN, CURRENCY_ID, TIME_CREATED) " +
			  			   " VALUES " +
			  			   " (SEQ_TOURNAMENTS.nextval, ?, ?, ?, ?, ?, ?, ?, null, ?, ?, ?, sysdate)";
			  ps = conn.prepareStatement(sql);
			  ps.setLong(1, tournament.getType().getId());
			  ps.setString(2, tournament.getName());
			  ps.setTimestamp(3, CommonUtil.convertToTimeStamp(tournament.getTimeStart()));
			  ps.setTimestamp(4, CommonUtil.convertToTimeStamp(tournament.getTimeEnd()));
			  ps.setInt(5, tournament.getActive());
			  ps.setLong(6, tournament.getMinInvAmount() * 100);
			  ps.setLong(7, tournament.getWriterId());
			  ps.setLong(8, tournament.getNumberOfUsersToShow().getId());
			  ps.setString(9, tournament.getSkinsToDB());
			  ps.setLong(10, tournament.getCurrency().getId());
			  ps.executeQuery();
			  
			  tournament.setId(getSeqCurValue(conn, "SEQ_TOURNAMENTS"));
		  } finally {
				closeResultSet(rs);
				closeStatement(ps);
		  }
		 //return true;
	}
	
	public static void insertTournamentSkinLang(Connection conn,Tournament tournament) throws SQLException {
		PreparedStatement ps = null;
		ArrayList<TournamentSkinsLanguages> list = tournament.getTournamentSkinsLangList();
		 logger.info("Going to INSERT to TOURNAMENT_SKIN_LANG table....");
		try {
			for (TournamentSkinsLanguages item : list) {
				if (item.getTabId() != 0) {
					String sql = "INSERT INTO tournament_skin_lang"
							+ "(ID, tournament_id, tab_id, prize_text, prize_image)"
							+ "VALUES"
							+ "(seq_tournament_skin_lang.nextval, ?, ?, ?, ?)";

					ps = conn.prepareStatement(sql);
					ps.setLong(1, tournament.getId());
					ps.setLong(2, item.getTabId());
					ps.setString(3, item.getPrizeText());
					ps.setString(4, item.getPrizeImage());
					ps.executeQuery();

					item.setId(getSeqCurValue(conn, "seq_tournament_skin_lang"));
				}

			}
		} finally {
			closeStatement(ps);
		}

		//return true;
	}
	
	public static void insertTournamentsBanners(Connection conn, TournamentsBanners tournamentsBanners) throws SQLException {
		PreparedStatement ps = null;
		ArrayList<TournamentBannerFile> files = tournamentsBanners.getFiles();
		 logger.info("Going to INSERT into TOURNAMENTS_BANNERS table....");
		try{
			String sql = "INSERT INTO tournaments_banners "
					+ " (ID, tab_id, banner_index, banner_image, writer_id, date_created) "
					+ " VALUES "
					+ " (seq_tournaments_banners.nextval,?,?,?,?,SYSDATE)";
			
			ps = conn.prepareStatement(sql);
			
			for(TournamentBannerFile file : files){
				ps.setLong(1, tournamentsBanners.getTabId());
				ps.setLong(2, file.getBannerIndex());
				ps.setString(3, TournamentsBanners.TOURNAMENTS_BANNERS_DIR_NAME + "/" + file.getFile().getName());
				ps.setLong(4, tournamentsBanners.getWriterId());
				ps.executeQuery();
			}
		}finally{
			closeStatement(ps);
		}
	}
	
	public static void updateTournamentsBanners(Connection conn, TournamentsBanners tournamentsBanners) throws SQLException{
		PreparedStatement ps = null;
		ArrayList<TournamentBannerFile> files = tournamentsBanners.getFiles();
		logger.info("Going to UPDATE TOURNAMENTS_BANNERS table....");
		try {
			
			String sql = " UPDATE tournaments_banners "+
						  " SET writer_id = ?, "+
						      " banner_image = ?, "+
						      " date_created = sysdate "+
						   " WHERE id = ? ";
			ps = conn.prepareStatement(sql);
			ps.setLong(1, tournamentsBanners.getWriterId());
			for (TournamentBannerFile file : files) {
				ps.setString(2, file.getBannerImage());
				ps.setLong(3, file.getId());
				ps.executeQuery();
			}
		} finally {
			closeStatement(ps);
		}
	}
	

	public static void updateTournamentSkinLang(Connection con, Tournament tournament) throws SQLException {
		PreparedStatement ps = null;
		ArrayList<TournamentSkinsLanguages> list = tournament.getTournamentSkinsLangList();
		logger.info("Going to UPDATE TOURNAMENT_SKIN_LANG table....");
		try {
			for (TournamentSkinsLanguages item : list) {
				String sql = "UPDATE tournament_skin_lang "
						+ " SET prize_text = ?, "
						+ " prize_image   = ? "
						+ " WHERE id = ? ";

				ps = con.prepareStatement(sql);
				ps.setString(1, item.getPrizeText());
				ps.setString(2, item.getPrizeImage());
				ps.setLong(3, item.getId());
				ps.executeUpdate();
			}
		} finally {
			closeStatement(ps);
		}
	}


	public static ArrayList<SelectItem> getNumberOfUsersSI(Connection con) throws SQLException {
        PreparedStatement ps = null;
        ResultSet rs = null;
        ArrayList<SelectItem> hm = new ArrayList<SelectItem>();
        try{
            String sql =
            	"SELECT " +
            		" * " +
            	"FROM " +
            		" tournament_num_of_users " +
            	"ORDER BY " +
            		" id";
            ps = con.prepareStatement(sql);
            rs = ps.executeQuery();
            while (rs.next()) {
                hm.add(new SelectItem(rs.getLong("id"), rs.getString("users_to_show")));
            }
        } finally {
            closeStatement(ps);
            closeResultSet(rs);
        }
        return hm;
	}
	
	public static ArrayList<SelectItem> getTypes(Connection con) throws SQLException {
        PreparedStatement ps = null;
        ResultSet rs = null;
        ArrayList<SelectItem> hm = new ArrayList<SelectItem>();
        try{
            String sql =
            	"SELECT " +
            		" * " +
            	"FROM " +
            		" tournament_type " +
            	"ORDER BY " +
            		" id";
            ps = con.prepareStatement(sql);
            rs = ps.executeQuery();
            while (rs.next()) {
                hm.add(new SelectItem(rs.getLong("id"), rs.getString("name")));
            }
        } finally {
            closeStatement(ps);
            closeResultSet(rs);
        }
        return hm;
	}


	public static boolean updateTournament(Connection con, Tournament tournament) throws SQLException {
		PreparedStatement ps = null;
		boolean isUpdate = false;
		int index = 1;
		try	{

		String sql = " UPDATE " +
					 	" tournaments " +
					 "SET " +
					 	"time_start	        = ?, " +
					 	"name               = ?, " +
					 	"number_of_users_id = ?, " +
					 	"writer_id          = ?, " +
					 	"time_end           = ?, " +
					 	"skin 				= ?, " +
					 	"time_updated       = sysdate " +
					 "WHERE " +
					 	"id = ?";

		ps = con.prepareStatement(sql);
		ps.setTimestamp(index++, CommonUtil.convertToTimeStamp(tournament.getTimeStart()));
		ps.setString(index++, tournament.getName());
		ps.setLong(index++, tournament.getNumberOfUsersToShow().getId());
		ps.setLong(index++, Utils.getWriter().getWriter().getId());
		ps.setTimestamp(index++, CommonUtil.convertToTimeStamp(tournament.getTimeEnd()));
		ps.setString(index++, tournament.getSkinsToDB());
		ps.setLong(index++, tournament.getId());

		isUpdate = ps.executeUpdate() > 0 ? true : false;

		} finally {
			closeStatement(ps);
		}

		return isUpdate;
	}
	
	public static boolean updateTournamentIsActive(Connection con, Tournament tournament) throws SQLException {
		PreparedStatement ps = null;
		boolean isUpdate = false;
		try	{

		String sql = "UPDATE tournaments " +
					 "SET is_active = ?, " +
					 	 "time_updated = sysdate, " +
					 	 "writer_id = ? " +
					 "WHERE " +
					 	"id = ?";

		ps = con.prepareStatement(sql);
		ps.setLong(1, tournament.getActive());
		ps.setLong(2, Utils.getWriter().getWriter().getId());
		ps.setLong(3, tournament.getId());

		isUpdate = ps.executeUpdate() > 0 ? true : false;

		} finally {
			closeStatement(ps);
		}

		return isUpdate;
	}
	
	public static ArrayList<TournamentLanguageTabs> getLangTabs(Connection con)throws SQLException {
		ArrayList<TournamentLanguageTabs> list = new ArrayList<TournamentLanguageTabs>();
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			String sql = "SELECT * FROM tournament_be_tabs_lng ORDER BY id";
			ps = con.prepareStatement(sql);
			rs = ps.executeQuery();

			while (rs.next()) {
				TournamentLanguageTabs item = new TournamentLanguageTabs();
				item.setTabId(rs.getLong("id"));
				item.setTabLabel(rs.getString("label"));
				item.setLangId(rs.getLong("def_lng_id"));
				list.add(item);
			}
		} finally {
			closeStatement(ps);
			closeResultSet(rs);
		}
		return list;
	}
}
