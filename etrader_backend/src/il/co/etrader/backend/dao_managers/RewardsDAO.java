package il.co.etrader.backend.dao_managers;

import il.co.etrader.backend.bl_vos.RewardUserTaskExpPoint;
import il.co.etrader.backend.bl_vos.RewardUserTierDistribution;
import il.co.etrader.backend.util.Constants;
import il.co.etrader.backend.util.Utils;
import il.co.etrader.util.CommonUtil;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;

import javax.faces.model.SelectItem;

import com.anyoption.common.beans.RewardTaskType;
import com.anyoption.common.beans.RewardTier;
import com.anyoption.common.beans.RewardUser;
import com.anyoption.common.beans.RewardUserBenefit;
import com.anyoption.common.beans.RewardUserHistory;
import com.anyoption.common.beans.RewardUserPlatform;
import com.anyoption.common.daos.DAOBase;
import com.anyoption.common.managers.RewardManager;
import com.anyoption.common.managers.RewardUsersHistoryManager;

/**
 * @author EranL
 *
 */
public class RewardsDAO extends DAOBase {
		
	/**
	 * Get reward actions (task or benefits)
	 * @param con
	 * @return
	 * @throws SQLException
	 */
	public static ArrayList<SelectItem> getRewardActions(Connection con) throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		ArrayList<SelectItem> list = new  ArrayList<SelectItem>();
		try {
			String sql =" SELECT " +
						"	'BENEFITS' AS type, " +
						"	rb.id, " +
						"	rb.name " + 
						" FROM " +
						"	rwd_benefits rb " +
						" UNION " +
						" SELECT " +
						"	'TASK' AS type, " +
						"	rtt.id, " + 
						"	rtt.name " +
						" FROM " +
						"	rwd_task_types rtt"; 
		    ps = con.prepareStatement(sql);
			rs = ps.executeQuery();
			while ( rs.next() ) {
				list.add(new SelectItem(rs.getString("type") + "_" + rs.getLong("id"), rs.getString("name")));
			}
		} finally {
			closeStatement(ps);
			closeResultSet(rs);
		}
		return list;
	}	
	
	/**
	 * Get reward tiers select item
	 * @param con
	 * @return
	 * @throws SQLException
	 */
	public static ArrayList<SelectItem> getRewardTiersSI(Connection con) throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		ArrayList<SelectItem> list = new  ArrayList<SelectItem>();
		try {
			String sql =" SELECT " +
		    			"	* " +
		    			" FROM " +
		    			"	rwd_tiers " +
		    			" WHERE " +
		    			"	min_exp_points > 0 "; // take only real tiers
		    ps = con.prepareStatement(sql);
			rs = ps.executeQuery();
			while (rs.next()) {
				list.add(new SelectItem(rs.getLong("id"), rs.getString("id") + "_" + rs.getString("name")));
			}
		} finally {
			closeStatement(ps);
			closeResultSet(rs);
		}
		return list;
	}
	
	/**
	 * Get reward tiers array
	 * @param con
	 * @return
	 * @throws SQLException
	 */
	public static ArrayList<RewardTier> getRewardTiers(Connection con) throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		ArrayList<RewardTier> list = new  ArrayList<RewardTier>();
		try {
			String sql =" SELECT " +
		    			"	rt.* " +
		    			" FROM " +
		    			"	rwd_tiers rt " +
		    			" WHERE " +
		    			"	rt.min_exp_points > 0 "; // take only real tiers
		    ps = con.prepareStatement(sql);
			rs = ps.executeQuery();
			while (rs.next()) {
				RewardTier vo = new RewardTier();
				vo.setId(rs.getInt("ID"));
				vo.setMinExperiencePoints(rs.getInt("MIN_EXP_POINTS"));
				vo.setName(rs.getString("NAME"));
				vo.setInActiveDays(rs.getInt("IN_ACTIVE_DAYS"));
				vo.setDaysToDemote(rs.getInt("DAYS_TO_DEMOTE"));
				vo.setExperiencePointsToReduce(rs.getInt("EXP_POINTS_TO_REDUCE"));
				list.add(vo);
			}
		} finally {
			closeStatement(ps);
			closeResultSet(rs);
		}
		return list;
	}
	
	/**
	 * Get reward history types 
	 * @param con
	 * @return
	 * @throws SQLException
	 */
	public static ArrayList<SelectItem> getRewardHistoryTypesSI(Connection con) throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		ArrayList<SelectItem> list = new  ArrayList<SelectItem>();
		try {
			String sql =" SELECT " +
		    			"	* " +
		    			" FROM " +
		    			"	rwd_his_types ";		    		
		    ps = con.prepareStatement(sql);
			rs = ps.executeQuery();
			while ( rs.next() ) {
				list.add(new SelectItem(rs.getLong("id"), rs.getString("name")));
			}
		} finally {
			closeStatement(ps);
			closeResultSet(rs);
		}
		return list;
	}
			
	/**
	 * Get rewards user
	 * @param con
	 * @param userId
	 * @return
	 * @throws SQLException
	 */
	public static RewardUser getRewardsUser(Connection con, long userId) throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		RewardUser rewardUser = null;
		try {
			String sql =" SELECT " +
						"	* " +
						" FROM " +
						"   rwd_users ru " +
						" WHERE " +
						"   ru.user_id = ? ";
		    ps = con.prepareStatement(sql);
		    ps.setLong(1, userId);
		    rs = ps.executeQuery();
			if (rs.next()) {
				rewardUser = new RewardUser();	
				rewardUser.setId(rs.getInt("ID"));
				rewardUser.setTierId(rs.getInt("RWD_TIER_ID")); 
				rewardUser.setUserId(rs.getLong("USER_ID"));				
				rewardUser.setExperiencePoints(rs.getInt("EXP_POINTS"));
				rewardUser.setTimeLastActivity(convertToDate(rs.getTimestamp("TIME_LAST_ACTIVITY")));
				rewardUser.setTimeLastReducedPoints(convertToDate(rs.getTimestamp("TIME_LAST_REDUCED_POINTS")));								
			}
		} finally {
			closeStatement(ps);
			closeResultSet(rs);
		}
		return rewardUser;
	}
			  	
	/**
	 * Get reward user task experience points distribution
	 * @param con
	 * @param userId
	 * @return
	 * @throws SQLException
	 */
	public static ArrayList<RewardUserTaskExpPoint> getRewardUserTaskExpPoints(Connection con, long userId) throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		ArrayList<RewardUserTaskExpPoint> list = new ArrayList<RewardUserTaskExpPoint>();
		try {
			String sql =
						// FTD
						" SELECT " +
						"	'1' as orderby, " +
						"	'FTD' as type, " +
						"	sum(amount_exp_points) as exp_pnts " +
						" FROM " +
						"	rwd_users_history ruh, " +
						"   rwd_action_types rat, " +
						"   rwd_user_tasks rut, " +
						"   rwd_tasks rt " +
						" WHERE " +
						"	ruh.rwd_action_type_id = rat.id " +
						"   AND rut.rwd_task_id = rt.id " +
						"   AND ruh.rwd_action_id = rut.id " +						
						"   AND rat.id = ? " + // task
						"   AND rt.rwd_task_type_id = ?  "  + //FTD
						"   AND ruh.user_id = ? " +						
						" UNION " +
						//Investments
						" SELECT " +
						"	'2' as orderby, " +
						"	'Investments' as type, " +
						"	sum(amount_exp_points) as exp_pnts " +
						" FROM " +
						"	rwd_users_history ruh, " +
						"   rwd_action_types rat, " +
						"   rwd_user_tasks rut, " +
						"   rwd_tasks rt, " +
						"   rwd_task_types rtt    " +
						" WHERE " +
						"   ruh.rwd_action_type_id = rat.id " +
						"   AND rut.rwd_task_id = rt.id " +
						"   AND ruh.rwd_action_id = rut.id " +
						"   AND rt.rwd_task_type_id = rtt.id " +
						"   AND rat.id = ? " + // task 
						"   AND rtt.id = ?  " + // investments pts
						"   AND ruh.user_id = ? " +
						" UNION " +
						//Other
						" SELECT " +
						"	'3' as orderby, " +
						"	'Other' as type, " +
						"	sum(amount_exp_points) as exp_pnts " +
						" FROM " +
						"   rwd_users_history ruh " +
						"   LEFT JOIN  rwd_user_tasks rut ON ruh.rwd_action_id  = rut.id " +
						"	   LEFT JOIN  rwd_tasks rt ON rt.id = rut.rwd_task_id " +
						"	      LEFT JOIN rwd_task_types rtt ON rtt.id = rt.rwd_task_type_id " +
						" WHERE " +
						"	ruh.user_id = ? " +
						"	AND ruh.AMOUNT_EXP_POINTS <> 0  " + // only amount that changed his exp pnts 
						"   AND ruh.rwd_his_type_id <> ?   " + // Reduce experience points	
                        "   AND ((rtt.ID is null) or (rtt.id <> ? AND rtt.id <> ?) ) " +  // remove investment pts/FTD but take in consideration fix balance
						" UNION " +
						//Demotion
						" SELECT " +
						"	'4' as orderby, " +
						"	'Demotion' as type, " +
						"	sum(amount_exp_points) as exp_pnts " +
						" FROM " +
						"   rwd_users_history ruh " +
						" WHERE " +
						"	ruh.rwd_his_type_id = ? " + // Reduce experience points
						"	AND ruh.user_id = ? ";   						
		    ps = con.prepareStatement(sql);
		    ps.setLong(1, RewardManager.TASK);
		    ps.setLong(2, RewardTaskType.REWARD_TASK_TYPE_FTD_AMOUNT);
		    ps.setLong(3, userId);
		    ps.setLong(4, RewardManager.TASK);
		    ps.setLong(5, RewardTaskType.REWARD_TASK_TYPE_INV_PNTS);
		    ps.setLong(6, userId);
		    ps.setLong(7, userId);
		    ps.setLong(8, RewardUsersHistoryManager.REDUCE_EXPERIENCE_POINTS);
		    ps.setLong(9, RewardTaskType.REWARD_TASK_TYPE_INV_PNTS);		    
		    ps.setLong(10, RewardTaskType.REWARD_TASK_TYPE_FTD_AMOUNT);		    
		    ps.setLong(11, RewardUsersHistoryManager.REDUCE_EXPERIENCE_POINTS);
		    ps.setLong(12, userId);
		    
		    rs = ps.executeQuery();
			while (rs.next()) {
				RewardUserTaskExpPoint vo = new RewardUserTaskExpPoint();				
				vo.setTaskType(rs.getString("type"));
				vo.setExperiencePointsGained(rs.getLong("exp_pnts"));
				list.add(vo);							
			}
		} finally {
			closeStatement(ps);
			closeResultSet(rs);
		}
		return list;
	}	
	
	/**
	 * Get reward user platforms (task experience points distribution) 
	 * @param con
	 * @param userId
	 * @return
	 * @throws SQLException
	 */
	public static ArrayList<RewardUserPlatform> getRewardUserPlatforms(Connection con, long userId) throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		ArrayList<RewardUserPlatform> list = new  ArrayList<RewardUserPlatform>();
		try {
			String sql =" SELECT " +
						"	opt.name, " +
						"	rup.* " +
						" FROM " +
						"   opportunity_product_types opt " +
						"   LEFT JOIN rwd_user_platforms rup ON rup.product_type_id = opt.id and rup.user_id = ? ";
		    ps = con.prepareStatement(sql);
		    ps.setLong(1, userId);
		    rs = ps.executeQuery();
			while (rs.next()) {
				RewardUserPlatform vo = new RewardUserPlatform();
				vo.setProductTypeName(rs.getString("NAME"));
				vo.setTurnover(rs.getLong("TURNOVER"));
				vo.setExperiencePoints(rs.getLong("EXP_POINTS"));
				list.add(vo);
			}
		} finally {
			closeStatement(ps);
			closeResultSet(rs);
		}
		return list;
	}	
	
	/**
	 * Get Rewards User Benefits 
	 * @param con
	 * @param userId
	 * @return
	 * @throws SQLException 
	 */
	public static ArrayList<RewardUserBenefit> getRewardsUserBenefits(Connection con, long userId) throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		ArrayList<RewardUserBenefit> list = new  ArrayList<RewardUserBenefit>();
		try {
			String sql =" SELECT " +
						"	rb.name reward_benefit_name, " +
						"   rb.rwd_tier_id reward_tier_id, " +
						"   rub.* " +
						" FROM " +
						"	rwd_benefits rb " +
						"	   LEFT JOIN rwd_user_benefits rub ON rb.id = rub.rwd_benefit_id AND user_id = ? " +
						" ORDER BY " +
						"	reward_tier_id ";		
			ps = con.prepareStatement(sql);
			ps.setLong(1, userId);
			rs = ps.executeQuery();
			while (rs.next()) {
				RewardUserBenefit vo = new RewardUserBenefit();
				vo.setRewardBenefitId(rs.getInt("RWD_BENEFIT_ID"));
				vo.setRewardBenefitName(rs.getString("REWARD_BENEFIT_NAME"));
				vo.setRewardTierId(rs.getInt("REWARD_TIER_ID"));
				vo.setActive(rs.getInt("IS_ACTIVE") == 1 ? true : false);
				vo.setUserId(rs.getLong("USER_ID"));				
				vo.setTimeCreated(convertToDate(rs.getTimestamp("TIME_CREATED")));																								
				list.add(vo);
			}
		} finally {
			closeStatement(ps);
			closeResultSet(rs);
		}
		return list;
	}
				
	/**
	 * Count reward user history table for user (for data model use) 
	 * @param con
	 * @param startRow
	 * @param pageSize
	 * @param userId
	 * @param from
	 * @param to
	 * @param tierId
	 * @param typeId
	 * @param actionId
	 * @return
	 * @throws SQLException
	 */
	public static int getRewardUserHistoryCount(Connection con, int startRow, int pageSize, long userId, Date from, Date to, 
			long tierId, long typeId, String actionId) throws SQLException {			
		PreparedStatement ps = null;
		ResultSet rs = null;
		int rowsCount = 0;
		try {					
			String sql =" SELECT " +
						"	count(*) AS rows_number " +
					 	" FROM ( ";			
			sql += getRewardUserHistoryString(con, startRow, pageSize, userId, from, to, 
					tierId, typeId, actionId);
			sql += " ) ";
			ps = con.prepareStatement(sql);
			ps.setLong(1, RewardManager.TASK);
			ps.setLong(2, RewardManager.BENEFIT);
			ps.setLong(3, RewardManager.TASK);
			ps.setLong(4, RewardManager.BENEFIT);
			ps.setLong(5, userId);
			ps.setTimestamp(6, CommonUtil.convertToTimeStamp(CommonUtil.getDateTimeFormat(from, Utils.getAppData().getUtcOffset())));
			ps.setTimestamp(7, CommonUtil.convertToTimeStamp(CommonUtil.getDateTimeFormat(CommonUtil.addDay(to), Utils.getAppData().getUtcOffset())));
			rs = ps.executeQuery();	
			while (rs.next()) {
				rowsCount = rs.getInt("rows_number");
			}
		} finally {
			closeStatement(ps);	
			closeResultSet(rs);
		}
		return rowsCount;				
	}
	
	/**
	 * Get reward user history list
	 * @param con
	 * @param startRow
	 * @param pageSize
	 * @param userId
	 * @param from
	 * @param to
	 * @param tierId
	 * @param typeId
	 * @param actionId
	 * @return
	 * @throws SQLException
	 */
	public static ArrayList<RewardUserHistory> getRewardUserHistory(Connection con, int startRow, int pageSize, long userId, Date from, Date to, 
			long tierId, long typeId, String actionId) throws SQLException {			
		PreparedStatement ps = null;
		ResultSet rs = null;
		ArrayList<RewardUserHistory> list = new ArrayList<RewardUserHistory>();
		try {							
			String sql = getRewardUserHistoryString(con, startRow, pageSize, userId, from, to, 
					tierId, typeId, actionId);
			sql+=	" WHERE " +
					"	row_num > ? " +
					" 	AND row_num <= ? ";					
			ps = con.prepareStatement(sql);
			ps.setLong(1, RewardManager.TASK);
			ps.setLong(2, RewardManager.BENEFIT);
			ps.setLong(3, RewardManager.TASK);
			ps.setLong(4, RewardManager.BENEFIT);
			ps.setLong(5, userId);
			ps.setTimestamp(6, CommonUtil.convertToTimeStamp(CommonUtil.getDateTimeFormat(from, Utils.getAppData().getUtcOffset())));
			ps.setTimestamp(7, CommonUtil.convertToTimeStamp(CommonUtil.getDateTimeFormat(CommonUtil.addDay(to), Utils.getAppData().getUtcOffset())));
			ps.setInt(8, startRow);
			ps.setInt(9, startRow + pageSize);
			rs = ps.executeQuery();	
			while (rs.next()) {
				RewardUserHistory history = new RewardUserHistory();
				history.setReferenceId(rs.getLong("reference_id"));
				history.setRewardActionName(rs.getString("action"));				
				history.setRewardHistoryTypeName(rs.getString("type"));
				history.setRewardTierId(rs.getInt("rwd_tier_id"));
				history.setTimeCreated(convertToDate(rs.getTimestamp("time_created")));
				history.setBalanceExperiencePoints(rs.getDouble("balance_exp_points"));				
				history.setAmountExperiencePoints(rs.getDouble("amount_exp_points"));				
				list.add(history);
			}
		} finally {
			closeStatement(ps);	
			closeResultSet(rs);
		}
		return list;				
	}
	
	/**
	 * Get reward user history query string  
	 * @param con
	 * @param startRow
	 * @param pageSize
	 * @param userId
	 * @param from
	 * @param to
	 * @param tierId
	 * @param typeId
	 * @param actionId
	 * @return
	 */
	public static String getRewardUserHistoryString(Connection con, int startRow, int pageSize, long userId, Date from, Date to, 
			long tierId, long typeId, String actionId) {
		String sql= " SELECT " +
					"	table_data.* " +
					" FROM " +
					"	( " +					
					"	SELECT " +
					"		ruh.reference_id, " +
					"   	CASE " +
					"			WHEN ruh.rwd_action_type_id = ? THEN rtt.name " +
					"   		WHEN ruh.rwd_action_type_id = ? THEN rb.name " +
					"			ELSE '' " +
					"		END as action , " +
					"		rht.name as type, " +
					"		ruh.rwd_tier_id, " +
					"		ruh.time_created, " +
					" 		ruh.balance_exp_points, " +
					"		ruh.amount_exp_points, " +
					"		ruh.rwd_his_type_id, " +
					"		row_number()over(order by ruh.id desc) as row_num " +    		
					" 	FROM " +
					"		rwd_users_history ruh " +
					"		LEFT JOIN rwd_user_tasks rut ON rut.id = ruh.rwd_action_id AND ruh.rwd_action_type_id = ? " + // task
					"	   		LEFT JOIN rwd_tasks rt ON rt.id = rut.rwd_task_id " +
					"	      		LEFT JOIN rwd_task_types rtt ON rtt.id = rt.rwd_task_type_id " +
					"   	LEFT JOIN rwd_user_benefits rub ON rub.id = ruh.rwd_action_id AND ruh.rwd_action_type_id = ? " + //benefit
					"			LEFT JOIN rwd_benefits rb ON rb.id = rub.rwd_benefit_id, " +
					"		rwd_his_types rht " +
					"	WHERE " +
					"		ruh.rwd_his_type_id = rht.id " +
					"		AND ruh.user_id = ? " +
					"		AND ruh.time_created >= ? " +
					"		AND ruh.time_created <= ? ";
		if (tierId > 0) {
			sql += 	" 		AND ruh.rwd_tier_id =  " + tierId;
		}
		if (typeId > 0) {
			sql += 	" 		AND ruh.rwd_his_type_id =  " + typeId;
		}		
		if (actionId != null && !actionId.equals("")) {
			String actionTypeName = actionId.split("_")[0];
			String actionTypeid = actionId.split("_")[1];
			if (actionTypeName.equalsIgnoreCase("task")) {
				sql += " 	AND rtt.id =  " + actionTypeid;
			} else {
				sql += " 	AND rb.id =  " + actionTypeid;
			}
		}																		
		sql += " ) table_data ";
		return sql;
	}
	
	/**
	 * Update rewards tier
	 * @param con
	 * @param rewardTiers
	 * @throws SQLException
	 */
	public static void updateTiers(Connection con, ArrayList<RewardTier> rewardTiers, long writerId) throws SQLException {
		PreparedStatement ps = null;
		int index = 1;
		try {							
			String sql =" UPDATE " +
						"	rwd_tiers rt " +
						" SET " +
						"	rt.writer_id = ?, " +
						"	rt.time_updated = sysdate, " +   
						"	rt.min_exp_points = CASE rt.id " +
						"			WHEN ? then ? ";
						for (int i = 0 ; i < rewardTiers.size(); i++) {
					sql += "		WHEN ? then ? ";
						}
					sql +=" 	END ";					
			ps = con.prepareStatement(sql);
			ps.setLong(index++, writerId);
			ps.setInt(index++, RewardTier.TIER_NOOB_0);
			ps.setInt(index++, 0);				
			for (int i = 0 ; i < rewardTiers.size(); i++) {
				ps.setInt(index++, rewardTiers.get(i).getId());
				ps.setInt(index++, rewardTiers.get(i).getMinExperiencePoints());				
			}			
			ps.executeUpdate();	
		} finally {
			closeStatement(ps);	
		}
	}
	
	
	/**
	 * Get Rewards Users Distribution By Tier
	 * @param con
	 * @return
	 * @throws SQLException
	 */
	public static ArrayList<RewardUserTierDistribution> getRewardsUsersDistributionByTier(Connection con) throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		ArrayList<RewardUserTierDistribution> list = new ArrayList<RewardUserTierDistribution>();
		try {							
			String sql =" SELECT " +
						"	rt.id AS rwd_tier_id, " +
						"   CASE WHEN user_dst.num_users IS NULL THEN 0 ELSE user_dst.num_users END AS num_users " +
						" FROM " +
						"	rwd_tiers rt " +
						"	LEFT JOIN " +
						"			(SELECT " +
						"				ru.rwd_tier_id, " +
						"				COUNT(*) AS num_users " +
						"			 FROM " +
						"			    rwd_users ru " +
						"			 GROUP BY " +
						"				ru.rwd_tier_id  " +
						"			 HAVING " +
						"				ru.rwd_tier_id > 0) user_dst ON rt.id = user_dst.rwd_tier_id " +
						" WHERE " +
						"	rt.id > 0 " +
						" ORDER BY " +
						"	rt.id "; // only real tiers									
			ps = con.prepareStatement(sql);			
			rs = ps.executeQuery();	
			while (rs.next()) {
				RewardUserTierDistribution vo = new RewardUserTierDistribution();
				vo.setTierId(rs.getInt("rwd_tier_id"));
				vo.setNumUsers(rs.getInt("num_users"));
				list.add(vo);
			}
		} finally {
			closeStatement(ps);	
			closeResultSet(rs);
		}
		return list;
	}
	
	/**
	 * Get estimated rewards users 
	 * @param con
	 * @return
	 * @throws SQLException
	 */
	public static ArrayList<RewardUserTierDistribution> getEstimateRewardsUsersDistributionByTier(Connection con, ArrayList<RewardTier> rewardTier) throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		int index = 1;
		ArrayList<RewardUserTierDistribution> list = new ArrayList<RewardUserTierDistribution>();
		try {							
			String sql = Constants.EMPTY_STRING;			
									for (int i = 0; i < rewardTier.size()  ; i++) {
										sql+=" SELECT " +
					rewardTier.get(i).getId() + " AS tier_id, " +
										     "	   count(*) num_users " +
										     " FROM " +
										     "	rwd_users ru " +
										     " WHERE " +
										     "	ru.rwd_tier_id > 0   " +
										     "	AND (ru.EXP_POINTS >= ?  ";
										     if (i < rewardTier.size() - 1) {
										sql+="							 AND ru.EXP_POINTS < ? ";
										     }
										sql+=" ) ";
									     	if (i < rewardTier.size() - 1) {
									    sql+=" UNION ";
									     	}
									}
			ps = con.prepareStatement(sql);
			for (int i = 0; i < rewardTier.size() ; i++) {
				ps.setLong(index++, rewardTier.get(i).getMinExperiencePoints());	
				if (i < rewardTier.size() - 1) {
					ps.setLong(index++, rewardTier.get(i + 1).getMinExperiencePoints());
				}
			}
			rs = ps.executeQuery();	
			while (rs.next()) {
				RewardUserTierDistribution vo = new RewardUserTierDistribution();
				vo.setTierId(rs.getInt("tier_id"));
				vo.setNumUsers(rs.getInt("num_users"));
				list.add(vo);
			}
		} finally {
			closeStatement(ps);	
			closeResultSet(rs);
		}
		return list;	
	}
	
	/**
	 * Count estimated rewards users that we change after tier change
	 * @param con
	 * @return
	 * @throws SQLException
	 */
	public static int countEstimatedRewardsUsersChanged(Connection con, ArrayList<RewardTier> rewardTier) throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		int index = 1;	
		int result = 0;
		try {							
			String sql =" SELECT " +
						"	sum(num_users) as users_changed " +
						" FROM ( ";			
									for (int i = 0; i < rewardTier.size()  ; i++) {										
										sql+=" SELECT " +
											 "	? AS tier_id, " +
										     "	count(*) num_users " +
										     " FROM " +
										     "	rwd_users ru " +
										     " WHERE " +
										     "	ru.rwd_tier_id > 0   " +
										     "  AND ru.rwd_tier_id <> ? " +
										     "	AND (ru.EXP_POINTS >= ?  ";
										     if (i < rewardTier.size() - 1) {
										sql+="							 AND ru.EXP_POINTS < ? ";
										     }
										sql+=" ) ";
									     	if (i < rewardTier.size() - 1) {
									    sql+=" UNION ";
									     	}
									}
				  sql+= " ) ";									
			ps = con.prepareStatement(sql);
			for (int i = 0; i < rewardTier.size() ; i++) {
				ps.setString(index++, String.valueOf(rewardTier.get(i).getId()));
				ps.setInt(index++, rewardTier.get(i).getId());
				ps.setLong(index++, rewardTier.get(i).getMinExperiencePoints());	
				if (i < rewardTier.size() - 1) {
					ps.setLong(index++, rewardTier.get(i + 1).getMinExperiencePoints());
				}
			}
			rs = ps.executeQuery();	
			while (rs.next()) {
				result = rs.getInt("users_changed");
			}
		} finally {
			closeStatement(ps);	
			closeResultSet(rs);
		}
		return result;	
	}	
		
	/**
	 * Check if the document previously approved
	 * @param connection
	 * @param userId
	 * @param fileId
	 * @return true if the document previously approved
	 * @throws SQLException
	 */
	public static boolean isDocPreviouslyApproved(Connection connection, long userId, long fileId) throws SQLException {
		PreparedStatement pstmt = null;
		ResultSet resultSet = null;
		int index = 1;
		try {
			String sql = " SELECT " +
							" ruh.id " +
						" FROM " +
							" rwd_users_history ruh, " +
							" rwd_user_tasks rut, " +
							" rwd_tasks rt " +
						" WHERE " +
							" ruh.rwd_action_id = rut.id " +
							" AND rut.rwd_task_id = rt.id " +
							" AND rt.rwd_task_type_id = ? " +
							" AND ruh.user_id = ? " +
							" AND ruh.reference_id = ? ";
			pstmt = connection.prepareStatement(sql);
			pstmt.setLong(index++, RewardTaskType.REWARD_TASK_TYPE_DOCS);
			pstmt.setLong(index++, userId);
			pstmt.setLong(index++, fileId);
			resultSet = pstmt.executeQuery();
			if (resultSet.next()) {
				return true;
			}
		} finally {
			closeStatement(pstmt);
		}
		return false;
	}
}