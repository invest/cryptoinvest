package il.co.etrader.backend.dao_managers;

import il.co.etrader.backend.bl_vos.StaticLandingPage;
import il.co.etrader.backend.bl_vos.StaticLandingPageLanguage;
import il.co.etrader.backend.bl_vos.StaticLandingPagePath;
import il.co.etrader.bl_vos.MarketingCombination;
import il.co.etrader.util.CommonUtil;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;

import javax.faces.model.SelectItem;

import com.anyoption.common.daos.DAOBase;

/**
 * @author EranL
 *
 */
public class StaticLPDAO extends DAOBase {

	/**
	 * Get all static landing pages
	 * @param con
	 * @param type
	 * @param languageId
	 * @param staticLandingPageId
	 * @return
	 * @throws SQLException
	 */
	public static ArrayList<StaticLandingPageLanguage> getStaticLandigPages(Connection con, long type, long languageId, long staticLandingPageId, 
			long writerIdForSkin, String staticLandingPageName, long staticLandingPagePathId) throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		int index = 1;
		ArrayList<StaticLandingPageLanguage> list = new ArrayList<StaticLandingPageLanguage>();
		try {
			String sql =" SELECT " +
							" slpl.id, " +
							" slp.id as static_lp_id, " +
							" slp.name, " +
							" slpt.name as type, " +
							" slpl.skin_id, " +
							" slpl.language_id, " +
							" l.display_name, " +
							" slp.type as type_id, " +
							" slp.static_landing_page_path_id as path_id, " +
							" slpp.name as path_name " +
						" FROM " +
							" static_landing_pages slp, " +
							" static_landing_page_type slpt, " +
							" static_landing_pages_language slpl, " +
							" static_landing_page_path slpp, " +
							" languages l " +
						" WHERE " +
							" slp.type = slpt.id " +
							" AND slpl.language_id = l.id " +
							" AND slpl.static_landing_page_id = slp.id " +
							" AND slpp.id = slp.static_landing_page_path_id ";
						if (languageId != 0) {
				sql+=		" AND l.id = ? ";
						}
						if (staticLandingPageId != 0) {
				sql+=		" AND slp.id = ? ";
									}
						if (writerIdForSkin != 0) {  // don't check skin in the marketing pages
				sql += 		" AND slpl.skin_id in (select ws.skin_id from writers_skin ws where ws.writer_id = ? ) ";
						}
						if (staticLandingPageName != null) {
				sql += 		" AND UPPER(slp.name) = UPPER(?) ";							
						}
						if (staticLandingPagePathId != 0) {
				sql+=		" AND slp.static_landing_page_path_id = ? ";
						}
				sql+= " ORDER BY " +
							" slpl.id ";

			ps = con.prepareStatement(sql);
			if (languageId != 0) {
				ps.setLong(index++, languageId);
			}
			if (staticLandingPageId != 0) {
				ps.setLong(index++, staticLandingPageId);
			}
			
			if (writerIdForSkin != 0) {
				ps.setLong(index++, writerIdForSkin);
			}
			if (staticLandingPageName != null) {
				ps.setString(index++, staticLandingPageName);
			}
			if (staticLandingPagePathId != 0) {
				ps.setLong(index++, staticLandingPagePathId);
			}
			
			rs = ps.executeQuery();
			while (rs.next()) {
				StaticLandingPageLanguage vo = getVO(rs);
				list.add(vo);
			}
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		return list;
	}

	/**
	 * Get landing pages names
	 * @param con
	 * @return
	 * @throws SQLException
	 */
	public static ArrayList<SelectItem> getStaticLandingPages(Connection con) throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		ArrayList<SelectItem> list = new ArrayList<SelectItem>();
		try {
			String sql =" SELECT " +
							" mlp.name AS name," +
							" slp.id AS id" +
						" FROM " +
							" static_landing_pages slp, " +
							" marketing_landing_pages mlp " +
						" WHERE " +
							" mlp.static_landing_page_id = slp.id " +  
						" ORDER BY " +
							" slp.name ";
			ps = con.prepareStatement(sql);
			rs = ps.executeQuery();
			while (rs.next()) {
				list.add(new SelectItem(rs.getLong("id"), rs.getString("name")));
			}
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		return list;
	}
	
	
	/**
	 * Get landing pages names by Writer SkinID
	 * @param con
	 * @return
	 * @throws SQLException
	 */
	public static ArrayList<SelectItem> getStaticLandingPagesByWriterSkinIdSI(Connection con, long writerIdForSkin) throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		ArrayList<SelectItem> list = new ArrayList<SelectItem>();
		try {
			String sql =" SELECT " +
							" mlp.name AS name," +
							" slp.id AS id" +
						" FROM " +
							" static_landing_pages slp, " +
							" marketing_landing_pages mlp " +
						" WHERE " +
							" mlp.static_landing_page_id = slp.id " +
							" and mlp.writer_id in (select distinct writer_id from " +
												" writers_skin where skin_id in " +
													" (select skin_id from writers_skin where writer_id ="+ writerIdForSkin +")) " +
						" ORDER BY " +
							" slp.name ";
			ps = con.prepareStatement(sql);
			rs = ps.executeQuery();
			while (rs.next()) {
				list.add(new SelectItem(rs.getLong("id"), rs.getString("name")));
			}
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		return list;
	}

	/**
	 * Get landing pages names
	 * @param con
	 * @return
	 * @throws SQLException
	 */
	public static ArrayList<MarketingCombination> getStaticCombination(Connection con, long combId, long skinId, long landingPage, long writerIdForSkin, long fetchCount, long pathId) throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		ArrayList<MarketingCombination> list = new ArrayList<MarketingCombination>();
		try {
			String sql = " SELECT " +
						    	   " slp.name as pageR, " +
						    	   " ca.id as utm_campaign, " +
						    	   " co.skin_id as skinID, " +
						    	   " co.id as combination_id, " +
						    	   " so.name source_name, " +
						    	   " me.name medium_name, " +
						    	   " ct.name content_name, " +
						    	   " mt.name type_name, " +
						    	   " lo.location, " +
						    	   " si.size_vertical, " +
						    	   " si.size_horizontal, " +
						    	   " slp.name as name, " +
						    	   " md.domain " +
						  " FROM " +
					    	" static_landing_pages slp " +
					    		" left join static_landing_pages_language slpl on slpl.STATIC_LANDING_PAGE_ID = slp.id " +
					    		" Left join marketing_landing_pages mlp on mlp.static_landing_page_id = slp.id " +
					    		" left join marketing_combinations co on co.landing_page_id = mlp.id  and co.skin_id = slpl.skin_id " +
					    		" left join marketing_locations lo on co.location_id = lo.id " +
					    		" left join  marketing_types mt  on  co.type_id = mt.id " +
					    		" left join   marketing_sizes si on  co.size_id = si.id " +
					    		" left join   marketing_mediums me on  co.medium_id = me.id " +
					    		" left join marketing_contents ct on  co.content_id = ct.id " +
					    		" left join  marketing_campaigns ca  on  co.campaign_id = ca.id " +
					    		" left join   marketing_sources so on  ca.source_id = so.id " +
								" left join marketing_combination_domains mcd on co.id = mcd.combination_id " +
								" left join  marketing_domains md on mcd.domain_id = md.id " +
					      " WHERE " +
					      	"co.id is not null ";

							if ( skinId > 0 ) {
								sql += "and co.skin_id = " + skinId + " ";
							}

							if ( combId > 0 ) {
								sql += "and co.id = " + combId + " ";
							}

							if ( landingPage > 0 ) {
								sql += "and slp.id = " + landingPage + " ";
							}

							if (writerIdForSkin > 0) {
								sql += 	" AND co.skin_id in (select ws.skin_id from writers_skin ws where ws.writer_id = " + writerIdForSkin+ ") ";
							}
							
							if (pathId > 0) {
								sql += " and slp.static_landing_page_path_id = " + pathId + " ";
							}

					    	sql += " order by co.id desc";
					    	
					    	if (fetchCount > 0){
					    	    sql = rowLimit(sql, fetchCount);   
					    	}		    	
			ps = con.prepareStatement(sql);
			rs = ps.executeQuery();
			while (rs.next()) {
				MarketingCombination vo = new MarketingCombination();
				vo.setId(rs.getLong("combination_id"));
				vo.setSkinId(rs.getLong("skinID"));
				vo.setCampaignId(rs.getLong("utm_campaign"));
				vo.setLandingPageUrl(rs.getString("pageR"));
				vo.setSourceName(rs.getString("source_name"));
				vo.setMediumName(rs.getString("medium_name"));
				vo.setContentName(rs.getString("content_name"));
				vo.setTypeName(rs.getString("type_name"));
				vo.setLocation(rs.getString("location"));
				vo.setVerticalSize(rs.getString("size_vertical"));
				vo.setHorizontalSizel(rs.getString("size_horizontal"));
				vo.getMarketingDomain().setDomain(rs.getString("domain"));
				list.add(vo);
			}
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		return list;
	}

	/**
	 * @param rs
	 * @return
	 * @throws SQLException
	 */
	protected static StaticLandingPageLanguage getVO(ResultSet rs) throws SQLException {
		StaticLandingPageLanguage vo = new StaticLandingPageLanguage();
		vo.setStaticLandingPageId(rs.getLong("STATIC_LP_ID"));
		vo.setId(rs.getLong("ID"));
		vo.setName(rs.getString("NAME"));
		vo.setType(rs.getString("TYPE"));
		vo.setLanguage(CommonUtil.getMessage(rs.getString("DISPLAY_NAME"), null));
		vo.setLanguageId(rs.getLong("LANGUAGE_ID"));
		vo.setSkinId(rs.getLong("SKIN_ID"));
		vo.setTypeId(rs.getLong("TYPE_ID"));
		StaticLandingPagePath slpp = new StaticLandingPagePath();
		slpp.setId(rs.getLong("PATH_ID"));
		slpp.setName(rs.getString("path_name"));
		vo.setStaticLandingPagePath(slpp);
		return vo;
	}
	
	/**
	 * Get static landing pages types
	 * @param con
	 * @return
	 * @throws SQLException
	 */
	public static ArrayList<SelectItem> getStaticLandingPageTypesSI(Connection con) throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		ArrayList<SelectItem> list = new  ArrayList<SelectItem>();
		try {
			String sql =" SELECT " +
		    			"	* " +
		    			" FROM " +
		    			"	static_landing_page_type ";
		    ps = con.prepareStatement(sql);
			rs = ps.executeQuery();
			while ( rs.next() ) {
				list.add(new SelectItem(rs.getLong("id"), rs.getString("name")));
				}
			} finally {
				closeStatement(ps);
				closeResultSet(rs);
			}
		return list;
	}
	
	/**
	 * Insert Static Landing Page
	 * @param con
	 * @param slp
	 * @throws SQLException
	 */
	public static void insertStaticLandingPage(Connection con, StaticLandingPage slp) throws SQLException {
		  PreparedStatement ps = null;
		  try {
				String sql =
					"INSERT INTO static_landing_pages " +
						"(id, name, type, writer_id, time_created, static_landing_page_path_id) " +
					"VALUES" +
						"(seq_static_lp.NEXTVAL, ?, ?, ?, sysdate, ?) ";
				ps = con.prepareStatement(sql);
				ps.setString(1, slp.getName());
				ps.setLong(2, slp.getTypeId());
				ps.setLong(3, slp.getWriterId());
				ps.setLong(4, slp.getStaticLandingPagePathId());
				ps.executeUpdate();
				slp.setId(getSeqCurValue(con, "seq_static_lp"));
		  } finally	{
				closeStatement(ps);
		  }
	}
	
	/**
	 * Insert Static Landing Page Language
	 * @param con
	 * @param slpl
	 * @throws SQLException
	 */
	public static void insertStaticLandingPageLanguage(Connection con, StaticLandingPageLanguage slpl) throws SQLException {
		  PreparedStatement ps = null;
		  try {
				String sql =
					"INSERT INTO static_landing_pages_language " +
						"(id, static_landing_page_id, language_id, skin_id, writer_id, time_created) " +
					"VALUES" +
						"(seq_static_lp_lang.NEXTVAL, ?, ?, ?, ?, sysdate) ";
				ps = con.prepareStatement(sql);
				ps.setLong(1, slpl.getStaticLandingPageId());
				ps.setLong(2, slpl.getLanguageId());
				ps.setLong(3, slpl.getSkinId());
				ps.setLong(4, slpl.getWriterId());
				ps.executeUpdate();
				slpl.setId(getSeqCurValue(con, "seq_static_lp_lang"));
		  } finally	{
				closeStatement(ps);
		  }
	}
	
	/**
	 * Get Static Landing Page URL
	 * @param con
	 * @return HashMap<Long, StaticLandingPagePath>
	 * @throws SQLException
	 */
	public static HashMap<Long, StaticLandingPagePath> getStaticLandingPagePath(Connection con) throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		HashMap<Long, StaticLandingPagePath> hm = new HashMap<Long, StaticLandingPagePath>();
		try {
			String sql = "SELECT " +
							" * " +
						" FROM " +
							" static_landing_page_path slpp " +
						" ORDER BY " +
							" slpp.id";
			ps = con.prepareStatement(sql);
			rs = ps.executeQuery();
			while (rs.next()) {
				StaticLandingPagePath vo = new StaticLandingPagePath();
				vo.setId(rs.getLong("id"));
				vo.setName(rs.getString("name"));
				vo.setFolderName(rs.getString("folder_name"));
				vo.setRegulated(rs.getInt("is_regulated") == 1 ? true : false);
				hm.put(vo.getId(), vo);
			}
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		return hm;
	}
	
	/**
	 * Get static lp names SI
	 * @param con
	 * @param landingPageType
	 * @param pathId
	 * @return ArrayList<SelectItem>
	 * @throws SQLException
	 */
	public static ArrayList<SelectItem> getStaticLpNamesSI(Connection con, long landingPageType, long pathId) throws SQLException {
		  ArrayList<SelectItem> list = new ArrayList<SelectItem>();
		  PreparedStatement ps = null;
		  ResultSet rs = null;
		  try {
			  String sql =  " SELECT " +
					  			" slp.id, " +
					  			" slp.name " +
					  		" FROM " +
					  			" static_landing_pages slp " +
							" WHERE " +
								" 1 = DECODE(?, 0, 1, " +
													" slp.static_landing_page_path_id, 1, 0)" + 
							" ORDER BY " +
								" upper(slp.name) ";
			  int index = 1;
			  ps = con.prepareStatement(sql);
			  ps.setLong(index++, pathId);
			  rs = ps.executeQuery();
			  while (rs.next()) {
				  list.add(new SelectItem(new Long(rs.getLong("id")), rs.getString("name")));
			  }
		  } finally {
			  closeResultSet(rs);
			  closeStatement(ps);
		  }
		  return list;
	  }
}
