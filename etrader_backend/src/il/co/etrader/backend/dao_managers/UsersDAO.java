
package il.co.etrader.backend.dao_managers;
import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.bouncycastle.crypto.CryptoException;

import com.anyoption.backend.service.userfields.Address;
import com.anyoption.common.beans.base.Currency;
import com.anyoption.common.beans.base.PendingUserWithdrawalsDetailsBase;
import com.anyoption.common.beans.base.Skin;
import com.anyoption.common.beans.base.UsersDetailsHistory;
import com.anyoption.common.daos.CurrenciesDAOBase;
import com.anyoption.common.managers.CountryManagerBase;
import com.anyoption.common.managers.SkinsManagerBase;
import com.anyoption.common.util.AESUtil;

import il.co.etrader.backend.bl_managers.InvestmentsManager;
import il.co.etrader.backend.bl_managers.TransactionsManager;
import il.co.etrader.backend.bl_vos.TCAccessToken;
import il.co.etrader.backend.bl_vos.TCAccessTokenSite;
import il.co.etrader.backend.bl_vos.User;
import il.co.etrader.backend.bl_vos.UserDetailsHistoryChanges;
import il.co.etrader.backend.bl_vos.UserRegulationHistory;
import il.co.etrader.backend.util.Constants;
import il.co.etrader.backend.util.Utils;
import il.co.etrader.bl_managers.TransactionsManagerBase;
import il.co.etrader.bl_vos.DeviceUniqueIdSkin;
import il.co.etrader.bl_vos.UserBase;
import il.co.etrader.dao_managers.TiersDAOBase;
import il.co.etrader.dao_managers.UsersDAOBase;
import il.co.etrader.util.CommonUtil;
import il.co.etrader.util.ConstantsBase;
import oracle.jdbc.OracleTypes;

public class UsersDAO extends UsersDAOBase{

	private static final Logger logger = Logger.getLogger(UsersDAOBase.class);
	
	private static final String USER_HIST_FIELDS = "(1,2,3,4,5,6,7,8,9,17,18,19,20,20,32,33,34,35,36,37,38,39)";
	private static final long COUNTRY_FIELD = 9;
	private static final long SKIN_FIELD = 19;

  	public static ArrayList getUsersList(Connection con, String userName,String fName,
  			String lName,String idNum,String email,Long userId, String phone)  throws SQLException{

  		  ArrayList list = new ArrayList();
  		  PreparedStatement ps = null;
		  ResultSet rs = null;
  
		  try {

			    String sql="SELECT * FROM users u, users_active_data uad WHERE u.id = uad.user_id ";

			    if (userName!=null && !userName.equals("")) {
			    	sql+=" UPPER(u.user_name) LIKE '%"+userName.toUpperCase()+"%' AND";
			    }

			    if (fName!=null && !fName.equals("")) {
			    	sql+=" UPPER(u.first_name) LIKE '%'||?||'%' AND";
			    }

			    if (lName!=null && !lName.equals("")) {
			    	sql+=" UPPER(u.last_name) LIKE '%'||?||'%' AND";
			    }

			    if (idNum!=null && !idNum.equals("")) {
			    	sql+=" u.id_num LIKE '%" + AESUtil.encrypt(idNum) + "%' AND";
			    }

			    if (email!=null && !email.equals("")) {
			    	sql+=" UPPER(u.email) LIKE '%"+email.toUpperCase()+"%' AND";
			    }

			    if (userId!=null && !userId.equals("")) {
			    	sql+=" UPPER(u.id) = "+userId+" AND";
			    }

			    if (phone!=null && !phone.equals("")) {
			    	sql+=" UPPER(u.mobile_phone) LIKE '%" +phone+"%' OR UPPER(u.land_line_phone) LIKE '%" +phone+"%'";
			    }

			    if (sql.endsWith("WHERE")) {
			    	sql=sql.substring(0,sql.length()-5);
			    }

			    if (sql.endsWith("AND")) {
			    	sql=sql.substring(0,sql.length()-3);
			    }

			    sql+=" ORDER BY u.user_name ";

			    ps = con.prepareStatement(sql);

			    if (fName!=null && !fName.equals("")) {
			    	ps.setString(1, fName.toUpperCase());

			    	if (lName!=null && !lName.equals("")) {
			    		ps.setString(2, lName.toUpperCase());
			    	}
			    } else
			    	if (lName!=null && !lName.equals("")) {
			    		ps.setString(1, lName.toUpperCase());
			    	}
				rs=ps.executeQuery();

				while (rs.next()) {
					User vo=new User(false);
					getVO(con,rs,vo);
					list.add(vo);

				}
		} catch (	CryptoException | InvalidKeyException | NoSuchAlgorithmException | NoSuchPaddingException | IllegalBlockSizeException
					| BadPaddingException | UnsupportedEncodingException | InvalidAlgorithmParameterException ce) {
			throw new SQLException(ce.getMessage());
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}

			return list;
	  }
  	
  	
	public static ArrayList<User> getUsersListWithContactID(Connection con, Long writerId, String userName,String fName,
  			String lName,String idNum,String email,Long userId, String phone,Long contactId,String mobile, ArrayList<String> countries, ArrayList<String> skinId, int firstRow, int rowsPerPage)  throws SQLException{

		ArrayList<User> list = new ArrayList<User>();
        Statement ps = null;
        ResultSet rs = null;

        try {
            String sql = sqlListWithContactID(false, writerId, userName, fName,
          			 lName, idNum, email, userId,  phone, contactId, mobile, countries, skinId,  firstRow,  rowsPerPage);

            ps = con.prepareStatement(sql);
            rs = ps.executeQuery(sql);

            while (rs.next()) {
				User vo=new User(false);
				getAdvancedSearchVO(con,rs,vo);
				list.add(vo);
            }

        } finally {
            closeStatement(ps);
            closeResultSet(rs);
        }
        return list;
	  }
	public static String sqlListWithContactID(boolean count, Long writerId, String userName,String fName,
  			String lName,String idNum,String email,Long userId, String phone,Long contactId,String mobile, ArrayList<String> countries, ArrayList<String> skinId, int firstRow, int rowsPerPage) throws SQLException{

  		  String sql = "";
		  String skin = skinId.toString().replace("[", "").replace("]", "");
		  String country = countries.toString().replace("[", "").replace("]", "");
		  if (firstRow>-1){
			  firstRow+=1;
		  }
		  boolean paging = firstRow > -1 && rowsPerPage > -1;
		  
		  try {
		
	        if(paging) {
	            sql += "SELECT * FROM ( SELECT a.*, ROWNUM rnum FROM  ( ";
	        }
	        sql += 
	                "SELECT ";
	                if(count){
	                    sql += " COUNT(*) AS rowCount ";
	                } else {
	                    sql += " * ";
	                }

			  	sql += "FROM ( SELECT DISTINCT * FROM (" +
								"SELECT " +
								"u.*" +
								",uad.copyop_user_status" +
								",uad.status_update_date" +
								",uad.aff_sub1" +
								",uad.aff_sub2" +
								",uad.aff_sub3" +
								",uad.bubbles_pricing_level" +
								",uad.has_bubbles" +
								",uad.has_dynamics" +
								",uad.is_non_reg_suspend " +
								",NVL(u.first_name,c.first_name)first_name_uc" +
								",NVL(u.last_name,c.last_name)last_name_uc" +
								",NVL(u.email,c.email)email_uc" +
								",NVL(u.mobile_phone,c.mobile_phone)mobile_uc" +
								",NVL(u.land_line_phone,c.land_line_phone)phone_uc" +
								",NVL(u.skin_id,c.skin_id)skin_id_uc" +
								",NVL(u.country_id,c.country_id) country_id_uc" +
								",c.id con_id" +
								",DECODE(u.id,NULL,'Contact','User') uc" +
								",DECODE(u.id,NULL,c.is_contact_by_email,u.is_contact_by_email) contact_By_Email" +
								",c.skin_id c_skin_id" +
								",c.country_id c_country_id " +
								 ",uad.is_need_change_password " +
								"FROM users u " +
								"LEFT JOIN contacts c " +
								"ON c.user_id = u.id " +
								"INNER JOIN users_active_data uad " +
								"ON u.id = uad.user_id " +
								"where";
					
					if (fName!=null && !fName.equals("")) {
						sql+=" upper(u.first_name) LIKE '" + fName.toUpperCase() + "%' and";
					}
					
					if (lName!=null && !lName.equals("")) {
				    	sql+=" upper(u.last_name) LIKE '" + lName.toUpperCase() + "%' and";
				    }
					
					if (email!=null && !email.equals("")) {
				    	sql+=" upper(u.email) LIKE '" + email.toUpperCase() + "%' and";
				    }
					
					if (phone!=null && !phone.equals("")) {
				    	sql+=" u.land_line_phone LIKE '" + phone + "%' and";
				    }
					
					if (mobile!=null && !mobile.equals("")) {
				    	sql+=" u.mobile_phone LIKE '" + mobile + "%'";
				    }
					
					if (sql.endsWith("where")) {
				    	sql=sql.substring(0,sql.length()-5);
				    }

				    if (sql.endsWith("and")) {
				    	sql=sql.substring(0,sql.length()-3);
				    }
					
								sql+=" UNION ALL " +
								"SELECT /*+INDEX (c IDX_LAST_NAME_CON IDX_FIRST_NAME_CON IDX_EMAIL_CON)*/ u.* " +
								",uad.copyop_user_status " +
								",uad.status_update_date" +
								",uad.aff_sub1" +
								",uad.aff_sub2" +
								",uad.aff_sub3" +
								",uad.bubbles_pricing_level" +
								",uad.has_bubbles" +
								",uad.has_dynamics" +
								",uad.is_non_reg_suspend " +
								",NVL(u.first_name,c.first_name)first_name_uc " +
								",NVL(u.last_name,c.last_name)last_name_uc " +
								",NVL(u.email,c.email)email_uc " +
								",NVL(u.mobile_phone,c.mobile_phone)mobile_uc " +
								",NVL(u.land_line_phone,c.land_line_phone)phone_uc " +
								",NVL(u.skin_id,c.skin_id)skin_id_uc " +
								",NVL(u.country_id,c.country_id) " +
								",c.id con_id " +
								",DECODE(u.id,NULL,'Contact','User') uc " +
								",DECODE(u.id,NULL,c.is_contact_by_email,u.is_contact_by_email) contact_By_Email " +
								",c.skin_id c_skin_id " +
								",c.country_id c_country_id " +
								",uad.is_need_change_password " +
								"FROM users u " +
								"RIGHT JOIN contacts c " +
								"ON c.user_id = u.id " +
								"INNER JOIN users_active_data uad " +
								"ON u.id = uad.user_id " +
								"where";
					
					if (fName!=null && !fName.equals("")) {
						sql+=" upper(u.first_name) LIKE '" + fName.toUpperCase() + "%' and";
					}
					
					if (lName!=null && !lName.equals("")) {
						sql+=" upper(u.last_name) LIKE '" + lName.toUpperCase() + "%' and";
					}
					
					if (email!=null && !email.equals("")) {
						sql+=" upper(u.email) LIKE '" + email.toUpperCase() + "%' and";
					}
					
					if (phone!=null && !phone.equals("")) {
						sql+=" u.land_line_phone LIKE '" + phone + "%' and";
					}
					
					if (mobile!=null && !mobile.equals("")) {
						sql+=" u.mobile_phone LIKE '" + mobile + "%'";
					}
					
					if (sql.endsWith("where")) {
						sql=sql.substring(0,sql.length()-5);
					}
					
					if (sql.endsWith("and")) {
						sql=sql.substring(0,sql.length()-3);
					}

					sql+=") where 1 = 1 " ;
					//	 commented due dev - 2768		
					//"  skin_id in (select ws.skin_id from WRITERS_SKIN ws where ws.writer_id = "+writerId+") and";
							
					if (userName!=null && !userName.equals("")) {
						sql+=" and upper(user_name) LIKE '"+userName.toUpperCase()+"%' ";
					}
					
					if (idNum!=null && !idNum.equals("")) {
				    	sql+=" and id_num = '" + AESUtil.encrypt(idNum) + "' ";
				    }
					
					if (!country.equals("") && !country.equals("0")) {
				    	sql += " and NVL(country_id,c_country_id) IN (" + country + ") "; 
				    }
					
					if(!skin.equals("") && !skin.equals("0")) {
				    	sql += " and NVL(skin_id,c_skin_id) IN ("+ skin + ") " ;
				    }
					
					if (userId!=null && !userId.equals("")) {
				    	sql+=" and id = "+userId ;
				    }
					
					if (contactId!=null && !contactId.equals("")) {
				    	sql+=" and con_id = "+contactId;
				    }

				    if (sql.endsWith("and")) {
				    	sql=sql.substring(0,sql.length()-3);
				    }
				    
					sql+=" ORDER BY user_name)";
					
					if(paging) {
	                    sql +=") a where rownum < " + (firstRow+rowsPerPage) + " ) where rnum  >=" + firstRow;
	                }   
					
		} catch (	CryptoException | InvalidKeyException | NoSuchAlgorithmException | NoSuchPaddingException | IllegalBlockSizeException
					| BadPaddingException | UnsupportedEncodingException | InvalidAlgorithmParameterException ce) {
			// FIXME very bad way of handling exceptions
			throw new SQLException(ce.getMessage());
		}
									
			return sql;
	  }
	
	public static int getUsersListWithContactIDCount(Connection con, Long writerId,  String userName,String fName,
  			String lName,String idNum,String email,Long userId, String phone,Long contactId,String mobile, ArrayList<String> countries, ArrayList<String> skinId)  throws SQLException{

		 int count = 0;
	        Statement ps = null;
	        ResultSet rs = null;

	        try {
	            String sql = sqlListWithContactID(true, writerId, userName, fName,
	          			 lName, idNum, email, userId,  phone, contactId, mobile, countries, skinId,  -1,  -1);

	            ps = con.prepareStatement(sql);
	            rs = ps.executeQuery(sql);

	            if (rs.next()) {
	                return rs.getInt("rowCount");
	            }

	        } finally {
	            closeStatement(ps);
	            closeResultSet(rs);
	        }
	        return count;
	  }

	  public static User getById(Connection con,long id, String writerSkinsToString )  throws SQLException{

		  PreparedStatement ps=null;
		  ResultSet rs=null;

		  User vo=null;

		  try
			{
			    String sql="SELECT * FROM users u, users_active_data uad WHERE u.id = uad.user_id AND u.id=?";

			    sql+=" AND skin_id IN (" + writerSkinsToString + ")";

				ps = con.prepareStatement(sql);
				ps.setLong(1, id);

				rs=ps.executeQuery();

				if (rs.next()) {
					vo=new User(false);
					getVO(con,rs,vo);
					vo.setCurrency(CurrenciesDAOBase.getById(con, vo.getCurrencyId().intValue()));

				}
			}

			finally	{
				closeResultSet(rs);
				closeStatement(ps);
			}
			return vo;
	  }

	  public static User getById(Connection con,long id)  throws SQLException{

		  PreparedStatement ps=null;
		  ResultSet rs=null;

		  User vo=null;

		  try
			{
			    String sql="select * from users u, users_active_data uad where u.id = uad.user_id AND u.id=?";

			    sql+=" AND skin_id in (" + Utils.getWriter().getSkinsToString() + ")";

				ps = con.prepareStatement(sql);
				ps.setLong(1, id);

				rs=ps.executeQuery();

				if (rs.next()) {
					vo=new User(false);
					getVO(con,rs,vo);
					vo.setCurrency(CurrenciesDAOBase.getById(con, vo.getCurrencyId().intValue()));

				}
			}

			finally	{
				closeResultSet(rs);
				closeStatement(ps);
			}
			return vo;
	  }

	  public static User getByUserName(Connection con,String name)  throws SQLException{

		  PreparedStatement ps=null;
		  ResultSet rs=null;

		  User vo=null;

		  FacesContext context=FacesContext.getCurrentInstance();

		  try
			{
			  		String sql="SELECT * FROM users u, users_active_data uad where u.id = uad.user_id AND u.user_name=upper(?)";

			    	sql+=" AND skin_id IN (" + Utils.getWriter().getSkinsToString() + ")";

					ps = con.prepareStatement(sql);
					ps.setString(1, name);

					rs=ps.executeQuery();

					if (rs.next()) {
						vo=new User(false);
						getVO(con,rs,vo);
					}
				}

				finally
				{
					closeResultSet(rs);
					closeStatement(ps);
				}
				return vo;

	  }
	  
	  public static User getUserByOnlyUserName(Connection con, String name)  throws SQLException{
		  PreparedStatement ps=null;
		  ResultSet rs=null;
		  User vo=null;

		  try
			{
			  		String sql="SELECT * FROM users u, users_active_data uad where u.id = uad.user_id AND u.user_name=upper(?)";
					ps = con.prepareStatement(sql);
					ps.setString(1, name);

					rs=ps.executeQuery();

					if (rs.next()) {
						vo=new User(false);
						getVO(con,rs,vo);
					}
				}
				finally
				{
					closeResultSet(rs);
					closeStatement(ps);
				}
				return vo;
	  }
	  	  

	  /**
	   * This method returns the number of times the user has used a next invest on us benefit
	   * @param con - connection
	   * @param userId  -the user id
	   * @return the number of times the user has used a next invest on us benefit
	   * @throws SQLException
	   */
	  public static int getNumOfBonus(Connection con,int userId,boolean isBonusUsed ) throws SQLException {

		  int res = 0;
		  PreparedStatement ps = null;
		  ResultSet rs = null;

		  try {
			    String sql1 =
			    	"SELECT " +
			    		"COUNT(*) count " +
			    	"FROM  " +
			    		"bonus b, " +
			    		"bonus_users bu, " +
			    		"bonus_states bs " +
   		            "WHERE " +
   		            		"b.id=bu.bonus_id " +
		                    "AND bu.user_id= ? " +
		                    "AND bs.id = bu.bonus_state_id ";

		                    if(isBonusUsed) {
		                    sql1 += "AND (bs.id = ? OR bs.id = ? )";
		                    }


				ps = con.prepareStatement(sql1);
				ps.setInt(1, userId);

				if (isBonusUsed) {
				ps.setLong(2,ConstantsBase.BONUS_STATE_USED);
				ps.setLong(3, ConstantsBase.BONUS_STATE_DONE);
				}

				rs=ps.executeQuery();

				if (rs.next()) {
					res=rs.getInt("count");
				}
			}

			finally
			{
				closeResultSet(rs);
				closeStatement(ps);
			}
			return res;
	  }

	  /**
	   * This method overrides UsersDAOBase.getByUserName
	   * @param con - DB connection
	   * @param name - user Name
	   * @param vo - the user object
	   * @return - success/fail
	   * @throws SQLException
	   */
	  public static boolean getByUserName(Connection con, String name, UserBase vo) throws SQLException {
		  	FacesContext context=FacesContext.getCurrentInstance();
			if (name == null || name.trim().equals(""))
				return false;
			PreparedStatement ps = null;
			ResultSet rs = null;

			try {
				String sql = "SELECT " +
								"u.*, uad.*, " +
								"CASE WHEN EXISTS ( " +
                                                      "	SELECT " +
                                                      "		1 " +
                                                      "	FROM " +
                                                      " 	user_market_disable umd " +
                                                      " WHERE " +
                                                      "     umd.is_active = 1 AND " +
                                                      "     umd.is_dev3 = 1 AND " +
                                                      "     umd.start_date <= sysdate AND " +
                                                      "     umd.end_date > sysdate AND " +
                                                      "     umd.user_id = u.id ) " +
                                "THEN 1 ELSE 0 END AS userDisable " +
							"FROM " +
								"users u, users_active_data uad " +
							"WHERE " +
								"u.id = uad.user_id " +
								"AND u.user_name = ? " +
					       		 "AND u.skin_id IN (" + Utils.getWriter().getSkinsToString() + ")";

				ps = con.prepareStatement(sql);

				ps.setString(1, name.toUpperCase());
				rs = ps.executeQuery();

				if (rs.next()) {
					getVO(con,rs,vo);
//					vo.setDeviceFamily(rs.getLong("device_family"));
//					vo = setDeviceFamily(con, vo.getId(), vo);
					vo.setLastLoginId(rs.getLong("last_login_id"));
					vo.setDecline(rs.getLong("is_declined") == 1 ? true : false);
					vo.setLastDeclineDate(convertToDate(rs.getTimestamp("last_decline_date")));
					long tierUserId = rs.getLong("tier_user_id");
					if (tierUserId > 0) {
						vo.setTierUser(TiersDAOBase.getTierUserById(con, tierUserId));
					} else {
						vo.setTierUser(null);
					}
					vo.setUserDisable(rs.getLong("userDisable") == 1 ? true : false);
					return true;

				}
			}

			finally {
				closeResultSet(rs);
				closeStatement(ps);
			}

			return false;
		}

	  /**
	   *
	   * @param con - DB connection
	   * @param name - user Name
	   * @param id - user Id
	   * @param vo - the user object
	   * @return - success/fail
	   * @throws SQLException
	   */
	  public static boolean getByUserNameOrId(Connection con, String name, Long id, String email, UserBase vo, Date arabicStartDate) throws SQLException {
			if ((name == null || name.trim().equals("")) && id == null &&(email == null || email.trim().equals(""))) {
				return false;
			}
			PreparedStatement ps = null;
			ResultSet rs = null;
			int pIndex = 1;
			long userId = 0;
			String userName = "";

			if (id != null) {
				userId = id.longValue();
			}
			if (name != null) {
				userName = name.toUpperCase();
			}

			try {
				String sql = " select u.*, uad.*, " +
							 "		CASE WHEN EXISTS ( " +
                                                      "	SELECT " +
                                                      "		1 " +
                                                      "	FROM " +
                                                      " 	user_market_disable umd " +
                                                      " WHERE " +
                                                      "     umd.is_active = 1 AND " +
                                                      "     umd.is_dev3 = 1 AND " +
                                                      "     umd.start_date <= sysdate AND " +
                                                      "     umd.end_date > sysdate AND " +
                                                      "     umd.user_id = u.id ) " +
                                   " THEN 1 ELSE 0 END AS userDisable " +
							 " from users u, users_active_data uad " +
							 " WHERE u.id = uad.user_id " +
							 " AND u.skin_id in (select ws.skin_id " +
							 					 " from WRITERS_SKIN ws " +
							 					 " where ws.writer_id = ?) ";// +
//							       " and (u.user_name = ? or u.id = ? or UPPER(u.email) = ? ) ";
				if (name != null && name.trim().length() > 0) {
					sql += " and u.user_name = ? ";
				} else if (id != null) {
					sql += " and u.id = ? ";
				} else {
					sql += " and UPPER(u.email) = ? ";
				}

	            if(null != arabicStartDate){
					sql += " and u.time_created >= ? ";
				}

				ps = con.prepareStatement(sql);
				ps.setLong(pIndex++, Utils.getWriter().getWriter().getId());
				if (name != null && name.trim().length() > 0) {
					ps.setString(pIndex++, userName);
				} else if (id != null) {
					ps.setLong(pIndex++, userId);
				} else {
					if(email != null) {
						email = email.toUpperCase();
					}
					ps.setString(pIndex++, email);
				}

				if (null != arabicStartDate) {
					ps.setTimestamp(pIndex++, CommonUtil.convertToTimeStamp(CommonUtil.getDateTimeFormat(arabicStartDate , Utils.getAppData().getUtcOffset())));
				}
				rs = ps.executeQuery();
				boolean retval = false;
				if (rs.next()) {
					getVO(con,rs,vo);
//					vo = setDeviceFamily(con, vo.getId(), vo);
//					vo.setDeviceFamily(rs.getLong("device_family"));
					vo.setLastLoginId(rs.getLong("last_login_id"));
					vo.setDecline(rs.getLong("is_declined") == 1 ? true : false);
					vo.setLastDeclineDate(convertToDate(rs.getTimestamp("last_decline_date")));
					long tierUserId = rs.getLong("tier_user_id");
					if (tierUserId > 0) {
						vo.setTierUser(TiersDAOBase.getTierUserById(con, tierUserId));
					} else {
						vo.setTierUser(null);
					}
					vo.setUserDisable(rs.getLong("userDisable") == 1 ? true : false);
					retval = true;
					
					vo.setSentDepDocumentsWriterId(rs.getLong("sent_dep_documents_writer_id"));
					vo.setSentDepDocumentsDateTime(convertToDate(rs.getTimestamp("sent_dep_documents_date_time")));
					vo.setVipStatusId(rs.getInt("vip_status_id"));
				}

				if (rs.next()) {
					vo.setUnique(false);
				} else {
					vo.setUnique(true);
				}
				
				return retval;
			}

			finally {
				closeResultSet(rs);
				closeStatement(ps);
			}
		}


	  /**
	   * Get all users with locked accounts,
	   * this accounts locked because they needed to send us documents for deposit
	   * @param con
	   * 	Db connection
	   * @return
	   * 	list of users
	   * @throws SQLException
	   */
	   public static ArrayList getLockedAccounts(Connection con)  throws SQLException {

	  		  ArrayList list = new ArrayList();
	  		  PreparedStatement ps = null;
			  ResultSet rs = null;

			  try
				{
				    String sql = "select id, user_name from users " +
				    		     "where is_active = 0 and id_doc_verify = 0 " +
				    		     "and docs_required = 1 " +
				    		     "order by user_name ";


				    ps = con.prepareStatement(sql);
					rs = ps.executeQuery();

					while (rs.next()) {
						User vo = new User(false);
						vo.setId(rs.getLong("id"));
						vo.setUserName(rs.getString("user_name"));
						list.add(vo);
					}
				}

				finally
				{
					closeResultSet(rs);
					closeStatement(ps);
				}

				return list;
		  }

	   /**
	    * Unlock user.
	    * this user was locked because he needed to send us documents for deposit
	    * to unlock the user :	set is_active true, id_doc_verify true ( we get his documents )
	    * 						and docs_required to false.
	    *
	    * @param conn
	    * 		Db connection
	    * @param userId
	    * 		user id for unlock
	    * @throws SQLException
	    */
	    public static void unlockUser(Connection conn, long userId, boolean activate) throws SQLException {
	        PreparedStatement pstmt = null;
	        try {
	            String sql = "update users " +
	                		 "set " +
	                		 "id_doc_verify = 1,  ";
	                		 if (activate) {	                			 
	                			 sql = sql + "is_active = 1,  ";
	                		 }
	                		 sql = sql +  "docs_required = 0  " +
	                		 "where id = ? ";

	            pstmt = conn.prepareStatement(sql);
	            pstmt.setLong(1, userId);
	            pstmt.executeUpdate();

	        } finally {
	            closeStatement(pstmt);
	        }
	    }


	    /**
	     * Get risky users list
	     * @param conn
	     * 		db connection
	     * @return
	     * 		list of all risky users
	     * @throws SQLException
	     */
	    public static ArrayList<SelectItem> getRiskyUsers(Connection conn) throws SQLException {

	    	PreparedStatement pstmt = null;
	        ResultSet rs = null;
	        ArrayList<SelectItem> list = new ArrayList<SelectItem>();

	        try {

	            String sql = "SELECT " +
								"u.id || '_0' as id, " +
								"u.user_name as user_name " +
							"FROM " +
						 		"users u " +
						 	"WHERE " +
					 			"u.is_risky = 1 " +
					 		"UNION " +
					 		"SELECT " +
					 			"u.id || '_' || apu.id as id, " +
								"apu.reference || '(' || u.user_name || ')' as user_name " +
							"FROM " +
								"api_external_users apu, " +
								"users u " +
							"WHERE " +
								"u.id = apu.user_id AND " +
								"apu.is_risky = 1 " +
							"ORDER BY " +
								"user_name ";

	            pstmt = conn.prepareStatement(sql);
				rs = pstmt.executeQuery();

				while (rs.next()) {
					list.add(new SelectItem(rs.getString("id"), rs.getString("user_name")));
				}

	        } finally {
	            closeStatement(pstmt);
	        }

	        return list;
	    }

	    /**
	     * is have deposits
	     * @param con db connection
	     * @param userId user id
	     * @return
	     * @throws SQLException
	     */
	    public static boolean isHaveDeposits(Connection con, long userId) throws SQLException {

	    	PreparedStatement ps = null;
	    	ResultSet rs = null;

	    	try {

	    		String sql = "select u.id from users u, transactions t, transaction_types tt " +
	    					 "where u.id = t.user_id and t.type_id = tt.id " +
	    					 "and tt.class_type = ? and u.id = ? ";

	    		ps = con.prepareStatement(sql);
	    		ps.setLong(1, TransactionsManagerBase.TRANS_CLASS_DEPOSIT);
	    		ps.setLong(2, userId);

	    		rs = ps.executeQuery();

	    		if ( rs.next() ) {
	    			return true;
	    		}
	    	} finally {
	    		closeResultSet(rs);
	    		closeStatement(ps);
	    	}

	    	return false;
	    }

	    /**
	     * Is user have bonus
	     * @param con db connection
	     * @param userId user id
	     * @return
	     * @throws SQLException
	     */
		public static boolean isHaveBonus(Connection con, long userId) throws SQLException {
			PreparedStatement ps = null;
			ResultSet rs = null;

			try	{
			    String sql = "SELECT bu.id " +
			    			 "FROM " +
			    			 	"bonus_users bu, bonus_states bs " +
			    			 "WHERE " +
			    			 	"user_id = ? AND " +
			    			 	"bu.bonus_state_id = bs.id AND " +
								"bs.id in ( " + ConstantsBase.BONUS_STATE_GRANTED + ", " + ConstantsBase.BONUS_STATE_ACTIVE + " ) AND " +
			    			 	"bu.type_id <> ? ";

				ps = con.prepareStatement(sql);
				ps.setLong(1, userId);
				ps.setLong(2, ConstantsBase.BONUS_TYPE_CONVERT_POINTS_TO_CASH);
				rs = ps.executeQuery();

				if ( rs.next() ) {
					return true;
				}
			} finally {
				closeResultSet(rs);
				closeStatement(ps);
			}
			return false;
		}


	    /**
	     * Get Calcalist users list
	     * @param conn
	     * 		db connection
	     * @return
	     * 		list of all Calcalist users
	     * @throws SQLException
	     */
	    public static ArrayList<User> getCalcalistUsers(Connection conn,String userId,String userIdNum,Date from,Date to) throws SQLException {
	    	PreparedStatement pstmt = null;
	        ResultSet rs = null;
	        ArrayList<User> list = new ArrayList<User>();
	        String[] totals;

	        try {
	            String sql = "SELECT " +
	                            "user_name, " +
			            		"first_name , " +
			            		"last_name , " +
			            		"time_created , " +
			            		"id, " +
			            		"id_num, " +
			            		"currency_id , " +
			            		"reference_id, " +
			            		"balance " +
	            			 "FROM " +
	            			 	"users " +
	            			 "WHERE " +
	            			    "is_active = 1 "+
	            			 	"AND user_name LIKE '%/_CG'  escape '/' " ;

    			  if (!CommonUtil.isParameterEmptyOrNull(userId)) {
    				  sql += "AND id = " + userId;
    			  }

			      if (null != from && null != to) {
			    	 sql += " AND time_created >= ? "+
					 		" AND time_created <= ? ";
			      }

			      if (!CommonUtil.isParameterEmptyOrNull(userIdNum)) {
	            	  sql += " AND	id_num like '" + AESUtil.encrypt(userIdNum) + "' " ;
	              }

    		sql += "ORDER BY " +
    					"time_created " +
    			   "DESC ";

	            pstmt = conn.prepareStatement(sql);

			    if (null != from && null != to) {
			    	pstmt.setTimestamp(1, CommonUtil.convertToTimeStamp(CommonUtil.getDateTimeFormat(from, CommonUtil.getUtcOffset())));
			    	pstmt.setTimestamp(2, CommonUtil.convertToTimeStamp(CommonUtil.getDateTimeFormat(to, CommonUtil.getUtcOffset())));
			    }

				rs = pstmt.executeQuery();

				while (rs.next()) {
					User vo = new User(false);
                    vo.setUserName(rs.getString("user_name"));
					vo.setFirstName(rs.getString("first_name"));
					vo.setLastName(rs.getString("last_name"));
					vo.setTimeCreated(rs.getTimestamp("time_created"));
					vo.setId(rs.getLong("id"));
					vo.setIdNum(AESUtil.decrypt(rs.getString("id_num")));
					vo.setCurrencyId(rs.getLong("currency_id"));
					vo.setReferenceId(rs.getBigDecimal("reference_id"));
					vo.setBalance(rs.getLong("balance"));
					totals = InvestmentsManager.getInvestmentsTotals(vo.getId());
					vo.setTotal(totals[0]);
					vo.setAvgAmount(CommonUtil.displayAmount(totals[1],1));
					vo.setTotalInvestments(CommonUtil.displayAmount(totals[2],1));
					list.add(vo);
				}
		} catch (	CryptoException | InvalidKeyException | NoSuchAlgorithmException | NoSuchPaddingException | IllegalBlockSizeException
					| BadPaddingException | UnsupportedEncodingException | InvalidAlgorithmParameterException ce) {
			// FIXME very bad way of handling exceptions
			throw new SQLException(ce.getMessage());
		} finally {
			closeResultSet(rs);
			closeStatement(pstmt);
		}

	        return list;
	    }



    /**
        *  Set Disable Phone Contact DAA
        * @param conn
        *       Db connection
     * @param sendType
     * @param activeUsersFilter
     * @param userId user id
     * @throws SQLException
        */
        public static ArrayList<User> getSMSUsers(Connection conn, Integer timeLastLoginStart, Integer timeLastLoginEnd, String countryIds, String skinId,
                                       String campaginIds, List<String> customerValue, String populationType, int hasSalesDeposit,
                                       List<String> depositors, long userClassId, int sendType, int activeUsersFilter) throws SQLException {
            PreparedStatement pstmt = null;
            ResultSet rs = null;
            ArrayList<User> list = new ArrayList<User>();
            try {
                String sql = "SELECT " +
                                "userData.id, " +
                                "userData.mobile_phone, " +
                                "userData.skin_id, " +
                                "userData.country_id, " +
                                "userData.user_name, " +
                                "userData.utc_offset, " +
                                "userData.is_contact_by_sms " +
                             "FROM " +
                                 "(SELECT " +
                                        "u.id id, " +
                                        (!depositors.contains(ConstantsBase.ALL_DEPOSIT) &&
                                        		depositors.contains(ConstantsBase.NO_DEPOSIT) ? "sum(case when (t1.id is not null) then 1 else 0 end ) as tran_count, " : "" ) +
                                        (depositors.contains(ConstantsBase.FAILED_DEPOSIT) ? "sum(case when (t1.id is not null and t1.status_id in ( " + TransactionsManagerBase.TRANS_STATUS_FAILED + ")) then 1 else 0 end ) as tran_failed, ":"") +
                                        (depositors.contains(ConstantsBase.SUCCESS_DEPOSIT)|| depositors.contains(ConstantsBase.FAILED_DEPOSIT) ? 
                                    		"sum(case when (t1.id is not null and t1.status_id in ( " + TransactionsManagerBase.TRANS_STATUS_SUCCEED + ","
														                                              + TransactionsManagerBase.TRANS_STATUS_PENDING + ","
														                                              + TransactionsManagerBase.TRANS_STATUS_CANCELED_ETRADER + ","
														                                              + TransactionsManagerBase.TRANS_STATUS_CANCELED_FRAUDS + ","
														                                              + TransactionsManagerBase.TRANS_STATUS_CANCEL_S_DEPOSIT + ") " +
														                                              	" and tt.class_type in (1,11))  then 1 else 0 end ) as tran_succeed, " : "" )  +
                                        (hasSalesDeposit != 0 ? "sum(case when (ti.transaction_id is not null) then 1 else 0 end ) as ts_count, ": "" ) +
                                        (!populationType.contains("0") ? "p.population_type_id, " : "" ) +
                                        (!customerValue.contains(ConstantsBase.ALL_CUSTOMER_VALUE) ? 
                                    		(customerValue.contains(ConstantsBase.CUSTOMER_VALUE_NEGATIVE) ||
                            				 customerValue.contains(ConstantsBase.CUSTOMER_VALUE_OPTION_2) ||
                            				 customerValue.contains(ConstantsBase.CUSTOMER_VALUE_OPTION_3) ||
                            				 customerValue.contains(ConstantsBase.CUSTOMER_VALUE_OPTION_4) ? "inv.winlose, " : "" )  : "" ) +
                                        "u.time_last_login, " +
                                        "u.country_id, " +
                                        "u.skin_id, " +
                                        "u.is_contact_by_sms, " +
                                        "u.combination_id, " +
                                        "u.mobile_phone, " +
                                        "u.user_name, " +
                                        "u.utc_offset, " +
                                        "u.class_id, " +
                                        " u.is_active " +
                                  "FROM " +
                                        "users u " +
                                        "LEFT JOIN countries c ON c.id = u.id " +
                                        "LEFT JOIN transactions t1 ON t1.user_id = u.id " +
                                             "LEFT JOIN transactions_issues ti ON t1.id = ti.transaction_id " +
                                             "LEFT JOIN transaction_types tt ON t1.type_id = tt.id " +
                                             (!populationType.contains("0") ?
                                             "LEFT JOIN population_users pu ON pu.user_id = u.id " +
                                             "LEFT JOIN population_entries pe ON pe.id = pu.curr_population_entry_id " +
                                                  "LEFT JOIN populations p ON p.id = pe.population_id " : "") +
                                        (!customerValue.contains(ConstantsBase.ALL_CUSTOMER_VALUE) ? 
                                        		(customerValue.contains(ConstantsBase.CUSTOMER_VALUE_NEGATIVE) ||
                                				 customerValue.contains(ConstantsBase.CUSTOMER_VALUE_OPTION_2) ||
                                				 customerValue.contains(ConstantsBase.CUSTOMER_VALUE_OPTION_3) ||
                                				 customerValue.contains(ConstantsBase.CUSTOMER_VALUE_OPTION_4) ? 
                                						 "LEFT JOIN (SELECT " +
                                                         "(sum(win)-sum(lose)) winlose, i.user_id " +
                                                      "FROM " +
                                                         "investments i " +
                                                      "WHERE " +
                                                         "i.is_canceled = 0 AND " +
                                                         "i.is_settled = 1 AND " +
                                                         "NOT (i.bonus_odds_change_type_id = 1 AND i.lose > 0) " + 
                                                      "GROUP BY " +
                                                          "i.user_id) inv ON u.id = inv.user_id " : "" ) : "" ) +                                                   
                                 "GROUP BY " +
                                     "u.id, " +
                                     (!populationType.contains("0") ? "p.population_type_id, " : "" ) +
                                     (!customerValue.contains(ConstantsBase.ALL_CUSTOMER_VALUE) ? 
                                     		(customerValue.contains(ConstantsBase.CUSTOMER_VALUE_NEGATIVE) ||
                             				 customerValue.contains(ConstantsBase.CUSTOMER_VALUE_OPTION_2) ||
                             				 customerValue.contains(ConstantsBase.CUSTOMER_VALUE_OPTION_3) ||
                             				 customerValue.contains(ConstantsBase.CUSTOMER_VALUE_OPTION_4) ? "inv.winlose, " : "" ) : "" ) +
                                     "u.time_last_login, " +
                                     "u.country_id, " +
                                     "u.skin_id, " +
                                     "u.is_contact_by_sms, " +
                                     "u.combination_id, " +
                                     "u.mobile_phone, " +
                                     "u.user_name, " +
                                     "u.utc_offset," +
                                     "u.class_id, " +
                                     "u.is_active ";
                                   if (null != campaginIds && !campaginIds.equals("0")) {
                                       sql += ") userData, " +
                                              "marketing_combinations mco ";
                                   } else {
                                       sql += ") userData ";
                                   }
                      sql += "WHERE ";
                                 if (timeLastLoginStart != null) {
                                     sql += "userData.time_last_login <= sysdate - " + timeLastLoginStart + " AND ";
                                 }
                                 if (timeLastLoginEnd != null) {
                                     sql += "userData.time_last_login >= sysdate - " + timeLastLoginEnd + " AND ";
                                 }
                                 if (null != skinId && !skinId.equals(Skin.SKINS_ALL)) {
                                     sql += "userData.skin_id in (" + skinId + ") AND ";
                                 }
                                 if (null != campaginIds && !campaginIds.equals("0")) {
                                     sql += "mco.campaign_id in (" + campaginIds + ") AND " +
                                            "userData.combination_id = mco.id AND ";
                                 }

                                 // Add multiple selection for customer value.
                                 String conditionToQuery = "AND";
                                 if (customerValue.size() > 1) {
                                	 conditionToQuery = "OR";
                                 }
                                 if (!customerValue.contains(ConstantsBase.ALL_CUSTOMER_VALUE)) {
                                     if (customerValue.contains(ConstantsBase.CUSTOMER_VALUE_NEGATIVE)) {
                                    	 sql += "userData.winlose <= 0 " + conditionToQuery + " ";
                                     }
                                     if (customerValue.contains(ConstantsBase.CUSTOMER_VALUE_OPTION_2)) {
                                    	 sql += "userData.winlose >= 0 AND " +
                                    	 		"userData.winlose <= 50000 " + conditionToQuery + " ";
                                     }
                                     if (customerValue.contains(ConstantsBase.CUSTOMER_VALUE_OPTION_3)) {
                                    	 sql += "userData.winlose >= 50100 AND " +
                                 	 			"userData.winlose <= 100000 " + conditionToQuery + " ";
                                     }
                                     if (customerValue.contains(ConstantsBase.CUSTOMER_VALUE_OPTION_4)) {
                                    	 sql += "userData.winlose >= 100100 " + conditionToQuery + " ";
                                     }
                                     // if it's multiple selection replace last substring 'OR' with 'AND'
                                     if (customerValue.size() > 1) {
                                    	 sql = sql.substring(0, sql.length() - 3 );
                                    	 sql += "AND ";
                                     }
                                 }

                                 if (!populationType.contains("0")) {
                                     sql += "userData.population_type_id in (" + populationType + ") AND ";
                                 }
                                 if (hasSalesDeposit != 0) {
                                     if (hasSalesDeposit == 2) {
                                         sql += "userData.ts_count > 0 AND ";
                                     } else {
                                         sql += "userData.ts_count = 0 AND ";
                                     }
                                 }

                                 // Add multiple selection for depositors.
                                 conditionToQuery = "AND";
                                 if (depositors.size() > 1) {
                                	 conditionToQuery = "OR";
                                 }
                                 if (!depositors.contains(ConstantsBase.ALL_DEPOSIT)) {
                                     if (depositors.contains(ConstantsBase.NO_DEPOSIT)) {
                                         sql += "userData.tran_count = 0 " + conditionToQuery + " ";
                                     }
                                     if (depositors.contains(ConstantsBase.FAILED_DEPOSIT)) {
                                         sql += "userData.tran_failed > 0 AND " +
                                                "userData.tran_succeed = 0 " + conditionToQuery + " ";
                                     }
                                     if (depositors.contains(ConstantsBase.SUCCESS_DEPOSIT)) {
                                         sql += "userData.tran_succeed > 0 " + conditionToQuery + " ";
                                     }
//                                   if it's multiple selection replace last substring 'OR' with 'AND'
                                     if (depositors.size() > 1) {
                                    	 sql = sql.substring(0, sql.length() - 3 );
                                    	 sql += "AND ";
                                     }
                                 }

                                 if (!countryIds.equals("0")) {
                                     sql += "userData.country_id in (" + countryIds + " ) AND ";
                                 }
                                 if ( userClassId != ConstantsBase.USER_CLASS_ALL )   {

                     		  		if ( userClassId == ConstantsBase.USER_CLASS_COMPANY_OR_PRIVATE )  {
                     		  			sql += "(userData.class_id = "+ConstantsBase.USER_CLASS_PRIVATE +
                     		  				  " OR userData.class_id = " + ConstantsBase.USER_CLASS_COMPANY+") AND ";
                     		  		}
                     		  		else {
                     		  				sql += "userData.class_id = " + userClassId + " AND ";
                     		  		}
                                 }
                                 if (sendType == Constants.SEND_SMS) {
                                	 sql += " userData.mobile_phone is not null AND " +
                                	 		" userData.is_contact_by_sms = 1 AND " +
                                	 		" exists ( select 1 " +
                                	 					"from countries c " +
                                	 					"where c.id = userData.country_id AND " +
                                	 							"c.is_sms_available = 1) AND ";
                                 }
                                 if (activeUsersFilter == Constants.USER_ACTIVE_INT){
                                	 sql += " userData.is_active = 1 AND ";
                                 }else if (activeUsersFilter == Constants.USER_NOT_ACTIVE_INT){
                                	 sql += " userData.is_active = 0 AND ";
                                 }
                                 sql += "1=1 ";

                pstmt = conn.prepareStatement(sql);
                logger.debug("sql to get sms/email users: " + sql);
                rs = pstmt.executeQuery();

                while (rs.next()) {
                    User vo = new User(false);
                    vo.setId(rs.getLong("id"));
                    vo.setMobilePhone(rs.getString("mobile_phone"));
                    vo.setSkinId(rs.getLong("skin_id"));
                    vo.setCountryId(rs.getLong("country_id"));
                    vo.setUtcOffset(rs.getString("utc_offset"));
                    vo.setUserName(rs.getString("user_name"));
                    vo.setIsContactBySMS(rs.getLong("is_contact_by_sms"));
                    list.add(vo);
                }

            } finally {
                closeStatement(pstmt);
            }

            return list;
        }


	public static ArrayList<User> getSMSUsersFromFile(Connection con, String usersFromFile,int sendType) throws SQLException {
		PreparedStatement pstmt = null;
        ResultSet rs = null;
        ArrayList<User> list = new ArrayList<User>();
        try {
            String sql = "SELECT " +
				            "u.id, " +
				            "u.mobile_phone, " +
				            "u.skin_id, " +
				            "u.country_id, " +
				            "u.user_name, " +
				            "u.utc_offset, " +
				            "u.is_contact_by_sms " +
				         "FROM " +
				            "users u " +
				         "WHERE " +
				         	" u.id in ( " + usersFromFile + " ) " +
				         ((sendType == Constants.SEND_SMS) ? " and is_contact_by_sms = 1 " : "" );
				         	

            pstmt = con.prepareStatement(sql);
            logger.debug("sql to get sms users: " + sql);
            rs = pstmt.executeQuery();

            while (rs.next()) {
                User vo = new User(false);
                vo.setId(rs.getLong("id"));
                vo.setMobilePhone(rs.getString("mobile_phone"));
                vo.setSkinId(rs.getLong("skin_id"));
                vo.setCountryId(rs.getLong("country_id"));
                vo.setUtcOffset(rs.getString("utc_offset"));
                vo.setUserName(rs.getString("user_name"));
                vo.setIsContactBySMS(rs.getLong("is_contact_by_sms"));
                list.add(vo);
            }

        } finally {
            closeStatement(pstmt);
        }

        return list;
	}

	public static void getMaxDepositAndDate(Connection con, User user) throws SQLException {
		PreparedStatement pstmt = null;
        ResultSet rs = null;

        try {
            String sql =" select " +
            				" max_amount.time_created, " +
            				" max_amount.amount " +
            			" from ( select " +
            						" t.time_created, " +
            						" t.amount " +
            					" from " +
            						" transactions t, " +
            						" transaction_types tt, " +
            						" transaction_class_types tct " +
            					" where " +
            						" t.user_id = ? " +
            						" AND t.type_id = tt.id " +
            						" AND tt.class_type = tct.id " +
            						" AND tct.id = ? " +
            						" AND t.status_id in (?,?,?) " +
            					" order by t.amount desc) max_amount " +
            			" where " +
            				" rownum = 1 ";

            pstmt = con.prepareStatement(sql);
            logger.debug("sql to get max deposit from user: " + sql);
            pstmt.setLong(1, user.getId());
            pstmt.setLong(2, TransactionsManagerBase.TRANS_CLASS_DEPOSIT);
            pstmt.setLong(3, TransactionsManagerBase.TRANS_STATUS_SUCCEED);
            pstmt.setLong(4, TransactionsManagerBase.TRANS_STATUS_PENDING);
            pstmt.setLong(5, TransactionsManagerBase.TRANS_STATUS_CANCELED_ETRADER);

            rs = pstmt.executeQuery();

            user.setMaxDepositAmount(CommonUtil.displayAmount( 0, user.getCurrencyId().intValue()));
            user.setMaxDepositDate("");

            if (rs.next()) {
            	user.setMaxDepositAmount(CommonUtil.displayAmount( rs.getString("amount"), user.getCurrencyId().intValue()));
            	//user.setMaxDepositDate(CommonUtil.getDateFormat(rs.getTimestamp("time_created")));
                user.setMaxDepositDate(CommonUtil.getDateFormat(rs.getTimestamp("time_created"), Utils.getWriter().getUtcOffset()));

            }

        } finally {
            closeStatement(pstmt);
        }

	}

	public static DeviceUniqueIdSkin getUserAgentFromLogins(Connection con, long lastLoginId) throws SQLException {
		PreparedStatement pstmt = null;
        ResultSet rs = null;
        DeviceUniqueIdSkin device = null;
        try {
            String sql = " SELECT " +
            			" device_unique_id, user_agent " +
            			" FROM  " +
            			" logins " +
            			" WHERE " +
            			" id = ? AND " +
            			" device_unique_id IS NOT NULL " +
            			" order by id desc ";

            pstmt = con.prepareStatement(sql);
            pstmt.setLong(1, lastLoginId);
            rs = pstmt.executeQuery();
            if (rs.next()) {
            	device = new DeviceUniqueIdSkin();
            	device.setUserAgent(rs.getString("user_agent"));
            	device.setDuid(rs.getString("device_unique_id"));
            }
        } finally {
        	closeResultSet(rs);
            closeStatement(pstmt);
        }
        return device;
	}

	/**
	 * Get combination id by device unique id
	 * @param con
	 * @param duid
	 * @return
	 * @throws SQLException
	 */
    public static Long getCombIdByDuid(Connection con, String duid) throws SQLException {
        PreparedStatement ps = null;
        ResultSet rs = null;
        Long combId = null;
        try {
            String sql =
            	"SELECT " +
            		"combination_id " +
            	"FROM " +
            		"device_unique_ids_skin " +
            	"WHERE " +
            		"device_unique_id = ?";
            ps = con.prepareStatement(sql);
            ps.setString(1, duid);
            rs = ps.executeQuery();
            if (rs.next()) {
            	combId = rs.getLong("combination_id");
            }
        } finally {
        	closeResultSet(rs);
        	closeStatement(ps);
        }
        return combId;
    }


	public static void updateUserBackgroundAndTip(Connection con, long userId, String background, String tip, long tag) throws SQLException {
        PreparedStatement ps = null;
        try {
            String sql =
            	"UPDATE " +
            		" users " +
            	"SET " +
            		" background = ?, " +
            		" tip = ?, " +
            		" tag = ? " +
            	"WHERE " +
            		"id = ?";
            ps = con.prepareStatement(sql);
            ps.setString(1, background);
            ps.setString(2, tip);
            ps.setLong(3, tag);
            ps.setLong(4, userId);
            ps.executeUpdate();
        } finally {
        	closeStatement(ps);
        }
	}

    public static void setUserToPromotionsTable(Connection con, ArrayList<User> usersList, HttpSession session, int sendType) throws SQLException{
    	int sumOfUsersList = usersList.size();
    	final int batchSize = 1000;
		int count = 0;
		double i=0;	
		String percentage = "";
		String sql = "INSERT INTO " +
				  "promotions_entries " +
				  		"(id," +
				  		" user_id," +
				  		" promotion_id," +
				  		" status_id," +
				  		" time_created)" +
				  		"VALUES" +
				  		"(SEQ_PROMOTIONS_ENTRIES.nextval, ? ," +
				  		"SEQ_PROMOTION_ID.currval," +
				  		Constants.PROMOTION_STATUS_ID_CREATED + "," +
				  		"sysdate)";
		PreparedStatement ps = con.prepareStatement(sql);
		try{
			for(User usr: usersList){			
			if(sendType == Constants.SEND_EMAIL || (sendType == Constants.SEND_SMS && usr.getContactBySMS())){
				ps.setLong(1 , usr.getId());
				ps.addBatch();
				count++;
				i++;
			}
			
			percentage = Double.toString(((i/sumOfUsersList)*100));
        	session.setAttribute(Constants.PROMOTION_TOTAL_AND_INSERTED_USERS,sumOfUsersList+ "_" + percentage.substring(0, percentage.indexOf(".")) );
        	
			if(count % batchSize == 0) {
		        ps.executeBatch();
		        }
			}
		} finally {
			ps.executeBatch();
			ps.close();
		}
    }   	

    public static void insertPromotion(Connection conn, Long templateId, int sendType, String message, long writerId, String promotionName) throws SQLException{
    	PreparedStatement ps = null;
    	ResultSet rs = null;
    	String sql = null;
    	int index =1;

    	try{
    		sql = 	"INSERT INTO " +
					"promotions " +
					"(id," +
					" template_id," +
					" promotion_type_id," +
					" promotion_text," +
					" writer_id," +
					" status_id," +
					" time_created," +
					" promotion_name)" +
					"VALUES" +
					"(SEQ_PROMOTION_ID.nextval," +
					templateId + ", " +
					sendType + ", " +
					" ?, " +
					writerId +", " +
					Constants.PROMOTION_STATUS_ID_CREATED + ", " +
					"sysdate, " +
					"? ) ";
    		ps = conn.prepareStatement(sql);
   			ps.setString(index++, message);
	    	ps.setString(index++, promotionName);
	    	rs = ps .executeQuery();
    	}finally{
    		closeResultSet(rs);
    		closeStatement(ps);
    	}
    }

    public static ArrayList<UserBase> getUsersListByPromotionId(Connection conn, long promotionId, boolean smsAvailable){
    	ArrayList<UserBase> list = new ArrayList<UserBase>();
    	PreparedStatement ps = null;
    	ResultSet rs = null;

    	String sql= getUsersListByPromotionIdSql(false, smsAvailable);
    	try {
    		
    		ps = conn.prepareStatement(sql);
    		ps.setLong(1, promotionId);
    		rs = ps.executeQuery();
    		while(rs.next()){
    			User vo = new User(false);
                vo.setId(rs.getLong("id"));
                vo.setMobilePhone(rs.getString("mobile_phone"));
                vo.setSkinId(rs.getLong("skin_id"));
                vo.setCountryId(rs.getLong("country_id"));
                vo.setUtcOffset(rs.getString("utc_offset"));
                vo.setUserName(rs.getString("user_name"));
                vo.setMobileNumberValidated(rs.getInt("mobile_phone_validated"));
                list.add(vo);
    		}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		} finally{
			closeResultSet(rs);
			closeStatement(ps);
		}
		return list;
    }
    
    
    
    public static int getUsersListCountByPromotionId(Connection conn, long promotionId, boolean smsAvailable){
    	PreparedStatement ps = null;
    	ResultSet rs = null;

    	String sql= getUsersListByPromotionIdSql(true, smsAvailable);
    	try {
    		
    		ps = conn.prepareStatement(sql);
    		ps.setLong(1, promotionId);
    		rs = ps.executeQuery();
    		if(rs.next()){
                return rs.getInt("count");
    		}
		} catch (Exception e) {
			e.printStackTrace();
		} finally{
			closeResultSet(rs);
			closeStatement(ps);
		}
    	return 0;
    }
    
    
    static String getUsersListByPromotionIdSql(boolean count, boolean smsAvailable) {
		String sql =
		
		"SELECT " +
				"u.id, " +
				"u.mobile_phone, " +
				"u.skin_id, " +
				"u.country_id, " +
				"u.user_name, " +
				"u.utc_offset, " +
				"u.mobile_phone_validated, " +
				"c.Is_Sms_Available sms_available " +
		"FROM  " +
				"users u, " +
				"promotions p, " +
				"promotions_entries pe, " +
				"countries c " +
		"WHERE " +
			" u.id = pe.user_id AND " +
			" p.id = pe.promotion_id AND " +
			" u.country_id = c.id AND " ;
			if(smsAvailable) {
				sql += "c.is_sms_available = 1 AND ";
			}
	sql += " p.id = ? ";
		
		if(count){
			return count(sql);
		} else {
			return sql;
		}
    }
    
    public static boolean isBonusAbuser(Connection conn, long userId) throws SQLException{
    	PreparedStatement ps = null;
    	ResultSet rs = null;

    	try {

    		String sql = "select bonus_abuser from users " +
    					 "where id = ? ";

    		ps = conn.prepareStatement(sql);
    		ps.setLong(1, userId);

    		rs = ps.executeQuery();

    		if ( rs.next() ) {
    			int ba = rs.getInt(1);
    			return ba != 0;
    		}
    	} finally {
    		closeResultSet(rs);
    		closeStatement(ps);
    	}

    	return false;
    }
    
    public static void updateUserContactByEmail(Connection con, User u) throws SQLException {
        PreparedStatement ps = null;
        try {
            String sql =
            	"UPDATE " +
            		" users " +
            	"SET " +
            		" is_contact_by_email = ? " +
            	"WHERE " +
            		"id = ?";
            ps = con.prepareStatement(sql);
            ps.setInt(1,u.getContactByEmail() ? 1 : 0);
            ps.setLong(2, u.getId());
            ps.executeUpdate();
        } finally {
        	closeStatement(ps);
        }
	}
    
    public static boolean isWaiveDepositsVelocityCheck(Connection con, long userId) throws SQLException {

    	PreparedStatement ps = null;
    	ResultSet rs = null;
    	long velocityActive = 1;
    	boolean result = false;
    	try {

    		String sql = "select transactions_manager.VELOCIY_CHECK_IS_ACTIVE(?) as velocity_active from dual ";    					 

    		ps = con.prepareStatement(sql);    	
    		ps.setLong(1, userId);

    		rs = ps.executeQuery();

    		if ( rs.next() ) {
    			velocityActive = rs.getLong("velocity_active");
    		}
    		
    		if (velocityActive == 0 ){
    			result = true;
    		}
    	} finally {
    		closeResultSet(rs);
    		closeStatement(ps);
    	}

    	return result;
    }
    
    /**
     * get users details HistoryTypes
     * @param con
     * @return
     * @throws SQLException
     * @throws CryptoException
     */
    public static ArrayList<SelectItem> getUsersDetailsHistoryTypes(Connection con) {
        PreparedStatement ps = null;
        ResultSet rs = null;
        ArrayList<SelectItem> list = new ArrayList<SelectItem>();
        try {
            String sql= " SELECT " +
                            " * " +
                        " FROM " +
                            " users_details_history_type ";
            ps = con.prepareStatement(sql);         
            rs = ps.executeQuery();
            while (rs.next()) {
                list.add(new SelectItem(rs.getString("id"), rs.getString("name")));
            }           
        } catch (SQLException e) {
            logger.error("Error! problem to get users details history types.", e);
        } finally {
            closeResultSet(rs);
            closeStatement(ps);
        }
        return list;
    }
    
    /**
     * get users details history.
     * @param con
     * @param from
     * @param to
     * @param userName
     * @param email
     * @param contactId
     * @param writersId
     * @param typesId
     * @param isExcludeTesters
     * @return list as ArrayList<UsersDetailsHistory>
     * @throws SQLException 
     */
    public static ArrayList<UsersDetailsHistory> getUsersDetailsHistory(Connection con, Date from, Date to, String userName, String email, long contactId, String writersId, String typesId, boolean isExcludeTesters, long userId) throws SQLException {
        PreparedStatement ps = null;
        ResultSet rs = null;
        ArrayList<UsersDetailsHistory> list = new ArrayList<UsersDetailsHistory>();
        SimpleDateFormat fmt = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        try {
            String sql= " SELECT " +
            		        " udh.*, " +
            		        " udht.name as field_changed, " +
            		        " udht.is_checkbox as is_checkbox_field, " +
            		        " udhc.users_details_history_type_id, " +
            		        " udhc.field_before, " +
            		        " udhc.field_after, " +
            		        " w.first_name as writer_first_name, " +
            		        " w.last_name as writer_last_name, " +
            		        " u.email " +
            		    " FROM " +
                            " users u, " +
            		        " users_details_history udh, " +
            		        " users_details_history_changes udhc, " +
            		        " users_details_history_type udht, " +
                            " writers w " +
            		    " WHERE " +
            		        " u.id = udh.user_id " +
            		        " AND udh.id = udhc.users_details_history_id " +
            		        " AND udhc.users_details_history_type_id = udht.id " +
            				" AND udh.writer_id = w.id ";
            if (null != from && null != to) {
                sql +=  " AND udh.time_created between to_date('"+ fmt.format(from) + "','dd/MM/yyyy HH24:mi:ss') " +
                            " AND to_date('"+ fmt.format(to) + "','dd/MM/yyyy HH24:mi:ss') +1 ";
            }
            if (!CommonUtil.isParameterEmptyOrNull(userName)) {
                sql +=  " AND udh.USER_NAME = '" + userName.toUpperCase() + "' ";
            }
            if (!CommonUtil.isParameterEmptyOrNull(email)) {
                sql +=  " AND u.email = '" + email + "' ";
            }
            if (contactId > -1) {
                sql +=  " AND u.contact_id = " + contactId + " ";
            }
            if (!writersId.equals(String.valueOf(Constants.ALL_FILTER_ID))) {
                sql +=  " AND udh.writer_id in (" + writersId + ") ";
            }
            if (!typesId.equals(String.valueOf(Constants.ALL_FILTER_ID))) {
                sql +=  " AND udhc.users_details_history_type_id in (" + typesId + ") ";
            }
            if (isExcludeTesters) {
                sql +=  " AND u.class_id <> " + ConstantsBase.USER_CLASS_TEST;
            }
            if (userId != 0) {
                sql +=  " AND u.id = " + userId + " ";
            }
            sql +=      " ORDER BY " +
                            " udh.time_created DESC ";
            ps = con.prepareStatement(sql);         
            rs = ps.executeQuery();
            while (rs.next()) {
                UsersDetailsHistory udh = new UsersDetailsHistory();
                udh.setId(rs.getLong("id"));
                udh.setTimeCreated(convertToDate(rs.getTimestamp("time_created")));
                udh.setWriterId(rs.getLong("writer_id"));
                udh.setUserId(rs.getLong("user_id"));
                udh.setUserName(rs.getString("user_name"));
                udh.setTypeId(rs.getLong("users_details_history_type_id"));
                udh.setFieldBefore(rs.getString("field_before"));
                udh.setFieldAfter(rs.getString("field_after"));
                udh.setLastReached(convertToDate(rs.getTimestamp("last_reached")));
                udh.setClassId(rs.getLong("class_id"));
                udh.setFieldChanged(rs.getString("field_changed"));
                udh.setIsCheckboxField(rs.getBoolean("is_checkbox_field"));
                udh.setEmail(rs.getString("email"));
                udh.setWriterFirstName(rs.getString("writer_first_name"));
                udh.setWriterLastName(rs.getString("writer_last_name"));
                list.add(udh);
            }
        } finally {
            closeResultSet(rs);
            closeStatement(ps);
        }
        return list;
    }
    
    public static String getLastWriterIdChat(Connection con, long userId) throws SQLException {
    	PreparedStatement ps = null;
        ResultSet rs = null;
        String writerName = "";
        
        try {
            String sql =
	            	"SELECT " +
	        		"		last_chat_writer_id, " +
	        		"		w.user_name " +
	            	"FROM " +
	        		"		users_additional_info uai " +
	        		"	INNER JOIN " +
	        		"		writers w " +
	        		"	ON " +
	        		"		uai.last_chat_writer_id =  w.id " +
	            	"WHERE " +
	        		"		uai.user_id = ? " +
	        		"ORDER BY " +
	        		"		uai.time_created desc ";
            ps = con.prepareStatement(sql);
            ps.setLong(1, userId);
            rs = ps.executeQuery();
            if (rs.next()) {
            	writerName = rs.getString("user_name");
            }
        } finally {
        	closeResultSet(rs);
        	closeStatement(ps);
        }
        return writerName;
    }
    
    /**
     * Get register source: anyoption/etrader/copyop and WEB/MOBILE
     * @param con
     * @param userId
     * @return
     * @throws SQLException
     */
    public static String getRegisterOriginal(Connection con, long userId) throws SQLException {
    	PreparedStatement ps = null;
        ResultSet rs = null;
        String regOrig = "";
        
        try {
            String sql =" SELECT " +
            			"	w.id writer_id, " +
            			"   p.name platform_name, " +
            			"   w.user_name writer_name " +
            			" FROM " +
            			"   users u, " +
            			"   platforms p, " +
            			"   writers w " +
            			" WHERE " +
            			"   w.id = u.writer_id " +
            			"   AND u.platform_id = p.id " +
            			"	AND u.id = ? ";

            ps = con.prepareStatement(sql);
            ps.setLong(1, userId);
            rs = ps.executeQuery();
            if (rs.next()) {
            	int writerId = rs.getInt("writer_id");            	
            	String writerName = rs.getString("writer_name");
            	if (writerId != com.anyoption.common.beans.Writer.WRITER_ID_COPYOP_MOBILE 
            			&& writerId != com.anyoption.common.beans.Writer.WRITER_ID_COPYOP_WEB
            			&& writerId != com.anyoption.common.beans.Writer.WRITER_ID_WEB
            			&& writerId != com.anyoption.common.beans.Writer.WRITER_ID_API
            			&& writerId != com.anyoption.common.beans.Writer.WRITER_ID_AUTO
            			&& writerId != com.anyoption.common.beans.Writer.WRITER_ID_MOBILE
            			&& writerId != com.anyoption.common.beans.Writer.WRITER_ID_AO_MINISITE
            			&& writerId != com.anyoption.common.beans.Writer.WRITER_ID_CO_MINISITE) {
            		writerName = "Backend";
            	}
            	regOrig = rs.getString("platform_name") + " " + writerName;	
            }            	
        } finally {
        	closeResultSet(rs);
        	closeStatement(ps);
        }
        return regOrig;
    }
    
    public static ArrayList<UserRegulationHistory> getUserRegulationHistory(Connection con, long userId, Date from, Date to) {
        PreparedStatement ps = null;
        ResultSet rs = null;
        ArrayList<UserRegulationHistory> list = new ArrayList<UserRegulationHistory>();
        SimpleDateFormat fmt = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        try {
            String sql= "SELECT be.*, w.user_name, w.utc_offset " + 
            			"FROM users_regulation_history_be be, writers w " +
            			"WHERE be.writer_id = w.id " +
            			"AND be.user_id = ? " +
            			"AND be.time_created BETWEEN to_date('"+ fmt.format(from) + "','dd/MM/yyyy HH24:mi:ss') " +
            			"AND to_date('"+ fmt.format(to) + "','dd/MM/yyyy HH24:mi:ss') + 1 " + 
            			"ORDER BY be.time_created, be.id";
            ps = con.prepareStatement(sql);
            ps.setLong(1, userId);
            rs = ps.executeQuery();
            while (rs.next()) {
                list.add(getVO(rs));
            }           
        } catch (SQLException e) {
            logger.error("Error! problem to get AO user regulation details history.", e);
        } finally {
            closeResultSet(rs);
            closeStatement(ps);
        }
        return list;
    }
    
    private static UserRegulationHistory getVO(ResultSet rs) throws SQLException {
    	
    	UserRegulationHistory urh = new UserRegulationHistory();
    	
    	urh.setId(rs.getLong("id"));
    	urh.setUserId(rs.getLong("user_id"));
    	urh.setTimeCreated(rs.getTimestamp("time_created"));
    	urh.setStep(rs.getString("step"));
    	urh.setComments(rs.getString("comments"));
    	urh.setWriterId(rs.getLong("writer_id"));
    	urh.setTrigerEvent(rs.getString("trig_event"));
    	urh.setWriterUserName(rs.getString("user_name"));
    	urh.setUtcOffsetCreated(rs.getString("utc_offset"));
    	
    	return urh;
    }


	public static boolean isUserFromMediaChannel(Connection con, long userId) throws SQLException {
		PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            String sql= "SELECT "
            		 	  + "* "
            		  + "FROM "
            		 	  + "etrader_anal.channels_updated "
            		  + "WHERE "
            		  	  + "id = ? AND "
            		  	  + "channel = 'Media TLV'";
            
            ps = con.prepareStatement(sql);
            ps.setLong(1, userId);
            rs = ps.executeQuery();
            
            if (rs.next()) {
                return true;
            }           
        } finally {
            closeResultSet(rs);
            closeStatement(ps);
        }
        return false;
	}
    
	public static PendingUserWithdrawalsDetailsBase pendingUserWithdrawalsDetails(Connection con, long userId) throws SQLException {
		PreparedStatement ps = null;
        ResultSet rs = null;
        PendingUserWithdrawalsDetailsBase pw = new PendingUserWithdrawalsDetailsBase();
        try {
            String sql= "SELECT " +  
            		" u.id as user_id, " + 
            		" case  when nvl(i.inv_count,0) > 0 then 1 else 0 end as is_has_inv, " +
            		" u.currency_id, " +
            		" u.WINLOSE_BALANCE, " +
            		" utils.GET_USER_BONUSES_BE_STATE(u.id) as b_state " +  
            		" FROM  " +
            		" users u, " +
            		" (select a.user_id, count(*) as inv_count from " +
            		" investments a " +
            		" where is_canceled = 0 " +
            		" and user_id = ? " +
            		" group by user_id) i " +
            		" where u.id = i.user_id (+) " +
            		" and u.id = ?";
            
            ps = con.prepareStatement(sql);
            ps.setLong(1, userId);
            ps.setLong(2, userId);
            rs = ps.executeQuery();
            
            if (rs.next()) {
            	pw.setUserBonusState(userId);
            	pw.setCurrencyId(rs.getLong("currency_id"));
            	pw.setHaveInvestments(rs.getBoolean("is_has_inv"));
            	pw.setWinLoseBalance(rs.getLong("WINLOSE_BALANCE")*-1);
            	pw.setWinloseBalanceTxt(CommonUtil.displayAmount(pw.getWinLoseBalance(), pw.getCurrencyId()));
            	pw.setUserBonusState(rs.getLong("b_state"));
            	pw.setSuccessfulWithdrawals(TransactionsManager.getNumberOfWithdraws(userId));
				long deposits =
						TransactionsManager.getTransactionsSummary(userId,
								TransactionsManagerBase.TRANS_TYPS_ALL_DEPOSITS_TXT,
								String.valueOf(TransactionsManagerBase.TRANS_STATUS_SUCCEED) + "," +
								String.valueOf(TransactionsManagerBase.TRANS_STATUS_PENDING), false);
				long depositsUsd =
						TransactionsManager.getTransactionsSummary(userId,
								TransactionsManagerBase.TRANS_TYPS_ALL_DEPOSITS_TXT,
								String.valueOf(TransactionsManagerBase.TRANS_STATUS_SUCCEED) + "," +
								String.valueOf(TransactionsManagerBase.TRANS_STATUS_PENDING), true);
				
				pw.setDepositAmount(deposits);
				pw.setDepositAmountTxt(CommonUtil.displayAmount(pw.getDepositAmount(), pw.getCurrencyId()));
				pw.setDepositAmountInUSD(depositsUsd);
				pw.setDepositAmountInUSDTxt(CommonUtil.displayAmount(pw.getDepositAmountInUSD(), Currency.CURRENCY_USD_ID));
            }           
        } finally {
            closeResultSet(rs);
            closeStatement(ps);
        }
        return pw;
	}
	
	public static ArrayList<UserDetailsHistoryChanges> getUserDetailsHistoryChangesList(Connection con, long userId, Date from, Date to, long fieldId, long writerId) throws SQLException {		
		PreparedStatement ps = null;
        ResultSet rs = null;
        ArrayList<UserDetailsHistoryChanges> list = new ArrayList<UserDetailsHistoryChanges>();
        String fieldIds = USER_HIST_FIELDS;
        String writerFilterSQL = "";        
        if(fieldId > 0){
        	fieldIds = "(" + fieldId + ")";
        }               
        
        final long BE_WRITERS_FILTER = -1;
        if(writerId != ConstantsBase.ALL_FILTER_ID_INT){
        	if(writerId == BE_WRITERS_FILTER){
        		writerFilterSQL = " and ud.writer_id not in (1, 200, 10000, 20000) ";
        	} else {
        		writerFilterSQL = " and ud.writer_id = " + writerId;
        	}
        }
        
        try {
            String sql= "SELECT " + 
            				" uc.id, ut.name, ut.id as type_id, uc.field_before, uc.field_after, w.user_name, ud.time_created, ud.writer_id " +
            			" FROM  " +
            				" users_details_history ud, " +
            				" users_details_history_changes uc, " +
            				" users_details_history_type ut, " +
            				" writers w " +
            		    " WHERE ud.id = uc.users_details_history_id " +
            		    	" and uc.users_details_history_type_id = ut.id " +
            		    	" and ut.id in " + fieldIds +
            		    	" and w.id = ud.writer_id " +
//            		    	" and ud.id not in " + 
//            		    		" (select min(users_details_history_id) " + 
//            		    			" from  users_details_history_changes d, " + 
//            		    			" users_details_history u " + 
//            		    			" where u.id=d.users_details_history_id " + 
//            		    		" and u.user_id = ?) " +
            		         "and nvl(field_before,'first_rec') != 'first_rec'" +            		    			
            		    	" and user_id = ? " +
            		    	" and ud.time_created >= ? " +
            		    	" and ud.time_created <= ? " +
            		    	writerFilterSQL +
            		    " ORDER BY uc.id desc " ;
            int index = 1;
            ps = con.prepareStatement(sql);
            ps.setLong(index++, userId);
            //ps.setLong(index++, userId);
            ps.setTimestamp(index++, CommonUtil.convertToTimeStamp(CommonUtil.getDateTimeFormat(from, Utils.getAppData().getUtcOffset())));
			ps.setTimestamp(index++, CommonUtil.convertToTimeStamp(CommonUtil.getDateTimeFormat(CommonUtil.addDay(to), Utils.getAppData().getUtcOffset())));

            rs = ps.executeQuery();
            
            while (rs.next()) {
            	UserDetailsHistoryChanges udh = new UserDetailsHistoryChanges();
            	udh.setId(rs.getLong("id"));
            	udh.setField(rs.getString("name"));
            	if (rs.getLong("type_id") == COUNTRY_FIELD) {
            		if(Long.valueOf(rs.getString("field_before")) > 0){
                    	udh.setOldValue(CountryManagerBase.getCountries().get(Long.valueOf(rs.getString("field_before"))).getName());
            		}
                	udh.setNewValue(CountryManagerBase.getCountries().get(Long.valueOf(rs.getString("field_after"))).getName());
            	} else if (rs.getLong("type_id") == SKIN_FIELD) {
                	udh.setOldValue(SkinsManagerBase.getSkins().get(Long.valueOf(rs.getString("field_before"))).getName());
                	udh.setNewValue(SkinsManagerBase.getSkins().get(Long.valueOf(rs.getString("field_after"))).getName());
            	} else {
                	udh.setOldValue(rs.getString("field_before"));
                	udh.setNewValue(rs.getString("field_after"));            		
            	}
            	udh.setChangedBy(rs.getString("user_name"));
            	udh.setTimeDate(convertToDate(rs.getTimestamp("time_created")));
            	udh.setWriterId(rs.getLong("writer_id"));
            	
            	list.add(udh);
            }           
        } finally {
            closeResultSet(rs);
            closeStatement(ps);
        }
        return list;
	}
	
	  public static ArrayList<SelectItem> getUserHistoryFieldsSI(Connection con) throws SQLException {
		  PreparedStatement ps=null;
		  ResultSet rs=null;
		  ArrayList<SelectItem> list = new ArrayList<SelectItem>();
		  try
			{
			  	String sql="select id, name "+
					"from users_details_history_type m " +
					" where id in " + USER_HIST_FIELDS +
					" order by id ";

				ps = con.prepareStatement(sql);
				rs=ps.executeQuery();
				while (rs.next()) {
					list.add(new SelectItem (rs.getString("id"), rs.getString("name")));
				}
			}
			finally
			{
				closeResultSet(rs);
				closeStatement(ps);
			}
			return list;
	  }
	  
	public static void updateSentDepDocumentsWriter(Connection con, long userId, long writerId) throws SQLException {
	    PreparedStatement ps = null;
	    try {
	        String sql =
	        	"UPDATE " +
	        		" users_active_data " +
	        	"SET " +
	        		" sent_dep_documents_writer_id = ? ," +
	        		" sent_dep_documents_date_time = sysdate " +
	        	"WHERE " +
	        		" user_id = ? ";
	        ps = con.prepareStatement(sql);
	        ps.setLong(1, writerId);
	        ps.setLong(2, userId);
	        ps.executeUpdate();
	    } finally {
	    	closeStatement(ps);
	    }
	}
	
	public static Address getAddressByUserId(Connection con, long userId) throws SQLException {
		Address address = null;
		CallableStatement cstmt = null;
		ResultSet rs = null;
		int index = 1;
		
		try {
			cstmt = con.prepareCall("{call pkg_user.get_user_address(o_user_address => ? " +
											", i_user_id => ? )}");
			
			cstmt.registerOutParameter(index++, OracleTypes.CURSOR);
			cstmt.setLong(index++, userId);
			
			cstmt.execute();
			
			rs = (ResultSet) cstmt.getObject(1);
			
			if (rs.next()) {
				address = new Address();
				address.setStreet(rs.getString("street"));
				address.setStreetNum(rs.getString("street_no"));
				address.setCity(rs.getString("city_name"));
				address.setZip(rs.getString("zip_code"));
				address.setCountry(rs.getString("country_name"));
			}
		} finally {
			closeStatement(cstmt);
			closeResultSet(rs);
		}
		
		return address;
	}
	
	public static void updateUserData(Connection con, long userId, Long dob, String firstName, String lastName,
			long writerId) throws SQLException {
		CallableStatement cstmt = null;
		ResultSet rs = null;
		int index = 1;
		
		try {
			cstmt = con.prepareCall("{call pkg_user.UPDATE_USER_DATA(I_USER_ID => ? " +
																		   ", I_TIME_BIRTH_DATE => ?" +
																		   ", I_USER_FIRST_NAME => ?" +
																		   ", I_USER_LAST_NAME => ?" +
																		   ", I_WRITER_ID => ? )}");			
			cstmt.setLong(index++, userId);
			setStatementValue(new Timestamp(dob == null ? 0 : dob + 1), index++, cstmt);
			setStatementValue(firstName, index++, cstmt);
			setStatementValue(lastName, index++, cstmt);
			cstmt.setLong(index++, writerId);
			
			cstmt.execute();			
		} finally {
			closeStatement(cstmt);
			closeResultSet(rs);
		}		
	}

	public static List<TCAccessTokenSite> getTCSites(Connection con) throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		List<TCAccessTokenSite> result = new ArrayList<>();
		try {
			String sql = "select id, name from trading_central_sites";
			ps = con.prepareStatement(sql);
			rs = ps.executeQuery();
			while (rs.next()) {
				TCAccessTokenSite site = new TCAccessTokenSite();
				site.setId(rs.getInt("id"));
				site.setName(rs.getString("name"));
				result.add(site);
			}
		} finally {
			closeStatement(ps);
			closeResultSet(rs);
		}
		return result;
	}

	public static List<TCAccessToken> getTCAccess(Connection con, long userId, Date from, Date to, String utcOffset, List<Integer> sites) throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		List<TCAccessToken> result = new ArrayList<>();
		try {
			String sql = "select * from (select 0 id, id user_id, 1 site, (select time_created from transactions where id = u.first_deposit_id) time_created,"
											+ " null end_date, 0 is_canceled, 0 writer_id, 'Basic' name "
										+ "from users u where u.id = ? "
						+ "union "
						+ "select tca.*, tcs.name from trading_central_access tca, trading_central_sites tcs where user_id = ? and tca.site = tcs.id ";
			if (null != from) {
				sql += "and time_created >= ? ";
			}

			if (null != to) {
				sql += "and time_created <= ? ";
			}
			if (!sites.contains(0)) {
				sql += "and site member of ? ";
			}
			sql += "order by time_created desc)";
			ps = con.prepareStatement(sql);
			int index = 0;
			ps.setLong(++index, userId);
			ps.setLong(++index, userId);
			if (null != from) {
				ps.setTimestamp(++index, CommonUtil.convertToTimeStamp(CommonUtil.getDateTimeFormat(from, utcOffset)));
			}

			if (null != to) {
				ps.setTimestamp(++index, CommonUtil.convertToTimeStamp(CommonUtil.getDateTimeFormat(CommonUtil.addDay(to), utcOffset)));
			}
			if (!sites.contains(0)) {
				ps.setArray(++index, getPreparedSqlArray(con, sites));
			}
			rs = ps.executeQuery();
			while (rs.next()) {
				TCAccessToken token = new TCAccessToken();
				result.add(token);
				token.setId(rs.getLong("id"));
				token.setSiteName(rs.getString("name"));
				if (rs.getTimestamp("time_created") != null) {
					token.setTimeCreated(convertToDate(rs.getTimestamp("time_created")));
				}
				if (rs.getTimestamp("end_date") != null) {
					token.setEndDate(convertToDate(rs.getTimestamp("end_date")));
				}
				token.setCanceled(rs.getBoolean("is_canceled"));
			}
		} finally {
			closeStatement(ps);
			closeResultSet(rs);
		}
		return result;
	}

	public static void cancelTCAccessToken(Connection con, long tokenId) throws SQLException {
		PreparedStatement ps = null;
		try {
			String sql = "update trading_central_access set is_canceled = 1 where id = ?";
			ps = con.prepareStatement(sql);
			ps.setLong(1, tokenId);
			ps.executeUpdate();
		} finally {
			closeStatement(ps);
		}
	}
	
	public static boolean addTCAccessToken(Connection con, int site, Date from, Date to, long userId, long writerId, String utcOffset) throws SQLException {
		PreparedStatement ps = null;
		PreparedStatement psOldRec = null;
		try {
			String sqlOldRec = "update trading_central_access set is_canceled = 1 where user_id = ? and trunc(end_date) > trunc(sysdate)";
			psOldRec = con.prepareStatement(sqlOldRec);
			psOldRec.setLong(1, userId);
			psOldRec.executeUpdate();
			String sql = "insert into trading_central_access values (SEQ_TRADING_CENTRAL_ACCESS.NEXTVAL, ?, ?, ?, ?, ?, ?)";
			ps = con.prepareStatement(sql);
			ps.setLong(1, userId);
			ps.setInt(2, site);
			ps.setTimestamp(3,  CommonUtil.convertToTimeStamp(CommonUtil.getDateTimeFormat(from, utcOffset)));
			ps.setTimestamp(4,  CommonUtil.convertToTimeStamp(CommonUtil.getDateTimeFormat(to, utcOffset)));
			ps.setBoolean(5, false);
			ps.setLong(6, writerId);
			return ps.executeUpdate() > 0;
		} finally {
			closeStatement(ps);
			closeStatement(psOldRec);
		}
	}
	
	  public static User getUserById(Connection con, long id)  throws SQLException{

		  PreparedStatement ps=null;
		  ResultSet rs=null;

		  User vo=null;

		  try
			{
			    String sql="select * from users u, users_active_data uad where u.id = uad.user_id AND u.id=?";

				ps = con.prepareStatement(sql);
				ps.setLong(1, id);

				rs=ps.executeQuery();

				if (rs.next()) {
					vo=new User(false);
					getVO(con,rs,vo);
					vo.setCurrency(CurrenciesDAOBase.getById(con, vo.getCurrencyId().intValue()));

				}
			}

			finally	{
				closeResultSet(rs);
				closeStatement(ps);
			}
			return vo;
	  }
}
