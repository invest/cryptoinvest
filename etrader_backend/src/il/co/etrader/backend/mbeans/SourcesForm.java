package il.co.etrader.backend.mbeans;

import java.io.Serializable;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;

import javax.faces.context.FacesContext;

import il.co.etrader.backend.bl_managers.AdminManager;
import il.co.etrader.backend.bl_managers.MarketingManager;
import il.co.etrader.backend.bl_vos.User;
import il.co.etrader.backend.util.Constants;
import il.co.etrader.backend.util.Utils;
import il.co.etrader.bl_vos.MarketingSource;
import il.co.etrader.util.CommonUtil;



public class SourcesForm implements Serializable {

	private static final long serialVersionUID = 522450867475611126L;

	private String sourceName;
	private ArrayList<MarketingSource> list;
	private MarketingSource source;

	public SourcesForm() throws SQLException{
		updateList();
	}

	public String updateList() throws SQLException{
		
	    FacesContext context = FacesContext.getCurrentInstance();
        User u = (User) context.getApplication().createValueBinding(Constants.BIND_USER).getValue(context);
        long writerIdForSkin = Constants.DEFAULT_WRITER_ID_FOR_SKIN;;

        if (u.isPartnerMarketingSelected()){ 
        	writerIdForSkin = Utils.getWriter().getWriter().getId();
        }

		list = MarketingManager.getMarketingSources(sourceName, true, writerIdForSkin);

		CommonUtil.setTablesToFirstPage();
		return null;
	}

	/**
	 * Update / insert source
	 * @return
	 * @throws SQLException
	 */
	public String updateInsert() throws SQLException{

		source.setWriterId(AdminManager.getWriterId());

		boolean res = MarketingManager.insertUpdateSource(source);
		if (res == false) {
			return null;
		}
		updateList();
		return Constants.NAV_MARKETING_SOURCES;
	}

	public int getListSize() {
		return list.size();
	}

	/**
	 * Navigate to edit source
	 * @return
	 * @throws SQLException
	 */
	public String navEdit() {
		return Constants.NAV_MARKETING_SOURCE;
	}

	/**
	 * Navigate to insert new source
	 * @return
	 * @throws SQLException
	 */
	public String navNew() throws SQLException {
		source = new MarketingSource();
		source.setWriterId(AdminManager.getWriterId());
		source.setTimeCreated(new Date());

		return Constants.NAV_MARKETING_SOURCE;
	}

	/**
	 * @return the list
	 */
	public ArrayList getList() {
		return list;
	}

	/**
	 * @param list the list to set
	 */
	public void setList(ArrayList list) {
		this.list = list;
	}

	/**
	 * @return the sourceName
	 */
	public String getSourceName() {
		return sourceName;
	}

	/**
	 * @param sourceName the sourceName to set
	 */
	public void setSourceName(String sourceName) {
		this.sourceName = sourceName;
	}

	/**
	 * @return the source
	 */
	public MarketingSource getSource() {
		return source;
	}

	/**
	 * @param source the source to set
	 */
	public void setSource(MarketingSource source) {
		this.source = source;
	}



}
