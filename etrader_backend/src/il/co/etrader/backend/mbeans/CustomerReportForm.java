package il.co.etrader.backend.mbeans;

import java.io.Serializable;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Hashtable;
import java.util.Locale;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.validator.EmailValidator;
import org.apache.log4j.Logger;

import com.anyoption.common.util.Utils;

import il.co.etrader.backend.bl_vos.User;
import il.co.etrader.backend.bl_vos.WriterWrapper;
import il.co.etrader.backend.util.Constants;
import il.co.etrader.backend.util.FormBase;
import il.co.etrader.bl_managers.ApplicationDataBase;
import il.co.etrader.bl_managers.CustomerReportManager;
import il.co.etrader.bl_vos.CustomerReportInvestments;
import il.co.etrader.bl_vos.CustomerReportSent;
import il.co.etrader.bl_vos.Skins;
import il.co.etrader.util.CommonUtil;

public class CustomerReportForm extends FormBase implements Serializable {

	private static final long serialVersionUID = -474689997688354959L;

	private static final Logger logger = Logger.getLogger(CustomerReportForm.class);
	
	


	private ArrayList<CustomerReportSent> reportList;

	private Long id = 0l;
	private Date from;
	private Date to;
	private long reportType = 0;
	private long sendMail;
	private String message;
	private CustomerReportSent customerReportSent;
	private long isResent;
	private ArrayList<SelectItem> selectIsResent = new ArrayList<SelectItem>();
	
    
	public CustomerReportForm() throws SQLException{

		GregorianCalendar gc = new GregorianCalendar();
		//set time as 00:00:00
		gc.set(Calendar.HOUR_OF_DAY, 0);
		gc.set(Calendar.MINUTE, 0);
		gc.set(Calendar.SECOND, 0);
		gc.add(Calendar.DAY_OF_MONTH, 1);
		to = gc.getTime();
		gc.add(Calendar.YEAR, -1);
		from = gc.getTime();
		
		isResent = -1;
		selectIsResent.add(new SelectItem(new Long(-1), CommonUtil.getMessage("general.all", null)));
		selectIsResent.add(new SelectItem(new Long(1), CommonUtil.getMessage("yes", null)));
		selectIsResent.add(new SelectItem(new Long(0), CommonUtil.getMessage("no", null)));
		
		search();		
	}

	public int getListSize() {
		if (reportList == null){
			return 0;
		}
		return reportList.size();
	}

	
	
	public String search() throws SQLException{
		clear();
		FacesContext context=FacesContext.getCurrentInstance();
		User user= (User)context.getApplication().createValueBinding(Constants.BIND_USER).getValue(context);
		reportList = CustomerReportManager.getCustomerReportSendAll(user.getId(), reportType, from, to, id, isResent);
		if(reportList.size() == 0){
			message = CommonUtil.getMessage("customer.report.emptylist", null);										
		}
		CommonUtil.setTablesToFirstPage();
		return null;
	}
	
	private void clear(){
		if(reportList != null){
			reportList.clear();
		}		
	}
	
	public String sendReport() throws SQLException{	
		FacesContext context=FacesContext.getCurrentInstance();
		User user= (User)context.getApplication().createValueBinding(Constants.BIND_USER).getValue(context);
		String email = user.getEmail();
		Date currDate = new Date();
		String sentDate = CommonUtil.getDateTimeFormatDisplay(currDate, CustomerReportInvestments.timeZoneName, "","dd/MM/yyyy HH:mm");;
		String comments = "Resent on " + sentDate;
		if(customerReportSent.getToSendMail() == customerReportSent.SEND_TO_OTHER){
			email = customerReportSent.getOtherEmail();
			String errMsg = "";
			if(CommonUtil.isParameterEmptyOrNull(email)){
				errMsg = CommonUtil.getMessage("customer.report.other.email.empty", null);
			}
			
			if(!EmailValidator.getInstance().isValid(email)){
				errMsg = CommonUtil.getMessage("customer.report.other.email.invalid", null);
			}
			
			if (!CommonUtil.isEmailValidUnix(email)) {
				errMsg = CommonUtil.getMessage("customer.report.other.email.invalid", null);
			}
			
			if(!CommonUtil.isParameterEmptyOrNull(errMsg)){
				FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR, errMsg, null);
				FacesContext.getCurrentInstance().addMessage("customerReportForm:data:" + reportList.indexOf(customerReportSent) + ":otherEmail", fm);
				return null;
			}
			
			comments += " to " + email; 
		}		
		

		WriterWrapper wr = (WriterWrapper) context.getApplication().createValueBinding(Constants.BIND_WRITER_WRAPPER).getValue(context);
		String writerComment = "";
		if(!CommonUtil.isParameterEmptyOrNull(customerReportSent.getNewComments())){
			writerComment = " " + customerReportSent.getNewComments();
		}
		comments += " by " + wr.getWriter().getUserName() + writerComment;
		String htmlReport = customerReportSent.getSentMsg();
		//replace sendDate
		htmlReport = htmlReport.replaceAll("<!--begin_sent_time-->" + ".*"+"<!--end_sent_time-->", sentDate);
		
		sendEmail(email, htmlReport);
		CustomerReportManager.updateCustomerReportReSent(customerReportSent.getId(), comments);
		logger.debug("Re-Sent the Customer Report:" + customerReportSent.getId() + " to userId:" + customerReportSent.getUserId());
		
		reportList.clear();
		String[] params=new String[3];
		params[0] = customerReportSent.getId() + "";
		params[1] = email;
		message =  CommonUtil.getMessage("customer.report.sent.report", params);				
		
		return null;
	}
	
	private void sendEmail(String toEmail, String body){
		 Hashtable<String, String> serverProperties = new Hashtable<String, String>();
		 Hashtable<String, String> emailProperties = new Hashtable<String, String>();
		 
		serverProperties.put("url",Utils.getProperty("email.server"));
		serverProperties.put("auth","true");
		serverProperties.put("user",Utils.getProperty("email.uname"));
		serverProperties.put("pass",Utils.getProperty("email.pass"));
		
		// Set the email properties
		emailProperties = new Hashtable<String, String>();
		Locale locale = new Locale("iw");
		emailProperties.put("subject",CommonUtil.getMessage(locale, "customer.report.subject", null));
		emailProperties.put("from", ApplicationDataBase.getSupportEmailBySkinId(Skins.SKIN_ETRADER));
		emailProperties.put("body", body);
		emailProperties.put("to", toEmail);
		
		Utils.sendEmail(serverProperties, emailProperties, null);
	}
	
	
	
//	  public  void htmlToPdfFile() {
//		  try {
//			    String html = customerReportSent.getSentMsg();
//			    OutputStream file = new FileOutputStream(new File("E:\\Test.pdf"));
//			    Document document = new Document();
//			    PdfWriter.getInstance(document, file);
//			    document.open();
//			    HTMLWorker htmlWorker = new HTMLWorker(document);
//			    htmlWorker.parse(new StringReader(html));
//			    document.close();
//			    file.close();
//			    
////			    //!!! DOWNLOAD FILES
////			    FacesContext fc = FacesContext.getCurrentInstance();
////			    HttpServletResponse response = (HttpServletResponse) fc.getExternalContext().getResponse();
////
////			    response.reset(); // Some JSF component library or some Filter might have set some headers in the buffer beforehand. We want to get rid of them, else it may collide.
////			    response.setContentType(".pdf"); // Check http://www.iana.org/assignments/media-types for all types. Use if necessary ServletContext#getMimeType() for auto-detection based on filename.
////			    //response.setContentLength(contentLength); // Set it with the file size. This header is optional. It will work if it's omitted, but the download progress will be unknown.
////			    response.setHeader("Content-Disposition", "attachment; filename=\"" + file + "\""); // The Save As popup magic is done here. You can give it any file name you want, this only won't work in MSIE, it will use current request URL as file name instead.
////
////			    OutputStream output = response.getOutputStream();
////			    // Now you can write the InputStream of the file to the above OutputStream the usual way.
////			    // ...
////
////			    fc.responseComplete();
////			    
////			    
////		        //Setup the output
////		        String contentType = "application/vnd.ms-excel";
////		        FacesContext fc = FacesContext.getCurrentInstance();
////		        filename = "chargebacks_report_"+ filename;
////		        HttpServletResponse response = (HttpServletResponse)fc.getExternalContext().getResponse();
////		        response.setHeader("Content-disposition", "attachment; filename=" + filename);
////		        response.setContentType(contentType);
////		        response.setCharacterEncoding("UTF-8");
////
////		        //Write the table back out
////		        PrintWriter out = response.getWriter();
////		        out.print(htmlBuffer);
////		        out.close();
////		        fc.responseComplete();
//			    
//			} catch (Exception e) {
//			    e.printStackTrace();
//			}
//	  }	  	  
//	  

	public String getHtml(){
		HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
        String id = (String) request.getParameter("id");        
		return getHtmlById(new Long (id));
	}
	
	private String getHtmlById(long id){
		String html = "";
		for(CustomerReportSent cr : reportList){
			if(cr.getId() == id){
				html = cr.getSentMsg();
			}
		}
		return html;
	}
	
	public long getCurrentIndex(CustomerReportSent crs){
		return reportList.indexOf(crs);
	}
	
	public ArrayList<CustomerReportSent> getReportList() {
		return reportList;
	}

	public void setReportList(ArrayList<CustomerReportSent> reportList) {
		this.reportList = reportList;
	}

	public Date getFrom() {
		return from;
	}

	public void setFrom(Date from) {
		this.from = from;
	}

	public Date getTo() {
		return to;
	}

	public void setTo(Date to) {
		this.to = to;
	}

	public long getReportType() {
		return reportType;
	}

	public void setReportType(long reportType) {
		this.reportType = reportType;
	}
	
	public ArrayList<CustomerReportSent> getList() {
		return reportList;
	}

	public long getSendMail() {
		return sendMail;
	}

	public void setSendMail(long sendMail) {
		this.sendMail = sendMail;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public CustomerReportSent getCustomerReportSent() {
		return customerReportSent;
	}

	public void setCustomerReportSent(CustomerReportSent customerReportSent) {
		this.customerReportSent = customerReportSent;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		if (id == null) {
			this.id = 0l;
		} else {
			this.id = id;
		}
	}

	public long getIsResent() {
		return isResent;
	}

	public void setIsResent(long isResent) {
		this.isResent = isResent;
	}

	public ArrayList<SelectItem> getSelectIsResent() {
		return selectIsResent;
	}

	public void setSelectIsResent(ArrayList<SelectItem> selectIsResent) {
		this.selectIsResent = selectIsResent;
	}

}

