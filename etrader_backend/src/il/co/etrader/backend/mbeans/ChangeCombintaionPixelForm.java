package il.co.etrader.backend.mbeans;

import java.io.Serializable;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import org.apache.log4j.Logger;

import il.co.etrader.backend.bl_managers.MarketingManager;
import il.co.etrader.bl_vos.MarketingCombination;

public class ChangeCombintaionPixelForm implements Serializable {

	private static final long serialVersionUID = 522450867475611126L;
    private static Logger log = Logger.getLogger(ChangeCombintaionPixelForm.class);

    protected long sourceId;
    protected long pixelId;
    protected long campaignId;
    protected long pixelTypeId;
    protected ArrayList<MarketingCombination> list;
    private boolean inAdditionToExistingPixel;

	public ChangeCombintaionPixelForm() {
		inAdditionToExistingPixel = false;
        list = new ArrayList<MarketingCombination>();
	}

    public String update() {
        FacesContext context = FacesContext.getCurrentInstance();

        if (sourceId == 0 && campaignId == 0) {
            FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_INFO, "You must choose Campaign or Source", null);
            context.addMessage("changeCombintaionPixelForm:campaignId", fm);
            context.addMessage("changeCombintaionPixelForm:sourceId", fm);
        } else {
            String msg = "Cant update Combination Pixel";
            try {
            	msg = MarketingManager.updateOrInsertCombintaionPixel(sourceId, pixelId, campaignId, inAdditionToExistingPixel);
            } catch (SQLException e) {
                log.debug("Cant update Combination Pixel. sourceId = " + sourceId + " pixelId = " + pixelId + " campaignId = " + campaignId, e);
            }
            FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_INFO, msg, null);
            context.addMessage(null, fm);
        }
        return null;
    }

    /**
     * @return the pixelId
     */
    public long getPixelId() {
        return pixelId;
    }

    /**
     * @param pixelId the pixelId to set
     */
    public void setPixelId(long pixelId) {
        this.pixelId = pixelId;
    }

    /**
     * @return the sourceId
     */
    public long getSourceId() {
        return sourceId;
    }

    /**
     * @param sourceId the sourceId to set
     */
    public void setSourceId(long sourceId) {
        this.sourceId = sourceId;
    }

    /**
     * @return the campaignId
     */
    public long getCampaignId() {
        return campaignId;
    }

    /**
     * @param campaignId the campaignId to set
     */
    public void setCampaignId(long campaignId) {
        this.campaignId = campaignId;
    }

    /**
     * @return the list
     */
    public ArrayList<MarketingCombination> getList() {
        return list;
    }

    /**
     * @param list the list to set
     */
    public void setList(ArrayList<MarketingCombination> list) {
        this.list = list;
    }

    public long getListSize() {
        return list.size();
    }

    /**
     * @return the pixel_type
     */
    public long getPixelTypeId() {
        return pixelTypeId;
    }

    /**
     * @param pixel_type the pixel_type to set
     */
    public void setPixelTypeId(long pixelTypeId) {
        this.pixelTypeId = pixelTypeId;
    }

	/**
	 * @return the inAdditionToExistingPixel
	 */
	public boolean isInAdditionToExistingPixel() {
		return inAdditionToExistingPixel;
	}

	/**
	 * @param inAdditionToExistingPixel the inAdditionToExistingPixel to set
	 */
	public void setInAdditionToExistingPixel(boolean inAdditionToExistingPixel) {
		this.inAdditionToExistingPixel = inAdditionToExistingPixel;
	}

}
