package il.co.etrader.backend.mbeans;

import java.io.Serializable;
import java.sql.SQLException;
import java.util.ArrayList;

import org.apache.log4j.Logger;

import il.co.etrader.backend.dao_managers.AnalyticsManager;
import il.co.etrader.backend.util.Utils;
import il.co.etrader.bl_vos.Analytics;
import il.co.etrader.util.CommonUtil;

/**
 * AnalyticsFtdForm
 * @author eyalo
 */
public class AnalyticsFtdForm implements Serializable {
	private static final long serialVersionUID = 1L;
	private static final Logger logger = Logger.getLogger(AnalyticsFtdForm.class);
	/* Info Parameters*/
	private ArrayList<Analytics> list;
	private Analytics analyticsTotal;
	
	/**
	 * AnalyticsFtdForm
	 */
	public AnalyticsFtdForm() {
		init();
	}
	
	/**
	 * initial
	 */
	public void init() {
		list = new ArrayList<Analytics>();
		analyticsTotal = new Analytics();
		search();
	}
	
	/**
	 * search
	 */
	public void search() {
		logger.info("Start search analytics ftd.");
		try {
			list = AnalyticsManager.getAnalyticsFtd();
			loadAnalyticsTotal();
		} catch (SQLException e){
			logger.error("Can't load analytics ftd :", e);
		}
		logger.info("End search analytics ftd.");
	}
	
	public void loadAnalyticsTotal() {
		logger.info("Start load analytics total.");
		long ftdCount = 0;
		double depositAmount = 0;
		for (Analytics analytics : list) {
			ftdCount += analytics.getFtdCount();
			depositAmount += analytics.getDepositAmount();
		}
		analyticsTotal.setFtdCount(ftdCount);
		analyticsTotal.setDepositAmount(depositAmount);
		logger.info("End load analytics total.");
	}
	
	/**
	 * Get time data fetched
	 * @param utcOffSet
	 * @return String
	 */
	public String getTimeDataFetchedTxt() {
		return CommonUtil.getDateTimeFormatDisplay(list.get(0).getTimeDataFetched(), Utils.getWriter().getUtcOffset(), " ");
	}

	/**
	 * @return the list
	 */
	public ArrayList<Analytics> getList() {
		return list;
	}

	/**
	 * @param list the list to set
	 */
	public void setList(ArrayList<Analytics> list) {
		this.list = list;
	}

	/**
	 * @return the analyticsTotal
	 */
	public Analytics getAnalyticsTotal() {
		return analyticsTotal;
	}

	/**
	 * @param analyticsTotal the analyticsTotal to set
	 */
	public void setAnalyticsTotal(Analytics analyticsTotal) {
		this.analyticsTotal = analyticsTotal;
	}


}
