package il.co.etrader.backend.mbeans;

import java.io.Serializable;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;

import javax.faces.application.FacesMessage;
import javax.faces.component.UICommand;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.Issue;
import com.anyoption.common.beans.base.UserRegulationBase;
import com.anyoption.common.managers.UserRegulationManager;

import il.co.etrader.backend.bl_managers.PendingDocumentsManager;
import il.co.etrader.backend.bl_managers.UsersManager;
import il.co.etrader.backend.bl_vos.PendingDocument;
import il.co.etrader.backend.dao_managers.PendingDocumentsDAOFilter;
import il.co.etrader.backend.util.Utils;
import il.co.etrader.bl_managers.IssuesManagerBase;
import il.co.etrader.util.CommonUtil;

public class PendingDocumentsForm implements Serializable{
	
	private static final long serialVersionUID = -3613141151596692463L;
	
	public static final int ORDER_BY_DEPOSIT_DATE_ASC	= 1;
	public static final int ORDER_BY_DEPOSIT_DATE_DESC	= 2;
	public static final int ORDER_BY_TIME_REACHED_ASC	= 3;
	public static final int ORDER_BY_TIME_REACHED_DESC	= 4;
	
	private static final Logger logger = Logger.getLogger(PendingDocumentsForm.class);
	// Filters
	Long userId = 0l;			// any
	String userName = "";		// any
	int interested = 0;			// ALL (yes/no) 
	int daysConditionQualified = 1;		// not reached 30 days
	int allDocsUploaded = 1;	// ALL (yes/no)
	int uploadedDocs = 0;		// ALL 
	int reached = 0;			// ALL
	int reachedSinceReject = 0;	// ALL
	int skinId = 0;             // ALL
	char equality;				// <, =, >
	int lastLogin = 1;
	boolean excludeTesters = true;
	int isUserActive = 1;
	int isUserBlocked = 1;
	private String totalDepositAmount = "";
	
	
	
	
	boolean supportReject = false;
	boolean controlReject = false;
	
	boolean supportScreen = true;
	boolean regulatedUsers = false;
	// Order by;
	int order = ORDER_BY_DEPOSIT_DATE_ASC;
    // Paging.
	private int totalRows;
    private int firstRow;
    private int rowsPerPage;
    private int totalPages;
    private int pageRange;
    private Integer[] pages;
    private int currentPage;
    
    // sorting
    private String sortColumn;
	private String prevSortColumn;
    private boolean sortAscending = true;
    private boolean activateAscending;

	
	ArrayList<PendingDocument> list;
	
	
	public String sortList() {
		if (activateAscending) {
			if (sortColumn.equals(prevSortColumn)) {
				sortAscending = !sortAscending;
			} else {
				sortAscending = true;
			}
		}

		if (sortColumn.equals("totalDepositAmount")) {
			if (sortAscending) {
				Collections.sort(getList(),(p1, p2) -> new Long(p1.getTotalDepositAmount()).compareTo(new Long(p2.getTotalDepositAmount())));
			} else {
				Collections.sort(getList(),(p1, p2) -> -new Long(p1.getTotalDepositAmount()).compareTo(new Long(p2.getTotalDepositAmount())));
			}
		}

		activateAscending = true;
		prevSortColumn = sortColumn;

		return null;
	}
	
	public PendingDocumentsForm(){
		
		 rowsPerPage = 10;
	     pageRange = 10;
	     daysConditionQualified = 0;
	     list = new ArrayList<PendingDocument>();
	}
	
	public int getListSize() {
	    return list.size();
	}
	
	public String search() {
	    initList();
	    return null;
	}

	public String initList() {
        // Load list and totalCount.
		if(sortColumn == null || !sortColumn.equals("totalDepositAmount")){
			loadData(true);
		}
        // Set currentPage, totalPages and pages.
        currentPage = (totalRows / rowsPerPage) - ((totalRows - firstRow) / rowsPerPage) + 1;
        totalPages = (totalRows / rowsPerPage) + ((totalRows % rowsPerPage != 0) ? 1 : 0);
        int pagesLength = Math.min(pageRange, totalPages);
        pages = new Integer[pagesLength];

        // firstPage must be greater than 0 and lesser than totalPages-pageLength.
        int firstPage = Math.min(Math.max(0, currentPage - (pageRange / 2)), totalPages - pagesLength);

        // Create pages (page numbers for page links).
        for (int i = 0; i < pagesLength; i++) {
            pages[i] = ++firstPage;
        }
        
		sortColumn = null;
        return null;
    }
	
	public void filterChanged(){
		firstRow = 0;
	}
	
	public void loadData(boolean countTotalRows){
		
		try {
			if(regulatedUsers) {
				list =		PendingDocumentsManager.getPendingDocumentsRegulated(createParams());
				if(countTotalRows){
					totalRows = PendingDocumentsManager.getPendingDocumentsRegulatedCount(createParams() );
				}
			} else {
				list 	=		PendingDocumentsManager.getPendingDocumentsNotRegulated(createParams());
				if(countTotalRows) {
					totalRows =		PendingDocumentsManager.getPendingDocumentsNotRegulatedCount(createParams());
				}
			}
		} catch (SQLException e) {
			logger.error("Can't load pending documents list :" + e);
		}
	}
	
	public String approve(PendingDocument p) {
		
			if(!supportScreen) {
				if(p.getComment()==null || p.getComment().length()<1) {
					FacesContext context = FacesContext.getCurrentInstance();
                    FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR,"Comment is mandatory field!" ,null);
                    context.addMessage(null, fm);
                    return null;
				}
			}
			UserRegulationBase urb = new UserRegulationBase();
			if(supportScreen){
				urb.setApprovedRegulationStep(UserRegulationBase.REGULATION_ALL_DOCUMENTS_SUPPORT_APPROVAL);
			} else {
				urb.setApprovedRegulationStep(UserRegulationBase.REGULATION_CONTROL_APPROVED_USER);
			}
			urb.setUserId(p.getUserId());
			urb.setWriterId(Utils.getWriter().getWriter().getId());
			urb.setComments(p.getComment());
			ArrayList<Issue> issuesList = new ArrayList<Issue>();
			issuesList = UserRegulationManager.getOpenedPendingDocsIssuesByUserId(urb.getUserId());
			if (issuesList != null) {
				try {
					UserRegulationManager.updateRegulationStep(urb);
					if(supportScreen){
						UsersManager.unlockUser(p.getUserId(), false, Utils.getWriter().getWriter().getId());
					}
					pendingDocumentsIssueActions(issuesList, urb);
					list.remove(p);
				} catch (SQLException e) {
					logger.debug("Can't support approve user: " + userId, e);
				}
			}
		
		return null;
	}
	
	private void pendingDocumentsIssueActions(ArrayList<Issue> issuesList, UserRegulationBase urb) {
		try {
			String issueActionComments = "Moved to control by: " + CommonUtil.getWriterName(urb.getWriterId());
			for (Issue issue : issuesList) {
				issue.setStatusId(IssuesManagerBase.ISSUE_STATUS_FINISHED + "");
				IssuesManagerBase.insertUserRegulationIssueAction(urb.getUserId(),
						IssuesManagerBase.ISSUE_ACTION_TYPE_PENDING_DOCS + "", issueActionComments,
						IssuesManagerBase.ISSUE_PRIORITY_LOW + "", false, issue, false);
			}
		} catch (SQLException e) {
			logger.debug("Cannot get writer name: ", e);
		}
	}
	
	public void sortByDepositDate(){
		if(order==ORDER_BY_DEPOSIT_DATE_ASC) {
			order = ORDER_BY_DEPOSIT_DATE_DESC;
		} else {
			order = ORDER_BY_DEPOSIT_DATE_ASC;
		}
		firstRow = 0;
		search();
	}

	public void sortByTimeReached(){
		if(order==ORDER_BY_TIME_REACHED_ASC){
			order = ORDER_BY_TIME_REACHED_DESC;
		} else {
			order = ORDER_BY_TIME_REACHED_ASC;
		}
		firstRow = 0;
		search();
	}

	
	public void reset() {
		userId = 0l;
		userName = "";
		interested = 0;
		daysConditionQualified = 1;
		allDocsUploaded = 0;
		uploadedDocs = -1;
		reached = 0;	
		lastLogin = 1;
		isUserActive = -1;
		totalDepositAmount = "";
		if(regulatedUsers){
			isUserBlocked = -1;
		}else{
			isUserBlocked = getIsUserBlocked();
		}
	}
	
   public void pageFirst() {
        page(0);
    }

    public void pageNext() {
        page(firstRow + rowsPerPage);
    }

    public void pagePrevious() {
        page(firstRow - rowsPerPage);
    }

    public void pageLast() {
        page(totalRows - ((totalRows % rowsPerPage != 0) ? totalRows % rowsPerPage : rowsPerPage));
    }

    public void page(ActionEvent event) {
        page((((Integer) ((UICommand) event.getComponent()).getValue()) - 1) * rowsPerPage);
    }

    private void page(int firstRow) {
        this.firstRow = firstRow;
        loadData(false); // Load requested page.
    }

	public ArrayList<PendingDocument> getList() {
		return list;
	}

	public void setList(ArrayList<PendingDocument> list) {
		this.list = list;
	}

	public Long getUserId() {
		
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public int getInterested() {
		return interested;
	}

	public void setInterested(int interested) {
		this.interested = interested;
	}

	public boolean isSupportScreen() {
		return supportScreen;
	}

	public void setSupportScreen(boolean supportScreen) {
		this.supportScreen = supportScreen;
	}

	public boolean isRegulatedUsers() {
		return regulatedUsers;
	}

	public void setRegulatedUsers(boolean regulatedUsers) {
		this.regulatedUsers = regulatedUsers;
	}

	public int getTotalRows() {
		return totalRows;
	}

	public void setTotalRows(int totalRows) {
		this.totalRows = totalRows;
	}

	public int getFirstRow() {
		return firstRow;
	}

	public void setFirstRow(int firstRow) {
		this.firstRow = firstRow;
	}

	public int getRowsPerPage() {
		return rowsPerPage;
	}

	public void setRowsPerPage(int rowsPerPage) {
		this.rowsPerPage = rowsPerPage;
	}

	public int getTotalPages() {
		return totalPages;
	}

	public void setTotalPages(int totalPages) {
		this.totalPages = totalPages;
	}

	public int getPageRange() {
		return pageRange;
	}

	public void setPageRange(int pageRange) {
		this.pageRange = pageRange;
	}

	public Integer[] getPages() {
		return pages;
	}

	public void setPages(Integer[] pages) {
		this.pages = pages;
	}

	public int getCurrentPage() {
		return currentPage;
	}

	public void setCurrentPage(int currentPage) {
		this.currentPage = currentPage;
	}

	public int getReached() {
		return reached;
	}

	public void setReached(int reached) {
		this.reached = reached;
	}

	public int getAllDocsUploaded() {
		return allDocsUploaded;
	}

	public void setAllDocsUploaded(int allDocsUploaded) {
		this.allDocsUploaded = allDocsUploaded;
	}

	public int getUploadedDocs() {
		return uploadedDocs;
	}

	public void setUploadedDocs(int uploadedDocs) {
		this.uploadedDocs = uploadedDocs;
	}

	public boolean isSupportReject() {
		return supportReject;
	}

	public void setSupportReject(boolean supportReject) {
		this.supportReject = supportReject;
	}

	public boolean isControlReject() {
		return controlReject;
	}

	public void setControlReject(boolean controlReject) {
		this.controlReject = controlReject;
	}

	public int getReachedSinceReject() {
		return reachedSinceReject;
	}

	public void setReachedSinceReject(int reachedSinceReject) {
		this.reachedSinceReject = reachedSinceReject;
	}

	public int getDaysConditionQualified() {
		return daysConditionQualified;
	}

	public void setDaysConditionQualified(int daysConditionQualified) {
		this.daysConditionQualified = daysConditionQualified;
	}

	private PendingDocumentsDAOFilter createParams(){
		// reason: there is isUserBlocked filter for NotRegulated pending documents, while for regulated there is NOT
		if (regulatedUsers) {
			isUserBlocked = -1;
		} else {
			isUserBlocked = getIsUserBlocked();
		}
		
		PendingDocumentsDAOFilter p = new PendingDocumentsDAOFilter(userId, userName, interested, reached, 
																	allDocsUploaded, uploadedDocs, order, supportReject, 
																	controlReject, firstRow, rowsPerPage, supportScreen, 
																	daysConditionQualified,equality,skinId,lastLogin,
																	excludeTesters,isUserActive, regulatedUsers, isUserBlocked, totalDepositAmount);
		return p;
	}

	public int getSkinId() {
		return skinId;
	}

	public void setSkinId(int skinId) {
		this.skinId = skinId;
	}

	public char getEquality() {
		return equality;
	}

	public void setEquality(char equality) {
		this.equality = equality;
	}

	public int getLastLogin() {
		return lastLogin;
	}

	public void setLastLogin(int lastLogin) {
		this.lastLogin = lastLogin;
	}

	public boolean isExcludeTesters() {
		return excludeTesters;
	}

	public void setExcludeTesters(boolean excludeTesters) {
		this.excludeTesters = excludeTesters;
	}
	
	public int getIsUserActive() {
		return isUserActive;
	}

	public void setIsUserActive(int isUserActive) {
		this.isUserActive = isUserActive;
	}

	public int getIsUserBlocked() {
		return isUserBlocked;
	}

	public void setIsUserBlocked(int isUserBlocked) {
		this.isUserBlocked = isUserBlocked;
	}

	public String getTotalDepositAmount() {
		return totalDepositAmount;
	}

	public void setTotalDepositAmount(String totalDepositAmount) {
		this.totalDepositAmount = totalDepositAmount;
	}

	public String getSortColumn() {
		return sortColumn;
	}

	public void setSortColumn(String sortColumn) {
		this.sortColumn = sortColumn;
	}

	public String getPrevSortColumn() {
		return prevSortColumn;
	}

	public void setPrevSortColumn(String prevSortColumn) {
		this.prevSortColumn = prevSortColumn;
	}

	
}