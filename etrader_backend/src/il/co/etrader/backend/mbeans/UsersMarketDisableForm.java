package il.co.etrader.backend.mbeans;
import java.io.Serializable;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.GregorianCalendar;

import org.apache.log4j.Logger;

import il.co.etrader.backend.bl_managers.AdminManager;
import il.co.etrader.backend.bl_managers.UsersManager;
import il.co.etrader.backend.util.Constants;
import il.co.etrader.backend.util.Utils;
import il.co.etrader.bl_managers.UsersManagerBase;
import il.co.etrader.bl_vos.UserMarketDisable;
import il.co.etrader.util.CommonUtil;


public class UsersMarketDisableForm implements Serializable {

	private static final long serialVersionUID = -374991586357133341L;
	private static final Logger logger = Logger.getLogger(UsersMarketDisableForm.class);

	private UserMarketDisable userMarketDisable;
	private ArrayList<UserMarketDisable> list;
	
	private String sortColumn;
	private String prevSortColumn;
    private boolean sortAscending = false;

	private Date from;
	private Date to;
	private String userId; //userId_apiExternalUserId
	private long active;
	private long marketId;
    private long isDev3;
    private long skinId;

	public UsersMarketDisableForm() throws SQLException{

		GregorianCalendar gc = new GregorianCalendar();
		from = gc.getTime();
		gc.set(Calendar.YEAR, gc.get(Calendar.YEAR) + Constants.USER_MARKET_DISABLE_PERID);
		to = gc.getTime();
		active = 0;
		marketId = 0;
        isDev3 = 0;
        skinId = 0;
        sortColumn="";
        prevSortColumn="";
        
		search();
	}

	public int getListSize() {
		return list.size();
	}


	/**
	 * Get users market disable list
	 * @return
	 * @throws SQLException
	 */
	public String search() throws SQLException {
		long userIdTmp = 0;
		long apiExternalUserId = 0;
		try {
			if (null != userId && !userId.equals("0")) {
				String[] userIdArr = userId.split("_");
				userIdTmp = Long.parseLong(userIdArr[0]);
				apiExternalUserId = Long.parseLong(userIdArr[1]);
			}
		} catch (NumberFormatException e) {
			logger.error("cant get user id and external user id " + userId + " using 0 for both");
		}
		list = UsersManager.getUsersMarketDisableList(userIdTmp, from, to, active, marketId, isDev3, skinId, apiExternalUserId);
		CommonUtil.setTablesToFirstPage();
		return null;
	}

	/**
	 * Navigate to edit user market disable page
	 * @return
	 */
	public String navEdit() {
		Calendar c1 = Calendar.getInstance();
		Calendar c2 = Calendar.getInstance();
		// set date to writer offset
		Date startWithOffset = CommonUtil.getDateTimeFormaByTz(userMarketDisable.getStartDate(), Utils.getWriter().getUtcOffset());
		Date endWithOffset = CommonUtil.getDateTimeFormaByTz(userMarketDisable.getEndDate(), Utils.getWriter().getUtcOffset());
		c1.setTime(startWithOffset);
		c2.setTime(endWithOffset);
		userMarketDisable.setStartDateHour(String.valueOf(c1.get(Calendar.HOUR_OF_DAY)));
		userMarketDisable.setStartDateMin(String.valueOf(c1.get(Calendar.MINUTE)));

		//	calc period in minuts
		long perdionMis = c2.getTimeInMillis() - c1.getTimeInMillis();
		long periodMin = perdionMis / (1000*60);
		userMarketDisable.setPeriod(periodMin);

		return Constants.NAV_USER_MARKET_DISABLE;
	}


	/**
	 * Navigate to insert new user market disable page
	 * @return
	 */
	public String navNew() {
		userMarketDisable = initUserMarketDisable();

		return Constants.NAV_USER_MARKET_DISABLE;
	}

	public static UserMarketDisable initUserMarketDisable() {
		UserMarketDisable userMarketDisableTemp = new UserMarketDisable();
		Calendar c1 = Calendar.getInstance();

		// set current date to writer offset
		Date startWithOffset = CommonUtil.getDateTimeFormaByTz(c1.getTime(), Utils.getWriter().getUtcOffset());
		c1.setTime(startWithOffset);

		userMarketDisableTemp.setStartDate(c1.getTime());
		userMarketDisableTemp.setStartDateHour(String.valueOf(c1.get(Calendar.HOUR_OF_DAY)));
		userMarketDisableTemp.setStartDateMin(String.valueOf(c1.get(Calendar.MINUTE)));
		return userMarketDisableTemp;
	}
	/**
	 * Update / insert user market disable
	 * @return
	 * @throws SQLException
	 */
	public String updateInsert() throws SQLException{

		// calc start date
		Calendar c1 = Calendar.getInstance();
		c1.setTime(userMarketDisable.getStartDate());
		c1.set(Calendar.HOUR_OF_DAY, Integer.parseInt(userMarketDisable.getStartDateHour()));
		c1.set(Calendar.MINUTE, Integer.parseInt(userMarketDisable.getStartDateMin()));
		userMarketDisable.setStartDate(c1.getTime());

		userMarketDisable = setUserMarketDisableEndDate(userMarketDisable);

		UsersManagerBase.insertUsersDetailsHistoryOneField(userMarketDisable.getWriterId(), userMarketDisable.getUserId(), userMarketDisable.getUserName(), null, 
				UsersManagerBase.USERS_DETAILS_HISTORY_TYPE_USER_DISABLE_DEV3, CommonUtil.getBooleanAsString(userMarketDisable.isDev3()), CommonUtil.getBooleanAsString(userMarketDisable.isDev3Temp()));
		UsersManagerBase.insertUsersDetailsHistoryOneField(userMarketDisable.getWriterId(), userMarketDisable.getUserId(), userMarketDisable.getUserName(), null, 
				UsersManagerBase.USERS_DETAILS_HISTORY_TYPE_USER_DISABLE_ACTIVE, CommonUtil.getBooleanAsString(userMarketDisable.isActive()), CommonUtil.getBooleanAsString(userMarketDisable.isActiveTemp()));
		userMarketDisable.updateIsActive();
		userMarketDisable.updateIsDev3();
		boolean res = UsersManager.insertUpdateUsersMarketDisable(userMarketDisable);
		if (res == false) {
			return null;
		}
		search();
		return Constants.NAV_USERS_MARKET_DISABLE;
	}

	public static UserMarketDisable setUserMarketDisableEndDate(UserMarketDisable umd) throws SQLException {
//		 calc end date by period
		Calendar c = Calendar.getInstance();
		c.setTime(umd.getStartDate());
		c.add(Calendar.YEAR, Constants.USER_MARKET_DISABLE_PERID);
		umd.setEndDate(c.getTime());
		umd.setWriterId(AdminManager.getWriterId());
		return umd;
	}

	public String sortList() {
		if (sortColumn.equals(prevSortColumn)) {
			sortAscending=!sortAscending;
		} else {
			sortAscending=true;
		}
		prevSortColumn=sortColumn;

		if (sortColumn.equals("winlose")) {
			Collections.sort(list, new WinLoseComparator(sortAscending));
		}
		CommonUtil.setTablesToFirstPage();

		return null;

	}
	
	public String getPrevSortColumn() {
		return prevSortColumn;
	}

	public void setPrevSortColumn(String prevSortColumn) {
		this.prevSortColumn = prevSortColumn;
	}

	public String getSortColumn() {
		return sortColumn;
	}

	public void setSortColumn(String sortColumn) {
		this.sortColumn = sortColumn;
	}
	
	public boolean isSortAscending(){
        return sortAscending;
    }

    public void setSortAscending(boolean sortAscending){
        this.sortAscending = sortAscending;
    }

	private class WinLoseComparator implements Comparator {
		private boolean ascending;
		public WinLoseComparator(boolean asc) {
			ascending=asc;
		}
   	    public int compare(Object o1, Object o2) {
   		 		UserMarketDisable op1=(UserMarketDisable)o1;
   		 	UserMarketDisable op2=(UserMarketDisable)o2;
   		 		if (ascending){
   		 			return new Long((long) op1.getWinLose()).compareTo(new Long((long) op2.getWinLose()));
   		 		}
   		 		return -new Long((long) op1.getWinLose()).compareTo(new Long((long) op2.getWinLose()));
		}
  	 }

	/**
	 * @return the from
	 */
	public Date getFrom() {
		return from;
	}

	/**
	 * @param from the from to set
	 */
	public void setFrom(Date from) {
		this.from = from;
	}

	/**
	 * @return the to
	 */
	public Date getTo() {
		return to;
	}

	/**
	 * @param to the to to set
	 */
	public void setTo(Date to) {
		this.to = to;
	}

	/**
	 * @return the userId
	 */
	public String getUserId() {
		return userId;
	}

	/**
	 * @param userId the userId to set
	 */
	public void setUserId(String userId) {
		this.userId = userId;
	}

	/**
	 * @return the userMarketDisable
	 */
	public UserMarketDisable getUserMarketDisable() {
		return userMarketDisable;
	}

	/**
	 * @param userMarketDisable the userMarketDisable to set
	 */
	public void setUserMarketDisable(UserMarketDisable userMarketDisable) {
		this.userMarketDisable = userMarketDisable;
	}

	/**
	 * @return the list
	 */
	public ArrayList<UserMarketDisable> getList() {
		return list;
	}

	/**
	 * @param list the list to set
	 */
	public void setList(ArrayList<UserMarketDisable> list) {
		this.list = list;
	}

	/**
	 * @return the active
	 */
	public long getActive() {
		return active;
	}

	/**
	 * @param active the active to set
	 */
	public void setActive(long active) {
		this.active = active;
	}

	/**
	 * @return the marketId
	 */
	public long getMarketId() {
		return marketId;
	}

	/**
	 * @param marketId the marketId to set
	 */
	public void setMarketId(long marketId) {
		this.marketId = marketId;
	}
	
	/**
	 * @return the skinId
	 */
	public long getSkinId() {
		return skinId;
	}

	/**
	 * @param skinId the skinId to set
	 */
	public void setSkinId(long skinId) {
		this.skinId = skinId;
	}

    /**
     * @return the isDev3
     */
    public long getIsDev3() {
        return isDev3;
    }

    /**
     * @param isDev3 the isDev3 to set
     */
    public void setIsDev3(long isDev3) {
        this.isDev3 = isDev3;
    }
}
