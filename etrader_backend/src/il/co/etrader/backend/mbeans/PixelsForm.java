package il.co.etrader.backend.mbeans;

import java.io.Serializable;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;

import javax.faces.context.FacesContext;

import il.co.etrader.backend.bl_managers.AdminManager;
import il.co.etrader.backend.bl_managers.MarketingManager;
import il.co.etrader.backend.bl_vos.User;
import il.co.etrader.backend.util.Constants;
import il.co.etrader.backend.util.Utils;
import il.co.etrader.bl_vos.MarketingPixel;
import il.co.etrader.util.CommonUtil;
import il.co.etrader.util.ConstantsBase;



public class PixelsForm implements Serializable {

	private static final long serialVersionUID = 522450867475611126L;

	private String pixelName;
	private ArrayList<MarketingPixel> list;
	private MarketingPixel pixel;
	private String word;

	public PixelsForm() throws SQLException{
		updateList();
	}

	public String updateList() throws SQLException{
		
	    FacesContext context = FacesContext.getCurrentInstance();
        User u = (User) context.getApplication().createValueBinding(Constants.BIND_USER).getValue(context);
        long writerIdForSkin = Constants.DEFAULT_WRITER_ID_FOR_SKIN;;

        if (u.isPartnerMarketingSelected()){ 
        	writerIdForSkin = Utils.getWriter().getWriter().getId();
        }

		list = MarketingManager.getMarketingPixels(pixelName, writerIdForSkin);

		CommonUtil.setTablesToFirstPage();
		return null;
	}

	/**
	 * Update / insert pixel
	 * @return
	 * @throws SQLException
	 */
	public String updateInsert() throws SQLException{

		pixel.setWriterId(AdminManager.getWriterId());

		boolean res = MarketingManager.insertUpdatePixel(pixel);
		if (res == false) {
			return null;
		}
		updateList();
		return Constants.NAV_MARKETING_PIXELS;
	}

	public int getListSize() {
		return list.size();
	}

	/**
	 * Navigate to edit pixel
	 * @return
	 * @throws SQLException
	 */
	public String navEdit() {
		return Constants.NAV_MARKETING_PIXEL;
	}

	/**
	 * Navigate to insert new pixel
	 * @return
	 * @throws SQLException
	 */
	public String navNew() throws SQLException {
		pixel = new MarketingPixel();
		pixel.setWriterId(AdminManager.getWriterId());
		pixel.setTimeCreated(new Date());

		return Constants.NAV_MARKETING_PIXEL;
	}

	/**
	 * Update userId parameter in pixel code
	 * @return
	 */
	public String updateUserIdParam() {
		if ( !CommonUtil.isParameterEmptyOrNull(word) ) {
				replceInPixels(ConstantsBase.MARKETING_PARAMETER_USERID);
			}
		return null;
	}

	/**
	 * Update mobile parameter in pixel code
	 * @return
	 */
	public String updateMobileParam() {
		if ( !CommonUtil.isParameterEmptyOrNull(word) ) {
				replceInPixels(ConstantsBase.MARKETING_PARAMETER_MOBIE);
			}
		return null;
	}

	/**
	 * Update currencyCode parameter in pixel code
	 * @return
	 */
	public String updateCurrencyParam() {
		if ( !CommonUtil.isParameterEmptyOrNull(word) ) {
			replceInPixels(ConstantsBase.MARKETING_PARAMETER_CURRENCY_CODE);
		}
		return null;
	}

	/**
	 * Update tranAmount parameter in pixel code
	 * @return
	 */
	public String updateTranAmountParam() {
		if ( !CommonUtil.isParameterEmptyOrNull(word) ) {
			replceInPixels(ConstantsBase.MARKETING_PARAMETER_TRANS_AMOUNT);
		}
		return null;
	}

	/**
	 * Update tranAmountUsd parameter in pixel code
	 * @return
	 */
	public String updateTranAmountUsdParam() {
		if ( !CommonUtil.isParameterEmptyOrNull(word) ) {
			replceInPixels(ConstantsBase.MARKETING_PARAMETER_TRANS_AMOUNT_USD);
		}
		return null;
	}

	public String updateTranAmountEurParam() {
		if ( !CommonUtil.isParameterEmptyOrNull(word) ) {
			replceInPixels(ConstantsBase.MARKETING_PARAMETER_TRANS_AMOUNT_EUR);
		}
		return null;
	}

	/**
	 * Update tranAId parameter in pixel code
	 * @return
	 */
	public String updateTranIdParam() {
		if ( !CommonUtil.isParameterEmptyOrNull(word) ) {
			replceInPixels(ConstantsBase.MARKETING_PARAMETER_TRANS_ID);
		}
		return null;
	}


	/**
	 * Update dynamic parameter in pixel code
	 * @return
	 */
	public String updateDynamicParam() {
		if ( !CommonUtil.isParameterEmptyOrNull(word) ) {
			replceInPixels(ConstantsBase.MARKETING_PARAMETER_DYNAMIC);
		}
		return null;
	}


	/**
	 * Update subAffId parameter in pixel code
	 * @return
	 */
	public String updateSubAffIdParam() {
		if ( !CommonUtil.isParameterEmptyOrNull(word) ) {
			replceInPixels(ConstantsBase.MARKETING_PARAMETER_SUB_AFF_CODE);
		}
		return null;
	}

	/**
	 * Update contactId parameter in pixel code
	 * @return
	 */
	public String updateContactIdParam() {
		if ( !CommonUtil.isParameterEmptyOrNull(word) ) {
			replceInPixels(ConstantsBase.MARKETING_PARAMETER_CONTACTID);
		}
		return null;
	}

	/**
	 * Update DUID parameter in pixel code
	 * @return
	 */
	public String updateDUIDParam() {
		if (!CommonUtil.isParameterEmptyOrNull(word)) {
			replceInPixels(ConstantsBase.MARKETING_PARAMETER_DUID);
		}
		return null;
	}

	/**
	 * Update DUID parameter in pixel code
	 * @return
	 */
	public String updateIPParam() {
		if (!CommonUtil.isParameterEmptyOrNull(word)) {
			replceInPixels(ConstantsBase.MARKETING_PARAMETER_IP);
		}
		return null;
	}
	
	/**
	 * Update AffSub1 parameter in pixel code
	 * @return
	 */
	public String updateAffSub1Param() {
		if (!CommonUtil.isParameterEmptyOrNull(word)) {
			replceInPixels(ConstantsBase.MARKETING_PARAMETER_AFF_SUB1);
		}
		return null;
	}
	
	/**
	 * Update AffSub2 parameter in pixel code
	 * @return
	 */
	public String updateAffSub2Param() {
		if (!CommonUtil.isParameterEmptyOrNull(word)) {
			replceInPixels(ConstantsBase.MARKETING_PARAMETER_AFF_SUB2);
		}
		return null;
	}
	
	/**
	 * Update AffSub3 parameter in pixel code
	 * @return
	 */
	public String updateAffSub3Param() {
		if (!CommonUtil.isParameterEmptyOrNull(word)) {
			replceInPixels(ConstantsBase.MARKETING_PARAMETER_AFF_SUB3);
		}
		return null;
	}
	
	/**
	 * Update email parameter in pixel code
	 * @return
	 */
	public String updateEmail() {
		if (!CommonUtil.isParameterEmptyOrNull(word)) {
			replceInPixels(ConstantsBase.MARKETING_PARAMETER_EMAIL);
		}
		return null;
	}	

	/**
	 * Replace word in pixel
	 * @param httpCode
	 * @param httpsCode
	 * @param param
	 */
	public void replceInPixels(String param) {

		String httpCode = pixel.getHttpCode();
		String httpsCode = pixel.getHttpsCode();

		if ( !CommonUtil.isParameterEmptyOrNull(httpCode) ) {
			pixel.setHttpCode(httpCode.replace(word, param));
		}

		if ( !CommonUtil.isParameterEmptyOrNull(httpsCode) ) {
			pixel.setHttpsCode(httpsCode.replace(word, param));
		}

	}



	/**
	 * @return the list
	 */
	public ArrayList getList() {
		return list;
	}

	/**
	 * @param list the list to set
	 */
	public void setList(ArrayList list) {
		this.list = list;
	}

	/**
	 * @return the pixel
	 */
	public MarketingPixel getPixel() {
		return pixel;
	}

	/**
	 * @param pixel the pixel to set
	 */
	public void setPixel(MarketingPixel pixel) {
		this.pixel = pixel;
	}

	/**
	 * @return the pixelName
	 */
	public String getPixelName() {
		return pixelName;
	}

	/**
	 * @param pixelName the pixelName to set
	 */
	public void setPixelName(String pixelName) {
		this.pixelName = pixelName;
	}

	/**
	 * @return the word
	 */
	public String getWord() {
		return word;
	}

	/**
	 * @param word the word to set
	 */
	public void setWord(String word) {
		this.word = word;
	}


}
