package il.co.etrader.backend.mbeans;

import java.io.IOException;
import java.io.Serializable;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.Country;

import il.co.etrader.backend.bl_managers.ApplicationData;
import il.co.etrader.backend.bl_managers.PopulationEntriesManager;
import il.co.etrader.backend.bl_managers.PopulationsManager;
import il.co.etrader.backend.bl_managers.WritersManager;
import il.co.etrader.backend.bl_vos.PopulationEntry;
import il.co.etrader.backend.bl_vos.User;
import il.co.etrader.backend.bl_vos.WriterWrapper;
import il.co.etrader.backend.helper.TrackingReportSender;
import il.co.etrader.backend.util.Constants;
import il.co.etrader.backend.util.Utils;
import il.co.etrader.bl_vos.PopulationType;
import il.co.etrader.bl_vos.Writer;
import il.co.etrader.population.PopulationHandlersException;
import il.co.etrader.util.CommonUtil;
import il.co.etrader.util.ConstantsBase;

public class TrackingForm implements Serializable {
	private static final long serialVersionUID = -3903035568763484725L;

	private static final Logger logger = Logger.getLogger(TrackingForm.class);

	private ArrayList list;
	private Date from = null;
	private Date to = null;
	private Date fromLogin = null;
	private Date toLogin = null;
	private long skinId;
	private PopulationEntry populationEntry;
	private long writerId;
	private boolean assignFilter;
	private long writerFilter;
	private String username;
	private Long userId;
	private String priorityId;
	private Long contactId;
	private long lastLoginInXMinutes;
	private long campaignId;
	private List<String> timeZone;
	private ArrayList<SelectItem> countriesList = new ArrayList<SelectItem>();
	private long userStatusId;
	private long userRankId;
	private String countriesGroup;
	private String[] selectedCountriesList;
	private String excludeCountries;
	private String includeCountries;
	private String populationTypesNames;
	private ArrayList<String> populationTypesList;
	private String populationTypes;
	private HashMap<Integer, ArrayList<String>> typeIdToValue;
	private boolean isRetention;
	private boolean isConvresion;
	private boolean balanceBelowMinInvestAmount;
	private boolean madeDepositButdidntMakeInv24HAfter;
	private int retentionStatus;
	private long populationDept;
	private int actionTypeId;
	private long retentionTeamId;
	private String affiliateKey;

	public void initTypeIdOfValues() {
		try {
			typeIdToValue = PopulationsManager.getTypesIdAndValueArray();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public String getGoConvertion() throws SQLException {
		this.populationDept = ConstantsBase.SALES_DEPOSIT_TYPE_CONVERSION;
		this.retentionTeamId = ConstantsBase.ALL_FILTER_ID;
		if (isRetention) {
			updateList();
		}
		Writer w = Utils.getWriter().getWriter();
		w.setScreenType(ConstantsBase.SALES_DEPOSIT_TYPE_CONVERSION);
		this.isConvresion = true;
		this.isRetention = false;
		return "";
	}

	public String getGoRetention() throws SQLException {
		this.populationDept = ConstantsBase.SALES_DEPOSIT_TYPE_RETENTION;
		this.retentionTeamId = ConstantsBase.ALL_FILTER_ID;
		if (isConvresion) {
			User u = Utils.getUser();
			updateList();
		}	
		Writer w = Utils.getWriter().getWriter();
		w.setScreenType(ConstantsBase.SALES_DEPOSIT_TYPE_RETENTION);
		this.isConvresion = false;
		this.isRetention = true;
		return "";
	}

	/**
	 * @return the contactId
	 */
	public Long getContactId() {
		return contactId;
	}

	/**
	 * @param contactId the contactId to set
	 */
	public void setContactId(Long contactId) {
		this.contactId = contactId;
	}

	/**
	 * @return the priorityId
	 */
	public String getPriorityId() {
		return priorityId;
	}

	/**
	 * @param priorityId the priorityId to set
	 */
	public void setPriorityId(String priorityId) {
		this.priorityId = priorityId;
	}

	/**
	 * @return the userId
	 */
	public Long getUserId() {
		return userId;
	}

	/**
	 * @param userId the userId to set
	 */
	public void setUserId(Long userId) {
		this.userId = userId;
	}

	/**
	 * @return the username
	 */
	public String getUsername() {
		return username;
	}

	/**
	 * @param username the username to set
	 */
	public void setUsername(String username) {
		this.username = username;
	}

	/**
	 * @return the assignFilter
	 */
	public boolean isAssignFilter() {
		return assignFilter;
	}

	/**
	 * @param assignFilter the assignFilter to set
	 */
	public void setAssignFilter(boolean assignFilter) {
		this.assignFilter = assignFilter;
	}

	/**
	 * @return the writerFilter
	 */
	public long getWriterFilter() {
		return writerFilter;
	}

	/**
	 * @param writerFilter the writerFilter to set
	 */
	public void setWriterFilter(long writerFilter) {
		this.writerFilter = writerFilter;
	}

	public TrackingForm() throws SQLException{
		skinId = 0 ;
		username = null ;
		userId = null ;
		contactId = null;
		priorityId = null ;
		writerId = 0 ;
		assignFilter = true;
		writerFilter = 0 ;
		campaignId = 0;
		timeZone = new ArrayList<String>();
		timeZone.add(ConstantsBase.ALL_TIME_ZONE_POPULATION);
		userStatusId = 0;
		userRankId = 0;
		countriesGroup = "0";
		selectedCountriesList = null;
		excludeCountries = "0";
		includeCountries = "0";
		populationTypesNames = "";
		populationTypesList = new ArrayList<String>();
		populationTypes = "";
		isRetention = false;
		isConvresion = false;
		balanceBelowMinInvestAmount = false;
		madeDepositButdidntMakeInv24HAfter = false;
		retentionStatus = 0;
		actionTypeId = 0;
		retentionTeamId = ConstantsBase.ALL_FILTER_ID;
		affiliateKey = null;

		initTypeIdOfValues();
		updateCountries(timeZone);
		GregorianCalendar gc = new GregorianCalendar();
		to = gc.getTime();
		gc.add(GregorianCalendar.YEAR,-1);
		gc.set(GregorianCalendar.HOUR_OF_DAY,0);
		gc.set(GregorianCalendar.MINUTE,0);
		gc.set(GregorianCalendar.SECOND,0);
		from = gc.getTime();
		updateList();
	}

	public String lockEntry() throws SQLException,PopulationHandlersException {
		boolean res = PopulationEntriesManager.lockEntry(populationEntry, false);
		if (res){
			CommonUtil.setTablesToFirstPage();
			return PopulationEntriesManager.loadEntry(populationEntry);
		}
		return null;
	}

	public String unLockEntry() throws SQLException,PopulationHandlersException {
		PopulationEntriesManager.unLockEntry(populationEntry);
		updateList();
		return null;
	}

	/**
	 * Load entry to session and navigate to entrySrip page
	 * @return
	 * @throws SQLException
	 */
	public String loadEntry() throws SQLException {
		return PopulationEntriesManager.loadEntry(populationEntry);
	}


	public int getListSize() {
		return list.size();
	}

	public String updateList() throws SQLException{

		FacesContext context=FacesContext.getCurrentInstance();
		WriterWrapper wr = (WriterWrapper) context.getApplication().createValueBinding(Constants.BIND_WRITER_WRAPPER).getValue(context);
		User user= (User)context.getApplication().createValueBinding(Constants.BIND_USER).getValue(context);

		long userIdVal = 0;
		if (null != userId){
			userIdVal = userId.longValue();
		}
		if (username != null && username.equals(ConstantsBase.EMPTY_STRING)){
			username = null;
		}

		long contactIdVal = 0;
		if (null != contactId) {
			contactIdVal = contactId.longValue();
		}

		if (user.isRetentionSelected()) {
			writerId = Utils.getWriter().getWriter().getId();
		} else {
			writerId= writerFilter;
		}

		list = PopulationEntriesManager.getPopulationEntriesByEntryType(populationTypes,skinId, writerId,
				ConstantsBase.POP_ENTRY_TYPE_TRACKING,from,to,fromLogin,toLogin,
				actionTypeId,assignFilter, userIdVal, username, contactIdVal, priorityId, wr, lastLoginInXMinutes, campaignId,
				getArrayToString(timeZone), userStatusId, countriesGroup, balanceBelowMinInvestAmount,
				madeDepositButdidntMakeInv24HAfter, retentionStatus, populationDept, userRankId, retentionTeamId, affiliateKey, null);



		CommonUtil.setTablesToFirstPage();
		return null;
	}

	public String exportTrackingResultToMail() throws IOException, SQLException, ParseException {
		String reportName = "tracking_report_";
		FacesContext context = FacesContext.getCurrentInstance();
		WriterWrapper wr = (WriterWrapper) context.getApplication().createValueBinding(Constants.BIND_WRITER_WRAPPER).getValue(context);
		User user= (User)context.getApplication().createValueBinding(Constants.BIND_USER).getValue(context);

		logger.info("Process " + reportName);

		SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd");
		String filename = reportName + fmt.format(from) + "_" + fmt.format(to) + ".csv";

		long currentWriterId = Utils.getWriter().getWriter().getId();

		// set defualt date for arabic partner
		if (currentWriterId == Constants.PARTNER_ARABIC_ID) {
			String enumDate = CommonUtil.getEnum(
					Constants.ENUM_PARTNER_ARABIC_DATE_ENUMERATOR,
					Constants.ENUM_PARTNER_ARABIC_DATE_CODE);
			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
			Date startDateAr = sdf.parse(enumDate);
			;
			if (from.before(startDateAr)) {
				from = startDateAr;
			}
		}

		// check email address
		if (CommonUtil.isParameterEmptyOrNull(Utils.getWriter().getWriter()
				.getEmail())) {
			FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR,
					CommonUtil.getMessage("error.enteries.report.invalid.mail", null),null);
			context.addMessage(null, fm);
			return null;
		}

		TrackingReportSender sender = new TrackingReportSender(this, filename, wr, user.isRetentionSelected());
		sender.start();

		FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_INFO,
				filename + " " + CommonUtil.getMessage("enteries.report.processing.mail", null),null);
		context.addMessage(null, fm);

		return null;
	}

	public ArrayList getList() {
		return list;
	}

	public void setList(ArrayList list) {
		this.list = list;
	}

	/**
	 * @return the from
	 */
	public Date getFrom() {
		return from;
	}

	/**
	 * @param from the from to set
	 */
	public void setFrom(Date from) {
		this.from = from;
	}

	/**
	 * @return the to
	 */
	public Date getTo() {
		return to;
	}

	/**
	 * @param to the to to set
	 */
	public void setTo(Date to) {
		this.to = to;
	}

	/**
	 * @return the skinId
	 */
	public long getSkinId() {
		return skinId;
	}

	/**
	 * @param skinId the skinId to set
	 */
	public void setSkinId(long skinId) {
		this.skinId = skinId;
	}

	/**
	 * @return the populationEntry
	 */
	public PopulationEntry getPopulationEntry() {
		return populationEntry;
	}

	/**
	 * @param populationEntry the populationEntry to set
	 */
	public void setPopulationEntry(PopulationEntry populationEntry) {
		this.populationEntry = populationEntry;
	}

	/**
	 * @return the campaignId
	 */
	public long getCampaignId() {
		return campaignId;
	}

	/**
	 * @param campaignId the campaignId to set
	 */
	public void setCampaignId(long campaignId) {
		this.campaignId = campaignId;
	}

	/**
	 * @return the timeZone
	 */
	public List<String> getTimeZone() {
		return timeZone;
	}

	/**
	 * @param timeZone the timeZone to set
	 */
	public void setTimeZone(List<String> timeZone) {
		this.timeZone = timeZone;
	}

	public boolean isTimeZonesEmpty() {
		if (null == timeZone || timeZone.equals("0")
				|| timeZone.size() == 0) {
			return true;
		}
		return false;
	}

	/**
     * return 0 if all selected else return string of GMT
     * @param temp
     * @return
     */
    public String getArrayToString(List<String> temp) {
        String list = ConstantsBase.EMPTY_STRING;
        for (int i = 0; i < temp.size(); i++) {
        	if(temp.get(i).equals(ConstantsBase.ALL_TIME_ZONE_POPULATION)){
        		return ConstantsBase.ALL_TIME_ZONE_POPULATION;
        	}
        	list += "'" + temp.get(i) + "'" + ",";
        }
        return list.substring(0,list.length()-1);
    }

	public void updateCountries(ValueChangeEvent event) {
		updateCountries((List<String>) event.getNewValue());
	}

	public void updateCountries(List<String> GMT) {
		ArrayList<SelectItem> list = new ArrayList<SelectItem>();
		countriesList.clear();
		FacesContext context = FacesContext.getCurrentInstance();
		WriterWrapper w = (WriterWrapper) context.getApplication()
				.createValueBinding(ConstantsBase.BIND_WRITER_WRAPPER)
				.getValue(context);
		if (!GMT.contains("0")) {
			for (Country c : ApplicationData.getCountries().values()) {
				if (GMT.contains(c.getGmtOffset())) {
					list.add(new SelectItem(Long.valueOf(c.getId()).toString(),
							CommonUtil.getMessage(c.getDisplayName(), null)
									+ ", "
									+ Utils.getUtcDifference(w.getUtcOffset(),
											c.getGmtOffset())));
				}
			}
			Collections.sort(list, new CommonUtil.selectItemComparator());
		} else {
			for (Country c : ApplicationData.getCountries().values()) {
				list.add(new SelectItem(Long.valueOf(c.getId()).toString(),
						CommonUtil.getMessage(c.getDisplayName(), null)
								+ ", "
								+ Utils.getUtcDifference(w.getUtcOffset(), c
										.getGmtOffset())));
			}
			Collections.sort(list, new CommonUtil.selectItemComparator());
		}
		countriesList.addAll(list);
	}

	/**
	 * @return the countriesGroup
	 */
	public String getCountriesGroup() {
		return countriesGroup;
	}

	/**
	 * @param countriesGroup the countriesGroup to set
	 */
	public void setCountriesGroup(String countriesGroup) {
		this.countriesGroup = countriesGroup;
	}
	/**
	 * Return true countries list is empty
	 *
	 * @return
	 */
	public boolean isCountriesEmpty() {
		if (null == selectedCountriesList || selectedCountriesList.equals("0")
				|| selectedCountriesList.length == 0) {
			return true;
		}
		return false;
	}

	public void updateCountriesToString() {
		String tmp = ConstantsBase.EMPTY_STRING;
		excludeCountries = "0";
		if (includeCountries.equals("")) { // if selection all exclude
											// countries
			includeCountries = ConstantsBase.COUNTRIES_NONE;
		}

		if (null != selectedCountriesList && selectedCountriesList.length == 0) {// if
																					// selection
																					// all
																					// include
																					// countries
			includeCountries = "0";
		}

		if (!includeCountries.equals(ConstantsBase.COUNTRIES_NONE)) {
			if (null != selectedCountriesList
					&& selectedCountriesList.length > 0
					&& selectedCountriesList.length < 120) {
				includeCountries = "0";
				for (int i = 0; i < selectedCountriesList.length; i++) {
					tmp += selectedCountriesList[i] + ",";
				}
				excludeCountries = tmp.substring(0, tmp.length() - 1);
			}
		}
	}

	/**
	 * @return the countriesList
	 */
	public ArrayList<SelectItem> getCountriesList() {
		return countriesList;
	}

	/**
	 * @param countriesList the countriesList to set
	 */
	public void setCountriesList(ArrayList<SelectItem> countriesList) {
		this.countriesList = countriesList;
	}

	/**
	 * @return the excludeCountries
	 */
	public String getExcludeCountries() {
		return excludeCountries;
	}

	/**
	 * @param excludeCountries the excludeCountries to set
	 */
	public void setExcludeCountries(String excludeCountries) {
		this.excludeCountries = excludeCountries;
	}

	/**
	 * @return the includeCountries
	 */
	public String getIncludeCountries() {
		return includeCountries;
	}

	/**
	 * @param includeCountries the includeCountries to set
	 */
	public void setIncludeCountries(String includeCountries) {
		this.includeCountries = includeCountries;
	}

	/**
	 * @return the selectedCountriesList
	 */
	public String[] getSelectedCountriesList() {
		return selectedCountriesList;
	}

	/**
	 * @param selectedCountriesList the selectedCountriesList to set
	 */
	public void setSelectedCountriesList(String[] selectedCountriesList) {
		this.selectedCountriesList = selectedCountriesList;
	}

	/**
	 * Return true population list is empty
	 *
	 * @return
	 */
	public boolean isPopulationTypesEmpty() {
		if (populationTypesNames.equals(ConstantsBase.EMPTY_STRING)) {
			return true;
		}
		return false;
	}

	/**
	 * @param populationTypesList
	 *            the populationTypesList to set
	 */
	public void setPopulationTypesList(ArrayList<String> populationTypesList) {
		this.populationTypesList = populationTypesList;

		int listSize = populationTypesList.size();
		populationTypes = "";
		populationTypesNames = "";

		if (listSize > 0) {
			String popType = null;
			String popTypeKey = null;
			String popTypeName = null;
			HashMap<Long, PopulationType> populationsTypes = ApplicationData
					.getPopulationsTypes();

			for (int i = 0; i < listSize; i++) {
				popType = populationTypesList.get(i);
				populationTypes += popType;
				popTypeKey = populationsTypes.get(Long.parseLong(popType))
						.getName();
				popTypeName = CommonUtil.getMessage(popTypeKey, null);
				populationTypesNames += popTypeName;

				// add comma if not last value
				if (listSize - 1 != i) {
					populationTypes += ",";
					populationTypesNames += ", ";
				}
			}
		}
	}

	/**
	 * @return the populationTypesNames
	 */
	public String getPopulationTypesNames() {
		return populationTypesNames;
	}

	/**
	 * @param populationTypesNames the populationTypesNames to set
	 */
	public void setPopulationTypesNames(String populationTypesNames) {
		this.populationTypesNames = populationTypesNames;
	}

	/**
	 * @return the populationTypesList
	 */
	public ArrayList<String> getPopulationTypesList() {
		return populationTypesList;
	}

	public ArrayList<String> getArrayOfRetention(){
		return typeIdToValue.get(Constants.POPULATION_TYPE_ID_RETENTION);
	}

	public ArrayList<String> getArrayOfConversion(){
		return typeIdToValue.get(Constants.POPULATION_TYPE_ID_CONVERSION);
	}
	/**
	 * @return the isConvresion
	 */
	public boolean isConvresion() {
		return isConvresion;
	}
	/**
	 * @param isConvresion the isConvresion to set
	 */
	public void setConvresion(boolean isConvresion) {
		this.isConvresion = isConvresion;
	}
	/**
	 * @return the isRetention
	 */
	public boolean isRetention() {
		return isRetention;
	}
	/**
	 * @param isRetention the isRetention to set
	 */
	public void setRetention(boolean isRetention) {
		this.isRetention = isRetention;
	}
	/**
	 * @return the populationTypes
	 */
	public String getPopulationTypes() {
		return populationTypes;
	}
	/**
	 * @param populationTypes the populationTypes to set
	 */
	public void setPopulationTypes(String populationTypes) {
		this.populationTypes = populationTypes;
	}
	/**
	 * @return the typeIdToValue
	 */
	public HashMap<Integer, ArrayList<String>> getTypeIdToValue() {
		return typeIdToValue;
	}
	/**
	 * @param typeIdToValue the typeIdToValue to set
	 */
	public void setTypeIdToValue(HashMap<Integer, ArrayList<String>> typeIdToValue) {
		this.typeIdToValue = typeIdToValue;
	}
	/**
	 * @return the balanceBelowMinInvestAmount
	 */
	public boolean isBalanceBelowMinInvestAmount() {
		return balanceBelowMinInvestAmount;
	}
	/**
	 * @param balanceBelowMinInvestAmount the balanceBelowMinInvestAmount to set
	 */
	public void setBalanceBelowMinInvestAmount(boolean balanceBelowMinInvestAmount) {
		this.balanceBelowMinInvestAmount = balanceBelowMinInvestAmount;
	}
	/**
	 * @return the madeDepositButdidntMakeInv24HAfter
	 */
	public boolean isMadeDepositButdidntMakeInv24HAfter() {
		return madeDepositButdidntMakeInv24HAfter;
	}
	/**
	 * @param madeDepositButdidntMakeInv24HAfter the madeDepositButdidntMakeInv24HAfter to set
	 */
	public void setMadeDepositButdidntMakeInv24HAfter(
			boolean madeDepositButdidntMakeInv24HAfter) {
		this.madeDepositButdidntMakeInv24HAfter = madeDepositButdidntMakeInv24HAfter;
	}
	/**
	 * @return the retentionStatus
	 */
	public int getRetentionStatus() {
		return retentionStatus;
	}

	/**
	 * @param retentionStatus
	 *            the retentionStatus to set
	 */
	public void setRetentionStatus(int retentionStatus) {
		this.retentionStatus = retentionStatus;
	}

	/**
	 * @return the userStatusId
	 */
	public long getUserStatusId() {
		return userStatusId;
	}

	/**
	 * @param userStatusId the userStatusId to set
	 */
	public void setUserStatusId(long userStatusId) {
		this.userStatusId = userStatusId;
	}

	/**
	 * @return the userRankId
	 */
	public long getUserRankId() {
		return userRankId;
	}

	/**
	 * @param userRankId the userRankId to set
	 */
	public void setUserRankId(long userRankId) {
		this.userRankId = userRankId;
	}

	public int getActionTypeId() {
		return actionTypeId;
	}

	public void setActionTypeId(int actionTypeId) {
		this.actionTypeId = actionTypeId;
	}

	public Date getFromLogin() {
		return fromLogin;
	}

	public void setFromLogin(Date fromLogin) {
		this.fromLogin = fromLogin;
	}

	public Date getToLogin() {
		return toLogin;
	}

	public void setToLogin(Date toLogin) {
		this.toLogin = toLogin;
	}

	/**
	 * @return the retentionTeamId
	 */
	public long getRetentionTeamId() {
		return retentionTeamId;
	}

	/**
	 * @param retentionTeamId the retentionTeamId to set
	 */
	public void setRetentionTeamId(long retentionTeamId) {
		this.retentionTeamId = retentionTeamId;
	}

	public String getAffiliateKey() {
		return affiliateKey;
	}

	public void setAffiliateKey(String affiliateKey) {
		this.affiliateKey = affiliateKey;
	}

	/**
	 * @return the lastLoginInXMinutes
	 */
	public long getLastLoginInXMinutes() {
		return lastLoginInXMinutes;
	}

	/**
	 * @param lastLoginInXMinutes the lastLoginInXMinutes to set
	 */
	public void setLastLoginInXMinutes(long lastLoginInXMinutes) {
		this.lastLoginInXMinutes = lastLoginInXMinutes;
	}
}
