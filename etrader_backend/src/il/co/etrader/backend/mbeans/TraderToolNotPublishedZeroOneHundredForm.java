package il.co.etrader.backend.mbeans;

import java.io.Serializable;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.GregorianCalendar;

import javax.faces.event.ValueChangeEvent;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.Opportunity;
import com.anyoption.common.enums.SkinGroup;

import il.co.etrader.backend.bl_managers.InvestmentsManager;
import il.co.etrader.backend.bl_vos.OpportunityTotal;
import il.co.etrader.backend.util.Constants;
import il.co.etrader.util.CommonUtil;



public class TraderToolNotPublishedZeroOneHundredForm implements Serializable {

	private static final long serialVersionUID = -4365573988415113079L;

	private static final Logger log = Logger.getLogger(TraderToolNotPublishedZeroOneHundredForm.class);

	private ArrayList<OpportunityTotal> list;

	private Date from;
	private String settled;
	private long groupId;
	private long market;
	private double eventLevel;

	//sorting
	private String sortColumn;
	private String prevSortColumn;
    private boolean sortAscending = false;

	public TraderToolNotPublishedZeroOneHundredForm() throws SQLException, ParseException{

		GregorianCalendar gc = new GregorianCalendar();
		from = gc.getTime();
		settled = Constants.NOT_SETTLED;
		market= 0;
		groupId = 0;

		sortColumn = "";
		prevSortColumn = "";

		updateList();
	}

	/**
	 * Get opportunities list size
	 * @return
	 * 		size of all the model and not page size
	 */
	public int getListSize() {
		int size = 0;
		if ( null != list ) {
			size = list.size();
		}

		return size;
	}

	/**
	 * Get list of opportunities
	 * @return
	 * @throws SQLException
	 * @throws ParseException
	 */
	public String updateList() throws SQLException, ParseException {
		list = InvestmentsManager.getTtOpportunities(from, settled, market, -1, groupId, true);
		CommonUtil.setTablesToFirstPage();
		return null;
	}

    public void updateMarketChange(ValueChangeEvent event) throws SQLException, ParseException{
    	market = (Long)event.getNewValue();
    	updateList();
    }

	/**
	 * Change settled radio button
	 * @param event
	 * @throws SQLException
	 * @throws ParseException
	 */
    public void updateChange(ValueChangeEvent event) throws SQLException, ParseException{
    	settled =(String)event.getNewValue();
    	updateList();
    }

	public Date getFrom() {
		return from;
	}

	public void setFrom(Date from) {
		this.from = from;
	}

	/**
	 * @return the settled
	 */
	public String getSettled() {
		return settled;
	}

	/**
	 * @param settled the settled to set
	 */
	public void setSettled(String settled) {
		this.settled = settled;
	}

	/**
	 * @return the market
	 */
	public long getMarket() {
		return market;
	}

	/**
	 * @param market the market to set
	 */
	public void setMarket(long market) {
		this.market = market;
	}

	public double getEventLevel() {
		return eventLevel;
	}

	public void setEventLevel(double eventLevel) {
		this.eventLevel = eventLevel;
	}

	/**
	 * @return the list
	 */
	public ArrayList<OpportunityTotal> getList() {
		return list;
	}

	/**
	 * @param list the list to set
	 */
	public void setList(ArrayList<OpportunityTotal> list) {
		this.list = list;
	}

	/**
	 * @return the prevSortColumn
	 */
	public String getPrevSortColumn() {
		return prevSortColumn;
	}

	/**
	 * @param prevSortColumn the prevSortColumn to set
	 */
	public void setPrevSortColumn(String prevSortColumn) {
		this.prevSortColumn = prevSortColumn;
	}

	/**
	 * @return the sortAscending
	 */
	public boolean isSortAscending() {
		return sortAscending;
	}

	/**
	 * @param sortAscending the sortAscending to set
	 */
	public void setSortAscending(boolean sortAscending) {
		this.sortAscending = sortAscending;
	}

	/**
	 * @return the sortColumn
	 */
	public String getSortColumn() {
		return sortColumn;
	}

	/**
	 * @param sortColumn the sortColumn to set
	 */
	public void setSortColumn(String sortColumn) {
		this.sortColumn = sortColumn;
	}

	/**
	 * Sort list
	 * @return
	 */
	public String sortList() {
		if (sortColumn.equals(prevSortColumn)) {
			sortAscending = !sortAscending;
		} else {
			sortAscending = true;
		}

		prevSortColumn = sortColumn;

		if (sortColumn.equals("id")) {
			Collections.sort(list,new IdComparator(sortAscending));
		}

		if (sortColumn.equals("marketName")) {
			Collections.sort(list,new MarketNameComparator(sortAscending));
		}

		if (sortColumn.equals("eventLevel")) {
			Collections.sort(list,new eventLevelComparator(sortAscending));
		}

		if (sortColumn.equals("estClosingTime")) {
			Collections.sort(list,new TimeEstClosingComparator(sortAscending));
		}

		if (sortColumn.equals("boughtAmount")) {
			Collections.sort(list, new BoughtAmountComparator(sortAscending));
		}

		if (sortColumn.equals("soldAmount")) {
			Collections.sort(list, new SoldAmountComparator(sortAscending));
		}

		if (sortColumn.equals("delta")) {
			Collections.sort(list, new DeltaComparator(sortAscending));
		}

		if (sortColumn.equals("volume")) {
			Collections.sort(list, new VolumeComparator(sortAscending));
		}

		if (sortColumn.equals("boughtCount")) {
			Collections.sort(list, new BoughtCountComparator(sortAscending));
		}
		
		if (sortColumn.equals("soldCount")) {
			Collections.sort(list, new SoldCountComparator(sortAscending));
		}
		
		if (sortColumn.equals("shiftparam")) {
			Collections.sort(list, new ShiftParamComparator(sortAscending, SkinGroup.ETRADER));
		}

		if (sortColumn.equals("autoShiftParameter")) {
			Collections.sort(list, new VolumeComparator(sortAscending));
		}

		if (sortColumn.equals("oppExposure")) {
			Collections.sort(list, new ExposureParamComparator(sortAscending, SkinGroup.ETRADER));
		}

		CommonUtil.setTablesToFirstPage();
		return null;
	}

	private class IdComparator implements Comparator {
		private boolean ascending;
		public IdComparator(boolean asc) {
			ascending = asc;
		}
   	    public int compare(Object o1, Object o2) {
   		 		Opportunity op1=(Opportunity)o1;
   		 		Opportunity op2=(Opportunity)o2;
   		 		if (ascending){
   		 			return new Long(op1.getId()).compareTo(new Long(op2.getId()));
   		 		}
   		 		return -new Long(op1.getId()).compareTo(new Long(op2.getId()));
		}
  	 }

	private class MarketNameComparator implements Comparator {
		private boolean ascending;
		public MarketNameComparator(boolean asc) {
			ascending = asc;
		}
   	    public int compare(Object o1, Object o2) {
   		 		Opportunity op1=(Opportunity)o1;
   		 		Opportunity op2=(Opportunity)o2;
   		 		if (ascending){
   		 		    return CommonUtil.getMarketName(op1.getMarketId()).compareTo(CommonUtil.getMarketName(op2.getMarketId()));
   		 		}
   		 		return -CommonUtil.getMarketName(op1.getMarketId()).compareTo(CommonUtil.getMarketName(op2.getMarketId())); 
		}
  	 }

	private class eventLevelComparator implements Comparator {
		private boolean ascending;
		public eventLevelComparator(boolean asc) {
			ascending = asc;
		}
   	    public int compare(Object o1, Object o2) {
   		 		Opportunity op1=(Opportunity)o1;
   		 		Opportunity op2=(Opportunity)o2;
   		 		if (ascending){
   		 			return new Double(op1.getCurrentLevel()).compareTo(new Double(op2.getCurrentLevel()));
   		 		}
   		 	return -new Double(op1.getCurrentLevel()).compareTo(new Double(op2.getCurrentLevel()));
		}
  	 }

	private class TimeEstClosingComparator implements Comparator {
		private boolean ascending;
		public TimeEstClosingComparator(boolean asc) {
			ascending = asc;
		}
   	    public int compare(Object o1, Object o2) {
   		 		Opportunity op1=(Opportunity)o1;
   		 		Opportunity op2=(Opportunity)o2;
   		 		if (ascending){
   		 			return op1.getTimeEstClosing().compareTo(op2.getTimeEstClosing());
   		 		}
   		 		return -op1.getTimeEstClosing().compareTo(op2.getTimeEstClosing());
		}
  	 }

	private class BoughtAmountComparator implements Comparator {
		private boolean ascending;
		public BoughtAmountComparator(boolean asc) {
			ascending = asc;
		}
   	    public int compare(Object o1, Object o2) {
   		 		OpportunityTotal op1=(OpportunityTotal)o1;
   		 		OpportunityTotal op2=(OpportunityTotal)o2;
   		 		if (ascending){
   		 			return new Double(op1.getBoughtAmount()).compareTo(new Double(op2.getBoughtAmount()));
   		 		}
   		 		return -new Double(op1.getBoughtAmount()).compareTo(new Double(op2.getBoughtAmount()));
		}
  	 }

	private class SoldAmountComparator implements Comparator {
		private boolean ascending;
		public SoldAmountComparator(boolean asc) {
			ascending = asc;
		}
   	    public int compare(Object o1, Object o2) {
   		 		OpportunityTotal op1=(OpportunityTotal)o1;
   		 		OpportunityTotal op2=(OpportunityTotal)o2;
   		 		if (ascending){
   		 			return new Double(op1.getSoldAmount()).compareTo(new Double(op2.getSoldAmount()));
   		 		}
   		 		return -new Double(op1.getSoldAmount()).compareTo(new Double(op2.getSoldAmount()));
		}
  	 }

	private class DeltaComparator implements Comparator {
		private boolean ascending;
		public DeltaComparator(boolean asc) {
			ascending = asc;
		}
   	    public int compare(Object o1, Object o2) {
   		 		OpportunityTotal op1=(OpportunityTotal)o1;
   		 		OpportunityTotal op2=(OpportunityTotal)o2;
   		 		if (ascending){
   		 			return new Integer(op1.getBoughtCountSubSoldCount()).compareTo(new Integer(op2.getBoughtCountSubSoldCount()));
   		 		}
   		 		return -new Integer(op1.getBoughtCountSubSoldCount()).compareTo(new Integer(op2.getBoughtCountSubSoldCount()));
		}
  	 }

	private class VolumeComparator implements Comparator {
		private boolean ascending;
		public VolumeComparator(boolean asc) {
			ascending = asc;
		}
   	    public int compare(Object o1, Object o2) {
   		 		OpportunityTotal op1=(OpportunityTotal)o1;
   		 		OpportunityTotal op2=(OpportunityTotal)o2;
   		 		if (ascending){
   		 			return new Double(op1.getCallsAmountAddPutsAmount()).compareTo(new Double(op2.getCallsAmountAddPutsAmount()));
   		 		}
   		 		return -new Double(op1.getCallsAmountAddPutsAmount()).compareTo(new Double(op2.getCallsAmountAddPutsAmount()));
		}
  	 }
	
	private class BoughtCountComparator implements Comparator {
		private boolean ascending;
		public BoughtCountComparator(boolean asc) {
			ascending = asc;
		}
		public int compare(Object o1, Object o2) {
				OpportunityTotal op1=(OpportunityTotal)o1;
	 			OpportunityTotal op2=(OpportunityTotal)o2;
	 			if (ascending){
	 				return new Integer(op1.getBoughtCount()).compareTo(new Integer(op2.getBoughtCount()));
	 			}
	 			return -new Integer(op1.getBoughtCount()).compareTo(new Integer(op2.getBoughtCount()));
			}
		}
	
	private class SoldCountComparator implements Comparator {
		private boolean ascending;
		public SoldCountComparator(boolean asc) {
			ascending = asc;
		}
   	    public int compare(Object o1, Object o2) {
   		 		OpportunityTotal op1=(OpportunityTotal)o1;
   		 		OpportunityTotal op2=(OpportunityTotal)o2;
   		 		if (ascending){
   		 			return new Integer(op1.getSoldCount()).compareTo(new Integer(op2.getSoldCount()));
   		 		}
   		 		return -new Integer(op1.getSoldCount()).compareTo(new Integer(op2.getSoldCount()));
		}
  	 }
	private class ShiftParamComparator implements Comparator<Opportunity> {
		private boolean ascending;
		private SkinGroup skinGroup;
		public ShiftParamComparator(boolean asascending, SkinGroup skinGroup) {
			this.ascending = asascending;
			this.skinGroup = skinGroup;
		}
   	    public int compare(Opportunity o1, Opportunity o2) {
   		 		if (ascending) {
   		 			return new Double(o1.getSkinGroupMappings().get(skinGroup).getShiftParameter())
	 						.compareTo(new Double(o2.getSkinGroupMappings().get(skinGroup).getShiftParameter()));
   		 		}
   		 		return -new Double(o1.getSkinGroupMappings().get(skinGroup).getShiftParameter())
	 						.compareTo(new Double(o2.getSkinGroupMappings().get(skinGroup).getShiftParameter()));
		}
  	}

	private class AutoShiftParamComparator implements Comparator {
		private boolean ascending;
		public AutoShiftParamComparator(boolean asc) {
			ascending=asc;
		}
   	    public int compare(Object o1, Object o2) {
   		 		Opportunity op1=(Opportunity)o1;
   		 		Opportunity op2=(Opportunity)o2;
   		 		if (ascending){
   		 			return new Double(op1.getAutoShiftParameter()).compareTo(new Double(op2.getAutoShiftParameter()));
   		 		}
   		 		return -new Double(op1.getAutoShiftParameter()).compareTo(new Double(op2.getAutoShiftParameter()));
		}
  	 }

	private class ExposureParamComparator implements Comparator<Opportunity> {
		private boolean ascending;
		private SkinGroup skinGroup;

		public ExposureParamComparator(boolean ascending, SkinGroup skinGroup) {
			this.ascending = ascending;
			this.skinGroup = skinGroup;
		}

		public int compare(Opportunity o1, Opportunity o2) {
			if (ascending) {
				return new Double(o1.getSkinGroupMappings().get(skinGroup).getMaxExposure())
							.compareTo(new Double(o2.getSkinGroupMappings().get(skinGroup).getMaxExposure()));
			}
			return -new Double(o1.getSkinGroupMappings().get(skinGroup).getMaxExposure())
							.compareTo(new Double(o2.getSkinGroupMappings().get(skinGroup).getMaxExposure()));
		}
	}
}
