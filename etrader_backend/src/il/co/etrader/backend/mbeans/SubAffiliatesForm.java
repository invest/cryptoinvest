package il.co.etrader.backend.mbeans;

import java.io.Serializable;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;

import il.co.etrader.backend.bl_managers.MarketingManager;
import il.co.etrader.backend.util.Constants;
import il.co.etrader.backend.util.Utils;
import il.co.etrader.bl_vos.MarketingSubAffiliate;
import il.co.etrader.util.CommonUtil;



public class SubAffiliatesForm implements Serializable {


	private static final long serialVersionUID = -2765640294879796340L;

	private String subAffName;
	private String affName;
	private ArrayList<MarketingSubAffiliate> list;
	private MarketingSubAffiliate subAffiliate;
	private SelectItem pixel;

	/**
	 * @throws SQLException
	 */
	public SubAffiliatesForm() throws SQLException{

		subAffName = null;
		affName = null;
		pixel = null;
		list = null;
		subAffiliate = null;
		updateList();
	}

	/**
	 * Update sub affiliates list
	 * @return
	 * @throws SQLException
	 */
	public String updateList() throws SQLException{
		list = MarketingManager.getMarketingSubAffilates(subAffName, affName);
		CommonUtil.setTablesToFirstPage();
		return null;
	}

	/**
	 * Update / insert location
	 * @return
	 * @throws SQLException
	 */
	public String updateInsert() throws SQLException{
		boolean isUpdate;
		if (subAffiliate.getId() == 0){
			isUpdate = false;
		}
		else{
			isUpdate = true;
		}
		FacesContext context = FacesContext.getCurrentInstance();
		subAffiliate.setWriterId(Utils.getWriter().getWriter().getId());
		boolean res = MarketingManager.insertUpdateSubAffiliate(subAffiliate);
		if (res == false) {
			return null;
		}
		if (isUpdate) {
			FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_INFO,CommonUtil.getMessage("general.update.success",null),null);
			context.addMessage(null, fm);
            
		}
		else {
			FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_INFO,CommonUtil.getMessage("general.insert.success",null),null);
			context.addMessage(null, fm);
            
		}
		updateList();
		return Constants.NAV_MARKETING_SUB_AFFILATES;
	}

	/**
	 * Delete pixel from sub affiliate
	 * @return
	 * @throws SQLException
	 */
	public String deletePixel() throws SQLException {
		FacesContext context = FacesContext.getCurrentInstance();
		MarketingManager.deletePixelByAffiliateId(0,subAffiliate.getId(), ((Long)pixel.getValue()).longValue());
		ArrayList<SelectItem> pixelsList = new ArrayList<SelectItem>();
		for (SelectItem si : subAffiliate.getPixelsSi()) {
			if (((Long)si.getValue()) != ((Long)pixel.getValue())) {
				pixelsList.add(si);
			}
		}
		subAffiliate.setPixelsSi(pixelsList);
		context.renderResponse();
		return null;
	}

	public int getListSize() {
		return list.size();
	}

	/**
	 * Navigate to edit location
	 * @return
	 * @throws SQLException
	 */
	public String navEdit() {
		return Constants.NAV_MARKETING_SUB_AFFILATE;
	}

	/**
	 * Navigate to insert new location
	 * @return
	 * @throws SQLException
	 */
	public String navNew() throws SQLException {
		subAffiliate = new MarketingSubAffiliate();
		subAffiliate.setWriterId(Utils.getWriter().getWriter().getId());
		subAffiliate.setWriterName(Utils.getWriter().getWriter().getUserName());
		return Constants.NAV_MARKETING_SUB_AFFILATE;
	}

	/**
	 * @return the list
	 */
	public ArrayList getList() {
		return list;
	}

	/**
	 * @param list the list to set
	 */
	public void setList(ArrayList list) {
		this.list = list;
	}

	/**
	 * @return the subAffName
	 */
	public String getSubAffName() {
		return subAffName;
	}

	/**
	 * @param subAffName the subAffName to set
	 */
	public void setSubAffName(String subAffName) {
		this.subAffName = subAffName;
	}

	/**
	 * @return the pixel
	 */
	public SelectItem getPixel() {
		return pixel;
	}

	/**
	 * @param pixel the pixel to set
	 */
	public void setPixel(SelectItem pixel) {
		this.pixel = pixel;
	}

	/**
	 * @return the affName
	 */
	public String getAffName() {
		return affName;
	}

	/**
	 * @param affName the affName to set
	 */
	public void setAffName(String affName) {
		this.affName = affName;
	}

	/**
	 * @return the subAffiliate
	 */
	public MarketingSubAffiliate getSubAffiliate() {
		return subAffiliate;
	}

	/**
	 * @param subAffiliate the subAffiliate to set
	 */
	public void setSubAffiliate(MarketingSubAffiliate subAffiliate) {
		this.subAffiliate = subAffiliate;
	}

}
