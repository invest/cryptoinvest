package il.co.etrader.backend.mbeans;

import il.co.etrader.bl_managers.PopulationsManagerBase;

/**
 * CallMeForm
 * @author eyalo
 */
public class CallMeForm extends PopulationsEntriesForm {
	private static final long serialVersionUID = -6347577378148050931L;

	public CallMeForm() {
		super();
	}
	
	public void init() {
		super.init();
		setSupportMoreOptionsType(PopulationsManagerBase.SUPPORT_MORE_OPTIONS_TYPE_CALL_ME);
	}
}
