package il.co.etrader.backend.mbeans;

import java.io.Serializable;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;

import org.apache.log4j.Logger;

import il.co.etrader.backend.bl_managers.OpportunityQuotes0100Manager;
import il.co.etrader.backend.bl_vos.OpportunityQuotes0100;
import il.co.etrader.util.CommonUtil;

public class OpportunityQuotes0100Form implements Serializable {
	private static final Logger logger = Logger.getLogger(OpportunityQuotes0100Form.class);

	private static final long serialVersionUID = 6006831103298729688L;

	private ArrayList<OpportunityQuotes0100> list;
	private Date from;
	private long marketId;
	private long oppId;

	public OpportunityQuotes0100Form() throws SQLException{
		GregorianCalendar gc = new GregorianCalendar();
		//set time as 00:00:00
		/*gc.set(Calendar.HOUR_OF_DAY, 0);
		gc.set(Calendar.MINUTE, 0);
		gc.set(Calendar.SECOND, 0);*/
		from = gc.getTime();

		marketId = 0;
		oppId = 0;
		search();
	}

	public int getListSize() {
		return list.size();
	}

	public String search() throws SQLException{
		try {
			list = OpportunityQuotes0100Manager.updateList(from, marketId, oppId);
		} catch (Exception e) {
			logger.info("cant get opp quotes 0100 list", e);
		}
		CommonUtil.setTablesToFirstPage();
		return null;
	}

	public ArrayList<OpportunityQuotes0100> getList() {
		return list;
	}

	public Date getFrom() {
		return from;
	}

	public void setFrom(Date from) {
		this.from = from;
	}

	public long getMarketId() {
		return marketId;
	}

	public void setMarketId(long marketId) {
		this.marketId = marketId;
	}

	public long getOppId() {
		return oppId;
	}

	public void setOppId(long oppId) {
		this.oppId = oppId;
	}

	public void setList(ArrayList<OpportunityQuotes0100> list) {
		this.list = list;
	}

	public String toString() {
	    final String TAB = "    ";
	    String retValue = "";
	    retValue = "OpportunityQuotes0100Form ( "
	        + super.toString() + TAB
	        + "list = " + this.list + TAB
	        + "from = " + this.from + TAB
	        + "marketId = " + this.marketId + TAB
	        + "oppId = " + this.oppId + TAB
	        + " )";
	    return retValue;
	}
}