package il.co.etrader.backend.mbeans;

import java.io.Serializable;

import org.apache.log4j.Logger;

import com.copyop.common.enums.base.ProfileLinkTypeEnum;
import com.copyop.common.managers.ProfileManager;

import il.co.etrader.util.CommonUtil;

/**
 * @author liors
 *
 */
public class CopyopUserCopyingForm extends CopyopUserCopyForm implements Serializable {
	
	private static final long serialVersionUID = -6788729175675130919L;
	private static final Logger logger = Logger.getLogger(CopyopUserCopyingForm.class);
	
	public CopyopUserCopyingForm() {
		super();
	}
	
	public void search() {
		//assumption: method return size can't return more than ConstantsBase.MAXIMUM_ORACLE_IN_CLAUSE elements.
		this.userCopyList = ProfileManager.getUsersCopy(ProfileLinkTypeEnum.COPY, user.getId(), from, userId, skinId, isStillCopy, isFrozen);	
		//TODO Refactoring. Filters layer - before insert to the list => efficiency. 
		this.userCopyList = ProfileManager.doUserCopyFilters(this.userCopyList, from, userId, skinId, isStillCopy, isFrozen);
		CommonUtil.setTablesToFirstPage();
		CheckIfEmptyMessage();
	}
}
