package il.co.etrader.backend.mbeans;

import java.io.Serializable;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;

import javax.faces.context.FacesContext;

import il.co.etrader.backend.bl_managers.AdminManager;
import il.co.etrader.backend.bl_managers.MarketingManager;
import il.co.etrader.backend.bl_vos.User;
import il.co.etrader.backend.util.Constants;
import il.co.etrader.backend.util.Utils;
import il.co.etrader.bl_vos.MarketingMedium;
import il.co.etrader.util.CommonUtil;



public class MediumsForm implements Serializable {

	private static final long serialVersionUID = 522450867475611126L;

	private String mediumName;
	private ArrayList<MarketingMedium> list;
	private MarketingMedium medium;

	public MediumsForm() throws SQLException{
		updateList();
	}

	public String updateList() throws SQLException{
		
	    FacesContext context = FacesContext.getCurrentInstance();
        User u = (User) context.getApplication().createValueBinding(Constants.BIND_USER).getValue(context);
        long writerIdForSkin = Constants.DEFAULT_WRITER_ID_FOR_SKIN;;

        if (u.isPartnerMarketingSelected()){ 
        	writerIdForSkin = Utils.getWriter().getWriter().getId();
        }

		list = MarketingManager.getMarketingMediums(mediumName, writerIdForSkin);

		CommonUtil.setTablesToFirstPage();
		return null;
	}

	/**
	 * Update / insert medium
	 * @return
	 * @throws SQLException
	 */
	public String updateInsert() throws SQLException{

		medium.setWriterId(AdminManager.getWriterId());

		boolean res = MarketingManager.insertUpdateMedium(medium);
		if (res == false) {
			return null;
		}
		updateList();
		return Constants.NAV_MARKETING_MEDIUMS;
	}

	public int getListSize() {
		return list.size();
	}

	/**
	 * Navigate to edit medium
	 * @return
	 * @throws SQLException
	 */
	public String navEdit() {
		return Constants.NAV_MARKETING_MEDIUM;
	}

	/**
	 * Navigate to insert new medium
	 * @return
	 * @throws SQLException
	 */
	public String navNew() throws SQLException {
		medium = new MarketingMedium();
		medium.setWriterId(AdminManager.getWriterId());
		medium.setTimeCreated(new Date());

		return Constants.NAV_MARKETING_MEDIUM;
	}

	/**
	 * @return the list
	 */
	public ArrayList getList() {
		return list;
	}

	/**
	 * @param list the list to set
	 */
	public void setList(ArrayList list) {
		this.list = list;
	}

	/**
	 * @return the medium
	 */
	public MarketingMedium getMedium() {
		return medium;
	}

	/**
	 * @param medium the medium to set
	 */
	public void setMedium(MarketingMedium medium) {
		this.medium = medium;
	}

	/**
	 * @return the mediumName
	 */
	public String getMediumName() {
		return mediumName;
	}

	/**
	 * @param mediumName the mediumName to set
	 */
	public void setMediumName(String mediumName) {
		this.mediumName = mediumName;
	}





}
