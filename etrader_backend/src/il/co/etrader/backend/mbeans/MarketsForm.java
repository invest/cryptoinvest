package il.co.etrader.backend.mbeans;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Serializable;
import java.io.Writer;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.Iterator;
import java.util.Locale;
import java.util.Map;

import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.Market;
import com.anyoption.common.managers.BubblesManager;

import il.co.etrader.backend.bl_managers.AdminManager;
import il.co.etrader.backend.bl_managers.WritersManager;
import il.co.etrader.backend.bl_vos.WriterWrapper;
import il.co.etrader.backend.util.Constants;
import il.co.etrader.backend.util.Utils;
import il.co.etrader.util.CommonUtil;

public class MarketsForm implements Serializable {

    private static final long serialVersionUID = -2488309487232984472L;

    private static final Logger logger = Logger.getLogger(MarketsForm.class);

    private static final String DELEMITER = ",";

	private Market market;
	private ArrayList list;
	private String name;
	private Long groupId;
	private Long oddsTypeId;
	private float fXALParameter;
	private static final String FXAL_QUOTE_PARAMETER = "FXAL=";

	public MarketsForm() throws SQLException{
		groupId = new Long(0);
		oddsTypeId = null;
		search();
	}

	public String updateInsert() throws SQLException{
		setMarketQuoteParams();
		boolean res = AdminManager.updateInsertMarket(market, oddsTypeId);
		if (!res) {
			return null;
		}
		//TraderManager.addToLog(writerId, table, key, command, desc);
		search();
		return Constants.NAV_MARKETS;
	}

	public String navEdit() {
		updateFXALParameter();
		return Constants.NAV_MARKET;
	}

	public String navList() throws SQLException{
		search();
		return Constants.NAV_MARKETS;
	}

	public String navNew() throws SQLException{
		market=new Market();
		market.setTimeCreated(new Date());
		market.setWriterId(AdminManager.getWriterId());
		return Constants.NAV_MARKET;
	}

	public String search() throws SQLException{

		list=AdminManager.searchMarkets(name,groupId.longValue());
		CommonUtil.setTablesToFirstPage();
		return null;
	}
	public ArrayList getMarketGroupsAll() throws SQLException{
		FacesContext context = FacesContext.getCurrentInstance();
		ArrayList l=AdminManager.getGroupsSI();
		l.add(0,new SelectItem(new Long(0),CommonUtil.getMessage("general.all",null, Utils.getWriterLocale(context))));

		return l;
	}

	public ArrayList getTypeOfShiftingGroups() throws SQLException{
		FacesContext context = FacesContext.getCurrentInstance();
		ArrayList l=new ArrayList();
		l.add(0,new SelectItem(new Long(1),CommonUtil.getMessage("market.shifting.type.average",null, Utils.getWriterLocale(context))));
		l.add(0,new SelectItem(new Long(0),CommonUtil.getMessage("market.shifting.type.fixed",null, Utils.getWriterLocale(context))));
		l.add(0,new SelectItem(new Long(2),CommonUtil.getMessage("market.shifting.type.max",null, Utils.getWriterLocale(context))));
		Collections.sort(l, new CommonUtil.selectItemComparator());
		return l;
	}
	public ArrayList getMarketGroups() throws SQLException{

		return AdminManager.getGroupsSI();
	}


	public ArrayList getList() {
		return list;
	}
	public void setList(ArrayList list) {
		this.list = list;
	}
	public int getListSize() {
		return list.size();
	}
	public Long getGroupId() {
		return groupId;
	}
	public void setGroupId(Long groupId) {
		this.groupId = groupId;
	}
	public Market getMarket() {
		return market;
	}
	public void setMarket(Market market) {
		this.market = market;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Constructs a <code>String</code> with all attributes
	 * in name = value format.
	 *
	 * @return a <code>String</code> representation
	 * of this object.
	 */
	@Override
	public String toString()
	{
	    final String TAB = " \n ";

	    String retValue = "";

	    retValue = "MarketsForm ( "
	        + super.toString() + TAB
	        + "market = " + this.market + TAB
	        + "list = " + this.list + TAB
	        + "name = " + this.name + TAB
	        + "groupId = " + this.groupId + TAB
	        + " )";

	    return retValue;
	}

	public Long getOddsTypeId() {
		return oddsTypeId;
	}

	public void setOddsTypeId(Long oddsTypeId) {
		this.oddsTypeId = oddsTypeId;
	}
	
	public void headersForCSVExport(File fileToSave) throws IOException {
		Writer output = null;

		logger.debug("Trying to write in CSV File...");
		try {
		    output = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(fileToSave), "UTF-8"));
		    output.write("ID");			// 1 -> markets.id
		    output.write(DELEMITER);
		    output.write("Name");		// 2 -> markets.name
		    output.write(DELEMITER);
		    output.write("Display Name");	// 3 -> markets.displayname
		    output.write(DELEMITER);
		    output.write("RIC");		// 4 -> markets.feedname
		    output.write(DELEMITER);
		    output.write("Time of creation");	// 5 -> markets.timecreated
		    output.write(DELEMITER);
		    output.write("Updated by");		// 6 -> markets.writername
		    output.write(DELEMITER);
		    output.write("Stock Market");	// 7 -> markets.exchangeid
		    output.write(DELEMITER);
		    output.write("Limitation");		// 8 -> markets.invlimitgroup
		    output.write(DELEMITER);
		    output.write("\n");
		} catch (Exception e) {
		    logger.error("Error when exporting CSV file:", e);
		} finally {
		    try {
			output.flush();
			output.close();
		    } catch (Exception e) {
			logger.error("Trader -> Markets header Data Form Error, Can't close file:", e);
		    }
		}
	    }

    public void csvExport(File fileToSave) throws Exception {
	headersForCSVExport(fileToSave);

	Writer output = null;

	try {
	    ArrayList<com.anyoption.common.beans.Market> markets = getList();
	    output = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(fileToSave, true), "UTF-8"));
	    for (com.anyoption.common.beans.Market mar :  markets) {
		//markets.id -> 1
		output.write(String.valueOf(mar.getId()));
		output.write(DELEMITER);
		//markets.name -> 2
		output.write(CommonUtil.appendDQ(mar.getName()));
		output.write(DELEMITER);
		//markets.displayname -> 3
		//gets the respective market displayed in GUI message to be displayed as is, standardized 
		output.write(CommonUtil.appendDQ(CommonUtil.getMessage(mar.getDisplayNameKey(), null, new Locale("en"))));
		output.write(DELEMITER);
		//markets.feedname -> 4
		output.write(mar.getFeedName());
		output.write(DELEMITER);
		//markets.timecreated -> 5
		output.write(String.valueOf(mar.getTimeCreated()));
		output.write(DELEMITER);
		//markets.writername -> 6
		if((WritersManager.getAllWritersMap().get(mar.getWriterId())) != null) {
		    output.write( (WritersManager.getAllWritersMap().get(mar.getWriterId())).getUserName() );
		    output.write(DELEMITER);
		}
		//markets.exchangeid -> 7
		output.write(mar.getExchangeName());
		output.write(DELEMITER);
		//markets.invlimitgroup -> 8
		output.write(mar.getInvestmentLimitsGroupName());
		output.write(DELEMITER);
		output.write("\n");
	    }
	} catch (Exception e) {
	    logger.error("Error when exporting CSV Data file:", e);
	} finally {
	    try {
		output.flush();
		output.close();
	    } catch (Exception e) {
		logger.error("Investment Data Form Error, Can't close file:", e);
	    }
	}
    }
	    
    /**
     * @return a pop up to save the given report or open it
     * @throws IOException
     * @throws NumberFormatException
     * @throws SQLException
     */
    public String exportTableToCSV() throws IOException, NumberFormatException, SQLException {
	// Set the filename
	Date dt = new Date();
	SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd_HHmmss");
	String filename = "markets_report_" + fmt.format(dt) + ".csv";

	// Setup the output
	String contentType = "text/csv";

	File tCSVfile = new File(CommonUtil.getProperty(Constants.FILES_PATH) + filename);
	FacesContext fc = FacesContext.getCurrentInstance();
	ExternalContext ex = fc.getExternalContext();
	HttpServletResponse response = (HttpServletResponse) ex.getResponse();
	try {
	    try {
		csvExport(tCSVfile);
	    } catch (Exception e) {
		logger.error("Problem creating CSV file due to: " + e);
		e.printStackTrace();
		return null;
	    }

	    int contentLength = (int) tCSVfile.length();

	    // just making sure nothing non current is retrieved
	    response.reset();

	    response.setContentType(contentType);
	    response.setContentLength(contentLength);
	    response.setCharacterEncoding("UTF-8");
	    response.setHeader("Content-disposition", "attachment; filename=" + filename);

	    // Write the file back out...
	    OutputStream outputStream = response.getOutputStream();
	    FileInputStream fis = new FileInputStream(tCSVfile);

	    try {
		int n = 0;
		byte[] buffer = new byte[4 * 1024];
		while ((n = fis.read(buffer)) != -1) {
		    outputStream.write(buffer, 0, n);
		}
		outputStream.flush();
	    } finally {
		fis.close();
		outputStream.close();
	    }
	} finally {
	    tCSVfile.delete();
	}

	fc.responseComplete();

	return null;
    }
    
	public ArrayList<SelectItem> getAssetBubblesPricingLevels() {
		ArrayList<SelectItem> bubblesLevels = new ArrayList<SelectItem>();
		Map<Long, String> bubbleLevels = BubblesManager.getBubbleLevels();
		Iterator it = bubbleLevels.entrySet().iterator();
		while (it.hasNext()) {
			Map.Entry pair = (Map.Entry)it.next();
			SelectItem si = new SelectItem();
			si.setValue(pair.getKey());
			si.setLabel((String)pair.getValue());
			bubblesLevels.add(si);
		}
		bubblesLevels = WriterWrapper.translateSI(bubblesLevels);
		return bubblesLevels;
	}

	private void setMarketQuoteParams() {
		int indx;
		if (market.getQuoteParams() != null) {
			indx = market.getQuoteParams().indexOf(FXAL_QUOTE_PARAMETER);
		} else {
			indx = -1;
		}
		if (indx > -1) {
			int end = market.getQuoteParams().substring(indx).indexOf(";");
			if (end < 0) {
				end = market.getQuoteParams().length();
			}
			market.setQuoteParams(market.getQuoteParams().replace(market.getQuoteParams().substring(indx + FXAL_QUOTE_PARAMETER.length(), end),
											String.valueOf(fXALParameter)));
		} else {
			if (market.getQuoteParams() != null && !market.getQuoteParams().endsWith(";")) {
				market.setQuoteParams(market.getQuoteParams() + ";" + FXAL_QUOTE_PARAMETER + fXALParameter + ";");
			} else if (market.getQuoteParams() != null) {
				market.setQuoteParams(market.getQuoteParams() + FXAL_QUOTE_PARAMETER + fXALParameter + ";");
			} else {
				market.setQuoteParams(FXAL_QUOTE_PARAMETER + fXALParameter + ";");
			}
		}
	}

	private void updateFXALParameter() {
		if (market.getQuoteParams() == null) {
			return;
		}
		int indx = market.getQuoteParams().indexOf(FXAL_QUOTE_PARAMETER);
		if (indx > -1) {
			int end = market.getQuoteParams().substring(indx).indexOf(";");
			if (end < 0) {
				end = market.getQuoteParams().length();
			}
			String fXALParameterStr = market.getQuoteParams().substring(indx + FXAL_QUOTE_PARAMETER.length(), end);
			try {
				fXALParameter = Float.valueOf(fXALParameterStr);
			} catch(NumberFormatException e) {
				logger.warn("Cannot parse FXAL param. Setting it to 0. Given value: " + fXALParameterStr, e);
				fXALParameter = 0;
			}
		}
	}

	public float getFXALParameter() {
		return fXALParameter;
	}

	public void setFXALParameter(float fXALParameter) {
		this.fXALParameter = fXALParameter;
	}
}