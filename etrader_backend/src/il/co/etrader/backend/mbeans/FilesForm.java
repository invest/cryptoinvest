package il.co.etrader.backend.mbeans;

import java.io.IOException;
import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.TimeZone;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;
import javax.faces.validator.ValidatorException;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.File;
import com.anyoption.common.beans.FileType;
import com.anyoption.common.beans.FilesAdditionalInfo;
import com.anyoption.common.beans.base.UserRegulationBase;
import com.anyoption.common.managers.CreditCardsManagerBase;
import com.anyoption.common.managers.FilesManagerBase;
import com.anyoption.common.managers.UserRegulationManager;
import com.anyoption.common.managers.UsersPendingDocsManagerBase;

import il.co.etrader.backend.bl_managers.ApplicationData;
import il.co.etrader.backend.bl_managers.FilesManager;
import il.co.etrader.backend.bl_managers.UsersManager;
import il.co.etrader.backend.bl_managers.WritersManager;
import il.co.etrader.backend.bl_vos.User;
import il.co.etrader.backend.bl_vos.WriterWrapper;
import il.co.etrader.backend.dao_managers.FilesDAOFilter;
import il.co.etrader.backend.util.Constants;
import il.co.etrader.backend.util.Utils;
import il.co.etrader.bl_managers.ApplicationDataBase;
import il.co.etrader.bl_managers.IssuesManagerBase;
import il.co.etrader.bl_managers.UsersManagerBase;
import il.co.etrader.bl_vos.Skins;
import il.co.etrader.bl_vos.UserBase;
import il.co.etrader.util.CommonUtil;
import il.co.etrader.util.ConstantsBase;

public class FilesForm implements Serializable {
	/**
	 *
	 */
	private static final long serialVersionUID = 2593884745967361812L;
	private static final Logger log = Logger.getLogger(FilesForm.class);

	private File file;
	private ArrayList<File> list;
	private String cardId;
	// filters
	ArrayList<SelectItem> allFileTypesSI = null;
	private long fileTypeId = 0; // ALL - default, file_types
	private int uploaded = 0; // 0 - ALL (default), 1 - Yes, 2 - No
	private int supportApproved = 0; // 0 - ALL (default), 1 - Yes, 2 - No
	private Date from;
	private Date to;
	private boolean isPrevApproved;
	private boolean isPrevControlApproved;
	private int rejectReason = 0;
	private int controlRejectReason = 0;
	private boolean showSendMailScreen = false;
	private boolean showSuccessMail = false;
	private boolean isCurrentFiles = true;
	
	private static final int DOCS_FOR_UPDATE_MAIL_HTML_TYPE = 1;
	private static final int DOCS_FOR_UPDATE_ISSUES_COMMENTS_TYPE = 2;
	
	public static final int FILE_NO_REJECT_REASON = 0;
	public static final int FILE_REJECT_REASON_OTHER = 44;
	
	public String getCardId() {
		return cardId;
	}

	public void setCardId(String cardId) {
		this.cardId = cardId;
	}

	public FilesForm() throws SQLException{
		resetFilter();
		search();
	}

	public int getListSize() {
		if(list!=null) {
			return list.size();
		} else {
			return 0;
		}
	}

	public String getFilesPath() {
		return CommonUtil.getProperty(Constants.FILES_SERVER_PATH);
	}

	public String updateInsertFile() throws SQLException {
		FacesContext context = FacesContext.getCurrentInstance();
		User user = (User)context.getApplication().createValueBinding(Constants.BIND_USER).getValue(context);
		if (!CommonUtil.isParameterEmptyOrNull(file.getExpYear()) && !CommonUtil.isParameterEmptyOrNull(file.getExpMonth())
				&& !CommonUtil.isParameterEmptyOrNull(file.getExpDay())) {
			FilesManagerBase.updateExpDate(file);
		} else {
			file.setExpDate(null);
		}
		if (!file.isSupportReject()) {
			file.setRejectReason(rejectReason);
		}
		
		if (!file.isControlReject()) {
			file.setControlRejectReason(controlRejectReason);
		}
		
		if(file.isSupportReject()){
			if(file.getComment()==null || file.getComment().length()<1 || file.getRejectReason()==0) {
				FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR,CommonUtil.getMessage("files.error.no.reject.reason", null, Utils.getWriterLocale(context)),null);
				context.addMessage(null, fm);
				return null;

			}
		}
		
		if((file.getFile() == null && file.getFileName() == null) && (file.isApproved() || file.isSupportReject() || file.isControlApproved() || file.isControlReject())){
			FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR,CommonUtil.getMessage("files.error.no.file.upload", null, Utils.getWriterLocale(context)),null);
			context.addMessage(null, fm);
			return null;
		}
		
		if (file.isApproved() && file.getExpDate() == null
				&& (file.getFileTypeId() == FileType.USER_ID_COPY || file.getFileTypeId() == FileType.DRIVER_LICENSE
						|| file.getFileTypeId() == FileType.UTILITY_BILL || file.getFileTypeId() == FileType.PASSPORT
						|| file.getFileTypeId() == FileType.CC_HOLDER_ID)) {
			FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR,
					CommonUtil.getMessage("files.error.mandatory.exp.date", null, Utils.getWriterLocale(context)),
					null);
			context.addMessage(null, fm);
			return null;
		}

		if (file.isControlApproved() && file.getExpDate() == null
				&& (file.getFileTypeId() == FileType.USER_ID_COPY || file.getFileTypeId() == FileType.DRIVER_LICENSE
						|| file.getFileTypeId() == FileType.UTILITY_BILL || file.getFileTypeId() == FileType.PASSPORT
						|| file.getFileTypeId() == FileType.CC_HOLDER_ID)) {
			FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR,
					CommonUtil.getMessage("files.error.mandatory.exp.date", null, Utils.getWriterLocale(context)),
					null);
			context.addMessage(null, fm);
			return null;
		}
		
		if(file.isControlReject()){
			String msg = ""; 
			if(file.getControlRejectReason() == 0){
				FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR,CommonUtil.getMessage("files.error.no.control.reject.reason", null, Utils.getWriterLocale(context)),null);
				context.addMessage(null, fm);
				return null;
			} else if (file.getControlRejectReason() == FILE_REJECT_REASON_OTHER && (file.getControlRejectComment() == null || (file.getControlRejectComment() != null && file.getControlRejectComment().length() < 1))) {
				FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR,CommonUtil.getMessage("files.error.no.control.reject.reason.comment", null, Utils.getWriterLocale(context)),null);
				context.addMessage(null, fm);
				return null;
			}
		}
		
		if(file.isApproved() && file.getRejectReason() > 0) {
			FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR,CommonUtil.getMessage("files.error.approved.with.reject.reason", null, Utils.getWriterLocale(context)),null);
			context.addMessage("filesForm:rejSup", fm);
			return null;
		}
		
		if (isCreditCardFileType()){
			if (!CommonUtil.isParameterEmptyOrNull(cardId)){
				file.setCcId(Long.parseLong(cardId));
			}else{
				FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR,CommonUtil.getMessage("files.error.no.cc.id", null, Utils.getWriterLocale(context)),null);
				context.addMessage("filesForm:ccId", fm);
				return null;
			}
		}

		if(file.getFile()!=null) {
			file.setUploaderId(Utils.getWriter().getWriter().getId());
			file.setUploadDate(new Date());
		}
		
		//Clear comments after re-upload
		if(!file.isSupportReject() && !CommonUtil.IsParameterEmptyOrNull(file.getComment())){
			file.setComment("");
		}
		
		if(!file.isControlReject() && !CommonUtil.IsParameterEmptyOrNull(file.getControlRejectComment())){
			file.setControlRejectComment("");
		}
		
		boolean res = false;
		if (file.isNotNeeded()) {
			try {
				res = UsersManager.updateInsertFile(file, user, File.STATUS_NOT_NEEDED);
			} catch (IOException e) {
				FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR,CommonUtil.getMessage("files.upload.problem", null, Utils.getWriterLocale(context)),null);
				context.addMessage("filesForm:upload", fm);
				return null;
			}
		} else {
			try {
				int fileStatusId = (int) file.getFileStatusId();
				if(file.getFile() != null){
					fileStatusId = File.STATUS_IN_PROGRESS;					
					FilesManagerBase.setFileDefaultValuesBE(file, file.getFileName());
				}
				res = UsersManager.updateInsertFile(file, user, fileStatusId);
			} catch (IOException e) {
				FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR,CommonUtil.getMessage("files.upload.problem", null, Utils.getWriterLocale(context)),null);
				context.addMessage("filesForm:upload", fm);
				return null;
			}
		}

		if (res==false) {
			return null;
		}
		
		// Execute reward tasks's system when the document approved (only first time approve for each file)
		// User regulated need to be approved by control, non regulated by support.
//		if ((!user.getIsRegulated() && file.isApproved() && (file.isApproved() != isPrevApproved)) ||
//				(user.getIsRegulated() && file.isControlApproved() && (file.isControlApproved() != isPrevControlApproved))) {
//			try {
//				if (!RewardsManager.isDocPreviouslyApproved(user.getId(), file.getId())) {
//					RewardUserTasksManager.rewardTasksHandler(TaskGroupType.DOCS, user.getId(), 0, file.getId(), BonusManagerBase.class, (int) Utils.getWriter().getWriter().getId());
//				}
//			} catch (SQLException e) {
//				log.error("Error when check if the document previously approved. ", e);
//			}
//		}
		
		if(file.isControlReject() 
				&& user.getUserRegulation().getApprovedRegulationStep() > UserRegulationBase.REGULATION_FIRST_DEPOSIT
					&& user.getUserRegulation().getApprovedRegulationStep() < UserRegulationBase.REGULATION_CONTROL_APPROVED_USER) {
			UserRegulationBase urb = new UserRegulationBase();
			urb.setApprovedRegulationStep(UserRegulationBase.REGULATION_MANDATORY_QUESTIONNAIRE_DONE);
			urb.setUserId(user.getId());
			urb.setWriterId(Utils.getWriter().getWriter().getId());
			urb.setComments("Control reject - back to step 3");
			UserRegulationManager.updateRegulationStep(urb);
		}
		
		if (user.isSupportSelected()) {
			UsersManager.loadUserStripFields(user, true, null);
		}
		
		if (file.getFileTypeId() == FileType.PASSPORT) {
			File id = UsersManager.getUserFileByType(user.getId(), FileType.USER_ID_COPY, Utils.getWriterLocale(context));
			setNotNeeded(id, user);
			File driversLicence = UsersManager.getUserFileByType(user.getId(), FileType.DRIVER_LICENSE, Utils.getWriterLocale(context));
			setNotNeeded(driversLicence, user);
		} else if (file.getFileTypeId() == FileType.USER_ID_COPY) {
			File passport = UsersManager.getUserFileByType(user.getId(), FileType.PASSPORT, Utils.getWriterLocale(context));
			setNotNeeded(passport, user);
			File driversLicence = UsersManager.getUserFileByType(user.getId(), FileType.DRIVER_LICENSE, Utils.getWriterLocale(context));
			setNotNeeded(driversLicence, user);
		} else if(file.getFileTypeId() == FileType.DRIVER_LICENSE) {
			File id = UsersManager.getUserFileByType(user.getId(), FileType.USER_ID_COPY, Utils.getWriterLocale(context));
			setNotNeeded(id, user);
			File passport = UsersManager.getUserFileByType(user.getId(), FileType.PASSPORT, Utils.getWriterLocale(context));
			setNotNeeded(passport, user);
		}
		
		try {
			UsersPendingDocsManagerBase.calculatePendingDocs(file.getId(), UsersPendingDocsManagerBase.EMPTY_PARAM);
		} catch (Exception e) {
			log.error("Can't calculate Pending docs ", e);
		}

		search();

		return Constants.NAV_FILES;
	}

	void setNotNeeded(File file, User user) throws SQLException {
		FacesContext context = FacesContext.getCurrentInstance();
		
		if (file != null) {
			try {
				UsersManager.updateInsertFile(file, user, File.STATUS_NOT_NEEDED);
			} catch (IOException e) {
				FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR,CommonUtil.getMessage("files.upload.problem", null, Utils.getWriterLocale(context)),null);
				context.addMessage("filesForm:upload", fm);
								
			}
		}
	}
	
	public String bNavEditFile() {
		cardId = String.valueOf(file.getCcId());
		setFilesAdditionalInfo();
		return Constants.NAV_FILE;
	}


	public String initFileValues() throws SQLException{
		FacesContext context=FacesContext.getCurrentInstance();
		cardId = ConstantsBase.EMPTY_STRING;
		file=new File();
		file.setWriterName(context.getExternalContext().getRemoteUser());
		file.setTimeCreated(new Date());
		file.setUtcOffsetCreated(Utils.getUser((long)file.getUserId()).getUtcOffset());
		file.setFileTypeId(FileType.ACCOUNT_CLOSING);

		return Constants.NAV_FILE;

	}


	public String search() throws SQLException{
		if(to.before(from)) {
			FacesContext context=FacesContext.getCurrentInstance();
			FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR,CommonUtil.getMessage("files.invalid.date.range", null, Utils.getWriterLocale(context)),null);
			context.addMessage("filesForm:ccId", fm);
			return null;
		}
		list = UsersManager.searchFiles(createFilter());
		CommonUtil.setTablesToFirstPage();
		return null;
	}


	public ArrayList<File> getList() {
		return list;
	}

	public File getFile() {
		return file;
	}

	public void setFile(File file) {
		this.file = file;
		setPrevApproved(file.isApproved());
		setPrevControlApproved(file.isControlApproved());
	}

	/**
	 * Constructs a <code>String</code> with all attributes
	 * in name = value format.
	 *
	 * @return a <code>String</code> representation
	 * of this object.
	 */
	public String toString()
	{
	    final String TAB = " \n ";

	    String retValue = "";

	    retValue = "FilesForm ( "
	        + super.toString() + TAB
	        + "file = " + this.file + TAB
	        + "list = " + this.list + TAB
	        + " )";

	    return retValue;
	}

	public void updateFileTypeId(ValueChangeEvent ev) {
		file.setFileTypeId((Long) ev.getNewValue());

		if (!isCreditCardFileType()){
			cardId = ConstantsBase.EMPTY_STRING;
		}
		setFilesAdditionalInfo();
	}
	
	public void updateIncludeInUpdateStatus() {
		file.setIncludeInUpdateStatus(!file.getIncludeInUpdateStatus());
	}

	public boolean isCreditCardFileType(){
		return ApplicationData.getFileTypes().get(file.getFileTypeId()).isCreditCardFile();
	}

	/**
	 * Set the files additional info by the file type id in order to display the additional info fields.
	 */
	public void setFilesAdditionalInfo(){
		FilesAdditionalInfo f = null;
		try {
			f = FilesManager.getFilesAdditionalInfo().get(file.getFileTypeId());
		} catch (SQLException e) {
			log.error("Can't get file additional info", e);
		}
		file.setFilesAdditionalInfo(f);
	}

	/**
	 * Validate files additional info fields.
	 * @param context
	 * @param comp
	 * @param value
	 * @throws UnsupportedEncodingException
	 */
	public void validateNumber(FacesContext context, UIComponent comp, Object value) throws UnsupportedEncodingException {
		String v = (String) value;
		if (!CommonUtil.isParameterEmptyOrNull(v) /*&& !CommonUtil.IsParameterEmptyOrNull(file.getFileTypeId())*/){
			long filType = Long.valueOf(file.getFileTypeId());
			if (filType == FileType.SSN){
				String pattern = "[0-9]*";
				if (v.length() < 4){
					String[] params = new String[2];
					params[0] = "4";
					params[1] = "number";
					FacesMessage msg = new FacesMessage(CommonUtil.getMessage("javax.faces.validator.LengthValidator.MINIMUM_detail", params));
					throw new ValidatorException(msg);
				} else if (v.length() > 9){
					String[] params = new String[2];
					params[0] = "9";
					params[1] = "number";
					FacesMessage msg = new FacesMessage(CommonUtil.getMessage("javax.faces.validator.LengthValidator.MAXIMUM_detail", params));
					throw new ValidatorException(msg);
				} else if (!v.matches(pattern)){
					FacesMessage msg = new FacesMessage(CommonUtil.getMessage("javax.faces.validator.LongRangeValidator.LIMIT_detail", null));
					throw new ValidatorException(msg);
				}
			} else if (filType == FileType.DRIVER_LICENSE || filType == FileType.PASSPORT || filType == FileType.USER_ID_COPY){
				String pattern = CommonUtil.getMessage("general.validate.english.and.numbers", null);
				if (!v.matches(pattern)){
					FacesMessage msg = new FacesMessage(CommonUtil.getMessage("register.letters.digits.info", null));
					throw new ValidatorException(msg);
				}
			}
		}
	}

	public void supportRejectListener(ValueChangeEvent event) throws Exception {
		
		try {
			Boolean isChecked = (Boolean) event.getNewValue();
			if (isChecked) {
				file.setApproved(false);
				file.setFileStatusId(File.STATUS_REJECTED);
				file.setRejectionDate(Calendar.getInstance(TimeZone.getTimeZone("GMT")).getTime());
				file.setRejectionWriterId(Utils.getWriter().getWriter().getId());
			} else {
				file.setRejectionDate(null);
				file.setRejectionWriterId(0);
			}
		} catch (Exception ex) {
			log.error("When supportRejectListener", ex);
		}
	}

	public void supportApproveListener(ValueChangeEvent event) throws Exception {
		try {
			Boolean isChecked = (Boolean) event.getNewValue();
			if (isChecked) {
				file.setColor(true);
				file.setSupportReject(false);
				file.setComment("");
				setRejectReason(FILE_NO_REJECT_REASON);
				file.setTimeSupportApproved(Calendar.getInstance(TimeZone.getTimeZone("GMT")).getTime());
				file.setSupportApprovedWriterId(Utils.getWriter().getWriter().getId());
			} else { 
				file.setColor(false);
				setRejectReason(file.getRejectReason());				
				file.setTimeSupportApproved(null);
				file.setSupportApprovedWriterId(0);
			}
		} catch (Exception ex) {
			log.error("When supportApproveListener", ex);
		}
	}
	
	public void controlApproveListener(ValueChangeEvent event) throws Exception {
		try {
			Boolean isChecked = (Boolean) event.getNewValue();
			if (isChecked) {
				file.setControlReject(false);
				file.setControlRejectComment("");
				setControlRejectReason(FILE_NO_REJECT_REASON);
				file.setTimeControlApproved(Calendar.getInstance(TimeZone.getTimeZone("GMT")).getTime());
				file.setControlApprovedWriterId(Utils.getWriter().getWriter().getId());				
			} else {
				setControlRejectReason(file.getControlRejectReason());
				file.setTimeControlApproved(null);
				file.setControlApprovedWriterId(0);
			}
		} catch (Exception ex) {
			log.error("When controlApproveListener", ex);	
		}
	}

	public void controlRejectListener(ValueChangeEvent event) throws Exception {
		try {
			Boolean isChecked = (Boolean) event.getNewValue();
			if (isChecked) {
				file.setControlApproved(false);
				file.setFileStatusId(File.STATUS_REJECTED);
				file.setTimeControlRejected(Calendar.getInstance(TimeZone.getTimeZone("GMT")).getTime());
				file.setControlRejectWriterId(Utils.getWriter().getWriter().getId());
			} else {
				file.setTimeControlRejected(null);
				file.setControlRejectWriterId(0);
			}
		} catch (Exception ex) {
			log.error("When controlRejectListener", ex);
		}
	}
	
	public void rejectReasonListener(ValueChangeEvent event) throws Exception {
		try {
			int reason = (int) event.getNewValue();
			if (reason > 0) {
				file.setRejectReason(reason);
			}
		} catch (Exception ex) {
			log.error("When rejectReasonListener", ex);
		}
	}
	
	public void uploadFileListener() throws Exception {
		try {
			file.setExpDay(null);
			file.setExpMonth(null);
			file.setExpYear(null);
		} catch (Exception ex) {
			log.error("When uploadFileListener", ex);
		}
	}
	
	public void controlRejectReasonListener(ValueChangeEvent event) throws Exception {
		try {
			int reason = (int) event.getNewValue();
			if (reason > 0) {
				file.setControlRejectReason(reason);
			}
		} catch (Exception ex) {
			log.error("When setControlRejectReason", ex);
		}
	}
	
	public ArrayList<SelectItem> getRejectReasons() {
		return CommonUtil.translateSI(ApplicationData.rejectReasonsByType.get(file.getFileTypeId()));
	}
	
	public String getRejectionWriterName() {
		try {
			return WritersManager.getWriter(file.getRejectionWriterId()).getUserName();
		} catch (SQLException e) {
			return "?" + file.getRejectionWriterId() + "?";
		}
	}
	
	public String getControlRejectionWriterName() {
		try {
			return WritersManager.getWriter(file.getControlRejectWriterId()).getUserName();
		} catch (SQLException e) {
			return "?" + file.getControlRejectWriterId() + "?";
		}
	}
	
	public long getFileTypeId() {
		return fileTypeId;
	}

	public void setFileTypeId(long fileTypeId) {
		this.fileTypeId = fileTypeId;
	}

	public int getUploaded() {
		return uploaded;
	}

	public void setUploaded(int uploaded) {
		this.uploaded = uploaded;
	}

	public int getSupportApproved() {
		return supportApproved;
	}

	public void setSupportApproved(int supportApproved) {
		this.supportApproved = supportApproved;
	}

	public void setList(ArrayList<File> list) {
		this.list = list;
	}

	public Date getFrom() {
		return from;
	}

	public void setFrom(Date from) {
		this.from = from;
	}

	public Date getTo() {
		return to;
	}

	public void setTo(Date to) {
		this.to = to;
	}

	FilesDAOFilter createFilter(){
		FacesContext context=FacesContext.getCurrentInstance();
		UserBase user= (UserBase)context.getApplication().createValueBinding(Constants.BIND_USER).getValue(context);
		
		FilesDAOFilter fdf = new FilesDAOFilter();
		fdf.setUserId(user.getId());
		fdf.setFileTypeId(fileTypeId);
		fdf.setUploaded(uploaded);
		fdf.setSupportApproved(supportApproved);
		fdf.setReceivedAtFrom(from);
		fdf.setReceivedAtTo(to);
		fdf.setCurrentFiles(isCurrentFiles);
		return fdf; 
	}
	
	public void resetFilter(){
		fileTypeId = 0; 
		uploaded = 0; 
		supportApproved = 0; 
		from = null;
		to = null;
		//from 1.1.2000, and "To" default value will be today's date
		Calendar c1 = Calendar.getInstance();
		c1.set(2000, 0, 1);
		from = c1.getTime();
		to = Calendar.getInstance().getTime();
		isCurrentFiles = true;
	}
	
	public ArrayList<SelectItem> getAllFileTypesSI(){
		if(allFileTypesSI == null) {
			allFileTypesSI = new ArrayList<SelectItem>();
			allFileTypesSI.add(new SelectItem(new Long(0),"All"));
			allFileTypesSI.addAll(WriterWrapper.translateSI((ArrayList<SelectItem>)ApplicationData.applicationSIHm.get("fileTypesSI")));
		}
		return allFileTypesSI;
	}

	public String getRejectionDateTxt() {
		return CommonUtil.getDateTimeFormatDisplay(file.getRejectionDate(), CommonUtil.getUtcOffset(file.getUtcOffsetCreated())).replace("&nbsp;", " ");
	}
	
	public String getControlRejectionDateTxt() {
		return CommonUtil.getDateTimeFormatDisplay(file.getTimeControlRejected(), CommonUtil.getUtcOffset(file.getUtcOffsetCreated())).replace("&nbsp;", " ");
	}

	public String getTimeCreatedTxt() {
		return CommonUtil.getDateTimeFormatDisplay(file.getTimeCreated(), CommonUtil.getUtcOffset(file.getUtcOffsetCreated()));
	}

	/**
	 * @return the isPrevApproved
	 */
	public boolean isPrevApproved() {
		return isPrevApproved;
	}

	/**
	 * @param isPrevApproved the isPrevApproved to set
	 */
	public void setPrevApproved(boolean isPrevApproved) {
		this.isPrevApproved = isPrevApproved;
	}

	/**
	 * @return the isPrevControlApproved
	 */
	public boolean isPrevControlApproved() {
		return isPrevControlApproved;
	}

	/**
	 * @param isPrevControlApproved the isPrevControlApproved to set
	 */
	public void setPrevControlApproved(boolean isPrevControlApproved) {
		this.isPrevControlApproved = isPrevControlApproved;
	}

	public int getRejectReason() {
		return rejectReason;
	}

	public void setRejectReason(int rejectReason) {
		this.rejectReason = rejectReason;
	}
	
	public boolean getShowSendMailScreen() {
		return showSendMailScreen;
	}

	public void setShowSendMailScreen(boolean showSendMailScreen) {
		this.showSendMailScreen = showSendMailScreen;
	}
	
	public boolean getshowSuccessMail() {
		return showSuccessMail;
	}

	public void setShowSuccessMail(boolean showSuccessMail) {
		this.showSuccessMail = showSuccessMail;
	}
	
	public void sendMailScreen(){
		setShowSendMailScreen(true);
		setShowSuccessMail(false);
	}
	
	/**
	 * This method returns a list of missing, approved or rejected users documents as String
	 * @return String
	 */	
	public HashMap<Integer, String> getDocsforUpdateMail(){
		HashMap<Integer, String> res = new HashMap<Integer, String>();
		StringBuilder sb = new StringBuilder("");
		StringBuilder comm = new StringBuilder(""); 
		String ls = "<br/>";
		FacesContext context = FacesContext.getCurrentInstance();
		User user = (User)context.getApplication().createValueBinding(Constants.BIND_USER).getValue(context);
		Locale locale = new Locale(ApplicationDataBase.getLanguage(ApplicationDataBase.getSkinById(user.getSkinId()).getDefaultLanguageId()).getCode());
		String rejected = CommonUtil.getMessage(locale, "file.rejected", null);
		String approved = CommonUtil.getMessage(locale, "files.is.approved", null);
		String missing = CommonUtil.getMessage(locale, "file.missing", null);
		
		Locale localeEn = new Locale(ApplicationDataBase.getLanguage(ApplicationDataBase.getSkinById(Skins.SKIN_REG_EN).getDefaultLanguageId()).getCode());
		String rejectedEn = " " + CommonUtil.getMessage(localeEn, "file.rejected", null);
		String approvedEn = " " + CommonUtil.getMessage(localeEn, "files.is.approved", null);
		String missingEn = " " + CommonUtil.getMessage(localeEn, "file.missing", null);
		String newLine = " \r\n ";
		comm.append("WriterId:").append(Utils.getWriter().getWriter().getUserName() + newLine);		
				
		list = getList();
		for (File file : list) {
			if (file.getIncludeInUpdateStatus()) {
				HashMap<String, String> reasonAndType = FilesManagerBase.getReasonAndTypeByFileId(file.getId());
				if (file.getFileName() != null) {
					if (file.isSupportReject() || file.isControlReject()) {
						if(file.getFileTypeId() == FileType.CC_COPY || file.getFileTypeId() == FileType.CC_COPY_BACK 
								|| file.getFileTypeId() == FileType.CC_COPY_FRONT) {
							String ccLast4 = null;
							if(file.getCcId() > 0) {
								try {
									ccLast4 = CreditCardsManagerBase.getById(file.getCcId()).getCcNumberLast4();
									if(ccLast4 == null) {
										return null;
									}
									sb.append("<ul><li><b>").append(CommonUtil.getMessage(locale, reasonAndType.get("type"), null)).append(":").append("</b>").append(ls).
										append(rejected).append(" - ").append(CommonUtil.getMessage(locale, reasonAndType.get("reason"), null)).append(ls).
										append(ccLast4).append("</li></ul>");
								} catch (SQLException e) {
									log.error("No credit card was found! ", e);
									return null;
								}
							}
						} else { 
							sb.append("<ul><li><b>").append(CommonUtil.getMessage(locale, reasonAndType.get("type"), null)).append(":").append("</b>").append(ls).
								append(rejected).append(" - ").append(CommonUtil.getMessage(locale, reasonAndType.get("reason"), null)).append(ls).
								append(String.valueOf(file.getId())).append("</li></ul>");
						}
						comm.append("FileId:").append(file.getId()).append(" FileType: ").append(CommonUtil.getMessage(localeEn, reasonAndType.get("type"), null) + " ").
							append(rejectedEn).append(" - ").append(CommonUtil.getMessage(localeEn, reasonAndType.get("reason"), null) + newLine);
					
					} else if (file.isApproved() || file.isControlApproved()) {
							if(file.getFileTypeId() == FileType.CC_COPY || file.getFileTypeId() == FileType.CC_COPY_BACK 
								|| file.getFileTypeId() == FileType.CC_COPY_FRONT) {
							String ccLast4 = null;
							if(file.getCcId() > 0) {
								try {
									ccLast4 = CreditCardsManagerBase.getById(file.getCcId()).getCcNumberLast4();
									if(ccLast4 == null) {
										return null;
									}
									sb.append("<ul><li><b>").append(CommonUtil.getMessage(locale, reasonAndType.get("type"), null)).append(":").append("</b>").append(ls).
										append(approved).append(ls).
										append(ccLast4).append("</li></ul>");
								} catch (SQLException e) {
									log.error("No credit card was found! ", e);
									return null;
								}
							}
						} else {
							sb.append("<ul><li><b>").append(CommonUtil.getMessage(locale, reasonAndType.get("type"), null)).append(":").append("</b>").append(ls).
								append(approved).append(ls).
								append(String.valueOf(file.getId())).append("</li></ul>");
						}
						comm.append("FileId:").append(file.getId()).append(" FileType: ").append(CommonUtil.getMessage(localeEn, reasonAndType.get("type"), null) + " ").
							append(approvedEn + newLine);
					}
				} else {
					sb.append("<ul><li><b>").append(CommonUtil.getMessage(locale, reasonAndType.get("type"), null) + " ").append(":").append("</b>").append(ls).
						append(missing).append("</li></ul>");
					
					comm.append("FileId:").append(file.getId()).append(" FileType: ").append(CommonUtil.getMessage(localeEn, reasonAndType.get("type"), null)).
						append(missingEn + newLine);
				}
			}
		}
		res.put(DOCS_FOR_UPDATE_MAIL_HTML_TYPE, sb.toString());
		res.put(DOCS_FOR_UPDATE_ISSUES_COMMENTS_TYPE, comm.toString());
		return res;
	}
	
	/**
	 * This method sends email for Documents update from Backend Files menu screen.
	 */
	public void sendUpdateMail(){
		HashMap<Integer, String> resDocs = getDocsforUpdateMail();
		String docsForUpdateMail = resDocs.get(DOCS_FOR_UPDATE_MAIL_HTML_TYPE);
		String docsForIssuesComm = resDocs.get(DOCS_FOR_UPDATE_ISSUES_COMMENTS_TYPE);
		
		FacesContext context = FacesContext.getCurrentInstance();
		User user = (User)context.getApplication().createValueBinding(Constants.BIND_USER).getValue(context);
		
		if (!CommonUtil.isParameterEmptyOrNull(docsForUpdateMail)) {
			try {
					UsersManagerBase.sendMailTemplateFile(ConstantsBase.TEMPLATE_DOCS_UPDATE, Utils.getWriter().getWriter().getId(), user, null, "", null, docsForUpdateMail);
					IssuesManagerBase.insertIssueForMail(user.getId(), IssuesManagerBase.ISSUE_SUBJECT_DOCS_UPDATE, true, docsForIssuesComm, 0);
					setShowSuccessMail(true);
					setShowSendMailScreen(false);
				} catch (Exception e) {
					log.error("Cannot send email for docs update (BE_automatic_mailer)! ", e);
			}
		} else {
			Utils.popupMessage("file.alert.no.document.selected");
			setShowSendMailScreen(true);
		}
	}

	public int getControlRejectReason() {
		return controlRejectReason;
	}

	public void setControlRejectReason(int controlRejectReason) {
		this.controlRejectReason = controlRejectReason;
	}
	
	public String getFileExpDate(Date expDate) {
		if(!CommonUtil.isParameterEmptyOrNull(expDate.toString())) {
			SimpleDateFormat sd = new SimpleDateFormat("dd/MM/yyyy");
			return sd.format(expDate);
		}
		return "";
	}
	
	public boolean documentHasExpired(Date date) {
		if (date != null) {
			return date.before(new Date());
		}
		return false;
	}
	
	public boolean ccHasExpired(Date date) {
		if (date != null) {
			Calendar cal = Calendar.getInstance();
			cal.setTime(date);
			Calendar calNow = Calendar.getInstance();
			if (cal.get(Calendar.YEAR) < calNow.get(Calendar.YEAR)) {
				return true;
			} else if (cal.get(Calendar.YEAR) == calNow.get(Calendar.YEAR)) {
				if (cal.get(Calendar.MONTH) < calNow.get(Calendar.MONTH)) {
					return true;
				}
			}
		}
		return false;
	}

	public Date stringToDate(String dateCC) {
		SimpleDateFormat formatter = new SimpleDateFormat("MM/yy");
		Date date = null;
		try {
			date = (Date) formatter.parse(dateCC);
		} catch (ParseException e) {
			log.error("problem composing birth date out of day, month and year.", e);
		}
		return date;
	}

	public boolean isCurrentFiles() {
		return isCurrentFiles;
	}

	public void setCurrentFiles(boolean isCurrentFiles) {
		this.isCurrentFiles = isCurrentFiles;
	}
}
