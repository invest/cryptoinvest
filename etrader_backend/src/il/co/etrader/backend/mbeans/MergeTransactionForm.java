package il.co.etrader.backend.mbeans;

import java.io.Serializable;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import com.anyoption.common.beans.Transaction;

import il.co.etrader.backend.bl_managers.TransactionsIssuesManager;
import il.co.etrader.backend.bl_vos.MergeTransaction;
import il.co.etrader.backend.util.Utils;
import il.co.etrader.util.CommonUtil;

public class MergeTransactionForm implements Serializable {

	private static final long serialVersionUID = 1891900454649211603L;

	private long firstTxId;
	private String mergeWithTxIdStr;
	private ArrayList<String> mergeWithTxIdList;
	private ArrayList<MergeTransaction> mergeTransactions;
	private boolean isFoundTransactions;
	
	public MergeTransactionForm(){
		init();
	}
	
	public String validate() throws SQLException {
		FacesMessage fm;
		FacesContext context = FacesContext.getCurrentInstance();
		
		if (!CommonUtil.isParameterEmptyOrNull(mergeWithTxIdStr) && firstTxId > 0) {
			String[] listOfTransactionsId = mergeWithTxIdStr.split(",");
			mergeWithTxIdList = new ArrayList<String>();
			for (int i = 0; i < listOfTransactionsId.length; i++) {
				String transactionId = listOfTransactionsId[i];
				
				if(!CommonUtil.isParameterEmptyOrNull(transactionId)) {
					mergeWithTxIdList.add(transactionId);
				}
			}
			
			Transaction firstTransaction = TransactionsIssuesManager.getMergeTransactions(firstTxId, true);
			
			if (null != firstTransaction) {			
				mergeTransactions = new ArrayList<MergeTransaction>();
				
				for(String transactionId : mergeWithTxIdList) {
					long trxId = Long.valueOf(transactionId).longValue();
					MergeTransaction mergeTransaction = TransactionsIssuesManager.getMergeTransactions(trxId, false);
					String error = "";
					if (null != mergeTransaction) {
						if (mergeTransaction.getUserId() == firstTransaction.getUserId() && mergeTransaction.getWriterId() == firstTransaction.getWriterId()) {
							mergeTransaction.setValidToMerge(true);
							mergeTransaction.setComment("OK");
						} else {							
							mergeTransaction.setValidToMerge(false);
							if (mergeTransaction.getUserId() != firstTransaction.getUserId()) {
								error = "User id is wrong,";
							}
							
							if (mergeTransaction.getWriterId() != firstTransaction.getWriterId()) {
								error = " Writer id is wrong,";
							}
							error = error.substring(0, error.length() - 1);//remove last ','
							mergeTransaction.setComment(error);
						}
					} else {
						mergeTransaction = new MergeTransaction();
						mergeTransaction.setId(trxId);
						mergeTransaction.setValidToMerge(false);
						mergeTransaction.setComment("Transaction wasn't found");
					}
					
					if (null != mergeTransaction) {
						mergeTransactions.add(mergeTransaction);
						if (mergeTransaction.isValidToMerge()) {
							isFoundTransactions = true;
						}
					}
				}
			} else {
				fm = new FacesMessage(FacesMessage.SEVERITY_INFO,
						CommonUtil.getMessage("menu.transaction.issue.merge.transactions.merge.error.tran.not.exist", null, Utils.getWriterLocale(context)),null);
				context.addMessage(null, fm);
				return null;
			}			
		} else {
			fm = new FacesMessage(FacesMessage.SEVERITY_INFO,
					CommonUtil.getMessage("menu.transaction.issue.merge.transactions.merge.error.empty.fields", null, Utils.getWriterLocale(context)),null);
			context.addMessage(null, fm);
			return null; //error message empty fields
		}		
		return null;
	}
	
	public String merge() throws SQLException {
		FacesMessage fm;
		FacesContext context = FacesContext.getCurrentInstance();
		String transactionsStr = "";
		
		for (MergeTransaction transaction : mergeTransactions) {
			if(transaction.isValidToMerge()) {
				transactionsStr += transaction.getId() + ",";
			}
		}
		
		transactionsStr = transactionsStr.substring(0, transactionsStr.length() - 1);
		
		TransactionsIssuesManager.mergeTransaction(firstTxId, transactionsStr, Utils.getWriter().getWriter().getId());
		fm = new FacesMessage(FacesMessage.SEVERITY_INFO,
				CommonUtil.getMessage("menu.transaction.issue.merge.transactions.merge.update.success", null, Utils.getWriterLocale(context)),null);
		context.addMessage(null, fm);
		init();
		return null;
	}
	
	public void init() {
		firstTxId = 0;
		mergeWithTxIdStr = "";
		isFoundTransactions = false;
	}

	/**
	 * @return the firstTxId
	 */
	public long getFirstTxId() {
		return firstTxId;
	}

	/**
	 * @param firstTxId the firstTxId to set
	 */
	public void setFirstTxId(long firstTxId) {
		this.firstTxId = firstTxId;
	}

	/**
	 * @return the mergeWithTxIdStr
	 */
	public String getMergeWithTxIdStr() {
		return mergeWithTxIdStr;
	}

	/**
	 * @param mergeWithTxIdStr the mergeWithTxIdStr to set
	 */
	public void setMergeWithTxIdStr(String mergeWithTxIdStr) {
		this.mergeWithTxIdStr = mergeWithTxIdStr;
	}

	/**
	 * @return the mergeWithTxIdList
	 */
	public ArrayList<String> getMergeWithTxIdList() {
		return mergeWithTxIdList;
	}

	/**
	 * @param mergeWithTxIdList the mergeWithTxIdList to set
	 */
	public void setMergeWithTxIdList(ArrayList<String> mergeWithTxIdList) {
		this.mergeWithTxIdList = mergeWithTxIdList;
	}

	/**
	 * @return the mergeTransactions
	 */
	public ArrayList<MergeTransaction> getMergeTransactions() {
		return mergeTransactions;
	}

	/**
	 * @param mergeTransactions the mergeTransactions to set
	 */
	public void setMergeTransactions(ArrayList<MergeTransaction> mergeTransactions) {
		this.mergeTransactions = mergeTransactions;
	}

	/**
	 * @return the isfoundTransactions
	 */
	public boolean isFoundTransactions() {
		return isFoundTransactions;
	}

	/**
	 * @param isfoundTransactions the isfoundTransactions to set
	 */
	public void setIsfoundTransactions(boolean isFoundTransactions) {
		this.isFoundTransactions = isFoundTransactions;
	}
	
}
