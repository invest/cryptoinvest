package il.co.etrader.backend.mbeans;

import java.io.Serializable;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;

import javax.faces.context.FacesContext;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.base.QuestionnaireGroup;
import com.anyoption.common.beans.base.QuestionnaireQuestion;
import com.anyoption.common.beans.base.QuestionnaireUserAnswer;
import com.anyoption.common.beans.base.UserRegulationBase;
import com.anyoption.common.managers.QuestionnaireManagerBase;
import com.anyoption.common.managers.UserRegulationManager;

import il.co.etrader.backend.bl_managers.ApplicationData;
import il.co.etrader.backend.bl_vos.WriterWrapper;
import il.co.etrader.backend.util.Constants;
import il.co.etrader.backend.util.Utils;
import il.co.etrader.bl_vos.UserBase;
import il.co.etrader.bl_vos.Writer;

public class RegCapQuestionnaireForm implements Serializable {

	/**
	 * pavelt
	 */
	private static final long serialVersionUID = -3523854248383366231L;
	
	private static final Logger log = Logger.getLogger(RegCapQuestionnaireForm.class);
	
	private QuestionnaireGroup questionnaire;
	private List<QuestionnaireUserAnswer> userAnswers;
	private UserBase user;
	private Writer writer;
	private UserRegulationBase userRegulation;
	private boolean submitted;
	private int knoledgeQuestionId;
	
	
	public RegCapQuestionnaireForm() throws SQLException {
		WriterWrapper ww = Utils.getWriter();
		writer = ww.getWriter();
		
		questionnaire = QuestionnaireManagerBase.getAllQuestionnaires().get(QuestionnaireGroup.REGULATION_CAPITAL_QUESTIONNAIRE_NAME);
		FacesContext context=FacesContext.getCurrentInstance();
		ApplicationData ap = (ApplicationData) context.getApplication().createValueBinding(Constants.BIND_APPLICATION_DATA).getValue(context);
		user = (UserBase)context.getApplication().createValueBinding(Constants.BIND_USER).getValue(context);
		userAnswers = QuestionnaireManagerBase.getUserAnswers(user.getId(), questionnaire.getQuestions(), writer.getId());	
		
	   	userRegulation = new UserRegulationBase();
	   	userRegulation.setUserId(user.getId());
	   	UserRegulationManager.getUserRegulation(userRegulation);
	   	if(userRegulation.getApprovedRegulationStep() == userRegulation.ET_CAPITAL_MARKET_QUESTIONNAIRE || userRegulation.getApprovedRegulationStep() == userRegulation.ET_REGULATION_DONE){
			setSubmitted(true);
		}
	}
	
	public List<QuestionnaireUserAnswer> getUserAnswers() {
		return userAnswers;
	}


	public void setUserAnswers(List<QuestionnaireUserAnswer> userAnswers) {
		this.userAnswers = userAnswers;
	}
	
	public List<QuestionnaireQuestion> getQuestions() {
		return questionnaire.getQuestions();
	}
	
	public String insertQuestionnaire() {
		
		log.info("Inserting questionnaire [" + questionnaire.getId() + ":" + questionnaire.getName() + "]  for user [" + user.getId() + "]");
		try {
			if (userRegulation.getApprovedRegulationStep() == UserRegulationBase.ET_REGULATION_KNOWLEDGE_QUESTION_USER && !userRegulation.isSuspended()) {
				for (QuestionnaireUserAnswer userAnswer : userAnswers) {
					userAnswer.setWriterId(writer.getId());
					userAnswer.setScore(QuestionnaireManagerBase.getAnswertScore(questionnaire, userAnswer));
				}
				
				UserRegulationManager.updateRegulationStepKnowledgeQuestion(userRegulation);
				QuestionnaireManagerBase.updateUserAnswers(userAnswers, true);

				if (canSubmit()) {
					long scoreGroup = QuestionnaireManagerBase.updateUserAnswersScoreGroup(user.getId());

					userRegulation.setApprovedRegulationStep(UserRegulationBase.ET_CAPITAL_MARKET_QUESTIONNAIRE);
					userRegulation.setWriterId(writer.getId());
					UserRegulationManager.updateRegulationStep(userRegulation);

					if (scoreGroup == UserRegulationBase.ET_REGULATION_HIGH_SCORE_GROUP_ID) {
						userRegulation.setApprovedRegulationStep(UserRegulationBase.ET_REGULATION_DONE);
						userRegulation.setWriterId(writer.getId());
						UserRegulationManager.updateRegulationStep(userRegulation);
					}
					if (scoreGroup == UserRegulationBase.ET_REGULATION_LOW_SCORE_GROUP_ID) {
	    				if(il.co.etrader.bl_managers.UsersManagerBase.sendActivationMailForSuspend(user.getId(), false)){
	    					log.error("Send Activation Mail For Suspend userId:" + user.getId());
	    				} else {
	    					log.error("Can't send Activation Mail For Suspend userId:" + user.getId());
	    				}
					}
				}
			}
		} catch(Exception ex) {
			log.error("Can not insert backend ET questionnaire :", ex);
			return null;
		}
		return Constants.NAV_ET_QUESTIONNAIRE;
	}
	
	public int getKnoledgeQuestionId() {
		knoledgeQuestionId = 0;
		if (userRegulation != null) {
			if (userRegulation.isKnowledgeQuestion()) {
				knoledgeQuestionId = 1;
			}
		}
		return knoledgeQuestionId;
	}

	public boolean isSubmitted() {
		return submitted;
	}

	public void setSubmitted(boolean submitted) {
		this.submitted = submitted;
	}
	
	
	public boolean canSubmit() {
		try {
			return QuestionnaireManagerBase.isAllCapitalAnswFilled(user.getId());
		} catch (SQLException e) {
			log.error("Can't get capital answer filled ", e);
			return false;
		}
	}

	public void setKnoledgeQuestionId(int knoledgeQuestionId) {
		if (knoledgeQuestionId == 0) {
			userRegulation.setKnowledgeQuestion(false);
		} else if (knoledgeQuestionId == 1) {
			userRegulation.setKnowledgeQuestion(true);
		}
		this.knoledgeQuestionId = knoledgeQuestionId;
	}
	
	public String getScoreGroupTxt() {
		String scoreGroupTxt = "";
		if (userRegulation.getScoreGroup() == UserRegulationBase.ET_REGULATION_HIGH_SCORE_GROUP_ID) {
			scoreGroupTxt = "HIGH";
		} else if (userRegulation.getScoreGroup() == UserRegulationBase.ET_REGULATION_MEDIUM_SCORE_GROUP_ID) {
			scoreGroupTxt = "MEDIUM";
		} else if (userRegulation.getScoreGroup() == UserRegulationBase.ET_REGULATION_LOW_SCORE_GROUP_ID) {
			scoreGroupTxt = "LOW";
		}
		return scoreGroupTxt;
	}
	
	public String questionnaireCompleteMessage() {
		String message = "";
		Date dateCreated = null;
		try {
			dateCreated = UserRegulationManager.getTimeWhenQuestionnaireFinallySubmited(user.getId());
		} catch (SQLException e) {
			log.error("Can not get time created for user with id: " + user.getId() + "", e);
			return message;
		}
		
		
		message = "Questionnaire was completed on " + dateCreated
													+ ". Group: " + getScoreGroupTxt() 
													+ ". Score: "+ userRegulation.getScore()+"";
	
		return message;
	}
}
