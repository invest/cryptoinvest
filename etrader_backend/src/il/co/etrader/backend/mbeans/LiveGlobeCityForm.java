package il.co.etrader.backend.mbeans;

import java.io.Serializable;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import com.anyoption.common.beans.Country;
import com.anyoption.common.beans.LiveGlobeCity;

import il.co.etrader.backend.bl_managers.LiveGlobeCityManager;
import il.co.etrader.backend.util.Constants;
import il.co.etrader.bl_managers.ApplicationDataBase;
import il.co.etrader.util.CommonUtil;
import il.co.etrader.util.ConstantsBase;

public class LiveGlobeCityForm implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 211787845418700189L;
	private ArrayList<LiveGlobeCity> list = new ArrayList<LiveGlobeCity>();
	private LiveGlobeCity liveGlobeCity;
	private String countryName;
	private Country country;
	private String currencyName;
	
	public LiveGlobeCityForm() throws SQLException{
		Search();
	}
	
	public String Search() throws SQLException {
		setList(LiveGlobeCityManager.getAllCities());
		CommonUtil.setTablesToFirstPage();
		return null;
	}
	
	public String edit(){
		return Constants.NAV_LIVE_GLOBE_CITY;
	}
	
	public String initValues() throws SQLException {
		liveGlobeCity = new LiveGlobeCity();
		liveGlobeCity.setId(0);
		liveGlobeCity.setCityName("");		
		liveGlobeCity.setGlobeLatitude("0");
		liveGlobeCity.setGlobeLongtitude("0");
		liveGlobeCity.setCountryId(1);
		liveGlobeCity.setCurrencyId(2);

		return Constants.NAV_LIVE_GLOBE_CITY;
	}
	
	public String updateInsert() throws SQLException {
		FacesContext context = FacesContext.getCurrentInstance();
		if(!liveGlobeCity.getCityName().matches("[a-zA-Z\\s]+")){
			FacesMessage fm = new FacesMessage(
					FacesMessage.SEVERITY_ERROR,
					CommonUtil.getMessage(
							"error.live.city.only.letters", null),
					null);
			context.addMessage(null, fm);
			Search();
			return null;
		}
		boolean res = LiveGlobeCityManager.updateInsertLiveGlobeCity(liveGlobeCity);
		Search();
		return Constants.NAV_LIVE_GLOBE_CITIES;	
	}	

	/**
	 * @return the list
	 */
	public ArrayList<LiveGlobeCity> getList() {
		return list;
	}

	/**
	 * @param list the list to set
	 */
	public void setList(ArrayList<LiveGlobeCity> list) {
		this.list = list;
	}

	/**
	 * @return the liveGlobeCity
	 */
	public LiveGlobeCity getLiveGlobeCity() {
		return liveGlobeCity;
	}

	/**
	 * @param liveGlobeCity the liveGlobeCity to set
	 */
	public void setLiveGlobeCity(LiveGlobeCity liveGlobeCity) {
		this.liveGlobeCity = liveGlobeCity;
	}	

	public int getListSize() {
		return list.size();
	}

	/**
	 * @return the countryName
	 */
	public String getCountryName(long countryId ) {
		FacesContext context = FacesContext.getCurrentInstance();
		ApplicationDataBase ap = (ApplicationDataBase)context.getApplication().createValueBinding(ConstantsBase.BIND_APPLICATION_DATA).getValue(context);
		country = ap.getCountry(countryId);
		return country.getName();
	}

	/**
	 * @param countryName the countryName to set
	 */
	public void setCountryName(String countryName) {
		this.countryName = countryName;
	}

	/**
	 * @return the currencyName
	 */
	public String getCurrencyName(long currencyId) {
		FacesContext context = FacesContext.getCurrentInstance();
		ApplicationDataBase ap = (ApplicationDataBase)context.getApplication().createValueBinding(ConstantsBase.BIND_APPLICATION_DATA).getValue(context);
		return  ap.getCurrencyById(currencyId).getNameKey();
		
	}

	/**
	 * @param currencyName the currencyName to set
	 */
	public void setCurrencyName(String currencyName) {
		this.currencyName = currencyName;
	}
}
