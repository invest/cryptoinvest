package il.co.etrader.backend.mbeans;

import java.io.Serializable;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.faces.context.FacesContext;

import il.co.etrader.backend.bl_managers.UsersManager;
import il.co.etrader.backend.bl_vos.User;
import il.co.etrader.backend.util.Utils;
import il.co.etrader.util.CommonUtil;


public class LockedAccountsForm implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2484371710361649454L;
	private ArrayList accountsList;
	private User user;

	public LockedAccountsForm() throws SQLException{
		user = new User();
		updateAccountsList();
	}

	public int getListSize() {
		return accountsList.size();
	}

	public ArrayList getAccountssList() {
		return accountsList;
	}

	public void setAccountsList(ArrayList accountsList) {
		this.accountsList = accountsList;
	}

	/**
	 * update locked accounts
	 * @return
	 * @throws SQLException
	 */
	public String updateAccountsList() throws SQLException{
		accountsList = UsersManager.getLockedAccounts();
		CommonUtil.setTablesToFirstPage();
		return null;
	}

	/**
	 * unlock user by user id
	 * @return
	 * @throws SQLException
	 */
	public String unlockUser() throws SQLException {

		FacesContext context = FacesContext.getCurrentInstance();
		UsersManager.unlockUser(user.getId(), true, Utils.getWriter().getWriter().getId());
		updateAccountsList();
		context.renderResponse();
		return null;
	}

	/**
	 * @return the user
	 */
	public User getUser() {
		return user;
	}

	/**
	 * @param user the user to set
	 */
	public void setUser(User user) {
		this.user = user;
	}

	/**
	 * @return the accountsList
	 */
	public ArrayList getAccountsList() {
		return accountsList;
	}



}
