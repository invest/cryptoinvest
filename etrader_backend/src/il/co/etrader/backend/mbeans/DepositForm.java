package il.co.etrader.backend.mbeans;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Iterator;

import javax.el.ValueExpression;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.CreditCard;
import com.anyoption.common.beans.Transaction;
import com.anyoption.common.beans.base.Skin;
import com.anyoption.common.bl_vos.ClearingLimitaion;
import com.anyoption.common.clearing.ClearingProvider;
import com.anyoption.common.managers.RewardUserTasksManager;
import com.anyoption.common.managers.RewardUserTasksManager.TaskGroupType;

import il.co.etrader.backend.bl_managers.AdminManager;
import il.co.etrader.backend.bl_managers.TransactionsManager;
import il.co.etrader.backend.bl_managers.UsersManager;
import il.co.etrader.backend.bl_vos.User;
import il.co.etrader.backend.bl_vos.WriterWrapper;
import il.co.etrader.backend.util.Constants;
import il.co.etrader.backend.util.Utils;
import il.co.etrader.bl_managers.BonusManagerBase;
import il.co.etrader.bl_managers.ClearingManager;
import il.co.etrader.bl_managers.TiersManagerBase;
import il.co.etrader.bl_managers.TransactionsManagerBase;
import il.co.etrader.bl_vos.Skins;
import il.co.etrader.bl_vos.UserBase;
import il.co.etrader.util.CommonUtil;
import il.co.etrader.util.ConstantsBase;
import il.co.etrader.util.TransactionFormater;

public class DepositForm extends DepositFormBase implements Serializable{
	private static final long serialVersionUID = 1L;

	private static final Logger logger = Logger.getLogger(DepositForm.class);

	private String cardId;
	private String deposit;
	private String comments;
	private String rootingComment;
	private long providerId = -1;
	private boolean sendReceipt;
	private long depositPoints;
	private Long wagering;

	private Transaction transaction;
	private ArrayList<Transaction> transactionsList;
	private Long transactionId;
	private String paypalTransactionId;
	private String paypalEmail;

	private String authorizationCode; // for phoneDeposit
	private boolean isCheckMinAmount;
	private String holderName; 
	
	ArrayList<SelectItem> clearingProviderRoutes = null; 
	ArrayList<SelectItem> list;
	
	private String ccPass;
	
	public DepositForm() throws SQLException {
		super();
		comments = "";
		cardId = "";
		sendReceipt = true;
		paypalTransactionId = "";
		isCheckMinAmount = true;

		User user = Utils.getUser();
		if (user.isInTier()) {
			depositPoints = user.getTierUser().getPoints();
		}

		list = TransactionsManager.getDisplayCreditCardsByUser(user.getId());

		for(Iterator<SelectItem> i = list.iterator();i.hasNext();){
			
			SelectItem si = i.next();

			if(si.getLabel().contains("Diners") || ((si.getLabel().contains("American Express")) && !(user.getSkinId() == Skins.SKIN_ETRADER))){
	    		i.remove(); 		
	    	}
			
		}
	}

	public ArrayList<SelectItem> getCcList() throws SQLException{
		 return list;
	}

	public boolean isFirstCard() throws SQLException{

		return getCcList().size()==0;
	}

	public void creditCardSelected(ValueChangeEvent ev) {
		providerId = -1;
		Long ccNumber = Long.parseLong((String)ev.getNewValue());
		try{
			CreditCard cc = TransactionsManagerBase.getCreditCard(new BigDecimal(ccNumber));
			User user = Utils.getUser();
			holderName = cc.getHolderName();
			clearingProviderRoutes = loadNonRestrictedProvidersSI(user, cc.getBin(), cc.getTypeId());
			// partial render
			FacesContext.getCurrentInstance().getPartialViewContext().getRenderIds().add("depositForm:providerType");
			FacesContext.getCurrentInstance().getPartialViewContext().getRenderIds().add("depositForm:holderName");
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public static ArrayList<SelectItem> loadNonRestrictedProvidersSI(User user, long ccBinId, long ccTypeId) throws SQLException{
		Hashtable<Long, ClearingProvider> allProvidersByBusinessCase = ClearingManager.getClearingProvidersByPaymentGroupAndBusinessCase().get(user.getSkin().getBusinessCaseId());
		ArrayList<ClearingLimitaion> clearingLimitaionList = TransactionsManagerBase.getClearingLimitaionList();
		
		ArrayList<SelectItem> cProviders = new ArrayList<SelectItem>();
		cProviders.add(new SelectItem(-1, ""));
		Iterator<ClearingProvider> it = allProvidersByBusinessCase.values().iterator();
		
		while(it.hasNext()) {
			ClearingProvider cp = it.next();
			long providerGroupId = ClearingManager.getProviderGroupIdByProviderId(cp.getId());
			if( !isRestricted(clearingLimitaionList, providerGroupId, ccBinId, user.getCountryId(), user.getCurrencyId(), ccTypeId)) {
				cProviders.add(new SelectItem(new Long(cp.getId()), cp.getName()));
			} 
		}
		return cProviders;
	}
	
	private static boolean isRestricted(ArrayList<ClearingLimitaion> clearingLimitaionList, long providerGroupId, long binId, long countryId, long currencyId, long ccTypeId ) {
		for(ClearingLimitaion cl: clearingLimitaionList) {
			if(cl.getClearingProviderIdGroup() == providerGroupId) {
				if( cl.getBinId() == binId ||
					cl.getCountryId() == countryId ||
					cl.getCurrencyId() == currencyId || 
					cl.getCreditCardTypeId() == ccTypeId ) {
					return true;
				}
			}
		}
		return false;
	}

	public String insertDeposit() throws SQLException {
		// prevent identical requests from being processed
        if (this.saveTokenIsInvalid() ) {
            return this.logInvalidSaveAndReturn(logger, null);
        }
        this.generateSaveToken();
        FacesContext context = FacesContext.getCurrentInstance();

		User user = Utils.getUser();
		UserBase u = TransactionsManager.getById(user.getId());
		Transaction t = null;
		String footer = "";
		
		String msg = TransactionsManager.checkUserStatus(user);
		if(!CommonUtil.isParameterEmptyOrNull(msg)){
			FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_INFO,CommonUtil.getMessage(msg, null, user.getLocale()),null);
	    	context.addMessage(null, fm);
			return null;
		}
		
		updateUserFields(user);
		
		t = TransactionsManager.insertDeposit(deposit,cardId,AdminManager.getWriterId(),u,Constants.BACKEND_CONST,null, false, isCheckMinAmount, this.rootingComment, this.providerId, ccPass, 0L);

		reloadIsFirstDeposit();
		
		if (user.isSupportSelected() || user.isRetentionSelected() || user.isRetentionMSelected()) {
			UsersManager.loadUserStripFields(user, true, null);
			logger.debug("updating user session details.");
		}

		if ( t != null ) {
			
			if(providerId!=-1) {
				sendEmail(t, u, providerId, rootingComment);
			}
			
			if (sendReceipt && t != null) {
				String[] params=new String[1];
				params[0]=String.valueOf(t.getCc4digit());
				String receiptNum = t.getReceiptNumTxt();
				if ( user.getSkinId() != Skin.SKIN_ETRADER ) {  // for ao take transaction id
					receiptNum = String.valueOf(t.getId());
				}

				footer=CommonUtil.getMessage("receipt.footer.creditcard", params, user.getLocale());
				if(t.getClearingProviderId() == 26 || t.getClearingProviderId() == 27 || t.getClearingProviderId() == 28) //AMEX
				{
				    footer=footer + System.getProperty("line.separator") + CommonUtil.getMessage("receipt.bank_descriptor", params, user.getLocale());
				}
				
				UsersManager.sendReceiptEmail( CommonUtil.displayAmount(t.getAmount(), t.getCurrency().getId()),t.getTimeCreatedTxt(),
						footer,receiptNum);
			}

			return Utils.navAfterUserOperation();
		}
		else {
			return null;
		}
	}

	private void sendEmail(Transaction t, UserBase u, long provider, String comment) {
	       // send email
        Hashtable<String, String> serverProperties = new Hashtable<String, String>();
		serverProperties.put("url", CommonUtil.getProperty("email.server"));
		serverProperties.put("auth", "true");
		serverProperties.put("user", CommonUtil.getProperty("email.uname"));
		serverProperties.put("pass", CommonUtil.getProperty("email.pass"));
		serverProperties.put("contenttype", "text/html; charset=UTF-8");

        Hashtable<String, String> email = new Hashtable<String, String>();
        email.put("subject", "Deposit was manually sent to a specific provider");
        FacesContext ctx = FacesContext.getCurrentInstance();
        
        String annaemail = ctx.getExternalContext().getInitParameter("anna.email");
        email.put("to", annaemail);

        String htmlBuffer = 
        		"<html>" +
        				"<ul>" +
			    			"<li>Transaction ID: "+t.getId()+" </li>" +
			    			"<li>User ID: "+t.getUserId()+" </li>" +
			    			"<li>Username: "+u.getUserName()+" </li>" +
			    			"<li>Amount: "+t.getAmount()+TransactionFormater.getCurrencyTxt(t)+"</li>" +
			    			"<li>ProviderId: "+providerId+"</li>" +
			    			"<li>Time created:"+t.getTimeCreatedTxt()+"</li>" +
			    			"<li>Time settled:"+TransactionFormater.getTimeSettledTxt(t)+" </li>" +
			    			"<li>Status:"+t.getStatusId()+" </li>" +
			    			"<li>Skin:"+u.getSkin().getName()+" </li>" +
			    			"<li>Writer:"+Utils.getWriter().getWriter().getUserName()+" </li>" +
			    			"<li>Comment:"+comment+" </li>" +
		    			"</ul>" +
        		"</html>";

        String senderEmailAddress = "";
        email.put("from", senderEmailAddress);
        email.put("body", htmlBuffer);

		CommonUtil.sendEmail(serverProperties, email, null);
		
	}

	public String insertAdminDeposit() throws SQLException{
		// 	prevent identical requests from being processed
		FacesContext context = FacesContext.getCurrentInstance();
        if (this.saveTokenIsInvalid() ) {
            return this.logInvalidSaveAndReturn(logger, null);
        }
        this.generateSaveToken();

		User user = Utils.getUser();

		int transTypeId = TransactionsManagerBase.TRANS_TYPE_ADMIN_DEPOSIT;
		
		updateUserFields(user);
		
		String msg = TransactionsManager.checkUserStatus(user);
		if(!CommonUtil.isParameterEmptyOrNull(msg)){
			FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_INFO,CommonUtil.getMessage(msg, null, user.getLocale()),null);
	    	context.addMessage(null, fm);
			return null;
		}

		boolean res = TransactionsManager.insertAdminDeposit(null,this,user.getId(),user.getUserName(), transTypeId);

		// Reward plan
		try {
			if (res && isFirstDeposit()) {
				RewardUserTasksManager.rewardTasksHandler(TaskGroupType.FIRST_TIME_DEPOSIT_AMOUNT, user.getId(),  
						CommonUtil.calcAmount(deposit), transactionId, BonusManagerBase.class, (int) Utils.getWriter().getWriter().getId());
			}
		} catch (Exception e) {
            logger.error(ConstantsBase.REWARD_PLAN_LOG_PREFIX + " ERROR RewardUserTasksManager exception after success transaction", e);
        }
		
		reloadIsFirstDeposit();

		UsersManager.loadUserStripFields(user, true, null);

		if (res)
			return Utils.navAfterUserOperation();
		else
			return null;
	}

	/**
	 * Insert bonus deposit with wagering
	 * @return
	 * @throws SQLException
	 */
	public String insertBonusDeposit() {
		FacesContext context = FacesContext.getCurrentInstance();
		boolean adminOrSadmin=false;
		WriterWrapper ww = Utils.getWriter();
		long writerId = ww.getWriter().getId();
		
		// 	prevent identical requests from being processed
        if (this.saveTokenIsInvalid() ) {
            return this.logInvalidSaveAndReturn(logger, null);
        }
        this.generateSaveToken();

        User user = Utils.getUser();
        
		long wageringVal = 0;
		if (null != wagering) {
			wageringVal = wagering.longValue();
		}

		if (	user.isHasAdminRole() || user.isHasSAdminRole() || user.isHasSalesMExt()){
			
			adminOrSadmin = true;
		}
		boolean res = false;
		try {
			updateUserFields(user);
			
			String msg = TransactionsManager.checkUserStatus(user);
			if(!CommonUtil.isParameterEmptyOrNull(msg)){
				FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_INFO,CommonUtil.getMessage(msg, null, user.getLocale()),null);
		    	context.addMessage(null, fm);
				return null;
			}
			
			res = TransactionsManager.insertBonusDeposit(user, 0, AdminManager.getWriterId(),
					Utils.calcAmount(deposit),comments , wageringVal, adminOrSadmin);
	
			reloadIsFirstDeposit();
	
			UsersManager.loadUserStripFields(user, true, null);
			res = true;
		} catch ( SQLException e) {
			res = false;
			logger.error("Error while inserting bonus deposit", e);
		}

		if (res) {
			FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_INFO, "Deposit successful",	null);
			context.addMessage(null, fm);
			return Utils.navAfterUserOperation();
		}
		else {
			FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Deposit unsuccessful",	null);
			context.addMessage(null, fm);
			return null;
		}
	}

	public String insertCashDeposit() throws SQLException {
		// 	prevent identical requests from being processed
		FacesContext context = FacesContext.getCurrentInstance();
        if (this.saveTokenIsInvalid() ) {
            return this.logInvalidSaveAndReturn(logger, null);
        }
        this.generateSaveToken();

        User user = Utils.getUser();
        
        updateUserFields(user);
        
		String msg = TransactionsManager.checkUserStatus(user);
		if(!CommonUtil.isParameterEmptyOrNull(msg)){
			FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_INFO,CommonUtil.getMessage(msg, null, user.getLocale()),null);
	    	context.addMessage(null, fm);
			return null;
		}
		
		boolean res = TransactionsManager.insertCashDeposit(deposit,user.getId());
		
		reloadIsFirstDeposit();

		UsersManager.loadUserStripFields(user, true, null);

		if (res) {
			return Utils.navAfterUserOperation();
		} else {
			return null;
		}
	}


	/**
	 * Insert Deposit from phone
	 * This deposit skip the Authorization stage, just do the Capture
	 * @return
	 * @throws SQLException
	 */
	public String insertPhoneDeposit() throws SQLException {
		FacesContext context = FacesContext.getCurrentInstance();
		// prevent identical requests from being processed
        if (this.saveTokenIsInvalid() ) {
            return this.logInvalidSaveAndReturn(logger, null);
        }

        this.generateSaveToken();

        User user = Utils.getUser();
        
		UserBase u = TransactionsManager.getById(user.getId());
		
		updateUserFields(user);
		
		String msg = TransactionsManager.checkUserStatus(user);
		if(!CommonUtil.isParameterEmptyOrNull(msg)){
			FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_INFO,CommonUtil.getMessage(msg, null, user.getLocale()),null);
	    	context.addMessage(null, fm);
			return null;
		}
		
		Transaction t = TransactionsManager.insertDeposit(deposit,cardId,AdminManager.getWriterId(),u,Constants.BACKEND_CONST,authorizationCode, false, true, null, -1, ccPass, 0L);

		reloadIsFirstDeposit();
		
		if (user.isSupportSelected() || user.isRetentionSelected() || user.isRetentionMSelected()) {
			UsersManager.loadUserStripFields(user, true, null);
			logger.debug("updating user session details.");
		}

		if ( t != null ) {

			if ( sendReceipt && t != null ) {
				String[] params = new String[1];
				params[0] = String.valueOf(t.getCc4digit());
				String receiptNum = t.getReceiptNumTxt();
				if ( user.getSkinId() != Skin.SKIN_ETRADER ) {  // for ao take transaction id
					receiptNum = String.valueOf(t.getId());
				}
				UsersManager.sendReceiptEmail(TransactionFormater.getAmountTxt(t),t.getTimeCreatedTxt(),
						CommonUtil.getMessage("receipt.footer.creditcard", params, user.getLocale()),receiptNum);
			}

			return Utils.navAfterUserOperation();
		}
		else {
			return null;
		}
	}



	public String goDeposit() throws SQLException {
		if (getCcList().size()>0)
			return Constants.NAV_OTHER_DEPOSIT;
		else
			return Constants.NAV_FIRST_DEPOSIT;
	}

	/**
	 * @return the cardId
	 */
	public String getCardId() {
		return cardId;
	}

	/**
	 * @param cardId the cardId to set
	 */
	public void setCardId(String cardId) {
		this.cardId = cardId;
	}

	/**
	 * @return the deposit
	 */
	public String getDeposit() {
		return deposit;
	}

	/**
	 * @param deposit the deposit to set
	 */
	public void setDeposit(String deposit) {
		this.deposit = deposit;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public boolean isSendReceipt() {
		return sendReceipt;
	}

	public void setSendReceipt(boolean sendReceipt) {
		this.sendReceipt = sendReceipt;
	}

	/**
	 * @return the authorizationCode
	 */
	public String getAuthorizationCode() {
		return authorizationCode;
	}

	/**
	 * @param authorizationCode the authorizationCode to set
	 */
	public void setAuthorizationCode(String authorizationCode) {
		this.authorizationCode = authorizationCode;
	}

	/**
	 * @return the depositPoints
	 */
	public long getDepositPoints() {
		return depositPoints;
	}

	/**
	 * @param depositPoints the depositPoints to set
	 */
	public void setDepositPoints(long depositPoints) {
		this.depositPoints = depositPoints;
	}

	/**
	 * @return the wagering
	 */
	public Long getWagering() {
		return wagering;
	}

	/**
	 * @param wagering the wagering to set
	 */
	public void setWagering(Long wagering) {
		this.wagering = wagering;
	}

	/**
	 * Constructs a <code>String</code> with all attributes
	 * in name = value format.
	 *
	 * @return a <code>String</code> representation
	 * of this object.
	 */
	public String toString()
	{
	    final String TAB = " \n ";

	    String retValue = "";

	    retValue = "DepositForm ( "
	        + super.toString() + TAB
	        + "cardId = " + this.cardId + TAB
	        + "deposit = " + this.deposit + TAB
	        + "comments = " + this.comments + TAB
	        + "sendReceipt = " + this.sendReceipt + TAB
	        + " )";

	    return retValue;
	}


	/**
	 * Points to cash money convert action
	 * @return
	 * @throws Exception
	 */
	public String convertPointsToCashDeposit() throws Exception {
		FacesContext context = FacesContext.getCurrentInstance();
		// prevent identical requests from being processed
        if (this.saveTokenIsInvalid() ) {
            return this.logInvalidSaveAndReturn(logger, null);
        }

        this.generateSaveToken();
        User user = Utils.getUser();
        WriterWrapper w = Utils.getWriter();
        String error = TransactionsManagerBase.validatePointsToCashAction("pointsToCashForm", depositPoints, user);
        if (null != error) {
        	return null;
        } else {
        	boolean res = TransactionsManager.insertPointsToCashDeposit(depositPoints, user, w.getWriter().getId(), 0L);
        	if (res) {
        		UsersManager.loadUserStripFields(user, true, null);
		    	String params[] = new String[2];
		    	params[0] = String.valueOf(depositPoints);
		    	double amount = TiersManagerBase.convertPointsToCash(depositPoints, user.getTierUser().getTierId());
		    	params[1] = CommonUtil.displayAmount(CommonUtil.calcAmount(amount), user.getCurrencyId());
		    	ValueExpression ve = context.getApplication().getExpressionFactory().createValueExpression(context.getELContext(), ConstantsBase.BIND_USER, User.class);
		    	ve.getValue(context.getELContext());
		    	ve.setValue(context.getELContext(), user);
		    	FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_INFO,CommonUtil.getMessage("points.to.cash.action.succeed", params),null);
		    	context.addMessage(null, fm);
                
		    	return Utils.navAfterUserOperation();
        	}
        }
        return null;
	}


	/**
	 * Fix balance deposit
	 * @return
	 * @throws SQLException
	 */
	public String insertFixBalanceDeposit() throws SQLException{
		// 	prevent identical requests from being processed
        if (this.saveTokenIsInvalid() ) {
            return this.logInvalidSaveAndReturn(logger, null);
        }
        this.generateSaveToken();
        User user = Utils.getUser();
        boolean res = TransactionsManager.insertTransactionByType(deposit, comments, user.getId(),user.getUserName(),
				TransactionsManagerBase.TRANS_TYPE_FIX_BALANCE_DEPOSIT,	ConstantsBase.LOG_BALANCE_FIX_BALANCE_DEPOSIT, true);
		if (res) {
			if (user.isSupportSelected() || user.isRetentionSelected() || user.isRetentionMSelected()) {
				UsersManager.loadUserStripFields(user, true, null);
			}
			return Utils.navAfterUserOperation();
		}
		else {
			return null;
		}
	}

	/**
	 * Deposit by company
	 * @return
	 * @throws SQLException
	 */
	public String insertCompanyDeposit() throws SQLException{
		// 	prevent identical requests from being processed
        if (this.saveTokenIsInvalid() ) {
            return this.logInvalidSaveAndReturn(logger, null);
        }
        this.generateSaveToken();
        User user = Utils.getUser();
		boolean res = TransactionsManager.insertTransactionByType(deposit, comments, user.getId(),user.getUserName(),
				TransactionsManagerBase.TRANS_TYPE_DEPOSIT_BY_COMPANY, ConstantsBase.LOG_BALANCE_DEPOSIT_BY_COMPANY, true);
		if (res) {
			if (user.isSupportSelected() || user.isRetentionSelected() || user.isRetentionMSelected()) {
				UsersManager.loadUserStripFields(user, true, null);
			}
			return Utils.navAfterUserOperation();
		}
		else {
			return null;
		}
	}

	public String getTransactionForCancel() throws SQLException {
		// 	prevent identical requests from being processed
        if (this.saveTokenIsInvalid() ) {
            return this.logInvalidSaveAndReturn(logger, null);
        }
        this.generateSaveToken();
        if (null == transactionId) {
        	transactionId = new Long(0);
        }
        transaction = TransactionsManager.getByIdForCancel(transactionId.longValue());
        if (null == transaction) {
        	FacesContext context = FacesContext.getCurrentInstance();
    		FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR,CommonUtil.getMessage("cancel.deposit.success.error", null),null);
    		context.addMessage(null, fm);
            
        } else {
        	if (null == transactionsList) {
        		transactionsList = new ArrayList<Transaction>();
        	}
        	transactionsList.add(transaction);
        }
        return null;
	}

	/**
	 * Cancel deposit after success status
	 * @return
	 * @throws SQLException
	 */
	public String cancelDepositAfterSuccess() throws SQLException{
		FacesContext context = FacesContext.getCurrentInstance();
		TransactionsManager.cancelDeposit(transaction, TransactionsManagerBase.TRANS_STATUS_CANCEL_S_DEPOSIT);
		User user = Utils.getUser();
		if (user.isSupportSelected() || user.isRetentionSelected() || user.isRetentionMSelected()) {
			UsersManager.loadUserStripFields(user, true, null);
		}
		transaction = null;
		transactionId = null;
		transactionsList = null;
		context.renderResponse();
		return null;
	}

	/**
	 * Insert Paypal deposit with wagering
	 * @return
	 * @throws SQLException
	 */
	public String insertPaypalDeposit() throws SQLException{
		FacesContext context = FacesContext.getCurrentInstance();
		// prevent identical requests from being processed
        if (this.saveTokenIsInvalid() ) {
            return this.logInvalidSaveAndReturn(logger, null);
        }
        this.generateSaveToken();

        User user = Utils.getUser();
        Transaction t = null;
        
        updateUserFields(user);
		
		String msg = TransactionsManager.checkUserStatus(user);
		if(!CommonUtil.isParameterEmptyOrNull(msg)){
			FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_INFO,CommonUtil.getMessage(msg, null, user.getLocale()),null);
	    	context.addMessage(null, fm);
			return null;
		}
				
		t = TransactionsManager.insertPayPalDeposit(deposit, AdminManager.getWriterId(), user, Constants.BACKEND_CONST, false, paypalEmail, paypalTransactionId, 0L);

		reloadIsFirstDeposit();

		if (user.isSupportSelected() || user.isRetentionSelected() || user.isRetentionMSelected()) {
			UsersManager.loadUserStripFields(user, true, null);
			logger.debug("updating user session details.");
		}

		if ( t != null ) {

			//TODO - add message [and reciept]

//			if (sendReceipt && t != null) {
//				String[] params=new String[1];
//				params[0]=String.valueOf(t.getCc4digit());
//				String receiptNum = t.getReceiptNumTxt();
//				if ( user.getSkinId() != ConstantsBase.ETRADER_SKIN ) {  // for ao take transaction id
//					receiptNum = String.valueOf(t.getId());
//				}
//
//				UsersManager.sendReceiptEmail(t.getAmountTxt(),t.getTimeCreatedTxt(),
//						CommonUtil.getMessage("receipt.footer.creditcard", params, user.getLocale()),receiptNum);
//			}

			return Utils.navAfterUserOperation();
		}
		else {
			return null;
		}
	}

	/**
	 * @return the transactionId
	 */
	public Long getTransactionId() {
		return transactionId;
	}

	/**
	 * @param transactionId the transactionId to set
	 */
	public void setTransactionId(Long transactionId) {
		this.transactionId = transactionId;
	}

	/**
	 * @param transaction the transaction to set
	 */
	public void setTransaction(Transaction transaction) {
		this.transaction = transaction;
	}

	/**
	 * @return the transactionsList
	 */
	public ArrayList<Transaction> getTransactionsList() {
		return transactionsList;
	}

	/**
	 * @param transactionsList the transactionsList to set
	 */
	public void setTransactionsList(ArrayList<Transaction> transactionsList) {
		this.transactionsList = transactionsList;
	}

	public long getListSize() {
		if (null == transactionsList) {
			return 0;
		}
		return this.transactionsList.size();
	}

	/**
	 * @return the paypalEmail
	 */
	public String getPaypalEmail() {
		return paypalEmail;
	}

	/**
	 * @param paypalEmail the paypalEmail to set
	 */
	public void setPaypalEmail(String paypalEmail) {
		this.paypalEmail = paypalEmail;
	}

	/**
	 * @return the paypalTransactionId
	 */
	public String getPaypalTransactionId() {
		return paypalTransactionId;
	}

	/**
	 * @param paypalTransactionId the paypalTransactionId to set
	 */
	public void setPaypalTransactionId(String paypalTransactionId) {
		this.paypalTransactionId = paypalTransactionId;
	}

	/**
	 * @return the isCheckMinAmount
	 */
	public boolean isCheckMinAmount() {
		return isCheckMinAmount;
	}

	/**
	 * @param isCheckMinAmount the isCheckMinAmount to set
	 */
	public void setCheckMinAmount(boolean isCheckMinAmount) {
		this.isCheckMinAmount = isCheckMinAmount;
	}

	public ArrayList<SelectItem> getClearingProviderRoutes() {
		return clearingProviderRoutes;
	}

	public void setClearingProviderRoutes(
			ArrayList<SelectItem> clearingProviderRoutes) {
		this.clearingProviderRoutes = clearingProviderRoutes;
	}

	public long getProviderId() {
		return providerId;
	}

	public void setProviderId(long providerId) {
		this.providerId = providerId;
	}

	public String getRootingComment() {
		return rootingComment;
	}

	public void setRootingComment(String rootingComment) {
		this.rootingComment = rootingComment;
	}

	public String getHolderName() {
		return holderName;
	}

	public void setHolderName(String holderName) {
		this.holderName = holderName;
	}

	public String getCcPass() {
		return ccPass;
	}

	public void setCcPass(String ccPass) {
		this.ccPass = ccPass;
	}
}
