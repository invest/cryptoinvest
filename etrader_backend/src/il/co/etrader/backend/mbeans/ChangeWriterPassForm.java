package il.co.etrader.backend.mbeans;

import java.io.Serializable;

import javax.faces.context.FacesContext;

import il.co.etrader.backend.bl_managers.AdminManager;
import il.co.etrader.backend.bl_vos.User;
import il.co.etrader.backend.util.Constants;

public class ChangeWriterPassForm implements Serializable{

	private static final long serialVersionUID = 4612326633439175192L;
	private String oldPassword;
    private String newPassword;
    private String verifyPass;

   public String changePass() throws Exception{

	   FacesContext context=FacesContext.getCurrentInstance();
	   String writerName = context.getExternalContext().getRemoteUser();
	   boolean res= AdminManager.changeWriterPass(this, AdminManager.getWriterByUserName(writerName));

	   if (res){
		   User user = (User) context.getApplication().createValueBinding(Constants.BIND_USER).getValue(context);
		   user.setPasswordReset(false);
		   if (null != user && user.isHasPartnerRole()){
			   return Constants.NAV_PARTNER_TABS;
		   } else {
			return Constants.NAV_SUPPORT_TABS;
		   }
	   }
		else
			return null;
	}


	public String getOldPassword() {
		return oldPassword;
	}

	public void setOldPassword(String oldPassword) {
		this.oldPassword = oldPassword;
	}

	public String getNewPassword() {
		return newPassword;
	}

	public void setNewPassword(String newPassword) {
		this.newPassword = newPassword;
	}

	public String getVerifyPass() {
		return verifyPass;
	}

	public void setVerifyPass(String verifyPass) {
		this.verifyPass = verifyPass;
	}
	
	public String toString() {
	    String ls = System.getProperty("line.separator");
	    return ls + "ChangeWriterPassForm: " + ls
	    	+ super.toString() + ls
	        + "oldPassword: " + oldPassword + ls
	        + "newPassword: " + newPassword + ls
	        + "verifyPass: " + verifyPass + ls;
	}

}