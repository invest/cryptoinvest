/**
 * 
 */
package il.co.etrader.backend.mbeans;

import java.io.Serializable;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.ValidatorException;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.base.QuestionnaireGroup;
import com.anyoption.common.beans.base.QuestionnaireQuestion;
import com.anyoption.common.beans.base.QuestionnaireUserAnswer;
import com.anyoption.common.beans.base.UserRegulationBase;
import com.anyoption.common.managers.QuestionnaireManagerBase;
import com.anyoption.common.managers.UserRegulationManager;

import il.co.etrader.backend.bl_managers.ApplicationData;
import il.co.etrader.backend.bl_vos.WriterWrapper;
import il.co.etrader.backend.util.Constants;
import il.co.etrader.backend.util.Utils;
import il.co.etrader.bl_managers.IssuesManagerBase;
import il.co.etrader.bl_managers.RiskAlertsManagerBase;
import il.co.etrader.bl_managers.UsersManagerBase;
import il.co.etrader.bl_vos.UserBase;
import il.co.etrader.bl_vos.Writer;
import il.co.etrader.util.CommonUtil;
import il.co.etrader.util.ConstantsBase;

public class RegSingleQuestionnaireForm implements Serializable {
	
	private static final long serialVersionUID = -3356012913052462423L;
	private static final Logger log = Logger.getLogger(RegSingleQuestionnaireForm.class);
	
	private static final int IS_CORECT_QUESTIONNAIRE = 2;
	private static final int IS_RESTRICTED_OR_FAILED_QUESTIONNAIRE = 1;
	
	private boolean submitted;
	private QuestionnaireGroup questionnaire;
	private List<QuestionnaireUserAnswer> userAnswers;
	private UserBase user;	
	private UserRegulationBase userRegulation;
	private Writer writer;
	private boolean answer12;
	private boolean answer13;
	private boolean answer14;
	private boolean answer15;
	private boolean isMissing12Quest;
	
	/**
	 * @throws Exception
	 */
	public RegSingleQuestionnaireForm() throws Exception {
		WriterWrapper ww = Utils.getWriter();
		writer = ww.getWriter();
	
		questionnaire = QuestionnaireManagerBase.getAllQuestionnaires().get(QuestionnaireGroup.REGULATION_SINGLE_QUESTIONNAIRE_NAME);
		FacesContext context=FacesContext.getCurrentInstance();
		ApplicationData ap = (ApplicationData) context.getApplication().createValueBinding(Constants.BIND_APPLICATION_DATA).getValue(context);
		user = (UserBase)context.getApplication().createValueBinding(Constants.BIND_USER).getValue(context);
		userAnswers = QuestionnaireManagerBase.getUserAnswers(user.getId(), questionnaire.getQuestions(), writer.getId());
		
		if(userAnswers.size() < 13){
			log.debug("The user " + user.getId() + " is whit missing PEP checkbox row add 13 questAnswert and 12 isMissing and show NULL");
			userAnswers.add(12, userAnswers.get(11));			
			isMissing12Quest = true;
		}
		
	   	userRegulation = new UserRegulationBase();
	   	userRegulation.setUserId(user.getId());
	   	UserRegulationManager.getUserRegulation(userRegulation);
	   	
	   	//Check if PEP quest. done and is missing for old iOS version then show NULL
	   	try {
		   	if(userRegulation.getApprovedRegulationStep() >= UserRegulationBase.REGULATION_MANDATORY_QUESTIONNAIRE_DONE){
		   		for (QuestionnaireUserAnswer qua: userAnswers){
		   			if(qua.getQuestionId() == 93 && qua.getAnswerId() == null){
		   				isMissing12Quest = true;
		   			}
		   		}
		   	}			
		} catch (Exception e) {
			log.error("When check if PEP quest is donne");
		}
	}
	
	/**
	 * @return
	 */
	public List<QuestionnaireQuestion> getQuestions() {
		return questionnaire.getQuestions();
	}
	
	/**
	 * @return the userAnswers
	 */
	public List<QuestionnaireUserAnswer> getUserAnswers() {
		return userAnswers;
	}

	public boolean isSubmitted() {
		return submitted;
	}

	public void setSubmitted(boolean submitted) {
		this.submitted = submitted;
	}

	public boolean isAnswer12() {
		List<QuestionnaireQuestion> questions = questionnaire.getQuestions();
		Long answer12 = userAnswers.get(12).getAnswerId();
		if (answer12 == null || answer12 == questions.get(12).getAnswers().get(0).getId()) {
			return true;
		} else {
			return false;
		}
	}

	public void setAnswer12(boolean answer12) {
		List<QuestionnaireQuestion> questions = questionnaire.getQuestions();
		if (answer12) {
			userAnswers.get(12).setAnswerId(questions.get(12).getAnswers().get(0).getId());
		} else {
			userAnswers.get(12).setAnswerId(questions.get(12).getAnswers().get(1).getId());
		}
	}

	public boolean isAnswer13() {
		List<QuestionnaireQuestion> questions = questionnaire.getQuestions();
		Long answer13 = userAnswers.get(13).getAnswerId();
		if (answer13 == null || answer13 == questions.get(13).getAnswers().get(0).getId()) {
			return true;
		} else {
			return false;
		}
	}

	public void setAnswer13(boolean answer13) {
		List<QuestionnaireQuestion> questions = questionnaire.getQuestions();
		if (answer13) {
			userAnswers.get(13).setAnswerId(questions.get(13).getAnswers().get(0).getId());
		} else {
			userAnswers.get(13).setAnswerId(questions.get(13).getAnswers().get(1).getId());
		}
	}
	
	public boolean isAnswer14() {
		List<QuestionnaireQuestion> questions = questionnaire.getQuestions();
		Long answer14 = userAnswers.get(14).getAnswerId();
		if (answer14 == null || answer14 == questions.get(14).getAnswers().get(0).getId()) {
			return true;
		} else {
			return false;
		}
	}

	public void setAnswer14(boolean answer14) {
		List<QuestionnaireQuestion> questions = questionnaire.getQuestions();
		if (answer14) {
			userAnswers.get(14).setAnswerId(questions.get(14).getAnswers().get(0).getId());
		} else {
			userAnswers.get(14).setAnswerId(questions.get(14).getAnswers().get(1).getId());
		}
	}
	
	public boolean isAnswer15() {
		List<QuestionnaireQuestion> questions = questionnaire.getQuestions();
		Long answer15 = userAnswers.get(15).getAnswerId();
		if (answer15 == null) {
			return false;
		} else if (answer15 == questions.get(15).getAnswers().get(0).getId()) {
			return true;
		} else {
			return false;
		}
	}

	public void setAnswer15(boolean answer15) {
		List<QuestionnaireQuestion> questions = questionnaire.getQuestions();
		if (answer15) {
			userAnswers.get(15).setAnswerId(questions.get(15).getAnswers().get(0).getId());
		} else {
			userAnswers.get(15).setAnswerId(questions.get(15).getAnswers().get(1).getId());
		}
	}
	
	public boolean isEdit() {
		if (userRegulation.getApprovedRegulationStep() == UserRegulationBase.REGULATION_FIRST_DEPOSIT) { 
			if(writer.getId() == ConstantsBase.REUT_USER_ID ||  writer.getId() == ConstantsBase.NAAMA_USER_ID || writer.getId() == ConstantsBase.QABG_USER_ID) {
				return true;
			} else {
				return false;
			}			
		} else {
			return false;
		}		
	}

	/**
	 * @return
	 * @throws Exception
	 */
	public String submitQuestionnaire()throws SQLException{
		
			log.info("Inserting questionnaire [" + questionnaire.getId() + ":" + questionnaire.getName() + "]  for user [" + user.getId() + "]");
		for (QuestionnaireUserAnswer userAnswer : userAnswers) {
			userAnswer.setWriterId(writer.getId());
		}
		
		QuestionnaireManagerBase.updateUserAnswers(userAnswers, false);
		
		FacesContext context = FacesContext.getCurrentInstance();
		
		if (userRegulation.getApprovedRegulationStep() < UserRegulationBase.REGULATION_MANDATORY_QUESTIONNAIRE_DONE) {
			
			log.warn("User [" + user.getId() + "] answered correct, moving to optional questionnaire...");
			
			userRegulation.setApprovedRegulationStep(UserRegulationBase.REGULATION_MANDATORY_QUESTIONNAIRE_DONE);
			userRegulation.setWriterId(writer.getId());
			//no optional questions in version 2
			userRegulation.setOptionalQuestionnaireDone(true);
			UserRegulationManager.updateRegulationStep(userRegulation);
			
			int isCorrectQuest = IS_CORECT_QUESTIONNAIRE;			
			//Check Answers
			if(QuestionnaireManagerBase.isSingelQuestionnaireIncorrectFilled(user.getId())){
				userRegulation.setScoreGroup(UserRegulationBase.AO_REGULATION_RESTRICTED_FAILED_Q_SCORE_GROUP_ID);
				QuestionnaireManagerBase.updateUserScoreGroupOnly(userRegulation);
				il.co.etrader.bl_managers.UsersManagerBase.sendMailAfterQuestAORegulation(userRegulation);
				IssuesManagerBase.insertIssueUserRegulationRestricted(user.getId(), IssuesManagerBase.ISSUE_ACTION_TYPE_RESTRICTED_FAILED_QUESTIONNAIRE, Writer.WRITER_ID_AUTO);
				log.debug("User " + user.getId() + " was suspended(" + userRegulation.getSuspendedReasonId() + "): Incorrect Filled answers");
				isCorrectQuest = IS_RESTRICTED_OR_FAILED_QUESTIONNAIRE;
			} else {
				//Check for Restricted Answers
				long restQuest = QuestionnaireManagerBase.isSingelQuestionnaireRestrictedFilled(user.getId());
				if(restQuest > 0){					
					userRegulation.setScoreGroup(restQuest);
					QuestionnaireManagerBase.updateUserScoreGroupOnly(userRegulation);
					il.co.etrader.bl_managers.UsersManagerBase.sendMailAfterQuestAORegulation(userRegulation);
					log.debug("User " + user.getId() + " was restricted");
					isCorrectQuest = IS_RESTRICTED_OR_FAILED_QUESTIONNAIRE;				
				}
			}
			Date timeAnswearCreated = QuestionnaireManagerBase.isUserAnswerNoOnPEPQuestion(user.getId());
			if(timeAnswearCreated != null ){
				log.debug("The userId:" + user.getId() + " is answered NO on PEP! Create Issue, Auto Prohibited, Send Emails");
				userRegulation.setPepState(UserRegulationBase.PEP_AUTO_PROHIBITED);
				QuestionnaireManagerBase.updateUserPepAnswer(userRegulation);
				IssuesManagerBase.insertUserRegulationPEPRestrictedIssue(user.getId(), writer.getId(), writer.getUserName(), false);
				try {
					log.debug("Try to send support Pep Mail");
					QuestionnaireManagerBase.sendEmailInCaseUserAnswearNoOnPEPQuestion(user, timeAnswearCreated, false);
					if(userRegulation.isQuestionaireIncorrect()){
						log.debug("The user is faild quest. and not will send user Pep Mail: "+ userRegulation);
					}else{
						log.debug("Try to send User Pep Mail");
						UsersManagerBase.sendMailTemplateFile(ConstantsBase.TEMPLATE_REGULATION_PEP, writer.getId(), user, null, new String(), new String(), null);
					}
				} catch (Exception e) {
					log.error("When send PEP emails", e);
				}
			} else {
				userRegulation.setPepState(UserRegulationBase.PEP_NON_PROHIBITED);
				QuestionnaireManagerBase.updateUserPepAnswer(userRegulation);
			}			
			//Check due documents Limit
			if(isCorrectQuest == IS_CORECT_QUESTIONNAIRE){
				UserRegulationManager.checkUserDueDocumentsDepLimit(userRegulation);
			}
			
			FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_INFO,CommonUtil.getMessage("questionnaire.mandatory.submit", null),null);
			context.addMessage(null, fm);					
			return Utils.navAfterUserOperation();
		} else {
			FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR,CommonUtil.getMessage("questionnaire.mandatory.submit.error", null),null);
			context.addMessage(null, fm);
			log.warn("User regulation status is in unappropriate state [" + userRegulation + "] for [mandatory] questionnaire. Cannot submit form. Reloading...");
			return null;
		}
	}

	public void validateCheckBox(FacesContext context, UIComponent component, Object value) throws ValidatorException {
		if (value.equals(Boolean.FALSE)) {
			throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR, CommonUtil.getMessage("error.mandatory", null), null));
		}
	}

	public boolean isMissing12Quest() {
		return isMissing12Quest;
	}

	public void setMissing12Quest(boolean isMissing12Quest) {
		this.isMissing12Quest = isMissing12Quest;
	}
	
}
