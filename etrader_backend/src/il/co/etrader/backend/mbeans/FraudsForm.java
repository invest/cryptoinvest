package il.co.etrader.backend.mbeans;

import java.io.Serializable;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;

import javax.faces.context.FacesContext;

import org.apache.log4j.Logger;

import il.co.etrader.backend.bl_managers.AdminManager;
import il.co.etrader.backend.bl_managers.UsersManager;
import il.co.etrader.backend.bl_vos.User;
import il.co.etrader.backend.util.Constants;
import il.co.etrader.backend.util.Utils;
import il.co.etrader.bl_managers.PopulationsManagerBase;
import il.co.etrader.bl_vos.Fraud;
import il.co.etrader.bl_vos.UserBase;
import il.co.etrader.util.CommonUtil;

public class FraudsForm implements Serializable {

	private static final long serialVersionUID = 6085528104534733896L;
	private static final Logger log = Logger.getLogger(FraudsForm.class);

	private Fraud fraud;
	private ArrayList list;

	private String typeId;

	public FraudsForm() throws SQLException{

		search();
	}

	public int getListSize() {

		return list.size();
	}



	public String updateInsertFraud() throws SQLException{

		FacesContext context=FacesContext.getCurrentInstance();
		User user = (User)context.getApplication().createValueBinding(Constants.BIND_USER).getValue(context);

		boolean res = UsersManager.updateInsertFraud(fraud,user);

		if (res == false) {
			return null;
		}

		try {
			PopulationsManagerBase.fraud(user.getId(), AdminManager.getWriterId());
		} catch (Exception e) {
			log.warn("Problem with population frauds activities event " + e);
		}

		if (user.isSupportSelected()) {
			UsersManager.loadUserStripFields(user, true, null);
		}

		search();

		return Constants.NAV_FRAUDS;
	}

	public String bNavEditFraud() {
		return Constants.NAV_FRAUD;
	}

	public String initFraudValues() throws SQLException{

		FacesContext context=FacesContext.getCurrentInstance();

		fraud=new Fraud();
		fraud.setWriterName(context.getExternalContext().getRemoteUser());
		fraud.setTimeCreated(new Date());
		fraud.setUtcOffsetCreated(Utils.getUser(fraud.getUserId()).getUtcOffset());

		return Constants.NAV_FRAUD;

	}

	public String search() throws SQLException{

		FacesContext context=FacesContext.getCurrentInstance();
		UserBase user= (UserBase)context.getApplication().createValueBinding(Constants.BIND_USER).getValue(context);

		list=UsersManager.searchFrauds(this,user.getId());
		CommonUtil.setTablesToFirstPage();
		return null;
	}


	public ArrayList getList() {
		return list;
	}

	public Fraud getFraud() {
		return fraud;
	}

	public void setFraud(Fraud fraud) {
		this.fraud = fraud;
	}

	public String getTypeId() {
		return typeId;
	}

	public void setTypeId(String typeId) {
		this.typeId = typeId;
	}

	/**
	 * Constructs a <code>String</code> with all attributes
	 * in name = value format.
	 *
	 * @return a <code>String</code> representation
	 * of this object.
	 */
	public String toString()
	{
	    final String TAB = " \n ";

	    String retValue = "";

	    retValue = "FraudsForm ( "
	        + super.toString() + TAB
	        + "fraud = " + this.fraud + TAB
	        + "list = " + this.list + TAB
	        + "typeId = " + this.typeId + TAB
	        + " )";

	    return retValue;
	}


}
