package il.co.etrader.backend.mbeans;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.MathContext;
import java.net.URLDecoder;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;

import org.apache.log4j.Logger;

import com.anyoption.backend.managers.TradersActionsManager;
//import com.anyoption.common.beans.BinaryZeroOneHundred;
import com.anyoption.common.beans.MarketDynamicsQuoteParams;
import com.anyoption.common.beans.Opportunity;
import com.anyoption.common.beans.ReutersQuotes;
import com.anyoption.common.enums.SkinGroup;
import com.anyoption.common.jms.BackendLevelsCache;
import com.anyoption.common.util.DynamicsUtil;
//import com.copyop.common.managers.ProfileManager;
//import com.copyop.migration.UserStatisticsManager;

import il.co.etrader.backend.bl_managers.InvestmentsManager;
import il.co.etrader.backend.bl_managers.OpportunityQuotes0100Manager;
import il.co.etrader.backend.bl_managers.TraderManager;
import il.co.etrader.backend.bl_vos.OpportunityQuotes0100;
import il.co.etrader.backend.bl_vos.User;
import il.co.etrader.backend.bl_vos.WriterWrapper;
import il.co.etrader.backend.dao_managers.MarketsManager;
import il.co.etrader.backend.util.Constants;
import il.co.etrader.backend.util.Utils;
import il.co.etrader.bl_managers.ApplicationDataBase;
import il.co.etrader.bl_managers.InvestmentsManagerBase;
import il.co.etrader.bl_managers.ReutersQuotesManagerBase;
import il.co.etrader.bl_vos.Writer;
import il.co.etrader.util.CommonUtil;
import il.co.etrader.util.ConstantsBase;

public class TraderToolActionForm implements Serializable {

	private static final long serialVersionUID = -7907475328926762537L;

	private static final Logger log = Logger.getLogger(TraderToolActionForm.class);
	String spreadError = "Over the limit spread ";
	private String action ;
	private long oppId;
	private String feedName;
	private String userName;
	private String password;
	
	private double spreadET;
	private boolean spreadETchanged;
	private double spreadAO;
	private boolean spreadAOchanged;
	private double spreadZH;
	private boolean spreadZHchanged;
	private double spreadTR;
	private boolean spreadTRchanged;	
	
	private String downMsg;
	private String upMsg;
	private String hour;
	private String minute;
	private Date to;
	private String marketName;
	private int maxExposure;
    private int oldMaxExposure;
	private List<String> shiftingOppIds;
	private ArrayList<SelectItem> listSI;
	private ArrayList<SelectItem> shiftingGroupSI;
//	private BinaryZeroOneHundred binaryZeroOneHundred;
//	private BinaryZeroOneHundred binaryZeroOneHundredOld;
	private MarketDynamicsQuoteParams dynamicsQuoteParams;
	private MarketDynamicsQuoteParams dynamicsQuoteParamsOld;
	private double btraderLimit;

	WriterWrapper writer;
	long writerId;
	BackendLevelsCache levelsCache;

	//for one touch opp
	private String oppType;

	//market decimal point for settle resettle
	private int decimalPoint;

	private double newLevel;
	private double newLevelCheck;
	private String ask;
	private String askCheck;
	private String bid;
	private String bidCheck;
	private String last;
	private String lastCheck;

	//binary0100
	private String paramValue;
	private String params;

	private long marketId;
	private boolean success;

	public TraderToolActionForm() {
		log.debug("constractor");
		initSkinGroupShift();

		GregorianCalendar gc = new GregorianCalendar();
		//set time as 00:00:00
		gc.set(Calendar.HOUR_OF_DAY, 0);
		gc.set(Calendar.MINUTE, 0);
		gc.set(Calendar.SECOND, 0);
		to = gc.getTime();

		FacesContext context = FacesContext.getCurrentInstance();
		levelsCache = getLevelsCache(context);
		writer = Utils.getWriter();
		writerId = writer.getWriter().getId();

		listSI = new ArrayList<SelectItem>();
		shiftingGroupSI = new ArrayList<SelectItem>();



		listSI.add(new SelectItem("Eyal","Eyal"));
		listSI.add(new SelectItem("Eran","Eran"));
		last = "";
		ask = "";
		bid = "";
		lastCheck = "";
		askCheck = "";
		bidCheck = "";

	}

	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * @param password the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * @return the userName
	 */
	public String getUserName() {
		return userName;
	}

	/**
	 * @param userName the userName to set
	 */
	public void setUserName(String userName) {
		this.userName = userName;
	}

	void initSkinGroupShift(){
		spreadET = 0.0;
		spreadETchanged = false;
		spreadAO = 0.0;
		spreadAOchanged = false;
		spreadZH = 0.0;
		spreadZHchanged = false;
		spreadTR = 0.0;
		spreadTRchanged = false;	
	}
	
	public String getClearForm() {
		initSkinGroupShift();
		action = null;
		oppId = 0 ;
		feedName = null;
		userName = null;
		password = null;
		downMsg = null;
		upMsg = null;
		hour = null;
		minute = null;
		marketName = null;
		to = new Date();
		maxExposure = 0;
        oldMaxExposure = 0;
		decimalPoint = 0;
		shiftingOppIds = new ArrayList<String>();
		shiftingGroupSI = null;
		oppType = null;
		paramValue = null;
		params = null;
//		binaryZeroOneHundred = null;
//		binaryZeroOneHundredOld = null;
		dynamicsQuoteParams = null;
		dynamicsQuoteParamsOld = null;
		success = false;
		return null;
	}

	public String getInit(){
		FacesContext context = FacesContext.getCurrentInstance();
		Map<String, String> pm = context.getExternalContext().getRequestParameterMap();

		String ls = System.getProperty("line.separator");
		StringBuffer sb = new StringBuffer();
		String suspendMsg[] = null;
		String suspendDate[] = null;
		String suspendTime[] = null;
		if (null != pm.get("action")) {
			getClearForm();
			log.debug("action: " + pm.get("action"));
			for (Iterator<String> i = pm.keySet().iterator(); i.hasNext();) {
	            String key = (String) i.next();
	            sb.append(key).append(": ").append(pm.get(key)).append(ls);
	        }
	        log.debug(sb.toString());
	        this.action = pm.get("action").toString();
	        if (null != pm.get("oppId")) {
	        	oppId = Integer.parseInt(pm.get("oppId").toString());
	        }
	        if (null != pm.get("suspendMsg") && !pm.get("suspendMsg").toString().equalsIgnoreCase("null") && pm.get("suspendMsg").toString().trim().length() > 0) {
	        	// up msg s down msg s date time            example: 1s1s17/9/2009 10:00
	        	String suspendMsgDB = pm.get("suspendMsg").toString();
	        	if (!suspendMsgDB.startsWith("one touch")) {
	        	suspendMsg = suspendMsgDB.split("s");
		        	upMsg = suspendMsg[0];
		        	downMsg = suspendMsg[1];
		        	// date time
		        	if (suspendMsg.length == 3 && suspendMsg[2].length() > 0) {
			        	suspendDate = suspendMsg[2].split(" ");
			        	// hour:min
			        	suspendTime = suspendDate[1].split(":");
			        	hour = suspendTime[0];
			        	minute = suspendTime[1];
			        	to = new Date(suspendDate[0]);
		        	}
	        	}
	        }
	        
        	String skinGroupSpreadET = "spread"+SkinGroup.ETRADER.getId(); 
	        if (null != pm.get(skinGroupSpreadET)) {
	        	 spreadET = Double.valueOf(pm.get(skinGroupSpreadET).toString());
	        }
        	String skinGroupSpreadAO = "spread"+SkinGroup.ANYOPTION.getId(); 
	        if (null != pm.get(skinGroupSpreadAO)) {
	        	 spreadAO = Double.valueOf(pm.get(skinGroupSpreadAO).toString());
	        }
        	String skinGroupSpreadZH = "spread"+SkinGroup.ZH.getId(); 
	        if (null != pm.get(skinGroupSpreadZH)) {
	        	 spreadZH = Double.valueOf(pm.get(skinGroupSpreadZH).toString());
	        }
        	String skinGroupSpreadTR = "spread"+SkinGroup.TR.getId(); 
	        if (null != pm.get(skinGroupSpreadTR)) {
	        	 spreadTR = Double.valueOf(pm.get(skinGroupSpreadTR).toString());
	        }
	        if (null != pm.get("feedName")) {
	        	this.feedName = pm.get("feedName").toString();
	        }
	        if (null != pm.get("marketName")) {
	        	try {
	        		this.marketName = URLDecoder.decode(pm.get("marketName").toString(), "UTF-8");
	        	} catch (Exception e) {
	        		log.error("cant decode market name: " + pm.get("marketName").toString(), e);
	        		this.marketName = pm.get("marketName").toString();
	        	}
	        }
	        
	        if (action.equalsIgnoreCase("spread")) {
		        try {
		        	String from = pm.get("time");
		        	if (null !=  from ) {
		        		shiftingGroupSI = TraderManager.getShiftGroupSINonPublished(oppId, new Date(Long.parseLong(from)));
		        		shiftingOppIds.add(String.valueOf(oppId));
		        	} else {
		        		shiftingGroupSI = TraderManager.getShiftGroupSI(oppId);
		        		shiftingOppIds.add(String.valueOf(oppId));
		        	}
				} catch (SQLException e) {
					log.error("cant get shifting Group SI for opp id: " + oppId, e);
				}
		    }
		}
        return "";
	}

	
	/**
	 * reset spread
	 * @return success or fail
	 * @throws SQLException
	 */
	public String resetSpreadAction() throws SQLException {
		
		setSpreadET(0);
		setSpreadAO(0);
		setSpreadTR(0);
		setSpreadZH(0);

		return spreadAction();
	}
	
	
	public boolean getIsDisable(){
		return action.equalsIgnoreCase("disable");
	}

	public boolean isSpreadAction(){
		return action.equalsIgnoreCase("spread");
	}


	public boolean getIsSuspend(){
		return action.equalsIgnoreCase("suspend");
	}

	/**
	 * @return the action
	 */
	public String getAction() {
		return action;
	}


	/**
	 * @param action the action to set
	 */
	public void setAction(String action) {
		this.action = action;
	}


	/**
	 * @return the feedName
	 */
	public String getFeedName() {
		return feedName;
	}


	/**
	 * @param feedName the feedName to set
	 */
	public void setFeedName(String feedName) {
		this.feedName = feedName;
	}


	/**
	 * @return the oppId
	 */
	public long getOppId() {
		return oppId;
	}


	/**
	 * @param oppId the oppId to set
	 */
	public void setOppId(long oppId) {
		this.oppId = oppId;
	}

	/**
	 * @return the suspendMsgsDown
	 */
	public ArrayList<SelectItem> getSuspendMsgsDown() {
		ArrayList<SelectItem> suspendMsgsDown = new ArrayList<SelectItem>();
		suspendMsgsDown.add(new SelectItem("1",CommonUtil.getMessage("suspend.lowerline1",null)));
		suspendMsgsDown.add(new SelectItem("2",CommonUtil.getMessage("suspend.lowerline2",null)));
		suspendMsgsDown.add(new SelectItem("3",CommonUtil.getMessage("suspend.lowerline3",null)));
		suspendMsgsDown.add(new SelectItem("4",CommonUtil.getMessage("suspend.lowerline4",null)));
		return suspendMsgsDown;
	}

	/**
	 * @return the suspendMsgsUp
	 */
	public ArrayList<SelectItem> getSuspendMsgsUp() {
		ArrayList<SelectItem> suspendMsgsUp = new ArrayList<SelectItem>();
		suspendMsgsUp.add(new SelectItem("1",CommonUtil.getMessage("suspend.upperline1",null)));
		suspendMsgsUp.add(new SelectItem("2",CommonUtil.getMessage("suspend.upperline2",null)));
		suspendMsgsUp.add(new SelectItem("3",CommonUtil.getMessage("suspend.upperline3",null)));
		suspendMsgsUp.add(new SelectItem("4",CommonUtil.getMessage("suspend.upperline4",null)));
		return suspendMsgsUp;
	}

	/**
	 * @return the downMsg
	 */
	public String getDownMsg() {
		return downMsg;
	}

	/**
	 * @param downMsg the downMsg to set
	 */
	public void setDownMsg(String downMsg) {
		this.downMsg = downMsg;
	}

	/**
	 * @return the upMsg
	 */
	public String getUpMsg() {
		return upMsg;
	}

	/**
	 * @param upMsg the upMsg to set
	 */
	public void setUpMsg(String upMsg) {
		this.upMsg = upMsg;
	}

	public String navAction() throws SQLException {
		if (!checkPassword(writer.getWriter(), action)) {
			return Constants.NAV_PARENT_REFRESH;
		}
		String msgKey = Constants.TRADERTOOL_FAILED;
		/*if (action.equalsIgnoreCase("spread")) {
			BigDecimal shiftParameter = new BigDecimal(spread/100, new MathContext(10));
			log.debug("sending notify for shift change: op:" + oppId + " perc:" + shiftParameter);
			if (levelsCache.notifyForOpportunityShiftParameterChange( oppId, shiftParameter)) {
				log.debug("notify ok.");
				try {
					TraderManager.insertShift(oppId, shiftParameter, new Date(), writerId);
					addToLog(writerId, "opportunities", oppId, ConstantsBase.LOG_COMMANDS_UPDATE_SHIFT_PERC, shiftParameter.toString());
					log.info("save shift change success");
					msgKey = Constants.TRADERTOOL_SUCCESS;
				} catch (SQLException e2) {
					log.error("can't save shift change to DB!", e2);
				}
			}
			returnMsg(msgKey, "to shift opp id: " + oppId + " spread%: " + shiftParameter.toString());
			return Constants.NAV_PARENT_REFRESH;
		}*/

		if (action.equalsIgnoreCase("disable")) {
			log.debug("disable opp id:" + oppId );
			if (levelsCache.updateTraderDisable(oppId, true)) {
				try {
					TraderManager.disableEnableOppByTrader(oppId, Constants.TRADERTOOL_DISABLE_OPP);
					addToLog(writerId, "opportunities", oppId, ConstantsBase.LOG_COMMANDS_DISABLE_OPP, "");
					TradersActionsManager.refreshUsersCache(writerId);
					log.info("opp disable successfully");
					msgKey = Constants.TRADERTOOL_SUCCESS;
				} catch (SQLException e2) {
					log.error("can't disable opp id: " + oppId, e2);
				}
			}
			returnMsg(msgKey, "disable opp id: " + oppId);
			return Constants.NAV_PARENT_REFRESH;
		}

		if (action.equalsIgnoreCase("enable")) {
			if (log.isDebugEnabled()) {
				log.debug("enable opp id:" + oppId );
			}
			if (levelsCache.updateTraderDisable(oppId, false)) {
				try {
					TraderManager.disableEnableOppByTrader(oppId, Constants.TRADERTOOL_ENABLE_OPP);
					addToLog(writerId, "opportunities", oppId, ConstantsBase.LOG_COMMANDS_ENABLE_OPP, "");
					TradersActionsManager.refreshUsersCache(writerId);
					log.info("successfully enable opp id: " + oppId);
					msgKey = Constants.TRADERTOOL_SUCCESS;
				} catch (SQLException e2) {
					log.error("can't enable opp id: " + oppId, e2);
				}
			}
			returnMsg(msgKey, "enable opp id: " + oppId);
			return Constants.NAV_PARENT_REFRESH;
		}

		if (action.equalsIgnoreCase("suspend")) {
			log.debug("suspend market feed name: " + feedName);
			try {
				String suspendMsg = null;
				GregorianCalendar gc = new GregorianCalendar();
				gc.setTime(to);
				gc.set(Calendar.HOUR_OF_DAY, Integer.parseInt(hour));
				gc.set(Calendar.MINUTE, Integer.parseInt(minute));
				gc.set(Calendar.SECOND, 0);

				SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy HH:mm");
		        dateFormat.setTimeZone(TimeZone.getTimeZone("GMT+0"));
		        String date = downMsg.equals("2")?"":dateFormat.format(gc.getTime());
		        suspendMsg = this.upMsg + "s" + this.downMsg + "s" + date;
		        log.info("notify market suspend: feed:" + this.feedName + " first line: '" + this.upMsg + "' second line: '" + this.downMsg + "' cal time: " + gc.getTime() + " formated date " + dateFormat.format(gc.getTime()));
				if (levelsCache.suspendMarket(feedName, true,suspendMsg)) {
					TraderManager.updateSuspendedMarket(feedName, true,suspendMsg);
					addToLog(writerId, "markets", MarketsManager.getMarketIdByOppId(oppId), ConstantsBase.LOG_COMMANDS_SUSPEND_MARKET, "");
					TradersActionsManager.refreshUsersCache(writerId);
                    log.info("market suspend successfully, feed name: " + feedName);
                    msgKey = Constants.TRADERTOOL_SUCCESS;
                }
			} catch (SQLException e2) {
				log.error("can't suspend market, feed name: " + feedName, e2);
			}
			returnMsg(msgKey, " to suspend market: " + marketName);
			return Constants.NAV_PARENT_REFRESH;
		}

		if (action.equalsIgnoreCase("resume")) {
			log.debug("resume market feed name: " + feedName );
			try {
				log.info("notify market resume: market feed:" + feedName + " Msg:");
				if (levelsCache.suspendMarket(feedName, false, "")) {
					TraderManager.updateSuspendedMarket(feedName, false, "");
					addToLog(writerId, "markets", MarketsManager.getMarketIdByOppId(oppId), ConstantsBase.LOG_COMMANDS_UNSUSPEND_MARKET, "");
					TradersActionsManager.refreshUsersCache(writerId);
                    log.info("market resume successfully, feed name: " + feedName);
                    msgKey = Constants.TRADERTOOL_SUCCESS;
                }
			} catch (SQLException e2) {
				log.error("can't resume market, feed name: " + feedName, e2);
			}
			returnMsg(msgKey, " resume market: " + marketName);
			return Constants.NAV_PARENT_REFRESH;
		}

		returnMsg(Constants.TRADERTOOL_FAILED," find action");
		return Constants.NAV_PARENT_REFRESH;
	}

	/**
	 * check user and pass
	 * @param writer
	 * @return return true if user and pass are equals else false
	 */
	private boolean checkPassword(Writer writer, String command) {
		if (this.userName.equalsIgnoreCase(writer.getUserName()) && this.password.equalsIgnoreCase(writer.getPassword())) {
			return true;
		}
		returnMsg("tradertool.faild.pass", command);
		return false;
	}

	/**
	 * get the levelcache from the context
	 * @param context
	 * @return levelcache
	 */
	private static BackendLevelsCache getLevelsCache(FacesContext context) {
		return (BackendLevelsCache) context.getExternalContext().getApplicationMap().get("levelsCache");
	}

	/**
	 * add the action to the log table in DB
	 * @param writerId the writer who make the action
	 * @param table the table
	 * @param key
	 * @param command
	 * @param desc
	 */
	private void addToLog(long writerId, String table, long key, int command, String desc) throws SQLException {
		TraderManager.addToLog(writerId, table, key, command, desc);
	}

	/**
	 * @return the to
	 */
	public Date getTo() {
		return to;
	}

	/**
	 * @param to the to to set
	 */
	public void setTo(Date to) {
		this.to = to;
	}

	/**
	 * @return the hour
	 */
	public String getHour() {
		return hour;
	}

	/**
	 * @param hour the hour to set
	 */
	public void setHour(String hour) {
		this.hour = hour;
	}

	/**
	 * @return the minute
	 */
	public String getMinute() {
		return minute;
	}

	/**
	 * @param minute the minute to set
	 */
	public void setMinute(String minute) {
		this.minute = minute;
	}

	/**
	 * @return the hours for selec menu
	 */
	public ArrayList<SelectItem> getHours() {
		ArrayList<SelectItem> hours = new ArrayList<SelectItem>();
		String value;
		for (int j = 0; j < 24; j++) {
			value = String.valueOf(j);
			if (j < 10) {
				value = "0" + j;
			}
			hours.add(new SelectItem(value,String.valueOf(j)));
		}
		return hours;
	}

	/**
	 * @return the minuts for select menu
	 */
	public ArrayList<SelectItem> getMinutes() {
		ArrayList<SelectItem> minutes = new ArrayList<SelectItem>();
		String value;
		for (int j = 0; j < 60; j++) {
			value = String.valueOf(j);
			if (j < 10) {
				value = "0" + j;
			}
			minutes.add(new SelectItem(String.valueOf(value),String.valueOf(j)));
		}
		return minutes;
	}

	/**
	 * @return the decimalPoint
	 */
	public int getDecimalPoint() {
		return decimalPoint;
	}

	/**
	 * @param decimalPoint the decimalPoint to set
	 */
	public void setDecimalPoint(int decimalPoint) {
		this.decimalPoint = decimalPoint;
	}

	/**
	 * @return the newLevel
	 */
	public double getNewLevel() {
		return newLevel;
	}

	/**
	 * @param newLevel the newLevel to set
	 */
	public void setNewLevel(double newLevel) {
		this.newLevel = newLevel;
	}

	/**
	 * @return the newLevelCheck
	 */
	public double getNewLevelCheck() {
		return newLevelCheck;
	}

	/**
	 * @param newLevelCheck the newLevelCheck to set
	 */
	public void setNewLevelCheck(double newLevelCheck) {
		this.newLevelCheck = newLevelCheck;
	}

	public String getAsk() {
		return ask;
	}

	public void setAsk(String ask) {
		this.ask = ask;
	}

	public String getAskCheck() {
		return askCheck;
	}

	public void setAskCheck(String askCheck) {
		this.askCheck = askCheck;
	}

	public String getBid() {
		return bid;
	}

	public void setBid(String bid) {
		this.bid = bid;
	}

	public String getBidCheck() {
		return bidCheck;
	}

	public void setBidCheck(String bidCheck) {
		this.bidCheck = bidCheck;
	}

	public String getLast() {
		return last;
	}

	public void setLast(String last) {
		this.last = last;
	}

	public String getLastCheck() {
		return lastCheck;
	}

	public void setLastCheck(String lastCheck) {
		this.lastCheck = lastCheck;
	}

	/**
	 * @return the oppType
	 */
	public String getOppType() {
		return oppType;
	}

	/**
	 * @param oppType the oppType to set
	 */
	public void setOppType(String oppType) {
		this.oppType = oppType;
	}

	/**
	 * manual settle opportunity
	 * @return success or faild
	 */
	public String settle() {
		if (newLevel == newLevelCheck && last.equals(lastCheck) && ask.equals(askCheck) && bid.equals(bidCheck)) {
			if (!checkPassword(writer.getWriter(), "settle")) {
				return Constants.NAV_PARENT_REFRESH;
			}
			String msgKey = Constants.TRADERTOOL_FAILED;
			BigDecimal level = new BigDecimal(newLevel, new MathContext(10));
            Opportunity opp = null;
            try {
                 opp = InvestmentsManager.getOpportunityById(oppId);
            } catch(SQLException e) {
                log.warn("cant get opp to settle from db", e);
            }
            if (null != opp && System.currentTimeMillis() > opp.getTimeEstClosing().getTime()) {
    			log.debug("sending manual close opportunity! oppId: " + oppId + ", level: "+ level);
    			if (levelsCache.manuallyCloseOpportunity(oppId, level)) {
    				insertReutersQuoutes(opp);
    				log.debug("sent manual close opportunity! oppId: " + oppId + ", level: "+ level);
    				try {
    					TraderManager.addToLog(writerId, "opportunities", oppId, ConstantsBase.LOG_COMMANDS_MANUAL_SETTLE, "closing Level: " + level);
    					log.debug("success to write settle command to DB");
    					msgKey = Constants.TRADERTOOL_SUCCESS;
    					success = true;
    				} catch (Exception exx) {
    					log.error("cant write to log table! command settle opp id: " + oppId, exx);
    				}
    			}
            } else {
                msgKey = "tradertool.faild.still.open";
            }
			returnMsg(msgKey, " settle opp id " + oppId);
			return Constants.NAV_PARENT_REFRESH;
		}
		returnMsg("tradertool.faild.not.equle", "settle opp id " + oppId);
		return Constants.NAV_PARENT_REFRESH;
	}
	
	public void insertReutersQuoutes(Opportunity opp) {
		ReutersQuotes rq = new ReutersQuotes();
		rq.setTime(opp.getTimeEstClosing());
		if (CommonUtil.isParameterEmptyOrNull(last)) {
			rq.setLast(BigDecimal.ZERO);
		} else {
			rq.setLast(new BigDecimal(last));
		}
		if (CommonUtil.isParameterEmptyOrNull(ask)) {
			rq.setAsk(BigDecimal.ZERO);
		} else {
			rq.setAsk(new BigDecimal(ask));
		}
		if (CommonUtil.isParameterEmptyOrNull(bid)) {
			rq.setBid(BigDecimal.ZERO);
		} else {
			rq.setBid(new BigDecimal(bid));
		}
		try {
			ReutersQuotesManagerBase.insertManually(oppId, rq);
		} catch (SQLException e) {
			log.debug("Error inserting reuters quoutes." + e);
		}
	}

	public void updateReutersQuotes(Opportunity opp) throws SQLException {
		ReutersQuotes rq = new ReutersQuotes();
		if (CommonUtil.isParameterEmptyOrNull(last)) {
			rq.setLast(BigDecimal.ZERO);
		} else {
			rq.setLast(new BigDecimal(last));
		}
		if (CommonUtil.isParameterEmptyOrNull(ask)) {
			rq.setAsk(BigDecimal.ZERO);
		} else {
			rq.setAsk(new BigDecimal(ask));
		}
		if (CommonUtil.isParameterEmptyOrNull(bid)) {
			rq.setBid(BigDecimal.ZERO);
		} else {
			rq.setBid(new BigDecimal(bid));
		}
		try {
			if (!ReutersQuotesManagerBase.updateManually(opp.getId(), rq)) {
				log.warn("No quotes were updated for opportunity with id " + opp.getId());
			}
		} catch (SQLException e) {
			log.debug("Error inserting reuters quoutes" + e);
			throw e;
		}
	}

	/**
	 * settle one touch opp
	 * @return success or faild page
	 */
	public String settleOneTouch() {
		if (newLevel == newLevelCheck) {
			if (!checkPassword(writer.getWriter(), "settleOneTouch")) {
				return Constants.NAV_PARENT_REFRESH;
			}
			Opportunity opp = null;
            try {
                 opp = InvestmentsManager.getOpportunityById(oppId);
            } catch(SQLException e) {
                log.warn("cant get opp to settle from db", e);
            }
			String msgKey = Constants.TRADERTOOL_FAILED;
			BigDecimal level = new BigDecimal(newLevel, new MathContext(10));
			String typeIdTxt = oppType.equalsIgnoreCase("1")?"HIGHEST":"LOWEST";
			String[] params = new String[3];
			params[0] = typeIdTxt;
			params[1] = oppType.equalsIgnoreCase("1")?level.toString():"";
			params[2] = oppType.equalsIgnoreCase("1")?"":level.toString();
			try {
				TraderManager.closeOneTouchOpportunity(oppId, level.doubleValue(), CommonUtil.getMessage("tradertool.oneTouch.furmolaTxt", params), writerId, typeIdTxt);
				log.debug("success to settle one touch oppId: " + oppId);
				insertReutersQuoutes(opp);
				msgKey = Constants.TRADERTOOL_SUCCESS;
			} catch (Exception exx) {
				log.error("faild to settle one touch opp id: " + oppId, exx);
			}
			returnMsg(msgKey, "settle one touch opp id " + oppId);
			return Constants.NAV_PARENT_REFRESH;
		}
		returnMsg("tradertool.faild.not.equle", "settle one touch opp id " + oppId);
		return Constants.NAV_PARENT_REFRESH;
	}

	/**
	 * settle one touch opp
	 * @return success or faild page
	 */
	public String resettle() {
		if (newLevel == newLevelCheck) {
			if (!checkPassword(writer.getWriter(), "resettle")) {
				return Constants.NAV_PARENT_REFRESH;
			}
			Opportunity opp = null;
			long oppTypeId = 0l;
            try {
                 opp = InvestmentsManager.getOpportunityById(oppId);
                 oppTypeId = opp.getOpportunityTypeId();
            } catch(SQLException e) {
                log.warn("cant get opp to settle from db", e);
            }
			String msgKey = Constants.TRADERTOOL_FAILED;
			BigDecimal level = new BigDecimal(newLevel, new MathContext(10));
			log.debug("resettle opportunity: " + oppId + ", level: " + level);
			try {
				HashSet<Long> listOfUsers = TraderManager.resettleOpp(oppId, level.doubleValue(), writerId, oppTypeId);
				TraderManager.addToLog(writerId, "opportunities", oppId, ConstantsBase.LOG_COMMANDS_RESETTLE_OPP, "closing Level: " + level);
				updateReutersQuotes(opp);
				log.debug("success to resettle oppid: " + oppId);
//				log.debug("start resettle for copyop profiles");
//				for(Long userId:listOfUsers) {
//					if(ProfileManager.profileExists(userId)){
//						try {
//							UserStatisticsManager.updateUserStatistics(userId);
//						} catch (Exception e) {
//							log.debug("cannot resettle for copyop profile "+userId);							
//						}
//					}
//				}
//				log.debug("end resettle for copyop profiles");
				msgKey = Constants.TRADERTOOL_SUCCESS;
			} catch (Exception exx) {
				log.error("faild to resettle opp id: " + oppId, exx);
			}
			returnMsg(msgKey, "resettle opp id " + oppId);
			return Constants.NAV_PARENT_REFRESH;
		}
		returnMsg("tradertool.faild.not.equle", "resettle opp id " + oppId);
		return Constants.NAV_PARENT_REFRESH;
	}

	/**
	 * resettle one touch opp
	 * @return success or faild page
	 */
	public String resettleOneTouch() {
		if (newLevel == newLevelCheck) {
			if (!checkPassword(writer.getWriter(), "resettleOneTouch")) {
				return Constants.NAV_PARENT_REFRESH;
			}
			Opportunity opp = null;
			long oppTypeId = 0l;
            try {
                 opp = InvestmentsManager.getOpportunityById(oppId);
                 oppTypeId = opp.getOpportunityTypeId();
            } catch(SQLException e) {
                log.warn("cant get opp to settle from db", e);
            }
			String msgKey = Constants.TRADERTOOL_FAILED;
			BigDecimal level = new BigDecimal(newLevel, new MathContext(10));
			String typeIdTxt = oppType.equalsIgnoreCase("1")?"HIGHEST":"LOWEST";
			String[] params = new String[3];
			params[0] = typeIdTxt;
			params[1] = oppType.equalsIgnoreCase("1")?level.toString():"";
			params[2] = oppType.equalsIgnoreCase("1")?"":level.toString();
			log.debug("resettle one touch opportunity: " + oppId + ", level: " + level);
			try {
				TraderManager.resettleOpp(oppId, level.doubleValue(), writerId, oppTypeId);
				TraderManager.addToLog(writerId, "opportunities", oppId, ConstantsBase.LOG_COMMANDS_RESETTLE_OPP, typeIdTxt +" Level: " + level);
				updateReutersQuotes(opp);
				log.debug("success to resettle one touch opp id " + oppId);
				msgKey = Constants.TRADERTOOL_SUCCESS;
			} catch (Exception exx) {
				log.error("faild to resettle one touch opp id: " + oppId, exx);
			}
			returnMsg(msgKey, "resettle one touch opp id " + oppId);
			return Constants.NAV_PARENT_REFRESH;
		}
		returnMsg("tradertool.faild.not.equle", "resettle one touch opp id " + oppId);
		return Constants.NAV_PARENT_REFRESH;
	}

	/**
	 * return msg for the user: success or faild
	 * @param msg to show for the user
	 * @param param that send to the msg
	 */
	public void returnMsg(String msg, String param) {
		FacesContext context = FacesContext.getCurrentInstance();
		String[] params = new String[1];
		params[0] = param;
		FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR,CommonUtil.getMessage(msg, params),null);
		context.addMessage(null, fm);

	}


	public String getInitSettle() {
		FacesContext context = FacesContext.getCurrentInstance();
		Map<String, String> pm = context.getExternalContext().getRequestParameterMap();
		if (null != pm.get("oppId")) {
			getClearForm();
	        oppId = Integer.parseInt(pm.get("oppId").toString());
        }
		if (null != pm.get("oppType")) {
        	this.oppType = pm.get("oppType").toString();
		}
		if (null != pm.get("marketName")) {
			try {
        		this.marketName = URLDecoder.decode(pm.get("marketName").toString(), "UTF-8");
        	} catch (Exception e) {
        		log.error("cant decode market name: " + pm.get("marketName").toString(), e);
        	}
        }
		if (null != pm.get("action")) {
	        this.action = pm.get("action").toString();
		}
		if (null != pm.get("decimalPoint")) {
			try {
				this.decimalPoint = Integer.parseInt(pm.get("decimalPoint").toString());
			} catch (Exception e) {
				log.error("cant parse market decimal Point: " + pm.get("decimalPoint").toString(), e);
			}
		}
		success = false;
		return "";
	}

	public String spreadAction() throws SQLException {
		String msgKey = Constants.TRADERTOOL_FAILED;
		User user = Utils.getUser();
		
		if(shiftingOppIds == null || shiftingOppIds.size()==0) {
			returnMsg(msgKey, "No opportunities currently loaded !");
			return Constants.NAV_PARENT_REFRESH;
		}
		
		if (!checkSpreadLimit(user, spreadET, Long.valueOf(shiftingOppIds.get(0)))) {
				returnMsg(msgKey, spreadError +" ET " + spreadET+" limit:"+btraderLimit);
				return Constants.NAV_PARENT_REFRESH;
		}
		if(!checkSpreadLimit(user, spreadAO, Long.valueOf(shiftingOppIds.get(0)))) {
			returnMsg(msgKey, spreadError + " AO " + spreadAO+" limit:"+btraderLimit);
			return Constants.NAV_PARENT_REFRESH;
		}	

		if(!checkSpreadLimit(user, spreadZH, Long.valueOf(shiftingOppIds.get(0)))) {
			returnMsg(msgKey, spreadError + " ZH " + spreadZH+" limit:"+btraderLimit);
			return Constants.NAV_PARENT_REFRESH;
		}
		if(!checkSpreadLimit(user, spreadTR, Long.valueOf(shiftingOppIds.get(0)))){
				returnMsg(msgKey, spreadError + " TR " + spreadTR+" limit:"+btraderLimit);
				return Constants.NAV_PARENT_REFRESH;
		}


		if (!checkPassword(writer.getWriter(), action)) {
			return Constants.NAV_PARENT_REFRESH;
		}
							  
		BigDecimal shiftParameterET = new BigDecimal(spreadET / 100, new MathContext(10));
		BigDecimal shiftParameterAO = new BigDecimal(spreadAO / 100, new MathContext(10));
		BigDecimal shiftParameterZH = new BigDecimal(spreadZH / 100, new MathContext(10));
		BigDecimal shiftParameterTR = new BigDecimal(spreadTR / 100, new MathContext(10));
		// the order in this list follows com.anyoption.common.enums.SkinGroup natural order
		ArrayList<BigDecimal> shiftParameters = new ArrayList<BigDecimal>();
		shiftParameters.add(shiftParameterET);
		shiftParameters.add(shiftParameterAO);
		shiftParameters.add(shiftParameterZH);
		shiftParameters.add(shiftParameterTR);
				
		long tempOppId;
		String msg = "";
		for (int i = 0; i < shiftingOppIds.size(); i++) {
			tempOppId = Long.valueOf(shiftingOppIds.get(i));
			log.debug("sending notify for shift change: op: " + tempOppId + " perc et: " + shiftParameters);
			if (levelsCache.notifyForOpportunityShiftParameterChange(tempOppId, shiftParameters)) {
				log.debug("notify ok.");
				try {
						if(spreadETchanged) {
							TraderManager.insertShift(tempOppId, shiftParameterET, SkinGroup.ETRADER, new Date(), writerId);
						}
						if(spreadAOchanged) {
							TraderManager.insertShift(tempOppId, shiftParameterAO, SkinGroup.ANYOPTION, new Date(), writerId);
						}
						if(spreadZHchanged) {
							TraderManager.insertShift(tempOppId, shiftParameterZH, SkinGroup.ZH, new Date(), writerId);
						}
						if(spreadTRchanged) {
							TraderManager.insertShift(tempOppId, shiftParameterTR, SkinGroup.TR, new Date(), writerId);
						}
						log.debug("success to insert shift opp id: " + tempOppId);
						msgKey = Constants.TRADERTOOL_SUCCESS;
						msg = "shift opp id " + tempOppId;
				} catch (SQLException e2) {
					log.debug("shift opp id: " + tempOppId + " sql problem", e2);
				}
			} else {
				msg = "shift opp id " + tempOppId;
			}
			returnMsg(msgKey, msg);
		}
		return Constants.NAV_PARENT_REFRESH;
	}

	/**
	 * check if the spread is in the limits
	 * @param user
	 * @param spread
	 * @return
	 */
	public boolean checkSpreadLimit(User user, double spread, long oppId) {
		try {
			btraderLimit = TraderManager.getNightShiftLimitByOppId(oppId);
		} catch (SQLException e) {
			log.warn("cant get night shift limit use defualt " + Constants.TRADERTOOL_BTRADER_SPREAD_LIMIT);
			btraderLimit = Constants.TRADERTOOL_BTRADER_SPREAD_LIMIT;
		}
		if (((user.isHasASTraderRole() || user.isHasSTraderRole()) && Math.abs(spread) <= Constants.TRADERTOOL_ASTRADER_SPREAD_LIMIT) ||
				(user.isHasTraderRole() && !user.isHasBTraderRole() && Math.abs(spread) <= Constants.TRADERTOOL_TRADER_SPREAD_LIMIT) ||
				(user.isHasBTraderRole() && Math.abs(spread) <= btraderLimit)) {
			return true;
		}
		return false;
	}

	/**
	 * @return the marketName
	 */
	public String getMarketName() {
		return marketName;
	}

	/**
	 * @param marketName the marketName to set
	 */
	public void setMarketName(String marketName) {
		this.marketName = marketName;
	}

	/**
	 * @return the maxExposure
	 */
	public int getMaxExposure() {
		return maxExposure;
	}

	/**
	 * @param maxExposure the maxExposure to set
	 */
	public void setMaxExposure(int maxExposure) {
		this.maxExposure = maxExposure;
	}

	public String getInitMaxExposure() {
		FacesContext context = FacesContext.getCurrentInstance();
		Map<String, String> pm = context.getExternalContext().getRequestParameterMap();
		if (null != pm.get("oppId")) {
			getClearForm();
	        oppId = Integer.parseInt(pm.get("oppId").toString());
        }

		if (null != pm.get("marketName")) {
			try {
        		this.marketName = URLDecoder.decode(pm.get("marketName").toString(), "UTF-8");
        	} catch (Exception e) {
        		log.error("cant decode market name: " + pm.get("marketName").toString(), e);
        	}
        }

		if (null != pm.get("maxExposure")) {
	        this.maxExposure = Integer.parseInt(pm.get("maxExposure").toString());
            this.oldMaxExposure = this.maxExposure;
        }

		if (null != pm.get("action")) {
	        this.action = pm.get("action").toString();
		}

		return "";
	}

	public String maxExposureAction() {
		User user = Utils.getUser();
        long maxExposureLimit = Constants.TRADERTOOL_TRADER_EXPOSURE_LIMIT;
        if (user.isHasASTraderRole() || user.isHasSTraderRole()) {
            maxExposureLimit = Constants.TRADERTOOL_ASTRADER_EXPOSURE_LIMIT;
        } else if (user.isHasBTraderRole()) {
            maxExposureLimit = Constants.TRADERTOOL_BTRADER_EXPOSURE_LIMIT;
        }

		if (((user.isHasASTraderRole() || user.isHasSTraderRole()) && maxExposure <= Constants.TRADERTOOL_ASTRADER_EXPOSURE_LIMIT) ||
				(user.isHasTraderRole() && !user.isHasBTraderRole() && maxExposure <= Constants.TRADERTOOL_TRADER_EXPOSURE_LIMIT) ||
                (user.isHasBTraderRole() && Math.abs(maxExposure - oldMaxExposure) <= Constants.TRADERTOOL_BTRADER_EXPOSURE_LIMIT)) {
			//do nothing
		} else {
			returnMsg(Constants.TRADERTOOL_FAILED, "change max exposure. You can change exposure to a maximum of " + (user.isHasBTraderRole() ? oldMaxExposure + maxExposureLimit : maxExposureLimit) + (user.isHasBTraderRole() ? " or a minimum of " + (oldMaxExposure - maxExposureLimit) : "") + " .");
			return Constants.NAV_PARENT_REFRESH;
		}
		if (!checkPassword(writer.getWriter(), action)) {
			return Constants.NAV_EMPTY;
		}

		try {
			if (levelsCache.updateMaxExposureAllSkinGroups(oppId, maxExposure)) {
				TraderManager.updateMaxExposureAllSkinGroup(oppId, maxExposure);
				addToLog(writerId, "opportunities", oppId, ConstantsBase.LOG_COMMANDS_CHANGE_EXPOSURE, "");
				TradersActionsManager.refreshUsersCache(writerId);
			} else {
				log.error("can't notify service max Exposure for opp id: " + oppId);
				returnMsg(Constants.TRADERTOOL_FAILED, "update max Exposure for opp id: " + oppId);
				return Constants.NAV_EMPTY;
			}
		} catch (SQLException e2) {
			log.error("can't update max Exposure for opp id: " + oppId, e2);
			returnMsg(Constants.TRADERTOOL_FAILED, "update max Exposure for opp id: " + oppId);
			return Constants.NAV_EMPTY;
		}
		log.info("seccfully update max Exposure for opp id " + oppId);
		returnMsg(Constants.TRADERTOOL_SUCCESS, "update max Exposure for opp id: " + oppId);
		return Constants.NAV_EMPTY;
	}

	public ArrayList<SelectItem> getScheduledListSI() {
		ApplicationDataBase ap = Utils.getApplicationData();
		ArrayList<SelectItem> si = new ArrayList<SelectItem>();
		si.add(new SelectItem(new Long(0),"All"));
		si.addAll(ap.getScheduledListSI());
		return si;
	}

	/**
	 * @return the shiftingOppIds
	 */
	public List<String> getShiftingOppIds() {
		return shiftingOppIds;
	}

	/**
	 * @param shiftingOppIds the shiftingOppIds to set
	 */
	public void setShiftingOppIds(List<String> shiftingOppIds) {
		this.shiftingOppIds = shiftingOppIds;
	}

	/**
	 * @return the shiftingGroupSI
	 */
	public ArrayList<SelectItem> getShiftingGroupSI() {
		return shiftingGroupSI;
	}

	/**
	 * @param shiftingGroupSI the shiftingGroupSI to set
	 */
	public void setShiftingGroupSI(ArrayList<SelectItem> shiftingGroupSI) {
		this.shiftingGroupSI = shiftingGroupSI;
	}

	public boolean isShiftingGroupSIEmpty() {
		if (null != shiftingGroupSI && shiftingGroupSI.size() > 0) {
			return false;
		}
		return true;
	}

	/**
	 * @return the listSI
	 */
	public ArrayList<SelectItem> getListSI() {
		return listSI;
	}

	/**
	 * @param listSI the listSI to set
	 */
	public void setListSI(ArrayList<SelectItem> listSI) {
		this.listSI = listSI;
	}

	/**
	 * @return the paramValue
	 */
	public String getParamValue() {
		return paramValue;
	}

	/**
	 * @param paramValue the paramValue to set
	 */
	public void setParamValue(String paramValue) {
		this.paramValue = paramValue;
	}


	/**
	 * @return the params
	 */
	public String getParams() {
		return params;
	}

	/**
	 * @param params the params to set
	 */
	public void setParams(String params) {
		this.params = params;
	}

//	public String getInitChangeParams0100() {
//		FacesContext context = FacesContext.getCurrentInstance();
//		Map<String, String> pm = context.getExternalContext().getRequestParameterMap();
//		if (null != pm.get("oppId")) {
//			getClearForm();
//	        oppId = Integer.parseInt(pm.get("oppId").toString());
//        }
//
//		if (null != pm.get("marketName")) {
//			try {
//        		this.marketName = URLDecoder.decode(pm.get("marketName").toString(), "UTF-8");
//        	} catch (Exception e) {
//        		log.error("cant decode market name: " + pm.get("marketName").toString(), e);
//        	}
//        }
//
//		if (null != pm.get("paramValue")) {
//	        this.paramValue = pm.get("paramValue").toString();
//        }
//
//		if (null != pm.get("params")) {
//			binaryZeroOneHundred = new BinaryZeroOneHundred(pm.get("params").toString());
//			binaryZeroOneHundredOld = new BinaryZeroOneHundred(pm.get("params").toString());
//        }
//
//		if (null != pm.get("action")) {
//	        this.action = pm.get("action").toString();
//		}
//
//		if (null != pm.get("marketId")) {
//	        this.marketId = Long.valueOf(pm.get("marketId").toString());
//        }
//
//		return "";
//	}
	
	public String getInitChangeDynamicsQuoteParams() {
		try {
			FacesContext context = FacesContext.getCurrentInstance();
			Map<String, String> pm = context.getExternalContext().getRequestParameterMap();
			if (null != pm.get("oppId")) {
				getClearForm();
		        oppId = Integer.parseInt(pm.get("oppId").toString());
		        Opportunity opp = InvestmentsManagerBase.getOpportunityById(oppId);
		        DynamicsUtil.parseOpportunityQuoteParams(opp);
				dynamicsQuoteParams = (MarketDynamicsQuoteParams) opp.getQuoteParamsObject();
				dynamicsQuoteParamsOld = dynamicsQuoteParams;
				log.trace("dynamicsQuoteParams: " + dynamicsQuoteParams);
				marketId = opp.getMarketId();
	        }

			if (null != pm.get("marketName")) {
				try {
	        		this.marketName = URLDecoder.decode(pm.get("marketName").toString(), "UTF-8");
	        	} catch (Exception e) {
	        		log.error("cant decode market name: " + pm.get("marketName").toString(), e);
	        	}
	        }
		} catch (Exception e) {
			log.error("Problem init change dynamics quote params screen.", e);
		}
		return "";
	}

	/**
	 * change param according to the acrion name
	 * @return
	 */
//	public String changeParamsAction() {
////		FacesContext context = FacesContext.getCurrentInstance();
////		User user = (User) context.getApplication().createValueBinding(Constants.BIND_USER).getValue(context);
//
////		double currentParamValue = Double.parseDouble(paramValue);
//		//check q or t limits
//		//if (action.equalsIgnoreCase("Q")) {
//			if (Constants.TRADERTOOL_TRADER_PARAM_Q_DOWN_LIMIT >= binaryZeroOneHundred.getQ() || binaryZeroOneHundred.getQ() >= Constants.TRADERTOOL_TRADER_PARAM_Q_UP_LIMIT) {
//				returnMsg(Constants.TRADERTOOL_FAILED, "change Q param. You can change Q to a maximum of " + Constants.TRADERTOOL_TRADER_PARAM_Q_UP_LIMIT + " or a minimum of " + Constants.TRADERTOOL_TRADER_PARAM_Q_DOWN_LIMIT + " .");
//				return Constants.NAV_PARENT_REFRESH;
//			}
//		//} else if (action.equalsIgnoreCase("T")) {
//			if (isNotValidateTparam(binaryZeroOneHundred)) {
//				returnMsg(Constants.TRADERTOOL_FAILED, "change T param. You can change T to a maximum of " + Constants.TRADERTOOL_TRADER_PARAM_T_UP_LIMIT + " or a minimum of " + Constants.TRADERTOOL_TRADER_PARAM_T_DOWN_LIMIT + " .");
//				return Constants.NAV_PARENT_REFRESH;
//			}
//		//}
//			if (Constants.TRADERTOOL_TRADER_PARAM_C_DOWN_LIMIT > binaryZeroOneHundred.getCThreashold() || binaryZeroOneHundred.getCThreashold() > (writer.getWriter().getId() == 3/*SD*/ || writer.getWriter().getId() == 5/*SB*/ ? Constants.TRADERTOOL_TRADER_PARAM_C_UP_LIMIT_SHY : Constants.TRADERTOOL_TRADER_PARAM_C_UP_LIMIT)) {
//				returnMsg(Constants.TRADERTOOL_FAILED, "change C param. You can change C to a maximum of " + (writer.getWriter().getId() == 3 || writer.getWriter().getId() == 5 ? Constants.TRADERTOOL_TRADER_PARAM_C_UP_LIMIT_SHY : Constants.TRADERTOOL_TRADER_PARAM_C_UP_LIMIT) + " or a minimum of " + Constants.TRADERTOOL_TRADER_PARAM_C_DOWN_LIMIT + " .");
//				return Constants.NAV_PARENT_REFRESH;
//			}
//		if (!checkPassword(writer.getWriter(), action)) {
//			return Constants.NAV_EMPTY;
//		}
//
//		try {
////			String paramWithValue = action + "=" + paramValue + ";";
//			if (levelsCache.updateDynamicsQuoteParams(oppId, binaryZeroOneHundred.toString())) {
//				//set the old params
//	//			BinaryZeroOneHundred temp = new BinaryZeroOneHundred(params);
//				//change the one we want
//	//			temp.changeParams(paramWithValue);
//				TraderManager.updateBinary0100opp(oppId, binaryZeroOneHundred.toString());
//
//				try {
//					OpportunityQuotes0100 vo = new OpportunityQuotes0100(oppId, marketId, Constants.SCREEN_TYPE_TT, writer.getWriter().getId(), binaryZeroOneHundred, binaryZeroOneHundred.getDiffrent(binaryZeroOneHundredOld));
//					OpportunityQuotes0100Manager.insertOpportunityQuotes0100(vo);
//				} catch (Exception e) {
//					log.debug("cant insert OpportunityQuotes0100", e);
//				}
//			} else {
//				log.error("can't notify service change param " + action + " for opp id: " + oppId);
//				returnMsg(Constants.TRADERTOOL_FAILED, "update change param " + action + "  for opp id: " + oppId);
//				return Constants.NAV_EMPTY;
//			}
//		} catch (SQLException e2) {
//			log.error("can't update change param " + action + " for opp id: " + oppId, e2);
//			returnMsg(Constants.TRADERTOOL_FAILED, "update change param " + action + " for opp id: " + oppId);
//			return Constants.NAV_EMPTY;
//		}
//		log.info("seccfully update change param " + action + " for opp id " + oppId);
//		returnMsg(Constants.TRADERTOOL_SUCCESS, "update change param " + action + " for opp id: " + oppId);
//		return Constants.NAV_EMPTY;
//	}

	public String changeDynamicsParamsAction() {
		if (Constants.TRADERTOOL_TRADER_PARAM_T_DOWN_LIMIT >= dynamicsQuoteParams.getT() || dynamicsQuoteParams.getT() >= Constants.TRADERTOOL_TRADER_PARAM_T_UP_LIMIT) {
			returnMsg(Constants.TRADERTOOL_FAILED, "change T param. You can change T to a maximum of " + Constants.TRADERTOOL_TRADER_PARAM_T_UP_LIMIT + " or a minimum of " + Constants.TRADERTOOL_TRADER_PARAM_T_DOWN_LIMIT + " .");
			return Constants.NAV_PARENT_REFRESH;
		}
		log.trace("writer user: " + writer.getWriter().getUserName() + " writer pass: " + writer.getWriter().getPassword() + " user: " + this.userName + " pass: " + this.password);
		if (!checkPassword(writer.getWriter(), action)) {
			return Constants.NAV_EMPTY;
		}
		String quoteParams = DynamicsUtil.quoteParamsToString(dynamicsQuoteParams);
		if (levelsCache.updateDynamicsQuoteParams(oppId, quoteParams)) {
			try {
				TraderManager.updateBinary0100opp(oppId, quoteParams);
				try {
					String comment = DynamicsUtil.quoteParamsToString(dynamicsQuoteParams) + DynamicsUtil.quoteParamsToString(dynamicsQuoteParamsOld);
					OpportunityQuotes0100 vo = new OpportunityQuotes0100(oppId, marketId, Constants.SCREEN_TYPE_TT, writer.getWriter().getId(), dynamicsQuoteParams, comment);
					OpportunityQuotes0100Manager.insertOpportunityQuotes0100(vo);
				} catch (Exception e) {
					log.debug("cant insert OpportunityQuotes0100", e);
				}
			} catch (Exception e) {
				log.error("Can't change param " + action + " for opp id: " + oppId, e);
				returnMsg(Constants.TRADERTOOL_FAILED, "update change param " + action + " for opp id: " + oppId);
				return Constants.NAV_EMPTY;
			}
		} else {
			log.error("can't notify service change param " + action + " for opp id: " + oppId);
			returnMsg(Constants.TRADERTOOL_FAILED, "update change param " + action + "  for opp id: " + oppId);
			return Constants.NAV_EMPTY;
		}
		log.info("seccfully update change param " + action + " for opp id " + oppId);
		returnMsg(Constants.TRADERTOOL_SUCCESS, "update change param " + action + " for opp id: " + oppId);
		return Constants.NAV_EMPTY;
	}
	
//	public int resolveSpreadType(){
//		if(spreadAOchanged && spreadETchanged ) {
//			return Constants.SHIFTING_TYPE_BOTH;
//		}
//		if(spreadAOchanged) {
//			return Constants.SHIFTING_TYPE_AO;
//		}
//		if( spreadETchanged) {
//			return Constants.SHIFTING_TYPE_ET;
//		}
//
//		return -1;
//	}

//	public BinaryZeroOneHundred getBinaryZeroOneHundred() {
//		return binaryZeroOneHundred;
//	}
//
//	public void setBinaryZeroOneHundred(BinaryZeroOneHundred binaryZeroOneHundred) {
//		this.binaryZeroOneHundred = binaryZeroOneHundred;
//	}

	public MarketDynamicsQuoteParams getDynamicsQuoteParams() {
		return dynamicsQuoteParams;
	}

	public void setDynamicsQuoteParams(MarketDynamicsQuoteParams dynamicsQuoteParams) {
		this.dynamicsQuoteParams = dynamicsQuoteParams;
	}

//	public static boolean isNotValidateTparam(BinaryZeroOneHundred b) {
//		return Constants.TRADERTOOL_TRADER_PARAM_T_DOWN_LIMIT >= b.getT() || b.getT() >= Constants.TRADERTOOL_TRADER_PARAM_T_UP_LIMIT;
//	}

	public static boolean isValidTParam(double t) {
		return Constants.TRADERTOOL_TRADER_PARAM_T_DOWN_LIMIT < t && t < Constants.TRADERTOOL_TRADER_PARAM_T_UP_LIMIT;
	}
	
//	public static boolean changeParamUpdate(BinaryZeroOneHundred bNew, long oppId, long marketId, WriterWrapper writer, BinaryZeroOneHundred bOld) throws SQLException {
//		FacesContext context = FacesContext.getCurrentInstance();
//		if (getLevelsCache(context).updateBinary0100opp(oppId, bNew.toString())) {
//			TraderManager.updateBinary0100opp(oppId, bNew.toString());
//
//			try {
//				OpportunityQuotes0100 vo = new OpportunityQuotes0100(oppId, marketId, Constants.SCREEN_TYPE_TT, writer.getWriter().getId(), bNew, bNew.getDiffrent(bOld));
//				OpportunityQuotes0100Manager.insertOpportunityQuotes0100(vo);
//			} catch (Exception e) {
//				log.debug("cant insert OpportunityQuotes0100", e);
//			}
//			return true;
//		}
//		return false;
//	}

//	public static boolean updateTparamAjax(BinaryZeroOneHundred bNew, long oppId, long marketId, WriterWrapper writer, BinaryZeroOneHundred bOld) throws SQLException {
//		if (isNotValidateTparam(bNew) || !changeParamUpdate(bNew, oppId, marketId, writer, bOld)) {
//			return false;
//		}
//		return true;
//	}

	public static boolean updateDynamicsTParam(long oppId, double changeToT, long marketId, WriterWrapper writer) {
		try {
			Opportunity opp = InvestmentsManagerBase.getOpportunityById(oppId);
			DynamicsUtil.parseOpportunityQuoteParams(opp);
			MarketDynamicsQuoteParams qp = (MarketDynamicsQuoteParams) opp.getQuoteParamsObject();
			qp.setT(qp.getT() + changeToT);
			if (isValidTParam(qp.getT())) {
				String qpStr = DynamicsUtil.quoteParamsToString(qp);
				FacesContext context = FacesContext.getCurrentInstance();
				if (getLevelsCache(context).updateDynamicsQuoteParams(oppId, qpStr)) {
					TraderManager.updateBinary0100opp(oppId, qpStr);
					try {
						String comment = DynamicsUtil.quoteParamsToString(qp) + opp.getQuoteParams();
						OpportunityQuotes0100 vo = new OpportunityQuotes0100(oppId, marketId, Constants.SCREEN_TYPE_TT, writer.getWriter().getId(), qp, comment);
						OpportunityQuotes0100Manager.insertOpportunityQuotes0100(vo);
					} catch (Exception e) {
						log.debug("cant insert OpportunityQuotes0100", e);
					}
					return true;
				}
			}
		} catch (Exception e) {
			log.error("Can't change dynamics T param", e);
		}
		return false;
	}
	
	/**
	 * suspend one touch market
	 * @return
	 */
	public String suspendOneTouchAction() {
		log.debug(action + " market feed name: " + feedName);
		String msgKey = Constants.TRADERTOOL_FAILED;
		if (!checkPassword(writer.getWriter(), action)) {
			return Constants.NAV_EMPTY;
		}
		try {
			boolean suspend = true;
			String suspendMsg = "one touch " + (new Date()).toString();
			if (action.equalsIgnoreCase("resume")) {
				suspend = false;
				suspendMsg = "";
			}
			TraderManager.updateSuspendedMarket(feedName, suspend, suspendMsg);
			addToLog(writerId, "markets", marketId, ConstantsBase.LOG_COMMANDS_SUSPEND_MARKET, "");
			TradersActionsManager.refreshUsersCache(writerId);
            log.info("market " + action + " successfully, feed name: " + feedName);
            msgKey = Constants.TRADERTOOL_SUCCESS;
		} catch (SQLException e2) {
			log.error("can't " + action +" market, feed name: " + feedName, e2);
		}
		returnMsg(msgKey, " to " + action + " market: " + marketName);
		return Constants.NAV_PARENT_REFRESH;
	}

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public double getSpreadET() {
		return spreadET;
	}

	public void setSpreadET(double spread) {
		if( spreadET != spread) {
			spreadETchanged = true;
		} else {
			spreadETchanged = false;
		}
		this.spreadET = spread;
	}

	public double getSpreadAO() {
		return spreadAO;
	}

	public void setSpreadAO(double spread) {
		if( spreadAO != spread) {
			spreadAOchanged = true;
		} else {
			spreadAOchanged = false;
		}
		this.spreadAO = spread;
	}

	public double getSpreadZH() {
		return spreadZH;
	}

	public void setSpreadZH(double spread) {
		if( spreadZH != spread) {
			spreadZHchanged = true;
		} else {
			spreadZHchanged = false;
		}
		this.spreadZH = spread;
	}

	public double getSpreadTR() {
		return spreadTR;
	}

	public void setSpreadTR(double spread) {
		if( spreadTR != spread) {
			spreadTRchanged = true;
		} else {
			spreadTRchanged = false;
		}
		this.spreadTR = spread;
	}

}
