package il.co.etrader.backend.mbeans;

import java.io.Serializable;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;

import com.anyoption.common.beans.Transaction;
import com.anyoption.common.managers.TransactionsManagerBase.transactionUpdateStatus;

import il.co.etrader.backend.bl_managers.TransactionsManager;
import il.co.etrader.backend.util.FormBase;
import il.co.etrader.backend.util.Utils;
import il.co.etrader.bl_managers.ClearingManager;
import il.co.etrader.util.CommonUtil;

public class TransactionUpdateForm extends FormBase implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1010880218083380717L;
	private Long transactionId;
	private long classId;
	private String captureCode;
	private long statusId;
	private List<SelectItem> transactionStatuses;
	private long typeId;
	private List<SelectItem> transactionTypes;
	private boolean withdraw;
	private long providerId;
	private List<SelectItem> clearingProviders;
	private boolean showClearingProviders;
	
	public TransactionUpdateForm() {
		classId = TransactionsManager.TRANS_CLASS_DEPOSIT;
		withdraw = false;
		showClearingProviders = false;
		transactionTypes = new ArrayList<SelectItem>();
		transactionStatuses = new ArrayList<SelectItem>();
		clearingProviders = new ArrayList<SelectItem>();
		initStatuses();
		updateTypes(classId);
		initClearingProviders();
	}

	public void updateTypes(ValueChangeEvent event) {
		long value = (Long) event.getNewValue();
		if (value == TransactionsManager.TRANS_CLASS_WITHDRAW) {
			withdraw = true;
		} else {
			withdraw = false;
		}
		showClearingProviders = false;
		updateTypes(value);
	}
	
	public void updateClearingProviders(ValueChangeEvent event) {
		long value = (Long) event.getNewValue();
		if (value == TransactionsManager.TRANS_TYPE_CUP_CHINA_DEPOSIT || value == TransactionsManager.TRANS_TYPE_CUP_CHINA_WITHDRAW) {
			showClearingProviders = true;
		} else {
			showClearingProviders = false;
		}
	}

	private void updateTypes(long classId) {
		transactionTypes.clear();
		transactionTypes.add(new SelectItem(0,""));
		if (classId == TransactionsManager.TRANS_CLASS_DEPOSIT) {
			transactionTypes.add(new SelectItem(TransactionsManager.TRANS_TYPE_CC_DEPOSIT,CommonUtil.getMessage("transactions.creditcard.deposit", null, Utils.getWriterLocale())));
			transactionTypes.add(new SelectItem(TransactionsManager.TRANS_TYPE_PAYPAL_DEPOSIT,CommonUtil.getMessage("transactions.paypal.deposit", null, Utils.getWriterLocale())));
			transactionTypes.add(new SelectItem(TransactionsManager.TRANS_TYPE_DIRECT_BANK_DEPOSIT,CommonUtil.getMessage("transactions.direct.deposit", null, Utils.getWriterLocale())));
			transactionTypes.add(new SelectItem(TransactionsManager.TRANS_TYPE_WIRE_DEPOSIT,CommonUtil.getMessage("transactions.wire.deposit", null, Utils.getWriterLocale())));
	        transactionTypes.add(new SelectItem(TransactionsManager.TRANS_TYPE_EFT_DEPOSIT,CommonUtil.getMessage("transactions.eft.deposit", null, Utils.getWriterLocale())));
			transactionTypes.add(new SelectItem(TransactionsManager.TRANS_TYPE_CASH_DEPOSIT,CommonUtil.getMessage("transactions.cash.deposit", null, Utils.getWriterLocale())));
			transactionTypes.add(new SelectItem(TransactionsManager.TRANS_TYPE_ENVOY_ONLINE_DEPOSIT,CommonUtil.getMessage("transactions.envoy.online.deposit", null, Utils.getWriterLocale())));
			transactionTypes.add(new SelectItem(TransactionsManager.TRANS_TYPE_ENVOY_BANKING_DEPOSIT,CommonUtil.getMessage("transactions.envoy.banking.deposit", null, Utils.getWriterLocale())));
		    transactionTypes.add(new SelectItem(TransactionsManager.TRANS_TYPE_ACH_DEPOSIT,CommonUtil.getMessage("transactions.ach.deposit", null, Utils.getWriterLocale())));
		    transactionTypes.add(new SelectItem(TransactionsManager.TRANS_TYPE_CASHU_DEPOSIT,CommonUtil.getMessage("transactions.cashu.deposit", null, Utils.getWriterLocale())));
		    transactionTypes.add(new SelectItem(TransactionsManager.TRANS_TYPE_UKASH_DEPOSIT,CommonUtil.getMessage("transactions.ukash.deposit", null, Utils.getWriterLocale())));
		    transactionTypes.add(new SelectItem(TransactionsManager.TRANS_TYPE_MONEYBOOKERS_DEPOSIT,CommonUtil.getMessage("transactions.moneybookers.deposit", null, Utils.getWriterLocale())));
		    transactionTypes.add(new SelectItem(TransactionsManager.TRANS_TYPE_BAROPAY_DEPOSIT,CommonUtil.getMessage("transactions.baropay.deposit", null, Utils.getWriterLocale())));
		    transactionTypes.add(new SelectItem(TransactionsManager.TRANS_TYPE_WEBMONEY_DEPOSIT,CommonUtil.getMessage("transactions.webmoney.deposit", null, Utils.getWriterLocale())));
		    transactionTypes.add(new SelectItem(TransactionsManager.TRANS_TYPE_CUP_CHINA_DEPOSIT,CommonUtil.getMessage("transactions.cup.china.deposit", null, Utils.getWriterLocale())));
		    transactionTypes.add(new SelectItem(TransactionsManager.TRANS_TYPE_INATEC_IFRAME_DEPOSIT, CommonUtil.getMessage("transaction.inatec.iframe.deposit", null, Utils.getWriterLocale())));
		    transactionTypes.add(new SelectItem(TransactionsManager.TRANS_TYPE_EPG_CHECKOUT_DEPOSIT, CommonUtil.getMessage("transaction.epg.checkout.deposit", null, Utils.getWriterLocale())));
		    transactionTypes.add(new SelectItem(TransactionsManager.TRANS_TYPE_DIRECT24_DEPOSIT, CommonUtil.getMessage("transactions.direct24.deposit", null, Utils.getWriterLocale())));
		    transactionTypes.add(new SelectItem(TransactionsManager.TRANS_TYPE_GIROPAY_DEPOSIT, CommonUtil.getMessage("transactions.giropay.deposit", null, Utils.getWriterLocale())));
		    transactionTypes.add(new SelectItem(TransactionsManager.TRANS_TYPE_EPS_DEPOSIT, CommonUtil.getMessage("transactions.eps.deposit", null, Utils.getWriterLocale())));
		} else if (classId == TransactionsManager.TRANS_CLASS_WITHDRAW) {
	        transactionTypes.add(new SelectItem(TransactionsManager.TRANS_TYPE_EFT_WITHDRAW,CommonUtil.getMessage("transactions.eft.withdraw", null, Utils.getWriterLocale())));
			transactionTypes.add(new SelectItem(TransactionsManager.TRANS_TYPE_CHEQUE_WITHDRAW,CommonUtil.getMessage("transactions.cheque.withdraw", null, Utils.getWriterLocale())));
			transactionTypes.add(new SelectItem(TransactionsManager.TRANS_TYPE_CC_WITHDRAW,CommonUtil.getMessage("transactions.cc.withdraw", null, Utils.getWriterLocale())));
			transactionTypes.add(new SelectItem(TransactionsManager.TRANS_TYPE_ENVOY_WITHDRAW,CommonUtil.getMessage("transactions.envoy.withdraw", null, Utils.getWriterLocale())));
			transactionTypes.add(new SelectItem(TransactionsManager.TRANS_TYPE_BAROPAY_WITHDRAW,CommonUtil.getMessage("transactions.baropay.withdraw", null, Utils.getWriterLocale())));
		    transactionTypes.add(new SelectItem(TransactionsManager.TRANS_TYPE_BANK_WIRE_WITHDRAW,CommonUtil.getMessage("transactions.bank.wire", null, Utils.getWriterLocale())));
		    transactionTypes.add(new SelectItem(TransactionsManager.TRANS_TYPE_PAYPAL_WITHDRAW,CommonUtil.getMessage("transactions.paypal.withdraw", null, Utils.getWriterLocale())));
		    transactionTypes.add(new SelectItem(TransactionsManager.TRANS_TYPE_MONEYBOOKERS_WITHDRAW,CommonUtil.getMessage("transactions.moneybookers.withdraw", null, Utils.getWriterLocale())));
		    transactionTypes.add(new SelectItem(TransactionsManager.TRANS_TYPE_WEBMONEY_WITHDRAW,CommonUtil.getMessage("transactions.webmoney.withdraw", null, Utils.getWriterLocale())));
		    transactionTypes.add(new SelectItem(TransactionsManager.TRANS_TYPE_CUP_CHINA_WITHDRAW,CommonUtil.getMessage("transactions.cup.china.withdraw", null, Utils.getWriterLocale())));
		    transactionTypes.add(new SelectItem(TransactionsManager.TRANS_TYPE_DIRECT24_WITHDRAW,CommonUtil.getMessage("transactions.direct24.withdraw", null, Utils.getWriterLocale())));
		    transactionTypes.add(new SelectItem(TransactionsManager.TRANS_TYPE_GIROPAY_WITHDRAW,CommonUtil.getMessage("transactions.giropay.withdraw", null, Utils.getWriterLocale())));
		    transactionTypes.add(new SelectItem(TransactionsManager.TRANS_TYPE_EPS_WITHDRAW,CommonUtil.getMessage("transactions.eps.withdraw", null, Utils.getWriterLocale())));
		    transactionTypes.add(new SelectItem(TransactionsManager.TRANS_TYPE_IDEAL_WITHDRAW,CommonUtil.getMessage("transaction.ideal.withdraw", null, Utils.getWriterLocale())));
		}
		
		Collections.sort(transactionTypes, new CommonUtil.selectItemComparator());
	}
	
	private void initStatuses() {
		transactionStatuses.add(new SelectItem(0,""));
		transactionStatuses.add(new SelectItem(TransactionsManager.TRANS_STATUS_STARTED,CommonUtil.getMessage("trans.status.started", null, Utils.getWriterLocale())));
        transactionStatuses.add(new SelectItem(TransactionsManager.TRANS_STATUS_PENDING,CommonUtil.getMessage("trans.status.pending", null, Utils.getWriterLocale())));
        transactionStatuses.add(new SelectItem(TransactionsManager.TRANS_STATUS_SUCCEED,CommonUtil.getMessage("trans.status.succeed", null, Utils.getWriterLocale())));
        transactionStatuses.add(new SelectItem(TransactionsManager.TRANS_STATUS_FAILED,CommonUtil.getMessage("trans.status.failed", null, Utils.getWriterLocale())));
        transactionStatuses.add(new SelectItem(TransactionsManager.TRANS_STATUS_REQUESTED,CommonUtil.getMessage("trans.status.requested", null, Utils.getWriterLocale())));
        transactionStatuses.add(new SelectItem(TransactionsManager.TRANS_STATUS_APPROVED,CommonUtil.getMessage("trans.status.approved", null, Utils.getWriterLocale())));
        transactionStatuses.add(new SelectItem(TransactionsManager.TRANS_STATUS_CANCELED,CommonUtil.getMessage("trans.status.canceled", null, Utils.getWriterLocale())));
        transactionStatuses.add(new SelectItem(TransactionsManager.TRANS_STATUS_CANCELED_ETRADER,CommonUtil.getMessage("trans.status.canceledbyetrader", null, Utils.getWriterLocale())));
        transactionStatuses.add(new SelectItem(TransactionsManager.TRANS_STATUS_CANCELED_FRAUDS,CommonUtil.getMessage("trans.status.canceledbyfrauds", null, Utils.getWriterLocale())));
        transactionStatuses.add(new SelectItem(TransactionsManager.TRANS_STATUS_AUTHENTICATE,CommonUtil.getMessage("trans.status.authenticate", null, Utils.getWriterLocale())));
        transactionStatuses.add(new SelectItem(TransactionsManager.TRANS_STATUS_IN_PROGRESS,CommonUtil.getMessage("trans.status.in.progress", null, Utils.getWriterLocale())));
        transactionStatuses.add(new SelectItem(TransactionsManager.TRANS_STATUS_CANCEL_S_DEPOSIT,CommonUtil.getMessage("trans.status.cancel.succ.deposit", null, Utils.getWriterLocale())));
        transactionStatuses.add(new SelectItem(TransactionsManager.TRANS_STATUS_CANCEL_M_N_R,CommonUtil.getMessage("trans.status.cancel.m.n.r", null, Utils.getWriterLocale())));
        transactionStatuses.add(new SelectItem(TransactionsManager.TRANS_STATUS_CANCEL_S_M,CommonUtil.getMessage("trans.status.cancel.s.m", null, Utils.getWriterLocale())));
        transactionStatuses.add(new SelectItem(TransactionsManager.TRANS_STATUS_CHARGE_BACK,CommonUtil.getMessage("trans.status.charge.back", null, Utils.getWriterLocale())));
        transactionStatuses.add(new SelectItem(TransactionsManager.TRANS_STATUS_SECOND_APPROVAL,CommonUtil.getMessage("trans.status.second.approval", null, Utils.getWriterLocale())));
        transactionStatuses.add(new SelectItem(TransactionsManager.TRANS_STATUS_REVERSE_WITHDRAW,CommonUtil.getMessage("trans.status.reverse", null, Utils.getWriterLocale())));
	}
	
	private void initClearingProviders() {
		clearingProviders.add(new SelectItem(0,""));
		clearingProviders.add(new SelectItem(ClearingManager.DELTAPAY_CHINA_PROVIDER_ID, CommonUtil.getMessage("clearing.provider.deltapay", null, Utils.getWriterLocale())));
		clearingProviders.add(new SelectItem(ClearingManager.CDPAY_PROVIDER_ID, CommonUtil.getMessage("clearing.provider.cdpay", null, Utils.getWriterLocale())));
	}

	public String updateTransactionCases() throws SQLException {
		FacesContext context = FacesContext.getCurrentInstance();
		FacesMessage fm = null;
		FacesMessage fm2 = null;
		Transaction t = TransactionsManager.getTransaction(transactionId);
		if (CommonUtil.isEpgProvider(t.getClearingProviderId())) {
			fm = new FacesMessage(FacesMessage.SEVERITY_ERROR, CommonUtil.getMessage("error.epg.transaction", null), null);
			context.addMessage(null,fm);
			return null;
		}
		transactionUpdateStatus updateStatus = null;
		String captureCodeToUpdate = withdraw ? captureCode : null;
		if (showClearingProviders == true) {
			updateStatus = TransactionsManager.updateTransaction(transactionId, Utils.getWriter().getWriter().getId(), typeId, statusId, providerId, captureCodeToUpdate, classId);
		} else {
			updateStatus = TransactionsManager.updateTransaction(transactionId, Utils.getWriter().getWriter().getId(), typeId, statusId, captureCodeToUpdate, classId);
		}
		switch (updateStatus) {
		case TRANS_BALANCE_UPDATED:
			fm = new FacesMessage(FacesMessage.SEVERITY_INFO, CommonUtil.getMessage("general.update.success", null), null);
			break;
		case TRANS_UPDATED_BALANCE_FAILED:
			fm = new FacesMessage(FacesMessage.SEVERITY_INFO, CommonUtil.getMessage("general.update.success", null), null);
			fm2 = new FacesMessage(FacesMessage.SEVERITY_WARN, CommonUtil.getMessage("transaction.update.balance.failed", null), null);
			break;
		case TRANS_UPDATED:
			fm = new FacesMessage(FacesMessage.SEVERITY_INFO, CommonUtil.getMessage("general.update.success", null), null);
			break;
		case TRANS_FAILED:
			fm = new FacesMessage(FacesMessage.SEVERITY_ERROR, CommonUtil.getMessage("transaction.update.problem", null), null);
			break;
		default:
			break;
		}
		
		context.addMessage(null,fm);
		if (fm2 != null) {
			context.addMessage(null,fm2);
		}
		return null;
	}
	
	public ArrayList<SelectItem> getClassIdSI() throws SQLException {
		ArrayList<SelectItem> list = new ArrayList<SelectItem>();
		list.add(new SelectItem(TransactionsManager.TRANS_CLASS_DEPOSIT, CommonUtil.getMessage("trans.update.class.deposit", null, Utils.getWriterLocale())));
		list.add(new SelectItem(TransactionsManager.TRANS_CLASS_WITHDRAW, CommonUtil.getMessage("trans.update.class.withdraw", null, Utils.getWriterLocale())));
		return list;
	}
	
	public Long getTransactionId() {
		return transactionId;
	}
	
	public void setTransactionId(Long transactionId) {
		this.transactionId = transactionId;
	}
	
	public String getCaptureCode() {
		return captureCode;
	}
	
	public void setCaptureCode(String captureCode) {
		this.captureCode = captureCode;
	}

	public long getStatusId() {
		return statusId;
	}

	public void setStatusId(long statusId) {
		this.statusId = statusId;
	}

	public long getClassId() {
		return classId;
	}

	public void setClassId(long classId) {
		this.classId = classId;
	}

	public List<SelectItem> getTransactionStatuses() {
		return transactionStatuses;
	}

	public void setTransactionStatuses(List<SelectItem> transactionStatuses) {
		this.transactionStatuses = transactionStatuses;
	}

	public long getTypeId() {
		return typeId;
	}

	public void setTypeId(long typeId) {
		this.typeId = typeId;
	}

	public List<SelectItem> getTransactionTypes() {
		return transactionTypes;
	}

	public void setTransactionTypes(List<SelectItem> transactionTypes) {
		this.transactionTypes = transactionTypes;
	}

	public boolean isWithdraw() {
		return withdraw;
	}

	public void setWithdraw(boolean withdraw) {
		this.withdraw = withdraw;
	}

	public long getProviderId() {
		return providerId;
	}

	public void setProviderId(long providerId) {
		this.providerId = providerId;
	}

	public List<SelectItem> getClearingProviders() {
		return clearingProviders;
	}

	public void setClearingProviders(List<SelectItem> clearingProviders) {
		this.clearingProviders = clearingProviders;
	}

	public boolean isShowClearingProviders() {
		return showClearingProviders;
	}

	public void setShowClearingProviders(boolean showClearingProviders) {
		this.showClearingProviders = showClearingProviders;
	}
}