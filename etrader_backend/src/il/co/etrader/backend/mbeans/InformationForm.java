package il.co.etrader.backend.mbeans;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;

import il.co.etrader.backend.bl_managers.InvestmentsManager;
import il.co.etrader.backend.bl_managers.PopulationsManager;
import il.co.etrader.backend.bl_managers.TransactionsManager;
import il.co.etrader.backend.bl_vos.User;
import il.co.etrader.backend.util.Utils;
import il.co.etrader.bl_vos.Population;
import il.co.etrader.util.CommonUtil;

public class InformationForm implements Serializable{

	/**
	 *
	 */
	private static final long serialVersionUID = 7289255460012736486L;
	private User user;
	private String currentPopulation;
	private String priviousPopulation;
	private long populationEntryStatusId;
	private String sumOfDeposits;
	private Date sumOfDepositsDateFrom;
	private Date sumOfDepositsDateTo;
	private String sumOfInvestments;
	private Date sumOfInvestmentsDateFrom;
	private Date sumOfInvestmentsDateTo;
	private String firstAsset;
	private String secondAsset;
	private String thirdAsset;
	private String isControlGroup;

	public InformationForm() throws Exception {
		user = Utils.getUser();
		ArrayList<Population> populationsList = PopulationsManager.getPopulationListByUserId(user.getId());
		if (null != populationsList && populationsList.size() > 0) {
			if (populationsList.size() == 1) {
				currentPopulation = populationsList.get(0).getName();
				priviousPopulation = CommonUtil.getMessage("general.none", null);
			} else {
				currentPopulation = populationsList.get(0).getName();
				priviousPopulation = populationsList.get(1).getName();
			}
			populationEntryStatusId = populationsList.get(0).getEntryStatusId();
		} else {
			currentPopulation = CommonUtil.getMessage("general.none", null);
			priviousPopulation = CommonUtil.getMessage("general.none", null);
		}

		sumOfDeposits = user.getDepositsTxt();
		sumOfDepositsDateFrom = user.getTimeCreated();
		sumOfDepositsDateTo = new Date();
		sumOfInvestments = user.getTotalInvestments();
		sumOfInvestmentsDateFrom = user.getTimeCreated();
		sumOfInvestmentsDateTo =  new Date();
		ArrayList<String> assetsList = new ArrayList<String>();
		assetsList = InvestmentsManager.getMarketAndSumOfInvestments(user.getId());
		if (assetsList.size() == 0) {
			firstAsset = CommonUtil.getMessage("general.none", null);
			secondAsset = CommonUtil.getMessage("general.none", null);
			thirdAsset = CommonUtil.getMessage("general.none", null);
		} else {
			if (assetsList.size() == 1) {
				firstAsset = assetsList.get(0);
				secondAsset = CommonUtil.getMessage("general.none", null);
				thirdAsset = CommonUtil.getMessage("general.none", null);
			} else {
				if (assetsList.size() == 2) {
					firstAsset = assetsList.get(0);
					secondAsset = assetsList.get(1);
					thirdAsset = CommonUtil.getMessage("general.none", null);
				} else {
					firstAsset = assetsList.get(0);
					secondAsset = assetsList.get(1);
					thirdAsset = assetsList.get(2);
				}
			}
		}
		isControlGroup = CommonUtil.getMessage("menu.retention.information.is.control.group.no", null);;
		if (null != populationsList && populationsList.get(0).getTimeControl() != null) {
			isControlGroup = CommonUtil.getMessage("menu.retention.information.is.control.group.yes", null);;
		}
	}

	public String sumDeposits() throws Exception {
		sumOfDeposits = CommonUtil.displayAmount(TransactionsManager.getSumDepositByDateRangeAndUserId(user.getId(), sumOfDepositsDateFrom, sumOfDepositsDateTo), user.getCurrencyId());
		return null;
	}

	public String sumInvestments() throws Exception {
		sumOfInvestments = CommonUtil.displayAmount(InvestmentsManager.getSumInvestmentsByDateRangeAndUserId(user.getId(), sumOfInvestmentsDateFrom, sumOfInvestmentsDateTo), user.getCurrencyId());
		return null;
	}
	/**
	 * @return the currentPopulation
	 */
	public String getCurrentPopulation() {
		return currentPopulation;
	}
	/**
	 * @param currentPopulation the currentPopulation to set
	 */
	public void setCurrentPopulation(String currentPopulation) {
		this.currentPopulation = currentPopulation;
	}
	/**
	 * @return the priviousPopulation
	 */
	public String getPriviousPopulation() {
		return priviousPopulation;
	}
	/**
	 * @param priviousPopulation the priviousPopulation to set
	 */
	public void setPriviousPopulation(String priviousPopulation) {
		this.priviousPopulation = priviousPopulation;
	}
	/**
	 * @return the sumOfDeposits
	 */
	public String getSumOfDeposits() {
		return sumOfDeposits;
	}
	/**
	 * @param sumOfDeposits the sumOfDeposits to set
	 */
	public void setSumOfDeposits(String sumOfDeposits) {
		this.sumOfDeposits = sumOfDeposits;
	}
	/**
	 * @return the sumOfInvestments
	 */
	public String getSumOfInvestments() {
		return sumOfInvestments;
	}
	/**
	 * @param sumOfInvestments the sumOfInvestments to set
	 */
	public void setSumOfInvestments(String sumOfInvestments) {
		this.sumOfInvestments = sumOfInvestments;
	}

	/**
	 * @return the sumOfDepositsDateFrom
	 */
	public Date getSumOfDepositsDateFrom() {
		return sumOfDepositsDateFrom;
	}

	/**
	 * @param sumOfDepositsDateFrom the sumOfDepositsDateFrom to set
	 */
	public void setSumOfDepositsDateFrom(Date sumOfDepositsDateFrom) {
		this.sumOfDepositsDateFrom = sumOfDepositsDateFrom;
	}

	/**
	 * @return the sumOfDepositsDateTo
	 */
	public Date getSumOfDepositsDateTo() {
		return sumOfDepositsDateTo;
	}

	/**
	 * @param sumOfDepositsDateTo the sumOfDepositsDateTo to set
	 */
	public void setSumOfDepositsDateTo(Date sumOfDepositsDateTo) {
		this.sumOfDepositsDateTo = sumOfDepositsDateTo;
	}

	/**
	 * @return the sumOfInvestmentsDateFrom
	 */
	public Date getSumOfInvestmentsDateFrom() {
		return sumOfInvestmentsDateFrom;
	}

	/**
	 * @param sumOfInvestmentsDateFrom the sumOfInvestmentsDateFrom to set
	 */
	public void setSumOfInvestmentsDateFrom(Date sumOfInvestmentsDateFrom) {
		this.sumOfInvestmentsDateFrom = sumOfInvestmentsDateFrom;
	}

	/**
	 * @return the sumOfInvestmentsDateTo
	 */
	public Date getSumOfInvestmentsDateTo() {
		return sumOfInvestmentsDateTo;
	}

	/**
	 * @param sumOfInvestmentsDateTo the sumOfInvestmentsDateTo to set
	 */
	public void setSumOfInvestmentsDateTo(Date sumOfInvestmentsDateTo) {
		this.sumOfInvestmentsDateTo = sumOfInvestmentsDateTo;
	}

	/**
	 * @return the firstAsset
	 */
	public String getFirstAsset() {
		return firstAsset;
	}

	/**
	 * @param firstAsset the firstAsset to set
	 */
	public void setFirstAsset(String firstAsset) {
		this.firstAsset = firstAsset;
	}

	/**
	 * @return the secondAsset
	 */
	public String getSecondAsset() {
		return secondAsset;
	}

	/**
	 * @param secondAsset the secondAsset to set
	 */
	public void setSecondAsset(String secondAsset) {
		this.secondAsset = secondAsset;
	}

	/**
	 * @return the thirdAsset
	 */
	public String getThirdAsset() {
		return thirdAsset;
	}

	/**
	 * @param thirdAsset the thirdAsset to set
	 */
	public void setThirdAsset(String thirdAsset) {
		this.thirdAsset = thirdAsset;
	}

	/**
	 * @return the populationEntryStatusId
	 */
	public long getPopulationEntryStatusId() {
		return populationEntryStatusId;
	}

	/**
	 * @param populationEntryStatusId the populationEntryStatusId to set
	 */
	public void setPopulationEntryStatusId(long populationEntryStatusId) {
		this.populationEntryStatusId = populationEntryStatusId;
	}

	public String getPopulationEntryStatusTxt() {
		String statusName = "";
		switch ((int)this.populationEntryStatusId) {
			case 1:
				statusName = CommonUtil.getMessage("population.entry.status.active", null);
				break;
			case 2:
				statusName = CommonUtil.getMessage("population.entry.status.sleeper", null);
				break;
			case 3:
				statusName = CommonUtil.getMessage("population.entry.status.coma", null);
				break;
		}
		return statusName;
	}

	/**
	 * @return the isControlGroup
	 */
	public String getIsControlGroup() {
		return isControlGroup;
	}

	/**
	 * @param isControlGroup the isControlGroup to set
	 */
	public void setIsControlGroup(String isControlGroup) {
		this.isControlGroup = isControlGroup;
	}

}
