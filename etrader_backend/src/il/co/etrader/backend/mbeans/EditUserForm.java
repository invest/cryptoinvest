package il.co.etrader.backend.mbeans;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.base.UserRegulationBase;
import com.anyoption.common.beans.base.UsersDetailsHistory;
import com.anyoption.common.managers.BubblesManager;
import com.anyoption.common.managers.TransactionsManagerBase;
import com.anyoption.common.managers.UserRegulationManager;
import com.anyoption.common.managers.UsersManagerBase;
//import com.copyop.common.enums.base.UserStateEnum;
//import com.copyop.common.managers.ProfileManager;

import il.co.etrader.backend.bl_managers.AdminManager;
import il.co.etrader.backend.bl_managers.UsersManager;
import il.co.etrader.backend.bl_vos.User;
import il.co.etrader.backend.bl_vos.WriterWrapper;
import il.co.etrader.backend.util.Constants;
import il.co.etrader.backend.util.Utils;
import il.co.etrader.bl_managers.AdminManagerBase;
import il.co.etrader.bl_managers.PopulationsManagerBase;
import il.co.etrader.bl_managers.SMSManagerBase;
import il.co.etrader.bl_vos.UserMarketDisable;
import il.co.etrader.util.CommonUtil;
import il.co.etrader.util.ConstantsBase;

public class EditUserForm implements Serializable {

	private static final long serialVersionUID = 8946202003512395827L;

	private static final Logger logger = Logger.getLogger(EditUserForm.class);

	private String userName;
	private String oldPassword;
	private String password;
	private String password2;
	private Long currencyId;
	private String firstName;
	private String lastName;
	private String street;
	private String streetNo;
	private long cityId;
	private String zipCode;
	private long countryId;;
	private String email;
	private String birthYear;
	private String birthMonth;
	private String birthDay;
	private boolean contactByEmail;
	private String mobileCountryPhoneCode;
	private String mobilePhone;
	private String landlineCountryPhoneCode;
	private String landLinePhone;
	private String gender;
	private String idNum;
	private boolean terms;
	//protected int isContactByEmail;
	protected long combId;
	protected long classId;
	protected String comments;
	protected String isActive;
	protected long limitId;
	private String cityName;
	private ArrayList usersList;
	private boolean sendPassword;
    protected String companyName;
    protected BigDecimal companyId;
    protected boolean taxExemption;
    protected boolean contactBySMS;
    protected BigDecimal referenceId;
    protected long languageId;
    protected int skinId;
    private int isNextInvestOnUs;
    private int sendDepositDocs;
    protected Long state;
    protected boolean contactByPhone;
    protected boolean isFalseAccount;
    protected boolean isVip;
    private boolean authorizedMail;
    protected boolean isRisky;
    protected boolean isUserDisable;
    private int specialCareManualy;
    private int oldSpecialCareManualy;
    private boolean isStopReverseWithdrawOption;
    private boolean isImmediateWithdraw;
    private Double defaultAmountValue;
    private long balanceStepId;
    private List<SelectItem> balanceStepsSI;
    private String copyopNickname;
    private boolean copyopBlock;
    private boolean copyopRemove;
    private boolean regularReportMail;
    private boolean oldRegularReportMail;
    private long bubblesPricingLevel;
    private boolean isDisableCcDeposits;
    private String changedSentDepositDoc;
    private boolean showCancelInvestment;
    
	public EditUserForm() throws SQLException {

		FacesContext context = FacesContext.getCurrentInstance();
		User user = (User) context.getApplication().createValueBinding(Constants.BIND_USER).getValue(context);

		if (user.getId() > 0) {

			UsersManager.loadUserStripFields(user, true, null);

			Date birth=user.getTimeBirthDate();
			if(birth!=null) {
				this.setBirthDay(user.getBirthDay());
				this.setBirthMonth(user.getBirthMonth());
				this.setBirthYear(user.getBirthYear());
			}
			this.skinId=new Long(user.getSkinId()).intValue();
			this.languageId = user.getLanguageId();

			this.setIsActive(user.getIsActive());
			this.setFirstName(user.getFirstName());
			this.setLastName(user.getLastName());
			this.setStreet(user.getStreet());
			this.setStreetNo(user.getStreetNo());
			this.cityName = ((isEtrader() && user.getCityId() != 0) ? AdminManagerBase.getCityById(user.getCityId()).getName() : user.getCityName());
			this.setZipCode(user.getZipCode());
			this.setCountryId(user.getCountryId());
			this.setEmail(user.getEmail());
			this.setContactByEmail(user.getIsContactByEmail()==1);

			this.setMobilePhone(user.getMobilePhone());
			this.setLandLinePhone(user.getLandLinePhone());

			this.setGender(user.getGender());
			this.setIdNum(user.getIdNum());
			this.setComments(user.getComments());
			this.setCurrencyId(user.getCurrencyId());
			this.setClassId(user.getClassId());
			this.setCombId(user.getCombinationId());
			this.setLimitId(user.getLimitId());
			this.setIsNextInvestOnUs(String.valueOf(user.getIsNextInvestOnUs()));
			this.setContactBySMS(user.isContactBySMS());
			this.setContactByPhone(user.isContactByPhone());
			this.setUserName(user.getUserName());
			this.authorizedMail = user.isAuthorizedMail();
			this.setRisky(user.isRisky());
			this.isUserDisable = user.isUserDisable();
			this.setStopReverseWithdrawOption(user.isStopReverseWithdrawOption());
			this.setImmediateWithdraw(user.isImmediateWithdraw());
			UserRegulationBase ur = new UserRegulationBase();
			ur.setUserId(user.getId());
			UserRegulationManager.getUserRegulation(ur);
			this.setOldRegularReportMail(ur.isRegularReportMail());
			this.setRegularReportMail(ur.isRegularReportMail());
//			if(user.getUserActiveData().getCopyopUserStatus() == UserStateEnum.STATE_BLOCKED){
//				this.setCopyopBlock(true);
//			}else{
//				this.setCopyopBlock(false);
//			}
//			
//			if(user.getUserActiveData().getCopyopUserStatus() == UserStateEnum.STATE_REMOVED){
//				this.setCopyopRemove(true);
//			}else{
//				this.setCopyopRemove(false);
//			}

			companyName=user.getCompanyName();
			companyId=user.getCompanyId();
			taxExemption=user.isTaxExemption();
			referenceId=user.getReferenceId();

			if (user.isIdDocVerify()) {  // deposit documents
				this.setSendDepositDocs(1);
			}
			else {
				this.setSendDepositDocs(0);
			}

            if (user.getSpecialCare().isSpecialCareManual() || user.getSpecialCare().isSpecialCareRed() || user.getSpecialCare().isSpecialCareGreen()) {
                this.setSpecialCareManualy(1);
                this.setOldSpecialCareManualy(1);
            }
            else {
                this.setSpecialCareManualy(0);
                this.setOldSpecialCareManualy(0);
            }

			this.isFalseAccount=user.isFalseAccount();
			this.isVip = user.isVip();
			Long stateId = user.getState();
			if (this.getCountryId() != Constants.COUNTRY_ID_US ) {
				stateId = new Long(0);
	    	}
			this.setState(stateId);
			this.bubblesPricingLevel = user.getUserActiveData().getBubblesPricingLevel();
			
//			try {
//    			user.setProfile(ProfileManager.getProfile(user.getId()));
//    			if (user.getProfile() != null) {
//    				this.copyopNickname = user.getProfile().getNickname();
//    			}
//    		} catch (Exception e) {
//    			logger.error("Unable to load profile with userId = " + user.getId(), e);
//    		} 
			
			this.isDisableCcDeposits = TransactionsManagerBase.isDisableCcDeposits(user.getId());
			
			if(user.getSentDepDocumentsDateTime() != null){
				changedSentDepositDoc = "Changed by " + Utils.getWriterName(user.getSentDepDocumentsWriterId()) + " "  
							+ CommonUtil.getDateTimeFormatForLiveAO(user.getSentDepDocumentsDateTime(), null, " ");
				
			}
			
			// one click user params
//			balanceStepId = BalanceStepsManager.getUserBalanceStep(user.getId());
//			balanceStepsSI = new ArrayList<SelectItem>();
//			defaultAmountValue = BalanceStepsManager.getUser
			this.setShowCancelInvestment(user.isShowCancelInvestment());
		}

	}

	public void updateClass(ValueChangeEvent ev) {
		FacesContext context = FacesContext.getCurrentInstance();
		if (ev.getNewValue()==null) {
			setClassId((Long)ev.getOldValue());
		} else {
			setClassId((Long)ev.getNewValue());
		}
	}

	/**
	 * Check if user have deposits
	 * @return
	 * @throws SQLException
	 */
	public boolean isHaveDeposits() throws SQLException {
		FacesContext context = FacesContext.getCurrentInstance();
		User user = (User) context.getApplication().createValueBinding(Constants.BIND_USER).getValue(context);
		return UsersManager.isHaveDeposits(user.getId());
	}

	/**
	 * Check if user have approved deposits
	 * @return
	 * @throws SQLException
	 */
	public boolean isHaveApprovedDeposits() throws SQLException {
		FacesContext context = FacesContext.getCurrentInstance();
		User user = (User) context.getApplication().createValueBinding(Constants.BIND_USER).getValue(context);
		return UsersManager.isHaveApprovedDeposits(user.getId());
	}
	
    public boolean isHaveApprovedAllClassTypeDeposits() throws SQLException {
        FacesContext context = FacesContext.getCurrentInstance();
        User user = (User) context.getApplication().createValueBinding(Constants.BIND_USER).getValue(context);
        return UsersManager.isHaveApprovedDeposits(user.getId(), true);
    }	
	
    public boolean isFirstPossibleDeposit() throws SQLException {
        FacesContext context = FacesContext.getCurrentInstance();
        User user = (User) context.getApplication().createValueBinding(Constants.BIND_USER).getValue(context);
        return TransactionsManagerBase.isFirstPossibleDeposit(user.getId());        
    }
    
    private boolean isPossibleChangeCurr(long userId, Long oldCurrencyId, Long newCurrencyId){
        boolean res = true;
        if(oldCurrencyId != newCurrencyId){
            logger.debug("Currency for userId: " + userId + " was changed from:" + oldCurrencyId + " to:" + newCurrencyId);
            try {
                if (!TransactionsManagerBase.isFirstPossibleDeposit(userId)){
                    res = false;
                    logger.debug("Currency for userId: " + userId + " can't to change!!! exist deposit transaction");
                    
                    FacesContext context = FacesContext.getCurrentInstance();
                    FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR,CommonUtil.getMessage("register.error.currency.change", null),null);
                    context.addMessage(null, fm);                    
                }
            } catch (Exception e) {
                logger.error("When check isPossibleChangeCurr ", e);
            }
        }
        return res;        
    }

//	/**
//	 * Operate after currency change
//	 * @param ev
//	 * @throws SQLException
//	 */
//	public void updateLimit(ValueChangeEvent ev) throws SQLException {
//		long currency = (Long)ev.getNewValue();
//		this.limitId = String.valueOf(UsersManager.getLimitId(skinId, currency));
//	}

	/**
	 * @return the userName
	 */
	public String getUserName() {
		return userName;
	}

	/**
	 * @param userName
	 *            the userName to set
	 */
	public void setUserName(String userName) {
		this.userName = userName;
	}

	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * @param password
	 *            the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * @return the password2
	 */
	public String getPassword2() {
		return password2;
	}

	/**
	 * @param password2
	 *            the password2 to set
	 */
	public void setPassword2(String password2) {
		this.password2 = password2;
	}

	/**
	 * @return the currencyId
	 */
	public long getCurrencyId() {
		return currencyId;
	}

	/**
	 * @param currencyId
	 *            the currencyId to set
	 */
	public void setCurrencyId(long currencyId) {
		this.currencyId = currencyId;
	}

	/**
	 * @return the firstName
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * @param firstName
	 *            the firstName to set
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	/**
	 * @return the lastName
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * @param lastName
	 *            the lastName to set
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	/**
	 * @return the street
	 */
	public String getStreet() {
		return street;
	}

	/**
	 * @param street
	 *            the street to set
	 */
	public void setStreet(String street) {
		this.street = street;
	}

	/**
	 * @return the cityId
	 */

	public long getCityId() {

		return cityId;
	}

	public void setCityId(long id) {

		this.cityId = id;

	}

	public String getCityName() {
		return cityName;
	}

	public void setCityName(String cityName) throws SQLException {
		if (isEtrader()) {
			cityId = AdminManagerBase.getCityIdByName(cityName);
		}
		this.cityName = cityName;
	}

	/**
	 * @return the zipCode
	 */
	public String getZipCode() {
		return zipCode;
	}

	/**
	 * @param zipCode
	 *            the zipCode to set
	 */
	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}

	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * @param email
	 *            the email to set
	 */
	public void setEmail(String email) {
		this.email = email.trim();
	}

	/**
	 * @return the birthYear
	 */
	public String getBirthYear() {
		return birthYear;
	}

	/**
	 * @param birthYear
	 *            the birthYear to set
	 */
	public void setBirthYear(String birthYear) {
		this.birthYear = birthYear;
	}

	/**
	 * @return the birthMonth
	 */
	public String getBirthMonth() {
		return birthMonth;
	}

	/**
	 * @param birthMonth
	 *            the birthMonth to set
	 */
	public void setBirthMonth(String birthMonth) {
		this.birthMonth = birthMonth;
	}

	/**
	 * @return the birthDay
	 */
	public String getBirthDay() {
		return birthDay;
	}

	/**
	 * @param birthDay
	 *            the birthDay to set
	 */
	public void setBirthDay(String birthDay) {
		this.birthDay = birthDay;
	}

	/**
	 * @return the contactByEmail
	 */
	public boolean isContactByEmail() {
		return contactByEmail;
	}


	/**
	 * @return the mobileCountryPhoneCode
	 */
	public String getMobileCountryPhoneCode() {
		return mobileCountryPhoneCode;
	}

	/**
	 * @param mobileCountryPhoneCode the mobileCountryPhoneCode to set
	 */
	public void setMobileCountryPhoneCode(String mobileCountryPhoneCode) {
		this.mobileCountryPhoneCode = mobileCountryPhoneCode;
	}

	/**
	 * @return the mobilePhone
	 */
	public String getMobilePhone() {
		return mobilePhone;
	}

	/**
	 * @param mobilePhone
	 *            the mobilePhone to set
	 */
	public void setMobilePhone(String mobilePhone) {
		this.mobilePhone = mobilePhone;
	}



	/**
	 * @return the landLineCountryPhoneCode
	 */
	public String getLandlineCountryPhoneCode() {
		return landlineCountryPhoneCode;
	}

	/**
	 * @param landLineCountryPhoneCode the landLineCountryPhoneCode to set
	 */
	public void setLandlineCountryPhoneCode(String landlineCountryPhoneCode) {
		this.landlineCountryPhoneCode = landlineCountryPhoneCode;
	}

	/**
	 * @return the landLinePhone
	 */
	public String getLandLinePhone() {
		return landLinePhone;
	}

	/**
	 * @param landLinePhone
	 *            the landLinePhone to set
	 */
	public void setLandLinePhone(String landLinePhone) {
		this.landLinePhone = landLinePhone;
	}

	/**
	 * @return the gender
	 */
	public String getGender() {
		return gender;
	}

	/**
	 * @param gender
	 *            the gender to set
	 */
	public void setGender(String gender) {
		this.gender = gender;
	}

	/**
	 * @return the idNum
	 */
	public String getIdNum() {
		return idNum;
	}

	/**
	 * @param idNum
	 *            the idNum to set
	 */
	public void setIdNum(String idNum) {
		if (idNum.isEmpty() || idNum == null) {
			this.idNum = ConstantsBase.NO_ID_NUM;
		} else {
			this.idNum = idNum;
		}
	}

	/**
	 * @return the terms
	 */
	public boolean isTerms() {
		return terms;
	}

	/**
	 * @param terms
	 *            the terms to set
	 */
	public void setTerms(boolean terms) {
		this.terms = terms;
	}

	public String getOldPassword() {
		return oldPassword;
	}

	public void setOldPassword(String oldPassword) {
		this.oldPassword = oldPassword;
	}

	public ArrayList getUsersList() throws SQLException {

		// if (usersList==null)
		// usersList=UsersManager.searchUser(this);

		return usersList;
	}

	public void setUsersList(ArrayList usersList) {
		this.usersList = usersList;
	}

	public int getListSize() throws SQLException {

		// if (usersList==null)
		// getUsersList();

		return usersList.size();
	}


	public String getStreetNo() {
		return streetNo;
	}

	public void setStreetNo(String streetNo) {
		this.streetNo = streetNo;
	}

	public boolean isSendPassword() {
		return sendPassword;
	}

	public void setSendPassword(boolean sendPassword) {
		this.sendPassword = sendPassword;
	}

	public void setCurrencyId(Long currencyId) {
		this.currencyId = currencyId;
	}

	public String getIsActive() {
		return isActive;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public ArrayList getLimits() throws SQLException {
		return AdminManager.getLimits();
	}

    /**
     * Get limits list by writer skins
     * @return
     * @throws SQLException
     */
    public ArrayList getLimitsBySkins() throws SQLException{
    	return AdminManager.getLimitsBySkins();
    }

	public long getCombId() {
		return combId;
	}

	public void setCombId(long combId) {
		this.combId = combId;
	}

	public long getClassId() {
		return classId;
	}

	public void setClassId(long classId) {
		this.classId = classId;
	}

	public void setIsActive(String isActive) {
		this.isActive = isActive;
	}

	public String updateUser() throws Exception {

		logger.debug(this);
		String isActiveOld = null;

		FacesContext context = FacesContext.getCurrentInstance();
		User user = (User) context.getApplication().createValueBinding(Constants.BIND_USER).getValue(context);
		isActiveOld = user.getIsActive();

		if (!CommonUtil.isHebrewSkin(getSkinId())) { // for anyoption
			user.setMobilePhone(user.handleLeadingZero(user.getMobilePhone()));
			user.setLandLinePhone(user.handleLeadingZero(user.getLandLinePhone()));
		}
		user.getSpecialCare().setScUpdatedBy((int)Utils.getWriter().getWriter().getId());
		if (this.getCountryId() != Constants.COUNTRY_ID_US ) {
			this.setState(0L);
    	}
        if (oldSpecialCareManualy == specialCareManualy){}
        // change user details.
        boolean isDev3Before = false;
        boolean isDev3After = false;
        if (user.isUserDisable() != this.isUserDisable) {
            if (this.isUserDisable) {
                isDev3Before = false;
                isDev3After = true;
            }
        }
        // Password change is done through different form and there is no need to check for password change
        this.setPassword(user.getPassword());
        HashMap<Long, String> userDetailsBeforeChangedHM = UsersManager.getUserDetailsHM(user.getSkinId(), user.getStreet(), user.getCountryId(), user.getGender(), user.getEmail(), user.getMobilePhone(), 
                user.getLandLinePhone(), user.getContactByEmail(), user.getContactBySMS(), user.getBirthDay(), user.getBirthMonth(), user.getBirthYear(), user.getCityName(), user.getIdNum(), user.getState(), 
                user.getFirstName(), user.getLastName(), user.getPassword(), null, user.getLanguageId(), String.valueOf(user.getClassId()), CommonUtil.getBooleanAsString(user.isIdDocVerify()), user.isAuthorizedMail(), user.isRisky(), this.getOldSpecialCareManualy(), 
                user.isStopReverseWithdrawOption(), user.isImmediateWithdraw(), isDev3Before, false, user.getUserName(), false, user.getUserActiveData().getBubblesPricingLevel());
        HashMap<Long, String> userDetailsAfterChangedHM = UsersManager.getUserDetailsHM((long)this.getSkinId(), this.getStreet(), this.getCountryId(), this.getGender(), this.getEmail(), this.getMobilePhone(), 
                this.getLandLinePhone(), this.isContactByEmail(), this.isContactBySMS(), this.getBirthDay(), this.getBirthMonth(), this.getBirthYear(), this.getCityName(), this.getIdNum(), this.getState(), 
                this.getFirstName(), this.getLastName(), this.getPassword(), null, this.getLanguageId(), String.valueOf(this.getClassId()), String.valueOf(this.getSendDepositDocs()), this.isAuthorizedMail(), this.isRisky(), this.getSpecialCareManualy(), 
                this.isStopReverseWithdrawOption(), this.isImmediateWithdraw(), isDev3After, false, this.getUserName(), false, this.bubblesPricingLevel);        
        ArrayList<UsersDetailsHistory> usersDetailsHistoryList = UsersManager.checkUserDetailsChange(userDetailsBeforeChangedHM, userDetailsAfterChangedHM);
        boolean verifyPhone = (!user.getMobilePhone().equals(mobilePhone) || user.getCountryId() != countryId);
        // TODO take care of updating the one click user params
        boolean res = UsersManager.updateUserDetails(user, this);
        if (regularReportMail != oldRegularReportMail) {
            UserRegulationBase userRegulation = new UserRegulationBase();
            userRegulation.setUserId(user.getId());
            userRegulation.setRegularReportMail(this.regularReportMail);
        	UserRegulationManager.updateRegularReportMail(userRegulation);
        	user.setUserRegulation(userRegulation);
        }

		if (res == true) {
			long writerId = Utils.getWriter().getWriter().getId();
			if (null != usersDetailsHistoryList) {
				UsersManagerBase.insertUsersDetailsHistory(writerId, user.getId(), user.getUserName(), usersDetailsHistoryList, String.valueOf(user.getClassId()));
			}
			if (verifyPhone) {
				SMSManagerBase.sendPhoneNumberVerificationMessage(user, user.getLocale(), user.getPlatformId());
			}
			try {
				if (user.isUserDisable() != this.isUserDisable) {
					if (this.isUserDisable) { //insert new
						UserMarketDisable umd = UsersMarketDisableForm.initUserMarketDisable();
						umd = UsersMarketDisableForm.setUserMarketDisableEndDate(umd);
						umd.setDev3(true);
						umd.setUserId(user.getId());
						umd.setMarketId(0);
						umd.setScheduled(0);
						umd.setActive(true);
						UsersManager.insertUpdateUsersMarketDisable(umd);
					} else { //update to remove all dev 3 for this user
						UsersManager.removeUsersMarketDisableByUserId(user.getId());
					}
				}
			} catch (Exception e1) {
				logger.warn("cant update user disable", e1);
			}
			UsersManager.loadUserStripFields(user, true, null);
			try {
				if (isActiveOld.equals(Constants.USER_ACTIVE) &&
						user.getIsActive().equals(Constants.USER_NOT_ACTIVE)) {
					PopulationsManagerBase.closeAccount(user.getId(), AdminManager.getWriterId());
				}
			} catch (Exception e) {
				logger.warn("Problem with population closeAccount event ", e);
			}
			return Utils.navAfterUserOperation();
		} else
			return null;
	}

	public Long getLimitId() {
		return limitId;
	}

	public void setLimitId(Long limId) {
		this.limitId = limId;
	}

	public BigDecimal getCompanyId() {
		return companyId;
	}

	public void setCompanyId(BigDecimal companyId) {
		this.companyId = companyId;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public BigDecimal getReferenceId() {
		return referenceId;
	}

	public void setReferenceId(BigDecimal referenceId) {
		this.referenceId = referenceId;
	}

	public boolean isTaxExemption() {
		return taxExemption;
	}

	public void setTaxExemption(boolean taxExemption) {
		this.taxExemption = taxExemption;
	}

	/**
	 * @return the isNextInvestOnUs
	 */
	public String getIsNextInvestOnUs() {
		return String.valueOf(isNextInvestOnUs);
	}

	/**
	 * @param isNextInvestOnUs the isNextInvestOnUs to set
	 */
	public void setIsNextInvestOnUs(String isNextInvestOnUs) {
		this.isNextInvestOnUs = Integer.valueOf(isNextInvestOnUs);
	}

	/**
	 * @return the sendDepositDocs
	 */
	public int getSendDepositDocs() {
		return sendDepositDocs;
	}

	/**
	 * @param sendDepositDocs the sendDepositDocs to set
	 */
	public void setSendDepositDocs(int sendDepositDocs) {
		this.sendDepositDocs = sendDepositDocs;
	}

	public boolean isClassCompany() {
		if (classId==Constants.USER_CLASS_COMPANY) {
			return true;
		}
		return false;

	}

	public void setContactByEmail(boolean contactByEmail) {
		this.contactByEmail = contactByEmail;
	}



	/**
	 * @return the countryId
	 */
	public long getCountryId() {
		return countryId;
	}

	/**
	 * @param countryId the countryId to set
	 */
	public void setCountryId(long countryId) {
		this.countryId = countryId;
	}

	/**
	 * Constructs a <code>String</code> with all attributes
	 * in name = value format.
	 *
	 * @return a <code>String</code> representation
	 * of this object.
	 */
	public String toString()
	{
	    final String TAB = " \n ";

	    String retValue = "";

	    retValue = "EditUserForm ( "
	        + super.toString() + TAB
	        + "userName = " + this.userName + TAB
	        + "oldPassword = " + "*****" + TAB
	        + "password = " + "*****" + TAB
	        + "password2 = " + "*****" + TAB
	        + "currencyId = " + this.currencyId + TAB
	        + "firstName = " + this.firstName + TAB
	        + "lastName = " + this.lastName + TAB
	        + "street = " + this.street + TAB
	        + "streetNo = " + this.streetNo + TAB
	        + "cityId = " + this.cityId + TAB
	        + "zipCode = " + this.zipCode + TAB
	        + "email = " + this.email + TAB
	        + "birthYear = " + this.birthYear + TAB
	        + "birthMonth = " + this.birthMonth + TAB
	        + "birthDay = " + this.birthDay + TAB
	        + "contactByEmail = " + this.contactByEmail + TAB
	        + "mobileCountryPhoneCode = " + this.mobileCountryPhoneCode + TAB
	        + "mobilePhone = " + this.mobilePhone + TAB
	        + "landlineCountryPhoneCode = " + this.landlineCountryPhoneCode + TAB
	        + "landlinePhone = " + this.landLinePhone + TAB
	        + "gender = " + this.gender + TAB
	        + "idNum = " + this.idNum + TAB
	        + "terms = " + this.terms + TAB
	        + "combId = " + this.combId + TAB
	        + "classId = " + this.classId + TAB
	        + "comments = " + this.comments + TAB
	        + "isActive = " + this.isActive + TAB
	        + "limitId = " + this.limitId + TAB
	        + "cityName = " + this.cityName + TAB
	        + "usersList = " + this.usersList + TAB
	        + "sendPassword = " + this.sendPassword + TAB
	        + "companyName = " + this.companyName + TAB
	        + "companyId = " + this.companyId + TAB
	        + "taxExemption = " + this.taxExemption + TAB
	        + "referenceId = " + this.referenceId + TAB
	        + "isContactByPhone = " + this.contactByPhone + TAB
	        + "isFalseAccount = " + this.isFalseAccount + TAB
	        + "showCancelInvestment = " + this.showCancelInvestment + TAB
	        + " )";

	    return retValue;
	}

	/**
	 * @return the languageId
	 */
	public long getLanguageId() {
		return languageId;
	}

	/**
	 * @param languageId the languageId to set
	 */
	public void setLanguageId(long languageId) {
		this.languageId = languageId;
	}

	/**
	 * @return the skinId
	 */
	public int getSkinId() {
		return skinId;
	}

	/**
	 * @param skinId the skinId to set
	 */
	public void setSkinId(int skinId) {
		this.skinId = skinId;
	}

	/**
	 * @return the contactBySMS
	 */
	public boolean isContactBySMS() {
		return contactBySMS;
	}

	/**
	 * @param contactBySMS the contactBySMS to set
	 */
	public void setContactBySMS(boolean contactBySMS) {
		this.contactBySMS = contactBySMS;
	}

	public boolean isEtrader() {
		return CommonUtil.isHebrewSkin(getSkinId());
	}

	/**
	 * @return the state
	 */
	public Long getState() {
		return state;
	}

	/**
	 * @param state the state to set
	 */
	public void setState(Long state) {
		this.state = state;
	}

	/**
	 * @return the contactByPhone
	 */
	public boolean isContactByPhone() {
		return contactByPhone;
	}

	/**
	 * @param contactByPhone the contactByPhone to set
	 */
	public void setContactByPhone(boolean contactByPhone) {
		this.contactByPhone = contactByPhone;
	}

	/**
	 * @return the isFalseAccount
	 */
	public boolean isFalseAccount() {
		return isFalseAccount;
	}

	/**
	 * @param isFalseAccount the isFalseAccount to set
	 */
	public void setFalseAccount(boolean isFalseAccount) {
		this.isFalseAccount = isFalseAccount;
	}

	public int getIsVip() {
		return (isVip ? 1 : 0);
	}

	public boolean isVip() {
		return isVip;
	}

	public void setIsVip(int isVip) {
		this.isVip = (isVip == 1 ? true : false);
	}

	/**
	 * @return the isAuthorizedMail
	 */
	public boolean isAuthorizedMail() {
		return authorizedMail;
	}

	/**
	 * @param isAuthorizedMail the isAuthorizedMail to set
	 */
	public void setAuthorizedMail(boolean authorizedMail) {
		this.authorizedMail = authorizedMail;
	}

    /**
     * @return the isRisky
     */
    public boolean isRisky() {
        return isRisky;
    }

    /**
     * @param isRisky the isRisky to set
     */
    public void setRisky(boolean isRisky) {
        this.isRisky = isRisky;
    }

    /**
     * @return the specialCare
     */
    public int getSpecialCareManualy() {
        return specialCareManualy;
    }

    /**
     * @param specialCare the specialCare to set
     */
    public void setSpecialCareManualy(int specialCare) {
        this.specialCareManualy = specialCare;
    }

    /**
     * @return the tempSpecialCareManualy
     */
    public int getOldSpecialCareManualy() {
        return oldSpecialCareManualy;
    }

    /**
     * @param tempSpecialCareManualy the tempSpecialCareManualy to set
     */
    public void setOldSpecialCareManualy(int tempSpecialCareManualy) {
        this.oldSpecialCareManualy = tempSpecialCareManualy;
    }

	/**
	 * @return the isStopReverseWithdrawOption
	 */
	public boolean isStopReverseWithdrawOption() {
		return isStopReverseWithdrawOption;
	}

	/**
	 * @param isStopReverseWithdrawOption the isStopReverseWithdrawOption to set
	 */
	public void setStopReverseWithdrawOption(boolean isStopReverseWithdrawOption) {
		this.isStopReverseWithdrawOption = isStopReverseWithdrawOption;
	}

	/**
	 * @return the isImmediateWithdraw
	 */
	public boolean isImmediateWithdraw() {
		return isImmediateWithdraw;
	}

	/**
	 * @param isImmediateWithdraw the isImmediateWithdraw to set
	 */
	public void setImmediateWithdraw(boolean isImmediateWithdraw) {
		this.isImmediateWithdraw = isImmediateWithdraw;
	}
	
	public boolean isWaiveDepositsVelocityCheck() throws SQLException {
		FacesContext context = FacesContext.getCurrentInstance();
		User user = (User) context.getApplication().createValueBinding(Constants.BIND_USER).getValue(context);
		return UsersManager.isWaiveDepositsVelocityCheck(user.getId());
	}
	
	public ArrayList<SelectItem> getUserBubblesPricingLevels() {
		ArrayList<SelectItem> bubblesLevels = new ArrayList<SelectItem>();
		ArrayList<SelectItem> bl = new ArrayList<SelectItem>();
		bubblesLevels.add(new SelectItem(new Long(0L), ""));
		
		Map<Long, String> bubbleLevels = BubblesManager.getBubbleLevels();
		Iterator it = bubbleLevels.entrySet().iterator();
		while (it.hasNext()) {
			Map.Entry pair = (Map.Entry)it.next();
			SelectItem si = new SelectItem();
			si.setValue(pair.getKey());
			si.setLabel((String)pair.getValue());
			bl.add(si);
		}
		bubblesLevels.addAll(WriterWrapper.translateSI(bl));
		return bubblesLevels;
	}

	public boolean getCanEditEmail(){
		return !userName.equalsIgnoreCase(email);
	}

	/**
	 * @return the isUserDisable
	 */
	public boolean isUserDisable() {
		return isUserDisable;
	}

	/**
	 * @param isUserDisable the isUserDisable to set
	 */
	public void setUserDisable(boolean isUserDisable) {
		this.isUserDisable = isUserDisable;
	}

	/**
	 * @return the defaultAmountValue
	 */
	public Double getDefaultAmountValue() {
		return defaultAmountValue;
	}

	/**
	 * @param defaultAmountValue the defaultAmountValue to set
	 */
	public void setDefaultAmountValue(Double defaultAmountValue) {
		this.defaultAmountValue = defaultAmountValue;
	}

	/**
	 * @return the balanceStepId
	 */
	public long getBalanceStepId() {
		return balanceStepId;
	}

	/**
	 * @param balanceStepId the balanceStepId to set
	 */
	public void setBalanceStepId(long balanceStepId) {
		this.balanceStepId = balanceStepId;
	}

	/**
	 * @return the balanceSteps
	 */
	public List<SelectItem> getBalanceStepsSI() {
		return balanceStepsSI;
	}

	/**
	 * @param balanceSteps the balanceSteps to set
	 */
	public void setBalanceStepsSI(List<SelectItem> balanceStepsSI) {
		this.balanceStepsSI = balanceStepsSI;
	}

	/**
	 * @return the copyopNickname
	 */
	public String getCopyopNickname() {
		return copyopNickname;
	}

	/**
	 * @param copyopNickname the copyopNickname to set
	 */
	public void setCopyopNickname(String copyopNickname) {
		this.copyopNickname = copyopNickname;
	}

	public boolean isCopyopBlock() {
		return copyopBlock;
	}

	public void setCopyopBlock(boolean copyopBlock) {
		this.copyopBlock = copyopBlock;
	}

	public boolean isCopyopRemove() {
		return copyopRemove;
	}

	public void setCopyopRemove(boolean copyopRemove) {
		this.copyopRemove = copyopRemove;
	}

	public boolean isRegularReportMail() {
		return regularReportMail;
	}

	public void setRegularReportMail(boolean regularReportMail) {
		this.regularReportMail = regularReportMail;
	}

	public boolean isOldRegularReportMail() {
		return oldRegularReportMail;
	}

	public void setOldRegularReportMail(boolean oldRegularReportMail) {
		this.oldRegularReportMail = oldRegularReportMail;
	}

	public long getBubblesPricingLevel() {
		return bubblesPricingLevel;
	}

	public void setBubblesPricingLevel(long bubblesPricingLevel) {
		this.bubblesPricingLevel = bubblesPricingLevel;
	}

	public boolean isDisableCcDeposits() {
		return isDisableCcDeposits;
	}

	public void setDisableCcDeposits(boolean isDisableCcDeposits) {
		this.isDisableCcDeposits = isDisableCcDeposits;
	}

	public String getChangedSentDepositDoc() {
		return changedSentDepositDoc;
	}

	public void setChangedSentDepositDoc(String changedSentDepositDoc) {
		this.changedSentDepositDoc = changedSentDepositDoc;
	}

	public boolean isShowCancelInvestment() {
		return showCancelInvestment;
	}

	public void setShowCancelInvestment(boolean showCancelInvestment) {
		this.showCancelInvestment = showCancelInvestment;
	}

}