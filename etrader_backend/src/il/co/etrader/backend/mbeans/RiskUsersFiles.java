package il.co.etrader.backend.mbeans;

import java.io.Serializable;

import com.anyoption.common.beans.base.CreditCard;

/**
 * @author EranL
 *
 */
public class RiskUsersFiles implements Serializable {

	private static final long serialVersionUID = 1L;

	private String userName;
	private long userId;
	private String skin;
	private int skinId;
	private int type;
	private String filesRiskStatus;
	private String ccNumber;

	/**
	 * Constructs a <code>String</code> with all attributes
	 * in name = value format.
	 *
	 * @return a <code>String</code> representation
	 * of this object.
	 */
	public String toString()
	{
	    final String TAB = " \n ";

	    String retValue = "";

	    retValue = "UserRiskIssue ( "
	        + super.toString() + TAB
	        + "userName = " + this.userName + TAB
	        + "userId = " + this.userId + TAB
	        + "skin = " + this.skin + TAB
	        + "type = " + this.type + TAB
	        + "ccNumber = " + this.ccNumber + TAB
	        + "filesRiskStatus = " + this.filesRiskStatus + TAB
	        + " )";
	    return retValue;
	}

	/**
	 * @return the filesRiskStatus
	 */
	public String getFilesRiskStatus() {
		return filesRiskStatus;
	}

	/**
	 * @param filesRiskStatus the filesRiskStatus to set
	 */
	public void setFilesRiskStatus(String filesRiskStatus) {
		this.filesRiskStatus = filesRiskStatus;
	}

	/**
	 * @return the skin
	 */
	public String getSkin() {
		return skin;
	}
	/**
	 * @param skin the skin to set
	 */
	public void setSkin(String skin) {
		this.skin = skin;
	}
	/**
	 * @return the type
	 */
	public int getType() {
		return type;
	}

	/**
	 * @param type the type to set
	 */
	public void setType(int type) {
		this.type = type;
	}

	/**
	 * @return the userId
	 */
	public long getUserId() {
		return userId;
	}
	/**
	 * @param userId the userId to set
	 */
	public void setUserId(long userId) {
		this.userId = userId;
	}
	/**
	 * @return the userName
	 */
	public String getUserName() {
		return userName;
	}
	/**
	 * @param userName the userName to set
	 */
	public void setUserName(String userName) {
		this.userName = userName;
	}

	/**
	 * @return the skinId
	 */
	public int getSkinId() {
		return skinId;
	}

	/**
	 * @param skinId the skinId to set
	 */
	public void setSkinId(int skinId) {
		this.skinId = skinId;
	}

	/**
	 * @return the ccNumber
	 */
	public String getCcNumber() {
		return ccNumber;
	}

	/**
	 * @param ccNumber the ccNumber to set
	 */
	public void setCcNumber(String ccNumber) {
		this.ccNumber = ccNumber;
	}
	
	public String getCcNumberXXXX() {
		if (ccNumber.matches("^[0-9]*$")) {
			String ccnum = String.valueOf(ccNumber);
			String mask = "";
			String maskedCcnum = "";
			if (ccnum.length() >= CreditCard.CC_NUMBER_DIGITS) {
				for (int i = 0; i < ccnum.length() - 10; i++) {
					mask += "x";
				}
				maskedCcnum = ccnum.substring(0, 6) + mask + ccnum.substring(ccnum.length() - 4);
			} else {
				for (int i = 0; i < ccnum.length() - 4; i++) {
					mask += "x";
				}
				maskedCcnum = mask + ccnum.substring(ccnum.length() - 4);
			}
			return maskedCcnum;
		} else {
			return ccNumber;
		}
	}
	
	public String getCcNumberLast4NotMasked() {
		if (ccNumber.matches("^[0-9]*$")) {
			String ccnum = String.valueOf(ccNumber);
			String mask = "";
			for (int i = 0; i < ccnum.length() - 4; i++) {
				mask += "x";
			}
			return mask + ccnum.substring(ccnum.length() - 4);
		} else {
			return ccNumber;
		}
	}

}
