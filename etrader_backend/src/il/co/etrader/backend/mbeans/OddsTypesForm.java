package il.co.etrader.backend.mbeans;

import java.io.Serializable;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;

import com.anyoption.common.beans.OpportunityOddsType;

import il.co.etrader.backend.bl_managers.InvestmentsManager;
import il.co.etrader.backend.util.Constants;
import il.co.etrader.util.CommonUtil;

public class OddsTypesForm implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7735246676116390438L;
	private OpportunityOddsType type;
	private ArrayList list;

	public OddsTypesForm() throws SQLException{
		search();
	}

	public int getListSize() {
		return list.size();
	}

	public String updateInsert() throws SQLException{
		boolean res=InvestmentsManager.updateInsertOddsType(type);
		if (res==false)
			return null;
		search();
		return Constants.NAV_ODDS_TYPES;
	}

	public String initValues() {
		type=new OpportunityOddsType();
		type.setTimeCreated(new Date());
		return Constants.NAV_ODDS_TYPE;
	}

	public String search() throws SQLException{
		list=InvestmentsManager.searchOddsTypes();
		CommonUtil.setTablesToFirstPage();
		return null;
	}

	public ArrayList getList() {
		return list;
	}
	public void setlist(ArrayList list) {
		this.list = list;
	}

	public OpportunityOddsType getType() {
		return type;
	}

	public void setType(OpportunityOddsType type) {
		this.type = type;
	}

	/**
	 * Constructs a <code>String</code> with all attributes
	 * in name = value format.
	 *
	 * @return a <code>String</code> representation
	 * of this object.
	 */
	public String toString()
	{
	    final String TAB = " \n ";

	    String retValue = "";

	    retValue = "OddsTypesForm ( "
	        + super.toString() + TAB
	        + "type = " + this.type + TAB
	        + "list = " + this.list + TAB
	        + " )";

	    return retValue;
	}


}
