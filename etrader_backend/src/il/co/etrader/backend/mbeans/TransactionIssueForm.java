package il.co.etrader.backend.mbeans;

import java.io.Serializable;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.Issue;
import com.anyoption.common.beans.IssueAction;
import com.anyoption.common.beans.Transaction;

import il.co.etrader.backend.bl_managers.IssuesManager;
import il.co.etrader.backend.bl_managers.TransactionsIssuesManager;
import il.co.etrader.backend.bl_managers.TransactionsManager;
import il.co.etrader.backend.bl_vos.TransactionsIssues;
import il.co.etrader.backend.bl_vos.WriterWrapper;
import il.co.etrader.backend.util.Constants;
import il.co.etrader.backend.util.Utils;
import il.co.etrader.bl_managers.IssuesManagerBase;
import il.co.etrader.util.CommonUtil;
import il.co.etrader.util.ConstantsBase;

/**
 * TransactionIssueForm class
 * @author EranL
 */
public class TransactionIssueForm implements Serializable {

	private static final long serialVersionUID = -8712950430966187328L;
	private Long transactionId;
	private Long issueActionId;
	private long writerId;
	private long userId;
	private boolean issueActionFound;
	private boolean mismatchRep;
	private boolean tranOrIaNotFound;
	private IssueAction issueAction;
	private ArrayList<TransactionsIssues> tranIssueList;
	private boolean isNeedApprove;
	private static final Logger log = Logger.getLogger(TransactionIssueForm.class);

	/**
	 * Init values
	 */
	public TransactionIssueForm(){
		initValues();
	}

	/**
	 * Init form values
	 */
	public void initValues(){
		transactionId = null;
		issueActionId = null;
		initInternalValues();

	}

	/**
	 * Init form values
	 */
	public void initInternalValues(){
		writerId = 0;
		issueActionFound = false;
		mismatchRep = false;
		tranIssueList = new ArrayList<TransactionsIssues>();
		issueAction = null;
		tranOrIaNotFound = false;
		isNeedApprove = false;
	}

	/**
	 * Insert into TransactionIssue table
	 * @return
	 */
	public String insert() {
		if (validateArgs())	{
			isNeedApprove = true;
		}
		return null;
	}


	/**
	 * Validate form args
	 * @return
	 */
	public boolean validateArgs(){
		FacesMessage fm;
		FacesContext context = FacesContext.getCurrentInstance();
		mismatchRep = false;
		issueActionFound = false;
		tranOrIaNotFound = false;
		tranIssueList.clear();
		ArrayList<TransactionsIssues> transactionsIssuesList;

		int res[] = new int[2];  // 0-tran found on ti table 1-ia found on ti table

		try{

			Transaction t = TransactionsManager.getTransaction(transactionId);
			issueAction = IssuesManagerBase.getActionById(issueActionId);

			if(null == t){//if transaction was not found
				fm = new FacesMessage(FacesMessage.SEVERITY_INFO,
						CommonUtil.getMessage("transaction.issue.error.tran.not.exist", null, Utils.getWriterLocale(context)),null);
				context.addMessage(null, fm);
                
				tranOrIaNotFound = true;
				return false;
			}

			if(null == issueAction){//if issue action was not found
				fm = new FacesMessage(FacesMessage.SEVERITY_INFO,
						CommonUtil.getMessage("transaction.issue.error.issue.not.exist", null, Utils.getWriterLocale(context)),null);
				context.addMessage(null, fm);
                
				tranOrIaNotFound = true;
				return false;
			}

			userId = t.getUserId();
			Issue issue = IssuesManager.getIssueById(issueAction.getIssueId());

			if (userId != issue.getUserId()){
				fm = new FacesMessage(FacesMessage.SEVERITY_INFO,
						CommonUtil.getMessage("transaction.issue.error.user.not.match", null, Utils.getWriterLocale(context)),null);
				context.addMessage(null, fm);
                
				tranOrIaNotFound = true;
				return false;
			}

			transactionsIssuesList = TransactionsIssuesManager.isTranOnTiTable(transactionId, issueActionId);

			for (TransactionsIssues ti:transactionsIssuesList){
				if (ti.getTransactionId() == transactionId){ //found tran on ti table
					res[0] = 1 ;
					tranIssueList.add(ti);
					break;
				}
				if (ti.getIssueActionId() == issueActionId){ //found ia on ti table
					res[1] = 1 ;
					tranIssueList.add(ti);
				}

			}


			if(res[0] == 1){// if transaction found on ti table
				fm = new FacesMessage(FacesMessage.SEVERITY_INFO,
						CommonUtil.getMessage("transaction.issue.error.transaction.on.ti", null, Utils.getWriterLocale(context)),null);
				context.addMessage(null, fm);
                
				tranOrIaNotFound = false;
				issueActionFound = false;
				return false;
			}

			if (res[1] == 1){// if ia found on ti table
				fm = new FacesMessage(FacesMessage.SEVERITY_INFO,
						CommonUtil.getMessage("transaction.issue.error.issue.action.on.ti", null, Utils.getWriterLocale(context)),null);
				context.addMessage(null, fm);
                
				tranOrIaNotFound = false;
				issueActionFound = true;
			}


		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return true;
	}



	/**
	 * After writer approve of existents issue action
	 * @return
	 */
	public String issueActionApprove(){
		issueActionFound = false;
		insertTransactionIssue();
		isNeedApprove = false;
		return null;
	}

	/**
	 * Init values after writer cancel opertion
	 * @return
	 */
	public String issueActionDecline(){
		initInternalValues();
		 return Constants.NAV_ADMIN_TRANS_ISSUE;
	}

	/**
	 * Validate represenentives from issue action and writer that has been chosen
	 * @return
	 */
	public boolean validateRepresentive(){
		if (!(issueAction.getWriterId() == writerId)){
		 mismatchRep = true;
		 return false;
		}
		return true;
	}

	/**
	 * Insert into TransationIssue table and update online deposits
	 * @return
	 */
	public String insertTransactionIssue(){
		FacesMessage fm;
		FacesContext context = FacesContext.getCurrentInstance();
		log.info("Going to insert into transaction issue table");

		try {
			int isFirstDeposit = 0;
			if (il.co.etrader.bl_managers.TransactionsManagerBase.getFirstTrxId(userId) == transactionId) {
				isFirstDeposit = 1;
			}
			TransactionsIssuesManager.insertIntoTi(transactionId, issueActionId, 
					ConstantsBase.TRAN_ISSUES_INSERT_MANUAL, Utils.getWriter().getWriter().getId(), isFirstDeposit);
			log.debug(" insert tran " + transactionId + " and action " + issueActionId + " to transaction issues ");
			// update online deposits list
//			TransactionsIssuesManager.updateOnlineDeposits(writerId, userId, transactionId);
			String[] params = new String[2];
			params[0] = String.valueOf(transactionId);
			params[1] = String.valueOf(issueActionId);
			fm = new FacesMessage(FacesMessage.SEVERITY_INFO,CommonUtil.getMessage("transaction.issue.insert.successfully", params),null);
			context.addMessage(null, fm);
            
			initValues();

		} catch (SQLException e) {
			log.error("Error cant insert into transaction issue " + e);
			e.printStackTrace();
		}
		return null;
	}

	public String getWriterNameById(long writerId) {
		FacesContext context = FacesContext.getCurrentInstance();
		WriterWrapper writer = context.getApplication().evaluateExpressionGet(context, "#{writer}", WriterWrapper.class);
		return writer.getAllWritersMap().get(writerId).getUserName();
	}
	
	/**
	 * @return the mismatchRep
	 */
	public boolean isMismatchRep() {
		return mismatchRep;
	}

	/**
	 * @param mismatchRep the mismatchRep to set
	 */
	public void setMismatchRep(boolean mismatchRep) {
		this.mismatchRep = mismatchRep;
	}

	/**
	 * @return the issueAction
	 */
	public IssueAction getIssueAction() {
		return issueAction;
	}

	/**
	 * @param issueAction the issueAction to set
	 */
	public void setIssueAction(IssueAction issueAction) {
		this.issueAction = issueAction;
	}


	/**
	 * @return the issueAction
	 */
	public Long getIssueActionId() {
		return issueActionId;
	}

	/**
	 * @param issueAction the issueAction to set
	 */
	public void setIssueActionId(Long issueActionId) {
		this.issueActionId = issueActionId;
	}

	/**
	 * @return the transactionId
	 */
	public Long getTransactionId() {
		return transactionId;
	}

	/**
	 * @param transactionId the transactionId to set
	 */
	public void setTransactionId(Long transactionId) {
		this.transactionId = transactionId;
	}

	/**
	 * @return the writerId
	 */
	public long getWriterId() {
		return writerId;
	}

	/**
	 * @param writerId the writerId to set
	 */
	public void setWriterId(long writerId) {
		this.writerId = writerId;
	}

	/**
	 * @return the issueActionFound
	 */
	public boolean isIssueActionFound() {
		return issueActionFound;
	}

	/**
	 * @param issueActionFound the issueActionFound to set
	 */
	public void setIssueActionFound(boolean issueActionFound) {
		this.issueActionFound = issueActionFound;
	}

	/**
	 * @return the tranIssueList
	 */
	public ArrayList<TransactionsIssues> getTranIssueList() {
		return tranIssueList;
	}

	/**
	 * @param tranIssueList the tranIssueList to set
	 */
	public void setTranIssueList(ArrayList<TransactionsIssues> tranIssueList) {
		this.tranIssueList = tranIssueList;
	}

	public int getListSize() {
		return tranIssueList.size();
	}

	/**
	 * @return the tranOrIaNotFound
	 */
	public boolean isTranOrIaNotFound() {
		return tranOrIaNotFound;
	}

	/**
	 * @param tranOrIaNotFound the tranOrIaNotFound to set
	 */
	public void setTranOrIaNotFound(boolean tranOrIaNotFound) {
		this.tranOrIaNotFound = tranOrIaNotFound;
	}

	/**
	 * @return the isNeedApprove
	 */
	public boolean isNeedApprove() {
		return isNeedApprove;
	}

	/**
	 * @param isNeedApprove the isNeedApprove to set
	 */
	public void setNeedApprove(boolean isNeedApprove) {
		this.isNeedApprove = isNeedApprove;
	}

}
