package il.co.etrader.backend.mbeans;

import java.io.Serializable;
import java.sql.SQLException;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import il.co.etrader.backend.util.Utils;
import il.co.etrader.bl_managers.ConfigManager;
import il.co.etrader.util.CommonUtil;

public class LiveConfigForm implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 2965330953992122467L;
	private long livepageGlobeUpdateInterval;
	private long livepageGlobeQueueTtl;
	private long livepageGlobeMinAmount;
	private long livepageGlobeFakeMinAmount;
	private long livepageGlobeFakeMaxAmount;

	
	private long deafultValue = 0;
	
	public LiveConfigForm() throws SQLException{
		initValue();
	}
	
	private void initValue() throws SQLException{
		livepageGlobeUpdateInterval = ConfigManager.getLongParameter(ConfigManager.LIVEPAGE_GLOBE_UPDATE_INTERVAL_PARAM_NAME, deafultValue);
		livepageGlobeQueueTtl = ConfigManager.getLongParameter(ConfigManager.LIVEPAGE_GLOBE_QUEUE_TTL_PARAM_NAME, deafultValue);
		livepageGlobeMinAmount = ConfigManager.getLongParameter(ConfigManager.LIVEPAGE_GLOBE_MIN_AMOUNT_PARAM_NAME, deafultValue);
		livepageGlobeFakeMinAmount = ConfigManager.getLongParameter(ConfigManager.LIVEPAGE_GLOBE_FAKE_MIN_AMOUNT_PARAM_NAME, deafultValue);
		livepageGlobeFakeMaxAmount = ConfigManager.getLongParameter(ConfigManager.LIVEPAGE_GLOBE_FAKE_MAX_AMOUNT_PARAM_NAME, deafultValue);
	}
	
	public String save() throws SQLException{
		FacesContext context = FacesContext.getCurrentInstance();
		if (livepageGlobeUpdateInterval <= 0 || livepageGlobeQueueTtl <= 0 ||  livepageGlobeMinAmount <= 0 
				|| livepageGlobeFakeMinAmount <= 0 || livepageGlobeFakeMaxAmount <=0 ){
			FacesMessage fm = new FacesMessage(
					FacesMessage.SEVERITY_ERROR,
					CommonUtil.getMessage(
							"error.live.numbers.greater", null),
					null);
			context.addMessage(null, fm);
			initValue();
			return null;
		}
		
		boolean res;
		res = ConfigManager.updateParametersByKey(ConfigManager.LIVEPAGE_GLOBE_UPDATE_INTERVAL_PARAM_NAME, livepageGlobeUpdateInterval);
		res = ConfigManager.updateParametersByKey(ConfigManager.LIVEPAGE_GLOBE_QUEUE_TTL_PARAM_NAME, livepageGlobeQueueTtl);
		res = ConfigManager.updateParametersByKey(ConfigManager.LIVEPAGE_GLOBE_MIN_AMOUNT_PARAM_NAME, livepageGlobeMinAmount);
		res = ConfigManager.updateParametersByKey(ConfigManager.LIVEPAGE_GLOBE_FAKE_MIN_AMOUNT_PARAM_NAME, livepageGlobeFakeMinAmount);
		res = ConfigManager.updateParametersByKey(ConfigManager.LIVEPAGE_GLOBE_FAKE_MAX_AMOUNT_PARAM_NAME, livepageGlobeFakeMaxAmount);
		
		initValue();
		
		FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_INFO,CommonUtil.getMessage("general.update.success", null, Utils.getWriterLocale(context)),null);
		context.addMessage(null, fm);
		return null;
	}

	/**
	 * @return the livepageGlobeUpdateInterval
	 */
	public long getLivepageGlobeUpdateInterval() {
		return livepageGlobeUpdateInterval;
	}

	/**
	 * @param livepageGlobeUpdateInterval the livepageGlobeUpdateInterval to set
	 */
	public void setLivepageGlobeUpdateInterval(long livepageGlobeUpdateInterval) {
		this.livepageGlobeUpdateInterval = livepageGlobeUpdateInterval;
	}

	/**
	 * @return the livepageGlobeQueueTtl
	 */
	public long getLivepageGlobeQueueTtl() {
		return livepageGlobeQueueTtl;
	}

	/**
	 * @param livepageGlobeQueueTtl the livepageGlobeQueueTtl to set
	 */
	public void setLivepageGlobeQueueTtl(long livepageGlobeQueueTtl) {
		this.livepageGlobeQueueTtl = livepageGlobeQueueTtl;
	}

	/**
	 * @return the livepageGlobeMinAmountTxtFloat
	 */
	public String getLivepageGlobeMinAmountTxtFloat() {
		return CommonUtil.displayAmountForInput(Long.valueOf(livepageGlobeMinAmount), false,1);		
	}

	/**
	 * @param livepageGlobeMinAmountTxtFloat the livepageGlobeMinAmountTxtFloat to set
	 */
	public void setLivepageGlobeMinAmountTxtFloat(String livepageGlobeMinAmountTxtFloat) {
		this.livepageGlobeMinAmount = CommonUtil.calcAmount(livepageGlobeMinAmountTxtFloat);
	}

	/**
	 * @return the livepageGlobeFakeMinamountTxtFloat
	 */
	public String getLivepageGlobeFakeMinAmountTxtFloat() {
		return CommonUtil.displayAmountForInput(Long.valueOf(livepageGlobeFakeMinAmount), false,1);		
	}

	/**
	 * @param livepageGlobeFakeMinAmountTxtFloat the livepageGlobeFakeMinAmountTxtFloat to set
	 */
	public void setLivepageGlobeFakeMinAmountTxtFloat(String livepageGlobeFakeMinAmountTxtFloat) {
		this.livepageGlobeFakeMinAmount = CommonUtil.calcAmount(livepageGlobeFakeMinAmountTxtFloat);
	}

	/**
	 * @return the livepageGlobeFakeMaxAmountTxtFloat
	 */
	public String getLivepageGlobeFakeMaxAmountTxtFloat() {
		return CommonUtil.displayAmountForInput(Long.valueOf(livepageGlobeFakeMaxAmount), false,1);	
	}

	/**
	 * @param livepageGlobeFakeMaxAmountTxtFloat the livepageGlobeFakeMaxAmountTxtFloat to set
	 */
	public void setLivepageGlobeFakeMaxAmountTxtFloat(String livepageGlobeFakeMaxAmountTxtFloat) {
		this.livepageGlobeFakeMaxAmount = CommonUtil.calcAmount(livepageGlobeFakeMaxAmountTxtFloat);
	} 

}
