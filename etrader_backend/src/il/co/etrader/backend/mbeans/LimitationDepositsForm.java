package il.co.etrader.backend.mbeans;

import java.io.Serializable;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.LimitationDeposits;

import il.co.etrader.backend.bl_managers.ApplicationData;
import il.co.etrader.backend.bl_managers.LimitationDepositsManager;
import il.co.etrader.backend.bl_vos.WriterWrapper;
import il.co.etrader.backend.util.Constants;
import il.co.etrader.backend.util.Utils;
import il.co.etrader.util.CommonUtil;
import il.co.etrader.util.ConstantsBase;

public class LimitationDepositsForm implements Serializable {

	private static final long serialVersionUID = -476321898003866278L;

	private static final Logger log = Logger.getLogger(LimitationDepositsForm.class);

    //many list box
    private List<Long> types;
    private List<Long> skins;
    private List<Long> currencies;
    private List<Long> countries;
    private List<Long> paymentRecipients;
    private List<Long> campaigns;
    private List<Long> affiliates;
    private long active;
    private long remarketing;

    private Long minDeposit;
    private Long minFirstDeposit;
    private Long defaultDeposit;
    private String comments;

    private ArrayList<LimitationDeposits> depositsList;

    private LimitationDeposits limitationDeposit;

    private boolean isShowConfirmation;

	public LimitationDepositsForm() {
        depositsList = new ArrayList<LimitationDeposits>();
        skins = new ArrayList<Long>();
        currencies = new ArrayList<Long>();
        countries = new ArrayList<Long>();
        paymentRecipients = new ArrayList<Long>();
        campaigns = new ArrayList<Long>();
        affiliates = new ArrayList<Long>();
        types = new ArrayList<Long>();
        initFilters();
        search();
	}

    public String search() {
        try {
            depositsList = LimitationDepositsManager.search(getArrayToString(skins), getArrayToString(currencies), getArrayToString(countries), getArrayToString(paymentRecipients), getArrayToString(campaigns), getArrayToString(affiliates), active, false, types, remarketing);
            CommonUtil.setTablesToFirstPage();
        } catch (SQLException e) {
            log.debug("cant search deposit limitation", e);
        }
        return null;
    }

    public String validate() {
        int multiCounter = 0; //check how much array we have with multi select
        if (skins.size() > 1) {
            multiCounter++;
        }
        if (currencies.size() > 1) {
            multiCounter++;
        }
        if (countries.size() > 1) {
            multiCounter++;
        }
        if (paymentRecipients.size() > 1) {
            multiCounter++;
        }
        if (campaigns.size() > 1) {
            multiCounter++;
        }
        if (affiliates.size() > 1) {
            multiCounter++;
        }

        if (multiCounter > 1) {
            FacesContext context = FacesContext.getCurrentInstance();
            FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_INFO, CommonUtil.getMessage("limits.error.multiselect", null, Utils.getWriterLocale(context)),null);
            context.addMessage(null, fm);
            return null;
        }
        
        if ((null == minDeposit || minDeposit < 1) && (null == minFirstDeposit || minFirstDeposit < 1) && (null == defaultDeposit || defaultDeposit < 1)) {
            FacesContext context = FacesContext.getCurrentInstance();
            FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_INFO, CommonUtil.getMessage("limits.error.min.deposit", null, Utils.getWriterLocale(context)),null);
            context.addMessage(null, fm);
            return null;
        }
        try {
            if (LimitationDepositsManager.search(getArrayToString(skins), getArrayToString(currencies), getArrayToString(countries), getArrayToString(paymentRecipients), getArrayToString(campaigns), getArrayToString(affiliates), Constants.ALL_FILTER_ID1, true, types, remarketing).size() > 0) {
                FacesContext context = FacesContext.getCurrentInstance();
                FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_INFO, CommonUtil.getMessage("limits.error.same.role", null, Utils.getWriterLocale(context)),null);
                context.addMessage(null, fm);
                return null;
            }

//
        } catch (Exception e) {
            log.debug("cant insert deposit limitation", e);
            FacesContext context = FacesContext.getCurrentInstance();
            String[] params = new String[1];
            params[0] = e.getMessage();
            FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_INFO, CommonUtil.getMessage("limits.error.insert.role", params, Utils.getWriterLocale(context)), null);
            context.addMessage(null, fm);
            return null;
        }

        isShowConfirmation = true;
        return null;

    }

    public String insert() {
        int arraySize = 1; //if no one is multi select
        if (skins.size() > 1) {
            arraySize = skins.size();
        }
        if (currencies.size() > 1) {
            arraySize = currencies.size();
        }
        if (countries.size() > 1) {
            arraySize = countries.size();
        }
        if (paymentRecipients.size() > 1) {
            arraySize = paymentRecipients.size();
        }
        if (campaigns.size() > 1) {
            arraySize = campaigns.size();
        }
        if (affiliates.size() > 1) {
            arraySize = affiliates.size();
        }

        try {
            LimitationDepositsManager.insert(skins, currencies, countries, paymentRecipients, campaigns, affiliates, minDeposit, minFirstDeposit, defaultDeposit, comments, arraySize, remarketing);
        } catch (Exception e) {
            log.debug("cant insert deposit limitation", e);
            FacesContext context = FacesContext.getCurrentInstance();
            String[] params = new String[1];
            params[0] = e.getMessage();
            FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_INFO, CommonUtil.getMessage("limits.error.insert.role", params, Utils.getWriterLocale(context)), null);
            context.addMessage(null, fm);
            return null;
        }


        initFilters();
        search();
        return Constants.NAV_LIMITATION_DEPOSITS_SEARCH;
    }

    public String cancel() {
        isShowConfirmation = false;
        return null;
    }

    public void initFilters() {
        active = Constants.ALL_FILTER_ID1;
        remarketing = Constants.ALL_FILTER_ID1;
        skins.clear();
        skins.add(0L);
        currencies.clear();
        currencies.add(0L);
        countries.clear();
        countries.add(0L);
        paymentRecipients.clear();
        paymentRecipients.add(0L);
        campaigns.clear();
        campaigns.add(0L);
        affiliates.clear();
        affiliates.add(0L);
        types.clear();
        types.add(0L);
        comments = null;
        minDeposit = null;
        minFirstDeposit = null;
        defaultDeposit = null;
        isShowConfirmation = false;
    }

    public String navNew() {
        initFilters();
        return Constants.NAV_LIMITATION_DEPOSITS_NEW;

    }

    public String navUpdate() {
        initFilters();
        depositsList.clear();
        return Constants.NAV_LIMITATION_DEPOSITS_UPDATE;

    }

    public String validateUpdate() {
        if (limitationDeposit.getMinimumDeposit() < 1 || limitationDeposit.getMinimumFirstDeposit() < 1) {
            FacesContext context = FacesContext.getCurrentInstance();
            FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_INFO, CommonUtil.getMessage("limits.error.min.deposit", null, Utils.getWriterLocale(context)),null);
            context.addMessage(null, fm);
            return null;
        }

        try {
            long id = limitationDeposit.isUpdateByGroup() ? limitationDeposit.getGroupId() : limitationDeposit.getId();
            depositsList = LimitationDepositsManager.searchById(id, limitationDeposit.isUpdateByGroup());
        } catch (SQLException e) {
            log.debug("cant update deposit limitation cant get the roles for id " + limitationDeposit.getId() + " by group " + limitationDeposit.isUpdateByGroup(), e);
            FacesContext context = FacesContext.getCurrentInstance();
            String[] params = new String[2];
            params[0] = String.valueOf(limitationDeposit.getId());
            params[0] = String.valueOf(limitationDeposit.isUpdateByGroup());
            FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_INFO, CommonUtil.getMessage("limits.error.search.role", null, Utils.getWriterLocale(context)), null);
            context.addMessage(null, fm);
            return null;
        }
        isShowConfirmation = true;
        return null;
    }


    public String update() {

        try {
            LimitationDepositsManager.update(limitationDeposit);
        } catch (SQLException e) {
            log.debug("cant update deposit limitation id " + limitationDeposit.getId(), e);
            FacesContext context = FacesContext.getCurrentInstance();
            String[] params = new String[1];
            params[0] = e.getMessage();
            FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_INFO, CommonUtil.getMessage("limits.error.insert.role", params, Utils.getWriterLocale(context)), null);
            context.addMessage(null, fm);
            return null;
        }
        search();
        return Constants.NAV_LIMITATION_DEPOSITS_SEARCH;
    }

    /**
     * @return the skins
     */
    public List<Long> getSkins() {
        return skins;
    }

    /**
     * @param skins the skins to set
     */
    public void setSkins(List<Long> skins) {
        this.skins = skins;
    }

    /**
     * @return the currencies
     */
    public List<Long> getCurrencies() {
        return currencies;
    }

    /**
     * @param currencies the currencies to set
     */
    public void setCurrencies(List<Long> currencies) {
        this.currencies = currencies;
    }

    /**
     * @return the affiliates
     */
    public List<Long> getAffiliates() {
        return affiliates;
    }

    /**
     * @param affiliates the affiliates to set
     */
    public void setAffiliates(List<Long> affiliates) {
        this.affiliates = affiliates;
    }

    /**
     * @return the campaigns
     */
    public List<Long> getCampaigns() {
        return campaigns;
    }

    /**
     * @param campaigns the campaigns to set
     */
    public void setCampaigns(List<Long> campaigns) {
        this.campaigns = campaigns;
    }

    /**
     * @return the countries
     */
    public List<Long> getCountries() {
        return countries;
    }

    /**
     * @param countries the countries to set
     */
    public void setCountries(List<Long> countries) {
        this.countries = countries;
    }

    /**
     * @return the paymentRecipients
     */
    public List<Long> getPaymentRecipients() {
        return paymentRecipients;
    }

    /**
     * @param paymentRecipients the paymentRecipients to set
     */
    public void setPaymentRecipients(List<Long> paymentRecipients) {
        this.paymentRecipients = paymentRecipients;
    }

    /**
     * @return the comments
     */
    public String getComments() {
        return comments;
    }

    /**
     * @param comments the comments to set
     */
    public void setComments(String comments) {
        this.comments = comments;
    }

    /**
     * @return the minDeposit
     */
    public Long getMinDeposit() {
    	return minDeposit;
    }

    /**
     * @param minDeposit the minDeposit to set
     */
    public void setMinDeposit(Long minDeposit) {
        this.minDeposit = minDeposit;
    }

    /**
     * @return the minFirstDeposit
     */
    public Long getMinFirstDeposit() {
        return minFirstDeposit;
    }

    /**
     * @param minFirstDeposit the minFirstDeposit to set
     */
    public void setMinFirstDeposit(Long minFirstDeposit) {
        this.minFirstDeposit = minFirstDeposit;
    }

    public Long getDefaultDeposit() {
		return defaultDeposit;
	}

	public void setDefaultDeposit(Long defaultDeposit) {
		if (null != defaultDeposit) {
			this.defaultDeposit = defaultDeposit;
		} else {
			this.defaultDeposit = 0L;
		}
	}

	/**
     * return null if all selected else return string of array
     * @param temp array to convert
     * @return String
     */
    public String getArrayToString(List<Long> temp) {
        String list = ConstantsBase.EMPTY_STRING;
        for (int i = 0; i < temp.size(); i++) {
            if(temp.get(i) == Constants.ALL_FILTER_ID){
                return null;
            }
            list += temp.get(i) + ",";
        }
        return list.substring(0, list.length() - 1);
    }

    /**
     *
     * @return
     */
    public int getListSize() {
        return depositsList.size();
    }

    /**
     * @return the depositsList
     */
    public ArrayList<LimitationDeposits> getDepositsList() {
        return depositsList;
    }

    /**
     * @param depositsList the depositsList to set
     */
    public void setDepositsList(ArrayList<LimitationDeposits> depositsList) {
        this.depositsList = depositsList;
    }

    /**
     * @return the active
     */
    public long getActive() {
        return active;
    }

    /**
     * @param active the active to set
     */
    public void setActive(long active) {
        this.active = active;
    }

    /**
     * Get active / inactive list
     * @return
     * @throws SQLException
     */
    public ArrayList<SelectItem> getIsActiveAllSI() throws SQLException {

        ArrayList<SelectItem> si = new ArrayList<SelectItem>();
        si.add(new SelectItem(Constants.ALL_FILTER_ID1, CommonUtil.getMessage("users.market.disable.all", null)));
        si.add(new SelectItem(Constants.FILTER_ACTIVE, CommonUtil.getMessage("users.market.disable.active", null)));
        si.add(new SelectItem(Constants.FILTER_NOT_ACTIVE, CommonUtil.getMessage("users.market.disable.Inactive", null)));

        return si;
    }

    /**
     * Get remarketing options list
     * @return
     * @throws SQLException
     */
    public ArrayList<SelectItem> getIsRemarketingSI(boolean isShowAll) throws SQLException {

        ArrayList<SelectItem> si = new ArrayList<SelectItem>();
        if (isShowAll) {
            si.add(new SelectItem(Constants.ALL_REPS_FILTER_ID, CommonUtil.getMessage("users.market.disable.all", null)));
        }
        si.add(new SelectItem(Constants.USERS_MARKET_DIS_NOT_ACTIVE, CommonUtil.getMessage("users.market.disable.Inactive", null)));
        si.add(new SelectItem(Constants.USERS_MARKET_DIS_ACTIVE, CommonUtil.getMessage("users.market.disable.active", null)));

        return si;
    }

    /**
     * @return the limitationDeposit
     */
    public LimitationDeposits getLimitationDeposit() {
        return limitationDeposit;
    }

    /**
     * @param limitationDeposit the limitationDeposit to set
     */
    public void setLimitationDeposit(LimitationDeposits limitationDeposit) {
        this.limitationDeposit = limitationDeposit;
    }

    /**
     * Get type select item
     * @return
     * @throws SQLException
     */
    public ArrayList<SelectItem> getTypeAllSI() throws SQLException {

        ArrayList<SelectItem> si = new ArrayList<SelectItem>();
        si.add(new SelectItem(ConstantsBase.ALL_FILTER_ID, CommonUtil.getMessage("users.market.disable.all", null)));
        si.add(new SelectItem(LimitationDeposits.AFFILATE, CommonUtil.getMessage("affilate", null)));
        si.add(new SelectItem(LimitationDeposits.CAMPAIGN, CommonUtil.getMessage("menu.ads", null)));
        si.add(new SelectItem(LimitationDeposits.COUNTRY, CommonUtil.getMessage("register.country", null)));
        si.add(new SelectItem(LimitationDeposits.CURRENCY, CommonUtil.getMessage("investments.currency", null)));
        si.add(new SelectItem(LimitationDeposits.PAYMENT_RECIPIENT, CommonUtil.getMessage("menu.payment.recipients", null)));
        si.add(new SelectItem(LimitationDeposits.SKIN, CommonUtil.getMessage("register.skin", null)));
        si.add(new SelectItem(LimitationDeposits.REMARKETING, CommonUtil.getMessage("limits.Remarketing", null)));

        return si;
    }

    /**
     * @return the types
     */
    public List<Long> getTypes() {
        return types;
    }

    /**
     * @param types the types to set
     */
    public void setTypes(List<Long> types) {
        this.types = types;
    }

    /**
     * @return the isShowConfirmation
     */
    public boolean isShowConfirmation() {
        return isShowConfirmation;
    }

    /**
     * @param isShowConfirmation the isShowConfirmation to set
     */
    public void setShowConfirmation(boolean isShowConfirmation) {
        this.isShowConfirmation = isShowConfirmation;
    }

    public String getTypesTxt() {
        String types = "";
        if (!skins.contains(0L)) {
            types += "skin</br>";
        }
        if (!currencies.contains(0L)) {
            types += "currencies</br>";
        }
        if (!countries.contains(0L)) {
            types += "countries</br>";
        }
        if (!paymentRecipients.contains(0L)) {
            types += "paymentRecipients</br>";
        }
        if (!campaigns.contains(0L)) {
            types += "campaigns</br>";
        }
        if (!affiliates.contains(0L)) {
            types += "affiliates</br>";
        }
        types += "remarketing</br>";
        return types;
    }

    public String getRules() {
        FacesContext context = FacesContext.getCurrentInstance();
        WriterWrapper wr = (WriterWrapper) context.getApplication().createValueBinding(Constants.BIND_WRITER_WRAPPER).getValue(context);
        String rules = "";
        if (!skins.contains(0L)) {
            rules += getAllLabels(wr.getSkinsAndEmptyLongSI(), skins);
        }
        if (!currencies.contains(0L)) {
            rules += getAllLabels(wr.getCurrencySI(), currencies);
        }
        if (!countries.contains(0L)) {
            ApplicationData ap = (ApplicationData) context.getApplication().createValueBinding(Constants.BIND_APPLICATION_DATA).getValue(context);
            try {
                rules += getAllLabels(ap.getAllCountriesList(), countries);
            } catch (SQLException e) {
                log.debug("cant get countries rules");
            }
        }
        if (!paymentRecipients.contains(0L)) {
            try {
                rules += getAllLabels(wr.getMarketingPaymentRecipientsAllWriterSkinId(), paymentRecipients);
            } catch (SQLException e) {
                log.debug("cant get paymentRecipients rules");
            }
        }
        if (!campaigns.contains(0L)) {
            try {
                rules += getAllLabels(wr.getAllMarketingCampaignsLongSI(), campaigns);
            } catch (SQLException e) {
                log.debug("cant get campaigns rules");
            }
        }
        if (!affiliates.contains(0L)) {
            try {
                rules += getAllLabels(wr.getMarketingAffiliatesAllSI(), affiliates);
            } catch (SQLException e) {
                log.debug("cant get affiliates rules");
            }
        }
        if (remarketing == Constants.USERS_MARKET_DIS_ACTIVE) {
            rules += "true</br>";
        } else {
            rules += "false</br>";
        }
        return rules;
    }

    /**
     * get the Labels from select item according to the multiselect dropdown
     * @param list select item list
     * @param list2 multi selet drop down selected options
     * @return list of labels that were choosen
     */
    public String getAllLabels(ArrayList list, List<Long> list2) {
        String labels = "";
        for (long id : list2) {
            for (int i = 0; i < list.size(); i++) {
                if ((Long)((SelectItem)list.get(i)).getValue() == id) {
                    labels += ((SelectItem)list.get(i)).getLabel() + ",";
                    break;
                }
            }
        }
        labels = labels.substring(0, labels.length() - 1);
        labels +="</br>";
        return labels;
    }

    /**
     * @return the remarketing
     */
    public long getRemarketing() {
        return remarketing;
    }

    /**
     * @param remarketing the remarketing to set
     */
    public void setRemarketing(long remarketing) {
        this.remarketing = remarketing;
    }

}
