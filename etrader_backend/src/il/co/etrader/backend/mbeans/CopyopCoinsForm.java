package il.co.etrader.backend.mbeans;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.UUID;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import org.apache.log4j.Logger;

import com.copyop.common.dto.Profile;
import com.copyop.common.enums.CoinsActionTypeEnum;
import com.copyop.common.managers.ProfileCountersManager;

import il.co.etrader.backend.bl_managers.CopyopCoinsManager;
import il.co.etrader.backend.bl_vos.CopyopCoins;
import il.co.etrader.backend.bl_vos.User;
import il.co.etrader.backend.util.Utils;
import il.co.etrader.util.CommonUtil;
import il.co.etrader.util.ConstantsBase;

/**
 * @author eranl
 *
 */
public class CopyopCoinsForm implements Serializable {	
	private static final long serialVersionUID = -1552472966777579961L;	 
	private static final Logger logger = Logger.getLogger(CopyopCoinsForm.class);
	
	private User user;
	private Profile profile;
	private ArrayList<CopyopCoins> copyopCoinsList;
		
	private long coinsBalance;
	private long coinsConverted;
	private long coinsMissed;
	
	// filters
	private Date from;
	private Date to;
	private long coinsStateId;
	private boolean regrantFilterSelected;
		
	private CopyopCoins regrantCopyopCoins;
		
	public CopyopCoinsForm() { 	
		user = Utils.getUser();				
		GregorianCalendar gc = new GregorianCalendar();
		gc.set(Calendar.HOUR_OF_DAY, 0);
		gc.set(Calendar.MINUTE, 0);
		gc.set(Calendar.SECOND, 0);
		to = gc.getTime();					
		gc.add(Calendar.MONTH, -1);		
		from = gc.getTime();					
		coinsBalance = ProfileCountersManager.getProfileCoinsBalance(user.getId());		
		coinsConverted = CopyopCoinsManager.sumConvertedCoins(user.getId()); 
		coinsMissed = CopyopCoinsManager.sumMissedCoins(user.getId());	
		search();
	}
	
	/**
	 * Search for coins balance history  
	 * @return
	 */
	public String search() {		
		// re-grant is "fake" state. actually it's 'missed' state and his column of re-grant is true
		regrantFilterSelected = false;
		if (coinsStateId == CoinsActionTypeEnum.RE_GRANT.getId()) {
			coinsStateId = CoinsActionTypeEnum.RESET_COINS.getId();
			regrantFilterSelected = true;
		}
		copyopCoinsList = CopyopCoinsManager.getCoinsBalanceHistoryByUser(user.getId(), 
								Integer.valueOf(String.valueOf(coinsStateId)), from, CommonUtil.addDay(to));
		if (copyopCoinsList != null) {
			for (CopyopCoins cc : copyopCoinsList) {
				// set amount
				cc.setAmountWF(CommonUtil.displayAmount(cc.getAmount(), user.getCurrencyId()));
				if (coinsStateId == CoinsActionTypeEnum.RESET_COINS.getId()) {
					if (regrantFilterSelected && cc.getActionTypeId() != CoinsActionTypeEnum.RE_GRANT.getId()) {
						copyopCoinsList.remove(cc);				
					} else if (!regrantFilterSelected && cc.getActionTypeId() != CoinsActionTypeEnum.RESET_COINS.getId()) {
						copyopCoinsList.remove(cc);
					}
				}
			}
			// setting back the filter choise
			if (regrantFilterSelected) {
				coinsStateId = CoinsActionTypeEnum.RE_GRANT.getId();
			}
		}	
		CommonUtil.setTablesToFirstPage();
		return null;
	}
	
	/**
	 * Re-grant missed coins for user
	 * @return
	 */
	public String regrantCoins() {
		FacesContext context = FacesContext.getCurrentInstance();
		String message = ConstantsBase.EMPTY_STRING;
		String[] params = null;		
		long regrantWriterId = Utils.getWriter().getWriter().getId();
		logger.info("Start: Writer Id: " + regrantWriterId + " is going to regarnt copyop coins. copyop balance history id:" + regrantCopyopCoins.getId() + " to userId:" + user.getId());	
		try {			
			CopyopCoinsManager.updateBalanceHistoryRegrant(regrantCopyopCoins.getCoins(), user.getId(),
					UUID.fromString(regrantCopyopCoins.getId()), Integer.valueOf(String.valueOf(regrantWriterId)));			
			message = "copyop.coins.regrant.msg.success";
			params = new String[2];
			params[0] = String.valueOf(user.getId()); 
			params[1] = String.valueOf(regrantCopyopCoins.getCoins()); ;
		} catch (Exception e) {
			logger.error("Can't regrant copyop coins for userId:" + user.getId(), e);
			message = "copyop.coins.regrant.msg.fail";
		}
		FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_INFO,
				CommonUtil.getMessage(message, params, Utils.getWriterLocale(context)), null);
		context.addMessage(null, fm);
		logger.info("End: Writer Id:: " + regrantWriterId + " is going to regarnt copyop coins. copyop balance history id:" + regrantCopyopCoins.getId() + " to userId:" + user.getId());
		return null;
	}
	
	/**
	 * @return
	 */
	public int getListSize() {
		return copyopCoinsList.size();
	}

	/**
	 * @return the profile
	 */
	public Profile getProfile() {
		return profile;
	}

	/**
	 * @param profile the profile to set
	 */
	public void setProfile(Profile profile) {
		this.profile = profile;
	}

	/**
	 * @return the copyopCoinsList
	 */
	public ArrayList<CopyopCoins> getCopyopCoinsList() {
		return copyopCoinsList;
	}

	/**
	 * @param copyopCoinsList the copyopCoinsList to set
	 */
	public void setCopyopCoinsList(ArrayList<CopyopCoins> copyopCoinsList) {
		this.copyopCoinsList = copyopCoinsList;
	}

	/**
	 * @return the from
	 */
	public Date getFrom() {
		return from;
	}

	/**
	 * @param from the from to set
	 */
	public void setFrom(Date from) {
		this.from = from;
	}

	/**
	 * @return the to
	 */
	public Date getTo() {
		return to;
	}

	/**
	 * @param to the to to set
	 */
	public void setTo(Date to) {
		this.to = to;
	}

	/**
	 * @return the coinsConverted
	 */
	public long getCoinsConverted() {
		return coinsConverted;
	}

	/**
	 * @param coinsConverted the coinsConverted to set
	 */
	public void setCoinsConverted(long coinsConverted) {
		this.coinsConverted = coinsConverted;
	}

	/**
	 * @return the coinsMissed
	 */
	public long getCoinsMissed() {
		return coinsMissed;
	}

	/**
	 * @param coinsMissed the coinsMissed to set
	 */
	public void setCoinsMissed(long coinsMissed) {
		this.coinsMissed = coinsMissed;
	}

	/**
	 * @return the coinsStateId
	 */
	public long getCoinsStateId() {
		return coinsStateId;
	}

	/**
	 * @param coinsStateId the coinsStateId to set
	 */
	public void setCoinsStateId(long coinsStateId) {
		this.coinsStateId = coinsStateId;
	}

	/**
	 * @return the coinsBalance
	 */
	public long getCoinsBalance() {
		return coinsBalance;
	}

	/**
	 * @param coinsBalance the coinsBalance to set
	 */
	public void setCoinsBalance(long coinsBalance) {
		this.coinsBalance = coinsBalance;
	}

	/**
	 * @return the regrantCopyopCoins
	 */
	public CopyopCoins getRegrantCopyopCoins() {
		return regrantCopyopCoins;
	}

	/**
	 * @param regrantCopyopCoins the regrantCopyopCoins to set
	 */
	public void setRegrantCopyopCoins(CopyopCoins regrantCopyopCoins) {
		this.regrantCopyopCoins = regrantCopyopCoins;
	}
	

}