package il.co.etrader.backend.mbeans;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Serializable;
import java.io.Writer;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.Opportunity;
import com.anyoption.common.jms.BackendLevelsCache;

import il.co.etrader.backend.bl_managers.AdminManager;
import il.co.etrader.backend.bl_managers.InvestmentsManager;
import il.co.etrader.backend.bl_vos.OpportunityOddsGroup;
import il.co.etrader.backend.bl_vos.User;
import il.co.etrader.backend.util.Constants;
import il.co.etrader.backend.util.Utils;
import il.co.etrader.bl_managers.InvestmentsManagerBase;
import il.co.etrader.bl_vos.OpportunityTemplate;
import il.co.etrader.bl_vos.Skins;
import il.co.etrader.util.CommonUtil;

public class OpportunityTemplatesForm implements Serializable {

	private static final long serialVersionUID = -639463071574235263L;
	
	private static final Logger logger = Logger.getLogger(OpportunityTemplatesForm.class);
	
	private static final String DELEMITER = ",";

	private OpportunityTemplate template;
	private ArrayList list;
	private String marketGroup;
	private long active;
	private final int APPLY_TO_ONE_DAY_AHEAD = 1;
	private ArrayList oddsGroupLists;
	private ArrayList<OpportunityOddsGroup> oddsGroups= new ArrayList<OpportunityOddsGroup>();

	public OpportunityTemplatesForm() throws SQLException{
		marketGroup="0,0";
		active = 0;

		search();
	}

	public int getListSize() {
		return list.size();
	}

	public String updateInsert() throws SQLException {
		
		//validate oddsGroup
		if (template.getOpportunityTypeId() == Opportunity.TYPE_REGULAR && template.getOddsGroup() == null){
			FacesContext context=FacesContext.getCurrentInstance();
			FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR,
			CommonUtil.getMessage("error.sadmin.selectors.opportunity.template.empty", null),null);
			context.addMessage(null, fm);
			
			return null;
		} else if (template.getOpportunityTypeId() == Opportunity.TYPE_REGULAR && !isValidOddsGoupsOddsType(template.getOddsGroup(),template.getOddsTypeId())) {
			FacesContext context=FacesContext.getCurrentInstance();
			FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR,
			CommonUtil.getMessage("error.sadmin.selectors.opportunity.template.exist", null),null);
			context.addMessage(null, fm);
			
			return null;
		}
		
		//update opportunity_templates
		if ((!getUser().isHasSAdminRole()) || (template.getOpportunityTypeId() == Opportunity.TYPE_ONE_TOUCH)) {
			template.setOpportunityTypeId(Opportunity.TYPE_ONE_TOUCH);
			template.setTimeZone("Israel");
			template.setScheduled(InvestmentsManager.SCHEDULED_ONE_TOUCH);
		}
		//template.setOpportunityTypeId(opportunityTypeID);
		boolean res = InvestmentsManager.updateInsertOppTemplate(template, false);
		String faildToNotify = "";

		//update all opportunities of that opportunity template
		ArrayList<Opportunity> list = null;

		if (template.isChangeAllScheduled()){
			list = InvestmentsManager.getOpportunitiesByTemplate(template, APPLY_TO_ONE_DAY_AHEAD, false, true);
		}else{
			if(template.isChangeAll()){
				list = InvestmentsManager.getOpportunitiesByTemplate(template, APPLY_TO_ONE_DAY_AHEAD, template.isChangeAll(), true);
			}
		}
		if (list != null && (template.isChangeAll() || template.isChangeAllScheduled())) {
			FacesContext context = FacesContext.getCurrentInstance();
			BackendLevelsCache levelsCache = (BackendLevelsCache) context.getExternalContext().getApplicationMap().get("levelsCache");

			//update opportunity and notify service
			boolean resp = false;
			for (int i=0; i<list.size(); i++) {
				long oddsTypeId = template.getOddsTypeId();
				resp = InvestmentsManager.updateOpportunityOdds(list.get(i), oddsTypeId, template.getOddsGroup());
				if (!resp) {
					if (!levelsCache.notifyForOpportunityOddsChange(list.get(i).getId())) {
						faildToNotify += String.valueOf(list.get(i).getId()) + " ";
					}
				}
			}
		}
		FacesContext contextLocale = FacesContext.getCurrentInstance();
		list = InvestmentsManager.searchOppTemplates(Long.valueOf(marketGroup.split(",")[0]),Long.valueOf(marketGroup.split(",")[1]),getUser().isHasSAdminRole(),active, Skins.SKIN_REG_EN);
		if (faildToNotify.length() > 0) { //if some opp notify to service was faild we will display error msg
			FacesContext context = FacesContext.getCurrentInstance();
			String[] params = new String[1];
			params[0] = faildToNotify;
			FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_INFO,CommonUtil.getMessage("oppTemplates.odds.faild", params, Utils.getWriterLocale(context)),null);
			context.addMessage(null, fm);
            
		}
		return Constants.NAV_OPP_TEMPLATES;
	}

	public String initValues() throws SQLException{
		template=new OpportunityTemplate();
		template.setWriterId(AdminManager.getWriterId());
		template.setTimeFirstInvest("00:00");
		template.setTimeEstClosing("00:00");
		template.setTimeLastInvest("00:00");
		template.setOpportunityTypeId(Opportunity.TYPE_REGULAR);
		template.setActive(true);
		template.setOneTouchDecimalPoint(new Long(0));
		template.setChangeAll(false);
		template.setChangeAllScheduled(false);
		return Constants.NAV_OPP_TEMPLATE;
	}

	public String search() throws SQLException{
		FacesContext contextLocale = FacesContext.getCurrentInstance();
		list=InvestmentsManager.searchOppTemplates(Long.valueOf(marketGroup.split(",")[0]),Long.valueOf(marketGroup.split(",")[1]),getUser().isHasSAdminRole(),active, Skins.SKIN_REG_EN);
		CommonUtil.setTablesToFirstPage();
		return null;
	}

	public void createDropDownList() throws SQLException {
		oddsGroups = InvestmentsManager.getOpportunityOddsGroup();
		oddsGroupLists = new ArrayList();
		oddsGroupLists.add(new SelectItem("", " "));
		for (OpportunityOddsGroup oog : oddsGroups) {
			oddsGroupLists.add(new SelectItem(oog.getOddsGroup(), oog.getOddsGroupValue() + "  " + oog.getDefaultPair()));
		}
	}
	
	private Boolean isValidOddsGoupsOddsType (String oddsGroup, long oddsTypeId) throws SQLException{
		
		return InvestmentsManager.isExistOddsGoupsInOddsType(oddsGroup, oddsTypeId);
	}

	public ArrayList getList() {
		return list;
	}

	public void setlist(ArrayList list) {
		this.list = list;
	}

	public OpportunityTemplate getTemplate() {
		return template;
	}

	public void setTemplate(OpportunityTemplate template) {
		this.template = template;
	}

	public String getMarketGroup() {
		return marketGroup;
	}

	public void setMarketGroup(String marketGroup) {
		this.marketGroup = marketGroup;
	}
	
	/**
	 * @return the active
	 */
	public long getActive() {
		return active;
	}


	/**
	 * @param active the active to set
	 */
	public void setActive(long active) {
		this.active = active;
	}

	/**
	 * Constructs a <code>String</code> with all attributes
	 * in name = value format.
	 *
	 * @return a <code>String</code> representation
	 * of this object.
	 */
	@Override
	public String toString()
	{
	    final String TAB = " \n ";

	    String retValue = "";

	    retValue = "OpportunityTemplatesForm ( "
	        + super.toString() + TAB
	        + "template = " + this.template + TAB
	        + "list = " + this.list + TAB
	        + "marketGroup = " + this.marketGroup + TAB
	        + " )";

	    return retValue;
	}

	public User getUser() {
		FacesContext context = FacesContext.getCurrentInstance();
		return (User) context.getApplication().createValueBinding(Constants.BIND_USER).getValue(context);
	}

	/**
	 * @return the oddsGroupLists
	 */
	public ArrayList getOddsGroupLists() {
		try {
			createDropDownList();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return oddsGroupLists;
	}

	/**
	 * @param oddsGroupLists the oddsGroupLists to set
	 */
	public void setOddsGroupLists(ArrayList oddsGroupLists) {
		this.oddsGroupLists = oddsGroupLists;
	}
	
	public String getGroupValuePct( String oddsGroup) throws SQLException{
		
		return InvestmentsManager.getOpportunityPairsGroupPct(oddsGroup);
	}
	
	public String getScheduledTxt(long scheduled) {
        return InvestmentsManagerBase.getScheduledTxt(scheduled);
    }

    public void headersForCSVExport(File fileToSave) throws IOException {
	Writer output = null;

	FacesContext context = FacesContext.getCurrentInstance();
	User user = (User) context.getApplication().getExpressionFactory().createValueExpression
   		(context.getELContext(), Constants.BIND_USER, User.class).getValue(context.getELContext());

	logger.debug("Try to write in CSV File...");
	try {
	    output = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(fileToSave), "UTF-8"));
	    output.write("ID"); 			// 1 -> opportunities.templates.id
	    output.write(DELEMITER);
	    output.write("Market"); 			// 2 -> opportunities.templates.market_id
	    output.write(DELEMITER);
	    output.write("Up/Down"); 			// 3 -> opportunities.templates.up_down
	    output.write(DELEMITER);
	    output.write("Opened for investments at");	// 4 -> opportunities.templates.time_first_invest
	    output.write(DELEMITER);
	    output.write("Approximated settling time");	// 5 -> opportunities.templates.time_est_closing
	    output.write(DELEMITER);
	    output.write("Closed for investments at");	// 6 -> opportunities.templates.time_last_invest
	    output.write(DELEMITER);
	    output.write("Odds type"); 			// 7 -> opportunities.templates.odds_type_id
	    output.write(DELEMITER);
	    output.write("active"); 			// 8 -> opportunities.templates.active
	    output.write(DELEMITER);
	    if (user.isHasSAdminRole()) {
		output.write("Scheduled"); 		// 9 -> opportunities.templates.scheduled
		output.write(DELEMITER);
	    }
	    output.write("Time zone"); 			// 10 -> opportunities.templates.timezone
	    output.write(DELEMITER);
	    if (user.isHasSAdminRole()) {
		output.write("Full Day"); 		// 11 -> opportunities.templates.fullday
		output.write(DELEMITER);
	    }
	    if (user.isHasSAdminRole()) {
		output.write("Half a Day"); 		// 12 -> opportunities.templates.halfday
		output.write(DELEMITER);
	    }
	    output.write("Updated by"); 		// 13 -> opportunities.templates.writer_id
	    output.write(DELEMITER);
	    if (user.isHasSAdminRole()) {
		output.write("Reduce the winning odds before the expiration?"); // 14  -> opportunities.templates.deduct_win_odds
		output.write(DELEMITER);
	    }
	    output.write("Option type"); 		// 15 -> opportunities.templates.opportunity_type_id
	    output.write(DELEMITER);
	    output.write("Pair group value"); 		// 16 -> return.refun.selector.group.value
	    output.write(DELEMITER);
	    output.write("\n");
	} catch (Exception e) {
	    logger.error("Error when exporting CSV file:", e);
	} finally {
	    try {
		output.flush();
		output.close();
	    } catch (Exception e) {
		logger.error("Trader -> Markets header Data Form Error, Can't close file:", e);
	    }
	}
    }

    public void csvExport(File fileToSave) throws Exception {
	headersForCSVExport(fileToSave);

	Writer output = null;
	
	FacesContext context = FacesContext.getCurrentInstance();
	User user = (User) context.getApplication().getExpressionFactory().createValueExpression
   		(context.getELContext(), Constants.BIND_USER, User.class).getValue(context.getELContext());

	try {
	    ArrayList<OpportunityTemplate> opTempList = getList();
	    output = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(fileToSave, true), "UTF-8"));
	    for (OpportunityTemplate oppTempl : opTempList) {
		// opportunities.templates.id -> 1
		output.write(String.valueOf(oppTempl.getId()));
		output.write(DELEMITER);
		// opportunities.templates.market_id -> 2
		output.write(CommonUtil.appendDQ(oppTempl.getMarketName()));
		output.write(DELEMITER);
		// opportunities.templates.up_down -> 3
		output.write(oppTempl.getUp_downDesc());
		output.write(DELEMITER);
		// opportunities.templates.time_first_invest -> 4
		output.write(oppTempl.getTimeFirstInvest());
		output.write(DELEMITER);
		// opportunities.templates.time_est_closing -> 5
		output.write(String.valueOf(oppTempl.getTimeEstClosing()));
		output.write(DELEMITER);
		// opportunities.templates.time_last_invest -> 6
		output.write(oppTempl.getTimeLastInvest());
		output.write(DELEMITER);
		// opportunities.templates.odds_type_id -> 7
		output.write(oppTempl.getOddsTypeName());
		output.write(DELEMITER);
		// opportunities.templates.active -> 8
		output.write(String.valueOf(oppTempl.getActive()));
		output.write(DELEMITER);
		// opportunities.templates.scheduled -> 9
		if (user.isHasSAdminRole()) {
		    output.write(oppTempl.getScheduledTxt());
		    output.write(DELEMITER);
		}
		// opportunities.templates.timezone-> 10
		output.write(String.valueOf(oppTempl.getTimeZone()));
		output.write(DELEMITER);
		// opportunities.templates.fullday -> 11
		if (user.isHasSAdminRole()) {
		    switch (oppTempl.getFullDay()) {
		    case OpportunityTemplate.IS_FULL_DAY_NO:
			output.write("No");
			output.write(DELEMITER);
			break;
		    case OpportunityTemplate.IS_FULL_DAY_YES:
			output.write("Yes");
			output.write(DELEMITER);
			break;
		    case OpportunityTemplate.IS_FULL_DAY_SUNDAY:
			output.write("Sunday");
			output.write(DELEMITER);
			break;
		    default:
			break;
		    }
		}
		// opportunities.templates.halfday -> 12
		if (user.isHasSAdminRole()) {
		    output.write(String.valueOf(oppTempl.isHalfDay()));
		    output.write(DELEMITER);
		}
		// opportunities.templates.writer_id -> 13
		output.write(CommonUtil.appendDQ(oppTempl.getWriterName()));
		output.write(DELEMITER);
		// opportunities.templates.deduct_win_odds -> 14
		if (user.isHasSAdminRole()) {
		    output.write(oppTempl.isDeductWinOdds() ? "Yes" : "No");
		    output.write(DELEMITER);
		}
		// opportunities.templates.opportunity_type_id -> 15
		output.write(oppTempl.getOpTypeDesc());
		output.write(DELEMITER);
		// return.refun.selector.group.value -> 16
		output.write(CommonUtil.appendDQ(oppTempl.getOddsGroupValue()));
		output.write(DELEMITER);
		output.write("\n");
	    }
	} catch (Exception e) {
	    logger.error("Error when exporting CSV Data file:", e);
	} finally {
	    try {
		output.flush();
		output.close();
	    } catch (Exception e) {
		logger.error("Investment Data Form Error, Can't close file:", e);
	    }
	}
    }
	    
    /**
     * @return a pop up to save the given report or open it
     * @throws IOException
     * @throws NumberFormatException
     * @throws SQLException
     */
    public String exportTableToCSV() throws IOException, NumberFormatException, SQLException {
	// Set the filename
	Date dt = new Date();
	SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd_HHmmss");
	String filename = "option_templates_" + fmt.format(dt) + ".csv";

	// Setup the output
	String contentType = "text/csv";

	File tCSVfile = new File(CommonUtil.getProperty(Constants.FILES_PATH) + filename);
	FacesContext fc = FacesContext.getCurrentInstance();
	ExternalContext ex = fc.getExternalContext();
	HttpServletResponse response = (HttpServletResponse) ex.getResponse();
	try {
	    try {
		csvExport(tCSVfile);
	    } catch (Exception e) {
		logger.error("Problem creating CSV file due to: " + e);
		e.printStackTrace();
		return null;
	    }

	    int contentLength = (int) tCSVfile.length();

	    // just making sure nothing non current is retrieved
	    response.reset();

	    response.setContentType(contentType);
	    response.setContentLength(contentLength);
	    response.setCharacterEncoding("UTF-8");
	    response.setHeader("Content-disposition", "attachment; filename=" + filename);

	    // Write the file back out...
	    OutputStream outputStream = response.getOutputStream();
	    FileInputStream fis = new FileInputStream(tCSVfile);

	    try {
		int n = 0;
		byte[] buffer = new byte[4 * 1024];
		while ((n = fis.read(buffer)) != -1) {
		    outputStream.write(buffer, 0, n);
		}
		outputStream.flush();
	    } finally {
		fis.close();
		outputStream.close();
	    }
	} finally {
	    tCSVfile.delete();
	}

	fc.responseComplete();

	return null;
    }
}
