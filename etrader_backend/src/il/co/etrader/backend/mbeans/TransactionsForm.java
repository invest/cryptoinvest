package il.co.etrader.backend.mbeans;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;

import javax.el.ValueExpression;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.Transaction;
import com.anyoption.common.beans.base.Skin;
import com.anyoption.common.bl_vos.ChargeBack;
import com.anyoption.common.clearing.ClearingException;
import com.anyoption.common.clearing.InatecInfo;
import com.anyoption.common.managers.TransactionPostponedManager;
import com.anyoption.common.managers.TransactionsManagerBase.TransactionSource;

import il.co.etrader.backend.bl_managers.TransactionsManager;
import il.co.etrader.backend.bl_managers.UsersManager;
import il.co.etrader.backend.bl_vos.TransactionTotal;
import il.co.etrader.backend.bl_vos.User;
import il.co.etrader.backend.bl_vos.WriterWrapper;
import il.co.etrader.backend.util.Constants;
import il.co.etrader.backend.util.Utils;
import il.co.etrader.bl_managers.TransactionsManagerBase;
import il.co.etrader.bl_vos.Writer;
import il.co.etrader.util.CommonUtil;
import il.co.etrader.util.CommonUtil.selectItemComparator;
import il.co.etrader.util.ConstantsBase;

public class TransactionsForm implements Serializable {

	private static final long serialVersionUID = -4474865182429765978L;
	
	private transient static final Logger log = Logger
			.getLogger(TransactionsForm.class);

	private long id;
	private Date from;
	private Date to;
	private ArrayList<Transaction> transactionsList;
	private String typeId;
	private String statusId;
	private String receiptNum;
	private String ccNum;
	private ArrayList<SelectItem> selectionStatuses;
	private ArrayList<SelectItem> selectionTypes;
	private String userName;
	private long classId;
	private int skinId;
	private Transaction transaction;
	private ArrayList<SelectItem> countriesSI;
	private long countryId;
	private long skinBusinessCaseId;
	private long hasInvestments;
    private long startHour;
    private long endHour;
    private long currencyId;
    private long campaignId;
    private long writerId = -1;
    private ArrayList<SelectItem> writerFilterList = new ArrayList<SelectItem>();
    private Date arabicStartDate;
    private long paymentMethodId;
    private int hasBalance;
    private int salesDepositsType;
    private int salesDepositsSource;
    private boolean showcCCnum;
    private boolean showBankWithdrawal;
    private long chosenBankId;
    private long clearingProvider;
    private String ccBin = null;
    private long wasRerouted;
    private long isManualRouted;
	private String searchBy;
	private ArrayList<SelectItem> selectWasRerouted = new ArrayList<SelectItem>();
	private ArrayList<SelectItem> selectIsManualRoute = new ArrayList<SelectItem>();
	private ArrayList<SelectItem> selectIs3d = new ArrayList<SelectItem>();
	private String htmlBuffer;
	private int isPostponed;
	private boolean isDisplayPostponed;
	private long providerTransactionId;
	private List<Long> affiliatesKeys;
	private long threeD = -1;
	
	public TransactionsForm() throws SQLException, ParseException{
		FacesContext context = FacesContext.getCurrentInstance();
		User sessionuser = (User) context.getApplication().createValueBinding(Constants.BIND_USER).getValue(context);
		GregorianCalendar gc = new GregorianCalendar(TimeZone.getTimeZone(Utils.getWriter().getUtcOffset()));
		//set time as 00:00:00
		gc.set(Calendar.HOUR_OF_DAY, 0 + gc.getTimeZone().getRawOffset()/(1000*60*60));
		gc.set(Calendar.MINUTE, 0);
		gc.set(Calendar.SECOND, 0);
		gc.add(Calendar.DAY_OF_MONTH, 1);
		to = gc.getTime();
		gc.add(Calendar.DAY_OF_MONTH, -1);
		if(sessionuser.isSupportSelected()){
			gc.add(Calendar.YEAR, -1);
		}else {
			gc.add(GregorianCalendar.DAY_OF_MONTH, -7);
		}
		from = gc.getTime();
		typeId = "0";
		hasInvestments = 0 ;
		searchBy = "0";
		statusId = "0";
		receiptNum = "";
		ccNum = "";
		userName = "";
		classId = ConstantsBase.USER_CLASS_COMPANY_OR_PRIVATE;   // deafult to real account
		startHour=99;
		endHour=99;
		paymentMethodId = 0;
		hasBalance = 0;
		salesDepositsType = 0;
		wasRerouted = 0; //no.
		isPostponed = 0;
		isDisplayPostponed = true;
		
        if (!sessionuser.isSupportSelected() && !sessionuser.isRetentionSelected() && !sessionuser.isRetentionMSelected()) {
        	 typeId = String.valueOf(TransactionsManager.TRANS_TYPE_ALL_DEPOSIT);
        	 statusId = String.valueOf(TransactionsManager.TRANS_STATUS_PANDING_AND_SUCCESS);
        	 gc.add(GregorianCalendar.DAY_OF_MONTH,7);
     	     from = gc.getTime();
        }

		if (sessionuser.isSupportSelected() || sessionuser.isRetentionSelected() || sessionuser.isRetentionMSelected()) {
			classId = ConstantsBase.USER_CLASS_ALL;
		}
		selectionTypes = new ArrayList<SelectItem>();
		
		Map<Long, String> transactionTypes = new HashMap<Long, String>();
		transactionTypes = TransactionsManager.getAllVisibleTransactionTypesList();
		Locale locale = Utils.getWriterLocale(context);
		
		Iterator it = transactionTypes.entrySet().iterator();
	    while (it.hasNext()) {
	        Map.Entry pair = (Map.Entry)it.next();
	        selectionTypes.add(new SelectItem(pair.getKey(), CommonUtil.getMessage((String) pair.getValue(), null, locale)));
	    }
	    Collections.sort(selectionTypes, new CommonUtil.selectItemComparator());
		selectionTypes.add(0, new SelectItem(String.valueOf(0),CommonUtil.getMessage("transactions.all.type", null, locale)));
		selectionTypes.add(1, new SelectItem(String.valueOf(TransactionsManager.TRANS_TYPE_ALL_WITHDRAW),CommonUtil.getMessage("transactions.all.withdraw", null, locale)));
		selectionTypes.add(2, new SelectItem(String.valueOf(TransactionsManager.TRANS_TYPE_ALL_DEPOSIT),CommonUtil.getMessage("transactions.all.deposit", null, locale)));

        // fix the status filter
	    updateStatuses(Long.valueOf(typeId));

	    // For arabic partner writer we want users that created after the arabic partner starting date
	    long currentWriterId = Utils.getWriter().getWriter().getId();
	    if ( currentWriterId == Constants.PARTNER_ARABIC_ID){
			String enumerator = CommonUtil.getEnum(Constants.ENUM_PARTNER_ARABIC_DATE_ENUMERATOR,Constants.ENUM_PARTNER_ARABIC_DATE_CODE);
			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
			arabicStartDate = sdf.parse(enumerator);
	    }
	    selectWasRerouted.add(new SelectItem(new Long(0), CommonUtil.getMessage("general.all", null)));
	    selectWasRerouted.add(new SelectItem(new Long(1),CommonUtil.getMessage("no", null)));
	    selectWasRerouted.add(new SelectItem(new Long(2),CommonUtil.getMessage("yes", null)));

	    selectIs3d.add(new SelectItem(new Long(-1), CommonUtil.getMessage("general.all", null)));
	    selectIs3d.add(new SelectItem(new Long(0),CommonUtil.getMessage("no", null)));
	    selectIs3d.add(new SelectItem(new Long(1),CommonUtil.getMessage("yes", null)));
	    
	    isManualRouted = -1;
	    selectIsManualRoute.add(new SelectItem(new Long(-1), CommonUtil.getMessage("general.all", null)));
	    selectIsManualRoute.add(new SelectItem(new Long(0),CommonUtil.getMessage("no", null)));
	    selectIsManualRoute.add(new SelectItem(new Long(1),CommonUtil.getMessage("yes", null)));
        affiliatesKeys = new ArrayList<Long>();
        affiliatesKeys.add((long) 0);
	    
		updateTransactionsList();
		showcCCnum = false;
        showBankWithdrawal = false;
	}

	
	public void cancelMaintenanceFee(){
		try {
			if(!TransactionsManagerBase.isMaintenanceFeeCanceled(transaction.getId())){
				com.anyoption.common.beans.Transaction cancel = createCancelFeeTransaction(transaction);
				TransactionsManagerBase.cancelMaintenanceFee(cancel, Utils.getWriter().getWriter().getId());
			}else {
				String msg = "This Maintenance fee was already cancelled !";
				FacesContext context = FacesContext.getCurrentInstance();
				FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_INFO, msg, null);
				context.addMessage(null, fm);
			}
		} catch (SQLException e) {
			String msg = "Can't cancel Maintenance fee !";
			FacesContext context = FacesContext.getCurrentInstance();
			FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_INFO, msg, null);
			context.addMessage(null, fm);
		}
	}
	
	com.anyoption.common.beans.Transaction createCancelFeeTransaction(com.anyoption.common.beans.Transaction src){
		com.anyoption.common.beans.Transaction tran = new com.anyoption.common.beans.Transaction();
		tran.setAmount(src.getAmount());
		tran.setDescription("maintenance.fee.cancellation");
		tran.setCurrency(src.getCurrency());
		tran.setIp(CommonUtil.getIPAddress());
		tran.setProcessedWriterId(Utils.getWriter().getWriter().getId());
		tran.setTimeSettled(new Date());
		tran.setTimeCreated(new Date());
		tran.setUtcOffsetCreated(Utils.getWriter().getUtcOffset());
		tran.setUtcOffsetSettled(Utils.getWriter().getUtcOffset());
		tran.setTypeId(TransactionsManagerBase.TRANS_TYPE_MAINTENANCE_FEE_CANCEL);
		tran.setUserId(src.getUserId());
		tran.setWriterId(Utils.getWriter().getWriter().getId());
		tran.setStatusId(TransactionsManagerBase.TRANS_STATUS_SUCCEED);
		tran.setComments("Manually canceled from backend");
		tran.setSkin(src.getSkin());
		tran.setReferenceId(new BigDecimal(src.getId()));
		tran.setLoginId(src.getLoginId());
		return tran;
	}
	
	  public String updateStatuses(ValueChangeEvent event) {
		  String value = (String)event.getNewValue();
		  if (!CommonUtil.isParameterEmptyOrNull(value)){
	          updateStatuses(Long.parseLong(value));
	          if ((Long.parseLong(value)== TransactionsManager.TRANS_TYPE_CC_DEPOSIT
	  				|| Long.parseLong(value)== TransactionsManager.TRANS_TYPE_CC_WITHDRAW)) {
	        	  showcCCnum = true;
	          } else {
	        	  showcCCnum = false;
	          }
		  }
          return null;
	  }

	  public void updateStatuses(long val) {
        selectionStatuses = new ArrayList<SelectItem>();
        FacesContext context = FacesContext.getCurrentInstance();
        if (val == 0 || val == TransactionsManager.TRANS_TYPE_ALL_WITHDRAW ||
                val == TransactionsManager.TRANS_TYPE_CHEQUE_WITHDRAW      ||
                val == TransactionsManagerBase.TRANS_TYPE_CC_WITHDRAW      ||
                val == TransactionsManagerBase.TRANS_TYPE_PAYPAL_WITHDRAW      ||
                val == TransactionsManagerBase.TRANS_TYPE_BANK_WIRE_WITHDRAW    ||
                val == TransactionsManagerBase.TRANS_TYPE_EFT_WITHDRAW
                ) {

            selectionStatuses.add(new SelectItem(String.valueOf(0),CommonUtil.getMessage("transactions.all.status", null, Utils.getWriterLocale(context))));
            selectionStatuses.add(new SelectItem(String.valueOf(TransactionsManager.TRANS_STATUS_STARTED),CommonUtil.getMessage("trans.status.started", null, Utils.getWriterLocale(context))));
            selectionStatuses.add(new SelectItem(String.valueOf(TransactionsManager.TRANS_STATUS_SUCCEED),CommonUtil.getMessage("trans.status.succeed", null, Utils.getWriterLocale(context))));
            selectionStatuses.add(new SelectItem(String.valueOf(TransactionsManager.TRANS_STATUS_FAILED),CommonUtil.getMessage("trans.status.failed", null, Utils.getWriterLocale(context))));
            selectionStatuses.add(new SelectItem(String.valueOf(TransactionsManager.TRANS_STATUS_REQUESTED),CommonUtil.getMessage("trans.status.requested", null, Utils.getWriterLocale(context))));
            selectionStatuses.add(new SelectItem(String.valueOf(TransactionsManager.TRANS_STATUS_APPROVED),CommonUtil.getMessage("trans.status.approved", null, Utils.getWriterLocale(context))));
            selectionStatuses.add(new SelectItem(String.valueOf(TransactionsManager.TRANS_STATUS_CANCELED), CommonUtil.getMessage("trans.status.canceled", null, Utils.getWriterLocale(context))));
            selectionStatuses.add(new SelectItem(String.valueOf(TransactionsManager.TRANS_STATUS_REVERSE_WITHDRAW),CommonUtil.getMessage("trans.status.reverse", null, Utils.getWriterLocale(context))));
            selectionStatuses.add(new SelectItem(String.valueOf(TransactionsManager.TRANS_STATUS_SECOND_APPROVAL),CommonUtil.getMessage("trans.status.second.approval", null, Utils.getWriterLocale(context))));

            if (val == 0) {
            	selectionStatuses.add(new SelectItem(String.valueOf(TransactionsManager.TRANS_STATUS_PENDING),CommonUtil.getMessage("trans.status.pending", null, Utils.getWriterLocale(context))));
            	selectionStatuses.add(new SelectItem(String.valueOf(TransactionsManager.TRANS_STATUS_CANCELED_ETRADER),CommonUtil.getMessage("trans.status.canceledbyetrader", null, Utils.getWriterLocale(context))));
            	selectionStatuses.add(new SelectItem(String.valueOf(TransactionsManager.TRANS_STATUS_CANCEL_M_N_R),CommonUtil.getMessage("trans.status.cancel.m.n.r", null, Utils.getWriterLocale(context))));
            	selectionStatuses.add(new SelectItem(String.valueOf(TransactionsManager.TRANS_STATUS_CANCEL_S_M),CommonUtil.getMessage("trans.status.cancel.s.m", null, Utils.getWriterLocale(context))));
            }

        } else if (val == TransactionsManager.TRANS_TYPE_ADMIN_DEPOSIT ||
                val == TransactionsManager.TRANS_TYPE_ADMIN_WITHDRAW ||
                val == TransactionsManager.TRANS_TYPE_REVERSE_WITHDRAW ||
                val == TransactionsManagerBase.TRANS_TYPE_BONUS_DEPOSIT ||
                val == TransactionsManagerBase.TRANS_TYPE_BONUS_WITHDRAW ||
                val == TransactionsManager.TRANS_TYPE_FIX_BALANCE_DEPOSIT ||
                val == TransactionsManager.TRANS_TYPE_FIX_BALANCE_WITHDRAW ||
                val == TransactionsManager.TRANS_TYPE_DEPOSIT_BY_COMPANY ||
                val == TransactionsManager.TRANS_TYPE_HOMO_FEE ||
				val == 8 || //TransactionsManager.TRANS_TYPE_WITHDRAW_FEE ||
				val == 11 || //TransactionsManagerBase.TRANS_TYPE_CC_WITHDRAW_FEE ||
				val == 27 )  //TransactionsManager.TRANS_TYPE_PAYPAL_WITHDRAW_FEE || 
        					{                
                selectionStatuses.add(new SelectItem(String.valueOf(TransactionsManager.TRANS_STATUS_SUCCEED),CommonUtil.getMessage("trans.status.succeed", null, Utils.getWriterLocale(context))));
        } else {
            selectionStatuses.add(new SelectItem(String.valueOf(0),CommonUtil.getMessage("transactions.all.status", null, Utils.getWriterLocale(context))));
            selectionStatuses.add(new SelectItem(String.valueOf(TransactionsManager.TRANS_STATUS_STARTED),CommonUtil.getMessage("trans.status.started", null, Utils.getWriterLocale(context))));
            selectionStatuses.add(new SelectItem(String.valueOf(TransactionsManager.TRANS_STATUS_PENDING),CommonUtil.getMessage("trans.status.pending", null, Utils.getWriterLocale(context))));
            selectionStatuses.add(new SelectItem(String.valueOf(TransactionsManager.TRANS_STATUS_SUCCEED),CommonUtil.getMessage("trans.status.succeed", null, Utils.getWriterLocale(context))));
            selectionStatuses.add(new SelectItem(String.valueOf(TransactionsManager.TRANS_STATUS_FAILED),CommonUtil.getMessage("trans.status.failed", null, Utils.getWriterLocale(context))));
            selectionStatuses.add(new SelectItem(String.valueOf(TransactionsManager.TRANS_STATUS_REQUESTED),CommonUtil.getMessage("trans.status.requested", null, Utils.getWriterLocale(context))));
            selectionStatuses.add(new SelectItem(String.valueOf(TransactionsManager.TRANS_STATUS_APPROVED),CommonUtil.getMessage("trans.status.approved", null, Utils.getWriterLocale(context))));
            selectionStatuses.add(new SelectItem(String.valueOf(TransactionsManager.TRANS_STATUS_CANCELED_ETRADER),CommonUtil.getMessage("trans.status.canceledbyetrader", null, Utils.getWriterLocale(context))));
            selectionStatuses.add(new SelectItem(String.valueOf(TransactionsManager.TRANS_STATUS_CANCELED_FRAUDS),CommonUtil.getMessage("trans.status.canceledbyfrauds", null, Utils.getWriterLocale(context))));
            selectionStatuses.add(new SelectItem(String.valueOf(TransactionsManager.TRANS_STATUS_AUTHENTICATE),CommonUtil.getMessage("trans.status.authenticate", null, Utils.getWriterLocale(context))));
            selectionStatuses.add(new SelectItem(String.valueOf(TransactionsManager.TRANS_STATUS_IN_PROGRESS),CommonUtil.getMessage("trans.status.in.progress", null, Utils.getWriterLocale(context))));
            selectionStatuses.add(new SelectItem(String.valueOf(TransactionsManager.TRANS_STATUS_PANDING_AND_SUCCESS),CommonUtil.getMessage("trans.status.in.pending.and.success", null, Utils.getWriterLocale(context))));
            selectionStatuses.add(new SelectItem(String.valueOf(TransactionsManager.TRANS_STATUS_CANCEL_S_DEPOSIT),CommonUtil.getMessage("trans.status.cancel.succ.deposit", null, Utils.getWriterLocale(context))));
        	selectionStatuses.add(new SelectItem(String.valueOf(TransactionsManager.TRANS_STATUS_CANCEL_M_N_R),CommonUtil.getMessage("trans.status.cancel.m.n.r", null, Utils.getWriterLocale(context))));
        	selectionStatuses.add(new SelectItem(String.valueOf(TransactionsManager.TRANS_STATUS_CANCEL_S_M),CommonUtil.getMessage("trans.status.cancel.s.m", null, Utils.getWriterLocale(context))));
        	selectionStatuses.add(new SelectItem(String.valueOf(TransactionsManager.TRANS_STATUS_CHARGE_BACK),CommonUtil.getMessage("trans.status.charge.back", null, Utils.getWriterLocale(context))));
        	selectionStatuses.add(new SelectItem(String.valueOf(TransactionsManager.TRANS_STATUS_SECOND_APPROVAL),CommonUtil.getMessage("trans.status.second.approval", null, Utils.getWriterLocale(context))));
        }
        ccNum="";
        if (val == TransactionsManagerBase.TRANS_TYPE_BANK_WIRE_WITHDRAW  || val == TransactionsManagerBase.TRANS_TYPE_EFT_WITHDRAW){
            showBankWithdrawal = true;
        }else {
            showBankWithdrawal = false;
        }
        isDisplayPostponed = true;
        if (val != TransactionsManagerBase.TRANS_TYPE_ALL && val != TransactionsManagerBase.TRANS_TYPE_CC_DEPOSIT
        		&& val != TransactionsManagerBase.TRANS_TYPE_CC_DEPOSIT_REROUTE && val != TransactionsManagerBase.TRANS_TYPE_ALL_DEPOSIT) {
        	isPostponed = 0;
        	isDisplayPostponed = false;
        }
      }

	public String initChargeBack(){

		FacesContext context=FacesContext.getCurrentInstance();
		User user= (User)context.getApplication().createValueBinding(Constants.BIND_USER).getValue(context);
		ChargeBack cb= (ChargeBack)context.getApplication().createValueBinding(Constants.BIND_CHARGE_BACK).getValue(context);

		cb.setStatusId((long)TransactionsManager.CHARGE_BACK_STATUS_STARTED);
		cb.setDescription("");
		cb.setId(0);
		cb.setAmount(transaction.getAmount());
		cb.setTimeCreated(new Date());
		cb.setWriter(context.getExternalContext().getRemoteUser());
		cb.setTransactionId(transaction.getId());
		cb.setUserId(transaction.getUserId());
		cb.setUserName(transaction.getUserName());
		cb.setUserFirstName(transaction.getUserFirstName());
		cb.setUserLastName(transaction.getUserLastName());
		cb.setState(ChargeBack.STATE_CHB);
		if (!user.isSupportSelected() && !user.isRetentionSelected() && !user.isRetentionMSelected()) {
			cb.setNavigation(Constants.NAV_ADMIN_TRANSACTIONS);
			return Constants.NAV_ADMIN_CHARGE_BACK;
		}
		else if (user.isRetentionSelected() || user.isRetentionMSelected()) {
			cb.setNavigation(Constants.NAV_RETENTION_TRANSACTIONS);
			return Constants.NAV_RETENTION_CHARGE_BACK;
		} else {
			cb.setNavigation(Constants.NAV_TRANSACTIONS);
			return Constants.NAV_CHARGE_BACK;
		}
	}


	/**
	 * Canceled by anyoption
	 * @return
	 * @throws SQLException
	 */
	public String cancelDeposit() throws SQLException{
		FacesContext context = FacesContext.getCurrentInstance();
		int cancelStatus = TransactionsManagerBase.TRANS_STATUS_CANCELED_ETRADER;
		if (transaction.isWireDeposit() || transaction.isCashDeposit()) {  // Wire deposit
			cancelStatus = TransactionsManagerBase.TRANS_STATUS_CANCEL_M_N_R;
		}

		TransactionsManager.cancelDeposit(transaction, cancelStatus);
		updateTransactionsList();

		User user = (User)context.getApplication().createValueBinding(Constants.BIND_USER).getValue(context);
		if (user.isSupportSelected()) {
			UsersManager.loadUserStripFields(user, true, null);
		}

		context.renderResponse();
		return null;
	}

	/**
	 * Canceled by Sadmin - support mistake
	 * @return
	 * @throws SQLException
	 */
	public String cancelDepositSupportMistake() throws SQLException{
		FacesContext context = FacesContext.getCurrentInstance();
		TransactionsManager.cancelDeposit(transaction, TransactionsManagerBase.TRANS_STATUS_CANCEL_S_M);
		updateTransactionsList();
		User user = (User)context.getApplication().createValueBinding(Constants.BIND_USER).getValue(context);
		if (user.isSupportSelected()) {
			UsersManager.loadUserStripFields(user, true, null);
		}
		context.renderResponse();
		return null;
	}

	/**
	 * Canceled by frauds
	 * @return
	 * @throws SQLException
	 */
	public String cancelDepositFrauds() throws SQLException{

		FacesContext context = FacesContext.getCurrentInstance();

		TransactionsManager.cancelDeposit(transaction, TransactionsManagerBase.TRANS_STATUS_CANCELED_FRAUDS);
		updateTransactionsList();

		User user = (User)context.getApplication().createValueBinding(Constants.BIND_USER).getValue(context);
		if (user.isSupportSelected()) {
			UsersManager.loadUserStripFields(user, true, null);
		}

		context.renderResponse();
		return null;
	}

	public String editChargeBack() throws SQLException{

		FacesContext context=FacesContext.getCurrentInstance();
		User user= (User)context.getApplication().createValueBinding(Constants.BIND_USER).getValue(context);

		if (transaction.getChargeBackId()!=null) {
			com.anyoption.common.bl_vos.ChargeBack cb=TransactionsManager.getChargeBack(transaction.getChargeBackId().longValue());

			cb.setUserId(transaction.getUserId());
			cb.setUserName(transaction.getUserName());
			cb.setUserFirstName(transaction.getUserFirstName());
			cb.setUserLastName(transaction.getUserLastName());
			cb.setAmount(transaction.getAmount());
			cb.setTransactionId(transaction.getId());
			if (!user.isSupportSelected()) {
				cb.setNavigation(Constants.NAV_ADMIN_TRANSACTIONS);
			} else {
				cb.setNavigation(Constants.NAV_TRANSACTIONS);
			}
			ValueExpression ve = context.getApplication().getExpressionFactory().createValueExpression(context.getELContext(), Constants.BIND_CHARGE_BACK, ChargeBack.class);
			ve.getValue(context.getELContext());
			ve.setValue(context.getELContext(), cb);
		}

		if (!user.isSupportSelected() && !user.isRetentionSelected() && !user.isRetentionMSelected() ) {
			return Constants.NAV_ADMIN_CHARGE_BACK;
		} else if(user.isRetentionSelected() || user.isRetentionMSelected()) {
			return Constants.NAV_RETENTION_CHARGE_BACK;
		} else {
			return Constants.NAV_CHARGE_BACK;
		}
	}


	public int getListSize() {
		return transactionsList.size();
	}

	public ArrayList getTransactionsList() {
		return transactionsList;
	}

	public void setTransactionsList(ArrayList<Transaction> transactionsList) {
		this.transactionsList = transactionsList;
	}

	public String updateTransactionsList() throws SQLException{
		GregorianCalendar gc = new GregorianCalendar();
		Date fromtmp = new Date(from.getTime());
		Date totmp = new Date(to.getTime());

		FacesContext context = FacesContext.getCurrentInstance();
		User user =(User)context.getApplication().createValueBinding(Constants.BIND_USER).getValue(context);
		int ccBinInt;
		if (null != ccBin && !ccBin.isEmpty() && ccBin.length()>0) {
		    ccBinInt = Integer.parseInt(ccBin);
		} else {
		    ccBinInt = 0;
		}
		

		userName = userName.toUpperCase();
		if (null != affiliatesKeys && affiliatesKeys.size() > 1 && affiliatesKeys.contains("0")) {
			affiliatesKeys.clear();
			affiliatesKeys.add((long) 0);
		}
		if (!user.isSupportSelected() && !user.isRetentionSelected() && !user.isRetentionMSelected()) {


			if (startHour != 99) {
				gc.setTime(fromtmp);
				gc.set(Calendar.HOUR_OF_DAY,(int)startHour);
				fromtmp = gc.getTime();
			}

			if (endHour != 99) {
				gc.setTime(totmp);
				gc.set(Calendar.HOUR_OF_DAY,(int)endHour);
				totmp = gc.getTime();
			}

			transactionsList = TransactionsManager.getTransactions(0,fromtmp,totmp,Long.valueOf(typeId).longValue(),
					Long.valueOf(statusId).longValue(),searchBy,receiptNum,ccNum,false,userName,classId,getSkinFilter(),
					countryId, skinBusinessCaseId,hasInvestments, startHour, endHour, currencyId, campaignId, writerId,0,
					false, arabicStartDate,paymentMethodId, hasBalance, salesDepositsType, salesDepositsSource, chosenBankId,
					clearingProvider, ccBinInt, wasRerouted, isManualRouted, id, isPostponed, providerTransactionId, 
					Utils.getArrayToString(affiliatesKeys), threeD);

			HashMap<Long, String> h = new HashMap<Long, String>();
			for (Transaction t : transactionsList) {
				h.put(new Long(t.getCountryId()), t.getCountryName());
			}
			countriesSI = CommonUtil.hashMap2SortedArrayList(h);
			createWriterFilter();
            addTotals(user);

		} else {
			transactionsList = TransactionsManager.getTransactions(user.getId(),from,to,Long.valueOf(typeId).longValue(),
					Long.valueOf(statusId).longValue(),searchBy,receiptNum,ccNum,false,userName,classId,getSkinFilter(),
					new Long(0), new Long(0), new Long(0), new Long(99), new Long(99), new Long(0), new Long(0), new Long(-2),
					0,false, arabicStartDate, paymentMethodId, hasBalance, salesDepositsType, salesDepositsSource, chosenBankId, 
					clearingProvider, ccBinInt, wasRerouted, isManualRouted, id, isPostponed, providerTransactionId, 
					Utils.getArrayToString(affiliatesKeys), threeD);
			addbookBack(user);
		}

		CommonUtil.setTablesToFirstPage();
		return null;
	}

	private void addbookBack(User user) {
		if (transactionsList.size() > 0) {

			ArrayList<Transaction> newList = new ArrayList<Transaction>();
			TransactionTotal bookBack = new TransactionTotal(2, ConstantsBase.CURRENCY_BASE_ID, new Date(), Utils.getUser().getUtcOffset());
			bookBack.setId(0);
			bookBack.setAmount(0);
			bookBack.setTotalprofitAmount(0);
			bookBack.setBookBack(true);
			
			for (int i = 0; i < transactionsList.size(); i++) {
				Transaction tran = (Transaction) transactionsList.get(i);

				if (i > 0 && i % Long.valueOf(user.getRowsByMenu()) == 0) {
					newList.add(new com.anyoption.common.beans.Transaction(1));
					newList.add(new com.anyoption.common.beans.Transaction(1));
					newList.add(new com.anyoption.common.beans.Transaction(1));
					newList.add(bookBack);

					bookBack = new TransactionTotal(2, ConstantsBase.CURRENCY_BASE_ID, new Date(), Utils.getUser().getUtcOffset());
					bookBack.setTotalprofitAmount(0);
					bookBack.setBookBack(true);
				}
				newList.add(tran);

				if (tran.isCcDeposit() && tran.isCreditAvailable()
						&& tran.getStatusId() == TransactionsManagerBase.TRANS_STATUS_SUCCEED) {
					bookBack.setTotalprofitAmount(bookBack.getTotalprofitAmount() + tran.getBaseAmount());
				}
			}

			newList.add(new com.anyoption.common.beans.Transaction(1));
			newList.add(new com.anyoption.common.beans.Transaction(1));
			newList.add(new com.anyoption.common.beans.Transaction(1));
			newList.add(bookBack);

			transactionsList = newList;
		}
	}
		

	/**
     * for admin add page total and global total to the table
     * @param user
	 */
	private void addTotals(User user) {
        if (transactionsList.size() > 0) {

            ArrayList<Transaction> newList= new ArrayList<Transaction>();
            TransactionTotal totalInv = new TransactionTotal(2, currencyId, new Date(), Utils.getUser().getUtcOffset());
            totalInv.setId(0);
            totalInv.setAmount(0);
            totalInv.setTotalCreditAmount(0);
            totalInv.setTotalDebtAmount(0);
            totalInv.setTotalprofitAmount(0);

            TransactionTotal totalInv2 = new TransactionTotal(3, currencyId, new Date(), Utils.getUser().getUtcOffset());
            totalInv2.setId(0);
            totalInv2.setAmount(0);
            totalInv2.setTotalCreditAmount(0);
            totalInv2.setTotalDebtAmount(0);
            totalInv2.setTotalprofitAmount(0);

            for (int i = 0; i < transactionsList.size(); i++) {
                Transaction tran =  transactionsList.get(i);
                if (tran.getCredit()) {
                    totalInv2.setTotalCreditAmount(totalInv2.getTotalCreditAmount() + (isCurrencyAllSelected() ? tran.getBaseAmount() : tran.getAmount()));
                } else {
                    totalInv2.setTotalDebtAmount(totalInv2.getTotalDebtAmount() + (isCurrencyAllSelected() ? tran.getBaseAmount() : tran.getAmount()));
                }
            }
            totalInv2.setTotalprofitAmount(totalInv2.getTotalCreditAmount() - totalInv2.getTotalDebtAmount());

            for (int i = 0; i < transactionsList.size(); i++) {
                Transaction tran = (Transaction) transactionsList.get(i);

                if (i > 0 && i % Long.valueOf(user.getRowsByMenu()) == 0) {
                    newList.add(new com.anyoption.common.beans.Transaction(1));
                    newList.add(totalInv);
                    newList.add(new com.anyoption.common.beans.Transaction(1));
                    newList.add(totalInv2);

                    totalInv = new TransactionTotal(2, currencyId, new Date(), Utils.getUser().getUtcOffset());
                    totalInv.setTotalCreditAmount(0);
                    totalInv.setTotalDebtAmount(0);
                    totalInv.setTotalprofitAmount(0);
                }
                newList.add(tran);

                if (tran.getCredit()) {
                    totalInv.setTotalCreditAmount(totalInv.getTotalCreditAmount() + (isCurrencyAllSelected() ? tran.getBaseAmount() : tran.getAmount()));
                } else {
                    totalInv.setTotalDebtAmount(totalInv.getTotalDebtAmount() + (isCurrencyAllSelected() ? tran.getBaseAmount() : tran.getAmount()));
                }
                totalInv.setTotalprofitAmount(totalInv.getTotalCreditAmount() - totalInv.getTotalDebtAmount());
            }

            newList.add(new com.anyoption.common.beans.Transaction(1));
            newList.add(totalInv);
            newList.add(new com.anyoption.common.beans.Transaction(1));
            newList.add(totalInv2);

            transactionsList = newList;
        }

    }

    /**
	 * Inatec status request call
	 * @return
	 * @throws ClearingException
	 * @throws SQLException
	 */
	public String statusReqCall() throws ClearingException, SQLException {
		FacesContext context = FacesContext.getCurrentInstance();
		WriterWrapper w = (WriterWrapper)context.getApplication().createValueBinding(Constants.BIND_WRITER_WRAPPER).getValue(context);

		InatecInfo info = TransactionsManagerBase.finishInatecTransaction(transaction, String.valueOf(transaction.getId()), null, w.getWriter().getId(),
				transaction.getUtcOffsetCreated(), false, 0L);

		updateTransactionsList();

		User user = (User)context.getApplication().createValueBinding(Constants.BIND_USER).getValue(context);
		TransactionsManagerBase.afterDepositAction(user, transaction, TransactionSource.TRANS_FROM_BACKEND);
		if (user.isSupportSelected()) {
			UsersManager.loadUserStripFields(user, true, null);
		}

		String msg = "";
		if (info.isSuccessful()) {
			msg = "transactions.status.request.call.success";
		} else {
			if (info.isNeedNotification()) {
				msg = "transactions.status.request.call.pending";
			} else {
				msg = "transactions.status.request.call.failed";
			}
		}

		FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_INFO,
				CommonUtil.getMessage(msg, null, Utils.getWriterLocale(context)), null);
		context.addMessage(null, fm);


		context.renderResponse();

		return null;
	}
	public String selectTransactions() throws SQLException{

		transactionsList = TransactionsManager.getTransactions(transaction.getId(),transaction.getReferenceId());
		CommonUtil.setTablesToFirstPage();
		return null;
	}
	
	/**
	 * Export data to excel
	 * @throws IOException
	 */
	public void exportHtmlTableToExcel() throws IOException {

		//Set the filename
        Date dt = new Date();
        SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd_HHmmss");
        String filename = fmt.format(dt) + ".xls";
        htmlBuffer=
        	"<html><head><meta http-equiv=\"Content-Type\" content=\"text/html; CHARSET=UTF-8\"></head>" + htmlBuffer + "</html>";

        //Setup the output
        String contentType = "application/vnd.ms-excel";
        FacesContext fc = FacesContext.getCurrentInstance();
        filename = "transaction_report_"+ filename;
        HttpServletResponse response = (HttpServletResponse)fc.getExternalContext().getResponse();
        response.setHeader("Content-disposition", "attachment; filename=" + filename);
        response.setContentType(contentType);
        response.setCharacterEncoding("UTF-8");

        //Write the table back out
        PrintWriter out = response.getWriter();
        out.print(htmlBuffer);
        out.close();
        fc.responseComplete();
    }
	
	public String getHtmlBuffer() {
		return htmlBuffer;
	}


	public void setHtmlBuffer(String htmlBuffer) {
		this.htmlBuffer = htmlBuffer;
	}

	public Date getFrom() {
		return from;
	}

	public void setFrom(Date from) {
		this.from = from;
	}

	public Date getTo() {
		return to;
	}

	public void setTo(Date to) {
		this.to = to;
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		if (id == null) {
			this.id = 0;
		} else {
			this.id = id;
		}
	}

	public Transaction getTransaction() {
		return transaction;
	}

	public void setTransaction(Transaction transaction) {
		this.transaction = transaction;
	}

	public String getSearchBy() {
		return searchBy;
	}
	public void setSearchBy(String searchBy) {
		this.searchBy = searchBy;
	}

	public ArrayList getSelectionStatuses() {
        Collections.sort(selectionStatuses, new selectItemComparator());
		return selectionStatuses;
	}

	public void setSelectionStatuses(ArrayList<SelectItem> selectionStatuses) {
		this.selectionStatuses = selectionStatuses;
	}

	public ArrayList getSelectionTypes() {
		return selectionTypes;
	}

	public void setSelectionTypes(ArrayList<SelectItem> selectionTypes) {
		this.selectionTypes = selectionTypes;
	}

	public String getStatusId() {
		return statusId;
	}

	public void setStatusId(String statusId) {
		this.statusId = statusId;
	}

	public String getTypeId() {
		return typeId;
	}

	public void setTypeId(String typeId) {
		this.typeId = typeId;
	}

	public String getReceiptNum() {
		return receiptNum;
	}

	public void setReceiptNum(String receiptNum) {
		this.receiptNum = receiptNum;
	}

	public String getCcNum() {
		return ccNum;
	}

	public void setCcNum(String ccNum) {
		this.ccNum = ccNum;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getUserName() {
		return this.userName;
	}

	public long getClassId() {
		return classId;
	}

	public void setClassId(long classId) {
		this.classId = classId;
	}

	/**
	 * Constructs a <code>String</code> with all attributes
	 * in name = value format.
	 *
	 * @return a <code>String</code> representation
	 * of this object.
	 */
	public String toString()
	{
	    final String TAB = " \n ";

	    String retValue = "";

	    retValue = "TransactionsForm ( "
	        + super.toString() + TAB
	        + "from = " + this.from + TAB
	        + "to = " + this.to + TAB
	        + "transactionsList = " + this.transactionsList + TAB
	        + "typeId = " + this.typeId + TAB
	        + "statusId = " + this.statusId + TAB
	        + "receiptNum = " + this.receiptNum + TAB
	        + "ccNum = " + this.ccNum + TAB
	        + "selectionStatuses = " + this.selectionStatuses + TAB
	        + "selectionTypes = " + this.selectionTypes + TAB
	        + "transaction = " + this.transaction + TAB
	        + "searchBy = " + this.searchBy + TAB
	        + " )";

	    return retValue;
	}

	public int getSkinId() {
		return skinId;
	}


	public void setSkinId(int skinId) {
		this.skinId = skinId;
	}

	public String getSkinFilter() {
		if (skinId == Skin.SKINS_ALL_VALUE) {
			return Utils.getWriter().getSkinsToString();
		}
		else {
			return String.valueOf(skinId);
		}
	}

	/**
	 * @return the countriesSI
	 */
	public ArrayList<SelectItem> getCountriesSI() {
		return countriesSI;
	}

	/**
	 * @param countriesSI the countriesSI to set
	 */
	public void setCountriesSI(ArrayList<SelectItem> countriesSI) {
		this.countriesSI = countriesSI;
	}

	/**
	 * @return the countryId
	 */
	public long getCountryId() {
		return countryId;
	}

	/**
	 * @param countryId the countryId to set
	 */
	public void setCountryId(long countryId) {
		this.countryId = countryId;
	}

	/**
	 * @return the skinBusinessCaseId
	 */
	public long getSkinBusinessCaseId() {
		return skinBusinessCaseId;
	}

	/**
	 * @param skinBusinessCaseId the skinBusinessCaseId to set
	 */
	public void setSkinBusinessCaseId(long skinBusinessCaseId) {
		this.skinBusinessCaseId = skinBusinessCaseId;
	}

	/**
	 * @return the hasInvestments
	 */
	public long getHasInvestments() {
		return hasInvestments;
	}

	/**
	 * @param hasInvestments the hasInvestments to set
	 */
	public void setHasInvestments(long hasInvestments) {
		this.hasInvestments = hasInvestments;
	}

    /**
     * @return the endHour
     */
    public long getEndHour() {
        return endHour;
    }

    /**
     * @param endHour the endHour to set
     */
    public void setEndHour(long endHour) {
        this.endHour = endHour;
    }

    /**
     * @return the startHour
     */
    public long getStartHour() {
        return startHour;
    }

    /**
     * @param startHour the startHour to set
     */
    public void setStartHour(long startHour) {
        this.startHour = startHour;
    }

    /**
     * @return the currencyId
     */
    public long getCurrencyId() {
        return currencyId;
    }

    /**
     * @param currencyId the currencyId to set
     */
    public void setCurrencyId(long currencyId) {
        this.currencyId = currencyId;
    }

    /**
     * @return the campaignId
     */
    public long getCampaignId() {
        return campaignId;
    }

    /**
     * @param campaignId the campaignId to set
     */
    public void setCampaignId(long campaignId) {
        this.campaignId = campaignId;
    }

    /**
     * @return the writerId
     */
    public long getWriterId() {
        return writerId;
    }

    /**
     * @param writerId the writerId to set
     */
    public void setWriterId(long writerId) {
        this.writerId = writerId;
    }
    
    public boolean haveWebmoneyDeposit() {
    	FacesContext context = FacesContext.getCurrentInstance();
        User sessionuser = context.getApplication().evaluateExpressionGet(context, Constants.BIND_USER, User.class);
    	long count = TransactionsManager.getTransactionCountByType(sessionuser.getId(), TransactionsManager.TRANS_TYPE_WEBMONEY_DEPOSIT);
    	if (count > 0) {
    		return true;
    	}
    	return false;
    }

    /**
	 * @return the writerFilterList
	 */
	public ArrayList<SelectItem> getWriterFilterList() {
		return writerFilterList;
	}

	/**
	 * @param writerFilterList the writerFilterList to set
	 */
	public void setWriterFilterList(ArrayList<SelectItem> writerFilterList) {
		this.writerFilterList = writerFilterList;
	}

	public boolean isCurrencyAllSelected() {
        FacesContext context = FacesContext.getCurrentInstance();
        User sessionuser = (User) context.getApplication().createValueBinding(Constants.BIND_USER).getValue(context);
        return currencyId == ConstantsBase.CURRENCY_ALL_ID && !sessionuser.isSupportSelected() && !sessionuser.isRetentionSelected() && !sessionuser.isRetentionMSelected();
    }

	public String approveDeposit() throws SQLException{

		FacesContext context = FacesContext.getCurrentInstance();

		TransactionsManager.approveDeposit(transaction, TransactionsManagerBase.TRANS_STATUS_SUCCEED);
		updateTransactionsList();

		User user = (User)context.getApplication().createValueBinding(Constants.BIND_USER).getValue(context);
		if (user.isSupportSelected()) {
			UsersManager.loadUserStripFields(user, true, null);
		}

		context.renderResponse();
		return null;
	}

	/**
	 * Create writer filter SI from transactions list
	 */
	private void createWriterFilter(){
		HashMap<Long, String> h = new HashMap<Long, String>();
		HashMap<Long, Writer> writersMap;
		writersMap = Utils.getWriter().getAllWritersMap();
		for (Transaction t :transactionsList){
			h.put(new Long(t.getWriterId()), writersMap.get(Long.valueOf(t.getWriterId())).getUserName());
		}
		writerFilterList = new ArrayList<SelectItem>();
		writerFilterList.add(new SelectItem(new Long(-1), "All"));
   		if (h.size() > 0) {
			for (Iterator<Long> i = h.keySet().iterator(); i.hasNext(); ) {
				Long elm = i.next();
				writerFilterList.add(new SelectItem(new Long(elm), h.get(elm)));
			}
			Collections.sort(writerFilterList, new selectItemComparator());
   		}
	}

	/**
	 * @return the paymentMethodId
	 */
	public long getPaymentMethodId() {
		return paymentMethodId;
	}

	/**
	 * @param paymentMethodId the paymentMethodId to set
	 */
	public void setPaymentMethodId(long paymentMethodId) {
		this.paymentMethodId = paymentMethodId;
	}

	/**
	 * @return the hasBalance
	 */
	public int getHasBalance() {
		return hasBalance;
	}

	/**
	 * @param hasBalance the hasBalance to set
	 */
	public void setHasBalance(int hasBalance) {
		this.hasBalance = hasBalance;
	}

	/**
	 * @return the salesDepositsType
	 */
	public int getSalesDepositsType() {
		return salesDepositsType;
	}

	/**
	 * @param salesDepositsType the salesDepositsType to set
	 */
	public void setSalesDepositsType(int salesDepositsType) {
		this.salesDepositsType = salesDepositsType;
	}

	/**
	 * @return the salesDepositsSource
	 */
	public int getSalesDepositsSource() {
		return salesDepositsSource;
	}

	/**
	 * @param salesDepositsSource the salesDepositsSource to set
	 */
	public void setSalesDepositsSource(int salesDepositsSource) {
		this.salesDepositsSource = salesDepositsSource;
	}

	/**
	 * @return the showcCCnum
	 */
	public boolean isShowcCCnum() {
		return showcCCnum;
	}

	/**
	 * @param showcCCnum the showcCCnum to set
	 */
	public void setShowcCCnum(boolean showcCCnum) {
		this.showcCCnum = showcCCnum;
	}

    /**
     * @return the showBankWithdrawal
     */
    public boolean isShowBankWithdrawal() {
        return showBankWithdrawal;
    }

    /**
     * @param showBankWithdrawal the showBankWithdrawal to set
     */
    public void setShowBankWithdrawal(boolean showBankWithdrawal) {
        this.showBankWithdrawal = showBankWithdrawal;
    }

    /**
     * @return the chosenBankId
     */
    public long getChosenBankId() {
        return chosenBankId;
    }

    /**
     * @param chosenBankId the chosenBankId to set
     */
    public void setChosenBankId(long chosenBankId) {
        this.chosenBankId = chosenBankId;
    }
    
    /**
     * @return the clearingProvider
     */
    public long getClearingProvider() {
        return this.clearingProvider;
    }

    /**
     * @param clearingProvider the clearingProvider to set
     */
    public void setClearingProvider(long clearingProvider) {
        this.clearingProvider = clearingProvider;
    }
    
    /**
     * @return the ccBin
     */
    public String getCcBin() {
        return this.ccBin;
    }

    /**
     * @param ccBin the ccBin to set
     */
    public void setCcBin(String ccBin) {
        this.ccBin = ccBin;
    }


	/**
	 * @return the wasRerouted
	 */
	public long getWasRerouted() {
		return wasRerouted;
	}

	/**
	 * @param wasRerouted the wasRerouted to set
	 */
	public void setWasRerouted(long wasRerouted) {
		this.wasRerouted = wasRerouted;
	}

	/**
	 * @return the selectWasRerouted
	 */
	public ArrayList<SelectItem> getSelectWasRerouted() {
		return selectWasRerouted;
	}

	/**
	 * @param selectWasRerouted the selectWasRerouted to set
	 */
	public void setSelectWasRerouted(ArrayList<SelectItem> selectWasRerouted) {
		this.selectWasRerouted = selectWasRerouted;
	}

	public ArrayList<SelectItem> getSelectIs3d() {
		return selectIs3d;
	}

	public void setSelectIs3d(ArrayList<SelectItem> selectIs3d) {
		this.selectIs3d = selectIs3d;
	}

	public long getIsManualRouted() {
		return isManualRouted;
	}

	public void setIsManualRouted(long isManualRouted) {
		this.isManualRouted = isManualRouted;
	}

	public ArrayList<SelectItem> getSelectIsManualRoute() {
		return selectIsManualRoute;
	}

	public void setSelectIsManualRoute(ArrayList<SelectItem> selectIsManualRoute) {
		this.selectIsManualRoute = selectIsManualRoute;
	}

	public int getIsPostponed() {
		return isPostponed;
	}

	public void setIsPostponed(int isPostponed) {
		this.isPostponed = isPostponed;
	}

	public boolean isDisplayPostponed() {
		return isDisplayPostponed;
	}

	public void setDisplayPostponed(boolean isDisplayPostponed) {
		this.isDisplayPostponed = isDisplayPostponed;
	}
	
	/**
	 * @param tran
	 */
	public void insertUpdateTransactionPostponed(Transaction tran) {
		tran.getTransactionPostponed().setWriterId(Utils.getWriter().getWriter().getId());
		tran.getTransactionPostponed().setTransactionId(tran.getId());
		tran.getTransactionPostponed().setTimeUpdated(new Date());
		try {
			TransactionPostponedManager.insertOrUpdateNumOfDays(tran.getTransactionPostponed());
		} catch (SQLException e) {
			log.debug("error cant insertUpdateTransactionPostponed", e);
		}
	}
	
	/**
	 * @param tran
	 * @return
	 */
	public boolean isValidPostponedDate(Transaction tran) {
		boolean res = true;
		if (tran != null) {
			Date sysdate = new Date();
			Calendar cSys = Calendar.getInstance();
			cSys.setTime(sysdate);
			Calendar requestedDate = Calendar.getInstance();
			requestedDate.setTime(tran.getTimeCreated());
			requestedDate.add(Calendar.DATE, tran.getTransactionPostponed().getNumberOfDays() + 1);
			res = false;
			if (requestedDate.after(cSys)) {
				res = true;
			}
		}
		return res;
	}


	/**
	 * @return the providerTransactionId
	 */
	public Long getProviderTransactionId() {
		return providerTransactionId;
	}


	/**
	 * @param providerTransactionId the providerTransactionId to set
	 */
	public void setProviderTransactionId(Long providerTransactionId) {
		if (providerTransactionId == null) {
			this.providerTransactionId = 0;
		} else {
			this.providerTransactionId = providerTransactionId;
		}
	}


	/**
	 * @return the affiliatesKeys
	 */
	public List<Long> getAffiliatesKeys() {
		return affiliatesKeys;
	}


	/**
	 * @param affiliatesKeys the affiliatesKeys to set
	 */
	public void setAffiliatesKeys(List<Long> affiliatesKeys) {
		this.affiliatesKeys = affiliatesKeys;
	}


	public long getThreeD() {
		return threeD;
	}


	public void setThreeD(long threeD) {
		this.threeD = threeD;
	}
	
	
}
