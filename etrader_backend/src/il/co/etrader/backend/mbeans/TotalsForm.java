package il.co.etrader.backend.mbeans;

import java.io.Serializable;
import java.sql.SQLException;
import java.util.ArrayList;

import org.apache.log4j.Logger;

import il.co.etrader.backend.bl_managers.PopulationsManager;
import il.co.etrader.bl_vos.Population;
import il.co.etrader.util.CommonUtil;

public class TotalsForm implements Serializable {

	private static final long serialVersionUID = 1172585477608144244L;

private static final Logger logger = Logger.getLogger(TrackingForm.class);

	private ArrayList<Population> list;
	private long skinId;
	private long populationType;



	public TotalsForm() throws SQLException{
		skinId = 0 ;
		populationType = 0 ;
		updateList();
	}

	public int getListSize() {
		return list.size();
	}

	public String updateList() throws SQLException{
		list = PopulationsManager.getCountPopulations(skinId,populationType);
		CommonUtil.setTablesToFirstPage();
		return null;
	}

	public ArrayList<Population> getList() {
		return list;
	}

	public void setList(ArrayList<Population>  list) {
		this.list = list;
	}


	/**
	 * @return the skinId
	 */
	public long getSkinId() {
		return skinId;
	}

	/**
	 * @param skinId the skinId to set
	 */
	public void setSkinId(long skinId) {
		this.skinId = skinId;
	}

	/**
	 * @return the populationType
	 */
	public long getPopulationType() {
		return populationType;
	}

	/**
	 * @param populationType the populationType to set
	 */
	public void setPopulationType(long populationType) {
		this.populationType = populationType;
	}


}
