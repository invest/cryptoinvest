package il.co.etrader.backend.mbeans;

import java.io.Serializable;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;

import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;

import com.anyoption.common.beans.Investment;
import com.anyoption.common.managers.InvestmentsManagerBase;

import il.co.etrader.backend.bl_managers.InvestmentsManager;
import il.co.etrader.backend.bl_managers.TransactionsManager;
import il.co.etrader.backend.bl_vos.User;
import il.co.etrader.backend.util.Constants;
import il.co.etrader.backend.util.Utils;
import il.co.etrader.bl_vos.TranInvestment;
import il.co.etrader.util.CommonUtil;
import il.co.etrader.util.InvestmentFormatter;

public class TransInvestmentsForm implements Serializable {

	private ArrayList<TranInvestment> tranInvestmentList;

	private TranInvestment tranInvestment;
	
	private long id;
	private Date from;
	private Date to;
	private String bonusIdFreeTextFilter;
	private String transIdFreeTextFilter;
	private int transactionTypeId;
	private ArrayList<SelectItem> transactionTypes;

	public TransInvestmentsForm() throws SQLException{
		FacesContext context = FacesContext.getCurrentInstance();
		GregorianCalendar gc = new GregorianCalendar(TimeZone.getTimeZone(Utils.getWriter().getUtcOffset()));
		gc.set(Calendar.HOUR_OF_DAY, 0 + gc.getTimeZone().getRawOffset()/(1000*60*60));
		gc.set(Calendar.MINUTE, 0);
		gc.set(Calendar.SECOND, 0);
		gc.set(Calendar.MILLISECOND, 0);
		gc.add(Calendar.DAY_OF_MONTH, -1);
		from = gc.getTime();
		gc.add(Calendar.DAY_OF_MONTH, 2);
		to = gc.getTime();
		
		transactionTypes = new ArrayList<SelectItem>();
		Map<Long, String> types = new HashMap<Long, String>();
		types = TransactionsManager.getAllVisibleTransactionTypesList();
		Locale locale = Utils.getWriterLocale(context);		
		Iterator it = types.entrySet().iterator();
	    while (it.hasNext()) {
	        Map.Entry pair = (Map.Entry)it.next();
	        transactionTypes.add(new SelectItem(pair.getKey(), CommonUtil.getMessage((String) pair.getValue(), null, locale)));
	    }
	    Collections.sort(transactionTypes, new CommonUtil.selectItemComparator());
		transactionTypes.add(0, new SelectItem(String.valueOf(0),CommonUtil.getMessage("transactions.all.type", null, locale)));

		
		updateList();
	}

	public int getListSize() {
		return tranInvestmentList.size();
	}

	public String updateList() throws SQLException{
		FacesContext context=FacesContext.getCurrentInstance();
		User user = (User)context.getApplication().createValueBinding(Constants.BIND_USER).getValue(context);
		from = CommonUtil.getStartOfDay(from);
		to = CommonUtil.getEndOfDay(to);
		tranInvestmentList = InvestmentsManager.getTransactionsInvestments(user.getId(), from, to, id, 
				bonusIdFreeTextFilter, transIdFreeTextFilter, transactionTypeId);
		CommonUtil.setTablesToFirstPage();
		return null;

	}

	public boolean isRolledUp(Investment i) {
		if (i == null) {
			return false;
		}
		return InvestmentsManagerBase.isRolledUp(i);
	}

	public boolean isRollUpBought(Investment i) {
		if (i == null) {
			return false;
		}
		return InvestmentsManagerBase.isRollUpBought(i);
	}

	public boolean isGmBought(Investment i) {
		if (i == null) {
			return false;
		}
		return InvestmentsManagerBase.isGmBought(i);
	}

	public String getRefundUpTxt(Investment i) {
		if (i == null) {
			return "";
		}
		return CommonUtil.displayAmount(InvestmentsManagerBase.getRefundUp(i), i.getCurrencyId());
	}

	public String getRefundDownTxt(Investment i) {
		if (i == null) {
			return "";
		}
		return CommonUtil.displayAmount(InvestmentsManagerBase.getRefundDown(i), i.getCurrencyId());
	}

	public String getClosingLevelTxt(Investment i) {
		if (i == null) {
			return "";
		}
		return InvestmentFormatter.getClosingLevelTxt(i);
	}

	public String getCurrentLevelTxt(Investment i) {
		if (i == null) {
			return "";
		}
		return InvestmentFormatter.getCurrentLevelTxt(i);
	}

	public String getTimeEstClosingTxt(Investment i) {
		if (i == null) {
			return "";
		}
		return InvestmentFormatter.getTimeEstClosingTxt(i);
	}

	public String getAmountTxt(Investment i) {
		if (i == null) {
			return "";
		}
		return InvestmentFormatter.getAmountTxt(i);
	}

	public String getCanceledWriterName(Investment i) throws SQLException {
		if (i == null) {
			return "";
		}
		return InvestmentFormatter.getCanceledWriterName(i);
	}

	public String getIsCanceledTxt(Investment i) {
		if (i == null) {
			return "";
		}
		return InvestmentFormatter.getIsCanceledTxt(i);
	}

	public String getScheduledTxt(Investment i) {
		if (i == null) {
			return "";
		}
		return il.co.etrader.bl_managers.InvestmentsManagerBase.getScheduledTxt(i.getScheduledId());
	}

    public String getInsuranceAmountTxt(Investment i) {
    	if (i == null) {
			return "";
		}
        return CommonUtil.displayAmount(InvestmentsManagerBase.getInsuranceAmount(i), i.getCurrencyId());
    }

	public ArrayList getList() {
		return tranInvestmentList;
	}

	public void setList(ArrayList list) {
		this.tranInvestmentList = list;
	}

	public TranInvestment getTranInvestment() {
		return tranInvestment;
	}

	public void setTranInvestment(TranInvestment tranInvestment) {
		this.tranInvestment = tranInvestment;
	}

	public Date getFrom() {
		return from;
	}

	public void setFrom(Date from) {
		this.from = from;
	}

	public Date getTo() {
		return to;
	}

	public void setTo(Date to) {
		this.to = to;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		if (id == null) {
			this.id = 0;
		} else {
			this.id = id;
		}
	}

	public String getBonusIdFreeTextFilter() {
		return bonusIdFreeTextFilter;
	}

	public void setBonusIdFreeTextFilter(String bonusIdFreeTextFilter) {
		if (bonusIdFreeTextFilter == null) {
			this.bonusIdFreeTextFilter = "";
		} else {
			this.bonusIdFreeTextFilter = bonusIdFreeTextFilter;
		}
	}

	public String getTransIdFreeTextFilter() {
		return transIdFreeTextFilter;
	}

	public void setTransIdFreeTextFilter(String trasnIdFreeTextFilter) {
		if (trasnIdFreeTextFilter == null) {
			this.transIdFreeTextFilter = "";
		} else {
			this.transIdFreeTextFilter = trasnIdFreeTextFilter;
		}
	}

	/**
	 * @return the transactionTypeId
	 */
	public int getTransactionTypeId() {
		return transactionTypeId;
	}

	/**
	 * @param transactionTypeId the transactionTypeId to set
	 */
	public void setTransactionTypeId(int transactionTypeId) {
		this.transactionTypeId = transactionTypeId;
	}

	/**
	 * @return the transactionTypes
	 */
	public ArrayList<SelectItem> getTransactionTypes() {
		return transactionTypes;
	}

	/**
	 * @param transactionTypes the transactionTypes to set
	 */
	public void setTransactionTypes(ArrayList<SelectItem> transactionTypes) {
		this.transactionTypes = transactionTypes;
	}

}
