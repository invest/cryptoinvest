package il.co.etrader.backend.mbeans;

import java.io.Serializable;
import java.sql.SQLException;

import javax.el.ELContext;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import org.apache.log4j.Logger;

import il.co.etrader.backend.bl_managers.ApiExternalUsersManager;
import il.co.etrader.backend.bl_managers.UsersManager;
import il.co.etrader.backend.bl_vos.ApiExternalUser;
import il.co.etrader.backend.util.Utils;
import il.co.etrader.bl_vos.UserMarketDisable;
import il.co.etrader.util.CommonUtil;

public class ApiExternalUserChangeDetailsForm implements Serializable {

	private static final Logger log = Logger.getLogger(ApiExternalUserChangeDetailsForm.class);

	private static final long serialVersionUID = -8281038051490295232L;
	private ApiExternalUser apiExternalUser;
	private ApiExternalUser apiExternalUserSession; //the old user in session

	public ApiExternalUserChangeDetailsForm() throws SQLException {
		FacesContext fc = FacesContext.getCurrentInstance();
	    try {
	        ELContext elContext = fc.getELContext();
	        Object bean = elContext.getELResolver().getValue(elContext, null, "apiExternalUserStripForm");
	        apiExternalUser = new ApiExternalUser();
	        apiExternalUser.copyUser(((ApiExternalUserStripForm)bean).getApiExternalUser());
	        apiExternalUserSession = ((ApiExternalUserStripForm)bean).getApiExternalUser();
	    } catch (RuntimeException e) {
	    	log.fatal("cant load bean apiExternalUserStripForm" , e);
	    }
	}

	public String updateUser() {

		boolean res;
		try {
			res = ApiExternalUsersManager.updateUserDetails(apiExternalUser);
			if (res == true) {
				//update the user in session
				apiExternalUserSession.setRisky(apiExternalUser.isRisky());
				FacesContext context = FacesContext.getCurrentInstance();
				FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_INFO, CommonUtil.getMessage("user.update.success", null, Utils.getWriterLocale(context)),null);
				context.addMessage(null, fm);
				try {
					if (apiExternalUser.isUserDisable() != apiExternalUserSession.isUserDisable()) {
						if (apiExternalUser.isUserDisable()) { //insert new
							UserMarketDisable umd = UsersMarketDisableForm.initUserMarketDisable();
							umd = UsersMarketDisableForm.setUserMarketDisableEndDate(umd);
							umd.setDev3(true);
							umd.setUserId(apiExternalUser.getUserId());
							umd.setMarketId(0);
							umd.setScheduled(0);
							umd.setApiExternalUserId(apiExternalUser.getId());
							UsersManager.insertUpdateUsersMarketDisable(umd);
						} else { //update to remove all dev 3 for this user
							UsersManager.removeUsersMarketDisableByUserId(apiExternalUser.getUserId(), apiExternalUser.getId());
						}
//						update the user in session
						apiExternalUserSession.setUserDisable(apiExternalUser.isUserDisable());
					}
				} catch (Exception e1) {
					log.warn("cant update user disable", e1);
				}
			}
		} catch (SQLException e) {
			log.fatal("cant update api external user details id = " + apiExternalUser.getId() , e);
		}
		return null;
	}


	/**
	 * @return the apiExternalUser
	 */
	public ApiExternalUser getApiExternalUser() {
		return apiExternalUser;
	}

	/**
	 * @param apiExternalUser the apiExternalUser to set
	 */
	public void setApiExternalUser(ApiExternalUser apiExternalUser) {
		this.apiExternalUser = apiExternalUser;
	}

}
