package il.co.etrader.backend.mbeans;

import java.io.Serializable;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.faces.context.FacesContext;
import javax.faces.event.ValueChangeEvent;

import com.anyoption.common.beans.MailBoxTemplate;
import com.anyoption.common.beans.MailBoxUser;

import il.co.etrader.backend.bl_managers.MailBoxTemplatesManager;
import il.co.etrader.backend.bl_managers.MailBoxUsersManager;
import il.co.etrader.backend.bl_vos.User;
import il.co.etrader.backend.util.Constants;
import il.co.etrader.backend.util.Utils;


public class MailBoxEmailsForm implements Serializable {

	private static final long serialVersionUID = 1L;

	private ArrayList<MailBoxUser> list;
	public MailBoxUser email;

	public MailBoxEmailsForm() throws SQLException {
		search();
	}

	public int getListSize() {
		return list.size();
	}

	public String search() throws SQLException {
		User user = Utils.getUser();
		list = MailBoxUsersManager.getAllEmails(user.getId());
		return null;
	}

	public ArrayList<MailBoxUser> getList() {
		return list;
	}

	/**
	 * Navigate to insert new bonus
	 * @return
	 * @throws SQLException
	 */
	public String navNew() throws SQLException {
		email = new MailBoxUser();
		email.setWriterId((int)Utils.getWriter().getWriter().getId());
		return Constants.NAV_MAILBOX_EMAIL;
	}

	/**
	 * Update / insert template
	 * @return
	 * @throws SQLException
	 */
	public String insert() throws SQLException{
		email.setWriterId((int)Utils.getWriter().getWriter().getId());
		if (null != email.getFreeText()) {
			email.setFreeText(email.getFreeText().replaceAll("\r\n", "<br/>"));
		}
		User u = Utils.getUser();
		email.setUserId(u.getId());
		MailBoxUsersManager.insertEmail(email);
        search();
		return Constants.NAV_MAILBOX_EMAILS;
	}

	public MailBoxUser getEmail() {
		return email;
	}

	public void setEmail(MailBoxUser email) {
		this.email = email;
	}

	public boolean isFreeText() {
		if (null != email && null != email.getTemplate() &&
				email.getTemplate().getTypeId() == Constants.MAILBOX_TYPE_FREE_TEXT) {
			return true;
		}
		return false;
	}

	public void changeTemplate(ValueChangeEvent event) throws SQLException {
		long templateId = (Long) event.getNewValue();
		MailBoxTemplate template = MailBoxTemplatesManager.getTemplateById(templateId);
		email.setSenderId(template.getSenderId());
		email.setIsHighPriority(template.getIsHighPriority());
		email.setSubject(template.getSubject());
		email.setTemplate(template);
		email.setPopupTypeId(template.getPopupTypeId());
		if (template.getTypeId() == Constants.MAILBOX_TYPE_FREE_TEXT) {
			email.setSubject(new String());
		}
		FacesContext context = FacesContext.getCurrentInstance();
		context.renderResponse();
	}
}
