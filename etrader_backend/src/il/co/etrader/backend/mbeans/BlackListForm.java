package il.co.etrader.backend.mbeans;

import java.io.Serializable;
import java.sql.SQLException;
import java.util.ArrayList;

import il.co.etrader.backend.bl_managers.BlackListManager;
import il.co.etrader.backend.bl_vos.BlackList;
import il.co.etrader.util.CommonUtil;

public class BlackListForm implements Serializable {

	private static final long serialVersionUID = 6880295337270723256L;

	private ArrayList<BlackList> list;

	// Filters
	String userIdStr;
	String userName;
	String userFirstName;
	String userLastName;
	String phone;
	String email;
	String password;
	String ip;
	long cityId;
	long campaignId;
	String actionTypeId;

	/**
	 * @return the campaignId
	 */
	public long getCampaignId() {
		return campaignId;
	}

	/**
	 * @param campaignId the campaignId to set
	 */
	public void setCampaignId(long campaignId) {
		this.campaignId = campaignId;
	}

	public BlackListForm() throws SQLException{
		searchBlackList();
	}

	public int getListSize() {
		return list.size();
	}

	public String searchBlackList() throws SQLException{
		long issueActionTypeId = 0;
		if (!CommonUtil.isParameterEmptyOrNull(actionTypeId)){
			issueActionTypeId = Long.parseLong(actionTypeId);
		}

		list = BlackListManager.getBlackList(userIdStr, userName, userFirstName, userLastName, phone, email, password, ip, cityId, campaignId, issueActionTypeId);

		CommonUtil.setTablesToFirstPage();
		return null;
	}


	/**
	 * @return the userIdStr
	 */
	public String getUserIdStr() {
		return userIdStr;
	}

	/**
	 * @param userIdStr the userIdStr to set
	 */
	public void setUserIdStr(String userIdStr) {
		this.userIdStr = userIdStr;
	}

	/**
	 * @return the userName
	 */
	public String getUserName() {
		return userName;
	}

	/**
	 * @param userName the userName to set
	 */
	public void setUserName(String userName) {
		this.userName = userName;
	}

	/**
	 * @return the list
	 */
	public ArrayList<BlackList> getList() {
		return list;
	}

	/**
	 * @return the cityId
	 */
	public long getCityId() {
		return cityId;
	}

	/**
	 * @param cityId the cityId to set
	 */
	public void setCityId(long cityId) {
		this.cityId = cityId;
	}

	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * @param email the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * @return the ip
	 */
	public String getIp() {
		return ip;
	}

	/**
	 * @param ip the ip to set
	 */
	public void setIp(String ip) {
		this.ip = ip;
	}

	/**
	 * @return the phone
	 */
	public String getPhone() {
		return phone;
	}

	/**
	 * @param phone the phone to set
	 */
	public void setPhone(String phone) {
		this.phone = phone;
	}

	/**
	 * @return the userFirstName
	 */
	public String getUserFirstName() {
		return userFirstName;
	}

	/**
	 * @param userFirstName the userFirstName to set
	 */
	public void setUserFirstName(String userFirstName) {
		this.userFirstName = userFirstName;
	}

	/**
	 * @return the userLastName
	 */
	public String getUserLastName() {
		return userLastName;
	}

	/**
	 * @param userLastName the userLastName to set
	 */
	public void setUserLastName(String userLastName) {
		this.userLastName = userLastName;
	}

	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * @param password the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * @return the actionTypeId
	 */
	public String getActionTypeId() {
		return actionTypeId;
	}

	/**
	 * @param actionTypeId the actionTypeId to set
	 */
	public void setActionTypeId(String actionTypeId) {
		this.actionTypeId = actionTypeId;
	}
}
