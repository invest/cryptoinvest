package il.co.etrader.backend.mbeans;


import java.io.IOException;
import java.io.PrintWriter;
import java.io.Serializable;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.ValueChangeEvent;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.base.Skin;

import il.co.etrader.backend.bl_managers.TransactionsIssuesManager;
import il.co.etrader.backend.bl_vos.SalesDepositsDetails;
import il.co.etrader.backend.bl_vos.SalesDepositsTotals;
import il.co.etrader.backend.bl_vos.User;
import il.co.etrader.backend.bl_vos.WriterWrapper;
import il.co.etrader.backend.util.Constants;
import il.co.etrader.backend.util.Utils;
import il.co.etrader.bl_vos.Writer;
import il.co.etrader.util.ConstantsBase;

public class SalesDepositsForm implements Serializable {

	private static final long serialVersionUID = 1172585477608144244L;

	private static final Logger logger = Logger.getLogger(SalesDepositsForm.class);

	// Totals filters
	private Date from;
	private Date to;
	private long writerId;
	private long currencyId;
	private long skinId;
	private Long transactionId;
	private Long userId;

	// Details filters
	// These four are duplicated since we don't wanna change tham when moving between screens
	private Date fromDetails;
	private Date toDetails;
	private long writerIdDetails;
	private long currencyIdDetails;
	private long skinIdDetails;

	private String dateFilterType;
	private int qualifiedFilter;
	private int salesDepositsType;

	// Totals fields
	private ArrayList<SalesDepositsTotals> totalsList;
	private ArrayList<SalesDepositsTotals> totalsSumList;
	private int totalsListSize;
	SalesDepositsTotals selectedTotalsRow;

	// Details fields
	private ArrayList<SalesDepositsDetails> detailsList;
	private ArrayList<SalesDepositsDetails> detailsSumList;
	private int detailsListSize;

	public SalesDepositsForm() throws SQLException, ParseException{
		initSalesDepositsTotals();
	}


	public void initSalesDepositsTotals() throws SQLException, ParseException{
		GregorianCalendar gc = new GregorianCalendar();
		gc.set(GregorianCalendar.HOUR_OF_DAY, 0);
		gc.set(GregorianCalendar.MINUTE, 0);
		gc.set(GregorianCalendar.SECOND, 0);
		to = gc.getTime();
		gc.set(GregorianCalendar.DATE, 1);
		from = gc.getTime();
		skinId = ConstantsBase.ALL_FILTER_ID;
		transactionId = null;
		userId = null;

		FacesContext context = FacesContext.getCurrentInstance();
		User user = (User)context.getApplication().createValueBinding(ConstantsBase.BIND_USER).getValue(context);

		if (user.isRetentionSelected()){
			WriterWrapper wr = Utils.getWriter();
			Writer w = wr.getWriter();
			writerId = w.getId();

			// Need to change currency by writer skin
			ArrayList<Integer> skinsArr = wr.getSkins();
			if (null != skinsArr && skinsArr.size() == 1 && skinsArr.contains(new Integer(Skin.SKIN_ETRADER_INT)) ){
				currencyId = ConstantsBase.CURRENCY_ILS_ID;
			}else{
				currencyId = 0 ;
			}

		}else {
			writerId = ConstantsBase.ALL_FILTER_ID;
			currencyId = ConstantsBase.ALL_FILTER_ID;
		}

		search();
	}

	public void initSalesDepositsDetails() throws SQLException{

		GregorianCalendar gc = new GregorianCalendar();
		toDetails = gc.getTime();
		gc.set(GregorianCalendar.DATE, 1);
		fromDetails = gc.getTime();
		skinIdDetails = ConstantsBase.ALL_FILTER_ID;
		transactionId = null;
		userId = null;

		FacesContext context = FacesContext.getCurrentInstance();
		User user = (User)context.getApplication().createValueBinding(ConstantsBase.BIND_USER).getValue(context);

		if (user.isRetentionSelected()){
			WriterWrapper wr = Utils.getWriter();
			Writer w = wr.getWriter();

			writerIdDetails = w.getId();

			// Need to change currency by writer skin
			ArrayList<Integer> skinsArr = wr.getSkins();
			if (null != skinsArr && skinsArr.size() == 1 && skinsArr.contains(new Integer(Skin.SKIN_ETRADER_INT)) ){
				currencyIdDetails = ConstantsBase.CURRENCY_ILS_ID;
			}else{
				currencyIdDetails = 0 ;
			}

		}else {
			writerIdDetails = ConstantsBase.ALL_FILTER_ID;
			currencyIdDetails = ConstantsBase.ALL_FILTER_ID;
		}

		dateFilterType = Constants.SALES_TIME_CREATED_FILTER;
		qualifiedFilter = ConstantsBase.ALL_FILTER_ID_INT;
		salesDepositsType = ConstantsBase.SALES_DEPOSIT_TYPE_CONVERSION;

		searchDetails();
	}

	public void initSalesDepositsForm() throws SQLException{
		GregorianCalendar gc = new GregorianCalendar();
		to = gc.getTime();
		toDetails = gc.getTime();
		gc.set(GregorianCalendar.DATE, 1);
		from = gc.getTime();
		fromDetails = gc.getTime();
		skinId = ConstantsBase.ALL_FILTER_ID;
		skinIdDetails = ConstantsBase.ALL_FILTER_ID;
		transactionId = null;
		userId = null;

		FacesContext context = FacesContext.getCurrentInstance();
		User user = (User)context.getApplication().createValueBinding(ConstantsBase.BIND_USER).getValue(context);

		if (user.isRetentionSelected()){
			WriterWrapper wr = Utils.getWriter();
			Writer w = wr.getWriter();

			writerId = w.getId();
			writerIdDetails = w.getId();

			// Need to change currency by writer skin
			ArrayList<Integer> skinsArr = wr.getSkins();
			if (null != skinsArr && skinsArr.size() == 1 && skinsArr.contains(new Integer(Skin.SKIN_ETRADER_INT)) ){
				currencyId = ConstantsBase.CURRENCY_ILS_ID;
				currencyIdDetails = ConstantsBase.CURRENCY_ILS_ID;
			}else{
				currencyId = 0 ;
				currencyIdDetails = 0 ;
			}

		}else {
			writerId = ConstantsBase.ALL_FILTER_ID;
			writerIdDetails = ConstantsBase.ALL_FILTER_ID;
			currencyId = ConstantsBase.ALL_FILTER_ID;
			currencyIdDetails = ConstantsBase.ALL_FILTER_ID;
		}

	}

	public int getTotalsListSize() {
		return totalsListSize;
	}

	public String search() throws SQLException, ParseException{
		totalsList = TransactionsIssuesManager.getSalesDeposits(from, to, writerId,currencyId,skinId);

		totalsListSize = totalsList.size();
		SalesDepositsTotals sdcSum;
		if (totalsListSize > 0){
			sdcSum = totalsList.remove(totalsListSize - 1);
			--totalsListSize;
		}else{
			sdcSum = new SalesDepositsTotals();
		}

		totalsSumList = new ArrayList<SalesDepositsTotals>();
		totalsSumList.add(sdcSum);

		return null;
	}

	public String searchDetails() throws SQLException{

		long userIdVal = 0;
		if (null != userId){
			userIdVal = userId.longValue();
		}

		long trxIdVal = 0;
		if (null != transactionId){
			trxIdVal = transactionId.longValue();
		}

		detailsList = TransactionsIssuesManager.getSalesDepositsDetails(userIdVal, trxIdVal ,fromDetails, toDetails, writerIdDetails, currencyIdDetails,
				skinIdDetails, dateFilterType, qualifiedFilter, salesDepositsType);

		detailsListSize = detailsList.size();
		SalesDepositsDetails sddSum;
		if (detailsListSize > 0){
			sddSum = detailsList.remove(detailsListSize - 1);
			--detailsListSize;
		}else{
			sddSum = new SalesDepositsDetails();
		}

		detailsSumList = new ArrayList<SalesDepositsDetails>();
		detailsSumList.add(sddSum);

		return null;
	}

	public String searchDetailsFromScreen() throws SQLException{
		searchDetails();

		return null;
	}


	public void updateDateFilterType(ValueChangeEvent ev) {
		FacesContext context = FacesContext.getCurrentInstance();
		if (!ev.getNewValue().equals(ConstantsBase.EMPTY_STRING)){
			dateFilterType = String.valueOf(ev.getNewValue());
		}
		context.renderResponse();
	}

	public boolean isTimeCreatedFilter(){
		if (dateFilterType.equals(Constants.SALES_TIME_CREATED_FILTER)){
			return true;
		}
		return false;
	}

	public String exportHtmlTableToExcel() throws IOException {
		FacesContext context=FacesContext.getCurrentInstance();

		if (detailsList.size() > 0) {
	        //Set the filename
	        Date dt = new Date();
	        SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd_HHmmss");
	        String filename = "Sales_Deposit_Details_" + fmt.format(dt) + ".xls";
	        String htmlBuffer=
	        	"<html><head><meta http-equiv=\"Content-Type\" content=\"text/html; CHARSET=UTF-8\"></head>"+
	        	"<body>" +
	        	"<br><b>Sales Deposits Details:</b>" +
	        	"<br><table border=\"1\"><tr>" +
		        		"<td><b>Representative</b></td>" +
		        		"<td><b>User Skin</b></td>" +
		        		"<td><b>User Id</b></td>" +
	        			"<td><b>Transaction Id</b></td>" +
		        		"<td><b>Amount</b></td>" +
		        		"<td><b>Transaction time</b></td>" +
		        		"<td><b>Call time</b></td>" +
		        		"<td><b>Qualification time</b></td>" +
		        		"<td><b>Turnover left</b></td>" +
		        		"<td><b>Currency</b></td>" +
		        		"<td><b>Population</b></td>" +
		        		"<td><b>Type</b></td>";

	        for (SalesDepositsDetails row:detailsList){
	        	htmlBuffer += "<tr><td><b>" + row.getWriterName() + "</b></td>" +
	        						"<td><b>" + row.getSkin() + "</b></td>" +
	        						"<td><b>" + row.getUserId() + "</b></td>" +
	        						"<td><b>" + row.getTransactionId() + "</b></td>" +
	        						"<td><b>" + row.getDepositAmount() + "</b></td>" +
	        						"<td><b>" + row.getDepositTimeTxt() + "</b></td>" +
	        						"<td><b>" + row.getActionTimeTxt() + "</b></td>" +
	        						"<td><b>" + row.getQualificationTimeTxt() + "</b></td>" +
	        						"<td><b>" + row.getTurnoverLeft() + "</b></td>" +
	        						"<td><b>" + row.getCurrency() + "</b></td>" +
	        						"<td><b>" + row.getPopulationName() + "</b></td>" +
	        						"<td><b>" + row.getSalesDepositTypeTxt() + "</b></td>";
	        }

	        htmlBuffer+="</table></body></html>";

	        //Setup the output
	        String contentType = "application/vnd.ms-excel";
	        HttpServletResponse response = (HttpServletResponse)context.getExternalContext().getResponse();
	        response.setHeader("Content-disposition", "attachment; filename=" + filename);
	        response.setContentType(contentType);
	        response.setCharacterEncoding("UTF-8");

	        //Write the table back out
	        PrintWriter out = response.getWriter();
	        out.print(htmlBuffer);
	        out.close();
	        context.responseComplete();
		}
		else
		{
			FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_INFO,
				"No Data were found",null);
			context.addMessage(null, fm);

		}

		return null;
	}

	public ArrayList<SalesDepositsTotals> getTotalsList() {
		return totalsList;
	}

	public void setTotalsList(ArrayList<SalesDepositsTotals>  list) {
		this.totalsList = list;
	}

	/**
	 * @return the from
	 */
	public Date getFrom() {
		return from;
	}

	/**
	 * @param from the from to set
	 */
	public void setFrom(Date from) {
		this.from = from;
	}

	/**
	 * @return the to
	 */
	public Date getTo() {
		return to;
	}

	/**
	 * @param to the to to set
	 */
	public void setTo(Date to) {
		this.to = to;
	}

	/**
	 * @return the writerId
	 */
	public long getWriterId() {
		return writerId;
	}

	/**
	 * @param writerId the writerId to set
	 */
	public void setWriterId(long writerId) {
		this.writerId = writerId;
	}

	/**
	 * @return the sumList
	 */
	public ArrayList<SalesDepositsTotals> getTotalsSumList() {
		return totalsSumList;
	}

	/**
	 * @return the currencyId
	 */
	public long getCurrencyId() {
		return currencyId;
	}

	/**
	 * @param currencyId the currencyId to set
	 */
	public void setCurrencyId(long currencyId) {
		this.currencyId = currencyId;
	}

	/**
	 * @return the detailsList
	 */
	public ArrayList<SalesDepositsDetails> getDetailsList() {
		return detailsList;
	}

	/**
	 * @param detailsList the detailsList to set
	 */
	public void setDetailsList(ArrayList<SalesDepositsDetails> detailsList) {
		this.detailsList = detailsList;
	}

	/**
	 * @return the detailsSumList
	 */
	public ArrayList<SalesDepositsDetails> getDetailsSumList() {
		return detailsSumList;
	}

	/**
	 * @param detailsSumList the detailsSumList to set
	 */
	public void setDetailsSumList(ArrayList<SalesDepositsDetails> detailsSumList) {
		this.detailsSumList = detailsSumList;
	}

	/**
	 * @return the detailsListSize
	 */
	public int getDetailsListSize() {
		return detailsListSize;
	}

	/**
	 * @param sumList the sumList to set
	 */
	public void setTotalsSumList(ArrayList<SalesDepositsTotals> sumList) {
		this.totalsSumList = sumList;
	}

	/**
	 * @return the salesDepositType
	 */
	public int getSalesDepositsType() {
		return salesDepositsType;
	}

	/**
	 * @param salesDepositType the salesDepositType to set
	 */
	public void setSalesDepositsType(int salesDepositType) {
		this.salesDepositsType = salesDepositType;
	}

	/**
	 * @return the qualifiedFilter
	 */
	public int getQualifiedFilter() {
		return qualifiedFilter;
	}

	/**
	 * @param qualifiedFilter the qualifiedFilter to set
	 */
	public void setQualifiedFilter(int qualifiedFilter) {
		this.qualifiedFilter = qualifiedFilter;
	}

	/**
	 * @return the skinId
	 */
	public long getSkinId() {
		return skinId;
	}

	/**
	 * @param skinId the skinId to set
	 */
	public void setSkinId(long skinId) {
		this.skinId = skinId;
	}

	/**
	 * @return the selectedTotalsRow
	 */
	public SalesDepositsTotals getSelectedTotalsRow() {
		return selectedTotalsRow;
	}

	/**
	 * @param selectedTotalsRow the selectedTotalsRow to set
	 */
	public void setSelectedTotalsRow(SalesDepositsTotals selectedTotalsRow) {
		this.selectedTotalsRow = selectedTotalsRow;
	}

	/**
	 * @return the fromDetails
	 */
	public Date getFromDetails() {
		return fromDetails;
	}

	/**
	 * @param fromDetails the fromDetails to set
	 */
	public void setFromDetails(Date fromDetails) {
		this.fromDetails = fromDetails;
	}

	/**
	 * @return the toDetails
	 */
	public Date getToDetails() {
		return toDetails;
	}

	/**
	 * @param toDetails the toDetails to set
	 */
	public void setToDetails(Date toDetails) {
		this.toDetails = toDetails;
	}

	/**
	 * @return the writerIdDetails
	 */
	public long getWriterIdDetails() {
		return writerIdDetails;
	}

	/**
	 * @param writerIdDetails the writerIdDetails to set
	 */
	public void setWriterIdDetails(long writerIdDetails) {
		this.writerIdDetails = writerIdDetails;
	}

	/**
	 * @param totalsListSize the totalsListSize to set
	 */
	public void setTotalsListSize(int totalsListSize) {
		this.totalsListSize = totalsListSize;
	}

	/**
	 * @return the currencyIdDetails
	 */
	public long getCurrencyIdDetails() {
		return currencyIdDetails;
	}

	/**
	 * @param currencyIdDetails the currencyIdDetails to set
	 */
	public void setCurrencyIdDetails(long currencyIdDetails) {
		this.currencyIdDetails = currencyIdDetails;
	}


	private String searchSelectedDetails() throws SQLException{
		String rowDateString = selectedTotalsRow.getDate();
		skinIdDetails = skinId;
		writerIdDetails = writerId;

		if (rowDateString.equals(Constants.EMPTY_STRING)){
			toDetails = to;
			fromDetails = from;
		}else{
			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
			Date rowDate;

			try {
				rowDate = sdf.parse(rowDateString);

				toDetails = rowDate;
				fromDetails = rowDate;
			} catch (ParseException e) {
				logger.error("Problem with parsing selected date");
				toDetails = to;
				fromDetails = from;
			}
		}

		searchDetails();
		return Constants.NAV_SALES_DEPOSITS_DETAILS;

	}

	public String conTranMadeCountLink() throws SQLException{
		dateFilterType = Constants.SALES_TIME_CREATED_FILTER;
		salesDepositsType = ConstantsBase.SALES_DEPOSIT_TYPE_CONVERSION;

		return searchSelectedDetails();
	}

	public String conTranMadeQualCountLink() throws SQLException{
		dateFilterType = Constants.SALES_TIME_CREATED_FILTER;
		salesDepositsType = ConstantsBase.SALES_DEPOSIT_TYPE_CONVERSION;
		qualifiedFilter = Constants.SALES_QUAL_FILTER_QUALIFIED;

		return searchSelectedDetails();
	}

	public String conTranQualCountLink() throws SQLException{
		dateFilterType = Constants.SALES_TIME_SETTLED_FILTER;
		salesDepositsType = ConstantsBase.SALES_DEPOSIT_TYPE_CONVERSION;

		return searchSelectedDetails();
	}

	public String retTranMadeCountLink() throws SQLException{
		dateFilterType = Constants.SALES_TIME_CREATED_FILTER;
		salesDepositsType = ConstantsBase.SALES_DEPOSIT_TYPE_RETENTION;

		return searchSelectedDetails();
	}

	public String retTranMadeQualCountLink() throws SQLException{
		dateFilterType = Constants.SALES_TIME_CREATED_FILTER;
		salesDepositsType = ConstantsBase.SALES_DEPOSIT_TYPE_RETENTION;
		qualifiedFilter = Constants.SALES_QUAL_FILTER_QUALIFIED;

		return searchSelectedDetails();
	}

	public String retTranQualCountLink() throws SQLException{
		dateFilterType = Constants.SALES_TIME_SETTLED_FILTER;
		salesDepositsType = ConstantsBase.SALES_DEPOSIT_TYPE_RETENTION;

		return searchSelectedDetails();
	}


	/**
	 * @return the dateFilterType
	 */
	public String getDateFilterType() {
		return dateFilterType;
	}


	/**
	 * @param dateFilterType the dateFilterType to set
	 */
	public void setDateFilterType(String dateFilterType) {
		this.dateFilterType = dateFilterType;
	}


	public long getSkinIdDetails() {
		return skinIdDetails;
	}


	public void setSkinIdDetails(long skinIdDetails) {
		this.skinIdDetails = skinIdDetails;
	}


	/**
	 * @return the transactionId
	 */
	public Long getTransactionId() {
		return transactionId;
	}


	/**
	 * @param transactionId the transactionId to set
	 */
	public void setTransactionId(Long transactionId) {
		this.transactionId = transactionId;
	}


	/**
	 * @return the userId
	 */
	public Long getUserId() {
		return userId;
	}


	/**
	 * @param userId the userId to set
	 */
	public void setUserId(Long userId) {
		this.userId = userId;
	}




}
