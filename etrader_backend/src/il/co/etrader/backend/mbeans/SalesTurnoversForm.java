package il.co.etrader.backend.mbeans;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.Serializable;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.base.Skin;

import il.co.etrader.backend.bl_managers.InvestmentsManager;
import il.co.etrader.backend.bl_managers.TransactionsIssuesManager;
import il.co.etrader.backend.bl_vos.SalesTurnoversDetails;
import il.co.etrader.backend.bl_vos.SalesTurnoversTotals;
import il.co.etrader.backend.bl_vos.User;
import il.co.etrader.backend.bl_vos.WriterWrapper;
import il.co.etrader.backend.util.Constants;
import il.co.etrader.backend.util.Utils;
import il.co.etrader.bl_managers.TransactionsManagerBase;
import il.co.etrader.bl_vos.Writer;
import il.co.etrader.util.ConstantsBase;

public class SalesTurnoversForm implements Serializable {
	
	private static final long serialVersionUID = 1843955274369187255L;
	private static final Logger logger = Logger.getLogger(SalesTurnoversForm.class);
	
	private Date from;
	private Date to;
	private long writerId;
	private long currencyId;
	private long skinId;
	private long salesTypeDepId;
	
	// Details filters
	// These four are duplicated since we don't wanna change tham when moving between screens
	private Date fromDetails;
	private Date toDetails;
	private long writerIdDetails;
	private long currencyIdDetails;
	private long skinIdDetails;
	private long salesTypeDepIdDetails;
	private long tranClassTypeIdDetails;
	
	// Totals fields
	private ArrayList<SalesTurnoversTotals> totalsList;
	private ArrayList<SalesTurnoversTotals> totalsSumList;
	private int totalsListSize;
	SalesTurnoversTotals selectedTotalsRow;
	
	// Details fields
	private ArrayList<SalesTurnoversDetails> detailsList;
	private ArrayList<SalesTurnoversDetails> detailsSumList;
	private int detailsListSize;
	
	private ArrayList<SalesTurnoversDetails> detailsListFromTotals;
	private ArrayList<SalesTurnoversDetails> detailsSumListFromTotals;
	private int detailsFromTotalsSize;
	
	public SalesTurnoversForm () throws Exception {
		GregorianCalendar gc = new GregorianCalendar();
		gc.set(GregorianCalendar.HOUR_OF_DAY, 0);
		gc.set(GregorianCalendar.MINUTE, 0);
		gc.set(GregorianCalendar.SECOND, 0);
		to = gc.getTime();
		gc.set(GregorianCalendar.DATE, 1);
		from = gc.getTime();
		skinId = ConstantsBase.ALL_FILTER_ID;
		
		FacesContext context = FacesContext.getCurrentInstance();
		User user = (User)context.getApplication().createValueBinding(ConstantsBase.BIND_USER).getValue(context);
		
		if (user.isRetentionSelected()){
			WriterWrapper wr = Utils.getWriter();
			Writer w = wr.getWriter();
			writerId = w.getId();

			// Need to change currency by writer skin
			ArrayList<Integer> skinsArr = wr.getSkins();
			if (null != skinsArr && skinsArr.size() == 1 && skinsArr.contains(new Integer(Skin.SKIN_ETRADER_INT)) ){
				currencyId = ConstantsBase.CURRENCY_ILS_ID;
			}else{
				currencyId = 0 ;
			}

		}else {
			writerId = ConstantsBase.ALL_FILTER_ID;
			currencyId = ConstantsBase.ALL_FILTER_ID;
			skinId = ConstantsBase.ALL_FILTER_ID;
		}
		// retention team
		salesTypeDepId = ConstantsBase.ALL_FILTER_ID;
		if (user.isRetentionMSelected()) {
			salesTypeDepId = ConstantsBase.SALES_TYPE_DEPARTMENT_UPGRADE_RETENTION;
		}
		search();
	}
	
	public String search() throws Exception {
		totalsList = InvestmentsManager.getSalesTurnovers(from, to, writerId, skinId, currencyId, salesTypeDepId);
		
		totalsListSize = totalsList.size();
		SalesTurnoversTotals sdcSum;
		if (totalsListSize > 0){
			sdcSum = totalsList.remove(totalsListSize - 1);
			--totalsListSize;
		}else{
			sdcSum = new SalesTurnoversTotals();
		}

		totalsSumList = new ArrayList<SalesTurnoversTotals>();
		totalsSumList.add(sdcSum);

		return null;
	}

	public void initSalesTurnoversDetails() throws SQLException {
		GregorianCalendar gc = new GregorianCalendar();
		toDetails = gc.getTime();
		gc.set(GregorianCalendar.DATE, 1);
		fromDetails = gc.getTime();
		skinIdDetails = ConstantsBase.ALL_FILTER_ID;

		FacesContext context = FacesContext.getCurrentInstance();
		User user = (User)context.getApplication().createValueBinding(ConstantsBase.BIND_USER).getValue(context);

		if (user.isRetentionSelected()){
			WriterWrapper wr = Utils.getWriter();
			Writer w = wr.getWriter();

			writerIdDetails = w.getId();

			// Need to change currency by writer skin
			ArrayList<Integer> skinsArr = wr.getSkins();
			if (null != skinsArr && skinsArr.size() == 1 && skinsArr.contains(new Integer(Skin.SKIN_ETRADER_INT)) ){
				currencyIdDetails = ConstantsBase.CURRENCY_ILS_ID;
			}else{
				currencyIdDetails = 0 ;
			}

		}else {
			writerIdDetails = ConstantsBase.ALL_FILTER_ID;
			currencyIdDetails = ConstantsBase.ALL_FILTER_ID;
		}
		// retention team
		salesTypeDepIdDetails = ConstantsBase.ALL_FILTER_ID;
		if (user.isRetentionMSelected()) {
			salesTypeDepIdDetails = ConstantsBase.SALES_TYPE_DEPARTMENT_UPGRADE_RETENTION;
		}
		searchDetails();
	}
	
	public String searchDetails() throws SQLException{


		detailsList = InvestmentsManager.getSalesTurnoverDetails(fromDetails, toDetails, currencyIdDetails, writerIdDetails, skinIdDetails, salesTypeDepIdDetails);

		detailsListSize = detailsList.size();
		SalesTurnoversDetails sddSum;
		if (detailsListSize > 0){
			sddSum = detailsList.remove(detailsListSize - 1);
			--detailsListSize;
		}else{
			sddSum = new SalesTurnoversDetails();
		}

		detailsSumList = new ArrayList<SalesTurnoversDetails>();
		detailsSumList.add(sddSum);

		return null;
	}	
	
	public String searchDetailsFromTotals() throws SQLException{
		
		detailsListFromTotals = TransactionsIssuesManager.getSaledDepositsDetailsOfRetention(fromDetails, toDetails, writerIdDetails, currencyIdDetails, skinIdDetails, salesTypeDepIdDetails, tranClassTypeIdDetails);
		
		detailsFromTotalsSize = detailsListFromTotals.size();
		SalesTurnoversDetails sddSum;
		if (detailsFromTotalsSize > 0){
			sddSum = detailsListFromTotals.remove(detailsFromTotalsSize - 1);
			--detailsFromTotalsSize;
		}else{
			sddSum = new SalesTurnoversDetails();
		}

		detailsSumListFromTotals = new ArrayList<SalesTurnoversDetails>();
		detailsSumListFromTotals.add(sddSum);

		return null;
	}
	
	public String searchDetailsFromScreen() throws SQLException{
		searchDetails();
		return null;
	}

	public String exportHtmlTableToExcel() throws IOException {
		FacesContext context=FacesContext.getCurrentInstance();

		if (detailsList.size() > 0) {
	        //Set the filename
	        Date dt = new Date();
	        SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd_HHmmss");
	        String filename = "Sales_Turnover_Details_" + fmt.format(dt) + ".xls";
	        String htmlBuffer=
	        	"<html><head><meta http-equiv=\"Content-Type\" content=\"text/html; CHARSET=UTF-8\"></head>"+
	        	"<body>" +
	        	"<br><b>Sales Deposits Details:</b>" +
	        	"<br><table border=\"1\"><tr>" +
		        		"<td><b>Representative</b></td>" +
		        		"<td><b>User Skin</b></td>" +
		        		"<td><b>User Id</b></td>" +
	        			"<td><b>Sum of deposits</b></td>" +
		        		"<td><b>Sum of investments</b></td>" +
		        		"<td><b>Campaign</b></td>" +
		        		"<td><b>Currency</b></td>" +
		        		"<td><b>Population</b></td>" +
		        		"<td><b>User rank</b></td>" +
		        		"<td><Status</b></td>";

	        for (SalesTurnoversDetails row:detailsList){
	        	htmlBuffer += "<tr><td><b>" + row.getWriterName() + "</b></td>" +
	        						"<td><b>" + row.getSkinId() + "</b></td>" +
	        						"<td><b>" + row.getUserId() + "</b></td>" +
	        						"<td><b>" + row.getSumOfDepositsTxt() + "</b></td>" +
	        						"<td><b>" + row.getSumOfTurnoversTxt()+ "</b></td>" +
	        						"<td><b>" + row.getCampaignId() + "</b></td>" +
	        						"<td><b>" + row.getCurrencyTxt() + "</b></td>" +
	        						"<td><b>" + row.getPopulationName() + "</b></td>" +
	        						"<td><b>" + row.getUserRankName() + "</b></td>" +
	        						"<td><b>" + row.getUserStatusName() + "</b></td>";
	        }

	        htmlBuffer+="</table></body></html>";

	        //Setup the output
	        String contentType = "application/vnd.ms-excel";
	        HttpServletResponse response = (HttpServletResponse)context.getExternalContext().getResponse();
	        response.setHeader("Content-disposition", "attachment; filename=" + filename);
	        response.setContentType(contentType);
	        response.setCharacterEncoding("UTF-8");

	        //Write the table back out
	        PrintWriter out = response.getWriter();
	        out.print(htmlBuffer);
	        out.close();
	        context.responseComplete();
		}
		else
		{
			FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_INFO,
				"No Data were found",null);
			context.addMessage(null, fm);

		}

		return null;
	}
	
	private String searchSelectedDetails() throws SQLException{
		String rowDateString = selectedTotalsRow.getDate();
		skinIdDetails = skinId;
		writerIdDetails = writerId;
		salesTypeDepIdDetails = salesTypeDepId;

		if (rowDateString.equals(Constants.EMPTY_STRING)){
			toDetails = to;
			fromDetails = from;
		}else{
			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
			Date rowDate;

			try {
				rowDate = sdf.parse(rowDateString);

				toDetails = rowDate;
				fromDetails = rowDate;
			} catch (ParseException e) {
				logger.error("Problem with parsing selected date");
				toDetails = to;
				fromDetails = from;
			}
		}

		searchDetailsFromTotals();
		return Constants.NAV_SALES_TURNOVERS_DETAILS_BY_DATE;

	}

	public String retTranMadeCountLink() throws SQLException{	
		tranClassTypeIdDetails = TransactionsManagerBase.TRANSACTION_CLASS_TYPE_REAL_DEPOSIT;
		return searchSelectedDetails();
	}
	
	/**
	 * link to search selected withdrawals details.
	 * @return String
	 * @throws SQLException
	 */
	public String retTranWithdrawMadeSumLink() throws SQLException{		
		tranClassTypeIdDetails = TransactionsManagerBase.TRANSACTION_CLASS_TYPE_REAL_WITHDRAWALS;
		return searchSelectedDetails();
	}

	/**
	 * Get sales type department by menu selected.
	 * @return String
	 */
	public String getSalesTypeDepByMenuSelected() {
		FacesContext context = FacesContext.getCurrentInstance();
		User user = (User)context.getApplication().createValueBinding(ConstantsBase.BIND_USER).getValue(context);
		if (user.isRetentionSelected()) {
			salesTypeDepId = ConstantsBase.ALL_FILTER_ID;
		}
		return "";
	}
	
	
	/**
	 * @return the from
	 */
	public Date getFrom() {
		return from;
	}

	/**
	 * @param from the from to set
	 */
	public void setFrom(Date from) {
		this.from = from;
	}

	/**
	 * @return the to
	 */
	public Date getTo() {
		return to;
	}

	/**
	 * @param to the to to set
	 */
	public void setTo(Date to) {
		this.to = to;
	}

	/**
	 * @return the writerId
	 */
	public long getWriterId() {
		return writerId;
	}

	/**
	 * @param writerId the writerId to set
	 */
	public void setWriterId(long writerId) {
		this.writerId = writerId;
	}

	/**
	 * @return the currencyId
	 */
	public long getCurrencyId() {
		return currencyId;
	}

	/**
	 * @param currencyId the currencyId to set
	 */
	public void setCurrencyId(long currencyId) {
		this.currencyId = currencyId;
	}

	/**
	 * @return the skinId
	 */
	public long getSkinId() {
		return skinId;
	}

	/**
	 * @param skinId the skinId to set
	 */
	public void setSkinId(long skinId) {
		this.skinId = skinId;
	}

	/**
	 * @return the totalsList
	 */
	public ArrayList<SalesTurnoversTotals> getTotalsList() {
		return totalsList;
	}

	/**
	 * @param totalsList the totalsList to set
	 */
	public void setTotalsList(ArrayList<SalesTurnoversTotals> totalsList) {
		this.totalsList = totalsList;
	}

	/**
	 * @return the totalsSumList
	 */
	public ArrayList<SalesTurnoversTotals> getTotalsSumList() {
		return totalsSumList;
	}

	/**
	 * @param totalsSumList the totalsSumList to set
	 */
	public void setTotalsSumList(ArrayList<SalesTurnoversTotals> totalsSumList) {
		this.totalsSumList = totalsSumList;
	}

	/**
	 * @return the totalsListSize
	 */
	public int getTotalsListSize() {
		return totalsListSize;
	}

	/**
	 * @param totalsListSize the totalsListSize to set
	 */
	public void setTotalsListSize(int totalsListSize) {
		this.totalsListSize = totalsListSize;
	}

	/**
	 * @return the fromDetails
	 */
	public Date getFromDetails() {
		return fromDetails;
	}

	/**
	 * @param fromDetails the fromDetails to set
	 */
	public void setFromDetails(Date fromDetails) {
		this.fromDetails = fromDetails;
	}

	/**
	 * @return the toDetails
	 */
	public Date getToDetails() {
		return toDetails;
	}

	public ArrayList<SalesTurnoversDetails> getDetailsSumListFromTotals() {
		return detailsSumListFromTotals;
	}

	public void setDetailsSumListFromTotals(
			ArrayList<SalesTurnoversDetails> detailsSumListFromTotals) {
		this.detailsSumListFromTotals = detailsSumListFromTotals;
	}

	/**
	 * @param toDetails the toDetails to set
	 */
	public void setToDetails(Date toDetails) {
		this.toDetails = toDetails;
	}

	/**
	 * @return the writerIdDetails
	 */
	public long getWriterIdDetails() {
		return writerIdDetails;
	}

	/**
	 * @param writerIdDetails the writerIdDetails to set
	 */
	public void setWriterIdDetails(long writerIdDetails) {
		this.writerIdDetails = writerIdDetails;
	}

	/**
	 * @return the currencyIdDetails
	 */
	public long getCurrencyIdDetails() {
		return currencyIdDetails;
	}

	/**
	 * @param currencyIdDetails the currencyIdDetails to set
	 */
	public void setCurrencyIdDetails(long currencyIdDetails) {
		this.currencyIdDetails = currencyIdDetails;
	}

	/**
	 * @return the skinIdDetails
	 */
	public long getSkinIdDetails() {
		return skinIdDetails;
	}

	/**
	 * @param skinIdDetails the skinIdDetails to set
	 */
	public void setSkinIdDetails(long skinIdDetails) {
		this.skinIdDetails = skinIdDetails;
	}

	/**
	 * @return the selectedTotalsRow
	 */
	public SalesTurnoversTotals getSelectedTotalsRow() {
		return selectedTotalsRow;
	}

	/**
	 * @param selectedTotalsRow the selectedTotalsRow to set
	 */
	public void setSelectedTotalsRow(SalesTurnoversTotals selectedTotalsRow) {
		this.selectedTotalsRow = selectedTotalsRow;
	}

	public ArrayList<SalesTurnoversDetails> getDetailsSumList() {
		return detailsSumList;
	}

	public void setDetailsSumList(ArrayList<SalesTurnoversDetails> detailsSumList) {
		this.detailsSumList = detailsSumList;
	}

	public ArrayList<SalesTurnoversDetails> getDetailsList() {
		return detailsList;
	}

	public void setDetailsList(ArrayList<SalesTurnoversDetails> detailsList) {
		this.detailsList = detailsList;
	}

	public int getDetailsListSize() {
		return detailsListSize;
	}

	public void setDetailsListSize(int detailsListSize) {
		this.detailsListSize = detailsListSize;
	}

	public int getDetailsFromTotalsSize() {
		return detailsFromTotalsSize;
	}

	public void setDetailsFromTotalsSize(int detailsFromTotalsSize) {
		this.detailsFromTotalsSize = detailsFromTotalsSize;
	}

	public ArrayList<SalesTurnoversDetails> getDetailsListFromTotals() {
		return detailsListFromTotals;
	}

	public void setDetailsListFromTotals(
			ArrayList<SalesTurnoversDetails> detailsListFromTotals) {
		this.detailsListFromTotals = detailsListFromTotals;
	}

	/**
	 * @return the salesTypeDepId
	 */
	public long getSalesTypeDepId() {
		return salesTypeDepId;
	}

	/**
	 * @param salesTypeDepId the salesTypeDepId to set
	 */
	public void setSalesTypeDepId(long salesTypeDepId) {
		this.salesTypeDepId = salesTypeDepId;
	}

	/**
	 * @return the salesTypeDepIdDetails
	 */
	public long getSalesTypeDepIdDetails() {
		return salesTypeDepIdDetails;
	}

	/**
	 * @param salesTypeDepIdDetails the salesTypeDepIdDetails to set
	 */
	public void setSalesTypeDepIdDetails(long salesTypeDepIdDetails) {
		this.salesTypeDepIdDetails = salesTypeDepIdDetails;
	}

	/**
	 * @return the tranClassTypeIdDetails
	 */
	public long getTranClassTypeIdDetails() {
		return tranClassTypeIdDetails;
	}

	/**
	 * @param tranClassTypeIdDetails the tranClassTypeIdDetails to set
	 */
	public void setTranClassTypeIdDetails(long tranClassTypeIdDetails) {
		this.tranClassTypeIdDetails = tranClassTypeIdDetails;
	}
}
