package il.co.etrader.backend.mbeans;

import java.io.Serializable;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.ValueChangeEvent;

import com.anyoption.backend.managers.TradersActionsManager;
import com.anyoption.common.beans.Opportunity;

import il.co.etrader.backend.bl_managers.AdminManager;
import il.co.etrader.backend.bl_managers.InvestmentsManager;
import il.co.etrader.backend.bl_managers.TraderManager;
import il.co.etrader.backend.bl_vos.User;
import il.co.etrader.backend.util.Constants;
import il.co.etrader.bl_vos.InvestmentLimit;
import il.co.etrader.util.CommonUtil;
import il.co.etrader.util.ConstantsBase;

public class InvLimitsForm implements Serializable {

	private static final long serialVersionUID = 347864375494777614L;

	private InvestmentLimit limit;
	private ArrayList<InvestmentLimit> list;

	// Filter params
	private Date from;
	private Date to;
	private long userId;
	private long active;
	private long marketId;

	public InvLimitsForm() throws SQLException{
		GregorianCalendar gc = new GregorianCalendar();
		gc.add(Calendar.MONTH, -1);
		from = gc.getTime();
		gc.set(Calendar.YEAR, gc.get(Calendar.YEAR) + Constants.USER_MARKET_DISABLE_PERID);
		to = gc.getTime();
		active = 0;
		marketId = 0;

		search();
	}


	public String updateInsert() throws SQLException{
		// If it's a new limit, check type by user and market
		if (limit.getId() == 0){
			FacesContext context = FacesContext.getCurrentInstance();
			User user =(User)context.getApplication().createValueBinding(Constants.BIND_USER).getValue(context);
			if (user.isSupportSelected()) {
				limit.setUserId(user.getId());
			}
			if (limit.getUserId() == ConstantsBase.ALL_FILTER_ID){

				if(limit.getMarketId() == ConstantsBase.ALL_FILTER_ID){
					FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR,
							CommonUtil.getMessage("error.inv.limit.must.choose.user.or.market", null),null);
					context.addMessage(null, fm);
                    
					return null;
				}else{
					limit.setInvLimitTypeId(InvestmentLimit.TYPE_MARKET);
				}
			}else{
				if(limit.getMarketId() == ConstantsBase.ALL_FILTER_ID){
					limit.setInvLimitTypeId(InvestmentLimit.TYPE_USERS);
				}else{
					limit.setInvLimitTypeId(InvestmentLimit.TYPE_USER_MARKET);
				}
			}
		}

		// calc start date
		Calendar c1 = Calendar.getInstance();
		c1.setTime(limit.getStartDate());
		c1.set(Calendar.HOUR_OF_DAY, Integer.parseInt(limit.getStartDateHour()));
		c1.set(Calendar.MINUTE, Integer.parseInt(limit.getStartDateMin()));
		limit.setStartDate(c1.getTime());

		// calc end date by period
		Calendar c = Calendar.getInstance();
		c.setTime(limit.getStartDate());
		c.add(Calendar.YEAR, Constants.USER_MARKET_LIMIT_PERID);
		limit.setEndDate(c.getTime());
		limit.setWriterId(AdminManager.getWriterId());

		boolean res=InvestmentsManager.updateInsertInvLimit(limit);
		if (!res) {
			return null;
		}
		TraderManager.addToLog(limit.getWriterId(), "investment_limits", limit.getMarketId(), ConstantsBase.LOG_COMMANDS_CHANGE_MAX_MIN_INV, "");
		TradersActionsManager.refreshUsersCache(limit.getWriterId());
		search();
		return Constants.NAV_INV_LIMITS;
	}

	public String navEdit() {
		return Constants.NAV_INV_LIMIT;
	}

	public String navList() throws SQLException{
		search();
		return Constants.NAV_INV_LIMITS;
	}

	public String navNew() throws SQLException{
		limit=new InvestmentLimit();

		return Constants.NAV_INV_LIMIT;
	}

	public String search() throws SQLException{
		FacesContext context = FacesContext.getCurrentInstance();
		User user =(User)context.getApplication().createValueBinding(Constants.BIND_USER).getValue(context);
		if (user.isSupportSelected()) {
			userId = user.getId();
		}
		list=InvestmentsManager.searchInvLimits(from, to, userId, active, marketId);
		CommonUtil.setTablesToFirstPage();
		return null;
	}

	public ArrayList<InvestmentLimit> getList() {
		return list;
	}
	public void setList(ArrayList<InvestmentLimit> list) {
		this.list = list;
	}
	public int getListSize() {
		return list.size();
	}

	public InvestmentLimit getLimit() {
		return limit;
	}

	public void setLimit(InvestmentLimit limit) {
		this.limit = limit;
	}


	/**
	 * Constructs a <code>String</code> with all attributes
	 * in name = value format.
	 *
	 * @return a <code>String</code> representation
	 * of this object.
	 */
	public String toString()
	{
	    final String TAB = " \n ";

	    String retValue = "";

	    retValue = "InvLimitsForm ( "
	        + super.toString() + TAB
	        + "limit = " + this.limit + TAB
	        + "list = " + this.list + TAB
	        + " )";

	    return retValue;
	}


	public void updateOpportunityTypeId(ValueChangeEvent ev) throws SQLException {
		FacesContext context = FacesContext.getCurrentInstance();
		Long newOpportunityTypeId = (Long)ev.getNewValue();

		limit.setOpportunityTypeId(newOpportunityTypeId);
		if (Opportunity.TYPE_REGULAR !=  newOpportunityTypeId){
			limit.setScheduled(ConstantsBase.ALL_FILTER_ID);
		}

		context.renderResponse();
	}


	/**
	 * @return the active
	 */
	public long getActive() {
		return active;
	}


	/**
	 * @param active the active to set
	 */
	public void setActive(long active) {
		this.active = active;
	}


	/**
	 * @return the from
	 */
	public Date getFrom() {
		return from;
	}


	/**
	 * @param from the from to set
	 */
	public void setFrom(Date from) {
		this.from = from;
	}


	/**
	 * @return the marketId
	 */
	public long getMarketId() {
		return marketId;
	}


	/**
	 * @param marketId the marketId to set
	 */
	public void setMarketId(long marketId) {
		this.marketId = marketId;
	}


	/**
	 * @return the to
	 */
	public Date getTo() {
		return to;
	}


	/**
	 * @param to the to to set
	 */
	public void setTo(Date to) {
		this.to = to;
	}


	/**
	 * @return the userId
	 */
	public long getUserId() {
		return userId;
	}


	/**
	 * @param userId the userId to set
	 */
	public void setUserId(long userId) {
		this.userId = userId;
	}
}
