package il.co.etrader.backend.mbeans;


import java.io.IOException;
import java.io.PrintWriter;
import java.io.Serializable;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.GregorianCalendar;

import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpServletResponse;

import il.co.etrader.backend.bl_managers.TargetsManager;
import il.co.etrader.backend.util.Constants;
import il.co.etrader.backend.util.Utils;
import il.co.etrader.bl_vos.MediaBuyerTarget;
import il.co.etrader.util.CommonUtil;


public class TargetsForm implements Serializable {

	private static final long serialVersionUID = 5730605078390266798L;

	private Date toDate;
	private String mediaBuyerName;
    private ArrayList<MediaBuyerTarget> mediaBuyersTargetslist;
    private long skinId;
    private long mediaBuyerId;
    private String year;
    private String month;
    private String day;
    private long skinBusinessCaseId;
    private MediaBuyerTarget mediaBuyer;
    private Date date ;
    private Date monthDate ;
    private String htmlBuffer;
    private GregorianCalendar gc ;
    private boolean displayDailyTable;


	public TargetsForm() throws SQLException{
        mediaBuyerId = 0;
        skinId = 0;
        gc = new GregorianCalendar();
        year = String.valueOf(gc.get(GregorianCalendar.YEAR));
        month = String.valueOf(gc.get(GregorianCalendar.MONTH)+1);
        if (month.length() == 1) {
            month="0"+month;
        }
        day = String.valueOf(gc.get(GregorianCalendar.DAY_OF_MONTH)-1);
        gc.set(Calendar.DAY_OF_MONTH, Integer.valueOf(day));
        toDate = gc.getTime();
        mediaBuyer = new MediaBuyerTarget();
        displayDailyTable = false;
        search();
	}


    public String searchDaily() throws SQLException{
        boolean sortAscending = false;
        displayDailyTable = true;
        MediaBuyerTarget.totalRegMade = 0;
        MediaBuyerTarget.totalDepMade = 0;
        mediaBuyersTargetslist = TargetsManager.getAllDailyTargets(toDate);
        if (MediaBuyerTarget.totalRegMade == 0 && MediaBuyerTarget.totalDepMade == 0){
            mediaBuyersTargetslist = new ArrayList<MediaBuyerTarget>();
            mediaBuyersTargetslist.size();
        } else {
            Collections.sort(mediaBuyersTargetslist, new PercentOfTotalDepositorsComparator(sortAscending));
            MediaBuyerTarget totalline = new MediaBuyerTarget();
            totalline.setRegMade(MediaBuyerTarget.totalRegMade);
            totalline.setDepMade(MediaBuyerTarget.totalDepMade);
            totalline.setMediaBuyerName("TOTAL");
            mediaBuyersTargetslist.add(0, totalline);
        }
        CommonUtil.setTablesToFirstPage();
        return null;
    }

    public String search() throws SQLException{
        displayDailyTable = false;
        gc.set(Calendar.DAY_OF_MONTH, 1);
        gc.set(Calendar.MONTH, Integer.valueOf(month)-1);
        gc.set(Calendar.YEAR, Integer.valueOf(year));
        monthDate = gc.getTime();
        MediaBuyerTarget.totalRegMade = 0;
        MediaBuyerTarget.totalDepMade = 0;
        MediaBuyerTarget.totalTargetReg = 0;
        MediaBuyerTarget.totalTargetDep = 0;
        mediaBuyersTargetslist = TargetsManager.getAllTargets(mediaBuyerId, monthDate , skinId, skinBusinessCaseId);
        if (mediaBuyersTargetslist.size() != 0){
            MediaBuyerTarget totalline = new MediaBuyerTarget();
            totalline.setRegMade(MediaBuyerTarget.totalRegMade);
            totalline.setDepMade(MediaBuyerTarget.totalDepMade);
            totalline.setRegTarget(MediaBuyerTarget.totalTargetReg);
            totalline.setDepTarget(MediaBuyerTarget.totalTargetDep);
            totalline.setMediaBuyerName("TOTAL");
            mediaBuyersTargetslist.add(0, totalline);
        }
        CommonUtil.setTablesToFirstPage();
        return null;
    }


    public String updateInsertTargets() throws SQLException {
        boolean res = TargetsManager.updateInsertTargets(mediaBuyer);
        if (!res) {
            return null;
        }
        search();
        return Constants.NAV_MEDIA_BUYER_TARGETS;
    }


    /**
     * Navigate to insert new bonus
     * @return
     * @throws SQLException
     */
    public String navNew() throws SQLException {
        // Set default date
        GregorianCalendar gc1 = new GregorianCalendar();
        gc1.set(Calendar.YEAR, Integer.valueOf(year));
        gc1.set(Calendar.MONTH, Integer.valueOf(month)-1);
        gc1.set(Calendar.DAY_OF_MONTH, 1);
        date = gc1.getTime();
        mediaBuyer = new MediaBuyerTarget();
        mediaBuyer.setUpdatedWriter((int)Utils.getWriter().getWriter().getId());
        mediaBuyer.setMonthTarget(date);
        mediaBuyer.setYearTarget(date);
        return Constants.NAV_MEDIA_BUYER_TARGET;
    }


    /**
     * Navigate to edit target
     * @return
     * @throws SQLException
     */
    public String navEdit() {
        mediaBuyer.setYearTarget(mediaBuyer.getMonthTarget());
        return Constants.NAV_MEDIA_BUYER_TARGET;
    }


    public void exportHtmlTableToExcel() throws IOException {

        //Set the filename
        Date dt = new Date();
        SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd_HHmmss");
        String filename = fmt.format(dt) + ".xls";
        htmlBuffer=
            "<html><head><meta http-equiv=\"Content-Type\" content=\"text/html; CHARSET=UTF-8\"></head>"+htmlBuffer+"</html>";


        //Setup the output
        String contentType = "application/vnd.ms-excel";
        FacesContext fc = FacesContext.getCurrentInstance();
        filename = "marketing_targets_"+ filename;
        HttpServletResponse response = (HttpServletResponse)fc.getExternalContext().getResponse();
        response.setHeader("Content-disposition", "attachment; filename=" + filename);
        response.setContentType(contentType);
        response.setCharacterEncoding("UTF-8");

        //Write the table back out
        PrintWriter out = response.getWriter();
        out.print(htmlBuffer);
        out.close();
        fc.responseComplete();
    }

	public int getListSize() {
		return mediaBuyersTargetslist.size();
	}

    private class PercentOfTotalDepositorsComparator implements Comparator<MediaBuyerTarget> {
        private boolean ascending;
        public PercentOfTotalDepositorsComparator(boolean asc) {
            ascending=asc;
        }
        public int compare(MediaBuyerTarget target1, MediaBuyerTarget target2) {
                if (ascending){
                    return new Long(Long.valueOf(target1.getPercentOfTotalDep().substring(0, target1.getPercentOfTotalDep().indexOf("%")))).compareTo(new Long(Long.valueOf(target2.getPercentOfTotalDep().substring(0, target2.getPercentOfTotalDep().indexOf("%")))));
                }
            return -new Long(Long.valueOf(target1.getPercentOfTotalDep().substring(0, target1.getPercentOfTotalDep().indexOf("%")))).compareTo(new Long(Long.valueOf(target2.getPercentOfTotalDep().substring(0, target2.getPercentOfTotalDep().indexOf("%")))));
        }
    }

//    public boolean isHasDevelopmentRole() throws SQLException{
//        ArrayList<SelectItem> list = null;
//        list = WritersManager.getWritersByDepId(Constants.DEPARTMENT_ID_DEVELOPMENT);
//        for (int i = 0; i<list.size();i++){
//            if (list(i).
//        }
//        return true;
//    }

    /**
     * @return the skinId
     */
    public long getSkinId() {
        return skinId;
    }


    /**
     * @param skinId the skinId to set
     */
    public void setSkinId(long skinId) {
        this.skinId = skinId;
    }


    /**
     * @return the mediaBuyerName
     */
    public String getMediaBuyerName() {
        return mediaBuyerName;
    }


    /**
     * @param mediaBuyerName the mediaBuyerName to set
     */
    public void setMediaBuyerName(String mediaBuyerName) {
        this.mediaBuyerName = mediaBuyerName;
    }


    /**
     * @return the years
     */
    public ArrayList getYears() {
        ArrayList<SelectItem> years = new ArrayList<SelectItem>();
        GregorianCalendar gc1 = new GregorianCalendar();
        for (int i = gc1.get(GregorianCalendar.YEAR) + 9; i >= gc1.get(GregorianCalendar.YEAR) - 4; i--) {
            years.add(new SelectItem(String.valueOf(i), String.valueOf(i)));
        }
        return years;
    }

    /**
     * @return the skinBusinessCaseId
     */
    public long getSkinBusinessCaseId() {
        return skinBusinessCaseId;
    }


    /**
     * @param skinBusinessCaseId the skinBusinessCaseId to set
     */
    public void setSkinBusinessCaseId(long skinBusinessCaseId) {
        this.skinBusinessCaseId = skinBusinessCaseId;
    }


    /**
     * @param year the year to set
     */
    public void setYear(String year) {
        this.year = year;
    }


    /**
     * @param month the month to set
     */
    public void setMonth(String month) {
        this.month = month;
    }


    /**
     * @return the month
     */
    public String getMonth() {
        return month;
    }

    /**
     * @return the year
     */
    public String getYear() {
        return year;
    }

    /**
     * @return the mediaBuyer
     */
    public MediaBuyerTarget getMediaBuyer() {
        return mediaBuyer;
    }

    /**
     * @param mediaBuyer the mediaBuyer to set
     */
    public void setMediaBuyer(MediaBuyerTarget mediaBuyer) {
        this.mediaBuyer = mediaBuyer;
    }

    /**
     * @return the mediaBuyerId
     */
    public long getMediaBuyerId() {
        return mediaBuyerId;
    }

    /**
     * @param mediaBuyerId the mediaBuyerId to set
     */
    public void setMediaBuyerId(long mediaBuyerId) {
        this.mediaBuyerId = mediaBuyerId;
    }

    /**
     * @return the mediaBuyersTargetslist
     */
    public ArrayList<MediaBuyerTarget> getMediaBuyersTargetslist() {
        return mediaBuyersTargetslist;
    }

    /**
     * @param mediaBuyersTargetslist the mediaBuyersTargetslist to set
     */
    public void setMediaBuyersTargetslist(
            ArrayList<MediaBuyerTarget> mediaBuyersTargetslist) {
        this.mediaBuyersTargetslist = mediaBuyersTargetslist;
    }


    /**
     * @return the monthDate
     */
    public Date getMonthDate() {
        return monthDate;
    }


    /**
     * @param monthDate the monthDate to set
     */
    public void setMonthDate(Date monthDate) {
        this.monthDate = monthDate;
    }


    /**
     * @return the htmlBuffer
     */
    public String getHtmlBuffer() {
        return htmlBuffer;
    }


    /**
     * @param htmlBuffer the htmlBuffer to set
     */
    public void setHtmlBuffer(String htmlBuffer) {
        this.htmlBuffer = htmlBuffer;
    }


    /**
     * @return the toDate
     */
    public Date getToDate() {
        return toDate;
    }


    /**
     * @param toDate the toDate to set
     */
    public void setToDate(Date toDate) {
        this.toDate = toDate;
    }

    /**
     * @return the displayTable
     */
    public boolean isDisplayDailyTable() {
        return displayDailyTable;
    }


    /**
     * @param displayTable the displayTable to set
     */
    public void setDisplayDailyTable(boolean displayTable) {
        this.displayDailyTable = displayTable;
    }

}
