package il.co.etrader.backend.mbeans;

import java.io.Serializable;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;

import il.co.etrader.backend.bl_managers.InvestmentsManager;
import il.co.etrader.backend.util.Constants;
import il.co.etrader.bl_vos.ExchangesHolidays;
import il.co.etrader.util.CommonUtil;

public class ExchangesHolidaysForm implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2907203816588350044L;
	private ArrayList list;
	private Long exchangeId;
	private Date from;
    private ExchangesHolidays exchangesHolidays;

	public ExchangesHolidaysForm() throws SQLException{
		exchangeId=null;
		from=null;
		search();
	}

	public String navNew() throws SQLException{
		exchangesHolidays=new ExchangesHolidays();
		exchangesHolidays.setHoliday(new Date());

		return Constants.NAV_EXCHANGE_HOLIDAYS;
	}

	public int getListSize() {
		return list.size();
	}

	public String search() throws SQLException{
		list=InvestmentsManager.searchExchangesHolidays(exchangeId,from);
		CommonUtil.setTablesToFirstPage();
		return null;
	}

	public ArrayList getList() {
		return list;
	}

	public void setList(ArrayList list) {
		this.list = list;
	}

	public String update() throws SQLException{
		boolean res=InvestmentsManager.updateInsertExchangeHoliday(exchangesHolidays);
		if (res==false)
			return null;
		search();
		return Constants.NAV_EXCHANGES_HOLIDAYS;
	}

	public ExchangesHolidays getExchangesHolidays() {
		return exchangesHolidays;
	}


	public void setExchangesHolidays(ExchangesHolidays exchangesHolidays) {
		this.exchangesHolidays = exchangesHolidays;
	}

	/**
	 * Constructs a <code>String</code> with all attributes
	 * in name = value format.
	 *
	 * @return a <code>String</code> representation
	 * of this object.
	 */
	public String toString()
	{
	    final String TAB = "    ";

	    String retValue = "";

	    retValue = "ExchangesHolidaysForm ( "
	        + super.toString() + TAB
	        + "list = " + this.list + TAB
	        + "exchangesHolidays = " + this.exchangesHolidays + TAB
	        + " )";

	    return retValue;
	}

	public Long getExchangeId() {
		return exchangeId;
	}

	public void setExchangeId(Long exchangeId) {
		this.exchangeId = exchangeId;
	}

	public Date getFrom() {
		return from;
	}

	public void setFrom(Date from) {
		this.from = from;
	}



}
