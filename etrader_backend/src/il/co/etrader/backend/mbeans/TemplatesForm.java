package il.co.etrader.backend.mbeans;

import java.io.Serializable;
import java.io.StringWriter;
import java.math.BigDecimal;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.TimeZone;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.AjaxBehaviorEvent;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;

import org.apache.log4j.Logger;
import org.apache.velocity.VelocityContext;

import com.anyoption.common.beans.CreditCard;
import com.anyoption.common.beans.Issue;
import com.anyoption.common.beans.IssueAction;
import com.anyoption.common.beans.base.Skin;
import com.anyoption.common.beans.base.UserRegulationBase;
import com.anyoption.common.managers.LanguagesManagerBase;
import com.anyoption.common.managers.UserRegulationManager;
import com.anyoption.common.util.SendTemplateEmail;

import il.co.etrader.backend.bl_managers.AdminManager;
import il.co.etrader.backend.bl_managers.PDFManager;
import il.co.etrader.backend.bl_managers.TransactionsManager;
import il.co.etrader.backend.bl_managers.UsersManager;
import il.co.etrader.backend.bl_vos.User;
import il.co.etrader.backend.bl_vos.WriterWrapper;
import il.co.etrader.backend.util.Constants;
import il.co.etrader.backend.util.Utils;
import il.co.etrader.bl_managers.EmailAuthenticationManagerBase;
import il.co.etrader.bl_managers.IssuesManagerBase;
import il.co.etrader.bl_managers.ResetPasswordManager;
import il.co.etrader.bl_managers.TransactionsManagerBase;
import il.co.etrader.bl_managers.UsersManagerBase;
import il.co.etrader.bl_vos.Skins;
import il.co.etrader.bl_vos.Template;
import il.co.etrader.bl_vos.UserBase;
import il.co.etrader.util.CommonUtil;
import il.co.etrader.util.ConstantsBase;

public class TemplatesForm implements Serializable {

	private static final long serialVersionUID = -3125599053382582373L;

	private static final Logger log = Logger.getLogger(TemplatesForm.class);

    public static final long NUM_OF_DAYS_USER_DID_NOT_MADE_DEPOSIT = 180;
	private Template template;
	private ArrayList<Template> list;
	private ArrayList<SelectItem> templateSubjects;
	private String templateId;
	private boolean displayCcList;
	private String cardId;
	private boolean userMadeDepositPast180Days = true;
	private String templateSendTo;
	private boolean displaySendMailToSupport;
	private boolean displayYearsList;
	private ArrayList<SelectItem> yearsList;
	private long year;
	private String blankTemplateContent;
	private String blankTemplateSubject;
	private Date from;
	private Date to;
	private boolean displayDates;

	public TemplatesForm() throws Exception {
		search();
		templateSendTo = "0";
		displaySendMailToSupport = true;
	}

	public int getListSize() {
		return list.size();
	}

	public String send() throws Exception {

		FacesContext context = FacesContext.getCurrentInstance();
		UserBase user = (UserBase)context.getApplication().createValueBinding(Constants.BIND_USER).getValue(context);		
					
		if (mailToSendTemplate(context, user) == null) {
			return null;
		}
					
		Issue issue = new Issue();
		IssueAction issueAction = new IssueAction();

		String[] args = new String[1];
		args[0] = String.valueOf(user.getId());
		
		Template template = AdminManager.getTemplateById(Long.parseLong(templateId));
		String subjectId = String.valueOf(IssuesManagerBase.ISSUE_SUBJ_GENERAL);
		String statusId = String.valueOf(IssuesManagerBase.ISSUE_STATUS_FINISHED);
		String channelId = String.valueOf(IssuesManagerBase.ISSUE_CHANNEL_EMAIL);
		String comments = CommonUtil.getMessage(template.getSubject(), args);
		
		if (template.getId() == Constants.TEMPLATE_BLANK) {
			subjectId = String.valueOf(IssuesManagerBase.ISSUE_SUBJECT_BLANK_TEMPLATE);
			channelId = String.valueOf(IssuesManagerBase.ISSUE_CHANNEL_EMAIL);
			comments = blankTemplateContent;			
			setBlankTemplateContent("");
			setBlankTemplateSubject("");
		}
		
		// Filling issue fields
		issue.setSubjectId(subjectId);
		issue.setStatusId(statusId);
		issue.setUserId(user.getId());
		issue.setContactId(user.getContactId());
		issue.setPriorityId(String.valueOf(IssuesManagerBase.ISSUE_PRIORITY_LOW));
		issue.setType(ConstantsBase.ISSUE_TYPE_REGULAR);
		// Filling action fields
		issueAction.setChannelId(channelId);
		issueAction.setActionTypeId(String.valueOf(IssuesManagerBase.ISSUE_ACTION_EMAIL));
		issueAction.setWriterId( AdminManager.getWriterId());
		issueAction.setSignificant(false);
		issueAction.setActionTime(new Date());
		issueAction.setActionTimeOffset(Utils.getWriter().getUtcOffset());
		issueAction.setTemplateId(template.getId());		
		issueAction.setComments(comments);

		IssuesManagerBase.insertIssue(issue, issueAction, user.getUtcOffset(),
				AdminManager.getWriterId(), user, 0);

		search();
		return Constants.NAV_SEND_TEMPLATE;
	}

	public String updateInsertTemplate() throws Exception {
		boolean res = AdminManager.updateInsertTemplate(template);
		if (res == false) {
			return null;
		}
		search();
		return Constants.NAV_TEMPLATES;
	}


	public String bNavAddTemplate() {
		template=new Template();
		return Constants.NAV_TEMPLATE;
	}
	public String bNavEditTemplate() {
		return Constants.NAV_TEMPLATE;
	}

	public ArrayList getTemplateSubjects() {
		return templateSubjects;
	}

	public String bNavAdminTemplates() throws Exception{
		search();
		return Constants.NAV_TEMPLATES;
	}

    public void updateChange(final AjaxBehaviorEvent event) throws SQLException{
    	if (Long.parseLong(templateId) == Constants.TEMPLATE_POA ||
    			Long.parseLong(templateId) == Constants.TEMPLATE_FIRST_WITHDRAWAL ||
    			Long.parseLong(templateId) == Constants.TEMPLATE_5DEPOSITS) {
    		displayCcList = true;
    	} else {
    		displayCcList = false;
    	}
    	// check if display the option to send email to support, customer or both.
    	if (Long.parseLong(templateId) == Constants.TEMPLATE_PASSWORD_REMINDER) {
    		displaySendMailToSupport = false;
    	} else {
    		displaySendMailToSupport = true;
    	}
    	
    	if(Long.parseLong(templateId) ==  Constants.TEMPLATE_INCOME_TAX_ID) {
    		displayYearsList = true;
    		setYearList();
    	} else {
    		displayYearsList = false;
    	}
    	
		if (Long.parseLong(templateId) == Constants.TEMPLATE_FIRST_WITHDRAWAL
				|| Long.parseLong(templateId) == Constants.TEMPLATE_JUST_CONFIRMATION
				|| Long.parseLong(templateId) == Constants.TEMPLATE_JUST_CONFIRMATION_OTHER
				|| Long.parseLong(templateId) == Constants.TEMPLATE_NON_CC_WITHDRAWAL) {
			displayDates = true;
		} else {
			displayDates = false;
		}
    }

	/**
	 * Get templates by user skin id
	 * @return
	 * 		list of templates subjects
	 * @throws Exception 
	 */
	public String search() throws Exception{
		FacesContext context=FacesContext.getCurrentInstance();
		User user = Utils.getUser();
		WriterWrapper w = Utils.getWriter();
		String skins = "";
		GregorianCalendar gc = new GregorianCalendar(TimeZone.getTimeZone(w.getUtcOffset()));
		to = gc.getTime();
		gc.add(Calendar.MONTH, -6);
		from = gc.getTime();
		
		//if ( user.isSupportSelected() ) {
			list = AdminManager.searchTemplatesByUserSkin(user.getSkinId());
		//}
		//else {   // admin selected
			//skins = w.getSkinsToString();
			//list = AdminManager.searchTemplatesByWriterSkins(skins);
		//}
		userMadeDepositPast180Days = TransactionsManager.userMadeDepositsInLastXDays(user.getId(), NUM_OF_DAYS_USER_DID_NOT_MADE_DEPOSIT);
		if(!userMadeDepositPast180Days) {
			FacesMessage fm = new FacesMessage(CommonUtil.getMessage("user.did.not.made.deposit.180.days", null),null);
			context.addMessage(null, fm);
		}					
		templateSubjects = new ArrayList<SelectItem>();
		String generalSubjectKey = ".general";
		int tempIdToRemove = Constants.TEMPLATE_BANK_TRANSFER_DETAILS_CONTACT;
		if (user.getId() == 0) {
			tempIdToRemove = Constants.TEMPLATE_BANK_TRANSFER_DETAILS_USER;
		}

		for ( Template t : list ) {
			if (t.getId() == Constants.TEMPLATE_BANK_TRANSFER_DETAILS_CONTACT ||
					t.getId() == Constants.TEMPLATE_BANK_TRANSFER_DETAILS_USER) {
				if (user.isEtrader()) {
					generalSubjectKey = Constants.TEMPLATE_BANK_TRANSFER_DETAILS_ETRADER;
				} else {
					generalSubjectKey = Constants.TEMPLATE_BANK_TRANSFER_DETAILS_AO;
				}
			}
			if((t.getId() != Constants.TEMPLATE_BEFORE_DEPOSIT)&&(t.getId() != Constants.TEMPLATE_AFTER_DEPOSIT)) {
				if (!userMadeDepositPast180Days) {
		            //we don't want to display this templates but we want to keep them in DB
					if(t.getId() != 1 && t.getId() != 2 && t.getId() != 35 && t.getId() != tempIdToRemove)  {
						if(user.getDeposits() > 0) {
							templateSubjects.add(new SelectItem(String.valueOf(t.getId()), CommonUtil.getMessage(t.getSubject() + generalSubjectKey , null)));
						}
						if(user.getDeposits() == 0 && t.getId() != 38 && t.getId() != 39) {
							templateSubjects.add(new SelectItem(String.valueOf(t.getId()), CommonUtil.getMessage(t.getSubject() + generalSubjectKey , null)));
						}
					}
				} else {
		            //we don't want to display this templates but we want to keep them in DB
					if(t.getId() != 1 && t.getId() != 2 && t.getId() != 35 && t.getId() != tempIdToRemove)
					templateSubjects.add(new SelectItem(String.valueOf(t.getId()), CommonUtil.getMessage(t.getSubject() + generalSubjectKey , null)));
				}
			}else{
				if(user.getId()!=Skin.SKIN_CHINESE_VIP && user.getSkinId() != Skin.SKIN_ARABIC && user.getSkinId() != Skin.SKIN_EN_US && user.getSkinId() != Skin.SKIN_ES_US && user.getSkinId()!=Skin.SKIN_KOREAN){
					   if(t.getId() == Constants.TEMPLATE_AFTER_DEPOSIT){
						   templateSubjects.add(new SelectItem(Constants.TEMPLATE_AFTER_DEPOSIT, CommonUtil.getMessage("afterdeposit.subject",null)));
					   }
					   if(t.getId() == Constants.TEMPLATE_BEFORE_DEPOSIT){
						   templateSubjects.add(new SelectItem(Constants.TEMPLATE_BEFORE_DEPOSIT, CommonUtil.getMessage("beforedeposit.subject",null)));
					   }
				}
			}
		}
		// For now Sales should see only "Thank you for your time" template when locking contact
		if (user.isRetentionSelected() && user.getId() == 0) {
			templateSubjects.clear();
			if(!CommonUtil.isParameterEmptyOrNull((user.getEmail()))) {
				templateSubjects.add(new SelectItem(Constants.TEMPLATE_CUSTOMER_SERVICE, CommonUtil.getMessage("following.sales.call.subject.general",null)));
			}
		}
		Collections.sort(templateSubjects, new CommonUtil.selectItemComparator());
		CommonUtil.setTablesToFirstPage();
		return null;
	}
	
	private void setYearList(){
		FacesContext context=FacesContext.getCurrentInstance();
		UserBase user = (UserBase)context.getApplication().createValueBinding(Constants.BIND_USER).getValue(context);
		Date time = CommonUtil.getDateTimeFormaByTz(GregorianCalendar.getInstance().getTime(), user.getUtcOffset());
		Calendar c = Calendar.getInstance();
		c.setTime(time);
		year = c.get(Calendar.YEAR) - 1;

		c.setTime(CommonUtil.getDateTimeFormaByTz(user.getTimeCreated(), user.getUtcOffset()));
		long regYear = c.get(Calendar.YEAR);

		yearsList = new ArrayList<SelectItem>();
		for (long y = year; y >= regYear - 1; y--){
			yearsList.add(new SelectItem(new Long(y), String.valueOf(y)));
		}
	}


	public ArrayList getList() {
		return list;
	}

	public Template getTemplate() {
		return template;
	}

	public void setTemplate(Template template) {
		this.template = template;
	}

	public String getTemplateId() {
		return templateId;
	}

	public void setTemplateId(String templateId) {
		this.templateId = templateId;
	}

	public String getFilesPath() {
		return CommonUtil.getProperty(Constants.TEMPLATES_SERVER_PATH);
	}

	/**
	 * Constructs a <code>String</code> with all attributes
	 * in name = value format.
	 *
	 * @return a <code>String</code> representation
	 * of this object.
	 */
	public String toString()
	{
	    final String TAB = " \n ";

	    String retValue = "";

	    retValue = "TemplatesForm ( "
	        + super.toString() + TAB
	        + "template = " + this.template + TAB
	        + "list = " + this.list + TAB
	        + "templateSubjects = " + this.templateSubjects + TAB
	        + "templateId = " + this.templateId + TAB
	        + " )";

	    return retValue;
	}

	public String getCardId() {
		return cardId;
	}

	public void setCardId(String cardId) {
		this.cardId = cardId;
	}

	public boolean isDisplayCcList() {
		return displayCcList;
	}

	public void setDisplayCcList(boolean displayCcList) {
		this.displayCcList = displayCcList;
	}

	/**
	 * 
	 * @return templateSendTo
	 */
	public String getTemplateSendTo() {
		return templateSendTo;
	}

	/**
	 * 
	 * @param templateSendTo the templateSendTo to set
	 */
	public void setTemplateSendTo(String templateSendTo) {
		this.templateSendTo = templateSendTo;
	}
	
	/**
	 * Change templateSendTo radio button
	 * @param event
	 * @throws SQLException
	 * @throws ParseException
	 */
    public void updateChangeSendTo(ValueChangeEvent event) throws SQLException, ParseException{
    	String newTemplateSendTo = (String)event.getNewValue();
    	if (!templateSendTo.equals(newTemplateSendTo)){
    		templateSendTo = newTemplateSendTo;
    	}

    }
    
    /**
     * 
     * @param user
     */
    public String mailToSendTemplate(FacesContext context, UserBase user) throws Exception{
    	String messageError = null;
    	String emailSupport = CommonUtil.getProperty("email.to.anyoption");
		if (user.getSkinId() == Skin.SKIN_CHINESE) {
			emailSupport = CommonUtil.getProperty("email.to.anyoption.zh");
		} else if (user.getSkinId() == Skin.SKIN_ETRADER) {
			emailSupport = CommonUtil.getProperty("email.to.etrader");
		}
    	int sendTo = Integer.parseInt(templateSendTo);
    	switch (sendTo) {
		case Constants.TEMPLATES_SEND_CUSTOMER:
			messageError = sendTemplates(context, user, null);
			break;			
		case Constants.TEMPLATES_SEND_SUPPORT:
			messageError = sendTemplates(context, user, emailSupport);
			break;
		case Constants.TEMPLATES_SEND_CUSTOMER_AND_SUPPORT:
			sendTemplates(context, user, emailSupport);
			messageError = sendTemplates(context, user, null);
			break;
		default:
			break;
		}
    	return messageError;
    }
    
    public String sendTemplates (FacesContext context, UserBase user, String mailToSupport) throws Exception{
    	if (Long.parseLong(templateId) == Constants.TEMPLATE_POA) {
			if (null == cardId) {
				FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_INFO,CommonUtil.getMessage("templates.card.not.found", null, Utils.getWriterLocale(context)), null);
				context.addMessage(null, fm);

				return null;
			}
			UserRegulationBase userRegulation = new UserRegulationBase();
			userRegulation.setUserId(user.getId());
			UserRegulationManager.getUserRegulationBackend(userRegulation);
			if( user.getSkinId() != Skins.SKIN_ETRADER && null != userRegulation.getApprovedRegulationStep()){
				PDFManager.sendAndCreatePOAPDFReg(user, cardId, templateId, mailToSupport);
			} else {
				PDFManager.sendAndCreatePOAPDF(user, cardId, templateId, mailToSupport);
			}
		} else if (Long.parseLong(templateId) == Constants.TEMPLATE_FIRST_WITHDRAWAL) {
			if (null == cardId) {
				FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_INFO,CommonUtil.getMessage("templates.card.not.found", null, Utils.getWriterLocale(context)), null);
				context.addMessage(null, fm);
				return null;
			}
			String error = PDFManager.sendAndCreateFirstWithdrawalPDF(user, cardId, templateId, mailToSupport, from, to);
			if (null != error) {
				return null;
			}
		} else if (Long.parseLong(templateId) == Constants.TEMPLATE_JUST_CONFIRMATION || Long.parseLong(templateId) == Constants.TEMPLATE_JUST_CONFIRMATION_OTHER) {
			String error = PDFManager.sendAndCreateJustConfirmationPDF(user, templateId, mailToSupport, from, to);
			if (null != error) {
				return null;
			}
		} else if (Long.parseLong(templateId) == Constants.TEMPLATE_CLOSING_OF_ACCOUNT) {
			String error = PDFManager.sendAndCreateClosingAccountPDF(user, templateId, mailToSupport);
			if (null != error) {
				return null;
			}
		} else if (Long.parseLong(templateId) == Constants.TEMPLATE_NON_CC_WITHDRAWAL){
			String error = PDFManager.sendAndCreateNonCcWithdrawalPDF(user, templateId, mailToSupport, from, to);
			if (null != error) {
				return null;
			}
		} else if (Long.parseLong(templateId) == Constants.TEMPLATE_5DEPOSITS) {
			if (null == cardId) {
				FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_INFO,CommonUtil.getMessage("templates.card.not.found", null, Utils.getWriterLocale(context)), null);
				context.addMessage(null, fm);

				return null;
			}
			CreditCard c = TransactionsManagerBase.getCreditCard(new BigDecimal(cardId));
			UsersManager.sendTemplate(Long.valueOf(templateId).longValue(),user, null, c.getCcNumberLast4(), null, 0, mailToSupport);
		} else if (Long.parseLong(templateId) == Template.MAIL_VALIDATION) {
			// generate authrization link and send welcome template
			EmailAuthenticationManagerBase.sendActivationEmail(Utils.getAOHomePageUrlHttps() + "jsp/loginAuth.jsf?",
					Utils.getWriter().getWriter().getId(), user);
			FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_INFO,CommonUtil.getMessage("templates.send.success", null, Utils.getWriterLocale(context)),null);
			context.addMessage(null, fm);

		} else if (Long.parseLong(templateId) == UsersManager.TEMPLATE_PASSWORD_REMINDER_MAIL_ID) {
			// generate resetPass link and send passReminder template
	    	String resKey = UsersManagerBase.generateRandomKey();
	    	String resLink = Utils.getHomePageUrlHttps(user.getSkinId()) + "jsp/resetPassword.jsf?" + Constants.RESET_KEY + "=" + resKey;
	    	ResetPasswordManager.insert(user.getUserName(), resKey);
	    	UsersManagerBase.sendMailTemplateFile(UsersManager.TEMPLATE_PASSWORD_REMINDER_MAIL_ID, Utils.getWriter().getWriter().getId(), user, false, resLink, mailToSupport, null, null, null);
			FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_INFO,CommonUtil.getMessage("templates.send.success", null, Utils.getWriterLocale(context)),null);
			context.addMessage(null, fm);

		} else if (Long.parseLong(templateId) == Constants.TEMPLATE_ACTIVATION_MAIL_LOW_SCORE || Long.parseLong(templateId) == Constants.TEMPLATE_ACTIVATION_MAIL_TRESHOLD) {
			UserRegulationBase userRegulation = new UserRegulationBase();
	        userRegulation.setUserId(user.getId());
	        UserRegulationManager.getUserRegulation(userRegulation);
	        int suspendReason = userRegulation.getSuspendedReasonId();
			if (suspendReason == 0) {
				FacesMessage fm = new FacesMessage(CommonUtil.getMessage("user.is.not.suspended", null),null);
				context.addMessage(null, fm);
				return null;
			}
		
			UsersManagerBase.sendActivationMailForSuspend(user.getId(), false);
		} else if (Long.parseLong(templateId) == Constants.TEMPLATE_AO_ACTIVATION_MAIL_TRESHOLD) {
			UserRegulationBase userRegulation = new UserRegulationBase();
	        userRegulation.setUserId(user.getId());
	        UserRegulationManager.getUserRegulation(userRegulation);
	        if (!userRegulation.isTresholdBlock()) {
	        	FacesMessage fm = new FacesMessage(CommonUtil.getMessage("user.is.not.blocked.due.treshold", null),null);
				context.addMessage(null, fm);
				return null;
	        }
	        boolean result = UsersManagerBase.sendActivationMailForBlockedUsers(user.getId());
	        if (!result) {
	        	FacesMessage fm = new FacesMessage(CommonUtil.getMessage("error.sending.unblock.email", null),null);
				context.addMessage(null, fm);
				return null;
	        } else {
	        	FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_INFO,CommonUtil.getMessage("templates.send.success", null, Utils.getWriterLocale(context)),null);
				context.addMessage(null, fm);
	        }
		} else if (Long.parseLong(templateId) == Constants.TEMPLATE_BLANK){
			
			boolean result = UsersManagerBase.sendBlankEmail(blankTemplateContent, user.getId(), Utils.getWriter().getWriter().getId(), mailToSupport, blankTemplateSubject);
			if (!result) {
				FacesMessage fm = new FacesMessage(CommonUtil.getMessage("error.sending.blank.email", null), null);
				context.addMessage(null, fm);
				return null;
			} else {
				FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_INFO, CommonUtil.getMessage("success.sending.blank.email", null), null);
				context.addMessage(null, fm);				
			}
		} else { // no attachment
			UsersManager.sendTemplate(Long.valueOf(templateId).longValue(), user, mailToSupport, year);
		}
    	return Constants.EMPTY_STRING;
    }

	/**
	 * @return the displaySendMailToSupport
	 */
	public boolean isDisplaySendMailToSupport() {
		return displaySendMailToSupport;
	}

	/**
	 * @param displaySendMailToSupport the displaySendMailToSupport to set
	 */
	public void setDisplaySendMailToSupport(boolean displaySendMailToSupport) {
		this.displaySendMailToSupport = displaySendMailToSupport;
	}

	public ArrayList<SelectItem> getYearsList() {
		return yearsList;
	}

	public void setYearsList(ArrayList<SelectItem> yearsList) {
		this.yearsList = yearsList;
	}

	public long getYear() {
		return year;
	}

	public void setYear(long year) {
		this.year = year;
	}

	public boolean isDisplayYearsList() {
		return displayYearsList;
	}

	public void setDisplayYearsList(boolean displayYearsList) {
		this.displayYearsList = displayYearsList;
	}
	
	public String getTemplateContent() {
		if (templateId != null) {
			Template templateDB = null;
			try {
				templateDB = AdminManager.getTemplateById(Long.valueOf(templateId));			
				if (null != templateDB) {
					FacesContext context=FacesContext.getCurrentInstance();
					UserBase user = (UserBase)context.getApplication().createValueBinding(Constants.BIND_USER).getValue(context);
					// get language code for getting the correct template 
		    		String langCode = LanguagesManagerBase.getCodeById(user.getSkin().getDefaultLanguageId());
		    		if (CommonUtil.isParameterEmptyOrNull(langCode)) {   // take israel to default
		    			langCode = ConstantsBase.ETRADER_LOCALE;
		    		}
		    		org.apache.velocity.Template templateFile = SendTemplateEmail.getEmailTemplate(templateDB.getFileName(), langCode, 
		    				user.getSkinId(), CommonUtil.getProperty("templates.path"),	user.getWriterId(), user.getPlatformId());
					StringWriter writer = new StringWriter();
					templateFile.merge(new VelocityContext(), writer);
					return writer.toString().replace("\"", "'").replace("\r", "").replace("\n", "");
				}
			} catch (Exception e) {
				log.debug("cant get template from id " + templateId, e);
			}
		}
		return null;
	}

	public String getBlankTemplateContent() {
		return blankTemplateContent;
	}

	public void setBlankTemplateContent(String blankTemplateContent) {
		this.blankTemplateContent = blankTemplateContent.replaceAll("\r\n", "\n").replaceAll("\r", "\n").replaceAll("\n", "<br/>");
	}

	public String getBlankTemplateSubject() {
		return blankTemplateSubject;
	}

	public void setBlankTemplateSubject(String blankTemplateSubject) {
		this.blankTemplateSubject = blankTemplateSubject;
	}

	public Date getFrom() {
		return from;
	}

	public void setFrom(Date from) {
		this.from = from;
	}

	public Date getTo() {
		return to;
	}

	public void setTo(Date to) {
		this.to = to;
	}

	public boolean isDisplayDates() {
		return displayDates;
	}

	public void setDisplayDates(boolean displayDates) {
		this.displayDates = displayDates;
	}
}
