package il.co.etrader.backend.mbeans;

import java.io.Serializable;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.Transaction;
import com.anyoption.common.beans.base.Skin;
import com.anyoption.common.util.AESUtil;
import com.anyoption.common.util.AnyOptionCreditCardValidator;

import il.co.etrader.backend.bl_managers.AdminManager;
import il.co.etrader.backend.bl_managers.ApplicationData;
import il.co.etrader.backend.bl_managers.TransactionsManager;
import il.co.etrader.backend.bl_managers.UsersManager;
import il.co.etrader.backend.bl_vos.User;
import il.co.etrader.backend.util.Constants;
import il.co.etrader.backend.util.Utils;
import il.co.etrader.bl_managers.TransactionsManagerBase;
import il.co.etrader.bl_vos.UserBase;
import il.co.etrader.util.CommonUtil;
import il.co.etrader.util.TransactionFormater;

public class NewCardForm extends DepositFormBase implements Serializable {

	private static final long serialVersionUID = 5902416991649596811L;

	private static final Logger logger = Logger.getLogger(NewCardForm.class);

	private String typeId;
	private String ccNum;
	private String ccPass;
	private String expMonth;
	private String expYear;
	private String holderName;
	private String holderIdNum;
	private String deposit;
	private boolean sendReceipt;
	private boolean isCcError;  //for cc validation
	private boolean manualRouting = false;
	private ArrayList ccTypes;
	ArrayList<SelectItem> clearingProviderRoutes;
	private String rootingComment;

	private long providerId = Constants.CLEARING_PROVIDER_NOT_SELECTED;
	private boolean isCheckMinAmount;

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public NewCardForm() throws SQLException {
		super();
		FacesContext context = FacesContext.getCurrentInstance();
		ApplicationData ap = (ApplicationData) context.getApplication().createValueBinding(Constants.BIND_APPLICATION_DATA).getValue(context);
		if (CommonUtil.isHebrewSkin(ap.getSkinId())){
			ccTypes = ap.getCreditCardTypes();
		}
		else{
			ccTypes = ap.getCreditCardTypesAo();
			Object ccAmex = null;
			for (Object obj : ccTypes) {
				SelectItem type = (SelectItem) obj;
				if (Integer.valueOf((String) type.getValue()) == TransactionsManagerBase.CC_TYPE_AMEX) {
					ccAmex = type;
					break;
				}
			}
			ccTypes.remove(ccAmex);
			
			Collections.sort(ccTypes, new Comparator() {
				
				@Override
				public int compare(Object o1, Object o2) {
					return ((SelectItem)o2).getLabel().compareTo(((SelectItem)o1).getLabel());
				}				
			});
			User user = context.getApplication().evaluateExpressionGet(context, Constants.BIND_USER, User.class);
			if (user.getCurrencyId() != Constants.CURRENCY_USD_ID && user.getCurrencyId() != Constants.CURRENCY_EUR_ID && user.getCurrencyId() != Constants.CURRENCY_GBP_ID) {
				// do nothing
			}
			else if (ccTypes.size() > 1) {
				ccTypes.add(ccTypes.size() - 1, ccAmex);
			}
			else {//in case something goes wrong, basically this should be unreachable
				ccTypes.add(ccAmex);
			}
		}

       for(Iterator<SelectItem> i = ccTypes.iterator();i.hasNext();){
			
			SelectItem si = i.next();

			if(si.getLabel().contains("Diners") || (si.getLabel().contains("American Express") && !CommonUtil.isHebrewSkin(ap.getSkinId()))){
	    		i.remove(); 		
	    	}
			
		}
		
		isCheckMinAmount = true;
		sendReceipt = true;
	}

	public String insertCreditCardAndDeposit() throws Exception {
		// cc validation
		User user = Utils.getUser();
		if (!AnyOptionCreditCardValidator.validateCreditCardNumber(ccNum, user.getSkinId())) {
			setCcError(true);
			return null;
		}

		// 	prevent identical requests from being processed
        if (this.saveTokenIsInvalid() ) {
            return this.logInvalidSaveAndReturn(logger, null);
        }
        this.generateSaveToken();
        
        if(manualRouting){
        	String bin = ccNum.substring(0, 6);
        	long binId = TransactionsManager.getBinId(Long.parseLong(bin), Long.parseLong(typeId));
			clearingProviderRoutes = DepositForm.loadNonRestrictedProvidersSI(user, binId,  Long.parseLong(typeId));
			manualRouting = false;
        	return "manuallyChangeProvider";
        }

		
		
		UserBase u = TransactionsManager.getById(user.getId());
		
		updateUserFields(user);
		UsersManager.insertUsersDetailsHistoryOneField(Utils.getWriter().getWriter().getId(), user.getId(), user.getUserName(), String.valueOf(user.getClassId()), UsersManager.USERS_DETAILS_HISTORY_TYPE_CREDIT_CARD_NUMBER, null, AESUtil.encrypt(ccNum));		
		Transaction t ;
		if(providerId != Constants.CLEARING_PROVIDER_NOT_SELECTED) {
			t = TransactionsManager.insertCreditCardAndDepositWithProvider(typeId,ccNum,ccPass,expMonth,expYear,holderName,holderIdNum,deposit,AdminManager.getWriterId(),u,Constants.BACKEND_CONST, true, isCheckMinAmount, providerId, 0L);
		} else {
			t = TransactionsManager.insertCreditCardAndDeposit(typeId,ccNum,ccPass,expMonth,expYear,holderName,holderIdNum,deposit,AdminManager.getWriterId(),u,Constants.BACKEND_CONST, true, isCheckMinAmount, 0L);
		}
		
		reloadIsFirstDeposit();
		
		if (sendReceipt && t != null) {
			String[] params = new String[1];
			params[0] = String.valueOf(t.getCc4digit());
			String receiptNum = t.getReceiptNumTxt();
			if ( user.getSkinId() != Skin.SKIN_ETRADER ) {  // for ao take transaction id
				receiptNum = String.valueOf(t.getId());
			}

			UsersManager.sendReceiptEmail(TransactionFormater.getAmountTxt(t),t.getTimeCreatedTxt(),CommonUtil.getMessage("receipt.footer.creditcard",params, user.getLocale()),receiptNum);
		}
		if (user.isSupportSelected() || user.isRetentionSelected() || user.isRetentionMSelected()) {
			logger.debug("updating user session details.");
			UsersManager.loadUserStripFields(user, true, null);
		}

		if (t != null) {
			return Utils.navAfterUserOperation();
		} else {
			FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_INFO, "Cannot insert deposit", "transaction failed");
			FacesContext.getCurrentInstance().addMessage(null, fm);
			return null;
		}
	}

	public String cancel(){
		return "deposit";
	}
	/**
	 * @return the typeId
	 */
	public String getTypeId() {
		return typeId;
	}

	/**
	 * @param typeId
	 *            the typeId to set
	 */
	public void setTypeId(String typeId) {
		this.typeId = typeId;
	}

	/**
	 * @return the ccNum
	 */
	public String getCcNum() {
		return ccNum;
	}

	/**
	 * @param ccNum
	 *            the ccNum to set
	 */
	public void setCcNum(String ccNum) {
		this.ccNum = ccNum;
	}

	/**
	 * @return the ccPass
	 */
	public String getCcPass() {
		return ccPass;
	}

	/**
	 * @param ccPass
	 *            the ccPass to set
	 */
	public void setCcPass(String ccPass) {
		this.ccPass = ccPass;
	}

	/**
	 * @return the expMonth
	 */
	public String getExpMonth() {
		return expMonth;
	}

	/**
	 * @param expMonth
	 *            the expMonth to set
	 */
	public void setExpMonth(String expMonth) {
		this.expMonth = expMonth;
	}

	/**
	 * @return the expYear
	 */
	public String getExpYear() {
		return expYear;
	}

	/**
	 * @param expYear
	 *            the expYear to set
	 */
	public void setExpYear(String expYear) {
		this.expYear = expYear;
	}

	/**
	 * @return the holderName
	 */
	public String getHolderName() {
		return holderName;
	}

	/**
	 * @param holderName
	 *            the holderName to set
	 */
	public void setHolderName(String holderName) {
		this.holderName = holderName;
	}

	/**
	 * @return the holderIdNum
	 */
	public String getHolderIdNum() {
		return holderIdNum;
	}

	/**
	 * @param holderIdNum
	 *            the holderIdNum to set
	 */
	public void setHolderIdNum(String holderIdNum) {
		this.holderIdNum = holderIdNum;
	}

	/**
	 * @return the deposit
	 */
	public String getDeposit() {
		return deposit;
	}

	/**
	 * @param deposit
	 *            the deposit to set
	 */
	public void setDeposit(String deposit) {
		this.deposit = deposit;
	}

	public boolean isSendReceipt() {
		return sendReceipt;
	}

	public void setSendReceipt(boolean sendReceipt) {
		this.sendReceipt = sendReceipt;
	}

	/**
	 * @return the isCcError
	 */
	public boolean isCcError() {
		return isCcError;
	}

	/**
	 * @param isCcError the isCcError to set
	 */
	public void setCcError(boolean isCcError) {
		this.isCcError = isCcError;
	}


	/**
	 * Constructs a <code>String</code> with all attributes
	 * in name = value format.
	 *
	 * @return a <code>String</code> representation
	 * of this object.
	 */
	public String toString()
	{
	    final String TAB = " \n ";

	    String retValue = "";

	    retValue = "NewCardForm ( "
	        + super.toString() + TAB
	        + "typeId = " + this.typeId + TAB
	        + "ccNum = " + CommonUtil.getNumberXXXX(this.ccNum) + TAB
	        + "ccPass = " + "*****" + TAB
	        + "expMonth = " + this.expMonth + TAB
	        + "expYear = " + this.expYear + TAB
	        + "holderName = " + this.holderName + TAB
	        + "holderIdNum = " + this.holderIdNum + TAB
	        + "deposit = " + this.deposit + TAB
	        + "sendReceipt = " + this.sendReceipt + TAB
	        + " )";

	    return retValue;
	}

	/**
	 * @return the ccTypes
	 */
	public ArrayList getCcTypes() {
		return ccTypes;
	}

	/**
	 * @param ccTypes the ccTypes to set
	 */
	public void setCcTypes(ArrayList ccTypes) {
		this.ccTypes = ccTypes;
	}

	/**
	 * @return the isCheckMinAmount
	 */
	public boolean isCheckMinAmount() {
		return isCheckMinAmount;
	}

	/**
	 * @param isCheckMinAmount the isCheckMinAmount to set
	 */
	public void setCheckMinAmount(boolean isCheckMinAmount) {
		this.isCheckMinAmount = isCheckMinAmount;
	}

	public boolean isManualRouting() {
		return manualRouting;
	}

	public void setManualRouting(boolean manualRouting) {
		this.manualRouting = manualRouting;
	}

	public ArrayList<SelectItem> getClearingProviderRoutes() {
		return clearingProviderRoutes;
	}

	public void setClearingProviderRoutes(
			ArrayList<SelectItem> clearingProviderRoutes) {
		this.clearingProviderRoutes = clearingProviderRoutes;
	}

	public long getProviderId() {
		return providerId;
	}

	public void setProviderId(long providerId) {
		this.providerId = providerId;
	}

	public String getRootingComment() {
		return rootingComment;
	}

	public void setRootingComment(String rootingComment) {
		this.rootingComment = rootingComment;
	}

	
}
