/**
 * 
 */
package il.co.etrader.backend.mbeans;

import java.io.Serializable;
import java.sql.SQLException;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import com.anyoption.backend.service.userdocumentsscreens.UserDocumentsScreenManager;
import com.anyoption.common.beans.Writer;

import il.co.etrader.backend.bl_managers.WritersManager;
import il.co.etrader.backend.util.Constants;
import il.co.etrader.login.LoginInfoBean;
import il.co.etrader.login.LoginInfoThreadLocal;
import il.co.etrader.util.CommonUtil;

/**
 * @author kirilim
 *
 */
public class LoginForm implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8009272442371592404L;
	
	private static final Logger logger = Logger.getLogger(LoginForm.class);
	private String username;
    private String password;
    
    public LoginForm() {}
    
    public String login() {
    	FacesContext context = FacesContext.getCurrentInstance();
    	String errorMessagesLabel = "login:errorlogin";
    	Writer writer = new Writer();
    	try {
			WritersManager.loadWriterByName(username, writer);
		} catch (SQLException e) {
			logger.debug("Can't load writer by name", e);
		}
    	if (writer.getId() == 0) {
    		FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR, CommonUtil.getMessage("header.login.error", null), null);
    		context.addMessage(errorMessagesLabel, fm);
    		return "";
    	}
    	
    	// TODO check if password has expired
    	
    	HttpServletRequest request = (HttpServletRequest) context.getExternalContext().getRequest();
    	HttpServletResponse response = (HttpServletResponse) context.getExternalContext().getResponse();
    	try {
    		request.login(username, password.toUpperCase()); // it turns out that the be pass is upper case
    		CommonUtil.addCookie(Constants.COOKIE_WRITER_NAME, username, request, response, Constants.COOKIE_WRITER_NAME);
    		if(writer.isPasswordReset()){
    			return Constants.NAV_CHANGE_WRITER_PASS;
    		}
    	} catch (ServletException e) { // failed login
    		LoginInfoBean loginInfo = LoginInfoThreadLocal.getLoginInfo();
    		String errorMessageKey;
    		if (loginInfo != null) {
    			errorMessageKey = loginInfo.getErrorMessageKey();
    		} else {
    			errorMessageKey = "header.login.error";
    			logger.warn("The login info from login module is null");
    		}
    		FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR, CommonUtil.getMessage(errorMessageKey, null), null);
    		context.addMessage(errorMessagesLabel, fm);
    		return "";
    	} finally {
    		LoginInfoThreadLocal.clear();
    	}
    	
    	return Constants.NAV_MAIN;
    }
    
    public String logout() {
    	FacesContext context = FacesContext.getCurrentInstance();
    	HttpServletRequest request = (HttpServletRequest) context.getExternalContext().getRequest();
    	HttpServletResponse response = (HttpServletResponse) context.getExternalContext().getResponse();
    	HttpSession session = request.getSession();
    	logger.info("Invalidating session [" + session.getId() + "]");
    	try {    
    		UserDocumentsScreenManager.unlockDocumentStatusBySession(session.getId());
		    request.logout();
		    Cookie cookieWriterName = CommonUtil.getCookie(request, Constants.COOKIE_WRITER_NAME);
		    CommonUtil.deleteCookie(cookieWriterName, response);
		} catch (Exception e) {
		    logger.error("Can't logout.", e);
		}    	
    	session.invalidate();
    	logger.info("Session invalidated successfully");
    	return "";
    }

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
}