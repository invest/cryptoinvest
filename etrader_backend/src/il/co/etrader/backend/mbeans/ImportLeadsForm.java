package il.co.etrader.backend.mbeans;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Serializable;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.StringTokenizer;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import com.anyoption.common.beans.Country;
import com.anyoption.common.beans.File;

import il.co.etrader.backend.bl_managers.ApplicationData;
import il.co.etrader.backend.util.Constants;
import il.co.etrader.backend.util.Utils;
import il.co.etrader.bl_managers.ContactsManager;
import il.co.etrader.bl_managers.PopulationsManagerBase;
import il.co.etrader.bl_vos.Contact;
import il.co.etrader.bl_vos.Skins;
import il.co.etrader.util.CommonUtil;

public class ImportLeadsForm implements Serializable {

	private static final long serialVersionUID = -1947733041383974301L;
	private ArrayList<Contact> contactsList;
	private File file;
	private String generalMessage;
	private String falseInsertMessage;
	ArrayList<String> errorLines;
	public static final long NUMBER_OF_COLUMN_IN_THE_CSV_FILE = 11;

	public ImportLeadsForm() throws SQLException {
		file = new File();
		errorLines = new ArrayList<String>();
	}

	/**
	 * @return
	 * @throws SQLException
	 */
	public String insertContact() throws SQLException {
		generalMessage = "";
		falseInsertMessage = "";
		errorLines.clear();
		FacesContext context = FacesContext.getCurrentInstance();
		FacesMessage fm = null;
		int falseInsertCounter = 0;
		contactsList = new ArrayList<Contact>();
		boolean errorFound = false;
		ArrayList<Contact> contactsList = new ArrayList<Contact>();
		long writerId = Utils.getWriter().getWriter().getId();

		try {

			BufferedReader fr = new BufferedReader(new InputStreamReader(file.getFile().getInputStream()));
			String contacts = new String();
			StringTokenizer st = null;
			Contact contact = null;
			int stringTokenizerCounter = 0;
			int errorLineNum = 1;

			fr.readLine();
			while (null != (contacts = fr.readLine())) {
				contact = new Contact();
				errorLineNum++;
				st = new StringTokenizer(contacts, ",");
				stringTokenizerCounter = st.countTokens();
				if( stringTokenizerCounter == 10 ) {  //if affiliate key is empty
					stringTokenizerCounter+=1;
				}
				if(stringTokenizerCounter == NUMBER_OF_COLUMN_IN_THE_CSV_FILE) {
					while (st.hasMoreTokens()) {
						st.nextToken();//skip gender
						contact.setLastName(st.nextToken());
						contact.setFirstName(st.nextToken());
						st.nextToken();//skip birthday
						st.nextToken();//skip zip code
						st.nextToken();//skip city
						contact.setPhone(st.nextToken());
						contact.setMobilePhone(contact.getPhone());
						contact.setLandLinePhone(contact.getPhone());
						contact.setEmail(st.nextToken());
						contact.setCombId(Long.parseLong(st.nextToken()));
						contact.setType(Contact.CONTACT_ME_TYPE);
						contact.setSkinId(Long.parseLong(st.nextToken()));
						if (st.hasMoreTokens()) {
							contact.setAffiliateKey(Long.parseLong(st.nextToken()));
						}
						contact.setContactByEmail(false);
						contact.setContactBySMS(false);
						contact.setDynamicParameter("Manually uploaded");
						// no need to add aff_sub too
						Skins skin = ApplicationData.getSkinById(contact.getSkinId());
						long countryId = skin.getDefaultCountryId();
						contact.setCountryId(countryId);
						Country country = ApplicationData.getCountry(countryId);
						contact.setUtcOffset(country.getGmtOffset());
						contact.setText(contact.getFirstName() + " " + contact.getLastName());
						contact.setWriterId(writerId);
						String error = null;
						if(!CommonUtil.isEmailValidUnix(contact.getEmail())) {
							error = "Wrong email address";
						} else if(!containsOnlyNumbers(contact.getPhone())) {
							error = "Invalid phone number";
						}
						if (null != error) {
							errorFound = true;
							falseInsertCounter++;
							errorLines.add(String.valueOf(errorLineNum) + " " + error);
							break;
						}
					}
					contactsList.add(contact);
				} else {
					errorFound = true;
					String error = "Empty field";
					falseInsertCounter++;
					errorLines.add(String.valueOf(errorLineNum) + " " + error);
				}
			}
		} catch (IOException e) {
			generalMessage = CommonUtil.getMessage("issues.file.error.problem", null);
			fm = new FacesMessage(FacesMessage.SEVERITY_ERROR, generalMessage, null);
			context.addMessage(null, fm);
			return null;
		}

		if (!errorFound) { //success
			generalMessage = CommonUtil.getMessage("import.leads.file.success.upload", null);
			for(Contact c: contactsList) {
				ContactsManager.insertContactRequest(c, true, false, null, null);
				PopulationsManagerBase.insertIntoPopulation(c.getId(), c.getSkinId(), 0, null, writerId,
						PopulationsManagerBase.POP_TYPE_CALLME, null);
			}
		} else { // failed
			generalMessage = CommonUtil.getMessage("import.leads.file.failed.upload", null);
			String[] params =  new String[1];
			params[0] = String.valueOf(falseInsertCounter);
			falseInsertMessage =  CommonUtil.getMessage("import.leads.error.summary", params);
			for(int i = 0; i < errorLines.size() ; i++) {
				falseInsertMessage += "Line #" + errorLines.get(i) + " <br/>";
			}
		}

		fm = new FacesMessage(FacesMessage.SEVERITY_ERROR, generalMessage, null);
		context.addMessage(null, fm);
		return null;
	}

    /**
     * Check if all digits are
     * @param str
     * @return
     */
    public boolean containsOnlyNumbers(String str) {
        if (str == null || str.length() == 0)
            return false;
        for (int i = 0; i < str.length(); i++) {
            if (!Character.isDigit(str.charAt(i)))
                return false;
        }
        return true;
    }

	public String getFilesPath() {
		return CommonUtil.getProperty(Constants.FILES_SERVER_PATH);
	}

	/**
	 * @return the file
	 */
	public File getFile() {
		return file;
	}

	/**
	 * @param file
	 *            the file to set
	 */
	public void setFile(File file) {
		this.file = file;
	}

	/**
	 * @return the errorLines
	 */
	public ArrayList<String> getErrorLines() {
		return errorLines;
	}

	/**
	 * @param errorLines the errorLines to set
	 */
	public void setErrorLines(ArrayList<String> errorLines) {
		this.errorLines = errorLines;
	}

	/**
	 * @return the falseInsertMessage
	 */
	public String getFalseInsertMessage() {
		return falseInsertMessage;
	}

	/**
	 * @param falseInsertMessage the falseInsertMessage to set
	 */
	public void setFalseInsertMessage(String falseInsertMessage) {
		this.falseInsertMessage = falseInsertMessage;
	}

	/**
	 * @return the contactsList
	 */
	public ArrayList<Contact> getContactsList() {
		return contactsList;
	}

	/**
	 * @param contactsList the contactsList to set
	 */
	public void setContactsList(ArrayList<Contact> contactsList) {
		this.contactsList = contactsList;
	}

	/**
	 * @return the generalMessage
	 */
	public String getGeneralMessage() {
		return generalMessage;
	}

	/**
	 * @param generalMessage the generalMessage to set
	 */
	public void setGeneralMessage(String generalMessage) {
		this.generalMessage = generalMessage;
	}

}
