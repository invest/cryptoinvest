package il.co.etrader.backend.mbeans;

import java.io.Serializable;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;

import org.apache.log4j.Logger;

import il.co.etrader.backend.bl_managers.UsersManager;
import il.co.etrader.backend.bl_vos.TCAccessToken;
import il.co.etrader.backend.bl_vos.User;
import il.co.etrader.backend.util.Constants;
import il.co.etrader.backend.util.Utils;
import il.co.etrader.util.CommonUtil;

/**
 * @author kiril.mutafchiev
 */
public class TCAccessForm implements Serializable {

	private static final long serialVersionUID = -6390952562159123232L;
	private static final Logger log = Logger.getLogger(TCAccessForm.class);
	private Date from;
	private Date to;
	private int site;
	private List<Integer> sites;
	private List<SelectItem> sitesSI;
	private List<TCAccessToken> list;
	private TCAccessToken token;
	private int timeFrame;
	private List<SelectItem> timeFrameSI;

	public TCAccessForm() {
		sitesSI = UsersManager.getTCSitesSI();
		to = new Date();
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.MONTH, -1);
		from = cal.getTime();
		sites = new ArrayList<>();
		sites.add(0);
		setTimeFrameSI(initTimeFrameSI());
	}

	public String search() {
		FacesContext context = FacesContext.getCurrentInstance();
		User user = (User) context.getApplication().evaluateExpressionGet(context, Constants.BIND_USER, User.class);
		if (sites.isEmpty()) {
			sites.add(0);
		}
		list = UsersManager.getTCAccess(user.getId(), from, to, Utils.getUtcOffset(), sites);
		return null;
	}
	
	public String navNew() {	
		return Constants.NAV_ADD_TC;
	}
	
	public String addNew() {
		FacesContext context = FacesContext.getCurrentInstance();
		User user = (User) context.getApplication().evaluateExpressionGet(context, Constants.BIND_USER, User.class);
		Date start = new Date();
		Calendar cal = Calendar.getInstance();
		switch (timeFrame) {
		case 1:
			cal.add(Calendar.WEEK_OF_YEAR, 2);
			break;

		case 2:
			cal.add(Calendar.MONTH, 1);
			break;

		case 3:
			cal.add(Calendar.MONTH, 2);
			break;

		default:
			cal.set(Calendar.DAY_OF_MONTH, 22);
			cal.set(Calendar.MONTH, 1);
			cal.set(Calendar.YEAR, 2222);
			break;
		}
		Date end = cal.getTime();
		try {
			boolean result = UsersManager.addTCAccessToken(this.site, start, end, user.getId(), user.getWriterId(), Utils.getUtcOffset());
			String msg;
			if (result) {
				msg = "tc.access.add.success";
			} else {
				msg = "tc.access.add.fail";
			}
			context.addMessage(	null,
								new FacesMessage(	FacesMessage.SEVERITY_ERROR,
													CommonUtil.getMessage(msg, null, Utils.getWriterLocale(context)),
													null));
		} catch (SQLException e) {
			log.debug("Unable to add tc access token", e);
			context.addMessage(	null,
								new FacesMessage(	FacesMessage.SEVERITY_ERROR,
													CommonUtil.getMessage("tc.access.add.fail", null, Utils.getWriterLocale(context)),
													null));
		}

		search();
		return Constants.NAV_TC;
	}

	public String cancel() {
		UsersManager.cancelTCAccessToken(token.getId());
		search();
		FacesContext context = FacesContext.getCurrentInstance();
		context.addMessage(	null,
							new FacesMessage(	FacesMessage.SEVERITY_ERROR,
												CommonUtil.getMessage("tc.access.cancel.success", null, Utils.getWriterLocale(context)),
												null));
		return Constants.NAV_TC;
	}

	public boolean isExpired(TCAccessToken token) {
		if (token.getEndDate() == null) {
			return true;
		}
		return token.getEndDate().getTime() < new Date().getTime();
	}

	private List<SelectItem> initTimeFrameSI() {
		List<SelectItem> si = new ArrayList<>();
		si.add(new SelectItem(1, CommonUtil.getMessage("tc.access.time.frame.2w", null)));
		si.add(new SelectItem(2, CommonUtil.getMessage("tc.access.time.frame.1m", null)));
		si.add(new SelectItem(3, CommonUtil.getMessage("tc.access.time.frame.2m", null)));
		si.add(new SelectItem(4, CommonUtil.getMessage("tc.access.time.frame.u", null)));
		return si;
	}

	public List<SelectItem> getNonAllsitesSI() {
		return sitesSI.subList(2, sitesSI.size());
	}
	
	public Date getFrom() {
		return from;
	}
	
	public void setFrom(Date from) {
		this.from = from;
	}
	
	public Date getTo() {
		return to;
	}
	
	public void setTo(Date to) {
		this.to = to;
	}
	
	public List<SelectItem> getSitesSI() {
		return sitesSI;
	}
	
	public void setSitesSI(List<SelectItem> sitesSI) {
		this.sitesSI = sitesSI;
	}

	public List<TCAccessToken> getList() {
		return list;
	}

	public void setList(List<TCAccessToken> list) {
		this.list = list;
	}

	public TCAccessToken getToken() {
		return token;
	}

	public void setToken(TCAccessToken token) {
		this.token = token;
	}

	public int getSite() {
		return site;
	}

	public void setSite(int site) {
		this.site = site;
	}

	public List<Integer> getSites() {
		return sites;
	}

	public void setSites(List<Integer> sites) {
		this.sites = sites;
	}

	public List<SelectItem> getTimeFrameSI() {
		return timeFrameSI;
	}

	public void setTimeFrameSI(List<SelectItem> timeFrameSI) {
		this.timeFrameSI = timeFrameSI;
	}

	public int getTimeFrame() {
		return timeFrame;
	}

	public void setTimeFrame(int timeFrame) {
		this.timeFrame = timeFrame;
	}
}