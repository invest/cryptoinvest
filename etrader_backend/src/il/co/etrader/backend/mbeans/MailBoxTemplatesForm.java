package il.co.etrader.backend.mbeans;

import java.io.IOException;
import java.io.Serializable;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;

import javax.el.ValueExpression;
import javax.faces.context.FacesContext;

import org.apache.myfaces.custom.fileupload.UploadedFile;

import com.anyoption.common.beans.MailBoxTemplate;

import il.co.etrader.backend.bl_managers.MailBoxTemplatesManager;
import il.co.etrader.backend.util.Constants;
import il.co.etrader.backend.util.Utils;
import il.co.etrader.util.CommonUtil;


public class MailBoxTemplatesForm implements Serializable {
	private static final long serialVersionUID = 5673061197402336381L;

	private ArrayList<MailBoxTemplate> list;
	public MailBoxTemplate template;

	private int skinId;
	private long typeId;
	private long senderId;
	private UploadedFile file;

	public MailBoxTemplatesForm() throws SQLException{
		search();
	}

	public int getListSize() {
		return list.size();
	}

	public String search() throws SQLException{
		list = MailBoxTemplatesManager.getAllTempaltes(skinId, typeId, senderId);
		CommonUtil.setTablesToFirstPage();
		return null;
	}

	public ArrayList<MailBoxTemplate> getList() {
		return list;
	}

	/**
	 * Navigate to edit bonus
	 * @return
	 * @throws SQLException
	 */
	public String navEdit() {
		FacesContext context = FacesContext.getCurrentInstance();
		ValueExpression ve = context.getApplication().getExpressionFactory().createValueExpression(context.getELContext(), "#{mailBoxTemplate}", MailBoxTemplate.class);
		ve.getValue(context.getELContext());
		ve.setValue(context.getELContext(), template);
		template.setDisplay(true);
		return Constants.NAV_MAILBOX_TEMPLATE;
	}

	/**
	 * Navigate to insert new bonus
	 * @return
	 * @throws SQLException
	 */
	public String navNew() throws SQLException {
		template = new MailBoxTemplate();
		template.setWriterId((int)Utils.getWriter().getWriter().getId());
		template.setTimeCreated(new Date());
		return Constants.NAV_MAILBOX_TEMPLATE;
	}

	/**
	 * Update / insert template
	 * @return
	 * @throws SQLException
	 */
	public String updateInsert() throws SQLException{
		template.setWriterId((int)Utils.getWriter().getWriter().getId());
		MailBoxTemplatesManager.insertUpdateTemplate(template);
        search();
		return Constants.NAV_MAILBOX_TEMPLATES;
	}

	public MailBoxTemplate getTemplate() {
		return template;
	}

	public void setTemplate(MailBoxTemplate template) {
		this.template = template;
	}

	/**
	 * @return the skinId
	 */
	public int getSkinId() {
		return skinId;
	}

	/**
	 * @param skinId the skinId to set
	 */
	public void setSkinId(int skinId) {
		this.skinId = skinId;
	}

	/**
	 * @return the senderId
	 */
	public long getSenderId() {
		return senderId;
	}

	/**
	 * @param senderId the senderId to set
	 */
	public void setSenderId(long senderId) {
		this.senderId = senderId;
	}

	/**
	 * @return the typeId
	 */
	public long getTypeId() {
		return typeId;
	}

	/**
	 * @param typeId the typeId to set
	 */
	public void setTypeId(long typeId) {
		this.typeId = typeId;
	}
	

	public UploadedFile getFile() {
		return file;
	}

	public void setFile(UploadedFile file) throws IOException {
		this.file = file;
		if (null != file) {
			template.setTemplate(CommonUtil.convertStreamToString(this.file.getInputStream()));
		}
	}
	
	public String displayTemplate() {
		FacesContext context = FacesContext.getCurrentInstance();
        ValueExpression ve = context.getApplication().getExpressionFactory().createValueExpression(context.getELContext(), "#{mailBoxTemplate}", MailBoxTemplate.class);
        ve.getValue(context.getELContext());
        ve.setValue(context.getELContext(), template);
        template.setDisplay(true);
		return null;
	}
}
