package il.co.etrader.backend.mbeans;

import java.io.IOException;
import java.io.Serializable;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.base.UsersDetailsHistory;

import il.co.etrader.backend.bl_managers.UsersManager;
import il.co.etrader.backend.bl_vos.WriterWrapper;
import il.co.etrader.backend.helper.ChangedUserDetailsReportSender;
import il.co.etrader.backend.util.Constants;
import il.co.etrader.backend.util.Utils;
import il.co.etrader.util.CommonUtil;
import il.co.etrader.util.ConstantsBase;

/**
 * @author EyalO
 */
public class ChangedUserDetailsForm implements Serializable{
    
    private static final long serialVersionUID = 1L;
    private static final Logger logger = Logger.getLogger(ChangedUserDetailsForm.class);
    private Date from;
    private Date to;
    private String userName;
    private String email;
    private Long contactId;
    private List<String> writersId;
    private List<String> typesId;
    private ArrayList<UsersDetailsHistory> list;
    private boolean isExludeTesters;
    private Long userId;
    
    /**
     * Changed User Details Form
     */
    public ChangedUserDetailsForm() {
        initFormFilters();
    }
    
    /**
     * initial parameters form filters
     */
    private void initFormFilters() {
        writersId = new ArrayList<String>();
        writersId.add(String.valueOf(Constants.ALL_FILTER_ID));
        typesId = new ArrayList<String>();
        typesId.add(String.valueOf(Constants.ALL_FILTER_ID));
        isExludeTesters = true;
        GregorianCalendar gc = new GregorianCalendar();
        to = gc.getTime();
        userId = null;
        contactId = null;
        userName = null;
        email = null;
    }
    
    /**
     * update list
     * @return String
     * @throws SQLException
     */
    public String updateList() throws SQLException {
    	FacesContext context = FacesContext.getCurrentInstance();
    	if (from.after(to)) {
    	    list = null;
    		FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR,CommonUtil.getMessage("general.illegaldate",null), null);
            context.addMessage(null, fm);
            return null;
    	}
        // User name and User Id number are after trim so "only spaces" case will be ignored.
        if (null != userName && userName.equals(ConstantsBase.EMPTY_STRING)){
            userName = null;
        }
        long userIdValue;
        long contactIdValue;
        if (null != userId){
            userIdValue = userId.longValue();
        }else {
            userIdValue = 0;
        }

        if (null != contactId){
            contactIdValue = contactId.longValue();
        }else {
            contactIdValue = -1;
        }
        
        list = UsersManager.getUsersDetailsHistory(from, to, userName, email, contactIdValue, getArrayToString(writersId), getArrayToString(typesId), isExludeTesters, userIdValue);
        return null;
    }
    
    /**
     * export review result to mail
     * @return String
     * @throws IOException
     * @throws SQLException
     * @throws ParseException
     */
    public String exportReviewResultToMail() throws IOException, SQLException, ParseException {
        String reportName = "changed_user_details_report_";
        FacesContext context = FacesContext.getCurrentInstance();
        WriterWrapper wr = (WriterWrapper) context.getApplication().createValueBinding(Constants.BIND_WRITER_WRAPPER).getValue(context);
        logger.info("Process " + reportName);
        SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd");
        String filename = reportName + fmt.format(from) + "_" + fmt.format(to) + ".csv";
        // check email address
        if (CommonUtil.isParameterEmptyOrNull(Utils.getWriter().getWriter().getEmail())) {
            FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR,CommonUtil.getMessage("error.enteries.report.invalid.mail",null), null);
            context.addMessage(null, fm);
            return null;
        }
        if (null == list || list.isEmpty()) {
            FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_INFO, "No data was Found.",null);
            context.addMessage(null, fm);
            return null;
        }
        ChangedUserDetailsReportSender sender = new ChangedUserDetailsReportSender(this, filename, wr);
        sender.start();
        FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_INFO, filename + " " + CommonUtil.getMessage("enteries.report.processing.mail", null),null);
        context.addMessage(null, fm);
        return null;
    }
    
    /**
     * return 0 if all selected else return string of array
     * @param temp
     * @return String
     */
    public String getArrayToString(List<String> temp) {
        String list = ConstantsBase.EMPTY_STRING;
        for (int i = 0; i < temp.size(); i++) {
            if(temp.get(i).equals(String.valueOf(Constants.ALL_FILTER_ID))){
                return String.valueOf(Constants.ALL_FILTER_ID);
            }
            list += "'" + temp.get(i) + "'" + ",";
        }
        return list.substring(0,list.length()-1);
    }

    /**
     * @return the from
     */
    public Date getFrom() {
        return from;
    }

    /**
     * @param from the from to set
     */
    public void setFrom(Date from) {
        this.from = from;
    }

    /**
     * @return the to
     */
    public Date getTo() {
        return to;
    }

    /**
     * @param to the to to set
     */
    public void setTo(Date to) {
        this.to = to;
    }

    /**
     * @return the userName
     */
    public String getUserName() {
        return userName;
    }

    /**
     * @param userName the userName to set
     */
    public void setUserName(String userName) {
        this.userName = userName;
    }

    /**
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * @param email the email to set
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * @return the contactId
     */
    public Long getContactId() {
        return contactId;
    }

    /**
     * @param contactId the contactId to set
     */
    public void setContactId(Long contactId) {
        this.contactId = contactId;
    }

    /**
     * @return the writersId
     */
    public List<String> getWritersId() {
        return writersId;
    }

    /**
     * @param writersId the writersId to set
     */
    public void setWritersId(List<String> writersId) {
        this.writersId = writersId;
    }

    /**
     * @return the typesId
     */
    public List<String> getTypesId() {
        return typesId;
    }

    /**
     * @param typesId the typesId to set
     */
    public void setTypesId(List<String> typesId) {
        this.typesId = typesId;
    }

    /**
     * @return the list
     */
    public ArrayList<UsersDetailsHistory> getList() {
        return list;
    }

    /**
     * @param list the list to set
     */
    public void setList(ArrayList<UsersDetailsHistory> list) {
        this.list = list;
    }
    
    /**
     * get list size.
     * @return size of the list
     */
    public int getListSize() {
        if (null == list || list.isEmpty()) {
            return 0;
        }
        return list.size();
    }

    /**
     * @return the isExludeTesters
     */
    public boolean isExludeTesters() {
        return isExludeTesters;
    }

    /**
     * @param isExludeTesters the isExludeTesters to set
     */
    public void setExludeTesters(boolean isExludeTesters) {
        this.isExludeTesters = isExludeTesters;
    }

    /**
     * @return the userId
     */
    public Long getUserId() {
        return userId;
    }

    /**
     * @param userId the userId to set
     */
    public void setUserId(Long userId) {
        this.userId = userId;
    }
    
}
