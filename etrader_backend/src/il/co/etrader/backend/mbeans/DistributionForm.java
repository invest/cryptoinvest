package il.co.etrader.backend.mbeans;

import java.io.Serializable;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Hashtable;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import org.apache.log4j.Logger;

import il.co.etrader.backend.util.Utils;
import il.co.etrader.bl_managers.DistributionManager;
import il.co.etrader.bl_vos.ProviderDistribution;
import il.co.etrader.distribution.DistributionHistory;
import il.co.etrader.distribution.DistributionType;
import il.co.etrader.util.CommonUtil;

public class DistributionForm implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 7049489498147884278L;

	private static final Logger logger = Logger.getLogger(DistributionForm.class);
	
	ArrayList<ProviderDistribution> distributionList;
	ArrayList<ProviderDistribution> distributionListCopy;

	String date;
	String writer;
	
	String errorMessage = "Your selected distribution doesn’t add up to 100."; 
	String successMessage = "Your selected distribution has been set successfully.";
	
	public DistributionForm() {
		init();
	}
	
	void init() {
		try {
			distributionList = DistributionManager.getProvidersListForDistribution(DistributionType.CUP);
			distributionListCopy = new ArrayList<ProviderDistribution>(distributionList.size());
			DistributionHistory distributionHistory = DistributionManager.getDistributionHistory(DistributionType.CUP);
			date = distributionHistory.getDateCreated();
			writer = distributionHistory.getWriter();
			for(ProviderDistribution pd : distributionList) {
				distributionListCopy.add(pd.clone());
			}
		} catch (SQLException e) {
			logger.debug("Can't load Distribution list:"+e.getMessage());
		}
	}
	
	public void save() {
		StringBuilder providerCsv = new StringBuilder();
		StringBuilder percentageCsv = new StringBuilder();
		// check sum of 100 %
		int totalPercentage = 0;
		ArrayList<Long> zeroPercentageProviders = new ArrayList<Long>();
		for(ProviderDistribution provider: distributionList) {
			totalPercentage += provider.getClearingProviderPercentage();
			if (providerCsv.length() > 0) {
				providerCsv.append(",");
			}
			if (percentageCsv.length() > 0) {
				percentageCsv.append(",");
			}
			providerCsv.append(provider.getClearingProviderId());
			percentageCsv.append(provider.getClearingProviderPercentage());
			// keep a list with providers set to zero
			if(provider.getClearingProviderPercentage()==0){
				zeroPercentageProviders.add(provider.getClearingProviderId());
			}
		}
		
		// if percentage does not add up to 100 return error msg
		if(totalPercentage != 100) {
	       	FacesMessage fm=new FacesMessage(errorMessage);
      	    FacesContext.getCurrentInstance().addMessage("distributionForm:saveError", fm);
	    	return;
		} 

		long writerId = Utils.getWriter().getWriter().getId();
		
		try {
			// clean up the distribution for providers that were set to zero
			for(int i=0;i<zeroPercentageProviders.size();i++) {
				Long id = zeroPercentageProviders.get(i);
				DistributionManager.cleanUpDistribution(id, DistributionType.CUP);
			}
			// save to database the newly set percentage
			for(ProviderDistribution provider: distributionList) {
				DistributionManager.updateClearingProvider(provider.getClearingProviderId(), provider.getClearingProviderPercentage());
			}
			// update distribution history
			if(writerId < 0) {
				writerId = 0;
			}
			DistributionManager.updateDistributionHistory(DistributionType.CUP, writerId, providerCsv.toString(), percentageCsv.toString() );
			// send email to anna
			sendEmail();
			// reset values
			init();
			// inform user success
	       	FacesMessage fm = new FacesMessage(successMessage);
	  	    FacesContext.getCurrentInstance().addMessage("distributionForm:saveSuccess", fm);
		} catch (SQLException e) {
			logger.debug(writerId+" Error updating Distribution History: "+e.getMessage());
			FacesMessage fm = new FacesMessage(e.getMessage());
      	    FacesContext.getCurrentInstance().addMessage("distributionForm:saveError", fm);
		}
	}
	
	private void sendEmail() {
	    // send email
		Hashtable<String, String> serverProperties = new Hashtable<String, String>();
		serverProperties.put("url", CommonUtil.getProperty("email.server"));
		serverProperties.put("auth", "true");
		serverProperties.put("user", CommonUtil.getProperty("email.uname"));
		serverProperties.put("pass", CommonUtil.getProperty("email.pass"));
		serverProperties.put("contenttype", "text/html; charset=UTF-8");
		
		Hashtable<String, String> email = new Hashtable<String, String>();
		email.put("subject", "The CUP providers distribution has been changed");
		FacesContext ctx = FacesContext.getCurrentInstance();
		 
		String annaemail = ctx.getExternalContext().getInitParameter("anna.email");
		email.put("to", annaemail);
		 
		StringBuilder html = new StringBuilder();
		html.append("<html><div>Old distribution:</div><ul>");
		for(ProviderDistribution old:distributionListCopy){
			html.append("<li>"+old.getClearingProviderName()+":"+old.getClearingProviderPercentage()+" </li>");
		}
		html.append("</ul><div>New distribution:</div><ul>");
		for(ProviderDistribution newP:distributionList){
			html.append("<li>"+newP.getClearingProviderName()+":"+newP.getClearingProviderPercentage()+" </li>");
		}
		html.append("</ul><p>Changed by:"+Utils.getWriter().getWriter().getUserName()+"</p></html>");
		
		String senderEmailAddress = "";
		email.put("from", senderEmailAddress);
		email.put("body", html.toString());

		CommonUtil.sendEmail(serverProperties, email, null);
		
	}
	
	public ArrayList<ProviderDistribution> getDistributionList() {
		return distributionList;
	}
	public void setDistributionList(ArrayList<ProviderDistribution> distributionList) {
		this.distributionList = distributionList;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public String getWriter() {
		return writer;
	}
	public void setWriter(String writer) {
		this.writer = writer;
	}
}
