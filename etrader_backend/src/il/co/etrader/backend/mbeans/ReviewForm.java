package il.co.etrader.backend.mbeans;

import java.io.IOException;
import java.io.Serializable;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import org.apache.log4j.Logger;

import il.co.etrader.backend.bl_managers.ApplicationData;
import il.co.etrader.backend.bl_managers.PopulationEntriesManager;
import il.co.etrader.backend.bl_vos.PopulationEntry;
import il.co.etrader.backend.bl_vos.User;
import il.co.etrader.backend.bl_vos.WriterWrapper;
import il.co.etrader.backend.helper.ReviewReportSender;
import il.co.etrader.backend.util.Constants;
import il.co.etrader.backend.util.Utils;
import il.co.etrader.bl_vos.Writer;
import il.co.etrader.population.PopulationHandlersException;
import il.co.etrader.util.CommonUtil;
import il.co.etrader.util.ConstantsBase;

public class ReviewForm implements Serializable {

	private static final long serialVersionUID = 3356403672833604508L;

	private ArrayList list;
	private Date from = null;
	private Date to = null;
	private Date fromLogin = null;
	private Date toLogin = null;
	private long skinId;
	private String userName;
	private PopulationEntry populationEntry;
	private boolean assignFilter;
	private Long userId;
	private String username;
	private Long contactId;
	private int actionTypeId;
	private long salesType;
	private long campaignId;
	private long retentionTeamId;
	private static final Logger logger = Logger
		.getLogger(ReviewForm.class);


	public ReviewForm() throws SQLException{
		skinId = 0 ;
		username = null ;
		userId = null ;
		assignFilter = false;
		actionTypeId = 0;
		GregorianCalendar gc = new GregorianCalendar();
		to = gc.getTime();
		gc.set(GregorianCalendar.YEAR, 2006);
		gc.set(GregorianCalendar.MONTH, GregorianCalendar.JANUARY);
		gc.set(GregorianCalendar.DAY_OF_MONTH, 1);
		gc.set(GregorianCalendar.HOUR_OF_DAY, 0);
		gc.set(GregorianCalendar.MINUTE, 0);
		gc.set(GregorianCalendar.SECOND, 0);
		from = gc.getTime();
		salesType = 1;
		campaignId = ConstantsBase.ALL_FILTER_ID;
		retentionTeamId = ConstantsBase.ALL_FILTER_ID;
		updateList();

	}

	public String updateList() throws SQLException{
		FacesContext context = FacesContext.getCurrentInstance();
		WriterWrapper wr = (WriterWrapper) context.getApplication().createValueBinding(Constants.BIND_WRITER_WRAPPER).getValue(context);

		long userIdVal = 0;
		if (null != userId){
			userIdVal = userId.longValue();
		}
		if (username != null && username.equals(ConstantsBase.EMPTY_STRING)){
			username = null;
		}

		long contactIdVal = 0;
		if (null != contactId) {
			contactIdVal = contactId.longValue();
		}

		Writer w = Utils.getWriter().getWriter();
		if (w.getSales_type() == Constants.SALES_TYPE_CONVERSION) {
			salesType = Constants.POPULATION_TYPE_ID_CONVERSION;
		} else if (w.getSales_type() == Constants.SALES_TYPE_RETENTION) {
			salesType = Constants.POPULATION_TYPE_ID_RETENTION;
		}

		list = PopulationEntriesManager.getPopulationEntriesByEntryType(null,skinId, new Long(0), ConstantsBase.POP_ENTRY_TYPE_REVIEW,
				from, to, fromLogin, toLogin, actionTypeId, assignFilter, userIdVal, username, contactIdVal, null, wr, 0, campaignId, null, 0, "0", false, false, 0, salesType, 0, retentionTeamId, null, null);

		CommonUtil.setTablesToFirstPage();
		return null;
	}

	public String lockEntry() throws SQLException,PopulationHandlersException {
		boolean res = PopulationEntriesManager.lockEntry(populationEntry, false);
		if (res ){
			CommonUtil.setTablesToFirstPage();
			return PopulationEntriesManager.loadEntry(populationEntry);
		}
		return null;

	}

	public String unLockEntry() throws SQLException,PopulationHandlersException {
		PopulationEntriesManager.unLockEntry(populationEntry);
		updateList();
		return null;
	}

	/**
	 * Load entry to session and navigate to entrySrip page
	 * @return
	 * @throws SQLException
	 */
	public String loadEntry() throws SQLException {
		return PopulationEntriesManager.loadEntry(populationEntry);
	}

	public String exportReviewResultToMail() throws IOException,
			SQLException, ParseException {
		// return exportHtmlTableToExcel("enteries_", true);
		String reportName = "enteries_report_";
		FacesContext context = FacesContext.getCurrentInstance();
		WriterWrapper wr = (WriterWrapper) context.getApplication()
				.createValueBinding(Constants.BIND_WRITER_WRAPPER).getValue(
						context);
		User user = (User) context.getApplication().createValueBinding(
				Constants.BIND_USER).getValue(context);
		logger.info("Process " + reportName);

		SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd");
		String filename = reportName + fmt.format(from) + "_" + fmt.format(to)
				+ ".csv";

		long currentWriterId = Utils.getWriter().getWriter().getId();

		// set defualt date for arabic partner
		if (currentWriterId == Constants.PARTNER_ARABIC_ID) {
			String enumDate = CommonUtil.getEnum(
					Constants.ENUM_PARTNER_ARABIC_DATE_ENUMERATOR,
					Constants.ENUM_PARTNER_ARABIC_DATE_CODE);
			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
			Date startDateAr = sdf.parse(enumDate);
			;
			if (from.before(startDateAr)) {
				from = startDateAr;
			}
		}

		// check email address
		if (CommonUtil.isParameterEmptyOrNull(Utils.getWriter().getWriter()
				.getEmail())) {
			FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR,
					CommonUtil.getMessage("error.enteries.report.invalid.mail",
							null), null);
			context.addMessage(null, fm);
			return null;
		}

		ApplicationData ap = (ApplicationData)context.getApplication().createValueBinding(Constants.BIND_APPLICATION_DATA).getValue(context);
		ReviewReportSender sender = new ReviewReportSender(this, filename,
				wr, user.isRetentionSelected(), ap);
		sender.start();

		FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_INFO, filename
				+ " "
				+ CommonUtil
						.getMessage("enteries.report.processing.mail", null),
				null);
		context.addMessage(null, fm);

		return null;
	}

	public int getListSize() {
		return list.size();
	}

	public ArrayList getList() {
		return list;
	}

	public void setList(ArrayList list) {
		this.list = list;
	}

	/**
	 * @return the from
	 */
	public Date getFrom() {
		return from;
	}

	/**
	 * @param from the from to set
	 */
	public void setFrom(Date from) {
		this.from = from;
	}

	/**
	 * @return the to
	 */
	public Date getTo() {
		return to;
	}

	/**
	 * @param to the to to set
	 */
	public void setTo(Date to) {
		this.to = to;
	}

	/**
	 * @return the skinId
	 */
	public long getSkinId() {
		return skinId;
	}

	/**
	 * @param skinId the skinId to set
	 */
	public void setSkinId(long skinId) {
		this.skinId = skinId;
	}

	/**
	 * @return the userName
	 */
	public String getUserName() {
		return userName;
	}

	/**
	 * @param userName the userName to set
	 */
	public void setUserName(String userName) {
		this.userName = userName;
	}

	/**
	 * @return the populationEntry
	 */
	public PopulationEntry getPopulationEntry() {
		return populationEntry;
	}

	/**
	 * @param populationEntry the populationEntry to set
	 */
	public void setPopulationEntry(PopulationEntry populationEntry) {
		this.populationEntry = populationEntry;
	}

	/**
	 * @return the assignFilter
	 */
	public boolean isAssignFilter() {
		return assignFilter;
	}

	/**
	 * @param assignFilter the assignFilter to set
	 */
	public void setAssignFilter(boolean assignFilter) {
		this.assignFilter = assignFilter;
	}

	/**
	 * @return the contactId
	 */
	public Long getContactId() {
		return contactId;
	}

	/**
	 * @param contactId the contactId to set
	 */
	public void setContactId(Long contactId) {
		this.contactId = contactId;
	}

	/**
	 * @return the username
	 */
	public String getUsername() {
		return username;
	}

	/**
	 * @param username the username to set
	 */
	public void setUsername(String username) {
		this.username = username;
	}

	/**
	 * @param userId the userId to set
	 */
	public void setUserId(Long userId) {
		this.userId = userId;
	}

	/**
	 * @return the userId
	 */
	public Long getUserId() {
		return userId;
	}

	/**
	 * @return the actionTypeId
	 */
	public int getActionTypeId() {
		return actionTypeId;
	}

	/**
	 * @param actionTypeId the actionTypeId to set
	 */
	public void setActionTypeId(int actionTypeId) {
		this.actionTypeId = actionTypeId;
	}

	/**
	 * @return the salesType
	 */
	public long getSalesType() {
		return salesType;
	}

	/**
	 * @param salesType the salesType to set
	 */
	public void setSalesType(long salesType) {
		this.salesType = salesType;
	}

	public Date getFromLogin() {
		return fromLogin;
	}

	public void setFromLogin(Date fromLogin) {
		this.fromLogin = fromLogin;
	}

	public Date getToLogin() {
		return toLogin;
	}

	public void setToLogin(Date toLogin) {
		this.toLogin = toLogin;
	}

	/**
	 * @return the campaignId
	 */
	public long getCampaignId() {
		return campaignId;
	}

	/**
	 * @param campaignId the campaignId to set
	 */
	public void setCampaignId(long campaignId) {
		this.campaignId = campaignId;
	}

	/**
	 * @return the retentionTeamId
	 */
	public long getRetentionTeamId() {
		return retentionTeamId;
	}

	/**
	 * @param retentionTeamId the retentionTeamId to set
	 */
	public void setRetentionTeamId(long retentionTeamId) {
		this.retentionTeamId = retentionTeamId;
	}

}
