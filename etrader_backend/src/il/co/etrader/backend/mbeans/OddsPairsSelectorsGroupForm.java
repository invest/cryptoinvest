package il.co.etrader.backend.mbeans;

import java.io.Serializable;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import il.co.etrader.backend.bl_managers.InvestmentsManager;
import il.co.etrader.backend.bl_vos.OpportunityOddsGroup;
import il.co.etrader.backend.bl_vos.OpportunityOddsPairTemplate;
import il.co.etrader.backend.util.Constants;
import il.co.etrader.backend.util.Utils;
import il.co.etrader.util.CommonUtil;

public class OddsPairsSelectorsGroupForm implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 5930459913030057243L;
	private ArrayList<OpportunityOddsGroup> list = new ArrayList<OpportunityOddsGroup>();
	private OpportunityOddsGroup oddsPairGroup;
	private ArrayList<OpportunityOddsPairTemplate> oddsGrouplist = new ArrayList<OpportunityOddsPairTemplate>();
	private long oddsGroupId;
	private long oddsGroupDefaultValue;

	public OddsPairsSelectorsGroupForm() throws SQLException {
		Search();
	}

	public String Search() throws SQLException {
		list = InvestmentsManager.getOpportunityOddsGroup();
		CommonUtil.setTablesToFirstPage();
		return null;
	}

	public String initValues() throws SQLException {
		oddsPairGroup = new OpportunityOddsGroup();
		oddsPairGroup.setOddsGroupID(0);
		oddsPairGroup.setOddsGroup("0 ");
		oddsPairGroup.setOddsGroupValue("  ");
		initValueOddsGroup();
		return Constants.NAV_ODDS_GROUP_PAIRS;
	}

	public String updateInsert() throws SQLException {	
		
		if (getOddsGroupSelected(oddsGrouplist).isEmpty()) {
			FacesContext context = FacesContext.getCurrentInstance();
			FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR, CommonUtil.getMessage( "error.sadmin.selectors.group.selected", null), null);
			context.addMessage(null, fm);
			return null;
		}
		
		FacesContext context = FacesContext.getCurrentInstance();
		long writerId = Utils.getWriter().getWriter().getId();
		
		boolean res = InvestmentsManager.updateInsertOpportunityOddsGroup(oddsPairGroup, getOddsGroupSelected(oddsGrouplist), writerId, getOddsGroupDefaultValue());

		Search();
		return Constants.NAV_ODDS_GROUPS_PAIRS;
	}

	public String edit() throws SQLException {
		initValueOddsGroup();
		return Constants.NAV_ODDS_GROUP_PAIRS;
	}

	public String initValueOddsGroup() throws SQLException {
		oddsGrouplist.clear();		
		oddsGrouplist = InvestmentsManager.getOpportunityOddsPairTemplate(getOddsPairGroup().getOddsGroup());
		setOddsGroupDefaultValue(InvestmentsManager.getDefValueByOddsGroup(getOddsPairGroup().getOddsGroup()));

		return null;
	};

	private String getOddsGroupSelected( ArrayList<OpportunityOddsPairTemplate> oddsGrouplistArr) {
		String odds = "";

		for (OpportunityOddsPairTemplate opt : oddsGrouplistArr) {

			if (opt.getIsSelected()) {
				odds = opt.getSelectorID() + " " + odds;
			}
		}

		return odds;
	}
	
	public void updateOddsGroupDefaultValue() throws SQLException {
		if (getOddsGroupId() != 0 && getOddsGroupDefaultValue() != 0) {
			long writerId = Utils.getWriter().getWriter().getId();
			InvestmentsManager.updatetOpportunityOddsGroupDefaultValue(getOddsGroupId(), getOddsGroupDefaultValue(), writerId);
			Search();
		}
	}

	public ArrayList<OpportunityOddsGroup> getList() {
		return list;
	}

	public void setList(ArrayList<OpportunityOddsGroup> list) {
		this.list = list;
	}

	public int getListSize() {
		return list.size();
	}

	public int getOddsGrouplistSize() {
		return oddsGrouplist.size();
	}

	/**
	 * @return the oddsPairGroup
	 */
	public OpportunityOddsGroup getOddsPairGroup() {
		return oddsPairGroup;
	}

	/**
	 * @param oddsPairGroup
	 *            the oddsPairGroup to set
	 */
	public void setOddsPairGroup(OpportunityOddsGroup oddsPairGroup) {
		this.oddsPairGroup = oddsPairGroup;
	}

	public ArrayList<OpportunityOddsPairTemplate> getOddsGrouplist() {
		return oddsGrouplist;
	}

	public void setOddsGrouplist(ArrayList<OpportunityOddsPairTemplate> oddsGrouplist) {
		this.oddsGrouplist = oddsGrouplist;
	}

	public long getOddsGroupId() {
		return oddsGroupId;
	}
	public void setOddsGroupId(long oddsGroupId) {
		this.oddsGroupId = oddsGroupId;
	}

	public long getOddsGroupDefaultValue() {
		return oddsGroupDefaultValue;
	}

	public void setOddsGroupDefaultValue(long oddsGroupDefaultValue) {
		this.oddsGroupDefaultValue = oddsGroupDefaultValue;
	}

}
