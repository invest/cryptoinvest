package il.co.etrader.backend.mbeans;

import java.sql.SQLException;

import com.copyop.common.managers.FixCountersManager;
import com.copyop.migration.UserStatisticsManager;

import il.co.etrader.backend.bl_vos.User;
import il.co.etrader.backend.util.Utils;

public class CopyopAdminForm {
	
	
	public String fisStatistics() {
		User user = Utils.getUser();
		try {
			UserStatisticsManager.updateUserStatistics(user.getId());
		} catch (SQLException e) {
			e.printStackTrace();
			return "Unsuccessful";
		}
		return "Done";
	}

	public String fixCounters() {
		User user = Utils.getUser();
		FixCountersManager.counterFix(user.getId());
		return "Done";
	}

}
