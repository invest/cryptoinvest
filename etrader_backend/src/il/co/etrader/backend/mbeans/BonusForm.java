package il.co.etrader.backend.mbeans;

import java.io.Serializable;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;

import javax.faces.model.SelectItem;

import com.anyoption.common.beans.Bonus;

import il.co.etrader.backend.bl_managers.AdminManager;
import il.co.etrader.backend.bl_managers.BonusManager;
import il.co.etrader.backend.util.Constants;


public class BonusForm implements Serializable {

	private static final long serialVersionUID = 2898774640629285342L;

	private ArrayList<Bonus> list;
	public Bonus bonus;

	public BonusForm() throws SQLException{
		search();
	}

	public int getListSize() {
		return list.size();
	}

	public String search() throws SQLException{

		list=BonusManager.getAllBonuses();

		return null;
	}

	public ArrayList<Bonus> getList() {
		return list;
	}

	/**
	 * Navigate to edit bonus
	 * @return
	 * @throws SQLException
	 */
	public String navEdit() {
		return Constants.NAV_BONUS;
	}

	/**
	 * Navigate to insert new bonus
	 * @return
	 * @throws SQLException
	 */
	public String navNew() throws SQLException {
		bonus = new Bonus();
		bonus.setWriterId((int)AdminManager.getWriterId());
		Date now = new Date();
		bonus.setTimeCreated(now);
		bonus.setStartDate(now);
		bonus.setEndDate(now);

		return Constants.NAV_BONUS;
	}

	/**
	 * Update / insert bonus
	 * @return
	 * @throws SQLException
	 */
	public String updateInsert() throws SQLException{

		bonus.setWriterId((int)AdminManager.getWriterId());

		BonusManager.insertUpdateBonus(bonus);

        search();
		return Constants.NAV_BONUSES;
	}

    /**
     * @return the bonus
     */
    public Bonus getBonus() {
        return bonus;
    }

    /**
     * @param bonus the bonus to set
     */
    public void setBonus(Bonus bonus) {
        this.bonus = bonus;
    }

    /**
     * get numric type select items
     * @return select list
     */
    public ArrayList<SelectItem> getNumricTypeSI() {
		ArrayList<SelectItem> numricTypeSI = new ArrayList<SelectItem>();
		//TODO: need to take it from the class_type table ... or from the bonus type itself
		//numricTypeSI.add(new SelectItem(Constants.BONUS_NUMERIC_TYPE_FIXED,CommonUtil.getMessage("bonus.numricType.fixed",null)));
		//numricTypeSI.add(new SelectItem(Constants.BONUS_NUMERIC_TYPE_PERCENT,CommonUtil.getMessage("bonus.numricType.precent",null)));

		return numricTypeSI;
	}


}
