/**
 * 
 */
package il.co.etrader.backend.mbeans;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.Serializable;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.faces.validator.ValidatorException;

import org.apache.log4j.Logger;
import org.apache.myfaces.custom.fileupload.UploadedFile;

import com.anyoption.common.beans.Tournament;
import com.anyoption.common.beans.TournamentLanguageTabs;
import com.anyoption.common.beans.TournamentSkinsLanguages;
import com.anyoption.common.util.ConstantsBase;

import il.co.etrader.backend.bl_managers.ApplicationData;
import il.co.etrader.backend.bl_managers.TournamentManager;
import il.co.etrader.backend.util.Constants;
import il.co.etrader.backend.util.Utils;
import il.co.etrader.bl_managers.ApplicationDataBase;
import il.co.etrader.util.CommonUtil;

/**
 * @author pavelt
 *
 */
public class TournamentForm implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private static final Logger logger = Logger.getLogger(TournamentForm.class);	
	
	private Tournament tournament;
	private ArrayList<Tournament> tournamentsList;
	
	
	public TournamentForm() throws SQLException{
		
		search();
	}
	
	public String search() throws SQLException{
		tournamentsList = TournamentManager.getAllTournaments();
		tournamentsList = TournamentManager.getAllTournamentSkinLang(tournamentsList);
		return Constants.NAV_TOURNAMENTS;
	}

	public String updateInsert() throws SQLException {
		
		if(tournament.getMinInvAmount() <= 0){
			CommonUtil.addFacesMessage(FacesMessage.SEVERITY_ERROR, "tournament.min.investment.amount.validation", null);
			return null;
		}
		ArrayList<TournamentSkinsLanguages> tourSkinLangList = tournament.getTournamentSkinsLangList();
		if (tourSkinLangList != null) {
			for (TournamentSkinsLanguages item : tourSkinLangList) {
				// check if certain tab is available
				if (item.getTabId() != 0) {
					
					// check if prize image or prize text is filled
					if (item.getPrizeText().equals("")) {
						Object[] params = new Object[1];
						params[0] = item.getTabLabel();
						CommonUtil.addFacesMessage(FacesMessage.SEVERITY_ERROR, "tournament.prize.text.validation", params);
						return null;
					}
					
					if(item.getPrizeImage() == null && item.getFile() == null){
						Object[] params = new Object[1];
						params[0] = item.getTabLabel();
						CommonUtil.addFacesMessage(FacesMessage.SEVERITY_ERROR, "tournament.missing.file.validation", params);
						return null;
					}
					
					// check if file is uploaded correctly
					if (item.getFile() != null) {
						if (!uploadFile(item)) {
							CommonUtil.addFacesMessage(FacesMessage.SEVERITY_ERROR,"tournament.upload.file.fail", null);
							return null;
						}
					}
				}
			}
		}
		
		if (!TournamentManager.insertUpdateTournament(tournament)) {
			CommonUtil.addFacesMessage(FacesMessage.SEVERITY_ERROR,"tournament.update.insert.fail", null);
			return null;
		} else {
			Object[] params = new Object[1];
			params[0] = tournament.getId();
			CommonUtil.addFacesMessage(FacesMessage.SEVERITY_INFO,"tournament.update.insert.success", params);
			return search();
		}
	}

	public String close() {
		Object[] params = new Object[1];
		params[0] = tournament.getId();
		try {
			tournament.setActive(0);
			boolean res = TournamentManager.updateTournamentIsActive(tournament);
			if (res == false) {
				CommonUtil.addFacesMessage(FacesMessage.SEVERITY_INFO, "tournament.close.fail", params);
				return null;
			}
			
			CommonUtil.addFacesMessage(FacesMessage.SEVERITY_INFO, "tournament.close.success", params);
			return search();
		} catch (SQLException e) {
			logger.error("cant update is active for tournament " + tournament.getId());
			CommonUtil.addFacesMessage(FacesMessage.SEVERITY_INFO, "tournament.close.fail", params);
		}
		return null;
	}
	
	public String initValues() {
		tournament = new Tournament();
		tournament.setWriterId(Utils.getWriter().getWriter().getId());
		initTournamentSkinsLanguagesTabs(tournament);
		return Constants.NAV_TOURNAMENT;
	}
	public int getListSize(){
		return tournamentsList.size();
	}
	
	public Tournament getTournament() {
		return tournament;
	}
	public void setTournament(Tournament tournament) {
		this.tournament = tournament;
	}
	public ArrayList<Tournament> getTournamentsList() {
		return tournamentsList;
	}
	public void setTournamentsList(ArrayList<Tournament> tournamentsList) {
		this.tournamentsList = tournamentsList;
	}
	
	@SuppressWarnings("unchecked")
	public ArrayList<SelectItem> getTypesSI() {
		return (ArrayList<SelectItem>) ApplicationData.getApplicationSIHm().get("tournamentTypesSI");
	}
	
	@SuppressWarnings("unchecked")
	public ArrayList<SelectItem> getNumberOfUserSI() {
		return (ArrayList<SelectItem>) ApplicationData.getApplicationSIHm().get("tournamentNumberOfUserSI");
	}
	
	
	public void validateDate(FacesContext context, UIComponent comp, Object value) {
		if (value == null) {
			return;
		}
		Date date = (Date) value;
		LocalDate localDate = date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
		if (localDate.isBefore(LocalDate.now())) {
			FacesMessage msg = new FacesMessage(CommonUtil.getMessage("tournament.start.date.before.current.date", null));
			throw new ValidatorException(msg);
		}
	}

	private Tournament initTournamentSkinsLanguagesTabs(Tournament tournament) {
		ArrayList<TournamentSkinsLanguages> list = new ArrayList<TournamentSkinsLanguages>();
		ArrayList<TournamentLanguageTabs> langTabsList = ApplicationDataBase.getTournamentLangTabs();
		for (TournamentLanguageTabs i: langTabsList) {
			TournamentSkinsLanguages item = new TournamentSkinsLanguages();
			item.setTabId(i.getTabId());
			item.setTabLabel(i.getTabLabel());
			item.setTabDefLangId(i.getLangId());
			list.add(item);
		}
		tournament.setTournamentSkinsLangList(list);

		return tournament;
	}
	
	public boolean uploadFile(TournamentSkinsLanguages item) {	
			try {
				BufferedInputStream bis;
				bis = new BufferedInputStream(item.getFile().getInputStream(), 4096);
				String path = CommonUtil.getProperty(ConstantsBase.FILES_PATH) + Tournament.TOURNAMENTS_DIR_NAME + "/" + item.getFile().getName();
				logger.debug("Going to upload tournament image " + item.getFile().getName() + " to: " + path);
				java.io.File targetFile = new java.io.File(path); // destination
				BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(targetFile), 4096);
				int theChar;
				while((theChar = bis.read()) != -1) {
					bos.write(theChar);
				}
				bos.close();
				bis.close();
				item.setPrizeImage(Tournament.TOURNAMENTS_DIR_NAME + "/" + item.getFile().getName());
				logger.debug("uploaded tournament image " + item.getFile().getName() + " to: " + path);
				return true;
			} catch (IOException e) {
				logger.error("cant upload file!! " + item.getFile().getName(), e);
				return false;
			}
	}
	
	/**
	 * Validate image file
	 * 1. File in the right format (image file) 
	 * @param context
	 * @param comp
	 * @param value
	 */
	public static void validateFile(FacesContext context, UIComponent comp, Object value) {
		UploadedFile file = (UploadedFile) value;
		if (file != null) {
			String extension = file.getName().substring(file.getName().lastIndexOf('.') + 1, file.getName().length());
			if (!extension.equalsIgnoreCase("JPG") && !extension.equalsIgnoreCase("JPEG") 
					&& !extension.equalsIgnoreCase("png") && !extension.equalsIgnoreCase("gif")) {			
				FacesMessage msg = new FacesMessage(CommonUtil.getMessage("lp.upload.error.file.content.type", null));
				throw new ValidatorException(msg);				
			}
		}
	}
}
