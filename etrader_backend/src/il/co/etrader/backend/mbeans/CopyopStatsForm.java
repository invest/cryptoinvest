package il.co.etrader.backend.mbeans;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;

import org.apache.log4j.Logger;
import org.primefaces.model.chart.Axis;
import org.primefaces.model.chart.AxisType;
import org.primefaces.model.chart.BarChartModel;
import org.primefaces.model.chart.ChartSeries;
import org.primefaces.model.chart.DonutChartModel;

import com.anyoption.common.beans.base.AssetResult;
import com.anyoption.common.beans.base.TradesGroup;
import com.anyoption.common.managers.MarketsManagerBase;
import com.copyop.common.Constants;
import com.copyop.common.dto.Profile;
import com.copyop.common.dto.ProfileInvResultSelf;
import com.copyop.common.enums.base.UserStateEnum;
import com.copyop.common.managers.ConfigurationCopyopManager;
import com.copyop.common.managers.ProfileManager;
import com.copyop.common.managers.ProfileStatisticsManager;
import com.copyop.helper.HitsStatisticHelper;

import il.co.etrader.backend.bl_vos.User;
import il.co.etrader.backend.util.Utils;
import il.co.etrader.util.CommonUtil;
import il.co.etrader.util.ConstantsBase;

/**
 * @author eranl
 *
 */
public class CopyopStatsForm implements Serializable {

	private static final long serialVersionUID = -650475224380158892L;
	private static final Logger log = Logger.getLogger(CopyopStatsForm.class);
	private int numOfInvestments;
	private int hitRate;
	private ArrayList<TradesGroup> tradesBreakdownList;
	private ArrayList<AssetResult> assetsBreakdownList;
	private String bestAssetName;
	private String bestAssetProfit;
	private int bestStreak;
	private String bestStreakProfit;
	private double trades;
	private String tradesPeriodMsg;
	private DonutChartModel assetsBreakdowDonutModel;
	private BarChartModel tradesBreakdownAnimatedModel;

	private final int MIN_INV_FOR_SHOW = 10;

	public CopyopStatsForm() {
		User user = Utils.getUser();
		int limit = new Integer(ConfigurationCopyopManager.getCopyopPropertiesValue("INV_SUCCESS_RATE_HISTORY_SIZE", "120"));
		ProfileInvResultSelf pir = ProfileStatisticsManager.getProfileInvResultSelf(user.getId(), limit, UserStateEnum.STATE_REGULAR);
		if (pir != null && pir.getResults() != null) {
			numOfInvestments = pir.getResults().size();
			// calculate hot statistic only when num of investments is above 'MIN_INV_FOR_SHOW'
			if (numOfInvestments >= MIN_INV_FOR_SHOW) {
				int frequency = Collections.frequency(pir.getResults(), Boolean.TRUE);
				hitRate = Math.round(((float) frequency / pir.getResults().size() * 100));
				tradesBreakdownList = (ArrayList<TradesGroup>) HitsStatisticHelper.getHitStatisticTradersGroup(pir);
				assetsBreakdownList = (ArrayList<AssetResult>) HitsStatisticHelper.getHitStatisticAssetResult(pir);
				if (assetsBreakdownList != null) {
					for (AssetResult vo : assetsBreakdownList) {
						String marketName = "Other";
						if (vo.getMarketId() > 0) {
							marketName = MarketsManagerBase.getMarketName(user.getSkinId(), vo.getMarketId());
						}
						vo.setMarketName(marketName);
					}
				}
			}
		}

		Profile profile = ProfileManager.getProfile(user.getId());
		if (profile != null) {
			long bestAsset = profile.getBestAsset();
			bestAssetProfit = il.co.etrader.backend.util.Constants.EMPTY_STRING;
			if (bestAsset != 0) {
				bestAssetName = CommonUtil.getMarketName(bestAsset, user.getSkinId());
				Long bestAssetResult = profile.getAssetsResults().get(profile.getBestAsset());
				long bestAssetProfitAmount  = (bestAssetResult != null) ? bestAssetResult : 0l;
				bestAssetProfit =  CommonUtil.displayAmount(bestAssetProfitAmount * 100, user.getCurrencyId());
			}
			bestStreak = profile.getBestStreak();
			bestStreakProfit = il.co.etrader.backend.util.Constants.EMPTY_STRING;
			if (bestStreak != 0) {
				bestStreakProfit = CommonUtil.displayAmount(profile.getBestStreakAmount(), user.getCurrencyId());
			}
			Calendar cal = Calendar.getInstance();
			cal.add(Calendar.DAY_OF_MONTH, -7);
			Date lastWeek = cal.getTime();
			cal = Calendar.getInstance();
			cal.add(Calendar.DAY_OF_MONTH, -28);
			Date lastMonth = cal.getTime();
			int monthSum = 0;
			int lastWeekSum = 0;
			SimpleDateFormat formatter = new SimpleDateFormat(Constants.TRADES_HISTORY_DATE_FORMAT);
			for (String dateStr : profile.getTradesHistory().keySet()) {
				try {
					Date date = formatter.parse(dateStr);
					if (date.before(lastMonth)) {
						continue;
					}
					monthSum += profile.getTradesHistory().get(dateStr);
					if (date.after(lastWeek)) {
						lastWeekSum += profile.getTradesHistory().get(dateStr);
					}
				} catch (ParseException e) {
					log.error("Unable to parse date = " + dateStr, e);
				}
			}
			double avgMonthTrades = monthSum / 28;
			double avgLastWeekSum = lastWeekSum / 7;
			if (avgMonthTrades < avgLastWeekSum) {
				if (avgLastWeekSum > 0 && avgLastWeekSum < 1) {
					trades = Math.floor(avgLastWeekSum);
				} else {
					trades = Math.round(avgLastWeekSum);
				}
				tradesPeriodMsg = "copyop.stats.trades.week";
			} else {
				if (avgMonthTrades > 0 && avgMonthTrades < 1) {
					trades = Math.floor(avgMonthTrades);
				} else {
					trades = Math.round(avgMonthTrades);
				}
				tradesPeriodMsg = "copyop.stats.trades.month";
			}

			if (assetsBreakdownList != null && assetsBreakdownList.size() > 0) {
				assetsBreakdowDonutModel = initAssetsBreakdowDonutModel();
		        assetsBreakdowDonutModel.setTitle("Assets Breakdown");
		        assetsBreakdowDonutModel.setLegendPosition("e");
		        assetsBreakdowDonutModel.setSliceMargin(5);
		        assetsBreakdowDonutModel.setShowDataLabels(true);
		        assetsBreakdowDonutModel.setDataFormat("value");
		        assetsBreakdowDonutModel.setShadow(false);
			}

			if (tradesBreakdownList != null && tradesBreakdownList.size() > 0) {
		        tradesBreakdownAnimatedModel = initTradesBreakdownBarModel();
		        tradesBreakdownAnimatedModel.setTitle("Trades Breakdown");
		        tradesBreakdownAnimatedModel.setAnimate(true);
		        tradesBreakdownAnimatedModel.setLegendPosition("ne");
		        Axis yAxis = tradesBreakdownAnimatedModel.getAxis(AxisType.Y);
		        yAxis.setMin(0);
		        yAxis.setMax(100);
		        yAxis.setLabel("Hits");
		        Axis xAxis = tradesBreakdownAnimatedModel.getAxis(AxisType.X);
		        xAxis.setLabel("Trades");
			}
		}

	}

	/**
	 * Init asset breakdown donut model chart
	 * @return
	 */
	private DonutChartModel initAssetsBreakdowDonutModel() {
		DonutChartModel model = new DonutChartModel();
		Map<String, Number> circle = new LinkedHashMap<String, Number>();
		for (AssetResult ar : assetsBreakdownList) {
			String marketName = ar.getMarketName();
			if (CommonUtil.isParameterEmptyOrNull(marketName)) {
				marketName = "Other";
			}
			circle.put(ar.getHitRate() + "% | " + marketName, ar.getHitRate());
		}
		model.addCircle(circle);
		return model;
	}

    /**
     * Init trades breakdown bar model chart
     * @return
     */
    private BarChartModel initTradesBreakdownBarModel() {
        BarChartModel model = new BarChartModel();
        ChartSeries trades = new ChartSeries();
        for (TradesGroup tg : tradesBreakdownList) {
        	trades.set(tg.getStartIndex() + "-" + tg.getEndIndex(), tg.getHitRate());
        }
        trades.setLabel(ConstantsBase.EMPTY_STRING);
        model.addSeries(trades);
        return model;
    }

	/**
	 * @return the hitRate
	 */
	public int getHitRate() {
		return hitRate;
	}

	/**
	 * @param hitRate the hitRate to set
	 */
	public void setHitRate(int hitRate) {
		this.hitRate = hitRate;
	}

	/**
	 * @return the tradesBreakdownList
	 */
	public ArrayList<TradesGroup> getTradesBreakdownList() {
		return tradesBreakdownList;
	}

	/**
	 * @param tradesBreakdownList the tradesBreakdownList to set
	 */
	public void setTradesBreakdownList(ArrayList<TradesGroup> tradesBreakdownList) {
		this.tradesBreakdownList = tradesBreakdownList;
	}

	/**
	 * @return the assetsBreakdownList
	 */
	public ArrayList<AssetResult> getAssetsBreakdownList() {
		return assetsBreakdownList;
	}

	/**
	 * @param assetsBreakdownList the assetsBreakdownList to set
	 */
	public void setAssetsBreakdownList(ArrayList<AssetResult> assetsBreakdownList) {
		this.assetsBreakdownList = assetsBreakdownList;
	}

	/**
	 * @return the numOfInvestments
	 */
	public int getNumOfInvestments() {
		return numOfInvestments;
	}

	/**
	 * @param numOfInvestments the numOfInvestments to set
	 */
	public void setNumOfInvestments(int numOfInvestments) {
		this.numOfInvestments = numOfInvestments;
	}

	/**
	 * @return the bestAssetName
	 */
	public String getBestAssetName() {
		return bestAssetName;
	}

	/**
	 * @param bestAssetName the bestAssetName to set
	 */
	public void setBestAssetName(String bestAssetName) {
		this.bestAssetName = bestAssetName;
	}

	/**
	 * @return the bestStreak
	 */
	public int getBestStreak() {
		return bestStreak;
	}

	/**
	 * @param bestStreak the bestStreak to set
	 */
	public void setBestStreak(int bestStreak) {
		this.bestStreak = bestStreak;
	}

	/**
	 * @return the trades
	 */
	public double getTrades() {
		return trades;
	}

	/**
	 * @param trades the trades to set
	 */
	public void setTrades(double trades) {
		this.trades = trades;
	}

	/**
	 * @return the bestAssetProfit
	 */
	public String getBestAssetProfit() {
		return bestAssetProfit;
	}

	/**
	 * @param bestAssetProfit the bestAssetProfit to set
	 */
	public void setBestAssetProfit(String bestAssetProfit) {
		this.bestAssetProfit = bestAssetProfit;
	}

	/**
	 * @return the bestStreakProfit
	 */
	public String getBestStreakProfit() {
		return bestStreakProfit;
	}

	/**
	 * @param bestStreakProfit the bestStreakProfit to set
	 */
	public void setBestStreakProfit(String bestStreakProfit) {
		this.bestStreakProfit = bestStreakProfit;
	}

	/**
	 * @return the tradesPeriodMsg
	 */
	public String getTradesPeriodMsg() {
		return tradesPeriodMsg;
	}

	/**
	 * @param tradesPeriodMsg the tradesPeriodMsg to set
	 */
	public void setTradesPeriodMsg(String tradesPeriodMsg) {
		this.tradesPeriodMsg = tradesPeriodMsg;
	}

	/**
	 * @return the assetsBreakdowDonutModel
	 */
	public DonutChartModel getAssetsBreakdowDonutModel() {
		return assetsBreakdowDonutModel;
	}

	/**
	 * @param assetsBreakdowDonutModel the assetsBreakdowDonutModel to set
	 */
	public void setAssetsBreakdowDonutModel(DonutChartModel assetsBreakdowDonutModel) {
		this.assetsBreakdowDonutModel = assetsBreakdowDonutModel;
	}

	/**
	 * @return the tradesBreakdownAnimatedModel
	 */
	public BarChartModel getTradesBreakdownAnimatedModel() {
		return tradesBreakdownAnimatedModel;
	}

	/**
	 * @param tradesBreakdownAnimatedModel the tradesBreakdownAnimatedModel to set
	 */
	public void setTradesBreakdownAnimatedModel(
			BarChartModel tradesBreakdownAnimatedModel) {
		this.tradesBreakdownAnimatedModel = tradesBreakdownAnimatedModel;
	}

}
