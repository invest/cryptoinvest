package il.co.etrader.backend.mbeans;

import java.io.Serializable;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIInput;
import javax.faces.context.FacesContext;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;

import com.anyoption.common.beans.Investment;

import il.co.etrader.backend.bl_managers.RiskAlertsManager;
import il.co.etrader.backend.bl_managers.TransactionsManager;
import il.co.etrader.backend.util.Utils;
import il.co.etrader.bl_managers.InvestmentsManagerBase;
import il.co.etrader.bl_managers.RiskAlertsManagerBase;
import il.co.etrader.bl_vos.RiskAlert;
import il.co.etrader.bl_vos.Skins;
import il.co.etrader.util.CommonUtil;
import il.co.etrader.util.ConstantsBase;
import il.co.etrader.util.InvestmentFormatter;

public class RiskAlertsForm implements Serializable {

	private static final long serialVersionUID = -2551404749599701475L;

	private Date from;
	private Date to;
	private String userName;
	private ArrayList timeTypeList;
	private String timeType;
	private ArrayList<Integer> skinIds;
	private long userClassId;
	private ArrayList selectionTypes;
	private ArrayList statusTypes;
	private ArrayList<RiskAlert> riskAlertList;
	private ArrayList<Long> riskAlertsTypeDescriptions;
	private int succeededTrx;
	private RiskAlert riskAlert;
	private int transactionTypeId;
	private int statusTypeId;

	public RiskAlertsForm(){
		initComponents();
		updateRiskAlertsList();
	}


	/**
	 * Initialize Componont
	 */
	public void initComponents() {
		GregorianCalendar gc = new GregorianCalendar();
		FacesContext context = FacesContext.getCurrentInstance();
		riskAlertList = new ArrayList<RiskAlert>();
		riskAlertsTypeDescriptions = new ArrayList<Long>();
		userName = null;
		skinIds = new ArrayList<Integer>();
		skinIds.add(Skins.SKINS_ALL_VALUE);
		succeededTrx = 0;
		transactionTypeId = 0;
		statusTypeId = RiskAlertsManagerBase.STATUS_TYPE_WAS_NOT_REVIEWED;
		timeType="time_created";
		userClassId = ConstantsBase.USER_CLASS_COMPANY_OR_PRIVATE;
		to = gc.getTime();
		gc.add(GregorianCalendar.DAY_OF_MONTH, -7);
		from = gc.getTime();
		createDropDownList();
	}

	/**
	 * Update riskt alert List
	 * @return
	 */
	public String updateRiskAlertsList(){
		riskAlertList = RiskAlertsManager.getRiskAlertsList(from, to, timeType, userName, skinIds, userClassId,
				riskAlertsTypeDescriptions, succeededTrx, transactionTypeId, statusTypeId);
		if(null == riskAlertList) {
			riskAlertList = new ArrayList<RiskAlert>();
		}
		CommonUtil.setTablesToFirstPage();
		return null;
	}


	/**
	 * @return the selectionTypes
	 */
	public ArrayList getSelectionTypes() {
		return selectionTypes;
	}
	/**
	 * @param selectionTypes the selectionTypes to set
	 */
	public void setSelectionTypes(ArrayList selectionTypes) {
		this.selectionTypes = selectionTypes;
	}
	/**
	 * @return the userClassId
	 */
	public long getUserClassId() {
		return userClassId;
	}
	/**
	 * @param userClassId the userClassId to set
	 */
	public void setUserClassId(long userClassId) {
		this.userClassId = userClassId;
	}
	/**
	 * @return the skinId
	 */
	public ArrayList<Integer> getSkinIds() {
		return skinIds;
	}
	/**
	 * @param skinId the skinId to set
	 */
	public void setSkinIds(ArrayList<Integer> skinIds) {
		this.skinIds = skinIds;
	}
	/**
	 * @return the from
	 */
	public Date getFrom() {
		return from;
	}
	/**
	 * @param from the from to set
	 */
	public void setFrom(Date from) {
		this.from = from;
	}

	/**
	 * @return the to
	 */
	public Date getTo() {
		return to;
	}
	/**
	 * @param to the to to set
	 */
	public void setTo(Date to) {
		this.to = to;
	}


	/**
	 * @return the timeType
	 */
	public String getTimeType() {
		return timeType;
	}


	/**
	 * @param timeType the timeType to set
	 */
	public void setTimeType(String timeType) {
		this.timeType = timeType;
	}


	/**
	 * @return the timeTypeList
	 */
	public ArrayList getTimeTypeList() {
		return timeTypeList;
	}


	/**
	 * @param timeTypeList the timeTypeList to set
	 */
	public void setTimeTypeList(ArrayList timeTypeList) {
		this.timeTypeList = timeTypeList;
	}


	/**
	 * @return the userName
	 */
	public String getUserName() {
		return userName;
	}
	/**
	 * @param userName the userName to set
	 */
	public void setUserName(String userName) {
		this.userName = userName;
	}

	/**
	 * @return the riskAlertList
	 */
	public ArrayList<RiskAlert> getRiskAlertList() {
		return riskAlertList;
	}

	/**
	 * @param riskAlertList the riskAlertList to set
	 */
	public void setRiskAlertList(ArrayList<RiskAlert> riskAlertList) {
		this.riskAlertList = riskAlertList;
	}

	/**
	 * @return
	 */
	public int getListSize() {
		return riskAlertList.size();
	}

	/**
	 * @return the riskAlertsTypeDescription
	 */
	public ArrayList<Long> getRiskAlertsTypeDescriptions() {
		return riskAlertsTypeDescriptions;
	}

	/**
	 * @param riskAlertsTypeDescription the riskAlertsTypeDescription to set
	 */
	public void setRiskAlertsTypeDescriptions(ArrayList<Long> riskAlertsTypeDescriptions) {
		this.riskAlertsTypeDescriptions = riskAlertsTypeDescriptions;
	}

	/**
	 * @return the succeededTrx
	 */
	public int getSucceededTrx() {
		return succeededTrx;
	}

	/**
	 * @param succeededTrx the succeededTrx to set
	 */
	public void setSucceededTrx(int succeededTrx) {
		this.succeededTrx = succeededTrx;
	}

	/**
	 * @return the riskAlert
	 */
	public RiskAlert getRiskAlert() {
		return riskAlert;
	}

	/**
	 * @param riskAlert the riskAlert to set
	 */
	public void setRiskAlert(RiskAlert riskAlert) {
		this.riskAlert = riskAlert;
	}

	/**
	 * @return the transactionTypeId
	 */
	public int getTransactionTypeId() {
		return transactionTypeId;
	}


	/**
	 * @param transactionTypeId the transactionTypeId to set
	 */
	public void setTransactionTypeId(int transactionTypeId) {
		this.transactionTypeId = transactionTypeId;
	}
	
	/**
	 * @return the statusTypes
	 */
	public ArrayList getStatusTypes() {
		return statusTypes;
	}

	/**
	 * @param statusTypes the statusTypes to set
	 */
	public void setStatusTypes(ArrayList statusTypes) {
		this.statusTypes = statusTypes;
	}


	/**
	 * @return the statusTypeId
	 */
	public int getStatusTypeId() {
		return statusTypeId;
	}


	/**
	 * @param statusTypeId the statusTypeId to set
	 */
	public void setStatusTypeId(int statusTypeId) {
		this.statusTypeId = statusTypeId;
	}

	/**
	 * Create transaction types list and time types list
	 */
	public void createDropDownList(){
		FacesContext context = FacesContext.getCurrentInstance();
		//set options of types
		selectionTypes = new ArrayList();
		selectionTypes.add(new SelectItem(0,CommonUtil.getMessage("transactions.all.type", null, Utils.getWriterLocale(context))));
		selectionTypes.add(new SelectItem(TransactionsManager.TRANS_TYPE_ALL_WITHDRAW,CommonUtil.getMessage("transactions.all.withdraw", null, Utils.getWriterLocale(context))));
		selectionTypes.add(new SelectItem(TransactionsManager.TRANS_TYPE_CC_DEPOSIT,CommonUtil.getMessage("transactions.creditcard.deposit", null, Utils.getWriterLocale(context))));
		selectionTypes.add(new SelectItem(RiskAlertsManagerBase.TYPE_INVESTMENT,CommonUtil.getMessage("menu.investments", null, Utils.getWriterLocale(context))));

		//set options of timeTypes
		timeTypeList = new ArrayList();
		timeTypeList.add(new SelectItem("time_created",CommonUtil.getMessage("investments.timecreated",null, Utils.getWriterLocale(context))) );
		timeTypeList.add(new SelectItem("time_settled",CommonUtil.getMessage("investments.timeestclosing",null, Utils.getWriterLocale(context))) );
		
		//set options of statuses
		statusTypes = new ArrayList();
		statusTypes.add(new SelectItem(RiskAlertsManagerBase.STATUS_TYPE_ALL,CommonUtil.getMessage("risk.alert.status.type.all", null, Utils.getWriterLocale(context))));
		statusTypes.add(new SelectItem(RiskAlertsManagerBase.STATUS_TYPE_WAS_NOT_REVIEWED,CommonUtil.getMessage("risk.alert.status.type.was.not.reviewed", null, Utils.getWriterLocale(context))));
		statusTypes.add(new SelectItem(RiskAlertsManagerBase.STATUS_TYPE_WAS_REVIEWED,CommonUtil.getMessage("risk.alert.status.type.was.reviewed", null, Utils.getWriterLocale(context))));
	}
	
	public boolean isRiskAlertType1TFirstInvestment() {
		return riskAlertsTypeDescriptions.contains(new Long(RiskAlertsManagerBase.RISK_TYPE_FIRST_INVESTMENT_IS_1T));
	}
	
	public boolean isRiskAlertTypeCcHolderNameMismatch() {
		return riskAlertsTypeDescriptions.contains(new Long(RiskAlertsManagerBase.RISK_TYPE_DEPOSIT_MISMATCH_CARD_HOLDER));
	}
	
	public boolean isRiskAlertTypeCcAlreadyUsed() {
		return riskAlertsTypeDescriptions.contains(new Long(RiskAlertsManagerBase.RISK_TYPE_CC_USED_ON_WEBSITE));
	}
	
	public boolean isRiskAlertTypeCcCountryDifferentThanRegistrationCountry() {
		return riskAlertsTypeDescriptions.contains(new Long(RiskAlertsManagerBase.RISK_TYPE_DEPOSIT_CC_COUNTRY_DIFF_THAN_REG_COUNTRY));
	}
	
	public boolean isRiskAlertTypeRegistrationFromCountryDifferentThanIpCountry() {
		return riskAlertsTypeDescriptions.contains(new Long(RiskAlertsManagerBase.RISK_TYPE_REGISTRATION_FROM_COUNTRY_DIFF_THAN_IP_COUNTRY));
	}
	
	public boolean isRiskAlertTypeDepositCCFromDifferentCountries() {
		return riskAlertsTypeDescriptions.contains(new Long(RiskAlertsManagerBase.RISK_TYPE_DEPOSIT_CC_FROM_DIFFERENT_COUNTRIES));
	}
	
	public boolean isOnlyRiskAlertTypeDepositCCFromDifferentCountries() {
		return (riskAlertsTypeDescriptions.contains(new Long(RiskAlertsManagerBase.RISK_TYPE_DEPOSIT_CC_FROM_DIFFERENT_COUNTRIES))
				&& riskAlertsTypeDescriptions.size() == 1);
	}
	
	public boolean isRiskAlertTypeSingleLogin() {
		return (riskAlertsTypeDescriptions.contains(new Long(RiskAlertsManagerBase.RISK_TYPE_SINGLE_LOGIN))
				&& riskAlertsTypeDescriptions.size() == 1);
	}
	
	public String updateReviewedRiskAlert(ValueChangeEvent event) throws SQLException {
		Boolean isReviewed = (Boolean) event.getNewValue();
		UIInput menu = (UIInput) event.getComponent();
		long riskAlertId = (Long)menu.getAttributes().get("riskAlertId");
		if(isReviewed) {
			RiskAlertsManager.updateReviewedRiskAlert(riskAlertId);
			FacesContext context = FacesContext.getCurrentInstance();
			FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_INFO, "'Reviewed' was updated successfully", null);
			context.addMessage(null,fm);
			updateRiskAlertsList();
		}
		return null;	
	}

	public String getAmountWithoutFeesTxt(Investment i) {
		if (i == null) {
			return "";
		}
		return CommonUtil.displayAmount(InvestmentsManagerBase.getAmountWithoutFees(i), i.getCurrencyId());
	}

	public String getCurrentLevelTxt(Investment i) {
		if (i == null) {
			return "";
		}
		return InvestmentFormatter.getCurrentLevelTxt(i);
	}

	public String getCanceledWriterName(Investment i) throws SQLException {
		if (i == null) {
			return "";
		}
		return InvestmentFormatter.getCanceledWriterName(i);
	}

	public String getIsCanceledTxt(Investment i) {
		if (i == null) {
			return "";
		}
		return InvestmentFormatter.getIsCanceledTxt(i);
	}

	public String getScheduledTxt(Investment i) {
		if (i == null) {
			return "";
		}
		return InvestmentsManagerBase.getScheduledTxt(i.getScheduledId());
	}
}