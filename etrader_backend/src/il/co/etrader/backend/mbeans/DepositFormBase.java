package il.co.etrader.backend.mbeans;

import java.io.Serializable;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import javax.faces.context.FacesContext;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.base.UsersDetailsHistory;
import com.anyoption.common.managers.CurrenciesManagerBase;
import com.anyoption.common.managers.TransactionsManagerBase;

import il.co.etrader.backend.bl_vos.User;
import il.co.etrader.backend.util.Constants;
import il.co.etrader.backend.util.FormBase;
import il.co.etrader.backend.util.Utils;
import il.co.etrader.bl_managers.UsersManagerBase;
import il.co.etrader.util.ConstantsBase;


public class DepositFormBase extends FormBase implements Serializable {
	
	private static final Logger logger = Logger.getLogger(DepositFormBase.class);
	
	
	private static final long serialVersionUID = 1L;
	private Long currencyId;
	private String street;
	private String cityNameAO = "";
	private String zipCode;
	private Date birthDate;
	private String birthDateDay;
	private String birthDateMonth;
	private String birthDateYear;
	private boolean isFirstDeposit;
	private boolean isFirstDepositExcludeBonus;
	private boolean isDisableCcDeposits;

	/**
	 * Constructor
	 * 
	 * @throws SQLException
	 */
	public DepositFormBase() throws SQLException {
		FacesContext context = FacesContext.getCurrentInstance();
		User user = context.getApplication().evaluateExpressionGet(context, Constants.BIND_USER, User.class);
		
		reloadIsFirstDeposit();
		this.birthDate	= user.getTimeBirthDate();
		dateToString();
		this.currencyId	= user.getCurrencyId();
		this.street		= user.getStreet();
		this.cityNameAO	= user.getCityName();
		this.zipCode	= user.getZipCode();
		this.isDisableCcDeposits = TransactionsManagerBase.isDisableCcDeposits(user.getId());
	}
	
	/**
	 * Updates the user fields in case of first deposit - Currency, Street, City, ZIP
	 * 
	 * @param user
	 * @throws SQLException
	 */
	protected void updateUserFields(User user) throws SQLException {
		logger.debug("Setting user fields: currencyId [" + currencyId
										+ "], street [" + street
										+ "], zipCode [" + zipCode
										+ "], cityNameAO [" + cityNameAO
										+ "], birthDateDay [" + birthDateDay
										+ "], birthDateMonth [" + birthDateMonth
										+ "], birthDateYear [" + birthDateYear
										+ "], birthDate [" + birthDate + "]");
		// change user details.
		ArrayList<UsersDetailsHistory> usersDetailsHistoryList = null;
		birthDate = stringToDate();
        try {
            HashMap<Long, String> userDetailsBeforeChangedHM = UsersManagerBase.getUserDetailsHM(user.getSkinId(), null, ConstantsBase.NON_SELECTED, null, null, null, null, false, false, null, null, null, null, null, 
                    ConstantsBase.NON_SELECTED, null, null, null, null);
            HashMap<Long, String> userDetailsAfterChangedHM = UsersManagerBase.getUserDetailsHM(user.getSkinId(), street, ConstantsBase.NON_SELECTED, null, null, null, null, false, false, null, null, null, cityNameAO, 
                    null, ConstantsBase.NON_SELECTED, null, null, null, birthDate);
            usersDetailsHistoryList = UsersManagerBase.checkUserDetailsChange(userDetailsBeforeChangedHM, userDetailsAfterChangedHM);
        } catch (Exception e) {
            logger.error("problem to save user details history.", e);
        }
		
		long limitId = UsersManagerBase.updateUserFields(user.getId(),
															currencyId,
															street,
															zipCode,
															cityNameAO,
															user.getSkinId(),
															birthDate);
		user.setLimitId(limitId);
		if (null != usersDetailsHistoryList) {
            UsersManagerBase.insertUsersDetailsHistory(Utils.getWriter().getWriter().getId(), user.getId(), user.getUserName(), usersDetailsHistoryList, String.valueOf(user.getClassId()));
        }
		
		user.setCurrencyId(currencyId);
		user.setCurrency(CurrenciesManagerBase.getCurrency(currencyId));
		
		user.setStreet(street);
		user.setZipCode(zipCode);
		user.setCityName(cityNameAO);
		
		logger.debug("User fields set successfully");
	}
	
	public void updateUserFieldsDepositForm(User user) throws SQLException {
		updateUserFields(user);
	}
	
	/**
	 * Reloads the isFirstDeposit flag used for showing user fields.
	 * 
	 * @throws SQLException
	 */
	protected void reloadIsFirstDeposit() throws SQLException {
		FacesContext context = FacesContext.getCurrentInstance();
		User user = context.getApplication().evaluateExpressionGet(context, Constants.BIND_USER, User.class);
		isFirstDeposit = TransactionsManagerBase.isFirstPossibleDeposit(user.getId());
		isFirstDepositExcludeBonus = TransactionsManagerBase.isFirstPossibleDepositExcludeBonus(user.getId());
		dateToString();
	}
	
	private Date stringToDate (){
		SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
		String dateInString = birthDateDay+"/"+birthDateMonth+"/"+ birthDateYear;
		Date date = null;
		try {
				date = (Date) formatter.parse(dateInString);
		} catch (ParseException e) {
			logger.error("problem composing birth date out of day, month and year.", e);
		}
		return date;
	}
	private void dateToString(){
		if(birthDate!=null){
			SimpleDateFormat formatDateJava = new SimpleDateFormat("dd/MM/yyyy");
			String date = formatDateJava.format(birthDate);
			this.birthDateDay = date.substring(0, date.indexOf("/"));
			this.birthDateMonth = date.substring(date.indexOf("/")+1, date.lastIndexOf("/"));
			this.birthDateYear = date.substring(date.lastIndexOf("/")+1, date.length());
		}
	}
	
	public void reloadIsFirstDepositForm() throws SQLException {
		reloadIsFirstDeposit();
	}

	public Long getCurrencyId() {
		return currencyId;
	}

	public void setCurrencyId(Long currencyId) {
		this.currencyId = currencyId;
	}

	public String getStreet() {
		return street;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	public String getCityNameAO() {
		return cityNameAO;
	}

	public void setCityNameAO(String cityNameAO) {
		this.cityNameAO = cityNameAO;
	}

	public String getZipCode() {
		return zipCode;
	}

	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}

	public boolean isFirstDeposit() {
		return isFirstDeposit;
	}

	public void setFirstDeposit(boolean isFirstDeposit) {
		this.isFirstDeposit = isFirstDeposit;
	}
	
	public String getBirthDateDay() {
		return birthDateDay;
	}

	public void setBirthDateDay(String birthDateDay) {
		this.birthDateDay = birthDateDay;
	}

	public String getBirthDateMonth() {
		return birthDateMonth;
	}

	public void setBirthDateMonth(String birthDateMonth) {
		this.birthDateMonth = birthDateMonth;
	}

	public String getBirthDateYear() {
		return birthDateYear;
	}

	public void setBirthDateYear(String birthDateYear) {
		this.birthDateYear = birthDateYear;
	}

	public boolean isFirstDepositExcludeBonus() {
		return isFirstDepositExcludeBonus;
	}

	public void setFirstDepositExcludeBonus(boolean isFirstDepositExcludeBonus) {
		this.isFirstDepositExcludeBonus = isFirstDepositExcludeBonus;
	}

	public boolean isDisableCcDeposits() {
		return isDisableCcDeposits;
	}

	public void setDisableCcDeposits(boolean isDisableCcDeposits) {
		this.isDisableCcDeposits = isDisableCcDeposits;
	}
	
}
