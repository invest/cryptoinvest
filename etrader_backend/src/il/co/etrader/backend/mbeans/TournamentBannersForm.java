/**
 * 
 */
package il.co.etrader.backend.mbeans;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.Serializable;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.ValidatorException;

import org.apache.log4j.Logger;
import org.apache.myfaces.custom.fileupload.UploadedFile;

import com.anyoption.common.beans.TournamentBannerFile;
import com.anyoption.common.beans.TournamentLanguageTabs;
import com.anyoption.common.beans.TournamentsBanners;
import com.anyoption.common.managers.TournamentManagerBase;
import com.anyoption.common.util.ConstantsBase;

import il.co.etrader.backend.bl_managers.TournamentManager;
import il.co.etrader.backend.util.Constants;
import il.co.etrader.backend.util.Utils;
import il.co.etrader.bl_managers.ApplicationDataBase;
import il.co.etrader.util.CommonUtil;

/**
 * @author pavelt
 *
 */
public class TournamentBannersForm implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8084240675879401960L;
	private static final Logger logger = Logger.getLogger(TournamentBannersForm.class);
	
	private ArrayList<TournamentsBanners> tournamentsBanners; 
	
	public TournamentBannersForm () throws SQLException{
		search();
	}
	
	public String search() throws SQLException {
		tournamentsBanners = TournamentManagerBase.getTournamentsBanners();
		if (tournamentsBanners.isEmpty()) {
			initValues(tournamentsBanners);
		}
		return Constants.NAV_TOURNAMENTS_BANNERS;
	}

	public ArrayList<TournamentsBanners> getTournamentsBanners() {
		return tournamentsBanners;
	}

	public void setTournamentsBanners(ArrayList<TournamentsBanners> tournamentsBanners) {
		this.tournamentsBanners = tournamentsBanners;
	}
	
	public String insertUpdateBanners() throws SQLException{
		if(tournamentsBanners != null){
			for(TournamentsBanners item : tournamentsBanners){
				for(TournamentBannerFile file : item.getFiles()){
					
					if(file.getFile() == null && file.getBannerImage() == null){
						Object[] params = new Object[1];
						params[0] = item.getTabLabel();
						CommonUtil.addFacesMessage(FacesMessage.SEVERITY_ERROR, "tournament.banner.validation", params);
						return null;
					}
				}
			}
			
			for(TournamentsBanners item : tournamentsBanners){
				for(TournamentBannerFile file : item.getFiles()){
					if(file.getFile() != null){
						if(!uploadFile(file)){
							CommonUtil.addFacesMessage(FacesMessage.SEVERITY_ERROR, "tournament.upload.file.fail", null);
							return null;
						}
					}
				}
				
				item.setWriterId(Utils.getWriter().getWriter().getId());
				TournamentManager.insertUpdateTournamentsBanners(item);
			}
		}
		CommonUtil.addFacesMessage(FacesMessage.SEVERITY_ERROR, "tournament.banner.update.insert.success", null);
		return search();
	}
	public boolean uploadFile(TournamentBannerFile file) {	
		if (file != null) {
			try {
				boolean dirExists = true;
				String dirName = CommonUtil.getProperty(ConstantsBase.FILES_PATH) + TournamentsBanners.TOURNAMENTS_BANNERS_DIR_NAME;
				java.io.File f = new java.io.File(dirName);
				if (!f.exists()) {
					try{
						f.mkdir();
					}catch(SecurityException se){
				    	dirExists = false;
					}
				}
				
				if(dirExists){
					BufferedInputStream bis;
					bis = new BufferedInputStream(file.getFile().getInputStream(), 4096);
					String path = dirName + "/" + file.getFile().getName();
					logger.debug("Going to upload tournament banner image " + file.getFile().getName() + " to: " + path);
					java.io.File targetFile = new java.io.File(path); // destination
					BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(targetFile), 4096);
					int theChar;
					while((theChar = bis.read()) != -1) {
						bos.write(theChar);
					}
					bos.close();
					bis.close();
					file.setBannerImage(TournamentsBanners.TOURNAMENTS_BANNERS_DIR_NAME + "/" + file.getFile().getName());
					logger.debug("uploaded tournament banner image " + file.getFile().getName() + " to: " + path);
					return true;
				}else{
					logger.error("can NOT create the tournamentBanners directory!");
					return false;
				}
			} catch (IOException e) {
				logger.error("can NOT upload tournament banner file!! " + file.getFile().getName(), e);
				return false;
			}
		}
		return false;
	}
	
	private ArrayList<TournamentsBanners> initValues(ArrayList<TournamentsBanners> bannersList) {
		ArrayList<TournamentLanguageTabs> langTabsList = ApplicationDataBase.getTournamentLangTabs();
		for (TournamentLanguageTabs tab : langTabsList) {
			TournamentsBanners item = new TournamentsBanners();
			item.setTabId(tab.getTabId());
			item.setTabLabel(tab.getTabLabel());
			for (int i = 1; i < 4; i++) {
				TournamentBannerFile defaultBanner = new TournamentBannerFile();
				defaultBanner.setBannerIndex(i);
				item.getFiles().add(defaultBanner);
			}
			bannersList.add(item);
		}
		return bannersList;
	}
	
	/**
	 * Validate image file
	 * 1. File in the right format (image file) 
	 * @param context
	 * @param comp
	 * @param value
	 */
	public static void validateFile(FacesContext context, UIComponent comp, Object value) {
		UploadedFile file = (UploadedFile) value;
		if (file != null) {
			String extension = file.getName().substring(file.getName().lastIndexOf('.') + 1, file.getName().length());
			if (!extension.equalsIgnoreCase("JPG") && !extension.equalsIgnoreCase("JPEG") 
					&& !extension.equalsIgnoreCase("png") && !extension.equalsIgnoreCase("gif")) {			
				FacesMessage msg = new FacesMessage(CommonUtil.getMessage("lp.upload.error.file.content.type", null));
				throw new ValidatorException(msg);				
			}
		}
	}
}
