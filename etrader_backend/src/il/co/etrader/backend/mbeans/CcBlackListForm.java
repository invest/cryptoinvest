package il.co.etrader.backend.mbeans;

import java.io.Serializable;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;

import il.co.etrader.backend.bl_managers.AdminManager;
import il.co.etrader.backend.util.Constants;
import il.co.etrader.bl_vos.CcBlackList;
import il.co.etrader.util.CommonUtil;

public class CcBlackListForm implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1327331850441257474L;
	private CcBlackList ccblacklist;
	private ArrayList list;
	private Date from;
	private Date to;

	public CcBlackListForm() throws SQLException{


		GregorianCalendar gc = new GregorianCalendar();
		//set time as 00:00:00
		//gc.set(Calendar.HOUR_OF_DAY, 0);
		//gc.set(Calendar.MINUTE, 0);
		//gc.set(Calendar.SECOND, 0);
		to = gc.getTime();
		gc.add(GregorianCalendar.MONTH, -1);
		from = gc.getTime();
		search();
	}

	public int getListSize() {
		return list.size();
	}

	public String updateInsert() throws SQLException{

		boolean res=AdminManager.updateInsertCcBlackList(ccblacklist);

		if (res==false)
			return null;

		search();
		return Constants.NAV_CC_BLACK_LIST;
	}

	public String navAddCcBlackList() throws SQLException{
		ccblacklist=new CcBlackList();
		ccblacklist.setIsActiveBoolean(true);
		ccblacklist.setWriterId(AdminManager.getWriterId());
		ccblacklist.setTimeCreated(new Date());
		return Constants.NAV_CC_BLACK_LIST_EDIT;
	}

	public String navEdit() {
		return Constants.NAV_CC_BLACK_LIST_EDIT;
	}

	public String navCcBlackList() throws SQLException{
		search();
		return Constants.NAV_CC_BLACK_LIST;
	}


	public String search() throws SQLException{
		list=AdminManager.searchCcBlackList(from,to);
		CommonUtil.setTablesToFirstPage();
		return null;
	}

	public ArrayList getList() {
		return list;
	}

	public CcBlackList getCcblacklist() {
		return ccblacklist;
	}

	public void setCcblacklist(CcBlackList ccblacklist) {
		this.ccblacklist = ccblacklist;
	}

	public Date getFrom() {
		return from;
	}

	public void setFrom(Date from) {
		this.from = from;
	}

	public Date getTo() {
		return to;
	}

	public void setTo(Date to) {
		this.to = to;
	}

	public void setList(ArrayList list) {
		this.list = list;
	}

	/**
	 * Constructs a <code>String</code> with all attributes
	 * in name = value format.
	 *
	 * @return a <code>String</code> representation
	 * of this object.
	 */
	public String toString()
	{
	    final String TAB = " \n ";

	    String retValue = "";

	    retValue = "CcBlackListForm ( "
	        + super.toString() + TAB
	        + "ccblacklist = " + this.ccblacklist + TAB
	        + "list = " + this.list + TAB
	        + "from = " + this.from + TAB
	        + "to = " + this.to + TAB
	        + " )";

	    return retValue;
	}

}
