package il.co.etrader.backend.mbeans;

import java.io.Serializable;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import org.apache.log4j.Logger;

import com.anyoption.common.managers.PopulationUsersAssignTypesManagerBase;

import il.co.etrader.backend.bl_managers.PopulationEntriesManager;
import il.co.etrader.backend.bl_managers.UsersManager;
import il.co.etrader.backend.bl_managers.WritersManager;
import il.co.etrader.backend.bl_vos.PopulationEntry;
import il.co.etrader.backend.bl_vos.PopulationUsersAssignment;
import il.co.etrader.backend.bl_vos.PopulationUsersAssignmentInfo;
import il.co.etrader.backend.bl_vos.User;
import il.co.etrader.backend.bl_vos.WriterWrapper;
import il.co.etrader.backend.util.Constants;
import il.co.etrader.backend.util.Utils;
import il.co.etrader.bl_vos.RankStatus;
import il.co.etrader.bl_vos.Writer;
import il.co.etrader.bl_vos.WriterMaxAssignment;
import il.co.etrader.population.PopulationHandlersException;
import il.co.etrader.util.CommonUtil;
import il.co.etrader.util.ConstantsBase;

public class OverviewForm implements Serializable {
	private static final long serialVersionUID = -3297426032762150635L;

	private static final Logger logger = Logger.getLogger(OverviewForm.class);

	private ArrayList<Writer> overAllList;
	private ArrayList<Writer> todayList;
	
	private ArrayList<RankStatus> rankStatus = null;
	private ArrayList<WriterMaxAssignment> wrMaxAssignment = null;
	private Writer selectedWriter = null;
	
	private long skinId;
	private String view;

	// collect filters
	private long entryTypeId;
	private long repId;
	private boolean collectAll;
	private long writerFilter;
	
	private long salesType;
	private boolean isRetention;
	private boolean isConvresion;
	private long retentionTeamId;
	private long reassignToWriterId;

	/**
	 * @return the writerFilter
	 */
	public long getWriterFilter() {
		return writerFilter;
	}

	/**
	 * @param writerFilter the writerFilter to set
	 */
	public void setWriterFilter(long writerFilter) {
		this.writerFilter = writerFilter;
	}

	public OverviewForm() throws SQLException{
		initFormFilters();
		initConverstionOrRetention();
		//updateList();
	}
	
	private void initFormFilters() {
		skinId = 0;
		writerFilter = 0;
		entryTypeId = 1;
		view = Constants.SALES_OVERVIEW_FILTER;
		isRetention = false;
		isConvresion = false;
		retentionTeamId = ConstantsBase.ALL_FILTER_ID;
	}
	
	public void initOverviewFormForm() throws SQLException{
		initFormFilters();
		updateList();
	}
	
	public String getGoConvertion() throws SQLException {
		salesType = ConstantsBase.SALES_DEPOSIT_TYPE_CONVERSION;
		this.retentionTeamId = ConstantsBase.ALL_FILTER_ID;
		if (isRetention) {
			updateList();
		}
		Writer w = Utils.getWriter().getWriter();
		w.setScreenType(ConstantsBase.SALES_DEPOSIT_TYPE_CONVERSION);
		this.isConvresion = true;
		this.isRetention = false;
		return "";
	}

	public String getGoRetention() throws SQLException {
		salesType = ConstantsBase.SALES_DEPOSIT_TYPE_RETENTION;
		if (isConvresion) {
			User u = Utils.getUser();
			if (u.isRetentionMSelected()) {
				retentionTeamId = WritersManager.RETENTION_TEAMS_UPGRADE;
			}
			updateList();
		}	
		Writer w = Utils.getWriter().getWriter();
		w.setScreenType(ConstantsBase.SALES_DEPOSIT_TYPE_RETENTION);
		this.isConvresion = false;
		this.isRetention = true;
		return "";
	}

	public int getOverAllListSize() {
		return overAllList.size();
	}

	public int getTodayListSize() {
		return todayList.size();
	}

	public String updateList() throws SQLException {
		if (view.equals(Constants.SALES_OVERVIEW_FILTER)) {
			 return updatOverAlleList();
		} else {
			return updatTodayeList();
		}
	}

	public String updatOverAlleList() throws SQLException {
		overAllList = WritersManager.getWritersOverAllView(skinId,writerFilter, salesType);
		return null;
	}

	public String updatTodayeList() throws SQLException {
		todayList = WritersManager.getWritersTodayView(skinId,writerFilter);
		return null;
	}
	
	public void initConverstionOrRetention() {
		Writer w = Utils.getWriter().getWriter();
		User u = Utils.getUser();
		if (w.getSales_type() == Constants.SALES_TYPE_RETENTION || (u.isRetentionMSelected() && isRetention)) {
			this.salesType = ConstantsBase.SALES_DEPOSIT_TYPE_RETENTION;			
		} else if (w.getSales_type() == Constants.SALES_TYPE_CONVERSION || (u.isRetentionMSelected() && isConvresion)) {
			this.salesType = ConstantsBase.SALES_DEPOSIT_TYPE_CONVERSION;
		}
	}


	/**
	 * Collect action
	 */
	public String collect() throws NumberFormatException, SQLException, PopulationHandlersException {
		FacesContext context = FacesContext.getCurrentInstance();
		WriterWrapper wr = (WriterWrapper)context.getApplication().createValueBinding(Constants.BIND_WRITER_WRAPPER).getValue(context);
		if (collectAll) {
			PopulationEntriesManager.collectTasks(null, Integer.parseInt(String.valueOf(entryTypeId)), wr.getWriter().getId(), skinId, retentionTeamId, salesType);
		} else { // collect for selected rep's
			ArrayList<Long> repList = new ArrayList<Long>();
			for (Writer w : overAllList) {
				if (w.isToCollect()) {
					repList.add(new Long(w.getId()));
				}
			}
			PopulationEntriesManager.collectTasks(repList, Integer.parseInt(String.valueOf(entryTypeId)), wr.getWriter().getId(), skinId, retentionTeamId, salesType);
		}
		Utils.popupMessage("retention.submit.collect.done");
		updateList();
		return null;
	}
	
	public String showReport(Writer w) {
		long writerId = w.getId();
		try{
			rankStatus = WritersManager.getRetentionRepresentativeAssignments(writerId);
			wrMaxAssignment = WritersManager.getWriterMaxAutoAssignment(writerId);
			int n = wrMaxAssignment.size()/21;
			// more than one skin assigned to this writer
			if(n>1 ){
				ArrayList<WriterMaxAssignment> was = new ArrayList<WriterMaxAssignment>(); 
				for(int i=0;i<wrMaxAssignment.size();i=i+n) {
					was.add(wrMaxAssignment.get(i));
				}
				wrMaxAssignment = was;
			}
			selectedWriter = w;
			reassignToWriterId = ConstantsBase.ALL_FILTER_ID;
		}catch(SQLException ex) {
			ex.printStackTrace();
			return null;
		}
		return Constants.MAX_ASSIGNMENT_REPORT;
	}

	public String rankIdToLabel(int id){
		switch (id) { 
			case 1: return CommonUtil.getMessage("overview.newbes", null);
			case 2: return CommonUtil.getMessage("overview.reg", null);
			case 3: return CommonUtil.getMessage("overview.beg", null);
			case 4: return CommonUtil.getMessage("overview.med", null);
			case 5: return CommonUtil.getMessage("overview.high", null);
			case 6: return CommonUtil.getMessage("overview.gold", null);
			case 7: return CommonUtil.getMessage("overview.platinum", null);
			default: return ""+id;
		}
	}
	
	/**
	 * Reassign
	 * @return String
	 * @throws PopulationHandlersException
	 * @throws SQLException
	 */
	public String reassign() throws PopulationHandlersException, SQLException {
		FacesContext context = FacesContext.getCurrentInstance();
		WriterWrapper wr = (WriterWrapper) context.getApplication().createValueBinding(Constants.BIND_WRITER_WRAPPER).getValue(context);
		PopulationUsersAssignment populationUsersAssignment = new PopulationUsersAssignment();
		if (isRetention) {
			for (RankStatus rs : rankStatus) {
				String userStatus = getUserStatus(rs);
				if (!userStatus.equals(ConstantsBase.EMPTY_STRING)) {
					PopulationUsersAssignment pua = reassignEntries(context, wr, reassignToWriterId, rs.getRank(), userStatus, ConstantsBase.ALL_FILTER_ID);
					populationUsersAssignment.setNumOfLeads(populationUsersAssignment.getNumOfLeads() + pua.getNumOfLeads());
					populationUsersAssignment.setNumOfLeadsUpdated(populationUsersAssignment.getNumOfLeadsUpdated() + pua.getNumOfLeadsUpdated());
					populationUsersAssignment.setAssignToRepId(pua.getAssignToRepId());
				}
			}
		} else {
			populationUsersAssignment = reassignEntries(context, wr, selectedWriter.getReassignToRepId(), 0, ConstantsBase.EMPTY_STRING, entryTypeId);
		}
		addMessages(context, populationUsersAssignment);
		updateList();
		if (isRetention) {
			updateRankStatusList();
		}
		return null;
	}
	
	/**
	 * Reassign entries
	 * @param context
	 * @param wr
	 * @param reassignToRepId
	 * @param userRankId
	 * @param userStatus
	 * @return PopulationUsersAssignment
	 * @throws PopulationHandlersException
	 * @throws SQLException
	 */
	public PopulationUsersAssignment reassignEntries(FacesContext context, WriterWrapper wr, long reassignToRepId, long userRankId, String userStatus, long popEntryTypeId) throws PopulationHandlersException, SQLException {
		long writerId = wr.getWriter().getId();
		long reassignFromRepId = selectedWriter.getId();
		String entriesListType = String.valueOf(popEntryTypeId);
		if (popEntryTypeId == ConstantsBase.ALL_FILTER_ID) {
			entriesListType = String.valueOf(ConstantsBase.POP_ENTRY_TYPE_GENERAL) + "," +
								String.valueOf(ConstantsBase.POP_ENTRY_TYPE_TRACKING) + "," +
								String.valueOf(ConstantsBase.POP_ENTRY_TYPE_CALLBACK);
		}
		ArrayList<PopulationEntry> popEntries = PopulationEntriesManager.getPopulationEntriesForReassign(reassignFromRepId, salesType, entriesListType, userRankId, userStatus);
		ArrayList<Integer> writerSkins = WritersManager.getWriterSkins(reassignToRepId);
		int successfullyUpdatedLeads = 0;
		for (PopulationEntry entry : popEntries) {
			if (reassignToRepId == PopulationUsersAssignTypesManagerBase.OPTION_UNASSIGN) {
				if (PopulationEntriesManager.cancelAssign(entry, false, false)) {
					successfullyUpdatedLeads++;
				}
			} else {
				if (writerSkins.contains((int)entry.getSkinId())) {
					if (isRetention) {
						PopulationUsersAssignmentInfo puai = PopulationEntriesManager.isAssignmentOk(reassignToRepId, entry.getSkinId(), entry.getUserRankId(), entry.getUserStatusId()); 
						if (puai.isAssignmentOk()) {
							entry.setAssignWriterId(reassignToRepId);
							if (PopulationEntriesManager.assign(entry, false, reassignFromRepId, false, true, writerId)) {
								successfullyUpdatedLeads++;
							}
						}
					} else {
						entry.setAssignWriterId(reassignToRepId);
						if (PopulationEntriesManager.assign(entry, false, reassignFromRepId, false, true, writerId)) {
							successfullyUpdatedLeads++;
						}
					}
				}
			}
		}
		PopulationUsersAssignment pua = new PopulationUsersAssignment();
		pua.setNumOfLeads(popEntries.size());
		pua.setNumOfLeadsUpdated(successfullyUpdatedLeads);
		pua.setAssignToRepId(reassignToRepId);
		return pua;
	}
	
	/**
	 * Add messages
	 * @param context
	 * @param populationUsersAssignment
	 * @throws SQLException
	 */
	public void addMessages(FacesContext context, PopulationUsersAssignment populationUsersAssignment) throws SQLException {
		Writer w = WritersManager.getWriter(populationUsersAssignment.getAssignToRepId());
		String writerName = CommonUtil.getMessage("overview.reassign.general.pool", null);
		if (null != w) {
			writerName = w.getUserName();
		}
		String[] params = new String[3];
		params[0] = String.valueOf(populationUsersAssignment.getNumOfLeadsUpdated());
		params[1] = String.valueOf(populationUsersAssignment.getNumOfLeads());
		params[2] = writerName;
		String msg = CommonUtil.getMessage("overview.reassign.summary", params);
		FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_INFO, msg, null);
		context.addMessage(null, fm);
	}
	
	/**
	 * Get User Status
	 * @param rs as RankStatus
	 * @return
	 */
	public String getUserStatus(RankStatus rs) {
		List<Long> statusList = new ArrayList<Long>();
		if (rs.isCheckedActive()) {
			statusList.add(UsersManager.USER_STATUS_ACTIVE);
		}
		if (rs.isCheckedSleeper()) {
			statusList.add(UsersManager.USER_STATUS_SLEEPER);
		}
		if (rs.isCheckedComma()) {
			statusList.add(UsersManager.USER_STATUS_COMA);
		}
		if (statusList.size() > 0) {
			return CommonUtil.getArrayToString(statusList);
		}
		return ConstantsBase.EMPTY_STRING;
	}
	
	public void updateRankStatusList() throws SQLException {
		rankStatus = WritersManager.getRetentionRepresentativeAssignments(selectedWriter.getId());
	}

	/**
	 * @return the overAllList
	 */
	public ArrayList<Writer> getOverAllList() {
		return overAllList;
	}

	/**
	 * @param overAllList the overAllList to set
	 */
	public void setOverAllList(ArrayList<Writer> overAllList) {
		this.overAllList = overAllList;
	}

	/**
	 * @return the todayList
	 */
	public ArrayList<Writer> getTodayList() {
		return todayList;
	}

	/**
	 * @param todayList the todayList to set
	 */
	public void setTodayList(ArrayList<Writer> todayList) {
		this.todayList = todayList;
	}

	/**
	 * @return the skinId
	 */
	public long getSkinId() {
		return skinId;
	}

	/**
	 * @param skinId the skinId to set
	 */
	public void setSkinId(long skinId) {
		this.skinId = skinId;
	}

	/**
	 * @return the view
	 */
	public String getView() {
		return view;
	}

	/**
	 * @param view the view to set
	 */
	public void setView(String view) {
		this.view = view;
	}

	/**
	 * @return the entryTypeId
	 */
	public long getEntryTypeId() {
		return entryTypeId;
	}

	/**
	 * @param entryTypeId the entryTypeId to set
	 */
	public void setEntryTypeId(long entryTypeId) {
		this.entryTypeId = entryTypeId;
	}

	/**
	 * @return the repId
	 */
	public long getRepId() {
		return repId;
	}

	/**
	 * @param repId the repId to set
	 */
	public void setRepId(long repId) {
		this.repId = repId;
	}

	/**
	 * @return the collectAll
	 */
	public boolean isCollectAll() {
		return collectAll;
	}

	/**
	 * @param collectAll the collectAll to set
	 */
	public void setCollectAll(boolean collectAll) {
		this.collectAll = collectAll;
	}

	/**
	 * @return the salesType
	 */
	public long getSalesType() {
		return salesType;
	}

	/**
	 * @param salesType the salesType to set
	 */
	public void setSalesType(long salesType) {
		this.salesType = salesType;
	}

	/**
	 * @return the isRetention
	 */
	public boolean isRetention() {
		return isRetention;
	}

	/**
	 * @param isRetention the isRetention to set
	 */
	public void setRetention(boolean isRetention) {
		this.isRetention = isRetention;
	}

	/**
	 * @return the isConvresion
	 */
	public boolean isConvresion() {
		return isConvresion;
	}

	/**
	 * @param isConvresion the isConvresion to set
	 */
	public void setConvresion(boolean isConvresion) {
		this.isConvresion = isConvresion;
	}

	public ArrayList<RankStatus> getRankStatus() {
		return rankStatus;
	}

	public void setRankStatus(ArrayList<RankStatus> rankStatus) {
		this.rankStatus = rankStatus;
	}

	public ArrayList<WriterMaxAssignment> getWrMaxAssignmen() {
		return wrMaxAssignment;
	}

	public void setWrMaxAssignmen(ArrayList<WriterMaxAssignment> wrMaxAssignmen) {
		this.wrMaxAssignment = wrMaxAssignmen;
	}

	public Writer getSelectedWriter() {
		return selectedWriter;
	}

	public void setSelectedWriter(Writer selectedWriter) {
		this.selectedWriter = selectedWriter;
	}
	
	/**
	 * @return the retentionTeamId
	 */
	public long getRetentionTeamId() {
		return retentionTeamId;
	}

	/**
	 * @param retentionTeamId the retentionTeamId to set
	 */
	public void setRetentionTeamId(long retentionTeamId) {
		this.retentionTeamId = retentionTeamId;
	}

	/**
	 * @return the reassignToWriterId
	 */
	public long getReassignToWriterId() {
		return reassignToWriterId;
	}

	/**
	 * @param reassignToWriterId the reassignToWriterId to set
	 */
	public void setReassignToWriterId(long reassignToWriterId) {
		this.reassignToWriterId = reassignToWriterId;
	}
}
