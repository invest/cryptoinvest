package il.co.etrader.backend.mbeans;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.TimeZone;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import il.co.etrader.backend.bl_managers.MarketingManager;
import il.co.etrader.backend.util.Utils;
import il.co.etrader.util.CommonUtil;


public class MarketingOperationsForm {
	/*
	 * up to 200 chars, mandatory field
	 */
	String	name;
	/*
	 * integer from 1-100, mandatory field
	 */
	Double durationDays; 
	/*
	 * by default current GMT time,  with the  possibility 
	 * to chose any other date from calendar
	 * mandatory field
	 */
	Date	timeCreated = Calendar.getInstance().getTime();
	String actionTimeHour;
	String actionTimeMin;
	
	static final String errorMessage	= "Please fill in the mandatory fields !" ;
	static final String successMessage	= "Marketing operation successfully created.";	
	
	
	public MarketingOperationsForm(){
		durationDays = null;
		name = "";
		TimeZone writerTm = TimeZone.getTimeZone(Utils.getWriter().getUtcOffset());
		GregorianCalendar gc = new GregorianCalendar(writerTm);
		timeCreated = gc.getTime();
		actionTimeHour = String.valueOf(gc.get(GregorianCalendar.HOUR_OF_DAY));
		actionTimeMin = String.valueOf(gc.get(GregorianCalendar.MINUTE));
	}
	
	public String save(){
		System.out.println("save");
		
		if(CommonUtil.isParameterEmptyOrNull(name)){
			FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR, errorMessage, null);
			 FacesContext.getCurrentInstance().addMessage("marketingOperationsForm:marketingOperationError1", fm);
		}
		
		if(durationDays==null){
			FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR, errorMessage, null);
			FacesContext.getCurrentInstance().addMessage("marketingOperationsForm:marketingOperationError2", fm);
		}
		
		Calendar cal = Calendar.getInstance();

		cal.setTime(timeCreated);
		cal.add(Calendar.HOUR_OF_DAY, Integer.parseInt(actionTimeHour));
		cal.add(Calendar.MINUTE, Integer.parseInt(actionTimeMin));
		
		try {
			boolean success = MarketingManager.createMarketingOperation(name, durationDays, cal.getTime());
			if(success) {
				FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR, successMessage, null);
				FacesContext.getCurrentInstance().addMessage("marketingOperationsForm:marketingOperationError3", fm);
			}
		} catch (Exception e) {
			e.printStackTrace();
			FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR, e.getMessage(), null);
			FacesContext.getCurrentInstance().addMessage("marketingOperationsForm:marketingOperationError3", fm);
		}
		return null;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public Double getDurationDays() {
		return durationDays;
	}


	public void setDurationDays(Double durationDays) {
		this.durationDays = durationDays;
	}


	public Date getTimeCreated() {
		return timeCreated;
	}


	public void setTimeCreated(Date timeCreated) {
		this.timeCreated = timeCreated;
	}

	public String getActionTimeHour() {
		return actionTimeHour;
	}

	public void setActionTimeHour(String actionTimeHour) {
		this.actionTimeHour = actionTimeHour;
	}

	public String getActionTimeMin() {
		return actionTimeMin;
	}

	public void setActionTimeMin(String actionTimeMin) {
		this.actionTimeMin = actionTimeMin;
	}
	
	
}