package il.co.etrader.backend.mbeans;

import java.io.Serializable;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;

import javax.faces.context.FacesContext;

import il.co.etrader.backend.bl_managers.AdminManager;
import il.co.etrader.backend.bl_managers.MarketingManager;
import il.co.etrader.backend.bl_vos.User;
import il.co.etrader.backend.util.Constants;
import il.co.etrader.backend.util.Utils;
import il.co.etrader.bl_vos.MarketingContent;
import il.co.etrader.util.CommonUtil;


public class ContentsForm implements Serializable {

	private static final long serialVersionUID = 522450867475611126L;

	private String contentName;
	private ArrayList<MarketingContent> list;
	private MarketingContent content;

	public ContentsForm() throws SQLException{
		updateList();
	}

	public String updateList() throws SQLException{
		
	    FacesContext context = FacesContext.getCurrentInstance();
        User u = (User) context.getApplication().createValueBinding(Constants.BIND_USER).getValue(context);
        long writerIdForSkin = Constants.DEFAULT_WRITER_ID_FOR_SKIN;

        if (u.isPartnerMarketingSelected()){ 
        	writerIdForSkin = Utils.getWriter().getWriter().getId();
        }

		list = MarketingManager.getMarketingContents(contentName, writerIdForSkin);

		CommonUtil.setTablesToFirstPage();
		return null;
	}

	/**
	 * Update / insert content
	 * @return
	 * @throws SQLException
	 */
	public String updateInsert() throws SQLException{

		content.setWriterId(AdminManager.getWriterId());

		boolean res = MarketingManager.insertUpdateContent(content);
		if (res == false) {
			return null;
		}
		updateList();
		return Constants.NAV_MARKETING_CONTENTS;
	}

	public int getListSize() {
		return list.size();
	}

	/**
	 * Navigate to edit content
	 * @return
	 * @throws SQLException
	 */
	public String navEdit() {
		return Constants.NAV_MARKETING_CONTENT;
	}

	/**
	 * Navigate to insert new content
	 * @return
	 * @throws SQLException
	 */
	public String navNew() throws SQLException {
		content = new MarketingContent();
		content.setWriterId(AdminManager.getWriterId());
		content.setTimeCreated(new Date());

		return Constants.NAV_MARKETING_CONTENT;
	}

	/**
	 * @return the list
	 */
	public ArrayList getList() {
		return list;
	}

	/**
	 * @param list the list to set
	 */
	public void setList(ArrayList list) {
		this.list = list;
	}

	/**
	 * @return the content
	 */
	public MarketingContent getContent() {
		return content;
	}

	/**
	 * @param medium the medium to set
	 */
	public void setContent(MarketingContent content) {
		this.content = content;
	}

	/**
	 * @return the contentName
	 */
	public String getContentName() {
		return contentName;
	}

	/**
	 * @param contentName the contentName to set
	 */
	public void setContentName(String contentName) {
		this.contentName = contentName;
	}


}
