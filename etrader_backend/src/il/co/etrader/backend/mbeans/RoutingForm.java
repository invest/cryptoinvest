package il.co.etrader.backend.mbeans;

import java.io.Serializable;
import java.util.ArrayList;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import org.apache.log4j.Logger;
import org.primefaces.event.FlowEvent;

import com.anyoption.common.beans.base.Country;
import com.anyoption.common.beans.base.CreditCardType;
import com.anyoption.common.beans.base.Currency;
import com.anyoption.common.beans.base.Skin;

import il.co.etrader.backend.bl_managers.ApplicationData;
import il.co.etrader.backend.bl_managers.ClearingRouteManager;
import il.co.etrader.backend.bl_vos.ClearingRoute;
import il.co.etrader.backend.util.Constants;

/**
 * @author liors
 *
 */
public class RoutingForm implements Serializable {

	private static final Logger logger = Logger.getLogger(RoutingForm.class);
	private static final long serialVersionUID = -5011753706723094515L;
	private ArrayList<ClearingRoute> clearingRoutes;
	private ArrayList<ClearingRoute> filteredRoute;
	private ClearingRoute clearingRoute;
	private ClearingRoute selectedRoute;
	private boolean skip;
	
	public RoutingForm() {
		clearingRoute = new ClearingRoute();
		clearingRoute.setActive(true);
		this.clearingRoutes = new ArrayList<ClearingRoute>();
	}
	
	public void search() {
			
	}
	
	@PostConstruct
    public void init() {
		try {			
			FacesContext context = FacesContext.getCurrentInstance();
			if ((ClearingRoute) FacesContext.getCurrentInstance().getExternalContext().getRequestMap().get("selectedRoute") != null) {
				clearingRoute = (ClearingRoute) FacesContext.getCurrentInstance().getExternalContext().getRequestMap().get("selectedRoute");
			}
			
			if (context.getViewRoot().getViewId().contains(Constants.NAV_CLEARING_ROUTES)) {
				this.clearingRoutes = ClearingRouteManager.getClearingRoutes();
			}			
		} catch (Exception e) {
			logger.error("ERROR! init routingForm", e);
		}
    }
	
	public String navigationAddNewRoute() {	
		return Constants.NAV_CLEARING_ROUTE;
	}	
	
	public String onFlowProcess(FlowEvent event) {
		if(skip) {
            skip = false;
            return Constants.NAV_ROUTE_CONFIRMATION;
        } else {
            return event.getNewStep();
        }
    }
	
	public String addNewRoute() {
		String result = null;
		FacesMessage msg = null;
		try {
			if (clearingRoute.getId() != 0) {
				ClearingRouteManager.updateClearingRoute(clearingRoute);
			} else {
				ClearingRouteManager.insertClearingRoute(clearingRoute);
			}
			
			msg = new FacesMessage("Successful");
		} catch (Exception e) {
			msg = new FacesMessage("ERROR!");
			logger.error("ERROR! tried to add new route", e);
		}
		FacesContext.getCurrentInstance().addMessage(null, msg);
		
		return result;
    }
	
	public String onRouteSelect() {
		String navigate = "";
		if (selectedRoute != null) {
			logger.info("Route Selected: " + selectedRoute.toString());
			FacesContext.getCurrentInstance().getExternalContext().getRequestMap().put("selectedRoute", selectedRoute);
			navigate = Constants.NAV_CLEARING_ROUTE;
		} else {
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Please Select Route"));
		}
		
		return navigate; 
    }
	
	/**
	 * @param currencyId
	 * @return
	 */
	public String getCurrencyNameKey(int currencyId) {
		String currencyName = "";
		Currency currency = ApplicationData.getCurrenciesMap().get(currencyId);
		if (currency != null){
			currencyName = currency.getNameKey();
		}
		return currencyName;	
	}
	
	/**
	 * @param skinId
	 * @return
	 */
	public String getSkinName(int skinId) {		
		String skinName = "";
		Skin skin = ApplicationData.getSkinsMap().get(skinId); 
		if (skin != null) {
			skinName = skin.getName();
		}
		return skinName;
	}
	
	/**
	 * @param countryId
	 * @return
	 */
	public String getCountryName(int countryId) {
		String countryName = "";
		Country country = ApplicationData.getCountriesMap().get(countryId);
		if (country != null) {
			countryName = country.getName();
		}
		return countryName;
	}
	
	/**
	 * @param creditCardTypeId
	 * @return
	 */
	public String getCreditCardTypeName(int creditCardTypeId) {
		String creditCardTypeName = "";
		CreditCardType creditCardType = ApplicationData.getCreditCardTypesMap().get(creditCardTypeId);
		if (creditCardType != null) {
			creditCardTypeName = creditCardType.getDescription();
		}
		return creditCardTypeName;
	}

	/**
	 * @return the clearingRoutes
	 */
	public ArrayList<ClearingRoute> getClearingRoutes() {
		return clearingRoutes;
	}

	/**
	 * @param clearingRoutes the clearingRoutes to set
	 */
	public void setClearingRoutes(ArrayList<ClearingRoute> clearingRoutes) {
		this.clearingRoutes = clearingRoutes;
	}

	/**
	 * @return the filteredRoute
	 */
	public ArrayList<ClearingRoute> getFilteredRoute() {
		return filteredRoute;
	}

	/**
	 * @param filteredRoute the filteredRoute to set
	 */
	public void setFilteredRoute(ArrayList<ClearingRoute> filteredRoute) {
		this.filteredRoute = filteredRoute;
	}

	/**
	 * @return the clearingRoute
	 */
	public ClearingRoute getClearingRoute() {
		return clearingRoute;
	}

	/**
	 * @param clearingRoute the clearingRoute to set
	 */
	public void setClearingRoute(ClearingRoute clearingRoute) {
		this.clearingRoute = clearingRoute;
	}

	/**
	 * @return the skip
	 */
	public boolean isSkip() {
		return skip;
	}

	/**
	 * @param skip the skip to set
	 */
	public void setSkip(boolean skip) {
		this.skip = skip;
	}

	/**
	 * @return the selectedRoute
	 */
	public ClearingRoute getSelectedRoute() {
		return selectedRoute;
	}

	/**
	 * @param selectedRoute the selectedRoute to set
	 */
	public void setSelectedRoute(ClearingRoute selectedRoute) {
		this.selectedRoute = selectedRoute;
	}
}
