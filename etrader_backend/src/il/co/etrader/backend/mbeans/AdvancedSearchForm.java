package il.co.etrader.backend.mbeans;

import java.io.Serializable;
import java.sql.SQLException;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import il.co.etrader.bl_managers.ContactsManager;
import il.co.etrader.bl_vos.Contact;
import il.co.etrader.util.CommonUtil;

public class AdvancedSearchForm implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = -3938004430373350488L;
	private Long contactId;
	private long tmpContactId;
	private String email;
	private String tmpEmail;
	private boolean isContactByEmail;
	private Contact contact = null;

	public String searchContacts() throws SQLException {

		FacesContext context = FacesContext.getCurrentInstance();
		if (null != contactId) {
			contact = ContactsManager.getContactByID(contactId);
		}else {
			if (!CommonUtil.isParameterEmptyOrNull(email)) {
				contact = ContactsManager.getContactByEmail(email);
			}
		}
		if (null == contact) {
			tmpContactId = 0;
			FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_INFO, CommonUtil.getMessage("contacts.emptylist", null), null);
			context.addMessage(null, fm);
		} else {
			tmpContactId = contact.getId();
			isContactByEmail = contact.isContactByEmail();
		}

		return null;
	}

	public String updateContact() throws SQLException {
		FacesContext context = FacesContext.getCurrentInstance();
		contact.setContactByEmail(isContactByEmail);
		ContactsManager.updateDuplicatesContactsByEmail(contact);
		tmpContactId = 0;
		contact = null;
		FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_INFO, CommonUtil.getMessage("general.update.success", null), null);
		context.addMessage(null, fm);
		return null;
	}

	public Long getContactId() {
		return contactId;
	}
	public void setContactId(Long contactId) {
		this.contactId = contactId;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}

	public boolean isContactByEmail() {
		return isContactByEmail;
	}

	public void setContactByEmail(boolean isContactByEmail) {
		this.isContactByEmail = isContactByEmail;
	}

	public long getTmpContactId() {
		return tmpContactId;
	}

	public void setTmpContactId(long tmpContactId) {
		this.tmpContactId = tmpContactId;
	}

	public String getTmpEmail() {
		return tmpEmail;
	}

	public void setTmpEmail(String tmpEmail) {
		this.tmpEmail = tmpEmail;
	}

}
