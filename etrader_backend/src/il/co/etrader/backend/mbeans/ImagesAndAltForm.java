package il.co.etrader.backend.mbeans;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Serializable;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.faces.model.SelectItem;

import org.apache.commons.io.IOUtils;
import org.apache.myfaces.custom.fileupload.UploadedFile;

import il.co.etrader.backend.cms.CmsDataObjects;
import il.co.etrader.backend.cms.CmsGuiBindingsManager;
import il.co.etrader.backend.cms.common.PropertyTypeEnum;
import il.co.etrader.util.CommonUtil;

public class ImagesAndAltForm implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 6354019777027684717L;
	private String page;
	private String  type;
	private String  key;
	private String  lang;
	private int skinId;
	private String image;
	private String editValue;
	private boolean uploadFormShow=false;
	private boolean editFormShow=false;
	private boolean previewBtnDisable=false;
	private boolean rollbackBtnDisable=false;
	private boolean commitBtnDisable=false;
	private boolean tableFilterShow=true;
	private boolean selectedTableShow=false;
	private CmsDataObjects cmsDataObj;
	private ArrayList<SelectItem> pageList;
	private ArrayList<SelectItem> typeList;
	private ArrayList<SelectItem> keyList;
	private ArrayList<SelectItem> langList;
	private String rowsByMenu ="10";
	private ArrayList<CmsDataObjects> objectsList;
	private UploadedFile upFile;
	private boolean rendSuccess=false;
	private boolean rendFailure=false;
	private String cvsMsg;
	private PropertyTypeEnum typeEnum;
	CmsGuiBindingsManager manager;

	ArrayList<SelectItem> pageListtmp = new ArrayList<SelectItem>();
	ArrayList<SelectItem> keyListtmp = new ArrayList<SelectItem>();
	ArrayList<SelectItem> langListtmp = new ArrayList<SelectItem>();
    ArrayList<CmsDataObjects> objectsListtmp= new ArrayList<CmsDataObjects>();
    ArrayList<CmsDataObjects> selectedObjectsList= new ArrayList<CmsDataObjects>();
	
	public ImagesAndAltForm() throws SQLException{		
	  manager = new CmsGuiBindingsManager();
	}

	public String EditAlt(){
		cmsDataObj.setType(typeEnum.ALT_TEXT);
		if(manager.edit(cmsDataObj)){
			refresh();
			setUploadFormShow(false);
			setEditFormShow(true);
		    setEditValue(cmsDataObj.getValue());
		    setCvsMsg(manager.getCvsMessage());
		    setPRCBtnDisable(true, true, false);
		    setSelectedObjectsList(cmsDataObj);
		    setTableShow(false,true);
		}else{
			setCvsMsg(manager.getCvsMessage());
			setUploadFormShow(false);
			setEditFormShow(false);
			setTableShow(true,false);
		}
		return null;
	}
	
	public String EditImg(){
		refresh();
		cmsDataObj.setType(typeEnum.IMAGE);
		if(manager.editImg(cmsDataObj)){
			setUploadFormShow(true);
			setEditFormShow(false);
			setPRCBtnDisable(false, true, false);
			setCvsMsg(manager.getCvsMessage());
			setSelectedObjectsList(cmsDataObj);
			setTableShow(false,true);
		}else{
			setCvsMsg(manager.getCvsMessage());
			setUploadFormShow(false);
			setEditFormShow(false);
			setTableShow(true,false);
		}		
		return null;
	}
	
	public String Search(){
		setUploadFormShow(false);
		setEditFormShow(false);
		setObjectsList(manager.search(skinId, page, PropertyTypeEnum.ALT_TEXT,key));
		setEditValue("");
		setCvsMsg("");
		CommonUtil.setTablesToFirstPage();
		return null;		
	}
	
	public String Commit(){
		setUploadFormShow(false);
		setEditFormShow(false);
		manager.commit(cmsDataObj);
		setCvsMsg(manager.getCvsMessage());
		setTableShow(true,false);
		return null;
  	}
	
   public String Rollback(){
		setUploadFormShow(false);
		setEditFormShow(false);
		manager.rollback(cmsDataObj);
		setCvsMsg(manager.getCvsMessage());
		setObjectsList(manager.search(skinId, page, PropertyTypeEnum.ALT_TEXT,key));
		setTableShow(true,false);
		return null;
	}

   public String Preview() throws Exception {	
	   setPRCBtnDisable(true, true, true); 
	   if(cmsDataObj.getType().equals(PropertyTypeEnum.ALT_TEXT)){
	    cmsDataObj.setValue(getEditValue());
	   }
	   manager.preview(cmsDataObj);
	   setCvsMsg(manager.getCvsMessage());
	   setTableShow(false,true);
	   return null;
    }
   
   public String upload() throws IOException{
        File fileN=null;
        OutputStream outStr=null;
        
		try {
			String dtFile= manager.getUploadImagePath(); 
			fileN = new File(dtFile);

			InputStream stream2 = upFile.getInputStream();
			outStr = new FileOutputStream(fileN);

			byte[] buf = new byte[1024];
			int len;
			while ((len = stream2.read(buf)) > 0){
				outStr.write(buf, 0, len);
			}
			stream2.close();
			outStr.close();

			rendSuccess=true;
			rendFailure=false;
			System.out.println("File Upload Successful.");
			setPRCBtnDisable(true, true, false);
			return null;
		}
		catch (Exception ioe) {
			System.out.println("File Upload Unsuccessful: "+ioe.toString());
			rendSuccess=false;
			rendFailure=true;
			return null;	
		}
		finally {
			IOUtils.closeQuietly(outStr);
		}
	}
   
	private void refresh(){
		setObjectsList(manager.search(skinId, page, PropertyTypeEnum.ALT_TEXT,key));
		ArrayList<CmsDataObjects> tmpList=getObjectsList();
		for(CmsDataObjects listItem : tmpList ){
			if(listItem.getKey().equals(cmsDataObj.getKey()) && listItem.getPage().equals(cmsDataObj.getPage())
					&& (listItem.getSkin()==cmsDataObj.getSkin()) && (listItem.getType()==cmsDataObj.getType())){
				cmsDataObj=listItem;
			}			
		}
	}

	public ArrayList<CmsDataObjects> getObjectsList() {
		return objectsList;
	}

	public void setObjectsList(ArrayList<CmsDataObjects> objectsList) {
		this.objectsList = objectsList;
	}
	
	public ArrayList<CmsDataObjects> getSelectedObjectsList() {
		return selectedObjectsList;
	}

	public void setSelectedObjectsList(CmsDataObjects selectedObjects) {
		selectedObjectsList.clear();
		selectedObjectsList.add(selectedObjects);
	}

	public String getRowsByMenu() {
		return rowsByMenu;
	}

	public void setRowsByMenu(String rowsByMenu) {
		this.rowsByMenu = rowsByMenu;
	}

	public String getPage() {
		return page;
	}

	public void setPage(String page) {
		this.page = page;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getLang() {
		return lang;
	}

	public void setLang(String lang) {
		this.lang = lang;
	}
	
	public int getSkinId() {
		return skinId;
	}

	public void setSkinId(int skinId) {
		this.skinId = skinId;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}
	
	public String getEditValue() {
		return editValue;
	}

	public void setEditValue(String editValue) {
		this.editValue = editValue;
	}
	
	public boolean getUploadFormShow(){
		return uploadFormShow;
	}

	public void setEditFormShow(boolean editFormShow){
		this.editFormShow = editFormShow;
	}
	
	public boolean getEditFormShow(){
		return editFormShow;
	}

	public void setUploadFormShow(boolean uploadFormShow){
		this.uploadFormShow = uploadFormShow;
	}
	
	public void setPreviewBtnDisable(boolean previewBtnDisable){
		this.previewBtnDisable = previewBtnDisable;
	}
	
	public boolean getPreviewBtnDisable(){
		return previewBtnDisable;
	}
	
	public void setRollbackBtnDisable(boolean rollbackBtnDisable){
		this.rollbackBtnDisable = rollbackBtnDisable;
	}
	
	public boolean getRollbackBtnDisable(){
		return rollbackBtnDisable;
	}
	
	public void setCommitBtnDisable(boolean commitBtnDisable){
		this.commitBtnDisable = commitBtnDisable;
	}
	
	public boolean getCommitBtnDisable(){
		return commitBtnDisable;
	}
	
	public void setTableFilterShow(boolean tableFilterShow){
		this.tableFilterShow = tableFilterShow;
	}
	
	public boolean getTableFilterShow(){
		return tableFilterShow;
	}
	
	public void setSelectedTableShow(boolean selectedTableShow){
		this.selectedTableShow = selectedTableShow;
	}
	
	public boolean getSelectedTableShow(){
		return selectedTableShow;
	}
	
	public CmsDataObjects getCmsDataObj() {
		return cmsDataObj;
	}

	public void setCmsDataObj(CmsDataObjects cmsDataObj) {
		this.cmsDataObj = cmsDataObj;
	}

	public ArrayList<SelectItem> getPageList() {
		pageListtmp.clear();
		pageListtmp=manager.getPageList();
		return pageListtmp;
	}

	public void setPageList(ArrayList<SelectItem> pageList) {
		this.pageList = pageList;
	}

	public ArrayList<SelectItem> getTypeList() {
		ArrayList<SelectItem> tempTypeList = new ArrayList<SelectItem>();
		return tempTypeList;
	}

	public void setTypeList(ArrayList<SelectItem> typeList) {
		this.typeList = typeList;
	}

	public ArrayList<SelectItem> getKeyList() {
		keyListtmp.clear();
		keyListtmp=manager.getKeyList(true);
		return keyListtmp;
	}

	public void setKeyList(ArrayList<SelectItem> keyList) {
		this.keyList = keyList;
	}

	public ArrayList<SelectItem> getLangList() {
		langListtmp= manager.getSkinList();
		return langListtmp;
	}

	public void setLangList(ArrayList<SelectItem> langList) {
		this.langList = langList;
	}
	
	public UploadedFile getUpFile(){
		return upFile;
	}

	public void setUpFile(UploadedFile upFile){
		this.upFile = upFile;
	}

	public boolean getRendSuccess(){
		return rendSuccess;
	}

	public void setRendSuccess(boolean rendSuccess){
		this.rendSuccess = rendSuccess;
	}

	public boolean getRendFailure(){
		return rendFailure;
	}

	public void setRendFailure(boolean rendFailure){
		this.rendFailure = rendFailure;
	}
	
	public int getListSize() {
		if (this.objectsList == null){
			return 0;
		}
		return this.objectsList.size();
	}
	
	public void setCvsMsg(String cvsMsg) {
		this.cvsMsg = cvsMsg;
	}
	
	public String getCvsMsg() {
		return cvsMsg;
	}
	
	private void setPRCBtnDisable(boolean previewBtn,boolean rollbackBtn,boolean commitBtn){
		setPreviewBtnDisable(previewBtn);
		setRollbackBtnDisable(rollbackBtn);
		setCommitBtnDisable(commitBtn);		
	}
	
	private void setTableShow(boolean tableFilters,boolean selectedTable){
		setTableFilterShow(tableFilters);
		setSelectedTableShow(selectedTable);
	}
}
