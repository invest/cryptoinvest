package il.co.etrader.backend.mbeans;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.GregorianCalendar;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.CopyopUserCopy;
import com.anyoption.common.beans.base.Skin;
import com.anyoption.common.util.ConstantsBase;
import com.copyop.common.dto.base.CopyConfig;
import com.copyop.common.managers.ProfileLinksManager;
import com.copyop.common.managers.ProfileManager;

import il.co.etrader.backend.bl_vos.User;
import il.co.etrader.backend.util.Constants;
import il.co.etrader.backend.util.Utils;
import il.co.etrader.util.CommonUtil;

/**
 * @author liors
 *
 */
public class CopyopUserCopyForm implements Serializable {
	
	private static final long serialVersionUID = -6788729175675130919L;
	private static final Logger logger = Logger.getLogger(CopyopUserCopyForm.class);
	protected ArrayList<CopyopUserCopy> userCopyList;
	private CopyopUserCopy userCopy;
	protected User user;

	//Filters
	protected Date from;
	protected long userId;
	protected int skinId;
	private ArrayList<SelectItem> selectStillCopy = new ArrayList<SelectItem>();
	private ArrayList<SelectItem> selectFrozen = new ArrayList<SelectItem>();
	protected int isStillCopy;
	protected int isFrozen;
	
	//Sort
	private String sortColumn;
	private String prevSortColumn;
    private boolean sortAscending = false;
    private boolean activateAscending;
	
	public CopyopUserCopyForm() {
		setUser(Utils.getUser());		
		GregorianCalendar gc = new GregorianCalendar();			
		gc.add(Calendar.YEAR, -1);		
		setFrom(gc.getTime());
		skinId = Skin.SKINS_ALL_VALUE;
		userId = ConstantsBase.FILTER_ALL;
		userCopyList = new ArrayList<CopyopUserCopy>();
		isStillCopy = ConstantsBase.FILTER_ALL;
		selectStillCopy.add(new SelectItem(new Long(ConstantsBase.FILTER_ALL), CommonUtil.getMessage("general.all", null)));
		selectStillCopy.add(new SelectItem(new Long(ConstantsBase.FILTER_YES), CommonUtil.getMessage("general.yes", null)));
		selectStillCopy.add(new SelectItem(new Long(ConstantsBase.FILTER_NO), CommonUtil.getMessage("general.no", null)));
		isFrozen = ConstantsBase.FILTER_YES;
		selectFrozen.add(new SelectItem(new Long(ConstantsBase.FILTER_YES), CommonUtil.getMessage("general.yes", null)));
		selectFrozen.add(new SelectItem(new Long(ConstantsBase.FILTER_NO), CommonUtil.getMessage("general.no", null)));		
		setSortColumn("");
		setPrevSortColumn("");
		activateAscending = false;
	}
	
	protected void CheckIfEmptyMessage() {
		if (this.userCopyList == null || getListSize() <= 0) {
			FacesContext context = FacesContext.getCurrentInstance();
			String[] params = new String[1];
			params[0] = String.valueOf(ConstantsBase.MAXIMUM_ORACLE_IN_CLAUSE);
			String msg = CommonUtil.getMessage("copyop.copy.empty", params, Utils.getWriterLocale(context));
			FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_INFO, msg, null);
			context.addMessage(null, fm);			
		} 	
	}
	
	public String delete(CopyopUserCopy copyopUserCopy) {
		//TODO BUG in deleteCopyProfileLink!!!
		ProfileLinksManager.deleteCopyProfileLink(	user.getId(),
													new CopyConfig(user.getId(), copyopUserCopy.getUserId(), copyopUserCopy.getInvCount(),
																	copyopUserCopy.getAmount(), copyopUserCopy.getAssets()),
													user.getClassId() == ConstantsBase.USER_CLASS_TEST,
													ProfileManager.isFrozenProfile(copyopUserCopy.getUserId())); // TODO: why
																													// deleteCopyProfileLink
																													// don't return
																													// indication of
																													// not/success?
		FacesContext context = FacesContext.getCurrentInstance();
		String msg = CommonUtil.getMessage("Delete should been success", null, Utils.getWriterLocale(context)); //TODO msg prop
		FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_INFO, msg, null);
		context.addMessage(null, fm);
			
		return null;
	}
	
	/**
	 * Sort list
	 * @return
	 */
	public String sortList() {
		if (activateAscending){
			if (getSortColumn().equals(getPrevSortColumn())) {
				sortAscending = !sortAscending;
			} else {
				sortAscending = true;
			}
		}

		activateAscending = true;
		setPrevSortColumn(getSortColumn());

		if (getSortColumn().equals("nickName")) {
			Collections.sort(userCopyList, new NicknameComparator(sortAscending));
		}
		
		if (getSortColumn().equals("copyopTradingProfit")) {
			Collections.sort(userCopyList, new TradingProfitComparator(sortAscending));
		}
		
		if (getSortColumn().equals("copyopInvLeftCopy")) {
			Collections.sort(userCopyList, new InvLeftCopy(sortAscending));
		}

		return null;
	}
	
	private class NicknameComparator implements Comparator {
		private boolean ascending;
		public NicknameComparator(boolean asc) {
			ascending = asc;
		}
   	    public int compare(Object o1, Object o2) {
   	    	CopyopUserCopy userCopy1 = (CopyopUserCopy)o1;
   	    	CopyopUserCopy userCopy2 = (CopyopUserCopy)o2;
	 		if (ascending){
	 			return userCopy1.getNickname().compareTo(userCopy2.getNickname());
	 		}
	 		return -userCopy1.getNickname().compareTo(userCopy2.getNickname());
		}
  	 }
	
	private class TradingProfitComparator implements Comparator {
		private boolean ascending;
		public TradingProfitComparator(boolean asc) {
			ascending = asc;
		}
   	    public int compare(Object o1, Object o2) {
   	    	CopyopUserCopy userCopy1 = (CopyopUserCopy)o1;
   	    	CopyopUserCopy userCopy2 = (CopyopUserCopy)o2;
	 		if (ascending){
	 			return new Long(userCopy1.getCopyTradingProfit()).compareTo(new Long(userCopy2.getCopyTradingProfit()));
	 		}
	 		return -new Long(userCopy1.getCopyTradingProfit()).compareTo(new Long(userCopy2.getCopyTradingProfit()));
		}
  	 }
	
	private class InvLeftCopy implements Comparator {
		private boolean ascending;
		public InvLeftCopy(boolean asc) {
			ascending = asc;
		}
   	    public int compare(Object o1, Object o2) {
   	    	CopyopUserCopy userCopy1 = (CopyopUserCopy)o1;
   	    	CopyopUserCopy userCopy2 = (CopyopUserCopy)o2;
	 		if (ascending){
	 			return new Integer(userCopy1.getInvCountReached()).compareTo(new Integer(userCopy2.getInvCountReached()));
	 		}
	 		return -new Integer(userCopy1.getInvCountReached()).compareTo(new Integer(userCopy2.getInvCountReached()));
		}
  	 }
		
	public String getInit() {
		return Constants.EMPTY_STRING;
	}
	
	public int getListSize() {
		return userCopyList.size();
	}

	/**
	 * @return the user
	 */
	public User getUser() {
		return user;
	}

	/**
	 * @param user the user to set
	 */
	public void setUser(User user) {
		this.user = user;
	}

	/**
	 * @return the from
	 */
	public Date getFrom() {
		return from;
	}

	/**
	 * @param from the from to set
	 */
	public void setFrom(Date from) {
		this.from = from;
	}

	/**
	 * @return the skinId
	 */
	public int getSkinId() {
		return skinId;
	}

	/**
	 * @param skinId the skinId to set
	 */
	public void setSkinId(int skinId) {
		this.skinId = skinId;
	}

	/**
	 * @return the userId
	 */
	public long getUserId() {
		return userId;
	}

	/**
	 * @param userId the userId to set
	 */
	public void setUserId(long userId) {
		this.userId = userId;
	}

	/**
	 * @return the isStillCopy
	 */
	public int getIsStillCopy() {
		return isStillCopy;
	}

	/**
	 * @param isStillCopy the isStillCopy to set
	 */
	public void setIsStillCopy(int isStillCopy) {
		this.isStillCopy = isStillCopy;
	}

	/**
	 * @return the isFrozen
	 */
	public int getIsFrozen() {
		return isFrozen;
	}

	/**
	 * @param isFrozen the isFrozen to set
	 */
	public void setIsFrozen(int isFrozen) {
		this.isFrozen = isFrozen;
	}

	/**
	 * @return the selectStillCopy
	 */
	public ArrayList<SelectItem> getSelectStillCopy() {
		return selectStillCopy;
	}

	/**
	 * @param selectStillCopy the selectStillCopy to set
	 */
	public void setSelectStillCopy(ArrayList<SelectItem> selectStillCopy) {
		this.selectStillCopy = selectStillCopy;
	}

	/**
	 * @return the selectFrozen
	 */
	public ArrayList<SelectItem> getSelectFrozen() {
		return selectFrozen;
	}

	/**
	 * @param selectFrozen the selectFrozen to set
	 */
	public void setSelectFrozen(ArrayList<SelectItem> selectFrozen) {
		this.selectFrozen = selectFrozen;
	}

	/**
	 * @return the userCopyingList
	 */
	public ArrayList<CopyopUserCopy> getUserCopyingList() {
		return userCopyList;
	}

	/**
	 * @param userCopyingList the userCopyingList to set
	 */
	public void setUserCopyingList(ArrayList<CopyopUserCopy> userCopyingList) {
		this.userCopyList = userCopyingList;
	}

	/**
	 * @return the userCopyList
	 */
	public ArrayList<CopyopUserCopy> getUserCopyList() {
		return userCopyList;
	}

	/**
	 * @param userCopyList the userCopyList to set
	 */
	public void setUserCopyList(ArrayList<CopyopUserCopy> userCopyList) {
		this.userCopyList = userCopyList;
	}

	/**
	 * @return the userCopy
	 */
	public CopyopUserCopy getUserCopy() {
		return userCopy;
	}

	/**
	 * @param userCopy the userCopy to set
	 */
	public void setUserCopy(CopyopUserCopy userCopy) {
		this.userCopy = userCopy;
	}

	/**
	 * @return the sortColumn
	 */
	public String getSortColumn() {
		return sortColumn;
	}

	/**
	 * @param sortColumn the sortColumn to set
	 */
	public void setSortColumn(String sortColumn) {
		this.sortColumn = sortColumn;
	}

	/**
	 * @return the prevSortColumn
	 */
	public String getPrevSortColumn() {
		return prevSortColumn;
	}

	/**
	 * @param prevSortColumn the prevSortColumn to set
	 */
	public void setPrevSortColumn(String prevSortColumn) {
		this.prevSortColumn = prevSortColumn;
	}
}
