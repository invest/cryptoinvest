//package il.co.etrader.backend.mbeans;
//
//import java.io.Serializable;
//import java.sql.SQLException;
//import java.util.ArrayList;
//
//import javax.faces.application.FacesMessage;
//import javax.faces.context.FacesContext;
//import javax.faces.event.ValueChangeEvent;
//
////import org.apache.log4j.Logger;
//
////import com.anyoption.common.beans.BinaryZeroOneHundred;
//import com.anyoption.common.beans.Market;
////import com.anyoption.common.jms.BackendLevelsCache;
//
//import il.co.etrader.backend.bl_managers.MarketingManager;
////import il.co.etrader.backend.bl_managers.OpportunityQuotes0100Manager;
////import il.co.etrader.backend.bl_vos.OpportunityQuotes0100;
//import il.co.etrader.backend.util.Constants;
////import il.co.etrader.backend.util.Utils;
////import il.co.etrader.bl_managers.MarketsManager;
//import il.co.etrader.util.CommonUtil;
//
//public class ZeroOneHundredForm implements Serializable{
//
////	private static final Logger log = Logger.getLogger(ZeroOneHundredForm.class);
//
//	private static final long serialVersionUID = 1L;
//	private ArrayList list;
//	private Market market;
////	private Market marketOld;
//	private long marketId;
//	private String generalMessage;
//
//	public ZeroOneHundredForm() {
//		try{
//		list = MarketingManager.getAllZeroOneHunderdSI();
//		//zeroOneHundred = new BinaryZeroOneHundred(OpportunitiesManagerBase.getOppTemplateQuote());
//		} catch (SQLException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//	}
//
//	public String update() throws Exception{
//		FacesContext context = FacesContext.getCurrentInstance();
////		BackendLevelsCache levelsCache = (BackendLevelsCache) context.getExternalContext().getApplicationMap().get("levelsCache");
////		if (levelsCache.binary0100market(market.getFeedName(), market.getZeroOneHundred().toString())) {
////			MarketsManager.setMarketAndOppQuote(market.getId(), market.getZeroOneHundred().toString());
////			try {
////				OpportunityQuotes0100 vo = new OpportunityQuotes0100(null, market.getId(), Constants.SCREEN_TYPE_DATIKA, Utils.getWriter().getWriter().getId(), market.getZeroOneHundred(), market.getZeroOneHundred().getDiffrent(marketOld.getZeroOneHundred()));
////				OpportunityQuotes0100Manager.insertOpportunityQuotes0100(vo);
////			} catch (Exception e) {
////				log.debug("cant insert OpportunityQuotes0100", e);
////			}
////			return Constants.NAV_ZERO_ONE_HUNDRED_UPDATE_BACK;
////		}
//		generalMessage = CommonUtil.getMessage("error.update.zero.one.hundred", null);
//		FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR, generalMessage, null);
//		context.addMessage(null, fm);
//		return null;
//	}
//
//	public void updateChange(ValueChangeEvent event) {
//		marketId = (Long)event.getNewValue();
//		try {
//			if (marketId == Constants.ALL_FILTER_ID) {
//				list = MarketingManager.getAllZeroOneHunderdSI();
//			} else {
//				list = MarketingManager.getZeroOneHunderdById(marketId);
//			}
//		} catch (SQLException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//	}
//
//	public String navEdit() {
//		return Constants.NAV_ZERO_ONE_HUNDRED_UPDATE;
//	}
//
//	public int getListSize() {
//		return list.size();
//	}
//
//	public ArrayList getList() {
//		return list;
//	}
//
//	public void setList(ArrayList list) {
//		this.list = list;
//	}
//
//	public Market getMarket() {
//		return market;
//	}
//
//	public void setMarket(Market market) {
//		this.market = market;
////		this.marketOld = new Market();
////		this.marketOld.setZeroOneHundred(new BinaryZeroOneHundred(market.getZeroOneHundred().toString()));
//	}
//
//	public long getMarketId() {
//		return marketId;
//	}
//
//	public void setMarketId(long marketId) {
//		this.marketId = marketId;
//	}
//
//	public String getGeneralMessage() {
//		return generalMessage;
//	}
//
//	public void setGeneralMessage(String generalMessage) {
//		this.generalMessage = generalMessage;
//	}
//}
