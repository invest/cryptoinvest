package il.co.etrader.backend.mbeans;


import java.io.Serializable;
import java.math.BigDecimal;
import java.math.MathContext;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.MarketGroup;
import com.anyoption.common.enums.SkinGroup;
import com.anyoption.common.jms.BackendLevelsCache;
import com.anyoption.common.managers.MarketsManagerBase;
import com.anyoption.common.service.CommonJSONService;
import com.anyoption.common.service.requests.InstantSpreadChangeMethodRequest;
import com.anyoption.common.service.results.MethodResult;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import il.co.etrader.backend.bl_managers.TraderManager;
import il.co.etrader.backend.bl_vos.User;
import il.co.etrader.backend.bl_vos.WriterWrapper;
import il.co.etrader.backend.dao_managers.MarketsManager;
import il.co.etrader.backend.util.Constants;
import il.co.etrader.bl_managers.ApplicationDataBase;
import il.co.etrader.bl_managers.SkinsManagerBase;
import il.co.etrader.util.CommonUtil;
import il.co.etrader.util.ConstantsBase;


public class TraderToolPublishForm implements Serializable {

	private static final long serialVersionUID = -7907475328926762537L;

	private static final Logger log = Logger.getLogger(TraderToolPublishForm.class);

	private List<Long> scheduleds;
	private String marketName;
	private HashMap<Integer, ArrayList<Long>> unavailableMarketsPerSkin;
	private String marketsByGroups;
	private long skinId = 0;
	static BackendLevelsCache levelsCache;

	public TraderToolPublishForm() {
		scheduleds = new ArrayList<Long>();
		scheduleds.add(new Long(0));
		
		FacesContext context = FacesContext.getCurrentInstance();
		levelsCache = getLevelsCache(context);
		HttpServletRequest request = (HttpServletRequest)context.getExternalContext().getRequest();
		WriterWrapper writer = (WriterWrapper) request.getSession().getAttribute("writer");
		ArrayList<SelectItem> skinsSI = writer.getSkinsSI();
		unavailableMarketsPerSkin = new HashMap<Integer, ArrayList<Long>>();
		
		for (int i = 0; i < skinsSI.size(); i++) {
			try {
				SelectItem si = skinsSI.get(i);
				Integer key = new Integer(si.getValue().toString());
				unavailableMarketsPerSkin.put(key, SkinsManagerBase.getUnvisibleMarketsPerSkin(key));
			} catch (SQLException e) {
				log.debug(e.getMessage());
			}
		}
	}
	
	/**
	 * Gets the levelcache from the context
	 * @param context
	 * @return levelcache
	 */
	private static BackendLevelsCache getLevelsCache(FacesContext context) {
		return (BackendLevelsCache) context.getExternalContext().getApplicationMap().get("levelsCache");
	}

	/**
	 * @return the scheduleds
	 */
	public List<Long> getScheduleds() {
		return scheduleds;
	}

	/**
	 * @param scheduleds the scheduleds to set
	 */
	public void setScheduleds(List<Long> scheduleds) {
		this.scheduleds = scheduleds;
	}

	public ArrayList<SelectItem> getScheduledListSI() {
		FacesContext context = FacesContext.getCurrentInstance();
		ApplicationDataBase ap = (ApplicationDataBase)context.getApplication().createValueBinding(ConstantsBase.BIND_APPLICATION_DATA).getValue(context);
		ArrayList<SelectItem> si = new ArrayList<SelectItem>();
		si.add(new SelectItem(new Long(0),"All"));
		si.addAll(ap.getScheduledListSI());
		return si;
	}

	/**
	 * @return the marketName
	 */
	public String getMarketName() {
		return marketName;
	}

	/**
	 * @param marketName the marketName to set
	 */
	public void setMarketName(String marketName) {
		this.marketName = marketName;
	}

	public String getLsServer() {
        return CommonUtil.getProperty("lightstreamer.url");
    }

	public String getLsPort() {
        return CommonUtil.getProperty("lightstreamer.port");
    }

	public String getLsDomain() {
        return CommonUtil.getProperty("lightstreamer.domain");
    }

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public long getSkinId() {
		return skinId;
	}

	public void setSkinId(long skinId) {
		this.skinId = skinId;
	}

	public HashMap<Integer, ArrayList<Long>> getUnavailableMarketsPerSkin() {
		return unavailableMarketsPerSkin;
	}

	public void setUnavailableMarketsPerSkin(
			HashMap<Integer, ArrayList<Long>> unavailableMarketsPerSkin) {
		this.unavailableMarketsPerSkin = unavailableMarketsPerSkin;
	}
	
	//{1:[1, n], 1:[1, n], n:[k, m]}
	public String getJSList(){
		String s= "{";
		Iterator it = unavailableMarketsPerSkin.entrySet().iterator();
	    while (it.hasNext()) {
	        Map.Entry pairs = (Map.Entry)it.next();
	        s+=(pairs.getKey() + " : " + pairs.getValue().toString());
	        //it.remove(); // avoids a ConcurrentModificationException
	        s+=",";
	    }
	    s += "}";
	    return s;
	}
	static String convertStreamToString(java.io.InputStream is) {
	    java.util.Scanner s = new java.util.Scanner(is, "UTF-8").useDelimiter("\\A");
	    return s.hasNext() ? s.next() : "";
	}
	
	/**
	 * check if the spread is in the limits
	 * @param user
	 * @param spread
	 * @return
	 */
	public static boolean checkSpreadLimit(User user, double spread, long oppId) {

	    double btraderLimit;

		try {
			btraderLimit = TraderManager.getNightShiftLimitByOppId(oppId);
		} catch (SQLException e) {
			log.warn("cant get night shift limit use defualt " + Constants.TRADERTOOL_BTRADER_SPREAD_LIMIT);
			btraderLimit = Constants.TRADERTOOL_BTRADER_SPREAD_LIMIT;
		}
		if (((user.isHasASTraderRole() || user.isHasSTraderRole()) && Math.abs(spread) <= Constants.TRADERTOOL_ASTRADER_SPREAD_LIMIT) ||
				(user.isHasTraderRole() && !user.isHasBTraderRole() && Math.abs(spread) <= Constants.TRADERTOOL_TRADER_SPREAD_LIMIT) ||
				(user.isHasBTraderRole() && Math.abs(spread) <= btraderLimit)) {
			return true;
		}
		return false;
	}
	
	public static MethodResult spreadAction(InstantSpreadChangeMethodRequest request, User user) {

		MethodResult result = new MethodResult();
	
		long oppID = request.getOppID();
		long marketID = request.getMarketID();
		long writerID = request.getWriterID();
		double currentSpreadAO = request.getCurrentSpreadAO();
		boolean sign = request.isSign();

		double spreadStep = MarketsManagerBase.getMarket(marketID).getInstantSpread();
		double maxSpreadOfStepsPositive = spreadStep * 10;
		double maxSpreadOfStepsNegative = (spreadStep * 10) * (-1);
		
		if(!checkSpreadLimit(user, currentSpreadAO + spreadStep, oppID)) {
		    	log.debug("AO has current spread" + currentSpreadAO + "and there is a limit set for night schift");
			result.setErrorCode(CommonJSONService.ERROR_CODE_BACKEND_NIGHT_SCHIFT_SPREAD_LIMIT_REACHED);
			return result;
		}

		if( ( sign ? (currentSpreadAO + spreadStep > maxSpreadOfStepsPositive) :
		    currentSpreadAO + (spreadStep * (-1)) < maxSpreadOfStepsNegative) ) {
		    log.debug("AO current spread is " + currentSpreadAO + " and with the given step " + spreadStep + 
			    " would go above limit of 10 times MAX allowed steps +-" + maxSpreadOfStepsPositive);
		    result.setErrorCode(CommonJSONService.ERROR_CODE_BACKEND_MAX_MIN_SPREAD_REACHED);
		    return result;
		}

		currentSpreadAO += sign ? spreadStep : (spreadStep * (-1));
		
		BigDecimal shiftParameterET = new BigDecimal(0 / 100, new MathContext(10));
		BigDecimal shiftParameterAO = new BigDecimal(currentSpreadAO / 100, new MathContext(10));
		BigDecimal shiftParameterZH = new BigDecimal(0 / 100, new MathContext(10));
		BigDecimal shiftParameterTR = new BigDecimal(0 / 100, new MathContext(10));

		ArrayList<BigDecimal> shiftParameters = new ArrayList<BigDecimal>();
		shiftParameters.add(shiftParameterET);
		shiftParameters.add(shiftParameterAO);
		shiftParameters.add(shiftParameterZH);
		shiftParameters.add(shiftParameterTR);
		

        	log.debug("Sending Notification for shift change with op: " + oppID + " perc at: " + shiftParameters);
        	if (levelsCache.notifyForOpportunityShiftParameterChange(oppID, shiftParameters)) {
        	    log.debug("Level Cache Notified");
        	    try {
        		TraderManager.insertShift(oppID, shiftParameterAO, SkinGroup.ANYOPTION, new Date(), writerID);
        		log.debug("Success to insert shift opp id: " + oppID);
        		result.setErrorCode(CommonJSONService.ERROR_CODE_SUCCESS);
        	    } catch (SQLException e2) {
        		log.debug("shift opp id: " + oppID + " sql problem", e2);
        		result.setErrorCode(CommonJSONService.ERROR_CODE_UNKNOWN);
        	    }
        	} else {
        	    result.setErrorCode(CommonJSONService.ERROR_CODE_UNKNOWN);
        	}
        	return result;
	}

	public String getMarketsByGroups() {
		LinkedHashMap<Long, MarketGroup> markets = new LinkedHashMap<Long, MarketGroup>();
		markets = MarketsManager.getMarketsByGroups();

		
		if(markets != null){
			Gson gson = new GsonBuilder().create();
			marketsByGroups = gson.toJson(markets);
		}
		return marketsByGroups;
	}

	public void setMarketsAndGroups(String marketsByGroups) {
		this.marketsByGroups = marketsByGroups;
	}	
	
}
