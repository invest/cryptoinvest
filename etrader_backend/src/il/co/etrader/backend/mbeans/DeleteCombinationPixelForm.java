package il.co.etrader.backend.mbeans;

import java.io.Serializable;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;

import org.apache.log4j.Logger;

import il.co.etrader.backend.bl_managers.MarketingManager;
import il.co.etrader.backend.bl_vos.User;
import il.co.etrader.backend.util.Constants;
import il.co.etrader.backend.util.Utils;
import il.co.etrader.bl_vos.MarketingCombination;

			 
public class DeleteCombinationPixelForm implements Serializable {

	private static final long serialVersionUID = -3071143379701935372L;
	private static Logger log = Logger.getLogger(DeleteCombinationPixelForm.class);

    protected long sourceId = 0;
    protected long campaignId = 0;
    protected long pixelId = 0 ;
    
    protected ArrayList<MarketingCombination> list = null;
    

	public DeleteCombinationPixelForm() {
	}

	public void reset(){
	    sourceId = 0;
	    campaignId = 0;
	    pixelId = 0 ;
    
	    list = null;
	}
	
    public String search() {
    	list = null;
        FacesContext context = FacesContext.getCurrentInstance();
        pixelId = 0;
        if (sourceId == 0 && campaignId == 0) {
            FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_INFO, "You must choose Campaign or Source", null);
            context.addMessage("deleteCombinationPixelForm:campaignId", fm);
            context.addMessage("deleteCombinationPixelForm:sourceId", fm);
        } else {
        	try {
                User u = (User) context.getApplication().createValueBinding(Constants.BIND_USER).getValue(context);
                long writerIdForSkin = Constants.DEFAULT_WRITER_ID_FOR_SKIN;;
        		
                if (u.isPartnerMarketingSelected()){
                	writerIdForSkin = Utils.getWriter().getWriter().getId();
                }
                
				list = MarketingManager.getCombinationsWithCampaignIDSourceID(campaignId, sourceId, writerIdForSkin);
			} catch (SQLException e) {
				log.debug(e.getMessage());
				e.printStackTrace();
			}
        }
        return null;
    }
    
    public String delete(){
        FacesContext context = FacesContext.getCurrentInstance();
        
    	if(pixelId == 0){
            FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_INFO, "You must choose Pixel or press search to make selection from table !", null);
            context.addMessage("deleteCombinationPixelForm:pixelId", fm);
    	} else {
    		if (sourceId == 0 && campaignId == 0) {
                FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_INFO, "You must choose Campaign or Source", null);
                context.addMessage("deleteCombinationPixelForm:campaignId", fm);
                context.addMessage("deleteCombinationPixelForm:sourceId", fm);
            } else {
            	try{
		            	int result = MarketingManager.deletePixelByCampaignIDorSourceID(pixelId, campaignId, sourceId);
		            	if( result == 0){
		                    FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_INFO, "No pixels were found for selected criteria.", null);
		                    context.addMessage("deleteCombinationPixelForm:deleteId", fm);
		            	} else {
		                    FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_INFO, ""+ result +" pixels were deleted.", null);
		                    context.addMessage("deleteCombinationPixelForm:deleteId", fm);
		            	}
            	}catch (SQLException e) {
    				log.debug(e.getMessage());
    				e.printStackTrace();
                    FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_INFO, "Database error.", null);
                    context.addMessage("deleteCombinationPixelForm:deleteId", fm);
            	}
            	
            }
    	}
    	return null;
    }

    
    public String deleteSelected(){
    	System.out.println("Delete Selected");
		try {
			
	    	for(MarketingCombination mcombination: list) {
	    		for(SelectItem si :mcombination.getPixelsSi()){
	    			if(si.isDisabled()){
							MarketingManager.deletePixelByCombId(mcombination.getId(), (Long)si.getValue());
	    			}
	    		}
	    	}
	    	
	    	FacesContext context = FacesContext.getCurrentInstance();
	    	User u = (User) context.getApplication().createValueBinding(Constants.BIND_USER).getValue(context);
            long writerIdForSkin = Constants.DEFAULT_WRITER_ID_FOR_SKIN;;
    		
            if (u.isPartnerMarketingSelected()){
            	writerIdForSkin = Utils.getWriter().getWriter().getId();
            }
	    	list = MarketingManager.getCombinationsWithCampaignIDSourceID(campaignId, sourceId, writerIdForSkin);
		} catch (SQLException e) {
			e.printStackTrace();
		}


    	return null;
    }


//    public void checkBoxSelected(ValueChangeEvent event) {
//        FacesContext.getCurrentInstance( ).renderResponse( );
//    }


    public void checkBoxSelected(SelectItem pixel) {
    	pixel.setDisabled(!pixel.isDisabled());
     
    }

    
    /**
     * @return the pixelId
     */
    public long getPixelId() {
        return pixelId;
    }

    /**
     * @param pixelId the pixelId to set
     */
    public void setPixelId(long pixelId) {
        this.pixelId = pixelId;
    }

    
    /**
     * @return the sourceId
     */
    public long getSourceId() {
        return sourceId;
    }

    /**
     * @param sourceId the sourceId to set
     */
    public void setSourceId(long sourceId) {
        this.sourceId = sourceId;
    }

    /**
     * @return the campaignId
     */
    public long getCampaignId() {
        return campaignId;
    }

    /**
     * @param campaignId the campaignId to set
     */
    public void setCampaignId(long campaignId) {
        this.campaignId = campaignId;
    }

    /**
     * @return the list
     */
    public ArrayList<MarketingCombination> getList() {
        return list;
    }

    /**
     * @param list the list to set
     */
    public void setList(ArrayList<MarketingCombination> list) {
        this.list = list;
    }

    public int getPixelsSize(int index){
    	return list.get(index).getPixelsSi().size();
    }

    public ArrayList<SelectItem> getPixels(int index){
    	return list.get(index).getPixelsSi();
    }

    
    public long getListSize() {
        return list.size();
    }
    
}