package il.co.etrader.backend.mbeans;

import java.io.Serializable;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import il.co.etrader.backend.bl_managers.ConstantInvestmentsStreamManager;
import il.co.etrader.backend.util.Constants;
import il.co.etrader.bl_managers.ApplicationDataBase;
import il.co.etrader.bl_vos.ConstantInvestmentsStream;
import il.co.etrader.bl_vos.Skins;
import il.co.etrader.util.CommonUtil;
import il.co.etrader.util.ConstantsBase;

public class LiveConstantInvestmentsStreamsForm implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -830859166926231185L;
	private ArrayList<ConstantInvestmentsStream> list = new ArrayList<ConstantInvestmentsStream>();
	private ConstantInvestmentsStream constantInvestmentsStream;
	private String skinsName;
	private Skins skins;
	private String investmentMinAmountTxt;
	
	public LiveConstantInvestmentsStreamsForm() throws SQLException{
		search();
	}
	
	public String search() throws SQLException {
		setList(ConstantInvestmentsStreamManager.getAll() );
		CommonUtil.setTablesToFirstPage();
		return null;
	}
	
	public String edit(){
		return Constants.NAV_CONSTANT_INVESTMENT_STREAM;
	}
	
	public String initValues() throws SQLException {	
		constantInvestmentsStream = new ConstantInvestmentsStream();
		constantInvestmentsStream.setId(0);
		constantInvestmentsStream.setSkinId((long) 0);
		constantInvestmentsStream.setInvestmentMinAmount(0);
		constantInvestmentsStream.setMaxRows(0);
		constantInvestmentsStream.setStreamName("");		
		return Constants.NAV_CONSTANT_INVESTMENT_STREAM;
	}
	
	public String updateInsert() throws SQLException {	
		FacesContext context = FacesContext.getCurrentInstance();
		if(ConstantInvestmentsStreamManager.isExistStreamName(constantInvestmentsStream) && constantInvestmentsStream.getId()==0 ){
			FacesMessage fm = new FacesMessage(
					FacesMessage.SEVERITY_ERROR,
					CommonUtil.getMessage(
							"error.live.exist.stream.name", null),
					null);
			context.addMessage(null, fm);			
			return null;
		}
		
		if (constantInvestmentsStream.getMaxRows() <= 0 || constantInvestmentsStream.getInvestmentMinAmount() <= 0 ){
			FacesMessage fm = new FacesMessage(
					FacesMessage.SEVERITY_ERROR,
					CommonUtil.getMessage(
							"error.live.numbers.greater", null),
					null);
			context.addMessage(null, fm);
			search();
			return null;
		}
		
		boolean res = ConstantInvestmentsStreamManager.updateInsertLiveGlobeCity(constantInvestmentsStream);
		search();
		return Constants.NAV_CONSTANT_INVESTMENTS_STREAMS;	
	}
	/**
	 * @return the list
	 */
	public ArrayList<ConstantInvestmentsStream> getList() {
		return list;
	}
	/**
	 * @param list the list to set
	 */
	public void setList(ArrayList<ConstantInvestmentsStream> list) {
		this.list = list;
	}
	/**
	 * @return the constantInvestmentsStream
	 */
	public ConstantInvestmentsStream getConstantInvestmentsStream() {
		return constantInvestmentsStream;
	}
	/**
	 * @param constantInvestmentsStream the constantInvestmentsStream to set
	 */
	public void setConstantInvestmentsStream(ConstantInvestmentsStream constantInvestmentsStream) {
		this.constantInvestmentsStream = constantInvestmentsStream;
	}
	/**
	 * @return the skinsName
	 */
	public String getSkinsName( long skinId) {
		if (skinId !=0) {
			FacesContext context = FacesContext.getCurrentInstance();
			ApplicationDataBase ap = (ApplicationDataBase)context.getApplication().createValueBinding(ConstantsBase.BIND_APPLICATION_DATA).getValue(context);
			skins=ap.getSkinById(skinId);
			return skins.getName();
		}else {
			return "";
		}
	}
	/**
	 * @param skinsName the skinsName to set
	 */
	public void setSkinsName(String skinsName) {
		this.skinsName = skinsName;
	}
	
	public String getAmountTxt (long amount){
		return CommonUtil.displayAmountForInput(Long.valueOf(amount), false,1);	
	}
	
	public int getListSize() {
		return list.size();
	}

	/**
	 * @return the investmentMinAmountTxt
	 */
	public String getInvestmentMinAmountTxt() {
		return  CommonUtil.displayAmountForInput(Long.valueOf(constantInvestmentsStream.getInvestmentMinAmount()), false,1);		
	}

	/**
	 * @param investmentMinAmountTxt the investmentMinAmountTxt to set
	 */
	public void setInvestmentMinAmountTxt(String investmentMinAmountTxt) {
		   constantInvestmentsStream.setInvestmentMinAmount( CommonUtil.calcAmount(investmentMinAmountTxt)) ;
	}
}
