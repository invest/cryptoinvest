package il.co.etrader.backend.mbeans;


import java.io.IOException;
import java.io.Serializable;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import org.apache.log4j.Logger;

import il.co.etrader.backend.bl_vos.User;
import il.co.etrader.backend.helper.MarketingReportSender;
import il.co.etrader.backend.util.Constants;
import il.co.etrader.backend.util.Utils;
import il.co.etrader.bl_vos.MarketingReport;
import il.co.etrader.util.CommonUtil;

public class MarketingReportForm implements Serializable {

	private static final long serialVersionUID = 1L;
	private static final Logger logger = Logger.getLogger(MarketingReportForm.class);

	private Date startDate;
	private Date endDate;
	private long skinId;
	private long sourceId;
	private ArrayList<MarketingReport> report;
	private long currentWriterId;
	private long marketingCampaignManager;
	private long writerIdForSkin = Constants.DEFAULT_WRITER_ID_FOR_SKIN;
	private boolean isBaseReportByDate;
	private boolean isDpReportByDate;

	public MarketingReportForm() throws SQLException, ParseException{
		GregorianCalendar gc = new GregorianCalendar();
		//set time as 00:00:00
		gc.set(Calendar.HOUR_OF_DAY, 0);
		gc.set(Calendar.MINUTE, 0);
		gc.set(Calendar.SECOND, 0);
		endDate = gc.getTime();
		gc.add(GregorianCalendar.MONTH, -1);
		startDate = gc.getTime();
		skinId = 0;
		sourceId = 0;
		currentWriterId = Utils.getWriter().getWriter().getId();
		
		//set defualt date for arabic partner
	    if ( currentWriterId == Constants.PARTNER_ARABIC_ID){
			String enumDate = CommonUtil.getEnum(Constants.ENUM_PARTNER_ARABIC_DATE_ENUMERATOR,Constants.ENUM_PARTNER_ARABIC_DATE_CODE);
			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
			Date startDateAr = sdf.parse(enumDate);;
			if (startDate.before(startDateAr)){
				startDate = startDateAr;
			}
	    }
	    
	    FacesContext context = FacesContext.getCurrentInstance();
        User u = (User) context.getApplication().createValueBinding(Constants.BIND_USER).getValue(context);        

        if (u.isPartnerMarketingSelected()){ 
        	setWriterIdForSkin(Utils.getWriter().getWriter().getId());
        }
	}


	public String exportMarketingReportToExcel() throws IOException, SQLException, ParseException {
		if(isBaseReportByDate){
		    return exportHtmlTableToExcel(Constants.MARKETING_REPORT_TYPE_BASE_BY_DATE_REPORT,"marketing_report_by_date_");
		}else {
		    return exportHtmlTableToExcel(Constants.MARKETING_REPORT_TYPE_BASIC,"marketing_report_");
		}	    
	}

	public String sendGoogleReportDp() throws IOException, SQLException, ParseException {
		if(isDpReportByDate){
		    return exportHtmlTableToExcel(Constants.MARKETING_REPORT_TYPE_DP_BY_DATE_REPORT,"dp_report_by_date_");
		}else {
		    return exportHtmlTableToExcel(Constants.MARKETING_REPORT_TYPE_GOOGLE_DP,"dp_report_");
		}	    
	}

	public String exportRegLeadsTotalsReportToExcel() throws IOException, SQLException, ParseException {
		return exportHtmlTableToExcel(Constants.MARKETING_REPORT_TYPE_REG_LEADS_TOTAL,"reg_leads_totals_");
	}
	
	public String exportRemarketingReport() throws IOException, SQLException, ParseException {
		return exportHtmlTableToExcel(Constants.MARKETING_REPORT_TYPE_REMARKETING_REPORT,"remarketing_report_");
	}
	
	public String exportHttpRefReport() throws IOException, SQLException, ParseException {
		return exportHtmlTableToExcel(Constants.MARKETING_REPORT_TYPE_HTTP_REF_REPORT,"http_ref_report_");
	}


	public String exportHtmlTableToExcel(int reportType, String reportName) throws IOException, SQLException, ParseException {
		FacesContext context=FacesContext.getCurrentInstance();
		String writersOffset = Utils.getWriter().getUtcOffset();		
		logger.info("Process " + reportName);
	

 		
        SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd");
        String filename = reportName + fmt.format(startDate) + "_" + fmt.format(endDate);

		//set defualt date for arabic partner
	    if ( currentWriterId == Constants.PARTNER_ARABIC_ID){
			String enumDate = CommonUtil.getEnum(Constants.ENUM_PARTNER_ARABIC_DATE_ENUMERATOR,Constants.ENUM_PARTNER_ARABIC_DATE_CODE);
			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
			Date startDateAr = sdf.parse(enumDate);
			if (startDate.before(startDateAr)){
				startDate = startDateAr;
			}
	    }


		if (Constants.MARKETING_REPORT_TYPE_BASIC == reportType ||
				Constants.MARKETING_REPORT_TYPE_REG_LEADS_TOTAL == reportType ||
				    Constants.MARKETING_REPORT_TYPE_REMARKETING_REPORT == reportType ||
				    	Constants.MARKETING_REPORT_TYPE_GOOGLE_DP == reportType || 
				    		Constants.MARKETING_REPORT_TYPE_HTTP_REF_REPORT == reportType ||
				    		    Constants.MARKETING_REPORT_TYPE_BASE_BY_DATE_REPORT == reportType ||
				    		        Constants.MARKETING_REPORT_TYPE_DP_BY_DATE_REPORT == reportType ){
			filename += ".csv";

			// check email address
			if (CommonUtil.isParameterEmptyOrNull(Utils.getWriter().getWriter().getEmail())){
				FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR,
						CommonUtil.getMessage("error.marketing.report.invalid.mail", null),null);
				context.addMessage(null, fm);
				return null;
			}

			MarketingReportSender sender = new MarketingReportSender(this,reportType,filename,Utils.getWriter());

			sender.start();

			FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_INFO,
					filename + " " + CommonUtil.getMessage("marketing.report.processing.mail", null),null);
			context.addMessage(null, fm);
            
			return null;
		}

		return null;
	}



	/**
	 * Constructs a <code>String</code> with all attributes
	 * in name = value format.
	 *
	 * @return a <code>String</code> representation
	 * of this object.
	 */
	public String toString()
	{
	    final String TAB = "    ";

	    String retValue = "";

	    retValue = "ClicksForm ( "
	        + super.toString() + TAB
	        + "startDate = " + this.startDate + TAB
	        + "endDate = " + this.endDate + TAB
	        + "skinId = " + this.skinId + TAB
	        + "sourceId = " + this.sourceId + TAB
	        + "marketingCampaignManager = " + this.marketingCampaignManager + TAB
	        + " )";

	    return retValue;
	}

	/**
	 * @return the endDate
	 */
	public Date getEndDate() {
		return endDate;
	}

	/**
	 * @param endDate the endDate to set
	 */
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	/**
	 * @return the report
	 */
	public ArrayList<MarketingReport> getReport() {
		return report;
	}

	/**
	 * @param report the report to set
	 */
	public void setReport(ArrayList<MarketingReport> report) {
		this.report = report;
	}

	/**
	 * @return the skinId
	 */
	public long getSkinId() {
		return skinId;
	}

	/**
	 * @param skinId the skinId to set
	 */
	public void setSkinId(long skinId) {
		this.skinId = skinId;
	}

	/**
	 * @return the startDate
	 */
	public Date getStartDate() {
		return startDate;
	}

	/**
	 * @param startDate the startDate to set
	 */
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	/**
	 * @return the sourceId
	 */
	public long getSourceId() {
		return sourceId;
	}

	/**
	 * @param sourceId the sourceId to set
	 */
	public void setSourceId(long sourceId) {
		this.sourceId = sourceId;
	}

	/**
	 * @return the currentWriterId
	 */
	public long getCurrentWriterId() {
		return currentWriterId;
	}


	/**
	 * @param currentWriterId the currentWriterId to set
	 */
	public void setCurrentWriterId(long currentWriterId) {
		this.currentWriterId = currentWriterId;
	}


	/**
	 * @return the marketingCampaignManager
	 */
	public long getMarketingCampaignManager() {
		return marketingCampaignManager;
	}


	/**
	 * @param marketingCampaignManager the marketingCampaignManager to set
	 */
	public void setMarketingCampaignManager(long marketingCampaignManager) {
		this.marketingCampaignManager = marketingCampaignManager;
	}

	/**
	 * @return the writerIdForSkin
	 */
	public long getWriterIdForSkin() {
		return writerIdForSkin;
	}


	/**
	 * @param writerIdForSkin the writerIdForSkin to set
	 */
	public void setWriterIdForSkin(long writerIdForSkin) {
		this.writerIdForSkin = writerIdForSkin;
	}


    /**
     * @return the isBaseReportByDate
     */
    public boolean isBaseReportByDate() {
        return isBaseReportByDate;
    }


    /**
     * @param isBaseReportByDate the isBaseReportByDate to set
     */
    public void setBaseReportByDate(boolean isBaseReportByDate) {
        this.isBaseReportByDate = isBaseReportByDate;
    }


    /**
     * @return the isDpReportByDate
     */
    public boolean isDpReportByDate() {
        return isDpReportByDate;
    }


    /**
     * @param isDpReportByDate the isDpReportByDate to set
     */
    public void setDpReportByDate(boolean isDpReportByDate) {
        this.isDpReportByDate = isDpReportByDate;
    }

}
