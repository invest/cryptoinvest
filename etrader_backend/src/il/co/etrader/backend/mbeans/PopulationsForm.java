package il.co.etrader.backend.mbeans;

import java.io.Serializable;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;

import javax.faces.event.ValueChangeEvent;

import il.co.etrader.backend.bl_managers.AdminManager;
import il.co.etrader.backend.bl_managers.PopulationsManager;
import il.co.etrader.backend.util.Constants;
import il.co.etrader.bl_vos.Population;
import il.co.etrader.util.CommonUtil;

public class PopulationsForm implements Serializable {

	private static final long serialVersionUID = 6174940962334147283L;

	private ArrayList<Population> list;
	private Population population;

	private long skinId;
	private long populationType;
	private boolean isActive;

	//sorting
	private String sortColumn;
	private String prevSortColumn;
    private boolean sortAscending = false;

	public PopulationsForm() throws SQLException {

		populationType = 0;
		skinId = 0;
		isActive = false;
		sortColumn="";
		prevSortColumn="";

		updateList();
	}

	public String updateList() throws SQLException{

		list = PopulationsManager.getPopulationList(populationType, skinId, isActive,null, 0);

		CommonUtil.setTablesToFirstPage();
		return null;
	}

	/**
	 * Update / insert combination
	 * @return
	 * @throws SQLException
	 */
	public String updateInsert() throws SQLException{

		population.setWriterId((int)AdminManager.getWriterId());

		PopulationsManager.insertUpdatePopulation(population);

		updateList();
		return Constants.NAV_POPULATIONS;
	}

	public int getListSize() {
		return list.size();
	}

	/**
	 * Navigate to edit population
	 * @return
	 * @throws SQLException
	 */
	public String navEdit() {
		return Constants.NAV_POPULATION;
	}

	/**
	 * Navigate to insert new population
	 * @return
	 * @throws SQLException
	 */
	public String navNew() throws SQLException {
		population = new Population();
		population.setWriterId((int)AdminManager.getWriterId());
		population.setTimeCreated(new Date());

		return Constants.NAV_POPULATION;
	}


	/**
	 * @return the list
	 */
	public ArrayList<Population> getList() {
		return list;
	}

	/**
	 * @param list the list to set
	 */
	public void setList(ArrayList<Population> list) {
		this.list = list;
	}

	/**
	 * @return the skinId
	 */
	public long getSkinId() {
		return skinId;
	}

	/**
	 * @param skinId the skinId to set
	 */
	public void setSkinId(long skinId) {
		this.skinId = skinId;
	}

	/**
	 * @return the population
	 */
	public Population getPopulation() {
		return population;
	}

	/**
	 * @param population the population to set
	 */
	public void setPopulation(Population population) {
		this.population = population;
	}

	/**
	 * @return the isActive
	 */
	public boolean getIsActive() {
		return isActive;
	}

	/**
	 * @param isActive the isActive to set
	 */
	public void setIsActive(boolean isActive) {
		this.isActive = isActive;
	}

	/**
	 * @return the populationType
	 */
	public long getPopulationType() {
		return populationType;
	}

	/**
	 * @param populationType the populationType to set
	 */
	public void setPopulationType(long populationType) {
		this.populationType = populationType;
	}

	/**
	 * update skin id from filter
	 * @param event filter event
	 * @throws SQLException
	 * @throws ParseException
	 */
    public void updateSkinChange(ValueChangeEvent event) throws SQLException {
    	skinId = (Long)event.getNewValue();
    	updateList();
    }

    /**
     * update population type from filter
     * @param event from population type filter
     * @throws SQLException
     * @throws ParseException
     */
    public void updatePopulationTypeChange(ValueChangeEvent event) throws SQLException {
    	populationType = (Long)event.getNewValue();
    	updateList();
    }

	/**
	 * Change settled radio button
	 * @param event
	 * @throws SQLException
	 * @throws ParseException
	 */
    public void updateIsActiveChange(ValueChangeEvent event) throws SQLException {
    	isActive =(Boolean)event.getNewValue();
    	updateList();
    }

    /**
	 * Sort list
	 * @return
	 */
	public String sortList() {
		if (sortColumn.equals(prevSortColumn)) {
			sortAscending = !sortAscending;
		} else {
			sortAscending = true;
		}

		prevSortColumn = sortColumn;

		if (sortColumn.equals("id")) {
			Collections.sort(list,new IdComparator(sortAscending));
		} else if (sortColumn.equals("populationTypeName")) {
			Collections.sort(list,new PopulationTypeNameComparator(sortAscending));
		} else if (sortColumn.equals("skinId")) {
			Collections.sort(list,new SkinIdComparator(sortAscending));
		} else if (sortColumn.equals("timeCreated")) {
			Collections.sort(list,new TimeCreatedComparator(sortAscending));
		} else if (sortColumn.equals("isActive")) {
			Collections.sort(list, new IsActiveComparator(sortAscending));
		}

		CommonUtil.setTablesToFirstPage();
		return null;
	}

	private class IdComparator implements Comparator {
		private boolean ascending;
		public IdComparator(boolean asc) {
			ascending = asc;
		}
   	    public int compare(Object o1, Object o2) {
   	    	Population pop1=(Population)o1;
   	    	Population pop2=(Population)o2;
	 		if (ascending){
	 			return new Long(pop1.getId()).compareTo(new Long(pop2.getId()));
	 		}
	 		return -new Long(pop1.getId()).compareTo(new Long(pop2.getId()));
		}
  	 }

	private class PopulationTypeNameComparator implements Comparator {
		private boolean ascending;
		public PopulationTypeNameComparator(boolean asc) {
			ascending = asc;
		}
   	    public int compare(Object o1, Object o2) {
   	    	Population pop1=(Population)o1;
   	    	Population pop2=(Population)o2;
	 		if (ascending){
	 			return pop1.getPopulationTypeName().compareTo(pop2.getPopulationTypeName());
	 		}
	 		return -pop1.getPopulationTypeName().compareTo(pop2.getPopulationTypeName());
		}
  	 }

	private class SkinIdComparator implements Comparator {
		private boolean ascending;
		public SkinIdComparator(boolean asc) {
			ascending = asc;
		}
   	    public int compare(Object o1, Object o2) {
   	    	Population pop1=(Population)o1;
   	    	Population pop2=(Population)o2;
   		 		if (ascending){
   		 			return new Long(pop1.getSkinId()).compareTo(new Long(pop2.getSkinId()));
   		 		}
   		 	return -new Long(pop1.getSkinId()).compareTo(new Long(pop2.getSkinId()));
		}
  	 }

	private class TimeCreatedComparator implements Comparator {
		private boolean ascending;
		public TimeCreatedComparator(boolean asc) {
			ascending = asc;
		}
   	    public int compare(Object o1, Object o2) {
   	    	Population pop1=(Population)o1;
   		 	Population pop2=(Population)o2;
   		 		if (ascending){
   		 			return pop1.getTimeCreated().compareTo(pop2.getTimeCreated());
   		 		}
   		 		return -pop1.getTimeCreated().compareTo(pop2.getTimeCreated());
		}
  	 }

	private class IsActiveComparator implements Comparator {
		private boolean ascending;
		public IsActiveComparator(boolean asc) {
			ascending = asc;
		}
   	    public int compare(Object o1, Object o2) {
   	    	Population pop1=(Population)o1;
   	    	Population pop2=(Population)o2;
	 		if (ascending){
	 			return new String(pop1.getIsActiveTxt()).compareTo(new String(pop2.getIsActiveTxt()));
	 		}
	 		return -new String(pop1.getIsActiveTxt()).compareTo(new String(pop2.getIsActiveTxt()));
		}
  	 }

	/**
	 * @return the prevSortColumn
	 */
	public String getPrevSortColumn() {
		return prevSortColumn;
	}

	/**
	 * @param prevSortColumn the prevSortColumn to set
	 */
	public void setPrevSortColumn(String prevSortColumn) {
		this.prevSortColumn = prevSortColumn;
	}

	/**
	 * @return the sortAscending
	 */
	public boolean isSortAscending() {
		return sortAscending;
	}

	/**
	 * @param sortAscending the sortAscending to set
	 */
	public void setSortAscending(boolean sortAscending) {
		this.sortAscending = sortAscending;
	}

	/**
	 * @return the sortColumn
	 */
	public String getSortColumn() {
		return sortColumn;
	}

	/**
	 * @param sortColumn the sortColumn to set
	 */
	public void setSortColumn(String sortColumn) {
		this.sortColumn = sortColumn;
	}

}
