package il.co.etrader.backend.mbeans;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;

import javax.faces.model.SelectItem;

import com.anyoption.common.beans.base.UserRegulationBase;
import com.anyoption.common.managers.UserRegulationManager;

import il.co.etrader.backend.util.Utils;
import il.co.etrader.bl_managers.UsersManagerBase;


public class UserRegulationForm implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 2454090201759947013L;
	private  String userName;
	private  Long userId;
	private static UserRegulationBase userRegulation;
	private  String selectedStep;
	private  String comments;
	private long writerId ;
	public  boolean showErrMessage;
	public boolean showOKMessage;
	private static final String PRE_COMMENTS = "regulation user overwrite by WRITER_ID :" ;
	private static final String ON = "on";
	
	private ArrayList<SelectItem> listSI;
	
	public UserRegulationForm() {
		
		showErrMessage = false;
		showOKMessage = false;
		listSI = null;
		userName = null;
		userId = null;
		comments = null;
		writerId = Utils.getWriter().getWriter().getId();
		
	}
    
	@SuppressWarnings("deprecation")
	public void updateRegulated() {
		try {
			Date today = new Date();
			listSI = null;
			userRegulation = new UserRegulationBase();
			userRegulation.setUserId(userId);
			userRegulation.setComments(PRE_COMMENTS + Utils.getWriter().getWriter().getId() +" "+ ON + " "+ today.toGMTString() +"" +" : "+ comments);
			userRegulation.setApprovedRegulationStep(new Integer(selectedStep));
			userRegulation.setWriterId(writerId);
			
		 if(UserRegulationManager.updateRegulationStepWithCheck(userRegulation) == 1) {
			 showErrMessage = false;
			 showOKMessage = true;
			 comments = null;
		    }else {
		    	showErrMessage = true;
		    	showOKMessage = false;
		    } 
		} catch (Exception ex) {
			showErrMessage = true;
			showOKMessage = false;
		}
	}
	
	public void getSIList() {
		try {
			showErrMessage = false;
			showOKMessage = false;
			comments = null;
			if (userName != null && (userId == null || userId == 0)) {
				   userId = UsersManagerBase.getUserIdByUserName(userName);
				}
			if((userName == null || "".equals(userName)) && (userId != 0 || userId != null)) {
				userName = UsersManagerBase.getUserNameById(userId);
			}
			listSI = UserRegulationManager.getRegulationStepsList(userId,writerId);
		}catch(Exception ex){
			ex.getMessage();
		}
	}
	
	public  String getUserName() {
		return userName;
	}

	public  void setUserName(String userName) {
		this.userName = userName;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getSelectedStep() {
		return selectedStep;
	}

	public  void setSelectedStep(String selectedStep) {
		this.selectedStep = selectedStep;
	}

	public  String getComments() {
		return comments;
	}

	public  void setComments(String comments) {
		this.comments = comments;
	}

	public boolean isShowErrMessage() {
		return showErrMessage;
	}

	public void setShowErrMessage(boolean showErrMessage) {
		this.showErrMessage = showErrMessage;
	}

	public boolean isShowOKMessage() {
		return showOKMessage;
	}

	public void setShowOKMessage(boolean showOKMessage) {
		this.showOKMessage = showOKMessage;
	}

	public ArrayList<SelectItem> getListSI() {
		return listSI;
	}

	public void setListSI(ArrayList<SelectItem> listSI) {
		this.listSI = listSI;
	}
	
	

}
