package il.co.etrader.backend.helper;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.Hashtable;
import java.util.List;
import java.util.Locale;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.Issue;

import il.co.etrader.backend.bl_managers.ApplicationData;
import il.co.etrader.backend.bl_managers.IssuesManager;
import il.co.etrader.backend.bl_vos.WriterWrapper;
import il.co.etrader.backend.mbeans.IssuesForm;
import il.co.etrader.backend.util.Constants;
import il.co.etrader.util.CommonUtil;
import il.co.etrader.util.ConstantsBase;

public class IssuesReportSender extends Thread {

	private static final Logger logger = Logger.getLogger(IssuesReportSender.class);

	private IssuesForm issuesReportForm;
	private String filename;
	private String mailRecipient;
	private ApplicationData ap;
	private WriterWrapper wr;
	private String skinFilter;

	public IssuesReportSender(IssuesForm form, String filename, ApplicationData ap, WriterWrapper wr, String skinFilter) {
		super();
		this.issuesReportForm = form;
		this.filename = filename;
		this.ap = ap;
		this.wr = wr;
		this.skinFilter = skinFilter;
	}

	public void run() {
		List<Issue> report = null;

		issuesReportForm.setUserName(issuesReportForm.getUserName().toUpperCase());
		if (issuesReportForm.getWriterFilter() > 0) {
			issuesReportForm.setByWriter(false);
		}
		issuesReportForm.setWriterId(issuesReportForm.getWriterFilter());

		long writerId = issuesReportForm.getWriterId();
		boolean bySignificantNotes = issuesReportForm.isBySignificantNotes();
		boolean byWriter = issuesReportForm.isByWriter();


		File file;
		String htmlBuffer = null;
		Writer output = null;
		String errorStr = null;
		File attachment = null;
		mailRecipient = wr.getWriter().getEmail();
		String env = CommonUtil.getProperty(ConstantsBase.CURRENT_SYSTEM , "false");
		String filePath = filename;

		try{

		if (this.issuesReportForm != null){
			report = IssuesManager.searchIssues(this.issuesReportForm, skinFilter, byWriter, bySignificantNotes,
					writerId, ap, wr.getUtcOffset(), wr.getLocalInfo().getLocale());
		}

		} catch (Exception e) {
			logger.error("error loading db ..."  ,e);
		}

		if (!env.equals("local")){
			filePath = "/usr/share/tomcat7/files/" + filePath;
		}

		try {

			if (errorStr == null) {

				boolean isHaveData = false;

				if (report.size() > 0){

					file = new File(filePath);
					output = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file), "UTF-8"));

						output.append("Issue ID");
						output.append(",");
						output.append("User ID");
						output.append(",");
						output.append("User Name");
						output.append(",");
						output.append("Updated at:");
						output.append(",");
						output.append("Status");
						output.append(",");
						output.append("Priority");
						output.append(",");
						output.append("Channel");
						output.append(",");
						output.append("Call direction");
						output.append(",");
						output.append("Reached Status");
						output.append(",");
						output.append("Reaction\\Activity");
						output.append(",");
						output.append("Skin");
						output.append(",");
						output.append("Comments");

						output.append("\n");


				        for (Issue row:report){
				        		isHaveData = true;

								output.append(String.valueOf(row.getId()));
								output.append(",");
								output.append(String.valueOf(row.getUserId()));
								output.append(",");
								output.append(row.getUsername());
								output.append(",");
								output.append(CommonUtil.getDateTimeFormatDisplay(row.getLastActionTime(), wr.getUtcOffset(), " "));
								output.append(",");
								output.append(row.getStatus().getName());
								output.append(",");
								output.append(row.getPriority().getName());
								output.append(",");
								output.append(row.getLastChannel().getName());
								output.append(",");
								output.append(CommonUtil.getMessage(row.getLastCallDirectionName(), null, new Locale(Constants.LOCALE_DEFAULT)));
								output.append(",");
								output.append(row.getLastReachedStatus().getName());
								output.append(",");
								output.append(row.getLastIssueActionType());
								output.append(",");
								output.append(row.getSkin());
								output.append(",");
								output.append( row.getLastComments().replace("\r\n", " ").replace(",", " "));

								output.append("\n");
				        }
				    	output.flush();
				        output.close();
				}
				if (!isHaveData){
					errorStr = "No data was Found.";
				}
			}

		}catch (Exception e) {
			errorStr = "There was a problem in processing your report " + e.toString();
			if (env.equals("live")){
				mailRecipient = mailRecipient + ";qa@etrader.co.il";
			}
        	logger.error(errorStr,e);
		}


        if (errorStr != null) {
        	htmlBuffer = errorStr + "<br/><br/>";
//        	SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
//        	htmlBuffer += " Parameters: <br/> " +
//        				  " start Date: " + sdf.format(from) + " <br/> " +
//        				  " end Date: " + sdf.format(to) + " <br/> " +
//        				  " skin Id: " + skinId + " <br/> " +
//			    		  " writerId: " + writerId  + " <br/> " +
//			    		  " entryType: " + entryType + " <br/> " +
//			    		  " isAssigned: " + isAssigned + " <br/> " +
//			    		  " userIdVal: " + userIdVal + " <br/> " +
//			    		  " username: " + username  + " <br/> " +
//			    		  " priorityId: " + priorityId  + " <br/> ";
        }else{
            attachment = new File(filePath);
            htmlBuffer = "The report is attached to this mail";
        }

        // send email
        Hashtable<String, String> serverProperties = new Hashtable<String, String>();
		serverProperties.put("url", CommonUtil.getProperty("email.server"));
		serverProperties.put("auth", "true");
		serverProperties.put("user", CommonUtil.getProperty("email.uname"));
		serverProperties.put("pass", CommonUtil.getProperty("email.pass"));
		serverProperties.put("contenttype", "text/html; charset=UTF-8");

        Hashtable<String, String> email = new Hashtable<String, String>();
        email.put("subject", filename);
        email.put("to", mailRecipient);

        String senderEmailAddress = mailRecipient;
        if (env.equals("live")){
	        if (issuesReportForm.getSkinFilter().equals("1")){
	        	senderEmailAddress = CommonUtil.getProperty("email.from");
	        }else{
	        	senderEmailAddress = CommonUtil.getProperty("email.from.anyoption");
	        }
        }
        email.put("from", senderEmailAddress);
        email.put("body", htmlBuffer);
        File []attachments={attachment};
		CommonUtil.sendEmail(serverProperties, email, attachments);
	}
}
