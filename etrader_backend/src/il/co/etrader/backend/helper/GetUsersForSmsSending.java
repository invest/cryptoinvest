package il.co.etrader.backend.helper;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.MailBoxTemplate;

import il.co.etrader.backend.bl_managers.MailBoxTemplatesManager;
import il.co.etrader.backend.bl_managers.UsersManager;
import il.co.etrader.backend.bl_vos.User;
import il.co.etrader.backend.util.Constants;
import il.co.etrader.bl_vos.Writer;
import il.co.etrader.util.CommonUtil;


public class GetUsersForSmsSending extends Thread{

	private static final Logger logger = Logger.getLogger(GetUsersForSmsSending.class);

	private ArrayList<User> usersList ;
	private String usersFromFile ;
	private Integer lastloginStart ;
	private Integer lastloginEnd ;
	private String includeCountries ;
	private String skinId ;
	private String campaignIds;
	private List<String> customerValue ;
	private String populationType;
	private int hasSalesDeposit;
	private List<String> depositors ;
	private long userClassId ;
	private int sendType ;
	private int activeUsersFilter ;
	private long mailBoxTemplateId;
	private long promotionId ;
	private String message ;
	private MailBoxTemplate template ;
	private Writer writer;
	private String promotionName;
	private HttpSession session;


	public GetUsersForSmsSending(Integer lastloginStart, Integer lastloginEnd, String includeCountries,String skinId, String campaignIds,
								 List<String> customerValue, String populationType, int hasSalesDeposit, List<String> depositors,
								 long userClassId, int sendType, int activeUsersFilter, long mailBoxTemplateId, String message,
								 Writer writer, String promotionName, HttpSession session, String usersFromFile) {

		super();
		this.lastloginStart = lastloginStart;
		this.lastloginEnd = lastloginEnd;
		this.includeCountries = includeCountries;
		this.skinId = skinId;
		this.campaignIds = campaignIds;
		this.customerValue = customerValue;
		this.populationType = populationType;
		this.hasSalesDeposit = hasSalesDeposit;
		this.depositors = depositors;
		this.userClassId = userClassId;
		this.sendType = sendType;
		this.activeUsersFilter = activeUsersFilter;
		this.mailBoxTemplateId = mailBoxTemplateId;
		this.message = message;
		this.writer = writer;
		this.usersFromFile = usersFromFile;
		this.promotionName = promotionName;
		this.session = session;
	}

	public void run() {

		if(CommonUtil.isParameterEmptyOrNull(usersFromFile)){
				try {
					usersList = UsersManager.getSMSUsers(lastloginStart, lastloginEnd, includeCountries, skinId,
					   									 campaignIds, customerValue, populationType,hasSalesDeposit,
					   									 depositors, userClassId, sendType, activeUsersFilter);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		} else {
			try {
				usersList = UsersManager.getSMSUsersFromFile(usersFromFile, sendType);
			} catch (SQLException e) {
				logger.error("file error problem", e);
				}
			}
		session.setAttribute(Constants.PROMOTION_TOTAL_INSERTED_TO_TABLE, usersList.size());
		if(sendType == Constants.SEND_SMS){
			try {
				UsersManager.insertDataToPromotionTable(usersList, null, sendType, message, writer, promotionName, session);
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} else {//email
			try {
				template = MailBoxTemplatesManager.getTemplateById(mailBoxTemplateId);
				UsersManager.insertDataToPromotionTable(usersList, template.getId(), sendType, message, writer, promotionName, session);
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	public int getActiveUsersFilter() {
		return activeUsersFilter;
	}

	public void setActiveUsersFilter(int activeUsersFilter) {
		this.activeUsersFilter = activeUsersFilter;
	}

	public String getCampaignIds() {
		return campaignIds;
	}

	public void setCampaignIds(String campaignIds) {
		this.campaignIds = campaignIds;
	}

	public List<String> getCustomerValue() {
		return customerValue;
	}

	public void setCustomerValue(List<String> customerValue) {
		this.customerValue = customerValue;
	}

	public List<String> getDepositors() {
		return depositors;
	}

	public void setDepositors(List<String> depositors) {
		this.depositors = depositors;
	}

	public int getHasSalesDeposit() {
		return hasSalesDeposit;
	}

	public void setHasSalesDeposit(int hasSalesDeposit) {
		this.hasSalesDeposit = hasSalesDeposit;
	}

	public String getIncludeCountries() {
		return includeCountries;
	}

	public void setIncludeCountries(String includeCountries) {
		this.includeCountries = includeCountries;
	}

	public Integer getLastloginEnd() {
		return lastloginEnd;
	}

	public void setLastloginEnd(Integer lastloginEnd) {
		this.lastloginEnd = lastloginEnd;
	}

	public Integer getLastloginStart() {
		return lastloginStart;
	}

	public void setLastloginStart(Integer lastloginStart) {
		this.lastloginStart = lastloginStart;
	}

	public String getPopulationType() {
		return populationType;
	}

	public void setPopulationType(String populationType) {
		this.populationType = populationType;
	}

	public int getSendType() {
		return sendType;
	}

	public void setSendType(int sendType) {
		this.sendType = sendType;
	}

	public String getSkinId() {
		return skinId;
	}

	public void setSkinId(String skinId) {
		this.skinId = skinId;
	}

	public long getUserClassId() {
		return userClassId;
	}

	public void setUserClassId(long userClassId) {
		this.userClassId = userClassId;
	}

	public String getUsersFromFile() {
		return usersFromFile;
	}

	public void setUsersFromFile(String usersFromFile) {
		this.usersFromFile = usersFromFile;
	}

	public ArrayList<User> getUsersList() {
		return usersList;
	}

	public void setUsersList(ArrayList<User> usersList) {
		this.usersList = usersList;
	}

	public long getMailBoxTemplateId() {
		return mailBoxTemplateId;
	}

	public void setMailBoxTemplateId(long mailBoxTemplateId) {
		this.mailBoxTemplateId = mailBoxTemplateId;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public long getPromotionId() {
		return promotionId;
	}

	public void setPromotionId(long promotionId) {
		this.promotionId = promotionId;
	}

	public String getPromotionName() {
		return promotionName;
	}

	public void setPromotionName(String promotionName) {
		this.promotionName = promotionName;
	}


}