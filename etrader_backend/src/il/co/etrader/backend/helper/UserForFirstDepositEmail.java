package il.co.etrader.backend.helper;

import java.io.Serializable;
import java.util.Date;
import java.util.Locale;
import java.util.Map;

import com.anyoption.common.beans.base.Currency;
import com.anyoption.common.beans.base.Skin;
import com.anyoption.common.managers.CurrenciesManagerBase;
import com.anyoption.common.util.ConstantsBase;

import il.co.etrader.backend.bl_managers.ApplicationData;
import il.co.etrader.util.CommonUtil;

public class UserForFirstDepositEmail implements Serializable {

	private static final long serialVersionUID = 1L;
	private long userId;
	private String userName = "";
	private int skinId;
	private String mobilePhone = "";
	private String landLinePhone= "";
	private long firstDepAmount;
	private Date firstDepositTimeCreated;
	private long currencyId;
	private long countryCode;
	private String countryName;

	public String toString() {
		final String TAB = " <br/> ";
		Currency currency = CurrenciesManagerBase.getCurrency(this.currencyId);
		
		if(CommonUtil.isParameterEmptyOrNull(mobilePhone)) {
			mobilePhone = "none";
		}

		if(CommonUtil.isParameterEmptyOrNull(landLinePhone)) {
			landLinePhone = "none";
		}

		if(CommonUtil.isParameterEmptyOrNull(userName)) {
			userName = "none";
		}
		String retValue = "";
		retValue = "id = " + this.userId + TAB
	        + "userName = " + this.userName + TAB
	        + "skin id = " + displaySkin(this.skinId) + TAB
	        + "country = " + this.countryName + TAB
	        + "mobile phone = " + this.countryCode + "-" + this.mobilePhone + TAB
	        + "landLine phone = " + this.countryCode + "-" + this.landLinePhone + TAB
	        + "first deposit amount = " + com.anyoption.common.util.CommonUtil.formatCurrencyAmount(this.firstDepAmount/100, true, currency) + TAB
	        + "first deposit time created = " + CommonUtil.getDateAndTimeFormat(this.firstDepositTimeCreated, "Israel")
	        + " (Israel time)";
		return retValue;
	}

	public String displaySkin(int id) {
		Locale localeEn = new Locale(ConstantsBase.LOCALE_DEFAULT);
		String skinName = CommonUtil.getMessage(localeEn, ApplicationData.skinsMap.get(id).getDisplayName(), null);	
		
		return skinName;
	}

	/**
	 * @return the firstDepAmount
	 */
	public long getFirstDepAmount() {
		return firstDepAmount;
	}
	/**
	 * @param firstDepAmount the firstDepAmount to set
	 */
	public void setFirstDepAmount(long firstDepAmount) {
		this.firstDepAmount = firstDepAmount;
	}
	/**
	 * @return the firstDepositTimeCreated
	 */
	public Date getFirstDepositTimeCreated() {
		return firstDepositTimeCreated;
	}
	/**
	 * @param firstDepositTimeCreated the firstDepositTimeCreated to set
	 */
	public void setFirstDepositTimeCreated(Date firstDepositTimeCreated) {
		this.firstDepositTimeCreated = firstDepositTimeCreated;
	}
	/**
	 * @return the landLinePhone
	 */
	public String getLandLinePhone() {
		return landLinePhone;
	}
	/**
	 * @param landLinePhone the landLinePhone to set
	 */
	public void setLandLinePhone(String landLinePhone) {
		this.landLinePhone = landLinePhone;
	}
	/**
	 * @return the mobilePhone
	 */
	public String getMobilePhone() {
		return mobilePhone;
	}
	/**
	 * @param mobilePhone the mobilePhone to set
	 */
	public void setMobilePhone(String mobilePhone) {
		this.mobilePhone = mobilePhone;
	}
	/**
	 * @return the skinId
	 */
	public int getSkinId() {
		return skinId;
	}
	/**
	 * @param skinId the skinId to set
	 */
	public void setSkinId(int skinId) {
		this.skinId = skinId;
	}
	/**
	 * @return the userId
	 */
	public long getUserId() {
		return userId;
	}
	/**
	 * @param userId the userId to set
	 */
	public void setUserId(long userId) {
		this.userId = userId;
	}
	/**
	 * @return the userName
	 */
	public String getUserName() {
		return userName;
	}
	/**
	 * @param userName the userName to set
	 */
	public void setUserName(String userName) {
		this.userName = userName;
	}
	/**
	 * @return the currencyId
	 */
	public long getCurrencyId() {
		return currencyId;
	}
	/**
	 * @param currencyId the currencyId to set
	 */
	public void setCurrencyId(long currencyId) {
		this.currencyId = currencyId;
	}
	/**
	 * @return the countryCode
	 */
	public long getCountryCode() {
		return countryCode;
	}

	/**
	 * @param countryCode the countryCode to set
	 */
	public void setCountryCode(long countryCode) {
		this.countryCode = countryCode;
	}
	/**
	 * @return the countryName
	 */
	public String getCountryName() {
		return countryName;
	}

	/**
	 * @param countryName the countryName to set
	 */
	public void setCountryName(String countryName) {
		this.countryName = countryName;
	}

}
