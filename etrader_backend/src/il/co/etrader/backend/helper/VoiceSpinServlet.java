package il.co.etrader.backend.helper;

import java.io.OutputStream;
import java.io.Serializable;
import java.util.ArrayList;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.Issue;
import com.anyoption.common.beans.IssueAction;
import com.anyoption.common.bl_vos.PopulationEntryBase;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import il.co.etrader.backend.bl_managers.AdminManager;
import il.co.etrader.backend.bl_managers.IssuesManager;
import il.co.etrader.backend.bl_managers.PopulationEntriesManager;
import il.co.etrader.backend.bl_managers.WritersManager;
import il.co.etrader.backend.bl_vos.PopulationEntry;
import il.co.etrader.backend.bl_vos.User;
import il.co.etrader.backend.bl_vos.VoiceSpin;
import il.co.etrader.backend.util.Constants;
import il.co.etrader.bl_managers.ContactsManager;
import il.co.etrader.bl_managers.IssuesManagerBase;
import il.co.etrader.bl_managers.PopulationsManagerBase;
import il.co.etrader.bl_managers.UsersManagerBase;
import il.co.etrader.bl_vos.Contact;
import il.co.etrader.bl_vos.Writer;
import il.co.etrader.util.CommonUtil;
import il.co.etrader.util.ConstantsBase;
import mailTaxJob.Utils;

/**
 * @author eran.levy
 *
 */
public class VoiceSpinServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final Logger logger = Logger.getLogger(VoiceSpinServlet.class);
		
	private static final String PARAM_ENTRY_ID = "e_id";
	private static final String PARAM_REACHED_STATUS_ID = "rs_id";
	private static final String PARAM_ACCESS_TOKEN = "access_token";
	private static final String PARAM_ISSUE_ACTION_TYPE_ID = "iat_id";
	private static final String PARAM_REP_NAME = "r";
	
	private static final int ERROR_CODE_GENERAL_SUCCESS 					= 0;
	private static final int ERROR_CODE_UNKNOWN_ERROR   					= 999;
	private static final int ERROR_CODE_AUTHENTICATION_PERMISSION_DENIED 	= 501;
	private static final int ERROR_CODE_VALIDATION_INVALID_INPUT 			= 502;
	
	private static final String ERROR_MSG_GENERAL_SUCCESS 					= "Success";
	private static final String ERROR_MSG_UNKNOWN_ERROR   					= "Unknown error";
	private static final String ERROR_MSG_AUTHENTICATION_PERMISSION_DENIED 	= "Permission denied";
	private static final String ERROR_MSG_VALIDATION_INVALID_INPUT 			= "Invalid input";

	/**
	 * doGet
	 */
    protected void doGet(HttpServletRequest request, HttpServletResponse response) {
    	logger.info("About to get request from automatic dialer");
    	MethodResult methodResult = new MethodResult();
    	methodResult.setErrorCode(ERROR_CODE_GENERAL_SUCCESS);
    	methodResult.setErrorMessage(ERROR_MSG_GENERAL_SUCCESS);
		VoiceSpin voiceSpin = getVSByRequest(request, methodResult);
		if (methodResult.getErrorCode() == ERROR_CODE_GENERAL_SUCCESS) {
			switch (voiceSpin.getReachedStatusId()) {
			case (int) IssuesManagerBase.ISSUE_REACHED_STATUS_NOT_REACHED:
				notReached(request, voiceSpin, methodResult);
				break;
			case (int) IssuesManagerBase.ISSUE_REACHED_STATUS_REACHED:
				reached(request, voiceSpin, methodResult);
				break;
			default:
				methodResult.setErrorCode(ERROR_CODE_VALIDATION_INVALID_INPUT);
				methodResult.setErrorMessage(ERROR_MSG_VALIDATION_INVALID_INPUT);
				logger.warn("Incorrect reached status id.");
				break;
			}
		}
		responseToClient(response, methodResult, request);
		logger.info("Finished to get request from automatic dialer.");
    }
    
    /**
     * doPost
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response) {    
    	logger.debug("doPost code");
    }
    
    /**
     * Get VoiceSpin by request
     * @param request
     * @return
     */
    public VoiceSpin getVSByRequest(HttpServletRequest request, MethodResult methodResult) {
    	VoiceSpin vs = new VoiceSpin();
    	try {
	    	String entryId = request.getParameter(PARAM_ENTRY_ID);
			String reachedStatusId = request.getParameter(PARAM_REACHED_STATUS_ID);
			String accessToken = request.getParameter(PARAM_ACCESS_TOKEN);
			logger.info("Automatic dialer's parameters: entryId = " + entryId + ", reachedStatusId = " + reachedStatusId  + ", accessToken = " + accessToken);
			vs.setGeneralValidInfo(false);
			if (!Utils.IsParameterEmptyOrNull(accessToken) &&
					accessToken.equals(CommonUtil.getEnum(Constants.ENUM_ACCESS_TOKEN_ENUMERATOR, Constants.ENUM_VOICE_SPIN_CODE))) {
				vs.setEntryId(Long.valueOf(entryId));
				vs.setReachedStatusId(Integer.valueOf(reachedStatusId));
				vs.setGeneralValidInfo(true);				
			} else {
				methodResult.setErrorCode(ERROR_CODE_AUTHENTICATION_PERMISSION_DENIED);
				methodResult.setErrorMessage(ERROR_MSG_AUTHENTICATION_PERMISSION_DENIED);
			}
		} catch (Exception e) {
			methodResult.setErrorCode(ERROR_CODE_VALIDATION_INVALID_INPUT);
			methodResult.setErrorMessage(ERROR_MSG_VALIDATION_INVALID_INPUT);
			logger.error("Problem with getVSByRequest", e);
		}
		return vs;
	}
	
    /**
     * Reached process
     * @param voiceSpin
     */
	public void reached(HttpServletRequest request, VoiceSpin voiceSpin, MethodResult methodResult) {
		logger.info("Reached process - start.");
		try {
			getVSByRequestReached(request, voiceSpin, methodResult);
			if (voiceSpin.isGeneralValidInfo() && voiceSpin.isActionValidInfo()) {
				long writerId = voiceSpin.getRepId();
				PopulationEntryBase populationEntryBase = PopulationsManagerBase.getPopulationEntryByEntryId(voiceSpin.getEntryId());
				PopulationEntry populationEntry = new PopulationEntry(populationEntryBase);
		        // assign.
		        long lastAssignWriterId = populationEntry.getAssignWriterId();
		        if (writerId != lastAssignWriterId) {
		            if (lastAssignWriterId != 0) {
		                PopulationEntriesManager.cancelAssign(populationEntry, false, true);
		            }
		            populationEntry.setAssignWriterId(writerId);
		            PopulationEntriesManager.assign(populationEntry, true, lastAssignWriterId, true, false, voiceSpin.getRepId());
		        }
			} else {
				methodResult.setErrorCode(ERROR_CODE_VALIDATION_INVALID_INPUT);
				methodResult.setErrorMessage(ERROR_MSG_VALIDATION_INVALID_INPUT);
				logger.warn("Info isn't valid.");
			}
		} catch (Exception e) {
			methodResult.setErrorCode(ERROR_CODE_UNKNOWN_ERROR);
			methodResult.setErrorMessage(ERROR_MSG_UNKNOWN_ERROR);
			logger.error("Problem with reached process.", e);
		}
		logger.info("Reached process - end.");
	}
	
	/**
	 * Not reached process
	 * @param voiceSpin
	 */
	public void notReached(HttpServletRequest request, VoiceSpin voiceSpin, MethodResult methodResult) {
		logger.info("Not reached process - start.");
		try {
			getVSByRequestNotReached(request, voiceSpin, methodResult);
			if (voiceSpin.isGeneralValidInfo() && voiceSpin.isActionValidInfo()) {
				PopulationEntryBase populationEntryBase = PopulationsManagerBase.getPopulationEntryByEntryId(voiceSpin.getEntryId());
				PopulationEntry populationEntry = new PopulationEntry(populationEntryBase);
				ArrayList<Integer> skins = WritersManager.getWriterSkins(voiceSpin.getRepId());
				User user = new User(true);
				//user.selectPopulationEntry(populationEntry.getUserId(), populationEntry.getContactId());
				UsersManagerBase.getByUserId(populationEntry.getUserId(), user);
				// insert issue.
		        Issue issue = new Issue();
		        issue.setPriorityId(String.valueOf(IssuesManagerBase.ISSUE_PRIORITY_LOW));
		        issue.setStatusId(String.valueOf(IssuesManagerBase.ISSUE_STATUS_CLOSED));
		        issue.setUserId(populationEntry.getUserId());
		        issue.setContactId(populationEntry.getContactId());
		        issue.setType(ConstantsBase.ISSUE_TYPE_REGULAR);
		        issue.setSubjectId(String.valueOf(IssuesManagerBase.ISSUE_SUBJ_GENERAL));
		        issue.setPopulationEntryId(voiceSpin.getEntryId());
		        IssueAction issueAction = new IssueAction();
		        issueAction.setCallDirectionId(IssueAction.ISSUE_DIRECTION_OUT_BOUND_PHONE_CALL);
		        issueAction.setChannelId(String.valueOf(IssuesManagerBase.ISSUE_CHANNEL_CALL));
		        issueAction.setActionTypeId(String.valueOf(voiceSpin.getIssueActionTypeId()));
		        issueAction.setWriterId(Writer.WRITER_ID_VOICE_SPIN);
		        issueAction.setComments("Added by automatic dialer");
		        int screenId = ConstantsBase.SCREEN_SALEM;
		        if (user.getId() == 0) { // user was not found
			        Contact contact = ContactsManager.getContactByID(populationEntry.getContactId());
					user.setUtcOffset(contact.getUtcOffset());	
					long userId = contact.getUserId();
					// If userId in this contact is 0, look for userId by phone number
					userId = UsersManagerBase.getUserIdByPhone(contact.getPhone());
					user.setId(userId);
		        }      
		        long issueId = IssuesManagerBase.searchIssueIdByPopEntryID(voiceSpin.getEntryId());
		        if (issueId == 0) {					
		        	 IssuesManager.insertIssue(issue, issueAction, user, screenId);
		        } else {
		            issue.setId(issueId);
		            IssuesManagerBase.insertAction(issue, issueAction, user.getUtcOffset(), Writer.WRITER_ID_VOICE_SPIN, user, 0);
		        }
			} else {
				methodResult.setErrorCode(ERROR_CODE_VALIDATION_INVALID_INPUT);
				methodResult.setErrorMessage(ERROR_MSG_VALIDATION_INVALID_INPUT);
				logger.warn("Info isn't valid.");
			}
        } catch (Exception e) {
        	methodResult.setErrorCode(ERROR_CODE_UNKNOWN_ERROR);
			methodResult.setErrorMessage(ERROR_MSG_UNKNOWN_ERROR);
			logger.error("Problem with not reached process.", e);
		}
		logger.info("Not reached process - end.");
	}
	
	/**
	 * Get VoiceSpin by request not reached
	 * @param request
	 * @param vs
	 * @return VoiceSpin
	 */
	public VoiceSpin getVSByRequestNotReached(HttpServletRequest request, VoiceSpin vs, MethodResult methodResult) {
		vs.setActionValidInfo(false);
		try {
			String issueActionTypeId = request.getParameter(PARAM_ISSUE_ACTION_TYPE_ID);
			logger.info("Automatic dialer's parameters: issueActionTypeId = " + issueActionTypeId);
			vs.setIssueActionTypeId(Long.valueOf(issueActionTypeId));
			if (vs.getIssueActionTypeId() == IssuesManagerBase.ISSUE_ACTION_NO_ANSWER ||
					vs.getIssueActionTypeId() == IssuesManagerBase.ISSUE_ACTION_LINE_BUSY ||
						vs.getIssueActionTypeId() == IssuesManagerBase.ISSUE_ACTION_WRONG_NUMBER) {
				vs.setActionValidInfo(true);
			} else {
				methodResult.setErrorCode(ERROR_CODE_VALIDATION_INVALID_INPUT);
				methodResult.setErrorMessage(ERROR_MSG_VALIDATION_INVALID_INPUT);
			}
			logger.info(vs.toString());
		} catch (Exception e) {
			methodResult.setErrorCode(ERROR_CODE_VALIDATION_INVALID_INPUT);
			methodResult.setErrorMessage(ERROR_MSG_VALIDATION_INVALID_INPUT);
			logger.error("Problem with getVSByRequestNotReached", e);
		}
		return vs;
	}
	
	/**
	 * Get VoiceSpin by request reached
	 * @param request
	 * @param vs
	 * @return VoiceSpin
	 */
	public VoiceSpin getVSByRequestReached(HttpServletRequest request, VoiceSpin vs, MethodResult methodResult) {
		vs.setActionValidInfo(false);
		try {
			String repName = request.getParameter(PARAM_REP_NAME);
			logger.info("Automatic dialer's parameters: repName = " + repName);
			Writer writer = AdminManager.getWriterByUserName(repName);
			logger.info("writer: " + writer.toString());
			vs.setRepId(writer.getId());
			vs.setActionValidInfo(true);
			logger.info(vs.toString());
		} catch (Exception e) {
			methodResult.setErrorCode(ERROR_CODE_VALIDATION_INVALID_INPUT);
			methodResult.setErrorMessage(ERROR_MSG_VALIDATION_INVALID_INPUT);
			logger.error("Problem with getVSByRequestReached", e);
		}
		return vs;
	}
	
	private static void responseToClient(HttpServletResponse response, MethodResult result, HttpServletRequest request) {
		try {									 
			GsonBuilder builder = new GsonBuilder();
			Gson gson = builder.create();
			// serialize the JSON response for client
			String jsonResponse = gson.toJson(result);
			if (null != jsonResponse && jsonResponse.length() > 0) {
				byte[] data = jsonResponse.getBytes("UTF-8");
				OutputStream os = response.getOutputStream();				
				response.setHeader("Content-Type", "application/json");
				response.setHeader("Content-Length", String.valueOf(data.length));
				os.write(data, 0, data.length);
				os.flush();
				os.close();				
			}
		} catch (Exception e) {
			logger.error("Error while trying to send response to client ", e);
		}
	}
	
	private class MethodResult implements Serializable {
		private static final long serialVersionUID = 7483931008204851483L;
		
		private int errorCode;
		private String errorMessage;
		
		public MethodResult() {
			
		}
		public int getErrorCode() {
			return errorCode;
		}
		public void setErrorCode(int errorCode) {
			this.errorCode = errorCode;
		}
		public String getErrorMessage() {
			return errorMessage;
		}
		public void setErrorMessage(String errorMessage) {
			this.errorMessage = errorMessage;
		}
	}
}
