package il.co.etrader.backend.helper;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Hashtable;

import il.co.etrader.backend.bl_managers.SalesDepositsManager;
import il.co.etrader.backend.bl_vos.SalesDepositsInfo;
import il.co.etrader.backend.bl_vos.SalesDepositsSummary;
import il.co.etrader.backend.util.Utils;

import org.apache.log4j.Logger;

public class SalesDepositsRetentionSender extends Thread {
	private static final Logger logger = Logger.getLogger(SalesDepositsRetentionSender.class);
	private SalesDepositsInfo info;
	
	public SalesDepositsRetentionSender(SalesDepositsInfo info) {
		super();
		this.info = info;
	}
	
	public void run() {
		/* Get all data */
		ArrayList<SalesDepositsSummary> summary = new ArrayList<SalesDepositsSummary>();
		try {
			summary = SalesDepositsManager.getDepositsRetentionPage(info.getUserId(), info.getStartDate(), info.getEndDate(), info.getWriterId(), 0, 0, info.getSkinId());
		} catch (Exception e) {
			logger.error("error loading sales deposits summary.", e);
		}
		
		/* Generate list to excel file */
		GregorianCalendar gc = new GregorianCalendar();
		Date date = gc.getTime();
		SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd");
		String filename = "SalesDeposits_ret_" + fmt.format(date) + ".xls";
		HashMap<String, String> hm = new HashMap<String, String>();
		hm.put(Utils.getMessage("sales.deposit.sales.representative", null), "getRepUserName");
		hm.put(Utils.getMessage("general.user.id", null), "getUserId");
		hm.put(Utils.getMessage("sales.deposits.depositAmountUSD", null), "getDepositAmountUSD");
		hm.put(Utils.getMessage("sales.deposits.withdrawAmountUSD", null), "getWithdrawAmountUSD");
		hm.put(Utils.getMessage("retention.user.rank", null), "getUserRank");
		hm.put(Utils.getMessage("retention.user.status", null), "getUserStatus");
		hm.put(Utils.getMessage("investments.timecreated", null), "getTransactionTime");
		File[] attachments = Utils.generateListToXls(filename, filename, hm, summary, info.getWriter().getUtcOffset());

		/* send email */
        Hashtable<String,String> server = new Hashtable<String,String>();
        server.put("url", Utils.getProperty("email.server"));
        server.put("auth", Utils.getProperty("email.auth"));
        server.put("user", Utils.getProperty("email.uname"));
        server.put("pass", Utils.getProperty("email.pass"));
        server.put("contenttype", Utils.getProperty("email.contenttype", "text/html; charset=UTF-8"));
        Hashtable<String,String> email = new Hashtable<String,String>();
        email.put("subject", filename);
        email.put("to", info.getWriter().getEmail());
        email.put("from", Utils.getProperty("email.from.anyoption"));
        email.put("body", "The report is attached to this mail");
        Utils.sendEmail(server, email, attachments);
	}
}
