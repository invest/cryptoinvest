package il.co.etrader.backend.helper;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Hashtable;

import org.apache.commons.lang.time.DateFormatUtils;
import org.apache.log4j.Logger;

import com.anyoption.common.beans.base.Skin;

import il.co.etrader.backend.bl_managers.MarketingReportManager;
import il.co.etrader.backend.bl_vos.RegLeadsDetails;
import il.co.etrader.backend.bl_vos.RegLeadsTotals;
import il.co.etrader.backend.bl_vos.RemarketingReport;
import il.co.etrader.backend.bl_vos.WriterWrapper;
import il.co.etrader.backend.mbeans.MarketingReportForm;
import il.co.etrader.backend.mbeans.RegLeadsForm;
import il.co.etrader.backend.util.Constants;
import il.co.etrader.bl_vos.MarketingReport;
import il.co.etrader.util.CommonUtil;
import il.co.etrader.util.ConstantsBase;

public class MarketingReportSender extends Thread {

	private static final Logger logger = Logger.getLogger(MarketingReportSender.class);

	private final String DELEMITER = ",";

	private MarketingReportForm marketingReportForm;
	private RegLeadsForm RegLeadsForm;
	private int reportType;
	private String filename;
	private WriterWrapper writer;
	private String mailRecipient;

	public MarketingReportSender(MarketingReportForm form, int reportType, String filename, WriterWrapper writer) {
		super();
		this.marketingReportForm = form;
		this.reportType = reportType;
		this.filename = filename;
		this.writer = writer;
	}

	public MarketingReportSender(RegLeadsForm form, int reportType, String filename, WriterWrapper writer) {
		super();
		this.RegLeadsForm = form;
		this.reportType = reportType;
		this.filename = filename;
		this.writer = writer;
	}


	public void run() {
		String writersOffset = writer.getUtcOffset();

		Date startDate = null;
		Date endDate = null;
		long skinId = 0;
		long skinBusinessCaseId = 0;
		long sourceId = 0;
		long landingPage = 0;
		long marketingCampaignManager = 0;
		long writerIdForSkin = Constants.DEFAULT_WRITER_ID_FOR_SKIN;;

		ArrayList<MarketingReport> report = null;
		ArrayList<RegLeadsTotals> reportRegLeadsTotal = null;
		ArrayList<RegLeadsDetails> reportRegLeadsDetails = null;
		ArrayList<RemarketingReport> reportRemarketing = null;

		File file;
		String htmlBuffer = null;
		Writer output = null;
		String errorStr = null;
		File attachment = null;
		mailRecipient = writer.getWriter().getEmail();
		String env = CommonUtil.getProperty(ConstantsBase.CURRENT_SYSTEM , "false");
		String filePath = filename;
		boolean isGoogleReport = false;

		if (marketingReportForm != null){
			startDate = marketingReportForm.getStartDate();
			endDate = marketingReportForm.getEndDate();
			skinId = marketingReportForm.getSkinId();
			marketingCampaignManager = marketingReportForm.getMarketingCampaignManager();
			sourceId = marketingReportForm.getSourceId();
			writerIdForSkin = marketingReportForm.getWriterIdForSkin();
		}


		if (!env.equals("local")){
			filePath = "/usr/share/tomcat7/files/" + filePath;
		}

		try {
			long reportSize = 0;

			try {
				if (Constants.MARKETING_REPORT_TYPE_BASIC == reportType){
					report = MarketingReportManager.getAll(skinId, skinBusinessCaseId, startDate, endDate, writersOffset,sourceId, landingPage, marketingCampaignManager, writerIdForSkin);
					reportSize = report.size();
				}else if (Constants.MARKETING_REPORT_TYPE_BASE_BY_DATE_REPORT == reportType ){
                    report = MarketingReportManager.getBaseReportByDate(skinId, skinBusinessCaseId, startDate, endDate, writersOffset, reportType, sourceId, marketingCampaignManager, writerIdForSkin);
                    isGoogleReport = true;
                    reportSize = report.size();
				}else if (Constants.MARKETING_REPORT_TYPE_GOOGLE_DP == reportType ){
					report = MarketingReportManager.getGoogleReport(skinId, skinBusinessCaseId, startDate, endDate, writersOffset, reportType, sourceId, marketingCampaignManager, writerIdForSkin);
					isGoogleReport = true;
					reportSize = report.size();
                }else if (Constants.MARKETING_REPORT_TYPE_DP_BY_DATE_REPORT == reportType ){
                    report = il.co.etrader.bl_managers.MarketingReportManager.getDPReportByDate(skinId, skinBusinessCaseId, startDate, endDate, writersOffset, reportType, sourceId, marketingCampaignManager, writerIdForSkin);
                    isGoogleReport = true;
                    reportSize = report.size();	
				}else if (Constants.MARKETING_REPORT_TYPE_REG_LEADS_TOTAL == reportType){
					reportRegLeadsTotal = MarketingReportManager.getRegLeadsTotalReport(skinId, skinBusinessCaseId, startDate, endDate, 0, 0, writerIdForSkin);
					reportSize = reportRegLeadsTotal.size();
				}else if (Constants.MARKETING_REPORT_TYPE_REG_LEADS_DETAILS == reportType){
					reportRegLeadsDetails = RegLeadsForm.getDetailsListToExport();
					reportSize = reportRegLeadsDetails.size();
				}else if(Constants.MARKETING_REPORT_TYPE_REMARKETING_REPORT == reportType) {
					reportRemarketing = MarketingReportManager.getRemarketingReportDetails(startDate,endDate,skinId,skinBusinessCaseId,sourceId, writerIdForSkin, marketingCampaignManager);
					reportSize = reportRemarketing.size();
				}else if(Constants.MARKETING_REPORT_TYPE_HTTP_REF_REPORT == reportType) {
					report = MarketingReportManager.getMarketingHttpRefererReport(startDate,endDate,skinId,sourceId, marketingCampaignManager, writerIdForSkin);
					reportSize = report.size();
				}
				
			} catch (SQLException e) {
				errorStr = "Error in getting Report Data for reportType " + reportType +  " <br/> " + e.toString();
				logger.error(errorStr,e);
			}

			if (errorStr == null) {
		        //Set the filename
				boolean isHaveData = false;

				if (reportSize > 0){
					file = new File(filePath);
					//output = new BufferedWriter(new FileWriter(file));
					output = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file), "UTF-8"));
					
					if (Constants.MARKETING_REPORT_TYPE_BASIC == reportType){
						
						output.append("Campaign manager");
						output.append(DELEMITER);
						output.append("Combination Id");
						output.append(DELEMITER);
						output.append("Campaign Name");
						output.append(DELEMITER);
						output.append("Source Name");
						output.append(DELEMITER);
						output.append("Medium");
						output.append(DELEMITER);
						output.append("Content");
						output.append(DELEMITER);
						output.append("Market Size Horizontal");
						output.append(DELEMITER);
						output.append("Market Size Vertical");
						output.append(DELEMITER);
						output.append("Location");
						output.append(DELEMITER);
						output.append("Landing Page Name");
						output.append(DELEMITER);
						output.append("Skin Id");
						output.append(DELEMITER);
						output.append("Short reg");
						output.append(DELEMITER);
						output.append("Registered Users Num");
						output.append(DELEMITER);
						output.append("FTD");
						output.append(DELEMITER);
	                    output.append("Reg First Deposit");
	                    output.append(DELEMITER);
						output.append("First remarketing Depositors Num");
						output.append(DELEMITER);
						output.append("House Win");
						output.append(DELEMITER);
	                    output.append("Faild Users");
	                    output.append(DELEMITER);
						
						output.append("\n");


				        for (MarketingReport row:report){
				        	
				        		isHaveData = true;
								
				        		output.append(row.getCampaignManager());
								output.append(DELEMITER);
								output.append(String.valueOf(row.getCombId()));
								output.append(DELEMITER);
								output.append(row.getCampaignName());
								output.append(DELEMITER);
								output.append(row.getSourceName());
								output.append(DELEMITER);
								output.append(row.getMedium());
								output.append(DELEMITER);
								output.append(row.getContent());
								output.append(DELEMITER);
								output.append(String.valueOf(row.getMarketSizeHorizontal()));
								output.append(DELEMITER);
								output.append(String.valueOf(row.getMarketSizeVertical()));
								output.append(DELEMITER);
								output.append(row.getLocation());
								output.append(DELEMITER);
								output.append(row.getLandingPageName());
								output.append(DELEMITER);
								output.append(String.valueOf(row.getSkinId()));
								output.append(DELEMITER);
								output.append(String.valueOf(row.getShortReg()));
								output.append(DELEMITER);
								output.append(String.valueOf(row.getRegisteredUsersNum()));
								output.append(DELEMITER);
								output.append(String.valueOf(row.getFtd()));
								output.append(DELEMITER);
                                output.append(String.valueOf(row.getRfd()));
                                output.append(DELEMITER);
								output.append(String.valueOf(row.getFirstRemDepNum()));
								output.append(DELEMITER);
								output.append(String.valueOf(row.getHouseWin()));
                                output.append(DELEMITER);
                                output.append(String.valueOf(row.getFailedUsers()));

								output.append("\n");				        	
				        }

				    	output.flush();
				        output.close();
					}else if (Constants.MARKETING_REPORT_TYPE_BASE_BY_DATE_REPORT == reportType){
                            
					        output.append("Dates");
                            output.append(DELEMITER);
	                        output.append("Campaign manager");
	                        output.append(DELEMITER);
	                        output.append("Combination Id");
	                        output.append(DELEMITER);
	                        output.append("Campaign Name");
	                        output.append(DELEMITER);
	                        output.append("Source Name");
	                        output.append(DELEMITER);
	                        output.append("Medium");
	                        output.append(DELEMITER);
	                        output.append("Content");
	                        output.append(DELEMITER);
	                        output.append("Market Size Horizontal");
	                        output.append(DELEMITER);
	                        output.append("Market Size Vertical");
	                        output.append(DELEMITER);
	                        output.append("Location");
	                        output.append(DELEMITER);
	                        output.append("Landing Page Name");
	                        output.append(DELEMITER);
	                        output.append("Skin Id");
	                        output.append(DELEMITER);
	                        output.append("Short reg");
	                        output.append(DELEMITER);
	                        output.append("Registered Users Num");
	                        output.append(DELEMITER);
	                        output.append("FTD");
	                        output.append(DELEMITER);
	                        output.append("Reg First Deposit");
	                        output.append(DELEMITER);
	                        output.append("First remarketing Depositors Num");
	                        output.append(DELEMITER);
	                        output.append("House Win");
	                        output.append(DELEMITER);
	                        output.append("Faild Users");
	                        output.append(DELEMITER);
	                        
	                        output.append("\n");


	                        for (MarketingReport row:report){
	                            
	                                isHaveData = true;
	                                
	                                output.append(DateFormatUtils.format(row.getDates(), "dd-MM-yyyy"));
	                                output.append(DELEMITER);
	                                output.append(row.getCampaignManager());
	                                output.append(DELEMITER);
	                                output.append(String.valueOf(row.getCombId()));
	                                output.append(DELEMITER);
	                                output.append(row.getCampaignName());
	                                output.append(DELEMITER);
	                                output.append(row.getSourceName());
	                                output.append(DELEMITER);
	                                output.append(row.getMedium());
	                                output.append(DELEMITER);
	                                output.append(row.getContent());
	                                output.append(DELEMITER);
	                                output.append(String.valueOf(row.getMarketSizeHorizontal()));
	                                output.append(DELEMITER);
	                                output.append(String.valueOf(row.getMarketSizeVertical()));
	                                output.append(DELEMITER);
	                                output.append(row.getLocation());
	                                output.append(DELEMITER);
	                                output.append(row.getLandingPageName());
	                                output.append(DELEMITER);
	                                output.append(String.valueOf(row.getSkinId()));
	                                output.append(DELEMITER);
	                                output.append(String.valueOf(row.getShortReg()));
	                                output.append(DELEMITER);
	                                output.append(String.valueOf(row.getRegisteredUsersNum()));
	                                output.append(DELEMITER);
	                                output.append(String.valueOf(row.getFtd()));
	                                output.append(DELEMITER);
	                                output.append(String.valueOf(row.getRfd()));
	                                output.append(DELEMITER);
	                                output.append(String.valueOf(row.getFirstRemDepNum()));
	                                output.append(DELEMITER);
	                                output.append(String.valueOf(row.getHouseWin()));
	                                output.append(DELEMITER);
	                                output.append(String.valueOf(row.getFailedUsers()));

	                                output.append("\n");                            
	                        }

	                        output.flush();
	                        output.close();				        
					}else if (Constants.MARKETING_REPORT_TYPE_GOOGLE_DP == reportType){
												
						output.append("Dynamic Parameter");
						output.append(DELEMITER);
						output.append("Campaign manager");
						output.append(DELEMITER);
						output.append("Combination Id");
						output.append(DELEMITER);
						output.append("Campaign Name");
						output.append(DELEMITER);
						output.append("Source Name");
						output.append(DELEMITER);
						output.append("Medium");
						output.append(DELEMITER);
						output.append("Content");
						output.append(DELEMITER);
						output.append("Market Size Horizontal");
						output.append(DELEMITER);
						output.append("Market Size Vertical");
						output.append(DELEMITER);
						output.append("Location");
						output.append(DELEMITER);
						output.append("Landing Page Name");
						output.append(DELEMITER);
						output.append("Skin Id");
						output.append(DELEMITER);
						output.append("Short reg");
						output.append(DELEMITER);
						output.append("Registered Users Num");
						output.append(DELEMITER);
						output.append("FTD");
						output.append(DELEMITER);
	                    output.append("Reg First Deposit");
	                    output.append(DELEMITER);
						output.append("First remarketing Depositors Num");
						output.append(DELEMITER);
						output.append("House Win");
						output.append(DELEMITER);
						output.append("\n");


				        for (MarketingReport row:report){
				        		isHaveData = true;

								output.append(row.getDynamicParam());
								output.append(DELEMITER);
				        		output.append(row.getCampaignManager());
								output.append(DELEMITER);
								output.append(String.valueOf(row.getCombId()));
								output.append(DELEMITER);
								output.append(row.getCampaignName());
								output.append(DELEMITER);
								output.append(row.getSourceName());
								output.append(DELEMITER);
								output.append(row.getMedium());
								output.append(DELEMITER);
								output.append(row.getContent());
								output.append(DELEMITER);
								output.append(String.valueOf(row.getMarketSizeHorizontal()));
								output.append(DELEMITER);
								output.append(String.valueOf(row.getMarketSizeVertical()));
								output.append(DELEMITER);
								output.append(row.getLocation());
								output.append(DELEMITER);
								output.append(row.getLandingPageName());
								output.append(DELEMITER);
								output.append(String.valueOf(row.getSkinId()));
								output.append(DELEMITER);
								output.append(String.valueOf(row.getShortReg()));
								output.append(DELEMITER);
								output.append(String.valueOf(row.getRegisteredUsersNum()));
								output.append(DELEMITER);
								output.append(String.valueOf(row.getFtd()));
								output.append(DELEMITER);
                                output.append(String.valueOf(row.getRfd()));
                                output.append(DELEMITER);
								output.append(String.valueOf(row.getFirstRemDepNum()));
								output.append(DELEMITER);
								output.append(String.valueOf(row.getHouseWin()));

								output.append("\n");				        	
				        }

				    	output.flush();
				        output.close();
                    }else if (Constants.MARKETING_REPORT_TYPE_DP_BY_DATE_REPORT == reportType){
                        
                        output.append("Dates");
                        output.append(DELEMITER);
                        output.append("Dynamic Parameter");
                        output.append(DELEMITER);
                        output.append("Campaign manager");
                        output.append(DELEMITER);
                        output.append("Combination Id");
                        output.append(DELEMITER);
                        output.append("Campaign Name");
                        output.append(DELEMITER);
                        output.append("Source Name");
                        output.append(DELEMITER);
                        output.append("Medium");
                        output.append(DELEMITER);
                        output.append("Content");
                        output.append(DELEMITER);
                        output.append("Market Size Horizontal");
                        output.append(DELEMITER);
                        output.append("Market Size Vertical");
                        output.append(DELEMITER);
                        output.append("Location");
                        output.append(DELEMITER);
                        output.append("Landing Page Name");
                        output.append(DELEMITER);
                        output.append("Skin Id");
                        output.append(DELEMITER);
                        output.append("Short reg");
                        output.append(DELEMITER);
                        output.append("Registered Users Num");
                        output.append(DELEMITER);
                        output.append("FTD");
                        output.append(DELEMITER);
                        output.append("Reg First Deposit");
                        output.append(DELEMITER);
                        output.append("First remarketing Depositors Num");
                        output.append(DELEMITER);
                        output.append("House Win");
                        output.append(DELEMITER);
                        output.append("\n");


                        for (MarketingReport row:report){
                                isHaveData = true;

                                output.append(DateFormatUtils.format(row.getDates(), "dd-MM-yyyy"));
                                output.append(DELEMITER);
                                output.append(row.getDynamicParam());
                                output.append(DELEMITER);
                                output.append(row.getCampaignManager());
                                output.append(DELEMITER);
                                output.append(String.valueOf(row.getCombId()));
                                output.append(DELEMITER);
                                output.append(row.getCampaignName());
                                output.append(DELEMITER);
                                output.append(row.getSourceName());
                                output.append(DELEMITER);
                                output.append(row.getMedium());
                                output.append(DELEMITER);
                                output.append(row.getContent());
                                output.append(DELEMITER);
                                output.append(String.valueOf(row.getMarketSizeHorizontal()));
                                output.append(DELEMITER);
                                output.append(String.valueOf(row.getMarketSizeVertical()));
                                output.append(DELEMITER);
                                output.append(row.getLocation());
                                output.append(DELEMITER);
                                output.append(row.getLandingPageName());
                                output.append(DELEMITER);
                                output.append(String.valueOf(row.getSkinId()));
                                output.append(DELEMITER);
                                output.append(String.valueOf(row.getShortReg()));
                                output.append(DELEMITER);
                                output.append(String.valueOf(row.getRegisteredUsersNum()));
                                output.append(DELEMITER);
                                output.append(String.valueOf(row.getFtd()));
                                output.append(DELEMITER);
                                output.append(String.valueOf(row.getRfd()));
                                output.append(DELEMITER);
                                output.append(String.valueOf(row.getFirstRemDepNum()));
                                output.append(DELEMITER);
                                output.append(String.valueOf(row.getHouseWin()));

                                output.append("\n");                            
                        }

                        output.flush();
                        output.close();


					}else if (Constants.MARKETING_REPORT_TYPE_REG_LEADS_TOTAL == reportType){

						output.append("Campaign ID");
						output.append(DELEMITER);
						output.append("Campaign Name");
						output.append(DELEMITER);
						output.append("Comb ID");
						output.append(DELEMITER);
						output.append("Campaign entries");
						output.append(DELEMITER);
						output.append("Call Me");
						output.append(DELEMITER);
						output.append("Full reg following Call Me");
						output.append(DELEMITER);
						output.append("Short Reg");
						output.append(DELEMITER);
						output.append("Full reg following short reg");
						output.append(DELEMITER);
						output.append("Full reg directly");
						output.append(DELEMITER);
						output.append("Total full reg");
						output.append(DELEMITER);
						output.append("First reg dep");

						output.append("\n");


				        for (RegLeadsTotals row:reportRegLeadsTotal){

			        		isHaveData = true;

			        		long campaignId = row.getCampaignId();
			        		if (campaignId > 0){
								output.append(String.valueOf(row.getCampaignId()));
			        		}else{
								output.append("");
			        		}
							output.append(DELEMITER);
							output.append(row.getCampaignName());
							output.append(DELEMITER);

			        		long combId = row.getCombId();
			        		if (combId > 0){
								output.append(String.valueOf(row.getCombId()));
			        		}else{
								output.append("");
			        		}
							output.append(DELEMITER);
							output.append(String.valueOf(row.getCampaignEntries()));
							output.append(DELEMITER);
							output.append(String.valueOf(row.getCallMeNum()));
							output.append(DELEMITER);
							output.append(String.valueOf(row.getRegCallMeNum()));
							output.append(DELEMITER);
							output.append(String.valueOf(row.getShortRegMum()));
							output.append(DELEMITER);
							output.append(String.valueOf(row.getRegShortRegNum()));
							output.append(DELEMITER);
							output.append(String.valueOf(row.getDirectRegNum()));
							output.append(DELEMITER);
							output.append(String.valueOf(row.getTotalRegNum()));
							output.append(DELEMITER);
							output.append(String.valueOf(row.getFirstRegDep()));


							output.append("\n");
			        	}

				    	output.flush();
				        output.close();

					}
					else if (Constants.MARKETING_REPORT_TYPE_REG_LEADS_DETAILS == reportType){

						output.append("Campaign ID");
						output.append(DELEMITER);
						output.append("Campaign Name");
						output.append(DELEMITER);
						output.append("Comb ID");
						output.append(DELEMITER);
						output.append("User Id");
						output.append(DELEMITER);
						output.append("Contact Id");
						output.append(DELEMITER);
						output.append("Time Created");
						output.append(DELEMITER);
						output.append("Original Population");
						output.append(DELEMITER);
						output.append("Current Population");
						output.append(DELEMITER);
						output.append("Entry Type");
						output.append(DELEMITER);
						output.append("Entry Type Action");
						output.append(DELEMITER);
						output.append("Is Depositor");
						output.append(DELEMITER);
						output.append("Is Sales Deposit");
						output.append(DELEMITER);
						output.append("Sales Rep Name");
						output.append(DELEMITER);
						output.append("Action Time");
						output.append(DELEMITER);
						output.append("Channel");
						output.append(DELEMITER);
						output.append("Department");
						output.append(DELEMITER);
						output.append("Rep Name");
						output.append(DELEMITER);
						output.append("Reached Status");
						output.append(DELEMITER);
						output.append("Subject");
						output.append(DELEMITER);
						output.append("Action Type");
						output.append(DELEMITER);
						output.append("Comments");
						output.append(DELEMITER);
						output.append("Issue Id");
						output.append("\n");

						String tempLead = "";
						String currLead = null;

				        for (RegLeadsDetails row:reportRegLeadsDetails){

			        		isHaveData = true;

			        		currLead = row.getUserId() + "-" + row.getContactId();

			        		if (!tempLead.equals(currLead) && !tempLead.equals("")){
			        			// Add separating line
			        			output.append("\n");
			        		}
			        		tempLead = currLead;

							output.append(String.valueOf(row.getCampaignId()));
							output.append(DELEMITER);
							output.append(String.valueOf(row.getCampaignName()));
							output.append(DELEMITER);
							output.append(String.valueOf(row.getCombId()));
							output.append(DELEMITER);
							output.append(String.valueOf(row.getUserId()));
							output.append(DELEMITER);
							output.append(String.valueOf(row.getContactId()));
							output.append(DELEMITER);
							output.append(String.valueOf(row.getTimeCreated()));
							output.append(DELEMITER);
							output.append(String.valueOf(row.getOriginalPop()));
							output.append(DELEMITER);
							output.append(String.valueOf(row.getCurrentPop()));
							output.append(DELEMITER);
							output.append(String.valueOf(row.getEntryType()));
							output.append(DELEMITER);
							output.append(String.valueOf(row.getEntryTypeActionType()));
							output.append(DELEMITER);
							output.append(String.valueOf(row.getIsDepositiorTxt()));
							output.append(DELEMITER);
							output.append(String.valueOf(row.getIsSalesDepTxt()));
							output.append(DELEMITER);
							output.append(String.valueOf(row.getSalesDepRepName()));
							output.append(DELEMITER);
							output.append(String.valueOf(row.getActionTime()));
							output.append(DELEMITER);
							output.append(String.valueOf(row.getChannel()));
							output.append(DELEMITER);
							output.append(String.valueOf(row.getDepartment()));
							output.append(DELEMITER);
							output.append(String.valueOf(row.getRepName()));
							output.append(DELEMITER);
							output.append(String.valueOf(row.getReachedStatus()));
							output.append(DELEMITER);
							output.append(String.valueOf(row.getSubject()));
							output.append(DELEMITER);
							output.append(String.valueOf(row.getActionType()));
							output.append(DELEMITER);
							output.append(row.getComments().replace("\r\n", " ").replace(DELEMITER, " "));
							output.append(DELEMITER);
							output.append(String.valueOf(row.getIssueActionId()));
							output.append("\n");
			        	}

				    	output.flush();
				        output.close();
					
					}else if (Constants.MARKETING_REPORT_TYPE_REMARKETING_REPORT == reportType){

						output.append("First comb");
						output.append(DELEMITER);
						output.append("First DP");
						output.append(DELEMITER);
						output.append("First source");
						output.append(DELEMITER);
						output.append("Remarketing comb");
						output.append(DELEMITER);
						output.append("Remarketing DP");
						output.append(DELEMITER);
						output.append("Remarketing source");
						output.append(DELEMITER);
						output.append("Num Of FTD");
						output.append("\n");

						for (RemarketingReport row : reportRemarketing) {
							isHaveData = true;

							output.append(String.valueOf(row.getFirstComb()));
							output.append(DELEMITER);
							output.append(String.valueOf(row.getFirstDP()));
							output.append(DELEMITER);
							output.append(String.valueOf(row.getFirstSource()));
							output.append(DELEMITER);
							output.append(String.valueOf(row.getRemarketingComb()));
							output.append(DELEMITER);
							output.append(String.valueOf(row.getRemarketingDP()));
							output.append(DELEMITER);
							output.append(String.valueOf(row.getRemarketingSource()));
							output.append(DELEMITER);
							output.append(String.valueOf(row.getNumOfFTD()));
							output.append("\n");
						}
						output.flush();
						output.close();

					} else if(Constants.MARKETING_REPORT_TYPE_HTTP_REF_REPORT == reportType) {

						
						output.append("HTTP referrer");
						output.append(DELEMITER);
						output.append("Decoded source query");
						output.append(DELEMITER);
						output.append("Dynamic parameter");
						output.append(DELEMITER);
						output.append("Campaign manager");
						output.append(DELEMITER);
						output.append("Combination id");
						output.append(DELEMITER);
						output.append("Campaign name");
						output.append(DELEMITER);
						output.append("Source");
						output.append(DELEMITER);
						output.append("Medium");
						output.append(DELEMITER);
						output.append("Skin id");
						output.append(DELEMITER);
						output.append("Short reg");
						output.append(DELEMITER);
						output.append("Registered users num");
						output.append(DELEMITER);
						output.append("FTD ");
						output.append(DELEMITER);
						output.append("First remarketing Depositors Num");
						output.append(DELEMITER);
						
						output.append("\n");


				        for (MarketingReport row:report){
				        	
				        		isHaveData = true;
								
				        		output.append("\""+row.getHttpReferer()+"\"");
								output.append(DELEMITER);
								output.append(row.getDsq());
								output.append(DELEMITER);
								output.append(row.getDynamicParam());
								output.append(DELEMITER);
								output.append(row.getCampaignManager());
								output.append(DELEMITER);
								output.append(""+row.getCombId());
								output.append(DELEMITER);
								output.append(row.getCampaignName());
								output.append(DELEMITER);
								output.append(row.getSourceName());
								output.append(DELEMITER);
								output.append(row.getMedium());
								output.append(DELEMITER);
								output.append(""+row.getSkinId());
								output.append(DELEMITER);
								output.append(""+row.getShortReg());
								output.append(DELEMITER);
								output.append(""+row.getRegisteredUsersNum());
								output.append(DELEMITER);
								output.append(String.valueOf(row.getFtd()));
								output.append(DELEMITER);								
								output.append(String.valueOf(row.getFirstRemDepNum()));
								output.append(DELEMITER);


								output.append("\n");				        	
				        }

				    	output.flush();
				        output.close();
					
					}

				}

				if (!isHaveData){
					errorStr = "No data was Found.";
				}
			}

		}catch (Exception e) {
			errorStr = "There was a problem in processing your report " + e.toString();
			if (env.equals("live")){
				mailRecipient = mailRecipient + ";qa@etrader.co.il";
			}
        	logger.error(errorStr,e);
		}


        if (errorStr != null) {
        	htmlBuffer = errorStr + "<br/><br/>";
        	SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        	htmlBuffer += " Parameters: <br/> " +
        				  " Start Date: " + sdf.format(startDate) + " <br/> " +
        				  " End Date: " + sdf.format(endDate) + " <br/> " +
        				  " Skin Id: " + skinId + " <br/> " +
        				  " Business Skin Id: " + skinBusinessCaseId + " <br/> " +
        				  " Source Id: " + sourceId +" <br/> " +
        				  " Landing Page Id: " + landingPage;

        }else{
            attachment = new File(filePath);
            htmlBuffer = "The report is attached to this mail";
        }

        // send email
        Hashtable<String, String> serverProperties = new Hashtable<String, String>();
		serverProperties.put("url", CommonUtil.getProperty("email.server"));
		serverProperties.put("auth", "false");
		serverProperties.put("user", CommonUtil.getProperty("email.uname"));
		serverProperties.put("pass", CommonUtil.getProperty("email.pass"));
		serverProperties.put("contenttype", "text/html; charset=UTF-8");

        Hashtable<String, String> email = new Hashtable<String, String>();
        email.put("subject", filename);
        email.put("to", mailRecipient);

        String senderEmailAddress = "support@anyoption.com";//mailRecipient;
        if (env.equals("live")){
	        if (CommonUtil.isHebrewSkin(skinId) || skinBusinessCaseId == Skin.SKIN_BUSINESS_ETRADER){
	        	senderEmailAddress = CommonUtil.getProperty("email.from");
	        }else{
	        	senderEmailAddress = CommonUtil.getProperty("email.from.anyoption");
	        }
        }
        email.put("from", senderEmailAddress);
        email.put("body", htmlBuffer);
        File []attachments={attachment};
		CommonUtil.sendEmail(serverProperties, email, attachments);
	}
}
