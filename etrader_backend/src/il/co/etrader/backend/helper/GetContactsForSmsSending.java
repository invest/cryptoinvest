package il.co.etrader.backend.helper;

import java.sql.SQLException;
import java.util.ArrayList;

import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import com.anyoption.common.util.CommonUtil;

import il.co.etrader.backend.bl_managers.ContactsManager;
import il.co.etrader.backend.util.Constants;
import il.co.etrader.bl_vos.Contact;


public class GetContactsForSmsSending extends Thread{

	private static final Logger logger = Logger.getLogger(GetContactsForSmsSending.class);
	
	ArrayList<Contact> contactsList;
	
	long writerId; 

	String skinIds;
	String message; 
	String promotionName;
	String countryIds;
	String campaignIds;
	String populationType;
	String usersFromFile;
	
	HttpSession session;
	
	public GetContactsForSmsSending(HttpSession session, String usersFromFile, long writerId, String skinIds, String message, String promotionName, String countryIds, String campaignIds, String populationType) {
		super();
		this.usersFromFile = usersFromFile;
		this.writerId = writerId; 
		this.skinIds = skinIds;
		this.message = message; 
		this.promotionName = promotionName;
		this.countryIds = countryIds;
		this.campaignIds = campaignIds;
		this.populationType = populationType;
		this.session = session;
	}

	public void run() {
		if(CommonUtil.isParameterEmptyOrNull(usersFromFile)) {
			try {
				contactsList = ContactsManager.getSMSContacts(skinIds, countryIds, campaignIds, populationType);
			} catch (Exception e) {
				logger.debug("ContactsManager.getSMSContacts", e);
			}
		} else {
			try {
				contactsList = ContactsManager.getSMSContactsFromFile(usersFromFile);
			} catch (SQLException e) {
				logger.error("Error while loading sms contacts from file:", e);
			}
		}
		
		session.setAttribute(Constants.PROMOTION_TOTAL_INSERTED_TO_TABLE, contactsList.size());

		try {
			if(contactsList.size()>0) {
				ContactsManager.insertDataToPromotionTable(contactsList, message, writerId, promotionName, session);
			}
		} catch (SQLException e) {
			logger.debug("Can't insert contacts promotion: "+e.getMessage());
		}
		session.setAttribute(Constants.PROMOTION_TOTAL_INSERTED_TO_TABLE, "0_100");
	}
}