package il.co.etrader.backend.helper;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.Writer;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Hashtable;
import java.util.List;
import java.util.Locale;

import org.apache.log4j.Logger;

import il.co.etrader.backend.bl_managers.ApplicationData;
import il.co.etrader.backend.bl_managers.PopulationEntriesManager;
import il.co.etrader.backend.bl_vos.PopulationEntry;
import il.co.etrader.backend.bl_vos.WriterWrapper;
import il.co.etrader.backend.mbeans.ReviewForm;
import il.co.etrader.backend.util.Constants;
import il.co.etrader.util.CommonUtil;
import il.co.etrader.util.ConstantsBase;

public class ReviewReportSender extends Thread {

	private static final Logger logger = Logger.getLogger(CallBackReportSender.class);

	private ReviewForm reviewForm;
	private String filename;
	private String mailRecipient;
	private WriterWrapper wr;
	private Boolean isRetentionSelected;
	private ApplicationData ap;

	public ReviewReportSender(ReviewForm form, String filename, WriterWrapper wr, Boolean isRetentionSelected, ApplicationData ap) {
		super();
		this.reviewForm = form;
		this.filename = filename;
		this.wr = wr;
		this.isRetentionSelected = isRetentionSelected;
		this.ap = ap;
	}

	public void run() {
		List<PopulationEntry> report = null;
		Long userId = reviewForm.getUserId();
		long skinId = reviewForm.getSkinId();
		Long contactId = reviewForm.getContactId();
		long writerId = wr.getWriter().getId();
		int entryType = ConstantsBase.POP_ENTRY_TYPE_CALLBACK;
		Date from = reviewForm.getFrom();
		Date to = reviewForm.getTo();
		Date fromLogin = reviewForm.getFromLogin();
		Date toLogin = reviewForm.getToLogin();
		boolean isAssigned = reviewForm.isAssignFilter();
		int actionTypeId = reviewForm.getActionTypeId();
		long retentionTeamId = reviewForm.getRetentionTeamId();
		String username = null ;

		long userIdVal = 0;
		if (null != userId){
			userIdVal = userId.longValue();
		}
		if (username != null && username.equals(ConstantsBase.EMPTY_STRING)) {
			username = null;
		}

		long contactIdVal =0;
		if (null != contactId) {
			contactIdVal = contactId.longValue();
		}
		writerId = wr.getWriter().getId();

		File file;
		String htmlBuffer = null;
		Writer output = null;
//		BufferedWriter output = null;
		String errorStr = null;
		File attachment = null;
		mailRecipient = wr.getWriter().getEmail();
		String env = CommonUtil.getProperty(ConstantsBase.CURRENT_SYSTEM , "false");
		String filePath = filename;

		try {

			if (this.reviewForm != null) {
				report = PopulationEntriesManager.getPopulationEntriesByEntryType(null,skinId, new Long(0),
						ConstantsBase.POP_ENTRY_TYPE_REVIEW,from,to,fromLogin,toLogin,
						actionTypeId,isAssigned, userIdVal, username, contactIdVal, null, wr, 0, 0, null, 0, "0", false, false, 0, 0, 0, retentionTeamId, null, null);
			}
		} catch (Exception e) {
			logger.error("error loading db ..."  ,e);
		}

		if (!env.equals("local")){
			filePath = "/usr/share/tomcat7/files/" + filePath;
		}

		try {
			if (errorStr == null) {
				boolean isHaveData = false;
				if (report.size() > 0) {
					file = new File(filePath);
					output = new BufferedWriter(new FileWriter(file));
					//
//					output = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file), "UTF-8"));
					//
					output.append("Entry Id ");
					output.append(",");
					output.append("User Id ");
					output.append(",");
					output.append("ContactID");
					output.append(",");
					output.append("Qualification Time");
					output.append(",");
					output.append("User Name");
					output.append(",");
					output.append("Population Now");
					output.append(",");
					output.append("Language");
					output.append(",");
					output.append("Country");
					output.append(",");
					output.append("Last Caller");
					output.append(",");
					output.append("LastCallTime");
					output.append(",");
					output.append("Calls");
					output.append(",");
					output.append("Comments");
					output.append(",");
					output.append("Action Type");
					output.append("\n");
			        for (PopulationEntry row:report) {
		        		isHaveData = true;
		        		output.append(String.valueOf(row.getCurrEntryId()));
						output.append(",");
						output.append(String.valueOf(row.getUserId()));
						output.append(",");
						output.append(String.valueOf(row.getContactId()));
						output.append(",");
						output.append(row.getQualificationTimeTxt(wr.getUtcOffset(), " "));
						output.append(",");
						output.append(row.getPopulationUserName());
						output.append(",");
						output.append(row.getCurrPopulationName());
						output.append(",");
						 //get language.
						Locale locale = null;
						try {
							locale = wr.getLocalInfo().getLocale();
						} catch (Exception e) {
							logger.error("Can't get message take default locale!");
							locale = new Locale(ConstantsBase.LOCALE_DEFAULT); // take default locale
						}
						String language;
						if (row.getLanguageId() > 0) {
							language = CommonUtil.getMessage(ap.getLanguage(row.getLanguageId()).getDisplayName(), null, locale);
						} else {
							language = ConstantsBase.EMPTY_STRING;
						}
						output.append(language);
						output.append(",");

						output.append(row.getCountryWithOffsetDiff(wr.getUtcOffset()));
						output.append(",");
						output.append(row.getLastCallWriter());
						output.append(",");
						output.append(row.getLastCallTimeTxt(wr.getUtcOffset(), "&nbsp;"));
						output.append(",");
						output.append(String.valueOf(row.getCallsCounts()));
						output.append(",");
						String comment = row.getComments();
						String newComment = comment.replace("\r\n", " ");
						newComment = newComment.replace(",", " ");
						output.append(newComment);
						output.append(",");
						output.append(row.getActionTypeName(new Locale(Constants.LOCALE_DEFAULT)));
						output.append("\n");
					}
			    	output.flush();
			        output.close();
				}
				if (!isHaveData) {
					errorStr = "No data was Found.";
				}
			}
		} catch (Exception e) {
			errorStr = "There was a problem in processing your report " + e.toString();
			if (env.equals("live")){
				mailRecipient = mailRecipient + ";qa@etrader.co.il";
			}
        	logger.error(errorStr,e);
		}
        if (errorStr != null) {
        	htmlBuffer = errorStr + "<br/><br/>";
        	SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        	htmlBuffer += htmlBuffer += " Parameters: <br/> " +
										  " start Date: " + sdf.format(from) + " <br/> " +
										  " end Date: " + sdf.format(to) + " <br/> " +
										  " skin Id: " + skinId + " <br/> " +
							    		  " writerId: " + writerId  + " <br/> " +
							    		  " contact Id: " + contactId + " <br/> " +
							    		  " isAssigned: " + isAssigned + " <br/> " +
							    		  " entryType: " + entryType + " <br/> " +
							    		  " username: " + username  + " <br/> " +
							    		  " userId: " + userId  + " <br/> ";

        } else {
            attachment = new File(filePath);
            htmlBuffer = "The report is attached to this mail";
        }

        // send email
        Hashtable<String, String> serverProperties = new Hashtable<String, String>();
		serverProperties.put("url", CommonUtil.getProperty("email.server"));
		serverProperties.put("auth", "true");
		serverProperties.put("user", CommonUtil.getProperty("email.uname"));
		serverProperties.put("pass", CommonUtil.getProperty("email.pass"));
		serverProperties.put("contenttype", "text/html; charset=UTF-8");

        Hashtable<String, String> email = new Hashtable<String, String>();
        email.put("subject", filename);
        email.put("to", mailRecipient);

        String senderEmailAddress = mailRecipient;
        if (env.equals("live")) {
	        if (CommonUtil.isHebrewSkin(skinId)) {
	        	senderEmailAddress = CommonUtil.getProperty("email.from");
	        } else {
	        	senderEmailAddress = CommonUtil.getProperty("email.from.anyoption");
	        }
        }
        email.put("from", senderEmailAddress);
        email.put("body", htmlBuffer);
        File []attachments={attachment};
		CommonUtil.sendEmail(serverProperties, email, attachments);
	}
}
