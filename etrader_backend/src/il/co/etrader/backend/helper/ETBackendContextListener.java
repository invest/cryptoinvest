package il.co.etrader.backend.helper;

import java.sql.SQLException;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;

import org.apache.log4j.Logger;

import com.anyoption.backend.service.userdocumentsscreens.UserDocumentsScreenManager;
import com.anyoption.common.jmx.DbJMXMonitor;
import com.anyoption.common.jmx.cassandra.JMXCassandraConnectionReset;
import com.anyoption.common.jobs.JobsScheduler;
import com.anyoption.common.managers.CurrencyRatesManagerBase;
import com.anyoption.common.managers.DBUtil;
import com.anyoption.common.managers.JobsManagerBase;

import il.co.etrader.backend.bl_managers.ApplicationData;
import il.co.etrader.service.WabServletContextListener;

/**
 * Handle the LevelsCache init and shutdown. We need the levels cache so we can
 * send commands to the service.
 *
 * @author Tony
 */
public class ETBackendContextListener extends WabServletContextListener {
    private static Logger log = Logger.getLogger(ETBackendContextListener.class);
    
    DbJMXMonitor db;
    DbJMXMonitor dataguard;
    private JMXCassandraConnectionReset jmxCassandraReset;

    /**
     * Notification that the application is started
     *
     * @param event
     */
    public void contextInitialized(ServletContextEvent event) {
        super.contextInitialized(event);
        
        try {
            ApplicationData.init();
        } catch (SQLException sqle) {
            log.error("Can't load ApplicationData.", sqle);
        }
        
    	ServletContext servletContext = event.getServletContext();

        String runJobs = servletContext.getInitParameter(JobsManagerBase.CONTEXT_PARAM_RUN_JOBS);
        if (null != runJobs && runJobs.equalsIgnoreCase("true")) {
        	String application = servletContext.getInitParameter(JobsManagerBase.CONTEXT_PARAM_RUN_APPLICATION);
            JobsScheduler js = new JobsScheduler(application);
            js.start();
            servletContext.setAttribute(JobsManagerBase.CONTEXT_PARAM_RUN_JOBS, js);
        }
        CurrencyRatesManagerBase.loadInvestmentRartesCache();

//    	try {
//            BackendLevelsCache levelsCache = new BackendLevelsCache(initialContextFactory, providerURL, connectionFactory, queue,  topic, msgPoolSize, recoveryPause, null);
//            servletContext.setAttribute("levelsCache", levelsCache);
//            if (log.isInfoEnabled()) {
//                log.info("LevelsCahce bound to the context");
//            }
//        } catch (Throwable t) {
//            log.error("Failed to init LevelsCache", t);
//        }
    	
    	
//    	String contactPointsStr = servletContext.getInitParameter("com.copyop.nodes.CONTACT_POINTS");
//		if (contactPointsStr != null) {
//			try {
//				String[] contactPoints = contactPointsStr.split(",");
//				String keyspace = servletContext.getInitParameter("com.copyop.session.keyspace");
//				ManagerBase.init(contactPoints, keyspace);
//			} catch (Exception e) {
//				log.error("Can't initialize " + ManagerBase.class, e);
//			}
//			try {
//				String jmxCassandraResetJMXName = servletContext.getInitParameter("com.anyoption.common.jmx.cassandra.JMX_NAME");
//				jmxCassandraReset = new JMXCassandraConnectionReset();
//				jmxCassandraReset.registerJMX(jmxCassandraResetJMXName);
//			} catch (Exception e) {
//				log.error("Cannot register jmxCassandraReset.", e);
//			}
//		}
        
		try {
			db = new DbJMXMonitor(DBUtil.getDataSource(), "backend");
			db.registerJMX();
			
			dataguard = new DbJMXMonitor(DBUtil.getSecondDataSource(), "dataguard");
			dataguard.registerJMX();
		} catch (SQLException e) {
			log.error("Can't register db monitor", e);
		}
		
		try {			
			UserDocumentsScreenManager.unlockDocumentStatusByServer();
		} catch (Exception e) {
			log.error("Can't clear user doc locks", e);
		}
       
    }

    /**
     * Notification that the application is about to shutdown
     *
     * @param event
     */
    public void contextDestroyed(ServletContextEvent event) {
    	ServletContext context = event.getServletContext();

    	JobsScheduler js = (JobsScheduler) context.getAttribute(JobsManagerBase.CONTEXT_PARAM_RUN_JOBS);
        if (null != js) {
            js.stopJobsScheduler();
        }

//        try {
//            BackendLevelsCache levelsCache = (BackendLevelsCache) context.getAttribute("levelsCache");
//            levelsCache.close();
//            if (log.isInfoEnabled()) {
//                log.info("LevelsCache closed.");
//            }
//        } catch (Throwable t) {
//            log.error("Failed to close LevelsCache.", t);
//        }
        
        try {
        	jmxCassandraReset.unregisterJMX();
        } catch (Exception e) {
        	log.error("Cannot unregister Cassandra reset.", e);
        }
        
//        try {
//        	boolean result = ManagerBase.closeCluster();
//        	log.info("Cluster closing result: " + result);
//        } catch (Exception e) {
//        	log.error("Problem closing cluster", e);
//        }

        db.unregisterJMX();
    }
}