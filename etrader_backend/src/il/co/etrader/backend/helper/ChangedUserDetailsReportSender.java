package il.co.etrader.backend.helper;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Hashtable;

import org.jboss.logging.Logger;

import com.anyoption.common.beans.base.UsersDetailsHistory;
import com.anyoption.common.util.AESUtil;

import il.co.etrader.backend.bl_managers.UsersManager;
import il.co.etrader.backend.bl_vos.WriterWrapper;
import il.co.etrader.backend.mbeans.ChangedUserDetailsForm;
import il.co.etrader.backend.util.Constants;
import il.co.etrader.util.CommonUtil;
import il.co.etrader.util.ConstantsBase;

public class ChangedUserDetailsReportSender extends Thread {
    private static final Logger logger = Logger.getLogger(ChangedUserDetailsReportSender.class);
    
    private ChangedUserDetailsForm changedUserDetailsForm;
    private String fileName;
    private String mailRecipient;
    private WriterWrapper wr;
    
    public ChangedUserDetailsReportSender(ChangedUserDetailsForm form, String filename, WriterWrapper wr) {
        super();
        this.changedUserDetailsForm = form;
        this.fileName = filename;
        this.wr = wr;
    }
    
    public void run() {
        ArrayList<UsersDetailsHistory> list = changedUserDetailsForm.getList();
        File file;
        String htmlBuffer = null;
        Writer output = null;
        String errorStr = null;
        File attachment = null;
        mailRecipient = wr.getWriter().getEmail();
        String env = CommonUtil.getProperty(ConstantsBase.CURRENT_SYSTEM , "false");
        String filePath = fileName;
        if (!env.equals("local")){
            filePath = "/usr/share/tomcat7/files/" + filePath;
        }
        try {
            if (errorStr == null) {
                boolean isHaveData = false;
                if (list.size() > 0) {
                    file = new File(filePath);
                    OutputStream os = new FileOutputStream(file);
        			// Representation (decimal) number for utf-8 encoding by byte order mark (BOM)
        			os.write(239);
        		    os.write(187);
        		    os.write(191);
        			output = new BufferedWriter(new OutputStreamWriter(os, "UTF-8"));
                    output.append("Date ");
                    output.append(",");
                    output.append("Writer ");
                    output.append(",");
                    output.append("User ID");
                    output.append(",");
                    output.append("Email");
                    output.append(",");
                    output.append("Username");
                    output.append(",");
                    output.append("Changed Field");
                    output.append(",");
                    output.append("Field Before");
                    output.append(",");
                    output.append("Field After");
                    output.append(",");
                    output.append("Last Reached");
                    output.append(",");
                    output.append("Tester ?");
                    output.append("\n");
                    for (UsersDetailsHistory row:list) {
                        isHaveData = true;
                        output.append(String.valueOf(row.getTimeCreated()));
                        output.append(",");
                        output.append(String.valueOf(row.getWriterFirstName() + " " + row.getWriterLastName()));
                        output.append(",");
                        output.append(String.valueOf(row.getUserId()));
                        output.append(",");
                        output.append(row.getEmail());
                        output.append(",");
                        output.append(row.getUserName());
                        output.append(",");
                        output.append(row.getFieldChanged());
                        output.append(",");
                        String fieldBefore = row.getFieldBefore();
                        String fieldAfter = row.getFieldAfter();
                        if (row.getIsCheckboxField()) {
                            fieldBefore = getCheckedOrNot(fieldBefore);
                            fieldAfter = getCheckedOrNot(fieldAfter);
                        }
                        if (row.getTypeId() == UsersManager.USERS_DETAILS_HISTORY_TYPE_CREDIT_CARD_NUMBER || row.getTypeId() == UsersManager.USERS_DETAILS_HISTORY_TYPE_PASSWORD) {
                            if (fieldBefore != null) {
                                fieldBefore = AESUtil.decrypt(fieldBefore);
                                fieldBefore = getNumberXXX(fieldBefore);
                            }
                            if (fieldAfter != null) {
                                fieldAfter = AESUtil.decrypt(fieldAfter);
                                fieldAfter = getNumberXXX(fieldAfter);
                            }
                        }
                        output.append(fieldBefore);
                        output.append(",");
                        output.append(fieldAfter);
                        output.append(",");
                        output.append(String.valueOf(row.getLastReached()));
                        output.append(",");
                        output.append(String.valueOf(row.getClassId() == 0 ? Constants.IS_TESTER : Constants.IS_NOT_TESTER));
                        output.append("\n");
                    }
                    output.flush();
                    output.close();
                }
                if (!isHaveData) {
                    errorStr = "No data was Found.";
                }
            }
        } catch (Exception e) {
            errorStr = "There was a problem in processing your report " + e.toString();
            if (env.equals("live")){
                mailRecipient = mailRecipient + ";qa@etrader.co.il";
            }
            logger.error(errorStr,e);
        }
        // check if have errors.
        if (errorStr != null) {
            htmlBuffer = errorStr + "<br/><br/>";
            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
            htmlBuffer += htmlBuffer += " Parameters: <br/> " +
                                          " start Date: " + sdf.format(changedUserDetailsForm.getFrom()) + " <br/> " +
                                          " end Date: " + sdf.format(changedUserDetailsForm.getTo()) + " <br/> " +
                                          " userName: " + changedUserDetailsForm.getUserName() + " <br/> " +
                                          " contact Id: " + changedUserDetailsForm.getContactId() + " <br/> " +
                                          " email: " + changedUserDetailsForm.getEmail() + " <br/> ";

        } else {
            attachment = new File(filePath);
            htmlBuffer = "The report is attached to this mail";
        }
        // send email
        Hashtable<String, String> serverProperties = new Hashtable<String, String>();
        serverProperties.put("url", CommonUtil.getProperty("email.server"));
        serverProperties.put("auth", "true");
        serverProperties.put("user", CommonUtil.getProperty("email.uname"));
        serverProperties.put("pass", CommonUtil.getProperty("email.pass"));
        serverProperties.put("contenttype", "text/html; charset=UTF-8");

        Hashtable<String, String> email = new Hashtable<String, String>();
        email.put("subject", fileName);
        email.put("to", mailRecipient);

        email.put("from", CommonUtil.getProperty("email.from.anyoption"));
        email.put("body", htmlBuffer);
        File []attachments={attachment};
        CommonUtil.sendEmail(serverProperties, email, attachments);
    }
    
    /**
     * replace all digits by * except last 4 digits.
     * @param num
     * @return 
     */
    public String getNumberXXX(String num) {
        String ccnum = String.valueOf(num);
        String out = "";
        for (int i = 0;i < ccnum.length() - 4; i++) {
            out += "*";
        }
        return out + ccnum.substring(ccnum.length() - 4);
    }
    
    /**
     * get checked or not.
     * @param value
     * @return Checked or Un-checked
     */
    public String getCheckedOrNot(String value) {
        if (value.equals("0")) {
            return Constants.USERS_DETAILS_UN_CHECKED;
        }
        return Constants.USERS_DETAILS_CHECKED;
    }
    
}
