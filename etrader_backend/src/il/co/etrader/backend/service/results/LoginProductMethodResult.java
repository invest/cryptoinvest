package il.co.etrader.backend.service.results;

import java.util.Map;

import com.anyoption.common.service.results.MethodResult;

import il.co.etrader.backend.bl_vos.LoginProductBean;
import il.co.etrader.bl_managers.LoginProductManager.LoginProductEnum;

/**
 * @author kiril.mutafchiev
 */
public class LoginProductMethodResult extends MethodResult {

	private Map<LoginProductEnum, String> loginProductTypes;
	private Map<Long, LoginProductBean> loginProductMap;
	private Map<Long, String> skinsFilter;
	private String autoLoginProduct;

	public Map<LoginProductEnum, String> getLoginProductTypes() {
		return loginProductTypes;
	}

	public void setLoginProductTypes(Map<LoginProductEnum, String> loginProductTypes) {
		this.loginProductTypes = loginProductTypes;
	}

	public Map<Long, LoginProductBean> getLoginProductMap() {
		return loginProductMap;
	}

	public void setLoginProductMap(Map<Long, LoginProductBean> loginProductMap) {
		this.loginProductMap = loginProductMap;
	}

	public Map<Long, String> getSkinsFilter() {
		return skinsFilter;
	}

	public void setSkinsFilter(Map<Long, String> skinsFilter) {
		this.skinsFilter = skinsFilter;
	}

	public String getAutoLoginProduct() {
		return autoLoginProduct;
	}

	public void setAutoLoginProduct(String autoLoginProduct) {
		this.autoLoginProduct = autoLoginProduct;
	}

	public String toString() {
		String ls = System.getProperty("line.separator");
		return ls + "LoginProductMethodResult: " + ls
				+ super.toString()
				+ "loginProductTypes: " + loginProductTypes + ls
				+ "loginProductMap: " + loginProductMap + ls
				+ "skinsFilter: " + skinsFilter + ls
				+ "autoLoginProduct: " + autoLoginProduct + ls;
	}
}