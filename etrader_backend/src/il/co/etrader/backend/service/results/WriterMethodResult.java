package il.co.etrader.backend.service.results;

import java.util.List;

import com.anyoption.common.service.results.MethodResult;

import il.co.etrader.backend.bl_vos.Representative;

public class WriterMethodResult extends MethodResult {
	
	public List<String> permissions;
	public List<Representative> conversionReps;
	public List<Representative> retentionReps;
	public String offset;
	public long writerId;

	public List<String> getPermissions() {
		return permissions;
	}

	public void setPermissions(List<String> permissions) {
		this.permissions = permissions;
	}

	public List<Representative> getConversionReps() {
		return conversionReps;
	}

	public void setConversionReps(List<Representative> conversionReps) {
		this.conversionReps = conversionReps;
	}

	public List<Representative> getRetentionReps() {
		return retentionReps;
	}

	public void setRetentionReps(List<Representative> retentionReps) {
		this.retentionReps = retentionReps;
	}

	public String getOffset() {
		return offset;
	}

	public void setOffset(String offset) {
		this.offset = offset;
	}

	public long getWriterId() {
		return writerId;
	}

	public void setWriterId(long writerId) {
		this.writerId = writerId;
	}
}
