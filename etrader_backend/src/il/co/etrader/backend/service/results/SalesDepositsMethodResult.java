package il.co.etrader.backend.service.results;

import java.util.ArrayList;

import com.anyoption.common.service.results.MethodResult;

import il.co.etrader.backend.bl_vos.SalesDepositsSummary;

/**
 * 
 * @author Ivan Petkov
 *
 */

public class SalesDepositsMethodResult extends MethodResult {
	
	private ArrayList<SalesDepositsSummary> depositSummary;
	
	private long pageCount;
	
	/**
	 * 
	 * @return the depositSummary
	 */
	public ArrayList<SalesDepositsSummary> getDepositSummary() {
		return depositSummary;
	}
	/**
	 * 
	 * @param depositSummary the depositSummary to set
	 */
	public void setDepositSummary(ArrayList<SalesDepositsSummary> depositSummary) {
		this.depositSummary = depositSummary;
	}

	/**
	 * 
	 * @return the pageCount
	 */
	public long getPageCount() {
		return pageCount;
	}

	/**
	 * 
	 * @param pageCount the pageCount to set
	 */
	public void setPageCount(long pageCount) {
		this.pageCount = pageCount;
	}

}
