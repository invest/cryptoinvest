package il.co.etrader.backend.service;

import java.io.InputStreamReader;
import java.io.OutputStream;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.anyoption.backend.service.marketpriorities.MarketsHPPRequest;
import com.anyoption.common.service.requests.DepositBonusBalanceMethodRequest;
import com.anyoption.common.service.requests.InstantSpreadChangeMethodRequest;
import com.anyoption.common.service.requests.MethodRequest;
import com.anyoption.common.service.requests.PendingUserWithdrawalsDetailMethodRequest;
import com.anyoption.common.service.requests.SuspendMarketsMethodRequest;
import com.anyoption.common.service.requests.TermsFileMethodRequest;
import com.anyoption.common.service.requests.UserMethodRequest;
import com.anyoption.common.service.results.MethodResult;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import il.co.etrader.backend.bl_vos.User;
import il.co.etrader.backend.bl_vos.WriterWrapper;
import il.co.etrader.backend.service.requests.SalesDepositsMethodRequest;
import il.co.etrader.backend.service.requests.UpdateLoginProductsMethodRequest;
import il.co.etrader.backend.service.requests.WriterMethodRequest;
import il.co.etrader.util.CommonUtil;

/**
 * @author Jamal
 */
public class AnyoptionServiceServlet extends HttpServlet {

	private static final long serialVersionUID = -3536866919755220527L;
	private static final Logger log = Logger.getLogger(AnyoptionServiceServlet.class);
	private static Map<String, MethodWithRoles> methods;

	@Override
	public void init() {
		log.info("Anyoption Service starting...");
		
		methods = new HashMap<String, MethodWithRoles>();
		try {
			Class<?> serviceClass = Class.forName("il.co.etrader.backend.service.AnyoptionService");
			methods.put("getUserDepositBonusBalance", new MethodWithRoles(
																			serviceClass.getMethod(	"getUserDepositBonusBalance",
																									new Class[] {	DepositBonusBalanceMethodRequest.class,
																													HttpServletRequest.class}),
																			Arrays.asList("support")));
			methods.put("suspendBubblesMarkets",
						new MethodWithRoles(serviceClass.getMethod(	"suspendBubblesMarkets",
																	new Class[] {SuspendMarketsMethodRequest.class}),
											Arrays.asList("trader", "strader", "astrader")));
			methods.put("getBubblesMarkets",
						new MethodWithRoles(serviceClass.getMethod("getBubblesMarkets", new Class[] {MethodRequest.class}), 
								Arrays.asList("trader", "strader", "astrader")));
			methods.put("getPendingUserWithdrawalsDetails",
					new MethodWithRoles(serviceClass.getMethod("getPendingUserWithdrawalsDetails", new Class[] {PendingUserWithdrawalsDetailMethodRequest.class}), 
							Arrays.asList("support", "accounting", "admin", "acounting")));
			methods.put("getInstantSpreadChange", new MethodWithRoles(serviceClass.getMethod("getInstantSpreadChange", 
				new Class[] {InstantSpreadChangeMethodRequest.class, HttpServletRequest.class}), Arrays.asList("trader", "strader", "astrader")));
			Class<?> serviceSalesDepositsClass = Class.forName("il.co.etrader.backend.service.access.SalesDeposits");
			methods.put("getDepositsConversion", new MethodWithRoles(serviceSalesDepositsClass.getMethod("getDepositsConversion",
					new Class[] {SalesDepositsMethodRequest.class,  HttpServletRequest.class}), Arrays.asList("retentionM", "retention")));
			methods.put("getDepositsRetention", new MethodWithRoles(serviceSalesDepositsClass.getMethod("getDepositsRetention",
					new Class[] {SalesDepositsMethodRequest.class,  HttpServletRequest.class}), Arrays.asList("retentionM", "retention")));
			methods.put("getWriter", new MethodWithRoles(serviceSalesDepositsClass.getMethod("getWriter",
					new Class[] {WriterMethodRequest.class,  HttpServletRequest.class}), Arrays.asList("retentionM", "retention")));
			methods.put("getWriterConversionRepList", new MethodWithRoles(serviceSalesDepositsClass.getMethod("getWriterConversionRepList",
					new Class[] {WriterMethodRequest.class,  HttpServletRequest.class}), Arrays.asList("retentionM", "retention")));
			methods.put("getWriterRetentionRepList", new MethodWithRoles(serviceSalesDepositsClass.getMethod("getWriterRetentionRepList",
					new Class[] {WriterMethodRequest.class,  HttpServletRequest.class}), Arrays.asList("retentionM", "retention")));
			methods.put("getTermsFilters",
					new MethodWithRoles(serviceClass.getMethod("getTermsFilters", new Class[] {MethodRequest.class}), 
							Arrays.asList("marketing", "sadmin", "cms")));
			methods.put("getTermsFile",
					new MethodWithRoles(serviceClass.getMethod("getTermsFile", new Class[] {TermsFileMethodRequest.class}), 
							Arrays.asList("marketing", "sadmin", "cms")));
			methods.put("saveTermsFile",
					new MethodWithRoles(serviceClass.getMethod("saveTermsFile", new Class[] {TermsFileMethodRequest.class}), 
							Arrays.asList("marketing", "sadmin", "cms")));
			methods.put("getSkinMarketsPriorities", new MethodWithRoles(serviceClass.getMethod("getSkinMarketsPriorities",
					new Class[] {UserMethodRequest.class}), 	Arrays.asList("admin", "sadmin", "trader")));
			methods.put("updateMarketsPriorities", new MethodWithRoles(serviceClass.getMethod("updateMarketsPriorities", 
					new Class[] {MarketsHPPRequest.class, HttpServletRequest.class}), 	Arrays.asList("admin", "sadmin", "trader")));
            methods.put("getUserSingleQuestionnaireDynamic", new MethodWithRoles(serviceClass.getMethod("getUserSingleQuestionnaireDynamic", 
            		new Class[] {UserMethodRequest.class, HttpServletRequest.class}), Arrays.asList("admin", "sadmin", "support")));
			methods.put("getLoginProducts",
						new MethodWithRoles(serviceClass.getMethod("getLoginProducts", new Class[] {MethodRequest.class}),
											Arrays.asList("strader", "sadmin")));
			methods.put("updateLoginProducts", new MethodWithRoles(
																	serviceClass.getMethod(	"updateLoginProducts",
																							new Class[] {	UpdateLoginProductsMethodRequest.class,
																											WriterWrapper.class}),
																	Arrays.asList("strader", "sadmin")));
		} catch (Exception e) {
			log.fatal("Cannot load service methods! ", e);
		}
		
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) {
		String uri = request.getRequestURI();
		String methodReq = uri.substring(uri.lastIndexOf("/") + 1);
		log.debug("URI requested: " + uri + " Method: " + methodReq + " sessionId: " + request.getSession().getId());

		if (log.isTraceEnabled()) {
			Enumeration<String> headerNames = request.getHeaderNames();
			String headerName = null;
			String ls = System.getProperty("line.separator");
			StringBuffer sb = new StringBuffer(ls);
			while (headerNames.hasMoreElements()) {
				headerName = headerNames.nextElement();
				sb.append(headerName).append(": ").append(request.getHeader(headerName)).append(ls);
			}
			log.trace(sb.toString());
		}

		Object result = null;
		try {
			MethodWithRoles mwr = methods.get(methodReq);
			if (mwr != null) {
				User user;
				synchronized (request.getSession()) {
					user = (User) request.getSession().getAttribute("user");
				}
				Gson gson = new GsonBuilder().serializeNulls().create();
				List<String> rolesSection = new ArrayList<String>(mwr.getRoles());
				rolesSection.retainAll(user.getRoles());
				if (!rolesSection.isEmpty()) {
					Method m = mwr.getMethod();
					Class<?>[] params = m.getParameterTypes();
					Object[] requestParams = new Object[params.length];
					requestParams[0] = gson.fromJson(new InputStreamReader(request.getInputStream(), "UTF-8"), params[0]);
					for (int i = 1; i < params.length; i++) {
	                    if (params[i] == HttpServletRequest.class) {
	                        requestParams[i] = request;
	                    } else if (params[i] == WriterWrapper.class) {
	                    	requestParams[i] = request.getSession().getAttribute("writer");
	                    }
					}
					if (requestParams[0] instanceof MethodRequest) {
						((MethodRequest) requestParams[0]).setIp(CommonUtil.getIPAddress(request));
					}
					result = m.invoke(null, requestParams);
	
				} else {
					log.error("Writer does not have privileges to execute this method");
					result = new MethodResult();
					((MethodResult) result).setErrorCode(AnyoptionService.ERROR_CODE_ROLE_REQUIRED);
				}
				String jsonResponse = gson.toJson(result);
				if (null != jsonResponse && jsonResponse.length() > 0) {
					log.debug(jsonResponse);
					byte[] data = jsonResponse.getBytes("UTF-8");
					OutputStream os = response.getOutputStream();
					response.setHeader("Content-Type", "application/json");
					response.setHeader("Content-Length", String.valueOf(data.length));
					os.write(data, 0, data.length);
					os.flush();
					os.close();
				}
			} else {
				log.warn("Method: " + methodReq + " not found!");
			}
		} catch (Exception e) {
			log.error("Problem executing " + methodReq + " Method! ", e);
		}
	}

	@Override
	public void destroy() {
		methods.clear();
		methods = null;
	}

	private class MethodWithRoles {

		private final Method method;
		private final List<String> roles;

		public MethodWithRoles(Method method, List<String> roles) {
			this.method = method;
			this.roles = roles;
		}

		public Method getMethod() {
			return method;
		}

		public List<String> getRoles() {
			return roles;
		}
	}
}