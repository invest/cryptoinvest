package il.co.etrader.backend.service;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import com.anyoption.backend.service.marketpriorities.MarketPrioritiesServices;
import com.anyoption.backend.service.marketpriorities.MarketsHPPRequest;
import com.anyoption.backend.service.marketpriorities.MarketsHPPResponse;
import com.anyoption.backend.service.terms.TermsFileMethodResult;
import com.anyoption.common.beans.Market;
import com.anyoption.common.beans.base.PendingUserWithdrawalsDetailsBase;
import com.anyoption.common.beans.base.TermsPartFile;
import com.anyoption.common.managers.MarketsManagerBase;
import com.anyoption.common.managers.TermsManagerBase;
import com.anyoption.common.service.CommonJSONService;
import com.anyoption.common.service.requests.DepositBonusBalanceMethodRequest;
import com.anyoption.common.service.requests.InstantSpreadChangeMethodRequest;
import com.anyoption.common.service.requests.MethodRequest;
import com.anyoption.common.service.requests.PendingUserWithdrawalsDetailMethodRequest;
import com.anyoption.common.service.requests.SuspendMarketsMethodRequest;
import com.anyoption.common.service.requests.TermsFileMethodRequest;
import com.anyoption.common.service.requests.UserMethodRequest;
import com.anyoption.common.service.results.BubblesMarketsMethodResult;
import com.anyoption.common.service.results.DepositBonusBalanceMethodResult;
import com.anyoption.common.service.results.MethodResult;
import com.anyoption.common.service.results.PendingUserWithdrawalsDetailsMethodResult;
import com.anyoption.common.service.results.TermsFiltersMethodResult;
import com.anyoption.common.service.results.UserMethodResult;
import com.anyoption.common.service.results.UserQuestionnaireResult;
import com.anyoption.common.util.CommonUtil;

import il.co.etrader.backend.bl_managers.LoginProductManager;
import il.co.etrader.backend.bl_managers.TraderManager;
import il.co.etrader.backend.bl_managers.UsersManager;
import il.co.etrader.backend.bl_vos.User;
import il.co.etrader.backend.bl_vos.WriterWrapper;
import il.co.etrader.backend.mbeans.TraderToolPublishForm;
import il.co.etrader.backend.service.requests.UpdateLoginProductsMethodRequest;
import il.co.etrader.backend.service.results.LoginProductMethodResult;
import il.co.etrader.bl_managers.SkinsManagerBase;
import il.co.etrader.util.ConstantsBase;

/**
 * @author Jamal
 */
public class AnyoptionService extends CommonJSONService {

	private static final Logger log = Logger.getLogger(AnyoptionService.class);

	
	public static DepositBonusBalanceMethodResult getUserDepositBonusBalance(DepositBonusBalanceMethodRequest request, HttpServletRequest httpRequest) {
		log.debug("getUserDepositBonusBalance:" + request);
		DepositBonusBalanceMethodResult result = new DepositBonusBalanceMethodResult();
		request.setRequestedFromBE(true);
		result = getDepositBonusBalanceBaseBase(request);			
		return result;
	}
	
	public static UserMethodResult suspendBubblesMarkets(SuspendMarketsMethodRequest request) {
		log.debug("suspendBubblesMarkets:" + request);
		
		UserMethodResult res = new UserMethodResult();
		res.setErrorCode(ERROR_CODE_SUCCESS);
		
		String suspendMsg = "1s2s";
		try {
			if (request.getMarketId() > 0) {
				Market market = MarketsManagerBase.getMarket(request.getMarketId());
				if (request.isOn()) {
					TraderManager.updateSuspendedMarket(market.getFeedName(), false, null);
				} else {
					TraderManager.updateSuspendedMarket(market.getFeedName(), true, suspendMsg);		
				}
			} else {
				if (request.isAllOn()) {
					TraderManager.updateSuspendedMarketByType(Market.PRODUCT_TYPE_BUBBLES, false, null);	
				} else {
					TraderManager.updateSuspendedMarketByType(Market.PRODUCT_TYPE_BUBBLES, true, suspendMsg);	
				}
			}
		} catch (Exception e) {
			log.error("Unable to suspend bubbles market(s).", e);
			res.setErrorCode(ERROR_CODE_UNKNOWN);
		}
							
    	return res;
	}
	
        public static MethodResult getInstantSpreadChange(InstantSpreadChangeMethodRequest request, HttpServletRequest httpRequest) {
        	log.debug("getInstantSpreadChange:" + request);
        	MethodResult result = new MethodResult();
        	User sessionUser = (User) httpRequest.getSession().getAttribute(ConstantsBase.BIND_SESSION_USER);
        	result = TraderToolPublishForm.spreadAction(request, sessionUser);
        	return result;
        }
	
	public static BubblesMarketsMethodResult getBubblesMarkets(MethodRequest request) {
		log.debug("getBubblesMarkets:" + request);
		
		BubblesMarketsMethodResult res = new BubblesMarketsMethodResult();	
		
		try {
			ArrayList<Market> allBubblesMarkets = MarketsManagerBase.getAllBubblesMarkets();
			ArrayList<HashMap<String, String>> markets = new ArrayList<HashMap<String, String>>();
						
			for (Market market : allBubblesMarkets) {
				HashMap<String, String> map = new HashMap<String, String>();
		        map.put("id", String.valueOf(market.getId()));
		        map.put("name", String.valueOf(market.getName()));
		        map.put("suspended", String.valueOf(market.isSuspended() ? 1 : 0));
		        markets.add(map);
			}			
			res.setMarkets(markets);
		} catch (SQLException e) {
			log.error("Unable to get bubbles market(s).", e);
			res.setErrorCode(ERROR_CODE_UNKNOWN);
		}
		
		return res;
	}
	
	public static PendingUserWithdrawalsDetailsMethodResult getPendingUserWithdrawalsDetails(PendingUserWithdrawalsDetailMethodRequest request) {
		log.debug("getPendingUserWithdrawalsDetails:" + request);
		PendingUserWithdrawalsDetailsMethodResult result = new PendingUserWithdrawalsDetailsMethodResult();
		PendingUserWithdrawalsDetailsBase pw = null;
		try {
			pw = UsersManager.pendingUserWithdrawalsDetails(request.getUserId());
		} catch (SQLException e) {
			log.error("Unable to get PendingUserWithdrawalsDetailsMethodResult", e);
			result.setErrorCode(ERROR_CODE_UNKNOWN);
		}	
		result.setPendingUserWithdrawalsDetailsBase(pw);
		return result;
	}
	
	public static TermsFiltersMethodResult getTermsFilters(MethodRequest request) {
		log.debug("getTermsFilters:" + request);
		TermsFiltersMethodResult result = new TermsFiltersMethodResult();
		try {
			result.setFilters(TermsManagerBase.getTermsFilter());
		} catch (SQLException e) {
			log.error("Unable to get getTermsFilter", e);
			result.setErrorCode(ERROR_CODE_UNKNOWN);
		}
		return result;
	}
	
	public static TermsFileMethodResult getTermsFile(TermsFileMethodRequest request) {
		log.debug("getTermsFile:" + request);
		TermsFileMethodResult result = new TermsFileMethodResult();
		try {
			TermsPartFile file = TermsManagerBase.getTermsFile(request.getPlatformIdFilter(), request.getSkinIdFilter(), request.getPartIdFilter());
			if(file.getFileName() != null){
				file.setHtml(getTermsFile(file.getFileName()));	
				result.setTermsPartFile(file);
			} else {
				log.debug("Can't find fileName :" + request);
			}
		} catch (Exception e) {
			log.error("Unable to get getTermsFile", e);
			result.setErrorCode(ERROR_CODE_UNKNOWN);
		}
		return result;
	}
	
	private static String getTermsFile(String fileName) {
		String result = null;
		String dirFileName = CommonUtil.getProperty(ConstantsBase.TERMS_FILES_PATH) + fileName;
		BufferedReader br = null;
		try {
			br = new BufferedReader(new InputStreamReader(new FileInputStream(dirFileName), "UTF8"));
			StringBuilder sb = new StringBuilder();
			String line = br.readLine();

			while (line != null) {
				sb.append(line);
				sb.append(System.lineSeparator());
				line = br.readLine();
			}
			result = sb.toString();
		} catch (Exception e) {
			log.error("Can't get termsFile ", e);
			e.printStackTrace();
		} finally {
			try {
				br.close();
			} catch (IOException e) {
				log.error("Can't close buffer ", e);
			}
		}
		return result;
	}
	
	public static MethodResult saveTermsFile(TermsFileMethodRequest request) {
		log.debug("saveTermsFile:" + request);
		MethodResult result = new MethodResult();
		try {
			TermsPartFile file = TermsManagerBase.getTermsFile(request.getPlatformIdFilter(), request.getSkinIdFilter(), request.getPartIdFilter());
			String fileName = file.getFileName();
			if(fileName != null){
				fileName = CommonUtil.getProperty(ConstantsBase.TERMS_FILES_PATH_PREVIEW) + fileName;
				Writer out = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(fileName), "UTF-8"));
	            out.write(request.getHtml());
	            out.close();
	            TermsManagerBase.updateFile(file.getId(), request);
	            copyTermsFileLiveFolder(request);
			} else {
				log.error("Can't find fileName :" + request);
				result.setErrorCode(ERROR_CODE_UNKNOWN);
			}
		} catch (Exception e) {
			log.error("Unable to get getTermsFile", e);
			result.setErrorCode(ERROR_CODE_UNKNOWN);
		}
		return result;
	}
	
	public static MethodResult copyTermsFileLiveFolder(TermsFileMethodRequest request) {
		log.debug("saveTermsFileLive:" + request);
		MethodResult result = new MethodResult();
		try {
			
			TermsPartFile file = TermsManagerBase.getTermsFile(request.getPlatformIdFilter(), request.getSkinIdFilter(), request.getPartIdFilter());
			String fileName = file.getFileName();
			if(fileName != null){
				fileName = CommonUtil.getProperty(ConstantsBase.TERMS_FILES_PATH_PREVIEW) + fileName;
				String liveFile =  CommonUtil.getProperty(ConstantsBase.TERMS_FILES_PATH) + file.getFileName();
				copyFile(fileName, liveFile);
			} else {
				log.error("Can't find fileName :" + request);
				result.setErrorCode(ERROR_CODE_UNKNOWN);
			}
		} catch (Exception e) {
			log.error("Unable to get getTermsFile", e);
			result.setErrorCode(ERROR_CODE_UNKNOWN);
		}
		return result;
	}
	
	public static MarketsHPPResponse getSkinMarketsPriorities(UserMethodRequest umr) {
		return MarketPrioritiesServices.getSkinMarketsPriorities(umr);
	}
	
	public static MethodResult updateMarketsPriorities(MarketsHPPRequest mr, HttpServletRequest request) {
		return MarketPrioritiesServices.updateMarketsPriorities(mr, request);
	}
	
	private static void copyFile(String sourPath, String destPath) throws IOException {
		Path sourcePath = Paths.get(sourPath);
		Path toDirPath = Paths.get(destPath);
	    Files.copy(sourcePath, toDirPath, StandardCopyOption.REPLACE_EXISTING);
	    
	}
	
	public static UserQuestionnaireResult getUserSingleQuestionnaireDynamic(UserMethodRequest request, HttpServletRequest httpRequest) {
		log.debug("getUserSingleQuestionnaireDynamic:" + request);
		User sessionUser = (User) httpRequest.getSession().getAttribute(ConstantsBase.BIND_SESSION_USER);
		return getUserSingleQuestionnaireDynamic(sessionUser, request.getWriterId());
    }

	public static LoginProductMethodResult getLoginProducts(MethodRequest request) {
		log.debug("getLoginProducts: " + request);
		LoginProductMethodResult result = new LoginProductMethodResult();
		result.setLoginProductTypes(LoginProductManager.getLoginProductTypes());
		result.setLoginProductMap(LoginProductManager.getLoginProductMap());
		result.setSkinsFilter(SkinsManagerBase.getAllSkinsFilter());
		result.setAutoLoginProduct(LoginProductManager.getAutoLoginProduct());
		return result;
	}

	public static MethodResult updateLoginProducts(UpdateLoginProductsMethodRequest request, WriterWrapper writer) {
		MethodResult result = new MethodResult();
		boolean res = LoginProductManager.updateLoginProducts(	request.getSkinIds(), request.getLoginProduct(), request.isEnabled(),
																writer.getWriter().getId());
		if (!res) {
			result.setErrorCode(ERROR_CODE_FAIL_TO_UPDATE);
		}
		return result;
	}
}