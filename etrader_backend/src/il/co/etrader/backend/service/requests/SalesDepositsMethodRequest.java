package il.co.etrader.backend.service.requests;

import com.anyoption.common.service.requests.MethodRequest;

/**
 * 
 * @author Ivan Petkov
 *
 */

public class SalesDepositsMethodRequest extends MethodRequest {
	
	public long userId;
	public String startDate;
	public String endDate;
	public long writerId;
	public long pageNo;
	public long resultsPerPage;
	public boolean isExportToExcel;
	public long skinId;

	public long getUserId() {
		return userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public long getWriterId() {
		return writerId;
	}

	public void setWriterId(long writerId) {
		this.writerId = writerId;
	}

	public long getPageNo() {
		return pageNo;
	}

	public void setPageNo(long pageNo) {
		this.pageNo = pageNo;
	}

	public long getResultsPerPage() {
		return resultsPerPage;
	}

	public void setResultsPerPage(long resultsPerPage) {
		this.resultsPerPage = resultsPerPage;
	}

	/**
	 * @return the isExportToExcel
	 */
	public boolean isExportToExcel() {
		return isExportToExcel;
	}

	/**
	 * @param isExportToExcel the isExportToExcel to set
	 */
	public void setExportToExcel(boolean isExportToExcel) {
		this.isExportToExcel = isExportToExcel;
	}

	/**
	 * @return the skinId
	 */
	public long getSkinId() {
		return skinId;
	}

	/**
	 * @param skinId the skinId to set
	 */
	public void setSkinId(long skinId) {
		this.skinId = skinId;
	}
	
}
