package com.anyoption.json.service;

import java.io.InputStreamReader;
import java.io.OutputStream;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.anyoption.common.service.requests.EmailValidatorRequest;
import com.anyoption.common.service.results.EmailValidatorResult;
import com.anyoption.common.service.util.EmailValidatorUtil;
import com.anyoption.json.util.JsonExclusionStrategy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class EmailValidatorServiceServlet  extends HttpServlet {

	
	private static final long serialVersionUID = 1L;
	 
	
	public void init() {
	
	}


	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, java.io.IOException {
		super.doGet(request, response);
	}

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, java.io.IOException {
    	Gson gson = new GsonBuilder().setExclusionStrategies(new JsonExclusionStrategy()).serializeNulls().create();
    	
    	EmailValidatorResult result = new EmailValidatorResult();

		// deserialize the JSON request from client
		EmailValidatorRequest validationRequest = gson.fromJson(new InputStreamReader(request.getInputStream(), "UTF-8"), EmailValidatorRequest.class);
		//System.out.println(validationRequest);
		int isValid = EmailValidatorUtil.validate(validationRequest.getEmail());
		result.setReturnCode(isValid);
		
		// serialize the JSON response for client
		String jsonResponse = gson.toJson(result);
		if (null != jsonResponse && jsonResponse.length() > 0) {
			byte[] data = jsonResponse.getBytes("UTF-8");
			OutputStream os = response.getOutputStream();
			response.setHeader("Content-Type", "application/json");
			response.setHeader("Content-Length", String.valueOf(data.length));
			os.write(data, 0, data.length);
			os.flush();
			os.close();
		}
 
    }

    public void destroy() {
    	
    }

    
}