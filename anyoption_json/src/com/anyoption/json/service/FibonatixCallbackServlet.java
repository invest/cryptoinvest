package com.anyoption.json.service;

import java.io.UnsupportedEncodingException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.Transaction;
import com.anyoption.common.beans.User;
import com.anyoption.common.beans.Writer;
import com.anyoption.common.clearing.ClearingManagerBase;
import com.anyoption.common.clearing.FibonatixClearingProvider;
import com.anyoption.common.managers.TransactionsManagerBase;
import com.anyoption.common.managers.UsersManagerBase;
import com.anyoption.common.managers.TransactionsManagerBase.TransactionSource;
import com.anyoption.common.util.CommonUtil;
import com.anyoption.managers.ClearingManager;
import com.anyoption.managers.PopulationsManagerBase;

public class FibonatixCallbackServlet extends HttpServlet { 
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -8804535777319851442L;
	public static final Logger log = Logger.getLogger(FibonatixCallbackServlet.class);

	
	@Override
	public void init() {
	}
	
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, java.io.IOException {
		doPost(request, response);
	}
	
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws UnsupportedEncodingException {
		String control = request.getParameter("control");
		String status = request.getParameter("status");
		String orderId = request.getParameter("orderid");
		String clientOrderId = request.getParameter("client_orderid");
		String errorMessage = request.getParameter("error-message");
		
		String  remoteAddr = request.getHeader("X-FORWARDED-FOR");
        if (remoteAddr == null || "".equals(remoteAddr)) {
            remoteAddr = request.getRemoteAddr();
        }
		String referrer = request.getHeader("referer");
		
		log.info("request from :" + remoteAddr + " referer:" + referrer);
		
		FibonatixClearingProvider fibonatix =  (FibonatixClearingProvider) ClearingManagerBase.getClearingProviders().get(ClearingManager.FIBONATIX_3D_PROVIDER_ID);
		if(!CommonUtil.isParameterEmptyOrNull(control) 
				&& !CommonUtil.isParameterEmptyOrNull(status) 
					&& !CommonUtil.isParameterEmptyOrNull(orderId) 
						&& !CommonUtil.isParameterEmptyOrNull(clientOrderId)
							&& fibonatix.isValidCall(control, status, orderId, clientOrderId) ) {
				long transactionId = Long.parseLong(clientOrderId);
				try{
					Transaction t = TransactionsManagerBase.getTransaction(transactionId);
		    		if(t != null) {
	            		String utcOffset = UsersManagerBase.getUserUtcOffset(t.getUserId()); 
	            		boolean success = "approved".equalsIgnoreCase(status);
	            		String message = success ? "" :errorMessage;
						TransactionsManagerBase.finishFibonatixTransaction(t, Writer.WRITER_ID_AUTO, utcOffset, orderId, message, success);
						
						try {
							PopulationsManagerBase.deposit(t.getUserId(), success, Writer.WRITER_ID_AUTO, t);
							User user = UsersManagerBase.getUserById(t.getUserId());
							TransactionsManagerBase.afterDepositAction(user, t, TransactionSource.TRANS_FROM_WEB);
							if(success) {
								com.anyoption.managers.TransactionsManagerBase.afterTransactionSuccess(t.getId(), t.getUserId(), t.getAmount(), Writer.WRITER_ID_AUTO, 0l);
							}
						} catch (Exception e) {
							log.warn("Problem with population succeed deposit event " + e);
						}
		    		} else {
		    			log.error("Can't load transaction:"+transactionId);
		    		}
				} catch (Exception e) {
					log.error("Failed to update fibonatix trx", e);
				}
			} else {
				log.error("Invalid call :" + request.getParameterMap().toString());
			}
	}
}
