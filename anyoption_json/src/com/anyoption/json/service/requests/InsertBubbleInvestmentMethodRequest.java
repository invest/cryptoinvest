package com.anyoption.json.service.requests;

import com.anyoption.common.service.requests.MethodRequest;


/**
 *
 * @author EyalG
 *
 */
public class InsertBubbleInvestmentMethodRequest extends MethodRequest {

	private long userId;
	private long opportunityId;
	private long amount;
	private double pageLevel;
	private float oddsWin;
	private String ipAddress;
	private long startTime;
	private long endTime;
	private double lowLevel;
	private double highLevel;
	private String s;
	private String token;
	
	/**
	 * @return the opportunityId
	 */
	public long getOpportunityId() {
		return opportunityId;
	}

	/**
	 * @param opportunityId the opportunityId to set
	 */
	public void setOpportunityId(long opportunityId) {
		this.opportunityId = opportunityId;
	}

	/**
	 * @return the pageLevel
	 */
	public double getPageLevel() {
		return pageLevel;
	}

	/**
	 * @param pageLevel the pageLevel to set
	 */
	public void setPageLevel(double pageLevel) {
		this.pageLevel = pageLevel;
	}

	public long getAmount() {
		return amount;
	}

	public void setAmount(long amount) {
		this.amount = amount;
	}

	/**
	 * @return the ipAddress
	 */
	public String getIpAddress() {
		return ipAddress;
	}

	/**
	 * @param ipAddress the ipAddress to set
	 */
	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	public long getUserId() {
		return userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	} 

	public double getLowLevel() {
		return lowLevel;
	}

	public void setLowLevel(double lowLevel) {
		this.lowLevel = lowLevel;
	}

	public double getHighLevel() {
		return highLevel;
	}

	public void setHighLevel(double highLevel) {
		this.highLevel = highLevel;
	}

	public String getS() {
		return s;
	}

	public void setS(String s) {
		this.s = s;
	}

	public long getWriterId() {
		return writerId;
	}

	public void setWriterId(long writerId) {
		this.writerId = writerId;
	}

	public long getStartTime() {
		return startTime;
	}

	public void setStartTime(long startTime) {
		this.startTime = startTime;
	}

	public long getEndTime() {
		return endTime;
	}

	public void setEndTime(long endTime) {
		this.endTime = endTime;
	}

	public float getOddsWin() {
		return oddsWin;
	}

	public void setOddsWin(float oddsWin) {
		this.oddsWin = oddsWin;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	@Override
	public String toString() {
		return "InsertBubbleInvestmentMethodRequest [userId=" + userId + ", opportunityId=" + opportunityId
				+ ", amount=" + amount + ", pageLevel=" + pageLevel + ", oddsWin=" + oddsWin + ", ipAddress="
				+ ipAddress + ", startTime=" + startTime + ", endTime=" + endTime + ", lowLevel=" + lowLevel
				+ ", highLevel=" + highLevel + ", s=" + s + ", token=" + token + "]";
	}
}