package com.anyoption.json.service;

import java.io.InputStreamReader;
import java.io.OutputStream;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.anyoption.common.util.CommonUtil;
import com.anyoption.json.requests.GeoTagRequest;
import com.anyoption.json.results.GeoTagResult;
import com.anyoption.json.util.JsonExclusionStrategy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class GeoTagServiceServlet  extends HttpServlet {

	
	private static final long serialVersionUID = 1L;
	 
	
	public void init() {
	
	}


	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, java.io.IOException {
		super.doGet(request, response);
	}

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, java.io.IOException {
    	Gson gson = new GsonBuilder().setExclusionStrategies(new JsonExclusionStrategy()).serializeNulls().create();

    	GeoTagResult result = new GeoTagResult();
    	
		// deserialize the JSON request from client
    	GeoTagRequest validationRequest = gson.fromJson(new InputStreamReader(request.getInputStream(), "UTF-8"), GeoTagRequest.class);
		//System.out.println(validationRequest);
		String countryCode = CommonUtil.getCountryCodeByIp(validationRequest.getIp());
		result.setCountryCode(countryCode);
		
		// serialize the JSON response for client
		String jsonResponse = gson.toJson(result);
		if (null != jsonResponse && jsonResponse.length() > 0) {
			byte[] data = jsonResponse.getBytes("UTF-8");
			OutputStream os = response.getOutputStream();
			response.setHeader("Content-Type", "application/json");
			response.setHeader("Content-Length", String.valueOf(data.length));
			os.write(data, 0, data.length);
			os.flush();
			os.close();
		}
 
    }

    public void destroy() {
    	
    }

    
}