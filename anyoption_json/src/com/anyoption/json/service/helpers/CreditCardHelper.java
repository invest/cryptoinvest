/**
 * 
 */
package com.anyoption.json.service.helpers;

import com.anyoption.common.beans.CreditCard;
import com.anyoption.util.CommonUtil;

/**
 * @author kirilim
 *
 */
public class CreditCardHelper {

	/**
     * Format currency amount and add currency symbol.
     *
     * @param Credit Card to format
     * @param currencyId
     */
	public static void initCreditCardCurrencyAmount(CreditCard cc, long currencyId) {
		cc.setCcNumberXXXX(cc.getCcNumberXXX());
		cc.setCreditAmountTxt(CommonUtil.formatCurrencyAmountAO(cc.getCreditAmount(), currencyId));
	}
}
