package com.anyoption.json.service.helpers;

import com.anyoption.common.beans.base.MailBoxUser;
import com.anyoption.util.CommonUtil;

public class MailBoxUserHelper {

	public static final int MAX_SUBJECT = 25; 
	
	public static void initMailBoxUser(MailBoxUser mbUser, String utcOffset) {
		if (mbUser.getSubject() != null && mbUser.getSubject().length() > MAX_SUBJECT) {
			mbUser.setSubjectShortTxt(mbUser.getSubject().substring(0, MAX_SUBJECT) + "...");
		} else {
			mbUser.setSubjectShortTxt(mbUser.getSubject());
		}
		mbUser.setTimeCreatedTxt(CommonUtil.getDateAndTimeFormatByDots(mbUser.getTimeCreated(), utcOffset));
	}
}
