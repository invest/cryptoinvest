package com.anyoption.json.service;


import java.io.PrintWriter;
import java.text.Collator;
import java.util.Collections;
import java.util.Comparator;
import java.util.Hashtable;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.Locale;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.Country;
import com.anyoption.common.beans.base.CountryMiniBean;
import com.anyoption.common.beans.base.Skin;
import com.anyoption.common.managers.CountryManagerBase;
import com.anyoption.common.managers.LanguagesManagerBase;
import com.anyoption.common.util.ConstantsBase;
import com.anyoption.managers.SkinsManagerBase;
import com.anyoption.util.CommonUtil;

public class CountriesServiceServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4029660242243702324L;
	private static final Logger log = Logger.getLogger(CountriesServiceServlet.class);
	
	public void init() throws ServletException {}
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, java.io.IOException {
		long t = System.currentTimeMillis();
		response.setContentType("application/json;charset=UTF-8");
		response.addHeader("Pragma", "no-cache");
		response.setHeader("Cache-Control", "no-store");
		String skinStr = request.getParameter(ConstantsBase.SKIN_ID);
		long skinId;
		try {
			skinId = Long.parseLong(skinStr);
		} catch(NumberFormatException nfe) {
			log.error("Unable to get skin id from request, provided skin is: " + skinStr, nfe);
			return;
		}
		if (skinId == Skin.SKIN_CHINESE || skinId == Skin.SKIN_RUSSIAN || skinId == Skin.SKIN_TURKISH) {
			skinId = Skin.SKIN_ENGLISH;
		}
		Locale locale = new Locale(LanguagesManagerBase.getLanguage(SkinsManagerBase.getSkin(skinId).getDefaultLanguageId()).getCode());
		Collator collator = Collator.getInstance(locale);
		Hashtable<Long, Country>  countriesTable = CountryManagerBase.getAllowedCountries();
		LinkedHashMap<Long, CountryMiniBean> counriesMap = new LinkedHashMap<Long, CountryMiniBean>();
		LinkedList <CountryMiniBean> countriesList = new LinkedList<CountryMiniBean>();
		
		for(Long key: countriesTable.keySet()) {
			String countryName = CommonUtil.getMessage(countriesTable.get(key).getDisplayName(), null, locale);
			CountryMiniBean cmb = new CountryMiniBean(countriesTable.get(key).getId(),countryName,countriesTable.get(key).getPhoneCode(),countriesTable.get(key).getA2(),collator.getCollationKey(countryName));
			countriesList.add(cmb);
		}

		Collections.sort(countriesList, new CountryMiniBeanComparator<CountryMiniBean>());
		
		PrintWriter out = response.getWriter();
		String json = "[";
		for(CountryMiniBean cmb:countriesList){
			json += "{\"id\":\""+cmb.getId()+"\", ";
			json += "\"displayName\":\""+cmb.getDisplayName()+"\", ";
			json += "\"phoneCode\":\""+cmb.getPhoneCode()+"\", ";
			json += "\"A2\":\""+cmb.getA2()+"\"},";
		}
		json = json.substring(0, json.length()-1);
		json += "]";
		out.write(json);
        out.flush();
        out.close();
        
        if (log.isDebugEnabled()) {
			log.debug("total time: " + (System.currentTimeMillis() - t));
		}
	}
	
	private static class CountryMiniBeanComparator<T> implements Comparator<CountryMiniBean> {

		@Override
		public int compare(CountryMiniBean o1, CountryMiniBean o2) {
			return o1.getDisplayName().compareTo(o2.getDisplayName());
		}
	}
	
}
