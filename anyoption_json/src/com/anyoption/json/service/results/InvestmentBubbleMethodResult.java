package com.anyoption.json.service.results;

import com.anyoption.common.service.results.MethodResult;
import com.google.gson.annotations.Expose;

/**
 * 
 * @author EyalG
 *
 */
public class InvestmentBubbleMethodResult extends MethodResult {

	@Expose
	protected long investmentId;
	
	@Expose
	private long balance;

    public long getInvestmentId() {
		return investmentId;
	}

	public void setInvestmentId(long investmentId) {
		this.investmentId = investmentId;
	}

	@Override
	public String toString() {
    	String ls = System.getProperty("line.separator");
    	return ls + "InvestmentMethodResult"
    			+ super.toString()
    			+ "investment Id: " + investmentId + ls
    			+ "user balance: " + balance + ls;
    }

    public long getBalance() {
	return balance;
    }

    public void setBalance(long balance) {
	this.balance = balance;
    }
}