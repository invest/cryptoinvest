package com.anyoption.json.service;


import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.anyoption.beans.base.Contact;
import com.anyoption.common.managers.CountryManagerBase;
import com.anyoption.common.util.CommonUtil;
import com.anyoption.common.util.ConstantsBase;
import com.anyoption.managers.ContactsManagerBase;

/**
 * 
 * @author eranl
 * AR-1518 - AO + CO - Support FB continued flow (Web + Mobile)
 *
 */
public class FBContinuedFlowServlet extends com.anyoption.common.util.FBContinuedFlowServlet {

	private static final long serialVersionUID = 1L;
	private static final Logger log = Logger.getLogger(FBContinuedFlowServlet.class);
	
	@Override
	public void insertContact(String email, String firstName, String lastName, long countryId, long skinId,
			HttpServletRequest request, HttpServletResponse response, String ip, String combId, 
			String dynamicParam, String affSub1, String affSub2, String affSub3, long writerId) {
		
		com.anyoption.beans.base.Contact contact = new com.anyoption.beans.base.Contact();
		contact.setWriterId(writerId);
		contact.setType(Contact.CONTACT_US_SHORT_REG_FORM);
		contact.setEmail(email);
		contact.setFirstName(firstName);
		contact.setLastName(lastName);
		contact.setCountryId(countryId);
		contact.setIp(ip);
		contact.setSkinId(skinId);
		contact.setCountryId(countryId);
		contact.setUserAgent(CommonUtil.getUserAgent(request));
		String offset = ConstantsBase.OFFSET_GMT;
		if (countryId > 0) {
			offset = CountryManagerBase.getCountries().get(countryId).getGmtOffset();
		}
		contact.setUtcOffset(offset);

		if (!CommonUtil.isParameterEmptyOrNull(dynamicParam)){
			contact.setDynamicParameter(dynamicParam);
		}
		if (!CommonUtil.isParameterEmptyOrNull(affSub1)){
			contact.setAff_sub1(affSub1);
		}
		if (!CommonUtil.isParameterEmptyOrNull(affSub2)){
			contact.setAff_sub2(affSub2);
		}
		if (!CommonUtil.isParameterEmptyOrNull(affSub3)){
			contact.setAff_sub3(affSub3);
		}
		if (!CommonUtil.isParameterEmptyOrNull(combId)){
			contact.setCombId(Long.valueOf(combId));
		}
	
		try {
			ContactsManagerBase.insertContact(contact, ip, null, null, null, skinId, 0);
		} catch (SQLException e) {
			log.error("Cannot insert contact", e);	
		}	
	}
	
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
    	saveData(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException  {
    	saveData(request, response);
    }
}
