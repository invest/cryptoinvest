package com.anyoption.json.util;

import java.util.ArrayList;
import java.util.Locale;

import org.apache.commons.validator.Field;
import org.apache.commons.validator.GenericTypeValidator;
import org.apache.commons.validator.GenericValidator;
import org.apache.commons.validator.ValidatorAction;
import org.apache.commons.validator.util.ValidatorUtils;
import org.apache.log4j.Logger;

import com.anyoption.common.beans.base.ApiErrorCode;
import com.anyoption.common.service.results.ErrorMessage;
import com.anyoption.util.CommonUtil;

/**
 * 
 * @author liors
 *
 */
public class ValidatorFieldChecks {
    private static final Logger log = Logger.getLogger(ValidatorFieldChecks.class);

    public static final String MSGS_PARAM = "java.util.ArrayList";

    /**
     * @param bean
     * @param va
     * @param field
     * @param msgs
     * @param locale
     * @return
     */
    public static boolean validateRequired(Object bean, ValidatorAction va, Field field, ArrayList<ErrorMessage> msgs, Locale locale) {
        String value = ValidatorUtils.getValueAsString(bean, field.getProperty());
        if (log.isTraceEnabled()) {
            log.trace("validateRequired - field: " + field.getProperty() + " value: " + value);
        }
        if (GenericValidator.isBlankOrNull(value)) {
            msgs.add(new ErrorMessage(field.getProperty(), CommonUtil.getMessage(locale, va.getMsg(), new String[] {field.getProperty()}), ApiErrorCode.API_ERROR_CODE_VALIDATION_MANDATORY_FIELD));
            return false;
        }
        return true;
    }

    /**
     * @param bean
     * @param va
     * @param field
     * @param msgs
     * @param locale
     * @return
     */
    public static boolean validateMask(Object bean, ValidatorAction va, Field field, ArrayList<ErrorMessage> msgs, Locale locale) {
        String value = ValidatorUtils.getValueAsString(bean, field.getProperty());
        String mask = field.getVarValue("mask");
        if (log.isTraceEnabled()) {
            log.trace("validateMask - field: " + field.getProperty() + " value: " + value + " mask: " + mask);
        }
        if (null == value || value.trim().equals("")) {
            return true;
        }
        if(locale.getLanguage().equals("iw")) {
        	if (!GenericValidator.matchRegexp(value, mask)) {
                msgs.add(new ErrorMessage(field.getProperty(), CommonUtil.getMessage(locale, va.getMsg(), new String[] {field.getProperty()}), ApiErrorCode.API_ERROR_CODE_VALIDATION_INVALID_PARAMETER));
                return false;
            }
        } else {
        	if (!value.matches(mask)) {
        		msgs.add(new ErrorMessage(field.getProperty(), CommonUtil.getMessage(locale, va.getMsg(), new String[] {field.getProperty()}), ApiErrorCode.API_ERROR_CODE_VALIDATION_INVALID_PARAMETER));
        		return false;
        	}
        }
        return true;
    }

    /**
     * @param bean
     * @param va
     * @param field
     * @param msgs
     * @param locale
     * @return
     */
    public static boolean validateInteger(Object bean, ValidatorAction va, Field field, ArrayList<ErrorMessage> msgs, Locale locale) {
        String value = ValidatorUtils.getValueAsString(bean, field.getProperty());
        if (log.isTraceEnabled()) {
            log.trace("validateInteger - field: " + field.getProperty() + " value: " + value);
        }
        if (GenericValidator.isBlankOrNull(value)) {
            return true;
        }
        if (null == GenericTypeValidator.formatInt(value)) {
            msgs.add(new ErrorMessage(field.getProperty(), CommonUtil.getMessage(locale, va.getMsg(), new String[] {field.getProperty()}), ApiErrorCode.API_ERROR_CODE_VALIDATION_INVALID_NUMBER));
            return false;
        }
        return true;
    }

    /**
     * @param bean
     * @param va
     * @param field
     * @param msgs
     * @param locale
     * @return
     */
    public static boolean validateLong(Object bean, ValidatorAction va, Field field, ArrayList<ErrorMessage> msgs, Locale locale) {
        String value = ValidatorUtils.getValueAsString(bean, field.getProperty());
        if (log.isTraceEnabled()) {
            log.trace("validateLong - field: " + field.getProperty() + " value: " + value);
        }
        if (GenericValidator.isBlankOrNull(value)) {
            return true;
        }
        if (null == GenericTypeValidator.formatLong(value)) {
            msgs.add(new ErrorMessage(field.getProperty(), CommonUtil.getMessage(locale, va.getMsg(), new String[] {field.getProperty()}), ApiErrorCode.API_ERROR_CODE_VALIDATION_INVALID_NUMBER));
            return false;
        }
        return true;
    }

    /**
     * @param bean
     * @param va
     * @param field
     * @param msgs
     * @param locale
     * @return
     */
    public static boolean validateFloat(Object bean, ValidatorAction va, Field field, ArrayList<ErrorMessage> msgs, Locale locale) {
        String value = ValidatorUtils.getValueAsString(bean, field.getProperty());
        if (log.isTraceEnabled()) {
            log.trace("validateLong - field: " + field.getProperty() + " value: " + value);
        }
        if (GenericValidator.isBlankOrNull(value)) {
            return true;
        }
        if (null == GenericTypeValidator.formatFloat(value)) {
            msgs.add(new ErrorMessage(field.getProperty(), CommonUtil.getMessage(locale, va.getMsg(), new String[] {field.getProperty()}), ApiErrorCode.API_ERROR_CODE_VALIDATION_INVALID_NUMBER));
            return false;
        }
        return true;
    }

    /**
     * @param bean
     * @param va
     * @param field
     * @param msgs
     * @param locale
     * @return
     */
    public static boolean validateDouble(Object bean, ValidatorAction va, Field field, ArrayList<ErrorMessage> msgs, Locale locale) {
        String value = ValidatorUtils.getValueAsString(bean, field.getProperty());
        if (log.isTraceEnabled()) {
            log.trace("validateLong - field: " + field.getProperty() + " value: " + value);
        }
        if (GenericValidator.isBlankOrNull(value)) {
            return true;
        }
        if (null == GenericTypeValidator.formatDouble(value)) {
            msgs.add(new ErrorMessage(field.getProperty(), CommonUtil.getMessage(locale, va.getMsg(), new String[] {field.getProperty()}), ApiErrorCode.API_ERROR_CODE_VALIDATION_INVALID_NUMBER));
            return false;
        }
        return true;
    }

    /**
     * @param bean
     * @param va
     * @param field
     * @param msgs
     * @param locale
     * @return
     */
    public static boolean validateEmail(Object bean, ValidatorAction va, Field field, ArrayList<ErrorMessage> msgs, Locale locale) {
        String value = ValidatorUtils.getValueAsString(bean, field.getProperty());
        if (log.isTraceEnabled()) {
            log.trace("validateEmail - field: " + field.getProperty() + " value: " + value);
        }
        if (GenericValidator.isBlankOrNull(value)) {
            return true;
        }
        if (!GenericValidator.isEmail(value)) {
            msgs.add(new ErrorMessage(field.getProperty(), CommonUtil.getMessage(locale, va.getMsg(), new String[] {field.getProperty()}), ApiErrorCode.API_ERROR_CODE_VALIDATION_INVALID_EMAIL));
            return false;
        }

        if (!CommonUtil.isEmailValidUnix(value)) {
        	msgs.add(new ErrorMessage(field.getProperty(), CommonUtil.getMessage(locale, va.getMsg(), new String[] {field.getProperty()}), ApiErrorCode.API_ERROR_CODE_VALIDATION_INVALID_EMAIL));
            return false;
        }
        return true;
    }

    /**
     * @param bean
     * @param va
     * @param field
     * @param msgs
     * @param locale
     * @return
     */
    public static boolean validateEqualTo(Object bean, ValidatorAction va, Field field, ArrayList<ErrorMessage> msgs, Locale locale) {
    	String value1 = ValidatorUtils.getValueAsString(bean, field.getProperty());
    	String eqaulToProp = field.getVarValue("equalFieldName");
    	String value2 = ValidatorUtils.getValueAsString(bean, eqaulToProp);
        if (log.isTraceEnabled()) {
            log.trace("validateEqualTo - field1: " + field.getProperty() + " value1: " + value1 +
            		" field2: " + eqaulToProp + " value2: " + value2);
        }
        boolean res = value1.equals(value2);
        if (!res) {
        	msgs.add(new ErrorMessage(field.getProperty(), CommonUtil.getMessage(locale, va.getMsg(), new String[] {field.getProperty(), eqaulToProp}), ApiErrorCode.API_ERROR_CODE_VALIDATION_RETYPED_DOES_NOT_MATCH));
        }
        return res;
    }

    /**
     * @param bean
     * @param va
     * @param field
     * @param msgs
     * @param locale
     * @return
     */
    public static boolean validateIntegerRange(Object bean, ValidatorAction va, Field field, ArrayList<ErrorMessage> msgs, Locale locale) {
        String value = ValidatorUtils.getValueAsString(bean, field.getProperty());
        String min = field.getVarValue("min");
        String max = field.getVarValue("max");
        if (log.isTraceEnabled()) {
            log.trace("validateIntegerRange - field: " + field.getProperty() + " value: " + value + " min: " + min + " max: " + max);
        }
        if (GenericValidator.isBlankOrNull(value)) {
            return true;
        }
        Integer val = GenericTypeValidator.formatInt(value);
        if (val < Integer.parseInt(min) || val > Integer.parseInt(max)) {
            msgs.add(new ErrorMessage(field.getProperty(), CommonUtil.getMessage(locale, va.getMsg(), new String[] {field.getProperty(), min, max})));
            return false;
        }

        return true;
    }

    /**
     * @param bean
     * @param va
     * @param field
     * @param msgs
     * @param locale
     * @return
     */
    public static boolean validateBirthYear(Object bean, ValidatorAction va, Field field, ArrayList<ErrorMessage> msgs, Locale locale) {
        String value = ValidatorUtils.getValueAsString(bean, field.getProperty());
        String max = field.getVarValue("max");
        if (log.isTraceEnabled()) {
            log.trace("validateIntegerRange - field: " + field.getProperty() + " value: " + value + " max: " + max);
        }
        if (GenericValidator.isBlankOrNull(value)) {
            return true;
        }
        Integer val = GenericTypeValidator.formatInt(value);
        if (val > Integer.parseInt(max)) {
            msgs.add(new ErrorMessage(field.getProperty(), CommonUtil.getMessage(locale, va.getMsg(), new String[] {field.getProperty(), max})));
            return false;
        }

        return true;
    }
    
    /**
     * @param bean
     * @param va
     * @param field
     * @param msgs
     * @param locale
     * @return
     */
    public static boolean validateName(Object bean, ValidatorAction va, Field field, ArrayList<ErrorMessage> msgs, Locale locale) {
        String value = ValidatorUtils.getValueAsString(bean, field.getProperty());
        String mask = CommonUtil.getMessage(locale, "general.validate.name", null) + "{1,15}";
        if (log.isTraceEnabled()) {
            log.trace("validateName - field: " + field.getProperty() + " value: " + value + " mask: " + mask);
        }
        if (null == value || value.trim().equals("")) {
            return true;
        }
        if(locale.getLanguage().equals("iw")) {
        	if (!GenericValidator.matchRegexp(value, mask)) {
        		msgs.add(new ErrorMessage(field.getProperty(), CommonUtil.getMessage(locale, va.getMsg(), new String[] {field.getProperty()}), ApiErrorCode.API_ERROR_CODE_VALIDATION_LETTERS_ONLY));
        		return false;
        	}
        } else {
        	if (!value.matches(mask)) {
        		msgs.add(new ErrorMessage(field.getProperty(), CommonUtil.getMessage(locale, va.getMsg(), new String[] {field.getProperty()}), ApiErrorCode.API_ERROR_CODE_VALIDATION_LETTERS_ONLY));
        		return false;
        	}
        }
        return true;
    } 
    

    /**
     * @param bean
     * @param va
     * @param field
     * @param msgs
     * @param locale
     * @return
     */
    public static boolean validateMaxCharactersRange(Object bean, ValidatorAction va, Field field, ArrayList<ErrorMessage> msgs, Locale locale) {
        String value = ValidatorUtils.getValueAsString(bean, field.getProperty());
        String max = field.getVarValue("max");
        if (log.isTraceEnabled()) {
            log.trace("validateMaxCharactersRange - field: " + field.getProperty() + " value: " + value + " max: " + max);
        }
        if (GenericValidator.isBlankOrNull(value)) {
            return true;
        }
        Integer val = value.length();
        if (val > Integer.parseInt(max)) {
            msgs.add(new ErrorMessage(field.getProperty(), CommonUtil.getMessage(locale, va.getMsg(), new String[] { max, field.getProperty()}), ApiErrorCode.API_ERROR_CODE_VALIDATION_VALUE_LONGER_THAN_ALLOWED));
            return false;
        }
        return true;
    }
    
    /**
     * @param bean
     * @param va
     * @param field
     * @param msgs
     * @param locale
     * @return
     */
    public static boolean validateMinCharactersRange(Object bean, ValidatorAction va, Field field, ArrayList<ErrorMessage> msgs, Locale locale) {
        String value = ValidatorUtils.getValueAsString(bean, field.getProperty());
        String min = field.getVarValue("min");
        if (log.isTraceEnabled()) {
            log.trace("validateMinCharactersRange - field: " + field.getProperty() + " value: " + value + " min: " + min);
        }
        if (GenericValidator.isBlankOrNull(value)) {
            return true;
        }
        Integer val = value.length();
        if (val < Integer.parseInt(min)) {
        	msgs.add(new ErrorMessage(field.getProperty(), CommonUtil.getMessage(locale, va.getMsg(), new String[] { min, field.getProperty()}), ApiErrorCode.API_ERROR_CODE_VALIDATION_VALUE_SHORTER_THAN_ALLOWED));
            return false;        	
        }
        return true;
    }
    
    /**
     * @param bean
     * @param va
     * @param field
     * @param msgs
     * @param locale
     * @return
     */
    public static boolean validateOnlyDigits(Object bean, ValidatorAction va, Field field, ArrayList<ErrorMessage> msgs, Locale locale) {
        String value = ValidatorUtils.getValueAsString(bean, field.getProperty());
        String mask = "^[0-9]+$";
        if (log.isTraceEnabled()) {
            log.trace("validateOnlyDigits - field: " + field.getProperty() + " value: " + value + " mask: " + mask);
        }
        if (GenericValidator.isBlankOrNull(value)) {
            return true;
        }
        if (!GenericValidator.matchRegexp(value, mask)) {
            msgs.add(new ErrorMessage(field.getProperty(), CommonUtil.getMessage(locale, va.getMsg(), new String[] {field.getProperty()}), ApiErrorCode.API_ERROR_CODE_VALIDATION_INVALID_NUMBER));
            return false;
        }
        return true;
    }
    
    /**
     * @param bean
     * @param va
     * @param field
     * @param msgs
     * @param locale
     * @return
     */
    public static boolean validateIP(Object bean, ValidatorAction va, Field field, ArrayList<ErrorMessage> msgs, Locale locale) {
        String value = ValidatorUtils.getValueAsString(bean, field.getProperty());
        String mask = "^([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\." +
        				"([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\." +
        				"([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\." +
        				"([01]?\\d\\d?|2[0-4]\\d|25[0-5])$";
        if (log.isTraceEnabled()) {
            log.trace("validateIP - field: " + field.getProperty() + " value: " + value + " mask: " + mask);
        }
        if (GenericValidator.isBlankOrNull(value)) {
            return true;
        }
        if (!GenericValidator.matchRegexp(value, mask)) {
            msgs.add(new ErrorMessage(field.getProperty(), CommonUtil.getMessage(locale, va.getMsg(), new String[] {field.getProperty()}), ApiErrorCode.API_ERROR_CODE_VALIDATION_INVALID_IP));
            return false;
        }
        return true;
    }
    
    /**
     * @param bean
     * @param va
     * @param field
     * @param msgs
     * @param locale
     * @return
     */
    public static boolean validateBoolean(Object bean, ValidatorAction va, Field field, ArrayList<ErrorMessage> msgs, Locale locale) {
        String value = ValidatorUtils.getValueAsString(bean, field.getProperty());
        String mask = "^true$|^false$";
        if (log.isTraceEnabled()) {
            log.trace("validateBoolean - field: " + field.getProperty() + " value: " + value + " mask: " + mask);
        }
        if (GenericValidator.isBlankOrNull(value)) {
            return true;
        }
        if (!GenericValidator.matchRegexp(value.toLowerCase(), mask)) {
            msgs.add(new ErrorMessage(field.getProperty(), CommonUtil.getMessage(locale, va.getMsg(), new String[] {field.getProperty()}), ApiErrorCode.API_ERROR_CODE_VALIDATION_INVALID_PARAMETER));
            return false;
        }
        return true;
    }
    
    /**
     * @param bean
     * @param va
     * @param field
     * @param msgs
     * @param locale
     * @return
     */
    public static boolean validateUTCOffset(Object bean, ValidatorAction va, Field field, ArrayList<ErrorMessage> msgs, Locale locale) {
        String value = ValidatorUtils.getValueAsString(bean, field.getProperty());
        String mask = "-720|-660|-600|-570|-540|-480|-420|-360|-300|-270|-240|-210|-180|-120|-60|0|60|120|180|210|240|270|300|330|345|360|390|420|480|525|540|570|600|630|660|690|720|765|780|840";
        if (log.isTraceEnabled()) {
            log.trace("validateUTCOffset - field: " + field.getProperty() + " value: " + value + " mask: " + mask);
        }
        if (GenericValidator.isBlankOrNull(value)) {
            return true;
        }
        if (!GenericValidator.matchRegexp(value, mask)) {
            msgs.add(new ErrorMessage(field.getProperty(), CommonUtil.getMessage(locale, va.getMsg(), new String[] {field.getProperty()}), ApiErrorCode.API_ERROR_CODE_VALIDATION_INVALID_PARAMETER));
            return false;
        }
        return true;
    }  
    
    /**
     * @param bean
     * @param va
     * @param field
     * @param msgs
     * @param locale
     * @return
     */
    public static boolean validateDateFormat(Object bean, ValidatorAction va, Field field, ArrayList<ErrorMessage> msgs, Locale locale) {
        String value = ValidatorUtils.getValueAsString(bean, field.getProperty());
        String datePattern = "yyyy-MM-dd";
        if (log.isTraceEnabled()) {
            log.trace("validateDateFormat - field: " + field.getProperty() + " value: " + value + " mask: " + datePattern);
        }
        if (GenericValidator.isBlankOrNull(value)) {
            return true;
        }        
        
        if (!GenericValidator.isDate(value, datePattern, true)) {
            msgs.add(new ErrorMessage(field.getProperty(), CommonUtil.getMessage(locale, va.getMsg(), new String[] {field.getProperty()}), ApiErrorCode.API_ERROR_CODE_VALIDATION_INVALID_DATE_FORMAT));
            return false;
        }
        return true;
    } 
    
    /**
     * Validate gender
     * @param bean
     * @param va
     * @param field
     * @param msgs
     * @param locale
     * @return true if pass validation
     */
    public static boolean validateGender(Object bean, ValidatorAction va, Field field, ArrayList<ErrorMessage> msgs, Locale locale) {
        String value = ValidatorUtils.getValueAsString(bean, field.getProperty());
        String mask = "^m$|^f$";
        if (log.isTraceEnabled()) {
            log.trace("validateGender - field: " + field.getProperty() + " value: " + value + " mask: " + mask);
        }
        if (GenericValidator.isBlankOrNull(value)) {
            return true;
        }
        if (!GenericValidator.matchRegexp(value, mask)) {
            msgs.add(new ErrorMessage(field.getProperty(), CommonUtil.getMessage(locale, va.getMsg(), new String[] {field.getProperty()}), ApiErrorCode.API_ERROR_CODE_VALIDATION_INVALID_NUMBER));
            return false;
        }
        return true;
    }   
}