package com.anyoption.json.util;

/**
 * MBean interface implemented by the JMXCacheClean to alow managing clear objects from cache over JMX.
 *
 * @author Eyal G
 */
public interface JMXCacheCleanMBean {

	/**
	 * get the info we have in the cache about the apk name (version, url, force update)
	 * @param apkName - the apk name we want to get info about
	 * @return the cache info we have about that apk name
	 */
	public String getApkInfo(String apkName);

	/**
	 * clear the cache for this apk name
	 * @param apkName apk name that we want to clear his name
	 */
	public void clearApkInfo(String apkName);

}
