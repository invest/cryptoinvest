package com.anyoption.json.util;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.concurrent.ConcurrentLinkedQueue;

import org.apache.log4j.Logger;

/**
 * A thread that queue and send pixels
 *
 * @author Eyal
 */
public class PixelHitter extends Thread {
    private static final Logger log = Logger.getLogger(PixelHitter.class);

    private boolean running;
    private long queueSize;

    //this is the queue of pending requests for Pixel to hit
    private ConcurrentLinkedQueue<String> toSendRequests = new ConcurrentLinkedQueue<String>();

    public PixelHitter() {
        queueSize = 0;
    }

    public void run() {
        running = true;
        log.warn("PixelHitter started");

        while (running) {
            Thread.currentThread().setName("PixelHitter");
            if (log.isTraceEnabled()) {
                log.trace("PixelHitter starting work.");
            }

            //go on until there are requests in the queue
            String nextRequest = null;
            try {
                // just get the message from the head but don't remove it
                while ((nextRequest = toSendRequests.peek()) != null) {
					URL url = new URL(nextRequest);
					HttpURLConnection conn = (HttpURLConnection) url.openConnection();
					conn.setConnectTimeout(15000);
					conn.setReadTimeout(15000);
					conn.connect();
					//only to keep connection open until we get response;
					InputStream in = conn.getInputStream();
					conn.disconnect();
                    toSendRequests.poll(); // now that the message was sent we can remove it
                    synchronized (this) {
                        queueSize--;
                    }
                    if (queueSize > 100) {
                        log.warn("Dispatched. queueSize: " + queueSize);
                    } else if (log.isTraceEnabled()) {
                        log.trace("Dispatched. queueSize: " + queueSize);
                    }
                }
            } catch (Exception e) {
            	log.warn("cant send pixel request = " + nextRequest, e);
                // if we can't send we have nothing to do and we will fall asleep until better times
            }

            if (log.isTraceEnabled()) {
                log.trace("PixelHitter falling asleep.");
            }

            try {
                synchronized (this) {
                    this.wait();
                }
            } catch (InterruptedException ie) {
                // do nothing
            }
        }

        log.warn("PixelHitter ends");
    }

    /**
     * Queue a message to be sent to the LevelsService.
     *
     * @param message the message to send
     */
    public void send(String pixel) {

        toSendRequests.add(pixel);
        synchronized (this) {
            queueSize++;
            try {
                this.notify();
            } catch (Throwable t) {
                log.error("Can't wake up the PixelHitter.", t);
            }
        }
    }

    /**
     * Wake up the sender to try to send messages if available.
     */
    public void wakeUp() {
        synchronized (this) {
            try {
                this.notify();
            } catch (Throwable t) {
                log.error("Can't wake up the PixelHitter.", t);
            }
        }
    }

    public void stopSenderThread() {
        running = false;
        wakeUp();
    }

    public int getQueueSize() {
        return toSendRequests.size();
    }
}