package com.anyoption.json.test;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import com.anyoption.json.requests.GeoTagRequest;
import com.anyoption.json.results.GeoTagResult;
import com.anyoption.json.util.JsonExclusionStrategy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class GeoTagTest {

		private final String USER_AGENT = "Mozilla/5.0";
	 
		public static void main(String[] args) throws Exception {
			GeoTagTest  http = new GeoTagTest ();
			System.out.println("Testing - GeoTag\n");
			http.sendPost();
		}
	 

		// HTTP POST request
		private void sendPost() throws Exception {
	 
//			String url = "http://localhost/geoTagService/EmailValidatorService";
			String url = "http://www.testenv.anyoption.com/geoTagService/";
			URL obj = new URL(url);
			HttpURLConnection con = (HttpURLConnection) obj.openConnection();
	 
			//add request header
			con.setRequestMethod("POST");
			con.setRequestProperty("User-Agent", USER_AGENT);
			con.setRequestProperty("Accept-Language", "en-US,en;q=0.5");

	    	Gson gson = new GsonBuilder().setExclusionStrategies(new JsonExclusionStrategy()).serializeNulls().create();
	    	
			// deserialize the JSON request from client
	    	GeoTagRequest geoTagRequest = new GeoTagRequest();
	    	geoTagRequest.setIp("92.247.82.234");
					
			String json = gson.toJson(geoTagRequest);

			// Send post request
			con.setDoOutput(true);
			DataOutputStream wr = new DataOutputStream(con.getOutputStream());
			wr.writeBytes(json);
			wr.flush();
			wr.close();
	 
			int responseCode = con.getResponseCode();
			System.out.println("Sending 'POST' request to URL : " + url);
			System.out.println("Post parameters : " + json);
			System.out.println("Response Code : " + responseCode);
	 
			BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
			GeoTagResult result = gson.fromJson(in, GeoTagResult.class);
			in.close();
	 
			//print result
			System.out.println(result);
		}
	 
}

