package com.anyoption.json.test;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import com.anyoption.common.service.requests.EmailValidatorRequest;
import com.anyoption.common.service.results.EmailValidatorResult;
import com.anyoption.json.util.JsonExclusionStrategy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class EmailValidatorTest {

		private final String USER_AGENT = "Mozilla/5.0";
	 
		public static void main(String[] args) throws Exception {
			EmailValidatorTest  http = new EmailValidatorTest ();
			System.out.println("Testing - EmailValidatorService\n");
			http.sendPost();
		}
	 

		// HTTP POST request
		private void sendPost() throws Exception {
	 
//			String url = "http://localhost/emailValidatorService";
			String url = "http://www.testenv.anyoption.com/emailValidationService";
			URL obj = new URL(url);
			HttpURLConnection con = (HttpURLConnection) obj.openConnection();
	 
			//add request header
			con.setRequestMethod("POST");
			con.setRequestProperty("User-Agent", USER_AGENT);
			con.setRequestProperty("Accept-Language", "en-US,en;q=0.5");

	    	Gson gson = new GsonBuilder().setExclusionStrategies(new JsonExclusionStrategy()).serializeNulls().create();
	    	
			// deserialize the JSON request from client
			EmailValidatorRequest validationRequest = new EmailValidatorRequest();
			validationRequest.setEmail("pavlinsm@anyoption.com");
					
			String json = gson.toJson(validationRequest);

			// Send post request
			con.setDoOutput(true);
			DataOutputStream wr = new DataOutputStream(con.getOutputStream());
			wr.writeBytes(json);
			wr.flush();
			wr.close();
	 
			int responseCode = con.getResponseCode();
			System.out.println("Sending 'POST' request to URL : " + url);
			System.out.println("Post parameters : " + json);
			System.out.println("Response Code : " + responseCode);
	 
			BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
			EmailValidatorResult result = gson.fromJson(in, EmailValidatorResult.class);
			in.close();
	 
			//print result
			System.out.println(result);
		}
	 
}

