package com.invest.validator;

import java.util.ArrayList;
import java.util.Locale;

import org.apache.commons.validator.Validator;
import org.apache.commons.validator.ValidatorException;

import com.anyoption.common.service.results.ErrorMessage;
import com.anyoption.json.service.AnyoptionService;
import com.anyoption.json.service.AnyoptionServiceServlet;
import com.anyoption.json.util.ValidatorFieldChecks;


public class Validators {
	
    public static ArrayList<ErrorMessage> validateInput(String formName, Object bean, Locale locale) throws ValidatorException {
        ArrayList<ErrorMessage> msgs = new ArrayList<ErrorMessage>();
        Validator validator = new Validator(AnyoptionServiceServlet.getValidatorResources(), formName);
        validator.setParameter(Validator.BEAN_PARAM, bean);
        validator.setParameter(ValidatorFieldChecks.MSGS_PARAM, msgs);
        validator.setParameter(Validator.LOCALE_PARAM, locale);
        validator.setClassLoader(AnyoptionService.class.getClassLoader());
        validator.validate();
        return msgs;
    }
    
}
