package com.invest.service.usertransaction;

/**
 * @author Kiril.m
 */
class EmailTemplateProperties {

	private long userId;
	private String userEmail;
	private String firstName;
	private String toCurrency;
	private double depositAmount;
	private String clientCurrency;

	public EmailTemplateProperties(	long userId, String userEmail, String firstName, String toCurrency, double depositAmount,
									String clientCurrency) {
		this.userId = userId;
		this.userEmail = userEmail;
		this.firstName = firstName;
		this.toCurrency = toCurrency;
		this.depositAmount = depositAmount;
		this.clientCurrency = clientCurrency;
	}

	public long getUserId() {
		return userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}

	public String getUserEmail() {
		return userEmail;
	}

	public void setUserEmail(String userEmail) {
		this.userEmail = userEmail;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getToCurrency() {
		return toCurrency;
	}

	public void setToCurrency(String toCurrency) {
		this.toCurrency = toCurrency;
	}

	public double getDepositAmount() {
		return depositAmount;
	}

	public void setDepositAmount(double depositAmount) {
		this.depositAmount = depositAmount;
	}

	public String getClientCurrency() {
		return clientCurrency;
	}

	public void setClientCurrency(String clientCurrency) {
		this.clientCurrency = clientCurrency;
	}

	@Override
	public String toString() {
		String ls = System.lineSeparator();
		return ls + this.getClass().getName() + ls
				+ super.toString() + ls
				+ "userId: " + userId + ls
				+ "userEmail: " + userEmail + ls
				+ "firstName: " + firstName + ls
				+ "toCurrency: " + toCurrency + ls
				+ "depositAmount: " + depositAmount + ls
				+ "clientCurrency: " + clientCurrency + ls;
	}
}