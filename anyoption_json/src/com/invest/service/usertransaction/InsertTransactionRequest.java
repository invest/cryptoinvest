package com.invest.service.usertransaction;

import com.anyoption.common.service.requests.UserMethodRequest;

/**
 * @author Kiril.m
 */
public class InsertTransactionRequest extends UserMethodRequest {

	private long amount;
	
	public InsertTransactionRequest(long amount, String ip) {
		this.amount = amount;
		this.ip = ip;
	}

	public InsertTransactionRequest() {
	}

	public long getAmount() {
		return amount;
	}

	public void setAmount(long amount) {
		this.amount = amount;
	}

	@Override
	public String toString() {
		String ls = System.lineSeparator();
		return ls + this.getClass().getName() + ls
				+ super.toString() + ls
				+ "amount: " + amount + ls;
	}
}