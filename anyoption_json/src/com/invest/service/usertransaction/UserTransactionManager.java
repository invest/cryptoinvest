package com.invest.service.usertransaction;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;

import org.apache.log4j.Logger;

import com.anyoption.common.clearing.CardPayClearingProvider;
import com.anyoption.common.clearing.CardPayInfo;
import com.anyoption.common.clearing.ClearingManagerConstants;
import com.anyoption.common.managers.BaseBLManager;
import com.anyoption.common.managers.TransactionsManagerBase;
import com.anyoption.managers.ClearingManager;
import com.anyoption.util.CommonUtil;
import com.invest.common.beans.CryptoTrade;
import com.invest.common.email.beans.EmailTemplate;
import com.invest.common.email.factories.EmailProviderFactory;
import com.invest.common.email.factories.EmailTemplateFactory;
import com.invest.common.enums.OrderState;
import com.invest.common.service.cryptotransaction.CryptoTransactionsManagerBase;

/**
 * @author Kiril.m
 */
public class UserTransactionManager extends BaseBLManager {

	private static final Logger log = Logger.getLogger(UserTransactionManager.class);

	public static long insertBankwireDeposit(long userId, long amount, long writerId, String ip) {
		Connection con = null;
		try {
			con = getConnection();
			// TODO when to update user balance
			con.setAutoCommit(false);
			long transactionId = UserTransactionDAO.insertDeposit(	con, userId, amount, TransactionsManagerBase.TRANS_TYPE_WIRE_DEPOSIT,
																	TransactionsManagerBase.TRANS_STATUS_PENDING, writerId, ip, 0L);
			if (transactionId > 0) {
				UserTransactionDAO.insertTransactionResolutionRows(con, transactionId);
				con.commit();
				return transactionId;
			} else {
				log.debug("Transaction was not inserted for user " + userId);
				con.rollback();
				return -1L;
			}
		} catch (SQLException e) {
			log.debug("Unable to insert deposit for user " + userId, e);
			try {
				con.rollback();
			} catch (SQLException e1) {
				log.debug("Cannot rollback connection", e1);
			}
			return -1L;
		} finally {
			try {
				con.setAutoCommit(true);
			} catch (SQLException e) {
				log.debug("Cannot return connection back to autocommit", e);
			}
			closeConnection(con);
		}
	}
	
	public static void placeOrder(CryptoTrade trn, long currencyId) throws SQLException {
		Connection con = null;
		try {
			con = getConnection();
			UserTransactionDAO.placeOrder(con, trn, currencyId);
		} finally {
			closeConnection(con);
		}
	}
	
	public static int getTotalDepositsPerMonth(int userId) throws SQLException {
		Connection con = null;
		try {
			con = getConnection();
			return UserTransactionDAO.getTotalDepositsPerMonth(userId, con);
		} finally {
			closeConnection(con);
		}
	}
	
	public static ArrayList<UserOrderHistoryReport> getUserOrderHistory(OrderHistoryRequest request) throws SQLException {
		Connection con = null;
		try {
			con = getConnection();
			return UserTransactionDAO.getUserOrderHistory(request, con);
		} finally {
			closeConnection(con);
		}
	}

	public static CardPayInfo insertCardPayDeposit(	long userId, long amount, String userEmail, String userCurrencyCode, long writerId,
													String ip) {
		if (!(ClearingManager	.getClearingProviders()
								.get(ClearingManagerConstants.CARD_PAY_PROVIDER_ID) instanceof CardPayClearingProvider)) {
			log.debug("Incorrect clearing provider, expected " + CardPayClearingProvider.class.getName());
			return null;
		}
		CardPayClearingProvider provider = (CardPayClearingProvider) ClearingManager.getClearingProviders()
																					.get(ClearingManagerConstants.CARD_PAY_PROVIDER_ID);
		Connection con = null;
		long transactionId;
		try {
			con = getConnection();
			con.setAutoCommit(false);
			transactionId = UserTransactionDAO.insertDeposit(	con, userId, amount, TransactionsManagerBase.TRANS_TYPE_CARDPAY_DEPOSIT,
																TransactionsManagerBase.TRANS_STATUS_STARTED, writerId, ip,
																provider.getId());
			if (transactionId > 0) {
				UserTransactionDAO.insertTransactionResolutionRows(con, transactionId);
				con.commit();
			} else {
				log.debug("Transaction was not inserted for user " + userId);
				con.rollback();
				return null;
			}
		} catch (SQLException e) {
			log.debug("Unable to insert deposit for user " + userId, e);
			try {
				con.rollback();
			} catch (SQLException e1) {
				log.debug("Cannot rollback connection", e1);
			}
			return null;
		} finally {
			try {
				con.setAutoCommit(true);
			} catch (SQLException e) {
				log.debug("Cannot return connection back to autocommit", e);
			}
			closeConnection(con);
		}
		CardPayInfo info = new CardPayInfo(	transactionId, "Executed from java service", userCurrencyCode,
											new BigDecimal(amount).divide(new BigDecimal("100")), userEmail);
		try {
			provider.constructParameters(info);
		} catch (Throwable e) {
			log.debug("Unable to construct parameters", e);
			return null;
		}
		return info;
	}

	public static boolean updateCardPayDeposit(	long transactionId, String status, long cardPayId, String description, int declineCode,
												boolean threeD, String declineReason) {
		if (transactionId <= 0) {
			log.debug("No transaction with id " + transactionId);
			return false;
		}
		int statusId = -1;
		switch (status) {
		case CardPayClearingProvider.STATUS_APPROVED:
//			statusId = TransactionsManagerBase.TRANS_STATUS_SUCCEED;
			statusId = TransactionsManagerBase.TRANS_STATUS_PENDING;
			break;

		case CardPayClearingProvider.STATUS_DECLINED:
			statusId = TransactionsManagerBase.TRANS_STATUS_FAILED;
			break;

		case CardPayClearingProvider.STATUS_PENDING:
//			statusId = TransactionsManagerBase.TRANS_STATUS_PENDING;
			statusId = TransactionsManagerBase.TRANS_STATUS_STARTED;
			break;

		default:
			break;
		}
		StringBuilder commentBuilder = new StringBuilder("#");
		commentBuilder	.append(CardPayClearingProvider.XML_CARD_PAY_ID_ATTRIBUTE).append(":").append(cardPayId).append(",")
						.append(CardPayClearingProvider.XML_DESCRIPTION_ATTRIBUTE).append(":").append(description).append(",")
						.append(CardPayClearingProvider.XML_STATUS_ATTRIBUTE).append(":").append(status).append(",")
						.append(CardPayClearingProvider.XML_DECLINE_CODE_ATTRIBUTE).append(":").append(declineCode).append("#");
		Connection con = null;
		try {
			try {
				con = getConnection();
				con.setAutoCommit(false);
				boolean res = false;
				if (updateTransaction(con, transactionId, statusId, threeD, commentBuilder.toString())) {
					if (statusId == TransactionsManagerBase.TRANS_STATUS_SUCCEED) {
						res = CryptoTransactionsManagerBase.updateCryptoTransactionState(	con, transactionId,
																							OrderState.READY_FOR_PROCESSING);
						if (res) {
							EmailTemplateProperties properties = getPropertiesForTemplate(con, transactionId);
							EmailTemplate template = EmailTemplateFactory.getCCTransactionApprovedTemplate(	properties.getUserId(),
																											CommonUtil.getConfig("email.from"),
																											properties.getUserEmail(), null,
																											properties.getFirstName(),
																											properties.getToCurrency(),
																											transactionId,
																											CommonUtil.getConfig("email.template.domain"),
																											properties.getDepositAmount(),
																											properties.getClientCurrency(),
																											properties.getToCurrency(),
																											CommonUtil.getConfig("email.template.homepage"),
																											CommonUtil.getConfig("email.support"));
							EmailProviderFactory.getEmailProvider().scheduleTemplateEmail(template);
						}
					} else {
						res = true;
						if (statusId == TransactionsManagerBase.TRANS_STATUS_FAILED) {
							EmailTemplateProperties properties = getPropertiesForTemplate(con, transactionId);
							EmailTemplateFactory.getCCTransactionFailedTemplate(properties.getUserId(), CommonUtil.getConfig("email.from"),
																				properties.getUserEmail(), null, properties.getFirstName(),
																				properties.getToCurrency(), transactionId,
																				CommonUtil.getConfig("email.template.domain"),
																				declineReason,
																				CommonUtil.getConfig("email.template.homepage"),
																				CommonUtil.getConfig("email.support"));
						}
					}
				}
				if (res) {
					con.commit();
				} else {
					con.rollback();
				}
				return res;
			} catch (SQLException e) {
				log.debug("Unable to update card pay deposit for transaction: " + transactionId, e);
				try {
					con.rollback();
				} catch (SQLException e1) {
					log.debug("Cannot rollback connection", e1);
				}
				return false;
			}
		} finally {
			try {
				con.setAutoCommit(true);
			} catch (SQLException e) {
				log.debug("Cannot return connection back to autocommit", e);
			}
			closeConnection(con);
		}
	}

	private static EmailTemplateProperties getPropertiesForTemplate(Connection con, long transactionId) throws SQLException {
		return UserTransactionDAO.getPropertiesForTemplate(con, transactionId);
	}

	private static boolean updateTransaction(	Connection con, long transactionId, int statusId, boolean threeD,
												String comments) throws SQLException {
		return UserTransactionDAO.updateTransaction(con, transactionId, statusId, threeD, comments);
	}
}