package com.invest.service.usertransaction;

import com.anyoption.common.service.results.UserMethodResult;

/**
 * @author Kiril.m
 */
public class CardPayTransactionResult extends UserMethodResult {

	private long transactionId;
	private String orderXML;
	private String sha512;

	public long getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(long transactionId) {
		this.transactionId = transactionId;
	}

	public String getOrderXML() {
		return orderXML;
	}

	public void setOrderXML(String orderXML) {
		this.orderXML = orderXML;
	}

	public String getSha512() {
		return sha512;
	}

	public void setSha512(String sha512) {
		this.sha512 = sha512;
	}
}