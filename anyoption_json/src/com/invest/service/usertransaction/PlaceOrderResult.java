/**
 * 
 */
package com.invest.service.usertransaction;

import com.anyoption.common.service.results.MethodResult;

/**
 * @author pavel.t
 *
 */
public class PlaceOrderResult extends MethodResult {

	private double amount;//500
	private String currency;//EUR
	private int accountId;//999592
	private int transactionId;//205721
	private String orderXML;
	private String sha512;
	
	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public int getAccountId() {
		return accountId;
	}

	public void setAccountId(int accountId) {
		this.accountId = accountId;
	}

	public int getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(int transactionId) {
		this.transactionId = transactionId;
	}

	public String getOrderXML() {
		return orderXML;
	}

	public void setOrderXML(String orderXML) {
		this.orderXML = orderXML;
	}

	public String getSha512() {
		return sha512;
	}

	public void setSha512(String sha512) {
		this.sha512 = sha512;
	}

	@Override
	public String toString() {
		return "PlaceOrderResult [amount=" + amount + ", currency=" + currency + ", accountId=" + accountId
				+ ", transactionId=" + transactionId + ", orderXML=" + orderXML + ", sha512=" + sha512 + "]";
	}
}