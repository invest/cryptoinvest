/**
 * 
 */
package com.invest.service.usertransaction;

import java.util.HashMap;
import java.util.Map;

import com.anyoption.common.service.results.MethodResult;

/**
 * @author pavel.t
 *
 */
public class RatesMethodResult extends MethodResult {

	private Map<String, Double> rates;
    private HashMap<String, HashMap<String, Double>> feesMinDeposits;

	public Map<String, Double> getRates() {
		return rates;
	}

	public void setRates(Map<String, Double> rates) {
		this.rates = rates;
	}

	public HashMap<String, HashMap<String, Double>> getFeesMinDeposits() {
		return feesMinDeposits;
	}

	public void setFeesMinDeposits(HashMap<String, HashMap<String, Double>> feesMinDeposits) {
		this.feesMinDeposits = feesMinDeposits;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "RatesMethodResult [rates=" + rates + ", feesMinDeposits=" + feesMinDeposits + "]";
	}
}
