/**
 * 
 */
package com.invest.service.usertransaction;

import java.util.ArrayList;

import com.anyoption.common.service.results.MethodResult;

/**
 * @author pavel.t
 *
 */
public class OrderHistoryResult extends MethodResult {

	private ArrayList<UserOrderHistoryReport> orderHistory;

	public ArrayList<UserOrderHistoryReport> getOrderHistory() {
		return orderHistory;
	}

	public void setOrderHistory(ArrayList<UserOrderHistoryReport> orderHistory) {
		this.orderHistory = orderHistory;
	}
}
