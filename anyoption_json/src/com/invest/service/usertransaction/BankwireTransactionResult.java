package com.invest.service.usertransaction;

import com.anyoption.common.service.results.UserMethodResult;

/**
 * @author Kiril.m
 */
public class BankwireTransactionResult extends UserMethodResult {

	private long transactionId;

	public long getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(long transactionId) {
		this.transactionId = transactionId;
	}
}