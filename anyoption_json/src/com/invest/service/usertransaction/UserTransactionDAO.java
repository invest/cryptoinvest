package com.invest.service.usertransaction;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;

import com.anyoption.common.daos.DAOBase;
import com.invest.common.beans.CryptoTrade;
import com.invest.common.service.cryptotransaction.CryptoTransactionsManagerBase;

import oracle.jdbc.OracleTypes;

/**
 * @author Kiril.m
 */
public class UserTransactionDAO extends DAOBase {

	public static long insertDeposit(	Connection con, long userId, long amount, int type, int status, long writerId, String ip,
										long providerId) throws SQLException {
		String sql = "insert into transactions (id, user_id, amount, type_id, status_id, writer_id, init_amount, ip, clearing_provider_id, time_created) "
						+ "values (?, ?, ?, ?, ?, ?, ?, ?, ?, sysdate)";
		int index = 1;
		try (PreparedStatement ps = con.prepareStatement(sql)) {
			long id = getSequenceNextVal(con, "SEQ_TRANSACTIONS");
			ps.setLong(index++, id);
			ps.setLong(index++, userId);
			ps.setLong(index++, amount);
			ps.setInt(index++, type);
			ps.setInt(index++, status); // 5
			ps.setLong(index++, writerId);
			ps.setLong(index++, amount);
			ps.setString(index++, ip);
			if (providerId > 0) {
				ps.setLong(index++, providerId); // 9
			} else {
				ps.setNull(index++, Types.NUMERIC); // 9
			}
			ps.executeQuery();
			return id;
		}
	}

	public static void insertTransactionResolutionRows(Connection con, long transactionId) throws SQLException {
		String sql = "insert into crypto_transaction_resolutions (transaction_id, assignee, time_created) values (?, ?, sysdate)";
		try (PreparedStatement ps = con.prepareStatement(sql)) {
			ps.setLong(1, transactionId);
			ps.setString(2, CryptoTransactionsManagerBase.ASSIGNEE_FINANCE);
			ps.addBatch();
			ps.setLong(1, transactionId);
			ps.setString(2, CryptoTransactionsManagerBase.ASSIGNEE_SUPPORT);
			ps.addBatch();
			ps.setLong(1, transactionId);
			ps.setString(2, CryptoTransactionsManagerBase.ASSIGNEE_RISK);
			ps.addBatch();
			ps.executeBatch();
		}
	}

	public static boolean updateTransaction(Connection con, long transactionId, int statusId, boolean threeD,
											String comments) throws SQLException {
		String sql = "update transactions set status_id = ?, is_3d = ?, comments = concat(comments, ?) where id = ?";
		try (PreparedStatement ps = con.prepareStatement(sql)) {
			ps.setInt(1, statusId);
			ps.setBoolean(2, threeD);
			ps.setString(3, comments);
			ps.setLong(4, transactionId);
			return ps.executeUpdate() == 1;
		}
	}

	public static void placeOrder(Connection con, CryptoTrade trn, long currencyId) throws SQLException {
		CallableStatement cstmt = null;
		ResultSet rs = null;
		int index = 1;
		
		try {
			cstmt = con.prepareCall("{call pkg_transactions.INSERT_CRYPTO_TRADE_TRN (i_market => ? " +
																					",i_from_currency => ?" +
																					",i_to_currency => ?" +
																					",i_client_currency => ?" +
																					//",i_quantity => ?" +
																					",i_quantity_client => ?" +
																					",i_amount => ?" +
																					",i_original_amount => ?" +
																					",i_fee => ?" +
																					",i_clearance_fee => ?" +
																					",i_rate => ?" +
																					",i_rate_market_client => ?" +
																					//",i_rate_market => ?" +
																					//",i_status => ?" +
																					",i_transaction_id => ?" +
																					//",i_uuid => ?" +
																					//",i_comments => ?" +
																					",i_wallet => ?" +
																					",i_user_id => ?" +
																					",i_currency_id => ?" +
																                    ",i_currency_to_set => ? )}");			
			setCallStatementValue(trn.getMarket(), index++, cstmt);
			setCallStatementValue(trn.getFromCurrency(), index++, cstmt);
			setCallStatementValue(trn.getToCurrency(), index++, cstmt);
			setCallStatementValue(trn.getClientCurrency(), index++, cstmt);
			//cstmt.setDouble(index++, trn.getQuantity());
			setCallStatementValue(trn.getQuantityClient(), index++, cstmt);
			setCallStatementValue(trn.getAmount(), index++, cstmt);
			setCallStatementValue(trn.getOriginalAmount(), index++, cstmt);
			setCallStatementValue(trn.getFee(), index++, cstmt);
			setCallStatementValue(trn.getClearanceFee(), index++, cstmt);
			setCallStatementValue(trn.getRate(), index++, cstmt);
			setCallStatementValue(trn.getRateMarketClient(), index++, cstmt);
			//cstmt.setDouble(index++, trn.getRateMarket());
			//cstmt.setDouble(index++, trn.getStatus());
			setCallStatementValue(trn.getTransactionId(), index++, cstmt);
			//cstmt.setString(index++, trn.getUuid());
			//cstmt.setString(index++, trn.getComments());
			setCallStatementValue(trn.getWallet(), index++, cstmt);
			setCallStatementValue(trn.getUserId(), index++, cstmt);
			setCallStatementValue(currencyId, index++, cstmt);
			setCallStatementValue(trn.isCurrencyNeedToBeSet(), index++, cstmt);
			
			cstmt.execute();			
		} finally {
			closeStatement(cstmt);
			closeResultSet(rs);
		}		
	}
	
	public static int getTotalDepositsPerMonth(int userId, Connection conn) throws SQLException {
		CallableStatement cstmt = null;
		ResultSet rs = null;
		int index = 1;
		int indexOut = 1;
		try {
			cstmt = conn.prepareCall("{call pkg_transactions.GET_TOTAL_DEPOSITS_PER_MONTH(i_user_id => ? " + 
																				 		",o_total_deposits_month => ?)}");
			cstmt.registerOutParameter(index++, OracleTypes.INTEGER);
			cstmt.setInt(index, userId);

			cstmt.executeQuery();
			
			return cstmt.getInt(indexOut++);

		} finally {
			closeStatement(cstmt);
			closeResultSet(rs);
		}
	}
	
	public static ArrayList<UserOrderHistoryReport> getUserOrderHistory(OrderHistoryRequest request, Connection conn) throws SQLException {
		
		CallableStatement cstmt = null;
		ResultSet rs = null;
		int index = 1;
		ArrayList<UserOrderHistoryReport> ordersHitory = new ArrayList<>();
		try {
			cstmt = conn.prepareCall("{call pkg_transactions.GET_ORDER_HISTORY(o_his_transactions => ? " +
																			  ",i_user_id => ?" +
																			  ",i_results_from => ?" +
																			  ",i_results_to => ?)}");
			cstmt.registerOutParameter(index++, OracleTypes.CURSOR);
			setCallStatementValue(request.getUserId(), index++, cstmt);
			setCallStatementValue(request.getResultsFrom(), index++, cstmt);
			setCallStatementValue(request.getResultsTo(), index++, cstmt);

			cstmt.executeQuery();

			rs = (ResultSet) cstmt.getObject(1);

			while (rs.next()) {
				UserOrderHistoryReport i = new UserOrderHistoryReport();
				i.setUserId(rs.getInt("user_id"));
				i.setTstamp(rs.getTimestamp("tstamp") == null ? 0 : rs.getTimestamp("tstamp").getTime());
				i.setCoin(rs.getString("coin"));
				i.setWallet(rs.getString("wallet"));
				i.setCurrency(rs.getString("currency"));
				i.setPricePerCoin(rs.getDouble("price_per_coin"));
				i.setAmount(rs.getDouble("amount"));
				i.setSubTotal(rs.getDouble("subtotal"));
				i.setTotal(rs.getInt("total"));
				i.setFee(rs.getDouble("fee"));
				i.setClearanceFee(rs.getDouble("clearance_fee"));
				i.setTransactionId(rs.getInt("transaction_id"));
				i.setStatus(rs.getInt("status"));
				i.setCreatedAt(rs.getTimestamp("created_at") == null ? 0 : rs.getTimestamp("created_at").getTime());
				i.setClosedTime(rs.getTimestamp("closed_time") == null ? 0 : rs.getTimestamp("closed_time").getTime());
				i.setComments(rs.getString("comments"));
				i.setOrderType(rs.getInt("order_type"));
				i.setPaymentMethod(rs.getInt("payment_method"));
				ordersHitory.add(i);
			}

		} finally {
			closeStatement(cstmt);
			closeResultSet(rs);
		}

		return ordersHitory;
	}
	
	private static void setCallStatementValue(Integer value, int index, CallableStatement cstmt) throws SQLException {
		if (value == null) {
			cstmt.setNull(index, OracleTypes.NUMBER);
		} else {
			cstmt.setLong(index, (Integer) value);
		}
	}
	
	private static void setCallStatementValue(Double value, int index, CallableStatement cstmt) throws SQLException {
		if (value == null) {
			cstmt.setNull(index, OracleTypes.NUMBER);
		} else {
			cstmt.setDouble(index, (Double) value);
		}
	}
	
	private static void setCallStatementValue(String value, int index, CallableStatement cstmt) throws SQLException {
		if (value == null) {
			cstmt.setNull(index, OracleTypes.VARCHAR);
		} else {
			cstmt.setString(index, (String) value);
		}
	}
	
	private static void setCallStatementValue(Long value, int index, CallableStatement cstmt) throws SQLException {
		if (value == null) {
			cstmt.setNull(index, OracleTypes.NUMBER);
		} else {
			cstmt.setLong(index, (Long) value);
		}
	}
	
	private static void setCallStatementValue(Boolean value, int index, CallableStatement cstmt) throws SQLException {
		if (value == null) {
			cstmt.setNull(index, OracleTypes.NUMBER);
		} else {
			if ((Boolean) value) {
				cstmt.setInt(index, 1);
			} else {
				cstmt.setInt(index, 0);
			}
		}
	}

	public static EmailTemplateProperties getPropertiesForTemplate(Connection con, long transactionId) throws SQLException {
		String sql = "select u.id, u.email, u.first_name, ct.to_currency, ct.original_amount / 100 as amount, ct.client_currency " + 
						"from users u " + 
						"  join crypto_trade ct on u.id = ct.user_id " + 
						"where ct.transaction_id = ?";
		try (PreparedStatement ps = con.prepareStatement(sql)) {
			ps.setLong(1, transactionId);
			ResultSet rs = ps.executeQuery();
			if (rs.next()) {
				return new EmailTemplateProperties(	rs.getLong("id"), rs.getString("email"), rs.getString("first_name"),
													rs.getString("to_currency"), rs.getDouble("amount"), rs.getString("client_currency"));
			}
		}
		return null;
	}
}