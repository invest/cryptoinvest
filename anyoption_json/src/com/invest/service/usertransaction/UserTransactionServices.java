package com.invest.service.usertransaction;

import java.sql.SQLException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.Writer;
import com.anyoption.common.beans.base.Currency;
import com.anyoption.common.beans.base.User;
import com.anyoption.common.clearing.CardPayInfo;
import com.anyoption.common.managers.CurrenciesManagerBase;
import com.anyoption.common.managers.CurrencyRatesManagerBase;
import com.anyoption.common.service.results.UserMethodResult;
import com.anyoption.managers.LimitsManagerBase;
import com.anyoption.util.CommonUtil;
import com.invest.common.beans.CryptoTrade;
import com.invest.common.email.factories.EmailProviderFactory;
import com.invest.common.email.factories.EmailTemplateFactory;
import com.invest.common.service.cryptotransaction.CryptoTransactionsManagerBase;
import com.invest.service.InvestServiceServlet;
import com.invest.service.user.UserServices;

/**
 * @author pavel.t
 *
 */
public class UserTransactionServices {

	public static final int RESPONSE_CODE_TRANSACTION_FAILED = 210;
	public static final int RESPONSE_CODE_DEPOSIT_DISABLED = 217;
	public static final int RESPONSE_CODE_MIN_DEPOSIT_BANK_WIRE_VIOLATED = 300;
	public static final int RESPONSE_CODE_MAX_DEPOSIT_PER_DAY_VIOLATED = 301;
	public static final int RESPONSE_CODE_MAX_DEPOSIT_PER_MONTH_VIOLATED = 302;
	public static final int RESPONSE_CODE_USER_CURRENCY_MISMATCH = 303;
	public static final int RESPONSE_CODE_USER_NOT_REGULATED = 304;
	public static final int RESPONSE_CODE_MAX_DEPOSIT_PER_TRN_VIOLATED = 305;

	private static final Logger log = Logger.getLogger(UserTransactionServices.class);
	private static final String PAYMENT_METHOD_BANKWIRE = "bank_transfer";
	private static final String PAYMENT_METHOD_CREDIT_CARD = "credit_card";

	public static RatesMethodResult getRates(HttpServletRequest httpRequest) {

		RatesMethodResult result = new RatesMethodResult();

		try {
			result.setRates(CurrencyRatesManagerBase.getUSDRates());
			result.setFeesMinDeposits(LimitsManagerBase.getAllCurrenciesLimitsFees());
		} catch (Exception e) {
			log.error("Can not getRates due to: ", e);
			result.setResponseCode(InvestServiceServlet.RESPONSE_CODE_UNKNOWN);
		}

		return result;
	}

	public static BankwireTransactionResult insertBankwireDeposit(InsertTransactionRequest request, HttpServletRequest httpRequest) {
		log.info("insertBankwireDeposit: " + request);
		request.setWriterId(Writer.WRITER_ID_WEB);
		BankwireTransactionResult result = new BankwireTransactionResult();
		User user = (User) httpRequest.getSession().getAttribute("user");
		if (user == null || !(user.getId() > 0)) {
			result.setResponseCode(UserServices.RESPONSE_CODE_SESSION_EXPIRED);
			return result;
		}
		long transactionId = UserTransactionManager.insertBankwireDeposit(user.getId(), request.getAmount(), request.getWriterId(), request.getIp());
		UserServices.reloadUser(result, httpRequest);
		if (result.getResponseCode() == InvestServiceServlet.RESPONSE_CODE_SUCCESS) {
			result.setTransactionId(transactionId);
		}
		return result;
	}

	public static CardPayTransactionResult insertCardPayDeposit(InsertTransactionRequest request, HttpServletRequest httpRequest) {
		log.info("insertCardPayDeposit: " + request);
		request.setWriterId(Writer.WRITER_ID_WEB);
		CardPayTransactionResult result = new CardPayTransactionResult();
		User user = (User) httpRequest.getSession().getAttribute("user");
		if (user == null || !(user.getId() > 0)) {
			result.setResponseCode(UserServices.RESPONSE_CODE_SESSION_EXPIRED);
			return result;
		}
		CardPayInfo info = UserTransactionManager.insertCardPayDeposit(	user.getId(), request.getAmount(), user.getEmail(),
																		CurrenciesManagerBase.getCurrency(user.getCurrencyId()).getCode(),
																		request.getWriterId(), request.getIp());
		if (info != null) {
			result.setTransactionId(info.getNumber());
			result.setOrderXML(info.getOrderXMLEncoded());
			result.setSha512(info.getSha512());
		} else {
			result.setResponseCode(RESPONSE_CODE_TRANSACTION_FAILED);
		}
		return result;
	}

	public static PlaceOrderResult placeOrder(PlaceOrderRequest request, HttpServletRequest httpRequest) {

		PlaceOrderResult result = new PlaceOrderResult();

		if (request == null) {
			result.setResponseCode(UserServices.RESPONSE_CODE_INVALID_INPUT);
			return result;
		}

		UserMethodResult userFromSesion = UserServices.init(httpRequest);

		if (userFromSesion.getResponseCode() == UserServices.RESPONSE_CODE_SESSION_EXPIRED) {
			result.setResponseCode(userFromSesion.getResponseCode());
			return result;
		}

		if (CryptoTransactionsManagerBase.isDepositDisabled()) {
			result.setResponseCode(RESPONSE_CODE_DEPOSIT_DISABLED);
			return result;
		}

		try {
			// get rates & min deposits & max deposits by client original amount currency
			Map<String, Double> rates = CurrencyRatesManagerBase.getUSDRates();
			HashMap<String, Double> feesMinMaxDepositByClientCurrency = LimitsManagerBase.getAllCurrenciesLimitsFees().get(request.getClientCurrency());
			double currencyId = (double) feesMinMaxDepositByClientCurrency.get("currencyId");

			User user = userFromSesion.getUser();
			boolean isCurrencyNeedToBeSet = false;
			
			
			//check if user is uploaded all necessary documents
			if (userFromSesion.getUserRegulation() == null || userFromSesion.getUserRegulation().getApprovedRegulationStep() != 2) {
				result.setResponseCode(RESPONSE_CODE_USER_NOT_REGULATED);
				return result;
			}

			if (user.getFirstDepositId() > 0 && user.getCurrencyId() != currencyId) {
				result.setResponseCode(RESPONSE_CODE_USER_CURRENCY_MISMATCH);
				return result;
			} else if (user.getFirstDepositId() == 0) {
				// trigger set first_deposit_id before placeOrder function!!!
				isCurrencyNeedToBeSet = true;
			}

			int userId = (int) user.getId();

			if (feesMinMaxDepositByClientCurrency.get("minDepositBankWire")/100 > request.getOriginalAmount()) {
				result.setResponseCode(RESPONSE_CODE_MIN_DEPOSIT_BANK_WIRE_VIOLATED);
				return result;
			}
			
			if (feesMinMaxDepositByClientCurrency.get("maxDeposit")/100 < request.getOriginalAmount()) {
				result.setResponseCode(RESPONSE_CODE_MAX_DEPOSIT_PER_TRN_VIOLATED);
				return result;
			}

			if (feesMinMaxDepositByClientCurrency.get("maxDepositPerDay")/100 < request.getOriginalAmount()) {
				result.setResponseCode(RESPONSE_CODE_MAX_DEPOSIT_PER_DAY_VIOLATED);
				return result;
			}

			if (feesMinMaxDepositByClientCurrency.get("maxDepositPerMonth")/100 < UserTransactionManager.getTotalDepositsPerMonth(userId)) {
				result.setResponseCode(RESPONSE_CODE_MAX_DEPOSIT_PER_MONTH_VIOLATED);
				return result;
			}

			long originalAmountCents = (long) request.getOriginalAmount() * 100;
			double rate = rates.get(request.getClientCurrency());
			String[] fromToCurrencyArr = request.getMarket().split("-");
			CryptoTrade trn = new CryptoTrade();
			trn.setMarket(request.getMarket());
			trn.setFromCurrency(fromToCurrencyArr[0]);
			trn.setToCurrency(fromToCurrencyArr[1]);
			trn.setOriginalAmount(originalAmountCents);
			trn.setQuantityClient(request.getQuantityClient());
			trn.setRateMarketClient(request.getRateMarketClient() * 100);// in stotinki
			trn.setWallet(request.getWallet());
			trn.setClientCurrency(request.getClientCurrency());
			trn.setUserId(userId);
			trn.setRate(rate);
			trn.setCurrencyNeedToBeSet(isCurrencyNeedToBeSet);
			// original amount to USD
			double amountUSD = CurrencyRatesManagerBase.convertToBaseAmount(originalAmountCents, (long) currencyId, new Date());
			trn.setAmount((long) amountUSD);// in stotinki
			// fees need to be calculated and populated in 'fees' and 'clearance_fee' columns
			String orderXML = null;
			String sha512 = null;
			switch (request.getMethod()) {
			case PAYMENT_METHOD_CREDIT_CARD:
				trn.setFee(request.getOriginalAmount() * feesMinMaxDepositByClientCurrency.get("creditCardFee"));
				CardPayTransactionResult cpResult = insertCardPayDeposit(	new InsertTransactionRequest(originalAmountCents, request.getIp()),
																			httpRequest);
				if (cpResult.getResponseCode() != InvestServiceServlet.RESPONSE_CODE_SUCCESS) {
					log.error("Unable to insert card pay deposit");
					result = new PlaceOrderResult();
					result.setResponseCode(cpResult.getResponseCode());
					return result;
				}
				trn.setTransactionId((int) cpResult.getTransactionId());
				trn.setClearanceFee(request.getOriginalAmount() * feesMinMaxDepositByClientCurrency.get("clearanceFee"));
				orderXML = cpResult.getOrderXML();
				sha512 = cpResult.getSha512();
				break;
			case PAYMENT_METHOD_BANKWIRE:
				// Currently we do not support bankwire
				/* falls trough */
				// trn.setFee(request.getOriginalAmount() * feesMinMaxDepositByClientCurrency.get("bankWireFee"));
				// BankwireTransactionResult wireResult = insertBankwireDeposit( new InsertTransactionRequest(originalAmountCents,
				// request.getIp()),
				// httpRequest);
				// if (wireResult.getTransactionId() < 1L) {
				// log.error("Can not insert BANK WIRE DEPOSIT!!!");
				// result.setResponseCode(InvestServiceServlet.RESPONSE_CODE_UNKNOWN);
				// return result;
				// }
				// trn.setTransactionId((int) wireResult.getTransactionId());
				// break;
			default:
				result.setResponseCode(InvestServiceServlet.RESPONSE_CODE_UNKNOWN);
				return result;
			}
			// Currently we support only EUR 
			if (currencyId != Currency.CURRENCY_EUR_ID) {
				result.setResponseCode(InvestServiceServlet.RESPONSE_CODE_UNKNOWN);
				return result;
			}
			// place crypto order and set user final currency
			UserTransactionManager.placeOrder(trn, (long) currencyId);
			result.setAmount(request.getOriginalAmount());
			result.setTransactionId(trn.getTransactionId());
			result.setCurrency(request.getClientCurrency());
			if (orderXML != null && sha512 != null) {
				result.setOrderXML(orderXML);
				result.setSha512(sha512);
			}
			if (request.getMethod().equals(PAYMENT_METHOD_BANKWIRE)) {
				EmailProviderFactory.getEmailProvider()
									.scheduleTemplateEmail(EmailTemplateFactory.getBankDetailsTemplate(	user.getId(),
																										CommonUtil.getConfig("email.from"),
																										user.getEmail(), null,
																										user.getFirstName(),
																										trn.getToCurrency(),
																										trn.getTransactionId(),
																										request.getOriginalAmount(),
																										request.getClientCurrency(),
																										CommonUtil.getConfig("email.template.homepage"),
																										CommonUtil.getConfig("email.support")));
			}
			result.setResponseCode(InvestServiceServlet.RESPONSE_CODE_SUCCESS);
		} catch (Exception e) {
			log.error("Can not place crypto order due to: ", e);
			result.setResponseCode(InvestServiceServlet.RESPONSE_CODE_UNKNOWN);
		}

		return result;
	}
	
	public static OrderHistoryResult getOrderHistory(OrderHistoryRequest request, HttpServletRequest httpRequest) {

		OrderHistoryResult result = new OrderHistoryResult();

		UserMethodResult userRes = UserServices.getUserFromSession(httpRequest);

		if (userRes.getResponseCode() == UserServices.RESPONSE_CODE_SESSION_EXPIRED) {
			result.setResponseCode(userRes.getResponseCode());
			return result;
		}

		if (request == null || userRes.getUser().getId() <= 0) {
			log.error("Can not find user in session!");
			result.setResponseCode(UserServices.RESPONSE_CODE_INVALID_INPUT);
			return result;
		}
		
		request.setUserId((int)userRes.getUser().getId());

		try {
			result.setOrderHistory(UserTransactionManager.getUserOrderHistory(request));
			result.setResponseCode(InvestServiceServlet.RESPONSE_CODE_SUCCESS);
		} catch (SQLException e) {
			log.error("Can not get order history due to: ", e);
			result.setResponseCode(InvestServiceServlet.RESPONSE_CODE_UNKNOWN);
		}

		return result;
	}
}