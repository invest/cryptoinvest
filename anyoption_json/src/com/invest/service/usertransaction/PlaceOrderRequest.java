/**
 * 
 */
package com.invest.service.usertransaction;

import com.anyoption.common.service.requests.MethodRequest;

/**
 * @author pavel.t
 *
 */
public class PlaceOrderRequest extends MethodRequest {
	
	private String market;//"USDT-ETH"
	private double originalAmount;//500
	private double quantityClient;//1.12242463
	private double rateMarketClient;//536
	private String wallet;//0x8e88ca0812cfda6f5f0ec5d2f39d1ae49432c4cc
	private String method;//bank_transfer
	private String clientCurrency;//EUR
	
	public String getMarket() {
		return market;
	}
	public void setMarket(String market) {
		this.market = market;
	}
	public double getOriginalAmount() {
		return originalAmount;
	}
	public void setOriginalAmount(double originalAmount) {
		this.originalAmount = originalAmount;
	}
	public double getQuantityClient() {
		return quantityClient;
	}
	public void setQuantityClient(double quantityClient) {
		this.quantityClient = quantityClient;
	}
	public double getRateMarketClient() {
		return rateMarketClient;
	}
	public void setRateMarketClient(double rateMarketClient) {
		this.rateMarketClient = rateMarketClient;
	}
	public String getWallet() {
		return wallet;
	}
	public void setWallet(String wallet) {
		this.wallet = wallet;
	}
	public String getMethod() {
		return method;
	}
	public void setMethod(String method) {
		this.method = method;
	}
	public String getClientCurrency() {
		return clientCurrency;
	}
	public void setClientCurrency(String clientCurrency) {
		this.clientCurrency = clientCurrency;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "PlaceOrderRequest [market=" + market + ", originalAmount=" + originalAmount + ", quantityClient="
				+ quantityClient + ", rateMarketClient=" + rateMarketClient + ", wallet=" + wallet + ", method="
				+ method + ", clientCurrency=" + clientCurrency + "]";
	}

}
