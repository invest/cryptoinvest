package com.invest.service.usertransaction;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.anyoption.common.clearing.CardPayClearingProvider;
import com.anyoption.common.clearing.CardPayInfo;
import com.anyoption.managers.ClearingManager;

/**
 * @author Kiril.m
 */
public class CardPayResultServlet extends HttpServlet {

	private static final long serialVersionUID = 4452188798169792873L;
	private static final Logger log = Logger.getLogger(CardPayResultServlet.class);
	private static final String ORDER_XML_ELEMENT = "orderXML";
	private static final String SHA512_ELEMENT = "sha512";

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String orderXML = req.getParameter(ORDER_XML_ELEMENT);
		String sha512 = req.getParameter(SHA512_ELEMENT);
		log.info(this.getClass().getName()	+ " reached with " + ORDER_XML_ELEMENT + ": " + orderXML + "; " + SHA512_ELEMENT + ": "
					+ sha512);
		if (!(ClearingManager.getClearingProviders().get(ClearingManager.CARD_PAY_PROVIDER_ID) instanceof CardPayClearingProvider)) {
			log.debug("Incorrect clearing provider, expected " + this.getClass().getName());
			// TODO return error
			return;
		}
		CardPayClearingProvider provider = (CardPayClearingProvider) ClearingManager.getClearingProviders()
																					.get(ClearingManager.CARD_PAY_PROVIDER_ID);
		CardPayInfo info = null;
		try {
			info = provider.constructResult(orderXML, sha512);
		} catch (Exception e) {
			log.debug("Unable to construct result", e);
			// TODO return error
			return;
		}
		if (info == null) {
			log.debug("Clearing provider info is null");
			// TODO return error
			return;
		}

		if (!UserTransactionManager.updateCardPayDeposit(	info.getNumber(), info.getStatus(), info.getCardPayId(), info.getDescription(),
															info.getDeclineCode(), info.isThreeD(), info.getDeclineReason())) {
			log.debug("Transaction was not updated");
			// TODO return error
			return;
		}
		log.info("Transaction updated successfully");
	}
}