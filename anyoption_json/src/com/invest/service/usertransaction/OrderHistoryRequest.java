/**
 * 
 */
package com.invest.service.usertransaction;

import com.anyoption.common.service.requests.MethodRequest;

/**
 * @author pavel.t
 *
 */
public class OrderHistoryRequest extends MethodRequest {

	private int userId;
	private int resultsFrom;
	private int resultsTo;

	/**
	 * @return the userId
	 */
	public int getUserId() {
		return userId;
	}

	/**
	 * @param userId
	 *            the userId to set
	 */
	public void setUserId(int userId) {
		this.userId = userId;
	}

	/**
	 * @return the resultsFrom
	 */
	public int getResultsFrom() {
		return resultsFrom;
	}

	/**
	 * @param resultsFrom
	 *            the resultsFrom to set
	 */
	public void setResultsFrom(int resultsFrom) {
		this.resultsFrom = resultsFrom;
	}

	/**
	 * @return the resultsTo
	 */
	public int getResultsTo() {
		return resultsTo;
	}

	/**
	 * @param resultsTo
	 *            the resultsTo to set
	 */
	public void setResultsTo(int resultsTo) {
		this.resultsTo = resultsTo;
	}
}
