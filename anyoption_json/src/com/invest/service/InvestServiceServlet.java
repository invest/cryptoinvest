package com.invest.service;

import org.apache.log4j.Logger;

import com.anyoption.common.service.CommonServiceServlet;

/**
 * @author Jamal
 */
public class InvestServiceServlet extends CommonServiceServlet {
	private static final long serialVersionUID = -3536866919755220527L;
	private static final Logger log = Logger.getLogger(InvestServiceServlet.class);
	public static final int RESPONSE_CODE_UNKNOWN = 999;
	public static final int RESPONSE_CODE_SUCCESS = 0;
	public static final int RESPONSE_CODE_INVALID_INPUT = 200;

	@Override
	public void init() {
		log.info("Invest Service init...");
		super.init();
	}
	
	protected String getPackage(String className){
		String pkg = "com.invest.service." + className.substring(0, className.indexOf("Service")) + ".";
		return pkg.toLowerCase();
	}
}