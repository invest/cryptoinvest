/**
 * 
 */
package com.invest.service.user;

import java.util.Date;

import com.anyoption.common.service.requests.MethodRequest;

/**
 * @author pavel.t
 *
 */
public class UpdateUserRequest extends MethodRequest {
	
	private String firstName;
	private String lastName;
	private String street;
	private String cityName;
	private String zipCode;
	private long countryId;
	private String mobilePhone;
	private String birthDay;
	private String birthMonth;
	private String birthYear;
	private Date birthDate;
	
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getStreet() {
		return street;
	}
	public void setStreet(String street) {
		this.street = street;
	}
	public String getCityName() {
		return cityName;
	}
	public void setCityName(String cityName) {
		this.cityName = cityName;
	}
	public String getZipCode() {
		return zipCode;
	}
	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}
	public long getCountryId() {
		return countryId;
	}
	public void setCountryId(long countryId) {
		this.countryId = countryId;
	}
	public String getMobilePhone() {
		return mobilePhone;
	}
	public void setMobilePhone(String mobilePhone) {
		this.mobilePhone = mobilePhone;
	}
	public String getBirthDay() {
		return birthDay;
	}
	public void setBirthDay(String birthDay) {
		this.birthDay = birthDay;
	}
	public String getBirthMonth() {
		return birthMonth;
	}
	public void setBirthMonth(String birthMonth) {
		this.birthMonth = birthMonth;
	}
	public String getBirthYear() {
		return birthYear;
	}
	public void setBirthYear(String birthYear) {
		this.birthYear = birthYear;
	}
	public Date getBirthDate() {
		return birthDate;
	}
	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}
	
	
	@Override
	public String toString() {
		return "UpdateUserRequest [firstName=" + firstName + ", lastName=" + lastName + ", street=" + street
				+ ", cityName=" + cityName + ", zipCode=" + zipCode + ", countryId=" + countryId + ", mobilePhone="
				+ mobilePhone + ", birthDay=" + birthDay + ", birthMonth=" + birthMonth + ", birthYear=" + birthYear
				+ ", birthDate=" + birthDate + "]";
	}

}
