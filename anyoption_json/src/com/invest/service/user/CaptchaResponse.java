/**
 * 
 */
package com.invest.service.user;

/**
 * @author pavel.t
 *
 */
public class CaptchaResponse {
	public boolean success;

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}
}