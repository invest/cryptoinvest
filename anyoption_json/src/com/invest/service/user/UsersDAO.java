package com.invest.service.user;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;

import com.anyoption.beans.User;
import com.anyoption.common.daos.DAOBase;

/**
 * @author Kiril.m
 */
public class UsersDAO extends DAOBase {

	static void saveToken(Connection con, long userId, String token, long validity, int type) throws SQLException {
		String sql = "insert into crypto_tokens (id, user_id, time_created, validity, token, token_type) values (?, ?, sysdate, ?, ?, ?)";
		try (PreparedStatement ps = con.prepareStatement(sql)) {
			long id = getSequenceNextVal(con, "SEQ_CRYPTO_TOKENS");
			ps.setLong(1, id);
			ps.setLong(2, userId);
			ps.setTimestamp(3, new Timestamp(validity));
			ps.setString(4, token);
			ps.setInt(5, type);
			ps.executeQuery();
		}	
	}

	static boolean validateToken(Connection con, String token) throws SQLException {
		String sql = "select id from crypto_tokens where token = ? and validity >= sysdate";
		try (PreparedStatement ps = con.prepareStatement(sql)) {
			ps.setString(1, token);
			ResultSet rs = ps.executeQuery();
			return rs.next();
		}
	}

	static void verifyUser(Connection con, String token) throws SQLException {
		String sql = "update users set is_authorized_mail = 1 "
					+ "where is_authorized_mail = 0 and id = (select user_id from crypto_tokens where token = ?)";
		try (PreparedStatement ps = con.prepareStatement(sql)) {
			ps.setString(1, token);
			ps.executeUpdate();
		}
	}

	static void updateUsedToken(Connection con, String token) throws SQLException {
		String sql = "update crypto_tokens set is_used = 1 where token = ?";
		try (PreparedStatement ps = con.prepareStatement(sql)) {
			ps.setString(1, token);
			ps.executeUpdate();
		}
	}
	
	public static User getUserByEmail(Connection conn, String email) throws SQLException {
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		User vo = null;
		try {
			String sql = "select u.* from users u where upper(u.user_name) = upper(?)";
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, email);
			rs = pstmt.executeQuery();
			if (rs.next()) {
				vo = new User();
				getUserVO(rs, vo);
				vo.setEmail(email);
			}
		} finally {
			closeResultSet(rs);
			closeStatement(pstmt);
		}
		return vo;
	}

	protected static void getUserVO(ResultSet rs, User vo) throws SQLException {
		vo.setId(rs.getLong("id"));
		vo.setIsActive(rs.getString("IS_ACTIVE"));
	}
	
	public static void changeMobilePhone(Connection con, String mobilePhone, long userId) throws SQLException {
		String sql = "UPDATE users SET mobile_phone = ? WHERE id = ?";
		try (PreparedStatement ps = con.prepareStatement(sql)) {
			ps.setString(1, mobilePhone);
			ps.setLong(2, userId);
			ps.executeUpdate();
		}
	}

	public static void insertSupportEmail(	Connection con, long userId, String fullName, String email, String phone, String subject,
											String message) throws SQLException {
		String sql = "insert into support_emails (id, user_id, full_name, email, phone, subject, body, time_created) "
						+ "values (?, ?, ?, ?, ?, ?, ?, sysdate)";
		try (PreparedStatement ps = con.prepareStatement(sql)) {
			long id = getSequenceNextVal(con, "SEQ_SUPPORT_EMAILS");
			ps.setLong(1, id);
			ps.setLong(2, userId);
			ps.setString(3, fullName);
			ps.setString(4, email);
			ps.setString(5, phone);
			ps.setString(6, subject);
			ps.setString(7, message);
			ps.executeQuery();
		}
	}
}