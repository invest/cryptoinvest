package com.invest.service.user;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.anyoption.common.util.Sha1;
import com.anyoption.util.CommonUtil;

/**
 * @author Kiril.m
 */
public class VerificationMailServlet extends HttpServlet {

	private static final long serialVersionUID = -667362271694295511L;
	private static final Logger log = Logger.getLogger(VerificationMailServlet.class);
	private static final String REDIRECT_URL_PARAM = "?emailConfirmed=true";

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String reqToken = req.getParameter(UsersManager.VERIFICATION_TOKEN);
		log.debug(this.getClass().getName() + " reached with token: " + reqToken);
		String userId = req.getParameter(UsersManager.VERIFICATION_USER_ID);
		String username = req.getParameter(UsersManager.VERIFICATION_USERNAME);
		String time = req.getParameter(UsersManager.VERIFICATION_TIME);
		String token;
		try {
			token = Sha1.encode(userId + UsersManager.VERIFICATION_SEPARATOR + username + UsersManager.VERIFICATION_SEPARATOR + time);
		} catch (Exception e) {
			log.debug("Unable to encode token", e);
			return;
		}
		if (token.equals(reqToken)) {
			if (UsersManager.validateToken(token)) {
				UsersManager.verifyUser(token);
				log.debug("User is verified, redirecting");
				resp.sendRedirect(CommonUtil.getConfig("fe.homepage.url.https") + REDIRECT_URL_PARAM);
			} else {
				log.debug("Received invalid token: " + token);
			}
		} else {
			log.debug("Tokens do not match. Received: "	+ reqToken + " with params: " + userId + ", " + username + ", " + time
						+ ". Calculated token: " + token);
		}
	}
}