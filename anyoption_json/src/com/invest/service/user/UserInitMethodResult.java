package com.invest.service.user;

import com.anyoption.common.beans.Country;
import com.anyoption.common.beans.base.Currency;
import com.anyoption.common.beans.base.Language;
import com.anyoption.common.service.results.UserMethodResult;

public class UserInitMethodResult extends UserMethodResult {

    private Country detectCountryByIp;
    private Currency defaultCurrencyByIp;
    private Language defaultLanguageByIp;
    private boolean depositDisabled;

	public Country getDetectCountryByIp() {
		return detectCountryByIp;
	}

	public void setDetectCountryByIp(Country detectCountryByIp) {
		this.detectCountryByIp = detectCountryByIp;
	}

	public Currency getDefaultCurrencyByIp() {
		return defaultCurrencyByIp;
	}

	public void setDefaultCurrencyByIp(Currency defaultCurrencyByIp) {
		this.defaultCurrencyByIp = defaultCurrencyByIp;
	}

	public Language getDefaultLanguageByIp() {
		return defaultLanguageByIp;
	}

	public void setDefaultLanguageByIp(Language defaultLanguageByIp) {
		this.defaultLanguageByIp = defaultLanguageByIp;
	}

	public boolean isDepositDisabled() {
		return depositDisabled;
	}

	public void setDepositDisabled(boolean depositDisabled) {
		this.depositDisabled = depositDisabled;
	}
}