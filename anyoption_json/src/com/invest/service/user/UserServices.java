package com.invest.service.user;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.Period;
import java.util.ArrayList;
import java.util.Date;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.validator.ValidatorException;
import org.apache.log4j.Logger;

import com.anyoption.beans.User;
import com.anyoption.common.beans.Country;
import com.anyoption.common.beans.Login;
import com.anyoption.common.beans.Writer;
import com.anyoption.common.beans.base.ApiErrorCode;
import com.anyoption.common.beans.base.CurrenciesRules;
import com.anyoption.common.beans.base.Currency;
import com.anyoption.common.beans.base.Language;
import com.anyoption.common.beans.base.UserRegulationBase;
import com.anyoption.common.managers.CountriesManagerBase;
import com.anyoption.common.managers.CountryManagerBase;
import com.anyoption.common.managers.CurrenciesManagerBase;
import com.anyoption.common.managers.CurrenciesRulesManagerBase;
import com.anyoption.common.managers.LanguagesManagerBase;
import com.anyoption.common.managers.LoginManager;
import com.anyoption.common.managers.LoginsManager;
import com.anyoption.common.managers.UserRegulationManager;
import com.anyoption.common.service.CommonJSONService;
import com.anyoption.common.service.requests.EmailValidatorRequest;
import com.anyoption.common.service.requests.MethodRequest;
import com.anyoption.common.service.results.EmailValidatorResult;
import com.anyoption.common.service.results.ErrorMessage;
import com.anyoption.common.service.results.MethodResult;
import com.anyoption.common.service.results.UserMethodResult;
import com.anyoption.common.service.util.EmailValidatorUtil;
import com.anyoption.common.util.MessageToFormat;
import com.anyoption.json.util.UserHelper;
import com.anyoption.managers.MarketingCombinationManagerBase;
import com.anyoption.managers.SkinsManagerBase;
import com.anyoption.managers.UsersManagerBase;
import com.anyoption.util.CommonUtil;
import com.anyoption.util.ConstantsBase;
import com.google.gson.Gson;
import com.invest.common.email.factories.EmailProviderFactory;
import com.invest.common.email.factories.EmailTemplateFactory;
import com.invest.common.service.cryptotransaction.CryptoTransactionsManagerBase;
import com.invest.service.InvestServiceServlet;
import com.invest.validator.Validators;


public class UserServices {
	private static final Logger log = Logger.getLogger(UserServices.class);
	
	//TODO move constants to separate class
	private static final String GOOGLE_SECRET_KEY = "6LfH-lYUAAAAAPbMfNe-r-w3JEPgy1wQVXOX3kPj";
	private static final String RECAPTCHA_REQUEST_PARAM = "g-recaptcha-response";
	private static final String GOOGLE_RECAPTHA_URL = "https://www.google.com/recaptcha/api/siteverify?secret=";
	private static final String VERIFICATION_URL_LINK = "verifyMail";
	public static final int RESPONSE_CODE_LOGIN_FAILED = 205;
	public static final int RESPONSE_CODE_SESSION_EXPIRED = 419;
	public static final int RESPONSE_CODE_INVALID_INPUT = 200;
	public static final int RESPONSE_CODE_USER_DOES_NOT_EXIST = 100;
	public static final int RESPONSE_CODE_PASSWORD_DOES_NOT_MATCH = 101;
	public static final String KEY_CAPTCHA = "captcha";
	public static final int RESPONSE_CODE_NOT_ACCEPTED_TERMS = 701;
	public static final int RESPONSE_CODE_INVALID_CAPTCH = 212;
	public static final int RESPONSE_CODE_GENERAL_VALIDATION = 207;
	public static final int RESPONSE_CODE_FAIL_TO_INSERT = 5000;
	public static final int RESPONSE_CODE_FAIL_TO_UPDATE = 5001;
	public static final int RESPONSE_CODE_UNABLE_TO_SEND_VERIFY_EMAIL = 10001;
	public static final int RESPONSE_CODE_UNABLE_TO_SEND_FORGOT_PASSWORD_EMAIL = 10002;
	public static final int RESPONSE_CODE_EMAIL_IN_USE = 214;
	public static final int RESPONSE_CODE_LNG_NOT_EXIST = 123;

	
	/**
     * update an existing user.
     *
     * @param request - Update User Method Request
     * @return MethodResult.
     */
	
    public static MethodResult updateUser(UpdateUserRequest request, HttpServletRequest httpRequest) {
    	log.debug("updateUser: " + request);
    	MethodResult result = new MethodResult();
    	try {
    		UserMethodResult userResult = getUserFromSession(httpRequest);
    		
			if (userResult.getResponseCode() == RESPONSE_CODE_SESSION_EXPIRED) {
				result.setResponseCode(userResult.getResponseCode());
				return result;
			}
			 
    		Locale locale = new Locale(LanguagesManagerBase.getLanguage(SkinsManagerBase.getSkin(request.getSkinId()).getDefaultLanguageId()).getCode());
    		//xml validation
    		//TODO repair regex to validate mobilePhone and birthDay
    		ArrayList<ErrorMessage> msgs = Validators.validateInput("updateUserCrytho", request, locale);
    		//under 18 validation
    		if(!isAdultEnoughToBuyCryptho18(request.getBirthYear(), request.getBirthMonth(), request.getBirthDay())) {
    			ErrorMessage errMsg = new ErrorMessage("birthYear", null, String.valueOf(ApiErrorCode.API_ERROR_CODE_VALIDATION_YEARS_OLD_18));
    			msgs.add(errMsg);
    		}
    		
            if (!msgs.isEmpty()) {
                result.setResponseCode(InvestServiceServlet.RESPONSE_CODE_INVALID_INPUT);
                result.addUserMessages(msgs);
                return result;
            }
            
            //validate  city name only
            Map<String, MessageToFormat> mtf = UsersManagerBase.validateUpdateUserDetails(request.getCityName(), locale);
            if (!mtf.isEmpty()) {
                result.setResponseCode(InvestServiceServlet.RESPONSE_CODE_INVALID_INPUT);
                for (Iterator<String> i = mtf.keySet().iterator(); i.hasNext();) {
                    String key = i.next();
                    MessageToFormat m = mtf.get(key);
                    result.addUserMessage(key, CommonUtil.getMessage(locale, m.getKey(), m.getParams()));
                }
                return result;
            }
            
            //TODO create trigger for user detail history
            User user = new User();
            
            user.setFirstName(request.getFirstName());
            user.setLastName(request.getLastName());
            user.setStreet(request.getStreet());
            user.setCityName(request.getCityName());
            user.setZipCode(request.getZipCode());
            user.setCountryId(request.getCountryId());
            user.setMobilePhone(request.getMobilePhone());
            user.setId(userResult.getUser().getId());
            user.setEmail(userResult.getUser().getEmail());
            
    		String strDate = request.getBirthYear()+"-"+request.getBirthMonth()+"-"+request.getBirthDay();
            user.setTimeBirthDate(convertStringToDate(strDate));
            
			Country c = CountriesManagerBase.getAllAllowedCountries().get(request.getCountryId());
			user.setUtcOffset(c.getGmtOffset());
            
            UsersManagerBase.updateUserDetails(user);
            
            UserRegulationBase ur = new UserRegulationBase();
            ur.setWriterId(Writer.WRITER_ID_WEB);
            ur.setFilledPersonalInformation(true);
            ur.setUserId(user.getId());
            UserRegulationManager.updateFilledPersonalInformation(ur);
            
            result.addUserMessage(null, CommonUtil.getMessage(locale, "user.update.success", null));
    	} catch (Exception e) {
            log.error("Error in updateUser", e);
            result.setResponseCode(InvestServiceServlet.RESPONSE_CODE_UNKNOWN);
        }
    	return result;
    }

	public static UserMethodResult init(HttpServletRequest httpRequest) {
		UserMethodResult res = new UserMethodResult();
		User user;
		res = getUserFromSession(httpRequest);
		if (res.getResponseCode() == RESPONSE_CODE_SESSION_EXPIRED) {
			return res;
		} else {
			try {
				user = UsersManagerBase.getById(res.getUser().getId());
				user.setPassword(null);
				res.setUser(user);
				httpRequest.getSession().setAttribute(ConstantsBase.BIND_SESSION_USER, user);
				res.setResponseCode(InvestServiceServlet.RESPONSE_CODE_SUCCESS);
				res = initUserReg(res, user);
			} catch (SQLException e) {
				log.error("Error while getting user by id due to: ", e);
				res.setResponseCode(InvestServiceServlet.RESPONSE_CODE_UNKNOWN);
			}
			return res;
		}
	}
	
	public static UserMethodResult updateUserLocale(UpdateUserLocaleRequest request, HttpServletRequest httpRequest) {
		UserMethodResult res = new UserMethodResult();
		long lngId = 0;
		res = getUserFromSession(httpRequest);
		if (res.getResponseCode() == RESPONSE_CODE_SESSION_EXPIRED) {
			return res;
		} else {
			try {

				if (request == null || request.getLanguage() == null || request.getLanguage().equals("")) {
					res.setResponseCode(InvestServiceServlet.RESPONSE_CODE_INVALID_INPUT);
					return res;
				}
				
				String lngToChange = request.getLanguage().toLowerCase();

				Hashtable<Long, Language> lngs = LanguagesManagerBase.getLanguages();

				Iterator<Map.Entry<Long, Language>> iterator = lngs.entrySet().iterator();
				while (iterator.hasNext()) {
					Map.Entry<Long, Language> entry = iterator.next();
					if (entry.getValue().getCode().toLowerCase().equals(lngToChange)) {
						lngId = entry.getKey();
						break;
					}
				}

				if (lngId <= 0) {
					res.setResponseCode(RESPONSE_CODE_LNG_NOT_EXIST);
					log.error("Language code does not exist in DB!");
					return res;
				}

				UsersManagerBase.updateUserLocale(res.getUser().getId(), lngId);
				res.getUser().setLocale(new Locale(lngToChange));
				res.setResponseCode(InvestServiceServlet.RESPONSE_CODE_SUCCESS);
			} catch (SQLException e) {
				log.error("Error while updating user locale due to: ", e);
				res.setResponseCode(InvestServiceServlet.RESPONSE_CODE_UNKNOWN);
			}
			return res;
		}
	}

	public static UserInitMethodResult getDefaultUserInit(HttpServletRequest httpRequest) {
		UserInitMethodResult res =  new UserInitMethodResult();
		//Get IP Country
		try {
			String ip = CommonUtil.getIPAddress(httpRequest);
			Long countryId = CommonJSONService.getCountryFromIp(ip, httpRequest);
			Country countryFromIp = CountryManagerBase.getCountries().get(countryId);
			res.setDetectCountryByIp(countryFromIp);
			
			//Get Default Currency by IP
			CurrenciesRules currenciesRules = new CurrenciesRules();
			currenciesRules.setCountryId(countryId);
			long defaultCurrencyId = CurrenciesRulesManagerBase.getDefaultCurrencyId(currenciesRules);
			Currency defaultCurrency = CurrenciesManagerBase.getCurrency(defaultCurrencyId);
			res.setDefaultCurrencyByIp(defaultCurrency);
			
			//Get Defaul Language by Ip
			Language defaultLanguageByIp = LanguagesManagerBase.getLanguageByCountryId(defaultCurrencyId);
			res.setDefaultLanguageByIp(defaultLanguageByIp);
			
			UserMethodResult userMethodResult = init(httpRequest);
			
			res.setUser(userMethodResult.getUser());
			res.setResponseCode(userMethodResult.getResponseCode());
			res.setUserRegulation(userMethodResult.getUserRegulation());
			res.setDepositDisabled(CryptoTransactionsManagerBase.isDepositDisabled());
		} catch (Exception e) {
			log.error("When setDeafultUserInit error:" , e);
		}
		return res;
	}
	
	public static UserMethodResult getUserFromSession(HttpServletRequest httpRequest) {
		UserMethodResult res = new UserMethodResult();
		User user;

		user = (User) httpRequest.getSession().getAttribute("user");

		if (user == null || user.getId() <= 0) {
			log.debug("User session expired!");
			res.setResponseCode(RESPONSE_CODE_SESSION_EXPIRED);
			return res;
		} else {
			res.setUser(user);
		}

		return res;
	}
	
	public static UserMethodResult login(LoginRequest request, HttpServletRequest httpRequest) {
		log.debug("getUser: " + request);
		UserMethodResult result = new UserMethodResult();
		String recap = httpRequest.getParameter(RECAPTCHA_REQUEST_PARAM);
		
		long lngId = 0;
		if (request.getLocale() != null && !request.getLocale().equals("")) {
			lngId = LanguagesManagerBase.getLngIdByCode(request.getLocale());
		} else if (lngId <= 0) {
			lngId = SkinsManagerBase.getSkin(16l).getDefaultLanguageId();
		}
			
		Locale locale = new Locale(LanguagesManagerBase.getLanguage(lngId).getCode());
			result = getUserFromSession(httpRequest);
			log.debug("Login ResponseCode="+result.getResponseCode());
			if(result.getResponseCode() != RESPONSE_CODE_SESSION_EXPIRED) {
				logout(httpRequest);
			}
			
			result.setUser(null);
			result.setResponseCode(0);
			
			User user;
			try {
				Map<String, String> config = UsersManagerBase.getCrypthoConfig();
				
				if(config.isEmpty() || !config.containsKey(KEY_CAPTCHA)) {
					log.debug("There is no configuration for enable/disable google recaptha");
					log.debug("Default recaptha is enabled!!!");
					config.put(KEY_CAPTCHA, "true");
				}
				
				if (config.get(KEY_CAPTCHA).equals("true")) {
					if (!captchaVerify(recap)) {
						log.debug("User login failed: recaptcha response false!");
						result.setResponseCode(RESPONSE_CODE_LOGIN_FAILED);
						result.addErrorMessage("", "User login failed");
						result.addUserMessage("", CommonUtil.getMessage(locale, "header.login.error", null));
						return result;
					}
				}
 
				user = validateAndLogin(request, result, request.getSkinId(), httpRequest);

				if (result.getResponseCode() != InvestServiceServlet.RESPONSE_CODE_SUCCESS) {
					return result;
				}

				if (user.getSkinId() == 0) {
					log.info("User login failed.");
					result.setResponseCode(RESPONSE_CODE_LOGIN_FAILED);
					result.addErrorMessage("", "User login failed");
					result.addUserMessage("", CommonUtil.getMessage(locale, "header.login.error", null));
				}

				if (result.getResponseCode() == InvestServiceServlet.RESPONSE_CODE_SUCCESS) {
					if (request.getUserName() != null && request.getPassword() != null) {

						ArrayList<String> keys;

						keys = LoginManager.loginActionFromWeb(user, request.getPassword());

						Login login = new Login();
						login.setLoginOffset(CommonUtil.formatJSUTCOffsetToString(request.getUtcOffset()));
						login.setSuccess(keys.isEmpty());
						login.setUserAgent(null);
						login.setLoginFrom(ConstantsBase.LOGIN_FROM_REGULAR);
						login.setWriterId(request.getWriterId());
						login.setServerId(CommonUtil.getServerName().split(":")[1]);
						Long combId = 22L;

						if (combId == null || combId == 0 || MarketingCombinationManagerBase.getById(combId) == null) {
							combId = SkinsManagerBase.getSkin(user.getSkinId()).getDefaultCombinationId();
						}

						login.setCombinationId(combId);
						
						try {
							result = initUserReg(result, user);
						} catch (SQLException e) {
							log.debug("Unable to init user", e);
							result.setResponseCode(RESPONSE_CODE_LOGIN_FAILED);
						}

						if (!keys.isEmpty() || result.getResponseCode() == RESPONSE_CODE_LOGIN_FAILED) {
							// insert failed login in logins table
							LoginsManager.insertLogin(user.getUserName(), login);
							result.setResponseCode(RESPONSE_CODE_LOGIN_FAILED);
							for (String key : keys) {
								result.addUserMessage("", CommonUtil.getMessage(locale, key, null));
							}
							return result;
						} else {
							// insert successful login in logins table
							long userLastLoginId = LoginsManager.insertLogin(user.getUserName(), login);
							httpRequest.getSession().setAttribute(ConstantsBase.LOGIN_ID, userLastLoginId);
							try {
								httpRequest.setAttribute(ConstantsBase.SESSION_SKIN, new Long(user.getSkinId()));
							} catch (Exception exc) {
								log.debug("can't init SESSION_SKIN ", exc);
							}
						}

						// add the user in session and result
						user.setPassword(null);
						result.setUser(user);
						user.setLocale(locale);
						httpRequest.getSession().setAttribute(ConstantsBase.BIND_SESSION_USER, user);
					}
				}
			} catch (Exception e) {
				log.error("Error in login user", e);
				result.setResponseCode(InvestServiceServlet.RESPONSE_CODE_UNKNOWN);
			}
		return result;
	}
    
	private static User validateAndLogin(LoginRequest request, MethodResult result, long skinId, HttpServletRequest httpRequest) throws Exception {
		User user;
		synchronized (httpRequest.getSession()) {
			user = (User) httpRequest.getSession().getAttribute("user");
		}
		
		if (null == user || user.getId() == 0) {
			if (null != request.getUserName() && null != request.getPassword()) {
				Locale locale = new Locale(LanguagesManagerBase.getLanguage(SkinsManagerBase.getSkin(skinId).getDefaultLanguageId()).getCode());
				ArrayList<ErrorMessage> msgs = Validators.validateInput("login", new UserHelper(request.getUserName(), request.getPassword()), locale);
				
				if (!msgs.isEmpty()) {
					result.setResponseCode(RESPONSE_CODE_LOGIN_FAILED);
					result.addErrorMessages(msgs);
					result.addUserMessages(msgs);
					log.info("Error message collection: " + msgs.toString());
					log.info("Invalid input - " + result.getErrorMessages());
					return null;
				}
				user = new User(); 
				UsersManagerBase.getUserByName(request.getUserName(), user);
				if (null == user || user.getId() <= 0 || !user.getPassword().equalsIgnoreCase(request.getPassword())) {
					log.info("User login failed.");
					result.setResponseCode(RESPONSE_CODE_LOGIN_FAILED);
					result.addErrorMessage("", "User login failed");
					return null;
				}
			} else {
				result.setResponseCode(RESPONSE_CODE_SESSION_EXPIRED);
			}
		}
		return user;
	}
    
	public static boolean captchaVerify(String clientResponse) {

		URL url;
		try {
			url = new URL(GOOGLE_RECAPTHA_URL + GOOGLE_SECRET_KEY + "&response=" + clientResponse);
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setRequestMethod("GET");
			String line, outputString = "";
			BufferedReader reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
			while ((line = reader.readLine()) != null) {
				outputString += line;
			}
			System.out.println(outputString);

			CaptchaResponse capRes = new Gson().fromJson(outputString, CaptchaResponse.class);
			return capRes.isSuccess();

		} catch (IOException e) {
			log.debug("Can not validate captcha due to: ", e);
			return false;
		}
	}
	
	private static boolean isAdultEnoughToBuyCryptho18(String year, String month, String day) {

		LocalDate today = LocalDate.now();
		LocalDate birthday = LocalDate.of(Integer.valueOf(year), Integer.valueOf(month), Integer.valueOf(day));

		Period p = Period.between(birthday, today);
		if (p.getYears() < ApiErrorCode.API_ERROR_CODE_VALIDATION_YEARS_OLD_18) {
			return false;
		} else {
			return true;
		}
	}
	
    public static MethodResult logout(HttpServletRequest httpRequest) {
    	log.debug("Try to logout");
    	MethodResult result = new MethodResult();
    	HttpSession session = httpRequest.getSession();
    	User user = (User) httpRequest.getSession().getAttribute("user");
    	try {
    		if(user != null && user.getId() > 0){
    			session.invalidate();
    			result.setResponseCode(InvestServiceServlet.RESPONSE_CODE_SUCCESS);
    			log.debug("User:" + user.getId() + " successfully logout");
    		} else {
    			result.setResponseCode(RESPONSE_CODE_SESSION_EXPIRED);
    			log.debug("Can't logout user not in Session ");
    		}    		
    	} catch (Exception e) {
            log.error("Error in logout method", e);
            result.setResponseCode(InvestServiceServlet.RESPONSE_CODE_UNKNOWN);
        }
    	return result;
    }
    
    private static Date convertStringToDate(String strDate) throws ParseException {
		DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		return formatter.parse(strDate);
    }

    public static UserMethodResult insertUser(InsertUserRequest request, HttpServletRequest httpRequest) {
		log.info("insertUser: " + request);
		UserMethodResult result = new UserMethodResult();
		User sessionUser = (User) httpRequest.getSession().getAttribute(ConstantsBase.BIND_SESSION_USER);
		if (sessionUser != null) {
			log.debug("There is already a user in session, invalidating");
			httpRequest.getSession().invalidate();
		}
		request.setSkinId(16L);
		String recaptcha = request.getReCaptcha();
		if (isReCaptchaEnabled() && (recaptcha == null || recaptcha.trim().isEmpty() || !captchaVerify(recaptcha))) {
			log.debug("Recaptcha validation failed");
			result.setResponseCode(RESPONSE_CODE_INVALID_CAPTCH);
			result.addErrorMessage("reCaptcha", RESPONSE_CODE_INVALID_CAPTCH);
			return result;
		}
		if (!request.isAcceptedTerms()) {
			result.setResponseCode(RESPONSE_CODE_NOT_ACCEPTED_TERMS);
			result.addErrorMessage("acceptedTerms", RESPONSE_CODE_NOT_ACCEPTED_TERMS);
			return result;
		}
		EmailValidatorRequest emailRequest = new EmailValidatorRequest();
		emailRequest.setEmail(request.getEmail());
		EmailValidatorResult emailResult = CommonJSONService.validateEmail(emailRequest);
		if (emailResult.getReturnCode() > 0) {
			result.setResponseCode(InvestServiceServlet.RESPONSE_CODE_INVALID_INPUT);
			int errorCode;
			switch (emailResult.getReturnCode()) {
			case EmailValidatorUtil.EMAIL_FAILED_REGEX_VALIDATION:
				/* falls through */
			case EmailValidatorUtil.EMAIL_FAILED_MX_VALIDATION:
				errorCode = RESPONSE_CODE_GENERAL_VALIDATION;
				break;

			case EmailValidatorUtil.EMAIL_FAILED_EXIST_VALIDATION:
				errorCode = RESPONSE_CODE_EMAIL_IN_USE;
				break;

			default:
				errorCode = InvestServiceServlet.RESPONSE_CODE_INVALID_INPUT;
				break;
			}
			result.addErrorMessage("email", errorCode);
			return result;
		}
		
		long lngId = 0;
		if (request.getLocale() != null && !request.getLocale().equals("")) {
			lngId = LanguagesManagerBase.getLngIdByCode(request.getLocale());
		} else if (lngId <= 0) {
			lngId = SkinsManagerBase.getSkin(16l).getDefaultLanguageId();
		}
		
		Locale locale = new Locale(LanguagesManagerBase.getLanguage(lngId).getCode());
		long countryId = CommonJSONService.getCountryFromIp(request.getIp(), httpRequest);
		ArrayList<ErrorMessage> msgs;
		
		try {
			msgs = Validators.validateInput("register", request, locale);
			if (!msgs.isEmpty()) {
				result.setErrorCode(InvestServiceServlet.RESPONSE_CODE_INVALID_INPUT);
				result.addErrorMessages(msgs);
				return result;
			}
		} catch (ValidatorException e1) {
			log.debug("Unable to validate input, user was not registered", e1);
			result.setResponseCode(InvestServiceServlet.RESPONSE_CODE_UNKNOWN);
			return result;
		}
		User user = UsersManager.insertUser(request.getEmail(), request.getPassword(), request.getSkinId(), request.isAcceptedTerms(), request.getIp(), lngId, countryId);
		if (user == null) {
			result.setResponseCode(RESPONSE_CODE_FAIL_TO_INSERT);
			return result;
		}
		user.setLocale(locale);
		try {
			result = initUserReg(result, user);
		} catch (SQLException e) {
			log.debug("Unable to init user", e);
			result.setResponseCode(RESPONSE_CODE_FAIL_TO_UPDATE); 
			return result;
		}
		httpRequest.getSession().setAttribute(ConstantsBase.BIND_SESSION_USER, user);
		try {
			UsersManager.sendEmailWithToken(user.getId(), user.getEmail(), "Verify your email", VERIFICATION_URL_LINK,
											UsersManager.TOKEN_TYPE_VERIFY_MAIL, user.getFirstName());
		} catch (Exception e) {
			log.debug("Unable to send verification email", e);
			result.setResponseCode(RESPONSE_CODE_UNABLE_TO_SEND_VERIFY_EMAIL);
			return result;
		}
		return result;
	}

	private static boolean isReCaptchaEnabled() {
		Map<String, String> config = null;
		try {
			config = UsersManagerBase.getCrypthoConfig();
		} catch (SQLException e) {
			log.debug("Unable to load reCaptcha config", e);
		}
		if (config == null || config.isEmpty() || !config.containsKey(KEY_CAPTCHA)) {
			log.debug("There is no configuration for enable/disable google recaptha");
			log.debug("Default recaptha is enabled!!!");
			config.put(KEY_CAPTCHA, "true");
		}
		return config.get(KEY_CAPTCHA).equals("true");
	}

    public static MethodResult resendVerificationMail(MethodRequest request, HttpServletRequest httpRequest) {
    	log.info("resendVerificationMail: " + request);
		UserMethodResult result = new UserMethodResult();
		User user = (User) httpRequest.getSession().getAttribute("user");
		if (user == null || user.getId() <= 0) {
			result.setResponseCode(RESPONSE_CODE_SESSION_EXPIRED);
			return result;
		}
		try {
			UsersManager.sendEmailWithToken(user.getId(), user.getEmail(), "Verify your email", VERIFICATION_URL_LINK,
											UsersManager.TOKEN_TYPE_VERIFY_MAIL, user.getFirstName());
		} catch (Exception e) {
			log.debug("Unable to resend verification email", e);
			result.setResponseCode(RESPONSE_CODE_UNABLE_TO_SEND_VERIFY_EMAIL);
			return result;
		}
		return result;
    }

	private static UserMethodResult initUserReg(UserMethodResult result, User user) throws SQLException {
		result.setUser(user);	
		UserRegulationBase ur = new UserRegulationBase();
		ur.setUserId(user.getId());
		UserRegulationManager.getUserRegulation(ur);
		result.setUserRegulation(ur);		
		return result;
	}
	
	public static MethodResult changePhoneNumber(ChangePhoneRequest request, HttpServletRequest httpRequest) {
		MethodResult result = new MethodResult();

		UserMethodResult userResult = getUserFromSession(httpRequest);

		if (userResult.getResponseCode() == RESPONSE_CODE_SESSION_EXPIRED) {
			result.setResponseCode(userResult.getResponseCode());
			return result;
		}

		Locale locale = new Locale(LanguagesManagerBase.getLanguage(SkinsManagerBase.getSkin(request.getSkinId()).getDefaultLanguageId()).getCode());
		try {
			ArrayList<ErrorMessage> msgs = Validators.validateInput("updatePhone", request, locale);

			if (!msgs.isEmpty()) {
				result.setResponseCode(InvestServiceServlet.RESPONSE_CODE_INVALID_INPUT);
				result.addUserMessages(msgs);
				return result;
			}

			UsersManager.changeMobilePhone(request.getMobilePhone(), userResult.getUser().getId());
			result.setResponseCode(InvestServiceServlet.RESPONSE_CODE_SUCCESS);
			com.anyoption.common.beans.base.User user = userResult.getUser();
			EmailProviderFactory.getEmailProvider()
								.scheduleTemplateEmail(EmailTemplateFactory.getEditProfileTemplate(	user.getId(),
																									CommonUtil.getConfig("email.from"),
																									user.getEmail(), null,
																									user.getFirstName(),
																									CommonUtil.getConfig("email.template.homepage"),
																									CommonUtil.getConfig("email.support")));
		} catch (Exception e) {
			log.error("Error in changePhoneNumber due to: ", e);
			result.setResponseCode(InvestServiceServlet.RESPONSE_CODE_UNKNOWN);
		}
		return result;
	}

	public static UserMethodResult reloadUser(UserMethodResult result, HttpServletRequest request) {
		User user = (User) request.getSession().getAttribute(ConstantsBase.BIND_SESSION_USER);
		if (user == null) {
			result.setErrorCode(RESPONSE_CODE_SESSION_EXPIRED);
			return result;
		}
		try {
			user = UsersManagerBase.getById(user.getId());
			user.setPassword(null);
			result.setUser(user);
			request.getSession().setAttribute(ConstantsBase.BIND_SESSION_USER, user);
			initUserReg(result, user);
			return result;
		} catch (SQLException e) {
			log.debug("Unable to reload user in session", e);
			result.setResponseCode(InvestServiceServlet.RESPONSE_CODE_UNKNOWN);
			return result;
		}
	}

	public static MethodResult sendSupportEmail(SendEmailRequest request, HttpServletRequest httpRequest) {
		log.info("sendSupportEmail: " + request);
		MethodResult result = new MethodResult();
		request.setSkinId(16L);
		User user = (User) httpRequest.getSession().getAttribute(ConstantsBase.BIND_SESSION_USER);
		if (user == null) {
			result.setErrorCode(RESPONSE_CODE_SESSION_EXPIRED);
			return result;
		}
		EmailValidatorRequest emailRequest = new EmailValidatorRequest();
		emailRequest.setEmail(request.getEmail());
		EmailValidatorResult emailResult = CommonJSONService.validateEmail(emailRequest);
		if (emailResult.getReturnCode() > 0 && emailResult.getReturnCode() != EmailValidatorUtil.EMAIL_FAILED_EXIST_VALIDATION) {
			// the user should be already registered
			result.setResponseCode(InvestServiceServlet.RESPONSE_CODE_INVALID_INPUT);
			int errorCode = 0;
			switch (emailResult.getReturnCode()) {
			case EmailValidatorUtil.EMAIL_FAILED_REGEX_VALIDATION:
				/* falls through */
			case EmailValidatorUtil.EMAIL_FAILED_MX_VALIDATION:
				errorCode = RESPONSE_CODE_GENERAL_VALIDATION;
				break;

			default:
				errorCode = InvestServiceServlet.RESPONSE_CODE_INVALID_INPUT;
				break;
			}
			result.addErrorMessage("email", errorCode);
			return result;
		}
		int lngId = SkinsManagerBase.getSkin(request.getSkinId()).getDefaultLanguageId();
		Locale locale = new Locale(LanguagesManagerBase.getLanguage(lngId).getCode());
		ArrayList<ErrorMessage> msgs;
		try {
			msgs = Validators.validateInput("updatePhone", request, locale);
			if (!msgs.isEmpty()) {
				result.setResponseCode(InvestServiceServlet.RESPONSE_CODE_INVALID_INPUT);
				result.addUserMessages(msgs);
				return result;
			}
		} catch (ValidatorException e) {
			log.debug("Unable to validate phone, email was not sent", e);
			result.setResponseCode(InvestServiceServlet.RESPONSE_CODE_UNKNOWN);
			return result;
		}
		UsersManager.sendSupportEmail(user.getId(), request.getFullName(), request.getEmail(), request.getMobilePhone(), request.getSubject(), request.getMessage());
		return result;
	}
}