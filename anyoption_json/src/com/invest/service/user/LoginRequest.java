/**
 * 
 */
package com.invest.service.user;

import com.anyoption.common.service.requests.MethodRequest;

/**
 * @author pavel.t
 *
 */
public class LoginRequest extends MethodRequest {

	private String userName;
	private String password;

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@Override
	public String toString() {
		return "LoginRequest [userName=" + userName + ", password=" + password + "]";

	}

}
