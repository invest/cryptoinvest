package com.invest.service.user;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Hashtable;

import org.apache.log4j.Logger;

import com.anyoption.beans.User;
import com.anyoption.common.beans.Country;
import com.anyoption.common.beans.Writer;
import com.anyoption.common.beans.base.Platform;
import com.anyoption.common.beans.base.Register;
import com.anyoption.common.managers.BaseBLManager;
import com.anyoption.common.managers.CountriesManagerBase;
import com.anyoption.common.util.Sha1;
import com.anyoption.managers.UsersManagerBase;
import com.anyoption.util.CommonUtil;
import com.invest.common.email.beans.EmailTemplate;
import com.invest.common.email.factories.EmailProviderFactory;
import com.invest.common.email.factories.EmailTemplateFactory;

/**
 * @author Kiril.m
 */
public class UsersManager extends BaseBLManager {

	private static final Logger log = Logger.getLogger(UsersManager.class);
	private static final long VERIFICATION_VALIDITY = 24 * 3600 * 1000; // 1 day
	public static final String VERIFICATION_SEPARATOR = "m@1l";
	static final String VERIFICATION_USER_ID = "a";
	static final String VERIFICATION_USERNAME = "b";
	static final String VERIFICATION_TIME = "c";
	static final String VERIFICATION_TOKEN = "d";
	static final int TOKEN_TYPE_VERIFY_MAIL = 1;
	public static final int TOKEN_TYPE_FORGOT_PASSWORD = 2;

	public static User insertUser(	String username, String password, long skinId, boolean acceptedTerms, String ip, long lngId, long countryId) {
		try {
			Register register = new Register();
			register.setUserName(username);
			register.setEmail(username);
			register.setPassword(password);
			register.setSkinId(skinId);
			register.setTerms(acceptedTerms);
			register.setIp(ip);
			register.setLanguageId(lngId);
			register.setCountryId(countryId);
			register.setPlatformId(Platform.CRYPTOINVEST);                
			Country c = CountriesManagerBase.getAllAllowedCountries().get(countryId);
			register.setUtcOffsetCreated(c.getGmtOffset());
        
			User user = UsersManagerBase.insertUser(register, Writer.WRITER_ID_WEB, false, null, null, null, null, 0L);
			user.setPassword(null);
			return user;
		} catch (SQLException e) {
			log.debug("Unable to insert user, returning null", e);
			return null;
		}
	}

	public static void sendEmailWithToken(	long userId, String userEmail, String subject, String listener, int tokenType,
											String name) throws Exception {
		long time = System.currentTimeMillis();
		String token = generateToken(userId, userEmail, time, tokenType);
		String tokenParam = "&" + VERIFICATION_TOKEN + "=" + token;
		String params = VERIFICATION_USER_ID	+ "=" + userId + "&" + VERIFICATION_USERNAME + "=" + userEmail + "&" + VERIFICATION_TIME + "="
						+ time;
		String url = null;
		
		if (tokenType == TOKEN_TYPE_FORGOT_PASSWORD) {
			url = CommonUtil.getProperty("forgot.pass.url") + "?" + params + tokenParam;
		} else {
			url = CommonUtil.getProperty("homepage.url.https") + listener + "?" + params + tokenParam;
		}
		
		log.debug("LINK: " + url);
		if (tokenType == TOKEN_TYPE_VERIFY_MAIL) {
			EmailTemplate template = EmailTemplateFactory.getVerificationTemplate(	userId, CommonUtil.getConfig("email.from"), userEmail,
																					null, url,
																					CommonUtil.getConfig("email.template.homepage"),
																					CommonUtil.getConfig("email.support"));
			EmailProviderFactory.getEmailProvider().scheduleTemplateEmail(template);
		} else {
			EmailTemplate template = EmailTemplateFactory.getForgotPasswordTemplate(userId, CommonUtil.getConfig("email.from"), userEmail,
																					null, name, url,
																					CommonUtil.getConfig("email.template.homepage"),
																					CommonUtil.getConfig("email.support"));
			EmailProviderFactory.getEmailProvider().scheduleTemplateEmail(template);
		}
	}

	public static String generateToken(long userId, String userEmail, long time, int type) throws Exception {
		String t = Sha1.encode(userId + VERIFICATION_SEPARATOR + userEmail + VERIFICATION_SEPARATOR + time);
		saveToken(userId, t, time + VERIFICATION_VALIDITY, type);
		return t; 
	}

	private static void saveToken(long userId, String token, long validity, int type) throws SQLException {
		Connection con = null;
		try {
			con = getConnection();
			UsersDAO.saveToken(con, userId, token, validity, type);
		} catch (SQLException e) {
			log.debug("Unable to save token for user: " + userId, e);
			throw e;
		} finally {
			closeConnection(con);
		}

	}

	public static boolean validateToken(String token) {
		Connection con = null;
		try {
			con = getConnection();
			return UsersDAO.validateToken(con, token);
		} catch (SQLException e) {
			log.debug("Unable to validate token: " + token, e);
			return false;
		} finally {
			closeConnection(con);
		}
	}

	public static void verifyUser(String token) {
		Connection con = null;
		try {
			con = getConnection();
			UsersDAO.verifyUser(con, token);
			UsersDAO.updateUsedToken(con, token);
		} catch (SQLException e) {
			log.debug("Unable to verify user with token: " + token, e);
		} finally {
			closeConnection(con);
		}
	}
	
	public static User getUserByEmail(String email) {
		Connection con = null;
		try {
			con = getConnection();
			return UsersDAO.getUserByEmail(con, email);
		} catch (SQLException e) {
			log.debug("Unable to get user with email: " + email, e);
			return null;
		} finally {
			closeConnection(con);
		}
	}
	
	public static void changeMobilePhone(String mobilePhone, long userId) {
		Connection con = null;
		try {
			con = getConnection();
			UsersDAO.changeMobilePhone(con, mobilePhone, userId);
		} catch (SQLException e) {
			log.debug("Unable to change mobilePhone for user with id: " + userId, e);
		} finally {
			closeConnection(con);
		}
	}

	public static void sendSupportEmail(long userId, String fullName, String email, String phone, String subject, String message) {
		try {
			insertSupportEmail(userId, fullName, email, phone, subject, message);
			sendEmailToSupport(userId, fullName, email, phone, subject, message);
		} catch (SQLException e) {
			log.debug("Unable to send email to support from user " + userId, e);
		}
	}

	private static void insertSupportEmail(	long userId, String fullName, String email, String phone, String subject,
											String message) throws SQLException {
		Connection con = null;
		try {
			con = getConnection();
			UsersDAO.insertSupportEmail(con, userId, fullName, email, phone, subject, message);
		} catch (SQLException e) {
			log.debug("Unable to insert support email for user " + userId, e);
			throw e;
		} finally {
			closeConnection(con);
		}
	}

	private static void sendEmailToSupport(long userId, String fullName, String email, String phone, String subject, String message) {
		StringBuilder body = new StringBuilder("User id: ");
		body.append(userId).append("<br/>Full name: ").append(fullName).append("<br/>Email: ").append(email).append("<br/>Phone: ")
			.append(phone).append("<br/>Subject: ").append(subject).append("<br/>Message: ").append(message);
		sendEmail(CommonUtil.getProperty("support.email"), email, subject, body.toString());
	}

	private static void sendEmail(String toEmail, String fromEmail, String subject, String body) {
		Hashtable<String, String> serverProperties = new Hashtable<String, String>();
		serverProperties.put("url", CommonUtil.getConfig("email.server"));
		serverProperties.put("auth", "true");
//		serverProperties.put("user", CommonUtil.getConfig("email.uname"));
//		serverProperties.put("pass", CommonUtil.getConfig("email.pass"));
		serverProperties.put("contenttype", "text/html; charset=UTF-8");
		Hashtable<String, String> email = new Hashtable<String, String>();
		email.put("subject", subject);
		email.put("to", toEmail);
		email.put("from", fromEmail);
		email.put("body", body);
		CommonUtil.sendEmail(serverProperties, email, null);
	}
}