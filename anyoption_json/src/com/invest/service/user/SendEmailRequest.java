package com.invest.service.user;

import com.anyoption.common.service.requests.UserMethodRequest;

/**
 * @author Kiril.m
 */
public class SendEmailRequest extends UserMethodRequest {

	private String fullName;
	private String email;
	private String mobilePhone;
	private String subject;
	private String message;

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getMobilePhone() {
		return mobilePhone;
	}

	public void setMobilePhone(String mobilePhone) {
		this.mobilePhone = mobilePhone;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	@Override
	public String toString() {
		String ls = System.lineSeparator();
		return ls + this.getClass().getName() + ls
				+ super.toString()
				+ "fullName: " + fullName + ls
				+ "email: " + email + ls
				+ "mobilePhone: " + mobilePhone + ls
				+ "subject: " + subject + ls
				+ "message: " + message + ls; 
	}
}