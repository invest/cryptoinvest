package com.invest.service.user;
	
import com.anyoption.common.service.requests.MethodRequest;

/**
 * @author Kiril.m
 */
public class InsertUserRequest extends MethodRequest {

	private String email;
	private String password;
	private String passwordConfirm;
	private String reCaptcha;
	private boolean acceptedTerms;

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getPasswordConfirm() {
		return passwordConfirm;
	}

	public void setPasswordConfirm(String passwordConfirm) {
		this.passwordConfirm = passwordConfirm;
	}

	public String getReCaptcha() {
		return reCaptcha;
	}

	public void setReCaptcha(String reCaptcha) {
		this.reCaptcha = reCaptcha;
	}

	public boolean isAcceptedTerms() {
		return acceptedTerms;
	}

	public void setAcceptedTerms(boolean acceptedTerms) {
		this.acceptedTerms = acceptedTerms;
	}

	public String toString() {
		String ls = System.lineSeparator();
		return ls + this.getClass().getName() + ls
				+ super.toString() + ls
				+ "email: " + email + ls
				+ "reCaptcha: " + reCaptcha + ls
				+ "acceptedTerms: " + acceptedTerms + ls;
	}
}