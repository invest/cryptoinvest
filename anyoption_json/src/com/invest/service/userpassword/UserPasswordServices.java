package com.invest.service.userpassword;

import java.util.ArrayList;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import com.anyoption.beans.User;
import com.anyoption.common.service.results.ErrorMessage;
import com.anyoption.common.service.results.MethodResult;
import com.anyoption.common.service.results.UserMethodResult;
import com.anyoption.common.util.MessageToFormat;
import com.anyoption.common.util.Sha1;
import com.anyoption.json.requests.ChangePasswordMethodRequest;
import com.anyoption.managers.UsersManagerBase;
import com.anyoption.util.CommonUtil;
import com.invest.common.email.factories.EmailProviderFactory;
import com.invest.common.email.factories.EmailTemplateFactory;
import com.invest.service.InvestServiceServlet;
import com.invest.service.user.UserServices;
import com.invest.service.user.UsersManager;
import com.invest.validator.Validators;


public class UserPasswordServices {
	private static final Logger log = Logger.getLogger(UserPasswordServices.class);
	public static final int RESPONSE_CODE_PASSWORD_DOES_NOT_MATCH = 101;
	private static final String RESET_PASSWORD_LISTENER = "InvestService/UserPasswordServices/resetPassword";
	
    public static UserMethodResult changePassword(ChangePasswordMethodRequest request, HttpServletRequest httpRequest) {
    	log.info("changePassword: Old pass=" + "*****" + " New pass=" + "*****");
    	UserMethodResult result = new UserMethodResult();
        try {
        	User user = (User) httpRequest.getSession().getAttribute("user");            
            if(user == null || !(user.getId() > 0)){
            	result.setResponseCode(UserServices.RESPONSE_CODE_SESSION_EXPIRED);
            	return result;
            } else {
            	user = UsersManagerBase.getById(user.getId());
            }
            
            ArrayList<ErrorMessage> msgs = Validators.validateInput("changePass", request, user.getLocale());
            if (!msgs.isEmpty()) {
                result.setErrorCode(InvestServiceServlet.RESPONSE_CODE_INVALID_INPUT);
                result.addUserMessages(msgs);
                return result;
            }
            
            Map<String, MessageToFormat> mtf = UsersManagerBase.changePassValidation(user, request.getPassword(), request.getPasswordNew());
            if (!mtf.isEmpty()) {
                result.setErrorCode(InvestServiceServlet.RESPONSE_CODE_INVALID_INPUT);
                return result;
            }
            
            UsersManagerBase.updateUserPassword(user.getId(), request.getPasswordNew());
            result.setResponseCode(InvestServiceServlet.RESPONSE_CODE_SUCCESS);
			log.debug("User:" + user.getId() + " successfully change password!");
			EmailProviderFactory.getEmailProvider()
								.scheduleTemplateEmail(EmailTemplateFactory.getEditProfileTemplate(	user.getId(),
																									CommonUtil.getConfig("email.from"),
																									user.getEmail(), null,
																									user.getFirstName(),
																									CommonUtil.getConfig("email.template.homepage"),
																									CommonUtil.getConfig("email.support")));
			//Refresh user
			result = UserServices.init(httpRequest);
        } catch (Exception e) {
            log.error("Error in changePassword ", e);
            result.setErrorCode(InvestServiceServlet.RESPONSE_CODE_UNKNOWN);
        }
        return result;
    }
    
	public static MethodResult forgotPassword(ForgotPasswordRequest request, HttpServletRequest httpRequest) {
		log.info("forgotPassword: " + request);
		MethodResult result = new MethodResult();
		try {

			User user = UsersManager.getUserByEmail(request.getEmail());
			if (null == user) {
				result.setResponseCode(UserServices.RESPONSE_CODE_USER_DOES_NOT_EXIST);
				return result;
			}

			UsersManager.sendEmailWithToken(user.getId(), user.getEmail(), "Forgot password", RESET_PASSWORD_LISTENER, UsersManager.TOKEN_TYPE_FORGOT_PASSWORD, user.getFirstName());
			result.setResponseCode(InvestServiceServlet.RESPONSE_CODE_SUCCESS);

		} catch (Exception e) {
			log.debug("Unable to send forgot password email due to: ", e);
			result.setResponseCode(UserServices.RESPONSE_CODE_UNABLE_TO_SEND_FORGOT_PASSWORD_EMAIL);
			return result;
		}
		return result;
	}
	
	public static MethodResult resetPassword(ResetPasswordRequest req, HttpServletRequest httpRequest) {
		MethodResult result = new MethodResult();
		String reqToken = req.getD();
		log.debug("forgotPassRedirect --> " + " reached with token: " + reqToken);
		String userId = req.getA();
		String username = req.getB();
		String time = req.getC();
		String token;
		try {
			token = Sha1.encode(userId + UsersManager.VERIFICATION_SEPARATOR + username + UsersManager.VERIFICATION_SEPARATOR + time);
		} catch (Exception e) {
			log.debug("Unable to encode token", e);
			result.setResponseCode(InvestServiceServlet.RESPONSE_CODE_UNKNOWN);
			return result;
		}
		if (token.equals(reqToken)) {
			if (UsersManager.validateToken(token)) {
				UsersManager.verifyUser(token);
				log.debug("User is verified! Password reseting......");
				try {
					if(req.getPassword().equals(req.getPasswordConfirm())) {
						User user = UsersManager.getUserByEmail(username);
						UsersManagerBase.updateUserPassword(user.getId(), req.getPassword());
					}else {
						result.setResponseCode(RESPONSE_CODE_PASSWORD_DOES_NOT_MATCH);
					}
				} catch (Exception e) {
					log.debug("Enable to reset user password due to: ", e);
					result.setResponseCode(InvestServiceServlet.RESPONSE_CODE_UNKNOWN);
				}
			} else {
				log.debug("Received invalid token: " + token);
				result.setResponseCode(InvestServiceServlet.RESPONSE_CODE_UNKNOWN);
			}
		} else {
			log.debug("Tokens do not match. Received: " + reqToken + " with params: " + userId + ", " + username + ", " + time + ". Calculated token: " + token);
			result.setResponseCode(InvestServiceServlet.RESPONSE_CODE_UNKNOWN);
		}
		return result;
	}
}
