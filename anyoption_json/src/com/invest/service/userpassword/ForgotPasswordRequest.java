/**
 * 
 */
package com.invest.service.userpassword;

/**
 * @author pavel.t
 *
 */
public class ForgotPasswordRequest {

	private String email;

	@Override
	public String toString() {
		return "ForgotPasswordRequest [email=" + email + "]";
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

}
