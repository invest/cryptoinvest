/**
 * 
 */
package com.invest.service.userpassword;

import com.anyoption.common.service.requests.MethodRequest;

/**
 * @author pavel.t
 *
 */
public class ResetPasswordRequest extends MethodRequest {

	private String a;
	private String b;
	private String c;
	private String d;
	private String password;
	private String passwordConfirm;

	public String getA() {
		return a;
	}

	public void setA(String a) {
		this.a = a;
	}

	public String getB() {
		return b;
	}

	public void setB(String b) {
		this.b = b;
	}

	public String getC() {
		return c;
	}

	public void setC(String c) {
		this.c = c;
	}

	public String getD() {
		return d;
	}

	public void setD(String d) {
		this.d = d;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getPasswordConfirm() {
		return passwordConfirm;
	}

	public void setPasswordConfirm(String passwordConfirm) {
		this.passwordConfirm = passwordConfirm;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "ResetPasswordRequest [a=" + a + ", b=" + b + ", c=" + c + ", d=" + d + ", password=" + password
				+ ", passwordConfirm=" + passwordConfirm + "]";
	}

}
