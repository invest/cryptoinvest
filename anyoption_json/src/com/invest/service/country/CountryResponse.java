/**
 * 
 */
package com.invest.service.country;

import java.util.Hashtable;

import com.anyoption.common.beans.Country;
import com.anyoption.common.service.results.MethodResult;

public class CountryResponse extends MethodResult {
	private Hashtable<Long, Country> countries;

	public Hashtable<Long, Country> getCountries() {
		return countries;
	}

	public void setCountries(Hashtable<Long, Country> countries) {
		this.countries = countries;
	}

}