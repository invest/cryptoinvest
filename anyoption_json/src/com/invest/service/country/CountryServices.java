package com.invest.service.country;

import java.sql.SQLException;
import java.util.Hashtable;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.Country;
import com.anyoption.common.managers.CountriesManagerBase;
import com.invest.service.InvestServiceServlet;


public class CountryServices {
	private static final Logger log = Logger.getLogger(CountryServices.class);
	
	public static CountryResponse getCountries(HttpServletRequest httpRequest) {
		CountryResponse res = new CountryResponse();
		Hashtable<Long, Country> countries = null;
		try {
			countries = CountriesManagerBase.getAllAllowedCountries();
		} catch (SQLException e) {
			log.error("Error in getCountries", e);
			res.setResponseCode(InvestServiceServlet.RESPONSE_CODE_UNKNOWN);
		}
		res.setCountries(countries);
		return res;
		}
	}