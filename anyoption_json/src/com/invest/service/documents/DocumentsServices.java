package com.invest.service.documents;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import com.anyoption.beans.User;
import com.anyoption.common.service.userdocuments.UserDocumentsManager;
import com.anyoption.common.service.userdocuments.UserDocumentsMethodResult;
import com.invest.service.InvestServiceServlet;
import com.invest.service.user.UserServices;


public class DocumentsServices {
	private static final Logger log = Logger.getLogger(DocumentsServices.class);
	public static UserDocumentsMethodResult getUserDocuments (HttpServletRequest httpRequest) {		
		log.debug("getUserDocuments:" + httpRequest);		
		UserDocumentsMethodResult result = new UserDocumentsMethodResult();	
		User user = (User) httpRequest.getSession().getAttribute("user");
		try {
			if(user != null && user.getId() > 0){
				result.setUserDocuments(UserDocumentsManager.getUserDocuments(user.getId()));
				result.setResponseCode(InvestServiceServlet.RESPONSE_CODE_SUCCESS);
			} else {
				log.error("When getUserDocument user session is expire");
				result.setResponseCode(UserServices.RESPONSE_CODE_SESSION_EXPIRED);
			}				
		} catch (Exception e) {
			log.error("When getUserDocument", e);
			result.setResponseCode(InvestServiceServlet.RESPONSE_CODE_UNKNOWN);
		}		
		return result;
	}
}
