package com.invest.service.print;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Parameter;
import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;


public class PrintServices {
	private static final Logger log = Logger.getLogger(PrintServices.class);
	
	public static PrintResponse print(HttpServletRequest request) {
		PrintResponse res = new PrintResponse();
		String param = request.getParameter("service");
		log.debug("Print Param : " + param);
		String classReq = "com.invest.service." + param.replace("Services", ".").toLowerCase() 
													+ param;
		log.debug("Print classReq : " + classReq);
		try {
			Class<?> serviceClass = Class.forName(classReq);
			 Method[] methods = serviceClass.getDeclaredMethods();
		        int nMethod = 1;
		        log.debug("*** List of all methods of Person class ***");
		        ArrayList<String> ml = new ArrayList<>();
		        for (Method method : methods) {
		        	 if (Modifier.isPublic(method.getModifiers())) {
				        	log.debug( ++nMethod+ ". " + method);
				        	ml.add(method.getName() + "(" + getMethodParams(method.getParameters()) + ")");
		        	    }
		        }
		        res.setServices(ml);
		} catch (ClassNotFoundException e) {
			log.error("When print ", e);
		}
		return res;
	}
	
	private static String getMethodParams(Parameter[] p){
		String res = "";
		for (Parameter parameter : p) {
			res += parameter.getParameterizedType() + ", ";
		}		
		return res.substring(0,res.length() - 1);
	}
}
