/**
 * 
 */
package com.invest.service.print;

import java.util.List;

import com.anyoption.common.service.results.MethodResult;

public class PrintResponse extends MethodResult {
	private List<String> services;

	public List<String> getServices() {
		return services;
	}

	public void setServices(List<String> services) {
		this.services = services;
	}

}