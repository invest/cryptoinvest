package com.copyop.json.service;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.anyoption.common.managers.UsersManagerBase;
import com.anyoption.common.service.requests.UserMethodRequest;
import com.anyoption.common.util.CommonUtil;
import com.anyoption.json.service.AnyoptionService;
import com.copyop.json.results.UserMethodResult;

/**
 * Auto login for copyop users when coming from API
 * @author eranl
 *
 */
public class AutoLoginServlet extends HttpServlet {

	private static final long serialVersionUID = -2238884853587600493L;	
	private static final Logger log = Logger.getLogger(AutoLoginServlet.class);
	private String copyopURL;
	
	public void init() throws ServletException {
		copyopURL = CommonUtil.getConfig("homepage.url.https");		
	}
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
		response.sendRedirect(copyopURL);
	}
	
	protected void doPost(HttpServletRequest httpRequest, HttpServletResponse response) throws IOException {		
		try {
			long beginTime = System.currentTimeMillis();
			String userId = (String) httpRequest.getParameter("user_id");
			String loginToken = (String) httpRequest.getParameter("login_token");
			log.info("Login token from request:" + loginToken + " userId: " + userId);		
			if (!CommonUtil.isParameterEmptyOrNull(loginToken)) {
				com.anyoption.common.beans.base.User user = UsersManagerBase.getUserByLoginToken(loginToken, Long.valueOf(userId));				
				if (user != null) {
					try {
						log.info("Login token userName:" + user.getUserName() + " skinId:" + user.getSkinId());
						UserMethodRequest userMethodRequest = new UserMethodRequest();
						userMethodRequest.setUserName(user.getUserName());
						userMethodRequest.setPassword(user.getPassword());
						userMethodRequest.setSkinId(user.getSkinId());						
						UserMethodResult userMethodResult  = CopyopService.getCopyopUser(userMethodRequest, httpRequest);
						if (userMethodResult.getErrorCode() != AnyoptionService.ERROR_CODE_SUCCESS) { // error login
							log.error("copyopAutoLogin error " + userMethodResult.toString());
						}
					} catch (Exception e) {
						log.error("Exception copyopAutoLogin error", e);							
					}										
				} else {
					log.info("user not found!! with Login token:" + loginToken + " userId: " + userId);
				}
			}
			long endTime = System.currentTimeMillis();
			log.debug("TIME: AutoLoginServlet " + (endTime - beginTime) + "ms");
		} catch (Exception e) {
			log.error("ERROR!!", e);
		}		
		response.sendRedirect(copyopURL);
	}
	    
}
