package com.copyop.json.service.test;

import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.io.output.ByteArrayOutputStream;
import org.apache.log4j.Logger;

import com.anyoption.common.service.requests.MethodRequest;
import com.anyoption.common.service.requests.UserMethodRequest;
import com.anyoption.common.service.results.MethodResult;
import com.anyoption.json.requests.BonusMethodRequest;
import com.anyoption.json.results.CountryIdMethodResult;
import com.copyop.json.requests.ChangeNicknameMethodRequest;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/**
 * @author liors
 *
 */
public class TestService {
	private static final Logger logger = Logger.getLogger(TestService.class);	

	public static boolean changeCopyopNickname(HttpServletRequest httpRequest, TestServiceRequest testServiceRequest, String serviceUrl) {			
		logger.info("changeCopyopNickname; service url: " + serviceUrl);
		ChangeNicknameMethodRequest copyopUserMethodRequest = new ChangeNicknameMethodRequest();
		//TODO wrapper
		if (httpRequest == null) {
			// Refactoring changes
//	    	copyopUserMethodRequest.setNickName(testServiceRequest.getNickName());
//			copyopUserMethodRequest.setUserId(testServiceRequest.getUserId());
		} else {
			//TODO get httpRequest parameters.
		}
		
		logger.debug("about to call serviceURL = " + serviceUrl);
		Boolean result = (Boolean)doServiceCall(copyopUserMethodRequest, new Boolean(""), serviceUrl);
        logger.info(result);
		return result;
	}
	
	
	public static CountryIdMethodResult checkGetCountryId(HttpServletRequest httpRequest, MethodRequest m, String serviceUrl) {			
		logger.info("checkGetCountryId; service url: " + serviceUrl);
		//TODO wrapper
		if (httpRequest == null) {
			// Refactoring changes
//	    	copyopUserMethodRequest.setNickName(testServiceRequest.getNickName());
//			copyopUserMethodRequest.setUserId(testServiceRequest.getUserId());
		} else {
			//TODO get httpRequest parameters.
		}
		
		logger.debug("about to call serviceURL = " + serviceUrl);
		CountryIdMethodResult result = (CountryIdMethodResult)doServiceCall(m, new CountryIdMethodResult(), serviceUrl);
        logger.info(result);
		return result;
	}
		
	public static MethodResult weekend(HttpServletRequest httpRequest, UserMethodRequest m, String serviceUrl) {			
		logger.debug("about to call serviceURL = " + serviceUrl);
		MethodResult result = (MethodResult)doServiceCall(m, new MethodResult(), serviceUrl);
        logger.info(result);
		return result;
	}
	
	public static MethodResult updateBonusStatus(HttpServletRequest httpRequest, BonusMethodRequest bmr, String serviceUrl) {			
		logger.debug("about to call serviceURL = " + serviceUrl);
		MethodResult result = (MethodResult)doServiceCall(bmr, new MethodResult(), serviceUrl);
        logger.info(result);
		return result;
	}

	private static Object doServiceCall(Object request, Object response, String serviceUrl) {
    	try { 		
	        byte[] buffer = new byte[2048]; // 2K read/write buffer. This might need change for better performance
	        URL u = new URL(serviceUrl);
	        HttpURLConnection httpCon = (HttpURLConnection)u.openConnection();
	        httpCon.setDoOutput(true);	 
	        Gson prettyGson = new GsonBuilder().setPrettyPrinting().create();
	        String pretJson = prettyGson.toJson(request);
	        logger.info("Pretty printing: " + pretJson);
	        Gson gson = new GsonBuilder().setPrettyPrinting().create();     
	        httpCon.getOutputStream().write(gson.toJson(request).getBytes("UTF-8"));
	        int readCount = -1;
	        InputStream is = httpCon.getInputStream(); 
	        ByteArrayOutputStream baos = new ByteArrayOutputStream();
	        OutputStream os = baos;
	        while ((readCount = is.read(buffer)) != -1) {
	        	logger.trace("readCount: " + readCount);
	            os.write(buffer, 0, readCount);
	        }	       
	        os.flush();
	        os.close();
            
            byte[] resp = baos.toByteArray();
            String json = new String(resp, "UTF-8");
            logger.trace("json: " + json);           
            return gson.fromJson(json, response.getClass());
	    } catch (Exception e) {
	    	logger.error("ERROR! problem forword service call", e); 
		}   
    	return null;
    }
}
