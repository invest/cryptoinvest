<%@page import="com.anyoption.beans.User"%>

<HTML>
<head>
	<meta charset="utf-8"/>
	<meta name="viewport" content="height=device-height, initial-scale=1.0, minimum-scale=1.0"/>
	<script>
		var isActive = true;
		var pageUrl = '/jsonService/jsp/bubbles.jsp';
		window.history.pushState({}, "Bubbles", pageUrl + "?isActive=" + isActive);
	</script>
	<script src="./js/bubbles.js?version=cvsTimeStamp"></script>
	<style>
		*{padding:0;margin:0;}
		html,body{width:100%;height:100%;}
	</style>
</head>
<BODY>
<%
User sessionUser = (User) session.getAttribute("user");
out.println("<div id='" + User.createBubblesToken(session, sessionUser.getId()) + "'></div>");
String token = (String) session.getAttribute("token");
%>
	<script>
	var linkProd='https://m.visualtrade.com/#/';
	var linkTest='https://aom.visualtrade.com/#/';
	<%
	out.println("var params='?skinId="+sessionUser.getSkinId()+"&userId="+sessionUser.getId()+"&utcoffset="+User.getUtcOffsetInMin(sessionUser.getUtcOffset())+"&token="+token+"&s="+sessionUser.bubblesSignture(token, sessionUser.getSkinId())+"&timestamp="+new java.util.Date()+"'");
	%>
	</script>
	<div id="bubbles_holder" style="display:none;">
		<iframe id="bubbles" name="bubbles" frameborder="no" scrolling="no" style="width:100%;height:100%;" src=""></iframe>
	</div>
<script>showActive();showIframe();</script>
</BODY>

</HTML>