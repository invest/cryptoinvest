package com.anyoption.bl_vos;

import java.util.Date;

/**
 * TaxHistory Bean class.
 *
 * @author Kobi
 */

public class TaxHistory {

	 private long id;
	 private long user_id;
	 private String period;
	 private long tax;            // like user_balance for some period
	 private long win;            // sum of all wins for user in some period
	 private long lose;			  // sum of all loses for user in some period
	 private Date time_created;
	 private long writer_id;
	 private long year;			// tax year
	 private String utcOffsetCreated;
	 private long sumInvest;


	/**
	 * @return the year
	 */
	public long getYear() {
		return year;
	}

	/**
	 * @param year the year to set
	 */
	public void setYear(long year) {
		this.year = year;
	}

	/**
	 * @return the id
	 */
	public long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(long id) {
		this.id = id;
	}

	/**
	 * @return the lose
	 */
	public long getLose() {
		return lose;
	}

	/**
	 * @param lose the lose to set
	 */
	public void setLose(long lose) {
		this.lose = lose;
	}

	/**
	 * @return the period
	 */
	public String getPeriod() {
		return period;
	}

	/**
	 * @param period the period to set
	 */
	public void setPeriod(String period) {
		this.period = period;
	}

	/**
	 * @return the tax
	 */
	public long getTax() {
		return tax;
	}

	/**
	 * @param tax the tax to set
	 */
	public void setTax(long tax) {
		this.tax = tax;
	}

	/**
	 * @return the time_created
	 */
	public Date getTime_created() {
		return time_created;
	}

	/**
	 * @param time_created the time_created to set
	 */
	public void setTime_created(Date time_created) {
		this.time_created = time_created;
	}

	/**
	 * @return the user_id
	 */
	public long getUser_id() {
		return user_id;
	}

	/**
	 * @param user_id the user_id to set
	 */
	public void setUser_id(long user_id) {
		this.user_id = user_id;
	}

	/**
	 * @return the win
	 */
	public long getWin() {
		return win;
	}

	/**
	 * @param win the win to set
	 */
	public void setWin(long win) {
		this.win = win;
	}

	/**
	 * @return the writer_id
	 */
	public long getWriter_id() {
		return writer_id;
	}

	/**
	 * @param writer_id the writer_id to set
	 */
	public void setWriter_id(long writer_id) {
		this.writer_id = writer_id;
	}

	/**
	 * @return the utcOffsetCreated
	 */
	public String getUtcOffsetCreated() {
		return utcOffsetCreated;
	}

	/**
	 * @param utcOffsetCreated the utcOffsetCreated to set
	 */
	public void setUtcOffsetCreated(String utcOffsetCreated) {
		this.utcOffsetCreated = utcOffsetCreated;
	}

	/**
	 * @return the numInvest
	 */
	public long getSumInvest() {
		return sumInvest;
	}

	/**
	 * @param numInvest the numInvest to set
	 */
	public void setSumInvest(long sumInvest) {
		this.sumInvest = sumInvest;
	}

	/**
	 * Constructs a <code>String</code> with all attributes
	 * in name = value format.
	 *
	 * @return a <code>String</code> representation
	 * of this object.
	 */
	public String toString()
	{
	    final String TAB = " \n ";

	    String retValue = "";

	    retValue = "TaxHistory ( "
	        + super.toString() + TAB
	        + "id = " + this.id + TAB
	        + "user_id = " + this.user_id + TAB
	        + "period = " + this.period + TAB
	        + "tax = " + this.tax + TAB
	        + "win = " + this.win + TAB
	        + "lose = " + this.lose + TAB
	        + "time_created = " + this.time_created + TAB
	        + "utcOffsetCreated = " + this.utcOffsetCreated + TAB
	        + "writer_id = " + this.writer_id + TAB
	        + "sumInvest = " + this.sumInvest + TAB
	        + " )";

	    return retValue;
	}


}
