package com.anyoption.bl_vos;

import java.io.Serializable;
import java.util.Date;

public class ResetPassword implements Serializable {

	private static final long serialVersionUID = 1L;
	private long id;
	private long userId;
	private String userName;
	private long skinId;
	private String resetKey;
	private Date timeCreated;
	private Date timeFirstClicked;
	private long clicksNum;


	public long getClicksNum() {
		return clicksNum;
	}
	public void setClicksNum(long clicksNum) {
		this.clicksNum = clicksNum;
	}
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getResetKey() {
		return resetKey;
	}
	public void setResetKey(String resetKey) {
		this.resetKey = resetKey;
	}
	public Date getTimeCreated() {
		return timeCreated;
	}
	public void setTimeCreated(Date timeCreated) {
		this.timeCreated = timeCreated;
	}
	public Date getTimeFirstClicked() {
		return timeFirstClicked;
	}
	public void setTimeFirstClicked(Date timeFirstClicked) {
		this.timeFirstClicked = timeFirstClicked;
	}
	public long getUserId() {
		return userId;
	}
	public void setUserId(long userId) {
		this.userId = userId;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public long getSkinId() {
		return skinId;
	}
	public void setSkinId(long skinId) {
		this.skinId = skinId;
	}
}
