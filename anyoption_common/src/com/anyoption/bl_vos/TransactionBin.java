//package com.anyoption.bl_vos;
//
//import java.io.Serializable;
//import java.util.Date;
//
//public class TransactionBin implements Serializable{
//
//	/**
//	 * 
//	 */
//	private static final long serialVersionUID = 8401493451055185416L;
//
//	private long id;
//	private long bin;
//	private Date timeLastSuccess;
//	private Date timeLastFailedReroute;
//	private boolean updatedByReroute;
//	private long businessCaseId;
//	private int platformId;
//	
//	public long getId() {
//		return id;
//	}
//	public void setId(long id) {
//		this.id = id;
//	}
//	public long getBin() {
//		return bin;
//	}
//	public void setBin(long bin) {
//		this.bin = bin;
//	}
//	public Date getTimeLastSuccess() {
//		return timeLastSuccess;
//	}
//	public void setTimeLastSuccess(Date timeLastSuccess) {
//		this.timeLastSuccess = timeLastSuccess;
//	}
//	public Date getTimeLastFailedReroute() {
//		return timeLastFailedReroute;
//	}
//	public void setTimeLastFailedReroute(Date timeLastFailedReroute) {
//		this.timeLastFailedReroute = timeLastFailedReroute;
//	}
//	public boolean isUpdatedByReroute() {
//		return updatedByReroute;
//	}
//	public void setUpdatedByReroute(boolean updatedByReroute) {
//		this.updatedByReroute = updatedByReroute;
//	}
//	
//	/**
//	 * @return the businessCaseId
//	 */
//	public long getBusinessCaseId() {
//		return businessCaseId;
//	}
//	
//	/**
//	 * @param businessCaseId the businessCaseId to set
//	 */
//	public void setBusinessCaseId(long businessCaseId) {
//		this.businessCaseId = businessCaseId;
//	}
//	/**
//	 * @return the platformId
//	 */
//	public int getPlatformId() {
//		return platformId;
//	}
//	/**
//	 * @param platformId the platformId to set
//	 */
//	public void setPlatformId(int platformId) {
//		this.platformId = platformId;
//	}
//}
