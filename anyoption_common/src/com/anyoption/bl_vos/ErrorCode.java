package com.anyoption.bl_vos;

import java.io.Serializable;
import java.sql.SQLException;
import java.util.ArrayList;

import org.jfree.util.Log;

import com.anyoption.managers.ClearingManager;

public class ErrorCode implements Serializable{

	/**
	 *  @author oshikl
	 */
	private static final long serialVersionUID = 3619990662205098764L;

	private long id;
	private String result;
	private long clearingProviderId;
	private long clearingProviderIdGroup;
	private boolean isSpecialCode;
	
	private static ArrayList<ErrorCode> errorCodes;
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public boolean isSpecialCode() {
		return isSpecialCode;
	}
	public void setSpecialCode(boolean isSpecialCode) {
		this.isSpecialCode = isSpecialCode;
	}
	public String getResult() {
		return result;
	}
	public void setResult(String result) {
		this.result = result;
	}
	public long getClearingProviderId() {
		return clearingProviderId;
	}
	public void setClearingProviderId(long clearingProviderId) {
		this.clearingProviderId = clearingProviderId;
	}
	public long getClearingProviderIdGroup() {
		return clearingProviderIdGroup;
	}
	public void setClearingProviderIdGroup(long clearingProviderIdGroup) {
		this.clearingProviderIdGroup = clearingProviderIdGroup;
	}
	public static ArrayList<ErrorCode> getAllErrorCodes(){
		if(errorCodes == null){
			try{
				errorCodes = ClearingManager.getAllErrorCodes();
			}catch (SQLException sqle) {
				// TODO: handle exception
				Log.error("Can't load error codes.", sqle);
			}
		}
		return errorCodes;
	}
}
