//package com.anyoption.bl_vos;
//
//import java.util.HashMap;
//import java.util.Map;
//
//import org.apache.log4j.Logger;
//
//
///**
// * BinaryZeroOneHundred class
// * for saveing all the data of opp that we need to calculate the formula of BinaryZeroOneHundred
// *
// * @author Eyal
// *
// */
//public class BinaryZeroOneHundred implements java.io.Serializable {
//	private static final long serialVersionUID = 1L;
//	private static Logger log = Logger.getLogger(BinaryZeroOneHundred.class);
//
//	private double P; 	//Market difference multiply
//	private double L;  	//Exponentiation distance from market (Permanent )
//	private double T;	//Traders parameter
//	private double U; 	//Second’s factor
//	private double Q; 	//Quoted spread
//	private double Z; 	//
//	private double cThreashold; 	//C stop trading halt
//	private double tick; 	//tick deviation
//
//	public BinaryZeroOneHundred(String vars) {
//		changeParams(vars);
//	}
//
//	public void changeParams(String vars) {
//		log.debug("params: " + vars);
//		Map<String, Double> varsMap = parseUrlToPairs(vars);
//		if (null != varsMap.get("P")) {
//			this.P = varsMap.get("P");
//		}
//		if (null != varsMap.get("L")) {
//			this.L = varsMap.get("L");
//		}
//		if (null != varsMap.get("T")) {
//			this.T = varsMap.get("T");
//		}
//		if (null != varsMap.get("U")) {
//			this.U = varsMap.get("U");
//		}
//		if (null != varsMap.get("Q")) {
//			this.Q = varsMap.get("Q");
//		}
//		if (null != varsMap.get("Z")) {
//			this.Z = varsMap.get("Z");
//		}
//		if (null != varsMap.get("cThreashold")) {
//			this.cThreashold = varsMap.get("cThreashold");
//		}
//		if (null != varsMap.get("tick")) {
//			this.tick = varsMap.get("tick");
//		}
//	}
//
//	private Map<String, Double> parseUrlToPairs(String urlString) {
//		Map<String, Double> urlMapping = new HashMap<String, Double>();
//
//		String[] params = urlString.split(";");
//	    for (String param : params) {
//	    	String[] pair = param.split("=");
//	    	if (pair.length == 2) {
//	    		urlMapping.put(pair[0], Double.parseDouble(pair[1]));
//	    	}
//	    }
//		return urlMapping;
//	}
//
//	/**
//	 * @return the l
//	 */
//	public double getL() {
//		return L;
//	}
//
//	/**
//	 * @param l the l to set
//	 */
//	public void setL(double l) {
//		L = l;
//	}
//
//	/**
//	 * @return the p
//	 */
//	public double getP() {
//		return P;
//	}
//
//	/**
//	 * @param p the p to set
//	 */
//	public void setP(double p) {
//		P = p;
//	}
//
//	/**
//	 * @return the q
//	 */
//	public double getQ() {
//		return Q;
//	}
//
//	/**
//	 * @param q the q to set
//	 */
//	public void setQ(double q) {
//		Q = q;
//	}
//
//	/**
//	 * @return the t
//	 */
//	public double getT() {
//		return T;
//	}
//
//	/**
//	 * @param t the t to set
//	 */
//	public void setT(double t) {
//		T = t;
//	}
//
//	/**
//	 * @return the u
//	 */
//	public double getU() {
//		return U;
//	}
//
//	/**
//	 * @param u the u to set
//	 */
//	public void setU(double u) {
//		U = u;
//	}
//
//	/**
//	 * @return the z
//	 */
//	public double getZ() {
//		return Z;
//	}
//
//	/**
//	 * @param z the z to set
//	 */
//	public void setZ(double z) {
//		Z = z;
//	}
//
//	/**
//	 * @return the cThreashold
//	 */
//	public double getCThreashold() {
//		return cThreashold;
//	}
//
//	/**
//	 * @return the tick
//	 */
//	public double getTick() {
//		return tick;
//	}
//
//	/**
//	 * @param tick the tick to set
//	 */
//	public void setTick(double tick) {
//		this.tick = tick;
//	}
//
//	/**
//	 * @param threashold the cThreashold to set
//	 */
//	public void setCThreashold(double threashold) {
//		cThreashold = threashold;
//	}
//
//	public double getFormulaResultPositive(double a, double c, long b) {
//		log.debug("a = " + a + " c = " + c + " b = " + b + " P = " + P + " L = " + L);
//		log.debug("positive 50 + " + (Math.pow((a * P), L)) + " * " + (1 + 60 * U / b) + " + " + c/Z + " + " + T);
//        return 50 + (Math.pow((a * P), L)) * (1 + 60 * U / b) + c/Z + T;
//	}
//
//	public double getFormulaResultNegative(double a, double c, long b) {
//		log.debug("a = " + a + " c = " + c + " b = " + b + " P = " + P + " L = " + L);
//		log.debug("Negative 50 - " + (Math.pow((a * P), L)) + " * " + (1 + 60 * U / b) + " + " + c/Z + " + " + T);
//		return 50 - (Math.pow((a * P), L)) * (1 + 60 * U / b) + c/Z + T;
//	}
//
//	/**
//	 * check if 2 {@link BinaryZeroOneHundred} are diffrents only params for TT (Q, T, cThreashold)
//	 * @param oppParams {@link BinaryZeroOneHundred}
//	 * @return true if there is diffrent else false
//	 */
//	public String TTparams() {
//		return "cThreashold=" + this.cThreashold + "&Q=" + this.Q + "&T=" + this.T;
//	}
//
//	/**
//	 * return string as we have in db
//	 */
//	public String toString() {
//		return "P=" + this.P + ";L=" + this.L + ";T=" + this.T +
//			";U=" + this.U + ";Q=" + this.Q + ";Z=" +
//			this.Z + ";cThreashold=" + this.cThreashold + ";tick=" + this.tick;
//	}
//}
