//package com.anyoption.util;
//
//import java.sql.Connection;
//import java.sql.SQLException;
//import java.util.Locale;
//
//import org.apache.log4j.Logger;
//
//import com.anyoption.beans.User;
//import com.anyoption.common.beans.Investment;
//import com.anyoption.common.beans.InvestmentRejects;
//import com.anyoption.common.beans.OpportunityMiniBean;
//import com.anyoption.common.bl_vos.InvestmentLimit;
//import com.anyoption.common.daos.InvestmentsDAOBase;
//import com.anyoption.common.jms.WebLevelLookupBean;
//import com.anyoption.common.jms.WebLevelsCache;
//import com.anyoption.common.managers.DepositBonusBalanceManagerBase;
//import com.anyoption.common.managers.InvestmentRejectsManagerBase;
//import com.anyoption.common.managers.LanguagesManagerBase;
//import com.anyoption.common.service.requests.DepositBonusBalanceMethodRequest;
//import com.anyoption.common.util.MessageToFormat;
//import com.anyoption.common.util.OpportunityCacheBean;
//import com.anyoption.daos.UsersDAOBase;
//import com.anyoption.managers.InvestmentsManagerBase;
//import com.anyoption.managers.SkinsManagerBase;
//
//public abstract class InvestmentValidatorAbstract {
//    private static final Logger log = Logger.getLogger(InvestmentValidatorAbstract.class);
//
//    public abstract MessageToFormat validate(Connection conn, User user, Investment inv, OpportunityCacheBean opportunity,
//    		String sessionId, WebLevelsCache levelsCache, WebLevelLookupBean levelLookupBean, double convertAmount, InvestmentRejects invRej, double rate, double requestAmount) throws SQLException;
//
//    protected MessageToFormat validateOpportunityOpened(OpportunityMiniBean oppMiniBean, User user, String sessionId, InvestmentRejects invRej) {
//        if (oppMiniBean.isDisabled() || oppMiniBean.isMarketSuspended() || oppMiniBean.isOppSuspended()) {
//            
//            String errorMsg = "error.investment.expired";
//            
//            if (log.isInfoEnabled()) {
//                log.info("Opportunity disabled - user: " + user.getUserName() + " sessionId: " + sessionId + " disabled: " + oppMiniBean.isDisabled() +
//                	" market suspended: " + oppMiniBean.isMarketSuspended() + " opportunity suspended: " + oppMiniBean.isOppSuspended());
//            }
//            if (oppMiniBean.isDisabled()) {
//                invRej.setRejectTypeId(ConstantsBase.INVESTMENT_REJECT_OPP_DISABELD);
//                invRej.setRejectAdditionalInfo("Disabled:, opp id: " + oppMiniBean.getId());
//                InvestmentRejectsManagerBase.insertInvestmentReject(invRej);
//                errorMsg = "error.investment.disabled";
//            }
//            if(oppMiniBean.isOppSuspended()) {
//        	invRej.setRejectTypeId(ConstantsBase.INVESTMENT_REJECT_OPP_SUSPENDED);
//        	invRej.setRejectAdditionalInfo("Suspended:, opp id: " + oppMiniBean.getId());
//        	InvestmentRejectsManagerBase.insertInvestmentReject(invRej);
//        	errorMsg = "error.investment.disabled";
//            }
//            
//            return new MessageToFormat(errorMsg, null);
//        }
//        return null;
//    }
//
//    /**
//     * Check if this user can make this investment balance-wise.
//     *
//     * @param conn
//     * @param user the user who wants to make investment (the balance should be up to date)
//     * @param slipEntry the slip entry of the investment
//     * @return <code>null</code> if check is ok else error message.
//     * @throws SQLException
//     */
//    protected static MessageToFormat validateUsersBalance(Connection conn, User user, Investment inv) throws SQLException {
//        UsersDAOBase.getByUserName(conn, user.getUserName(), user); // actually take the current balance
//        long balance;
//        if (inv.getTypeId() == Investment.TYPE_BUBBLE) { // do not include bonus balance in bubbles investments
//        	DepositBonusBalanceMethodRequest request = new DepositBonusBalanceMethodRequest();
//        	request.setBalanceCallType(DepositBonusBalanceMethodRequest.CALL_TOTAL_AMOUNT_BALANCE);
//        	request.setUserId(user.getId());
//        	balance = DepositBonusBalanceManagerBase.getDepositBonusBalance(request).getDepositCashBalance();
//        } else {
//        	balance = user.getBalance();
//        }
//        if (balance < inv.getAmount() + inv.getOptionPlusFee()) {
//            if (log.isDebugEnabled()) {
//                log.debug("Not enough money. Balance is: " + user.getBalance() + " amount: " + inv.getAmount() + " optionPlusFee: " + inv.getOptionPlusFee());
//            }
//            return new MessageToFormat("error.investment.nomoney", null);
//        }
//        return null;
//    }
//
//    /**
//     * Cehck if this user can make this investment. Check if he has enough money and that
//     * the amount he want to invest is in the allowed interval (min, max).
//     *
//     * @param conn
//     * @param user the user who wants to make investment (the balance should be up to date)
//     * @param slipEntry the slip entry of the investment
//     * @param pageOddsWin the current odds win displayed on the client page
//     * @param pageOddsLose the current odds lose displayed on the client page
//     * @param invRej the reject investment detaiis
//     * @param utcOffsetCreated 
//     * @return <code>null</code> if check is ok else error message.
//     * @throws SQLException
//     */
//    protected static MessageToFormat validateInvestment(Connection conn, User user, OpportunityCacheBean o, long amount, double pageOddsWin, double pageOddsLose, InvestmentRejects invRej, String utcOffsetCreated) throws SQLException {
//        InvestmentLimit il = InvestmentsDAOBase.getInvestmentLimit(conn, o.getOpportunityTypeId(), o.getScheduled(), o.getMarketId(), user.getCurrencyId(), user.getId());
//        long minAmount = il.getMinAmount();
//        long maxAmount = il.getMaxAmount();
//        if (amount < minAmount || amount > maxAmount) {
//            if (log.isDebugEnabled()) {
//                log.debug("Limit reached. min: " + minAmount + " max: " + maxAmount);
//            }
//
//            int rejetcTypeId = ConstantsBase.INVESTMENT_REJECT_MIN_INV_LIMIT;
//            String errorMsg = "";
//            long limitAmount;
//            if ( amount < minAmount ) {
//                invRej.setRejectAdditionalInfo("Min Limit:, amount: " + amount + " , minimum: " + minAmount);
//                errorMsg = "error.investment.limit.min";
//                limitAmount = minAmount;
//            } else {
//                invRej.setRejectAdditionalInfo("Max Limit:, amount: " + amount + " , maximum: " + maxAmount);
//                rejetcTypeId = ConstantsBase.INVESTMENT_REJECT_MAX_INV_LIMIT;
//                errorMsg = "error.investment.limit.max";
//                limitAmount = maxAmount;
//            }
//
//            invRej.setRejectTypeId(rejetcTypeId);
//            InvestmentRejectsManagerBase.insertInvestmentReject(invRej);
//            return new MessageToFormat(errorMsg, new Object[] {CommonUtil.formatCurrencyAmount(limitAmount, true, user.getCurrencyId())});
//        } else if (invRej.getTypeId() != Investment.TYPE_BUBBLE) {
//	        long sumInv = InvestmentsManagerBase.getSumAllActiveInvestments(o.getId(), user.getId());
//	        double invLimit = maxAmount * o.getMaxInvAmountCoeffPerUser();
//	        if (amount + sumInv > invLimit) {
//				log.debug("Max investments amount reached, userId: "+ user.getId() + ", active investments: " + sumInv + ", limit: "
//							+ invLimit);
//				int rejetcTypeId = ConstantsBase.INVESTMENT_REJECT_INV_SUM_LIMIT;
//				invRej.setRejectTypeId(rejetcTypeId);
//				InvestmentRejectsManagerBase.insertInvestmentReject(invRej);
//				Locale locale = new Locale(LanguagesManagerBase	.getLanguage(SkinsManagerBase.getSkin(user.getSkinId()).getDefaultLanguageId())
//																.getCode());
//				return new MessageToFormat(	"error.investment.sum.limit",
//											new Object[] {	CommonUtil.getMessage(o.getMarketDisplayName(), null, locale),
//															CommonUtil.getTimeFormat(o.getTimeEstClosing(), utcOffsetCreated)});
//	        }
//        }
//        return null;
//    }
//    
//    protected String printDetails(String userName, String sessionId, OpportunityCacheBean o, Investment inv, WebLevelLookupBean wllb) {
//        return  " user: " + userName +
//                " sessionId: " + sessionId +
//                " marketId: " + o.getMarketId() +
//                " exp time: " + o.getTimeEstClosing() +
//                " amount: " + inv.getAmount() +
//                " return: " + inv.getOddsWin() +
//                " pageLevel: " + inv.getCurrentLevel();
//    }
//}