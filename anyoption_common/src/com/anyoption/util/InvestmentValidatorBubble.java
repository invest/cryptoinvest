package com.anyoption.util;

import java.sql.Connection;
import java.sql.SQLException;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.User;
import com.anyoption.common.beans.Writer;
import com.anyoption.beans.InvestmentLimit;
import com.anyoption.common.beans.ExposureBean;
import com.anyoption.common.beans.Investment;
import com.anyoption.common.beans.InvestmentRejects;
import com.anyoption.common.beans.OpportunityMiniBean;
import com.anyoption.common.enums.SkinGroup;
import com.anyoption.common.jms.WebLevelLookupBean;
import com.anyoption.common.jms.WebLevelsCache;
import com.anyoption.common.managers.InvestmentRejectsManagerBase;
import com.anyoption.common.managers.OpportunitiesManagerBase;
import com.anyoption.common.util.CommonUtil;
import com.anyoption.common.util.ConstantsBase;
import com.anyoption.common.util.InvestmentValidatorAbstract;
import com.anyoption.common.util.MessageToFormat;
import com.anyoption.common.util.OpportunityCacheBean;
import com.anyoption.daos.LimitsDAOBase;
import com.anyoption.managers.InvestmentsManagerBase;
import com.anyoption.managers.UsersManagerBase;

public class InvestmentValidatorBubble extends InvestmentValidatorAbstract {
    private static final Logger log = Logger.getLogger(InvestmentValidatorBubble.class);

    @Override
    public MessageToFormat validate(Connection conn, User user, Investment inv, OpportunityCacheBean o, String sessionId, WebLevelsCache levelsCache, WebLevelLookupBean levelLookupBean, double convertAmount, InvestmentRejects invRej, double rate, double requestAmount) throws SQLException {
    	MessageToFormat msg = validateCurrentLevel(inv.getCurrentLevel());
    	OpportunityMiniBean oppMiniBean = null;
		if (msg == null) {
			oppMiniBean = OpportunitiesManagerBase.getOpportunityMiniBean(o.getId());
			msg = validateOpportunityOpened(oppMiniBean, user, sessionId, invRej);
		}
        if (null == msg) {//opp is open
            msg = validateUsersBalance(conn, user, inv);
            if (null == msg) { //user have money
                long s = System.currentTimeMillis();
				msg = (inv.getWriterId() != Writer.WRITER_ID_BUBBLES_DEDICATED_APP)	? validateInvestment(	conn, user, o, inv.getAmount(),
																											inv.getOddsWin(),
																											inv.getOddsLose(), invRej,
																											inv.getUtcOffsetCreated())
																					: validateBDAInvestment(conn, user.getCurrencyId(),
																											inv.getAmount(), invRej);
                if (log.isDebugEnabled()) {
                    log.debug("TIME: validateInvestmentLimits " + (System.currentTimeMillis() - s));
                }
                if (null == msg && 
                		UsersManagerBase.isUserMarketDisabled(user.getId(), o.getMarketId(), o.getScheduled(),
                				false, inv.getApiExternalUserId())) {//investment amount is good check user disable
                    log.warn("User Market Disabled..." + printDetails(user.getUserName(), sessionId, o, inv, null));
                    invRej.setRejectTypeId(ConstantsBase.INVESTMENT_REJECT_USER_MARKET_DISABELD);
                    invRej.setRejectAdditionalInfo("User Market Disabled");
                    InvestmentRejectsManagerBase.insertInvestmentReject(invRej);
                    msg = new MessageToFormat("error.investment.deviation", null);
                }
                if (null == msg) { //check that opp is open for inv (we dont check odds!!!)
                    msg = InvestmentsManagerBase.validateOpportunity(user, o, oppMiniBean, inv.getOddsWin(), inv.getOddsLose(), invRej);
                    if (null == msg) { //not user market disable
                        // balance check before insert investment to avoid negative balance when doing simultaneous investments
                        msg = validateUsersBalance(conn, user, inv);
                    }
                }
                
                //check if worst case return is reached
                ExposureBean exposureBean = null;
                           
                if(msg == null && (exposureBean = InvestmentsManagerBase.checkInvestmentExposure(o.getId(), inv.getAmount(), convertAmount, inv.getTypeId(), invRej, SkinGroup.getById(user.getPlatformId()), inv.getMarketId(), user.getLocale(), user.getCurrencyId(), o.getScheduled(), user.getSkinId())) != null){
                	if(exposureBean.getErrorMessageKey() != null){
                		msg = new MessageToFormat(exposureBean.getErrorMessageKey(), exposureBean.getErrorMessageParams());
                	}
                	if (exposureBean.isMaxExposureReached()) {
        	            log.warn("Worst case return: " + printDetails(user.getUserName(), sessionId, o, inv, null));
                	}
                }
            }
        }
        return msg;
    }

	private MessageToFormat validateBDAInvestment(	Connection con, Long currencyId, long amount,
													InvestmentRejects invRej) throws SQLException {
		InvestmentLimit il = LimitsDAOBase.getBDAInvestmentLimit(con, currencyId);
		long minAmount = il.getMinAmount();
		long maxAmount = il.getMaxAmount();
		if (amount < minAmount || amount > maxAmount) {
			if (log.isDebugEnabled()) {
				log.debug("Limit reached. min: " + minAmount + " max: " + maxAmount);
			}
			int rejetcTypeId = ConstantsBase.INVESTMENT_REJECT_MIN_INV_LIMIT;
			String errorMsg = "";
			long limitAmount;
			if (amount < minAmount) {
				invRej.setRejectAdditionalInfo("Min Limit:, amount: " + amount + " , minimum: " + minAmount);
				errorMsg = "error.investment.limit.min";
				limitAmount = minAmount;
			} else {
				invRej.setRejectAdditionalInfo("Max Limit:, amount: " + amount + " , maximum: " + maxAmount);
				rejetcTypeId = ConstantsBase.INVESTMENT_REJECT_MAX_INV_LIMIT;
				errorMsg = "error.investment.limit.max";
				limitAmount = maxAmount;
			}
			invRej.setRejectTypeId(rejetcTypeId);
			InvestmentRejectsManagerBase.insertInvestmentReject(invRej);
			return new MessageToFormat(errorMsg, new Object[] {CommonUtil.formatCurrencyAmountAO(limitAmount, true, currencyId)});
		}
		return null;
	}

	@Override
    protected String printDetails(String userName, String sessionId, OpportunityCacheBean o, Investment inv, WebLevelLookupBean wllb) {
		String details = super.printDetails(userName, sessionId, o, inv, wllb);
		details = " type id (BUBBLE = 3): " + inv.getTypeId();
    	return details;
    }
}