//package com.anyoption.util;
//
//import java.util.Date;
//
///**
// * 
// * @author liors
// *
// */
//public class OpportunityCacheBean {
//    private long id;
//    private Date timeFirstInvest;
//    private Date timeEstClosing;
//    private Date timeLastInvest;
//    private int scheduled;
//    private String marketDisplayName;
//    private Long marketDecimalPoint;
//    private long marketId;
//    private long opportunityTypeId;
//	private double maxInvAmountCoeffPerUser;
//
//    /**
//     * @return id
//     */
//    public long getId() {
//        return id;
//    }
//
//    /**
//     * @param id
//     */
//    public void setId(long id) {
//        this.id = id;
//    }
//
//    /**
//     * @return timeFirstInvest
//     */
//    public Date getTimeFirstInvest() {
//        return timeFirstInvest;
//    }
//
//    /**
//     * @param timeFirstInvest
//     */
//    public void setTimeFirstInvest(Date timeFirstInvest) {
//        this.timeFirstInvest = timeFirstInvest;
//    }
//
//    /**
//     * @return timeEstClosing
//     */
//    public Date getTimeEstClosing() {
//        return timeEstClosing;
//    }
//
//    /**
//     * @param timeEstClosing
//     */
//    public void setTimeEstClosing(Date timeEstClosing) {
//        this.timeEstClosing = timeEstClosing;
//    }
//
//    /**
//     * @return timeLastInvest
//     */
//    public Date getTimeLastInvest() {
//        return timeLastInvest;
//    }
//
//    /**
//     * @param timeLastInvest
//     */
//    public void setTimeLastInvest(Date timeLastInvest) {
//        this.timeLastInvest = timeLastInvest;
//    }
//
//    /**
//     * @return scheduled
//     */
//    public int getScheduled() {
//        return scheduled;
//    }
//
//    /**
//     * @param scheduled
//     */
//    public void setScheduled(int scheduled) {
//        this.scheduled = scheduled;
//    }
//
//    /**
//     * @return marketDecimalPoint
//     */
//    public Long getMarketDecimalPoint() {
//        return marketDecimalPoint;
//    }
//
//    /**
//     * @param marketDecimalPoint
//     */
//    public void setMarketDecimalPoint(Long marketDecimalPoint) {
//        this.marketDecimalPoint = marketDecimalPoint;
//    }
//
//    /**
//     * @return marketDisplayName
//     */
//    public String getMarketDisplayName() {
//        return marketDisplayName;
//    }
//
//    /**
//     * @param marketDisplayName
//     */
//    public void setMarketDisplayName(String marketDisplayName) {
//        this.marketDisplayName = marketDisplayName;
//    }
//
//	public long getMarketId() {
//		return marketId;
//	}
//
//	public void setMarketId(long marketId) {
//		this.marketId = marketId;
//	}
//
//	public long getOpportunityTypeId() {
//		return opportunityTypeId;
//	}
//
//	public void setOpportunityTypeId(long opportunityTypeId) {
//		this.opportunityTypeId = opportunityTypeId;
//	}
//
//	public double getMaxInvAmountCoeffPerUser() {
//		return maxInvAmountCoeffPerUser;
//	}
//
//	public void setMaxInvAmountCoeffPerUser(double maxInvAmountCoeffPerUser) {
//		this.maxInvAmountCoeffPerUser = maxInvAmountCoeffPerUser;
//	}
//}