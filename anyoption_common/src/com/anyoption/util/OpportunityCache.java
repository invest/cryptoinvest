//package com.anyoption.util;
//
//import java.sql.SQLException;
//import java.util.Hashtable;
//import java.util.Iterator;
//
//import org.apache.log4j.Logger;
//
//import com.anyoption.common.util.OpportunityCacheBean;
//import com.anyoption.managers.OpportunitiesManagerBase;
//
///**
// * 
// * @author liors
// *
// */
//public class OpportunityCache {
//    private static final Logger log = Logger.getLogger(OpportunityCache.class);
//    
//    private Hashtable<Long, OpportunityCacheBean> opportunities;
//    private OpportunityCacheCleaner cleaner;
//    
//    public OpportunityCache() {
//        opportunities = new Hashtable<Long, OpportunityCacheBean>();
//        cleaner = new OpportunityCacheCleaner();
//        cleaner.start();
//    }
//    
//    public OpportunityCacheBean getOpportunity(long oppId) throws SQLException {
//        if (oppId != 0 && !opportunities.containsKey(oppId)) {
//            OpportunityCacheBean b = OpportunitiesManagerBase.getOpportunityCacheBean(oppId);
//            if (null != b) {
//                opportunities.put(oppId, b);
//                if (log.isDebugEnabled()) {
//                    log.debug("Added opportunity " + oppId);
//                }
//            } else {
//                log.warn("No opportunity with id " + oppId);
//            }
//        }
//        return opportunities.get(oppId);
//    }
//    
//    public void stopOpportunityCacheCleaner() {
//        cleaner.stopOpportunityCacheCleaner();
//    }
//    
//    class OpportunityCacheCleaner extends Thread {
//        private boolean running;
//        
//        public void run() {
//            Thread.currentThread().setName("OpportunityCacheCleaner");
//            log.info("OpportunityCacheCleaner starting...");
//            running = true;
//            while (running) {
//                log.debug("Clean OpportunityCache...");
//                try {
//                    long halfAnHourAgo = System.currentTimeMillis() - 30 * 60 * 1000;
//                    Iterator<Long> i = opportunities.keySet().iterator();
//                    OpportunityCacheBean b = null;
//                    while (i.hasNext()) {
//                        b = opportunities.get(i.next());
//                        if (b.getTimeEstClosing().getTime() < halfAnHourAgo) {
//                            i.remove();
//                            log.trace("Removed opp " + b.getId());
//                        }
//                    }
//                    if (log.isDebugEnabled()) {
//                        log.debug("OpportunityCache size: " + opportunities.size());
//                    }
//                    Thread.sleep(10 * 60 * 1000);
//                } catch (Exception e) {
//                    log.error("Error while cleaning OpportunityCache.", e);
//                }
//            }
//            log.info("OpportunityCacheCleaner done.");
//        }
//        
//        public void stopOpportunityCacheCleaner() {
//            log.info("OpportunityCacheCleaner stopping...");
//            running = false;
//            this.interrupt();
//        }
//    }
//}