package com.anyoption.util;

import java.text.DecimalFormat;
import java.util.Locale;

import com.anyoption.common.beans.base.BonusUsers;

public class BonusUserFormater {
	
	public static void initFormattedValues(BonusUsers bu, Locale locale, String utcOffset) {
		setEndDateTxt(bu, utcOffset);
		setStartDateTxt(bu, utcOffset);
		setBonusDescriptionTxt(bu, locale);
		setBonusStateTxt(bu, locale);
		setGeneralTermsCaption(bu, locale);
	}
	
	/**
	 * @return the bonusStateTxt
	 */
	public static void setBonusStateTxt(BonusUsers bu, Locale locale) {
		long returnState = 0L;
		if (bu.getBonusStateId() == ConstantsBase.BONUS_STATE_WITHDRAWN || bu.getBonusStateId() == ConstantsBase.BONUS_STATE_WAGERING_WAIVED) {			
			returnState = ConstantsBase.BONUS_STATE_USED;
		} else {
			returnState = bu.getBonusStateId();
		}
		bu.setBonusStateTxt(CommonUtil.getMessage("bonus.state" + returnState, null, locale)); 
	}
	
	/**
	 * @return the bonusStateDescriptionTxt
	 */
	public static void setBonusDescriptionTxt(BonusUsers bu, Locale locale) {
		String[] params = new String[7];
		params[0] = String.valueOf(CommonUtil.formatCurrencyAmountAO(bu.getBonusAmount(), bu.getCurrency().getId()));
		params[1] = String.valueOf(bu.getInvestmentId());
		String format = "##0";
		DecimalFormat fmt = new DecimalFormat(format);
		params[2] = String.valueOf(fmt.format(bu.getBonusPercent() * 100));
		params[3] = String.valueOf(CommonUtil.formatCurrencyAmountAO(bu.getSumInvQualify(), bu.getCurrency().getId()));
		params[4] = String.valueOf(bu.getNumOfActions());
		params[5] = String.valueOf(bu.getNumOfActions() + 1);
		params[6] = String.valueOf(CommonUtil.formatCurrencyAmountAO(bu.getMinDepositAmount(), bu.getCurrency().getId()));
		bu.setBonusDescriptionTxt(CommonUtil.getMessage(bu.getBonusDescription() , params, locale));
	}
	
	/**
	 * @return the endDateTxt
	 */
	public static void setEndDateTxt(BonusUsers bu, String utcOffset) {
		bu.setEndDateTxt(CommonUtil.getDateAndTimeFormatByDots(bu.getEndDate(), utcOffset));
	}

	/**
	 * @return the startDateTxt
	 */
	public static void setStartDateTxt(BonusUsers bu, String utcOffset) {
		bu.setStartDateTxt( CommonUtil.getDateAndTimeFormatByDots(bu.getStartDate(), utcOffset));
	}
	
	/**
	 * @return the generalTermsCaption
	 */
	public static void setGeneralTermsCaption(BonusUsers bu, Locale locale) {
		bu.setGeneralTermsCaption(CommonUtil.getMessage("CMS.termsBonus" + bu.getTypeId() + ".text.t1", null, locale));
	}
}
