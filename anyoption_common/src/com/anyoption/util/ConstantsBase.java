package com.anyoption.util;

public class ConstantsBase extends com.anyoption.common.util.ConstantsBase {
	public static final int WEB_CONST = 1;
	public static final int BACKEND_CONST = 2;
//	public static final long DEFAULT_AD_ID = 1;
//	public static final long DEFAULT_AD_ID_ANYOPTION = 1000;    // default(en)
//	public static final String DEFAULT_COMBINATION_NAME_ETRADER = "etrader";
//	public static final String DEFAULT_COMBINATION_NAME_ANYOPTION = "anyoption_en";
//	public static final long AD_ID_MERLIN = 11;
	//public static final long AD_ID_MERLIN2 = 28;
	//public static final long AD_ID_ADS_MARKET = 31;
	public static final long VALIDATION_MAIL_TEMPLATE_ID = 35;

	public static final int GENERAL_NO = 1;
	public static final int GENERAL_YES = 2;

	public static final String BIND_CACHE_OBJECTS = "#{cacheObjects}";
	public static final String BIND_WRITER_WRAPPER = "#{writer}";
	public static final String BIND_USER = "#{user}";
	public static final String BIND_POPULATION_ENTRY = "#{populationEntry}";
	public static final String BIND_SESSION_USER = "user";
	public static final String BIND_SESSION_TRANSACTION = "transaction";
	public static final String LOGIN_ID = "loginId";
	public static final String BIND_USER_Form = "#{userForm}";
	public static final String BIND_INVESTMENTS_LIST = "#{investmentsList}";

	public static final String BIND_APPLICATION_DATA = "#{applicationData}";
	public static final String BIND_VISIT = "#{visit}";
	public static final String BIND_MESSAGES_LIST = "#{messagesList}";

    public static final String BIND_TRADING_PAGE_BEAN = "#{tradingPageBean}";
    public static final String BIND_CLEARING_INFO = "#{clearingInfo}";
    public static final String BIND_INATEC_INFO = "#{inatecInfo}";
    public static final String BIND_ENVOY_INFO = "#{envoyInfo}";

	//public static final String ENUM_WITHDRAWAL_FEE_AMOUNT = "1000";
	//public static final String ENUM_FEES = "fees";
	public static final String ENUM_CREDIT_CARDS_NOT_VISIBLE_LIST = "cc_not_visible_list";
	//public static final String ENUM_DEFAULT_FEE = "default_fee";
	//public static final String ENUM_DEFAULT_FEE_CC = "default_fee_cc";
	//public static final String ENUM_MAX_AMOUNT_FOR_FEE = "maxamount_for_fee";
	public static final String ENUM_LOG_BALANCE = "log_balance";

    public static final Boolean ENUM_WITH_FEE = true;
	//public static final String ENUM_MARKETS = "markets";
	//public static final String ENUM_DEFAULT_GROUP_MARKET = "default_group_market";
    //public static final String ENUM_FREE_INVEST = "free_invest";
    //public static final String ENUM_FREE_INVEST_MIN = "free_invest_min";
    //public static final String ENUM_FREE_INVEST_MAX = "free_invest_max";

	public static final String ERROR_MIN_DEPOSIT="error.min.deposit";
	public static final String ERROR_MAX_DEPOSIT="error.max.deposit";
	public static final String ERROR_MAX_DAILY_DEPOSIT="error.max.daily.deposit";
	public static final String ERROR_MAX_MONTHLY_DEPOSIT="error.max.monthly.deposit";
	public static final String ERROR_CARD_NOT_ALLOWED_WRONG_ID="error.card.not.allowed.wrong.id";
	public static final String ERROR_CARD_NOT_ALLOWED_DUP_CARD="error.card.not.allowed.dup.card";
	//public static final String ERROR_CARD_NOT_ALLOWED="error.card.not.allowed";
	public static final String ERROR_CARD_COUNTRY_BLOCKED="error.country.blocked";
	public static final String ERROR_CARD_EXPIRED = "error.creditcard.expdate";
	public static final String ERROR_CARD_FAILURE_REPEAT = "error.card.failure.repeat";

	public static final String ERROR_CARD_IN_USE="error.card.already.inuse";
	public static final String ERROR_BLACK_LIST="error.black.list";
	public static final String ERROR_BIN_BLACK_LIST="error.bin.black.list";
	public static final String ERROR_FAILED_CLEARING="error.card.clearing";
	public static final String ERROR_CLEARING_TIMEOUT="error.creditcard.timeout";
	public static final String ERROR_ILLEGAL_HOLDER_ID="error.creditcard.holderid.not.match.card";
	public static final String ERROR_FAILED_CAPTURE="error.card.capture";
	public static final String ERROR_BONUS_DEPOSIT="error.bonus.deposit";
	public static final String ERROR_BONUS_WITHDRAW="error.bonus.withdraw";
	public static final String ERROR_DEPOSIT_DOCS_REQUIRED="error.deposit.docs.required";

	public static final String ERROR_DEPOSIT_DOCS_REQUIRED_MSG="error.deposit.docs.required.msg";

    public static final String PROVIDER_SIGN_DEFAULT = "SMC";

    public static final String XOR_PROVIDER = "xor";
    public static final String XOR_CONFIG_FILE_PATH = "xor.properties";
    public static final String XOR_TRANSACTION_DIRECTION_CREDIT = "Credit";
    public static final String XOR_TRANSACTION_DIRECTION_DEBIT = "Debit";

    public static final String ROLE_SUPPORT= "support";
    public static final String ROLE_ACCOUNTING= "accounting";
    public static final String ROLE_TRADER= "trader";
    public static final String ROLE_RETENTION= "retention";
    public static final String ROLE_RETENTIONM= "retentionM";
    public static final String ROLE_PARTNER= "partner";
	public static final String ROLE_ADMIN= "admin";
	public static final String ROLE_SADMIN= "sadmin";
	public static final String ROLE_STRADER= "strader";
	public static final String ROLE_SSUPPORT= "ssupport";
	public static final String ROLE_ASTRADER= "astrader";  // above strader
	public static final String ROLE_MARKETING= "marketing";
	public static final String ROLE_TV= "tv";

	public static final int LOG_COMMANDS_CANCEL_ALL_INV = 1;
	public static final int LOG_COMMANDS_UPDATE_OPP = 2;
	public static final int LOG_COMMANDS_CHANGE_OPP_ODS = 3;
	public static final int LOG_COMMANDS_UPDATE_MARKET = 4;
	public static final int LOG_COMMANDS_UPDATE_TA25 = 5;
	public static final int LOG_COMMANDS_MANUAL_SETTLE = 6;
	public static final int LOG_COMMANDS_ENABLE_OPP = 7;
	public static final int LOG_COMMANDS_DISABLE_OPP = 8;
	public static final int LOG_COMMANDS_UPDATE_SHIFT_PERC = 9;
	public static final int LOG_COMMANDS_UPDATE_SHIFT_PT = 10;
	public static final int LOG_COMMANDS_RESETTLE_OPP = 11;
	public static final int LOG_COMMANDS_DISABLE_MARKET = 12;
	public static final int LOG_COMMANDS_ENABLE_MARKET = 13;
    public static final int LOG_COMMANDS_MONITORING_DISABLE_OPP = 14;
    public static final int LOG_COMMANDS_MONITORING_ENABLE_OPP = 15;
    public static final int LOG_COMMANDS_UPDATE_FTSE = 16;
    public static final int LOG_COMMANDS_UPDATE_CAC = 17;
    public static final int LOG_COMMANDS_UPDATE_SP500 = 18;
    public static final int LOG_COMMANDS_UPDATE_KLSE = 19;
    public static final int LOG_COMMANDS_UPDATE_D4BANKS = 20;
    public static final int LOG_COMMANDS_UPDATE_BANKSBURSA = 21;
    public static final int LOG_COMMANDS_UPDATE_D4SPREAD = 22;


    public static final String TABLE_TAX_HISTORY = "tax_history";
    public static final String TABLE_BONUS_USERS = "bonus_users";

    public static final int LOG_BALANCE_CC_DEPOSIT = 1;
    public static final int LOG_BALANCE_CANCEL_DEPOSIT = 2;
    public static final int LOG_BALANCE_ADMIN_DEPOSIT = 3;
    public static final int LOG_BALANCE_ADMIN_WITHDRAW = 4;
    public static final int LOG_BALANCE_WIRE_DEPOSIT = 5;
    public static final int LOG_BALANCE_REJECT_WITHDRAW = 6;
    public static final int LOG_BALANCE_REVERSE_WITHDRAW = 7;
    public static final int LOG_BALANCE_CANCEL_INVESTMENT = 8;
    public static final int LOG_BALANCE_CHEQUE_WITHDRAW = 9;
    public static final int LOG_BALANCE_INSERT_INVESTMENT = 10;
    public static final int LOG_BALANCE_SETTLE_INVESTMENT = 11;
    public static final int LOG_BALANCE_CASH_DEPOSIT = 12;
    public static final int LOG_BALANCE_TAX_PAYMENT = 13;
    public static final int LOG_BALANCE_CC_WITHDRAW = 14;
    public static final int LOG_BALANCE_BONUS_DEPOSIT = 15;
    public static final int LOG_BALANCE_BONUS_WITHDRAW = 16;
    public static final int LOG_BALANCE_BANK_WIRE = 17;
    public static final int LOG_BALANCE_DIRECT_DEPOSIT = 18;
    public static final int LOG_BALANCE_POINTS_TO_CASH = 19;
    public static final int LOG_BALANCE_FIX_BALANCE_DEPOSIT = 20;
    public static final int LOG_BALANCE_FIX_BALANCE_WITHDRAW = 21;
    public static final int LOG_BALANCE_DEPOSIT_BY_COMPANY = 22;
    public static final int LOG_BALANCE_PAYPAL_DEPOSIT = 23;
    public static final int LOG_BALANCE_PAYPAL_WITHDRAW = 24;
    public static final int LOG_BALANCE_CASHU_DEPOSIT = 25;
    public static final int LOG_BALANCE_ACH_DEPOSIT = 26;
    public static final int LOG_BALANCE_UKASH_DEPOSIT = 27;
    public static final int LOG_BALANCE_CHARGE_BACK = 28;
    public static final int LOG_BALANCE_ENVOY_WITHDRAW = 29;
    public static final int LOG_BALANCE_MONEYBOOKERS_DEPOSIT = 30;
    public static final int LOG_BALANCE_RESETTLE_INVESTMENT = 31;
    public static final int LOG_BALANCE_EFT_DEPOSIT = 32;
    public static final int LOG_BALANCE_EFT_WITHDRAW = 33;
    public static final int LOG_BALANCE_MONEYBOOKERS_WITHDARW = 34;
    public static final int LOG_BALANCE_WEBMONEY_DEPOSIT = 35;
    public static final int LOG_BALANCE_BAROPAY_WITHDRAW = 42;
    public static final int LOG_BALANCE_COPYOP_COINS_TO_CASH = 48;

	public static final int USER_CLASS_COMPANY = 2;
	public static final int USER_CLASS_PRIVATE= 1;
	public static final int USER_CLASS_TEST = 0;
	public static final int USER_CLASS_ALL = 100;
	public static final int USER_CLASS_COMPANY_OR_PRIVATE = 101;
	public static final String USER_CLASS_COMPANY_OR_PRIVATE_LABLE = "user.class.privatecompany";

	public static final long INVESTMENT_CLASS_STATUS_CANCELLED = 0;
	public static final long INVESTMENT_CLASS_STATUS_ACTIVE = 1;
	public static final long INVESTMENT_CLASS_STATUS_CANCELLED_AND_NON_CANCELLED_INVESTMENT = 2;

	public static final double TAX_PERCENTAGE = 0.25;

	public static final int MAX_COMMENTS = 80;
	public static final int MAX_TRANSACTION_COMMENTS = 80;

	public static final int LINE_TYPE_REGULAR = 0;
	public static final int LINE_TYPE_COMMENT = 1;
	public static final int LINE_TYPE_PAGE_SUMMARY = 2;
	public static final int LINE_TYPE_GENERAL_SUMMARY = 3;

    public static final int NUMBER_OPPORTUNITIES_IN_BANNER = 5;

    public static final String ENUM_TAX_PERIOD = "period";
    public static final String ENUM_TAX_PERIOD_HALF_YEAR = "period_half_year";
    public static final String ENUM_TAX_PERIOD_FULL_YEAR = "period_full_year";

    public static final String CREDIT_CARD_ISRACARD_TYPE = "4";

    public static final String NAV_FIRST_DEPOSIT = "firstDeposit";

    public static int DEPARTMENT_SUPPORT = 2;
    public static int DEPARTMENT_RETENTION = 3;

    ///Add enums for one touch option validation

    public static final String ENUM_ONE_TOUCH = "one_touch";
    public static final String ENUM_ONE_TOUCH_USER_ASSET_MAX = "one_touch_max_asset";
    public static final String ENUM_ONE_TOUCH_USER_ASSET_MIN = "one_touch_min_asset";
    public static final String ENUM_ONE_TOUCH_ALL_USERS_MAX = "one_touch_max_users";

    //Default time for refresh ENUMS
    public static final long DEFAULT_ENUMS_REFRESH = 60000;

    public static final int CC_FEE_CANCEL_YES = 1;
    public static final int CC_FEE_CANCEL_NO = 0;

    //dropDowns
    public static final String INVESTMENT_CLASS_STATUS_DROPDOWN= "InvestmentsClassStatusDropdown";
	public static final String XOR_TRANSACTION_FORM_CLASS_TYPE_DROPDOWN= "XorTransactionFormClassTypeDropdown";

	public static final String EMPTY_STRING = "";
	public static final String SPACE = " ";

	public static final int WIN_LOSE_TOTAL_PERIOD = 1;
	public static final int WIN_LOSE_YEAR_PERIOD = 2;
	public static final int WIN_LOSE_HALF_YEAR_PERIOD = 3;

    public static final int ONE_TOUCH_OPPORTUNITIES_SELECT = 8;

    // constants for campaigns
    public static final long ALL_CAMPAIGNS = 0;
    //public static final long ADS_MARKET_LEAD_FAST = 74;
    //public static final long ADS_MARKET_REGISTRATION_FAST = 75;
    //public static final long ADS_MARKET_LEAD_DREAM = 76;
    //public static final long ADS_MARKET_REGISTRATION_DREAM = 77;
  	public static final String CURRENCY_ALL = "currencies.all";
    public static final long CURRENCY_ALL_ID = 0;
//    public static final String CURRENCY_ILS = "currency.ils";
//    public static final long CURRENCY_ILS_ID = 1;
//    public static final String CURRENCY_USD = "currency.usd";
//    public static final long CURRENCY_USD_ID = 2;
    public static final String CURRENCY_USD_IBAN = "DE 94 5123 0800 0000 0501 44";
    public static final String CURRENCY_USD_ACCOUNT_NUMBER = "50144" ;
//    public static final String CURRENCY_EUR = "currency.eur";
//    public static final long CURRENCY_EUR_ID = 3;
    public static final String CURRENCY_EUR_IBAN = "DE 58 5123 0800 0000 0501 13";
    public static final String CURRENCY_EUR_ACCOUNT_NUMBER = "50113" ;
//    public static final String CURRENCY_GBP = "currency.gbp";
    public static final String CURRENCY_GBP_IBAN = "DE67 5123 0800 0000 0501 45";
    public static final String CURRENCY_GBP_ACCOUNT_NUMBER = "50145" ;
//    public static final long CURRENCY_GBP_ID = 4;
//    public static final String CURRENCY_TRY = "currency.try";
    public static final String CURRENCY_TRY_IBAN = "DE 42 5123 0800 0000 0502 07";
    public static final String CURRENCY_TRY_ACCOUNT_NUMBER = "50207" ;
//    public static final long CURRENCY_TRY_ID = 5;
//    public static final long CURRENCY_RUB_ID = 6;
//    public static final long CURRENCY_CNY_ID = 7;
//    public static final long CURRENCY_KR_ID = 8;
    //public static final long DSNR_LEAD_FAST = 79;
    ////public static final long DSNR_REGISTRATION_FAST=80;
    //public static final long DSNR_LEAD_DREAM = 81;
    //public static final long DSNR_REGISTRATION_DREAM=82;

    //public static final long WESELL_LEAD_FAST = 57;
    //public static final long WESELL_REGISTRATION_FAST= 58;
    //public static final long WESELL_LEAD_DREAM = 59;
    //public static final long WESELL_REGISTRATION_DREAM=60;

  	public static final String APPLICATION_SOURCE = "application.source";
    public static final String APPLICATION_SOURCE_WEB = "web";
    public static final String APPLICATION_SOURCE_BANNERS = "banners";
    public static final String APPLICATION_SOURCE_BACKEND = "backend";
    public static final String APPLICATION_SOURCE_CONSOLE = "console";

    public static final String CURRENT_SYSTEM = "system";
    public static final int XOR_ID_FOR_TEST = 999;


    public static final String FREE_INVEST_MIN = "free_invest_min";
    public static final String FREE_INVEST_MAX = "free_invest_max";

    public static final int UTCOFFSET_DEFAULT = -120;
    public static final String UTCOFFSET_DEFAULT_PERIOD1 = "GMT+03:00";
    public static final String UTCOFFSET_DEFAULT_PERIOD2 = "GMT+02:00";
    public static final String LIVE_SYSTEM = "live";
    public static final String TEST_SYSTEM = "test";
    public static final String LOCAL_SYSTEM = "local";

    public static final String DEPOSIT_DOCS_LIMIT1 = "warn_limit";
    public static final String DEPOSIT_DOCS_LIMIT2 = "lockAccount_trigger_limit";

    public static final String ALL_FILTER_KEY = "general.all";
    public static final long ALL_FILTER_ID = 0;
    public static final int ALL_FILTER_ID_INT = 0;
    public static final long ISSUE_ID_DOCUMENT=12;

    public static final long ANYOPTION_CITY_ID = 0;
    public static final String ETRADER_LOCALE = "iw";
    public static final String TURKISH_LOCALE = "tr";
    public static final String SPANISH_LOCALE = "es";
    public static final String ARABIC_LOCALE = "ar";
    public static final String GERMAN_LOCALE = "de";
    //public static final String LOCALE_DEFAULT = "en";
    public static final String XOR_URL="xor.url";
    public static final String XOR_TERMINAL_ID = "xor.terminalId";
    public static final String XOR_USERNAME = "xor.username";
    public static final String XOR_PASSWORD = "xor.password";
    public static final String XOR_SIGN = "xor.sign";
    public static final String XOR_IS_TEST_TERMINAL = "xor.isTestTerminal";

   
    public static final long RATE_PRECISISON = 5;

    public static final long MILISEC_IN_A_MONTH = 2678400000l;
    public static final long MOBILE_DIGITS_NUMBER_ON_ETRADER = 10;

    public static final long OFFSET_GAIN_BETWEEN_CREATED_AND_SETTLED = 1;

    public static final String COUNTRY_EMPTY_VALUE = "0";

    public static final String UTC_OFFSET = "utcOffset";

    public static final int MARKET_GROUP_CURRENCIES = 4;
    public static final int MARKET_GROUP_COMMODITIES = 5;

    public static final long USERS_MARKET_DIS_ACTIVE_ALL = 0;
    public static final long USERS_MARKET_DIS_ACTIVE = 1;
    public static final long USERS_MARKET_DIS_NOT_ACTIVE = 2;

    public static final String ETRADER_DEFAULT_LIMIT="1";
    public static final String AO_DEFAULT_LIMIT="2";

    public static final int DEFAULT_OPPORTUNITY_TYPE=1;
    public static final int AMEX_CARD_TYPE = 5;

    public static String UNIX_ERROR_STREAM = "runError: ";
//    public static String UNIX_GEO_IP_LOOKUP_COMMAND = "geoiplookup ";
//    public static String UNIX_GEO_IP_LOOKUP_RES_COUNTRY = ",";
//    public static String UNIX_GEO_IP_LOOKUP_RES_ERROR_COUNTRY = "--";

    public static final String CAMPAIGN_UNKNOWN = "Unknown";
    public static final String COMBINATION_ID_DEFUALT = "1";  // TODO: change to defualt from db

	// pixels parameters
	public static final String MARKETING_PARAMETER_USERID = "userIdParam";
	public static final String MARKETING_PARAMETER_MOBIE = "mobilePhoneParam";
	public static final String MARKETING_PARAMETER_CURRENCY_CODE = "currencyCodeParam";
	public static final String MARKETING_PARAMETER_TRANS_AMOUNT = "tranAmountParam";
	public static final String MARKETING_PARAMETER_TRANS_ID = "tranIdParam";
	public static final String MARKETING_PARAMETER_TRANS_AMOUNT_USD = "tranAmountUsdParam";

	public static final String NAME_TAG = "<name>";
	public static final String VALUE_TAG = "<value>";

	public static final String NAME_CLOSING_TAG = "</name>";
	public static final String VALUE_CLOSING_TAG = "</value>";

	public static final long MAESTRO_CARD_TYPE = 6;

	// deposit status parameters
	public static final int ALL_DEPOSIT = 0;
	public static final int NO_DEPOSIT = 1;
	public static final int FAILED_DEPOSIT = 2;
	public static final int SUCCESS_DEPOSIT = 3;
	public static final String NO_DEPOSIT_TXT = "No Deposit";
	public static final String FAILED_DEPOSIT_TXT = "Failed";
	public static final String SUCCESS_DEPOSIT_TXT = "Success";

	public static final long EXISTS = 1;
	public static final long NOT_EXISTS = 0;

	// Population Entity
	public static final int POP_ENTRY_DAYS_BETWEEN_REMOVE_BY_ACTIONS_COUNT = 30;

	public static final int POP_ENTRY_TYPE_GENERAL = 1;
	public static final int POP_ENTRY_TYPE_TRACKING = 2;
	public static final int POP_ENTRY_TYPE_CALLBACK = 3;
	public static final int POP_ENTRY_TYPE_REVIEW = 4;
	public static final int POP_ENTRY_TYPE_REMOVE_FROM_SALES = 5;
	public static final int POP_ENTRY_TYPE_COLLECT_ALL = 0;

	public static final int POPULATION_ENTRY_SORT_BY_TIME_CREATED = 1;

	public static final int POPULATION_ENTRIES_LOCKED_BY_WRITER = 1;
	public static final int POPULATION_ENTRIES_ASSIGNED_TO_WRITER = 2;

	public static final int USER_CHANGE_DETAILS_PHONE_NUMBER = 1;
	public static final int USER_CHANGE_DETAILS_SKIN = 2;

	public static String POPULATION_SIGN = " (P)";

	public static String ACCOUNT_CLOSED = "population.entry.account.closed";
	public static String ACCOUNT_OPEN = "population.entry.account.open";

	public static final String INATEC_SHARED_SECRET = "Wcc5wzx4V3I8S8SZ";
	public static final String INATEC_NOTIFICATION_SECRET = "U1Ht6MHwWgN29aMu";
	public static final String INATEC_SHARED_SECRET_LIVE = "ncjTRN432OAZdsFW";
	public static final String INATEC_NOTIFICATION_SECRET_LIVE = "71cDK0G6Aej7yQEC";
    public static final String INATEC_TRANSACTION_ID_SESSION_PARAM = "inatecTransactionId";
    public static final String NETELLER_TRANSACTION_ID_SESSION_PARAM = "netellerTransactionId";
    
	public static int ONE_TOUCH_AO_UNIT_PRICE = 100;
	public static String ONE_TOUCH_MARKETS = "200"; // list of all markets that are not in skin_market_group_markets for one-Touch

	public static final long BONUS_CLASS_TYPE_FIXED = 1;
	public static final long BONUS_CLASS_TYPE_PERCENT = 2;
	public static final long BONUS_CLASS_TYPE_NEXT_INVEST_ON_US = 3;

	// Envoy
    public static final String ENVOY_NOTIFICATIONS_IP_1 = "213.129.74.111";
    public static final String ENVOY_NOTIFICATIONS_IP_2 = "93.91.29.197";

    public static final String ENVOY_NOTIFY_SUCCESS = "SUCCESS";
	public static final String ENVOY_NOTIFY_ERROR = "ERROR";
	public static final String ENVOY_NOTIFY_CANCELLED = "CANCELLED";
	public static final String ENVOY_NOTIFY_OPEN = "OPEN";
	public static final String ENVOY_NOTIFY_FAILURE = "FAILURE";
	public static final String ENVOY_NOTIFY_EXPIRED = "EXPIRED";

	public static final int ENVOY_PAY_IN_NOTIFICATION_ID = 1;
	public static final int ENVOY_PAY_OUT_NOTIFICATION_ID = 2;
	public static final int ENVOY_PAY_OUT_REVERSAL_NOTIFICATION_ID = 3;
	public static final int ENVOY_PAY_IN_REVERSAL_NOTIFICATION_ID = 4;

	public static final String ENVOY_PAY_IN_NOTIFICATION = "PaymentNotification";
	public static final String ENVOY_PAY_OUT_NOTIFICATION = "PaymentOutNotification";
	public static final String ENVOY_PAY_OUT_REVERSAL_NOTIFICATION = "PaymentOutReversalNotification";
	public static final String ENVOY_PAY_IN_REVERSAL_NOTIFICATION = "PaymentInReversalNotification";

	public static final int USER_ID_REFERENCE_DIGITS_NUMBER = 7;
	public static final int USER_ID_REFERENCE_START_POSITION = 5;

	// Loyalty
    //TODO: take this values from TiersManagerBase, add the warning action also to TiersManagerBase and to messages(Backend)..
	public static final int LOYALTY_JOB_ACTION_UPGRADE = 1;
	public static final int LOYALTY_JOB_ACTION_DOWNGRADE = 2;
	public static final int LOYALTY_JOB_ACTION_EXPIRED = 3;
	public static final int LOYALTY_JOB_ACTION_WARNING = 4;
	public static final int LOYALTY_JOB_ACTION_REQUALIFICATION = 5;

	public static final long PIXEL_TYPE_FIRST_DEPOSIT = 1;
	public static final long PIXEL_TYPE_REGISTRATION = 2;
	public static final long PIXEL_TYPE_CONTACTME = 3;

	// Calcalist
	public static final String CALCALIST_GAME = "cGame";
	public static final long CAL_GAME_VIRTUAL_MONEY = 100000;
	public static final int CAL_DUPLICATE_REAL_TO_GAME = 1;
	public static final int CAL_DUPLICATE_GAME_TO_REAL = 2;
	public static final int RANKING_TABLE_ROW_NUM_SHORT = 5;
	public static final int RANKING_TABLE_SHORT_LENGTH = 11;
	public static final int RANKING_TABLE_ROW_NUM_LONG = 15;
	public static final int RANKING_TABLE_LONG_LENGTH = 17;
	public static final int WINNERS_NUM = 3;
	public static final int GAME_LENGTH_WEEKS = 4;

	// Affiliates Data
	public static final String AFFILIATE_PASSWORD_JOTTIX = "gH5a2";
	public static final String AFFILIATE_PASSWORD_JOTTIX_US = "qasw21";
	public static final long AFFILIATE_RECIPIENT_ID_JOTTIX = 65;
	public static final long AFFILIATE_RECIPIENT_ID_JOTTIX_US = 82;

	public static final String AFFILIATE_GROUP_STRING_DATE = "Date";
	public static final String AFFILIATE_GROUP_STRING_DP = "DP";
	public static final String NETREFER_DELIMITER = "_";
	public static final String REFERPARTNER_DELIMITER = "_";

	public static final int AFFILIATE_NO_GROUP = 0;
	public static final int AFFILIATE_GROUP_BY_DATE = 1;
	public static final int AFFILIATE_GROUP_BY_DP = 2;


	// Mail templates
	public static int TEMPLATE_WELCOME_MAIL_ID = 1;
	public static int TEMPLATE_WELCOME_BAIDU_MAIL_ID = 54;
	public static int TEMPLATE_CONFIRM_MAIL_ID = 2;
	public static int TEMPLATE_WELCOME_MAIL_CAL_GAME_ID = 20;
	public static long CALCALIST_COMBINATION_ID = 829;
	public static int TEMPLATE_PASSWORD_REMINDER_MAIL_ID = 3;
	public static long TEMPLATE_CC_RECEIPT_EMAIL = 24;
	public static long TEMPLATE_BANK_TRANSFER_DETAIL=52;
	public static long TEMPLATE_ACTIVATION_MAIL_LOW_SCORE = 68;
	public static long TEMPLATE_ACTIVATION_MAIL_TRESHOLD = 69;
	public static long TEMPLATE_ACTIVATION_MAIL_RESTRICTED = 70;
	public static long TEMPLATE_DOCUMENT_SCENARIO = 71;
	public static long TEMPLATE_DOCUMENT_SCENARIO_TIME_PASSED = 72;

	public static final String ENUM_SMS_INVEST_MIN = "sms_invest_min";
	public static final String ENUM_SMS_INVEST_MIN_CODE_PREFFIX = "sms.invest.currency.";

	public static final String SERVER_URLS_MIOPCIONES = "serverurls6";

	public static final long BONUS_INSTANT_DYNAMIC_AMOUNT = 100;

	public static final int TRAN_ISSUES_INSERT_ONLINE = 1;

    public static final long INVESTMENT_MAX_LIMIT_CALCALIST = 100000;

	//Issue type
	public static int ISSUE_TYPE_REGULAR  = 1;
	public static int ISSUE_TYPE_RISK  = 2;

	public static final int MAILBOX_TYPE_BONUS = 1;

//    public static final String OBJECT_SHARE_CHARTS_UPDATER = "chartsUpdater";
//    public static final String OBJECT_SHARE_LEVELS_HISTORY_CACHE = "levelHistoryCache";
//    public static final String OBJECT_SHARE_CHARTS_HISTORY_CACHE = "chartHistoryCache";

    public static final long XMASS_BONUS_ID = 242;

    public static final long MBLOX_PROVIDER = 4;
    
    public static final long MOBIVATE_PROVIDER = 6;

    public static final long UNICELL_PROVIDER_ET = 1;

    public static final String CONTACT_ME_SUBJECT = "Contact me, ";
    public static final String CONTACT_ME_DYNMAIC_PARAM = "Dynamic Parameter: ";
    public static final String CONTACT_ME_SUBJECT_LANG = "Language: ";
    public static final String CONTACT_ME_LANG_ISRAEL = "Hebrew";

    public static final long BONUS_STATE_GRANTED = 1;
    public static final long BONUS_STATE_ACTIVE = 2;
	public static final long BONUS_STATE_USED = 3;
    public static final long BONUS_STATE_DONE = 4;
    public static final long BONUS_STATE_REFUSED = 5;
	public static final long BONUS_STATE_CANCELED = 7;
	public static final long BONUS_STATE_WITHDRAWN = 8;
	public static final long BONUS_STATE_WAGERING_WAIVED = 9;

    public static final long BONUS_TYPE_CONVERT_POINTS_TO_CASH = 8;

	public static int CC_VISIBLE  = 1;
	public static int CC_NOT_VISIBLE  = 0;

	public static final String MOBILE_VERSIONS_ANDROID_APK = "android apk";

	public static final int BLACK_LIST_BIN = -1;

	//Deposit Email over threshold to traders
    public static final int THRESHOLD_DEPOSIT = 3000;


    public static final int BUSINESSCASE_ID_ET  = 1;
    public static final int BUSINESSCASE_ID_AO  = 2;
	public static final String ADD_CLEARING_ROUTES_COMMENT = "Added by Rerouting Mechanism";

    public static int DAYS_NOT_TO_REROUTE = -7;

	public static final int LOG_BALANCE_DELTA_PAY_CHINA_WITHDRAW = 38;
	public static final String BARO_PAY_SENDER_NAME = "Anyoption";
    public static String UNIX_HOSTNAME_COMMAND = "hostname ";

    public final static int WRITER_MOBILE_ID = 200;
    
    //Rewards plan
    public static final String NO_FEE = "0";
    
    //new systemCheck
    public static final String DB_CHECK = "db_check";
    
    // copyop tool tip 
    public static final int LAST_FIVE_INVESTMENTS = 5;
    public static final int LAST_TEN_INVESTMENTS = 10;
    public static final int COPYOP_FULLY_COPY_STEP_SECOND = 2;
    public static final int COPYOP_FULLY_COPY_STEP_THIRD = 3;
    public static final int COPYOP_FULLY_COPY_STEP_FOURTH = 4;
    public static final int SIXTY_PERCENT_HIT_IN_LAST_FIVE_INVESTMENTS = 3;
    public static final int SIXTY_PERCENT_HIT_IN_LAST_TEN_INVESTMENTS = 6;
    public static final int MIN_SUCCESSFUL_LAST_INVESTMENTS = 2;
    
    // skin id
    public static final long SKIN_ENGLISH_USD = 2;
    public static final long SKIN_ENGLISH_GREY = 7;
    public static final long SKIN_ENGLISH_USD_REG = 16;
    
    public static final int PROFILE_LAST_INVESTMENTS_COUNT_LIMIT = 3;
}