package com.anyoption.util;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.math.MathContext;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Locale;
import java.util.MissingResourceException;
import java.util.ResourceBundle;
import java.util.TimeZone;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import com.anyoption.beans.User;
import com.anyoption.common.beans.Country;
import com.anyoption.common.beans.LiveGlobeCity;
import com.anyoption.common.beans.Transaction;
import com.anyoption.common.beans.base.Currency;
import com.anyoption.common.beans.base.Skin;
import com.anyoption.common.beans.base.User.MobileNumberValidation;
import com.anyoption.common.enums.CopyOpInvTypeEnum;
import com.anyoption.common.jms.LevelsCache;
import com.anyoption.common.managers.CountriesManagerBase;
import com.anyoption.common.managers.CountryManagerBase;
import com.anyoption.common.managers.CurrenciesManagerBase;
import com.anyoption.common.managers.CurrencyRatesManagerBase;
import com.anyoption.common.managers.LanguagesManagerBase;
import com.anyoption.common.sms.SMSException;
import com.anyoption.helper.LiveHelper;
import com.anyoption.managers.GeneralManager;
import com.anyoption.managers.SMSManagerBase;
import com.anyoption.managers.SkinsManagerBase;
import com.anyoption.managers.UsersManagerBase;
import com.anyoption.sms.SMS;

public class CommonUtil extends com.anyoption.common.util.CommonUtil {
    private static final Logger log = Logger.getLogger(CommonUtil.class);

    public static String CONFIGURATION_FILE = "server";
    protected static String ENUMERATOR_REFRESH_PERIOD = "config.reloadperiod";
    public static String UNIX_CHECKMX_COMMAND = "checkmx ";
    protected static long enumeratorRefreshPeriod = -1;
    protected static long lastRefresh = 0;
	private static String messagesMultiFilePath = "MessageResources";

    protected static ResourceBundle config = null;
    //protected static ArrayList<Enumerator> enumerators = null; //TODO: remove it after 2 deploys

    /**
     * Take a long value from the server.properties.
     *
     * @param key
     * @return The requested server.properties parameter as long or 0 if not found.
     */
    public static long getConfigLong(String key) {
        return getConfigLong(key, 0);
    }

    /**
     * Take a long value from the server.properties.
     *
     * @param key
     * @return The requested server.properties parameter as long or 0 if not found.
     */
    public static long getConfigLong(String key, long defVal) {
        String val = getConfig(key);
        long rez = defVal;
        try {
            rez = Long.parseLong(val);
        } catch (Exception e) {
            log.warn("Can't parse config key: " + key + ". Default: " + defVal);
        }
        return rez;
    }

    /**
     * return true if the enums should be refreshed, false otherwise
     *
     * @return boolean
     *  In case the value isn't in properties, need to take default value.
     *  Used in the method refreshEnums
     */
    //TODO: remove it after 2 deploys
    /*private static boolean shouldRefreshEnums() {
        if (null == enumerators) {
            return true;
        }
        if (enumeratorRefreshPeriod == -1) {
            enumeratorRefreshPeriod = getConfigLong(ENUMERATOR_REFRESH_PERIOD, ConstantsBase.DEFAULT_ENUMS_REFRESH);
        }
        if (System.currentTimeMillis() - lastRefresh > enumeratorRefreshPeriod) {
            return true;
        }
        return false;
    }*/
    
    //TODO: remove it after 2 deploys
    /*private static void refreshEnums() throws SQLException {
        if (shouldRefreshEnums()) {
            Connection con = DBUtil.getDataSource().getConnection();
            try {
                refreshEnums(con);
            } finally {
                con.close();
            }
        }
    }*/
    
    //TODO: remove it after 2 deploys
    /*private static void refreshEnums(Connection conn) throws SQLException {
        long curTime = new Date().getTime();
        if (shouldRefreshEnums()) {
            enumerators = GeneralDAO.getAllEnumerators(conn);
            lastRefresh = curTime;
        }
    }*/

    //TODO: remove it after 2 deploys
    /*public static String getEnum(Connection conn, String enumerator, String code) throws SQLException {
        refreshEnums(conn);
        for (int i = 0; i < enumerators.size(); i++) {
            Enumerator e = enumerators.get(i);
            if (e.getEnumerator().equals(enumerator) && e.getCode().equals(code))
                return e.getValue();
        }
        return null;
    }*/

    //TODO: remove it after 2 deploys
    /*public static String getEnum(String enumerator, String code) throws SQLException {
        refreshEnums();
        for (int i = 0; i < enumerators.size(); i++) {
            Enumerator e = enumerators.get(i);
            if (e.getEnumerator().equals(enumerator) && e.getCode().equals(code))
                return e.getValue();
        }
        return null;
    }*/
    //TODO: remove it after 2 deploys
    /*public static ArrayList<Enumerator> getEnum(String enumerator) throws SQLException {
        ArrayList<Enumerator> l = new ArrayList<Enumerator>();
        refreshEnums();
        for (int i = 0; i < enumerators.size(); i++) {
            Enumerator e = enumerators.get(i);
            if (e.getEnumerator().equals(enumerator))
                l.add(e);
        }
        return l;
    }*/

    public static String getMessage(long languageId, String key, Object[] params) {
        Locale l = new Locale(LanguagesManagerBase.getLanguage(languageId).getCode());
        return getMessage(l, key, params);
    }

    public static String displayAmountForInput(long amount, boolean showCurrency, int currencyId) {
    	if (currencyId <= 0) {
    		// setting the default currency ID
    		log.debug("Invalid currency [" + currencyId
    					+ "] for formatting. Setting the default currency ["
    					+ ConstantsBase.CURRENCY_BASE_ID + "]");
			currencyId = (int) ConstantsBase.CURRENCY_BASE_ID;
		}
		Currency currency = CurrenciesManagerBase.getCurrency(currencyId);
		DecimalFormat sd = new DecimalFormat(
								CurrenciesManagerBase.getAmountFormat(currency, true),
								new DecimalFormatSymbols(Locale.US));
		double amountDecimal = amount;
		amountDecimal /= 100;
		String out = "";
		if (showCurrency) {
			out = currency.getSymbol() + sd.format(amountDecimal);
		} else {
			out = sd.format(amountDecimal);
		}

		return out;
	}

    public static String getCurrencySymbol(long currencyId) {
    	return CurrenciesManagerBase.getCurrency(currencyId).getSymbol();
    }

    /**
     * Format the UTC offset returned by JS Date.getTimezoneOffset() method like
     * GMT+/-HH:MM.
     *
     * @param utcOffset the UTC offset in mins as returned by Date.getTimezoneOffset() JS method
     * @return The passed UTC offset in format GMT+/-HH:MM.
     */
    public static String formatJSUTCOffsetToString(int utcOffset) {
        // The Date.getTimezoneOffset() in JS returns -120 for GMT+2 for example and 240 for GMT+4
        String sign = utcOffset < 0 ? "+" : "-";
        int absUtcOffset = Math.abs(utcOffset);
        String hours = String.valueOf(absUtcOffset / 60);
        if (hours.length() < 2) {
            hours = "0" + hours;
        }
        String mins = String.valueOf(absUtcOffset % 60);
        if (mins.length() < 2) {
            mins = "0" + mins;
        }
        return "GMT" + sign + hours + ":" + mins;
    }

    public static boolean validateHebrewOnly(String value) {
        if (null == value) {
            return true;
        }
        String pattern = getMessage(new Locale("iw"), "general.validate.hebrew.only", null);
        String v = value.replaceAll("[-`'\\(\\)\"\u2018]", " ");
        return !(v.trim().equals("") || !v.matches(pattern));
    }

    public static boolean validateLettersOnly(String value, Locale l) {
    	 if (null == value) {
             return true;
         }
    	 String pattern = CommonUtil.getMessage(l, "general.validate.letters.only", null);
    	 return !(value.trim().equals("") || !value.matches(pattern));
    }

    public static long calcAmount(double d) {
        d = d * 100;
        return new Double(d).longValue();
    }

	public static long calcAmount(String s) throws NumberFormatException {
		BigDecimal bd = new BigDecimal(s);
		return bd.multiply(new BigDecimal(100)).longValue();
	}

    /**
     * Round a BigDecimal to certain order (power of 10).
     *
     * eg:
     * round(new BigDecimal("123.456"), new BigDecimal("100"))   = 100
     * round(new BigDecimal("123.456"), new BigDecimal("10"))    = 120
     * round(new BigDecimal("123.456"), new BigDecimal("1"))     = 123
     * round(new BigDecimal("123.456"), new BigDecimal("0.1"))   = 123.5
     * round(new BigDecimal("123.456"), new BigDecimal("0.01"))  = 123.46
     * round(new BigDecimal("123.456"), new BigDecimal("0.001")) = 123.456
     *
     * @param number
     * @param roundOrder
     * @return
     */
    public static BigDecimal round(BigDecimal number, BigDecimal roundOrder) {
        BigDecimal result = number.divide(roundOrder);
        int rmp = result.precision() - result.scale();
        if (rmp < 0) {
            rmp = 1;
        }
        result = result.round(new MathContext(rmp));
        result = result.multiply(roundOrder);
        return result;
    }

	public static boolean isHebrewSkin(long skinId){
		if ((skinId == Skin.SKIN_ETRADER) || (skinId == Skin.SKIN_TLV)){
			return true;
		}
		return false;
	}

	/**
     * Get contry code by ip with unix command
     * @param ip  for getting country code
     * @return country code

    public static String getCountryCodeByIp(String ip) {

   	 String code = null;
   	 String command = ConstantsBase.UNIX_GEO_IP_LOOKUP_COMMAND + ip;
   	 String unixRes = runUnixCommand(command);

   	 if (IsParameterEmptyOrNull(unixRes) || unixRes.indexOf(ConstantsBase.UNIX_ERROR_STREAM) > -1) {   // error with running command
   		 log.warn("Error with running unix geoiplookup  command, res: " + unixRes);
   		 return null;
   	 } else {  // get country code from string result
   		 	int index = unixRes.indexOf(ConstantsBase.UNIX_GEO_IP_LOOKUP_RES_COUNTRY);  // search for string in the result e.g:
   		 	if (index > -1) {														// GeoIP Country Edition: US, United States
   		 		try {
	    		 	code = unixRes.substring(index-2, index);
	    		 	if (IsParameterEmptyOrNull(code) ||
	    		 			code.equalsIgnoreCase(ConstantsBase.UNIX_GEO_IP_LOOKUP_RES_ERROR_COUNTRY)) {   // error to extract code

	    		 		log.warn("Error to extract countryCode with running unix geoiplookup command, res: " + unixRes);
	    		 		return null;
	    		 	}
   		 		} catch (IndexOutOfBoundsException e) {
   		 			log.warn("Error to extract countryCode with running unix geoiplookup command, res: " + e);
   		 			return null;
				}
   		 	}
   	 }
   	 return code;
    }*/

    /**
     * Run unix command
     * @param command  unix command to run
     * @return
     */
     public static String runUnixCommand(String command) {

	       String s = "";
	       String res = "";

	        try {
	        		log.info("going to run unix command: " + command);
		            Process p = Runtime.getRuntime().exec(command);

		            BufferedReader stdInput = new BufferedReader(new
		                 InputStreamReader(p.getInputStream()));

		            BufferedReader stdError = new BufferedReader(new
		                 InputStreamReader(p.getErrorStream()));

		            // read the output from the command
			         while ((s = stdInput.readLine()) != null) {
			        	 log.info(s);
			             res += s;
			         }

			         // read any errors from the attempted command
			         while ((s = stdError.readLine()) != null) {
			            	res = ConstantsBase.UNIX_ERROR_STREAM + s;
			            	log.info(s);
			            	break;
			         }

	        } catch (IOException e) {
	        		log.info("Exception running unix command: " + e);
		            res = ConstantsBase.UNIX_ERROR_STREAM;
		    }

	        return res;
    }

    /**
     * Get skin id by ip
     * @param ip  for getting skin id
     * @return skinId
     */
	public static Long getSkinIdByIp(String ip){
		String countryCode = CommonUtil.getCountryCodeByIp(ip);
		Long skinId = null;
		if (!CommonUtil.IsParameterEmptyOrNull(countryCode)) { // no need country recognition for skinEsMiop
			log.debug("Get Counrty code from Ip, Country code: " + countryCode);
			try {
				// get Country id from Db
				long countryId = CountriesManagerBase.getCountryIdByCode(countryCode);
				if (countryId == 0) {  // country not in Db, set to default skin
					skinId = new Long(Skin.SKINS_DEFAULT);
					log.debug("Country not exists in the DB, Take default skin. ");
				} else {  // country found
					skinId = CountriesManagerBase.getSkinIdByCountryId(countryId);
					if (skinId == 0) {
						skinId = new Long(Skin.SKINS_DEFAULT);
					}
				}
			} catch (Exception e) {
				skinId = null;
				log.warn("Error in CommonUtil : getSkinIdByIp, " + e);
			}
	    }
		return skinId;
	}

	public static String getIPAddress(HttpServletRequest req) {
		String ipNotFound = "IP NOT FOUND!";
		String ip = null;
		if (req != null) {
			ip = req.getHeader("x-forwarded-for");
			if (null == ip) {
				ip = req.getRemoteAddr();
			}
			log.info("Full ip from header = " + ip);
			int indexOfLastComma = ip.indexOf(',');    // for proxy users(list of ip's) we need only the first ip in the list
			if (indexOfLastComma != -1) {
				ip = ip.substring(0, indexOfLastComma).trim();
				log.info("First ip only = " + ip);
			}
		}
		return ip == null ? ipNotFound : ip ;
	}

	/**
	 * Get date formated by timeZone
	 * Using this function for all filters
	 * @param d
	 * 		Date object for format
	 * @param timeZoneName
	 * 		Time zone string
	 * @return
	 * 		Date instance with timeZone format
	 */
	public static Date getDateTimeFormaByTz(Date d, String timeZoneName) {
		if ( IsParameterEmptyOrNull(timeZoneName) ) {
			return d;
		}
		TimeZone tz = TimeZone.getTimeZone(timeZoneName);
		long newDateMilSec = 0;
		long utcOffset = tz.getRawOffset();
		newDateMilSec = d.getTime() + utcOffset;
		return new Date(newDateMilSec);
	}


	/**
	 * Get message, this is a common code for geting message with locale
	 * instance or without locale instance.
	 * @param key
	 * 		message key
	 * @param params
	 * 		parameters for the message
	 * @param bundle
	 * 		the message file
	 * @return
	 */
	public static String getMessageShared(String key, Object params[], ResourceBundle bundle) {
		if (key == null || key.trim().equals("")) {
			return "";
		}

		String text = null;
		try {
			text = bundle.getString(key);
		} catch (MissingResourceException e) {
			text = "?" + key + "?";
		}
		if (params != null) {
			text = text.replaceAll("'", "@@@@@");
			MessageFormat mf = new MessageFormat(text);
			text = mf.format(params, new StringBuffer(), null).toString();
			text = text.replaceAll("@@@@@", "'");
		}
		return text;
	}

	/**
	 * Get message by locale
	 * @param key
	 * 		message key
	 * @param params
	 * 		parameters for the message
	 * @param locale
	 * 		locale instance
	 * @return
	 */
	public static String getMessage(String key, Object params[], Locale locale) {
		ResourceBundle bundle = ResourceBundle.getBundle(messagesMultiFilePath, locale);
		return getMessageShared(key, params, bundle);
	}

//	public static String formatLevelByMarket(Double d, long marketId, long dpoint) {
//        if (d == null) {
//            return "";
//        }
//        String format = "###,###,##0.";
//        for (int i = 0; i < dpoint; i++) {
//            format += "0";
//        }
//        DecimalFormat f = new DecimalFormat(format);
//        f.setDecimalSeparatorAlwaysShown(true);
//
//        // Cut '.' if needed
//        String res = f.format(d);
//        int lastIndex = res.length() - 1;
//        if (res.endsWith(".")) {
//            res = res.substring(0, lastIndex);
//        }
//        return res;
//    }

	/**
	 * This method adds a day to the date received
	 * @param d - date received
	 * @return  -date received + 1 day
	 */
	public static Date addDay(Date d) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(d);
		cal.add(Calendar.DATE, 1);
		return cal.getTime();
	}

	/**
	 * return Date object of the given date on 0:00:00.000
	 * @param date
	 * @return Date object of the given date on 0:00:00.000
	 */
	public static Date getStartOfDay(Date date) {
		GregorianCalendar calendar = new GregorianCalendar();
		calendar.setTime(date);
		calendar.set(GregorianCalendar.HOUR_OF_DAY, 0);
		calendar.set(GregorianCalendar.MINUTE, 0);
		calendar.set(GregorianCalendar.SECOND, 0);
		calendar.set(GregorianCalendar.MILLISECOND, 0);

		return calendar.getTime();
	}

    public static void alertOverDepositLimitByEmail(Transaction tr, User user) {
        if (tr.getAmount()/100 >= ConstantsBase.THRESHOLD_DEPOSIT && !user.isTestUser()){
            try{
                new SendEmailOverDepositLimit(tr, user).start();
            }catch (Exception e) {
                log.warn("Error, problem sending email over " + ConstantsBase.THRESHOLD_DEPOSIT + " deposit! " + e);
            }
        }
    }

	/**
	 * this method convert an amount from one currency to an amount in base currency
	 * @param amount - the amount to convert from
	 * @param currencyId - the currency to convert from
	 * @param date - the exchange rate date
	 * @return the new amount
	 */
	public static Double convertToBaseAmount(long amount, long currencyId, Date date) {
			return CurrencyRatesManagerBase.convertToBaseAmount(amount, currencyId, date);
	}

	/**
     * Check mail validation with unix command
     * @param mail
     * @return

    public static boolean isEmailValidUnix(String mail) {
   	 if (getConfig("system").equalsIgnoreCase("local")) { //cancel validation from local env.
   		 return true;
   	 }
   	 if (!mail.trim().equals("") && mail.contains("@")) {
   		 String[] mailSuffix = mail.split("@");
   		 if (mailSuffix.length != 2) { //email must have only one '@'
   			 return false;
   		 }
   		 String command = UNIX_CHECKMX_COMMAND + mailSuffix[1];
   		 String unixRes = runUnixCommand(command);

   		 if (IsParameterEmptyOrNull(unixRes)) {   // error with running command
   			 log.warn("Error with running unix checkmx  command, res: " + unixRes);
   			 return false;
   		 } else if (unixRes.equals("1")) { // result from CHECKMX
   			 return true;
   		 }
   	 }
		 return false;
    }*/

    /**
     * Return the countryId by IP, if the country was not found we will take by default of skinId
     * @param skinId
     * @param ip
     * @return
     */
    public static long getCountryIdByIp(long skinId, String ip) {
	    long countryId = 0;
	    String countryCode = CommonUtil.getCountryCodeByIp(ip);
	    if (null != countryCode) {
	        log.debug("Get Counrty code from IP, Country code: " + countryCode);
	        Country c = CountryManagerBase.getCountry(countryCode);
	        if (null != c) {
	            countryId = c.getId();
	        }
	    }
	    if (countryId == 0) {
	    	countryId = SkinsManagerBase.getSkin(skinId).getDefaultCountryId();
	    }
	    return countryId;
    }


    /**
     * Return the currencyLetters by Id
     * @param currencyId
     * @return
     */
    public static String getCurrencyLettersById(long currencyId) throws SQLException {

    	String currencyLetters = null;
		currencyLetters = UsersManagerBase.getCurrencyLettersById(currencyId);
		if (null != currencyLetters) {
		    log.debug("Get Currency Letters By Id, Currency Letters: " + currencyLetters);
		}
		return currencyLetters;
    }

	/**
	 * @param get affiliate key from dynamicParam only if the combination is from Netrefer
	 * @return
	 */
	public static long getAffIdFromDynamicParameter(String dynamicParam, long combId, long skinId) {
		long affKey = 0;
		ArrayList<Long> netreferCombinationsIdList;
		ArrayList<Long> referpartnerCombinationsIdList;
		try {
			netreferCombinationsIdList = SkinsManagerBase.getNetreferCombinationsIdList(skinId);
			if (netreferCombinationsIdList != null){
				if (netreferCombinationsIdList.contains(combId)){
					affKey = Long.parseLong(dynamicParam.split(ConstantsBase.NETREFER_DELIMITER)[0]);
				}
			}

			referpartnerCombinationsIdList = SkinsManagerBase.getReferpartnerCombinationsIdList(skinId);
			if (referpartnerCombinationsIdList != null){
				if (referpartnerCombinationsIdList.contains(combId)){
					affKey = Long.parseLong(dynamicParam.split(ConstantsBase.REFERPARTNER_DELIMITER)[1]);
				}
			}
		} catch (Exception e) {
			log.error("Could not getAffIdFromDynamicParameter, dynamicParam [" + dynamicParam
									+ "], combId [" + combId + "], skinId [" + skinId + "] ", e);
			affKey = 0;
		}
		return affKey;
	}

	/**
	 * put the param in params hashmap only if its not set yet
	 * @param params params hashmap
	 * @param param the param to insert
	 * @param value the value of the param
	 */
	public static void putParam(HashMap<String,String> params, String param, String value) {
		if (IsParameterEmptyOrNull(params.get(param))) {
			params.put(param, value);
		}
	}

	public static void sendNotifyForInvestment(long oppId,
												double investmentId,
												double amount,
												long origAmount,
												long type,
												long marketId,
												int productTypeId,
												double level,
												LevelsCache levelsCache,
												User user,
			    								CopyOpInvTypeEnum cpOpInvType) {
		String cityName;
		long countryId;
		String cityLatitude;
		String cityLongtitude;
		long currencyId;

		if (user.getSkinId() == Skin.SKIN_ETRADER) {
			LiveGlobeCity randCity = LiveHelper.getRandomCityForEtrader();
			log.info("Investment ["
					+ investmentId
					+ "] from opportunity ["
					+ oppId
					+ "] comes from ETrader, replacing city info and amount for display with ["
					+ randCity + "]");
			cityName = randCity.getCityName();
			countryId = randCity.getCountryId();
			cityLatitude = randCity.getGlobeLatitude();
			cityLongtitude = randCity.getGlobeLongtitude();

			origAmount = Math.round((1.0D / CurrencyRatesManagerBase.getUSDRate(randCity.getCurrencyId())) * amount);

			currencyId = randCity.getCurrencyId();
		} else {
			cityName = user.getCityFromGoogle();
			countryId = user.getCountryId();
			cityLatitude = user.getCityLatitude();
			cityLongtitude = user.getCityLongitude();
			currencyId = user.getCurrencyId();
		}

		String amountForDisplay;
		long flooredAmount;
		if (amount > 0d) {
			long flooredOrigAmount = new Double(Math.floor(origAmount / 100d)).longValue() * 100L;
			amountForDisplay = formatCurrencyAmountAO(flooredOrigAmount,
													true,
													CurrenciesManagerBase.getCurrency(currencyId),
													ConstantsBase.DISPLAY_FORMAT_LIVE_PAGE);
			flooredAmount = Math.round(CurrencyRatesManagerBase.getUSDRate(currencyId) * flooredOrigAmount);
		} else {
			amountForDisplay = "-1";
			flooredAmount = -1L;
		}

		levelsCache.notifyForInvestment(oppId,
										investmentId,
										amount,
										type,
										amountForDisplay,
										cityName,
										countryId,
										marketId,
										productTypeId,
										user.getSkinId(),
										level,
										System.currentTimeMillis(),
										cityLatitude,
										cityLongtitude,
										flooredAmount,
										cpOpInvType);
	}

	public static boolean getIsLive() {
		String property = CommonUtil.getConfig(ConstantsBase.CURRENT_SYSTEM, "false");
		return null != property && property.equals("live");
	}

	public static String getRunDbCheck()  {
		return GeneralManager.getRunDbCheck();
	}
	/* Merged code */
	// public static void sendRegistrationSMSByTextLocalProvider(User user, Locale locale) {
	//
	// if (user == null || user.getId() == 0) {
	// log.error("User is null! Registration SMS can not be send!");
	// return;
	// } else if (user.getSkinId() == Skin.SKIN_CHINESE || user.getSkinId() == Skin.SKIN_KOREAN) {
	// log.error("Can not send registration SMS for CHINESE and KOREAN skin!");
	// return;
	// }
	//
	// try {
	// String senderName = "";
	// String senderNumber = "";
	// String phoneCode = "";
	// //String encodedMsg = "";
	// String msg = "";
	// long smsId = 0;
	//
	// // determine SMS message and link app based on user platform id
	// senderNumber = CountryManagerBase.getById(user.getCountryId()).getSupportPhone();
	// phoneCode = CountryManagerBase.getPhoneCodeById(user.getCountryId());
	// Object[] params = new Object[1];
	// if (user.getPlatformId() == ConstantsBase.PLATFORM_ID_ERTADER) {
	// senderName = "etrader";
	// params[0] = getMessage(locale, "sms.register.link.etrader", null);
	// msg = getMessage(locale, "sms.register", params );
	// } else if (user.getPlatformId() == ConstantsBase.PLATFORM_ID_ANYOPTION) {
	// senderName = "anyoption";
	// params[0] = getMessage(locale, "sms.register.link.anyoption", null);
	// msg = getMessage(locale, "sms.register", params );
	// } else if (user.getPlatformId() == ConstantsBase.PLATFORM_ID_COPYOP) {
	// senderName = "copyop";
	// params[0] = getMessage(locale, "sms.register.link.copyop", null);
	// msg = getMessage(locale, "sms.register.copyop", params );
	// } else {
	// senderName = "backend";
	// if (user.getSkinId() == Skin.SKIN_ETRADER) {
	// params[0] = getMessage(locale, "sms.register.link.etrader", null);
	// msg = getMessage(locale, "sms.register", params );
	// } else {
	// params[0] = getMessage(locale, "sms.register.link.anyoption", null);
	// msg = getMessage(locale, "sms.register", params );
	// }
	// }
	//
	// if (msg.equals("")) {
	// log.error("Can NOT get sms message from bundle.");
	// return;
	// }
	// // determine provider by character length
	// /*if ( msg.length() <= ConstantsBase.MAX_CHARACTERS_ALLOWED_BY_MBLOX_PROVIDER ) {
	// smsId = SMSManagerBase.sendTextMessage(senderName, senderNumber, phoneCode + user.getMobilePhone(), msg, user.getId(),
	// SMSManagerBase.SMS_KEY_TYPE_SMS, ConstantsBase.MBLOX_PROVIDER);
	// } else {}*/
	// //encodedMsg = encodeRegisterMessage(msg);
	// /* STOP encoding msg for TextLocal provider MONI's request!!!*/
	// //Switching sms providers from TextLocal to Mobivate and no need for special encoding for now...
	// smsId = SMSManagerBase.sendTextMessage(senderName, senderNumber, phoneCode + user.getMobilePhone(), msg, user.getId(),
	// SMSManagerBase.SMS_KEY_TYPE_USERID, ConstantsBase.MOBIVATE_PROVIDER, SMS.DESCRIPTION_WELCOME, MobileNumberValidation.NOTVALIDATED);
	//
	// // insert SMS issue for backend traking purposes
	// if (smsId == 0) {
	// log.error("SMS id is 0.");
	// return;
	// } else {
	// SMSManagerBase.insertIssueForSMS(user, new Date(), msg, smsId);
	// }
	//
	// } catch (SMSException smse) {
	// log.error("Failed to send SMS.", smse);
	// } catch (SQLException e) {
	// log.error("Failed to send SMS.", e);
	// }
	// }
}