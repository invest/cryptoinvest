package com.anyoption.util;



import java.sql.SQLException;
import java.text.DecimalFormat;
import java.util.Locale;

import com.anyoption.common.beans.Transaction;
import com.anyoption.common.managers.CurrencyRatesManagerBase;

public class TransactionFormater {
	
	public static String getDollarAmount(Transaction t) throws SQLException {
		DecimalFormat sd = new DecimalFormat("###########0.00");
		return sd.format(CurrencyRatesManagerBase.convertToBaseAmount(t.getAmount(), t.getCurrency().getId(), t.getTimeCreated()) / 100);
	}
	
	public static void setTimeCreatedTxt(Transaction t, String utcOffset) {
	    t.setTimeCreatedTxt(CommonUtil.getTimeAndDateFormat(t.getTimeCreated(), utcOffset));
	}
	
	/**
	 * Init formatted values before serializing to JSON
	 * @param l
	 */
	public static void initFormattedValues(Transaction t, Locale l, String utcOffset) {
		t.setTypeNameTxt(l);
		setAmountTxt(t);
		t.getReceiptNumTxt();
		setTimeCreatedTxt(t, utcOffset);
	}
	
	public static void setAmountTxt(Transaction t) {
		t.setAmountWF(CommonUtil.formatCurrencyAmountAO(t.getAmount(), true, t.getCurrencyId()));
	}

}
