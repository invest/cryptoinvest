package com.anyoption.distribution;

import java.util.ArrayList;

public class NewProviderSelector implements Selector<Long> {

	ArrayList<Long> providersWithFailedAttempt;
	ArrayList<Long> allProviders;
	private final int SELECTOR_ID = 1;
	
	public NewProviderSelector(ArrayList<Long> providersWithFailedAttempt, ArrayList<Long> allProviders) {
		this.providersWithFailedAttempt = providersWithFailedAttempt;
		this.allProviders = allProviders;
	}

	/*
	 * Tries to find a provider to which no deposit attempt was made
	 * if no failed attempt so far or all providers are tried returns null
	 */
	@Override
	public Long select() {
		if(providersWithFailedAttempt.size() == 0) {
			return null;
		}
		allProviders.removeAll(providersWithFailedAttempt);
		if(allProviders.size() > 0){
			return allProviders.get(0);
		} else {
			return null;
		}
	}

	@Override
	public int getId() {
		return SELECTOR_ID;
	}

}
