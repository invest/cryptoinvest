package com.anyoption.distribution;

public interface Selector<T extends Object> {
	public int getId();
	public T select();

}
