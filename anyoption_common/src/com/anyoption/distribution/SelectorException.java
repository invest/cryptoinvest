package com.anyoption.distribution;

public class SelectorException extends Exception {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 7447810807020223089L;
	
	
	public SelectorException(String msg) {
		super(msg);
	}

}
