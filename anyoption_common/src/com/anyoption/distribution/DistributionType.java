package com.anyoption.distribution;

public enum DistributionType {
	CUP(1); // China Union Pay related clearing providers
	
	private int value;    

	private DistributionType(int value) {
		this.value = value;
	}

	public int getValue() {
		return value;
	}
}
