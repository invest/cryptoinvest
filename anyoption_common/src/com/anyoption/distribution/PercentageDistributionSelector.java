package com.anyoption.distribution;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Map;
import java.util.Random;

/**
 * 
 * Distributes objects accordingly to preset percentage.
 * 
 */
public class PercentageDistributionSelector<T extends Object> implements Selector<T>{

        private static final String DIFFERENT_LENGTH_OF_ARRAYS = "Different length of arrays";
		private static final String PERCENTAGES_MUST_BE_IN_DESCENDING_ORDER = "Percentages must be in descending order";
		private static final String PERCENTAGES_MUST_ADD_UP_TO_100 = "Percentages must add up to 100%:";
		private final ArrayList<Share> shares;
        private final Random random = new Random();
        private final int SELECTOR_ID = 2;

        /**
         * Create using arrays.
         * 
         * @param values 
         * 			objects you want to distribute
         * @param relativePercentage 
         * 			percentages corresponding to each value in the value map.
         * 			MUST BE IN DESCENDING ORDER (i.e. 50,30,10,5,5)
         * @throws SelectorException 
         */
        public PercentageDistributionSelector(T[] values, Integer[] relativePercentage) throws SelectorException {
                int total = 0;
                int check = 100;
                shares = new ArrayList<Share>();
                
                if(values.length!=relativePercentage.length) {
                	throw new SelectorException(DIFFERENT_LENGTH_OF_ARRAYS);
                }
                
                for (int i = 0; i < values.length; i++) {
                        T valueId = values[i];
                        int percentage = relativePercentage[i];
                        if(percentage > check) {
                        	throw new SelectorException(PERCENTAGES_MUST_BE_IN_DESCENDING_ORDER);
                        }
                        total = total + percentage;
                        shares.add(new Share(total, valueId));
                        check = percentage;
                }
                
                if(total!=100){
                	throw new SelectorException(PERCENTAGES_MUST_ADD_UP_TO_100+total);
                }
        }

        /**
         * Create with a Map. 
         * Key: the object to distribute
         * Value: the relative percentage
         * 
         * @param objectWithPercentageMap
         * @throws SelectorException 
         */
        public PercentageDistributionSelector(Map<T, Integer> objectWithPercentageMap) throws SelectorException {
                int total = 0;
                int check = 100;
                shares = new ArrayList<Share>();
                for (T valueKey : objectWithPercentageMap.keySet()) {
                        Integer percentage = objectWithPercentageMap.get(valueKey);
                        if(percentage > check) {
                        	throw new SelectorException(PERCENTAGES_MUST_BE_IN_DESCENDING_ORDER);
                        }
                        total = total + percentage;
                        shares.add(new Share(total, valueKey));
                        check = percentage;
                }
                
                if(total!=100){
                	throw new SelectorException(PERCENTAGES_MUST_ADD_UP_TO_100 + total);
                }
        }

        public T select() {
                int rndValue = random.nextInt(101);

                for (Share f : shares) {
                        if (rndValue <= f.percentage) {
                                return f.theValue;
                        }
                }
                //should never happen
                return null;
        }

        /**
         * 
         * Holds the percentage-to-value mapping.
         * 
         */
        private class Share {
                int percentage; // share of the whole distribution
                T theValue;   // distributed object

                public Share(int percentage, T value) {
                        this.percentage = percentage;
                        this.theValue = value;
                }
                
                public String toString(){
                        return percentage + " : " + theValue.toString();
                }
        }

        /**
         * For test purposes
         */
        public static final void main(String[] args){
            test();
            System.exit(0);
        }
        
        public static final void test(){
                try{
                        String[] values = new String[]{"Provider A", "Provider B", "Provider C", "Provider D"};
                        Integer[] percentage = new Integer[]{50,30,15,5};
                        Integer[] counter = new Integer[values.length]; 
                        Arrays.fill(counter, 0);
                        
                        
                        int howMany = 100;
                        System.out.println("Generating "+ howMany +" values:");
                        
                        for(int i = 1; i < howMany; i++){
                    		PercentageDistributionSelector<String> g = new PercentageDistributionSelector<String>(values, percentage);
                            String v = g.select();
                            //System.out.println( v );
                            for(int j=0; j<values.length; j++){
                                    if(v.equals(values[j])){
                                            //count it.
                                            counter[j]++;
                                    }
                            }
	                        System.out.println(i+" Users. Distribution is: ");
	                        for(int k = 0;k < values.length; k++){
	                        		int j = Math.round(((float)counter[k]/(float)i)*100);
	                                System.out.print(values[k] + " = " + j + "%");
	                                System.out.print("\t");
	                        }
	                        System.out.println("");
                        }
                        
                        System.out.println("Total numbers: ");
                        for(int k = 0; k < values.length; k++){
                                System.out.print(values[k] + " = " + counter[k]);
                                System.out.print("\t");
                        }
                        
                } catch(Exception e){
                        System.err.println(e.getMessage());
                        e.printStackTrace(System.err);
                        
                }
        }

		@Override
		public int getId() {
			return SELECTOR_ID;
		}
}
