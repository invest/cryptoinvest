//package com.anyoption.beans;
//
//import java.io.Serializable;
//
///**
// * SkinLanguage vo class
// * 
// * @author Kobi
// */
//public class SkinLanguages implements Serializable {
//	private long id;
//	private long skinId;
//	private long languageId;
//	private String displayName;
//
//	public SkinLanguages() {
//		displayName = "";
//	}
//
//	/**
//	 * @return the languageId
//	 */
//	public long getLanguageId() {
//		return languageId;
//	}
//
//	/**
//	 * @param languageId the languageId to set
//	 */
//	public void setLanguageId(long languageId) {
//		this.languageId = languageId;
//	}
//
//	/**
//	 * @return the displayName
//	 */
//	public String getDisplayName() {
//		return displayName;
//	}
//
//	/**
//	 * @param displayName the displayName to set
//	 */
//	public void setDisplayName(String displayName) {
//		this.displayName = displayName;
//	}
//
//	/**
//	 * @return the id
//	 */
//	public long getId() {
//		return id;
//	}
//
//	/**
//	 * @param id the id to set
//	 */
//	public void setId(long id) {
//		this.id = id;
//	}
//
//	/**
//	 * @return the skinId
//	 */
//	public long getSkinId() {
//		return skinId;
//	}
//
//	/**
//	 * @param skinId the skinId to set
//	 */
//	public void setSkinId(long skinId) {
//		this.skinId = skinId;
//	}
//
//	public String toString() {
//	    String ls = System.getProperty("line.separator");
//	    return ls + "SkinLanguage" + ls
//	        + super.toString() + ls
//	        + "skinId: " + skinId + ls
//	        + "languageId: " + languageId + ls
//	        + "displayName: " + displayName + ls;
//	}
//}