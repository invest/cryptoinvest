//package com.anyoption.beans;
//
//import java.sql.SQLException;
//import java.util.Date;
//
//import org.apache.log4j.Logger;
//
//import com.anyoption.common.annotations.AnyoptionNoJSON;
//import com.anyoption.managers.CreditCardTypesManagerBase;
//import com.anyoption.util.CommonUtil;
//
//public class CreditCard extends com.anyoption.beans.base.CreditCard {
//
//    private static final Logger log = Logger.getLogger(CreditCard.class);
//
//    @AnyoptionNoJSON
//	private Date timeModified;
//    @AnyoptionNoJSON
//    private String holderIdNum;
//    @AnyoptionNoJSON
//    private boolean visible;
//    @AnyoptionNoJSON
//    private boolean allowed;
//    @AnyoptionNoJSON
//    protected Date timeCreated;
//    @AnyoptionNoJSON
//    protected String utcOffsetCreated;
//    @AnyoptionNoJSON
//    protected String utcOffsetModified;
//    @AnyoptionNoJSON
//    private boolean isDocumentsSent;
//    @AnyoptionNoJSON
//    private boolean selected;
//    @AnyoptionNoJSON
//    private boolean toCopy;
//    @AnyoptionNoJSON
//	protected boolean creditEnabled; // true in case the cc allow credit(bookBack)
//    @AnyoptionNoJSON
//    private String recurringTransaction; //empty_string if the card haven't used in TBI yet
//    @AnyoptionNoJSON
//    private long bin;
//
//    
//    public long getBin() {
//		return bin;
//	}
//
//	public void setBin(long bin) {
//		this.bin = bin;
//	}
//
//	/**
//     * 
//     */
//    public CreditCard() {
//    }
//
//    /**
//     * @param cc - Credit Card
//     */
//    public CreditCard(com.anyoption.beans.base.CreditCard cc) {
//    	this.id = cc.getId();
//    	this.userId = cc.getUserId();
//    	this.typeId = cc.getTypeId();
//    	this.ccNumber = cc.getCcNumber();
//    	this.ccPass = cc.getCcPass();
//    	this.expMonth = cc.getExpMonth();
//    	this.expYear = cc.getExpYear();
//    	this.holderName = cc.getHolderName();
//    	this.holderIdNum = cc.getHolderId(); 
//    }
//
//	/** 
//	 * @return selected
//	 */
//	public boolean isSelected() {
//		return selected;
//	}
//
//	/**
//	 * @param selected
//	 */
//	public void setSelected(boolean selected) {
//		this.selected = selected;
//	}
//
////	public CreditCardType getType() {
////        if (null == type && typeId > 0) {
////            try {
////                type = CreditCardTypesManagerBase.getById(typeId);
////            } catch (SQLException sqle) {
////                log.error("Can't load card type.", sqle);
////            }
////        }
////		return type;
////	}
//
//	/* (non-Javadoc)
//	 * @see com.anyoption.beans.base.CreditCard#setType(com.anyoption.beans.base.CreditCardType)
//	 */
//	public void setType(CreditCardType type) {
//		this.type = type;
//	}
//
//	/**
//	 * @return
//	 */
//	public String getCcNumberLast4() {
//		String ccnum = String.valueOf(ccNumber);
//		return ccnum.substring(ccnum.length() - 4);
//	}
//
//    /**
//     * Init formatted values before serializing to JSON
//     */
//	public void initFormattedValues(long currencyId) {
//		ccNumberXXXX = getCcNumberXXX();
//		creditAmountTxt = CommonUtil.formatCurrencyAmount(creditAmount, currencyId);
//	}
//
//	/**
//	 * @return
//	 */
//	public String getCcNumberXXX() {
//		String ccnum = String.valueOf(ccNumber);
//		String out = "";
//		for (int i = 0;i < ccnum.length() - 4; i++) {
//			out += "X";
//        }
//		return out + ccnum.substring(ccnum.length() - 4);
//	}
//
//	/**
//	 * @return
//	 */
//	public String getHolderIdNum() {
//		return holderIdNum;
//	}
//
//	/**
//	 * @param holderIdNum
//	 */
//	public void setHolderIdNum(String holderIdNum) {
//		this.holderIdNum = holderIdNum;
//	}
//
//	/**
//	 * @return
//	 */
//	public Date getTimeModified() {
//		return timeModified;
//	}
//
//	/**
//	 * @param timeModified
//	 */
//	public void setTimeModified(Date timeModified) {
//		this.timeModified = timeModified;
//	}
//
//	/**
//	 * @return
//	 */
//	public boolean isAllowed() {
//        return allowed;
//    }
//
//    /**
//     * @param allowed
//     */
//    public void setAllowed(boolean allowed) {
//        this.allowed = allowed;
//        setIsAllowed(allowed == true ? 1 : 0);
//    }
//
//    /**
//     * @return
//     */
//    public boolean isVisible() {
//        return visible;
//    }
//
//    /**
//     * @param visible
//     */
//    public void setVisible(boolean visible) {
//        this.visible = visible;
//    }
//
//	/**
//	 * @return
//	 */
//	public boolean getIsToCopyBoolean() {
//		return toCopy;
//	}
//
//	/**
//	 * @return the utcOffsetCreated
//	 */
//	public String getUtcOffsetCreated() {
//		return utcOffsetCreated;
//	}
//
//	/**
//	 * @param utcOffsetCreated the utcOffsetCreated to set
//	 */
//	public void setUtcOffsetCreated(String utcOffsetCreated) {
//		this.utcOffsetCreated = utcOffsetCreated;
//	}
//
//	/**
//	 * @return the utcOffsetModified
//	 */
//	public String getUtcOffsetModified() {
//		return utcOffsetModified;
//	}
//
//	/**
//	 * @param utcOffsetModified the utcOffsetModified to set
//	 */
//	public void setUtcOffsetModified(String utcOffsetModified) {
//		this.utcOffsetModified = utcOffsetModified;
//	}
//
//	/**
//	 * @return the creditEnabled
//	 */
//	public boolean isCreditEnabled() {
//		return creditEnabled;
//	}
//
//	/**
//	 * @param creditEnabled the creditEnabled to set
//	 */
//	public void setCreditEnabled(boolean creditEnabled) {
//		this.creditEnabled = creditEnabled;
//	}
//
//	/**
//	 * @return
//	 */
//	public boolean isDocumentsSent() {
//		return isDocumentsSent;
//	}
//
//	/**
//	 * @param isDocumentsSent
//	 */
//	public void setDocumentsSent(boolean isDocumentsSent) {
//		this.isDocumentsSent = isDocumentsSent;
//	}
//
//    /**
//     * @return
//     */
//    public String toStringForComments() {
//        return "CVV = " + this.ccPass + " | " +
//             "expMonth = " + this.expMonth + " | " +
//             "expYear = " + this.expYear + " | " +
//             "holderIdNum = " + this.holderIdNum;
//    }
//
//	/**
//	 * @return the timeCreated
//	 */
//	public Date getTimeCreated() {
//		return timeCreated;
//	}
//
//	/**
//	 * @param timeCreated the timeCreated to set
//	 */
//	public void setTimeCreated(Date timeCreated) {
//		this.timeCreated = timeCreated;
//	}
//	
//	/**
//	 * @return
//	 */
//	public String getBinStr(){
//		return String.valueOf(ccNumber).subSequence(0, 6).toString();
//	}
//	
//	public String getRecurringTransaction() {
//		return recurringTransaction;
//	}
//
//	public void setRecurringTransaction(String recurringTransaction) {
//		this.recurringTransaction = recurringTransaction;
//	}
//
//	/* (non-Javadoc)
//	 * @see com.anyoption.beans.base.CreditCard#toString()
//	 */
//	public String toString() {
//        String ls = System.getProperty("line.separator");
//        return ls + "CreditCard" + ls
//            + super.toString() + ls
//            + "id: " + id + ls
//            + "userId: " + userId + ls
//            + "typeId: " + typeId + ls
//            + "ccNumber: " + ccNumber + ls
//            + "ccPass: " + ccPass + ls
//            + "timeCreated: " + timeCreated + ls
//            + "timeModified: " + timeModified + ls
//            + "expMonth: " + expMonth + ls
//            + "expYear: " + expYear + ls
//            + "holderName: " + holderName + ls
//            + "holderIdNum: " + holderIdNum + ls
//            + "visible: " + visible + ls
//            + "allowed: " + allowed + ls
//            + "type: " + type + ls
//            + "selected: " + selected + ls;
//    }
//}