//package com.anyoption.beans;
//
//import java.text.DecimalFormat;
//import java.util.Locale;
//
//import com.anyoption.util.CommonUtil;
//import com.anyoption.util.ConstantsBase;
//
//
///**
// * BonusUsers vo class.
// * 
// * @author Kobi
// */
//public class BonusUsers extends com.anyoption.beans.base.BonusUsers implements java.io.Serializable {
//	private static final long serialVersionUID = 5289496849546211085L;
//	
//	public void initFormattedValues(Locale locale, String utcOffset) {
//		setEndDateTxt(utcOffset);
//		setStartDateTxt(utcOffset);
//		setBonusDescriptionTxt(locale);
//		setBonusStateTxt(locale);
//		setGeneralTermsCaption(locale);
//	}
//	
//	/**
//	 * @return the bonusStateTxt
//	 */
//	public void setBonusStateTxt(Locale locale) {
//		long returnState = 0L;
//		if (bonusStateId == ConstantsBase.BONUS_STATE_DONE || bonusStateId == ConstantsBase.BONUS_STATE_WITHDRAWN || bonusStateId == ConstantsBase.BONUS_STATE_WAGERING_WAIVED) {			
//			returnState = ConstantsBase.BONUS_STATE_USED;
//		} else {
//			returnState = bonusStateId;
//		}
//		bonusStateTxt = CommonUtil.getMessage("bonus.state" + returnState, null, locale); 
//	}
//	
//	/**
//	 * @return the bonusStateDescriptionTxt
//	 */
//	public void setBonusDescriptionTxt(Locale locale) {
//		String[] params = new String[7];
//		params[0] = String.valueOf(CommonUtil.formatCurrencyAmount(bonusAmount, currency.getId()));
//		params[1] = String.valueOf(this.investmentId);
//		String format = "##0";
//		DecimalFormat fmt = new DecimalFormat(format);
//		params[2] = String.valueOf(fmt.format(this.bonusPercent * 100));
//		params[3] = String.valueOf(CommonUtil.formatCurrencyAmount(sumInvQualify, currency.getId()));
//		params[4] = String.valueOf(this.numOfActions);
//		params[5] = String.valueOf(this.numOfActions + 1);
//		params[6] = String.valueOf(CommonUtil.formatCurrencyAmount(minDepositAmount, currency.getId()));
//		bonusDescriptionTxt = CommonUtil.getMessage(bonusStateDescription, params, locale);
//	}
//	
//	/**
//	 * @return the endDateTxt
//	 */
//	public void setEndDateTxt(String utcOffset) {
//		endDateTxt = CommonUtil.getDateAndTimeFormatByDots(endDate, utcOffset);
//	}
//
//	/**
//	 * @return the startDateTxt
//	 */
//	public void setStartDateTxt(String utcOffset) {
//		startDateTxt = CommonUtil.getDateAndTimeFormatByDots(startDate, utcOffset);
//	}
//	
//	/**
//	 * @return the generalTermsCaption
//	 */
//	public void setGeneralTermsCaption(Locale locale) {
//		generalTermsCaption = CommonUtil.getMessage("CMS.termsBonus" + typeId + ".text.t1", null, locale);
//	}
//}