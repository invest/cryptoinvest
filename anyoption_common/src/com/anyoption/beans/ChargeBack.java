//package com.anyoption.beans;
//
//import java.util.Date;
//
//public class ChargeBack implements java.io.Serializable {
//
//	private static final long serialVersionUID = -4041761250949083075L;
//	
//	private long id;
//	private long writerId;
//	private Date timeCreated;
//	private long statusId;
//	private String description;
//	private String statusName;
//	private String utcOffsetCreated;
//
//	private String writer;
//	private long amount;
//	private long transactionId;
//	private String navigation;
//
//	private long userId;
//	private String userName;
//	private String userFirstName;
//	private String userLastName;
//
//	/**
//	 * @return description
//	 */
//	public String getDescription() {
//		return description;
//	}
//    
//	/**
//	 * @param description
//	 */
//	public void setDescription(String description) {
//		this.description = description;
//	}
//    
//	/**
//	 * @return
//	 */
//	public long getId() {
//		return id;
//	}
//    
//	/**
//	 * @param id
//	 */
//	public void setId(long id) {
//		this.id = id;
//	}
//    
//	/**
//	 * @return
//	 */
//	public long getStatusId() {
//		return statusId;
//	}
//    
//	/**
//	 * @param statusId
//	 */
//	public void setStatusId(long statusId) {
//		this.statusId = statusId;
//	}
//    
//	/**
//	 * @return
//	 */
//	public Date getTimeCreated() {
//		return timeCreated;
//	}
//    
//	/**
//	 * @param timeCreated
//	 */
//	public void setTimeCreated(Date timeCreated) {
//		this.timeCreated = timeCreated;
//	}
//    
//	/**
//	 * @return
//	 */
//	public String getWriter() {
//		return writer;
//	}
//    
//	/**
//	 * @param writer
//	 */
//	public void setWriter(String writer) {
//		this.writer = writer;
//	}
//    
//	/**
//	 * @return
//	 */
//	public long getWriterId() {
//		return writerId;
//	}
//    
//	/**
//	 * @param writerId
//	 */
//	public void setWriterId(long writerId) {
//		this.writerId = writerId;
//	}
//    
//	/**
//	 * @return
//	 */
//	public long getAmount() {
//		return amount;
//	}
//    
//	/**
//	 * @param amount
//	 */
//	public void setAmount(long amount) {
//		this.amount = amount;
//	}
//    
//	/**
//	 * @return
//	 */
//	public long getTransactionId() {
//		return transactionId;
//	}
//    
//	/**
//	 * @param transactionId
//	 */
//	public void setTransactionId(long transactionId) {
//		this.transactionId = transactionId;
//	}
//
//	/**
//	 * @return
//	 */
//	public long getUserId() {
//		return userId;
//	}
//    
//	/**
//	 * @param userId
//	 */
//	public void setUserId(long userId) {
//		this.userId = userId;
//	}
//    
//	/**
//	 * @return
//	 */
//	public String getNavigation() {
//		return navigation;
//	}
//    
//	/**
//	 * @param navigation
//	 */
//	public void setNavigation(String navigation) {
//		this.navigation = navigation;
//	}
//    
//	/**
//	 * @return
//	 */
//	public String doNavigation() {
//		return navigation;
//	}
//    
//	/**
//	 * @return
//	 */
//	public String getUserFirstName() {
//		return userFirstName;
//	}
//    
//	/**
//	 * @param userFirstName
//	 */
//	public void setUserFirstName(String userFirstName) {
//		this.userFirstName = userFirstName;
//	}
//    
//	/**
//	 * @return
//	 */
//	public String getUserLastName() {
//		return userLastName;
//	}
//    
//	/**
//	 * @param userLastName
//	 */
//	public void setUserLastName(String userLastName) {
//		this.userLastName = userLastName;
//	}
//    
//	/**
//	 * @return
//	 */
//	public String getUserName() {
//		return userName;
//	}
//    
//	/**
//	 * @param userName
//	 */
//	public void setUserName(String userName) {
//		this.userName = userName;
//	}
//
//	/**
//	 * @return the statusName
//	 */
//	public String getStatusName() {
//		return statusName;
//	}
//
//	/**
//	 * @param statusName the statusName to set
//	 */
//	public void setStatusName(String statusName) {
//		this.statusName = statusName;
//	}
//
//	/**
//	 * @return the utcOffsetCreated
//	 */
//	public String getUtcOffsetCreated() {
//		return utcOffsetCreated;
//	}
//
//	/**
//	 * @param utcOffsetCreated the utcOffsetCreated to set
//	 */
//	public void setUtcOffsetCreated(String utcOffsetCreated) {
//		this.utcOffsetCreated = utcOffsetCreated;
//	}
//    
//    /* (non-Javadoc)
//     * @see java.lang.Object#toString()
//     */
//    public String toString() {
//        String ls = System.getProperty("line.separator");
//        return ls + "ChargeBack" + ls
//            + super.toString() + ls
//            + "id: " + id + ls
//            + "writerId: " + writerId + ls
//            + "timeCreated: " + timeCreated + ls
//            + "statusId: " + statusId + ls
//            + "description: " + description + ls
//            + "utcOffsetCreated: " + utcOffsetCreated + ls
//            + "writer: " + writer + ls
//            + "amount: " + amount + ls
//            + "transactionId: " + transactionId + ls
//            + "navigation: " + navigation + ls
//            + "userId: " + userId + ls
//            + "userName: " + userName + ls
//            + "userFirstName: " + userFirstName + ls
//            + "userLastName: " + userLastName + ls;
//    }
//}