/**
 *
 */
package com.anyoption.beans;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

import com.anyoption.common.beans.base.AssetIndexBase;
import com.anyoption.common.managers.LanguagesManagerBase;
import com.anyoption.managers.SkinsManagerBase;
import com.anyoption.util.CommonUtil;



/**
 * @author AviadH
 *
 */
public class AssetIndex extends AssetIndexBase implements Serializable{

	//market
	private String timeFirstInvest;
	private String timeEstClosing;

	public AssetIndex(AssetIndexBase assetBase){
		super();
		this.id = assetBase.getId();
		this.tradingDays = assetBase.getTradingDays();
		this.fridayTime = assetBase.getFridayTime();
		this.startTime = assetBase.getStartTime();
		this.endTime = assetBase.getEndTime();
		this.isFullDay = assetBase.isFullDay();
		this.feedName = assetBase.getFeedName();
		this.marketGroupId = assetBase.getMarketGroupId();
		this.timeZoneString = assetBase.getTimeZoneString();
		this.optionPlusMarketId = assetBase.getOptionPlusMarketId();
		this.reutersField = assetBase.getReutersField();
	    this.expiryFormula = assetBase.getExpiryFormula();
	    this.description = assetBase.getDescription();
	}

	/**
	 * @return the timeFirstInvest
	 */
	public String getTimeFirstInvest() {
		return timeFirstInvest;
	}
	/**
	 * @param timeFirstInvest the timeFirstInvest to set
	 */
	public void setTimeFirstInvest(String timeFirstInvest) {
		this.timeFirstInvest = timeFirstInvest;
	}
	/**
	 * @return the timeEstClosing
	 */
	public String getTimeEstClosing() {
		return timeEstClosing;
	}
	/**
	 * @param timeEstClosing the timeEstClosing to set
	 */
	public void setTimeEstClosing(String timeEstClosing) {
		this.timeEstClosing = timeEstClosing;
	}

	public void getTradingTimeString(String userOffset, long skinId, long marketId, String assetIndexExpFormulaKey){

        ArrayList<String> daysOfWeekMsgs = new ArrayList<String>(7);
        daysOfWeekMsgs.add(0,"assetIndex.sunday");
        daysOfWeekMsgs.add(1,"assetIndex.monday");
        daysOfWeekMsgs.add(2,"assetIndex.tuesday");
        daysOfWeekMsgs.add(3,"assetIndex.wednesday");
        daysOfWeekMsgs.add(4,"assetIndex.thursday");
        daysOfWeekMsgs.add(5,"assetIndex.friday");
        daysOfWeekMsgs.add(6,"assetIndex.saturday");

        Locale locale = new Locale(LanguagesManagerBase.getLanguage(SkinsManagerBase.getSkin(skinId).getDefaultLanguageId()).getCode());

        TimeZone tzMarket = TimeZone.getTimeZone(timeZoneString);
        String tradingDays = this.getTradingDays();
        int firstDay = tradingDays.indexOf("1");
        int lastDay = tradingDays.lastIndexOf("1");

        String[] time = this.getStartTime().split(":");
        int hour = Integer.parseInt(time[0]) + (tzMarket.inDaylightTime(new Date()) ? -1 : 0);
        int min = Integer.parseInt(time[1]);
        Calendar c = Calendar.getInstance();
        c.set(Calendar.DAY_OF_WEEK, firstDay + 1);
        c.set(Calendar.HOUR_OF_DAY, hour);
        c.set(Calendar.MINUTE, min);
        c.set(Calendar.SECOND, 00);
        firstDay = CommonUtil.getDateTimeFormaByTz(c.getTime(), userOffset).getDay();
        timeFirstInvest = CommonUtil.getTimeFormat(c.getTime(), userOffset);

        time = this.getEndTime().split(":");
        hour = Integer.parseInt(time[0]) + (tzMarket.inDaylightTime(new Date()) ? -1 : 0);
        min = Integer.parseInt(time[1]);
        c.set(Calendar.DAY_OF_WEEK, lastDay + 1);
        c.set(Calendar.HOUR_OF_DAY, hour);
        c.set(Calendar.MINUTE, min);
        lastDay = CommonUtil.getDateTimeFormaByTz(c.getTime(), userOffset).getDay() ;
        timeEstClosing = CommonUtil.getTimeFormat(c.getTime(), userOffset);

        if (this.isFullDay()) {
        	tradingDaysFormat = CommonUtil.getMessage(locale, daysOfWeekMsgs.get(firstDay),null) + " " + timeFirstInvest + " - " + CommonUtil.getMessage(locale, daysOfWeekMsgs.get(lastDay), null) + " " + timeEstClosing;
        } else {
        	tradingDaysFormat = CommonUtil.getMessage(locale, daysOfWeekMsgs.get(firstDay),null) + " - " + CommonUtil.getMessage(locale, daysOfWeekMsgs.get(lastDay),null) + " " + timeFirstInvest + " - " + timeEstClosing;
        }
    	try{
    		if (null != this.getFridayTime()) {
                time = this.getFridayTime().split(":");
                hour = Integer.parseInt(time[0]) + (tzMarket.inDaylightTime(new Date()) ? -1 : 0);
                min = Integer.parseInt(time[1]);
                c.set(Calendar.HOUR_OF_DAY, hour);
                c.set(Calendar.MINUTE, min);
    			//setSuspendedMessage(CommonUtil.getTimeFormat(c.getTime(), userOffset)); // storing friday time in unused suspendedMessage
                tradingDaysFormat += " \n" + CommonUtil.getMessage(locale, "assetIndex.friday", null) +  " " + timeFirstInvest + " - " + CommonUtil.getTimeFormat(c.getTime(), userOffset);
    		}

    	} catch(Exception e) {
    		//EXCEPTION
    	}
        if (optionPlusMarketId != 0) {
            feedName = feedName.substring(0, feedName.length() - 8);
        }

//        if (marketGroupId == 2){
//        	expiryFormula = CommonUtil.getMessage(locale, "assetIndex.expiryLevelReuters", null);
//        	reutersField = feedName + " " + CommonUtil.getMessage(locale, "assetIndex.lastValue", null);
//        } else if (marketGroupId == 4){
//        	if((skinId != Skin.SKIN_ETRADER || skinId != Skin.SKIN_EN_US || skinId != Skin.SKIN_ARABIC) && marketId != 637 ){
//        		expiryFormula = CommonUtil.getMessage(locale, "assetIndex.expiryLevel.currencyPairs", null);
//        	} else {
//        		expiryFormula = CommonUtil.getMessage(locale, "assetIndex.expiryLevelLastBidBy2", null);
//        	}
//        	if (feedName.indexOf("=") > -1) {
//        		reutersField = feedName.substring(0, feedName.indexOf("=")) + "H=";
//        	} else {
//        		reutersField = feedName + "H=";
//        	}
//        } else if (marketGroupId == 5){
//        	expiryFormula = CommonUtil.getMessage(locale, "assetIndex.expiryLevelLastBidBy3", null);
//        	reutersField = feedName + "v1";
//        } else if (marketGroupId == 3 && skinId == Skin.SKIN_ETRADER){        	
//        	expiryFormula = CommonUtil.getMessage(locale, "assetIndex.expiryLevelDaily", null);
//        	reutersField = feedName;
//        } else if ((marketGroupId == 3 && skinId != Skin.SKIN_ETRADER) || (marketGroupId == 6 && skinId == Skin.SKIN_ETRADER)){
//        	if (exchangeId == 2){
//        		expiryFormula = CommonUtil.getMessage(locale, "assetIndex.expiryLevelLastBidBy3", null);
//        	} else {
//        		expiryFormula = CommonUtil.getMessage(locale, "assetIndex.expiryLevelLastBidBy3.not.usa", null);
//        	}
//        	reutersField = feedName;
//        }
//        
//        if(assetIndexExpFormulaKey != null){
//        	expiryFormula = CommonUtil.getMessage(locale, assetIndexExpFormulaKey, null);
//        }
	}

}
