//package com.anyoption.beans;
//
//import java.sql.SQLException;
//import java.text.DecimalFormat;
//import java.util.Locale;
//
//import com.anyoption.managers.CurrencyRatesManagerBase;
//import com.anyoption.util.CommonUtil;
//
///**
// *  WARNING : put in this class only formatting functions for json
// *  New variables and methods that do not require CommonUtil 
// *  and ApplicationDataBase should go to the merged Transaction  
// * 
// */
//public class Transaction extends com.anyoption.common.beans.Transaction {
//	
//	private static final long serialVersionUID = 2174601019818421447L;
//
//	public String getDollarAmount() throws SQLException {
//		DecimalFormat sd = new DecimalFormat("###########0.00");
//		return sd.format(CurrencyRatesManagerBase.convertToBaseAmount(amount, currency.getId(), timeCreated) / 100);
//	}
//    
//    @Override
//    public void setTimeCreatedTxt(String utcOffset) {
//        timeCreatedTxt = CommonUtil.getTimeAndDateFormat(timeCreated, utcOffset);
//    }
//    
//    /**
//     * Init formatted values before serializing to JSON
//     * @param l
//     */
//	public void initFormattedValues(Locale l, String utcOffset) {
//		setTypeNameTxt(l);
//		setAmountTxt();
//		getReceiptNumTxt();
//		setTimeCreatedTxt(utcOffset);
//	}
//	
//    public void setAmountTxt() {
//        amountWF = CommonUtil.formatCurrencyAmount(amount, true, currencyId);
//    }
//}