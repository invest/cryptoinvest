package com.anyoption.beans;

public class Contact extends com.anyoption.beans.base.Contact {

	private static final long serialVersionUID = 1L;

	public String toString() {
        String ls = System.getProperty("line.separator");
        return ls + "Contact" + ls
            + super.toString() + ls
            + "id: " + id + ls
            + "text: " + text + ls
            + "userId: " + userId + ls
            + "phone: " + phone + ls
            + "type: " + type + ls
            + "countryId: " + countryId + ls
            + "email: " + email + ls
            + "skinId: " + skinId + ls
            + "writerId: " + writerId + ls;
    }
}