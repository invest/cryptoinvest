//package com.anyoption.beans;
//
//import java.sql.SQLException;
//import java.util.Hashtable;
//import java.util.Iterator;
//
//
//import org.apache.log4j.Logger;
//
//import com.anyoption.managers.CountriesManagerBase;
//
///**
// * Country bean wrapper
// *
// * @author KobiM
// */
//public class Country extends com.anyoption.beans.base.Country {
//
//	private static final long serialVersionUID = 1L;
//	private static final Logger log = Logger.getLogger(Country.class);
//	//private static Hashtable<Long, Country> countries;
//	/**
//	 * 
//	 */
//	private boolean isSmsAvailable;
//	/**
//	 * 
//	 */
//	private boolean isHaveUkashSite;
//
////	/**
////	 * @param id
////	 * @return
////	 */
////	public static Country getCountry(long id) {
////		getCountries();
////	    return countries.get(id);
////	}
//
////	/**
////	 * @param a2
////	 * @return
////	 */
////	public static Country getCountry(String a2) {
////	    getCountries();
////        // TODO: if this is used a lot we should do a Map for faster lookup
////	    Country c = null;
////	    for (Iterator<Country> i = countries.values().iterator(); i.hasNext();) {
////	        c = i.next();
////	        if (c.getA2().equals(a2)) {
////	            return c;
////	        }
////	    }
////	    return null;
////	}
//	
////	/**
////	 * @return
////	 */
////	public static Hashtable<Long, Country> getCountries() {
////		if (null == countries) {
////	        try {
////	            countries = CountriesManagerBase.getAll();
////	        } catch (SQLException sqle) {
////	            log.error("Can't load countries.", sqle);
////	        }
////	    }
////		return countries;
////	}
//
//	/* (non-Javadoc)
//	 * @see com.anyoption.beans.base.Country#toString()
//	 */
//	public String toString() {
//	    String ls = System.getProperty("line.separator");
//	    return ls + "Country" + ls
//	        + super.toString() + ls
//	        + "id: " + id + ls
//	        + "name: " + name + ls
//	        + "a2: " + a2 + ls
//	        + "a3: " + a3 + ls
//	        + "phoneCode: " + phoneCode + ls
//	        + "gmtOffset: " + gmtOffset + ls;
//	}
//
//	/**
//	 * @return isSmsAvailable
//	 */
//	public boolean isSmsAvailable() {
//		return isSmsAvailable;
//	}
//
//	/**
//	 * @param isSmsAvailable
//	 */
//	public void setSmsAvailable(boolean isSmsAvailable) {
//		this.isSmsAvailable = isSmsAvailable;
//	}
//
//	/**
//	 * @return isHaveUkashSite
//	 */
//	public boolean isHaveUkashSite() {
//		return isHaveUkashSite;
//	}
//
//	/**
//	 * @param isHaveUkashSite
//	 */
//	public void setHaveUkashSite(boolean isHaveUkashSite) {
//		this.isHaveUkashSite = isHaveUkashSite;
//	}
//}