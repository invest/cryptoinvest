package com.anyoption.beans;

import java.io.Serializable;
import java.util.Date;

/**
 * Marketing combination class
 *
 * @author Kobi.
 */
public class MarketingCombination implements Serializable {
    protected long id;
    protected long skinId;
    protected long campaignId;
    protected long mediumId;
    protected long contentId;
    protected long sizeId;
    protected long typeId;
    protected long locationId;
    protected long landingPageId;
    protected long writerId;
    protected Date timeCreated;
    protected long pixelId;       // for adding a new pixel

    protected String skinName;
    protected String campaignName;
    protected String sourceName;
    protected String mediumName;
    protected String contentName;
    protected String typeName;
    protected String location;
    protected String landingPageName;
    protected String landingPageUrl;
    protected String verticalSize;
    protected String horizontalSizel;

    protected MarketingCombinationPixels combPixels;  // combination pixels
    protected long paymentType;

    protected String url; 	// final url

	/**
	 * @return the campaignId
	 */
	public long getCampaignId() {
		return campaignId;
	}

	/**
	 * @param campaignId the campaignId to set
	 */
	public void setCampaignId(long campaignId) {
		this.campaignId = campaignId;
	}

	/**
	 * @return the contentId
	 */
	public long getContentId() {
		return contentId;
	}

	/**
	 * @param contentId the contentId to set
	 */
	public void setContentId(long contentId) {
		this.contentId = contentId;
	}

	/**
	 * @return the id
	 */
	public long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(long id) {
		this.id = id;
	}

	/**
	 * @return the landingPageId
	 */
	public long getLandingPageId() {
		return landingPageId;
	}

	/**
	 * @param landingPageId the landingPageId to set
	 */
	public void setLandingPageId(long landingPageId) {
		this.landingPageId = landingPageId;
	}

	/**
	 * @return the locationId
	 */
	public long getLocationId() {
		return locationId;
	}

	/**
	 * @param locationId the locationId to set
	 */
	public void setLocationId(long locationId) {
		this.locationId = locationId;
	}

	/**
	 * @return the mediumId
	 */
	public long getMediumId() {
		return mediumId;
	}

	/**
	 * @param mediumId the mediumId to set
	 */
	public void setMediumId(long mediumId) {
		this.mediumId = mediumId;
	}

	/**
	 * @return the sizeId
	 */
	public long getSizeId() {
		return sizeId;
	}

	/**
	 * @param sizeId the sizeId to set
	 */
	public void setSizeId(long sizeId) {
		this.sizeId = sizeId;
	}

	/**
	 * @return the skinId
	 */
	public long getSkinId() {
		return skinId;
	}

	/**
	 * @param skinId the skinId to set
	 */
	public void setSkinId(long skinId) {
		this.skinId = skinId;
	}

	/**
	 * @return the timeCreated
	 */
	public Date getTimeCreated() {
		return timeCreated;
	}

	/**
	 * @param timeCreated the timeCreated to set
	 */
	public void setTimeCreated(Date timeCreated) {
		this.timeCreated = timeCreated;
	}

	/**
	 * @return the typeId
	 */
	public long getTypeId() {
		return typeId;
	}

	/**
	 * @param typeId the typeId to set
	 */
	public void setTypeId(long typeId) {
		this.typeId = typeId;
	}

	/**
	 * @return the writerId
	 */
	public long getWriterId() {
		return writerId;
	}

	/**
	 * @param writerId the writerId to set
	 */
	public void setWriterId(long writerId) {
		this.writerId = writerId;
	}


	/**
	 * @return the campaignName
	 */
	public String getCampaignName() {
		return campaignName;
	}

	/**
	 * @param campaignName the campaignName to set
	 */
	public void setCampaignName(String campaignName) {
		this.campaignName = campaignName;
	}

	/**
	 * @return the contentName
	 */
	public String getContentName() {
		return contentName;
	}

	/**
	 * @param contentName the contentName to set
	 */
	public void setContentName(String contentName) {
		this.contentName = contentName;
	}

	/**
	 * @return the landingPageName
	 */
	public String getLandingPageName() {
		return landingPageName;
	}

	/**
	 * @param landingPageName the landingPageName to set
	 */
	public void setLandingPageName(String landingPageName) {
		this.landingPageName = landingPageName;
	}

	/**
	 * @return the landingPageUrl
	 */
	public String getLandingPageUrl() {
		return landingPageUrl;
	}

	/**
	 * @param landingPageUrl the landingPageUrl to set
	 */
	public void setLandingPageUrl(String landingPageUrl) {
		this.landingPageUrl = landingPageUrl;
	}

	/**
	 * @return the location
	 */
	public String getLocation() {
		return location;
	}

	/**
	 * @param location the location to set
	 */
	public void setLocation(String location) {
		this.location = location;
	}

	/**
	 * @return the mediumName
	 */
	public String getMediumName() {
		return mediumName;
	}

	/**
	 * @param mediumName the mediumName to set
	 */
	public void setMediumName(String mediumName) {
		this.mediumName = mediumName;
	}

	/**
	 * @return the sourceName
	 */
	public String getSourceName() {
		return sourceName;
	}

	/**
	 * @param sourceName the sourceName to set
	 */
	public void setSourceName(String sourceName) {
		this.sourceName = sourceName;
	}

	/**
	 * @return the typeName
	 */
	public String getTypeName() {
		return typeName;
	}

	/**
	 * @param typeName the typeName to set
	 */
	public void setTypeName(String typeName) {
		this.typeName = typeName;
	}

	/**
	 * @return the skinName
	 */
	public String getSkinName() {
		return skinName;
	}

	/**
	 * @param skinName the skinName to set
	 */
	public void setSkinName(String skinName) {
		this.skinName = skinName;
	}

	/**
	 * @return the horizontalSizel
	 */
	public String getHorizontalSizel() {
		return horizontalSizel;
	}

	/**
	 * @param horizontalSizel the horizontalSizel to set
	 */
	public void setHorizontalSizel(String horizontalSizel) {
		this.horizontalSizel = horizontalSizel;
	}

	/**
	 * @return the verticalSize
	 */
	public String getVerticalSize() {
		return verticalSize;
	}

	/**
	 * @param verticalSize the verticalSize to set
	 */
	public void setVerticalSize(String verticalSize) {
		this.verticalSize = verticalSize;
	}

	/**
	 * @return the url for the combination
	 */
	public String getUrl() {
		this.url = this.landingPageUrl +
				   "?combid=" + this.id +
				   "&utm_campaign=" + this.campaignId +
				   "&utm_source=" + this.sourceName +
				   "&utm_medium=" + this.mediumName +
				   "&utm_content=" + this.contentName;

		if (sizeId > 0 || typeId > 0 || locationId > 0) {
			url += "&utm_term=";

			if (sizeId > 0) {
				url += this.verticalSize + "X" + this.horizontalSizel;
			}

			if (typeId > 0) {
				if (sizeId > 0) {
					url += "|";
				}
				url += typeName;
			}

			if (locationId > 0) {
				if (sizeId > 0 || typeId > 0) {
					url += "|";
				}
				url += location;
			}
		}
		url += "&s=" + skinId;
		return url;

	}

	/**
	 * @return the paymentType
	 */
	public long getPaymentType() {
		return paymentType;
	}

	/**
	 * @param paymentType the paymentType to set
	 */
	public void setPaymentType(long paymentType) {
		this.paymentType = paymentType;
	}


	/**
	 * @return the pixelId
	 */
	public long getPixelId() {
		return pixelId;
	}

	/**
	 * @param pixelId the pixelId to set
	 */
	public void setPixelId(long pixelId) {
		this.pixelId = pixelId;
	}


	/**
	 * @param url the url to set
	 */
	public void setUrl(String url) {
		this.url = url;
	}


	/**
	 * @return the combPixels
	 */
	public MarketingCombinationPixels getCombPixels() {
		return combPixels;
	}

	/**
	 * @param combPixels the combPixels to set
	 */
	public void setCombPixels(MarketingCombinationPixels combPixels) {
		this.combPixels = combPixels;
	}

	/**
	 * toString implementation.
	 */
	public String toString() {
        String ls = System.getProperty("line.separator");
        return ls + "MarketingCombination:" + ls +
            "id: " + id + ls +
            "skinId: " + skinId + ls +
            "campaignId: " + campaignId + ls +
            "mediumId: " + mediumId + ls +
            "contentId: " + contentId + ls +
            "sizeId: " + sizeId + ls +
            "typeId: " + typeId + ls +
            "locationId: " + locationId + ls +
            "landingPageId: " + landingPageId + ls +
	        "writerId: " + writerId + ls +
	        "timeCreated: " + timeCreated + ls;
    }
}