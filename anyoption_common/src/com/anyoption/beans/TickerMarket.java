package com.anyoption.beans;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import com.anyoption.util.CommonUtil;

public class TickerMarket implements Serializable {
    private long marketId;
    private String marketName;
    private boolean hasOpened;
    private BigDecimal lastClosingLevel;
    private Date lastClosingLevelTime;
    private BigDecimal lastLevel;
    private Date lastLevelTime;
    private int decimal;
    
    public long getMarketId() {
        return marketId;
    }
    
    public void setMarketId(long marketId) {
        this.marketId = marketId;
    }
    
    public boolean isHasOpened() {
        return hasOpened;
    }
    
    public void setHasOpened(boolean hasOpened) {
        this.hasOpened = hasOpened;
    }
    
    public BigDecimal getLastClosingLevel() {
        return lastClosingLevel;
    }
    
    public void setLastClosingLevel(BigDecimal lastClosingLevel) {
        this.lastClosingLevel = lastClosingLevel;
    }
    
    public Date getLastClosingLevelTime() {
        return lastClosingLevelTime;
    }
    
    public void setLastClosingLevelTime(Date lastClosingLevelTime) {
        this.lastClosingLevelTime = lastClosingLevelTime;
    }
    
    public BigDecimal getLastLevel() {
        return lastLevel;
    }
    
    public void setLastLevel(BigDecimal lastLevel) {
        this.lastLevel = lastLevel;
    }
    
    public Date getLastLevelTime() {
        return lastLevelTime;
    }
    
    public void setLastLevelTime(Date lastLevelTime) {
        this.lastLevelTime = lastLevelTime;
    }
    
    public int getDecimal() {
        return decimal;
    }
    
    public void setDecimal(int decimal) {
        this.decimal = decimal;
    }
    
    public String getMarketName() {
        return marketName;
    }

    public void setMarketName(String marketName) {
        this.marketName = marketName;
    }

    public Calendar getTime() {
        Calendar c = Calendar.getInstance(TimeZone.getTimeZone("GMT"));
        if (null != lastLevel) {
            c.setTime(lastLevelTime);
        } else {
            c.setTime(lastClosingLevelTime);
        }
        return c;
    }
    
    public String getLevel() {
        if (null != lastLevel) {
            return CommonUtil.formatLevel(lastLevel.doubleValue(), decimal - 1);
        }
        return CommonUtil.formatLevel(lastClosingLevel.doubleValue(), decimal - 1);
    }
    
    public String getColor() {
        if (null == lastLevel) {
            return "closed";
        }
        if (lastLevel.compareTo(lastClosingLevel) > 0) {
            return "green";
        }
        return "red";
    }
}