//package com.anyoption.beans;
//
///**
// * Bonus population limit
// *
// * @author Kobi
// *
// */
//public class BonusPopulationLimit {
//	private long id;
//	private long bonusId;
//	private long popTypeId;
//	private long currencyId;
//	private long minDepositParam;
//	private long maxDepositParam;
//	private long minResult;
//	private long multiplication;
//	private long level;
//	private long stepRangeMul;
//	private long stepLevelMul;
//	private double stepBonusAddition;
//	private double minBonusPercent;
//	private int limitUpdateType;
//	private long bonusAmount;
//	private long minInvestmentAmount;
//	private long maxInvestmentAmount;
//
//	/**
//	 * @return the limitUpdateType
//	 */
//	public int getLimitUpdateType() {
//		return limitUpdateType;
//	}
//
//	/**
//	 * @param limitUpdateType the limitUpdateType to set
//	 */
//	public void setLimitUpdateType(int limitUpdateType) {
//		this.limitUpdateType = limitUpdateType;
//	}
//
//	/**
//	 * @return bonusId
//	 */
//	public long getBonusId() {
//		return bonusId;
//	}
//
//	/**
//	 * @param bonusId
//	 */
//	public void setBonusId(long bonusId) {
//		this.bonusId = bonusId;
//	}
//
//	/**
//	 * @return currencyId
//	 */
//	public long getCurrencyId() {
//		return currencyId;
//	}
//
//	/**
//	 * @param currencyId
//	 */
//	public void setCurrencyId(long currencyId) {
//		this.currencyId = currencyId;
//	}
//
//	/**
//	 * @return
//	 */
//	public long getId() {
//		return id;
//	}
//
//	/**
//	 * @param id
//	 */
//	public void setId(long id) {
//		this.id = id;
//	}
//
//	/**
//	 * @return max Deposit Parameter
//	 */
//	public long getMaxDepositParam() {
//		return maxDepositParam;
//	}
//
//	/**
//	 * @param maxDepositParam
//	 */
//	public void setMaxDepositParam(long maxDepositParam) {
//		this.maxDepositParam = maxDepositParam;
//	}
//
//	/**
//	 * @return min deposit Parameter
//	 */
//	public long getMinDepositParam() {
//		return minDepositParam;
//	}
//
//	/**
//	 * @param minDepositParam
//	 */
//	public void setMinDepositParam(long minDepositParam) {
//		this.minDepositParam = minDepositParam;
//	}
//
//	/**
//	 * @return
//	 */
//	public long getMinResult() {
//		return minResult;
//	}
//
//	/**
//	 * @param minResult
//	 */
//	public void setMinResult(long minResult) {
//		this.minResult = minResult;
//	}
//
//	/**
//	 * @return multiplication
//	 */
//	public long getMultiplication() {
//		return multiplication;
//	}
//
//	/**
//	 * @param multiplication
//	 */
//	public void setMultiplication(long multiplication) {
//		this.multiplication = multiplication;
//	}
//
//	/**
//	 * @return popTypeId
//	 */
//	public long getPopTypeId() {
//		return popTypeId;
//	}
//
//	/**
//	 * @param popTypeId
//	 */
//	public void setPopTypeId(long popTypeId) {
//		this.popTypeId = popTypeId;
//	}
//
//	/**
//	 * @return level
//	 */
//	public long getLevel() {
//		return level;
//	}
//
//	/**
//	 * @param level
//	 */
//	public void setLevel(long level) {
//		this.level = level;
//	}
//
//	/**
//	 * @return stepLevelMul
//	 */
//	public long getStepLevelMul() {
//		return stepLevelMul;
//	}
//
//	/**
//	 * @param steplevelMul
//	 */
//	public void setStepLevelMul(long steplevelMul) {
//		this.stepLevelMul = steplevelMul;
//	}
//
//	/**
//	 * @return stepRangeMul
//	 */
//	public long getStepRangeMul() {
//		return stepRangeMul;
//	}
//
//	/**
//	 * @param stepRangeMul
//	 */
//	public void setStepRangeMul(long stepRangeMul) {
//		this.stepRangeMul = stepRangeMul;
//	}
//
//	/**
//	 * @return mi nBonus Percent
//	 */
//	public double getMinBonusPercent() {
//		return minBonusPercent;
//	}
//
//	/**
//	 * @param minBonusPercent
//	 */
//	public void setMinBonusPercent(double minBonusPercent) {
//		this.minBonusPercent = minBonusPercent;
//	}
//
//	/**
//	 * @return step Bonus Addition
//	 */
//	public double getStepBonusAddition() {
//		return stepBonusAddition;
//	}
//
//	/**
//	 * @param stepBonusAddition
//	 */
//	public void setStepBonusAddition(double stepBonusAddition) {
//		this.stepBonusAddition = stepBonusAddition;
//	}
//
//	/**
//	 * @return the bonusAmount
//	 */
//	public long getBonusAmount() {
//		return bonusAmount;
//	}
//
//	/**
//	 * @param bonusAmount the bonusAmount to set
//	 */
//	public void setBonusAmount(long bonusAmount) {
//		this.bonusAmount = bonusAmount;
//	}
//
//	/**
//	 * @return the maxInvestmentAmount
//	 */
//	public long getMaxInvestmentAmount() {
//		return maxInvestmentAmount;
//	}
//
//	/**
//	 * @param maxInvestmentAmount the maxInvestmentAmount to set
//	 */
//	public void setMaxInvestmentAmount(long maxInvestmentAmount) {
//		this.maxInvestmentAmount = maxInvestmentAmount;
//	}
//
//	/**
//	 * @return the minInvestmentAmount
//	 */
//	public long getMinInvestmentAmount() {
//		return minInvestmentAmount;
//	}
//
//	/**
//	 * @param minInvestmentAmount the minInvestmentAmount to set
//	 */
//	public void setMinInvestmentAmount(long minInvestmentAmount) {
//		this.minInvestmentAmount = minInvestmentAmount;
//	}
//
//}
