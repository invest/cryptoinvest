//package com.anyoption.beans;
//
//import java.util.Date;
//
//public class OpportunityOddsType implements java.io.Serializable {
//	private long id;
//	private String name;
//	private float overOddsWin;
//	private float overOddsLose;
//	private float underOddsWin;
//	private float underOddsLose;
//	private boolean isDefault;
//	private Date timeCreated;
//    private float oddsWinDeductStep;
//
//	public long getId() {
//		return id;
//	}
//    
//	public void setId(long id) {
//		this.id = id;
//	}
//    
//	public boolean isDefault() {
//		return isDefault;
//	}
//    
//	public void setDefault(boolean isDefault) {
//		this.isDefault = isDefault;
//	}
//    
//	public String getName() {
//		return name;
//	}
//    
//	public void setName(String name) {
//		this.name = name;
//	}
//    
//	public float getOverOddsLose() {
//		return overOddsLose;
//	}
//    
//	public void setOverOddsLose(float overOddsLose) {
//		this.overOddsLose = overOddsLose;
//	}
//    
//	public float getOverOddsWin() {
//		return overOddsWin;
//	}
//    
//	public void setOverOddsWin(float overOddsWin) {
//		this.overOddsWin = overOddsWin;
//	}
//    
//	public Date getTimeCreated() {
//		return timeCreated;
//	}
//    
//	public void setTimeCreated(Date timeCreated) {
//		this.timeCreated = timeCreated;
//	}
//    
//	public float getUnderOddsLose() {
//		return underOddsLose;
//	}
//    
//	public void setUnderOddsLose(float underOddsLose) {
//		this.underOddsLose = underOddsLose;
//	}
//    
//	public float getUnderOddsWin() {
//		return underOddsWin;
//	}
//    
//	public void setUnderOddsWin(float underOddsWin) {
//		this.underOddsWin = underOddsWin;
//	}
//    
//	public float getOddsWinDeductStep() {
//        return oddsWinDeductStep;
//    }
//    
//    public void setOddsWinDeductStep(float oddsWinDeductStep) {
//        this.oddsWinDeductStep = oddsWinDeductStep;
//    }
//
//	public String toString() {
//	    String ls = System.getProperty("line.separator");
//	    return ls + "OpportunityOddsType" + ls
//	        + super.toString() + ls
//	        + "id: " + id + ls
//	        + "name: " + name + ls
//	        + "overOddsWin: " + overOddsWin + ls
//	        + "overOddsLose: " + overOddsLose + ls
//	        + "underOddsWin: " + underOddsWin + ls
//	        + "underOddsLose: " + underOddsLose + ls
//	        + "isDefault: " + isDefault + ls
//	        + "timeCreated: " + timeCreated + ls
//            + "oddsWinDeductStep: " + oddsWinDeductStep + ls;
//	}
//}