//package com.anyoption.beans;
//
//import java.io.Serializable;
//import java.util.Date;
//
///**
// * BonusCurrency vo class.
// *
// * @author Kobi
// */
//public class BonusCurrency implements Serializable {
//	private long id;
//	private long bonusId;
//	private long currencyId;
//	private long bonusAmount;
//	private double bonusPercent;
//	private long minDepositAmount;
//	private long maxDepositAmount;
//	private long sumInvestQualify;
//	private long writerId;
//	private Date timeCreated;
//	private long sumInvestWithdraw;
//	private long step;
//	private long minInvestAmount;
//	private long maxInvestAmount;
//	private long sumDeposits;
//
//	/**
//	 * @return the sumDeposits
//	 */
//	public long getSumDeposits() {
//		return sumDeposits;
//	}
//
//	/**
//	 * @param sumDeposits the sumDeposits to set
//	 */
//	public void setSumDeposits(long sumDeposits) {
//		this.sumDeposits = sumDeposits;
//	}
//
//	/**
//	 * @return the step
//	 */
//	public long getStep() {
//		return step;
//	}
//
//	/**
//	 * @param step the step to set
//	 */
//	public void setStep(long steps) {
//		this.step = steps;
//	}
//
//	/**
//	 * @return the bonusAmount
//	 */
//	public long getBonusAmount() {
//		return bonusAmount;
//	}
//
//	/**
//	 * @param bonusAmount the bonusAmount to set
//	 */
//	public void setBonusAmount(long bonusAmount) {
//		this.bonusAmount = bonusAmount;
//	}
//
//	/**
//	 * @return the bonusId
//	 */
//	public long getBonusId() {
//		return bonusId;
//	}
//
//	/**
//	 * @param bonusId the bonusId to set
//	 */
//	public void setBonusId(long bonusId) {
//		this.bonusId = bonusId;
//	}
//
//	/**
//	 * @return the bonusPercent
//	 */
//	public double getBonusPercent() {
//		return bonusPercent;
//	}
//
//	/**
//	 * @param bonusPercent the bonusPercent to set
//	 */
//	public void setBonusPercent(double bonusPercent) {
//		this.bonusPercent = bonusPercent;
//	}
//
//	/**
//	 * @return the currencyId
//	 */
//	public long getCurrencyId() {
//		return currencyId;
//	}
//
//	/**
//	 * @param currencyId the currencyId to set
//	 */
//	public void setCurrencyId(long currencyId) {
//		this.currencyId = currencyId;
//	}
//
//	/**
//	 * @return the id
//	 */
//	public long getId() {
//		return id;
//	}
//
//	/**
//	 * @param id the id to set
//	 */
//	public void setId(long id) {
//		this.id = id;
//	}
//
//	/**
//	 * @return the maxDepositAmount
//	 */
//	public long getMaxDepositAmount() {
//		return maxDepositAmount;
//	}
//
//	/**
//	 * @param maxDepositAmount the maxDepositAmount to set
//	 */
//	public void setMaxDepositAmount(long maxDepositAmount) {
//		this.maxDepositAmount = maxDepositAmount;
//	}
//
//	/**
//	 * @return the minDepositAmount
//	 */
//	public long getMinDepositAmount() {
//		return minDepositAmount;
//	}
//
//	/**
//	 * @param minDepositAmount the minDepositAmount to set
//	 */
//	public void setMinDepositAmount(long minDepositAmount) {
//		this.minDepositAmount = minDepositAmount;
//	}
//
//	/**
//	 * @return the sumInvestQualify
//	 */
//	public long getSumInvestQualify() {
//		return sumInvestQualify;
//	}
//
//	/**
//	 * @param sumInvestQualify the sumInvestQualify to set
//	 */
//	public void setSumInvestQualify(long sumInvestQualify) {
//		this.sumInvestQualify = sumInvestQualify;
//	}
//
//	/**
//	 * @return the sumInvestWithdraw
//	 */
//	public long getSumInvestWithdraw() {
//		return sumInvestWithdraw;
//	}
//
//	/**
//	 * @param sumInvestWithdraw the sumInvestWithdraw to set
//	 */
//	public void setSumInvestWithdraw(long sumInvestWithdraw) {
//		this.sumInvestWithdraw = sumInvestWithdraw;
//	}
//
//	/**
//	 * @return the timeCreated
//	 */
//	public Date getTimeCreated() {
//		return timeCreated;
//	}
//
//	/**
//	 * @param timeCreated the timeCreated to set
//	 */
//	public void setTimeCreated(Date timeCreated) {
//		this.timeCreated = timeCreated;
//	}
//
//	/**
//	 * @return the writerId
//	 */
//	public long getWriterId() {
//		return writerId;
//	}
//
//	/**
//	 * @param writerId the writerId to set
//	 */
//	public void setWriterId(long writerId) {
//		this.writerId = writerId;
//	}
//
//	/**
//	 * @return the maxInvestAmount
//	 */
//	public long getMaxInvestAmount() {
//		return maxInvestAmount;
//	}
//
//	/**
//	 * @param maxInvestAmount the maxInvestAmount to set
//	 */
//	public void setMaxInvestAmount(long maxInvestAmount) {
//		this.maxInvestAmount = maxInvestAmount;
//	}
//
//	/**
//	 * @return the minInvestAmount
//	 */
//	public long getMinInvestAmount() {
//		return minInvestAmount;
//	}
//
//	/**
//	 * @param minInvestAmount the minInvestAmount to set
//	 */
//	public void setMinInvestAmount(long minInvestAmount) {
//		this.minInvestAmount = minInvestAmount;
//	}
//}