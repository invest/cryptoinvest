//package com.anyoption.beans;
//
//import java.util.Hashtable;
//
//import org.apache.log4j.Logger;
//
//import com.anyoption.managers.LanguagesManagerBase;
//
///**
// * Language bean wrapper
// *
// * @author KobiM
// *
// */
//public class Language extends com.anyoption.beans.base.Language {
//    private static final Logger log = Logger.getLogger(Language.class);
//
//    protected static Hashtable<Long, Language> languages;
//
//    public static Language getLanguage(long id) {
//    	getLanguages();
//        return languages.get(id);
//    }
//
//    public static Hashtable<Long, Language> getLanguages() {
//        if (null == languages) {
//            try {
//                languages = LanguagesManagerBase.getAllLanguages();
//            } catch (Exception e) {
//                log.error("Can't load languages.", e);
//            }
//        }
//        return languages;
//    }
//
//}