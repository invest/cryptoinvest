//package com.anyoption.beans;
//
//public class InvestmentType implements java.io.Serializable {
//    public static final long CALL = 1;
//    public static final long PUT = 2;
//    
//	private long id;
//	private String name;
//	private long writerId;
//	private String displayName;
//
//	public String getDisplayName() {
//		return displayName;
//	}
//    
//	public void setDisplayName(String displayName) {
//		this.displayName = displayName;
//	}
//    
//	public long getId() {
//		return id;
//	}
//    
//	public void setId(long id) {
//		this.id = id;
//	}
//    
//	public String getName() {
//		return name;
//	}
//    
//	public void setName(String name) {
//		this.name = name;
//	}
//    
//	public long getWriterId() {
//		return writerId;
//	}
//    
//	public void setWriterId(long writerId) {
//		this.writerId = writerId;
//	}
//    
//	public String toString() {
//	    String ls = System.getProperty("line.separator");
//	    return ls + "InvestmentType" + ls
//	        + super.toString() + ls
//	        + "id: " + id + ls
//	        + "name: " + name + ls
//	        + "writerId: " + writerId + ls
//	        + "displayName: " + displayName + ls;
//	}
//}