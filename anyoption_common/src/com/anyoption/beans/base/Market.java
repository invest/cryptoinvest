//package com.anyoption.beans.base;
//
//public class Market implements java.io.Serializable, Cloneable {
//	protected long id;
//	protected String displayName;
//	protected boolean isOptionPlusMarket;
//
//	/**
//	 * @return the displayName
//	 */
//	public String getDisplayName() {
//		return displayName;
//	}
//
//	/**
//	 * @param displayName the displayName to set
//	 */
//	public void setDisplayName(String displayName) {
//		this.displayName = displayName;
//	}
//
//	/**
//	 * @return the id
//	 */
//	public long getId() {
//		return id;
//	}
//
//	/**
//	 * @param id the id to set
//	 */
//	public void setId(long id) {
//		this.id = id;
//	}
//
//	@Override
//	public Object clone() throws CloneNotSupportedException {
//		return super.clone();
//	}
//
//	public boolean isOptionPlusMarket() {
//		return isOptionPlusMarket;
//	}
//
//	public void setOptionPlusMarket(boolean isOptionPlusMarket) {
//		this.isOptionPlusMarket = isOptionPlusMarket;
//	}
//
//	@Override
//	public String toString() {
//		if (null != displayName) {
//			return displayName + (isOptionPlusMarket ? "+" : "");
//		}
//		return "";
//	}
//}