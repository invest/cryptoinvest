//package com.anyoption.beans.base;
//
//import java.io.Serializable;
//import java.text.Collator;
//import java.util.ArrayList;
//import java.util.Comparator;
//
///**
// * Country vo class
// *
// * @author Kobi
// */
//public class Country implements Serializable {
//
//	private static final long serialVersionUID = 1L;
//	protected long id;
//	protected String name;
//	protected String a2;
//	protected String a3;
//	protected String phoneCode;
//	protected String displayName;
//	protected String supportPhone;
//	protected String supportFax;
//	protected String gmtOffset;
//	private ArrayList<PaymentMethod> payments;  
//
//	/**
//	 * @return the a2
//	 */
//	public String getA2() {
//		return a2;
//	}
//
//	/**
//	 * @param a2 the a2 to set
//	 */
//	public void setA2(String a2) {
//		this.a2 = a2;
//	}
//
//	/**
//	 * @return the a3
//	 */
//	public String getA3() {
//		return a3;
//	}
//
//	/**
//	 * @param a3 the a3 to set
//	 */
//	public void setA3(String a3) {
//		this.a3 = a3;
//	}
//
//	/**
//	 * @return the id
//	 */
//	public long getId() {
//		return id;
//	}
//
//	/**
//	 * @param id the id to set
//	 */
//	public void setId(long id) {
//		this.id = id;
//	}
//
//	/**
//	 * @return the name
//	 */
//	public String getName() {
//		return name;
//	}
//
//	/**
//	 * @param name the name to set
//	 */
//	public void setName(String name) {
//		this.name = name;
//	}
//
//	/**
//	 * @return the phoneCode
//	 */
//	public String getPhoneCode() {
//		return phoneCode;
//	}
//
//	/**
//	 * @param phoneCode the phoneCode to set
//	 */
//	public void setPhoneCode(String phoneCode) {
//		this.phoneCode = phoneCode;
//	}
//
//	/**
//	 * @return the supportFax
//	 */
//	public String getSupportFax() {
//		return supportFax;
//	}
//
//	/**
//	 * @param supportFax the supportFax to set
//	 */
//	public void setSupportFax(String supportFax) {
//		this.supportFax = supportFax;
//	}
//
//	/**
//	 * @return the supportPhone
//	 */
//	public String getSupportPhone() {
//		return supportPhone;
//	}
//
//	/**
//	 * @param supportPhone the supportPhone to set
//	 */
//	public void setSupportPhone(String supportPhone) {
//		this.supportPhone = supportPhone;
//	}
//
//	/**
//	 * @return the displayName
//	 */
//	public String getDisplayName() {
//		return displayName;
//	}
//
//	/**
//	 * @param displayName the displayName to set
//	 */
//	public void setDisplayName(String displayName) {
//		this.displayName = displayName;
//	}
//
//	/**
//	 * @return the gmtOffset
//	 */
//	public String getGmtOffset() {
//		return gmtOffset;
//	}
//
//	/**
//	 * @param gmtOffset the gmtOffset to set
//	 */
//	public void setGmtOffset(String gmtOffset) {
//		this.gmtOffset = gmtOffset;
//	}
//
//	/**
//	 * @return the payments
//	 */
////	public ArrayList<PaymentMethod> getPayments() {
////		return payments;
////	}
////
////	/**
////	 * @param payments the payments to set
////	 */
////	public void setPayments(ArrayList<PaymentMethod> payments) {
////		this.payments = payments;
////	}
//	
//  public static class AlphaComparator implements Comparator<Country> {
//	  @Override
//	  public int compare(Country a, Country b) {
//	      String aLabel = a.getName();
//	      String bLabel = b.getName();
//	      int returnVal = 0;
//	      try {
//	          Collator collator = Collator.getInstance();
//	          returnVal = collator.compare(aLabel, bLabel);
//	      } catch (Exception e) {
//	      }
//			return returnVal;
//	  }
//  }
//	
//    public String toString() {
//    	if (null !=  this.name) {
//    		return this.name;
//    	}
//    	return "";
//    }
//	
//}