//package com.anyoption.beans.base;
//
//import java.io.Serializable;
//
//public class Currency implements Serializable {
//
//	/**
//	 * 
//	 */
//	private static final long serialVersionUID = 1L;
//	
//	protected long id;
//	protected String symbol;
//	protected int isLeftSymbol;
//	protected String code;
//	protected String nameKey;
//	protected String displayName;
//	protected int decimalPointDigits;
//
//	public String getCode() {
//		return code;
//	}
//
//	public void setCode(String code) {
//		this.code = code;
//	}
//
//	public long getId() {
//		return id;
//	}
//
//	public void setId(long id) {
//		this.id = id;
//	}
//
//	public int getIsLeftSymbol() {
//		return isLeftSymbol;
//	}
//
//	public void setIsLeftSymbol(int isLeftSymbol) {
//		this.isLeftSymbol = isLeftSymbol;
//	}
//
//	public boolean getIsLeftSymbolBool() {
//		return ( isLeftSymbol == 1 ? true : false );
//	}
//
//	public String getNameKey() {
//		return nameKey;
//	}
//
//	public void setNameKey(String nameKey) {
//		this.nameKey = nameKey;
//	}
//
//	public String getSymbol() {
//		return symbol;
//	}
//
//	public void setSymbol(String symbol) {
//		this.symbol = symbol;
//	}
//
//    public String getDisplayName() {
//        return displayName;
//    }
//
//    public void setDisplayName(String displayName) {
//        this.displayName = displayName;
//    }
//
//	/**
//	 * @return the decimalPointDigits
//	 */
//	public int getDecimalPointDigits() {
//		return decimalPointDigits;
//	}
//
//	/**
//	 * @param decimalPointDigits the decimalPointDigits to set
//	 */
//	public void setDecimalPointDigits(int decimalPointDigits) {
//		this.decimalPointDigits = decimalPointDigits;
//	}
//}