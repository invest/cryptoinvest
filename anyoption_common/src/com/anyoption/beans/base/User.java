//package com.anyoption.beans.base;
//
//import java.util.Date;
//import java.util.Locale;
//
//import com.anyoption.common.beans.base.Currency;
//import com.anyoption.common.beans.base.Skin;
//import com.google.gson.annotations.Expose;
//
//
//public class User extends com.anyoption.common.beans.base.User {
//    /**
//	 * 
//	 */
//	private static final long serialVersionUID = 1L;
//	
//	@Expose
//    protected long balance;
//    protected long taxBalance;
//    protected String encryptedPassword;
//    protected String hashedPassword;
//    @Expose
//    protected String firstName;
//    @Expose
//    protected String lastName;
//    protected String street;
//    protected long cityId;
//    protected String zipCode;
//    protected Date timeCreated;
//    @Expose
//    protected String email;
//    protected Date timeBirthDate;
//    protected int isContactByEmail;
//    protected long isContactBySMS;
//    protected String mobilePhone;
//    protected String landLinePhone;
//    protected String idNum;
//    protected String streetNo;
//    protected String utcOffset;
//    protected Long state;
//    protected long countryId;
//    protected String cityName = "";
//    protected long combinationId;
//    protected String dynamicParam;
//    protected long contactId;
//    protected long isContactByPhone;
//    protected Date timeFirstVisit;
//    protected boolean isDecline;
//    @Expose
//    protected String balanceWF;
//    protected String taxBalanceWF;
//    protected long currencyId;
//    @Expose
//    protected String currencySymbol;
//    protected boolean currencyLeftSymbol;
//
//    protected String mobilePhonePrefix;
//    protected String landLinePhonePrefix;
//    protected String mobilePhoneSuffix;
//    protected String landLinePhoneSuffix;
//
//    protected Locale locale;
//    protected String deviceUniqueId;
//    protected boolean hasBonus;
//    protected String timeBirthDateTxt;
//    protected String genderTxt;
//    
//    protected String userAgent;
//    protected String httpReferer;
//    protected boolean isAcceptedTerms;
//    
//    protected String cityLongitude; // for globe LIVE AO
//    protected String cityLatitude; // for globe LIVE AO
//    protected String cityFromGoogle; // get city from google maps API
//    
//    protected boolean isStopReverseWithdrawOption;
//        
//    protected Currency currency;
//
//    //for update user details
//    private String birthYearUpdate;
//    private String birthMonthUpdate;
//    private String birthDayUpdate;
//    protected String gender;
//    @Expose
//    protected String loginToken; 
//
//    /**
//	 * @return the genderTxt
//	 */
//	public String getGenderTxt() {
//		return genderTxt;
//	}
//
//	/**
//	 * @return the timeBirthDateTxt
//	 */
//	public String getTimeBirthDateTxt() {
//		return timeBirthDateTxt;
//	}
//
//	public boolean isHasBonus() {
//		return hasBonus;
//	}
//
//	public void setHasBonus(boolean hasBonus) {
//		this.hasBonus = hasBonus;
//	}
//
//    public long getBalance() {
//        return balance;
//    }
//
//    public void setBalance(long balance) {
//        this.balance = balance;
//    }
//
//    public long getCityId() {
//        return cityId;
//    }
//
//    public void setCityId(long cityId) {
//        this.cityId = cityId;
//    }
//
//    public String getCityName() {
//        return cityName;
//    }
//
//    public void setCityName(String cityName) {
//        this.cityName = cityName;
//    }
//
//    public long getCombinationId() {
//        return combinationId;
//    }
//
//    public void setCombinationId(long combinationId) {
//        this.combinationId = combinationId;
//    }
//
//    public long getContactId() {
//        return contactId;
//    }
//
//    public void setContactId(long contactId) {
//        this.contactId = contactId;
//    }
//
//    public long getCountryId() {
//        return countryId;
//    }
//
//    public void setCountryId(long countryId) {
//        this.countryId = countryId;
//    }
//
//    public String getDynamicParam() {
//        return dynamicParam;
//    }
//
//    public void setDynamicParam(String dynamicParam) {
//        this.dynamicParam = dynamicParam;
//    }
//
//    public String getEmail() {
//        return email;
//    }
//
//    public void setEmail(String email) {
//        this.email = email;
//    }
//
//    public String getFirstName() {
//        return firstName;
//    }
//
//    public void setFirstName(String firstName) {
//        this.firstName = firstName;
//    }
//
//    public String getIdNum() {
//        return idNum;
//    }
//
//    public void setIdNum(String idNum) {
//        this.idNum = idNum;
//    }
//
//    public int getIsContactByEmail() {
//        return isContactByEmail;
//    }
//
//    public void setIsContactByEmail(int isContactByEmail) {
//        this.isContactByEmail = isContactByEmail;
//    }
//
//    public long getIsContactByPhone() {
//        return isContactByPhone;
//    }
//
//    public boolean isContactByPhone() {
//        return (isContactByPhone==1);
//    }
//
//    public void setIsContactByPhone(long isContactByPhone) {
//        this.isContactByPhone = isContactByPhone;
//    }
//
//    public boolean isDecline() {
//        return isDecline;
//    }
//
//    public void setDecline(boolean isDecline) {
//        this.isDecline = isDecline;
//    }
//
//    public String getLandLinePhone() {
//        return landLinePhone;
//    }
//
//    public void setLandLinePhone(String landLinePhone) {
//        this.landLinePhone = landLinePhone;
//        if (landLinePhone != null && landLinePhone.length() > 3) {
//            landLinePhonePrefix = landLinePhone.substring(0, 3);
//            landLinePhoneSuffix = landLinePhone.substring(3);
//        }
//    }
//
//    public void setLandLinePhoneET(String landLinePhone) {
//        this.landLinePhone = landLinePhone;
//        if (landLinePhone != null && landLinePhone.length() > 2) {
//        	if (landLinePhone.startsWith("07")){
//        		this.landLinePhonePrefix = landLinePhone.substring(0, 3);
//        		this.landLinePhoneSuffix = landLinePhone.substring(3);
//        	} else {
//        		this.landLinePhonePrefix = landLinePhone.substring(0, 2);
//        		this.landLinePhoneSuffix = landLinePhone.substring(2);
//        	}
//        }
//    }
//    
//    public void setLandLinePhoneCD(String landLinePhone) {
//        this.landLinePhone = landLinePhone;
//    }
//
//    public String getLandLinePhonePrefix() {
//        return landLinePhonePrefix;
//    }
//
//    public void setLandLinePhonePrefix(String landLinePhonePrefix) {
//        this.landLinePhonePrefix = landLinePhonePrefix;
//    }
//
//    public String getLandLinePhoneSuffix() {
//        return landLinePhoneSuffix;
//    }
//
//    public void setLandLinePhoneSuffix(String landLinePhoneSuffix) {
//        this.landLinePhoneSuffix = landLinePhoneSuffix;
//    }
//
//    public String getLastName() {
//        return lastName;
//    }
//
//    public void setLastName(String lastName) {
//        this.lastName = lastName;
//    }
//
//    public String getMobilePhone() {
//        return mobilePhone;
//    }
//
//    public void setMobilePhone(String mobilePhone) {
//        this.mobilePhone = mobilePhone;
//        if (mobilePhone != null && mobilePhone.length() > 3) {
//            mobilePhonePrefix = mobilePhone.substring(0, 3);
//            mobilePhoneSuffix = mobilePhone.substring(3);
//        }
//    }
//
//    public String getMobilePhonePrefix() {
//        return mobilePhonePrefix;
//    }
//
//    public void setMobilePhonePrefix(String mobilePhonePrefix) {
//        this.mobilePhonePrefix = mobilePhonePrefix;
//    }
//
//    public String getMobilePhoneSuffix() {
//        return mobilePhoneSuffix;
//    }
//
//    public void setMobilePhoneSuffix(String mobilePhoneSuffix) {
//        this.mobilePhoneSuffix = mobilePhoneSuffix;
//    }
//
//    public Long getState() {
//        return state;
//    }
//
//    public void setState(Long state) {
//        this.state = state;
//    }
//
//    public String getStreet() {
//    	if (null == street) {
//    		return "";
//    	}
//		return street;
//    }
//
//    public void setStreet(String street) {
//        this.street = street;
//    }
//
//    public String getStreetNo() {
//    	if (null == streetNo) {
//    		return "";
//    	}
//		return streetNo;
//    }
//
//    public void setStreetNo(String streetNo) {
//        this.streetNo = streetNo;
//    }
//
//    public long getTaxBalance() {
//        return taxBalance;
//    }
//
//    public void setTaxBalance(long taxBalance) {
//        this.taxBalance = taxBalance;
//    }
//
//    public Date getTimeBirthDate() {
//        return timeBirthDate;
//    }
//
//    public void setTimeBirthDate(Date timeBirthDate) {
//        this.timeBirthDate = timeBirthDate;
//    }
//
//    public Date getTimeCreated() {
//        return timeCreated;
//    }
//
//    public void setTimeCreated(Date timeCreated) {
//        this.timeCreated = timeCreated;
//    }
//
//    public Date getTimeFirstVisit() {
//        return timeFirstVisit;
//    }
//
//    public void setTimeFirstVisit(Date timeFirstVisit) {
//        this.timeFirstVisit = timeFirstVisit;
//    }
//
//    public String getUtcOffset() {
//        return utcOffset;
//    }
//
//    public void setUtcOffset(String utcOffset) {
//        this.utcOffset = utcOffset;
//    }
//
//    public String getZipCode() {
//        return zipCode;
//    }
//
//    public void setZipCode(String zipCode) {
//        this.zipCode = zipCode;
//    }
//
//	public String getDeviceUniqueId() {
//		return deviceUniqueId;
//	}
//
//	public void setDeviceUniqueId(String deviceUniqueId) {
//		this.deviceUniqueId = deviceUniqueId;
//	}
//
//    public long getIsContactBySMS() {
//        return isContactBySMS;
//    }
//
//    public void setIsContactBySMS(long isContactBySMS) {
//        this.isContactBySMS = isContactBySMS;
//    }
//
//	public boolean isContactBySMS() {
//		return (isContactBySMS==1);
//	}
//
//	/**
//	 * @return the balanceWF
//	 */
//	public String getBalanceWF() {
//		return balanceWF;
//	}
//
//	/**
//	 * @param balanceWF the balanceWF to set
//	 */
//	public void setBalanceWF(String balanceWF) {
//		this.balanceWF = balanceWF;
//	}
//
//	/**
//	 * @return the encryptedPassword
//	 */
//	public String getEncryptedPassword() {
//		return encryptedPassword;
//	}
//
//	/**
//	 * @param encryptedPassword the encryptedPassword to set
//	 */
//	public void setEncryptedPassword(String encryptedPassword) {
//		this.encryptedPassword = encryptedPassword;
//	}
//
//	public String getHashedPassword() {
//        return hashedPassword;
//    }
//
//    public void setHashedPassword(String hashedPassword) {
//        this.hashedPassword = hashedPassword;
//    }
//
//    /**
//	 * @return the currencyId
//	 */
//	public long getCurrencyId() {
//		return currencyId;
//	}
//
//	/**
//	 * @param currencyId the currencyId to set
//	 */
//	public void setCurrencyId(long currencyId) {
//		this.currencyId = currencyId;
//	}
//
//    public String getCurrencySymbol() {
//        return currencySymbol;
//    }
//
//    public void setCurrencySymbol(String currencySymbol) {
//        this.currencySymbol = currencySymbol;
//    }
//
//    public boolean isCurrencyLeftSymbol() {
//        return currencyLeftSymbol;
//    }
//
//    public void setCurrencyLeftSymbol(boolean currencyLeftSymbol) {
//        this.currencyLeftSymbol = currencyLeftSymbol;
//    }
//
//    public Locale getLocale() {
//        return locale;
//    }
//
//    public void setLocale(Locale locale) {
//        this.locale = locale;
//    }
//
//	public String getTaxBalanceWF() {
//		return taxBalanceWF;
//	}
//
//	public void setTaxBalanceWF(String taxBalanceWF) {
//		this.taxBalanceWF = taxBalanceWF;
//	}
//
//	public boolean isAO() {
//		return skinId > Skin.SKIN_ETRADER;
//	}
//	
//	public String getUserAgent() {
//		return userAgent;
//	}
//
//	public void setUserAgent(String userAgent) {
//		this.userAgent = userAgent;
//	}
//
//	public String getHttpReferer() {
//		return httpReferer;
//	}
//
//	public void setHttpReferer(String httpReferer) {
//		this.httpReferer = httpReferer;
//	}
//
//	public boolean isAcceptedTerms() {
//		return isAcceptedTerms;
//	}
//
//	public void setAcceptedTerms(boolean isAcceptedTerms) {
//		this.isAcceptedTerms = isAcceptedTerms;
//	}
//
//	/**
//	 * @return the cityLongitude
//	 */
//	public String getCityLongitude() {
//		return cityLongitude;
//	}
//
//	/**
//	 * @param cityLongitude the cityLongitude to set
//	 */
//	public void setCityLongitude(String cityLongitude) {
//		this.cityLongitude = cityLongitude;
//	}
//
//	/**
//	 * @return the cityLatitude
//	 */
//	public String getCityLatitude() {
//		return cityLatitude;
//	}
//
//	/**
//	 * @param cityLatitude the cityLatitude to set
//	 */
//	public void setCityLatitude(String cityLatitude) {
//		this.cityLatitude = cityLatitude;
//	}
//
//	/**
//	 * @return the cityFromGoogle
//	 */
//	public String getCityFromGoogle() {
//		return cityFromGoogle;
//	}
//
//	/**
//	 * @param cityFromGoogle the cityFromGoogle to set
//	 */
//	public void setCityFromGoogle(String cityFromGoogle) {
//		this.cityFromGoogle = cityFromGoogle;
//	}
//	
//	/**
//	 * @return the isStopReverseWithdrawOption
//	 */
//	public boolean isStopReverseWithdrawOption() {
//		return isStopReverseWithdrawOption;
//	}
//
//	/**
//	 * @param isStopReverseWithdrawOption the isStopReverseWithdrawOption to set
//	 */
//	public void setStopReverseWithdrawOption(boolean isStopReverseWithdrawOption) {
//		this.isStopReverseWithdrawOption = isStopReverseWithdrawOption;
//	}
//
//	public Currency getCurrency() {
//		return currency;
//	}
//
//	public void setCurrency(Currency currency) {
//		this.currency = currency;
//	}
//
//	public boolean getCanChangeEmail(){
//		return !email.equals(userName);
//	}
//
//	/**
//	 * @param gender the gender to set
//	 */
//	public void setGender(String gender) {
//		this.gender = gender;
//	}
//
//	/**
//	 * @return the birthDayUpdate
//	 */
//	public String getBirthDayUpdate() {
//		return birthDayUpdate;
//	}
//
//	/**
//	 * @param birthDayUpdate the birthDayUpdate to set
//	 */
//	public void setBirthDayUpdate(String birthDayUpdate) {
//		this.birthDayUpdate = birthDayUpdate;
//	}
//
//	/**
//	 * @return the birthMonthUpdate
//	 */
//	public String getBirthMonthUpdate() {
//		return birthMonthUpdate;
//	}
//
//	/**
//	 * @param birthMonthUpdate the birthMonthUpdate to set
//	 */
//	public void setBirthMonthUpdate(String birthMonthUpdate) {
//		this.birthMonthUpdate = birthMonthUpdate;
//	}
//
//	/**
//	 * @return the birthYearUpdate
//	 */
//	public String getBirthYearUpdate() {
//		return birthYearUpdate;
//	}
//
//	/**
//	 * @param birthYearUpdate the birthYearUpdate to set
//	 */
//	public void setBirthYearUpdate(String birthYearUpdate) {
//		this.birthYearUpdate = birthYearUpdate;
//	}
//
//	public String getGender() {
//		return gender;
//	}
//
//	/**
//	 * @return the loginToken
//	 */
//	public String getLoginToken() {
//		return loginToken;
//	}
//
//	/**
//	 * @param loginToken the loginToken to set
//	 */
//	public void setLoginToken(String loginToken) {
//		this.loginToken = loginToken;
//	}
//	
//}