//package com.anyoption.beans.base;
//
//import java.io.Serializable;
//
///**
// * 
// * @author liors
// *
// */
//public class Wire implements Serializable {
//    protected long id;
//    protected String bankId;
//    protected String branch;
//    protected String accountNum;
//    protected String accountName;
//    protected String accountInfo;
//    protected String deposit;    // for wire deposit
//
//    protected String beneficiaryName;
//    protected String swift;
//    protected String iban;
//    protected String bankNameTxt;   // just name without bankId related
//
//    protected String branchAddress;
//    protected String amount;
//    protected String bankCode;
//
//    protected String accountType; // from envoy system
//    protected String checkDigits; // from envoy system
//    protected Long bankFeeAmount;
//    
//    protected String branchName;
//
//    /**
//     * @return
//     */
//    public String getAccountName() {
//        return accountName;
//    }
//
//    /**
//     * @param accountName
//     */
//    public void setAccountName(String accountName) {
//        this.accountName = accountName;
//    }
//
//    /**
//     * @return
//     */
//    public String getAccountNum() {
//        return accountNum;
//    }
//
//    /**
//     * @param accountNum
//     */
//    public void setAccountNum(String accountNum) {
//        this.accountNum = accountNum;
//    }
//
//    /**
//     * @return
//     */
//    public String getBankId() {
//        return bankId;
//    }
//
//    /**
//     * @param bankId
//     */
//    public void setBankId(String bankId) {
//        this.bankId = bankId;
//    }
//
//    /**
//     * @return
//     */
//    public String getBranch() {
//        return branch;
//    }
//
//    /**
//     * @param branch
//     */
//    public void setBranch(String branch) {
//        this.branch = branch;
//    }
//
//    /**
//     * @return
//     */
//    public long getId() {
//        return id;
//    }
//
//    /**
//     * @param id
//     */
//    public void setId(long id) {
//        this.id = id;
//    }
//
//    /**
//     * @return
//     */
//    public String getDeposit() {
//        return deposit;
//    }
//
//    /**
//     * @param deposit
//     */
//    public void setDeposit(String deposit) {
//        this.deposit = deposit;
//    }
//
//    /**
//     * @param bankName the bankName to set
//     */
//    public void setBankNameTxt(String bankNameTxt) {
//        this.bankNameTxt = bankNameTxt;
//    }
//
//    /**
//     * @return the bankName
//     */
//    public String getBankNameTxt() {
//        return bankNameTxt;
//    }
//
//    /**
//     * @return the beneficiaryName
//     */
//    public String getBeneficiaryName() {
//        return beneficiaryName;
//    }
//
//    /**
//     * @param beneficiaryName the beneficiaryName to set
//     */
//    public void setBeneficiaryName(String beneficiaryName) {
//        this.beneficiaryName = beneficiaryName;
//    }
//
//    /**
//     * @return the iban
//     */
//    public String getIban() {
//        return iban;
//    }
//
//    /**
//     * @param iban the iban to set
//     */
//    public void setIban(String iban) {
//        this.iban = iban;
//    }
//
//    /**
//     * @return the swift
//     */
//    public String getSwift() {
//        return swift;
//    }
//
//    /**
//     * @param swift the swift to set
//     */
//    public void setSwift(String swift) {
//        this.swift = swift;
//    }
//
//    /**
//     * @return the amount
//     */
//    public String getAmount() {
//        return amount;
//    }
//
//    /**
//     * @param amount the amount to set
//     */
//    public void setAmount(String amount) {
//        this.amount = amount;
//    }
//
//    public String getBranchAddress() {
//        return branchAddress;
//    }
//
//    public void setBranchAddress(String branchAddress) {
//        this.branchAddress = branchAddress;
//    }
//
//    /**
//     * @return the bankCode
//     */
//    public String getBankCode() {
//        return bankCode;
//    }
//
//    /**
//     * @param bankCode the bankCode to set
//     */
//    public void setBankCode(String bankCode) {
//        this.bankCode = bankCode;
//    }
//
//    /**
//     * @return the accountType
//     */
//    public String getAccountType() {
//        return accountType;
//    }
//
//    /**
//     * @param accountType the accountType to set
//     */
//    public void setAccountType(String accountType) {
//        this.accountType = accountType;
//    }
//
//    /**
//     * @return the checkDigits
//     */
//    public String getCheckDigits() {
//        return checkDigits;
//    }
//
//    /**
//     * @param checkDigits the checkDigits to set
//     */
//    public void setCheckDigits(String checkDigits) {
//        this.checkDigits = checkDigits;
//    }
//
//    /**
//     * @return the bankFeeAmount
//     */
//    public Long getBankFeeAmount() {
//        return bankFeeAmount;
//    }
//
//    /**
//     * @param bankFeeAmount the bankFeeAmount to set
//     */
//    public void setBankFeeAmount(Long bankFeeAmount) {
//        this.bankFeeAmount = bankFeeAmount;
//    }
//
//	/**
//	 * @return the branchName
//	 */
//	public String getBranchName() {
//		return branchName;
//	}
//
//	/**
//	 * @param branchName the branchName to set
//	 */
//	public void setBranchName(String branchName) {
//		this.branchName = branchName;
//	}
//
//	public String getAccountInfo() {
//		return accountInfo;
//	}
//
//	public void setAccountInfo(String accountInfo) {
//		this.accountInfo = accountInfo;
//	}
//	
//}