//package com.anyoption.beans.base;
//
//import java.io.Serializable;
//import java.util.Date;
//
//public class MarketRate implements Serializable {
//    protected Date rateTime;
//    protected double graphRate;
//    protected double dayRate;
//    
//    // Mobile client related
//    protected float chartXPosition;
//
//    public MarketRate() {
//    }
//    
//    public MarketRate(Date rateTime, double graphRate, double dayRate, float chartXPosition) {
//        this.rateTime = rateTime;
//        this.graphRate = graphRate;
//        this.dayRate = dayRate;
//        this.chartXPosition = chartXPosition;
//    }
//    
//    public Date getRateTime() {
//        return rateTime;
//    }
//
//    public void setRateTime(Date rateTime) {
//        this.rateTime = rateTime;
//    }
//
//    public double getGraphRate() {
//        return graphRate;
//    }
//
//    public void setGraphRate(double graphRate) {
//        this.graphRate = graphRate;
//    }
//
//    public double getDayRate() {
//        return dayRate;
//    }
//
//    public void setDayRate(double dayRate) {
//        this.dayRate = dayRate;
//    }
//
//    public float getChartXPosition() {
//        return chartXPosition;
//    }
//
//    public void setChartXPosition(float chartXPosition) {
//        this.chartXPosition = chartXPosition;
//    }
//}