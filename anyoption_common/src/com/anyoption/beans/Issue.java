//package com.anyoption.beans;
//
//import java.util.Date;
//
//public class Issue implements java.io.Serializable{
//
//	private long id;
//	private long userId;
//	private String subjectId;
//	private String priorityId;
//	private String statusId;
//	private String username;
//	private String skin;
//	private String smsStatus;
//	private long creditCardId;
//
//	private long populationEntryId;
//
//	// Fields that hold the values of the last action
//	private Date lastActionTime;
//	private String lastActionTimeOffset;
//	private boolean lastSignificant;
//	private String lastComments;
//	private String lastChannelId;
//	private String lastReachedStatusId;
//	private long lastCallDirectionId;
//	private String lastIssueActionType;
//
//	private String lastActivityName;
//
//	private String chat;
//	private String chatType;
//
//	private Date timeCreated;
//	private int daysInProgress;
//	private int daysWithoutTreatment;
//	private String country;
//	private int expandCollapseRI;
//	private int type;
//	private String filesRiskStatusId;
//	private Date lastReachedCallDate;
//	private Date lastCallDate;
//	private String riskReasons;
//
//	public Issue() {
//	}
//
//	/**
//	 * Costractor that copys an issue to compare fields that had been changed.
//	 */
//	public Issue(Issue issueToKeep) {
//		id = issueToKeep.id;
//		subjectId = new String(issueToKeep.getSubjectId());
//		priorityId = new String(issueToKeep.getPriorityId());
//		statusId = new String(issueToKeep.getStatusId());
//	}
//
//	/**
//	 * @return the lastChannelId
//	 */
//	public String getLastChannelId() {
//		return lastChannelId;
//	}
//
//	/**
//	 * @param lastChannelId the lastChannelId to set
//	 */
//	public void setLastChannelId(String lastChannelId) {
//		this.lastChannelId = lastChannelId;
//	}
//
//	/**
//	 * @return the lastComments
//	 */
//	public String getLastComments() {
//		return lastComments;
//	}
//
//	/**
//	 * @return the lastComments
//	 */
//	public String getLastCommentsWithBr() {
//		return lastComments.replace("\n", "<br/>");
//	}
//
//	/**
//	 * @param lastComments the lastComments to set
//	 */
//	public void setLastComments(String lastComments) {
//		this.lastComments = lastComments;
//	}
//
//	/**
//	 * @return the lastReachedStatusId
//	 */
//	public String getLastReachedStatusId() {
//		return lastReachedStatusId;
//	}
//
//	/**
//	 * @param lastReachedStatusId the lastReachedStatusId to set
//	 */
//	public void setLastReachedStatusId(String lastReachedStatusId) {
//		this.lastReachedStatusId = lastReachedStatusId;
//	}
//
//	/**
//	 * @return the lastSignificant
//	 */
//	public boolean isLastSignificant() {
//		return lastSignificant;
//	}
//
//	/**
//	 * @param lastSignificant the lastSignificant to set
//	 */
//	public void setLastSignificant(boolean lastSignificant) {
//		this.lastSignificant = lastSignificant;
//	}
//
//	public String getPriorityId() {
//		return priorityId;
//	}
//	public void setPriorityId(String priorityId) {
//		this.priorityId = priorityId;
//	}
//
//	public long getId() {
//		return id;
//	}
//	public void setId(long id) {
//		this.id = id;
//	}
//
//	public String getStatusId() {
//		return statusId;
//	}
//	public void setStatusId(String statusId) {
//		this.statusId = statusId;
//	}
//	public String getSubjectId() {
//		return subjectId;
//	}
//	public void setSubjectId(String subjectId) {
//		this.subjectId = subjectId;
//	}
//
//	public long getUserId() {
//		return userId;
//	}
//	public void setUserId(long userId) {
//		this.userId = userId;
//	}
//	public String getUsername() {
//		return username;
//	}
//
//	public void setUsername(String username) {
//		this.username = username;
//	}
//
//	/**
//	 * Constructs a <code>String</code> with all attributes
//	 * in name = value format.
//	 *
//	 * @return a <code>String</code> representation
//	 * of this object.
//	 */
//	public String toString()
//	{
//	    final String TAB = " \n ";
//
//	    String retValue = "";
//
//	    retValue = "Issue ( "
//	        + super.toString() + TAB
//	        + "id = " + this.id + TAB
//	        + "userId = " + this.userId + TAB
//	        + "subjectId = " + this.subjectId + TAB
//	        + "priorityId = " + this.priorityId + TAB
//	        + "statusId = " + this.statusId + TAB
//	        + "populationEntryId = " + this.populationEntryId + TAB
//	        + "daysInProgress = " + this.daysInProgress + TAB
//	        + "country = " + this.country + TAB
//	        + " )";
//
//	    return retValue;
//	}
//
//	public String getSkin() {
//		return skin;
//	}
//
//	public void setSkin(String skin) {
//		this.skin = skin;
//	}
//
//	/**
//	 * @return the lastActionTime
//	 */
//	public Date getLastActionTime() {
//		return lastActionTime;
//	}
//
//	/**
//	 * @param lastActionTime the lastActionTime to set
//	 */
//	public void setLastActionTime(Date lastActionTime) {
//		this.lastActionTime = lastActionTime;
//	}
//
//	/**
//	 * @return the lastActionTimeOffset
//	 */
//	public String getLastActionTimeOffset() {
//		return lastActionTimeOffset;
//	}
//
//	/**
//	 * @param lastActionTimeOffset the lastActionTimeOffset to set
//	 */
//	public void setLastActionTimeOffset(String lastActionTimeOffset) {
//		this.lastActionTimeOffset = lastActionTimeOffset;
//	}
//
//	/**
//	 * @return the lastCallDirectionId
//	 */
//	public long getLastCallDirectionId() {
//		return lastCallDirectionId;
//	}
//
//	/**
//	 * @param lastCallDirectionId the lastCallDirectionId to set
//	 */
//	public void setLastCallDirectionId(long lastCallDirectionId) {
//		this.lastCallDirectionId = lastCallDirectionId;
//	}
//
//	/**
//	 * @return the populationEntryId
//	 */
//	public long getPopulationEntryId() {
//		return populationEntryId;
//	}
//
//	/**
//	 * @param populationEntryId the populationEntryId to set
//	 */
//	public void setPopulationEntryId(long populationEntryId) {
//		this.populationEntryId = populationEntryId;
//	}
//
//	/**
//	 * @return the lastActivityName
//	 */
//	public String getLastActivityName() {
//		return lastActivityName;
//	}
//
//	/**
//	 * @param lastActivityName the lastActivityName to set
//	 */
//	public void setLastActivityName(String lastActivityName) {
//		this.lastActivityName = lastActivityName;
//	}
//
//	public String getChat() {
//		return chat;
//	}
//
//	public void setChat(String chat) {
//		this.chat = chat;
//	}
//
//	/**
//	 * @return the lastIssueActionType
//	 */
//	public String getLastIssueActionType() {
//		return lastIssueActionType;
//	}
//
//	/**
//	 * @param lastIssueActionType the lastIssueActionType to set
//	 */
//	public void setLastIssueActionType(String lastIssueActionType) {
//		this.lastIssueActionType = lastIssueActionType;
//	}
//
//	public String getChatType() {
//		return chatType;
//	}
//
//	public void setChatType(String chatType) {
//		this.chatType = chatType;
//	}
//
//	/**
//	 * @return the timeCreated
//	 */
//	public Date getTimeCreated() {
//		return timeCreated;
//	}
//
//	/**
//	 * @param timeCreated the timeCreated to set
//	 */
//	public void setTimeCreated(Date timeCreated) {
//		this.timeCreated = timeCreated;
//	}
//
//	/**
//	 * @return the daysInProgress
//	 */
//	public int getDaysInProgress() {
//		return daysInProgress;
//	}
//
//	/**
//	 * @param daysInProgress the daysInProgress to set
//	 */
//	public void setDaysInProgress(int daysInProgress) {
//		this.daysInProgress = daysInProgress;
//	}
//
//	/**
//	 * @return the daysWithoutTreatment
//	 */
//	public int getDaysWithoutTreatment() {
//		return daysWithoutTreatment;
//	}
//
//	/**
//	 * @param daysWithoutTreatment the daysWithoutTreatment to set
//	 */
//	public void setDaysWithoutTreatment(int daysWithoutTreatment) {
//		this.daysWithoutTreatment = daysWithoutTreatment;
//	}
//
//	/**
//	 * @return the country
//	 */
//	public String getCountry() {
//		return country;
//	}
//
//	/**
//	 * @param country the country to set
//	 */
//	public void setCountry(String country) {
//		this.country = country;
//	}
//
//	/**
//	 * @return the expandCollapseRI
//	 */
//	public int getExpandCollapseRI() {
//		return expandCollapseRI;
//	}
//
//	/**
//	 * @param expandCollapseRI the expandCollapseRI to set
//	 */
//	public void setExpandCollapseRI(int expandCollapseRI) {
//		this.expandCollapseRI = expandCollapseRI;
//	}
//
//	/**
//	 * @return the type
//	 */
//	public int getType() {
//		return type;
//	}
//
//	/**
//	 * @param type the type to set
//	 */
//	public void setType(int type) {
//		this.type = type;
//	}
//
//	/**
//	 * @return the filesRiskStatusId
//	 */
//	public String getFilesRiskStatusId() {
//		return filesRiskStatusId;
//	}
//
//	/**
//	 * @param filesRiskStatusId the filesRiskStatusId to set
//	 */
//	public void setFilesRiskStatusId(String filesRiskStatusId) {
//		this.filesRiskStatusId = filesRiskStatusId;
//	}
//
//	/**
//	 * @return the lastCallDate
//	 */
//	public Date getLastCallDate() {
//		return lastCallDate;
//	}
//
//	/**
//	 * @param lastCallDate the lastCallDate to set
//	 */
//	public void setLastCallDate(Date lastCallDate) {
//		this.lastCallDate = lastCallDate;
//	}
//
//	/**
//	 * @return the lastReachedCallDate
//	 */
//	public Date getLastReachedCallDate() {
//		return lastReachedCallDate;
//	}
//
//	/**
//	 * @param lastReachedCallDate the lastReachedCallDate to set
//	 */
//	public void setLastReachedCallDate(Date lastReachedCallDate) {
//		this.lastReachedCallDate = lastReachedCallDate;
//	}
//
//	/**
//	 * @return the riskReasons
//	 */
//	public String getRiskReasons() {
//		return riskReasons;
//	}
//
//	/**
//	 * @param riskReasons the riskReasons to set
//	 */
//	public void setRiskReasons(String riskReasons) {
//		this.riskReasons = riskReasons;
//	}
//
//	public String getSmsStatus(){
//		return smsStatus;
//	}
//
//	public void setSmsStatus(String smsStatus) {
//		this.smsStatus = smsStatus;
//	}
//
//	public long getCreditCardId() {
//		return creditCardId;
//	}
//
//	public void setCreditCardId(long creditCardId) {
//		this.creditCardId = creditCardId;
//	}
//}
