//package com.anyoption.beans;
//
//import java.io.Serializable;
//
///**
// * SkinCurrencies vo class
// * 
// * @author Kobi
// */
//public class SkinCurrencies implements Serializable {
//	private long id;
//	private long skinId;
//	private long currencyId;
//	private String displayName;
//	private double cashToPointsRate;
//
//	public SkinCurrencies() {
//		displayName = "";
//	}
//
//	/**
//	 * @return the currencyId
//	 */
//	public long getCurrencyId() {
//		return currencyId;
//	}
//
//	/**
//	 * @param currencyId the currencyId to set
//	 */
//	public void setCurrencyId(long currencyId) {
//		this.currencyId = currencyId;
//	}
//
//	/**
//	 * @return the displayName
//	 */
//	public String getDisplayName() {
//		return displayName;
//	}
//
//	/**
//	 * @param displayName the displayName to set
//	 */
//	public void setDisplayName(String displayName) {
//		this.displayName = displayName;
//	}
//
//	/**
//	 * @return the id
//	 */
//	public long getId() {
//		return id;
//	}
//
//	/**
//	 * @param id the id to set
//	 */
//	public void setId(long id) {
//		this.id = id;
//	}
//
//	/**
//	 * @return the skinId
//	 */
//	public long getSkinId() {
//		return skinId;
//	}
//
//	/**
//	 * @param skinId the skinId to set
//	 */
//	public void setSkinId(long skinId) {
//		this.skinId = skinId;
//	}
//
//	/**
//	 * @return the cashToPointsRate
//	 */
//	public double getCashToPointsRate() {
//		return cashToPointsRate;
//	}
//
//	/**
//	 * @param cashToPointsRate the cashToPointsRate to set
//	 */
//	public void setCashToPointsRate(double cashToPointsRate) {
//		this.cashToPointsRate = cashToPointsRate;
//	}
//
//	public String toString() {
//	    String ls = System.getProperty("line.separator");
//	    return ls + "SkinCurrencies" + ls
//	        + super.toString() + ls
//	        + "skinId: " + skinId + ls
//	        + "currencyId: " + currencyId + ls
//	        + "displayName: " + displayName + ls
//	        + "cashToPointsRate: " + cashToPointsRate + ls;
//	}
//}