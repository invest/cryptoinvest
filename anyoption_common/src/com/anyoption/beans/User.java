package com.anyoption.beans;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import com.anyoption.common.annotations.AnyoptionNoJSON;
import com.anyoption.common.beans.Country;
import com.anyoption.common.beans.Skin;
import com.anyoption.common.beans.base.Currency;
import com.anyoption.common.managers.CurrenciesManagerBase;
import com.anyoption.common.managers.LanguagesManagerBase;
import com.anyoption.common.util.Sha1;
import com.anyoption.managers.SkinsManagerBase;
import com.anyoption.util.CommonUtil;



public class User extends com.anyoption.common.beans.User {
    /**
	 * 
	 */
	private static final long serialVersionUID = -1777795724482389745L;

	private static final Logger log = Logger.getLogger(User.class);
    
//    @AnyoptionNoJSON
//    protected Date timeLastLogin;
    @AnyoptionNoJSON
    protected String comments;
    @AnyoptionNoJSON
    protected String ip;
    @AnyoptionNoJSON
    protected long limitId;
    @AnyoptionNoJSON
    protected Date timeModified;

//    protected long classId;
    @AnyoptionNoJSON
    protected String keyword;
    @AnyoptionNoJSON
    protected boolean taxExemption;
    @AnyoptionNoJSON
    protected String companyName;
    @AnyoptionNoJSON
    protected BigDecimal companyId;
    @AnyoptionNoJSON
    protected BigDecimal referenceId;
    @AnyoptionNoJSON
    protected int isNextInvestOnUs;
    @AnyoptionNoJSON
    protected boolean idDocVerify;    // if true - user send us documents
    @AnyoptionNoJSON
    protected String utcOffsetModified;
    @AnyoptionNoJSON
    protected long languageId;
    @AnyoptionNoJSON
    protected String utcOffsetCreated;
    @AnyoptionNoJSON
    protected boolean isRisky;
    @AnyoptionNoJSON
    protected long tierUserId;
    @AnyoptionNoJSON
    protected boolean isFalseAccount;
    @AnyoptionNoJSON
    protected boolean isContactForDaa;
    @AnyoptionNoJSON
    protected Date lastDeclineDate;
	/* Merged code */
	// @AnyoptionNoJSON
	// protected long writerId;

    //use to comper last login date with now to know if to show banner
    @AnyoptionNoJSON
    protected Date timeLastLoginFromDb;

    @AnyoptionNoJSON
    private boolean haveRegistrationBonus;
    @AnyoptionNoJSON
    protected long affiliateKey;
    @AnyoptionNoJSON
    protected long subAffiliateKey;
    @AnyoptionNoJSON
    protected String specialCode;
    @AnyoptionNoJSON
    protected long chatId;
    @AnyoptionNoJSON
    protected String clickId;
    @AnyoptionNoJSON
    protected boolean isVip;
    @AnyoptionNoJSON
    protected boolean isAuthorizedMail;  // Indicate that the user activate our authentication link after registration
    @AnyoptionNoJSON
    protected long authorizedMailRefusedTimes;
    @AnyoptionNoJSON
    protected Date timeFirstAuthorized;
    @AnyoptionNoJSON
    protected String userAgent;
    @AnyoptionNoJSON
    protected Country country;
    @AnyoptionNoJSON
    protected String countryName;
    @AnyoptionNoJSON
    protected String languageName;
    @AnyoptionNoJSON
    protected String skinName;
    @AnyoptionNoJSON
    protected Skin skin;
    @AnyoptionNoJSON
    protected long rankId;
//    @AnyoptionNoJSON
//    protected long statusId;
    @AnyoptionNoJSON
    protected long totalWinLose;
    

    public void setGenderTxt(Locale locale) {
    	if (null == gender) {
    		genderTxt = null;
    	} else if (gender.equals("M")) {
    		genderTxt = CommonUtil.getMessage(locale, "register.gender.male", null);
    	} else {
    		genderTxt = CommonUtil.getMessage(locale, "register.gender.female", null);
    	}
    }
    
    @Override
    public String getGender() {
        return gender;
    }

    @Override
    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getBalanceTxt() {
        return CommonUtil.formatCurrencyAmountAO(balance, true, currencyId);
    }

//    public long getClassId() {
//        return classId;
//    }
//
//    public void setClassId(long classId) {
//        this.classId = classId;
//    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public BigDecimal getCompanyId() {
        return companyId;
    }

    public void setCompanyId(BigDecimal companyId) {
        this.companyId = companyId;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getGenderForTemplateEnHeUse() {
        if (gender.equals("M")) {
            return "template.gender.male";
        } else {
            return "template.gender.female";
        }
    }

    public String getGenderForTemplateUse() {
        if (gender.equals("M")) {
            return "template.des.gender.male";
        } else {
            return "template.des.gender.female";
        }
    }

    public boolean isIdDocVerify() {
        return idDocVerify;
    }

    public void setIdDocVerify(boolean idDocVerify) {
        this.idDocVerify = idDocVerify;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public boolean isContactForDaa() {
        return isContactForDaa;
    }

	public void setContactForDaa(boolean isContactForDaa) {
        this.isContactForDaa = isContactForDaa;
    }

    public boolean isFalseAccount() {
        return isFalseAccount;
    }

    public void setFalseAccount(boolean isFalseAccount) {
        this.isFalseAccount = isFalseAccount;
    }

    public int getIsNextInvestOnUs() {
        return isNextInvestOnUs;
    }

    public void setIsNextInvestOnUs(int isNextInvestOnUs) {
        this.isNextInvestOnUs = isNextInvestOnUs;
    }

    public boolean isRisky() {
        return isRisky;
    }

    public void setRisky(boolean isRisky) {
        this.isRisky = isRisky;
    }

    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    public long getLanguageId() {
        return languageId;
    }

    public void setLanguageId(long languageId) {
        this.languageId = languageId;
    }

    public Date getLastDeclineDate() {
        return lastDeclineDate;
    }

    public void setLastDeclineDate(Date lastDeclineDate) {
        this.lastDeclineDate = lastDeclineDate;
    }

    public long getLimitId() {
        return limitId;
    }

    /**
	 * Return limitId long value
	 * @return
	 */
	public Long getLimitIdLongValue() {
		return Long.valueOf(limitId);
	}
	
    public void setLimitId(long limitId) {
        this.limitId = limitId;
    }

    public BigDecimal getReferenceId() {
        return referenceId;
    }

    public void setReferenceId(BigDecimal referenceId) {
        this.referenceId = referenceId;
    }

    public String getTaxBalanceTxt() {
        return CommonUtil.formatCurrencyAmountAO(taxBalance, true, currencyId);
    }

    public boolean isTaxExemption() {
        return taxExemption;
    }

    public void setTaxExemption(boolean taxExemption) {
        this.taxExemption = taxExemption;
    }

    public long getTierUserId() {
        return tierUserId;
    }

    public void setTierUserId(long tierUserId) {
        this.tierUserId = tierUserId;
    }

    public void setBirthDay(int day) {
        // do nothing
    }

    public int getBirthDay() {
        Calendar c = Calendar.getInstance();
        c.setTime(timeBirthDate);
        return c.get(Calendar.DAY_OF_MONTH);
    }

    public void setBirthMonth(int month) {
        // do nothing
    }

    public int getBirthMonth() {
        Calendar c = Calendar.getInstance();
        c.setTime(timeBirthDate);
        return c.get(Calendar.MONTH) + 1;
    }

    public void setBirthYear(int year) {
        // do nothing
    }

    public int getBirthYear() {
        Calendar c = Calendar.getInstance();
        c.setTime(timeBirthDate);
        return c.get(Calendar.YEAR);
    }

    public Date getTimeLastLogin() {
        return timeLastLogin;
    }

    public void setTimeLastLogin(Date timeLastLogin) {
        this.timeLastLogin = timeLastLogin;
    }

    public Date getTimeLastLoginFromDb() {
        return timeLastLoginFromDb;
    }

    public void setTimeLastLoginFromDb(Date timeLastLoginFromDb) {
        this.timeLastLoginFromDb = timeLastLoginFromDb;
    }

    public Date getTimeModified() {
        return timeModified;
    }

    public void setTimeModified(Date timeModified) {
        this.timeModified = timeModified;
    }

    /**
     * get userName for calcalistGame without _cg suffix
     * @return
     */
    public String getUserNameCalGame() {
        String userName = getUserName();
        return userName.substring(0, userName.length() - 3);
    }

    public String getUtcOffsetCreated() {
        return utcOffsetCreated;
    }

    public void setUtcOffsetCreated(String utcOffsetCreated) {
        this.utcOffsetCreated = utcOffsetCreated;
    }

    public String getUtcOffsetModified() {
        return utcOffsetModified;
    }

    public void setUtcOffsetModified(String utcOffsetModified) {
        this.utcOffsetModified = utcOffsetModified;
    }

    public boolean isHaveRegistrationBonus() {
        return haveRegistrationBonus;
    }

    public void setHaveRegistrationBonus(boolean haveRegistrationBonus) {
        this.haveRegistrationBonus = haveRegistrationBonus;
    }

    @Override
    public Locale getLocale() {
        if (null == locale) {
            locale = new Locale(LanguagesManagerBase.getLanguage(SkinsManagerBase.getSkin(skinId).getDefaultLanguageId()).getCode());
        }
        return locale;
    }

    @Override
    public String getCurrencySymbol() {
    	return CommonUtil.getCurrencySymbol(this.currencyId);
    }

    @Override
    public Currency getCurrency() {
    	return CurrenciesManagerBase.getCurrency(currencyId);
    }
	/* Merged code */
	//
	// public long getWriterId() {
	// return writerId;
	// }
	//
	// public void setWriterId(long writerId) {
	// this.writerId = writerId;
	// }

    /**
     * Init formatted values before serializing to JSON
     * @param l
     */
	public void initFormattedValues(String utcOffset, Locale locale) {
		balanceWF = getBalanceTxt();
		taxBalanceWF = getTaxBalanceTxt();
		currencySymbol = getCurrencySymbol();
		currencyLeftSymbol = CurrenciesManagerBase.getCurrency(currencyId).getIsLeftSymbolBool();
		setGenderTxt(locale);
		setTimeBirthDateTxt(utcOffset);
		try {
		    hashedPassword = Sha1.encode(password);
		} catch (Exception e) {
		    log.error("Can't hash password.", e);
		}
		getLocale();
	}

    @Override
    public String toString() {
        String ls = System.getProperty("line.separator");
        return ls + "User" + ls
            + super.toString() + ls
            + "id: " + id + ls
            + "balance: " + balance + ls
            + "taxBalance: " + taxBalance + ls
            + "userName: " + userName + ls
            + "password: " + "*****" + ls
            + "currencyId: " + currencyId + ls
            + "firstName: " + firstName + ls
            + "lastName: " + lastName + ls
            + "street: " + street + ls
            + "cityId: " + cityId + ls
            + "zipCode: " + zipCode + ls
            + "timeCreated: " + timeCreated + ls
            + "timeLastLogin: " + timeLastLogin + ls
            + "isActive: " + isActive + ls
            + "email: " + email + ls
            + "comments: " + comments + ls
            + "ip: " + ip + ls
            + "timeBirthDate: " + timeBirthDate + ls
            + "isContactByEmail: " + isContactByEmail + ls
            + "mobilePhone: " + mobilePhone + ls
            + "landLinePhone: " + landLinePhone + ls
            + "gender: " + gender + ls
            + "idNum: " + idNum + ls
            + "limitId: " + limitId + ls
            + "timeModified: " + timeModified + ls
            + "classId: " + classId + ls
            + "streetNo: " + streetNo + ls
            + "lastFailedTime: " + lastFailedTime + ls
            + "failedCount: " + failedCount + ls
            + "keyword: " + keyword + ls
            + "taxExemption: " + taxExemption + ls
            + "companyName: " + companyName + ls
            + "companyId: " + companyId + ls
            + "referenceId: " + referenceId + ls
            + "isNextInvestOnUs: " + isNextInvestOnUs + ls
            + "isContactBySMS: " + isContactBySMS + ls
            + "idDocVerify: " + idDocVerify + ls
            + "utcOffsetModified: " + utcOffsetModified + ls
            + "languageId: " + languageId + ls
            + "docsRequired: " + docsRequired + ls
            + "utcOffset: " + utcOffset + ls
            + "state: " + state + ls
            + "skinId: " + skinId + ls
            + "countryId: " + countryId + ls
            + "cityName: " + cityName + ls
            + "utcOffsetCreated: " + utcOffsetCreated + ls
            + "isRisky: " + isRisky + ls
            + "combinationId: " + combinationId + ls
            + "dynamicParam: " + dynamicParam + ls
            + "contactId: " + contactId + ls
            + "isContactByPhone: " + isContactByPhone + ls
            + "tierUserId: " + tierUserId + ls
            + "isFalseAccount: " + isFalseAccount + ls
            + "isAcceptedTerms: " + isAcceptedTerms + ls
            + "timeFirstVisit: " + timeFirstVisit + ls
            + "isDecline: " + isDecline + ls
            + "isContactForDaa: " + isContactForDaa + ls
            + "lastDeclineDate: " + lastDeclineDate + ls
            + "timeLastLoginFromDb: " + timeLastLoginFromDb + ls
            + "haveRegistrationBonus: " + haveRegistrationBonus + ls
            + "writerId: " + writerId + ls
            + "platformId: " + platformId + ls
            + "showCancelInvestment: " + showCancelInvestment + ls
            + "previousLoginTime: " + previousLoginTime + ls;
    }

	public long getAffiliateKey() {
		return affiliateKey;
	}

	public void setAffiliateKey(long affiliateKey) {
		this.affiliateKey = affiliateKey;
	}

	public long getSubAffiliateKey() {
		return subAffiliateKey;
	}

	public void setSubAffiliateKey(long subAffiliateKey) {
		this.subAffiliateKey = subAffiliateKey;
	}

	public String getSpecialCode() {
		return specialCode;
	}

	public void setSpecialCode(String specialCode) {
		this.specialCode = specialCode;
	}

	public long getChatId() {
		return chatId;
	}

	public void setChatId(long chatId) {
		this.chatId = chatId;
	}

	public String getClickId() {
		return clickId;
	}

	public void setClickId(String clickId) {
		this.clickId = clickId;
	}

	public boolean isVip() {
		return isVip;
	}

	public void setVip(boolean isVip) {
		this.isVip = isVip;
	}

	public boolean isAuthorizedMail() {
		return isAuthorizedMail;
	}

	public void setAuthorizedMail(boolean isAuthorizedMail) {
		this.isAuthorizedMail = isAuthorizedMail;
	}

	public long getAuthorizedMailRefusedTimes() {
		return authorizedMailRefusedTimes;
	}

	public void setAuthorizedMailRefusedTimes(long authorizedMailRefusedTimes) {
		this.authorizedMailRefusedTimes = authorizedMailRefusedTimes;
	}

	public Date getTimeFirstAuthorized() {
		return timeFirstAuthorized;
	}

	public void setTimeFirstAuthorized(Date timeFirstAuthorized) {
		this.timeFirstAuthorized = timeFirstAuthorized;
	}

	@Override
	public String getUserAgent() {
		return userAgent;
	}

	@Override
	public void setUserAgent(String userAgent) {
		this.userAgent = userAgent;
	}

	@Override
	public Country getCountry() {
		return country;
	}

	@Override
	public void setCountry(Country country) {
		this.country = country;
	}

	public String getCountryName() {
		return countryName;
	}

	public void setCountryName(String countryName) {
		this.countryName = countryName;
	}

	public String getLanguageName() {
		return languageName;
	}

	public void setLanguageName(String languageName) {
		this.languageName = languageName;
	}

	public String getSkinName() {
		return skinName;
	}

	public void setSkinName(String skinName) {
		this.skinName = skinName;
	}

	public Skin getSkin() {
		return skin;
	}

	public void setSkin(Skin skin) {
		this.skin = skin;
	}
	
	public void setTimeBirthDateTxt(String utcOffset) {
		timeBirthDateTxt = CommonUtil.getDateAndTimeFormatByDots(timeBirthDate, utcOffset);
	}
	
	public boolean isTestUser() {
		return classId == 0;
	}
	
	public long getRankId() {
		return rankId;
	}

	public void setRankId(long rankId) {
		this.rankId = rankId;
	}

//	public long getStatusId() {
//		return statusId;
//	}
//
//	public void setStatusId(long statusId) {
//		this.statusId = statusId;
//	}
	
	public long getTotalWinLose() {
		return totalWinLose;
	}

	public void setTotalWinLose(long totalWinLose) {
		this.totalWinLose = totalWinLose;
	}	
	
	public static String createBubblesToken(HttpSession session, long userId) { 
		return CommonUtil.createBubblesToken(session, userId);
	}
	
	public static int getUtcOffsetInMin(String utcOffset) {
        return CommonUtil.getUtcOffsetInMin(utcOffset);
    }
}