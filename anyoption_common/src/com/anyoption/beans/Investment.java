//package com.anyoption.beans;

//public class Investment extends com.anyoption.common.beans.Investment {
//    /**
//	 * 
//	 */
//	private static final long serialVersionUID = 3623353658351932774L;
//	// TODO not used anywhere?
//	public static final int STATUS_CANCELED = 100;
//    public static final int STATUS_SETTLED = 101;
//    public static final int STATUS_VOID = 102;
//    public static final int STATUS_NOT_SETTLED = 104;

//    public static final int SCHEDULED_HOURLY = 1;
//    public static final int SCHEDULED_DAILY = 2;
//    public static final int SCHEDULED_WEEKLY = 3;
//    public static final int SCHEDULED_MONTHLY = 4;
//    public static final int SCHEDULED_ONE_TOUCH = 5;
//    public static final int SCHEDULED_ALL = 0;

//    public static final int INVESTMENT_ONE_TOUCH_WIN = 1;
//
//    public static final long INVESTMENT_ONE_TOUCH_UP = 1;
//    public static final long INVESTMENT_ONE_TOUCH_DOWN = 0;
//
//    public static final long INVESTMENT_TYPE_ONE_DECIMAL = 2;

//    public static HashMap<Long, String> TYPE_NAMES = new HashMap<Long, String>();
//    public static HashMap<Integer, String> SCHEDULED_NAMES = new HashMap<Integer, String>();
//    static {
//        TYPE_NAMES.put(new Long(1), "Call");
//        TYPE_NAMES.put(new Long(2), "Put");
//        TYPE_NAMES.put(new Long(3), "One touch");
//
//        SCHEDULED_NAMES.put(1, "Hourly");
//        SCHEDULED_NAMES.put(2, "Daily");
//        SCHEDULED_NAMES.put(3, "Weekly");
//        SCHEDULED_NAMES.put(4, "Monthly");
//        SCHEDULED_NAMES.put(5, "One touch");
//    }

    
//    @AnyoptionNoJSON
//    protected long userId;
//    @AnyoptionNoJSON
//    protected long baseAmount;
//    @AnyoptionNoJSON
//    protected long currencyId;
//    @AnyoptionNoJSON
//    protected BigDecimal wwwLevel;
//    @AnyoptionNoJSON
//    protected Double realLevel;
//    @AnyoptionNoJSON
//    protected long writerId;
//    @AnyoptionNoJSON
//    protected String ip;
//    @AnyoptionNoJSON
//    protected long win;
//    @AnyoptionNoJSON
//    protected long baseWin;
//    @AnyoptionNoJSON
//    protected long lose;
//    @AnyoptionNoJSON
//    protected long baseLose;
//    @AnyoptionNoJSON
//    protected long houseResult;
//    @AnyoptionNoJSON
//    protected int isSettled;
//    @AnyoptionNoJSON
//    protected int isCanceled;
//    @AnyoptionNoJSON
//    protected Date timeCanceled;
//    @AnyoptionNoJSON
//    protected int isVoid;
//    @AnyoptionNoJSON
//    protected BigDecimal canceledWriterId;
//    @AnyoptionNoJSON
//    protected int isFree;
//    @AnyoptionNoJSON
//    protected double rate;
//    @AnyoptionNoJSON
//    protected long referenceInvestmentId;

//    @AnyoptionNoJSON
//    protected String utcOffsetCreated;
//    @AnyoptionNoJSON
//    protected String utcOffsetSettled;
//    @AnyoptionNoJSON
//    protected String utcOffsetCancelled;
//    @AnyoptionNoJSON
//    protected String utcOffsetEstClosing;
//    @AnyoptionNoJSON
//    protected String utcOffsetActClosing;

//    @AnyoptionNoJSON
//    protected Date timeLastInvest;
//    @AnyoptionNoJSON
//    protected Date timeActClosing;
//    @AnyoptionNoJSON
//    protected Date timeFirstInvest;
//    @AnyoptionNoJSON
//    protected String typeName;
//    @AnyoptionNoJSON
//    protected Double closingLevel;
//    @AnyoptionNoJSON
//    protected boolean selected;
//    @AnyoptionNoJSON
//    protected long scheduledId;
//    @AnyoptionNoJSON
//    protected long decimalPoint;

//    @AnyoptionNoJSON
//    protected String userName;
//    @AnyoptionNoJSON
//    protected String userFirstName;
//    @AnyoptionNoJSON
//    protected String userLastName;

//    @AnyoptionNoJSON
//    protected int totalLine;
//    @AnyoptionNoJSON
//    protected boolean isPositive;
//    @AnyoptionNoJSON
//    protected String userCountryCode;
//    @AnyoptionNoJSON
//    protected int oneTouchDecimalPoint;

//    @AnyoptionNoJSON
//    protected String skin;
//    @AnyoptionNoJSON
//    protected long insuranceAmountGM;
//    @AnyoptionNoJSON
//    protected double insurancePremiaPercent;
//    @AnyoptionNoJSON
//    protected double insuranceQualifyPercent;
//    @AnyoptionNoJSON
//    protected double currentInsuranceQualifyPercent;
//    @AnyoptionNoJSON
//    protected double insuranceCurrentLevel;
//    @AnyoptionNoJSON
//    protected boolean showInsurance;
//    @AnyoptionNoJSON
//    protected int insurancePosition;
//    @AnyoptionNoJSON
//    protected double roolUpQualifyPercent;
//    @AnyoptionNoJSON
//    protected int insuranceType; //1 = GM , 2 = RU
//    @AnyoptionNoJSON
//    protected double rollUpQualifyPercent;
//    @AnyoptionNoJSON
//    protected boolean isRollUp;
//    @AnyoptionNoJSON
//    protected boolean isGoldenMinutes;
//    @AnyoptionNoJSON
//    protected double rollUpPremiaPercent;
//    @AnyoptionNoJSON
//    protected Integer insuranceFlag;
//    @AnyoptionNoJSON
//    protected long rolledInvId;
//    @AnyoptionNoJSON
//    protected double marketLastDayClosingLevel;
//    @AnyoptionNoJSON
//    protected boolean acceptedSms;
//    @AnyoptionNoJSON
//    protected long bonusUserId;
//    @AnyoptionNoJSON
//    protected boolean isOpenGraph;
//    @AnyoptionNoJSON
//
//    private long sumInvest;         // sum of investments for one opportunity (my invest page)
    //	for caleculate the bonus amount if its nextInvestOnUs from type 2 or 3
//    @AnyoptionNoJSON
//    protected double bonusWinOdds;
//    @AnyoptionNoJSON
//    protected double bonusLoseOdds;
//    @AnyoptionNoJSON
//    private Date timeQuoted;
//    @AnyoptionNoJSON
//    protected int bonusOddsChangeTypeId;
//    @AnyoptionNoJSON
//    private boolean isHaveHourly;
//    @AnyoptionNoJSON
//    protected long defaultAmountValue;
//    @AnyoptionNoJSON
//	private long copyopInvId;
//    @AnyoptionNoJSON
//	private boolean isLikeHourly;

//	public Investment() {}
//
//	 public Investment(int totalLine2) {
//        totalLine = totalLine2;
//    }

//    public long getRefund() {
//        return amount + win - lose;
//    }

//    public long getBaseRefund() {
//        return baseAmount + baseWin - baseLose;
//    }

//    public long getRefundUp() {
//        Double win = new Double(getAmountWithoutFees() + getAmountWithoutFees() * oddsWin);
//        Double lose = new Double(getAmountWithoutFees() - getAmountWithoutFees() * oddsLose);
//        if (typeId == INVESTMENT_TYPE_PUT  && isFree == 1) {  // lose & is free
//            return amount;
//        }
//        if (INVESTMENT_TYPE_CALL == typeId || (getIsOneTouchInv() && isUpInvestment())) { // win
//            return Math.round(win);
//        }
//        return Math.round(lose);
//    }

//    public long getBaseRefundUp() {
//        Double win = new Double(getBaseAmountWithoutFees() + getBaseAmountWithoutFees() * oddsWin);
//        Double lose = new Double(getBaseAmountWithoutFees() - getBaseAmountWithoutFees() * oddsLose);
//        if (typeId == INVESTMENT_TYPE_PUT  && isFree == 1) {
//            return  baseAmount;
//        }
//        if (INVESTMENT_TYPE_CALL == typeId || (getIsOneTouchInv() && isUpInvestment())) {
//            return Math.round(win);
//        }
//        return Math.round(lose);
//    }

//    public long getRefundDown() {
//        Double win = new Double(getAmountWithoutFees() + getAmountWithoutFees() * oddsWin);
//        Double lose = new Double(getAmountWithoutFees() - getAmountWithoutFees() * oddsLose);
//        if (typeId == INVESTMENT_TYPE_CALL && isFree == 1) { // lose & is free
//            return  amount;
//        }
//        if (INVESTMENT_TYPE_PUT == typeId || (getIsOneTouchInv() && isDownInvestment())) { // win
//            return Math.round(win);
//        }
//        return Math.round(lose);
//    }

//    public long getBaseRefundDown() { //TODO: base
//        Double win = new Double(getBaseAmountWithoutFees() + getBaseAmountWithoutFees() * oddsWin);
//        Double lose = new Double(getBaseAmountWithoutFees() - getBaseAmountWithoutFees() * oddsLose);
//        if (typeId == INVESTMENT_TYPE_CALL && isFree == 1) {
//            return baseAmount;
//        }
//        if (INVESTMENT_TYPE_PUT == typeId || (getIsOneTouchInv() && isDownInvestment())) {
//            return Math.round(win);
//        }
//        return Math.round(lose);
//    }

//    public long getRefundSort() {
//        if (isCanceled == 1) {
//            return Long.MAX_VALUE;
//        }
//        return amount + win - lose;
//    }

//    public boolean isSelected() {
//        return selected;
//    }
//
//    public void setSelected(boolean selected) {
//        this.selected = selected;
//    }

//    public double getCurrentLevelValue() {
//        return currentLevel.doubleValue();
//    }

//    public Double getClosingLevel() {
//        return closingLevel;
//    }
//
//    public void setClosingLevel(Double closingLevel) {
//        this.closingLevel = closingLevel;
//    }

//    public Date getTimeActClosing() {
//        return timeActClosing;
//    }
//
//    public void setTimeActClosing(Date timeActClosing) {
//        this.timeActClosing = timeActClosing;
//    }
//
//    public Date getTimeLastInvest() {
//        return timeLastInvest;
//    }
//
//    public void setTimeLastInvest(Date timeLastInvest) {
//        this.timeLastInvest = timeLastInvest;
//    }
//
//    public Date getTimeFirstInvest() {
//        return timeFirstInvest;
//    }
//
//    public void setTimeFirstInvest(Date timeFirstInvest) {
//        this.timeFirstInvest = timeFirstInvest;
//    }
//
//    public String getTypeName() {
//        return typeName;
//    }
//
//    public void setTypeName(String typeName) {
//        this.typeName = typeName;
//    }

//    public long getAmountWithFees() {
//        return amount + getInsuranceAmount();
//    }

//    public long getHouseResult() {
//        return houseResult;
//    }
//
//    public void setHouseResult(long houseResult) {
//        this.houseResult = houseResult;
//    }

//    public String getIp() {
//        return ip;
//    }
//
//    public void setIp(String ip) {
//        this.ip = ip;
//    }

//    public int getIsCanceled() {
//        return isCanceled;
//    }
//
//    public void setIsCanceled(int isCanceled) {
//        this.isCanceled = isCanceled;
//    }

//    public int getIsSettled() {
//        return isSettled;
//    }
//
//    public void setIsSettled(int isSettled) {
//        this.isSettled = isSettled;
//    }

//    public int getIsVoid() {
//        return isVoid;
//    }
//
//    public void setIsVoid(int isVoid) {
//        this.isVoid = isVoid;
//    }

//    public long getLose() {
//        return lose;
//    }
//
//    public void setLose(long lose) {
//        this.lose = lose;
//    }

//    public Date getTimeCanceled() {
//        return timeCanceled;
//    }
//
//    public void setTimeCanceled(Date timeCanceled) {
//        this.timeCanceled = timeCanceled;
//    }

//    public long getUserId() {
//        return userId;
//    }
//
//    public void setUserId(long userId) {
//        this.userId = userId;
//    }

//    public long getWin() {
//        return win;
//    }
//
//    public void setWin(long win) {
//        this.win = win;
//    }

//    public long getWriterId() {
//        return writerId;
//    }
//
//    public void setWriterId(long writerId) {
//        this.writerId = writerId;
//    }

//    public BigDecimal getCanceledWriterId() {
//        return canceledWriterId;
//    }
//
//    public void setCanceledWriterId(BigDecimal canceledWriterId) {
//        this.canceledWriterId = canceledWriterId;
//    }

//    public long getScheduledId() {
//        return scheduledId;
//    }
//
//    public void setScheduledId(long scheduledId) {
//        this.scheduledId = scheduledId;
//    }

//    public String getUserFirstName() {
//        return userFirstName;
//    }
//
//    public void setUserFirstName(String userFirstName) {
//        this.userFirstName = userFirstName;
//    }
//
//    public String getUserLastName() {
//        return userLastName;
//    }
//
//    public void setUserLastName(String userLastName) {
//        this.userLastName = userLastName;
//    }
//
//    public String getUserName() {
//        return userName;
//    }
//
//    public void setUserName(String userName) {
//        this.userName = userName;
//    }
//
//    public int getTotalLine() {
//        return totalLine;
//    }
//
//    public void setTotalLine(int totalLine) {
//        this.totalLine = totalLine;
//    }

//    public Double getRealLevel() {
//        return realLevel;
//    }
//
//    public void setRealLevel(Double realLevel) {
//        this.realLevel = realLevel;
//    }
//    public BigDecimal getWwwLevel() {
//        return wwwLevel;
//    }
//    public void setWwwLevel(BigDecimal wwwLevel) {
//        this.wwwLevel = wwwLevel;
//    }

//    public int getIsFree() {
//        return isFree;
//    }
//
//    public void setIsFree(int isFree) {
//        this.isFree = isFree;
//    }

//    public boolean isUpInvestment() {
//        if (oneTouchUpDown == ConstantsBase.ONE_TOUCH_IS_UP) {
//            return true;
//        }
//        return false;
//    }
//
//    public boolean isDownInvestment() {
//        if (oneTouchUpDown == ConstantsBase.ONE_TOUCH_IS_DOWN) {
//            return true;
//        }
//        return false;
//    }

//    /**
//     * Check if its one touch investment
//     * @return
//     */
//    public boolean getIsOneTouchInv() {
//        if (typeId == INVESTMENT_TYPE_ONE) {
//            return true;
//        }
//        return false;
//    }

//    public long getCurrencyId() {
//        return currencyId;
//    }
//
//    public void setCurrencyId(long currencyId) {
//        this.currencyId = currencyId;
//    }
//
//    public long getBaseAmount() {
//        return baseAmount;
//    }
//
//    public void setBaseAmount(long baseAmount) {
//        this.baseAmount = baseAmount;
//    }

//    /**
//     * @return the baseLose
//     */
//    public long getBaseLose() {
//        return baseLose;
//    }
//
//    /**
//     * @param baseLose the baseLose to set
//     */
//    public void setBaseLose(long baseLose) {
//        this.baseLose = baseLose;
//    }
//
//    /**
//     * @return the baseWin
//     */
//    public long getBaseWin() {
//        return baseWin;
//    }
//
//    /**
//     * @param baseWin the baseWin to set
//     */
//    public void setBaseWin(long baseWin) {
//        this.baseWin = baseWin;
//    }

//    /**
//     * @return the utcOffsetCreated
//     */
//    public String getUtcOffsetCreated() {
//        return utcOffsetCreated;
//    }
//
//    /**
//     * @param utcOffsetCreated the utcOffsetCreated to set
//     */
//    public void setUtcOffsetCreated(String utcOffsetCreated) {
//        this.utcOffsetCreated = utcOffsetCreated;
//    }
//
//    /**
//     * @return the utcOffsetSettled
//     */
//    public String getUtcOffsetSettled() {
//        return utcOffsetSettled;
//    }
//
//    /**
//     * @param utcOffsetSettled the utcOffsetSettled to set
//     */
//    public void setUtcOffsetSettled(String utcOffsetSettled) {
//        this.utcOffsetSettled = utcOffsetSettled;
//    }
//
//    /**
//     * @return the utcOffsetCancelled
//     */
//    public String getUtcOffsetCancelled() {
//        return utcOffsetCancelled;
//    }
//
//    /**
//     * @param utcOffsetCancelled the utcOffsetCancelled to set
//     */
//    public void setUtcOffsetCancelled(String utcOffsetCancelled) {
//        this.utcOffsetCancelled = utcOffsetCancelled;
//    }

//    /**
//     * @return the utcOffsetActClosing
//     */
//    public String getUtcOffsetActClosing() {
//        return utcOffsetActClosing;
//    }
//
//    /**
//     * @param utcOffsetActClosing the utcOffsetActClosing to set
//     */
//    public void setUtcOffsetActClosing(String utcOffsetActClosing) {
//        this.utcOffsetActClosing = utcOffsetActClosing;
//    }
//
//    /**
//     * @return the utcOffsetEstClosing
//     */
//    public String getUtcOffsetEstClosing() {
//        return utcOffsetEstClosing;
//    }
//
//    /**
//     * @param utcOffsetEstClosing the utcOffsetEstClosing to set
//     */
//    public void setUtcOffsetEstClosing(String utcOffsetEstClosing) {
//        this.utcOffsetEstClosing = utcOffsetEstClosing;
//    }

//    /**
//     * get is call option or not
//     * @return
//     */
//    public boolean getIsCallOption() {
//        if (typeId == INVESTMENT_TYPE_CALL) {
//            return true;
//        }
//        return false;
//    }

//    /**
//     * @return the userCountryCode
//     */
//    public String getUserCountryCode() {
//        return userCountryCode;
//    }
//
//    /**
//     * @param userCountryCode the userCountryCode to set
//     */
//    public void setUserCountryCode(String userCountryCode) {
//        this.userCountryCode = userCountryCode;
//    }
//
//    public int getOneTouchDecimalPoint() {
//        return oneTouchDecimalPoint;
//    }
//
//    public void setOneTouchDecimalPoint(int oneTouchDecimalPoint) {
//        this.oneTouchDecimalPoint = oneTouchDecimalPoint;
//    }
//
//    public String getSkin() {
//        return skin;
//    }
//
//    public void setSkin(String skin) {
//        this.skin = skin;
//    }

//    public long getDecimalPoint() {
//        return decimalPoint;
//    }
//
//    public void setDecimalPoint(long decimalPoint) {
//        this.decimalPoint = decimalPoint;
//    }

//    /**
//     * @return the  insuranceAmountGM + insuranceAmountRU
//     */
//    public long getInsuranceAmount() {
//        return  insuranceAmountGM + insuranceAmountRU;
//    }

//    /**
//     * in the money
//     * @return case call: gm level - inv current level, case put: inv current level - gm level
//     */
//    public double getCurrentInsuranceQualifyPercent() {
//        return currentInsuranceQualifyPercent;
//    }
//
//    public void setCurrentInsuranceQualifyPercent(double currentInsuranceQualifyPercent) {
//        this.currentInsuranceQualifyPercent = currentInsuranceQualifyPercent;
//    }
//
//    public double getInsuranceQualifyPercent() {
//        return insuranceQualifyPercent;
//    }
//
//    public void setInsuranceQualifyPercent(double insuranceQualifyPercent) {
//        this.insuranceQualifyPercent = insuranceQualifyPercent;
//    }

//    public double getInsurancePremiaPercent() {
//        return insurancePremiaPercent;
//    }
//
//    public void setInsurancePremiaPercent(double insurancePremiaPercent) {
//        this.insurancePremiaPercent = insurancePremiaPercent;
//    }

//    public double getInsuranceCurrentLevel() {
//        return insuranceCurrentLevel;
//    }
//
//    public void setInsuranceCurrentLevel(double insuranceCurrentLevel) {
//        this.insuranceCurrentLevel = insuranceCurrentLevel;
//    }
//
//    public boolean isShowInsurance() {
//        return showInsurance;
//    }
//
//    public void setShowInsurance(boolean showInsurance) {
//        this.showInsurance = showInsurance;
//    }
//
//    public int getInsurancePosition() {
//        return insurancePosition;
//    }
//
//    public void setInsurancePosition(int insurancePosition) {
//        this.insurancePosition = insurancePosition;
//    }

//    /**
//     * @return the bonusUserId
//     */
//    public long getBonusUserId() {
//        return bonusUserId;
//    }
//
//    /**
//     * @param bonusUserId the bonusUserId to set
//     */
//    public void setBonusUserId(long bonusUserId) {
//        this.bonusUserId = bonusUserId;
//    }

//    /**
//     * @return the roolUpQualifyPercent
//     */
//    public double getRoolUpQualifyPercent() {
//        return roolUpQualifyPercent;
//    }
//
//    /**
//     * @param roolUpQualifyPercent the roolUpQualifyPercent to set
//     */
//    public void setRoolUpQualifyPercent(double roolUpQualifyPercent) {
//        this.roolUpQualifyPercent = roolUpQualifyPercent;
//    }
//
//    /**
//     * @return the insuranceType
//     */
//    public int getInsuranceType() {
//        return insuranceType;
//    }
//
//    /**
//     * @param insuranceType the insuranceType to set
//     */
//    public void setInsuranceType(int insuranceType) {
//        this.insuranceType = insuranceType;
//    }
//
//    /**
//     * @return the isGoldenMinutes
//     */
//    public boolean isGoldenMinutes() {
//        return isGoldenMinutes;
//    }
//
//    /**
//     * @param isGoldenMinutes the isGoldenMinutes to set
//     */
//    public void setGoldenMinutes(boolean isGoldenMinutes) {
//        this.isGoldenMinutes = isGoldenMinutes;
//    }
//
//    /**
//     * @return the isRollUp
//     */
//    public boolean isRollUp() {
//        return isRollUp;
//    }
//
//    /**
//     * @param isRollUp the isRollUp to set
//     */
//    public void setRollUp(boolean isRollUp) {
//        this.isRollUp = isRollUp;
//    }
//
//    /**
//     * @return the rollUpQualifyPercent
//     */
//    public double getRollUpQualifyPercent() {
//        return rollUpQualifyPercent;
//    }
//
//    /**
//     * @param rollUpQualifyPercent the rollUpQualifyPercent to set
//     */
//    public void setRollUpQualifyPercent(double rollUpQualifyPercent) {
//        this.rollUpQualifyPercent = rollUpQualifyPercent;
//    }

//    /**
//     * @return the rollUpPremiaPercent
//     */
//    public double getRollUpPremiaPercent() {
//        return rollUpPremiaPercent;
//    }
//
//    /**
//     * @param rollUpPremiaPercent the rollUpPremiaPercent to set
//     */
//    public void setRollUpPremiaPercent(double rollUpPremiaPercent) {
//        this.rollUpPremiaPercent = rollUpPremiaPercent;
//    }

//    /**
//     * @return the referenceInvestmentId
//     */
//    public long getReferenceInvestmentId() {
//        return referenceInvestmentId;
//    }
//
//    /**
//     * @param referenceInvestmentId the referenceInvestmentId to set
//     */
//    public void setReferenceInvestmentId(long referenceInvestmentId) {
//        this.referenceInvestmentId = referenceInvestmentId;
//    }

//    /**
//     * @return the rate
//     */
//    public double getRate() {
//        return rate;
//    }
//
//    /**
//     * @param rate the rate to set
//     */
//    public void setRate(double rate) {
//        this.rate = rate;
//    }

//    /**
//     * @return the marketLastDayClosingLevel
//     */
//    public double getMarketLastDayClosingLevel() {
//        return marketLastDayClosingLevel;
//    }
//
//    /**
//     * @param marketLastDayClosingLevel the marketLastDayClosingLevel to set
//     */
//    public void setMarketLastDayClosingLevel(double marketLastDayClosingLevel) {
//        this.marketLastDayClosingLevel = marketLastDayClosingLevel;
//    }

//    /**
//     * @return the acceptedSms
//     */
//    public boolean isAcceptedSms() {
//        return acceptedSms;
//    }
//
//    /**
//     * @param acceptedSms the acceptedSms to set
//     */
//    public void setAcceptedSms(boolean acceptedSms) {
//        this.acceptedSms = acceptedSms;
//    }

//    /**
//     * @return the insuranceFlag
//     */
//    public Integer getInsuranceFlag() {
//        return insuranceFlag;
//    }
//
//    /**
//     * @param insuranceFlag the insuranceFlag to set
//     */
//    public void setInsuranceFlag(Integer insuranceFlag) {
//        this.insuranceFlag = insuranceFlag;
//    }

//    /**
//     * @return the rolledInvId
//     */
//    public long getRolledInvId() {
//        return rolledInvId;
//    }
//
//    /**
//     * @param rolledInvId the rolledInvId to set
//     */
//    public void setRolledInvId(long rolledInvId) {
//        this.rolledInvId = rolledInvId;
//    }

//    /**
//     * check if the investment was rolled up or not
//     * @return true if it was rolled else false
//     */
//    public boolean isRolledUp() {
//        return isCanceled == 1 && rolledInvId > 0 ? true : false;
//    }

//    /**
//     * check if the investment is roll up or not.
//     * @return true if it is roll up else false
//     */
//    public boolean isRollUpBought() {
//        return referenceInvestmentId > 0 && insuranceAmountRU > 0 ? true : false;
//    }

//    /**
//     * check if the investment was bought as GM or not.
//     * @return true if it was bought as GM else false
//     */
//    public boolean isGmBought() {
//        return null != insuranceFlag && insuranceFlag.intValue() == 1 && insuranceAmountGM > 0 ? true : false;
//    }

//    /**
//     * @return the isOpenGraph
//     */
//    public boolean isOpenGraph() {
//        return isOpenGraph;
//    }
//
//    /**
//     * @param isOpenGraph the isOpenGraph to set
//     */
//    public void setOpenGraph(boolean isOpenGraph) {
//        this.isOpenGraph = isOpenGraph;
//    }

//    /**
//     * @return the sumInvest
//     */
//    public long getSumInvest() {
//        return sumInvest;
//    }
//
//    /**
//     * @param sumInvest the sumInvest to set
//     */
//    public void setSumInvest(long sumInvest) {
//        this.sumInvest = sumInvest;
//    }

//    /**
//     * @return the insuranceAmountGM
//     */
//    public long getInsuranceAmountGM() {
//        return insuranceAmountGM;
//    }
//
//    /**
//     * @param insuranceAmountGM the insuranceAmountGM to set
//     */
//    public void setInsuranceAmountGM(long insuranceAmountGM) {
//        this.insuranceAmountGM = insuranceAmountGM;
//    }
//
//    /**
//     * check if this investment opp is publish by checking if we pass the first time to invest
//     * @return true if its publish else false
//     */
//    public boolean isInvestmentOppIsPublish() {
//        long secLeft = timeFirstInvest.getTime() - System.currentTimeMillis();
//        if (secLeft < 0) { //if we pass the time first invest
//            return true;
//        }
//        return false;
//    }

    //take out from the amount the fees.
//    public long getAmountWithoutFees() {
//        return amount - insuranceAmountGM - insuranceAmountRU;
//    }
//
//    public long getBaseAmountWithoutFees() {
//        return baseAmount - Math.round(insuranceAmountGM * rate) - Math.round(insuranceAmountRU * rate);
//    }

//    public String toString() {
//        String ls = System.getProperty("line.separator");
//        return ls + "Investment" + ls
//            + super.toString() + ls
//            + "id: " + id + ls
//            + "userId: " + userId + ls
//            + "opportunityId: " + opportunityId + ls
//            + "typeId: " + typeId + ls
//            + "amount: " + amount + ls
//            + "currentLevel: " + currentLevel + ls
//            + "oddsWin: " + oddsWin + ls
//            + "oddsLose: " + oddsLose + ls
//            + "writerId: " + writerId + ls
//            + "ip: " + ip + ls
//            + "win: " + win + ls
//            + "lose: " + lose + ls
//            + "houseResult: " + houseResult + ls
//            + "timeCreated: " + timeCreated + ls
//            + "timeSettled: " + timeSettled + ls
//            + "isSettled: " + isSettled + ls
//            + "isCanceled: " + isCanceled + ls
//            + "timeCanceled: " + timeCanceled + ls
//            + "isVoid: " + isVoid + ls
//            + "canceledWriterId: " + canceledWriterId + ls
//            + "marketId: " + marketId + ls
//            + "marketName: " + marketName + ls
//            + "timeEstClosing: " + timeEstClosing + ls
//            + "timeLastInvest: " + timeLastInvest + ls
//            + "timeActClosing: " + timeActClosing + ls
//            + "timeFirstInvest: " + timeFirstInvest + ls
//            + "typeName: " + typeName + ls
//            + "closingLevel: " + closingLevel + ls
//            + "selected: " + selected + ls
//            + "scheduledId: " + scheduledId + ls
//            + "userName: " + userName + ls
//            + "userFirstName: " + userFirstName + ls
//            + "userLastName: " + userLastName + ls
//            + "isFree: " + isFree + ls
//            + "decimalPoint: " + decimalPoint + ls
//            + "insuranceAmountGM: " + insuranceAmountGM + ls
//            + "insuranceAmountRU: " + insuranceAmountRU + ls
//            + "insurancePremiaPercent: " + insurancePremiaPercent + ls
//            + "insuranceQualifyPercent: " + insuranceQualifyPercent + ls
//            + "currentInsuranceQualifyPercent: " + currentInsuranceQualifyPercent + ls
//            + "insuranceCurrentLevel: " + insuranceCurrentLevel + ls
//            + "showInsurance: " + showInsurance + ls
//            + "insurancePosition: " + insurancePosition + ls
//            + "utcOffsetCreated: " + utcOffsetCreated + ls
//            + "defaultAmountValue: " + defaultAmountValue + ls
//            + "copyopType: " + copyopType + ls;
//    }

//    public void initFormattedValues(Locale locale, String utcOffset){
//    	setAsset(locale);
//    	setLevel();
//    	setExpiryLevel();
//    	setTimePurchased(utcOffset);
//    	setTimeEstClosingTxt(utcOffset);
//    	setCurrentLevelTxt();
//    	setAmountTxt();
//    	setAmountReturnWF();
//    }

//    public void setCurrentLevelTxt() {
//    	currentLevelTxt = CommonUtil.formatLevel(this.getCurrentLevel(), MarketsManagerBase.getMarket(this.getMarketId()).getDecimalPoint());
//    }

//    public void setTimeEstClosingTxt(String utcOffset) {
//    	timeEstClosingTxt = CommonUtil.getTimeAndDateFormat(timeEstClosing, utcOffset);
//    }

//    public void setAsset(Locale locale) {
//        asset = CommonUtil.getMessage(locale, MarketsManagerBase.getMarket(this.getMarketId()).getDisplayNameKey(), null);
//    }

//    public void setLevel() {
//        level = CommonUtil.formatLevel(this.getCurrentLevel(), MarketsManagerBase.getMarket(this.getMarketId()).getDecimalPoint());
//    }

//    public void setExpiryLevel() {
//    	Market m = MarketsManagerBase.getMarket(this.getMarketId());
//        expiryLevel = CommonUtil.formatLevel(this.getClosingLevel(), m.getDecimalPoint() - m.getDecimalPointSubtractDigits());
//    }

//    public void setTimePurchased(String utcOffset) {
//    	if (null == this.getTimeCreated()) {
//    		timePurchased = null;
//    	}
//        timePurchased = Calendar.getInstance(TimeZone.getTimeZone("GMT"));
//        timePurchased.setTime(this.getTimeCreated());
//        timePurchaseTxt = CommonUtil.getTimeAndDateFormat(timePurchased.getTime(), utcOffset);
//    }
//
//    public void setAmountTxt() {
//        amountTxt = CommonUtil.formatCurrencyAmount(this.getAmount(), true, this.getCurrencyId());
//    }

//    public void setAmountReturnWF() {
//        if (null != this.getTimeSettled()) {
//            amountReturnWF = CommonUtil.formatCurrencyAmount(this.getAmount() + this.getWin() - this.getLose(), true, this.getCurrencyId());
//        } else {
//        	amountReturnWF = "";
//        }
//    }


//	public double getBonusLoseOdds() {
//		return bonusLoseOdds;
//	}
//
//	public void setBonusLoseOdds(double bonusLoseOdds) {
//		this.bonusLoseOdds = bonusLoseOdds;
//	}

//	public Date getTimeQuoted() {
//		return timeQuoted;
//	}
//
//	public void setTimeQuoted(Date timeQuoted) {
//		this.timeQuoted = timeQuoted;
//	}



//	public int getBonusOddsChangeTypeId() {
//		return bonusOddsChangeTypeId;
//	}
//
//	public void setBonusOddsChangeTypeId(int bonusOddsChangeTypeId) {
//		this.bonusOddsChangeTypeId = bonusOddsChangeTypeId;
//	}

//	/**
//	 * @return the bonusWinOdds
//	 */
//	public double getBonusWinOdds() {
//		return bonusWinOdds;
//	}
//
//	/**
//	 * @param bonusWinOdds the bonusWinOdds to set
//	 */
//	public void setBonusWinOdds(double bonusWinOdds) {
//		this.bonusWinOdds = bonusWinOdds;
//	}

//	public String getOnlyTimeEstClosingTxt() {
//        return CommonUtil.getTimeFormat(timeEstClosing, utcOffsetCreated);
//    }

//	public String getAmountWithoutFeesTxt() {
//		return CommonUtil.formatCurrencyAmount(getAmountWithoutFees(), currencyId);
//	}

//	public String getInsuranceAmountRUTxt() {
//		return CommonUtil.formatCurrencyAmount(insuranceAmountRU, currencyId);
//	}

//	public String getBonusName(Locale locale) {
//		if (bonusOddsChangeTypeId > 0) {
//			return CommonUtil.getMessage(locale, bonusDisplayName, null);
//		}
//		return null;
//	}
//
//    public String getInsuranceAmountGMTxt() {
//        return CommonUtil.formatCurrencyAmount(insuranceAmountGM, currencyId);
//    }
//
//    public String getOptionPlusFeeTxt() {
//   	 return CommonUtil.formatCurrencyAmount(optionPlusFee, currencyId);
//   }
//
//    /**
//     * @return the isOptionPlus
//     */
//    public boolean isOptionPlusOpportunityTypeId() {
//        return (opportunityTypeId == Opportunity.TYPE_OPTION_PLUS);
//    }
//
//	public String getRefundUpTxt() {
//		return CommonUtil.formatCurrencyAmount(getRefundUp(), currencyId);
//	}
//
//	public String getRefundDownTxt() {
//		return CommonUtil.formatCurrencyAmount(getRefundDown(), currencyId);
//	}
//
//	/**
//	 * @return the isHaveHourly
//	 */
//	public boolean isHaveHourly() {
//		return isHaveHourly;
//	}
//
//	/**
//	 * @param isHaveHourly the isHaveHourly to set
//	 */
//	public void setHaveHourly(boolean isHaveHourly) {
//		this.isHaveHourly = isHaveHourly;
//	}

//	public long getDefaultAmountValue() {
//		return defaultAmountValue;
//	}
//
//	public void setDefaultAmountValue(long defaultAmountValue) {
//		this.defaultAmountValue = defaultAmountValue;
//	}

//	/**
//	 * @return the copyopInvId
//	 */
//	public long getCopyopInvId() {
//		return copyopInvId;
//	}
//
//	/**
//	 * @param copyopInvId the copyopInvId to set
//	 */
//	public void setCopyopInvId(long copyopInvId) {
//		this.copyopInvId = copyopInvId;
//	}

//	public boolean isLikeHourly() {
//		return isLikeHourly;
//	}
//
//	public void setLikeHourly(boolean isLikeHourly) {
//		this.isLikeHourly = isLikeHourly;
//	}
//}