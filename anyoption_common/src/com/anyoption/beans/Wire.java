//package com.anyoption.beans;
//
//
//public class Wire extends com.anyoption.beans.base.Wire {
//    public Wire() {
//        id = 0;
//        beneficiaryName = "";
//        swift = "";
//        iban = "";
//        accountNum = "";
//        bankNameTxt = "";
//        amount = "";
//        bankId = "";
//        branch = "";
//        accountName = "";
//    }
//    
//    public Wire(com.anyoption.beans.base.Wire w) {
//        this.id = w.getId();
//        this.bankId = w.getBankId();
//        this.branch = w.getBranch();
//        this.accountNum = w.getAccountNum();
//        this.accountName = w.getAccountName();
//        this.accountInfo = w.getAccountInfo();
//        this.deposit = w.getDeposit();
//
//        this.beneficiaryName = w.getBeneficiaryName();
//        this.swift = w.getSwift();
//        this.iban = w.getIban();
//        this.bankNameTxt = w.getBankNameTxt();
//
//        this.branchAddress = w.getBranchAddress();
//        this.amount = w.getAmount();
//        this.bankCode = w.getBankCode();
//
//        this.accountType = w.getAccountType();
//        this.checkDigits = w.getCheckDigits();
//        this.bankFeeAmount = w.getBankFeeAmount();
//        this.branchName = w.getBranchName();
//    }
//
//    public String getBankName() {
//        if (null == bankId) {
//            return "";
//        }
//        return Bank.getBank(new Long(bankId)).getName();
//    }
//
//    public String toString() {
//        String ls = System.getProperty("line.separator");
//        return ls + "Wire" + ls
//            + super.toString() + ls
//            + "id: " + id + ls
//            + "bankId: " + bankId + ls
//            + "branch: " + branch + ls
//            + "accountNum: " + accountNum + ls
//            + "accountName: " + accountName + ls
//            + "accountInfo: " + accountInfo + ls
//            + "deposit: " + deposit + ls
//            + "beneficiaryName: " + beneficiaryName + ls
//            + "swift: " + swift + ls
//            + "iban: " + iban + ls
//            + "accountNumber: " + accountNum + ls
//            + "amount: " + amount + ls
//            + "branchName: " + branchName + ls;
//    }
//}