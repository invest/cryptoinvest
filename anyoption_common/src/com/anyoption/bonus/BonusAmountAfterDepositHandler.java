//package com.anyoption.bonus;
//
//
//import java.sql.Connection;
//import java.sql.SQLException;
//
//import org.apache.log4j.Logger;
//
//import com.anyoption.common.beans.Bonus;
//import com.anyoption.common.beans.InvestmentBonusData;
//import com.anyoption.common.beans.base.BonusUsers;
//import com.anyoption.common.bl_vos.BonusCurrency;
//import com.anyoption.common.bonus.BonusHandlerBase;
//import com.anyoption.common.bonus.BonusHandlersException;
//import com.anyoption.common.bonus.BonusUtil;
//import com.anyoption.daos.BonusDAOBase;
//import com.anyoption.managers.BonusManagerBase;
//import com.anyoption.util.CommonUtil;
//
//public class BonusAmountAfterDepositHandler extends BonusHandlerBase {
//
//	private static final Logger log = Logger.getLogger(BonusAmountAfterDepositHandler.class);
//
//	/**
//	 *  bonusInsert event handler implementation
//	 */
//	@Override
//	public boolean bonusInsert(Connection conn,BonusUsers bu, Bonus b, BonusCurrency bc, long userId, long currencyId, long writerId, long popEntryId, int bonusPopLimitTypeId, boolean isNeedToSendInternalMail) throws BonusHandlersException{
//		boolean res = false;
//		bu.setBonusStateId(Bonus.STATE_PENDING);
//
//		try{
//			BonusManagerBase.getLimitsForBonus(conn,bu, b, bc, popEntryId,bonusPopLimitTypeId, userId, currencyId, false);
//
//			bu.setSumInvWithdrawal(bc.getBonusAmount() * b.getWageringParameter());
//
//			res = BonusDAOBase.insertBonusUser(conn, bu, b, bc, popEntryId, isNeedToSendInternalMail);
//
//		} catch (SQLException e) {
//			throw new BonusHandlersException("Error in inserting bonus" , e);
//		}
//		return res;
//	}
//
//	/**
//	 *  getBonusMessage event handler implementation
//	 */
//	@Override
//	public String getBonusMessage(Bonus b, BonusCurrency bc, long popEnteyId, int bonusPopLimitTypeId, long userId, long bonusAmount, long sumInvQualify, long sumDeposit) throws BonusHandlersException{
//		String out = "";
//
//		try{
//			long bonusId = b.getId();
//			String descMsg = BonusManagerBase.getBonusDescription(bonusId, 1); // state could only be 1 - granted
//
//			if(b.getNumberOfActions()>1) {
//				out+="Make " + b.getNumberOfActions() +" deposits and get " + bc.getBonusAmount()/100 + " to your account<br/>";
//			}
//
//			if (null != bc){
//				long currencyId = bc.getCurrencyId();
//				BonusUsers limitsBu = new BonusUsers();
//
//				limitsBu.setBonusId(bonusId);
//				BonusManagerBase.getLimitsForBonus(limitsBu, b, bc, popEnteyId, bonusPopLimitTypeId,userId, currencyId, false);
//				long minDepositAmount = limitsBu.getSumDeposits();
//
//				if(!(b.getNumberOfActions()>1)){
//					out += com.anyoption.bonus.BonusHandlerBase.getBonusDescription(bc.getBonusAmount()/100,  bc.getBonusPercent(),bc.getSumInvestQualify()/100,
//							b.getNumberOfActions(),descMsg,String.valueOf(bc.getMinDepositAmount()/100),String.valueOf(bc.getMaxDepositAmount()/100),
//							String.valueOf(bc.getMinInvestAmount()/100), bc.getMaxInvestAmount(), String.valueOf((int)(b.getOddsWin()*100)),
//							String.valueOf((int)(b.getOddsLose()*100)), currencyId) +  " <br/>";
//				}
//
//				out += "Wagering parameter " + b.getWageringParameter() + " on the " + b.getCondition() +  " <br/>";
//
//				if (minDepositAmount > 0){
//					out += "<br/>" +
//					CommonUtil.getMessage(null,"bonus.population.grant.msg", new String[] {CommonUtil.formatCurrencyAmount(minDepositAmount, true, currencyId)});
//				}
//			}else{
//				throw new BonusHandlersException("No BonusCurrency for bonus id " + bonusId);
//			}
//
//			out += "Wagering parameter "+b.getWageringParameter()+" ";
//			out += "on the "+b.getCondition();
//
//		} catch (SQLException e) {
//			throw new BonusHandlersException("Error in creating bonus message" , e);
//		}
//
//		return out;
//	}
//
//	/**
//	 *  isActivateBonus event handler implementation
//	 */
//	@Override
//	public boolean isActivateBonusAfterTransactionSuccess(Connection conn, BonusUsers bu, long amount, long transactionId) throws BonusHandlersException{
//
//		if(bu.getMinDepositAmount() <= amount){
//			return true;
//		}
//		return false;
//	}
//
//	/**
//	 *  activateBonusAfterTransactionSuccess event handler implementation
//	 */
//	@Override
//	public void activateBonusAfterTransactionSuccess(Connection conn, BonusUsers bu, long transactionId, long userId, long amount, long writerId) throws BonusHandlersException{
//
//    	try {
//			if (bu.getNumOfActionsReached() + 1 == bu.getNumOfActions() ) {
//
//	            try {
//	                conn.setAutoCommit(false);
//
//	                activateBonusAndAddToBalance(conn, bu, userId, writerId, transactionId, 0);
////		            TransactionsDAOBase.setTransactionBonuUsersId(conn, transactionId, bu.getId());
//
//		            conn.commit();
//
//	            } catch (SQLException sqle) {
//	                log.error("Can't set connection back to autocommit.", sqle);
//	                try {
//	                    conn.rollback();
//	                } catch (SQLException sqlie) {
//	                    log.error("Can't rollback.", sqlie);
//	                }
//	                throw sqle;
//	            } finally {
//	            	conn.setAutoCommit(true);
//	            }
//	        } else {
//        		BonusDAOBase.addBonusUsersAction(conn, bu.getId());
//	        }
//    	} catch (SQLException sqle) {
//            throw new BonusHandlersException("Error in activateBonusAfterTransactionSuccess ",sqle);
//        }
//
//	}
//
//	/**
//	 *  touchBonusesAfterInvestmentSuccess event handler implementation
//	 */
//	@Override
//	public long touchBonusesAfterInvestmentSuccess(Connection conn, BonusUsers bu, long amountLeft) throws BonusHandlersException{
//		try {
//			BonusDAOBase.useBonusUsers(conn, bu.getId());
//		} catch (SQLException e) {
//			throw new BonusHandlersException("Error in touchBonusesAfterInvestmentSuccess ",e);
//		}
//        return (amountLeft - bu.getBonusAmount());
//	}
//
//	/**
//	 *  cancelBonusToUser event handler implementation
//	 */
//	@Override
//    public void cancelBonusToUser(Connection conn, BonusUsers bonusUser, long stateToUpdate, String utcOffset, long writerId, long skinId, String ip) throws BonusHandlersException {
//		BonusUtil.cancelBonusToUserWithdraw(conn, bonusUser, stateToUpdate, utcOffset, writerId, skinId, ip);
//    }
//
//	/**
//	 *  setBonusStateDescription event handler implementation
//	 */
//	@Override
//    public void setBonusStateDescription(BonusUsers bonusUser) throws BonusHandlersException{
//		long bonusStateId = bonusUser.getBonusStateId();
//
//		if(bonusUser.getNumOfActions()>1){
//    		if(Bonus.STATE_ACTIVE == bonusStateId){
//    			bonusUser.setBonusStateDescription("bonus.state.active.description");
//    		} else if(Bonus.STATE_GRANTED == bonusStateId ||
//    				Bonus.STATE_MISSED == bonusStateId ||
//					Bonus.STATE_USED == bonusStateId ||
//					Bonus.STATE_DONE == bonusStateId){
//    			bonusUser.setBonusStateDescription("bonus.fixedAmount.numOfActions.granted.description");
//    		}
//    	}
//	}
//
//	/**
//	 *  activateBonusAfterInvestmentSuccessByInvAmount event handler implementation
//	 */
//	@Override
//	public boolean activateBonusAfterInvestmentSuccessByInvAmount(Connection conn, BonusUsers bu, long amountLeft, long userId, long investmentId, long writerId, boolean isFree) throws BonusHandlersException{
//		return false;
//	}
//
//	/**
//	 *  activateBonusAfterInvestmentSuccessByInvCount event handler implementation
//	 */
//	@Override
//    public boolean activateBonusAfterInvestmentSuccessByInvCount(Connection conn, BonusUsers bu,  boolean isInvWasCountForActivation, long investmentId) throws BonusHandlersException{
//		return isInvWasCountForActivation;
//	}
//
//	/**
//	 *  handleBonusOnSettleInvestment event handler implementation
//	 */
//	@Override
//	public long handleBonusOnSettleInvestment(Connection conn, InvestmentBonusData investment, boolean isWin) throws BonusHandlersException{
//        return 0;
//	}
//
//	/**
//	 *  getAmountThatUserCantWithdraw event handler implementation
//	 */
//	@Override
//	public long getAmountThatUserCantWithdraw(long win, long lose, long invAmount, BonusUsers bu){
//		return 0;
//	}
//}