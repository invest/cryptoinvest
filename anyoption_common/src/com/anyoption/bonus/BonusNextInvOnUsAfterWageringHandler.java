//package com.anyoption.bonus;
//
//
//import java.sql.Connection;
//import java.sql.SQLException;
//
//import com.anyoption.beans.Investment;
//import com.anyoption.common.beans.Bonus;
//import com.anyoption.common.beans.base.BonusUsers;
//import com.anyoption.common.bl_vos.BonusCurrency;
//import com.anyoption.common.bonus.BonusHandlerBase;
//import com.anyoption.common.bonus.BonusHandlersException;
//import com.anyoption.common.bonus.BonusUtil;
//import com.anyoption.daos.BonusDAOBase;
//import com.anyoption.managers.BonusManagerBase;
//import com.anyoption.util.CommonUtil;
//
//public class BonusNextInvOnUsAfterWageringHandler extends BonusHandlerBase {
//
//	/**
//	 *  getBonusMessage event handler implementation
//	 */
//	@Override
//	public String getBonusMessage(Bonus b, BonusCurrency bc, long popEnteyId, int bonusPopLimitTypeId, long userId) throws BonusHandlersException{
//		String out = "";
//
//		try {
//			long bonusId = b.getId();
//
//			String descMsg = BonusManagerBase.getBonusDescription(bonusId, 1); // state could only be 1 - granted
//
//			if (null != bc) {
//				long currencyId = bc.getCurrencyId();
//				BonusUsers limitsBu = new BonusUsers();
//
//				limitsBu.setBonusId(bonusId);
//				BonusManagerBase.getLimitsForBonus(limitsBu, b, bc, popEnteyId, bonusPopLimitTypeId,userId, currencyId, false);
//				long minDepositAmount = limitsBu.getSumDeposits();
//
//				out += BonusHandlerBase.getBonusDescription(bc.getBonusAmount()/100,bc.getBonusPercent(),bc.getSumInvestQualify()/100,
//						b.getNumberOfActions(),descMsg,String.valueOf(bc.getMinDepositAmount()/100),String.valueOf(bc.getMaxDepositAmount()/100),
//						String.valueOf(bc.getMinInvestAmount()/100), bc.getMaxInvestAmount(), String.valueOf((int)(b.getOddsWin()*100)),
//						String.valueOf((int)(b.getOddsLose()*100)), currencyId) +  " <br/>";
//
//				out += "Wagering parameter " + b.getWageringParameter() + " on the " + b.getCondition() +  " <br/>";
//
//				if (minDepositAmount >0){
//					out += "<br/>" +
//					CommonUtil.getMessage(null,"bonus.population.grant.msg", new String[] {CommonUtil.formatCurrencyAmount(minDepositAmount, true, currencyId)});
//				}
//
//			} else {
//				throw new BonusHandlersException("No BonusCurrency for bonus id " + bonusId);
//			}
//		} catch (SQLException e) {
//			throw new BonusHandlersException("Error in creating bonus message" , e);
//		}
//		return out;
//	}
//
//	/**
//	 *  isActivateBonus event handler implementation
//	 */
//	@Override
//	public boolean isActivateBonusAfterTransactionSuccess(Connection conn, BonusUsers bu, long amount, long transactionId) throws BonusHandlersException{
//
//    	long depositsSum = bu.getSumDeposits();
//
//    	// Check if requiered deposits sum > 0
//    	if (depositsSum > 0){
//
//    		if (bu.getSumDepositsReached() == 0 && amount >= depositsSum){
//    			// Add the amount of current deposit to sum deposits reached
//        		try{
//        			bu.setSumDepositsReached(amount);
//        			BonusDAOBase.addBonusUsersSumDeposits(conn, bu.getId(), amount);
////        			TransactionsDAOBase.setTransactionBonuUsersId(conn, transactionId, bu.getId());
//        		}catch (SQLException e) {
//    				throw new BonusHandlersException("can't addBonusUsersSumDeposits ", e);
//    			}
//    		}
//
//        	if (bu.getSumDepositsReached() >= depositsSum &&
//        			bu.getSumInvQualifyReached() >= bu.getSumInvQualify()){
//        		return true;
//        	}else{
//        		return false;
//        	}
//    	}
//
//    	return false;
//	}
//
//	/**
//	 *  activateBonusAfterTransactionSuccess event handler implementation
//	 */
//	@Override
//	public void activateBonusAfterTransactionSuccess(Connection conn, BonusUsers bu, long transactionId, long userId, long amount, long writerId) throws BonusHandlersException{
//    	try {
//    		BonusDAOBase.activateBonusUsers(conn, bu, transactionId, 0);
//    	} catch (SQLException sqle) {
//            throw new BonusHandlersException("can't activateBonusUsers ",sqle);
//        }
//	}
//
//	/**
//	 *  touchBonusesAfterInvestmentSuccess event handler implementation
//	 */
//	@Override
//	public long touchBonusesAfterInvestmentSuccess(Connection conn, BonusUsers bu, long amountLeft) throws BonusHandlersException{
//        return amountLeft;
//	}
//
//	/**
//	 *  wageringAfterInvestmentSuccess event handler implementation
//	 */
//	@Override
//	public long wageringAfterInvestmentSuccess(Connection conn, BonusUsers bu, long amountLeft, long investmentId) throws BonusHandlersException{
//		if (bu.getBonusStateId() != Bonus.STATE_ACTIVE){
//			amountLeft = super.wageringAfterInvestmentSuccess(conn, bu, amountLeft, investmentId);
//		}
//		return amountLeft;
//	}
//
//
//	/**
//	 *  setBonusStateDescription event handler implementation
//	 */
//	@Override
//    public void setBonusStateDescription(BonusUsers bonusUser) throws BonusHandlersException{
//		// Do Nothing
//	}
//
//	/**
//	 *  activateBonusAfterInvestmentSuccessByInvAmount event handler implementation
//	 */
//	@Override
//	public boolean activateBonusAfterInvestmentSuccessByInvAmount(Connection conn, BonusUsers bu, long amountLeft, long userId, long investmentId, long writerId, boolean isFree) throws BonusHandlersException{
//		try{
//			boolean isAmountLeftUsed = true;
//
//			if (bu.getSumInvQualify() - bu.getSumInvQualifyReached() > amountLeft) {
//		        BonusDAOBase.addBonusUserQualifyWagering(conn, bu.getId(), amountLeft);
//		    } else {
//		    	if (bu.getSumInvQualify() - bu.getSumInvQualifyReached() > 0) {
//			    	BonusDAOBase.addBonusUserQualifyWagering(conn, bu.getId(), bu.getSumInvQualify());
//				}else{
//					isAmountLeftUsed = false;
//				}
//
//		    	// check if sum deposit was reached (in case there is one)
//	        	if (bu.getSumDeposits() == 0 || bu.getSumDepositsReached() >= bu.getSumDeposits()){
//	        		BonusDAOBase.activateBonusUsers(conn, bu, 0, investmentId);
//	        	}
//		    }
//			return isAmountLeftUsed;
//		}catch (SQLException e) {
//			throw new BonusHandlersException("Error in activateBonusAfterInvestmentSuccessByInvAmount ", e);
//		}
//	}
//
//	/**
//	 *  activateBonusAfterInvestmentSuccessByInvCount event handler implementation
//	 */
//	@Override
//    public boolean activateBonusAfterInvestmentSuccessByInvCount(Connection conn, BonusUsers bu,  boolean isInvWasCountForActivation, long investmentId) throws BonusHandlersException{
//		return isInvWasCountForActivation;
//	}
//
//	/**
//	 *  handleBonusOnSettleInvestment event handler implementation
//	 */
//	@Override
//	public long handleBonusOnSettleInvestment(Connection conn, Investment investment, boolean isWin) throws BonusHandlersException{
//        return 0;
//	}
//
//	/**
//	 *  getAmountThatUserCantWithdraw event handler implementation
//	 */
//	@Override
//	public long getAmountThatUserCantWithdraw(long win, long lose, long invAmount, BonusUsers bu){
//
//		if (win > 0){
//			return win;
//		}else{
//			return lose;
//		}
//	}
//	
//	/**
//	 *  cancelBonusToUser event handler implementation
//	 */
//	@Override
//	public void cancelBonusToUser(Connection conn, BonusUsers bonusUser, long stateToUpdate, String utcOffset, long writerId, String ip, long skinId) throws BonusHandlersException {
//		BonusUtil.cancelBonusToUserWithdraw(conn, bonusUser, stateToUpdate, utcOffset, writerId, ip, skinId);
//    }
//}