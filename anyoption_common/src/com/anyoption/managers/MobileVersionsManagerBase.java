package com.anyoption.managers;

import java.sql.Connection;
import java.sql.SQLException;

import com.anyoption.beans.MobileVersion;
import com.anyoption.common.managers.BaseBLManager;
import com.anyoption.daos.MobileVersionsDAOBase;

public class MobileVersionsManagerBase extends BaseBLManager {

	/**
	 * get the app apk info
	 * @param apkName android(Et/ao) or iphone(Et/ao)
	 * @throws SQLException
	 */
    public static MobileVersion getAPK(String apkName) throws SQLException {
        Connection conn = null;
        MobileVersion mv = null;
        try {
            conn = getConnection();
            mv = MobileVersionsDAOBase.getAPK(conn, apkName);
        } finally {
            closeConnection(conn);
        }
        return mv;
    }
}