//package com.anyoption.managers;
//
//import java.sql.Connection;
//import java.sql.SQLException;
//
//import org.apache.log4j.Logger;
//
//import com.anyoption.common.beans.InvestmentRejects;
//import com.anyoption.common.managers.BaseBLManager;
//import com.anyoption.daos.InvestmentRejectsDAOBase;
//
///**
// * Investment Rejects web manager.
// *
// * @author Eyal
// */
//public class InvestmentRejectsManagerBase extends BaseBLManager {
//    protected static Logger log = Logger.getLogger(InvestmentRejectsManagerBase.class);
//    
//	public static void insertInvestmentReject(InvestmentRejects inv) {
//        Connection conn = null;
//        try {
//            conn = getConnection();
//            InvestmentRejectsDAOBase.insertInvestmentRejects(conn, inv);
//        } catch (SQLException e) {
//        	log.debug("cant create connection to DB " + e);
//        } finally {
//            closeConnection(conn);
//        }
//	}
//}