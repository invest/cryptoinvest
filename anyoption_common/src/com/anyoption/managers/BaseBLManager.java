//package com.anyoption.managers;
//
//import java.sql.Connection;
//import java.sql.SQLException;
//
//import javax.sql.DataSource;
//
//import org.apache.log4j.Logger;
//
//import com.anyoption.common.managers.DBUtil;
//
//public abstract class BaseBLManager {
//	private static final Logger logger = Logger.getLogger(BaseBLManager.class);
//
//	protected static Connection getConnection() throws SQLException{
//		DataSource ds = DBUtil.getDataSource();
//		if (ds == null) {
//			logger.error("************************************* ");
//			logger.error("TIME: GOT NULL in data source!!!!!!!! ");
//			logger.error("************************************* ");
//		}
//
//		Connection con = null;
//		con = ds.getConnection();
//		if (con == null) {
//			logger.error("************************************* ");
//			logger.error("TIME: GOT NULL in Connection!!!!!!!! ");
//			logger.error("************************************* ");
//		} else {
//            try {
//                if (!con.getAutoCommit()) {
//                    logger.error("************************************* ");
//                    logger.error("Got conn with AutoCommit false!!!!!!! ");
//                    logger.error("************************************* ");
//                    con.setAutoCommit(true);
//                }
//            } catch (SQLException sqle) {
//                logger.warn("Can't check auto commit.", sqle);
//            }
//        }
//		return con;
//	}
//    
//    /**
//     * Close db connection.
//     * 
//     * @param conn the db conn to close
//     */
//    public static void closeConnection(Connection conn) {
//        if (null != conn) {
//            try {
//                if (!conn.getAutoCommit()) {
//                    logger.error("************************************* ");
//                    logger.error("Return conn with AutoCommit false!!!! ");
//                    logger.error("************************************* ");
//                    StackTraceElement[] st = Thread.currentThread().getStackTrace();
//                    String ls = System.getProperty("line.separator");
//                    StringBuffer sb = new StringBuffer();
//                    sb.append(ls).append("Stack trace:").append(ls);
//                    for (int i = 0; i < st.length; i++) {
//                        sb
//                            .append(st[i].getClassName())
//                            .append(".")
//                            .append(st[i].getMethodName())
//                            .append("(")
//                            .append(st[i].getFileName())
//                            .append(":")
//                            .append(st[i].getLineNumber())
//                            .append(")")
//                            .append(ls);
//                    }
//                    logger.error(sb.toString());
//                    
//                    conn.setAutoCommit(true);
//                }
//            } catch (SQLException sqle) {
//                logger.warn("Can't check auto commit.", sqle);
//            }
//            try {
//                conn.close();
//            } catch (SQLException sqle) {
//                logger.error("Can't close connection.", sqle);
//            }
//        }
//    }
//}