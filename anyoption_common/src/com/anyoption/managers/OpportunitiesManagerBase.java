package com.anyoption.managers;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import com.anyoption.common.beans.Market;
import com.anyoption.common.beans.Opportunity;
import com.anyoption.common.util.OpportunityCacheBean;
import com.anyoption.daos.OpportunitiesDAOBase;
import com.anyoption.util.CommonUtil;

public class OpportunitiesManagerBase extends com.anyoption.common.managers.OpportunitiesManagerBase {
    /**
     * Get details for an opportunity that is currently running.
     *
     * @param id the id of the opportunity
     * @return <code>Opportunity</code> with the requested details
     * @throws SQLException
     */
    public static Opportunity getRunningById(long id) throws SQLException {
        Connection conn = null;
        try {
            conn = getConnection();
            return OpportunitiesDAOBase.getRunningById(conn, id);
        } finally {
            closeConnection(conn);
        }
    }

    public static ArrayList<Opportunity> getClosedOpportunities(Date date, long marketId, long marketGroupId, long skinId, int startRow, int pageSize) throws SQLException {
        ArrayList<Opportunity> l = null;
        Connection conn = null;
        Date from = null;
        Date to = null;
        try {
            conn = getConnection();
            if (null != date) {
            	//            	from = new Date(date.getTime()- 86400000); // get date of previous day
            	// set the times to zero
	            Calendar fromC = Calendar.getInstance();
	            Calendar toC = Calendar.getInstance();
	            fromC.setTime(date);
	            toC.setTime(date);
	            fromC.set(Calendar.HOUR_OF_DAY, 0);
	            fromC.set(Calendar.MINUTE, 0);
	            fromC.set(Calendar.SECOND, 0);
	            toC.set(Calendar.HOUR_OF_DAY, 23);
	            toC.set(Calendar.MINUTE, 59);
	            toC.set(Calendar.SECOND, 59);
	            from = fromC.getTime();
	            to = toC.getTime();
            }
            l = OpportunitiesDAOBase.getClosedOpportunities(conn, from, to, marketId, marketGroupId, skinId, startRow, pageSize);
        } finally {
            closeConnection(conn);
        }
        return l;
    }

    public static OpportunityCacheBean getOpportunityCacheBean(long oppId) throws SQLException {
        Connection conn = null;
        try {
            conn = getConnection();
            return OpportunitiesDAOBase.getOpportunityCacheBean(conn, oppId);
        } finally {
            closeConnection(conn);
        }
    }

    public static long getMarketCurrentOpportunity(long marketId, long opportunityTypeId) throws SQLException {
        Connection conn = null;
        try {
            conn = getConnection();
            return OpportunitiesDAOBase.getMarketCurrentOpportunity(conn, marketId, opportunityTypeId);
        } finally {
            closeConnection(conn);
        }
    }
    
    public static ArrayList<Long> getOptionPlusCurrentMarket(long skinId) throws SQLException {
        Connection conn = null;
        try {
            conn = getConnection();
            return OpportunitiesDAOBase.getOptionPlusCurrentMarket(conn, skinId);
        } finally {
            closeConnection(conn);
        }
    }
    
    public static Opportunity getNextHourlyOppToOpenByOppId(long id) throws SQLException {
        Connection conn = null;
        try {
            conn = getConnection();
            return OpportunitiesDAOBase.getNextHourlyOppToOpenByOppId(conn, id);
        } finally {
            closeConnection(conn);
        }
    }
    
    /**
     * Get current hourly opportunity by market and opportunity type id
     * @param marketId
     * @param opportunityTypeId
     * @return
     * @throws SQLException
     */
    public static Opportunity getCurrentHourlyOppByMarket(long marketId, long opportunityTypeId) throws SQLException {
        Connection conn = null;
        try {
            conn = getConnection();
            return OpportunitiesDAOBase.getCurrentHourlyOppByMarket(conn, marketId, opportunityTypeId);
        } finally {
            closeConnection(conn);
        }    	
    }
    
    /**
     * Init formatted values before serializing to JSON
     * @param l
     */
    public static void initFormattedValues(long skinId, String utcOffset, com.anyoption.common.beans.base.Opportunity op) throws SQLException {
        Market m = MarketsManagerBase.getMarket(op.getMarketId());
        op.setCloseLevelTxt( CommonUtil.formatLevel(op.getClosingLevel(), m.getDecimalPoint()));
        op.setMarketName(MarketsManagerBase.getMarketName(skinId, m.getId()));
        op.setTimeEstClosingTxt(CommonUtil.getDateAndTimeFormat(op.getTimeEstClosing(), utcOffset));
    }
}