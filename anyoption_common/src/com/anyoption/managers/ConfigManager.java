package com.anyoption.managers;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;

import com.anyoption.common.managers.BaseBLManager;
import com.anyoption.daos.ConfigDAO;

public class ConfigManager extends BaseBLManager{

	public static HashMap<Long, Long> getPriorityOfProvider() throws SQLException{
		Connection conn = getConnection();
		try{
			return ConfigDAO.getPriorityOfProvider(conn);
		} finally {
			closeConnection(conn);
		}
	}

	public static long getDaysNumForFailures() throws SQLException{
		Connection conn = getConnection();
		try{
			return ConfigDAO.getDaysNumForFailures(conn);
		} finally {
			closeConnection(conn);
		}
	}

	public static long getFailuresNumConsecutive() throws SQLException{
		Connection conn = getConnection();
		try{
			return ConfigDAO.getFailuresNumConsecutive(conn);
		} finally {
			closeConnection(conn);
		}
	}
}
