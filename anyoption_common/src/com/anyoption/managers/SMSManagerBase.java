package com.anyoption.managers;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Hashtable;

import org.apache.log4j.Logger;

import com.anyoption.beans.User;
import com.anyoption.common.beans.Issue;
import com.anyoption.common.beans.IssueAction;
import com.anyoption.common.sms.SMSException;
import com.anyoption.daos.SMSDAOBase;
import com.anyoption.sms.SMSProvider;
import com.anyoption.sms.SMSProviderConfig;

public class SMSManagerBase extends com.anyoption.common.managers.SMSManagerBase {
	private static final Logger log = Logger.getLogger(SMSManagerBase.class);
	private static Hashtable<Long, SMSProvider> smsProviders;

//	public static final long SMS_KEY_TYPE_SMS = 1;
    public static final long SMS_KEY_TYPE_USERID = 2;
    public static final long SMS_KEY_TYPE_SMARTPHONE = 3;

    public static void loadSMSProviders(Connection conn, int maxRetries) throws SMSException {
        if (null == smsProviders) {
        	smsProviders = new Hashtable<Long, SMSProvider>();
            try {
                ArrayList<SMSProviderConfig> l = SMSDAOBase.loadSMSProvidersConfigs(conn);
                SMSProviderConfig c = null;
                for (int i = 0; i < l.size(); i++) {
                    c = l.get(i);
                    c.setMaxRetries(maxRetries);
                    try {
                        Class cl = Class.forName(c.getProviderClass());
                        SMSProvider p = (SMSProvider) cl.newInstance();
                        p.setConfig(c);
                        smsProviders.put(c.getId(), p);
                    } catch (Exception e) {
                        log.error("Failed to load provider for config: " + c, e);
                    }
                }
            } catch (Exception e) {
                throw new SMSException("Error loading sms providers.", e);
            }
        }
    }

    public static void loadSMSProviders(int maxRetries) throws SMSException {
        if (null == smsProviders) {
            Connection conn = null;
            try {
                conn = getConnection();
                loadSMSProviders(conn, maxRetries);
            } catch (SMSException ce) {
                throw ce;
            } catch (Throwable t) {
                throw new SMSException("Error loading sms providers.", t);
            } finally {
                closeConnection(conn);
            }
        } else {
            log.warn("sms providers already loaded.");
        }
    }

//    public static void sendTextMessage(String sender, String senderNumber, String phone, String message, OracleConnection con, long keyValue, long keyType, long providerId, long descriptionId) throws SMSException {
//        sendMessage(SMS.TYPE_TEXT, sender, senderNumber, phone, message, null, -1, con, keyValue, keyType, providerId, descriptionId);
//    }

//    public static long sendTextMessage(String sender, String senderNumber, String phone, String message, long keyValue, long keyType, long providerId, long descriptionId, long initialStatusId) throws SMSException {
//        return sendMessage(SMS.TYPE_TEXT, sender, senderNumber, phone, message, null, -1, keyValue, keyType, providerId, descriptionId, initialStatusId);
//    }
//
//    public static void sendWAPPushMessage(String sender, String senderNumber, String phone, String message, String wapPushURL, long keyValue, long keyType, long providerId, long descriptionId, long initialStatusId) throws SMSException {
//        sendMessage(SMS.TYPE_WAP_PUSH, sender, senderNumber, phone, message, wapPushURL, -1, keyValue, keyType, providerId, descriptionId, initialStatusId);
//    }

//    public static long sendMessage(long typeId, String sender, String senderNumber, String phone, String message, String wapPushURL, long dstPort,
//    		OracleConnection conn, long keyValue, long keyType, long providerId, long descriptionId) throws SMSException {
//
//    	String ls = System.getProperty("line.separator");
//    	if(log.isTraceEnabled()){
//    		log.trace("going to add queued sms, typeId:" +  typeId + ls +
//    				"phone: " + phone + ls + "message: " + message + ls +
//    				"wapPushURL: " + wapPushURL + ls + "dstPort: " + dstPort + ls);
//    	}
//        try {
//        	 return SMSDAOBase.queueMessage(conn, typeId, sender, senderNumber, phone, message, wapPushURL, dstPort, keyValue, keyType, providerId, descriptionId);
//        } catch (Exception e) {
//            throw new SMSException("Failed to queue message.", e);
//        }
//    }

//    public static long sendMessage(long typeId, String sender, String senderNumber, String phone, String message, String wapPushURL,
//    		long dstPort, long keyValue, long keyType, long providerId, long descriptionId, long initialStatusId) throws SMSException {
//        Connection conn = null;
//        try {
//            conn = getConnection();
//            return sendMessage(typeId, sender, senderNumber, phone, message, wapPushURL, dstPort, conn, keyValue, keyType, providerId, descriptionId, initialStatusId);
//        } catch (Exception e) {
//            throw new SMSException("Failed to queue message.", e);
//        } finally {
//            closeConnection(conn);
//        }
//    }

    /**
     * Get smsProvider by id
     * @param providerId
     * @return
     */
    public static SMSProvider getProviderById(long providerId) {
    	return smsProviders.get(new Long(providerId));
    }

    /**
     * Close SMS providers. Iterate over all initialized SMS providers and tell them
     * to release all resources they use. Needed when SMS provider keep permanently
     * opened connections for communication.
     */
    public static void closeSMSProviders() {
        if (null != smsProviders) {
            for (SMSProvider p : smsProviders.values()) {
                p.close();
            }
            log.info("SMSProviders closed.");
        }
    }
    
	/* Merged code */
	// public static void insertIssueForSMS(User user, Date timeSend, String msg, long smsId) {
	// Connection conn = null;
	// Issue issue = new Issue();
	// issue.setSubjectId(String.valueOf(IssuesManagerBase.ISSUE_SUBJECT_REGISTRATION_SMS));
	// issue.setStatusId(String.valueOf(IssuesManagerBase.ISSUE_STATUS_CLOSED));
	// issue.setPriorityId(String.valueOf(IssuesManagerBase.ISSUE_PRIORITY_LOW));
	// issue.setUserId(user.getId());
	// issue.setContactId(user.getContactId());
	// //issue.setType(ConstantsBase.ISSUE_TYPE_REGULAR);
	// // Filling action fields
	// IssueAction issueAction = new IssueAction();
	// issueAction.setChannelId(String.valueOf(IssuesManagerBase.ISSUE_CHANNEL_SMS)); //SMS
	// issueAction.setActionTime(timeSend);
	// issueAction.setActionTypeId(String.valueOf(IssuesManagerBase.ISSUE_ACTION_SMS));
	// issueAction.setSignificant(false);
	// issueAction.setComments(msg);
	// issueAction.setSmsId(smsId);
	// issueAction.setWriterId(user.getWriterId());
	// issueAction.setCallDirectionId(IssueAction.ISSUE_DIRECTION_OUT_BOUND_PHONE_CALL);
	// issueAction.setActionTimeOffset(user.getUtcOffset());
	//
	// try{
	// // Insert Issue & action
	// conn = getConnection();
	// com.anyoption.common.managers.IssuesManagerBase.insertIssueAndIssueAction(conn, issue, issueAction);
	// } catch(SQLException e) {
	// log.error("Enable to insert Issue for user with id : " + user.getId(), e);
	// }finally{
	// closeConnection(conn);
	// }
	//
	// }
}