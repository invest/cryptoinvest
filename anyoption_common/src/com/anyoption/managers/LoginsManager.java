package com.anyoption.managers;

import java.sql.Connection;
import java.sql.SQLException;

import com.anyoption.daos.LoginsDAO;

public class LoginsManager extends com.anyoption.common.managers.LoginsManager {	

	public static boolean logOut(long loginId) throws SQLException {
		Connection conn = getConnection();
		boolean res;
		try {
			res = LoginsDAO.logOut(conn, loginId);
		} finally {
			closeConnection(conn);
		}
		return res;
	}
}