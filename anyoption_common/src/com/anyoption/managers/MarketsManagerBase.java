package com.anyoption.managers;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.Market;
import com.anyoption.common.beans.TreeItem;
import com.anyoption.common.beans.base.MarketGroup;
import com.anyoption.daos.MarketsDAOBase;

public class MarketsManagerBase extends com.anyoption.common.managers.MarketsManagerBase {

	private static final Logger log = Logger.getLogger(MarketsManagerBase.class);
	private static HashMap<Long, String> expLevelCache;

    public static ArrayList<Market> getChartsUpdaterMarkets() throws SQLException {
        Connection conn = null;
        try {
            conn = getConnection();
            return MarketsDAOBase.getChartsUpdaterMarkets(conn);
        } finally {
            closeConnection(conn);
        }
    }

//    public static ArrayList<TreeItem> getClosedOpportunitiesMarketsAnyoption(long skinId) throws SQLException {
//        Connection conn = null;
//        try {
//            conn = getConnection();
//            return MarketsDAOBase.getClosedMarketsAnyoption(conn, skinId);
//        } finally {
//            closeConnection(conn);
//        }
//    }

//    public static ArrayList<TreeItem> getTreeItemsAnyoption(long skinId) throws SQLException {
//        Connection conn = null;
//        try {
//            conn = getConnection();
//            return MarketsDAOBase.getTreeItems(conn, skinId);
//        } finally {
//            closeConnection(conn);
//        }
//    }

//	public static Hashtable<Long, MarketGroup> getSkinGroupsMarkets(long skinId) throws SQLException {
//		Connection conn = null;
//        try {
//            conn = getConnection();
//            return MarketsDAOBase.getSkinGroupsMarkets(conn, skinId);
//        } finally {
//            closeConnection(conn);
//        }
//	}

	public static ArrayList<com.anyoption.common.beans.base.Market> getSkinMarketsListSorted(long skinId, long marketTypeId) throws SQLException {
		Connection conn = null;
		ArrayList<com.anyoption.common.beans.base.Market> list = null;
        try {
            conn = getConnection();
            list = MarketsDAOBase.getSkinMarketsListSorted(conn, skinId, marketTypeId);
        } finally {
            closeConnection(conn);
        }
        return list;
	}

	public static String getMarketAssetIndexExpLevelCalculation(long marketId)throws SQLException {

		String displayTxt = null;
		HashMap<Long, String> expLevel = getExpLevelCache();
		displayTxt = expLevel.get(marketId);
		return displayTxt;
	}

	/**
	 * @return the expLevelCache
	 * @throws SQLException
	 */
	private static HashMap<Long, String> getExpLevelCache() throws SQLException {
		if(expLevelCache==null){

			Connection conn = null;
			try {
				conn = getConnection();
				expLevelCache = MarketsDAOBase.getMarketAssetIndexExpLevelCalculation(conn);
			} finally {
				closeConnection(conn);
			}
		}
		return expLevelCache;
	}

}