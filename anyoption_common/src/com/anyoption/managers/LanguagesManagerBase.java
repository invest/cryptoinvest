//package com.anyoption.managers;
//
//import java.sql.Connection;
//import java.sql.SQLException;
//import java.util.Hashtable;
//
//import org.apache.log4j.Logger;
//
//import com.anyoption.common.beans.base.Language;
//import com.anyoption.common.daos.LanguagesDAOBase;
//import com.anyoption.common.managers.BaseBLManager;
//
//public class LanguagesManagerBase extends BaseBLManager {
//	private static final Logger log = Logger.getLogger(LanguagesManagerBase.class);
//	
//	protected static Hashtable<Long, Language> languages;
//	
//	 private static Hashtable<Long, Language> getAllLanguages() throws SQLException {
//		 Connection conn = null;
//		 try {
//			 conn = getConnection();
//			 return LanguagesDAOBase.getAllLanguages(conn);
//		 } finally {
//			 closeConnection(conn);
//		 }
//	 }
//	
//    public static Language getLanguage(long id) {
//    	getLanguages();
//        return languages.get(id);
//    }
//    
//    public static Hashtable<Long, Language> getLanguages() {
//        if (null == languages) {
//            try {
//                languages = LanguagesManagerBase.getAllLanguages();
//            } catch (Exception e) {
//                log.error("Can't load languages.", e);
//            }
//        }
//        return languages;
//    }
//    
//}