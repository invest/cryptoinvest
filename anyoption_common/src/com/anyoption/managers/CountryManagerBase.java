//package com.anyoption.managers;
//
//import java.sql.Connection;
//import java.sql.SQLException;
//import java.util.Hashtable;
//import java.util.Iterator;
//
//import org.apache.log4j.Logger;
//
//import com.anyoption.common.beans.Country;
//import com.anyoption.common.managers.BaseBLManager;
//import com.anyoption.daos.CountryDAOBase;
//
//public class CountryManagerBase extends BaseBLManager {
//	
//	private static final Logger log = Logger.getLogger(CountryManagerBase.class);
//	private static Hashtable<Long, Country> countries;
//	private static Hashtable<Long, Country> allowedCountries;
//	private static Hashtable<String, Country> countryByA3Code;
//	
//	/**
//	 * @param id
//	 * @return
//	 */
//	public static Country getCountry(long id) {
//		getCountries();
//	    return countries.get(id);
//	}
//	
//	/**
//	 * @return
//	 */
//	public static Hashtable<Long, Country> getCountries() {
//		if (null == countries) {
//	        try {
//	            countries = CountriesManagerBase.getAll();
//	            countryByA3Code = new Hashtable<String, Country>();
//	            Country c = null;
//				for (Iterator<Long> i = countries.keySet().iterator(); i.hasNext();) {
//					Long countryId = i.next();
//					c = countries.get(countryId); 
//					countryByA3Code.put(c.getA3(), c);
//				}
//	        } catch (SQLException sqle) {
//	            log.error("Can't load countries.", sqle);
//	        }
//	    }
//		return countries;
//	}
//	
//	public static Hashtable<Long, Country> getAllowedCountries() {
//		if (null == allowedCountries) {
//	        try {
//	        	allowedCountries = CountriesManagerBase.getAllAllowedCountries();
//	            countryByA3Code = new Hashtable<String, Country>();
//	            Country c = null;
//				for (Iterator<Long> i = allowedCountries.keySet().iterator(); i.hasNext();) {
//					Long countryId = i.next();
//					c = allowedCountries.get(countryId); 
//					countryByA3Code.put(c.getA3(), c);
//				}
//	        } catch (SQLException sqle) {
//	            log.error("Can't load countries.", sqle);
//	        }
//	    }
//		return allowedCountries;
//	}
//	
//	/**
//	 * @param a2
//	 * @return
//	 */
//	public static Country getCountry(String a2) {
//	    getCountries();
//        // TODO: if this is used a lot we should do a Map for faster lookup
//	    Country c = null;
//	    for (Iterator<Country> i = countries.values().iterator(); i.hasNext();) {
//	        c = i.next();
//	        if (c.getA2().equals(a2)) {
//	            return c;
//	        }
//	    }
//	    return null;
//	}
//	
//	/**
//	 * Get code by country id
//	 * @param countryId
//	 * @return
//	 * @throws SQLException
//	 */
//	public static String getCodeById(long countryId) throws SQLException {
//		Connection con = getConnection();
//		try {
//			return CountryDAOBase.getCodeById(con, countryId);
//		} finally {
//			closeConnection(con);
//		}
//	}
//
//	/**
//	 * Get country id by code
//	 * @param code
//	 * 		country code
//	 * @return
//	 * @throws SQLException
//	 */
//	public static long getIdByCode(String code) throws SQLException {
//		Connection con = getConnection();
//		try {
//			return CountryDAOBase.getIdByCode(con, code);
//		} finally {
//			closeConnection(con);
//		}
//	}
//
//    /**
//     * Get phone code by country id
//     * @param code
//     *      country code
//     * @return
//     * @throws SQLException
//     */
//    public static String getPhoneCodeById(long id) throws SQLException {
//        Connection con = getConnection();
//        try {
//            return CountryDAOBase.getPhoneCodeById(con, id);
//        } finally {
//            closeConnection(con);
//        }
//    }
//
//    public static Country getById(long countryId) throws SQLException {
//        Connection conn = null;
//        try {
//            conn = getConnection();
//            return CountryDAOBase.getById(conn, countryId);
//        } finally {
//            closeConnection(conn);
//        }
//    }
//
//	/**
//	 * @return the countryByCode
//	 */
//	public static Hashtable<String, Country> getCountryByA3Code() {
//		getCountries();
//		return countryByA3Code;
//	}
//
//	/**
//	 * @param countryByCode the countryByCode to set
//	 */
//	public static void setCountryByA3Code(Hashtable<String, Country> countryByA3Code) {
//		CountryManagerBase.countryByA3Code = countryByA3Code;
//	}
//    
//    
//}