package com.anyoption.managers;

import java.sql.Connection;

import org.apache.log4j.Logger;

import com.anyoption.common.managers.BaseBLManager;
import com.anyoption.daos.GeneralDAO;

public class GeneralManager extends BaseBLManager{
	private static final Logger log = Logger.getLogger(BonusManagerBase.class);
	
	public static String getRunDbCheck()  {
		Connection con = null;
		long timeBefore = 0;
		long timeAfter = 0;

		try {
			 timeBefore = System.currentTimeMillis();
			 con = getConnection();
			 GeneralDAO.getDB(con);
			 timeAfter = System.currentTimeMillis();
			 log.debug("Db check: ok, time: " + String.valueOf(timeAfter-timeBefore) + "ms");
			 return "dbok";

		} catch (Exception e) {
			log.debug("Db check: not ok, time: " + String.valueOf(timeAfter-timeBefore) + "ms");

		} finally {
			try {
			closeConnection(con);
			} catch (Exception e){}
		}
		return "";
	}
}
