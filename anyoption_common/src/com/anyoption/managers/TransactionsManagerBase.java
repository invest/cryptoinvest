package com.anyoption.managers;


import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.net.SocketTimeoutException;
import java.net.URLDecoder;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import com.anyoption.beans.Limit;
import com.anyoption.beans.User;
import com.anyoption.bl_vos.ErrorCode;
import com.anyoption.common.beans.CreditCard;
import com.anyoption.common.beans.LimitationDeposits;
import com.anyoption.common.beans.Transaction;
import com.anyoption.common.beans.Writer;
import com.anyoption.common.beans.base.BaroPayRequest;
import com.anyoption.common.beans.base.BonusUsers;
import com.anyoption.common.beans.base.QuestionnaireUserAnswer;
import com.anyoption.common.beans.base.Skin;
import com.anyoption.common.beans.base.ThreeDParams;
import com.anyoption.common.bl_vos.ClearingRoute;
import com.anyoption.common.bl_vos.PopulationEntryBase;
import com.anyoption.common.bl_vos.TransactionBin;
import com.anyoption.common.bl_vos.WireBase;
import com.anyoption.common.bonus.BonusHandlerBase;
import com.anyoption.common.bonus.BonusHandlerFactory;
import com.anyoption.common.bonus.BonusHandlersException;
import com.anyoption.common.clearing.CDPayInfo;
import com.anyoption.common.clearing.ClearingException;
import com.anyoption.common.clearing.ClearingInfo;
import com.anyoption.common.clearing.ClearingManagerBase;
import com.anyoption.common.clearing.DeltaPayInfo;
import com.anyoption.common.clearing.InatecClearingProvider;
import com.anyoption.common.clearing.InatecInfo;
import com.anyoption.common.clearing.Powerpay21ClearingProvider;
import com.anyoption.common.daos.BaroPayDAOBase;
import com.anyoption.common.daos.CreditCardsDAOBase;
import com.anyoption.common.daos.QuestionnaireDAOBase;
import com.anyoption.common.daos.TransactionsDAOBase;
import com.anyoption.common.daos.WiresDAOBase;
import com.anyoption.common.managers.CountryManagerBase;
import com.anyoption.common.managers.InvestmentsManagerBase;
import com.anyoption.common.managers.LimitationDepositsManagerBase;
import com.anyoption.common.managers.RewardUserTasksManager;
import com.anyoption.common.managers.RewardUserTasksManager.TaskGroupType;
import com.anyoption.common.managers.SendStolenCCDepositEmail;
import com.anyoption.common.managers.UsersAutoMailDaoManagerBase;
import com.anyoption.common.managers.WritersManagerBase;
import com.anyoption.common.util.AppsflyerEventSender;
import com.anyoption.common.util.MessageToFormat;
import com.anyoption.common.util.Sha1;
import com.anyoption.daos.BinBlackListDAOBase;
import com.anyoption.daos.BonusDAOBase;
import com.anyoption.daos.CcBlackListDAOBase;
import com.anyoption.daos.GeneralDAO;
import com.anyoption.daos.LimitsDAOBase;
import com.anyoption.daos.UsersDAOBase;
import com.anyoption.distribution.Distribution;
import com.anyoption.distribution.DistributionMaker;
import com.anyoption.distribution.DistributionType;
import com.anyoption.distribution.MobileSupportSelector;
import com.anyoption.distribution.Selector;
import com.anyoption.distribution.SelectorException;
import com.anyoption.util.CommonUtil;
import com.anyoption.util.ConstantsBase;
import com.anyoption.util.SendDepositErrorEmail;

public class TransactionsManagerBase extends com.anyoption.common.managers.TransactionsManagerBase {
    private static final Logger log = Logger.getLogger(TransactionsManagerBase.class);



    /**
     * Insert admin deposit
     * This function written for calcalist game users that should get virtual money
     * @return
     * @throws SQLException
     */
    public static void insertAdminDeposit(User user, int transTypeId, String utcOffset, double amount, long writerId) throws SQLException {
        if (log.isInfoEnabled()) {
            log.info("Insert Admin deposit - user: " + user.getUserName() + " amount: " + amount);
        }
        Connection conn = null;
        try {
            Transaction tran = new Transaction();
            tran.setAmount(CommonUtil.calcAmount(amount));
            tran.setComments("virtual game money");
            tran.setCreditCardId(null);
            tran.setDescription(null);
            tran.setIp(user.getIp());
            tran.setProcessedWriterId(writerId);
            tran.setChequeId(null);
            tran.setTimeSettled(new Date());
            tran.setTimeCreated(new Date());
            tran.setUtcOffsetCreated(utcOffset);
            tran.setUtcOffsetSettled(utcOffset);
            tran.setTypeId(transTypeId);
            tran.setUserId(user.getId());
            tran.setWriterId(writerId);
            tran.setWireId(null);
            tran.setChargeBackId(null);
            tran.setStatusId(TransactionsManagerBase.TRANS_STATUS_SUCCEED);
            tran.setReceiptNum(null);

            conn = getConnection();
            conn.setAutoCommit(false);
            UsersDAOBase.addToBalance(conn, user.getId(), tran.getAmount());
            TransactionsDAOBase.insert(conn, tran);
            UsersDAOBase.insertBalanceLog(conn, writerId, tran.getUserId(), ConstantsBase.TABLE_TRANSACTIONS,
                    tran.getId(), ConstantsBase.LOG_BALANCE_ADMIN_DEPOSIT, utcOffset);
            conn.commit();

            if (log.isInfoEnabled()) {
                log.info("Admin deposit inserted successfully!");
            }
        } catch (SQLException sqle) {
            try {
                conn.rollback();
            } catch (SQLException e) {
                log.error("Can't rollback.", e);
            }
            throw sqle;
        } finally {
            conn.setAutoCommit(true);
            closeConnection(conn);
        }
    }

	public static boolean insertBonusDeposit(Connection con, User user,long bonusUserId, long writerId, long amount,
			String comments, String ip, long loginId) throws SQLException {

		if (log.isEnabledFor(Level.INFO)) {
			String ls = System.getProperty("line.separator");
			log.log(Level.INFO, "Insert Bonus deposit: " + ls + "user:"
				+ user.getUserName() + ls );
		}

		try {
			Transaction tran = new Transaction();
			tran.setAmount(amount);
			tran.setCurrency(user.getCurrency());
			tran.setComments(comments);
			tran.setCreditCardId(null);
			tran.setDescription("log.balance.bonus.deposit");
			tran.setIp(ip);
			tran.setProcessedWriterId(writerId);
			tran.setChequeId(null);
			tran.setTimeSettled(new Date());
			tran.setTimeCreated(new Date());
			tran.setUtcOffsetCreated(user.getUtcOffset());
			tran.setUtcOffsetSettled(user.getUtcOffset());
			tran.setTypeId(TransactionsManagerBase.TRANS_TYPE_BONUS_DEPOSIT);
			tran.setUserId(user.getId());
			tran.setWriterId(writerId);
			tran.setWireId(null);
			tran.setChargeBackId(null);
			tran.setStatusId(TRANS_STATUS_SUCCEED);
			tran.setReceiptNum(null);
			tran.setBonusUserId(bonusUserId);
			tran.setLoginId(loginId);

			try {
				con.setAutoCommit(false);
				UsersDAOBase.addToBalance(con, user.getId(), amount);
				TransactionsDAOBase.insert(con, tran);
				UsersDAOBase.insertBalanceLog(con, writerId,
						tran.getUserId(), ConstantsBase.TABLE_TRANSACTIONS, tran
								.getId(), ConstantsBase.LOG_BALANCE_BONUS_DEPOSIT, user.getUtcOffset());
				con.commit();
			} catch (SQLException e) {
				log.log(Level.ERROR, "insertBonusDeposit: ", e);
				try {
					con.rollback();
				} catch (SQLException ie) {
					log.log(Level.ERROR, "Can't rollback.", ie);
				}
					throw e;
			} finally {
				try {
					con.setAutoCommit(true);
				} catch (Exception e) {
					log.error("Can't set back to autocommit.", e);
				}
			}

			//TODO: print BE success message
//			if (ConstantsBase.WRITER_ID_AUTO != writerId) {
//				String[] params = new String[1];
//				params[0] = CommonUtil.formatCurrencyAmount(tran.getAmount(), true, tran.getCurrencyId());
//				FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_INFO,
//						CommonUtil.getMessage("deposit.success", params), null);
//				context.addMessage(null, fm);
//			}

			if (log.isEnabledFor(Level.INFO)) {
				log.log(Level.INFO, "Bonus deposit inserted successfully!");
			}

		} catch (SQLException e) {
			log.log(Level.ERROR, "insertBonusDeposit: ", e);
			throw e;
		}
		return true;
	}



	public static String validateDepositForm(	Map<String, MessageToFormat> mtf, User user, long amount, CreditCard card, long writerId,
												Transaction t, boolean isCheckMinAmount) throws SQLException {
        String error = null;
        Connection conn = null;
        try {
        	 conn = getConnection();
        	 
        	 if(TransactionsDAOBase.isDisableCcDeposits(conn, user.getId())){
                 log.debug("Disable All Credit Card deposits for userID:" + user.getId());
        		 mtf.put("", new MessageToFormat("error.creditcard.notallowed", null));
                 error = ConstantsBase.ERROR_DISABLED_CC_DEPOSIT;
                 t.setDescription(error);
                 return error;
        	 }
        	 
        	 if (!card.isAllowed()) {
        		 mtf.put("", new MessageToFormat("error.creditcard.notallowed", null));
        		 if(card.isBlockedForDeposit()){
        			 error = "error.card.not.allowed.deposit";
        		 } else {
        			 error = "error.card.not.allowed";
        		 }
        		 t.setDescription(error);
             } else {
				error = (writerId != Writer.WRITER_ID_BUBBLES_DEDICATED_APP)	? validateDepositLimits(mtf, user, amount, t,
																										isCheckMinAmount)
																				: validateBDADepositLimits(	mtf, user.getCurrencyId(),
																											amount, t);
             }
                          
             if (!mtf.isEmpty()) {
            	 t.setStatusId(TRANS_STATUS_FAILED);
            	 t.setTimeSettled(new Date());
            	 t.setUtcOffsetSettled(user.getUtcOffset());
     	    }
		} finally {
			closeConnection(conn);
		}      
        return error;
    }

	public static String validateDepositLimits(
    						Map<String, MessageToFormat> mtf,
    						User user,
    						long amount,
    						Transaction t,
    						boolean isCheckMinAmount) throws SQLException {
        Connection conn = null;
        String error = null;
        try {
        	conn = getConnection();
            Limit limit = LimitsDAOBase.getById(conn, user.getLimitId());
            LimitationDeposits ld = LimitationDepositsManagerBase.getLimitByUserId(conn, user.getId());
            limit.setMinDeposit(ld.getMinimumDeposit());
            limit.setMinFirstDeposit(ld.getMinimumFirstDeposit());
            error = validateDepositLimits(mtf, conn, amount, user.getId(), user.getCurrencyId(), limit, isCheckMinAmount);
            if (error != null) {
            	t.setDescription(error);
            	return error;
            }
        } finally {
            closeConnection(conn);
        }
        return error;
    }
    
    public static String validateDepositLimits(
			    						Map<String, MessageToFormat> mtf,
			    						long skinId,
										long amount,
										long userId,
										long currencyId) throws SQLException {
    	 Connection conn = null;
         try {
         	conn = getConnection();
         	Limit limit = LimitsDAOBase.getBySkinAndCurrency(conn, skinId, currencyId);
            LimitationDeposits ld = LimitationDepositsManagerBase.getLimitByUserId(conn, userId);
            limit.setMinDeposit(ld.getMinimumDeposit());
            limit.setMinFirstDeposit(ld.getMinimumFirstDeposit());
         	if (limit != null) {
         		return validateDepositLimits(mtf, conn, amount, userId, currencyId, limit, true);
         	} else {
         		log.error("No limit for skin [" + skinId
							+ "] and currency [" + currencyId
							+ "]; user [" + userId
							+ "], amount [" + amount + "]");
         		mtf.put("amount", new MessageToFormat("no.such.limit", null));
         		return "no.such.limit";
         	}
         } finally {
             closeConnection(conn);
         }
    }
    
    private static String validateDepositLimits(
    						Map<String, MessageToFormat> mtf,
							Connection conn,
							long amount,
							long userId,
							long currencyId,
							Limit limit,
							boolean isCheckMinAmount) throws SQLException {
    	if (TransactionsDAOBase.isFirstDeposit(conn, userId, 0)) {
            limit.setMinDeposit(limit.getMinFirstDeposit());
        }

        if (amount < limit.getMinDeposit() && isCheckMinAmount) {
            mtf.put("amount", new MessageToFormat("error.creditcard.mindeposit",
                    new String[] {CommonUtil.formatCurrencyAmountAO(limit.getMinDeposit(), true, currencyId)}));
            return ConstantsBase.ERROR_MIN_DEPOSIT;
        }
        if (amount > limit.getMaxDeposit()) {
            mtf.put("amount", new MessageToFormat("error.creditcard.maxdeposit",
                    new String[] {CommonUtil.formatCurrencyAmountAO(limit.getMaxDeposit(), true, currencyId)}));
            return ConstantsBase.ERROR_MAX_DEPOSIT;
        }

        long totalToday = TransactionsDAOBase.getSummaryByDates(conn, userId, TRANS_CLASS_DEPOSIT, new Date(), new Date());
        if (totalToday + amount > limit.getMaxDepositPerDay()) {
            mtf.put("amount", new MessageToFormat("error.creditcard.deposit.maxday",
                    new String[] {CommonUtil.formatCurrencyAmountAO(limit.getMaxDepositPerDay(), true, currencyId)}));
            return ConstantsBase.ERROR_MAX_DAILY_DEPOSIT;
        }

        GregorianCalendar gc = new GregorianCalendar();
        gc.set(GregorianCalendar.DATE, 1);
        long totalMonth = TransactionsDAOBase.getSummaryByDates(conn, userId, TRANS_CLASS_DEPOSIT, gc.getTime(), new Date());
        if (totalMonth + amount > limit.getMaxDepositPerMonth()) {
            mtf.put("amount", new MessageToFormat("error.creditcard.deposit.maxmonth",
                    new String[] {CommonUtil.formatCurrencyAmountAO(limit.getMaxDepositPerMonth(), true, currencyId)}));
            return ConstantsBase.ERROR_MAX_MONTHLY_DEPOSIT;
        }
    	
    	return null;
    }

	private static String validateBDADepositLimits(Map<String, MessageToFormat> mtf, long currencyId, long amount, Transaction t) {
		Connection con;
		Limit limit;
		try {
			con = getConnection();
			limit = LimitsDAOBase.getBDADepositLimits(con, currencyId);
		} catch (SQLException e) {
			log.debug("Unable to load limit for currencyId: " + currencyId, e);
			mtf.put("", new MessageToFormat("error.card.not.allowed", null));
			t.setDescription("error.limit.unavailable");
			return "error.limit.unavailable";
		}
		if (amount < limit.getMinDeposit()) {
			mtf.put("amount",
					new MessageToFormat("error.creditcard.mindeposit",
										new String[] {CommonUtil.formatCurrencyAmountAO(limit.getMinDeposit(), true, currencyId)}));
			return ConstantsBase.ERROR_MIN_DEPOSIT;
		}
		if (amount > limit.getMaxDeposit()) {
			mtf.put("amount",
					new MessageToFormat("error.creditcard.maxdeposit",
										new String[] {CommonUtil.formatCurrencyAmountAO(limit.getMaxDeposit(), true, currencyId)}));
			return ConstantsBase.ERROR_MAX_DEPOSIT;
		}
		return null;
	}

    public static Transaction getById(long id) throws SQLException {
        Connection conn = null;
        try {
            conn = getConnection();
            return TransactionsDAOBase.getById(conn, id);
        } finally {
            closeConnection(conn);
        }
    }

    /**
     * Get user transactions by dates range
     * @param id
     * @param from
     * @param to
     * @return
     * @throws SQLException
     */
	public static ArrayList<Transaction> getTransactionsByUserAndDates(long id, Date from, Date to, Integer startRow, Integer pageSize) throws SQLException {
		Connection con = getConnection();
		try {
			String status = TRANS_STATUS_SUCCEED + "," + TRANS_STATUS_PENDING + "," + TRANS_STATUS_REQUESTED + "," + TRANS_STATUS_APPROVED + ","
				+ TRANS_STATUS_REVERSE_WITHDRAW + "," + TRANS_STATUS_CANCELED_ETRADER + "," + TRANS_STATUS_IN_PROGRESS + "," +
					TRANS_STATUS_CANCELED_FRAUDS + "," + TRANS_STATUS_CANCEL_S_DEPOSIT  + "," + TRANS_STATUS_CHARGE_BACK + "," +
					TRANS_STATUS_CANCEL_S_M + "," + TRANS_STATUS_CANCEL_M_N_R + "," + TRANS_STATUS_SECOND_APPROVAL;
			return TransactionsDAOBase.getByUserAndDates(con, id, from, to, status, TRANS_TYPE_INTERNAL_CREDIT, startRow, pageSize);
		} finally {
			closeConnection(con);
		}
	}

	/**
	 * Get pending withdrawals transactions by userId
	 * @param userId
	 * @return
	 * @throws SQLException
	 */
	public static ArrayList<Transaction> getPendingWithdraws(long userId) throws SQLException {
		Connection con = getConnection();
		try {
			String status = String.valueOf(TRANS_STATUS_REQUESTED) + "," + String.valueOf(TRANS_STATUS_APPROVED) + "," + String.valueOf(TRANS_STATUS_SECOND_APPROVAL);
			return TransactionsDAOBase.getByUserAndStatus(con, userId, status,
					TransactionsManagerBase.TRANS_CLASS_WITHDRAW, TransactionsDAOBase.ORDER_ASC);
		} finally {
			closeConnection(con);
		}
	}

    public static Map<String, MessageToFormat> reverseWithdrawsValidation(Transaction[] transactions) throws Exception  {
        Map<String, MessageToFormat> res = new HashMap<String, MessageToFormat>();
        if (null == transactions || transactions.length == 0) {
            res.put("", new MessageToFormat("error.transaction.nonselected", null));
        }
        return res;
    }

    /**
     * Load transaction data and call reverse withdrawals method
     * @return
     * @throws Exception
     */
    public static MessageToFormat reverseWithdrawsWithDateLoading(Transaction[] transactions, User user, long writerId, String ip, long loginId) throws Exception  {
        Connection con = getConnection();
        try {
            return reverseWithdraws(con, transactions, user, writerId, ip, loginId);
        } finally {
            closeConnection(con);
        }
    }

    /**
     * Reverse withdrawals action  get list of transcation and reverse each one of them
     *
     * @param transactions list for reverse
     * @param user
     * @param writerId
     * @param ip
     * @return
     * @throws Exception
     */
    public static MessageToFormat reverseWithdraws(Connection con, Transaction[] transactions, User user, long writerId, String ip, long loginId) throws Exception  {
        MessageToFormat successMsg = null;
        int totalAmount = 0;
        try {
            con.setAutoCommit(false);
            int count = 0;
            for (Transaction tran : transactions) {

                if (log.isEnabledFor(Level.INFO)) {
                        String ls = System.getProperty("line.separator");
                        log.log(Level.INFO, "Insert ReverseWithdraw: " + ls + tran.toString() + ls);
                    }

                    UsersDAOBase.addToBalance(con, user.getId(), tran.getAmount());
                    UsersDAOBase.insertBalanceLog(con, writerId, tran.getUserId(), ConstantsBase.TABLE_TRANSACTIONS, tran.getId(),
                                    ConstantsBase.LOG_BALANCE_REVERSE_WITHDRAW, user.getUtcOffset());

                    // update currenct transaction
                    long newBalance = user.getBalance() + tran.getAmount();
                    tran.setTimeSettled(new Date());
                    tran.setProcessedWriterId(writerId);
                    tran.setStatusId(TRANS_STATUS_REVERSE_WITHDRAW);
                    tran.setWriterId(writerId);
                    TransactionsDAOBase.updateReverseWithdraw(con, tran);

                    // insert reverse withdrawal transaction
                    tran.setReferenceId(new BigDecimal(tran.getId()));
                    tran.setIp(ip);
                    tran.setTypeId(TRANS_TYPE_REVERSE_WITHDRAW);
                    tran.setStatusId(TRANS_STATUS_SUCCEED);
                    tran.setTimeSettled(new Date());
                    tran.setUtcOffsetSettled(user.getUtcOffset());
                    tran.setLoginId(loginId);
                    TransactionsDAOBase.insert(con, tran);

                    if (log.isEnabledFor(Level.INFO)) {
                        String ls = System.getProperty("line.separator");
                        log.log(Level.INFO, "ReverseWithdraw insert successfully! " + ls + tran.toString());
                    }

                    user.setBalance(newBalance);
                    totalAmount += tran.getAmount();
                    count++;
            }
            con.commit();
            if (transactions.length == count) {
				PopulationEntryBase pe = PopulationsManagerBase.getPopulationUserByUserId(user.getId());
				if(pe != null && pe.getCurrPopualtionTypeId() == PopulationsManagerBase.POP_TYPE_OPEN_WITHDRAW &&
						pe.getEntryTypeId() != PopulationsManagerBase.ENTRY_TYPE_REMOVE_PERMANENTLY) {
					PopulationsManagerBase.insertIntoPopulation(0, 0, user.getId(), 
							pe.getCurrPopulationName(), pe.getAssignWriterId(), PopulationsManagerBase.POP_TYPE_RETENTION, pe);
				}
			}

            successMsg = new MessageToFormat("reverseWithdraw.success",
                    new String[]{CommonUtil.formatCurrencyAmountAO(totalAmount, true, user.getCurrencyId())});

            return successMsg;
        } catch (SQLException e) {
            log.log(Level.ERROR, "Error on reverseWithdraw:", e);
            try {
                con.rollback();
            } catch (SQLException ie) {
                log.log(Level.ERROR, "Can't rollback.", ie);
            }
            throw e;
        } finally {
            con.setAutoCommit(true);
        }
    }

    public static Transaction createTransactionObject(User user, long amount, int typeId, CreditCard cc,
    		String ip, long writerId,  String error) {
        Transaction tran = new Transaction();
        tran.setAmount(amount);
        tran.setComments("");
        tran.setCreditCardId(new BigDecimal(cc.getId()));
        tran.setCc4digit(cc.getCcNumberLast4());
        tran.setDescription(error);
        tran.setIp(ip);
        tran.setProcessedWriterId(writerId);
        tran.setChequeId(null);
        tran.setTimeCreated(new Date());
        tran.setTypeId(typeId);
        tran.setUserId(user.getId());
        tran.setWriterId(writerId);
        tran.setWireId(null);
        tran.setChargeBackId(null);
        tran.setStatusId(TRANS_STATUS_STARTED);
        tran.setUtcOffsetCreated(user.getUtcOffset());
        return tran;
    }

	public static String validateInsertDeposit(Map<String, MessageToFormat> mtf, User user, long amount, CreditCard cc, Transaction t,
			long writerId, int source) throws SQLException {
		String error = null;
	    if (null == cc) {
	        log.info("Transaction Failed: no such card");
            mtf.put("",  new MessageToFormat("error.creditcard.notallowed", null));
            error = "error.card.not.allowed";
            t.setDescription(error);
	    } else {
	    	//limits and card allowed
	    	error = validateDepositForm(mtf, user, amount, cc, writerId, t, true);
	    	if (mtf.isEmpty()) {
	    	    Connection conn = null;
	    	    try {
	    	        conn = getConnection();
	    	        if(source == ConstantsBase.BACKEND_CONST) {
		                if (CcBlackListDAOBase.isCardBlacklisted(conn, cc.getCcNumber(), true)) {
		                    log.info("Transaction Failed: card is black listed!");
		                    mtf.put("", new MessageToFormat("error.creditcard.blacklisted", null));
		                    error = ConstantsBase.ERROR_BLACK_LIST;
		                    t.setDescription(error);
		                } else if (BinBlackListDAOBase.isBinBlacklisted(conn, String.valueOf(cc.getCcNumber()).substring(0, 6), true, user.getSkinId())) {
		                    log.info("Transaction Failed: BIN is black listed!");
		                    mtf.put("", new MessageToFormat("error.creditcard.binblacklisted", null));
		                    error = ConstantsBase.ERROR_BIN_BLACK_LIST;
		                    t.setDescription(error);
		                }
	    	        }
                    // check deposit limits in case we don't have earlier error
                    boolean firstRealDeposit = TransactionsDAOBase.isFirstDeposit(conn, user.getId(), 0);
                    if (user.getSkinId() != Skin.SKIN_ETRADER && !SkinsManagerBase.getSkin(user.getSkinId()).isRegulated()) { // just for anyoption NON REGULATED
                        if (!user.isIdDocVerify()) { // user didn't send documents
                            HashMap<String, Long> limits = LimitsDAOBase.getDepositLimits(conn, user.getLimitId());
                            long sumDepositsUntillNow = TransactionsDAOBase.getSumDepositsForLimitation(conn, user.getId());
                            long sumDeposits = sumDepositsUntillNow + amount; // add current deposit amount
                            if (sumDeposits > limits.get(ConstantsBase.DEPOSIT_DOCS_LIMIT2)) {
                                t.setDescription(ConstantsBase.ERROR_DEPOSIT_DOCS_REQUIRED);
                                t.setComments(cc.toStringForComments());
                                mtf.put("", new MessageToFormat("error.creditcard.notallowed", null));
                                error = ConstantsBase.ERROR_DEPOSIT_DOCS_REQUIRED;
                                if (!firstRealDeposit) {
                                    if (log.isDebugEnabled()) {
                                         log.debug("send email (docs required) to the user for sum deposits bigger than "
                                                + limits.get(ConstantsBase.DEPOSIT_DOCS_LIMIT2) + " currency: " + user.getCurrencyId());
                                   }
                                    try {
                                        UsersManagerBase.sendMailTemplateFile(UsersManagerBase.TEMPLATE_DEPOSIT_DOCUMENT4, writerId, user);
                                    } catch (Exception e) {
                                        log.error("can't send email (deposit email4) for deposit! ", e);
                                    }
                                    if (sumDepositsUntillNow == limits.get(ConstantsBase.DEPOSIT_DOCS_LIMIT2)) {
                                        if (!user.isDocsRequired()) { // if it true, it's a trigger for lockAccounts job
                                            user.setDocsRequired(true);
                                            UsersDAOBase.setFlagsAfterMaxDeposit(conn, user.getId());
                                            if (log.isDebugEnabled()) {
                                                log.debug("update documents required field for user: " + user.getUserName());
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    } // TODO: add deposit failure code (required client intervention)
	    	    } finally {
	    	        closeConnection(conn);
	    	    }
    		}
	    }
	    
	    // check velocity deposits case we don't have earlier error
	    if (mtf.isEmpty()) {
	    	error = velocityCheck(user.getId());
	    	if(error != null){
	            mtf.put("", new MessageToFormat("error.creditcard.velocity.check", null));	           
	            t.setDescription(error);	    		
	    	}
	    }
	    
	    if (!mtf.isEmpty()) {
	        t.setStatusId(TRANS_STATUS_FAILED);
            t.setTimeSettled(new Date());
            t.setUtcOffsetSettled(user.getUtcOffset());
	    }
	    return error;
	}

    public static Transaction insertDeposit(Map<String, MessageToFormat> mtf, String error, User user, Transaction t, CreditCard cc, long writerId,
            int source, String authorizationCode, String formName, boolean firstNewCardChanged, long loginId, String termUrl) throws SQLException {

        if (log.isInfoEnabled()) {
            String ls = System.getProperty("line.separator");
            log.info(ls + "Insert deposit:" + ls +
                    "status: " + t.getStatusId() + ls +
                    "amount: " + t.getAmount() + ls +
                    "cardId: " + cc.getId() + ls +
                    "writer: " + writerId + ls +
                    "userId: " + user.getId() + ls +
                    "error:	 " + error + ls +
                    "source: " + source + ls +
                    "platformId: " + user.getPlatformId()+ ls);
        }

        Connection conn = getConnection();
        boolean riskAlert = false;

        try {
            TransactionsDAOBase.insert(conn, t);
            if (log.isInfoEnabled()) {
                log.info("Transaction record inserted: " + t);
            }

            String errorCode = "";
                try {
//        			if (mtf.isEmpty() && null == error  && source == ConstantsBase.WEB_CONST && !firstNewCardChanged &&
//        					isFailureRepeat(conn, t.getUserId(), t.getCreditCardId(), t.getAmount(), t.getId(), firstDeposit)) {
//        				t.setClearingProviderId(new Long(0));
//        				error = ConstantsBase.ERROR_CARD_FAILURE_REPEAT;
//        			}
                	
                	//Chech CC from Diff Countries
                	RiskAlertsManagerBase.riskAlertDepositHandlerCCFromDiffCountry(conn, t);
                	
        			if (error != null) {
        				if (error.equals(ConstantsBase.ERROR_CARD_NOT_ALLOWED_DUP_CARD)) {
        					ArrayList<com.anyoption.common.beans.base.User> users = CreditCardsDAOBase.checkCardExistence(conn, cc.getCcNumber(), cc.getUserId());
        					StringBuilder usersString = new StringBuilder();
        					if (users != null) {
        						for (com.anyoption.common.beans.base.User u: users) {
        							usersString.append(u.getUserName());
        							usersString.append(", ");
        						}
        					}
        					String usrs = usersString.substring(0, usersString.length() -1);
        					t.setComments(cc.toStringForComments() + "| duplicate Card on users:" + usrs);
        				} else if (error.equals(ConstantsBase.ERROR_CARD_FAILURE_REPEAT)) {
        					t.setComments(cc.toStringForComments() + "| card failure repeat");
        				} else {
        					t.setComments(cc.toStringForComments());
        				}

        				if (log.isInfoEnabled()) {
        					String ls = System.getProperty("line.separator");
        					log.info("Transaction Failed: " + ls + error + ls);
        				}
        				t.setClearingProviderId(new Long(0));
        				t.setStatusId(TRANS_STATUS_FAILED);
        				t.setDescription(error);
        			} else if (!cc.isAllowed()) {
        				if (log.isInfoEnabled()) {
        					String ls = System.getProperty("line.separator");
        					log.info("Transaction Failed: card not allowed!" + ls);
        				}
        				t.setClearingProviderId(new Long(0));
        				t.setStatusId(TRANS_STATUS_FAILED);
        				if(cc.isBlockedForDeposit()){
        					t.setDescription("error.card.not.allowed.deposit");
        				} else {
        					t.setDescription("error.card.not.allowed");
        				}
        				mtf.put("", new MessageToFormat("error.creditcard.notallowed", null));
        			} else if (AdminManagerBase.isBlacklisted(cc, true , user)) {
        					String ls = System.getProperty("line.separator");
        					log.debug("Transaction Failed: card is black listed!" + ls);
        				t.setClearingProviderId(new Long(0));
        				t.setStatusId(TRANS_STATUS_FAILED);
        				t.setDescription(ConstantsBase.ERROR_BLACK_LIST);
        				mtf.put("", new MessageToFormat("error.creditcard.notallowed", null));
        			} else if (t.getStatusId() != TRANS_STATUS_FAILED) { // check card clearing
        				// MERGED
        				ClearingInfo ci = depositWithReroutingBase(conn, t, user, cc, -1/*not used from json*/, writerId, authorizationCode);
        				t = TransactionsDAOBase.getById(conn, ci.getTransactionId());
        				if(ci.is3dSecure()) {
        					ThreeDParams threeDParams = new ThreeDParams();
        					threeDParams.setAcsUrl(ci.getAcsUrl());
        					threeDParams.setPaReq(ci.getPaReq());
        					threeDParams.setMD(String.valueOf(ci.getTransactionId())+";"+writerId+";"+user.getUtcOffset());
        					threeDParams.setTermUrl(termUrl);
        					threeDParams.setForm(ci.getFormData());
        					t.setThreeDParams(threeDParams);
        					t.setThreeD(true);
        				}
        			}
                    if (t.getStatusId() == TRANS_STATUS_FAILED) { // update transaction on failed cases
                        t.setTimeSettled(new Date());
                        t.setUtcOffsetSettled(user.getUtcOffset());
                        TransactionsDAOBase.update(conn, t);
                    }
                    // set description if needed
    				String description = (null == t.getDescription()) ? "" : ", description: " +  t.getDescription();
                    log.info("insert deposit finished successfully! " + t.getDescription());

                } catch (Exception exp) {
                    log.error("Exception in insert deposit! ", exp);
                }
	            if (t.getStatusId() == TRANS_STATUS_PENDING || t.getStatusId() == TRANS_STATUS_SUCCEED) {
	            	boolean firstRealDeposit = TransactionsDAOBase.isFirstDeposit(conn, user.getId(), 0);
	            	depositSuccess(user, writerId, t, error, errorCode, riskAlert, cc, mtf, firstRealDeposit, loginId);
	            } else if (t.getStatusId() == TRANS_STATUS_AUTHENTICATE) {
	                return t;
	            } else {
	            	depositFailed(user, writerId, t, error, errorCode, riskAlert, source, cc, mtf);
	            }	            
	            return t;
        } finally {
            closeConnection(conn);
        }
    }

    public static void depositSuccess(User user, long writerId, Transaction t, String error, String errorCode, boolean riskAlert, CreditCard cc, Map<String, MessageToFormat> mtf, boolean firstRealDeposit, long loginId) {
        try{
        	sendEmailByDepositLimits(user, firstRealDeposit, writerId);
        } catch (Exception e) {
            log.warn("Problem with send Email By Deposit Limits " + e);
        }
        
        try {
        	PopulationsManagerBase.deposit(user.getId(), true, writerId, t);
        } catch (Exception e) {
            log.warn("Problem with population succeed deposit event " + e);
        }
        
        try {
            afterTransactionSuccess(t.getId(), user.getId(), t.getAmount(), writerId, loginId);
        } catch (Exception e) {
            log.error("Problem processing bonuses after success transaction", e);
        }

       try {
        	RiskAlertsManagerBase.riskAlertDepositHandler(writerId, error, t, user, riskAlert, errorCode, cc.getHolderName(), firstRealDeposit, cc.isDocumentsSent(),cc.getCcNumber());
        } catch (Exception e) {
        	log.error("Problem with risk Alert handler ", e);
		}
        try {
            CommonUtil.alertOverDepositLimitByEmail(t, user);
        } catch (RuntimeException e) {
            log.error("Exception sending email alert to traders! ", e);
        }
    }
    
    public static void depositFailed(User user, long writerId, Transaction t, String error, String errorCode, boolean riskAlert, int source, CreditCard cc, Map<String, MessageToFormat> mtf) {
    	 // deposit failed
    	try {
    		try {
			 PopulationsManagerBase.deposit(user.getId(), false, writerId, t);
    		} catch (Exception e) {
    				log.error("Problem with population failed deposit event", e);
    		  }

			try {
				RiskAlertsManagerBase.riskAlertDepositHandler(writerId, error, t, user, riskAlert,
						errorCode, null, false, true, 0);
			} catch (Exception e) {
				log.error("Problem with risk Alert handler ", e);
			}

			if (null != error && error.equals(ConstantsBase.ERROR_DEPOSIT_DOCS_REQUIRED)) { // for AO deposit limits
				mtf.put("", new MessageToFormat(ConstantsBase.ERROR_DEPOSIT_DOCS_REQUIRED_MSG, null));
			}
			
			 //collect the parameters we need for velocity error support email
		if (null != error
				&& (error
						.equals(ConstantsBase.ERROR_DEPOSIT_VELOCITY_AO_NO_DOC)
						|| error.equals(ConstantsBase.ERROR_DEPOSIT_VELOCITY_AO_HAVE_DOC)
						|| error.equals(ConstantsBase.ERROR_DEPOSIT_VELOCITY_ET_NO_DOC) 
						|| error.equals(ConstantsBase.ERROR_DEPOSIT_VELOCITY_ET_HAVE_DOC))) {
					sendVelocityErrorMail(error, user.getUserName(),user.getSkinId());
		   }

			//for display error msg to repeat failure and prevent duplicate message
			if (null != error && !error.equals(ConstantsBase.ERROR_DEPOSIT_DOCS_REQUIRED) && error.equals(ConstantsBase.ERROR_CARD_FAILURE_REPEAT)) {
				mtf.put("", new MessageToFormat("error.creditcard.notallowed", null));
			}

        if (user.getClassId() != ConstantsBase.USER_CLASS_TEST && source != ConstantsBase.BACKEND_CONST) {
            String email = "deposit.error.email";
            String subject = "deposit.error.email.subject";

            if (null != t.getDescription() && t.getDescription().equals(ConstantsBase.ERROR_DEPOSIT_DOCS_REQUIRED)) { // for AO deposit limits
                email = "deposit.documents.needed.email";
                subject = "deposit.documents.needed.email.subject";
            }

            // send mail to support
            // collect the parameters we need for the email
            HashMap<String, String> params = null;
            params = new HashMap<String, String>();
            params.put(SendDepositErrorEmail.PARAM_EMAIL, CommonUtil.getConfig(email, null));
            params.put(SendDepositErrorEmail.PARAM_USER_NAME, user.getUserName());
            params.put(SendDepositErrorEmail.PARAM_USER_ID, user.getIdNum());
            params.put(SendDepositErrorEmail.PARAM_TRANSACTION_ID, String.valueOf(t.getId()));
            params.put(SendDepositErrorEmail.PARAM_CC_NUM_LAST4, t.getCc4digit());
            params.put(SendDepositErrorEmail.PARAM_AMOUNT, CommonUtil.formatCurrencyAmountAO(t.getAmount(), true, user.getCurrencyId()));

            if (user.getSkinId() == Skin.SKIN_ETRADER) {
                params.put(SendDepositErrorEmail.PARAM_DECLINE_DESCRIPTION, CommonUtil.getMessage(Locale.ENGLISH, t.getDescription(), null));
                params.put(SendDepositErrorEmail.PARAM_DECLINE_COMMENTS, t.getComments());
            } else { // put english content to AO skins
                params.put(SendDepositErrorEmail.PARAM_DECLINE_DESCRIPTION, CommonUtil.getMessage(Locale.ENGLISH, t.getDescription(), null));
                params.put(SendDepositErrorEmail.PARAM_DECLINE_COMMENTS, t.getComments());
            }

            params.put(SendDepositErrorEmail.PARAM_DATE, t.getTimeCreated() + " " + t.getUtcOffsetCreated());

            // get credit card type name
            String ccTypeName = cc.getType().getName();
            params.put(SendDepositErrorEmail.PARAM_CC_TYPE, ccTypeName);

            // Populate also server related params, in order to take out common functionality
            // Use params
            params.put(SendDepositErrorEmail.MAIL_TO, CommonUtil.getConfig(email, null));
            params.put(SendDepositErrorEmail.MAIL_SUBJECT , CommonUtil.getConfig(subject, null) + " Skin:" + CommonUtil.getMessage(Locale.ENGLISH, SkinsManagerBase.getSkin(user.getSkinId()).getDisplayName(), null));

            new SendDepositErrorEmail(params).start();
        }

		// Risk alerts
        if (user.getClassId() != ConstantsBase.USER_CLASS_TEST) {
			String emails = CommonUtil.getConfig("deposit.error.risk.alert.email", null);
			String subject = "deposit.error.email.risk.alert.subject";

			boolean isStolenCC = RiskAlertsManagerBase.isStolenCard(errorCode,t);

			if (null != error && error.equals(ConstantsBase.ERROR_CARD_NOT_ALLOWED_DUP_CARD)) {
				riskAlert = true;
			}

			if (riskAlert) {
				HashMap<String, String> params = null;
				params = new HashMap<String, String>();
				params.put(SendDepositErrorEmail.PARAM_USER_NAME, user.getUserName());
				params.put(SendDepositErrorEmail.PARAM_USER_ID, user.getIdNum());
				params.put(SendDepositErrorEmail.PARAM_TRANSACTION_ID, String.valueOf(t.getId()));
				params.put(SendDepositErrorEmail.PARAM_CC_NUM_LAST4, t.getCc4digit());
				params.put(SendDepositErrorEmail.PARAM_AMOUNT, CommonUtil.formatCurrencyAmountAO(t.getAmount(), true, user.getCurrencyId()));
				params.put(SendDepositErrorEmail.PARAM_DATE, t.getTimeCreatedTxt());

                if (user.getSkinId() == Skin.SKIN_ETRADER) {
                    params.put(SendDepositErrorEmail.PARAM_DECLINE_DESCRIPTION, CommonUtil.getMessage(Locale.ENGLISH, t.getDescription(), null));
                    params.put(SendDepositErrorEmail.PARAM_DECLINE_COMMENTS, t.getComments());
                } else { // put english content to AO skins
                    params.put(SendDepositErrorEmail.PARAM_DECLINE_DESCRIPTION, CommonUtil.getMessage(Locale.ENGLISH, t.getDescription(), null));
                    params.put(SendDepositErrorEmail.PARAM_DECLINE_COMMENTS, t.getComments());
                }

                // get credit card type name
                String ccTypeName = cc.getType().getName();
                params.put(SendDepositErrorEmail.PARAM_CC_TYPE, ccTypeName);
                params.put(SendDepositErrorEmail.MAIL_SUBJECT , CommonUtil.getConfig(subject, null) + " Skin:" + CommonUtil.getMessage(Locale.ENGLISH, SkinsManagerBase.getSkin(user.getSkinId()).getDisplayName(), null));

				String[] splitTo = emails.split(";");
				for (int i = 0; i < splitTo.length; i++) {
					params.put(SendDepositErrorEmail.PARAM_EMAIL, splitTo[i]);
					params.put(SendDepositErrorEmail.MAIL_TO, splitTo[i]);
					new SendDepositErrorEmail(params).start();
				}
			}
			
			if(isStolenCC){
				HashMap<String, String> params = null;
				params = new HashMap<String, String>();
				params.put(SendStolenCCDepositEmail.PARAM_TRANSACTION_ID, String.valueOf(t.getId()));
				params.put(SendStolenCCDepositEmail.PARAM_DATE, CommonUtil.getDateAndTimeFormat(t.getTimeCreated(), null));
				params.put(SendStolenCCDepositEmail.PARAM_CC_NUM_LAST4, t.getCc4digit());
				params.put(SendStolenCCDepositEmail.PARAM_CC_TYPE, CommonUtil.getMessage(cc.getType().getName(), null, new Locale(ConstantsBase.LOCALE_DEFAULT)));
				params.put(SendStolenCCDepositEmail.PARAM_USER_ID, user.getId() + "");
				params.put(SendStolenCCDepositEmail.PARAM_USER_COUNTRY, CommonUtil.getMessage(user.getCountryName(), null, new Locale(ConstantsBase.LOCALE_DEFAULT)));
				params.put(SendStolenCCDepositEmail.PARAM_WRITER, WritersManagerBase.getWriterName(t.getWriterId()));
				params.put(SendStolenCCDepositEmail.PARAM_DECLINE_DESCRIPTION, CommonUtil.getMessage(t.getDescription(), null, new Locale(ConstantsBase.LOCALE_DEFAULT)));
				params.put(SendStolenCCDepositEmail.PARAM_DECLINE_COMMENTS, CommonUtil.getMessage(t.getComments(), null, new Locale(ConstantsBase.LOCALE_DEFAULT)));														
				params.put(SendStolenCCDepositEmail.MAIL_SUBJECT , "Suspicious CC attempt alert:[" + user.getId() + "] [" + user.getUserName() + "] [" + CommonUtil.getMessage(Locale.ENGLISH, SkinsManagerBase.getSkin(user.getSkinId()).getDisplayName(), null) + "]");

				String[] splitTo = emails.split(";");
				for (int i = 0; i < splitTo.length; i++) {
					params.put(SendStolenCCDepositEmail.PARAM_EMAIL, splitTo[i]);
					params.put(SendStolenCCDepositEmail.MAIL_TO, splitTo[i]);
					new SendStolenCCDepositEmail(params).start();
				}							
			}
		}
    } catch (Exception e) {
        log.error("Could not send email.", e);
    }
   
    }
    
    /**
     * send warnning emails to users after deposit(TRANS_STATUS_PENDING)
     *
     * @param depositVal deposit amout
     * @param cardId user credit card id
     * @param writerId writer id - from web it's 1(or from backend writer)
     * @param user user that operate this deposit
     * @param firstDeposit true - in case it's his first deposit
     * @throws SQLException
     */
    public static void sendEmailByDepositLimits(User user, boolean firstDeposit, long writerId) throws SQLException {
        long sumDeposits = 0;
        HashMap<String, Long> limits = new HashMap<String, Long>();
        if (user.getSkinId() != Skin.SKIN_ETRADER && !SkinsManagerBase.getSkin(user.getSkinId()).isRegulated()) { // just for anyoption
            if (!user.isIdDocVerify()) { // user didn't send documents
                Connection conn = null;
                try {
                    conn = getConnection();
                    limits = LimitsDAOBase.getDepositLimits(conn, user.getLimitId());
                    sumDeposits = TransactionsDAOBase.getSumDepositsForLimitation(conn, user.getId());
                    if (firstDeposit) {
                        if (sumDeposits < limits.get(ConstantsBase.DEPOSIT_DOCS_LIMIT1)) {
                            log.debug("send email to the user for first deposit less than "
                                    + limits.get(ConstantsBase.DEPOSIT_DOCS_LIMIT1));
                            try {
                                UsersManagerBase.sendMailTemplateFile(UsersManagerBase.TEMPLATE_DEPOSIT_DOCUMENT1, writerId, user);
                            } catch (Exception e) {
                                log.error("can't send email (deposit email1) for first deposit! ", e);
                            }
                        } else if (sumDeposits >= limits.get(ConstantsBase.DEPOSIT_DOCS_LIMIT1) &&
                                sumDeposits <= limits.get(ConstantsBase.DEPOSIT_DOCS_LIMIT2)) {
                            log.debug("send email to the user for first deposit between "
                                    + limits.get(ConstantsBase.DEPOSIT_DOCS_LIMIT1) + " and " +
                                            limits.get(ConstantsBase.DEPOSIT_DOCS_LIMIT2));
                            try {
                                UsersManagerBase.sendMailTemplateFile(UsersManagerBase.TEMPLATE_DEPOSIT_DOCUMENT2, writerId, user);
                            } catch (Exception e) {
                                log.error("can't send email (deposit email2) for first deposit!", e);
                            }
                        }
                    } else { // not first deposit
                        if (sumDeposits >= limits.get(ConstantsBase.DEPOSIT_DOCS_LIMIT1) &&
                                sumDeposits <= limits.get(ConstantsBase.DEPOSIT_DOCS_LIMIT2)) {
                            log.debug("send email to the user for sum deposits between "
                                    + limits.get(ConstantsBase.DEPOSIT_DOCS_LIMIT1) + " and " +
                                            limits.get(ConstantsBase.DEPOSIT_DOCS_LIMIT2));
                            try {
                                UsersManagerBase.sendMailTemplateFile(UsersManagerBase.TEMPLATE_DEPOSIT_DOCUMENT3, writerId, user);
                            } catch (Exception e) {
                                log.error("can't send email (deposit email3) for deposit! ", e);
                            }
                        }
                    }
                } finally {
                    closeConnection(conn);
                }
            }
        }
    }

	public static void afterTransactionSuccess(long transactionId, long userId, long amount, long writerId, long loginId) throws SQLException {
		// Reward plan
		int countRealDeposit = 0;
        try {
        	countRealDeposit = TransactionsManagerBase.countRealDeposit(userId);
        	if (countRealDeposit == ConstantsBase.NUM_OF_REAL_SUCCESS_DEPOSIT) { // check if this first deposit
        		RewardUserTasksManager.rewardTasksHandler(TaskGroupType.FIRST_TIME_DEPOSIT_AMOUNT, userId,  
        				amount,  transactionId, BonusManagerBase.class, (int) writerId);
        	}
        } catch (Exception e) {
            log.error(ConstantsBase.REWARD_PLAN_LOG_PREFIX + " ERROR RewardUserTasksManager exception after success transaction", e);
        }
        
        try {
			if (countRealDeposit == ConstantsBase.NUM_OF_REAL_SUCCESS_DEPOSIT) {
				new AppsflyerEventSender(transactionId, ConstantsBase.SERVER_PIXEL_TYPE_FIRST_DEPOSIT).run();
			}
		} catch (Exception e1) {
			log.error("cant run appsflyer event sender", e1);
		}
		// Bonus
		Connection conn = null;
		try {
            conn = getConnection();
            afterTransactionSuccess(conn, transactionId, userId, amount, writerId, loginId);
		 } catch (SQLException sqle) {
	           log.error("Problem processing bonuses after transaction");
	            throw sqle;
        } finally {
            closeConnection(conn);
        }
		Transaction tr = TransactionsManagerBase.getTransaction(transactionId);
		if(tr.getClassType() == TransactionsManagerBase.TRANS_CLASS_DEPOSIT){
			//Regultion POP Documents
			popDocumentsActionCreate(userId, tr);
			UsersManagerBase.updateLastDepositOrInvestDate(userId);
			log.debug("If need notife with email manegers for reached total deposits for userId:" + userId);
			UsersAutoMailDaoManagerBase.notifeReachedTotalDeposit(userId);
		}
	}
	
    public static void afterTransactionSuccess(Connection conn, long transactionId, long userId, long amount, long writerId, long loginId) throws SQLException {
        ArrayList<BonusUsers> bonuses = BonusDAOBase.getBonusesForActivation(conn, userId);
        boolean isBonusActivated = false;
        boolean isActivateBonus;

        for (BonusUsers bu : bonuses) {
        	isActivateBonus = false;

        	BonusHandlerBase bh = BonusHandlerFactory.getInstance(bu.getTypeId());

        	if (null != bh){

            	// Check whether current bonus should be activated
        		try {
					isActivateBonus = bh.isActivateBonusAfterTransactionSuccess(conn, bu, amount, transactionId);
				} catch (BonusHandlersException e) {
					log.error("Error on activation check for bonus user id " + bu.getId());
					throw new SQLException();
				}

            	// if need to activate bonus
            	if (isActivateBonus && !isBonusActivated){

                	isBonusActivated = true;
                	try {
						bh.activateBonusAfterTransactionSuccess(conn, bu, transactionId, userId, amount, writerId, loginId);
					} catch (BonusHandlersException e) {
						log.error("Error on activating bonus user id " + bu.getId());
						throw new SQLException();
					}
            	}
        	}
        }

        InvestmentsManagerBase.afterTransactionSuccess(userId);
    }


    /**
     * Direct banking deposit with Envoy service
     *
     * @param tran
     * @param user
     * @throws SQLException
     */
    public static Transaction insertDirectDepositEnvoy(Transaction tran, User user) throws SQLException {
        if (log.isInfoEnabled()) {
            String ls = System.getProperty("line.separator");
            log.info(ls + "Insert deposit:" + ls +
                    "amount: " + tran.getAmount() + ls +
                    "userId: " + user.getId() + ls);
        }

        Connection con = getConnection();
        try {
            tran.setDescription(null);
            tran.setCreditCardId(null);
            tran.setCc4digit(null);
            tran.setProcessedWriterId(0);
            tran.setChequeId(null);
            tran.setTimeCreated(new Date());
            tran.setUserId(user.getId());
            tran.setWriterId(0);
            tran.setWireId(null);
            tran.setChargeBackId(null);
            tran.setUtcOffsetCreated(user.getUtcOffset());
            tran.setClearingProviderId(ClearingManager.ENVOY_PROVIDER_ID);

            if (tran.getStatusId() == TRANS_STATUS_FAILED ||
                    tran.getStatusId() == TRANS_STATUS_CANCELED ||
                    tran.getStatusId() == TRANS_STATUS_SUCCEED) {
                tran.setTimeSettled(new Date());
                tran.setUtcOffsetSettled(user.getUtcOffset());
            } else {
                tran.setTimeSettled(null);
                tran.setUtcOffsetSettled(null);
            }

            TransactionsDAOBase.insert(con, tran);
            log.info("insert directDeposit finished successfully! ");
        } finally {
            closeConnection(con);
        }
        return tran;
    }

//    /**
//     * Insert bonus withdrawal to user
//     * this function operated after cancel bonus to user
//     * @param amount bonus deposit amount that should withdraw now
//     * @param userId the user that want to withdraw with
//     * @param utcOffset utcOffset of the user
//     * @param writerId writer that operate this action
//     * @param bonusUserId bonusUserId reference
//     * @param ip TODO
//     * @return
//     * @throws SQLException
//     */	public static boolean insertBonusWithdraw(Connection con, long amount, long userId, String utcOffset, long writerId, long bonusUserId, String ip) throws SQLException {
//
//		if (log.isEnabledFor(Level.INFO)) {
//			String ls = System.getProperty("line.separator");
//			log.log(Level.INFO, "Insert Bonus Withdraw: " + ls + "userId: "
//					+ userId + ls);
//		}
//
//		try {
//			Transaction tran = new Transaction();
//			tran.setAmount(amount);
//			tran.setComments(null);
//			tran.setCreditCardId(null);
//			tran.setDescription("log.balance.bonus.withdraw");
//			tran.setIp(ip);
//			tran.setProcessedWriterId(writerId);
//			tran.setTimeSettled(new Date());
//			tran.setTimeCreated(new Date());
//			tran.setUtcOffsetCreated(utcOffset);
//			tran.setUtcOffsetSettled(utcOffset);
//			tran.setTypeId(TransactionsManagerBase.TRANS_TYPE_BONUS_WITHDRAW);
//			tran.setUserId(userId);
//			tran.setWriterId(writerId);
//			tran.setStatusId(TRANS_STATUS_SUCCEED);
//			tran.setChequeId(null);
//			tran.setWireId(null);
//			tran.setChargeBackId(null);
//			tran.setReceiptNum(null);
//			tran.setBonusUserId(bonusUserId);
//
//			try {
//				con.setAutoCommit(false);
//				UsersDAOBase.addToBalance(con, userId, -amount);
//				TransactionsDAOBase.insert(con, tran);
//				UsersDAOBase.insertBalanceLog(con, writerId,
//						tran.getUserId(), ConstantsBase.TABLE_TRANSACTIONS, tran
//								.getId(), ConstantsBase.LOG_BALANCE_BONUS_WITHDRAW, utcOffset);
//				con.commit();
//			} catch (SQLException e) {
//				log.log(Level.ERROR, "insertBonusWithdraw: ", e);
//				try {
//					con.rollback();
//				} catch (SQLException ie) {
//					log.log(Level.ERROR, "Can't rollback.", ie);
//				}
//				throw e;
//			} finally {
//				try {
//					con.setAutoCommit(true);
//				} catch (Exception e) {
//					log.error("Can't set back to autocommit.", e);
//				}
//			}
//			if (log.isEnabledFor(Level.INFO)) {
//				String ls = System.getProperty("line.separator");
//				log.log(Level.INFO, "Bonus Withdraw insert successfully! " + ls
//						+ tran.toString());
//			}
//		} catch (SQLException e) {
//			log.log(Level.ERROR, "insertBonusWithdraw: ", e);
//			throw e;
//		}
//		return true;
//	}

//     /**
//      * Is first succeed deposit
//      * @param userId user id
//      * @return
//      * @throws SQLException
//      */
// 	public static boolean isFirstDeposit(long userId, long expectedValue) throws SQLException {
// 		Connection con = getConnection();
// 		try {
// 			return isFirstDeposit(con, userId, expectedValue);
// 		} finally {
// 			closeConnection(con);
// 		}
// 	}

// 	public static boolean isFirstDeposit(Connection con, long userId, long expectedValue) throws SQLException {
// 		return TransactionsDAOBase.isFirstDeposit(con, userId, expectedValue);
// 	}

 	public static long getFirstTransactionId(long userId) throws SQLException {
		Connection con = getConnection();
		try {

			return TransactionsDAOBase.getFirstTransactionId(con, userId);
		} finally {
			closeConnection(con);
		}
	}
 	
    public static String validateWithdrawForm(long amount, User user, boolean isFee, boolean splitErrorMsg,
            boolean checkLimits, CreditCard cc, String paypalEmail, String moneybookersEmail, boolean backend,
            Map<String, MessageToFormat> res) throws SQLException {
        Connection con = getConnection();
        try {
            UsersDAOBase.getByUserName(con, user.getUserName(), user);
            if (user.getBalance() < amount) {
                res.put("amount", new MessageToFormat("error.withdraw.highAmount", null, MessageToFormat.ERROR_MSG_NOT_DISPLAY_FIELD));
                return "error.transaction.withdraw.highamount";
            }
            if (!BonusManagerBase.userCanWithdraw(user.getId(), user.getBalance(), amount)) {
                res.put("", new MessageToFormat("bonus.cannot.withdraw.error", null, MessageToFormat.ERROR_MSG_NOT_DISPLAY_FIELD));
                return "bonus.cannot.withdraw.error";
            }
            Limit limit = LimitsDAOBase.getById(con, user.getLimitId());

			if (isFee && ((amount < limit.getMinWithdraw() && checkLimits) || (amount < 0))) {
				if (splitErrorMsg) { // for Web cc withdrawal
                    res.put("amount", new MessageToFormat("error.minwithdraw1", null, MessageToFormat.ERROR_MSG_NOT_DISPLAY_FIELD));
                } else { // Note: error.minwithdraw2 is now empty but it used for long msg in the web
                    res.put("amount", new MessageToFormat("error.minwithdraw", null, MessageToFormat.ERROR_MSG_NOT_DISPLAY_FIELD));
                }
                return "error.transaction.withdraw.minamounnt";
            }
            if (amount > limit.getMaxWithdraw()) {
                res.put("amount", new MessageToFormat("error.maxwithdraw",
                		new String[] {CommonUtil.formatCurrencyAmountAO(limit.getMaxWithdraw(), user.getCurrencyId())}, MessageToFormat.ERROR_MSG_NOT_DISPLAY_FIELD));
                return "error.transaction.withdraw.maxamount";
            }
            if (null != cc && cc.isCreditEnabled() &&  amount > cc.getCreditAmount()) {
                res.put("amount", new MessageToFormat("error.maxwithdraw",
                		new String[] {CommonUtil.formatCurrencyAmountAO(cc.getCreditAmount(), user.getCurrencyId())}, MessageToFormat.ERROR_MSG_NOT_DISPLAY_FIELD));
                return "error.transaction.withdraw.maxamounnt";
            }
            if (null != paypalEmail && !backend && !TransactionsDAOBase.isPaypalEmailValid(con, user.getId(), paypalEmail)) {
                res.put("email", new MessageToFormat("error.paypal.email.not.valid", null));
                return "error.paypal.email.not.valid";
            }
            if (null != moneybookersEmail && !backend && !TransactionsDAOBase.isMoneybookersEmailValid(con, user.getId(), moneybookersEmail)) {
                res.put("email", new MessageToFormat("error.moneybookers.email.not.valid", null));
                return "error.moneybookers.email.not.valid";
            }
        } finally {
            closeConnection(con);
        }
        return null;
    }

    /**
     * Validate credit card for:
     * expiration
     * not allowed
     * not issued from India
     * @param cc creditCard instance
     * @return
     */
    public static String validateCardWithdraw(CreditCard cc, Map<String, MessageToFormat> res) {
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.DATE, 1);
        Calendar expCal = Calendar.getInstance();
        expCal.clear();
        expCal.set(Integer.parseInt(cc.getExpYear()) + 2000, Integer.parseInt(cc.getExpMonth()), 1);

        if (cal.after(expCal)) {
            res.put("ccNum", new MessageToFormat(ConstantsBase.ERROR_CARD_EXPIRED, null));
            return "error.creditcard.expdate";
        } else if (cc.isBlocked()) {
            res.put("", new MessageToFormat("error.creditcard.notallowed.withdraw", null, MessageToFormat.ERROR_MSG_NOT_DISPLAY_FIELD ));
    		return "error.card.not.allowed";
        }
        return null;
    }

    /**
     * Insert credit card withdrawal
     *
     * @param famount witdrawal amount
     * @param cardId credit card number
     * @param name user name
     * @param error
     * @param user
     * @param writerId
     * @param feeCancel fee flag
     * @throws SQLException
     */
    public static void insertCreditCardWithdraw(long amount, CreditCard cc, String error,
    		User user, long writerId, boolean feeCancel, Long depositRefId, String ip, long loginId, QuestionnaireUserAnswer questUserAnswer) throws SQLException {
        log.info("Insert Credit Card Withdraw - user: " + user.getUserName() + " amount: " + amount);
        Connection con = getConnection();
        try {
            UsersDAOBase.getByUserName(con, user.getUserName(), user);
            long newBalance = user.getBalance() - amount;
            long clearingProviderId = 0;

            Transaction tran = new Transaction();
            tran.setAmount(amount);
            tran.setComments(null);
            tran.setCreditCardId(new BigDecimal(cc.getId()));
            tran.setIp(ip);
            tran.setProcessedWriterId(writerId);
            tran.setTimeCreated(new Date());
            tran.setUtcOffsetCreated(user.getUtcOffset());
            tran.setTypeId(TRANS_TYPE_CC_WITHDRAW);
            tran.setUserId(user.getId());
            tran.setWriterId(writerId);
            tran.setWireId(null);
            tran.setChargeBackId(null);
            tran.setReceiptNum(null);
            tran.setChequeId(null);
            tran.setFeeCancel(feeCancel);
            tran.setLoginId(loginId);
            if (cc.isCreditEnabled()) { // credit withdrawal
                tran.setCreditWithdrawal(true);
                clearingProviderId = cc.getClearingProviderId();
            } else {
                tran.setCreditWithdrawal(false);
                clearingProviderId = cc.getCftClearingProviderId();
            }

            //  checking credit card
//            if (error == null) {
//                if (cc.getIsAllowed() == 0) {
//                    log.info("Transaction Failed: card not allowed!");
//                    mtf.put("", new MessageToFormat("error.creditcard.notallowed.withdraw", null));
//                    error = "error.card.not.allowed";
//                }
//            }

            if (error == null) {
                tran.setDescription(null);
                tran.setStatusId(TRANS_STATUS_REQUESTED);
                tran.setTimeSettled(null);
                tran.setClearingProviderId(clearingProviderId);

                try {
                    con.setAutoCommit(false);
                    UsersDAOBase.addToBalance(con, user.getId(), -amount);
                    user.setBalance(newBalance);
                    if (null != depositRefId) { // credit from backend
                        tran.setDepositReferenceId(depositRefId.longValue());
                    }
                    TransactionsDAOBase.insert(con, tran);
                    GeneralDAO.insertBalanceLog(con,writerId,tran.getUserId(), ConstantsBase.TABLE_TRANSACTIONS, tran.getId(), ConstantsBase.LOG_BALANCE_CC_WITHDRAW, user.getUtcOffset());
					if (questUserAnswer != null) {
						questUserAnswer.setTransactionId(tran.getId());
						QuestionnaireDAOBase.insertUserAnswer(con, questUserAnswer, true);
					}
                    con.commit();
					PopulationEntryBase popUser = PopulationsManagerBase.getPopulationUserByUserId(user.getId());
					PopulationsManagerBase.insertIntoPopulation(con, PopulationsManagerBase.POP_TYPE_OPEN_WITHDRAW, 0, 0, user.getId(), writerId, popUser);
                } catch (SQLException e) {
                    log.error("ERROR during Credit card Withdraw! ", e);
                    try {
                        con.rollback();
                    } catch (SQLException ie) {
                        log.log(Level.ERROR, "Can't rollback.", ie);
                    }
                    throw e;
                } finally {
                    try {
                        con.setAutoCommit(true);
                    } catch (Exception e) {
                        log.error("Cannot set back to autoCommit! " + e);
                    }
                }
            } else { // there is an error
                tran.setDescription(error);
                tran.setStatusId(TRANS_STATUS_FAILED);
                tran.setTimeSettled(new Date());
                tran.setUtcOffsetSettled(user.getUtcOffset());
                TransactionsDAOBase.insert(con, tran);
            }

			try {
				RiskAlertsManagerBase.riskAlertWithdrawalHandler(writerId, error,
					tran.getId(), tran.getAmount(), tran.getUserId(), user.getBalance());
			} catch (Exception e) {
				log.error("Problem with risk Alert handler ", e);
			}

            //  set description if needed
            String description = (null == tran.getDescription()) ? "" : ", description: " +  tran.getDescription();
            log.info("Insert Credit card Withdraw finished successfully! " + description);
        } finally {
            closeConnection(con);
        }
    }

    /**
     * Validation for wire form
     * On the res we fill the error that user should see, and returning the error that should be on the transaction (description field)
     * @param amount amount for bank wire request
     * @param user requested user for bank wire
     * @param isFee bank wire with fee or not
     * @param checkLimits check minimum limit or not
     * @return
     * @throws SQLException
     */
    public static String validateWireForm(long amount, User user, boolean isFee, boolean checkLimits, boolean backend,
    		Map<String, MessageToFormat> res) throws SQLException {
        Connection con = getConnection();
        try {
            UsersDAOBase.getByUserName(con, user.getUserName(), user);
            if (user.getBalance() < amount) {
                res.put("amount", new MessageToFormat("error.bankWire.highAmount", null, MessageToFormat.ERROR_MSG_NOT_DISPLAY_FIELD));
                return "error.transaction.bankWire.highamount";
            }
            if (!BonusManagerBase.userCanWithdraw(user.getId(), user.getBalance(), amount)) {
                res.put("amount", new MessageToFormat("bonus.cannot.withdraw.error", null, MessageToFormat.ERROR_MSG_NOT_DISPLAY_FIELD));
                return "bonus.cannot.withdraw.error";
            }
            Limit limit = LimitsDAOBase.getById(con, user.getLimitId());
            long ccWithdraw = TransactionsManagerBase.getCreditCardMinWithdraw(user.getCurrencyId());
            //Block withdrawal by cheque/Bank wire attempt if user has non visible CC
            ArrayList<CreditCard> cc = CreditCardsDAOBase.getCcWithrawalByUserId(con, user.getId(), null, ccWithdraw, user.getSkinId(), false);
            if (!backend && cc.size() > 0) {
                res.put("", new MessageToFormat("error.minwithdraw1", null, MessageToFormat.ERROR_MSG_NOT_DISPLAY_FIELD));
                return "error.withdraw.card.not.visible";
            }
            long wireMinWithdraw = TransactionsManagerBase.getWireMinWithdraw(user.getCurrencyId());
            if (isFee && amount <= wireMinWithdraw && checkLimits ) {
                res.put("amount", new MessageToFormat("error.minbankwire", new String[] {CommonUtil.formatCurrencyAmountAO(wireMinWithdraw,
                		user.getCurrencyId())}, MessageToFormat.ERROR_MSG_NOT_DISPLAY_FIELD));
                return "error.transaction.bankWire.minamounnt";
            }
            if (amount > limit.getMaxBankWire() && checkLimits) {
                res.put("amount", new MessageToFormat("error.maxbankwire", new String[] {CommonUtil.formatCurrencyAmountAO(limit.getMaxBankWire(),
                		user.getCurrencyId())}, MessageToFormat.ERROR_MSG_NOT_DISPLAY_FIELD));
                return "error.transaction.bankWire.maxamount";
            }
        } finally {
            closeConnection(con);
        }
        return null;
    }

    /**
     * Insert Bank wire
     *
     * @param wire wire instance
     * @param res
     * @param user the user that creat wire request
     * @param writerId the writer id
     * @param feeCancel if this bank wire with fee or not
     * @throws SQLException
     */
    public static void insertBankWireWithdraw(WireBase wire, String error, User user, long writerId, boolean feeCancel, boolean eftwithdraw, String ip, long loginId, QuestionnaireUserAnswer questUserAnswer) throws SQLException {
        if (log.isInfoEnabled()) {
            String ls = System.getProperty("line.separator");
            log.info("Insert BankWire: " + ls
                    + "User:" + user.getUserName() + ls
                    + "amount:" + wire.getAmount() + ls
                    + "Swift:" + wire.getSwift() + ls
                    + "Iban:" + wire.getIban() + ls
                    + "AccountNum:" + wire.getAccountNum() + ls
                    + "BankName:" + wire.getBankNameTxt() + ls
            		+ "BranchAddress:" + wire.getBranchAddress() + ls
            		+ "BranchName:" + wire.getBranchName() + ls);
        }
        Connection con = getConnection();
        try {
            UsersDAOBase.getByUserName(con, user.getUserName(), user);
            long amount = CommonUtil.calcAmount(Double.parseDouble(wire.getAmount()));
            long newBalance = user.getBalance() - amount;

            Transaction tran = new Transaction();
            tran.setAmount(amount);
            tran.setComments(null);
            tran.setCreditCardId(null);
            tran.setIp(ip);
            tran.setProcessedWriterId(writerId);
            tran.setTimeCreated(new Date());
            tran.setUtcOffsetCreated(user.getUtcOffset());
            if (eftwithdraw){
                tran.setTypeId(TRANS_TYPE_EFT_WITHDRAW);
                log.info("****** EFT Withdraw ********");
            } else {
                tran.setTypeId(TRANS_TYPE_BANK_WIRE_WITHDRAW);
            }
            tran.setUserId(user.getId());
            tran.setWriterId(writerId);
            tran.setChargeBackId(null);
            tran.setReceiptNum(null);
            tran.setChequeId(null);
            tran.setFeeCancel(feeCancel);
            tran.setLoginId(loginId);

            if (error == null) { // no error
                try {
                    con.setAutoCommit(false);
                    WiresDAOBase.insertBankWire(con, wire);
                    tran.setWireId(new BigDecimal(wire.getId()));
                    tran.setDescription(null);
                    tran.setStatusId(TRANS_STATUS_REQUESTED);
                    tran.setTimeSettled(null);
                    UsersDAOBase.addToBalance(con, user.getId(), -amount);
                    user.setBalance(newBalance);
                    TransactionsDAOBase.insert(con, tran);
                    if (eftwithdraw){
                        GeneralDAO.insertBalanceLog(con,writerId,tran.getUserId(), ConstantsBase.TABLE_TRANSACTIONS, tran.getId(), ConstantsBase.LOG_BALANCE_EFT_WITHDRAW, user.getUtcOffset());
                    } else {
                        GeneralDAO.insertBalanceLog(con,writerId,tran.getUserId(), ConstantsBase.TABLE_TRANSACTIONS, tran.getId(), ConstantsBase.LOG_BALANCE_BANK_WIRE, user.getUtcOffset());
                    }
					if (questUserAnswer != null) {
						questUserAnswer.setTransactionId(tran.getId());
						QuestionnaireDAOBase.insertUserAnswer(con, questUserAnswer, true);
					}
                    con.commit();
					PopulationEntryBase popUser = PopulationsManagerBase.getPopulationUserByUserId(user.getId());
					PopulationsManagerBase.insertIntoPopulation(con, PopulationsManagerBase.POP_TYPE_OPEN_WITHDRAW, 0, 0, user.getId(), writerId, popUser);

                    log.info("Insert BankWire finished successfully!");
                } catch (SQLException e) {
                    log.error("ERROR during bank wire Withdraw! ", e);
                    try {
                        con.rollback();
                    } catch (SQLException ie) {
                        log.log(Level.ERROR, "Can't rollback.", ie);
                    }
                    throw e;
                } finally {
                    try {
                        con.setAutoCommit(true);
                    } catch (Exception e) {
                        log.error("Cannot set back to autoCommit! " + e);
                    }
                }
            } else {  // error
                tran.setWireId(null);
                tran.setDescription(error);
                tran.setStatusId(TRANS_STATUS_FAILED);
                tran.setTimeSettled(new Date());
                tran.setUtcOffsetSettled(user.getUtcOffset());
                TransactionsDAOBase.insert(con, tran);
                log.info("Insert BankWire finished with error!");
            }
            try{
                RiskAlertsManagerBase.riskAlertWithdrawalHandler(writerId, error, tran.getId(), tran.getAmount(), tran.getUserId(), user.getBalance());
            } catch (Exception e) {
                log.error("Problem with risk Alert handler ", e);
            }
        } catch (SQLException e) {
            log.error("ERROR during BankWire!! ", e);
        } finally {
            closeConnection(con);
        }
    }

//	private static boolean isFailureRepeat(Connection conn, long userId, long cardId, long amount , long transactionId, boolean firstDeposit) throws SQLException {
//		return TransactionsDAOBase.isFailureRepeat(conn, userId, cardId, amount , transactionId, firstDeposit);
//	}

	/**
	 * Get fee By limit id and tran type
	 * @throws SQLException
	 */
    @Deprecated
	public static long getFeeByTranTypeId(long limitId, int tranTypeId) throws SQLException {
		Connection con = getConnection();
		try {
			return LimitsDAOBase.getFeeByTranTypeId(con, limitId, tranTypeId);
		} finally {
			closeConnection(con);
		}
	}

	@Deprecated
	public static long getFeeByTranTypeId(Limit limit, int tranTypeId) {
		switch (tranTypeId) {
		case TransactionsManagerBase.TRANS_TYPE_CC_WITHDRAW:
			return limit.getCcFee();
		case TransactionsManagerBase.TRANS_TYPE_BANK_WIRE_WITHDRAW:
			return limit.getBankWireFee();
		case TransactionsManagerBase.TRANS_TYPE_EFT_WITHDRAW:
			return limit.getBankWireFee();
		case TransactionsManagerBase.TRANS_TYPE_CHEQUE_WITHDRAW:
			return limit.getChequeFee();
		case TransactionsManagerBase.TRANS_TYPE_MONEYBOOKERS_WITHDRAW:
			return limit.getBankWireFee();
		case TransactionsManagerBase.TRANS_TYPE_WEBMONEY_WITHDRAW:
			return limit.getBankWireFee();
		case TransactionsManagerBase.TRANS_TYPE_BAROPAY_WITHDRAW:
			return limit.getBankWireFee();
		case TransactionsManagerBase.TRANS_TYPE_CUP_CHINA_WITHDRAW:
			return limit.getBankWireFee();
//		case TransactionsManagerBase.TRANS_TYPE_LOW_AMOUNT_WITHDRAW_FEE:
//			return (long) (limit.getAmountForLowWithdraw() * TransactionsManagerBase.TRANS_TYPE_LOW_AMOUNT_WITHDRAW_FEE_COEFFICIENT);
		default:
			return 0;
		}
	}

	public static void updatePixelRunTime(long id) throws SQLException {
		Connection con = getConnection();
		try {
			TransactionsDAOBase.updatePixelRunTime(con, id);
		} finally {
			closeConnection(con);
		}
	}

	public static long getSumOfDepositsForRank(long userId) throws SQLException {
		Connection con = getConnection();
		try {
			return TransactionsDAOBase.getSumOfDepositsForRank(con, userId);
		} finally {
			closeConnection(con);
		}
	}

	@Deprecated
	public static boolean startReroutingProcess(ClearingInfo ci, ClearingRoute cr, CreditCard cc, Transaction tran, User user,
												String depositVal, TransactionBin tb,
												long loginId)	throws SQLException, InvalidKeyException, NoSuchAlgorithmException,
																NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException,
																UnsupportedEncodingException, InvalidAlgorithmParameterException {
		ClearingInfo newCi = new ClearingInfo(ci);
		ArrayList<ErrorCode> list = ErrorCode.getAllErrorCodes();
		boolean errorExist = false;
		boolean reroutedSucccessfully = false;
		ErrorCode ec = new ErrorCode();
		HashMap<Long, Long> priorityOfreroute = ConfigManager.getPriorityOfProvider();
		long clearingGroup = 0;
		for(ErrorCode e : list){
			if(ci.getResult().equalsIgnoreCase(e.getResult()) &&   e.getClearingProviderId() == ci.getProviderId()){
				errorExist = true;
				ec.setId(e.getId());
				ec.setSpecialCode(e.isSpecialCode());
				break;
			}
		}
		clearingGroup = ClearingManager.getproviderGroupById(ci.getProviderId());
		long check = 0 ;
		for(check = 1 ; check < priorityOfreroute.size()+1 ; check++){// remove current provider from priority list
			Object key = priorityOfreroute.get(check);
			if((Long)key == clearingGroup){
				priorityOfreroute.remove(check);
			}
		}

		if(errorExist){	//getting limit for this transaction
			ArrayList<Long> cpNotToReroute = ClearingManager.getRelevantlimits(ci, cr);
			for(int i = 0 ;i < cpNotToReroute.size(); i++){
				if(priorityOfreroute.containsValue(cpNotToReroute.get(i))){
					boolean foundProvider = false;
					for(long j = 1 ; !foundProvider ; j++){
						if(priorityOfreroute.containsKey(j)){
							long value = priorityOfreroute.get(j);
							long limitValue = cpNotToReroute.get(i);
							if(value == limitValue){
								priorityOfreroute.remove(j);
								foundProvider = true;
							}
						}
					}
				}
			}
			if(!(priorityOfreroute.size() > 0 )){
				log.error("No clearing Providers to reroute");
				return false;
			}
		} else {
			return false;
		}
		if (!ec.isSpecialCode()) {// check days or faliures
			//check x_days OR 20_failures
			String ccBin = ci.getCcn();
			ccBin = ccBin.substring(0, 6);
			if(!checkDaysOrFailuresToBin(cr,Long.parseLong(ccBin), tb)){
				log.error("No consecutive errors");
				return false;
			}
		}

		long x = 1;
		long index = 1;
		int count = priorityOfreroute.size();
		long currency = ci.getCurrencyId();
		HashMap<Long, Long> hmClearingProvider = new HashMap<Long, Long>();
		while(count > 0){// change the groupId to clearing provider id
			if(priorityOfreroute.containsKey(x)){
			if(priorityOfreroute.get(x) == ClearingManager.CLEARING_GROUP_INATEC){
				if(currency == ConstantsBase.CURRENCY_USD_ID){
					hmClearingProvider.put(index, ClearingManager.INATEC_PROVIDER_ID_USD);
					index++;
				} else if(currency == ConstantsBase.CURRENCY_EUR_ID){
					hmClearingProvider.put(index, ClearingManager.INATEC_PROVIDER_ID_EUR);
					index++;
				}else if (currency == ConstantsBase.CURRENCY_GBP_ID){
					hmClearingProvider.put(index, ClearingManager.INATEC_PROVIDER_ID_GBP);
					index++;
				} else if (currency == ConstantsBase.CURRENCY_TRY_ID){
					hmClearingProvider.put(index, ClearingManager.INATEC_PROVIDER_ID_TRY);
					index++;
				}
			} else if(priorityOfreroute.get(x) == ClearingManager.CLEARING_GROUP_TBI){
				hmClearingProvider.put(index, ClearingManager.TBI_AO_PROVIDER_ID);
				index++;
			} else if(priorityOfreroute.get(x) == ClearingManager.CLEARING_GROUP_WIRE_CARD){
				hmClearingProvider.put(index, ClearingManager.WC_PROVIDER_ID);
				index++;
			}
			count--;
			}
			x++;
		}
		index = 1;
		boolean madeRerouteAttempt = false;
		HashMap<Long, Transaction> hmNewTran = new HashMap<Long, Transaction>();
		while(hmClearingProvider.size() > 0 && !reroutedSucccessfully){// make try to deposit by reroute
			if(hmClearingProvider.containsKey(index)){
			newCi.setProviderId(hmClearingProvider.get(index));
			tran.setReroutingTranId(tran.getId());
			hmNewTran.put(newCi.getProviderId(), performReroutingAttemptTran(tran, newCi, user, depositVal, loginId));
			madeRerouteAttempt = true;
			if(newCi.isSuccessful()){
				reroutedSucccessfully = true;ArrayList<ClearingRoute> crList = new ArrayList<ClearingRoute>();
				if(cr.getStartBIN()!= null && cr.getStartBIN() != "" && !ec.isSpecialCode()){
					if(isInatecClearingProvider(hmClearingProvider.get(index))){
						crList = ClearingManager.existclearingRouteWithCurrnecy(cr.getStartBIN());
						insertClearingRoutesForInatec(crList, cr, cc, hmClearingProvider.get(index), ec.getId(), user.getPlatformId());
						crList = ClearingManager.getClearingRouteByBin(cr.getStartBIN());//getClearingRouteById(cr.getId());
						for (ClearingRoute temp : crList){
							if(		temp.getCurrnecyId() == 0  &&  temp.getCcType() == 0 &&  temp .getCountryId() == 0 && temp.getSkinId() == 0){
								try {
									ClearingManager.updateClearingRouteIsActive(false, temp.getId(), cc, ec.getId());
								} catch (Exception e1) {
									// TODO Auto-generated catch block
									log.debug(e1);
								}
							}
						}
					} else {
						ClearingManager.updateClearingProviderAndLog(cr, cc, hmClearingProvider.get(index), ec.getId(), false);
						ClearingRoute temp = ClearingManager.getClearingRouteById(cr.getId());
						if(		temp.getCurrnecyId() > 0  ||  temp.getCcType() > 0 ||  temp .getCountryId() > 0 || temp.getSkinId() > 0 ){
							ArrayList<ClearingRoute> crArr = ClearingManager.getClearingRouteByBin(cr.getStartBIN());
							if(crArr.size() > 0 ){
								if(!crArr.get(0).isActive()){
									try {
										ClearingManager.updateClearingRouteIsActive(true, crArr.get(0).getId(), cc, ec.getId());
									} catch (Exception e1) {
										// TODO Auto-generated catch block
										log.debug(e1);
									}
								}
								ClearingManager.updateClearingProviderAndLog(crArr.get(0), cc, hmClearingProvider.get(index), ec.getId(), isInatecClearingProvider(hmClearingProvider.get(index)));
							}else {
								ClearingManager.insertNewClearingRouteAndLog(cr, cc, hmClearingProvider.get(index), ec.getId(), 0, isInatecClearingProvider(hmClearingProvider.get(index)), user.getPlatformId());
							}
						}
						crList = ClearingManager.existclearingRouteWithCurrnecy(cr.getStartBIN());
						for(ClearingRoute vo : crList){
							if(isInatecClearingProvider(vo.getDepositClearingProviderId())){
								try {
									ClearingManager.updateClearingRouteIsActive(false, vo.getId(), cc, ec.getId());
								} catch (Exception e1) {
									// TODO Auto-generated catch block
									log.debug("Error Updateing", e1);
								}
							}
						}
					}
					if(isInatecClearingProvider(cr.getDepositClearingProviderId())){
						crList = ClearingManager.existclearingRouteWithCurrnecy(cr.getStartBIN());
						for(ClearingRoute vo : crList){
							try {
								ClearingManager.updateClearingRouteIsActive(false, vo.getId(), cc, ec.getId());
							} catch (Exception e1) {
								// TODO Auto-generated catch block
								e1.printStackTrace();
							}
						}
					}
				} else if (cr.getStartBIN() == null && !ec.isSpecialCode()){
					long crId = ClearingManager.logicForUpdateClearingRoute(cc.getBinStr(), hmClearingProvider.get(index));
					long newClearingRouteId = 0;
					if(isInatecClearingProvider(hmClearingProvider.get(index))){
						crList = ClearingManager.existclearingRouteWithCurrnecy(cc.getBinStr());
						insertClearingRoutesForInatec(crList, cr, cc, hmClearingProvider.get(index), ec.getId(), user.getPlatformId());
					} else {
						if(crId > 0){
							newClearingRouteId = crId;
							try {
								ClearingManager.updateClearingRouteIsActive(true, crId, cc, ec.getId());
							} catch (Exception e1) {
								// TODO Auto-generated catch block
								e1.printStackTrace();
								log.debug(e1);
							}
						}else{
							newClearingRouteId = ClearingManager.insertNewClearingRouteAndLog(cr, cc, hmClearingProvider.get(index), ec.getId(), 0, isInatecClearingProvider(hmClearingProvider.get(index)), user.getPlatformId());
							crList = ClearingManager.existclearingRouteWithCurrnecy(cc.getBinStr());
							for(ClearingRoute vo : crList){
								try {
									ClearingManager.updateClearingRouteIsActive(false, crId, cc, ec.getId());
								} catch (Exception e1) {
									// TODO Auto-generated catch block
									e1.printStackTrace();
								}
							}
						}
					}
					ClearingRoute crTemp = new ClearingRoute();
					crTemp = ClearingManager.getClearingRouteById(newClearingRouteId);
					if(crTemp != null){
						cr = crTemp;
					}
				}
			}
			hmClearingProvider.remove(index);
			}
			index++;
		}
		if(madeRerouteAttempt && !reroutedSucccessfully){
			ClearingManager.updateTimeLastReroute(cr.getId());
			TransactionsManagerBase.updateTimeLastRerouteFailed(tb);
		}
	return reroutedSucccessfully;

	}

	@Deprecated
	private static	Transaction
			performReroutingAttemptTran(Transaction formerTran, ClearingInfo ci, User user, String depositVal,
										long loginId)	throws SQLException, InvalidKeyException, NoSuchAlgorithmException,
														NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException,
														UnsupportedEncodingException, InvalidAlgorithmParameterException {

//		ClearingRoute cr = new ClearingRoute();
//		Connection con = getConnection();
//		Transaction tran = new Transaction();
//		CreditCard	cc =  CreditCardsDAOBase.getById(getConnection(), ci.getCardId());
//		formerTran.setIsRerouted(true);
//		tran.setAmount(formerTran.getAmount());
//		tran.setComments("");
//		tran.setCreditCardId(formerTran.getCreditCardId());
//		tran.setCc4digit(formerTran.getCc4digit());
//		tran.setDescription(null);
//		tran.setIp(formerTran.getIp());
//		tran.setProcessedWriterId(Writer.WRITER_ID_WEB);
//		tran.setChequeId(null);
//		tran.setTimeCreated(new Date());
//		tran.setTypeId(TRANS_TYPE_CC_DEPOSIT_REROUTE);
//		tran.setUserId(formerTran.getUserId());
//		tran.setWriterId(Writer.WRITER_ID_MOBILE);
//		tran.setWireId(null);
//		tran.setChargeBackId(null);
//		tran.setStatusId(TRANS_STATUS_STARTED);
//		tran.setCurrency(formerTran.getCurrency());
//		tran.setUtcOffsetCreated(formerTran.getUtcOffsetCreated());
//		tran.setIsRerouted(true);
//		tran.setReroutingTranId(formerTran.getId());
//		tran.setLoginId(loginId);
//		TransactionsDAOBase.insert(con, tran);
//		ci.setTransactionId(tran.getId());
//
//		if (log.isInfoEnabled()) {
//			String ls = System.getProperty("line.separator");
//			log.info("Transaction record inserted: " + ls + tran.toString() + ls);
//		}
//
//		boolean timeout = false;
//		String errorCode = "";
//
//		try {
//			cr = ClearingManager.deposit(ci, true);
//			if(ci.isSuccessful()){
//				//update tran & clearing provider
//				formerTran.setTypeId(TRANS_TYPE_CC_DEPOSIT_REROUTE);
//				tran.setTypeId(TRANS_TYPE_CC_DEPOSIT);
//			}
//		} catch (ClearingException ce) {
//			if (ce.getCause() instanceof SocketTimeoutException) {
//				timeout = true;
//			}
//
//			log.error("Failed to authorize transaction: " + ci.toString(), ce);
//			ci.setResult("999");
//			ci.setSuccessful(false);
//			ci.setMessage(ce.getMessage());
//		}
//		tran.setClearingProviderId(ci.getProviderId());
//		tran.setComments(ci.getResult() + "|" + ci.getMessage() + "|" + ci.getUserMessage() + "|" + ci.getProviderTransactionId());
//		tran.setXorIdAuthorize(ci.getProviderTransactionId());
//		if(ci.isSuccessful()){
//			if (log.isInfoEnabled()) {
//				log.info("Transaction Successful! authNumber: " + ci.getAuthNumber());
//			}
//			tran.setAuthNumber(ci.getAuthNumber());
//			tran.setAcquirerResponseId(ci.getAcquirerResponseId());
//            if (null != ci.getPaReq()) {
//                tran.setStatusId(TRANS_STATUS_AUTHENTICATE);
//            } else {
//                tran.setStatusId(TRANS_STATUS_PENDING);
//            }
//            tran.setTimeSettled(null);
//
//            if(!CommonUtil.IsParameterEmptyOrNull(ci.getRecurringTransaction()) && ci.getProviderId() == ClearingManager.TBI_AO_PROVIDER_ID){
//				cc.setRecurringTransaction(ci.getRecurringTransaction());
//				CreditCardsDAOBase.update(getConnection(), cc);
//			}
//            if (tran.getStatusId() != TRANS_STATUS_AUTHENTICATE) {
//            	try {
//            		con.setAutoCommit(false);
//					UsersDAOBase.addToBalance(con, user.getId(), tran.getAmount());
//					GeneralDAO.insertBalanceLog(con, Writer.WRITER_ID_WEB, tran.getUserId(), ConstantsBase.TABLE_TRANSACTIONS, tran.getId(), ConstantsBase.LOG_BALANCE_CC_DEPOSIT, user.getUtcOffset());
//					TransactionsDAOBase.update(con, tran);
//					con.commit();
//            	} catch (Exception e) {
//            		log.error("Exception in success deposit Transaction! ", e);
//    				try {
//    					con.rollback();
//    				} catch (Throwable it) {
//    					log.error("Can't rollback.", it);
//    				}
//            	} finally {
//                    try {
//                        con.setAutoCommit(true);
//                    } catch (Exception e) {
//                        log.error("Can't set back to autocommit.", e);
//                    }
//        		}
//            } else {  // AUTHENTICATE status
//            	TransactionsDAOBase.update(con, tran);
//            }
//		} else { // failed clearing
//			tran.setComments(tran.getComments() + " | " + cc.toStringForComments());
//			if (log.isInfoEnabled()) {
//				String ls = System.getProperty("line.separator");
//				log.info("Transaction failed clearing! " + ci.toString() + ls);
//			}
//
//			tran.setStatusId(TRANS_STATUS_FAILED);
//			if (timeout) {
//				tran.setDescription(ConstantsBase.ERROR_CLEARING_TIMEOUT);
//			} else {
//				tran.setDescription(ConstantsBase.ERROR_FAILED_CLEARING);
//			}
//		}
//		errorCode = ci.getResult();
//		if (tran.getStatusId() == TRANS_STATUS_FAILED ) {//update transaction on failed cases
//			tran.setTimeSettled(new Date());
//			tran.setUtcOffsetSettled(user.getUtcOffset());
//			TransactionsDAOBase.update(con, tran);
//		}
//
//		return tran;
		return null;
	}

	public static boolean checkDaysOrFailuresToBin(ClearingRoute cr, long bin, TransactionBin tb) throws SQLException{
		boolean isGoodToReroute = false;
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.DAY_OF_MONTH , (int)-ConfigManager.getDaysNumForFailures());
		Date date = cal.getTime();
		if(tb.getTimeLastSuccess() == null || !tb.getTimeLastSuccess().after(date)){// checking the last success params
			isGoodToReroute = true;
			log.debug("last success is more than then days to calaulate");
		} else { // check 20 consecutive failure by unique CC
			long numOfFailures = getConsecutiveFailuresByBin(cr, bin);
			if(numOfFailures >= ConfigManager.getFailuresNumConsecutive()){
				isGoodToReroute = true;
			}
		}
		return  isGoodToReroute;
	}

	public static long getConsecutiveFailuresByBin(ClearingRoute cr , long bin) throws SQLException{
		Connection conn = getConnection();
		try{
			return TransactionsDAOBase.getConsecutiveFailuresByBin(conn, cr, bin);
		} finally {
			closeConnection(conn);
		}
	}

	public static void updateTimeLastSuccess(TransactionBin tb) throws SQLException{
		Connection con = getConnection();
		try {
			TransactionsDAOBase.updateTimeLastSuccess(con, tb);
		} finally {
			closeConnection(con);
		}

	}

	public static void updateTimeLastRerouteFailed(TransactionBin tb) throws SQLException{
		Connection con = getConnection();
		try {
			TransactionsDAOBase.updateTimeLastRerouteFailed(con, tb);
		} finally {
			closeConnection(con);
		}

	}


	private static void insertClearingRoutesForInatec(ArrayList<ClearingRoute> crList, ClearingRoute cr, CreditCard cc, long ProviderId, long errorCodeId, int platformId) {
		boolean tryCurr = false,eurCurr = false,usdCurr = false,gbpCurr = false;
		for(ClearingRoute vo : crList){
		try{
			if(vo.getCurrnecyId() == ConstantsBase.CURRENCY_EUR_ID){
				ClearingManager.updateClearingProviderAndLog(vo, cc, ClearingManager.INATEC_PROVIDER_ID_EUR, errorCodeId, true);
				if(!vo.isActive()){
					ClearingManager.updateClearingRouteIsActive(true, vo.getId(), cc, errorCodeId);
				}
				eurCurr = true;
			}else if (vo.getCurrnecyId() == ConstantsBase.CURRENCY_TRY_ID){
				ClearingManager.updateClearingProviderAndLog(vo, cc, ClearingManager.INATEC_PROVIDER_ID_TRY, errorCodeId, true);
				if(!vo.isActive()){
					ClearingManager.updateClearingRouteIsActive(true, vo.getId(), cc, errorCodeId);
				}
				tryCurr = true;
			}else if (vo.getCurrnecyId() == ConstantsBase.CURRENCY_USD_ID){
				ClearingManager.updateClearingProviderAndLog(vo, cc, ClearingManager.INATEC_PROVIDER_ID_USD, errorCodeId, true);
				if(!vo.isActive()){
					ClearingManager.updateClearingRouteIsActive(true, vo.getId(), cc, errorCodeId);
				}
				usdCurr = true;
			}else if (vo.getCurrnecyId() == ConstantsBase.CURRENCY_GBP_ID){
				ClearingManager.updateClearingProviderAndLog(vo, cc, ClearingManager.INATEC_PROVIDER_ID_GBP, errorCodeId, true);
				if(!vo.isActive()){
					ClearingManager.updateClearingRouteIsActive(true, vo.getId(), cc, errorCodeId);
				}
				gbpCurr = true;
			}
		}catch (Exception e) {
			// TODO: handle exception
			log.debug(e);
		}
		}
		try{
			if(!eurCurr){
				ClearingManager.insertNewClearingRouteAndLog(cr, cc, ClearingManager.INATEC_PROVIDER_ID_EUR, errorCodeId, ConstantsBase.CURRENCY_EUR_ID, true, platformId);
			}
			if(!tryCurr){
				ClearingManager.insertNewClearingRouteAndLog(cr, cc, ClearingManager.INATEC_PROVIDER_ID_TRY, errorCodeId, ConstantsBase.CURRENCY_TRY_ID, true, platformId);
			}
			if(!usdCurr){
				ClearingManager.insertNewClearingRouteAndLog(cr, cc, ClearingManager.INATEC_PROVIDER_ID_USD, errorCodeId, ConstantsBase.CURRENCY_USD_ID, true, platformId);
			}
			if(!gbpCurr){
				ClearingManager.insertNewClearingRouteAndLog(cr, cc, ClearingManager.INATEC_PROVIDER_ID_GBP, errorCodeId, ConstantsBase.CURRENCY_GBP_ID, true, platformId);
			}
		} catch (Exception e) {
			// TODO: handle exception
			log.debug(e);
		}
	}

	private static boolean isInatecClearingProvider(Long providerId) {
		// TODO Auto-generated method stub
		if(providerId == ClearingManager.INATEC_PROVIDER_ID_EUR ||
				providerId == ClearingManager.INATEC_PROVIDER_ID_GBP ||
				providerId == ClearingManager.INATEC_PROVIDER_ID_TRY ||
				providerId == ClearingManager.INATEC_PROVIDER_ID_USD ){
			return true;
		}
		return false;
	}

	/** Insert DeltaPayChinaDeposit transaction
	 * @param depositVal
	 * @param writerId
	 * @param user
	 * @param
	 * @return
	 * @throws SQLException
	 */
	public static DeltaPayInfo insertDeltaPayChinaDeposit(String depositVal, long writerId, User user, String hpURL, String imagesDomain, int selectorId,
														 long providerID, String ipAddress, Map<String, MessageToFormat> mtf, DeltaPayInfo info, long loginId) throws SQLException {

		if (log.isInfoEnabled()) {
			String ls = System.getProperty("line.separator");
			log.info(ls + "Insert DeltaPayChina transaction deposit:" + ls +
                    "amount: " + depositVal + ls +
                    "writer: " + writerId + ls +
                    "userId: " + user.getId() + ls);
		}

		Transaction tran = null;
		String error = null;
		Connection con = getConnection();

		try {
			long deposit = CommonUtil.calcAmount(depositVal);
			tran = new Transaction();
			error = validateDepositLimits(mtf, user, deposit, tran, true);//(con, formName, user, deposit, source, true);
			tran.setAmount(CommonUtil.calcAmount(depositVal));
			tran.setComments(null);
			tran.setCreditCardId(null);
			tran.setDescription(null);
			tran.setIp(ipAddress);
			tran.setProcessedWriterId(writerId);
			tran.setChequeId(null);
			tran.setTimeSettled(null);
			tran.setTimeCreated(new Date());
			tran.setTypeId(TRANS_TYPE_CUP_CHINA_DEPOSIT);
			tran.setUserId(user.getId());
			tran.setWriterId(writerId);
			tran.setChargeBackId(null);
			tran.setStatusId(TRANS_STATUS_STARTED);
			tran.setCurrency(user.getCurrency());
			tran.setUtcOffsetCreated(user.getUtcOffset());
			tran.setUtcOffsetSettled(null);
			tran.setClearingProviderId(providerID);
			tran.setSelectorId(selectorId);
			tran.setLoginId(loginId);
			if (null != error) {
				tran.setStatusId(TRANS_STATUS_FAILED);
				tran.setDescription(error);
				tran.setTimeSettled(new Date());
				tran.setUtcOffsetSettled(user.getUtcOffset());
			} else {
				tran.setStatusId(TRANS_STATUS_AUTHENTICATE);
			}
			TransactionsDAOBase.insert(con, tran);



			//save the data for request to provider(deltaPay)
			info = new DeltaPayInfo(user, tran, hpURL);
			info.setFirstName(user.getFirstName());
			info.setLastName(user.getLastName());
			info.setAmount(tran.getAmount());
			info.setTransactionId(tran.getId());
			info.setErrorMessageFromRequset(error);

			if (null != error) {
				tran.setComments("Request has been send, didn't get answer or the customer at the provider site");
			}
			TransactionsDAOBase.update(con, tran);

			
	        log.info("DeltaPay request:  \n" +
		        "affiliate: " + info.getAffiliate() + "\n" +
				"paymethod: Credit Card" + "\n" +
				"processing_mode: sale" + "\n" +
				"redirect: " + info.getRedirect() + "\n" +
				"location: AFF" + "\n" +
				"order_id: " + info.getTransactionId() + "\n" +
				"terminal_name: " + info.getTerminalName() + "\n" +
				"agent_name: " + info.getAgentName() + "\n" +
				"first_name: " + info.getFirstName() + "\n" +
				"last_name: " + info.getLastName() + "\n" +
				"address1: " + info.getAddress() + "\n" +
				"city: " + user.getCityName() + "\n" +
				"state: NA" + "\n" +
				"country: " + CountryManagerBase.getCountry(user.getCountryId()).getA2() + "\n" +
				"zip: " + user.getZipCode() + "\n" +
				"telephone: " + user.getMobilePhone() + "\n" +
				"amount: " + info.getFullAmountStr() + "\n" +
				"currency: " + user.getCurrency().getNameKey() + "\n" +
				"email: " + user.getEmail() + "\n" +
				"card_type: " + info.getCardTypeName() + "\n" +
				"card_number: " + info.getCcn() + "\n" +
				"cvv: " + info.getCvv() + "\n" +
				"expiry_mo: " + info.getExpireMonth() + "\n" +
				"expiry_yr: " + info.getExpireYear() + "\n");

			if (null != error){ // deposit failed due to minimum limitation
				try {
					PopulationsManagerBase.deposit(user.getId(), false, writerId, tran);
				} catch (Exception e) {
					log.warn("Problem with population failed deposit event " + e);
				}
			}
		} catch (Exception e) {
			log.debug("problem with insert delata pay deposit", e);
			return null;
		} finally {
			closeConnection(con);
		}
		return info;
	}

	/**
	 * insert DeltaPayChina Withdraw
	 * @param error
	 * 		if there is an error with the request
	 * @param user
	 * 		the user that creat wire request
	 * @param writerId
	 * 		the writer id
	 * @param feeCancel
	 * 		if this bank wire with fee or not
	 * @throws SQLException
	 */	
	public static void insertDeltaPayChinaWithdraw(String amount, String error, User user, long writerId, boolean feeCancel, String ipAddress, long loginId)	throws SQLException {

		if (log.isEnabledFor(Level.INFO)) {
			String ls = System.getProperty("line.separator");
			log.log(Level.INFO,
							"insertDeltaPayChinaWithdraw " + ls + "User:" + user.getUserName() + ls + "famount:" + amount);
		}

		Connection con = getConnection();

		try {
			UsersDAOBase.getByUserName(con, user.getUserName(), user);
			long newBalance = user.getBalance() - CommonUtil.calcAmount(amount);

			Transaction tran = new Transaction();
			tran.setAmount(CommonUtil.calcAmount(amount));
			tran.setComments(null);
			tran.setCreditCardId(null);
			tran.setIp(ipAddress);
			tran.setProcessedWriterId(writerId);
			tran.setCreditWithdrawal(true);// for deltaPAy cft -- only true
			tran.setTimeCreated(new Date());
			tran.setUtcOffsetCreated(user.getUtcOffset());
            tran.setTypeId(TRANS_TYPE_DELTAPAY_CHINA_WITHDRAW);
			tran.setUserId(user.getId());
			tran.setWriterId(writerId);
			tran.setChargeBackId(null);
			tran.setReceiptNum(null);
			tran.setChequeId(null);
			tran.setFeeCancel(feeCancel);
			tran.setLoginId(loginId);

			if (error == null) { // no error
				try {
					con.setAutoCommit(false);
					tran.setDescription(null);
					tran.setStatusId(TRANS_STATUS_REQUESTED);
					tran.setTimeSettled(null);
					UsersDAOBase.addToBalance(con, user.getId(), -CommonUtil.calcAmount(amount));
					user.setBalance(newBalance);
					TransactionsDAOBase.insert(con, tran);
					GeneralDAO.insertBalanceLog(con,writerId,tran.getUserId(), ConstantsBase.TABLE_TRANSACTIONS, tran.getId(), ConstantsBase.LOG_BALANCE_DELTA_PAY_CHINA_WITHDRAW, user.getUtcOffset());
					con.commit();
					PopulationEntryBase popUser = PopulationsManagerBase.getPopulationUserByUserId(user.getId());
					if (popUser.getEntryTypeId() != PopulationsManagerBase.ENTRY_TYPE_REMOVE_PERMANENTLY) {
						PopulationsManagerBase.insertIntoPopulation(con, PopulationsManagerBase.POP_TYPE_OPEN_WITHDRAW, 0, 0, user.getId(), writerId, popUser);
					}
					log.info("insert DeltaPayChinaWithdraw finished successfully!");
				} catch (SQLException e) {
					log.error("ERROR during DeltaPayChinaWithdraw! ", e);
					try {
						    con.rollback();
						} catch (SQLException ie) {
							log.log(Level.ERROR, "Can't rollback.", ie);
						}
						throw e;
				} finally {
					try {
						con.setAutoCommit(true);
					} catch (Exception e) {
						log.error("Cannot set back to autoCommit! " + e);
					}
				}
			} else {  // error
				tran.setWireId(null);
				tran.setDescription(error);
				tran.setStatusId(TRANS_STATUS_FAILED);
				tran.setTimeSettled(new Date());
				tran.setUtcOffsetSettled(user.getUtcOffset());
				TransactionsDAOBase.insert(con, tran);
				log.info("Insert deltapay Withdraw finished with error!");
			}
			try{
				RiskAlertsManagerBase.riskAlertWithdrawalHandler(writerId,error,
					tran.getId(),tran.getAmount(),tran.getUserId(),user.getBalance());
			} catch (Exception e) {
				log.error("Problem with risk Alert handler ", e);
			}
		} catch (SQLException e) {
			log.error("ERROR during deltapay Withdraw!! ", e);
		} finally {
			closeConnection(con);
		}
	}

	/**
	 * Insert BaroPayDeposit transaction
	 * @param depositVal
	 * @param writerId
	 * @param user
	 * @param language
	 * @param hpURL
	 * @param source
	 * @param formName
	 * @param imagesDomain
	 * @return tran as Transaction
	 * @throws SQLException
	 */
//	public static BaroPayInfo insertBaroPayDeposit(String depositVal, long writerId, User user, String ipAddress, String landLinePhone, 
//												   Map<String, MessageToFormat> mtf, BaroPayInfo bpInfo, String senderName, long loginId) throws SQLException {
//
//		if (log.isInfoEnabled()) {
//			String ls = System.getProperty("line.separator");
//			log.info(ls + "Insert BaroPay transaction deposit:" + ls +
//                    "amount: " + depositVal + ls +
//                    "writer: " + writerId + ls +
//                    "userId: " + user.getId() + ls);
//		}
//
//		Transaction tran = null;
//		String error = null;
//		Connection con = getConnection();
//
//		try {
//			long deposit = CommonUtil.calcAmount(depositVal);
//			tran = new Transaction();
//			error = validateDepositLimits(mtf, user, deposit, tran, true);			
//			tran.setAmount(CommonUtil.calcAmount(depositVal));
//			tran.setComments(null);
//			tran.setCreditCardId(null);
//			tran.setIp(ipAddress);
//			tran.setProcessedWriterId(writerId);
//			tran.setChequeId(null);
//			tran.setTimeSettled(null);
//			tran.setTimeCreated(new Date());
//			tran.setTypeId(TRANS_TYPE_BAROPAY_DEPOSIT);
//			tran.setUserId(user.getId());
//			tran.setWriterId(writerId);
//			tran.setChargeBackId(null);
//			tran.setStatusId(TRANS_STATUS_STARTED);
//			tran.setCurrency(user.getCurrency());
//			tran.setUtcOffsetCreated(user.getUtcOffset());
//			tran.setUtcOffsetSettled(null);
//			tran.setClearingProviderId(ClearingManager.BAROPAY_PROVIDER_ID);
//			if (null != error) {
//				tran.setStatusId(TRANS_STATUS_FAILED);
//				tran.setDescription(error);
//				tran.setTimeSettled(new Date());
//				tran.setUtcOffsetSettled(user.getUtcOffset());
//			} else {
//				tran.setStatusId(TRANS_STATUS_AUTHENTICATE);
//			}
//			tran.setLoginId(loginId);
//			TransactionsDAOBase.insert(con, tran);
//			bpInfo = new BaroPayInfo(user, tran, landLinePhone);
//			bpInfo.setDepositAmount(depositVal);
//			ClearingManager.setBaroPayDepositDetails(bpInfo, ClearingManager.BAROPAY_PROVIDER_ID);
//	        //insert BaroPay Request details
//	        BaroPayRequest baroPayRequest = new BaroPayRequest();
//	        baroPayRequest.setPhone(landLinePhone);
//	        baroPayRequest.setSender(senderName);
//	        baroPayRequest.setTransactionId(tran.getId());
//	        BaroPayDAOBase.insertBaroPayRequest(con, baroPayRequest);
//	        if (tran.getStatusId() == TRANS_STATUS_AUTHENTICATE) {
//	        	boolean timeout = false; // check timeout when we connect to BaroPayProvider.
//                try {
//                      ClearingManager.authorizeBaroPayDeposit(bpInfo, ClearingManager.BAROPAY_PROVIDER_ID); // request and get first response from BaroPay
//                      } catch (ClearingException ce) { // Clearing failed
//                            if (ce.getCause() instanceof SocketTimeoutException) {
//                                  timeout = true;
//                            }
//                            log.error("Failed to authorize transaction: " + bpInfo.toString(), ce);
//                            bpInfo.setResult("999");
//                            bpInfo.setSuccessful(false);
//                            bpInfo.setMessage(ce.getMessage());
//                      }
//                String comment = "";
//                      if (null != bpInfo.getResult()) {
//                            comment += bpInfo.getResult() + "|";
//                      }
//                      if (null != bpInfo.getMessage()) {
//                            comment += bpInfo.getMessage() + "|";
//                      }
//                      if (null != bpInfo.getUserMessage()) {
//                            comment += bpInfo.getUserMessage() + "|";
//                      }
//                      if (comment.length() > 0) {
//                            comment = comment.substring(0, comment.length()-1);
//                      }
//                      tran.setComments(comment);
//                      if (timeout) {
//                            if (log.isInfoEnabled()) {
//                                  String ls = System.getProperty("line.separator");
//                                  log.info("Transaction failed BaroPayDeposit! " + bpInfo.toString() + ls);
//                            }
//                            tran.setStatusId(TRANS_STATUS_FAILED);
//                            mtf.put("", new MessageToFormat(ConstantsBase.ERROR_CLEARING_TIMEOUT, null));
//                            tran.setDescription(ConstantsBase.ERROR_CLEARING_TIMEOUT);
//                            // update transaction on failed cases
//                            tran.setTimeSettled(new Date());
//                            tran.setUtcOffsetSettled(user.getUtcOffset());
//                            TransactionsDAOBase.update(con, tran);
//                      }
//		        BaroPayResponse baroPayResponse = new BaroPayResponse();
//		        long providerTransactionId = 0;
//		        if (null != bpInfo.getProviderTransactionId()) {
//		        	providerTransactionId = Long.valueOf(bpInfo.getProviderTransactionId());
//		        }
//		        baroPayResponse.setPaymentId(providerTransactionId);
//		        int status = 1;
//		        if (null != bpInfo.getResult()) {
//		        	status = Integer.valueOf(bpInfo.getResult());
//		        }
//		        baroPayResponse.setStatus(status);
//		        baroPayResponse.setDescription(bpInfo.getMessage());			      
//		        baroPayResponse.setTransactionId(tran.getId());
//		        baroPayResponse.setXmlResponse(bpInfo.getXmlResponse());			        
//		        BaroPayDAOBase.insertBaroPayResponse(con, baroPayResponse);
//		        log.info("BaroPay fields: " + "\n" +
//		        			"DepositID = " + bpInfo.getTransactionId() + "\n" +
//		        			"MemberCode = " + bpInfo.getUserId() + "\n" +
//		        			"MobileNumber = " + bpInfo.getUserPhone() + "\n" +
//		        			"Currency = " + bpInfo.getCurrencySymbol() + "\n" +
//		        			"Amount = " + bpInfo.getDepositAmount() + "\n" +
//		        			"SenderName = " + bpInfo.getSenderName() + "\n" +
//		        			"returnURL=" + bpInfo.getStatusURL() + "\n" +
//		        			"AgentID=" + bpInfo.getMerchantAccount());
//				log.info("insert BaroPay transaction finished successfully! ");
//	        }
//			if (null != error){ // deposit failed due to minimum limitation
//				try {
//					PopulationsManagerBase.deposit(user.getId(), false, writerId, tran);
//				} catch (Exception e) {
//					log.warn("Problem with population failed deposit event " + e);
//				}
//			}
//		} catch (Exception e) {
//			log.error("Problem in BaroPAY!! " + e);
//			return null;
//		} finally {
//			closeConnection(con);
//			TransactionsManagerBase.afterDepositAction(user, tran, TransactionSource.TRANS_FROM_MOBILE);
//		}		
//		return bpInfo;
//	}

	/**
	 * insert BaroPay Withdraw
	 * @param error
	 * 		if there is an error with the request
	 * @param user
	 * 		the user that creat wire request
	 * @param writerId
	 * 		the writer id
	 * @param feeCancel
	 * 		if this bank wire with fee or not
	 * @return 
	 * @throws SQLException
	 */	
    public static Transaction insertBaroPayWithdraw(String amount, String error, User user, long writerId, boolean feeCancel, String bankName, String accountOwner, 
    										 String accountNum, String ipAddress, Map<String, MessageToFormat> mtf, long loginId)	throws SQLException {

		if (log.isEnabledFor(Level.INFO)) {
			String ls = System.getProperty("line.separator");
			log.log(Level.INFO,
							"insertBaroPayWithdraw " + ls + "User:" + user.getUserName() + ls + "famount:" + amount);
		}
		Transaction tran = new Transaction();
		Connection con = getConnection();

		try {
			UsersDAOBase.getByUserName(con, user.getUserName(), user);
			long newBalance = user.getBalance() - CommonUtil.calcAmount(amount);			
            
			tran.setAmount(CommonUtil.calcAmount(amount));
			tran.setComments(null);
			tran.setCreditCardId(null);
			tran.setIp(ipAddress);
			tran.setProcessedWriterId(writerId);
			tran.setCreditWithdrawal(true);// for deltaPAy cft -- only true
			tran.setTimeCreated(new Date());
			tran.setUtcOffsetCreated(user.getUtcOffset());
            tran.setTypeId(TRANS_TYPE_BAROPAY_WITHDRAW);
			tran.setUserId(user.getId());
			tran.setWriterId(writerId);
			tran.setChargeBackId(null);
			tran.setReceiptNum(null);
			tran.setChequeId(null);
			tran.setFeeCancel(feeCancel);

			if (error == null) { // no error
				try {
					con.setAutoCommit(false);
					tran.setDescription(null);
					tran.setStatusId(TRANS_STATUS_REQUESTED);
					tran.setTimeSettled(null);
					tran.setLoginId(loginId);
					UsersDAOBase.addToBalance(con, user.getId(), -CommonUtil.calcAmount(amount));
					user.setBalance(newBalance);
					TransactionsDAOBase.insert(con, tran);
					// insert baroPayRequest.
					BaroPayRequest baroPayRequest = new BaroPayRequest();
					baroPayRequest.setBankName(bankName);
					baroPayRequest.setAccountOwner(accountOwner);
					baroPayRequest.setAccountNumber(accountNum);
					baroPayRequest.setTransactionId(tran.getId());
					BaroPayDAOBase.insertBaroPayRequest(con, baroPayRequest);
					GeneralDAO.insertBalanceLog(con,writerId,tran.getUserId(), ConstantsBase.TABLE_TRANSACTIONS, tran.getId(), ConstantsBase.LOG_BALANCE_BAROPAY_WITHDRAW, user.getUtcOffset());
					con.commit();
					PopulationEntryBase popUser = PopulationsManagerBase.getPopulationUserByUserId(user.getId());
					if (popUser.getEntryTypeId() != PopulationsManagerBase.ENTRY_TYPE_REMOVE_PERMANENTLY) {
						PopulationsManagerBase.insertIntoPopulation(con, PopulationsManagerBase.POP_TYPE_OPEN_WITHDRAW, 0, 0, user.getId(), writerId, popUser);
					}
					log.info("insert BaroPayWithdraw finished successfully!");
				} catch (SQLException e) {
					log.error("ERROR during BaroPayWithdraw! ", e);
					try {
						    con.rollback();
						} catch (SQLException ie) {
							log.log(Level.ERROR, "Can't rollback.", ie);
						}
						throw e;
				} finally {
					try {
						con.setAutoCommit(true);
					} catch (Exception e) {
						log.error("Cannot set back to autoCommit! " + e);
					}
				}
			} else {  // error
				tran.setWireId(null);
				tran.setDescription(error);
				tran.setStatusId(TRANS_STATUS_FAILED);
				tran.setTimeSettled(new Date());
				tran.setUtcOffsetSettled(user.getUtcOffset());
				TransactionsDAOBase.insert(con, tran);
				log.info("Insert BaroPayWithdraw finished with error!");
			}				
			try{
				RiskAlertsManagerBase.riskAlertWithdrawalHandler(writerId,error,
					tran.getId(),tran.getAmount(),tran.getUserId(),user.getBalance());
			} catch (Exception e) {
				log.error("Problem with risk Alert handler ", e);
			}
		} catch (SQLException e) {
			log.error("ERROR during BaroPayWithdraw!! ", e);
		} finally {
			closeConnection(con);
		}
		return tran;
	}

	public static CDPayInfo insertCDPayDeposit(String depositVal, long writerId, User user, Map<String, MessageToFormat> mtf, String ipAddress, int selectorId,
											   String hpURL, String imagesDomain, CDPayInfo cdpayInfo, long loginId) throws SQLException {
		if (log.isInfoEnabled()) {
			String ls = System.getProperty("line.separator");
			log.info(ls + "Insert CDPay transaction deposit:" + ls +
	                "amount: " + depositVal + ls +
	                "writer: " + writerId + ls +
	                "userId: " + user.getId() + ls);
		}
	
		Transaction tran = null;
		String error = null;
		Connection con = getConnection();
	
		try {
			long deposit = CommonUtil.calcAmount(depositVal);
			tran = new Transaction();
			error = validateDepositLimits(mtf, user, deposit, tran, true);
			tran.setAmount(CommonUtil.calcAmount(depositVal));
			tran.setComments(null);
			tran.setCreditCardId(null);
			tran.setDescription(null);
			tran.setIp(ipAddress);
			tran.setProcessedWriterId(writerId);
			tran.setChequeId(null);
			tran.setTimeSettled(null);
			tran.setTimeCreated(new Date());
			tran.setTypeId(TRANS_TYPE_CUP_CHINA_DEPOSIT);
			tran.setUserId(user.getId());
			tran.setWriterId(writerId);
			tran.setChargeBackId(null);
			tran.setStatusId(TRANS_STATUS_STARTED);
			tran.setCurrency(user.getCurrency());
			tran.setUtcOffsetCreated(user.getUtcOffset());
			tran.setUtcOffsetSettled(null);
			tran.setClearingProviderId(ClearingManager.CDPAY_PROVIDER_ID);
			tran.setSelectorId(selectorId);
			if (null != error) {
				tran.setStatusId(TRANS_STATUS_FAILED);
				tran.setDescription(error);
				tran.setTimeSettled(new Date());
				tran.setUtcOffsetSettled(user.getUtcOffset());
			} else {
				tran.setStatusId(TRANS_STATUS_AUTHENTICATE);
			}
			tran.setLoginId(loginId);
			//insert cdpay to transaction
			TransactionsDAOBase.insert(con, tran);
			cdpayInfo = new CDPayInfo(user, tran, user.getLocale().getLanguage(), hpURL, user.getEmail(), depositVal, CommonUtil.getIsLive(), imagesDomain);
			ClearingManager.setCDPayDepositDetails(cdpayInfo, ClearingManager.CDPAY_PROVIDER_ID);
			//Not sending user details to CDPay.
			cdpayInfo = setCDPayInfoToSend(cdpayInfo);
			String control = cdpayInfo.crateCDPayChecksum(); //Hash control msg.
			cdpayInfo.setControl(control);
			cdpayInfo.setMerchantOrder(String.valueOf(tran.getId()));
	        log.info("\nCDPay Fields: " + "\n" +		        			
	        			"RedirectURL - POST to = " + cdpayInfo.getRedirectURL() + "\n" +
	        			"Mendatory Fields: \n" +	        			
		        			"version=" + cdpayInfo.getVersion()  + "\n" +
		        			"merchant_account = " + cdpayInfo.getMerchantAccount() + "\n" +
		        			"merchant_order = " + cdpayInfo.getTransactionId() + "\n" +
		        			"merchant_product_desc = " + cdpayInfo.getMerchantProductDesc() + "\n" +
		        			"first_name=" + cdpayInfo.getFirstName()  + "\n" +
		        			"last_name=" + cdpayInfo.getLastName()  + "\n" +
		        			"address1=" + cdpayInfo.getAddress()  + "\n" +
		        			"city=" + cdpayInfo.getCity()  + "\n" +
		        			"zip_code=" + cdpayInfo.getZip()  + "\n" +
		        			"country=" + cdpayInfo.getCountryA2()  + "\n" +
		        			"phone=" + cdpayInfo.getUserPhone()  + "\n" +
		        			"email=" + cdpayInfo.getEmail()  + "\n" +
		        			"amount=" + cdpayInfo.getDepositAmount() + "\n" +
		        			"currency=" + cdpayInfo.getCurrencySymbol() + "\n" +
		        			"ipaddress=" + cdpayInfo.getIp() + "\n" +
		        			"returnURL=" + cdpayInfo.getReturnURL() + "\n" +
		        			"control=" + cdpayInfo.getControl()  + "\n" +
	        			"Optional Fields:\n" +
		        			"server_return_url="  + cdpayInfo.getServerReturnURL()  + "\n" +
		        			"bankcode="  + cdpayInfo.getBankcode()  + "\n");			
			log.info("insert CDPay transaction finished successfully! ");
			if (null != error){ // deposit failed due to minimum limitation
				try {
					PopulationsManagerBase.deposit(user.getId(), false, writerId, tran);
				} catch (Exception e) {
					log.warn("Problem with population failed deposit event " + e);
				}
			}
		} catch (Exception e) {
			return null;
		} finally {
			closeConnection(con);
		}
		return cdpayInfo;
	}
	
	/**
	 * @param cdpayInfo
	 * Do not need to send user details to CDPay clearing provider.
	 * @return
	 */
	private static CDPayInfo setCDPayInfoToSend(CDPayInfo cdpayInfo) {
		CDPayInfo cdpayInfoToSend = cdpayInfo;
		cdpayInfoToSend.setFirstName("any");
		cdpayInfoToSend.setLastName("option");
		cdpayInfoToSend.setAddress("khtur");
		cdpayInfoToSend.setCity("c");
		cdpayInfoToSend.setZip("z");
		cdpayInfoToSend.setCountryA2("CN");
		cdpayInfoToSend.setUserPhone("0");
		cdpayInfoToSend.setEmail("support@anyoption.com");
		return cdpayInfoToSend;
	}
	
	public static Distribution getDistributedProviderId(DistributionType type, long userId) throws SelectorException, SQLException {
		// prepare selectors - mobile is exception to normal distribution
		// only MobileSupportSelector is used
		MobileSupportSelector mss = new MobileSupportSelector();
		ArrayList<Selector<Long>> selectors = new ArrayList<Selector<Long>>();
		selectors.add(mss);
		DistributionMaker distributor = new DistributionMaker(selectors, type);
		return distributor.getClearingProviderId(userId);
	}
	
	private static String velocityCheck(long userId) throws SQLException {
		String resault = null;
		Connection con = getConnection();			
		try {
			resault = TransactionsDAOBase.velocityCheck(con, userId);
		} finally {
			closeConnection(con);
		}
		return resault;
	}
	
	private static void sendVelocityErrorMail(String error,String userName, Long skin )
	{
		try {
			HashMap<String, String> params = null;
			params = new HashMap<String, String>();
			//collect the parameters we need for velocity error support email
					String velocityEmail = null;
					String mailTo = null;
					if(skin == Skin.SKIN_ETRADER) {
						velocityEmail= CommonUtil.getConfig("email.to.etrader");
					}else if(skin == Skin.SKIN_CHINESE) {
						velocityEmail= CommonUtil.getConfig("email.to.anyoption.zh");
					}else if(skin == Skin.SKIN_KOREAN) {
						velocityEmail= CommonUtil.getConfig("email.to.anyoption.kr");
					}else {
						velocityEmail= CommonUtil.getConfig("email.to.anyoption");
					}
					mailTo = velocityEmail + ", " + CommonUtil.getConfig("email.to.velocity.person");
					params.put(SendDepositErrorEmail.PARAM_EMAIL, velocityEmail);	
					params.put(SendDepositErrorEmail.PARAM_USER_NAME, userName);
					params.put(SendDepositErrorEmail.PARAM_VELOCITY_DESCRIPTION,CommonUtil.getConfig(error));
					params.put(SendDepositErrorEmail.PARAM_SKIN,""+ skin+"");
					params.put(SendDepositErrorEmail.MAIL_TO, mailTo);
					params.put(SendDepositErrorEmail.MAIL_SUBJECT ," Velocity check: " + CommonUtil.getConfig(error));
					
					new SendDepositErrorEmail(params).start();
		
	    } catch (Exception e) {
		    log.error("Could not send velocity email.", e);
   }
}		

	public static long getCUPWithdrawalAmount(long userId, boolean countBeforeCUP) throws SQLException {
		Connection con = null;
		try {
			con = getConnection();
			return TransactionsDAOBase.getCUPWithdrawalAmount(con, userId, countBeforeCUP);
		} finally {
			closeConnection(con);
		}
	}

	/**
	 * Method that returns the last user deposit
	 * 
	 * @param userId
	 * @return the last user deposit or empty transaction if user has no deposits
	 */
	public static Transaction getLastUserDeposit(long userId) {
		Connection con = null;
		try {
			con = getConnection();
			return TransactionsDAOBase.getLastUserDeposit(con, userId);
		} catch (SQLException e) {
			log.debug("Can't get last user deposit", e);
			return new Transaction();
		} finally {
			closeConnection(con);
		}
	}
	
	public static Transaction getCopyopCoinsTransaction(long amount, long writerId, com.anyoption.common.beans.User user, String comments, String ip) {
        Transaction t = new Transaction();
        t.setUserId(user.getId());
        t.setAmount(amount);
        t.setTypeId(TransactionsManagerBase.TRANS_TYPE_COPYOP_COINS_TO_CASH);
        t.setStatusId(TransactionsManagerBase.TRANS_STATUS_SUCCEED);        
        t.setIp(ip);
        t.setComments(comments);
        t.setTimeSettled(new Date());
        t.setTimeCreated(new Date());
        t.setUtcOffsetCreated(user.getUtcOffset());
        t.setUtcOffsetSettled(user.getUtcOffset());
        t.setWriterId(writerId);
        return t;
	}

	/**
	 * Direct banking deposit with PowerPay service
	 * 
	 * @param redirectUrl
	 * 
	 * @throws SQLException
	 */
	public static Transaction insertPowerpayDeposit(Map<String, MessageToFormat> mtf, String hpUrl, String ipAddress, String depositVal, long writerId,
													User user, String bankCode, String iban, long paymentTypeId,
													String beneficiaryName, String homePageUrl, long loginId) throws SQLException {
		if (log.isInfoEnabled()) {
			String ls = System.getProperty("line.separator");
			log.info(ls + "Insert deposit:" + ls + "amount: " + depositVal + ls + "writer: " + writerId + ls + "userId: " + user.getId()
						+ ls);
		}
		WireBase wire = null;    // saving bank properties
		Connection con = null;
		try {
			con = getConnection();
			long deposit = CommonUtil.calcAmount(depositVal);
			Transaction tran = new Transaction();
			String error = validateDepositLimits(mtf, user, deposit, tran, true);
			tran.setAmount(CommonUtil.calcAmount(depositVal));
			tran.setComments("");
			tran.setCreditCardId(null);
			tran.setCc4digit(null);
			tran.setDescription(null);
			tran.setIp(ipAddress);
			tran.setProcessedWriterId(writerId);
			tran.setChequeId(null);
			tran.setTimeSettled(null);
			tran.setTimeCreated(new Date());
			tran.setTypeId(TRANS_TYPE_DIRECT_BANK_DEPOSIT);
			tran.setUserId(user.getId());
			tran.setWriterId(writerId);
			tran.setWireId(null);
			tran.setChargeBackId(null);
			tran.setStatusId(TRANS_STATUS_STARTED);
			tran.setCurrency(user.getCurrency());
			tran.setCurrencyId(user.getCurrency().getId());
			tran.setUtcOffsetCreated(user.getUtcOffset());
			tran.setPaymentTypeId(paymentTypeId);
			tran.setLoginId(loginId);

			wire = new WireBase();
			wire.setIban(iban);
			wire.setBankCode(bankCode);
			wire.setBeneficiaryName(beneficiaryName);
			WiresDAOBase.insertBankWire(con, wire);

			tran.setWireId(new BigDecimal(wire.getId()));
			TransactionsDAOBase.insert(con, tran);
			if (log.isInfoEnabled()) {
				String ls = System.getProperty("line.separator");
				log.info("Transaction record inserted: " + ls + tran.toString() + ls);
			}
			boolean timeout = false;
			try {
				if (null == error) {  // start clearing
					InatecInfo ci = new InatecInfo(user, tran, wire, 0);
					if (log.isInfoEnabled()) {
						String ls = System.getProperty("line.separator");
						log.info("Transaction Clearing started: Clearing Info: " + ci.toString() + ls);
					}
					
					try {
						Powerpay21ClearingProvider powerpay21 =  (Powerpay21ClearingProvider) ClearingManager.getClearingProviders().get(ClearingManager.POWERPAY21_PROVIDER_ID_APS);
						ClearingInfo result = null;
						if(paymentTypeId == 4) { // Eps
							result = powerpay21.directEpsDeposit(hpUrl, deposit, beneficiaryName, tran.getId(), user.getEmail());
						} else if(paymentTypeId ==  3) { // Giropay
							result = powerpay21.directGiropayDeposit(hpUrl, deposit, iban /*iban*/, bankCode, beneficiaryName, tran.getId(), user.getEmail());
						} else if(paymentTypeId == 2) {  // Sofort
							result = powerpay21.directSofortDeposit(hpUrl, deposit, iban /*iban*/, bankCode, beneficiaryName, tran.getId(), user.getEmail());
						} else {
							result = new ClearingInfo();
							tran.setStatusId(TRANS_STATUS_FAILED);
							tran.setComments("Unknown paymentTypeId:" + paymentTypeId);
							log.error("Unknown paymentTypeId:" + paymentTypeId);
						}
		
						String redirectUrl = "";
						if(result.getAcsUrl() != null) {
							redirectUrl = URLDecoder.decode(result.getAcsUrl(), "UTF-8");
						}
						log.info("Redirecting url is:" + redirectUrl );
						
						if(result.isSuccessful()) {
							tran.setStatusId(TRANS_STATUS_AUTHENTICATE);
						} else {
							tran.setStatusId(TRANS_STATUS_FAILED);
						}
						tran.setClearingProviderId(ClearingManager.POWERPAY21_PROVIDER_ID_APS);
						tran.setRedirectUrl(redirectUrl);
						tran.setXorIdAuthorize(result.getProviderTransactionId());
						tran.setComments(result.getMessage());
					} catch (ClearingException ce) {
						tran.setStatusId(TRANS_STATUS_FAILED);
						tran.setDescription(ce.getMessage());
						log.error("Error! problem with Powerpay21, transaction: " + tran.getId(), ce);
					}
				} else {  // error found
					tran.setStatusId(TRANS_STATUS_FAILED);
					tran.setDescription(error);
					tran.setTimeSettled(new Date());
					tran.setUtcOffsetSettled(user.getUtcOffset());
				}
					
				TransactionsDAOBase.update(con, tran);
				
				log.info("Insert Powerpay finished with status " + tran.getStatusId());
			} catch (Exception exp) {
				log.error("Exception in insert Powerpay! ", exp);
			}
			afterDepositAction(user, tran, TransactionSource.TRANS_FROM_WEB);
			String formattedDepositVal = CommonUtil.formatCurrencyAmountAO(CommonUtil.calcAmount(depositVal), tran.getCurrencyId());
			if (tran.getStatusId() == TRANS_STATUS_SUCCEED) {
				String[] params = new String[1];
				params[0] = formattedDepositVal;
				String successMsg = "deposit.success";
				mtf.put("", new MessageToFormat(successMsg, null));
				try {
					PopulationsManagerBase.deposit(user.getId(), true, writerId, tran);
				} catch (Exception e) {
					log.warn("Problem with population succeed deposit event " + e);
				}
				try {
					afterTransactionSuccess(tran.getId(), user.getId(), tran.getAmount(), writerId, loginId);
				} catch (Exception e) {
					log.error("Problem processing bonuses after success transaction", e);
				}
				try {
					CommonUtil.alertOverDepositLimitByEmail(tran, user);
				} catch (RuntimeException e) {
					log.error("Exception sending email alert to traders! ", e);
				}
				return tran;
			} else if (tran.getStatusId() == TRANS_STATUS_AUTHENTICATE) {  // banking redirect
				return tran;
			} else { // deposit failed
				try {
					try {
						PopulationsManagerBase.deposit(user.getId(), false, writerId, tran);
					} catch (Exception e) {
						log.warn("Problem with population failed deposit event " + e);
					}
					if (user.getClassId() != ConstantsBase.USER_CLASS_TEST) {
						String email;
						if (!CommonUtil.IsParameterEmptyOrNull(error) && error.equals(ConstantsBase.ERROR_MIN_DEPOSIT)) {
							email = SkinsManagerBase.getSupportEmailBySkinId(user.getSkinId());
						} else {
							if (user.getSkinId() == Skin.SKIN_CHINESE) {
								email = CommonUtil.getProperty("deposit.error.zh.email", null);
							} else {
								email = CommonUtil.getProperty("deposit.error.email", null);
							}
						}

						String subject = CommonUtil.getProperty("deposit.error.email.subject", null) + " "
											+ CommonUtil.getMessage(user.getLanguageId(), "transactions.direct.banking.deposit", null)
											+ ": " + InatecClearingProvider.getMessageClass(tran.getPaymentTypeId());
						// send mail to support
						// collect the parameters we need for the email
						HashMap<String, String> params = null;
						params = new HashMap<String, String>();
						params.put(SendDepositErrorEmail.PARAM_EMAIL, email);
						params.put(SendDepositErrorEmail.PARAM_USER_NAME, user.getUserName());
						params.put(SendDepositErrorEmail.PARAM_TRANSACTION_ID, String.valueOf(tran.getId()));
						params.put(SendDepositErrorEmail.PARAM_AMOUNT, formattedDepositVal);
						params.put(	SendDepositErrorEmail.PARAM_DECLINE_DESCRIPTION,
									CommonUtil.getMessage(tran.getDescription(), null, new Locale(ConstantsBase.LOCALE_DEFAULT)));
						params.put(	SendDepositErrorEmail.PARAM_DECLINE_COMMENTS,
									CommonUtil.getMessage(tran.getComments(), null, new Locale(ConstantsBase.LOCALE_DEFAULT)));
						params.put(SendDepositErrorEmail.PARAM_DATE, tran.getTimeCreatedTxt());
						// Populate also server related params, in order to take out common functionality
						// Use params
						params.put(SendDepositErrorEmail.MAIL_TO, email);
						params.put(	SendDepositErrorEmail.MAIL_SUBJECT,
									subject
											+ " Skin:"
											+ CommonUtil.getMessage(user.getLanguageId(), SkinsManagerBase.getSkin(user.getSkinId())
																											.getDisplayName(), null));
						new SendDepositErrorEmail(params).start();
					}

				} catch (Exception e) {
					log.error("Could not send email.", e);
				}
				return null;
			}
		} finally {
			closeConnection(con);
		}
	}

	
	/**
	 * Direct banking deposit with Inatec service
	 * 
	 * @param redirectUrl
	 * 
	 * @throws SQLException
	 */
	public static Transaction insertDirectDeposit(Map<String, MessageToFormat> mtf, String ipAddress, String depositVal, long writerId,
													User user, String bankCode, String accountNum, long paymentTypeId,
													String beneficiaryName, String homePageUrl, long loginId) throws SQLException {
		if (log.isInfoEnabled()) {
			String ls = System.getProperty("line.separator");
			log.info(ls + "Insert deposit:" + ls + "amount: " + depositVal + ls + "writer: " + writerId + ls + "userId: " + user.getId()
						+ ls);
		}
		WireBase wire = null;    // saving bank properties
		Connection con = null;
		try {
			con = getConnection();
			long deposit = CommonUtil.calcAmount(depositVal);
			Transaction tran = new Transaction();
			String error = validateDepositLimits(mtf, user, deposit, tran, true);
			tran.setAmount(CommonUtil.calcAmount(depositVal));
			tran.setComments("");
			tran.setCreditCardId(null);
			tran.setCc4digit(null);
			tran.setDescription(null);
			tran.setIp(ipAddress);
			tran.setProcessedWriterId(writerId);
			tran.setChequeId(null);
			tran.setTimeSettled(null);
			tran.setTimeCreated(new Date());
			tran.setTypeId(TRANS_TYPE_DIRECT_BANK_DEPOSIT);
			tran.setUserId(user.getId());
			tran.setWriterId(writerId);
			tran.setWireId(null);
			tran.setChargeBackId(null);
			tran.setStatusId(TRANS_STATUS_STARTED);
			tran.setCurrency(user.getCurrency());
			tran.setCurrencyId(user.getCurrency().getId());
			tran.setUtcOffsetCreated(user.getUtcOffset());
			tran.setPaymentTypeId(paymentTypeId);
			tran.setLoginId(loginId);

			wire = new WireBase();
			wire.setAccountNum(accountNum);
			wire.setBankCode(bankCode);
			wire.setBeneficiaryName(beneficiaryName);
			WiresDAOBase.insertBankWire(con, wire);

			tran.setWireId(new BigDecimal(wire.getId()));
			TransactionsDAOBase.insert(con, tran);
			if (log.isInfoEnabled()) {
				String ls = System.getProperty("line.separator");
				log.info("Transaction record inserted: " + ls + tran.toString() + ls);
			}
			boolean timeout = false;
			try {
				if (null == error) {  // start clearing
					InatecInfo ci = new InatecInfo(user, tran, wire, 0);
					if (log.isInfoEnabled()) {
						String ls = System.getProperty("line.separator");
						log.info("Transaction Clearing started: Clearing Info: " + ci.toString() + ls);
					}
					long providerId = ClearingManager.getProvidersIdByPaymentGroupAndBusinessCase()
														.get(SkinsManagerBase.getSkin(user.getSkinId()).getBusinessCaseId())
														.get(ClearingManager.CLEARING_PAYMENT_GROUP_INATEC_ONLINE);
					try {
						ci.setHomePageUrl(homePageUrl);
						ClearingManager.directDeposit(ci, providerId);
					} catch (ClearingException ce) {
						if (ce.getCause() instanceof SocketTimeoutException) {
							timeout = true;
						}
						log.error("Error! problem with directDeposit, transaction: " + ci.toString(), ce);
						ci.setResult("999");
						ci.setSuccessful(false);
						ci.setMessage(ce.getMessage());
					}
					tran.setClearingProviderId(ci.getProviderId());
					tran.setComments(ci.getResult() + "|" + ci.getMessage() + "|" + ci.getUserMessage() + "|"
										+ ci.getProviderTransactionId());
					tran.setXorIdAuthorize(ci.getProviderTransactionId());
					tran.setAuthNumber(ci.getAuthNumber());
					if (ci.isSuccessful()) {   // success
						if (log.isInfoEnabled()) {
							log.info("Transaction successful from Inatec");
						}
						if (null != ci.getRedirectUrl()) {   // authenticate status(inatec status was pending), bank redirect needed
							tran.setStatusId(TRANS_STATUS_AUTHENTICATE);
							tran.setInatecRedirectSecret(ci.getRedirectSecret());
							tran.setInatecAccountNum(accountNum);
							tran.setInatecBankSortCode(bankCode);
							tran.setBeneficiaryName(beneficiaryName);
							tran.setTimeSettled(null);
							tran.setRedirectUrl(ci.getRedirectUrl());
							TransactionsDAOBase.update(con, tran);
						} else {  // without bank redirect
							tran.setStatusId(TRANS_STATUS_SUCCEED);
							tran.setTimeSettled(new Date());
							tran.setUtcOffsetSettled(user.getUtcOffset());
							try {
								con.setAutoCommit(false);
								UsersDAOBase.addToBalance(con, user.getId(), CommonUtil.calcAmount(depositVal));
								GeneralDAO.insertBalanceLog(con, writerId, tran.getUserId(), ConstantsBase.TABLE_TRANSACTIONS,
															tran.getId(), ConstantsBase.LOG_BALANCE_DIRECT_DEPOSIT, user.getUtcOffset());
								TransactionsDAOBase.update(con, tran);
								con.commit();
							} catch (Exception e) {
								log.error("Exception in success direct deposit Transaction! ", e);
								try {
									con.rollback();
								} catch (Throwable it) {
									log.error("Can't rollback.", it);
								}
							} finally {
								try {
									con.setAutoCommit(true);
								} catch (Exception e) {
									log.error("Can't set back to autocommit.", e);
								}
							}
						}
					} else {  // failed
						if (log.isInfoEnabled()) {
							String ls = System.getProperty("line.separator");
							log.info("Transaction failed InatecCheckout! " + ci.toString() + ls);
						}
						tran.setStatusId(TRANS_STATUS_FAILED);
						mtf.put("", new MessageToFormat("error.creditcard.unapproved", null));
						if (timeout) {
							tran.setDescription(ConstantsBase.ERROR_CLEARING_TIMEOUT);
						} else {
							tran.setDescription(ConstantsBase.ERROR_FAILED_CLEARING);
						}
					}
				} else {  // error found
					tran.setStatusId(TRANS_STATUS_FAILED);
					tran.setDescription(error);
				}
				if (tran.getStatusId() == TRANS_STATUS_FAILED) {   // update transaction on failed cases
					tran.setTimeSettled(new Date());
					tran.setUtcOffsetSettled(user.getUtcOffset());
					TransactionsDAOBase.update(con, tran);
				}
				// set description if needed
				String description = (null == tran.getDescription()) ? "" : ", description: " + tran.getDescription();
				log.info("insert directDeposit finished successfully! " + description);
			} catch (Exception exp) {
				log.error("Exception in insert directDeposit! ", exp);
			}
			afterDepositAction(user, tran, TransactionSource.TRANS_FROM_WEB);
			String formattedDepositVal = CommonUtil.formatCurrencyAmountAO(CommonUtil.calcAmount(depositVal), tran.getCurrencyId());
			if (tran.getStatusId() == TRANS_STATUS_SUCCEED) {
				String[] params = new String[1];
				params[0] = formattedDepositVal;
				String successMsg = "deposit.success";
				mtf.put("", new MessageToFormat(successMsg, null));
				try {
					PopulationsManagerBase.deposit(user.getId(), true, writerId, tran);
				} catch (Exception e) {
					log.warn("Problem with population succeed deposit event " + e);
				}
				try {
					afterTransactionSuccess(tran.getId(), user.getId(), tran.getAmount(), writerId, loginId);
				} catch (Exception e) {
					log.error("Problem processing bonuses after success transaction", e);
				}
				try {
					CommonUtil.alertOverDepositLimitByEmail(tran, user);
				} catch (RuntimeException e) {
					log.error("Exception sending email alert to traders! ", e);
				}
				return tran;
			} else if (tran.getStatusId() == TRANS_STATUS_AUTHENTICATE) {  // banking redirect
				return tran;
			} else { // deposit failed
				try {
					try {
						PopulationsManagerBase.deposit(user.getId(), false, writerId, tran);
					} catch (Exception e) {
						log.warn("Problem with population failed deposit event " + e);
					}
					if (user.getClassId() != ConstantsBase.USER_CLASS_TEST) {
						String email;
						if (!CommonUtil.IsParameterEmptyOrNull(error) && error.equals(ConstantsBase.ERROR_MIN_DEPOSIT)) {
							email = SkinsManagerBase.getSupportEmailBySkinId(user.getSkinId());
						} else {
							if (user.getSkinId() == Skin.SKIN_CHINESE) {
								email = CommonUtil.getProperty("deposit.error.zh.email", null);
							} else {
								email = CommonUtil.getProperty("deposit.error.email", null);
							}
						}

						String subject = CommonUtil.getProperty("deposit.error.email.subject", null) + " "
											+ CommonUtil.getMessage(user.getLanguageId(), "transactions.direct.banking.deposit", null)
											+ ": " + InatecClearingProvider.getMessageClass(tran.getPaymentTypeId());
						// send mail to support
						// collect the parameters we need for the email
						HashMap<String, String> params = null;
						params = new HashMap<String, String>();
						params.put(SendDepositErrorEmail.PARAM_EMAIL, email);
						params.put(SendDepositErrorEmail.PARAM_USER_NAME, user.getUserName());
						params.put(SendDepositErrorEmail.PARAM_TRANSACTION_ID, String.valueOf(tran.getId()));
						params.put(SendDepositErrorEmail.PARAM_AMOUNT, formattedDepositVal);
						params.put(	SendDepositErrorEmail.PARAM_DECLINE_DESCRIPTION,
									CommonUtil.getMessage(tran.getDescription(), null, new Locale(ConstantsBase.LOCALE_DEFAULT)));
						params.put(	SendDepositErrorEmail.PARAM_DECLINE_COMMENTS,
									CommonUtil.getMessage(tran.getComments(), null, new Locale(ConstantsBase.LOCALE_DEFAULT)));
						params.put(SendDepositErrorEmail.PARAM_DATE, tran.getTimeCreatedTxt());
						// Populate also server related params, in order to take out common functionality
						// Use params
						params.put(SendDepositErrorEmail.MAIL_TO, email);
						params.put(	SendDepositErrorEmail.MAIL_SUBJECT,
									subject
											+ " Skin:"
											+ CommonUtil.getMessage(user.getLanguageId(), SkinsManagerBase.getSkin(user.getSkinId())
																											.getDisplayName(), null));
						new SendDepositErrorEmail(params).start();
					}

				} catch (Exception e) {
					log.error("Could not send email.", e);
				}
				return null;
			}
		} finally {
			closeConnection(con);
		}
	}
	
	
	/**
	 * Deposit with Neteller service (direct integration)
	 * 
	 * @throws SQLException
	 */
	public static Transaction insertNetellerDeposit(Map<String, MessageToFormat> mtf, String ipAddress, String depositVal, long writerId,
													User user, String email, String verificationCode, long loginId) throws SQLException {
		if (log.isInfoEnabled()) {
			String ls = System.getProperty("line.separator");
			log.info(ls + "Insert Neteller deposit:" + ls + "amount: " + depositVal + ls + "writer: " + writerId + ls + "userId: " + user.getId()
						+ ls);
		}

		Connection con = null;
		try {
			con = getConnection();
			long deposit = CommonUtil.calcAmount(depositVal);
			Transaction tran = new Transaction();
			String error = validateDepositLimits(mtf, user, deposit, tran, true);
			
			tran.setAmount(CommonUtil.calcAmount(depositVal));
			tran.setComments("");
			tran.setCreditCardId(null);
			tran.setCc4digit(null);
			tran.setDescription(null);
			tran.setIp(ipAddress);
			tran.setProcessedWriterId(writerId);
			tran.setChequeId(null);
			tran.setTimeSettled(null);
			tran.setTimeCreated(new Date());
			tran.setTypeId(TRANS_TYPE_NETELLER_DEPOSIT);
			tran.setUserId(user.getId());
			tran.setWriterId(writerId);
			tran.setWireId(null);
			tran.setChargeBackId(null);
			tran.setStatusId(TRANS_STATUS_STARTED);
			tran.setCurrency(user.getCurrency());
			tran.setCurrencyId(user.getCurrency().getId());
			tran.setUtcOffsetCreated(user.getUtcOffset());
			tran.setLoginId(loginId);

			TransactionsDAOBase.insert(con, tran);
			if (log.isInfoEnabled()) {
				String ls = System.getProperty("line.separator");
				log.info("Transaction record inserted: " + ls + tran.toString() + ls);
			}
			boolean timeout = false;
			try {
				if (null == error) {  // start clearing
					ClearingInfo ci = new ClearingInfo();
					ci.setAmount(tran.getAmount());
					ci.setTransactionId(tran.getId());
					if (log.isInfoEnabled()) {
						String ls = System.getProperty("line.separator");
						log.info("Transaction Clearing started: Clearing Info: " + ci.toString() + ls);
					}
					try {
						ClearingManager.netellerDeposit(ci, user.getCurrency().getCode(), email, verificationCode);
					} catch (ClearingException ce) {
						if (ce.getCause() instanceof SocketTimeoutException) {
							timeout = true;
						}
						log.error("Error! problem with directDeposit, transaction: " + ci.toString(), ce);
						ci.setResult("999");
						ci.setSuccessful(false);
						ci.setMessage(ce.getMessage());
					}
					tran.setClearingProviderId(ci.getProviderId());
					tran.setComments(ci.getResult() + "|" + ci.getMessage() + "|" + ci.getUserMessage() + "|"
										+ ci.getProviderTransactionId());
					tran.setXorIdAuthorize(ci.getProviderTransactionId());
					tran.setAuthNumber(ci.getAuthNumber());
					if (ci.isSuccessful()) {   // success
						if (log.isInfoEnabled()) {
							log.info("Transaction successful from Neteller");
						}
						tran.setStatusId(TRANS_STATUS_SUCCEED);
						tran.setTimeSettled(new Date());
						tran.setUtcOffsetSettled(user.getUtcOffset());
						try {
							con.setAutoCommit(false);
							UsersDAOBase.addToBalance(con, user.getId(), CommonUtil.calcAmount(depositVal));
							GeneralDAO.insertBalanceLog(con, writerId, tran.getUserId(), ConstantsBase.TABLE_TRANSACTIONS,
														tran.getId(), ConstantsBase.LOG_BALANCE_DIRECT_DEPOSIT, user.getUtcOffset());
							TransactionsDAOBase.updateForStatus(con, tran, TRANS_STATUS_STARTED);
							con.commit();
						} catch (Exception e) {
							log.error("Exception in success neteller Transaction! ", e);
							try {
								con.rollback();
							} catch (Throwable it) {
								log.error("Can't rollback.", it);
							}
						} finally {
							try {
								con.setAutoCommit(true);
							} catch (Exception e) {
								log.error("Can't set back to autocommit.", e);
							}
						}
					} else {  // failed
						if (log.isInfoEnabled()) {
							String ls = System.getProperty("line.separator");
							log.info("Transaction failed InatecCheckout! " + ci.toString() + ls);
						}
						tran.setStatusId(TRANS_STATUS_FAILED);
						mtf.put("", new MessageToFormat("error.creditcard.unapproved", null));
						if (timeout) {
							tran.setDescription(ConstantsBase.ERROR_CLEARING_TIMEOUT);
						} else {
							tran.setDescription(ConstantsBase.ERROR_FAILED_CLEARING);
						}
					}
				} else {  // error found
					tran.setStatusId(TRANS_STATUS_FAILED);
					tran.setDescription(error);
				}
				// set description if needed
				String description = (null == tran.getDescription()) ? "" : ", description: " + tran.getDescription();
				log.info("insert directDeposit finished successfully! " + description);
			} catch (Exception exp) {
				log.error("Exception in insert directDeposit! ", exp);
			}
			afterDepositAction(user, tran, TransactionSource.TRANS_FROM_WEB);
			

			if (tran.getStatusId() == TRANS_STATUS_SUCCEED) {
				
				String successMsg = "deposit.success";
				mtf.put("", new MessageToFormat(successMsg, null));
				try {
					PopulationsManagerBase.deposit(user.getId(), true, writerId, tran);
				} catch (Exception e) {
					log.warn("Problem with population succeed deposit event " + e);
				}
				try {
					afterTransactionSuccess(tran.getId(), user.getId(), tran.getAmount(), writerId, loginId);
				} catch (Exception e) {
					log.error("Problem processing bonuses after success transaction", e);
				}
				try {
					CommonUtil.alertOverDepositLimitByEmail(tran, user);
				} catch (RuntimeException e) {
					log.error("Exception sending email alert to traders! ", e);
				}
				return tran;
			} else { // deposit failed
				try {
					
					try {
						PopulationsManagerBase.deposit(user.getId(), false, writerId, tran);
					} catch (Exception e) {
						log.warn("Problem with population failed deposit event " + e);
					}
					if (user.getClassId() != ConstantsBase.USER_CLASS_TEST) {
						String supportEmail; 
						if (!CommonUtil.IsParameterEmptyOrNull(error) && error.equals(ConstantsBase.ERROR_MIN_DEPOSIT)) {
							supportEmail = SkinsManagerBase.getSupportEmailBySkinId(user.getSkinId());
						} else {
							if (user.getSkinId() == Skin.SKIN_CHINESE) {
								supportEmail = CommonUtil.getProperty("deposit.error.zh.email", null);
							} else {
								supportEmail = CommonUtil.getProperty("deposit.error.email", null);
							}
						}

						String subject = CommonUtil.getProperty("deposit.error.email.subject", null) + " "
											+ CommonUtil.getMessage(user.getLanguageId(), "transactions.direct.banking.deposit", null)
											+ ": " + InatecClearingProvider.getMessageClass(tran.getPaymentTypeId());
						// send mail to support
						// collect the parameters we need for the email
						HashMap<String, String> params = null;
						params = new HashMap<String, String>();
						params.put(SendDepositErrorEmail.PARAM_EMAIL, supportEmail);
						params.put(SendDepositErrorEmail.PARAM_USER_NAME, user.getUserName());
						params.put(SendDepositErrorEmail.PARAM_TRANSACTION_ID, String.valueOf(tran.getId()));
						String formattedDepositVal = CommonUtil.formatCurrencyAmountAO(CommonUtil.calcAmount(depositVal), tran.getCurrencyId());
						params.put(SendDepositErrorEmail.PARAM_AMOUNT, formattedDepositVal);
						params.put(	SendDepositErrorEmail.PARAM_DECLINE_DESCRIPTION,
									CommonUtil.getMessage(tran.getDescription(), null, new Locale(ConstantsBase.LOCALE_DEFAULT)));
						params.put(	SendDepositErrorEmail.PARAM_DECLINE_COMMENTS,
									CommonUtil.getMessage(tran.getComments(), null, new Locale(ConstantsBase.LOCALE_DEFAULT)));
						params.put(SendDepositErrorEmail.PARAM_DATE, tran.getTimeCreatedTxt());
						// Populate also server related params, in order to take out common functionality
						// Use params
						params.put(SendDepositErrorEmail.MAIL_TO, supportEmail);
						params.put(	SendDepositErrorEmail.MAIL_SUBJECT,
									subject
											+ " Skin:"
											+ CommonUtil.getMessage(user.getLanguageId(), SkinsManagerBase.getSkin(user.getSkinId())
																											.getDisplayName(), null));
						new SendDepositErrorEmail(params).start();
					}

					tran.setTimeSettled(new Date());
					tran.setUtcOffsetSettled(user.getUtcOffset());
					TransactionsDAOBase.update(con, tran);
					
				} catch (Exception e) {
					log.error("Could not send email.", e);
				}
				return null;
			}
		} finally {
			closeConnection(con);
		}
	}


	/**
	 * Check if the transactions is Inatec online deposit and we didn't get notification from Inatec
	 * @param id transaction id
	 * @return
	 * @throws SQLException
	 */
	public static boolean isInatecNotNotifiedTrx(long id) throws SQLException {
		Connection con = null;
		try {
			con = getConnection();
			return TransactionsDAOBase.isInatecNotNotifiedTrx(con, id);
		} finally {
			closeConnection(con);
		}
	}

    /**
     * Finish Inatec direct banking transaction. this is done after redirect back to our site.
     * @param t Transaction instance from the context
     * @param txId trIx param
     * @param cs check sum param
     * @param writerId the id of the writer the perform the action
     * @param utcOffset utcOffset value
     * @throws ClearingException
     * @throws SQLException
     */
	public static InatecInfo finishInatecTransaction(Transaction t, String txId, String cs, long writerId, String utcOffset,
														boolean sha1Verification, long loginId) throws ClearingException, SQLException {
    	Connection con = null;
    	boolean csVerification = false;
    	InatecInfo info = null;
    	try {
    		if (sha1Verification) {
				// check cs value with Sha1 - Secure Hash Algorithm,
				//Inatec Formula: Checksum: sha1(sha1(txid+"."+REDIRECTSECRET)+"."+sharedsecret)
	    		String csValue = "";
	    		try {
	    			String sharedSecret = ConstantsBase.INATEC_SHARED_SECRET;
	    			if (CommonUtil.getIsLive()) {
	    				sharedSecret = ConstantsBase.INATEC_SHARED_SECRET_LIVE;
	    			}
	    			csValue = Sha1.encode(Sha1.encode(txId + "." + t.getInatecRedirectSecret())
									+ "." + sharedSecret);

	    			if (csValue.equals(cs)) {
	    				csVerification = true;
	    			}
	    		} catch (Exception e) {
	    			log.warn("Error operating Sha1 encode! " + e);
	    			csVerification = false;
				}

	    		if (!csVerification) {
	    			// TODO why proceed without validation?
	    			log.warn("Inatec cs param verification failed!");
	    		}
    		}

    		info = new InatecInfo();
            info.setTransactionId(t.getId());
            info.setProviderId(t.getClearingProviderId());
            info.setAuthNumber(t.getAuthNumber());
            info.setInatecPaymentType(t.getPaymentTypeId());
            ClearingManager.onlineStatusResult(info);

            if (!info.isSuccessful() &&
            		info.isNeedNotification()) {   // pending status in statusRequest response
            									   // trying to run purchase 1 more time for getting final status
            	ClearingManager.onlineStatusResult(info);
            }

            con = getConnection();
            if (info.isSuccessful()) {  // success
                t.setStatusId(TRANS_STATUS_SUCCEED);
                t.setDescription(null);
                t.setXorIdCapture(info.getAuthNumber());
                try {
                	con.setAutoCommit(false);
	                UsersDAOBase.addToBalance(con, t.getUserId(), t.getAmount());
	                GeneralDAO.insertBalanceLog(con, writerId, t.getUserId(), ConstantsBase.TABLE_TRANSACTIONS, t.getId(), ConstantsBase.LOG_BALANCE_DIRECT_DEPOSIT, utcOffset);
	                t.setComments(info.getResult() + "|" + info.getMessage() + "|" + info.getUserMessage() + "|" + info.getProviderTransactionId());
	                TransactionsDAOBase.updatePurchase(con, t.getId(), t.getStatusId(), t.getDescription(), t.getComments(), t.getXorIdCapture(), utcOffset, writerId);
	                con.commit();
                } catch (SQLException e) {
            		log.error("Exception in finishInatecTransaction! success case. ", e);
            		try {
            			con.rollback();
            		} catch (Throwable it) {
            			log.error("Can't rollback.", it);
            		}
            		throw e;
            	} finally {
            		try {
            			con.setAutoCommit(true);
            		} catch (Exception e) {
            			log.error("Can't set back to autocommit.", e);
            		}
            	}
    			try {
    				PopulationsManagerBase.deposit(t.getUserId(), true, writerId, t);
    			} catch (Exception e) {
    				log.warn("Problem with population succeed deposit event " + e);
    			}
                try {
                    afterTransactionSuccess(t.getId(), t.getUserId(), t.getAmount(), writerId, loginId);
                } catch (Exception e) {
                    log.error("Problem processing bonuses after success transaction", e);
                }
            } else {  // failed / in progress
        		if (info.isNeedNotification()) {  // need to pull final status from Inatec
        			t.setStatusId(TRANS_STATUS_IN_PROGRESS);
        			log.debug("Inatec direct deposit trx: " + t.getId() + ", need to pull final result");
        		} else {    // failed
            		t.setStatusId(TRANS_STATUS_FAILED);
            		t.setDescription(null);
        			try {
        				PopulationsManagerBase.deposit(t.getUserId(), false, writerId, t);
        			} catch (Exception e) {
        				log.warn("Problem with population failed deposit event " + e);
        			}
        		}
                t.setComments(info.getResult() + "|" + info.getMessage() + "|" + info.getUserMessage() + "|" + info.getProviderTransactionId());
                TransactionsDAOBase.updatePurchase(con, t.getId(), t.getStatusId(), t.getDescription(), t.getComments(), t.getXorIdCapture(), utcOffset, writerId);
           	}
    	} catch (SQLException e) {
    		log.error("Exception in finishInatecTransaction! ", e);
    		throw e;
    	} finally {
    		closeConnection(con);
    	}
    	return info;
    }
	
    public static Transaction finish3dSecureTransaction(Transaction t, String paRes, long writerId, User user) throws Exception {
        try {

            ClearingInfo info = new ClearingInfo();
            info.setTransactionId(t.getId());
            info.setProviderId(t.getClearingProviderId());
            info.setAuthNumber(t.getXorIdAuthorize());
            info.setAmount(t.getAmount());
            info.setCvv(null);
            info.setPaRes(paRes);
            info.setCurrencyId(user.getCurrencyId());
            info.setCountryA2(user.getCountry().getA2());
            info.setAcquirerResponseId(t.getAcquirerResponseId());
            ClearingManagerBase.purchase(info);
            
            t = finish3dSecureTransaction(t, writerId, user, info);

           
        } catch (Exception e) {
    		log.error("Exception in finish3dSecureTransaction! ", e);
    		t.setStatusId(TRANS_STATUS_FAILED);
    		throw e;
    	} 
        return t;
    }
    
    private static Transaction finish3dSecureTransaction(Transaction t, long writerId, User user, ClearingInfo info) throws ClearingException, SQLException {
    	 Connection conn = null;
         try {
        	 	conn = getConnection();
        	 	
				if (info.isSuccessful()) {
				    t.setStatusId(TRANS_STATUS_SUCCEED);
				    t.setDescription(null);
				    t.setWriterId(writerId);
				    t.setXorIdCapture(info.getProviderTransactionId());
				    t.setCaptureNumber(info.getCaptureNumber());
				    t.setUtcOffsetSettled(user.getUtcOffset());
				    t.setProcessedWriterId(writerId);
				    t.setTimeSettled(Calendar.getInstance().getTime());
				    try {
				    	conn.setAutoCommit(false);
				        UsersDAOBase.addToBalance(conn, t.getUserId(), t.getAmount());
				        GeneralDAO.insertBalanceLog(conn, writerId, t.getUserId(), ConstantsBase.TABLE_TRANSACTIONS, t.getId(), ConstantsBase.LOG_BALANCE_CC_DEPOSIT, user.getUtcOffset());
				        t.setComments(t.getComments()+"["+info.getResult() + "|" + info.getMessage() + "|" + info.getUserMessage() + "|" + info.getProviderTransactionId()+"]");
				        TransactionsDAOBase.updateForStatus(conn, t, TRANS_STATUS_AUTHENTICATE);
				        conn.commit();
				        
			        	CreditCard cc = CreditCardsDAOBase.getById(conn, t.getCreditCardId().longValue());

			            if (t.getStatusId() == TRANS_STATUS_SUCCEED) {
			            	boolean firstRealDeposit = TransactionsDAOBase.isFirstDeposit(conn, user.getId(), 0);
			            	depositSuccess(user, writerId, t, null, null, false, cc, new HashMap<String, MessageToFormat>(), firstRealDeposit, user.getLastLoginId());
			            } else {
			            	depositFailed(user, writerId, t, null, null, false, 0, cc, new HashMap<String, MessageToFormat>());
			            }	
				    } catch (Exception e) {
						log.error("Exception in finish3DTransaction! success case. ", e);
						rollback(conn);
						throw e;
					} finally {
						setAutoCommitBack(conn);
					}
				
				} else {
				    t.setStatusId(TRANS_STATUS_FAILED);
				    t.setDescription(null);
			        t.setComments("["+info.getResult() + "|" + info.getMessage() + "|" + info.getUserMessage() + "|" + info.getProviderTransactionId()+"]");
				    TransactionsDAOBase.updateForStatus(conn, t, TRANS_STATUS_AUTHENTICATE);
				}
         } finally {
        	 closeConnection(conn);
         }
        return t;
    }
}