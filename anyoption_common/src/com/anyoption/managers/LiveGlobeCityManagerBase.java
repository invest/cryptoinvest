/**
 * 
 */
package com.anyoption.managers;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.LiveGlobeCity;
import com.anyoption.common.managers.BaseBLManager;
import com.anyoption.daos.LiveGlobeCityDAOBase;

/**
 * @author pavelhe
 *
 */
public class LiveGlobeCityManagerBase extends BaseBLManager {
	private static final Logger log = Logger.getLogger(LiveGlobeCityManagerBase.class);
	
	private static ArrayList<LiveGlobeCity> cachedCities; 
	
	public static ArrayList<LiveGlobeCity> getAllCities() throws SQLException {
		Connection con = null;
		try {
			con = getConnection();
			return LiveGlobeCityDAOBase.getAllCities(con);
		} finally {
			closeConnection(con);
		}
	}
	
	public static ArrayList<LiveGlobeCity> getActiveCities() throws SQLException {
		Connection con = null;
		try {
			con = getConnection();
			return LiveGlobeCityDAOBase.getActiveCities(con);
		} finally {
			closeConnection(con);
		}
	}
	
	public static ArrayList<LiveGlobeCity> getActiveCitiesCached() {
		if (cachedCities == null || cachedCities.size() == 0) {
			try {
				cachedCities = getActiveCities();
			} catch (SQLException e) {
				log.error("Could not get live globe cities from DB", e);
			}
		}
		return cachedCities;
	}

}
