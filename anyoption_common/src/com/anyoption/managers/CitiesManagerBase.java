/**
 * 
 */
package com.anyoption.managers;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;

import org.apache.log4j.Logger;

import com.anyoption.beans.base.City;
import com.anyoption.common.managers.BaseBLManager;
import com.anyoption.daos.CitiesDAOBase;

/**
 * @author AviadH
 *
 */
public class CitiesManagerBase extends BaseBLManager {

	private static final Logger log = Logger.getLogger(CitiesManagerBase.class);

    public static ArrayList<City> getAllCities() throws SQLException {
        Connection conn = null;
        try {
            conn = getConnection();
            return CitiesDAOBase.getAll(conn);
        } finally {
            closeConnection(conn);
        }
    }
}
