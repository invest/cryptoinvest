package com.anyoption.managers;

import java.sql.Connection;
import java.sql.SQLException;

import org.apache.log4j.Logger;

import com.anyoption.beans.TierUserHistory;
import com.anyoption.common.managers.BaseBLManager;
import com.anyoption.daos.TiersDAOBase;

public class TiersManagerBase extends BaseBLManager {
    private static final Logger log = Logger.getLogger(TiersManagerBase.class);
    
    public static final long TIER_ACTION_QUALIFIED = 1;
    public static final long TIER_ACTION_REMOVED = 2;
    public static final long TIER_ACTION_CONV_POINTS_TO_CASH = 3;
    public static final long TIER_ACTION_OBTAIN_POINTS = 4;
    public static final long TIER_ACTION_POINTS_EXPIRED = 5;
    public static final long TIER_ACTION_WELCOME_BONUS = 6;
    public static final long TIER_ACTION_REVERSE_POINTS = 7;
    public static final long TIER_ACTION_EXPIRATION_WARNING = 8;

    public static final long TIER_LEVEL_BLUE = 1;
    public static final long TIER_LEVEL_SILVER = 2;
    public static final long TIER_LEVEL_GOLD = 3;
    public static final long TIER_LEVEL_PLATINUM = 4;

    /**
     * Update user loylaty points with connection param
     * @param tierUserId
     * @param writerId
     * @param points
     * @throws SQLException
     */
    public static void updateTierUser(Connection con, long tierUserId, long writerId, long points) throws SQLException {
        log.debug("Going to update user LPoints after invest, tierUserId: " + tierUserId +
                    " InvPoints: " + points);
        TiersDAOBase.updateUserTier(con, tierUserId, writerId, points);
    }

    /**
     * Create TierUSerHistory instance by instance params
     * @return
     */
    public static TierUserHistory getTierUserHisIns(long tierId, long userId, long actionId, long writerId, long points,
            long actionPoints, long keyValue, String tableName, String utcOffset) {

        TierUserHistory h = new TierUserHistory();
        h.setTierId(tierId);
        h.setUserId(userId);
        h.setTierHistoryActionId(actionId);
        h.setWriterId(writerId);
        h.setPoints(points);
        h.setActionPoints(actionPoints);
        h.setKeyValue(keyValue);
        h.setTableName(tableName);
        h.setUtcOffset(utcOffset);
        return h;
    }
}