package com.anyoption.managers;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Iterator;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.base.CreditCardType;
import com.anyoption.common.beans.base.Skin;
import com.anyoption.common.managers.BaseBLManager;
import com.anyoption.daos.CreditCardTypesDAOBase;

public class CreditCardTypesManagerBase extends BaseBLManager {
	
	private static final Logger log = Logger.getLogger(CreditCardTypesManagerBase.class);
	private static HashMap<Long, CreditCardType> creditCardTypesET;
	private static HashMap<Long, CreditCardType> creditCardTypesAO;
	
    public static CreditCardType getById(long id) throws SQLException {
        Connection conn = null;
        try {
            conn = getConnection();
            return CreditCardTypesDAOBase.getById(conn, id);
        } finally {
            closeConnection(conn);
        }
    }

    /**
     * Get creditCards types by skinId
     * @param skinId
     * @return
     * @throws SQLException
     */
    public static HashMap<Long, CreditCardType> getBySkin(long skinId) throws SQLException {
    	Connection con = null;
    	HashMap<Long, CreditCardType> hm = new HashMap<Long, CreditCardType>();
        try {
            con = getConnection();
            HashMap<Long, CreditCardType> allHm = CreditCardTypesDAOBase.getAll(con);
            for (Iterator<Long> iter = allHm.keySet().iterator(); iter.hasNext();) {
            	Long ccTypeId = iter.next();
            	if (skinId == Skin.SKIN_ETRADER && ccTypeId.longValue() != TransactionsManagerBase.CC_TYPE_MAESTRO) {
            		hm.put(ccTypeId, allHm.get(ccTypeId));
            	}
            	if (skinId != Skin.SKIN_ETRADER && ccTypeId.longValue() != TransactionsManagerBase.CC_TYPE_ISRACARD &&
            			ccTypeId.longValue() != TransactionsManagerBase.CC_TYPE_AMEX) {
            		hm.put(ccTypeId, allHm.get(ccTypeId));
            	}
            }
        } finally {
            closeConnection(con);
        }
		return hm;
    }
    
    /**
     * @param skinId
     * @return
     */
    public static HashMap<Long, CreditCardType> getCreditCardTypes(long skinId) {
    	if (skinId == Skin.SKIN_ETRADER) {
    		creditCardTypesET = getCreditCardTypes(creditCardTypesET, skinId);
        	return creditCardTypesET;
    	} else {
    		creditCardTypesAO = getCreditCardTypes(creditCardTypesAO, skinId);
    		return creditCardTypesAO;
    	}
    }
    
    /**
     * @param creditCardTypes
     * @param skinId
     * @return
     */
    public static HashMap<Long, CreditCardType> getCreditCardTypes(HashMap<Long, CreditCardType> creditCardTypes, long skinId) {
    	if (null == creditCardTypes) {
    		  try {
    			  creditCardTypes = getBySkin(skinId);
  	        } catch (SQLException sqle) {
  	            log.error("Can't load creditCard types.", sqle);
  	        }
      	}
      	return creditCardTypes;
    }
}