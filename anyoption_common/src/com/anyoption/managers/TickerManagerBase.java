package com.anyoption.managers;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;

import com.anyoption.beans.TickerMarket;
import com.anyoption.common.managers.BaseBLManager;
import com.anyoption.daos.TickerDAOBase;

public class TickerManagerBase extends BaseBLManager {
    /**
     * Load specified number of markets details for specified skin sorted by
     * opened/suspended/closed/home page priority.
     *
     * @param conn
     * @param skinId
     * @return <code>ArrayList<TickerMarket></code>
     * @throws SQLException
     */
    public static ArrayList<TickerMarket> getSkinTickerMarkets(long skinId) throws SQLException {
        Connection conn = null;
        try {
            conn = getConnection();
            return TickerDAOBase.getSkinTickerMarkets(conn, skinId);
        } finally {
            closeConnection(conn);
        }
    }
}