//package com.anyoption.managers;
//
//import java.sql.Connection;
//import java.sql.SQLException;
//import java.util.Hashtable;
//
//import com.anyoption.common.beans.Country;
//import com.anyoption.common.managers.BaseBLManager;
//import com.anyoption.daos.CountriesDAOBase;
//
//public class CountriesManagerBase extends BaseBLManager {
//    /**
//     * Get all countries
//     *
//     * @param con connection to Db
//     * @return <code>Hashtable<Long, Country></code>
//     * @throws SQLException
//     */
//    public static Hashtable<Long, Country> getAll() throws SQLException {
//        Connection conn = null;
//        try {
//            conn = getConnection();
//            return CountriesDAOBase.getAll(conn);
//        } finally {
//            closeConnection(conn);
//        }
//    }
//    
//    public static Hashtable<Long, Country> getAllAllowedCountries() throws SQLException {
//        Connection conn = null;
//        try {
//            conn = getConnection();
//            return CountriesDAOBase.getAllAllowedCountries(conn);
//        } finally {
//            closeConnection(conn);
//        }
//    }
//
//    /**
//     * Get country by code
//     *
//     * @param countryCode country code
//     * @return long countryId
//     * @throws SQLException
//     */
//    public static long getCountryIdByCode(String countryCode) throws SQLException {
//        Connection conn = null;
//        try {
//            conn = getConnection();
//            return CountriesDAOBase.getCountryIdByCountryCode(conn, countryCode);
//        } finally {
//            closeConnection(conn);
//        }
//    }
//
//    /**
//     * Get skin by country id
//     *
//     * @param countryId country id
//     * @return long skin id
//     * @throws SQLException
//     */
//    public static long getSkinIdByCountryId(long countryId) throws SQLException {
//        Connection conn = null;
//        try {
//            conn = getConnection();
//            return CountriesDAOBase.getSkinIdByCountryId(conn, countryId);
//        } finally {
//            closeConnection(conn);
//        }
//    }
//}