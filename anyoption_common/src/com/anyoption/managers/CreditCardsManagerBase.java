//package com.anyoption.managers;
//
//import java.sql.Connection;
//import java.sql.SQLException;
//import java.util.ArrayList;
//import java.util.Calendar;
//import java.util.HashMap;
//import java.util.Map;
//
//import org.apache.log4j.Level;
//import org.apache.log4j.Logger;
//
//import com.anyoption.beans.User;
//import com.anyoption.bl_vos.TransactionBin;
//import com.anyoption.common.beans.CreditCard;
//import com.anyoption.common.beans.base.Skin;
//import com.anyoption.common.daos.CreditCardsDAOBase;
//import com.anyoption.common.managers.BaseBLManager;
//import com.anyoption.common.util.AnyOptionCreditCardValidator;
//import com.anyoption.daos.TransactionsDAOBase;
//import com.anyoption.util.CommonUtil;
//import com.anyoption.util.ConstantsBase;
//import com.anyoption.util.MessageToFormat;
//
//public class CreditCardsManagerBase extends BaseBLManager {
//    private static final Logger log = Logger.getLogger(CreditCardsManagerBase.class);
//
//    public static CreditCard getById(long id) throws SQLException {
//        Connection conn = null;
//        try {
//            conn = getConnection();
//            return CreditCardsDAOBase.getById(conn, id);
//        } finally {
//            closeConnection(conn);
//        }
//    }
//    
//    public static CreditCard getByCardIdAndUserId(long cardId, long userId) throws SQLException {
//        Connection conn = null;
//        try {
//            conn = getConnection();
//            return CreditCardsDAOBase.getByCardIdAndUserId(conn, cardId, userId);
//        } finally {
//            closeConnection(conn);
//        }
//    }    
//    
//    /**
//     * Validate cvv and dates of the credit card
//     * @param card
//     * @param user
//     * @return
//     * @throws SQLException
//     */
//    public static Map<String, MessageToFormat> validateCvvAndDates(CreditCard card, User user) throws SQLException {
//        Map<String, MessageToFormat> res = new HashMap<String, MessageToFormat>();
//
//        Calendar cal = Calendar.getInstance();
//        cal.set(Calendar.DATE, 1);
//        Calendar expCal = Calendar.getInstance();
//        expCal.clear();
//        expCal.set(Integer.parseInt(card.getExpYear()) + 2000, Integer.parseInt(card.getExpMonth()), 1);
//        if (cal.after(expCal)) {
//            res.put("exp", new MessageToFormat("error.creditcard.expdateError", null));
//        }
//        
//        if(card.getCcPass().length() != 3) {
//            res.put("ccPass", new MessageToFormat("error.cvvLength", null));
//        }  
//		return res;
//    }
//    
//    public static Map<String, MessageToFormat> validateCardNumber(CreditCard card, long skinId) {
//    	 Map<String, MessageToFormat> res = new HashMap<String, MessageToFormat>();
//         try {
//             if (!AnyOptionCreditCardValidator.validateCreditCardNumber(String.valueOf(card.getCcNumber()), skinId)) {
//                 res.put("ccNumber", new MessageToFormat("error.creditcard.num", null));
//				if (AnyOptionCreditCardValidator.isAmex() || AnyOptionCreditCardValidator.isDiners()) {
//					res.put(CreditCard.ERROR_CODE_CC_NOT_SUPPORTED, new MessageToFormat("error.creditcard.not.supported", null));
//				}
//                 return res;
//             }
//         } catch (Exception e) {
//             log.error("Can't validate ccn.", e);
//             res.put("ccNumber", new MessageToFormat("error.creditcard.num", null));
//             return res;
//         } 
//         return res;
//    }
//
//
//    public static Map<String, MessageToFormat> validateCardForm(CreditCard card, User user) throws SQLException {
//        Map<String, MessageToFormat> res = new HashMap<String, MessageToFormat>();
//        res = validateCvvAndDates(card, user);
//        if (!res.isEmpty()) {
//        	return res;
//        }
//        res = validateCardNumber(card, user.getSkinId());
//        if (!res.isEmpty()) {
//        	return res;
//        }
//        return res;
//    }
//    
//    
//    public static String validateCardExistens(CreditCard card, User user,  Map<String, MessageToFormat> res) throws SQLException {      
//	    Connection conn = null;
//	    String error = null;
//	    try {
//	        conn = getConnection();
//	        card.setVisible(true);
//	        card.setPermission(CreditCard.ALLOWED);
//	        // check if card number already exists and the holder id number is the same
//	        CreditCard currentCC = CreditCardsDAOBase.getByNumberAndUserId(conn, card.getCcNumber(), card.getUserId());
//	        if (null != currentCC) {
//	            card.setAllowed(currentCC.isAllowed());
//	            card.setVisible(currentCC.isVisible());
//	        }
//	        // check if the card is allowed and in your card list then skip this checks Bug_id=1999
//	        if ( ( null == currentCC ) || ( currentCC != null && !currentCC.isAllowed()))  {
//	            if (user.getSkinId() == Skin.SKIN_ETRADER && !card.getHolderIdNum().equals(user.getIdNum())) {
//	                if (log.isInfoEnabled()) {
//	                    log.info("User is trying to use a Credit Card with different ID number - user: " + user.getUserName());
//	                }
//	                res.put("holderIdNum", new MessageToFormat("error.creditcard.notallowed", null)); // error msg to user
//	                error = ConstantsBase.ERROR_CARD_NOT_ALLOWED_WRONG_ID; //error msg to transaction
//	            } else if (null != CreditCardsDAOBase.checkCardExistence(conn, card.getCcNumber(), card.getUserId())) {
//	                if (log.isInfoEnabled()) {
//	                    log.info("User is trying to use a Credit Card That already exists in the system - user: " + user.getUserName());
//	                }
//	                res.put("ccn", new MessageToFormat("error.creditcard.notallowed", null)); // error msg to user
//	                error = ConstantsBase.ERROR_CARD_NOT_ALLOWED_DUP_CARD;  //error msg to transaction
//	            }
//	        }
//			if (null != error) {
//				if (currentCC != null && currentCC.isBlockedForDeposit()) {
//					card.setPermission(CreditCard.NOT_ALLOWED_DEPOSIT);
//				} else {
//					card.setPermission(CreditCard.NOT_ALLOWED);
//				}
//			}
//	    } finally {
//	        closeConnection(conn);
//	    }	
//	    return error;
//    }
//    
//
//    public static void insertCard(CreditCard card, User user, long writerId, int source) throws SQLException {
//        if (log.isInfoEnabled()) {
//            String ls = System.getProperty("line.separator");
//            log.info("Insert card: " + card +
//                    "writerId: " + writerId + ls +
//                    "userId:" + user.getId() + ls +
//                    "source:" + source + ls +
//                    "platformId:" + user.getPlatformId() + ls);
//        }
//        // 	Set cc country id with user's country id for cases where bin is not in bins table
//        card.setCountryId(user.getCountryId()); 
//        card.setUserId(user.getId());
//        card.setUtcOffsetCreated(user.getUtcOffset());
//        card.setUtcOffsetModified(user.getUtcOffset());
//
//        TransactionBin tb = new TransactionBin();
//        Connection conn = null;
//        try {
//            conn = getConnection();
//            CreditCard currentCC = CreditCardsDAOBase.getByNumberAndUserId(conn, card.getCcNumber(), card.getUserId());
//            if (null == currentCC) {
//                if (log.isInfoEnabled()) {
//                    log.info("Insert creditCard: " + card);
//                }
//                CreditCardsDAOBase.insert(conn, card);
//            } else {
//                if (log.isInfoEnabled()) {
//                    log.info("Update creditCard details: " + card);
//                }
//                card.setAllowed(currentCC.isAllowed());
//                if (CommonUtil.isHebrewSkin(user.getSkinId())) { // etrader users
//					if (Long.parseLong(card.getHolderIdNum()) != Long.parseLong(user.getIdNum())) {   // wrong id_num
//						card.setPermission(CreditCard.NOT_ALLOWED);
//						// TODO insert blocking issue
//					}
//				}
//                card.setVisible(true);
//                card.setId(currentCC.getId());
//                CreditCardsDAOBase.update(conn, card);
//            }
//          //insert to transactionsBins table if don't exist
//			tb = TransactionsDAOBase.getTransactionBinByBin(conn, card.getBin(), SkinsManagerBase.getSkin(user.getSkinId()).getBusinessCaseIdForTranBins());
//			
//			if(tb.getId() == 0 ){
//				tb.setBin(card.getBin());
//				tb.setBusinessCaseId(SkinsManagerBase.getSkin(user.getSkinId()).getBusinessCaseIdForTranBins());
//				tb.setPlatformId(user.getPlatformId());
//				TransactionsDAOBase.insertTranBin(conn, tb);
//			}
//        } finally {
//            closeConnection(conn);
//        }
//    }
//
//	public static void updateCardDetails(CreditCard card) throws Exception {
//		if (log.isEnabledFor(Level.INFO)) {
//			String ls = System.getProperty("line.separator");
//			log.log(Level.INFO, "updating credit card details:" + ls +
//					"id: " + card.getId() + ls +
//					"month: " + card.getExpMonth() + ls +
//					"year: " + card.getExpYear() + ls +
//					"ccPass: " + card.getCcPass() + ls);
//		}
//		Connection con = getConnection();
//		try {
//			CreditCardsDAOBase.updateWebDetails(con, card);
//			if (log.isEnabledFor(Level.INFO)) {
//				String ls = System.getProperty("line.separator");
//				log.log(Level.INFO, "update creditCard finished successfully!" + ls);
//			}
//		} finally {
//			closeConnection(con);
//		}
//	}
//
//    /**
//     * Get visible cards by user
//     * @param userId
//     * @return
//     * @throws SQLException
//     */
//    public static ArrayList<CreditCard> getDisplayCreditCardsByUser(long userId) throws SQLException {
//        Connection conn = null;
//        try {
//            conn = getConnection();
//            return CreditCardsDAOBase.getVisibleByUserId(conn, userId);
//        } finally {
//            closeConnection(conn);
//        }
//    }
//
//    /**
//     * Delete card by id
//     * @param id
//     * @throws SQLException
//     */
//	public static void deleteCard(long id) throws SQLException {
//		if (log.isEnabledFor(Level.INFO)) {
//			String ls = System.getProperty("line.separator");
//			log.log(Level.INFO, " deleting card: " + ls +
//					"cardId: " + id);
//		}
//		Connection con = getConnection();
//		try {
//			CreditCardsDAOBase.updateCardNotVisible(con, id);
//			if (log.isEnabledFor(Level.INFO)) {
//				String ls = System.getProperty("line.separator");
//				log.log(Level.INFO, " card deleted sucessfuly!" + ls);
//			}
//		} finally {
//			closeConnection(con);
//		}
//	}
//
//    public static boolean isCreditCard3DSecure(String bin) throws SQLException {
//        boolean b = false;
//        Connection conn = null;
//        try {
//            conn = getConnection();
//            b = CreditCardsDAOBase.isCreditCard3DSecure(conn, bin);
//        } finally {
//            closeConnection(conn);
//        }
//        return b;
//    }
//    
//	/**
//	 * Return all visa Credit cards of some user, credit cards that the user make some
//	 * deposit from them and the deposit is approved.
//	 * @param id user id
//	 * @return	Array List of credit cards
//	 * @throws SQLException
//	 */
//	public static ArrayList<CreditCard> getDisplayWithrawalCreditCardsByUser(long id, Long creditCardId, long minWithdrawal, 
//			long skinId, boolean onlyVisibleCards) throws SQLException {
//		Connection con = getConnection();
//		try {
//			return CreditCardsDAOBase.getCcWithrawalByUserId(con, id, creditCardId, minWithdrawal, skinId, onlyVisibleCards);
//		} finally {
//			closeConnection(con); 
//		}
//	}
//	
//	/**
//	 * delete credit card.
//	 * @param credit card id
//	 * @throws SQLException
//	 */
//	public static void deleteCreditCard(long creditCardId)throws SQLException{
//		
//		Connection con = getConnection();
//		try {
//		CreditCardsDAOBase.updateCardNotVisible(con,creditCardId);;
//		} finally {
//			closeConnection(con); 
//		}
//	}
//	
//	/**
//	 * check if credit card is valid.
//	 * @param credit card id
//	 * @throws SQLException
//	 */
//	public static boolean checkIfCardIsValid( long creditCardId, long userId)throws SQLException{
//		
//		Connection con = getConnection();
//		try {
//			return CreditCardsDAOBase.checkIfCardIsValid(con,creditCardId,userId);
//		} finally {
//			closeConnection(con); 
//		}
//		
//	
//	}
//	
//    /**
//     * Get last valid card by user
//     * @param userId
//     * @return
//     * @throws SQLException
//     */
//    public static long getLastActiveCreditCardsByUser(long userId) throws SQLException {
//        Connection conn = null;
//        try {
//            conn = getConnection();
//            return CreditCardsDAOBase.getLastActiveCreditCardsByUser(conn, userId);
//        } finally {
//            closeConnection(conn);
//        }
//    }	
//	
//	
//	
//}