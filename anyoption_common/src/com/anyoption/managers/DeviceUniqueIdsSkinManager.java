package com.anyoption.managers;

import java.sql.Connection;
import java.sql.SQLException;

import com.anyoption.common.managers.DeviceUniqueIdsSkinManagerBase;
import com.anyoption.daos.DeviceUniqueIdsSkinDAO;

public class DeviceUniqueIdsSkinManager extends DeviceUniqueIdsSkinManagerBase {
	public static void insert(String deviceId, long skinId, String userAgent, String ip, Long countryId, Long combId, String appVer, String dynamicParam, long affiliateKey) throws SQLException {
		insert(deviceId, skinId, userAgent, ip, countryId, combId, appVer, dynamicParam, affiliateKey, null, 0, null, null, null);
	}
	
	public static void insert(String deviceId, long skinId, String userAgent, String ip, Long countryId, Long combId, String appVer,
			String dynamicParam, long affiliateKey, String appsflyerId, int osTypeId, String deviceFamily, String idfa, String advertisingId) throws SQLException {
		Connection conn = getConnection();
		try {
			DeviceUniqueIdsSkinDAO.insert(conn, deviceId, skinId, userAgent, ip, countryId, combId, appVer, dynamicParam, affiliateKey, appsflyerId, osTypeId, deviceFamily, idfa, advertisingId);
		} finally {
			closeConnection(conn);
		}
	}

    public static void updateSkindId(String deviceId, long skinId) throws SQLException {
        Connection conn = getConnection();
        try {
            DeviceUniqueIdsSkinDAO.updateSkindId(conn, deviceId, skinId);
        } finally {
            closeConnection(conn);
        }
    }

	public static void updateCombinationId(String deviceId, long combId) throws SQLException {
		Connection conn = getConnection();
		try {
			DeviceUniqueIdsSkinDAO.updateCombinationId(conn, deviceId, combId);
		} finally {
			closeConnection(conn);
		}
	}

	public static void updateVersion(String deviceId, String appVer, String idfa, String advertisingId) throws SQLException {
		Connection conn = getConnection();
		try {
			DeviceUniqueIdsSkinDAO.updateAppVer(conn, deviceId, appVer, idfa, advertisingId);
		} finally {
			closeConnection(conn);
		}
	}

	public static void updateC2dmRegistrationId(String deviceId, String c2dmRegistrationId) throws SQLException {
		Connection conn = getConnection();
		try {
			DeviceUniqueIdsSkinDAO.updateC2dmRegistrationId(conn, deviceId, c2dmRegistrationId);
		} finally {
			closeConnection(conn);
		}
	}
}