//package com.anyoption.managers;
//
//import java.sql.Connection;
//import java.sql.SQLException;
//
//import com.anyoption.common.bl_vos.ChargeBack;
//import com.anyoption.common.daos.ChargeBacksDAOBase;
//import com.anyoption.common.managers.BaseBLManager;
//
//public class ChargeBacksManagerBase extends BaseBLManager {
//    public static ChargeBack getById(long id) throws SQLException {
//        Connection conn = null;
//        try {
//            conn = getConnection();
//            return ChargeBacksDAOBase.getById(conn, id);
//        } finally {
//            closeConnection(conn);
//        }
//    }
//}