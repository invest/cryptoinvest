//package com.anyoption.clearing;
//
//import java.io.Serializable;
//import java.sql.SQLException;
//import java.util.Map;
//
//import org.apache.log4j.Logger;
//
//import com.anyoption.beans.User;
//import com.anyoption.common.beans.Transaction;
//import com.anyoption.common.beans.base.BaroPayRequest;
//import com.anyoption.common.clearing.ClearingInfo;
//import com.anyoption.util.CommonUtil;
//import com.anyoption.util.ConstantsBase;
//
//import il.co.etrader.clearing.BaroPayClearingProvider;
//
///**
// * BaroPayInfo clearing info class
// * @author Eyal O
// *
// */
//
///**
// * @author eyalo
// *
// */
//public class BaroPayInfo extends ClearingInfo implements Serializable {
//
//	private static final long serialVersionUID = 1L;
//
//	private static final Logger log = Logger.getLogger(BaroPayInfo.class);
//	private final String STATUS_URL = "jsp/baroPayStatusURL.jsf";
//	//private final String STATUS_WITHDRAW_URL = "jsp/baroPayWithdrawStatusURL.jsf";
//	private String statusURL;
//	private String landLinePhone;
//	private String statusWithdrawURL;
//	private BaroPayRequest baroPayRequest;
//
//	//status URL fields
//	private String transaction_id;
//	private String user_id;
//	private String redirectURL;
//	private String merchantAccount;
//	private String status;
//	private String merchant_id;
//	private String senderName;
//	private String depositAmount;
//	private String xmlResponse;
//
//	public BaroPayInfo() {
//    }
//
//	public BaroPayInfo(User user, Transaction t, String landLinePhone) throws SQLException{
//		super(user, t.getAmount(), t.getTypeId(), t.getId(), t.getIp(), t.getAcquirerResponseId(), 0);
//		
//		String homePageUrl = CommonUtil.getConfig("homepage.url");
//		this.statusURL = homePageUrl + STATUS_URL;
//		this.statusWithdrawURL = homePageUrl + STATUS_URL;
//    	this.depositAmount = String.valueOf(t.getAmount());
//    	this.senderName = ConstantsBase.BARO_PAY_SENDER_NAME;
//    	this.setLandLinePhone(landLinePhone);
//    	this.transactionId = t.getId();
//    	this.baroPayRequest = new BaroPayRequest();
//    }
//
//	/**
//	 * @return the redirectURL
//	 */
//	public String getRedirectURL() {
//		return redirectURL;
//	}
//
//	/**
//	 * @param redirectURL the redirectURL to set
//	 */
//	public void setRedirectURL(String redirectURL) {
//		this.redirectURL = redirectURL;
//	}
//
//	/**
//	 * @return the merchantAccount
//	 */
//	public String getMerchantAccount() {
//		return merchantAccount;
//	}
//
//	/**
//	 * @param merchantAccount the merchantAccount to set
//	 */
//	public void setMerchantAccount(String merchantAccount) {
//		this.merchantAccount = merchantAccount;
//	}
//	
//	/**
//	 * @return the status
//	 */
//	public String getStatus() {
//		return status;
//	}
//
//	/**
//	 * @param status the status to set
//	 */
//	public void setStatus(String status) {
//		this.status = status;
//	}
//	
//	/**
//	 * @return the merchant_id
//	 */
//	public String getMerchant_id() {
//		return merchant_id;
//	}
//
//	/**
//	 * @param merchant_id the merchant_id to set
//	 */
//	public void setMerchant_id(String merchant_id) {
//		this.merchant_id = merchant_id;
//	}
//
//	/**
//	 * @return the senderName
//	 */
//	public String getSenderName() {
//		return senderName;
//	}
//
//	/**
//	 * @param senderName the senderName to set
//	 */
//	public void setSenderName(String senderName) {
//		this.senderName = senderName;
//	}
//	
//	/**
//	 * @return the depositAmount
//	 */
//	public String getDepositAmount() {
//		return depositAmount;
//	}
//
//	/**
//	 * @param depositAmount the depositAmount to set
//	 */
//	public void setDepositAmount(String depositAmount) {
//		this.depositAmount = depositAmount;
//	}
//	
//	/**
//	 * @return the transaction_id
//	 */
//	public String getTransaction_id() {
//		return transaction_id;
//	}
//
//	/**
//	 * @param transaction_id the transaction_id to set
//	 */
//	public void setTransaction_id(String transaction_id) {
//		this.transaction_id = transaction_id;
//	}
//
//	/**
//	 * @return the user_id
//	 */
//	public String getUser_id() {
//		return user_id;
//	}
//
//	/**
//	 * @param user_id the user_id to set
//	 */
//	public void setUser_id(String user_id) {
//		this.user_id = user_id;
//	}
//
//	/**
//	 * @return the statusURL
//	 */
//	public String getStatusURL() {
//		return statusURL;
//	}
//
//	/**
//	 * @param statusURL the statusURL to set
//	 */
//	public void setStatusURL(String statusURL) {
//		this.statusURL = statusURL;
//	}
//	
//	public String getXmlResponse() {
//		return xmlResponse;
//	}
//
//	public void setXmlResponse(String xmlResponse) {
//		this.xmlResponse = xmlResponse;
//	}
//
//	public String getLandLinePhone() {
//		return landLinePhone;
//	}
//
//	public void setLandLinePhone(String landLinePhone) {
//		this.landLinePhone = landLinePhone;
//	}
//	
//	/**
//	 * @return the statusWithdrawURL
//	 */
//	public String getStatusWithdrawURL() {
//		return statusWithdrawURL;
//	}
//
//	/**
//	 * @param statusWithdrawURL the statusWithdrawURL to set
//	 */
//	public void setStatusWithdrawURL(String statusWithdrawURL) {
//		this.statusWithdrawURL = statusWithdrawURL;
//	}
//
//	/**
//	 * @return the baroPayRequest
//	 */
//	public BaroPayRequest getBaroPayRequest() {
//		return baroPayRequest;
//	}
//
//	/**
//	 * @param baroPayRequest the baroPayRequest to set
//	 */
//	public void setBaroPayRequest(BaroPayRequest baroPayRequest) {
//		this.baroPayRequest = baroPayRequest;
//	}
//	
//	/**
//	 * Handle status URL from BaroPay
//	 * @param requestParameterMap
//	 * @return
//	 */
//	public boolean handleStatusUrl(Map requestParameterMap){
//		try {
//			transaction_id = (String) requestParameterMap.get("DepositID");
//			user_id = (String) requestParameterMap.get("MemberCode");
//			currencySymbol = (String) requestParameterMap.get("Currency");
//			depositAmount = (String) requestParameterMap.get("Amount");
//			providerTransactionId = (String) requestParameterMap.get("PaymentID");
//			result = (String) requestParameterMap.get("Transresult");
//			String TAB = "\n";
//				log.info( TAB + "BaroPay status_url fields:" + TAB
//					+ "transaction_id=" + transactionId + TAB
//					+ "user_id(MemberCode)=" + userId + TAB
//					+ "currency_symbol=" + currencySymbol + TAB
//					+ "deposit_amount=" + depositAmount + TAB
//					+ "provider_transaction_id=" + providerTransactionId + TAB
//					+ "resault(Transresult)=" + result);
//		} catch (Exception e){
//			log.error("Error retrieving data from BaroPay status URL " + e);
//			return false;
//		}
//		return true;
//	}
//
//	public String returnStatusString(){
//		if (status.equals(BaroPayClearingProvider.STATUS_PROCESSED_CODE)){
//			return BaroPayClearingProvider.STATUS_PROCESSED_STRING;
//		} else if (status.equals(BaroPayClearingProvider.STATUS_CANCELLED_CODE)){
//			return BaroPayClearingProvider.STATUS_CANCELLED_STRING;
//		} else if (status.equals(BaroPayClearingProvider.STATUS_FAILED_CODE)){
//			return BaroPayClearingProvider.STATUS_FAILED_STRING;
//		} else if (status.equals(BaroPayClearingProvider.STATUS_PENDING_CODE)){
//			return BaroPayClearingProvider.STATUS_PENDING_STRING;
//		} else if (status.equals(BaroPayClearingProvider.STATUS_CHARGEBACK_CODE)){
//			return BaroPayClearingProvider.STATUS_CHARGEBACK_STRING;
//		}
//		return null;
//	}
//
//}