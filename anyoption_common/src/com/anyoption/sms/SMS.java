package com.anyoption.sms;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import org.apache.log4j.Logger;

import com.anyoption.common.daos.DAOBase;

public class SMS extends com.anyoption.common.sms.SMS {
    private static Logger log = Logger.getLogger(SMS.class);

    public static final long STATUS_QUEUED = 1;
    public static final long STATUS_SUBMITTED = 2;
    public static final long STATUS_FAILED_TO_SUBMIT = 3;
    public static final long STATUS_DELIVERED_TO_SMSC = 4;
    public static final long STATUS_QUEUED_ON_SMSC = 5;
    public static final long STATUS_DELIVERED = 6;
    public static final long STATUS_FAILED = 7;
    public static final long STATUS_UNROUTABLE = 8;

//    public static final long TYPE_TEXT = 1;
    public static final long TYPE_WAP_PUSH = 2;
    public static final long TYPE_JME_PUSH = 3;

    public static final long APP_TYPE_NONE = 0;
    public static final long APP_TYPE_SMS_PROMOTION = 1;
    
    public static final long DESCRIPTION_RESET_PASSWORD = 1;
//    public static final long DESCRIPTION_EXPIRY = 2;
    public static final long DESCRIPTION_WELCOME = 3;
    public static final long DESCRIPTION_PROMOTION = 4;
    public static final long DESCRIPTION_DOWNLOAD = 5;

    protected long id;
    protected String senderPhoneNumber;
    protected String senderName;
    protected String phoneNumber;
    protected String messageBody;
    protected String wapURL;
    protected long appId;
    protected long appType;
    protected long subjectId;
    protected long skinId;
    protected long status;
    protected int dstPort;
    //protected long msgQueueId;
    protected long promotionId;
    //protected boolean secondAttempt;
    protected long msgType;
    protected String error;
    protected long smsProviderId;
    protected long retries;
    protected Date scheduledTime;
    protected Date timeSent;
    protected String reference;
    protected int maxRetries;           // max retires for sumbiting the message to the provider
    protected boolean workerHandle;     // true if there is some worker that handle the message
    protected long providerId;
    protected long descriptionId;

    protected SMS() {
        // Do not create explicitly SMS instances. Use subclasses.
    }

    public static SMS getInstance(ResultSet rs) throws SQLException {
        long typeId = rs.getLong("sms_type_id");
        if (typeId == SMS.TYPE_TEXT) {
            TextSMS msg = new TextSMS();
            msg.setId(rs.getLong("id"));
            msg.setMsgType(typeId);
            msg.setSenderPhoneNumber(rs.getString("sender_number"));
            msg.setSenderName(rs.getString("sender"));
            msg.setPhoneNumber(rs.getString("phone"));
            msg.setMessageBody(rs.getString("message"));
            msg.setSmsProviderId(rs.getLong("sms_provider_id"));
            msg.setRetries(rs.getLong("retries"));
            msg.setScheduledTime(DAOBase.convertToDate(rs.getTimestamp("scheduled_time")));
            msg.setTimeSent(DAOBase.convertToDate(rs.getTimestamp("time_sent")));
            msg.setStatus(rs.getLong("sms_status_id"));
            msg.setReference(rs.getString("reference"));
            msg.setError(rs.getString("error"));
            msg.setProviderId(rs.getLong("provider_id"));
            msg.setDescriptionId(rs.getLong("sms_description_id"));
            return msg;
        }
        return null;
    }

    public long getAppId() {
        return appId;
    }

    public void setAppId(long appId) {
        this.appId = appId;
    }

    public long getAppType() {
        return appType;
    }

    public void setAppType(long appType) {
        this.appType = appType;
    }

    public int getDstPort() {
        return dstPort;
    }

    public void setDstPort(int dstPort) {
        this.dstPort = dstPort;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getMessageBody() {
        return messageBody;
    }

    public void setMessageBody(String messageBody) {
        this.messageBody = messageBody;
    }

//    public long getMsgQueueId() {
//        return msgQueueId;
//    }
//
//    public void setMsgQueueId(long msgQueueId) {
//        this.msgQueueId = msgQueueId;
//    }

    public long getMsgType() {
        return msgType;
    }

    public void setMsgType(long msgType) {
        this.msgType = msgType;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public long getPromotionId() {
        return promotionId;
    }

    public void setPromotionId(long promotionId) {
        this.promotionId = promotionId;
    }

//    public boolean isSecondAttempt() {
//        return secondAttempt;
//    }
//
//    public void setSecondAttempt(boolean secondAttempt) {
//        this.secondAttempt = secondAttempt;
//    }

    public String getSenderPhoneNumber() {
        return senderPhoneNumber;
    }

    public void setSenderPhoneNumber(String senderPhoneNumber) {
        this.senderPhoneNumber = senderPhoneNumber;
    }

    public String getSenderName() {
        return senderName;
    }

    public void setSenderName(String senderName) {
        this.senderName = senderName;
    }

    public long getSkinId() {
        return skinId;
    }

    public void setSkinId(long skinId) {
        this.skinId = skinId;
    }

    public long getStatus() {
        return status;
    }

    public void setStatus(long status) {
        this.status = status;
    }

    public String getWapURL() {
        return wapURL;
    }

    public void setWapURL(String wapURL) {
        this.wapURL = wapURL;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public long getRetries() {
        return retries;
    }

    public void setRetries(long retries) {
        this.retries = retries;
    }

    public Date getScheduledTime() {
        return scheduledTime;
    }

    public void setScheduledTime(Date scheduledTime) {
        this.scheduledTime = scheduledTime;
    }

    public long getSmsProviderId() {
        return smsProviderId;
    }

    public void setSmsProviderId(long smsProviderId) {
        this.smsProviderId = smsProviderId;
    }

    public long getSubjectId() {
        return subjectId;
    }

    public void setSubjectId(long subjectId) {
        this.subjectId = subjectId;
    }

    public Date getTimeSent() {
        return timeSent;
    }

    public void setTimeSent(Date timeSent) {
        this.timeSent = timeSent;
    }

    /**
	 * @return the maxRetries
	 */
	public int getMaxRetries() {
		return maxRetries;
	}

	/**
	 * @param maxRetries the maxRetries to set
	 */
	public void setMaxRetries(int maxRetries) {
		this.maxRetries = maxRetries;
	}

	/**
	 * @return the workerHandle
	 */
	public boolean isWorkerHandle() {
		return workerHandle;
	}

	/**
	 * @param workerHandle the workerHandle to set
	 */
	public void setWorkerHandle(boolean workerHandle) {
		this.workerHandle = workerHandle;
	}

	public String toString() {
        String ls = System.getProperty("line.separator");
        return ls + "SMS:" + ls +
            "id: " + id + ls +
            "senderPhoneNumber: " + senderPhoneNumber + ls +
            "senderName: " + senderName + ls +
            "phoneNumber: " + phoneNumber + ls +
            "messageBody: " + messageBody + ls +
            "wapURL: " + wapURL + ls +
            "appId: " + appId + ls +
            "appType: " + appType + ls +
            "subjectId: " + subjectId + ls +
            "skinId: " + skinId + ls +
            "status: " + status + ls +
            "dstPort: " + dstPort + ls +
//            "msgQueueId: " + msgQueueId + ls +
            "promotionId: " + promotionId + ls +
//            "secondAttempt: " + secondAttempt + ls +
            "msgType: " + msgType + ls +
            "error: " + error + ls +
            "smsProviderId: " + smsProviderId + ls +
            "retries: " + retries + ls +
            "scheduledTime: " + scheduledTime + ls +
            "timeSent: " + timeSent + ls +
            "maxRetries: " + maxRetries + ls +
            "reference: " + reference + ls +
            "provider id: " + providerId + ls;
    }


	/**
     * Turns a byte array to hex string.
     *
     * @param arr the byte array
     * @return A hex string presenting the byte array content.
     */
    public static String byteArrayToHexString(byte[] arr) {
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < arr.length; i++) {
            sb.append(toFixedLenHexString(Integer.toHexString(arr[i] & 0x000000FF), 2));
        }

        return sb.toString();
    }

    /**
     * Makes a hex string to have a requested len. For example if you need hex string with len 4 you can pass one with
     * len 1 and this function will pad it on the left with the correct number of 0s so the len becomes 4.
     *
     * @param hex the current hex str
     * @param len the needed len
     * @return New string that contains the padded hex string.
     */
    public static String toFixedLenHexString(String hex, int len) {
        String pad = "0000000000";
        int ln = hex.length();
        if (ln < len) {
            return pad.substring(0, len - ln) + hex;
        }

        return hex;
    }

    /**
     * Check to see if given string is unicode or not.
     *
     * @author Nir.
     */
    public static boolean isUnicode(String text) {
        try {
            //  Convert the bytes to UTF-8
            byte[] data = text.getBytes("UTF-8");
            //  Check each byte to see if its unicode or not
            for (int index = 0; index < data.length; index++) {
                //  check the MSB to see if its 00 padding.
                if ((char) data[index] > 256) {
                    return true;
                }
            }
        } catch (Exception e) {
            log.error("Exception while trying to determine if text is unicode or not.", e);
        }
        // Text is not unicode, or exception occur
        return false;
    }

	/**
	 * @return the providerId
	 */
	public long getProviderId() {
		return providerId;
	}

	/**
	 * @param providerId the providerId to set
	 */
	public void setProviderId(long providerId) {
		this.providerId = providerId;
	}
	
	/**
	 * @return the decriptionId
	 */
	public long getDescriptionId() {
		return descriptionId;
	}

	/**
	 * @param descriptionId the descriptionId to set
	 */
	public void setDescriptionId(long descriptionId) {
		this.descriptionId = descriptionId;
	}
}