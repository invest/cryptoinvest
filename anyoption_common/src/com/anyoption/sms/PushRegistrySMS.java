package com.anyoption.sms;

import java.io.IOException;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

public class PushRegistrySMS extends SMS {
    private static Logger log = Logger.getLogger(PushRegistrySMS.class);
    
    /*
     * 060504xxxxyyyy where : xxxx - is the destination port (in our case = 5432) yyyy - is the origin port in HEX
     * format
     */
    protected static final String PUSH_REGISTRY_SMS_UDH = "060504153823F0"; // 5432
    protected static final String PUSH_REGISTRY_SMS_START = "0791798268004544040C911989684385620000408002804532800F";

    public PushRegistrySMS() {
        
    }

    /**
     * Compose the UDH of MIDP 2.0 push registry SMS.
     *
     * @param destPort the port on which the app listen to
     * @return The composed UDH or the default one (port 5432) if port 0 specified.
     */
    protected String composePushRegistrySMSUDH(int destPort) {
        if (destPort == 0) {
            return PUSH_REGISTRY_SMS_UDH; // return the default UDH with dest port 5432
        }
        byte[] port = new byte[2];
        port[0] = (byte) ((destPort & 0x0000FF00) >>> 8);
        port[1] = (byte) (destPort & 0x000000FF);
        if (log.isEnabledFor(Level.DEBUG)) {
            log.debug("port: " + destPort + //
                    " port[0]: " + port[0] + //
                    " port[1]: " + port[1] + //
                    " str: " + new String(toHexString(port)) + //
                    " my: " + byteArrayToHexString(port));
        }
        return PUSH_REGISTRY_SMS_UDH.substring(0, 6) + // 060504
                new String(toHexString(port)) + // dest port
                PUSH_REGISTRY_SMS_UDH.substring(10); // src port
    }

    /**
     * Compose J2ME 2.0 push registry SMS body by specified info (message body).
     *
     * @return Hex string of the SMS message body.
     */
    protected String getBody() {
        String pdu = "";
        try {
            byte[] msg_byte = compress(convertUnicode2GSM(messageBody));
            char[] msg_char = toHexString(msg_byte);
            pdu = new String(msg_char);
        } catch (Exception e) {
            log.log(Level.ERROR, "Failed to compose push registry SMS.", e);
        }
        return PUSH_REGISTRY_SMS_START + pdu;
    }

    /**
     * Convert a Unicode text string into the GSM standard alphabet
     *
     * @param msg text string in ASCII
     * @return text string in GSM standard alphabet
     */
    public static byte[] convertUnicode2GSM(String msg) {
        byte[] data = new byte[msg.length()];

        for (int i = 0; i < msg.length(); i++) {
            if (msg.charAt(i) >= 1488 && msg.charAt(i) <= 1514) {
                new IOException("GSM alphabet ISO 8858-9 not supported yet ");
            }

            switch (msg.charAt(i)) {
            case '@':
                data[i] = 0x00;
                break;
            case '$':
                data[i] = 0x02;
                break;
            case '\n':
                data[i] = 0x0A;
                break;
            case '\r':
                data[i] = 0x0D;
                break;
            case '_':
                data[i] = 0x11;
                break;
            case ' ':
                data[i] = 0x20;
                break;
            case '!':
                data[i] = 0x21;
                break;
            case '\"':
                data[i] = 0x22;
                break;
            case '#':
                data[i] = 0x23;
                break;
            case '%':
                data[i] = 0x25;
                break;
            case '&':
                data[i] = 0x26;
                break;
            case '\'':
                data[i] = 0x27;
                break;
            case '(':
                data[i] = 0x28;
                break;
            case ')':
                data[i] = 0x29;
                break;
            case '*':
                data[i] = 0x2A;
                break;
            case '+':
                data[i] = 0x2B;
                break;
            case ',':
                data[i] = 0x2C;
                break;
            case '-':
                data[i] = 0x2D;
                break;
            case '.':
                data[i] = 0x2E;
                break;
            case '/':
                data[i] = 0x2F;
                break;
            case '0':
                data[i] = 0x30;
                break;
            case '1':
                data[i] = 0x31;
                break;
            case '2':
                data[i] = 0x32;
                break;
            case '3':
                data[i] = 0x33;
                break;
            case '4':
                data[i] = 0x34;
                break;
            case '5':
                data[i] = 0x35;
                break;
            case '6':
                data[i] = 0x36;
                break;
            case '7':
                data[i] = 0x37;
                break;
            case '8':
                data[i] = 0x38;
                break;
            case '9':
                data[i] = 0x39;
                break;
            case ':':
                data[i] = 0x3A;
                break;
            case ';':
                data[i] = 0x3B;
                break;
            case '<':
                data[i] = 0x3C;
                break;
            case '=':
                data[i] = 0x3D;
                break;
            case '>':
                data[i] = 0x3E;
                break;
            case '?':
                data[i] = 0x3F;
                break;
            case 'A':
                data[i] = 0x41;
                break;
            case 'B':
                data[i] = 0x42;
                break;
            case 'C':
                data[i] = 0x43;
                break;
            case 'D':
                data[i] = 0x44;
                break;
            case 'E':
                data[i] = 0x45;
                break;
            case 'F':
                data[i] = 0x46;
                break;
            case 'G':
                data[i] = 0x47;
                break;
            case 'H':
                data[i] = 0x48;
                break;
            case 'I':
                data[i] = 0x49;
                break;
            case 'J':
                data[i] = 0x4A;
                break;
            case 'K':
                data[i] = 0x4B;
                break;
            case 'L':
                data[i] = 0x4C;
                break;
            case 'M':
                data[i] = 0x4D;
                break;
            case 'N':
                data[i] = 0x4E;
                break;
            case 'O':
                data[i] = 0x4F;
                break;
            case 'P':
                data[i] = 0x50;
                break;
            case 'Q':
                data[i] = 0x51;
                break;
            case 'R':
                data[i] = 0x52;
                break;
            case 'S':
                data[i] = 0x53;
                break;
            case 'T':
                data[i] = 0x54;
                break;
            case 'U':
                data[i] = 0x55;
                break;
            case 'V':
                data[i] = 0x56;
                break;
            case 'W':
                data[i] = 0x57;
                break;
            case 'X':
                data[i] = 0x58;
                break;
            case 'Y':
                data[i] = 0x59;
                break;
            case 'Z':
                data[i] = 0x5A;
                break;
            case 'a':
                data[i] = 0x61;
                break;
            case 'b':
                data[i] = 0x62;
                break;
            case 'c':
                data[i] = 0x63;
                break;
            case 'd':
                data[i] = 0x64;
                break;
            case 'e':
                data[i] = 0x65;
                break;
            case 'f':
                data[i] = 0x66;
                break;
            case 'g':
                data[i] = 0x67;
                break;
            case 'h':
                data[i] = 0x68;
                break;
            case 'i':
                data[i] = 0x69;
                break;
            case 'j':
                data[i] = 0x6A;
                break;
            case 'k':
                data[i] = 0x6B;
                break;
            case 'l':
                data[i] = 0x6C;
                break;
            case 'm':
                data[i] = 0x6D;
                break;
            case 'n':
                data[i] = 0x6E;
                break;
            case 'o':
                data[i] = 0x6F;
                break;
            case 'p':
                data[i] = 0x70;
                break;
            case 'q':
                data[i] = 0x71;
                break;
            case 'r':
                data[i] = 0x72;
                break;
            case 's':
                data[i] = 0x73;
                break;
            case 't':
                data[i] = 0x74;
                break;
            case 'u':
                data[i] = 0x75;
                break;
            case 'v':
                data[i] = 0x76;
                break;
            case 'w':
                data[i] = 0x77;
                break;
            case 'x':
                data[i] = 0x78;
                break;
            case 'y':
                data[i] = 0x79;
                break;
            case 'z':
                data[i] = 0x7A;
                break;
            default:
                new IOException("Retrieved unsupported character: " + msg.charAt(i));
            } // switch
        } // for
        return data;
    } // convertUnicode2GSM

    /**
     * Compress a readable text message into the GSM standard alphabet (1 character -> 7 bit data)
     *
     * @param data text string in Unicode
     * @return text string in GSM standard alphabet
     */
    public static byte[] compress(byte[] data) {
        int l;
        int n; // length of compressed data
        byte[] comp;

        // calculate length of message
        l = data.length;
        n = (l * 7) / 8;
        if ((l * 7) % 8 != 0) {
            n++;
        }

        comp = new byte[n];
        int j = 0; // index in data
        int s = 0; // shift from next data byte
        for (int i = 0; i < n; i++) {
            comp[i] = (byte) ((data[j] & 0x7F) >>> s);
            s++;
            if (j + 1 < l) {
                comp[i] += (byte) ((data[j + 1] << (8 - s)) & 0xFF);
            }
            if (s < 7) {
                j++;
            }
            else {
                s = 0;
                j += 2;
            }
        }
        return comp;
    }

    /**
     * Convert data into a hex string
     *
     * @param data to convert
     * @return in hex string converted data
     */
    public static char[] toHexString(byte[] data) {
        int l = data.length;
        char[] hex = new char[2 * l];

        int j = 0; // index in hex
        for (int i = 0; i < data.length; i++) {
            switch (data[i] & 0xF0) {
            case 0x00:
                hex[j] = '0';
                break;
            case 0x10:
                hex[j] = '1';
                break;
            case 0x20:
                hex[j] = '2';
                break;
            case 0x30:
                hex[j] = '3';
                break;
            case 0x40:
                hex[j] = '4';
                break;
            case 0x50:
                hex[j] = '5';
                break;
            case 0x60:
                hex[j] = '6';
                break;
            case 0x70:
                hex[j] = '7';
                break;
            case 0x80:
                hex[j] = '8';
                break;
            case 0x90:
                hex[j] = '9';
                break;
            case 0xA0:
                hex[j] = 'A';
                break;
            case 0xB0:
                hex[j] = 'B';
                break;
            case 0xC0:
                hex[j] = 'C';
                break;
            case 0xD0:
                hex[j] = 'D';
                break;
            case 0xE0:
                hex[j] = 'E';
                break;
            case 0xF0:
                hex[j] = 'F';
                break;
            } // switch
            j++;
            switch (data[i] & 0x0F) {
            case 0x00:
                hex[j] = '0';
                break;
            case 0x01:
                hex[j] = '1';
                break;
            case 0x02:
                hex[j] = '2';
                break;
            case 0x03:
                hex[j] = '3';
                break;
            case 0x04:
                hex[j] = '4';
                break;
            case 0x05:
                hex[j] = '5';
                break;
            case 0x06:
                hex[j] = '6';
                break;
            case 0x07:
                hex[j] = '7';
                break;
            case 0x08:
                hex[j] = '8';
                break;
            case 0x09:
                hex[j] = '9';
                break;
            case 0x0A:
                hex[j] = 'A';
                break;
            case 0x0B:
                hex[j] = 'B';
                break;
            case 0x0C:
                hex[j] = 'C';
                break;
            case 0x0D:
                hex[j] = 'D';
                break;
            case 0x0E:
                hex[j] = 'E';
                break;
            case 0x0F:
                hex[j] = 'F';
                break;
            } // switch
            j++;
        }
        return hex;
    }
}