package com.anyoption.sms;

import org.apache.log4j.Logger;

import com.anyoption.common.sms.SMSException;

public abstract class SMSProvider {
    private static final Logger log = Logger.getLogger(SMSProvider.class);
    
    protected long id;
    protected String name;
    protected String url;
    protected String username;
    protected String password;
    protected String dlrUrl;
    protected int maxRetries;

    public void setConfig(SMSProviderConfig config) {
        id = config.getId();
        name = config.getName();
        url = config.getUrl();
        username = config.getUserName();
        password = config.getPassword();
        dlrUrl = config.getDlrUrl();
        maxRetries = config.getMaxRetries();

        if (log.isInfoEnabled()) {
            String ls = System.getProperty("line.separator");
            log.info(getClass().getName() + " configured with:" + ls +
                    "id: " + id + ls +
                    "name: " + name + ls +
                    "url: " + url + ls +
                    "dlrUrl: " + dlrUrl + ls +
                    "username: " + username + ls +
                    "maxRetries: " + maxRetries + ls);
        }
    }

    public void send(SMS msg) throws SMSException {
        if (msg instanceof TextSMS) {
            send((TextSMS) msg);
        } else if (msg instanceof WapPushSMS) {
            send((WapPushSMS) msg);
        } else if (msg instanceof PushRegistrySMS) {
            send((PushRegistrySMS) msg);
        } else {
            throw new SMSException("Wrong SmsType.");
        }
    }
    
    public abstract void send(TextSMS msg) throws SMSException;

    public abstract void send(WapPushSMS msg) throws SMSException;

    public abstract void send(PushRegistrySMS msg) throws SMSException;

    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }
    
    public void close() {
        // nothing to do
    }
}