//package com.anyoption.jms;
//
///**
// * Interface to be implemented by classes interested in opportunities current
// * level changes. 
// * 
// * @author Tony
// */
//public interface LevelsCacheCurrentLevelListener {
//    /**
//     * Notify about opportunity current level change.
//     * 
//     * @param oppId the id of the opportunity which current level have changed
//     * @param realLevel the new real level of the opportunity
//     */
//    public void update(long oppId, double currentLevel);
//}