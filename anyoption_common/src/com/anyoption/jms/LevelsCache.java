//package com.anyoption.jms;
//
//import java.lang.management.ManagementFactory;
//import java.math.BigDecimal;
//import java.util.HashMap;
//import java.util.Iterator;
//
//import javax.management.MBeanServer;
//import javax.management.ObjectName;
//
//import org.apache.log4j.Logger;
//
//import com.anyoption.common.jms.ifc.LevelsCacheCurrentLevelListener;
//import com.anyoption.common.jms.ifc.LevelsCacheInvestmentListener;
//import com.anyoption.common.jms.ifc.LevelsCacheListener;
//import com.anyoption.common.jms.ifc.LevelsCacheMBean;
//import com.anyoption.common.jms.ifc.LevelsCacheMonitoringAlertsListener;
//import com.anyoption.common.jms.ifc.LevelsCacheRealLevelListener;
//
//public class LevelsCache extends SimpleLevelsCache implements LevelsCacheMBean {
//    private static final Logger log = Logger.getLogger(LevelsCache.class);
//
//    private LevelsCacheListener listener;
//    private LevelsCacheCurrentLevelListener currentLevelListener;
//    private LevelsCacheRealLevelListener realLevelListener;
//    private LevelsCacheInvestmentListener investmentListener;
//    private LevelsCacheMonitoringAlertsListener monitoringAlertsListener;
//
//    private String jmxName;
//
//    /**
//     * Constructs a <code>LevelsCache</code> that can be used only as "control" connection for the
//     * service. It will open just connection to the control queue and will not listen for updates by
//     * the topic.
//     *
//     * @param initialContextFactory
//     * @param providerURL
//     * @param connectionFactoryName
//     * @param queueName
//     * @param msgPoolSize
//     * @param recoveryPause
//     */
//    public LevelsCache(String initialContextFactory, String providerURL, String connectionFactoryName, String queueName, int msgPoolSize, int recoveryPause) {
//        this(initialContextFactory, providerURL, connectionFactoryName, queueName, null, msgPoolSize, recoveryPause, null);
//    }
//
//    /**
//     * Construct a <code>LevelsCahce</code> that can be used to "control" the service as well as
//     * listen for levels (current/real) updates.
//     *
//     * @param initialContextFactory
//     * @param providerURL
//     * @param connectionFactoryName
//     * @param queueName
//     * @param topicName
//     * @param msgPoolSize
//     * @param recoveryPause
//     * @param listener
//     */
//    public LevelsCache(String initialContextFactory, String providerURL, String connectionFactoryName, String queueName, String topicName, int msgPoolSize, int recoveryPause, LevelsCacheListener listener) {
//        super(initialContextFactory, providerURL, connectionFactoryName, queueName, topicName, listener);
//        this.listener = listener;
//        this.msgPoolSize = msgPoolSize;
//        this.recoveryPause = recoveryPause;
//    }
//
//    public void setCurrentLevelListener(LevelsCacheCurrentLevelListener currentLevelListener) {
//        this.currentLevelListener = currentLevelListener;
//    }
//
//    public void setRealLevelListener(LevelsCacheRealLevelListener realLevelListener) {
//        this.realLevelListener = realLevelListener;
//    }
//
//    public void setInvestmentListener(LevelsCacheInvestmentListener investmentListener) {
//        this.investmentListener = investmentListener;
//    }
//
//    public void setMonitoringAlertsListener(LevelsCacheMonitoringAlertsListener monitoringAlertsListener) {
//        this.monitoringAlertsListener = monitoringAlertsListener;
//    }
//
//    /**
//     * send data to specific group page
//     *
//     * @param DataToSend the data to send from ls
//     * @param skinsPriorities group priorities to send each group page diff priorities
//     * @param groupId the group id
//     */
//    protected void sendGroupUpdate(HashMap<String, String> DataToSend , HashMap<Integer, HashMap<String, Integer>> skinsPriorities , long groupId) {
//        Integer key = null;
//        for (Iterator<Integer> i = skinsPriorities.keySet().iterator(); i.hasNext();) {
//            key = i.next();
//            HashMap<String, String> tmpClone = (HashMap<String, String>)DataToSend.clone();
//            if (tmpClone.containsKey("ET_GROUP_PRIORITY")) {
//                String groupPriority = tmpClone.get("ET_GROUP_PRIORITY");
//                String [] temp = null;
//                temp = groupPriority.split("_"); // split it to priority and date
//                int groupSkinPriority = skinsPriorities.get(key).get("group_priority");
//                tmpClone.put("ET_GROUP_PRIORITY", String.valueOf(groupSkinPriority + Integer.parseInt(temp[0]))+ temp[1]);
//                listener.update("group_" + key + "_" + groupId, (HashMap<String, String>) tmpClone);
//                log.debug("sent update to group number " + groupId + " with skin id " + key);
//            }
//        }
//    }
//
//    /**
//     * send data to specific home page
//     *
//     * @param DataToSend
//     * @param skinsPriorities
//     * @param key
//     */
//    protected void sendHomeUpdate(HashMap<String, String> DataToSend , HashMap<Integer, HashMap<String, Integer>> skinsPriorities , int key) {
//        if (null != skinsPriorities) {
//            if (!skinsPriorities.get(key).isEmpty()) {
//                HashMap<String, String> tmpClone = (HashMap<String, String>)DataToSend.clone();
//                if (tmpClone.containsKey("ET_PRIORITY")) {
//                    String homePagePriority = tmpClone.get("ET_PRIORITY");
//                    int homePageSkinPriority = skinsPriorities.get(key).get("home_page_priority");
//                    tmpClone.put("ET_PRIORITY", String.valueOf(homePagePriority + homePageSkinPriority));
//                    listener.update("home_" + key, (HashMap<String, String>) tmpClone);
//                    log.debug("sent update to home with skin id " + key);
//                }
//            }
//        }
//    }
//
//    protected void sendInsurancesUpdate(long oppId, HashMap<String, String> tmp, double level) {
//        listener.insurancesUpdate(oppId, tmp, level);
//    }
//
//    /**
//     * Notify LevelService about investment that was made.
//     *
//     * @param oppId the id of the opp on which an investment was made
//     * @param investmentId the id of the investment
//     * @param amount the amount invested (in cents)
//     * @param type the type of the investment (call (1)/put (2))
//     */
//    public void notifyForInvestment(long oppId, double investmentId, double amount, long type) {
//        sender.send("investment_" + oppId + "_" + investmentId + "_" + amount + "_" + type);
//    }
//    
//    /**
//     * Notify LevelService about investment that was made.
//     *
//     * @param oppId the id of the opp on which an investment was made
//     * @param investmentId the id of the investment
//     * @param amount the amount invested (in cents)
//     * @param type the type of the investment (call (1)/put (2))
//     * @param amountForDisplay investment amount in user's currency
//     * @param cityNameInEnglish
//     * @param countryId
//     * @param marketId
//     * @param productTypeId the type of the investment (call (1)/put (2))
//     * @param userSkinId the skin ID of the user that made the investment - will be used when filtering investments for LIVE AO "Constant stream of investments" table
//     * @param level formatted string for display
//     * @param time number of millis since epoch
//     * @param globeLat City Latitude
//     * @param globeLong City Longitude
//     * @param flooredAmount in USD
//     */
//    public void notifyForInvestment(long oppId,
//    								double investmentId,
//    								double amount,
//    								long type,
//    								String amountForDisplay,
//    								String cityNameInEnglish,
//    								long countryId,
//    								long marketId,
//    								int productTypeId,
//    								long userSkinId,
//    								double level,
//    								long time,
//    								String globeLat,
//    								String globeLong,
//    								long flooredAmount) {
//    	notifyForInvestment(oppId,
//    						investmentId,
//    						amount,
//    						type,
//    						amountForDisplay,
//    						cityNameInEnglish,
//    						countryId,
//    						marketId,
//    						productTypeId,
//    						userSkinId,
//    						level,
//    						time,
//    						globeLat,
//    						globeLong,
//    						0,
//    						0,
//    						flooredAmount);
//    }
//
//    /**
//     * Notify LevelService about investment that was made.
//     *
//     * @param oppId the id of the opp on which an investment was made
//     * @param investmentId the id of the investment
//     * @param amount the amount invested (in cents)
//     * @param type the type of the investment (call (1)/put (2))
//     * @param amountForDisplay investment amount in user's currency
//     * @param cityNameInEnglish
//     * @param countryId
//     * @param marketId
//     * @param productTypeId the type of the investment (call (1)/put (2))
//     * @param userSkinId the skin ID of the user that made the investment - will be used when filtering investments for LIVE AO "Constant stream of investments" table
//     * @param level formatted string for display
//     * @param time number of millis since epoch
//     * @param globeLat City Latitude
//     * @param globeLong City Longitude
//     * @param aboveTotal for 0100 tt column
//     * @param belowTotal for 0100 tt column
//     * @param flooredAmount in USD
//     */
//    public void notifyForInvestment(long oppId,
//    								double investmentId,
//    								double amount,
//    								long type,
//    								String amountForDisplay,
//    								String cityNameInEnglish,
//    								long countryId,
//    								long marketId,
//    								int productTypeId,
//    								long userSkinId,
//    								double level,
//    								long time,
//    								String globeLat,
//    								String globeLong,
//    								double aboveTotal,
//    								double belowTotal,
//    								long flooredAmount) {
//        sender.send("investment_" + oppId
//							+ "_" + investmentId
//							+ "_" + amount
//							+ "_" + type
//							+ "_" + amountForDisplay
//							+ "_" + cityNameInEnglish
//							+ "_" + countryId
//							+ "_" + marketId
//							+ "_" + productTypeId
//							+ "_" + userSkinId
//							+ "_" + level
//							+ "_" + time
//							+ "_" + globeLat
//							+ "_" + globeLong
//							+ "_" + aboveTotal
//							+ "_" + belowTotal
//							+ "_" + flooredAmount);
//    }
//
//    /**
//     * Notify LevelService about Avg Invest in Last 30 Days change.
//     *
//     * @param useAvgInvLast30Days specify which mode of calculation to use
//     *      use one of <code>ServiceConfig.AVERAGE_INVESTMENT_FIXED</code>,
//     *      <code>ServiceConfig.AVERAGE_INVESTMENT_CALCULATED</code>,
//     *      <code>ServiceConfig.AVERAGE_INVESTMENT_MAX_OF_BOTH</code>
//     * @param avgInvLast30Days the new fixed value.
//     */
////    public void notifyForAvgInvLast30DaysChange(int useAvgInvLast30Days, double avgInvLast30Days) {
////        sender.send("avg_" + useAvgInvLast30Days + "_" + avgInvLast30Days);
////    }
//
//    /**
//     * Notify service for change in market configuration.
//     *
//     * @param feedName the feed name of the market (.TA25 for example)
//     * @param updatesPause min pause in milliseconds between updates sent to JMS
//     * @param significantPercentage percent of change under which update will be discarded (eg: 1% is 0.01)
//     * @param realUpdatesPause min pause in milliseconds between updates sent to JMS for real level
//     * @param realSignificantPercentage percent of change under which real updates will be discarded (eg: 1% is 0.01)
//     * @param randomCeiling up boundary of random change for the current level (eg: 0.025% is 0.00025)
//     * @param randomFloor down boundary of random change for the current level (eg: -0.025% is -0.00025)
//     * @param firstShiftParameter what is the first shift size in percentage (eg: 0.02 for 2%)
//     * @param percentageOfAvgInvestment what is the percentage of the average_investments.
//     *      in case |sum of call - sum of puts| >  percentage_of_average_investments * 100 * average_investments
//     *      -> we will shift
//     *      (eg: 0.1 for 10%)
//     * @param fixedAmountForShifting what is the amount incase
//     *      |sum of call � sum of puts| > fixed_amount_for_shifting -> we will shift
//     *      (full currency units)
//     * @param typeOfShifting what type of shifting to use:
//     *      -) <code>Market.TYPE_OF_SHIFTING_FIXED_AMOUNT</code> - only
//     *          fixed_amount_for_shifting (no percentage)
//     *      -) <code>Market.TYPE_OF_SHIFTING_AVERAGE_INVESTMENT</code> -
//     *          only average_investments * percentage_of_average_investments * 100
//     *      -) <code>Market.TYPE_OF_SHIFTING_MAX_OF_BOTH</code> -
//     *          the highest from both
//     * @param disableAfterPeriod period in millis after which the market opps will be disabled. 0 to turn this monitoring off.
//     * @return <code>true</code> if successfull else <code>false</code>
//     */
//    public boolean notifyForMarketCfgChange(
//            String feedName,
//            int updatesPause,
//            BigDecimal significantPercentage,
//            int realUpdatesPause,
//            BigDecimal realSignificantPercentage,
//            BigDecimal randomCeiling,
//            BigDecimal randomFloor,
//            BigDecimal firstShiftParameter,
//            BigDecimal percentageOfAvgInvestment,
//            BigDecimal fixedAmountForShifting,
//            int typeOfShifting,
//            long disableAfterPeriod) {
//        sender.send(
//                "marketcfg_" +
//                feedName + "_" +
//                updatesPause + "_" +
//                significantPercentage + "_" +
//                realUpdatesPause + "_" +
//                realSignificantPercentage + "_" +
//                randomCeiling + "_" +
//                randomFloor + "_" +
//                firstShiftParameter + "_" +
//                percentageOfAvgInvestment + "_" +
//                fixedAmountForShifting + "_" +
//                typeOfShifting + "_" +
//                disableAfterPeriod);
//        return false;
//    }
//
//    /**
//     * Notify LevelService about TA25 parameter change.
//     *
//     * @param ta25Parameter the new parameter value
//     * @return <code>true</code> if successfull else <code>false</code>
//     */
//    public boolean notifyForTA25ParameterChange(BigDecimal ta25Parameter) {
//        sender.send("ta25_" + ta25Parameter);
//        return false;
//    }
//
//    /**
//     * Notify LevelService about FTSE parameter change.
//     *
//     * @param ftseParameter the new parameter value
//     * @return <code>true</code> if successfull else <code>false</code>
//     */
//    public boolean notifyForFTSEParameterChange(BigDecimal ftseParameter) {
//        sender.send("ftse_" + ftseParameter);
//        return false;
//    }
//
//    /**
//     * Notify LevelService about CAC parameter change.
//     *
//     * @param cacParameter the new parameter value
//     * @return <code>true</code> if successfull else <code>false</code>
//     */
//    public boolean notifyForCACParameterChange(BigDecimal cacParameter) {
//        sender.send("cac_" + cacParameter);
//        return false;
//    }
//
//    /**
//     * Notify LevelService about SP500 parameter change.
//     *
//     * @param SP500Parameter the new parameter value
//     * @return <code>true</code> if successfull else <code>false</code>
//     */
//    public boolean notifyForSP500ParameterChange(BigDecimal SP500Parameter) {
//        sender.send("sp500_" + SP500Parameter);
//        return false;
//    }
//
//    /**
//     * Notify LevelService about opportunity odds change.
//     *
//     * @param oppId the id of the opp which odds have been changed
//     * @return <code>true</code> if successfull else <code>false</code>
//     */
//    public boolean notifyForOpportunityOddsChange(long oppId) {
//        sender.send("odds_" + oppId);
//        return false;
//    }
//
//    /**
//     * Notify LevelService about opportunity shift parameter change.
//     *
//     * @param oppId the id of the opp which shift parameter have been changed
//     * @param shiftParameter the new value of the opp shift parameter
//     *      (in % - the current level is multiplied by (1 + shiftParameter))
//     *      (eg. pass 0.01 for 1%)
//     * @return <code>true</code> if successfull else <code>false</code>
//     */
//    public boolean notifyForOpportunityShiftParameterChange(long oppId, BigDecimal shiftParameter) {
//        sender.send("shift_" + oppId + "_" + shiftParameter);
//        return false;
//    }
//
//    /**
//     * Manually close opportunity.
//     *
//     * @param oppId the id of the opportunity to close
//     * @param closingLevel the closing level for the opportunity
//     * @return <code>true</code> if successfull else <code>false</code>
//     */
//    public boolean manuallyCloseOpportunity(long oppId, BigDecimal closingLevel) {
//        sender.send("close_" + oppId + "_" + closingLevel);
//        return false;
//    }
//
//    /**
//     * Suspend/unsuspend marekt.
//     *
//     * @param feedName the feed name of the market to suspend/unsuspend
//     * @param suspend <code>true</code> to suspend <code>false</code> to unsuspend
//     * @param message the message to put on the suspended market on the trading pages
//     * @return <code>true</code> if successfull else <code>false</code>
//     */
//    public boolean suspendMarket(String feedName, boolean suspend, String message) {
//        sender.send("suspend_" + feedName + "_" + suspend + "_" + message);
//        return false;
//    }
//
//    /**
//     * Register the instance in the platform MBean server under given name.
//     *
//     * @param name the name under which to register
//     */
//    public void registerJMX(String name) {
//        jmxName = name;
//        try {
//            MBeanServer mbs = ManagementFactory.getPlatformMBeanServer();
//            ObjectName on = new ObjectName(jmxName);
//            mbs.registerMBean(this, on);
//            if (log.isInfoEnabled()) {
//                log.info("LevelsCahce MBean registered - name: " + jmxName);
//            }
//        } catch (Throwable t) {
//            log.error("Failed to register in the MBean server.", t);
//        }
//    }
//
//    /**
//     * Unregister the instance from the platform MBean server. Use the name
//     * under which it was registered.
//     */
//    public void unregisterJMX() {
//        try {
//            MBeanServer mbs = ManagementFactory.getPlatformMBeanServer();
//            ObjectName on = new ObjectName(jmxName);
//            mbs.unregisterMBean(on);
//            if (log.isInfoEnabled()) {
//                log.info("LevelsCahce MBean unregistered - name: " + jmxName);
//            }
//        } catch (Throwable t) {
//            log.error("Failed to unregister from the MBean server.", t);
//        }
//    }
//}