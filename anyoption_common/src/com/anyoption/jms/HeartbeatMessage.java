//package com.anyoption.jms;
//
//import java.io.Serializable;
//
//public class HeartbeatMessage implements Serializable {
//	private static final long serialVersionUID = 1L;
//	public long lifeId;
//	
//	public HeartbeatMessage(long lifeId) {
//		this.lifeId = lifeId;
//	}
//    
//    public String toString() {
//        String ls = System.getProperty("line.separator");
//        return ls + "HeartbeatMessage:" + ls +
//            "lifeId: " + lifeId + ls;
//    }
//}