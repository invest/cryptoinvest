//package com.anyoption.jms;
//
//import java.util.Collections;
//import java.util.Hashtable;
//import java.util.Iterator;
//import java.util.LinkedList;
//import java.util.List;
//
//import javax.jms.JMSException;
//import javax.jms.Message;
//import javax.jms.ObjectMessage;
//
//import org.apache.log4j.Logger;
//
//import com.anyoption.common.jms.ETraderFeedMessage;
//import com.anyoption.common.jms.WebLevelLookupBean;
//import com.anyoption.common.jms.ifc.LevelsCacheListener;
//
///**
// * JMS communication component for WEB applications.
// *
// * @author Tony
// */
//public class WebLevelsCache extends LevelsCache {
//    private static final Logger log = Logger.getLogger(WebLevelsCache.class);
//
//    private List<WebLevelLookupBean> requests = null;
//    private Hashtable<Long, long[]> currentMarketsOnHomePage;
//    private long[] openedMarkets;
//
//    public WebLevelsCache(String initialContextFactory, String providerURL, String connectionFactoryName, String queueName, String topicName, int msgPoolSize, int recoveryPause, LevelsCacheListener listener) {
//        super(initialContextFactory, providerURL, connectionFactoryName, queueName, topicName, msgPoolSize, recoveryPause, listener);
//
//        requests = Collections.synchronizedList(new LinkedList<WebLevelLookupBean>());
//        currentMarketsOnHomePage = new Hashtable<Long, long[]>();
//    }
//
//    public void getLevels(WebLevelLookupBean wllb) {
//        requests.add(wllb);
//        sender.send("level_" + wllb.getFeedName() + "_" + wllb.getOppId() + "_" + wllb.getSkinGroup().getId());
//        synchronized (wllb) {
//            try {
//                wllb.wait(3000);
//            } catch (InterruptedException ie) {
//                // do nothing
//            }
//        }
//    }
//
//    public long[] getMarketsOnHomePage(long skinId) {
//        return currentMarketsOnHomePage.get(skinId);
//    }
//
//    public long[] getOpenedMarkets() {
//        return openedMarkets;
//    }
//
//    public void onMessage(Message message) {
//        if (null == message) {
//            log.warn("Invalid message");
//            return;
//        }
//        if (log.isDebugEnabled()) {
//            log.debug("Received message");
//        }
//        //we have to extract data from the Message object
//        ETraderFeedMessage feedMsg = null;
//        try {
//            if (message instanceof ObjectMessage) {
//                //cast Message to ObjectMessage.
//                Object obj = ((ObjectMessage) message).getObject();
//                if (obj instanceof ETraderFeedMessage) {
//                    feedMsg = (ETraderFeedMessage) obj;
//                } else if (obj instanceof String) {
//                    String msg = (String) obj;
//                    if (log.isDebugEnabled()) {
//                        log.debug("String message received: " + msg);
//                    }
//                    if (msg.startsWith("aohp")) {
//                        String[] arr = msg.split("_");
//                        String[] ms = arr[2].split("\\|");
//                        long[] lms = new long[ms.length];
//                        for (int i = 0; i < ms.length; i++) {
//                            lms[i] = Long.parseLong(ms[i]);
//                        }
//                        currentMarketsOnHomePage.put(Long.parseLong(arr[1]), lms);
//                    } else if (msg.startsWith("aost")) {
//                        String[] arr = msg.split("_");
//                        long[] lms;
//                        if (arr.length > 1) {
//                            String[] ms = arr[1].split("\\|");
//                            lms = new long[ms.length];
//                            for (int i = 0; i < ms.length; i++) {
//                                lms[i] = Long.parseLong(ms[i]);
//                            }
//                        } else {
//                            lms = new long[0];
//                        }
//                        openedMarkets = lms;
//                    } else {
//                        if (log.isDebugEnabled()) {
//                            log.debug("Got message: " + message);
//                        }
//                    }
//                    /* !!! WARNING !! METHOD EXIT POITN !!! */
//                    return;
//                } else {
//                    if (log.isDebugEnabled()) {
//                        log.debug("Got message: " + message);
//                    }
//                    return;
//                }
//                if (log.isDebugEnabled()) {
//                    log.debug("Valid message");
//                }
//            } else {
//                if (log.isDebugEnabled()) {
//                    log.debug("Got message: " + message);
//                }
//                return;
//            }
//        } catch (ClassCastException jmse) {
//            //if message isn't an ObjectMessage or message.getObject() isn't a ETraderFeedMessage
//            //then this update is not "correct"
//            log.warn("Invalid message - (no ETraderFeedMessage instance)");
//            return;
//        } catch (JMSException jmse) {
//            log.error("StockQuotesJMSAdapter.onMessage - JMSException: " + jmse.getMessage(), jmse);
//            return;
//        }
//        if (log.isTraceEnabled()) {
//            log.trace("Message: " + feedMsg);
//        }
//        try {
//            WebLevelLookupBean wllb = null;
//            for (Iterator<WebLevelLookupBean> i = requests.iterator(); i.hasNext();) {
//                wllb = i.next();
//                if (wllb.getOppId() == feedMsg.oppId) {
//                    i.remove();
//                    wllb.setDevCheckLevel(feedMsg.devCheckLevel);
//                    wllb.setRealLevel(feedMsg.realLevel);
//                    wllb.setAutoDisable(feedMsg.isAutoDisable);
//                    wllb.setBid(feedMsg.bid);
//                    wllb.setOffer(feedMsg.offer);
//                    wllb.setInvestmentsRejectInfo(feedMsg.investmentsRejectInfo);
//                    synchronized (wllb) {
//                        wllb.notify();
//                    }
//                }
//            }
//        } catch (Throwable t) {
//            log.error("Failed to process level update.", t);
//        }
//    }
//
//    public void getSnapshot() {
//        sender.send("homepages");
//    }
//}