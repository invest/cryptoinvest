//package com.anyoption.jms;
//
///**
// * Helper bean for WEB apps level lookup.
// *
// * @author Tony
// */
//public class WebLevelLookupBean {
//    protected long oppId;
//    protected String feedName;
//    protected double devCheckLevel;
//    protected double realLevel;
//    protected boolean isAO;
//    protected boolean isAutoDisable;
//    protected double bid;
//    protected double offer;
//    protected String investmentsRejectInfo;
//
//	public WebLevelLookupBean(boolean isAO) {
//		super();
//		this.isAO = isAO;
//	}
//
//    public double getDevCheckLevel() {
//        return devCheckLevel;
//    }
//
//    public void setDevCheckLevel(double devCheckLevel) {
//        this.devCheckLevel = devCheckLevel;
//    }
//
//    public String getFeedName() {
//        return feedName;
//    }
//
//    public void setFeedName(String feedName) {
//        this.feedName = feedName;
//    }
//
//    public long getOppId() {
//        return oppId;
//    }
//
//    public void setOppId(long oppId) {
//        this.oppId = oppId;
//    }
//
//    public double getRealLevel() {
//        return realLevel;
//    }
//
//    public void setRealLevel(double realLevel) {
//        this.realLevel = realLevel;
//    }
//
//    public boolean isAO() {
//		return isAO;
//	}
//
//	public void setAO(boolean isAO) {
//		this.isAO = isAO;
//	}
//
//	public boolean isAutoDisable() {
//		return isAutoDisable;
//	}
//
//	public void setAutoDisable(boolean isAutoDisable) {
//		this.isAutoDisable = isAutoDisable;
//	}
//
//	public double getBid() {
//		return bid;
//	}
//
//	public void setBid(double bid) {
//		this.bid = bid;
//	}
//
//	public double getOffer() {
//		return offer;
//	}
//
//	public void setOffer(double offer) {
//		this.offer = offer;
//	}
//
//	public String getInvestmentsRejectInfo() {
//		return investmentsRejectInfo;
//	}
//
//	public void setInvestmentsRejectInfo(String investmentsRejectInfo) {
//		this.investmentsRejectInfo = investmentsRejectInfo;
//	}
//
//	public String toString() {
//        String ls = System.getProperty("line.separator");
//        return ls + "WebLevelLookupBean:" + ls +
//            "oppId: " + oppId + ls +
//            "feedName: " + feedName + ls +
//            "devCheckLevel: " + devCheckLevel + ls +
//            "realLevel: " + realLevel + ls +
//            "isAO: " + isAO + ls +
//            "isAutoDisable: " + isAutoDisable + ls +
//            "bid: " + bid + ls +
//            "offer: " + offer + ls +
//        	"investmentsRejectInfo: " + investmentsRejectInfo + ls;
//    }
//}