//package com.anyoption.jms;
//
//import java.io.Serializable;
//import java.util.ArrayList;
//import java.util.Properties;
//
//import javax.jms.JMSException;
//import javax.jms.Message;
//import javax.jms.ObjectMessage;
//import javax.jms.Queue;
//import javax.jms.QueueConnection;
//import javax.jms.QueueConnectionFactory;
//import javax.jms.QueueReceiver;
//import javax.jms.QueueSender;
//import javax.jms.QueueSession;
//import javax.jms.Session;
//import javax.jms.TextMessage;
//import javax.jms.Topic;
//import javax.jms.TopicConnection;
//import javax.jms.TopicConnectionFactory;
//import javax.jms.TopicPublisher;
//import javax.jms.TopicSession;
//import javax.jms.TopicSubscriber;
//import javax.naming.Context;
//import javax.naming.InitialContext;
//import javax.naming.NamingException;
//
//import org.apache.log4j.Logger;
//
//import com.anyoption.common.jms.ifc.ExtendedMessageListener;
//
//public class JMSHandler {
//	private static final Logger logger = Logger.getLogger(JMSHandler.class);
//	
//    private QueueSender queueSender;
//    private QueueSession queueSession;
//    private QueueConnection queueConnection;
//    private Queue queue;
//    private String queueConnectionFactoryName;
//    private String queueName;
//    private boolean queueSessionReady = false;
//    
//    private TopicPublisher topicPublisher;
//    private TopicSession topicSession;
//    private TopicConnection topicConnection;
//    private Topic topic;
//    private String topicConnectionFactoryName;
//    private String topicName;
//    private boolean topicSessionReady = false;
//    
//    private boolean JMSReady = false;
//    private Context jndiContext;
//    
//    private ExtendedMessageListener messageListener;
//    
//	private TextMessagePool textMessagePool;
//	private ObjectMessagePool objectMessagePool;
//	
//	private String initialContextFactory;
//	private String providerURL;
//    private String logName;
//	
//	/**
//	 * This object can handle:
//	 * 1 TopicSubscriber and 1 TopicPublisher
//	 * related to the same ConnectionFactory
//	 * 1 QueueReceiver and 1 QueueSender
//	 * related to the same ConnectionFactory
//	 * All related to the same InitialContextFactory
//	 */
//	public JMSHandler(String logName, String initialContextFactory, String providerURL, String queueConnectionFactoryName, String queueName, String topicConnectionFactoryName, String topicName) {
//        this.logName = logName;
//		this.providerURL = providerURL;
//		this.initialContextFactory = initialContextFactory;
//		
//		this.topicConnectionFactoryName = topicConnectionFactoryName;
//		this.topicName = topicName;
//		
//		this.queueConnectionFactoryName = queueConnectionFactoryName;
//		this.queueName = queueName;
//		
//		if (logger.isDebugEnabled()) {
//		    logger.debug(logName + " JMSHandler Ready");
//        }
//	}
//	
//	public void setListener(ExtendedMessageListener messageListener) {
//		this.messageListener = messageListener;
//	}
//	
//	/**
//	 * initiate the InitialContext
//	 */
//	private synchronized void initJMS() throws JMSException, NamingException{
//		if (JMSReady) {
//			//InitialContext is already OK, exit
//			return;
//		}
//		
//		//Prepare a Properties object to be passed to the InitialContext
//		//constructor giving the InitialContextFactory name and
//		//the JMS server url
//		Properties properties = new java.util.Properties();
//		properties.put(javax.naming.Context.INITIAL_CONTEXT_FACTORY, this.initialContextFactory);
//		properties.put(javax.naming.Context.PROVIDER_URL, this.providerURL);
//		
//		//create the InitialContext
//		this.jndiContext = new InitialContext(properties);
//		logger.info(logName + " JNDI Context[" + jndiContext.getEnvironment() + "]...");
//		
//		//InitialContext is now ready
//		JMSReady = true;
//	}
//	
//	/**
//	 * close all open Sessions/Connections and unset ready flags
//	 */
//	public synchronized void reset() {
//        if (logger.isInfoEnabled()) {
//            logger.info(logName + " Reset JMSHandler begin");
//        }
//		if (topicSession != null) {
//			try {
//				topicSession.close();
//			} catch (JMSException e) {
//                logger.error(logName + " Can't close topic session.", e);
//			}
//		}
//		if (topicConnection != null) {
//			try {
//				topicConnection.close();
//			} catch (JMSException e) {
//                logger.error(logName + " Can't close topic connection", e);
//			}
//		}
//		if (queueSession != null) {
//			try {
//				queueSession.close();
//			} catch (JMSException e) {
//                logger.error(logName + " Can't close queue session.", e);
//			}
//		}
//		if (queueConnection != null) {
//			try {
//				queueConnection.close();
//			} catch (JMSException e) {
//                logger.error(logName + " Can't close queue connection.", e);
//			}
//		}
//		topicSession = null;
//		topicConnection = null;
//		queueSession = null;
//		queueConnection = null;
//		JMSReady = false;
//		queueSessionReady = false;
//		topicSessionReady = false;
//        if (logger.isInfoEnabled()) {
//            logger.info(logName + " Reset JMSHandler end");
//        }
//	}
//	
//    public synchronized void close() {
//        reset();
//        if (null != jndiContext) {
//            if (logger.isInfoEnabled()) {
//                logger.info(logName + " Closing InitialContext.");
//            }
//            try {
//                jndiContext.close();
//            } catch (Throwable t) {
//                logger.error(logName + " Cant close InitialContext.", t);
//            }
//        }
//    }
//    
//	/**
//	 * prepare the QueueSession
//	 */
//	private synchronized void initQueueSession() throws JMSException, NamingException {
//		if (queueSessionReady) {
//			//QueueSession is already OK, exit
//			return;
//		}
//		//first of all we have to inititiate the InitialContext
//		//(without this we can't instantiate a QueueSession)
//		initJMS();
//		
//		//lookup to find our QueueConnectionFactory
//		QueueConnectionFactory queueConnectionFactory = null;
//        if (logger.isInfoEnabled()) {
//            logger.info(logName + " Looking up queue connection factory [" + queueConnectionFactoryName + "]...");
//        }
//		queueConnectionFactory = (QueueConnectionFactory) jndiContext.lookup(queueConnectionFactoryName);
//	
//		//lookup to find our queue
//        if (logger.isInfoEnabled()) {
//            logger.info(logName + " Looking up queue [" + queueName + "]...");
//        }
//		this.queue = (Queue) jndiContext.lookup(queueName);
//
//		//get the QueueConnection from our QueueConnectionFactory
//		queueConnection = queueConnectionFactory.createQueueConnection();
//        if (logger.isDebugEnabled()) {
//            logger.debug(logName + " Queue connection created");
//        }
//		
//		//if set we pass our ExtendedMessageListener to the QueueConnection as ExceptionListener
//		if (messageListener != null) {
//			queueConnection.setExceptionListener(messageListener);
//		}
//		
//		//get the QueueSession from our QueueConnectionFactory
//		queueSession = queueConnection.createQueueSession(false, Session.AUTO_ACKNOWLEDGE);
//        if (logger.isDebugEnabled()) {
//            logger.debug(logName + " Queue session created");
//        }
//        
//        //QueueSession is now ready
//        queueSessionReady = true;
//	}
//	
//	/**
//	 * prepare the TopicSession
//	 */
//	private synchronized void initTopicSession() throws JMSException, NamingException {
//		if (topicSessionReady) {
//			//TopicSession is already OK, exit
//			return;
//		}
//		//first of all we have to inititiate the InitialContext
//		//(without this we can't instantiate a TopicSession)
//		initJMS();
//		
//		//lookup to find our TopicConnectionFactory
//		TopicConnectionFactory topicConnectionFactory = null;
//        if (logger.isInfoEnabled()) {
//            logger.info(logName + " Looking up topic connection factory [" + topicConnectionFactoryName + "]...");
//        }
//		topicConnectionFactory = (TopicConnectionFactory) jndiContext.lookup(topicConnectionFactoryName);
//		
//		//lookup to find our Topic
//        if (logger.isInfoEnabled()) {
//            logger.info(logName + " Looking up topic [" + topicName + "]...");
//        }
//		topic = (Topic) jndiContext.lookup(topicName);
//		
//		//get the TopicConnection from our TopicConnectionFactory
//		topicConnection = topicConnectionFactory.createTopicConnection();
//        if (logger.isDebugEnabled()) {
//            logger.debug(logName + " Topic connection created");
//        }
//		
//		//if set we pass our ExtendedMessageListener to the TopicConnection as ExceptionListener
//		if (messageListener != null) {
//			topicConnection.setExceptionListener(messageListener);
//		}
//		
//		//get the TopicSession from our TopicConnectionFactory
//		topicSession = topicConnection.createTopicSession(false, Session.AUTO_ACKNOWLEDGE);
//        if (logger.isDebugEnabled()) {
//            logger.debug(logName + " Topic session created");
//        }
//
//        //TopicSession is now ready
//        topicSessionReady = true;
//	}
//	
//	/**
//	 * prepare the QueueReceiver 
//	 */
//	public  synchronized void initQueueReceiver() throws JMSException, NamingException {
//		//first of all we have to inititiate the QueueSession
//		//(without this we can't instantiate a QueueReceiver)
//		initQueueSession();
//		
//		//get the QueueReceiver from our QueueSession
//		QueueReceiver queueReceiver = queueSession.createReceiver(queue);
//        if (logger.isDebugEnabled()) {
//            logger.debug(logName + " Queue receiver created");
//        }
//		
//		//if set we pass our ExtendedMessageListener to the QueueReceiver as MessageListener
//		if (messageListener != null) {
//			queueReceiver.setMessageListener(messageListener);
//		}
//		
//		//start listening to JMS
//		queueConnection.start();
//        if (logger.isDebugEnabled()) {
//            logger.debug(logName + " Queue connection started");
//        }
//	}
//	
//	public synchronized void initQueueSender(int msgPoolSize) throws JMSException, NamingException {
//		//first of all we have to inititiate the QueueSession
//		//(without this we can't instantiate a QueueSender)
//		initQueueSession();
//		
//		//get the QueueSender from our QueueSession
//		queueSender = queueSession.createSender(queue);
//        if (logger.isDebugEnabled()) {
//            logger.debug(logName + " Queue sender created");
//        }
//		
//		//create the message pool for text messages
//		textMessagePool = new TextMessagePool(queueSession, msgPoolSize);
//        if (logger.isDebugEnabled()) {
//            logger.debug(logName + " Text message pool created");
//        }
//	}
//	
//	public synchronized void sendMessage(String text) throws JMSException {
//		//check if QueueSession is ready
//		if (!queueSessionReady) {
//			//QueueSession is not ready, we can't send messages
//			throw new JMSException("Queue sender not ready");
//		}
//		
//		//get a TextMessage from the pool
//		TextMessage textMessage = (TextMessage) this.textMessagePool.getMessage();
//		//fill it with text (our message to be sent)
//    	textMessage.setText(text);
//        if (logger.isDebugEnabled()) {
//            logger.debug(logName + " Sending message: " + text);
//        }
//		//send to JMS
//		queueSender.send(textMessage);
//		//release the TextMessage to the pool
//		textMessagePool.release(textMessage);
//	}
//	
//	public synchronized void initTopicSubscriber() throws JMSException, NamingException {
//		//first of all we have to inititiate the TopicSession
//		//(without this we can't instantiate a TopicSubscriber)
//		initTopicSession();
//		
//		//get the TopicSubscriber from our TopicSession
//		TopicSubscriber topicSubscriber = topicSession.createSubscriber(topic, null, true);
//        if (logger.isDebugEnabled()) {
//            logger.debug(logName + " Topic subscriber created");
//        }
//		
//		//if set we pass our ExtendedMessageListener to the TopicSubscriber as MessageListener
//		if (messageListener != null) {
//			topicSubscriber.setMessageListener(messageListener);
//		}
//		
//		//start listening to JMS
//		topicConnection.start();
//        if (logger.isInfoEnabled()) {
//            logger.info(logName + " Topic connection started");
//        }
//	}
//	
//	public synchronized void initTopicPublisher(int msgPoolSize) throws JMSException, NamingException {
//		//first of all we have to inititiate the TopicSession
//		//(without this we can't instantiate a TopicPublisher)
//		initTopicSession();
//		
//		//get the TopicPublisher from our TopicSession
//        this.topicPublisher = topicSession.createPublisher(topic);
//        if (logger.isDebugEnabled()) {
//            logger.debug(logName + " Topic publisher created");
//        }
//        
//        //create the message pool for ETraderFeedMessage messages
//        this.objectMessagePool = new ObjectMessagePool(topicSession, msgPoolSize);
//        if (logger.isDebugEnabled()) {
//            logger.debug(logName + " Object message pool created");
//        }
//	}
//	
//	
//	public synchronized void publishMessage(Serializable obj) throws JMSException{
//		//check if TopicSession is ready
//		if (!topicSessionReady) {
//			//TopicSession is not ready, we can't publish messages
//			throw new JMSException("Topic publisher not ready");
//		}
//		long startTimestamp = System.currentTimeMillis();
//		//get an ObjectMessage from the pool
//		ObjectMessage objectMessage = (ObjectMessage) this.objectMessagePool.getMessage();
//    	//fill it with obj (our message to be sent)
//		objectMessage.setObject(obj);
//		//publish to JMS
//		this.topicPublisher.publish(objectMessage);
//		//release the ObjectMessage to the pool
//		this.objectMessagePool.release(objectMessage);
//        if (logger.isDebugEnabled()) {
//            logger.debug(logName + obj + " Published in: " + (System.currentTimeMillis() - startTimestamp));
//        }
//	}
//	
//	
//	
////////////////////////MessagePool
//    
//    /**
//     * Implement a pool of JMS messages used in subscribe/unsubscribe operations.
//     */
//    private abstract class MessagePool {
//    	protected Session session = null;
//    	private int lenSegmentMsgPool = 0;
//    	private ArrayList<Message> freeMessagePool = null;
//    	
//    	/**
//    	 * create the pool
//    	 */
//    	public MessagePool(Session session, int lenSegmentMsgPool) throws JMSException {
//    		this.session = session;
//    		this.lenSegmentMsgPool = lenSegmentMsgPool;
//    		this.freeMessagePool = new ArrayList<Message>(lenSegmentMsgPool);
//
//    		//fill the pool with lenSegmentMsgPool empty messages
//    		this.createNewMessages(this.lenSegmentMsgPool);
//    	}
//    	
//    	/**
//    	 * add new messages to the pool
//    	 */
//    	private void createNewMessages(int lenSegmentMsgPool) throws JMSException {
//    		for (int i=0; i<lenSegmentMsgPool; i++) {
//    			this.freeMessagePool.add(this.createMessage());
//    		}
//    	}
//    	
//    	/**
//    	 * this method will be implemented by subclasses. Each subclass
//    	 * can create its own type of message
//    	 */
//    	protected abstract Message createMessage() throws JMSException;
//    	
//    	/**
//    	 * get a message from the pool 
//    	 */
//    	public synchronized Message getMessage() throws JMSException {
//    		if (this.freeMessagePool.size() == 0) {
//    			//if there aren't free messages in the pool, fill
//    			//the pool with new messages
//    			this.createNewMessages(this.lenSegmentMsgPool);
//    		}
//    		//remove a message from the free messages list and return to caller 
//    		return (Message) this.freeMessagePool.remove(this.freeMessagePool.size()-1);
//    	}
//    	
//    	public synchronized void release(Message message) throws JMSException {
//    		if (message == null) {
//    			//message is null, can't add to the pool
//    			logger.error(logName + " Can't realese a null message in free message pool");
//                throw new JMSException("Message pool error");
//    		}
//
//    		//put the released message in the free messages list
//    		this.freeMessagePool.add(message);
//    	}
//    }
//    
//    private class TextMessagePool extends MessagePool {
//    	public TextMessagePool(Session session, int lenSegmentMsgPool) throws JMSException {
//    		super(session, lenSegmentMsgPool);
//    	}
//    	
//    	protected Message createMessage() throws JMSException {
//    		//use a JMS session to create a new TextMessage
//    		return this.session.createTextMessage();
//    	}
//    }
//    
//    private class ObjectMessagePool extends MessagePool {
//    	public ObjectMessagePool(Session session, int lenSegmentMsgPool) throws JMSException {
//    		super(session, lenSegmentMsgPool);
//    	}
//    	
//    	protected Message createMessage() throws JMSException {
//    		//uses a JMS session to create a new ObjectMessage
//    		return this.session.createObjectMessage();
//    	}
//    }
//
//    public String getLogName() {
//        return logName;
//    }
//
//    public void setLogName(String logName) {
//        this.logName = logName;
//    }
//
//    public String getTopicName() {
//        return topicName;
//    }
//
//    public String getQueueName() {
//        return queueName;
//    }
//}