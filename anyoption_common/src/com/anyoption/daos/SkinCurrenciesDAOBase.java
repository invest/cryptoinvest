package com.anyoption.daos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.anyoption.common.beans.SkinCurrency;
import com.anyoption.common.daos.DAOBase;


/**
 * SkinCurrenciesD DAO Base class.
 *
 * @author Kobi
 */
public class SkinCurrenciesDAOBase extends DAOBase {

	public static ArrayList<SkinCurrency> getAllBySkin(Connection con, long skinId) throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		ArrayList<SkinCurrency> list = new ArrayList<SkinCurrency>();
		try {
			String sql =
				"SELECT " +
					"s.*, " +
					"c.display_name " +
				"FROM " +
					"skin_currencies s, " +
					"currencies c " +
				"WHERE " +
					"s.currency_id = c.id AND " +
					"s.skin_id = ? " +
					"order by s.currency_id ";

			ps = con.prepareStatement(sql);
			ps.setLong(1, skinId);
			rs = ps.executeQuery();

			while( rs.next() ) {
				list.add(getVO(rs));
			}
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		return list;
	}


	/**
	 * Fill SkinCurrency object
	 * @param rs
	 * 		ResultSet
	 * @return
	 * 		SkinCurrency object
	 * @throws SQLException
	 */
	protected static SkinCurrency getVO(ResultSet rs) throws SQLException {
		SkinCurrency vo = new SkinCurrency();
		vo.setId(rs.getLong("id"));
		vo.setSkinId(rs.getLong("skin_id"));
		vo.setCurrencyId(rs.getLong("currency_id"));
		vo.setDisplayName(rs.getString("display_name"));
		vo.setCashToPointsRate(rs.getDouble("cash_to_points_rate"));
		vo.setLimitId(rs.getLong("limit_id"));
		return vo;
	}

}