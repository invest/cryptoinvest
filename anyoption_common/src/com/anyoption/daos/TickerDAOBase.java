package com.anyoption.daos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Locale;

import com.anyoption.beans.TickerMarket;
import com.anyoption.common.daos.DAOBase;
import com.anyoption.common.managers.LanguagesManagerBase;
import com.anyoption.managers.MarketsManagerBase;
import com.anyoption.managers.SkinsManagerBase;
import com.anyoption.util.CommonUtil;

/**
 * Ticker line DAO.
 *
 * @author Tony
 */
public class TickerDAOBase extends DAOBase {
    /**
     * Load specified number of markets details for specified skin sorted by
     * opened/suspended/closed/home page priority.
     *
     * @param conn
     * @param skinId
     * @return <code>ArrayList<TickerMarket></code>
     * @throws SQLException
     */
    public static ArrayList<TickerMarket> getSkinTickerMarkets(Connection conn, long skinId) throws SQLException {
        ArrayList<TickerMarket> l = new ArrayList<TickerMarket>();
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            String sql =
                    "SELECT " +
                        "T.market_id, " +
				        "T.has_opened, " +
				        "T.is_suspended, " +
				        "T.ticker_priority, " +
                    	"O1.closing_level last_day_closing_level, " +
                    	"to_char(O1.time_est_closing, 'YYYY-MM-DD HH24:MI:SS.FF3 TZHTZM') AS last_day_closing_time, " +
                    	"O2.closing_level last_closing_level, " +
                    	"to_char(O2.time_est_closing, 'YYYY-MM-DD HH24:MI:SS.FF3 TZHTZM') AS last_closing_time, " +
                    	"T.decimal_point " +
                    "FROM " +
                        "(SELECT * " +
                        " FROM " +
	                        "(SELECT " +
	                            "DISTINCT B.market_id, " +
	                            "1 AS has_opened, " +
	                            "D.is_suspended, " +
	                            "B.ticker_priority, " +
	                            "D.decimal_point " +
	                        "FROM " +
	                            "skin_market_groups A, " +
	                            "skin_market_group_markets B, " +
	                            "opportunities C, " +
	                            "markets D " +
	                        "WHERE " +
	                            "A.skin_id = ? AND " +
	                            "B.skin_market_group_id = A.id AND " +
	                            "C.market_id = B.market_id AND " +
	                            "D.id = C.market_id AND " +
	                            "C.is_published = 1 AND " +
	                            "C.time_est_closing > current_timestamp AND " +
	                            "C.OPPORTUNITY_TYPE_ID = 1 AND " +
	                            "B.TICKER_PRIORITY != 999 " +
	                        "GROUP BY " +
	                            "B.market_id, " +
	                            "D.is_suspended, " +
	                            "B.ticker_priority, " +
	                            "D.decimal_point " +
	                        "UNION " +
	                        "SELECT " +
	                            "DISTINCT B.market_id, " +
	                            "0 AS has_opened, " +
	                            "D.is_suspended, " +
	                            "B.ticker_priority, " +
	                            "D.decimal_point " +
	                        "FROM " +
	                            "skin_market_groups A, " +
	                            "skin_market_group_markets B, " +
	                            "markets D, " +
	                            "opportunities O " +
	                        "WHERE " +
	                            "A.skin_id = ? AND " +
	                            "B.skin_market_group_id = A.id AND " +
	                            "O.market_id = B.market_id AND " +
	                            "O.OPPORTUNITY_TYPE_ID = 1 AND " +
	                            "D.id = B.market_id AND " +
	                            "B.TICKER_PRIORITY != 999 AND " +
	                            "NOT EXISTS( " +
	                                "SELECT " +
	                                    "id " +
	                                "FROM " +
	                                    "opportunities " +
	                                "WHERE " +
	                                    "market_id = D.id AND " +
	                                    "OPPORTUNITY_TYPE_ID = 1 AND " +
	                                    "time_est_closing > current_timestamp AND " +
	                                    "is_published = 1) " +
	                        "GROUP BY " +
	                            "B.market_id, " +
	                            "D.is_suspended, " +
	                            "B.ticker_priority, " +
	                            "D.decimal_point " +
	                        "ORDER BY " +
	                            "has_opened desc, " +
	                            "is_suspended, " +
	                            "ticker_priority) " +
	                    "WHERE " +
	                        "rownum <= (SELECT ticker_assets_count FROM skins WHERE id = ?)) T LEFT JOIN opportunities O1 " +
	                        																			" on GET_TKR_LAST_DAY_CLOSING_OP_ID (T.market_id) =  O1.id " +
																			                 " LEFT JOIN opportunities O2 " +
																			                 			" on GET_TKR_LAST_CLOSING_OPP_ID(T.market_id) =  O2.id ";
            pstmt = conn.prepareStatement(sql);
            pstmt.setLong(1, skinId);
            pstmt.setLong(2, skinId);
            pstmt.setLong(3, skinId);
            rs = pstmt.executeQuery();
            Locale locale = new Locale(LanguagesManagerBase.getLanguage(SkinsManagerBase.getSkin(skinId).getDefaultLanguageId()).getCode());
            TickerMarket b = null;
            while (rs.next()) {
                b = new TickerMarket();
                b.setMarketId(rs.getLong("market_id"));
                b.setMarketName(MarketsManagerBase.getMarketName(skinId, b.getMarketId()));
                b.setDecimal(rs.getInt("decimal_point"));
                b.setHasOpened(rs.getBoolean("has_opened"));
                b.setLastClosingLevel(rs.getBigDecimal("last_day_closing_level"));
                b.setLastClosingLevelTime(getTimeWithTimezone(rs.getString("last_day_closing_time")));
                b.setLastLevel(rs.getBigDecimal("last_closing_level"));
                b.setLastLevelTime(getTimeWithTimezone(rs.getString("last_closing_time")));
                // in case its new market and it was never closed....
                if (null == b.getLastLevelTime() && null == b.getLastClosingLevelTime()) {
                	continue;
                }
                l.add(b);
            }
        } finally {
            closeResultSet(rs);
            closeStatement(pstmt);
        }
        return l;
    }
}