package com.anyoption.daos;



import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.GregorianCalendar;

import org.apache.log4j.Logger;

import com.anyoption.bl_vos.TaxHistory;
import com.anyoption.common.daos.DAOBase;
import com.anyoption.util.CommonUtil;
import com.anyoption.util.ConstantsBase;

/**
 * TaxHistory DAO Class.
 *
 * @author Kobi
 */
public class TaxHistoryDAOBase extends DAOBase{

	private static final Logger logger = Logger.getLogger(TaxHistoryDAOBase.class);

	/**
	 * check if the user already paid Mid-year Tax.
	 * @param con
	 * 			the db connection to use
	 * @param userId
	 * 			the id of the user who did the investment
	 * @param period
	 * 			type of the period that user paid tax(1_year / 2_year)
	 * @return boolean
	 * 			true if the user pay mid-year tax
	 * 			false if the user doesnt pay mid-year tax
	 * @throws SQLException
	 */
	 public static boolean isPayMidYearTax(Connection con, long userId, long year) throws SQLException {

	  		PreparedStatement ps = null;
			ResultSet rs = null;

			String period = CommonUtil.getEnum(con, ConstantsBase.ENUM_TAX_PERIOD,ConstantsBase.ENUM_TAX_PERIOD_HALF_YEAR);

			try
			{
			    String sql="select * from tax_history "+
			    		   "where user_id=? and period='"+period+"' "+
			    		   "and year="+year;

			    ps = con.prepareStatement(sql);
				ps.setLong(1, userId);
				rs = ps.executeQuery();

				if ( rs.next() ) {  //found this user
					return true;
				}
			}
			finally
			{
				closeResultSet(rs);
				closeStatement(ps);
			}
			return false;
	  	}



	 /**
		 * return tax to Mid-year.
		 * @param con
		 * 			the db connection to use
		 * @param userId
		 * 			the id of the user
		 * @param period
		 * 			type of the period that user paid tax(1_year / 2_year)
		 * @return long
		 * 			value of Mid-Year tax
		 * @throws SQLException
		 */
		 public static long getMidYearTax(Connection con, long userId) throws SQLException {

		  		PreparedStatement ps = null;
				ResultSet rs = null;
				long userTax = 0;

				String periodOneStr = CommonUtil.getEnum(ConstantsBase.ENUM_TAX_PERIOD,ConstantsBase.ENUM_TAX_PERIOD_HALF_YEAR);
				GregorianCalendar gc = new GregorianCalendar();
				long year = gc.get(GregorianCalendar.YEAR);

				try {
				    String sql="select tax from tax_history "+
				    		   "where user_id=? and period='"+periodOneStr+"' "+
				    		   "and year="+year;

				    ps = con.prepareStatement(sql);
					ps.setLong(1, userId);
					rs = ps.executeQuery();

					if ( rs.next() ) {  //found this user
						userTax = rs.getLong("tax");
					}
				}
				finally {
					closeResultSet(rs);
					closeStatement(ps);
				}

				return userTax;
		  	}


		/**
		 * Get TaxHistory
		 * @param userId
		 * 		user id
		 * @param year
		 * 		year of the job
		 * @param period
		 * 		period of the year
		 *
		 * @return
		 * 		TaxHistory instance
		 * @throws SQLException
		 */
		 public static TaxHistory get(Connection con, long userId,  long year, String period) throws SQLException {

		  		PreparedStatement ps = null;
				ResultSet rs = null;
				TaxHistory t = new TaxHistory();

				try {
				    String sql = "select tax, win, lose, sum_investments, user_id " +
				    		     "from tax_history " +
				    		     "where user_id = ? and period = ? " +
				    		     "and year = ? ";

				    ps = con.prepareStatement(sql);
					ps.setLong(1, userId);
					ps.setString(2, period);
					ps.setLong(3, year);

					rs = ps.executeQuery();

					if ( rs.next() ) {
						t.setUser_id(rs.getLong("user_id"));
						t.setTax(rs.getLong("tax"));
						t.setWin(rs.getLong("win"));
						t.setLose(rs.getLong("lose"));
						t.setSumInvest(rs.getLong("sum_investments"));
					}
				}

				finally {
					closeResultSet(rs);
					closeStatement(ps);
				}

				return t;
		  	}



}

