package com.anyoption.daos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.anyoption.beans.MobileVersion;
import com.anyoption.common.daos.DAOBase;

public class MobileVersionsDAOBase extends DAOBase {

	/**
	 * Get the APK version.
	 *
	 * @param conn
	 * @param apkName
	 * @throws SQLException
	 */
    public static MobileVersion getAPK(Connection conn, String apkName) throws SQLException {
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        MobileVersion mv = new MobileVersion();
        try {
            String sql =
            		"SELECT " +
            			"mv.version, " +
            			"mv.force_update, " +
            			"mv.apk_download_link " +
            		"FROM " +
            			"mobile_versions mv " +
            		"WHERE " +
            			"mv.name LIKE ?";

            pstmt = conn.prepareStatement(sql);
            pstmt.setString(1, apkName);
            rs = pstmt.executeQuery();
            if (rs.next()) {
            	mv.setApkVersion(rs.getInt("version"));
            	mv.setForceUpdate(rs.getInt("force_update") == 1);
            	mv.setDownloadLink(rs.getString("apk_download_link"));
            }
        } finally {
            closeResultSet(rs);
            closeStatement(pstmt);
        }
        return mv;
    }
}