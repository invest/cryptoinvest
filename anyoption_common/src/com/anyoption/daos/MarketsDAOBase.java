package com.anyoption.daos;

import java.io.UnsupportedEncodingException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;

import com.anyoption.common.beans.Investment;
import com.anyoption.common.beans.Market;
import com.anyoption.common.beans.Opportunity;
import com.anyoption.common.beans.OpportunityType;
import com.anyoption.common.beans.TreeItem;
import com.anyoption.common.beans.base.MarketGroup;
import com.anyoption.util.ConstantsBase;

public class MarketsDAOBase extends com.anyoption.common.daos.MarketsDAOBase {

//    /**
//     * Get markets for skin by opportunity type and schedule
//     * @param conn
//     * @param skinId
//     * @param opportunityTypeId
//     * @return
//     * @throws SQLException
//     */
//    public static Hashtable<Long, Market> getMarketsForSkinByOppType(Connection conn, long skinId, long opportunityTypeId, boolean onlyHorly) throws SQLException {
//        PreparedStatement pstmt = null;
//        ResultSet rs = null;
//        Hashtable<Long, Market> ht = new Hashtable<Long, Market>();
//        int index = 1;
//        try {
//        	String sql =" SELECT " +
//        				"	m.id, " +
//        				"   m.display_name, " +
//        				"   m.market_group_id, " +
//        				"   mg.display_name as market_group_name " +
//        				" FROM " +
//        				"   markets m, " +
//        				"   market_groups mg, " +
//        				"   skin_market_groups smg, " +
//        				"   skin_market_group_markets smgm " +
//        				" WHERE " +
//        				"   mg.id = m.market_group_id " +
//        				"   AND smgm.skin_market_group_id = smg.id " +
//        				"   AND m.id = smgm.market_id " +
//        				"   AND smg.skin_id = ? " +
//        				"   AND EXISTS ( SELECT" +
//        				"                  1 " +
//        				"                FROM " +
//        				"                  opportunity_templates ot1 " +
//        				"                WHERE " +
//        				"	                ot1.market_id = m.id " +
//        				"                   AND ot1.is_active = 1 ";
//        				if (onlyHorly) {
//        		  sql +=" 		            AND ot1.scheduled = ? ";
//        				}
//        		  sql +="       	        AND ot1.opportunity_type_id = ?) ";
//            pstmt = conn.prepareStatement(sql);
//            pstmt.setLong(index++, skinId);
//            if (onlyHorly) {
//            	pstmt.setInt(index++, Investment.SCHEDULED_HOURLY);
//            }
//            pstmt.setLong(index++, opportunityTypeId);
//            rs = pstmt.executeQuery();
//            Market m = null;
//            while (rs.next()) {
//                m = new Market();
//                m.setId(rs.getLong("id"));
//                m.setDisplayName(rs.getString("display_name"));
//                m.setGroupId(rs.getLong("market_group_id"));
//                m.setGroupName(rs.getString("market_group_name"));
//                ht.put(rs.getLong("id"), m);
//            }
//        } finally {
//            closeResultSet(rs);
//            closeStatement(pstmt);
//        }
//        return ht;
//    }

    public static ArrayList<Market> getChartsUpdaterMarkets(Connection conn) throws SQLException {
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        ArrayList<Market> l = new ArrayList<Market>();
        try {
            String sql =
                " SELECT " +
                    " m.id, " +
                    " m.display_name, " +
                    " m.decimal_point, " +
                    " CASE WHEN lhot.time_first_invest is not null THEN lhot.time_first_invest ELSE lhot.time_est_closing END last_hour_opening_time, " +
                    " CASE WHEN count (open_opp.id) > 0 THEN 1 ELSE 0 END opened, " +
                    " CASE WHEN count (opp_temp.id) > 0 THEN 1 ELSE 0 END hourly " +
                " FROM " +
                    " markets m " +
                        " left join opportunities open_opp on open_opp.market_id = m.id  " +
                                                            " and open_opp.is_published = 1 " +
                                                            " and open_opp.time_est_closing > trunc(current_date - 1) " +
                                                            " and open_opp.opportunity_type_id = 1 " +
                        " left join opportunity_templates opp_temp on opp_temp.market_id = m.id " +
                                                                    " and opp_temp.scheduled = 1 " +
                                                                    " and opp_temp.is_active = 1 " +
                        " left join (select " +
                                        " m.id , " +
                                        " to_char(min(curr_opp.time_first_invest), 'YYYY-MM-DD HH24:MI:SS.FF3 TZHTZM')  time_first_invest, " +
                                        " to_char(max(close_opp.time_est_closing), 'YYYY-MM-DD HH24:MI:SS.FF3 TZHTZM') time_est_closing " +
                                    " from " +
                                        " markets m " +
                                            " left join opportunities curr_opp on curr_opp.market_id = m.id " +
                                                                                " and curr_opp.scheduled = 1 " +
                                                                                " and curr_opp.is_published = 1 " +
                                            " left join opportunities close_opp on m.id = close_opp.market_id " +
                                                                                " and close_opp.time_act_closing IS NOT NULL " +
                                                                                " and close_opp.opportunity_type_id = 1 " +
                                    " group by m.id) lhot on lhot.id = m.id " +
                " GROUP BY " +
                    " m.id, " +
                    " m.display_name, " +
                    " m.decimal_point, " +
                    " lhot.time_first_invest, " +
                    " lhot.time_est_closing ";

            pstmt = conn.prepareStatement(sql);
            rs = pstmt.executeQuery();
            Market m = null;
            while(rs.next()) {
                m = getMarketVOShort(rs);
                m.setLastHourClosingTime(getTimeWithTimezone(rs.getString("last_hour_opening_time")));
                m.setOpened(rs.getBoolean("opened"));
                m.setHaveHourly(rs.getBoolean("hourly"));
                l.add(m);
            }
        } finally {
            closeResultSet(rs);
            closeStatement(pstmt);
        }
        return l;
    }

    protected static Market getMarketVOShort(ResultSet rs) throws SQLException {
        Market vo = new Market();
        vo.setId(rs.getLong("id"));
        vo.setDecimalPoint(new Long(rs.getLong("decimal_Point")));
        vo.setDisplayNameKey(rs.getString("display_name"));
        return vo;
    }

//    /**
//     * Get markets list for last levels dropdown list
//     * @param con
//     * @param skinId
//     * @return
//     * @throws SQLException
//     * @throws UnsupportedEncodingException
//     */
//	public static ArrayList<TreeItem> getClosedMarketsAnyoption(Connection con, long skinId) throws SQLException {
//		PreparedStatement ps = null;
//		ResultSet rs = null;
//		ArrayList<TreeItem> list = new ArrayList<TreeItem>();
//		try {
//			String sql =
//				"SELECT " +
//					"DISTINCT(smg.MARKET_GROUP_ID||','|| smgm.market_id) as display_id," +
//					"m.id as market_id," +
//					"smg.MARKET_GROUP_ID, " +
//					"m.DISPLAY_NAME as market_name, " +
//					"mg.DISPLAY_NAME as group_name," +
//					"smgm.group_priority ," +
//					"smg.SKIN_ID," +
//					"mdg2.sort_priority as sub_group_sort_priority, " +
//					"(SELECT mdg.display_name from market_display_groups mdg where smgm.skin_display_group_id = mdg.id (+)) as subGroup_display_name " +
//				 "FROM " +
//				 	"SKIN_MARKET_GROUP_MARKETS smgm , " +
//				 	"markets m , " +
//				 	"market_groups mg , " +
//				 	"skin_market_groups smg," +
//				 	"market_display_groups mdg2, " +
//				 	"opportunities o, " +
//				 	"opportunity_templates ot " +
//				 "WHERE " +
//				 	"smgm.market_id = m.id AND " +
//				 	"smgm.skin_market_group_id = smg.ID AND " +
//				 	"smg.MARKET_GROUP_ID = mg.ID AND " +
//				 	"smg.SKIN_ID = ? AND " +
//				 	"o.market_id = m.id AND " +
//				 	"o.time_act_closing is not null AND " +
//				 	"mdg2.id(+) = smgm.skin_display_group_id AND "+
//				 	"ot.scheduled = 2 AND " +
//				 	"ot.is_active = 1 AND " +
//				 	"ot.is_full_day = 1 AND " +
//				 	"m.id = ot.market_id AND " +
//				 	"m.id NOT IN (13,25,24,23,61,12) " +
//				 "ORDER BY " +
//				 	"smg.MARKET_GROUP_ID, " +
//				 	"sub_group_sort_priority, " +
//				 	"smgm.group_priority,display_id ";
//
//			ps = con.prepareStatement(sql);
//			ps.setLong(1,skinId);
//
//			rs = ps.executeQuery();
//
//			String group = null;
//	        String geoGroup = null;
//
//			while(rs.next()) {
//				TreeItem temp = new TreeItem();
//				temp.setGroupName(rs.getString("group_name"));
//				temp.setMarketGroupId(Integer.parseInt(rs.getString("market_group_id")));
//				temp.setMarketName(rs.getString("market_name"));
//				temp.setMarketSubGroupDisplayName(rs.getString("subgroup_display_name"));
//				temp.setMarketId(Integer.parseInt(rs.getString("market_id")));
//	            if (null == group || !group.equals(temp.getGroupName())) {
//	                group = temp.getGroupName();
//	                temp.setPrintGroup(true);
//	            } else {
//	                temp.setPrintGroup(false);
//	            }
//	            if (temp.getMarketGroupId() != ConstantsBase.MARKET_GROUP_CURRENCIES && temp.getMarketGroupId() != ConstantsBase.MARKET_GROUP_COMMODITIES
//	            		&& (null == geoGroup || !geoGroup.equals(temp.getMarketSubGroupDisplayName()))) {
//	                geoGroup = temp.getMarketSubGroupDisplayName();
//	                temp.setPrintGeoGroup(true);
//	            } else {
//	                temp.setPrintGeoGroup(false);
//	            }
//				list.add(temp);
//			}
//		} finally {
//			closeResultSet(rs);
//			closeStatement(ps);
//		}
//		return list;
//	}


//    /**
//     * Get markets list for marketsGroup dropdown list (homePage)
//     * @param con
//     * @param skinId
//     * @return
//     * @throws SQLException
//     * @throws UnsupportedEncodingException
//     */
//	public static ArrayList<TreeItem> getTreeItems(Connection con, long skinId) throws SQLException {
//		PreparedStatement ps = null;
//		ResultSet rs = null;
//		ArrayList<TreeItem> list = new ArrayList<TreeItem>();
//		try {
//			String sql =
//				"SELECT " +
//					"smgm.market_id, " +
//					"m.DISPLAY_NAME as market_name, " +
//					"smg.MARKET_GROUP_ID, " +
//					"mg.DISPLAY_NAME as group_name, " +
//					"smgm.group_priority, " +
//					"smg.SKIN_ID, " +
//					"mdg2.sort_priority as sub_group_sort_priority, " +
//					"(SELECT mdg.display_name from market_display_groups  mdg where smgm.skin_display_group_id = mdg.id (+)) as subGroup_display_name " +
//				"FROM " +
//					"SKIN_MARKET_GROUP_MARKETS smgm, " +
//					"markets m, " +
//					"market_groups mg, " +
//					"skin_market_groups smg, " +
//					"opportunity_templates ot, " +
//					"market_display_groups mdg2, " +
//					"market_name_skin mns " +
//				"WHERE " +
//					"smgm.market_id = m.id AND " +
//					"smgm.skin_market_group_id = smg.ID AND " +
//					"smg.MARKET_GROUP_ID = mg.ID AND " +
//					"smg.SKIN_ID = ? AND " +
//					" ot.scheduled=2 AND " +
//					"ot.is_active = 1 AND " +
//					"ot.is_full_day = 1 AND " +
//					"m.id = ot.market_id AND " +
//					"mns.market_id = m.id AND " +
//					"mns.skin_id = smg.skin_id AND " +
//					"mdg2.id(+) = smgm.skin_display_group_id " +
//				"ORDER BY " +
//					"smg.MARKET_GROUP_ID, " +
//					"sub_group_sort_priority," +
//					"upper(mns.name)";
//
//			ps = con.prepareStatement(sql);
//			ps.setLong(1, skinId);
//
//			rs = ps.executeQuery();
//			String group = null;
//			String geoGroup = null;
//			while (rs.next()) {
//				TreeItem temp = new TreeItem();
//				temp.setGroupName(rs.getString("group_name"));
//				temp.setMarketGroupId(Integer.parseInt(rs
//						.getString("market_group_id")));
//				temp.setMarketName(rs.getString("market_name"));
//				temp.setMarketSubGroupDisplayName(rs
//						.getString("subgroup_display_name"));
//				temp.setMarketId(Integer.parseInt(rs.getString("market_id")));
//				if (null == group || !group.equals(temp.getGroupName())) {
//					group = temp.getGroupName();
//					temp.setPrintGroup(true);
//				} else {
//					temp.setPrintGroup(false);
//				}
//				if (temp.getMarketGroupId() != 4
//						&& temp.getMarketGroupId() != 5
//						&& (null == geoGroup || !geoGroup.equals(temp
//								.getMarketSubGroupDisplayName()))) {
//					geoGroup = temp.getMarketSubGroupDisplayName();
//					temp.setPrintGeoGroup(true);
//				} else {
//					temp.setPrintGeoGroup(false);
//				}
//				list.add(temp);
//			}
//		} finally {
//			closeResultSet(rs);
//			closeStatement(ps);
//		}
//		return list;
//	}

//	public static Hashtable<Long, MarketGroup> getSkinGroupsMarkets(Connection conn, long skinId) throws SQLException {
//		PreparedStatement ps = null;
//		ResultSet rs = null;
//		Hashtable<Long, MarketGroup> groups = new Hashtable<Long, MarketGroup>();
//		try {
//			String sql =
//				"SELECT " +
//			        "smgm.market_id, " +
//			        "m.display_name as market_name, " +
//			        "m.option_plus_market_id, " +
//			        "smg.market_group_id, " +
//			        "mg.display_name as group_name, " +
//			        "m.exchange_id, " +
//				    "m.decimal_point, " +
//				    "m.decimal_point_subtract_digits, " +
//				    "m.type_id m_type_id " +
//			    "FROM " +
//			        "skin_market_group_markets smgm, " +
//			        "markets m, " +
//			        "market_groups mg, " +
//			        "skin_market_groups smg, " +
//			        "market_name_skin mns " +
//			    "WHERE " +
//			        "smgm.market_id = m.id AND " +
//			        "smgm.skin_market_group_id = smg.ID AND " +
//			        "smg.MARKET_GROUP_ID = mg.ID AND " +
//			        "smg.SKIN_ID = ? AND " +
//			        "EXISTS (SELECT " +
//			        			"1 " +
//			        		"FROM " +
//			        			"opportunity_templates ot " +
//			        		"WHERE " +
//			        			"m.id = ot.market_id AND " +
//			        			"ot.is_active = 1 AND " +
//			        			"ot.scheduled < 5 AND " +
//			        			"ot.opportunity_type_id NOT IN (" + Opportunity.TYPE_BINARY_0_100_ABOVE + ", " + Opportunity.TYPE_BINARY_0_100_BELOW + ")) AND " +
//			        "mns.market_id = m.id AND " +
//			        "mns.skin_id = smg.skin_id " +
//			    "ORDER BY " +
//			        "smg.market_group_id, " +
//			        "upper(mns.name)";
//
//			ps = conn.prepareStatement(sql);
//			ps.setLong(1, skinId);
//			rs = ps.executeQuery();
//
//			while (rs.next()) {
//				com.anyoption.common.beans.base.Market m = new com.anyoption.common.beans.base.Market();
//				m.setDisplayName(rs.getString("market_name"));
//				m.setId(rs.getLong("market_id"));
//				m.setExchangeId(rs.getLong("exchange_id"));
//				long groupId = rs.getLong("market_group_id");
//				m.setDecimalPoint(rs.getLong("decimal_point"));
//				m.setDecimalPointSubtractDigits(rs.getInt("decimal_point_subtract_digits"));
//				m.setGroupId(groupId);
//				long optionPlusMarketId = rs.getLong("option_plus_market_id");
//				int marketType = rs.getInt("m_type_id");
//				m.setProductTypeId(marketType);
//				if (optionPlusMarketId > 0) { //we r in option+ market
//					m.setOptionPlusMarket(true);
//					groupId = ConstantsBase.MARKET_GROUP_OPTION_PLUS_ID;
//				}
//				if (marketType == OpportunityType.PRODUCT_TYPE_DYNAMICS ) {
//					groupId = ConstantsBase.MARKET_GROUP_DYNAMICS_ID;
//				}
//				if (null == groups.get(groupId)) {
//					MarketGroup mg = new MarketGroup();
//					mg.setId(groupId);
//					if (ConstantsBase.MARKET_GROUP_OPTION_PLUS_ID == groupId) { //if option +
//						mg.setName(ConstantsBase.MARKET_GROUP_OPTION_PLUS_DISPLAY_NAME);
//					} else if (ConstantsBase.MARKET_GROUP_DYNAMICS_ID == groupId) { 
//						mg.setName(ConstantsBase.MARKET_GROUP_DYNAMICS_NAME);
//					}else {
//						mg.setName(rs.getString("group_name"));
//					}
//					mg.setMarkets(new ArrayList<com.anyoption.common.beans.base.Market>());
//					groups.put(groupId, mg);
//				}
//				groups.get(groupId).getMarkets().add(m);
//			}
//		} finally {
//			closeResultSet(rs);
//			closeStatement(ps);
//		}
//		return groups;
//	}

	/**
	 * get market list sorted
	 * @param conn db connection
	 * @param skinId skin id
	 * @param marketTypeId market type id
	 * @return all markets sorted by market name
	 * @throws SQLException
	 */
	public static ArrayList<com.anyoption.common.beans.base.Market> getSkinMarketsListSorted(Connection conn, long skinId, long marketTypeId) throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		ArrayList<com.anyoption.common.beans.base.Market> markets = new ArrayList<com.anyoption.common.beans.base.Market>();
		try {
			String sql =
				"SELECT " +
			        "smgm.market_id, " +
			        "m.display_name as market_name, " +
			        "m.option_plus_market_id, " +
			        "m.bubbles_pricing_level " +
			    "FROM  " +
			        "skin_market_group_markets smgm, " +
			        "markets m, " +
			        "skin_market_groups smg, " +
			        "market_name_skin mns " +
			    "WHERE " +
			        "smgm.market_id = m.id AND " +
			        "smgm.skin_market_group_id = smg.ID AND " +
			        "smg.SKIN_ID = ? AND " +
			        "EXISTS (SELECT " +
			        			"1 " +
			        		"FROM " +
			        			"opportunity_templates ot " +
			        		"WHERE " +
			        			"m.id = ot.market_id AND " +
			        			"ot.is_active = 1) AND " +
			        "mns.market_id = m.id AND " +
			        "mns.skin_id = smg.skin_id ";
		   if (marketTypeId > 0) {
		        sql += "AND m.type_id = ? ";
		   }
		sql += "ORDER BY " +
			        "upper(mns.name) ";
			ps = conn.prepareStatement(sql);
			ps.setLong(1, skinId);
			if (marketTypeId > 0) {
				ps.setLong(2, marketTypeId);
			}
			rs = ps.executeQuery();

			while (rs.next()) {
				com.anyoption.common.beans.base.Market m = new com.anyoption.common.beans.base.Market();
				m.setDisplayName(rs.getString("market_name"));
				m.setId(rs.getLong("market_id"));
				m.setBubblesPricingLevel(rs.getInt("bubbles_pricing_level"));
				long optionPlusMarketId = rs.getLong("option_plus_market_id");
				if (optionPlusMarketId > 0) { //we r in option+ market
					m.setOptionPlusMarket(true);
				}
				markets.add(m);
			}
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		return markets;
	}

    public static HashMap<Long, String> getMarketAssetIndexExpLevelCalculation(Connection con ) throws SQLException {
        PreparedStatement ps = null;
        ResultSet rs = null;
        HashMap<Long, String> hm = new HashMap<Long, String>();
        try {
            String sql = " SELECT " +
                            " ID, ASSET_INDEX_EXP_LEVLEL_CALC " +
                       " FROM " +
                           " MARKETS " +
                       " WHERE " +
                           " ASSET_INDEX_EXP_LEVLEL_CALC is not null";
            ps = con.prepareStatement(sql);
            rs = ps.executeQuery();
            while (rs.next()) {
                hm.put(rs.getLong("ID"), rs.getString("ASSET_INDEX_EXP_LEVLEL_CALC"));
            }
        } finally {
            closeResultSet(rs);
            closeStatement(ps);
        }
        return hm;
    }

}