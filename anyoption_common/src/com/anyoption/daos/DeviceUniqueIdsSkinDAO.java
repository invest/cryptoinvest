package com.anyoption.daos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Types;

import com.anyoption.common.daos.DeviceUniqueIdsSkinDAOBase;
import com.anyoption.common.util.CommonUtil;

public class DeviceUniqueIdsSkinDAO extends DeviceUniqueIdsSkinDAOBase {
    public static void insert(Connection conn, String deviceId, long skinId, String userAgent, String ip, Long countryId, Long combId, String appVer, String dynamicParam, long affiliateKey,
    		String appsflyerId, int osTypeId, String deviceFamily, String idfa, String advertisingId) throws SQLException {
        PreparedStatement pstmt = null;
        try {
            String sql =
                    "INSERT INTO " +
                        "device_unique_ids_skin(id, device_unique_id, skin_id, user_agent, time_created, time_modified, ip, country_id, combination_id, app_version, dynamic_param, affiliate_key, appsflyer_id, os_type_id, device_family, idfa, advertising_id) " +
                    "VALUES(seq_device_unique_ids_skin.nextval, ?, ?, ?, sysdate, sysdate, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) ";
            pstmt = conn.prepareStatement(sql);
            pstmt.setString(1, deviceId);
            pstmt.setLong(2, skinId);
            if (null != userAgent) {
                pstmt.setString(3, userAgent);
            } else {
                pstmt.setNull(3, Types.VARCHAR);
            }
            if (null != ip) {
                pstmt.setString(4, ip);
            } else {
                pstmt.setNull(4, Types.VARCHAR);
            }
            if (null != countryId) {
                pstmt.setLong(5, countryId);
            } else {
                pstmt.setNull(5, Types.NUMERIC);
            }
            if (null != combId) {
                pstmt.setLong(6, combId);
            } else {
                pstmt.setNull(6, Types.NUMERIC);
            }
            pstmt.setString(7, appVer);
            if (null != dynamicParam) {
                pstmt.setString(8, dynamicParam);
            } else {
                pstmt.setNull(8, Types.VARCHAR);
            }
            if (0 != affiliateKey) {
                pstmt.setLong(9, affiliateKey);
            } else {
                pstmt.setNull(9, Types.NUMERIC);
            }
            
            if (null != appsflyerId) {
                pstmt.setString(10, appsflyerId);
            } else {
                pstmt.setNull(10, Types.VARCHAR);
            }
            
            if (0 != osTypeId) {
                pstmt.setLong(11, osTypeId);
            } else {
                pstmt.setNull(11, Types.NUMERIC);
            }
            
            if (null != deviceFamily) {
                pstmt.setString(12, deviceFamily);
            } else {
                pstmt.setNull(12, Types.VARCHAR);
            }
            
            if (null != idfa) {
                pstmt.setString(13, idfa);
            } else {
                pstmt.setNull(13, Types.VARCHAR);
            }
            
            if (null != advertisingId) {
                pstmt.setString(14, advertisingId);
            } else {
                pstmt.setNull(14, Types.VARCHAR);
            }
            pstmt.executeUpdate();
        } finally {
            closeStatement(pstmt);
        }
    }

    public static void updateSkindId(Connection conn, String deviceId, long skinId) throws SQLException {
        PreparedStatement pstmt = null;
        try {
            String sql =
                    "UPDATE " +
                        "device_unique_ids_skin " +
                    "SET " +
                        "skin_id = ?, " +
                        "time_modified = sysdate " +
                    "WHERE " +
                        "device_unique_id = ? ";
            pstmt = conn.prepareStatement(sql);
            pstmt.setLong(1, skinId);
            pstmt.setString(2, deviceId);
            pstmt.executeUpdate();
        } finally {
            closeStatement(pstmt);
        }
    }

    public static void updateCombinationId(Connection conn, String deviceId, long combId) throws SQLException {
        PreparedStatement pstmt = null;
        try {
            String sql =
                    "UPDATE " +
                        "device_unique_ids_skin " +
                    "SET " +
                        "combination_id = ?, " +
                        "time_modified = sysdate " +
                    "WHERE " +
                        "device_unique_id = ? ";
            pstmt = conn.prepareStatement(sql);
            pstmt.setLong(1, combId);
            pstmt.setString(2, deviceId);
            pstmt.executeUpdate();
        } finally {
            closeStatement(pstmt);
        }
    }

    public static void updateAppVer(Connection conn, String deviceId, String appVer, String idfa, String advertisingId) throws SQLException {
        PreparedStatement pstmt = null;
        int index = 1;
        try {
            String sql =
                    "UPDATE " +
                        "device_unique_ids_skin " +
                    "SET " +
                        "time_modified = sysdate ";
            if (!CommonUtil.isParameterEmptyOrNull(appVer)) {
            	sql += " ,app_version = ? ";
            }
            if (!CommonUtil.isParameterEmptyOrNull(idfa)) {
            	sql += " ,idfa = ? ";
            }
            if (!CommonUtil.isParameterEmptyOrNull(advertisingId)) {
            	sql += " ,advertising_id = ? ";
            }            
            sql += " WHERE " +
                        " device_unique_id = ? ";
            pstmt = conn.prepareStatement(sql);
            if (!CommonUtil.isParameterEmptyOrNull(appVer)) {
            	pstmt.setString(index++, appVer);
            }
            if (!CommonUtil.isParameterEmptyOrNull(idfa)) {
            	pstmt.setString(index++, idfa);
            }
            if (!CommonUtil.isParameterEmptyOrNull(advertisingId)) {
            	pstmt.setString(index++, advertisingId);
            }
            pstmt.setString(index++, deviceId);
            pstmt.executeUpdate();
        } finally {
            closeStatement(pstmt);
        }
    }

    public static void updateC2dmRegistrationId(Connection conn, String deviceId, String c2dmRegistrationId) throws SQLException {
        PreparedStatement pstmt = null;
        try {
            String sql =
                    "UPDATE " +
                        "device_unique_ids_skin " +
                    "SET " +
                        "c2dm_registration_id = ?, " +
                        "time_modified = sysdate " +
                    "WHERE " +
                        "device_unique_id = ? ";
            pstmt = conn.prepareStatement(sql);
            pstmt.setString(1, c2dmRegistrationId);
            pstmt.setString(2, deviceId);
            pstmt.executeUpdate();
        } finally {
            closeStatement(pstmt);
        }
    }
}