package com.anyoption.daos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.anyoption.beans.TierUserHistory;
import com.anyoption.common.beans.TierUser;
import com.anyoption.common.daos.DAOBase;
import com.anyoption.managers.TiersManagerBase;

public class TiersDAOBase extends DAOBase {
    /**
     * Update user points after invest
     * @param con db connection
     * @param id tierUser id
     * @param writerId writer that perform this action
     * @param points loyalty updated points of the user after invest
     * @throws SQLException
     */
    public static void updateUserTier(Connection con, long id, long writerId, long points) throws SQLException {
        PreparedStatement ps = null;
        try {
            String sql =
                "UPDATE " +
                  "tier_users " +
                "SET " +
                  "writer_id = ?, " +
                  "points = ? " +
                "WHERE " +
                  "id = ? ";
            ps = con.prepareStatement(sql);
            ps.setLong(1, writerId);
            ps.setLong(2, points);
            ps.setLong(3, id);
            ps.executeUpdate();
        } finally {
            closeStatement(ps);
        }
    }

    /**
     * Insert into Tier users history table
     * @param con db connection
     * @param tH TierUserHistory instance for the insert
     * @throws SQLException
     */
    public static void insertIntoTierHistory(Connection con, TierUserHistory tH) throws SQLException {
        PreparedStatement ps = null;
        try {
            String sql =
                      "INSERT INTO " +
                          "tier_users_history(id, tier_id, user_id, tier_his_action, time_created,writer_id, points, key_value, action_points, table_name, utc_offset) " +
                       "VALUES " +
                          "(SEQ_TIER_USERS_HISTORY.NEXTVAL,?,?,?,sysdate,?,?,?,?,?,?)";
            ps = con.prepareStatement(sql);
            ps.setLong(1, tH.getTierId());
            ps.setLong(2, tH.getUserId());
            ps.setLong(3, tH.getTierHistoryActionId());
            ps.setLong(4, tH.getWriterId());
            ps.setLong(5, tH.getPoints());
            ps.setLong(6, tH.getKeyValue());
            ps.setLong(7, tH.getActionPoints());
            ps.setString(8, tH.getTableName());
            ps.setString(9, tH.getUtcOffset());
            ps.executeUpdate();
        } finally {
            closeStatement(ps);
        }
    }

    /**
     *  Get invest points by investment id
     * @param con
     * @param investmentId the investment that the user maybe got points
     * @return
     * @throws SQLException
     */
    public static long getInvetPointsByInvestmentId(Connection con, long investmentId) throws SQLException {
        PreparedStatement ps = null;
        ResultSet rs = null;
        long points = 0;
        try {
            String sql =
                "SELECT " +
                    "tuh.action_points " +
                "FROM " +
                    "tier_users_history tuh, " +
                    "investments i " +
                "WHERE " +
                    "i.id = ? AND " +
                    "(i.insurance_flag <> 2 OR " +
                    "i.insurance_amount_ru = 0 AND " +
                    "i.insurance_flag = 2) AND " +
                    "i.user_id = tuh.user_id AND " +
                    "tuh.key_value = i.id AND " +
                    "tuh.tier_his_action = ? ";
            ps = con.prepareStatement(sql);
            ps.setLong(1, investmentId);
            ps.setLong(2, TiersManagerBase.TIER_ACTION_OBTAIN_POINTS);
            rs = ps.executeQuery();
            if (rs.next()) {
                points = rs.getLong("action_points");
            }
        } finally {
            closeResultSet(rs);
            closeStatement(ps);
        }
        return points;
    }

    /**
     * Get tierUSer bu userId
     * @param con db connection
     * @param userId tier details of this user
     * @return
     * @throws SQLException
     */
    public static TierUser getTierUserByUserId(Connection con, long userId) throws SQLException {
        PreparedStatement ps = null;
        ResultSet rs = null;
        TierUser vo = null;
        try {
              String sql =
                  "SELECT " +
                      "tu.*, " +
                      "t.description " +
                  "FROM " +
                      "tier_users tu, " +
                      "tiers t, " +
                      "users u " +
                  "WHERE " +
                      "tu.user_id = ? AND " +
                      "tu.tier_id = t.id AND " +
                      "u.tier_user_id = tu.id ";
              ps = con.prepareStatement(sql);
              ps.setLong(1, userId);
              rs = ps.executeQuery();
              if (rs.next()) {
                  vo = getTierUserVO(rs);
                  vo.setTierName(rs.getString("description"));
              }
          } finally {
              closeResultSet(rs);
              closeStatement(ps);
          }
          return vo;
    }

    /**
     * get TierUserVO
     * @param rs
     * @return
     * @throws SQLException
     */
    protected static TierUser getTierUserVO(ResultSet rs) throws SQLException {
        TierUser tierUser = new TierUser();
        tierUser.setId(rs.getLong("id"));
        tierUser.setUserId(rs.getLong("user_id"));
        tierUser.setTierId(rs.getLong("tier_id"));
        tierUser.setPoints(rs.getLong("points"));
        tierUser.setWriterId(rs.getLong("writer_id"));
        tierUser.setTimeCreated(convertToDate(rs.getTimestamp("time_created")));
        tierUser.setQualificationTime(convertToDate(rs.getTimestamp("qualification_time")));
        return tierUser;
    }
}