package com.anyoption.daos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.anyoption.common.beans.File;

public class FilesDAOBase extends com.anyoption.common.daos.FilesDAOBase {
	public static void updateFileRecords(Connection con, long fileId, long issueActionId) throws SQLException {
		 PreparedStatement ps = null;
         ResultSet rs = null;
         try {
               String sql ="UPDATE " +
               		"Files " +
               		"set issue_action_id = ? " +
               		"WHERE " +
               		"id= ?";
               ps = con.prepareStatement(sql);
               ps.setLong(1, issueActionId);
               ps.setLong(2, fileId);
               ps.executeUpdate();
         } finally {
               closeStatement(ps);
               closeResultSet(rs);
         }
	}
	
	public static ArrayList<File> getUserFileIdsByIdNoIssue(Connection con, long userId) throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		ArrayList<File> uf = new ArrayList<File>();
		try {

			String sql = 
						" SELECT " + 
									" id, file_status_id, file_type_id " + 
						" FROM " + 
									" files " +
						" WHERE " +
									" user_id = ? " +
						" AND " +
									" issue_action_id is null";

			ps = con.prepareStatement(sql);
			ps.setLong(1, userId);
			rs = ps.executeQuery();

			if (rs.next()) {
				uf.add(new File(rs.getLong("id"), rs.getLong("file_status_id"), rs.getLong("file_type_id")));
			}
		}

		finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		return uf;
	}	
	 
		public static void updateFileRecords(Connection con, File file, long issueActionId) throws SQLException {
			PreparedStatement ps = null;
			ResultSet rs = null;
			try {
				String sql = "UPDATE " + "Files " + "set issue_action_id = ? "
						+ "WHERE " + "id= ?";

				ps = con.prepareStatement(sql);
				ps.setLong(1, issueActionId);
				ps.setLong(2, file.getId());
				ps.executeUpdate();
			} finally {
				closeStatement(ps);
				closeResultSet(rs);
			}
		}
}