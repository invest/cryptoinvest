package com.anyoption.daos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.anyoption.common.daos.DAOBase;

public class CcBlackListDAOBase extends DAOBase {
    public static boolean isCardBlacklisted(Connection conn, long ccn, boolean deposit) throws SQLException {
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            String sql =
                "SELECT " +
                    "id " +
                "FROM " +
                    "cc_black_List " +
                "WHERE " +
                    "cc_number = ? AND " +
                    "is_active = 1 AND " +
                    "is_deposit = ?";
            pstmt = conn.prepareStatement(sql);
            pstmt.setLong(1, ccn);
            pstmt.setBoolean(2, deposit);
            rs = pstmt.executeQuery();
            if (rs.next()) {
                return true;
            }
        } finally {
            closeResultSet(rs);
            closeStatement(pstmt);
        }
        return false;
    }
    
    
    public static boolean isCardBlacklistedWithdraw(Connection con, long ccn) throws SQLException {
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            String sql =
                "SELECT " +
                    "id " +
                "FROM " +
                    "cc_black_List " +
                "WHERE " +
                    "cc_number = ? AND " +
                    "is_active = 1 ";
            pstmt = con.prepareStatement(sql);
            pstmt.setLong(1, ccn);

            rs = pstmt.executeQuery();
            if (rs.next()) {
                return true;
            }
        } finally {
            closeResultSet(rs);
            closeStatement(pstmt);
        }
        return false;
    }
    
    
}