package com.anyoption.daos;

import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.SQLIntegrityConstraintViolationException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.bouncycastle.crypto.CryptoException;

import com.anyoption.beans.User;
import com.anyoption.common.beans.Skin;
import com.anyoption.common.beans.Transaction;
import com.anyoption.common.beans.base.Language;
import com.anyoption.common.beans.base.MailBoxTemplate;
import com.anyoption.common.beans.base.MailBoxUser;
import com.anyoption.common.beans.base.UserAnycapital;
import com.anyoption.common.beans.base.UserAnycapital.Gender;
import com.anyoption.common.beans.base.UserAnycapitalExtraFields;
import com.anyoption.common.beans.base.UserRegulationBase;
import com.anyoption.common.daos.TransactionsDAOBase;
import com.anyoption.common.managers.CountryManagerBase;
import com.anyoption.common.managers.CurrenciesManagerBase;
import com.anyoption.common.managers.LanguagesManagerBase;
import com.anyoption.common.util.AESUtil;
import com.anyoption.common.util.ConstantsBase;
import com.anyoption.managers.MailBoxTemplateManagerBase;
import com.anyoption.managers.SkinsManagerBase;
import com.anyoption.managers.TransactionsManagerBase;
import com.anyoption.util.CommonUtil;

public class UsersDAOBase extends com.anyoption.common.daos.UsersDAOBase {
    public static final Logger log = Logger.getLogger(UsersDAOBase.class);

    public static User getByUserName(Connection conn, String userName, User user) throws SQLException {
        if (null == userName || userName.trim().equals("")) {
            return null;
        }

        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
        	String sql =
                    " SELECT " +
                        " u.*, " +
                        " s.name AS skin_name, " +
                        " decode(nvl(ur.id,0) ,0 ,0 ,1 ) AS is_regulation, " +
                        " pda.amount AS predefined_deposit_amount, " +
                        " uad.is_need_change_password, " +
                        " uad.has_dynamics, " +
                        " uad.has_bubbles, "
                        + "uad.show_cancel_investment " +
                    " FROM " +
                        " users u, " +
                        " users_regulation ur, " +
                        " skins s, " +
                        " predefined_deposit_amount pda," +
                        " users_active_data uad" +
                    " WHERE " +
                        " u.skin_id = s.id " +
                        " AND u.id = ur.user_id(+) " +
                        " AND u.id = uad.user_id " +
                        " AND u.currency_id = pda.currency_id(+) " +
                        " AND upper(user_name) = upper(?) ";
            pstmt = conn.prepareStatement(sql);
            pstmt.setString(1, userName.toUpperCase());
            rs = pstmt.executeQuery();
            if (rs.next()) {
                if (null == user) {
                    user = new User();
                }
                getUserVO(rs, user);
                
				if (rs.getLong("skin_id") != Skin.SKIN_ETRADER) {
					user.setPredefinedDepositAmount(rs.getString("predefined_deposit_amount"));
				}
				Hashtable<String, Boolean> productViewFlags = new Hashtable<String, Boolean>();
				productViewFlags.put(ConstantsBase.HAS_DYNAMICS_FLAG, rs.getBoolean("has_dynamics"));
				if (CommonUtil.getProperty("isHasBubbles.always.flag").equalsIgnoreCase("true")) {
					productViewFlags.put(ConstantsBase.HAS_BUBBLES_FLAG, true);
				} else {					
					productViewFlags.put(ConstantsBase.HAS_BUBBLES_FLAG, rs.getBoolean("has_bubbles"));
				}
				user.setProductViewFlags(productViewFlags);
				
                return user;
            }
        } finally {
            closeResultSet(rs);
            closeStatement(pstmt);
        }
        return null;
    }

    protected static void getUserVO(ResultSet rs, User vo) throws SQLException {
        vo.setId(rs.getLong("id"));
        vo.setBalance(rs.getLong("BALANCE"));
        vo.setTaxBalance(rs.getLong("TAX_BALANCE"));
        vo.setUserName(rs.getString("USER_NAME"));
        vo.setFirstDepositId(rs.getLong("first_deposit_id"));
        try {
        	vo.setPassword(AESUtil.decrypt(rs.getString("password")));
        	vo.setEncryptedPassword(rs.getString("password"));
	        vo.setCurrencyId(rs.getLong("CURRENCY_ID"));
	        vo.setFirstName(rs.getString("FIRST_NAME"));
	        vo.setLastName(rs.getString("LAST_NAME"));
	        vo.setStreet(rs.getString("STREET"));
	        vo.setStreetNo(rs.getString("STREET_no"));
	        vo.setCityId(rs.getLong("CITY_ID"));
	        vo.setZipCode(rs.getString("ZIP_CODE"));
	        vo.setCityName(rs.getString("CITY_NAME"));

	        vo.setTimeCreated(convertToDate(rs.getTimestamp("TIME_CREATED")));
	        vo.setTimeLastLogin(convertToDate(rs.getTimestamp("TIME_LAST_LOGIN")));
	        vo.setTimeLastLoginFromDb(convertToDate(rs.getTimestamp("TIME_LAST_LOGIN")));
	        vo.setTimeBirthDate(convertToDate(rs.getTimestamp("TIME_BIRTH_DATE")));
	        vo.setTimeModified(convertToDate(rs.getTimestamp("TIME_MODIFIED")));
	        vo.setTimeFirstVisit(convertToDate(rs.getTimestamp("TIME_FIRST_VISIT")));

	        vo.setIsActive(rs.getString("IS_ACTIVE"));
	        vo.setEmail(rs.getString("EMAIL"));
	        vo.setCombinationId(rs.getLong("combination_id"));
	        vo.setComments(rs.getString("COMMENTS"));
	        vo.setIp(rs.getString("IP"));
	        vo.setIsContactByEmail(rs.getInt("IS_CONTACT_BY_EMAIL"));
	        vo.setMobilePhone(rs.getString("MOBILE_PHONE"));
	        vo.setLandLinePhone(rs.getString("LAND_LINE_PHONE"));
	        vo.setGender(rs.getString("GENDER"));
	        vo.setIdNum(rs.getString("ID_NUM"));
		} catch (	CryptoException | InvalidKeyException | IllegalBlockSizeException | BadPaddingException | NoSuchAlgorithmException
					| NoSuchPaddingException | InvalidAlgorithmParameterException ce) {
			// FIXME very bad way of handling exceptions
			throw new SQLException("CryptoException: " + ce.getMessage());
		}
        vo.setLimitId(rs.getLong("LIMIT_ID"));
        vo.setClassId(rs.getLong("class_id"));
        vo.setLastFailedTime(convertToDate(rs.getTimestamp("last_failed_time")));
        vo.setFailedCount(rs.getInt("failed_count"));

        vo.setCompanyId(rs.getBigDecimal("company_id"));
        vo.setCompanyName(rs.getString("company_name"));
        vo.setReferenceId(rs.getBigDecimal("reference_id"));

        vo.setIsContactBySMS(rs.getLong("is_contact_by_sms"));
        vo.setIsContactByPhone(rs.getLong("is_contact_by_phone"));

        // for anyOption
        vo.setCountryId(rs.getLong("country_id"));
        vo.setLanguageId(rs.getLong("language_id"));
        vo.setSkinId(rs.getLong("skin_id"));

        vo.setUtcOffsetCreated(rs.getString("utc_offset_created"));
        vo.setUtcOffsetModified(rs.getString("utc_offset_modified"));
        vo.setUtcOffset(rs.getString("utc_offset"));

        vo.setIdDocVerify(rs.getBoolean("id_doc_verify"));
        vo.setState(rs.getLong("state_code"));
        vo.setDocsRequired(rs.getBoolean("docs_required"));
        vo.setDynamicParam(rs.getString("dynamic_param"));
        vo.setFalseAccount(rs.getBoolean("is_false_account"));
        vo.setAcceptedTerms(rs.getBoolean("is_accepted_terms"));
        vo.setWriterId(rs.getLong("writer_id"));
        vo.setSkinName(rs.getString("skin_name"));
        vo.setTotalWinLose(rs.getLong("win_lose"));
        vo.setStatusId(rs.getLong("status_id"));
        vo.setPlatformId(rs.getInt("platform_id"));
        vo.setIsNeedChangePassword(rs.getBoolean("is_need_change_password"));
        vo.setAuthorizedMail(rs.getBoolean("is_authorized_mail"));
    }


	public static void addToBalance(Connection con, long id, long amount) throws SQLException {
		PreparedStatement ps = null;
		if (log.isEnabledFor(Level.DEBUG)) {
			String ls = System.getProperty("line.separator");
			log.log(Level.DEBUG,"Adding to balance - "+amount+" to user:"+id + ls );
		}
		try {
			String sql =
				" update users " +
				" set " +
					" balance = balance + ?," +
					" time_modified = sysdate, " +
					" utc_offset_modified = utc_offset " +
				" where id = ?";

			ps =  con.prepareStatement(sql);
			ps.setLong(1,amount);
			ps.setLong(2,id);
			ps.executeUpdate();
		} finally {
			closeStatement(ps);
		}
	}

    /**
     * Check if there is existing user with the specified id number.
     *
     * @param conn the db connection to use
     * @param userName the user name to check
     * @return <code>ture</code> if the user name is in use else <code>false</code>.
     * @throws SQLException
     * @throws CryptoException
     * @throws UnsupportedEncodingException 
     * @throws BadPaddingException 
     * @throws IllegalBlockSizeException 
     * @throws NoSuchPaddingException 
     * @throws NoSuchAlgorithmException 
     * @throws InvalidKeyException 
     * @throws InvalidAlgorithmParameterException 
     */
	public static boolean isUserIdNumberInUse(Connection conn, String idnum)	throws SQLException, CryptoException, InvalidKeyException,
																				NoSuchAlgorithmException, NoSuchPaddingException,
																				IllegalBlockSizeException, BadPaddingException,
																				UnsupportedEncodingException,
																				InvalidAlgorithmParameterException {
        boolean inUse = false;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            String sql =
                "SELECT " +
                    "id " +
                "FROM " +
                    "users " +
                "WHERE " +
                    "id_num = ?";
            pstmt = conn.prepareStatement(sql);
            pstmt.setString(1, AESUtil.encrypt(idnum));
            rs = pstmt.executeQuery();
            if (rs.next()) {
                inUse = true;
            }
        } finally {
            closeResultSet(rs);
            closeStatement(pstmt);
        }
        return inUse;
    }

    public static long getDefaultClassId(Connection conn) throws SQLException {
        long id = 0;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            String sql =
                "SELECT " +
                    "id " +
                "FROM " +
                    "user_classes " +
                "WHERE " +
                    "is_default = 1";
            pstmt = conn.prepareStatement(sql);
            rs = pstmt.executeQuery();
            if (rs.next()) {
                id = rs.getLong("id");
            }
        } finally {
            closeResultSet(rs);
            closeStatement(pstmt);
        }
        return id;
    }
    
	public static Map<String, String> getCrypthoConfig(Connection conn) {
		Map<String, String> config = new HashMap<>();
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			String sql = "SELECT " + " * " + "FROM " + "cryptho_config ";
			pstmt = conn.prepareStatement(sql);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				config.put(rs.getString("config_key"), rs.getString("config_value"));
			}
		} catch (SQLException e) {
			log.error("Fail to retrieve config values due to: ", e);
			log.debug("There is no configuration for enable/disable google recaptha");
			log.debug("Default behaviour --> recaptha is enabled!!!");
			config.put("captcha", "true");
			return config;
		} finally {
			closeResultSet(rs);
			closeStatement(pstmt);
		}
		return config;
	}

    public static void insert(Connection conn, User vo) throws SQLException {
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            String sql =
                "INSERT INTO users " +
                    "(balance, tax_balance, user_name, password, currency_id, first_name, last_name, street, street_no, " +
                    "city_id, zip_code, is_active, email, combination_id, comments, ip, time_birth_date, " +
                    "is_contact_by_email, mobile_phone, land_line_phone, gender, id_num, limit_id, id, time_created, " +
                    "time_last_login, time_modified, class_id, keyword, company_id, company_name, tax_exemption, " +
                    "reference_id, city_name, country_id, language_id, skin_id, is_contact_by_sms, utc_offset_created, " +
                    "utc_offset_modified, id_doc_verify, state_code, dynamic_param, contact_id, is_false_account, " +
                    "is_accepted_terms, time_first_visit, writer_id, utc_offset, device_unique_id, password_back, is_authorized_mail, " +
                    "time_first_authorized, id_num_back, user_agent, http_referer, affiliate_key, device_family, platform_id) " +
                "VALUES " +
                    "(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, sysdate, " +
                    "?, sysdate, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
            
            long id = getSequenceNextVal(conn, "SEQ_USERS");
			vo.setId(id);
            
            pstmt = conn.prepareStatement(sql);
            pstmt.setLong(1,vo.getBalance());
            pstmt.setLong(2,vo.getTaxBalance());
            if (CommonUtil.containsTestDomain(vo.getUserName())) {  // for testing
				if (isUserNameInUse(conn, vo.getUserName(), false, null)) {
					vo.setUserName(String.valueOf(id));
				}
			}
            pstmt.setString(3,vo.getUserName().toUpperCase());
            try {
	            if (vo.getSkinId() == Skin.SKIN_ETRADER) {
	            	vo.setPassword(vo.getPassword().toUpperCase());
	            }
	            pstmt.setString(4, AESUtil.encrypt(vo.getPassword()));
	            pstmt.setLong(5, vo.getCurrencyId());
	            pstmt.setString(6, CommonUtil.capitalizeFirstLetters(vo.getFirstName()));
	            pstmt.setString(7, CommonUtil.capitalizeFirstLetters(vo.getLastName()));
	            pstmt.setString(8, vo.getStreet());
	            pstmt.setString(9, vo.getStreetNo());
	            pstmt.setLong(10, vo.getCityId());
	            pstmt.setString(11, vo.getZipCode());
	            pstmt.setInt(12, Integer.parseInt(vo.getIsActive()));
	            pstmt.setString(13, vo.getEmail());
	            pstmt.setLong(14, vo.getCombinationId());
	            pstmt.setString(15, vo.getComments());
	            pstmt.setString(16, vo.getIp());
	            if (vo.getTimeBirthDate() == null) {
	            	pstmt.setNull(17, Types.DATE);
	            } else {
	            	pstmt.setTimestamp(17, convertToTimestamp(vo.getTimeBirthDate()));
	            }
	            pstmt.setInt(18, vo.getIsContactByEmail());
	            pstmt.setString(19, vo.getMobilePhone());
	            pstmt.setString(20, vo.getLandLinePhone());
	            pstmt.setString(21, vo.getGender());
	            if (vo.getIdNum() == null) {
	            	pstmt.setNull(22, Types.VARCHAR);
	            } else {
	            	pstmt.setString(22, AESUtil.encrypt(vo.getIdNum()));
	            }
			} catch (	CryptoException | InvalidKeyException | NoSuchAlgorithmException | NoSuchPaddingException | IllegalBlockSizeException
						| BadPaddingException | UnsupportedEncodingException | InvalidAlgorithmParameterException ce) {
				throw new SQLException("Unable to encrypt String" + ce.getMessage());
			}
            pstmt.setLong(23, vo.getLimitId());
            pstmt.setLong(24, vo.getId());
            pstmt.setTimestamp(25, convertToTimestamp(vo.getTimeLastLogin()));
            pstmt.setLong(26, vo.getClassId());
            pstmt.setString(27, vo.getKeyword());
            pstmt.setBigDecimal(28, vo.getCompanyId());
            pstmt.setString(29, vo.getCompanyName());
            pstmt.setInt(30, vo.isTaxExemption()==true ? 1 : 0);
            pstmt.setBigDecimal(31, vo.getReferenceId());
            pstmt.setString(32, vo.getCityName());
            pstmt.setLong(33, vo.getCountryId());
            pstmt.setLong(34, vo.getLanguageId());
            pstmt.setLong(35, vo.getSkinId());
            pstmt.setLong(36, vo.getIsContactBySMS());
            pstmt.setString(37, vo.getUtcOffsetCreated());
            pstmt.setString(38, vo.getUtcOffsetModified());
            pstmt.setBoolean(39, vo.isIdDocVerify());
			pstmt.setLong(40, (vo.getState() == null) ? 0L : vo.getState());
            pstmt.setString(41, vo.getDynamicParam());
            pstmt.setLong(42, vo.getContactId());
            pstmt.setBoolean(43, vo.isFalseAccount());
            pstmt.setBoolean(44, vo.isAcceptedTerms());
            pstmt.setTimestamp(45, convertToTimestamp(vo.getTimeFirstVisit()));
            pstmt.setLong(46, vo.getWriterId());
            pstmt.setString(47, vo.getUtcOffsetCreated());
            pstmt.setString(48, vo.getDeviceUniqueId());
//            pstmt.setString(49, vo.getPassword());
            pstmt.setNull(49, Types.VARCHAR);
            pstmt.setInt(50, vo.isAuthorizedMail() ? 1 : 0);
            pstmt.setTimestamp(51, vo.getSkinId() == Skin.SKIN_ETRADER ? null : convertToTimestamp(new Date()));
            pstmt.setString(52, vo.getIdNum());
            pstmt.setString(53, vo.getUserAgent());
            pstmt.setString(54, vo.getHttpReferer());
            if (vo.getAffiliateKey() == 0) {
            	pstmt.setNull(55, Types.NUMERIC);
            } else {
            	pstmt.setLong(55, vo.getAffiliateKey());
            }
            pstmt.setString(56, (vo.getDeviceFamily() == null || vo.getDeviceFamily().equals("null")) ? "-100" : vo.getDeviceFamily());
            pstmt.setInt(57, vo.getPlatformId());
            pstmt.executeUpdate();
        } catch(SQLIntegrityConstraintViolationException e) {
        	String msg = "Constraint violated: " + e.getMessage().substring(e.getMessage().indexOf('(') + 1, e.getMessage().indexOf(')'));
        	if (e.getMessage().toUpperCase().contains(ACTIVE_SKIN_CONSTRAINT)) {
				log.error(msg + ", skinId: " + vo.getSkinId());
			} else {
				log.error(msg, e);
			}
			throw e;
        } finally {
            closeResultSet(rs);
            closeStatement(pstmt);
        }
    }
    
	public static void updateDetailsCryptho(Connection con, com.anyoption.common.beans.base.User vo) throws SQLException {
		PreparedStatement ps = null;
		try {
			String sql =
					"UPDATE " +
						"users " +
					"SET " +
						"city_name = ?, " +
						"country_id = ?, " +
						"mobile_phone = ?, " +
						"street = ?, " +
						"zip_code = ?, " +
						"time_birth_date = ?, " +
						"first_name = ?, " +
						"last_name = ? " +
			       "WHERE id = ? ";

			ps = con.prepareStatement(sql);

			int index = 1;

			ps.setString(index++, vo.getCityName());
			ps.setLong(index++, vo.getCountryId());
			ps.setString(index++, vo.getMobilePhone());
			ps.setString(index++, vo.getStreet());
			ps.setString(index++, vo.getZipCode());
            if (vo.getTimeBirthDate() == null) {
            	ps.setNull(index++, Types.DATE);
            } else {
            	ps.setTimestamp(index++, CommonUtil.convertToTimeStamp(vo.getTimeBirthDate()));
            }
            
            ps.setString(index++, vo.getFirstName());
            ps.setString(index++, vo.getLastName());
			ps.setLong(index++, vo.getId());
			
			ps.executeUpdate();
			
		} finally {
			closeStatement(ps);
		}
	}

	public static void updateDetails(Connection con, com.anyoption.common.beans.base.User vo) throws SQLException {
		PreparedStatement ps = null;
		try {
			String sql =
					"UPDATE " +
						"users " +
					"SET " +
						"first_name = ?, " +
						"last_name = ?, " +
						"city_name = ?, " +
						"country_id = ?, " +
						"email = ?, " +
						"is_contact_by_email = ?, " +
						"is_contact_by_sms = ?, " +
						"mobile_phone = ?, " +
						"land_line_phone = ?, " +
						"street = ?, " +
						"street_no = ?, " +
						"zip_code = ?, " +
						"state_code = ?, " +
						"gender = ?, " +
						"time_birth_date = ?, " +
						"is_accepted_terms = ?, " +
						"utc_offset = ? " +
					 "WHERE id = ? ";

			ps = con.prepareStatement(sql);

			int index = 1;
			
			ps.setString(index++, vo.getFirstName());
			ps.setString(index++, vo.getLastName());
			ps.setString(index++, vo.getCityName());
			ps.setLong(index++, vo.getCountryId());
			ps.setString(index++ ,vo.getEmail());
			ps.setInt(index++, vo.getIsContactByEmail());
			ps.setLong(index++, vo.getIsContactBySMS());
			ps.setString(index++, vo.getMobilePhone());
			ps.setString(index++ ,vo.getLandLinePhone());
			ps.setString(index++, vo.getStreet());
			ps.setString(index++, vo.getStreetNo());
			ps.setString(index++, vo.getZipCode());
			ps.setLong(index++, vo.getState() == null ? 0 : vo.getState());
			ps.setString(index++, vo.getGender());
            if (vo.getTimeBirthDate() == null) {
            	ps.setNull(index++, Types.DATE);
            } else {
            	ps.setTimestamp(index++, CommonUtil.convertToTimeStamp(vo.getTimeBirthDate()));
            }
			ps.setInt(index++, vo.isAcceptedTerms() ? 1 : 0);
			ps.setString(index++, vo.getUtcOffset());
			ps.setLong(index++, vo.getId());
			ps.executeUpdate();
		} finally {
			closeStatement(ps);
		}
	}

  	public static long getSkinIdByUserId(Connection con,long userId)  throws SQLException{

		  PreparedStatement ps=null;
		  ResultSet rs=null;
		  long skinId=0;
		  try {
			    String sql="select skin_id from users where id=?";

				ps = con.prepareStatement(sql);
				ps.setLong(1, userId);

				rs=ps.executeQuery();

				if (rs.next()) {
					skinId = rs.getLong("SKIN_ID");
				}
			}

			finally
			{
				closeResultSet(rs);
				closeStatement(ps);
			}
			return skinId;

	  }

  	/**
  	 * Get user password details
  	 * @param con
  	 * @param name
  	 * @param skinId
  	 * @return
  	 * @throws SQLException
  	 */
    public static HashMap<String, Object> getPasswordDetails(Connection con, String name, long skinId)  throws SQLException{
        PreparedStatement ps = null;
        ResultSet rs = null;

        try {
            String sql =
                "SELECT " +
    				"* " +
    			"FROM " +
    				"users " +
    			"WHERE " +
    				"user_name = ? AND " +
    				"skin_id = ?";
            ps = con.prepareStatement(sql);
            ps.setString(1, name);
            ps.setLong(2, skinId);
            rs=ps.executeQuery();
            if (rs.next()) {
                HashMap<String, Object> h = new HashMap<String, Object>();
                h.put("id", rs.getString("id"));
                try {
                	h.put("password", AESUtil.decrypt(rs.getString("password")));
				} catch (	CryptoException | InvalidKeyException | IllegalBlockSizeException | BadPaddingException | NoSuchAlgorithmException
							| NoSuchPaddingException | InvalidAlgorithmParameterException ce) {
					// FIXME very bad way of handling exceptions
					throw new SQLException("CryptoException: " + ce.getMessage());
				}
                h.put("email", rs.getString("email"));
                h.put("birthDate", rs.getDate("time_birth_date"));
                if (skinId == Skin.SKIN_ETRADER) {
                	try {
                		h.put("idNum", AESUtil.decrypt(rs.getString("id_num")));
					} catch (	CryptoException | InvalidKeyException | IllegalBlockSizeException | BadPaddingException
								| NoSuchAlgorithmException | NoSuchPaddingException | InvalidAlgorithmParameterException e) {
						// FIXME very bad way of handling exceptions
						throw new SQLException(e.getMessage());
					}
                }
                return h;
                }
        } finally {
            closeResultSet(rs);
            closeStatement(ps);
        }
        return null;
    }

    /**
     *
     * @param con db connection
     * @param email user email
     * @param idNum user id number (for ET only)
     * @param skinId user skin id
     * @return
     * @throws Exception
     */
    public static ArrayList<User> getPasswordDetails(Connection con, String email) throws Exception {
        ArrayList<User> arr = new ArrayList<User>();
		PreparedStatement ps = null;
        ResultSet rs = null;

        try {
            String sql = "SELECT " +
            			 	"u.id user_id, " +
            			 	"u.password, " +
            			 	"u.email, " +
            			 	"u.skin_id, " +
            			 	"u.user_name, " +
            			 	"u.mobile_phone, " +
            			 	"u.mobile_phone_validated, " +
            			 	"c.phone_code as mobile_phone_prefix, " +
            			 	"c.id country_id " +
            			 "FROM " +
            			 	"users u , " +
            			 	"countries c " +
            			 "WHERE " +
            			 	 "u.country_id = c.id " +
            			     "and upper(u.email) = upper(?) ";
            

            ps = con.prepareStatement(sql);
            ps.setString(1, email.toLowerCase());


            rs = ps.executeQuery();
            while (rs.next()) {
            	User usr = new User();
            	usr.setId(rs.getLong("user_id"));
            	usr.setPassword(AESUtil.decrypt(rs.getString("password")));
            	usr.setEmail(rs.getString("email"));
            	usr.setSkinId(rs.getLong("skin_id"));
            	usr.setUserName(rs.getString("user_name"));
            	usr.setMobilePhone(rs.getString("mobile_phone"));
            	usr.setMobilePhonePrefix(rs.getString("mobile_phone_prefix"));
            	usr.setCountryId(rs.getLong("country_id"));
            	usr.setMobileNumberValidated(rs.getInt("mobile_phone_validated"));
            	arr.add(usr);
            }
        } finally {
            closeResultSet(rs);
            closeStatement(ps);
        }
        return arr;
    }

	public static void updatePassword(Connection con, User vo)	throws SQLException, InvalidKeyException, NoSuchAlgorithmException,
																NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException,
																UnsupportedEncodingException, InvalidAlgorithmParameterException {
		PreparedStatement ps = null;
		try {
			String sql =
					"UPDATE " +
						"users " +
					"SET " +
						"password = ? , " +
						"password_back = ? " +
					"WHERE " +
						"id = ? ";

			ps = con.prepareStatement(sql);
			try {
				ps.setString(1, AESUtil.encrypt(vo.getPassword()));
			} catch (CryptoException ce) {
				throw new SQLException("CryptoException: " + ce.getMessage());
			}
			ps.setString(2, vo.getPassword());
			ps.setLong(3, vo.getId());
			ps.executeUpdate();
		} finally {
			closeStatement(ps);
		}
	}

	/**
	 * Update document_required field in a case the
	 * user sum deposits over the max, he need to send us documents.
	 *
	 * @param conn Db connection
	 * @param userId user id
	 * @throws SQLException
	 */
	public static void setFlagsAfterMaxDeposit(Connection conn, long userId) throws SQLException {
	    PreparedStatement pstmt = null;
	    try {
	        String sql =
	            "UPDATE " +
	                "users " +
	            "SET " +
	                "docs_required = 1 " +
	            "WHERE " +
	                "id = ?";
	        pstmt = conn.prepareStatement(sql);
	        pstmt.setLong(1, userId);
	        pstmt.executeUpdate();
	    } finally {
	        closeStatement(pstmt);
	    }
	}

    public static User getById(Connection conn, long userId) throws SQLException {
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        User vo = null;
        try {
            String sql =
                " SELECT " +
                    " u.*, " +
                    " s.name as skin_name, " +
                    " decode(nvl(ur.id,0) ,0 ,0 ,1) as is_regulation, " +
                    " uad.is_need_change_password, "
                    + "uad.show_cancel_investment " +
                " FROM " +
                    " users u , " +
                    " users_regulation ur, " +
                    " skins s," +
                    " users_active_data uad " +
                " WHERE " +
                	" u.skin_id = s.id " +
                	" AND u.id = ur.user_id(+) " +
                	" AND u.id = uad.user_id " +
                    " AND u.id = ? ";
            pstmt = conn.prepareStatement(sql);
            pstmt.setLong(1, userId);
            rs = pstmt.executeQuery();
            if (rs.next()) {
                vo = new User();
                getUserVO(rs, vo);
            }
        } finally {
            closeResultSet(rs);
            closeStatement(pstmt);
        }
        return vo;
    }

	public static void updateLoginFailed(Connection con, String userName, boolean isNotActive) throws SQLException {
		PreparedStatement ps = null;
		try {
			String sql =
					"UPDATE " +
						"users " +
					"SET " +
						"last_failed_time = SYSDATE, " +
						"failed_count = FAILED_COUNT+1 ";

			if (isNotActive) {
				sql += ", IS_ACTIVE = 0 ";
			}

			sql += "WHERE " +
				   		"UPPER(user_name) = ?";

			ps = con.prepareStatement(sql);
			ps.setString(1, userName.toUpperCase());
			ps.executeUpdate();
		} finally {
			closeStatement(ps);
		}
	}

	public static void updateLoginSucceed(Connection con, String userName) throws SQLException {
		PreparedStatement ps = null;
		try {
			String sql =
					"UPDATE " +
						"users " +
					"SET " +
						"last_failed_time = NULL, " +
						"failed_count = 0, " +
						"time_last_login = sysdate " +
					"WHERE " +
				   		"UPPER(user_name) = ?";

			ps = con.prepareStatement(sql);
			ps.setString(1, userName.toUpperCase());
			ps.executeUpdate();
		} finally {
			closeStatement(ps);
		}
	}

	public static void updateUser(Connection con, User vo) throws SQLException {
		PreparedStatement ps = null;
		try {
			String sql =
					"UPDATE " +
						"users SET " +
							"user_name = ?, " +
							"password = ?, " +
							"password_back = ?, " +
							"currency_id = ?," +
							"first_name = ?, " +
							"last_name = ?, " +
							"street = ?, " +
							"city_id = ?, " +
							"zip_code = ?, " +
							"time_last_login = ?, " +
							"is_active = ?, " +
							"email = ?, " +
							"combination_id = ?, " +
							"comments = ?, " +
							"ip = ?, " +
							"time_birth_date = ?, " +
							"is_contact_by_email = ?, " +
							"mobile_phone = ?, " +
							"land_line_phone = ?, " +
							"gender = ?, " +
							"id_num = ?, " +
							"id_num_back = ?, " +
							"limit_id = ?, " +
							"time_modified = sysdate, " +
							"class_id = ?, " +
							"street_no = ?, " +
							"last_failed_time = ?, " +
							"failed_count = ?," +
							"company_id = ?, " +
							"company_name = ?, " +
							"tax_exemption = ?, " +
							"reference_id = ?, " +
							"is_contact_by_sms = ?, " +
							"skin_id = ?, " +
							"utc_offset_modified = ?, " +
							"city_name = ?, " +
							"utc_offset = ?, " +
							"id_doc_verify = ?, " +
							"country_id = ?, " +
							"language_id = ?, " +
							"state_code = ?, "+
							"docs_required = ?, " +
							"is_contact_by_phone = ?, " +
							"is_false_account = ?, " +
							"is_vip = ?, " +
							"is_authorized_mail = ?, " +
							"authorized_mail_refused_times = ?, " +
							"is_risky = ? " +
					"WHERE id=?";

			ps = con.prepareStatement(sql);

			ps.setString(1,vo.getUserName().toUpperCase());

			String password;
			if(vo.getSkinId()== Skin.SKIN_ETRADER){
				password = vo.getPassword().toUpperCase();
			} else {
				password = vo.getPassword();
			}
			try {
				ps.setString(2, AESUtil.encrypt(password));
			} catch (	CryptoException | InvalidKeyException | NoSuchAlgorithmException | NoSuchPaddingException | IllegalBlockSizeException
						| BadPaddingException | UnsupportedEncodingException | InvalidAlgorithmParameterException ce) {
				// FIXME very bad way of handling exceptions
				throw new SQLException("CryptoException: " + ce.getMessage());
			}
			ps.setString(3, password); // password_back

			ps.setLong(4,vo.getCurrencyId());

			ps.setString(5,CommonUtil.capitalizeFirstLetters(vo.getFirstName()));
			ps.setString(6,CommonUtil.capitalizeFirstLetters(vo.getLastName()));
			ps.setString(7,vo.getStreet());
			ps.setLong(8,vo.getCityId());
			ps.setString(9,vo.getZipCode());

			ps.setTimestamp(10,CommonUtil.convertToTimeStamp(vo.getTimeLastLogin()));

			ps.setInt(11,Integer.parseInt(vo.getIsActive()));
			ps.setString(12,vo.getEmail());
			ps.setLong(13,vo.getCombinationId());
			ps.setString(14,vo.getComments());
			ps.setString(15,vo.getIp());
			ps.setTimestamp(16,CommonUtil.convertToTimeStamp(vo.getTimeBirthDate()));

			ps.setInt(17,vo.getIsContactByEmail());

			ps.setString(18,vo.getMobilePhone());
			ps.setString(19,vo.getLandLinePhone());
			ps.setString(20,vo.getGender());
			ps.setString(21, AESUtil.encrypt(vo.getIdNum()));
			ps.setString(22,vo.getIdNum()); // id_num_back

			ps.setLong(23,vo.getLimitId());

			ps.setLong(24,vo.getClassId());
			ps.setString(25,vo.getStreetNo());

			ps.setTimestamp(26,CommonUtil.convertToTimeStamp(vo.getLastFailedTime()));
			ps.setInt(27,vo.getFailedCount());

			ps.setBigDecimal(28,vo.getCompanyId());
			ps.setString(29,vo.getCompanyName());
			ps.setInt(30,vo.isTaxExemption()==true ? 1 : 0);
			ps.setBigDecimal(31,vo.getReferenceId());
			//ps.setInt(31,vo.getIsNextInvestOnUs());
			ps.setInt(32,vo.isContactBySMS() ?  1 : 0);
			ps.setLong(33, vo.getSkinId());
			ps.setString(34,vo.getUtcOffset());
			ps.setString(35, vo.getCityName());
			ps.setString(36, vo.getUtcOffset());
			ps.setInt(37, vo.isIdDocVerify()==true ? 1 : 0);
			ps.setLong(38, vo.getCountryId());
			ps.setLong(39, vo.getLanguageId());
			ps.setLong(40, vo.getState());
			ps.setInt(41, vo.isDocsRequired()==true ? 1 : 0);
			ps.setInt(42,vo.isContactByPhone() ?  1 : 0);
			ps.setInt(43,(vo.isFalseAccount() ? 1 : 0));
			ps.setInt(44,(vo.isVip() ? 1 : 0));
			ps.setInt(45,(vo.isAuthorizedMail() ? 1 : 0));
			ps.setLong(46,(vo.getAuthorizedMailRefusedTimes()));
            ps.setInt(47,(vo.isRisky() ? 1 : 0));
			// Set The user Id
			ps.setLong(48, vo.getId());
			ps.executeUpdate();

		} catch (	CryptoException | InvalidKeyException | NoSuchAlgorithmException | NoSuchPaddingException | IllegalBlockSizeException
					| BadPaddingException | UnsupportedEncodingException | InvalidAlgorithmParameterException ce) {
			// FIXME very bad way of handling exceptions
			throw new SQLException(ce.getMessage());
		} finally {
			closeStatement(ps);
		}
	}

	public static boolean getByUserId(Connection conn, long userId, User vo) throws SQLException {
		return getByUserId(conn, userId, vo, false);
	}

public static boolean getByUserId(Connection con, long userId, User vo, boolean basic) throws SQLException {
	PreparedStatement ps = null;
	ResultSet rs = null;
	try {
	    String sql =
	            "SELECT " +
	                "* " +
	            "FROM " +
	                "users " +
	            "WHERE " +
	                "id = ?";
	    ps = con.prepareStatement(sql);
	    ps.setLong(1,userId);
	    rs = ps.executeQuery();
		if (rs.next()) {
		 	if (basic) {
	        	getVOBasic(rs, vo);
	        } else {
	            getVO(con, rs, vo);
				/*long tierUserId = rs.getLong("tier_user_id");
				if (tierUserId > 0) {   			************ NOT IN USE***********
					vo.setTierUser(TiersDAOBase.getTierUserById(con, tierUserId));
				}*/
	        }
			return true;
		}
	} finally {
	    closeResultSet(rs);
	    closeStatement(ps);
	}
	return false;
	}



	protected static void getVOBasic(ResultSet rs, User vo) throws SQLException {
	    vo.setId(rs.getLong("id"));
	    vo.setBalance(rs.getLong("BALANCE"));
	    vo.setTaxBalance(rs.getLong("TAX_BALANCE"));
	    vo.setUserName(rs.getString("USER_NAME"));
	    try {
	    	vo.setPassword(AESUtil.decrypt(rs.getString("password")));
		} catch (	CryptoException | InvalidKeyException | IllegalBlockSizeException | BadPaddingException | NoSuchAlgorithmException
					| NoSuchPaddingException | InvalidAlgorithmParameterException ce) {
			// FIXME very bad way of handling exceptions
			throw new SQLException("CryptoException: " + ce.getMessage());
		}
	    vo.setCurrencyId(rs.getLong("CURRENCY_ID"));
	    vo.setFirstName(rs.getString("FIRST_NAME"));
	    vo.setLastName(rs.getString("LAST_NAME"));
	    vo.setStreet(rs.getString("STREET"));
	    vo.setStreetNo(rs.getString("STREET_no"));
	    vo.setCityId(rs.getLong("CITY_ID"));
	    vo.setZipCode(rs.getString("ZIP_CODE"));
	    vo.setFirstDepositId(rs.getLong("first_deposit_id"));

	    vo.setTimeCreated(convertToDate(rs.getTimestamp("TIME_CREATED")));
	    vo.setTimeLastLogin(convertToDate(rs.getTimestamp("TIME_LAST_LOGIN")));
	    vo.setTimeLastLoginFromDb(convertToDate(rs.getTimestamp("TIME_LAST_LOGIN")));
	    vo.setTimeBirthDate(convertToDate(rs.getTimestamp("TIME_BIRTH_DATE")));
	    vo.setTimeModified(convertToDate(rs.getTimestamp("TIME_MODIFIED")));
	    vo.setTimeFirstVisit(convertToDate(rs.getTimestamp("TIME_FIRST_VISIT")));

	    vo.setIsActive(rs.getString("IS_ACTIVE"));
	    vo.setEmail(rs.getString("EMAIL"));
	    vo.setCombinationId(rs.getLong("combination_id"));
	    vo.setComments(rs.getString("COMMENTS"));
	    vo.setIp(rs.getString("IP"));

	    vo.setIsContactByEmail(rs.getInt("IS_CONTACT_BY_EMAIL"));
	    vo.setMobilePhone(rs.getString("MOBILE_PHONE"));
	    if(rs.getLong("skin_id") == Skin.SKIN_ETRADER) {
	    	vo.setLandLinePhoneET(rs.getString("LAND_LINE_PHONE"));
	    } else {
	    	vo.setLandLinePhone(rs.getString("LAND_LINE_PHONE"));
	    }
	    vo.setGender(rs.getString("GENDER"));
	    try {
	    	vo.setIdNum(AESUtil.decrypt(rs.getString("id_num")));
		} catch (	CryptoException | InvalidKeyException | IllegalBlockSizeException | BadPaddingException | NoSuchAlgorithmException
					| NoSuchPaddingException | InvalidAlgorithmParameterException ce) {
			// FIXME very bad way of handling exceptions
			throw new SQLException("CryptoException: " + ce.getMessage());
		}
	    vo.setLimitId(rs.getLong("LIMIT_ID"));
	    vo.setClassId(rs.getLong("class_id"));
	    vo.setLastFailedTime(convertToDate(rs.getTimestamp("last_failed_time")));
	    vo.setFailedCount(rs.getInt("failed_count"));

	    vo.setCompanyId(rs.getBigDecimal("company_id"));
	    vo.setCompanyName(rs.getString("company_name"));
	    vo.setTaxExemption(rs.getInt("tax_exemption")==1);
	    vo.setReferenceId(rs.getBigDecimal("reference_id"));

	    vo.setIsContactBySMS(rs.getLong("is_contact_by_sms"));
	    vo.setIsContactByPhone(rs.getLong("is_contact_by_phone"));

	    // for anyOption
	    vo.setCountryId(rs.getLong("country_id"));

	    vo.setLanguageId(rs.getLong("language_id"));

	    vo.setSkinId(rs.getLong("skin_id"));

	    vo.setUtcOffsetCreated(rs.getString("utc_offset_created"));
	    vo.setUtcOffsetModified(rs.getString("utc_offset_modified"));
	    vo.setUtcOffset(rs.getString("utc_offset"));
		vo.setContactForDaa(rs.getLong("is_contact_for_daa") == 1 ? true : false);
	    long idDocNeedVerify = rs.getLong("id_doc_verify");
	    if ( idDocNeedVerify == 1 ) {
	        vo.setIdDocVerify(true);
	    }
	    else {
	        vo.setIdDocVerify(false);
	    }

	    vo.setState(rs.getLong("state_code"));

	    long docsRequired = rs.getLong("docs_required");
	    if ( docsRequired == 1 ) {
	        vo.setDocsRequired(true);
	    } else {
	        vo.setDocsRequired(false);
	    }

	    vo.setDynamicParam(rs.getString("dynamic_param"));
		if (rs.getInt("is_false_account") == 1){
			vo.setFalseAccount(true);
		} else {
			vo.setFalseAccount(false);
		}

		if (rs.getInt("is_accepted_terms") == 1){
			vo.setAcceptedTerms(true);
		} else {
			vo.setAcceptedTerms(false);
		}


		vo.setAffiliateKey(rs.getLong("affiliate_key"));
		vo.setSubAffiliateKey(rs.getLong("sub_affiliate_key"));
		vo.setSpecialCode(rs.getString("special_code"));
		vo.setChatId(rs.getLong("chat_id"));
	    vo.setWriterId(rs.getLong("writer_id"));
	    vo.setContactId(rs.getLong("contact_id"));
	    vo.setClickId(rs.getString("click_id"));
	    vo.setVip(rs.getInt("is_vip")==1);
	    vo.setAuthorizedMail(rs.getInt("is_authorized_mail") == 1 ? true : false);
	    vo.setAuthorizedMailRefusedTimes(rs.getLong("authorized_mail_refused_times"));
	    vo.setTimeFirstAuthorized(convertToDate(rs.getTimestamp("TIME_FIRST_AUTHORIZED")));
	    vo.setUserAgent(rs.getString("user_agent"));
	    vo.setDeviceUniqueId(rs.getString("device_unique_id"));
	    vo.setRisky(rs.getLong("is_risky") == 1 ? true : false);
	    vo.setTotalWinLose(rs.getLong("win_lose"));
	    vo.setPlatformId(rs.getInt("platform_id"));
	}

	protected static void getVO(Connection con, ResultSet rs, User vo) throws SQLException {
	    getVOBasic(rs, vo);

		//vo.setIsNextInvestOnUs(rs.getInt("is_next_invest_on_us"));
		//vo.setIsNextInvestOnUs(BonusDAOBase.hasNextInvestOnUs(con, rs.getLong("id")) > 0 ? 1 : 0);

		// for anyOption
	    vo.setCountry(CountryManagerBase.getCountry(vo.getCountryId()));
	    if (null != vo.getCountry()) {
	        vo.setCountryName(vo.getCountry().getDisplayName());
	    }
	//	String countryName = CountryDAOBase.getNameById(con, countryId.intValue());
	//	if( null != countryId && null!= countryName) {
	//		vo.setCountryId(countryId);
	//		vo.setCountryName(countryName);
	//	}

	    Language lng = LanguagesManagerBase.getLanguage(vo.getLanguageId());
	    if (null != lng) {
	        vo.setLanguageName(lng.getDisplayName());
	    }
	//	String languageName = LanguagesDAOBase.getNameById(con,languageId.intValue());
	//	if( null != languageId  && null!= languageName) {
	//		vo.setLanguageId(languageId);
	//		vo.setLanguageName(languageName);
	//	}
	    vo.setCurrency(CurrenciesManagerBase.getCurrency(vo.getCurrencyId()));

		Long skinId = rs.getLong("skin_id");
		String skinName = SkinsDAOBase.getNameById(con, skinId.intValue());
		if( null != skinId && null != skinName) {
			vo.setSkinId(skinId);
			vo.setSkinName(skinName);
			vo.setSkin(SkinsManagerBase.getSkin(skinId));
		}

		vo.setCityName(rs.getString("city_name"));
	}

	    public static void setUserFirstDeclineTran(Connection con, long userId, Transaction tran) throws SQLException {
	        PreparedStatement pstmt = null;
	        try {
	            String sql = "UPDATE users_active_data " + 
	            			 "SET first_decline_amount_usd = ? " +
	            			 "WHERE user_id = ? AND first_decline_amount_usd is null";
	            
	            double rate = TransactionsDAOBase.getRateById(con, tran.getId());
	            double amount = tran.getAmount() * rate;
	            
	            pstmt = con.prepareStatement(sql);
	            
	            pstmt.setDouble(1, amount);
	            pstmt.setLong(2, userId);
	            
	            pstmt.executeUpdate();

	        } finally {
	            closeStatement(pstmt);
	        }
	    }
	    
	    public static void updateFirstDeclineAmount(Connection con, long userId) throws SQLException {
	        PreparedStatement pstmt = null;
	        try {
	            String sql = "UPDATE users_active_data " + 
	            			 "SET first_decline_amount_usd = null " +
	            			 "WHERE user_id = ?";
	            
	            pstmt = con.prepareStatement(sql);
	            
	            pstmt.setLong(1, userId);
	            
	            pstmt.executeUpdate();

	        } finally {
	            closeStatement(pstmt);
	        }
	    }
	    
	    /**
	     * Get userId by phone or email
	     * @param con
	     * @param phone
	     * @param email
	     * @return
	     * @throws SQLException
	     */
	    public static long getUserByPhoneEmail(Connection con, String phone, String email, long countryId) throws SQLException {
	    	 PreparedStatement pstmt = null;
	         ResultSet rs = null;
	         long userId = 0;
	         try{
	             String sql =
	                     "SELECT " +
	                         "id " +
	                     "FROM " +
	                         "users " +
	                     "WHERE " +
	                     	 "country_id = ? AND " +
	                         "(mobile_phone = ? OR " +
	                         "land_line_phone = ? ";

	             if (null != email) {
	            	 sql += "OR upper(email) = upper(?) ";
	             }

	             sql += ") ";


	             pstmt = con.prepareStatement(sql);
	             pstmt.setLong(1, countryId);
	             pstmt.setString(2, phone);
	             pstmt.setString(3, phone);
	             if (null != email) {
	            	 pstmt.setString(4, email);
	             }
	             rs = pstmt.executeQuery();
	             if (rs.next()) {
	            	 userId =  rs.getLong("id");
	             }
	         } finally {
	             closeStatement(pstmt);
	             closeResultSet(rs);
	         }
	         return userId;
	    }

	    /**
	     * Get User mailBox emails
	     * @param conn
	     * @param userId
	     * @return
	     * @throws SQLException
	     */
	    public static ArrayList<MailBoxUser> getUserMailBox(Connection conn, long userId) throws SQLException {
	        PreparedStatement pstmt = null;
	        ResultSet rs = null;
	        ArrayList<MailBoxUser> emails = new ArrayList<MailBoxUser>();
	        try{
	            String sql =
	                    "SELECT " +
	                        "mbu.id, " +
	                        "mbu.status_id, " +
	                        "mbu.time_created, " +
	                        "mbu.time_read, " +
	                        "mbu.utc_offset_read, " +
	                        "mbu.is_high_priority, " +
	                        "mbu.subject, " +
	                        "mbu.transaction_id, " +
	                        "mbu.attachment_id, " +
	                        "mbt.type_id, " +
	                        "mbt.template, " +
	                        "mbs.sender, " +
	                        "mbu.free_text, " +
	                        "mbu.bonus_user_id, " +
	                        "mbu.template_parameters, " +
	                        "mut.attachment_name " +
	                    "FROM " +
	                        "mailbox_users mbu " +
	                        	"LEFT JOIN mailbox_users_attachments mut on mbu.attachment_id = mut.id, " +
	                        "mailbox_templates mbt, " +
	                        "mailbox_senders mbs " +
	                    "WHERE " +
	                        "mbu.user_id = ? AND " +
	                        "mbu.template_id = mbt.id AND " +
	                        "mbu.sender_id = mbs.id AND " +
	                        "mbu.status_id != ? " +
		                "ORDER BY " +
		                	"mbu.time_created desc ";

	            pstmt = conn.prepareStatement(sql);
	            pstmt.setLong(1, userId);
	            pstmt.setLong(2, MailBoxUser.MAILBOX_STATUS_DETETED);

	            rs = pstmt.executeQuery();
	            while (rs.next()) {
	            	MailBoxUser email = new MailBoxUser();
	            	email.setId(rs.getLong("id"));
	            	email.setStatusId(rs.getLong("status_id"));
	            	email.setTimeCreated(convertToDate(rs.getTimestamp("time_created")));
	            	email.setTimeRead(convertToDate(rs.getTimestamp("time_read")));
	            	email.setUtcOffsetRead(rs.getString("utc_offset_read"));
	            	email.setSubject(rs.getString("subject"));
	            	email.setSenderName(rs.getString("sender"));
	            	email.setIsHighPriority(rs.getInt("is_high_priority") == 1 ? true : false);
	            	email.setFreeText(rs.getString("free_text"));
	            	email.setBonusUserId(rs.getLong("bonus_user_id"));
	            	email.setTransactionId(rs.getLong("transaction_id"));
	            	email.setAttachmentId(rs.getLong("attachment_id"));
	            	MailBoxTemplate template = new MailBoxTemplate();
	            	template.setTypeId(rs.getLong("type_id"));
	            	template.setTemplate(rs.getString("template"));
	            	String params = rs.getString("template_parameters");
	            	String attachmentName = rs.getString("attachment_name");
	            	if (null != attachmentName) {
	            		email.setAttachmentName(attachmentName);
	            	}
			    	if (params != null) {
			    		template.setTemplate(MailBoxTemplateManagerBase.insertParameters(template.getTemplate(), params));
			    	}
	                email.setTemplate(template);
	                emails.add(email);
	            }
	        } finally {
	            closeStatement(pstmt);
	            closeResultSet(rs);
	        }
	        return emails;
	    }

	    public static void updateEmailsByUserId(Connection conn, long userId, String emails, long statusId, String utcOffset) throws SQLException {
	    	PreparedStatement pstmt = null;
	    	try {
	            String sql =
	                "UPDATE " +
	                    "mailbox_users " +
	                "SET " +
	                    "status_id = ? ";

	            if (statusId == MailBoxUser.MAILBOX_STATUS_READ) {
	            	sql +=
	            		",time_read = sysdate, " +
	            		"utc_offset_read = '" + utcOffset + "' ";
	            }
	            sql +=
	              "WHERE " +
	               	"user_id = ? AND " +
	                 "id in ( " + emails + ") ";

	            pstmt = conn.prepareStatement(sql);
	            pstmt.setLong(1, statusId);
	            pstmt.setLong(2, userId);
	            pstmt.executeUpdate();
	    	} finally {
	            closeStatement(pstmt);
	        }
	    }


	    public static String getCurrencyLettersById(Connection conn, long currencyId) throws SQLException {
	    	PreparedStatement pstmt = null;
	    	ResultSet rs = null;
	    	String currencyLetters = null;
	    	try {
	            String sql =
	                "SELECT " +
	                    "name_key " +
	                "FROM " +
	                    "CURRENCIES " +
	                "WHERE " +
	               		"id = ?  ";

	            pstmt = conn.prepareStatement(sql);
	            pstmt.setLong(1, currencyId);
	            pstmt.executeUpdate();
	            rs = pstmt.executeQuery();
	            if (rs.next()){
	            	currencyLetters = rs.getString("name_key");
	            }


	    	} finally {
	            closeStatement(pstmt);
	            closeResultSet(rs);
	        }
	    	return currencyLetters;
	    }

    public static boolean isTaxExemption(Connection con, long userId) throws SQLException {
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            String sql =
                    "SELECT " +
                        "* " +
                    "FROM " +
                        "users " +
                    "WHERE " +
                        "id = ? AND " +
                        "tax_exemption = 1";
            ps = con.prepareStatement(sql);
            ps.setLong(1,userId);
            rs = ps.executeQuery();
            if (rs.next()) {
                return true;
            }
        } finally {
            closeResultSet(rs);
            closeStatement(ps);
        }
        return false;
    }

    public static long getTaxBalance(Connection con, long userId) throws SQLException {
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            String sql = "select tax_balance from users where id=?";
            ps = con.prepareStatement(sql);
            ps.setLong(1,userId);
            rs = ps.executeQuery();
            if (rs.next()) {
                return rs.getLong("tax_balance");
            }
        } finally {
            closeResultSet(rs);
            closeStatement(ps);
        }
        return 0;
    }

    public static void updateTax(Connection con, long id, long amount) throws SQLException {
        log.debug("update tax - " + amount + " to user:" + id);
        PreparedStatement ps = null;
        try {
            String sql = "update users set tax_balance=? ,time_modified=sysdate where id=?";
            ps = con.prepareStatement(sql);
            ps.setLong(1, amount);
            ps.setLong(2, id);
            ps.executeUpdate();
        } finally {
            closeStatement(ps);
        }
    }

    /**
     * this method returns the utcoffset of a user by userId
     * @param con - DB connection
     * @param userId - the user Id
     * @return UTC_OFFSET of the user
     * @throws SQLException
     */
    public static String getUtcOffset(Connection con, long userId) throws SQLException{
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            String sql = "select utc_offset from users where id=?";
            ps = con.prepareStatement(sql);
            ps.setLong(1, userId);
            rs = ps.executeQuery();
            if (rs.next()) {
                return rs.getString("utc_offset");
            }
        } finally {
            closeResultSet(rs);
            closeStatement(ps);
        }
        return ConstantsBase.EMPTY_STRING;
    }

    public static long getCurrencyIdByUserName(Connection conn, String userName) throws SQLException {
        if (null == userName || userName.trim().equals("")) {
            return 0;
        }

        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            String sql =
                " SELECT " +
                    " u.currency_id " +
                " FROM " +
                    " users u " +
                " WHERE " +
                    " user_name = ? ";
            pstmt = conn.prepareStatement(sql);
            pstmt.setString(1, userName.toUpperCase());
            rs = pstmt.executeQuery();
            if (rs.next()) {
                return rs.getLong("currency_id");
            }
        } finally {
            closeResultSet(rs);
            closeStatement(pstmt);
        }
        return 0;
    }

    public static void updateUserDepNoInv24H(Connection con, long userId) throws SQLException {
		PreparedStatement ps = null;
		try {
			String sql = " UPDATE " +
						 "		users u" +
						 " SET " +
						 "		dep_no_inv_24h = 0 " +
				   		 " WHERE " +
				   		 "		u.id = ? ";

			ps = con.prepareStatement(sql);
			ps.setLong(1, userId);
			ps.executeUpdate();
		} finally {
    		closeStatement(ps);
    	}
	}

	public static long calculateUserRankById(Connection con, long userId) throws SQLException {
		PreparedStatement ps = null;
    	ResultSet rs = null;
		try {
			String sql =
					" SELECT " +
					"		ur.id as user_rank_id " +
			        " FROM ( " +
			        "       SELECT " +
			        "             sum((t.amount/100) * t.rate) as userAmount " +
			        "       FROM " +
			        "             transactions t, transaction_types tt " +
			        "       WHERE " +
			        "             t.user_id = ? " +
			        "             AND t.type_id = tt.id " +
			        "             AND tt.class_type = ? " +
			        "             AND t.status_id in ("+ TransactionsManagerBase.SUCCESS_DEPOSITS_TYPES +") " +
			        "       ) tab, users_rank ur " +
			        " WHERE " +
			        "		(tab.userAmount >= ur.min_sum_of_deposits AND tab.userAmount < ur.max_sum_of_deposits) " +
			        "       OR (tab.userAmount >= ur.min_sum_of_deposits AND ur.max_sum_of_deposits is null) " ;

			ps = con.prepareStatement(sql);
			ps.setLong(1, userId);
			ps.setInt(2, TransactionsManagerBase.TRANS_CLASS_DEPOSIT);
			rs = ps.executeQuery();

    		if ( rs.next() ) {
    			return rs.getLong("user_rank_id");
    		} else {
    			return 0;
    		}
		} finally {
    		closeResultSet(rs);
    		closeStatement(ps);
		}
	}

	public static void updateUserRank(Connection con, long userId, long userRank) throws SQLException {
		PreparedStatement ps = null;
		try {
			String sql = " UPDATE " +
						 "		users u " +
						 " SET " +
						 "		rank_id = ? " +
				   		 " WHERE " +
				   		 "		u.id = ? ";

			ps = con.prepareStatement(sql);
			ps.setLong(1, userRank);
			ps.setLong(2, userId);
			ps.executeUpdate();
		} finally {
    		closeStatement(ps);
    	}
	}

	public static void insertIntoUserRankHist(Connection con, long userId, long userRank, long transactionId) throws SQLException {
		PreparedStatement ps = null;
		try {
			String sql = " INSERT INTO users_rank_hist (id,QUALIFICATION_DATE, TRANSACTION_ID, USER_ID, USER_RANK_ID) " +
						 " VALUES (seq_users_rank_hist.nextval, sysdate, ?,?,?) ";
			ps = con.prepareStatement(sql);
			ps.setLong(1, transactionId);
			ps.setLong(2, userId);
			ps.setLong(3, userRank);
			ps.executeUpdate();
		} finally {
    		closeStatement(ps);
    	}
	}

	public static void updateUserStatus(Connection con, long userId, long userStatus) throws SQLException {
		PreparedStatement ps = null;
		try {
			String sql = " UPDATE " +
						 "		users u " +
						 " SET " +
						 "		status_id = ? " +
				   		 " WHERE " +
				   		 "		u.id = ? ";

			ps = con.prepareStatement(sql);
			ps.setLong(1, userStatus);
			ps.setLong(2, userId);
			ps.executeUpdate();
		} finally {
    		closeStatement(ps);
    	}
	}

	public static void insertIntoUserStatusHist(Connection con, long userId, long userStatus) throws SQLException {
		PreparedStatement ps = null;
		try {
			String sql = " INSERT INTO users_status_hist (id,QUALIFICATION_DATE,  USER_ID, USER_STATUS_ID) " +
						 " VALUES (seq_users_status_hist.nextval, sysdate, ?,?) ";
			ps = con.prepareStatement(sql);
			ps.setLong(1, userId);
			ps.setLong(2, userStatus);
			ps.executeUpdate();
		} finally {
    		closeStatement(ps);
    	}

	}

	public static boolean isNewbies(Connection con, long userId) throws SQLException {
		PreparedStatement ps = null;
    	ResultSet rs = null;
		try {
			String sql = " SELECT " +
						 "		* " +
						 " FROM ( " +
						 "		SELECT " +
						 "			 u.first_deposit_id fdi " +
						 " 		FROM " +
						 "			 users u " +
						 "		WHERE u.id = ? " +
						 "		) tab, " +
						 "		transactions t " +
						 " WHERE t.id = tab.fdi " +
						 " AND t.time_created > sysdate - 7 " ;

			ps = con.prepareStatement(sql);
			ps.setLong(1, userId);
			rs = ps.executeQuery();

    		if ( rs.next() ) {
    			return true;
    		} else {
    			return false;
    		}
		} finally {
    		closeResultSet(rs);
    		closeStatement(ps);
		}
	}


    /**
     * Get city coordinates from google.
     * On 'cities_google_coordinates' table we are saving all coordinates by city and country of the user.
     * @param con
     * @param user
     * @return
     * @throws SQLException
     */
    public static boolean getUserCityCoordinates(Connection  con, User user, String countryName) throws SQLException {
    	PreparedStatement ps = null;
    	ResultSet rs = null;
    	try {
    		String sql =" SELECT " +
    					"	 * " +
						" FROM " +
						" 	cities_google_coordinates cgc " +
						" WHERE " +
						" 	(upper(cgc.user_city_name) = upper(?) OR cgc.google_city_name = ? )" +
						" 	AND upper(cgc.user_country_name) = upper(?) " +
						" 	AND cgc.google_city_latitude is not null " +
						" 	AND cgc.google_city_longtitude is not null " +
						" ORDER BY " +
						" 	cgc.google_city_name NULLS LAST ";

    		ps = con.prepareStatement(sql);
    		ps.setString(1, user.getCityName());
    		ps.setString(2, user.getCountryName());
    		ps.setString(3, countryName);
    		rs = ps.executeQuery();
    		if (rs.next()) {
    			String googleCityName = rs.getString("GOOGLE_CITY_NAME");
    			if (CommonUtil.IsParameterEmptyOrNull(googleCityName)) {
    				return false;
    			}
    			String googleCityLatitude = rs.getString("GOOGLE_CITY_LATITUDE");
    			String googleCityLongtitude = rs.getString("GOOGLE_CITY_LONGTITUDE");
    			user.setCityFromGoogle(googleCityName);
    			user.setCityLatitude(googleCityLatitude);
    			user.setCityLongitude(googleCityLongtitude);
    			return true;
    		}
    		return false;
    	} finally {
    		closeResultSet(rs);
    		closeStatement(ps);
    	}
    }

    /**
     * Check if it's test user
     * @param con
     * @param userId
     * @return
     * @throws SQLException
     */
    public static boolean isTestUser(Connection con, long userId) throws SQLException {
   	 	PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            String sql =
            			" SELECT " +
            			"	u.class_id " +
            			" FROM " +
            			"	users u " +
            			" WHERE " +
            			"	u.id = ? ";
            pstmt = con.prepareStatement(sql);
            pstmt.setLong(1, userId);
            rs = pstmt.executeQuery();
            if (rs.next()) {
            	long classId = rs.getLong("class_id");
            	if (classId == 0) {
            		return true;
            	}
            }
        } finally {
    		closeResultSet(rs);
    		closeStatement(pstmt);
		}
    	return false;
    }

	/**
	 * @param connection
	 * @param request
	 * @return
	 * @throws SQLException
	 * @throws CryptoException 
	 * @throws NoSuchPaddingException 
	 * @throws NoSuchAlgorithmException 
	 * @throws BadPaddingException 
	 * @throws IllegalBlockSizeException 
	 * @throws InvalidKeyException 
	 * @throws InvalidAlgorithmParameterException 
	 */
	public static UserAnycapital getCapitalUser(Connection connection,
												String email)	throws SQLException, CryptoException, InvalidKeyException,
																IllegalBlockSizeException, BadPaddingException, NoSuchAlgorithmException,
																NoSuchPaddingException, InvalidAlgorithmParameterException {
		PreparedStatement pstmt = null;
        ResultSet rs = null;
        UserAnycapital user = new UserAnycapital();
        try {
            String sql =
            			"SELECT " +
            			"	u.* " +
            			"FROM " +
            			"	users u " +
            			"WHERE " +
            			"	upper(u.email) = upper(?) ";
            pstmt = connection.prepareStatement(sql);
            pstmt.setString(1, email);
            rs = pstmt.executeQuery();
            if (rs.next()) {
            	user.setUserName(rs.getString("user_name"));
            	user.setFirstName(rs.getString("first_name"));
            	user.setLastName(rs.getString("last_name"));
            	user.setTimeBirthDate(convertToDate(rs.getTimestamp("time_birth_date")));
            	String gender = rs.getString("gender");
            	Gender g = Gender.NOT_KNOWN;
            	if (!CommonUtil.isParameterEmptyOrNull(gender)) {
            		g = Gender.getByToken(gender);
            	}
            	user.setGender(g);
            	user.setEmail(rs.getString("email"));
            	user.setStreet(rs.getString("street"));
            	user.setCityName(rs.getString("city_name"));
            	user.setStreetNo(rs.getString("street_no"));
            	user.setCountryId(rs.getInt("country_id"));
            	user.setMobilePhone(rs.getString("mobile_phone"));
            	user.setLandLinePhone(rs.getString("land_line_phone"));
            	user.setPassword(AESUtil.decrypt(rs.getString("password")));
            	
            	user.setAoUserId(rs.getInt("id"));
            	user.setIsActive(rs.getInt("is_active"));
            	user.setIsContactByEmail(rs.getInt("is_contact_by_email"));
            	user.setIsContactByPhone(rs.getInt("is_contact_by_phone"));
            	user.setIsContactBySms(rs.getInt("is_contact_by_sms"));
            	user.setIsAcceptedTerms(rs.getInt("is_accepted_terms"));
            	
            	user.setLanguageId(rs.getInt("language_id"));
            }
        } finally {
    		closeResultSet(rs);
    		closeStatement(pstmt);
		}
    	return user;
	}


	/**
	 * @param connection
	 * @param userId
	 * @return
	 * @throws SQLException
	 */
	public static UserAnycapitalExtraFields getCapitalUserExtraFields (
			Connection connection, String email) throws SQLException {
		PreparedStatement pstmt = null;
        ResultSet rs = null;
        UserAnycapitalExtraFields user = new UserAnycapitalExtraFields();
        try {
            String sql =
            			"SELECT " +
            			"	u.id " +
            			"	,uad.vip_status_id " +
            			"	,CASE WHEN " +
            			"			u.class_id <> 0 " +
            			"			AND ur.approved_regulation_step = " + UserRegulationBase.REGULATION_CONTROL_APPROVED_USER +
            			"			AND ur.suspended_reason_id = " +  UserRegulationBase.NOT_SUSPENDED +
            			"		THEN 1 " +
            			"		ELSE 0 " +
            			"	END AS is_regulated " +
            			"FROM " +
            			"	users u " +
            			"	LEFT JOIN users_regulation ur ON u.id = ur.user_id " +
            			"	LEFT JOIN users_active_data uad ON u.id = uad.user_id " +
            			"WHERE " +
            			"	upper(u.email) = upper(?) ";
            pstmt = connection.prepareStatement(sql);
            pstmt.setString(1, email);
            rs = pstmt.executeQuery();
            if (rs.next()) {
            	user.setAoUserId(rs.getInt("id"));
            	user.setVipStatusId(rs.getInt("vip_status_id"));
            	user.setRegulated(rs.getBoolean("is_regulated"));
            }
        } finally {
    		closeResultSet(rs);
    		closeStatement(pstmt);
		}
    	return user;
	}
	
	public static void updateUserPassword(Connection con, long userId, String newPassword) throws SQLException {
		CallableStatement cstmt = null;
		ResultSet rs = null;
		int index = 1;
		
		try {
			cstmt = con.prepareCall("{call pkg_user.UPDATE_USER_PASS (I_USER_ID => ? " +
																     ", I_PASSWORD => ? )}");			
			cstmt.setLong(index++, userId);
			try {
				cstmt.setString(index++, AESUtil.encrypt(newPassword));
			} catch (	CryptoException | InvalidKeyException | NoSuchAlgorithmException | NoSuchPaddingException | IllegalBlockSizeException
						| BadPaddingException | UnsupportedEncodingException | InvalidAlgorithmParameterException ce) {
				throw new SQLException("CryptoException: " + ce.getMessage());
			}
			cstmt.execute();			
		} finally {
			closeStatement(cstmt);
			closeResultSet(rs);
		}		
	}
	
    public static void updateUserLocale(Connection con, long userId, long lngId) throws SQLException {
		PreparedStatement ps = null;
		try {
			String sql = " UPDATE " +
						 "		users u" +
						 " SET " +
						 "		language_id = ? " +
				   		 " WHERE " +
				   		 "		u.id = ? ";

			ps = con.prepareStatement(sql);
			ps.setLong(1, lngId);
			ps.setLong(2, userId);
			ps.executeUpdate();
		} finally {
    		closeStatement(ps);
    	}
	}
}