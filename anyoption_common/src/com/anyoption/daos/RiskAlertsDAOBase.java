package com.anyoption.daos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.anyoption.beans.RiskAlert;
import com.anyoption.managers.TransactionsManagerBase;

public class RiskAlertsDAOBase extends com.anyoption.common.daos.RiskAlertsDAOBase {
	/**
	 * Insert risk alert into Risk alert table
	 * @param con
	 * @param vo
	 * @throws SQLException
	 */
	public static void insert(Connection con, RiskAlert vo) throws SQLException {
		  PreparedStatement ps = null;
		  try {
				String sql =
					"INSERT INTO risk_alerts " +
						"(id, type_id, time_created, writer_id, transaction_id, investment_id) " +
					"VALUES" +
						"(seq_risk_alerts.NEXTVAL, ?, sysdate, ?, ?, ?) ";

				ps = con.prepareStatement(sql);
				ps.setLong(1, vo.getTypeId());
				ps.setLong(2, vo.getWriterId());
				if (vo.getTransactionId() > 0) {
					ps.setLong(3, vo.getTransactionId());
				} else {
					ps.setString(3, null);
				}
				if (vo.getInvestmentId() > 0) {
					ps.setLong(4, vo.getInvestmentId());
				} else {
					ps.setString(4, null);
				}
				ps.executeUpdate();
				vo.setId(getSeqCurValue(con, "seq_risk_alerts"));
		  } finally	{
				closeStatement(ps);
		  }
	  }

    /**
     * Get number of deposit for user
	 * @param con
	 * @param userId
	 * @return
	 * @throws SQLException
	 */
	public static long getNumberOfDeposit(Connection con, long userId) throws SQLException{
	        PreparedStatement pstmt = null;
	        ResultSet rs = null;
	        long numDeposit = 0;

	        try {
	        	String sql ="SELECT " +
	        					"count(*) num_dep " +
	        				"FROM " +
	        					"transactions t, transaction_types tt " +
	        				"WHERE " +
	        					"t.type_id = tt.id " +
	        					"AND t.type_id = ? " +
	        					"AND t.status_id IN (?,?) " +
	        					"AND t.user_id = ?";

	        	pstmt = con.prepareStatement(sql);
	        	pstmt.setLong(1, TransactionsManagerBase.TRANS_TYPE_CC_DEPOSIT);
	        	pstmt.setLong(2, TransactionsManagerBase.TRANS_STATUS_SUCCEED);
	        	pstmt.setLong(3, TransactionsManagerBase.TRANS_STATUS_PENDING);
	        	pstmt.setLong(4, userId);
	        	rs = pstmt.executeQuery();
	        	if (rs.next()){
	        		numDeposit = rs.getLong("num_dep");
	        		return numDeposit;
	        	}
	        } finally {
	            closeResultSet(rs);
	            closeStatement(pstmt);
	        }
	       return numDeposit;
	  }



}
