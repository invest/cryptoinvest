package com.anyoption.daos;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;

import com.anyoption.beans.Contact;
import com.anyoption.beans.User;
import com.anyoption.common.beans.base.Skin;
import com.anyoption.common.daos.DAOBase;
import com.anyoption.common.util.CommonUtil;

import oracle.jdbc.OracleTypes;

public class ContactsDAOBase extends DAOBase {
    /**
     * checks if there's a contact with the same phone number as the new user.
     * @param connection
     * @param user
     * @throws SQLException
     */
    public static Contact searchFreeContactByUserPhone(Connection conn, User user) throws SQLException {
    	PreparedStatement ps = null;
		ResultSet rs = null;
		int x = 1;

    	try {

    		String sql = " SELECT " +
    					 "		* " +
    					 " FROM " +
    					 "		contacts " +
    					 " WHERE " +
    					 "		user_id = 0 " +
    					 "		AND id = MARKETING.GET_FREE_CONTACT_DETAILS(?, ?, ?, ?, ?) ";

				ps = conn.prepareStatement(sql);
				ps.setLong(x++, user.getCountryId());
				ps.setString(x++, user.getLandLinePhone());
				ps.setString(x++, user.getMobilePhone());
				ps.setString(x++, user.getEmail());
				ps.setLong(x++, user.getSkinId());

			rs = ps.executeQuery();

        	if (rs.next()){
        		Contact vo = getVO(rs);
				return vo;
        	}
        } finally {
        	closeResultSet(rs);
			closeStatement(ps);
        }
        return null;
    }

    /**
     * checks if there's a contact with the same phone number as the new user.
     *
     * @param connection
     * @param user
     * @throws SQLException
     */
    public static Contact getVO(ResultSet rs) throws SQLException {
        Contact vo = new Contact();

		vo.setId(rs.getLong("id"));
		vo.setText(rs.getString("name"));
		vo.setType(rs.getLong("type"));
		vo.setPhone(rs.getString("phone"));
		vo.setUserId(rs.getLong("user_id"));
		vo.setEmail(rs.getString("email"));
		vo.setSkinId(rs.getLong("skin_id"));
		vo.setCountryId(rs.getLong("country_id"));
		vo.setWriterId(rs.getLong("writer_id"));
		vo.setCombId(rs.getLong("combination_id"));
		vo.setDynamicParameter(rs.getString("dynamic_parameter"));
		vo.setAff_sub1(rs.getString("aff_sub1"));
		vo.setAff_sub1(rs.getString("aff_sub2"));
		vo.setAff_sub1(rs.getString("aff_sub3"));
		vo.setUtcOffset(rs.getString("utc_offset"));
		vo.setTimeCreated(convertToDate(rs.getTimestamp("time_created")));
        vo.setIp(rs.getString("ip"));
        vo.setFirstName(rs.getString("first_name"));
        vo.setLastName(rs.getString("last_name"));
        vo.setMobilePhone(rs.getString("mobile_phone"));
        vo.setLandLinePhone(rs.getString("land_line_phone"));
        vo.setContactByEmail(rs.getInt("IS_CONTACT_BY_EMAIL")==1?true:false);
        vo.setContactBySMS(rs.getInt("IS_CONTACT_BY_SMS")==1?true:false);
        vo.setDeviceUniqueId(rs.getString("DEVICE_UNIQUE_ID"));
	
        return vo;
    }

    public static void updateContactRequest(Connection conn, long contactId, long userId) throws SQLException {
        PreparedStatement pstmt = null;
        try {
            String sql =
                "UPDATE " +
                    "contacts " +
                "SET " +
                    "user_id = ?, " +
                    "time_updated = SYSDATE " +
                "WHERE " +
                    "id = ?";
            pstmt = conn.prepareStatement(sql);
            pstmt.setLong(1, userId);
            pstmt.setLong(2, contactId);
            pstmt.executeUpdate();
        } finally {
            closeStatement(pstmt);
        }
    }

	  /**
	   * Is contact exists, if yes return contactId
	   * @param con db connection
	   * @param phone phone number of existing check
	   * @param email email of existing check
	   * @return
	   * @throws SQLException
	   */
	  public static long isExists(Connection con, String phone, String email) throws SQLException {
		  PreparedStatement ps = null;
		  ResultSet rs = null;

		  try {
			  String sql =
				  " SELECT " +
			  	  	" id " +
			  	  " FROM " +
		   	 	  	" contacts " +
			      " WHERE " +
			   	  	" phone like ? " +
			   	  	" OR mobile_phone like ? " +
			   	  	" OR land_line_phone like ? " +
			   	  	" OR email like ? ";

			  ps = con.prepareStatement(sql);
			  ps.setString(1, phone);
			  ps.setString(2, phone);
			  ps.setString(3, phone);
			  ps.setString(4, email);
			  rs = ps.executeQuery();

			  if(rs.next()) {
				  return rs.getLong("id");
			  }

		  } finally {
			  closeResultSet(rs);
			  closeStatement(ps);
		  }
		  return 0;
	  }

	  public static void insertContactRequest(Connection con, com.anyoption.beans.base.Contact vo) throws SQLException {
		  PreparedStatement ps = null;
		  try {
			  String sql = "INSERT INTO CONTACTS(" +
					  			"id," +
					  			"name," +
					  			"type," +
					  			"phone," +
					  			"user_id," +
					  			"skin_id," +
					  			"country_id," +
					  			"email," +
					  			"time_created," +
					  			"writer_id," +
					  			"time_updated," +
					  			"combination_id," +
					  			"dynamic_parameter," +
					  			"utc_offset," +
					  			"ip," +
					  			"phone_type, " +
					  			"user_agent," +
					  			"dfa_placement_id," +
					  			"dfa_creative_id," +
					  			"dfa_macro," +
					  			"first_name, " +
					  			"last_name, " +
					  			"mobile_phone, " +
					  			"land_line_phone, " +
					  			"is_contact_by_email, " +
					  			"is_contact_by_sms, " +
					  			"device_unique_id, " +
					  			"http_referer, " +
					  			"affiliate_key,  " +
					  			"aff_sub1, " +
					  			"aff_sub2, " +
					  			"aff_sub3 " +
					  			") " +
			  			"VALUES(?,?,?,?,?,?,?,?,SYSDATE,?,SYSDATE,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) ";

			  ps = con.prepareStatement(sql);
			  ps.setLong(1, vo.getId());
			  ps.setString(2,vo.getName());
			  ps.setLong(3,vo.getType());
			  ps.setString(4, vo.getPhone());
			  ps.setLong(5, vo.getUserId());
			  ps.setLong(6, vo.getSkinId());
			  ps.setLong(7,vo.getCountryId());
			  ps.setString(8, vo.getEmail());
			  ps.setLong(9, vo.getWriterId());
			  ps.setLong(10, vo.getCombId());
			  ps.setString(11, vo.getDynamicParameter());
			  ps.setString(12, vo.getUtcOffset());
              ps.setString(13, vo.getIp());
              ps.setString(14, vo.getPhoneType());
              ps.setString(15, vo.getUserAgent());
              ps.setString(16, vo.getDfaPlacementId());
              ps.setString(17, vo.getDfaCreativeId());
              ps.setString(18, vo.getDfaMacro());
              ps.setString(19, CommonUtil.capitalizeFirstLetters(vo.getFirstName()));
              ps.setString(20, CommonUtil.capitalizeFirstLetters(vo.getLastName()));
              ps.setString(21, vo.getMobilePhone());
              ps.setString(22, vo.getLandLinePhone());
              ps.setInt(23, vo.isContactByEmail()? 1 : 0);
              ps.setInt(24, vo.isContactBySMS() ? 1 : 0);
              ps.setString(25, vo.getDeviceUniqueId());
              ps.setString(26, vo.getHttpReferer());
              if (vo.getAffiliateKey() == 0) {
            	  ps.setNull(27, Types.NUMERIC);
              } else {
            	  ps.setLong(27, vo.getAffiliateKey());
              }
              ps.setString(28, vo.getAff_sub1());
              ps.setString(29, vo.getAff_sub2());
              ps.setString(30, vo.getAff_sub3());
			  ps.executeUpdate();
		  }	finally	{
				closeStatement(ps);
		  }
	  }
	  
	  public static void insertContact(Connection con, com.anyoption.beans.base.Contact vo) throws SQLException {
			CallableStatement cstmt = null;
			int index = 1;
		  try {
			  
				cstmt = con.prepareCall("{call PKG_USER.INSERT_CONTACT(I_NAME => ?, " +
																	   "I_TYPE => ?, " +
																	   "I_PHONE => ?, " +
																	   "I_USER_ID => ?, " +
																	   "I_SKIN_ID => ?, " +
																	   "I_COUNTRY_ID => ?, " +
																	   "I_EMAIL => ?, " +
																	   "I_WRITER_ID => ?, " +
																	   "I_COMBINATION_ID => ?, " +
																	   "I_DYNAMIC_PARAMETER => ?, " +
																	   "I_UTC_OFFSET => ?, " +
																	   "I_IP => ?, " +
																	   "I_PHONE_TYPE => ?, " +
																	   "I_USER_AGENT => ?, " +
																	   "I_DFA_PLACEMENT_ID => ?, " +
																	   "I_DFA_CREATIVE_ID => ?, " +
																	   "I_DFA_MACRO => ?, " +
																	   "I_FIRST_NAME => ?, " +
																	   "I_LAST_NAME => ?, " +
																	   "I_MOBILE_PHONE => ?, " +
																	   "I_LAND_LINE_PHONE => ?, " +
																	   "I_IS_CONTACT_BY_EMAIL => ?, " +
																	   "I_IS_CONTACT_BY_SMS => ?, " +
																	   "I_DEVICE_UNIQUE_ID => ?, " +
																	   "I_HTTP_REFERER => ?, " +
																	   "I_AFFILIATE_KEY => ?, " +
																	   "I_AFF_SUB1 => ?, " +
																	   "I_AFF_SUB2 => ?, " +
																	   "I_AFF_SUB3 => ? )}");

				setStatementValue(vo.getName(), index++, cstmt);
				cstmt.setLong(index++, vo.getType());
				setStatementValue(vo.getPhone(), index++, cstmt);
				cstmt.setLong(index++, vo.getUserId());
				cstmt.setLong(index++, vo.getSkinId());
				cstmt.setLong(index++, vo.getCountryId());
				setStatementValue(vo.getEmail(), index++, cstmt);
				cstmt.setLong(index++, vo.getWriterId());
				setStatementValue(vo.getCombId(), index++, cstmt);
				setStatementValue(vo.getDynamicParameter(), index++, cstmt);
				cstmt.setString(index++, vo.getUtcOffset());// mandatory in contacts table
				setStatementValue(vo.getIp(), index++, cstmt);
				setStatementValue(vo.getPhoneType(), index++, cstmt);
				setStatementValue(vo.getUserAgent(), index++, cstmt);
				
				setStatementValue(vo.getDfaPlacementId(), index++, cstmt);
				setStatementValue(vo.getDfaCreativeId(), index++, cstmt);
				setStatementValue(vo.getDfaMacro(), index++, cstmt);
				setStatementValue(CommonUtil.capitalizeFirstLetters(vo.getFirstName()), index++, cstmt);
				setStatementValue(CommonUtil.capitalizeFirstLetters(vo.getLastName()), index++, cstmt);
				setStatementValue(vo.getMobilePhone(), index++, cstmt);
				setStatementValue(vo.getLandLinePhone(), index++, cstmt);
				setStatementValue(vo.isContactByEmail(), index++, cstmt);
				
				setStatementValue(vo.isContactBySMS(), index++, cstmt);
				setStatementValue(vo.getDeviceUniqueId(), index++, cstmt);
				setStatementValue(vo.getHttpReferer(), index++, cstmt);
				
				if (vo.getAffiliateKey() == 0) {
					cstmt.setNull(index++, OracleTypes.NUMBER);
				} else {
					cstmt.setLong(index++, vo.getAffiliateKey());
				}
				setStatementValue(vo.getAff_sub1(), index++, cstmt);
				setStatementValue(vo.getAff_sub2(), index++, cstmt);
				setStatementValue(vo.getAff_sub3(), index++, cstmt);

				cstmt.execute();
		  }	finally	{
				closeStatement(cstmt);
		  }
	  }
	  
	/**
	 * Update contact users
	 * @param con
	 * @param vo
	 * @throws SQLException
	 */
	public static void updateContact(Connection con,Contact vo) throws SQLException {
	        PreparedStatement pstmt = null;
	        try {
	            String sql =
	                    "UPDATE " +
	                        " contacts " +
	                    "SET " +
		                    " time_updated = sysdate, " +
		                	" utc_offset = ?, " +
		                	" skin_id = ?, " +
		                	" first_name = ?, " +
		                	" last_name = ?, " +
		                	" email = ?, " +
		                	" country_id = ?, " +
		                	" mobile_phone = ?, " +
		                	" land_line_phone = ?, " +
		                	" ip = ?, " +
		                	" user_agent = ?, " +
		                	" writer_id = ?, " +
		                	" device_unique_id = ?, " +
		                	" user_id = ?, " +
	                    	" is_contact_by_email = ?, " +
	                    	" is_contact_by_sms = ?, " +
	                    	" combination_id = ?, " +
	                    	" dfa_placement_id = ?, " +
	                    	" dfa_creative_id = ?, " +
	                    	" dfa_macro = ?, " +
	                    	" dynamic_parameter = ?, " +
	                    	" http_referer = ?, " +
	                    	" aff_sub1 = ?, " +
	                    	" aff_sub2 = ?, " +
	                    	" aff_sub3 = ? " +
	                    "WHERE " +
	                        " id = ? ";
	            pstmt = con.prepareStatement(sql);
	            pstmt.setString(1, vo.getUtcOffset());
	            pstmt.setLong(2, vo.getSkinId());
	            pstmt.setString(3, CommonUtil.capitalizeFirstLetters(vo.getFirstName()));
	            pstmt.setString(4, CommonUtil.capitalizeFirstLetters(vo.getLastName()));
	            pstmt.setString(5, vo.getEmail());
	            pstmt.setLong(6, vo.getCountryId());
	            pstmt.setString(7, vo.getMobilePhone());
	            pstmt.setString(8, vo.getLandLinePhone());
	            pstmt.setString(9, vo.getIp());
	            pstmt.setString(10, vo.getUserAgent());
	            pstmt.setLong(11, vo.getWriterId());
	            pstmt.setString(12, vo.getDeviceUniqueId());
	            pstmt.setLong(13, vo.getUserId());
	            pstmt.setInt(14, vo.isContactByEmail() ? 1 : 0);
	            pstmt.setInt(15, vo.isContactBySMS() ? 1 : 0);
	            pstmt.setLong(16, vo.getCombId());
	            pstmt.setString(17, vo.getDfaPlacementId());
	            pstmt.setString(18, vo.getDfaCreativeId());
	            pstmt.setString(19, vo.getDfaMacro());
	            pstmt.setString(20, vo.getDynamicParameter());
	            pstmt.setString(21, vo.getHttpReferer());
	            pstmt.setLong(22, vo.getId());
	            pstmt.setString(23, vo.getAff_sub1());
	            pstmt.setString(24, vo.getAff_sub2());
	            pstmt.setString(25, vo.getAff_sub3());
	            
	            pstmt.executeUpdate();
	        } finally {
	            closeStatement(pstmt);
	        }

	  }
	
	/**
     * checks if there's a user with the same email/mobile in order to connect contact with user
     * @param connection
     * @param user
     * @throws SQLException
     */
    public static long searchUserIdFromContactDetails(Connection con, Contact contact) throws SQLException {
    	PreparedStatement ps = null;
		ResultSet rs = null;
		long contactId = 0;
		int index = 1;
    	try {
    		String sql = " SELECT " +
    						" u.id, " +
    					    " CASE " + //email is more "powerfull" than mobile in etrader.
    				        	" WHEN u.email = ?  and  u.mobile_phone = ? THEN 1 " +
    				            " WHEN u.email = ?   THEN 2 " +
    				            " ELSE 3 " +
    						" END " +
    					 " FROM " +
    					 	" users u " +
    					 " WHERE ";
               if (contact.getSkinId() == Skin.SKIN_ETRADER){
            	   sql+=	" upper(u.email) = upper(?) " +
    						" OR u.mobile_phone = ? ";
               } else {// on anyoption we have duplicate email validation so we need to check only mobile phone
            	   sql+=	" u.mobile_phone = ? ";
               }
	               sql+=" ORDER by " +
							    " CASE " + //email is more "powerfull" than mobile in etrader.
						        	" WHEN u.email = ?  and  u.mobile_phone = ? THEN 1 " +
						            " WHEN u.email = ?   THEN 2 " +
						            " ELSE 3 " +
						        " END ";
				ps = con.prepareStatement(sql);
				ps.setString(index++, contact.getEmail());
				ps.setString(index++, contact.getMobilePhone());
				ps.setString(index++, contact.getEmail());
				if (contact.getSkinId() == Skin.SKIN_ETRADER){
					ps.setString(index++, contact.getEmail());
				}
				ps.setString(index++, contact.getMobilePhone());
				ps.setString(index++, contact.getEmail());
				ps.setString(index++, contact.getMobilePhone());
				ps.setString(index++, contact.getEmail());
			rs = ps.executeQuery();
        	if (rs.next()){
        		contactId = rs.getLong("id");
				return contactId;
        	}
        } finally {
        	closeResultSet(rs);
			closeStatement(ps);
        }
        return contactId;
    }
    
    
	/**
     * Get contact by device unique Id (for register attempt purpose)  
     * @param connection
     * @param user
     * @throws SQLException
     */
    public static Contact findContactByDeviceUniqueId(Connection con, String duId, long contactType) throws SQLException{
    	PreparedStatement ps = null;
		ResultSet rs = null;
		Contact c = null;
		try {
    		String sql = " SELECT " +
    						" * " +
    					 " FROM " +
    					 	" contacts c " +
    					 " WHERE " +
    					 	" c.device_unique_id = ? " +
    					 	" AND c.type = ? " +
    					 " ORDER BY " +
    					 	" c.id desc " ;
    		
    		ps = con.prepareStatement(sql);
			ps.setString(1, duId);
			ps.setLong(2, contactType);
			rs = ps.executeQuery(); 
			if (rs.next()){
        		c = getVO(rs);
        	}
		} finally {
	       	closeResultSet(rs);
			closeStatement(ps);
	    }
	    return c;	
    }
    
	/**
     * Get contact by id
     * @param connection
     * @param user
     * @throws SQLException
     */
    public static Contact getContactById(Connection con, long id) throws SQLException{
    	PreparedStatement ps = null;
		ResultSet rs = null;
		Contact c = null;
		try {
    		String sql = " SELECT " +
    						" * " +
    					 " FROM " +
    					 	" contacts c " +
    					 " WHERE " +
    					 	" c.id = ? ";
    		
    		ps = con.prepareStatement(sql);
			ps.setLong(1, id);
			rs = ps.executeQuery(); 
			if (rs.next()){
        		c = getVO(rs);
        	}
		} finally {
	       	closeResultSet(rs);
			closeStatement(ps);
	    }
	    return c;	
    }
    
}