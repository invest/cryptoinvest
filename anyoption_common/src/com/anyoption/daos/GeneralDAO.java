package com.anyoption.daos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.anyoption.common.daos.DAOBase;
//import java.util.ArrayList;
//import com.anyoption.beans.Enumerator; //TODO: remove it after 2 deploys

public class GeneralDAO extends DAOBase {
	//TODO: remove it after 2 deploys
    /*public static ArrayList<Enumerator> getAllEnumerators(Connection conn) throws SQLException {
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        ArrayList<Enumerator> list = new ArrayList<Enumerator>();
        try {
            String sql =
                "SELECT " +
                    "* " +
                "FROM " +
                    "enumerators";
            pstmt = conn.prepareStatement(sql);
            rs = pstmt.executeQuery();
            while (rs.next()) {
                list.add(getEnumeratorVO(rs));
            }
        } finally {
            closeResultSet(rs);
            closeStatement(pstmt);
        }
        return list;
    }

    private static Enumerator getEnumeratorVO(ResultSet rs) throws SQLException{
        Enumerator vo = new Enumerator();
        vo.setId(rs.getLong("id"));
        vo.setEnumerator(rs.getString("enumerator"));
        vo.setCode(rs.getString("code"));
        vo.setDescription(rs.getString("description"));
        vo.setValue(rs.getString("value"));
        return vo;
    }*/
    
    
    public static void insertBalanceLog(Connection con,long writerId,long userId,String table,long key,int command, String utcoffset) throws SQLException {

		  PreparedStatement ps=null;

		  try
			{
			    String sql="insert into balance_history (ID,TIME_CREATED,user_id,balance,tax_balance,WRITER_ID," +
			    		"TABLE_NAME,KEY_VALUE,COMMAND, utc_offset) "+
			    	"values (seq_balance_history.nextval,sysdate,?,(select balance from users where id=?)," +
			    	"(select tax_balance from users where id=?),?,?,?,?,?) ";

				ps = con.prepareStatement(sql);

				ps.setLong(1, userId);
				ps.setLong(2, userId);
				ps.setLong(3, userId);
				ps.setLong(4, writerId);
				ps.setString(5, table);
				ps.setLong(6, key);
				ps.setInt(7, command);
				ps.setString(8, utcoffset);

				ps.executeUpdate();

			}

			finally
			{
				closeStatement(ps);
			}

	  }
    
    public static void getDB(Connection conn) throws SQLException {
    	PreparedStatement ps=null;
		  ResultSet rs=null;

		  try
			{
			  String sql=" select 1 from dual ";

				ps = conn.prepareStatement(sql);

				rs=ps.executeQuery();

			}

			finally
			{
				closeResultSet(rs);
				closeStatement(ps);
			}
    }
    
}