package com.anyoption.daos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.anyoption.common.beans.SkinLanguage;
import com.anyoption.common.daos.DAOBase;


/**
 * SkinLanguages DAO Base class.
 *
 * @author Kobi
 */
public class SkinLanguagesDAOBase extends DAOBase {

	public static ArrayList<SkinLanguage> getAllBySkin(Connection con, long skinId) throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		ArrayList<SkinLanguage> list = new ArrayList<SkinLanguage>();
		try {
			String sql =
				"SELECT " +
					"s.*, " +
					"l.display_name " +
				 "FROM " +
				 	"skin_languages s, " +
				 	"languages l " +
				 "WHERE " +
				 	"s.language_id = l.id AND " +
				 	"s.skin_id = ? ";

			ps = con.prepareStatement(sql);
			ps.setLong(1, skinId);
			rs = ps.executeQuery();

			while( rs.next() ) {
				list.add(getVO(rs));
			}
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		return list;
	}


	/**
	 * Fill SkinLanguage object
	 * @param rs
	 * 		ResultSet
	 * @return
	 * 		SkinLanguage object
	 * @throws SQLException
	 */
	protected static SkinLanguage getVO(ResultSet rs) throws SQLException {
		SkinLanguage vo = new SkinLanguage();
		vo.setId(rs.getLong("id"));
		vo.setSkinId(rs.getLong("skin_id"));
		vo.setLanguageId(rs.getLong("language_id"));
		vo.setDisplayName(rs.getString("display_name"));
		return vo;
	}



}