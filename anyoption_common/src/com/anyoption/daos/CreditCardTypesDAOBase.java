package com.anyoption.daos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;

import com.anyoption.common.beans.base.CreditCardType;
import com.anyoption.common.daos.DAOBase;

public class CreditCardTypesDAOBase extends DAOBase {
    public static CreditCardType getById(Connection conn, long id) throws SQLException {
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        CreditCardType vo = null;
        try {
            String sql =
                "SELECT " +
                    "* " +
                "FROM " +
                    "credit_card_types " +
                "WHERE " +
                    "id = ?";
            pstmt = conn.prepareStatement(sql);
            pstmt.setLong(1, id);
            rs = pstmt.executeQuery();
            if (rs.next()) {
                vo = new CreditCardType();
                vo.setId(rs.getLong("id"));
                vo.setName(rs.getString("name"));
            }
        } finally {
            closeResultSet(rs);
            closeStatement(pstmt);
        }
        return vo;
    }

	public static HashMap<Long, CreditCardType> getAll(Connection con) throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		HashMap<Long, CreditCardType> hp = new HashMap<Long, CreditCardType>();
		try {
		    String sql =
		    	"SELECT " +
		    		"* " +
		    	"FROM " +
		    		"credit_card_types " +
		    	"ORDER BY " +
		    		"name";

			ps = con.prepareStatement(sql);
			rs = ps.executeQuery();

			while (rs.next()) {
				CreditCardType c = new CreditCardType();
				c.setId(rs.getLong("id"));
				c.setName(rs.getString("name"));
				hp.put(c.getId(), c);
			}
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		return hp;
	  }
}