package com.anyoption.daos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.anyoption.beans.MarketingCombination;
import com.anyoption.common.daos.DAOBase;

public class MarketingCombinationsDAOBase extends DAOBase {
    /**
     * Get by id
     * @param con
     * @param id
     * @return
     * @throws SQLException
     */
    public static MarketingCombination getById(Connection con, long id) throws SQLException {
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        MarketingCombination vo = null;
        try {
            String sql =
                "SELECT " +
                    "mco.*, " +
                    "mca.name as campaign_name  " +
                "FROM " +
                    "marketing_combinations mco, " +
                    "marketing_campaigns mca " +
                "WHERE " +
                    "mco.id = ? " +
                    "AND mco.campaign_id = mca.id";
            pstmt = con.prepareStatement(sql);
            pstmt.setLong(1, id);
            rs = pstmt.executeQuery();
            if (rs.next()) {
                vo = getVO(rs);
                vo.setCampaignName(rs.getString("campaign_name"));
            }
        } finally {
            closeResultSet(rs);
            closeStatement(pstmt);
        }
        return vo;
    }

    /**
     * Get VO
     * 
     * @param rs Result set instance
     * @return MarketingCombination object
     * @throws SQLException
     */
    protected static MarketingCombination getVO(ResultSet rs) throws SQLException {
        MarketingCombination vo = new MarketingCombination();
        vo.setId(rs.getLong("id"));
        vo.setSkinId(rs.getLong("skin_id"));
        vo.setCampaignId(rs.getLong("campaign_id"));
        vo.setMediumId(rs.getLong("medium_id"));
        vo.setContentId(rs.getLong("content_id"));
        vo.setSizeId(rs.getLong("size_id"));
        vo.setTypeId(rs.getLong("type_id"));
        vo.setLocationId(rs.getLong("location_id"));
        vo.setLandingPageId(rs.getLong("landing_page_id"));
        vo.setWriterId(rs.getLong("writer_id"));
        vo.setTimeCreated(convertToDate(rs.getTimestamp("time_created")));
        return vo;
    }

    /**
	   * Get combination instance by id
	   * @param con db connection
	   * @param combId combination id
	   * @return  combination instance
	   * @throws SQLException
	   */
	   public static MarketingCombination getCombinationById(Connection con, long combId) throws SQLException {

	   	  PreparedStatement ps = null;
		  ResultSet rs = null;
		  MarketingCombination com = new MarketingCombination();

		  try {
			    String sql = "SELECT " +
			    			 	"mc.*, " +
			    			 	"mca.payment_type_id, " +
			    			 	"mca.name as campaign_name " +
			    			 "FROM " +
			    			 	"marketing_combinations mc, " +
			    			 	"marketing_campaigns mca, " +
			    			 	"marketing_payment_types mpt " +
			    			 "WHERE " +
			    			 	"mc.campaign_id = mca.id AND " +
			    			 	"mca.payment_type_id = mpt.id " +
			    			 	"AND mc.id = ? ";

				ps = con.prepareStatement(sql);
				ps.setLong(1, combId);

				rs = ps.executeQuery();

				if (rs.next()) {
					com = getVO(rs);
					com.setPaymentType(rs.getLong("payment_type_id"));
					com.setCampaignName(rs.getString("campaign_name"));
					com.setCombPixels(MarketingPixelsDAOBase.getPixelsByCombId(con, com.getId()));
				}
			}
			finally {
				closeResultSet(rs);
				closeStatement(ps);
			}
			return com;
	   }
}