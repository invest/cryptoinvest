//package com.anyoption.daos;
//
//import java.sql.Connection;
//import java.sql.PreparedStatement;
//import java.sql.ResultSet;
//import java.sql.SQLException;
//import java.sql.Statement;
//import java.sql.Timestamp;
//import java.text.ParseException;
//import java.text.SimpleDateFormat;
//import java.util.Date;
//
//import org.apache.log4j.Logger;
//
//public class DAOBase {
//    private static final Logger logger = Logger.getLogger(DAOBase.class);
//
//    public static String ORDER_ASC = "asc";
//    public static String ORDER_DESC = "desc";
//
//    protected static void updateQuery(Connection conn, String sql) throws SQLException {
//        Statement st = null;
//        try {
//            st = conn.createStatement();
//            st.executeUpdate(sql);
//        } finally {
//            closeStatement(st);
//        }
//    }
//
//    protected static long getSeqCurValue(Connection con, String seq) throws SQLException {
//        PreparedStatement ps = null;
//        ResultSet rs = null;
//        try {
//            ps = con.prepareStatement("select " + seq + ".currval from dual");
//            rs = ps.executeQuery();
//            if (rs.next()) {
//                return rs.getLong(1);
//            }
//        } finally {
//            closeResultSet(rs);
//            closeStatement(ps);
//        }
//        return 0;
//    }
//
//	  public static long getSequenceNextVal(Connection con,String seq) throws SQLException {
//		  PreparedStatement ps=null;
//		  ResultSet rs=null;
//
//		  try {
//			  String sql = "SELECT " + seq + ".nextval from dual";
//
//			  ps = con.prepareStatement(sql);
//			  rs = ps.executeQuery();
//				if (rs.next()) {
//					return rs.getLong(1);
//				}
//			} finally {
//				closeResultSet(rs);
//				closeStatement(ps);
//			}
//			return 0;
//	  }
//
//
//    public static void closeStatement(Statement stmt) {
//        if (stmt != null) {
//            try {
//                stmt.close();
//            } catch (SQLException ex) {
//                logger.error(stmt, ex);
//            }
//        }
//    }
//
//    public static void closeResultSet(ResultSet rs) {
//        if (rs != null) {
//            try {
//                rs.close();
//            } catch (SQLException ex) {
//                logger.error(rs, ex);
//            }
//        }
//    }
//
//    public static Date convertToDate(Timestamp t) {
//        if (null == t) {
//            return null;
//        }
//        return new Date(t.getTime());
//    }
//
//    public static Timestamp convertToTimestamp(Date d) {
//        if (null == d) {
//            return null;
//        }
//        return new Timestamp(d.getTime());
//    }
//
//    public static Date getTimeWithTimezone(String time) {
//        Date d = null;
//        if (null != time) {
//            SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS Z");
//            try {
//                d = df.parse(time);
//            } catch (ParseException pe) {
//                logger.error("Can't parse time with timezone.", pe);
//            }
//        }
//        return d;
//    }
//}