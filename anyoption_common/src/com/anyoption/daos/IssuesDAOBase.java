package com.anyoption.daos;

import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import org.bouncycastle.crypto.CryptoException;

import com.anyoption.common.beans.IssueAction;
import com.anyoption.common.bl_vos.PopulationEntryBase;
import com.anyoption.common.util.AESUtil;
import com.anyoption.managers.IssuesManagerBase;

/**
 * IssuesDAOBase class
 * @author Kobi
 *
 */
public class IssuesDAOBase extends com.anyoption.common.daos.IssuesDAOBase {

	  /**
	   * get last action by issue id
	   * @param con db connection
	   * @param issueId the issue id of the action
	   * @return IssueAction instance
	   * @throws SQLException
	   */
	  public static IssueAction getActionById(Connection con,long actionId) throws SQLException {

		  PreparedStatement ps = null;
		  ResultSet rs = null;

		  IssueAction vo = null;

		  try {
			  String sql =
				  " SELECT " +
				  		" ia.*, " +
				  		" iat.channel_id, " +
				  		" iat.reached_status_id " +
		  		  " FROM " +
		  		  		" issue_actions ia, " +
		  		  		" issue_action_types iat " +
				  " WHERE " +
				  		" id = ? " +
				  		" and iat.id = ia.issue_action_type_id ";


				ps = con.prepareStatement(sql);
				ps.setLong(1, actionId);

				rs = ps.executeQuery();

				if (rs.next()) {
					vo = getActionVO(rs);
					vo.setIssueId(rs.getLong("issue_id"));
					vo.setActionTime(convertToDate(rs.getTimestamp("action_time")));
				}
			} finally {
				closeResultSet(rs);
				closeStatement(ps);
			}
			return vo;
	  }

	  /**
	   * Vo for action
	   * @param rs
	   * @return
	   * @throws SQLException
	   */
	  protected static IssueAction getActionVO(ResultSet rs) throws SQLException {
			IssueAction vo = new IssueAction();
			vo.setActionId(rs.getLong("id"));
			vo.setWriterId(rs.getLong("writer_id"));
			vo.setChannelId(rs.getString("channel_id"));
			vo.setCallDirectionId(rs.getLong("direction_id"));
			vo.setReachedStatusId(rs.getString("reached_status_id"));
			vo.setActionTypeId(rs.getString("issue_action_type_id"));
			vo.setIssueId(rs.getLong("issue_id"));
			vo.setActionTime(convertToDate(rs.getTimestamp("action_time")));
			return vo;
	  }
	  
	  /**
       * Insert Document request
       * @param con
       * @param ccId
       * @throws SQLException
       */
      public static void insertDocRequest(Connection con, long issueActionId, long ccId, String type, long userId) throws SQLException {
            PreparedStatement ps = null;
            ResultSet rs = null;

            try {
                  String sql =" INSERT INTO " +
                                  " issue_risk_doc_req(ID, ISSUE_ACTION_ID, DOC_TYPE, CC_ID) " +
                              " VALUES " +
                                  " (seq_issue_risk_doc_req.nextval,?,?,?) ";

                  ps = con.prepareStatement(sql);
                  ps.setLong(1, issueActionId);
                  ps.setString(2, type);
                  if (ccId != 0){
                      ps.setLong(3, ccId);
                  } else {
                      ps.setString(3, null);
                  }

                  ps.executeUpdate();
            } finally {
                  closeStatement(ps);
                  closeResultSet(rs);
              }
            //files request
            ps = null;
            rs = null;

            try {
                  String sql =" INSERT INTO " +
                          " files " +
                              " (id, user_id, file_type_id, writer_id, time_created, utc_offset_created,cc_id, is_approved, file_status_id,  issue_action_id) " + 
                          " VALUES " +
                              " (seq_files.nextval, ?, ?, 200, sysdate, 'GMT+02:00', ?, 0, 1, ?) ";
                  ps = con.prepareStatement(sql);

                  ps.setLong(1, userId);
                  if (type.equals("2")) {
                      ps.setString(2, "4");
                  } else {
                      ps.setString(2, "18");
                  }
                  if (ccId != 0){
                      ps.setLong(3, ccId);
                  } else {
                      ps.setString(3, null);
                  }
                  ps.setLong(4, issueActionId);
                  
                  ps.executeUpdate();
            } finally {
                  closeStatement(ps);
                  closeResultSet(rs);
              }
            
      }
      
      public static void updateDocState(Connection con, long userId, long ccId, long docState) throws SQLException {
          PreparedStatement ps = null;
          ResultSet rs = null;

          try {
                String sql =" UPDATE " +
                                " users " +
                            " SET " +
                                " files_risk_status_id = nvl (files_risk_status_id,?) " + 
                            " WHERE " +
                                " id = ? ";

                ps = con.prepareStatement(sql);

                ps.setLong(1, docState);
                ps.setLong(2, userId);
               
                ps.executeUpdate();
          } finally {
                closeStatement(ps);
                closeResultSet(rs);
          }
          
          ps = null;
          rs = null;

          try {
                String sql =" UPDATE " +
                                " credit_cards " +
                            " SET " +
                                " files_risk_status_id = nvl (files_risk_status_id,?) " + 
                            " WHERE " +
                                " id = ? and" +
                                " user_id = ? ";

                ps = con.prepareStatement(sql);

                ps.setLong(1, docState);
                ps.setLong(2, ccId);
                ps.setLong(3, userId);
               
                ps.executeUpdate();
          } finally {
                closeStatement(ps);
                closeResultSet(rs);
          }
    }

	  /**
	   * Update use_id column on issues table by population entry id
	   * @param con db connection
	   * @param popUserEntry
	   * @throws SQLException
	   */
	  public static void updateIssuesByPopUserId(Connection con, PopulationEntryBase popUserEntry) throws SQLException {
		  PreparedStatement ps = null;
		  try {
			  String sql = "UPDATE " +
			  				  "issues " +
			  			   "SET " +
			  			   	  "user_id = ? " +
			  			   "WHERE " +
			  			   	  "population_entry_id in (select pe.id " +
			  			   	  						 " from population_entries pe " +
			  			   	  						 " where population_users_id = ?) ";

			  ps = con.prepareStatement(sql);
			  ps.setLong(1, popUserEntry.getUserId());
			  ps.setLong(2, popUserEntry.getPopulationUserId());
			  ps.executeUpdate();
		  } finally {
			  closeStatement(ps);
		  }
	  }
	  
	public static int isExistsIssueByUserAndCC(	Connection con, long ccNum, long userId,
												int subjectId)	throws SQLException, CryptoException, InvalidKeyException,
																NoSuchAlgorithmException, NoSuchPaddingException, IllegalBlockSizeException,
																BadPaddingException, UnsupportedEncodingException,
																InvalidAlgorithmParameterException {
	        PreparedStatement ps = null;
	        ResultSet rs = null;
	        int ret = 0;
	        try {
	            String sql =
	                " SELECT " +
	                      " * " +
	                " FROM " +
	                      " issues i, credit_cards cc " +
	                " WHERE " +
	                      " i.credit_card_id=cc.id " +
	                      " and i.user_id = ? " +
	                      " and cc.cc_number = ?" +
	                      " and i.subject_id = ?";

	              ps = con.prepareStatement(sql);
	              ps.setLong(1, userId);
	              ps.setString(2, AESUtil.encrypt(Long.toString(ccNum)));
	              ps.setInt(3, subjectId);

	              rs = ps.executeQuery();

	              while (rs.next()) {
	                  ret++;
	              }
	          } finally {
	              closeResultSet(rs);
	              closeStatement(ps);
	          }
	          return ret;
	    }
	  
	 
	    public static int[] getExistsIssueByUserAndCC(Connection con, long ccNum, long userId) throws SQLException, CryptoException {

	        PreparedStatement ps = null;
	        ResultSet rs = null;
	        int ret[] = {0, 0, 0, 0};

	        try {
	            String sql =
	                    "SELECT NVL(rl.id, fk.id) id, " +
	                            "  NVL(rl.doc_type, fk.doc_type) doc_type, " +
	                            "  NVL(rl.status_id, fk.status_id) status_id, " +
	                            "  NVL(rl.cc_same, fk.cc_same) cc_same " +
	                            " FROM " +
	                            "  (SELECT i.id, " +
	                            "    irdr.doc_type, " +
	                            "    i.status_id, " +
	                            "    DECODE(irdr.cc_id, ?, 1, 0) cc_same " +
	                            "  FROM ISSUE_RISK_DOC_REQ irdr, " +
	                            "    issues i, " +
	                            "    issue_actions ia " +
	                            "  WHERE i.id    = ia.issue_id " +
	                            "  AND ia.id     = irdr.issue_action_id " +
	                            "  AND i.subject_id != " + IssuesManagerBase.ISSUE_SUBJECT_REG_DOC +
	                            "  AND i.user_id = ? " +
	                            "  ) rl " +
	                            " FULL OUTER JOIN " +
	                            "  (SELECT 0 id, 0 doc_type, 0 status_id, 0 cc_same FROM dual " +
	                            "  ) fk " +
	                            " ON fk.id       =rl.id " +
	                            " AND fk.doc_type=rl.doc_type " +
	                            " ORDER BY 1 DESC, " +
	                            " 4 DESC" ;

	              ps = con.prepareStatement(sql);
	              ps.setLong(1, ccNum);
	              ps.setLong(2, userId);

	              rs = ps.executeQuery();

	              if (rs.next()) {
	                  ret[0] = rs.getInt("id");
	                  ret[1] = rs.getInt("doc_type");
	                  ret[2] = rs.getInt("status_id");
	                  ret[3] = rs.getInt("cc_same");
	              }
	          } finally {
	              closeResultSet(rs);
	              closeStatement(ps);
	          }
	          return ret;
	    }
	    
	    /**
         * Update issue status by issue action id
         * @param con
         * @param statusId
         * @param actionId
         * @param issueId TODO
         * @return
         * @throws SQLException
         */
        public static boolean updateIssueStatusByIaId(Connection con, int statusId, long actionId, long issueId) throws SQLException{
            PreparedStatement ps = null;
            ResultSet rs = null;
            try {
                String sql =" UPDATE " +
                                " issues " +
                            " SET " +
                                " status_id = ? " +
                            " WHERE ";
                if (issueId > 0){
                    sql +=      " id = ? ";
                }else{
                    sql +=
                                " id = (  SELECT " +
                                            " ia.issue_id " +
                                        " FROM " +
                                            " issue_actions ia " +
                                        " WHERE " +
                                            " ia.id = ?) ";
                }

                ps = con.prepareStatement(sql);
                ps.setLong(1, statusId);
                if (issueId > 0){
                    ps.setLong(2, issueId);
                }else{
                    ps.setLong(2, actionId);
                }

                ps.executeUpdate();

            } finally   {
                    closeResultSet(rs);
                    closeStatement(ps);
            }
            return true;
        }
}
