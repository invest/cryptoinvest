package com.anyoption.daos;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import com.anyoption.common.beans.Exchange;
import com.anyoption.common.beans.Market;
import com.anyoption.common.beans.Opportunity;
import com.anyoption.common.beans.OpportunityOddsType;
import com.anyoption.common.beans.OpportunityType;
import com.anyoption.common.util.OpportunityCacheBean;
import com.anyoption.util.ConstantsBase;

public class OpportunitiesDAOBase extends com.anyoption.common.daos.OpportunitiesDAOBase {
    private static final Logger log = Logger.getLogger(OpportunitiesDAOBase.class);

    /**
     * Get details for an opportunity that is currently running.
     *
     * @param conn the db conn to use
     * @param id the id of the opportunity
     * @return <code>Opportunity</code> with the requested details
     * @throws SQLException
     */
    public static Opportunity getRunningById(Connection conn, long id) throws SQLException {
        Opportunity o = new Opportunity();
        o.setId(id);
        return getRunningById(conn, o, false);
    }

    /**
     * Get details for an opportunity that is currently running.
     *
     * WARN: This query depends on the server session settings for the days numbers (first day of the week)
     * It use sunday as first day of the week (SUN = 1)
     *
     * @param conn the db conn to use
     * @param id the id of the opportunity
     * @return <code>Opportunity</code> with the requested details
     * @throws SQLException
     */
    public static Opportunity getRunningById(Connection conn, Opportunity o, boolean canBeSettled) throws SQLException {
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            String sql =
                "SELECT " +
                    "A.id, " +
                    "A.market_id, " +
                    "C.*, " +
//                    "C.market_group_id, " +
//                    "C.display_name, " +
//                    "C.name, " +
//                    "C.feed_name, " +
//                    "C.decimal_point, " +
//                    "C.investment_limits_group_id, " +
//                    "C.updates_pause, " +
//                    "C.significant_percentage, " +
//                    "C.real_updates_pause, " +
//                    "C.real_significant_percentage, " +
//                    "C.random_ceiling, " +
//                    "C.random_floor, " +
//                    "C.first_shift_parameter, " +
//                    "C.average_investments, " +
//                    "C.percentage_of_avg_investments, " +
//                    "C.fixed_amount_for_shifting, " +
//                    "C.type_of_shifting, " +
//                    "C.disable_after_period, " +
//                    "C.is_suspended, " +
//                    "C.suspended_message, " +
//                    "C.sec_between_investments, " +
//                    "C.sec_between_investments_ip, " +
//                    "C.sec_for_dev2, " +
//                    "C.amount_for_dev2, " +
//                    "C.amount_for_dev2_night, " +
//                    "C.amount_for_dev3, " +
//                    "C.acceptable_level_deviation, " +
//                    "C.acceptable_level_deviation_3, " +
//                    "C.no_auto_settle_after, " +
//                    "C.curent_level_alert_perc, " +
//                    "C.option_plus_fee, " +
//                    "C.option_plus_market_id, " +
//                    "C.decimal_point_subtract_digits, " +
                    "to_char(A.time_first_invest, 'YYYY-MM-DD HH24:MI:SS.FF3 TZHTZM') AS time_first_invest, " +
                    "to_char(A.time_est_closing, 'YYYY-MM-DD HH24:MI:SS.FF3 TZHTZM') AS time_est_closing, " +
                    "to_char(A.time_last_invest, 'YYYY-MM-DD HH24:MI:SS.FF3 TZHTZM') AS time_last_invest, " +
                    "A.current_level, " +
                    "A.auto_shift_parameter, " +
                    "get_opp_last_auto_shift(A.id) AS last_shift, " +
                    "A.scheduled, " +
                    "A.deduct_win_odds, " +
                    "A.is_disabled_service, " +
                    "A.is_disabled, " +
                    "A.opportunity_type_id, " +
                    "A.odds_group, " +
                    "B.over_odds_win, " +
                    "B.over_odds_lose, " +
                    "B.under_odds_win, " +
                    "B.under_odds_lose, " +
                    "B.odds_win_deduct_step, " +
                    "D.max_exposure, " +
                    "D.is_run_with_market, " +
                    "D.is_use_margin, " +
                    "D.margin_percentage_up, " +
                    "D.margin_percentage_down, " +
                    "E.closing_level, " +
                    "pkg_oppt_manager.get_opportunity_state(C.exchange_id, A.market_id, A.time_first_invest, A.time_last_invest, A.time_act_closing, A.scheduled) AS state " +
//                    "CASE " +
//                    "WHEN current_timestamp < A.time_first_invest THEN " + Opportunity.STATE_CREATED + " " +
//                    "WHEN A.time_first_invest <= current_timestamp AND current_timestamp < A.time_last_invest THEN " +
//                        "CASE " +
//                        "WHEN A.scheduled = " + Opportunity.SCHEDULED_WEEKLY + " OR A.scheduled = " + Opportunity.SCHEDULED_MONTHLY
//                        	+ " OR A.scheduled =  " + Opportunity.SCHEDULED_QUARTERLY + " THEN " +
//                            "CASE " +
//                            "WHEN EXISTS( " + // if we fall into a day template for that market
//                                "SELECT " +
//                                    "id " +
//                                "FROM " +
//                                    "opportunity_templates " +
//                                "WHERE " +
//                                    "market_id = A.market_id AND " +
//                                    "scheduled = " + Opportunity.SCHEDULED_DAYLY + " AND " +
//                                    "is_full_day = CASE WHEN (NOT F.id IS NULL AND F.is_half_day = 1) OR (market_id = 15 AND to_char(current_date, 'D') = 6) THEN 0 ELSE 1 END AND " + // if it is a half day we need a half day day template
//                                    "to_timestamp_tz(to_char(current_date, 'yyyy-mm-dd') || time_first_invest || time_zone, 'yyyy-mm-dd hh24:mi TZR') < current_timestamp AND " +
//                                    "to_timestamp_tz(to_char(current_date, 'yyyy-mm-dd') || time_last_invest || time_zone, 'yyyy-mm-dd hh24:mi TZR') > current_timestamp " +
//                                    ") AND " +
//                                "(F.id IS NULL OR F.is_half_day = 1) AND " + // make sure today is not a full day holiday
//                                "NOT ( " + // and not exchange weekend
//                                    "((C.exchange_id = 1 OR C.exchange_id = 17 OR C.exchange_id = 18) AND (to_char(current_date, 'D') = 6 OR to_char(current_date, 'D') = 7)) OR" +
//	                                "(NOT (C.exchange_id = 1 OR C.exchange_id = 17 OR C.exchange_id = 18 OR C.exchange_id = 16) AND (to_char(current_date, 'D') = 7 OR to_char(current_date, 'D') = 1)) OR " +
//	                                "(C.exchange_id = 16 AND (to_char(current_date, 'D') = 5 OR to_char(current_date, 'D') = 6 OR to_char(current_date, 'D') = 7))" +
//                                ") THEN " + Opportunity.STATE_OPENED + " " +
//                            "WHEN EXISTS( " + // if we fall between time last invest of a day template and time close
//                                "SELECT " +
//                                    "id " +
//                                "FROM " +
//                                    "opportunity_templates " +
//                                "WHERE " +
//                                    "market_id = A.market_id AND " +
//                                    "scheduled = " + Opportunity.SCHEDULED_DAYLY + " AND " +
//                                    "is_full_day = CASE WHEN (NOT F.id IS NULL AND F.is_half_day = 1) OR (market_id = 15 AND to_char(current_date, 'D') = 6) THEN 0 ELSE 1 END AND " + // if it is a half day we need a half day day template
//                                    "to_timestamp_tz(to_char(current_date, 'yyyy-mm-dd') || time_last_invest || time_zone, 'yyyy-mm-dd hh24:mi TZR') < current_timestamp AND " +
//                                    "to_timestamp_tz(to_char(current_date, 'yyyy-mm-dd') || time_est_closing || time_zone, 'yyyy-mm-dd hh24:mi TZR') > current_timestamp " +
//                                    ") AND " +
//                                "(F.id IS NULL OR F.is_half_day = 1) AND " + // make sure today is not a full day holiday
//                                "NOT ( " + // and not exchange weekend
//                                    "((C.exchange_id = 1 OR C.exchange_id = 17 OR C.exchange_id = 18) AND (to_char(current_date, 'D') = 6 OR to_char(current_date, 'D') = 7)) OR" +
//                                    "(NOT (C.exchange_id = 1 OR C.exchange_id = 17 OR C.exchange_id = 18 OR C.exchange_id = 16) AND (to_char(current_date, 'D') = 7 OR to_char(current_date, 'D') = 1)) OR " +
//                                    "(C.exchange_id = 16 AND (to_char(current_date, 'D') = 5 OR to_char(current_date, 'D') = 6 OR to_char(current_date, 'D') = 7))" +
//                                ") THEN " + Opportunity.STATE_WAITING_TO_PAUSE + " " +
//                            "ELSE " + Opportunity.STATE_PAUSED + " " +
//                            "END " +
//                        "ELSE " +
//                            "CASE " +
//                            "WHEN A.time_first_invest <= current_timestamp AND current_timestamp < A.time_last_invest - to_dsinterval('0 00:10:00') THEN " + Opportunity.STATE_OPENED + " " +
//                            "ELSE " + Opportunity.STATE_LAST_10_MIN + " " +
//                            "END " +
//                        "END " +
//                    "WHEN A.time_last_invest < current_timestamp AND current_timestamp < time_last_invest + to_dsinterval('0 00:01:00') THEN " + Opportunity.STATE_CLOSING_1_MIN + " " +
//                    "WHEN A.time_last_invest + to_dsinterval('0 00:01:00') < current_timestamp AND current_timestamp < time_est_closing THEN " + Opportunity.STATE_CLOSING + " " +
//                    "ELSE " + Opportunity.STATE_CLOSED + " " +
//                    "END AS state " +
                "FROM " +
                    "opportunities A, " +
                    "opportunity_odds_types B, " +
                    "opportunity_types D, " +
                    "markets C LEFT JOIN " +
                    "(SELECT " +
                        "M.id, " +
                        "O.closing_level " +
                    "FROM " +
                        "markets M, " +
                        "opportunities O " +
                    "WHERE " +
                        "M.id = O.market_id AND " +
                        "NOT O.closing_level IS NULL AND " +
                        "NOT O.time_act_closing IS NULL AND " +
                        "O.time_act_closing = " +
                            "(SELECT " +
                                "MAX(x.time_act_closing) " +
                            "FROM " +
                                "opportunities X " +
                            "WHERE " +
                                "(X.opportunity_type_id = 1 OR opportunity_type_id = 3) AND " +
                                "X.market_id = O.market_id AND " +
                                "NOT X.closing_level IS NULL AND " +
                                "NOT X.time_act_closing IS NULL AND " +
                                "NOT X.scheduled = " + Opportunity.SCHEDULED_HOURLY + ")) E ON C.id = E.id LEFT JOIN " +
                    "exchange_holidays F ON C.exchange_id = F.exchange_id AND trunc(F.holiday) = trunc(current_date) " +
                "WHERE " +
                    "A.odds_type_id = B.id AND " +
                    "A.market_id = C.id AND " +
                    "A.opportunity_type_id = D.id AND " +
                    (!canBeSettled ? "A.is_settled = 0 AND " : "") +
                    "(A.opportunity_type_id = 1 OR A.opportunity_type_id = 3 OR A.opportunity_type_id = 6) AND " +
                    "A.id = ?";
            pstmt = conn.prepareStatement(sql);
            pstmt.setLong(1, o.getId());
            rs = pstmt.executeQuery();
            if (rs.next()) {
                o = fillVO(o, rs, true, true, true, false);
                o.setState(rs.getInt("state"));
                o.getMarket().setOptionPlusMarketId(rs.getLong("option_plus_market_id"));
                o.setSkinGroupMappings(loadOpportunitySkingGroupMappings(conn, o.getId()));
                if (log.isTraceEnabled()) {
                    log.trace("Loaded skinGroupMappings: " + o.getSkinGroupMappings());
                }
            } else {
                log.error("Couldn't load the opp.");
            }
//        } catch (SQLException sqle) {
//            log.log(Level.ERROR, "", sqle);
//            throw sqle;
//        } catch (Throwable t) {
//            log.log(Level.ERROR, "", t);
        } finally {
            closeResultSet(rs);
            closeStatement(pstmt);
        }
        return o;
    }

    /**
     * Load the data from the current position of the result set
     * in Opportunity VO.
     *
     * @param rs the result set
     * @return <code>Opportunity</code>
     * @throws SQLException
     */
    protected static Opportunity fillVO(
            Opportunity o,
            ResultSet rs,
            boolean fillMarket,
            boolean fillOddsType,
            boolean fillType,
            boolean fillExchange) throws SQLException {
        Opportunity vo = null;
        if (null == o) {
            vo = new Opportunity();
        } else {
            vo = o;
        }
        vo.setId(rs.getLong("id"));
        vo.setCurrentLevel(rs.getDouble("current_level"));
        try {
            String tfi = rs.getString("time_first_invest");
            String tec = rs.getString("time_est_closing");
            String tli = rs.getString("time_last_invest");
            if (log.isTraceEnabled()) {
            	log.trace("time_first_invest: " + tfi);
            	log.trace("time_est_closing: " + tec);
            	log.trace("time_last_invest: " + tli);
            }

            SimpleDateFormat localDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS Z");
            vo.setTimeFirstInvest(localDateFormat.parse(tfi));
            vo.setTimeEstClosing(localDateFormat.parse(tec));
            vo.setTimeLastInvest(localDateFormat.parse(tli));
//            vo.setTimeFirstInvest(tzDateFormat.parse(rs.getString("time_first_invest")));
//            vo.setTimeEstClosing(tzDateFormat.parse(rs.getString("time_est_closing")));
//            vo.setTimeLastInvest(tzDateFormat.parse(rs.getString("time_last_invest")));
//        } catch (ParseException pe) {
//            log.error("Can't parse datetime.", pe);
//        }
        } catch (Throwable t) {
            log.error("Can't parse datetime.", t);
        }
        vo.setClosingLevel(rs.getDouble("closing_level"));
        vo.setAutoShiftParameter(rs.getDouble("auto_shift_parameter"));
        vo.setLastShift(rs.getBigDecimal("last_shift"));
        if (null != vo.getLastShift()) {
            int cmp = vo.getLastShift().compareTo(new BigDecimal(0));
            if (cmp != 0) {
                vo.setShifting(true);
                vo.setShiftUp(cmp > 0);
            }
            vo.setLastShift(vo.getLastShift().abs());
        }
        vo.setScheduled(rs.getInt("scheduled"));
        vo.setDeductWinOdds(rs.getBoolean("deduct_win_odds"));
        vo.setDisabledService(rs.getBoolean("is_disabled_service"));
        vo.setDisabled(rs.getBoolean("is_disabled"));
        vo.setOpportunityTypeId(rs.getLong("opportunity_type_id"));
        vo.setOddsGroup(rs.getString("odds_group"));

        if (fillMarket) {
//            Market market = new Market();
//            market.setId(rs.getLong("market_id"));
//            market.setGroupId(new Long(rs.getLong("market_group_id")));
//            market.setDisplayName(rs.getString("display_name"));
//            market.setName(rs.getString("name"));
//            market.setFeedName(rs.getString("feed_name"));
//            market.setDecimalPoint(new Long(rs.getLong("decimal_point")));
//            market.setInvestmentLimitsGroupId(new Long(rs.getLong("investment_limits_group_id")));
//            market.setUpdatesPause(rs.getInt("updates_pause"));
//            market.setSignificantPercentage(rs.getBigDecimal("significant_percentage"));
//            market.setRealUpdatesPause(rs.getInt("real_updates_pause"));
//            market.setRealSignificantPercentage(rs.getBigDecimal("real_significant_percentage"));
//            market.setRandomCeiling(rs.getBigDecimal("random_ceiling"));
//            market.setRandomFloor(rs.getBigDecimal("random_floor"));
//            market.setFirstShiftParameter(rs.getBigDecimal("first_shift_parameter"));
//            market.setAverageInvestments(rs.getBigDecimal("average_investments"));
//            market.setPercentageOfAverageInvestments(rs.getBigDecimal("percentage_of_avg_investments"));
//            market.setFixedAmountForShifting(rs.getBigDecimal("fixed_amount_for_shifting"));
//            market.setTypeOfShifting(rs.getLong("type_of_shifting"));
//            market.setDisableAfterPeriod(rs.getLong("disable_after_period"));
//            market.setSuspended(rs.getBoolean("is_suspended"));
//            market.setSuspendedMessage(rs.getString("suspended_message"));
//            market.setSecBetweenInvestments(rs.getInt("sec_between_investments"));
//            market.setSecForDev2(rs.getInt("sec_for_dev2"));
//            market.setAmountForDev2(rs.getFloat("amount_for_dev2"));
//            market.setAcceptableDeviation(rs.getBigDecimal("acceptable_level_deviation"));
//            market.setAcceptableDeviation3(rs.getBigDecimal("acceptable_level_deviation_3"));
//            market.setNoAutoSettleAfter(rs.getDouble("no_auto_settle_after"));
//            market.setCurrentLevelAlertPerc(rs.getBigDecimal("curent_level_alert_perc"));
//            market.setOptionPlusFee(rs.getLong("option_plus_fee"));
//            market.setSecBetweenInvestmentsSameIp(rs.getInt("sec_between_investments_ip"));
//            market.setAmountForDev3(rs.getFloat("amount_for_dev3"));
//            market.setDecimalPointSubtractDigits(rs.getInt("decimal_point_subtract_digits"));
//            market.setAmountForDev2Night(rs.getFloat("amount_for_dev2_night"));
            Market market = MarketsDAOBase.getMarketVO(rs);
            market.setId(rs.getLong("market_id"));
            vo.setMarketId(market.getId());
            vo.setMarket(market);
        }

        if (fillOddsType) {
            OpportunityOddsType oddsType = new OpportunityOddsType();
            oddsType.setOverOddsWin(rs.getFloat("over_odds_win"));
            oddsType.setOverOddsLose(rs.getFloat("over_odds_lose"));
            oddsType.setUnderOddsWin(rs.getFloat("under_odds_win"));
            oddsType.setUnderOddsLose(rs.getFloat("under_odds_lose"));
            oddsType.setOddsWinDeductStep(rs.getFloat("odds_win_deduct_step"));
            vo.setOddsType(oddsType);
        }

        if (fillType) {
            OpportunityType type = new OpportunityType();
            type.setMaxExposure(rs.getLong("max_exposure"));
            type.setIsRunWithMarket(rs.getInt("is_run_with_market"));
            type.setIsUseMargin(rs.getInt("is_use_margin"));
            type.setMarginPercentageUp(rs.getFloat("margin_percentage_up"));
            type.setMarginPercentageDown(rs.getFloat("margin_percentage_down"));
            vo.setType(type);
        }

        if (fillExchange) {
            Exchange exchange = new Exchange();
            exchange.setId(rs.getLong("exchange_id"));
            exchange.setName(rs.getString("exchange_name"));
            vo.setExchangeId(exchange.getId());
            vo.setExchange(exchange);
        }
        if (log.isTraceEnabled()) {
            log.trace("Loaded: " + vo.toString());
        }
        return vo;
    }

    protected static String getFormatTimesSQL() {
        return
   	 " to_char(o.time_first_invest, 'YYYY-MM-DD HH24:MI:SS.FF3 TZHTZM') AS ftime_first_invest, " +
   	 " to_char(o.time_est_closing, 'YYYY-MM-DD HH24:MI:SS.FF3 TZHTZM') AS ftime_est_closing, " +
   	 " to_char(o.time_last_invest, 'YYYY-MM-DD HH24:MI:SS.FF3 TZHTZM') AS ftime_last_invest, " +
   	 " to_char(o.time_act_closing, 'YYYY-MM-DD HH24:MI:SS.FF3 TZHTZM') AS ftime_act_closing, " +
   	 " to_char(o.time_created, 'YYYY-MM-DD HH24:MI:SS.FF3 TZHTZM') AS ftime_created, " +
   	 " to_char(o.TIME_SETTLED, 'YYYY-MM-DD HH24:MI:SS.FF3 TZHTZM') AS fTIME_SETTLED ";

   }

    /**
     * Gets the closed opportunities for last levels.
     *
     * @param conn the db connection to use
     * @return <code>ArrayList</code> of <code>Opportunity</code>s.
     * @throws SQLException
     */
    public static ArrayList<Opportunity> getClosedOpportunities(Connection conn, Date from, Date to, long marketId, long marketGroupId, long skinId,
    			int startRow, int pageSize) throws SQLException {
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        ArrayList<Opportunity> list = new ArrayList<Opportunity>();
        int index = 1;
        try {
            String sql =
	            	"SELECT * " +
					   "FROM " +
					   		"(SELECT X.* , rownum AS rownumIn " +
					   	 	"FROM " +
			                    "(SELECT " +
			                    	"o.*," + getFormatTimesSQL()+ " " +
			                    "FROM " +
			                    	"opportunities o " +
			               		"WHERE " +
			               			"SYS_EXTRACT_UTC(O.TIME_ACT_CLOSING) > (sysdate-7) AND " +
			               			"o.opportunity_type_id <> ? AND " +
			               			"o.market_id IN (SELECT smgm.market_id " +
												 	"FROM skin_market_group_markets smgm, skin_market_groups smg2, markets m " +
												 	"WHERE " +
												 		"smgm.skin_market_group_id = smg2.id AND " +
												 		"smg2.skin_id= ? AND " +
												 		"smgm.market_id = m.id ";

             if (marketGroupId != 0) {   // market selected
             	sql += "AND smg2.market_group_id = ? ";
             }

             sql+= ") ";

         	if (marketId != 0) {
         		sql += "AND o.market_id = ? ";
         	}

         	if(null != from) {
         		sql += "AND o.time_est_closing between (?) and (?) ";
         	}

           	sql += "ORDER BY " +
           				"o.time_est_closing desc NULLS LAST) X ) ";

           	if (pageSize > 0) {
           		sql += "WHERE " +
           					"rownumIn > " + startRow + " AND " +
           					"rownumIn <= " + (startRow + pageSize);
           	}

            pstmt = conn.prepareStatement(sql);

            pstmt.setInt(index++, ConstantsBase.OPPORTUNITIES_TYPE_ONE_TOUCH);
            pstmt.setLong(index++, skinId);
            if (marketGroupId != 0) {
            	pstmt.setLong(index++, marketGroupId);
            }
            if (marketId != 0) {
            	pstmt.setLong(index++, marketId);
            }

            if (null != from && null != to) {
	            pstmt.setTimestamp(index++, convertToTimestamp(from));
	            pstmt.setTimestamp(index++, convertToTimestamp(to));
            }

            rs = pstmt.executeQuery();
            Opportunity o = null;
            while (rs.next()) {
            	o = getVO(rs);
                o.setSkinGroupMappings(loadOpportunitySkingGroupMappings(conn, o.getId()));
                list.add(o);
            }
        } catch (SQLException sqle) {
            log.log(Level.ERROR, "", sqle);
            throw sqle;
        } catch (Throwable t) {
            log.log(Level.ERROR, "", t);
        } finally {
            closeResultSet(rs);
            closeStatement(pstmt);
        }
        return list;
    }

    protected static Opportunity getVO(ResultSet rs) throws SQLException{
    	Opportunity vo = new Opportunity();
    	vo.setId(rs.getLong("ID"));
    	vo.setMarketId(rs.getLong("MARKET_ID"));
    	try {
            SimpleDateFormat localDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS Z");
    		if (rs.getString("ftime_first_invest")!=null)
    			vo.setTimeFirstInvest(localDateFormat.parse(rs.getString("ftime_first_invest")));
    		if (rs.getString("ftime_est_closing")!=null)
    			vo.setTimeEstClosing(localDateFormat.parse(rs.getString("ftime_est_closing")));
    		if (rs.getString("ftime_last_invest")!=null)
    			vo.setTimeLastInvest(localDateFormat.parse(rs.getString("ftime_last_invest")));
    		if (rs.getString("ftime_act_closing")!=null)
    			vo.setTimeActClosing(localDateFormat.parse(rs.getString("ftime_act_closing")));
    		if (rs.getString("ftime_created")!=null)
    			vo.setTimeCreated(localDateFormat.parse(rs.getString("ftime_created")));
    		if (rs.getString("fTIME_SETTLED")!=null)
    			vo.setTimeSettled(localDateFormat.parse(rs.getString("fTIME_SETTLED")));

        } catch (ParseException pe) {
            log.error("Can't parse datetime.", pe);
        }

    	vo.setCurrentLevel(rs.getDouble("CURRENT_LEVEL"));
    	vo.setClosingLevel(rs.getDouble("CLOSING_LEVEL"));
      	vo.setPublished(rs.getInt("IS_PUBLISHED") == 1 ? true : false);
    	vo.setSettled(rs.getInt("IS_SETTLED") == 1 ? true : false);
    	vo.setOddsTypeId(rs.getLong("ODDS_TYPE_ID"));
    	vo.setWriterId(rs.getLong("WRITER_ID"));
    	vo.setOpportunityTypeId(rs.getLong("OPPORTUNITY_TYPE_ID"));
    	vo.setDisabled(rs.getInt("is_disabled")==1);
    	vo.setDisabledService(rs.getInt("IS_DISABLED_SERVICE")==1);
    	vo.setDisabledTrader(rs.getInt("IS_DISABLED_TRADER")==1);
    	vo.setScheduled(rs.getInt("scheduled"));
    	vo.setCloseLevelTxt(rs.getString("CLOSING_LEVEL_CALCULATION"));
		return vo;
    }

    public static OpportunityCacheBean getOpportunityCacheBean(Connection conn, long oppId) throws SQLException {
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        OpportunityCacheBean oppcb = null;
        try {
            String sql =
                "SELECT " +
                    "A.id, " +
                    "to_char(A.time_first_invest, 'YYYY-MM-DD HH24:MI:SS.FF3 TZHTZM') AS time_first_invest, " +
                    "to_char(A.time_last_invest, 'YYYY-MM-DD HH24:MI:SS.FF3 TZHTZM') AS time_last_invest, " +
                    "to_char(A.time_est_closing, 'YYYY-MM-DD HH24:MI:SS.FF3 TZHTZM') AS time_est_closing, " +
                    "A.scheduled, " +
                    "B.display_name, " +
                    "B.decimal_point, " +
                    "B.id AS market_id, " +
                    "A.opportunity_type_id, " +
                    "A.max_inv_coeff " +
                "FROM " +
                    "opportunities A, " +
                    "markets B " +
                "WHERE " +
                    "A.market_id = B.id AND " +
                    "A.id = ?";

            pstmt = conn.prepareStatement(sql);
            pstmt.setLong(1, oppId);
            rs = pstmt.executeQuery();
            if (rs.next()) {
                oppcb = new OpportunityCacheBean();
                oppcb.setId(rs.getLong("id"));
                oppcb.setTimeFirstInvest(getTimeWithTimezone(rs.getString("time_first_invest")));
                oppcb.setTimeLastInvest(getTimeWithTimezone(rs.getString("time_last_invest")));
                oppcb.setTimeEstClosing(getTimeWithTimezone(rs.getString("time_est_closing")));
                oppcb.setScheduled(rs.getInt("scheduled"));
                oppcb.setMarketDisplayName(rs.getString("display_name"));
                oppcb.setMarketDecimalPoint(rs.getLong("decimal_point"));
                oppcb.setMarketId(rs.getLong("market_id"));
                oppcb.setOpportunityTypeId(rs.getLong("opportunity_type_id"));
                oppcb.setMaxInvAmountCoeffPerUser(rs.getDouble("max_inv_coeff"));
            }
        } finally {
            closeResultSet(rs);
            closeStatement(pstmt);
        }
        return oppcb;
    }

    public static long getMarketCurrentOpportunity(Connection conn, long marketId, long opportunityTypeId) throws SQLException {
        long oppId = 0;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            String sql =
                "SELECT " +
                    "A.id " +
                "FROM " +
                    "opportunities A, " +
                    "markets B " +
                "WHERE " +
                    "A.market_id = B.id AND " +
                    "A.market_id = ? AND " +
                    "A.opportunity_type_id = ? AND " +
                    "A.is_published = 1 AND " +
//                    "B.is_suspended = 0 AND " +
                    "A.closing_level IS NULL " +
                "ORDER BY " +
//                    "A.scheduled, " +
                    "A.time_est_closing";
            pstmt = conn.prepareStatement(sql);
            pstmt.setLong(1, marketId);
            pstmt.setLong(2, opportunityTypeId);
            rs = pstmt.executeQuery();
            if (rs.next()) {
                oppId = rs.getInt("id");
            }
        } finally {
            closeResultSet(rs);
            closeStatement(pstmt);
        }
        return oppId;
    }

    public static ArrayList<Long> getOptionPlusCurrentMarket(Connection conn, long skinId) throws SQLException {
	    ArrayList<Long> l = new ArrayList<Long>();
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            String sql =
            	"SELECT " +
					"B.market_id " +
				"FROM " +
					"skin_market_groups A, " +
					"skin_market_group_markets B, " +
					"markets C, " +
					"opportunities D " +
				"WHERE " +
					"A.skin_id = ? AND " +
					"A.id = B.skin_market_group_id AND " +
					"B.market_id = C.id AND " +
					"NOT C.option_plus_market_id IS NULL AND " +
					"B.market_id = D.market_id AND " +
					"D.is_published = 1 AND " +
					"C.is_suspended = 0 AND " +
					"D.closing_level IS NULL " +
				"GROUP BY " +
					"B.market_id, " +
					"B.home_page_priority " +
				"ORDER BY " +
					"B.home_page_priority";
            pstmt = conn.prepareStatement(sql);
            pstmt.setLong(1, skinId);
            rs = pstmt.executeQuery();
            while (rs.next()) {
            	l.add(rs.getLong("market_id"));
            }
        } finally {
            closeResultSet(rs);
            closeStatement(pstmt);
        }
        return l;
	}

    public static Opportunity getNextHourlyOppToOpenByOppId(Connection conn, long oppId) throws SQLException {
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        Opportunity nextOpp = null;
        try {
            String sql =
                "SELECT " +
                    "o.is_disabled_trader, " +
                    "o.is_disabled_service, " +
                    "o.closing_level, " +
                    "o.current_level, " +
                    "o.id, " +
                    "o.is_disabled, " +
                    "o.is_published, " +
                    "o.is_settled, " +
                    "o.market_id, " +
                    "o.odds_type_id, " +
                    "o.opportunity_type_id, " +
                    "o.scheduled, " +
                    "o.writer_id, " +
                    "o.CLOSING_LEVEL_CALCULATION, " +
                    "to_char(o.time_act_closing, 'YYYY-MM-DD HH24:MI:SS.FF3 TZHTZM') AS ftime_act_closing, " +
                    "to_char(o.time_created, 'YYYY-MM-DD HH24:MI:SS.FF3 TZHTZM') AS ftime_created, " +
                    "to_char(o.time_est_closing, 'YYYY-MM-DD HH24:MI:SS.FF3 TZHTZM') AS ftime_est_closing, " +
                    "to_char(o.time_first_invest, 'YYYY-MM-DD HH24:MI:SS.FF3 TZHTZM') AS ftime_first_invest, " +
                    "to_char(o.time_last_invest, 'YYYY-MM-DD HH24:MI:SS.FF3 TZHTZM') AS ftime_last_invest, " +
                    "to_char(o.time_settled, 'YYYY-MM-DD HH24:MI:SS.FF3 TZHTZM') AS ftime_settled " +
                "FROM " +
                    " Opportunities o " +
                 "WHERE " +
                    "o.id = GET_OPP_ROLL_UP(?) ";
            pstmt = conn.prepareStatement(sql);
            pstmt.setLong(1, oppId);
            rs = pstmt.executeQuery();
            if(rs.next()) {
                nextOpp = getVO(rs);
                nextOpp.setSkinGroupMappings(loadOpportunitySkingGroupMappings(conn, nextOpp.getId()));
            }
        } finally {
            closeResultSet(rs);
            closeStatement(pstmt);
        }
        return nextOpp;
    }
    
    /**
     * Get current hourly opportunity by market
     * @param conn
     * @param marketId
     * @param opportunityTypeId
     * @return
     * @throws SQLException
     */
    public static Opportunity getCurrentHourlyOppByMarket(Connection conn, long marketId, long opportunityTypeId) throws SQLException {
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        Opportunity currentOpp = null;
        try {
            String sql =" SELECT " +
            			"	o.*," + getFormatTimesSQL() + "," +
            			" 	oot.over_odds_win, " +
            			"	oot.over_odds_lose " +                              
            			" FROM " +
            			"	opportunities o, " +
                        "	opportunity_odds_types oot " +
            			" WHERE " +
            			"	o.odds_type_id = oot.id " +
            			"	AND o.market_id = ? " +
            			"	AND o.is_published = " + Opportunity.PUBLISHED_YES +
            			"	AND o.opportunity_type_id = ? " +
            			"	AND o.is_settled = 0 " + 
            			"	AND o.is_disabled = 0 " +
            			"	AND	SYS_EXTRACT_UTC(o.time_est_closing) > sysdate " +
            			"	AND NOT EXISTS (SELECT " +
            			"	                   1 " +
            			"                   FROM " +
            			" 						opportunities o1 " +
            			"	                WHERE " +
            			"                       o1.market_id = o.market_id " +
            			"                       AND o1.is_published = "   + Opportunity.PUBLISHED_YES +
            			"                       AND o1.is_disabled = 1 " +
            			"                       AND o1.opportunity_type_id = ? " +
            			"                       AND o1.is_settled = 0 ) " +                                           			
            			
            			" ORDER BY " +
            			"	o.scheduled ";
            pstmt = conn.prepareStatement(sql);
            pstmt.setLong(1, marketId);
            pstmt.setLong(2, opportunityTypeId);
            pstmt.setLong(3, opportunityTypeId);
            rs = pstmt.executeQuery();
            if (rs.next()) {
            	currentOpp = getVO(rs);
            	currentOpp.setPageOddsLose(rs.getFloat("over_odds_lose"));
            	currentOpp.setPageOddsWin(rs.getFloat("over_odds_win"));
            	currentOpp.setSkinGroupMappings(loadOpportunitySkingGroupMappings(conn, currentOpp.getId()));
            }
        } finally {
            closeResultSet(rs);
            closeStatement(pstmt);
        }
        return currentOpp;
    }

}