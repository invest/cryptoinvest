package com.anyoption.population;


import java.sql.Connection;

import org.apache.log4j.Logger;

import com.anyoption.beans.User;
import com.anyoption.common.beans.Transaction;
import com.anyoption.common.bl_vos.PopulationEntryBase;
import com.anyoption.managers.PopulationsManagerBase;

public class PopShortRegFormHandler extends PopulationHandlerBase {
	private static final Logger log = Logger.getLogger(PopShortRegFormHandler.class);

	/**
	 * deposit event handler implementation
	 */
	@Override
	public void deposit(Connection con, PopulationEntryBase popUserEntry, boolean successful, long writerId, Transaction tran, User user) throws PopulationHandlersException {
		if (!successful){
			PopulationsManagerBase.insertIntoDeclinePop(con, user, popUserEntry, false, writerId, tran.getDescription());
		}
	}

	/**
	 * invest event handler implementation
	 */
	@Override
	public void invest(PopulationEntryBase popUserEntry, boolean successful, long writerId) throws PopulationHandlersException {
		//	do nothing
	}

	/**
	 * closeAccount event handler implementation
	 */
	@Override
	public void close(PopulationEntryBase popUserEntry, long writerId ) throws PopulationHandlersException {
		//	do nothing
	}

	/**
	 * fraud activities event handler implementation
	 */
	@Override
	public void fraud(PopulationEntryBase popUserEntry, long writerId) throws PopulationHandlersException {
		//	do nothing
	}

	/**
	 * user creation event handler implementation
	 * Implement call me population Issues update upon user creation
	 */
	public void userCreation(Connection con, PopulationEntryBase popUserEntry, long writerId) throws PopulationHandlersException {
		long currEntryId = popUserEntry.getCurrEntryId();
		log.debug("userCreation event remove Process, popEntryId: " + currEntryId);
		delete(con, popUserEntry, writerId,PopulationsManagerBase.POP_ENT_HIS_STATUS_REMOVED_QUALIFICATION, 0, true);
	}

	/**
	 * reached call event handler implementation
	 * Implement call me population remove upon reached call
	 */
	public void reachedCall(Connection con, PopulationEntryBase popUserEntry, long writerId, User user) throws PopulationHandlersException {
		log.debug("userCreation event remove Process, popEntryId: " + popUserEntry.getCurrEntryId());
		delete(con, popUserEntry, writerId,PopulationsManagerBase.POP_ENT_HIS_STATUS_REMOVED_QUALIFICATION, 0,false);

		//TODO fix message
//		FacesContext context=FacesContext.getCurrentInstance();
//		FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_INFO,
//				CommonUtil.getMessage("retention.entry.removed.from.population", null),null);
//		context.addMessage(null, fm);
	}

}