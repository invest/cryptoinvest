package com.copyop.json.requests;

/**
 * @author kirilim
 */
public class InsertInvestmentMethodRequest extends com.anyoption.common.service.requests.InsertInvestmentMethodRequest {

	private Long destUserId;

	/**
	 * @return the destUserId
	 */
	public Long getDestUserId() {
		return destUserId;
	}

	/**
	 * @param destUserId the destUserId to set
	 */
	public void setDestUserId(Long destUserId) {
		this.destUserId = destUserId;
	}

	@Override
	public String toString() {
		String ls = System.getProperty("line.separator");
		return ls + "com.copyop.json.requests.InsertInvestmentMethodRequest: "
				+ super.toString()
				+ "destUserId: " + destUserId + ls;
	}
}