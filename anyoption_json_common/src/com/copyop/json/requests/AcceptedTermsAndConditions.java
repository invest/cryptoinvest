/**
 * 
 */
package com.copyop.json.requests;

import com.anyoption.common.service.requests.UserMethodRequest;

public class AcceptedTermsAndConditions extends UserMethodRequest {

	private String nickname;
	private String avatar;
	private boolean acceptedTermsAndConditions;

	/**
	 * @return the nickname
	 */
	public String getNickname() {
		return nickname;
	}

	/**
	 * @param nickname the nickname to set
	 */
	public void setNickname(String nickname) {
		this.nickname = nickname;
	}

	/**
	 * @return the avatar
	 */
	public String getAvatar() {
		return avatar;
	}

	/**
	 * @param avatar the avatar to set
	 */
	public void setAvatar(String avatar) {
		this.avatar = avatar;
	}
	
	public boolean isAcceptedTermsAndConditions() {
		return acceptedTermsAndConditions;
	}

	public void setAcceptedTermsAndConditions(boolean acceptedTermsAndConditions) {
		this.acceptedTermsAndConditions = acceptedTermsAndConditions;
	}

	public String toString() {
		String ls = System.getProperty("line.separator");
		return ls + "com.copyop.json.requests.UserMethodRequest: "
				+ super.toString()
				+ "nickname: " + nickname + ls
				+ "avatar: " + avatar + ls
				+ "acceptedTermsAndConditions: " + acceptedTermsAndConditions + ls;
	}
}