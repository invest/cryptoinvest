/**
 * 
 */
package com.copyop.json.requests;


/**
 * @author kirilim
 */
public class WatchListMethodRequest extends ProfileMethodRequest {

	private boolean autoWatchRequired;

	/**
	 * @return the needAutoWatch
	 */
	public boolean isAutoWatchRequired() {
		return autoWatchRequired;
	}

	/**
	 * @param needAutoWatch the needAutoWatch to set
	 */
	public void setAutoWatchRequired(boolean autoWatchRequired) {
		this.autoWatchRequired = autoWatchRequired;
	}

	public String toString() {
		String ls = System.getProperty("line.separator");
	    return ls + "WatchListMethodRequest: " + ls
	    		+ super.toString() + ls
	    		+ "needAutoWatch: " + autoWatchRequired + ls;
	}
}