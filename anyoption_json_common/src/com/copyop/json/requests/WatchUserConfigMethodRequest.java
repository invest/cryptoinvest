package com.copyop.json.requests;

import com.anyoption.common.service.requests.UserMethodRequest;
import com.copyop.common.enums.base.ProfileLinkCommandEnum;

public class WatchUserConfigMethodRequest extends UserMethodRequest {

	private long destUserId;
	private ProfileLinkCommandEnum command;
	private boolean watchingPushNotification;
	
	public long getDestUserId() {
		return destUserId;
	}
	public void setDestUserId(long destUserId) {
		this.destUserId = destUserId;
	}
	public ProfileLinkCommandEnum getCommand() {
		return command;
	}
	public void setCommand(ProfileLinkCommandEnum command) {
		this.command = command;
	}
	
	public boolean isWatchingPushNotification() {
		return watchingPushNotification;
	}
	public void setWatchingPushNotification(boolean watchingPushNotification) {
		this.watchingPushNotification = watchingPushNotification;
	}

	public String toString() {
		String ls = System.getProperty("line.separator");
		return ls + "WatchUserConfigMethodRequest: " + ls
				+ super.toString()
				+ "destUserId: " + destUserId + ls
				+ "command: " + command + ls 
				+ "watchingPushNotification: " + watchingPushNotification + ls;
	}
}