package com.copyop.json.requests;

import java.util.List;

import com.anyoption.common.service.requests.UserMethodRequest;

/**
 * @author kirilim
 */
public class UpdateFacebookFriendsMethodRequest extends UserMethodRequest {

	private List<String> friends;
	private String facebookId;

	/**
	 * @return the friends
	 */
	public List<String> getFriends() {
		return friends;
	}

	/**
	 * @param friends the friends to set
	 */
	public void setFriends(List<String> friends) {
		this.friends = friends;
	}

	/**
	 * @return the facebookId
	 */
	public String getFacebookId() {
		return facebookId;
	}

	/**
	 * @param facebookId the facebookId to set
	 */
	public void setFacebookId(String facebookId) {
		this.facebookId = facebookId;
	}
	
	public String toString() {
        String ls = System.getProperty("line.separator");
        return ls + "UpdateFacebookFriendsMethodRequest: "
            + super.toString()
            + "friends: " + friends + ls
            + "facebookId: " + facebookId + ls;
	}
}