package com.copyop.json.requests;

import com.anyoption.common.service.requests.UserMethodRequest;

/**
 * @author pavelhe
 */
public class OriginalInvProfileMethodRequest extends UserMethodRequest {

	private long copiedInvestmentId;

	/**
	 * @return the requestedUserId
	 */
	public long getCopiedInvestmentId() {
		return copiedInvestmentId;
	}

	/**
	 * @param requestedUserId the requestedUserId to set
	 */
	public void setCopiedInvestmentId(long copiedInvestmentId) {
		this.copiedInvestmentId = copiedInvestmentId;
	}

	@Override
	public String toString() {
		String ls = System.getProperty("line.separator");
        return ls + "OriginalInvProfileMethodRequest: "
            + super.toString()
            + "copiedInvestmentId: " + copiedInvestmentId + ls;
	}
}