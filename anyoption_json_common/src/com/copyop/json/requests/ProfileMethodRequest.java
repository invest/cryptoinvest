package com.copyop.json.requests;

import com.anyoption.common.service.requests.UserMethodRequest;

/**
 * @author kirilim
 */
public class ProfileMethodRequest extends UserMethodRequest {

	private long requestedUserId;

	/**
	 * @return the requestedUserId
	 */
	public long getRequestedUserId() {
		return requestedUserId;
	}

	/**
	 * @param requestedUserId the requestedUserId to set
	 */
	public void setRequestedUserId(long requestedUserId) {
		this.requestedUserId = requestedUserId;
	}

	public String toString() {
		String ls = System.getProperty("line.separator");
        return ls + "ProfileMethodRequest: "
            + super.toString()
            + "requestedUserId: " + requestedUserId + ls;
	}
}