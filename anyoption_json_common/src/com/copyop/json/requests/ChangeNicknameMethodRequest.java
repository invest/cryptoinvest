package com.copyop.json.requests;

import com.anyoption.common.service.requests.UserMethodRequest;

/**
 * @author liors
 *
 */
public class ChangeNicknameMethodRequest extends UserMethodRequest {

	private String nickname;

	/**
	 * @return the nickName
	 */
	public String getNickname() {
		return nickname;
	}

	/**
	 * @param nickName the nickName to set
	 */
	public void setNickname(String nickname) {
		this.nickname = nickname;
	}

	@Override
	public String toString() {
		return "CopyopUserMethodRequest [nickname=" + nickname + "]";
	}
}