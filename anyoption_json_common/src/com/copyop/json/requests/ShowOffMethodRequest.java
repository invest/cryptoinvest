package com.copyop.json.requests;

import com.anyoption.common.service.requests.UserMethodRequest;

/**
 * @author kirilim
 */
public class ShowOffMethodRequest extends UserMethodRequest {

	private long investmentId;

	public long getInvestmentId() {
		return investmentId;
	}

	public void setInvestmentId(long investmentId) {
		this.investmentId = investmentId;
	}
}