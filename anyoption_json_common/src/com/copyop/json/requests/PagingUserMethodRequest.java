package com.copyop.json.requests;

import com.anyoption.common.service.requests.UserMethodRequest;

/**
 * @author kirilim
 */
public class PagingUserMethodRequest extends UserMethodRequest {

	private boolean resetPaging;

	public boolean isResetPaging() {
		return resetPaging;
	}

	public void setResetPaging(boolean resetPaging) {
		this.resetPaging = resetPaging;
	}
    
    public String toString() {
        String ls = System.getProperty("line.separator");
        return ls + "PagingUserMethodRequest: "
            + super.toString()
            + "resetPaging: " + resetPaging + ls;
    }
}