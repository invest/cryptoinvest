package com.copyop.json.requests;

import com.anyoption.common.service.requests.UserMethodRequest;

public class CopyopHotDataMethodRequest extends UserMethodRequest {

	private int updateTypeEnumId;
	private boolean isInitCall;
	


	public int getUpdateTypeEnumId() {
		return updateTypeEnumId;
	}

	public void setUpdateTypeEnumId(int updateTypeEnumId) {
		this.updateTypeEnumId = updateTypeEnumId;
	}
	
	public boolean isInitCall() {
		return isInitCall;
	}

	public void setInitCall(boolean isInitCall) {
		this.isInitCall = isInitCall;
	}
	
	public String toString() {
		String ls = System.getProperty("line.separator");
		return ls + "CopyopHotDataMethodRequest: " + ls
				+ super.toString()
				+ "updateTypeEnumId: " + updateTypeEnumId + ls
				+ "isInitCall: " + isInitCall + ls;
	}
}