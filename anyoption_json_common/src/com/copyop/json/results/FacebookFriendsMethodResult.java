package com.copyop.json.results;

import java.util.List;

import com.anyoption.common.service.results.MethodResult;
import com.copyop.common.dto.base.Profile;

/**
 * @author kirilim
 */
public class FacebookFriendsMethodResult extends MethodResult {

	List<Profile> friends;

	/**
	 * @return the friends
	 */
	public List<Profile> getFriends() {
		return friends;
	}

	/**
	 * @param friends the friends to set
	 */
	public void setFriends(List<Profile> friends) {
		this.friends = friends;
	}

	public String toString() {
		String ls = System.getProperty("line.separator");
		return ls + "FacebookFriendsMethodResult: "
				+ super.toString()
				+ "friends: " + friends + ls;
	}
}