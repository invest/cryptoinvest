/**
 * 
 */
package com.copyop.json.results;

import com.anyoption.common.service.results.MethodResult;
import com.copyop.common.dto.base.CopyConfig;


/**
 * @author kirilim
 */
public class CopyUserConfigMethodResult extends MethodResult {

	private CopyConfig config;

	public CopyConfig getConfig() {
		return config;
	}

	public void setConfig(CopyConfig config) {
		this.config = config;
	}

	public String toString() {
		String ls = System.getProperty("line.separator");
		return ls + "CopyUserConfigMethodResult: " + ls
				+ super.toString()
				+ "config: " + config + ls;
	}
}