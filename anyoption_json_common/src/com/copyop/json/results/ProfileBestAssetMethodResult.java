package com.copyop.json.results;

import java.util.List;

import com.anyoption.common.service.results.MethodResult;

/**
 * @author kirilim
 */
public class ProfileBestAssetMethodResult extends MethodResult {

	private List<Long> marketIds;

	public List<Long> getMarketIds() {
		return marketIds;
	}

	public void setMarketIds(List<Long> marketIds) {
		this.marketIds = marketIds;
	}

	public String toString() {
		String ls = System.getProperty("line.separator");
		return ls + "ProfileBestAssetMethodResult: "
				+ super.toString()
				+ "marketIds: " + marketIds + ls;
	}
}