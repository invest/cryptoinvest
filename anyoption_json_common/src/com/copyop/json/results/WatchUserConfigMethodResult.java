/**
 * 
 */
package com.copyop.json.results;

import com.anyoption.common.service.results.MethodResult;

public class WatchUserConfigMethodResult extends MethodResult {

	private boolean watchingPushNotification;
	

	public boolean isWatchingPushNotification() {
		return watchingPushNotification;
	}

	public void setWatchingPushNotification(boolean watchingPushNotification) {
		this.watchingPushNotification = watchingPushNotification;
	}

	public String toString() {
		String ls = System.getProperty("line.separator");
		return ls + "WatchUserConfigMethodResult: " + ls
				+ super.toString()
				+ "watchingPushNotification: " + watchingPushNotification + ls;
	}
}