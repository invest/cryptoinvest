package com.copyop.json.results;

import java.util.List;

import com.anyoption.common.service.results.MethodResult;
import com.copyop.common.dto.base.Profile;

/**
 * @author kirilim
 */
public class WatchListMethodResult extends MethodResult {

	private List<? extends Profile> watchList;

	/**
	 * @return the watchingList
	 */
	public List<? extends Profile> getWatchList() {
		return watchList;
	}

	/**
	 * @param watchingList the watchingList to set
	 */
	public void setWatchList(List<? extends Profile> watchList) {
		this.watchList = watchList;
	}

	public String toString() {
		String ls = System.getProperty("line.separator");
		return ls + "WatchListMethodResult: "
				+ super.toString()
				+ "watchList: " + watchList + ls;
	}
}