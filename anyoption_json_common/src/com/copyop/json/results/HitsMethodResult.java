/**
 * 
 */
package com.copyop.json.results;

import java.util.Date;
import java.util.List;

import com.anyoption.common.beans.base.AssetResult;
import com.anyoption.common.beans.base.TradesGroup;
import com.anyoption.common.service.results.MethodResult;

/**
 * @author kirilim
 *
 */
public class HitsMethodResult extends MethodResult {
	
	public static long ASSET_MARKET_OTHER = 0;

	private int hitRate;
	private int investmentsCount;
	private Date lastTradeTime;
	private List<TradesGroup> tradesGroups;
	private List<AssetResult> assetResults;

	public int getHitRate() {
		return hitRate;
	}

	public void setHitRate(int hitRate) {
		this.hitRate = hitRate;
	}

	public int getInvestmentsCount() {
		return investmentsCount;
	}

	public void setInvestmentsCount(int investmentsCount) {
		this.investmentsCount = investmentsCount;
	}

	public Date getLastTradeTime() {
		return lastTradeTime;
	}

	public void setLastTradeTime(Date lastTradeTime) {
		this.lastTradeTime = lastTradeTime;
	}

	public List<TradesGroup> getTradesGroups() {
		return tradesGroups;
	}

	public void setTradesGroups(List<TradesGroup> tradesGroups) {
		this.tradesGroups = tradesGroups;
	}

	public List<AssetResult> getAssetResults() {
		return assetResults;
	}

	public void setAssetResults(List<AssetResult> assetResults) {
		this.assetResults = assetResults;
	}

	public String toString() {
		String ls = System.getProperty("line.separator");
		return ls + "HitsMethodResult: " 
				+ super.toString()
				+ "hitRate: " + hitRate + ls
				+ "investmentsCount: " + investmentsCount + ls
				+ "lastTradeTime: " + lastTradeTime + ls
				+ "tradesGroups: " + tradesGroups + ls
				+ "assetResults: " + assetResults + ls;
	}

//	public class TradesGroup {
//
//		private int startIndex;
//		private int endIndex;
//		private int hitRate;
//
//		public int getStartIndex() {
//			return startIndex;
//		}
//
//		public void setStartIndex(int startIndex) {
//			this.startIndex = startIndex;
//		}
//
//		public int getEndIndex() {
//			return endIndex;
//		}
//
//		public void setEndIndex(int endIndex) {
//			this.endIndex = endIndex;
//		}
//
//		public int getHitRate() {
//			return hitRate;
//		}
//
//		public void setHitRate(int hitRate) {
//			this.hitRate = hitRate;
//		}
//
//		public String toString() {
//			String ls = System.getProperty("line.separator");
//			return ls + "TradesGroup: " + ls
//					+ super.toString() + ls
//					+ "startIndex: " + startIndex + ls
//					+ "endIndex: " + endIndex + ls
//					+ "hitRate" + hitRate + ls;
//		}
//	}

//	public class AssetResult {
//
//		private long marketId;
//		private int hitRate;
//
//		public long getMarketId() {
//			return marketId;
//		}
//
//		public void setMarketId(long marketId) {
//			this.marketId = marketId;
//		}
//
//		public int getHitRate() {
//			return hitRate;
//		}
//
//		public void setHitRate(int hitRate) {
//			this.hitRate = hitRate;
//		}
//
//		public String toString() {
//			String ls = System.getProperty("line.separator");
//			return ls + "AssetResult: " + ls
//					+ super.toString() + ls
//					+ "marketId: " + marketId + ls
//					+ "hitRate: " + hitRate + ls;
//		}
//	}
}