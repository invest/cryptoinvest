package com.copyop.json.results;

import java.util.TreeMap;

import com.anyoption.common.service.results.MethodResult;

/**
 * @author kirilim
 */
public class ProfileCoinsMethodResult extends MethodResult {

	private long balance;
	private TreeMap<Long, Long> rates;
	private TreeMap <Long, Long> coinsPerAmountInv;
	private long convertRemainingTime;
	private long minInvAmount;
	private long followCoins;

	public long getFollowCoins() {
	    return followCoins;
	}

	public void setFollowCoins(long followCoins) {
	    this.followCoins = followCoins;
	}

	public TreeMap<Long, Long> getCoinsPerAmountInv() {
	    return coinsPerAmountInv;
	}

	public void setCoinsPerAmountInv(TreeMap<Long, Long> coinsPerAmountInv) {
	    this.coinsPerAmountInv = coinsPerAmountInv;
	}

	public long getMinInvAmount() {
	    return minInvAmount;
	}

	public void setMinInvAmount(long minInvAmount) {
	    this.minInvAmount = minInvAmount;
	}

	public long getBalance() {
		return balance;
	}

	public void setBalance(long balance) {
		this.balance = balance;
	}

	public TreeMap<Long, Long> getRates() {
		return rates;
	}

	public void setRates(TreeMap<Long, Long> rates) {
		this.rates = rates;
	}

	public long getConvertRemainingTime() {
		return convertRemainingTime;
	}

	public void setConvertRemainingTime(long convertRemainingTime) {
		this.convertRemainingTime = convertRemainingTime;
	}

	@Override
	public String toString() {
		String ls = System.getProperty("line.separator");
		return ls + "ProfileCoinsMethodResult: "
				+ super.toString()
				+ "balance: " + balance + ls
				+ "rates: " + rates + ls
				+ "convertRemainingTime: " + convertRemainingTime + ls
				+ "minimum investment amount: " + minInvAmount + ls
				+ "coins per amounts of investment: " + coinsPerAmountInv + ls
				+ "coins per follow: " + followCoins + ls;
	}
}