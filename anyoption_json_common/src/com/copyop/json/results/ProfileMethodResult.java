package com.copyop.json.results;

import java.util.Date;
import java.util.List;

import com.anyoption.common.beans.base.Investment;
import com.anyoption.common.service.results.MethodResult;
import com.copyop.common.dto.base.Profile;
import com.copyop.common.enums.base.ProfileLinkTypeEnum;

/**
 * @author kirilim
 */
public class ProfileMethodResult extends MethodResult {

	private String avatar;
	private String nickname;
	private Date timeCreated;
	private int riskAppetite;
	private boolean online;
	private boolean frozen;
	private String description;
	private int hitRate;
	private long copiersCount;
	private long watchersCount;
	private long marketsTotalResult;
	private List<TopMarket> topMarkets;
	private long bestAssetMarketId;
	private long bestAssetMarketResult;
	private int streakCount;
	private long streakResult;
	private double avgTrades;
	private int avgTradesPeriod;
	private List<? extends Investment> lastInvestments;
	private List<Profile> copiedProfiles;
	private List<Profile> similarProfiles;
	private long copyingCount;
	private long watchingCount;
	private List<ProfileWithLink> fbFriendsWithLink;
	private String firstName;
	private String lastName;
	private boolean watching;
	private boolean copying;
	private long shareCount;
	private long rateCount;
	private boolean showHitRate;
	private boolean openHitRate;

	private int copiedInvestmentsTotalSuccess;
	private List<TopTrader> topTraders;
	private List<Profile> fbFriends;

	/**
	 * @return the avatar
	 */
	public String getAvatar() {
		return avatar;
	}

	/**
	 * @param avatar the avatar to set
	 */
	public void setAvatar(String avatar) {
		this.avatar = avatar;
	}

	/**
	 * @return the nickname
	 */
	public String getNickname() {
		return nickname;
	}

	/**
	 * @param nickname the nickname to set
	 */
	public void setNickname(String nickname) {
		this.nickname = nickname;
	}

	/**
	 * @return the timeCreated
	 */
	public Date getTimeCreated() {
		return timeCreated;
	}

	/**
	 * @param timeCreated the timeCreated to set
	 */
	public void setTimeCreated(Date timeCreated) {
		this.timeCreated = timeCreated;
	}

	/**
	 * @return the riskAppetite
	 */
	public int getRiskAppetite() {
		return riskAppetite;
	}

	/**
	 * @param riskAppetite the riskAppetite to set
	 */
	public void setRiskAppetite(int riskAppetite) {
		this.riskAppetite = riskAppetite;
	}

	/**
	 * @return the online
	 */
	public boolean isOnline() {
		return online;
	}

	/**
	 * @param online the online to set
	 */
	public void setOnline(boolean online) {
		this.online = online;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return the hitRate
	 */
	public int getHitRate() {
		return hitRate;
	}

	/**
	 * @param hitRate the hitRate to set
	 */
	public void setHitRate(int hitRate) {
		this.hitRate = hitRate;
	}

	/**
	 * @return the copiersCount
	 */
	public long getCopiersCount() {
		return copiersCount;
	}

	/**
	 * @param copiersCount the copiersCount to set
	 */
	public void setCopiersCount(long copiersCount) {
		this.copiersCount = copiersCount;
	}

	/**
	 * @return the watchersCount
	 */
	public long getWatchersCount() {
		return watchersCount;
	}

	/**
	 * @param watchersCount the watchersCount to set
	 */
	public void setWatchersCount(long watchersCount) {
		this.watchersCount = watchersCount;
	}

	/**
	 * @return the marketsTotalResult
	 */
	public long getMarketsTotalResult() {
		return marketsTotalResult;
	}

	/**
	 * @param marketsTotalResult the marketsTotalResult to set
	 */
	public void setMarketsTotalResult(long marketsTotalResult) {
		this.marketsTotalResult = marketsTotalResult;
	}

	/**
	 * @return the topMarkets
	 */
	public List<TopMarket> getTopMarkets() {
		return topMarkets;
	}

	/**
	 * @param topMarkets the topMarkets to set
	 */
	public void setTopMarkets(List<TopMarket> topMarkets) {
		this.topMarkets = topMarkets;
	}

	/**
	 * @return the bestAssetMarketId
	 */
	public long getBestAssetMarketId() {
		return bestAssetMarketId;
	}

	/**
	 * @param bestAssetMarketId the bestAssetMarketId to set
	 */
	public void setBestAssetMarketId(long bestAssetMarketId) {
		this.bestAssetMarketId = bestAssetMarketId;
	}

	/**
	 * @return the bestAssetMarketResult
	 */
	public long getBestAssetMarketResult() {
		return bestAssetMarketResult;
	}

	/**
	 * @param bestAssetMarketResult the bestAssetMarketResult to set
	 */
	public void setBestAssetMarketResult(long bestAssetMarketResult) {
		this.bestAssetMarketResult = bestAssetMarketResult;
	}

	/**
	 * @return the streakCount
	 */
	public int getStreakCount() {
		return streakCount;
	}

	/**
	 * @param streakCount the streakCount to set
	 */
	public void setStreakCount(int streakCount) {
		this.streakCount = streakCount;
	}

	/**
	 * @return the streakResult
	 */
	public long getStreakResult() {
		return streakResult;
	}

	/**
	 * @param streakResult the streakResult to set
	 */
	public void setStreakResult(long streakResult) {
		this.streakResult = streakResult;
	}

	/**
	 * @return the avgTrades
	 */
	public double getAvgTrades() {
		return avgTrades;
	}

	/**
	 * @param avgTrades the avgTrades to set
	 */
	public void setAvgTrades(double avgTrades) {
		this.avgTrades = avgTrades;
	}

	/**
	 * @return the avgTradesPeriod
	 */
	public int getAvgTradesPeriod() {
		return avgTradesPeriod;
	}

	/**
	 * @param avgTradesPeriod the avgTradesPeriod to set
	 */
	public void setAvgTradesPeriod(int avgTradesPeriod) {
		this.avgTradesPeriod = avgTradesPeriod;
	}

	/**
	 * @return the lastInvestments
	 */
	public List<? extends Investment> getLastInvestments() {
		return lastInvestments;
	}

	/**
	 * @param lastInvestments the lastInvestments to set
	 */
	public void setLastInvestments(List<? extends Investment> lastInvestments) {
		this.lastInvestments = lastInvestments;
	}

	/**
	 * @return the copiedProfiles
	 */
	public List<Profile> getCopiedProfiles() {
		return copiedProfiles;
	}

	/**
	 * @param copiedProfiles the copiedProfiles to set
	 */
	public void setCopiedProfiles(List<Profile> copiedProfiles) {
		this.copiedProfiles = copiedProfiles;
	}

	/**
	 * @return the copyingCount
	 */
	public long getCopyingCount() {
		return copyingCount;
	}

	/**
	 * @param copyingCount the copyingCount to set
	 */
	public void setCopyingCount(long copyingCount) {
		this.copyingCount = copyingCount;
	}

	/**
	 * @return the watchingCount
	 */
	public long getWatchingCount() {
		return watchingCount;
	}

	/**
	 * @param watchingCount the watchingCount to set
	 */
	public void setWatchingCount(long watchingCount) {
		this.watchingCount = watchingCount;
	}

	/**
	 * @return the fbFriendsWithLink
	 */
	public List<ProfileWithLink> getFbFriendsWithLink() {
		return fbFriendsWithLink;
	}

	/**
	 * @param fbFriendsWithLink the fbFriendsWithLink to set
	 */
	public void setFbFriendsWithLink(List<ProfileWithLink> fbFriendsWithLink) {
		this.fbFriendsWithLink = fbFriendsWithLink;
	}

	public int getCopiedInvestmentsTotalSuccess() {
		return copiedInvestmentsTotalSuccess;
	}

	public void setCopiedInvestmentsTotalSuccess(int copiedInvestmentsTotalSuccess) {
		this.copiedInvestmentsTotalSuccess = copiedInvestmentsTotalSuccess;
	}

	public List<TopTrader> getTopTraders() {
		return topTraders;
	}

	public void setTopTraders(List<TopTrader> topTraders) {
		this.topTraders = topTraders;
	}

	public List<Profile> getSimilarProfiles() {
		return similarProfiles;
	}

	public void setSimilarProfiles(List<Profile> similarProfiles) {
		this.similarProfiles = similarProfiles;
	}

	public boolean isFrozen() {
		return frozen;
	}

	public void setFrozen(boolean frozen) {
		this.frozen = frozen;
	}

	public List<Profile> getFbFriends() {
		return fbFriends;
	}

	public void setFbFriends(List<Profile> fbFriends) {
		this.fbFriends = fbFriends;
	}

	public boolean isWatching() {
		return watching;
	}

	public void setWatching(boolean watching) {
		this.watching = watching;
	}

	public boolean isCopying() {
		return copying;
	}

	public void setCopying(boolean copying) {
		this.copying = copying;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public long getShareCount() {
		return shareCount;
	}

	public void setShareCount(long shareCount) {
		this.shareCount = shareCount;
	}

	public long getRateCount() {
		return rateCount;
	}

	public void setRateCount(long rateCount) {
		this.rateCount = rateCount;
	}

	public boolean isShowHitRate() {
		return showHitRate;
	}

	public void setShowHitRate(boolean showHitRate) {
		this.showHitRate = showHitRate;
	}

	public boolean isOpenHitRate() {
		return openHitRate;
	}

	public void setOpenHitRate(boolean openHitRate) {
		this.openHitRate = openHitRate;
	}

	public class TopMarket {

		public static final int TOP_MARKETS_COUNT = 3;
		private long marketId;
		private int investmentsCount;
		private long result;
		private double resultDisplay;

		public TopMarket(long marketId, int copiedCount, long profit) {
			this.marketId = marketId;
			investmentsCount = copiedCount;
			result = profit;
			resultDisplay = profit / 100f;
		}

		/**
		 * @return the marketId
		 */
		public long getMarketId() {
			return marketId;
		}

		/**
		 * @param marketId the marketId to set
		 */
		public void setMarketId(long marketId) {
			this.marketId = marketId;
		}

		/**
		 * @return the investmentsCount
		 */
		public int getInvestmentsCount() {
			return investmentsCount;
		}

		/**
		 * @param investmentsCount the investmentsCount to set
		 */
		public void setInvestmentsCount(int investmentsCount) {
			this.investmentsCount = investmentsCount;
		}

		/**
		 * @return the result
		 */
		public long getResult() {
			return result;
		}

		/**
		 * @param result the result to set
		 */
		public void setResult(long result) {
			this.result = result;
			setResultDisplay(result / 100f);
		}

		public double getResultDisplay() {
			return resultDisplay;
		}

		public void setResultDisplay(double resultDisplay) {
			this.resultDisplay = resultDisplay;
		}
	}

	public class TopTrader {

		public static final int TOP_TRADERS_COUNT = 3;
		private long userId;
		private String nickname;
		private String avatar;
		private int investmentsCount;
		private long profit;
		private double profitDisplay;

		public TopTrader(long userId, int investmentsCount, long profit) {
			this.userId = userId;
			this.investmentsCount = investmentsCount;
			this.profit = profit;
			this.profitDisplay = profit / 100f;
		}

		public long getUserId() {
			return userId;
		}

		public void setUserId(long userId) {
			this.userId = userId;
		}

		public int getInvestmentsCount() {
			return investmentsCount;
		}

		public void setInvestmentsCount(int investmentsCount) {
			this.investmentsCount = investmentsCount;
		}

		public long getProfit() {
			return profit;
		}

		public void setProfit(long profit) {
			this.profit = profit;
			setProfitDisplay(profit / 100f);
		}

		public String getNickname() {
			return nickname;
		}

		public void setNickname(String nickname) {
			this.nickname = nickname;
		}
		
		public String getAvatar() {
			return avatar;
		}

		public void setAvatar(String avatar) {
			this.avatar = avatar;
		}

		public double getProfitDisplay() {
			return profitDisplay;
		}

		public void setProfitDisplay(double profitDisplay) {
			this.profitDisplay = profitDisplay;
		}
	}

	public class ProfileWithLink {

		private Profile profile;
		private ProfileLinkTypeEnum link;

		public ProfileWithLink(com.copyop.common.dto.base.Profile profile, ProfileLinkTypeEnum link) {
			this.profile = profile;
			this.link = link;
		}

		public Profile getProfile() {
			return profile;
		}

		public void setProfile(Profile profile) {
			this.profile = profile;
		}

		public ProfileLinkTypeEnum getLink() {
			return link;
		}

		public void setLink(ProfileLinkTypeEnum link) {
			this.link = link;
		}
	}
}