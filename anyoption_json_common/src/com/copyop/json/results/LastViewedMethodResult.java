/**
 * 
 */
package com.copyop.json.results;

import java.util.List;

import com.anyoption.common.service.results.MethodResult;

public class LastViewedMethodResult extends MethodResult {
	
	private List<LastViewedUser> lastViewedUser;

	public List<LastViewedUser> getLastViewedUser() {
		return lastViewedUser;
	}

	public void setLastViewedUser(List<LastViewedUser> lastViewedUser) {
		this.lastViewedUser = lastViewedUser;
	}

	public class LastViewedUser {

		public LastViewedUser(long userId, String avatar, String nickname) {
			super();
			this.userId = userId;
			this.avatar = avatar;
			this.nickname = nickname;
		}
		private long userId;
		private String avatar;
		private String nickname;
		
		public long getUserId() {
			return userId;
		}
		public void setUserId(long userId) {
			this.userId = userId;
		}
		public String getAvatar() {
			return avatar;
		}
		public void setAvatar(String avatar) {
			this.avatar = avatar;
		}
		public String getNickname() {
			return nickname;
		}
		public void setNickname(String nickname) {
			this.nickname = nickname;
		}

	}
}