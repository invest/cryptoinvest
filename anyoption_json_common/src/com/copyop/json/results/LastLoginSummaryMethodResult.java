package com.copyop.json.results;

import java.util.List;

import com.anyoption.common.service.results.MethodResult;

public class LastLoginSummaryMethodResult extends MethodResult {

	public final int TOP_TRADERS_COUNT = 3;
	private List<TopLogTrader> topLogTraders;

	public List<TopLogTrader> getTopLogTraders() {
		return topLogTraders;
	}

	public void setTopLogTraders(List<TopLogTrader> topLogTraders) {
		this.topLogTraders = topLogTraders;
	}
	
	public String toString() {
		String ls = System.getProperty("line.separator");
		return ls + "LastLoginSummaryMethodResult: " + super.toString()
				+ "topLogTraders: " + topLogTraders;
	}

	public class TopLogTrader {

		private long userId;
		private String nickname;
		private String avatar;
		private int investmentsCount;
		private double profit;
		private boolean showIncreaseAmount;
		private boolean isAfterLastLogOff;
		private boolean isFrozen;
		
		public TopLogTrader() {
			
		}
		
		public TopLogTrader(long userId, String nickname, String avatar, int investmentsCount, double profit, boolean showIncreaseAmount, boolean isAfterLastLogOff, boolean isFrozen) {
			this.userId = userId;
			this.nickname = nickname;
			this.avatar = avatar;			
			this.investmentsCount = investmentsCount;
			this.profit = profit;
			this.showIncreaseAmount = showIncreaseAmount;
			this.isAfterLastLogOff = isAfterLastLogOff;
			this.isFrozen = isFrozen;
		}		

		public long getUserId() {
			return userId;
		}

		public void setUserId(long userId) {
			this.userId = userId;
		}

		public int getInvestmentsCount() {
			return investmentsCount;
		}

		public void setInvestmentsCount(int investmentsCount) {
			this.investmentsCount = investmentsCount;
		}

		public double getProfit() {
			return profit;
		}

		public void setProfit(double profit) {
			this.profit = profit;
		}

		public String getNickname() {
			return nickname;
		}

		public void setNickname(String nickname) {
			this.nickname = nickname;
		}

		public String getAvatar() {
			return avatar;
		}

		public void setAvatar(String avatar) {
			this.avatar = avatar;
		}

		public boolean isShowIncreaseAmount() {
			return showIncreaseAmount;
		}

		public void setShowIncreaseAmount(boolean showIncreaseAmount) {
			this.showIncreaseAmount = showIncreaseAmount;
		}
		
		public boolean isAfterLastLogOff() {
			return isAfterLastLogOff;
		}

		public void setAfterLastLogOff(boolean isAfterLastLogOff) {
			this.isAfterLastLogOff = isAfterLastLogOff;
		}
		
		public boolean isFrozen() {
			return isFrozen;
		}

		public void setFrozen(boolean isFrozen) {
			this.isFrozen = isFrozen;
		}

		public String toString() {
			String ls = System.getProperty("line.separator");
			return ls + "TopLogTrader: " + super.toString()
					+ "userId: " + userId + ls + "nickname: " + nickname + ls
					+ "avatar: " + avatar + ls + "investmentsCount: "
					+ investmentsCount + ls + "profit: " + profit + ls
					+ "showIncreaseAmount: " + showIncreaseAmount + ls
					+ "isAfterLastLogOff: " + isAfterLastLogOff + ls
					+ "isFrozen: " + isFrozen + ls;
		}

	}
}
