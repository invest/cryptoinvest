package com.copyop.json.results;

import java.util.List;

import com.anyoption.common.service.results.MethodResult;
import com.copyop.common.dto.base.FeedMessage;

public class CoyopHotDataMethodResult extends MethodResult {

	private List<FeedMessage> feed;
	private List<Integer> existUpdateTypeEnumId;

	/**
	 * @return the feed
	 */
	public List<FeedMessage> getFeed() {
		return feed;
	}

	/**
	 * @param feed the feed to set
	 */
	public void setFeed(List<FeedMessage> feed) {
		this.feed = feed;
	}
	

	public List<Integer> getExistUpdateTypeEnumId() {
		return existUpdateTypeEnumId;
	}

	public void setExistUpdateTypeEnumId(List<Integer> existUpdateTypeEnumId) {
		this.existUpdateTypeEnumId = existUpdateTypeEnumId;
	}

	public String toString() {
		 String ls = System.getProperty("line.separator");
		 return ls + "FeedMethodResult: "
				 + super.toString()
				 + "feed: " + feed + ls
		 		 + "existUpdateTypeEnumId: " + existUpdateTypeEnumId + ls;
	}
}