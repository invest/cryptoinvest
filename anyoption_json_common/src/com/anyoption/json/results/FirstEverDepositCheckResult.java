/**
 * 
 */
package com.anyoption.json.results;

import com.anyoption.common.service.results.MethodResult;


/**
 * @author pavelhe
 *
 */
public class FirstEverDepositCheckResult extends MethodResult {
	private boolean firstEverDeposit;

	/**
	 * @return the firstEverDeposit
	 */
	public boolean isFirstEverDeposit() {
		return firstEverDeposit;
	}

	/**
	 * @param firstEverDeposit the firstEverDeposit to set
	 */
	public void setFirstEverDeposit(boolean firstEverDeposit) {
		this.firstEverDeposit = firstEverDeposit;
	}

	@Override
	public String toString() {
		return "FirstEverDepositCheckResult [firstEverDeposit="
				+ firstEverDeposit + ", toString()=" + super.toString() + "]";
	}
}
