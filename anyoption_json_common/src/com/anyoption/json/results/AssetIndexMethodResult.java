/**
 * 
 */
package com.anyoption.json.results;

import com.anyoption.common.beans.base.AssetIndexBase;
import com.anyoption.common.service.results.MethodResult;

/**
 * @author AviadH
 *
 */
public class AssetIndexMethodResult extends MethodResult {
	
	private AssetIndexBase assetIndex; 

	/**
	 * @return the assetIndex
	 */
	public AssetIndexBase getAssetIndex() {
		return assetIndex;
	}

	/**
	 * @param assetIndex the assetIndex to set
	 */
	public void setAssetIndex(AssetIndexBase assetIndex) {
		this.assetIndex = assetIndex;
	}
	
}
