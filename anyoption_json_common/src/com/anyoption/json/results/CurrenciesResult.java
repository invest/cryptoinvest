package com.anyoption.json.results;

import java.util.HashMap;

import com.anyoption.common.beans.base.Currency;
import com.anyoption.common.service.results.MethodResult;


public class CurrenciesResult extends MethodResult{

	protected Currency[] currency;
	protected HashMap<Long, String> predefinedDepositAmountMap;

	public Currency[] getCurrency() {
		return currency;
	}

	public void setCurrency(Currency[] currency) {
		this.currency = currency;
	}

	public HashMap<Long, String> getPredefinedDepositAmountMap() {
		return predefinedDepositAmountMap;
	}

	public void setPredefinedDepositAmountMap(HashMap<Long, String> predefinedDepositAmountMap) {
		this.predefinedDepositAmountMap = predefinedDepositAmountMap;
	}
	
}
