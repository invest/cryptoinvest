/**
 * 
 */
package com.anyoption.json.results;

import com.anyoption.common.service.results.MethodResult;


/**
 * 
 * @author liors
 *
 */
public class RegisterAttemptMethodResult extends MethodResult {
	private long registerAttemptId; //contactId  
	
    //Marketing Tracking
    private String mId;
    private String marketingStaticPart;
    private String etsMId;

	/**
	 * @return the registerAttemptId
	 */
	public long getRegisterAttemptId() {
		return registerAttemptId;
	}

	/**
	 * @param registerAttemptId the registerAttemptId to set
	 */
	public void setRegisterAttemptId(long registerAttemptId) {
		this.registerAttemptId = registerAttemptId;
	}

    /**
     * @return the mId
     */
    public String getmId() {
        return mId;
    }

    /**
     * @param mId the mId to set
     */
    public void setmId(String mId) {
        this.mId = mId;
    }

    /**
     * @return the marketingStaticPart
     */
    public String getMarketingStaticPart() {
        return marketingStaticPart;
    }

    /**
     * @param marketingStaticPart the marketingStaticPart to set
     */
    public void setMarketingStaticPart(String marketingStaticPart) {
        this.marketingStaticPart = marketingStaticPart;
    }

    /**
     * @return the etsMId
     */
    public String getEtsMId() {
        return etsMId;
    }

    /**
     * @param etsMId the etsMId to set
     */
    public void setEtsMId(String etsMId) {
        this.etsMId = etsMId;
    }

}
