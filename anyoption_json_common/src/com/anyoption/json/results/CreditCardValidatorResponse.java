package com.anyoption.json.results;

public class CreditCardValidatorResponse {
	boolean valid;
	boolean diners;
	private boolean amex;

	public boolean isValid() {
		return valid;
	}

	public void setValid(boolean valid) {
		this.valid = valid;
	}

	public boolean isDiners() {
		return diners;
	}

	public void setDiners(boolean diners) {
		this.diners = diners;
	}
	
	public boolean isAmex() {
		return amex;
	}

	public void setAmex(boolean amex) {
		this.amex = amex;
	}

	@Override
	public String toString() {
		return "CreditCardValidatorResponse [valid=" + valid + ", diners=" + diners + ", amex=" + amex + "]";
	}
	
	
}
