package com.anyoption.json.results;

import com.anyoption.common.service.results.MethodResult;

/**
 * 
 * @author liors
 *
 */
public class UpdateMethodResult extends MethodResult {
    protected boolean needAPKUpdate;
    protected boolean forceAPKUpdate;
    protected String downloadLink;
    protected long skinId;
    protected long serverTime;
    protected long countryId;
    protected long combId;
    protected long currencyId;
    protected String dynamicParam;
    // TODO set aff_sub
    protected String aff_sub1;
    protected String aff_sub2;
    protected String aff_sub3;
    
    protected String mId;    
    protected String etsMid;
    
    protected boolean isHasDynamics;

	/**
	 * @return
	 */
	public boolean isForceAPKUpdate() {
		return forceAPKUpdate;
	}

	/**
	 * @param forceAPKUpdate
	 */
	public void setForceAPKUpdate(boolean forceAPKUpdate) {
		this.forceAPKUpdate = forceAPKUpdate;
	}

	/**
	 * @return
	 */
	public boolean isNeedAPKUpdate() {
		return needAPKUpdate;
	}

	/**
	 * @param needAPKUpdate
	 */
	public void setNeedAPKUpdate(boolean needAPKUpdate) {
		this.needAPKUpdate = needAPKUpdate;
	}

    /**
     * @return
     */
    public long getSkinId() {
        return skinId;
    }

    /**
     * @param skinId
     */
    public void setSkinId(long skinId) {
        this.skinId = skinId;
    }

    /**
     * @return
     */
    public long getServerTime() {
        return serverTime;
    }

    /**
     * @param serverTime
     */
    public void setServerTime(long serverTime) {
        this.serverTime = serverTime;
    }

	/**
	 * @return the downloadLink
	 */
	public String getDownloadLink() {
		return downloadLink;
	}

	/**
	 * @param downloadLink the downloadLink to set
	 */
	public void setDownloadLink(String downloadLink) {
		this.downloadLink = downloadLink;
	}

	/**
	 * @return the countryId
	 */
	public long getCountryId() {
		return countryId;
	}

	/**
	 * @param countryId the countryId to set
	 */
	public void setCountryId(long countryId) {
		this.countryId = countryId;
	}

    public long getCombId() {
        return combId;
    }

    public void setCombId(long combId) {
        this.combId = combId;
    }

    public String getDynamicParam() {
        return dynamicParam;
    }

    public void setDynamicParam(String dynamicParam) {
        this.dynamicParam = dynamicParam;
    }

	public String getmId() {
		return mId;
	}

	public void setmId(String mId) {
		this.mId = mId;
	}

    /**
     * @return the etsMid
     */
    public String getEtsMid() {
        return etsMid;
    }

    /**
     * @param etsMid the etsMid to set
     */
    public void setEtsMid(String etsMid) {
        this.etsMid = etsMid;
    }

	public String getAff_sub1() {
		return aff_sub1;
	}

	public void setAff_sub1(String aff_sub1) {
		this.aff_sub1 = aff_sub1;
	}

	public String getAff_sub2() {
		return aff_sub2;
	}

	public void setAff_sub2(String aff_sub2) {
		this.aff_sub2 = aff_sub2;
	}

	public String getAff_sub3() {
		return aff_sub3;
	}

	public void setAff_sub3(String aff_sub3) {
		this.aff_sub3 = aff_sub3;
	}

	public long getCurrencyId() {
		return currencyId;
	}

	public void setCurrencyId(long currencyId) {
		this.currencyId = currencyId;
	}
	

	public boolean isHasDynamics() {
		return isHasDynamics;
	}

	public void setHasDynamics(boolean isHasDynamics) {
		this.isHasDynamics = isHasDynamics;
	}

	@Override
	public String toString() {
		return "UpdateMethodResult [needAPKUpdate=" + needAPKUpdate
				+ ", forceAPKUpdate=" + forceAPKUpdate + ", downloadLink="
				+ downloadLink + ", skinId=" + skinId + ", serverTime="
				+ serverTime + ", countryId=" + countryId + ", combId="
				+ combId + ", currencyId=" + currencyId + ", dynamicParam="
				+ dynamicParam + ", aff_sub1=" + aff_sub1 + ", aff_sub2="
				+ aff_sub2 + ", aff_sub3=" + aff_sub3 + ", mId=" + mId
				+ ", etsMid=" + etsMid + ", toString()=" + super.toString()
				+ "]";
	}
}