/**
 * 
 */
package com.anyoption.json.results;

import com.anyoption.common.service.results.MethodResult;


/**
 * @author IdanZ
 *
 */
public class CurrencyMethodResult extends MethodResult {
	
	private String currencyLetters;

	/**
	 * @return currency in Letters
	 */
	public String getCurrencyLetters() {
		return currencyLetters;
	}

	/**
	 * @param currencyLetters
	 */
	public void setCurrencyLetters(String currencyLetters) {
		this.currencyLetters = currencyLetters;
	}
}
