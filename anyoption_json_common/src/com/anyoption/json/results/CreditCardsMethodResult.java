package com.anyoption.json.results;

import com.anyoption.common.beans.base.CreditCard;
import com.anyoption.common.service.results.MethodResult;

/**
 * @author EranL
 *
 */
public class CreditCardsMethodResult extends MethodResult {	
	 protected CreditCard[] options;

	/**
	 * @return the options
	 */
	public CreditCard[] getOptions() {
		return options;
	}

	/**
	 * @param options the options to set
	 */
	public void setOptions(CreditCard[] options) {
		this.options = options;
	}
	 
}