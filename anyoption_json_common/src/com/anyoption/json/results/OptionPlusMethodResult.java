/**
 * 
 */
package com.anyoption.json.results;

import com.anyoption.common.service.results.MethodResult;


/**
 * @author AviadH
 *
 */
public class OptionPlusMethodResult extends MethodResult{

	private String price;
	private String priceTxt;

	/**
	 * @return
	 */
	public String getPriceTxt() {
		return priceTxt;
	}

	/**
	 * @param priceTxt
	 */
	public void setPriceTxt(String priceTxt) {
		this.priceTxt = priceTxt;
	}

	/**
	 * @return
	 */
	public String getPrice() {
		return price;
	}

	/**
	 * @param price
	 */
	public void setPrice(String price) {
		this.price = price;
	}
}
