package com.anyoption.json.results;

import com.anyoption.common.service.results.MethodResult;
import com.anyoption.json.util.SkinOptionItem;

/**
 * 
 * @author liors
 *
 */
public class SkinsOptionsMethodResult extends MethodResult {
    protected SkinOptionItem[] options;

	/**
	 * @return the options
	 */
	public SkinOptionItem[] getOptions() {
		return options;
	}

	/**
	 * @param options the options to set
	 */
	public void setOptions(SkinOptionItem[] options) {
		this.options = options;
	}

}