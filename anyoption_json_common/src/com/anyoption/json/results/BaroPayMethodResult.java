package com.anyoption.json.results;

import com.anyoption.common.service.results.MethodResult;



/**
 * @author EranL
 *
 */
public class BaroPayMethodResult extends MethodResult {
    protected boolean allowBaroPayWithdraw ;

	/**
	 * @return the allowBaroPayWithdraw
	 */
	public boolean isAllowBaroPayWithdraw() {
		return allowBaroPayWithdraw;
	}

	/**
	 * @param allowBaroPayWithdraw the allowBaroPayWithdraw to set
	 */
	public void setAllowBaroPayWithdraw(boolean allowBaroPayWithdraw) {
		this.allowBaroPayWithdraw = allowBaroPayWithdraw;
	}
  
}