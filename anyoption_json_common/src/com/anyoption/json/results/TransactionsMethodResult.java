package com.anyoption.json.results;

import com.anyoption.common.beans.base.Transaction;
import com.anyoption.common.service.results.MethodResult;

/**
 * 
 * @author liors
 *
 */
public class TransactionsMethodResult extends MethodResult {
    protected Transaction[] transactions;

	/**
	 * @return the transactions
	 */
	public Transaction[] getTransactions() {
		return transactions;
	}

	/**
	 * @param transactions the transactions to set
	 */
	public void setTransactions(Transaction[] transactions) {
		this.transactions = transactions;
	}

}