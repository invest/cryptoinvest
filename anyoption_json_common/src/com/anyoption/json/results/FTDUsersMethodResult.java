package com.anyoption.json.results;

import com.anyoption.common.beans.base.FTDUser;
import com.anyoption.common.service.results.MethodResult;
import com.google.gson.annotations.Expose;

/**
 * @author EranL
 *
 */
public class FTDUsersMethodResult extends MethodResult {

	@Expose
	private FTDUser[] FTDUsersList;

	/**
	 * @return the fTDUsersList
	 */
	public FTDUser[] getFTDUsersList() {
		return FTDUsersList;
	}

	/**
	 * @param fTDUsersList the fTDUsersList to set
	 */
	public void setFTDUsersList(FTDUser[] fTDUsersList) {
		FTDUsersList = fTDUsersList;
	}

}
