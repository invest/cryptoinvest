package com.anyoption.json.results;

import com.anyoption.common.service.results.MethodResult;


public class CupWithdrawalMethodResult extends MethodResult {
	private long maxCupWithdrawal;

	public long getMaxCupWithdrawal() {
		return maxCupWithdrawal;
	}

	public void setMaxCupWithdrawal(long maxCupWithdrawal) {
		this.maxCupWithdrawal = maxCupWithdrawal;
	}

}
