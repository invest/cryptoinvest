package com.anyoption.json.results;

import com.anyoption.common.service.results.UserMethodResult;

/**
 * 
 * @author liors
 *
 */
public class InsurancesMethodResult extends UserMethodResult {
    protected Long[] investmentIds;
    protected String[] results;
    protected String credit;

    /**
     * @return list of investments id 
     */
    public Long[] getInvestmentIds() {
        return investmentIds;
    }

    /**
     * @param investmentIds
     */
    public void setInvestmentIds(Long[] investmentIds) {
        this.investmentIds = investmentIds;
    }

    /**
     * @return
     */
    public String[] getResults() {
        return results;
    }

    /**
     * @param results
     */
    public void setResults(String[] results) {
        this.results = results;
    }

    /**
     * @return credit
     */
    public String getCredit() {
        return credit;
    }

    /**
     * @param credit
     */
    public void setCredit(String credit) {
        this.credit = credit;
    }
    
    /* (non-Javadoc)
     * @see com.anyoption.json.results.UserMethodResult#toString()
     */
    public String toString() {
        String ls = System.getProperty("line.separator");
        return ls + "InsurancesMethodResult: "
            + super.toString()
            + "investmentIds: " + investmentIds + ls
            + "results: " + results + ls
            + "credit: " + credit + ls;
    }
}