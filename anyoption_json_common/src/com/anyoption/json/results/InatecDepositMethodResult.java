package com.anyoption.json.results;

import com.anyoption.common.service.results.MethodResult;


/**
 * @author kirilim
 */
public class InatecDepositMethodResult extends MethodResult {

	private long transactionId;
	private String redirectUrl;
	private boolean success;

	public long getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(long transactionId) {
		this.transactionId = transactionId;
	}

	public String getRedirectUrl() {
		return redirectUrl;
	}

	public void setRedirectUrl(String redirectUrl) {
		this.redirectUrl = redirectUrl;
	}

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	@Override
	public String toString() {
		 String ls = System.getProperty("line.separator");
	        return ls + "InatecDepositMethodResult: "
	        	+ super.toString()
	        	+ "transactionId: " + transactionId + ls
	        	+ "redirectUrl: " + redirectUrl + ls
	        	+ "success: " + success;
	}
}