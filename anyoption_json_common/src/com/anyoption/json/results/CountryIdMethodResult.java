/**
 * 
 */
package com.anyoption.json.results;

import com.anyoption.common.service.results.MethodResult;


/**
 * @author EranL
 *
 */
public class CountryIdMethodResult extends MethodResult {
	
	private long countryId;
	private long countryStatus;

	/**
	 * @return the countryId
	 */
	public long getCountryId() {
		return countryId;
	}

	/**
	 * @param countryId the countryId to set
	 */
	public void setCountryId(long countryId) {
		this.countryId = countryId;
	}

	/**
	 * @return the countryStatus
	 */
	public long getCountryStatus() {
		return countryStatus;
	}

	/**
	 * @param countryStatus the countryStatus to set
	 */
	public void setCountryStatus(long countryStatus) {
		this.countryStatus = countryStatus;
	} 
	
}
