package com.anyoption.json.requests;

import com.anyoption.common.service.requests.UserMethodRequest;

/**
 * @author kirilim
 */
public class InatecDepositFinishMethodRequest extends UserMethodRequest {

	private long inatecTransactionId;
	private String cs;

	public long getInatecTransactionId() {
		return inatecTransactionId;
	}

	public void setInatecTransactionId(long inatecTransactionId) {
		this.inatecTransactionId = inatecTransactionId;
	}

	public String getCs() {
		return cs;
	}

	public void setCs(String cs) {
		this.cs = cs;
	}

	@Override
	public String toString() {
		String ls = System.getProperty("line.separator");
		return ls + "InatecDepositFinishMethodRequest: "
				+ super.toString()
				+ "inatecTransactionId: " + inatecTransactionId + ls
				+ "cs: " + cs + ls;
	}
}