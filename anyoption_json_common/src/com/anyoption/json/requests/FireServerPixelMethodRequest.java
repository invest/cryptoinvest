package com.anyoption.json.requests;

import com.anyoption.common.service.requests.UserMethodRequest;

/**
 *
 * @author EyalG
 *
 */
public class FireServerPixelMethodRequest extends UserMethodRequest {
	public int publisherId;
	public String publisherIdentifier;
	public long userId;
	public long transactionId;
	public int pixelTypeId;
	
	public int getPublisherId() {
		return publisherId;
	}
	
	public void setPublisherId(int publisherId) {
		this.publisherId = publisherId;
	}
	
	public String getPublisherIdentifier() {
		return publisherIdentifier;
	}
	
	public void setPublisherIdentifier(String publisherIdentifier) {
		this.publisherIdentifier = publisherIdentifier;
	}
	
	public long getUserId() {
		return userId;
	}
	
	public void setUserId(long userId) {
		this.userId = userId;
	}
	
	public long getTransactionId() {
		return transactionId;
	}
	
	public void setTransactionId(long transactionId) {
		this.transactionId = transactionId;
	}

	public int getPixelTypeId() {
		return pixelTypeId;
	}

	public void setPixelTypeId(int pixelTypeId) {
		this.pixelTypeId = pixelTypeId;
	}

}
