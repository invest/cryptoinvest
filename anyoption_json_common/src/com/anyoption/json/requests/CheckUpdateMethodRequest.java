package com.anyoption.json.requests;

import com.anyoption.common.service.requests.MethodRequest;


/**
 * contains apkVersion, apkName, combinationId and c2dmRegistrationId.
 * extends from MethodRequest.
 * 
 * c2dmRegistrationId: with this id we can send push notification to client.
 * 
 * @author liors
 *
 */
public class CheckUpdateMethodRequest extends MethodRequest {
    protected int apkVersion;
    protected String apkName;
    protected Long combinationId;
    protected String c2dmRegistrationId;
    protected String dynamicParam;
    protected String affSub1; 
    protected String affSub2;
    protected String affSub3;
    
    protected String mId;
    protected boolean isFirstOpening;
    
    protected String etsMId;
    protected String utmSource;
    
    protected String appsflyerId;
    protected int osTypeId;
    protected String deviceFamily;
    protected String idfa;
    protected String advertisingId;

	/**
	 * @return apkVersion
	 */
	public int getApkVersion() {
		return apkVersion;
	}

	/**
	 * @param apkVersion
	 */
	public void setApkVersion(int apkVersion) {
		this.apkVersion = apkVersion;
	}

	/**
	 * @return apkName
	 */
	public String getApkName() {
		return apkName;
	}

	/**
	 * @param apkName
	 */
	public void setApkName(String apkName) {
		this.apkName = apkName;
	}

    /**
     * @return combinationId
     */
    public Long getCombinationId() {
        return combinationId;
    }

    /**
     * @param combinationId
     */
    public void setCombinationId(Long combinationId) {
        this.combinationId = combinationId;
    }

    /**
     * @return c2dmRegistrationId
     */
    public String getC2dmRegistrationId() {
		return c2dmRegistrationId;
	}

	/**
	 * @param registrationId
	 */
	public void setC2dmRegistrationId(String registrationId) {
		c2dmRegistrationId = registrationId;
	}

	public String getDynamicParam() {
		return dynamicParam;
	}

	public void setDynamicParam(String dynamicParam) {
		this.dynamicParam = dynamicParam;
	}

	/* (non-Javadoc)
	 * @see com.anyoption.json.requests.MethodRequest#toString()
	 */
	public String toString() {
        String ls = System.getProperty("line.separator");
        return ls + "CheckUpdateMethodRequest: "
            + super.toString()
            + "apkVersion: " + apkVersion + ls
            + "apkName: " + apkName + ls
            + "combinationId: " + combinationId + ls
        	+ "c2dmRegistrationId: " + c2dmRegistrationId + ls
        	+ "dynamicParam: " + dynamicParam + ls
        	+ "mId: " + mId + ls
        	+ "isFirstOpening: " + isFirstOpening + ls
        	+ "osTypeId: " + osTypeId + ls
        	+ "appsflyerId: " + appsflyerId + ls
        	+ "idfa: " + idfa + ls
        	+ "advertisingId: " + advertisingId + ls
        	+ "deviceFamily: " + deviceFamily;
    }

	public String getmId() {
		return mId;
	}

	public void setmId(String mId) {
		this.mId = mId;
	}

	public boolean isFirstOpening() {
		return isFirstOpening;
	}

	public void setFirstOpening(boolean isFirstOpening) {
		this.isFirstOpening = isFirstOpening;
	}

    /**
     * @return the etsMId
     */
    public String getEtsMId() {
        return etsMId;
    }

    /**
     * @param etsMId the etsMId to set
     */
    public void setEtsMId(String etsMId) {
        this.etsMId = etsMId;
    }

	public String getUtmSource() {
		return utmSource;
	}

	public void setUtmSource(String utmSource) {
		this.utmSource = utmSource;
	}

	public String getAppsflyerId() {
		return appsflyerId;
	}

	public void setAppsflyerId(String appsflyerId) {
		this.appsflyerId = appsflyerId;
	}

	public int getOsTypeId() {
		return osTypeId;
	}

	public void setOsTypeId(int osTypeId) {
		this.osTypeId = osTypeId;
	}

	public String getDeviceFamily() {
		return deviceFamily;
	}

	public void setDeviceFamily(String deviceFamily) {
		this.deviceFamily = deviceFamily;
	}

	public String getAffSub1() {
		return affSub1;
	}

	public void setAffSub1(String affSub1) {
		this.affSub1 = affSub1;
	}

	public String getAffSub2() {
		return affSub2;
	}

	public void setAffSub2(String affSub2) {
		this.affSub2 = affSub2;
	}

	public String getAffSub3() {
		return affSub3;
	}

	public void setAffSub3(String affSub3) {
		this.affSub3 = affSub3;
	}

	/**
	 * @return the idfa
	 */
	public String getIdfa() {
		return idfa;
	}

	/**
	 * @param idfa the idfa to set
	 */
	public void setIdfa(String idfa) {
		this.idfa = idfa;
	}

	/**
	 * @return the advertisingId
	 */
	public String getAdvertisingId() {
		return advertisingId;
	}

	/**
	 * @param advertisingId the advertisingId to set
	 */
	public void setAdvertisingId(String advertisingId) {
		this.advertisingId = advertisingId;
	}
	
}