package com.anyoption.json.requests;

import java.util.ArrayList;

import com.anyoption.common.beans.base.ApiNotificationResponse;
import com.anyoption.common.service.results.MethodResult;
import com.google.gson.annotations.Expose;

/**
 * @author EranL
 *
 */
public class ApproveNotificationMethodResponse extends MethodResult {
    @Expose
	ArrayList<ApiNotificationResponse> notificationsResponse;

	/**
	 * @return the notificationsResponse
	 */
	public ArrayList<ApiNotificationResponse> getNotificationsResponse() {
		return notificationsResponse;
	}

	/**
	 * @param notificationsResponse the notificationsResponse to set
	 */
	public void setNotificationsResponse(
			ArrayList<ApiNotificationResponse> notificationsResponse) {
		this.notificationsResponse = notificationsResponse;
	}

}

