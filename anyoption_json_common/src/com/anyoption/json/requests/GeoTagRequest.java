package com.anyoption.json.requests;

public class GeoTagRequest {
	String ip;

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	@Override
	public String toString() {
		return "GeoTagRequest [ip=" + ip + "]";
	}
	
}
