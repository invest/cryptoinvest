///**
// * 
// */
//package com.anyoption.json.requests;
//
//import java.util.List;
//
//import com.anyoption.common.beans.base.QuestionnaireUserAnswer;
//import com.anyoption.common.service.requests.UserMethodRequest;
//
///**
// * @author pavelhe
// *
// */
//public class UpdateUserQuestionnaireAnswersRequest extends UserMethodRequest {
//	
//	private List<QuestionnaireUserAnswer> userAnswers;
//
//	/**
//	 * @return the userAnswers
//	 */
//	public List<QuestionnaireUserAnswer> getUserAnswers() {
//		return userAnswers;
//	}
//
//	/**
//	 * @param userAnswers the userAnswers to set
//	 */
//	public void setUserAnswers(List<QuestionnaireUserAnswer> userAnswers) {
//		this.userAnswers = userAnswers;
//	}
//	
//	public String toString() {
//		String ls = System.getProperty("line.separator");
//		return ls + "UpdateUserQuestionnaireAnswersRequest: "
//				    + super.toString()
//				    + "userAnswers: " + userAnswers + ls;
//    }
//
//}
