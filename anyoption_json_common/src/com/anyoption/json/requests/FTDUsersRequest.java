package com.anyoption.json.requests;

import com.anyoption.common.service.requests.MethodRequest;


/**
 * @author EranL
 *
 */
public class FTDUsersRequest extends MethodRequest {
	
	protected long apiUserId;
	protected String dateRequest;
	

	/**
	 * @return the apiUserId
	 */
	public long getApiUserId() {
		return apiUserId;
	}
	/**
	 * @param apiUserId the apiUserId to set
	 */
	public void setApiUserId(long apiUserId) {
		this.apiUserId = apiUserId;
	}
	/**
	 * @return the dateRequest
	 */
	public String getDateRequest() {
		return dateRequest;
	}
	/**
	 * @param dateRequest the dateRequest to set
	 */
	public void setDateRequest(String dateRequest) {
		this.dateRequest = dateRequest;
	}

}