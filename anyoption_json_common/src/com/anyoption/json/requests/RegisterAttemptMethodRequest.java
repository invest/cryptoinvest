package com.anyoption.json.requests;

import com.anyoption.common.service.requests.MethodRequest;


/**
 * 
 * @author liors
 *
 */
public class RegisterAttemptMethodRequest extends MethodRequest {
	
	private String email;	
	private String firstName;
    private String lastName;
    private String mobilePhone;
    private String landLinePhone;
    private Long combinationId;
	private boolean isContactByEmail;
	private boolean isContactBySMS;
	private long countryId;
	private String dynamicParameter;
	private String aff_sub1;
	private String aff_sub2;
	private String aff_sub3;

    private long id;
	private String userAgent;
	private String httpReferer;
	
    //Marketing Tracking
    private String mId;
    private String marketingStaticPart;
    private String etsMId;
	
	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}
	/**
	 * @param email the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}
	/**
	 * @return the firstName
	 */
	public String getFirstName() {
		return firstName;
	}
	/**
	 * @param firstName the firstName to set
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	/**
	 * @return the lastName
	 */
	public String getLastName() {
		return lastName;
	}
	/**
	 * @param lastName the lastName to set
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	/**
	 * @return the mobilePhone
	 */
	public String getMobilePhone() {
		return mobilePhone;
	}
	/**
	 * @param mobilePhone the mobilePhone to set
	 */
	public void setMobilePhone(String mobilePhone) {
		this.mobilePhone = mobilePhone;
	}
	/**
	 * @return the landLinePhone
	 */
	public String getLandLinePhone() {
		return landLinePhone;
	}
	/**
	 * @param landLinePhone the landLinePhone to set
	 */
	public void setLandLinePhone(String landLinePhone) {
		this.landLinePhone = landLinePhone;
	}
	/**
	 * @return the isContactByEmail
	 */
	public boolean isContactByEmail() {
		return isContactByEmail;
	}
	/**
	 * @param isContactByEmail the isContactByEmail to set
	 */
	public void setContactByEmail(boolean isContactByEmail) {
		this.isContactByEmail = isContactByEmail;
	}
	/**
	 * @return the isContactBySMS
	 */
	public boolean isContactBySMS() {
		return isContactBySMS;
	}
	/**
	 * @param isContactBySMS the isContactBySMS to set
	 */
	public void setContactBySMS(boolean isContactBySMS) {
		this.isContactBySMS = isContactBySMS;
	}
	/**
	 * @return the combinationId
	 */
	public Long getCombinationId() {
		return combinationId;
	}
	/**
	 * @param combinationId the combinationId to set
	 */
	public void setCombinationId(Long combinationId) {
		this.combinationId = combinationId;
	}
	/**
	 * @return the countryId
	 */
	public long getCountryId() {
		return countryId;
	}
	/**
	 * @param countryId the countryId to set
	 */
	public void setCountryId(long countryId) {
		this.countryId = countryId;
	}
	/**
	 * @return the dynamicParameter
	 */
	public String getDynamicParameter() {
		return dynamicParameter;
	}
	/**
	 * @param dynamicParameter the dynamicParameter to set
	 */
	public void setDynamicParameter(String dynamicParameter) {
		this.dynamicParameter = dynamicParameter;
	}
	/**
	 * @return the userAgent
	 */
	public String getUserAgent() {
		return userAgent;
	}
	/**
	 * @param userAgent the userAgent to set
	 */
	public void setUserAgent(String userAgent) {
		this.userAgent = userAgent;
	}
	/**
	 * @return the httpReferer
	 */
	public String getHttpReferer() {
		return httpReferer;
	}
	/**
	 * @param httpReferer the httpReferer to set
	 */
	public void setHttpReferer(String httpReferer) {
		this.httpReferer = httpReferer;
	}
	/**
	 * @return the id
	 */
	public long getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(long id) {
		this.id = id;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "RegisterAttemptMethodRequest [email=" + email + ", firstName="
				+ firstName + ", lastName=" + lastName + ", mobilePhone="
				+ mobilePhone + ", landLinePhone=" + landLinePhone
				+ ", combinationId=" + combinationId + ", isContactByEmail="
				+ isContactByEmail + ", isContactBySMS=" + isContactBySMS
				+ ", countryId=" + countryId + ", dynamicParameter="
				+ dynamicParameter + ", id=" + id + ", userAgent=" + userAgent
				+ ", etsMId=" + etsMId
				+ ", httpReferer=" + httpReferer + "]";
	}
    /**
     * @return the mId
     */
    public String getmId() {
        return mId;
    }
    /**
     * @param mId the mId to set
     */
    public void setmId(String mId) {
        this.mId = mId;
    }
    /**
     * @return the marketingStaticPart
     */
    public String getMarketingStaticPart() {
        return marketingStaticPart;
    }
    /**
     * @param marketingStaticPart the marketingStaticPart to set
     */
    public void setMarketingStaticPart(String marketingStaticPart) {
        this.marketingStaticPart = marketingStaticPart;
    }
    /**
     * @return the etsMId
     */
    public String getEtsMId() {
        return etsMId;
    }
    /**
     * @param etsMId the etsMId to set
     */
    public void setEtsMId(String etsMId) {
        this.etsMId = etsMId;
    }
    
	public String getAff_sub1() {
		return aff_sub1;
	}
	
	public void setAff_sub1(String aff_sub1) {
		this.aff_sub1 = aff_sub1;
	}
	
	public String getAff_sub2() {
		return aff_sub2;
	}
	
	public void setAff_sub2(String aff_sub2) {
		this.aff_sub2 = aff_sub2;
	}
	
	public String getAff_sub3() {
		return aff_sub3;
	}
	
	public void setAff_sub3(String aff_sub3) {
		this.aff_sub3 = aff_sub3;
	}
}