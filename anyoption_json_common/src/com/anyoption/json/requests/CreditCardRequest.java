package com.anyoption.json.requests;

import com.anyoption.common.service.requests.UserMethodRequest;

public class CreditCardRequest extends UserMethodRequest{
	private int cardId;

	public int getCardId() {
		return cardId;
	}

	public void setCardId(int cardId) {
		this.cardId = cardId;
	}
	
	public String toString() {
		String ls = System.getProperty("line.separator");
		return ls + "CreditCardRequest: " + ls
				+ super.toString() + ls
				+ "cardId: " + cardId + ls;
	}
}