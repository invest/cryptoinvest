/**
 * 
 */
package com.anyoption.json.requests;

import com.anyoption.common.service.requests.MethodRequest;


/**
 * @author AviadH
 *
 */
public class AssetIndexMethodRequest extends MethodRequest {
	
	private long marketId;

	/**
	 * @return the marketId
	 */
	public long getMarketId() {
		return marketId;
	}

	/**
	 * @param marketId the marketId to set
	 */
	public void setMarketId(long marketId) {
		this.marketId = marketId;
	}
	
}
