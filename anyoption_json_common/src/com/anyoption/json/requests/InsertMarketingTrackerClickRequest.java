package com.anyoption.json.requests;

import com.anyoption.common.service.requests.MethodRequest;


public class InsertMarketingTrackerClickRequest extends MethodRequest {
	
	private String staticPart; 
	private String dynamicPart;
	/**
	 * @return the staticPart
	 */
	public String getStaticPart() {
		return staticPart;
	}
	/**
	 * @param staticPart the staticPart to set
	 */
	public void setStaticPart(String staticPart) {
		this.staticPart = staticPart;
	}
	/**
	 * @return the dynamicPart
	 */
	public String getDynamicPart() {
		return dynamicPart;
	}
	/**
	 * @param dynamicPart the dynamicPart to set
	 */
	public void setDynamicPart(String dynamicPart) {
		this.dynamicPart = dynamicPart;
	}

}
