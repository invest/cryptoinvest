///**
// * 
// */
//package com.anyoption.json.requests;
//
//import java.util.Calendar;
//import java.util.Date;
//
//import com.anyoption.common.service.requests.UserMethodRequest;
//
///**
// * @author AviadH
// *
// */
//public class InvestmentsMethodRequest extends UserMethodRequest {
//	
//	private String accountId;
//	private String accountPassword;
//	private long clientId;
//	private Calendar from;
//	private long fromEpoch;
//	private Calendar to;
//	private long toEpoch;
//	private boolean isSettled;
//	private long marketId;
//	private int startRow;
//	private int pageSize;
//	private String apiExternalUserReference;
//	private int period;
//	
//	/**
//	 * @return the accountId
//	 */
//	public String getAccountId() {
//		return accountId;
//	}
//	/**
//	 * @param accountId the accountId to set
//	 */
//	public void setAccountId(String accountId) {
//		this.accountId = accountId;
//	}
//	/**
//	 * @return the accountPassword
//	 */
//	public String getAccountPassword() {
//		return accountPassword;
//	}
//	/**
//	 * @param accountPassword the accountPassword to set
//	 */
//	public void setAccountPassword(String accountPassword) {
//		this.accountPassword = accountPassword;
//	}
//	/**
//	 * @return the clientId
//	 */
//	public long getClientId() {
//		return clientId;
//	}
//	/**
//	 * @param clientId the clientId to set
//	 */
//	public void setClientId(long clientId) {
//		this.clientId = clientId;
//	}
//	/**
//	 * @return the userName
//	 */
//	public String getUserName() {
//		return userName;
//	}
//	/**
//	 * @param userName the userName to set
//	 */
//	public void setUserName(String userName) {
//		this.userName = userName;
//	}
//	/**
//	 * @return the password
//	 */
//	public String getPassword() {
//		return password;
//	}
//	/**
//	 * @param password the password to set
//	 */
//	public void setPassword(String password) {
//		this.password = password;
//	}
//	
//	/**
//     * @return the fromEpoch
//     */
//    public long getFromEpoch() {
//        return fromEpoch;
//    }
//    
//    /**
//     * @param fromEpoch the fromEpoch to set
//     */
//    public void setFromEpoch(long fromEpoch) {
//        this.fromEpoch = fromEpoch;
//    }
//    
//    /**
//	 * @return the from
//	 */
//	public Calendar getFrom() {
//        if (null == from && fromEpoch > 0) {
//            from = Calendar.getInstance();
//            from.setTime(new Date(fromEpoch));
//        }
//		return from;
//	}
//	
//	/**
//	 * @param from the from to set
//	 */
//	public void setFrom(Calendar from) {
//		this.from = from;
//	}
//	
//	/**
//     * @return the toEpoch
//     */
//    public long getToEpoch() {
//        return toEpoch;
//    }
//    
//    /**
//     * @param toEpoch the toEpoch to set
//     */
//    public void setToEpoch(long toEpoch) {
//        this.toEpoch = toEpoch;
//    }
//    
//    /**
//	 * @return the to
//	 */
//	public Calendar getTo() {
//	    if (null == to && toEpoch > 0) {
//	        to = Calendar.getInstance();
//	        to.setTime(new Date(toEpoch));
//	    }
//		return to;
//	}
//	
//	/**
//	 * @param to the to to set
//	 */
//	public void setTo(Calendar to) {
//		this.to = to;
//	}
//	/**
//	 * @return the isSettled
//	 */
//	public boolean isSettled() {
//		return isSettled;
//	}
//	/**
//	 * @param isSettled the isSettled to set
//	 */
//	public void setSettled(boolean isSettled) {
//		this.isSettled = isSettled;
//	}
//	/**
//	 * @return the marketId
//	 */
//	public long getMarketId() {
//		return marketId;
//	}
//	/**
//	 * @param marketId the marketId to set
//	 */
//	public void setMarketId(long marketId) {
//		this.marketId = marketId;
//	}
//	/**
//	 * @return the startRow
//	 */
//	public int getStartRow() {
//		return startRow;
//	}
//	/**
//	 * @param startRow the startRow to set
//	 */
//	public void setStartRow(int startRow) {
//		this.startRow = startRow;
//	}
//	/**
//	 * @return the pageSize
//	 */
//	public int getPageSize() {
//		return pageSize;
//	}
//	/**
//	 * @param pageSize the pageSize to set
//	 */
//	public void setPageSize(int pageSize) {
//		this.pageSize = pageSize;
//	}
//	/**
//	 * @return the apiExternalUserReference
//	 */
//	public String getApiExternalUserReference() {
//		return apiExternalUserReference;
//	}
//	/**
//	 * @param apiExternalUserReference the apiExternalUserReference to set
//	 */
//	public void setApiExternalUserReference(String apiExternalUserReference) {
//		this.apiExternalUserReference = apiExternalUserReference;
//	}
//	
//	public int getPeriod() {
//		return period;
//	}
//	public void setPeriod(int period) {
//		this.period = period;
//	}
//	
//    public String toString() {
//        String ls = System.getProperty("line.separator");
//        return ls + "InvestmentsMethodRequest: "
//            + super.toString()
//            + "accountId: " + accountId + ls
//            + "clientId: " + clientId + ls
//            + "from: " + from + ls
//            + "fromEpoch: " + fromEpoch + ls
//            + "to: " + to + ls
//            + "toEpoch: " + toEpoch + ls
//            + "isSettled: " + isSettled + ls
//            + "marketId: " + marketId + ls
//            + "startRow: " + startRow + ls
//            + "pageSize: " + pageSize + ls
//            + "apiExternalUserReference: " + apiExternalUserReference + ls
//            + "period: " + period + ls;
//    }
//	
//}
