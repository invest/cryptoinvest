package com.anyoption.json.requests;

import com.anyoption.common.service.requests.UserMethodRequest;

/**
 *
 * @author liors
 *
 */
public class ChangePasswordMethodRequest extends UserMethodRequest{

   private String password;
   private String passwordNew;
   private String passwordReType;
   private String captcha;

   public String getCaptcha() {
	return captcha;
}

public void setCaptcha(String captcha) {
	this.captcha = captcha;
}

public ChangePasswordMethodRequest() {

   }

	/**
	 * @return the reNewPass
	 */
	public String getPasswordNew() {
		return passwordNew;
	}

	/**
	 * @param reNewPass the reNewPass to set
	 */
	public void setPasswordNew(String reNewPass) {
		this.passwordNew = reNewPass;
	}

	/**
	 * @return passwordReType
	 */
	public String getPasswordReType() {
		return passwordReType;
	}

	/**
	 * @param passwordReType
	 */
	public void setPasswordReType(String passwordReType) {
		this.passwordReType = passwordReType;
	}
	
	   public String getPassword() {
			return password;
		}

		public void setPassword(String password) {
			this.password = password;
		}

}