package com.anyoption.json.requests;

import com.anyoption.common.service.requests.UserMethodRequest;

/**
 * 
 * @author liors
 *
 */
public class OptionPlusMethodRequest extends UserMethodRequest {
    
	protected long investmentId;
    protected Double price;
   
	/**
	 * @return investment Id
	 */
	public long getInvestmentId() {
		return investmentId;
	}

	/**
	 * @param investmentId
	 */
	public void setInvestmentId(long investmentId) {
		this.investmentId = investmentId;
	}

	/**
	 * @return price
	 */
	public Double getPrice() {
		return price;
	}

	/**
	 * @param price
	 */
	public void setPrice(Double price) {
		this.price = price;
	}
	
    public String toString() {
        String ls = System.getProperty("line.separator");
        return ls + "OptionPlusMethodRequest: "
            + super.toString()
            + "investmentId: " + investmentId + ls
            + "price: " + price + ls;
    }
    
}