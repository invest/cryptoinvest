/**
 * 
 */
package com.anyoption.json.requests;

import java.util.ArrayList;

import com.anyoption.common.beans.base.MailBoxUser;
import com.anyoption.common.service.results.MethodResult;

/**
 * @author AviadH
 *
 */
public class InboxMethodResult extends MethodResult{
	
	private ArrayList<MailBoxUser> emails;

	/**
	 * @return the emails
	 */
	public ArrayList<MailBoxUser> getEmails() {
		return emails;
	}

	/**
	 * @param emails the emails to set
	 */
	public void setEmails(ArrayList<MailBoxUser> emails) {
		this.emails = emails;
	}
	
}
