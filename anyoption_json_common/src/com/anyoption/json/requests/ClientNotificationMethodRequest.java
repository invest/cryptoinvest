package com.anyoption.json.requests;

import java.util.ArrayList;

import com.anyoption.common.beans.base.ApiNotification;
import com.anyoption.common.service.requests.MethodRequest;

/**
 * @author EranL
 */
public class ClientNotificationMethodRequest extends MethodRequest {
	private ArrayList<ApiNotification> notifications;

	/**
	 * @return the notifications
	 */
	public ArrayList<ApiNotification> getNotifications() {
		return notifications;
	}

	/**
	 * @param notifications the notifications to set
	 */
	public void setNotifications(ArrayList<ApiNotification> notifications) {
		this.notifications = notifications;
	}

}