/**
 * 
 */
package com.anyoption.json.requests;

import com.anyoption.common.beans.base.MailBoxUser;
import com.anyoption.common.service.requests.UserMethodRequest;

/**
 * @author AviadH
 *
 */
public class MailBoxMethodRequest extends UserMethodRequest{
	
	private MailBoxUser mailBoxUser;

	/**
	 * @return the mailBoxUser
	 */
	public MailBoxUser getMailBoxUser() {
		return mailBoxUser;
	}

	/**
	 * @param mailBoxUser the mailBoxUser to set
	 */
	public void setMailBoxUser(MailBoxUser mailBoxUser) {
		this.mailBoxUser = mailBoxUser;
	}
	
    public String toString() {
        String ls = System.getProperty("line.separator");
        return ls + "MailBoxMethodRequest: "
            + super.toString()
            + "mailBoxUser: " + mailBoxUser + ls;
    }

}
