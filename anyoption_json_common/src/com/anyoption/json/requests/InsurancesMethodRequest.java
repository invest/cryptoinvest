package com.anyoption.json.requests;

import com.anyoption.common.service.requests.UserMethodRequest;
import com.anyoption.common.util.GoldenMinute.GoldenMinuteType;

/**
 * 
 * @author liors
 *
 */
public class InsurancesMethodRequest extends UserMethodRequest {
    protected int insuranceType;
    protected GoldenMinuteType gmType;
    protected Long[] investmentIds;
    protected Double[] insuranceAmounts;

    /**
     * @return list of investment Ids
     */
    public Long[] getInvestmentIds() {
        return investmentIds;
    }

    /**
     * @param investmentIds
     */
    public void setInvestmentIds(Long[] investmentIds) {
        this.investmentIds = investmentIds;
    }

    /** 
     * @return insuranceType
     */
    public int getInsuranceType() {
        return insuranceType;
    }

    /**
     * @param insuranceType
     */
    public void setInsuranceType(int insuranceType) {
        this.insuranceType = insuranceType;
    }

    /**
     * @return list of insuranceAmounts
     */
    public Double[] getInsuranceAmounts() {
        return insuranceAmounts;
    }

    /**
     * @param insuranceAmounts
     */
    public void setInsuranceAmounts(Double[] insuranceAmounts) {
        this.insuranceAmounts = insuranceAmounts;
    }
 
    public GoldenMinuteType getGmType() {
		return gmType;
	}

	public void setGmType(GoldenMinuteType gmType) {
		this.gmType = gmType;
	}

	/* (non-Javadoc)
     * @see com.anyoption.json.requests.UserMethodRequest#toString()
     */
    public String toString() {
        String ls = System.getProperty("line.separator");
        return ls + "InsurancesMethodRequest: "
            + super.toString()
            + "insuranceType: " + insuranceType + ls
            + "gmType: " + gmType.name() + ls
            + "investmentIds: " + investmentIds + ls
            + "insuranceAmounts: " + insuranceAmounts + ls;
    }
}