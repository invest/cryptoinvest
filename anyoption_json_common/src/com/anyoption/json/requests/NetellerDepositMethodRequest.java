package com.anyoption.json.requests;

import com.anyoption.common.service.requests.UserMethodRequest;

public class NetellerDepositMethodRequest extends UserMethodRequest {
	String amount;
	String email;
	String verificationCode;
	
	public String getAmount() {
		return amount;
	}
	public void setAmount(String amount) {
		this.amount = amount;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getVerificationCode() {
		return verificationCode;
	}
	public void setVerificationCode(String verificationCode) {
		this.verificationCode = verificationCode;
	}
	
}
