package com.anyoption.json.requests;

import com.anyoption.common.service.requests.MethodRequest;


/**
 * @author liors
 *
 */
public class PixelMethodRequest extends MethodRequest {
	private String dynamicParam;
	private long combinationId;
	private long pixelType; //TODO why not enum? PixelGenerator...
	private long userId;
	private long transactionId;

	/**
	 * @return the combinationId
	 */
	public long getCombinationId() {
		return combinationId;
	}
	/**
	 * @param combinationId the combinationId to set
	 */
	public void setCombinationId(long combinationId) {
		this.combinationId = combinationId;
	}

	@Override
	public String toString() {
		return "PixelMethodRequest [dynamicParam=" + dynamicParam
				+ ", combinationId=" + combinationId + ", pixelType="
				+ pixelType + "]";
	}
	/**
	 * @return the pixelType
	 */
	public long getPixelType() {
		return pixelType;
	}
	/**
	 * @param pixelType the pixelType to set
	 */
	public void setPixelType(long pixelType) {
		this.pixelType = pixelType;
	}
	/**
	 * @return the dynamicParam
	 */
	public String getDynamicParam() {
		return dynamicParam;
	}
	/**
	 * @param dynamicParam the dynamicParam to set
	 */
	public void setDynamicParam(String dynamicParam) {
		this.dynamicParam = dynamicParam;
	}
	/**
	 * @return the userId
	 */
	public long getUserId() {
		return userId;
	}
	/**
	 * @param userId the userId to set
	 */
	public void setUserId(long userId) {
		this.userId = userId;
	}
	/**
	 * @return the transactionId
	 */
	public long getTransactionId() {
		return transactionId;
	}
	/**
	 * @param transactionId the transactionId to set
	 */
	public void setTransactionId(long transactionId) {
		this.transactionId = transactionId;
	}
}
