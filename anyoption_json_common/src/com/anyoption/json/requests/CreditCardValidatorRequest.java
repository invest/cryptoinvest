package com.anyoption.json.requests;

public class CreditCardValidatorRequest {
	String ccNumber;
	long skinId;

	public String getCcNumber() {
		return ccNumber;
	}

	public void setCcNumber(String ccNumber) {
		this.ccNumber = ccNumber;
	}

	public long getSkinId() {
		return skinId;
	}

	public void setSkinId(long skinId) {
		this.skinId = skinId;
	}
	
	
}
