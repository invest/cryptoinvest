package com.anyoption.json.requests;

import com.anyoption.common.service.requests.MethodRequest;


public class EmailMethodRequest extends MethodRequest {

	String mailBody;
	String from;
	
	public String getMailBody() {
		return mailBody;
	}

	public void setMailBody(String mailBody) {
		this.mailBody = mailBody;
	}

	public String getFrom() {
		return from;
	}

	public void setFrom(String from) {
		this.from = from;
	}

}
