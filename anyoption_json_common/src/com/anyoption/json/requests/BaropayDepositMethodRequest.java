package com.anyoption.json.requests;

import com.anyoption.common.service.requests.UserMethodRequest;

/**
 * @author EranL
 */
public class BaropayDepositMethodRequest extends UserMethodRequest {
	private String amount;
	private String landPhone;
	private String senderName;
	
	/**
	 * @return the amount
	 */
	public String getAmount() {
		return amount;
	}
	/**
	 * @param amount the amount to set
	 */
	public void setAmount(String amount) {
		this.amount = amount;
	}
	public String getLandPhone() {
		return landPhone;
	}
	public void setLandPhone(String landPhone) {
		this.landPhone = landPhone;
	}
	public String getSenderName() {
		return senderName;
	}
	public void setSenderName(String senderName) {
		this.senderName = senderName;
	}
	
    public String toString() {
        String ls = System.getProperty("line.separator");
        return ls + "BaropayDepositMethodRequest: "
            + super.toString()
            + "amount: " + amount + ls
            + "landPhone: " + landPhone + ls
            + "senderName: " + senderName + ls;
    }

}