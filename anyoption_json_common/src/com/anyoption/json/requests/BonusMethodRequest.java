package com.anyoption.json.requests;

/**
 * contain bonusId and bonusStateId.
 *  extends from FormsMethodRequest.
 * @author liors
 *
 */
public class BonusMethodRequest extends FormsMethodRequest {
	
    protected long bonusId;
    protected long bonusStateId;
    // 1 true, 0 false aka ALL bonues records
    protected int showOnlyNotSeenBonuses;
    
    
    public int getShowOnlyNotSeenBonuses() {
        return showOnlyNotSeenBonuses;
    }
    public void setShowOnlyNotSeenBonuses(int showOnlyNotSeenBonuses) {
        this.showOnlyNotSeenBonuses = showOnlyNotSeenBonuses;
    }
    /**
	 * @return the bonusId
	 */
	public long getBonusId() {
		return bonusId;
	}
	/**
	 * @param bonusId the bonusId to set
	 */
	public void setBonusId(long bonusId) {
		this.bonusId = bonusId;
	}
	/*
	 * @return the bonusStateId
	 */
	public long getBonusStateId() {
		return bonusStateId;
	}
	/**
	 * @param bonusStateId the bonusStateId to set
	 */
	public void setBonusStateId(long bonusStateId) {
		this.bonusStateId = bonusStateId;
	}
	
    @Override
    public String toString() {
        String ls = System.getProperty("line.separator");
        return ls + "BonusMethodRequest: "
            + super.toString()
            + "bonusId: " + bonusId + ls
            + "bonusStateId: " + bonusStateId + ls
            + "showOnlyNotSeenBonuses: " + showOnlyNotSeenBonuses + ls;
    }
}