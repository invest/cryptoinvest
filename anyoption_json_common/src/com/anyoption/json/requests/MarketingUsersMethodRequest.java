package com.anyoption.json.requests;

import com.anyoption.common.service.requests.UserMethodRequest;

/**
 * MarketingUsersMethodRequest
 * contains affiliateKey and dateRequest
 * @author Eyal O
 *
 */
public class MarketingUsersMethodRequest extends UserMethodRequest {
    protected long affiliateKey;
    protected String dateRequest;

	/**
	 * @return the affiliateKey
	 */
	public long getAffiliateKey() {
		return affiliateKey;
	}

	/**
	 * @param affiliateKey the affiliateKey to set
	 */
	public void setAffiliateKey(long affiliateKey) {
		this.affiliateKey = affiliateKey;
	}
    
	/**
	 * @return the dateRequest
	 */
	public String getDateRequest() {
		return dateRequest;
	}

	/**
	 * @param dateRequest the dateRequest to set
	 */
	public void setDateRequest(String dateRequest) {
		this.dateRequest = dateRequest;
	}

	@Override
	public String toString() {
        String ls = System.getProperty("line.separator");
        return ls + " MarketingUsersMethodRequest: "
            + super.toString()
            + " affiliateKey: " + affiliateKey + ls
        	+ " dateRequest: " + dateRequest + ls;
    }
}