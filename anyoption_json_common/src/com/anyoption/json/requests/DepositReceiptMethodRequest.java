/**
 * 
 */
package com.anyoption.json.requests;

import com.anyoption.common.service.requests.UserMethodRequest;

/**
 * @author AviadH
 *
 */
public class DepositReceiptMethodRequest extends UserMethodRequest {
	
	private long transactionId;

	/**
	 * @return the transactionId
	 */
	public long getTransactionId() {
		return transactionId;
	}

	/**
	 * @param transactionId the transactionId to set
	 */
	public void setTransactionId(long transactionId) {
		this.transactionId = transactionId;
	}
	
    public String toString() {
        String ls = System.getProperty("line.separator");
        return ls + "DepositReceiptMethodRequest: "
            + super.toString()
            + "transactionId: " + transactionId + ls;
    }
	
}
