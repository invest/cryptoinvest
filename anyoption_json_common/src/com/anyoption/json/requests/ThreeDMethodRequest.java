package com.anyoption.json.requests;

import com.anyoption.common.service.requests.UserMethodRequest;

public class ThreeDMethodRequest extends UserMethodRequest {
	String MD;
	String paRes;
	String UTC;
	
	long transactionId;

	public String getPaRes() {
		return paRes;
	}

	public void setPaRes(String paRes) {
		this.paRes = paRes;
	}

	public long getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(long transactionId) {
		this.transactionId = transactionId;
	}

	public String getMD() {
		return MD;
	}

	public void setMD(String mD) {
		MD = mD;
	}

	public String getUTC() {
		return UTC;
	}

	public void setUTC(String uTC) {
		UTC = uTC;
	}

	@Override
	public String toString() {
		return "ThreeDMethodRequest [MD=" + MD + ", paRes=" + paRes + ", UTC=" + UTC + ", transactionId="
				+ transactionId + "]";
	}

}