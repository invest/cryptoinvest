package il.co.etrader.test_cases;


import org.junit.Test;
import il.co.etrader.daily.balance.report.DailyBalanceReportJob;
public class testDailyBalanceReportJob {

	private final static String fileAO = "C:\\workspace\\etrader_common\\res_job\\test\\daily_balance_report_AO.properties";
	private final static String fileET="C:\\workspace\\etrader_common\\res_job\\test\\daily_balance_report.properties";
	@Test
	public void test() throws Exception {
	
		String [] args = new String[2];
		// property file
		args[0] = fileAO;
		// days for reporting default 1
		args[1] = "1";
		
		DailyBalanceReportJob.main(args);
	}

}
