package epg.jobs;

/**
 * @author LioR SoLoMoN
 *
 */
public class SummaryEPGCaptureWithdrawal {
	private long userId;
	private long transactionId;
	private String epgTransactionId;
	
	/**
	 * 
	 */
	public SummaryEPGCaptureWithdrawal() { 
		
	}
	
	/**
	 * @param userId
	 * @param transactionId
	 * @param epgTransactionId
	 */
	public SummaryEPGCaptureWithdrawal(long userId, long transactionId, String epgTransactionId) {
		this.userId = userId;
		this.transactionId = transactionId;
		this.epgTransactionId = epgTransactionId;
	}
	
	/**
	 * @return the userId
	 */
	public long getUserId() {
		return userId;
	}
	/**
	 * @param userId the userId to set
	 */
	public void setUserId(long userId) {
		this.userId = userId;
	}
	/**
	 * @return the transactionId
	 */
	public long getTransactionId() {
		return transactionId;
	}
	/**
	 * @param transactionId the transactionId to set
	 */
	public void setTransactionId(long transactionId) {
		this.transactionId = transactionId;
	}
	/**
	 * @return the epgTransactionId
	 */
	public String getEpgTransactionId() {
		return epgTransactionId;
	}
	/**
	 * @param epgTransactionId the epgTransactionId to set
	 */
	public void setEpgTransactionId(String epgTransactionId) {
		this.epgTransactionId = epgTransactionId;
	}
}
