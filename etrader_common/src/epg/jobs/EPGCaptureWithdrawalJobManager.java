package epg.jobs;

import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.TreeSet;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import org.apache.log4j.Logger;
import org.bouncycastle.crypto.CryptoException;

import com.anyoption.common.clearing.ClearingException;
import com.anyoption.common.clearing.ClearingInfo;
import com.anyoption.common.clearing.EPGClearingProvider;
import com.anyoption.common.daos.GeneralDAO;
import com.anyoption.common.daos.TransactionRequestResponseDAO;
import com.anyoption.common.daos.TransactionsDAOBase;
import com.anyoption.common.daos.UsersDAOBase;
import com.anyoption.common.payments.CaptureWithdrawal;
import com.anyoption.common.util.AESUtil;
import com.anyoption.common.util.CommonUtil;

import il.co.etrader.bl_managers.ClearingManager;
import il.co.etrader.bl_managers.TransactionsManagerBase;
import il.co.etrader.bl_vos.Writer;
import il.co.etrader.util.ConstantsBase;

/**
 * @author LioR SoLoMoN
 *
 */
public class EPGCaptureWithdrawalJobManager {
	private static final Logger logger = Logger.getLogger(EPGCaptureWithdrawalJobManager.class);
	private static final String MSG_PREFIX = "EPG Capture Withdrawal Job; ";
	
	public static TreeSet<SummaryEPGCaptureWithdrawal> doJob(Connection connection, String transactionIds) {
		logger.info(MSG_PREFIX + " START JOB with (may have) given transactionIds: " + transactionIds);
		TreeSet<SummaryEPGCaptureWithdrawal> summaryRecords = new TreeSet<SummaryEPGCaptureWithdrawal>(new Comparator<SummaryEPGCaptureWithdrawal>() {
			@Override
			public int compare(SummaryEPGCaptureWithdrawal s1,
					SummaryEPGCaptureWithdrawal s2) {
				return s1.getTransactionId() > s2.getTransactionId() ? 1 : -1;
			}
		});
		ArrayList<ClearingInfo> transactionsInfoList;
		try {
			transactionsInfoList = getEPGTransactionsToCaptureWithdrawal(connection, transactionIds);
			logger.info(MSG_PREFIX + " got a list of transactions. size: " + transactionsInfoList.size());
			if (transactionsInfoList != null && !transactionsInfoList.isEmpty()) {
				logger.info(MSG_PREFIX + " go one-by-one");
				for (ClearingInfo info : transactionsInfoList) {
					try {
						Scenario scenario = requestEPGTransactionsCaptureWithdrawal(info, connection);
						completeFlow(connection, scenario, info);						
						summaryRecords.add(
								new SummaryEPGCaptureWithdrawal(
										info.getUserId(), info.getTransactionId(), info.getProviderTransactionId()));
					} catch (ClearingException ce) {
						logger.error(MSG_PREFIX + " Error! trasnaction to capture; transactionId: " + info.getTransactionId() + " move to the next transaction");
					}
				}
			}
		} catch (Exception e) {
			logger.error(MSG_PREFIX + " Error! EPG trasnaction process");
		} 
		logger.info(MSG_PREFIX + " JOB END with (may have) given transactionIds: " + transactionIds);
		return summaryRecords;
	}

	private static void completeFlow(Connection connection, Scenario scenario, ClearingInfo info) {
		logger.info(MSG_PREFIX + " START to Complete Flow on transactionId: " + info.getTransactionId());
		switch (scenario) {
			case SUCCESS_WITHDRAW:
				logger.info(MSG_PREFIX + " SUCCESS_WITHDRAW ");
				SuccessWithdraw(connection, info);
				break;
			case FAIL_WITHDRAW:
				logger.info(MSG_PREFIX + " FAIL_WITHDRAW");
				FailWithdraw(connection, info);
				break;
			case PARTIALLY_SUCCESS:
				logger.info(MSG_PREFIX + " PARTIALLY_SUCCESS");
				PartiallySuccess();
				break;
			default: 
                logger.info(MSG_PREFIX + " default? ");
                break;
		}
		logger.info(MSG_PREFIX + " END to Complete Flow on transactionId: " + info.getTransactionId());
	}

	private static void PartiallySuccess() {
		// TODO Auto-generated method stub
		
	}

	/**
	 * Fail Withdraw
	 * @param connection
	 * @param info
	 */
	private static void FailWithdraw(Connection connection, ClearingInfo info) {
		logger.info("FailWithdraw - START.");
		try {
			connection.setAutoCommit(false);
			updateTransaction(connection, info, TransactionsManagerBase.TRANS_STATUS_FAILED);
			UsersDAOBase.addToBalance(connection, info.getUserId(), info.getAmount());
			GeneralDAO.insertBalanceLog(connection, Writer.WRITER_ID_AUTO, info.getUserId(), ConstantsBase.TABLE_TRANSACTIONS, 
				info.getTransactionId(), ConstantsBase.LOG_BALANCE_EPG_FAILED_WITHDRAW, info.getUtcOffset());
			connection.commit();
		} catch (Exception e) {
			logger.error("Exception in FailWithdraw! ", e);
			try {
				connection.rollback();
			} catch (Throwable it) {
				logger.error("Can't rollback.", it);
			}
		} finally {
			try {
				connection.setAutoCommit(true);
			} catch (Exception e) {
				logger.error("Can't set back to autocommit.", e);
			}
		}
		logger.info("FailWithdraw - END.");
	}

	/**
	 * Success Withdraw
	 * @param connection
	 * @param info
	 */
	private static void SuccessWithdraw(Connection connection, ClearingInfo info) {
		logger.info("SuccessWithdraw - START.");
		updateTransaction(connection, info, TransactionsManagerBase.TRANS_STATUS_SUCCEED);
		logger.info("SuccessWithdraw - END.");
	}

	private static Scenario requestEPGTransactionsCaptureWithdrawal(ClearingInfo info, Connection connection) throws ClearingException {
		logger.info(MSG_PREFIX + " START to send request to EPG for transactionId: " + info.getTransactionId());
		Scenario scenario = Scenario.FAIL_WITHDRAW;
		ClearingManager.withdraw(info);
		try {
			TransactionRequestResponseDAO.insert(connection, info.getRequestResponse());
		} catch (SQLException e) {
			logger.error("ERROR insert request response", e);
		}
		if (info.isSuccessful()) {
			scenario = Scenario.SUCCESS_WITHDRAW;
		}
		
		//TODO how to know partial???
		
		logger.info(MSG_PREFIX + " END to send request to EPG for transactionId:"
				+ " " + info.getTransactionId() + "; got: " + scenario.getDeclaringClass().getName());
		return scenario;		
	}

	private static ArrayList<ClearingInfo> getEPGTransactionsToCaptureWithdrawal(Connection connection, String transactionIds) throws SQLException {
		logger.info(MSG_PREFIX + " START to get EPG Transactions To Capture Withdrawal ");
		PreparedStatement ps = null;
		ResultSet rs = null;
		ArrayList<ClearingInfo> transactions = new ArrayList<ClearingInfo>();
		
		try {
			String sql = generateSQL(transactionIds);
			ps = connection.prepareStatement(sql);
			ps.setLong(1, TransactionsManagerBase.TRANS_STATUS_SECOND_APPROVAL);
			ps.setLong(2, ConstantsBase.ACCOUNTING_APPROVED_YES);
			ps.setLong(3, TransactionsManagerBase.TRANS_TYPE_CC_WITHDRAW);
			if (!CommonUtil.isParameterEmptyOrNull(transactionIds)) {
				ps.setLong(4, TransactionsManagerBase.TRANS_TYPE_INTERNAL_CREDIT);
			}
			rs = ps.executeQuery();
			while (rs.next()) {
				try {
					ClearingInfo info = new ClearingInfo();  	
					info.setUserId(rs.getLong("userid"));
					info.setTransactionId(rs.getLong("tid"));
					info.setAmount(rs.getLong("amount"));
					info.setCountryA2(rs.getString("a2"));
					info.setCurrencySymbol(rs.getString("code"));
					info.setCcn(AESUtil.decrypt(rs.getString("ccnumber")));					
					info.setFirstName(rs.getString("first_name"));
					info.setLastName(rs.getString("last_name"));
					info.setCvv(rs.getString("cvv"));
					info.setExpireMonth(rs.getString("expmonth"));
					info.setExpireYear(rs.getString("expyear"));
					info.setAddress(rs.getString("street"));
					info.setCity(rs.getString("city_name"));
					info.setZip(rs.getString("zip_code"));
					info.setCountryA2(rs.getString("a2"));
					info.setSkinId(rs.getLong("skin_id"));
					info.setIp(rs.getString("ip"));
					info.setOwner(rs.getString("holder_name"));
					info.setProviderId(rs.getLong("clearing_provider_id"));
					info.setUtcOffset(rs.getString("utc_offset"));
					
					info.setAdditionalRequestParams(EPGClearingProvider.getAdditionalDetails(
							TransactionsDAOBase.getEpgWithdrawalRequestAdditionalDetails(connection, info)));
					transactions.add(info);
				} catch (	CryptoException | InvalidKeyException | IllegalBlockSizeException | BadPaddingException | NoSuchAlgorithmException
							| NoSuchPaddingException | InvalidAlgorithmParameterException ce) {
					logger.error("ERROR! problem to get transaction from db. move to the next record ", ce);
				}
			}		
		} finally {
            try {
                rs.close();
            } catch (Exception e) {
            	logger.error(MSG_PREFIX + " Can't close",e);
            }
            try {
                ps.close();
            } catch (Exception e) {
            	logger.error(MSG_PREFIX + " Can't close",e);
            }           
        }
		logger.info(MSG_PREFIX + " END to get EPG Transactions To Capture Withdrawal ");
		return transactions;
	}
	
	private static String generateSQL(String transactionIds) {
		//TODO it's duplicated from cap with job, change it when the spec will be final
		String sql =
					"SELECT " +
		            "	c.holder_id_num idnum, " +
		            "	t.user_id userid, " +
		            "	t.id tid, " +
		            "	c.cc_number ccnumber, " +
		            "	c.cc_pass cvv, " +
		            "	c.exp_month expmonth, " +
		            "	curr.code, "+
		            "	c.exp_year expyear, " +
		            "	c.type_id type_id, " +
		            "	c.holder_name, " +
		            "	c.id ccId, " +
		            "	t.amount amount, " +
		            "	t.auth_number auth_number, " +
		            "	w.user_name writer, " +
		            "	u.user_name user_name, " +
		            "	t.fee_cancel, " +
		            "	s.default_xor_id, " +
		            "	u.skin_id, " +
		            "	u.utc_offset, " +
		            "	u.street, " +
		            "	u.zip_code, " +
		            "	u.currency_id, " +
		            "	u.first_name, " +
		            "	u.last_name, " +
		            "	u.city_name, " +
		            "	l.cc_fee, " +
		            "	cntr.id AS country_id, " +
		            "	cntr.a2, " +
		            "	t.ip, " +
		            "	t.is_credit_withdrawal, " +
		            "	t.deposit_reference_id, " +
		            "	t.clearing_provider_id, " +
		            "	t.type_id transaction_type_id, " +
		            "	t.internals_amount, " +
		            "	c.type_id_by_bin," +
		            "	tu.time_created as transferred_user_time, " +
		            "	tu.is_first_dep_made as transferred_user_depositor, " +
		            "	l.amt_for_low_withdraw, " +
		            "	t.time_created, " +
		            "	t.utc_offset_created " +
		        	"FROM " +
		            "	transactions t, " +
		            "	credit_cards c, " +
		            "	writers w, " +
		            "	users u left join transferred_users tu on u.id = tu.user_id, " +
		            "	skins s, " +
		            "	currencies curr, " +
		            "	limits l, " +
		            "	countries cntr, " +
		            "	clearing_providers cp " +
		        	"WHERE " +
		        	"	t.clearing_provider_id = cp.id AND " +
		            "	t.user_id = u.id AND " +
		            "	t.credit_card_id = c.id AND " +
		            "	t.writer_id = w.id AND " +
		            "	s.id = u.skin_id AND " +
		            "	curr.id = u.currency_id AND " +
		            "	u.limit_id = l.id AND " +
		            "	u.country_id = cntr.id AND " +
		            "	u.class_id <> 0 AND " +
		            "	t.status_id = ? AND " +
		            "	t.is_accounting_approved = ? AND " +
					"	cp.payment_group_id = " + TransactionsManagerBase.PAYMENT_GROUP_EPG + " AND " ;
        	//Add an option to run job with specific transcation id
            if (CommonUtil.isParameterEmptyOrNull(transactionIds)) {
               	sql +=
                    "	t.type_id = ? AND " +
               		"	t.time_created < sysdate ";
            } else {
        		sql +=
        			"	t.id in ( " + transactionIds + " ) AND " +
        			"	(t.type_id = ? OR  t.type_id = ? ) ";
            }
            sql += 	"ORDER BY " +
            		"	curr.id, userid ,tid";
		return sql;
	}

	public enum Scenario {
		SUCCESS_WITHDRAW, FAIL_WITHDRAW, PARTIALLY_SUCCESS
	}
	
	/**
	 * Update Transaction
	 * @param connection
	 * @param info
	 * @param tranStatusId
	 */
	private static void updateTransaction(Connection connection, ClearingInfo info, int tranStatusId) {
		logger.info("updateTransaction - START.");
		CaptureWithdrawal captureWithdrawal = new CaptureWithdrawal();
		captureWithdrawal.setStatusId(tranStatusId);
		captureWithdrawal.setComment(info.getResult() + " | " + info.getMessage() + " | " + info.getUserMessage() + " | " + info.getProviderTransactionId());
		captureWithdrawal.setDescription(ConstantsBase.EMPTY_STRING);
		captureWithdrawal.setTransactionId(info.getTransactionId());
		captureWithdrawal.setProviderTransactionId(info.getProviderTransactionId());
		captureWithdrawal.setClearingProviderId(info.getProviderId());
		captureWithdrawal.setUtcOffsetSettled(info.getUtcOffset());
		try {
			/* Update transaction */
			TransactionsManagerBase.updateTransactionAfterCaptureWithdrawal(connection, captureWithdrawal);
		} catch (SQLException e) {
			logger.error("Error in updateTransaction. ", e);
		}
		logger.info("updateTransaction - END.");
	}
}
