package il.co.etrader.util;
import java.util.Comparator;

import javax.faces.model.SelectItem;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.Market;

/**
     * SelectItem comperator class
     * @author idan
     */
    public class SelectItemComparatorHebrewPriority implements Comparator<SelectItem> {

        private static final Logger log = Logger.getLogger(SelectItemComparatorHebrewPriority.class);


        public int compare(SelectItem a, SelectItem b) {
            return compare(a.getLabel(), b.getLabel());
        }


        public int compare(Market a, Market b) {
            return compare(a.getName(), b.getName());
        }


        public int compare(String item1, String item2) {
            //String kol1 = ConstantsBase.SELECTITEM_COMP_KOL;
            //String kol = kol1.toString();
            if (item1.indexOf("(") > -1) {
                item1 = item1.substring(0, item1.indexOf("("));
            }

            if (item2.indexOf("(") > -1) {
                item2 = item2.substring(0, item2.indexOf("("));
            }
            String item1Trimmed = item1;
            if (item1.indexOf(" ") > -1) {
                item1Trimmed = item1.substring(0,item1.indexOf(" "));
            }
            String item2Trimmed = item2;
            if (item2.indexOf(" ") > -1) {
                item2Trimmed = item2.substring(0,item2.indexOf(" "));
            }

            if (!isEnglishLettersAndNumbersOnly(item1) && isEnglishLettersAndNumbersOnly(item2)) {
                return -1;
            } else if (!isEnglishLettersAndNumbersOnly(item2) && isEnglishLettersAndNumbersOnly(item1)){
                return 1;
            } else if (item1Trimmed.equals("\u05db\u05dc")) { //"כל"
                return -1;
            } else if (item2Trimmed.equals("\u05db\u05dc")) { //"כל"
                return 1;
            } else {
                return item1.compareToIgnoreCase(item2);
            }
        }

        public static boolean isEnglishLettersAndNumbersOnly(Object value) {
            String pattern = CommonUtil.getMessage("general.validate.english.numbers.spaces", null);
            String v = (String) value;
            v = v.replaceAll("-", " ");
            v = v.replaceAll("`", " ");
            v = v.replaceAll("'", " ");
            v = v.replaceAll("\\(", " ");
            v = v.replaceAll("\\)", " ");
            v = v.replaceAll("\"", " ");
            v = v.replaceAll("‘", " ");
            v = v.replaceAll("&", " ");
            v = v.replaceAll("/", " ");

            if (!v.matches(pattern) || v.trim().equals("")) {
               return false;
            }
            else return true;
        }


        public static boolean isEnglishLettersOnly(Object value) throws Exception {
            String pattern = "^['a-zA-Z ]+$";
            String v = (String) value;
            if (!v.matches(pattern) || v.trim().equals("")) {
                return false;
            }
            else return true;
        }


        /*public int compare(Object o1, Object o2) {
            // TODO Auto-generated method stub
            if ( o1 instanceof SelectItem){
                return compare(((SelectItem) o1).getLabel(),((SelectItem) o2).getLabel());
            }
            if ( o1 instanceof Market){
                return compare(((Market) o1).getDisplayNameKeyTxt(),((Market) o2).getDisplayNameKeyTxt());
            }
            return 0;
        }*/

    }
