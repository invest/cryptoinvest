package il.co.etrader.util;

import java.io.File;
import java.io.OutputStream;
import java.io.StringReader;
import java.io.StringWriter;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

public class JaxbHelper {
	
	    private static Marshaller marshaller = null;
	    private static Unmarshaller unmarshaller = null;

	    static {
	        try {
	            JAXBContext context = JAXBContext.newInstance("il.co.etrader.clearing.walpay");
	            marshaller = context.createMarshaller();
	            // include <?xml version="1.0" encoding="UTF-8"?>
	            marshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
	            // pretty print
	            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
//	            marshaller.setProperty(Marshaller.JAXB_ENCODING, "UTF-8");

	            unmarshaller = context.createUnmarshaller();
	        } catch (JAXBException e) {
	            throw new RuntimeException("There was a problem creating a JAXBContext object for formatting the object to XML.");
	        }
	    }

	    public static Object unmarshal(File source) {
	    	try {
	    		 return (Object) unmarshaller.unmarshal(source);
	    	} catch (JAXBException jaxbe) {
	    		jaxbe.printStackTrace();
	    	}
			return null;
	    }
	    
	    public static Object unmarshal(StringBuffer source) {
	    	try {
	    		StringReader reader = new StringReader(source.toString());
	    		return (Object) unmarshaller.unmarshal(reader);
	    	} catch (JAXBException jaxbe) {
	    		jaxbe.printStackTrace();
	    	}
			return null;
	    }
	    
	    
	    public void marshal(Object obj, OutputStream os) {
	        try {
	            marshaller.marshal(obj, os);
	        } catch (JAXBException jaxbe) {
	          jaxbe.printStackTrace();
	        }
	    }

	    public static String marshalToString(Object obj) {
	        try {
	            StringWriter sw = new StringWriter();
	            marshaller.marshal(obj, sw);
	            return sw.toString();
	        } catch (JAXBException jaxbe) {
	            jaxbe.printStackTrace();
	        }
			return null;
	    }
	    
}

