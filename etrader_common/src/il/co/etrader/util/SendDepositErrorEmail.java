package il.co.etrader.util;

import java.util.HashMap;
import java.util.Hashtable;

import org.apache.log4j.Logger;
import org.apache.velocity.Template;

/**
 * SendDepositErrorEmail class
 * 		for deposit failure issues
 *
 * @author Kobi
 */
public class SendDepositErrorEmail extends Thread  {

	// The email parameters that we need
	public static final String PARAM_EMAIL = "Email";
	public static final String PARAM_USER_NAME = "userName";
	public static final String PARAM_USER_ID = "userId";
	public static final String PARAM_TRANSACTION_ID = "transactionId";
	public static final String PARAM_CC_NUM_LAST4 = "ccNumberLast4";
	public static final String PARAM_CC_TYPE = "ccType";
	public static final String PARAM_AMOUNT = "amount";
	public static final String PARAM_DATE = "date";
	public static final String PARAM_DECLINE_DESCRIPTION = "decline_description";
	public static final String PARAM_DECLINE_COMMENTS = "decline_comments";
	public static final String PARAM_VELOCITY_DESCRIPTION = "velocity description";
	public static final String PARAM_SKIN = "skin";
	public static final String PARAM_FILE_ID = "fileId";
	public static final String PARAM_WRITER_ID = "writerId";

	//Add additional constants to server parameters
	public static final String MAIL_FROM ="from";
	public static final String MAIL_TO = "to";
	public static final String MAIL_SUBJECT = "subject";

	//private final  int END_OF_LINE = Character.LINE_SEPARATOR;
	private final String  END_OF_LINE = "<br/>";
	private final char DELIMITER = ':' ;


	private final String [] MAIL_FIELDS_LIST = {PARAM_USER_NAME ,PARAM_USER_ID, PARAM_TRANSACTION_ID ,PARAM_CC_NUM_LAST4, PARAM_CC_TYPE
												,PARAM_AMOUNT,PARAM_DECLINE_DESCRIPTION , PARAM_DECLINE_COMMENTS, PARAM_DATE,PARAM_SKIN,PARAM_VELOCITY_DESCRIPTION};


	// Class logger
	public static final Logger logger = Logger.getLogger(SendDepositErrorEmail.class);

	// The Email template
	private static Template emailTemplate;

	// The server properties
	private static Hashtable serverProperties;

	// The specific email Properties
	private static Hashtable emailProperties;


	/**
	 *
	 * @param request - the request with all the parameters
	 * @throws Exception - if template not found
	 */
	public SendDepositErrorEmail(HashMap params) throws Exception {

		logger.debug("Trying to send email with the following params: " + params);

		// check to see if the variables init or not
		init(params);

		// set the specific email properties
		//Use constant instead of hard coded key and  put all code in the init method.
		/* emailProperties.put("to", CommonUtil.getProperty("contact.email"));

		emailProperties.put("body", getEmailBody(params));*/

	}

	/**
	 *
	 */
	private void init(HashMap params) throws Exception {

		// set the server properties
		serverProperties = new Hashtable();
		serverProperties.put("url", CommonUtil.getProperty("email.server"));
		serverProperties.put("auth", "true");
		serverProperties.put("user", CommonUtil.getProperty("email.uname"));
		serverProperties.put("pass", CommonUtil.getProperty("email.pass"));

		// Set the email properties
		emailProperties = new Hashtable();
		//Put params from outside params
		emailProperties.put(MAIL_FROM, params.get(PARAM_EMAIL));
		emailProperties.put(MAIL_TO, params.get(MAIL_TO));
		emailProperties.put(MAIL_SUBJECT, params.get(MAIL_SUBJECT));
		emailProperties.put("body", getEmailBody(params));

	}

	public void run() {
		logger.debug("Sending Email ...");
		// send the email
		CommonUtil.sendEmail(serverProperties, emailProperties, null);
		logger.debug("Sending Email completed ");

	}

	public String getEmailBody(HashMap params) throws Exception {

		StringBuffer mailBody = new StringBuffer("");

		String paramValue , paramKey;
		//If we have params and not empty then we will populate the mail body
		if(null!= params && ! params.isEmpty())
		{
			for ( int i= 0 ; i < MAIL_FIELDS_LIST.length; i++)
			{
				paramKey = MAIL_FIELDS_LIST[i];

				paramValue = (String)params.get(paramKey);
				if(!CommonUtil.isParameterEmptyOrNull(paramValue))
				{
					mailBody.append(paramKey).
							append(DELIMITER).
							append(paramValue).
							append(END_OF_LINE);
				}
			}

		}


		return mailBody.toString();

	}



}
