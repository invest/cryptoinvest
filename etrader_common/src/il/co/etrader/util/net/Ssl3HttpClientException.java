package il.co.etrader.util.net;

public class Ssl3HttpClientException extends Exception {

	/**
	 * generated UID 
	 */
	private static final long serialVersionUID = 4431615921646510311L;

	public Ssl3HttpClientException(String message) {
		super(message);
	}

}
