package il.co.etrader.util.net;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;

import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLHandshakeException;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.BasicClientConnectionManager;


public class Ssl3HttpClient {
	
	public static final String CONTENT_TYPE = "Content-Type";
	public static final String CONTENT_TYPE_FORM = "application/x-www-form-urlencoded";
	
	/**
	 * @param what Http post request ( e.g paramName1=paramValue1&paramName2=paramValue2 )
	 * @param where Uri for the request 
	 * @return response from server
	 * @throws SSLHandshakeException 
	 * @throws UnsupportedEncodingException 
	 * @throws Exception
	 */
	public static StringBuffer sendPost(String what, String where) throws Ssl3HttpClientException {
		BufferedReader reader = null;
		try{
				HttpClient client = createTestHttpClient();
				HttpPost method = new HttpPost(where);
				method.setHeader(CONTENT_TYPE, CONTENT_TYPE_FORM);
				method.setEntity(new StringEntity(what));
		
			    HttpResponse response = client.execute(method);
			    int statusCode = response.getStatusLine().getStatusCode();
			    if (statusCode != HttpStatus.SC_OK) {
			        throw new Exception("Error code : " + statusCode);
			    }
			    InputStream in = response.getEntity().getContent();
			    reader = new BufferedReader(new InputStreamReader(in));
			    StringBuffer buffer = new StringBuffer();
			    String strLine;
			    while ((strLine = reader.readLine()) != null)   {
			    	buffer.append(strLine);
			    }
			    return buffer;
		    } catch(Exception e) {
		    	throw new Ssl3HttpClientException(e.getMessage());
		    } finally {
		    	try{
		    		if(reader != null) {
		    			reader.close();
		    		}
		    	} catch(Exception e){}
		    }
	}

	/**
	 * Creates Ssl client supporting only Ssl3 - to fix compatibility issues with certain servers
	 * @return
	 * @throws Exception
	 */
	private static HttpClient createTestHttpClient() throws Exception {
	    	SSLContext sslContext =	SSLContext.getInstance("SSLv3");
	    	sslContext.init(null, null, null);
    		Sslv3SocketFactory sslSocketFactory = new Sslv3SocketFactory(sslContext, SSLSocketFactory.STRICT_HOSTNAME_VERIFIER); 
	        SchemeRegistry registry = new SchemeRegistry();
	        registry.register(new Scheme("https", 443, sslSocketFactory));
	        registry.register(new Scheme("http",80, PlainSocketFactory.getSocketFactory()));
	        ClientConnectionManager ccm = new BasicClientConnectionManager(registry);
	        return new DefaultHttpClient(ccm);
	}
}
