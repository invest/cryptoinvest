package il.co.etrader.util;

import java.lang.management.ManagementFactory;

import javax.management.MBeanServer;
import javax.management.ObjectName;

import org.apache.log4j.Level;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

public class LogLevelManager implements LogLevelManagerMBean {
    private static final Logger log = Logger.getLogger(LogLevelManager.class);
    
    protected String jmxName;
    
    public void setLoggerLevel(String category, String level) {
        if (null == level || level.equals("") || level.equalsIgnoreCase("null")) {
            // TODO: setting null doesn't seems to work properly. See how properly to set it back
            // to use the parent level.
            LogManager.getLogger(category).setLevel(null);
        }
        LogManager.getLogger(category).setLevel(Level.toLevel(level));
    }    

    public String getLoggerLevel(String category) {
        Logger logger = LogManager.getLogger(category);
        if (null == logger) {
            return "No such logger.";
        }
        Level level = logger.getLevel();
        if (null == level) {
            return "No level.";
        }
        return LogManager.getLogger(category).getLevel().toString();
    }

    /**
     * Register the instance in the platform MBean server under given name.
     *
     * @param name the name under which to register
     */
    public void registerJMX(String name) {
        jmxName = name;
        try {
            MBeanServer mbs = ManagementFactory.getPlatformMBeanServer();
            ObjectName on = new ObjectName(jmxName);
            mbs.registerMBean(this, on);
            if (log.isInfoEnabled()) {
                log.info("LogLevelManager MBean registered - name: " + jmxName);
            }
        } catch (Exception e) {
            log.error("Failed to register in the MBean server.", e);
        }
    }

    /**
     * Unregister the instance from the platform MBean server. Use the name
     * under which it was registered.
     */
    public void unregisterJMX() {
        try {
            MBeanServer mbs = ManagementFactory.getPlatformMBeanServer();
            ObjectName on = new ObjectName(jmxName);
            mbs.unregisterMBean(on);
            if (log.isInfoEnabled()) {
                log.info("LogLevelManager MBean unregistered - name: " + jmxName);
            }
        } catch (Exception e) {
            log.error("Failed to unregister from the MBean server.", e);
        }
    }
}