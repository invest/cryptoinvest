package il.co.etrader.util;

import java.io.FileOutputStream;
import java.io.IOException;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Font.FontFamily;
import com.itextpdf.text.Image;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfWriter;

/**
 * PDF creator example, implemented with iText
 *
 * @author Kobi
 *
 */
public class PDFCreator {

    /** Path to the resulting PDF file. */
    public static final String RESULT =
        "c:/PDF_Results/pageSizeExm.pdf";

    public static final String RESOURCE =
    	"http://picbase.anyoption.com/images/email/temp_head.jpg";

    /**
     * Creates a PDF file: hello.pdf
     * @param    args    no arguments needed
     */
    public static void main(String[] args) throws DocumentException, IOException {
        // step 1
        Document document = new Document();
        // step 2
        PdfWriter.getInstance(document, new FileOutputStream(RESULT));
        document.setPageSize(PageSize.A4);
        document.setMargins(36, 72, 108, 180);
        document.setMarginMirroring(true);
        // step 3
        document.open();
        // step 4
        //   Add an image
        Image header = Image.getInstance(String.format(RESOURCE));

        header.setAlignment(Image.ALIGN_CENTER);
        header.scaleAbsolute(400f, 84f);
        document.add(header);

        // new line
        Phrase phrase = new Phrase("");
        phrase.add("\n");
        document.add(phrase);
        document.add(phrase);

        Font font = new Font(FontFamily.TIMES_ROMAN, 12);
        Font fontBold = new Font(FontFamily.TIMES_ROMAN, 12, Font.BOLD);

        document.add(new Paragraph(
            "The left margin of this odd page is 36pt (0.5 inch); " +
            "the right margin 72pt (1 inch); " +
            "the top margin 108pt (1.5 inch); " +
            "the bottom margin 180pt (2.5 inch).", font));
        Paragraph paragraph = new Paragraph();
        paragraph.setAlignment(Element.ALIGN_JUSTIFIED);
        paragraph.setFont(font);
        for (int i = 0; i < 20; i++) {
            paragraph.add("Hello World! Hello People! " +
            		"Hello Sky! Hello Sun! Hello Moon! Hello Stars!");
        }
        document.add(paragraph);

        document.add(phrase);

        document.add(new Paragraph(
            "The right margin of this even page is 36pt (0.5 inch); " +
            "the left margin 72pt (1 inch).", fontBold));
        // step 5

        document.close();
    }

}