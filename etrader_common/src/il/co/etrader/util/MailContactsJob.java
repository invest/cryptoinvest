package il.co.etrader.util;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import com.anyoption.common.beans.Issue;
import com.anyoption.common.beans.IssueAction;
import com.anyoption.common.daos.LanguagesDAOBase;

import il.co.etrader.bl_managers.IssuesManagerBase;
import il.co.etrader.bl_vos.Contact;
import il.co.etrader.bl_vos.Template;
import il.co.etrader.bl_vos.UserBase;
import il.co.etrader.bl_vos.Writer;
import il.co.etrader.dao_managers.IssuesDAOBase;
import il.co.etrader.dao_managers.SkinsDAOBase;
import il.co.etrader.dao_managers.TemplatesDAO;

public class MailContactsJob extends JobUtil {
	 private static Logger log = Logger.getLogger(MailContactsJob.class);
	 public static final long TEMPLATES_TYPE_SEQUENCE = 2;
	 public static final String OFFSET_GMT = "GMT+00:00";
	 public static String langIdForIssueCommant = "";

	 protected static Hashtable<String,String> initServerParameters() {
	    Hashtable<String,String> server = new Hashtable<String,String>();
	    server.put("url", getPropertyByFile("email.url"));
	    server.put("auth", getPropertyByFile("email.auth"));
	    server.put("user", getPropertyByFile("email.user"));
	    server.put("pass", getPropertyByFile("email.pass"));
	    server.put("contenttype", getPropertyByFile("email.contenttype", "text/html; charset=UTF-8"));
	    log.info("Initialize server parameters: " + server.toString());
	    return server;
	 }

	 protected static HashMap<String, String> initEmailParameters(Contact c, Template t) throws Exception {
    	HashMap<String, String> params = new HashMap<String, String>();
        params.put("subject", t.getSubject());
	    params.put("to", c.getEmail());
	    if (CommonUtil.isHebrewSkin(c.getSkinId())) {
	    	params.put("from", getPropertyByFile("email.from.etrader"));
	    } else {
	    	params.put("from", getPropertyByFile("email.from.anyoption"));
	    }
		if (!CommonUtil.isParameterEmptyOrNull(c.getFirstName())) {
			params.put("userFirstName", c.getFirstName());
		} else {
			if (!CommonUtil.isParameterEmptyOrNull(c.getText())) {
				params.put("userFirstName", c.getText());
			} else {
				params.put("userFirstName", ConstantsBase.EMPTY_STRING);
			}
		}


		params.put("supportPhone", String.valueOf(getSupportPhoneByCountryId(c.getCountryId())));
		//params.put("minInvest", "?????????");
		//params.put("maxInvest", "???????????");
		//params.put("bonusExpDate", "bonus Exp Date");
		params.put("currency", getCurrencyBySkin(c.getSkinId()));
		//params.put("bonusExpDays", "???????????");
		log.info("Initialize email parameters: " + params.toString());
		return params;
	 }

	 protected static String getCurrencyBySkin(long skinId) {
		String currency = "";
		log.info("Get currency for skin " + skinId);
		switch ((int)skinId) {
			case 1:
				currency = properties.getProperty("currency.ils");
				break;
			case 2:
			case 4:
			case 13:
			case 14:
				currency = properties.getProperty("currency.usd");
				break;
			case 5:
			case 8:
			case 9:
			case 12:
				currency = properties.getProperty("currency.eur");
				break;
			case 10:
				currency = properties.getProperty("currency.rub");
				break;
			case 3:
				currency = properties.getProperty("currency.try");
				break;
			case 15:
				currency = properties.getProperty("currency.cny");
			default:
				currency = properties.getProperty("currency.usd");
				break;
			}
		return currency;
	 }

	 protected static String getSupportPhoneByCountryId(long countryId) throws Exception {
		PreparedStatement ps = null;
		ResultSet rs = null;
		Connection con = getConnection();
		try {
			String sql =
				" SELECT " +
				"      c.* " +
				" FROM " +
				"      countries c " +
				" WHERE " +
				"      c.id = ? ";

			ps = con.prepareStatement(sql);
			ps.setLong(1, countryId);
			rs = ps.executeQuery();

			if (rs.next()) {
				log.info("Got country support phone code");
				return rs.getString("SUPPORT_PHONE");
			}
		} catch (Exception e) {
			log.log(Level.FATAL,"Can't get contacts ",e);
		} finally {
			try {
				rs.close();
			} catch (Exception e) {
				log.log(Level.ERROR,"Can't close",e);
			}
			try {
				ps.close();
			} catch (Exception e) {
				log.log(Level.ERROR,"Can't close",e);
			}
			try {
				con.close();
			} catch (Exception e) {
				log.log(Level.ERROR,"Can't close",e);
			}
		}

		return null;
	}

	protected static boolean send(Hashtable<String,String> server, String templateName, HashMap<String, String> params,
			String subjectKey,String commentKey, long issueSubjectId,
			Contact contact, Long recordId, long templateId) throws Exception {
		Connection con = getConnection();
		try {
			return sendSingleEmailToContact(con, server, templateName, params, subjectKey, commentKey, issueSubjectId, contact, recordId, templateId);
		} finally {
			con.close();
		}
	}

	protected static boolean sendSingleEmailToContact(Connection conn, Hashtable serverProperties,
	   		String templateName,
	   		HashMap<String, String> params, String subjectKey, String commentKey,
	   	    long issueSubjectId, Contact contact, Long recordId, long templateId)
	   		throws Exception {

	   	long langId = SkinsDAOBase.getById(conn, (int)contact.getSkinId()).getDefaultLanguageId();
	   	String langCode = LanguagesDAOBase.getCodeById(conn, langId);
	   	langIdForIssueCommant = langCode;
	   	log.debug("skinId: " + contact.getSkinId() + " , langId: " + langId + ", langCode: " + langCode);
	   	try {
	   		mailTemplate = getEmailTemplate(templateName, langCode, String.valueOf(contact.getSkinId()));
	   	} catch (Exception e) {
	   		log.error("!!! ERROR >> get email template, template does not exists");
	   		return false;
	   	}
	   	Hashtable<String, String> emailProperties = new Hashtable<String, String>();
	   	String subjectParam = properties.getProperty(params.get("subject") + "." + langCode);
	   	if (!CommonUtil.isParameterEmptyOrNull(subjectParam)) {
	   		emailProperties.put("subject", subjectParam);
	   	} else {
	   		log.error("!!! ERROR >> get email subject, subject is null");
	   		return false;
		}
	   	String fromParam = params.get("from");
	   	if (fromParam != null) {
	   		emailProperties.put("from", fromParam);
	   	}

		if (properties.getProperty("send.to").equalsIgnoreCase("Live")){
			String toParam = params.get("to");
		   	if (toParam != null) {
		   		emailProperties.put("to", toParam);
		   	} else {
		   		log.error("!!! ERROR >> get email address, email is null");
		   		return false;
		   	}
		} else {
			emailProperties.put("to", properties.getProperty("send.to"));
		}

	   	try {
	   		emailProperties.put("body",getEmailBody(params));
	   	} catch (Exception e) {
		    	log.fatal("!!! ERROR >> getEmailBody: " + e.getMessage());
		    	return false;
	    }
	    sendSingleEmail(properties,emailProperties, langCode);
	    log.debug("Email " + templateName + " was sent to " + contact.toString());
	    return true;
   }

	protected static Template getTemplate(long templateId) throws SQLException {
		Connection con = getConnection();
		try {
			return TemplatesDAO.get(con, templateId);
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			con.close();
		}
		return null;
	}

	public static boolean insertIssue(Issue issue, IssueAction issueAction,String utcOffset,long writerId, UserBase user, int screenId) throws SQLException {
		Connection con = getConnection();
    	boolean res = false;
    	try {
    		con.setAutoCommit(false);

			// Insert Issue
			IssuesDAOBase.insertIssue(con, issue);
			issueAction.setIssueId(issue.getId());
			issueAction.setWriterId(writerId);
			issueAction.setActionTime(new Date());
			issueAction.setActionTimeOffset(utcOffset);
			// Insert Issue action
			res = IssuesManagerBase.validateAndInsertAction(con, issueAction, user, issue, screenId);
			if (!res){
				log.error("Can not insert issue action");
				con.rollback();
				return false;
			}else{
				con.commit();
				return true;
			}
			//insertAction(con, issueAction);
    	} catch (Exception e) {
    		e.printStackTrace();
    		try {
				con.rollback();
			} catch (SQLException ie) {
			}

		} finally {
			con.setAutoCommit(true);
    		con.close();
    	}
		return false;
	}

	public static void createAndInsertIssueAndIssueAction(Contact c, Template t, String[] args) throws Exception {
		Issue issue = new Issue();
		IssueAction issueAction = new IssueAction();
		//Filling issue fields
		issue.setSubjectId(String.valueOf(IssuesManagerBase.ISSUE_SUBJ_GENERAL));
		issue.setStatusId(String.valueOf(IssuesManagerBase.ISSUE_STATUS_ON_PROGRESS));
		issue.setUserId(c.getUserId());
		issue.setContactId(c.getId());
		issue.setPriorityId(String.valueOf(IssuesManagerBase.ISSUE_PRIORITY_LOW));
		issue.setType(ConstantsBase.ISSUE_TYPE_REGULAR);

		// Filling action fields
		issueAction.setChannelId(String.valueOf(IssuesManagerBase.ISSUE_CHANNEL_EMAIL));
		issueAction.setActionTypeId(String.valueOf(IssuesManagerBase.ISSUE_ACTION_EMAIL));
		issueAction.setWriterId(Writer.WRITER_ID_WEB);
		issueAction.setSignificant(false);
		issueAction.setActionTime(new Date());
		issueAction.setActionTimeOffset(OFFSET_GMT);
		issueAction.setTemplateId(t.getId());
		args[0] = String.valueOf(c.getId());
		String subject = properties.getProperty(t.getSubject() + "." + langIdForIssueCommant);
		issueAction.setComments(subject);
		UserBase user = new UserBase();
		user.setId(c.getUserId());
		user.setContactId(c.getId());
		user.setEmail(c.getEmail());
		boolean res = insertIssue(issue,issueAction,c.getUtcOffset(), Writer.WRITER_ID_WEB, user, 0);
		if (res) {
		    log.info("Insert issue , contact id " + c.getId() + ".");
		} else {
			log.info("Insert issue was unsuccessful , contact id " + c.getId() + ".");
		}
	}
}
