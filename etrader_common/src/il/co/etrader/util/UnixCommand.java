package il.co.etrader.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class UnixCommand {

	public static void main(String args[]) {

	       String s = null;

	        try {

	        	if ( args.length > 0 ) {

		            String ip = args[0];
		            System.out.println("ip: " + ip);

		            Process p = Runtime.getRuntime().exec("geoiplookup " + ip);

		            BufferedReader stdInput = new BufferedReader(new
		                 InputStreamReader(p.getInputStream()));

		            BufferedReader stdError = new BufferedReader(new
		                 InputStreamReader(p.getErrorStream()));

		            // read the output from the command

		            System.out.println("Here is the standard output of the command:\n");
		            while ((s = stdInput.readLine()) != null) {
		                System.out.println(s);
		            }

		            // read any errors from the attempted command

		            System.out.println("Here is the standard error of the command (if any):\n");
		            while ((s = stdError.readLine()) != null) {
		                System.out.println(s);
		            }

		        }  else {
		        	System.out.println("need ip argument");
		        }

	        } catch (IOException e) {
		            System.out.println("exception happened - here's what I know: ");
		            e.printStackTrace();
		            System.exit(-1);
		    }
	}

}




