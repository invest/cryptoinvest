//package il.co.etrader.jms;
//
//import java.io.Serializable;
//import java.text.SimpleDateFormat;
//import java.util.Date;
//import java.util.HashMap;
//import java.util.Iterator;
//import java.util.Map;
//
//import com.anyoption.common.beans.BinaryZeroOneHundred;
//import com.anyoption.common.beans.OpportunitySkinGroupMap;
//import com.anyoption.common.enums.SkinGroup;
//
///**
// * A message published by LevelService and received from LevelsCache.
// * This message brings real level and optionally current level
// * (in the cases current level is not ready yet).
// *
// * @author Tony
// */
//public class ETraderFeedMessage implements Serializable {
//    private static final long serialVersionUID = -6885973956330287381L;
//
//    public HashMap<Integer, Boolean> home;
//	public HashMap<String, String> currentValues = null;
//	public long lifeId;
//    public double level;
//    public long groupId;
//    public long oppId;
//    public double realLevel;
//    public long timeQueued;
//    public long timeDispatched;
//    public double devCheckLevel;
//	public HashMap<Integer, HashMap<String, Integer>> skinsPriorities;
//	//for tt
//	public double autoShiftParameter;
//	public double shiftParameter;
//	public double shiftParameterAO;
//	public double calls;
//	public double puts;
//	public long maxExposure;
//	public String feedName;
//	public long decimalPoint;
//	public boolean isTraderDisable;
//	public boolean isAutoDisable;
//	public double contractsBought;
//	public double contractsSold;
//	public BinaryZeroOneHundred parameters;
//	public double bid;
//	public double offer;
//	public String investmentsRejectInfo;
//	public boolean notClosed;
//	// TODO
//	public Map<SkinGroup, OpportunitySkinGroupMap> skinGroupMapping;
//
//
//    /**
//     * Create message.
//     *
//     * @param home <code>true</code> if this opportunity is on the home page else <code>false</code>
//     * @param currentValues the values for the current level. This values are actually designed for the LS update.
//     * @param lifeId the service current execution id
//     * @param level the current level
//     * @param groupId the group of the market of the opp
//     * @param oppId the id of the opp
//     * @param realLevel the real level of the opp (the market of the opp)
//     * @param devCheckLevel
//     * @param autoShiftParameter
//     * @param shiftParameter
//     * @param shiftParameterAO
//     * @param calls volume bougth (in $)
//     * @param puts volume sold (in $)
//     * @param maxExposure
//     * @param feedName
//     * @param decimalPoint
//     * @param isTraderDisable
//     * @param contractsBought contracts Bought (binary 0-100) (in $)
//     * @param contractsSold contracts Sold (binary 0-100) (in $)
//     * @param parameters binary 0-100 parameters
//     */
//    public ETraderFeedMessage(
//    		HashMap<Integer, Boolean> home,
//            final HashMap<String, String> currentValues,
//            long lifeId,
//            double level,
//            long groupId,
//            long oppId,
//            double realLevel,
//            double devCheckLevel,
//            HashMap<Integer, HashMap<String, Integer>> skinsPriorities,
//            double autoShiftParameter,
//    		double shiftParameter,
//    		double shiftParameterAO,
//    		double calls,
//    		double puts,
//    		long maxExposure,
//    		String feedName,
//    		long decimalPoint,
//    		boolean isTraderDisable,
//    		boolean isAutoDisable,
//    		double contractsBought,
//    		double contractsSold,
//    		BinaryZeroOneHundred parameters,
//    		double bid,
//    		double offer,
//    		String investmentsRejectInfo,
//    		boolean notClosed
//            ) {
//        this.home = home;
//        this.currentValues = currentValues;
//        this.lifeId = lifeId;
//        this.level = level;
//        this.groupId = groupId;
//        this.oppId = oppId;
//        this.realLevel = realLevel;
//        this.devCheckLevel = devCheckLevel;
//        this.skinsPriorities = skinsPriorities;
//        this.autoShiftParameter = autoShiftParameter;
//        this.shiftParameter = shiftParameter;
//        this.shiftParameterAO = shiftParameterAO;
//        this.calls = calls;
//        this.puts = puts;
//        this.maxExposure = maxExposure;
//        this.feedName = feedName;
//        this.decimalPoint = decimalPoint;
//        this.isTraderDisable = isTraderDisable;
//        this.isAutoDisable = isAutoDisable;
//        this.contractsBought = contractsBought;
//        this.contractsSold = contractsSold;
//        this.parameters = parameters;
//        this.bid = bid;
//		this.offer = offer;
//		this.investmentsRejectInfo = investmentsRejectInfo;
//		this.notClosed = notClosed;
//    }
//
//    public String toString() {
//        String ls = System.getProperty("line.separator");
//        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss:SSS Z");
//        StringBuffer sb = new StringBuffer();
//        sb.append(ls).append("ETraderFeedMessage: ").append(ls);
//        sb.append("home: ").append(null != home ? home.toString() : "null").append(ls);
//        sb.append("lifeId: ").append(lifeId).append(ls);
//        sb.append("level: ").append(level).append(ls);
//        sb.append("gropId: ").append(groupId).append(ls);
//        sb.append("oppId: ").append(oppId).append(ls);
//        sb.append("realLevel: ").append(realLevel).append(ls);
//        sb.append("devCheckLevel: ").append(devCheckLevel).append(ls);
//        sb.append("timeQueued: ").append(timeQueued > 0 ? sdf.format(new Date(timeQueued)) : "").append(ls);
//        sb.append("timeDispatched: ").append(timeDispatched > 0 ? sdf.format(new Date(timeDispatched)) : "").append(ls);
//        if (null != currentValues) {
//            String key = null;
//            for (Iterator<String> i = currentValues.keySet().iterator(); i.hasNext();) {
//                key = i.next();
//                sb.append(key).append(": ").append(currentValues.get(key)).append(ls);
//            }
//        }
//        if (null != skinsPriorities) {
//            Integer intKey = null;
//            for (Iterator<Integer> i = skinsPriorities.keySet().iterator(); i.hasNext();) {
//                intKey = i.next();
//                sb.append(intKey.toString()).append(": ").append(skinsPriorities.get(intKey).toString()).append(ls);
//            }
//        }
//        sb.append("autoShiftParameter: ").append(autoShiftParameter).append(ls);
//        sb.append("shiftParameter: ").append(shiftParameter).append(ls);
//        sb.append("shiftParameterAO: ").append(shiftParameterAO).append(ls);
//        sb.append("calls: ").append(calls).append(ls);
//        sb.append("puts: ").append(puts).append(ls);
//        sb.append("maxExposure: ").append(maxExposure).append(ls);
//        sb.append("feedName: ").append(feedName).append(ls);
//        sb.append("decimalPoint: ").append(decimalPoint).append(ls);
//        sb.append("isTraderDisable: ").append(isTraderDisable).append(ls);
//        sb.append("isAutoDisable: ").append(isAutoDisable).append(ls);
//        sb.append("contractsBought: ").append(contractsBought).append(ls);
//        sb.append("contractsSold: ").append(contractsSold).append(ls);
//        sb.append("parameters: ").append(null != parameters ? parameters.toString() : "null").append(ls);
//        sb.append("bid: ").append(bid).append(ls);
//        sb.append("offer: ").append(offer).append(ls);
//        sb.append("investmentsRejectInfo: ").append(investmentsRejectInfo).append(ls);
//        sb.append("notClosed: ").append(notClosed).append(ls);
//
//        return sb.toString();
//    }
//}