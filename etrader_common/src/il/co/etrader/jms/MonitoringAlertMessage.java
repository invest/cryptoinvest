//package il.co.etrader.jms;
//
//import java.io.Serializable;
//
///**
// * A message published by LevelService and received from LevelsCache.
// * This message brings notification about opportunity disabled/enabled
// * by the monitoring.
// * 
// * @author Tony
// */
//public class MonitoringAlertMessage implements Serializable {
//    private static final long serialVersionUID = 1L;
//
//    public long marketId;
//    public boolean disabled;
//    public String description;
//    
//    /**
//     * Create a message.
//     * 
//     * @param marketId the id of the market about which is the event
//     * @param disabled the new status of the opportunity (disabled/enabled)
//     * @param description text description of the reason of the event
//     */
//    public MonitoringAlertMessage(long marketId, boolean disabled, String description) {
//        this.marketId = marketId;
//        this.disabled = disabled;
//        this.description = description;
//    }
//    
//    public String toString() {
//        String ls = System.getProperty("line.separator");
//        return ls + "MonitoringAlertMessage:" + ls +
//            "marketId: " + marketId + ls +
//            "disabled: " + disabled + ls +
//            "description: " + description + ls;
//    }
//}