//package il.co.etrader.jms;
//
//import java.util.HashMap;
//
//import com.anyoption.common.jms.SimpleLevelsCacheListener;
//
///**
// * Interface to be implemented by classes interested in "COMMAND" mode
// * subscribtions updates.
// *
// * The current possible subscribtions are "home", "group2",
// * "group3", "group4" and "group5".
// * See Lightstreamer docs for more information.
// *
// * @author Tony
// */
//public interface LevelsCacheListener extends SimpleLevelsCacheListener {
//    /**
//     * Notify about "COMMAND" mode subscription event (command).
//     *
//     * @param itemName the name of the "COMMAND" mode subscribtion this update is for.
//     * @param update the update - the command and its fields
//     */
//    public void update(String itemName, HashMap<String, String> update);
//
//    /**
//     * Notify personal insurances subscriptions about opportunity update. The
//     * key entry in the update keep the opportunity id. The update must be
//     * cloned before passed to Lightstreamer as Lightstreamer engine add
//     * internal fields to the update that prevent it from beeing used in another
//     * notification to the engine.
//     *
//     * @param opportunityId
//     * @param update
//     */
//    public void insurancesUpdate(long opportunityId, HashMap<String, String> update, double level);
//}