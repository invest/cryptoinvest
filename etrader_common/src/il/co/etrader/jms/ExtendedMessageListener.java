//package il.co.etrader.jms;
//
//import javax.jms.ExceptionListener;
//import javax.jms.MessageListener;
//
///**
// * Combine MessageListener and ExceptionListener in one interface.
// */
//public interface ExtendedMessageListener extends MessageListener, ExceptionListener {
//}