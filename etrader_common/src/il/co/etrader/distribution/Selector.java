package il.co.etrader.distribution;

public interface Selector<T extends Object> {
	public int getId();
	public T select();

}
