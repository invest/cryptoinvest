package il.co.etrader.distribution;

public class DistributionHistory {
	String writer;
	String dateCreated;
	
	public DistributionHistory(String writer, String dateCreated) {
		this.writer = writer;
		this.dateCreated = dateCreated;
	}
	public String getWriter() {
		return writer;
	}
	public void setWriter(String writer) {
		this.writer = writer;
	}
	public String getDateCreated() {
		return dateCreated;
	}
	public void setDateCreated(String dateCreated) {
		this.dateCreated = dateCreated;
	}
	
	
}
