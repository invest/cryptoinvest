package il.co.etrader.jobs;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Hashtable;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.apache.log4j.Logger;

import com.anyoption.common.beans.Job;
import com.anyoption.common.jobs.ScheduledJob;
import com.anyoption.common.util.CommonUtil;
import com.anyoption.common.util.Utils;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;

/**
 * @author eran.levy
 * AR-2754
 * Facebook Integration - Send automatic report from facebook (order ID)
 *
 */
public class FBOrderIdJob implements ScheduledJob {
		
	private static final Logger log = Logger.getLogger(FBOrderIdJob.class);	
	
	private static String EMAIL_TO 		= "emailTo";
	private static String FB_URL 		= "fbURL";
	private static String DAYS 			= "days";
	private static String PIXEL_ID 		= "pixelId";
	private static String APP_ID 		= "appId";
	private static String ACCESS_TOKEN 	= "accessToken";
	private static String MAIL_SUBJECT 	= "mailSubject";

	private String emailTo;
	private String fbURL;
	private int days; 
	private String pixelId;
	private String appId;
	private String accessToken;	
	private String mailSubject;
	
	private static String fromDate;
	private static String toDate;
	
	@Override
	public void setJobInfo(Job job) {
		if (null != job.getConfig() && !job.getConfig().trim().equals("")) {
            String[] ps = job.getConfig().split(",");
            for (int i = 0; i < ps.length; i++) {
                String[] p = ps[i].split("/=/");
                if (p[0].equals(EMAIL_TO)) {
                	emailTo = p[1].toString();                	
                } else if (p[0].equals(FB_URL)) {
                	fbURL = (p[1].toString());
                } else if (p[0].equals(DAYS)) {
                	days = Integer.valueOf(p[1].toString());
                } else if (p[0].equals(PIXEL_ID)) {
                	pixelId = (p[1].toString());
                } else if (p[0].equals(APP_ID)) {
                	appId = (p[1].toString());
                } else if (p[0].equals(ACCESS_TOKEN)) {
                	accessToken = (p[1].toString());
                } else if (p[0].equals(MAIL_SUBJECT)) {
                	mailSubject = (p[1].toString());
                }
            }
		}
	}
				
	@Override
	public boolean run() {
		log.info("FBOrderIdJob - process START.");
		//Configure from to dates
		long toTimeInMills;
		long fromTimeInMills;
		GregorianCalendar gc = new GregorianCalendar();				
		gc.set(Calendar.HOUR_OF_DAY, 0);
		gc.set(Calendar.MINUTE, 0);
		gc.set(Calendar.SECOND, 0);
		toTimeInMills = gc.getTimeInMillis();
		gc.add(Calendar.DAY_OF_MONTH, -days);
		fromTimeInMills = gc.getTimeInMillis();		
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		fromDate = sdf.format(new Date(fromTimeInMills));
		toDate = sdf.format(new Date(toTimeInMills));
		fromTimeInMills /= 1000;
		toTimeInMills /= 1000;		
		String url = fbURL;
		// app id request
		url = url.replace("FROM_DATE", String.valueOf(fromTimeInMills));
		url = url.replace("TO_DATE", String.valueOf(toTimeInMills));
		url = url + "app_id=" + appId + accessToken;		
		String appIdJsonResponse = getRequest(url);			
		// pixel id request
		url = fbURL;
		url = url.replace("FROM_DATE", String.valueOf(fromTimeInMills));
		url = url.replace("TO_DATE", String.valueOf(toTimeInMills));
		url = url + "pixel_id=" + pixelId + accessToken;		
		String pixelIdJsonResponse = getRequest(url);		
		jsonPrettyPrinting(pixelIdJsonResponse);
		
		String report = "<br/><br/>Web:<br/><br/><pre>" +
						jsonPrettyPrinting(pixelIdJsonResponse) + "</pre>" +
						"<br/><br/>Mobile:<br/><br/><pre>" +
						jsonPrettyPrinting(appIdJsonResponse) + "</pre>";
		log.info(report);
		sendEmail(report);
		log.info("FBOrderIdJob - process END.");
		return true;
	}

	@Override
	public void stop() {		
	}
	
	/**
	 * Pretty print for JSON
	 * @param json
	 * @return
	 */
	private String jsonPrettyPrinting(String json) {
		Gson gson = new GsonBuilder().setPrettyPrinting().disableHtmlEscaping().create();
		JsonParser parser = new JsonParser();
		JsonElement je = parser.parse(json);
		return gson.toJson(je);
	}
	
	/**
	 * Get Request from URL
	 * @param url
	 * @return
	 */
	private String getRequest(String url) {
		log.debug("Going to get request for URL:" + url);
    	HttpClient httpclient = new DefaultHttpClient();
    	String jsonResponse = "";
        try {
            HttpGet httpget = new HttpGet(url);
            httpget.addHeader("Accept", "application/json");
            log.debug("executing request " + httpget.getURI());
            HttpResponse response = httpclient.execute(httpget);
            log.debug("--------------status--------------------------");
            log.debug(response.getStatusLine().toString());
            log.debug("----------------------------------------");
            
            if (response.getStatusLine().getStatusCode() == 200) {
                jsonResponse = EntityUtils.toString(response.getEntity());
                log.debug("--------------content--------------------------");
                log.debug(jsonResponse);
                log.debug("----------------------------------------");                
            }
		} catch (IOException e) {
			log.error("ERROR", e);
        } finally {
            // When HttpClient instance is no longer needed,
            // shut down the connection manager to ensure
            // immediate deallocation of all system resources
            httpclient.getConnectionManager().shutdown();
        }
        return jsonResponse;
    }
	
	/**
	 * Send email
	 */
	private void sendEmail(String content) {
        Hashtable<String, String> serverProperties = new Hashtable<String, String>();
		serverProperties.put("url", Utils.getProperty("email.server"));
		serverProperties.put("auth","true");
		serverProperties.put("user", Utils.getProperty("email.uname"));
		serverProperties.put("pass", Utils.getProperty("email.pass"));
        Hashtable<String, String> email = new Hashtable<String, String>();
        email.put("subject", mailSubject + " " + fromDate + " to:" + toDate);
	    email.put("to", emailTo); 
	    email.put("from", "support@anyoption.com"); 	    
        email.put("body", content);
		CommonUtil.sendEmail(serverProperties, email, null);		
    }
	
}
