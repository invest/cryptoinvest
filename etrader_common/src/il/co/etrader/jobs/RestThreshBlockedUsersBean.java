package il.co.etrader.jobs;

import java.io.Serializable;
import java.util.Date;

import il.co.etrader.bl_vos.UserBase;

/**
 * @author eranl
 *
 */
public class RestThreshBlockedUsersBean extends UserBase implements Serializable  {
	
	private static final long serialVersionUID = 6849886192179206909L;
	
	private Date timeBlocked;

	/**
	 * @return the timeBlocked
	 */
	public Date getTimeBlocked() {
		return timeBlocked;
	}

	/**
	 * @param timeBlocked the timeBlocked to set
	 */
	public void setTimeBlocked(Date timeBlocked) {
		this.timeBlocked = timeBlocked;
	}
	
}
