package il.co.etrader.jobs;

import il.co.etrader.bl_managers.ApplicationDataBase;
import il.co.etrader.util.CommonUtil;

import java.util.Date;
import java.util.HashMap;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.Job;
import com.anyoption.common.beans.base.Currency;
import com.anyoption.common.jobs.ScheduledJob;
import com.anyoption.common.managers.SilverPopManager;
import com.anyoption.common.util.FtpUtil;
import com.anyoption.external.silverpop.SilverPopException;
import com.anyoption.external.silverpop.SilverPopImpex;

public class SilverpopSynchJob implements ScheduledJob {
    private static final Logger log = Logger.getLogger(SilverpopSynchJob.class);
    
    private boolean running;
    private Date lastRunTime;
    private String tempFolder;
    private String srcFolder;
    private String ftpUrl;
    private String ftpUserName;
    private String ftpPasswrod;
    private String spUrl;
    private String spUserName;
    private String spPassword;
    private String spNotifyEmail;
    private String spUserMapFile;
    private String spContactMapFile;
    private int spUserListId;
    private int spContactListId;
    private long spTimeout;
    private int spSleep;
    
    private String testEmail = null;

    public void setJobInfo(Job job) {
        if (null != job.getConfig() && !job.getConfig().trim().equals("")) {
            lastRunTime = job.getLastRunTime();
            String[] ps = job.getConfig().split(",");
            for (int i = 0; i < ps.length; i++) {
                String[] p = ps[i].split("=");
                if (p[0].equals("tempFolder")) {
                    tempFolder = p[1];
                } else if (p[0].equals("ftpUrl")) {
                    ftpUrl = p[1];
                } else if (p[0].equals("ftpUserName")) {
                    ftpUserName = p[1];
                } else if (p[0].equals("ftpPasswrod")) {
                    ftpPasswrod = p[1];
                } else if (p[0].equals("spUrl")) {
                    spUrl = p[1];
                } else if (p[0].equals("spUserName")) {
                    spUserName = p[1];
                } else if (p[0].equals("spPassword")) {
                    spPassword = p[1];
                } else if (p[0].equals("spNotifyEmail")) {
                    spNotifyEmail = p[1];
                } else if (p[0].equals("spUserMapFile")) {
                    spUserMapFile = p[1];
                } else if (p[0].equals("spContactMapFile")) {
                    spContactMapFile = p[1];
                } else if (p[0].equals("spUserListId")) {
                    spUserListId = Integer.parseInt(p[1]);
                } else if (p[0].equals("spContactListId")) {
                    spContactListId = Integer.parseInt(p[1]);
                } else if (p[0].equals("spTimeout")) {
                    spTimeout = Long.parseLong(p[1]);
                } else if (p[0].equals("spSleep")) {
                    spSleep = Integer.parseInt(p[1]);
                } else if (p[0].equals("testEmail")) {
                    testEmail = p[1];
                } else if (p[0].equals("srcFolder")) {
                	srcFolder = p[1];
                }
                else {
                    log.warn("Unknown property " + ps[i]);
                }
            }
        }
        if (log.isInfoEnabled()) {
            String ls = System.getProperty("line.separator");
            log.info("SilverpopSynchJob configured with: " + ls
                    + "lastRunTime: " + lastRunTime + ls
                    + "tempFolder: " + tempFolder + ls
                    + "srcFolder: " + srcFolder + ls
                    + "ftpUrl: " + ftpUrl + ls
                    + "ftpUserName: " + ftpUserName + ls
                    + "spUrl: " + spUrl + ls
                    + "spUserName: " + spUserName + ls
                    + "spNotifyEmail: " + spNotifyEmail + ls
                    + "spUserMapFile: " + spUserMapFile + ls
                    + "spContactMapFile: " + spContactMapFile + ls
                    + "spUserListId: " + spUserListId + ls
                    + "spContactListId: " + spContactListId + ls
                    + "spTimeout: " + spTimeout + ls
                    + "testEmail: " + testEmail + ls
                    + "spSleep: " + spSleep + ls);
        }
    }
    
    public boolean run() {
        try {
            running = true;
            
            // Export users and contacts to files to upload to SilverPop
            HashMap<String, String> currencySymbols = new HashMap<String, String>();
            for (Currency c : ApplicationDataBase.getCurrenciesList()) {
                currencySymbols.put(c.getSymbol(), CommonUtil.getMessage(c.getSymbol(), null));
            }
            String[] files = new String[2];
            if (running) {
                files[0] = SilverPopManager.exportUsers(tempFolder, currencySymbols, testEmail);
            }
            if (running) {
                files[1] = SilverPopManager.exportContacts(tempFolder, testEmail);
            }

            // Upload to SilverPop FTP user list
            FtpUtil ftpUtil = new FtpUtil(ftpUrl, ftpUserName, ftpPasswrod);
            if (running) {
                String[] filesToUpload = new String[2];
                filesToUpload[0] = tempFolder + "/" + files[0];
                filesToUpload[1] = srcFolder + "/" + spUserMapFile;
                running = ftpUtil.uploadFiles(filesToUpload, FtpUtil.FTP_SILVERPOP_UPLOAD_DIRECTORY);
            }
            
            // SilverPop API calls - import/export user list
            SilverPopImpex spApi = new SilverPopImpex(spUrl, spUserName, spPassword, spNotifyEmail);
            if (running) {
                files[0] = spApi.importExport(files[0], spUserMapFile, spUserListId, spTimeout, spSleep, lastRunTime, false);
            }
            
            // Upload to SilverPop FTP contact list
            if(running) {
            	String[] filesToUpload = new String[2];
            	filesToUpload[0] = tempFolder + "/" + files[1];
            	filesToUpload[1] = srcFolder + "/" + spContactMapFile;
                running = ftpUtil.uploadFiles(filesToUpload, FtpUtil.FTP_SILVERPOP_UPLOAD_DIRECTORY);
            }
            
            // SilverPop API calls - import/export contacts list
            if (running) {
                files[1] = spApi.importExport(files[1], spContactMapFile, spContactListId, spTimeout, spSleep, lastRunTime, true);
            }


            files[0] = tempFolder + "/" + files[0];
            files[1] = tempFolder + "/" + files[1];

            // Download from SilverPop FTP
            if (running) {
                running = ftpUtil.downloadFiles(files, FtpUtil.FTP_SILVERPOP_DOWNLOAD_DIRECTORY);
            }
            
            // Import changes from SilverPop to our DB
            if (running) {
                SilverPopManager.importUsers(files[0]);
            }
            if (running) {
                SilverPopManager.importContacts(files[1]);
            }
		}catch(SilverPopException e){
			log.debug("Can't complete silverpop import-export");
			running = false;
        } catch (Exception e) {
            log.error("Can't synch with SilverPop.", e);
            running = false;
        }
        return running;
    }
    
    public void stop() {
        running = false;
    }
}