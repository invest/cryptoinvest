package il.co.etrader.jobs;

import il.co.etrader.bl_managers.TransactionsManagerBase;
import il.co.etrader.util.CommonUtil;

import java.util.Hashtable;
import java.util.Locale;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import com.anyoption.common.beans.Job;
import com.anyoption.common.jobs.ScheduledJob;

/**
 * 
 * Migration job from single process to running by Tomcat.
 * 
 * @author eyal.ohana
 *
 */
public class AutoCancelWithdrawalJob implements ScheduledJob {
	private static Logger log = Logger.getLogger(AutoCancelWithdrawalJob.class);
	public static final String CANCEL_WITHDRAWAL_EMAIL_REPORT_TO = "report_to";
	private static String reportTo;

	@Override
	public void setJobInfo(Job job) {
		if (null != job.getConfig() && !job.getConfig().trim().equals("")) {
            String[] ps = job.getConfig().split(",");
            for (int i = 0; i < ps.length; i++) {
                String[] p = ps[i].split("=");
                if (p[0].equals(CANCEL_WITHDRAWAL_EMAIL_REPORT_TO)) {
                	reportTo = p[1].toString();                	
                }
            }
		}
	}

	@Override
	public boolean run() {
		log.info("AutoCancelWithdrawalJob - process START.");
		StringBuffer report = new StringBuffer();
		try {
			TransactionsManagerBase.SendCancelWithdrawalEmail(report);
		} catch (Exception e) {
			log.error("Problem with cancel withdrawal.", e);
		}
		 // build the report
        String reportBody = "<html><body>" +
        						report.toString() +
        					"</body></html>";
	    if (log.isEnabledFor(Level.INFO)) {
	        log.log(Level.INFO, "Sending report email: " + reportBody);
	    }
	    //  send email
	    Hashtable<String,String> server = new Hashtable<String,String>();
	    server.put("url", CommonUtil.getProperty("email.server"));
	    server.put("auth", CommonUtil.getProperty("email.auth", "false"));
	    server.put("user", CommonUtil.getProperty("email.uname"));
	    server.put("pass", CommonUtil.getProperty("email.pass"));
	    server.put("contenttype", CommonUtil.getProperty("email.contenttype", "text/html; charset=UTF-8"));
	    Hashtable<String,String> email = new Hashtable<String,String>();
	    email.put("subject", CommonUtil.getMessage(new Locale("en"), "cancel.withdrawal.email.report.subject", null));	    
	    email.put("to", reportTo);
	    email.put("from", CommonUtil.getProperty("email.from"));
	    email.put("body", reportBody);
	    CommonUtil.sendEmail(server, email, null);
	    log.info("AutoCancelWithdrawalJob - process FINISH.");
		return true;
	}

	@Override
	public void stop() {
		log.info("AutoCancelWithdrawalJob - STOP.");
	}
	


}
