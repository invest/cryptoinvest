package il.co.etrader.jobs;

import java.util.ArrayList;
import java.util.Date;

import il.co.etrader.bl_managers.DbParametersManager;
import il.co.etrader.bl_managers.TransactionsManagerBase;
import il.co.etrader.bl_managers.UsersManagerBase;
import il.co.etrader.bl_vos.FireServerPixelHelper;
import il.co.etrader.bl_vos.FireServerPixelFields;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.FireServerPixelInfo;
import com.anyoption.common.beans.Job;
import com.anyoption.common.beans.ServerPixel;
import com.anyoption.common.jobs.ScheduledJob;
import com.anyoption.common.managers.ServerPixelsManagerBase;
import com.anyoption.common.util.ConstantsBase;

/**
 * 
 * @author eyal.ohana
 *
 */
public class FireServerPixelsJob implements ScheduledJob {
	private static Logger log = Logger.getLogger(FireServerPixelsJob.class);

	@Override
	public void setJobInfo(Job job) {

	}

	@Override
	public boolean run() {
		log.info("FireServerPixelsJob - process START.");
		try {
			/* General parameters */
			Date timeLastRun = DbParametersManager.getLastRun(DbParametersManager.DB_PARAMS_FIRE_PIXEL);
			Date currentDate = new Date();
			FireServerPixelFields fireServerPixelFields = new FireServerPixelFields();
			fireServerPixelFields.setFromDate(timeLastRun);
			fireServerPixelFields.setToDate(currentDate);
			/* Get all register users and deposits users which have all the conditions to fire pixels for them. */
			ArrayList<FireServerPixelHelper> registerUserList = UsersManagerBase.getUsersToFireServerPixel(fireServerPixelFields);
			ArrayList<FireServerPixelHelper> ftdUserList = TransactionsManagerBase.getFtdToFireServerPixel(fireServerPixelFields);
			/* Fire pixels */
			for (FireServerPixelHelper fsph : registerUserList) {
				FireServerPixelInfo fireServerPixelInfoRegister = new FireServerPixelInfo();
				fireServerPixelInfoRegister.setServerPixelsPublisherId(ServerPixel.SERVER_PIXELS_PUBLISHER_ADQUANT);
				fireServerPixelInfoRegister.setServerPixelsTypeId(ConstantsBase.SERVER_PIXEL_TYPE_REGISTER);
				fireServerPixelInfoRegister.setPublisherIdentifier(null);
				fireServerPixelInfoRegister.setUserId(fsph.getUserId());
				fireServerPixelInfoRegister.setTransactionId(0);
				fireServerPixelInfoRegister.setDeviceId(null);
				fireServerPixelInfoRegister.setPlatformId(fsph.getPlatformId());
				String orderId = fsph.getAdvertisingId();
				if (null == orderId) {
					orderId = fsph.getIdfa();
				}
				if (null != orderId) {
					fireServerPixelInfoRegister.setOrderId(orderId);
					ServerPixelsManagerBase.fireServerPixel(fireServerPixelInfoRegister);
				} else {
					log.info("Can't find idfa or android advertising id to the user: " + fsph.getUserId());
				}				
			}
			for (FireServerPixelHelper fsph : ftdUserList) {
				FireServerPixelInfo fireServerPixelInfoFtd = new FireServerPixelInfo();
				fireServerPixelInfoFtd.setServerPixelsPublisherId(ServerPixel.SERVER_PIXELS_PUBLISHER_ADQUANT);
				fireServerPixelInfoFtd.setServerPixelsTypeId(ConstantsBase.SERVER_PIXEL_TYPE_FIRST_DEPOSIT);
				fireServerPixelInfoFtd.setPublisherIdentifier(null);
				fireServerPixelInfoFtd.setUserId(fsph.getUserId());
				fireServerPixelInfoFtd.setTransactionId(fsph.getTransactionId());
				fireServerPixelInfoFtd.setDeviceId(null);
				fireServerPixelInfoFtd.setPlatformId(fsph.getPlatformId());
				String orderId = fsph.getAdvertisingId();
				if (null == orderId) {
					orderId = fsph.getIdfa();
				}
				if (null != orderId) {
					fireServerPixelInfoFtd.setOrderId(orderId);
					ServerPixelsManagerBase.fireServerPixel(fireServerPixelInfoFtd);
				} else {
					log.info("Can't find idfa or android advertising id to the user: " + fsph.getUserId());
				}
			}
			/* update time Last Run */
			DbParametersManager.updateLastRun(DbParametersManager.DB_PARAMS_FIRE_PIXEL, currentDate);
		} catch (Exception e){
			log.error("Error, Problem with FireServerPixelsJob.", e);
		}
		log.info("FireServerPixelsJob - process END.");
		return true;
	}

	@Override
	public void stop() {
		log.info("FireServerPixelsJob - STOP.");
	}
}
