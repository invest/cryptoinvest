package il.co.etrader.jobs;


import il.co.etrader.bl_managers.UsersManagerBase;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.Job;
import com.anyoption.common.jobs.ScheduledJob;
import com.anyoption.common.managers.SkinsManagerBase;
import com.anyoption.common.util.CommonUtil;
import com.anyoption.common.util.Utils;

/**
 * @author eranl
 * AR-1327 A daily summary mail to be sent at the EOD for each past day with the following data:
 *		Subject line:
 *		Daily summary - [Amount of users] Users blocked from trading (questionnaire restricted status and reached loss threshold) - [Date]
 *		Email body - for each user blocked in a different line:
 */
public class RestrictedBlockedUsersMailJob implements ScheduledJob  {	
      
    private static final Logger log = Logger.getLogger(RestrictedBlockedUsersMailJob.class);
    private String emailFrom;
    private String emailTo;
    private String system;

    @Override
    public void setJobInfo(Job job) {
    	emailFrom = CommonUtil.getConfig("email.from");
    	emailTo = CommonUtil.getConfig("email.to.restricted.questionnaire");
    	system = CommonUtil.getConfig("system");
    }

    @Override
    public boolean run() {
    	long beginTime = System.currentTimeMillis();
    	try {		
    		SimpleDateFormat formatDate = new SimpleDateFormat("dd/MM/yyyy HH:mm");
    		Calendar c = Calendar.getInstance();
			c.add(Calendar.DAY_OF_MONTH, -1);
			Date dayBefore = new Date(c.getTimeInMillis());	    
			ArrayList<RestThreshBlockedUsersBean> users = UsersManagerBase.getRestThreshBlockedUsersData();	    
			String mailSubject ="Daily summary - " + system + " - " + users.size() + " Users blocked from trading " +
								"(questionnaire restricted status and reached loss threshold) - " 
								+ Utils.getDateTimeFormatDisplay(dayBefore, null, " ","dd/MM/yyyy");	    
	    
			String mailBody = "";
			if (users.size() == 0) {
				mailBody = "No - Users blocked from trading (questionnaire restricted status and reahed"
							+ " loss threshold) - have been found for last 24 hours";
			} else {
				for (RestThreshBlockedUsersBean user : users) {
        			String skinName;
        			try {
        				Locale locale = new Locale("en");
        				skinName = CommonUtil.getMessage(locale, SkinsManagerBase
        						.getSkin(user.getSkinId()).getDisplayName(), null);
        			} catch (Exception e) {
        				log.error("cant get skin name will use skin id");
        				skinName = String.valueOf(user.getSkinId());
        			}
					//User ID: [User ID] - Email: [Email] - Skin: [Skin] - Blocked at: [Block timestamp]
					mailBody +=	" User ID: " 		+ user.getId() 								+
							  	" -Email: " 		+ user.getEmail() 							+ 
							  	" -Skin: " 		+ skinName									+
							  	" -Blocked at: " 	+ formatDate.format(user.getTimeBlocked()) 	+ " GMT \r\n ";;
				}
			}
				
			// send email
			CommonUtil.sendEmailMsg(emailFrom, emailTo, mailSubject, mailBody);
			return true;
		} catch (Exception e) {
			log.error("Error when running RestrictedBlockedUsersMailJob: ", e);
			return false;
		} finally {
			long endTime = System.currentTimeMillis();
			log.debug("Finished RestrictedBlockedUsersMailJob in:" + CommonUtil.getTimeFromMillis(endTime - beginTime));
		}
	}

    @Override
    public void stop() {
    	log.info("RestrictedBlockedUsersMailJob stopping...");
    }
}