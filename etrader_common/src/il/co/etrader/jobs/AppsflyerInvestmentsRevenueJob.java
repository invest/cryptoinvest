package il.co.etrader.jobs;

import il.co.etrader.util.JobUtil;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.log4j.Logger;

import com.anyoption.common.beans.AppsflyerEvent;
import com.anyoption.common.beans.AppsflyerTrade;
import com.anyoption.common.beans.ServerPixel;
import com.anyoption.common.daos.InvestmentsDAOBase;
import com.anyoption.common.daos.ServerPixelsDAOBase;
import com.anyoption.common.util.ConstantsBase;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class AppsflyerInvestmentsRevenueJob extends JobUtil {
	
	private static final Logger log = Logger.getLogger(AppsflyerInvestmentsRevenueJob.class);    
	
	public final static int RESPONSE_STATUS_SUCCESS = 200;
    
    public static void main(String[] args) throws SQLException, IOException {
    	log.debug("AppsflyerInvestmentsRevenueJob started");
    	Connection conn = null;
		try {
			propFile = args[0];
	        String job_interval = getPropertyByFile("job.interval");
	        String numberOfRecords = getPropertyByFile("number.of.records");
	        String users = getPropertyByFile("run.on.specific.user", ConstantsBase.EMPTY_STRING);
	        log.info("job_interval:" + job_interval + " numberOfRecords:" + numberOfRecords + " users:" + users);
			long beginTime = System.currentTimeMillis();
			log.debug("Try to get Users for appsflyer investments revenue event send.");
			conn = getConnection();
			conn.setAutoCommit(false);
			InvestmentsDAOBase.insertAppsflyerInvestmentHouseResult(conn, Integer.parseInt(job_interval), users);
			HashMap<Long, AppsflyerTrade> appsflyerEventsMap = new HashMap<Long, AppsflyerTrade>();
			appsflyerEventsMap = InvestmentsDAOBase.getAppsflyerInvestmentHouseResult(conn, Integer.parseInt(numberOfRecords));
			while (!appsflyerEventsMap.isEmpty()) {
				ArrayList<Long> idForUpdate = new ArrayList<Long>();
				Iterator<Entry<Long, AppsflyerTrade>> it = appsflyerEventsMap.entrySet().iterator();
				while (it.hasNext()) {
					String apiURL = getPropertyByFile("api.url");
					Map.Entry pair = (Map.Entry) it.next();					
		        	if (((AppsflyerTrade) pair.getValue()).getAppsflyerEvent().getOsTypeId() == 2) { // iPhone
		        		apiURL += "id";
		        	}				
					if (sendEvent(((AppsflyerTrade) pair.getValue()).getAppsflyerEvent(), apiURL)) {
						try {
							ServerPixelsDAOBase.insert(conn, new ServerPixel(ConstantsBase.SERVER_PIXEL_TYPE_TRADE_PROFIT, ((AppsflyerTrade) pair.getValue()).getUserId(), ServerPixel.SERVER_PIXELS_PUBLISHER_APPSFLYER));
							idForUpdate.add((Long) pair.getKey());
						} catch (Exception e) {
							log.error("cant insert to pixels server", e);
						}
					} else {
						log.debug("NOT FOUND Users for Appslyer investments revenue event sending.");
					}
				}
				if (!idForUpdate.isEmpty() || idForUpdate != null) {
					InvestmentsDAOBase.updateAppsflyerEventsStatus(conn, idForUpdate);
				}
				appsflyerEventsMap = InvestmentsDAOBase.getAppsflyerInvestmentHouseResult(conn, Integer.parseInt(numberOfRecords));
			}
			long endTime = System.currentTimeMillis();
			log.debug("Finish to inserting proccess System Highest Hit in:" + (endTime - beginTime) + "ms");
			conn.commit();
		} catch (Exception e) {
			log.error("When run AppsflyerInvestmentsRevenueJob", e);
		} finally {
			try {
				conn.setAutoCommit(true);
			} catch (SQLException e) {
				log.error("Can't setAutoCommit true.", e);
			}
    	}
		log.debug("AppsflyerInvestmentsRevenueJob finish");
	}
	
	private static boolean sendEvent(AppsflyerEvent appsflyerEvent, String apiUrl) {
		HttpClient httpclient = null;
        try {
        	httpclient = new DefaultHttpClient();
            HttpPost request = new HttpPost(apiUrl + appsflyerEvent.getAppId());
            request.addHeader("content-type", "application/json");
            request.addHeader("authentication", appsflyerEvent.getDevKey());
            Gson gson = new GsonBuilder().create();
            
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
            
            AppsflyerInvestmentsRevenueJob revenueJob = new AppsflyerInvestmentsRevenueJob();
            String json = gson.toJson(revenueJob.new Event(appsflyerEvent.getAppsflyerId(), appsflyerEvent.getIp(), "trade",
            		String.valueOf(appsflyerEvent.getAmount()), "USD", sdf.format(appsflyerEvent.getTimeCreated())));
            StringEntity params = new StringEntity(json);
            request.setEntity(params);
            
            log.debug("executing request " + request.getURI() + " json " + json + " dev key " +  appsflyerEvent.getDevKey());

            HttpResponse response = httpclient.execute(request);
            
            log.debug("--------------status--------------------------");
            log.debug(response.getStatusLine().toString());
            log.debug("----------------------------------------");
            
            if (response.getStatusLine().getStatusCode() == RESPONSE_STATUS_SUCCESS) {
            	return true;
            }
            log.debug("cant send appsflyer event response code: " + response.getStatusLine().getStatusCode());
            
		} catch (Exception e) {
			log.error("cant send appsflyer event", e);
        } finally {
            // When HttpClient instance is no longer needed,
            // shut down the connection manager to ensure
            // immediate deallocation of all system resources
            httpclient.getConnectionManager().shutdown();
        }
        return false;
    }
	
	private class Event {
		private String appsflyer_id;
		private String ip;
		private String eventName;
		private String eventValue;
		private String eventCurrency;
		private String eventTime;
		
		public Event(String appsflyer_id, String ip, String eventName,
				String eventValue, String eventCurrency, String eventTime) {
			super();
			this.appsflyer_id = appsflyer_id;
			this.ip = ip;
			this.eventName = eventName;
			this.eventValue = eventValue;
			this.eventCurrency = eventCurrency;
			this.eventTime = eventTime;
		}

		public String getAppsflyer_id() {
			return appsflyer_id;
		}

		public void setAppsflyer_id(String appsflyer_id) {
			this.appsflyer_id = appsflyer_id;
		}

		public String getIp() {
			return ip;
		}

		public void setIp(String ip) {
			this.ip = ip;
		}

		public String getEventName() {
			return eventName;
		}

		public void setEventName(String eventName) {
			this.eventName = eventName;
		}

		public String getEventValue() {
			return eventValue;
		}

		public void setEventValue(String eventValue) {
			this.eventValue = eventValue;
		}

		public String getEventCurrency() {
			return eventCurrency;
		}

		public void setEventCurrency(String eventCurrency) {
			this.eventCurrency = eventCurrency;
		}

		public String getEventTime() {
			return eventTime;
		}

		public void setEventTime(String eventTime) {
			this.eventTime = eventTime;
		}		
	}

}
