package il.co.etrader.jobs;

import il.co.etrader.bl_managers.BonusManagerBase;
import il.co.etrader.bl_managers.UsersManagerBase;
import il.co.etrader.bl_vos.UserBase;
import il.co.etrader.util.ConstantsBase;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import com.anyoption.common.beans.Bonus;
import com.anyoption.common.beans.Job;
import com.anyoption.common.beans.Writer;
import com.anyoption.common.beans.base.BonusUsers;
import com.anyoption.common.bl_vos.BonusCurrency;
import com.anyoption.common.jobs.ScheduledJob;
import com.anyoption.common.managers.BaseBLManager;

/**
 * @author eran.levy
 *
 */
public class BonusDepositJob extends BaseBLManager implements ScheduledJob {
	
	private static Logger log = Logger.getLogger(BonusDepositJob.class);
	private static String COMMENTS = "comments";
	private static String WAGERING = "wagering";
	private long wagering;
	private String comments;

	@Override
	public void setJobInfo(Job job) {
		if (null != job.getConfig() && !job.getConfig().trim().equals("")) {
            String[] ps = job.getConfig().split(",");
            for (int i = 0; i < ps.length; i++) {
                String[] p = ps[i].split("=");
                if (p[0].equals(COMMENTS)) {
                	comments = p[1].toString();                	
                } else if (p[0].equals(WAGERING)) {
                	wagering = Long.valueOf(p[1].toString());
                }
            }
		}
	}

	@Override
	public boolean run() {
		log.info("BonusDepositJob - process START.");
		Connection con = null;
		ArrayList<BonusDepositUser> list = getBonusDepositUsersList();
		for (BonusDepositUser bdu: list) {
			try {
				UserBase user = UsersManagerBase.getUserById(bdu.getUserId());
				// create bonusUser instance by amount and wagering
				BonusUsers bonusUser = new BonusUsers();
				bonusUser.setUserId(user.getId());
				bonusUser.setBonusId(ConstantsBase.BONUS_INSTANT_DYNAMIC_AMOUNT);
				bonusUser.setWriterId(Writer.WRITER_ID_AUTO);
				bonusUser.setComments(comments);
				con = getConnection();
				Bonus b = BonusManagerBase.getBonusById(con, bonusUser.getBonusId());
				b.setWageringParameter(wagering);
				BonusCurrency bc = new BonusCurrency();
				bc.setBonusAmount(bdu.getAmount());
				log.info("Going to grant bonus to user " + bdu.toString());
				boolean res = BonusManagerBase.insertBonusUser(bonusUser, user, Writer.WRITER_ID_AUTO, 
						con, bc, b, 0, 0, false);				
				log.info("Status granting bonus for user:" + res);
				if (res) {
					updateUsersGrantBonus(con, user.getId(), bonusUser.getId());
				}
			} catch (Exception e) {
				log.error("Error while granting bonus deposit", e);
			} finally {
				closeConnection(con);
			}
		}
		log.info("BonusDepositJob - process END.");
		return true;		
	}

	@Override
	public void stop() {		
	} 
	
	/**
	 * Get BonusDepositUsers List
	 * @return
	 */
	public ArrayList<BonusDepositUser> getBonusDepositUsersList() {
		ArrayList<BonusDepositUser> list = new ArrayList<BonusDepositUser>();
		BonusDepositJob bonusDepositJob = new BonusDepositJob();		
		Connection con = null;
		ResultSet rs = null;
		PreparedStatement ps = null;
		try {
			con = getConnection();
			String sql =
				" SELECT" +
				"	* " +
				" FROM" +
				"	users_grant_bonus " +
				" WHERE " +
				"	is_grant_bonus = 0 "  +
				"	AND type = 2 ";

			ps = con.prepareStatement(sql);
			rs = ps.executeQuery();
			while (rs.next()) {
				BonusDepositUser bdu = bonusDepositJob.new BonusDepositUser();
				bdu.setUserId(rs.getLong("user_id"));
				bdu.setAmount(rs.getLong("amount"));
				list.add(bdu);
			}
		} catch (Exception e) {
			log.error("Error while trying to get user ", e);
		} finally {
	    	try {
				ps.close();
			} catch (Exception e) {
				log.log(Level.ERROR,"Can't close",e);
			}
	    	try {
	    		con.close();
			} catch (Exception e) {
				log.log(Level.ERROR,"Can't close",e);
			}		
		}				
		return list;
	}
	
    public static void updateUsersGrantBonus(Connection conn, long userId, long bonusUserId) {
		PreparedStatement ps = null;
    	try {
    		String sql = " UPDATE " +
    					 " 	users_grant_bonus " +
    					 " SET " +
    					 " 	is_grant_bonus = 1, " +
    					 " 	bonus_user_id = ? " +
    					 " WHERE " +
    					 "	user_id = ? " +
    					 " 	AND bonus_user_id = 0 " +
    					 " 	AND type = 2 ";
    		ps = conn.prepareStatement(sql);
    		ps.setLong(1, bonusUserId);
			ps.setLong(2, userId);
    		ps.executeUpdate();
    	} catch(SQLException se){
	    	log.fatal("Exception during performing action : ", se);
	    	se.printStackTrace();
	    } catch (Exception e) {
	        log.fatal("Exception during performing action :", e);
	        e.printStackTrace();
	    } finally {
	    	try {
				ps.close();
			} catch (Exception e) {
				log.log(Level.ERROR,"Can't close",e);
			}
		}
    } 
	
	class BonusDepositUser {
		private long userId;
		private long amount;
		/**
		 * @return the userId
		 */
		public long getUserId() {
			return userId;
		}
		/**
		 * @param userId the userId to set
		 */
		public void setUserId(long userId) {
			this.userId = userId;
		}
		/**
		 * @return the amount
		 */
		public long getAmount() {
			return amount;
		}
		/**
		 * @param amount the amount to set
		 */
		public void setAmount(long amount) {
			this.amount = amount;
		}
		/* (non-Javadoc)
		 * @see java.lang.Object#toString()
		 */
		@Override
		public String toString() {
			return "BonusDepositUser [userId=" + userId + ", amount=" + amount
					+ "]";
		}		
	}
}