package il.co.etrader.jobs;

import org.apache.log4j.Logger;

import com.anyoption.common.jobs.ScheduledJob;

public class CustomerReportsSendMailJobRunnable implements Runnable {
	private static final Logger log = Logger.getLogger(CustomerReportsSendMailJobRunnable.class);  
	private Thread t;

	public void run() {
		log.debug("Running CustomerReport");
		 ScheduledJob currentJob;
		try {
			Class<?> cl = Class.forName("il.co.etrader.jobs.CustomerReportsSendMailJob");
            currentJob = (ScheduledJob) cl.newInstance();
            log.info("start job CustomerReportsSendMailJob ") ;
            long jobStartTime = System.currentTimeMillis();
            currentJob.run();
            currentJob = null;
            log.info("end job CustomerReportsSendMailJob running time: " + (System.currentTimeMillis() - jobStartTime));
		} catch (Exception e) {
			log.error("CalculateCustomerReportd Error", e);
		}
	}

	public void start() {
		System.out.println("Starting CustomerReport Thread");
		if (t == null) {
			t = new Thread(this);
			t.start();
		}
	}
}
