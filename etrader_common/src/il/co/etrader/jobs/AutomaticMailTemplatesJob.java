package il.co.etrader.jobs;

import il.co.etrader.bl_managers.IssuesManagerBase;
import il.co.etrader.bl_managers.JobsManagerBase;
import il.co.etrader.bl_managers.UsersManagerBase;
import il.co.etrader.bl_vos.UserBase;
import il.co.etrader.bl_vos.Writer;

import java.util.List;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.Job;
import com.anyoption.common.jobs.ScheduledJob;

public class AutomaticMailTemplatesJob implements ScheduledJob {
    private static final Logger log = Logger.getLogger(AutomaticMailTemplatesJob.class);
    
    private boolean running;

    public void setJobInfo(Job job) {
        // do nothing
    }
    
    public boolean run() {
        running = true;
        if (running) {
            sendMaintenanceFeeMonth();
        }
        if (running) {
            sendMaintenanceFeeWeek();
        }
        if (running) {
            collectMaintenanceFee();
        }
        return running;
    }
    
    private void sendDocumentRequestMailTemplate(List<UserBase> usersList, long templateId, long writerId, long issueSubjectId, String issueActionComment) {
        for (UserBase u : usersList) {
            try {
                if (!running) {
                    return;
                }
                UsersManagerBase.sendMailTemplateFile(templateId, writerId, u, false, null, null, null, null, null, null, null, true, null, null, null);
                IssuesManagerBase.insertIssueForMail(u.getId(), issueSubjectId, u.getIsContactByEmail() == 1, issueActionComment, 0);
            } catch (Exception e) {
                log.error("Can't send template: " + templateId + " to user: " + u, e);
            }
        }
    }

    private void sendMaintenanceFeeMonth() {
        try {
            List<UserBase> usersList = JobsManagerBase.getUsersListForMaintenanceFee(28, "MAINTENANCE_FEE_FIRST_EMAIL"); // According to spec a month is 28 days
            sendDocumentRequestMailTemplate(usersList, UsersManagerBase.TEMPLATE_MAINTENANCE_FEE, Writer.WRITER_ID_AUTO, IssuesManagerBase.ISSUE_SUBJECT_MAINTENANCE_FEE_EMAIL1, "One month to maintenance fee notification");
        } catch (Exception e) {
            log.error("Can't send template.", e);
        }
    }

    private void sendMaintenanceFeeWeek() {
        try {
            List<UserBase> usersList = JobsManagerBase.getUsersListForMaintenanceFee(7, "MAINTENANCE_FEE_SECOND_EMAIL");
            sendDocumentRequestMailTemplate(usersList, UsersManagerBase.TEMPLATE_MAINTENANCE_FEE, Writer.WRITER_ID_AUTO, IssuesManagerBase.ISSUE_SUBJECT_MAINTENANCE_FEE_EMAIL2, "One week to maintenance fee notification");
        } catch (Exception e) {
            log.error("Can't send template.", e);
        }
    }
    
    private void collectMaintenanceFee() {
        try {
            JobsManagerBase.collectMaintenanceFee();
        } catch (Exception e) {
            log.error("Can't collect maintenance fee.", e);
        }
    }
    
    public void stop() {
        log.info("AutomaticMailTemplatesJob stopping...");
        running = false;
    }
}