package il.co.etrader.jobs;

import java.util.ArrayList;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.Job;
import com.anyoption.common.jobs.ScheduledJob;

import il.co.etrader.bl_managers.RiskAlertsManagerBase;
import il.co.etrader.bl_managers.UsersManagerBase;
import il.co.etrader.bl_vos.Writer;

/**
 * @author radostinab
 *
 */
public class SingleLoginAlertJob implements ScheduledJob {
	private static Logger log = Logger.getLogger(SingleLoginAlertJob.class);
	@Override
	public void setJobInfo(Job job) {

	}

	@Override
	public boolean run() {
		log.debug("SingleLoginAlertJob - process START.");
		try {
			ArrayList<Long> list = UsersManagerBase.getSingleLoginUsers();
			for (Long transaction : list) {
				RiskAlertsManagerBase.insert(RiskAlertsManagerBase.RISK_TYPE_SINGLE_LOGIN, Writer.WRITER_ID_WEB,
						transaction, 0);
			}
			if (list.size() == 0) {
				log.info("No users with a single login found.");
			}
		} catch (Exception e) {
			log.error("Error, Problem with SingleLoginAlertJob.", e);
		}
		log.info("SingleLoginAlertJob - process END.");
		return true;
	}

	@Override
	public void stop() {
		log.debug("SingleLoginAlertJob - STOP.");
	}
}
