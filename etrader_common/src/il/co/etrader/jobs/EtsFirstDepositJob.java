package il.co.etrader.jobs;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.Job;
import com.anyoption.common.jobs.ScheduledJob;
import com.anyoption.common.util.MarketingETS;

public class EtsFirstDepositJob implements ScheduledJob {
    private static final Logger log = Logger.getLogger(EtsFirstDepositJob.class);

    private void sendEtsFirstDeposits() {
        try {
            MarketingETS.etsUsersFirstDeposit();
        } catch (Exception e) {
            log.error("Can't send First Deposit to ETS server ", e);
        }
    }

    public void setJobInfo(Job job) {
        // do nothing
    }

    public boolean run() {
        sendEtsFirstDeposits();
        return true;
    }

    public void stop() {
        log.info("EtsFirstDepositJob stopping...");
    }
}