package il.co.etrader.jobs;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Hashtable;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.Job;
import com.anyoption.common.bl_vos.PopulationEntryBase;
import com.anyoption.common.jobs.ScheduledJob;
import com.anyoption.common.util.Utils;

import il.co.etrader.bl_managers.PopulationsManagerBase;
import il.co.etrader.bl_vos.Writer;
import il.co.etrader.population.PopulationHandlersException;
import il.co.etrader.util.CommonUtil;

/**
 * Auto update population job
 * @author EyalG
 */
public class ExitFromPortfolioJob implements ScheduledJob {
	private static Logger log = Logger.getLogger(ExitFromPortfolioJob.class);
	private String emailTo;
	private long batch;
	
	@Override
	public void setJobInfo(Job job) {
		if (null != job.getConfig() && !job.getConfig().trim().equals("")) {
			String[] arr = job.getConfig().split(",");
			Hashtable<String, String> map = new Hashtable<>();
			for (String el : arr) {
				String[] el1 = el.split("=");
				map.put(el1[0], el1[1]);
			}

			batch = Long.valueOf(map.get("batch"));
			emailTo = map.get("email_to");
		}
	}

	@Override
	public boolean run() {
		log.debug("ExitFromPortfolioJob - process START. with batch = " + batch);
		long startTime = System.currentTimeMillis();
		StringBuffer report = new StringBuffer("<br><table border=\"1\">");
		report.append("<tr><td>User id</td><td>population User Id</td><td>assign Writer Id</td><td>curr Popualtion Type Id</td><td>Population entry id</td></tr>");
		ArrayList<PopulationEntryBase> usersToUnassign;
		boolean isUsersToUnassign = true;
		long min = 0;
		long max = batch;
		long allUsersCount = 0;
		try {
			
			while (isUsersToUnassign) {
				
				usersToUnassign = PopulationsManagerBase.getPopulationEntryTmp(min, max);
				min += batch;
				max += batch;
				allUsersCount += usersToUnassign.size();
				
				for (PopulationEntryBase populationEntryBase : usersToUnassign) {
					try {
						PopulationsManagerBase.cancelAssign(populationEntryBase, Writer.WRITER_ID_AUTO, populationEntryBase.getCurrEntryId());
						report.append("<tr><td>" + populationEntryBase.getUserId() + "</td><td>"
												 + populationEntryBase.getPopulationUserId() + "</td><td>"
												 + populationEntryBase.getAssignWriterId() + "</td><td>"
												 + populationEntryBase.getCurrPopualtionTypeId() + "</td><td>"
												 + populationEntryBase.getCurrEntryId() + "</td></tr>");
					} catch (PopulationHandlersException e) {
						log.error("cant Unassign user " + populationEntryBase, e);
					}
				}
				if (usersToUnassign.isEmpty()) {
					isUsersToUnassign = false;
				}
			}
		} catch (SQLException e) {
			log.error("cant Unassign users", e);
		}
		report.append("</table>");
		
		long endTime = System.currentTimeMillis();
		String reportBody = "<html><body> <b>Exit From Portfolio Job:</b><br/><b>job run at: </b> " + new Date() + "<br/><b>Execution time:</b> " + getExecutionTime(endTime - startTime) + 
				"<br/><b>Count of users:</b> " + allUsersCount + "<br/>" + report.toString() + "</body></html>";
		
		Hashtable<String, String> serverProperties = new Hashtable<String, String>();
		serverProperties.put("url",Utils.getProperty("email.server"));
		serverProperties.put("auth","true");
		serverProperties.put("user",Utils.getProperty("email.uname"));
		serverProperties.put("pass",Utils.getProperty("email.pass"));

        Hashtable<String, String> email = new Hashtable<String, String>();
        email.put("subject", "Exit From Portfolio Job report");
	    email.put("to", emailTo); 
	    email.put("from", "support@anyoption.com"); 	    
        email.put("body", reportBody);
		CommonUtil.sendEmail(serverProperties, email, null);
		
		log.debug("ExitFromPortfolioJob finished with total users: " + allUsersCount);
		log.debug("ExitFromPortfolioJob - process END.");
		return true;
	}

	@Override
	public void stop() {
		log.info("ExitFromPortfolioJob - STOP.");
	}
	
	private String getExecutionTime(long time) {
		return String.format("%02d min, %02d sec", TimeUnit.MILLISECONDS.toMinutes(time), Math.abs(TimeUnit.MILLISECONDS.toSeconds(time) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(time))));
	}
}
