package il.co.etrader.jobs;

import java.sql.SQLException;
import java.util.ArrayList;

import il.co.etrader.bl_managers.PopulationsManagerBase;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.Job;
import com.anyoption.common.bl_vos.PopulationEntryBase;
import com.anyoption.common.jobs.ScheduledJob;

public class ModifyOpenWithdrawToRetentionJob implements ScheduledJob {
	private static Logger log = Logger.getLogger(ModifyOpenWithdrawToRetentionJob.class);

	@Override
	public void setJobInfo(Job job) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean run() {
		log.info("ModifyOpenWithdrawToRetentionJob - process START.");
		try {
			ArrayList<Long> userIdList = PopulationsManagerBase.getSpecialOpenWithdrawUsers();
			for (Long userId : userIdList) {
				PopulationEntryBase pe = PopulationsManagerBase.getPopulationUserByUserId(userId);
				if(pe != null && pe.getCurrPopualtionTypeId() == PopulationsManagerBase.POP_TYPE_OPEN_WITHDRAW &&
						pe.getEntryTypeId() != PopulationsManagerBase.ENTRY_TYPE_REMOVE_PERMANENTLY) {
					PopulationsManagerBase.insertIntoPopulation(0, 0, userId, 
							pe.getCurrPopulationName(), pe.getAssignWriterId(), PopulationsManagerBase.POP_TYPE_RETENTION, pe);
				}
			}
		} catch (SQLException e) {
			log.error("Problem with ModifyOpenWithdrawToRetentionJob", e);
		}
		log.info("ModifyOpenWithdrawToRetentionJob - process FINISH.");
		return true;
	}

	@Override
	public void stop() {
		log.info("ModifyOpenWithdrawToRetentionJob - process STOP.");
	}
	
}
