package il.co.etrader.jobs;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.UnsupportedEncodingException;
import java.sql.SQLException;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.Job;
import com.anyoption.common.jobs.ScheduledJob;
import com.anyoption.common.managers.TxtLocalManager;
import com.anyoption.external.txtlocal.TxtLocalClient;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.SftpException;

public class TxtLocalSynchJob  implements ScheduledJob {
	private static final Logger log = Logger.getLogger(TxtLocalSynchJob.class);
	
    private String pathToUpload	= null;
    private String tempFolder	= null;
    private String ftpUrl		= null;
    private String ftpUserName	= null;
    private String ftpPasswrod	= null;
    private String groupId		= null;
    
	@Override
	public void setJobInfo(Job job) {
		if (null != job.getConfig() && !job.getConfig().trim().equals("")) {
            String[] ps = job.getConfig().split(",");
            for (int i = 0; i < ps.length; i++) {
                String[] p = ps[i].split("=");
                if (p[0].equals("pathToUpload")) {
                	pathToUpload = p[1];
                } else if (p[0].equals("ftpUrl")) {
                    ftpUrl = p[1];
                } else if (p[0].equals("ftpUserName")) {
                    ftpUserName = p[1];
                } else if (p[0].equals("ftpPasswrod")) {
                    ftpPasswrod = p[1];
                } else if(p[0].equals("groupId")) {
                	groupId = p[1];
                } else if(p[0].equals("tempFolder")) {
                	tempFolder = p[1];
                }
            }
		}
	}

	@Override
	public boolean run() {
		try {
			String file = TxtLocalManager.exportUsers(tempFolder, groupId);
			TxtLocalClient txtlocalClient = new TxtLocalClient(ftpUrl, ftpUserName, ftpPasswrod, pathToUpload, new File(file));
			txtlocalClient.upload();
			return true;
		} catch (UnsupportedEncodingException e) {
			log.error("Can't write to file", e);
		} catch (FileNotFoundException e) {
			log.error("File not found", e);
		} catch (SQLException e) {
			log.error("SQL problem", e);
		} catch (JSchException e) {
			log.error("Can't connect to sftp", e);
		} catch (SftpException e) {
			log.error("Can't upload", e);
		}

		return false;
	}

	@Override
	public void stop() {
		// TODO Auto-generated method stub
		
	}
}
