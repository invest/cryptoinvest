package il.co.etrader.jobs;

import java.util.HashMap;
import java.util.concurrent.TimeUnit;

import org.apache.commons.collections.keyvalue.MultiKey;
import org.apache.log4j.Logger;

import com.anyoption.common.beans.EpgTransactionStatus;
import com.anyoption.common.beans.Job;
import com.anyoption.common.encryption.SecureAlgorithms;
import com.anyoption.common.jobs.ScheduledJob;
import com.anyoption.common.managers.TransactionsManagerBase;
import com.anyoption.common.util.ClearingUtil;


/**
 * check epg transaction status job
 * @author EyalG
 */
public class EpgTransactionsStatusJob implements ScheduledJob {
	private static Logger log = Logger.getLogger(EpgTransactionsStatusJob.class);
	private String statusUrl;
	
	@Override
	public void setJobInfo(Job job) {
		if (null != job.getConfig() && !job.getConfig().trim().equals("")) {
            String[] ps = job.getConfig().split(",");
            for (int i = 0; i < ps.length; i++) {
                String[] p = ps[i].split("=");
                if (p[0].equals("statusUrl")) {
                	statusUrl = p[1].toString();                	
                }
            }
		}
	}

	@Override
	public boolean run() {
		long startTime = System.currentTimeMillis();
		log.info("ExitFromPortfolioJob - process START. with status Url = " + statusUrl + " start time " + startTime);
		HashMap<MultiKey, EpgTransactionStatus> transactionsMap = new HashMap<MultiKey, EpgTransactionStatus>();
		try {
			transactionsMap = TransactionsManagerBase.getEpgTransactionsStatus();
			transactionsMap.values().parallelStream().forEach((transaction) -> {
				try {
					String body = transaction.getEpgRequestParams();
					log.debug("send request to epg url " + statusUrl + " with params " + body);
					ClearingUtil.executePOSTRequest(statusUrl, body);
				} catch (Exception e) {
					log.error("cant check status for epg transactions getMarchent Id = " + transaction.getMarchentId() + " password = " + transaction.getPassword());
				}
			});
//			for (ArrayList<EpgTransactionStatus> transactionList : transactionsMap.values()) {
//				try {
//					PopulationsManagerBase.cancelAssign(populationEntryBase, Writer.WRITER_ID_AUTO, populationEntryBase.getCurrEntryId());
//					report.append("<tr><td>" + populationEntryBase.getUserId() + "</td><td>" + populationEntryBase.getPopulationUserId()  + "</td><td>" + populationEntryBase.getAssignWriterId() + "</td><td>" + populationEntryBase.getCurrPopualtionTypeId() + "</td><td>" + populationEntryBase.getCurrEntryId() + "</td></tr>");
//				} catch (PopulationHandlersException e) {
//					log.error("cant Unassign user " + populationEntryBase, e);
//				}
//			}
		} catch (Exception e) {
			log.error("cant Unassign users", e);
		}
		
		long endTime = System.currentTimeMillis();
		log.info("ExitFromPortfolioJob - process END. end time " + endTime);
		return true;
	}

	@Override
	public void stop() {
		log.info("ExitFromPortfolioJob - STOP.");
	}
	
	private String getExecutionTime(long time) {
		return String.format("%02d min, %02d sec", 
				    TimeUnit.MILLISECONDS.toMinutes(time),
				    Math.abs(TimeUnit.MILLISECONDS.toSeconds(time) - 
				    TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(time)))
				);
	}
}
