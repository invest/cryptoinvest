//package il.co.etrader.jobs.copyop;
//
//
//import java.util.ArrayList;
//
//import org.apache.log4j.Logger;
//
//import com.anyoption.common.beans.Job;
//import com.anyoption.common.jobs.ScheduledJob;
//import com.anyoption.common.managers.BestHotDataManager;
//import com.copyop.common.dto.base.FeedMessage;
//import com.copyop.common.jms.CopyOpEventSender;
//import com.copyop.common.jms.updates.UpdateMessage;
//import com.copyop.common.managers.ConfigurationCopyopManager;
//
//public class CopyopSystemHighestHitJob implements ScheduledJob {
//	//The post will be generated at: 1:00, 4:00, 7:00, 10:00, 13:00, 16:00, 19:00 , 22:00 
//    private static final Logger log = Logger.getLogger(CopyopSystemHighestHitJob.class);    
//    
//    private void insertSystemHighestHit() {
//        try {
//        	ArrayList<FeedMessage> feedList = BestHotDataManager.getSystemHighestHit();        	  	
//        	int i = 1;
//        	
//        	for(FeedMessage fm: feedList){
//        		UpdateMessage updateMsg = new UpdateMessage(fm);
//            	int delay = new Integer(ConfigurationCopyopManager.getCopyopPropertiesValue("SYSTEM_HIGHEST_HIT_DELAY_MSG_" + i, "0"));
//        		CopyOpEventSender.sendUpdate(updateMsg, CopyOpEventSender.USER_DELAY_SYSTEM_MSG, delay, 0);
//        		i ++;
//        		log.debug("Sended [" + updateMsg + "] MSG with delay:" + delay);
//        	}
//        	log.debug("***********SEND " + feedList.size() + " MSG*************");
//        	
//        } catch (Exception e) {
//            log.error("Can't insert System Highest Hit", e);
//        }
//    }
//
//    public void setJobInfo(Job job) {
//        // do nothing
//    }
//
//    public boolean run() {
//    	try {
//    		long beginTime = System.currentTimeMillis();
//        	log.debug("=====================BEGIN COPYOP SYSTEM 16 Highest Hit JOB=====================");
//        	log.debug("Try to insert System Highest Hit...");
//        	insertSystemHighestHit();
//        	log.debug("Finish to inserting proccess System Highest Hit");      
//        	long endTime = System.currentTimeMillis();
//        	log.debug("=====================END COPYOP SYSTEM 16 Highest Hit JOB in:" + (endTime - beginTime) + "ms =====================");
//            return true;
//			
//		} catch (Exception e) {
//			log.error("When run COPYOP SYSTEM Highest Hit Job",e);
//			return false;
//		}
//    }
//
//    public void stop() {
//        log.info("CopyopSystemHighestHitJob stopping...");
//    }
//}