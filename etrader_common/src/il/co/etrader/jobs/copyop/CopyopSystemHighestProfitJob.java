//package il.co.etrader.jobs.copyop;
//
//
//import java.util.ArrayList;
//
//import org.apache.log4j.Logger;
//
//import com.anyoption.common.beans.Job;
//import com.anyoption.common.jobs.ScheduledJob;
//import com.anyoption.common.managers.BestHotDataManager;
//import com.copyop.common.dto.base.FeedMessage;
//import com.copyop.common.jms.CopyOpEventSender;
//import com.copyop.common.jms.updates.UpdateMessage;
//import com.copyop.common.managers.ConfigurationCopyopManager;
//
//public class CopyopSystemHighestProfitJob implements ScheduledJob {
//	//The post will be generated at: 2:00, 5,00, 8:00, 11:00, 14:00, 17:00, 20:00 , 23:00 
//    private static final Logger log = Logger.getLogger(CopyopSystemHighestProfitJob.class);    
//    
//    private void insertSystemHighestProfit() {
//        try {
//        	ArrayList<FeedMessage> feedList = BestHotDataManager.getSystemHighestProfit(3);        	        	
//        	int i = 1;
//        	
//        	for(FeedMessage fm: feedList){
//        		UpdateMessage updateMsg = new UpdateMessage(fm);
//            	int delay = new Integer(ConfigurationCopyopManager.getCopyopPropertiesValue("SYSTEM_HIGHEST_PROFIT_DELAY_MSG_" + i, "0"));
//        		CopyOpEventSender.sendUpdate(updateMsg, CopyOpEventSender.USER_DELAY_SYSTEM_MSG, delay, 0);
//        		i ++;
//        		log.debug("Sended [" + updateMsg + "] MSG with delay:" + delay);
//        	}
//        	log.debug("***********SEND " + feedList.size() + " MSG*************");
//        } catch (Exception e) {
//            log.error("Can't insert Highest Profit", e);
//        }
//    }
//
//    public void setJobInfo(Job job) {
//        // do nothing
//    }
//
//    public boolean run() {
//    	try {
//    		long beginTime = System.currentTimeMillis();
//        	log.debug("=====================BEGIN COPYOP SYSTEM 17 Highest Profit JOB=====================");       	
//        	log.debug("Try to insert System Highest Profit...");
//        	insertSystemHighestProfit();
//        	log.debug("Finish to inserting proccess System Highest Profit");  
//        	long endTime = System.currentTimeMillis();
//        	log.debug("=====================END COPYOP SYSTEM 17 Highest Profit JOB in:" + (endTime - beginTime) + "ms =====================");
//            return true;
//			
//		} catch (Exception e) {
//			log.error("When run COPYOP SYSTEM Highest Profit Job",e);
//			return false;
//		}
//    }
//
//    public void stop() {
//        log.info("CopyopSystemHighestProfitJob stopping...");
//    }
//}