package il.co.etrader.jobs.copyop;


import java.util.ArrayList;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.Job;
import com.anyoption.common.jobs.ScheduledJob;
import com.copyop.common.dto.Frozen;
import com.copyop.common.enums.base.ProfileLinkCommandEnum;
import com.copyop.common.jms.CopyOpEventSender;
import com.copyop.common.jms.events.ProfileManageEvent;
import com.copyop.common.managers.FrozenCopyopManager;
import com.copyop.common.managers.ProfileManager;

public class CopyopFrozenJob implements ScheduledJob {
	//Every 1h
    private static final Logger log = Logger.getLogger(CopyopFrozenJob.class);   

    private void freshOffUsers() {
        try {
        	log.debug("Get Fresh/Off Users");
        	ArrayList<Frozen> freshUsers = FrozenCopyopManager.getFreshOffUsers();
        	
        	log.debug("Set Fresh/Off Users:" + freshUsers);
        	FrozenCopyopManager.update(freshUsers);
        	
        	log.debug("Set isFrozen = false in profile");
        	ProfileManager.updateFrozenTrader(freshUsers);
        } catch (Exception e) {
            log.error("When get Fresh user frozen ", e);
        }
    }
    
    private void brokeUsers() {
        try {
        	log.debug("Get Broke Users");
        	ArrayList<Frozen> brokeUsers = FrozenCopyopManager.getBrokeFrozen();
        	if(brokeUsers.size() > 0){
            	log.debug("Set Frozen Broke  Users:" + brokeUsers);
            	FrozenCopyopManager.update(brokeUsers);
            	
            	log.debug("Set isFrozen = true in profile");
            	ProfileManager.updateFrozenTrader(brokeUsers);
            	
            	log.debug("Isert into temp frozen table");
            	ProfileManager.insertFrozenTraderTemp(brokeUsers);
        	} else {
        		log.debug("The Broke Users list size is:" + brokeUsers.size());
        	}
        	
        } catch (Exception e) {
            log.error("When broke users", e);
        }
    }
    
    private void dormantUsers() {
        try {
        	log.debug("Get Dormant Users");
        	ArrayList<Frozen> dormantUsers = FrozenCopyopManager.getDormantFrozen();
        	if(dormantUsers.size() > 0){
            	log.debug("Set Dormant Users:" + dormantUsers);
            	FrozenCopyopManager.update(dormantUsers);
            	
            	log.debug("Set isFrozen = true in profile");
            	ProfileManager.updateFrozenTrader(dormantUsers);
            	
            	log.debug("Isert into temp frozen table");
            	ProfileManager.insertFrozenTraderTemp(dormantUsers);
        	} else {
        		log.debug("The Dormant Users list size is:" + dormantUsers.size());
        	}
        } catch (Exception e) {
            log.error("When Dormant users", e);
        }
    }
    
    private void copyopServiceNotif() {
        try {
        	ProfileManageEvent linkManageEvent = new ProfileManageEvent(ProfileLinkCommandEnum.FREEZE);
        	CopyOpEventSender.sendEvent(linkManageEvent, CopyOpEventSender.PROFILE_MANAGE_DESTINATION_NAME);
        } catch (Exception e) {
            log.error("When Dormant users", e);
        }
    }

    public void setJobInfo(Job job) {
        // do nothing
    }

    public boolean run() {
    	try {    	
    		long beginTime = System.currentTimeMillis();
    		
        	log.debug("=====================BEGIN FROZEN JOB=====================");       	
        	log.debug("Try to Check Fresh/Off Frozen staus for new users...");
        	freshOffUsers();
        	log.debug("Finish Check Fresh/Off Frozen staus for new users");
        	
        	log.debug("Try to Check Broke Frozen...");
        	brokeUsers();
        	log.debug("Finish to Broke Frozen");
        	
        	log.debug("Try to Check Dormant Frozen...");
        	dormantUsers();
        	log.debug("Finish to Dormant Frozen");        	
        	
        	log.debug("Notife to Copyop Service...");
        	copyopServiceNotif();
        	
        	long endTime = System.currentTimeMillis();
        	log.debug("=====================END FROZEN JOB in:" + (endTime - beginTime) + "ms =====================");        	
        	
            return true;
			
		} catch (Exception e) {
			log.error("When run Copyop Hot Job",e);
			return false;
		}
    }

    public void stop() {
        log.info("CopyopHotTableJob stopping...");
    }
}