package il.co.etrader.jobs.copyop;

import org.apache.log4j.Logger;

import com.copyop.common.managers.ProfileManager;
import com.copyop.common.managers.RiskAppetiteManager;

public class CopyopCalculateRiskAppetiteRunnable implements Runnable {
	private static final Logger log = Logger.getLogger(CopyopCalculateRiskAppetiteRunnable.class);  
	private Thread t;
	private String userId;

	CopyopCalculateRiskAppetiteRunnable(String userId) {
		this.userId = userId;
	}

	public void run() {
		log.debug("Running calculate risk appetite for userId:" + userId);
		long beginTime = System.currentTimeMillis();
		try {
			long user = new Long(userId);
			float riskAppetite = RiskAppetiteManager.calculateUserRiskAppetite(user);			
			ProfileManager.insertRiskAppetite(user, riskAppetite);
			RiskAppetiteManager.updateRiskAppetite(riskAppetite, user);
		} catch (Exception e) {
			log.error("Calculate risk appetite for userId " + userId + " interrupted. Error", e);
		}
		long endTime = System.currentTimeMillis();
		System.out.println("Calculate risk appetite for userId " + userId + " finished in:" + (endTime - beginTime) + "ms");
	}

	public void start() {
		System.out.println("Starting " + userId);
		if (t == null) {
			t = new Thread(this, userId);
			t.start();
		}
	}
}
