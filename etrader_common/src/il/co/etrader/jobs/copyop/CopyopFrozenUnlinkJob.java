package il.co.etrader.jobs.copyop;


import org.apache.log4j.Logger;

import com.anyoption.common.beans.Job;
import com.anyoption.common.jobs.ScheduledJob;
import com.copyop.common.managers.FrozenCopyopManager;

public class CopyopFrozenUnlinkJob implements ScheduledJob {
	    private static final Logger log = Logger.getLogger(CopyopFrozenUnlinkJob.class);   


    public void setJobInfo(Job job) {
        // do nothing
    }

    public boolean run() {
    	try {    	
    		long beginTime = System.currentTimeMillis();
    		
        	log.debug("=====================BEGIN UNLINK FROZEN JOB=====================");       	
        	FrozenCopyopManager.unlinkFrozenUsers();
        	long endTime = System.currentTimeMillis();
        	log.debug("=====================END FROZEN JOB in:" + (endTime - beginTime) + "ms =====================");        	
        	
            return true;
		} catch (Exception e) {
			log.error("When run Copyop Frozen Unlink Job",e);
			return false;
		}
    }

    public void stop() {
        log.info("Copyop Frozen Unlink Job stopping...");
    }
}