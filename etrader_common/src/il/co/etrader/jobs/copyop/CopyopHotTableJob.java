//package il.co.etrader.jobs.copyop;
//
//
//import org.apache.log4j.Logger;
//
//import com.anyoption.common.beans.Job;
//import com.anyoption.common.jobs.ScheduledJob;
//import com.anyoption.common.managers.BestHotDataManager;
//import com.copyop.common.managers.FeedManager;
//
//public class CopyopHotTableJob implements ScheduledJob {
//
//    private static final Logger log = Logger.getLogger(CopyopHotTableJob.class);
//    
//    private static final String BEST_HOT_TRADERS = 		"BEST_HOT_TRADERS";
//    private static final String BEST_HOT_TRADES = 		"BEST_HOT_TRADES";
//    private static final String BEST_HOT_COPIERS = 		"BEST_HOT_COPIERS";
//    private static final String BEST_HOT_ASSET_SPECIALISTS = 	"BEST_HOT_ASSET_SPECIALISTS";
//    
//    private String cfgBestHotType;
//    
//    private void insertHotBestAssetSpecialists() {
//	try {
//	    FeedManager.insertFeedMessage(BestHotDataManager.getHotAssetSpecialists());
//	} catch (Exception e) {
//	    log.error("Can't insert Asset Specialists ", e);
//	}
//    }
//
//    private void insertHotBestTraders() {
//        try {
//        	FeedManager.insertFeedMessage(BestHotDataManager.getHotBestTraders());
//        } catch (Exception e) {
//            log.error("Can't insert Best Traders", e);
//        }
//    }
//    
//    private void insertHotBestTrades() {
//        try {
//        	FeedManager.insertFeedMessage(BestHotDataManager.getHotBestTrades());
//        } catch (Exception e) {
//            log.error("Can't insert Best Trades", e);
//        }
//    }
//    
//    private void insertHotBestCopiers() {
//        try {
//        	FeedManager.insertFeedMessage(BestHotDataManager.getHotBestCopiers());
//        } catch (Exception e) {
//            log.error("Can't insert Best Copiers", e);
//        }
//    }
//
//    @Override
//    public void setJobInfo(Job job) {
//    	if (null != job.getConfig()) {
//    		cfgBestHotType = job.getConfig();
//    	}
//    }
//
//    @Override
//    public boolean run() {
//    	try {
//    		long beginTime = System.currentTimeMillis();
//        	log.debug("=====================BEGIN COPYOP *" + cfgBestHotType + "* 6x JOB=====================");
//        	
//        	if (cfgBestHotType.contains(BEST_HOT_ASSET_SPECIALISTS)) {
//            	log.debug("Trying to insert Hot Asset Specialists...");
//            	insertHotBestAssetSpecialists();
//            	log.debug("Finished inserting proccess Asset Specialists");
//            	}
//        	
//        	if (cfgBestHotType.contains(BEST_HOT_TRADERS)) {
//            	log.debug("Trying to insert Hot Best Traders...");
//            	insertHotBestTraders();
//            	log.debug("Finished inserting proccess Hot Best Traders");
//        	}
//        	
//        	if (cfgBestHotType.contains(BEST_HOT_TRADES)) {
//            	log.debug("Trying to insert Hot Best Trades...");
//            	insertHotBestTrades();
//            	log.debug("Finished inserting proccess Hot Best Trades");
//        	}
//        	
//        	if (cfgBestHotType.contains(BEST_HOT_COPIERS)) {
//            	log.debug("Trying to insert Hot Best Copiers...");
//            	insertHotBestCopiers();
//            	log.debug("Finished inserting proccess Hot Best Copiers");
//        	}
//        	
//        	long endTime = System.currentTimeMillis();
//        	log.debug("=====================END COPYOP *" + cfgBestHotType + "* 6x JOB in:" + (endTime - beginTime) + "ms =====================");
//            return true;
//			
//		} catch (Exception e) {
//			log.error("When run Copyop " + cfgBestHotType + " Job", e);
//			return false;
//		}
//    }
//
//    @Override
//    public void stop() {
//        log.info("CopyopHotTableJob stopping...");
//    }
//}