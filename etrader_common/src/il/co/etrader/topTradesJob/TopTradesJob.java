package il.co.etrader.topTradesJob;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import com.anyoption.common.beans.Country;
import com.anyoption.common.beans.Investment;
import com.anyoption.common.beans.LiveGlobeCity;
import com.anyoption.common.beans.base.Skin;
import com.anyoption.common.daos.CountryDAOBase;

import il.co.etrader.bl_managers.InvestmentsManagerBase;
import il.co.etrader.dao_managers.LiveGlobeCityDAOBase;
import il.co.etrader.helper.LiveHelper;
import il.co.etrader.util.CommonUtil;
import il.co.etrader.util.ConstantsBase;
import il.co.etrader.util.JobUtil;

public class TopTradesJob extends JobUtil {
	private static Logger log = Logger.getLogger(TopTradesJob.class);
	
	private static HashMap<Long, ArrayList<Investment>> lastTopTrades;
	protected static HashMap<Long, Country> countries;
	private static ArrayList<LiveGlobeCity> cities;
	private static long skinBusinessCaseId; 

	public static void main(String[] args) {
		log.info("Top trades job started.");
        propFile = args[0];
        skinBusinessCaseId = 0;
        try {
        	skinBusinessCaseId = Integer.parseInt(args[1]);
        	if (skinBusinessCaseId != Skin.SKIN_BUSINESS_ETRADER && skinBusinessCaseId != Skin.SKIN_BUSINESS_ANYOPTION) {
        		log.error("Wrong property of run type, must be number (1 or 2)");
 				return;
        	}
        } catch (Exception e) {
        	log.error("Wrong property of run type, must be number (1 or 2)");
 			return;
		}
        
        lastTopTrades = new HashMap<Long, ArrayList<Investment>>();
        countries = new HashMap<Long, Country>();
        cities = new ArrayList<LiveGlobeCity>();
        
        //get all countries and active cities.
        log.info("going to get all countries and active cities.");
        Connection con = null;
		try {
			con = getConnection();
			ArrayList<Country> cList = CountryDAOBase.getAll(con);
			for (Country c : cList) {
				c.setPaymentsMethodCountry(CountryDAOBase.getPaymentMethodsByCountryId(con, c.getId()));
	            countries.put(c.getId(), c);
			}
			cities = LiveGlobeCityDAOBase.getActiveCities(con);
			con.close();
		} catch (SQLException se) {
			log.fatal("Exception during get countries :", se);	    	
	    	se.printStackTrace();
		}
        
		//get list with all top trades data.
        String tillDate = getTillLastDay();
		try {
			con = getConnection();
			ArrayList<Investment> lastDayTopTrades;
			ArrayList<Investment> lastWeekTopTrades;
			ArrayList<Investment> lastMonthTopTrades;
			int lastDay = (int) (ConstantsBase.LIVE_AO_TOP_TRADES_LAST_DAY * (-1));
			log.info("going to get top trades for last day.");
			do {
				lastDayTopTrades = InvestmentsManagerBase.getTopTrades(con, getFromLastDayOfWeek(lastDay), getTillLastDayOfWeek(lastDay + 1), skinBusinessCaseId);
				lastDay--; // when no investments in last day, take investments from day before.
			} while (lastDayTopTrades.isEmpty());
			log.info("going to get top trades for last week.");
			lastWeekTopTrades = InvestmentsManagerBase.getTopTrades(con, getFromLastDays((int) (ConstantsBase.LIVE_AO_TOP_TRADES_LAST_WEEK * (-1))), tillDate, skinBusinessCaseId);
			log.info("going to get top trades for last month.");
			lastMonthTopTrades = InvestmentsManagerBase.getTopTrades(con, getFromLastDays((int) (ConstantsBase.LIVE_AO_TOP_TRADES_LAST_MONTH * (-1))), tillDate, skinBusinessCaseId);
			if (skinBusinessCaseId != Skin.SKIN_BUSINESS_ETRADER){					
				setDefaultCapitalCity(lastDayTopTrades, con);
				setDefaultCapitalCity(lastWeekTopTrades, con);
				setDefaultCapitalCity(lastMonthTopTrades, con);
			}
			lastTopTrades.put(ConstantsBase.LIVE_AO_TOP_TRADES_LAST_DAY, lastDayTopTrades);
			lastTopTrades.put(ConstantsBase.LIVE_AO_TOP_TRADES_LAST_WEEK, lastWeekTopTrades);
			lastTopTrades.put(ConstantsBase.LIVE_AO_TOP_TRADES_LAST_MONTH, lastMonthTopTrades);
			con.close();
		} catch (SQLException se) {
			log.fatal("Exception during performing action :", se);	    	
	    	se.printStackTrace();
		}
		
		// insert list to DB.
		log.info("going to insert top trades to DB.");
		try {
			con = getConnection();
			con.setAutoCommit(false);
			Iterator iter = lastTopTrades.keySet().iterator();
			while (iter.hasNext()) {
				long lastDays = (Long) iter.next();
				ArrayList<Investment> tempLastTopTrades = lastTopTrades.get(lastDays);
				int typeLastDay = 0;
				switch ((int)lastDays) {
				case (int)ConstantsBase.LIVE_AO_TOP_TRADES_LAST_DAY:
					typeLastDay = ConstantsBase.TOP_TRADES_TYPE_LAST_DAY;
					break;
				case (int)ConstantsBase.LIVE_AO_TOP_TRADES_LAST_WEEK:
					typeLastDay = ConstantsBase.TOP_TRADES_TYPE_LAST_WEEK;
					break;
				case (int)ConstantsBase.LIVE_AO_TOP_TRADES_LAST_MONTH:
					typeLastDay = ConstantsBase.TOP_TRADES_TYPE_LAST_MONTH;
					break;
					
				default:
					typeLastDay = ConstantsBase.TOP_TRADES_TYPE_LAST_DAY;
					break;
				}
				for (Investment inv : tempLastTopTrades) {
					insertTopTrades(con, inv, typeLastDay);
				}
			}
			con.commit();
		} catch (SQLException se) {
			log.fatal("Exception during insert to top trades.", se);
			try {
        		con.rollback();
        	} catch (Throwable it) {
        		log.error("Can't rollback.", it);
        	}
			se.printStackTrace();
		} finally {
			try {
        		con.setAutoCommit(true);
        	} catch (Exception e) {
        		log.error("Can't set back to autocommit.", e);
        	}
			try {
				con.close();
			} catch (SQLException e) {
				log.error("Can't close connection.", e);
			}
		}
        
        if (log.isEnabledFor(Level.INFO)) {
	        log.log(Level.INFO, "Job completed.");
	    }
    }
	
	/**
	 *
	 * @return date of last day as SimpleDateFormat
	 */
	private static String getFromLastDays(int lastDays) {
		GregorianCalendar gc = new GregorianCalendar();
		gc.setTime(new Date());
		gc.add(GregorianCalendar.DAY_OF_MONTH, lastDays);
		SimpleDateFormat sd = new SimpleDateFormat("dd/MM/yyyy");
		return sd.format(gc.getTime());
	}

	/**
	 *
	 * @return date of today as SimpleDateFormat
	 */
	private static String getTillLastDay() {
		GregorianCalendar gc = new GregorianCalendar();
		gc.setTime(new Date());
		SimpleDateFormat sd = new SimpleDateFormat("dd/MM/yyyy");
		return sd.format(gc.getTime());
	}
	
	/**
	 *
	 * @return date of last day as SimpleDateFormat
	 */
	private static String getFromLastDayOfWeek(int lastDays) {
		GregorianCalendar gc = new GregorianCalendar();
		gc.setTime(new Date());
		gc.setFirstDayOfWeek(GregorianCalendar.TUESDAY);
		gc.add(GregorianCalendar.DAY_OF_MONTH, lastDays);
		if (gc.get(GregorianCalendar.DAY_OF_WEEK) == GregorianCalendar.SATURDAY || gc.get(GregorianCalendar.DAY_OF_WEEK) == GregorianCalendar.SUNDAY) {
			gc.set(GregorianCalendar.DAY_OF_WEEK, GregorianCalendar.FRIDAY);
		}
		SimpleDateFormat sd = new SimpleDateFormat("dd/MM/yyyy");
		return sd.format(gc.getTime());
	}

	/**
	 *
	 * @return date of today as SimpleDateFormat
	 */
	private static String getTillLastDayOfWeek(int tillDays) {
		GregorianCalendar gc = new GregorianCalendar();
		gc.setTime(new Date());
		gc.setFirstDayOfWeek(GregorianCalendar.TUESDAY);
		gc.add(GregorianCalendar.DAY_OF_MONTH, tillDays);
		if (gc.get(GregorianCalendar.DAY_OF_WEEK) == GregorianCalendar.SUNDAY || gc.get(GregorianCalendar.DAY_OF_WEEK) == GregorianCalendar.MONDAY) {
			gc.set(GregorianCalendar.DAY_OF_WEEK, GregorianCalendar.SATURDAY);
		}
		SimpleDateFormat sd = new SimpleDateFormat("dd/MM/yyyy");
		return sd.format(gc.getTime());
	}
	
	/**
	 * Set default city name for users that their city name can't be found on google API
	 * @param list
	 * @return
	 * @throws SQLException 
	 */
	private static void setDefaultCapitalCity(ArrayList<Investment> list, Connection con) throws SQLException {				
		for (Investment i: list) {
			if (Long.valueOf(i.getSkin()) == Skin.SKIN_BUSINESS_ETRADER) {
				LiveGlobeCity randomCity = LiveHelper.getRandomCityForEtraderForJob(con, cities);
				i.setCountryId(randomCity.getCountryId());
				i.setCountryName(countries.get(randomCity.getCountryId()).getDisplayName());
				i.setCityName(randomCity.getCityName());
			} else if (CommonUtil.isParameterEmptyOrNull(i.getCityName())) {
				i.setCityName(countries.get(i.getCountryId()).getCapitalCity());
			}
		}
	}
	
	/**
	 * insert top trades to DB.
	 * @param conn
	 * @param inv
	 * @param typeLastDay
	 * @throws SQLException
	 */
	private static void insertTopTrades(Connection conn, Investment inv, int typeLastDay) throws SQLException {
		PreparedStatement ps = null;
    	try {
    		String sql = " INSERT INTO " +
			            	" TOP_TRADES(ID, SKIN_BUSINESS_CASE_ID, LAST_DAYS, USER_ID, COUNTRY_ID, COUNTRY_DISPLAY_NAME, TIME_EST_CLOSING, " +
			            	" OPPORTUNITY_TYPE_NAME, MARKET_DISPLAY_NAME, CLOSING_LEVEL, PROFIT_USER, CITY_NAME, OPPORTUNITY_TYPE_ID, TIME_CREATED, OPPORTUNITY_ID) " +
			            " VALUES (SEQ_TOP_TRADES.NEXTVAL,?,?,?,?,?,?,?,?,?,?,?,?,sysdate,?) ";
    		ps = conn.prepareStatement(sql);
    		ps.setLong(1, skinBusinessCaseId);
    		ps.setLong(2, typeLastDay);
    		ps.setLong(3, inv.getUserId());
    		ps.setLong(4, inv.getCountryId());
    		ps.setString(5, inv.getCountryName());
    		ps.setTimestamp(6, convertToTimestamp(inv.getTimeEstClosing()));
    		ps.setString(7, inv.getNameOpportunityType());
    		ps.setString(8, inv.getMarketName());
    		ps.setDouble(9, inv.getClosingLevel());
    		ps.setLong(10, inv.getProfitUser());
    		ps.setString(11, inv.getCityName());
    		ps.setLong(12, inv.getOpportunityId());
    		ps.setLong(13, inv.getOpportunityTypeId());
    		ps.executeUpdate();
    	} finally {
			closeStatement(ps);
		}
    }
	
	/**
	 * convert date to timestamp.
	 * @param d
	 * @return ts as Timestamp
	 */
	private static Timestamp convertToTimestamp(Date d) {
        if (d == null) {
            return null;
        }
        Timestamp ts = new Timestamp(d.getTime());
        return ts;
    }
}

