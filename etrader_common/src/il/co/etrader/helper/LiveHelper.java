/**
 * 
 */
package il.co.etrader.helper;

import il.co.etrader.bl_managers.LiveGlobeCityManagerBase;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.LiveGlobeCity;

/**
 * @author pavelhe
 *
 */
public class LiveHelper {
	private static final Logger log = Logger.getLogger(LiveHelper.class);
	
	private static Random random;
	private static List<LiveGlobeCity> etraderReplacementCities;
	
	// TODO Get cities from DB! This is a temporary solution.
	static {
		random = new Random();
		
		etraderReplacementCities = new ArrayList<LiveGlobeCity>();
//		LiveGlobeCity city = new LiveGlobeCity();
//		city.setCityName("Victoria");
//		city.setCountryId(182L);
//		city.setGlobeLatitude("-4.6191430");
//		city.setGlobeLongtitude("55.4513149");
//		etraderReplacementCities.add(city);
		
//		LiveGlobeCity city = new LiveGlobeCity();
//		city.setCityName("Canberra");
//		city.setCountryId(14L);
//		city.setGlobeLatitude("-35.3082355");
//		city.setGlobeLongtitude("149.1242241");
//		etraderReplacementCities.add(city);
		
//		city = new LiveGlobeCity();
//		city.setCityName("Brasília");
//		city.setCountryId(31L);
//		city.setGlobeLatitude("-15.7801482");
//		city.setGlobeLongtitude("-47.9291698");
//		etraderReplacementCities.add(city);
		
//		city = new LiveGlobeCity();
//		city.setCityName("Helsinki");
//		city.setCountryId(72L);
//		city.setGlobeLatitude("60.1698450");
//		city.setGlobeLongtitude("24.9385508");
//		etraderReplacementCities.add(city);
		
		LiveGlobeCity city = new LiveGlobeCity();
		city.setCityName("Berlin");
		city.setCountryId(80L);
		city.setGlobeLatitude("52.5191710");
		city.setGlobeLongtitude("13.4060912");
		etraderReplacementCities.add(city);
		
		city = new LiveGlobeCity();
		city.setCityName("Rome");
		city.setCountryId(102L);
		city.setGlobeLatitude("41.9015141");
		city.setGlobeLongtitude("12.4607737");
		etraderReplacementCities.add(city);
		
		city = new LiveGlobeCity();
		city.setCityName("Moscow");
		city.setCountryId(172L);
		city.setGlobeLatitude("55.7496460");
		city.setGlobeLongtitude("37.6236800");
		etraderReplacementCities.add(city);
		
		city = new LiveGlobeCity();
		city.setCityName("Madrid");
		city.setCountryId(192L);
		city.setGlobeLatitude("40.4167754");
		city.setGlobeLongtitude("-3.7037902");
		etraderReplacementCities.add(city);
		
//		city = new LiveGlobeCity();
//		city.setCityName("Stockholm");
//		city.setCountryId(200L);
//		city.setGlobeLatitude("59.3289300");
//		city.setGlobeLongtitude("18.0649100");
//		etraderReplacementCities.add(city);
		
		city = new LiveGlobeCity();
		city.setCityName("London");
		city.setCountryId(219L);
		city.setGlobeLatitude("51.5073346");
		city.setGlobeLongtitude("-0.1276831");
		etraderReplacementCities.add(city);
	}
	
	
	public static LiveGlobeCity getRandomCityForEtrader() {
		List<LiveGlobeCity> cities = LiveGlobeCityManagerBase.getActiveCitiesCached();
		if (cities == null || cities.size() == 0) {
			log.warn("Could not get list of live globe cities from manager. Using the hard-coded list instead...");
			cities = etraderReplacementCities;
		}
			
		return cities.get(random.nextInt(cities.size()));
	}
	
	public static LiveGlobeCity getRandomCityForEtraderForJob(Connection con, List<LiveGlobeCity> cities) throws SQLException {
		if (cities == null || cities.size() == 0) {
			log.warn("Could not get list of live globe cities from manager. Using the hard-coded list instead...");
			cities = etraderReplacementCities;
		}
		return cities.get(random.nextInt(cities.size()));
	}
	
}
