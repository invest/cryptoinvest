package il.co.etrader.i18n;


import java.beans.FeatureDescriptor;
import java.util.Iterator;
import java.util.Locale;

import javax.el.ELContext;
import javax.el.ELResolver;
import javax.faces.context.FacesContext;


public class MessageSourceELResolver extends ELResolver {

	private static final Object[] NO_ARGS = new Object[] {};
    private ELResolver original = null;

    public MessageSourceELResolver () {
    	super();
    }
    
    public MessageSourceELResolver(final ELResolver aOriginal) {
        this.original = aOriginal;
    }

    public Object getValue(final ELContext context, final Object base, final Object property) {
        if (base instanceof MessageSource) {
            MessageSource messageSource = (MessageSource) base;
            Locale locale = FacesContext.getCurrentInstance().getViewRoot()
                                        .getLocale();

            return messageSource.getMessage((String) property, NO_ARGS, locale);
        } else {
            return original.getValue(context, base, property);
        }
    }

    public void setValue(final ELContext context, final Object base, final Object property,
        final Object value) {
        if (base instanceof MessageSource) {
            return; //read only for now
        } else {
            original.setValue(context, base, property, value);
        }
    }

    public boolean isReadOnly(final ELContext context, final Object base, final Object property) {
        if (base instanceof MessageSource) {
            return true;
        } else {
            return original.isReadOnly(context, base, property);
        }
    }

    public Class<?> getType(final ELContext context, final Object base, final Object property) {
        if (base instanceof MessageSource) {
            return String.class;
        } else {
            return original.getType(context, base, property);
        }
    }

    public Object getValue(final ELContext context, final Object base, final int index) {
        if (base instanceof MessageSource) {
            return getValue(context, base, "" + index);
        } else {
            return original.getValue(context, base, index);
        }
    }

    public void setValue(final ELContext context, final Object base, final int index, final Object value) {
        if (base instanceof MessageSource) {
            //AppContext read only
            return;
        } else {
            original.setValue(context, base, index, value);
        }
    }

    public boolean isReadOnly(final ELContext context, final Object base, final int index) {
        if (base instanceof MessageSource) {
            return true;
        } else {
            return original.isReadOnly(context, base, index);
        }
    }

    public Class<?> getType(final ELContext context, final Object base, final int index) {
        if (base instanceof MessageSource) {
            return getType(context, base, "" + index);
        } else {
            return original.getType(context, base, index);
        }
    }

		@Override
		public Class<?> getCommonPropertyType(ELContext context, Object base) {
			return original.getCommonPropertyType(context, base);
		}

		@Override
		public Iterator<FeatureDescriptor> getFeatureDescriptors(ELContext context,
				Object base) {
			return original.getFeatureDescriptors(context, base);
		}
}