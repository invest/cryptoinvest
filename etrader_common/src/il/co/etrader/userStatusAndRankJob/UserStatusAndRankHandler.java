package il.co.etrader.userStatusAndRankJob;

import il.co.etrader.util.JobUtil;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

public class UserStatusAndRankHandler extends JobUtil{
	private static Logger log = Logger.getLogger(UserStatusAndRankHandler.class);
	
	public static void main(String[] args) throws Exception {
		propFile = args[0];
		log.info("Start User status Job");
		runUserStatusHandler();
		log.info("Finish User status Job");
		log.info("Start User rank Job");
		runUserRankHandler();
		log.info("Finish User rank Job");
	}

	private static void runUserStatusHandler() throws SQLException {
		Connection con = getConnection();
		CallableStatement cstmt = null;
        try {
            cstmt = con.prepareCall("BEGIN USERS_STATUS_HANDLER(); END;");
            cstmt.execute();
		} catch (Exception e) {
			log.log(Level.FATAL,"Can't run users_status_handler ", e);
		} finally {
			try {
				cstmt.close();
			} catch (Exception e) {
				log.log(Level.ERROR,"Can't close", e);
			}
			try {
				con.close();
			} catch (Exception e) {
				log.log(Level.ERROR,"Can't close", e);
			}
		}		
	}
	
	private static void runUserRankHandler() throws SQLException {
		Connection con = getConnection();
		CallableStatement cstmt = null;
        try {
            cstmt = con.prepareCall("BEGIN USERS_RANK_HANDLER(); END;");
            cstmt.execute();
		} catch (Exception e) {
			log.log(Level.FATAL,"Can't run users_rank_handler ", e);
		} finally {
			try {
				cstmt.close();
			} catch (Exception e) {
				log.log(Level.ERROR,"Can't close", e);
			}
			try {
				con.close();
			} catch (Exception e) {
				log.log(Level.ERROR,"Can't close", e);
			}
		}		
	}
}
