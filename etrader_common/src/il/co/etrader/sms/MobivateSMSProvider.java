package il.co.etrader.sms;

import java.net.URLEncoder;
import java.util.Date;
import java.util.Hashtable;

import org.apache.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.anyoption.common.sms.SMSException;
import com.anyoption.common.util.ClearingUtil;

public class MobivateSMSProvider extends SMSProvider {
    
	private static final Logger log = Logger.getLogger(MobivateSMSProvider.class);
	
	private Hashtable<String, String> ccToRoutingID;
	
    public MobivateSMSProvider() throws SMSException {
    	log.info("constructor");
    }

    @Override
    public void setConfig(SMSProviderConfig config) {
    	super.setConfig(config);
    	log.info("config url:"+ url + " config username:"+username);
		String getAllCCandRouteIDs = null;
		try {
			Hashtable<String, String> headers;
			headers = new Hashtable<String, String>();
			//To send Unicode messages, make sure the following header is set:
			headers.put("User-Agent", "anyoptionrequestapi");
		     getAllCCandRouteIDs = ClearingUtil.executeGETRequest(url +username+":"+password+"/entity/user.UserRoutePricing/ALL/visible", null, true, headers);
			} catch (Exception e) {
		    log.error("Can not get all Mobivate routes GET request", e);
		    throw new RuntimeException("Mobivate error"+e.getMessage());
		}
		
		try {
		    Document doc = ClearingUtil.parseXMLToDocument(getAllCCandRouteIDs);
		    ccToRoutingID = parseUserRouting(doc, "userroutepricing", "countryCode", "userRouteId");	
		} catch (Exception e) {
		    log.error("Can not process Mobivate all routes request", e);
		    throw new RuntimeException("Mobivate error"+e.getMessage());
		}
    }

    
    private Hashtable<String, String> parseUserRouting(Document rootEl, String userRoutePricingTag,
	    String countryCodeTag, String userRouteIdTag) {
		Hashtable<String, String> routing = new Hashtable<String, String>();
	
		NodeList allChildRootEldocument = rootEl.getElementsByTagName(userRoutePricingTag);
	
		for (int i = 0; i < allChildRootEldocument.getLength(); i++) {
		    Node node = allChildRootEldocument.item(i);
		    if (node.getNodeType() == Node.ELEMENT_NODE) {
				Element eElement = (Element) node;
				String countryCode = eElement.getElementsByTagName(countryCodeTag).item(0).getTextContent();
				String userRouteId = eElement.getElementsByTagName(userRouteIdTag).item(0).getTextContent();
				//in case the given first tag has more than one item in it separated by a comma
				if (countryCode.contains(",")) {
				    String[] ccSinOneEl = countryCode.split(",");
				    for (String xtraCC : ccSinOneEl) {
			    		if(routing.get(xtraCC) == null) {
			    			routing.put(xtraCC, userRouteId);
			    		} else {
			    			log.info("duplicate routing for:"+xtraCC+ " value:" + userRouteId);
			    		}
			    	}
				} else {
		    		if(routing.get(countryCode) == null) {
		    			routing.put(countryCode, userRouteId);
		    		} else {
		    			log.info("duplicate routing for["+countryCode+ "] value [" + userRouteId + "]");
		    		}
				}
		    }
		}
		return routing;
    }
    
    @Override
    public void send(TextSMS msg) throws SMSException {
	        try {
	            msg.setTimeSent(new Date());
	            String request = "xml=" + URLEncoder.encode(composeTextRequest(msg), "UTF-8");
	            Hashtable<String, String> headers;
				headers = new Hashtable<String, String>();
				//To send Unicode messages, make sure the following header is set:
				headers.put("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
				headers.put("User-Agent", "anyoptionrequestapi");
	            String response = ClearingUtil.executePOSTRequest(url +username+":"+password+"/send/sms/single?", request, false, true, headers, "UTF-8");
	            parseResponse(msg, response);
	            return;
	        } catch (Exception e) {
	            log.error("Failed to submit message - " + e);
	            throw new SMSException("Failed to submit message.", e);
	        }
    }

    @Override
    public void send(WapPushSMS msg) throws SMSException {
        throw new SMSException("Not implemented yet.");
    }

    @Override
    public void send(PushRegistrySMS msg) throws SMSException {
        throw new SMSException("Not implemented yet.");
    }
    
    private String composeTextRequest(TextSMS msg) {
		String request =
	        "<message>\n" + 
	        "    <originator>"	+ msg.getSenderName()	+ "</originator>\n" + 
	        "    <recipient>"	+ msg.getPhoneNumber() 	+ "</recipient>\n" + 
	        "    <body>"		+ msg.getMessageBody() 	+ "</body>\n" +
	        //REFERENCE -> Used to correlate message with client reference
	        "    <reference>"	+	msg.getId()		+ "</reference>\n" ;
		
	        if(msg.getStatus() == SMS.STATUS_NOT_VERIFIED) {
	        	//HLR route ID
	        	log.info("SENDING HLR MESSAGE:"+msg.getId());
	        	request += "    <routeId>mhlrglobal</routeId>\n";
	        } else {
	        	//ROUTE -> UUID which identifies the route used for delivering the message
	        	String routeId = getRouteID(msg.getPhoneNumber());
	        	request += "    <routeId>"		+ routeId +"</routeId>\n" ;
	        	log.info("SENDING MESSAGE:"+ msg.getId()+" with route ID:"+routeId);
	        }
	        request += "</message>" ;
	        
	        return request;
    }
  
    private String getRouteID(String msgPhoneNumber) {
		int getRouteIDtries = 1;
		String routeID = null;
		do {
		    routeID = ccToRoutingID.get(msgPhoneNumber.substring(0, getRouteIDtries));
		    getRouteIDtries++;
		} while (routeID == null && getRouteIDtries < 4);//max Country codes are 3(4) digits
	
		return routeID;
    }

    private void parseResponse(TextSMS msg, String response) throws Exception {
		Document doc = ClearingUtil.parseXMLToDocument(response);
		Element root = doc.getDocumentElement();
		Node status = ClearingUtil.getNode(root, "authentication");
		String code = ClearingUtil.getElementValue((Element) status, "code/*");
		if (null != code && code.equals("0")) {
			if(msg.getStatus() == SMS.STATUS_NOT_VERIFIED) {
				msg.setStatus(SMS.STATUS_WAITING_HLR);
			} else {
				msg.setStatus(SMS.STATUS_SUBMITTED);				
			}
			msg.setError(" ");
		} else {
		    msg.setStatus(SMS.STATUS_FAILED_TO_SUBMIT);
		    msg.setError("Error, return code from Mobivate is:" + code);
		}
    }

    public Hashtable<String,String> getCcToRoutingID() {
    	return ccToRoutingID;
    }

    public void setCcToRoutingID(Hashtable<String, String> ccToRoutingID) {
    	this.ccToRoutingID = ccToRoutingID;
   }
    
}