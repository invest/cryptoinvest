package il.co.etrader.sms;

import java.io.InputStream;
import java.util.Properties;

import org.apache.log4j.Logger;

public class NagiosSMSSend {
    private static final Logger log = Logger.getLogger(NagiosSMSSend.class);
    
    public static void main(String[] args) {
        if (null == args || args.length < 2) {
            System.out.println("Usage: NagiosSMSSend <receiverNumber> <SMS text>");
            System.exit(1);
        }
        Properties config = null;
        InputStream is = null;
        try {
            is = NagiosSMSSend.class.getClassLoader().getResourceAsStream("nagiosSmsSend.properties");
            config = new Properties();
            config.load(is);
        } catch (Exception e) {
            log.error("Error loading Nagios SMS config.", e);
        } finally {
            if (null != is) {
                try {
                    is.close();
                } catch (Exception e) {
                    log.error("Can't close.", e);
                }
            }
        }
        if (null == config) {
            log.error("Can't find configuration.");
            System.exit(1);
        }
        SMSProviderConfig smsConfig = null;
        try {
            smsConfig = new SMSProviderConfig();
            smsConfig.setId(Long.parseLong(config.getProperty("smsprovider.id")));
            smsConfig.setName(config.getProperty("smsprovider.name"));
            smsConfig.setProviderClass(config.getProperty("smsprovider.class"));
            smsConfig.setUrl(config.getProperty("smsprovider.url"));
            smsConfig.setUserName(config.getProperty("smsprovider.user"));
            smsConfig.setPassword(config.getProperty("smsprovider.pass"));
            smsConfig.setProps(config.getProperty("smsprovider.props"));
            smsConfig.setDlrUrl(config.getProperty("smsprovider.dlr"));
            log.info("SMS Provider config: " + smsConfig.toString());
        } catch (Exception e) {
            log.error("Error loading the config.", e);
        }
        try {
            Class cl = Class.forName(smsConfig.getProviderClass());
            SMSProvider p = (SMSProvider) cl.newInstance();
            p.setConfig(smsConfig);

            String[] rcpts = args[0].split(",");
            for (int i = 0; i < rcpts.length; i++) {
                TextSMS sms = new TextSMS();
                sms.setSenderName("Nagios");
                sms.setSenderPhoneNumber("+44-8081890112");
                sms.setPhoneNumber(rcpts[i]);
                sms.setMessageBody(args[1]);
                sms.setProviderId(smsConfig.getId());
                p.send(sms);
                log.info("SMS sending to <" + rcpts[i] + "> result - status: " + sms.getStatus() + " error: " + sms.getError());
            }
        } catch (Exception e) {
            log.error("Can't send SMS.", e);
        }
    }
}