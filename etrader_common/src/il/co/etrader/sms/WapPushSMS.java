package il.co.etrader.sms;

public class WapPushSMS extends SMS {
    public WapPushSMS() {
        
    }

    /**
     * Create a compact binary representation of WAP push message (SI) as described in WAP-167-ServiceInd-20010731-a
     * section 8.
     *
     * @return Hex string with the message body in compact binary representation of the WAP push message (SI).
     */
    protected String getBody() throws Exception {
        byte msgBody[];
        if (isUnicode(messageBody)) {
            msgBody = messageBody.getBytes("UTF-8");
        } else {
            msgBody = messageBody.getBytes(); // TODO what happens if the default platform encoding is UTF-8? probably LATIN should be here
        }
        return  "01" + // Transaction ID
                "06" + // PDU Type push
                "04" + // Headers Length (content-type + headers)
                "03" + // Content-type length
                "ae" + // Content-type: application/vnd.wap.sic
                "81" + // Charset
                "ea" + // UTF-8
                "02" + // Version number (wbxml_version)
                "05" + // Unknown Public Identifier (si_public_id)
                "6a" + // Charset= (sibxml->charset)
                "00" + // String table length
                "45" + // <si>
                "c6" + // <indication
                "0c" + // href="http://
                "03" + // inline string follows
                byteArrayToHexString(wapURL.getBytes()) + "00" + // end of inline string
                "08" + // action="signal-high"
                "01" + // end of indication attribute list
                "03" + // inline string follows
                byteArrayToHexString(msgBody) + "00" + // end of inline string
                "01" + // </indication>
                "01";  // </si>
    }
}