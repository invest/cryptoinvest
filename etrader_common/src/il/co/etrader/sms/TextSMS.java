package il.co.etrader.sms;

public class TextSMS extends SMS {
	long keyType;
	long keyValue;
	
    public TextSMS() {
        
    }

	public long getKeyType() {
		return keyType;
	}

	public void setKeyType(long keyType) {
		this.keyType = keyType;
	}

	public long getKeyValue() {
		return keyValue;
	}

	public void setKeyValue(long keyValue) {
		this.keyValue = keyValue;
	}
    
}