package il.co.etrader.sms;

import java.util.Calendar;
import java.util.Date;
import java.util.Hashtable;

import org.apache.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.anyoption.common.sms.SMSException;
import com.anyoption.common.util.ClearingUtil;

public class UniCellSMSProvider extends SMSProvider {
    private static final Logger log = Logger.getLogger(UniCellSMSProvider.class);

    private Hashtable<String, String> headers;

    public UniCellSMSProvider() {
        headers = new Hashtable<String, String>();
        headers.put("Content-Type", "text/xml; charset=UTF-8");
    }

    public void send(TextSMS msg) throws SMSException {
        try {
        	msg.setTimeSent(new Date());
            String request = composeTextRequest(msg.getId(), msg.getSenderName(), msg.getPhoneNumber(), msg.getMessageBody());
            String response = ClearingUtil.executePOSTRequest(url, request, false, true, headers);
            parseResponse(msg, response);
        } catch (Exception e) {
            throw new SMSException("Failed to submit message.", e);
        }
    }

    public void send(WapPushSMS msg) throws SMSException {
        throw new SMSException("Not implemented yet.");
    }

    public void send(PushRegistrySMS msg) throws SMSException {
        throw new SMSException("Not implemented yet.");
    }

    private String composeTextRequest(long msgId, String sender, String phone, String message) {
        return
                "<?xml version=\"1.0\" encoding=\"UNICODE\"?>\n" +
                "<sms command=\"submit\" version=\"1.0\">" +
                    "<account>" +
                        "<id>" + username + "</id>" +
                        "<password>" + password + "</password>" +
                    "</account>" +
                    "<attributes>" +
                        "<validity type=\"relative\" units=\"d\">1</validity>" +
                        "<notify type=\"4\">" + dlrUrl + "</notify>" +
                        "<reference>" + msgId + "</reference>" +
                        "<replyPath>" + sender + "</replyPath>" +
                    "</attributes>" +
                    "<targets>" +
                        "<cellphone>" + phone + "</cellphone>" +
                    "</targets>" +
                    "<data type=\"text\"><![CDATA[" + message + "]]></data>" +
                "</sms>";
    }

    private void parseResponse(SMS msg, String response) {
        try {
            Document doc = ClearingUtil.parseXMLToDocument(response);
            Element root = doc.getDocumentElement();
            String resultType = ClearingUtil.getAttribute(root, "", "type");
            if (null != resultType && resultType.equals("success")) {
                msg.setStatus(SMS.STATUS_SUBMITTED);
                msg.setError(ClearingUtil.getElementValue(root, "code/*") + " " + ClearingUtil.getElementValue(root, "message/*"));
                msg.setReference(ClearingUtil.getElementValue(root, "references/reference/*"));
            } else {
                msg.setStatus(SMS.STATUS_FAILED_TO_SUBMIT);
                if (null != resultType) {
                    String errorCode = ClearingUtil.getElementValue(root, "error/code/*");
                    msg.setError(errorCode + " " + ClearingUtil.getElementValue(root, "error/message/*"));
                    if (null != errorCode && errorCode.equals("400")) { // handle temporary problem - retry in 2 min
                    	log.debug("got temporary error, " + errorCode);
                    	if (msg.getRetries() < maxRetries) {
                    		log.debug("retry in 2 minutes, retryNum:" + (msg.getRetries()+1));
	                    	msg.setError(msg.getError() + ", lastSent:" + msg.getTimeSent() + " ");
	                    	msg.setRetries(msg.getRetries() + 1);
	                    	Calendar c = Calendar.getInstance();
	                    	c.add(Calendar.MINUTE, 2);
	                    	msg.setScheduledTime(c.getTime());
	                    	msg.setStatus(SMS.STATUS_QUEUED);
                    	} else {
                    		log.debug("not retry, reached to max retries!");
                    	}
                    }
                } else {
                    msg.setError("Unparsable response");
                }
            }
        } catch (Exception e) {
            log.error("Can't process UniCell response.", e);
        }
    }
}