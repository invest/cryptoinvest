/**
 * 
 */
package il.co.etrader.sms;

import java.net.URLEncoder;
import java.util.Date;

import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.anyoption.common.sms.SMSException;
import com.anyoption.common.util.ClearingUtil;

/**
 * @author pavelt
 *
 */
public class TextLocalSMSProvider extends SMSProvider {
	private static Logger log = Logger.getLogger(TextLocalSMSProvider.class);
	
	
	public TextLocalSMSProvider() {
	}
	
	@Override
	public void send(TextSMS msg) throws SMSException {
		try {
			msg.setTimeSent(new Date());
			String body = "apiKey=" + URLEncoder.encode(props, "UTF-8")// login authentication with props instead of username and password
						+ "&message=" + URLEncoder.encode(msg.getMessageBody(), "UTF-8")
						+ "&sender=" + URLEncoder.encode(msg.getSenderName(), "UTF-8")
						+ "&numbers=" + URLEncoder.encode(msg.getPhoneNumber(), "UTF-8");
			String response = ClearingUtil.executePOSTRequest(url, body);
			parseJSONResponse(response, msg);
		} catch (Exception ex) {
			throw new SMSException("Failed to submit message.", ex);
		}
	}
	
	private static void parseJSONResponse(String response, TextSMS msg) {
		try {
			JSONObject jsonObjRes = new JSONObject(response);
			String status = jsonObjRes.getString("status");
			System.out.println(status);
			if (status != null && status.equals("success")) {
				msg.setStatus(SMS.STATUS_DELIVERED);
			} else {
				JSONArray error = jsonObjRes.getJSONArray("errors");
				String errMessage = error.getJSONObject(0).getString("message");
				msg.setStatus(SMS.STATUS_FAILED);
				msg.setError(errMessage);
			}
		} catch (JSONException ex) {
			log.error("Response is not the correct format :", ex);
			msg.setStatus(SMS.STATUS_FAILED);
			msg.setError("Response is not the correct format");
		}
	}
	
	@Override
	public void send(WapPushSMS msg) throws SMSException {
		throw new SMSException("Not implemented yet.");

	}

	@Override
	public void send(PushRegistrySMS msg) throws SMSException {
		throw new SMSException("Not implemented yet.");

	}

}
