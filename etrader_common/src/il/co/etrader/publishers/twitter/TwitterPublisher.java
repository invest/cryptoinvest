package il.co.etrader.publishers.twitter;

import il.co.etrader.util.CommonUtil;

import org.apache.log4j.Logger;

import winterwell.jtwitter.Twitter;
import winterwell.jtwitter.TwitterException;

public class TwitterPublisher {

	private static final Logger log = Logger.getLogger(TwitterPublisher.class);

	private static final String ETRADER_USER_NAME = "ashert@etrader.co.il";
	private static final String ETRADER_PASSWORD = "147258";

	private static final String TEST_USER_NAME = "ashert@etrader.co.il";
	private static final String TEST_PASSWORD = "147258";

	private static final String ANYOPTION_LIVE_USER_NAME = "anyoption";
	private static final String ANYOPTION_LIVE_PASSWORD = "anyopt2011";

	public static void publish(String userName, String password, String msg) throws TwitterException {

		publish(userName, password, msg, null);
	}

	public static void publish(String userName, String password, String msg1, String msg2) throws TwitterException {
		if (msg1 == null) {
			msg1 = "";
		}
		if (msg2 == null) {
			msg2 = "";
		}

		msg1 = CommonUtil.diacriticsCharsEncode(msg1);
		msg2 = CommonUtil.diacriticsCharsEncode(msg2);
		int msg2Len = msg2.length();
		int msg1Len = msg1.length();

		if (msg1Len + msg2Len > 160) {
			msg1 = msg1.substring(0, 160-msg2Len);
		}
		Twitter t = new Twitter(userName, password);
		t.updateStatus(msg1 + msg2);
	}

	private static String getAnyoptionUserName() {
		if (CommonUtil.getAppData().getIsLive()) {
			return ANYOPTION_LIVE_USER_NAME;
		}
		return TEST_USER_NAME;
	}

	private static String getAnyoptionPassword() {
		if (CommonUtil.getAppData().getIsLive()) {
			return ANYOPTION_LIVE_PASSWORD;
		}
		return TEST_PASSWORD;
	}


	public static void etraderPublish(String msg) throws TwitterException {

		publish(ETRADER_USER_NAME, ETRADER_PASSWORD, msg);
	}


	public static void anyoptionPublish(String msg1, String msg2) throws TwitterException {

		try {
			publish(getAnyoptionUserName(), getAnyoptionPassword(), msg1, msg2);
		} catch (TwitterException te) {
			log.error("Twitter did not publish: " + msg1 + msg2);
		}
	}

	public static void anyoptionPublish(String msg) throws Exception {
		publish(getAnyoptionUserName(), getAnyoptionPassword(), msg, null);
	}
}
