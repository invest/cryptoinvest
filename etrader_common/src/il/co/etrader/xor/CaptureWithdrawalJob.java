package il.co.etrader.xor;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.Hashtable;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import com.anyoption.common.beans.Transaction;
import com.anyoption.common.clearing.ClearingException;
import com.anyoption.common.clearing.DeltaPayClearingProvider;
import com.anyoption.common.clearing.DeltaPayInfo;
import com.anyoption.common.util.AESUtil;

import il.co.etrader.bl_managers.TransactionsManagerBase;
import il.co.etrader.bl_vos.Writer;
import il.co.etrader.dao_managers.TransactionsDAOBase;
import il.co.etrader.util.CommonUtil;
import il.co.etrader.util.ConstantsBase;
import il.co.etrader.util.JobUtil;


/**
 * Go over the pending transactions from the previous day and make "capture" on them.
 *
 */
public class CaptureWithdrawalJob extends JobUtil {

    private static Logger log = Logger.getLogger(CaptureWithdrawalJob.class);

    public static int TRANSACTION_HAS_FEE = 0;
    public static int NUMBER_OF_HOURS_IN_A_DAY = 24;
    public static int CURRENCY_ILS = 1;


	/**
	 * Goes over the approved (by accounting) transactions in the db from the previosday and make a "capture" request
	 * for each of them
	 *
	 */
	private static void captureWithdrawalTransactions() {
		String hours = getPropertyByFile("xor.delay.hours");
		ReportSummery summery = new ReportSummery();
		StringBuffer report = new StringBuffer("<br><b>Capture withdraw transactions:</b><table border=\"1\">");
		report.append("<tr><td>Transaction id</td><td>Provider</td><td>Status</td><td>Description</td><td>Amount</td><td>Currency</td></tr>");
		Connection conn1 = null;
		Connection conn2 = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		int xorId;
		int skinId;
		String utc_offset = ConstantsBase.EMPTY_STRING;
		String lastCurrencyCode = null;

		try {
			conn1 = getConnection(ClearingUtil.DB_URL, ClearingUtil.DB_USER, ClearingUtil.DB_PASS);

			String sql =
            	" select u.id_num idnum,t.user_id userid,t.id tid,c.cc_number ccnumber,c.cc_pass cvv,c.exp_month expmonth,curr.code, "+
            	" c.exp_year expyear,c.type_id type_id,t.amount amount,t.auth_number auth_number,w.user_name writer, " +
            	" t.fee_cancel,s.default_xor_id,u.skin_id, u.utc_offset, u.currency_id, l.cc_fee "+
            	" from transactions t, credit_cards c,writers w,users u,skins s,currencies curr, limits l " +
            	" where t.user_id=u.id and t.credit_card_id=c.id and t.writer_id=w.id and s.id=u.skin_id and curr.id=u.currency_id " +
            	" and u.limit_id = l.id and u.class_id <> 0 and t.status_id = "+TransactionsManagerBase.TRANS_STATUS_SECOND_APPROVAL + " and " +
            	" t.is_accounting_approved = "+ ConstantsBase.ACCOUNTING_APPROVED_YES +" and " +
                " t.type_id = "+TransactionsManagerBase.TRANS_TYPE_CC_WITHDRAW +" and " +
                " t.time_created < sysdate " +
                " order by curr.id ";

			pstmt = conn1.prepareStatement(sql);
			rs = pstmt.executeQuery();

			while (rs.next()) {
                ClearingInfo info = new ClearingInfo();
                info.setTransactionId(rs.getInt("tid"));
                info.setCcn(AESUtil.decrypt(rs.getString("ccnumber")));
                info.setExpireMonth(rs.getString("expmonth"));
                info.setExpireYear(rs.getString("expyear"));
                info.setCvv(rs.getString("cvv"));
                info.setCcTypeId(rs.getInt("type_id"));
                String currentCurrencyCode = rs.getString("code");
                info.setCurrencySymbol(currentCurrencyCode);
                info.setCurrencyId(rs.getInt("currency_id"));
                info.setAuthNumber(rs.getString("auth_number"));
                info.setAmount(rs.getLong("amount"));
                info.setUserId(rs.getLong("userid"));
                skinId = rs.getInt("skin_id");
                if ( CommonUtil.isHebrewSkin(skinId)) {
                	try {
                		info.setCardUserId(Long.valueOf(AESUtil.decrypt(rs.getString("idnum"))));
                	} catch (Exception e) {
                		// FIXME very bad way to handle exceptions
						throw new SQLException(e.getMessage());
					}
                }
                else {  // Ao users(no id number)
                	info.setCardUserId(0);
                }
                xorId = rs.getInt("default_xor_id");
                info.setTransactionType(ConstantsBase.XOR_TRANSACTION_DIRECTION_CREDIT);
                utc_offset = rs.getString("utc_offset");      //save user utc offset
                //set fee indicaor and amount in case transaction has fee
                boolean hasFee = rs.getInt("fee_cancel") == TRANSACTION_HAS_FEE;
                long feeAmount = rs.getLong("cc_fee");
                long amount = rs.getLong("amount");
                long amountAfterFee = amount - feeAmount;
                if (hasFee) {
                	info.setAmount(amountAfterFee);
                }

                if (log.isEnabledFor(Level.INFO)) {
                    log.log(Level.INFO, "Going to capture transaction: " + info.toString() + " on provider: XOR");
                }
                Connection connClearingProvider = null;
                try {
                	connClearingProvider = getConnection(ClearingUtil.DB_URL, ClearingUtil.DB_USER, ClearingUtil.DB_PASS);
                	XORClearingProvider cp = new XORClearingProvider();
                	String system = getPropertyByFile("system");

                	if(system.equals(ConstantsBase.LIVE_SYSTEM)) {
//                		cp = SkinsDAOBase.getXORClearingProvider(connClearingProvider,skinId,0,0);
                	}
                	else {
//                		cp = SkinsDAOBase.getXORClearingProvider(connClearingProvider,skinId,1,0);
                	}

                    cp.capture(info);
                } catch (ClearingException ce) {
                    log.log(Level.ERROR, "Failed to capture withdrawal transaction: " + info.toString(), ce);
                    info.setResult("999");
                    info.setMessage(ce.getMessage());
                } finally {
                	try {
                		connClearingProvider.close();
                	} catch (Exception e) {
                		log.error("cant close connection: connClearingProvider", e);
					}
                }

                // currency code display
                if ( null == lastCurrencyCode || !lastCurrencyCode.equals(currentCurrencyCode) ) {
                    lastCurrencyCode = currentCurrencyCode;
                    report.append("<tr>");
                    report.append("<td colspan=\"6\" align=\"center\" valign=\"middle\"><b>" + lastCurrencyCode + "</b>");report.append("</td>");
                    report.append("</tr>");
                }

                if (info.isSuccessful()) {
                    report.append("<tr>");
                } else {
                    report.append("<tr style=\" color: #FF0000; font-weight: bold;\">");
                }

                report.append("<td>").append(info.getTransactionId()).append("</td><td>");
                report.append("XOR").append("</td><td>");
                report.append(info.getResult()).append("</td><td>");
                report.append(info.getMessage()).append("</td><td>");
                report.append(ClearingUtil.formatAmount(info.getAmount())).append("</td><td>");
                report.append(currentCurrencyCode).append("</td></tr>\n");
                summery.addTransaction(rs.getString("writer"), "XOR", info.getAmount(), currentCurrencyCode, info.isSuccessful());

                //verify result from xor is numeric
                int result = -1;
                try {
                    result = Integer.parseInt(info.getResult());
                } catch (NumberFormatException nfe) {
                    log.log(Level.WARN, "Result not a number.", nfe);
                }
                try {
                	//handle success from xor
                	conn2 = getConnection(ClearingUtil.DB_URL, ClearingUtil.DB_USER, ClearingUtil.DB_PASS);
	                String updateSql;
                	if (info.isSuccessful()) {

                      	//update transaction status to success and amount to the amount after fee (incase there was fee)
	            		updateSql = "update transactions set time_settled=sysdate, status_id="+TransactionsManagerBase.TRANS_STATUS_SUCCEED+
	                   					   ",comments='"+info.getResult()+"|"+info.getMessage() + "|"+info.getUserMessage()+"|"+info.getXorTranId()+"'"+
	                   					   " , xor_id_capture='"+info.getXorTranId()+ "' , amount=" + info.getAmount() + ", " +
	                   					   " utc_offset_settled = '"+ utc_offset + "' where id="+info.getTransactionId();

	                   	conn2.setAutoCommit(false); //in order to support rollback once and update or an insert was not succesfull

	                   	//insert new fee transaction
	                   	if (hasFee) {
		                	Transaction feeTransaction = getFeeTransaction(info); //creating the fee transaction accoring to the orig transaction
		                	feeTransaction.setAmount(feeAmount);
		                	feeTransaction.setUtcOffsetCreated(utc_offset);
		                	feeTransaction.setUtcOffsetSettled(utc_offset);
		    				TransactionsDAOBase.insert(conn2, feeTransaction);
		    				if (log.isEnabledFor(Level.INFO)) {
		    					String ls = System.getProperty("line.separator");
		    					log.log(Level.INFO, " fee transaction inserted: " + ls
		    							+ feeTransaction.toString());
		    				}

		    				//update Transaction reference
		    				TransactionsDAOBase.updateRefId(conn2, info.getTransactionId(), feeTransaction.getId());
	                   	}
                	}
                	//handle failed from xor
                	else {
                		//update transaction status to failed
                		updateSql = "update transactions set time_settled=sysdate, status_id="+TransactionsManagerBase.TRANS_STATUS_FAILED+
	          	 						   ",comments='"+info.getResult()+"|"+info.getMessage() + "|"+info.getUserMessage()+"|"+info.getXorTranId()+"'"+
	          	 						   " , description='"+ConstantsBase.ERROR_FAILED_CAPTURE+"', xor_id_capture='"+info.getXorTranId()+"' , " +
	          	 						   " utc_offset_settled = '"+ utc_offset + "' where id="+info.getTransactionId();
                	}
                	conn2.prepareStatement(updateSql).executeUpdate();
                	conn2.commit();
                } finally {
                	try {
                		conn2.close();
                	} catch (Exception e) {
                		log.log(Level.ERROR, "Can't close: conn2", e);
                	}
                }
            }
        } catch (Exception e) {
            log.log(Level.ERROR, "Error while capturing pending transactions.", e);
        } finally {
            try {
                rs.close();
            } catch (Exception e) {
                log.log(Level.ERROR, "Can't close", e);
            }
            try {
                pstmt.close();
            } catch (Exception e) {
                log.log(Level.ERROR, "Can't close", e);
            }
            try {
                conn1.close();
            } catch (Exception e) {
                log.log(Level.ERROR, "Can't close", e);
            }

        }
        report.append("</table>");

        String reportBody = "<html><body>" +
            summery.getSummery() +
            report.toString() +
            "</body></html>";

        if (log.isEnabledFor(Level.INFO)) {
            log.log(Level.INFO, "Sending report email: " + reportBody);
        }

        // send email
        Hashtable server = new Hashtable();
        server.put("url", getPropertyByFile("xor.email.url"));
        server.put("auth", getPropertyByFile("xor.email.auth"));
        server.put("user", getPropertyByFile("xor.email.user"));
        server.put("pass", getPropertyByFile("xor.email.pass"));
        server.put("contenttype", getPropertyByFile("xor.email.contenttype", "text/html; charset=UTF-8"));

        Hashtable email = new Hashtable();
        email.put("subject", getPropertyByFile("xor.email.subject"));
        email.put("to", getPropertyByFile("xor.email.to"));
        email.put("from", getPropertyByFile("xor.email.from"));
        email.put("body", reportBody);
        sendEmail(server, email);
    }

	/**
	 * This method create a fee transaction and sets it accoring to the clearing info
	 * @param info - Clearing info sent to Xor
	 * @return New fee transaction
	 */
	private static Transaction getFeeTransaction(ClearingInfo info) {
		Transaction t = new Transaction();
    	t.setUserId(info.getUserId());
    	t.setTypeId(TransactionsManagerBase.TRANS_TYPE_HOMO_FEE);
    	t.setTimeCreated(new Date());
    	t.setStatusId(TransactionsManagerBase.TRANS_STATUS_SUCCEED);
    	t.setWriterId(Writer.WRITER_ID_AUTO);
    	t.setIp("IP NOT FOUND!");
    	t.setTimeSettled(new Date());
    	t.setProcessedWriterId(Writer.WRITER_ID_AUTO);
    	t.setReferenceId(new BigDecimal(info.getTransactionId()));
    	t.setAuthNumber(info.getAuthNumber());
    	t.setXorIdCapture(info.getXorTranId());
    	return t;
	}

	public static void CaptureTESTTTTTTTTTT() {
		DeltaPayClearingProvider c = new DeltaPayClearingProvider();
		DeltaPayInfo deltaInfo = new DeltaPayInfo();
		deltaInfo.setAffiliate("ALFA");
        deltaInfo.setPayMethod("Credit Card");
        deltaInfo.setTransactionId(3299999);
        deltaInfo.setAmount(5000);
        deltaInfo.setCurrencySymbol("USD");
        deltaInfo.setAuthNumber("ALFA 4F55C8EE5EC93");
		try {
			c.bookback(deltaInfo);
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

    /**
     * Capture Withdrawal Job.
     *
     * @param args
     */
    public static void main(String[] args) {

//        if (null == args || args.length < 1) {
//            log.log(Level.FATAL, "Configuration file not specified, insert properties file name.");
//            return;
//        }
//
//        propFile = args[0];


//        if (log.isEnabledFor(Level.INFO)) {
//            log.log(Level.INFO, "Starting the job.");
//        }

        //captureWithdrawalTransactions();
        CaptureTESTTTTTTTTTT();

//        if (log.isEnabledFor(Level.INFO)) {
//            log.log(Level.INFO, "Job completed.");
//        }
    }
}