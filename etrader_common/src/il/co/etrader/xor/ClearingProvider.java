//package il.co.etrader.xor;
//
//import java.util.Properties;
//
//import com.anyoption.common.clearing.ClearingException;
//
///**
// * Interface to be implemented for clearing providers so the rest of the logic
// * don't depend on specific clearing provider.
// *
// * @author Tony
// */
//public interface ClearingProvider {
//    /**
//     * Process an "authorize" call through this provider.
//     * 
//     * @param info thransaction info
//     * @throws ClearingException
//     */
//    public void authorize(ClearingInfo info) throws ClearingException;
//
//    /**
//     * Process an "capture" call through this provider.
//     * 
//     * @param info thransaction info
//     * @throws ClearingException
//     */
//    public void capture(ClearingInfo info) throws ClearingException;
//
//    /**
//     * Process an "cancel" call through this provider.
//     * 
//     * @param info thransaction info
//     * @throws ClearingException
//     */
//    public void cancel(ClearingInfo info) throws ClearingException;
//
//    /**
//     * Set clearing provider properties.
//     *
//     * @param key
//     * @param props
//     * @throws ClearingException
//     */
//    public void setProperties(String key, Properties props) throws ClearingException;
//    
//    /**
//     * Check if this transaction will not exceed the provider limits.
//     * 
//     * @param info thransaction info
//     * @return <code>true</code> if the transaction do NOT exceed the limits else <code>false</code>.
//     * @throws ClearingException
//     */
//    public boolean checkLimits(ClearingInfo info) throws ClearingException;
//}