package il.co.etrader.xor;

import com.anyoption.common.clearing.ClearingException;

public class NoRouteException extends ClearingException {
    public NoRouteException() {
        super();
    }

    public NoRouteException(String message) {
        super(message);
    }

    public NoRouteException(String message, Throwable t) {
        super(message, t);
    }
}