package il.co.etrader.dao_managers;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;

import javax.servlet.http.HttpSession;

import il.co.etrader.bl_vos.UserBase;
import il.co.etrader.sms.SMSProviderConfig;
import il.co.etrader.util.ConstantsBase;

public class SMSDAOBase extends com.anyoption.common.daos.SMSDAOBase {

//    public static long queueMessage(
//    		OracleConnection conn,
//            long typeId,
//            String sender,
//            String senderNumber,
//            String phone,
//            String message,
//            String wapPushURL,
//            long dstPort,
//            long keyValue,
//            long keyType,
//            long providerId, 
//            long descriptionId) throws SQLException {
//
//        OraclePreparedStatement opstmt = null;
//
//        try {
//            String sql =
//                "INSERT INTO sms " +
//                    "(id, sms_type_id, sender, phone, message, wap_url, dst_port, scheduled_time, sms_status_id, key_value, key_type, provider_Id, sender_number, sms_description_id) " +
//                 "VALUES " +
//                     "(SEQ_SMS.NEXTVAL, ?, ?, ?, ?, ?, ?, sysdate, 1, ?, ?, ?, ?, ?)";
//
//            opstmt = (OraclePreparedStatement)conn.prepareStatement(sql);
//            opstmt.setFormOfUse(4, OraclePreparedStatement.FORM_NCHAR);
//
//            opstmt.setLong(1, typeId);
//            opstmt.setString(2, sender);
//            opstmt.setString(3, phone);
//            opstmt.setString(4, message);
//            if (null != wapPushURL) {
//                opstmt.setString(5, wapPushURL);
//            } else {
//                opstmt.setNull(5, Types.VARCHAR);
//            }
//            if (dstPort > 0) {
//                opstmt.setLong(6, dstPort);
//            } else {
//                opstmt.setNull(6, Types.NUMERIC);
//            }
//            opstmt.setLong(7, keyValue);
//            opstmt.setLong(8, keyType);
//            opstmt.setLong(9, providerId);
//            if (null != senderNumber) {
//                opstmt.setString(10, senderNumber);
//            } else {
//                opstmt.setNull(10, Types.VARCHAR);
//            }
//            opstmt.setLong(11, descriptionId);
//            opstmt.executeUpdate();
//
//            return getSeqCurValue(conn, "SEQ_SMS");
//        } finally {
//            closeStatement(opstmt);
//        }
//    }
    
	public static long queueMessageBatch(Connection conn,  long typeId, String sender, String senderNumber, String phone, String message, HttpSession session, 
			String wapPushURL,long dstPort, long keyType, long providerId, ArrayList<UserBase> usersList) throws SQLException {

		String sql = "INSERT INTO sms " +
				"(id, sms_type_id, sender, phone, message, wap_url, dst_port, scheduled_time, sms_status_id, key_value, key_type, provider_Id, sender_number) " +
				"VALUES " +
				"(SEQ_SMS.NEXTVAL, ?, ?, ?, ?, ?, ?, sysdate, 1, ?, ?, ?, ?)";
		PreparedStatement ps = conn.prepareStatement(sql);
		int sumOfUsersList = usersList.size();
		final int batchSize = 1000;
		int count = 0;
		double i = 0;	
		String percentage = "";
		int index = 0;
		session.setAttribute(ConstantsBase.PROMOTION_TOTAL_SENDED_TO_USERS, "0_0");
		try{
			for (UserBase usr: usersList) {
				ps.setLong(index++, typeId);
				ps.setString(index++, sender);
				ps.setString(index++, phone);
				ps.setString(index++, message);
				if (null != wapPushURL) {
					ps.setString(index++, wapPushURL);
				} else {
					ps.setNull(index++, Types.VARCHAR);
				}
				if (dstPort > 0) {
					ps.setLong(index++, dstPort);
				} else {
					ps.setNull(index++, Types.NUMERIC);
				}
				ps.setLong(index++, usr.getId());
				ps.setLong(index++, keyType);
				ps.setLong(index++, providerId);
				if (null != senderNumber) {
					ps.setString(index++, senderNumber);
				} else {
					ps.setNull(index++, Types.VARCHAR);
				}
				ps.addBatch();
				count++;
				index = 0;

				percentage = Double.toString(((i/sumOfUsersList)*100));
				session.setAttribute(ConstantsBase.PROMOTION_TOTAL_SENDED_TO_USERS,sumOfUsersList+ "_" + percentage.substring(0, percentage.indexOf(".")) );

				if (count % batchSize == 0) {
					ps.executeBatch();
				}
			}
			return getSeqCurValue(conn, "SEQ_SMS");
		} finally {
			ps.executeBatch();
			ps.close();
		}
	}
    		
//    	opstmt = (OraclePreparedStatement)conn.prepareStatement(sql);
//            opstmt.setFormOfUse(4, OraclePreparedStatement.FORM_NCHAR);
//
//            opstmt.setLong(1, typeId);
//            opstmt.setString(2, sender);
//            opstmt.setString(3, phone);
//            opstmt.setString(4, message);
//            if (null != wapPushURL) {
//                opstmt.setString(5, wapPushURL);
//            } else {
//                opstmt.setNull(5, Types.VARCHAR);
//            }
//            if (dstPort > 0) {
//                opstmt.setLong(6, dstPort);
//            } else {
//                opstmt.setNull(6, Types.NUMERIC);
//            }
//            opstmt.setLong(7, keyValue);
//            opstmt.setLong(8, keyType);
//            opstmt.setLong(9, providerId);
//            if (null != senderNumber) {
//                opstmt.setString(10, senderNumber);
//            } else {
//                opstmt.setNull(10, Types.VARCHAR);
//            }
//            opstmt.executeUpdate();
//
//            return getSeqCurValue(conn, "SEQ_SMS");
//        } finally {
//            closeStatement(opstmt);
//        }
//    }

    /**
     * Load all providers configs.
     *
     * @param conn
     * @return <code>ArrayList<SMSProviderConfig></code>
     * @throws SQLException
     */
    public static ArrayList<SMSProviderConfig> loadSMSProvidersConfigs(Connection conn) throws SQLException {
        ArrayList<SMSProviderConfig> l = new ArrayList<SMSProviderConfig>();
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            String sql =
                "SELECT " +
                    "* " +
                "FROM " +
                    "sms_providers";
            pstmt = conn.prepareStatement(sql);
            rs = pstmt.executeQuery();
            SMSProviderConfig c = null;
            while (rs.next()) {
                c = new SMSProviderConfig();
                c.setId(rs.getLong("id"));
                c.setProviderClass(rs.getString("provider_class"));
                c.setUrl(rs.getString("url"));
                c.setDlrUrl(rs.getString("dlr_url"));
                c.setUserName(rs.getString("user_name"));
                c.setPassword(rs.getString("password"));
                c.setProps(rs.getString("props"));
                c.setName(rs.getString("name"));
                l.add(c);
            }
        } finally {
            closeResultSet(rs);
            closeStatement(pstmt);
        }
        return l;
    }
}