
package il.co.etrader.dao_managers;
import java.io.UnsupportedEncodingException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.anyoption.common.daos.DAOBase;

import il.co.etrader.bl_vos.City;

public class CitiesDAO extends DAOBase{
	  public static void insert(Connection con,City vo) throws SQLException {
		  PreparedStatement ps = null;
		  try {
				String sql="insert into cities(id,name) values(?,?) ";
				ps = con.prepareStatement(sql);
				ps.setLong(1, vo.getId());
				ps.setString(2, vo.getName());
				ps.executeUpdate();
		  }finally{
				closeStatement(ps);
			}
	  }

		  public static ArrayList<City> getAll(Connection con) throws SQLException {

			  PreparedStatement ps=null;
			  ResultSet rs=null;
			  ArrayList<City> list=new ArrayList<City>();

			  try {
				   //for anyoption: CITIES.ID=0 is a AO city
				    String sql="select * from Cities where id<>0 order by name";

					ps = con.prepareStatement(sql);

					rs=ps.executeQuery();

					while (rs.next()) {
						City vo=new City();
						vo.setId(rs.getLong("id"));
						vo.setName(rs.getString("name"));
						list.add(vo);
					}
				}

				finally
				{
					closeResultSet(rs);
					closeStatement(ps);
				}
				return list;

		  }
		  public static ArrayList<String> getByPrefix(Connection con,String prefix) throws SQLException,UnsupportedEncodingException {
			  PreparedStatement ps = null;
			  ResultSet rs = null;
			  ArrayList<String> list = new ArrayList<String>();
			  try {
				  String sql="select * from Cities where name like ?||'%' order by name";
					ps = con.prepareStatement(sql);
					ps.setString(1, prefix);
					rs = ps.executeQuery();
					while (rs.next()) {
						String name=rs.getString("name");
						list.add(name);
					}
				} finally {
					closeResultSet(rs);
					closeStatement(ps);
				}
				return list;
		  }

		  public static City getById(Connection con,long id) throws SQLException {

			  PreparedStatement ps=null;
			  ResultSet rs=null;

			  City vo=null;
			  try
				{
				    String sql="select * from Cities where id=?";
					ps = con.prepareStatement(sql);
					ps.setLong(1, id);
					rs=ps.executeQuery();
					if (rs.next()) {
						vo=new City();
						vo.setId(rs.getLong("id"));
						vo.setName(rs.getString("name"));
					}
				}

				finally
				{
					closeResultSet(rs);
					closeStatement(ps);
				}
				return vo;

		  }

		  public static long getIdByName(Connection con,String n) throws SQLException {
			  if (n==null || n.trim().equals(""))
				  return 0;

			  PreparedStatement ps=null;
			  ResultSet rs=null;
			  try {
				    String sql="select * from Cities where name=?";
					ps = con.prepareStatement(sql);
					ps.setString(1, n.trim());
					rs=ps.executeQuery();
					if (rs.next()) {
						return rs.getLong("id");
					}
				} finally {
					closeResultSet(rs);
					closeStatement(ps);
				}
				return 0;
		  }
}