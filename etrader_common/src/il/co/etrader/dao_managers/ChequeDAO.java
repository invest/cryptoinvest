package il.co.etrader.dao_managers;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.anyoption.common.daos.DAOBase;

import il.co.etrader.bl_vos.Cheque;

public class ChequeDAO extends DAOBase{
	  public static void insert(Connection con,Cheque vo) throws SQLException {
		  PreparedStatement ps = null;
		  ResultSet rs = null;
		  try {
 				String sql="insert into Cheques(id,name,street,city_id,zip_code,cheque_id,street_no,fee_cancel, city_name) values(seq_cheques.nextval,?,?,?,?,?,?,?,?) ";

				ps = con.prepareStatement(sql);
				ps.setString(1, vo.getName());
				ps.setString(2, vo.getStreet());
				ps.setLong(3, vo.getCityId());
				ps.setString(4, vo.getZipCode());
				ps.setString(5, vo.getChequeId());
				ps.setString(6, vo.getStreetNo());
				if (vo.getFeeCancel())
					ps.setInt(7, 1);
				else
					ps.setInt(7, 0);
				ps.setString(8, vo.getCityName());
				ps.executeUpdate();
				vo.setId(getSeqCurValue(con,"seq_cheques"));
		    } finally {
 			    closeResultSet(rs);
				closeStatement(ps);
			}
	  }

	  public static void update(Connection con,Cheque vo) throws SQLException {
		  PreparedStatement ps = null;
		  try {

				String sql="update Cheques set name=?,street=?,city_id=?,zip_code=?,cheque_id=?,street_no=?,fee_cancel=? where id=?";

				ps = con.prepareStatement(sql);
				ps.setString(1, vo.getName());
				ps.setString(2, vo.getStreet());
				ps.setLong(3, vo.getCityId());
				ps.setString(4, vo.getZipCode());
				ps.setString(5, vo.getChequeId());
				ps.setString(6, vo.getStreetNo());
				if (vo.getFeeCancel())
					ps.setInt(7, 1);
				else
					ps.setInt(7, 0);
				ps.setLong(8, vo.getId());

				ps.executeUpdate();
		  } finally {
				closeStatement(ps);
			}
	  }
	  
	  public static void updateWithoutFee(Connection con,Cheque vo) throws SQLException {
		  PreparedStatement ps = null;
		  try {
				String sql="update Cheques set name=?,street=?,city_id=?,zip_code=?,cheque_id=?,street_no=?,fee_cancel=? where id=?";

				ps = con.prepareStatement(sql);
				ps.setString(1, vo.getName());
				ps.setString(2, vo.getStreet());
				ps.setLong(3, vo.getCityId());
				ps.setString(4, vo.getZipCode());
				ps.setString(5, vo.getChequeId());
				ps.setString(6, vo.getStreetNo());
				if (vo.getFeeCancel())
					ps.setInt(7, 1);
				else
					ps.setInt(7, 0);
				ps.setLong(8, vo.getId());

				ps.executeUpdate();
		  } finally {
				closeStatement(ps);
			}
	  }	  

		  public static ArrayList getAll(Connection con) throws SQLException {

			  PreparedStatement ps=null;
			  ResultSet rs=null;
			  ArrayList list=new ArrayList();

			  try
				{
				    String sql="select * from Cheques";

					ps = con.prepareStatement(sql);

					rs=ps.executeQuery();

					while (rs.next()) {
						Cheque vo=new Cheque();

						vo.setId(rs.getLong("id"));
						vo.setName(rs.getString("name"));
						vo.setStreet(rs.getString("street"));
						vo.setStreetNo(rs.getString("street_no"));
						vo.setCityId(rs.getLong("city_id"));
						vo.setZipCode(rs.getString("zip_code"));
						vo.setChequeId(rs.getString("cheque_id"));
						vo.setFeeCancel(rs.getInt("fee_cancel")==1);
						list.add(vo);
					}
				}

				finally
				{
					closeResultSet(rs);
					closeStatement(ps);
				}
				return list;

		  }

		  public static Cheque getById(Connection con,long id) throws SQLException {

			  PreparedStatement ps=null;
			  ResultSet rs=null;

			  Cheque vo=null;
			  try
				{
				    String sql="select * from Cheques where id=?";

					ps = con.prepareStatement(sql);
					ps.setLong(1, id);

					rs=ps.executeQuery();

					if (rs.next()) {
						vo=new Cheque();
						vo.setId(rs.getLong("id"));
						vo.setName(rs.getString("name"));
						vo.setStreet(rs.getString("street"));
						vo.setStreetNo(rs.getString("street_no"));
						vo.setCityId(rs.getLong("city_id"));
						vo.setZipCode(rs.getString("zip_code"));
						vo.setChequeId(rs.getString("cheque_id"));
						vo.setFeeCancel(rs.getInt("fee_cancel")==1);
					}
				}

				finally
				{
					closeResultSet(rs);
					closeStatement(ps);
				}
				return vo;

		  }


		  /**
		   * Update fee cancel field
		   * @param con
		   * 		Db connection
		   * @param chequeId
		   * 		cheque id
		   * @param feeCancel
		   * 		true - in case we cancel the fee
		   * @throws SQLException
		   */
		  public static void updateFee(Connection con, long chequeId, boolean feeCancel) throws SQLException {

			  PreparedStatement ps = null;

			  try {

					String sql = "update Cheques set fee_cancel=? where id=?";

					ps = (PreparedStatement) con.prepareStatement(sql);

					if (feeCancel) {
						ps.setInt(1, 1);
					}
					else {
						ps.setInt(1, 0);
					}

					ps.setLong(2, chequeId);

					ps.executeUpdate();

			  }	finally {
					closeStatement(ps);
				}

		  }


}
