package il.co.etrader.dao_managers;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;

import com.anyoption.common.daos.LimitsDAOBase;

import il.co.etrader.bl_managers.TransactionsManagerBase;
import il.co.etrader.bl_vos.Limit;
import il.co.etrader.util.CommonUtil;
import il.co.etrader.util.ConstantsBase;

public class LimitsDAO extends LimitsDAOBase {

	  public static void insert(Connection con,Limit vo) throws SQLException {
		  PreparedStatement ps = null;
		  try {

				String sql = "insert into limits(id,name,min_deposit,min_withdraw,max_withdraw,max_deposit" +
							 ",max_deposit_per_day,max_deposit_per_month,time_last_update,writer_id,comments,time_created," +
							 "min_bank_wire,max_bank_wire,free_invest_min,free_invest_max,bank_wire_fee,cc_fee, " +
							 "cheque_fee,deposit_docs_limit1,deposit_docs_limit2,deposit_alert) " +
							 "values(seq_limits.nextval,?,?,?,?,?,?,?,?,?,?,sysdate,?,?,?,?,?,?,?,?,?,?) ";

				ps = con.prepareStatement(sql);
				ps.setString(1, vo.getName());
				ps.setLong(2, vo.getMinDeposit());
				ps.setLong(3, vo.getMinWithdraw());
				ps.setLong(4, vo.getMaxWithdraw());
				ps.setLong(5, vo.getMaxDeposit());
				ps.setLong(6, vo.getMaxDepositPerDay());
				ps.setLong(7, vo.getMaxDepositPerMonth());
				ps.setTimestamp(8, CommonUtil.convertToTimeStamp(vo.getTimeLastUpdate()));
				ps.setLong(9, vo.getWriterId());
				ps.setString(10, vo.getComments());
				ps.setLong(11, vo.getMinBankWire());
				ps.setLong(12, vo.getMaxBankWire());
				ps.setLong(13, vo.getFreeInvestMin());
				ps.setLong(14, vo.getFreeInvestMax());
				ps.setLong(15, vo.getBankWireFee());
				ps.setLong(16, vo.getCcFee());
				ps.setLong(17, vo.getChequeFee());
				ps.setLong(18, vo.getDepositDocsLimit1());
				ps.setLong(19, vo.getDepositDocsLimit2());
				ps.setLong(20, vo.getDepositAlert());

				ps.executeUpdate();
				vo.setId(getSeqCurValue(con,"seq_limits"));

		  }
			finally {
				closeStatement(ps);
			}

	  }


	  public static void update(Connection con,Limit vo) throws SQLException {
		  PreparedStatement ps = null;
		  try {

				String sql = "update limits set name=?,min_deposit=?,min_withdraw=?,max_withdraw=?,max_deposit=? " +
							 ",max_deposit_per_day=?,max_deposit_per_month=?,time_last_update=?,writer_id=?,comments=?,time_created=sysdate, " +
							 "min_bank_wire=?,max_bank_wire=?,free_invest_min=?,free_invest_max=?,bank_wire_fee=?,cc_fee=?, " +
							 "cheque_fee=?,deposit_docs_limit1=?,deposit_docs_limit2=?,deposit_alert=? " +
							 "where id=? ";

				ps = con.prepareStatement(sql);
				ps.setString(1, vo.getName());
				ps.setLong(2, vo.getMinDeposit());
				ps.setLong(3, vo.getMinWithdraw());
				ps.setLong(4, vo.getMaxWithdraw());
				ps.setLong(5, vo.getMaxDeposit());
				ps.setLong(6, vo.getMaxDepositPerDay());
				ps.setLong(7, vo.getMaxDepositPerMonth());
				ps.setTimestamp(8, CommonUtil.convertToTimeStamp(vo.getTimeLastUpdate()));
				ps.setLong(9, vo.getWriterId());
				ps.setString(10, vo.getComments());
				ps.setLong(11, vo.getMinBankWire());
				ps.setLong(12, vo.getMaxBankWire());
				ps.setLong(13, vo.getFreeInvestMin());
				ps.setLong(14, vo.getFreeInvestMax());
				ps.setLong(15, vo.getBankWireFee());
				ps.setLong(16, vo.getCcFee());
				ps.setLong(17, vo.getChequeFee());
				ps.setLong(18, vo.getDepositDocsLimit1());
				ps.setLong(19, vo.getDepositDocsLimit2());
				ps.setLong(20, vo.getDepositAlert());
				ps.setLong(21, vo.getId());
				ps.executeUpdate();
		  } finally {
				closeStatement(ps);
			}
	  }

	 public static ArrayList<Limit> getAll(Connection con) throws SQLException {

		  PreparedStatement ps = null;
		  ResultSet rs = null;
		  ArrayList<Limit> list = new ArrayList<Limit>();

		  try {

			    String sql = "select l.*, w.user_name writer " +
			    		     "from limits l, writers w " +
			    		     "where l.writer_id = w.id ";

				ps = con.prepareStatement(sql);
				rs = ps.executeQuery();
				while (rs.next()) {
					Limit l = getVO(con,rs);
					l.setWriterName(rs.getString("writer"));
					list.add(l);
				}

			} finally {

				closeResultSet(rs);
				closeStatement(ps);
			}

			return list;

		  }

	 /**
	  * Get limits list by skin and currency , not just the default
	  * @param con
	  * 	db connection
	  * @param skinId
	  * 	limits skin id
	  * @param currencyId
	  * 	limits currency id
	  * @return
	  * @throws SQLException
	  */
	 public static ArrayList getBySkinCurrency(Connection con, int skinId, long currencyId, String writerSkins) throws SQLException {

		  PreparedStatement ps = null;
		  ResultSet rs = null;
		  ArrayList list = new ArrayList();

		  try {

			    String sql = "select distinct l.*, w.user_name writer, sc.currency_id, " +
			    			 "sc.is_default, sc.skin_id, s.display_name, c.code " +
			    		     "from limits l, writers w, skin_currencies sc, skins s, currencies c " +
			    		     "where l.writer_id = w.id and s.id = sc.skin_id " +
			    		     "and sc.limit_id = l.id and c.id = sc.currency_id " +
			    		     "and sc.skin_id in (" + writerSkins + ") ";

			    if ( skinId > 0 ) {
			    	sql += "and sc.skin_id = ? ";
			    }

			    if ( currencyId > 0 ) {
			    	sql += "and sc.currency_id = ? ";
			    }

			    sql += "order by l.id ";

				ps = con.prepareStatement(sql);

				if ( skinId > 0 ) {
					ps.setInt(1, skinId);
				}

				if ( currencyId > 0 && skinId > 0) {
					ps.setLong(2, currencyId);
				} else if ( currencyId > 0 ) {
					ps.setLong(1, currencyId);
				}

				rs = ps.executeQuery();

				while (rs.next()) {
					Limit l = getVO(con,rs);
					l.setWriterName(rs.getString("writer"));
					l.setCurrencyId(rs.getLong("currency_id"));
					l.setIsDefault(rs.getInt("is_default"));
					l.setSkinId(rs.getInt("skin_id"));
					l.setSkinName(rs.getString("display_name"));
					l.setCurrencyName(rs.getString("code"));
					list.add(l);
				}

			} finally {

				closeResultSet(rs);
				closeStatement(ps);
			}

			return list;

		  }

		  public static Limit getById(Connection con,long id) throws SQLException {
			  PreparedStatement ps = null;
			  ResultSet rs = null;

			  try
				{
				    String sql=" select l.*,w.user_name writer from limits l, writers w " +
				    		   " where l.id=? and l.writer_id=w.id ";
					ps = con.prepareStatement(sql);
					ps.setLong(1, id);
					rs = ps.executeQuery();
					if (rs.next()) {
						Limit l = getVO(con,rs);
						l.setWriterName(rs.getString("writer"));
						return l;
					}
				}
				finally
				{
					closeResultSet(rs);
					closeStatement(ps);
				}
				return null;
		  }


		  /**
		   * Get default Limit id By skin id and currency id
		   * @param con
		   * 	Db connection
		   * @param skinId
		   * 	skin id for mapping
		   * @param currencyId
		   * 	currency id for mapping
		   * @return
		   * 	limit id
		   * @throws SQLException
		   */
		  public static long getBySkinAndCurrency(Connection con, long skinId, long currencyId) throws SQLException {

			  PreparedStatement ps = null;
			  ResultSet rs = null;
			  long limitId = 0;

			  try
				{
				    String sql = "select limit_id from skin_currencies " +
				    			 "where skin_id = ? and currency_id = ? and is_default = 1" ;

					ps = con.prepareStatement(sql);
					ps.setLong(1, skinId);
					ps.setLong(2, currencyId);

					rs = ps.executeQuery();

					if (rs.next()) {
						limitId = rs.getLong("limit_id");
					}
				}

				finally
				{
					closeResultSet(rs);
					closeStatement(ps);
				}

				return limitId;
		  }

	  	/**
	     * Get min and max values for free investment
	     * @param conn
	     * @param limitId
	     * 		limit id of the user
	     * @return
	     * 		HashMap with min & max values
	     * @throws SQLException
	     */
	    public static HashMap<String,Long> getMinAndMaxFreeInv(Connection conn, long limitId) throws SQLException {

	    PreparedStatement pstmt = null;
	    ResultSet rs = null;
	    HashMap<String,Long> freeInvValues = new HashMap<String,Long>();

	    try {

	        String sql = "select free_invest_min, free_invest_max " +
	        			 "from limits  " +
	        			 "where id = ? ";

	        pstmt = conn.prepareStatement(sql);
	        pstmt.setLong(1, limitId);

	        rs = pstmt.executeQuery();

	        if ( rs.next() ) {
	            freeInvValues.put(ConstantsBase.FREE_INVEST_MIN, rs.getLong("free_invest_min"));
	            freeInvValues.put(ConstantsBase.FREE_INVEST_MAX, rs.getLong("free_invest_max"));
	        }

	    } finally {
	        closeResultSet(rs);
	        closeStatement(pstmt);
	    }

	    return freeInvValues;
	}


	  private static Limit getVO(Connection con,ResultSet rs) throws SQLException{
		  	Limit vo = new Limit();
		  	vo.setId(rs.getLong("id"));
			vo.setName(rs.getString("name"));
			vo.setMinDeposit(rs.getLong("MIN_DEPOSIT"));
			vo.setMinWithdraw(rs.getLong("MIN_WITHDRAW"));
			vo.setMaxDeposit(rs.getLong("MAX_DEPOSIT"));
			vo.setMaxWithdraw(rs.getLong("MAX_WITHDRAW"));
			vo.setMaxDepositPerDay(rs.getLong("MAX_DEPOSIT_PER_DAY"));
			vo.setMaxDepositPerMonth(rs.getLong("MAX_DEPOSIT_PER_MONTH"));
			vo.setTimeLastUpdate(convertToDate(rs.getTimestamp("TIME_LAST_UPDATE")));
			vo.setTimeCreated(convertToDate(rs.getTimestamp("TIME_CREATED")));
			vo.setWriterId(rs.getLong("WRITER_ID"));
			vo.setComments(rs.getString("COMMENTS"));
			vo.setMinBankWire(rs.getLong("min_bank_wire"));
			vo.setMaxBankWire(rs.getLong("max_bank_wire"));
			vo.setFreeInvestMin(rs.getLong("free_invest_min"));
			vo.setFreeInvestMax(rs.getLong("free_invest_max"));
			vo.setBankWireFee(rs.getLong("bank_wire_fee"));
			vo.setCcFee(rs.getLong("cc_fee"));
			vo.setChequeFee(rs.getLong("cheque_fee"));
			vo.setDepositDocsLimit1(rs.getLong("deposit_docs_limit1"));
			vo.setDepositDocsLimit2(rs.getLong("deposit_docs_limit2"));
			vo.setMinFirstDeposit(rs.getLong("min_first_deposit"));
			vo.setDepositAlert(rs.getLong("deposit_alert"));
			vo.setAmountForLowWithdraw(rs.getLong("amt_for_low_withdraw"));
			return vo;
	  }

	  //TODO : need to change function
		  public static void clearDefaultLimit(Connection con) throws SQLException {

			  String sql="update limits set is_default=0";

			  updateQuery(con,sql);

		  }

	    /**
	     * get deposit limits by limitId
	     * @param limitId
	     * 		user limit id
	     * @return
		 * 		HashMap with deposit limits values
		 * 			value1: deposit amount for warnning (limit1)
		 * 			value2: deposit amount for lock account (limit2)
		 * @throws SQLException
	     */
	    public static HashMap<String,Long> getDepositLimits(Connection conn, long limitId) throws SQLException {

	    PreparedStatement pstmt = null;
	    ResultSet rs = null;
	    HashMap<String,Long> limits = new HashMap<String,Long>();

	    try {

	        String sql = "select deposit_docs_limit1, deposit_docs_limit2 " +
	        			 "from limits " +
	        			 "where id = ? ";

	        pstmt = conn.prepareStatement(sql);
	        pstmt.setLong(1, limitId);

	        rs = pstmt.executeQuery();

	        if ( rs.next() ) {
	            limits.put(ConstantsBase.DEPOSIT_DOCS_LIMIT1, rs.getLong("deposit_docs_limit1"));
	            limits.put(ConstantsBase.DEPOSIT_DOCS_LIMIT2, rs.getLong("deposit_docs_limit2"));
	        }

	    } finally {
	        closeResultSet(rs);
	        closeStatement(pstmt);
	    }

	    return limits;
	}

		 /**
		  * Get limits list by writer skins
		  * @param con
		  * 	db connection
		  * @param skinId
		  * 	limits skin id
		  * @param currencyId
		  * 	limits currency id
		  * @return
		  * @throws SQLException
		  */
		 public static ArrayList getByWriterSkins(Connection con, String writerSkins) throws SQLException {

			  PreparedStatement ps = null;
			  ResultSet rs = null;
			  ArrayList list = new ArrayList();

			  try {

				    String sql = "select distinct l.*, w.user_name writer " +
				    		     "from limits l, writers w, skin_currencies sc, skins s " +
				    		     "where l.writer_id = w.id and s.id = sc.skin_id " +
				    		     "and sc.limit_id = l.id " +
				    		     "and sc.skin_id in (" + writerSkins + ") ";


				    sql += "order by l.id ";

					ps = con.prepareStatement(sql);

					rs = ps.executeQuery();

					while (rs.next()) {
						Limit l = getVO(con,rs);
						l.setWriterName(rs.getString("writer"));
						list.add(l);
					}

				} finally {

					closeResultSet(rs);
					closeStatement(ps);
				}

				return list;

			  }


		  /**
		   * Get fee By limit id and tran type is
		   * @param con
		   * 	Db connection
		   * @param limitId
		   * 	user limit id, for mapping
		   * @param tranTypeId
		   * 	withdraw tranTypeId , for mapping
		   * @return
		   * 	withdrawal fee
		   * @throws SQLException
		   */
		 @Deprecated
		 public static long getFeeByTranTypeId(Connection con, long limitId, int tranTypeId) throws SQLException {
			  PreparedStatement ps = null;
			  ResultSet rs = null;
			  long fee = 0;
			  String feeTypeStr = "";

			  // find fee type
			  switch (tranTypeId) {
			  	case TransactionsManagerBase.TRANS_TYPE_CC_WITHDRAW:
			  		feeTypeStr = " cc_fee ";
					break;
			  	case TransactionsManagerBase.TRANS_TYPE_BANK_WIRE_WITHDRAW:
			  		feeTypeStr = " bank_wire_fee ";
					break;
                case TransactionsManagerBase.TRANS_TYPE_EFT_WITHDRAW:
                    feeTypeStr = " bank_wire_fee ";
                    break;
			  	case TransactionsManagerBase.TRANS_TYPE_CHEQUE_WITHDRAW:
			  		feeTypeStr = " cheque_fee ";
					break;
			  	case TransactionsManagerBase.TRANS_TYPE_PAYPAL_WITHDRAW:
			  		feeTypeStr = " paypal_fee" ;
					break;
			  	case TransactionsManagerBase.TRANS_TYPE_ENVOY_WITHDRAW:
			  		feeTypeStr = " envoy_fee ";
					break;
			  	case TransactionsManagerBase.TRANS_TYPE_MONEYBOOKERS_WITHDRAW:
			  		feeTypeStr = " bank_wire_fee ";
					break;
			  	case TransactionsManagerBase.TRANS_TYPE_WEBMONEY_WITHDRAW:
			  		feeTypeStr = " bank_wire_fee ";
					break;
			  	case TransactionsManagerBase.TRANS_TYPE_BAROPAY_WITHDRAW:
			  		feeTypeStr = " bank_wire_fee ";
					break;
			  	case TransactionsManagerBase.TRANS_TYPE_CUP_CHINA_WITHDRAW:
			  		feeTypeStr = " bank_wire_fee ";
					break;
//			  	case TransactionsManagerBase.TRANS_TYPE_LOW_AMOUNT_WITHDRAW_FEE:
//			  		feeTypeStr = " amt_for_low_withdraw * " + TransactionsManagerBase.TRANS_TYPE_LOW_AMOUNT_WITHDRAW_FEE_COEFFICIENT + " ";
//			  		break;
				default:
					return 0;
			  }

			  try {
				    String sql =
				    	"SELECT " +
				    		feeTypeStr + " fee " +
				    	"FROM " +
				    		"limits " +
				    	"WHERE " +
				    		"id = ? ";

					ps = con.prepareStatement(sql);
					ps.setLong(1, limitId);
					rs = ps.executeQuery();

					if (rs.next()) {
						fee = rs.getLong("fee");
					}
				} finally {
					closeResultSet(rs);
					closeStatement(ps);
				}
				return fee;
		  }

		  public static long getDepositAlertLimits(Connection con, long limitId) throws SQLException {

			  PreparedStatement ps = null;
			  ResultSet rs = null;
			  long threshold_deposit = 0;

			  try {

				  	String sql = "SELECT " +
				  				 	"deposit_alert " +
				  				 "FROM " +
				  				 	"limits " +
				  				 "WHERE " +
				  				 	"id = ? ";

					ps = con.prepareStatement(sql);
					ps.setLong(1, limitId);
					rs = ps.executeQuery();
					if (rs.next()) {
						threshold_deposit = rs.getLong("deposit_alert");
					}

				} finally {

					closeResultSet(rs);
					closeStatement(ps);
				}

				return threshold_deposit;

			  }

}
