package il.co.etrader.dao_managers;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.anyoption.common.beans.SkinPaymentMethod;
import com.anyoption.common.daos.DAOBase;


/**
 * SkinPaymentMethod DAO Base class.
 *
 * @author Elad
 */
public class SkinPaymentMethodDAOBase extends DAOBase {


	public static ArrayList<SkinPaymentMethod> getAllBySkinId(Connection con,long skinId) throws SQLException {

		PreparedStatement ps=null;
		ResultSet rs=null;
		ArrayList<SkinPaymentMethod> list=new ArrayList<SkinPaymentMethod> ();

		SkinPaymentMethod vo=null;
		try
		{
			String sql="select * from skin_payment_methods where skin_id=?";

			ps = con.prepareStatement(sql);
			ps.setLong(1, skinId);

			rs=ps.executeQuery();

			while (rs.next()) {
				vo=new SkinPaymentMethod();
				vo.setId(rs.getLong("id"));
				vo.setSkinId(rs.getLong("skin_id"));
				vo.setTransactionTypeId(rs.getLong("transaction_type_id"));
				list.add(vo);
			}
		}

		finally
		{
			closeResultSet(rs);
			closeStatement(ps);
		}
		return list;

	}
}
