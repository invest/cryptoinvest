package il.co.etrader.dao_managers;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Random;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import com.anyoption.common.beans.Investment;
import com.anyoption.common.beans.InvestmentLimitsGroup;
import com.anyoption.common.beans.OneTouchFields;
import com.anyoption.common.beans.Opportunity;
import com.anyoption.common.beans.OpportunityType;
import com.anyoption.common.beans.base.Skin;
import com.anyoption.common.bl_vos.OptionPlusQuote;
import com.anyoption.common.managers.MarketsManagerBase;

import il.co.etrader.bl_managers.InvestmentsManagerBase;
import il.co.etrader.bl_vos.CopyopInvestmentDetails;
import il.co.etrader.bl_vos.CopyopInvestmentDetails.Pair;
import il.co.etrader.bl_vos.InvestmentLimit;
import il.co.etrader.bl_vos.Skins;
import il.co.etrader.bl_vos.UserBase;
import il.co.etrader.util.CommonUtil;
import il.co.etrader.util.ConstantsBase;

public class InvestmentsDAOBase extends com.anyoption.common.daos.InvestmentsDAOBase {

	private static final Logger logger = Logger.getLogger(InvestmentsDAOBase.class);

//	protected static Investment getVO(ResultSet rs) throws SQLException {
//		Investment vo = new Investment();
//		vo.setId(rs.getLong("id"));
//		vo.setUserId(rs.getLong("user_id"));
//		vo.setOpportunityId(rs.getLong("opportunity_Id"));
//		vo.setTypeId(rs.getLong("type_Id"));
//		vo.setTimeCreated(convertToDate(rs.getTimestamp("time_created")));
//		vo.setAmount(rs.getLong("amount"));
//		vo.setCurrentLevel(rs.getDouble("current_level"));
//		vo.setRealLevel(rs.getDouble("real_level"));
//		vo.setWwwLevel(rs.getBigDecimal("www_level"));
//		vo.setOddsWin(rs.getDouble("odds_Win"));
//		vo.setOddsLose(rs.getDouble("odds_Lose"));
//		vo.setWriterId(rs.getLong("writer_id"));
//		vo.setCanceledWriterId(rs.getBigDecimal("canceled_writer_id"));
//		vo.setIp(rs.getString("ip"));
//		vo.setWin(rs.getLong("win"));
//		vo.setLose(rs.getLong("lose"));
//		vo.setHouseResult(rs.getLong("house_Result"));
//		vo.setTimeSettled(convertToDate(rs.getTimestamp("time_settled")));
//		vo.setIsSettled(rs.getInt("is_settled"));
//		vo.setIsCanceled(rs.getInt("is_canceled"));
//		vo.setTimeCanceled(convertToDate(rs.getTimestamp("time_canceled")));
//		vo.setIsVoid(rs.getInt("is_void"));
//		vo.setBonusOddsChangeTypeId(rs.getInt("bonus_odds_change_type_id"));
//        vo.setUtcOffsetCreated(rs.getString("utc_offset_created"));
//        vo.setUtcOffsetSettled(rs.getString("utc_offset_settled"));
//        vo.setUtcOffsetCancelled(rs.getString("utc_offset_cancelled"));
//        vo.setInsuranceAmountGM(rs.getLong("insurance_amount_gm"));
//        vo.setInsuranceAmountRU(rs.getLong("insurance_amount_ru"));
//        vo.setReferenceInvestmentId(rs.getLong("REFERENCE_INVESTMENT_ID"));
//        vo.setRate(rs.getDouble("rate"));
//        vo.setInsuranceFlag(rs.getInt("insurance_flag"));
//        if (rs.wasNull()) { //check if insurance flag was null in db if yes set it to null
//            vo.setInsuranceFlag(null);
//        }
//        vo.setAcceptedSms(rs.getLong("is_accepted_sms") == 1 ? true : false);
//        vo.setOptionPlusFee(rs.getLong("option_plus_fee"));
//        vo.setCopyOpInvId(rs.getLong("copyop_inv_id"));
//        vo.setCopyOpTypeId(rs.getInt("copyop_type_id"));
//        vo.setLikeHourly(rs.getLong("is_like_hourly") == 1 ? true : false);
//		return vo;
//	}

	public static Investment getById(Connection con, long id) throws SQLException {

		PreparedStatement ps = null;
		ResultSet rs = null;
		Investment inv = null;
		try {
			String sql = "SELECT " +
                             "i.*," +
                             "u.currency_id currency_id, " +
                             "u.skin_id skin_Id " +
                         "FROM " +
                             "investments i, " +
                             "users u " +
                         "WHERE " +
                             "i.user_id = u.id and " +
                             "i.id = ? ";
			ps = con.prepareStatement(sql);
			ps.setLong(1, id);
			rs = ps.executeQuery();
			if (rs.next())	{
				inv =  getVO(rs);
				inv.setSkin(rs.getString("skin_id"));
				return inv;
			}
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		return null;

	}

	public static Investment getDetails(Connection con, long id) throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		SimpleDateFormat tzDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS Z");
		try {
			String sql =
				"SELECT " +
				    "i.*, " +
				    "o.market_id, " +
				    "int.display_name type_name, " +
				    "o.closing_level, " +
				    "m.display_name market_name, " +
				    "to_char(o.time_act_closing, 'YYYY-MM-DD HH24:MI:SS.FF3 TZHTZM') AS time_act_closing, " +
				    "to_char(o.time_est_closing, 'YYYY-MM-DD HH24:MI:SS.FF3 TZHTZM') AS time_est_closing, " +
				    "o.scheduled scheduled, " +
				    "u.currency_id currency_id, " +
				    "u.utc_offset utc_offset, " +
				    "m.decimal_point, " +
                    "iref.d rolled_inv_id " +
				"FROM " +
				    "investments i, " +
				    "opportunities o, " +
				    "markets m, " +
				    "investment_types int, " +
				    "users u, " +
                    "(select " +
                    "   i2.id as d, " +
                    "   i2.REFERENCE_INVESTMENT_ID as r " +
                    "from " +
                    "   investments i2 " +
                    "where " +
                    "   i2.REFERENCE_INVESTMENT_ID > 0) iref " +
                "WHERE " +
                    "i.user_id = u.id AND " +
                    "i.id = ? AND " +
                    "i.opportunity_id = o.id AND " +
                    "o.market_id = m.id AND " +
                    "i.type_id = int.id AND " +
                    "i.id = iref.r(+)";
			ps = con.prepareStatement(sql);
			ps.setLong(1, id);
			rs = ps.executeQuery();
			if (rs.next()) {
				Investment i = getVO(rs);
				i.setScheduledId(rs.getLong("scheduled"));
				i.setMarketId(rs.getLong("market_id"));
                i.setDecimalPoint(rs.getLong("decimal_point"));
				i.setClosingLevel(rs.getDouble("closing_Level"));
				i.setTypeName(CommonUtil.getMessage(rs.getString("type_name"),null));
				if (rs.getString("time_act_closing") != null) {
					i.setTimeActClosing(tzDateFormat.parse(rs.getString("time_act_closing")));
					i.setUtcOffsetActClosing(rs.getString("utc_offset"));
				}
				if (rs.getString("time_est_closing") != null) {
					i.setTimeEstClosing(tzDateFormat.parse(rs.getString("time_est_closing")));
					i.setUtcOffsetEstClosing(rs.getString("utc_offset"));
				}
				i.setMarketName(MarketsManagerBase.getMarketName(Skins.SKIN_REG_EN, rs.getLong("market_id")));

				// set up_down field for one touch investments
				if ( i.getTypeId() == InvestmentsManagerBase.INVESTMENT_TYPE_ONE ) {
					int upDown = InvestmentsDAOBase.getOneTouchFields(con, i.getOpportunityId() ).getUpDown();
					i.setOneTouchUpDown(upDown);
				}
                i.setRolledInvId(rs.getLong("rolled_inv_id"));
				return i;
			}
		} catch (ParseException e) {
			logger.debug(e.toString());
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		return null;
	}

    /**
     * get all the investments for opp id
     * @param con
     * @param id opp id
     * @param showInusrance true if u want to show all the investments,
     *                      false if u want to get only investments that wasnt bought with insurance or sell with option +
     * @return list of investments
     * @throws SQLException
     */
	public static ArrayList<Investment> getAllInvestmentsByOppId(Connection con, long id, boolean showInusrance) throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		ArrayList<Investment> list = new ArrayList<Investment>();
		try {
			String sql = "SELECT " +
                            "i.* " +
                         "FROM " +
                            "investments i, " +
                            "opportunities o " +
                         "WHERE " +
                             "i.opportunity_id = ? AND " +
                             "o.id = i.opportunity_id ";
            if (!showInusrance) { //dont take investments that was settled b4 time thats mean gm or options plus
                      sql += "AND i.insurance_amount_gm = 0 AND " +
                             "i.time_settled > SYS_EXTRACT_UTC(o.time_est_closing) ";
            }
                      sql += "ORDER BY " +
                                 "i.user_id,i.id";
			ps = con.prepareStatement(sql);
			ps.setLong(1, id);
			rs = ps.executeQuery();
			while(rs.next()) {
				list.add(getVO(rs));
			}
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		return list;
	}

	public static void cancelInvestment(Connection con, long id, long writerId, boolean isSettled) throws SQLException {
		PreparedStatement ps = null;

		try {
			String sql;

			if (isSettled) {
				sql = "update investments i set i.is_settled=1,i.is_canceled=1,i.time_canceled=sysdate,i.utc_offset_cancelled=(select utc_offset from users u where i.user_id=u.id) ,canceled_writer_id=? " + " where id=?";
			} else { // open investment
				sql =
					"UPDATE " +
						"investments i " +
					"SET " +
						"i.time_settled = sysdate, " +
                        "i.time_settled_year = EXTRACT(YEAR FROM GET_TIME_BY_PERIOD(sysdate)), " +
                        "i.time_settled_month = EXTRACT(MONTH FROM GET_TIME_BY_PERIOD(sysdate)), " +
						"i.house_result = 0, " +
						"i.is_settled = 1, " +
						"i.is_canceled = 1, " +
						"i.time_canceled = sysdate, " +
						"i.utc_offset_cancelled = (SELECT utc_offset FROM users u WHERE i.user_id = u.id), " +
						"canceled_writer_id = ? " +
					"WHERE " +
						"id = ?";
			}

			ps = con.prepareStatement(sql);

			ps.setLong(1, writerId);
			ps.setLong(2, id);

			ps.executeUpdate();

		} finally {
			closeStatement(ps);
		}
	}

	/**
	 * List all the investments done on opportunity that is closed and are not
	 * settled, canceled or void.
	 *
	 * @param conn the db connection to use
	 * @return
	 * @throws SQLException
	 */
	public static ArrayList<Integer> getInvestmentsToSettle(Connection conn) throws SQLException {
		ArrayList<Integer> l = new ArrayList<Integer>();
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			String sql =
					"SELECT " +
                        "A.id " +
                    "FROM " +
                        "investments A, " +
                        "opportunities B " +
                    "WHERE " +
                        "A.opportunity_id = B.id AND " +
                        "NOT B.time_act_closing IS NULL AND " +
                        "B.opportunity_type_id in (" + Opportunity.TYPE_REGULAR + "," + Opportunity.TYPE_OPTION_PLUS + ","
                        								+ Opportunity.TYPE_DYNAMICS + "," + Opportunity.TYPE_ONE_TOUCH + ","
                        								+ Opportunity.TYPE_BINARY_0_100_ABOVE + "," + Opportunity.TYPE_BINARY_0_100_BELOW
                        								+ ") AND " +
                        "A.is_settled = 0 AND " +
                        "A.is_canceled = 0 AND " +
                        "A.is_void = 0";
			pstmt = conn.prepareStatement(sql);
			rs = pstmt.executeQuery();
			while(rs.next()) {
				l.add(rs.getInt("id"));
			}
		} finally {
			closeResultSet(rs);
			closeStatement(pstmt);
		}
		return l;
	}

    /**
     * List all the investments done on opportunity that is closed and are not
     * settled, canceled or void.
     *
     * @param conn the db connection to use
     * @param investmentId the id of the investment to load
     * @param oppSettled true if u want to get the investment only if the opp is closed, false if u want also not closed opp (use for 5 golden minutes)
     * @param isGM if its take profit (GM) true else false
     * @return
     * @throws SQLException
     */
    public static Investment getInvestmentToSettle(Connection conn, long investmentId, boolean oppSettled, boolean isGM, Long userId) throws SQLException {
        Investment i = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            String sql =
                    "SELECT " +
                        "A.*, ";
                        if (isGM) { //if we want to settle take profit investment (GM) we should set the closing level from opportunity.gm_level
                            sql += "B.gm_level as closing_level, ";
                        } else {
                            sql += "B.closing_level, ";
                        }
                 sql += "M.id as market_id, " +
                        "M.insurance_premia_percent, " +
                        "M.roll_up_premia_percent, " +
                        "M.display_name m_display_name, " +
                        "M.decimal_point m_decimal_point, " +
                        "it.display_name i_type_name, " +
                        "to_char(B.time_est_closing, 'YYYY-MM-DD HH24:MI:SS.FF3 TZHTZM') AS time_est_closing, " +
                        "bu.odds_win as bonus_odds_win, " +
                        "bu.odds_lose as bonus_odds_lose, " +
                        "b.opportunity_type_id, " +
                        "b.scheduled scheduled, " +
                        "b.current_level as opp_current_level " +
                    "FROM " +
                        "investments A left join bonus_users bu on A.bonus_user_id = bu.id, " +
                        "opportunities B, " +
                        "markets M, " +
                        "investment_types it " +
                    "WHERE " +
                        "A.opportunity_id = B.id AND "+
                        "M.id = B.market_id AND " +
                        "A.type_id = it.id AND ";
            if (oppSettled) {
                sql +=  "NOT ";
            }
            sql +=      "B.time_act_closing IS NULL AND " +
                        "A.is_settled = 0 AND " +
                        "A.is_canceled = 0 AND " +
                        "A.is_void = 0 AND " +
                        "A.id = ? ";
            if (null != userId) {
            	sql +=  "AND A.user_id = ?";
            }
            pstmt = conn.prepareStatement(sql);
            pstmt.setLong(1, investmentId);
            if (null != userId) {
            	pstmt.setLong(2, userId.longValue());
            }
            rs = pstmt.executeQuery();
            while(rs.next()) {
                i = getVO(rs);
               
                i.setClosingLevel(rs.getDouble("closing_level"));
                i.setBonusUserId(rs.getLong("bonus_user_id"));
                i.setMarketId(rs.getLong("market_id"));
                i.setInsurancePremiaPercent(rs.getDouble("insurance_premia_percent"));
                i.setRollUpPremiaPercent(rs.getDouble("roll_up_premia_percent"));
                i.setMarketName(rs.getString("m_display_name"));
                i.setDecimalPoint(rs.getLong("m_decimal_point"));
                i.setTypeName(rs.getString("i_type_name"));
                i.setBonusLoseOdds(rs.getDouble("bonus_odds_lose"));
                i.setBonusWinOdds(rs.getDouble("bonus_odds_win"));
                SimpleDateFormat localDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS Z");
                try {
					i.setTimeEstClosing(localDateFormat.parse(rs.getString("time_est_closing")));
				} catch (ParseException e) {
					logger.error("Problem parsing timeEstClosing! " + e);
				}
				i.setOpportunityTypeId(rs.getLong("opportunity_type_id"));
				i.setOppCurrentLevel(rs.getDouble("opp_current_level"));
				i.setApiExternalUserId(rs.getLong("api_external_user_id"));
				i.setScheduledId(rs.getLong("scheduled"));
            }
        } finally {
            closeResultSet(rs);
            closeStatement(pstmt);
        }
        return i;
    }

//    /**
//     * Return whether a one touch opportunity related to this investmentis up or down
//     * @param con - the connection string
//     * @param investmentId - the investment id
//     * @return 1 - up, 0 - down, -1 - not one touch
//     */
//    public static long getOneTouchInvestmentUpDown(Connection con, long investmentId) throws SQLException {
//    	long isUpDown = -1;
//    	PreparedStatement pstmt = null;
//    	ResultSet rs = null;
//    	try {
//    		String sql = "SELECT " +
//    						"up_down " +
//    					"FROM " +
//    						"investments i, opportunity_one_touch oot " +
//    					"WHERE " +
//    						"i.opportunity_id = oot.opportunity_id AND " +
//    						"i.id = ?";
//    		pstmt = con.prepareStatement(sql);
//    		pstmt.setLong(1, investmentId);
//    		rs = pstmt.executeQuery();
//    		if (rs.next()) {
//    			isUpDown = rs.getLong("up_down");
//    		}
//    	} finally {
//			closeResultSet(rs);
//			closeStatement(pstmt);
//		}
//		return isUpDown;
//    }

	/**
	 * Get the current win/lose amount for specified user. In the calculation
	 * are taken the investments done during the current year and are settled.
	 *
	 * @param conn the db connection to use
	 * @param userId the id of the user to calculate for
	 * @return The current win/lose amount for the user for the current year.
	 * @throws SQLException
	 */
	public static long getTotalWinLoseForTheYear1(Connection conn, long userId, boolean isPayTax,String utcOffset) throws SQLException {
		long wl = 0;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		String dateDelta = "";

		if(utcOffset.equals(ConstantsBase.UTCOFFSET_DEFAULT_PERIOD2)){
			dateDelta = ConstantsBase.GMT_2_OFFSET_DELTA;
		}else if(utcOffset.equals(ConstantsBase.UTCOFFSET_DEFAULT_PERIOD1)){
			dateDelta = ConstantsBase.GMT_3_OFFSET_DELTA;
		}

		try {
			String sql =
					" SELECT " +
					    " SUM(i.win) - SUM(i.lose) + SUM(NVL(t.amount,0)) AS win_lose " +
                    " FROM " +
                        " investments i left join transactions t on t.bonus_user_id = i.bonus_user_id and i.bonus_odds_change_type_id > 1 " +
                    " WHERE " +
                        " i.user_id = ? AND " +  /*i change time_created->time_settled*/
                        " i.time_settled_year = EXTRACT(YEAR FROM sysdate " + dateDelta + ") AND " +
                        " i.is_settled = 1 AND " +
                        " i.is_canceled = 0 AND " +
                        " not (i.bonus_odds_change_type_id = 1 and i.lose > 0)"; //do not include "next invest on us" investments that have lost

			if ( isPayTax ) {  // user already paid tax for the mid-year
				sql += " AND i.time_settled_month >= " + ConstantsBase.TAX_JOB_PERIOD1_END_PART_MONTH.substring(1);
			}
			pstmt = conn.prepareStatement(sql);
			pstmt.setLong(1, userId);
			rs = pstmt.executeQuery();
			if (rs.next()) {
				wl = rs.getLong("win_lose");
			}
		} finally {
			closeResultSet(rs);
			closeStatement(pstmt);
		}
		return wl;
	}

	/**
	 * Update the user balances when settle investment.
	 *
	 * @param conn
	 *            the db connection to use
	 * @param userId
	 *            the id of the user who did the investment
	 * @param result
	 *            the result of the investment - the amount in cents won/lost
	 *            from this investment(to be added to/taken from the balance).
	 *            Positive amount means winning, negative losing. If a positive
	 *            amount is passed it is expected to include the invested amount
	 *            also (not net win).
	 * @param tax
	 *            the tax balance amount after the settlement
	 * @throws SQLException
	 */
	public static void settleInvestmentUpdateUserBalances(Connection conn, long userId, long result, long tax, long winLose) throws SQLException {
		PreparedStatement pstmt = null;
		try {
			String sql =
                    "UPDATE " +
                        "users " +
                    "SET " +
                        "tax_balance = CASE WHEN tax_exemption = 1 THEN 0 ELSE ? END, " +
                        "balance = balance + tax_balance + ? - CASE WHEN tax_exemption = 1 THEN 0 ELSE ? END, " +
                        "win_lose = ? " +
                    "WHERE " +
                        "id = ?";
			pstmt = conn.prepareStatement(sql);
			pstmt.setLong(1, tax);
			pstmt.setLong(2, result);
            pstmt.setLong(3, tax);
            pstmt.setLong(4, winLose);
			pstmt.setLong(5, userId);
			pstmt.executeUpdate();
		} finally {
			closeStatement(pstmt);
		}
	}

	/**
	 * Update an investment to settled state.
	 *
	 * @param conn
	 *            the db connection to use
	 * @param userId
	 *            the id of the user who did the investment
	 * @param investmentId
	 *            the id of the investment to settle
	 * @param result
	 *            the result of the investment - the amount in cents won/lost
	 *            from this investment. Positive amount means winning, negative
	 *            losing. If a positive amount is passed it is expected to
	 *            include the invested amount also (not net win).
	 * @param netResult
	 *            the clean win/lose from this investment in cents (if win it
	 *            should not include the amount invested only the profit)
	 * @param closingLevel
	 * @param shouldUpdateTimeSettled
     * @param insuranceAmount gm insurance amount
	 * @throws SQLException
	 */
	public static void settleInvestment(Connection conn, long userId, long investmentId, long result, boolean voidBet, long insuranceAmountGM,
			long win, long lose, Double closingLevel, Date timeQuoted, Date now, boolean shouldUpdateTimeSettled) throws SQLException {
		PreparedStatement pstmt = null;
		try {
			String sql =
			        " UPDATE " +
			            " investments i " +
                    " SET " +
                        " i.win = ?, " +
                        " i.lose = ?, " +
                        " i.house_result = ?, " +
                        " i.utc_offset_settled = (select utc_offset from users u where i.user_id = u.id), " + //utc_offset is taken from the user of the investment
                        " i.is_settled = 1, " +
                        " i.is_void = ?, " +
                        " i.insurance_amount_gm = ?, " +
                        " i.amount = i.amount + ?, " +    // Add insuranceAmountGM for GM investments(if it's not GM it will be amount + 0)
                        " i.settled_level = ?, " +
                        " i.time_quoted = ? ";
           if (shouldUpdateTimeSettled) {
                 sql += " ,i.time_settled = ?, " +
		                " i.time_settled_year = EXTRACT(YEAR FROM GET_TIME_BY_PERIOD(?)), " +
		                " i.time_settled_month = EXTRACT(MONTH FROM GET_TIME_BY_PERIOD(?)) ";
           }
                 sql += " WHERE " +
                        " i.id = ?"
                        + "and i.is_settled = 0";
            int i = 1;
			pstmt = conn.prepareStatement(sql);
			pstmt.setLong(i++, win);
			pstmt.setLong(i++, lose);
			pstmt.setLong(i++, -(win-lose));
			pstmt.setBoolean(i++, voidBet);
            pstmt.setDouble(i++, insuranceAmountGM);
            pstmt.setLong(i++, insuranceAmountGM);
            pstmt.setDouble(i++, closingLevel);
			pstmt.setTimestamp(i++, CommonUtil.convertToTimeStamp(timeQuoted));
			if (shouldUpdateTimeSettled) {
				Timestamp t = CommonUtil.convertToTimeStamp(null != now ? now : new Date());
				pstmt.setTimestamp(i++, t);
				pstmt.setTimestamp(i++, t);
				pstmt.setTimestamp(i++, t);
			}
			pstmt.setLong(i++, investmentId);

			if (logger.isDebugEnabled()) {
	            String ls = System.getProperty("line.separator");
	            logger.log(Level.DEBUG, ls +
	            		"SettleInvestment SQL: " + sql + ls +
	            		"Values: " + ls +
	            		"win: " + win + ls +
	            		"lose: " + lose + ls +
	            		"houseResult: " + -(win-lose) + ls +
	            		"is_void: " + (voidBet) + ls +
	            		"investment id: " + investmentId + ls +
                        "insurance Amount GM: " + insuranceAmountGM + ls);
			}

			int updateCount = pstmt.executeUpdate();
			if (updateCount == 0) {
				throw new SQLException("No rows updated! Probably the user tries to settle investment twice");
			}
		} finally {
			closeStatement(pstmt);
		}
	}

	 /**
		 * check if the investment settled in Mid-year.
		 * @param con
		 * 			the db connection to use
		 * @param invId
		 * 			the id of the investment
		 * @return boolean
		 * 			true if the investment settled in Mid-year
		 * 			false if the investment doesnt settled in Mid-year
		 * @throws SQLException
		 */
		 public static boolean isSettleInMidYear(Connection con, long invId, String year) throws SQLException {

		  		PreparedStatement ps = null;
				ResultSet rs = null;

				String from = ConstantsBase.TAX_JOB_PERIOD1_START_PART + year;
				String to = ConstantsBase.TAX_JOB_PERIOD1_END_PART + year;

				try
				{
				    String sql="select * from investments "+
				    		   "where id=? and time_settled >= to_date('"+from+"','DDMMYYYY') "+
				    		   "and time_settled <= to_date('"+to+"','DDMMYYYY')";

				    ps = con.prepareStatement(sql);
					ps.setLong(1, invId);
					rs = ps.executeQuery();

					if ( rs.next() ) {  //found this investment
						return true;
					}
				}
				finally
				{
					closeResultSet(rs);
					closeStatement(ps);
				}
				return false;
		  	}
	public static long getSumAllActiveInvestmentsByOppId(Connection con,long id) throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		long sumInvestments = 0;
		try {
			//String sql = " select sum(i.amount) as total_investment from investments i where i.OPPORTUNITY_ID=? and i.is_canceled = 0 order by i.user_id,i.id";
			String sql ="select sum((i.amount*i.rate)) as total_investment "+
						"from investments i, users u "+
						"where i.OPPORTUNITY_ID=? and i.is_canceled = 0 and u.id=i.user_id and u.class_id<>0 "+
						"order by i.user_id,i.id";

			ps = con.prepareStatement(sql);
			ps.setLong(1, id);
			rs = ps.executeQuery();
			while(rs.next()) {
				sumInvestments = rs.getLong("total_investment");
			}
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		return sumInvestments;
	}

	/**
	 * function that return oneTouchUpDown value for some one touch investment(type 3).
	 * @param con
	 * @param oppId
	 * 		opportunity id
	 * @throws SQLException
	 */
	public static int getOneTouchUpDownByOpp(Connection con, long oppId) throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		int upDown = -1;
		try {
			String sql =
				"select op.up_down " +
				"from opportunities o, opportunity_one_touch op " +
				"where o.id = ? and o.id = op.opportunity_id " +
				"and o.opportunity_type_id = ? ";


			ps = con.prepareStatement(sql);
			ps.setLong(1,oppId);
			ps.setLong(2, Opportunity.TYPE_ONE_TOUCH );
			rs = ps.executeQuery();
			if (rs.next()) {
				upDown = rs.getInt("up_down");
			}
		} catch (Exception e) {
			logger.error(e.toString());
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		return upDown;
	}

	public static int getOneTouchDecimalPoint(Connection con, long oppId) throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		int decimal = 0;
		try {
			String sql =
				"select op.decimal_point " +
				"from opportunities o, opportunity_one_touch op " +
				"where o.id = ? and o.id = op.opportunity_id " +
				"and o.opportunity_type_id = ? ";
			ps = con.prepareStatement(sql);
			ps.setLong(1,oppId);
			ps.setLong(2, Opportunity.TYPE_ONE_TOUCH );
			rs = ps.executeQuery();
			if (rs.next()) {
				decimal = rs.getInt("decimal_point");
			}
		} catch (Exception e) {
			logger.error(e.toString());
		} finally{
			closeResultSet(rs);
			closeStatement(ps);
		}
		return decimal;
	}

	public static OneTouchFields getOneTouchFields(Connection con, long oppId) throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		int decimal = 0;
		int up_down = 0;
		try {
			String sql =
				"select op.decimal_point,op.up_down " +
				"from opportunities o, opportunity_one_touch op " +
				"where o.id = ? and o.id = op.opportunity_id " +
				"and o.opportunity_type_id = ? ";
			ps = con.prepareStatement(sql);
			ps.setLong(1,oppId);
			ps.setLong(2, Opportunity.TYPE_ONE_TOUCH );
			rs = ps.executeQuery();
			if (rs.next()) {
				decimal = rs.getInt("decimal_point");
				up_down = rs.getInt("up_down");
			}
		} catch (Exception e) {
			logger.error(e.toString());
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		OneTouchFields retVal = new  OneTouchFields();
		retVal.setDecimalPoint(decimal);
		retVal.setUpDown(up_down);
		return retVal;
	}

    public static HashMap<String, Integer> getInsuranceTime(Connection conn) throws SQLException {
        HashMap<String, Integer> l = new HashMap<String, Integer>();
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            String sql = "SELECT " +
                             "* " +
                         "FROM " +
                             "enumerators e " +
                         "WHERE " +
                             "enumerator LIKE 'insurance_period_%' " + 
                         "ORDER BY " +
                             "enumerator";
            pstmt = conn.prepareStatement(sql);
            rs = pstmt.executeQuery();
            while (rs.next()) {
                l.put(rs.getString("enumerator"), rs.getInt("value"));
            }
        } finally {
            closeResultSet(rs);
            closeStatement(pstmt);
        }

        return l;
    }

    /**
     * Check if user have investments
     * @param con
     * @param userId
     * @return
     * @throws SQLException
     */
    public static boolean hasInvestments(Connection con, long userId, boolean oneTouch) throws SQLException{
    	PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {
			String sql ="SELECT " +
							"count(*) num_inv " +
						"FROM " +
							"investments " +
						"WHERE " +
							"user_id=" + userId;

			pstmt = con.prepareStatement(sql);
			rs = pstmt.executeQuery();
			if(rs.next()) {
				if (oneTouch && rs.getLong("num_inv") > 1){
					return true;
				} else if (!oneTouch && rs.getLong("num_inv") > 0){
					return true;
				}
			}
		} finally {
			closeResultSet(rs);
			closeStatement(pstmt);
		}
		return false;
    }

    /**
     * Load the <code>InvestmentLimit</code> VO details from <code>ResultSet</code>.
     * The result set should be positioned at the record to load.
     *
     * @param rs the <code>ResultSet</code> to read the VO details from
     * @return New <code>InvestmentLimit</code> VO with the details loaded.
     * @throws SQLException
     */
	protected static InvestmentLimit getInvestmentLimitVo(ResultSet rs) throws SQLException {

		InvestmentLimit vo = new InvestmentLimit();
		vo.setId(rs.getLong("ID"));
		vo.setInvLimitTypeId(rs.getInt("LIMIT_TYPE_ID"));
		vo.setOpportunityTypeId(rs.getLong("OPPORTUNITY_TYPE_ID"));
		vo.setScheduled(rs.getLong("SCHEDULED"));
		vo.setUserId(rs.getLong("USER_ID"));
		vo.setMarketId(rs.getLong("MARKET_ID"));
		vo.setStartDate(convertToDate(rs.getTimestamp("START_DATE")));
		vo.setEndDate(convertToDate(rs.getTimestamp("END_DATE")));
		vo.setTimeCreated(convertToDate(rs.getTimestamp("TIME_CREATED")));
		vo.setWriterId(rs.getLong("WRITER_ID"));
		vo.setMinAmountGroupId(rs.getLong("MIN_LIMIT_GROUP_ID"));
		vo.setMaxAmountGroupId(rs.getLong("MAX_LIMIT_GROUP_ID"));
		vo.setActive(rs.getInt("IS_ACTIVE")==1?true:false);

		return vo;
	}

    public static InvestmentLimit getInvestmentLimit(Connection conn, Opportunity opp, UserBase user) throws SQLException {
        InvestmentLimit il = getInvestmentLimit(conn, opp.getOpportunityTypeId(), opp.getScheduled(), opp.getMarketId(), user.getCurrencyId(), user.getId());
        return il;
    }
    /**
     * Locate and load <code>InvestmentLimit</code> by group and currency.
     *
     * @param conn the db connection to use
     * @param investmentLimitsGroupId the investment limits group
     * @param currencyId the currency
     * @return The requested <code>InvestmentLimit</code>.
     * @throws SQLException
     */
    public static InvestmentLimit getInvestmentLimit(Connection conn, long oppTypeId, long oppScheduled, long oppMarketId, long userCurrencyId, long userId) throws SQLException {
        InvestmentLimit il = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;

        try {
            String sql =
                    " SELECT " +
                        " il.*, " +
                        " ilgcmin.value min_amount," +
                        " ilgcmax.value max_amount " +
                    " FROM " +
                        " investment_limits il," +
                        " investment_limit_types ilt," +
                        " INVESTMENT_LIMIT_GROUP_CURR ilgcmin, " +
                        " INVESTMENT_LIMIT_GROUP_CURR ilgcmax " +
                    " WHERE " +
                        " il.LIMIT_TYPE_ID = ilt.id " +
                        " AND il.MIN_LIMIT_GROUP_ID = ilgcmin.INVESTMENT_LIMIT_GROUP_ID " +
                        " AND ilgcmin.CURRENCY_ID = ? " +
                        " AND il.MAX_LIMIT_GROUP_ID = ilgcmax.INVESTMENT_LIMIT_GROUP_ID " +
                        " AND ilgcmax.CURRENCY_ID = ? " +
                        " AND il.IS_ACTIVE = 1 " +
                        " AND ilt.IS_ACTIVE = 1 " +
                        " AND (il.user_id is null or il.user_id = ?) " +
                        " AND (il.OPPORTUNITY_TYPE_ID is null or il.OPPORTUNITY_TYPE_ID = ?) " +
                        " AND (il.SCHEDULED is null or il.SCHEDULED = ?) " +
                        " AND (il.MARKET_ID is null or il.MARKET_ID = ?) " +
                        " AND (il.START_DATE is null " +
                        	  " or il.END_DATE is null" +
                        	  " or sysdate between il.START_DATE and il.END_DATE) " +
                        " AND (il.START_TIME is null " +
                              " OR il.END_TIME is null " +
                              " OR to_char(sysdate, 'hh24:mi') >= il.START_TIME " +
                              " OR to_char(sysdate, 'hh24:mi') < il.END_TIME) " +
                    " ORDER BY " +
                    	" ilt.priority ";

            pstmt = conn.prepareStatement(sql);
            pstmt.setLong(1, userCurrencyId);
            pstmt.setLong(2, userCurrencyId);
            pstmt.setLong(3, userId);
            pstmt.setLong(4, oppTypeId);
            pstmt.setLong(5, oppScheduled);
            pstmt.setLong(6, oppMarketId);

            rs = pstmt.executeQuery();
            if (rs.next()) {
                il = getInvestmentLimitVo(rs);
                il.setMinAmount(rs.getLong("min_amount"));
                il.setMaxAmount(rs.getLong("max_amount"));
            }
        } finally {
            closeResultSet(rs);
            closeStatement(pstmt);
        }
        return il;
    }

    /**
     * Locate and load <code>InvestmentLimitGroups</code>.
     *
     * @param con the db connection to use
     * @return The <code>InvestmentLimitsGroup</code> map.
     * @throws SQLException
     */
    public static HashMap<Long, InvestmentLimitsGroup> getInvestmentLimitGroupsMap(Connection con) throws SQLException {
    	HashMap<Long, InvestmentLimitsGroup> list = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            String sql =
                    " SELECT " +
                        " ilg.*," +
                        " ilgc.CURRENCY_ID, " +
                        " ilgc.VALUE, " +
                        " c.CODE CURR_CODE " +
                    " FROM " +
                        " INVESTMENT_LIMITS_GROUPS ilg, " +
                        " INVESTMENT_LIMIT_GROUP_CURR ilgc, " +
                        " CURRENCIES c " +
                    " WHERE " +
                        " ilg.ID = ilgc.INVESTMENT_LIMIT_GROUP_ID " +
                        " AND ilg.IS_ACTIVE = 1 " +
                        " AND ilgc.currency_id = c.id " +
                    " ORDER BY " +
                    	" ilg.GROUP_LEVEL, ilg.id , ilgc.CURRENCY_ID ";

            pstmt = con.prepareStatement(sql);
            rs = pstmt.executeQuery();

            long lastGroupId = 0;
            InvestmentLimitsGroup invLimitsGroup = null;
            String cDisplay = null;

            while (rs.next()) {
            	if (null == list){
            		list = new HashMap<Long, InvestmentLimitsGroup>();
            	}

            	long currGroupId = rs.getLong("ID");

            	if (currGroupId != lastGroupId) {
            		invLimitsGroup = new InvestmentLimitsGroup();
            		list.put(currGroupId, invLimitsGroup);
            		cDisplay = invLimitsGroup.getAmountPerCurrencyDisplay();

                 	invLimitsGroup.setId(currGroupId);
                 	invLimitsGroup.setGroupName(rs.getString("GROUP_NAME"));
                 	invLimitsGroup.setLevel(rs.getInt("GROUP_LEVEL"));
                 	invLimitsGroup.setActive(rs.getInt("IS_ACTIVE")==1?true:false);

                	lastGroupId = currGroupId;
            	}else{
            		cDisplay += ", ";
            	}

            	String currencyCode = rs.getString("CURR_CODE");
                long currencyId = rs.getLong("CURRENCY_ID");
                long value = rs.getLong("VALUE");
                invLimitsGroup.getAmountPerCurrency().put(currencyId, value);

                cDisplay += currencyCode + " - " + ((double)value/100);
                invLimitsGroup.setAmountPerCurrencyDisplay(cDisplay);
            }
        } finally {
            closeResultSet(rs);
            closeStatement(pstmt);
        }
        return list;
    }

	public static void insertQuote(Connection conn, long investmentId, Date timeQuoted, double price, double minPromile, double maxPromile, double minMinutes, double maxMinutes, double level) throws SQLException {
		PreparedStatement pstmt = null;
		try {
			String sql =
			        " INSERT INTO " +
			            " OPTIONS_PLUS_QUOTES(ID,TIME_CREATED,INVESTMENT_ID,TIME_QUOTED,PRICE,MIN_PROMILE,MAX_PROMILE,MIN_MINUTES,MAX_MINUTES,QUOTE_LEVEL) " +
                    " VALUES (SEQ_OPTIONS_PLUS_QUOTES.NEXTVAL,sysdate,?,?,?,?,?,?,?,?) ";
			pstmt = conn.prepareStatement(sql);
			pstmt.setLong(1, investmentId);
			pstmt.setTimestamp(2, CommonUtil.convertToTimeStamp(timeQuoted));
			pstmt.setDouble(3, price);
			pstmt.setDouble(4, minPromile);
			pstmt.setDouble(5, maxPromile);
			pstmt.setDouble(6, minMinutes);
			pstmt.setDouble(7, maxMinutes);
			pstmt.setDouble(8, level);
			pstmt.executeUpdate();
		} finally {
			closeStatement(pstmt);
		}
	}

    public static double calcOptionsPlusePrice(Connection conn, double promil, double minPass, long marketId, long investmentId, Date timeQuoted, BigDecimal invAmount, double level) throws SQLException {
        double price = 0;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            String sql =
                " SELECT " +
                    " opp.price price_percent, " +
                    " pr.min_promile, " +
                    " pr.max_promile, " +
                    " mi.min_minutes, " +
                    " mi.max_minutes " +
                " FROM " +
                    " options_plus_prices opp, " +
                    " options_plus_minutes_range mi, " +
                    " options_plus_promile_range pr " +
                " WHERE " +
                    " opp.minutes_range_id = mi.id  " +
                    " AND mi.market_id = ? " +
                    " AND ? between mi.min_minutes AND mi.max_minutes " +
                    " AND opp.promile_range_id = pr.id " +
                    " AND pr.market_id = ? " +
                    " AND ? between pr.min_promile AND pr.max_promile ";

            pstmt = conn.prepareStatement(sql);
            pstmt.setLong(1, marketId);
            pstmt.setDouble(2, minPass);
            pstmt.setLong(3, marketId);
            pstmt.setDouble(4, promil);

            rs = pstmt.executeQuery();
            if (rs.next()) {
            	double pricePercent = rs.getDouble("price_percent");

                Random r = new Random();
                double rnd = (r.nextDouble() - 0.5);
                BigDecimal bRnd = new BigDecimal(String.valueOf(rnd)).setScale(1, RoundingMode.HALF_UP);
                logger.debug("option plus random is : " + bRnd.doubleValue());
            	BigDecimal p = (new BigDecimal(String.valueOf(pricePercent)).multiply(invAmount)).add(bRnd);
                price = p.round(new MathContext(p.precision() - p.scale() + 2, RoundingMode.HALF_UP)).doubleValue();

                insertQuote(conn, investmentId, timeQuoted, price, rs.getDouble("min_promile"),
                		rs.getDouble("max_promile"), rs.getDouble("min_minutes"),
                		rs.getDouble("max_minutes"), level);
            }else{
            	logger.error("Couldn't find price for invId: " + investmentId + ", promil: " + promil + ", min pass: " + minPass +
            			", time quoted: " + timeQuoted);
            }
        } finally {
            closeResultSet(rs);
            closeStatement(pstmt);
        }
        return price;
    }

    public static OptionPlusQuote getLastQuote(Connection conn, long investmentId, double price, Date sellTime) throws SQLException {

    	PreparedStatement pstmt = null;
        ResultSet rs = null;
        OptionPlusQuote q = null;
        try {
            String sql =
                " SELECT " +
                    " opq.id quote_id, " +
                    " opq.quote_level, " +
                    " opq.time_quoted, " +
                    " opq.time_quoted + (select dp.num_value from DB_PARAMETERS dp where dp.name = 'quote_time') time_quote_expired " +
                " FROM " +
                    " options_plus_quotes opq " +
                " WHERE " +
                    " opq.investment_id = ? " +
                    " AND opq.price = ? " +
                " ORDER BY "  +
                	" opq.time_quoted desc";

            pstmt = conn.prepareStatement(sql);
            pstmt.setLong(1, investmentId);
            pstmt.setDouble(2, price);

            rs = pstmt.executeQuery();
            if (rs.next()) {
            	q = new OptionPlusQuote();
                q.setId(rs.getLong("quote_id"));
                q.setQuoteLevel(rs.getDouble("quote_level"));
                q.setTimeQuoted(convertToDate(rs.getTimestamp("time_quoted")));
                q.setTimeQuoteExpired(convertToDate(rs.getTimestamp("time_quote_expired")));
            }
        } finally {
            closeResultSet(rs);
            closeStatement(pstmt);
        }
        return q;
    }

	public static void updatePurchasedQuote(Connection conn, long quoteId) throws SQLException {
		PreparedStatement pstmt = null;
		try {
			String sql =
			        " Update " +
			            " OPTIONS_PLUS_QUOTES opq " +
                    " Set " +
                    	" opq.IS_PURCHASED = 1 " +
                    " Where " +
                    	" opq.id = ? ";
			pstmt = conn.prepareStatement(sql);
			pstmt.setLong(1, quoteId);
			pstmt.executeUpdate();
		} finally {
			closeStatement(pstmt);
		}
	}

	public static void updateTimeSettledBinary0100(Connection conn, Date now, long investmentId) throws SQLException {
		PreparedStatement pstmt = null;
		try {
			String sql =
				"UPDATE " +
				"  investments i2 " +
				"SET " +
				"  i2.time_settled = ?, " +
				"  i2.time_settled_year = EXTRACT(YEAR FROM GET_TIME_BY_PERIOD(?)), " +
				"  i2.time_settled_month = EXTRACT(MONTH FROM GET_TIME_BY_PERIOD(?)) " +
				"WHERE " +
				"  EXISTS (SELECT " +
				"			1 " +
				"		   FROM " +
				"			investments i " +
				"		   WHERE " +
				"			i.id = ? AND " +
				"  			i.user_id = i2.user_id AND " +
				"  			i.opportunity_id = i2.opportunity_id AND " +
				"  			i.type_id = i2.type_id AND " +
				"  			i.amount = i2.amount AND " +
				"  			i.current_level = i2.current_level AND " +
				"  			i.time_created = i2.time_created) AND " +
				"  i2.id != ? AND " +
                "  i2.is_settled = 0 AND " +
                "  i2.is_canceled = 0 AND " +
                "  i2.is_void = 0";
			pstmt = conn.prepareStatement(sql);
			Timestamp t = CommonUtil.convertToTimeStamp(now);
			pstmt.setTimestamp(1, t);
			pstmt.setTimestamp(2, t);
			pstmt.setTimestamp(3, t);
			pstmt.setLong(4, investmentId);
			pstmt.setLong(5, investmentId);
			pstmt.executeUpdate();
		} finally {
			closeStatement(pstmt);
		}
	}
	
	/**
	 * Binary0100.
	 * This function get investment id and return an array of all the investment id that connected to the given id.
	 * The array includes the given id.
	 * @param con
	 * @param invId
	 * @return
	 * @throws SQLException
	 */
	public static ArrayList<Long> getInvestmentsConnectedIDByInvId(Connection con, long invId) throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		ArrayList<Long> list = new ArrayList<Long>();
		try {
			String sql = 
						" SELECT " +
								" i.id " +
						" FROM " +
								" investments i, " +
								" ( " +
									" select " +
										" i2.time_created, " +
										" i2.opportunity_id, " +
										" i2.type_id, " +
										" i2.amount, " +
										" i2.current_level, " +
										" i2.group_inv_id " +
									" from " +
										" investments i2 " +
									" where " +
										" i2.id = ? " +
								" ) i3 " +
						" WHERE " +
							" i3.time_created = i.time_created and " +
							" i3. opportunity_id = i. opportunity_id and " +
							" i3.type_id = i.type_id and " +
							" i3.amount = i.amount and " +
							" i3.current_level= i.current_level and " +
							" i3.group_inv_id = i.group_inv_id " +
						" ORDER BY " +
							" i.id ";
				           
			ps = con.prepareStatement(sql);
			ps.setLong(1, invId);
			rs = ps.executeQuery();
			while (rs.next()) {
				list.add(rs.getLong("id"));
			}
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		return list;
	}
    
    /**
     * @param conn
     * @param userId
     * @param bonusActivated
     * @return
     * @throws SQLException
     */
    public static long getInvestmentAmountOnTimeFrame(Connection conn, long userId, Date bonusActivated) throws SQLException {
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        
        try {
            String sql =
            	"SELECT " +
            	"	sum(i.amount) as investments_amount " +
            	"FROM " +
            	"	investments i " +
            	"WHERE " +
            	"	i.user_id = ? AND " +
                " 	i.is_settled = 1 AND " +
                " 	i.is_canceled = 0 AND " +
            	"	i.time_created between to_date(?, 'yyyy-MM-dd HH24:MI:SS') AND ADD_MONTHS(to_date(?, 'yyyy-MM-dd HH24:MI:SS'), " + +ConstantsBase.BONUS_MANAGEMENT_TIME_FRAME + ")"; 
            pstmt = conn.prepareStatement(sql);
            pstmt.setLong(1, userId);
            pstmt.setString(2, dateFormat.format(bonusActivated));
            pstmt.setString(3, dateFormat.format(bonusActivated));
            rs = pstmt.executeQuery();
            
            if (rs.next()) {
                return rs.getLong("investments_amount");
            }
        } finally {
            closeResultSet(rs);
            closeStatement(pstmt);
        }
        return 0;
    }
    
    /**
     * @param conn
     * @param userId
     * @param bonusActivated
     * @return
     * @throws SQLException
     */
    public static long getHouseResultOnTimeFrame(Connection conn, long userId, Date bonusActivated) throws SQLException {
        PreparedStatement pstmt = null;
        ResultSet rs = null; 
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        
        try {
            String sql =
            	"SELECT " +
            	"	sum(i.house_result) as house_results " +
            	"FROM " +
            	"	investments i " +
            	"WHERE " +
            	"	i.user_id = ? AND " +
                " 	i.is_settled = 1 AND " +
                " 	i.is_canceled = 0 AND " +
            	"	i.time_created between to_date(?, 'yyyy-MM-dd HH24:MI:SS') AND ADD_MONTHS(to_date(?, 'yyyy-MM-dd HH24:MI:SS'), " + +ConstantsBase.BONUS_MANAGEMENT_TIME_FRAME + ")";
            pstmt = conn.prepareStatement(sql);
            pstmt.setLong(1, userId);
            pstmt.setString(2, dateFormat.format(bonusActivated));
            pstmt.setString(3, dateFormat.format(bonusActivated));
            rs = pstmt.executeQuery();
            
            if (rs.next()) {
                return rs.getLong("house_results");
            }
        } finally {
            closeResultSet(rs);
            closeStatement(pstmt);
        }
        return 0;
    }
    
    /**
     * @param conn
     * @param userId
     * @param bonusActivated
     * @return
     * @throws SQLException
     */
    public static ArrayList<Long> getInvestmentAmountAndHouseResultOnTimeFrame(Connection conn, long userId, Date bonusActivated) throws SQLException {
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        ArrayList<Long> inv = new ArrayList<Long>(); 
        try {
            String sql =
            	"SELECT " +
            	"	sum(i.amount) as investments_amount, " +
            	"	sum(i.house_result) as house_results " +
            	"FROM " +
            	"	investments i, " +
            	"WHERE " +
            	"	i.user_id = ? " +
            	"	i.time_created between ADD_MONTHS(?, " + -ConstantsBase.BONUS_MANAGEMENT_TIME_FRAME + ") to ? ";
            
            pstmt = conn.prepareStatement(sql);
            pstmt.setLong(1, userId);
            pstmt.setTimestamp(2, CommonUtil.convertToTimeStamp(bonusActivated));
            rs = pstmt.executeQuery();
            
            if (rs.next()) {
                inv.add(rs.getLong("investments_amount"));
                inv.add(rs.getLong("house_results"));
            }
        } finally {
            closeResultSet(rs);
            closeStatement(pstmt);
        }
        return inv;
    }

    /**
     * Get 5 last top trades for LIVE AO page (last day/week/month)
     * @param con
     * @param fromDate
     * @param tillDate
     * @return
     * @throws SQLException
     */
    public static ArrayList<Investment> getLastTopTrades(Connection con, String fromDate, String tillDate, long skinBusinessCaseId) throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		ArrayList<Investment> list = new ArrayList<Investment>();
		Investment inv;
		try {
			String sql = "SELECT " +
							" * " +
						" FROM " +
							" (SELECT " +
								" u.skin_id, " +
								" u.country_id, " +
								" country.display_name country_diplay_name, " +
								" oppt.name as opportunity_type_name, " +
								" m.display_name as market_display_name, " +
								" op.closing_level, " +
								" inv.opportunity_id as opportunity_id, " +
								" u.id as userId, " +
								" city.google_city_name as cityName, " +
								" op.opportunity_type_id, " +
								" op.time_est_closing as timeEstClosing, " +
								" SUM((((inv.house_result * inv.rate) * (-1)))) as profit_user " +
							" FROM " +
								" investments inv, " +
								" opportunities op, " +
								" opportunity_types opt, " +
								" opportunity_product_types oppt, " +
								" markets m, " +
				                " users u LEFT JOIN  countries country ON country.id = u.country_id " +
				                "   	  	LEFT JOIN cities_google_coordinates city ON UPPER(u.city_name) = UPPER(city.user_city_name) AND UPPER(country.country_name) = UPPER(city.user_country_name) " +
							" WHERE " +
								" inv.opportunity_id = op.id AND " +
								" op.opportunity_type_id = opt.id AND " +
								" op.market_id = m.id AND " +
								" inv.user_id = u.id AND " +
								" inv.is_settled = 1 AND " +
								" inv.is_canceled = 0 AND " +
								" op.is_settled = 1 AND " + //we want to take only settled opportunities
								" oppt.id = opt.product_type_id AND " +
								" op.time_est_closing >= to_date('" + fromDate + "','DD/MM/YYYY') AND " +
								" op.time_est_closing < to_date('" + tillDate + "','DD/MM/YYYY') AND " +
								" u.class_id <> 0 AND " +
								" m.exchange_id not in (?,?) AND " +
								" opt.product_type_id <> ? ";
			if (skinBusinessCaseId == Skin.SKIN_BUSINESS_ETRADER) {
				sql = sql + " and u.skin_id = " + Skin.SKIN_ETRADER + " ";
			}
			sql = sql +
							" GROUP BY " +
								" u.skin_id, " +
								" u.country_id, " +
								" country.display_name, " +
								" oppt.name, " +
								" m.display_name, " +
								" op.closing_level, " +
								" inv.opportunity_id, " +
								" u.id, " +
								" city.google_city_name, " +
								" op.opportunity_type_id, " +
								" op.time_est_closing " +
							" ORDER BY " +
								" profit_user DESC) " +
						" WHERE " +
							" rownum <= 5 ";
			ps = con.prepareStatement(sql);
			if (skinBusinessCaseId == Skin.SKIN_BUSINESS_ETRADER) {
				ps.setInt(1, -1);
				ps.setInt(2, -1);	
			} else {
				ps.setInt(1, Opportunity.ISRAEL_EXCHANGE);
				ps.setInt(2, Opportunity.USD_ILS_EXCHANGE);				
			}
			ps.setInt(3, OpportunityType.PRODUCT_TYPE_ONE_TOUCH);
			rs = ps.executeQuery();
			while(rs.next()) {
				inv = new Investment();
				inv.setUserId(rs.getLong("userId"));
				inv.setCountryId(rs.getLong("country_id"));
				inv.setCountryName(rs.getString("country_diplay_name"));
				inv.setTimeEstClosing(convertToDate(rs.getTimestamp("timeEstClosing")));
				inv.setNameOpportunityType(rs.getString("opportunity_type_name"));
				inv.setMarketName(rs.getString("market_display_name"));
				inv.setClosingLevel(rs.getDouble("closing_level"));
				inv.setProfitUser(rs.getLong("profit_user"));
				inv.setCityName(rs.getString("cityName"));
				inv.setOpportunityId(rs.getLong("opportunity_type_id"));
				inv.setOpportunityTypeId(rs.getLong("opportunity_id"));
				long skinIdSQL = rs.getLong("skin_id");
				inv.setSkin(String.valueOf(Skin.SKIN_BUSINESS_ANYOPTION));
				if (skinIdSQL == Skin.SKIN_BUSINESS_ETRADER) {
					inv.setSkin(String.valueOf(Skin.SKIN_BUSINESS_ETRADER));
				}
				list.add(inv);
			}
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		return list;
	}
    
    /**
     * Get copyop investment details
     * @param con
     * @param originalInvId
     * @return
     * @throws SQLException
     */
    public static CopyopInvestmentDetails getCopyopInvestmentDetails(Connection con, long originalInvId) throws SQLException {
        PreparedStatement ps = null;
        ResultSet rs = null;
        CopyopInvestmentDetails copyopinvestmentdetails = null;
        try {
            String sql =" SELECT " +
            			"	times_copied.*, " +
            			"	inv_info.*, " +
            			"	sum_amount.* " +
            			" FROM " +
            			// Count times copied
            			"	(SELECT " +
            			"		COUNT(*) num_times " +
            			" 	 FROM " +
            			"		investments i " +
            			"	 WHERE " +
            			"		i.copyop_inv_id = ?) times_copied, " +
            			// Original investment info
            			"	(SELECT " +
            			"	     u.id original_user_id, " +
            			"	     u.user_name original_user_name, " +
            			"	     i.current_level original_level, " +
            			"	     (i.rate * i.amount) original_amount_usd, " +
            			"	     i.amount original_amount," +
            			"		 u.currency_id original_currency_id " +
            			"	 FROM " +
            			"	     investments i, " +
            			"	     users u " +
            			"	WHERE " +
            			"	     u.id = i.user_id " +
            			"	     AND i.id = ?)inv_info, " +
            			// Sum original and all copied
            			"	(SELECT " +
            			"	   SUM(i.rate * i.amount) sum_amount_usd " +
            			"	 FROM " +
            			"      investments i " +
            			"	 WHERE " +
            			"       i.copyop_inv_id = ? " +
            			"		OR i.id = ?) sum_amount ";
            ps = con.prepareStatement(sql);
            ps.setLong(1, originalInvId);
            ps.setLong(2, originalInvId);
            ps.setLong(3, originalInvId);
            ps.setLong(4, originalInvId);
			rs = ps.executeQuery();
			while(rs.next()) {
				copyopinvestmentdetails = new CopyopInvestmentDetails();
				copyopinvestmentdetails.setTimesCopied(rs.getLong("num_times"));	
				copyopinvestmentdetails.setOriginalUserId(rs.getLong("original_user_id"));
				copyopinvestmentdetails.setOriginalUserName(rs.getString("original_user_name"));
				copyopinvestmentdetails.setOriginalRate(rs.getString("original_level"));
				copyopinvestmentdetails.setOriginalInvestmentAmount(rs.getLong("original_amount"));
				copyopinvestmentdetails.setOriginalInvestmentAmountUSD(rs.getLong("original_amount_usd"));
				copyopinvestmentdetails.setSumOriginalAndCopyUSD(rs.getLong("sum_amount_usd"));	
				copyopinvestmentdetails.setUserCurrencyId(rs.getLong("original_currency_id"));				
			}
        } finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
        return copyopinvestmentdetails;
    }
    
    /**
     * Get copypinvestment copy
     * @param con
     * @param originalInvId
     * @return
     * @throws SQLException
     */
    public static ArrayList<Pair> getCopyopInvestmentsCopy(Connection con, long originalInvId) throws SQLException {
        PreparedStatement ps = null;
        ResultSet rs = null;
        ArrayList<CopyopInvestmentDetails.Pair> list = new ArrayList<CopyopInvestmentDetails.Pair>();
        try {
            String sql =" SELECT " +
            			"	u.user_name, " +
            			"	i.id " +
            			" FROM " +
            			"    users u, " +
            			"    investments i " +
            			" WHERE " +
            			"    u.id = i.user_id " +
            			"    AND i.copyop_inv_id = ? " ;
            ps = con.prepareStatement(sql);
            ps.setLong(1, originalInvId);
			rs = ps.executeQuery();
			while(rs.next()) {				
				Pair pair = new Pair();
				pair.setUserName(rs.getString("user_name"));
				pair.setInvestmentId(rs.getLong("id"));
				list.add(pair);				
			}
			
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		return list;
	}
    
	public static Investment getDynamicsPartiallyClosedOriginalInvAmount(Connection con, Investment i) throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		Investment inv = new Investment();
		try {
			String sql = "SELECT i.amount FROM investments i " +
							"WHERE i.id = ? ";
			ps = con.prepareStatement(sql);
			ps.setLong(1, i.getReferenceInvestmentId());
			rs = ps.executeQuery();
			if (rs.next()) {
				inv.setAmount(rs.getLong("amount"));
				inv.setCurrencyId(i.getCurrencyId());
			}
			return inv;

		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
	}
}