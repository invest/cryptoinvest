package il.co.etrader.dao_managers;

//import il.co.etrader.bl_vos.Click;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import org.apache.log4j.Logger;

import com.anyoption.common.daos.DAOBase;


public class ClicksDAOBase extends DAOBase{

	  private static final Logger logger = Logger.getLogger(ClicksDAOBase.class);

		public static long insertClick(Connection con,long combId,String ip, String dynamicParam, Long affId, Long subAffId,
				String httpReferer, String dfaPlacmentId, String dfaCreativeId, String dfaMacro) throws SQLException {
			PreparedStatement ps = null;
			try {
				String sql =
						" insert into clicks(" +
							"id," +
							"combination_id," +
							"ip," +
							"time_created," +
							"dynamic_param, " +
							"affiliate_key, " +
							"sub_affiliate_key, " +
							"http_referer, " +
							"dfa_placement_id," +
							"dfa_creative_id," +
							"dfa_macro) " +
						                	"values(seq_clicks.nextval,?,?,sysdate,?,?,?,?,?,?,?)";

				ps = con.prepareStatement(sql);

				ps.setLong(1,combId);
				ps.setString(2,ip);
				ps.setString(3, dynamicParam);
				if (null != affId && affId > 0) {
					ps.setLong(4, affId);
				} else {
					ps.setString(4, null);
				}
				if (null != subAffId && subAffId > 0) {
					ps.setLong(5, subAffId);
				} else {
					ps.setString(5, null);
				}
				ps.setString(6, httpReferer);
				ps.setString(7, dfaPlacmentId);
				ps.setString(8, dfaCreativeId);
				ps.setString(9, dfaMacro);
				ps.executeUpdate();
			} finally {
				closeStatement(ps);
			}
			return getSeqCurValue(con,"seq_clicks");
	  }
}
