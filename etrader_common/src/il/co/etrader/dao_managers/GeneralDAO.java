//package il.co.etrader.dao_managers;
//
////import il.co.etrader.bl_vos.Enumerator;
//
//import java.sql.Connection;
//import java.sql.PreparedStatement;
//import java.sql.ResultSet;
//import java.sql.SQLException;
//import java.util.HashMap;
//import java.util.HashSet;
//
//import org.apache.log4j.Logger;
//
//import com.anyoption.common.daos.DAOBase;
////import java.util.ArrayList;
//
//public class GeneralDAO extends DAOBase{
//
//	private static final Logger logger = Logger.getLogger(GeneralDAO.class);
//
//	  /*public static ArrayList getAll(Connection con) throws SQLException {
//
//			  PreparedStatement ps=null;
//			  ResultSet rs=null;
//			  ArrayList list=new ArrayList();
//
//			  try
//				{
//				  String sql=" select * from enumerators ";
//
//					ps = con.prepareStatement(sql);
//
//					rs=ps.executeQuery();
//
//					while (rs.next()) {
//
//						list.add(getVO(rs));
//					}
//				}
//
//				finally
//				{
//					closeResultSet(rs);
//					closeStatement(ps);
//				}
//				return list;
//
//		  }
//
//
//
//		  private static Enumerator getVO(ResultSet rs) throws SQLException{
//			  	Enumerator vo=new Enumerator();
//				vo.setId(rs.getLong("id"));
//				vo.setEnumerator(rs.getString("enumerator"));
//				vo.setCode(rs.getString("code"));
//				vo.setDescription(rs.getString("description"));
//				vo.setValue(rs.getString("value"));
//
//				return vo;
//		  }*/
//
//
//
//		  public static long getSequenceNextVal(Connection con,String seq) throws SQLException {
//
//			  PreparedStatement ps=null;
//			  ResultSet rs=null;
//
//			  try
//				{
//				  String sql=" select "+seq+".nextval from dual ";
//
//					ps = con.prepareStatement(sql);
//
//					rs=ps.executeQuery();
//					if (rs.next()) {
//						return rs.getLong(1);
//					}
//				}
//
//				finally
//				{
//					closeResultSet(rs);
//					closeStatement(ps);
//				}
//				return 0;
//		  }
//
//		  public static double getLastShekelDollar(Connection con) throws SQLException {
//
//			  PreparedStatement ps=null;
//			  ResultSet rs=null;
//
//			  try
//				{
//				  String sql=" select closing_level from markets m,opportunities o "+
//					  " where m.id=o.market_id and m.name like '%USD%' and m.name like '%ILS%' "+
//					  " and o.closing_level is not null and o.TIME_SETTLED= "+
//					  " (select max(o.time_settled) from markets m,opportunities o "+
//					  " where m.id=o.market_id and m.name like '%USD%' and m.name like '%ILS%'"+
//					  " and o.closing_level is not null) ";
//
//					ps = con.prepareStatement(sql);
//
//					rs=ps.executeQuery();
//					if (rs.next()) {
//						return rs.getDouble(1);
//					}
//				}
//
//				finally
//				{
//					closeResultSet(rs);
//					closeStatement(ps);
//				}
//				return 4;
//		  }
//
//
//
//		  public static void getTestQuery(Connection con) throws SQLException {
//
//
//			  PreparedStatement ps=null;
//			  ResultSet rs=null;
//
//			  try
//				{
//				  String sql=" select 1 from dual ";
//
//					ps = con.prepareStatement(sql);
//
//					rs=ps.executeQuery();
//
//				}
//
//				finally
//				{
//					closeResultSet(rs);
//					closeStatement(ps);
//				}
//
//		  }
//
//		  /**
//		   * Insert query - for testing
//		   * @param con
//		   * @throws SQLException
//		   */
//		  public static void getTestInsertQuery(Connection con) throws SQLException {
//
//			  PreparedStatement ps = null;
//
//			  try {
//				    String sql = "insert into temp(id, time_created) "+
//			    				 "values (seq_temp.nextval, sysdate) ";
//
//					ps = con.prepareStatement(sql);
//					ps.executeUpdate();
//
//				}
//
//				finally	{
//					closeStatement(ps);
//				}
//
//		  }
//
//
//		  /*
//		  public static void getTestQuery(Connection con,String s) throws SQLException {
//
//
//			  PreparedStatement ps=null;
//			  ResultSet rs=null;
//
//			  try
//				{
//
//					ps = con.prepareStatement(s);
//
//					rs=ps.executeQuery();
//
//				}
//
//				finally
//				{
//					closeResultSet(rs);
//					closeStatement(ps);
//				}
//
//		  }*/
//
//		  public static void insertLog(Connection con,long writerId,String table,long key,int command,String desc) throws SQLException {
//
//			  PreparedStatement ps=null;
//
//			  try
//				{
//				    String sql="insert into log (ID,WRITER_ID,TIME_CREATED,DESCRIPTION,TABLE_NAME,KEY_VALUE,COMMAND) "+
//				    	"values (seq_log.nextval,?,sysdate,(select e.value from enumerators e where e.enumerator='log_commands' and e.code='"+command+"')||' : "+desc+"',?,?,?) ";
//
//
//					ps = con.prepareStatement(sql);
//
//					ps.setLong(1, writerId);
//					ps.setString(2, table);
//					ps.setLong(3, key);
//					ps.setInt(4, command);
//
//					ps.executeUpdate();
//
//				}
//
//				finally
//				{
//					closeStatement(ps);
//				}
//
//		  }
//
////		  public static void insertBalanceLog(Connection con,long writerId,long userId,String table,long key,int command, String utcoffset) throws SQLException {
////
////			  PreparedStatement ps=null;
////
////			  try
////				{
////				    String sql="insert into balance_history (ID,TIME_CREATED,user_id,balance,tax_balance,WRITER_ID," +
////				    		"TABLE_NAME,KEY_VALUE,COMMAND, utc_offset) "+
////				    	"values (seq_balance_history.nextval,sysdate,?,(select balance from users where id=?)," +
////				    	"(select tax_balance from users where id=?),?,?,?,?,?) ";
////
////					ps = con.prepareStatement(sql);
////
////					ps.setLong(1, userId);
////					ps.setLong(2, userId);
////					ps.setLong(3, userId);
////					ps.setLong(4, writerId);
////					ps.setString(5, table);
////					ps.setLong(6, key);
////					ps.setInt(7, command);
////					ps.setString(8, utcoffset);
////
////					ps.executeUpdate();
////
////				}
////
////				finally
////				{
////					closeStatement(ps);
////				}
////
////		  }
//
//		    public static long runStressSelect(Connection con) throws SQLException {
//		        long combId = 0;
//		        PreparedStatement ps = null;
//		        ResultSet rs = null;
//		        try {
//		            String sql = "select id from marketing_combinations order by id";
//		            ps = con.prepareStatement(sql);
//		            rs = ps.executeQuery();
//		            while (rs.next()) {
//		                combId = rs.getLong("id");
//		            }
//		        } finally {
//		            closeResultSet(rs);
//		            closeStatement(ps);
//		        }
//		        return combId;
//		    }
//
//		    public static void runStressInsert(Connection con, long combId) throws SQLException {
//		        PreparedStatement ps = null;
//		        try {
//		            String sql = "INSERT INTO clicks (id, combination_id, ip, time_created) VALUES (SEQ_CLICKS.NEXTVAL, ?, '0.0.0.0', current_date)";
//		            ps = con.prepareStatement(sql);
//		            ps.setLong(1, combId);
//		            ps.executeUpdate();
//		        } finally {
//		            closeStatement(ps);
//		        }
//		    }
//		    
//		/**
//		 * Return HashSet of Allow IP (for example register for this office) 
//		 * @param con
//		 * @return
//		 * @throws SQLException
//		 */
//		public static HashSet<String> getAllowIPList(Connection con) throws SQLException {
//			  PreparedStatement ps = null;
//			  ResultSet rs = null;
//			  HashSet<String> hs = new HashSet<String>();
//			  try {
//				  String sql =" SELECT " +
//				  			  "	 *	" +
//				  			  "	FROM " +
//				  			  "	 allow_ip ";
//					ps = con.prepareStatement(sql);
//					rs = ps.executeQuery();
//					while (rs.next()) {
//						hs.add(rs.getString("ip"));
//					}
//			 } finally {
//				 closeResultSet(rs);
//				 closeStatement(ps);
//			 }
//			  return hs;
//		  }
//		
//		public static HashMap<Long, String> getAllPredefinedDepositAmounts(Connection con) throws SQLException {
//			  PreparedStatement ps = null;
//			  ResultSet rs = null;
//			  HashMap<Long, String> hm = new HashMap<Long, String>();
//			  try {
//				  String sql =" SELECT " +
//				  			  "	 *	" +
//				  			  "	FROM " +
//				  			  "	 predefined_deposit_amount ";
//					ps = con.prepareStatement(sql);
//					rs = ps.executeQuery();
//					while (rs.next()) {
//						hm.put(rs.getLong("currency_id"), rs.getString("amount"));
//					}
//			 } finally {
//				 closeResultSet(rs);
//				 closeStatement(ps);
//			 }
//			  return hm;
//		  }
//
//}
//
