package il.co.etrader.dao_managers;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import com.anyoption.common.daos.DAOBase;

/**
 * Skins DAO Base class.
 *
 * @author Kobi
 */
public class UrlsDAOBase extends DAOBase {

	/**
	 * get our domains list
	 * @param con
	 * @return ArrayList<String> with our domains
	 * @throws SQLException
	 */
	public static ArrayList<String> getOurDomains(Connection con) throws SQLException {

		PreparedStatement ps = null;
		ResultSet rs = null;
		ArrayList<String> domains = new ArrayList<String>();
		try
		{
		    String sql = "SELECT " +
		    			 "	url_name " +
		    		     "FROM " +
		    		     "	urls ";

		    ps = con.prepareStatement(sql);
		    rs = ps.executeQuery();

			while (rs.next()) {
				domains.add(rs.getString("url_name"));
			}

		} finally {
			closeResultSet(rs);
	        closeStatement(ps);
		}

		return domains;
	}
}