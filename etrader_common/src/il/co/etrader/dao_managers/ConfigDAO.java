package il.co.etrader.dao_managers;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;

import org.apache.log4j.Logger;

import com.anyoption.common.daos.DAOBase;

import il.co.etrader.bl_vos.ClearingPriority;
import il.co.etrader.util.ConstantsBase;

public class ConfigDAO extends DAOBase {
	public static final Logger log = Logger.getLogger(ConfigDAO.class);
	 
	public static HashMap<Long, Long> getPriorityOfProvider(Connection conn) throws SQLException{
		String priorityKey = "transaction_reroute_provider_priority_";
		HashMap<Long, Long> hm =  new HashMap<Long, Long>(); 
				
		PreparedStatement ps = null;
		ResultSet rs = null;
		String sql = " SELECT c.* " +
					 " FROM config c " +
					 " WHERE c.key like '" + priorityKey + "%' ";
		try{ 
			ps = conn.prepareStatement(sql);
			rs = ps .executeQuery();
			while (rs.next()){
				
				String priorityKeyFromDB = rs.getString("key");
				Long priority = Long.parseLong(priorityKeyFromDB.substring(priorityKeyFromDB.length()-1));
				Long clearingProviderIdGroup = rs.getLong("value");
				hm.put( priority, clearingProviderIdGroup);
			}
		} finally {
			closeStatement(ps);
			closeResultSet(rs);
		}		
		return hm;
	}

	public static long getDaysNumForFailures(Connection conn) throws SQLException{
		PreparedStatement ps = null;
		ResultSet rs = null;
		long value = 0;
		String sql = " select c.value " +
					 " from config c " +
					 " where c.key like 'transaction_reroute_x_days' ";
		try{
			ps = conn.prepareStatement(sql);
		
			rs = ps.executeQuery();
			if(rs.next()){
				value = rs.getLong("value");
			}
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		return value;
	}

	public static long getFailuresNumConsecutive(Connection conn) throws SQLException{
		PreparedStatement ps = null;
		ResultSet rs = null;
		long value = 0;
		String sql = " select c.value " +
					 " from config c " +
					 " where c.key like 'transaction_reroute_y_failures' ";
		try{
			ps = conn.prepareStatement(sql);
		
			rs = ps.executeQuery();
			if(rs.next()){
				value = rs.getLong("value");
			}
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		return value;
	}
	
	public static boolean updateParameters(Connection con,long keyId, long value) {
		PreparedStatement ps = null;
		try {
			String sql = " update config " +
						 " set value = ? " +
						 " where id = ? ";

			ps = con.prepareStatement(sql);
			ps.setLong(1, value);
			ps.setLong(2, keyId);
            ps.executeUpdate();
		} catch(Exception e) {
			return false;
		} finally {
			closeStatement(ps);
		}
		return true;
	}

	public static ArrayList<ClearingPriority> getPriorityToChange(Connection conn) throws SQLException {
		ArrayList<ClearingPriority> list = new ArrayList<ClearingPriority>();
		PreparedStatement ps = null;
		ResultSet rs = null;
		String sql = " SELECT co.*, cpg.description " +
					 "  FROM (SELECT  distinct(cpg.group_id ) as group_id , cpg.description " +
					 "        FROM clearing_providers_groups cpg) cpg, config co " +
					 " WHERE  co.value = cpg.group_id  AND co.key like '" + ConstantsBase.REROUTING_PRIORITY_CONFIG_KEY + "%' " +
					 " ORDER BY co.key " ;
		try{
			ps = conn.prepareStatement(sql);
			rs = ps.executeQuery();
			
			while(rs.next()){
				ClearingPriority cp = new ClearingPriority();
				Long priority = Long.parseLong(rs.getString("key").substring(ConstantsBase.REROUTING_PRIORITY_CONFIG_KEY.length()));
				cp.setId(rs.getLong("id"));
				cp.setPriorityText(rs.getString("key"));
				cp.setPriority(priority);
				cp.setGroupId(rs.getLong("value"));
				cp.setGroupName(rs.getString("description"));
				
				list.add(cp);
			}
		} finally {
			closeStatement(ps);
			closeResultSet(rs);
		}
		return list;
	}
	
	/**
	 * TODO JAVA DOC
	 * 
	 * @param parameterName
	 * @param conn
	 * @return
	 * @throws SQLException
	 */
	public static String getParameter(String parameterName, Connection conn) throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		String value = null;
		String sql = " select c.value " +
					 " from config c " +
					 " where UPPER(c.key) = UPPER(?) ";
		try {
			ps = conn.prepareStatement(sql);
			ps.setString(1, parameterName);
			rs = ps.executeQuery();
			if(rs.next()){
				value = rs.getString("value");
			}
			log.debug("Config param " + parameterName + ": " + value);
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		
		return value;
	}
	
	/**
	 * TODO JAVA DOC
	 * 
	 * @param parameterName
	 * @param conn
	 * @return
	 * @throws SQLException
	 */
	public static long getLongParameter(String parameterName, Connection conn) throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		Long value = null;
		String sql = " select c.value " +
					 " from config c " +
					 " where UPPER(c.key) = UPPER(?) ";
		try {
			ps = conn.prepareStatement(sql);
			ps.setString(1, parameterName);
			rs = ps.executeQuery();
			if(rs.next()){
				value = rs.getLong("value");
			}
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		
		return value;
	}
	
	public static boolean updateParametersByKey(Connection con,String key, long value) {
		PreparedStatement ps = null;
		try {
			String sql = " update config " +
						 " set value = ? " +
						 " where UPPER(key) = UPPER(?) ";

			ps = con.prepareStatement(sql);
			ps.setLong(1, value);
			ps.setString(2, key);
            ps.executeUpdate();
		} catch(Exception e) {
			return false;
		} finally {
			closeStatement(ps);
		}
		return true;
	}

}
