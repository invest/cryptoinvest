/**
 *
 */
package il.co.etrader.dao_managers;


import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import org.bouncycastle.crypto.CryptoException;

import com.anyoption.common.beans.Issue;
import com.anyoption.common.beans.IssueAction;
import com.anyoption.common.bl_vos.PopulationEntryBase;
import com.anyoption.common.service.userdocuments.UserDocumentsDAO;
import com.anyoption.common.util.AESUtil;

import il.co.etrader.bl_managers.IssuesManagerBase;
import il.co.etrader.bl_managers.PopulationsManagerBase;
import il.co.etrader.issueActions.IssueActionType;
import il.co.etrader.util.CommonUtil;
import il.co.etrader.util.ConstantsBase;

/**
 * IssuesDAOBase class
 * @author Kobi
 *
 */
public class IssuesDAOBase extends com.anyoption.common.daos.IssuesDAOBase {

	  /**
	   * get last action by issue id
	   * @param con db connection
	   * @param issueId the issue id of the action
	   * @return IssueAction instance
	   * @throws SQLException
	   */
	  public static IssueAction getActionById(Connection con,long actionId) throws SQLException {

		  PreparedStatement ps = null;
		  ResultSet rs = null;

		  IssueAction vo = null;

		  try {
			  String sql =
				  " SELECT " +
				  		" ia.*, " +
				  		" iat.channel_id, " +
				  		" iat.reached_status_id " +
		  		  " FROM " +
		  		  		" issue_actions ia, " +
		  		  		" issue_action_types iat " +
				  " WHERE " +
				  		" ia.id = ? " +
				  		" and iat.id = ia.issue_action_type_id ";


				ps = con.prepareStatement(sql);
				ps.setLong(1, actionId);

				rs = ps.executeQuery();

				if (rs.next()) {
					vo = getActionVO(rs);
				}
			} finally {
				closeResultSet(rs);
				closeStatement(ps);
			}
			return vo;
	  }

	  /**
	   * Update use_id column on issues table by population entry id
	   * @param con db connection
	   * @param popUserEntry
	   * @throws SQLException
	   */
	  public static void updateIssuesByPopUserId(Connection con, PopulationEntryBase popUserEntry) throws SQLException {
		  PreparedStatement ps = null;
		  try {
			  String sql = "UPDATE " +
			  				  "issues " +
			  			   "SET " +
			  			   	  "user_id = ? " +
			  			   "WHERE " +
			  			   	  "population_entry_id in (select pe.id " +
			  			   	  						 " from population_entries pe " +
			  			   	  						 " where population_users_id = ?) ";

			  ps = con.prepareStatement(sql);
			  ps.setLong(1, popUserEntry.getUserId());
			  ps.setLong(2, popUserEntry.getPopulationUserId());
			  ps.executeUpdate();
		  } finally {
			  closeStatement(ps);
		  }
	  }

	  /**
	   * Get actions by population entry id
	   * @param con db connection
	   * @param popEntryId - if sales count population entry id for actions count
	   * @param issueId - if risk count
	   * @return
	   * @throws SQLException
	   */
	  public static ArrayList<IssueAction> getActionsCount(Connection con,long popEntryId, long issueId) throws SQLException {

		  PreparedStatement ps = null;
		  ResultSet rs = null;
		  ArrayList<IssueAction> actionList = new ArrayList<IssueAction>();

		  try {
			  String sql = " SELECT " +
			  					" ia.*, " +
						  		" iat.channel_id, " +
						  		" iat.reached_status_id " +
							" FROM " +
								" issue_actions ia, " +
								" issues i," +
								" issue_action_types iat " +
							" WHERE " +
								" i.id = ia.issue_id " +
								" AND iat.id = ia.issue_action_type_id " +
								" AND iat.channel_id = " + IssuesManagerBase.ISSUE_CHANNEL_CALL + " ";
			  if (popEntryId > 0){
				  sql +=
								" AND i.population_entry_id = ? " +
								" AND ia.action_time > (select max(peh.time_created) " +
	                                                  " from population_entries_hist peh " +
	                                                  " where peh.population_entry_id = i.population_entry_id " +
	                                                  "   and peh.status_id in (" + PopulationsManagerBase.POP_ENT_HIS_STATUS_REMOVED_X_CALLS_AND_REACHED_BEFORE  +
	                                                  							"," + PopulationsManagerBase.POP_ENT_HIS_STATUS_NEW + ")) ";
			  }else{
				  sql += 		" AND i.id = ? " +
				  				" AND i.type = " + ConstantsBase.ISSUE_TYPE_RISK + " ";
			  }
			  sql +=
					        " ORDER BY " +
					        	" action_time desc ";

				ps = con.prepareStatement(sql);
				if (popEntryId > 0){
					ps.setLong(1, popEntryId);
				}else{
					ps.setLong(1, issueId);
				}

				rs = ps.executeQuery();
				while (rs.next()) {
					IssueAction action = getActionVO(rs);
					action.setActionTime(convertToDate(rs.getTimestamp("action_time")));
					actionList.add(action);
				}
			}

			finally {
				closeResultSet(rs);
				closeStatement(ps);
			}
			return actionList;
	  }

	  /**
	   * Get users/contcat reached calls num
	   * @param con db connection
	   * @param popUserEntry population user
	   * @return
	   * @throws SQLException
	   */
	  public static long getReachedCallsNum(Connection con, PopulationEntryBase popUserEntry) throws SQLException {

		  PreparedStatement ps = null;
		  ResultSet rs = null;
		  long userId = popUserEntry.getUserId();

		  try {
			  String sql = " SELECT " +
			  					" count(*) calls_num " +
							" FROM " +
								" issue_actions ia, " +
								" issues i," +
								" issue_action_types iat ";
			  if (0 == userId){
				  sql+=         " ,population_entries pe, " +
				  				" population_users pu ";
			  }

			  sql+=         " WHERE " +
								" i.id = ia.issue_id " +
								" AND iat.id = ia.issue_action_type_id " +
								" AND iat.channel_id = " + IssuesManagerBase.ISSUE_CHANNEL_CALL +
								" AND iat.reached_status_id = " + IssuesManagerBase.ISSUE_REACHED_STATUS_REACHED;

			  if (0 == userId){
				  sql+=         " AND i.population_entry_id = pe.id " +
				  				" AND pe.population_users_id = " + popUserEntry.getPopulationUserId();
			  }else {
				  sql+=			" AND i.user_id = " + userId;
			  }

			  ps = con.prepareStatement(sql);

			  rs = ps.executeQuery();
			  if (rs.next()) {
				  return rs.getLong("calls_num");
			  }
		  }

		  finally {
			closeResultSet(rs);
			closeStatement(ps);
		  }
		  return 0;
	  }

	  /**
	   * Vo for action
	   * @param rs
	   * @return
	   * @throws SQLException
	   */
	  protected static IssueAction getActionVO(ResultSet rs) throws SQLException {
			IssueAction vo = new IssueAction();
			vo.setActionId(rs.getLong("id"));
			vo.setWriterId(rs.getLong("writer_id"));
			vo.setChannelId(rs.getString("channel_id"));
			vo.setCallDirectionId(rs.getLong("direction_id"));
			vo.setReachedStatusId(rs.getString("reached_status_id"));
			vo.setActionTypeId(rs.getString("issue_action_type_id"));
			vo.setIssueId(rs.getLong("issue_id"));
			vo.setActionTime(convertToDate(rs.getTimestamp("action_time")));
			return vo;
	  }
	  
	    /**
	     * Insert Document request
	     * @param con
	     * @param ccId
	     * @throws SQLException
	     */
	    public static void insertDocRequest(Connection con, long issueActionId, long ccId, String type, long userId) throws SQLException {
	          PreparedStatement ps = null;
	          ResultSet rs = null;

	          try {
	                String sql =" INSERT INTO " +
	                                " issue_risk_doc_req(ID, ISSUE_ACTION_ID, DOC_TYPE, CC_ID) " +
	                            " VALUES " +
	                                " (seq_issue_risk_doc_req.nextval,?,?,?) ";

	                ps = con.prepareStatement(sql);
	                ps.setLong(1, issueActionId);
	                ps.setString(2, type);
	                if (ccId != 0){
	                    ps.setLong(3, ccId);
	                } else {
	                    ps.setString(3, null);
	                }

	                ps.executeUpdate();
	          } finally {
	                closeStatement(ps);
	                closeResultSet(rs);
	            }
	        //files request
	            ps = null;
	            rs = null;
	            try {
	                  String sql =" INSERT INTO " +
	                              " files " +
	                              	" (id, user_id, file_type_id, writer_id, time_created, utc_offset_created,cc_id, is_approved, file_status_id,  issue_action_id) " + 
	                              " VALUES " +
	                              	" (seq_files.nextval, ?, ?, 1, sysdate, 'GMT+02:00', ?, 0, 1, ?) ";

	                  ps = con.prepareStatement(sql);
	                 
	                  ps.setLong(1, userId);
	                  if (type.equals("2")) {
	                      ps.setString(2, "4");
	                  } else {
	                      ps.setString(2, "18");
	                  }
	                  if (ccId != 0){
	                      ps.setLong(3, ccId);
	                  } else {
	                      ps.setString(3, null);
	                  }
	                  ps.setLong(4, issueActionId);
	                  
	                  ps.executeUpdate();
	                  UserDocumentsDAO.setCurrentFlagDocs(con, userId, new Long(type), ccId);
	            } finally {
	                  closeStatement(ps);
	                  closeResultSet(rs);
	              }
	    }
	    
	    public static void updateDocState(Connection con, long userId, long ccId, long docState) throws SQLException {
            PreparedStatement ps = null;
            ResultSet rs = null;
            try {
                  String sql =" UPDATE " +
                                  " users " +
                              " SET " +
                                  " files_risk_status_id = nvl (files_risk_status_id,?) " + 
                              " WHERE " +
                                  " id = ? ";

                  ps = con.prepareStatement(sql);
                  ps.setLong(1, docState);
                  ps.setLong(2, userId);
                  ps.executeUpdate();
            } finally {
                  closeStatement(ps);
                  closeResultSet(rs);
            }
            
            ps = null;
            rs = null;

            try {
                  String sql =" UPDATE " +
                                  " credit_cards " +
                              " SET " +
                                  " files_risk_status_id = nvl (files_risk_status_id,?) " + 
                              " WHERE " +
                                  " id = ? AND " +
                                  " user_id = ? ";

                  ps = con.prepareStatement(sql);
                  ps.setLong(1, docState);
                  ps.setLong(2, ccId);
                  ps.setLong(3, userId);
                 
                  ps.executeUpdate();
            } finally {
                  closeStatement(ps);
                  closeResultSet(rs);
            }
      }

	  /**
	   * Check if we send email to user for spesific transaction
	   * @param con
	   * @param userId
	   * @param transactionId
	   * @return
	   * @throws SQLException
	   */
	  public static Date isEmailSent(Connection con, long userId, long transactionId) throws SQLException {
		  PreparedStatement ps = null;
		  ResultSet rs = null;

		  try {
			  String sql =
				  "SELECT " +
				  	"ia.action_time, " +
				  	"ia.action_time_offset " +
				  "FROM " +
				  	"users u, " +
			        "issues i, " +
			        "issue_actions ia " +
			      "WHERE " +
			      	"u.id = ? AND " +
			        "i.user_id = u.id AND " +
			        "ia.issue_id = i.id AND " +
			        "ia.ISSUE_ACTION_TYPE_ID = ? AND " +
			        "ia.transaction_id is not null AND " +
			        "ia.transaction_id = ? " +
			       "ORDER BY " +
                     "ia.action_time ";

				ps = con.prepareStatement(sql);
				ps.setLong(1, userId);
				ps.setLong(2, IssuesManagerBase.ISSUE_ACTION_EMAIL);
				ps.setLong(3, transactionId);

				rs = ps.executeQuery();
				if (rs.next()) {
					Date actionTime = convertToDate(rs.getTimestamp("action_time"));
					String actionOffset = rs.getString("action_time_offset");
					return CommonUtil.getDateTimeFormaByTz(actionTime, actionOffset);
				}
			} finally {
				closeResultSet(rs);
				closeStatement(ps);
			}
			return null;
	  }


		/**
		 * Update issue status by issue action id
		 * @param con
		 * @param statusId
		 * @param actionId
		 * @param issueId TODO
		 * @return
		 * @throws SQLException
		 */
		public static boolean updateIssueStatusByIaId(Connection con, int statusId, long actionId, long issueId) throws SQLException{
			PreparedStatement ps = null;
			ResultSet rs = null;
			try {
			    String sql =" UPDATE " +
			    				" issues " +
			    			" SET " +
			    				" status_id = ? " +
			    			" WHERE ";
			    if (issueId > 0){
			    	sql += 		" id = ? ";
			    }else{
			    	sql +=
			    				" id = (  SELECT " +
			    							" ia.issue_id " +
			    						" FROM " +
			    							" issue_actions ia " +
			    						" WHERE " +
			    							" ia.id = ?) ";
			    }

			    ps = con.prepareStatement(sql);
			    ps.setLong(1, statusId);
			    if (issueId > 0){
			    	ps.setLong(2, issueId);
			    }else{
			    	ps.setLong(2, actionId);
			    }

				ps.executeUpdate();

			} finally	{
					closeResultSet(rs);
					closeStatement(ps);
			}
			return true;
		}

		/**
		 * update Support Unreach Status
		 * @param con
		 * @param issueId
		 * @return
		 * @throws SQLException
		 */
		public static void updateSupportUnreachStatus(Connection con, long issueId) throws SQLException{
			PreparedStatement ps = null;
			ResultSet rs = null;
			try {

				String excludeStatuses = IssuesManagerBase.FILES_RISK_STATUS_CLOSED + "," +
											IssuesManagerBase.FILES_RISK_STATUS_DONE_SUPPORT + "," +
											IssuesManagerBase.FILES_RISK_STATUS_NEED_APPROVE + "," +
											IssuesManagerBase.FILES_RISK_STATUS_DONE_UNCOOPERATIVE;

			    String sql =" begin " +

							"	  for v_req in (select  " +
							"	                  ir.doc_type, " +
							"	                  i.user_id, " +
							"	                  ir.cc_id, " +
							"	                  case when ir.doc_type = " + ConstantsBase.FILE_TYPE_ID_COPY + " then  " +
							"	                         u.files_risk_status_id " +
							"	                       else " +
							"	                         cc.files_risk_status_id " +
							"	                       end files_risk_status_id     " +
							"	                from " +
							"	                  issues i  " +
							"	                    left join users u on u.id = i.user_id,  " +
							"	                  issue_actions ia, " +
							"	                  issue_risk_doc_req ir " +
							"	                    left join credit_cards cc on cc.id = ir.cc_id " +
							"	                where  " +
							"	                  i.id = ia.issue_id " +
							"	                  and ia.id = ir.issue_action_id " +
							"	                  and i.id = ? " +
							"	                ) loop " +
							//		--update risk issue with contact user to done support unreached
		                    " 		if (v_req.doc_type = " + ConstantsBase.RISK_REQUEST_CONTACT_USER + " ) then " +
		                    "    		update issues iss " +
		                    "     			set iss.status_id = " + IssuesManagerBase.FILES_RISK_STATUS_DONE_SUPPORT_UNREACHED + " " +
		                    "    		where " +
		                    "       		iss.id = ?; " +
		                    " 		 end if; " +
							//	    -- check if need to update doc status
							"	    if (v_req.files_risk_status_id not in (" + excludeStatuses + ")) then " +
							//	      -- check if user doc (v_req.doc_type = 1) or cc doc (v_req.doc_type = 2)
							"	      if (v_req.doc_type = " + ConstantsBase.FILE_TYPE_ID_COPY + ") then " +
							"	         update users t " +
							"	         set t.files_risk_status_id = " + IssuesManagerBase.FILES_RISK_STATUS_DONE_SUPPORT_UNREACHED + " " +
							"	         where t.id = v_req.user_id; " +
							"	      elsif (v_req.doc_type = " + ConstantsBase.FILE_TYPE_CC + ") then " +
							"	         update credit_cards t " +
							"	         set t.files_risk_status_id = " + IssuesManagerBase.FILES_RISK_STATUS_DONE_SUPPORT_UNREACHED + " " +
							"	         where t.id = v_req.cc_id; " +
							"	      end if; " +
							"	    end if; " +
							"	  end loop; " +
							"	end; ";


			    ps = con.prepareStatement(sql);
			    ps.setLong(1, issueId);
			    ps.setLong(2, issueId);
				ps.executeUpdate();

			} finally	{
					closeResultSet(rs);
					closeStatement(ps);
			}
		}


		/**
		 * Get Relevant cards/Id copy when pressing on issue action id
		 * @param con
		 * @param issueActionId
		 * @return
		 * @throws SQLException
		 * @throws CryptoException
		 * @throws NoSuchPaddingException  
		 * @throws NoSuchAlgorithmException 
		 * @throws BadPaddingException 
		 * @throws IllegalBlockSizeException 
		 * @throws InvalidKeyException 
		 * @throws InvalidAlgorithmParameterException 
		 */
	public static String getRelevantCardsToIssueAction(Connection con, long issueActionId)	throws SQLException, CryptoException,
																							InvalidKeyException, IllegalBlockSizeException,
																							BadPaddingException, NoSuchAlgorithmException,
																							NoSuchPaddingException,
																							InvalidAlgorithmParameterException {
			PreparedStatement ps = null;
			ResultSet rs = null;
			String cardsOrIdCopy = ConstantsBase.EMPTY_STRING;
			int type = 0;
			try {
				String sql =" SELECT " +
				  				" rdk.doc_type, " +
				  				" cc.cc_number, " +
				  				" cct.name " +
				  			" FROM " +
				  				" issue_risk_doc_req rdk " +
				  				" 	LEFT JOIN credit_cards cc ON cc.id = rdk.cc_id " +
				  				" 		LEFT JOIN credit_card_types cct ON cct.id = cc.type_id " +
				  			" WHERE " +
				  				" rdk.issue_action_id = ? " +
				  			" ORDER BY rdk.doc_type ";

			    ps = con.prepareStatement(sql);
			    ps.setLong(1, issueActionId);
				rs = ps.executeQuery();
				while (rs.next()) {
					type = rs.getInt("doc_type");
					if (type == ConstantsBase.FILE_TYPE_ID_COPY){
						cardsOrIdCopy += "Id Copy <br/> ";
					} else if (type == ConstantsBase.FILE_TYPE_CC){
						String ccnumber = AESUtil.decrypt(rs.getString("cc_number"));
						String cardNumber = ccnumber.substring(ccnumber.length()-4);
						String cardType = CommonUtil.getMessage(rs.getString("name"), null);
						cardsOrIdCopy = cardType + " " + cardNumber + " <br/> " + cardsOrIdCopy;
					}else{
						cardsOrIdCopy = "Contact user";
						
					}
				}
			} finally	{
				closeResultSet(rs);
				closeStatement(ps);
			}
			return cardsOrIdCopy;
		}
		//
		public static IssueActionType getLastIssueActionTypeByContactId(Connection con, long contact_id) throws SQLException {
			IssueActionType vo = null;
			PreparedStatement ps = null;
			ResultSet rs = null;
			try {
				String sql ="SELECT " +
								" iat.* " +
							"FROM " +
								" population_users pu, " +
								" population_entries pe, " +
								" issues i, " +
								" issue_actions ia, " +
								" issue_action_types iat " +
							"WHERE " +
								" pu.id = pe.population_users_id " +
								" AND pe.id = i.population_entry_id " +
								" AND i.last_action_id = ia.id " +
								" AND pu.contact_id = ? " +
								" AND ia.issue_action_type_id = iat.id " ;

				ps = con.prepareStatement(sql);
				ps.setLong(1, contact_id);
				rs = ps.executeQuery();

				if(rs.next()) {
					vo = getActionTypeVO(rs);
				}

				return vo;
			} finally {
				closeResultSet(rs);
				closeStatement(ps);
			}
		}

		public static IssueActionType getLastIssueActionTypeByUserId(Connection con, long userId) throws SQLException {
			IssueActionType vo = null;
			PreparedStatement ps = null;
			ResultSet rs = null;
			try {
				String sql ="SELECT " +
								" iat.* " +
							"FROM " +
								" population_users pu, " +
								" population_entries pe, " +
								" issues i, " +
								" issue_actions ia, " +
								" issue_action_types iat " +
							"WHERE " +
								" pu.id = pe.population_users_id " +
								" AND pe.id = i.population_entry_id " +
								" AND i.last_action_id = ia.id " +
								" AND pu.user_id = ? " +
								" AND ia.issue_action_type_id = iat.id " ;

				ps = con.prepareStatement(sql);
				ps.setLong(1, userId);
				rs = ps.executeQuery();

				if(rs.next()) {
					vo = getActionTypeVO(rs);
				}

				return vo;
			} finally {
				closeResultSet(rs);
				closeStatement(ps);
			}
		}

	public static IssueActionType getLastIssueActionTypeByEntryId(Connection con, long EntryId) throws SQLException {
		IssueActionType vo = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			String sql ="SELECT " +
							" iat.* " +
						"FROM " +
							" issues i, " +
							" issue_actions ia, " +
							" issue_action_types iat " +
						"WHERE " +
							"     i.population_entry_id = ? " +
							" AND i.last_action_id = ia.id " +
							" AND ia.issue_action_type_id = iat.id " ;

			ps = con.prepareStatement(sql);
			ps.setLong(1, EntryId);
			rs = ps.executeQuery();

			if(rs.next()) {
				vo = getActionTypeVO(rs);
			}

			return vo;
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
	}

	protected static IssueActionType getActionTypeVO(ResultSet rs) throws SQLException {
		IssueActionType vo = new IssueActionType();
		vo.setId(rs.getLong("id"));
		vo.setName(rs.getString("NAME"));
		vo.setChannelId(rs.getLong("CHANNEL_ID"));
		vo.setReachedStatusId(rs.getLong("REACHED_STATUS_ID"));
		vo.setDepositSearchType(rs.getInt("DEPOSIT_SEARCH_TYPE"));
		return vo;
	}
	
	/**
     * exists issue for combination user and credit card
     * @param con
     * @param ccNum
     * @param userId
     * @return
     * @throws SQLException
     * @throws CryptoException
	 * @throws UnsupportedEncodingException 
	 * @throws BadPaddingException 
	 * @throws IllegalBlockSizeException 
	 * @throws NoSuchPaddingException 
	 * @throws NoSuchAlgorithmException 
	 * @throws InvalidKeyException 
	 * @throws InvalidAlgorithmParameterException 
     */
	public static int isExistsIssueByUserAndCC(	Connection con, long ccNum, long userId,
												int subjectId)	throws SQLException, CryptoException, InvalidKeyException,
																NoSuchAlgorithmException, NoSuchPaddingException, IllegalBlockSizeException,
																BadPaddingException, UnsupportedEncodingException,
																InvalidAlgorithmParameterException {

        PreparedStatement ps = null;
        ResultSet rs = null;
        int ret = 0;

        try {
            String sql =
                " SELECT " +
                      " * " +
                " FROM " +
                      " issues i, credit_cards cc " +
                " WHERE " +
                      " i.credit_card_id=cc.id " +
                      " and i.user_id = ? " +
                      " and cc.cc_number = ?" +
                      " and i.subject_id = ?";

              ps = con.prepareStatement(sql);
              ps.setLong(1, userId);
              ps.setString(2, AESUtil.encrypt(Long.toString(ccNum)));
              ps.setInt(3, subjectId);

              rs = ps.executeQuery();

              while (rs.next()) {
                  ret++;
              }
          } finally {
              closeResultSet(rs);
              closeStatement(ps);
          }
          return ret;
    }
	
	 /**
     * exists issue for combination user and credit card
     * @param con
     * @param ccNum
     * @param userId
     * @param Doc
     * @return
     * @throws SQLException
     * @throws CryptoException
     */
    public static int[] getExistsIssueByUserAndCC(Connection con, long ccNum, long userId) throws SQLException, CryptoException {

        PreparedStatement ps = null;
        ResultSet rs = null;
        int ret[] = {0, 0, 0, 0};

        try {
            String sql =
                    "SELECT NVL(rl.id, fk.id) id, " +
                            "  NVL(rl.doc_type, fk.doc_type) doc_type, " +
                            "  NVL(rl.status_id, fk.status_id) status_id, " +
                            "  NVL(rl.cc_same, fk.cc_same) cc_same " +
                            " FROM " +
                            "  (SELECT i.id, " +
                            "    irdr.doc_type, " +
                            "    i.status_id, " +
                            "    DECODE(irdr.cc_id, ?, 1, 0) cc_same " +
                            "  FROM ISSUE_RISK_DOC_REQ irdr, " +
                            "    issues i, " +
                            "    issue_actions ia " +
                            "  WHERE i.id    = ia.issue_id " +
                            "  AND ia.id     = irdr.issue_action_id " +
                            " AND i.subject_id != " + IssuesManagerBase.ISSUE_SUBJECT_REG_DOC +
                            "  AND i.user_id = ? " +
                            "  ) rl " +
                            " FULL OUTER JOIN " +
                            "  (SELECT 0 id, 0 doc_type, 0 status_id, 0 cc_same FROM dual " +
                            "  ) fk " +
                            " ON fk.id       =rl.id " +
                            " AND fk.doc_type=rl.doc_type " +
                            " ORDER BY 1 DESC, " +
                            " 4 DESC" ;

              ps = con.prepareStatement(sql);
              ps.setLong(1, ccNum);
              ps.setLong(2, userId);

              rs = ps.executeQuery();

              if (rs.next()) {
                  ret[0] = rs.getInt("id");
                  ret[1] = rs.getInt("doc_type");
                  ret[2] = rs.getInt("status_id");
                  ret[3] = rs.getInt("cc_same");
              }
          } finally {
              closeResultSet(rs);
              closeStatement(ps);
          }
          return ret;
    }
    
    public static boolean InsertIssueRiskDocReq(Connection con, long issueActionId, String type, long ccId) throws SQLException {
    	PreparedStatement ps = null;
        ResultSet rs = null;
        try {
              String sql =" INSERT INTO " +
                              " issue_risk_doc_req(ID, ISSUE_ACTION_ID, DOC_TYPE, CC_ID) " +
                          " VALUES " +
                              " (seq_issue_risk_doc_req.nextval,?,?,?) ";
              
              ps = con.prepareStatement(sql);
              ps.setLong(1, issueActionId);
              ps.setString(2, type);
              if (ccId != 0){
                  ps.setLong(3, ccId);
              } else {
                  ps.setString(3, null);
              }
              ps.executeUpdate();
        } finally {
              closeStatement(ps);
              closeResultSet(rs);
          }
        return true;
    }

	public static void updatePendingRegulationIssuesForUser(Connection con,	long userId) throws SQLException {
    	PreparedStatement ps = null;
        try {
              String sql =" UPDATE " +
                              " issue_actions " +
                          " SET " +
                              " comments = 'Please ask for docs' " +
                          " WHERE " +
                              " issue_id IN " +
                              " (SELECT id FROM issues WHERE user_id = "+userId+" AND subject_id = " + IssuesManagerBase.ISSUE_SUBJECT_REG_DOC +" ) " +
                              " AND issue_action_type_id = "+IssuesManagerBase.ISSUE_ACTION_PENDING_DOC;
                              
              ps = con.prepareStatement(sql);
              ps.executeUpdate();
        } finally {
              closeStatement(ps);
          }
	}
	
	public static boolean isPendingDocIssuesForUser(Connection con, long userId) throws SQLException {
		 
		 PreparedStatement stmnt = null;
	     ResultSet rs = null;
	     boolean result = false;
	     try {
	    	 String sql = "SELECT count(i.id) c FROM " +
	    	 		      " issues i, " +
	    	 		      " issue_actions ia " +
	    	 		      " WHERE i.id = ia.issue_id " +
	    	 		      " AND i.subject_id = ? " + 
	    	 		      " AND ia.issue_action_type_id = ? " + 
	    	 		      " AND i.user_id = ? ";
	    	
	    	 stmnt = con.prepareStatement(sql);
	    	 stmnt.setInt(1,IssuesManagerBase.ISSUE_SUBJECT_REG_DOC);
	    	 stmnt.setInt(2,IssuesManagerBase.ISSUE_ACTION_PENDING_DOC);
	    	 stmnt.setLong(3,userId);
             rs = stmnt.executeQuery();
             if(rs.next()) {
                 int size = rs.getInt("c");
                 if(size > 1) {
                	 result = true;
                 }
             }
             
	     }finally {
	    	 closeStatement(stmnt);
             closeResultSet(rs);
	     }
		return result;
	}
	
	public static boolean isPendingDocIssuesForCC(Connection con, long userId,long creditCardId) throws SQLException {
		
		PreparedStatement stmnt = null;
		ResultSet rs = null;
		boolean result = false;
		try {
			 String sql = "SELECT DISTINCT " +
			 		      "cc.id  FROM " +
			 		      "issues i, issue_actions ia, files f, credit_cards cc "+
                          "WHERE i.id = ia.issue_id "+
                          "AND f.issue_action_id = ia.id "+
                          "AND f.cc_id = cc.id " +
                          "AND i.subject_id = 56 " +
                          "AND cc.user_id = ? " +
                          "AND cc.id = ? ";
			 stmnt = con.prepareStatement(sql);
			 stmnt.setLong(1, userId);
			 stmnt.setLong(2, creditCardId);
             rs = stmnt.executeQuery();
             if(rs.next()) {
                result = true;
             }
			
		} finally {
			closeStatement(stmnt);
            closeResultSet(rs);
		}
		return result;
	}
	
	public static long issueActionTypeForDeactivation(Connection con, long userId)throws SQLException{
		PreparedStatement stmnt = null;
		ResultSet rs = null;
		long result = 0l;
		try {
			 String sql = " SELECT " + 
					 	  	" ia.id, ia.issue_action_type_id " +
					 	  " FROM " + 
						 	  " issues iss ," +
						 	  " issue_actions ia," +
						 	  " users usr " +
					 	  "	WHERE " +
						 	  "	iss.user_id = usr.id AND " +
						 	  " usr.id = ? AND " +
						 	  " usr.is_active = 0 AND" +
						 	  " ia.issue_id = iss.id AND " +
						 	  " ia.issue_action_type_id in (1,9,10,11,13,39)" +
						 	  " order by iss.user_id, ia.id desc";
			 stmnt = con.prepareStatement(sql);
			 stmnt.setLong(1, userId);
			 rs = stmnt.executeQuery();
            if(rs.next()) {
               result = rs.getLong("issue_action_type_id");
            }
			
		} finally {
			closeStatement(stmnt);
           closeResultSet(rs);
		}
		return result;
	}
	
	/**
	 * Search issues
	 * @param byWriter - search writer in comments
	 * @return long
	 * @throws SQLException
	 */
	public static long searchIssueIdByPopEntryID(Connection con,long populationEntryId) throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		long issueId = 0;
		try {
			String sql = " SELECT " +
		  					" i.id " +
		  				" FROM " +
		  					" issues i " +
		  				" WHERE " +
		  					" i.population_entry_id = ? ";
			ps = con.prepareStatement(sql);
			ps.setLong(1,populationEntryId);
			rs = ps.executeQuery();
			if (rs.next()) {
				issueId = rs.getLong("id");
			}
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		return issueId;
	}
	
	public static Issue getIssueByCreditCardIdAndSubjectId(Connection con, long creditCardId, long subjectId) throws SQLException {
		Issue vo = null;
		PreparedStatement ps = null;
		ResultSet rs = null;

		try {
			String sql = " SELECT * " + " FROM issues " + " WHERE credit_card_id = ? " + " AND subject_id = ?";

			ps = con.prepareStatement(sql);
			ps.setLong(1, creditCardId);
			ps.setLong(2, subjectId);

			rs = ps.executeQuery();
			if (rs.next()) {
				vo = new Issue();
				vo.setId(rs.getLong("id"));
				vo.setUserId(rs.getLong("user_id"));
				vo.setSubjectId(rs.getString("subject_id"));
				vo.setStatusId(rs.getString("status_id"));
				vo.setTimeCreated(rs.getDate("time_created"));
				vo.setCreditCardId(rs.getLong("credit_card_id"));
				vo.setPriorityId(rs.getString("priority_id"));
			}
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		return vo;
	  }
	
	public static IssueAction getLastCcChangeIssueAction(Connection con, long userId, long creditCardId, long issueId,
			long issueActionType) throws SQLException {
		IssueAction vo = null;
		PreparedStatement ps = null;
		ResultSet rs = null;

		try {
			String sql = " SELECT " + 
								" * " + 
						 " FROM " +
						 	"  (SELECT " + 
						 			" ia.*, " + 
						 			" w.user_name " + 
						 	" FROM " + 
						 			" issue_actions ia, " + 
						 			" issues i, " +
						 			" writers w " +
						 	" WHERE " + 
						 			" ia.issue_id = i.id " + 
						 			" AND ia.writer_id = w.id " + 
						 			" AND i.user_id = ? " +
						 			" AND i.credit_card_id = ? " + 
						 			" AND i.subject_id = ? " + 
						 			" AND ia.issue_action_type_id = ? " +
						 	" ORDER BY "+ 
						 			" ia.action_time DESC ) " + 
						 "WHERE "+ 
						 		" ROWNUM = 1";

			ps = con.prepareStatement(sql);
			ps.setLong(1, userId);
			ps.setLong(2, creditCardId);
			ps.setLong(3, issueId);
			ps.setLong(4, issueActionType);

			rs = ps.executeQuery();
			if (rs.next()) {
				vo = new IssueAction();
				vo.setActionId(rs.getLong("id"));
				vo.setWriterId(rs.getLong("writer_id"));
				vo.setComments(rs.getString("comments"));
				vo.setActionTime(rs.getDate("action_time"));
				vo.setActionTypeId(rs.getString("issue_action_type_id"));
				vo.setWriterName(rs.getString("user_name"));
			}
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		return vo;
	}
	
}
