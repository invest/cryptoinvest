package il.co.etrader.dao_managers;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;

import com.anyoption.common.daos.DAOBase;

import il.co.etrader.bl_vos.CcBlackList;
import il.co.etrader.util.CommonUtil;

public class CcBlackListDAO extends DAOBase{
    public static void insert(Connection con,CcBlackList vo) throws SQLException {
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            String sql = "insert into cc_black_list(id,writer_id,time_created,comments,cc_number,is_active)" +
            " values(seq_cc_black_list.nextval,?,?,?,?,?) ";

            ps = con.prepareStatement(sql);
            ps.setLong(1, vo.getWriterId());
            ps.setTimestamp(2, CommonUtil.convertToTimeStamp(vo.getTimeCreated()));
            ps.setString(3, vo.getComments());
            ps.setLong(4, Long.valueOf(vo.getCcNum()).longValue());
            ps.setInt(5, vo.getIsActive());
            ps.executeUpdate();
            vo.setId(getSeqCurValue(con, "seq_cc_black_list"));
        } finally {
            closeStatement(ps);
            closeResultSet(rs);
        }
    }

    public static void update(Connection con, CcBlackList vo) throws SQLException {
        PreparedStatement ps = null;
        try {
            String sql = "update cc_black_list set comments=?,is_active=? where id=?";
            ps = con.prepareStatement(sql);
            ps.setString(1, vo.getComments());
            ps.setInt(2, vo.getIsActive());
            ps.setLong(3, vo.getId());
            ps.executeUpdate();
        } finally {
            closeStatement(ps);
        }
    }

    public static ArrayList<CcBlackList> search(Connection con, Date from, Date to) throws SQLException {
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        ArrayList<CcBlackList> list = new ArrayList<CcBlackList>();
        try {
            String sql =
                "SELECT " +
                    "* " +
                "FROM " +
                    "cc_black_List " +
                "WHERE " +
                    "time_created >= ? AND " +
                    "time_created <= ?";
            pstmt = con.prepareStatement(sql);
            pstmt.setTimestamp(1, CommonUtil.convertToTimeStamp(CommonUtil.getDateTimeFormat(from, CommonUtil.getAppData().getUtcOffset())));
            pstmt.setTimestamp(2, CommonUtil.convertToTimeStamp(CommonUtil.getDateTimeFormat(CommonUtil.addDay(to), CommonUtil.getAppData().getUtcOffset())));
            rs = pstmt.executeQuery();
            while (rs.next()) {
                list.add(getVO(rs));
            }
        } finally {
            closeResultSet(rs);
            closeStatement(pstmt);
        }
        return list;
    }

    public static boolean isCardBlacklisted(Connection con, long ccn, boolean deposit) throws SQLException {
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            String sql =
                "SELECT " +
                    "id " +
                "FROM " +
                    "cc_black_List " +
                "WHERE " +
                    "cc_number = ? AND " +
                    "is_active = 1 AND " +
                    "is_deposit = ?";
            pstmt = con.prepareStatement(sql);
            pstmt.setLong(1, ccn);
            pstmt.setInt(2, deposit ? 1 : 0);
            rs = pstmt.executeQuery();
            if (rs.next()) {
                return true;
            }
        } finally {
            closeResultSet(rs);
            closeStatement(pstmt);
        }
        return false;
    }

    public static boolean isCardBlacklistedWithdraw(Connection con, long ccn) throws SQLException {
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            String sql =
                "SELECT " +
                    "id " +
                "FROM " +
                    "cc_black_List " +
                "WHERE " +
                    "cc_number = ? AND " +
                    "is_active = 1 ";
            pstmt = con.prepareStatement(sql);
            pstmt.setLong(1, ccn);

            rs = pstmt.executeQuery();
            if (rs.next()) {
                return true;
            }
        } finally {
            closeResultSet(rs);
            closeStatement(pstmt);
        }
        return false;
    }


    private static CcBlackList getVO(ResultSet rs) throws SQLException {
    	CcBlackList vo = new CcBlackList();
    	vo.setId(rs.getLong("id"));
    	vo.setIsActive(rs.getInt("is_active"));
    	vo.setCcNum(rs.getString("cc_number"));
    	vo.setComments(rs.getString("comments"));
    	vo.setTimeCreated(convertToDate(rs.getTimestamp("time_created")));
    	vo.setWriterId(rs.getLong("writer_id"));
    	return vo;
    }
}