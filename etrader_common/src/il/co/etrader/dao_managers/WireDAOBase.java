//package il.co.etrader.dao_managers;
//
//import il.co.etrader.bl_vos.WireBase;
//
//import java.math.BigDecimal;
//import java.sql.Connection;
//import java.sql.PreparedStatement;
//import java.sql.ResultSet;
//import java.sql.SQLException;
//
//import oracle.jdbc.OraclePreparedStatement;
//import oracle.jdbc.OracleResultSet;
//
//import org.apache.log4j.Logger;
//
//import com.anyoption.common.daos.DAOBase;
//import com.anyoption.common.util.CommonUtil;
////import il.co.etrader.util.CommonUtil;
//
//public class WireDAOBase extends DAOBase{
//
//	private static final Logger logger = Logger.getLogger(WireDAOBase.class);
//
//	/**
//	 * Insert wire Deposit
//	 * @param con
//	 * @param vo
//	 * @throws SQLException
//	 */
//	  public static void insert(Connection con,WireBase vo) throws SQLException {
//		  OraclePreparedStatement ps = null;
//		  ResultSet rs = null;
//		  long id=0;
//		  try {
//
//				String sql="insert into Wires(id,bank_id,branch,account_num,account_name,bank_name,branch_address, account_info" +
//						") values(seq_Wires.nextval,?,?,?,?,?,?,?) ";
//
//				ps = (OraclePreparedStatement)con.prepareStatement(sql);
//                ps.setFormOfUse(4, OraclePreparedStatement.FORM_NCHAR);
//
//                //in AO there is no bank Id
//                if (null!=vo.getBankId()) {
//                	ps.setLong(1, Long.valueOf(vo.getBankId()).longValue());
//                }
//
//				ps.setLong(2, Long.valueOf(vo.getBranch()).longValue());
//				ps.setLong(3, Long.valueOf(vo.getAccountNum()).longValue());
//				ps.setString(4,vo.getAccountName());
//				ps.setString(5,vo.getBankName());
//				ps.setString(6, vo.getBranchAddress());
//				ps.setString(7, vo.getAccountInfo());
//
//
//				ps.executeUpdate();
//				vo.setId(getSeqCurValue(con,"seq_Wires"));
//		  }
//			finally
//			{
//				closeStatement(ps);
//				closeResultSet(rs);
//			}
//
//	  }
//
//
//	  public static WireBase get(Connection con,long id) throws SQLException {
//
//		  OraclePreparedStatement ps=null;
//		  OracleResultSet rs=null;
//
//		  try
//			{
//			  String sql="SELECT " +
//                            "w.*, " +
//                            "b.name as Bank_Withdrawal_Name " +
//                         "FROM " +
//                             "wires w " +
//                                 "LEFT JOIN banks b ON w.withdrawal_source = b.id " +
//                         "WHERE w.id=? ";
//				ps = (OraclePreparedStatement)con.prepareStatement(sql);
//				ps.setLong(1, id);
//				rs=(OracleResultSet)ps.executeQuery();
//				if (rs.next()) {
//					WireBase w = getVO(rs);
//                    w.setBankWithdrawName(rs.getString("Bank_Withdrawal_Name"));
//
//
//					return w;
//				}
//			}
//			finally
//			{
//				closeResultSet(rs);
//				closeStatement(ps);
//			}
//			return null;
//	  }
//
//		/**
//		 * Insert Bank Wire
//		 * @param con
//		 * 		Db connection
//		 * @param vo
//		 *
//		 * @throws SQLException
//		 */
//		public static void insertBankWire(Connection con,WireBase vo) throws SQLException {
//
//			  OraclePreparedStatement ps = null;
//			  ResultSet rs = null;
//			  long id = 0;
//
//			  try {
//
//					String sql = "insert into Wires(id, bank_id, branch, account_num, account_name, " +
//	                             "bank_name, swift, iban, beneficiary_name,branch_address, bank_code,branch_name, account_info) " +
//								 "values(seq_Wires.nextval,?,?,?,?,?,?,?,?,?,?,?,?) ";
//
//					ps = (OraclePreparedStatement)con.prepareStatement(sql);
//	                ps.setFormOfUse(4, OraclePreparedStatement.FORM_NCHAR);
//
//                    if ( CommonUtil.IsParameterEmptyOrNull(vo.getBankId())) {
//                        ps.setString(1, null);
//                    } else {
//                        ps.setLong(1, Long.parseLong(vo.getBankId()));
//                    }
//
//					if ( CommonUtil.IsParameterEmptyOrNull(vo.getBranch())) {
//						ps.setString(2, null);
//					} else {
//						ps.setLong(2, Long.parseLong(vo.getBranch()));
//					}
//					if ( CommonUtil.IsParameterEmptyOrNull(vo.getAccountNum())) {
//						ps.setString(3, null);
//					} else {
//						ps.setBigDecimal(3, new BigDecimal(vo.getAccountNum()));
//					}
//					ps.setString(4,vo.getAccountName());
//					ps.setString(5,vo.getBankNameTxt());
//					ps.setString(6,vo.getSwift());
//					ps.setString(7,vo.getIban());
//					ps.setString(8,vo.getBeneficiaryName());
//					ps.setString(9, vo.getBranchAddress());
//					ps.setString(10, vo.getBankCode());
//					ps.setString(11, vo.getBranchName());
//					ps.setString(12, vo.getAccountInfo());
//
//					ps.executeUpdate();
//					vo.setId(getSeqCurValue(con,"seq_Wires"));
//
//			  } finally	{
//					closeStatement(ps);
//					closeResultSet(rs);
//				}
//
//		  }
//
//		  private static WireBase getVO(ResultSet rs) throws SQLException{
//				WireBase vo = new WireBase();
//
//				vo.setId(rs.getLong("id"));
//				vo.setBankId(rs.getString("bank_id"));
//				vo.setBranch(rs.getString("branch"));
//				vo.setAccountNum(rs.getString("account_num"));
//				vo.setAccountName(rs.getString("account_name"));
//				vo.setAccountInfo(rs.getString("account_info"));
//
//				vo.setBeneficiaryName(rs.getString("beneficiary_name"));
//				vo.setSwift(rs.getString("swift"));
//				vo.setIban(rs.getString("iban"));
//				vo.setBankNameTxt(rs.getString("bank_name"));
//				vo.setBranchAddress(rs.getString("branch_address"));
//				vo.setBankFeeAmount(rs.getLong("bank_fee_amount"));
//				vo.setBranchName(rs.getString("branch_name"));
//				return vo;
//		  }
//
//		  public static void updateBankFeeAmount(Connection con,Long wireId,Long bankFeeAmount) throws SQLException{
//
//				PreparedStatement ps = null;
//				try {
//
//					String sql = "UPDATE" +
//							     "	wires " +
//							     "SET " +
//							     "	bank_fee_amount = ? " +
//							     "WHERE" +
//							     "	id = ? ";
//
//					ps = con.prepareStatement(sql);
//					ps.setLong(1,bankFeeAmount);
//					ps.setLong(2, wireId);
//
//					ps.executeUpdate();
//
//				}
//				finally {
//					closeStatement(ps);
//				}
//
//
//		}
//
//          public static void updateBankWireWithdrawal(Connection con,Long wireId, Long bankFeeAmount) throws SQLException{
//
//                PreparedStatement ps = null;
//                try {
//
//                    String sql = "UPDATE" +
//                                 "  wires " +
//                                 "SET " +
//                                 "  withdrawal_source = ? " +
//                                 "WHERE" +
//                                 "  id = ? ";
//
//                    ps = con.prepareStatement(sql);
//                    ps.setLong(1,bankFeeAmount);
//                    ps.setLong(2, wireId);
//
//                    ps.executeUpdate();
//
//                }
//                finally {
//                    closeStatement(ps);
//                }
//
//
//          }
//}
