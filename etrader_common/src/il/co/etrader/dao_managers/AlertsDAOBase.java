package il.co.etrader.dao_managers;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import org.apache.log4j.Logger;

import com.anyoption.common.daos.DAOBase;

import il.co.etrader.bl_vos.AlertsBase;

public class AlertsDAOBase extends DAOBase {

    private static final Logger logger = Logger.getLogger(AlertsDAOBase.class);
    
    public boolean insertAlertsBase(Connection con, ArrayList<AlertsBase> bases) throws SQLException{
        
        int counter = 0;
        boolean response = false;
        PreparedStatement ps = null;
        String sql = "INSERT INTO " +
                     " users_alerts " +
                     	" (user_id, alert_type, alert_status_id, alert_class_type, alert_from_amount, alert_to_amount, alert_email_subject, alert_email_recepients) " +
                     " VALUES " +
                     	" (?,?,?,?,?,?,?,?) ";
        try{
            
            for (AlertsBase ab : bases  ){
                
                ps = null;
                ps = con.prepareStatement(sql);
                ps.setLong(1, ab.getUserId());
                ps.setInt(2,ab.getAlertType());
                ps.setString(3,ab.getAlertStatusId());
                
                if(ab.getAlertClassType()==null){
                ps.setString(4,null);   
                }else{
                ps.setInt(4,ab.getAlertClassType());}
                if(ab.getAlertFromAmount()==null){
                ps.setString(5, null);  
                }else{
                ps.setInt(5,ab.getAlertFromAmount());}
                if(ab.getAlertToAmount()==null){
                    ps.setString(6, null);
                }else{
                ps.setInt(6,ab.getAlertToAmount());}
                ps.setString(7,ab.getAlertEmailSubject());
                ps.setString(8,ab.getAlertEmailRecepients());
                counter =counter+ps.executeUpdate();
                
            }
        }
        
        finally{
            
            if(counter == bases.size()){
                
                response = true;
                
            }else{
                
                response = false;
                con.rollback();
            }
            
            closeStatement(ps);
        }
        
         
        return response;
    }
    

    public boolean deleteAlertsBase(Connection con, ArrayList<AlertsBase> bases) throws SQLException {
        int counter = 0;
        boolean response = false;
        PreparedStatement ps = null;
      
        String sql = "Delete " +
                     " users_alerts " +
                     "WHERE " +
                     "user_id = ? " +
                     "AND " +
                     " alert_type = ?";
        try {
            for (AlertsBase ab : bases  ) {
                ps = null;
                ps = con.prepareStatement(sql);
                ps.setLong(1, ab.getUserId());
                ps.setInt(2,ab.getAlertType());
                counter =counter+ps.executeUpdate();
            }
        } finally {
            if (counter == bases.size()) {
                response = true;
            } else {
                response = false;
                con.rollback();
            }
            closeStatement(ps);
        }
        return response;
    }
    
    public ArrayList<AlertsBase> getAlertsByUser(Connection con, Long userId) throws SQLException{
        
        ArrayList<AlertsBase> al = new ArrayList<AlertsBase>();
        PreparedStatement ps = null;
        ResultSet rs = null;
        
        try {
            
            String sql =" SELECT " +
                                 " * " +
                          " FROM " +
                                 " users_alerts " +
                         " WHERE " +
                                 " user_id = ? ";
            
            ps = con.prepareStatement(sql);
            ps.setLong(1, userId);
            rs = ps.executeQuery();
            
            while (rs.next()) {
                
                AlertsBase base = new AlertsBase();
        
                    base.setAlertClassType(rs.getInt("ALERT_CLASS_TYPE"));
                    base.setAlertEmailRecepients(rs.getString("ALERT_EMAIL_RECEPIENTS"));
                    base.setAlertEmailSubject(rs.getString("ALERT_EMAIL_SUBJECT"));
                    base.setAlertToAmount(rs.getInt("ALERT_TO_AMOUNT"));
                    base.setAlertFromAmount(rs.getInt("ALERT_FROM_AMOUNT"));
                    base.setAlertType(rs.getInt("ALERT_TYPE"));
                    base.setAlertStatusId(rs.getString("ALERT_STATUS_ID"));
                    
                al.add(base);
            }
            
            
        } finally {
            closeResultSet(rs);
            closeStatement(ps);
        }
        return al;
    
        
    }
    
public boolean deleteAlertsBaseAll(Connection con, Long userId) throws SQLException{
        int counter = 0;
        boolean response = false;
        PreparedStatement ps = null;
      
        String sql = "Delete " +
                     " users_alerts " +
                     "WHERE " +
                     "user_id = ? ";
        try {
                ps = null;
                ps = con.prepareStatement(sql);
                ps.setLong(1, userId);
                counter =ps.executeUpdate();
        } finally {
            if (counter != 0) {
                response = true;
            } else {
                response = false;
                con.rollback();
            }
            closeStatement(ps);
        }
        return response;
    }
}