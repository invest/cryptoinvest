package il.co.etrader.dao_managers;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import com.anyoption.common.daos.DAOBase;

import il.co.etrader.bl_vos.Chat;

public class ChatDAOBase extends DAOBase {
  public static void insert(Connection con, Chat vo) throws SQLException {
	  PreparedStatement ps = null;
	  try {
			String sql =
				"INSERT INTO liveperson_chats(id, chat, type_id, non_html_chat) VALUES (seq_liveperson_chats.nextval,?,?,?) ";
			ps = con.prepareStatement(sql);
			ps.setString(1, vo.getChat());
			ps.setLong(2, vo.getTypeId());
			ps.setString(3, vo.getNonHtmlChat());
			ps.executeUpdate();
			vo.setId(getSeqCurValue(con,"seq_liveperson_chats"));
	    } finally {
			closeStatement(ps);
		}
  }
}