package il.co.etrader.dao_managers;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;

import org.apache.log4j.Logger;

import com.anyoption.common.daos.UsersAutoMailDaoBase;

import il.co.etrader.bl_vos.UsersAutoMail;
import oracle.jdbc.OracleTypes;

public class UsersAutoMailDao extends UsersAutoMailDaoBase {
	
	private static final Logger log = Logger.getLogger(UsersAutoMailDao.class);	
	
	public static ArrayList<UsersAutoMail> getUsersAutoMail(Connection conn, long reasonId) throws SQLException {
		ArrayList<UsersAutoMail> res = new ArrayList<>();
		CallableStatement cstmt = null;
		ResultSet rs = null;
		int index = 1;
		try {
			cstmt = conn.prepareCall("{call PKG_USERS_AUTO_MAIL.GET_USERS_AUTO_MAIL(   O_DATA => ? " +
																					" ,I_REASON_ID => ? )}");
		
			cstmt.registerOutParameter(index++, OracleTypes.CURSOR);
			setStatementValue(reasonId, index++, cstmt);
			
			cstmt.execute();			
			rs = (ResultSet) cstmt.getObject(1);
			while (rs.next()) {
				UsersAutoMail uam = new UsersAutoMail(rs.getLong("id"), 
													 rs.getLong("user_id"));
				res.add(uam);				
			}
			rs.close();
		} finally {
			closeStatement(cstmt);
			closeResultSet(rs);
		}
		return res;
	}	
	
	public static void updateUsersAutoMailSent(Connection conn, ArrayList<Long> mailIds) throws SQLException {
		CallableStatement cstmt = null;
		int index = 1;		
		try {
			cstmt = conn.prepareCall("{call PKG_USERS_AUTO_MAIL.UPDATE_USER_AUTO_MAIL_SENT( I_MAIL_ID => ? )}"); 																					   
			if(mailIds != null){
				cstmt.setArray(index++, getPreparedSqlArray(conn, mailIds));
			} else {
				cstmt.setNull(index++, Types.ARRAY, "NUMBER_TABLE");					
			}
			cstmt.execute();
			log.debug("Updated Sent Users Auto MailId's:" + mailIds);
		} finally {
			closeStatement(cstmt);
		}
	}
	
	public static void updateUsersAutoMailReject(Connection conn, ArrayList<Long> mailIds) throws SQLException {
		CallableStatement cstmt = null;
		int index = 1;		
		try {
			cstmt = conn.prepareCall("{call PKG_USERS_AUTO_MAIL.UPDATE_USER_AUTO_MAIL_ERR( I_MAIL_ID => ? )}"); 																					   
			if(mailIds != null){
				cstmt.setArray(index++, getPreparedSqlArray(conn, mailIds));
			} else {
				cstmt.setNull(index++, Types.ARRAY, "NUMBER_TABLE");					
			}
			cstmt.execute();
			log.debug("Updated Reject Err Users Auto MailId's:" + mailIds);
		} finally {
			closeStatement(cstmt);
		}
	}
	
	public static ArrayList<UsersAutoMail> getUsersnotifeReachedTotalDeposit(Connection conn, long reasonId) throws SQLException {
		ArrayList<UsersAutoMail> res = new ArrayList<>();
		CallableStatement cstmt = null;
		ResultSet rs = null;
		int index = 1;
		try {
			cstmt = conn.prepareCall("{call PKG_USERS_AUTO_MAIL.GET_USERS_NOTIFE_TTL_DEPOSITS( O_DATA => ? )}");
		
			cstmt.registerOutParameter(index++, OracleTypes.CURSOR);						
			cstmt.execute();			
			rs = (ResultSet) cstmt.getObject(1);
			while (rs.next()) {
				UsersAutoMail uam = new UsersAutoMail(rs.getLong("id"), 
													  rs.getLong("user_id"),
													  rs.getTimestamp("questionnaire_done_date") != null ? convertToDate(rs.getTimestamp("questionnaire_done_date")): null,
													  rs.getString("skin_name"));
				res.add(uam);				
			}
			rs.close();
		} finally {
			closeStatement(cstmt);
			closeResultSet(rs);
		}
		return res;
	}
}