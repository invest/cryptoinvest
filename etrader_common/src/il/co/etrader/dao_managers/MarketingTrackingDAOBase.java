//package il.co.etrader.dao_managers;
//
//import il.co.etrader.util.CommonUtil;
//
//import java.sql.Connection;
//import java.sql.PreparedStatement;
//import java.sql.ResultSet;
//import java.sql.SQLException;
//import java.util.ArrayList;
//
//import oracle.jdbc.OraclePreparedStatement;
//
//import org.apache.log4j.Logger;
//
//import com.anyoption.common.beans.base.MarketingTracking;
//import com.anyoption.common.beans.base.MarketingTrackingCookieStatic;
//import com.anyoption.common.daos.DAOBase;
//
//public class MarketingTrackingDAOBase extends DAOBase{
//	
//	private static final Logger logger = Logger.getLogger(MarketingTrackingDAOBase.class);
//	
//	public static boolean insertMarketingTracking(Connection con, ArrayList<MarketingTracking> tracking) throws SQLException{
//	        
//		int counter = 0;
//		boolean response = false;
//		PreparedStatement ps = null;
//
//		String sql = "Begin MARKETING.INSERT_MARKETING_TRACKING(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);  End;";
//		try {
//			for (MarketingTracking tr : tracking) {
//				ps = null;
//				ps = (OraclePreparedStatement) con.prepareStatement(sql);
//				ps.setString(1, tr.getmId());
//				ps.setLong(2, tr.getCombinationId());
//				ps.setTimestamp(3, CommonUtil.convertToTimeStamp(tr.getTimeStatic()));
//				ps.setString(4, tr.getHttpReferer());
//				ps.setString(5, tr.getDyanmicParameter());
//				ps.setLong(6, tr.getCombinationIdDynamic());
//				ps.setTimestamp(7, CommonUtil.convertToTimeStamp(tr.getTimeDynamic()));
//				ps.setString(8, tr.getHttpRefererDynamic());
//				ps.setString(9, tr.getDyanmicParameterDynamic());
//				ps.setLong(10, tr.getContactId());
//				ps.setLong(11, tr.getUserId());
//				ps.setLong(12, tr.getMarketingTrackingActivityId());
//
//				counter = counter + ps.executeUpdate();
//			}
//		} finally {
//			if (counter == tracking.size()) {
//				response = true;
//			} else {
//				response = false;
//				con.rollback();
//			}
//			closeStatement(ps);
//		}
//		return response;
//	}
//	
//	public static String getMidByUserId(Connection con, long userId)
//			throws SQLException {
//		String mId = "";
//		PreparedStatement stmt = null;
//		ResultSet rs = null;
//		try {
//			String sql = "SELECT " + " MARKETING.GET_MID_BY_USER_ID(?) as MID "
//					+ " FROM " + " DUAL ";
//			stmt = con.prepareStatement(sql);
//			stmt.setLong(1, userId);
//			rs = stmt.executeQuery();
//			if (rs.next()) {
//				mId = rs.getString("MID");
//			}
//		} finally {
//			closeResultSet(rs);
//			closeStatement(stmt);
//		}
//		return mId;
//	}
//	
//	public static Long getUserIdByMid(Connection con, String mid)
//			throws SQLException {
//		long userId = 0;
//		PreparedStatement stmt = null;
//		ResultSet rs = null;
//		try {
//			String sql = "SELECT " +
//							" distinct mt.USER_ID as USER_ID " + 
//						  " FROM " +
//							" marketing_tracking mt, marketing_tracking_static ms " + 
//							" WHERE " + 
//							" mt.marketing_tracking_static_id = ms.id  " +
//							" and mid = ? " +
//							" and USER_ID>0";
//			stmt = con.prepareStatement(sql);
//			stmt.setString(1, mid);
//			rs = stmt.executeQuery();
//			if (rs.next()) {
//				userId = rs.getLong("USER_ID");
//			}
//		} finally {
//			closeResultSet(rs);
//			closeStatement(stmt);
//		}
//		return userId;
//	}
//	
//	public static Long getContactIdByMid(Connection con, String mid)
//			throws SQLException {
//		long contactId = 0;
//		PreparedStatement stmt = null;
//		ResultSet rs = null;
//		try {
//			String sql = "SELECT " +
//					" distinct mt.CONTACT_ID as CONTACT_ID " + 
//				  " FROM " +
//					" marketing_tracking mt, marketing_tracking_static ms " + 
//					" WHERE " + 
//					" mt.marketing_tracking_static_id = ms.id  " +
//					" and mid = ? " +
//					" and CONTACT_ID>0";
//			stmt = con.prepareStatement(sql);
//			stmt.setString(1, mid);
//			rs = stmt.executeQuery();
//			if (rs.next()) {
//				contactId = rs.getLong("CONTACT_ID");
//			}
//		} finally {
//			closeResultSet(rs);
//			closeStatement(stmt);
//		}
//		return contactId;
//	}
//	
//	public static boolean isHaveActivity (Connection con, String mid) throws SQLException{
//		boolean result = false;
//		long activity = 0;
//		PreparedStatement stmt = null;
//		ResultSet rs = null;
//		try {
//			String sql = "SELECT " + " MARKETING.GET_ACTIVITY_BY_MID(?) as ACTIVITY "
//					+ " FROM " + " DUAL ";
//			stmt = con.prepareStatement(sql);
//			stmt.setString(1, mid);
//			rs = stmt.executeQuery();
//			if (rs.next()) {
//				activity = rs.getLong("ACTIVITY");
//			}
//			if(activity > 0){
//				result = true;
//			}
//			
//		} finally {
//			closeResultSet(rs);
//			closeStatement(stmt);
//		}
//		
//		return result;
//	}
//	
//	public static MarketingTrackingCookieStatic getMarketingTrackingCookieStatic(Connection con, String mId) throws SQLException {
//	   	  
//		PreparedStatement ps = null;
//		  ResultSet rs = null;
//		  MarketingTrackingCookieStatic mtc = new MarketingTrackingCookieStatic();
//
//		  try {
//			    String sql = "SELECT " +
//			    				" mts.mid, mts.combination_id, mts.time_static, mts.http_referer, mts.dynamic_param " +
//			    			 "FROM " +
//			    			 	"marketing_tracking_static mts " +
//			    			 "WHERE mts.mid = ? ";
//
//				ps = con.prepareStatement(sql);
//				ps.setString(1, mId);
//
//				rs = ps.executeQuery();
//
//				if (rs.next()) {
//					mtc.setMs(rs.getString("mid"));
//					mtc.setCs(rs.getString("combination_id"));
//					mtc.setTs(rs.getTimestamp("time_static"));
//					mtc.setHttp(rs.getString("http_referer"));
//					mtc.setDp(rs.getString("dynamic_param"));
//				}
//			}
//			finally {
//				closeResultSet(rs);
//				closeStatement(ps);
//			}
//			return mtc;
//	}	   
//}
