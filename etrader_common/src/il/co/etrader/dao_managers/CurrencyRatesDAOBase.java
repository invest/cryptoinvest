///**
// * 
// */
//package il.co.etrader.dao_managers;
//
//import java.sql.Connection;
//import java.sql.PreparedStatement;
//import java.sql.ResultSet;
//import java.sql.SQLException;
//import java.util.Date;
//
//import org.apache.log4j.Logger;
//
//import com.anyoption.common.daos.DAOBase;
//
//import il.co.etrader.util.CommonUtil;
//
///**
// * @author pavelhe
// *
// */
//public class CurrencyRatesDAOBase extends DAOBase {
//	
//	private static final Logger logger = Logger.getLogger(CurrencyRatesDAOBase.class);
//	
//	/**
//	 * this method convert an amount from one currency to an amount in base currency
//	 * @param amount - the amount to convert from
//	 * @param currencyId - the currency to convert from
//	 * @param date - the exchange rate date
//	 * @return the new amount
//	 */
//	public static Double convertToBaseAmount(Connection con,
//												long amount,
//												long currencyId,
//												Date date) throws SQLException {
//		PreparedStatement ps=null;
//		ResultSet rs=null;
//		Double res = new Double(0);
//
//		try {
//			String sql="select convert_amount_to_usd(?,?,?) base_amount from dual";
//			ps = con.prepareStatement(sql);
//
//			ps.setLong(1, amount);
//			ps.setLong(2, currencyId);
//			ps.setTimestamp(3, CommonUtil.convertToTimeStamp((date != null ? date : new Date())));
//
//			rs=ps.executeQuery();
//
//			if (rs.next()) {
//				res = rs.getDouble("base_amount");
//			} else {
//				/* This should never happen! */
//				logger.error("SQL query [" + sql + "] did not return converted amount for given amount ["
//								+ amount + "], currency [" + currencyId + "] and date [" + date + "]");
//			}
//		} finally {
//			closeResultSet(rs);
//			closeStatement(ps);
//		}
//		
//		return res;
//	}
//
//	public static Double convertToEuroAmount(Connection con, long amount, long currencyId, Date date) throws SQLException {
//		PreparedStatement ps=null;
//		ResultSet rs=null;
//		Double res = new Double(0);
//
//		try {
//			String sql="select convert_amount_to_eur(?,?,?) base_amount from dual";
//			ps = con.prepareStatement(sql);
//
//			ps.setLong(1, amount);
//			ps.setLong(2, currencyId);
//			ps.setTimestamp(3, CommonUtil.convertToTimeStamp(new Date()));
//
//			rs=ps.executeQuery();
//
//			if (rs.next()) {
//				res = rs.getDouble("base_amount");
//			}
//		}finally {
//			closeResultSet(rs);
//			closeStatement(ps);
//		}
//		return res;
//	}
//	
//}
