package il.co.etrader.dao_managers;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import javax.faces.model.SelectItem;

import oracle.jdbc.OracleTypes;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import com.anyoption.common.beans.Transaction;
import com.anyoption.common.clearing.ClearingManagerBase;
import com.anyoption.common.managers.TransactionsManagerBase;
import com.anyoption.common.util.ConstantsBase;

import il.co.etrader.bl_vos.FireServerPixelHelper;
import il.co.etrader.bl_vos.FireServerPixelFields;
import il.co.etrader.util.CommonUtil;

public abstract class TransactionsDAOBase extends com.anyoption.common.daos.TransactionsDAOBase {
	private static final Logger logger = Logger.getLogger(TransactionsDAOBase.class);

	//a new insert method is added to allow inserting transaction from the service (since the service is running under Jboss)
	public static  void insertFromService(Connection con, Transaction vo) throws SQLException {
	  PreparedStatement ps = null;
	  ResultSet rs = null;
	  try {
		  	String rate = "trunc(convert_amount_to_usd(1,(select currency_id from users where id=?),sysdate),"+ ConstantsBase.RATE_PRECISISON +")";
			String sql="insert into transactions t(id,user_id,credit_card_id,type_id," +
					"amount,status_id,description,writer_id,ip,time_settled,comments,processed_writer_id," +
					"time_created,reference_id,cheque_id,wire_id,charge_back_id,receipt_num,fee_cancel," +
					"utc_offset_created, utc_offset_settled, bonus_user_id, rate, acquirer_response_id, is_rerouted, rerouting_transaction_id) " +
										"values(seq_transactions.nextval,?,?,?,?,?,?,?,?,?,?,?,sysdate,?,?,?,?,?,?,?,?,?," + rate + ",?,?,?)";

			ps = (PreparedStatement)con.prepareStatement(sql);
			ps.setLong(1, vo.getUserId());
			ps.setBigDecimal(2,vo.getCreditCardId());
			ps.setLong(3, vo.getTypeId());
			ps.setLong(4, vo.getAmount());
			ps.setLong(5, vo.getStatusId());
			ps.setString(6, vo.getDescription());  //In Orace this field is of type NCHAR hence will have problems inserting hebrew
			ps.setLong(7, vo.getWriterId());
			ps.setString(8, vo.getIp());
			ps.setTimestamp(9, CommonUtil.convertToTimeStamp(vo.getTimeSettled()));
			ps.setString(10, vo.getComments());	//In Orace this field is of type NCHAR hence will have problems inserting hebrew
			ps.setLong(11, vo.getProcessedWriterId());
			ps.setBigDecimal(12,vo.getReferenceId());
			ps.setBigDecimal(13,vo.getChequeId());
			ps.setBigDecimal(14,vo.getWireId());
			ps.setBigDecimal(15,vo.getChargeBackId());
			ps.setBigDecimal(16,vo.getReceiptNum());
			if (vo.isFeeCancel()) {
				ps.setInt(17, ConstantsBase.CC_FEE_CANCEL_YES);
			}
			else {
				ps.setInt(17, ConstantsBase.CC_FEE_CANCEL_NO);
			}
			ps.setString(18, vo.getUtcOffsetCreated());
			ps.setString(19, vo.getUtcOffsetSettled());
            if (vo.getBonusUserId() > 0) {
                ps.setLong(20, vo.getBonusUserId());
            } else {
                ps.setNull(20, Types.NUMERIC);
            }
			ps.setLong(21, vo.getUserId());
			ps.setString(22, vo.getAcquirerResponseId());
			ps.setLong(23, vo.isRerouted()?1:0);
			ps.setLong(24, vo.getReroutingTranId());
			
			ps.executeUpdate();

			vo.setId(getSeqCurValue(con,"seq_transactions"));

			if (logger.isDebugEnabled()) {
	            String ls = System.getProperty("line.separator");
	            logger.log(Level.DEBUG, ls + "Transaction started:" + vo.toString());
			}
	  }
		finally
		{
			closeStatement(ps);
			closeResultSet(rs);
		}

  }
//	  public static void insert(Connection con,Transaction vo) throws SQLException {
//		  OraclePreparedStatement ps = null;
//		  ResultSet rs = null;
//		  try {
//			  	String rate = " trunc(convert_amount_to_usd(1,(select currency_id from users where id=?),sysdate),"+ ConstantsBase.RATE_PRECISISON +")";
//				String sql=	" insert into transactions t(id,user_id,credit_card_id,type_id," +
//							" amount,status_id,description,writer_id,ip,time_settled,comments,processed_writer_id,time_created,reference_id," +
//							" cheque_id,wire_id,charge_back_id,receipt_num,fee_cancel, utc_offset_created, utc_offset_settled, is_credit_withdrawal," +
//							" splitted_reference_id, deposit_reference_id,payment_type_id, bonus_user_id, is_accounting_approved, rate," +
//							" auth_number,clearing_provider_id, paypal_email, envoy_account_num, moneybookers_email, webmoney_purse, xor_id_authorize, " +
//							" xor_id_capture , acquirer_response_id, is_rerouted, rerouting_transaction_id, selector_id) " +
//								" values(seq_transactions.nextval,?,?,?,?,?,?,?,?,?,?,?,sysdate,?,?,?,?,?,?,?,?,?,?,?,?,?,?, "+rate+",?,?,?,?,?,?,?,?,?,?,?,?)";
//
//				ps = (OraclePreparedStatement)con.prepareStatement(sql);
//				ps.setFormOfUse(6, OraclePreparedStatement.FORM_NCHAR);
//				ps.setFormOfUse(10, OraclePreparedStatement.FORM_NCHAR);
//
//				ps.setLong(1, vo.getUserId());
//				ps.setBigDecimal(2,vo.getCreditCardId());
//				ps.setLong(3, vo.getTypeId());
//				ps.setLong(4, vo.getAmount());
//				ps.setLong(5, vo.getStatusId());
//				ps.setString(6, vo.getDescription());
//				ps.setLong(7, vo.getWriterId());
//				ps.setString(8, vo.getIp());
//				ps.setTimestamp(9, CommonUtil.convertToTimeStamp(vo.getTimeSettled()));
//				ps.setString(10, vo.getComments());
//				ps.setLong(11, vo.getProcessedWriterId());
//				ps.setBigDecimal(12,vo.getReferenceId());
//				ps.setBigDecimal(13,vo.getChequeId());
//				ps.setBigDecimal(14,vo.getWireId());
//				ps.setBigDecimal(15,vo.getChargeBackId());
//				ps.setBigDecimal(16,vo.getReceiptNum());
//				if (vo.isFeeCancel()) {
//					ps.setInt(17, ConstantsBase.CC_FEE_CANCEL_YES);
//				}
//				else {
//					ps.setInt(17, ConstantsBase.CC_FEE_CANCEL_NO);
//				}
//				ps.setString(18, vo.getUtcOffsetCreated());
//				ps.setString(19, vo.getUtcOffsetSettled());
//				if ( vo.isCreditWithdrawal() ) {
//					ps.setInt(20, 1);
//				} else {
//					ps.setInt(20, 0);
//				}
//				ps.setLong(21, vo.getSplittedReferenceId());
//				ps.setLong(22, vo.getDepositReferenceId());
//				ps.setLong(23, vo.getPaymentTypeId());
//                if (vo.getBonusUserId() > 0) {
//                    ps.setLong(24, vo.getBonusUserId());
//                } else {
//                    ps.setNull(24, Types.NUMERIC);
//                }
//				if (vo.isAccountingApproved()) {
//					ps.setInt(25, 1);
//				}
//				else {
//					ps.setInt(25, 0);
//				}
//				ps.setLong(26, vo.getUserId());
//				ps.setString(27, vo.getAuthNumber());
//				ps.setLong(28, vo.getClearingProviderId());
//				ps.setString(29, vo.getPayPalEmail());
//				ps.setString(30, vo.getEnvoyAccountNum());
//				ps.setString(31, vo.getMoneybookersEmail());
//				ps.setString(32, vo.getWebMoneyPurse());
//
//				if (vo.isPayPalDeposit()){
//					ps.setString(33, vo.getXorIdAuthorize());
//					ps.setString(34, vo.getXorIdCapture());
//				}else{
//					ps.setString(33, null);
//					ps.setString(34, null);
//				}
//				ps.setString(35, vo.getAcquirerResponseId());
//				ps.setLong(36, vo.isRerouted()?1:0);
//				ps.setLong(37, vo.getReroutingTranId());
//				ps.setLong(38, vo.getSelectorId());
//				ps.executeUpdate();
//
//				vo.setId(getSeqCurValue(con,"seq_transactions"));
//
//				if (logger.isDebugEnabled()) {
//		            String ls = System.getProperty("line.separator");
//		            logger.log(Level.DEBUG, ls + "Transaction started:" + vo.toString());
//				}
//		  }	finally {
//				closeStatement(ps);
//				closeResultSet(rs);
//			}
//
//	  }

	  /**
	   * Update Transaction after capture(from backend)
	   * @param con
	   * @param vo
	   * 		Transaction insatnce
	   * @return
	   * @throws SQLException
	   */
	  public static  long updateCapture(Connection con,Transaction vo) throws SQLException {
		  PreparedStatement ps = null;
		  try {

				String sql =
					"UPDATE " +
						"transactions t " +
					"SET " +
						"user_id=?," +
						"credit_card_id=?," +
						"type_id=?," +
						" amount=?," +
						"status_id=?," +
						"description=?," +
						"writer_id=?," +
						"ip=?," +
						"time_settled=sysdate," +
						"comments=?," +
						"processed_writer_id=?," +
						"reference_id=?," +
						"cheque_id=?," +
						"wire_id=?," +
						"charge_back_id=?," +
						"receipt_num=?," +
						"auth_number=?," +
						"xor_id_capture=?, " +
						"utc_offset_settled=?, " +
						"clearing_provider_id=? ";

				sql += "WHERE " +
							"id = ? ";

				ps = con.prepareStatement(sql);

				ps.setLong(1, vo.getUserId());
				ps.setBigDecimal(2,vo.getCreditCardId());
				ps.setLong(3, vo.getTypeId());
				ps.setLong(4, vo.getAmount());
				ps.setLong(5, vo.getStatusId());
				ps.setString(6, vo.getDescription());
				ps.setLong(7, vo.getWriterId());
				ps.setString(8, vo.getIp());
				//ps.setTimestamp(9, CommonUtil.convertToTimeStamp(vo.getTimeSettled()));
				ps.setString(9, vo.getComments());
				ps.setLong(10, vo.getProcessedWriterId());
				ps.setBigDecimal(11,vo.getReferenceId());
				ps.setBigDecimal(12,vo.getChequeId());
				ps.setBigDecimal(13,vo.getWireId());
				ps.setBigDecimal(14,vo.getChargeBackId());
				ps.setBigDecimal(15,vo.getReceiptNum());
				ps.setString(16,vo.getAuthNumber());
				ps.setString(17, vo.getXorIdCapture());
				ps.setString(18, vo.getUtcOffsetSettled());
				ps.setLong(19, vo.getClearingProviderId());
				ps.setLong(20, vo.getId());

				ps.executeUpdate();

				if (logger.isDebugEnabled()) {
		            String ls = System.getProperty("line.separator");
		            logger.log(Level.DEBUG, ls + "Transaction updated:" + vo.toString());
				}
		  }	finally	{
				closeStatement(ps);
			}
			return vo.getId();
	  }

	  public static  long update(Connection con, Transaction vo) throws SQLException {
          PreparedStatement ps = null;
	      try {
	          String sql =
                  "UPDATE " +
                      "transactions t " +
                  "SET " +
                      "user_id = ?, " +
                      "credit_card_id = ?, " +
                      "type_id = ?, " +
                      "amount = ?, " +
                      "status_id = ?, " +
                      "description = ?, " +
                      "writer_id = ?, " +
                      "ip = ?, " +
                      "time_settled = ?, " +
                      "comments = ?, " +
                      "processed_writer_id = ?, " +
                      "reference_id = ?, " +
                      "cheque_id = ?, " +
                      "wire_id = ?, " +
                      "charge_back_id = ?, " +
                      "receipt_num = ?, " +
                      "auth_number = ?, " +
                      "xor_id_authorize = ?, " +
                      "utc_offset_settled = ?, " +
                      "clearing_provider_id = ?, " +
                      "fee_cancel = ?, " +
                      "xor_id_capture=?, " +
                      "moneybookers_email=?, " +
                      "acquirer_response_id=?, " +
                      "is_rerouted = ? , " +
                      "rerouting_transaction_id = ?, " +
                      "IS_MANUAL_ROUTING = ?, " +
                      "payment_type_id = ? " +
                  "WHERE " +
                      "id = ?";

	          ps = con.prepareStatement(sql);

	          ps.setLong(1, vo.getUserId());
	          ps.setBigDecimal(2,vo.getCreditCardId());
	          ps.setLong(3, vo.getTypeId());
	          ps.setLong(4, vo.getAmount());
	          ps.setLong(5, vo.getStatusId());
	          ps.setString(6, vo.getDescription());
	          ps.setLong(7, vo.getWriterId());
	          ps.setString(8, vo.getIp());
	          ps.setTimestamp(9, CommonUtil.convertToTimeStamp(vo.getTimeSettled()));
	          ps.setString(10, vo.getComments());
	          ps.setLong(11, vo.getProcessedWriterId());
	          ps.setBigDecimal(12,vo.getReferenceId());
	          ps.setBigDecimal(13,vo.getChequeId());
	          ps.setBigDecimal(14,vo.getWireId());
	          ps.setBigDecimal(15,vo.getChargeBackId());
	          ps.setBigDecimal(16,vo.getReceiptNum());
	          ps.setString(17,vo.getAuthNumber());
	          ps.setString(18,vo.getXorIdAuthorize());
	          ps.setString(19, vo.getUtcOffsetSettled());
              ps.setLong(20, vo.getClearingProviderId());
              if ( vo.isFeeCancel() ) {
            	  ps.setLong(21, ConstantsBase.CC_FEE_CANCEL_YES);
              } else {
            	  ps.setLong(21, ConstantsBase.CC_FEE_CANCEL_NO);
              }
              ps.setString(22, vo.getXorIdCapture());
              ps.setString(23, vo.getMoneybookersEmail());
              ps.setString(24, vo.getAcquirerResponseId());
              ps.setLong(25, vo.isRerouted() ? 1: 0);
              ps.setLong(26, vo.getReroutingTranId());
              ps.setLong(27, vo.getManualRouted());
	          ps.setLong(28, vo.getPaymentTypeId());
	          ps.setLong(29, vo.getId());
	          
	          ps.executeUpdate();
	          if (logger.isDebugEnabled()) {
	              String ls = System.getProperty("line.separator");
	              logger.log(Level.DEBUG, ls + "Transaction updated:" + vo.toString());
	          }
	      } finally {
	          closeStatement(ps);
	      }
	      return vo.getId();
	  }
	  
	  public static  long updateReverseWithdraw(Connection con, Transaction vo) throws SQLException {
          PreparedStatement ps = null;
	      try {
	          String sql =
                  "UPDATE " +
                      "transactions t " +
                  "SET " +
                      "user_id = ?, " +
                      "credit_card_id = ?, " +
                      "type_id = ?, " +
                      "amount = ?, " +
                      "status_id = ?, " +
                      "description = ?, " +
                      "writer_id = ?, " +
                      "ip = ?, " +
                      "time_settled = ?, " +
                      "comments = ?, " +
                      "processed_writer_id = ?, " +
                      "reference_id = ?, " +
                      "cheque_id = ?, " +
                      "wire_id = ?, " +
                      "charge_back_id = ?, " +
                      "receipt_num = ?, " +
                      "auth_number = ?, " +
                      "xor_id_authorize = ?, " +
                      "utc_offset_settled = ?, " +
                      "clearing_provider_id = ?, " +
                      "fee_cancel = ?, " +
                      "xor_id_capture=?, " +
                      "moneybookers_email=?, " +
                      "acquirer_response_id=?, " +
                      "is_rerouted = ? , " +
                      "rerouting_transaction_id = ?, " +
                      "IS_MANUAL_ROUTING = ? " +
                  "WHERE " +
                      "id = ? " +
                      " and status_id in (" + TransactionsManagerBase.TRANS_STATUS_REQUESTED + ", " + TransactionsManagerBase.TRANS_STATUS_APPROVED + ", " + TransactionsManagerBase.TRANS_STATUS_SECOND_APPROVAL + ")";

	          ps = con.prepareStatement(sql);

	          ps.setLong(1, vo.getUserId());
	          ps.setBigDecimal(2,vo.getCreditCardId());
	          ps.setLong(3, vo.getTypeId());
	          ps.setLong(4, vo.getAmount());
	          ps.setLong(5, vo.getStatusId());
	          ps.setString(6, vo.getDescription());
	          ps.setLong(7, vo.getWriterId());
	          ps.setString(8, vo.getIp());
	          ps.setTimestamp(9, CommonUtil.convertToTimeStamp(vo.getTimeSettled()));
	          ps.setString(10, vo.getComments());
	          ps.setLong(11, vo.getProcessedWriterId());
	          ps.setBigDecimal(12,vo.getReferenceId());
	          ps.setBigDecimal(13,vo.getChequeId());
	          ps.setBigDecimal(14,vo.getWireId());
	          ps.setBigDecimal(15,vo.getChargeBackId());
	          ps.setBigDecimal(16,vo.getReceiptNum());
	          ps.setString(17,vo.getAuthNumber());
	          ps.setString(18,vo.getXorIdAuthorize());
	          ps.setString(19, vo.getUtcOffsetSettled());
              ps.setLong(20, vo.getClearingProviderId());
              if ( vo.isFeeCancel() ) {
            	  ps.setLong(21, ConstantsBase.CC_FEE_CANCEL_YES);
              } else {
            	  ps.setLong(21, ConstantsBase.CC_FEE_CANCEL_NO);
              }
              ps.setString(22, vo.getXorIdCapture());
              ps.setString(23, vo.getMoneybookersEmail());
              ps.setString(24, vo.getAcquirerResponseId());
              ps.setLong(25, vo.isRerouted() ? 1: 0);
              ps.setLong(26, vo.getReroutingTranId());
              ps.setLong(27, vo.getManualRouted());
	          ps.setLong(28, vo.getId());
	          
	          
	          int updateCount = ps.executeUpdate();
	          if (updateCount == 0) {
					throw new SQLException("No rows updated! Probably the user tries to reverse withdraw twice");
	          }
	          if (logger.isDebugEnabled()) {
	              String ls = System.getProperty("line.separator");
	              logger.log(Level.DEBUG, ls + "Transaction updated:" + vo.toString());
	          }
	      } finally {
	          closeStatement(ps);
	      }
	      return vo.getId();
	  }

    public static  Transaction getById(Connection con, long id) throws SQLException {
        PreparedStatement ps = null;
        ResultSet rs = null;
        Transaction vo = null;
        try {
            String sql =
                "SELECT " +
                    "t.*, " +
                    "ty.description typename, " +
                    "ty.class_type classtype, " +
                    "u.currency_id currency_id, " +
                    "pt.name paymentMethodName, " +
                    "pt.description directProviderName, " +
                    "cp.id_descriptor clearingDescriptor " +
                "FROM " +
                    "transactions t, " +
                    "transaction_types ty, " +
                    "users u, " +
                    "payment_methods pt, " +
                    "clearing_providers cp " +
                "WHERE " +
                    "t.user_id = u.id AND " +
                    "t.type_id = ty.id AND " +
                    "t.payment_type_id = pt.id (+) AND " +
                    "t.clearing_provider_id = cp.id (+) AND " +
                    "t.id = ?";
            ps = con.prepareStatement(sql);
            ps.setLong(1, id);
            rs = ps.executeQuery();
            if (rs.next()) {
            	vo = getVO(con, rs);
            	vo.setPaymentTypeDesc(rs.getString("directProviderName"));
            	vo.setPaymentTypeName(rs.getString("paymentMethodName"));
            	vo.setBonusUserId(rs.getLong("bonus_user_id"));

				if (!vo.isChequeWithdraw()) {

					// set fee_cancel field
					long feeCancel = rs.getLong("fee_cancel");
					if ( feeCancel == ConstantsBase.CC_FEE_CANCEL_NO ) {
						vo.setFeeCancel(false);
					}
					else if ( feeCancel == ConstantsBase.CC_FEE_CANCEL_YES ) {
						vo.setFeeCancel(true);
					}

					if ( vo.getTypeId() == TransactionsManagerBase.TRANS_TYPE_CC_WITHDRAW ) {
						vo.setCc4digit(CreditCardsDAO.getById(con, Long.parseLong(String.valueOf(vo.getCreditCardId()))).getCcNumberLast4());
					}
				}

				long feeCancelByAdmin = rs.getLong("fee_cancel_by_admin");
				if ( feeCancelByAdmin == ConstantsBase.CC_FEE_CANCEL_NO ) {
					vo.setFeeCancelByAdmin(false);
				}
				else if ( feeCancelByAdmin == ConstantsBase.CC_FEE_CANCEL_YES ) {
					vo.setFeeCancelByAdmin(true);
				}
				vo.setClearingProviderDescriptor(ClearingManagerBase.getClearingProviderDescriptor(rs.getLong("clearingDescriptor")));
                return vo;
            }
        } finally {
            closeResultSet(rs);
            closeStatement(ps);
        }
        return vo;
    }

    public static  ArrayList<SelectItem> getTransactionStatusesSI(Connection con) throws SQLException {
        PreparedStatement ps = null;
        ResultSet rs = null;
        ArrayList<SelectItem> list = new ArrayList<SelectItem>();
        try {
            String sql = "select * from transaction_statuses order by id";
            ps = con.prepareStatement(sql);
            rs=ps.executeQuery();
            while (rs.next()) {
                list.add(new SelectItem(rs.getString("id"),rs.getString("description")));
            }
        } finally {
            closeResultSet(rs);
            closeStatement(ps);
        }
        return list;
    }

    public static ArrayList<Transaction> getAll(Connection con) throws SQLException {

		  PreparedStatement ps=null;
		  ResultSet rs=null;
		  ArrayList<Transaction> list=new ArrayList<Transaction>();

		  try
			{
			    String sql="select t.*,ty.description typename,ty.class_type classtype, u.currency_id currency_id from transactions t,transaction_types ty, users u " +
			    		" where t.user_id=u.id and t.type_id=ty.id order by t.time_created desc";
				ps = con.prepareStatement(sql);
				rs=ps.executeQuery();
				while (rs.next()) {
					Transaction t=getVO(con,rs);
					    list.add(t);
				}
			}

			finally
			{
				closeResultSet(rs);
				closeStatement(ps);
			}
			return list;

	  }
    
    protected static Transaction getVO(Connection con, ResultSet rs) throws SQLException{
        Transaction vo = new Transaction();
        vo.setId(rs.getLong("id"));
        vo.setUserId(rs.getLong("user_id"));
        vo.setCreditCardId(rs.getBigDecimal("credit_card_id"));
        vo.setReferenceId(rs.getBigDecimal("reference_id"));
        vo.setTypeId(rs.getLong("type_id"));
        vo.setTimeCreated(convertToDate(rs.getTimestamp("time_created")));
        vo.setTimeSettled(convertToDate(rs.getTimestamp("time_settled")));
        vo.setAmount(rs.getLong("amount"));
        vo.setStatusId(rs.getLong("status_id"));
        vo.setDescription(CommonUtil.getMessage(rs.getString("description"), null));
        vo.setWriterId(rs.getLong("writer_id"));
        vo.setIp(rs.getString("ip"));
        vo.setComments(rs.getString("comments"));
        vo.setProcessedWriterId(rs.getLong("processed_writer_id"));
        vo.setChequeId(rs.getBigDecimal("cheque_id"));
        vo.setWireId(rs.getBigDecimal("wire_id"));
        vo.setChargeBackId(rs.getBigDecimal("charge_back_id"));
        vo.setReceiptNum(rs.getBigDecimal("receipt_num"));
        vo.setTypeName(rs.getString("typename"));
        vo.setClassType(rs.getLong("classtype"));
        vo.setAuthNumber(rs.getString("auth_number"));

        vo.setXorIdAuthorize(rs.getString("xor_id_authorize"));
        vo.setXorIdCapture(rs.getString("xor_id_capture"));
        vo.setCurrencyId(rs.getInt("currency_id"));
        vo.setCurrency(CommonUtil.getAppData().getCurrencyById(rs.getInt("currency_id")));
        vo.setUtcOffsetCreated(rs.getString("utc_offset_created"));
        vo.setUtcOffsetSettled(rs.getString("utc_offset_settled"));
        vo.setClearingProviderId(rs.getLong("clearing_provider_id"));
        vo.setEnvoyAccountNum(rs.getString("ENVOY_ACCOUNT_NUM"));
        vo.setPaymentTypeId(rs.getLong("PAYMENT_TYPE_ID"));
        vo.setAcquirerResponseId(rs.getString("acquirer_response_id"));
        vo.setIsRerouted(rs.getLong("is_rerouted") == 1 ? true : false);
        vo.setReroutingTranId(rs.getLong("rerouting_transaction_id"));
        vo.setManualRouted(rs.getLong("IS_MANUAL_ROUTING"));
        vo.setSelectorId(rs.getInt("selector_id"));
        vo.setPayPalEmail(rs.getString("paypal_email"));
        vo.setMoneybookersEmail(rs.getString("moneybookers_email"));
        vo.setThreeD(rs.getLong("is_3d") == 1 ? true : false);
        return vo;
    }



	/**
	 * Get sum deposits of user
	 * this is for deposit limitation for anyoption
	 * we take only riski transaction type, (is_risky=1)
	 * @param con
	 * 		Db connection
	 * @param userId
	 * 		user id for taking his deposits
	 * @return
	 * 		sum deposits of the user
	 * @throws SQLException
	 */
	public static  long getSumDepositsForLimitation(Connection con,long userId) throws SQLException {
	
		  PreparedStatement ps = null;
		  ResultSet rs = null;
		  long sumDeposits = 0;
	
		  try {
			    String sql = "select sum(t.amount) sum_deposits " +
			    		     "from transactions t, transaction_types tt " +
			    		     "where t.user_id = ? and t.type_id = tt.id and " +
			    		     "tt.class_type = ? and " +
			    		     "tt.is_risky = 1 and " +
			    		     "t.status_id in ("+TransactionsManagerBase.TRANS_STATUS_SUCCEED+","+TransactionsManagerBase.TRANS_STATUS_PENDING+")";
	
				ps = con.prepareStatement(sql);
				ps.setLong(1, userId);
				ps.setLong(2, TransactionsManagerBase.TRANS_CLASS_DEPOSIT);
	
				rs = ps.executeQuery();
	
				if ( rs.next() ) {
					sumDeposits = rs.getLong("sum_deposits");
				}
	
			} finally {
				closeResultSet(rs);
				closeStatement(ps);
			}
	
			return sumDeposits;
	}
	/**
	 * Get direct deposit payment types
	 * @param conn db connection
	 * @return
	 * @throws SQLException
	 */
    public static  HashMap<Long,SelectItem> getDirectDepositPaymentTypes(Connection conn) throws SQLException {
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        HashMap<Long,SelectItem> list = new HashMap<Long,SelectItem>();

        try {
            String sql =
                " SELECT " +
                    " *" +
                " FROM " +
                    " payment_methods " +
                " WHERE" +
                	" IS_INATEC_DIRECT_PAYMENT = 1";

            pstmt = conn.prepareStatement(sql);
            rs = pstmt.executeQuery();

            while (rs.next()) {
            	long id = rs.getLong("id");
                SelectItem item = new SelectItem(new Long(id), rs.getString("description"));
                list.put(new Long(id), item);

            }
        } finally {
            closeResultSet(rs);
            closeStatement(pstmt);
        }
        return list;
    }
	/**
	 * Get WebMoney purses list in order to withdraw
	 * @param conn db connection
	 * @param userId
	 * @return
	 * @throws SQLException
	 */
    public static  ArrayList<SelectItem> getWebMoneyPursesByUserId(Connection conn, long userId) throws SQLException {
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        ArrayList<SelectItem> list = new ArrayList<SelectItem>();
        try {
            String sql =
                " SELECT " +
                	" wd.lmi_payer_purse, " +
                	" max(t.time_created) " +
                " FROM " +
                	" transactions t, " +
                	" webmoney_deposit wd " +
                " WHERE " +
                	" t.user_id = ? " +
                	" AND t.id = wd.transaction_id " +
                	" AND t.status_id in (" + TransactionsManagerBase.SUCCESS_DEPOSITS_TYPES + ") " +
                	" AND t.type_id = " + TransactionsManagerBase.TRANS_TYPE_WEBMONEY_DEPOSIT +
                " GROUP BY " +
                	" wd.lmi_payer_purse " +
                " ORDER BY " +
                	" max(t.time_created) desc ";
            pstmt = conn.prepareStatement(sql);
            pstmt.setLong(1, userId);

            rs = pstmt.executeQuery();

            while (rs.next()) {
            	String payerPurse = rs.getString("lmi_payer_purse");
            	list.add(new SelectItem(payerPurse, payerPurse));
            }
        } finally {
            closeResultSet(rs);
            closeStatement(pstmt);
        }
        return list;
    }
    


	public static Date getFirstDepositWithCC(Connection conn, long userId, long ccId) throws SQLException {
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		
		try {
		    String sql =
		    			"SELECT " + 
		    					"time_created " + 
		    			"FROM " + 
		    					"transactions " + 
						"WHERE " + 
								"user_id      		= ? " + 
								"AND credit_card_id = ? " + 
								"AND rownum = 1 " + 
						"ORDER BY " + 
								"time_created ";
		
		    pstmt = conn.prepareStatement(sql);
		    pstmt.setLong(1, userId);
		    pstmt.setLong(2, ccId);		    
		    rs = pstmt.executeQuery();
		    if (rs.next()) {
		    	return convertToDate(rs.getTimestamp("time_created"));
		    }
		} finally {
		    closeResultSet(rs);
		    closeStatement(pstmt);
		}
		return null;
	}
	
	/**
	 * Get first time deposit to fire server pixel
	 * @param con
	 * @param fireServerPixelFields
	 * @return ArrayList<FireServerPixelHelper>
	 * @throws SQLException
	 */
	public static ArrayList<FireServerPixelHelper> getFtdToFireServerPixel(Connection con, FireServerPixelFields fireServerPixelFields) throws SQLException {
		CallableStatement cstmt = null;
		ResultSet rs = null;
		ArrayList<FireServerPixelHelper> list = new ArrayList<FireServerPixelHelper>();
		int index = 1;
		try {
			cstmt = con.prepareCall("{call pkg_server_pixels.get_deposit_info(o_deposits => ? "+
																   ",i_from_date => ? "+
																   ",i_to_date => ?)}");
			cstmt.registerOutParameter(index++, OracleTypes.CURSOR);
			cstmt.setTimestamp(index++, CommonUtil.convertToTimeStamp(fireServerPixelFields.getFromDate()));
			cstmt.setTimestamp(index++, CommonUtil.convertToTimeStamp(fireServerPixelFields.getToDate()));
			cstmt.executeQuery();
			rs = (ResultSet) cstmt.getObject(1);
			while (rs.next()) {
				FireServerPixelHelper fireServerPixelHelper = new FireServerPixelHelper();
				fireServerPixelHelper.setUserId(rs.getLong("id"));
				fireServerPixelHelper.setPlatformId(rs.getLong("platform_id"));
				fireServerPixelHelper.setTransactionId(rs.getLong("tran_id"));
				fireServerPixelHelper.setIdfa(rs.getString("idfa"));
				fireServerPixelHelper.setAdvertisingId(rs.getString("advertising_id"));
				list.add(fireServerPixelHelper);
			}
		} finally {
			closeResultSet(rs);
			closeStatement(cstmt);
		}
		return list;
	}

}