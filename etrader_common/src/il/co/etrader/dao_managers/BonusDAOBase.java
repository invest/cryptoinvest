package il.co.etrader.dao_managers;


import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.Bonus;
import com.anyoption.common.beans.base.BonusUsers;
import com.anyoption.common.bl_vos.BonusCurrency;
import com.anyoption.common.bonus.BonusHandlerBase;
import com.anyoption.common.bonus.BonusHandlerFactory;
import com.anyoption.common.util.BonusFormulaDetails;
import com.anyoption.common.util.CommonUtil;
import com.anyoption.common.util.ConstantsBase;

/**
 * BonusDAOBase class.
 *
 * @author Eliran
 */
/**
 * @author liors
 *
 */
public class BonusDAOBase extends com.anyoption.common.daos.BonusDAOBase {

	private static final Logger log = Logger.getLogger(BonusDAOBase.class);



//    /**
//     * Update state id of the bonus
//     * @param con db connection
//     * @param id bonus users id
//     * @param stateId bonus state id for update
//     * @throws SQLException
//     */
//    public static void updateState(Connection con, long id, long stateId, long writerIdCancel) throws SQLException {
//
//        PreparedStatement ps = null;
//        try {
//            String sql = " UPDATE " +
//            				" bonus_users " +
//            		     " SET " +
//            		     	" bonus_state_id = ? ";
//
//            if (ConstantsBase.BONUS_STATE_CANCELED == stateId){
//            	sql += 		" ,TIME_CANCELED = sysdate ";
//            }else if (ConstantsBase.BONUS_STATE_REFUSED == stateId){
//            	sql += 		" ,TIME_REFUSED = sysdate ";
//            } else if (ConstantsBase.BONUS_STATE_WITHDRAWN == stateId) { //TODO need to be refactor - contain time status change in other table. 
//            	sql += 		" ,TIME_WITHDRAWN = sysdate ";
//            } else if (ConstantsBase.BONUS_STATE_WAGERING_WAIVED == stateId) {
//            	sql += 		" ,TIME_WAGERING_WAIVED = sysdate ";
//            }
//            
//            if (stateId == ConstantsBase.BONUS_STATE_CANCELED || 
//            		stateId == ConstantsBase.BONUS_STATE_WITHDRAWN || 
//            		stateId == ConstantsBase.BONUS_STATE_WAGERING_WAIVED) {
//            	sql += 		" ,WRITER_ID_CANCEL = " + writerIdCancel + " ";
//            }
//            
//            sql +=
//                        " WHERE " +
//                            " id = ?";
//
//            ps = con.prepareStatement(sql);
//
//            ps.setLong(1, stateId);
//            ps.setLong(2, id);
//            ps.executeUpdate();
//
//        } finally {
//            closeStatement(ps);
//        }
//    }

    /**
     * Check if user can withdraw.
     * User cannot withdraw in case : 1. have bonuses with used state.
     * 								  2. have bonuses with active state and amountForWithdraw > (balance - sumActiveAmount)
     * @param userId user id
     * @param con db connection
     * @return true only if user dont have any bonus in state used else false
     * @throws SQLException
     */
    public static boolean userCanWithdraw(Connection con, long userId, long balance, long amount) throws SQLException {
    	boolean canWithdraw = true;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
	    long sumWageringAmount = 0;

	    try{
		    String sql =
		            " SELECT " +
		            	" bu.*," +
		            	" bt.class_type_id, " +
		            	" i.win, " +
		            	" i.lose, " +
		            	" i.amount inv_amount, " +
		            	" i.is_settled " +
		            " FROM " +
		            	" bonus_types bt, " +
		            	" bonus_users bu " +
		            		" left join investments i on i.bonus_user_id = bu.id " +
		            " WHERE " +
		            	" bu.type_id = bt.id " +
		        		" AND bu.user_id = ? ";

		    pstmt = con.prepareStatement(sql);
		    pstmt.setLong(1, userId);
		    rs = pstmt.executeQuery();

			while (rs.next()) {
				BonusUsers bu = getBonusUsersVO(rs, con);

				if (ConstantsBase.BONUS_CLASS_TYPE_NEXT_INVEST_ON_US == rs.getLong("class_type_id")) {

					if (bu.getBonusStateId() == ConstantsBase.BONUS_STATE_USED && rs.getInt("IS_SETTLED") == 1) {
						BonusHandlerBase bh = BonusHandlerFactory.getInstance(bu.getTypeId());
						long win = rs.getLong("win");
						long lose = rs.getLong("lose");
						long invAmount = rs.getLong("inv_amount");

						sumWageringAmount += bh.getAmountThatUserCantWithdraw(win, lose, invAmount, bu);
					}

				} else {
					if (bu.getBonusStateId() == ConstantsBase.BONUS_STATE_USED) {
						canWithdraw = false;
						break;
					} else if (bu.getBonusStateId() == ConstantsBase.BONUS_STATE_ACTIVE) {
						sumWageringAmount += bu.getBonusAmount();
					}
				}
			}

		    if (canWithdraw && sumWageringAmount > 0) {  // have active bonuses and don't have used bonuses
		    	if (amount > (balance - sumWageringAmount) ) {
		    		canWithdraw = false;
		    	}
		    }

		} finally {
		   closeStatement(pstmt);
		   closeResultSet(rs);
		}
		return canWithdraw;
	}

    public static BonusUsers getBonusUserByBonusId(Connection con, long userId, long bonusId) throws SQLException {
    	BonusUsers bu = null;
    	PreparedStatement pstmt = null;
		ResultSet rs = null;
    	try{
		    String sql =
		            "SELECT " +
		            	"bu.* " +
		            "FROM " +
		            	"bonus_users bu " +
		            "WHERE " +
		        		"bu.user_id = ? AND " +
		            	"bu.bonus_id = ? ";

		    pstmt = con.prepareStatement(sql);
		    pstmt.setLong(1, userId);
		    pstmt.setLong(2, bonusId);
		    rs = pstmt.executeQuery();

		    if (rs.next()) {
		    	bu = new BonusUsers();
		    	bu = getBonusUsersVO(rs, con);
		    }
    	} finally {
		   closeStatement(pstmt);
		   closeResultSet(rs);
		}
		return bu;
    }

    public static long hasNextInvestOnUs(Connection conn, long userId) throws SQLException {
        long id = 0;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            String sql =
                    "SELECT " +
                        "bu.id " +
                    "FROM " +
                        "bonus_users bu, " +
                        "bonus_types bt " +
                    "WHERE " +
                        "bu.user_id = ? AND " +
                        "bt.class_type_id = " + ConstantsBase.BONUS_CLASS_TYPE_NEXT_INVEST_ON_US + " AND " +
                        "bu.bonus_state_id = " + ConstantsBase.BONUS_STATE_ACTIVE + " AND " +
                        "bt.id = bu.type_id AND "+
                        "(bu.start_date IS NULL OR bu.start_date <= current_date) AND " +
                        "(bu.end_date IS NULL OR bu.end_date >= current_date) " +
                    "ORDER BY " +
                        "bu.id";
            pstmt = conn.prepareStatement(sql);
            pstmt.setLong(1, userId);
            rs = pstmt.executeQuery();
            if (rs.next()) {
                id = rs.getLong("id");
            }
        } finally {
            closeResultSet(rs);
            closeStatement(pstmt);
        }
        return id;
    }

    public static ArrayList<BonusUsers> getBonusesForActivationByType(Connection conn, long userId, long activationType) throws SQLException {
        ArrayList<BonusUsers> list = new ArrayList<BonusUsers>();
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try{
            String sql =
                    " SELECT " +
                        " * " +
                    " FROM " +
                        " bonus_users bu," +
                        " bonus_types bt " +
                    " WHERE " +
                        " bu.user_id = ? " +
                        " AND bu.bonus_state_id = " + ConstantsBase.BONUS_STATE_GRANTED + " " +
                        " AND (bu.start_date IS NULL OR bu.start_date <= current_date) " +
                        " AND(bu.end_date IS NULL OR bu.end_date >= current_date) " +
                        " AND bu.type_id = bt.id " +
                        " AND bt.activation_type_id = " + activationType + " " +
                    " ORDER BY " +
                        " time_created ";
            pstmt = conn.prepareStatement(sql);
            pstmt.setLong(1, userId);
            rs = pstmt.executeQuery();
            while (rs.next()) {
            	BonusUsers bu = getBonusUsersVO(rs, conn);
            	bu.setBonusClassType(rs.getLong("class_type_id"));
                list.add(bu);
            }
        } finally {
            closeResultSet(rs);
            closeStatement(pstmt);
        }
        return list;
    }

//    public static ArrayList<BonusUsers> getBonusesForActivation(Connection conn, long userId) throws SQLException {
//        ArrayList<BonusUsers> list = new ArrayList<BonusUsers>();
//        PreparedStatement pstmt = null;
//        ResultSet rs = null;
//        try{
//            String sql =
//                    " SELECT " +
//                        " * " +
//                    " FROM " +
//                        " bonus_users bu," +
//                        " bonus_types bt " +
//                    " WHERE " +
//                        " bu.user_id = ? " +
//                        " AND (bu.bonus_state_id = " + ConstantsBase.BONUS_STATE_GRANTED + " " +
//                        		" OR (bu.bonus_state_id = " + ConstantsBase.BONUS_STATE_ACTIVE + " AND bu.bonus_id = " + ConstantsBase.DYNAMIC_DEPOSIT_GET_AMOUNT_AFTER_SUM_INVEST_ID + ") " +
//                        		" OR (bu.bonus_state_id = " + ConstantsBase.BONUS_STATE_ACTIVE + " AND bu.bonus_id = " + ConstantsBase.DYNAMIC_AMOUNT_AFTER_SUM_INVEST_ID + ")) " +
//                        " AND (bu.start_date IS NULL OR bu.start_date <= current_date) " +
//                        " AND(bu.end_date IS NULL OR bu.end_date >= current_date) " +
//                        " AND bu.type_id = bt.id " +
//                    " ORDER BY " +
//                        " time_created ";
//            pstmt = conn.prepareStatement(sql);
//            pstmt.setLong(1, userId);
//            rs = pstmt.executeQuery();
//            while (rs.next()) {
//            	BonusUsers bu = getBonusUsersVO(rs, conn);
//            	bu.setBonusClassType(rs.getLong("class_type_id"));
//            	bu.setTurnoverParam(rs.getLong("turnover_param"));
//                list.add(bu);
//            }
//        } finally {
//            closeResultSet(rs);
//            closeStatement(pstmt);
//        }
//        return list;
//    }


    public static ArrayList<BonusUsers> getActiveAndUsedWithWagering(Connection conn, long userId) throws SQLException {
        ArrayList<BonusUsers> list = new ArrayList<BonusUsers>();
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try{
            String sql =
                    "SELECT " +
                        "* " +
                    "FROM " +
                        "bonus_users " +
                    "WHERE " +
                        "user_id = ? AND " +
                        "bonus_state_id IN (" + ConstantsBase.BONUS_STATE_ACTIVE + ", " + ConstantsBase.BONUS_STATE_USED + ") AND " +
                        "NOT sum_invest_withdraw IS NULL AND " +
                        "sum_invest_withdraw > 0 " +
                    "ORDER BY " +
                        "time_created";
            pstmt = conn.prepareStatement(sql);
            pstmt.setLong(1, userId);
            rs = pstmt.executeQuery();
            while (rs.next()) {
                list.add(getBonusUsersVO(rs, conn));
            }
        } finally {
            closeResultSet(rs);
            closeStatement(pstmt);
        }
        return list;
    }

	public static boolean isHasBonusActiveOrGranted(Connection con, long id) throws SQLException {
		boolean flag = false;
		PreparedStatement pstmt = null;
        ResultSet rs = null;
        try{
        	String sql =
        		"SELECT " +
		             "* " +
		         "FROM " +
		             "bonus_users " +
		         "WHERE " +
		             "user_id = ? AND " +
		             "bonus_state_id IN (" + ConstantsBase.BONUS_STATE_GRANTED  + ", " + ConstantsBase.BONUS_STATE_ACTIVE + ") AND " +
		             "(start_date IS NULL OR start_date <= sysdate) AND " +
		             "(end_date IS NULL OR end_date >= sysdate) AND " +
		             "type_id <> ? ";

            pstmt = con.prepareStatement(sql);
            pstmt.setLong(1, id);
            pstmt.setLong(2, ConstantsBase.BONUS_TYPE_CONVERT_POINTS_TO_CASH);
            rs = pstmt.executeQuery();

            if (rs.next()) {
                flag = true;
            }
        } finally {
            closeResultSet(rs);
            closeStatement(pstmt);
        }
		return flag;
	}

	public static String getDescriptionByTypeAndState (Connection con,long bonusId,long stateId) throws SQLException{
    	String description = "";
    	PreparedStatement stmt = null;
    	ResultSet rs = null;
    	try{
    		String sql = "SELECT "+
    					"message "+
    					"FROM " +
    					"bonus_type_state_msg btsm,bonus b"+
    					" WHERE "+
    					"type=b.type_id and state=? and b.id=?";
    		stmt = con.prepareStatement(sql);
    		stmt.setLong(1, stateId);
    		stmt.setLong(2, bonusId);
    		rs = stmt.executeQuery();
    		if(rs.next()){
    			description = rs.getString("message");
    		}
    	} finally{
    		closeResultSet(rs);
    		closeStatement(stmt);
    	}
    	return description;
    }

	/**
     * Get min and max values for free investment
     * @param conn
     * @param userId
     * @return
     * 		HashMap with min & max values
     * @throws SQLException
     */
    public static HashMap<String,Long> getMinAndMaxFreeInv(Connection conn, long bonusUserId) throws SQLException {

    PreparedStatement pstmt = null;
    ResultSet rs = null;
    HashMap<String,Long> freeInvValues = new HashMap<String,Long>();

    try {

        String sql = " select " +
        				" min_invest_amount, " +
        				" max_invest_amount " +
        			 " from " +
        			 	" bonus_users  " +
        			 " where " +
        			 	" id = ? ";

        pstmt = conn.prepareStatement(sql);
        pstmt.setLong(1, bonusUserId);

        rs = pstmt.executeQuery();

        if ( rs.next() ) {
            freeInvValues.put(ConstantsBase.FREE_INVEST_MIN, rs.getLong("min_invest_amount"));
            freeInvValues.put(ConstantsBase.FREE_INVEST_MAX, rs.getLong("max_invest_amount"));
        }

    } finally {
        closeResultSet(rs);
        closeStatement(pstmt);
    }

    return freeInvValues;
}

	/**
	 * Insert new converted points bonus to user
	 * @param con db connection
	 * @param bu bonusUser instance
	 * @param currencyId
	 * @return
	 * @throws SQLException
	 */
	public static boolean insertConvertedPointsBonus(Connection con, BonusUsers bu, long currencyId) throws SQLException {

		PreparedStatement ps = null;
		Timestamp timeActive = null;
		try {
			String sql="INSERT " +
						"INTO bonus_users(ID, " +
										  "USER_ID," +
										  "START_DATE," +
										  "END_DATE," +
										  "BONUS_STATE_ID," +
										  "BONUS_AMOUNT," +
										  "BONUS_PERCENT," +
										  "MIN_DEPOSIT_AMOUNT," +
										  "MAX_DEPOSIT_AMOUNT," +
										  "SUM_INVEST_QUALIFY," +
										  "WRITER_ID," +
										  "TIME_CREATED," +
										  "SUM_INVEST_QUALIFY_REACHED," +
										  "SUM_INVEST_WITHDRAW," +
										  "SUM_INVEST_WITHDRAW_REACHED," +
										  "TIME_ACTIVATED," +
										  "TIME_USED," +
										  "TIME_DONE," +
										  "TYPE_ID," +
										  "WAGERING_PARAMETER," +
										  "NUMBER_OF_ACTIONS," +
										  "NUMBER_OF_ACTIONS_REACHED," +
										  "BONUS_ID, " +
										  "ADJUSTED_AMOUNT) " +
						"VALUES(SEQ_BONUS_USERS.nextval,?,sysdate,?,?,?,?,?,?,?," +
								"?,sysdate,?,?,?,?,?,?,?,?,?,?,?,?)";

			// get Bonus data for insert
			Bonus b = getBonusById(con, bu.getBonusId());
			//BonusCurrency bc = getBonusCurrency(con, bu.getBonusId(), currencyId);

			if (null == b) {
				return false;
			}

			long sumInvestWithdrawal = bu.getBonusAmount() * b.getWageringParameter();
			timeActive = CommonUtil.convertToTimeStamp(new Date());

			ps = con.prepareStatement(sql);
			ps.setLong(1, bu.getUserId());
			ps.setTimestamp(2, null);
			ps.setLong(3, bu.getBonusStateId());
			ps.setLong(4, bu.getBonusAmount());
			ps.setDouble(5, 0);
			ps.setLong(6, 0);
			ps.setLong(7, 0);
			ps.setLong(8, 0);
			ps.setLong(9, bu.getWriterId());
			ps.setLong(10, bu.getSumInvQualifyReached());
			ps.setLong(11, sumInvestWithdrawal);
			ps.setLong(12, bu.getSumInvWithdrawalReached());
			ps.setTimestamp(13, timeActive);
			ps.setTimestamp(14, null);
			ps.setTimestamp(15, null);
			ps.setLong(16, b.getTypeId());
			ps.setLong(17, b.getWageringParameter());
			ps.setLong(18, b.getNumberOfActions());
			ps.setLong(19, bu.getNumOfActionsReached());
			ps.setLong(20, bu.getBonusId());
			ps.setLong(21, bu.getBonusAmount());

			ps.executeUpdate();

			bu.setId(getSeqCurValue(con, "SEQ_BONUS_USERS"));
		}
		finally {
			closeStatement(ps);
		}

		return true;
	}

	/**
	 * Get convert points bonusId by skin id
	 * @param con
	 * @param skinId
	 * @return
	 * @throws SQLException
	 */
	public static long getConvertPointsBonusIdBySkin(Connection con, long skinId) throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		long bonusId = 0;

		try	{
		    String sql = "SELECT " +
		    				"b.id bonus_id " +
		    			 "FROM " +
		    			 	"bonus b, " +
		    			 	"bonus_skins bs, " +
		    			 	"bonus_types bt " +
		    			 "WHERE " +
		    			 	"b.id = bs.bonus_id AND " +
		    			 	"bs.skin_id = ? AND " +
		    			 	"bt.id = ? AND " +
		    			 	"b.type_id = bt.id";

			ps = con.prepareStatement(sql);
			ps.setLong(1, skinId);
			ps.setLong(2, ConstantsBase.BONUS_TYPE_CONVERT_POINTS_TO_CASH);

			rs = ps.executeQuery();

			if ( rs.next() ) {
				bonusId = rs.getLong("bonus_id");
			}
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		return bonusId;
	}

//	/**
//	 * insert bonus to user
//	 * @param con db connection
//	 * @param popEntryId TODO
//	 * @throws SQLException
//	 */
//	public static boolean insertBonusUser(Connection con, BonusUsers bu, Bonus b,BonusCurrency bc, long popEntryId, boolean isNeedToSendInternalMail) throws SQLException {
//		PreparedStatement ps = null;
//		try {
//			String sql="INSERT " +
//						"INTO bonus_users(ID, " +
//										  "USER_ID," +
//										  "START_DATE," +
//										  "END_DATE," +
//										  "BONUS_STATE_ID," +
//										  "BONUS_AMOUNT," +
//										  "BONUS_PERCENT," +
//										  "MIN_DEPOSIT_AMOUNT," +
//										  "MAX_DEPOSIT_AMOUNT," +
//										  "SUM_INVEST_QUALIFY," +
//										  "WRITER_ID," +
//										  "TIME_CREATED," +
//										  "SUM_INVEST_QUALIFY_REACHED," +
//										  "SUM_INVEST_WITHDRAW," +
//										  "SUM_INVEST_WITHDRAW_REACHED," +
//										  "TIME_ACTIVATED," +
//										  "TIME_USED," +
//										  "TIME_DONE," +
//										  "TYPE_ID," +
//										  "WAGERING_PARAMETER," +
//										  "NUMBER_OF_ACTIONS," +
//										  "NUMBER_OF_ACTIONS_REACHED," +
//										  "BONUS_ID, " +
//										  "min_invest_amount, " +
//										  "max_invest_amount, " +
//										  "odds_win, " +
//										  "odds_lose," +
//										  "SUM_DEPOSITS," +
//										  "POPULATION_ENTRY_ID," +
//										  "TURNOVER_PARAM," +
//										  "ADJUSTED_AMOUNT) " +
//						"VALUES(SEQ_BONUS_USERS.nextval,?,sysdate,?,?,?,?,?,?,?," +
//								"?,sysdate,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
//
//			// calculate end date
//			Calendar cal = Calendar.getInstance();
//			cal.set(Calendar.HOUR_OF_DAY, 21);
//            cal.set(Calendar.MINUTE, 0);
//            cal.set(Calendar.SECOND, 0);
//
//			if (bu.getBonusId() != ConstantsBase.XMASS_BONUS_ID ){ // not xmass bonus
//				cal.add(Calendar.DAY_OF_MONTH, ((int)b.getDefaultPeriod()));
//			}else{ // xmass bonus
//	            cal.set(GregorianCalendar.DAY_OF_MONTH, 31);
//			}
//
//			ps = con.prepareStatement(sql);
//			ps.setLong(1, bu.getUserId());
//			ps.setTimestamp(2, CommonUtil.convertToTimeStamp(cal.getTime()));
//			ps.setLong(3, bu.getBonusStateId());
//			ps.setLong(4, bc.getBonusAmount());
//			ps.setDouble(5, bc.getBonusPercent());
//			ps.setLong(6, bc.getMinDepositAmount());
//			ps.setLong(7, bc.getMaxDepositAmount());
//			ps.setLong(8, bc.getSumInvestQualify());
//			ps.setLong(9, bu.getWriterId());
//			ps.setLong(10, bu.getSumInvQualifyReached());
//			ps.setLong(11, bu.getSumInvWithdrawal());
//			ps.setLong(12, bu.getSumInvWithdrawalReached());
//			ps.setTimestamp(13, CommonUtil.convertToTimeStamp(bu.getTimeActivated()));
//			ps.setTimestamp(14, null);
//			ps.setTimestamp(15, CommonUtil.convertToTimeStamp(bu.getTimeDone()));
//			ps.setLong(16, b.getTypeId());
//			ps.setLong(17, b.getWageringParameter());
//			ps.setLong(18, b.getNumberOfActions());
//			ps.setLong(19, bu.getNumOfActionsReached());
//			ps.setLong(20, bu.getBonusId());
//			ps.setLong(21, bc.getMinInvestAmount());
//			ps.setLong(22, bc.getMaxInvestAmount());
//			ps.setDouble(23, b.getOddsWin());
//			ps.setDouble(24, b.getOddsLose());
//			ps.setLong(25, bu.getSumDeposits());
//			if (popEntryId > 0){
//				ps.setLong(26, popEntryId);
//			}else{
//				ps.setString(26, null);
//			}
//			ps.setLong(27, b.getTurnoverParam());
//			ps.setLong(28, bc.getBonusAmount());
//			ps.executeUpdate();
//
//			// save needed values in bonus user instance
//			bu.setTypeId(b.getTypeId());
//			bu.setBonusAmount(bc.getBonusAmount());
//			bu.setId(getSeqCurValue(con, "SEQ_BONUS_USERS"));
//
//			// Insert new email
//			try {
//				int skinId = (int)UsersDAOBase.getSkinIdByUserId(con, bu.getUserId());
//				if (/*!CommonUtil.isHebrewSkin(skinId) && */isNeedToSendInternalMail) {
//					int langId = SkinsDAOBase.getById(con, skinId).getDefaultLanguageId();
//					MailBoxTemplate template = MailBoxTemplatesDAOBase.getTemplateByTypeSkinLanguage(con, ConstantsBase.MAILBOX_TYPE_BONUS, skinId, langId);
//					MailBoxUser email = new MailBoxUser();
//					email.setTemplateId(template.getId());
//					email.setUserId(bu.getUserId());
//					email.setWriterId(bu.getWriterId());
//					email.setSenderId(template.getSenderId());
//					email.setIsHighPriority(template.getIsHighPriority());
//					email.setPopupTypeId(template.getPopupTypeId());
//					email.setSubject(template.getSubject());
//					email.setBonusUserId(bu.getId());
//					MailBoxUsersDAOBase.insert(con, email);
//				}
//			} catch (Exception e) {
//				log.warn("Problem sending a new email to user inbox!", e);
//			}
//		} finally {
//			closeStatement(ps);
//		}
//		return true;
//	}

//	/**
//	 * Get bonusCurrencyId list by bonus id and currency id
//	 * @return
//	 * @throws SQLException
//	 */
//	public static ArrayList<BonusUsersStep> getBonusSteps(Connection con, long bonusId, long currencyId) throws SQLException {
//		PreparedStatement ps = null;
//		ResultSet rs = null;
//		ArrayList<BonusUsersStep> list =  new ArrayList<BonusUsersStep>();
//
//		try	{
//		    String sql = "SELECT * " +
//		    			 "FROM " +
//		    			 	"bonus_currency " +
//		    			 "WHERE " +
//		    			 	"bonus_id = ? AND " +
//		    			 	"currency_id = ? " +
//		    			 " ORDER BY step ";
//
//			ps = con.prepareStatement(sql);
//			ps.setLong(1, bonusId);
//			ps.setLong(2, currencyId);
//			rs = ps.executeQuery();
//
//			while ( rs.next() ) {
//				BonusUsersStep temp = new BonusUsersStep();
//		    	temp.setBonusPercent(rs.getDouble("bonus_percent"));
//		    	temp.setMinDepositAmount(rs.getLong("min_deposit_amount"));
//		    	temp.setMaxDepositAmount(rs.getLong("max_deposit_amount"));
//				list.add(temp);
//			}
//		} finally {
//			closeResultSet(rs);
//			closeStatement(ps);
//		}
//		return list;
//	}

	/**
	 * Get bonus currency by bonus id and currency id
	 * @return
	 * @throws SQLException
	 */
	public static BonusCurrency getBonusCurrency(Connection con, long bonusId, long currencyId) throws SQLException {
		BonusCurrency bc = null;
		PreparedStatement ps = null;
		ResultSet rs = null;

		try	{
		    String sql = "SELECT * FROM ( " +
		    				"SELECT * FROM bonus_currency WHERE bonus_id = ? AND currency_id = ? " +
		    				"ORDER BY step) " +
		    			 "WHERE rownum = 1";

			ps = con.prepareStatement(sql);
			ps.setLong(1, bonusId);
			ps.setLong(2, currencyId);
			rs = ps.executeQuery();

			if ( rs.next() ) {
				bc = getBCVO(rs);
			}
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		return bc;
	}


	/**
	 * Fill BonusCurrency
	 * @return
	 * 		BonusCurrency object
	 * @throws SQLException
	 */
	protected static BonusCurrency getBCVO(ResultSet rs) throws SQLException {
		BonusCurrency vo = new BonusCurrency();
		vo.setId(rs.getLong("ID"));
		vo.setBonusId(rs.getLong("BONUS_ID"));
		vo.setCurrencyId(rs.getLong("CURRENCY_ID"));
		vo.setBonusAmount(rs.getLong("BONUS_AMOUNT"));
		vo.setBonusPercent(rs.getDouble("BONUS_PERCENT"));
		vo.setMinDepositAmount(rs.getLong("MIN_DEPOSIT_AMOUNT"));
		vo.setMaxDepositAmount(rs.getLong("MAX_DEPOSIT_AMOUNT"));
		vo.setSumInvestQualify(rs.getLong("SUM_INVEST_QUALIFY"));
		vo.setWriterId(rs.getLong("WRITER_ID"));
		vo.setTimeCreated(convertToDate(rs.getTimestamp("TIME_CREATED")));
		vo.setSumInvestWithdraw(rs.getLong("SUM_INVEST_WITHDRAW"));
		vo.setStep(rs.getLong("STEP"));
		vo.setMinInvestAmount(rs.getLong("min_invest_amount"));
		vo.setMaxInvestAmount(rs.getLong("max_invest_amount"));
		vo.setMaxInvestAmount(rs.getLong("max_invest_amount"));
		vo.setSumDeposits(rs.getLong("SUM_DEPOSITS"));
		return vo;
	}

	public static BonusUsers getLastBonusForUser(Connection conn, long userId) throws SQLException {
        BonusUsers bUser = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try{
            String sql =
                    "SELECT " +
                        "* " +
                    "FROM " +
                        "bonus_users " +
                    "WHERE " +
                        "user_id = ? AND " +
                        "bonus_state_id IN (" + ConstantsBase.BONUS_STATE_GRANTED + ", "
                        						+ ConstantsBase.BONUS_STATE_ACTIVE + ", " +
                        						+ ConstantsBase.BONUS_STATE_USED   + ", " +
                        						+ ConstantsBase.BONUS_STATE_DONE + ")" +
                    "ORDER BY " +
                        "time_created desc";
            pstmt = conn.prepareStatement(sql);
            pstmt.setLong(1, userId);
            rs = pstmt.executeQuery();
            if (rs.next()) {
            	bUser = getBonusUsersVO(rs, conn);
            }
        } finally {
            closeResultSet(rs);
            closeStatement(pstmt);
        }
        return bUser;
    }

//    /**
//     * Load all bonus population limits
//     * @param con
//     * @return
//     * @throws SQLException
//     */
//    public static ArrayList<BonusPopulationLimit> getBonusPopulationLimits(Connection con, long popEntryId, long bonusId, long currencyId) throws SQLException {
//        PreparedStatement ps = null;
//        ResultSet rs = null;
//        ArrayList<BonusPopulationLimit> list = new ArrayList<BonusPopulationLimit>();
//        try	{
//            String sql =
//            	" SELECT " +
//            		" bp.bonus_id, " +
//            		" bp.population_type_id, " +
//            		" bp.update_type, " +
//            		" bpl.* " +
//            	" FROM " +
//            		" bonus_populations bp, " +
//            		" bonus_population_limits bpl," +
//            		" populations p," +
//            		" population_entries pe " +
//            	" WHERE " +
//            		" bpl.bonus_population_id = bp.id " +
//            		" AND bp.population_type_id = p.population_type_id " +
//            		" AND p.id = pe.population_id " +
//            		" AND pe.id = " + popEntryId + " " +
//            		" AND bp.bonus_id = " + bonusId + " " +
//            		" AND bpl.currency_id = " + currencyId + " " +
//            	" ORDER BY " +
//            		" bpl.bonus_population_id, " +
//            		" bpl.currency_id, " +
//            		" bpl.level_no ";
//
//            ps = con.prepareStatement(sql);
//            rs = ps.executeQuery();
//            while (rs.next()) {
//            	BonusPopulationLimit bpl = new BonusPopulationLimit();
//            	bpl.setId(rs.getLong("id"));
//            	bpl.setBonusId(rs.getLong("bonus_id"));
//            	bpl.setPopTypeId(rs.getLong("population_type_id"));
//            	bpl.setCurrencyId(rs.getLong("currency_id"));
//            	bpl.setMinDepositParam(rs.getLong("min_deposit_param"));
//            	bpl.setMaxDepositParam(rs.getLong("max_deposit_param"));
//            	bpl.setMinResult(rs.getLong("min_result"));
//            	bpl.setMultiplication(rs.getLong("multiplication"));
//            	bpl.setLevel(rs.getLong("level_no"));
//            	bpl.setStepRangeMul(rs.getLong("STEP_RANGE_MUL"));
//            	bpl.setStepLevelMul(rs.getLong("STEP_LEVEL_MUL"));
//            	bpl.setStepBonusAddition(rs.getDouble("STEP_BONUS_ADDITION"));
//            	bpl.setMinBonusPercent(rs.getDouble("MIN_BONUS_PERCENT"));
//            	bpl.setLimitUpdateType(rs.getInt("UPDATE_TYPE"));
//            	bpl.setBonusAmount(rs.getLong("BONUS_AMOUNT"));
//            	bpl.setMinInvestmentAmount(rs.getLong("MIN_INVESTMENT_AMOUNT"));
//            	bpl.setMaxInvestmentAmount(rs.getLong("MAX_INVESTMENT_AMOUNT"));
//                list.add(bpl);
//            }
//        } finally {
//            closeStatement(ps);
//            closeResultSet(rs);
//        }
//        return list;
//    }

    /**
     * Update is_exp_email_sent
     * @param con db connection
     * @param id bonus users id
     * @param is_exp_email_sent true if the user already get notification email
     * @throws SQLException
     */
    public static void updateExpEmailSent(Connection con, long id, long isExpEmailSent) throws SQLException {
        PreparedStatement ps = null;
        try {
            String sql = "UPDATE " +
            				"bonus_users " +
            		     "SET " +
            		     	"is_exp_email_sent = ? " +
                        "WHERE " +
                             "id = ? ";

            ps = con.prepareStatement(sql);

            ps.setLong(1, isExpEmailSent);
            ps.setLong(2, id);
            ps.executeUpdate();

        } finally {
            closeStatement(ps);
        }
    }
	
	public static long getRemarketingAutoBonusId(Connection con, long userId) throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		long bonusId = 0;

		try	{
		    String sql =" SELECT " +
		    			"	bap.bonus_id, " +
		    			"   rl.time_created " +
		    			" FROM " +
		    			"   bonus_auto_parmeters bap, " +
		    			"	remarketing_logins rl, " +
		    			"   users u, " +
		    			"	marketing_combinations mc, " +
		    			"	skins s " +
		    			" WHERE " +
		    			"	rl.user_id = ? " +
		    			"	AND bap.type = ? " +
		    			"	AND  rl.user_id = u.id " +
		    			"	AND rl.combination_id = mc.id " +
		    			"	AND u.skin_id = s.id " +
		    			"   AND bap.combination_id in (0, rl.combination_id) " +
		    			"	AND bap.campaign_id in (0, mc.campaign_id) " +
		    			"	AND bap.skin_id in (0, u.skin_id) " +
		    			"	AND bap.business_skin_id in (0, s.business_case_id) " +
		    			"	AND bap.country_id in (0, u.country_id) " +
		    			"	AND bap.affiliate_key in (0, u.affiliate_key) " +
		    			"	AND bap.is_active = 1 " +
		    			"	AND sysdate between bap.time_start and bap.time_end " +
		    			" ORDER BY " +
		    			"	rl.time_created desc, " +
		    			"	bap.combination_id desc, " +
		    			"	bap.campaign_id desc, " +
		    			"	bap.skin_id desc, " +
		    			"	bap.business_skin_id desc, " +
		    			"	bap.country_id desc, " +
		    			"	bap.affiliate_key DESC ";

			ps = con.prepareStatement(sql);
			ps.setLong(1, userId);
			ps.setLong(2, ConstantsBase.BONUS_AUTO_PARAMETER_TYPE_REMARKETING);
			


			rs = ps.executeQuery();

			if ( rs.next() ) {
				bonusId = rs.getLong("bonus_id");
			}
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		return bonusId;
	}
	
    public static boolean userHaveRemarketingBonus(Connection con, long userId) throws SQLException {
    	boolean haveRemarketingBonus = false;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		
	    try{
		    String sql =
		            " SELECT " +
		            	" * " +
		            " FROM " +
		            	" REMARKETING_BONUS " + 
		            " WHERE " +
		        		"  user_id = ? ";

		    pstmt = con.prepareStatement(sql);
		    pstmt.setLong(1, userId);
		    rs = pstmt.executeQuery();

		    while (rs.next() ) {
		    	haveRemarketingBonus = true;
		    }

		} finally {
		   closeStatement(pstmt);
		   closeResultSet(rs);
		}
		return haveRemarketingBonus;
	}
    
	public static void insertRemarketingBonus(Connection conn, Long userId, Long bonusUserId) throws SQLException{
		PreparedStatement ps = null;
		ResultSet rs = null;
		String sql = " insert " +
						" into REMARKETING_BONUS " +
							" (id, USER_ID, BONUS_USER_ID, time_created) " +
						" values " +
							" (SEQ_REMARKETING_BONUS.nextval,?,?,sysdate)";
		int index = 1;
		try{
			
			ps = conn.prepareStatement(sql);
			ps.setLong(index++, userId);
			ps.setLong(index++, bonusUserId);
			ps.executeUpdate();			

		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
	}
	
//	/**
//	 * Insert Statement to bonus_investments table
//	 * @param conn
//	 * @param investmentId
//	 * @param bonusUsersId
//	 * @param bonusAmount
//	 * @param adjustedAmountHistory
//	 * @param typeId
//	 * @throws SQLException
//	 */
//	public static void insertBonusInvestments(Connection conn, long investmentId, long bonusUsersId, long bonusAmount, long adjustedAmountHistory, int typeId) throws SQLException {
//		PreparedStatement ps = null;
//		int ind = 1;
//		try {
//			String sql =
//						"INSERT INTO bonus_investments " +
//						"	( " +
//						"		id, " +
//						"		investments_id, " +
//						"		bonus_users_id, " +
//						"		bonus_amount, " +
//						"		time_created, " +
//						"		adjusted_amount, " +
//						"		type_id " +
//						" 	) " +
//						"VALUES	" +
//						"	( " +
//						"		SEQ_BONUS_INVESTMENTS.nextval, " +
//						"		?, " +
//						"		?, " +
//						"		?, " +
//						"		sysdate, " +
//						"		?, " +
//						"		? " +
//						"	) ";
//
//			ps = conn.prepareStatement(sql);
//			ps.setLong(ind++, investmentId);
//			ps.setLong(ind++, bonusUsersId);
//			ps.setLong(ind++, bonusAmount);
//			ps.setLong(ind++, adjustedAmountHistory);
//			ps.setLong(ind++, typeId);
//			ps.executeUpdate();
//			log.debug("\nBMS, insertBonusInvestments, investmentId:" + investmentId + ", bonusUsersId:" + bonusUsersId + ", bonusAmount:" + bonusAmount +
//					", adjustedAmount:" + adjustedAmountHistory + ", typeId:" + typeId);
//		} finally {
//			closeStatement(ps);
//		}
//	}
	
//    /**
//     * Update adjusted_amount attribute by the given bonus_users id.
//     * @param conn
//     * @param id - of bonus_users table
//     * @param amount - to be insert to adjusted amount attribute
//     * @throws SQLException
//     */
//    public static void updateAdjustedAmount(Connection conn, long id, long amount) throws SQLException {
//        PreparedStatement ps = null;
//        int ind = 1;
//        try {
//            String sql = 
//            			"UPDATE " +
//            			"	bonus_users " +
//            		    "SET " +
//            		    "	adjusted_amount = ? " +
//                        "WHERE " +
//                        "	id = ?";
//            ps = conn.prepareStatement(sql);
//            ps.setLong(ind++, amount);
//            ps.setLong(ind++, id);
//            ps.executeUpdate();
//            log.debug("\nBMS, updateAdjustedAmount in bonus_users table, adjusted_amount:" + amount + " where id: " + id);
//        } finally {
//            closeStatement(ps);
//        }
//    }
	
	/**
	 * Use Select statement to retrieve all bonus_users data where  the state is using ("used").
	 * @param conn
	 * @param userId
	 * @return
	 * @throws SQLException
	 */
	public static ArrayList<BonusUsers> getBonusUsed(Connection conn, long userId) throws SQLException {
        ArrayList<BonusUsers> list = new ArrayList<BonusUsers>();
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            String sql =
                    	"SELECT " +
                    	" 	bu.* " +
                        //"	t.class_type_id " +
                        "FROM " +
                        " 	bonus_users bu " +
                        "WHERE " +
                        " 	bu.wagering_parameter > 0 and " +
                        " 	bu.user_id = ? and " +
                        "	bu.bonus_state_id = " + ConstantsBase.BONUS_STATE_USED + " " +
                    	"ORDER BY " +
                        "	bu.time_created";
            pstmt = conn.prepareStatement(sql);
            pstmt.setLong(1, userId);
            rs = pstmt.executeQuery();
            while (rs.next()) {
            	BonusUsers bu = getBonusUsersVO(rs, conn);
            	//bu.setBonusClassType(rs.getLong("class_type_id"));
                list.add(bu);
            }
        } finally {
            closeResultSet(rs);
            closeStatement(pstmt);
        }
        return list;
    }

	/**
	 * This method return the sum of adjusted amount by given user id if the bonus is used and has transaction.
	 * Bonus that count as "free" does not have transaction on investment, but if the user is not win on the investment 
	 * there will be a transaction.
	 *  
	 * @param conn
	 * @param userId
	 * @return
	 * @throws SQLException
	 */
	public static long getUserTotalAdjustedAmount(Connection conn, long userId) throws SQLException {
		long total = 0;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            String sql =
            			"SELECT " +
                        "	SUM(adjusted_amount) as total " +
                        "FROM " +
                        "	bonus_users bu " +
                        "WHERE " +
                        "	bu.wagering_parameter > 0 and " +
                        "	bu.user_id = ? and " +
                        "	bu.bonus_state_id in (" + ConstantsBase.BONUS_STATE_USED + "," + ConstantsBase.BONUS_STATE_ACTIVE + ") ";
            pstmt = conn.prepareStatement(sql);
            pstmt.setLong(1, userId);
            rs = pstmt.executeQuery();
            if (rs.next()) {
                total = rs.getLong("total");
            }
        } finally {
            closeResultSet(rs);
            closeStatement(pstmt);
        }
        return total;
	}
	
    
    /**
     * Use Select statement to retrieve total bonus amount in a given investment id.
     * @param conn
     * @param investmentId
     * @return sum of Bonus Amount where investmentId in bonus_investment table.
     * @throws SQLException
     */
    public static long getBonusInvestmentsAmount(Connection conn, long investmentId) throws SQLException {
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        long bonusAmount = -1;
        try {
            String sql =
		            	"SELECT " +
		            	"	SUM(bi.bonus_amount) as totalBonusAmount " +
		            	"FROM " +
		            	"	bonus_investment bi " +
		            	"WHERE " +
	            		"	investments_id = ? ";
            pstmt = conn.prepareStatement(sql);
            pstmt.setLong(1, investmentId);
            rs = pstmt.executeQuery();
            if (rs.next()) {
                bonusAmount = rs.getLong("totalBonusAmount");
            }
        } finally {
            closeResultSet(rs);
            closeStatement(pstmt);
        }
        return bonusAmount;
    }
    
//    /**
//     * Get bonus amount when there was insert investment by given investmentId and type.
//     * 
//    /**
//     * @param conn
//     * @param investmentId
//     * @param isSettled 
//     * @return
//     * @throws SQLException
//     */
//    public static ArrayList<BonusFormulaDetails> getBonusAmounts(Connection conn, long investmentId, boolean isSettled) throws SQLException {
//        PreparedStatement pstmt = null;
//        ResultSet rs = null;
//        ArrayList<BonusFormulaDetails> bonusFormulaD = new ArrayList<BonusFormulaDetails>();
//        try {
//            String sql =
//		            	"SELECT " +
//		            	"	bu.id as bonus_users_id, " +
//		            	"	bi.bonus_amount, " +
//		            	"	bu.adjusted_amount " +
//		            	"FROM " +
//		            	"	bonus_investments bi, " +
//		            	" 	bonus_users bu " +
//		            	"WHERE " +
//	            		"	bu.id = bi.bonus_users_id AND " +
//	            		"	bi.investments_id = ? AND " +
//	            		"	bi.type_id = ? ";
//            pstmt = conn.prepareStatement(sql);
//            pstmt.setLong(1, investmentId);
//            pstmt.setLong(2, isSettled == true ? ConstantsBase.BONUS_INVESTMENTS_TYPE_SETTLED_INV : ConstantsBase.BONUS_INVESTMENTS_TYPE_INSERT_INV);
//            rs = pstmt.executeQuery();
//            while (rs.next()) {
//            	BonusFormulaDetails bfd = new BonusFormulaDetails();
//            	bfd.setAdjustedAmount(rs.getLong("adjusted_amount"));
//            	bfd.setAmount(rs.getLong("bonus_amount"));
//            	bfd.setBonusUsersId(rs.getLong("bonus_users_id"));
//            	bonusFormulaD.add(bfd);
//            }
//        } finally {
//            closeResultSet(rs);
//            closeStatement(pstmt);
//        }
//        return bonusFormulaD;
//    }
	
	public static BonusUsers getLastBonusUserByBonusId(Connection con, long userId, long bonusId) throws SQLException {
    	BonusUsers bu = null;
    	PreparedStatement pstmt = null;
		ResultSet rs = null;
    	try{
		    String sql =
		            "SELECT " +
		            	"bu.* " +
		            "FROM " +
		            	"bonus_users bu " +
		            "WHERE " +
		        		"bu.user_id = ? AND " +
		            	"bu.bonus_id = ? " +
		        		"order by bu.id desc ";

		    pstmt = con.prepareStatement(sql);
		    pstmt.setLong(1, userId);
		    pstmt.setLong(2, bonusId);
		    rs = pstmt.executeQuery();

		    if (rs.next()) {
		    	bu = new BonusUsers();
		    	bu = getBonusUsersVO(rs, con);
		    }
    	} finally {
		   closeStatement(pstmt);
		   closeResultSet(rs);
		}
		return bu;
    }
	
    /**
     * @param con
     * @param id - bonus user id
     * @param isCleanupEmailSent
     * @throws SQLException
     */
    public static void updateCleanupEmailSent(Connection con, long id, boolean isCleanupEmailSent) throws SQLException {
        PreparedStatement ps = null;
        try {
            String sql = 
            			"UPDATE " +
            			" 	bonus_users " +
            		    "SET " +
            		    " 	is_cleanup_email_sent = ? " +
                        "WHERE " +
                        "	id = ? ";

            ps = con.prepareStatement(sql);
            ps.setInt(1, isCleanupEmailSent == true ? 1 : 0);
            ps.setLong(2, id);
            ps.executeUpdate();

        } finally {
            closeStatement(ps);
        }
    }

	/**
	 * BMS
	 * @param con
	 * @param investmentId
	 * @throws SQLException
	 */
	public static void updateAdjustedAmountUponResettle(Connection conn, long investmentId) throws SQLException {
		PreparedStatement pstmt = null;		
		try {
		    String sql =
		    		"UPDATE " +
    				"	bonus_users bu " +
    				"SET " +
    				"	bu.adjusted_amount =  " +
    				"		bu.adjusted_amount - " +
    				"		( " + 
    				"			SELECT " +
    				"				bi.bonus_amount " +
    				"			FROM " +
    				"			 	bonus_investments bi " +
    				"			WHERE " +
    				"			 	bi.investments_id = ? " +
    				" 				AND bu.id = bi.bonus_users_id " +
    				"				AND bi.type_id = " + ConstantsBase.BONUS_INVESTMENTS_TYPE_INSERT_INV +
    				"		) " +    				
					"WHERE " + 
					"	bu.id in " +
					"		( " +
					"			SELECT " + 
					"				bi.bonus_users_id " + 
					"			FROM	" + 
					"				bonus_investments bi " + 
					"			WHERE " +
					"				bi.investments_id = ? " +
					"				AND bi.type_id = 1 " + 
					"		) ";
		    pstmt = conn.prepareStatement(sql);
		    pstmt.setLong(1, investmentId);
		    pstmt.setLong(2, investmentId);
		    if (pstmt.executeUpdate() > 0) {
		    	log.info("\nBMS, update bonus_users Adjusted Amount Upon Resettle; investmentId : " + investmentId); 
		    }
    	} finally {
		   closeStatement(pstmt);
		}
	}
} 