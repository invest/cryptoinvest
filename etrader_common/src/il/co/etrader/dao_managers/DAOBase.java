//package il.co.etrader.dao_managers;
//
//import java.sql.Connection;
//import java.sql.PreparedStatement;
//import java.sql.ResultSet;
//import java.sql.SQLException;
//import java.sql.Statement;
//import java.sql.Timestamp;
//import java.text.ParseException;
//import java.text.SimpleDateFormat;
//import java.util.Date;
//
//import org.apache.log4j.Logger;
//
//public class DAOBase {
//    private static final Logger logger = Logger.getLogger(DAOBase.class);
//
//    public static String ORDER_ASC = "asc";
//    public static String ORDER_DESC = "desc";
//
//    protected static void updateQuery(Connection con, String sql) throws SQLException {
//        Statement st = null;
//        try {
//            st = con.createStatement();
//            st.executeUpdate(sql);
//        } finally {
//            closeStatement(st);
//        }
//    }
//
//    protected static long getSeqCurValue(Connection con, String seq) throws SQLException {
//        PreparedStatement ps = null;
//        ResultSet rs = null;
//        try {
//            ps = con.prepareStatement("select " + seq + ".currval from dual");
//            rs = ps.executeQuery();
//            if (rs.next()) {
//                return rs.getLong(1);
//            }
//        } finally {
//            closeResultSet(rs);
//            closeStatement(ps);
//        }
//        return 0;
//    }
//
//    public static void closeStatement(final Statement stmt) {
//        if (stmt != null) {
//            try {
//                stmt.close();
//            } catch (SQLException ex) {
//                logger.error(stmt, ex);
//            }
//        }
//    }
//
//    public static void closeResultSet(final ResultSet rs) {
//        if (rs != null) {
//            try {
//                rs.close();
//            } catch (SQLException ex) {
//                logger.error(rs, ex);
//            }
//        }
//    }
//
//    public static Date convertToDate(Timestamp t) {
//        if (t == null) {
//            return null;
//        }
//        return new Date(t.getTime());
//    }
//    
//    public static Date getTimeWithTimezone(String time) {
//        Date d = null;
//        if (null != time) {
//            SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS Z");
//            try {
//                d = df.parse(time);
//            } catch (ParseException pe) {
//                logger.error("Can't parse time with timezone.", pe);
//            }
//        }
//        return d;
//    }
//}