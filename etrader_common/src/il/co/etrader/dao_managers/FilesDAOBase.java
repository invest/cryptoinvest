/**
 * 
 */
package il.co.etrader.dao_managers;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.Locale;

import com.anyoption.common.beans.File;

import il.co.etrader.util.CommonUtil;

/**
 * @author kirilim
 * 
 */
public class FilesDAOBase extends com.anyoption.common.daos.FilesDAOBase {
	
	public final static String fileCount = "fileCount";
	public final static String supportApprove = "supportApprove";
	public final static String controlApprove = "controlApprove";
	
	public static void updateBE(Connection con, File vo) throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		int index = 1;

		try {

			String sql = " UPDATE "
							+ " Files "
						+ " SET "
							+ " file_type_id = ?, "
							+ " writer_id = ? , "
							+ " reference = ?, "
							+ " file_name = ?, "
							+ " is_approved = ?, "
							+ " time_updated = sysdate, "
							+ " file_status_id = ?, "
							+ " id_number = ?, "
							+ " first_name = ?, "
							+ " last_name = ?, "
							+ " exp_date = ?, "
							+ " country_id = ?, "
							+ " is_color = ?, "
							+ " is_approved_control = ?, "
							+ " is_control_rejected = ?, "
							+ " is_support_rejected = ?, "
							+ " reject_reason_id = ?, "
							+ " comments = ?, "
							+ " CONTROL_REJECTION_REASON_ID = ?, "
							+ " CONTROL_REJECTION_COMMENTS = ?, "
							+ " rejection_writer_id = ?, "
							+ " uploader_id = ?, "
							+ " TIME_UPLOADED = ?, "
							+ " rejection_date = ?, "							
							+ " SUPPORT_APPROVED_WRITER_ID = ?, "
							+ " TIME_SUPPORT_APPROVED = ?, "
							+ " CONTROL_APPROVED_WRITER_ID = ?, "
							+ " TIME_CONTROL_APPROVED  = ?, "							
							+ " CONTROL_REJECT_WRITER_ID = ?, "
							+ " TIME_CONTROL_REJECTED = ? ";
							
			if (vo.getCcId() != 0) {
				sql += " ,cc_id = ? ";
			}
			sql += " WHERE " + " id = ?";

			ps = con.prepareStatement(sql);

			ps.setLong(index++, Long.valueOf(vo.getFileTypeId()).longValue());
			ps.setLong(index++, vo.getWriterId());
			ps.setString(index++, vo.getReference());
			ps.setString(index++, vo.getFileName());
			ps.setInt(index++, vo.isApproved() ? 1 : 0);
			//ps.setTimestamp(index++,CommonUtil.convertToTimeStamp(vo.getTimeUpdated()));
			ps.setLong(index++, vo.getFileStatusId());
			ps.setString(index++, vo.getNumber());
			ps.setString(index++, CommonUtil.capitalizeFirstLetters(vo.getFirstName()));
			ps.setString(index++, CommonUtil.capitalizeFirstLetters(vo.getLastName()));
			ps.setTimestamp(index++, convertToTimestamp(vo.getExpDate()));
			ps.setLong(index++, vo.getCountryId());
			ps.setInt(index++, vo.isColor() ? 1 : 0);
			ps.setInt(index++, vo.isControlApproved() ? 1 : 0);
			ps.setInt(index++, vo.isControlReject() ? 1 : 0);
			ps.setInt(index++, vo.isSupportReject() ? 1 : 0);
			ps.setInt(index++, vo.getRejectReason());
			ps.setString(index++, vo.getComment());			
			ps.setInt(index++, vo.getControlRejectReason());
			ps.setString(index++, vo.getControlRejectComment());			
			ps.setLong(index++, vo.getRejectionWriterId());
			ps.setLong(index++, vo.getUploaderId());
			ps.setTimestamp(index++, convertToTimestamp(vo.getUploadDate()));
			ps.setTimestamp(index++, convertToTimestamp(vo.getRejectionDate()));
			ps.setLong(index++, vo.getSupportApprovedWriterId());
			ps.setTimestamp(index++, convertToTimestamp(vo.getTimeSupportApproved()));
			ps.setLong(index++, vo.getControlApprovedWriterId());
			ps.setTimestamp(index++, convertToTimestamp(vo.getTimeControlApproved()));			
			ps.setLong(index++, vo.getControlRejectWriterId());
			ps.setTimestamp(index++, convertToTimestamp(vo.getTimeControlRejected()));
			if (vo.getCcId() != 0) {
				ps.setLong(index++, vo.getCcId());
				ps.setLong(index++, vo.getId());
			} else {
				ps.setLong(index++, vo.getId());
			}

			ps.executeUpdate();

		} finally {
			closeStatement(ps);
			closeResultSet(rs);
		}

	}
	
	public static void updateSupportControlStates(Connection con, File vo) throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		int index = 1;

		try {

			String sql = " UPDATE "
							+ " Files "
						+ " SET "
							+ " SUPPORT_APPROVED_WRITER_ID = ? , "
							+ " TIME_SUPPORT_APPROVED = ? , "
							+ " CONTROL_APPROVED_WRITER_ID = ? , "
							+ " TIME_CONTROL_APPROVED = ? "
							+ "  WHERE " 
							+ " id = ?";

			ps = con.prepareStatement(sql);

			ps.setLong(index++, vo.getSupportApprovedWriterId());
			ps.setTimestamp(index++, CommonUtil.convertToTimeStamp(vo.getTimeSupportApproved()));
			ps.setLong(index++, vo.getControlApprovedWriterId());
			ps.setTimestamp(index++, CommonUtil.convertToTimeStamp(vo.getTimeControlApproved()));
			ps.setLong(index++, vo.getId());			
			ps.executeUpdate();
		} finally {
			closeStatement(ps);
			closeResultSet(rs);
		}

	}

	public static List<com.anyoption.common.beans.File> getFilesByActionId(Connection con, long issueId, long userId, Locale locale) throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		List<com.anyoption.common.beans.File> uf = new ArrayList<com.anyoption.common.beans.File>();

		try {

			String sql = "SELECT f.*,ft.name "
					+ "FROM issues i, issue_actions ia,issue_subjects ij, users u, files f, file_types ft "
					+ "WHERE i.id = ia.issue_id "
					+ "AND u.id = i.user_id " 
					+ "AND i.subject_id = ij.id "
					+ "AND f.issue_action_id = ia.id "
					+ "AND ft.id = f.file_type_id " + "AND ia.issue_id = i.id "
					+ "AND ij.id = 56 " 
					+ "AND u.id = ? " 
					+ "AND i.id = ? ";

			ps = con.prepareStatement(sql);
			ps.setLong(1, userId);
			ps.setLong(2, issueId);
			rs = ps.executeQuery();

			while (rs.next()) {
				com.anyoption.common.beans.File f = getVO(rs, locale);
				uf.add(f);
			}

		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}

		return uf;
	}

	
	public static List<com.anyoption.common.beans.File> getFilesByUserId(Connection con, long userId, Locale locale) throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		List<com.anyoption.common.beans.File> uf = new ArrayList<com.anyoption.common.beans.File>();

		try {

			String sql = "SELECT f.*,ft.name "
					+ "FROM issues i, issue_actions ia,issue_subjects ij, users u, files f, file_types ft "
					+ "WHERE i.id = ia.issue_id "
					+ "AND u.id = i.user_id " 
					+ "AND i.subject_id = ij.id "
					+ "AND f.issue_action_id = ia.id "
					+ "AND ft.id = f.file_type_id " 
					+ "AND ia.issue_id = i.id "
					+ "AND u.id = ? ";

			ps = con.prepareStatement(sql);
			ps.setLong(1, userId);
			rs = ps.executeQuery();

			while (rs.next()) {
				com.anyoption.common.beans.File f = getVO(rs, locale);
				uf.add(f);
			}

		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}

		return uf;
	}

//	protected static ArrayList<File> getUserFiles(Connection con, long userId, long fileType, long ccId) throws SQLException {
//		OraclePreparedStatement ps = null;
//		OracleResultSet rs = null;
//		ArrayList<File> list = new ArrayList<File>();
//		// TODO add the cc types front and back also
//		String fileTypes = (fileType != 0) ? String.valueOf(fileType) : FileType.USER_ID_COPY + ", " +
//															FileType.PASSPORT + ", " +
//															FileType.BANKWIRE_CONFIRMATION + ", " +
//															FileType.DRIVER_LICENSE + ", " +
//															FileType.UTILITY_BILL + ", " +
//															FileType.CC_COPY_FRONT + ", " +
//															FileType.CC_COPY_BACK;
//		File vo = null;		
//		 try {
//			  String sql=
//			  " select " +
//			  		" f.*," +
//			  		" t.name," +
//			  		" w.user_name, " +
//			  		" cc.cc_number," +
//			  		" cct.name cc_type_name " +
//			  " from " +
//			  		" File_types t," +
//			  		" writers w, " +
//			  		" Files f " +
//			  			" left join credit_cards cc on cc.id = f.cc_id " +
//			  				" left join credit_card_types cct on cct.id = cc.type_id " +
//			  					" left join issue_actions ia on f.issue_action_id = ia.id " +
//			  " where " +
//			  		" f.file_type_id=t.id " +
//			  		" and f.writer_id=w.id " +
//			  		" and f.user_id = ?" +
//			  		" and f.file_type_id in (" + fileTypes + ")";
////			  		" and (ia.writer_id = 1 or f.issue_action_id = 0)"; // to make sure we look for files from auto issue, not manually created
////			  		" and f.writer_id = 1"; 
//			  if (ccId != 0) {
//				  sql += " and f.cc_id = " + ccId;
//			  }
//			  sql += " order by " +
//			  		" f.id ";
//
//				ps = (OraclePreparedStatement)con.prepareStatement(sql);
//				ps.setLong(1, userId);
//
//				rs=(OracleResultSet)ps.executeQuery();
//
//				while (rs.next()) {
//					vo = getVO(rs);
//					String ccNumber = rs.getString("cc_number");
//
//					if (!CommonUtil.IsParameterEmptyOrNull(ccNumber)){
//						String ccName = CommonUtil.getMessage(rs.getString("cc_type_name"),null);
//						try {
//							ccNumber = Encryptor.decryptStringToString(ccNumber);
//						} catch (CryptoException ce) {
//							throw new SQLException(ce.getMessage());
//						}
//
//						int l = ccNumber.length();
//						if (l>4){
//							vo.setCcName(ccNumber.substring(l - 4, l) + "  " + ccName);
//						}
//					}
//					list.add(vo);
//				}
//			}
//			finally
//			{
//				closeResultSet(rs);
//				closeStatement(ps);
//			}
//		
//		return list;
//	}
	public static Hashtable<String,Integer> getFirstIssueFilesStatusByUserId(Connection con,long userId) throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		Hashtable<String,Integer> table = new Hashtable<String,Integer>();
		
		try {

			String sql =  " SELECT " +  
                          " count(f.id) files_num "+
                          ",sum (decode(nvl(f.is_approved,0),0,0,1)) support_approve "+
                          ",sum (decode(nvl(f.is_approved_control,0),0,0,1)) control_approve "+  
                          " FROM " +
                          " issues i, issue_actions ia, files f "+
                          " WHERE i.id =ia.issue_id "+
                          " AND f.issue_action_id = ia.id "+
                          "and f.file_status_id != 5 "+
                          " AND i.id = (SELECT min(i.id) FROM issues i, issue_actions ia "+
                          " WHERE user_id = ? AND i.id =ia.issue_id AND i.subject_id = 56 AND ia.issue_action_type_id = 44 ) ";

			ps = con.prepareStatement(sql);
			ps.setLong(1, userId);
			rs = ps.executeQuery();

			while (rs.next()) {
				
				table.put(fileCount, rs.getInt("files_num"));
				table.put(supportApprove, rs.getInt("support_approve"));
				table.put(controlApprove, rs.getInt("control_approve"));
			}

		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		return table;

	}
}