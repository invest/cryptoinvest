package il.co.etrader.dao_managers;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.anyoption.common.daos.DAOBase;

/**
 * SkinUrlCountryMap DAO Base class.
 *
 * @author Kobi
 */
public class SkinUrlCountryMapDAOBase extends DAOBase {


	/**
	 * Get skin_id by country_id and url_id
	 * @param con
	 * 		connection to Db
	 * @param countryId
	 * 		country id
	 * @param urlId
	 * 		url id
	 * @return
	 * 		skin id
	 * @throws SQLException
	 */
	public static int getSkinIdByCountryAndUrl(Connection con, long countryId, int urlId) throws SQLException {

		PreparedStatement ps = null;
		ResultSet rs = null;
		int skinId = 0;

		try
		{
		    String sql = "select skin_id from skin_url_country_map "+
		    		     "where country_id = ? and url_id = ? ";

		    ps = con.prepareStatement(sql);
			ps.setLong(1, countryId);
			ps.setInt(2, urlId);

			rs = ps.executeQuery();

			if ( rs.next() ) {
				skinId = rs.getInt("skin_id");
			}

		} finally {
			closeStatement(ps);
			closeResultSet(rs);
		}

		return skinId;
	}

}