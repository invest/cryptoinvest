package il.co.etrader.dao_managers;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.faces.model.SelectItemGroup;

import com.anyoption.common.beans.TreeItem;
import com.anyoption.common.beans.base.Skin;
import com.anyoption.common.daos.DAOBase;
import com.anyoption.common.enums.SkinGroup;
import com.anyoption.common.managers.MarketsManagerBase;
import com.anyoption.common.managers.SkinsManagerBase;

import il.co.etrader.bl_managers.ApplicationDataBase;
import il.co.etrader.bl_vos.Skins;
import il.co.etrader.util.CommonUtil;
import il.co.etrader.util.ConstantsBase;

/**
 * Skins DAO Base class.
 *
 * @author Kobi
 */
public class SkinsDAOBase extends DAOBase {
	/**
	 * Return Skins instance by id
	 *
	 * @param con
	 *            connection to db
	 * @param id
	 *            skin id
	 * @return Skins object
	 * @throws SQLException
	 */
	public static Skins getById(Connection con, int id) throws SQLException {

		PreparedStatement ps = null;
		ResultSet rs = null;
		Skins skin = null;

		try {

			String sql = "select * from skins " + "where id = ? ";

			ps = con.prepareStatement(sql);
			ps.setInt(1, id);

			rs = ps.executeQuery();

			if (rs.next()) {
				skin = getVO(rs);
			}

		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}

		return skin;

	}

	/**
	 * Return all skins
	 *
	 * @param con
	 *            DB connection
	 * @return list of all skins
	 * @throws SQLException
	 */
	public static ArrayList<Skins> getAll(Connection con) throws SQLException {

		Statement st = null;
		ResultSet rs = null;
		ArrayList<Skins> list = new ArrayList<Skins>();

		try {

			String sql = "select * from skins ";

			st = con.createStatement();
			rs = st.executeQuery(sql);

			while (rs.next()) {
				list.add(getVO(rs));
			}

		} finally {
			closeResultSet(rs);
			closeStatement(st);
		}

		return list;
	}

	/**
	 * Return all markets by skin
	 *
	 * @param skinId
	 *            skin id
	 * @param con
	 *            DB connection
	 * @return list of all markets skin
	 * @throws SQLException
	 */
	public static HashMap<Long, String> getSkinsMarkets(Connection con,
			long skinId) throws SQLException {

		PreparedStatement st = null;
		ResultSet rs = null;
		HashMap<Long, String> list = new HashMap<Long, String>();

		try {

			String sql = "select m.id, m.display_name "
					+ "from skin_market_groups sm, skin_market_group_markets smg, markets m "
					+ "where sm.id = smg.skin_market_group_id and sm.skin_id = ? "
					+ "and smg.market_id = m.id ";

			st = con.prepareStatement(sql);
			st.setLong(1, skinId);

			rs = st.executeQuery();

			while (rs.next()) {

				long marketId = rs.getLong("id");
				String marketDisplayName = rs.getString("display_name");
				list.put(marketId, marketDisplayName);
			}

		} finally {
			closeResultSet(rs);
			closeStatement(st);
		}

		return list;
	}
	
	public static ArrayList<Long> getUnvisibleMarketsPerSkin(Connection con, long skinId) throws SQLException {
		PreparedStatement st = null;
		ResultSet rs = null;
		ArrayList<Long> list = new ArrayList<Long>();
		
		String sql = 
					"SELECT " +
							" m.id as market_id, " +
							" m.display_name " +
					"FROM " +
							" markets m " +
					"WHERE " +
							" id " +
					"NOT IN " +
							" (" +
								"SELECT " +
									" m.id " +
								"FROM " +
									" skin_market_groups sm, " +
									" skin_market_group_markets smg, " +
									" markets m " +
								"WHERE " +
									" sm.id = smg.skin_market_group_id " +
								"AND " +
									" sm.skin_id    = ? " +
								"AND " +
									" smg.market_id = m.id " +
							" )";
		try {
			st = con.prepareStatement(sql);
			st.setLong(1, skinId);
			rs = st.executeQuery();

			while (rs.next()) {
				list.add(rs.getLong("market_id"));
			}
		} finally {
			closeResultSet(rs);
			closeStatement(st);
		}

		return list;
	}

	/**
	 * Returns a Skin name by id
	 *
	 * @param con -
	 *            DB connection
	 * @param id -
	 *            Skin id
	 * @return Skin name
	 * @throws SQLException
	 */
	public static String getNameById(Connection con, int id)
			throws SQLException {

		String name = ConstantsBase.EMPTY_STRING;
		Statement ps = null;
		ResultSet rs = null;

		try {
			String sql = "select name from skins where id=" + id;

			ps = con.createStatement();

			rs = ps.executeQuery(sql);

			if (rs.next()) {
				name = rs.getString("name");
			}

		} finally {
			closeStatement(ps);
			closeResultSet(rs);
		}

		return name;
	}

	/**
	 * Fill Skins object
	 *
	 * @param rs
	 *            ResultSet
	 * @return Skins object
	 * @throws SQLException
	 */
	protected static Skins getVO(ResultSet rs) throws SQLException {
		Skins vo = new Skins();
		vo.setId(rs.getInt("id"));
		vo.setName(rs.getString("name"));
		vo.setDisplayName(rs.getString("display_name"));
		vo.setDefaultCountryId(rs.getInt("default_country_id"));
		vo.setDefaultLanguageId(rs.getInt("default_language_id"));
		vo.setDefaultXorId(rs.getInt("default_xor_id"));
		vo.setDefaultCurrencyId(rs.getInt("default_currency_id"));
		vo.setDefaultCombinationId(rs.getLong("default_combination_id"));
		vo.setTierQualificationPeriod(rs.getLong("tier_qualification_period"));
		vo.setBusinessCaseId(rs.getLong("business_case_id"));
        vo.setSupportStartTime(rs.getString("support_start_time"));
        vo.setSupportEndTime(rs.getString("support_end_time"));
        vo.setLoadTickerMarkets(rs.getInt("is_load_ticker") == 1 ? true : false);
        vo.setCountryInLocaleUse(rs.getInt("is_country_in_locale_use") == 1 ? true : false);
        vo.setKeySubdomain(rs.getString("locale_subdomain"));
        vo.setSubdomain(rs.getString("subdomain"));
        vo.setIsRegulated(rs.getInt("is_regulated") == 1 ? true : false);
        vo.setSupportEmail(rs.getString("support_email"));
        vo.setStyle(rs.getString("style"));
        vo.setTemplates(rs.getString("templates"));
        vo.setSkinGroup(SkinGroup.getById(rs.getLong("SKIN_GROUP_ID")));
        vo.setActive(rs.getInt("is_active") == 1 ? true : false);
		return vo;
	}

	public static ArrayList<TreeItem> getTreeItems(Connection con, long skinId, boolean is0100)
			throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		ArrayList<TreeItem> list = new ArrayList<TreeItem>();
		// HashMap<String, HashMap<String, ArrayList<TreeItem>>> marketsList =
		// new HashMap<String, HashMap<String,ArrayList<TreeItem>>>();
		try {

			String sql = "select smgm.market_id , m.DISPLAY_NAME as market_name, smg.MARKET_GROUP_ID , mg.DISPLAY_NAME as group_name,smgm.group_priority ,smg.SKIN_ID,mdg2.sort_priority as sub_group_sort_priority, "
					+ " ( select  mdg.display_name from market_display_groups  mdg where smgm.skin_display_group_id = mdg.id (+)) as subGroup_display_name "
					+ " from SKIN_MARKET_GROUP_MARKETS smgm, markets m, market_groups mg, skin_market_groups smg, market_display_groups mdg2, market_name_skin mns "
					+ " where smgm.market_id = m.id  "
					+ " and smgm.skin_market_group_id = smg.ID "
					+ " and smg.MARKET_GROUP_ID = mg.ID "
					+ " and smg.SKIN_ID = "
					+   skinId 
					+   (is0100 ? "and m.feed_name like '%Binary0-100' " : "" )
					+ " and exists (select id from opportunity_templates where market_id = m.id and is_active = 1 and opportunity_type_id = 1) "
					+ " and mdg2.id(+) = smgm.skin_display_group_id "
                    + " and mns.market_id = m.id "
                    + " and mns.skin_id = smg.skin_id "
					+ " order by smg.MARKET_GROUP_ID, sub_group_sort_priority, upper(mns.name) ";

			ps = con.prepareStatement(sql);

			rs = ps.executeQuery();

			// HashMap<String,ArrayList<TreeItem>> marketGroup = null;
			// ArrayList<TreeItem> geographicGroup= null;
			String group = null;
			String geoGroup = null;
			while (rs.next()) {
				TreeItem temp = new TreeItem();
				temp.setGroupName(rs.getString("group_name"));
				temp.setMarketGroupId(Integer.parseInt(rs
						.getString("market_group_id")));
				temp.setMarketName(MarketsManagerBase.getMarketName(skinId, Long.parseLong(rs.getString("market_id"))));
				temp.setMarketSubGroupDisplayName(rs
						.getString("subgroup_display_name"));
				temp.setMarketId(Integer.parseInt(rs.getString("market_id")));
				if (null == group || !group.equals(temp.getGroupName())) {
					group = temp.getGroupName();
					temp.setPrintGroup(true);
				} else {
					temp.setPrintGroup(false);
				}
				if (temp.getMarketGroupId() != 4
						&& temp.getMarketGroupId() != 5
						&& (null == geoGroup || !geoGroup.equals(temp
								.getMarketSubGroupDisplayName()))) {
					geoGroup = temp.getMarketSubGroupDisplayName();
					temp.setPrintGeoGroup(true);
				} else {
					temp.setPrintGeoGroup(false);
				}
				list.add(temp);
			}
			/*
			 * String currMarketGroup = null;String currGeogGroup = null;
			 *
			 * for(TreeItem tmp:list){ currMarketGroup = tmp.getGroup_name();
			 * currGeogGroup = tmp.getMarket_subGroup_display_name();
			 * if(currGeogGroup==null) // we have or commodities or currency {
			 * marketGroup = marketsList.get(currMarketGroup);
			 * if(marketGroup==null) { marketGroup = new HashMap<String,
			 * ArrayList<TreeItem>>(); geographicGroup = new ArrayList<TreeItem>();
			 * geographicGroup.add(tmp); marketGroup.put(currGeogGroup,
			 * geographicGroup); marketsList.put(currMarketGroup,marketGroup);
			 * }else{ geographicGroup = marketGroup.get(currMarketGroup);
			 * if(geographicGroup==null) { geographicGroup = new ArrayList<TreeItem>();
			 * geographicGroup.add(tmp); marketGroup.put(currGeogGroup,
			 * geographicGroup); marketsList.put(currMarketGroup,marketGroup);
			 * }else{ geographicGroup.add(tmp); marketGroup.put(currGeogGroup,
			 * geographicGroup); marketsList.put(currMarketGroup,marketGroup); }
			 *  } } if(marketsList.get(currMarketGroup)==null){ // add new
			 * markets and geographic group marketGroup = new HashMap<String,
			 * ArrayList<TreeItem>>(); geographicGroup = new ArrayList<TreeItem>();
			 * geographicGroup.add(tmp); marketGroup.put(currGeogGroup,
			 * geographicGroup); marketsList.put(currMarketGroup,marketGroup);
			 * }else{ //there is already marketgroup marketGroup =
			 * marketsList.get(currMarketGroup); geographicGroup =
			 * marketGroup.get(tmp.getMarket_subGroup_display_name());
			 * if(geographicGroup == null){ geographicGroup = new ArrayList<TreeItem>();
			 * geographicGroup.add(tmp);
			 * marketGroup.put(tmp.getMarket_subGroup_display_name(),geographicGroup);
			 * }else{ geographicGroup.add(tmp);
			 * marketGroup.put(tmp.getMarket_subGroup_display_name(),geographicGroup);
			 * marketsList.put(currMarketGroup, marketGroup); } } }
			 */

		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		return list;

	}

	public static ApplicationDataBase getAppData() {
		FacesContext context = FacesContext.getCurrentInstance();
		ApplicationDataBase appData = (ApplicationDataBase) context
				.getApplication().createValueBinding(
						ConstantsBase.BIND_APPLICATION_DATA).getValue(context);
		return appData;
	}

	/**
	 * This method chooses a skin for a new user by using the country of the new
	 * user and ignoring the URL_ID column
	 *
	 * @param countryId -
	 *            the user countryId
	 * @return new User Skin
	 * @throws SQLException
	 */
	public static Skins getNewUserSkin(Connection con, long countryId)
			throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			String sql = "select s.* from skins s, skin_url_country_map su where s.id= su.skin_id and su.country_id=? and rownum=1";
			ps = con.prepareStatement(sql);
			ps.setLong(1, countryId);
			rs = ps.executeQuery();
			if (rs.next()) {
				return getVO(rs);
			}
		}

		finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		Skins def = new Skins();
		def.setId((int) ConstantsBase.CURRENCY_BASE_ID);
		def.setName(ConstantsBase.CURRENCY_BASE);
		return def;
	}

	/**
	 * This method returns the combination id of netrrefer for this skin
	 *
	 * @return Netrefer Combination Id
	 * @throws SQLException
	 */
	public static ArrayList<Long> getNetreferCombinationsId(Connection con, long skinId)
			throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		ArrayList<Long> list = new ArrayList<Long>();
		String skinIds = CommonUtil.getArrayToString(SkinsManagerBase.getRegulatedAndNonSkinIdsBySkinId(skinId));
		try {
			String sql = "SELECT " +
							"* " +
						"FROM " +
							"marketing_combinations " +
						"WHERE " +
							"skin_id in (" + skinIds + ") AND " +
							"campaign_id = ? " +
						"ORDER BY " +
							"id ";
			ps = con.prepareStatement(sql);
			ps.setLong(1, ConstantsBase.NETREFER_CAMPAIGN_ID);
			rs = ps.executeQuery();
			while (rs.next()) {
				list.add(rs.getLong("id"));
			}
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		return list;
	}
	
	/**
	 * This method returns the combination id of netrrefer for this skin
	 *
	 * @return Netrefer Combination Id
	 * @throws SQLException
	 */
	public static ArrayList<Long> getReferpartnerCombinationsId(Connection con, long skinId)
			throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		ArrayList<Long> list = new ArrayList<Long>();
		String skinIds = CommonUtil.getArrayToString(SkinsManagerBase.getRegulatedAndNonSkinIdsBySkinId(skinId));
		try {
			String sql = "SELECT " +
							"* " +
						"FROM " +
							"marketing_combinations " +
						"WHERE " +
							"skin_id in (" + skinIds + ") AND " +
							"campaign_id = ? " +
						"ORDER BY " +
							"id ";
			ps = con.prepareStatement(sql);
			ps.setLong(1, ConstantsBase.REFERPARTNER_CAMPAIGN_ID);
			rs = ps.executeQuery();
			while (rs.next()) {
				list.add(rs.getLong("id"));
			}
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		return list;
	}	

	/**
	 * get all skin business cases
	 *
	 * @param con
	 * @return
	 * @throws SQLException
	 */
	public static ArrayList<SelectItem> getSkinBusinessCases(Connection con)
			throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		ArrayList<SelectItem> list = new ArrayList<SelectItem>();
		try {
			String sql = "SELECT " + "id, " + "description " + "FROM "
					+ "skin_business_cases order by code";
			ps = con.prepareStatement(sql);
			rs = ps.executeQuery();
			while (rs.next()) {
				list.add(new SelectItem(new Long(rs.getLong("id")),rs.getString("description")));
			}
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		return list;
	}
	
	/**
	 * get all skin business cases
	 *
	 * @param connection
	 * @return
	 * @throws SQLException
	 */
	public static Map<Integer, String> getSkinBusinessCasesMap(Connection connection) throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		 Map<Integer, String> list = new LinkedHashMap<Integer, String>();
		try {
			String sql = 
					"SELECT " +
					"	id" +
					"	,description " +
					"FROM " +
					"	skin_business_cases " +
					"ORDER BY " +
					"	code";
			ps = connection.prepareStatement(sql);
			rs = ps.executeQuery();
			while (rs.next()) {
				list.put(rs.getInt("id"), rs.getString("description"));
			}
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		return list;
	}

    /**
     * Load all markets that have active templates for select box options grouped by market groups.
     *
     * @param conn
     * @return
     * @throws SQLException
     */
    public static ArrayList<SelectItem> getMarketsSI(Connection conn, long skinId) throws SQLException {
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        ArrayList<SelectItem> l = new ArrayList<SelectItem>();
        try {
            String sql =
                "SELECT " +
                    "A.id AS market_id, " +
                    "sg.market_group_id, " +
                    "A.display_name, " +
                    "B.display_name AS group_display_name " +
                "FROM " +
                    "markets A, " +
                    "market_groups B, " +
                    "skin_market_group_markets sm, " +
                    "skin_market_groups sg " +
                "WHERE " +
                    "sg.market_group_id = B.id AND " +
                    "EXISTS(SELECT 1 FROM opportunity_templates WHERE market_id = A.id AND is_active = 1 AND scheduled < 5 AND opportunity_type_id = 1) " +
                    "and sm.market_id = A.id " +
                    "and sm.skin_market_group_id = sg.id " +
                    "and sg.skin_id = ? " +
                "ORDER BY " +
                    "sg.market_group_id, " +
                    "A.id";
            pstmt = conn.prepareStatement(sql);
            pstmt.setLong(1, skinId);
            rs = pstmt.executeQuery();
            long prevGroupId = -1;
            SelectItemGroup group = null;
            ArrayList<SelectItem> items = null;

            //  logger.debug("cant load locale!");

            while(rs.next()) {
                long groupId = rs.getLong("market_group_id");
                if (groupId != prevGroupId) {
                    if (null != group && null != items) {
                        SelectItem[] si = new SelectItem[items.size()];
                        for (int i = 0; i < si.length; i++) {
                            si[i] = items.get(i);
                        }
                        group.setSelectItems(si);
                    }
                    String groupName = CommonUtil.getMessage(rs.getString("group_display_name"), null);
                    group = new SelectItemGroup(groupName);
                    l.add(group);
                    prevGroupId = groupId;
                    items = new ArrayList<SelectItem>();
                }
                items.add(new SelectItem(rs.getString("market_id"), MarketsManagerBase.getMarketName(skinId, rs.getLong("market_id"))));
            }
            if (null != group && null != items) {
                SelectItem[] si = new SelectItem[items.size()];
                for (int i = 0; i < si.length; i++) {
                    si[i] = items.get(i);
                }
                group.setSelectItems(si);
            }
        } finally {
            closeResultSet(rs);
            closeStatement(pstmt);
        }
        return l;
    }

    /**
     * This function return binary0100 markets in given skinId
     * @param conn
     * @param skinId
     * @return
     * @throws SQLException
     */
    public static HashMap<Long, String> getMarketsBinary0100(Connection conn, long skinId)
			throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		 HashMap<Long, String> hm = new  HashMap<Long, String>();

		try {
			String sql = 	"SELECT " +
								"m.id as marketId, " +
								"m.display_name " +
		  			   		"FROM " +
		  			   			"markets m, " +
			  			   		"skin_market_groups smg, " +
			  			   		"skin_market_group_markets smgm " +
		  			   		"WHERE " +
		  			   			"smg.skin_id = ? and " +
		  			   			"smgm.market_id = m.id and " +
		  			   			"smg.id = smgm.skin_market_group_id and " +
		  			   			"feed_name like '%Binary0-100' ";

			ps = conn.prepareStatement(sql);
			ps.setLong(1, skinId);
			rs = ps.executeQuery();
			while(rs.next()) {
				hm.put(rs.getLong("marketId"), MarketsManagerBase.getMarketName(skinId, rs.getLong("marketId")));
			  }
		  }finally {
			  closeResultSet(rs);
			  closeStatement(ps);
		  }
		
		return hm;
	}
    
    /**
     * This function return binary0100 markets in given skinId
     * @param conn
     * @param skinId
     * @return
     * @throws SQLException
     */
    public static HashMap<Long, String> getMarketsDynamics(Connection conn, long skinId)
			throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		HashMap<Long, String> hm = new HashMap<Long, String>();

		try {
			String sql = 	"SELECT " +
								"m.id as marketId, " +
								"m.display_name " +
		  			   		"FROM " +
		  			   			"markets m, " +
			  			   		"skin_market_groups smg, " +
			  			   		"skin_market_group_markets smgm " +
		  			   		"WHERE " +
		  			   			"smg.skin_id = ? and " +
		  			   			"smgm.market_id = m.id and " +
		  			   			"smg.id = smgm.skin_market_group_id and " +
		  			   			"feed_name like '%Dynamicssss' ";

			ps = conn.prepareStatement(sql);
			ps.setLong(1, skinId);
			rs = ps.executeQuery();
			while(rs.next()) {
				hm.put(rs.getLong("marketId"), MarketsManagerBase.getMarketName(skinId, rs.getLong("marketId")));
			  }
		  }finally {
			  closeResultSet(rs);
			  closeStatement(ps);
		  }
		
		return hm;
	}

	public static HashMap<String, String> getSubDomainsLocales(Connection con, boolean isSubdomainIsKey, boolean isRegulated) throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		HashMap<String, String> list = new HashMap<String, String>();
		
		try {
			String sql = " SELECT " +
						 "		s.locale_subdomain, " +
						 "		s.subdomain " +
						 " FROM " +
						 "		skins s" +
						 " WHERE" +
						 "		s.is_regulated = " +(isRegulated ? "1" : "0");	

			ps = con.prepareStatement(sql);
			rs = ps.executeQuery();
			while(rs.next()) {
				if(isSubdomainIsKey) {
					list.put(rs.getString("subdomain"),rs.getString("locale_subdomain"));
				} else {
					list.put(rs.getString("locale_subdomain"), rs.getString("subdomain"));
				}				
			  }
		  }finally {
			  closeResultSet(rs);
			  closeStatement(ps);
		  }
		
		return list;
	}

	public static HashSet<String> getRegulatedSkins(Connection con) throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		HashSet<String> list = new HashSet<String>();
		
		try {
			String sql = " SELECT " +
						 "		s.locale_subdomain " +
						 " FROM " +
						 "		skins s" +
						 " WHERE" +
						 "		s.is_regulated = 1 ";	
	
			ps = con.prepareStatement(sql);
			rs = ps.executeQuery();
			while(rs.next()) {
				list.add(rs.getString("locale_subdomain"));
			}
		  }finally {
			  closeResultSet(rs);
			  closeStatement(ps);
		  }
		
		return list;
	}
	
	/**
	 * update control counting per skin.
	 * @param con
	 * @param skinId
	 * @param controlCount
	 * @throws SQLException
	 */
	public static void updateControlCount(Connection con, long skinId, long controlCount) throws SQLException{
	  PreparedStatement ps = null;
	  try {

			String sql=
						" UPDATE " +
							" skins " +
						" SET " +
							" CONTROL_COUNTING = ? " +
						" WHERE " +
							" id = ? ";

				ps = con.prepareStatement(sql);
				ps.setLong(1, controlCount);
				ps.setLong(2, skinId);
				ps.executeUpdate();
		  } finally {
			  closeStatement(ps);
		  }
	  }
	
	/**
	 * @param connection
	 * @return
	 * @throws SQLException
	 */
	public static  Map<Integer, Skin> getSkinsMaping(Connection connection) throws SQLException {
		Map<Integer, Skin> skins = new LinkedHashMap<Integer, Skin>();
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			String sql =
				"SELECT " +
			    "	* " +
				"FROM " +
				"	skins ";
			ps = connection.prepareStatement(sql);
			rs = ps.executeQuery();
			while (rs.next()) {
				skins.put(rs.getInt("id"), getVO(rs));
			}
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		return skins;
	}
} 