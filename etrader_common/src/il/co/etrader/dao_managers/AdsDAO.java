package il.co.etrader.dao_managers;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.anyoption.common.bl_vos.Ad;
import com.anyoption.common.daos.DAOBase;

public class AdsDAO extends DAOBase{
	  public static void insert(Connection con,Ad vo) throws SQLException {
		  PreparedStatement ps = null;
		  try {
				String sql="insert into ads(id,is_default,name) values(seq_ads.NEXTVAL,?,?) ";

				ps = con.prepareStatement(sql);
				ps.setInt(1, vo.getIsDefault());
				ps.setString(2, vo.getName());

				ps.executeUpdate();
				vo.setId(getSeqCurValue(con,"seq_ads"));
		  } finally {
				closeStatement(ps);
			}
	  }

	  public static void update(Connection con,Ad vo) throws SQLException {
		  PreparedStatement ps = null;
		  try {
				String sql="update ads set is_default=?,name=? where id=? ";
				ps = con.prepareStatement(sql);
				ps.setInt(1, vo.getIsDefault());
				ps.setString(2, vo.getName());
				ps.setLong(3, vo.getId());
				ps.executeUpdate();
		  }
			finally
			{
				closeStatement(ps);
			}
	  }

	  public static void clearDefaultAd(Connection con) throws SQLException {

		  String sql="update ads set is_default=0";

		  updateQuery(con,sql);

	  }


		  public static ArrayList getAll(Connection con) throws SQLException {

			  PreparedStatement ps=null;
			  ResultSet rs=null;
			  ArrayList list=new ArrayList();

			  try
				{
				    String sql="select * from ads order by name";

					ps = con.prepareStatement(sql);

					rs=ps.executeQuery();

					while (rs.next()) {
						Ad vo=new Ad();
						vo.setId(rs.getLong("id"));
						vo.setName(rs.getString("name"));
						vo.setIsDefault(rs.getInt("is_default"));
						list.add(vo);
					}
				}

				finally
				{
					closeResultSet(rs);
					closeStatement(ps);
				}
				return list;

		  }

		  public static Ad getById(Connection con,long id) throws SQLException {

			  PreparedStatement ps=null;
			  ResultSet rs=null;

			  Ad vo=null;
			  try
				{
				    String sql="select * from ads where id=?";

					ps = con.prepareStatement(sql);
					ps.setLong(1, id);

					rs=ps.executeQuery();

					if (rs.next()) {
						vo=new Ad();
						vo.setId(rs.getLong("id"));
						vo.setName(rs.getString("name"));
						vo.setIsDefault(rs.getInt("is_default"));

					}
				}

				finally
				{
					closeResultSet(rs);
					closeStatement(ps);
				}
				return vo;

		  }

		  /*
		  public static Ad getDefault(Connection con) throws SQLException {

			  PreparedStatement ps=null;
			  ResultSet rs=null;

			  Ad vo=null;
			  try
				{
				    String sql="select * from ads where is_default=1";

					ps = con.prepareStatement(sql);

					rs=ps.executeQuery();

					if (rs.next()) {
						vo=new Ad();
						vo.setId(rs.getLong("id"));
						vo.setName(rs.getString("name"));
						vo.setIsDefault(rs.getInt("is_default"));

					}
				}

				finally
				{
					closeResultSet(rs);
					closeStatement(ps);
				}
				return vo;

		  }*/

}
