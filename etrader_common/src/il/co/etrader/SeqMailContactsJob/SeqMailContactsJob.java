package il.co.etrader.SeqMailContactsJob;

import il.co.etrader.bl_vos.SequenceContact;
import il.co.etrader.bl_vos.Template;
import il.co.etrader.util.CommonUtil;
import il.co.etrader.util.MailContactsJob;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

public class SeqMailContactsJob extends MailContactsJob{
	private static Logger log = Logger.getLogger(SeqMailContactsJob.class);

	public static void main(String[] args) throws Exception {
		propFile = args[0];
		long diffSeconds = 0;
		long SeqJobStart = System.currentTimeMillis();
		ArrayList<SequenceContact> emailContacts = new ArrayList<SequenceContact>();
		log.info("Starting the Seq Mail Contacts Job.");
		long getContactsStart = System.currentTimeMillis();
		emailContacts = getContacts();
		long getContactsEnd = System.currentTimeMillis();
		diffSeconds = (getContactsEnd - getContactsStart) / (1000);
		log.info("Getting contacts took: " + diffSeconds  + " seconds");
		Calendar cal = null;
		long counter = 0;
		long sendEmailStart = System.currentTimeMillis();
		for (SequenceContact c : emailContacts) {
			cal = Calendar.getInstance();
			Date lastAction = c.getActionTime();
			cal.add(Calendar.DATE, -(int)c.getNumberOfDaysAfterLastEmail());
			if (cal.getTime().after(lastAction)) {
				long nextTemplateId = c.getNextTemplateId();
				Template t = getTemplate(nextTemplateId);
			    Hashtable<String,String> server = initServerParameters();
			    HashMap<String, String> params = initEmailParameters(c, t);
			    boolean isEmailSent = false;
			    try {
			    	isEmailSent = send(server, t.getFileName(), params, t.getSubject(), null, 0, c, null, t.getId());
			    } catch (Exception e) {
			    	log.error("Email was not sent ");
			    }

			    if (isEmailSent) {
			    	log.info("Sending " + t.getId() + " template to " + c.toString() + ".");
				    createAndInsertIssueAndIssueAction(c, t, args);
				    counter++;
			    }
			}
		}
		long sendEmailEnd = System.currentTimeMillis();
		diffSeconds = (sendEmailEnd - sendEmailStart) / (1000);
		log.info("Sending emails took: " + diffSeconds + " seconds");
		long SeqJobEnd = System.currentTimeMillis();
		diffSeconds = (SeqJobEnd - SeqJobStart) / (1000);
		log.info("Done with Seq Mail Contacts Job, this took: " + diffSeconds + " seconds");
		log.info(counter + " Emails sent");
	}

	protected static ArrayList<SequenceContact> getContacts() throws Exception {
		Connection con = getConnection();
		PreparedStatement ps = null;
		ResultSet rs = null;
		ArrayList<SequenceContact> emailContacts = new ArrayList<SequenceContact>();
		String contactsId = properties.getProperty("contact.id");

		try {
			String sql = " SELECT " +
            			 " 		ecs.next_template_id, " +
            			 " 		contact.contact_id, " +
            			 "		ecs.num_days_after, " +
            			 " 		contact.email, " +
            			 " 		contact.user_id, " +
            			 " 		contact.first_name, " +
            			 " 		contact.last_name, " +
            			 " 		contact.skin_id, " +
            			 " 		contact.country_id, " +
            			 " 		contact.utc_offset, " +
            			 "		contact.action_time, " +
            			 "		contact.type " +
            			 " FROM  " +
            			 "		email_contact_sequence ecs," +
            			 "	    ( " +
            			 "		SELECT " +
            			 "			 max(ia.template_id) as max_template, " +
			             "			 max(ia.action_time) as action_time, " +
            			 "			 c.id as contact_id, " +
            			 "			 c.email as email, " +
            			 "			 c.user_id as user_id, " +
            			 "			 c.first_name as first_name, " +
            			 "			 c.last_name as last_name, " +
            			 "			 c.skin_id as skin_id, " +
            			 "			 c.country_id as country_id, " +
            			 "			 c.utc_offset as utc_offset," +
            			 "			 c.type as type " +
            			 "		FROM " +
            			 "			 issue_actions ia , " +
            			 "			 templates t , " +
            			 "			 issues i  , " +
            			 "			 contacts c " +
            			 "		WHERE " +
            			 "			 ia.issue_id = i.id " +
            			 "			 AND ia.template_id is not null " +
            			 "			 AND i.contact_id = c.id " +
            			 "			 AND ia.template_id = t.id " +
            			 "			 AND t.type_id = ? " +
            			 "			 AND c.class_id <> 0 " +
            			 "			 AND c.is_contact_by_email = 1 " +
            			 "			 AND  c.user_id = 0 " +
            			 "			 GROUP BY c.id," +
            			 "				      c.email, " +
            			 "					  c.user_id , " +
            			 "					  c.first_name, " +
            			 "					  c.last_name, " +
            			 "					  c.skin_id, " +
            			 "					  c.country_id, " +
            			 "					  c.utc_offset," +
            			 "					  c.type " +
            			 "		) contact " +
            			 " WHERE " +
            			 "		ecs.template_id = contact.max_template " +
            			 "		AND  ecs.next_template_id is not null " +
            			 "      AND ecs.contact_type = contact.type ";
				if (!CommonUtil.isParameterEmptyOrNull(contactsId)) {
						sql += " AND contact.contact_id in (" + contactsId + ") ";
				 }

			ps = con.prepareStatement(sql);
			ps.setLong(1, TEMPLATES_TYPE_SEQUENCE);
			rs = ps.executeQuery();

			while (rs.next()) {
				SequenceContact vo = new SequenceContact();
				vo.setId(rs.getLong("contact_id"));
				vo.setEmail(rs.getString("email"));
				vo.setUserId(rs.getLong("user_id"));
				vo.setFirstName(rs.getString("first_name"));
				vo.setLastName(rs.getString("last_name"));
				vo.setSkinId(rs.getLong("skin_id"));
				vo.setCountryId(rs.getLong("country_id"));
				vo.setUtcOffset(rs.getString("utc_offset"));
				vo.setNextTemplateId(rs.getLong("next_template_id"));
				vo.setActionTime(rs.getDate("action_time"));
				vo.setNumberOfDaysAfterLastEmail(rs.getLong("num_days_after"));
				vo.setType(rs.getLong("type"));
				emailContacts.add(vo);
			}

		} catch (Exception e) {
			log.log(Level.FATAL,"Can't get contacts ",e);
		} finally {
			try {
				rs.close();
			} catch (Exception e) {
				log.log(Level.ERROR,"Can't close",e);
			}
			try {
				ps.close();
			} catch (Exception e) {
				log.log(Level.ERROR,"Can't close",e);
			}
			try {
				con.close();
			} catch (Exception e) {
				log.log(Level.ERROR,"Can't close",e);
			}
		}

		return emailContacts;
	}
}
