package il.co.etrader.bl_vos;

public class TransactionStatus implements java.io.Serializable{
	private long id;
	private String code;
	private String description;

	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	/**
	 * Constructs a <code>String</code> with all attributes
	 * in name = value format.
	 *
	 * @return a <code>String</code> representation
	 * of this object.
	 */
	public String toString()
	{
	    final String TAB = " \n ";

	    String retValue = "";

	    retValue = "TransactionStatus ( "
	        + super.toString() + TAB
	        + "id = " + this.id + TAB
	        + "code = " + this.code + TAB
	        + "description = " + this.description + TAB
	        + " )";

	    return retValue;
	}



}
