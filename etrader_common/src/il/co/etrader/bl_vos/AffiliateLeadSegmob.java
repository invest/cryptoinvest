package il.co.etrader.bl_vos;

import java.io.Serializable;

public class AffiliateLeadSegmob implements Serializable {
	private static final long serialVersionUID = 8248698904736320826L;
	
	private String trackID;
	private String updated;
	private String first_name;
	private String last_name;
	private String country;
	private String real_phone_number;
	private String email;
	private String story_short_url;
	
	/**
	 * @return the trackID
	 */
	public String getTrackID() {
		return trackID;
	}
	/**
	 * @param trackID the trackID to set
	 */
	public void setTrackID(String trackID) {
		this.trackID = trackID;
	}
	/**
	 * @return the updated
	 */
	public String getUpdated() {
		return updated;
	}
	/**
	 * @param updated the updated to set
	 */
	public void setUpdated(String updated) {
		this.updated = updated;
	}
	/**
	 * @return the first_name
	 */
	public String getFirst_name() {
		return first_name;
	}
	/**
	 * @param first_name the first_name to set
	 */
	public void setFirst_name(String first_name) {
		this.first_name = first_name;
	}
	/**
	 * @return the last_name
	 */
	public String getLast_name() {
		return last_name;
	}
	/**
	 * @param last_name the last_name to set
	 */
	public void setLast_name(String last_name) {
		this.last_name = last_name;
	}
	/**
	 * @return the country
	 */
	public String getCountry() {
		return country;
	}
	/**
	 * @param country the country to set
	 */
	public void setCountry(String country) {
		this.country = country;
	}
	/**
	 * @return the real_phone_number
	 */
	public String getReal_phone_number() {
		return real_phone_number;
	}
	/**
	 * @param real_phone_number the real_phone_number to set
	 */
	public void setReal_phone_number(String real_phone_number) {
		this.real_phone_number = real_phone_number;
	}
	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}
	/**
	 * @param email the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}
	/**
	 * @return the story_short_url
	 */
	public String getStory_short_url() {
		return story_short_url;
	}
	/**
	 * @param story_short_url the story_short_url to set
	 */
	public void setStory_short_url(String story_short_url) {
		this.story_short_url = story_short_url;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "AffiliateLeadSegmob [trackID=" + trackID + ", updated="
				+ updated + ", first_name=" + first_name + ", last_name="
				+ last_name + ", country=" + country + ", real_phone_number="
				+ real_phone_number + ", email=" + email + ", story_short_url="
				+ story_short_url + "]";
	}
	
}
