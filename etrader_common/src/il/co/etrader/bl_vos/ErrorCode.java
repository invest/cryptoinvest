package il.co.etrader.bl_vos;

import java.io.Serializable;

public class ErrorCode implements Serializable{

	/**
	 * @author oshikl
	 */
	private static final long serialVersionUID = 4278180111927374399L;
	private long id;
	private String result;
	private long clearingProviderId;
	private long clearingProviderIdGroup;
	private boolean isSpecialCode;
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public boolean isSpecialCode() {
		return isSpecialCode;
	}
	public void setSpecialCode(boolean isSpecialCode) {
		this.isSpecialCode = isSpecialCode;
	}
	public String getResult() {
		return result;
	}
	public void setResult(String result) {
		this.result = result;
	}
	public long getClearingProviderId() {
		return clearingProviderId;
	}
	public void setClearingProviderId(long clearingProviderId) {
		this.clearingProviderId = clearingProviderId;
	}
	public long getClearingProviderIdGroup() {
		return clearingProviderIdGroup;
	}
	public void setClearingProviderIdGroup(long clearingProviderIdGroup) {
		this.clearingProviderIdGroup = clearingProviderIdGroup;
	}

}
