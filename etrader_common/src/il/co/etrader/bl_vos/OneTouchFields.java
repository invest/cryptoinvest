//package il.co.etrader.bl_vos;
//
//// class used to contain all the fields special for One Touch
//
//public class OneTouchFields implements java.io.Serializable {
//	private long id;
//	private int upDown;
//	private int decimalPoint;
//
//	public long getId() {
//		return id;
//	}
//	public int getDecimalPoint() {
//		return decimalPoint;
//	}
//	public void setDecimalPoint(int decimalPoint) {
//		this.decimalPoint = decimalPoint;
//	}
//	public int getUp_down() {
//		return upDown;
//	}
//	public void setUp_down(int up_down) {
//		this.upDown = up_down;
//	}
//	public void setId(long id) {
//		this.id = id;
//	}
//	/**
//	 * Constructs a <code>String</code> with all attributes
//	 * in name = value format.
//	 *
//	 * @return a <code>String</code> representation
//	 * of this object.
//	 */
//	public String toString()
//	{
//	    final String TAB = " \n ";
//
//	    String retValue = "";
//
//	    retValue = "OneTouchFields ( "
//	        + super.toString() + TAB
//	        + "id = " + this.id + TAB
//	        + "up_down = " + this.upDown + TAB
//	        + "decimalPoint = " + this.decimalPoint + TAB
//	        + " )";
//
//	    return retValue;
//	}
//}
