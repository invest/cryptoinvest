package il.co.etrader.bl_vos;

import il.co.etrader.util.CommonUtil;
import il.co.etrader.util.ConstantsBase;

import java.io.Serializable;
import java.sql.SQLException;
import java.text.DecimalFormat;

public class InvestmentsGroup implements Serializable {

	private static final long serialVersionUID = -764069242581951757L;

	private long userId;
	private String userName;
	private long usersBalance;
	private long usersBalanceBase;
	private Long usersCurrencyId;
	private String country;
	private String countryIds;
	private long countryId;
	private String skin;
	private String asset;
	private long totalInv;
	private long totalReturn;
	private long hits;
	private long count;
	private int sumIdx;
	private String apiExternalUserReference;
	private long apiExternalUserId;
	private long skinId;

	public static final int SUM_IDX_VAL = 0;
	public static final int SUM_IDX_LINE = 1;
	public static final int SUM_IDX_SUM = 2;
	public static final int SUM_IDX_TOTAL = 3;

	public InvestmentsGroup() {
		this.sumIdx = SUM_IDX_VAL;
	}

	public InvestmentsGroup(int sumIdx) {
		this.sumIdx = sumIdx;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}

	public long getUserId() {
		return userId;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getUserName() {
		return userName;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getCountry() {
		return country;
	}

	public void setSkin(String skin) {
		this.skin = skin;
	}

	public String getSkin() {
		return skin;
	}

	public void setAsset(String asset) {
		this.asset = asset;
	}

	public String getAsset() {
		return asset;
	}

	public void setTotalInv(long totalInv) {
		this.totalInv = totalInv;
	}

	public long getTotalInv() {
		return totalInv;
	}

	public String getTotalInvTxt() {
		return CommonUtil.displayAmount(totalInv, ConstantsBase.CURRENCY_BASE_ID);
	}

	public void setTotalReturn(long totalReturn) {
		this.totalReturn = totalReturn;
	}

	public long getTotalReturn() {
		return totalReturn;
	}

	public String getTotalReturnTxt() {
		return CommonUtil.displayAmount(totalReturn, ConstantsBase.CURRENCY_BASE_ID);
	}

	public double getReturnRatio() {
		return (double)totalReturn/(double)totalInv;
	}

	public String getReturnPrecentTxt() {
		DecimalFormat sd = new DecimalFormat("#0.00%");
		return sd.format(getReturnRatio());
	}

	public long getTotalProfit() {
		return totalInv - totalReturn;
	}

	public String getTotalProfitTxt() {
		return CommonUtil.displayAmount(getTotalProfit(), ConstantsBase.CURRENCY_BASE_ID);
	}


	public void setHits(long totalHits) {
		this.hits = totalHits;
	}

	public long getHits() {
		return hits;
	}

	public double getHitsRatio() {
		return (double)hits/(double)count;
	}

	public String getHitsPrecentTxt() {
		DecimalFormat sd = new DecimalFormat("#0.00%");
		return sd.format(getHitsRatio());
	}

	public void setCount(long investmentsNumber) {
		this.count = investmentsNumber;
	}

	public long getCount() {
		return count;
	}

	public boolean getIsVal() {
		return SUM_IDX_VAL == sumIdx;
	}

	public boolean getIsLine() {
		return SUM_IDX_LINE == sumIdx;
	}

	public boolean getIsSum() {
		return SUM_IDX_SUM == sumIdx;
	}

	public boolean getIsTotal() {
		return SUM_IDX_TOTAL == sumIdx;
	}

	public String getUsersBalanceTxt() throws SQLException {
		if (usersBalance > 0 && usersCurrencyId > 0){
			return CommonUtil.displayAmount(usersBalance, ConstantsBase.CURRENCY_USD_ID);
		}else{
			return ConstantsBase.EMPTY_STRING;
		}
	}

	public String getUsersBalanceBaseTxt() throws SQLException {
		if (usersBalanceBase > 0 && usersCurrencyId > 0) {
			return CommonUtil.displayAmount(usersBalanceBase, ConstantsBase.CURRENCY_USD_ID);
		} else {
			return ConstantsBase.EMPTY_STRING;
		}
	}

	/**
	 * @return the usersBalance
	 */
	public long getUsersBalance() {
		return usersBalance;
	}

	/**
	 * @param usersBalance the usersBalance to set
	 */
	public void setUsersBalance(long usersBalance) {
		this.usersBalance = usersBalance;
	}

	/**
	 * @return the usersCurrencyId
	 */
	public Long getUsersCurrencyId() {
		return usersCurrencyId;
	}

	/**
	 * @param usersCurrencyId the usersCurrencyId to set
	 */
	public void setUsersCurrencyId(Long usersCurrencyId) {
		this.usersCurrencyId = usersCurrencyId;
	}

	public long getUsersBalanceBase() {
		return usersBalanceBase;
	}

	public void setUsersBalanceBase(long usersBalanceBase) {
		this.usersBalanceBase = usersBalanceBase;
	}

	public long getCountryId() {
		return countryId;
	}

	public void setCountryId(long countryId) {
		this.countryId = countryId;
	}

	public String getCountryIds() {
		return countryIds;
	}

	public void setCountryIds(String countryIds) {
		this.countryIds = countryIds;
	}

	/**
	 * @return the apiExternalUserReference
	 */
	public String getApiExternalUserReference() {
		return apiExternalUserReference;
	}

	/**
	 * @param apiExternalUserReference the apiExternalUserReference to set
	 */
	public void setApiExternalUserReference(String apiExternalUserReference) {
		this.apiExternalUserReference = apiExternalUserReference;
	}

	/**
	 * @return the apiExternalUserId
	 */
	public long getApiExternalUserId() {
		return apiExternalUserId;
	}

	/**
	 * @param apiExternalUserId the apiExternalUserId to set
	 */
	public void setApiExternalUserId(long apiExternalUserId) {
		this.apiExternalUserId = apiExternalUserId;
	}

	/**
	 * @return the skinId
	 */
	public long getSkinId() {
		return skinId;
	}

	/**
	 * @param skinId the skinId to set
	 */
	public void setSkinId(long skinId) {
		this.skinId = skinId;
	}

}
