//package il.co.etrader.bl_vos;
//
//public class ACHDepositBase implements java.io.Serializable {
//
//	private static final long serialVersionUID = 4298858357742478550L;
//
//	protected long id;
//	protected long transactionId;
//	protected String payeeEmail;
//	protected String payeeAddress;
//	protected String payeeCity;
//	protected String payeeState;
//	protected String payeeZip;
//	protected String payeePhone;
//
//	protected String routingNo;
//	protected String accountNo;
//
//
//	public ACHDepositBase() {
//		id = 0;
//		transactionId = 0;
//		payeeEmail = "";
//		payeeAddress = "";
//		payeeCity = "";
//		payeeState = "";
//		payeeZip = "";
//		payeePhone = "";
//
//		routingNo = "";
//		accountNo = "";
//	}
//
//	/**
//	 * Constructs a <code>String</code> with all attributes
//	 * in name = value format.
//	 *
//	 * @return a <code>String</code> representation
//	 * of this object.
//	 */
//	public String toString()
//	{
//	    final String ls = " \n ";
//
//	    String retValue = "";
//
//	    retValue = "ACHDepositBase ( "
//	        + super.toString() + ls
//	        + "id = " + this.id + ls
//	        + "transactionId = " + this.transactionId + ls
//	        + "payeeEmail = " + this.payeeEmail + ls
//	        + "payeeAddress = " + this.payeeAddress + ls
//	        + "payeeCity = " + this.payeeCity + ls
//	        + "payeeState = " + this.payeeState + ls
//	        + "payeeZip = " + this.payeeZip + ls
//	        + "payeePhone = " + this.payeePhone + ls
//	        + "routingNo = " + this.routingNo + ls
//	        + "accountNo = " + this.accountNo + ls
//	        + " )";
//
//	    return retValue;
//	}
//
//	public String getAccountNo() {
//		return accountNo;
//	}
//
//	public void setAccountNo(String accountNo) {
//		this.accountNo = accountNo;
//	}
//
//	public long getId() {
//		return id;
//	}
//
//	public void setId(long id) {
//		this.id = id;
//	}
//
//	public String getPayeeAddress() {
//		return payeeAddress;
//	}
//
//	public void setPayeeAddress(String payeeAddress) {
//		this.payeeAddress = payeeAddress;
//	}
//
//	public String getPayeeCity() {
//		return payeeCity;
//	}
//
//	public void setPayeeCity(String payeeCity) {
//		this.payeeCity = payeeCity;
//	}
//
//	public String getPayeeEmail() {
//		return payeeEmail;
//	}
//
//	public void setPayeeEmail(String payeeEmail) {
//		this.payeeEmail = payeeEmail;
//	}
//
//	public String getPayeePhone() {
//		return payeePhone;
//	}
//
//	public void setPayeePhone(String payeePhone) {
//		this.payeePhone = payeePhone;
//	}
//
//	public String getPayeeState() {
//		return payeeState;
//	}
//
//	public void setPayeeState(String payeeState) {
//		this.payeeState = payeeState;
//	}
//
//	public String getPayeeZip() {
//		return payeeZip;
//	}
//
//	public void setPayeeZip(String payeeZip) {
//		this.payeeZip = payeeZip;
//	}
//
//	public String getRoutingNo() {
//		return routingNo;
//	}
//
//	public void setRoutingNo(String routingNo) {
//		this.routingNo = routingNo;
//	}
//
//	public long getTransactionId() {
//		return transactionId;
//	}
//
//	public void setTransactionId(long transactionId) {
//		this.transactionId = transactionId;
//	}
//}
