//package il.co.etrader.bl_vos;
//
//import org.apache.log4j.Level;
//
///**
// * SkinLanguage vo class
// * @author Kobi
// *
// */
//public class SkinLanguages implements java.io.Serializable {
//
//	private long id;
//	private long skinId;
//	private long languageId;
//	private String displayName;
//
//	public SkinLanguages() {
//		displayName = "";
//	}
//
//
//
//	/**
//	 * @return the languageId
//	 */
//	public long getLanguageId() {
//		return languageId;
//	}
//
//	/**
//	 * @param languageId the languageId to set
//	 */
//	public void setLanguageId(long languageId) {
//		this.languageId = languageId;
//	}
//
//	/**
//	 * @return the displayName
//	 */
//	public String getDisplayName() {
//		return displayName;
//	}
//
//	/**
//	 * @param displayName the displayName to set
//	 */
//	public void setDisplayName(String displayName) {
//		this.displayName = displayName;
//	}
//
//	/**
//	 * @return the id
//	 */
//	public long getId() {
//		return id;
//	}
//
//	/**
//	 * @param id the id to set
//	 */
//	public void setId(long id) {
//		this.id = id;
//	}
//
//	/**
//	 * @return the skinId
//	 */
//	public long getSkinId() {
//		return skinId;
//	}
//
//	/**
//	 * @param skinId the skinId to set
//	 */
//	public void setSkinId(long skinId) {
//		this.skinId = skinId;
//	}
//
//
//
//	/**
//	 * Constructs a <code>String</code> with all attributes
//	 * in name = value format.
//	 *
//	 * @return a <code>String</code> representation
//	 * of this object.
//	 */
//	public String toString()
//	{
//	    final String TAB = " \n ";
//
//	    String retValue = "";
//
//	    retValue = "SkinLanguage ( "
//	        + super.toString() + TAB
//	        + "skinId = " + this.skinId + TAB
//	        + "languageId = " + this.languageId + TAB
//	        + "displayName = " + this.displayName + TAB
//	        + " )";
//
//	    return retValue;
//	}
//
//
//
//
//}
