package il.co.etrader.bl_vos;

import il.co.etrader.util.CommonUtil;

import java.io.Serializable;
import java.sql.SQLException;
import java.util.Date;

/**
 * Marketing payment recipients class
 *
 * @author Kobi.
 */
public class MarketingPaymentRecipient implements Serializable {
    private static final long serialVersionUID = 1L;

    public static final long GOOGLE = 25;
    public static final long YAHOO = 46;
    public static final long MSN = 119;
    public static final long YAHOO_PPC = 133;
    public static final long FACEBOOK = 27;

    protected long id;
    protected String agentName;
    protected long writerId;
    protected Date timeCreated;


	/**
	 * @return the id
	 */
	public long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(long id) {
		this.id = id;
	}

	/**
	 * @return the name
	 */
	public String getAgentName() {
		return agentName;
	}

	/**
	 * @param name the name to set
	 */
	public void setAgentName(String agentName) {
		this.agentName = agentName;
	}

	/**
	 * @return the timeCreated
	 */
	public Date getTimeCreated() {
		return timeCreated;
	}

	/**
	 * @param timeCreated the timeCreated to set
	 */
	public void setTimeCreated(Date timeCreated) {
		this.timeCreated = timeCreated;
	}

	/**
	 * @return the writerId
	 */
	public long getWriterId() {
		return writerId;
	}

	/**
	 * @param writerId the writerId to set
	 */
	public void setWriterId(long writerId) {
		this.writerId = writerId;
	}

	/**
	 * @return the timeCreatedTxt
	 */
	public String getTimeCreatedTxt() {
		return CommonUtil.getDateFormat(timeCreated, CommonUtil.getUtcOffset());
	}


	/**
	 * Get writer name
	 * @return
	 * @throws SQLException
	 */
	public String getWriterTxt() throws SQLException {
		return CommonUtil.getWriterName(writerId);
	}


	/**
	 * toString implementation.
	 */
	public String toString() {
        String ls = System.getProperty("line.separator");
        return ls + "MarketingCampaigns:" + ls +
            "id: " + id + ls +
            "agentName: " + agentName + ls +
	        "writerId: " + writerId + ls +
	        "timeCreated: " + timeCreated + ls;
    }

}