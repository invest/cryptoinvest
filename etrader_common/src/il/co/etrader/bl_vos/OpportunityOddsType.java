//package il.co.etrader.bl_vos;
//
//import java.util.Date;
//
//public class OpportunityOddsType implements java.io.Serializable{
//
//	private long id;
//	private String name;
//	private float overOddsWin;
//	private float overOddsLose;
//	private float underOddsWin;
//	private float underOddsLose;
//	private int isDefault;
//	private Date timeCreated;
//    private float oddsWinDeductStep;
//
//	public long getId() {
//		return id;
//	}
//	public void setId(long id) {
//		this.id = id;
//	}
//	public int getIsDefault() {
//		return isDefault;
//	}
//	public void setIsDefault(int isDefault) {
//		this.isDefault = isDefault;
//	}
//	public String getName() {
//		return name;
//	}
//	public void setName(String name) {
//		this.name = name;
//	}
//	public float getOverOddsLose() {
//		return overOddsLose;
//	}
//	public void setOverOddsLose(float overOddsLose) {
//		this.overOddsLose = overOddsLose;
//	}
//	public float getOverOddsWin() {
//		return overOddsWin;
//	}
//	public void setOverOddsWin(float overOddsWin) {
//		this.overOddsWin = overOddsWin;
//	}
//	public Date getTimeCreated() {
//		return timeCreated;
//	}
//	public void setTimeCreated(Date timeCreated) {
//		this.timeCreated = timeCreated;
//	}
//	public float getUnderOddsLose() {
//		return underOddsLose;
//	}
//	public void setUnderOddsLose(float underOddsLose) {
//		this.underOddsLose = underOddsLose;
//	}
//	public float getUnderOddsWin() {
//		return underOddsWin;
//	}
//	public void setUnderOddsWin(float underOddsWin) {
//		this.underOddsWin = underOddsWin;
//	}
//	public boolean isDefaultBoolean() {
//		return isDefault==1;
//	}
//	public void setDefaultBoolean(boolean i) {
//		if (i)
//			isDefault=1;
//		else
//			isDefault=0;
//	}
////	public String getTimeCreatedTxt() {
////		return CommonUtil.getDateTimeFormatDisplay(timeCreated, CommonUtil.getUtcOffset(ConstantsBase.EMPTY_STRING));
////	}
//
//	public float getOddsWinDeductStep() {
//        return oddsWinDeductStep;
//    }
////	public String getOddsWinDeductStepTxt() {
////        return CommonUtil.displayDecimalForInput(oddsWinDeductStep);
////    }
//
//    public void setOddsWinDeductStep(float oddsWinDeductStep) {
//        this.oddsWinDeductStep = oddsWinDeductStep;
//    }
//
//    /**
//	 * Constructs a <code>String</code> with all attributes
//	 * in name = value format.
//	 *
//	 * @return a <code>String</code> representation
//	 * of this object.
//	 */
//	public String toString()
//	{
//	    final String TAB = " \n ";
//
//	    String retValue = "";
//
//	    retValue = "OpportunityOddsType ( "
//	        + super.toString() + TAB
//	        + "id = " + this.id + TAB
//	        + "name = " + this.name + TAB
//	        + "overOddsWin = " + this.overOddsWin + TAB
//	        + "overOddsLose = " + this.overOddsLose + TAB
//	        + "underOddsWin = " + this.underOddsWin + TAB
//	        + "underOddsLose = " + this.underOddsLose + TAB
//	        + "isDefault = " + this.isDefault + TAB
//	        + "timeCreated = " + this.timeCreated + TAB
//            + "oddsWinDeductStep = " + this.oddsWinDeductStep + TAB
//	        + " )";
//
//	    return retValue;
//	}
//}