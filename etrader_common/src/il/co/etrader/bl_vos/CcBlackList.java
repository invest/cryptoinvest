package il.co.etrader.bl_vos;

import il.co.etrader.bl_managers.ApplicationDataBase;
import il.co.etrader.util.CommonUtil;
import il.co.etrader.util.ConstantsBase;

import java.sql.SQLException;
import java.util.Date;

public class CcBlackList implements java.io.Serializable{
	private long id;
	private long writerId;
	private Date timeCreated;
	private String comments;
	private String ccNum;
	private int isActive;

	public CcBlackList() {
		id=0;
	}
	public String getTimeCreatedTxt() {
		return CommonUtil.getDateTimeFormatDisplay(timeCreated, CommonUtil.getUtcOffset(ConstantsBase.EMPTY_STRING));
	}
	public String getCcNum() {
		return ccNum;
	}
	public void setCcNum(String ccNum) {
		this.ccNum = ccNum;
	}
	public String getComments() {
		return comments;
	}
	public void setComments(String comments) {
		this.comments = comments;
	}
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public Date getTimeCreated() {
		return timeCreated;
	}
	public void setTimeCreated(Date timeCreated) {
		this.timeCreated = timeCreated;
	}

	public long getWriterId() {
		return writerId;
	}
	public String getWriterName() throws SQLException{

		return ApplicationDataBase.getWriterName(writerId);
	}

	public void setWriterId(long writerId) {
		this.writerId = writerId;
	}
	public int getIsActive() {
		return isActive;
	}
	public void setIsActive(int active) {
		this.isActive = active;
	}

	public boolean getIsActiveBoolean() {
		return isActive==1;
	}
	public void setIsActiveBoolean(boolean a) {
		if (a)
			isActive=1;
		else
			isActive=0;
	}
	/**
	 * Constructs a <code>String</code> with all attributes
	 * in name = value format.
	 *
	 * @return a <code>String</code> representation
	 * of this object.
	 */
	public String toString()
	{
	    final String TAB = " \n ";

	    String retValue = "";

	    retValue = "CcBlackList ( "
	        + super.toString() + TAB
	        + "id = " + this.id + TAB
	        + "writerId = " + this.writerId + TAB
	        + "timeCreated = " + this.timeCreated + TAB
	        + "comments = " + this.comments + TAB
	        + "ccNum = " + this.ccNum + TAB
	        + "isActive = " + this.isActive + TAB
	        + " )";

	    return retValue;
	}



}
