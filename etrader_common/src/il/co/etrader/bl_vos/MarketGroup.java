//package il.co.etrader.bl_vos;
//
//import il.co.etrader.util.CommonUtil;
//import il.co.etrader.util.ConstantsBase;
//
//import java.io.Serializable;
//import java.sql.SQLException;
//import java.util.Date;
//
///**
// * Market group VO.
// */
//public  class MarketGroup implements Serializable {
//    private long id;
//    private String name;
//    private String displayName;
//    private String displayNameKey;
//    private Date timeCreated;
//    private String url; //group web page url
//
//    protected long writerId;
//    //private double acceptableLevelDeviation;
//
////    public String getTimeCreatedTxt() {
////		return CommonUtil.getDateTimeFormatDisplay(timeCreated, CommonUtil.getUtcOffset(ConstantsBase.EMPTY_STRING));
////	}
////    public String getWriterName() throws SQLException{
////		return CommonUtil.getWriterName(writerId);
////	}
//
//	/**
//     * @return Returns the id.
//     */
//    public long getId() {
//        return id;
//    }
//    /**
//     * @param id The id to set.
//     */
//    public void setId(long id) {
//        this.id = id;
//    }
//
//    /**
//     * @return Returns the name.
//     */
//    public String getName() {
//        return name;
//    }
//
//    /**
//     * @param name The name to set.
//     */
//    public void setName(String name) {
//        this.name = name;
//    }
//
//    /**
//     * @return Returns the displayName.
//     */
//    public String getDisplayName() {
//        return displayName;
//    }
//
//    /**
//     * @param displayName The displayName to set.
//     */
//    public void setDisplayName(String displayName) {
//        this.displayName = displayName;
//    }
//
//    /**
//     * @return Returns the timeCreated.
//     */
//    public Date getTimeCreated() {
//        return timeCreated;
//    }
//
//    /**
//     * @param timeCreated The timeCreated to set.
//     */
//    public void setTimeCreated(Date timeCreated) {
//        this.timeCreated = timeCreated;
//    }
//
//    /**
//     * @return Returns the writerId.
//     */
//    public long getWriterId() {
//        return writerId;
//    }
//
//    /**
//     * @param writerId The writerId to set.
//     */
//    public void setWriterId(long writerId) {
//        this.writerId = writerId;
//    }
//
//	/**
//	 * Constructs a <code>String</code> with all attributes
//	 * in name = value format.
//	 *
//	 * @return a <code>String</code> representation
//	 * of this object.
//	 */
//	public String toString()
//	{
//	    final String TAB = " \n ";
//
//	    String retValue = "";
//
//	    retValue = "MarketGroup ( "
//	        + super.toString() + TAB
//	        + "id = " + this.id + TAB
//	        + "name = " + this.name + TAB
//	        + "displayName = " + this.displayName + TAB
//	        + "timeCreated = " + this.timeCreated + TAB
//	        + "url = " + this.url + TAB
//	        + "writerId = " + this.writerId + TAB
//	        + " )";
//
//	    return retValue;
//	}
//	public String getDisplayNameKey() {
//		return displayNameKey;
//	}
//	public void setDisplayNameKey(String displayNameKey) {
//		this.displayNameKey = displayNameKey;
//	}
//    /**
//     * @return Returns the market url.
//     */
//	public String getUrl() {
//		return url;
//	}
//	/**
//     * @param url The url to set.
//     */
//	public void setUrl(String url) {
//		this.url = url;
//	}
//
//
//
//}