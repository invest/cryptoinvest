package il.co.etrader.bl_vos;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Marketing affiliate pixels class
 *
 * @author Eran.
 */
public class MarketingAffiliatePixels implements Serializable {
    private static final long serialVersionUID = 1L;

    protected long id;
    protected long affiliateId;
    protected long writerId;
    protected ArrayList<MarketingPixel> pixels;


	/**
	 * @return the id
	 */
	public long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(long id) {
		this.id = id;
	}

	/**
	 * @return the affiliateId
	 */
	public long getAffiliateId() {
		return affiliateId;
	}

	/**
	 * @param affiliateId the affiliateId to set
	 */
	public void setAffiliateId(long affiliateId) {
		this.affiliateId = affiliateId;
	}

	/**
	 * @return the writerId
	 */
	public long getWriterId() {
		return writerId;
	}

	/**
	 * @param writerId the writerId to set
	 */
	public void setWriterId(long writerId) {
		this.writerId = writerId;
	}

	/**
	 * @return the pixels
	 */
	public ArrayList<MarketingPixel> getPixels() {
		return pixels;
	}

	/**
	 * @param pixels the pixels to set
	 */
	public void setPixels(ArrayList<MarketingPixel> pixels) {
		this.pixels = pixels;
	}

}