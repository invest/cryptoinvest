package il.co.etrader.bl_vos;

import il.co.etrader.bl_managers.ApplicationDataBase;
import il.co.etrader.util.CommonUtil;

import java.sql.SQLException;
import java.util.Date;

/**
 * BinBlackList class.
 *
 * @author Kobi
 */
public class BinBlackList implements java.io.Serializable{
	private long id;
	private Date timeCreated;
	private long writerId;
	private long fromBin;
	private long toBin;
	private int isActive;
	private String comments;

	public BinBlackList() {

	}

	public String getTimeCreatedTxt() {
		return CommonUtil.getDateTimeFormatDisplay(timeCreated);
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Date getTimeCreated() {
		return timeCreated;
	}

	public void setTimeCreated(Date timeCreated) {
		this.timeCreated = timeCreated;
	}

	public long getWriterId() {
		return writerId;
	}

	public String getWriterName() throws SQLException{

		return ApplicationDataBase.getWriterName(writerId);
	}

	public void setWriterId(long writerId) {
		this.writerId = writerId;
	}

	public int getIsActive() {
		return isActive;
	}

	public void setIsActive(int active) {
		this.isActive = active;
	}

	public boolean getIsActiveBoolean() {
		return isActive == 1;
	}
	public void setIsActiveBoolean(boolean a) {
		if (a) {
			isActive = 1;
		}
		else {
			isActive = 0;
		}
	}

	/**
	 * @return the fromBin
	 */
	public long getFromBin() {
		return fromBin;
	}

	/**
	 * @param fromBin the fromBin to set
	 */
	public void setFromBin(long fromBin) {
		this.fromBin = fromBin;
	}

	/**
	 * @return the toBin
	 */
	public long getToBin() {
		return toBin;
	}

	/**
	 * @param toBin the toBin to set
	 */
	public void setToBin(long toBin) {
		this.toBin = toBin;
	}

	/**
	 * Constructs a <code>String</code> with all attributes
	 * in name = value format.
	 *
	 * @return a <code>String</code> representation
	 * of this object.
	 */
	public String toString()
	{
	    final String TAB = " \n ";

	    String retValue = "";

	    retValue = "CcBlackList ( "
	        + super.toString() + TAB
	        + "id = " + this.id + TAB
	        + "writerId = " + this.writerId + TAB
	        + "timeCreated = " + this.timeCreated + TAB
	        + "comments = " + this.comments + TAB
	        + "fromBin = " + this.fromBin + TAB
	        + "toBin = " + this.toBin + TAB
	        + "isActive = " + this.isActive + TAB
	        + " )";

	    return retValue;
	}

}
