//package il.co.etrader.bl_vos;
//
//import il.co.etrader.util.CommonUtil;
//import il.co.etrader.util.ConstantsBase;
//
//import java.sql.SQLException;
//import java.text.DecimalFormat;
//import java.text.NumberFormat;
//
//
///**
// * BonusUsers vo class.
// * @author Kobi
// */
//public class BonusUsers extends com.anyoption.common.beans.base.BonusUsers {
//	protected static final long serialVersionUID = 5289496849546211085L;
//
//	public BonusUsers() {
//		utcOffsetUser = null;
//		setBonusOddsWin(false);
//	}
//
//	/**
//	 * @return the bonusStateDescriptionTxt
//	 */
//	public String getBonusDescriptionTxt() {
//		String[] params = new String[7];
//		params[0] = String.valueOf(this.getBonusAmountTxt());
//		params[1] = String.valueOf(this.investmentId);
//		String format = "##0";
//		DecimalFormat fmt = new DecimalFormat(format);
//		params[2] = String.valueOf(fmt.format(this.bonusPercent*100));
//		params[3] = String.valueOf(this.getSumInvQualifyTxt());
//		params[4] = String.valueOf(this.numOfActions);
//		params[5] = String.valueOf(this.numOfActions+1);
//		params[6] = String.valueOf(this.getMinDepositAmountTxt());
//		return CommonUtil.getMessage(bonusStateDescription, params);
//	}
//
//
//	/**
//	 * @return the bonusAmount
//	 */
//	public String getBonusAmountTxt() {
//		return CommonUtil.displayAmount(bonusAmount, currency.getId());
//	}
//
//	public String getBonusOddsWinTxt() {
//		return CommonUtil.displayAmount(0, currency.getId());
//	}
//	/**
//	 * @return the adjusted amount.
//	 */
//	public String getBonusAdjustedAmountTxt() {
//		return CommonUtil.displayAmount(adjustedAmount, currency.getId());
//	}
//
//	/**
//	 * @return the endDateTxt
//	 */
//	public String getEndDateTxt() {
//		return CommonUtil.getDateFormat(endDate, ConstantsBase.OFFSET_GMT);
//	}
//
//	public String getMinDepositAmountTxt() {
//		return CommonUtil.displayAmount(minDepositAmount, currency.getId());
//	}
//
//	public String getMaxDepositAmountTxt(){
//		return  CommonUtil.displayAmount(maxDepositAmount,currency.getId());
//	}
//	public String getBonusPercentTxt(){
//		NumberFormat nf = NumberFormat.getIntegerInstance();
//		return nf.format(bonusPercent*100);
//	}
//	public String getSumInvQualifyTxt() {
//		return  CommonUtil.displayAmount(sumInvQualify,currency.getId());
//	}
//	public String getSumInvQualifyReachedTxt() {
//		return  CommonUtil.displayAmount(sumInvQualifyReached,currency.getId());
//	}
//	public String getSumInvWithdrawalTxt(){
//		return  CommonUtil.displayAmount(sumInvWithdrawal,currency.getId());
//	}
//	public String getSumInvWithdrawalReachedTxt(){
//		return  CommonUtil.displayAmount(sumInvWithdrawalReached,currency.getId());
//	}
//
//	/**
//	 * @return the startDateTxt
//	 */
//	public String getStartDateTxt() {
//		return CommonUtil.getDateFormat(startDate, CommonUtil.getUtcOffset());
//	}
//
//	/**
//	 * @return the timeActivated
//	 */
//	public String getTimeActivatedTxt() {
//		return CommonUtil.getDateFormat(timeActivated, CommonUtil.getUtcOffset());
//	}
//
//	/**
//	 * @return the timeCreatedTxt
//	 */
//	public String getTimeCreatedTxt() {
//		String utc = null;
//		if (null == utcOffsetUser) {
//			utc = CommonUtil.getUtcOffset();
//		} else {
//			utc = utcOffsetUser;
//		}
//		return CommonUtil.getDateFormat(timeCreated, utc);
//	}
//
//	/**
//	 * @return the dateAndTimeCreatedTxt
//	 */
//	public String getDateAndTimeCreatedTxt() {
//		String utc = null;
//		if (null == utcOffsetUser) {
//			utc = CommonUtil.getUtcOffset();
//		} else {
//			utc = utcOffsetUser;
//		}
//		return CommonUtil.getDateFormat(timeCreated, utc) + "  " + CommonUtil.getTimeFormat(timeCreated, utc);
//	}
//
//	/**
//	 * @return the getTimeDoneTxt
//	 */
//	public String getTimeDoneTxt() {
//		return CommonUtil.getDateFormat(timeDone, CommonUtil.getUtcOffset());
//	}
//
//	/**
//	 * @return the getTimeUsedTxt
//	 */
//	public String getTimeUsedTxt() {
//		return CommonUtil.getDateFormat(timeUsed, CommonUtil.getUtcOffset());
//	}
//
//	/**
//	 * @return the bonusStateTxt
//	 */
//	public String getBonusStateTxt() {
//		long returnState = 0L;
//		if (bonusStateId == ConstantsBase.BONUS_STATE_DONE || bonusStateId == ConstantsBase.BONUS_STATE_WITHDRAWN || bonusStateId == ConstantsBase.BONUS_STATE_WAGERING_WAIVED) {			
//			returnState = ConstantsBase.BONUS_STATE_USED;
//		} else {
//			returnState = bonusStateId;
//		}
//		return CommonUtil.getMessage("bonus.state" + returnState, null); 
//	}
//
//	/**
//	 * Get writer name
//	 * @return
//	 * @throws SQLException
//	 */
//	public String getWriterTxt() throws SQLException {
//		return CommonUtil.getWriterName(writerId);
//	}
//
//	public String getMaxInvestAmountTxt() {
//		return CommonUtil.displayAmount(maxInvestAmount, currency.getId());
//	}
//
//	public String getMinInvestAmountTxt() {
//		return CommonUtil.displayAmount(minInvestAmount, currency.getId());
//	}
//
//	/**
//	 * @return the sumDepositsReachedTxt
//	 */
//	public String getSumDepositsReachedTxt() {
//		return CommonUtil.displayAmount(sumDepositsReached, currency.getId());
//	}
//
//	/**
//	 * @return the sumDepositsTxt
//	 */
//	public String getSumDepositsTxt() {
//		return CommonUtil.displayAmount(sumDeposits, currency.getId());
//	}
//
//	/**
//	 * @return the timeRefusedTxt
//	 */
//	public String getTimeRefusedTxt() {
//		String utc = null;
//		if (null == utcOffsetUser) {
//			utc = CommonUtil.getUtcOffset();
//		} else {
//			utc = utcOffsetUser;
//		}
//		return CommonUtil.getDateFormat(timeRefused, utc);
//	}
//
//	/**
//	 * @return the timeCanceledTxt
//	 */
//	public String getTimeCanceledTxt() {
//		String utc = null;
//		if (null == utcOffsetUser) {
//			utc = CommonUtil.getUtcOffset();
//		} else {
//			utc = utcOffsetUser;
//		}
//		return CommonUtil.getDateFormat(timeCanceled, utc);
//	}
//
//	/**
//	 * calculate the amount to be deduct to the user
//	 * if the amount to be deduct is grater than the user balance, the amount to be deduct will be the user balance.
//	 * in case user's skin_id = 1 (ET) - because of tax issues , we will not deduct amount that is higher than original bonus amount
//	 * @param userBalance
//	 * @param skinId
//	 * @return the amount to be deduct from the user balance with currency.
//	 */
//	public String getAmountToDeduct(long userBalance, long skinId) {	
//		long amountToDeduct = 0;
//		if (this.bonusStateId == ConstantsBase.BONUS_STATE_ACTIVE || this.bonusStateId == ConstantsBase.BONUS_STATE_USED) {
//			amountToDeduct = bonusAmount;
//			if (skinId != Skins.SKIN_ETRADER) {
//				amountToDeduct = Math.max(bonusAmount, adjustedAmount);
//			}
//	        if ((userBalance - amountToDeduct) < 0) {
//	        	amountToDeduct = userBalance;
//	        }
//		}
//        
//        return CommonUtil.displayAmount(amountToDeduct, true, currency.getId());
//	}
//}
