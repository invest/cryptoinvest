package il.co.etrader.bl_vos;

import il.co.etrader.util.CommonUtil;

import java.io.Serializable;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;

import javax.faces.model.SelectItem;

import com.anyoption.common.beans.base.Skin;
import com.anyoption.common.util.ConstantsBase;

/**
 * Marketing combination class
 *
 * @author Kobi.
 */
public class MarketingCombination implements Serializable {
    private static final long serialVersionUID = 1L;

    protected long id;
    protected long skinId;
    protected long campaignId;
    protected long mediumId;
    protected long contentId;
    protected long sizeId;
    protected long typeId;
    protected long locationId;
    protected long landingPageId;
    protected long writerId;
    protected Date timeCreated;
    protected long pixelId;       // for adding a new pixel
    protected long sourceId;
    protected long paymentRecipientId;

    protected String skinName;
    protected String campaignName;
    protected String sourceName;
    protected String mediumName;
    protected String contentName;
    protected String typeName;
    protected String location;
    protected String landingPageName;
    protected String landingPageUrl;
    protected String verticalSize;
    protected String horizontalSizel;

    protected MarketingCombinationPixels combPixels;  // combination pixels
    protected long paymentType;

    protected String url; 	// final url
    protected String cdnUrl; 	// convert url to cdn
    
    protected String etsUrl;
    protected String etsCdnUrl;
    
    protected ArrayList<SelectItem> pixelsSi;
    protected int campaignPriority;
    protected String campaignDesc;
    protected int landingPageType;
    
    protected long urlSourceTypeId;
    protected String dynamicParam;
    protected MarketingDomains marketingDomain;
    protected long pathId;
    
    public MarketingCombination() {
    	marketingDomain = new MarketingDomains();
    	urlSourceTypeId = ConstantsBase.MC_URL_SOUREC_TYPE_WEB;
    	landingPageType = ConstantsBase.MARKETING_LANDING_PAGE_STATIC;
    }
    
	/**
	 * @return the campaignId
	 */
	public long getCampaignId() {
		return campaignId;
	}

	/**
	 * @param campaignId the campaignId to set
	 */
	public void setCampaignId(long campaignId) {
		this.campaignId = campaignId;
	}

	/**
	 * @return the contentId
	 */
	public long getContentId() {
		return contentId;
	}

	/**
	 * @param contentId the contentId to set
	 */
	public void setContentId(long contentId) {
		this.contentId = contentId;
	}

	/**
	 * @return the id
	 */
	public long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(long id) {
		this.id = id;
	}

	/**
	 * @return the landingPageId
	 */
	public long getLandingPageId() {
		return landingPageId;
	}

	/**
	 * @param landingPageId the landingPageId to set
	 */
	public void setLandingPageId(long landingPageId) {
		this.landingPageId = landingPageId;
	}

	/**
	 * @return the locationId
	 */
	public long getLocationId() {
		return locationId;
	}

	/**
	 * @param locationId the locationId to set
	 */
	public void setLocationId(long locationId) {
		this.locationId = locationId;
	}

	/**
	 * @return the mediumId
	 */
	public long getMediumId() {
		return mediumId;
	}

	/**
	 * @param mediumId the mediumId to set
	 */
	public void setMediumId(long mediumId) {
		this.mediumId = mediumId;
	}

	/**
	 * @return the sizeId
	 */
	public long getSizeId() {
		return sizeId;
	}

	/**
	 * @param sizeId the sizeId to set
	 */
	public void setSizeId(long sizeId) {
		this.sizeId = sizeId;
	}

	/**
	 * @return the skinId
	 */
	public long getSkinId() {
		return skinId;
	}

	/**
	 * @param skinId the skinId to set
	 */
	public void setSkinId(long skinId) {
		this.skinId = skinId;
	}

	/**
	 * @return the timeCreated
	 */
	public Date getTimeCreated() {
		return timeCreated;
	}

	/**
	 * @param timeCreated the timeCreated to set
	 */
	public void setTimeCreated(Date timeCreated) {
		this.timeCreated = timeCreated;
	}

	/**
	 * @return the typeId
	 */
	public long getTypeId() {
		return typeId;
	}

	/**
	 * @param typeId the typeId to set
	 */
	public void setTypeId(long typeId) {
		this.typeId = typeId;
	}

	/**
	 * @return the writerId
	 */
	public long getWriterId() {
		return writerId;
	}

	/**
	 * @param writerId the writerId to set
	 */
	public void setWriterId(long writerId) {
		this.writerId = writerId;
	}


	/**
	 * @return the campaignName
	 */
	public String getCampaignName() {
		return campaignName;
	}

	/**
	 * @param campaignName the campaignName to set
	 */
	public void setCampaignName(String campaignName) {
		this.campaignName = campaignName;
	}

	/**
	 * @return the contentName
	 */
	public String getContentName() {
		return contentName;
	}

	/**
	 * @param contentName the contentName to set
	 */
	public void setContentName(String contentName) {
		this.contentName = contentName;
	}

	/**
	 * @return the landingPageName
	 */
	public String getLandingPageName() {
		return landingPageName;
	}

	/**
	 * @param landingPageName the landingPageName to set
	 */
	public void setLandingPageName(String landingPageName) {
		this.landingPageName = landingPageName;
	}

	/**
	 * @return the landingPageUrl
	 */
	public String getLandingPageUrl() {
		return landingPageUrl;
	}

	/**
	 * @param landingPageUrl the landingPageUrl to set
	 */
	public void setLandingPageUrl(String landingPageUrl) {
		this.landingPageUrl = landingPageUrl;
	}

	/**
	 * @return the location
	 */
	public String getLocation() {
		return location;
	}

	/**
	 * @param location the location to set
	 */
	public void setLocation(String location) {
		this.location = location;
	}

	/**
	 * @return the mediumName
	 */
	public String getMediumName() {
		return mediumName;
	}

	/**
	 * @param mediumName the mediumName to set
	 */
	public void setMediumName(String mediumName) {
		this.mediumName = mediumName;
	}

	/**
	 * @return the sourceName
	 */
	public String getSourceName() {
		return sourceName;
	}

	/**
	 * @param sourceName the sourceName to set
	 */
	public void setSourceName(String sourceName) {
		this.sourceName = sourceName;
	}

	/**
	 * @return the typeName
	 */
	public String getTypeName() {
		return typeName;
	}

	/**
	 * @param typeName the typeName to set
	 */
	public void setTypeName(String typeName) {
		this.typeName = typeName;
	}

	/**
	 * @return the skinName
	 */
	public String getSkinName() {
		return skinName;
	}

	/**
	 * @param skinName the skinName to set
	 */
	public void setSkinName(String skinName) {
		this.skinName = skinName;
	}

	/**
	 * @return the horizontalSizel
	 */
	public String getHorizontalSizel() {
		return horizontalSizel;
	}

	/**
	 * @param horizontalSizel the horizontalSizel to set
	 */
	public void setHorizontalSizel(String horizontalSizel) {
		this.horizontalSizel = horizontalSizel;
	}

	/**
	 * @return the verticalSize
	 */
	public String getVerticalSize() {
		return verticalSize;
	}

	/**
	 * @param verticalSize the verticalSize to set
	 */
	public void setVerticalSize(String verticalSize) {
		this.verticalSize = verticalSize;
	}

	/**
	 * Get writer name
	 * @return
	 * @throws SQLException
	 */
	public String getWriterTxt() throws SQLException {
		return CommonUtil.getWriterName(writerId);
	}
	
    private String getEtsLandingPage() {
        String landingPage = CommonUtil.getProperty("anyoption.ets.landing.page.url");
        if (skinId == Skin.SKIN_ETRADER) {
            landingPage = CommonUtil.getProperty("etrader.ets.landing.page.url");
        } else if (skinId == Skin.SKIN_CHINESE_VIP) {
            landingPage = CommonUtil.getProperty("vip168.ets.landing.page.url");
        } else if (skinId == Skin.SKIN_REG_IT) {
            landingPage = CommonUtil.getProperty("anyoption.ets.it.landing.page.url");
        }
        
        return landingPage += "?type=dynamic";
    }
    
    private String getEtsCdnLandingPage() {
        String landingPage = CommonUtil.getProperty("anyoption.ets.landing.page.url");
        if (skinId == Skin.SKIN_ETRADER) {
            landingPage = CommonUtil.getProperty("etrader.ets.landing.page.url");
        } else if (skinId == Skin.SKIN_CHINESE_VIP) {
            landingPage = CommonUtil.getProperty("vip168.ets.landing.page.url");
        } else if (skinId == Skin.SKIN_REG_IT) {
            landingPage = CommonUtil.getProperty("anyoption.ets.it.landing.page.url");
        }
        
        return landingPage += "?type=static";
    } 	

	/**
	 * @return the url for the combination
	 */
	public String getUrl() {
		if (urlSourceTypeId == ConstantsBase.MC_URL_SOUREC_TYPE_ANDROID || urlSourceTypeId == ConstantsBase.MC_URL_SOUREC_TYPE_IOS){
			return createMobileUrl(marketingDomain.getDomain());
		} else {
			return createUrl(marketingDomain.getDomain());
		}
	}
	
    public String getEtsUrl() {
        String landingPage = getEtsLandingPage();   
        return createUrl(landingPage);
    }
    
    public void setEtsUrl(String etsUrl) {
        this.etsUrl = etsUrl;
    }

	/**
	 *  Create cdn URL:
	 *  Replace www with cdn
	 * @return the cndUrl for the combination
	 */
	public String getCdnUrl() {
		if (urlSourceTypeId == ConstantsBase.MC_URL_SOUREC_TYPE_ANDROID || urlSourceTypeId == ConstantsBase.MC_URL_SOUREC_TYPE_IOS){
			return createMobileUrl();
		} else {
			String landingPage = CommonUtil.getProperty("cdn.domain.anyoption");
			if (skinId == Skin.SKIN_ETRADER) {
				landingPage = CommonUtil.getProperty("cdn.domain.etrader");
			} else if (skinId == Skin.SKIN_CHINESE_VIP) {
				landingPage = CommonUtil.getProperty("cdn.domain.vip168");
			} else if (skinId == Skin.SKIN_REG_IT) {
				landingPage = CommonUtil.getProperty("cdn.domain.anyoption.it");
			}
			// change www to cdn.
			if (!CommonUtil.getProperty("system").equalsIgnoreCase("local")){ //not from local env
				landingPage = landingPage.replace("www", "cdn");
			}
			return createUrl(landingPage);
		}
	}
	
    public String getEtsCdnUrl() {
        String landingPage = getEtsCdnLandingPage();        
        return createUrl(landingPage);
    }	
	
	public void setCdnUrl(String cdnUrl){
	    this.cdnUrl = cdnUrl;
	}
	
    public void setEtsCdnUrl(String etsCdnUrl){
        this.etsCdnUrl = etsCdnUrl;
    }	

	/**
	 * @return the paymentType
	 */
	public long getPaymentType() {
		return paymentType;
	}

	/**
	 * @param paymentType the paymentType to set
	 */
	public void setPaymentType(long paymentType) {
		this.paymentType = paymentType;
	}


	/**
	 * @return the pixelId
	 */
	public long getPixelId() {
		return pixelId;
	}

	/**
	 * @param pixelId the pixelId to set
	 */
	public void setPixelId(long pixelId) {
		this.pixelId = pixelId;
	}


	/**
	 * @param url the url to set
	 */
	public void setUrl(String url) {
		this.url = url;
	}


	/**
	 * @return the combPixels
	 */
	public MarketingCombinationPixels getCombPixels() {
		return combPixels;
	}

	/**
	 * @param combPixels the combPixels to set
	 */
	public void setCombPixels(MarketingCombinationPixels combPixels) {
		this.combPixels = combPixels;
	}

	/**
	 * @return the pixelsSi
	 */
	public ArrayList<SelectItem> getPixelsSi() {
		return pixelsSi;
	}

	/**
	 * @param pixelsSi the pixelsSi to set
	 */
	public void setPixelsSi(ArrayList<SelectItem> pixelsSi) {
		this.pixelsSi = pixelsSi;
	}

	public String getCampaignForStrip() {
		String str = campaignName;
		if (null != contentName || null != location) {
			str += "_";
			if (null != contentName) {
				str += contentName;
			}
			str += "_";
			if (null != location) {
				str += location;
			}
		}

		return str;
	}

	/**
	 * toString implementation.
	 */
	public String toString() {
        String ls = System.getProperty("line.separator");
        return ls + "MarketingCombination:" + ls +
            "id: " + id + ls +
            "skinId: " + skinId + ls +
            "campaignId: " + campaignId + ls +
            "mediumId: " + mediumId + ls +
            "contentId: " + contentId + ls +
            "sizeId: " + sizeId + ls +
            "typeId: " + typeId + ls +
            "locationId: " + locationId + ls +
            "landingPageId: " + landingPageId + ls +
	        "writerId: " + writerId + ls +
	        "timeCreated: " + timeCreated + ls;
    }

	/**
	 * @return the campaignPriority
	 */
	public int getCampaignPriority() {
		return campaignPriority;
	}

	/**
	 * @param campaignPriority the campaignPriority to set
	 */
	public void setCampaignPriority(int campaignPrority) {
		this.campaignPriority = campaignPrority;
	}

	/**
	 * @return the campaignDesc
	 */
	public String getCampaignDesc() {
		return campaignDesc;
	}

	/**
	 * @param campaignDesc the campaignDesc to set
	 */
	public void setCampaignDesc(String campaignDesc) {
		this.campaignDesc = campaignDesc;
	}

	public long getPaymentRecipientId() {
		return paymentRecipientId;
	}

	public void setPaymentRecipientId(long paymentRecipientId) {
		this.paymentRecipientId = paymentRecipientId;
	}

	public long getSourceId() {
		return sourceId;
	}

	public void setSourceId(long sourceId) {
		this.sourceId = sourceId;
	}

	public String createUrl(String landingPage) {
		return createUrl(landingPage, false);
	}
	
	public String createUrl(String landingPage, boolean isFacebookCF) {
		String pageRStr = "";
		if (!isFacebookCF) {
			pageRStr = "&pageR=" + this.landingPageUrl;
		}
		// If it's Ultimate landing page - go with the old logic cause we have pixel in the landing page
		if (this.landingPageId == 52) {
			landingPage = this.landingPageUrl;
			pageRStr = "";
		}
		this.url = landingPage +
				   "?combid=" + this.id +
				   pageRStr +
				   "&utm_campaign=" + this.campaignId +
				   "&utm_source=" + this.sourceName +
				   "&utm_medium=" + this.mediumName +
				   "&utm_content=" + this.contentName;
		if ( sizeId > 0 || typeId > 0 || locationId > 0 ) {
			url += "&utm_term=";
			if ( sizeId > 0 ) {
				url += this.verticalSize + "X" + this.horizontalSizel;
			}
			if ( typeId > 0 ) {
				if ( sizeId > 0 ) {
					url += "|";
				}
				url += typeName;
			}
			if ( locationId > 0 ) {
				if ( sizeId > 0 || typeId > 0 ) {
					url += "|";
				}
				url += location;
			}
		}
		if (!isFacebookCF) {
			url += "&s=" + skinId;
		}
		if (!CommonUtil.isParameterEmptyOrNull(dynamicParam)){
			url += "&dp=" + this.dynamicParam;
		}	
		return url;
	}
	
	public String createMobileUrl() {
		String requestUrl = null;
		if (this.urlSourceTypeId == ConstantsBase.MC_URL_SOUREC_TYPE_ANDROID){
			requestUrl = "http://app.appsflyer.com/com.anyoption.android.app";
		} else if (this.urlSourceTypeId == ConstantsBase.MC_URL_SOUREC_TYPE_IOS){
			requestUrl = " http://app.appsflyer.com/id403987356";
		}
		this.url =requestUrl + "?" +
				"pid=" + this.sourceName + 
				"&c=" + this.campaignId +
				"&af_siteid=" + this.id +
				"&af_sub2=" + this.mediumName + 
				"&af_sub3=" + this.contentName;
		if (!CommonUtil.isParameterEmptyOrNull(dynamicParam)){
			url += "&af_sub1=" + this.dynamicParam;
		}				
		return url;
	}
	
    public String createEtsUrl(String landingPage) {
        String pageRStr = "&pageR=" + this.landingPageUrl;
        // If it's Ultimate landing page - go with the old logic cause we have pixel in the landing page
        if (this.landingPageId == 52) {
            landingPage = this.landingPageUrl;
            pageRStr = "";
        }
        this.etsUrl = landingPage +
                   "&combid=" + this.id +
                   pageRStr +
                   "&utm_campaign=" + this.campaignId +
                   "&utm_source=" + this.sourceName +
                   "&utm_medium=" + this.mediumName +
                   "&utm_content=" + this.contentName;
        if ( sizeId > 0 || typeId > 0 || locationId > 0 ) {
            etsUrl += "&utm_term=";
            if ( sizeId > 0 ) {
                etsUrl += this.verticalSize + "X" + this.horizontalSizel;
            }
            if ( typeId > 0 ) {
                if ( sizeId > 0 ) {
                    etsUrl += "|";
                }
                etsUrl += typeName;
            }
            if ( locationId > 0 ) {
                if ( sizeId > 0 || typeId > 0 ) {
                    etsUrl += "|";
                }
                etsUrl += location;
            }
        }
        etsUrl += "&s=" + skinId;
        return etsUrl;
    }

	public long getUrlSourceTypeId() {
		return urlSourceTypeId;
	}

	public void setUrlSourceTypeId(long urlSourceTypeId) {
		this.urlSourceTypeId = urlSourceTypeId;
	}

	public String getDynamicParam() {
		return dynamicParam;
	}

	public void setDynamicParam(String dynamicParam) {
		this.dynamicParam = dynamicParam;
	}

	public int getLandingPageType() {
		return landingPageType;
	}

	public void setLandingPageType(int landingPageType) {
		this.landingPageType = landingPageType;
	}

	/**
	 * @return the marketingDomain
	 */
	public MarketingDomains getMarketingDomain() {
		return marketingDomain;
	}

	/**
	 * @param marketingDomain the marketingDomain to set
	 */
	public void setMarketingDomain(MarketingDomains marketingDomain) {
		this.marketingDomain = marketingDomain;
	}
	
	/**
	 * Create mobile URL
	 * @param domain
	 * @return URL as String
	 */
	public String createMobileUrl(String domain) {
		this.url = domain + "?" +
				"pid=" + this.sourceName + 
				"&c=" + this.campaignId +
				"&af_siteid=" + this.id +
				"&af_sub2=" + this.mediumName + 
				"&af_sub3=" + this.contentName;
		if (!CommonUtil.isParameterEmptyOrNull(dynamicParam)){
			url += "&af_sub1=" + this.dynamicParam;
		}				
		return url;
	}

	/**
	 * @return the pathId
	 */
	public long getPathId() {
		return pathId;
	}

	/**
	 * @param pathId the pathId to set
	 */
	public void setPathId(long pathId) {
		this.pathId = pathId;
	}
	
	/**
	 * Create facebookCF URL
	 * @param type type of domain 1 - anyoption,  2 - copyop
	 * @return facebookCF URL
	 */
	public String getFacebookCFUrl(Integer type) {
		String landingPage = CommonUtil.getProperty("anyoption.facebook.CF.url");
		if (type == ConstantsBase.COPYOP_URL) {
			landingPage = CommonUtil.getProperty("copyop.facebook.CF.url");
		}		
		return createUrl(landingPage, true);
	}
	
}