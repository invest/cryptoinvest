package il.co.etrader.bl_vos;

import il.co.etrader.bl_managers.InvestmentsManagerBase;
import il.co.etrader.util.CommonUtil;
import il.co.etrader.util.ConstantsBase;

import java.sql.SQLException;
import java.util.Date;

import com.anyoption.common.enums.SkinGroup;

/**
 * Opportunity Shifting VO.
 */

public class OpportunityShifting implements java.io.Serializable {

	private static final long serialVersionUID = -184598570881249647L;

	private long id;
    private long opportunityId;
    private float shifting;
    private boolean up;
    private Date shiftingTime;
    private long writer_id;
    private long market_id;
    private Date time_est_closing;
    private long scheduled;
    private SkinGroup skinGroup;


    public String getDirectionTxt () {
    	if (up)
    		return CommonUtil.getMessage("opportunities.shiftings.directionup", null);
    	else
    		return CommonUtil.getMessage("opportunities.shiftings.directiondown", null);
    }

	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public long getOpportunityId() {
		return opportunityId;
	}
	public void setOpportunityId(long opportunityId) {
		this.opportunityId = opportunityId;
	}
	public float getShifting() {

		if (shifting==0)
			return 0;

		if (isUp())
			return shifting;
		else
			return -shifting;

	}
	public void setShifting(float shifting) {
		this.shifting = shifting;
	}
	public Date getShiftingTime() {
		return shiftingTime;
	}
	public void setShiftingTime(Date shiftingTime) {
		this.shiftingTime = shiftingTime;
	}
	public boolean isUp() {
		return up;
	}
	public void setUp(boolean up) {
		this.up = up;
	}
	public String getShiftingTimeTxt() {
		return CommonUtil.getDateTimeFormatDisplay(shiftingTime, CommonUtil.getUtcOffset(ConstantsBase.EMPTY_STRING));
	}
	public String getShiftingTxt() {
		return CommonUtil.displayDecimal(shifting);
	}
	public long getMarket_id() {
		return market_id;
	}
	public void setMarket_id(long market_id) {
		this.market_id = market_id;
	}
	public String getMarketName() {
		return CommonUtil.getMarketName(market_id);
	}
	public long getScheduled() {
		return scheduled;
	}
	public void setScheduled(long scheduled) {
		this.scheduled = scheduled;
	}
	public String getScheduledName() {
		return InvestmentsManagerBase.getScheduledTxt(scheduled);
	}
	public Date getTime_est_closing() {
		return time_est_closing;
	}
	public void setTime_est_closing(Date time_est_closing) {
		this.time_est_closing = time_est_closing;
	}
	public String getTimeEstClosingTxt() {
		return CommonUtil.getDateTimeFormatDisplay(time_est_closing, CommonUtil.getUtcOffset(ConstantsBase.EMPTY_STRING));
	}
	public long getWriter_id() {
		return writer_id;
	}
	public void setWriter_id(long writer_id) {
		this.writer_id = writer_id;
	}
	public String getWriterName() throws SQLException{
		return CommonUtil.getWriterName(writer_id);
	}

	/**
	 * @return the skinGroup
	 */
	public SkinGroup getSkinGroup() {
		return skinGroup;
	}

	/**
	 * @param skinGroup the skinGroup to set
	 */
	public void setSkinGroup(SkinGroup skinGroup) {
		this.skinGroup = skinGroup;
	}

	public String getTypeIdTxt() {
		if (skinGroup == SkinGroup.AUTO) {
			return CommonUtil.getMessage("opportunities.shiftings.type.auto", null);
		} else if (skinGroup == SkinGroup.ETRADER) {
			return CommonUtil.getMessage("opportunities.shiftings.type.et", null);
		} else if (skinGroup == SkinGroup.ANYOPTION) {
			return CommonUtil.getMessage("opportunities.shiftings.type.ao", null);
		} else if (skinGroup == SkinGroup.ZH) {
			return CommonUtil.getMessage("opportunities.shiftings.type.zh", null);
		} else if (skinGroup == SkinGroup.TR) {
			return CommonUtil.getMessage("opportunities.shiftings.type.tr", null);
		}
		
		return "error";
	}

	/**
	 * Constructs a <code>String</code> with all attributes
	 * in name = value format.
	 *
	 * @return a <code>String</code> representation
	 * of this object.
	 */
	public String toString()
	{
	    final String TAB = "    ";

	    String retValue = "";

	    retValue = "OpportunityShifting ( "
	        + super.toString() + TAB
	        + "id = " + this.id + TAB
	        + "opportunityId = " + this.opportunityId + TAB
	        + "shifting = " + this.shifting + TAB
	        + "up = " + this.up + TAB
	        + "shiftingTime = " + this.shiftingTime + TAB
	        + "writer_id = " + this.writer_id + TAB
	        + "market_id = " + this.market_id + TAB
	        + "time_est_closing = " + this.time_est_closing + TAB
	        + "scheduled = " + this.scheduled + TAB
	        + "skinGroup = " + this.skinGroup + TAB
	        + " )";

	    return retValue;
	}

}