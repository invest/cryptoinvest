package il.co.etrader.bl_vos;

//import il.co.etrader.util.CommonUtil;
import il.co.etrader.util.CommonUtil;
import il.co.etrader.util.ConstantsBase;

import java.io.Serializable;
import java.util.Date;


public class Analytics implements Serializable {

	private static final long serialVersionUID = 1L;
	private long platformId;
	private String platformName;
	private long ftdCount;
	private double depositAmount;
	private Date timeDataFetched;
	
	/**
	 * @return the ftdAmount
	 */
	public String getAmountTxt(double amount) {			
		return CommonUtil.displayAmount(Math.round(amount), true,  ConstantsBase.CURRENCY_USD_ID, false, "###,###,##0");
	}

	/**
	 * @return the depositAmount
	 */
	public double getDepositAmount() {
		return depositAmount;
	}

	/**
	 * @param depositAmount the depositAmount to set
	 */
	public void setDepositAmount(double depositAmount) {
		this.depositAmount = depositAmount;
	}

	/**
	 * @return the ftdCount
	 */
	public long getFtdCount() {
		return ftdCount;
	}

	/**
	 * @param ftdCount the ftdCount to set
	 */
	public void setFtdCount(long ftdCount) {
		this.ftdCount = ftdCount;
	}

	/**
	 * @return the platformName
	 */
	public String getPlatformName() {
		return platformName;
	}

	/**
	 * @param platformName the platformName to set
	 */
	public void setPlatformName(String platformName) {
		this.platformName = platformName;
	}

	/**
	 * @return the platformId
	 */
	public long getPlatformId() {
		return platformId;
	}

	/**
	 * @param platformId the platformId to set
	 */
	public void setPlatformId(long platformId) {
		this.platformId = platformId;
	}

	/**
	 * @return the timeDataFetched
	 */
	public Date getTimeDataFetched() {
		return timeDataFetched;
	}

	/**
	 * @param timeDataFetched the timeDataFetched to set
	 */
	public void setTimeDataFetched(Date timeDataFetched) {
		this.timeDataFetched = timeDataFetched;
	}
}
