package il.co.etrader.bl_vos;

import java.util.Date;

public class UsersAutoMail implements java.io.Serializable {

	private static final long serialVersionUID = -1382192838699918241L;	
	
	private long mailId;
	private long userId;
	private Date questionnaireDoneDate;
	private String skinName;
	
	public UsersAutoMail(long mailId, long userId) {
		super();
		this.mailId = mailId;
		this.userId = userId;
	}
	
	public UsersAutoMail(long mailId, long userId, Date questionnaireDoneDate, String skinName) {
		super();
		this.mailId = mailId;
		this.userId = userId;
		this.questionnaireDoneDate = questionnaireDoneDate;
		this.skinName = skinName;
	}
	
	public long getMailId() {
		return mailId;
	}
	public void setMailId(long mailId) {
		this.mailId = mailId;
	}
	public long getUserId() {
		return userId;
	}
	public void setUserId(long userId) {
		this.userId = userId;
	}
	
	public String toString() {
        String ls = System.getProperty("line.separator");
        return ls + "UsersAutoMail:" + ls +
            "mailId: " + mailId + ls +
            "userId: " + userId + ls;
    }
	public String getSkinName() {
		return skinName;
	}
	public void setSkinName(String skinName) {
		this.skinName = skinName;
	}
	public Date getQuestionnaireDoneDate() {
		return questionnaireDoneDate;
	}
	public void setQuestionnaireDoneDate(Date questionnaireDoneDate) {
		this.questionnaireDoneDate = questionnaireDoneDate;
	}
}
