//package il.co.etrader.bl_vos;
//
//import il.co.etrader.util.CommonUtil;
//
//import java.sql.Timestamp;
//import java.util.Calendar;
//import java.util.Date;
//import java.util.GregorianCalendar;
//
//import org.apache.myfaces.custom.fileupload.UploadedFile;
//
//import com.anyoption.common.beans.FilesAdditionalInfo;
//
//public class File extends com.anyoption.common.beans.File {
//
//
//	private static final long serialVersionUID = -8071060595433742435L;
////	private long id;
////	private long userId;
////	private String fileTypeId;
////	private long writerId;
////	private long uploaderId;
////	private String uploaderName;
////	private Date timeCreated;
////	private String utcOffsetCreated;
////	private String reference;
////	private UploadedFile file;
////	private long ccId;
////	private String ccName;
////	private boolean isApproved;
////	private boolean isColor;
////	private boolean isControlApproved;
////	private boolean isSupportReject;
////	private boolean isControlReject;
////	private Integer rejectReason;
////	private String comment;
////	private long rejectionWriterId;
////	private Date rejectionDate;
//	
////	private String fileName;
////	private String fileTypeName;
////	private String writerName;
//
//	//Risk Issues
////	private int fileStatusId;
////	private Date timeUpdated;
////	private long issueActionsId;
////	private long countryId;
//
////	//Files additional info
////	private String number;
////	private String firstName;
////	private String lastName;
////	private String expYear;
////	private String expMonth;
////	private String expDay;
////	private Date expDate;
////	private FilesAdditionalInfo filesAdditionalInfo;
//
////	public static final int STATUS_REQUESTED = 1;
////	public static final int STATUS_IN_PROGRESS = 2;
////	public static final int STATUS_DONE = 3;
////	public static final int STATUS_INVALID = 4;
////	public static final int STATUS_NOT_NEEDED = 5;
//
//	public File() {
//		super();
//	}
//	
//	public File(com.anyoption.common.beans.File file) {
//		this.setFileName(file.getFileName());
//		this.setFileType(file.getFileType());
//		this.setId(file.getId());
//		this.setFileTypeId(file.getFileTypeId());
//		this.setUserId(file.getUserId());
//		this.setWriterId(file.getWriterId());
//		this.setTimeCreated(file.getTimeCreated());
//		this.setUtcOffsetCreated(file.getUtcOffsetCreated());
//		this.setReference(file.getReference());
//		this.setCcId(file.getCcId());
//		this.setApproved(file.isApproved());
//		this.setColor(file.isColor());
//		this.setControlApproved(file.isControlApproved());
//		this.setSupportReject(file.isSupportReject());
//		this.setControlReject(file.isControlReject());
//		this.setRejectReason(file.getRejectReason());
//		this.setComment(file.getComment());
//		this.setCcName(file.getCcName());
//		this.setFileTypeName(file.getFileTypeName());
//		this.setUploaderId(file.getUploaderId());
//		this.setFileStatusId(file.getFileStatusId());
//		this.setTimeUpdated(file.getTimeUpdated());
//		this.setIssueActionsId(file.getIssueActionsId());
//		this.setCountryId(file.getCountryId());
//		this.setNumber(file.getNumber());
//		this.setFirstName(file.getFirstName());
//		this.setLastName(file.getLastName());
//		this.setExpDate(file.getExpDate());
//	}
//
////	public String getWriterName() {
////		return writerName;
////	}
////
////	public void setWriterName(String writerName) {
////		this.writerName = writerName;
////	}
//
//	public String getTimeCreatedTxt() {
//		return CommonUtil.getDateTimeFormatDisplay(timeCreated, CommonUtil.getUtcOffset(utcOffsetCreated));
//	}
//
////	public String getFileTypeId() {
////		return fileTypeId;
////	}
////
////	public void setFileTypeId(String fileTypeId) {
////		this.fileTypeId = fileTypeId;
////	}
//
////	public String getFileTypeName() {
////		return fileTypeName;
////	}
////
////	public void setFileTypeName(String fileTypeName) {
////		this.fileTypeName = fileTypeName;
////	}
//
////	public long getId() {
////		return id;
////	}
////
////	public void setId(long id) {
////		this.id = id;
////	}
//
////	public String getReference() {
////		return reference;
////	}
////
////	public void setReference(String reference) {
////		this.reference = reference;
////	}
//
////	public Date getTimeCreated() {
////		return timeCreated;
////	}
////
////	public void setTimeCreated(Date timeCreated) {
////		this.timeCreated = timeCreated;
////	}
//
////	public long getUserId() {
////		return userId;
////	}
////
////	public void setUserId(long userId) {
////		this.userId = userId;
////	}
//
////	public long getWriterId() {
////		return writerId;
////	}
////
////	public void setWriterId(long writerId) {
////		this.writerId = writerId;
////	}
////
////	public UploadedFile getFile() {
////		return file;
////	}
////
////	public void setFile(UploadedFile file) {
////		this.file = file;
////	}
//
////	public String getFileName() {
////		return fileName;
////	}
////
////	public void setFileName(String fileName) {
////		this.fileName = fileName;
////	}
////
////	/**
////	 * Constructs a <code>String</code> with all attributes
////	 * in name = value format.
////	 *
////	 * @return a <code>String</code> representation
////	 * of this object.
////	 */
////	public String toString()
////	{
////	    final String TAB = " \n ";
////
////	    String retValue = "";
////
////	    retValue = "File ( "
////	        + super.toString() + TAB
//////	        + "id = " + this.id + TAB
//////	        + "userId = " + this.userId + TAB
//////	        + "fileTypeId = " + this.fileTypeId + TAB
//////	        + "writerId = " + this.writerId + TAB
////	        + "timeCreated = " + this.timeCreated + TAB
//////	        + "reference = " + this.reference + TAB
////	        + "file = " + this.file + TAB
//////	        + "fileName = " + this.fileName + TAB
//////	        + "fileTypeName = " + this.fileTypeName + TAB
////	        + "writerName = " + this.writerName + TAB
////	        + "utcOffsetCreated = " + this.utcOffsetCreated + TAB
////	        + "fileStatusId = " + this.fileStatusId + TAB
//////	        + "timeUpdated = " + this.timeUpdated + TAB
//////	        + "issueActionsId = " + this.issueActionsId + TAB
//////	        + "firstName = " + this.firstName + TAB
//////	        + "lastName = " + this.lastName + TAB
//////	        + "number = " + this.number + TAB
////	        + " )";
////
////	    return retValue;
////	}
//
////	/**
////	 * @return the utcOffsetCreated
////	 */
////	public String getUtcOffsetCreated() {
////		return utcOffsetCreated;
////	}
////
////	/**
////	 * @param utcOffsetCreated the utcOffsetCreated to set
////	 */
////	public void setUtcOffsetCreated(String utcOffsetCreated) {
////		this.utcOffsetCreated = utcOffsetCreated;
////	}
//
////	public long getCcId() {
////		return ccId;
////	}
////
////	public void setCcId(long ccId) {
////		this.ccId = ccId;
////	}
//
////	public boolean isApproved() {
////		return isApproved;
////	}
////
////	public void setApproved(boolean isApproved) {
////		this.isApproved = isApproved;
////	}
//
////	public String getCcName() {
////		return ccName;
////	}
////
////	public void setCcName(String ccName) {
////		this.ccName = ccName;
////	}
//
////	/**
////	 * @return the fileStatusId
////	 */
////	public int getFileStatusId() {
////		return fileStatusId;
////	}
////
////	/**
////	 * @param fileStatusId the fileStatusId to set
////	 */
////	public void setFileStatusId(int fileStatusId) {
////		this.fileStatusId = fileStatusId;
////	}
//
////	/**
////	 * @return the issueActionsId
////	 */
////	public long getIssueActionsId() {
////		return issueActionsId;
////	}
////
////	/**
////	 * @param issueActionsId the issueActionsId to set
////	 */
////	public void setIssueActionsId(long issueActionsId) {
////		this.issueActionsId = issueActionsId;
////	}
////
////	/**
////	 * @return the timeUpdated
////	 */
////	public Date getTimeUpdated() {
////		return timeUpdated;
////	}
////
////	/**
////	 * @param timeUpdated the timeUpdated to set
////	 */
////	public void setTimeUpdated(Date timeUpdated) {
////		this.timeUpdated = timeUpdated;
////	}
////
////	/**
////	 * @return the filesAdditionalInfo
////	 */
////	public FilesAdditionalInfo getFilesAdditionalInfo() {
////		return filesAdditionalInfo;
////	}
////
////	/**
////	 * @param filesAdditionalInfo the filesAdditionalInfo to set
////	 */
////	public void setFilesAdditionalInfo(FilesAdditionalInfo filesAdditionalInfo) {
////		this.filesAdditionalInfo = filesAdditionalInfo;
////	}
//
////	/**
////	 * @return the firstName
////	 */
////	public String getFirstName() {
////		return firstName;
////	}
////
////	/**
////	 * @param firstName the firstName to set
////	 */
////	public void setFirstName(String firstName) {
////		this.firstName = firstName;
////	}
////
////	/**
////	 * @return the number
////	 */
////	public String getNumber() {
////		return number;
////	}
////
////	/**
////	 * @param number the number to set
////	 */
////	public void setNumber(String number) {
////		this.number = number;
////	}
////
////	/**
////	 * @return the lastName
////	 */
////	public String getLastName() {
////		return lastName;
////	}
////
////	/**
////	 * @param lastName the lastName to set
////	 */
////	public void setLastName(String lastName) {
////		this.lastName = lastName;
////	}
////
////	/**
////	 * @return the expDay
////	 */
////	public String getExpDay() {
////		return expDay;
////	}
////
////	/**
////	 * @param expDay the expDay to set
////	 */
////	public void setExpDay(String expDay) {
////		this.expDay = expDay;
////	}
////
////	/**
////	 * @return the expMonth
////	 */
////	public String getExpMonth() {
////		return expMonth;
////	}
////
////	/**
////	 * @param expMonth the expMonth to set
////	 */
////	public void setExpMonth(String expMonth) {
////		this.expMonth = expMonth;
////	}
////
////	/**
////	 * @return the expYear
////	 */
////	public String getExpYear() {
////		return expYear;
////	}
////
////	/**
////	 * @param expYear the expYear to set
////	 */
////	public void setExpYear(String expYear) {
////		this.expYear = expYear;
////	}
////
////	/**
////	 * @return the expDate
////	 */
////	public Date getExpDate() {
////		if (!CommonUtil.isParameterEmptyOrNull(expYear) && !CommonUtil.isParameterEmptyOrNull(expMonth) && !CommonUtil.isParameterEmptyOrNull(expDay)){
////			Calendar c = Calendar.getInstance();
////			c.set(Calendar.HOUR_OF_DAY, 12);
////			c.set(Calendar.MINUTE, 0);
////			c.set(Calendar.SECOND, 0);
////			c.set(Calendar.YEAR, Integer.valueOf(expYear));
////			c.set(Calendar.MONTH, Integer.valueOf(expMonth) - 1);
////			c.set(Calendar.DAY_OF_MONTH, Integer.valueOf(expDay));
////			expDate = c.getTime();
////		}
////		return expDate;
////	}
////
////	public Timestamp getExpDateTimestamp(){
////		getExpDate();
////		if (expDate != null){
////			return CommonUtil.convertToTimeStamp(expDate);
////		}
////		return null;
////	}
////
////	/**
////	 * @param expDate the expDate to set
////	 */
////	public void setExpDate(Date expDate) {
////		this.expDate = expDate;
////	}
////
////	/**
////	 * Build expiration date from parameters (year,month,day)
////	 */
////	public void setExpDateParams(){
////		Calendar c = Calendar.getInstance();
////		GregorianCalendar gc = new GregorianCalendar();
////		gc.setTime(expDate);
////		c.setTime(expDate);
////		expDay = String.valueOf(c.get(Calendar.DAY_OF_MONTH));
////		expMonth = String.valueOf(c.get(Calendar.MONTH)+1);
////		expYear = String.valueOf(c.get(Calendar.YEAR));
////		//add "0" String for drop down
////		if (expDay.length() == 1){
////			expDay = "0" + expDay;
////		}
////		if (expMonth.length() == 1){
////			expMonth = "0" + expMonth;
////		}
////	}
//
////	public long getCountryId() {
////		return countryId;
////	}
////
////	public void setCountryId(long countryId) {
////		this.countryId = countryId;
////	}
////	public boolean isColor() {
////		return isColor;
////	}
////
////	public void setColor(boolean isColor) {
////		this.isColor = isColor;
////	}
////
////	public boolean isControlApproved() {
////		return isControlApproved;
////	}
////
////	public void setControlApproved(boolean isControlApproved) {
////		this.isControlApproved = isControlApproved;
////	}
////
////	public boolean isSupportReject() {
////		return isSupportReject;
////	}
////
////	public void setSupportReject(boolean isSupportReject) {
////		this.isSupportReject = isSupportReject;
////	}
////
////	public boolean isControlReject() {
////		return isControlReject;
////	}
////
////	public void setControlReject(boolean isControlReject) {
////		this.isControlReject = isControlReject;
////	}
//
////	/**
////	 * @return the rejectReason
////	 */
////	public Integer getRejectReason() {
////		return rejectReason;
////	}
////
////	/**
////	 * @param rejectReason the rejectReason to set
////	 */
////	public void setRejectReason(Integer rejectReason) {
////		this.rejectReason = rejectReason;
////	}
////
////	/**
////	 * @return the comment
////	 */
////	public String getComment() {
////		return comment;
////	}
////
////	/**
////	 * @param comment the comment to set
////	 */
////	public void setComment(String comment) {
////		this.comment = comment;
////	}
//
////	public boolean isNotNeeded() {
////		return fileStatusId == STATUS_NOT_NEEDED;
////	}
////
////	public void setNotNeeded(boolean notNeeded) {
////		if(notNeeded){
////			fileStatusId = STATUS_NOT_NEEDED;
////		} else {
////			fileStatusId = STATUS_REQUESTED;
////		}
////	}
////
////	public long getRejectionWriterId() {
////		return rejectionWriterId;
////	}
////
////	public void setRejectionWriterId(long rejectionWriterId) {
////		this.rejectionWriterId = rejectionWriterId;
////	}
////
////	public Date getRejectionDate() {
////		return rejectionDate;
////	}
////
////	public void setRejectionDate(Date rejectionDate) {
////		this.rejectionDate = rejectionDate;
////	}
////
////	public Timestamp getRejectionDateTimestamp(){
////		if (rejectionDate != null){
////			return CommonUtil.convertToTimeStamp(rejectionDate);
////		}
////		return null;
////	}
////	
////	public String getRejectionDateTxt() {
////		return CommonUtil.getDateTimeFormatDisplay(rejectionDate, CommonUtil.getUtcOffset(utcOffsetCreated)).replace("&nbsp;", " ");
////	}
////
////	public long getUploaderId() {
////		return uploaderId;
////	}
////
////	public void setUploaderId(long uploaderId) {
////		this.uploaderId = uploaderId;
////	}
////
////	public String getUploaderName() {
////		return uploaderName;
////	}
////
////	public void setUploaderName(String uploaderName) {
////		this.uploaderName = uploaderName;
////	}
//}