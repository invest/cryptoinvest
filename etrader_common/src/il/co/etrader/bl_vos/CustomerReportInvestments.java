package il.co.etrader.bl_vos;

import il.co.etrader.bl_managers.InvestmentsManagerBase;
import il.co.etrader.util.CommonUtil;
import il.co.etrader.util.ConstantsBase;

import java.util.Date;
import java.util.Locale;

import com.anyoption.common.beans.Opportunity;
import com.anyoption.common.beans.base.Currency;
import com.anyoption.common.managers.CurrenciesManagerBase;
import com.anyoption.common.managers.MarketsManagerBase;


public class CustomerReportInvestments implements java.io.Serializable{

	public CustomerReportInvestments(long reportId, long reportType, long userId,
			String firstName, String lastName, String email, Date fromDate,
			Date toDate, long balanceBeginDate, long balanceEndDate,
			long investmentId, long opportunityType, long reportGroup,
			long marketId, String marketName, long direction,
			boolean isCanceled, boolean isSettled, String expiryLevel,
			Double purchaseLevel, Date expiredDateTime, Date timeCreated,
			Date canceledDateTime, long amount, long returnAmount, long returnAmountCanceledAfteSet,
			long returnIfCorrectAmount, long returnIfIncorrectAmount,
			String comments, long rfCommission, long tpCommission, long opFee,
			Date timeGenerate, 
			Double oppCuretLevel, Date oppTimeEstClosing, int oneTouchUpDown, int oneTouchDecimalPoint) {
		super();
		this.reportId = reportId;
		this.reportType = reportType;
		this.userId = userId;
		this.firstName = firstName;
		this.lastName = lastName;
		this.email = email;
		this.fromDate = fromDate;
		this.toDate = toDate;
		this.balanceBeginDate = balanceBeginDate;
		this.balanceEndDate = balanceEndDate;
		this.investmentId = investmentId;
		this.opportunityType = opportunityType;
		this.reportGroup = reportGroup;
		this.marketId = marketId;
		this.marketName = marketName;
		this.direction = direction;
		this.isCanceled = isCanceled;
		this.isSettled = isSettled;
		this.expiryLevel = expiryLevel;
		this.purchaseLevel = purchaseLevel;
		this.expiredDateTime = expiredDateTime;
		this.timeCreated = timeCreated;
		this.canceledDateTime = canceledDateTime;
		this.amount = amount;
		this.returnAmount = returnAmount;
		this.returnAmountCanceledAfteSet = returnAmountCanceledAfteSet;
		this.returnIfCorrectAmount = returnIfCorrectAmount;
		this.returnIfIncorrectAmount = returnIfIncorrectAmount;
		this.comments = comments;
		this.rfCommission = rfCommission;
		this.tpCommission = tpCommission;
		this.opFee = opFee;
		this.timeGenerate = timeGenerate;		
		this.oppCuretLevel = oppCuretLevel;
		this.oppTimeEstClosing = oppTimeEstClosing;
		this.oneTouchUpDown = oneTouchUpDown;
		this.oneTouchDecimalPoint = oneTouchDecimalPoint;
		
	}
	public CustomerReportInvestments() {

	}
	/**
	 * 
	 */
	public static final int CUSTOMER_REPORT_GROUP_ROLL_FORWARD_NATURAL_INVESTMENT = 2;
	public static final int CUSTOMER_REPORT_GROUP_ROLL_FORWARD_THEN_ROLL_FORWARDED_AGAIN = 3;
	public static final int CUSTOMER_REPORT_GROUP_ROLL_FORWARD_THEN_SETTLED_NATURALLY = 4;
	public static final int CUSTOMER_REPORT_GROUP_TAKE_PROFIT = 5;
	public static final int CUSTOMER_REPORT_GROUP_OPTION_PLUS = 6;
	public static final int CUSTOMER_REPORT_GROUP_BINARY_0100 = 8;
	public static final int CUSTOMER_REPORT_GROUP_CANCEL_BEFORE_SETTLED = 9;

	public static final int CUSTOMER_REPORT_GROUP_CANCEL_BO_AFTER_SETTLED = 101;
	public static final int CUSTOMER_REPORT_GROUP_CANCEL_RF_AFTER_SETTLED = 104;
	public static final int CUSTOMER_REPORT_GROUP_CANCEL_TP_AFTER_SETTLED = 105;
	public static final int CUSTOMER_REPORT_GROUP_CANCEL_OPP_AFTER_SETTLED = 106;
	public static final int CUSTOMER_REPORT_GROUP_CANCEL_OT_AFTER_SETTLED = 107;
	public static final int CUSTOMER_REPORT_GROUP_CANCEL_0100_AFTER_SETTLED = 108;
	
	public static final int CUSTOMER_REPORT_GROUP_ROLL_FORWARD_OPEN = 22;
	public static final int CUSTOMER_REPORT_GROUP_OPTION_PLUS_OPEN = 23;
	public static final int CUSTOMER_REPORT_GROUP_BINARY_0100_OPEN = 24;

	public static final String timeZoneName = "Asia/Jerusalem";
	private Locale locale = new Locale("iw");
	
	private static final long serialVersionUID = -993956099052992786L;
	private long reportId;
	private long reportType;
	
	private long userId;
	private String firstName;
	private String lastName;
	private String email;
	private Date fromDate;
	private Date toDate;
	
	private long balanceBeginDate;
	private long balanceEndDate;
	
	private long investmentId;
	private long opportunityType;
	private long reportGroup;
	private long marketId;
	private String marketName;
	private long direction;
	private boolean isCanceled;
	private boolean isSettled;
	
	private String expiryLevel;
	private Double purchaseLevel;
	private Date expiredDateTime;
	private Date timeCreated;
	private Date canceledDateTime;
	private long amount;
	private long returnAmount;
	private long returnAmountCanceledAfteSet;
	private long returnIfCorrectAmount;
	private long returnIfIncorrectAmount;
	private String comments;
	private long rfCommission;
	private long tpCommission;
	private long opFee;
	private Date timeGenerate;
	
	private Double oppCuretLevel;	
	private Date oppTimeEstClosing;	
	private int oneTouchUpDown = -1;
	private int oneTouchDecimalPoint;		
	
	Currency currency = CurrenciesManagerBase.getCurrency(Currency.CURRENCY_ILS_ID);
	
	public long getReportId() {
		return reportId;
	}
	public void setReportId(long reportId) {
		this.reportId = reportId;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public Date getFromDate() {
		return fromDate;
	}
	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}
	public Date getToDate() {
		return toDate;
	}
	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}
	public long getReportType() {
		return reportType;
	}
	public void setReportType(long reportType) {
		this.reportType = reportType;
	}
	public long getBalanceBeginDate() {
		return balanceBeginDate;
	}
	public void setBalanceBeginDate(long balanceBeginDate) {
		this.balanceBeginDate = balanceBeginDate;
	}
	public long getBalanceEndDate() {
		return balanceEndDate;
	}
	public void setBalanceEndDate(long balanceEndDate) {
		this.balanceEndDate = balanceEndDate;
	}
	public long getInvestmentId() {
		return investmentId;
	}
	public void setInvestmentId(long investmentId) {
		this.investmentId = investmentId;
	}
	public long getOpportunityType() {
		return opportunityType;
	}
	public void setOpportunityType(long opportunityType) {
		this.opportunityType = opportunityType;
	}
	public long getReportGroup() {
		return reportGroup;
	}
	public void setReportGroup(long reportGroup) {
		this.reportGroup = reportGroup;
	}
	public long getMarketId() {
		return marketId;
	}
	public void setMarketId(long marketId) {
		this.marketId = marketId;
	}
	public long getDirection() {
		if(direction == InvestmentsManagerBase.INVESTMENT_TYPE_ONE){
			if(oneTouchUpDown == ConstantsBase.ONE_TOUCH_IS_DOWN){
				return  2l;
			} else {
				return  1l;
			}
		}
		return direction;
	}
	public void setDirection(long direction) {
		this.direction = direction;
	}
	public boolean isCanceled() {
		return isCanceled;
	}
	public void setCanceled(boolean isCanceled) {
		this.isCanceled = isCanceled;
	}
	public boolean isSettled() {
		return isSettled;
	}
	public void setSettled(boolean isSettled) {
		this.isSettled = isSettled;
	}
	public String getExpiryLevel() {
		return expiryLevel;
	}
	public void setExpiryLevel(String expiryLevel) {
		this.expiryLevel = expiryLevel;
	}
	public Double getPurchaseLevel() {
		return purchaseLevel;
	}
	public void setPurchaseLevel(Double purchaseLevel) {
		this.purchaseLevel = purchaseLevel;
	}
	public Date getExpiredDateTime() {
		return expiredDateTime;
	}
	public void setExpiredDateTime(Date expiredDateTime) {
		this.expiredDateTime = expiredDateTime;
	}

	public Date getTimeCreated() {
		return timeCreated;
	}
	public void setTimeCreated(Date timeCreated) {
		this.timeCreated = timeCreated;
	}
	public Date getCanceledDateTime() {
		return canceledDateTime;
	}
	public void setCanceledDateTime(Date canceledDateTime) {
		this.canceledDateTime = canceledDateTime;
	}
	public long getAmount() {
		return amount;
	}
	public void setAmount(long amount) {
		this.amount = amount;
	}
	public long getReturnAmount() {
		return returnAmount;
	}
	public void setReturnAmount(long returnAmount) {
		this.returnAmount = returnAmount;
	}
	public long getReturnIfCorrectAmount() {
		return returnIfCorrectAmount;
	}
	public void setReturnIfCorrectAmount(long returnIfCorrectAmount) {
		this.returnIfCorrectAmount = returnIfCorrectAmount;
	}
	public long getReturnIfIncorrectAmount() {
		return returnIfIncorrectAmount;
	}
	public void setReturnIfIncorrectAmount(long returnIfIncorrectAmount) {
		this.returnIfIncorrectAmount = returnIfIncorrectAmount;
	}
	public String getComments() {
		return comments;
	}
	public void setComments(String comments) {
		this.comments = comments;
	}
	public Date getTimeGenerate() {
		return timeGenerate;
	}
	public void setTimeGenerate(Date timeGenerate) {
		this.timeGenerate = timeGenerate;
	}
	public String getMarketName() {
		return marketName;
	}
	public void setMarketName(String marketName) {
		this.marketName = marketName;
	}
	public long getRfCommission() {
		return rfCommission;
	}
	public void setRfCommission(long rfCommission) {
		this.rfCommission = rfCommission;
	}
	public long getTpCommission() {
		return tpCommission;
	}
	public void setTpCommission(long tpCommission) {
		this.tpCommission = tpCommission;
	}
	public long getOpFee() {
		return opFee;
	}
	public void setOpFee(long opFee) {
		this.opFee = opFee;
	}
	public long getUserId() {
		return userId;
	}
	public void setUserId(long userId) {
		this.userId = userId;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public long getReturnAmountCanceledAfteSet() {
		return returnAmountCanceledAfteSet;
	}
	public void setReturnAmountCanceledAfteSet(long returnAmountCanceledAfteSet) {
		this.returnAmountCanceledAfteSet = returnAmountCanceledAfteSet;
	}
	
	public Double getOppCuretLevel() {
		return oppCuretLevel;
	}
	public void setOppCuretLevel(Double oppCuretLevel) {
		this.oppCuretLevel = oppCuretLevel;
	}
	public Date getOppTimeEstClosing() {
		return oppTimeEstClosing;
	}
	public void setOppTimeEstClosing(Date oppTimeEstClosing) {
		this.oppTimeEstClosing = oppTimeEstClosing;
	}
	
	public int getOneTouchUpDown() {
		return oneTouchUpDown;
	}
	public void setOneTouchUpDown(int oneTouchUpDown) {
		this.oneTouchUpDown = oneTouchUpDown;
	}
	public int getOneTouchDecimalPoint() {
		return oneTouchDecimalPoint;
	}
	public void setOneTouchDecimalPoint(int oneTouchDecimalPoint) {
		this.oneTouchDecimalPoint = oneTouchDecimalPoint;
	}

	public String getFromDateTxt(){
		return CommonUtil.getDateTimeFormatDisplay(getFromDate(), null, "", "dd/MM/yyyy");
	}
	
	public String getToDateTxt(){
		return CommonUtil.getDateTimeFormatDisplay(getToDate(), null, "", "dd/MM/yyyy");
	}
	
	public String getTimeGenerateTxt() {
		return CommonUtil.getDateTimeFormatDisplay(getTimeGenerate(), timeZoneName, "", "dd/MM/yyyy HH:mm");
	}
	
	public String getBalanceBeginDateTxt() {
		return CommonUtil.formatCurrencyAmountCustomerReport(balanceBeginDate, currency);
	}
	
	public String getBalanceEndDateTxt() {
		return CommonUtil.formatCurrencyAmountCustomerReport(balanceEndDate, currency);
	}
	
	public String getAssetNameTxt() {
		if(opportunityType == Opportunity.TYPE_BINARY_0_100_ABOVE || opportunityType == Opportunity.TYPE_BINARY_0_100_BELOW){
			return getPrintBinary0100Event();
		}
		return MarketsManagerBase.getMarketName(Skins.SKIN_REG_EN, marketId);
	}
	
	private String getPrintBinary0100Event() {
		String result = null;
		String[] params = null;
		params=new String[3];
		params[0] = MarketsManagerBase.getMarketName(Skins.SKIN_ETRADER_INT, marketId);
		params[1] = CommonUtil.formatLevelByMarket(oppCuretLevel, marketId);
		params[2] = CommonUtil.getTimeFormat(oppTimeEstClosing, timeZoneName, "HH:mm");
		String messageKeyAbove = "opportunity.above";
		String messageKeyBelow = "opportunity.below";
		
//		if(CommonUtil.getMessage(locale, getMarketName(), null).toUpperCase().contains("DAX")){
//			messageKeyAbove = "opportunity.dax.above";
//			messageKeyBelow = "opportunity.dax.below";
//		}
		
		if (opportunityType == Opportunity.TYPE_BINARY_0_100_ABOVE) {
			result = CommonUtil.getMessage(locale, messageKeyAbove, params);
		}
		if (opportunityType == Opportunity.TYPE_BINARY_0_100_BELOW) {
			result = CommonUtil.getMessage(locale, messageKeyBelow, params);
		}

		return result;
	}
	
	public String getPurchaseLevelTxt() {

		if (InvestmentsManagerBase.INVESTMENT_TYPE_ONE == direction) {
				return CommonUtil.formatLevelByDecimalPoint(purchaseLevel, oneTouchDecimalPoint);
		}
		else {
			return CommonUtil.formatLevelByMarket(purchaseLevel, marketId);
		}
	}
	
	public String getExpiryLevelTxt(){
		String msg = expiryLevel;
		if(expiryLevel == null ){
			return "";
		}
		
		if(expiryLevel.contains("customer.report") || expiryLevel.contains("onetouch.investment")){
			msg = CommonUtil.getMessage(locale, expiryLevel, null); 
		} else if (CommonUtil.isParameterEmptyOrNull(expiryLevel)){
			msg = expiryLevel;
		} else {
			Double eLevel = new Double(expiryLevel);
			if(eLevel > 0){
				msg = CommonUtil.formatLevelByMarket(eLevel, marketId);
			} else{
				msg = "";
			}
		}
		return msg;
	}
	
	public String getTimeCreatedHourTxt() {
		return CommonUtil.getDateTimeFormatDisplay(timeCreated, timeZoneName, "", "HH:mm");
	}
	
	public String getTimeCreatedDaysTxt() {
		return CommonUtil.getDateTimeFormatDisplay(timeCreated, timeZoneName, "", "dd/MM/yyyy");
	}
	
	public String getExpiredDateTimeHourTxt() {
		if(reportGroup == CUSTOMER_REPORT_GROUP_CANCEL_BEFORE_SETTLED){
			return getCanceledHourTimeTxt();
		}
		return CommonUtil.getDateTimeFormatDisplay(expiredDateTime, timeZoneName, "", "HH:mm");
	}
	
	public String getExpiredDateTimeDaysTxt() {
		if(reportGroup == CUSTOMER_REPORT_GROUP_CANCEL_BEFORE_SETTLED){
			return getCanceledDateTimeTxt();
		}
		return CommonUtil.getDateTimeFormatDisplay(expiredDateTime,timeZoneName, "", "dd/MM/yyyy");
	}
	
	public String getCanceledHourTimeTxt(){
		return CommonUtil.getDateTimeFormatDisplay(canceledDateTime,timeZoneName, "", "HH:mm");
	}
	
	public String getCanceledDateTimeTxt(){
		return CommonUtil.getDateTimeFormatDisplay(canceledDateTime,timeZoneName, "", "dd/MM/yyyy");
	}
	
	public String getAmountTxt() {
		return CommonUtil.formatCurrencyAmountCustomerReport(amount, currency);
	}
	
	public String getReturnAmountTxt() {
		return CommonUtil.formatCurrencyAmountCustomerReport(returnAmount, currency);
	}
	
	public String getReturnIfCorrectAmountTxt() {
		return CommonUtil.formatCurrencyAmountCustomerReport(returnIfCorrectAmount, currency);
	}
	
	public String getReturnIfIncorrectAmountTxt() {
		return CommonUtil.formatCurrencyAmountCustomerReport(returnIfIncorrectAmount, currency);
	}
	
	public String getCommentsTxt() {
		
		if(!CommonUtil.isParameterEmptyOrNull(comments)){
			if(reportGroup == CUSTOMER_REPORT_GROUP_ROLL_FORWARD_NATURAL_INVESTMENT ||
					reportGroup == CUSTOMER_REPORT_GROUP_CANCEL_BEFORE_SETTLED ||
					reportGroup >= CUSTOMER_REPORT_GROUP_CANCEL_BO_AFTER_SETTLED ){
				return CommonUtil.getMessage(locale, comments, null);
			} else if(reportGroup == CUSTOMER_REPORT_GROUP_ROLL_FORWARD_THEN_ROLL_FORWARDED_AGAIN || 
						reportGroup == CUSTOMER_REPORT_GROUP_ROLL_FORWARD_THEN_SETTLED_NATURALLY ||
							reportGroup == CUSTOMER_REPORT_GROUP_CANCEL_RF_AFTER_SETTLED || 
									reportGroup == CUSTOMER_REPORT_GROUP_ROLL_FORWARD_OPEN){
				String[] params = null;
				params=new String[2];
				params[0] = CommonUtil.formatCurrencyAmountCustomerReport((amount - rfCommission), currency);//Amount				
				params[1] = CommonUtil.formatCurrencyAmountCustomerReport(rfCommission, currency); //Premium
				
				return CommonUtil.getMessage(locale, comments, params);
			} else if(reportGroup == CUSTOMER_REPORT_GROUP_TAKE_PROFIT || 
						reportGroup == CUSTOMER_REPORT_GROUP_CANCEL_TP_AFTER_SETTLED){
				String[] params = null;
				params=new String[2];
				params[0] = CommonUtil.formatCurrencyAmountCustomerReport((amount - tpCommission), currency);//Amount				
				params[1] = CommonUtil.formatCurrencyAmountCustomerReport(tpCommission, currency); //Premium				
				return CommonUtil.getMessage(locale, comments, params);
			} else if(reportGroup == CUSTOMER_REPORT_GROUP_OPTION_PLUS || 
						reportGroup == CUSTOMER_REPORT_GROUP_CANCEL_OPP_AFTER_SETTLED || 
							reportGroup == CUSTOMER_REPORT_GROUP_BINARY_0100 || 
								reportGroup == CUSTOMER_REPORT_GROUP_CANCEL_0100_AFTER_SETTLED ||
										reportGroup == CUSTOMER_REPORT_GROUP_OPTION_PLUS_OPEN ||
												reportGroup == CUSTOMER_REPORT_GROUP_BINARY_0100_OPEN){
			String[] params = null;
			params = new String[1];
			params[0] = CommonUtil.formatCurrencyAmountCustomerReport(opFee, currency); //Premium
			
			return CommonUtil.getMessage(locale, comments, params);
			}
		}
		return comments;
	}
	
	public boolean isDown() {
		if(getDirection() > 1){
			return true;
		}
		return false;
	}
	
	public boolean isEven(int i) {
		boolean res = false;
		if((i % 2) == 0){
			res = true;
		}		
		return res;
	}		
}
