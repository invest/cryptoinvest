package il.co.etrader.bl_vos;


/**
 * Chat class
 * For saving chat transcripts content of the chat's
 *
 * @author Kobi
 *
 */
public class Chat implements java.io.Serializable {
	private static final long serialVersionUID = 1L;

	private long id;
	private String chat;
	private String nonHtmlChat;
	private long typeId;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getChat() {
		return chat;
	}

	public void setChat(String chat) {
		this.chat = chat;
	}

	public long getTypeId() {
		return typeId;
	}

	public void setTypeId(long typeId) {
		this.typeId = typeId;
	}

	public String getNonHtmlChat() {
		return nonHtmlChat;
	}

	public void setNonHtmlChat(String nonHtmlChat) {
		this.nonHtmlChat = nonHtmlChat;
	}
}
