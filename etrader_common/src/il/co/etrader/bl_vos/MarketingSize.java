package il.co.etrader.bl_vos;

import il.co.etrader.util.CommonUtil;

import java.io.Serializable;
import java.sql.SQLException;
import java.util.Date;

/**
 * Marketing size class
 *
 * @author Kobi.
 */
public class MarketingSize implements Serializable {
    private static final long serialVersionUID = 1L;

    protected long id;
    protected long verticalSize;
    protected long horizontalSize;
    protected long writerId;
    protected Date timeCreated;

	/**
	 * @return the id
	 */
	public long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(long id) {
		this.id = id;
	}

	/**
	 * @return the timeCreated
	 */
	public Date getTimeCreated() {
		return timeCreated;
	}

	/**
	 * @return the timeCreatedTxt
	 */
	public String getTimeCreatedTxt() {
		return CommonUtil.getDateFormat(timeCreated, CommonUtil.getUtcOffset());
	}

	/**
	 * @param timeCreated the timeCreated to set
	 */
	public void setTimeCreated(Date timeCreated) {
		this.timeCreated = timeCreated;
	}

	/**
	 * @return the writerId
	 */
	public long getWriterId() {
		return writerId;
	}

	/**
	 * Get writer name
	 * @return
	 * @throws SQLException
	 */
	public String getWriterTxt() throws SQLException {
		return CommonUtil.getWriterName(writerId);
	}

	/**
	 * @param writerId the writerId to set
	 */
	public void setWriterId(long writerId) {
		this.writerId = writerId;
	}

	/**
	 * @return the horizontalSize
	 */
	public long getHorizontalSize() {
		return horizontalSize;
	}

	/**
	 * @param horizontalSize the horizontalSize to set
	 */
	public void setHorizontalSize(long horizontalSize) {
		this.horizontalSize = horizontalSize;
	}

	/**
	 * @return the verticalSize
	 */
	public long getVerticalSize() {
		return verticalSize;
	}

	/**
	 * @param verticalSize the verticalSize to set
	 */
	public void setVerticalSize(long verticalSize) {
		this.verticalSize = verticalSize;
	}

	/**
	 * Get sizeTxt
	 * @return
	 */
	public String getSizeTxt() {
		return this.verticalSize + "X" + this.horizontalSize;
	}

	/**
	 * toString implementation.
	 */
	public String toString() {
        String ls = System.getProperty("line.separator");
        return ls + "MarketingSize:" + ls +
            "id: " + id + ls +
            "vertinalSize: " + verticalSize + ls +
            "horizontalSize: " + horizontalSize + ls +
	        "writerId: " + writerId + ls +
	        "timeCreated: " + timeCreated + ls;
    }

}