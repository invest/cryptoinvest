//package il.co.etrader.bl_vos;
//
//import il.co.etrader.bl_managers.ApplicationDataBase;
//import il.co.etrader.util.CommonUtil;
//
//import java.io.Serializable;
//import java.util.Date;
//
///**
// * Tier class
// *
// * @author Kobi.
// */
//public class Tier implements Serializable {
//    private static final long serialVersionUID = 1L;
//
//    protected long id;
//    protected long qualificationPoints;
//    protected long maintenancePoints;
//    protected long minQualificationPeriod;   // min days after registration that user can qualified to some tie
//    protected long expirationDays;
//    protected long convertMinPoints;
//    protected long convertBenefitPercent;
//    protected Date timeCreated;
//    protected long writerId;
//    protected boolean loyaltyStatment;
//    protected boolean customerSupport;
//    protected boolean financialWorkshop;
//    protected boolean goldenMinutes;
//    protected boolean freeSms;
//    protected boolean personalGifts;
//    protected boolean fastWithdrawals;
//    protected boolean freeWithdrawals;
//    protected boolean vipCustomerSupport;
//    protected boolean vipAccountManager;
//    protected boolean personalSupportManager;
//    protected boolean haveSpecialBonuses;
//    protected String description;
//    protected long welcomeBonusId;
//    protected long welcomeTemplateId;
//
//    protected long skinId;
//    protected long tierLevel;
//
//	/**
//	 * @return the convertBenefitPercent
//	 */
//	public long getConvertBenefitPercent() {
//		return convertBenefitPercent;
//	}
//
//	/**
//	 * @param convertBenefitPercent the convertBenefitPercent to set
//	 */
//	public void setConvertBenefitPercent(long convertBenefitPercent) {
//		this.convertBenefitPercent = convertBenefitPercent;
//	}
//
//	/**
//	 * @return the convertMinPoints
//	 */
//	public long getConvertMinPoints() {
//		return convertMinPoints;
//	}
//
//	/**
//	 * @param convertMinPoints the convertMinPoints to set
//	 */
//	public void setConvertMinPoints(long convertMinPoints) {
//		this.convertMinPoints = convertMinPoints;
//	}
//
//	/**
//	 * @return the customerSupport
//	 */
//	public boolean isCustomerSupport() {
//		return customerSupport;
//	}
//
//	/**
//	 * @param customerSupport the customerSupport to set
//	 */
//	public void setCustomerSupport(boolean customerSupport) {
//		this.customerSupport = customerSupport;
//	}
//
//	/**
//	 * @return the description
//	 */
//	public String getDescription() {
//		return description;
//	}
//
//	/**
//	 * @param description the description to set
//	 */
//	public void setDescription(String description) {
//		this.description = description;
//	}
//
//	/**
//	 * @return the expirationDays
//	 */
//	public long getExpirationDays() {
//		return expirationDays;
//	}
//
//	/**
//	 * @param expirationDays the expirationDays to set
//	 */
//	public void setExpirationDays(long expirationDays) {
//		this.expirationDays = expirationDays;
//	}
//
//	/**
//	 * @return the fastWithdrawals
//	 */
//	public boolean isFastWithdrawals() {
//		return fastWithdrawals;
//	}
//
//	/**
//	 * @param fastWithdrawals the fastWithdrawals to set
//	 */
//	public void setFastWithdrawals(boolean fastWithdrawals) {
//		this.fastWithdrawals = fastWithdrawals;
//	}
//
//	/**
//	 * @return the financialWorkshop
//	 */
//	public boolean isFinancialWorkshop() {
//		return financialWorkshop;
//	}
//
//	/**
//	 * @param financialWorkshop the financialWorkshop to set
//	 */
//	public void setFinancialWorkshop(boolean financialWorkshop) {
//		this.financialWorkshop = financialWorkshop;
//	}
//
//	/**
//	 * @return the freeSms
//	 */
//	public boolean isFreeSms() {
//		return freeSms;
//	}
//
//	/**
//	 * @param freeSms the freeSms to set
//	 */
//	public void setFreeSms(boolean freeSms) {
//		this.freeSms = freeSms;
//	}
//
//	/**
//	 * @return the freeWithdrawals
//	 */
//	public boolean isFreeWithdrawals() {
//		return freeWithdrawals;
//	}
//
//	/**
//	 * @param freeWithdrawals the freeWithdrawals to set
//	 */
//	public void setFreeWithdrawals(boolean freeWithdrawals) {
//		this.freeWithdrawals = freeWithdrawals;
//	}
//
//	/**
//	 * @return the goldenMinutes
//	 */
//	public boolean isGoldenMinutes() {
//		return goldenMinutes;
//	}
//
//	/**
//	 * @param goldenMinutes the goldenMinutes to set
//	 */
//	public void setGoldenMinutes(boolean goldenMinutes) {
//		this.goldenMinutes = goldenMinutes;
//	}
//
//	/**
//	 * @return the haveSpecialBonuses
//	 */
//	public boolean isHaveSpecialBonuses() {
//		return haveSpecialBonuses;
//	}
//
//	/**
//	 * @param haveSpecialBonuses the haveSpecialBonuses to set
//	 */
//	public void setHaveSpecialBonuses(boolean haveSpecialBonuses) {
//		this.haveSpecialBonuses = haveSpecialBonuses;
//	}
//
//	/**
//	 * @return the id
//	 */
//	public long getId() {
//		return id;
//	}
//
//	/**
//	 * @param id the id to set
//	 */
//	public void setId(long id) {
//		this.id = id;
//	}
//
//	/**
//	 * @return the loyaltyStatment
//	 */
//	public boolean isLoyaltyStatment() {
//		return loyaltyStatment;
//	}
//
//	/**
//	 * @param loyaltyStatment the loyaltyStatment to set
//	 */
//	public void setLoyaltyStatment(boolean loyaltyStatment) {
//		this.loyaltyStatment = loyaltyStatment;
//	}
//
//	/**
//	 * @return the maintenancePoints
//	 */
//	public long getMaintenancePoints() {
//		return maintenancePoints;
//	}
//
//	/**
//	 * @param maintenancePoints the maintenancePoints to set
//	 */
//	public void setMaintenancePoints(long maintenancePoints) {
//		this.maintenancePoints = maintenancePoints;
//	}
//
//	/**
//	 * @return the minQualificationPeriod
//	 */
//	public long getMinQualificationPeriod() {
//		return minQualificationPeriod;
//	}
//
//	/**
//	 * @param minQualificationPeriod the minQualificationPeriod to set
//	 */
//	public void setMinQualificationPeriod(long minQualificationPeriod) {
//		this.minQualificationPeriod = minQualificationPeriod;
//	}
//
//	/**
//	 * @return the personalGifts
//	 */
//	public boolean isPersonalGifts() {
//		return personalGifts;
//	}
//
//	/**
//	 * @param personalGifts the personalGifts to set
//	 */
//	public void setPersonalGifts(boolean personalGifts) {
//		this.personalGifts = personalGifts;
//	}
//
//	/**
//	 * @return the personalSupportManager
//	 */
//	public boolean isPersonalSupportManager() {
//		return personalSupportManager;
//	}
//
//	/**
//	 * @param personalSupportManager the personalSupportManager to set
//	 */
//	public void setPersonalSupportManager(boolean personalSupportManager) {
//		this.personalSupportManager = personalSupportManager;
//	}
//
//	/**
//	 * @return the qualificationPoints
//	 */
//	public long getQualificationPoints() {
//		return qualificationPoints;
//	}
//
//	/**
//	 * @param qualificationPoints the qualificationPoints to set
//	 */
//	public void setQualificationPoints(long qualificationPoints) {
//		this.qualificationPoints = qualificationPoints;
//	}
//
//	/**
//	 * @return the timeCreated
//	 */
//	public Date getTimeCreated() {
//		return timeCreated;
//	}
//
//	/**
//	 * @param timeCreated the timeCreated to set
//	 */
//	public void setTimeCreated(Date timeCreated) {
//		this.timeCreated = timeCreated;
//	}
//
//	/**
//	 * @return the vipAccountManager
//	 */
//	public boolean isVipAccountManager() {
//		return vipAccountManager;
//	}
//
//	/**
//	 * @param vipAccountManager the vipAccountManager to set
//	 */
//	public void setVipAccountManager(boolean vipAccountManager) {
//		this.vipAccountManager = vipAccountManager;
//	}
//
//	/**
//	 * @return the vipCustomerSupport
//	 */
//	public boolean isVipCustomerSupport() {
//		return vipCustomerSupport;
//	}
//
//	/**
//	 * @param vipCustomerSupport the vipCustomerSupport to set
//	 */
//	public void setVipCustomerSupport(boolean vipCustomerSupport) {
//		this.vipCustomerSupport = vipCustomerSupport;
//	}
//
//	/**
//	 * @return the writerId
//	 */
//	public long getWriterId() {
//		return writerId;
//	}
//
//	/**
//	 * @param writerId the writerId to set
//	 */
//	public void setWriterId(long writerId) {
//		this.writerId = writerId;
//	}
//
//	/**
//	 * @return the welcomeBonusId
//	 */
//	public long getWelcomeBonusId() {
//		return welcomeBonusId;
//	}
//
//	/**
//	 * @param welcomeBonus the welcomeBonus to set
//	 */
//	public void setWelcomeBonusId(long welcomeBonusId) {
//		this.welcomeBonusId = welcomeBonusId;
//	}
//
//	/**
//	 * @return the skinId
//	 */
//	public long getSkinId() {
//		return skinId;
//	}
//
//	/**
//	 * @param skinId the skinId to set
//	 */
//	public void setSkinId(long skinId) {
//		this.skinId = skinId;
//	}
//
//	/**
//	 * @return the tierLevel
//	 */
//	public long getTierLevel() {
//		return tierLevel;
//	}
//
//	/**
//	 * @param tierLevel the tierLevel to set
//	 */
//	public void setTierLevel(long tierLevel) {
//		this.tierLevel = tierLevel;
//	}
//
//	/**
//	 * convert points to cash
//	 * @param points
//	 * @return
//	 */
//	public double convertedAmountByPoints(long points, double pointsToCashRate) {
//		double amount = 0;
//		amount = ((points * pointsToCashRate) * (1 + (convertBenefitPercent/100)));
//		return amount;
//	}
//
//	/**
//	 * toString implementation.
//	 */
//	public String toString() {
//        String ls = System.getProperty("line.separator");
//        return ls + "Tier:" + ls +
//            "id: " + id + ls +
//	        "qualificationPoints: " + qualificationPoints + ls +
//	        "maintenancePoints: " + maintenancePoints + ls +
//	        "minQualificationPeriod: " + minQualificationPeriod + ls +
//	        "expirationDays: " + expirationDays + ls +
//	        "convertMinPoints: " + convertMinPoints + ls +
//	        "convertBenefitPercent: " + convertBenefitPercent + ls +
//	        "timeCreated: " + timeCreated + ls +
//	        "writerId: " + writerId + ls +
//	        "loyaltyStatment: " + loyaltyStatment + ls +
//	        "customerSupport: " + customerSupport + ls +
//	        "financialWorkshop: " + financialWorkshop + ls +
//	        "goldenMinutes: " + goldenMinutes + ls +
//	        "freeSms: " + freeSms + ls +
//	        "personalGifts: " + personalGifts + ls +
//	        "fastWithdrawals: " + fastWithdrawals + ls +
//	        "vipCustomerSupport: " + vipCustomerSupport + ls +
//	        "vipAccountManager: " + vipAccountManager + ls +
//	        "personalSupportManager: " + personalSupportManager + ls +
//	        "haveSpecialBonuses: " + haveSpecialBonuses + ls +
//	        "description: " + description + ls +
//	        "welcomeBonusId: " + welcomeBonusId + ls;
//    }
//
//	/**
//	 * @return the welcomeTemplateId
//	 */
//	public long getWelcomeTemplateId() {
//		return welcomeTemplateId;
//	}
//
//	/**
//	 * @param welcomeTemplateId the welcomeTemplateId to set
//	 */
//	public void setWelcomeTemplateId(long welcomeTemplateId) {
//		this.welcomeTemplateId = welcomeTemplateId;
//	}
//
//}