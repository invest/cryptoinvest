package il.co.etrader.bl_vos;

import java.io.Serializable;

public class ClearingPriority implements Serializable{

	/**
	 * @author oshikl
	 */
	private static final long serialVersionUID = 6296943743535225701L;
	
	private long id;
	private long priority;
	private String priorityText;
	private long groupId;
	private String groupName;
	private boolean isChanged;
	
	public boolean isChanged() {
		return isChanged;
	}
	public void setChanged(boolean isChanged) {
		this.isChanged = isChanged;
	}
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public long getPriority() {
		return priority;
	}
	public void setPriority(long priority) {
		this.priority = priority;
	}
	public long getGroupId() {
		return groupId;
	}
	public void setGroupId(long groupId) {
		this.groupId = groupId;
	}
	public String getGroupName() {
		return groupName;
	}
	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}
	public String getPriorityText() {
		return priorityText;
	}
	public void setPriorityText(String priorityText) {
		this.priorityText = priorityText;
	}

}
