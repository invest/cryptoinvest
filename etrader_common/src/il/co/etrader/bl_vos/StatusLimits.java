package il.co.etrader.bl_vos;

public class StatusLimits implements java.io.Serializable {

	private static final long serialVersionUID = 1L;

	double limit;

	/**
	 * @return the limit
	 */
	public double getLimit() {
		return limit;
	}

	/**
	 * @param limit the limit to set
	 */
	public void setLimit(double limit) {
		this.limit = limit;
	}

		
}
