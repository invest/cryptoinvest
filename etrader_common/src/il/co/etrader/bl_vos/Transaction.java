//package il.co.etrader.bl_vos;
//
//import il.co.etrader.bl_managers.ApplicationDataBase;
//import il.co.etrader.bl_managers.TransactionsManagerBase;
//import il.co.etrader.bl_managers.UsersManagerBase;
//import il.co.etrader.util.CommonUtil;
//import il.co.etrader.util.ConstantsBase;
//
//import java.math.BigDecimal;
//import java.sql.SQLException;
//import java.text.DecimalFormat;
//import java.util.ArrayList;
//import java.util.Locale;
//
//import org.apache.log4j.Logger;
//
//import com.anyoption.common.beans.CreditCard;
//import com.anyoption.common.beans.base.Skin;
//import com.anyoption.common.bl_vos.ACHDepositBase;
//import com.anyoption.common.bl_vos.UkashDeposit;
//import com.anyoption.common.bl_vos.WebMoneyDeposit;
//import com.anyoption.common.bl_vos.WireBase;
//
//
///**
// *  WARNING : put in this class only formatting functions for web !
// *  New variables and methods that do not require CommonUtil 
// *  and ApplicationDataBase should go to the merged Transaction  
// * 
// */
//public class Transaction extends com.anyoption.common.beans.Transaction {
//
//	private static final long serialVersionUID = -4781726533384538156L;
//	private static final Logger log = Logger.getLogger(Transaction.class);
//
//	public Transaction() {
//		cheque = null;
//		creditCard = null;
//		wire = null;
//		noAch = false;
//		ach = null;
//		ukash = null;
//		chargeBack = null;
//		isCreditWithdrawal = false;
//		moneybookers = null;
//		webMoneyDeposit = null;
//		baroPayResponse = null;
//        specialCare = null;
//        cdpayDeposit = null;
//	}
//
//    public Transaction(int totalLine) {
//        super(totalLine);
//    }
//
//    public String getDollarAmount() {
//		try {
//			DecimalFormat sd = new DecimalFormat("###########0.00");
//			return sd.format(CommonUtil.convertToBaseAmount(amount, currency.getId(), timeCreated)/100);
//		} catch (Exception e) {
//			log.error("cant get dollar amount return 0 for transaction id = " + id , e);
//		}
//		return "0";
//	//	return sd.format((double)amount/(double)100/(double)AdminManagerBase.getLastShekelDollar());
//	}
//
//	public String getEuroAmount() {
//		try {
//			DecimalFormat sd = new DecimalFormat("###########0.00");
//			return sd.format(CommonUtil.convertToEuroAmount(amount, currency.getId(), timeCreated)/100);
//		} catch (Exception e) {
//			log.error("cant get euro amount return 0 for transaction id = " + id , e);
//		}
//		return "0";
//	//	return sd.format((double)amount/(double)100/(double)AdminManagerBase.getLastShekelDollar());
//	}
//
//	public CreditCard getCreditCard() throws SQLException{
//		if (creditCard==null) {
//			creditCard=TransactionsManagerBase.getCreditCard(creditCardId);
//		}
//		return creditCard;
//	}
//
//	public WireBase getWire() throws SQLException{
//		if (wire==null) {
//			wire=TransactionsManagerBase.getWire(wireId);
//		}
//		return wire;
//	}
//
//	public ACHDepositBase getAch() throws SQLException{
//		if (null == ach && !noAch) {
//			ach = TransactionsManagerBase.getAch(id);
//			if (null == ach) {
//				noAch = true;
//			}
//		}
//		return ach;
//	}
//
//	public UkashDeposit getUkashDeposit() throws SQLException{
//		if (ukash == null) {
//			ukash=TransactionsManagerBase.getUkash(id);
//		}
//		return ukash;
//	}
//
//	public String getExistFee() {
//		if (referenceId!=null) {
//			return CommonUtil.getMessage("yes",null);
//		} else {
//			return CommonUtil.getMessage("no",null);
//		}
//	}
//
//	public String getAmountTxt() {
//		return CommonUtil.displayAmount(amount, currency.getId());
//	}
//	public String getAmountTxtNoCur() {
//		return CommonUtil.displayAmount(amount,false, currency.getId());
//	}
//
//	public String getTimeSettledTxt() {
//		return CommonUtil.getDateTimeFormatDisplay(timeSettled, CommonUtil.getUtcOffset(utcOffsetSettled));
//	}
//	public String getLastWithdrawlSetteldTxt(){
//		return CommonUtil.getDateTimeFormatDisplay(lastWithdrawlSettled, CommonUtil.getUtcOffset(utcOffsetSettled));
//	}
//
//	/**
//	 * Get type name message
//	 * in case transaction type is direct banking deposit: add payment type name to message
//	 * @return
//	 */
//	public String getTypeNameTxt() {
//		String type = CommonUtil.getMessage(typeName, null);
//		if (typeId == TransactionsManagerBase.TRANS_TYPE_DIRECT_BANK_DEPOSIT &&
//				null != paymentTypeDesc) {
//			type = CommonUtil.getMessage(paymentTypeDesc, null);
//			if (isInatecInProgress()) {
//				type += " " + CommonUtil.getMessage("direct.banking.deposit.pending", null);
//			}
//		} else if (typeId == TransactionsManagerBase.TRANS_TYPE_ENVOY_ONLINE_DEPOSIT ||
//					typeId == TransactionsManagerBase.TRANS_TYPE_ENVOY_BANKING_DEPOSIT ||
//					typeId == TransactionsManagerBase.TRANS_TYPE_ENVOY_WITHDRAW){
//
//			type += " " + paymentTypeName;
//
//			if (this.statusId == TransactionsManagerBase.TRANS_STATUS_IN_PROGRESS){
//				type += " " + CommonUtil.getMessage("direct.banking.deposit.pending", null);
//			}
//		}
//		return type;
//	}
//
//
//	/**
//	 * Set class type and set credit member
//	 * @param ct
//	 */
//	@Override
//	public void setClassType(long ct) {
//		this.classType = ct;
//
//		if (this.classType == TransactionsManagerBase.TRANS_CLASS_DEPOSIT ||
//				(this.classType == TransactionsManagerBase.TRANS_CLASS_INTERNALS && this.typeId == TransactionsManagerBase.TRANS_TYPE_CC_DEPOSIT_REROUTE) || 
//				this.classType == TransactionsManagerBase.TRANS_CLASS_ADMIN_DEPOSITS ||
//				this.classType == TransactionsManagerBase.TRANS_CLASS_BONUS_DEPOSITS ||
//				this.classType == TransactionsManagerBase.TRANS_CLASS_REVERSE_WITHDRAWALS || 
//				(this.classType == TransactionsManagerBase.TRANS_CLASS_INTERNALS && this.typeId == TransactionsManagerBase.TRANS_TYPE_MAINTENANCE_FEE_CANCEL) ) {
//
//			credit = true;
//		} else {
//			credit = false;
//		}
//	}
//
//	@Override
//	public boolean isUserDeposit() {
//		if (typeId == TransactionsManagerBase.TRANS_TYPE_CC_DEPOSIT ||
//				typeId == TransactionsManagerBase.TRANS_TYPE_WIRE_DEPOSIT ||
//                typeId == TransactionsManagerBase.TRANS_TYPE_EFT_DEPOSIT ||
//				typeId == TransactionsManagerBase.TRANS_TYPE_CASH_DEPOSIT) {
//			return true;
//		}
//
//		return false;
//	}
//    
//	public boolean isBaroPayWithdrawl() { 
//		return (typeId == TransactionsManagerBase.TRANS_TYPE_BAROPAY_WITHDRAW);
//	}
//
//	public String getStatusName() {
//		long statusIdForDisplay = statusId;
//
//		if (ApplicationDataBase.isWeb() && TransactionsManagerBase.TRANS_STATUS_CHARGE_BACK == statusId){
//			statusIdForDisplay = TransactionsManagerBase.TRANS_STATUS_CANCEL_S_DEPOSIT;
//		}
//
//		return ApplicationDataBase.getTranStatusName(statusIdForDisplay);
//	}
//	public String getWriterName() throws SQLException{
//		return CommonUtil.getWriterName(writerId);
//	}
//	public String getProcessedWriterName() throws SQLException{
//		return CommonUtil.getWriterName(processedWriterId);
//	}
//
//	public void setChargeBack(ChargeBack chargeBack) {
//		this.chargeBack = chargeBack;
//	}
//
//	public void setCheque(Cheque cheque) {
//		this.cheque = cheque;
//	}
//
//	/**
//	 * @return
//	 * @throws Exception
//	 */
//	public ArrayList<Long> getTransactionsReroutConnectedIDById() throws Exception {
//    	return TransactionsManagerBase.getTransactionsReroutConnectedIDById(id);
//    }
//	
//	public String getAmountForInput() {
//		return CommonUtil.displayAmountForInput(amount, false, 0);
//	}
//
//	/**
//	 * Return true in case it's withdrawal trx or internal credit trx
//	 * @return
//	 */
//	@Override
//	public boolean isCcOrInternalWithdrawal() {
//		if (typeId == TransactionsManagerBase.TRANS_TYPE_CC_WITHDRAW ||
//				typeId == TransactionsManagerBase.TRANS_TYPE_INTERNAL_CREDIT
//				|| typeId == TransactionsManagerBase.TRANS_TYPE_CUP_INTERNAL_CREDIT
//				) {
//			return true;
//		}
//		return false;
//	}
//
//	/**
//	 * Return available credit amount with currency
//	 * @return
//	 */
//	public String getAvailableCreditAmountTxt() {
//		return CommonUtil.displayAmount(amount-creditAmount, currency.getId());
//	}
//
//	/**
//	 * Return credit amount with currency
//	 * @return
//	 */
//	public String getCreditAmountTxt() {
//		return CommonUtil.displayAmount(creditAmount, currency.getId());
//	}
//
//
//
//    public String getCurrencyTxt() {
//    	if (null!= currency){
//    		return CommonUtil.getMessage(currency.getSymbol(), null);
//    	}
//    	log.error("Currency wasn't found for transaction: " + id);
//        return "????";
//    }
//
//
//    public String getBusinessCaseNameTxt() {
//        return CommonUtil.getMessage(getBusinessCaseName(), null);
//    }
//
//
//    public String getBaseAmountTxt() {
//        return CommonUtil.displayAmount(getBaseAmount(),ConstantsBase.CURRENCY_BASE_ID);
//    }
//
//	/**
//	 * @return the ukash
//	 */
//	public UkashDeposit getUkash() {
//		return ukash;
//	}
//
//	/**
//	 * @return the webMoneyDeposit
//	 * @throws SQLException
//	 */
//	public WebMoneyDeposit getWebMoneyDeposit() throws SQLException {
//		if (null == webMoneyDeposit) {
//			webMoneyDeposit = TransactionsManagerBase.getWebMoneyDeposit(id);
//		}
//		return webMoneyDeposit;
//	}
//
//	/**
//	 * @param ukash the ukash to set
//	 */
//	public void setUkash(UkashDeposit ukash) {
//		this.ukash = ukash;
//	}
//
//	public String getUpdateDepositPixelRunTime() throws SQLException {
//		if (id != 0) {
//			TransactionsManagerBase.updatePixelRunTime(id);
//		}
//		return null;
//	}
//
//    public String getWireDepositReceipt() {
//        String[] params = new String[3];
//        params[0] = CommonUtil.getMessage(wire.getBankNameTxt(), null);
//        params[1] = wire.getBranch();
//        params[2] = wire.getAccountNum();
//        return CommonUtil.getMessage("transaction.wireDeposit.receipt", params);
//    }
//
//	public String getStyleForPendingWithdraw() {
//		String style = "";
//		
//		if((this.isOldTransaction() && !this.isWeekOldTransaction()) || this.isUserImmediateWithdraw()){
//			if(isRegulated()) {
//				style = "table_row_bordeaux";
//			} else {
//				style = "table_row_bold_red";
//			}
//		} else if (this.isWeekOldTransaction()){
//			if(isRegulated()) {
//				style = "table_row_bordeaux";
//			} else {
//				style = "user_strip_value_darkgreen";
//			}
//		}
//		return style;
//	}
//	
//    public String sendDepositReceipt() {  	
//    	UserBase user = new UserBase();
//    	try {
//			UsersManagerBase.getByUserId(userId, user);
//		} catch (SQLException e1) {
//			log.error("problem getting user from DB", e1);
//			return null;
//		}
//    	Locale locale = new Locale(ApplicationDataBase.getLanguage(ApplicationDataBase.getSkinById(user.getSkinId()).getDefaultLanguageId()).getCode());
//    	initFormattedValues(locale, user.getUtcOffset());
//    	
//    	//send the receipt deposit
//    	String[] params = new String[1];
//		params[0] = getCc4digit();
//		if ( user.getSkinId() != Skin.SKIN_ETRADER ) {  // for ao take transaction id
//			receiptNum = BigDecimal.valueOf(getId());
//		}		
//		user.setUserName(user.getFirstName() + " " + user.getLastName());
//		String footer = CommonUtil.getMessage("receipt.footer.creditcard.mb", params, locale);
//		if(getClearingProviderId() == 26 || getClearingProviderId() == 27 || getClearingProviderId() == 28) //AMEX
//        {
//            footer = footer + System.getProperty("line.separator") + CommonUtil.getMessage("receipt.bank_descriptor", params, locale);
//        }
//
//		try {
//			UsersManagerBase.sendMailTemplateFile(Template.CC_RECEIPT_MAIL_ID, Writer.WRITER_ID_WEB,	user, amountWF, footer, getReceiptNumWebDepositTxt(), null, null);
//		} catch (Exception e) {
//			log.error("problem sending mail after reciept", e);
//		}
//		return null;
//
//    }
//    
//    /**
//     * Init formatted values before serializing to JSON
//     * @param l
//     */
//	public void initFormattedValues(Locale l, String utcOffset) {
//		setTypeNameTxt(l);
//		setAmountTxt();
//		getReceiptNumWebDepositTxt();
//		//setTimeCreatedTxt(utcOffset);
//	}
//
//	 public void setAmountTxt() {
//		 amountWF = CommonUtil.displayAmount(amount, getCurrencyId());
//	 }
//
//	 
////	   public void setTimeCreatedTxt(String utcOffset) {
////	        timeCreated = CommonUtil.getDateTimeFormatDisplay(timeCreated, CommonUtil.getUtcOffset(utcOffsetCreated));
////	    }
//	
//	public boolean isTransactionCanceled(){
//		try {
//			if(TransactionsManagerBase.isMaintenanceFeeCanceled(this.getId())){
//				return false;	
//			}
//		} catch (SQLException e) {
//			e.printStackTrace();
//		}
//		return true;
//	}
//	
//}