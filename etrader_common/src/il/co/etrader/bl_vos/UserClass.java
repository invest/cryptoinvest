package il.co.etrader.bl_vos;

import java.util.Date;

public class UserClass implements java.io.Serializable {

	private long id;
	private int isDefault;
	private String name;

	private Date timeCreated;

	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public int getIsDefault() {
		return isDefault;
	}
	public void setIsDefault(int isDefault) {
		this.isDefault = isDefault;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Date getTimeCreated() {
		return timeCreated;
	}
	public void setTimeCreated(Date timeCreated) {
		this.timeCreated = timeCreated;
	}
	/**
	 * Constructs a <code>String</code> with all attributes
	 * in name = value format.
	 *
	 * @return a <code>String</code> representation
	 * of this object.
	 */
	public String toString()
	{
	    final String TAB = " \n ";

	    String retValue = "";

	    retValue = "UserClass ( "
	        + super.toString() + TAB
	        + "id = " + this.id + TAB
	        + "isDefault = " + this.isDefault + TAB
	        + "name = " + this.name + TAB
	        + "timeCreated = " + this.timeCreated + TAB
	        + " )";

	    return retValue;
	}


}
