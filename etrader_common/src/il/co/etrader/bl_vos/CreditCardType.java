//package il.co.etrader.bl_vos;
//
//public class CreditCardType implements java.io.Serializable {
//	private long id;
//	private String name;
//    protected boolean threeDSecure;
//
//	public long getId() {
//		return id;
//	}
//
//	public void setId(long id) {
//		this.id = id;
//	}
//
//	public String getName() {
//		return name;
//	}
//
//	public void setName(String name) {
//		this.name = name;
//	}
//
//	public boolean isThreeDSecure() {
//        return threeDSecure;
//    }
//
//    public void setThreeDSecure(boolean threeDSecure) {
//        this.threeDSecure = threeDSecure;
//    }
//
//    /**
//	 * @return a <code>String</code> representation of this object.
//	 */
//	public String toString() {
//	    String ls = System.getProperty("line.separator");
//	    return ls + "CreditCardType:" + ls
//	        + "id = " + id + ls
//	        + "name = " + name + ls
//            + "threeDSecure (3D) = " + threeDSecure + ls;
//	}
//}