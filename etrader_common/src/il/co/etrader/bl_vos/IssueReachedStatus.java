//package il.co.etrader.bl_vos;
//
//public class IssueReachedStatus  implements java.io.Serializable{
//
//	private long id;
//	private String name;
//	private String key;
//
//	/**
//	 * @return the key
//	 */
//	public String getKey() {
//		return key;
//	}
//	/**
//	 * @param key the key to set
//	 */
//	public void setKey(String key) {
//		this.key = key;
//	}
//	public long getId() {
//		return id;
//	}
//	public void setId(long id) {
//		this.id = id;
//	}
//	public String getName() {
//		return name;
//	}
//	public void setName(String name) {
//		this.name = name;
//	}
//	/**
//	 * Constructs a <code>String</code> with all attributes
//	 * in name = value format.
//	 *
//	 * @return a <code>String</code> representation
//	 * of this object.
//	 */
//	public String toString()
//	{
//	    final String TAB = " \n ";
//
//	    String retValue = "";
//
//	    retValue = "IssueReachedStatus ( "
//	        + super.toString() + TAB
//	        + "id = " + this.id + TAB
//	        + "name = " + this.name + TAB
//	        + " )";
//
//	    return retValue;
//	}
//
//}
