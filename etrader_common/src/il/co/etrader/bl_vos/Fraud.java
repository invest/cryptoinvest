package il.co.etrader.bl_vos;

import il.co.etrader.util.CommonUtil;

import java.util.Date;

public class Fraud implements java.io.Serializable{

	private long id;
	private long userId;
	private String typeId;
	private long writerId;
	private Date timeCreated;
	private String utcOffsetCreated;
	private String comments;

	private String typeName;
	private String writerName;

	public Fraud() {
		id=0;
	}

	public String getWriterName() {
		return writerName;
	}

	public void setWriterName(String writerName) {
		this.writerName = writerName;
	}

	public String getTimeCreatedTxt() {
		return CommonUtil.getDateTimeFormatDisplay(timeCreated, CommonUtil.getUtcOffset(utcOffsetCreated));
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Date getTimeCreated() {
		return timeCreated;
	}

	public void setTimeCreated(Date timeCreated) {
		this.timeCreated = timeCreated;
	}

	public long getUserId() {
		return userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}

	public long getWriterId() {
		return writerId;
	}

	public void setWriterId(long writerId) {
		this.writerId = writerId;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public String getTypeId() {
		return typeId;
	}

	public void setTypeId(String typeId) {
		this.typeId = typeId;
	}

	public String getTypeName() {
		return typeName;
	}

	public void setTypeName(String typeName) {
		this.typeName = typeName;
	}

	/**
	 * @return the utcOffsetCreated
	 */
	public String getUtcOffsetCreated() {
		return utcOffsetCreated;
	}

	/**
	 * @param utcOffsetCreated the utcOffsetCreated to set
	 */
	public void setUtcOffsetCreated(String utcOffsetCreated) {
		this.utcOffsetCreated = utcOffsetCreated;
	}

	/**
	 * Constructs a <code>String</code> with all attributes
	 * in name = value format.
	 *
	 * @return a <code>String</code> representation
	 * of this object.
	 */
	public String toString()
	{
	    final String TAB = " \n ";

	    String retValue = "";

	    retValue = "Fraud ( "
	        + super.toString() + TAB
	        + "id = " + this.id + TAB
	        + "userId = " + this.userId + TAB
	        + "typeId = " + this.typeId + TAB
	        + "writerId = " + this.writerId + TAB
	        + "timeCreated = " + this.timeCreated + TAB
	        + "comments = " + this.comments + TAB
	        + "typeName = " + this.typeName + TAB
	        + "writerName = " + this.writerName + TAB
	        + " )";

	    return retValue;
	}
}
