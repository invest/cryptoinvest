package il.co.etrader.bl_vos;

import il.co.etrader.util.CommonUtil;

import java.io.Serializable;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Date;
import java.util.GregorianCalendar;


/**
 * @author IdanZ
 *
 */
public class MediaBuyerTarget implements Serializable {

    private static final long serialVersionUID = 1L;
    private long id;
    private long mediaBuyerId;
    private long skinId;
    private Date monthTarget;
    private Date yearTarget;
    private long regTarget;
    private long depTarget;
    private long updatedWriter;
    private String mediaBuyerName;
    private String skinName;
    private String updatedWriterName;
    private long regMade;
    private long depMade;
    private String meetingReg;
    private String meetingDep;
    private String predictionReg;
    private String predictionDep;
    public static long totalRegMade;
    public static long totalDepMade;
    public static long totalTargetReg;
    public static long totalTargetDep;
    private String percentOfTotalReg;
    private String percentOfTotalDep;


//    MediaBuyerTarget() throws SQLException{
//        this.depMade = TargetsManager.getNumDepMade();
//    }


    /**
     * @return the depTarget
     */
    public long getDepTarget() {
        return depTarget;
    }
    /**
     * @param depTarget the depTarget to set
     */
    public void setDepTarget(long depTarget) {
        this.depTarget = depTarget;
    }
    /**
     * @return the id
     */
    public long getId() {
        return id;
    }
    /**
     * @param id the id to set
     */
    public void setId(long id) {
        this.id = id;
    }
    /**
     * @return the mediaBuyerId
     */
    public long getMediaBuyerId() {
        return mediaBuyerId;
    }
    /**
     * @param mediaBuyerId the mediaBuyerId to set
     */
    public void setMediaBuyerId(long mediaBuyerId) {
        this.mediaBuyerId = mediaBuyerId;
    }
    /**
     * @return the regTarget
     */
    public long getRegTarget() {
        return regTarget;
    }
    /**
     * @param regTarget the regTarget to set
     */
    public void setRegTarget(long regTarget) {
        this.regTarget = regTarget;
    }
    /**
     * @return the skinId
     */
    public long getSkinId() {
        return skinId;
    }
    /**
     * @param skinId the skinId to set
     */
    public void setSkinId(long skinId) {
        this.skinId = skinId;
    }

    /**
     * @return the updatedWriter
     */
    public long getUpdatedWriter() {
        return updatedWriter;
    }
    /**
     * @param updatedWriter the updatedWriter to set
     */
    public void setUpdatedWriter(long updatedWriter) {
        this.updatedWriter = updatedWriter;
    }
    /**
     * @param monthTarget the monthTarget to set
     */
    public void setMonthTarget(Date monthTarget) {
        this.monthTarget = monthTarget;
    }
    /**
     * @return the monthTarget
     */
    public Date getMonthTarget() {
        return monthTarget;
    }
    /**
     * @return the mediaBuyerName
     * @throws SQLException
     */
    public String getMediaBuyerName() throws SQLException {
        if (mediaBuyerId != 0) {
            return CommonUtil.getWriterName(mediaBuyerId);
        } else {
            return this.mediaBuyerName;
        }
    }
    /**
     * @param mediaBuyerName the mediaBuyerName to set
     */
    public void setMediaBuyerName(String mediaBuyerName) {
        this.mediaBuyerName = mediaBuyerName;
    }
    /**
     * @return the skinName
     */
    public String getSkinName() {
        return skinName;
    }
    /**
     * @param skinName the skinName to set
     */
    public void setSkinName(String skinName) {
        this.skinName = skinName;
    }
    /**
     * @return the updatedWriterName
     */
    public String getUpdatedWriterName() {
        return updatedWriterName;
    }
    /**
     * @param updatedWriterName the updatedWriterName to set
     */
    public void setUpdatedWriterName(String updatedWriterName) {
        this.updatedWriterName = updatedWriterName;
    }
    /**
     * @return the regMade
     */
    public Long getRegMade() {
        return regMade;
    }
    /**
     * @param regMade the regMade to set
     */
    public void setRegMade(Long regMade) {
        this.regMade = regMade;
    }
    /**
     * @return the depMade
     */
    public Long getDepMade() {
        return depMade;
    }
    /**
     * @param depMade the depMade to set
     */
    public void setDepMade(Long depMade) {
        this.depMade = depMade;
    }
    /**
     * @return the meetingDep
     */
    public String getMeetingDep() {
        if (depTarget != 0){
            NumberFormat format = new DecimalFormat("0%");
            double meetingDepDouble = (double)depMade/depTarget;
            return format.format(meetingDepDouble);
        } else {
            return "0";
        }
    }
    /**
     * @param meetingDep the meetingDep to set
     */
    public void setMeetingDep(String meetingDep) {
        this.meetingDep = meetingDep;
            //Math.round((double)(depMade/regTarget*100));
    }
    /**
     * @return the predictionReg
     */
    public String getPredictionReg() {
        int daysFromBeginMonth = GregorianCalendar.getInstance().get(GregorianCalendar.DAY_OF_MONTH);
        int year = GregorianCalendar.getInstance().get(GregorianCalendar.YEAR);
        GregorianCalendar gc = new GregorianCalendar();
        if (monthTarget != null){
            gc.set(year,monthTarget.getMonth(),daysFromBeginMonth);
        } else {
            int monthT =  GregorianCalendar.getInstance().get(GregorianCalendar.MONTH);
            gc.set(year, monthT ,daysFromBeginMonth);
        }
        long daysInMonth = gc.getActualMaximum(GregorianCalendar.DAY_OF_MONTH);
        double predictionReg = ((double)regMade/daysFromBeginMonth)*(double)daysInMonth;
        DecimalFormat df = new DecimalFormat("0");
        return String.valueOf(df.format(predictionReg));
    }
    /**
     * @param predictionReg the predictionReg to set
     */
    public void setPredictionReg(String predictionReg) {
        this.predictionReg = predictionReg;
    }
    /**
     * @return the predictionDep
     */
    public String getPredictionDep() {
        int daysFromBeginMonth = GregorianCalendar.getInstance().get(GregorianCalendar.DAY_OF_MONTH);
        int year = GregorianCalendar.getInstance().get(GregorianCalendar.YEAR);
        GregorianCalendar gc = new GregorianCalendar();
        if (monthTarget != null){
            gc.set(year,monthTarget.getMonth(),daysFromBeginMonth);
        } else {
            int monthT =  GregorianCalendar.getInstance().get(GregorianCalendar.MONTH);
            gc.set(year, monthT ,daysFromBeginMonth);
        }
        long daysInMonth = gc.getActualMaximum(GregorianCalendar.DAY_OF_MONTH);
        double predictionDep = ((double)depMade/daysFromBeginMonth)*(double)daysInMonth;
        DecimalFormat df = new DecimalFormat("0");
        return String.valueOf(df.format(predictionDep));
    }
    /**
     * @param predictionDep the predictionDep to set
     */
    public void setPredictionDep(String predictionDep) {
        this.predictionDep = predictionDep;
    }
    /**
     * @param meetingReg the meetingReg to set
     */
    public void setMeetingReg(String meetingReg) {
        this.meetingReg = meetingReg;
    }
    /**
     * @return the meetingReg
     */
    public String getMeetingReg() {
        if (regTarget != 0){
            NumberFormat format = new DecimalFormat("0%");
            double meetingRegDouble = (double)regMade/regTarget;
            return format.format(meetingRegDouble);
        } else {
            return "0";
        }
    }
    /**
     * @return the totalRegMade
     */
    public long getTotalRegMade() {
        return totalRegMade;
    }
    /**
     * @param totalRegMade the totalRegMade to set
     */
    public void setTotalRegMade(long totalRegMade) {
        this.totalRegMade = totalRegMade;
    }
    /**
     * @return the totalDepMade
     */
    public long getTotalDepMade() {
        return totalDepMade;
    }
    /**
     * @param totalDepMade the totalDepMade to set
     */
    public void setTotalDepMade(long totalDepMade) {
        this.totalDepMade = totalDepMade;
    }
    /**
     * @return the percentOfTotalReg
     */
    public String getPercentOfTotalReg() {
        if (totalRegMade == 0){
            return "0%";
        } else {
            NumberFormat format = new DecimalFormat("0%");
            double percentOfTotalReg = (double)regMade/totalRegMade;
            return format.format(percentOfTotalReg);
        }

    }
    /**
     * @param percentOfTotalReg the percentOfTotalReg to set
     */
    public void setPercentOfTotalReg(String percentOfTotalReg) {
        this.percentOfTotalReg = percentOfTotalReg;
    }
    /**
     * @return the percentOfTotalDep
     */
    public String getPercentOfTotalDep() {
        if (totalDepMade == 0){
            return "0%";
        } else {
            NumberFormat format = new DecimalFormat("0%");
            double percentOfTotalDep = (double)depMade/totalDepMade;
            return format.format(percentOfTotalDep);
        }
    }
    /**
     * @param percentOfTotalDep the percentOfTotalDep to set
     */
    public void setPercentOfTotalDep(String percentOfTotalDep) {
        this.percentOfTotalDep = percentOfTotalDep;
    }
    /**
     * @return the yearTarget
     */
    public Date getYearTarget() {
        return yearTarget;
    }
    /**
     * @param yearTarget the yearTarget to set
     */
    public void setYearTarget(Date yearTarget) {
        this.yearTarget = yearTarget;
    }
    /**
     * @return the totalTargetReg
     */
    public static long getTotalTargetReg() {
        return totalTargetReg;
    }
    /**
     * @param totalTargetReg the totalTargetReg to set
     */
    public static void setTotalTargetReg(long totalTargetReg) {
        MediaBuyerTarget.totalTargetReg = totalTargetReg;
    }
    /**
     * @return the totalTargetDep
     */
    public static long getTotalTargetDep() {
        return totalTargetDep;
    }
    /**
     * @param totalTargetDep the totalTargetDep to set
     */
    public static void setTotalTargetDep(long totalTargetDep) {
        MediaBuyerTarget.totalTargetDep = totalTargetDep;
    }

}
