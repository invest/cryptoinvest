package il.co.etrader.bl_vos;
import il.co.etrader.bl_managers.InvestmentsManagerBase;
import il.co.etrader.bl_managers.TransactionsManagerBase;
import il.co.etrader.util.CommonUtil;

import java.sql.SQLException;
import java.util.Date;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.Investment;
import com.anyoption.common.beans.Transaction;

public class TranInvestment implements java.io.Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -6551476432533251302L;

	private static final Logger logger = Logger.getLogger(TranInvestment.class);

	public static int TRANSACTION=0;
	public static int INVESTMENT=1;

	private long balance;
	private long taxBalance;
	private String tableName;
	private long key;
	private int invTranType;
	private String type;
	private Date timeCreated;
	protected String utcOffsetCreated;
	private long amount;
	private int currencyId;
	private long win;
	private long lose;
	private String writer;
	private boolean rolledUpInvest;
	private Transaction transaction;
	private Investment investment;

	public TranInvestment() {
	}

	public Transaction getTransaction() throws SQLException{
		if (isCheckInvestment())
			return null;

		if (transaction == null) {
			logger.debug("get transaction from db");
			transaction = TransactionsManagerBase.getTransaction(key);
		}
		return transaction;
	}
	public Investment getInvestment() throws SQLException{
		if (!isCheckInvestment())
			return null;

		if (investment==null) {
			logger.debug("get investment from db");
			investment=InvestmentsManagerBase.getInvestmentDetails(key);
		}
		return investment;
	}
	public String getTimeCreatedTxt() {
		return CommonUtil.getDateTimeFormatDisplay(timeCreated, CommonUtil.getUtcOffset(utcOffsetCreated));
	}
	public String getAmountTxt() {
		return CommonUtil.displayAmount(amount, currencyId);
	}
	public String getBalanceTxt() {
		return CommonUtil.displayAmount(balance, currencyId);
	}
	public String getTaxBalanceTxt() {
		return CommonUtil.displayAmount(taxBalance, currencyId);
	}

	public String getWinTxt() {
		if (win==0)
			return "";
		return CommonUtil.displayAmount(win, currencyId);
	}
	public String getLoseTxt() {
		if (lose==0)
			return "";
		return CommonUtil.displayAmount(lose, currencyId);
	}

	public long getAmount() {
		return amount;
	}
	public void setAmount(long amount) {
		this.amount = amount;
	}

	public int getInvTranType() {
		return invTranType;
	}
	public void setInvTranType(int invTranType) {
		this.invTranType = invTranType;
	}
	public long getLose() {
		return lose;
	}
	public void setLose(long lose) {
		this.lose = lose;
	}
	public Date getTimeCreated() {
		return timeCreated;
	}
	public void setTimeCreated(Date timeCreated) {
		this.timeCreated = timeCreated;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
	public long getWin() {
		return win;
	}
	public void setWin(long win) {
		this.win = win;
	}
	public String getWriter() {
		return writer;
	}
	public void setWriter(String writer) {
		this.writer = writer;
	}

	public boolean isCheckInvestment() {
		if (invTranType==INVESTMENT)
			return true;

		return false;
	}

	public long getBalance() {
		return balance;
	}

	public void setBalance(long balance) {
		this.balance = balance;
	}

	public long getKey() {
		return key;
	}

	public void setKey(long key) {
		this.key = key;
	}

	public String getTableName() {
		return tableName;
	}

	public void setTableName(String tableName) {
		this.tableName = tableName;
	}

	public long getTaxBalance() {
		return taxBalance;
	}

	public void setTaxBalance(long taxBalance) {
		this.taxBalance = taxBalance;
	}

	/**
	 * Constructs a <code>String</code> with all attributes
	 * in name = value format.
	 *
	 * @return a <code>String</code> representation
	 * of this object.
	 */
	public String toString()
	{
	    final String TAB = " \n ";

	    String retValue = "";

	    retValue = "TranInvestment ( "
	        + super.toString() + TAB
	        + "balance = " + this.balance + TAB
	        + "taxBalance = " + this.taxBalance + TAB
	        + "tableName = " + this.tableName + TAB
	        + "key = " + this.key + TAB
	        + "invTranType = " + this.invTranType + TAB
	        + "type = " + this.type + TAB
	        + "timeCreated = " + this.timeCreated + TAB
	        + "amount = " + this.amount + TAB
	        + "win = " + this.win + TAB
	        + "lose = " + this.lose + TAB
	        + "writer = " + this.writer + TAB
	        + "transaction = " + this.transaction + TAB
	        + "investment = " + this.investment + TAB
	        + " )";

	    return retValue;
	}

	public int getCurrencyId() {
		return currencyId;
	}

	public void setCurrencyId(int currencyId) {
		this.currencyId = currencyId;
	}

	/**
	 * @param utcOffset the utcOffset to set
	 */
	public void setUtcOffsetCreated(String utcOffsetCreated) {
		this.utcOffsetCreated = utcOffsetCreated;
	}

	/**
	 * @return the rolledUpInvest
	 */
	public boolean isRolledUpInvest() {
		return rolledUpInvest;
	}

	/**
	 * @param rolledUpInvest the rolledUpInvest to set
	 */
	public void setRolledUpInvest(boolean rolledUpInvest) {
		this.rolledUpInvest = rolledUpInvest;
	}

}
