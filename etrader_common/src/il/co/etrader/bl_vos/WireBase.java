package il.co.etrader.bl_vos;

import il.co.etrader.util.CommonUtil;

import com.anyoption.common.bl_vos.BankBase;

public class WireBase extends com.anyoption.common.bl_vos.WireBase implements java.io.Serializable {

	private static final long serialVersionUID = 2586458267594676743L;

	public String getBankName() {
		if (bankId == null) {
			return "";
		}
		BankBase bank = CommonUtil.getCacheObjects().getBankById(new Long(bankId).longValue());
    	return bank.getName();
    }
}
