package il.co.etrader.bl_vos;

import il.co.etrader.util.CommonUtil;

import java.io.Serializable;
import java.util.ArrayList;

import com.anyoption.common.beans.base.Currency;

/**
 * @author eranl
 *
 */
public class CopyopInvestmentDetails implements Serializable {

	private static final long serialVersionUID = -7984108086902280725L;
	
	
	private long originalInvestmentAmountUSD;
	private long originalInvestmentAmount;
	private long originalUserId;
	private String originalUserName;
	private String originalRate;
	private long originalInvestmentId;
	private long sumOriginalAndCopyUSD;
	private long timesCopied;
	private long currencyId;
	private long userCurrencyId;
	private ArrayList<Pair> copyBy; 
	
	public CopyopInvestmentDetails() {
		copyBy = new ArrayList<Pair>();	 
	}

	/**
	 * @return the originalInvestmentId
	 */
	public long getOriginalInvestmentId() {
		return originalInvestmentId;
	}

	/**
	 * @param originalInvestmentId the originalInvestmentId to set
	 */
	public void setOriginalInvestmentId(long originalInvestmentId) {
		this.originalInvestmentId = originalInvestmentId;
	}

	/**
	 * @return the originalInvestmentAmount
	 */
	public long getOriginalInvestmentAmount() {
		return originalInvestmentAmount;
	}

	/**
	 * @param originalInvestmentAmount the originalInvestmentAmount to set
	 */
	public void setOriginalInvestmentAmount(long originalInvestmentAmount) {
		this.originalInvestmentAmount = originalInvestmentAmount;
	}

	/**
	 * @return the sumOriginalAndCopyUSD
	 */
	public long getSumOriginalAndCopyUSD() {
		return sumOriginalAndCopyUSD;
	}

	/**
	 * @param sumOriginalAndCopyUSD the sumOriginalAndCopyUSD to set
	 */
	public void setSumOriginalAndCopyUSD(long sumOriginalAndCopyUSD) {
		this.sumOriginalAndCopyUSD = sumOriginalAndCopyUSD;
	}

	/**
	 * @return the timesCopied
	 */
	public long getTimesCopied() {
		return timesCopied;
	}

	/**
	 * @param timesCopied the timesCopied to set
	 */
	public void setTimesCopied(long timesCopied) {
		this.timesCopied = timesCopied;
	}
	
	/**
	 * @return the originalUserId
	 */
	public long getOriginalUserId() {
		return originalUserId;
	}

	/**
	 * @param originalUserId the originalUserId to set
	 */
	public void setOriginalUserId(long originalUserId) {
		this.originalUserId = originalUserId;
	}

	/**
	 * @return the originalUserName
	 */
	public String getOriginalUserName() {
		return originalUserName;
	}

	/**
	 * @return the originalRate
	 */
	public String getOriginalRate() {
		return originalRate;
	}

	/**
	 * @param originalRate the originalRate to set
	 */
	public void setOriginalRate(String originalRate) {
		this.originalRate = originalRate;
	}

	/**
	 * @param originalUserName the originalUserName to set
	 */
	public void setOriginalUserName(String originalUserName) {
		this.originalUserName = originalUserName;
	}

	/**
	 * @return the copyBy
	 */
	public ArrayList<Pair> getCopyBy() {
		return copyBy;
	}

	/**
	 * @param copyBy the copyBy to set
	 */
	public void setCopyBy(ArrayList<Pair> copyBy) {
		this.copyBy = copyBy;
	}
	
	/**
	 * @return the currencyId
	 */
	public long getCurrencyId() {
		return currencyId;
	}

	/**
	 * @param currencyId the currencyId to set
	 */
	public void setCurrencyId(long currencyId) {
		this.currencyId = currencyId;
	}
	
	/**
	 * @return the userCurrencyId
	 */
	public long getUserCurrencyId() {
		return userCurrencyId;
	}

	/**
	 * @param userCurrencyId the userCurrencyId to set
	 */
	public void setUserCurrencyId(long userCurrencyId) {
		this.userCurrencyId = userCurrencyId;
	}
	
	/**
	 * @return the originalInvestmentAmountUSD
	 */
	public long getOriginalInvestmentAmountUSD() {
		return originalInvestmentAmountUSD;
	}

	/**
	 * @param originalInvestmentAmountUSD the originalInvestmentAmountUSD to set
	 */
	public void setOriginalInvestmentAmountUSD(long originalInvestmentAmountUSD) {
		this.originalInvestmentAmountUSD = originalInvestmentAmountUSD;
	}

	public String getOriginalInvestmentAmountTxt() {
		return CommonUtil.displayAmount(originalInvestmentAmount, true, userCurrencyId);
	}
	
	public String getSumOriginalAndCopyUSDTxt() {
		return CommonUtil.displayAmount(sumOriginalAndCopyUSD, Currency.CURRENCY_USD_ID);
	}
	
	public String getOriginalInvestmentAmountUSDTxt() {
		return CommonUtil.displayAmount(originalInvestmentAmountUSD, Currency.CURRENCY_USD_ID);
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "CopyopInvestmentDetails [originalInvestmentAmountUSD="
				+ originalInvestmentAmountUSD + ", originalInvestmentAmount="
				+ originalInvestmentAmount + ", originalUserId="
				+ originalUserId + ", originalUserName=" + originalUserName
				+ ", originalRate=" + originalRate + ", originalInvestmentId="
				+ originalInvestmentId + ", sumOriginalAndCopyUSD="
				+ sumOriginalAndCopyUSD + ", timesCopied=" + timesCopied
				+ ", currencyId=" + currencyId + ", userCurrencyId="
				+ userCurrencyId + ", copyBy=" + copyBy + "]";
	}


	/**
	 * Pair of copyop copiers details (user name and investment id)
	 * @author eranl
	 *
	 */
	public static class Pair implements Serializable {
		
		private static final long serialVersionUID = -3537036524540601978L;
		
		private String userName;
		private long investmentId;
		
		public Pair() {}
		
		public Pair(String userName, long investmentId) {
			this.userName = userName;
			this.investmentId = investmentId;			
		}
		
		/**
		 * @return the userName
		 */
		public String getUserName() {
			return userName;
		}
		/**
		 * @param userName the userName to set
		 */
		public void setUserName(String userName) {
			this.userName = userName;
		}

		/**
		 * @return the investmentId
		 */
		public long getInvestmentId() {
			return investmentId;
		}

		/**
		 * @param investmentId the investmentId to set
		 */
		public void setInvestmentId(long investmentId) {
			this.investmentId = investmentId;
		}
		
	}
	
}
