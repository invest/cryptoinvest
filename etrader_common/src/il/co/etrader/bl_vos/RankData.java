package il.co.etrader.bl_vos;

import java.util.ArrayList;
import java.util.HashMap;

import com.anyoption.common.bl_vos.PopulationEntryBase;

public class RankData implements java.io.Serializable {

	private static final long serialVersionUID = 1L;
		
	private int numberOfRanks; //number of ranks
	private HashMap<Long, StatusData> statusData; 	//Key = status id, Value = number of statuses
	
	public RankData() {
		statusData = new HashMap<Long, StatusData>();
	}

	/**
	 * @return the numberOfRanks
	 */
	public int getNumberOfRanks() {
		return numberOfRanks;
	}

	/**
	 * @param numberOfRanks the numberOfRanks to set
	 */
	public void setNumberOfRanks(int numberOfRanks) {
		this.numberOfRanks = numberOfRanks;
	}
	
	public int getNumberOfRanks1() {
		int sum = 0;
		for (Long iter : statusData.keySet()) {
			 ArrayList<PopulationEntryBase> pebArray = statusData.get(iter).getStatusArray();
			 if (pebArray != null) {
				 sum += pebArray.size();
			 }
		}
		return sum;
	}
		
//	
//	public void increaseStatus(long key) {
//		long value = statusData.get(key);
//		value++;
//		statusData.put(key, value);
//	}
	

	/**
	 * @return the statusData
	 */
	public HashMap<Long, StatusData> getStatusData() {
		return statusData;
	}

	/**
	 * @param statusData the statusData to set
	 */
	public void setStatusData(HashMap<Long, StatusData> statusData) {
		this.statusData = statusData;
	}

	public void increaseRank() {
		numberOfRanks++;
	}
	
}
