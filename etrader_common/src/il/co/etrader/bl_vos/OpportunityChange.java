package il.co.etrader.bl_vos;

import java.util.Date;

public class OpportunityChange implements java.io.Serializable{

	private long id;
	private long opportunityId;
	private long oldUpLevel;
	private long oldDownLevel;
	private Date timeCreated;
	private long writerId;

	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public long getOldDownLevel() {
		return oldDownLevel;
	}
	public void setOldDownLevel(long oldDownLevel) {
		this.oldDownLevel = oldDownLevel;
	}
	public long getOldUpLevel() {
		return oldUpLevel;
	}
	public void setOldUpLevel(long oldUpLevel) {
		this.oldUpLevel = oldUpLevel;
	}
	public long getOpportunityId() {
		return opportunityId;
	}
	public void setOpportunityId(long opportunityId) {
		this.opportunityId = opportunityId;
	}
	public Date getTimeCreated() {
		return timeCreated;
	}
	public void setTimeCreated(Date timeCreated) {
		this.timeCreated = timeCreated;
	}
	public long getWriterId() {
		return writerId;
	}
	public void setWriterId(long writerId) {
		this.writerId = writerId;
	}
	/**
	 * Constructs a <code>String</code> with all attributes
	 * in name = value format.
	 *
	 * @return a <code>String</code> representation
	 * of this object.
	 */
	public String toString()
	{
	    final String TAB = " \n ";

	    String retValue = "";

	    retValue = "OpportunityChange ( "
	        + super.toString() + TAB
	        + "id = " + this.id + TAB
	        + "opportunityId = " + this.opportunityId + TAB
	        + "oldUpLevel = " + this.oldUpLevel + TAB
	        + "oldDownLevel = " + this.oldDownLevel + TAB
	        + "timeCreated = " + this.timeCreated + TAB
	        + "writerId = " + this.writerId + TAB
	        + " )";

	    return retValue;
	}



}

