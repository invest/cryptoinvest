package il.co.etrader.bl_vos;

import il.co.etrader.bl_managers.TiersManagerBase;
import il.co.etrader.bl_managers.TransactionsManagerBase;
import il.co.etrader.util.CommonUtil;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.faces.model.SelectItem;

import com.anyoption.common.beans.AssetIndexMarket;
import com.anyoption.common.beans.Market;
import com.anyoption.common.beans.SkinCurrency;
import com.anyoption.common.beans.SkinLanguage;
import com.anyoption.common.beans.SkinPaymentMethod;
import com.anyoption.common.beans.TreeItem;
import com.anyoption.common.beans.base.Skin;

/**
 * Skin vo class
 * @author Kobi
 *
 */
public class Skins extends Skin implements java.io.Serializable, Cloneable, Comparable<Skins> {
	private static final long serialVersionUID = -1807172844554557361L;
	
	private ArrayList<Long> netreferCombinationId;
	private ArrayList<Long> referpartnerCombinationId;
    private boolean isLoadTickerMarkets;
    private boolean isCountryInLocaleUse;

    private ArrayList<SkinCurrency> skinCurrenciesList;
	private ArrayList<SkinLanguage> skinLanguagesList;
	private HashMap<Long, String> skinMarketsList;       // all markets(id,display_name) of the skin
	//private ArrayList<SelectItem> skinMarketsHomePageList;     // all markets for homePage
	private ArrayList<SkinPaymentMethod> skinPaymentMethodsList; //all payment methods for this skin
	private ArrayList<TreeItem> treeItems;
	private ArrayList<TreeItem> TreeItems0100;
	private ArrayList<SelectItem> optionTypes;
	private ArrayList<Market> assetIndex;
	private ArrayList<AssetIndexMarket> assetIndexMarkets;
	private Map<Long, List<AssetIndexMarket>> groupAssetIndexMarket;	
	private HashMap<Long, Long> tiersLevels;     // save tiersLevel for the skin (for knowing the next tier level)
//    private ArrayList<SelectItem> marketsListSI; // markets with active templates
    private HashMap<Long, String> optionPlusMarketsList; // option plus markets with active templates
    private ArrayList<ArrayList<Market>> bankOptionList;
    private HashMap<Long, String> markets0100;
    private HashMap<Long, String> marketsDynamics;
    private HashMap<Long, SkinCurrency> skinCurrenciesHM;
    private ArrayList<SelectItem> bankWireWithdraw;
	/**
	 * In the service storage for the current market on home page field that is sent to
	 * all LS items
	 */
	private String currentMarketsOnHomePage;
	
	/**
	 * A cached message that contains top markets ids by groups.
	 */
	private String currentMarketsByGroup;

	/**
	 * @return the skinCurrenciesList
	 */
	public ArrayList<SkinCurrency> getSkinCurrenciesList() {
		return skinCurrenciesList;
	}

	/**
	 * @param skinCurrenciesList the skinCurrenciesList to set
	 */
	public void setSkinCurrenciesList(ArrayList<SkinCurrency> skinCurrenciesList) {
		this.skinCurrenciesList = skinCurrenciesList;
	}

	/**
	 * @return the skinLanguagesList
	 */
	public ArrayList<SkinLanguage> getSkinLanguagesList() {
		return skinLanguagesList;
	}

	/**
	 * @param skinLanguagesList the skinLanguagesList to set
	 */
	public void setSkinLanguagesList(ArrayList<SkinLanguage> skinLanguagesList) {
		this.skinLanguagesList = skinLanguagesList;
	}

	/**
	 * @return the skinMarketsList
	 */
	public HashMap<Long, String> getSkinMarketsList() {
		return skinMarketsList;
	}

	/**
	 * @param skinMarketsList the skinMarketsList to set
	 */
	public void setSkinMarketsList(HashMap<Long, String> skinMarketsList) {
		this.skinMarketsList = skinMarketsList;
	}

	/**
	 * @return the skinMarketsHomePageList
	 */
//	public ArrayList<SelectItem> getSkinMarketsHomePageList() {
//		return skinMarketsHomePageList;
//	}

	/**
	 * @param skinMarketsHomePageList the skinMarketsHomePageList to set
	 */
//	public void setSkinMarketsHomePageList(
//			ArrayList<SelectItem> skinMarketsHomePageList) {
//		this.skinMarketsHomePageList = skinMarketsHomePageList;
//	}

	public ArrayList<SelectItem> getOptionTypes() {
		return optionTypes;
	}

	public void setOptionTypes(ArrayList<SelectItem> optionTypes) {
		this.optionTypes = optionTypes;
	}

	public ArrayList<TreeItem> getTreeItems() {
		return treeItems;
	}

		public void setTreeItems(ArrayList<TreeItem> treeItems) {
		this.treeItems = treeItems;
	}

	/**
	 * @return the skinPaymentMethodsList
	 */
	public ArrayList<SkinPaymentMethod> getSkinPaymentMethodsList() {
		return skinPaymentMethodsList;
	}



	/**
	 * @param skinPaymentMethodsList the skinPaymentMethodsList to set
	 */
	public void setSkinPaymentMethodsList(
			ArrayList<SkinPaymentMethod> skinPaymentMethodsList) {
		this.skinPaymentMethodsList = skinPaymentMethodsList;
	}

	/**
	 * This method receives a method of payment id and returns a flag to indicate whether this skin is allowed to use this method of payment
	 * @param methodId - the method Id
	 * @return whether the method of payment is allowed for this skin
	 */
	private boolean methodAllowed(int methodId) {
		boolean res = false;
		for (SkinPaymentMethod paymentMethod : skinPaymentMethodsList) {
			if (paymentMethod.getTransactionTypeId() == methodId) {
				res = true;
				break;
			}
		}
		return res;
	}

	/**
	 * This method returns a flag to indicate if this skin is allowed to use credit card deposit
	 * @return whether this skin is allowed to use credit card deposit
	 */
	public boolean isCreditDepositAllowed() {
		return methodAllowed(TransactionsManagerBase.TRANS_TYPE_CC_DEPOSIT);
	}

	/**
	 * This method returns a flag to indicate if this skin is allowed to use credit card withdraw
	 * @return whether this skin is allowed to use credit card withdraw
	 */
	public boolean isCreditWithdrawAllowed() {
		return methodAllowed(TransactionsManagerBase.TRANS_TYPE_CC_WITHDRAW);
	}

	/**
	 * This method returns a flag to indicate if this skin is allowed to use admin deposit
	 * @return whether this skin is allowed to use admin deposit
	 */
	public boolean isAdminDepositAllowed() {
		return methodAllowed(TransactionsManagerBase.TRANS_TYPE_ADMIN_DEPOSIT);
	}

	/**
	 * This method returns a flag to indicate if this skin is allowed to use admin withdraw
	 * @return whether this skin is allowed to use admin withdraw
	 */
	public boolean isAdminWithdrawAllowed() {
		return methodAllowed(TransactionsManagerBase.TRANS_TYPE_ADMIN_WITHDRAW);
	}

	/**
	 * This method returns a flag to indicate if this skin is allowed to use bonus deposit
	 * @return whether this skin is allowed to use bonus deposit
	 */
	public boolean isBonusDepositAllowed() {
		return methodAllowed(TransactionsManagerBase.TRANS_TYPE_BONUS_DEPOSIT);
	}

	/**
	 * This method returns a flag to indicate if this skin is allowed to use bonus withdraw
	 * @return whether this skin is allowed to use bonus withdraw
	 */
	public boolean isBonusWithdrawAllowed() {
		return methodAllowed(TransactionsManagerBase.TRANS_TYPE_BONUS_WITHDRAW);
	}

	/**
	 * This method returns a flag to indicate if this skin is allowed to use cash deposit
	 * @return whether this skin is allowed to use cash withdraw
	 */
	public boolean isCashDepositAllowed() {
		return methodAllowed(TransactionsManagerBase.TRANS_TYPE_CASH_DEPOSIT);
	}

	/**
	 * This method returns a flag to indicate if this skin is allowed to use cheque deposit
	 * @return whether this skin is allowed to use cheque deposit
	 */
	public boolean isChequeWithdrawAllowed() {
		return methodAllowed(TransactionsManagerBase.TRANS_TYPE_CHEQUE_WITHDRAW);
	}

	/**
	 * This method returns a flag to indicate if this skin is allowed to use wire deposit
	 * @return whether this skin is allowed to use wire depoist
	 */
	public boolean isWireDepositAllowed() {
		return methodAllowed(TransactionsManagerBase.TRANS_TYPE_WIRE_DEPOSIT);
	}

	/**
	 * This method returns a flag to indicate if this skin is allowed to use phone deposit (same as cc deposit)
	 * @return whether this skin is allowed to use phone withdraw
	 */
	public boolean isPhoneDepositAllowed() {
		return methodAllowed(TransactionsManagerBase.TRANS_TYPE_CC_DEPOSIT);
	}

	/**
	 * This method returns a flag to indicate if this skin is allowed to use reverse withdraw (allways true)
	 * @return whether this skin is allowed to use reverse withdraw
	 */
	public boolean isReverseWithdrawAllowed() {
		return true;
	}

	/**
	 * This method returns a flag to indicate if this skin is allowed to use chargeback withdraw (allways true)
	 * @return whether this skin is allowed to use chargeback withdraw
	 */
	public boolean isChargebackWithdrawAllowed() {
		return true;
	}

	/**
	 * This method returns a flag to indicate if this skin is allowed to use bank wire withdraw
	 * @return whether this skin is allowed to use bank wire withdraw
	 */
	public boolean isBankWithdrawAllowed() {
		return methodAllowed(TransactionsManagerBase.TRANS_TYPE_BANK_WIRE_WITHDRAW);
	}

	public ArrayList<Market> getAssetIndex() {
		return assetIndex;
	}

	public void setAssetIndex(ArrayList<Market> assetIndex) {
		this.assetIndex = assetIndex;
	}
	
	public ArrayList<AssetIndexMarket> getAssetIndexMarkets() {
		return assetIndexMarkets;
	}

	public void setAssetIndexMarkets(ArrayList<AssetIndexMarket> assetIndexMarkets) {
		this.assetIndexMarkets = assetIndexMarkets;
	}

	/**
	 * @return the defaultCombinationId
	 */
	public ArrayList<Long> getNetreferCombinationId() {
		return netreferCombinationId;
	}

	/**
	 * @param defaultCombinationId the defaultCombinationId to set
	 */
	public void setNetreferCombinationId(ArrayList<Long> netreferCombinationId) {
		this.netreferCombinationId = netreferCombinationId;
	}
	
	public ArrayList<Long> getReferpartnerCombinationId() {
		return referpartnerCombinationId;
	}

	/**
	 * @param defaultCombinationId the defaultCombinationId to set
	 */
	public void setReferpartnerCombinationId(ArrayList<Long> referpartnerCombinationId) {
		this.referpartnerCombinationId = referpartnerCombinationId;
	}

	/**
	 * @return the tiersLevels
	 */
	public HashMap<Long, Long> getTiersLevels() {
		return tiersLevels;
	}

	/**
	 * @param tiersLevels the tiersLevels to set
	 */
	public void setTiersLevels(HashMap<Long, Long> tiersLevels) {
		this.tiersLevels = tiersLevels;
	}

	/**
	 * Get blue level tier
	 * @param skinId
	 * @return
	 */
	public long getBlueTier() {
		return getTierIdByLevel(TiersManagerBase.TIER_LEVEL_BLUE);
	}

	/**
	 * Get next level tier by tierId
	 * @param tierId
	 * @return
	 */
	public long getNextLvlTierId(long tierId) {
		long nextLevl = tiersLevels.get(new Long(tierId)) + 1;
		return getTierIdByLevel(nextLevl);
	}

	/**
	 * Get tierId by level
	 * @param level
	 * @return
	 */
	public long getTierIdByLevel(long level) {
		long tierId = 0;
		for (Iterator<Long> iter = tiersLevels.keySet().iterator() ; iter.hasNext(); ) {
			long id = iter.next();
			if (tiersLevels.get(new Long(id)) == level ) {
				tierId = id;
				break;
			}
		}
		return tierId;
	}

	/**
	 * Is tierLevel is the maxLevel in this skin
	 * @param tierId
	 * @return
	 */
	public boolean isHigherLvlTier(long tierId) {
		long level = tiersLevels.get(new Long(tierId));
		if (level == tiersLevels.size()) {
			return true;
		}
		return false;
	}

	/**
	 * Override Clone function for Skins Object
	 * Doing deep clone just to skinPaymentMethodsList.
	 * All other list are null.
	 * @return cloned Skins instance
	 */
	public Object clone() throws CloneNotSupportedException {
		Skins clonedSkin = (Skins)super.clone();

		@SuppressWarnings("unchecked")
		ArrayList<SkinPaymentMethod> spmCloned = (ArrayList<SkinPaymentMethod>) skinPaymentMethodsList.clone();
		// deep clone for skinPaymentMethodsList
		for ( int i = 0 ; i < spmCloned.size() ; i++ ) {
			spmCloned.set(i, (SkinPaymentMethod)(spmCloned.get(i)).clone());
		}
		clonedSkin.setSkinPaymentMethodsList(spmCloned);

		// set other attributes to null ( we use this just in userBase and we dont want to save all lists! )
		clonedSkin.setSkinCurrenciesList(null);
		clonedSkin.setSkinLanguagesList(null);
		clonedSkin.setSkinMarketsList(null);
		//clonedSkin.setSkinMarketsHomePageList(null);
		clonedSkin.setTreeItems(null);
		clonedSkin.setTreeItems0100(null);
		clonedSkin.setOptionTypes(null);
		clonedSkin.setAssetIndex(null);
		clonedSkin.setAssetIndexMarkets(null);
		clonedSkin.setTiersLevels(null);

		return clonedSkin;
	}

	/**
	 * CompareTo implementation by skin name
	 */
	public int compareTo(Skins o) {
		return CommonUtil.getMessage(this.displayName, null).compareTo(CommonUtil.getMessage(o.displayName, null));
	}

    public String getCurrentMarketsOnHomePage() {
        return currentMarketsOnHomePage;
    }

    public void setCurrentMarketsOnHomePage(String currentMarketsOnHomePage) {
        this.currentMarketsOnHomePage = currentMarketsOnHomePage;
    }

    /**
	 * @return the currentMarketsByGroup
	 */
	public String getCurrentMarketsByGroup() {
		return currentMarketsByGroup;
	}

	/**
	 * @param currentMarketsByGroup the currentMarketsByGroup to set
	 */
	public void setCurrentMarketsByGroup(String currentMarketsByGroup) {
		this.currentMarketsByGroup = currentMarketsByGroup;
	}

	/**
     * @return the marketsListSi
     */
//    public ArrayList<SelectItem> getMarketsListSI() {
//        return marketsListSI;
//    }

    /**
     * @param marketsListSi the marketsListSi to set
     */
//    public void setMarketsListSI(ArrayList<SelectItem> marketsListSI) {
//        this.marketsListSI = marketsListSI;
//    }

    /**
     * @return the optionPlusMarketsList
     */
    public HashMap<Long, String> getOptionPlusMarketsList() {
        return optionPlusMarketsList;
    }

    /**
     * @param optionPlusMarketsList the optionPlusMarketsList to set
     */
    public void setOptionPlusMarketsList(
            HashMap<Long, String> optionPlusMarketsList) {
        this.optionPlusMarketsList = optionPlusMarketsList;
    }

    /**
     * @return the isLoadTickerMarkets
     */
    public boolean isLoadTickerMarkets() {
        return isLoadTickerMarkets;
    }

    /**
     * @param isLoadTickerMarkets the isLoadTickerMarkets to set
     */
    public void setLoadTickerMarkets(boolean isLoadTickerMarkets) {
        this.isLoadTickerMarkets = isLoadTickerMarkets;
    }

	public boolean isCountryInLocaleUse() {
		return isCountryInLocaleUse;
	}

	public void setCountryInLocaleUse(boolean isCountryInLocaleUse) {
		this.isCountryInLocaleUse = isCountryInLocaleUse;
	}

	public ArrayList<ArrayList<Market>> getBankOptionList() {
		return bankOptionList;
	}

	public void setBankOptionList(ArrayList<ArrayList<Market>> bankOptionList) {
		this.bankOptionList = bankOptionList;
	}

	public ArrayList<TreeItem> getTreeItems0100() {
		return TreeItems0100;
	}

	public void setTreeItems0100(ArrayList<TreeItem> treeItems0100) {
		TreeItems0100 = treeItems0100;
	}

	/**
	 * @return the markets0100
	 */
	public HashMap<Long, String> getMarkets0100() {
		return markets0100;
	}

	/**
	 * @param markets0100 the markets0100 to set
	 */
	public void setMarkets0100(HashMap<Long, String> markets0100) {
		this.markets0100 = markets0100;
	}

	/**
	 * @return the skinCurrenciesHM
	 */
	public HashMap<Long, SkinCurrency> getSkinCurrenciesHM() {
		return skinCurrenciesHM;
	}

	/**
	 * @param skinCurrenciesHM the skinCurrenciesHM to set
	 */
	public void setSkinCurrenciesHM(HashMap<Long, SkinCurrency> skinCurrenciesHM) {
		this.skinCurrenciesHM = skinCurrenciesHM;
	}

	/**
	 * @return the bankWireWithdraw
	 */
	public ArrayList<SelectItem> getBankWireWithdraw() {
		return bankWireWithdraw;
	}

	/**
	 * @param bankWireWithdraw the bankWireWithdraw to set
	 */
	public void setBankWireWithdraw(ArrayList<SelectItem> bankWireWithdraw) {
		this.bankWireWithdraw = bankWireWithdraw;
	}

	public Map<Long, List<AssetIndexMarket>> getGroupAssetIndexMarket() {
		return groupAssetIndexMarket;
	}

	public void setGroupAssetIndexMarket(Map<Long, List<AssetIndexMarket>> groupAssetIndexMarket) {
		this.groupAssetIndexMarket = groupAssetIndexMarket;
	}

	public HashMap<Long, String> getMarketsDynamics() {
		return marketsDynamics;
	}

	public void setMarketsDynamics(HashMap<Long, String> marketsDynamics) {
		this.marketsDynamics = marketsDynamics;
	}	
}