package il.co.etrader.bl_vos;

import il.co.etrader.util.CommonUtil;

import java.math.BigDecimal;

/**
 * Service config VO.
 *
 * @author Tony
 */
public class ServiceConfig implements java.io.Serializable {
    private double ta25Parameter;
    private BigDecimal ftseParameter;
    private BigDecimal cacParameter;
    private BigDecimal sp500Parameter;
    private BigDecimal klseParameter;
	private BigDecimal d4BanksParameter;
	private BigDecimal banksBursaParameter;
	private BigDecimal d4SpreadParameter;


	public String getTa25ParameterTxt() {
		return CommonUtil.displayDecimalForInput(ta25Parameter);
	}
	public String getFtseTxt() {
		if (ftseParameter==null)
			return "";
		return CommonUtil.displayDecimalForInput(ftseParameter.doubleValue());
	}
	public String getCacTxt() {
		if (cacParameter==null)
			return "";
		return CommonUtil.displayDecimalForInput(cacParameter.doubleValue());
	}
	public String getSp500Txt() {
		if (sp500Parameter==null)
			return "";
		return CommonUtil.displayDecimalForInput(sp500Parameter.doubleValue());
	}

	public String getKlseTxt() {
		if (klseParameter==null) {
			return "";
		}
		return CommonUtil.displayDecimalForInput(klseParameter.doubleValue());
	}

	public String getD4BanksTxt() {
		if (d4BanksParameter == null) {
			return "";
		}
		return CommonUtil.displayDecimalForInput(d4BanksParameter.doubleValue());
	}

	public String getBanksBursaParameterTxt() {
		if (banksBursaParameter == null) {
			return "";
		}
		return CommonUtil.displayDecimalForInput(banksBursaParameter.doubleValue());
	}

	public String getD4SpreadParameterTxt() {
		if (d4SpreadParameter == null) {
			return "";
		}
		return CommonUtil.displayDecimalForInput(d4SpreadParameter.doubleValue());
	}

	public void setTa25ParameterTxt(String ta25ParameterTxt) {
		this.ta25Parameter = new Double(ta25ParameterTxt).doubleValue();
	}

    public double getTa25Parameter() {
        return ta25Parameter;
    }

    public void setTa25Parameter(double ta25Parameter) {
        this.ta25Parameter = ta25Parameter;
    }

    public BigDecimal getCacParameter() {
        return cacParameter;
    }

    public void setCacParameter(BigDecimal cacParameter) {
        this.cacParameter = cacParameter;
    }

    public BigDecimal getFtseParameter() {
        return ftseParameter;
    }

    public void setFtseParameter(BigDecimal ftseParameter) {
        this.ftseParameter = ftseParameter;
    }

    /**
	 * Constructs a <code>String</code> with all attributes
	 * in name = value format.
	 *
	 * @return a <code>String</code> representation
	 * of this object.
	 */
	public String toString() {
	    final String TAB = " \n ";

	    String retValue = "";

	    retValue = "ServiceConfig ( "
	        + super.toString() + TAB
	        + "ta25Parameter = " + this.ta25Parameter + TAB
            + "ftseParameter = " + this.ftseParameter + TAB
            + "cacParameter = " + this.cacParameter + TAB
            + "sp500Parameter = " + this.sp500Parameter + TAB
            + "klseParameter = " + this.klseParameter + TAB
            + "banksBursaParameter = " + this.banksBursaParameter + TAB
            + "d4BanksParameter = " + this.d4BanksParameter + TAB
            + "d4SpreadParameter = " + this.d4SpreadParameter + TAB

	        + " )";
	    return retValue;
	}
	public BigDecimal getSp500Parameter() {
		return sp500Parameter;
	}
	public void setSp500Parameter(BigDecimal sp500Parameter) {
		this.sp500Parameter = sp500Parameter;
	}

	/**
	 * @return the klseParameter
	 */
	public BigDecimal getKlseParameter() {
		return klseParameter;
	}

	/**
	 * @param klseParameter the klseParameter to set
	 */
	public void setKlseParameter(BigDecimal klseParameter) {
		this.klseParameter = klseParameter;
	}
	/**
	 * @return the banksBursaParameter
	 */
	public BigDecimal getBanksBursaParameter() {
		return banksBursaParameter;
	}
	/**
	 * @param banksBursaParameter the banksBursaParameter to set
	 */
	public void setBanksBursaParameter(BigDecimal banksBursaParameter) {
		this.banksBursaParameter = banksBursaParameter;
	}
	/**
	 * @return the d4BanksParameter
	 */
	public BigDecimal getD4BanksParameter() {
		return d4BanksParameter;
	}
	/**
	 * @param banksParameter the d4BanksParameter to set
	 */
	public void setD4BanksParameter(BigDecimal banksParameter) {
		d4BanksParameter = banksParameter;
	}
	/**
	 * @return the d4SpreadParameter
	 */
	public BigDecimal getD4SpreadParameter() {
		return d4SpreadParameter;
	}
	/**
	 * @param spreadParameter the d4SpreadParameter to set
	 */
	public void setD4SpreadParameter(BigDecimal spreadParameter) {
		d4SpreadParameter = spreadParameter;
	}


}