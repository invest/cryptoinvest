//package il.co.etrader.bl_vos;
//
//import il.co.etrader.bl_managers.CurrenciesManagerBase;
//
//import java.sql.SQLException;
//import java.util.HashMap;
//import java.util.Map;
//
//public class Currency implements java.io.Serializable {
//	/**
//	 * 
//	 */
//	private static final long serialVersionUID = 1L;
//	
//	private long id;
//	private String symbol;
//	private int isLeftSymbol;
//	private String code;
//	private String nameKey;
//	private String displayName;
//	private int decimalPointDigits;
//	
//	private static HashMap<Long, Currency> currencies;
//
//	public Currency() {
//		/* Empty constructor */
//	}
//
//	public Currency(Currency c) {
//		this.id = c.id;
//		this.symbol=c.symbol;
//		this.isLeftSymbol=c.isLeftSymbol;
//		this.code=c.code;
//		this.nameKey=c.nameKey;
//		this.displayName=c.displayName;
//		this.decimalPointDigits = c.decimalPointDigits;
//	}
//
//	public String getCode() {
//		return code;
//	}
//	
//	public void setCode(String code) {
//		this.code = code;
//	}
//	
//	public long getId() {
//		return id;
//	}
//	
//	public void setId(long id) {
//		this.id = id;
//	}
//
//	public int getIsLeftSymbol() {
//		return isLeftSymbol;
//	}
//	
//	public void setIsLeftSymbol(int isLeftSymbol) {
//		this.isLeftSymbol = isLeftSymbol;
//	}
//
//	public boolean getIsLeftSymbolBool() {
//		return ( isLeftSymbol == 1 ? true : false );
//	}
//
//	public String getNameKey() {
//		return nameKey;
//	}
//	
//	public void setNameKey(String nameKey) {
//		this.nameKey = nameKey;
//	}
//	
//	public String getSymbol() {
//		return symbol;
//	}
//	
//	public void setSymbol(String symbol) {
//		this.symbol = symbol;
//	}
//    
//    public String getDisplayName() {
//        return displayName;
//    }
//    
//    public void setDisplayName(String displayName) {
//        this.displayName = displayName;
//    }
//	
//    /**
//     * @return the decimalPointDigits
//     */
//    public int getDecimalPointDigits() {
//    	return decimalPointDigits;
//    }
//    
//    /**
//     * @param decimalPointDigits the decimalPointDigits to set
//     */
//    public void setDecimalPointDigits(int decimalPointDigits) {
//    	this.decimalPointDigits = decimalPointDigits;
//    }
//    
//	public String toString() {
//	    String ls = System.getProperty("line.separator");
//	    return ls + "Currency: " + ls
//	        + super.toString() + ls
//	        + "id: " + id + ls
//	        + "symbol: " + symbol + ls
//	        + "isLeftSymbo: " + isLeftSymbol + ls
//	        + "code: " + code + ls
//	        + "nameKey: " + nameKey + ls
//	        + "decimalPointDigits: " + decimalPointDigits + ls;
//	}
//	
//	private static void loadCurrenciesIfEmpty() throws SQLException {
//		 if (null == currencies) {
//			 currencies = CurrenciesManagerBase.getAllCurrencies();
//	    }
//	}
//	
//	public static Currency getCurrencyByCode(String currencyCode) throws SQLException {
//		loadCurrenciesIfEmpty();
//		for (Currency currency : currencies.values()) {
//			if (currency.getCode().equalsIgnoreCase(currencyCode)) {
//				return currency;
//			}
//		}
//		return null;
//	}
//	
//	public static Currency getCurrency(long currencyId) throws SQLException {
//		loadCurrenciesIfEmpty();
//	    return currencies.get(currencyId);
//	}
//	
//	public static Map<Long, Currency> getAllCurrencies() throws SQLException {
//		loadCurrenciesIfEmpty();
//		return currencies;
//	}
//}