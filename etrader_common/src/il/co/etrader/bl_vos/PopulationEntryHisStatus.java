package il.co.etrader.bl_vos;

import java.io.Serializable;

/**
 * Population Entry History Status VO class
 * @author Eliran
 *
 */
public class PopulationEntryHisStatus implements Serializable {

	private static final long serialVersionUID = 1063897471618041902L;

	private int id;
	private boolean isRemoveFromPopulation;
	private boolean isCancelAssign;
	private int entryTypeChange;


	/**
	 * toString implementation
	 */
	public String toString() {
	   String ls = System.getProperty("line.separator");
	   return "population Entry History Status (" + ls +
		  "id: " + id + ls +
		  "isRemoveFromPopulation: " + this.isRemoveFromPopulation + ls +
		  "entryTypeChange: " + this.entryTypeChange + ls;
	}


	/**
	 * @return the entryTypeChange
	 */
	public int getEntryTypeChange() {
		return entryTypeChange;
	}


	/**
	 * @param entryTypeChange the entryTypeChange to set
	 */
	public void setEntryTypeChange(int entryTypeChange) {
		this.entryTypeChange = entryTypeChange;
	}

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}


	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}


	/**
	 * @return the isRemoveFromPopulation
	 */
	public boolean isRemoveFromPopulation() {
		return isRemoveFromPopulation;
	}


	/**
	 * @param isRemoveFromPopulation the isRemoveFromPopulation to set
	 */
	public void setRemoveFromPopulation(boolean isRemoveFromPopulation) {
		this.isRemoveFromPopulation = isRemoveFromPopulation;
	}


	/**
	 * @return the isCancelAssign
	 */
	public boolean isCancelAssign() {
		return isCancelAssign;
	}


	/**
	 * @param isCancelAssign the isCancelAssign to set
	 */
	public void setCancelAssign(boolean isCancelAssign) {
		this.isCancelAssign = isCancelAssign;
	}
}
