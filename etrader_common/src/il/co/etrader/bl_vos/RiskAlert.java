package il.co.etrader.bl_vos;

import il.co.etrader.bl_managers.ApplicationDataBase;
import il.co.etrader.bl_managers.InvestmentsManagerBase;
import il.co.etrader.bl_managers.TransactionsManagerBase;
import il.co.etrader.util.CommonUtil;

import java.sql.SQLException;
import java.util.Date;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.Investment;
import com.anyoption.common.beans.Transaction;


public class RiskAlert implements java.io.Serializable {
	private static final Logger log = Logger.getLogger(RiskAlert.class);

	private static final long serialVersionUID = 1L;

	private long id;
	private long typeId;
	private long writerId;
	private Date timeCreated;
	private Date timeSettled;
	private Transaction transaction;
	private Investment investment;
	private long skinId;
	private long investmentId ;
	private long transactionId;
	private UserBase user;
	private long riskAlertClassType;
	private long riskAlertType;
	private String riskAlertClassTypeDecription;
	private String riskAlertTypeDecription;
	private int succeededTrx;
	private int campaign_id;
    private String businessCaseName;
    private long balanceAfter1TouchFirstInvestment;
    private String ccHolderName;
    private String accountHolderNameVsCcHolderNameTxt;
    private String firstUserWhoUsedSameCC;
    private String moreUsersWhoUsedSameCC;
    private String userCountryVsCcCountryTxt;
    private String userCountryVsIpCountryTxt;
    private int wasReviewed;
    private long writerReviewedId;
    private Date timeReviewed;
    private String ccCountries;
    private String last4CCDep;
    private Date lastDateCCDep;
    private long ftdAmount;
    private boolean isFTDCC;
    private long currencyId;
    
	public RiskAlert() {
		user = new UserBase();
		succeededTrx = 0;
		moreUsersWhoUsedSameCC = "";
	}

	public RiskAlert(long typeId, long writerId, long transactionId, long investmentId) {
		this.typeId = typeId;
		this.writerId = writerId;
		this.transactionId = transactionId;
		this.investmentId = investmentId;
	}

	/**
	 * Get Transaction details
	 * @return
	 * @throws SQLException
	 */
	public Transaction getTransaction() throws SQLException{
		if (transaction == null) {
			log.debug("get transaction from db");
			transaction = TransactionsManagerBase.getTransaction(transactionId);
		}
		return transaction;
	}

	/**
	 * Get Investment details
	 * @return
	 * @throws SQLException
	 */
	public Investment getInvestment() throws SQLException{
		if (investment == null) {
			log.debug("get investment from db");
			investment = InvestmentsManagerBase.getInvestmentDetails(investmentId);
		}
		return investment;
	}


	/**
	 * @return the id
	 */
	public long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(long id) {
		this.id = id;
	}

	/**
	 * @return the timeCreated
	 */
	public Date getTimeCreated() {
		return timeCreated;
	}

	/**
	 * @param timeCreated the timeCreated to set
	 */
	public void setTimeCreated(Date timeCreated) {
		this.timeCreated = timeCreated;
	}

	/**
	 * @return the typeId
	 */
	public long getTypeId() {
		return typeId;
	}

	/**
	 * @param typeId the typeId to set
	 */
	public void setTypeId(long typeId) {
		this.typeId = typeId;
	}

	/**
	 * @return the writerId
	 */
	public long getWriterId() {
		return writerId;
	}

	/**
	 * @param writerId the writerId to set
	 */
	public void setWriterId(long writerId) {
		this.writerId = writerId;
	}


	/**
	 * @param transaction the transaction to set
	 */
	public void setTransaction(Transaction transaction) {
		this.transaction = transaction;
	}


	/**
	 * @return the riskAlertClassType
	 */
	public long getRiskAlertClassType() {
		return riskAlertClassType;
	}

	/**
	 * @param riskAlertClassType the riskAlertClassType to set
	 */
	public void setRiskAlertClassType(long riskAlertClassType) {
		this.riskAlertClassType = riskAlertClassType;
	}

	/**
	 * @return the riskAlertType
	 */
	public long getRiskAlertType() {
		return riskAlertType;
	}

	/**
	 * @param riskAlertType the riskAlertType to set
	 */
	public void setRiskAlertType(long riskAlertType) {
		this.riskAlertType = riskAlertType;
	}

	/**
	 * @return the user
	 */
	public UserBase getUser() {
		return user;
	}

	/**
	 * @param user the user to set
	 */
	public void setUser(UserBase user) {
		this.user = user;
	}


	/**
	 * @return the riskAlertClassTypeDecription
	 */
	public String getRiskAlertClassTypeDecription() {
		return CommonUtil.getMessage(riskAlertClassTypeDecription,null);
	}

	/**
	 * @param riskAlertClassTypeDecription the riskAlertClassTypeDecription to set
	 */
	public void setRiskAlertClassTypeDecription(String riskAlertClassTypeDecription) {
		this.riskAlertClassTypeDecription = riskAlertClassTypeDecription;
	}

	/**
	 * @return the riskAlertTypeDecription
	 */
	public String getRiskAlertTypeDecription() {
		return CommonUtil.getMessage(riskAlertTypeDecription,null);
	}

	/**
	 * @param riskAlertTypeDecription the riskAlertTypeDecription to set
	 */
	public void setRiskAlertTypeDecription(String riskAlertTypeDecription) {
		this.riskAlertTypeDecription = riskAlertTypeDecription;
	}

	/**
	 * @return the investmentId
	 */
	public long getInvestmentId() {
		return investmentId;
	}

	/**
	 * @param investmentId the investmentId to set
	 */
	public void setInvestmentId(long investmentId) {
		this.investmentId = investmentId;
	}

	/**
	 * @return the skinId
	 */
	public long getSkinId() {
		return skinId;
	}

	/**
	 * @param skinId the skinId to set
	 */
	public void setSkinId(long skinId) {
		this.skinId = skinId;
	}

	/**
	 * Return user skin name
	 * @return
	 */
	public String getUserSkinName(){
		return CommonUtil.getMessage(ApplicationDataBase.getSkinById(user.getSkinId()).getDisplayName(),null);
	}

	/**
	 * @return the succeededTrx
	 */
	public int getSucceededTrx() {
		return succeededTrx;
	}

	/**
	 * @param succeededTrx the succeededTrx to set
	 */
	public void setSucceededTrx(int succeededTrx) {
		this.succeededTrx = succeededTrx;
	}

	/**
	 * Get time created with hours format
	 * @return
	 */
	public String getTimeCreatedTxt() {
			return CommonUtil.getDateTimeFormatDisplay(timeCreated,CommonUtil.getDefualtUtcOffset());
	}

	/**
	 * Get time settled with hours format
	 * @return
	 */
	public String getTimeSettledTxt() {
			return CommonUtil.getDateTimeFormatDisplay(timeSettled,CommonUtil.getDefualtUtcOffset());
	}

	/**
	 * @return the transactionId
	 */
	public long getTransactionId() {
		return transactionId;
	}

	/**
	 * @param transactionId the transactionId to set
	 */
	public void setTransactionId(long transactionId) {
		this.transactionId = transactionId;
	}

	/**
	 * @param investment the investment to set
	 */
	public void setInvestment(Investment investment) {
		this.investment = investment;
	}

	/**
	 * @return the campaign_id
	 */
	public int getCampaign_id() {
		return campaign_id;
	}

	/**
	 * @param campaign_id the campaign_id to set
	 */
	public void setCampaign_id(int campaign_id) {
		this.campaign_id = campaign_id;
	}


	/**
	 * @return the businessCaseName
	 */
	public String getBusinessCaseName() {
		return businessCaseName;
	}

	/**
	 * @param businessCaseName the businessCaseName to set
	 */
	public void setBusinessCaseName(String businessCaseName) {
		this.businessCaseName = businessCaseName;
	}

    /**
     * Return business case name
     * @return
     */
    public String getBusinessCaseNameTxt() {
        return CommonUtil.getMessage(businessCaseName, null);
    }

	/**
	 * @return the timeSettled
	 */
	public Date getTimeSettled() {
		return timeSettled;
	}

	/**
	 * @param timeSettled the timeSettled to set
	 */
	public void setTimeSettled(Date timeSettled) {
		this.timeSettled = timeSettled;
	}

	/**
	 * @return the balanceAfter1TouchFirstInvestment
	 */
	public long getBalanceAfter1TouchFirstInvestment() {
		return balanceAfter1TouchFirstInvestment;
	}

	/**
	 * @param balanceAfter1TouchFirstInvestment the balanceAfter1TouchFirstInvestment to set
	 */
	public void setBalanceAfter1TouchFirstInvestment(
			long balanceAfter1TouchFirstInvestment) {
		this.balanceAfter1TouchFirstInvestment = balanceAfter1TouchFirstInvestment;
	}

	/**
	 * @return the ccHolderName
	 */
	public String getCcHolderName() {
		return ccHolderName;
	}

	/**
	 * @param ccHolderName the ccHolderName to set
	 */
	public void setCcHolderName(String ccHolderName) {
		this.ccHolderName = ccHolderName;
	}

	/**
	 * @return the accountHolderNameVsCcHolderNameTxt
	 */
	public String getAccountHolderNameVsCcHolderNameTxt() {
		return accountHolderNameVsCcHolderNameTxt;
	}

	/**
	 * @param accountHolderNameVsCcHolderNameTxt the accountHolderNameVsCcHolderNameTxt to set
	 */
	public void setAccountHolderNameVsCcHolderNameTxt(
			String accountHolderNameVsCcHolderNameTxt) {
		this.accountHolderNameVsCcHolderNameTxt = accountHolderNameVsCcHolderNameTxt;
	}
	
	/**
	 * @return the firstUserWhoUsedSameCC
	 */
	public String getFirstUserWhoUsedSameCC() {
		return firstUserWhoUsedSameCC;
	}

	/**
	 * @param firstUserWhoUsedSameCC the firstUserWhoUsedSameCC to set
	 */
	public void setFirstUserWhoUsedSameCC(String firstUserWhoUsedSameCC) {
		this.firstUserWhoUsedSameCC = firstUserWhoUsedSameCC;
	}

	/**
	 * @return the usersWhoUsedSameCC
	 */
	public String getMoreUsersWhoUsedSameCC() {
		return moreUsersWhoUsedSameCC;
	}

	/**
	 * @param usersWhoUsedSameCC the usersWhoUsedSameCC to set
	 */
	public void setMoreUsersWhoUsedSameCC(String moreUsersWhoUsedSameCC) {
		this.moreUsersWhoUsedSameCC = moreUsersWhoUsedSameCC;
	}
	
	public boolean isExistMoreUserWhoUsedSameCC() {
		return moreUsersWhoUsedSameCC.equals("");
	}

	/**
	 * @return the wasReviewed
	 */
	public int getWasReviewed() {
		return wasReviewed;
	}

	/**
	 * @param wasReviewed the wasReviewed to set
	 */
	public void setWasReviewed(int wasReviewed) {
		this.wasReviewed = wasReviewed;
	}

	/**
	 * @return the writerReviewed
	 */
	public long getWriterReviewedId() {
		return writerReviewedId;
	}

	/**
	 * @param writerReviewed the writerReviewed to set
	 */
	public void setWriterReviewedId(long writerReviewedId) {
		this.writerReviewedId = writerReviewedId;
	}
	
	/**
	 * @return the timeReviewed
	 */
	public Date getTimeReviewed() {
		return timeReviewed;
	}

	/**
	 * @param timeReviewed the timeReviewed to set
	 */
	public void setTimeReviewed(Date timeReviewed) {
		this.timeReviewed = timeReviewed;
	}

	/**
	 * Get time reviewed with hours format
	 * @return
	 */
	public String getTimeReviewedTxt() {
			return CommonUtil.getDateTimeFormatDisplay(timeReviewed,CommonUtil.getDefualtUtcOffset());
	}
	
	public String getWriterReviewedTxt() throws SQLException {
		return CommonUtil.getWriterName(writerReviewedId);
	}

	/**
	 * @return the userCountryVsCcCountryTxt
	 */
	public String getUserCountryVsCcCountryTxt() {
		return userCountryVsCcCountryTxt;
	}

	/**
	 * @param userCountryVsCcCountryTxt the userCountryVsCcCountryTxt to set
	 */
	public void setUserCountryVsCcCountryTxt(String userCountryVsCcCountryTxt) {
		this.userCountryVsCcCountryTxt = userCountryVsCcCountryTxt;
	}

	/**
	 * @return the userCountryVsIpCountryTxt
	 */
	public String getUserCountryVsIpCountryTxt() {
		return userCountryVsIpCountryTxt;
	}

	/**
	 * @param userCountryVsIpCountryTxt the userCountryVsIpCountryTxt to set
	 */
	public void setUserCountryVsIpCountryTxt(String userCountryVsIpCountryTxt) {
		this.userCountryVsIpCountryTxt = userCountryVsIpCountryTxt;
	}

	public String toString() {
        String ls = System.getProperty("line.separator");
        return ls + "RiskAlert" + ls
            + super.toString() + ls
            + "id: " + id + ls
            + "typeId: " + typeId + ls
            + "transactionId: " + transactionId + ls
            + "investmentId: " + investmentId + ls
            + "userId: " + user.getId() + ls
            + "writerId: " + writerId + ls;
    }

	public String getCcCountries() {
		return ccCountries;
	}

	public void setCcCountries(String ccCountries) {
		this.ccCountries = ccCountries;
	}

	public String getLast4CCDep() {
		return last4CCDep;
	}

	public void setLast4CCDep(String last4ccDep) {
		last4CCDep = last4ccDep;
	}

	public Date getLastDateCCDep() {
		return lastDateCCDep;
	}

	public void setLastDateCCDep(Date lastDateCCDep) {
		this.lastDateCCDep = lastDateCCDep;
	}

	public long getFtdAmount() {
		return ftdAmount;
	}

	public void setFtdAmount(long ftdAmount) {
		this.ftdAmount = ftdAmount;
	}

	public boolean isFTDCC() {
		return isFTDCC;
	}

	public void setFTDCC(boolean isFTDCC) {
		this.isFTDCC = isFTDCC;
	}
	
	public long getCurrencyId() {
		return currencyId;
	}

	public void setCurrencyId(long currencyId) {
		this.currencyId = currencyId;
	}

	public String getUserFTDAmount() {
		return CommonUtil.displayAmount(ftdAmount, currencyId);
	}
}
