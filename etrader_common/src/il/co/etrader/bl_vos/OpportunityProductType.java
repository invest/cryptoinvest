package il.co.etrader.bl_vos;


/**
 * Opportunity type VO.
 *
 * @author Tony
 */
public class OpportunityProductType implements java.io.Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public static long BINARY_TYPE_ID = 1L;
	public static long ONE_TOUCH_TYPE_ID = 2L;
	public static long OPTION_PLUS_TYPE_ID = 3L;
	public static long BINARY_ZERO_HUNDRED_TYPE_ID = 4L;
	
    private long id;
    private String name;
    
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}

}