package il.co.etrader.bl_vos;

import il.co.etrader.util.CommonUtil;
import il.co.etrader.util.ConstantsBase;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class OptionExpiration implements java.io.Serializable {

    private static final long serialVersionUID = 3031445129115409981L;

    private long id;

	private long marketId;
	private Date expiryDate;
	private Date timeCreated;
	private long writerId;
	private boolean active;
	private String writerName;
	private String marketName;
	private boolean nextAgreementOnSameDay;
    private Date expiryDateEnd;
    private String year;
    private String letter;

    public OptionExpiration() {
        year = String.valueOf(Calendar.getInstance().get(Calendar.YEAR));
    }


	public String getTimeCreatedTxt() {
		return CommonUtil.getDateTimeFormatDisplay(timeCreated, CommonUtil.getUtcOffset(ConstantsBase.EMPTY_STRING));
	}

	public String getExpiryDateTxt() {
		return CommonUtil.getDateFormat(expiryDate);
	}

    public String getExpiryDateEndTxt() {
        return CommonUtil.getDateFormat(expiryDateEnd);
    }

	public String getExpiryDateMinus1Txt() {

		if (!nextAgreementOnSameDay) {

			GregorianCalendar gc=new GregorianCalendar();
			gc.setTime(expiryDate);
			gc.add(GregorianCalendar.DAY_OF_MONTH, -1);
			return CommonUtil.getDateFormat(gc.getTime());

		}
		return CommonUtil.getDateFormat(expiryDate);
	}



	public String getWriterName() {
		return writerName;
	}

	public void setWriterName(String writerName) {
		this.writerName = writerName;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public Date getExpiryDate() {
		return expiryDate;
	}

	public void setExpiryDate(Date expiryDate) {
		this.expiryDate = expiryDate;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public long getMarketId() {
		return marketId;
	}

	public void setMarketId(long marketId) {
		this.marketId = marketId;
	}

	public Date getTimeCreated() {
		return timeCreated;
	}

	public void setTimeCreated(Date timeCreated) {
		this.timeCreated = timeCreated;
	}

	public long getWriterId() {
		return writerId;
	}

	public void setWriterId(long writerId) {
		this.writerId = writerId;
	}

	/**
	 * Constructs a <code>String</code> with all attributes in name = value
	 * format.
	 *
	 * @return a <code>String</code> representation of this object.
	 */
	public String toString() {
		final String TAB = " \n ";

		String retValue = "";

		retValue =
				"OptionExpiration ( " + super.toString() + TAB + "id = " + this.id + TAB + "marketId = " + this.marketId + TAB + "expiryDate = " + this.expiryDate + TAB + "timeCreated = " + this.timeCreated + TAB + "writerId = " + this.writerId + TAB + "active = " + this.active + TAB + "expiryDateEnd = " + this.expiryDateEnd + TAB + " )";

		return retValue;
	}

	public String getMarketName() {
		return marketName;
	}

	public void setMarketName(String marketName) {
		this.marketName = marketName;
	}

	public boolean isNextAgreementOnSameDay() {
		return nextAgreementOnSameDay;
	}

	public void setNextAgreementOnSameDay(boolean nextAgreementOnSameDay) {
		this.nextAgreementOnSameDay = nextAgreementOnSameDay;
	}

    /**
     * @return the expiryDateEnd
     */
    public Date getExpiryDateEnd() {
        return expiryDateEnd;
    }

    /**
     * @param expiryDateEnd the expiryDateEnd to set
     */
    public void setExpiryDateEnd(Date expiryDateEnd) {
        this.expiryDateEnd = expiryDateEnd;
    }

    /**
     * @return the letter
     */
    public String getLetter() {
        return letter;
    }

    /**
     * @param letter the letter to set
     */
    public void setLetter(String letter) {
        this.letter = letter;
    }

    /**
     * @return the year
     */
    public String getYear() {
        return year;
    }

    /**
     * @return the year last 2 digit
     */
    public String getYearLast2Digit() {
        return year.substring(year.length() - 2);
    }

    /**
     * @param year the year to set
     */
    public void setYear(String year) {
        this.year = year;
    }
}