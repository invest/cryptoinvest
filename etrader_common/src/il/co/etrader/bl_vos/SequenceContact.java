package il.co.etrader.bl_vos;

import java.sql.Date;


public class SequenceContact extends Contact{
	/**
	 *
	 */
	private static final long serialVersionUID = 1L;
	private long nextTemplateId;
	private Date actionTime;
	private long numberOfDaysAfterLastEmail;
	/**
	 * @return the numberOfDaysAfterLastEmail
	 */
	public long getNumberOfDaysAfterLastEmail() {
		return numberOfDaysAfterLastEmail;
	}
	/**
	 * @param numberOfDaysAfterLastEmail the numberOfDaysAfterLastEmail to set
	 */
	public void setNumberOfDaysAfterLastEmail(long numberOfDaysAfterLastEmail) {
		this.numberOfDaysAfterLastEmail = numberOfDaysAfterLastEmail;
	}
	/**
	 * @return the actionTime
	 */
	public Date getActionTime() {
		return actionTime;
	}
	/**
	 * @param actionTime the actionTime to set
	 */
	public void setActionTime(Date actionTime) {
		this.actionTime = actionTime;
	}
	/**
	 * @return the nextTemplateId
	 */
	public long getNextTemplateId() {
		return nextTemplateId;
	}
	/**
	 * @param nextTemplateId the nextTemplateId to set
	 */
	public void setNextTemplateId(long nextTemplateId) {
		this.nextTemplateId = nextTemplateId;
	}
}
