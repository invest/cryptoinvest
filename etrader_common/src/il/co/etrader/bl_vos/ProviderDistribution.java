package il.co.etrader.bl_vos;

public class ProviderDistribution {
	
	private long id;
	private String name;
	private int percentage;
	
	public ProviderDistribution(long id, String name, int percentage) {
		this.id = id;
		this.name = name;
		this.percentage = percentage;
	}
	public long getClearingProviderId() {
		return id;
	}
	public void setClearingProviderId(long clearingProviderId) {
		this.id = clearingProviderId;
	}
	public String getClearingProviderName() {
		return name;
	}
	public void setClearingProviderName(String clearingProviderName) {
		this.name = clearingProviderName;
	}
	public int getClearingProviderPercentage() {
		return percentage;
	}
	public void setClearingProviderPercentage(int clearingProviderPercentage) {
		this.percentage = clearingProviderPercentage;
	}
	
	public ProviderDistribution clone() {
		return new ProviderDistribution(id, name, percentage);
	}
	
}
