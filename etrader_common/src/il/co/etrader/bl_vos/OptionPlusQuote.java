//package il.co.etrader.bl_vos;
//
//import java.io.Serializable;
//import java.util.Date;
//
//public class OptionPlusQuote implements Serializable{
//
//	private static final long serialVersionUID = 918870186384532899L;
//
//	private long id;
//	private double quoteLevel;
//	private Date timeQuoted;
//	private Date timeQuoteExpired;
//
//	/**
//	 * @return the timeQuoteExpired
//	 */
//	public Date getTimeQuoteExpired() {
//		return timeQuoteExpired;
//	}
//	/**
//	 * @param timeQuoteExpired the timeQuoteExpired to set
//	 */
//	public void setTimeQuoteExpired(Date timeQuoteExpired) {
//		this.timeQuoteExpired = timeQuoteExpired;
//	}
//	/**
//	 * @return the quoteLevel
//	 */
//	public double getQuoteLevel() {
//		return quoteLevel;
//	}
//	/**
//	 * @param quoteLevel the quoteLevel to set
//	 */
//	public void setQuoteLevel(double quoteLevel) {
//		this.quoteLevel = quoteLevel;
//	}
//	/**
//	 * @return the id
//	 */
//	public long getId() {
//		return id;
//	}
//	/**
//	 * @param id the id to set
//	 */
//	public void setId(long id) {
//		this.id = id;
//	}
//	/**
//	 * @return the timeQuoted
//	 */
//	public Date getTimeQuoted() {
//		return timeQuoted;
//	}
//	/**
//	 * @param timeQuoted the timeQuoted to set
//	 */
//	public void setTimeQuoted(Date timeQuoted) {
//		this.timeQuoted = timeQuoted;
//	}
//
//}
