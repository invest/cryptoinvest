//package il.co.etrader.bl_vos;
//
//import java.util.Date;
//
//public class MarketingTrackingCookieStatic implements java.io.Serializable{
//
//	private static final long serialVersionUID = 5651106788162437452L;
//	
//	private String ms;
//	private String cs;
//	private String http;
//	private String dp;
//	private Date ts;
//	/**
//	 * @return the ms
//	 */
//	public String getMs() {
//		return ms;
//	}
//	/**
//	 * @param ms the ms to set
//	 */
//	public void setMs(String ms) {
//		this.ms = ms;
//	}
//	/**
//	 * @return the cs
//	 */
//	public String getCs() {
//		return cs;
//	}
//	/**
//	 * @param cs the cs to set
//	 */
//	public void setCs(String cs) {
//		this.cs = cs;
//	}
//	/**
//	 * @return the http
//	 */
//	public String getHttp() {
//		return http;
//	}
//	/**
//	 * @param http the http to set
//	 */
//	public void setHttp(String http) {
//		this.http = http;
//	}
//	/**
//	 * @return the dp
//	 */
//	public String getDp() {
//		return dp;
//	}
//	/**
//	 * @param dp the dp to set
//	 */
//	public void setDp(String dp) {
//		this.dp = dp;
//	}
//	/**
//	 * @return the ts
//	 */
//	public Date getTs() {
//		return ts;
//	}
//	/**
//	 * @param ts the ts to set
//	 */
//	public void setTs(Date ts) {
//		this.ts = ts;
//	}
//
//}
