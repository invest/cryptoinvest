//package il.co.etrader.bl_vos;
//
////import il.co.etrader.bl_managers.ApplicationDataBase;
////import il.co.etrader.util.CommonUtil;
//import il.co.etrader.util.ConstantsBase;
//
//import java.math.BigDecimal;
//import java.sql.SQLException;
//import java.util.Calendar;
//import java.util.Date;
//
//public class Market implements java.io.Serializable, Cloneable {
//    private static final long serialVersionUID = 1L;
//
//    /**
//     * Use the fixed value in the markets.fixed_amount_for_shifting in the
//     * exposure calculations.
//     */
//    public static final int TYPE_OF_SHIFTING_FIXED_AMOUNT = 0;
//
//    /**
//     * Use the auto calculated average investment in the last 30 days
//     * (markets.average_investments) in exposure calculations.
//     */
//    public static final int TYPE_OF_SHIFTING_AVERAGE_INVESTMENT = 1;
//
//    /**
//     * Use the higher of the fixed markets.average_investments and
//     * markets.fixed_amount_for_shifting in the
//     * exposure calculations.
//     */
//    public static final int TYPE_OF_SHIFTING_MAX_OF_BOTH = 2;
//
//    public static final int MARKET_TEL_AVIV_25_ID   = 3;
//    public static final int MARKET_FTSE_ID          = 4;
//    public static final int MARKET_CAC_ID           = 10;
//    public static final int MARKET_USD_ILS_ID       = 15;
//    public static final int MARKET_USD_EUR_ID       = 16;
//    public static final int MARKET_USD_TRY_ID       = 17;
//    public static final int MARKET_USD_JPY_ID       = 18;
//    public static final int MARKET_GOLD_ID          = 20;
//    public static final int MARKET_OIL_ID           = 21;
//    public static final int MARKET_NASDAQ_ID        = 7;
//    public static final int MARKET_USD_RAND_ID      = 26;
//    public static final int MARKET_NASDAQF_ID       = 136;
//    public static final int MARKET_SILVER_ID        = 137;
//    public static final int MARKET_COPPER_ID        = 138;
//    public static final int MARKET_SHANGHAI_ID      = 294;
//    public static final int MARKET_AUD_USD_ID       = 289;
//    public static final int MARKET_NZD_USD_ID       = 290;
//    public static final int MARKET_JAKARTA_ID		= 370;
//    public static final int MARKET_ENERGY_ISRAEL_ID	= 435;
//    public static final int MARKET_TEL_AVIV_25_MAOF_ID = 436;
//    public static final int MARKET_ISE30_INDEX_ID   = 437;
//    public static final int MARKET_KLSE_FUTURE_ID   = 477 ;
//    public static final int MARKET_VIX_ID           = 538;
//    public static final int MARKET_ISE_FINANCIAL_ID = 539;
//    public static final int MARKET_ISE_INDUSTRIAL_ID = 540;
//    public static final int MARKET_DAX_FUTURE_ID    = 541;
//    public static final int MARKET_SP_FUTURE_ID     = 542;
//    public static final int MARKET_USD_EUR_PLUS_ID  = 552;
//    public static final int MARKET_USD_JPY_PLUS_ID  = 561;
//    public static final int MARKET_AUD_USD_PLUS_ID  = 581;
//
//    private long id;
//	private String name;
//	private String displayName;
//	private String displayNameKey;
//	private String feedName;
//	private Date timeCreated;
//	private long writerId;
//	private Long decimalPoint;
//    private Long groupId;
//    private Long investmentLimitsGroupId;
//    private String groupName;
//    private Long exchangeId;
//    //private int homePagePriority;
//    //private boolean homePageHourFirst;
//    private int groupPriority;
//    private BigDecimal currentLevelAlertPerc;
//
//    private int updatesPause;
//    private BigDecimal significantPercentage;
//    private int realUpdatesPause;
//    private BigDecimal realSignificantPercentage;
//    private BigDecimal randomCeiling;
//    private BigDecimal randomFloor;
//    private BigDecimal firstShiftParameter;
//    private BigDecimal averageInvestments;
//    private BigDecimal percentageOfAverageInvestments;
//    private BigDecimal fixedAmountForShifting;
//    private BigDecimal acceptableDeviation;
//    private BigDecimal acceptableDeviation3;
//    private Long typeOfShifting;
//    private Long disableAfterPeriod;
//    private boolean suspended;
//    private String suspendedMessage;
//    private boolean hasFutureAgreement;
//    private boolean nextAgreementSameDay;
//
//    private String exchangeName;
//    private String investmentLimitsGroupName;
//
//    // banner_priority used to tell us the priority of this market
//    private int banners_priority;
//
//    private String timeFirstInvest;
//    private String timeEstClosing;
//    private String timeZone;
//
//    private boolean haveHourly;
//    private boolean havedayly;
//    private boolean haveweekly;
//    private boolean havemonthly;
//    private boolean isHasLongTermOpp;
//
//    private int secBetweenInvestments;
//    private int secForDev2;
//    private float amountForDev2;
//    private float amountForDev2Night;
//    private float amountForDev3;
//
//	private int max_exposure;
//    // frequencePriority used to tell the frequence of level change when user not login bugId: 2029
//  //  private int frequencePriority;
//    private Date lastHourClosingTime;
//    private boolean opened;
//
//    //for 5 golden minutse
//    private double insurancePremiaPercent;
//    private double insuranceQualifyPercent;
//    private boolean isGoldenMinutes;
//
////  for roll up
//    private double rollUpPremiaPercent;
//    private double rollUpQualifyPercent;
//    private boolean isRollUp;
//
//    private double noAutoSettleAfter;
//
//    private long optionPlusFee;
//    private String tradingDays;  // assetIndex
//    private String displayGroupNameKey; // assetIndex
//    private int groupMarketsCounter; // assetIndex
//    private long optionPlusMarketId;
//
//    private int secBetweenInvestmentsSameIp;
//    private int decimalPointSubtractDigits;
//
//    private boolean isFullDay;    // for mobile assetIndex use
//    private boolean isOptionPlus;
//
////  for 1T
//    private long exposureReached;
//    private long raiseExposureUpAmount;
//    private long raiseExposureDownAmount;
//    private double upStrikeRaiseLowerPercent;
//    private double downStrikeRaiseLowerPercent;
//    
//    //is market open 24/7
//    private boolean twentyFourSeven;
//
//    //for binary 0-100
//    private BinaryZeroOneHundred zeroOneHundred;
//
//	/**
//	 * Mobile Refactoring:Remove logic in markets.xhtml Do not USE
//	 */
////	public String getTypeOfShiftingTxt() {
////
////    	if (typeOfShifting==0) {
////    		return CommonUtil.getMessage("market.shifting.type.fixed", null);
////    	}
////    	if (typeOfShifting==1) {
////    		return CommonUtil.getMessage("market.shifting.type.average", null);
////    	}
////    	if (typeOfShifting==2) {
////    		return CommonUtil.getMessage("market.shifting.type.max", null);
////    	}
////
////    	return "";
////    }
//	/**
//	 * Mobile Refactoring:Remove patterns in PresetationPatternsBase and markets.xhtml and 
//	 */
////    public String getfirstShiftParameterTxt() {
////    	return CommonUtil.displayDecimal(firstShiftParameter);
////    }
////    public String getCurrentLevelAlertPercTxt() {
////    	return CommonUtil.displayDecimal(currentLevelAlertPerc);
////    }
////    public String getaverageInvestmentsTxt() {
////    	return CommonUtil.displayDecimal(averageInvestments);
////    }
////    public String getpercentageOfAverageInvestmentsTxt() {
////    	return CommonUtil.displayDecimal(percentageOfAverageInvestments);
////    }
////    public String getfixedAmountForShiftingTxt() {
////    	return CommonUtil.displayDecimal(fixedAmountForShifting);
////    }
////    public String getUpdatesPauseTxt() {
////    	return CommonUtil.displayLong(updatesPause);
////    }
////    public String getsignificantPercentageTxt() {
////    	return CommonUtil.displayDecimal(significantPercentage);
////    }
////    public String getrealUpdatesPauseTxt() {
////    	return CommonUtil.displayLong(realUpdatesPause);
////    }
////    public String getrealSignificantPercentageTxt() {
////    	return CommonUtil.displayDecimal(realSignificantPercentage);
////    }
////    public String getrandomCeilingTxt() {
////    	return CommonUtil.displayDecimal(randomCeiling);
////    }
////    public String getrandomFloorTxt() {
////    	return CommonUtil.displayDecimal(randomFloor);
////    }
//
//
//	public Long getDecimalPoint() {
//		return decimalPoint;
//	}
//
//	public void setDecimalPoint(Long decimalPoint) {
//		this.decimalPoint = decimalPoint;
//	}
//
//	public String getDisplayName() {
//		return displayName;
//	}
//
//	public void setDisplayName(String displayName) {
//		this.displayName = displayName;
//	}
//
//	public String getFeedName() {
//		return feedName;
//	}
//
//	public void setFeedName(String feedName) {
//		this.feedName = feedName;
//	}
//
//	public long getId() {
//		return id;
//	}
//
//	public void setId(long id) {
//		this.id = id;
//	}
//
//	/**
//     * @return the investmentLimitsGroupId
//     */
//    public Long getInvestmentLimitsGroupId() {
//        return investmentLimitsGroupId;
//    }
//
//    /**
//     * @param investmentLimitsGroupId the investmentLimitsGroupId to set
//     */
//    public void setInvestmentLimitsGroupId(Long investmentLimitsGroupId) {
//        this.investmentLimitsGroupId = investmentLimitsGroupId;
//    }
//
//    public String getName() {
//		return name;
//	}
//
//	public void setName(String name) {
//		this.name = name;
//	}
//
//	public BigDecimal getRealSignificantPercentage() {
//        return realSignificantPercentage;
//    }
//
//    public void setRealSignificantPercentage(BigDecimal realSignificantPercentage) {
//        this.realSignificantPercentage = realSignificantPercentage;
//    }
//
//    public int getRealUpdatesPause() {
//        return realUpdatesPause;
//    }
//
//    public void setRealUpdatesPause(int realUpdatesPause) {
//        this.realUpdatesPause = realUpdatesPause;
//    }
//
//    public BigDecimal getSignificantPercentage() {
//        return significantPercentage;
//    }
//
//    public void setSignificantPercentage(BigDecimal significantPercentage) {
//        this.significantPercentage = significantPercentage;
//    }
//
//    public Date getTimeCreated() {
//		return timeCreated;
//	}
//
//	public void setTimeCreated(Date timeCreated) {
//		this.timeCreated = timeCreated;
//	}
//
////	public String getWriterName() throws SQLException{
////
////		return ApplicationDataBase.getWriterName(writerId);
////	}
//
//	public String getGroupName() {
//		return groupName;
//	}
//	
//	/**
//	 * Mobile Refactoring:Remove. all assetIndexInfo.xhtml mobile web
//	 */
////    public String getGroupNameTxt() {
////        return CommonUtil.getMessage(groupName, null);
////    }
//	/**
//	 * Mobile Refactoring:Remove. pattern get from com.anyoption.common.util.jsf.PresetationPatternsWeb
//	 */
//
////	public String getTimeCreatedTxt() {
////		return CommonUtil.getDateTimeFormatDisplay(timeCreated, CommonUtil.getUtcOffset(ConstantsBase.EMPTY_STRING));
////	}
//
//	public long getWriterId() {
//		return writerId;
//	}
//
//	public void setWriterId(long writerId) {
//		this.writerId = writerId;
//	}
//
//    /**
//     * @return Returns the groupId.
//     */
//    public Long getGroupId() {
//        return groupId;
//    }
//
//    /**
//     * @param groupId The groupId to set.
//     */
//    public void setGroupId(Long groupId) {
//        this.groupId = groupId;
//    }
//
//    public void setGroupName(String groupName) {
//		this.groupName = groupName;
//	}
//
//	public Long getExchangeId() {
//		return exchangeId;
//	}
//
//	public void setExchangeId(Long exchangeId) {
//		this.exchangeId = exchangeId;
//	}
//
////    /**
////     * @return the homePageHourFirst
////     */
////    public boolean isHomePageHourFirst() {
////        return homePageHourFirst;
////    }
////
////    /**
////     * @param homePageHourFirst the homePageHourFirst to set
////     */
////    public void setHomePageHourFirst(boolean homePageHourFirst) {
////        this.homePageHourFirst = homePageHourFirst;
////    }
////
////    /**
////     * @return the homePagePriority
////     */
////    public int getHomePagePriority() {
////        return homePagePriority;
////    }
////
////    /**
////     * @param homePagePriority the homePagePriority to set
////     */
////    public void setHomePagePriority(int homePagePriority) {
////        this.homePagePriority = homePagePriority;
////    }
//
//	public String getExchangeName() {
//		return exchangeName;
//	}
//
//	public void setExchangeName(String exchangeName) {
//		this.exchangeName = exchangeName;
//	}
//
//	public String getInvestmentLimitsGroupName() {
//		return investmentLimitsGroupName;
//	}
//
//	public void setInvestmentLimitsGroupName(String investmentLimitsGroupName) {
//		this.investmentLimitsGroupName = investmentLimitsGroupName;
//	}
//
////    public int getGroupPriority() {
////        return groupPriority;
////    }
////
////    public void setGroupPriority(int groupPriority) {
////        this.groupPriority = groupPriority;
////    }
//
//	public int getUpdatesPause() {
//        return updatesPause;
//    }
//
//    public void setUpdatesPause(int updatesPause) {
//        this.updatesPause = updatesPause;
//    }
//
//    public BigDecimal getAverageInvestments() {
//        return averageInvestments;
//    }
//
//    public void setAverageInvestments(BigDecimal averageInvestments) {
//        this.averageInvestments = averageInvestments;
//    }
//
//    public BigDecimal getFirstShiftParameter() {
//        return firstShiftParameter;
//    }
//
//    public void setFirstShiftParameter(BigDecimal firstShiftParameter) {
//        this.firstShiftParameter = firstShiftParameter;
//    }
//
//    public BigDecimal getFixedAmountForShifting() {
//        return fixedAmountForShifting;
//    }
//
//    public void setFixedAmountForShifting(BigDecimal fixedAmountForShifting) {
//        this.fixedAmountForShifting = fixedAmountForShifting;
//    }
//
//    public BigDecimal getPercentageOfAverageInvestments() {
//        return percentageOfAverageInvestments;
//    }
//
//    public void setPercentageOfAverageInvestments(
//            BigDecimal percentageOfAverageInvestments) {
//        this.percentageOfAverageInvestments = percentageOfAverageInvestments;
//    }
//
//    public BigDecimal getRandomCeiling() {
//        return randomCeiling;
//    }
//
//    public void setRandomCeiling(BigDecimal randomCeiling) {
//        this.randomCeiling = randomCeiling;
//    }
//
//    public BigDecimal getRandomFloor() {
//        return randomFloor;
//    }
//
//    public void setRandomFloor(BigDecimal randomFloor) {
//        this.randomFloor = randomFloor;
//    }
//
//    public Long getTypeOfShifting() {
//        return typeOfShifting;
//    }
//
//    public void setTypeOfShifting(Long typeOfShifting) {
//        this.typeOfShifting = typeOfShifting;
//    }
//
//    public Long getDisableAfterPeriod() {
//        return disableAfterPeriod;
//    }
//
//    public void setDisableAfterPeriod(Long disableAfterPeriod) {
//        this.disableAfterPeriod = disableAfterPeriod;
//    }
//
//	public boolean isSuspended() {
//        return suspended;
//    }
//
//    public void setSuspended(boolean suspended) {
//        this.suspended = suspended;
//    }
//
//    public String getSuspendedMessage() {
//        return suspendedMessage;
//    }
//
//    public void setSuspendedMessage(String suspendedMessage) {
//        this.suspendedMessage = suspendedMessage;
//    }
//
//    public String getDisplayNameKey() {
//		return displayNameKey;
//	}
//
//	/**
//	 * Mobile Refactoring:Remove. Change in ETRADER WEB: currencies.xhtml;
//	 * commodities.xhtml; foreignsocks.xhtml; indicies.xhtml; stocks.xhmtl;
//	 */
//	// public String getDisplayNameKeyTxt() {
//	// if (null == displayNameKey) {
//	// return "";
//	// }
//	// return CommonUtil.getMessage(displayNameKey, null);
//	// }
//
//	public void setDisplayNameKey(String displayNameKey) {
//		this.displayNameKey = displayNameKey;
//	}
//
//	public float getAmountForDev2() {
//        return amountForDev2;
//    }
//
//    public void setAmountForDev2(float amountForDev2) {
//        this.amountForDev2 = amountForDev2;
//    }
//
//    public int getSecBetweenInvestments() {
//        return secBetweenInvestments;
//    }
//
//    public void setSecBetweenInvestments(int secBetweenInvestments) {
//        this.secBetweenInvestments = secBetweenInvestments;
//    }
//
//    public int getSecForDev2() {
//        return secForDev2;
//    }
//
//    public void setSecForDev2(int secForDev2) {
//        this.secForDev2 = secForDev2;
//    }
//
//    /**
//	 * Constructs a <code>String</code> with all attributes
//	 * in name = value format.
//	 *
//	 * @return a <code>String</code> representation
//	 * of this object.
//	 */
//	public String toString() {
//	    final String TAB = " \n ";
//
//	    String retValue = "";
//
//	    retValue = "Market ( "
//	        + super.toString() + TAB
//	        + "id = " + this.id + TAB
//	        + "name = " + this.name + TAB
//	        + "displayName = " + this.displayName + TAB
//	        + "displayNameKey = " + this.displayNameKey + TAB
//	        + "feedName = " + this.feedName + TAB
//	        + "timeCreated = " + this.timeCreated + TAB
//	        + "writerId = " + this.writerId + TAB
//	        + "decimalPoint = " + this.decimalPoint + TAB
//	        + "groupId = " + this.groupId + TAB
//	        + "investmentLimitsGroupId = " + this.investmentLimitsGroupId + TAB
//	        + "groupName = " + this.groupName + TAB
//	        + "exchangeId = " + this.exchangeId + TAB
//	     //   + "homePagePriority = " + this.homePagePriority + TAB
//	     //   + "homePageHourFirst = " + this.homePageHourFirst + TAB
//	        + "updatesPause = " + this.updatesPause + TAB
//	        + "significantPercentage = " + this.significantPercentage + TAB
//	        + "realUpdatesPause = " + this.realUpdatesPause + TAB
//	        + "realSignificantPercentage = " + this.realSignificantPercentage + TAB
//	        + "randomCeiling = " + this.randomCeiling + TAB
//	        + "randomFloor = " + this.randomFloor + TAB
//	        + "firstShiftParameter = " + this.firstShiftParameter + TAB
//	        + "acceptableDeviation = " + this.acceptableDeviation + TAB
//            + "acceptableDeviation 3 = " + this.acceptableDeviation3 + TAB
//	        + "averageInvestments = " + this.averageInvestments + TAB
//	        + "percentageOfAverageInvestments = " + this.percentageOfAverageInvestments + TAB
//	        + "fixedAmountForShifting = " + this.fixedAmountForShifting + TAB
//	        + "typeOfShifting = " + this.typeOfShifting + TAB
//	        + "disableAfterPeriod = " + this.disableAfterPeriod + TAB
//            + "suspended = " + this.suspended + TAB
//            + "suspendedMessage = " + this.suspendedMessage + TAB
//	        + "exchangeName = " + this.exchangeName + TAB
//	        + "investmentLimitsGroupName = " + this.investmentLimitsGroupName + TAB
//	        + "bannersPriority = " + this.banners_priority + TAB
//            + "secBetweenInvestments = " + this.secBetweenInvestments + TAB
//            + "secForDev2 = " + this.secForDev2 + TAB
//            + "amountForDev2 = " + this.amountForDev2 + TAB
//            + "amountForDev3 = " + this.amountForDev3 + TAB
//            + "lastHourClosingTime = " + this.lastHourClosingTime + TAB
//            + "opened = " + this.opened + TAB
//            + "isHasLongTermOpp = " + this.isHasLongTermOpp + TAB
//            + "noAutoSettleAfter = " + this.noAutoSettleAfter + TAB
//            + "max_exposure = " + this.max_exposure + TAB
//            + "optionPlusMarketId = " + this.optionPlusMarketId + TAB
//            + "secBetweenInvestmentsSameIp = " + this.secBetweenInvestmentsSameIp + TAB
//            + "groupPriority = " + this.groupPriority + TAB
//	        + " )";
//
//	    return retValue;
//	}
//
//	public BigDecimal getCurrentLevelAlertPerc() {
//		return currentLevelAlertPerc;
//	}
//
//	public void setCurrentLevelAlertPerc(BigDecimal currentLevelAlertPerc) {
//		this.currentLevelAlertPerc = currentLevelAlertPerc;
//	}
//	
//	public BigDecimal getAcceptableDeviation() {
//		return acceptableDeviation;
//	}
//	public void setAcceptableDeviation(BigDecimal acceptableDeviation) {
//		this.acceptableDeviation = acceptableDeviation;
//	}
//	public boolean isHasFutureAgreement() {
//		return hasFutureAgreement;
//	}
//	public void setHasFutureAgreement(boolean hasFutureAgreement) {
//		this.hasFutureAgreement = hasFutureAgreement;
//	}
//	public boolean isNextAgreementSameDay() {
//		return nextAgreementSameDay;
//	}
//	public void setNextAgreementSameDay(boolean nextAgreementSameDay) {
//		this.nextAgreementSameDay = nextAgreementSameDay;
//	}
//	public int getBannersPriority() {
//		return banners_priority;
//	}
//	public void setBannersPriority(int banners_priority) {
//		this.banners_priority = banners_priority;
//	}
///*	public int getFrequencePriority() {
//		return frequencePriority;
//	}
//	public void setFrequencePriority(int frequencePriority) {
//		this.frequencePriority = frequencePriority;
//	}*/
//	public boolean isHavedayly() {
//		return havedayly;
//	}
//	public void setHavedayly(boolean havedayly) {
//		this.havedayly = havedayly;
//	}
//	public boolean isHaveHourly() {
//		return haveHourly;
//	}
//	public void setHaveHourly(boolean haveHourly) {
//		this.haveHourly = haveHourly;
//	}
//	public boolean isHavemonthly() {
//		return havemonthly;
//	}
//	public void setHavemonthly(boolean havemonthly) {
//		this.havemonthly = havemonthly;
//	}
//	public boolean isHaveweekly() {
//		return haveweekly;
//	}
//	public void setHaveweekly(boolean haveweekly) {
//		this.haveweekly = haveweekly;
//	}
//	public String getTimeEstClosing() {
//		return timeEstClosing;
//	}
//	public void setTimeEstClosing(String time_est_closing) {
//		timeEstClosing = time_est_closing;
//	}
//	public String getTimeFirstInvest() {
//		return timeFirstInvest;
//	}
//	public void setTimeFirstInvest(String time_first_invest) {
//		timeFirstInvest = time_first_invest;
//	}
//	public String getTimeZone() {
//		return timeZone;
//	}
//	public void setTimeZone(String time_zone) {
//		timeZone = time_zone;
//	}
//
//	public Date getLastHourClosingTime() {
//        return lastHourClosingTime;
//    }
//
//    public void setLastHourClosingTime(Date lastHourClosingTime) {
//        this.lastHourClosingTime = lastHourClosingTime;
//    }
//
//    public boolean isOpened() {
//        return opened;
//    }
//
//    public void setOpened(boolean opened) {
//        this.opened = opened;
//    }
//
//    public Market clone(){
//		Market retVal = new Market();
//		retVal.setId(this.id);
//		retVal.setName(this.name);
//        retVal.setDisplayName(this.displayName);
//        retVal.setDisplayNameKey(this.displayNameKey);
//        retVal.setFeedName(this.feedName);
//		retVal.setTimeCreated(this.timeCreated);
//		retVal.setWriterId(this.writerId);
//		retVal.setDecimalPoint(this.decimalPoint);
//		retVal.setGroupId (this.groupId);
//		retVal.setInvestmentLimitsGroupId(investmentLimitsGroupId);
//		retVal.setGroupName (groupName);
//		retVal.setExchangeId (exchangeId);
//		retVal.setCurrentLevelAlertPerc (currentLevelAlertPerc);
//		retVal.setUpdatesPause (updatesPause);
//		retVal.setSignificantPercentage (significantPercentage);
//		retVal.setRealUpdatesPause (realUpdatesPause);
//		retVal.setRealSignificantPercentage (realSignificantPercentage);
//		retVal.setRandomCeiling (randomCeiling);
//		retVal.setRandomFloor (randomFloor);
//		retVal.setFirstShiftParameter (firstShiftParameter);
//		retVal.setAverageInvestments (averageInvestments);
//		retVal.setPercentageOfAverageInvestments (percentageOfAverageInvestments);
//		retVal.setFixedAmountForShifting (fixedAmountForShifting);
//		retVal.setAcceptableDeviation (acceptableDeviation);
//        retVal.setAcceptableDeviation3 (acceptableDeviation3);
//		retVal.setTypeOfShifting (typeOfShifting);
//		retVal.setDisableAfterPeriod (disableAfterPeriod);
//		retVal.setSuspended (suspended);
//		retVal.setSuspendedMessage (suspendedMessage);
//		retVal.setHasFutureAgreement (hasFutureAgreement);
//		retVal.setNextAgreementSameDay (nextAgreementSameDay);
//		retVal.setExchangeName (exchangeName);
//		retVal.setInvestmentLimitsGroupName (investmentLimitsGroupName);
//		retVal.setBannersPriority(banners_priority);
//		retVal.setTimeFirstInvest(timeFirstInvest);
//		retVal.setTimeEstClosing(timeEstClosing);
//		retVal.setTimeZone(timeZone);
//		retVal.setHaveHourly(haveHourly);
//		retVal.setHavedayly (havedayly);
//		retVal.setHaveweekly (haveweekly);
//		retVal.setHavemonthly (havemonthly);
//        retVal.setSecBetweenInvestments(secBetweenInvestments);
//        retVal.setSecForDev2(secForDev2);
//        retVal.setAmountForDev2(amountForDev2);
//        retVal.setAmountForDev2(amountForDev2Night);
//        retVal.setAmountForDev3(amountForDev3);
//        retVal.setMax_exposure(max_exposure);
//        retVal.setLastHourClosingTime(lastHourClosingTime);
//        retVal.setOpened(opened);
//        retVal.setHasLongTerm(isHasLongTermOpp);
//        retVal.setNoAutoSettleAfter(noAutoSettleAfter);
//        retVal.setTradingDays(tradingDays);
//        retVal.setDisplayGroupNameKey(displayGroupNameKey);
//        retVal.setOptionPlusMarketId(optionPlusMarketId);
//        retVal.setGroupMarketsCounter(groupMarketsCounter);
//        retVal.setSecBetweenInvestmentsSameIp(secBetweenInvestmentsSameIp);
//        retVal.setDecimalPointSubtractDigits(decimalPointSubtractDigits);
//        retVal.setFullDay(isFullDay);
//        retVal.setOptionPlus(isOptionPlus);
//        retVal.setGroupPriority(groupPriority);
//        retVal.setZeroOneHundred(zeroOneHundred);
//		return retVal;
//	}
//	public int getMax_exposure() {
//		return max_exposure;
//	}
//	public void setMax_exposure(int max_exposure) {
//		this.max_exposure = max_exposure;
//	}
//    /**
//     * @return the insurancePremiaPercent
//     */
//    public double getInsurancePremiaPercent() {
//        return insurancePremiaPercent;
//    }
//    /**
//     * @param insurancePremiaPercent the insurancePremiaPercent to set
//     */
//    public void setInsurancePremiaPercent(double insurancePremiaPercent) {
//        this.insurancePremiaPercent = insurancePremiaPercent;
//    }
//    /**
//     * @return the insuranceQualifyPercent
//     */
//    public double getInsuranceQualifyPercent() {
//        return insuranceQualifyPercent;
//    }
//    /**
//     * @param insuranceQualifyPercent the insuranceQualifyPercent to set
//     */
//    public void setInsuranceQualifyPercent(double insuranceQualifyPercent) {
//        this.insuranceQualifyPercent = insuranceQualifyPercent;
//    }
//    /**
//     * @return the isGoldenMinutes
//     */
//    public boolean isGoldenMinutes() {
//        return isGoldenMinutes;
//    }
//    /**
//     * @param isGoldenMinutes the isGoldenMinutes to set
//     */
//    public void setGoldenMinutes(boolean isGoldenMinutes) {
//        this.isGoldenMinutes = isGoldenMinutes;
//    }
//    /**
//     * @return the isRollUp
//     */
//    public boolean isRollUp() {
//        return isRollUp;
//    }
//    /**
//     * @param isRollUp the isRollUp to set
//     */
//    public void setRollUp(boolean isRollUp) {
//        this.isRollUp = isRollUp;
//    }
//    /**
//     * @return the rollUpPremiaPercent
//     */
//    public double getRollUpPremiaPercent() {
//        return rollUpPremiaPercent;
//    }
//    /**
//     * @param rollUpPremiaPercent the rollUpPremiaPercent to set
//     */
//    public void setRollUpPremiaPercent(double rollUpPremiaPercent) {
//        this.rollUpPremiaPercent = rollUpPremiaPercent;
//    }
//    /**
//     * @return the rollUpQualifyPercent
//     */
//    public double getRollUpQualifyPercent() {
//        return rollUpQualifyPercent;
//    }
//    /**
//     * @param rollUpQualifyPercent the rollUpQualifyPercent to set
//     */
//    public void setRollUpQualifyPercent(double rollUpQualifyPercent) {
//        this.rollUpQualifyPercent = rollUpQualifyPercent;
//    }
//
//    /**
//     * @return the isHasLongTerm
//     */
//    public boolean isHasLongTermOpp() {
//        return isHasLongTermOpp;
//    }
//    /**
//     * @param isHasLongTerm the isHasLongTerm to set
//     */
//    public void setHasLongTerm(boolean isHasLongTermOpp) {
//        this.isHasLongTermOpp = isHasLongTermOpp;
//    }
//    /**
//     * @return the noAutoSettleAfter
//     */
//    public double getNoAutoSettleAfter() {
//        return noAutoSettleAfter;
//    }
//    /**
//     * @param noAutoSettleAfter the noAutoSettleAfter to set
//     */
//    public void setNoAutoSettleAfter(double noAutoSettleAfter) {
//        this.noAutoSettleAfter = noAutoSettleAfter;
//    }
//    /**
//     * @return the optionPlusFee
//     */
//    public long getOptionPlusFee() {
//        return optionPlusFee;
//    }
//    /**
//     * @param optionPlusFee the optionPlusFee to set
//     */
//    public void setOptionPlusFee(long optionPlusFee) {
//        this.optionPlusFee = optionPlusFee;
//    }
//    /**
//     * @return the tradingDays
//     */
//    public String getTradingDays() {
//        return tradingDays;
//    }
//    /**
//     * @param tradingDays the tradingDays to set
//     */
//    public void setTradingDays(String tradingDays) {
//        this.tradingDays = tradingDays;
//    }
//
//    /**
//     * @return the displayGroupNameKey
//     */
//    public String getDisplayGroupNameKey() {
//        return displayGroupNameKey;
//    }
//
//    /**
//     * @param displayGroupNameKey the displayGroupNameKey to set
//     */
//    public void setDisplayGroupNameKey(String displayGroupNameKey) {
//        this.displayGroupNameKey = displayGroupNameKey;
//    }
//
//	/**
//	 * Mobile Refactoring:Remove. all assetIndexInfo.xhtml mobile web
//	 */
////    public String getDisplayGroupNameKeyTxt() {
////        if (null == displayGroupNameKey) {
////            return "";
////        }
////        return CommonUtil.getMessage(displayGroupNameKey, null);
////    }
//
//	public long getOptionPlusMarketId() {
//		return optionPlusMarketId;
//	}
//
//	public void setOptionPlusMarketId(long optionPlusMarketId) {
//		this.optionPlusMarketId = optionPlusMarketId;
//	}
//
//	/**
//	 * @return the groupMarketsCounter
//	 */
//	public int getGroupMarketsCounter() {
//		return groupMarketsCounter;
//	}
//
//	/**
//	 * @param groupMarketsCounter the groupMarketsCounter to set
//	 */
//	public void setGroupMarketsCounter(int groupMarketsCounter) {
//		this.groupMarketsCounter = groupMarketsCounter;
//	}
//    /**
//     * @return the acceptableDeviation3
//     */
//    public BigDecimal getAcceptableDeviation3() {
//        return acceptableDeviation3;
//    }
//    /**
//     * @param acceptableDeviation3 the acceptableDeviation3 to set
//     */
//    public void setAcceptableDeviation3(BigDecimal acceptableDeviation3) {
//        this.acceptableDeviation3 = acceptableDeviation3;
//    }
//
//    /**
//     * @return the secBetweenInvestmentsSameIp
//     */
//    public int getSecBetweenInvestmentsSameIp() {
//        return secBetweenInvestmentsSameIp;
//    }
//
//    /**
//     * @param secBetweenInvestmentsSameIp the secBetweenInvestmentsSameIp to set
//     */
//    public void setSecBetweenInvestmentsSameIp(int secBetweenInvestmentsSameIp) {
//        this.secBetweenInvestmentsSameIp = secBetweenInvestmentsSameIp;
//    }
//
//    /**
//     * @return the amountForDev3
//     */
//    public float getAmountForDev3() {
//        return amountForDev3;
//    }
//
//    /**
//     * @param amountForDev3 the amountForDev3 to set
//     */
//    public void setAmountForDev3(float amountForDev3) {
//        this.amountForDev3 = amountForDev3;
//    }
//
//    /**
//     * @return the decimalPointSubtractDigits
//     */
//    public int getDecimalPointSubtractDigits() {
//        return decimalPointSubtractDigits;
//    }
//
//    /**
//     * @param decimalPointSubtractDigits the decimalPointSubtractDigits to set
//     */
//    public void setDecimalPointSubtractDigits(int decimalPointSubtractDigits) {
//        this.decimalPointSubtractDigits = decimalPointSubtractDigits;
//    }
//    /**
//     * @return the amountForDev2Night
//     */
//    public float getAmountForDev2Night() {
//        return amountForDev2Night;
//    }
//    /**
//     * @param amountForDev2Night the amountForDev2Night to set
//     */
//    public void setAmountForDev2Night(float amountForDev2Night) {
//        this.amountForDev2Night = amountForDev2Night;
//    }
//	/**
//	 * @return the isFullDay
//	 */
//	public boolean isFullDay() {
//		return isFullDay;
//	}
//	/**
//	 * @param isFullDay the isFullDay to set
//	 */
//	public void setFullDay(boolean isFullDay) {
//		this.isFullDay = isFullDay;
//	}
//
//    /**
//     * if market time created is less then 3 months return true else false
//     * @return true if its new else false
//     */
//    public boolean isNewMarket() {
//        Calendar createTime = Calendar.getInstance();
//        createTime.setTime(timeCreated);
//        Calendar monthsAgo = Calendar.getInstance();
//        monthsAgo.set(Calendar.MONTH, monthsAgo.get(Calendar.MONTH) - 3);
//        if (createTime.after(monthsAgo)) {
//            return true;
//        }
//        return false;
//    }
//
//    /**
//     * @return the isOptionPlus
//     */
//    public boolean isOptionPlus() {
//        return isOptionPlus;
//    }
//
//    /**
//     * @param isOptionPlus the isOptionPlus to set
//     */
//    public void setOptionPlus(boolean isOptionPlus) {
//        this.isOptionPlus = isOptionPlus;
//    }
//
//    /**
//     * @return the group Priority
//     */
//    public int getGroupPriority() {
//        return groupPriority;
//    }
//
//    /**
//     * @return the group Priority with 2 digits or more
//     */
//    public String getGroupPriority2Digit() {
//        if (groupPriority < 10) {
//            return "0" + groupPriority;
//        }
//        return String.valueOf(groupPriority);
//    }
//
//    /**
//     * @param groupPriority the group Priority to set
//     */
//    public void setGroupPriority(int groupPriority) {
//        this.groupPriority = groupPriority;
//    }
//    /**
//     * @return the exposureReached
//     */
//    public long getExposureReached() {
//        return exposureReached;
//    }
//    /**
//     * @param exposureReached the exposureReached to set
//     */
//    public void setExposureReached(long exposureReached) {
//        this.exposureReached = exposureReached;
//    }
//    /**
//     * @return the raiseExposureUpAmount
//     */
//    public long getRaiseExposureUpAmount() {
//        return raiseExposureUpAmount;
//    }
//    /**
//     * @param raiseExposureUpAmount the raiseExposureUpAmount to set
//     */
//    public void setRaiseExposureUpAmount(long raiseExposureUpAmount) {
//        this.raiseExposureUpAmount = raiseExposureUpAmount;
//    }
//    /**
//     * @return the raiseExposureDownAmount
//     */
//    public long getRaiseExposureDownAmount() {
//        return raiseExposureDownAmount;
//    }
//    /**
//     * @param raiseExposureDownAmount the raiseExposureDownAmount to set
//     */
//    public void setRaiseExposureDownAmount(long raiseExposureDownAmount) {
//        this.raiseExposureDownAmount = raiseExposureDownAmount;
//    }
//    /**
//     * @return the upStrikeRaiseLowerPercent
//     */
//    public double getUpStrikeRaiseLowerPercent() {
//        return upStrikeRaiseLowerPercent;
//    }
//    /**
//     * @param upStrikeRaiseLowerPercent the upStrikeRaiseLowerPercent to set
//     */
//    public void setUpStrikeRaiseLowerPercent(double upStrikeRaiseLowerPercent) {
//        this.upStrikeRaiseLowerPercent = upStrikeRaiseLowerPercent;
//    }
//    /**
//     * @return the downStrikeRaiseLowerPercent
//     */
//    public double getDownStrikeRaiseLowerPercent() {
//        return downStrikeRaiseLowerPercent;
//    }
//    /**
//     * @param downStrikeRaiseLowerPercent the downStrikeRaiseLowerPercent to set
//     */
//    public void setDownStrikeRaiseLowerPercent(double downStrikeRaiseLowerPercent) {
//        this.downStrikeRaiseLowerPercent = downStrikeRaiseLowerPercent;
//    }
//	/**
//	 * @return the twentyFourSeven
//	 */
//	public boolean isTwentyFourSeven() {
//		return twentyFourSeven;
//	}
//	/**
//	 * @param twentyFourSeven the twentyFourSeven to set
//	 */
//	public void setTwentyFourSeven(boolean twentyFourSeven) {
//		this.twentyFourSeven = twentyFourSeven;
//	}
//	
//	public BinaryZeroOneHundred getZeroOneHundred() {
//		return zeroOneHundred;
//	}
//	public void setZeroOneHundred(BinaryZeroOneHundred zeroOneHundred) {
//		this.zeroOneHundred = zeroOneHundred;
//	}
//}