package il.co.etrader.bl_vos;

public class AlertsBase {
    
    private Long userId;
    private Integer alertType;  
    private String alertStatusId;   
    private Integer alertClassType; 
    private Integer alertFromAmount;    
    private Integer alertToAmount;  
    private String alertEmailSubject;   
    private String alertEmailRecepients;
    
    public Long getUserId() {
        return userId;
    }
    public void setUserId(Long userId) {
        this.userId = userId;
    }
    public Integer getAlertType() {
        return alertType;
    }
    public void setAlertType(Integer alertType) {
        this.alertType = alertType;
    }
    public String getAlertStatusId() {
        return alertStatusId;
    }
    public void setAlertStatusId(String alertStatusId) {
        this.alertStatusId = alertStatusId;
    }
    public Integer getAlertClassType() {
        return alertClassType;
    }
    public void setAlertClassType(Integer alertClassType) {
        this.alertClassType = alertClassType;
    }
    public Integer getAlertFromAmount() {
        return alertFromAmount;
    }
    public void setAlertFromAmount(Integer alertFromAmount) {
        this.alertFromAmount = alertFromAmount;
    }
    public Integer getAlertToAmount() {
        return alertToAmount;
    }
    public void setAlertToAmount(Integer alertToAmount) {
        this.alertToAmount = alertToAmount;
    }
    public String getAlertEmailSubject() {
        return alertEmailSubject;
    }
    public void setAlertEmailSubject(String alertEmailSubject) {
        this.alertEmailSubject = alertEmailSubject;
    }
    public String getAlertEmailRecepients() {
        return alertEmailRecepients;
    }
    public void setAlertEmailRecepients(String alertEmailRecepients) {
        this.alertEmailRecepients = alertEmailRecepients;
    }
    

}
