package il.co.etrader.bl_vos;

import java.util.ArrayList;

import com.anyoption.common.bl_vos.PopulationEntryBase;

public class StatusData implements java.io.Serializable {

	private static final long serialVersionUID = 1L;
	
	private ArrayList<PopulationEntryBase> statusArray;
	private int countOfAssignment; //auto Assign Job

	public StatusData() {
		statusArray = new ArrayList<PopulationEntryBase>();
	}

	/**
	 * @return the statusArray
	 */
	public ArrayList<PopulationEntryBase> getStatusArray() {
		return statusArray;
	}

	/**
	 * @param statusArray the statusArray to set
	 */
	public void setStatusArray(ArrayList<PopulationEntryBase> statusArray) {
		this.statusArray = statusArray;
	}

	/**
	 * @return the countOfAssignment
	 */
	public int getCountOfAssignment() {
		return countOfAssignment;
	}

	/**
	 * @param countOfAssignment the countOfAssignment to set
	 */
	public void setCountOfAssignment(int countOfAssignment) {
		this.countOfAssignment = countOfAssignment;
	}
		
}
