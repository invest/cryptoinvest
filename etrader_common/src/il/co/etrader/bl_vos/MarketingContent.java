package il.co.etrader.bl_vos;

import il.co.etrader.util.CommonUtil;

import java.io.Serializable;
import java.sql.SQLException;
import java.util.Date;

import org.apache.myfaces.custom.fileupload.UploadedFile;

/**
 * Marketing content class
 *
 * @author Kobi.
 */
public class MarketingContent implements Serializable {
    private static final long serialVersionUID = 1L;

    protected long id;
    protected String name;
    protected long writerId;
    protected Date timeCreated;
    protected String comments;
    protected String fileName;
    protected UploadedFile file;

	/**
	 * @return the id
	 */
	public long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(long id) {
		this.id = id;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}


	/**
	 * @return the timeCreated
	 */
	public Date getTimeCreated() {
		return timeCreated;
	}

	/**
	 * @return the timeCreatedTxt
	 */
	public String getTimeCreatedTxt() {
		return CommonUtil.getDateFormat(timeCreated, CommonUtil.getUtcOffset());
	}

	/**
	 * @param timeCreated the timeCreated to set
	 */
	public void setTimeCreated(Date timeCreated) {
		this.timeCreated = timeCreated;
	}

	/**
	 * @return the writerId
	 */
	public long getWriterId() {
		return writerId;
	}

	/**
	 * Get writer name
	 * @return
	 * @throws SQLException
	 */
	public String getWriterTxt() throws SQLException {
		return CommonUtil.getWriterName(writerId);
	}

	/**
	 * @param writerId the writerId to set
	 */
	public void setWriterId(long writerId) {
		this.writerId = writerId;
	}

	/**
	 * @return the comments
	 */
	public String getComments() {
		return comments;
	}

	/**
	 * @param comments the comments to set
	 */
	public void setComments(String comments) {
		this.comments = comments;
	}

	/**
	 * @return the fileName
	 */
	public String getFileName() {
		return fileName;
	}

	/**
	 * @param fileName the fileName to set
	 */
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	/**
	 * @return the file
	 */
	public UploadedFile getFile() {
		return file;
	}

	/**
	 * @param file the file to set
	 */
	public void setFile(UploadedFile file) {
		this.file = file;
	}

	/**
	 * toString implementation.
	 */
	public String toString() {
        String ls = System.getProperty("line.separator");
        return ls + "MarketingContent:" + ls +
            "id: " + id + ls +
            "name: " + name + ls +
	        "writerId: " + writerId + ls +
	        "comments: " + comments + ls +
	        "timeCreated: " + timeCreated + ls;
    }

}