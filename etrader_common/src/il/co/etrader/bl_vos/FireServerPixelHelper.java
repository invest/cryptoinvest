package il.co.etrader.bl_vos;

/**
 * 
 * @author eyal.ohana
 *
 */
public class FireServerPixelHelper {
	private long userId;
	private long platformId;
	private long transactionId;
	private String idfa;
	private String advertisingId;
	
	/**
	 * @return the userId
	 */
	public long getUserId() {
		return userId;
	}
	
	/**
	 * @param userId the userId to set
	 */
	public void setUserId(long userId) {
		this.userId = userId;
	}
	
	/**
	 * @return the idfa
	 */
	public String getIdfa() {
		return idfa;
	}
	
	/**
	 * @param idfa the idfa to set
	 */
	public void setIdfa(String idfa) {
		this.idfa = idfa;
	}
	
	/**
	 * @return the advertisingId
	 */
	public String getAdvertisingId() {
		return advertisingId;
	}
	
	/**
	 * @param advertisingId the advertisingId to set
	 */
	public void setAdvertisingId(String advertisingId) {
		this.advertisingId = advertisingId;
	}

	/**
	 * @return the platformId
	 */
	public long getPlatformId() {
		return platformId;
	}

	/**
	 * @param platformId the platformId to set
	 */
	public void setPlatformId(long platformId) {
		this.platformId = platformId;
	}

	/**
	 * @return the transactionId
	 */
	public long getTransactionId() {
		return transactionId;
	}

	/**
	 * @param transactionId the transactionId to set
	 */
	public void setTransactionId(long transactionId) {
		this.transactionId = transactionId;
	}
}
