package il.co.etrader.bl_vos;

import il.co.etrader.util.CommonUtil;

import java.io.Serializable;
import java.sql.SQLException;
import java.util.Date;

/**
 * Marketing territory class
 *
 * @author Kobi.
 */
public class MarketingTerritory implements Serializable {
    private static final long serialVersionUID = 1L;

    protected long id;
    protected String territory;
    protected long writerId;
    protected Date timeCreated;


	/**
	 * @return the id
	 */
	public long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(long id) {
		this.id = id;
	}

	/**
	 * @return the territory
	 */
	public String getTerritory() {
		return territory;
	}

	/**
	 * @param territory the territory to set
	 */
	public void setTerritory(String territory) {
		this.territory = territory;
	}

	/**
	 * @return the timeCreated
	 */
	public Date getTimeCreated() {
		return timeCreated;
	}

	/**
	 * @param timeCreated the timeCreated to set
	 */
	public void setTimeCreated(Date timeCreated) {
		this.timeCreated = timeCreated;
	}

	/**
	 * @return the writerId
	 */
	public long getWriterId() {
		return writerId;
	}

	/**
	 * @param writerId the writerId to set
	 */
	public void setWriterId(long writerId) {
		this.writerId = writerId;
	}

	/**
	 * @return the timeCreatedTxt
	 */
	public String getTimeCreatedTxt() {
		return CommonUtil.getDateFormat(timeCreated, CommonUtil.getUtcOffset());
	}


	/**
	 * Get writer name
	 * @return
	 * @throws SQLException
	 */
	public String getWriterTxt() throws SQLException {
		return CommonUtil.getWriterName(writerId);
	}


	/**
	 * toString implementation.
	 */
	public String toString() {
        String ls = System.getProperty("line.separator");
        return ls + "MarketingCampaigns:" + ls +
            "id: " + id + ls +
            "territory: " + territory + ls +
	        "writerId: " + writerId + ls +
	        "timeCreated: " + timeCreated + ls;
    }

}