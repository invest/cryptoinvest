package il.co.etrader.bl_vos;

import il.co.etrader.bl_managers.InvestmentsManagerBase;
import il.co.etrader.bl_managers.TransactionsManagerBase;
import il.co.etrader.util.CommonUtil;
import il.co.etrader.util.ConstantsBase;

import java.io.Serializable;
import java.sql.SQLException;
import java.util.Date;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.Investment;
import com.anyoption.common.beans.Transaction;

/**
 * TierUserHistory class
 *
 * @author Kobi.
 */
public class TierUserHistory implements Serializable {
	private static final long serialVersionUID = -6618354849329323311L;

	private static final Logger logger = Logger.getLogger(TierUserHistory.class);

    protected long id;
    protected long userId;
    protected long tierId;
    protected long writerId;
    protected Date timeCreated;
    protected long points;            // user loyalty points
    protected long keyValue;          // reference to the trx / invest
    protected long tierHistoryActionId;
    protected long actionPoints;
    protected String tableName;
    protected String utcOffset;

    protected String tierHisAction;
    protected String writer;
    protected String tierName;

    protected Transaction transaction;
    protected Investment investment;

	/**
	 * @return the id
	 */
	public long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(long id) {
		this.id = id;
	}

	/**
	 * @return the keyValue
	 */
	public long getKeyValue() {
		return keyValue;
	}

	/**
	 * @param keyValue the keyValue to set
	 */
	public void setKeyValue(long keyValue) {
		this.keyValue = keyValue;
	}

	/**
	 * @return the points
	 */
	public long getPoints() {
		return points;
	}

	/**
	 * @param points the points to set
	 */
	public void setPoints(long points) {
		this.points = points;
	}

	/**
	 * @return the tierHistoryAction
	 */
	public long getTierHistoryActionId() {
		return tierHistoryActionId;
	}

	/**
	 * @param tierHistoryAction the tierHistoryAction to set
	 */
	public void setTierHistoryActionId(long tierHistoryActionId) {
		this.tierHistoryActionId = tierHistoryActionId;
	}

	/**
	 * @return the tierId
	 */
	public long getTierId() {
		return tierId;
	}

	/**
	 * @param tierId the tierId to set
	 */
	public void setTierId(long tierId) {
		this.tierId = tierId;
	}

	/**
	 * @return the timeCreated
	 */
	public Date getTimeCreated() {
		return timeCreated;
	}

	/**
	 * @return the timeCreated with offset
	 */
	public String getTimeCreatedTxt() {
		return CommonUtil.getDateTimeFormatDisplay(timeCreated, utcOffset);
	}

	/**
	 * @param timeCreated the timeCreated to set
	 */
	public void setTimeCreated(Date timeCreated) {
		this.timeCreated = timeCreated;
	}

	/**
	 * @return the userId
	 */
	public long getUserId() {
		return userId;
	}

	/**
	 * @param userId the userId to set
	 */
	public void setUserId(long userId) {
		this.userId = userId;
	}

	/**
	 * @return the writerId
	 */
	public long getWriterId() {
		return writerId;
	}

	/**
	 * @param writerId the writerId to set
	 */
	public void setWriterId(long writerId) {
		this.writerId = writerId;
	}

	/**
	 * @return the tierHisAction
	 */
	public String getTierHisAction() {
		return tierHisAction;
	}

	/**
	 * @param tierHisAction the tierHisAction to set
	 */
	public void setTierHisAction(String tierHisAction) {
		this.tierHisAction = tierHisAction;
	}

	/**
	 * @return the actionPoints
	 */
	public long getActionPoints() {
		return actionPoints;
	}

	/**
	 * @param actionPoints the actionPoints to set
	 */
	public void setActionPoints(long actionPoints) {
		this.actionPoints = actionPoints;
	}

	/**
	 * @return the tableName
	 */
	public String getTableName() {
		return tableName;
	}

	/**
	 * @param tableName the tableName to set
	 */
	public void setTableName(String tableName) {
		this.tableName = tableName;
	}

	/**
	 * @return the writer
	 */
	public String getWriter() {
		return writer;
	}

	/**
	 * @param writer the writer to set
	 */
	public void setWriter(String writer) {
		this.writer = writer;
	}

	/**
	 * @return the tierName
	 */
	public String getTierName() {
		return tierName;
	}

	/**
	 * @param tierName the tierName to set
	 */
	public void setTierName(String tierName) {
		this.tierName = tierName;
	}

	/**
	 * @return the utcOffset
	 */
	public String getUtcOffset() {
		return utcOffset;
	}

	/**
	 * @param utcOffset the utcOffset to set
	 */
	public void setUtcOffset(String utcOffset) {
		this.utcOffset = utcOffset;
	}

	public boolean isInvestmentKey() {
		if (!CommonUtil.isParameterEmptyOrNull(tableName) && tableName.equals(ConstantsBase.TABLE_INVESTMENTS)) {
			return true;
		}
		return false;
	}

	public boolean isTransactionKey() {
		if (!CommonUtil.isParameterEmptyOrNull(tableName) && tableName.equals(ConstantsBase.TABLE_TRANSACTIONS)) {
			return true;
		}
		return false;
	}

	public Transaction getTransaction() throws SQLException{
		if (transaction == null) {
			logger.debug("get transaction from db");
			transaction = TransactionsManagerBase.getTransaction(keyValue);
		}
		return transaction;
	}

	public Investment getInvestment() throws SQLException{
		if (investment == null) {
			logger.debug("get investment from db");
			investment = InvestmentsManagerBase.getInvestmentDetails(keyValue);
		}
		return investment;
	}

	/**
	 * toString implementation.
	 */
	public String toString() {
        String ls = System.getProperty("line.separator");
        return ls + "TierUserHis:" + ls +
            "id: " + id + ls +
            "userId: " + userId + ls +
            "tierId: " + tierId + ls +
            "writerId: " + writerId + ls +
            "timeCreated: " + timeCreated + ls +
            "points: " + points + ls +
            "actionPoints: " + actionPoints + ls +
            "keyValue: " + keyValue + ls;
    }

}