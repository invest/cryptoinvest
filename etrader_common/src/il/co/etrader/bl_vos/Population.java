package il.co.etrader.bl_vos;


import il.co.etrader.bl_managers.ApplicationDataBase;
import il.co.etrader.util.CommonUtil;

import java.io.Serializable;
import java.sql.SQLException;
import java.util.Date;

import org.apache.log4j.Logger;

public class Population implements Serializable {

	private static final long serialVersionUID = 3613443037310248916L;

	private static final Logger logger = Logger.getLogger(Population.class);

	private long id;
	private String name;
	private long skinId;
	private long languageId;
	private Date startDate;
	private Date endDate;
	private int threshold;
	private long numIssues;
	private boolean isActive;
	private long refreshTimePeriod;
	private int numberOfMinutes;
	private int numberOfDays;
	private int writerId;
	private Date timeCreated;
	private long balanceAmount;
	private int lastInvestDay;
	private long populationTypeId;
	private String populationTypeName;
	private int populationTypePriority;
	private int numberOfDeposits;
	private int countPopulation;
	private int entryStatusId;
	private Date timeControl;

	/**
	 * @return the entryStatusId
	 */
	public int getEntryStatusId() {
		return entryStatusId;
	}

	/**
	 * @param entryStatusId the entryStatusId to set
	 */
	public void setEntryStatusId(int entryStatusId) {
		this.entryStatusId = entryStatusId;
	}

	public Population() {
    }

	/**
	 * @return the balanceAmount
	 */
	public long getBalanceAmount() {
		return balanceAmount;
	}

	/**
	 * @param balanceAmount the balanceAmount to set
	 */
	public void setBalanceAmount(long balanceAmount) {
		this.balanceAmount = balanceAmount;
	}

	/**
	 * @return the endDate
	 */
	public Date getEndDate() {
		return endDate;
	}

	/**
	 * @param endDate the endDate to set
	 */
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	/**
	 * @return the id
	 */
	public long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(long id) {
		this.id = id;
	}

	/**
	 * @return the isActive
	 */
	public boolean getIsActive() {
		return isActive;
	}

	/**
	 * @param isActive the isActive to set
	 */
	public void setIsActive(boolean isActive) {
		this.isActive = isActive;
	}

	/**
	 * @return the languageId
	 */
	public long getLanguageId() {
		return languageId;
	}

	/**
	 * @param languageId the languageId to set
	 */
	public void setLanguageId(long languageId) {
		this.languageId = languageId;
	}

	/**
	 * @return the lastInvestDay
	 */
	public int getLastInvestDay() {
		return lastInvestDay;
	}

	/**
	 * @param lastInvestDay the lastInvestDay to set
	 */
	public void setLastInvestDay(int lastInvestDay) {
		this.lastInvestDay = lastInvestDay;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the numberOfDays
	 */
	public int getNumberOfDays() {
		return numberOfDays;
	}

	/**
	 * @param numberOfDays the numberOfDays to set
	 */
	public void setNumberOfDays(int numberOfDays) {
		this.numberOfDays = numberOfDays;
	}

	/**
	 * @return the numberOfMinutes
	 */
	public int getNumberOfMinutes() {
		return numberOfMinutes;
	}

	/**
	 * @param numberOfMinutes the numberOfMinutes to set
	 */
	public void setNumberOfMinutes(int numberOfMinutes) {
		this.numberOfMinutes = numberOfMinutes;
	}

	/**
	 * @return the numIssues
	 */
	public long getNumIssues() {
		return numIssues;
	}

	/**
	 * @param numIssues the numIssues to set
	 */
	public void setNumIssues(long numIssues) {
		this.numIssues = numIssues;
	}

	/**
	 * @return the populationTypeId
	 */
	public long getPopulationTypeId() {
		return populationTypeId;
	}

	/**
	 * @param populationTypeId the populationTypeId to set
	 */
	public void setPopulationTypeId(long populationTypeId) {
		this.populationTypeId = populationTypeId;
	}

	/**
	 * @return the refreshTimePeriod
	 */
	public long getRefreshTimePeriod() {
		return refreshTimePeriod;
	}

	/**
	 * @param refreshTimePeriod the refreshTimePeriod to set
	 */
	public void setRefreshTimePeriod(long refreshTimePeriod) {
		this.refreshTimePeriod = refreshTimePeriod;
	}

	/**
	 * @return the skinId
	 */
	public long getSkinId() {
		return skinId;
	}

	/**
	 * @param skinId the skinId to set
	 */
	public void setSkinId(long skinId) {
		this.skinId = skinId;
	}

	/**
	 * @return the startDate
	 */
	public Date getStartDate() {
		return startDate;
	}

	/**
	 * @param startDate the startDate to set
	 */
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	/**
	 * @return the threshold
	 */
	public int getThreshold() {
		return threshold;
	}

	/**
	 * @param threshold the threshold to set
	 */
	public void setThreshold(int threshold) {
		this.threshold = threshold;
	}

	/**
	 * @return the timeCreated
	 */
	public Date getTimeCreated() {
		return timeCreated;
	}

	/**
	 * @return the timeCreated
	 */
	public String getTimeCreatedTxt() {
		return timeCreated.toString();
	}

	/**
	 * @param timeCreated the timeCreated to set
	 */
	public void setTimeCreated(Date timeCreated) {
		this.timeCreated = timeCreated;
	}

	/**
	 * @return the writerId
	 */
	public int getWriterId() {
		return writerId;
	}

	/**
	 * @param writerId the writerId to set
	 */
	public void setWriterId(int writerId) {
		this.writerId = writerId;
	}

	/**
	 * @return the populationTypeName
	 */
	public String getPopulationTypeName() {
		return CommonUtil.getMessage(populationTypeName,null);
	}

	/**
	 * @param populationTypeName the populationTypeName to set
	 */
	public void setPopulationTypeName(String populationTypeName) {
		this.populationTypeName = populationTypeName;
	}

	/**
	 * get skin key name by skin id
	 * @return skin key name
	 */
	public String getSkinName() {
		return CommonUtil.getMessage(ApplicationDataBase.getSkinById(skinId).getDisplayName(),null);
	}

	/**
	 * get language key name by language id
	 * @return language key name
	 */
	public String getLanguageName() {
		return ApplicationDataBase.getLanguage(languageId).getDisplayName();
	}

	/**
	 * get isActive txt if true 1 else 0
	 * @return 1 or 0
	 */
	public String getIsActiveTxt() {
		return isActive ? "1" : "0";
	}

	/**
	 * Get writer name
	 * @return
	 * @throws SQLException
	 */
	public String getWriterTxt() throws SQLException {
		return CommonUtil.getWriterName(writerId);
	}

	public String toString() {
	   String ls = System.getProperty("line.separator");
	   return ls + "population:" + ls +
        "id: " + id + ls +
        "name: " + name + ls +
        "balance amount: " + this.balanceAmount + ls +
        "startDate: " + startDate + ls +
        "endDate: " + endDate + ls +
    	"language Id: " + this.languageId + ls +
    	"last Invest Day: " + this.lastInvestDay + ls +
        "writerId: " + writerId + ls +
        "number Of Days: " + this.numberOfDays + ls +
        "number Of Minutes: " + this.numberOfMinutes + ls +
        "num Issues: " + this.numIssues + ls +
        "population Type Id: " + this.populationTypeId + ls +
        "refresh Time Period: " + this.refreshTimePeriod + ls +
        "skin Id: " + this.skinId + ls +
        "isActive: " + this.isActive + ls +
        "timeCreated: " + timeCreated + ls +
        "number Of Deposits " + numberOfDeposits;

	}

	/**
	 * @return the numberOfDeposits
	 */
	public int getNumberOfDeposits() {
		return numberOfDeposits;
	}

	/**
	 * @param numberOfDeposits the numberOfDeposits to set
	 */
	public void setNumberOfDeposits(int numberOfDeposits) {
		this.numberOfDeposits = numberOfDeposits;
	}

	/**
	 * @return the countPopulation
	 */
	public int getCountPopulation() {
		return countPopulation;
	}

	/**
	 * @param countPopulation the countPopulation to set
	 */
	public void setCountPopulation(int countPopulation) {
		this.countPopulation = countPopulation;
	}

	public int getPopulationTypePriority() {
		return populationTypePriority;
	}

	public void setPopulationTypePriority(int populationTypePriority) {
		this.populationTypePriority = populationTypePriority;
	}

	/**
	 * @return the timeControl
	 */
	public Date getTimeControl() {
		return timeControl;
	}

	/**
	 * @param timeControl the timeControl to set
	 */
	public void setTimeControl(Date timeControl) {
		this.timeControl = timeControl;
	}

}
