package il.co.etrader.bl_vos;

public class WriterMaxAssignment {

		int rank		= 0;
		int assignLimit	= 0;
		int statusId	= 0;
		int writerId	= 0;
		
		public WriterMaxAssignment() {
			
		}
		
		public WriterMaxAssignment(int rank, int assignLimit, int statusId, int writerId) {
			this.rank = rank;
			this.assignLimit = assignLimit;
			this.statusId = statusId;
			this.writerId = writerId;	
		}
		
		public int getRank() {
			return rank;
		}
		public void setRank(int rank) {
			this.rank = rank;
		}
		public int getAssignLimit() {
			return assignLimit;
		}
		public void setAssignLimit(int assignLimit) {
			this.assignLimit = assignLimit;
		}
		public int getStatusId() {
			return statusId;
		}
		public void setStatusId(int statusId) {
			this.statusId = statusId;
		}
		public int getWriterId() {
			return writerId;
		}
		public void setWriterId(int writerId) {
			this.writerId = writerId;
		}

		@Override
		public String toString() {
			return "WriterMaxAssignment [rank=" + rank + ", assignLimit="
					+ assignLimit + ", statusId=" + statusId + ", writerId="
					+ writerId + "]";
		}
		
		
	}
