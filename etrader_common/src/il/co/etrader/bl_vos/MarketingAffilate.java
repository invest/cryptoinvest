package il.co.etrader.bl_vos;


import il.co.etrader.util.CommonUtil;
import il.co.etrader.util.ConstantsBase;

import java.io.Serializable;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.faces.model.SelectItem;

import com.anyoption.common.managers.MarketingAffiliatesManagerBase;

/**
 * Marketing affilate class
 *
 * @author Kobi.
 */
public class MarketingAffilate implements Serializable {
    private static final long serialVersionUID = 1L;

    protected long id;
    protected String name;
    protected String key;
    protected long writerId;
    protected String writerName;
    protected String specialCode;

    private ArrayList<SelectItem> pixelsSi;
    protected MarketingAffiliatePixels affiliatePixels;  // affiliate pixels
    protected long pixelId;
    private String priorityId;
    protected boolean isAllowAffilate;
    private boolean active;
    private String siteLink;
    private long affiliateManagerId;

	/**
	 * @return the id
	 */
	public long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(long id) {
		this.id = id;
	}

	/**
	 * @return the key
	 */
	public String getKey() {
		return key;
	}

	/**
	 * @param key the key to set
	 */
	public void setKey(String key) {
		this.key = key;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the writerId
	 */
	public long getWriterId() {
		return writerId;
	}

	/**
	 * @param writerId the writerId to set
	 */
	public void setWriterId(long writerId) {
		this.writerId = writerId;
	}

	/**
	 * @return the writerName
	 */
	public String getWriterName() {
		return writerName;
	}

	/**
	 * @param writerName the writerName to set
	 */
	public void setWriterName(String writerName) {
		this.writerName = writerName;
	}

	public String getWriterTxt() throws SQLException {
		return CommonUtil.getWriterName(writerId);
	}

	/**
	 * @return the pixelId
	 */
	public long getPixelId() {
		return pixelId;
	}

	/**
	 * @param pixelId the pixelId to set
	 */
	public void setPixelId(long pixelId) {
		this.pixelId = pixelId;
	}

	/**
	 * @return the pixelsSi
	 */
	public ArrayList<SelectItem> getPixelsSi() {
		return pixelsSi;
	}

	/**
	 * @param pixelsSi the pixelsSi to set
	 */
	public void setPixelsSi(ArrayList<SelectItem> pixelsSi) {
		this.pixelsSi = pixelsSi;
	}
	
	

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	/**
	 * toString implementation.
	 */
	public String toString() {
        String ls = System.getProperty("line.separator");
        return ls + "Marketing Affiliate:" + ls +
            "id: " + id + ls +
            "name: " + name + ls +
	        "writerId: " + writerId + ls +
	        "key: " + key + ls;
    }

	/**
	 * @return the affiliatePixels
	 */
	public MarketingAffiliatePixels getAffiliatePixels() {
		return affiliatePixels;
	}

	/**
	 * @param affiliatePixels the affiliatePixels to set
	 */
	public void setAffiliatePixels(MarketingAffiliatePixels affiliatePixels) {
		this.affiliatePixels = affiliatePixels;
	}

    /**
     * @return the specialCode
     */
    public String getSpecialCode() {
        return specialCode;
    }

    /**
     * @param specialCode the specialCode to set
     */
    public void setSpecialCode(String specialCode) {
        this.specialCode = specialCode;
    }

    /**
	 * @return the priorityId
	 */
	public String getPriorityId() {
		return priorityId;
	}

	/**
	 * @param priorityId the priorityId to set
	 */
	public void setPriorityId(String priorityId) {
		this.priorityId = priorityId;
	}
	
	/**
	 * @return the isAllowAffilate
	 */
	public boolean isAllowAffilate() {
		return isAllowAffilate;
	}

	/**
	 * @param isAllowAffilate the isAllowAffilate to set
	 */
	public void setAllowAffilate(boolean isAllowAffilate) {
		this.isAllowAffilate = isAllowAffilate;
	}

	/**
	 * @return the AffiliatesPriorityText
	 */
	public String getAffiliatesPriorityText() {
		if (CommonUtil.isParameterEmptyOrNull(priorityId)){
			return "";
		}
		int priority = Integer.valueOf(priorityId);
		switch (priority) {
		case 1:
			return "LOW";
		case 2:
			return "MED";
		case 3:
			return "HIGH";
		default:
			break;
		}
		return "";
	}
	
	public String getActiveAffiliate() {
		if (active) {
			return "Active";
		} else {
			return "Inactive";
		}
	}
	
    /**
     * @return the siteLink
     */
    public String getSiteLink() {
        return siteLink;
    }

    /**
     * @param siteLink the siteLink to set
     */
    public void setSiteLink(String siteLink) {
        this.siteLink = siteLink;
    }

	/**
	 * @return the affiliateManagerId
	 */
	public long getAffiliateManagerId() {
		return affiliateManagerId;
	}

	/**
	 * @param affiliateManagerId the affiliateManagerId to set
	 */
	public void setAffiliateManagerId(long affiliateManagerId) {
		this.affiliateManagerId = affiliateManagerId;
	}

	/**
	 * Get affiliate manager name
	 * @return affiliate manager name as String
	 * @throws SQLException
	 */
	public String getAffiliateManagerName() throws SQLException {
		if (affiliateManagerId == MarketingAffiliatesManagerBase.AFFILIATE_MANAGER_ALL) {
			return ConstantsBase.EMPTY_STRING;
		}
		return CommonUtil.getWriterName(affiliateManagerId);
	}
}