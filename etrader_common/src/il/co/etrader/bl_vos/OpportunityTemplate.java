package il.co.etrader.bl_vos;

import il.co.etrader.bl_managers.InvestmentsManagerBase;
import il.co.etrader.util.CommonUtil;
import il.co.etrader.util.ConstantsBase;

import java.sql.SQLException;

import com.anyoption.common.beans.Opportunity;

/**
 * Opportunity template vo
 */
public class OpportunityTemplate implements java.io.Serializable {
	private static final long serialVersionUID = -8946105232238454851L;
	
	public static final int IS_FULL_DAY_NO = 0;
	public static final int IS_FULL_DAY_YES = 1;
	public static final int IS_FULL_DAY_SUNDAY = 2;

	private long id;
    private Long marketId;
    private long writerId;
    private String timeFirstInvest;
    private String timeEstClosing;
    private String timeLastInvest;
    private long oddsTypeId;
    private long opportunityTypeId;
    private boolean active;
    private long scheduled;
    private String timeZone;
    private int fullDay;
    private boolean halfDay;
    private boolean deductWinOdds;
    private Long oneTouchDecimalPoint;
    private String upDown;

    private String marketName;
    private String oddsTypeName;
    private String opTypeDesc;
    private boolean changeAll;
	private boolean changeAllScheduled;
	private boolean isOpenGraph;
	private String oddsGroup;
	private String oddsGroupValue;
	private double maxInvAmountCoeffPerUser;
	private byte[] halfDayWeek;
	private byte[] fullDayWeek;
	private String timeEstClosingHalfday;
	private String timeLastInvestHalfday;

    public OpportunityTemplate() {
    	timeFirstInvest = "";
    	timeEstClosing = "";
    	timeLastInvest = "";
    	marketId=new Long(0);
    }
    public long getId() {
        return id;
    }

    public String getScheduledTxt() {
    	return InvestmentsManagerBase.getScheduledTxt(scheduled);
    }

    public void setId(long id) {
        this.id = id;
    }

	public long getMarketId() {
		return marketId;
	}

	public void setMarketId(long marketId) {
		this.marketId = marketId;
	}

	public long getOddsTypeId() {
		return oddsTypeId;
	}

	public void setOddsTypeId(long oddsTypeId) {
		this.oddsTypeId = oddsTypeId;
	}

	public long getOpportunityTypeId() {
		return opportunityTypeId;
	}

	public void setOpportunityTypeId(long opportunityTypeId) {
		this.opportunityTypeId = opportunityTypeId;
	}

	public String getTimeEstClosing() {
		return timeEstClosing;
	}

	public void setTimeEstClosing(String timeEstClosing) {

		this.timeEstClosing = timeEstClosing;
	}

	public String getTimeFirstInvest() {
		return timeFirstInvest;
	}

	public void setTimeFirstInvest(String timeFirstInvest) {
		this.timeFirstInvest = timeFirstInvest;
	}

	public String getTimeLastInvest() {
		return timeLastInvest;
	}

	public void setTimeLastInvest(String timeLastInvest) {
		this.timeLastInvest = timeLastInvest;
	}

	public long getWriterId() {
		return writerId;
	}

	public void setWriterId(long writerId) {
		this.writerId = writerId;
	}

    public String getWriterName() throws SQLException {
		return CommonUtil.getWriterName(writerId);
	}
	public String getMarketName() {
		return marketName;
	}

	public void setMarketName(String marketName) {
		this.marketName = marketName;
	}
	public String getOddsTypeName() {
		return oddsTypeName;
	}
	public void setOddsTypeName(String oddsTypeName) {
		this.oddsTypeName = oddsTypeName;
	}

	public String getOpTypeDesc() {
		return opTypeDesc;
	}

	public void setOpTypeDesc(String opTypeDesc) {
		this.opTypeDesc = opTypeDesc;
	}

	public String getTimeFirstInvestMin() {
		String[] parts=timeFirstInvest.split(":");
		return parts[1];
	}
	public String getTimeLastInvestMin() {
		String[] parts=timeLastInvest.split(":");
		return parts[1];
	}
	public String getTimeEstClosingMin() {
		String[] parts=timeEstClosing.split(":");
		return parts[1];
	}
	public String getTimeFirstInvestHour() {
		String[] parts=timeFirstInvest.split(":");
		return parts[0];
	}
	public String getTimeLastInvestHour() {
		String[] parts=timeLastInvest.split(":");
		return parts[0];
	}
	public String getTimeEstClosingHour() {
		String[] parts=timeEstClosing.split(":");
		return parts[0];
	}

	public void setTimeFirstInvestMin(String min) {
		timeFirstInvest=timeFirstInvest.substring(0,3)+min;
	}
	public void setTimeLastInvestMin(String min) {
		timeLastInvest=timeLastInvest.substring(0,3)+min;
	}
	public void setTimeEstClosingMin(String min) {
		timeEstClosing=timeEstClosing.substring(0,3)+min;
	}
	public void setTimeFirstInvestHour(String h) {
		timeFirstInvest=h+timeFirstInvest.substring(2);
	}
	public void setTimeLastInvestHour(String h) {
		timeLastInvest=h+timeLastInvest.substring(2);
	}
	public void setTimeEstClosingHour(String h) {
		timeEstClosing=h+timeEstClosing.substring(2);
	}
	public boolean isActive() {
		return active;
	}
	public boolean getActive() {
		return active;
	}
	public void setActive(boolean active) {
		this.active = active;
	}
	public void setMarketId(Long marketId) {
		this.marketId = marketId;
	}
	public long getScheduled() {
		return scheduled;
	}
	public void setScheduled(long scheduled) {
		this.scheduled = scheduled;
	}

    public String getTimeZone() {
        return timeZone;
    }

    public void setTimeZone(String timeZone) {
        this.timeZone = timeZone;
    }

    public int getFullDay() {
        return fullDay;
    }

    public void setFullDay(int fullDay) {
        this.fullDay = fullDay;
    }

    public boolean isHalfDay() {
        return halfDay;
    }

    public void setHalfDay(boolean halfDay) {
        this.halfDay = halfDay;
    }

	public boolean isDeductWinOdds() {
		return deductWinOdds;
	}
	public void setDeductWinOdds(boolean deductWinOdds) {
		this.deductWinOdds = deductWinOdds;
	}
	/**
	 * Constructs a <code>String</code> with all attributes
	 * in name = value format.
	 *
	 * @return a <code>String</code> representation
	 * of this object.
	 */
	public String toString()
	{
	    final String TAB = " \n ";

	    String retValue = "";

	    retValue = "OpportunityTemplate ( "
	        + super.toString() + TAB
	        + "id = " + this.id + TAB
	        + "marketId = " + this.marketId + TAB
	        + "writerId = " + this.writerId + TAB
	        + "timeFirstInvest = " + this.timeFirstInvest + TAB
	        + "timeEstClosing = " + this.timeEstClosing + TAB
	        + "timeLastInvest = " + this.timeLastInvest + TAB
	        + "oddsTypeId = " + this.oddsTypeId + TAB
	        + "opportunityTypeId = " + this.opportunityTypeId + TAB
	        + "active = " + this.active + TAB
	        + "scheduled = " + this.scheduled + TAB
	        + "timeZone = " + this.timeZone + TAB
	        + "fullDay = " + this.fullDay + TAB
	        + "halfDay = " + this.halfDay + TAB
	        + "marketName = " + this.marketName + TAB
	        + "oddsTypeName = " + this.oddsTypeName + TAB
	        + "opTypeDesc = " + this.opTypeDesc + TAB
	        + "one Touch decimal Point = " + this.oneTouchDecimalPoint + TAB
	        + "up down = " + this.upDown + TAB
	        + "maxInvAmountCoeffPerUser = " + this.maxInvAmountCoeffPerUser + TAB
	        + " )";

	    return retValue;
	}
	/**
	 * @return the decimalPoint
	 */
	public Long getOneTouchDecimalPoint() {
		return oneTouchDecimalPoint;
	}
	/**
	 * @param decimalPoint the decimalPoint to set
	 */
	public void setOneTouchDecimalPoint(Long oneTouchdecimalPoint) {
		this.oneTouchDecimalPoint = oneTouchdecimalPoint;
	}
	/**
	 * @return the up_down
	 */
	public String getUpDown() {
		return upDown;
	}
	/**
	 * @param up_down the up_down to set
	 */
	public void setUpDown(String upDown) {
		this.upDown = upDown;
	}

	/**
	 * check if one touch is up or down
	 * @return up if one touch is up else down
	 */
	public String getUp_downDesc() {
		if (upDown.equalsIgnoreCase((String.valueOf(ConstantsBase.ONE_TOUCH_IS_DOWN))) && this.opportunityTypeId == Opportunity.TYPE_ONE_TOUCH) {
			return CommonUtil.getMessage("opportunities.one.touch.down", null);
		} else if (upDown.equalsIgnoreCase((String.valueOf(ConstantsBase.ONE_TOUCH_IS_UP))) && this.opportunityTypeId == Opportunity.TYPE_ONE_TOUCH) {
			return CommonUtil.getMessage("opportunities.one.touch.up", null);
		}
		return "";
	}
	public boolean isChangeAllScheduled() {
		return changeAllScheduled;
	}

	public void setChangeAllScheduled(boolean changeAllScheduled) {
		this.changeAllScheduled = changeAllScheduled;
	}

	public boolean isChangeAll() {
		return changeAll;
	}

	public void setChangeAll(boolean changeAll) {
		this.changeAll = changeAll;
	}

	/**
	 * @return the isOpenGraph
	 */
	public boolean isOpenGraph() {
		return isOpenGraph;
	}
	/**
	 * @param isOpenGraph the isOpenGraph to set
	 */
	public void setOpenGraph(boolean isOpenGraph) {
		this.isOpenGraph = isOpenGraph;
	}
	
	public String getOddsGroup() {
		return oddsGroup;
	}

	public void setOddsGroup(String oddsGroup) {
		this.oddsGroup = oddsGroup;
	}
	/**
	 * @return the oddsGroupValue
	 */
	public String getOddsGroupValue() {
		return oddsGroupValue;
	}
	/**
	 * @param oddsGroupValue the oddsGroupValue to set
	 */
	public void setOddsGroupValue(String oddsGroupValue) {
		this.oddsGroupValue = oddsGroupValue;
	}

	public double getMaxInvAmountCoeffPerUser() {
		return maxInvAmountCoeffPerUser;
	}
	
	public void setMaxInvAmountCoeffPerUser(double maxInvAmountCoeffPerUser) {
		this.maxInvAmountCoeffPerUser = maxInvAmountCoeffPerUser;
	}
	
	public byte[] getHalfDayWeek() {
		return halfDayWeek;
	}
	
	public void setHalfDayWeek(byte[] halfDayWeek) {
		this.halfDayWeek = halfDayWeek;
	}
	
	public byte[] getFullDayWeek() {
		return fullDayWeek;
	}
	
	public void setFullDayWeek(byte[] fullDayWeek) {
		this.fullDayWeek = fullDayWeek;
	}
	
	public String getTimeEstClosingHalfday() {
		return timeEstClosingHalfday;
	}
	
	public void setTimeEstClosingHalfday(String timeEstClosingHalfday) {
		this.timeEstClosingHalfday = timeEstClosingHalfday;
	}
	
	public String getTimeLastInvestHalfday() {
		return timeLastInvestHalfday;
	}
	
	public void setTimeLastInvestHalfday(String timeLastInvestHalfday) {
		this.timeLastInvestHalfday = timeLastInvestHalfday;
	}
}