package il.co.etrader.bl_vos;

import java.io.Serializable;

public class clearingLimit implements Serializable{

	/**
	 * @author oshikl
	 */
	private static final long serialVersionUID = 5301737404143853087L;
	
	private long id;
	private long limitType;
	private String limitName;
	private long clearingProviderGroupId;
	private boolean tbiAnyoption;
	private boolean wireCard;
	private boolean inatec;
	private boolean wireCard3D;
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public long getLimitType() {
		return limitType;
	}
	public void setLimitType(long limitType) {
		this.limitType = limitType;
	}
	public String getLimitName() {
		return limitName;
	}
	public void setLimitName(String limitName) {
		this.limitName = limitName;
	}
	public long getClearingProviderGroupId() {
		return clearingProviderGroupId;
	}
	public void setClearingProviderGroupId(long clearingProviderGroupId) {
		this.clearingProviderGroupId = clearingProviderGroupId;
	}
	public boolean isTbiAnyoption() {
		return tbiAnyoption;
	}
	public void setTbiAnyoption(boolean tbiAnyoption) {
		this.tbiAnyoption = tbiAnyoption;
	}
	public boolean isWireCard() {
		return wireCard;
	}
	public void setWireCard(boolean wireCard) {
		this.wireCard = wireCard;
	}
	public boolean isInatec() {
		return inatec;
	}
	public void setInatec(boolean inatec) {
		this.inatec = inatec;
	}
	public boolean isWireCard3D() {
		return wireCard3D;
	}
	public void setWireCard3D(boolean wireCard3D) {
		this.wireCard3D = wireCard3D;
	}
	
}
