//package il.co.etrader.bl_vos;
//
//import java.util.Date;
//
//public class MarketingTrackingCookieDynamic implements java.io.Serializable{
//
//	private static final long serialVersionUID = -5149043541284386194L;
//	
//	private String http;
//	private String cd;
//	private Date td;
//	private String dp;
//	/**
//	 * @return the http
//	 */
//	public String getHttp() {
//		return http;
//	}
//	/**
//	 * @param http the http to set
//	 */
//	public void setHttp(String http) {
//		this.http = http;
//	}
//	/**
//	 * @return the cd
//	 */
//	public String getCd() {
//		return cd;
//	}
//	/**
//	 * @param cd the cd to set
//	 */
//	public void setCd(String cd) {
//		this.cd = cd;
//	}
//	/**
//	 * @return the td
//	 */
//	public Date getTd() {
//		return td;
//	}
//	/**
//	 * @param td the td to set
//	 */
//	public void setTd(Date td) {
//		this.td = td;
//	}
//	/**
//	 * @return the dp
//	 */
//	public String getDp() {
//		return dp;
//	}
//	/**
//	 * @param dp the dp to set
//	 */
//	public void setDp(String dp) {
//		this.dp = dp;
//	}
//	
//
//}
