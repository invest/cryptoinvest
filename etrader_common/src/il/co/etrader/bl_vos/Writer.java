package il.co.etrader.bl_vos;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import com.anyoption.common.bl_vos.PopulationEntryBase;

public class Writer extends WriterBase implements java.io.Serializable{
    /**
	 *
	 */
	private static final long serialVersionUID = -2251370492510271050L;
	
	//Writers
    public static final int WRITER_ID_AUTO = 0;
    public static final int WRITER_ID_WEB = 1;
    public static final int WRITER_ID_MOBILE = 200;
    public static final int WRITER_ID_VOICE_SPIN = 27000;

//	private long id;
//  private String userName;
    private String password;
    private String firstName;
    private String lastName;
    private String street;
    private String streetNo;
    private String cityId;
    private String zipCode;
    private String email;
    private String comments;
    private Date timeBirthDate;
    private String mobilePhone;
    private String landLinePhone;
    private ArrayList<Integer> skins;
    private int isActive;
    private long groupId;
    private boolean isSupportEnable;
    //private long deptId;
    private int sales_type;
    private String nickNameFirst;
    private String nickNameLast;
    private long salesTypeDepartmentId;

    // rep data
    private long generalAssignedRecords;
    private long trackingAssignedRecords;
    private long callBacksAssignedRecords;
    private boolean toCollect;
    private long allAssignedRecords;
    private long calledReachedRecords;
    private long calledNotReachedRecords;
    private long notCalleRecords;
    private long callBacksRecords;
    private long reassignToRepId;

    private long onlineDepositSkinId;  // recognize deposit user skin for Tv screens
    private String onlineDepositAmount;
    private int onlineDepositType;
    private long screenType;
    private boolean isPasswordReset;
    
    private HashMap<Long, WritersSkin> writerSkins;
    // Key = rank id
    private HashMap<Long, RankData> ranksData; //auto assignment job
    
    //private long roleId;
    
    
    public Writer() {
    	writerSkins = new HashMap<Long, WritersSkin>(); 
    	ranksData = new HashMap<Long, RankData>();
    }
    
	/**
	 * @return the screenType
	 */
	public long getScreenType() {
		return screenType;
	}
	/**
	 * @param screenType the screenType to set
	 */
	public void setScreenType(long screenType) {
		this.screenType = screenType;
	}
	/**
	 * @return the isSupportEnable
	 */
	public boolean isSupportEnable() {
		return isSupportEnable;
	}
	/**
	 * @param isSupportEnable the isSupportEnable to set
	 */
	public void setSupportEnable(boolean isSupportEnable) {
		this.isSupportEnable = isSupportEnable;
	}
	public String getStreetNo() {
		return streetNo;
	}
	public void setStreetNo(String streetNo) {
		this.streetNo = streetNo;
	}
	public String getCityId() {
		return cityId;
	}
	public void setCityId(String cityId) {
		this.cityId = cityId;
	}
	public String getComments() {
		return comments;
	}
	public void setComments(String comments) {
		this.comments = comments;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
//	public long getId() {
//		return id;
//	}
//	public void setId(long id) {
//		this.id = id;
//	}
	public int getIsActive() {
		return isActive;
	}
	public void setIsActive(int isActive) {
		this.isActive = isActive;
	}
	public String getLandLinePhone() {
		return landLinePhone;
	}
	public void setLandLinePhone(String landLinePhone) {
		this.landLinePhone = landLinePhone;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getMobilePhone() {
		return mobilePhone;
	}
	public void setMobilePhone(String mobilePhone) {
		this.mobilePhone = mobilePhone;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getStreet() {
		return street;
	}
	public void setStreet(String street) {
		this.street = street;
	}
	public Date getTimeBirthDate() {
		return timeBirthDate;
	}
	public void setTimeBirthDate(Date timeBirthDate) {
		this.timeBirthDate = timeBirthDate;
	}
//	public String getUserName() {
//		return userName;
//	}
//	public void setUserName(String userName) {
//		this.userName = userName;
//	}
	public String getZipCode() {
		return zipCode;
	}
	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}

    public String getLandLinePhonePrefix() {
    	if (landLinePhone==null || landLinePhone.equals(""))
    		return "";

    	return landLinePhone.substring(0,3);
    }
    public String getLandLinePhoneSuffix() {
    	if (landLinePhone==null || landLinePhone.equals(""))
    		return "";

    	if (landLinePhone.length()>7)
    		return landLinePhone.substring(3);

    	return landLinePhone;
    }
    public String getMobilePhonePrefix() {
    	if (mobilePhone==null || mobilePhone.equals(""))
    		return "";

    	return mobilePhone.substring(0,3);
    }

    public String getMobilePhoneSuffix() {
    	if (mobilePhone==null || mobilePhone.equals(""))
    		return "";

    	if (mobilePhone.length()>7)
    		return mobilePhone.substring(3);

    	return mobilePhone;
    }

    public void setLandLinePhonePrefix(String s) {
    	landLinePhone=s+getLandLinePhoneSuffix();
    }
    public void setLandLinePhoneSuffix(String s) {
    	landLinePhone=getLandLinePhonePrefix()+s;
    }
    public void setMobilePhonePrefix(String s) {
    	mobilePhone=s+getMobilePhoneSuffix();
    }
    public void setMobilePhoneSuffix(String s) {
    	mobilePhone=getMobilePhonePrefix()+s;
    }

	/**
	 * Constructs a <code>String</code> with all attributes
	 * in name = value format.
	 *
	 * @return a <code>String</code> representation
	 * of this object.
	 */
	public String toString()
	{
	    final String TAB = " \n ";

	    String retValue = "";

	    retValue = "Writer ( "
	        + super.toString() + TAB
	        + "password = " + "*****" + TAB
	        + "firstName = " + this.firstName + TAB
	        + "lastName = " + this.lastName + TAB
	        + "nickNameFirst " + this.nickNameFirst + TAB
	        + "nickNameLast " + this.nickNameLast + TAB
	        + "street = " + this.street + TAB
	        + "streetNo = " + this.streetNo + TAB
	        + "cityId = " + this.cityId + TAB
	        + "zipCode = " + this.zipCode + TAB
	        + "email = " + this.email + TAB
	        + "comments = " + this.comments + TAB
	        + "timeBirthDate = " + this.timeBirthDate + TAB
	        + "mobilePhone = " + this.mobilePhone + TAB
	        + "landLinePhone = " + this.landLinePhone + TAB
	        + "isActive = " + this.isActive + TAB
	        + "salesTypeDepartmentId = " + this.salesTypeDepartmentId + TAB
	        + " )";

	    return retValue;
	}
	public ArrayList<Integer> getSkins() {
		return skins;
	}

	public void setSkins(ArrayList<Integer> skins) {
		this.skins = skins;
	}

	/**
	 * @return the groupId
	 */
	public long getGroupId() {
		return groupId;
	}

	/**
	 * @param groupId the groupId to set
	 */
	public void setGroupId(long groupId) {
		this.groupId = groupId;
	}

	/**
	 * @return the callBacksAssignedRecords
	 */
	public long getCallBacksAssignedRecords() {
		return callBacksAssignedRecords;
	}

	/**
	 * @param callBacksAssignedRecords the callBacksAssignedRecords to set
	 */
	public void setCallBacksAssignedRecords(long callBacksAssignedRecords) {
		this.callBacksAssignedRecords = callBacksAssignedRecords;
	}

	/**
	 * @return the generalAssignedRecords
	 */
	public long getGeneralAssignedRecords() {
		return generalAssignedRecords;
	}

	/**
	 * @param generalAssignedRecords the generalAssignedRecords to set
	 */
	public void setGeneralAssignedRecords(long generalAssignedRecords) {
		this.generalAssignedRecords = generalAssignedRecords;
	}

	/**
	 * @return the trackingAssignedRecords
	 */
	public long getTrackingAssignedRecords() {
		return trackingAssignedRecords;
	}

	/**
	 * @param trackingAssignedRecords the trackingAssignedRecords to set
	 */
	public void setTrackingAssignedRecords(long trackingAssignedRecords) {
		this.trackingAssignedRecords = trackingAssignedRecords;
	}

	/**
	 * @return the toCollect
	 */
	public boolean isToCollect() {
		return toCollect;
	}

	/**
	 * @param toCollect the toCollect to set
	 */
	public void setToCollect(boolean toCollect) {
		this.toCollect = toCollect;
	}

	/**
	 * @return the allAssignedRecords
	 */
	public long getAllAssignedRecords() {
		return allAssignedRecords;
	}

	/**
	 * @param allAssignedRecords the allAssignedRecords to set
	 */
	public void setAllAssignedRecords(long allAssignedRecords) {
		this.allAssignedRecords = allAssignedRecords;
	}

	/**
	 * @return the callBacksRecords
	 */
	public long getCallBacksRecords() {
		return callBacksRecords;
	}

	/**
	 * @param callBacksRecords the callBacksRecords to set
	 */
	public void setCallBacksRecords(long callBacksRecords) {
		this.callBacksRecords = callBacksRecords;
	}

	/**
	 * @return the calledNotReachedRecords
	 */
	public long getCalledNotReachedRecords() {
		return calledNotReachedRecords;
	}

	/**
	 * @param calledNotReachedRecords the calledNotReachedRecords to set
	 */
	public void setCalledNotReachedRecords(long calledNotReachedRecords) {
		this.calledNotReachedRecords = calledNotReachedRecords;
	}

	/**
	 * @return the calledReachedRecords
	 */
	public long getCalledReachedRecords() {
		return calledReachedRecords;
	}

	/**
	 * @param calledReachedRecords the calledReachedRecords to set
	 */
	public void setCalledReachedRecords(long calledReachedRecords) {
		this.calledReachedRecords = calledReachedRecords;
	}

	/**
	 * @return the notCalleRecords
	 */
	public long getNotCalleRecords() {
		return notCalleRecords;
	}

	/**
	 * @param notCalleRecords the notCalleRecords to set
	 */
	public void setNotCalleRecords(long notCalleRecords) {
		this.notCalleRecords = notCalleRecords;
	}

	/**
	 * @return the onlineDepositSkinId
	 */
	public long getOnlineDepositSkinId() {
		return onlineDepositSkinId;
	}

	/**
	 * @param onlineDepositSkinId the onlineDepositSkinId to set
	 */
	public void setOnlineDepositSkinId(long onlineDepositSkinId) {
		this.onlineDepositSkinId = onlineDepositSkinId;
	}

	/**
	 * @return the onlineDepositAmount
	 */
	public String getOnlineDepositAmount() {
		return onlineDepositAmount;
	}

	/**
	 * @param onlineDepositAmount the onlineDepositAmount to set
	 */
	public void setOnlineDepositAmount(String onlineDepositAmount) {
		this.onlineDepositAmount = onlineDepositAmount;
	}
//	/**
//	 * @return the deptId
//	 */
//	public long getDeptId() {
//		return deptId;
//	}
//	/**
//	 * @param deptId the deptId to set
//	 */
//	public void setDeptId(long deptId) {
//		this.deptId = deptId;
//	}
	/**
	 * @return the sales_type
	 */
	public int getSales_type() {
		return sales_type;
	}
	/**
	 * @param sales_type the sales_type to set
	 */
	public void setSales_type(int sales_type) {
		this.sales_type = sales_type;
	}
	/**
	 * @return the onlineDepositType
	 */
	public int getOnlineDepositType() {
		return onlineDepositType;
	}
	/**
	 * @param onlineDepositType the onlineDepositType to set
	 */
	public void setOnlineDepositType(int onlineDepositType) {
		this.onlineDepositType = onlineDepositType;
	}
	/**
	 * @return the nickNameFirst
	 */
	public String getNickNameFirst() {
		return nickNameFirst;
	}
	/**
	 * @param nickNameFirst the nickNameFirst to set
	 */
	public void setNickNameFirst(String nickNameFirst) {
		this.nickNameFirst = nickNameFirst;
	}
	/**
	 * @return the nickNameLast
	 */
	public String getNickNameLast() {
		return nickNameLast;
	}
	/**
	 * @param nickNameLast the nickNameLast to set
	 */
	public void setNickNameLast(String nickNameLast) {
		this.nickNameLast = nickNameLast;
	}
	/**
	 * @return the ranksData
	 */
	public HashMap<Long, RankData> getRanksData() {
		return ranksData;
	}
	/**
	 * @param ranksData the ranksData to set
	 */
	public void setRanksData(HashMap<Long, RankData> ranksData) {
		this.ranksData = ranksData;
	}

	/**
	 * @return the writerSkins
	 */
	public HashMap<Long, WritersSkin> getWriterSkins() {
		return writerSkins;
	}

	/**
	 * @param writerSkins the writerSkins to set
	 */
	public void setWriterSkins(HashMap<Long, WritersSkin> writerSkins) {
		this.writerSkins = writerSkins;
	}
	
	/**
	 * @return the salesTypeDepartmentId
	 */
	public long getSalesTypeDepartmentId() {
		return salesTypeDepartmentId;
	}

	/**
	 * @param salesTypeDepartmentId the salesTypeDepartmentId to set
	 */
	public void setSalesTypeDepartmentId(long salesTypeDepartmentId) {
		this.salesTypeDepartmentId = salesTypeDepartmentId;
	}
	
	public boolean reachedAssignLimit(long skinId) {
		long sumAssign = 0;
		for (Long iter : writerSkins.get(skinId).getRanksData().keySet()) {
			Long rankId = iter;
			writerSkins.get(skinId).getRanksData().get(rankId);
			for (Long iter2 : writerSkins.get(skinId).getRanksData().get(rankId).getStatusData().keySet()) {
				Long statusId = iter2;
				ArrayList<PopulationEntryBase> pebArray = writerSkins.get(skinId).getRanksData().get(rankId).getStatusData().get(statusId).getStatusArray();
				if (pebArray != null) {
					sumAssign += pebArray.size();
				}
			}
		}
		long assignLimit = writerSkins.get(skinId).getAssignLimit();
		if (assignLimit <= sumAssign) {
			return true;
		}
		return false;
	}
	

	public boolean isCanAssign(long skinId, long rankId, long statusId) {
		double rankLimitPercentage = writerSkins.get(skinId).getRankStatusLimits().get(rankId).getLimit();
		double statusLimitPercentage = writerSkins.get(skinId).getRankStatusLimits().get(rankId).getStatusLimit().get(statusId);
		long assignLimit = writerSkins.get(skinId).getAssignLimit();
		long limit = Math.round(assignLimit * rankLimitPercentage * statusLimitPercentage);
		long numAssignForStatus = 0;
		try {
			numAssignForStatus = writerSkins.get(skinId).getRanksData().get(rankId).getStatusData().get(statusId).getStatusArray().size();
		} catch (Exception e) {

		}	
		if (numAssignForStatus < limit && !reachedAssignLimit(skinId)) { // check if we didn't reach by rank and status AND we didn't reach to total assign 
			return true;
		}
		return false;
	}
	
	public double assignsDivLimit(long skinId, long rankId, long statusId) {
		double rankLimitPercentage = writerSkins.get(skinId).getRankStatusLimits().get(rankId).getLimit();
		double statusLimitPercentage = writerSkins.get(skinId).getRankStatusLimits().get(rankId).getStatusLimit().get(statusId);
		long assignLimit = writerSkins.get(skinId).getAssignLimit();
		double limit = Math.max(Math.round(assignLimit * rankLimitPercentage * statusLimitPercentage), 1);
		double numAssignForStatus = 0;
		try {
			numAssignForStatus = writerSkins.get(skinId).getRanksData().get(rankId).getStatusData().get(statusId).getStatusArray().size();
		} catch (Exception e) {

		}		
		return numAssignForStatus / limit;
	}

	/**
	 * @return the reassignToRepId
	 */
	public long getReassignToRepId() {
		return reassignToRepId;
	}

	/**
	 * @param reassignToRepId the reassignToRepId to set
	 */
	public void setReassignToRepId(long reassignToRepId) {
		this.reassignToRepId = reassignToRepId;
	}

	public boolean isPasswordReset() {
		return isPasswordReset;
	}

	public void setPasswordReset(boolean isPasswordReset) {
		this.isPasswordReset = isPasswordReset;
	}

//	public long getRoleId() {
//		return roleId;
//	}
//
//	public void setRoleId(long roleId) {
//		this.roleId = roleId;
//	}
	

	
}
