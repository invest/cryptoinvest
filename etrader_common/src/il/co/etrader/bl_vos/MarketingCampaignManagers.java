package il.co.etrader.bl_vos;

import java.io.Serializable;

public class MarketingCampaignManagers implements Serializable{
	private static final long serialVersionUID = 1L;
	
	private long campaignManagerId;
	private String campaignManagerUserame;
	/**
	 * @return the campaignManagerId
	 */
	public long getCampaignManagerId() {
		return campaignManagerId;
	}
	/**
	 * @param campaignManagerId the campaignManagerId to set
	 */
	public void setCampaignManagerId(long campaignManagerId) {
		this.campaignManagerId = campaignManagerId;
	}
	/**
	 * @return the campaignManagerUserame
	 */
	public String getCampaignManagerUserame() {
		return campaignManagerUserame;
	}
	/**
	 * @param campaignManagerUserame the campaignManagerUserame to set
	 */
	public void setCampaignManagerUserame(String campaignManagerUserame) {
		this.campaignManagerUserame = campaignManagerUserame;
	}

}
