//package il.co.etrader.bl_vos;
//
//public class PaymentMethodCountry implements java.io.Serializable {
//	private static final long serialVersionUID = 1L;
//
//	private long id;
//	private long countryId;
//	private long paymentMethodId;
//	private boolean isExcluded;
//
//
//	/**
//	 * @return the id
//	 */
//	public long getId() {
//		return id;
//	}
//
//	/**
//	 * @param id the id to set
//	 */
//	public void setId(long id) {
//		this.id = id;
//	}
//
//	/**
//	 * @return the isExcluded
//	 */
//	public boolean isExcluded() {
//		return isExcluded;
//	}
//
//	/**
//	 * @param isExcluded the isExcluded to set
//	 */
//	public void setExcluded(boolean isExcluded) {
//		this.isExcluded = isExcluded;
//	}
//
//	/**
//	 * @return the countryId
//	 */
//	public long getCountryId() {
//		return countryId;
//	}
//
//	/**
//	 * @param countryId the countryId to set
//	 */
//	public void setCountryId(long countryId) {
//		this.countryId = countryId;
//	}
//
//	/**
//	 * @return the paymentMethodId
//	 */
//	public long getPaymentMethodId() {
//		return paymentMethodId;
//	}
//
//	/**
//	 * @param paymentMethodId the paymentMethodId to set
//	 */
//	public void setPaymentMethodId(long paymentMethodId) {
//		this.paymentMethodId = paymentMethodId;
//	}
//}
