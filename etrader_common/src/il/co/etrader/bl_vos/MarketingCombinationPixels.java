package il.co.etrader.bl_vos;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Marketing combination pixels class
 *
 * @author Kobi.
 */
public class MarketingCombinationPixels implements Serializable {
    private static final long serialVersionUID = 1L;

    protected long id;
    protected long combId;
    protected ArrayList<MarketingPixel> pixels;

	/**
	 * @return the combId
	 */
	public long getCombId() {
		return combId;
	}

	/**
	 * @param combId the combId to set
	 */
	public void setCombId(long combId) {
		this.combId = combId;
	}

	/**
	 * @return the id
	 */
	public long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(long id) {
		this.id = id;
	}

	/**
	 * @return the pixels
	 */
	public ArrayList<MarketingPixel> getPixels() {
		return pixels;
	}

	/**
	 * @param pixels the pixels to set
	 */
	public void setPixels(ArrayList<MarketingPixel> pixels) {
		this.pixels = pixels;
	}

}