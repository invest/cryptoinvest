package il.co.etrader.bl_vos;

import org.apache.myfaces.custom.fileupload.UploadedFile;

public class Template extends com.anyoption.common.bl_vos.Template{

	private static final long serialVersionUID = 9014110416302629423L;
	private UploadedFile file;

	public UploadedFile getFile() {
		return file;
	}

	public void setFile(UploadedFile file) {
		this.file = file;
	}

	/**
	 * Constructs a <code>String</code> with all attributes
	 * in name = value format.
	 *
	 * @return a <code>String</code> representation
	 * of this object.
	 */
	public String toString() {
	    final String TAB = " \n ";

	    String retValue = "";

	    retValue = "Template ( "
	        + super.toString() + TAB
	        + "file = " + this.file + TAB
	        + " )";

	    return retValue;
	}
}
