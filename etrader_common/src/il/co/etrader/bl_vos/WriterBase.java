package il.co.etrader.bl_vos;

public class WriterBase implements java.io.Serializable{
    /**
	 *
	 */
	private static final long serialVersionUID = -2251370492510271050L;

    public WriterBase(long id, String userName, long deptId, long roleId, String departmentName, 
    		String utcOffset, boolean isMultitude) {
		super();
		this.id = id;
		this.userName = userName;
		this.deptId = deptId;
		this.roleId = roleId;
		this.departmentName = departmentName;
		this.utcOffset = utcOffset;
		this.isMultitude = isMultitude;
	}
    
    public WriterBase() {		
	}
    
	private long id;
	private String userName;
    private long deptId;
    private long roleId;
    private String departmentName;
    private String utcOffset;
   
    //isMultitude for permission screens
    private boolean isMultitude;
	
    public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public long getDeptId() {
		return deptId;
	}
	public void setDeptId(long deptId) {
		this.deptId = deptId;
	}
	public long getRoleId() {
		return roleId;
	}
	public void setRoleId(long roleId) {
		this.roleId = roleId;
	}
	
	public String toString()
	{
	    final String TAB = " \n ";

	    String retValue = "";

	    retValue = super.toString() + TAB
	        + "id = " + this.id + TAB
	        + "userName = " + this.userName + TAB
	        + "deptId = " + this.deptId + TAB
	        + "roleId = " + this.roleId + TAB;

	    return retValue;
	}
	public String getDepartmentName() {
		return departmentName;
	}
	public void setDepartmentName(String departmentName) {
		this.departmentName = departmentName;
	}
	public boolean isMultitude() {
		return isMultitude;
	}
	public void setMultitude(boolean isMultitude) {
		this.isMultitude = isMultitude;
	}
	public String getUtcOffset() {
		return utcOffset;
	}
	public void setUtcOffset(String utcOffset) {
		this.utcOffset = utcOffset;
	}
    	
}
