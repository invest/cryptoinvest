package il.co.etrader.bl_vos;

import java.io.Serializable;

/**
 * Marketing affiliate pixels class
 *
 * @author Eran.
 */
public class MarketingSubAffiliatePixels extends MarketingAffiliatePixels implements Serializable {
    private static final long serialVersionUID = 1L;

    protected long subAffiliateId;

	/**
	 * @return the subAffiliateId
	 */
	public long getSubAffiliateId() {
		return subAffiliateId;
	}

	/**
	 * @param subAffiliateId the subAffiliateId to set
	 */
	public void setSubAffiliateId(long subAffiliateId) {
		this.subAffiliateId = subAffiliateId;
	}


}