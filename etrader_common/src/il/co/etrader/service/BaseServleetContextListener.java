package il.co.etrader.service;

import il.co.etrader.util.LogLevelManager;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.apache.log4j.Logger;

public class BaseServleetContextListener implements ServletContextListener {
    private static Logger log = Logger.getLogger(BaseServleetContextListener.class);
    
//    protected String initialContextFactory;
//    protected String providerURL;
//    protected String connectionFactory;
//    protected String topic;
//    protected String queue;
//    protected String msgPoolSizeStr;
//    protected int msgPoolSize;
//    protected int recoveryPause;
    protected String jmxName;
    protected String jmxLogManagerName;
    protected LogLevelManager logLevelManager;

    /**
     * Notification that the application is started
     *
     * @param event
     */
    public void contextInitialized(ServletContextEvent event) {
        ServletContext servletContext = event.getServletContext();
//        initialContextFactory = servletContext.getInitParameter("il.co.etrader.service.level.INITIAL_CONTEXT_FACTORY");
//        providerURL = servletContext.getInitParameter("il.co.etrader.service.level.PROVIDER_URL");
//        connectionFactory = servletContext.getInitParameter("il.co.etrader.service.level.CONNECTION_FACTORY_NAME");
//        topic = servletContext.getInitParameter("il.co.etrader.service.level.TOPIC_NAME");
//        queue = servletContext.getInitParameter("il.co.etrader.service.level.QUEUE_NAME");
//        msgPoolSizeStr = servletContext.getInitParameter("il.co.etrader.service.level.MSG_POOL_SIZE");
//        String recoveryPauseStr = servletContext.getInitParameter("il.co.etrader.service.level.RECOVERY_PAUSE");
//        msgPoolSize = 15;
//        try {
//            msgPoolSize = Integer.parseInt(msgPoolSizeStr);
//        } catch (Throwable t) {
//            log.warn("Can't parse msgPoolSize. Set to 15.", t);
//        }
//        recoveryPause = 2000;
//        try {
//            recoveryPause = Integer.parseInt(recoveryPauseStr);
//        } catch (Throwable t) {
//            log.warn("Can't parse recoveryPause. Set to 2000.", t);
//        }
        jmxName = servletContext.getInitParameter("il.co.etrader.service.level.JMX_NAME");
        jmxLogManagerName = servletContext.getInitParameter("il.co.etrader.log4j.JMX_NAME");
        if (log.isDebugEnabled()) {
            String ls = System.getProperty("line.separator");
            log.debug(ls + "Configuration:" + ls +
//                    "initialContextFactory: " + initialContextFactory + ls +
//                    "providerURL: " + providerURL + ls +
//                    "connectionFactory: " + connectionFactory + ls +
//                    "topic: " + topic + ls +
//                    "queue: " + queue + ls +
//                    "msgPoolSize: " + msgPoolSize + ls +
//                    "recoveryPause: " + recoveryPause + ls +
                    "jmxName: " + jmxName + ls +
                    "jmxLogManagerName: " + jmxLogManagerName + ls);
        }
        try {
            logLevelManager = new LogLevelManager();
            logLevelManager.registerJMX(jmxLogManagerName);
        } catch (Exception e) {
            log.error("Can't register LogLevelManager.", e);
        }
    }

    /**
     * Notification that the application is about to shutdown
     *
     * @param event
     */
    public void contextDestroyed(ServletContextEvent event) {
//        try {
//            ServletContext context = event.getServletContext();
//            LevelsCache levelsCache = (LevelsCache) context.getAttribute("levelsCache");
//            levelsCache.unregisterJMX();
//            levelsCache.close();
//            if (log.isInfoEnabled()) {
//                log.info(levelsCache.getClass().getName() + " unbound from JNDI");
//            }
//        } catch (Throwable t) {
//            log.error("Failed to unbind LevelsCache from JNDI", t);
//        }
        try {
            logLevelManager.unregisterJMX();
        } catch (Exception e) {
            log.error("Can't unregister LogLevelManager from JNDI.", e);
        }
    }
}