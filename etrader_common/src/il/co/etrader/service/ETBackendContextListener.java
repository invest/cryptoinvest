package il.co.etrader.service;

import javax.servlet.ServletContextEvent;

/**
 * Handle the LevelsCache init and shutdown. We need the levels cache so we can
 * send commands to the service.
 *
 * @author Tony
 */
public class ETBackendContextListener extends WabServletContextListener {
//    private static Logger log = Logger.getLogger(ETBackendContextListener.class);

    /**
     * Notification that the application is started
     *
     * @param event
     */
    public void contextInitialized(ServletContextEvent event) {
        super.contextInitialized(event);
//    	ServletContext servletContext = event.getServletContext();
//    	try {
//            LevelsCache levelsCache = new LevelsCache(initialContextFactory, providerURL, connectionFactory, queue,  msgPoolSize, recoveryPause);
//            servletContext.setAttribute("levelsCache", levelsCache);
//            if (log.isInfoEnabled()) {
//                log.info("LevelsCahce bound to the context");
//            }
//        } catch (Throwable t) {
//            log.error("Failed to init LevelsCache", t);
//        }
    }

    /**
     * Notification that the application is about to shutdown
     *
     * @param event
     */
    public void contextDestroyed(ServletContextEvent event) {
//        try {
//        	ServletContext context = event.getServletContext();
//            LevelsCache levelsCache = (LevelsCache) context.getAttribute("levelsCache");
//            levelsCache.close();
//            if (log.isInfoEnabled()) {
//                log.info("LevelsCache closed.");
//            }
//        } catch (Throwable t) {
//            log.error("Failed to close LevelsCache.", t);
//        }
    }
}