package il.co.etrader.issueActions;

import il.co.etrader.bl_managers.ApplicationDataBase;
import il.co.etrader.bl_managers.PopulationsManagerBase;
import il.co.etrader.bl_vos.UserBase;
import il.co.etrader.dao_managers.IssuesDAOBase;
import il.co.etrader.dao_managers.PopulationsDAOBase;
import il.co.etrader.util.CommonUtil;
import il.co.etrader.util.ConstantsBase;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Date;

import javax.faces.context.FacesContext;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.Issue;
import com.anyoption.common.beans.IssueAction;

public class ActionNoConversationSalesCallHandler extends ActionHandlerBase{

	private static final Logger log = Logger.getLogger(ActionNoConversationSalesCallHandler.class);

	/**
	 *  validateAction event handler implementation
	 */
	@Override
	public boolean beforeInsertAction(Connection conn,UserBase user, IssueAction action, FacesContext context, int screenId) throws ActionHandlersException{
		long userId = user.getId();
		int statusId = 0;

		if (null == popUserEntry){
			try{
				popUserEntry = com.anyoption.common.daos.PopulationsDAOBase.getPopulationUserByUserId(conn, userId);
			}catch (SQLException e) {
				log.error("Error in insertAction fo, user: " + user.getId() , e);
				return false;
			}
		}

		if (null != popUserEntry){

			long popUserId = popUserEntry.getPopulationUserId();

			if (screenId == ConstantsBase.SCREEN_SALEM || screenId == ConstantsBase.SCREEN_SALE) {
				if (action.isCallBack()){
					//Unassigned CB that more than a week
					if(isCbMoreThanWeek(action, CommonUtil.getDateTimeFormaByTz(new Date(), action.getCallBackTimeOffset()), user)) {
						statusId = PopulationsManagerBase.POP_ENT_HIS_STATUS_CALLBACK_CANCEL_ASSIGN;
					} else {
						statusId = PopulationsManagerBase.POP_ENT_HIS_STATUS_CALLBACK;
					}
				} else if (popUserEntry.getEntryTypeId() == ConstantsBase.POP_ENTRY_TYPE_CALLBACK) {
					statusId = PopulationsManagerBase.POP_ENT_HIS_STATUS_CALLBACK_REMOVE_BY_CALL;
				}

				popHisStatus = ApplicationDataBase.getPopEntryHisStatusHash().get(statusId);
				updatePopUserByStatus(popUserEntry, popHisStatus, action, context);
			}

			if (popUserEntry.getDelayId() > 0) {

	            try {
					delay = PopulationsDAOBase.getPopulationDelayForPopUser(conn,popUserEntry.getPopulationUserId());
					int delayType = delay.getDelayType();

					if (null != delay){
	            		if (PopulationsManagerBase.POP_DELAY_TYPE_NA == delayType){
	            			popUserEntry.setDelayId(0);
	            		}
					}
	            } catch (SQLException e) {
					log.error("Problem in canceling delay for popUser" + popUserId);
				}
			}
		}
		return true;
	}

	/**
	 *  validateAction event handler implementation
	 */
	@Override
	public void insertAction(Connection conn,UserBase user,Issue i,IssueAction action, FacesContext context) throws Exception{

		//	Insert Issue action
		IssuesDAOBase.insertAction(conn, action);

		if (null != popUserEntry){
			updateEntryType(conn, popUserEntry, popHisStatus, action);
		}
	}

}
