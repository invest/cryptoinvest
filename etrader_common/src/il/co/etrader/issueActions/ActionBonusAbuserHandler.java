package il.co.etrader.issueActions;

import il.co.etrader.bl_managers.UsersManagerBase;
import il.co.etrader.bl_vos.UserBase;
import il.co.etrader.dao_managers.IssuesDAOBase;

import java.sql.Connection;

import javax.faces.context.FacesContext;

import com.anyoption.common.beans.Issue;
import com.anyoption.common.beans.IssueAction;

public class ActionBonusAbuserHandler extends ActionHandlerBase{

	/**
	 *  validateAction event handler implementation
	 */
	@Override
	public boolean beforeInsertAction(Connection conn,UserBase user, IssueAction action, FacesContext context, int screenId) throws ActionHandlersException{

		return true;
	}

	/**
	 *  validateAction event handler implementation
	 */
	@Override
	public void insertAction(Connection conn,UserBase user,Issue i,IssueAction action, FacesContext context) throws Exception{
		/*
		 * if selected activity is "Bonus abuser" 
		 * update user table
		 */
		
		UsersManagerBase.updateUserBonusAbuser(user.getId(), true);
		
		//	Insert Issue action
		IssuesDAOBase.insertAction(conn, action);
	}

}