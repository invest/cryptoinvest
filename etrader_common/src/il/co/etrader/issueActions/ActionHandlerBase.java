package il.co.etrader.issueActions;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.Issue;
import com.anyoption.common.beans.IssueAction;
import com.anyoption.common.beans.base.Skin;
import com.anyoption.common.beans.base.UsersDetailsHistory;
import com.anyoption.common.bl_vos.PopulationEntryBase;

import il.co.etrader.bl_managers.ApplicationDataBase;
import il.co.etrader.bl_managers.PopulationsManagerBase;
import il.co.etrader.bl_managers.UsersManagerBase;
import il.co.etrader.bl_vos.PopulationDelay;
import il.co.etrader.bl_vos.PopulationEntryHisStatus;
import il.co.etrader.bl_vos.UserBase;
import il.co.etrader.dao_managers.PopulationsDAOBase;
import il.co.etrader.util.CommonUtil;
import il.co.etrader.util.ConstantsBase;


public abstract class ActionHandlerBase {

	private static final Logger log = Logger.getLogger(ActionHandlerBase.class);
	protected PopulationEntryBase popUserEntry;
	protected PopulationEntryHisStatus popHisStatus;
	protected PopulationDelay delay;
	protected long hisStatusPopEntryId;

    /**
     * Process a validation check before inserting this action type through this handler.
     *
     * @param conn - Connection
     * @param user - UserBase
     * @param action - IssueAction
     * @param context - FacesContext
     * @param screenId TODO
     */
    public abstract boolean beforeInsertAction(Connection conn,UserBase user, IssueAction action, FacesContext context, int screenId) throws ActionHandlersException;

    /**
     * Process a insertAction for this action type through this handler.
     *
     * @param conn - Connection
     * @param user - UserBase
     * @param i - Issue
     * @param action - IssueAction
     * @param context - FacesContext
     * @throws Exception TODO
     */
	public abstract void insertAction(Connection conn,UserBase user,Issue i,IssueAction action, FacesContext context) throws Exception;


	public  void updateEntryType(Connection conn,
										PopulationEntryBase popUserEntry,
										PopulationEntryHisStatus popHisStatus,
										IssueAction action) throws Exception {
		
		if (null != popHisStatus){

			if (popHisStatus.getEntryTypeChange() > 0){
				popUserEntry.setEntryTypeActionId(action.getActionId());
			}

			if (0 != hisStatusPopEntryId){
				PopulationsManagerBase.insertIntoPopulationEntriesHis(conn,
																	  hisStatusPopEntryId,
																	  action.getWriterId(),
																	  popHisStatus.getId(),
																	  popUserEntry.getAssignWriterId(),
																	  action.getActionId());
			} else {
				log.info("Cant Insert into Population Entries His because popEntryId = 0, popUserEntry = " + popUserEntry);
			}
		}

		PopulationsDAOBase.updatePopulationUser(conn, popUserEntry);

	}

	public  void updatePopUserByStatus(
			PopulationEntryBase popUserEntry,
			PopulationEntryHisStatus popHisStatus,
			IssueAction action,
			FacesContext context) {

		if (null != popHisStatus){

			int newEntryTypeId = popHisStatus.getEntryTypeChange();
			 hisStatusPopEntryId = popUserEntry.getCurrEntryId();

			if (0 == hisStatusPopEntryId){
				hisStatusPopEntryId = popUserEntry.getOldPopEntryId();
			}

			if (0 != newEntryTypeId){
				int oldEntryTypeId = popUserEntry.getEntryTypeId();

				popUserEntry.setEntryTypeId(newEntryTypeId);

				if (newEntryTypeId != oldEntryTypeId){
					ArrayList<Integer> entryTypeIds = new ArrayList<Integer>();
					entryTypeIds.add(newEntryTypeId);
					addDisplayMsg("retention.entry.entry.type.change", context, null, entryTypeIds);
				}
			}

			if (popHisStatus.isRemoveFromPopulation()){
				popUserEntry.setCurrEntryId(0);
				popUserEntry.setLockHistoryId(0);

				addDisplayMsg("retention.entry.removed.from.population", context, null, null);
			}

			if (popHisStatus.isCancelAssign()){
				UsersManagerBase.insertUsersDetailsHistoryOneField(action.getWriterId(), popUserEntry.getUserId(), popUserEntry.getUserName(), String.valueOf(popUserEntry.getUserClassId()), 
    					UsersManagerBase.USERS_DETAILS_HISTORY_TYPE_RETENTION_REP, String.valueOf(popUserEntry.getAssignWriterId()), ConstantsBase.UNASSIGN_RETENTION_REP);
				popUserEntry.setAssignWriterId(0);

				addDisplayMsg("retention.entry.remove.from.rep.assignment", context, null, null);
			}
		}
	}

	public static void addDisplayMsg(String msgKey, FacesContext context, Logger log, ArrayList<Integer> validEntryTypeIds) {
		if (null != context){
			FacesMessage fm;
			String msg = CommonUtil.getMessage(msgKey, null);

			if (null != validEntryTypeIds){
				msg += getValidEntryTypesMsg(validEntryTypeIds);
			}

			fm = new FacesMessage(FacesMessage.SEVERITY_ERROR,msg,null);
			context.addMessage(null, fm);
            
		}
		if (null != log){
			log.error(msgKey);
		}
	}

	public static String getValidEntryTypesMsg(ArrayList<Integer> validEntryTypeIds){
		String msg = "";
		int listSize = validEntryTypeIds.size();

		for (int i=0; i < listSize ; ++i){
			String key = ApplicationDataBase.populationEntryType.get(validEntryTypeIds.get(i));
			msg += " " + CommonUtil.getMessage(key,null);

			if (i != listSize -1){
				msg += ",";
			}
		}
		return msg;
	}

	protected static boolean insertUpdateDelay(Connection conn, PopulationDelay delay, PopulationEntryBase popUserEntry, IssueAction action) throws Exception{

		if (null != delay){

         	if (!delay.isPassedDelayMaxCount()){
         		delay.setDelayActionId(action.getActionId());
         		if (delay.getId() == 0){
            		// insert new delay
            		PopulationsDAOBase.insertPopulationDelay(conn, delay);
                	popUserEntry.setDelayId(delay.getId());
            	} else {
            		// update delay
            		PopulationsDAOBase.updatePopulationDelay(conn, delay);
            	}
         		return true;
         	}
        }
		return false;
	}
	
	protected void userActiveStateSaveHistory(UserBase user, long writerId, boolean beforeValue, boolean afterValue){		
        HashMap<Long, String> userDetailsBeforeChangedHM = null;
        HashMap<Long, String> userDetailsAfterChangedHM = null;
        try {
            userDetailsBeforeChangedHM = UsersManagerBase.getUserDetailsHM(ConstantsBase.NON_SELECTED, null, ConstantsBase.NON_SELECTED, null, null, null, null, false, 
                    false, null, null, null, null, null, ConstantsBase.NON_SELECTED, null, null, null, null, 
                    ConstantsBase.NON_SELECTED, null, null, false, false, (int)ConstantsBase.NON_SELECTED, false, false, false, false, null, beforeValue, UsersManagerBase.USER_DEFAULT_BUBBLES_PRICING_LEVEL);
            userDetailsAfterChangedHM = UsersManagerBase.getUserDetailsHM(ConstantsBase.NON_SELECTED, null, ConstantsBase.NON_SELECTED, null, null, null, null, false, 
                    false, null, null, null, null, null, ConstantsBase.NON_SELECTED, null, null, null, null, 
                    ConstantsBase.NON_SELECTED, null, null, false, false, (int)ConstantsBase.NON_SELECTED, false, false, false, false, null, afterValue, UsersManagerBase.USER_DEFAULT_BUBBLES_PRICING_LEVEL);
            
            ArrayList<UsersDetailsHistory> usersDetailsHistoryList = UsersManagerBase.checkUserDetailsChange(userDetailsBeforeChangedHM, userDetailsAfterChangedHM);
    		if (null != usersDetailsHistoryList) {
    			UsersManagerBase.insertUsersDetailsHistory(writerId, user.getId(), user.getUserName(), usersDetailsHistoryList, String.valueOf(user.getClassId()));
    		}
        } catch (Exception e) {
        	log.error("Error while getting History user details ", e);
        }
	}

	/**
	 * Unassign callback that scheduled more than a week
	 * @param issueAction
	 * @param currentTimeWithCallBackOffset
	 * @param populationEntry
	 */
	public boolean isCbMoreThanWeek(IssueAction issueAction, Date currentTimeWithCallBackOffset, UserBase user){

		Date callBackTimeAfterOffset = CommonUtil.getDateTimeFormaByTz(issueAction.getCallBackDateAndTime(),issueAction.getCallBackTimeOffset());
		GregorianCalendar gc = new GregorianCalendar();
		gc.setTime(currentTimeWithCallBackOffset);
		//Set 2 weeks for partner writers
		if (user.getSkinId() == Skin.SKIN_GERMAN){
			gc.add(GregorianCalendar.DAY_OF_MONTH,14);
		} else {
			gc.add(GregorianCalendar.DAY_OF_MONTH,7);
		}
		if (callBackTimeAfterOffset.after(gc.getTime())){
			return true;
		}
		return false;
   }

	/**
	 * @param popUserEntry the popUserEntry to set
	 */
	public void setPopUserEntry(PopulationEntryBase popUserEntry) {
		this.popUserEntry = popUserEntry;
	}	
}