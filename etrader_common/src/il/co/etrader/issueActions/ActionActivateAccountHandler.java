package il.co.etrader.issueActions;

import il.co.etrader.bl_managers.ApplicationDataBase;
import il.co.etrader.bl_managers.PopulationsManagerBase;
import il.co.etrader.bl_vos.UserBase;
import il.co.etrader.dao_managers.IssuesDAOBase;
import il.co.etrader.dao_managers.UsersDAOBase;

import java.sql.Connection;
import java.sql.SQLException;

import javax.faces.context.FacesContext;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.Issue;
import com.anyoption.common.beans.IssueAction;
import com.anyoption.common.daos.PopulationsDAOBase;

public class ActionActivateAccountHandler extends ActionHandlerBase{

	private static final Logger log = Logger.getLogger(ActionActivateAccountHandler.class);


	/**
	 *  validateAction event handler implementation
	 */
	@Override
	public boolean beforeInsertAction(Connection conn, UserBase user, IssueAction action, FacesContext context, int screenId) throws ActionHandlersException{
		long userId = user.getId();

		if (0 == userId){
			addDisplayMsg("issue.action.only.for.users", context, log, null);
			return false;
		} else if (user.isActive()){
			addDisplayMsg("issue.action.user.is.active", context, log, null);
			return false;
		}

		if (null == popUserEntry){
			try{
				popUserEntry = PopulationsDAOBase.getPopulationUserByUserId(conn, userId);
			}catch (SQLException e) {
				log.error("Error in insertAction fo, user: " + user.getId() , e);
				return false;
			}
		}

		if (null != popUserEntry){
			int statusId = PopulationsManagerBase.POP_ENT_HIS_STATUS_REACTIVATE_ACCOUNT;

			popHisStatus = ApplicationDataBase.getPopEntryHisStatusHash().get(statusId);
			updatePopUserByStatus(popUserEntry, popHisStatus, action, context);
		}

		return true;
	}

	/**
	 *  validateAction event handler implementation
	 */
	@Override
	public void insertAction(Connection conn, UserBase user, Issue i, IssueAction action, FacesContext context) throws Exception{

		//	Insert Issue action
		IssuesDAOBase.insertAction(conn, action);

		UsersDAOBase.setIsActive(conn,i.getUserId(), true);
		UsersDAOBase.setFalseAccount(conn, i.getUserId(), false);
		userActiveStateSaveHistory(user, action.getWriterId(), false, true);
		if (null != popUserEntry){
			updateEntryType(conn, popUserEntry, popHisStatus, action);
		}
	}
}
