package il.co.etrader.issueActions;

public class IssueActionType implements java.io.Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;

	private long id;
	private String name;
	private long channelId;
	private long reachedStatusId;
	private int screenId;
	private int depositSearchType;

	/**
	 * @return the depositSearchType
	 */
	public int getDepositSearchType() {
		return depositSearchType;
	}

	/**
	 * @param depositSearchType the depositSearchType to set
	 */
	public void setDepositSearchType(int depositSearchType) {
		this.depositSearchType = depositSearchType;
	}

	/**
	 * @return the channelId
	 */
	public long getChannelId() {
		return channelId;
	}

	/**
	 * @param channelId the channelId to set
	 */
	public void setChannelId(long channelId) {
		this.channelId = channelId;
	}

	/**
	 * @return the id
	 */
	public long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(long id) {
		this.id = id;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the reachedStatusId
	 */
	public long getReachedStatusId() {
		return reachedStatusId;
	}

	/**
	 * @param reachedStatusId the reachedStatusId to set
	 */
	public void setReachedStatusId(long reachedStatusId) {
		this.reachedStatusId = reachedStatusId;
	}

	/**
	 * @return the screenId
	 */
	public int getScreenId() {
		return screenId;
	}

	/**
	 * @param screenId the screenId to set
	 */
	public void setScreenId(int screenId) {
		this.screenId = screenId;
	}

	public String toString() {
        String ls = System.getProperty("line.separator");
        return ls + "IssueActionRoles" + ls
            + super.toString() + ls
            + "id: " + id + ls
            + "name: " + name + ls
            + "screenId: " + screenId + ls
            + "channelId: " + channelId + ls
            + "reachedStatusId: " + reachedStatusId;
    }

}
