package il.co.etrader.issueActions;

import java.sql.Connection;
import java.sql.SQLException;

import javax.faces.context.FacesContext;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.Issue;
import com.anyoption.common.beans.IssueAction;
import com.anyoption.common.beans.Skin;
import com.anyoption.common.managers.SkinsManagerBase;
import com.anyoption.common.managers.WritersManagerBase;

import il.co.etrader.bl_managers.IssuesManagerBase;
import il.co.etrader.bl_managers.UsersManagerBase;
import il.co.etrader.bl_vos.UserBase;
import il.co.etrader.dao_managers.IssuesDAOBase;


public class ActionNonRegSuspendHandler extends ActionHandlerBase{
	private static final Logger log = Logger.getLogger(ActionNonRegSuspendHandler.class);

	/**
	 *  validateAction event handler implementation
	 */
	@Override
	public boolean beforeInsertAction(Connection conn, UserBase user, IssueAction action, FacesContext context, int screenId) throws ActionHandlersException{
		
		Skin s = SkinsManagerBase.getSkin(user.getSkinId());
	   	
	   	if(s.isRegulated()){
			addDisplayMsg("issue.action.regulated.user.cant.nonreg.suspend", context, log, null);	
			log.debug("Can't insert issue Action! User is Regulated:" + user.getId());
			return false;
	   	}
	   	
	   	int actionType = Integer.valueOf(action.getActionTypeId());
	   	try {
			if(actionType == IssuesManagerBase.ISSUE_ACTION_TYPE_SUSPEND_NON_REGULATION && UsersManagerBase.isNonRegUserSuspend(user.getId())){
				addDisplayMsg("issue.action.regulated.user.cant.suspend", context, log, null);	
				log.debug("Can't insert issue Action! User is suspended:" + user.getId());
				return false;
			}
	   	
		   	if(actionType == IssuesManagerBase.ISSUE_ACTION_TYPE_REMOVE_SUSPEND_NON_REGULATION && !UsersManagerBase.isNonRegUserSuspend(user.getId())){
				addDisplayMsg("issue.action.regulated.user.cant.suspend", context, log, null);	
				log.debug("Can't insert issue Action! User is not suspended:" + user.getId());
				return false;
		   	}	   	
		} catch (SQLException e) {
			log.error(e);
			return false;
		}
	   	return true;
	}

	/**
	 *  validateAction event handler implementation
	 */
	@Override
	public void insertAction(Connection conn, UserBase user, Issue i, IssueAction action, FacesContext context) throws Exception{

		int actionType = Integer.valueOf(action.getActionTypeId());
		String writerUserName = " " + WritersManagerBase.getWriterName(action.getWriterId()) + " " ;

	   	if(actionType == IssuesManagerBase.ISSUE_ACTION_TYPE_SUSPEND_NON_REGULATION){
	   		UsersManagerBase.updateUserNonRegSuspend(user.getId(), 1);
	   		action.setComments("The user was suspend from writer:" + writerUserName + action.getComments());
		   	log.debug("The userId:" + user.getId() + " was suspended");
		
	   	} else if(actionType == IssuesManagerBase.ISSUE_ACTION_TYPE_REMOVE_SUSPEND_NON_REGULATION){	   		
	   		UsersManagerBase.updateUserNonRegSuspend(user.getId(), 0);
	   		action.setComments("The user was un-suspend from writer:" + writerUserName + action.getComments());
		   	log.debug("The userId:" + user.getId() + " was un-suspended");   	}
	   	
		//	Insert Issue action				
		IssuesDAOBase.insertAction(conn, action);
	}

}
