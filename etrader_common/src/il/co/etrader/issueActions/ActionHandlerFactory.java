/**
 *
 */
package il.co.etrader.issueActions;

import il.co.etrader.bl_managers.IssuesManagerBase;
import il.co.etrader.bl_managers.UsersManagerBase;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.IssueAction;
import com.anyoption.common.util.CommonUtil;
import com.anyoption.common.util.ConstantsBase;

/**
 * Factory class for action handlers
 * @author Eliran
 */
public abstract class ActionHandlerFactory {

	private static final Logger log = Logger.getLogger(ActionHandlerFactory.class);

	 /**
     * Create instance of the right subclass of ActionHandler
     *
     * @param actionTypen typeId - split logically ActionHandler one from another
     * @return
     */
    public static ActionHandlerBase getInstance(IssueAction action, long userStatusId) {
    	int actionType = Integer.valueOf(action.getActionTypeId());
        if (log.isTraceEnabled()) {
            log.trace("Getting instance for actionType: " + actionType);
        }

        if (CommonUtil.isHasRetentionDepartmentId(action.getWriterId(), ConstantsBase.SALES_TYPE_DEPARTMENT_COMA_RETENTION)) {
        	if (userStatusId == UsersManagerBase.USER_STATUS_COMA) {
        		switch (actionType) {
	            	case IssuesManagerBase.ISSUE_ACTION_QUERY_ANSWERED:
	            	case IssuesManagerBase.ISSUE_ACTION_NOT_COOPERATIVE:
	            	case IssuesManagerBase.ISSUE_ACTION_DENIED_ACCOUNT_OPENING:
	    				return new ActionNotInterestedHandler();
        		}
        	} else if (userStatusId == UsersManagerBase.USER_STATUS_ACTIVE || userStatusId == UsersManagerBase.USER_STATUS_SLEEPER) {
        		switch (actionType) {
	        		case IssuesManagerBase.ISSUE_ACTION_WILL_DEPOSIT_LATER:
	                case IssuesManagerBase.ISSUE_ACTION_NOT_INTERESTED_DONT_HAVE_MONEY:
	                case IssuesManagerBase.ISSUE_ACTION_NOT_INTERESTED_DONT_WANT_TO_TRADE:
	                case IssuesManagerBase.ISSUE_ACTION_NOT_INTERESTED_NOT_RELEVANT:
	        		case IssuesManagerBase.ISSUE_ACTION_NOT_SURE:
	        		case IssuesManagerBase.ISSUE_ACTION_OPENED_ACCOUNT:
	        			return new ActionDefaultSalesCallHandler();
        		}
        	}
        }
        
        switch (actionType) {
			case IssuesManagerBase.ISSUE_ACTION_FALSE_ACCOUNT:
				return new ActionFalseAccountHandler();

			case IssuesManagerBase.ISSUE_ACTION_CANCEL_PHONE_CONTACT:
				return new ActionCancelPhoneContactHandler();

			case IssuesManagerBase.ISSUE_ACTION_CANCEL_PHONE_CONTACT_DAA:
				return new ActionCancelPhoneContactDAAHandler();

			case IssuesManagerBase.ISSUE_ACTION_CANCEL_CALL_BACK:
				return new ActionCancelCallBackHandler();

			case IssuesManagerBase.ISSUE_ACTION_RETURN_TO_SALES:
				return new ActionReturnToSalesHandler();

			case IssuesManagerBase.ISSUE_ACTION_REMOVE_FROM_POPULATION:
				return new ActionRemoveFromPopHandler();

			case IssuesManagerBase.ISSUE_ACTION_REMOVE_FROM_SALES:
				return new ActionRemoveFromSalesHandler();

			case IssuesManagerBase.ISSUE_ACTION_DISPLAY_MARKETING_USERS:
				return new ActionDisplayMarketingUsersHandler();

			case IssuesManagerBase.ISSUE_ACTION_FRAUD:
				return new ActionFraudHandler();

			case IssuesManagerBase.ISSUE_ACTION_CHB_CLOSE:
				return new ActionCHBClosedHandler();

			case IssuesManagerBase.ISSUE_ACTION_DUPLICATE_ACCOUNT:
				return new ActionDuplicateAccountHandler();

			case IssuesManagerBase.ISSUE_ACTION_ACTIVATE_ACCOUNT:
				return new ActionActivateAccountHandler();

			case IssuesManagerBase.ISSUE_ACTION_DEACTIVATE_ACCOUNT:
				return new ActionDeactivateAccountHandler();

			case IssuesManagerBase.ISSUE_ACTION_CANT_TALK_NOW:
				return new ActionNoConversationSalesCallHandler();

			case IssuesManagerBase.ISSUE_ACTION_WILL_DEPOSIT_LATER:
				return new ActionToTrackingHandler();

			case IssuesManagerBase.ISSUE_ACTION_NOT_INTERESTED_NOW:
				return new ActionToTrackingHandler();

			case IssuesManagerBase.ISSUE_ACTION_NOT_SURE:
				return new ActionToTrackingHandler();

			case IssuesManagerBase.ISSUE_ACTION_BURN:
				if (userStatusId == UsersManagerBase.USER_STATUS_COMA) {					
					return new ActionRemoveFromSalesHandler();
				} else {
					return new ActionToReviewHandler();
				}

			case IssuesManagerBase.ISSUE_ACTION_QUERY_ANSWERED:
				return new ActionDefaultSalesCallHandler();

			case IssuesManagerBase.ISSUE_ACTION_NO_ANSWER:
				return new ActionNoAnswerLineBusyHandler();

			case IssuesManagerBase.ISSUE_ACTION_ALREADY_DEPOSIT:
				return new ActionDefaultSalesCallHandler();

			case IssuesManagerBase.ISSUE_ACTION_NOT_COOPERATIVE:
				return new ActionDefaultSupportCallHandler();

			case IssuesManagerBase.ISSUE_ACTION_CALL_BACK:
				return new ActionDefaultSupportCallHandler();

			case IssuesManagerBase.ISSUE_ACTION_DIFFERENT_LANGUAGE:
				return new ActionToReviewHandler();

			case IssuesManagerBase.ISSUE_ACTION_LINE_BUSY:
				return new ActionNoAnswerLineBusyHandler();

			case IssuesManagerBase.ISSUE_ACTION_NOT_INTERESTED:
				return new ActionNotInterestedHandler();

			case IssuesManagerBase.ISSUE_ACTION_THIRD_PARTY:
				return new ActionThirdPartyHandler();

			case IssuesManagerBase.ISSUE_ACTION_WRONG_NUMBER:
				return new ActionWrongNumberHandler();

			case IssuesManagerBase.ISSUE_ACTION_DEPOSIT_DURING_CALL:
				return new ActionDepositDuringCallHandler();

			case IssuesManagerBase.ISSUE_ACTION_COOPERATION:
				return new ActionDefaultSupportCallHandler();

			case IssuesManagerBase.ISSUE_ACTION_DENIED_ACCOUNT_OPENING:
				return new ActionToReviewHandler();

			case IssuesManagerBase.ISSUE_ACTION_OPENED_ACCOUNT:
				return new ActionToTrackingHandler();

			case IssuesManagerBase.ISSUE_ACTION_SMS:
				return new ActionDefaultHandler();

			case IssuesManagerBase.ISSUE_ACTION_EMAIL:
				return new ActionDefaultHandler();

			case IssuesManagerBase.ISSUE_ACTION_COMMENT:
				return new ActionDefaultHandler();

			case IssuesManagerBase.ISSUE_ACTION_CHAT:
				return new ActionDefaultHandler();

			case IssuesManagerBase.ISSUE_ACTION_RISK_CONTACT_USER:
				return new ActionDefaultHandler();

			case IssuesManagerBase.ISSUE_ACTION_RISK_NEED_DOCUMENT:
				return new ActionDefaultHandler();

			case IssuesManagerBase.ISSUE_ACTION_ALLOWED_CREDIT_CARD:
				return new ActionDefaultHandler();
				
			case IssuesManagerBase.ISSUE_ACTION_CUT_OFF_CALLS:
				return new ActionToReviewHandler();
				
			case IssuesManagerBase.ISSUE_ACTION_BONUS_ABUSER:
				return new ActionBonusAbuserHandler();
				
			case IssuesManagerBase.ISSUE_ACTION_PENDING_DOC:
				return new ActionPendingDocHandler();
				
			case IssuesManagerBase.ISSUE_ACTION_VELOCITY_WAIVE:
				return new ActionDefaultHandler();
			case IssuesManagerBase.ISSUE_ACTION_VELOCITY_ACTIVATE:
				return new ActionDefaultHandler();
				
			case IssuesManagerBase.ISSUE_ACTION_SUSPEND_REGULATION:
				return new ActionDefaultHandler();
			case IssuesManagerBase.ISSUE_ACTION_REMOVE_SUSPEND_REGULATION:
				return new ActionDefaultHandler();
				
            case IssuesManagerBase.ISSUE_ACTION_REMOVE_FROM_TREATMENT:
                return new ActionDefaultHandler();
                
            case IssuesManagerBase.ISSUE_ACTION_NOT_INTERESTED_DONT_HAVE_MONEY:
            case IssuesManagerBase.ISSUE_ACTION_NOT_INTERESTED_DONT_WANT_TO_TRADE:
            case IssuesManagerBase.ISSUE_ACTION_NOT_INTERESTED_NOT_RELEVANT:
				return new ActionNotInterestedHandler();
				
            case IssuesManagerBase.ISSUE_ACTION_DORMANT_CUSTOMER_CONVERSATION:
            	return new ActionDefaultHandler();
            	
            case IssuesManagerBase.ISSUE_ACTION_AUTOMATIC_DIALER:
                return new ActionDefaultHandler();

            case IssuesManagerBase.ISSUE_ACTION_EMAIL_CHANGED:
                return new ActionDefaultHandler();
                
            case IssuesManagerBase.ISSUE_ACTION_TYPE_REGULATION_PEP_ACCOUNT_TEMPORARY_SUSPENDED:
            case IssuesManagerBase.ISSUE_ACTION_TYPE_REGULATION_PEP_APPROVED_COMPLIANCE_UNSUSPENDED:
            case IssuesManagerBase.ISSUE_ACTION_TYPE_REGULATION_PEP_FALSE_CLASSIFICATION_UNSUSPENDED:
                return new ActionPepHandler();
                
            case IssuesManagerBase.ISSUE_ACTION_TYPE_BLOCK_CC_DEPS:
            case IssuesManagerBase.ISSUE_ACTION_TYPE_ENABLE_CC_DEPS:
                return new ActionDisabledCCDepositsHandler();
                
            case IssuesManagerBase.ISSUE_ACTION_TYPE_MAKE_VIP:
            	return new ActionVipMakeHandler();
            	
            case IssuesManagerBase.ISSUE_ACTION_TYPE_REVOKE_VIP:
            	return new ActionVipRevokeHandler();
            	
            case IssuesManagerBase.ISSUE_ACTION_TYPE_RESTRICT_VIP:
            	return new ActionVipRestrictHandler();
            	
            case IssuesManagerBase.ISSUE_ACTION_TYPE_UNRESTRICT_VIP:
            	return new ActionVipUnRestrictHandler();

            case IssuesManagerBase.ISSUE_ACTION_TYPE_SUSPEND_NON_REGULATION:
            case IssuesManagerBase.ISSUE_ACTION_TYPE_REMOVE_SUSPEND_NON_REGULATION:
                return new ActionNonRegSuspendHandler();
				
			default:
				// create only instance with relevant type
	        	log.error("Error in finding action handler for action type " + actionType);
	        	return null;
		}
    }

}
