package il.co.etrader.issueActions;

import il.co.etrader.bl_managers.UsersManagerBase;
import il.co.etrader.bl_vos.UserBase;
import il.co.etrader.dao_managers.IssuesDAOBase;
import il.co.etrader.dao_managers.UsersDAOBase;

import java.sql.Connection;

import javax.faces.context.FacesContext;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.Issue;
import com.anyoption.common.beans.IssueAction;

public class ActionVipUnRestrictHandler extends ActionHandlerBase {
	private static final Logger log = Logger.getLogger(ActionVipUnRestrictHandler.class);

	@Override
	public boolean beforeInsertAction(Connection conn, UserBase user, IssueAction action, FacesContext context, int screenId) throws ActionHandlersException {
		int vipStatusId = user.getVipStatusId();
		if (vipStatusId != UsersManagerBase.USER_VIP_STATUS_RESTRICTED) {
			addDisplayMsg("issue.action.vip.status.not.restricted", context, log, null);
			return false;
		}
		return true;
	}

	@Override
	public void insertAction(Connection conn, UserBase user, Issue i, IssueAction action, FacesContext context) throws Exception {
		/*	Insert Issue action */
		IssuesDAOBase.insertAction(conn, action);
		/*	Update VIP Status */
		UsersDAOBase.updateVipStatus(conn, user.getId(), UsersManagerBase.USER_VIP_STATUS_NOT_VIP);
		user.setVipStatusId(UsersManagerBase.USER_VIP_STATUS_NOT_VIP);
	}

}
