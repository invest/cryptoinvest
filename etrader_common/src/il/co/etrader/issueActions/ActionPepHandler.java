package il.co.etrader.issueActions;

import java.sql.Connection;
import java.sql.SQLException;

import javax.faces.context.FacesContext;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.Issue;
import com.anyoption.common.beans.IssueAction;
import com.anyoption.common.beans.base.UserRegulationBase;
import com.anyoption.common.managers.QuestionnaireManagerBase;
import com.anyoption.common.managers.UserRegulationManager;

import il.co.etrader.bl_managers.ApplicationDataBase;
import il.co.etrader.bl_managers.IssuesManagerBase;
import il.co.etrader.bl_managers.UsersManagerBase;
import il.co.etrader.bl_vos.UserBase;
import il.co.etrader.dao_managers.IssuesDAOBase;
import il.co.etrader.util.ConstantsBase;


public class ActionPepHandler extends ActionHandlerBase{
	private static final Logger log = Logger.getLogger(ActionPepHandler.class);

	/**
	 *  validateAction event handler implementation
	 */
	@Override
	public boolean beforeInsertAction(Connection conn,UserBase user, IssueAction action, FacesContext context, int screenId) throws ActionHandlersException{
		UserRegulationBase userRegulation = new UserRegulationBase();
	   	userRegulation.setUserId(user.getId());
	   	int actionType = Integer.valueOf(action.getActionTypeId());
	   	try {
			UserRegulationManager.getUserRegulation(userRegulation);
		} catch (SQLException e) {
			log.error("Can't get userRegulation", e);
			return false;
		}
	   	if(actionType == IssuesManagerBase.ISSUE_ACTION_TYPE_REGULATION_PEP_ACCOUNT_TEMPORARY_SUSPENDED 
	   			&& UserRegulationManager.isPepProhibited(userRegulation, user.getSkinId())){
			addDisplayMsg("issue.action.pep.already.suspended", context, log, null);	
			log.debug("Can't insert issue Action! User is already suspended:" + userRegulation);
			return false;
	   	}
	   	
	   	if((actionType == IssuesManagerBase.ISSUE_ACTION_TYPE_REGULATION_PEP_APPROVED_COMPLIANCE_UNSUSPENDED 
	   			|| actionType == IssuesManagerBase.ISSUE_ACTION_TYPE_REGULATION_PEP_FALSE_CLASSIFICATION_UNSUSPENDED)
	   				&& !UserRegulationManager.isPepProhibited(userRegulation, user.getSkinId())){
			addDisplayMsg("issue.action.pep.not.in.suspended", context, log, null);	
			log.debug("Can't insert issue Action! User is not in PEP suspended:" + userRegulation);
			return false;
	   	}
	   	
	   	return true;
	}

	/**
	 *  validateAction event handler implementation
	 */
	@Override
	public void insertAction(Connection conn, UserBase user, Issue i, IssueAction action, FacesContext context) throws Exception{

		int actionType = Integer.valueOf(action.getActionTypeId());
		UserRegulationBase userRegulation = new UserRegulationBase();
	   	userRegulation.setUserId(user.getId());
	   	UserRegulationManager.getUserRegulation(userRegulation);
		String writerUserName = ApplicationDataBase.getWriterName(action.getWriterId());
		String addComments = action.getComments();
		
	   	if(actionType == IssuesManagerBase.ISSUE_ACTION_TYPE_REGULATION_PEP_ACCOUNT_TEMPORARY_SUSPENDED){
		   	log.debug("The userId:" + user.getId() + " is writer prohibited PEP! Create Issue, Send Email");
			userRegulation.setPepState(UserRegulationBase.PEP_WRITER_PROHIBITED);
			QuestionnaireManagerBase.updateUserPepAnswer(userRegulation);
			log.debug("Try to send User Pep Mail");
			UsersManagerBase.sendMailTemplateFile(ConstantsBase.TEMPLATE_REGULATION_PEP, user.getWriterId(), user, null, new String(), new String(), null);			
			addComments = addComments + " User classified PEP by " + writerUserName + ", temporarily suspended.";			
	   	} else if(actionType == IssuesManagerBase.ISSUE_ACTION_TYPE_REGULATION_PEP_APPROVED_COMPLIANCE_UNSUSPENDED){
	   		log.debug("The userId:" + user.getId() + " is unsuspended after APPROVED COMPLIANCE!");
	   		userRegulation.setPepState(UserRegulationBase.PEP_APPROVED_BY_COMPLIANCE);
			QuestionnaireManagerBase.updateUserPepAnswer(userRegulation);
			addComments = addComments + " PEP approved for trading by CEO. updated by: " + writerUserName;
	   	} else if(actionType == IssuesManagerBase.ISSUE_ACTION_TYPE_REGULATION_PEP_FALSE_CLASSIFICATION_UNSUSPENDED){
	   		log.debug("The userId:" + user.getId() + " is unsuspended after FALSE CLASSIFICATION!");
	   		userRegulation.setPepState(UserRegulationBase.PEP_FALSE_CLASSIFICATION);
			QuestionnaireManagerBase.updateUserPepAnswer(userRegulation);
			addComments = addComments + " User classified PEP by mistake. " + writerUserName;
			QuestionnaireManagerBase.updateCheckedPepAnswer(user.getId());
			log.debug("The userId:" + user.getId() + " is cheked PEP Answer ");
	   	}
		//	Insert Issue action
		action.setComments(addComments);
		IssuesDAOBase.insertAction(conn, action);
	}

}
