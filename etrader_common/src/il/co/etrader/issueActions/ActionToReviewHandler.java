package il.co.etrader.issueActions;

import il.co.etrader.bl_managers.ApplicationDataBase;
import il.co.etrader.bl_managers.PopulationsManagerBase;
import il.co.etrader.bl_vos.UserBase;
import il.co.etrader.dao_managers.IssuesDAOBase;
import il.co.etrader.util.ConstantsBase;

import java.sql.Connection;
import java.util.ArrayList;

import javax.faces.context.FacesContext;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.Issue;
import com.anyoption.common.beans.IssueAction;

public class ActionToReviewHandler extends ActionHandlerBase{

    private static final Logger log = Logger.getLogger(ActionToReviewHandler.class);


    /**
     *  validateAction event handler implementation
     */
    @Override
    public boolean beforeInsertAction(Connection conn,UserBase user, IssueAction action, FacesContext context, int screenId) throws ActionHandlersException{

        if (null != popUserEntry){
            int statusId = 0;            int entryTypeId = popUserEntry.getEntryTypeId();

            if (ConstantsBase.POP_ENTRY_TYPE_GENERAL == entryTypeId ||
            		ConstantsBase.POP_ENTRY_TYPE_TRACKING == entryTypeId ||
            		ConstantsBase.POP_ENTRY_TYPE_CALLBACK == entryTypeId){

            	statusId = PopulationsManagerBase.POP_ENT_HIS_STATUS_REVIEW;
            }else{
				ArrayList<Integer> entryTypeIds = new ArrayList<Integer>();
				entryTypeIds.add(ConstantsBase.POP_ENTRY_TYPE_GENERAL);
				entryTypeIds.add(ConstantsBase.POP_ENTRY_TYPE_TRACKING);
				entryTypeIds.add(ConstantsBase.POP_ENTRY_TYPE_CALLBACK);
				addDisplayMsg("issue.action.only.for.specific.entry.types", context, log, entryTypeIds);
				return false;
            }

			popHisStatus = ApplicationDataBase.getPopEntryHisStatusHash().get(statusId);
			updatePopUserByStatus(popUserEntry, popHisStatus, action, context);
            return true;
        }else {
			addDisplayMsg("issue.action.only.for.sales", context, log, null);
            return false;
        }
    }

    /**
     *  validateAction event handler implementation
     */
    @Override
    public void insertAction(Connection conn,UserBase user,Issue i,IssueAction action, FacesContext context) throws Exception{

        //	Insert Issue action
        IssuesDAOBase.insertAction(conn, action);

        updateEntryType(conn, popUserEntry, popHisStatus, action);
    }
}
