package il.co.etrader.issueActions;

import java.sql.Connection;
import java.sql.SQLException;

import javax.faces.context.FacesContext;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.Issue;
import com.anyoption.common.beans.IssueAction;
import com.anyoption.common.managers.TransactionsManagerBase;
import com.anyoption.common.managers.WritersManagerBase;

import il.co.etrader.bl_managers.IssuesManagerBase;
import il.co.etrader.bl_vos.UserBase;
import il.co.etrader.dao_managers.IssuesDAOBase;
import il.co.etrader.util.CommonUtil;


public class ActionDisabledCCDepositsHandler extends ActionHandlerBase{
	private static final Logger log = Logger.getLogger(ActionDisabledCCDepositsHandler.class);

	/**
	 *  validateAction event handler implementation
	 */
	@Override
	public boolean beforeInsertAction(Connection conn,UserBase user, IssueAction action, FacesContext context, int screenId) throws ActionHandlersException{

	   	int actionType = Integer.valueOf(action.getActionTypeId());
	   	boolean isDisable = false;
		try {
			isDisable = TransactionsManagerBase.isDisableCcDeposits(user.getId());			
		} catch (SQLException e) {
			log.error("Can't get isDisableCcDeposits", e);
			return false;
		}
	   	
	   	if(actionType == IssuesManagerBase.ISSUE_ACTION_TYPE_BLOCK_CC_DEPS && isDisable ){
			addDisplayMsg("issue.action.cc.already.block.deps", context, log, null);	
			log.debug("Can't insert issue Action! User is already blocked:" + user.getId());
			return false;
	   	}
	   	
	   	if(actionType == IssuesManagerBase.ISSUE_ACTION_TYPE_ENABLE_CC_DEPS && !isDisable ){
			addDisplayMsg("issue.action.cc.already.enable.deps", context, log, null);	
			log.debug("Can't insert issue Action! User is already enable:" + user.getId());
			return false;
	   	}
	   	
	   	
	   	
	   	return true;
	}

	/**
	 *  validateAction event handler implementation
	 */
	@Override
	public void insertAction(Connection conn, UserBase user, Issue i, IssueAction action, FacesContext context) throws Exception{

		int actionType = Integer.valueOf(action.getActionTypeId());
		String writerUserName = " " + WritersManagerBase.getWriterName(action.getWriterId()) + " " ;

		//updateDisableCcDeposits
	   	if(actionType == IssuesManagerBase.ISSUE_ACTION_TYPE_BLOCK_CC_DEPS){
		   	TransactionsManagerBase.updateDisableCcDeposits(true, user.getId());
		   	log.debug("The userId:" + user.getId() + " is block cc deps");
	   		action.setComments(CommonUtil.getMessage("issue.action.blocked.cc.deposits.comm", null) + writerUserName + action.getComments());
		
	   	} else if(actionType == IssuesManagerBase.ISSUE_ACTION_TYPE_ENABLE_CC_DEPS){	   		
	   		TransactionsManagerBase.updateDisableCcDeposits(false, user.getId());
	   		log.debug("The userId:" + user.getId() + " is enable cc deps");
	   		action.setComments(CommonUtil.getMessage("issue.action.enable.cc.deposits.comm", null) + writerUserName + action.getComments());
	   	}
	   	
		//	Insert Issue action				
		IssuesDAOBase.insertAction(conn, action);
	}

}
