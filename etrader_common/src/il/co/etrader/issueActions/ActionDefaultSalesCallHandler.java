package il.co.etrader.issueActions;

import il.co.etrader.bl_managers.ApplicationDataBase;
import il.co.etrader.bl_managers.IssuesManagerBase;
import il.co.etrader.bl_managers.PopulationsManagerBase;
import il.co.etrader.bl_vos.UserBase;
import il.co.etrader.dao_managers.IssuesDAOBase;
import il.co.etrader.dao_managers.PopulationsDAOBase;
import il.co.etrader.population.PopulationHandlersException;
import il.co.etrader.util.CommonUtil;
import il.co.etrader.util.ConstantsBase;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Date;

import javax.faces.context.FacesContext;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.Issue;
import com.anyoption.common.beans.IssueAction;


public class ActionDefaultSalesCallHandler extends ActionHandlerBase{

	private static final Logger log = Logger.getLogger(ActionDefaultSalesCallHandler.class);

	/**
	 *  validateAction event handler implementation
	 */
	@Override
	public boolean beforeInsertAction(Connection conn,UserBase user, IssueAction action, FacesContext context, int screenId) throws ActionHandlersException{

		int statusId = 0;

		if (null != popUserEntry){

			long popUserId = popUserEntry.getPopulationUserId();

			// Already deposited is only relevant for users
			if (popUserEntry.getUserId() == 0 &&
					action.getActionTypeId().equals(String.valueOf(IssuesManagerBase.ISSUE_ACTION_ALREADY_DEPOSIT))) {
				addDisplayMsg("issue.action.only.for.users", context, log, null);
				return false;
			}

			if (action.isCallBack()) {
				//Unassigned CB that more than a week
				if(isCbMoreThanWeek(action, CommonUtil.getDateTimeFormaByTz(new Date(), action.getCallBackTimeOffset()), user)) {
					statusId = PopulationsManagerBase.POP_ENT_HIS_STATUS_CALLBACK_CANCEL_ASSIGN;
				} else {
					statusId = PopulationsManagerBase.POP_ENT_HIS_STATUS_CALLBACK;
				}
			} else if (popUserEntry.getEntryTypeId() == ConstantsBase.POP_ENTRY_TYPE_CALLBACK) {
				statusId = PopulationsManagerBase.POP_ENT_HIS_STATUS_CALLBACK_REMOVE_BY_CALL;
			}

			popHisStatus = ApplicationDataBase.getPopEntryHisStatusHash().get(statusId);
			updatePopUserByStatus(popUserEntry, popHisStatus, action, context);

			if (popUserEntry.getDelayId() > 0) {

	            try {
					delay = PopulationsDAOBase.getPopulationDelayForPopUser(conn,popUserEntry.getPopulationUserId());
					int delayType = delay.getDelayType();

					if (null != delay){
	            		if (PopulationsManagerBase.POP_DELAY_TYPE_NA == delayType ||
	            				PopulationsManagerBase.POP_DELAY_TYPE_NI == delayType){
	            			popUserEntry.setDelayId(0);
	            		}
					}
	            } catch (SQLException e) {
					log.error("Problem in canceling delay for popUser" + popUserId);
				}
			}
		} else {
			addDisplayMsg("issue.action.only.for.sales", context, log, null);
            return false;
		}
		return true;
	}

	/**
	 *  validateAction event handler implementation
	 */
	@Override
	public void insertAction(Connection conn,UserBase user,Issue i,IssueAction action, FacesContext context) throws Exception{

		//	Insert Issue action
		IssuesDAOBase.insertAction(conn, action);

		updateEntryType(conn, popUserEntry, popHisStatus, action);

		try {
			PopulationsManagerBase.reachedCall(conn,popUserEntry, action.getWriterId(), user);
		} catch (PopulationHandlersException e) {
			log.debug("fail to remove from call me for pop user " + popUserEntry.getPopulationUserId(),e);
		}
	}

}
