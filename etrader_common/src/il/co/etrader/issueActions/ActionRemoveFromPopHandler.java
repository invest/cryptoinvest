package il.co.etrader.issueActions;

import il.co.etrader.bl_managers.ApplicationDataBase;
import il.co.etrader.bl_managers.PopulationsManagerBase;
import il.co.etrader.bl_vos.UserBase;
import il.co.etrader.dao_managers.IssuesDAOBase;
import il.co.etrader.dao_managers.UsersDAOBase;
import il.co.etrader.util.ConstantsBase;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.faces.context.FacesContext;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.Issue;
import com.anyoption.common.beans.IssueAction;
import com.anyoption.common.daos.PopulationsDAOBase;

public class ActionRemoveFromPopHandler extends ActionHandlerBase{

	private static final Logger log = Logger.getLogger(ActionRemoveFromPopHandler.class);


	/**
	 *  validateAction event handler implementation
	 */
	@Override
	public boolean beforeInsertAction(Connection conn,UserBase user, IssueAction action, FacesContext context, int screenId) throws ActionHandlersException{
		int statusId = 0;

		if (null == popUserEntry){
			try{
				long userId = user.getId();
				popUserEntry = PopulationsDAOBase.getPopulationUserByUserId(conn, userId);
			}catch (SQLException e) {
				log.error("Error in insertAction fo, user: " + user.getId() , e);
				return false;
			}
		}

		if (null != popUserEntry){
			if (popUserEntry.getCurrEntryId() == 0){
				addDisplayMsg("issue.action.user.not.in.pop", context, log, null);
				return false;
			}else{
				if (popUserEntry.getCurrPopualtionDeptId() == ConstantsBase.DEPARTMENT_RETENTION && screenId != ConstantsBase.SCREEN_SUPPORT){
					long entryTypeId = popUserEntry.getEntryTypeId();

					if (ConstantsBase.POP_ENTRY_TYPE_GENERAL == entryTypeId ||
							ConstantsBase.POP_ENTRY_TYPE_REVIEW == entryTypeId){

						statusId = PopulationsManagerBase.POP_ENT_HIS_STATUS_REVIEW_REMOVE_CURR_POP;
					}else{
						ArrayList<Integer> entryTypeIds = new ArrayList<Integer>();
						entryTypeIds.add(ConstantsBase.POP_ENTRY_TYPE_GENERAL);
						entryTypeIds.add(ConstantsBase.POP_ENTRY_TYPE_REVIEW);
						addDisplayMsg("issue.action.only.for.specific.entry.types", context, log, entryTypeIds);
						return false;
					}
				}else{
					statusId = PopulationsManagerBase.POP_ENT_HIS_STATUS_REMOVED_CURR_POP_SUPPORT;
				}
			}

			popHisStatus = ApplicationDataBase.getPopEntryHisStatusHash().get(statusId);
			updatePopUserByStatus(popUserEntry, popHisStatus, action, context);
		} else {
        	addDisplayMsg("issue.action.only.for.sales", context, log, null);
            return false;
        }

		return true;
	}

	/**
	 *  validateAction event handler implementation
	 */
	@Override
	public void insertAction(Connection conn,UserBase user,Issue i,IssueAction action, FacesContext context) throws Exception{

		//	Insert Issue action
		IssuesDAOBase.insertAction(conn, action);
		
		updateEntryType(conn, popUserEntry, popHisStatus, action);
		
		
		if (popHisStatus.getId() == PopulationsManagerBase.POP_ENT_HIS_STATUS_REMOVED_CURR_POP_SUPPORT && user.getFirstDepositId() > 0) {
			PopulationsManagerBase.insertIntoPopulation(conn, PopulationsManagerBase.POP_TYPE_RETENTION, popUserEntry.getContactId(),
					popUserEntry.getSkinId(), popUserEntry.getUserId(), 0, popUserEntry);
			UsersDAOBase.setUserLastDecline(conn, user.getId(), false, null);
		}
	}
}
