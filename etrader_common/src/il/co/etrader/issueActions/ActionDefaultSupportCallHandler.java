package il.co.etrader.issueActions;

import il.co.etrader.bl_managers.IssuesManagerBase;
import il.co.etrader.bl_managers.PopulationsManagerBase;
import il.co.etrader.bl_vos.UserBase;
import il.co.etrader.dao_managers.IssuesDAOBase;
import il.co.etrader.dao_managers.PopulationsDAOBase;

import java.sql.Connection;
import java.sql.SQLException;

import javax.faces.context.FacesContext;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.Issue;
import com.anyoption.common.beans.IssueAction;


public class ActionDefaultSupportCallHandler extends ActionHandlerBase{

	private static final Logger log = Logger.getLogger(ActionDefaultSupportCallHandler.class);

	/**
	 *  validateAction event handler implementation
	 */
	@Override
	public boolean beforeInsertAction(Connection conn,UserBase user, IssueAction action, FacesContext context, int screenId) throws ActionHandlersException{

		long userId = user.getId();

		if (null == popUserEntry){
			try{
				popUserEntry = com.anyoption.common.daos.PopulationsDAOBase.getPopulationUserByUserId(conn, userId);
			}catch (SQLException e) {
				log.error("Error in insertAction fo, user: " + user.getId() , e);
			}
		}

		//	check if user has a delay that needs to be canceled
		if (null != popUserEntry && popUserEntry.getDelayId() > 0){
			long popUserId = popUserEntry.getPopulationUserId();

            try {
				delay = PopulationsDAOBase.getPopulationDelayForPopUser(conn,popUserEntry.getPopulationUserId());
				int delayType = delay.getDelayType();

				if (null != delay){
            		if (PopulationsManagerBase.POP_DELAY_TYPE_NA == delayType &&
            				action.getReachedStatusId().equals(String.valueOf(IssuesManagerBase.ISSUE_REACHED_STATUS_REACHED))){
            			popUserEntry.setDelayId(0);
            		}
				}
            } catch (SQLException e) {
				log.error("Problem in canceling delay for popUser" + popUserId);
			}
		}
		return true;
	}

	/**
	 *  validateAction event handler implementation
	 */
	@Override
	public void insertAction(Connection conn,UserBase user,Issue i,IssueAction action, FacesContext context) throws Exception{

		//	Insert Issue action
		IssuesDAOBase.insertAction(conn, action);

		if (null != popUserEntry){
			updateEntryType(conn, popUserEntry, popHisStatus, action);
		}
	}

}
