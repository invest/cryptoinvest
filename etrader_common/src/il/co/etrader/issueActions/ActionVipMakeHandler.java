package il.co.etrader.issueActions;

import il.co.etrader.bl_managers.UsersManagerBase;
import il.co.etrader.bl_vos.UserBase;
import il.co.etrader.dao_managers.IssuesDAOBase;
import il.co.etrader.dao_managers.UsersDAOBase;

import java.sql.Connection;
import java.sql.SQLException;

import javax.faces.context.FacesContext;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.Issue;
import com.anyoption.common.beans.IssueAction;
import com.anyoption.common.beans.base.UserRegulationBase;
import com.anyoption.common.daos.UsersRegulationDAOBase;

public class ActionVipMakeHandler extends ActionHandlerBase {
	private static final Logger log = Logger.getLogger(ActionVipMakeHandler.class);

	@Override
	public boolean beforeInsertAction(Connection conn, UserBase user, IssueAction action, FacesContext context, int screenId) throws ActionHandlersException {
		/* UserRegulationBase */
		UserRegulationBase ur = new UserRegulationBase();
		ur.setUserId(user.getId());
		try {
			UsersRegulationDAOBase.getUserRegulation(conn, ur);
		} catch (SQLException e) {
			log.error("Error when try to get user regulation, user id: " + user.getId() , e);
			return false;
		}
		/* Conditions before insert action */
		int vipStatusId = user.getVipStatusId();
		if (vipStatusId == UsersManagerBase.USER_VIP_STATUS_VIP) {
			addDisplayMsg("issue.action.vip.status.already.vip", context, log, null);
			return false;
		} else if (vipStatusId == UsersManagerBase.USER_VIP_STATUS_RESTRICTED) {
			addDisplayMsg("issue.action.vip.status.vip.restricted", context, log, null);
			return false;
		} else if (user.isBonusAbuser()) {
			addDisplayMsg("issue.action.vip.status.bonus.abuser", context, log, null);
			return false;
		} else if (null != ur.getSuspendedReasonId() && ur.getSuspendedReasonId() > 0) {
			addDisplayMsg("User is marked as inactive trader", context, log, null);
			return false;
		}
		return true;
	}

	@Override
	public void insertAction(Connection conn, UserBase user, Issue i, IssueAction action, FacesContext context) throws Exception {
		/*	Insert Issue action */
		IssuesDAOBase.insertAction(conn, action);
		/*	Update VIP Status */
		UsersDAOBase.updateVipStatus(conn, user.getId(), UsersManagerBase.USER_VIP_STATUS_VIP);
		user.setVipStatusId(UsersManagerBase.USER_VIP_STATUS_VIP);
	}
}
