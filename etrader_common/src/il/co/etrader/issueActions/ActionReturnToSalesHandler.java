package il.co.etrader.issueActions;

import il.co.etrader.bl_managers.ApplicationDataBase;
import il.co.etrader.bl_managers.PopulationsManagerBase;
import il.co.etrader.bl_vos.UserBase;
import il.co.etrader.dao_managers.IssuesDAOBase;
import il.co.etrader.util.ConstantsBase;

import java.sql.Connection;
import java.util.ArrayList;

import javax.faces.context.FacesContext;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.Issue;
import com.anyoption.common.beans.IssueAction;

public class ActionReturnToSalesHandler extends ActionHandlerBase{

	private static final Logger log = Logger.getLogger(ActionReturnToSalesHandler.class);


	/**
	 *  validateAction event handler implementation
	 */
	@Override
	public boolean beforeInsertAction(Connection conn,UserBase user, IssueAction action, FacesContext context, int screenId) throws ActionHandlersException{

		if (null != popUserEntry){
			long entryTypeId = popUserEntry.getEntryTypeId();
			int statusId = 0;

			try{

				if (ConstantsBase.POP_ENTRY_TYPE_REVIEW == entryTypeId){

					statusId = PopulationsManagerBase.POP_ENT_HIS_STATUS_REVIEW_REMOVE_BACK_TO_SALES;

				}else if (ConstantsBase.POP_ENTRY_TYPE_REMOVE_FROM_SALES == entryTypeId ||
							ConstantsBase.POP_ENTRY_TYPE_DELAY == entryTypeId){

					statusId = PopulationsManagerBase.POP_ENT_HIS_STATUS_BACK_TO_SALES_MANUALLY;
				}else{
					ArrayList<Integer> entryTypeIds = new ArrayList<Integer>();
					entryTypeIds.add(ConstantsBase.POP_ENTRY_TYPE_REVIEW);
					entryTypeIds.add(ConstantsBase.POP_ENTRY_TYPE_REMOVE_FROM_SALES);
					entryTypeIds.add(ConstantsBase.POP_ENTRY_TYPE_DELAY);
					addDisplayMsg("issue.action.only.for.specific.entry.types", context, log, entryTypeIds);
					return false;
				}

				popHisStatus = ApplicationDataBase.getPopEntryHisStatusHash().get(statusId);
				updatePopUserByStatus(popUserEntry, popHisStatus, action, context);
			}catch (Exception e) {
				log.error("Error in insertAction fo, user: " + user.getId() , e);
				return false;
			}
		}else {
			if (screenId == ConstantsBase.SCREEN_MARKETING && action.getChannelId().equals(ConstantsBase.ISSUE_ACTION_CHANNEL_ACTIVITY)) {
				if (user.getId() > 0 && user.getFirstDepositId() > 0) {
					try {
						PopulationsManagerBase.insertIntoPopulation(conn, PopulationsManagerBase.POP_TYPE_RETENTION, user.getContactId(), 0, user.getId(), 0, null);
						int statusId = PopulationsManagerBase.POP_ENT_HIS_STATUS_BACK_TO_SALES_MANUALLY;
						popHisStatus = ApplicationDataBase.getPopEntryHisStatusHash().get(statusId);
						popUserEntry = PopulationsManagerBase.getPopulationUserByUserId(user.getId());
						PopulationsManagerBase.insertIntoPopulationEntriesHis(conn,popUserEntry.getCurrEntryId(), action.getWriterId(), popHisStatus.getId(), popUserEntry.getAssignWriterId(), action.getActionId());
						
					} catch (Exception e) {
						log.error("Error in insertAction fo, insert into pop, user: " + user.getId() , e);
						return false;
					}
				} else {
					addDisplayMsg("issue.action.only.for.retention.users", context, log, null);
		            return false;
				}
			} else {
				addDisplayMsg("issue.action.only.for.sales", context, log, null);
	            return false;
			}
		}

		return true;
	}


	/**
	 *  validateAction event handler implementation
	 */
	@Override
	public void insertAction(Connection conn,UserBase user,Issue i,IssueAction action, FacesContext context) throws Exception{

		//	Insert Issue action
		IssuesDAOBase.insertAction(conn, action);
		if(null != popUserEntry) {
			updateEntryType(conn, popUserEntry, popHisStatus, action);
		}
	}
}
