package il.co.etrader.issueActions;

/**
 * ActionHandlersException class
 * @author Eliran
 *
 */
public class ActionHandlersException extends Exception {

	/**
	 *
	 */
	private static final long serialVersionUID = -838967382232927392L;

	public ActionHandlersException() {
        super();
    }

    public ActionHandlersException(String message) {
        super(message);
    }

    public ActionHandlersException(String message, Throwable t) {
        super(message, t);
    }
}