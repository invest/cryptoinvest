package il.co.etrader.jmx;

import il.co.etrader.bl_managers.ApplicationDataBase;
import il.co.etrader.jobs.CustomerReportsSendMailJobRunnable;

import java.lang.management.ManagementFactory;

import javax.management.MBeanServer;
import javax.management.ObjectName;

import org.apache.log4j.Logger;

public class JMXCacheCleanParts implements JMXCacheCleanPartsMBean {
	public static final Logger log = Logger.getLogger(JMXCacheCleanParts.class);
	
	public static final String WEB_CLEAR_CLEAN_PART_JMX_OBJECT_NAME_AO	= "il.co.etrader.jmx.:type=webCleanCachePart,name=AOWebCleanCachePart";
	public static final String WEB_CLEAR_CLEAN_PART_JMX_OBJECT_NAME_ET	= "il.co.etrader.jmx.:type=webCleanCachePart,name=ETWebCleanCachePart";

	protected String jmxName;

	public boolean initSkinAssetIndexMarkets() {
		try {
			ApplicationDataBase.initSkinAssetIndexMarkets();
			return true;
		} catch (Exception e) {
			log.debug("Can't init AssetIndexMarkets!!!", e);
			return false;
		}
	}
	
	public boolean resetSkinAssetIndexMarkets() {
		try {
			ApplicationDataBase.resetSkinAssetIndexMarkets();
			return true;
		} catch (Exception e) {
			log.debug("Can't reset AssetIndexMarkets!!!", e);
			return false;
		}
	}
	
	public boolean runCutsomerReportJob() {
		try {
			CustomerReportsSendMailJobRunnable thr = new CustomerReportsSendMailJobRunnable();
			thr.start();
			return true;
		} catch (Exception e) {
			log.debug("Can't un cutsomer report JOB!!!", e);
			return false;
		}
	}
	/**
     * Register the instance in the platform MBean server under given name.
     *
     * @param name the name under which to register
     */
    public void registerJMX(String name) {
        jmxName = name;
        try {
            MBeanServer mbs = ManagementFactory.getPlatformMBeanServer();
            ObjectName on = new ObjectName(jmxName);
            mbs.registerMBean(this, on);
            if (log.isInfoEnabled()) {
                log.info("JMXCacheClean MBean registered - name: " + jmxName);
            }
        } catch (Exception e) {
            log.error("Failed to register in the MBean server.", e);
        }
    }

    /**
     * Unregister the instance from the platform MBean server. Use the name
     * under which it was registered.
     */
    public void unregisterJMX() {
        try {
            MBeanServer mbs = ManagementFactory.getPlatformMBeanServer();
            ObjectName on = new ObjectName(jmxName);
            mbs.unregisterMBean(on);
            if (log.isInfoEnabled()) {
                log.info("JMXCacheClean MBean unregistered - name: " + jmxName);
            }
        } catch (Exception e) {
            log.error("Failed to unregister from the MBean server.", e);
        }
    }
}
