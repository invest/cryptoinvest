package il.co.etrader.FirstMailContactsJob;

import il.co.etrader.bl_vos.Contact;
import il.co.etrader.bl_vos.SequenceContact;
import il.co.etrader.bl_vos.Template;
import il.co.etrader.util.CommonUtil;
import il.co.etrader.util.MailContactsJob;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

public class FirstMailContactsJob extends MailContactsJob {
	private static Logger log = Logger.getLogger(FirstMailContactsJob.class);

	public static void main(String[] args) throws Exception {
		propFile = args[0];
		long diffSeconds = 0;
		long firstJobStart = System.currentTimeMillis();
		ArrayList<SequenceContact> emailContacts = new ArrayList<SequenceContact>();
		long DuplicateStart = System.currentTimeMillis();
		log.info("Start Duplicated Contact Job");
		runDuplicatesContactsHandlerJob();
		long DuplicateEnd = System.currentTimeMillis();
		diffSeconds = (DuplicateEnd - DuplicateStart) / (1000);
		log.info("Finish Duplicated Contact Job , this part took: " + diffSeconds  + " seconds");
		long counter = 0;
		boolean isDuplicate = false;
		log.info("Starting the First Mail Contacts Job.");
		long getContactsStart = System.currentTimeMillis();
		emailContacts = getFirstContacts();
		long getContactsEnd = System.currentTimeMillis();
		diffSeconds = (getContactsEnd - getContactsStart) / (1000);
		log.info("Getting contacts took: " + diffSeconds  + " seconds");
		long sendEmailStart = System.currentTimeMillis();
		for (SequenceContact c : emailContacts) {
			isDuplicate = false;
			String duplicatesContacts = getDuplicatedContacts(c.getId());
			if (!CommonUtil.isParameterEmptyOrNull(duplicatesContacts)) {
				isDuplicate = isEmailSentToDuplicate(duplicatesContacts);
			}
			if(!isDuplicate) {
				Template t = getTemplate(getFirstSeqEmail(c.getType()));
			    Hashtable<String,String> server = initServerParameters();
			    HashMap<String, String> params = initEmailParameters(c, t);
			    boolean isEmailSent = false;
			    try {
			    	isEmailSent = send(server, t.getFileName(), params, t.getSubject(), null, 0, c, null, t.getId());
			    } catch (Exception e) {
			    	log.error("Email was not sent ");
			    }

				if (true) {
					log.info("Sending first email to " + c.toString() + ".");
					createAndInsertIssueAndIssueAction(c, t, args);
					counter++;
				}
			} else {
				log.info("Email was already sent to duplicate contact, contact " + c.getId());
			}
		}
		long sendEmailEnd = System.currentTimeMillis();
		diffSeconds = (sendEmailEnd - sendEmailStart) / (1000);
		log.info("Sending emails took: " + diffSeconds + " seconds");
		long firstJobEnd = System.currentTimeMillis();
		diffSeconds = (firstJobEnd - firstJobStart) / (1000);
		log.info("Done with First Mail Contacts Job, this part took: " + diffSeconds + " seconds");
		log.info(counter + " Emails sent");
	}

	protected static ArrayList<SequenceContact> getFirstContacts() throws Exception {
		Connection con = getConnection();
		PreparedStatement ps = null;
		ResultSet rs = null;
		ArrayList<SequenceContact> emailContacts = new ArrayList<SequenceContact>();
		String contactsId = properties.getProperty("contact.id");
		String numberOfDaysBack = properties.getProperty("number.of.days.back");

		try {
			String sql =  " SELECT " +
	 					  "		 c.id, " +
	 					  "		 c.email, " +
	 					  "		 c.user_id, " +
	 					  "		 c.first_name, " +
	 					  "		 c.last_name, " +
	 					  "		 c.skin_id, " +
	 					  "		 c.country_id, " +
	 					  "      c.utc_offset, " +
	 					  "		 c.type " +
	 					  " FROM " +
	 					  "		contacts c LEFT JOIN issues i on i.contact_id = c.id " +
					 	  "		LEFT JOIN issue_actions ia on ia.issue_id = i.id " +
					 	  "		LEFT JOIN templates t on t.id = ia.template_id " +
					 	  " WHERE " +
					 	  "		c.user_id = 0 " +
					 	  "		AND c.is_contact_by_email = 1 " +
					 	  "		AND c.email is not null " +
					 	  "		AND c.type in (?, ?) " +
					 	  "		AND (t.type_id <> ? OR t.type_id is null) ";

					 if (!CommonUtil.isParameterEmptyOrNull(contactsId)) {
						 sql += " AND c.id in (" + contactsId + ") ";
					 }

					 if (!CommonUtil.isParameterEmptyOrNull(numberOfDaysBack)) {
						 sql += "AND c.time_created between sysdate - (" + numberOfDaysBack + ") AND sysdate ";
					 }
					 sql += " GROUP BY " +
				 	  		"		c.id, " +
				 	  		"		c.email, " +
				 	  		"		c.user_id, " +
				 	  		"		c.first_name, " +
				 	  		"		c.last_name, " +
				 	  		"		c.skin_id, " +
				 	  		"		c.country_id, " +
				 	  		"		c.utc_offset, " +
				 	  		"		c.type ";

			ps = con.prepareStatement(sql);
			ps.setLong(1, Contact.CONTACT_US_REGISTER_ATTEMPTS);
			ps.setLong(2, Contact.CONTACT_US_SHORT_REG_FORM);
			ps.setLong(3, TEMPLATES_TYPE_SEQUENCE);

			rs = ps.executeQuery();

			while (rs.next()) {
				SequenceContact vo = new SequenceContact();
				vo.setId(rs.getLong("id"));
				vo.setEmail(rs.getString("email"));
				vo.setUserId(rs.getLong("user_id"));
				vo.setFirstName(rs.getString("first_name"));
				vo.setLastName(rs.getString("last_name"));
				vo.setSkinId(rs.getLong("skin_id"));
				vo.setCountryId(rs.getLong("country_id"));
				vo.setUtcOffset(rs.getString("utc_offset"));
				vo.setType(rs.getLong("type"));
				emailContacts.add(vo);
			}

		} catch (Exception e) {
			log.log(Level.FATAL,"Can't get contacts ",e);
		} finally {
			try {
				rs.close();
			} catch (Exception e) {
				log.log(Level.ERROR,"Can't close",e);
			}
			try {
				ps.close();
			} catch (Exception e) {
				log.log(Level.ERROR,"Can't close",e);
			}
			try {
				con.close();
			} catch (Exception e) {
				log.log(Level.ERROR,"Can't close",e);
			}
		}

		return emailContacts;
	}

	protected static long getFirstSeqEmail(long type) throws Exception {
		Connection con = getConnection();
		PreparedStatement ps = null;
		ResultSet rs = null;

		try {
			String sql = " SELECT " +
						 "      ecs.* " +
						 " FROM " +
						 "		email_contact_sequence ecs " +
						 " WHERE " +
						 "		ecs.template_id is null" +
						 "		AND contact_type = ? ";
			ps = con.prepareStatement(sql);
			ps.setLong(1, type);
			rs = ps.executeQuery();

			if (rs.next()) {
				return rs.getLong("next_template_id");
			}
		} catch (Exception e) {
			log.log(Level.FATAL,"Can't get template_id ",e);
		} finally {
			try {
				rs.close();
			} catch (Exception e) {
				log.log(Level.ERROR,"Can't close",e);
			}
			try {
				ps.close();
			} catch (Exception e) {
				log.log(Level.ERROR,"Can't close",e);
			}
			try {
				con.close();
			} catch (Exception e) {
				log.log(Level.ERROR,"Can't close",e);
			}
		}
		return 0;
	}

	protected static String getDuplicatedContacts(long contactId) throws Exception {
		Connection con = getConnection();
		PreparedStatement ps = null;
		ResultSet rs = null;

		try {
			String sql = " SELECT " +
						 "		dc.* " +
						 " FROM " +
						 "		duplicates_contacts dc " +
						 " WHERE " +
						 "		dc.contact_id = ? ";
			ps = con.prepareStatement(sql);
			ps.setLong(1, contactId);
			rs = ps.executeQuery();

			if (rs.next()) {
				return rs.getString("dup_contact_id");
			}
		} catch (Exception e) {
			log.log(Level.FATAL,"Can't get duplicates ",e);
		} finally {
			try {
				rs.close();
			} catch (Exception e) {
				log.log(Level.ERROR,"Can't close",e);
			}
			try {
				ps.close();
			} catch (Exception e) {
				log.log(Level.ERROR,"Can't close",e);
			}
			try {
				con.close();
			} catch (Exception e) {
				log.log(Level.ERROR,"Can't close",e);
			}
		}
		return null;
	}

	protected static boolean isEmailSentToDuplicate(String duplicates) throws Exception {
		Connection con = getConnection();
		PreparedStatement ps = null;
		ResultSet rs = null;

		try {
			String sql = " SELECT " +
						 "		count(c.id) AS num " +
						 " FROM " +
						 "		contacts c LEFT JOIN issues i on i.contact_id = c.id " +
						 "		LEFT JOIN issue_actions ia on ia.issue_id = i.id " +
						 "		LEFT JOIN templates t on t.id = ia.template_id " +
						 " WHERE " +
						 "			c.user_id = 0 " +
						 "			AND c.is_contact_by_email = 1 " +
						 "			AND c.email is not null " +
						 "			AND c.type in (5,6) " +
						 "			AND t.type_id = 2 " +
						 "			AND c.id in (" + duplicates +") ";
			ps = con.prepareStatement(sql);
			rs = ps.executeQuery();

			if (rs.next()) {
				if (rs.getLong("num") > 0 ) {
					return true;
				}
			}
		} catch (Exception e) {
			log.log(Level.FATAL,"Can't get result ",e);
		} finally {
			try {
				rs.close();
			} catch (Exception e) {
				log.log(Level.ERROR,"Can't close",e);
			}
			try {
				ps.close();
			} catch (Exception e) {
				log.log(Level.ERROR,"Can't close",e);
			}
			try {
				con.close();
			} catch (Exception e) {
				log.log(Level.ERROR,"Can't close",e);
			}
		}
		return false;
	}

	public static void runDuplicatesContactsHandlerJob() throws SQLException {
		Connection con = getConnection();
		CallableStatement cstmt = null;
        try {
            cstmt = con.prepareCall("BEGIN DUPLICATED_CONTACTS_HANDLER(); END;");
            cstmt.execute();
		} catch (Exception e) {
			log.log(Level.FATAL,"Can't get contacts ",e);
		} finally {
			try {
				cstmt.close();
			} catch (Exception e) {
				log.log(Level.ERROR,"Can't close",e);
			}
			try {
				con.close();
			} catch (Exception e) {
				log.log(Level.ERROR,"Can't close",e);
			}
		}
	}
}

