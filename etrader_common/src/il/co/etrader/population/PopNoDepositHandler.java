package il.co.etrader.population;


import java.sql.Connection;
import java.sql.SQLException;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.Transaction;
import com.anyoption.common.bl_vos.PopulationEntryBase;

import il.co.etrader.bl_managers.PopulationsManagerBase;
import il.co.etrader.bl_managers.UsersManagerBase;
import il.co.etrader.bl_vos.UserBase;
import il.co.etrader.util.ConstantsBase;

public class PopNoDepositHandler extends PopulationHandlerBase {
	private static final Logger log = Logger.getLogger(PopNoDepositHandler.class);


	/**
	 * deposit event handler implementation
	 */
	@Override
	public void deposit(Connection con, PopulationEntryBase popUserEntry, boolean successful, long writerId, Transaction tran, UserBase user) throws PopulationHandlersException {
		if (!successful){
			PopulationsManagerBase.insertIntoDeclinePop(con, user, popUserEntry, false, writerId, tran.getDescription());
		} else {
			try {
				UsersManagerBase.insertUsersDetailsHistoryOneField(writerId, popUserEntry.getUserId(), popUserEntry.getUserName(), String.valueOf(popUserEntry.getUserClassId()), 
    					UsersManagerBase.USERS_DETAILS_HISTORY_TYPE_RETENTION_REP, String.valueOf(popUserEntry.getAssignWriterId()), ConstantsBase.UNASSIGN_RETENTION_REP);
				popUserEntry.setAssignWriterId(0);
				PopulationsManagerBase.insertIntoPopulation(0, 0, user.getId(), user.getUserName(), 0, PopulationsManagerBase.POP_TYPE_RETENTION, popUserEntry);
				UsersManagerBase.updateUserRankIfNeeded(user.getId(), tran.getId(), user.getRankId());
				UsersManagerBase.updateUserStatusIfNeeded(user.getId(), user.getStatusId(), user.getBalance(), tran.getId(), null, null, null);
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}	
	}

	/**
	 * invest event handler implementation
	 */
	@Override
	public void invest(PopulationEntryBase popUserEntry, boolean successful, long writerId) throws PopulationHandlersException {
		//	do nothing
	}

	/**
	 * user creation event handler implementation
	 */
	@Override
	public void userCreation(Connection con, PopulationEntryBase popUserEntry, long writerId) throws PopulationHandlersException {
		//	do nothing
	}

	/**
	 * reached call event handler implementation
	 */
	@Override
	public void reachedCall(Connection con, PopulationEntryBase popUserEntry, long writerId, UserBase user) throws PopulationHandlersException {
		//	do nothing
	}
}