package il.co.etrader.population;

import java.sql.Connection;
import java.sql.SQLException;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.Transaction;
import com.anyoption.common.bl_vos.PopulationEntryBase;

import il.co.etrader.bl_managers.PopulationsManagerBase;
import il.co.etrader.bl_managers.UsersManagerBase;
import il.co.etrader.bl_vos.UserBase;
import il.co.etrader.util.CommonUtil;

public class PopDormentConvertionHandler extends PopulationHandlerBase {
	private static final Logger log = Logger.getLogger(PopulationHandlerBase.class);
	@Override
	public void deposit(Connection con, PopulationEntryBase popUserEntry, boolean successful, long writerId, Transaction tran, UserBase user) throws PopulationHandlersException {
		try {
			UsersManagerBase.updateUserRankIfNeeded(user.getId(), tran.getId(), user.getRankId());
			UsersManagerBase.updateUserStatusIfNeeded(user.getId(), user.getStatusId(), user.getBalance(), tran.getId(), null, null, null);
		} catch (SQLException e) {
			e.printStackTrace();
		}

		
	}

	@Override
	public void invest(PopulationEntryBase popUserEntry, boolean successful, long writerId) throws PopulationHandlersException {
		if (successful) {
			try {
				if(popUserEntry.getEntryTypeId() != PopulationsManagerBase.ENTRY_TYPE_REMOVE_PERMANENTLY) {
					PopulationsManagerBase.insertIntoPopulation(0, 0, popUserEntry.getUserId(), "", 
							0, PopulationsManagerBase.POP_TYPE_RETENTION, popUserEntry);
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}		
	}

	@Override
	public void userCreation(Connection con, PopulationEntryBase popUserEntry,
			long writerId) throws PopulationHandlersException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void reachedCall(Connection con, PopulationEntryBase popUserEntry, long writerId, UserBase user) throws PopulationHandlersException {
		FacesContext context=FacesContext.getCurrentInstance();
		FacesMessage fm = null;
		try {
			PopulationsManagerBase.insertIntoPopulation(con, PopulationsManagerBase.POP_TYPE_RETENTION, popUserEntry.getContactId(), 
					0, popUserEntry.getUserId(), popUserEntry.getAssignWriterId(), popUserEntry);
			fm = new FacesMessage(FacesMessage.SEVERITY_INFO, CommonUtil.getMessage("retention.entry.return.to.retention.population", null),null);
		} catch (SQLException e) {
			log.error("Could not insert new pop", e);
		}	
	}

}
