/**
 *
 */
package il.co.etrader.population;

import org.apache.log4j.Logger;

import il.co.etrader.bl_managers.PopulationsManagerBase;


/**
 * Factory class for population handlers
 * @author Kobi
 */
public abstract class PopulationHandlerFactory {

	private static final Logger log = Logger.getLogger(PopulationHandlerFactory.class);


	 /**
     * Create instance of the right subclass of populationHandler
     *
     * @param populationType population typeId - split logically populationHandler one from another
     * @return
     */
    public static PopulationHandlerBase getInstance(long populationType) {
        if (log.isTraceEnabled()) {
            log.trace("Getting instance for populationType: " + populationType);
        }
        if(populationType == PopulationsManagerBase.POP_TYPE_DECLINE_FIRST) {
        	return new PopDeclineFirstHandler();
        } else if(populationType == PopulationsManagerBase.POP_TYPE_DECLINE_LAST) {
        	return new PopDeclineLastHandler();
        } else if(populationType == PopulationsManagerBase.POP_TYPE_DORMANT) {
        	return new PopDormantHandler();
        } else if(populationType == PopulationsManagerBase.POP_TYPE_NO_DEPOSIT) {
        	return new PopNoDepositHandler();
        } else if(populationType == PopulationsManagerBase.POP_TYPE_CALLME) {
        	return new PopCallMeHandler();
        } else if(populationType == PopulationsManagerBase.POP_TYPE_OTHER) {
        	return new PopOtherHandler();
        } else if (populationType == PopulationsManagerBase.POP_TYPE_NEW_DORMANT) {
        	return new PopDormantHandler();
        } else if (populationType == PopulationsManagerBase.POP_TYPE_NO_INVESTMENTS) {
        	return new PopNoInvestmentsHandler();
        } else if (populationType == PopulationsManagerBase.POP_TYPE_SPECIAL_CONVERSION) {
        	return new PopSpecialConversionHandler();
        } else if (populationType == PopulationsManagerBase.POP_TYPE_DECLINED_SALES) {
        	return new PopDeclineSalesHandler();
        } else if (populationType == PopulationsManagerBase.POP_TYPE_MARKETING) {
        	return new PopMarketingHandler();
        } else if (populationType == PopulationsManagerBase.POP_TYPE_MIN_NO_DEPOSIT) {
        	return new PopNoXDepositHandler();
        } else if(populationType == PopulationsManagerBase.POP_TYPE_ONE_DEPOSIT) {
        	return new PopNoXDepositHandler();
        } else if (populationType == PopulationsManagerBase.POP_TYPE_NO_2ND_DEPOSIT) {
        	return new PopNoXDepositHandler();
        } else if (populationType == PopulationsManagerBase.POP_TYPE_NO_3RD_DEPOSIT) {
        	return new PopNoXDepositHandler();
        } else if (populationType == PopulationsManagerBase.POP_TYPE_NO_4TH_DEPOSIT) {
        	return new PopNoXDepositHandler();
        } else if (populationType == PopulationsManagerBase.POP_TYPE_NO_5TH_DEPOSIT) {
        	return new PopNoXDepositHandler();
        } else if (populationType == PopulationsManagerBase.POP_TYPE_NO_6TH_DEPOSIT) {
        	return new PopNoXDepositHandler();
        } else if (populationType == PopulationsManagerBase.POP_TYPE_NO_7TH_DEPOSIT) {
        	return new PopNoXDepositHandler();
        } else if (populationType == PopulationsManagerBase.POP_TYPE_SHORT_REG_FORM) {
        	return new PopShortRegFormHandler();
        } else if ( populationType == PopulationsManagerBase.POP_TYPE_REGISTER_ATTEMPT) {
        	return new PopRegisterAttempt();
        } else if (populationType == PopulationsManagerBase.POP_TYPE_OPEN_WITHDRAW) {
        	return new PopOpenWithdrawHandler();
        } else if (populationType == PopulationsManagerBase.POP_TYPE_RETENTION) {
        	return new PopRetentionHandler();
        } else if (populationType == PopulationsManagerBase.POP_TYPE_DORMENT_CONVERTION) {
        	return new PopDormentConvertionHandler();
        } else if (populationType == PopulationsManagerBase.POP_TYPE_MANUALLY_IMPORTED_CONVERSION) {
        	return new PopManuallyImportedConversion();
        } else if (populationType == PopulationsManagerBase.POP_TYPE_FIRST_DEPOSIT_DECLINE_DOCS_REQUIRED) {
        	return new PopFirstDepositDeclineDocsRequired();
        } else if (populationType == PopulationsManagerBase.POP_TYPE_QUICK_START) {
        	return new PopQuickStartHandler();
        } else if (populationType == PopulationsManagerBase.POP_TYPE_SPECIAL_RETENTION) {
        	return new PopSpecialRetentionHandler();
        }
        	// create only instance with relevant type
        	return null;
        }
}
