package il.co.etrader.population;


import java.sql.Connection;
import java.sql.SQLException;
import java.util.Date;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.IssueAction;
import com.anyoption.common.beans.Transaction;
import com.anyoption.common.bl_vos.PopulationEntryBase;

import il.co.etrader.bl_managers.IssuesManagerBase;
import il.co.etrader.bl_managers.PopulationsManagerBase;
import il.co.etrader.bl_vos.PopulationEntryHis;
import il.co.etrader.bl_vos.UserBase;
import il.co.etrader.dao_managers.PopulationsDAOBase;
import il.co.etrader.util.ConstantsBase;

public abstract class PopulationHandlerBase {
	private static final Logger log = Logger.getLogger(PopulationHandlerBase.class);

	/**
	 * Process an "deposit" call through this handler.
	 *
	 * @param con db connection
	 * 	in this event i added the connection becuase in inatec service we also use this event and we are getting the connection
	 * 	on other way
	 * @param successful deposit status
	 * @param writerId writer that perform the action
	 * @param tran TODO
	 * @param user TODO
	 * @param populationEntryId the population entry id
	 */
    public abstract void deposit(Connection con, PopulationEntryBase popUserEntry, boolean successful, long writerId, Transaction tran, UserBase user) throws PopulationHandlersException;

    /**
     * Process an "invest" call through this handler.
     *
	 * @param populationEntryId the population entry id
     * @param successful invest status
     * @param writerId writer that perform the action
     */
    public abstract void invest(PopulationEntryBase popUserEntry, boolean successful,long writerId) throws PopulationHandlersException;

	/**
	 * Process population upon user creation
	 * @param con TODO
	 * @param writerId writer the create the user
	 * @param populationEntryId population entry
	 * @param userId the new user id
	 * @throws PopulationHandlersException
	 */
    public abstract void userCreation(Connection con, PopulationEntryBase popUserEntry, long writerId) throws PopulationHandlersException;


	/**
	 * Process population upon reached call
	 *
	 * @param populationEntryId population entry
	 * @param userId the new user id
	 * @param writerId writer the create the user
	 * * @param user UserBase the create the user
	 * @throws PopulationHandlersException
	 */
    public abstract void reachedCall(Connection con, PopulationEntryBase popUserEntry, long writerId, UserBase user) throws PopulationHandlersException;


     /**
     * Process an "issueAction" call through this handler.
     * There are 2 options for remove: immediate option and actions counting options,
     * in the second option, taking num_issues from the population and checking with num actions
     * with the same population entry id.
     *
     * @param issueId issue id
     * @param populationEntryId population enrty id of the issue
     * @param writerId writer the performed the action
     * @throws PopulationHandlersException
     * @throws SQLException
     *
     * @return isRemoved - true if entry remove from population after this action and false otherwise.
     */
    public boolean issueAction(IssueAction action, PopulationEntryBase popUserEntry) throws PopulationHandlersException, SQLException {
    	boolean isRemoved = false;
    	long writerId = action.getWriterId();
    	String IssueActionTypeIdString = action.getActionTypeId();

    	if(null != IssueActionTypeIdString){
    		int issueActionTypeId = Integer.valueOf(IssueActionTypeIdString);

    		if(issueActionTypeId == IssuesManagerBase.ISSUE_ACTION_FALSE_ACCOUNT){
    			log.debug("issueAction event Process: false account remove, popEntryId: " + popUserEntry.getCurrEntryId());
    			delete(null, popUserEntry, writerId,
    					PopulationsManagerBase.POP_ENT_HIS_STATUS_REMOVED_FALSE_ACCOUNT, action.getActionId(),true);
    			isRemoved = true;

    		} else if(issueActionTypeId == IssuesManagerBase.ISSUE_ACTION_CANCEL_PHONE_CONTACT){
    			log.debug("issueAction event Process: disable phone contact remove, popEntryId: " + popUserEntry.getCurrEntryId());
    			delete(null, popUserEntry, writerId,
    					PopulationsManagerBase.POP_ENT_HIS_STATUS_REMOVED_DISABLE_PHONE_CONTACT, action.getActionId(),true);
    			isRemoved = true;

    		} else if(issueActionTypeId == IssuesManagerBase.ISSUE_ACTION_CANCEL_PHONE_CONTACT_DAA &&
    					(popUserEntry.getCurrPopualtionTypeId() == PopulationsManagerBase.POP_TYPE_DECLINE_LAST ||
    							popUserEntry.getCurrPopualtionTypeId() == PopulationsManagerBase.POP_TYPE_DECLINE_FIRST ||
    							popUserEntry.getCurrPopualtionTypeId() == PopulationsManagerBase.POP_TYPE_DECLINED_SALES )){
    			log.debug("issueAction event Process: disable phone contact daa remove, popEntryId: " + popUserEntry.getCurrEntryId());
    			delete(null, popUserEntry, writerId,
    					PopulationsManagerBase.POP_ENT_HIS_STATUS_REMOVED_DISABLE_PHONE_CONTACT_DAA, action.getActionId(),true);
    			isRemoved = true;
    		}
    	}
    	return isRemoved;
    }


    /**
     * Process an "close" call through this handler.
     *
	 * @param populationEntyId the population entry id
	 * @param writerId writer that perform the action
     */
    public void close(PopulationEntryBase popUserEntry, long writerId) throws PopulationHandlersException {
    	log.debug("close account event Process, popEntryId: " + popUserEntry.getCurrEntryId());
    	delete(null, popUserEntry, writerId, PopulationsManagerBase.POP_ENT_HIS_STATUS_REMOVED_INACTIVE, 0,true);
    }

    /**
     * Process an "fraud" call through this handler.
     *
	 * @param populationEntyId the population entry id
	 * @param writerId writer that perform the action
     */
    public void fraud(PopulationEntryBase popUserEntry, long writerId) throws PopulationHandlersException {
    	log.debug("fraud event Process, popEntryId: " + popUserEntry.getCurrEntryId());
    	delete(null, popUserEntry, writerId, PopulationsManagerBase.POP_ENT_HIS_STATUS_REMOVED_FRAUD, 0,true);
    }

    /**
     * Process an "lock" call through this handler.
     *
     * @param populationEntryId population entry id to lock
     * @param writerId writer that perform the action
     * @throws PopulationHandlersException
     */
    public final void lock(Connection con, PopulationEntryBase popEntry, long writerId) throws PopulationHandlersException {
    	long lockEntryId = popEntry.getOldPopEntryId();
    	PopulationEntryBase writerLockedEntry = null;
    	boolean isUnlocked = false;
    	long popUserId = popEntry.getPopulationUserId();
    	log.debug("lock event Process, popEntryId: " + lockEntryId);
    	try {
			writerLockedEntry = PopulationsDAOBase.getWriterLockedEntryBase(con, writerId);
		} catch (SQLException e1) {
			log.debug("can't get locked entry for writer: " + writerId ,e1);
		}

		if (null != writerLockedEntry) {
			try {
				isUnlocked = PopulationsManagerBase.unLock(con, writerLockedEntry, writerId);
			} catch (SQLException e) {
				log.debug("can't get unlocked entry of popEntryId: " + lockEntryId + " for writer: " + writerId ,e);
				isUnlocked = false;
			}
		} else {
			isUnlocked = true;
		}

		if (isUnlocked) {
			popEntry.setCurrEntryId(lockEntryId);
			try {
				con.setAutoCommit(false);
		    	PopulationEntryHis popHis = insert(con, popEntry, writerId,  PopulationsManagerBase.POP_ENT_HIS_STATUS_LOCKED, 0);
				PopulationsDAOBase.updateLockHistoryId(con, popUserId, popHis.getId());
				popEntry.setLockHistoryId(popHis.getId());
				con.commit();
			} catch (SQLException e) {
	    		log.error("Exception in lock action! for pop user Id: " + popUserId, e);
	    		try {
	    			con.rollback();
	    		} catch (Throwable it) {
	    			log.error("Can't rollback.", it);
	    		}
	    		throw new PopulationHandlersException(e.getMessage());
	    	} finally {
	    		try {
	    			con.setAutoCommit(true);
	    		} catch (Exception e) {
	    			log.error("Can't set back to autocommit.", e);
	    		}
	    	}
		}
    }

    /**
     * Process an "unlock" call through this handler.
     *
     * @param populationEntryId population entry id to unlock
     * @param writerId writer that perform the action
     * @throws PopulationHandlersException
     */
    public final void unLock(PopulationEntryBase popEntry, long writerId) throws PopulationHandlersException {
    	long lockEntryId = popEntry.getOldPopEntryId();
    	log.debug("unlock event Process, PopulationEntryId: " + lockEntryId);
    	popEntry.setCurrEntryId(lockEntryId);
    	insert(null, popEntry, writerId, PopulationsManagerBase.POP_ENT_HIS_STATUS_UNLOCKED, 0);
    	try {
    		PopulationEntryBase popUserEntry = PopulationsManagerBase.getPopulationUserById(popEntry.getPopulationUserId());
    		popUserEntry.setLockHistoryId(0);
			PopulationsManagerBase.updatePopulationUser(popUserEntry);
		} catch (Exception e) {
			log.debug("can't unlock, popEntryId: " + lockEntryId + " for writer: " + writerId);
		}
    }

    /**
     * Process an "unlock" call through this handler.
     *
     * @param populationEntryId population entry id to unlock
     * @param writerId writer that perform the action
     * @throws PopulationHandlersException
     */
    public final boolean unLock(Connection con, PopulationEntryBase popEntry, long writerId) throws PopulationHandlersException {
    	long lockEntryId = popEntry.getOldPopEntryId();
    	log.debug("unlock event Process, PopulationEntryId: " + lockEntryId);
    	popEntry.setCurrEntryId(lockEntryId);
    	boolean res = true;
    	try {
    		con.setAutoCommit(false);
        	insert(con, popEntry, writerId, PopulationsManagerBase.POP_ENT_HIS_STATUS_UNLOCKED, 0);
			PopulationsDAOBase.updateLockHistoryId(con, popEntry.getPopulationUserId(), 0);
			con.commit();
		} catch (Exception e) {
			log.debug("can't unlock, popEntryId: " + lockEntryId + " for writer: " + writerId);
			res = false;
			try {
				con.rollback();
			} catch (Throwable it) {
				log.error("Can't rollback.", it);
			}
			throw new PopulationHandlersException(e.getMessage());
		} finally {
			try {
				con.setAutoCommit(true);
			} catch (Exception e) {
				log.error("Can't set back to autocommit.", e);
			}
		}
		return res;
    }

    /**
     * Insert into populationEntriesHistory
     * @param populationEntyId population entry id
     * @param comments comments for the action
     * @param writerId writer that process the action
	 * @param statusId the status of the population entryHis
	 * @return population history id
     * @throws SQLException
     */
    protected PopulationEntryHis insert(Connection con, PopulationEntryBase popEntry, long writerId, long statusId, long actionId) throws PopulationHandlersException {
    	PopulationEntryHis popEnHis = new PopulationEntryHis();
    	popEnHis.setPopulationEntryId(popEntry.getCurrEntryId());
    	popEnHis.setWriterId(writerId);
    	popEnHis.setStatusId(statusId);
    	popEnHis.setAssignWriterId(popEntry.getAssignWriterId());
    	popEnHis.setIssueActionId(actionId);
    	if (null == con) {
    		PopulationsManagerBase.insertIntoPopulationHistory(popEnHis);
    	} else {
    		PopulationsManagerBase.insertIntoPopulationHistory(con, popEnHis);
    	}
    	return popEnHis;
    }

    /**
     * Update population. meaning to insert row to population_entries_hist table with
     * spesific populationEntyId
     * @param writerId writer that process the action
     * @param statusId the status of the population entryHis
     * @param newQualificationTime TODO
     * @param populationEntyId population entry id
     * @param comments comments for the action
     * @throws SQLException
     */
    protected void update(Connection con, PopulationEntryBase popUserEntry, long writerId,long statusId, long actionId, Date newQualificationTime) throws PopulationHandlersException {
    	log.debug("Going to update user population, popEntryId: " + popUserEntry.getCurrEntryId());
    	insert(con, popUserEntry, writerId, statusId, actionId);
    	popUserEntry.setQualificationTime(newQualificationTime);
    	try {
    		PopulationsDAOBase.updateEntryQualificationTime(con, popUserEntry);
		} catch (SQLException e) {
			log.error("ERROR, problem updating qualification time for population user id " +
					popUserEntry.getPopulationUserId(),e);
		}
    }

    /**
     * Remove from population. meaning to insert row to population_entries_hist table with
     * spesific populationEntyId
     * @param populationEntyId population entry id
     * @param comments comments for the action
     * @param writerId writer that process the action
	 * @param statusId the status of the population entryHis
     * @throws SQLException
     */
    protected void delete(Connection con, PopulationEntryBase popUserEntry, long writerId, long statusId, long actionId, boolean isCancelAssign) throws PopulationHandlersException {
    	log.debug("Going to remove user from population, popEntryId: " + popUserEntry.getCurrEntryId());
    	insert(con, popUserEntry, writerId,  statusId, actionId);
    	popUserEntry.setCurrEntryId(0);
    	// Cancel assign if entry is general type.
    	if (popUserEntry.getEntryTypeId() == ConstantsBase.POP_ENTRY_TYPE_GENERAL || isCancelAssign){
    		popUserEntry.setAssignWriterId(0);
    	}
    	try {
        	if (null == con) {
        		PopulationsManagerBase.updatePopulationUser(popUserEntry);
        	} else {
        		PopulationsDAOBase.updatePopulationUser(con, popUserEntry);
        	}
		} catch (Exception e) {
			log.error("Error with remoing from population for population user " +popUserEntry.getPopulationUserId());
		}

    }

	/**
	 * deposit event handler implementation for declines pops
	 */
	public void declineDeposit(Connection con, PopulationEntryBase popUserEntry, boolean successful, long writerId, Transaction tran) throws PopulationHandlersException {
		if(successful) {
			log.debug("deposit event Process, popEntryId: " + popUserEntry.getCurrEntryId());
			delete(con, popUserEntry, writerId, PopulationsManagerBase.POP_ENT_HIS_STATUS_REMOVED_QUALIFICATION, 0,false);
		} else {
			log.debug("deposit event update Process, popEntryId: " + popUserEntry.getCurrEntryId());
			update(con, popUserEntry, writerId,PopulationsManagerBase.POP_ENT_HIS_STATUS_UPDATE, 0, tran.getTimeSettled());
		}
	}

}