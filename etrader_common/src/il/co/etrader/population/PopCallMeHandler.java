package il.co.etrader.population;


import java.sql.Connection;
import java.sql.SQLException;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.Transaction;
import com.anyoption.common.bl_vos.PopulationEntryBase;

import il.co.etrader.bl_managers.PopulationsManagerBase;
import il.co.etrader.bl_managers.UsersManagerBase;
import il.co.etrader.bl_vos.UserBase;
import il.co.etrader.util.CommonUtil;

public class PopCallMeHandler extends PopulationHandlerBase {
	private static final Logger log = Logger.getLogger(PopCallMeHandler.class);

	/**
	 * deposit event handler implementation
	 */
	@Override
	public void deposit(Connection con, PopulationEntryBase popUserEntry, boolean successful, long writerId, Transaction tran, UserBase user) throws PopulationHandlersException {
		if (!successful){
			PopulationsManagerBase.insertIntoDeclinePop(con, user, popUserEntry, false, writerId, tran.getDescription());
		} else {
			try {
				UsersManagerBase.updateUserRankIfNeeded(user.getId(), tran.getId(), user.getRankId());
				UsersManagerBase.updateUserStatusIfNeeded(user.getId(), user.getStatusId(), user.getBalance(), tran.getId(), null, null, null);
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * invest event handler implementation
	 */
	@Override
	public void invest(PopulationEntryBase popUserEntry, boolean successful, long writerId) throws PopulationHandlersException {
		//	do nothing
	}

	/**
	 * closeAccount event handler implementation
	 */
	@Override
	public void close(PopulationEntryBase popUserEntry, long writerId ) throws PopulationHandlersException {
		//	do nothing
	}

	/**
	 * fraud activities event handler implementation
	 */
	@Override
	public void fraud(PopulationEntryBase popUserEntry, long writerId) throws PopulationHandlersException {
		//	do nothing
	}

	/**
	 * user creation event handler implementation
	 * Implement call me population Issues update upon user creation
	 */
	public void userCreation(Connection con, PopulationEntryBase popUserEntry, long writerId) throws PopulationHandlersException {
		// Do nothing
	}

	/**
	 * reached call event handler implementation
	 * Implement call me population remove upon reached call
	 */
	public void reachedCall(Connection con, PopulationEntryBase popUserEntry, long writerId, UserBase user) throws PopulationHandlersException {
		FacesContext context=FacesContext.getCurrentInstance();
		FacesMessage fm = null;
		if (popUserEntry.getUserId() > 0) {
			if (popUserEntry.getOldPopulationTypeId() == PopulationsManagerBase.POP_TYPE_OPEN_WITHDRAW) {
				try {
					PopulationsManagerBase.insertIntoPopulation(con, PopulationsManagerBase.POP_TYPE_OPEN_WITHDRAW, popUserEntry.getContactId(), 
							0, popUserEntry.getUserId(), popUserEntry.getAssignWriterId(), popUserEntry);
					fm = new FacesMessage(FacesMessage.SEVERITY_INFO, CommonUtil.getMessage("retention.entry.return.to.open.withdraw.population", null),null);
				} catch (SQLException e) {
					log.error("Could not insert new pop", e);
				}
			} else { 
				try {
					if (null != user && user.getFirstDepositId() > 0) {
						PopulationsManagerBase.insertIntoPopulation(con, PopulationsManagerBase.POP_TYPE_RETENTION, popUserEntry.getContactId(), 
								0, popUserEntry.getUserId(), popUserEntry.getAssignWriterId(), popUserEntry);
						fm = new FacesMessage(FacesMessage.SEVERITY_INFO, CommonUtil.getMessage("retention.entry.return.to.retention.population", null),null);
					}
				} catch (SQLException e) {
					log.error("Could not insert new pop", e);
				}
			}	
		} else {
			log.debug("userCreation event remove Process, popEntryId: " + popUserEntry.getCurrEntryId());
			delete(con, popUserEntry, writerId,PopulationsManagerBase.POP_ENT_HIS_STATUS_REMOVED_QUALIFICATION, 0, false);
			fm = new FacesMessage(FacesMessage.SEVERITY_INFO, CommonUtil.getMessage("retention.entry.removed.from.population", null), null);
		}
		context.addMessage(null, fm);
	}

}