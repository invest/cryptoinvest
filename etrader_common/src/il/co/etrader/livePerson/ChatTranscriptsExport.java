package il.co.etrader.livePerson;

import il.co.etrader.bl_managers.IssuesManagerBase;
import il.co.etrader.bl_vos.Chat;
import il.co.etrader.dao_managers.ChatDAOBase;
import il.co.etrader.dao_managers.IssuesDAOBase;
import il.co.etrader.dao_managers.UsersDAOBase;
import il.co.etrader.dao_managers.WritersDAO;
import il.co.etrader.rss.RssReader;
import il.co.etrader.util.CommonUtil;
import il.co.etrader.util.ConstantsBase;
import il.co.etrader.util.JobUtil;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import javax.xml.datatype.DatatypeFactory;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.anyoption.common.beans.Issue;
import com.anyoption.common.beans.IssueAction;
import com.anyoption.common.util.ClearingUtil;
/**
 * ChatTranscriptsExport class.
 * Job for saving livePerson chat transcripts.
 *
 * @author Kobi
 */
public class ChatTranscriptsExport extends JobUtil{
    private static Logger log = Logger.getLogger(ChatTranscriptsExport.class);
    /**
     * Build Html of the chat
     * @param chatStartTime chat start time GMT
     * @param chatEndTime  chat end time GMT
     * @param userName userName that request the chat
     * @param linesNodes xml nodes of the chat transcript
     * @return
     */
    public static String buildChatHTML(String chatStartTime, String chatEndTime, String writerName, NodeList linesNodes) {
    	String chatHtml = "<html><body>";
		chatHtml += "<table border='0' cellpadding='0' cellspacing='0' style='font-weight:bold;color:#093755'>" +
						"<tr valign='top' height='5px' bgcolor='#c0c0c0'>" +
							"<td height='5px' colspan='3'>General Info</td>" +
						"</tr>" +
						"<tr valign='top'>" +
							"<td> Chat start time </td>" +
							"<td width='20px'>&#160;</td>" +
							"<td> " + chatStartTime + " </td>" +
						"</tr>" +
						"<tr valign='top'>" +
							"<td> Chat end time </td>" +
							"<td width='20px'>&#160;</td>" +
							"<td> " + chatEndTime + " </td>" +
						"</tr>" +
						"<tr valign='top'>" +
							"<td> Operator </td>" +
							"<td width='20px'>&#160;</td>" +
							"<td> " + writerName + " </td>" +
						"</tr>" +
						"<tr valign='top' height='10px'>" +
							"<td height='10px' colspan='3'>&#160;</td>" +
						"</tr>" +
					"</table>";

		chatHtml += "<table border='0' cellpadding='0' cellspacing='0'>" +
						"<tr valign='top' height='5px' bgcolor='#c0c0c0'>" +
							"<td height='5px' colspan='3'>Chat Transcript</td>" +
						"</tr>";
		for (int x = 0; x < linesNodes.getLength(); x++) {
			Node line = linesNodes.item(x);
			String info = ClearingUtil.getAttribute((Element)line, "", "by");
			String text = line.getChildNodes().item(0).getFirstChild().getNodeValue();
			String StyleColor = info.equals("info") ? "green" : "blue";
			chatHtml +=	"<tr valign='top'>" +
							"<td valign='top' style='font-weight:bold;color:"+StyleColor+"'>"  +
								info + ":" +
							"</td>" +
							"<td width='5px'>&#160;</td>" +
							"<td valign='top' style='color:black'>"  +
									text +
							"</td></tr>";
		}

		chatHtml += "</table></body></html>";
		return chatHtml;
    }

    public static String buildChatNonHTML(String chatStartTime, String chatEndTime, String writerName, NodeList linesNodes){
    	String newLine ="\r\n";
    	String chatNonHTML = " Chat time " + newLine;
    		   chatNonHTML += " Chat Start time " + chatStartTime + " Chat end time " + chatEndTime + newLine +
    				     	  " Operator " + writerName + newLine ;
	   
    	for (int x = 0; x < linesNodes.getLength(); x++) {
			Node line = linesNodes.item(x);
			String info = ClearingUtil.getAttribute((Element)line, "", "by");
			String text = line.getChildNodes().item(0).getFirstChild().getNodeValue();
			info = removeHtmlTagsInText(info);
			text = removeHtmlTagsInText(text);			
			chatNonHTML +=	info + ":" + text + newLine ;
		}
    	
    	return chatNonHTML;
    }
    
    private static String removeHtmlTagsInText(String text) {
    	String temp = "";
    	while(!CommonUtil.isParameterEmptyOrNull(text)){
			if(text.startsWith("<")){
				text = text.substring(text.indexOf(">") + 1);
				if(text.contains("<")){
					temp = text.substring(0, text.indexOf("<"));
				}else{
					temp = text;
				}
				text = ""; 
			} else {
				temp = text;
				text = "";
			} 	
    	}

		return temp;
	}

	/**
     * Insert new issue for chat with chatId indicator
     * @param userId
     * @param writerId
     * @param chatId id of the chat
     * @throws Exception
     */
    public static void insertIssueAndChat(Chat chat, long userId, long writerId, Date chatTime, String userOffset) throws Exception {
    	Connection con = null;
		Issue issue = new Issue();
	   	IssueAction issueAction = new IssueAction();

	   	issue.setSubjectId(String.valueOf(IssuesManagerBase.ISSUE_SUBJ_GENERAL));
		issue.setStatusId(String.valueOf(IssuesManagerBase.ISSUE_STATUS_FINISHED));
		issue.setUserId(userId);
		issue.setPriorityId(String.valueOf(IssuesManagerBase.ISSUE_PRIORITY_LOW));
		issue.setType(ConstantsBase.ISSUE_TYPE_REGULAR);
		// Filling action fields
		issueAction.setChannelId("8"); // Chat
		issueAction.setActionTypeId(String.valueOf(IssuesManagerBase.ISSUE_ACTION_CHAT));
		issueAction.setWriterId(writerId);
		issueAction.setSignificant(false);
		issueAction.setActionTime(chatTime);
		issueAction.setActionTimeOffset(userOffset);
		issueAction.setComments("Chat");

		con = getConnection("db.url","db.user","db.pass");
		try {
			con.setAutoCommit(false);
			//	Insert chat and get chatID
			ChatDAOBase.insert(con, chat);
			// Insert Issue
			IssuesDAOBase.insertIssue(con, issue);
			//	Insert Issue action
			issueAction.setIssueId(issue.getId());
			issueAction.setChatId(chat.getId());
			//IssuesManagerBase.validateAndInsertAction(con, action, user, i);
			IssuesDAOBase.insertAction(con, issueAction);
			con.commit();
    	} catch (Exception e) {
    		log.error("Problem inserting Chat transcript and issue!");
    		try {
    			con.rollback();
    		} catch (Throwable it) {
    			log.error("Can't rollback.", it);
    		}
		} finally {
			con.setAutoCommit(true);
			try {
        		con.close();
        	} catch (Exception e) {
        		log.log(Level.ERROR, "Can't close connection", e);
        	}
		}
    }

    /**
     * liveperson chat trnnscripts export Job.
     *
     * @param args
     * 			the parameter is some file that include some properties
     * @throws SQLException
     * @throws IOException
     */
    public static void main(String[] args) throws SQLException, IOException {

   	if (null == args || args.length < 1) {
            log.log(Level.FATAL, "Configuration file not specified, insert properties file name.");
            return;
        }
        propFile = args[0];

        if (log.isEnabledFor(Level.INFO)) {
            log.log(Level.INFO, "Starting the livepersonChatJob.");
        }
        String ls = System.getProperty("line.separator");
        // build dates
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        //Only after 25 hours we are getting the relevant and the most updated data from LIVEPERSON, so from now on we will run the LIVEPERSON JOB for 2 days from now.
		GregorianCalendar gc = new GregorianCalendar();
		gc.add(Calendar.DAY_OF_MONTH, -2);
		Date d = gc.getTime();
        String dateStrFrom = sdf.format(d);
        String dateStrTo = sdf.format(d);

        String fromParam = getPropertyByFile("liveperson.from", ConstantsBase.EMPTY_STRING);
        String toParam = getPropertyByFile("liveperson.to", ConstantsBase.EMPTY_STRING);
        if (!CommonUtil.isParameterEmptyOrNull(fromParam) &&
        		!CommonUtil.isParameterEmptyOrNull(toParam)) {
        	dateStrFrom = fromParam;
        	dateStrTo = toParam;
        }
        String xmlUrl = generateXMLURL(dateStrFrom, dateStrTo);
        try {
        	Document originalDocument = RssReader.getDocumentFromURL(xmlUrl);
        	//Copy original XML in order to save it
            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            DocumentBuilder db = dbf.newDocumentBuilder();
            Node originalRoot = originalDocument.getDocumentElement();
            Document copiedDocument = db.newDocument();
            Node copiedRoot = copiedDocument.importNode(originalRoot, true);
            copiedDocument.appendChild(copiedRoot);
        	String requestXML = ClearingUtil.transformDocumentToString(copiedDocument);

        	//Create xml file
        	String fileName = "TranscriptsExport-" + dateStrFrom + ".xml";
    	    File file = new File(fileName);
    	    Writer output = new BufferedWriter(new FileWriter(file));
    	    output.write(requestXML);

			Element root = originalDocument.getDocumentElement();
			NodeList sessionsNodes = root.getElementsByTagName("Session");
			for (int i = 0; i < sessionsNodes.getLength(); i++) {
				Node node = sessionsNodes.item(i);
				Node varValuesNode = ClearingUtil.getNode((Element)node, "/VarValues");
				NodeList varValueNodes = varValuesNode.getChildNodes();
				for (int y = 0; y < varValueNodes.getLength(); y++) {
					Node var = varValueNodes.item(y);
					String name = ClearingUtil.getAttribute((Element)var, "", "name");
					String invitation = null;
					if (name.equals("Invitation")) {
						invitation = var.getFirstChild().getNodeValue();
					} else if (name.equals("UserName")) {

						// search invitation chat type node if not found yet...
						if (null == invitation) {
							for (int z = y+1 ; z < varValueNodes.getLength(); z++) {
								Node chatTypeVar = varValueNodes.item(z);
								String atrrName = ClearingUtil.getAttribute((Element)chatTypeVar, "", "name");
								if (atrrName.equals("Invitation")) {
									invitation = chatTypeVar.getFirstChild().getNodeValue();
									break;
								}
							}
						}

						// Save chat transcripts
						String userName = var.getFirstChild().getNodeValue();
						log.info("Starting to save data for User chat, userName: " + userName);
						Node chatNode = ClearingUtil.getNode((Element)node, "/Chat");   // chat node

						// parsing the xml date time
						String chatStartTimeXML = ClearingUtil.getAttribute((Element)chatNode, "", "start_time");
						String chatEndTimeXML = ClearingUtil.getAttribute((Element)chatNode, "", "end_time");

						Date chatStartDateTime = DatatypeFactory.newInstance().newXMLGregorianCalendar(chatStartTimeXML).toGregorianCalendar().getTime();
						Date chatEndTimeDate = DatatypeFactory.newInstance().newXMLGregorianCalendar(chatEndTimeXML).toGregorianCalendar().getTime();

						String chatStartTime = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss").format(chatStartDateTime);
						String chatEndTime = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss").format(chatEndTimeDate);

						NodeList linesNodes = chatNode.getChildNodes();   // take chat lines
						Node repNode = ClearingUtil.getNode((Element)node, "/Reps");
						String repName = ClearingUtil.getAttribute((Element)repNode.getLastChild(), "", "repName");
						repName = repName.toUpperCase();
						String chatHtml = buildChatHTML(chatStartTime, chatEndTime, repName, linesNodes);
						String chatNonHtml = buildChatNonHTML(chatStartTime, chatEndTime, repName, linesNodes);
						Chat chat = new Chat();
						chat.setChat(chatHtml);
						chat.setNonHtmlChat(chatNonHtml);
						long chatTypeId = ConstantsBase.LIVEPERSON_CHAT_TYPE_DEFAULT;
						if (invitation != null) {
							ChatTypes chatTypes = ChatTypes.Default;
							try {
								chatTypes = ChatTypes.valueOf(invitation);
							} catch (Exception e) {
								chatTypes = ChatTypes.Default;
							} 
							//TODO change to switch on string case when using jdk7.
					        switch(chatTypes) {
					            case Invitation:
					            	chatTypeId = ConstantsBase.LIVEPERSON_CHAT_TYPE_INVITATION;
					                break;
					            case Invitation2:
					            	chatTypeId = ConstantsBase.LIVEPERSON_CHAT_TYPE_INVITATION_FAILED_DEPOSIT;
					                break;  
					            case LeaveOpenAccountInv:
					            	chatTypeId = ConstantsBase.LIVEPERSON_CHAT_TYPE_INVITATION_LEAVE_OPEN_ACCOUNT;
					            	break;
					            case LeaveFirstDepositInv:
					            	chatTypeId = ConstantsBase.LIVEPERSON_CHAT_TYPE_INVITATION_LEAVE_DEPOSIT;
					            	break;
					            case Default:
					            	chatTypeId = ConstantsBase.LIVEPERSON_CHAT_TYPE_DEFAULT;
					            	break;
					        }						
						}
						chat.setTypeId(chatTypeId);

						Connection con = null;
						long userId = 0;
						long writerId = 0;
						String userOffset = "GMT+00:00";
						try {
							con = getConnection("db.url","db.user","db.pass");

							userId = UsersDAOBase.getUserIdByUserName(con, userName);
							if (userId == 0) {
								log.warn("userId not found! userName: " + userName);
								break; // return to the sessions nodes loop
							} else { // take user Offset
								userOffset = UsersDAOBase.getUtcOffset(con, userId);
							}
							writerId = WritersDAO.getWrterIdByUserName(con, repName);
							if (writerId == 0) {
								log.warn("writerId not found, set to defualt! writerName: " + repName);
							}
						} catch(Exception e) {
							log.error("Exception inserting chat data! " + e);
							e.printStackTrace();
						} finally {
							try {
			            		con.close();
			            	} catch (Exception e) {
			            		log.log(Level.ERROR, "Can't close connection", e);
			            	}
						}
						// Insert chat and issue
						if (log.isEnabledFor(Level.INFO)) {
							log.info(ls + "About to insert issue & livepersonChat " + ls +
				                    "userId: " + userId + ls +
				                    "chatTypeId: " + chat.getTypeId() + ls +
				                    "writerId: " + writerId + ls +
				                    "chatStartDateTime: " + chatStartDateTime + ls +
				                    "userOffset: " + userOffset + ls);
				        }
						insertIssueAndChat(chat, userId, writerId, chatStartDateTime, userOffset);
						break;   // return to the sessions nodes loop
					}
				}
			}
		} catch (Exception e) {
			log.error("Problem getting chat transcripts. " + e);
		}
        if (log.isEnabledFor(Level.INFO)) {
            log.log(Level.INFO, "livepersonChatJob completed!.");
        }
    }
    
    private static String generateXMLURL(String dateStrFrom, String dateStrTo) {
    	String queryStr = getPropertyByFile("test.xml.url");
        if (CommonUtil.isParameterEmptyOrNull(queryStr)) {
        	queryStr = "https://server.iad.liveperson.net/hc/report.zip?" +
            	"cmd=chatsExport&" +
            	"site="+ getPropertyByFile("liveperson.site") +"&" +
            	"user="+ getPropertyByFile("liveperson.user") +"&" +
            	"pass="+ getPropertyByFile("liveperson.pass") +"&" +
            	"startDate="+ dateStrFrom +"T00:00:00&" +
            	"endDate="+ dateStrTo +"T23:59:59&" +
            	"toZip=false&" +
            	"format=xml";
        }
        
		return queryStr;
	}
    
	private enum ChatTypes {
    	Invitation, Invitation2, LeaveOpenAccountInv, LeaveFirstDepositInv, Default;
    }
}