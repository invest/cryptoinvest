package il.co.etrader.SalesCancelAssignJob;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Hashtable;

import org.apache.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFRichTextString;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import com.anyoption.common.bl_vos.PopulationEntryBase;

import il.co.etrader.bl_managers.IssuesManagerBase;
import il.co.etrader.bl_managers.PopulationsManagerBase;
import il.co.etrader.bl_managers.UsersManagerBase;
import il.co.etrader.bl_vos.PopulationEntryHis;
import il.co.etrader.bl_vos.Writer;
import il.co.etrader.dao_managers.PopulationsDAOBase;
import il.co.etrader.population.PopulationHandlersException;
import il.co.etrader.util.CommonUtil;
import il.co.etrader.util.JobUtil;

/**
 * [DEV-1862] - Cancel assign comma entries for retention  
 * @author EranL
 *
 */
public class SalesCancelAssignJob extends JobUtil {
	private static Logger log = Logger.getLogger(SalesCancelAssignJob.class);
	private static HSSFRow xlsRow;
	private static HSSFSheet xlsSheetCancelEntries; 
	private static HSSFSheet xlsSheetLockedAssign;
	private static HSSFWorkbook xlsWorkbook;
	private static int rowCounter;
	private static String fileName;

	public static void main(String[] args) throws Exception {
		propFile = args[0];		
		log.info("Sales cancel assign job started.");
		ArrayList<PopulationEntryBase> populationEntries = new ArrayList<PopulationEntryBase>();
		//Excel parameter
		xlsWorkbook = new HSSFWorkbook();
		xlsSheetCancelEntries = null;
		xlsSheetLockedAssign = null;
		xlsRow = null;		
		rowCounter = 0;
		fileName ="Cancel_assign_report.xls";
		String mailSubject =  getPropertyByFile("email.subject");
		//DB Connection
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;			
		try {
			con = getConnection();
			String sql =" CREATE GLOBAL TEMPORARY TABLE temp_session" +
						" 	ON COMMIT PRESERVE  ROWS AS " +
						" SELECT " +
						"	pu.id populationUserId, " +
						"	pu.curr_population_entry_id, " +
						"	pu.contact_id, " +
						"	pu.user_id, " +
						"   u.user_name userName, " +
						"   u.status_id, " +
						"	u.rank_id, " +
						"   pu.curr_assigned_writer_id, " +
						"   w.user_name, " +
						"	pu.lock_history_id " +
						" FROM " +
						"	population_users pu " +
						"		LEFT JOIN contacts c ON pu.contact_id = c.id " +
						"		LEFT JOIN users u ON pu.user_id = u.id " +
						"		LEFT JOIN population_entries pe ON pe.id = pu.curr_population_entry_id " +
						"       	LEFT JOIN populations p ON p.id = pe.population_id " +
						"	           LEFT JOIN population_types pt on   pt.id = p.population_type_id, " +
						"	writers w" +
						" WHERE " +
						"	pu.curr_assigned_writer_id = w.id " +
						"	AND w.is_active = 1 " +
						"	AND w.auto_assign = 1 " +
						"   AND u.status_id = " + UsersManagerBase.USER_STATUS_COMA + 
						"	AND pt.type_id in ( " + PopulationsManagerBase.SALES_TYPE_RETENTION + "," + PopulationsManagerBase.SALES_TYPE_IMMEDIATE_TREATMENT + ") ";
			ps = con.prepareStatement(sql);
			ps.executeUpdate();	

			String sql2 =" SELECT " +
						"	* " +
						" FROM " +
						"	temp_session ts " +
						" WHERE " +
						"	ts.user_id NOT IN (SELECT " +
						"		                    i.user_id" +
						"	                   FROM " +
						"	                  	    issues i, " +
						"	                  	    issue_actions ia, " +
						"	                       issue_action_types iat " +
						"	                    WHERE	" +
						"	                       i.id = ia.issue_id " +
						"	                       AND iat.id = ia.issue_action_type_id " +
						"	                       AND i.user_id = ts.user_id " +
						"	                       AND ia.writer_id = ts.curr_assigned_writer_id " +
						"	                       AND ia.issue_action_type_id in (" + IssuesManagerBase.ISSUE_ACTION_WILL_DEPOSIT_LATER + "," + IssuesManagerBase.ISSUE_ACTION_NOT_SURE + ") " +
						"	                       AND ia.action_time > sysdate -7 ) ";
			ps = con.prepareStatement(sql2);
			rs = ps.executeQuery();
			while (rs.next()) {
				PopulationEntryBase peb = new PopulationEntryBase();
				peb.setCurrEntryId(rs.getLong("curr_population_entry_id"));
				peb.setAssignWriterId(rs.getLong("curr_assigned_writer_id"));
				peb.setUserId(rs.getLong("user_id"));
				peb.setContactId(rs.getLong("contact_id"));		
				peb.setLockHistoryId(rs.getLong("lock_history_id"));
				peb.setPopulationUserId(rs.getLong("populationUserId"));
				populationEntries.add(peb);
			}	
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		try {
			con.close();
		} catch (Exception e) {
			log.error("Can't close",e);
		}	
		
		try {
			con = getConnection();
			String sql =" DROP TABLE temp_session ";
			ps = con.prepareStatement(sql);
			ps.executeUpdate();	
		} finally {				
			closeStatement(ps);
		} try {
			con.close();
		} catch (Exception e) {
			log.error("Can't close",e);
		}			

		//Create sheets
		xlsSheetCancelEntries = xlsWorkbook.createSheet("Cancel Entries");
		xlsSheetLockedAssign = xlsWorkbook.createSheet("Locked Entries");
		//Create Cancel Entries sheet
		xlsRow = xlsSheetCancelEntries.createRow(rowCounter++);			
		setColumnsNameInSheet(xlsRow);
		//cancel assign
		try {
			con = getConnection();
			cancelAssign(populationEntries, con);	
		} finally {
			closeResultSet(rs);
			closeStatement(ps);
		}
		try {
			con.close();
		} catch (Exception e) {
			log.error("Can't close",e);
		}									
		// send email
        Hashtable<String,String> server = new Hashtable<String,String>();
        server.put("url", getPropertyByFile("email.url"));
        server.put("auth", getPropertyByFile("email.auth"));
        server.put("user", getPropertyByFile("email.user"));
        server.put("pass", getPropertyByFile("email.pass"));
        server.put("contenttype", getPropertyByFile("email.contenttype", "text/html; charset=UTF-8"));
        Hashtable<String,String> email = new Hashtable<String,String>();
        email.put("subject", mailSubject);
        email.put("to", getPropertyByFile("email.to"));
        email.put("from", getPropertyByFile("email.from"));
        email.put("body", "The report is attached to this mail");
        File attachment = new File(fileName);
        File []attachments={attachment};
        CommonUtil.sendEmail(server, email, attachments);
        log.info("Sales cancel assign job Ended.");
	}
	
    /**
	 * Cancel assign action
	 * in case the entry locked, unlock done before the cancel
     * @param isSingleCancel TODO
     * @return
     * @throws PopulationHandlersException
     * @throws SQLException
     */
    public static void cancelAssign(ArrayList<PopulationEntryBase> populationEntries, Connection con) throws PopulationHandlersException, SQLException {
    	log.info("Start cancel assign procedure");		
    	ArrayList<PopulationEntryBase> pebLockedList = new ArrayList<PopulationEntryBase>();
    	boolean simulate = Boolean.valueOf(getPropertyByFile("simulate", "true"));
    	for (PopulationEntryBase peb : populationEntries) {
			long populationEntryId = peb.getCurrEntryId();
			if (!peb.isEntryLocked()) {
				//case when users are not in any population
				if (peb.getCurrEntryId() == 0){
					log.debug("populationEntry for " + peb.getPopulationUserId() + " is 0");
					continue;
					//populationEntryId = peb.getOldPopEntryId();
				}
				log.debug("Going to cancel assign to populationEntryId=" + populationEntryId + " populationUserId=" + peb.getPopulationUserId() + " userId=" + peb.getUserId() + " writerId=" + peb.getAssignWriterId());
				if (!simulate) {
					PopulationEntryHis popEnHis = new PopulationEntryHis();
			    	popEnHis.setPopulationEntryId(peb.getCurrEntryId());
			    	popEnHis.setWriterId(Writer.WRITER_ID_AUTO);
			    	popEnHis.setStatusId(PopulationsManagerBase.POP_ENT_HIS_STATUS_CANCEL_ASSIGNE);
			    	popEnHis.setAssignWriterId( peb.getAssignWriterId());
			    	popEnHis.setIssueActionId(0);
			    	PopulationsManagerBase.insertIntoPopulationHistory(con, popEnHis);
			    	PopulationsDAOBase.updateAssignWriter(con, peb.getPopulationUserId(), PopulationsManagerBase.POP_PUBLIC_ASSIGN, false);

				}
	    	} else { //record is locked
	    		log.warn("Warning!!! populationEntryId=" + peb.getCurrEntryId() + " userId=" + peb.getUserId() + " is locked by " + peb.getAssignWriterId());
	    		pebLockedList.add(peb);
	    	}			
			xlsRow = xlsSheetCancelEntries.createRow(rowCounter++);
			setColumnsValuesInSheet(xlsWorkbook, xlsRow, peb.getUserId(), peb.getAssignWriterId(), peb.getPopulationUserId(), peb.getCurrEntryId());
    	}    	
    	//Write locked entries		
		xlsRow = null;		
		rowCounter = 0;
		xlsRow = xlsSheetLockedAssign.createRow(rowCounter++);			
		setColumnsNameInSheet(xlsRow);    	
    	for (PopulationEntryBase peb: pebLockedList) {
			xlsRow = xlsSheetLockedAssign.createRow(rowCounter++);
			setColumnsValuesInSheet(xlsWorkbook, xlsRow, peb.getUserId(), peb.getAssignWriterId(), peb.getPopulationUserId(), peb.getCurrEntryId());    		
    	}    	
    	writeToExcel(xlsWorkbook, fileName);
    	log.info("End cancel assign procedure");
    }
    
	private static void writeToExcel(HSSFWorkbook xlsWorkbook, String fileName){
		FileOutputStream fos = null;
		try {
            fos = new FileOutputStream(new File(fileName));
            xlsWorkbook.write(fos);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (fos != null) {
                try {
                    fos.flush();
                    fos.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
	}

	private static void setColumnsNameInSheet(HSSFRow xlsRow){
		int cellCounter = 0;
		HSSFCell xlsCell = null;
		xlsCell = xlsRow.createCell(cellCounter++);
		xlsCell.setCellValue(new HSSFRichTextString("User ID"));
		xlsCell = xlsRow.createCell(cellCounter++);
		xlsCell.setCellValue(new HSSFRichTextString("Writer Id"));
		xlsCell = xlsRow.createCell(cellCounter++);
		xlsCell.setCellValue(new HSSFRichTextString("Population User Id"));
		xlsCell = xlsRow.createCell(cellCounter++);
		xlsCell.setCellValue(new HSSFRichTextString("Population Entry Id"));				
	}

	private static void setColumnsValuesInSheet(HSSFWorkbook xlsWorkbook, HSSFRow xlsRow, long userId, long writerId, long populationUserId, long populationEntryId) throws SQLException{
		int cellCounter = 0;
		HSSFCell xlsCell = null;
		HSSFCellStyle cellStyle = xlsWorkbook.createCellStyle();
		cellStyle.setWrapText(true);
		xlsCell = xlsRow.createCell(cellCounter++);
		xlsCell.setCellValue(userId);
		xlsCell = xlsRow.createCell(cellCounter++);
		xlsCell.setCellValue(writerId);
		xlsCell = xlsRow.createCell(cellCounter++);
		xlsCell.setCellValue(populationUserId);
		xlsCell = xlsRow.createCell(cellCounter++);
		xlsCell.setCellValue(populationEntryId);
        xlsCell = xlsRow.createCell(cellCounter++);
	}  
	    
}

