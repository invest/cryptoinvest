package il.co.etrader.login;

import java.security.Principal;

/**
 * <p> This class implements the <code>Principal</code> interface
 * and represents a ET user.
 *
 * <p> Principals such as this <code>ETUser</code>
 * may be associated with a particular <code>Subject</code>
 * to augment that <code>Subject</code> with an additional
 * identity.  Refer to the <code>Subject</code> class for more information
 * on how to achieve this.  Authorization decisions can then be based upon 
 * the Principals associated with a <code>Subject</code>.
 * 
 * @version 1.0 16.05.2007
 * @see java.security.Principal
 * @see javax.security.auth.Subject
 */
public class ETUser implements Principal, java.io.Serializable {
	/**
	 * @serial
	 */
	private String name;

	/**
	 * Create a ETUser with a username.
	 *
	 * @param name the username for this user.
	 * @exception IllegalArgumentException if the <code>name</code>	is <code>null</code>.
	 */
	public ETUser(String name) {
		if (null == name) {
			throw new IllegalArgumentException("illegal null input");
		}
		this.name = name;
	}

	/**
	 * Return the username for this <code>ETUser</code>.
	 *
	 * @return the username for this <code>ETUser</code>
	 */
	public String getName() {
		return name;
	}

	/**
	 * Return a string representation of this <code>ETUser</code>.
	 *
	 * @return a string representation of this <code>ETUser</code>.
	 */
	public String toString() {
		return("ETUser:  " + name);
	}

	/**
	 * Compares the specified Object with this <code>ETUser</code>
	 * for equality.  Returns true if the given object is also a
	 * <code>ETUser</code> and the two <code>ETUser</code>s
	 * have the same username.
	 *
	 * @param o Object to be compared for equality with this <code>ETUser</code>.
	 * @return true if the specified Object is equal equal to this <code>ETUser</code>.
	 */
	public boolean equals(Object o) {
		if (null == o) {
			return false;
		}

		if (this == o) {
			return true;
		}

		if (!(o instanceof ETUser)) {
			return false;
		}

		ETUser that = (ETUser)o;
		if (this.getName().equals(that.getName())) {
			return true;
		}

		return false;
	}

	/**
	 * Return a hash code for this <code>ETUser</code>.
	 *
	 * @return a hash code for this <code>ETUser</code>.
	 */
	public int hashCode() {
		return name.hashCode();
	}
}