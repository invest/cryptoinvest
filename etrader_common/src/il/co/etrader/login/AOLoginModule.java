package il.co.etrader.login;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Map;

import javax.security.auth.Subject;
import javax.security.auth.callback.Callback;
import javax.security.auth.callback.CallbackHandler;
import javax.security.auth.callback.NameCallback;
import javax.security.auth.callback.PasswordCallback;
import javax.security.auth.callback.UnsupportedCallbackException;
import javax.security.auth.login.FailedLoginException;
import javax.security.auth.login.LoginException;
import javax.security.auth.spi.LoginModule;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import com.anyoption.common.beans.User;
import com.anyoption.common.managers.BaseBLManager;
import com.anyoption.common.managers.LoginManager;
import com.anyoption.common.managers.UsersManagerBase;

/**
 * This is a custom LoginModule for the AO application. It is needed as apart
 * from the standart username and password, if the user is active (the active
 * flag in the users table).
 * skin id of the user must be diferent from etrader skin id
 */
public class AOLoginModule extends BaseBLManager implements LoginModule {
	private static final Logger log = Logger.getLogger(AOLoginModule.class);

	// initial state
	private Subject subject;
	private CallbackHandler callbackHandler;
	//private Map sharedState;
	//private Map options;

	// the authentication status
	private boolean succeeded = false;
	private boolean commitSucceeded = false;

	// username and password
	private String username;
	private char[] password;

	public AOLoginModule() {
		if (log.isEnabledFor(Level.DEBUG)) {
			log.debug("Creating AOLoginModule.");
		}
	}

	/**
	 * Initialize this <code>LoginModule</code>.
	 *
	 * @param subject the <code>Subject</code> to be authenticated.
	 * @param callbackHandler a <code>CallbackHandler</code> for communicating
     *      with the end user (prompting for user names and passwords, for
     *      example).
	 * @param sharedState shared <code>LoginModule</code> state.
	 * @param options options specified in the login <code>Configuration</code>
     *      for this particular <code>LoginModule</code>.
	 */
	@SuppressWarnings("rawtypes")
	public void initialize(Subject subject, CallbackHandler callbackHandler, Map sharedState, Map options) {
		this.subject = subject;
		this.callbackHandler = callbackHandler;
		//this.sharedState = sharedState;
		//this.options = options;
	}

	/**
	 * Authenticate the user by prompting for a user name and password.
	 *
	 * @return true in all cases since this <code>LoginModule</code> should not
     *      be ignored.
	 * @exception FailedLoginException if the authentication fails.
	 * @exception LoginException if this <code>LoginModule</code> is unable to
     *      perform the authentication.
	 */
	public boolean login() throws LoginException {
		// prompt for a user name and password
		if (callbackHandler == null) {
			throw new LoginException("Error: no CallbackHandler available");
		}

		Callback[] callbacks = new Callback[2];
		callbacks[0] = new NameCallback("user name:");
		callbacks[1] = new PasswordCallback("password:", false);
		try {
			callbackHandler.handle(callbacks);
			username = ((NameCallback) callbacks[0]).getName();
			char[] tmpPassword = ((PasswordCallback) callbacks[1]).getPassword();
			if (tmpPassword == null) {
				// treat a NULL password as an empty password
				tmpPassword = new char[0];
			}
			password = new char[tmpPassword.length];
			System.arraycopy(tmpPassword, 0, password, 0, tmpPassword.length);
			((PasswordCallback) callbacks[1]).clearPassword();
		} catch (IOException ioe) {
			throw new LoginException(ioe.toString());
		} catch (UnsupportedCallbackException uce) {
			throw new LoginException("Error: " + uce.getCallback().toString() + " not available");
		}

		if (log.isEnabledFor(Level.DEBUG)) {
			log.log(Level.DEBUG, "User name <" + username + ">");
		}

		succeeded = true;
		try {
			User user = new User();
			UsersManagerBase.loadUserByName(username, user);
			if (user.getId() > 0) { // user exists
				ArrayList<String> messages = LoginManager.loginActionFromWeb(user, new String(password));
				if (messages.isEmpty()) { // succeeded login
					succeeded = true;
				} else { // failed login
					succeeded = false;
					LoginInfoBean loginInfo = new LoginInfoBean();
					loginInfo.setErrorMessageKey(messages.get(0));
					LoginInfoThreadLocal.setLoginInfo(loginInfo);
				}
			} else { // user doesn't exists
				succeeded = false;
				if (log.isInfoEnabled()) {
					log.log(Level.INFO, "User '" + username + "' doesn't exist!");
				}
			}
		} catch (SQLException e) {
			succeeded = false;
			log.debug("SQLException while login user", e);
		} catch (Exception e) {
			succeeded = false;
			log.log(Level.ERROR, "Exception while login user", e);
		}

		if (!succeeded) {
			log.log(Level.INFO, "Login failed");
			throw new FailedLoginException("Login failed");
		}

		return true;
	}

	/**
	 * <p>
	 * This method is called if the LoginContext's overall authentication
     * succeeded (the relevant REQUIRED, REQUISITE, SUFFICIENT and OPTIONAL
     * LoginModules succeeded).
	 *
	 * <p>
	 * If this LoginModule's own authentication attempt succeeded (checked by
     * retrieving the private state saved by the <code>login</code> method),
     * then this method associates a <code>UserPrincipal</code> with the
	 * <code>Subject</code> located in the <code>LoginModule</code>. If this
     * LoginModule's own authentication attempted failed, then this method
     * removes any state that was originally saved.
     *
	 * <p>
	 * @exception LoginException if the commit fails.
	 * @return true if this LoginModule's own login and commit attempts succeeded, or false otherwise.
	 */
	public boolean commit() throws LoginException {
		log.info("commit login!");

		if (succeeded == false) {
			return false;
		} else {
			// add a ETUser (authenticated identity) to the Subject
			ETUser user = new ETUser(username);
			if (!subject.getPrincipals().contains(user)) {
				subject.getPrincipals().add(user);
			}

			// add a ETRole (authorization identitys) to the Subject
			ETRole role = new ETRole("web");
			if (!subject.getPrincipals().contains(role)) {
				subject.getPrincipals().add(role);
			}

			log.info(subject.toString());

			// in any case, clean out state
			username = null;
			for (int i = 0; i < password.length; i++) {
				password[i] = ' ';
			}
			password = null;
			commitSucceeded = true;
			return true;
		}
	}

	/**
	 * <p>
	 * This method is called if the LoginContext's overall authentication failed.
     * (the relevant REQUIRED, REQUISITE, SUFFICIENT and OPTIONAL LoginModules
     * did not succeed).
	 *
	 * <p>
	 * If this LoginModule's own authentication attempt succeeded (checked by
     * retrieving the private state saved by the <code>login</code> and
     * <code>commit</code> methods), then this method cleans up any state that
     * was originally saved.
	 *
	 * <p>
	 * @exception LoginException if the abort fails.
	 * @return false if this LoginModule's own login and/or commit attempts
     * failed, and true otherwise.
	 */
	public boolean abort() throws LoginException {
		if (succeeded == false) {
			return false;
		} else {
			if (succeeded == true && commitSucceeded == false) {
				// login succeeded but overall authentication failed
				succeeded = false;
				username = null;
				if (password != null) {
					for (int i = 0; i < password.length; i++)
						password[i] = ' ';
					password = null;
				}
			} else {
				// overall authentication succeeded and commit succeeded,
				// but someone else's commit failed
				logout();
			}
        }
		return true;
	}

	/**
	 * Logout the user.
	 *
	 * <p>
	 * This method removes the <code>ETUser</code> and <code>ETRole</code> that
     * were added by the <code>commit</code> method.
	 *
	 * @exception LoginException if the logout fails.
	 * @return true in all cases since this <code>LoginModule</code> should not
     *      be ignored.
	 */
	public boolean logout() throws LoginException {
		if (log.isEnabledFor(Level.DEBUG)) {
			log.debug("Log out user " + username);
		}

		subject.getPrincipals().removeAll(subject.getPrincipals());
		succeeded = false;
		commitSucceeded = false;
		username = null;
		if (password != null) {
			for (int i = 0; i < password.length; i++) {
				password[i] = ' ';
			}
			password = null;
		}

		return true;
	}
}