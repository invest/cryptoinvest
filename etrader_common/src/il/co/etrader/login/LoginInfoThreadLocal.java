/**
 * 
 */
package il.co.etrader.login;

/**
 * @author kirilim
 *
 */
public class LoginInfoThreadLocal {

	private static final ThreadLocal<LoginInfoBean> loginInfo = new ThreadLocal<LoginInfoBean>();

	public static LoginInfoBean getLoginInfo() {
		return loginInfo.get();
	}	
	
	public static void setLoginInfo(LoginInfoBean info) {
		loginInfo.set(info);
	}
	
	public static void clear() {
		loginInfo.remove();
	}
}
