/**
 * 
 */
package il.co.etrader.login;

/**
 * @author kirilim
 *
 */
public class LoginInfoBean {

	private String errorMessageKey;

	public String getErrorMessageKey() {
		return errorMessageKey;
	}
	
	public void setErrorMessageKey(String errorMessageKey) {
		this.errorMessageKey = errorMessageKey;
	}
}
