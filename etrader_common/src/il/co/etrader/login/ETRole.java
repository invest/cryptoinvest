package il.co.etrader.login;

import java.security.Principal;

/**
 * <p> This class implements the <code>Principal</code> interface
 * and represents a ET role.
 *
 * <p> Principals such as this <code>ETRole</code>
 * may be associated with a particular <code>Subject</code>
 * to augment that <code>Subject</code> with an additional
 * authorization.  Refer to the <code>Subject</code> class for more information
 * on how to achieve this.  Authorization decisions can then be based upon 
 * the Principals associated with a <code>Subject</code>.
 * 
 * @version 1.0 16.05.2007
 * @see java.security.Principal
 * @see javax.security.auth.Subject
 */
public class ETRole implements Principal, java.io.Serializable {
	/**
	 * @serial
	 */
	private String name;

	/**
	 * Create a ETRole with a name.
	 *
	 * @param name the name for this role.
	 * @exception IllegalArgumentException if the <code>name</code>	is <code>null</code>.
	 */
	public ETRole(String name) {
		if (null == name) {
			throw new IllegalArgumentException("illegal null input");
		}
		this.name = name;
	}

	/**
	 * Return the name for this <code>ETRole</code>.
	 *
	 * @return the name for this <code>ETRole</code>
	 */
	public String getName() {
		return name;
	}

	/**
	 * Return a string representation of this <code>ETRole</code>.
	 *
	 * @return a string representation of this <code>ETRole</code>.
	 */
	public String toString() {
		return("ETRole:  " + name);
	}

	/**
	 * Compares the specified Object with this <code>ETRole</code>
	 * for equality.  Returns true if the given object is also a
	 * <code>ETRole</code> and the two <code>ETRole</code>s
	 * have the same name.
	 *
	 * @param o Object to be compared for equality with this <code>ETRole</code>.
	 * @return true if the specified Object is equal equal to this <code>ETRole</code>.
	 */
	public boolean equals(Object o) {
		if (null == o) {
			return false;
		}

		if (this == o) {
			return true;
		}

		if (!(o instanceof ETRole)) {
			return false;
		}

		ETRole that = (ETRole)o;
		if (this.getName().equals(that.getName())) {
			return true;
		}

		return false;
	}

	/**
	 * Return a hash code for this <code>ETRole</code>.
	 *
	 * @return a hash code for this <code>ETRole</code>.
	 */
	public int hashCode() {
		return name.hashCode();
	}
}