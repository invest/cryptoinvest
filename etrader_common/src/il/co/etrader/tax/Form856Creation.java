package il.co.etrader.tax;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import com.anyoption.common.util.AESUtil;
import com.anyoption.common.util.ConstantsBase;

import il.co.etrader.bl_vos.UserBase;
import il.co.etrader.util.JobUtil;


/**
 * Create 856 Tax form.
 *
 * @author Kobi
 */
public class Form856Creation extends JobUtil {
    private static Logger log = Logger.getLogger(Form856Creation.class);

    private static StringBuffer report = new StringBuffer();

    private static Writer output = null;
    private static File file = null;
    private static long taxSum = 0;
    private static final String CRLF = "\r\n";


    private static ArrayList<UserBase> getDetailsForTaxForm(long year) throws SQLException {

        Connection con = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;

        ArrayList<UserBase> list = new ArrayList<UserBase>();

        try {
            con = getConnection(ConstantsBase.DB_URL, ConstantsBase.DB_USER, ConstantsBase.DB_PASS);

            String sql =
            	"SELECT " +
		            "ROUND((TAX.Tax/100), 0) as Tax, " +
		            "substr(u.id, 1, 14) as user_id , " +
		            "u.user_name, " +
		            "u.id_num as id_number, " +
		            "substr((u.first_name || ' ' || u.last_name),1,22) as name, " +
		            "substr((u.street  || ' ' || u.street_no),1,21) as address, " +
		            "substr(c.name,1,13) as city " +
            	"FROM  " +
		            "users u, " +
		            "cities c, " +
		            "(SELECT " +
		                  "user_id as userId, " +
		                  "sum (tax) as Tax " +
		             "FROM " +
		                  "tax_history " +
		             "WHERE " +
		                  "year = ? " +
		             "GROUP BY  " +
		                    "user_id " +
		             "HAVING " +
		                    "sum (tax) > 0 )  TAX " +
            	"WHERE " +
		            "TAX.userId = u.id AND " +
		            "u.class_id <> 0 AND " +
		            "u.city_id = c.id AND " +
		            "u.id <> 5805 " +  // Shay K
            	"ORDER BY " +
		          "u.user_name ";

            pstmt = con.prepareStatement(sql);
            pstmt.setLong(1, year);

            rs = pstmt.executeQuery();

            while ( rs.next() ) {
            	UserBase u = new UserBase();
            	u.setId(rs.getLong("user_id"));
            	u.setTaxBalance(rs.getLong("Tax"));
            	u.setUserName(rs.getString("user_name"));
            	u.setIdNum(AESUtil.decrypt(rs.getString("id_number")).substring(0, 9));
            	u.setFirstName(rs.getString("name"));
            	u.setStreet(rs.getString("address"));
            	u.setCityNameForJob(rs.getString("city"));
            	list.add(u);
            	taxSum += u.getTaxBalance();

            	// create row
            	output.write("935844852");   // Case predictions
            	output.write("96");
            	output.write(String.valueOf(year));
            	output.write(" ");
            	output.write("1");
            	output.write(characterWrapper(u.getIdNum(), "0", 9));
            	output.write(characterWrapper(String.valueOf(u.getId()), "0", 14));
            	output.write(characterWrapperRight(u.getFirstName().toUpperCase(), " ", 22));
            	output.write(characterWrapperRight(u.getStreet().toUpperCase(), " ", 21));
            	output.write(characterWrapperRight(u.getCityName().toUpperCase(), " ", 13));
            	output.write(characterWrapper(String.valueOf(u.getTaxBalance() * (int)(100 / ConstantsBase.TAX_PERCENTAGE / 100)), "0", 11));
            	output.write(characterWrapper(String.valueOf(u.getTaxBalance()), "0", 9));
            	output.write(characterWrapper("", "0", 8));
            	output.write(characterWrapper("", "0", 8));
            	output.write("20");
            	output.write(characterWrapper("", " ", 12));
            	output.write(characterWrapper("", " ", 14));
            	output.write(characterWrapper("", " ", 52));
            	output.write("01");
            	output.write("60" + CRLF);
            }

           }
        	catch (Exception e) {
        		log.log(Level.ERROR, "Error while Retrieving users from Db.", e);
            } finally {
            	try {
            		rs.close();
            	} catch (Exception e) {
            		log.log(Level.ERROR, "Can't close", e);
            	}
            	try {
            		pstmt.close();
            	} catch (Exception e) {
            		log.log(Level.ERROR, "Can't close", e);
            	}
            	try {
            		con.close();
            	} catch (Exception e) {
                log.log(Level.ERROR, "Can't close connection", e);
            	}
            }

            report.append("</table>");

        return list;

    }


    //TODO: before run, chage file to UTF-8

    /**
     * 856 tax form.
     *
     * @param args
     * 			the parameter is some file that include some properties
     * @throws SQLException
     * @throws IOException
     */
    public static void main(String[] args) throws SQLException, IOException {

    	if (null == args || args.length < 1) {
            log.log(Level.FATAL, "Configuration file not specified, insert properties file name.");
            return;
        }

        propFile = args[0];

        String year = getPropertyByFile("tax.form.year");
        String tik_nikoim = "35844852";

        String fileName = tik_nikoim + "." + year.substring(1);
        if (log.isEnabledFor(Level.INFO)) {
            log.log(Level.INFO, "Starting to build form 856. fileName: " + fileName);
        }

	    file = new File(fileName);
	    output = new BufferedWriter(new FileWriter(file));

	    // add header row
	    output.write(characterWrapper("", " ", 216));  // 216 char in each row
	    output.write(CRLF);

	    // add content rows
        ArrayList<UserBase> usersList = getDetailsForTaxForm(Long.parseLong(year));

        // add footer row
    	output.write("935844852");
    	output.write("96");
    	output.write(String.valueOf(year));
    	output.write(characterWrapper("", " ", 4));
    	output.write("0");
    	output.write("1");
    	output.write("513975854");  // private company number
    	output.write(characterWrapper("", "0", 12)); // new
    	output.write(characterWrapper("", "0", 10)); // new
    	output.write(characterWrapper("", "0", 11));
    	output.write("0037469990");
    	output.write(characterWrapper("", " ", 28));
    	output.write(characterWrapper(String.valueOf(taxSum * (int)(100 / ConstantsBase.TAX_PERCENTAGE / 100)), "0", 12)); // SUM TASHLUMIM
    	output.write(characterWrapper(String.valueOf(taxSum), "0", 10)); // MAS HCHNASA
    	output.write(characterWrapper("", "0", 9)); // MAAM
    	output.write(characterWrapper("", " ", 9));
    	output.write(characterWrapper(String.valueOf(usersList.size()), "0", 6));
    	output.write(characterWrapper(String.valueOf(usersList.size()), "0", 6));
    	output.write(characterWrapper("", " ", 50));
    	output.write("הקידב");
    	output.write(characterWrapper("", " ", 6));
    	output.write("70");
    	output.write(CRLF);


    	for (int i=1; i<12; i++) {
	    	// add 80 record
	    	output.write("935844852");
	    	output.write("96");
	    	output.write(String.valueOf(year));
	    	output.write(characterWrapper(String.valueOf(i), "0", 2));
	    	output.write(characterWrapper("", "0", 6));
	    	output.write(characterWrapper("", "0", 12));
	    	output.write(characterWrapper("", "0", 12));
	    	output.write(characterWrapper("", "0", 12));
	    	output.write(characterWrapper("", " ", 155));
	    	output.write("80");
	    	output.write(CRLF);
    	}
    	// for last month
    	output.write("935844852");
    	output.write("96");
    	output.write(String.valueOf(year));
    	output.write("12");
    	output.write(characterWrapper(String.valueOf(usersList.size()), "0", 6));
    	output.write(characterWrapper(String.valueOf(taxSum * (int)(100 / ConstantsBase.TAX_PERCENTAGE / 100)), "0", 12));
    	output.write(characterWrapper(String.valueOf(taxSum), "0", 12));
    	output.write(characterWrapper("", "0", 12));
    	output.write(characterWrapper("", " ", 155));
    	output.write("80");
    	output.write(CRLF);



    	output.flush();
        output.close();

	    if (log.isEnabledFor(Level.INFO)) {
	        log.log(Level.INFO, "Tax Form creation completed.");
	    }

	 }

    /**
     * Text wrapper left by character and column size
     * @param text
     * @param character
     * @param fieldSize
     * @return
     */
    public static String characterWrapper(String text, String character, long fieldSize) {
    	String out = "";
    	long len = fieldSize - text.length();
    	for (int i = 0 ; i < len ; i ++) {
    		out += character;
    	}
    	out += text;
		return out;
    }

    /**
     * Text wrapper right by character and column size
     * @param text
     * @param character
     * @param fieldSize
     * @return
     */
    public static String characterWrapperRight(String text, String character, long fieldSize) {
    	String out = text;
    	long len = fieldSize - text.length();
    	for (int i = 0 ; i < len ; i ++) {
    		out += character;
    	}
		return out;
    }

}
