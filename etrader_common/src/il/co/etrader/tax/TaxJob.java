package il.co.etrader.tax;

import il.co.etrader.bl_vos.TaxHistory;
import il.co.etrader.util.CommonUtil;
import il.co.etrader.util.ConstantsBase;
import il.co.etrader.util.JobUtil;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.Hashtable;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import com.anyoption.common.daos.GeneralDAO;


/**
 * TaxJob class.
 * This class represent the job for tax.
 *
 * @author Kobi
 */
public class TaxJob extends JobUtil {
    private static Logger log = Logger.getLogger(TaxJob.class);

    private static boolean isNeedPositiveCheck = false;
    private static ReportSummery summery = new ReportSummery();
    private static StringBuffer report = new StringBuffer();
    private static final String ISRAEL_CURRENCY = "\u20AA";


    /**
     * Retrieve all users for tax payment. this function return ArrayList of TaxHistory
     * each TaxHistory in the List construct to specific user.
     *
     * @param period
     * 			the period of the tax.(TAX_JOB_PERIOD1/TAX_JOB_PERIOD2)
     * @throws SQLException
     */
    private static ArrayList<TaxHistory> retrieveUsersForPeriod(String period, String year) throws SQLException {

        report.append("<br><b>TaxHistory Information for Period: " + period + ", Year: " + year + " :</b><table border=\"1\">");

        report.append("<tr><td>TaxHistory id</td><td>UserId</td><td>Tax</td><td>Win</td>" +
	       			  "<td>Lose</td><td>Win-Lose</td></tr>");

    	String from = "";
    	String to = "";
    	//String fromPeriod2 = "";
    	String utsOffset = "";

        Connection con = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;

        ArrayList<TaxHistory> list = new ArrayList<TaxHistory>();

        // Checking the period and set the dates
        /*if ( period.equals( TaxUtil.TAX_JOB_PERIOD1 ) ) {
        	from = TaxUtil.TAX_JOB_PERIOD1_START_PART + year;
        	to = TaxUtil.TAX_JOB_PERIOD1_END_PART + year;
        	isNeedPositiveCheck = true;
        	utsOffset = ConstantsBase.UTCOFFSET_DEFAULT_PERIOD1;
        }
        else {  // TAX_JOB_PERIOD2
        	from = TaxUtil.TAX_JOB_PERIOD2_START_PART + year;
        	to = TaxUtil.TAX_JOB_PERIOD2_END_PART + year;
        	fromPeriod2 = TaxUtil.TAX_JOB_PERIOD2_FROM + year;   // in case we need to take only period2 tax
        	utsOffset = ConstantsBase.UTCOFFSET_DEFAULT_PERIOD2;
        }*/

        from = ConstantsBase.TAX_JOB_PERIOD1_START_PART + year;
    	to = ConstantsBase.TAX_JOB_PERIOD2_END_PART + year;
    	isNeedPositiveCheck = false;
    	utsOffset = ConstantsBase.UTCOFFSET_DEFAULT_PERIOD2;
    	
        try {
            con = getConnection(ConstantsBase.DB_URL, ConstantsBase.DB_USER, ConstantsBase.DB_PASS);

            String sql = "SELECT " +
            				"u.id userId, " +
            				"SUM(i.win-i.lose) + SUM(NVL(t.amount,0)) AS win_lose, " +
            				"SUM(i.win) + SUM(NVL(t.amount,0)) win, "+
            				"SUM(i.lose) lose, " +
            				"u.email  email, " +
            				"SUM(i.amount) sumInvestments "+
            			 "FROM " +
            			  	"users u, " +
            			  	"investments i " +
            			  		"LEFT JOIN transactions t ON t.bonus_user_id = i.bonus_user_id AND i.bonus_odds_change_type_id > 1 " +
            			 "WHERE " +
            			  	"u.id = i.user_id AND " +
            			  	"u.class_id <> 0 AND " +
            			  	"is_settled = 1 AND " +
            			  	"is_canceled = 0 AND " +
            			  	"u.tax_exemption = 0 AND " +
            			  	"u.skin_id = 1 AND ";
	          	//run on specific user
				if (!CommonUtil.isParameterEmptyOrNull(getPropertyByFile("user.id", ConstantsBase.EMPTY_STRING))) {
						sql+=	" u.id = ? AND ";
				}
				sql +=
            			  	"NOT (i.bonus_odds_change_type_id = 1 and i.lose > 0) AND " +

            			  	"((EXTRACT (MONTH FROM i.time_settled) BETWEEN " + ConstantsBase.GMT_2_TO_3_FROM_MONTH + " AND " +
            			  													 + ConstantsBase.GMT_2_TO_3_TO_MONTH + " AND " +

	            			   "trunc(i.time_settled " + ConstantsBase.GMT_3_OFFSET_DELTA + " ) >= trunc(to_date('"+from+"','DDMMYYYY')) AND " +
	            			   "trunc(i.time_settled " + ConstantsBase.GMT_3_OFFSET_DELTA + " ) <= trunc(to_date('"+to+"','DDMMYYYY'))) " +

            			  	"OR " +

            			  	"(EXTRACT (MONTH FROM i.time_settled) NOT BETWEEN " + ConstantsBase.GMT_2_TO_3_FROM_MONTH + " AND " +
								 											    + ConstantsBase.GMT_2_TO_3_TO_MONTH + " AND " +

							   "trunc(i.time_settled " + ConstantsBase.GMT_2_OFFSET_DELTA + " ) >= trunc(to_date('"+from+"','DDMMYYYY')) AND " +
        			  		   "trunc(i.time_settled " + ConstantsBase.GMT_2_OFFSET_DELTA + " ) <= trunc(to_date('"+to+"','DDMMYYYY')))) " +

            			 "GROUP BY " +
            			 	"u.id," +
            			 	"u.email ";

            if (isNeedPositiveCheck) {
            	sql += "HAVING (SUM(i.win-i.lose) + SUM(NVL(t.amount,0))) > 0 ";
            }

            pstmt = con.prepareStatement(sql);
            if (!CommonUtil.isParameterEmptyOrNull(getPropertyByFile("user.id", ConstantsBase.EMPTY_STRING))) {
				pstmt.setString(1, getPropertyByFile("user.id"));
			}
            rs = pstmt.executeQuery();

            while ( rs.next() ) {

            	TaxHistory t = new TaxHistory();  // create new TaxHistory instance

                t.setId( TaxUtil.getSeqNextValue(con, "seq_tax_history") );
            	t.setUser_id(rs.getLong("userId"));
            	t.setPeriod(period);
            	t.setTax(Math.round	( rs.getLong("win_lose") * ConstantsBase.TAX_PERCENTAGE ));
            	t.setWin(rs.getLong("win"));
            	t.setLose(rs.getLong("lose"));
            	t.setTime_created(new GregorianCalendar().getTime());
            	t.setWriter_id(ConstantsBase.TAX_JOB_WRITER_ID);
            	t.setYear(Long.valueOf(year));
            	t.setUtcOffsetCreated(utsOffset);
            	t.setSumInvest(rs.getLong("sumInvestments"));

            	/*if ( period.equals(TaxUtil.TAX_JOB_PERIOD2)) {    // calculate tax in case user paid mid-year tax

            		Connection con2 = null;
    		  		PreparedStatement ps2 = null;
    				ResultSet rst2 = null;

            		try {
	            		con2 = getConnection(TaxUtil.DB_URL, TaxUtil.DB_USER, TaxUtil.DB_PASS);

					    String sql2 =
					    	  "SELECT " +
					    	  	"SUM(i.win-i.lose) + SUM(NVL(t.amount,0)) AS win_lose, " +
					    		"SUM(i.win) + SUM(NVL(t.amount,0)) win, " +
					    		"SUM(i.lose) lose " +
							  "FROM " +
							    "users u " +
							    	"LEFT JOIN investments i ON u.id = i.user_id AND " +
				                        "i.is_settled = 1 AND " +
				                        "i.is_canceled = 0 AND " +
				                        "NOT (i.bonus_odds_change_type_id = 1 and i.lose > 0) AND " +

			            			  	"((EXTRACT (MONTH FROM i.time_settled) BETWEEN " + TaxUtil.GMT_2_TO_3_FROM_MONTH + " AND " +
											                                             + TaxUtil.GMT_2_TO_3_TO_MONTH + " AND " +

										  "trunc(i.time_settled " + TaxUtil.GMT_3_OFFSET_DELTA + " ) >= trunc(to_date('"+fromPeriod2+"','DDMMYYYY')) AND " +
										  "trunc(i.time_settled " + TaxUtil.GMT_3_OFFSET_DELTA + " ) <= trunc(to_date('"+to+"','DDMMYYYY'))) " +

										"OR " +

										"(EXTRACT (MONTH FROM i.time_settled) NOT BETWEEN " + TaxUtil.GMT_2_TO_3_FROM_MONTH + " AND " +
																				            + TaxUtil.GMT_2_TO_3_TO_MONTH + " AND " +

										  "trunc(i.time_settled " + TaxUtil.GMT_2_OFFSET_DELTA + " ) >= trunc(to_date('"+fromPeriod2+"','DDMMYYYY')) AND " +
										  "trunc(i.time_settled " + TaxUtil.GMT_2_OFFSET_DELTA + " ) <= trunc(to_date('"+to+"','DDMMYYYY')))) " +

										  	"LEFT JOIN transactions t ON t.bonus_user_id = i.bonus_user_id AND i.bonus_odds_change_type_id > 1 " +

				              "WHERE " +
							  	"u.class_id <> 0 AND " +
							    "u.tax_exemption = 0 AND " +
							    "u.skin_id = 1 AND " +
							    "u.id = ? AND " +
							    "EXISTS (select tax from tax_history " +
							            "where user_id = ? and period = ? " +
							            "and year = ? ) " +
							  "GROUP BY " +
							  	"u.id ";

					    ps2 = con2.prepareStatement(sql2);
					    ps2.setLong(1, t.getUser_id());
					    ps2.setLong(2, t.getUser_id());
					    ps2.setString(3, TaxUtil.TAX_JOB_PERIOD1);
					    ps2.setLong(4, Long.valueOf(year));
					    rst2 = ps2.executeQuery();

					    if (rst2.next()) {   // in case we take tax form this user for period1
				            t.setTax(Math.round( rst2.getLong("win_lose") * ConstantsBase.TAX_PERCENTAGE ));
				            t.setWin(rst2.getLong("win"));
				            t.setLose(rst2.getLong("lose"));
					    }

            		} catch (Exception e) {
                		log.log(Level.ERROR, "Error while Retrieving period2 tax user for tax calculation: " +
                				t.getUser_id() + ", " + e);
                    } finally {
                    	try {
                    		rst2.close();
                    		ps2.close();
                    		con2.close();
                    	} catch (Exception e) {
                    		log.log(Level.ERROR, "Can't close ", e);
                    	}
                    }
            	}*/

            	if ( t.getTax() < 0 ) {    // zero tax field, in case we get negative (win-lose)*tax_precentage
            		t.setTax(0);
            	}

            	list.add(t);   // add user to taxHistory list

            	// add to report
                  report.append("<tr>");
                  report.append("<td>").append(t.getId()).append("</td>");
                  report.append("<td>").append(t.getUser_id()).append("</td>");
                  report.append("<td>").append(displayAmount(t.getTax(),false)).append("</td>");
                  report.append("<td>").append(displayAmount(t.getWin(),false)).append("</td>");
                  report.append("<td>").append(displayAmount(t.getLose(),false)).append("</td>");
                  report.append("<td>").append(displayAmount( (t.getWin()-t.getLose()),false)).append("</td>");
                  report.append("</tr>");
                  summery.addTaxBalance(t.getTax());
            }

           }
        	catch (Exception e) {
        		log.log(Level.ERROR, "Error while Retrieving users from Db.", e);
            } finally {
            	try {
            		rs.close();
            	} catch (Exception e) {
            		log.log(Level.ERROR, "Can't close", e);
            	}
            	try {
            		pstmt.close();
            	} catch (Exception e) {
            		log.log(Level.ERROR, "Can't close", e);
            	}
            	try {
            		con.close();
            	} catch (Exception e) {
                log.log(Level.ERROR, "Can't close connection", e);
            	}
            }

            report.append("</table>");

        return list;

    }



    /**
     * Update the tax_balance to zero for all users that pay tax
     * isNeedPositiveCheck is a member of the class.
     */
    public static void updateUserTaxBalance() {

            Connection con = null;
            PreparedStatement pstmt = null;

            if (log.isEnabledFor(Level.DEBUG)) {
                log.log(Level.INFO, "Going to update tax_balance to users");
            }

    		try {
    			con = getConnection(ConstantsBase.DB_URL, ConstantsBase.DB_USER, ConstantsBase.DB_PASS);

    			String sql = "update users set tax_balance=? , win_lose = 0, time_modified=sysdate where class_id<>0 "+
    						 "and tax_exemption=? ";


    			// need to check if the tax_balance is positive
    			if ( isNeedPositiveCheck ) {
    				sql += "and tax_balance > 0";

    			}

    			pstmt = con.prepareStatement(sql);

    			pstmt.setLong(1, ConstantsBase.ZERO_TAX_BALANCE);
    			pstmt.setInt(2, ConstantsBase.NO_EXEMPTION);

    			pstmt.executeUpdate();

    		}
    		catch (Exception e) {
                log.log(Level.ERROR, "Error while updating users.", e);
            }
    		finally {
    				try {
    					pstmt.close();
    				} catch (Exception e) {
    					log.log(Level.ERROR, "Can't close", e);
    				}
    				try {
                		con.close();
                	} catch (Exception e) {
                    log.log(Level.ERROR, "Can't close connection", e);
                	}
    		}
    }


    /**
     * Insert row to TAX_BALANCE table
     *
     * @param taxHistory
     * 				its the information of some user(row in table TaxHistory)
     */
    public static void insertToTaxHistory(TaxHistory taxHistory) {

            Connection con = null;
            PreparedStatement pstmt = null;

            if (log.isEnabledFor(Level.DEBUG)) {
                log.log(Level.INFO, "Going to insert data of some user to tax_history table, userId: " + taxHistory.getUser_id());
            }

    		try {
    			con = getConnection(ConstantsBase.DB_URL, ConstantsBase.DB_USER, ConstantsBase.DB_PASS);

    			String sql =
					"insert into tax_history(id,user_id,period,tax,win,lose,time_created," +
											"writer_id,year,utc_offset_created,sum_investments) "+

					"values (?,?,?,?,?,?,?,?,?,?,?)";

    			pstmt = con.prepareStatement(sql);

    			pstmt.setLong(1, taxHistory.getId());
    			pstmt.setLong(2, taxHistory.getUser_id());
    			pstmt.setString(3, taxHistory.getPeriod());
    			pstmt.setLong(4, taxHistory.getTax());
    			pstmt.setLong(5, taxHistory.getWin());
    			pstmt.setLong(6, taxHistory.getLose());
    			pstmt.setTimestamp(7, CommonUtil.convertToTimeStamp(taxHistory.getTime_created()));
    			pstmt.setLong(8, taxHistory.getWriter_id());
    			pstmt.setLong(9, taxHistory.getYear());
    			pstmt.setString(10, taxHistory.getUtcOffsetCreated());
    			pstmt.setLong(11, taxHistory.getSumInvest());

    			pstmt.executeUpdate();

    		}
    		catch (Exception e) {
                log.log(Level.ERROR, "Error while insert rows to tax_history table in DB.", e);
            }
    		finally {
                try {
                    pstmt.close();
                } catch (Exception e) {
                    log.log(Level.ERROR, "Can't close", e);
                }
                try {
            		con.close();
            	} catch (Exception e) {
                log.log(Level.ERROR, "Can't close connection", e);
            	}
    		}
    }


    /**
     * Insert row to BALANCE_HISTORY table
     *
     * @param taxHistory
     * 				its the information of some user(row in table TaxHistory)
     * This function call to insertBalanceLog function in GeneralDAO class.
     * This is done for the backend tracking
     */
    public static void insertToBalanceHistory(TaxHistory taxHistory) {

    	  Connection con = null;

          if (log.isEnabledFor(Level.DEBUG)) {
              log.log(Level.INFO, "Going to insert data of some user to balance_history table, userId: " + taxHistory.getUser_id());
          }

  		try {
  			con = getConnection(ConstantsBase.DB_URL, ConstantsBase.DB_USER, ConstantsBase.DB_PASS);

  			GeneralDAO.insertBalanceLog(con, ConstantsBase.TAX_JOB_WRITER_ID,
  					taxHistory.getUser_id(), ConstantsBase.TABLE_TAX_HISTORY,
  					taxHistory.getId(),ConstantsBase.LOG_BALANCE_TAX_PAYMENT, taxHistory.getUtcOffsetCreated());

  		}
  		catch (Exception e) {
              log.log(Level.ERROR, "Error while insert rows to tax_history table in DB.", e);
         }
  		finally {
              try {
          		con.close();
          	} catch (Exception e) {
              log.log(Level.ERROR, "Can't close connection", e);
          	}
  		}
  }


    /**
     * Tax Job.
     *
     * @param args
     * 			the parameter is some file that include some properties
     * @throws SQLException
     * @throws IOException
     */
    public static void main(String[] args) throws SQLException, IOException {

    	if (null == args || args.length < 1) {
            log.log(Level.FATAL, "Configuration file not specified, insert properties file name.");
            return;
        }

        propFile = args[0];

        // get the period type from the file parameter
        String period = getPropertyByFile("tax.job.period.key");
        String year = getPropertyByFile("tax.job.period.year");

        if (log.isEnabledFor(Level.INFO)) {
            log.log(Level.INFO, "Starting the Tax job.");
        }

        ArrayList<TaxHistory> usersList = retrieveUsersForPeriod(period, year);

        if (log.isEnabledFor(Level.INFO)) {
            log.log(Level.INFO, "Starting to insert data to Tax_History table.");
        }

        // go over the usersList and insert each TaxHistory that represent row, to table tax_history
        for ( TaxHistory t : usersList ) {
        	insertToTaxHistory(t);
        }

        if (log.isEnabledFor(Level.INFO)) {
            log.log(Level.INFO, "Starting to update tax_balance column in users table.");
        }

        // Update tax_balance column for the users
        updateUserTaxBalance();

        // add to report - the users that we zero their tax balance
        report.append("<table border=\"0\"><tr><td><br/></td></tr><tr><td><b>*Update tax_balance field to zero for this users</b></tr></td></table>");

        // go over the usersList and insert information to balance_history table
        for ( TaxHistory t : usersList ) {
        	insertToBalanceHistory(t);
        }

        //  add to report
        report.append("<table border=\"0\"><tr><td><br/></td></tr><tr><td><b>*Add row to balance_history table for each user that have tax</b></tr></td></table></br></br>");

        // build the report
        String reportBody = "<html><body>" +
        						report.toString() +
        						summery.getSummery(period) +
        					"</body></html>";


	    if (log.isEnabledFor(Level.INFO)) {
	        log.log(Level.INFO, "Sending report email: " + reportBody);
	    }

	    // create file that contain the report.
	    // the file name include the tax period and the year
	    Writer output = null;
	    File file = new File("taxJob_"+period+"_"+year+".html");
	    output = new BufferedWriter( new FileWriter(file) );
	    output.write(reportBody);
	    output.close();

	    //  send email
	    Hashtable<String,String> server = new Hashtable<String,String>();
	    server.put("url", getPropertyByFile("tax.email.url"));
	    server.put("auth", getPropertyByFile("tax.email.auth"));
	    server.put("user", getPropertyByFile("tax.email.user"));
	    server.put("pass", getPropertyByFile("tax.email.pass"));
	    server.put("contenttype", getPropertyByFile("tax.email.contenttype", "text/html; charset=UTF-8"));

	    Hashtable<String,String> email = new Hashtable<String,String>();
	    email.put("subject", getPropertyByFile("tax.email.subject"));
	    email.put("to", getPropertyByFile("tax.email.to"));
	    email.put("from", getPropertyByFile("tax.email.from"));
	    email.put("body", reportBody);
	    sendEmail(server, email);

	    if (log.isEnabledFor(Level.INFO)) {
	        log.log(Level.INFO, "Tax Job completed.");
	    }

	 }
    
    //Quick fix due to change in commonUtil.displayAmount - use other db connection.
    /**
	 * DisplayAmount function
     *
	 * @param amount amount to display
	 * @param showCurrency if needed to show currency symbol
	 * @return
	 */
	public static String displayAmount(long amount, boolean showCurrency) {
		DecimalFormat sd = new DecimalFormat("###,###,##0.00");
		double amountDecimal = amount;
		amountDecimal /= 100;
		String out = "";
		if (showCurrency) {
			out = ISRAEL_CURRENCY + sd.format(amountDecimal);	
		} else {
			out = sd.format(amountDecimal);
		}
		return out;
	}
}

/**
 * class that report summery.
 *
 * @author Kobi
 */
class ReportSummery {

    private long TaxBalanceSummary = 0;

    /**
     * add the tax balance of some user to TaxBalanceSummary
     *
     * @param Taxamount
     * 			TaxBalance of some user
     */
    void addTaxBalance(long Taxamount) {
    	if ( Taxamount > 0 ) {
    		TaxBalanceSummary += Taxamount;
    	}
    }

    /**
     * Create the summery part of the report.
     *
     * @return HTML representing the summery part of the report.
     */
    String getSummery(String period) {
    	StringBuffer sb = new StringBuffer("<table border=\"1\">");
    	sb.append("<th>TaxBalance Summary:</th>");
        sb.append("<tr><td>Period-Type</td><td>Total-Tax</td></tr>");
        sb.append("<td>").append(period).append("</td>");
        sb.append("<td>").append(TaxJob.displayAmount(TaxBalanceSummary,false)).append("</td>");
        sb.append("</table>");
        return sb.toString();
    }
}
