package il.co.etrader.bl_managers;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import org.apache.log4j.Logger;

import com.anyoption.common.bl_vos.PopulationEntryBase;

import il.co.etrader.bl_vos.UserBase;
import il.co.etrader.bl_vos.Writer;
import il.co.etrader.dao_managers.PopulationEntriesDAOBase;
import il.co.etrader.population.PopulationHandlersException;

public class PopulationEntriesManagerBase extends PopulationsManagerBase {
	private static final Logger log = Logger.getLogger(PopulationEntriesManagerBase.class);
	public static final String ENUM_REMOVE_FROM_POPULATION = "remove_from_population";
	public static final String CODE_NUM_DAYS_LAST_DECLINE = "num_days_last_decline";

    public static ArrayList<PopulationEntryBase> getAllEntries(Connection con, String populationTypes, Long skinIds, long businessSkin, long languageId,
    				long assignedWriterId, long campaignId, String countries, Date fromDate,
    				Date toDate, int colorType, boolean isAssigned, long userId, String userName, long contactId,
    				long supportMoreOptionsType, String timeZone, String priorityId, String excludeCountrries,boolean hideNoAnswer,
    				String callsCount, String countriesGroup, boolean isNotCalledPops, String includeAffiliates, String excludeAffiliates, int verifiedUsers,
    				long lastSalesDepRepId, int campaignPriority, boolean isSpecialCare, Date fromLogin, Date toLogin, long lastLoginInXMinutes,
    				boolean balanceBelowMinInvestAmount, boolean madeDepositButdidntMakeInv24HAfter, long userStatusId, long populationDept, long userRankId, boolean isFromJob,
    				long retentionTeamId, long everCalled, String sortAmountColumn, String userPlatform, long countryGroupTierId) throws SQLException {
   		    		    		
    		return PopulationEntriesDAOBase.getAllEntries(con, populationTypes, skinIds.toString(), businessSkin, languageId, assignedWriterId, campaignId, countries,
    				fromDate, toDate, colorType, isAssigned, userId, userName, contactId, supportMoreOptionsType, timeZone, priorityId, excludeCountrries, hideNoAnswer, 
    				callsCount, countriesGroup, isNotCalledPops, includeAffiliates, excludeAffiliates, verifiedUsers, lastSalesDepRepId, campaignPriority, isSpecialCare,
    				fromLogin, toLogin, lastLoginInXMinutes, balanceBelowMinInvestAmount, madeDepositButdidntMakeInv24HAfter, userStatusId, populationDept, userRankId, isFromJob,true, 0L,
    				retentionTeamId, everCalled, sortAmountColumn, userPlatform, countryGroupTierId); 
    } 
    
    /**
     *  Return coma representatives that are participants with auto assignment job and their  status and rank distribution 
     * @param con
     * @return
     * @throws SQLException
     */
    public static HashMap<Long, Writer> getComaRepSI(Connection con, long specificWriterId) throws SQLException {
    	return PopulationEntriesDAOBase.getComaRepSI(con, specificWriterId); 
    }
    
    /**
     * Return Initial Stage representatives that are participants with auto assignment job
     * @param con
     * @param specificWriterId
     * @return
     * @throws SQLException
     */
    public static HashMap<Long, Writer> getInitialStageRepSI(Connection con, long specificWriterId) throws SQLException {
    	return PopulationEntriesDAOBase.getInitialStageRepSI(con, specificWriterId); 
    }
    
    public static boolean assign(Connection con, PopulationEntryBase populationEntry, long writerId) throws PopulationHandlersException, SQLException {
        long populationEntryId = populationEntry.getCurrEntryId();
        boolean res = false;
        if (!PopulationsManagerBase.isLocked(con, populationEntry.getPopulationUserId())) {
	        PopulationsManagerBase.insertIntoPopulationEntriesHis(con, populationEntryId, writerId,
	                                        PopulationsManagerBase.POP_ENT_HIS_STATUS_ASSIGNED, populationEntry.getAssignWriterId(), 0);
	
	        PopulationsManagerBase.updateAssignWriter(con, populationEntry.getPopulationUserId(), populationEntry.getAssignWriterId());
	        res = true;
        }
        return res;
    }

    /**
     * Get pop entries for update pop
     * @param peb
     * @param user
     * @return
     * @throws SQLException
     */
    public static ArrayList<PopulationEntryBase> getPopEntriesForUpdatePop(PopulationEntryBase peb, UserBase user) throws SQLException  {
    	log.info("Get population entries for update population - START.");
    	Connection con = null;
		try {
			con = getConnection();
			return PopulationEntriesDAOBase.getPopEntriesForUpdatePop(con, peb, user);
		} finally {
			closeConnection(con);
			log.info("Get population entries for update population - END.");
		}
    }
    
    public static boolean isRetentionPopulation(long userId) throws SQLException  {
    	log.info("check if user is in retention population - START.");
    	Connection con = null;
		try {
			con = getConnection();
			return PopulationEntriesDAOBase.isRetentionPopulation(con, userId);
		} finally {
			closeConnection(con);
			log.info("check if user is in retention population - END.");
		}
    }
}