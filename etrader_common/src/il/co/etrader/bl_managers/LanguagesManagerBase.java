//package il.co.etrader.bl_managers;
//
//import java.sql.Connection;
//import java.sql.SQLException;
//import java.util.ArrayList;
//
//import org.apache.log4j.Logger;
//
//import com.anyoption.common.beans.base.Language;
//import com.anyoption.common.daos.LanguagesDAOBase;
//import com.anyoption.common.managers.BaseBLManager;
//
//public class LanguagesManagerBase extends BaseBLManager {
//	private static final Logger log = Logger.getLogger(LanguagesManagerBase.class);
//
//	public static String getCodeById(long languageId) throws SQLException {
//		Connection con = getConnection();
//		try {
//			return LanguagesDAOBase.getCodeById(con, languageId);
//		} finally {
//			closeConnection(con);
//		}
//	}
//
//	public static ArrayList<Language> getAllLanguages() {
//		Connection con;
//		try {
//			con = getConnection();
//		} catch (Exception e) {
//			log.debug("cant connect to DB to load languages", e);
//			return null;
//		}
//
//		try {
//			return LanguagesDAOBase.getAll(con);
//		} catch(SQLException e) {
//			log.debug("cant load Languages for charts" , e);
//			return null;
//		}
//		finally {
//			closeConnection(con);
//		}
//	}
//}