package il.co.etrader.bl_managers;

import il.co.etrader.bl_vos.UserClass;
import il.co.etrader.dao_managers.UsersDAOBase;
import il.co.etrader.util.CommonUtil;
import il.co.etrader.util.ConstantsBase;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.HashMap;

import javax.faces.model.SelectItem;

import com.anyoption.common.beans.base.Currency;
import com.anyoption.common.beans.base.Skin;
import com.anyoption.common.bl_vos.BankBase;
import com.anyoption.common.bl_vos.ChargeBack;
import com.anyoption.common.daos.BanksDAOBase;
import com.anyoption.common.daos.CurrenciesDAOBase;
import com.anyoption.common.managers.BaseBLManager;
import com.anyoption.common.managers.ChargeBacksManagerBase;

public class CacheObjects extends BaseBLManager{

	//Application Objects HM
	public static HashMap applicationObjectsHm;

	//Application SI's
	public static HashMap<String, ArrayList<SelectItem>> applicationSIHm;

	//objects
	protected HashMap banks;
	protected HashMap currencies;
	protected HashMap userClasses;
	protected HashMap chargebackStatuses;

	//Si's
	protected ArrayList<SelectItem> banksSI;
	protected ArrayList<SelectItem> currenciesSI;
	protected ArrayList<SelectItem> userClassesSI;
	protected ArrayList<SelectItem> chargebackStatusesSI;


	public CacheObjects() throws Exception {
		initObjects();
		initRefrences();
	}

	private void initObjects() throws Exception {

		Connection con = null;
		try {
			 con = getConnection();

			//init HashMaps
			applicationObjectsHm = new HashMap();
			applicationSIHm = new HashMap();

			//Application Objects

			HashMap hm = new HashMap();
			ArrayList<SelectItem> si = new ArrayList<SelectItem>();

			//banks
	   		hm = BanksDAOBase.getBanks(con, ConstantsBase.BANK_TYPE_DEPOSIT, Skin.SKIN_BUSINESS_ETRADER);
	   		si = CommonUtil.hashMap2SelectListString(hm, "getId", "getName");
			applicationObjectsHm.put("banks", (HashMap<Long,BankBase>)hm.clone());
			applicationSIHm.put("banksSI", (ArrayList<SelectItem>)si.clone());
			hm.clear();
			si.clear();

			//currencies
	   		hm = CurrenciesDAOBase.getAllCurrencies(con);
	   		si = CommonUtil.hashMap2SelectListString(hm, "getId", "getDisplayName");
			applicationObjectsHm.put("currencies", (HashMap<Long,BankBase>)hm.clone());
			applicationSIHm.put("currenciesSI", (ArrayList<SelectItem>)si.clone());
			hm.clear();
			si.clear();

	   		//user classes
			hm = UsersDAOBase.getUserClasses(con);
			si = CommonUtil.hashMap2SelectList(hm, "getId", "getName");
			applicationObjectsHm.put("userClasses", (HashMap<Long,UserClass>)hm.clone());
			applicationSIHm.put("userClassesSI", (ArrayList<SelectItem>)si.clone());
			hm.clear();
			si.clear();

	   		//user classes SI (String)
			hm = UsersDAOBase.getUserClasses(con);
			si = CommonUtil.hashMap2SelectListString(hm, "getId", "getName");
			applicationSIHm.put("userClassesStringSI", (ArrayList<SelectItem>)si.clone());
			hm.clear();
			si.clear();

			//chargebacks
			hm = ChargeBacksManagerBase.getAllStatuses();
			si = CommonUtil.hashMap2SelectList(hm, "getStatusId", "getStatusName");
			applicationObjectsHm.put("chargebackStatuses", (HashMap<Integer,ChargeBack>)hm.clone());
			applicationSIHm.put("chargebackStatusesSI", (ArrayList<SelectItem>)si.clone());
		} finally {
			closeConnection(con);
		}

	}

	private void initRefrences() {

		banks = (HashMap)applicationObjectsHm.get("banks");
		banksSI = (ArrayList<SelectItem>)applicationSIHm.get("banksSI");

		currencies = (HashMap)applicationObjectsHm.get("currencies");
		currenciesSI = (ArrayList<SelectItem>)applicationSIHm.get("currenciesSI");

		userClasses = (HashMap)applicationObjectsHm.get("userClasses");
		userClassesSI = addUserClassesSpecialOptions((ArrayList)applicationSIHm.get("userClassesSI"));

		chargebackStatuses = (HashMap)applicationObjectsHm.get("chargebackStatuses");
		chargebackStatusesSI = (ArrayList)applicationSIHm.get("chargebackStatusesSI");
	}

	/**
	 * This method add user classes special options
	 * @param list
	 * @return
	 */
	private ArrayList<SelectItem> addUserClassesSpecialOptions(ArrayList<SelectItem> list) {
		SelectItem allItem = new SelectItem((long)ConstantsBase.USER_CLASS_ALL, ConstantsBase.ALL_FILTER_KEY);
		SelectItem companyOrPrivate = new SelectItem((long)ConstantsBase.USER_CLASS_COMPANY_OR_PRIVATE, ConstantsBase.USER_CLASS_COMPANY_OR_PRIVATE_LABLE);
		list.add(0, allItem);
		list.add(list.size(), companyOrPrivate);
		return list;
	}

	/**
	 * This method adds an empty item to a select list
	 * @param list
	 * @return
	 */
	private ArrayList<SelectItem> addAllOption(ArrayList<SelectItem> list) {
		SelectItem allItem = new SelectItem(ConstantsBase.ALL_FILTER_ID, ConstantsBase.ALL_FILTER_KEY);
		list.add(0, allItem);
		return list;
	}

	/**
	 * @return the banks
	 */
	public BankBase getBankById(long id) {
		return (BankBase)banks.get(id);
	}

	/**
	 * @return the currencies
	 */
	public Currency getCurrencyById(long id) {
		return (Currency)currencies.get(id);
	}

	/**
	 * @return the banksSI
	 */
	public ArrayList<SelectItem> getBanksSI() {
		return banksSI;
	}

	/**
	 * @return the userClassesSI
	 */
	public ArrayList<SelectItem> getUserClassesSI() {
		return userClassesSI;
	}

	/**
	 * @return the userClasses
	 */
	public HashMap getUserClasses() {
		return userClasses;
	}

	/**
	 * @return the chargebackStatuses
	 */
	public HashMap getChargebackStatuses() {
		return chargebackStatuses;
	}

	/**
	 * @return the chargebackStatusesSI
	 */
	public ArrayList<SelectItem> getChargebackStatusesSI() {
		return chargebackStatusesSI;
	}

}
