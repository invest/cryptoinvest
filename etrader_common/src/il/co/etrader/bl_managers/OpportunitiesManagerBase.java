package il.co.etrader.bl_managers;

import java.sql.Connection;
import java.sql.SQLException;

import com.anyoption.common.util.OpportunityCacheBean;

import il.co.etrader.dao_managers.OpportunitiesDAOBase;
import il.co.etrader.util.CommonUtil;

public class OpportunitiesManagerBase extends com.anyoption.common.managers.OpportunitiesManagerBase {
    public static OpportunityCacheBean getOpportunityCacheBean(long oppId) throws SQLException {
        Connection conn = null;
        try {
            conn = getConnection();
            return OpportunitiesDAOBase.getOpportunityCacheBean(conn, oppId);
        } finally {
            closeConnection(conn);
        }
    }
    
    public static String getStatusTxt(int isSettled, int isPublished){
        if (isSettled == 1)
            return CommonUtil.getMessage("opportunities.settled", null);
        if (isPublished == 1)
            return CommonUtil.getMessage("opportunities.published", null);

        return CommonUtil.getMessage("opportunities.notpublished", null);
    }
    
//    public static String getOppTemplateQuote() throws SQLException{
//    	Connection conn = null;    		
//    	try{
//    		 conn = getConnection();
//    		return OpportunitiesDAOBase.getOppTemplateQuote(conn);
//    	} finally {
//    		closeConnection(conn);
//    	}
//    	
//    }
//    
//    public static void setOppTemplateQuote(String quote) throws SQLException{
//    	Connection conn = null;
//    	try{
//    		conn = getConnection();
//    		OpportunitiesDAOBase.setOppTemplateQuote(conn, quote);
//    	} finally {
//    		closeConnection(conn);
//    	}
//    }
}