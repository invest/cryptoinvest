package il.co.etrader.bl_managers;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import com.anyoption.common.beans.Bonus;
import com.anyoption.common.beans.base.BonusUsers;
import com.anyoption.common.beans.base.BonusUsersStep;
import com.anyoption.common.beans.base.Currency;
import com.anyoption.common.bl_vos.BonusCurrency;
import com.anyoption.common.bl_vos.BonusPopulationLimit;
import com.anyoption.common.bonus.BonusHandlerBase;
import com.anyoption.common.bonus.BonusHandlerFactory;
import com.anyoption.common.daos.UsersAwardBonusDAO;

import il.co.etrader.bl_vos.UserBase;
import il.co.etrader.dao_managers.BonusDAOBase;
import il.co.etrader.dao_managers.UsersDAOBase;
import il.co.etrader.util.ConstantsBase;


public class BonusManagerBase extends com.anyoption.common.managers.BonusManagerBase {

    private static final Logger logger = Logger.getLogger(BonusManagerBase.class);


	/**
	 * Update bonus users state
	 * @param id
	 * @param stateId
	 * @throws SQLException
	 */
    public static void updateBonusUser(long id, long stateId, long writerIdCancel) throws SQLException {

        Connection con = getConnection();
        try {
        	updateBonusUser(con, id, stateId, writerIdCancel);
        } finally {
            closeConnection(con);
        }
    }

    /**
     * Update bonus users state
     * @param con
     * @param id
     * @param stateId
     * @throws SQLException
     */
    public static void updateBonusUser(Connection con, long id, long stateId, long writerIdCancel) throws SQLException {
    	if (logger.isEnabledFor(Level.DEBUG)) {
            logger.log(Level.DEBUG,"update state for bonusUsers id: " + id);
        }
    	BonusDAOBase.updateState(con, id, stateId, writerIdCancel);
    }

    /**
     * check if user can withdraw.
     * @param userId
     * @return true only if user dont have any bonus in state used else false
     * @throws SQLException
     */
    public static boolean userCanWithdraw(long userId, long balance, long amount) throws SQLException {
    	boolean flag = false;
		Connection con = getConnection();
		try {
			flag = BonusDAOBase.userCanWithdraw(con, userId, balance, amount);
		} finally {
		    closeConnection(con);
		}
		return flag;
	}

    /**
     * Get bonus user by user id and bonus id
     * @param userId
     * @return list of bonus
     * @throws SQLException
     */
    public static BonusUsers getBonusUserByBonusId(long userId, long bonusId) throws SQLException {
		Connection con = getConnection();
		try {
			return BonusDAOBase.getBonusUserByBonusId(con, userId, bonusId);
		} finally {
		    closeConnection(con);
		}
	}

    /**
     * cancel bonus to user
     * @param bonusUser
     * @param stateToUpdate
     * @param utcOffset
     * @param writerId
     * @throws SQLException
     */
    public static void cancelBonusToUser(BonusUsers bonusUser, long stateToUpdate,
    		String utcOffset, long writerId, long skinId, String ip, long loginId) throws SQLException {
    	Connection con = getConnection();
    	try {
    		cancelBonusToUser(con, bonusUser, stateToUpdate, utcOffset, writerId, skinId, loginId, ip);
    	} finally {
			closeConnection(con);
    	}
    }

    public static String getBonusDescription(long bonusId,long stateId) throws SQLException{
    	Connection con = getConnection();
    	String description = "";
    	try{
    		description = BonusDAOBase.getDescriptionByTypeAndState(con,bonusId,stateId);
    	}finally{
    		closeConnection(con);
    	}
    	return description;

    }

	public static boolean isHasBonusActiveOrGranted(long id) throws SQLException {
		Connection con = getConnection();
		try {
			return BonusDAOBase.isHasBonusActiveOrGranted(con, id);
		} finally {
		    closeConnection(con);
		}
	}

	public static Bonus getBonusById(long bonusId) throws SQLException{
		Connection con = null;
        try {
    		con = getConnection();
            return getBonusById(con, bonusId);
        } finally {
            closeConnection(con);
        }
	}

    public static Bonus getBonusById(Connection con, long bonusId) throws SQLException{
        return BonusDAOBase.getBonusById(con, bonusId);
    }

    /**
     * Insert bonus to user
     * @param bUser BonusUser instnace
     * @param popEntryId
     * @param bonusPopLimitTypeId
     * @param currencyId user currency
     * @return
     * @throws SQLException
     */
    public static boolean insertBonusUser(BonusUsers bUser, UserBase user,long writerId, long popEntryId, int bonusPopLimitTypeId, boolean isBackend) throws SQLException {
    	return insertBonusUser(bUser, user, writerId, popEntryId, bonusPopLimitTypeId, isBackend, 0);
    }
    
    public static boolean insertBonusUser(BonusUsers bUser, UserBase user,long writerId, long popEntryId, int bonusPopLimitTypeId, boolean isBackend, long wagering) throws SQLException {
    	Connection	con = getConnection();
    	try {
    		return insertBonusUser(bUser,user,writerId,con, popEntryId, bonusPopLimitTypeId, isBackend, wagering);
    	} finally {
        	closeConnection(con);
        }
    }

   /**
     * Insert bonus to user Wrapper
     * @param bUser BonusUser instnace
	 * @param popEntryId
	 * @param bonusPopLimitTypeId
	 * @param currencyId user currency
	 * @return
     * @throws SQLException
     */
    public static boolean insertBonusUser(BonusUsers bUser, UserBase user,long writerId, Connection con, long popEntryId, int bonusPopLimitTypeId, boolean isBackend) throws SQLException {
    	return insertBonusUser(bUser, user, writerId, con, popEntryId, bonusPopLimitTypeId, isBackend, 0);
    }
    
    public static boolean insertBonusUser(BonusUsers bUser, UserBase user,long writerId, Connection con, long popEntryId, int bonusPopLimitTypeId, boolean isBackend,long wagering) throws SQLException {
       	long bonusId = bUser.getBonusId();
		Bonus b = getBonusById(con,bonusId); //  get Bonus data for insert
		BonusCurrency bc = BonusDAOBase.getBonusCurrency(con,bonusId, user.getCurrencyId());
		if (wagering > 0) {
			b.setWageringParameter(wagering);
		}
        return insertBonusUser(bUser, user, writerId, con, bc, b, popEntryId, bonusPopLimitTypeId, isBackend);
    }

    /**
     * Insert bonus to user
     * @param bUser bonusUser instance
     * @param user user instnace
     * @param writerId
     * @param con
     * @param bc bonusCurrency instance
     * @param b bonus instance
     * @param popTypeId
     * @param bonusPopLimitTypeId
     * @return
     * @throws SQLException
     */
    public static boolean insertBonusUser(BonusUsers bUser, UserBase user, long writerId, Connection con,
    		BonusCurrency bc, Bonus b, long popEntryId, int bonusPopLimitTypeId, boolean isBackend) throws SQLException {

    	boolean res = false;

        if (logger.isEnabledFor(Level.DEBUG)) {
            logger.log(Level.DEBUG,"Insert new bonus to user: " + bUser.getUserId());
         }

        if (null != b && null != bc) {

			BonusHandlerBase bh = BonusHandlerFactory.getInstance(b.getTypeId());
			// Set default sumDeposits from BonusCurrency - not relevant to insert bonus via Backend
			if (!isBackend){
				bUser.setSumDeposits(bc.getSumDeposits());
			}
			if (null != bh){
				try {
					con.setAutoCommit(false);
					res = bh.bonusInsert(con, bUser, b, bc, user.getId(), user.getCurrencyId(), writerId, popEntryId, bonusPopLimitTypeId, true);
					con.commit();

		    	} catch (Exception e) {
		    		logger.error("Exception Insert new bonus id:" + b.getId() + " to user id: " + user.getId(), e);
					try {
						con.rollback();
					} catch (Throwable it) {
						logger.error("Can't rollback.", it);
					}
		            SQLException sqle = new SQLException();
		            sqle.initCause(e);
		            throw sqle;
		    	} finally {
		            try {
		                con.setAutoCommit(true);
		            } catch (Exception e) {
		            	logger.error("Can't set back to autocommit.", e);
		            }
				}
			}
	    }
	    return res;
    }

    public static BonusCurrency getBonusCurrency(long bonusId, long currencyId) throws SQLException{
		Connection con = null;
		try{
			con = getConnection();
			return BonusDAOBase.getBonusCurrency(con, bonusId, currencyId);
		}finally{
			closeConnection(con);
		}
	}

    public static BonusUsers getLastBonusForUser(long userId) throws SQLException {
    	Connection con = null;
		try{
			con = getConnection();
			return BonusDAOBase.getLastBonusForUser(con, userId);
		}finally{
			closeConnection(con);
		}
    }

	public static ArrayList<BonusUsersStep> getBonusSteps(long bonusId,long currencyId) throws SQLException{
		Connection con = null;
		try{
			con = getConnection();
			return BonusDAOBase.getBonusSteps(con, bonusId, currencyId);
		}finally{
			closeConnection(con);
		}
	}

//    /**
//     * Get amount for bonus limitation by population type id
//     * @param con Connection
//     * @param user
//     * @param popTypeId population type id
//     * @return
//     * @throws SQLException
//     */
//    public static long getAmountForLimitCreation(Connection con, long userId, int bonusPopLimitTypeId) throws SQLException {
//    	long amountForLimit = -1;
//
//    	Transaction t = null;
//
//    	switch (bonusPopLimitTypeId) {
//    	case BonusManagerBase.BONUS_P_L_FIXED:
//    		amountForLimit = 0;
//    		break;
//   		case BonusManagerBase.BONUS_P_L_DEPOSIT_ATTEMPT:
//   			t = TransactionsDAOBase.getLastFailedDeposit(con, userId);
//   			if (null != t) {
//   				logger.debug("Got lastFailedDeposit. trxId: " + t.getId() + ", amount: " + t.getAmount());
//   				amountForLimit = t.getAmount();
//   			} else {
//   				logger.warn("Problem getting lastFailedDeposit!");
//   				amountForLimit = -1;
//   			}
//   			break;
//   		case BonusManagerBase.BONUS_P_L_AVG_DEPOSITS:
//   			long avgDeposits = TransactionsDAOBase.getAVGSucceedDeposit(con, userId);
//   			if (avgDeposits == 0) {
//   				logger.warn("Problem getting firstSucceedDeposit!");
//   				amountForLimit = -1;
//   			} else {
//   				amountForLimit = avgDeposits;
//   			}
//   			break;
//   		case BonusManagerBase.BONUS_P_L_BALANCE:
//   			UserBase user = UsersDAOBase.getUserById(con, userId);
//   			if (null != user){
//   				long balance = user.getBalance();
//	   			if (balance > 0) {
//	   				amountForLimit = balance;
//	   			} else {
//	   				amountForLimit = -1;
//	   				logger.warn("Problem getting users balance!");
//	   			}
//   			}else{
//   				amountForLimit = -1;
//   				logger.warn("user wasn't found, user id " + userId);
//   			}
//
//   			break;
//   		default:
//   			logger.warn("no case for bonusPopLimitTypeId: " + bonusPopLimitTypeId);
//			amountForLimit = -1;
//			break;
//    	}
//
//
//    	return amountForLimit;
//    }

	/**
	 * Get bonus Population limits by bonusId, popTypeId and currencyId
	 * @param popTypeId
	 * @param bonusid
	 * @param currencyId
	 * @return
	 * @throws SQLException
	 */
	public static ArrayList<BonusPopulationLimit> getBonusPopulationLimits(long popEntryId, long bonusId, long currencyId) throws SQLException{
		Connection con = null;
		try {
			con = getConnection();
			return BonusDAOBase.getBonusPopulationLimits(con,popEntryId,bonusId,currencyId);
		} finally {
			closeConnection(con);
		}
	}

//    /**
//     * Get min deposit amount defined by population limits
//     * @param amountForLimit amount for limit calculation
//     * @param currencyId user currecy
//     * @param isHasSteps
//     * @param con TODO
//     * @param BonusUsers bu
//     * @param Bonus b
//     * @param BonusCurrency bc
//     * @param popTypeId  population type id of the user
//     * @return
//     * @throws SQLException
//     */
//    public static boolean getMinDepositByPopLimitations(BonusUsers bu, Bonus b, BonusCurrency bc, long popEntryId, long amountForLimit, long currencyId, boolean isHasSteps, Connection con) throws SQLException {
//
//    	long bonusId = bu.getBonusId();
//    	long limitValue = 0;
//    	ArrayList<BonusPopulationLimit> bpl = BonusDAOBase.getBonusPopulationLimits(con,popEntryId,bonusId,currencyId);
//
//		for (int i = 0; i < bpl.size(); i++) {
//			BonusPopulationLimit l = bpl.get(i);
//
//			if (amountForLimit >= l.getMinDepositParam() && amountForLimit <= l.getMaxDepositParam()) {
//
//				long baseCalculation = l.getMultiplication() * amountForLimit;
//				long baseCalculationRounded = roundAmountToNearestX(baseCalculation,ConstantsBase.BONUS_ROUND_TOP);
//
//		   		if (!isHasSteps){
//   					limitValue = Math.max(baseCalculationRounded, l.getMinResult());
//
//   					if (limitValue > 0){
//	   					switch (l.getLimitUpdateType()) {
//							case ConstantsBase.BONUS_LIMIT_UPDATE_TYPE_SUM_DEPOSITS:
//								// If sum deposits hasn'e been changed by admin, set new sum deposits
//								if (bu.getSumDeposits() == 0){
//									bu.setSumDeposits(limitValue);
//								}
//								break;
//
//							case ConstantsBase.BONUS_LIMIT_UPDATE_TYPE_SUM_INVESTS:
//								bc.setSumInvestQualify(limitValue);
//								break;
//
//							case ConstantsBase.BONUS_LIMIT_UPDATE_TYPE_ACTION_NUM:
//								b.setNumberOfActions(limitValue);
//								break;
//
//							default:
//								logger.error("LimitUpdateType " + l.getLimitUpdateType() + " not recognized");
//								return false;
//						}
//	   					// Check if needs to change bonus amount
//	   					long bonusAmount = l.getBonusAmount();
//
//	   					if (bonusAmount > 0){
//	   						bc.setBonusAmount(bonusAmount);
//	   					}
//
//	   					// Check if needs to change min and/or investment amount
//	   					long minInvAmount = l.getMinInvestmentAmount();
//	   					long maxInvAmount = l.getMaxInvestmentAmount();
//
//	   					if (minInvAmount > 0){
//	   						bc.setMinInvestAmount(minInvAmount);
//	   					}
//	   					if (maxInvAmount > 0){
//	   						bc.setMaxInvestAmount(maxInvAmount);
//	   					}
//
//	   					return true;
//   					}
//   					return false;
//
//	   			}else{
//	   	   			ArrayList<BonusUsersStep> bonusSteps = BonusDAOBase.getBonusSteps(con, bonusId, currencyId);
//	   	   			long stepMin = 0;
//    				long stepMax = 0;
//    				String bonusPercentStr= null;
//    				DecimalFormat df = new DecimalFormat("###.###");
//
//	    			for (int j = 0; j < bonusSteps.size(); j++){
//
//	    				// if first step, take the min between base calculation and min result
//	    				if (stepMin == 0){
//	    					stepMin = Math.max(baseCalculationRounded, l.getMinResult());
//	    				}else{
//	    					stepMin = stepMax + 100;
//	    				}
//	    				stepMax = roundAmountToNearestX(baseCalculation * l.getStepRangeMul() * (long)Math.pow(l.getStepLevelMul(), j),
//	    								ConstantsBase.BONUS_ROUND_TOP);
//
//	    				bonusSteps.get(j).setMinDepositAmount(stepMin);
//	    				bonusSteps.get(j).setMaxDepositAmount(stepMax);
//
//	    				bonusPercentStr = df.format(l.getMinBonusPercent() + l.getStepBonusAddition() * j);
//	    				bonusSteps.get(j).setBonusPercent(Double.valueOf(bonusPercentStr));
//	    			}
//	    			bu.setBonusSteps(bonusSteps);
//	    			return true;
//	   			}
//			}
//		}
//    	return false;
//    }

//    private static long roundAmountToNearestX(long amount, long xValue){
//    	if (amount > xValue / 2){
//            BigDecimal bd = new BigDecimal(String.valueOf(amount));
//            bd = bd.setScale(0, BigDecimal.ROUND_HALF_UP);
//            bd = bd.divide(new BigDecimal(String.valueOf(xValue)), BigDecimal.ROUND_HALF_UP);
//	    	return bd.longValue() * xValue;
//    	}else{
//    		return amount;
//    	}
//    }

//    /**
//     * getMinDepositAmount by bonus popualtion limitations
//     * @param BonusUsers bu
//     * @param Bonus b
//     * @param BonusCurrency bc
//     * @param popTypeId
//     * @param bonusPopLimitTypeId
//     * @param userId
//     * @param currencyId
//     * @param isHasSteps
//     * @return minDepositAmount
//     */
//    public static boolean getLimitsForBonus(BonusUsers bu, Bonus b, BonusCurrency bc, long popEntryId, int bonusPopLimitTypeId, long userId, long currencyId, boolean isHasSteps) throws SQLException{
//    	Connection con = getConnection();
//
//    	return getLimitsForBonus(con, bu, b, bc, popEntryId, bonusPopLimitTypeId, userId, currencyId, isHasSteps);
//    }
//
//    /**
//     * getMinDepositAmount by bonus popualtion limitations
//     * @param Connection con
//     * @param BonusUsers bu
//     * @param Bonus b
//     * @param BonusCurrency bc
//     * @param popTypeId
//     * @param bonusPopLimitTypeId
//     * @param userId
//     * @param currencyId
//     * @param isHasSteps
//     * @return minDepositAmount
//     */
//    public static boolean getLimitsForBonus(Connection con, BonusUsers bu, Bonus b, BonusCurrency bc, long popEntryId, int bonusPopLimitTypeId, long userId, long currencyId, boolean isHasSteps) throws SQLException{
//    		if (popEntryId != 0 && bonusPopLimitTypeId != 0) {
//        		long amountForLimit = getAmountForLimitCreation(con, userId, bonusPopLimitTypeId);
//
//    			if (amountForLimit != -1) {
//    				return getMinDepositByPopLimitations(bu, b, bc, popEntryId, amountForLimit, currencyId, isHasSteps, con);
//    			} else {
//    				logger.info("Problem with bonus population limits!");
//    			}
//        	}
//		return false;
//    }

	/**
	 * @param connection
	 * @param userId
	 * @param bonusId
	 * @return
	 */
	public static boolean generateInsertBonus(Connection connection, Long userId, Long bonusId) {
		boolean result = false;
		try {			
	   		UserBase user = new UserBase();
		   	UsersDAOBase.getByUserId(connection, userId, user,true);					
			BonusUsers bonusUser = new BonusUsers();
    		bonusUser.setUserId(user.getId());
    		//bonusUser.setWriterId(user.getWriterId());
    		bonusUser.setBonusId(bonusId);
    		result = BonusManagerBase.insertBonusUser(bonusUser, (UserBase)user, 0/*writerId*/, connection, 0, 0, false /*isBackend*/);
		} catch (SQLException e) {
			logger.error(ConstantsBase.REWARD_PLAN_LOG_PREFIX + "generateInsertBonus; userId: " + userId + "bonusId: " + bonusId, e);
		}	
		return result;
	}
	
	/**
	 * insert bonus with issue and update is granted to user that come from users award bonus
	 * @param user
	 * @param usersAwardBonusId
	 * @param bonusId
	 * @param writerId
	 * @param issueSubjectId
	 * @param issueStatusId
	 * @param userId
	 * @param issuePriorityId
	 * @param issueType
	 * @param issueActionTypeId
	 * @param issueActionIsSignificant
	 * @param issueActionTime
	 * @param issueActionTimeOffset
	 * @param issueActionComments
	 * @return
	 * @throws SQLException
	 */
	public static boolean insertBonusWithIssue(UserBase user, long usersAwardBonusId, Long bonusId, long writerId, String issueSubjectId, String issueStatusId, long userId, String issuePriorityId, int issueType,
			String issueActionTypeId, boolean issueActionIsSignificant, Date issueActionTime, String issueActionTimeOffset, String issueActionComments) throws SQLException {
		boolean result = false;
		BonusUsers bonusUser = new BonusUsers();
		bonusUser.setUserId(user.getId());
		bonusUser.setWriterId(writerId);
		bonusUser.setBonusId(bonusId);
		Connection conn = getConnection();
		try {							
        	conn.setAutoCommit(false);
    		result = BonusManagerBase.insertBonusUser(bonusUser, (UserBase)user, writerId, conn, 0, 0, false);
    		if (result) {
    			IssuesManagerBase.insertIssue(conn, issueSubjectId, issueStatusId, user.getId(), issuePriorityId, issueType, issueActionTypeId, writerId, issueActionIsSignificant,
    					issueActionTime, issueActionTimeOffset, issueActionComments);
    			UsersAwardBonusDAO.updateBonusGranted(conn, usersAwardBonusId);
    		}
    		conn.commit();
    	} catch (SQLException e) {
    		logger.error("InsertBonusWithIssue; userId: " + user.getId() + " bonusId: " + bonusId, e);
    		try {
    			conn.rollback();
    		} catch (SQLException ie) {
    			logger.log(Level.ERROR, "in InsertBonusWithIssue, Can't rollback.", ie);
    		}
    		throw e;
		} finally {
			conn.setAutoCommit(true);
			closeConnection(conn);
		}	
		return result;
	}
	
//	/**
//	 * Accept bonus user
//	 * @param bu
//	 * @param user
//	 * @throws SQLException
//	 */
//	public static void acceptBonusUser(BonusUsers bu, UserBase user) throws SQLException {
//		Connection con = getConnection();
//		BonusHandlerBase bh = BonusHandlerFactory.getInstance(bu.getTypeId());
//		if (null != bh) {
//			try {
//				con.setAutoCommit(false);
//				bh.acceptBonus(con, bu, user);
//				con.commit();
//			} catch (SQLException e){
//				logger.error("Accept bonus user, userId: " + user.getId() + " BonusUserId: " + bu.getId(), e);
//	    		try {
//	    			con.rollback();
//	    		} catch (SQLException ie) {
//	    			logger.error("In accept bonus user, Can't rollback.", ie);
//	    		}
//	    		throw e;
//			} finally {
//				con.setAutoCommit(true);
//				closeConnection(con);
//			}
//		}
//	}
}