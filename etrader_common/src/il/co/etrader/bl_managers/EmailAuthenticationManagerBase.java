package il.co.etrader.bl_managers;

import il.co.etrader.bl_vos.EmailAuthentication;
import il.co.etrader.bl_vos.Template;
import il.co.etrader.bl_vos.UserBase;
import il.co.etrader.dao_managers.EmailAuthenticationDAOBase;
import il.co.etrader.dao_managers.UsersDAOBase;
import il.co.etrader.util.CommonUtil;
import il.co.etrader.util.ConstantsBase;

import java.sql.Connection;
import java.sql.SQLException;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import com.anyoption.common.managers.BaseBLManager;

public class EmailAuthenticationManagerBase extends BaseBLManager {

	public EmailAuthenticationManagerBase() throws Exception {
	}

	public static boolean insert(long userId, String email, String authKey, long writerId) throws SQLException {
		Connection conn = getConnection();
		boolean res;
		try {
			res = EmailAuthenticationDAOBase.insert(conn, userId, email, authKey, writerId);
		} finally {
			closeConnection(conn);
		}
		return res;
	}

	public static EmailAuthentication get(String authKey, boolean addClick) throws SQLException {
		Connection conn = getConnection();
		EmailAuthentication ea;
		try {
			ea = EmailAuthenticationDAOBase.get(conn, authKey, addClick);
		} finally {
			closeConnection(conn);
		}
		return ea;
	}

	public static long getNumOfActivationsSent(long userId) throws SQLException {
		Connection conn = getConnection();
		try {
			return EmailAuthenticationDAOBase.getNumOfActivationsSent(conn, userId);
		} finally {
			closeConnection(conn);
		}
	}

	/**
	 * validations for sending activation email to user
	 * @param userId
	 * @return
	 * @throws SQLException
	 */
	public static boolean validateSendActivation(long userId) throws SQLException {
		FacesContext context = FacesContext.getCurrentInstance();
		Connection conn = getConnection();
		try {
			if (EmailAuthenticationDAOBase.getNumOfActivationsSent(conn, userId) > ConstantsBase.ACTIVATION_EMAIL_MAX_SENT_TIMES) {
				FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR,
						CommonUtil.getMessage("error.email.activation",	null), null);
				context.addMessage(null, fm);

				return false;
			} else if (UsersDAOBase.isAuthrizedEmail(conn, userId)) {
				FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR,
						CommonUtil.getMessage("error.email.activation",	null), null);
				context.addMessage(null, fm);

				return false;
			}
			return true;
		} finally {
			closeConnection(conn);
		}
	}

	/**
	 * Send activation email
	 * @param authLink
	 * @param writerId
	 * @param user
	 * @throws Exception
	 */
	public static void sendActivationEmail(String authLink, long writerId, UserBase user) throws Exception {
		String authKey = UsersManagerBase.generateRandomKey();
		String link = authLink + ConstantsBase.AUTH_KEY + "=" + authKey;
		UsersManagerBase.sendMailTemplateFile(Template.MAIL_VALIDATION, writerId, user, false, null, null, null, null, null, link, null, false, null, null, null);
		insert(user.getId(), user.getEmail(), authKey, writerId);
	}

}