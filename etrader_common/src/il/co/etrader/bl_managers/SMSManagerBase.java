package il.co.etrader.bl_managers;



import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Hashtable;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.Country;
import com.anyoption.common.beans.Issue;
import com.anyoption.common.beans.IssueAction;
import com.anyoption.common.beans.base.User.MobileNumberValidation;
import com.anyoption.common.sms.SMSException;

import il.co.etrader.bl_vos.Contact;
import il.co.etrader.bl_vos.UserBase;
import il.co.etrader.dao_managers.SMSDAOBase;
import il.co.etrader.sms.SMS;
import il.co.etrader.sms.SMSProvider;
import il.co.etrader.sms.SMSProviderConfig;
import il.co.etrader.util.CommonUtil;
import il.co.etrader.util.ConstantsBase;
import oracle.ucp.jdbc.HarvestableConnection;

public class SMSManagerBase extends com.anyoption.common.managers.SMSManagerBase {
	private static final Logger log = Logger.getLogger(SMSManagerBase.class);
	private static Hashtable<Long, SMSProvider> smsProviders;

    public static void loadSMSProviders(Connection conn, int maxRetries) throws SMSException {
        if (null == smsProviders) {
        	smsProviders = new Hashtable<Long, SMSProvider>();
            try {
                ArrayList<SMSProviderConfig> l = SMSDAOBase.loadSMSProvidersConfigs(conn);
                SMSProviderConfig c = null;
                for (int i = 0; i < l.size(); i++) {
                    c = l.get(i);
                    c.setMaxRetries(maxRetries);
                    try {
                        Class cl = Class.forName(c.getProviderClass());
                        SMSProvider p = (SMSProvider) cl.newInstance();
                        p.setConfig(c);
                        smsProviders.put(c.getId(), p);
                    } catch (Exception e) {
                        log.error("Failed to load provider for config: " + c, e);
                    }
                }
            } catch (Exception e) {
                throw new SMSException("Error loading sms providers.", e);
            }
        }
    }

    public static void loadSMSProviders(int maxRetries) throws SMSException {
        if (null == smsProviders) {
            Connection conn = null;
            try {
                conn = getConnection();
                loadSMSProviders(conn, maxRetries);
            } catch (SMSException ce) {
                throw ce;
            } catch (Throwable t) {
                throw new SMSException("Error loading sms providers.", t);
            } finally {
                closeConnection(conn);
            }
        } else {
            log.warn("sms providers already loaded.");
        }
    }

//  public static void sendTextMessage(String sender, String senderNumber, String phone, String message, OracleConnection con, long keyValue, long keyType, long providerId, long descriptionId) throws SMSException {
//  sendMessage(SMS.TYPE_TEXT, sender, senderNumber, phone, message, null, -1, con, keyValue, keyType, providerId, descriptionId);
//}

//public static long sendTextMessage(String sender, String senderNumber, String phone, String message, long keyValue, long keyType, long providerId, long descriptionId) throws SMSException {
//  return sendMessage(SMS.TYPE_TEXT, sender, senderNumber, phone, message, null, -1, keyValue, keyType, providerId, descriptionId);
//}
//
//public static void sendWAPPushMessage(String sender, String senderNumber, String phone, String message, String wapPushURL, long keyValue, long keyType, long providerId, long descriptionId) throws SMSException {
//  sendMessage(SMS.TYPE_WAP_PUSH, sender, senderNumber, phone, message, wapPushURL, -1, keyValue, keyType, providerId, descriptionId);
//}

//public static long sendMessage(long typeId, String sender, String senderNumber, String phone, String message, String wapPushURL, long dstPort,
//		OracleConnection conn, long keyValue, long keyType, long providerId, long descriptionId) throws SMSException {
//
//	String ls = System.getProperty("line.separator");
//	if(log.isTraceEnabled()){
//		log.trace("going to add queued sms, typeId:" +  typeId + ls +
//  		"phone: " + phone + ls + "message: " + message + ls +
//  		"wapPushURL: " + wapPushURL + ls + "dstPort: " + dstPort + ls);
//	}
//  try {
//  	 return SMSDAOBase.queueMessage(conn, typeId, sender, senderNumber, phone, message, wapPushURL, dstPort, keyValue, keyType, providerId, descriptionId);
//  } catch (Exception e) {
//      throw new SMSException("Failed to queue message.", e);
//  }
//}


    /**
     * Get smsProvider by id
     * @param providerId
     * @return
     */
    public static SMSProvider getProviderById(long providerId) {
    	return smsProviders.get(new Long(providerId));
    }

    /**
     * Close SMS providers. Iterate over all initialized SMS providers and tell them
     * to release all resources they use. Needed when SMS provider keep permanently
     * opened connections for communication.
     */
    public static void closeSMSProviders() {
        if (null != smsProviders) {
            for (SMSProvider p : smsProviders.values()) {
                p.close();
            }
            log.info("SMSProviders closed.");
        }
    }
    

    public static void sendSms(ArrayList<UserBase> usersList, String message,
    										 HttpSession session, long writerId, long promotion_id, int issueSubjectId) throws SQLException{
    	Connection conn = getConnection();
    	conn.setAutoCommit(false);
    	HarvestableConnection hc = (HarvestableConnection) conn;
		hc.setConnectionHarvestable(false);
		
    	
    	int sumOfUsersList = usersList.size();
    	int divInThousand = sumOfUsersList/ConstantsBase.THOUSAND;
    	double i = 0;
    	String percentage = "";
    	List<UserBase> subList = new ArrayList<UserBase>();
    	session.setAttribute(ConstantsBase.PROMOTION_TOTAL_SENDED_TO_USERS, "0_0");
    	if(divInThousand < sumOfUsersList){
    		percentage = Double.toString(((i * ConstantsBase.THOUSAND/sumOfUsersList)*100));
            session.setAttribute(ConstantsBase.PROMOTION_TOTAL_SENDED_TO_USERS, sumOfUsersList + "_" + percentage.substring(0, percentage.indexOf(".")) );
    		subList = usersList.subList(divInThousand*ConstantsBase.THOUSAND, sumOfUsersList);
    		sendSmsAndInsertIssue(conn, subList, message, writerId, promotion_id, issueSubjectId);
    	}
    	while(sumOfUsersList > ConstantsBase.THOUSAND && divInThousand > 0){
    		subList = usersList.subList((divInThousand-1) * ConstantsBase.THOUSAND, divInThousand * ConstantsBase.THOUSAND);
			sendSmsAndInsertIssue(conn, subList, message, writerId, promotion_id, issueSubjectId);
            i++;
            percentage = Double.toString(((i * ConstantsBase.THOUSAND/sumOfUsersList)*100));
            session.setAttribute(ConstantsBase.PROMOTION_TOTAL_SENDED_TO_USERS, sumOfUsersList + "_" + percentage.substring(0, percentage.indexOf(".")) );
            divInThousand--;
        }
    	
    	if(divInThousand == 0 ){
    		session.setAttribute(ConstantsBase.PROMOTION_TOTAL_SENDED_TO_USERS, sumOfUsersList + "_100");
    	}
    	
        try {
      		conn.commit();
      	} catch (SQLException sqle) {
              try {
                  conn.rollback();
              } catch (SQLException sqlie) {
                  log.error("Failed to rollback.", sqlie);
              }
              throw sqle;
      	} finally {
      		hc.setConnectionHarvestable(true);
          	conn.setAutoCommit(true);
          	closeConnection(conn);
      	}
//    	try {
//			Thread.currentThread().sleep(3000);
//		} catch (InterruptedException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//    	session.setAttribute(ConstantsBase.PROMOTION_TOTAL_SENDED_TO_USERS, "0_0");
    }

    public static void sendSmsAndInsertIssue(Connection conn, List<UserBase> usersList, String message, long writerId, long promotion_id, int issueSubjectId){
    	String SenderName = "";

    	for(UserBase user : usersList){
    		Country country = ApplicationDataBase.getCountry(user.getCountryId());
    		if (country.isAlphaNumericSender()) {
            	SenderName = country.getSupportPhone();
            	SenderName = SenderName.replaceAll("[^0-9]", "");
            	if(SenderName.length() > ConstantsBase.MAX_SIZE_SENDER_NAME){
            		SenderName = SenderName.substring(country.getPhoneCode().length());
            	}
            } else if (CommonUtil.isHebrewSkin(user.getSkinId())) {
                SenderName = "etrader";
            } else {
            	SenderName = "anyoption";
            }
			try {
				String phoneCode = country.getPhoneCode();				
				if (null != user.getMobilePhone() && user.getMobilePhone().indexOf("0") == 0) { // leading zero (index 0)
	                user.setMobilePhone(user.getMobilePhone().substring(1));
	            }				
				long smsID = sendMessage(SMS.TYPE_TEXT, SenderName, country.getSupportPhone(), phoneCode + user.getMobilePhone(),
						 message, null, SMS_DEST_PORT, conn, user.getId(),SMS_KEY_TYPE_USERID, ConstantsBase.MOBIVATE_PROVIDER, SMS.DESCRIPTION_PROMOTION, user.getMobileNumberValidation());//ConstantsBase.MBLOX_PROVIDER);
				//IssuesManagerBase.createSmsOrMailIssue(conn, user, IssuesManagerBase.ISSUE_CHANNEL_SMS, IssuesManagerBase.ISSUE_ACTION_SMS, issueSubjectId, smsID, message, writerId);
			} catch (SMSException smse) {
                log.warn("Failed to send SMS.", smse);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
    	}
  
    }

    
    
    public static void sendSmsForContacts( ArrayList<Contact> contactsList, String message, long writerId, long promotion_id) throws SQLException{
    	Connection conn = getConnection();
    	conn.setAutoCommit(false);
    	HarvestableConnection hc = (HarvestableConnection) conn;
		hc.setConnectionHarvestable(false);

    	String SenderName = "";
    	try{
	    	for(Contact contact : contactsList){
	    		Country country = ApplicationDataBase.getCountry(contact.getCountryId());
	    		if (country.isAlphaNumericSender()) {
	            	SenderName = country.getSupportPhone();
	            	SenderName = SenderName.replaceAll("[^0-9]", "");
	            	if(SenderName.length() > ConstantsBase.MAX_SIZE_SENDER_NAME){
	            		SenderName = SenderName.substring(country.getPhoneCode().length());
	            	}
	            } else if (CommonUtil.isHebrewSkin(contact.getSkinId())) {
	                SenderName = "etrader";
	            } else {
	            	SenderName = "anyoption";
	            }
				try {
					String phoneCode = country.getPhoneCode();				
					if (null != contact.getMobilePhone() && contact.getMobilePhone().indexOf("0") == 0) { // leading zero (index 0)
						contact.setMobilePhone(contact.getMobilePhone().substring(1));
		            }				
					long smsID = sendMessage(SMS.TYPE_TEXT, SenderName, country.getSupportPhone(), phoneCode + contact.getMobilePhone(),
							 message, null, SMS_DEST_PORT, conn, contact.getId(),SMS_KEY_TYPE_CONTACT, ConstantsBase.MOBIVATE_PROVIDER, SMS.DESCRIPTION_PROMOTION, MobileNumberValidation.NOTVALIDATED);
					//IssuesManagerBase.createSmsIssueForContact(conn, contact, smsID, message, writerId);
				} catch (SMSException smse) {
	                log.warn("Failed to send SMS.", smse);
				} 
	    	}
    	} finally {
    		conn.commit();
        	conn.setAutoCommit(true);
        	hc.setConnectionHarvestable(true);
    		closeConnection(conn);
    	}
    }
	
	/* Merged code */
	// public static void insertIssueForSMS(UserBase user, Date timeSend, String msg, long smsId) {
	// Connection conn = null;
	// Issue issue = new Issue();
	// issue.setSubjectId(String.valueOf(IssuesManagerBase.ISSUE_SUBJECT_REGISTRATION_SMS));
	// issue.setStatusId(String.valueOf(IssuesManagerBase.ISSUE_STATUS_CLOSED));
	// issue.setPriorityId(String.valueOf(IssuesManagerBase.ISSUE_PRIORITY_LOW));
	// issue.setUserId(user.getId());
	// issue.setContactId(user.getContactId());
	// //issue.setType(ConstantsBase.ISSUE_TYPE_REGULAR);
	// // Filling action fields
	// IssueAction issueAction = new IssueAction();
	// issueAction.setChannelId(String.valueOf(IssuesManagerBase.ISSUE_CHANNEL_SMS)); //SMS
	// issueAction.setActionTime(timeSend);
	// issueAction.setActionTypeId(String.valueOf(IssuesManagerBase.ISSUE_ACTION_SMS));
	// issueAction.setSignificant(false);
	// issueAction.setComments(msg);
	// issueAction.setSmsId(smsId);
	// issueAction.setWriterId(user.getWriterId());
	// issueAction.setCallDirectionId(IssueAction.ISSUE_DIRECTION_OUT_BOUND_PHONE_CALL);
	// issueAction.setActionTimeOffset(user.getUtcOffset());
	// try{
	// // Insert Issue & action
	// conn = getConnection();
	// com.anyoption.common.managers.IssuesManagerBase.insertIssueAndIssueAction(conn, issue, issueAction);
	// } catch(SQLException e) {
	// log.error("Enable to insert Issue for user with id : " + user.getId(), e);
	// }finally{
	// closeConnection(conn);
	// }
	//
	// }
}