package il.co.etrader.bl_managers;

import il.co.etrader.bl_vos.Writer;
import il.co.etrader.dao_managers.TransactionsDAOBase;
import il.co.etrader.dao_managers.TransactionsIssuesDAOBase;
import il.co.etrader.dao_managers.UsersDAOBase;
import il.co.etrader.dao_managers.WritersDAO;
import il.co.etrader.util.CommonUtil;
import il.co.etrader.util.ConstantsBase;

import java.sql.Connection;
import java.sql.SQLException;

import org.apache.log4j.Logger;

import com.anyoption.common.managers.BaseBLManager;

public class TransactionsIssuesManagerBase extends BaseBLManager {

	private static final Logger logger = Logger.getLogger(TransactionsIssuesManagerBase.class);

	/**
	 * Insert Transaction Issues record
	 * @param tranId transactionId
	 * @param actionId transactionId
	 * @throws SQLException
	 */
	public static void insertIntoTi(long tranId, long actionId, int tran_issue_insert_type, long writerId, int isFirstDeposit) throws SQLException{
		Connection con = getConnection();

		try {
			TransactionsIssuesDAOBase.insertIntoTi(con, tranId, actionId, tran_issue_insert_type, writerId, isFirstDeposit);
		} finally {
			closeConnection(con);
		}
	}

	/**
	 * update online deposits writers list after inserting new transaction issue record
	 * @param writerId
	 * @param issuesActionId
	 * @throws SQLException
	 */
	public static void updateOnlineDeposits(long writerId, long userId, long trxId) throws SQLException {
		Connection con = getConnection();
		try {
			long skinId = UsersDAOBase.getSkinIdByUserId(con, userId);
			long currencyId = Long.parseLong(UsersDAOBase.getCurrencyIdByUserId(con, String.valueOf(userId)));
			long amount = TransactionsDAOBase.getAmountById(con, trxId);
			Writer w = WritersDAO.get(con, writerId);

			if (w.getSales_type() > 0){
				w.setOnlineDepositSkinId(skinId);
				String depositType = ConstantsBase.SALES_DEPOSIT_TYPE_RETENTION_STR;
				int depositTypeId = ConstantsBase.SALES_DEPOSIT_TYPE_RETENTION;
				Long firstDepositId = UsersDAOBase.getFirstDepositId(con, userId);
				if (null != firstDepositId && firstDepositId == trxId) {
					depositType = ConstantsBase.SALES_DEPOSIT_TYPE_CONVERSION_STR;
					depositTypeId = ConstantsBase.SALES_DEPOSIT_TYPE_CONVERSION;
				}
				w.setOnlineDepositAmount(CommonUtil.displayAmount(amount, currencyId) + " " + depositType);
				w.setOnlineDepositType(depositTypeId);
				ApplicationDataBase.addWriterAfterSuccessDeposit(w);
			}
		} finally {
			closeConnection(con);
		}
	}

	/**
	 * update online deposits writers list after inserting new transaction issue record
	 * @param writerId
	 * @param issuesActionId
	 * @throws SQLException
	 */
	public static void updateOnlineDeposits(Connection con , long writerId, long userId, long trxId) throws SQLException {
		try {
			long skinId = UsersDAOBase.getSkinIdByUserId(con, userId);
			long currencyId = Long.parseLong(UsersDAOBase.getCurrencyIdByUserId(con, String.valueOf(userId)));
			long amount = TransactionsDAOBase.getAmountById(con, trxId);
			Writer w = WritersDAO.get(con, writerId);

			if (w.getSales_type() > 0){
				w.setOnlineDepositSkinId(skinId);
				String depositType = ConstantsBase.SALES_DEPOSIT_TYPE_RETENTION_STR;
				Long firstDepositId = UsersDAOBase.getFirstDepositId(con, userId);
				int depositTypeId = ConstantsBase.SALES_DEPOSIT_TYPE_RETENTION;
				if (null != firstDepositId && firstDepositId == trxId) {
					depositType = ConstantsBase.SALES_DEPOSIT_TYPE_CONVERSION_STR;
					depositTypeId = ConstantsBase.SALES_DEPOSIT_TYPE_CONVERSION;
				}
				w.setOnlineDepositAmount(CommonUtil.displayAmount(amount, currencyId) + " " + depositType);
				w.setOnlineDepositType(depositTypeId);
				ApplicationDataBase.addWriterAfterSuccessDeposit(w);
			}
		} finally {

		}
	}

}