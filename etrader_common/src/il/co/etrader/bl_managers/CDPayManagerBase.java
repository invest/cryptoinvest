//package il.co.etrader.bl_managers;
//
//import java.sql.Connection;
//import java.sql.SQLException;
//
//import com.anyoption.common.bl_vos.CDPayDeposit;
//import com.anyoption.common.daos.CDPayDepositDAO;
//import com.anyoption.common.managers.BaseBLManager;
//
///**
// * @author liors
// *
// */
//public class CDPayManagerBase extends BaseBLManager {
//	
//	public static CDPayDeposit getCDPayDeposit(long  transactionId) throws SQLException {
//        Connection con = getConnection();
//        try {
//        	return CDPayDepositDAO.get(con, transactionId); 
//        } finally {
//           closeConnection(con);
//        }		
//	}	
//}