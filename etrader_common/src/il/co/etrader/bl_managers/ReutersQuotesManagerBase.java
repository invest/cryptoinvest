package il.co.etrader.bl_managers;

import java.sql.Connection;
import java.sql.SQLException;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.Opportunity;
import com.anyoption.common.beans.ReutersQuotes;
import com.anyoption.common.managers.BaseBLManager;

import il.co.etrader.dao_managers.ReutersQuotesDAOBase;

public class ReutersQuotesManagerBase extends BaseBLManager {
	private static final Logger log = Logger.getLogger(ReutersQuotesManagerBase.class);

	public static void insert(Opportunity o) throws SQLException {
		Connection conn = getConnection();
		try {
			conn.setAutoCommit(false);
			log.debug("try to insert into db LastUpdateQuote opp id " + o.getId() + " /n " + o.getLastUpdateQuote());
			ReutersQuotesDAOBase.insert(conn, o.getId(), o.getLastUpdateQuote());
		    conn.commit();
        } catch (SQLException e) {
        	log.error("Exception in insert Reuters Quotes! ", e);
        	try {
        		conn.rollback();
        	} catch (Throwable it) {
        		log.error("Can't rollback.", it);
        	}
        	throw e;
        } finally {
        	try {
        		conn.setAutoCommit(true);
        	} catch (Exception e) {
        		log.error("Can't set back to autocommit.", e);
        	}
        	closeConnection(conn);
        }
	}

	public static void insertManually(long oppId, ReutersQuotes rq) throws SQLException {
		Connection conn = getConnection();
		try {
			conn.setAutoCommit(false);
			ReutersQuotesDAOBase.insert(conn, oppId, rq);
			conn.commit();
		} catch (SQLException e) {
			log.error("Exception in insert Reuters Quotes! ", e);
			try {
				conn.rollback();
			} catch (Throwable it) {
				log.error("Can't rollback.", it);
			}
			throw e;
		} finally {
			try {
				conn.setAutoCommit(true);
			} catch (Exception e) {
				log.error("Can't set back to autocommit.", e);
			}
			closeConnection(conn);
		}
	}

	public static boolean updateManually(long oppId, ReutersQuotes rq) throws SQLException {
		Connection con = null;
		try {
			con = getConnection();
			return ReutersQuotesDAOBase.update(con, oppId, rq);
		} catch (SQLException e) {
			log.debug("Unable to update quotes for opportunity with id " + oppId, e);
			throw e;
		} finally {
			closeConnection(con);
		}
	}
}