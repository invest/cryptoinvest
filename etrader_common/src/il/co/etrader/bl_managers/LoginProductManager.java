package il.co.etrader.bl_managers;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import com.anyoption.common.managers.BaseBLManager;

/**
 * @author kiril.mutafchiev
 */
public class LoginProductManager extends BaseBLManager {

	protected static final String LOGIN_PRODUCT_TIME_ZONE = "Asia/Jerusalem";
	protected static final int AUTO_WEEKEND_START_DAY_OF_WEEK = Calendar.FRIDAY;
	protected static final int AUTO_WEEKEND_START_HOUR = 23;
	protected static final int AUTO_WEEKEND_START_MINUTE = 31;
	protected static final int AUTO_WEEKEND_END_DAY_OF_WEEK = Calendar.SUNDAY;
	protected static final int AUTO_WEEKEND_END_HOUR = 23;
	protected static final int AUTO_WEEKEND_END_MINUTE = 59;
	protected static final LoginProductEnum AUTO_WEEKEND_PRODUCT = LoginProductEnum.ONE_TOUCH;
	protected static final LoginProductEnum AUTO_NOT_WEEKEND_PRODUCT = LoginProductEnum.BINARY;

	public enum LoginProductEnum {
									BINARY(1), ONE_TOUCH(2), OPTION_PLUS(3), BUBBLES(4), DYNAMICS(5), LONG_TERM(6);

		private int id;

		private static final Map<Integer, LoginProductEnum> ID_TO_PRODUCT_MAP = new HashMap<>();
		static {
			for (LoginProductEnum product : LoginProductEnum.values()) {
				ID_TO_PRODUCT_MAP.put(product.getId(), product);
			}
		}

		private LoginProductEnum(int id) {
			this.id = id;
		}

		public static LoginProductEnum getById(int id) {
			LoginProductEnum product = ID_TO_PRODUCT_MAP.get(id);
			if (product == null) {
				throw new IllegalArgumentException("No product with id: " + id);
			} else {
				return product;
			}
		}

		public int getId() {
			return id;
		}
	}
}