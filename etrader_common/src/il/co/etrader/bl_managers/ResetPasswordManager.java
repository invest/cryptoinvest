package il.co.etrader.bl_managers;

import il.co.etrader.bl_vos.ResetPassword;
import il.co.etrader.dao_managers.ResetPasswordDAO;

import java.sql.Connection;
import java.sql.SQLException;

import com.anyoption.common.managers.BaseBLManager;

public class ResetPasswordManager extends BaseBLManager {

	public ResetPasswordManager() throws Exception {
	}

	public static boolean insert(String userName, String resKey) throws SQLException {
		Connection conn = getConnection();
		boolean res;
		try {
			res = ResetPasswordDAO.insert(conn, userName, resKey);
		} finally {
			closeConnection(conn);
		}
		return res;
	}

	public static ResetPassword get(String resKey, boolean addClick) throws SQLException {
		Connection conn = getConnection();
		ResetPassword rp;
		try {
			rp = ResetPasswordDAO.get(conn, resKey, addClick);
		} finally {
			closeConnection(conn);
		}
		return rp;
	}
}