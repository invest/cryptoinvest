package il.co.etrader.bl_managers;

import il.co.etrader.bl_vos.Contact;
import il.co.etrader.bl_vos.UserBase;
import il.co.etrader.dao_managers.ContactsDAO;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.Country;
import com.anyoption.common.daos.GeneralDAO;
import com.anyoption.common.managers.BaseBLManager;

	public abstract class ContactsManager extends BaseBLManager {
		private static final Logger log = Logger.getLogger(TransactionsManagerBase.class);
				
		public static Contact insertContactRequest(Contact newContactReq, boolean importLeadsScreen, 
				boolean insertRemarketingLogin, UserBase user, HttpServletRequest request)throws SQLException{
			Connection con = null;
			try {
				con = getConnection();
				long id = GeneralDAO.getSequenceNextVal(con,"SEQ_CONTACTS_ID");
				newContactReq.setId(id);
				ContactsDAO.insertContactRequest(con, newContactReq, importLeadsScreen);
				if (insertRemarketingLogin && user == null) { 
					RemarketingLoginsManagerBase.insertRemarektingLoginBytype(id, RemarketingLoginsManagerBase.REMARKETING_TYPE_CONTACT, 0, 
							null, null, null, 0, request);
				}	
				return newContactReq;
			} finally {
				closeConnection(con);
			}
		}		

		/**
		 * Update contact
		 * @param c
		 * @throws SQLException
		 */
		public static void updateContact(Contact c)throws SQLException{
			Connection con = null;
			try {
				con = getConnection();
				ContactsDAO.updateContact(con,c);
			} finally {
				closeConnection(con);
			}
		}

		public static void updateContactRequest(long contactId,long userId)throws SQLException{
			Connection con = null;
			try {
				con = getConnection();
				ContactsDAO.updateContactRequest(con,contactId,userId);
			} finally {
				closeConnection(con);
			}
		}

		/**
		 * Check is contact already exists, if yes return contactId
		 * @param phone contact me phone number
		 * @param email contact email
		 * @return
		 * @throws SQLException
		 */
		public static long isExist(String phone, String email) throws SQLException {
			Connection con = null;
			try {
				con = getConnection();
				return ContactsDAO.isExists(con, phone, email);
			} finally {
				closeConnection(con);
			}
		}

		/**
		 * Get the contact by contactId
		 * @param contactId
		 * @return
		 * @throws SQLException
		 */
		public static Contact getContactByID(long contactId) throws SQLException {
			Connection con = null;
			try {
				con = getConnection();
				Contact c = ContactsDAO.getById(con, contactId);
				if (null != c) {
				    Country country = ApplicationDataBase.getCountry(c.getCountryId());
			        if (null != country) {
			        	 c.setCountryName(country.getDisplayName());
			        }
				}
				return c;
			} finally {
				closeConnection(con);
			}
		}

		 /**
	     * checks if there's a contact with the same phone number as the new user.
	     * @param connection
	     * @param user
	     * @throws SQLException
	     */
	    public static Contact searchFreeContactByUserPhone(Connection con, UserBase user) throws SQLException {
	    	Contact contact = null;
	    	try {
	        	contact = ContactsDAO.searchFreeContactByUserPhone(con, user);
	        } finally {
	        }
	        return contact;
	    }

	    /**
	     * Checks if there's a user with the same email/mobile in order to connect contact with user
	     * @param contact
	     * @return
	     * @throws SQLException
	     */
	    public static long searchUserIdFromContactDetails(Contact contact) throws SQLException {
			Connection con = null;
			try {
				con = getConnection();
				return ContactsDAO.searchUserIdFromContactDetails(con, contact);
			} finally {
				closeConnection(con);
			}

	    }

	    public static Contact getContactByEmail(String email) throws SQLException {
			Connection con = null;
			try {
				con = getConnection();
				return ContactsDAO.getContactByEmail(con, email);
			} finally {
				closeConnection(con);
			}
	    }

		public static void updateDuplicatesContactsByEmail(Contact contact) throws SQLException {
			Connection con = null;
			try {
				con = getConnection();
				ContactsDAO.updateDuplicatesContactsByEmail(con, contact);
			} finally {
				closeConnection(con);
			}
		}
	   
		public static void updateIsContactByEmail(Contact c) throws SQLException {
			
			Connection con = null;
			try{
				con = getConnection();
				ContactsDAO.updateIsContactByEmail(con, c);
			}finally{
				
				closeConnection(con);
			}
			  
		  }
		
		/**
		 * Insert contact list
		 * @param list
		 * @throws SQLException
		 */
		public static void insertContactList(ArrayList<Contact> list)throws SQLException{
			Connection con = getConnection();
			try { 
				ContactsDAO.insertContactList(con, list);
			} finally {
				closeConnection(con);
			}
		}
		
		/**
		 * Is contact by affiliate exists
		 * @param contact
		 * @return boolean
		 * @throws SQLException
		 */
		public static boolean isContactByAffiliateExists(Contact contact) throws SQLException {
			Connection con = getConnection();
			try {
				return ContactsDAO.isContactByAffiliateExists(con, contact);
			} finally {
				closeConnection(con);
			}
		}
}