//package il.co.etrader.bl_managers;
//
//import il.co.etrader.dao_managers.MarketingTrackingDAOBase;
//
//import java.sql.Connection;
//import java.sql.SQLException;
//import java.util.ArrayList;
//
//import com.anyoption.common.beans.base.MarketingTracking;
//import com.anyoption.common.beans.base.MarketingTrackingCookieStatic;
//import com.anyoption.common.managers.BaseBLManager;
//
//public class MarketingTrackingDAOManager extends BaseBLManager{
//	
//	
//    public static boolean insertMarketingTracking(ArrayList<MarketingTracking> tracking) throws SQLException{
//        
//		Connection con = getConnection();
//		boolean result = false;
//		
//		try{
//			
//			result = MarketingTrackingDAOBase.insertMarketingTracking(con, tracking);
//			
//		}finally{
//			
//			closeConnection(con);
//		}
//		return result;
//    }
//
//    public static String getMidByUserId(long userId) throws SQLException{
//    	
//    	Connection con = getConnection();
//    	String mId = "";
//    	try{
//    		mId = MarketingTrackingDAOBase.getMidByUserId(con, userId);
//    	}finally{
//    		closeConnection(con);
//    	}
//    	return mId;
//    }
//    
//    public static long getUserIdByMid(String mid) throws SQLException{
//    	
//    	Connection con = getConnection();
//    	long userId = 0;
//    	try{
//    		userId = MarketingTrackingDAOBase.getUserIdByMid(con, mid);
//    	}finally{
//    		closeConnection(con);
//    	}
//    	return userId;
//    }
//    
//    public static long getContactIdByMid(String mid) throws SQLException{
//    	
//    	Connection con = getConnection();
//    	long contactId = 0;
//    	try{
//    		contactId = MarketingTrackingDAOBase.getContactIdByMid(con, mid);
//    	}finally{
//    		closeConnection(con);
//    	}
//    	return contactId;
//    }
//    
//    public static boolean isHaveActivity(String mid) throws SQLException{
//    	
//    	Connection con = getConnection();
//    	boolean result = false;
//    	try{
//    		result = MarketingTrackingDAOBase.isHaveActivity(con, mid);
//    	}finally{
//    		closeConnection(con);
//    	}
//    	return result;
//    }
//    
//	public static MarketingTrackingCookieStatic getMarketingTrackingCookieStatic(String mId) throws SQLException {
//		Connection con = getConnection();
//		try {
//			return MarketingTrackingDAOBase.getMarketingTrackingCookieStatic(con, mId);
//		} finally {
//			closeConnection(con);
//		}
//	}
//}
