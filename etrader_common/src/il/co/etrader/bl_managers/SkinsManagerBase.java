package il.co.etrader.bl_managers;

import java.sql.Connection;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Map;

import javax.faces.model.SelectItem;

import com.anyoption.common.beans.Skin;
import com.anyoption.common.beans.TreeItem;

import il.co.etrader.bl_vos.Skins;
import il.co.etrader.dao_managers.SkinsDAOBase;

/**
 *  Skins common manager.
 */
public class SkinsManagerBase extends com.anyoption.common.managers.SkinsManagerBase {
	public static final long BUSINESS_CASE_ETRADER = 1;
	public static final long BUSINESS_CASE_ANYOPTION = 2;
	public static final long BUSINESS_CASE_PARTNERS = 3;
	
	private static HashMap<Long, String> skinsFilter;
	public static final String FILTER_SKINS = "skins";

	private static Map<Long, ArrayList<TreeItem>> treeItems = new HashMap<Long, ArrayList<TreeItem>>();
	private static Map<Long, LocalDate> treeItemsDates = new HashMap<Long, LocalDate>();
	
	/**
	 * This method chooses a skin for a new user by using the country of the new user and ignoring the URL_ID column
	 * @param countryId - the user countryId
	 * @return
	 * @throws SQLException
	 */
	public static Skins getNewUserSkin(long countryId) throws SQLException {
		Connection con = getConnection();
		try {
			return SkinsDAOBase.getNewUserSkin(con, countryId);
		} finally {
			closeConnection(con);
		}
	}

	public static Skins getById(long skinId) throws SQLException {
		Connection con = getConnection();
		try {
			return SkinsDAOBase.getById(con, (int)skinId);
		} finally {
			closeConnection(con);
		}
	}

    public static ArrayList<SelectItem> getMarketsSI(int skinId) throws SQLException {
        Connection con = getConnection();
        try {
            return SkinsDAOBase.getMarketsSI(con, skinId);
        } finally {
            closeConnection(con);
        }
    }

	public static HashMap<String, String> getSubDomainsLocales(boolean isSubdomainIsKey, boolean isRegulated) throws SQLException {
        Connection con = getConnection();
        try {
            return SkinsDAOBase.getSubDomainsLocales(con, isSubdomainIsKey, isRegulated);
        } finally {
            closeConnection(con);
        }
	}
	
	public static ArrayList<Long> getUnvisibleMarketsPerSkin(long skinId) throws SQLException {
        Connection con = getConnection();
        try {
            return SkinsDAOBase.getUnvisibleMarketsPerSkin(con, skinId);
        } finally {
            closeConnection(con);
        }
	}

	public static HashSet<String> getRegulatedSkins() throws SQLException {
        Connection con = getConnection();
        try {
            return SkinsDAOBase.getRegulatedSkins(con);
        } finally {
            closeConnection(con);
        }
	}
	
	/**
	 * update control counting per skin.
	 * @param con
	 * @param skinId
	 * @param controlCount
	 * @throws SQLException
	 */
	public static void updateControlCount(Connection con, long skinId, long controlCount) throws SQLException {
		SkinsDAOBase.updateControlCount(con, skinId, controlCount);
        
	}
	
	public static HashMap<Long, String> getAllSkinsFilter() {
		if (skinsFilter == null) {
			Hashtable<Long, Skin> skinList = SkinsManagerBase.getSkins();
			skinsFilter = new HashMap<Long, String>();
			for (Long key : skinList.keySet()) {
				if (skinList.get(key).isActive()) {
					skinsFilter.put((long) skinList.get(key).getId(), skinList.get(key).getDisplayName());
				}
			}
		}
		return skinsFilter;
	}
	
	/**
	 * Get netrefer combinations id
	 * @param skinId
	 * @return ArrayList<Long>
	 * @throws SQLException
	 */
	public static ArrayList<Long> getNetreferCombinationsId(long skinId) throws SQLException {
		Connection con = getConnection();
		try {
            return SkinsDAOBase.getNetreferCombinationsId(con, skinId);
        } finally {
            closeConnection(con);
        }
	}
	
	public static ArrayList<TreeItem> getTreeItems(long skinId, boolean is0100) throws SQLException {
		LocalDate currentDate = LocalDateTime.now().toLocalDate();
		if (null == treeItemsDates.get(skinId)
				|| currentDate.isAfter(treeItemsDates.get(skinId))
				|| treeItems.get(skinId) == null) {
			synchronized (treeItems) {
				if (null == treeItemsDates.get(skinId)
						|| currentDate.isAfter(treeItemsDates.get(skinId))
						|| treeItems.get(skinId) == null) {
					Connection con = null;
					try {
						con = getConnection();
						treeItems.put(skinId, SkinsDAOBase.getTreeItems(con, skinId, is0100));
						treeItemsDates.put(skinId, currentDate);
					} finally {
						closeConnection(con);
					}
				}				
			}
		}
		return treeItems.get(skinId);
	}
}