package il.co.etrader.bl_managers;

import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.bouncycastle.crypto.CryptoException;

import com.anyoption.common.beans.Issue;
import com.anyoption.common.beans.IssueAction;
import com.anyoption.common.bl_vos.PopulationEntryBase;
import com.anyoption.common.managers.UsersPendingDocsManagerBase;

import il.co.etrader.bl_vos.Chat;
import il.co.etrader.bl_vos.Contact;
import il.co.etrader.bl_vos.UserBase;
import il.co.etrader.bl_vos.Writer;
import il.co.etrader.dao_managers.ChatDAOBase;
import il.co.etrader.dao_managers.FilesDAOBase;
import il.co.etrader.dao_managers.IssuesDAOBase;
import il.co.etrader.dao_managers.PopulationsDAOBase;
import il.co.etrader.issueActions.ActionHandlerBase;
import il.co.etrader.issueActions.ActionHandlerFactory;
import il.co.etrader.util.ConstantsBase;
import oracle.ucp.jdbc.HarvestableConnection;


public class IssuesManagerBase extends com.anyoption.common.managers.IssuesManagerBase {
	private static final Logger logger = Logger.getLogger(IssuesManagerBase.class);	

	/**
	 * Get actions by population entry id
	 * @param popEntryId
	 * @param issueId
	 * @return
	 * @throws SQLException
	 */
	public static ArrayList<IssueAction> getActionsCount(long popEntryId, long issueId) throws SQLException {
		Connection con = null;
		try {
			con = getConnection();
			return IssuesDAOBase.getActionsCount(con, popEntryId, issueId);
		} finally {
			closeConnection(con);
		}
	}

	/**
	 * Update issues by population entry id
	 * this menthod called after in
	 * @param popEntryId
	 * @param userId
	 * @throws SQLException
	 */
	public static void updateIssuesAndUserForContact(Connection con, PopulationEntryBase popUser) throws SQLException {
		try {
			con.setAutoCommit(false);

			IssuesDAOBase.updateIssuesByPopUserId(con, popUser);
			PopulationsDAOBase.updateUserIdForContact(con, popUser);
		} catch (Exception exp) {
			logger.error("Exception in updateIssuesAndUserForContact event ", exp);
		    try {
				con.rollback();
		    } catch (Throwable it) {
		    	logger.error("Can't rollback.", it);
			}
		} finally {
			con.setAutoCommit(true);
		}
	}

	public static void insertAction(Connection con,IssueAction action) throws SQLException {
    	try{
    		IssuesDAOBase.insertAction(con, action);
    	} finally {
		}
    }
	
	public static void insertAction(IssueAction action) throws SQLException {
		Connection con = null;
    	try{
    		con = getConnection();
    		IssuesDAOBase.insertAction(con, action);
    	} finally {
    		closeConnection(con);
		}
    }
	public static boolean insertIssue(Issue issue, IssueAction issueAction,String utcOffset,long writerId, UserBase user, int screenId) throws SQLException {
    	Connection con = getConnection();
    	boolean res = false;
    	try {


    		con.setAutoCommit(false);

			// Insert Issue
			IssuesDAOBase.insertIssue(con, issue);

			issueAction.setIssueId(issue.getId());
			issueAction.setWriterId(writerId);
			issueAction.setActionTime(new Date());
			issueAction.setActionTimeOffset(utcOffset);
			// Insert Issue action
			res = validateAndInsertAction(con, issueAction, user, issue, screenId);
			if (!res){
				logger.log(Level.INFO, "Can't insertIssue: " + issue.toString() + " and action: " + issueAction.toString());
				con.rollback();
				return false;
			}else{
				con.commit();
				return true;
			}
			//insertAction(con, issueAction);
    	} catch (Exception e) {
    		logger.log(Level.ERROR, "Can't insertIssue: " + issue.toString() + " and action: " + issueAction.toString(), e);
    		try {
				con.rollback();
			} catch (SQLException ie) {
				logger.log(Level.ERROR, "Can't rollback.", ie);
			}

		} finally {
			con.setAutoCommit(true);
    		closeConnection(con);
    	}
		return true;
    }
	
	public static boolean insertIssue(Connection con, Issue issue, IssueAction issueAction,String utcOffset,long writerId, UserBase user, int screenId) throws Exception {
    	boolean res = false;
		// Insert Issue
		IssuesDAOBase.insertIssue(con, issue);
		issueAction.setIssueId(issue.getId());
		issueAction.setWriterId(writerId);
		issueAction.setActionTime(new Date());
		issueAction.setActionTimeOffset(utcOffset);
		// Insert Issue action
		res = validateAndInsertAction(con, issueAction, user, issue, screenId);
		if (!res){
			logger.log(Level.INFO, "Can't insertIssue: " + issue.toString() + " and action: " + issueAction.toString());
			return false;
		}else{
			return true;
		}
    }
	
	public static boolean insertAction(Issue issue, IssueAction issueAction,String utcOffset,long writerId, UserBase user, int screenId) throws SQLException {
        Connection con = getConnection();
        boolean res = false;
        try {


            con.setAutoCommit(false);

            // Insert Issue
            //IssuesDAOBase.insertIssue(con, issue);

            issueAction.setIssueId(issue.getId());
            issueAction.setWriterId(writerId);
            issueAction.setActionTime(new Date());
            issueAction.setActionTimeOffset(utcOffset);
            // Insert Issue action
            res = validateAndInsertAction(con, issueAction, user, issue, screenId);
            if (!res){
                logger.log(Level.INFO, "Can't insertIssue: " + issue.toString() + " and action: " + issueAction.toString());
                con.rollback();
                return false;
            }else{
                con.commit();
                return true;
            }
            //insertAction(con, issueAction);
        } catch (Exception e) {
            logger.log(Level.ERROR, "Can't insertIssue: " + issue.toString() + " and action: " + issueAction.toString(), e);
            try {
                con.rollback();
            } catch (SQLException ie) {
                logger.log(Level.ERROR, "Can't rollback.", ie);
            }

        } finally {
            con.setAutoCommit(true);
            closeConnection(con);
        }
        return true;
    }
	
	public static boolean insertDocRequest(long issueActionId, long ccId, String type, long userId) throws SQLException {
        Connection con = getConnection();
        try {
            IssuesDAOBase.insertDocRequest(con, issueActionId, ccId, type, userId);           
        } catch (Exception e) {
            logger.log(Level.ERROR, "Can't insertDocRequest: " + issueActionId, e);
        } finally {
            closeConnection(con);
        }
        return true;
    }
	
	public static boolean updateDocState(long userId, long ccId, long docState) throws SQLException {
        Connection con = getConnection();
        try {
            IssuesDAOBase.updateDocState(con, userId, ccId, docState);            
        } catch (Exception e) {
            logger.log(Level.ERROR, "Can't updateDocState: " + docState, e);
        } finally {
            closeConnection(con);
        }
        return true;
    }

	/**
	 * Get action by id
	 * @param id of the action
	 * @return
	 * @throws SQLException
	 */
	public static IssueAction getActionById(long actionId) throws SQLException {
		Connection con = null;
		try {
			con = getConnection();
			return IssuesDAOBase.getActionById(con, actionId);
		} finally {
			closeConnection(con);
		}
	}

	  /**
	   * Get users/contcat reached calls num
	   * @param con db connection
	   * @param popUserEntry population user
	   * @return
	   * @throws SQLException
	   */
	  public static long getReachedCallsNum(PopulationEntryBase popUserEntry) throws SQLException {
			Connection con = null;
			try {
				con = getConnection();
				return IssuesDAOBase.getReachedCallsNum(con, popUserEntry);
			} finally {
				closeConnection(con);
			}
	  }


	  public static boolean validateAndInsertAction(Connection con, IssueAction action, UserBase user, Issue i, int screenId) throws Exception{
		  	boolean res = false;
		  	long issuePopEntryId = i.getPopulationEntryId();
		  	FacesContext context = FacesContext.getCurrentInstance();

			if (null != action.getActionTypeId()) {
				ActionHandlerBase ah = ActionHandlerFactory.getInstance(action, user.getStatusId());
				if (null != ah){

					if (0 != issuePopEntryId){
						ah.setPopUserEntry(PopulationsDAOBase.getPopulationEntryByEntryId(con, issuePopEntryId));
					}

					res = ah.beforeInsertAction(con, user, action, context, screenId);
					if (res){
						ah.insertAction(con, user, i, action, context);
					}

				}
				
				if(i.getSubjectId().contains(com.anyoption.common.managers.IssuesManagerBase.ISSUE_SUBJECT_PENDING_DOC + "") &&
						!(action.getWriterId() == Writer.WRITER_ID_WEB 
							|| action.getWriterId() == Writer.WRITER_ID_AUTO
								|| action.getWriterId() == Writer.WRITER_ID_MOBILE)){
					UsersPendingDocsManagerBase.updateLastWriterActionTime(i.getId());
				}
			}
			return res;
	  }

	/**
	 * Get relevant cards/Id copy when pressing on issue action id
	 * @param issueActionId
	 * @return
	 * @throws SQLException
	 * @throws CryptoException
	 * @throws NoSuchPaddingException  
	 * @throws NoSuchAlgorithmException 
	 * @throws BadPaddingException 
	 * @throws IllegalBlockSizeException 
	 * @throws InvalidKeyException 
	 * @throws InvalidAlgorithmParameterException 
	 */
	public static String getRelevantCardsToIssueAction(long issueActionId)	throws SQLException, CryptoException, InvalidKeyException,
																			IllegalBlockSizeException, BadPaddingException,
																			NoSuchAlgorithmException, NoSuchPaddingException,
																			InvalidAlgorithmParameterException {
		Connection con = null;
		try {
			con = getConnection();
			return IssuesDAOBase.getRelevantCardsToIssueAction(con, issueActionId);
		} finally {
			closeConnection(con);
		}
	}

	public static void insertIssue(Issue issue, IssueAction issueAction,String utcOffset,long writerId, UserBase user, int screenId, Connection conn) throws Exception {
			// Insert Issue
			IssuesDAOBase.insertIssue(conn, issue);

			issueAction.setIssueId(issue.getId());
			issueAction.setWriterId(writerId);
			issueAction.setActionTime(new Date());
			issueAction.setActionTimeOffset(utcOffset);
			validateAndInsertAction(conn, issueAction, user, issue, screenId);

    }

	public static void insertIssueForContact(Connection conn, Issue issue, IssueAction issueAction, String utcOffset, long writerId) throws SQLException {

			IssuesDAOBase.insertIssue(conn, issue);

			issueAction.setIssueId(issue.getId());
			issueAction.setWriterId(writerId);
			issueAction.setActionTime(new Date());
			issueAction.setActionTimeOffset(utcOffset);

			IssuesDAOBase.insertAction(conn, issueAction);
    }
	
	public static boolean isExistsIssueByUserAndCC(	long ccNum, long userId,
													int subjectId)	throws SQLException, CryptoException, InvalidKeyException,
																	NoSuchAlgorithmException, NoSuchPaddingException,
																	IllegalBlockSizeException, BadPaddingException,
																	UnsupportedEncodingException, InvalidAlgorithmParameterException {
	    Connection con = null;
	    try {
	      con = getConnection();

	      if (IssuesDAOBase.isExistsIssueByUserAndCC(con, ccNum, userId, subjectId)==0) {
              return false;
          } else {
              return true;
          }
          
	    } finally {
	      closeConnection(con);
	    }
	  }
   
   public static int[] getExistsIssueByUserAndCC(long ccNum, long userId) throws SQLException, CryptoException
   {
     Connection con = null;
     try {
       con = getConnection();

       return IssuesDAOBase.getExistsIssueByUserAndCC(con, ccNum, userId);
       
     } finally {
       closeConnection(con);
     }
   }
   
   public static boolean updateIssueState(int statusId, long actionId, long issueId) throws SQLException {
       Connection con = getConnection();
       boolean res = false;
       try {
           res=IssuesDAOBase.updateIssueStatusByIaId(con, statusId, actionId, issueId);            
       } catch (Exception e) {
           logger.log(Level.ERROR, "Can't updateIssueState for issue: " + Long.toString(issueId), e);
       } finally {
           closeConnection(con);
       }
       return res;
   }
   
   public static boolean InsertIssueRiskDocReq(long issueActionId, String type, long ccId) throws SQLException {
	   Connection con = getConnection();
       try {
           if(IssuesDAOBase.InsertIssueRiskDocReq(con, issueActionId,type,ccId)) {
        	   return true;
           } 
       } catch (Exception e) {
           logger.log(Level.ERROR, "Can't insertDocRequest: " + issueActionId, e);
           return false;
       } finally {
           closeConnection(con);
       }
       return true;
   }
   
   public static List<com.anyoption.common.beans.File> getFileListByActionId(long issueId, long userId, Locale locale) throws SQLException{
	   
	   Connection con = getConnection();
	   List<com.anyoption.common.beans.File> list = null;
       try {
          list = FilesDAOBase.getFilesByActionId(con,issueId,userId, locale);
       } finally {
           closeConnection(con);
       }
       return list;
   }
   
public static List<com.anyoption.common.beans.File> getFileListByUserId(long userId, Locale locale) throws SQLException{
	   
	   Connection con = getConnection();
	   List<com.anyoption.common.beans.File> list = null;
       try {
          list = FilesDAOBase.getFilesByUserId(con,userId, locale);
       } finally {
           closeConnection(con);
       }
       return list;
   }
   
   /*
    * changes the comment of the issues created for deposit before filling 
    * the mandatory questionnaire
    */
   public static void updatePendingRegulationIssuesForUser(long userId) throws SQLException {
   	Connection con = null;
    	try {
			con = getConnection();
			IssuesDAOBase.updatePendingRegulationIssuesForUser(con, userId);
    	}finally {
    		closeConnection(con);
    	}
	}

    /**
     * Add mail issue for user.
     *
     * @param userId
     * @param issueSubjectId
     * @param isContactByMail
     * @param issueActionComment
     * @param transactionId 
     */
    public static void insertIssueForMail(long userId, long issueSubjectId, boolean isContactByMail, String issueActionComment, long transactionId) throws SQLException {
        Connection conn = null;
        try {
            conn = getConnection();
            conn.setAutoCommit(false);
            Issue issue = new Issue();
            issue.setSubjectId(String.valueOf(issueSubjectId));
            issue.setStatusId(String.valueOf(IssuesManagerBase.ISSUE_STATUS_FINISHED));
            issue.setUserId(userId);
            issue.setPriorityId(String.valueOf(IssuesManagerBase.ISSUE_PRIORITY_LOW));
            issue.setType(ConstantsBase.ISSUE_TYPE_REGULAR);
            IssuesDAOBase.insertIssue(conn, issue);

            IssueAction issueAction = new IssueAction();
            issueAction.setChannelId(String.valueOf(IssuesManagerBase.ISSUE_CHANNEL_EMAIL));
            if (isContactByMail) {
                issueAction.setActionTypeId(String.valueOf(IssuesManagerBase.ISSUE_ACTION_EMAIL));
            } else {
                issueAction.setActionTypeId(String.valueOf(IssuesManagerBase.ISSUE_ACTION_COMMENT));
            }
            issueAction.setWriterId(Writer.WRITER_ID_AUTO);
            issueAction.setSignificant(false);
            issueAction.setActionTime(new Date());
            issueAction.setActionTimeOffset("GMT+00:00");
            issueAction.setComments(issueActionComment);
            issueAction.setTransactionId(transactionId);
            issueAction.setIssueId(issue.getId());
            IssuesDAOBase.insertAction(conn, issueAction);
            conn.commit();
        } catch (SQLException sqle) {
            rollback(conn);
            throw sqle;
        } finally {
            setAutoCommitBack(conn);
            closeConnection(conn);
        }
    }
    
    public static boolean isPendingDocIssuesForUser(long userId) throws SQLException {
    	Connection con = null;
    	boolean result = false;
    	try {
    		con = getConnection();
    		result = IssuesDAOBase.isPendingDocIssuesForUser(con,userId);
 
    	}finally {
          closeConnection(con);    		
    	}
    	return result;
    }
    
    public static boolean isPendingDocIssuesForCC(long userId,long creditCardId) throws SQLException {
    	Connection con = null;
    	boolean result = false;
    	try {
    		con = getConnection();
    		result = IssuesDAOBase.isPendingDocIssuesForCC(con,userId,creditCardId);
 
    	}finally {
          closeConnection(con);    		
    	}
    	return result;
    }
    
	public static void createSmsOrMailIssue(ArrayList<UserBase> users, HttpSession session, int issueChannelId, int issueActionTypeId, int issueSubjectId, String message, long writerId) throws SQLException {
		// Filling issues fields
		Issue issue = new Issue();
		issue.setSubjectId(String.valueOf(issueSubjectId)); // selected on screen
		issue.setStatusId(String.valueOf(IssuesManagerBase.ISSUE_STATUS_FINISHED));
		issue.setPriorityId(String.valueOf(IssuesManagerBase.ISSUE_PRIORITY_MEDIUM));
		issue.setType(ConstantsBase.ISSUE_TYPE_REGULAR);
		// Filling action fields
		IssueAction issueAction = new IssueAction();
		issueAction.setChannelId(String.valueOf(issueChannelId)); //EMAIL/SMS
		issueAction.setActionTypeId(String.valueOf(issueActionTypeId));
		issueAction.setWriterId(writerId);
		issueAction.setSignificant(false);
		issueAction.setComments(message);

		Connection conn = null;
		HarvestableConnection hc = null;
		try{
			// Insert Issue & action
			conn = getConnection();
			conn.setAutoCommit(false);
			hc = (HarvestableConnection) conn;
			hc.setConnectionHarvestable(false);
			for(int i =0;i<users.size();i++) {
				UserBase u = users.get(i);
				issue.setContactId(u.getContactId());
				issue.setUserId(u.getId());
				try{
					// Insert Issue & action
					IssuesManagerBase.insertIssue(issue, issueAction, ConstantsBase.OFFSET_GMT, writerId, u, 0, conn);
					if(i%1000==0) {
						int percentageDone = 50 + (i/users.size())*100;
						session.setAttribute(ConstantsBase.PROMOTION_TOTAL_SENDED_TO_USERS, users.size() +"_"+percentageDone  );
					}
				} catch (Exception e){
					rollback(conn);
					throw new SQLException(e.getMessage());
				}
			}
			
		} finally {
			conn.commit();
			hc.setConnectionHarvestable(true);
			conn.setAutoCommit(true);
			closeConnection(conn);    		
	    }
	}
	
	
	public static void createSmsIssueForContact(Connection conn, Contact c, Long smsId, String message, long writerId) throws SQLException{
		// Filling issues fields
		Issue issue = new Issue();
		issue.setSubjectId(String.valueOf(ISSUE_SUBJ_SMS));
		issue.setStatusId(String.valueOf(IssuesManagerBase.ISSUE_STATUS_FINISHED));
		issue.setPriorityId(String.valueOf(IssuesManagerBase.ISSUE_PRIORITY_MEDIUM));
		issue.setType(ConstantsBase.ISSUE_TYPE_REGULAR);
		issue.setContactId(c.getId());

		// Filling action fields
		IssueAction issueAction = new IssueAction();
		issueAction.setChannelId(String.valueOf(ISSUE_CHANNEL_SMS)); 
		issueAction.setActionTypeId(String.valueOf(ISSUE_ACTION_SMS));
		issueAction.setWriterId(writerId);
		issueAction.setSignificant(false);
		issueAction.setComments(message);
		
		if(smsId!=null) {
			issueAction.setSmsId(smsId);
		}
		// Insert Issue & action

		IssuesManagerBase.insertIssueForContact(conn, issue, issueAction, ConstantsBase.OFFSET_GMT, writerId);
	}
    
    
	public static void createSmsOrMailIssue(Connection conn, UserBase u, int issueChannelId, int issueActionTypeId, int issueSubjectId, Long smsId, String message, long writerId) throws SQLException{
		// Filling issues fields
		Issue issue = new Issue();
		issue.setSubjectId(String.valueOf(issueSubjectId)); // selected on screen
		issue.setStatusId(String.valueOf(IssuesManagerBase.ISSUE_STATUS_FINISHED));
		issue.setPriorityId(String.valueOf(IssuesManagerBase.ISSUE_PRIORITY_MEDIUM));
		issue.setType(ConstantsBase.ISSUE_TYPE_REGULAR);
		issue.setContactId(u.getContactId());
		issue.setUserId(u.getId());
		// Filling action fields
		IssueAction issueAction = new IssueAction();
		issueAction.setChannelId(String.valueOf(issueChannelId)); //EMAIL/SMS
		issueAction.setActionTypeId(String.valueOf(issueActionTypeId));
		issueAction.setWriterId(writerId);
		issueAction.setSignificant(false);
		issueAction.setComments(message);
		
		if(smsId!=null) {
			issueAction.setSmsId(smsId);
		}
		// Insert Issue & action
		try{
			IssuesManagerBase.insertIssue(issue, issueAction, ConstantsBase.OFFSET_GMT, writerId, u, 0, conn);
		} catch (Exception e){
			throw new SQLException(e.getMessage());
		}
	}
	
	public static long getActionForDeactivation(long userId){
		Connection con = null;
		long result = 0l;
		try {
			try {
				con = getConnection();
				result =IssuesDAOBase.issueActionTypeForDeactivation(con, userId);
			} catch (SQLException e) {
				e.printStackTrace();
			}
			
		} finally {
			closeConnection(con);
		}
		return result;
	}

	/**
	 * Search issue id by pop entry id
	 * @param populationEntryId
	 * @return long
	 * @throws SQLException
	 */
	public static long searchIssueIdByPopEntryID(long populationEntryId) throws SQLException {
		Connection con = getSecondConnection();
		try {
			return IssuesDAOBase.searchIssueIdByPopEntryID(con, populationEntryId);
		} finally {
			closeConnection(con);
		}
	}
	
	public static Issue getIssueByCreditCardIdAndSubjectId(long creditCardId, long actionTypeId) throws SQLException {
		Connection con = getConnection();
		try {
			return IssuesDAOBase.getIssueByCreditCardIdAndSubjectId(con, creditCardId, actionTypeId);
		} finally {
			closeConnection(con);
		}
	}
	
	public static IssueAction getLastCcChangeIssueAction(long userId, long creditCardId, long issueId, long issueActionType) throws SQLException {
		Connection con = getConnection();
		try {
			return IssuesDAOBase.getLastCcChangeIssueAction(con, userId, creditCardId, issueId, issueActionType);
		} finally {
			closeConnection(con);
		}
	}
	
    public static void insertIssueAndChat(Chat chat, long userId, long writerId, Date chatTime, String userOffset) throws Exception {
    	Connection con = null;
		Issue issue = new Issue();
	   	IssueAction issueAction = new IssueAction();

	   	issue.setSubjectId(String.valueOf(IssuesManagerBase.ISSUE_SUBJ_GENERAL));
		issue.setStatusId(String.valueOf(IssuesManagerBase.ISSUE_STATUS_FINISHED));
		issue.setUserId(userId);
		issue.setPriorityId(String.valueOf(IssuesManagerBase.ISSUE_PRIORITY_LOW));
		issue.setType(ConstantsBase.ISSUE_TYPE_REGULAR);
		// Filling action fields
		issueAction.setChannelId("8"); // Chat
		issueAction.setActionTypeId(String.valueOf(IssuesManagerBase.ISSUE_ACTION_CHAT));
		issueAction.setWriterId(writerId);
		issueAction.setSignificant(false);
		issueAction.setActionTime(chatTime);
		issueAction.setActionTimeOffset(userOffset);
		issueAction.setComments("Chat");

		con = getConnection();
		try {
			con.setAutoCommit(false);
			//	Insert chat and get chatID
			ChatDAOBase.insert(con, chat);
			// Insert Issue
			IssuesDAOBase.insertIssue(con, issue);
			//	Insert Issue action
			issueAction.setIssueId(issue.getId());
			issueAction.setChatId(chat.getId());
			//IssuesManagerBase.validateAndInsertAction(con, action, user, i);
			IssuesDAOBase.insertAction(con, issueAction);
			con.commit();
    	} catch (Exception e) {
    		logger.error("Problem inserting Chat transcript and issue! due to: ", e);
    		try {
    			con.rollback();
    		} catch (Throwable it) {
    			logger.error("Can't rollback.", it);
    		}
		} finally {
			con.setAutoCommit(true);
			try {
        		con.close();
        	} catch (Exception e) {
        		logger.log(Level.ERROR, "Can't close connection ", e);
        	}
		}
    }
}
