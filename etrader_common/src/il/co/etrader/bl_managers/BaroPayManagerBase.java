//package il.co.etrader.bl_managers;
//
//import java.sql.Connection;
//import java.sql.SQLException;
//
//import com.anyoption.common.beans.base.BaroPayRequest;
//import com.anyoption.common.beans.base.BaroPayResponse;
//import com.anyoption.common.daos.BaroPayDAOBase;
//import com.anyoption.common.managers.BaseBLManager;
//
///**
// * @author eyalo
// *
// */
//public class BaroPayManagerBase extends BaseBLManager {
//
//	public static void insertBaroPayResponse(BaroPayResponse vo) throws SQLException {
//        Connection con = getConnection();
//        try {
//           BaroPayDAOBase.insertBaroPayResponse(con, vo);
//        } finally {
//           closeConnection(con);
//        }
//	}
// 
//	public static void insertBaroPayRequest(BaroPayRequest vo) throws SQLException {
//        Connection con = getConnection();
//        try {
//        	BaroPayDAOBase.insertBaroPayRequest(con, vo);  
//        } finally {
//        	closeConnection(con);
//        }		
//	}
//	
//	public static BaroPayRequest getBaroPayRequest(long  transactionId) throws SQLException {
//        Connection con = getConnection();
//        try {
//        	return BaroPayDAOBase.getBaroPayRequest(con, transactionId); 
//        } finally {
//           closeConnection(con); 
//        }	
//	}
//	
//	public static BaroPayResponse getBaroPayResponse(long  transactionId) throws SQLException {
//        Connection con = getConnection();
//        try {
//        	return BaroPayDAOBase.getBaroPayResponse(con, transactionId); 
//        } finally {
//           closeConnection(con);
//        }		
//	}
//	
//
//	public static boolean isHaveBaroPaySuccessDeposit(long userId) throws SQLException {
//	    Connection con = getConnection();
//	    try {
//	    	return BaroPayDAOBase.isHaveBaroPaySuccessDeposit(con, userId); 
//	    } finally {
//	       closeConnection(con);
//	    }
//	}
//
//	
//}