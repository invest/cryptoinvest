package il.co.etrader.bl_managers;
import il.co.etrader.bl_vos.AssetIndex;
import il.co.etrader.dao_managers.AssetIndexDAO;
import il.co.etrader.util.CommonUtil;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.AssetIndexsTempData;
import com.anyoption.common.beans.base.AssetIndexInfo;
import com.anyoption.common.beans.base.AssetIndexLevelCalc;
import com.anyoption.common.daos.AssetIndexMarketDAO;
import com.anyoption.common.managers.BaseBLManager;
import com.anyoption.common.managers.LanguagesManagerBase;

public class AssetIndexManagerBase extends BaseBLManager {
	private static final Logger log = Logger.getLogger(LanguagesManagerBase.class);

	public static HashMap<Long,AssetIndex> getAll(long skinId) throws SQLException {

		Connection con = getConnection();
		try {
			return AssetIndexDAO.getAll(con, skinId);
		} finally {
			closeConnection(con);
		}
	}
	
	public static void initAssetIndexsMarketBySkinTempData(int skinId, List<AssetIndexsTempData> assetIndexsTempData, String languageCode, Connection conn) throws SQLException {
		
		log.debug("Begin init initAssetIndexsMarketBySkinTempData for SkinID:" + skinId);
		List<AssetIndexInfo> assetIndexInfoList = new ArrayList<AssetIndexInfo>();
		Locale locale = null;
		if (null != languageCode) {
			locale = new Locale(languageCode);
		}

			 for (AssetIndexsTempData temp : assetIndexsTempData) {				 
				 
				 AssetIndexInfo	assetIndexInfo = new AssetIndexInfo();
				 List<AssetIndexLevelCalc> assetIndexLevelCalcList = new ArrayList<AssetIndexLevelCalc>();
				 
				 assetIndexInfo.setMarketId(temp.getMarketId());
				 assetIndexInfo.setSkinId(skinId);
				 assetIndexInfo.setMarketDescription(CommonUtil.getBackupMessage(temp.getMarketDescription(), null, locale));
				 assetIndexInfo.setAdditionalText(CommonUtil.getBackupMessage(temp.getAdditionalText(), null, locale));
				 
				 getAssetIndexLelvelCalcTemp(skinId, locale, temp, assetIndexLevelCalcList);
				 assetIndexInfo.setAssetIndexLevelCalc(assetIndexLevelCalcList);
				 
				 assetIndexInfoList.add(assetIndexInfo);
		}
			 
			 AssetIndexMarketDAO.insertAssetIndexsMarketInfoTemp(conn, assetIndexInfoList);
	}

	private static void getAssetIndexLelvelCalcTemp(int skinId, Locale locale, AssetIndexsTempData temp, List<AssetIndexLevelCalc> assetIndexLevelCalcList) {
		for (int i = 1; i <= 2; i++){
			 AssetIndexLevelCalc assetIndexLevelCalc = new AssetIndexLevelCalc();
			 
			 //Hourly
			 if(temp.isHourly() && i == 1){
				 assetIndexLevelCalc.setMarketId(temp.getMarketId());
				 assetIndexLevelCalc.setSkinId(skinId);
				 assetIndexLevelCalc.setExpiryTypeText(CommonUtil.getBackupMessage("assetIndex.expTypeHourly", null, locale));
				 
				 String reutersFieldTxt;
				 if(temp.getReutersFieald() != null){
					 reutersFieldTxt = CommonUtil.getBackupMessage(temp.getReutersFieald(), null, locale);
				 } else {
					 reutersFieldTxt = temp.getMarketFeedName();
				 }						 
				 if(temp.isLastValueHourly()){
					 String lastValue = " " + CommonUtil.getBackupMessage("assetIndex.lastValue", null, locale);
					 reutersFieldTxt = reutersFieldTxt + lastValue;
				 }
				 
				 if(temp.isClosingLevelHourly()){
					 String closingLevel = " " + CommonUtil.getBackupMessage("assetIndex.closingLevel", null, locale);
					 reutersFieldTxt = reutersFieldTxt + closingLevel;
				 }						 
				 assetIndexLevelCalc.setReutersFieldText(reutersFieldTxt);
				 assetIndexLevelCalc.setExpiryFormulaText(CommonUtil.getBackupMessage(temp.getExpFormulaFirst(), null, locale));						 
				 
				 assetIndexLevelCalcList.add(assetIndexLevelCalc);
			 }
			 
			 //Others
			 if( (temp.isEndOfDay() || temp.isEndOfWeek() || temp.isEndOfMonth()) && i == 2){
				 assetIndexLevelCalc.setMarketId(temp.getMarketId());
				 assetIndexLevelCalc.setSkinId(skinId);
				 
				 String expiryTypeText = "";
				 if(temp.isEndOfDay()){					 
					 expiryTypeText = CommonUtil.getBackupMessage("assetIndex.expTypeEndOf", null, locale) + " " 
							 					+ CommonUtil.getBackupMessage("assetIndex.expTypeDay", null, locale);
				 } 
				 if (temp.isEndOfWeek()){
					 expiryTypeText = expiryTypeText + ", " + CommonUtil.getBackupMessage("assetIndex.expTypeWeek", null, locale);
				 } 
				 if (temp.isEndOfMonth()){
					 expiryTypeText = expiryTypeText + ", " + CommonUtil.getBackupMessage("assetIndex.expTypeMonth", null, locale);
				 }
				 assetIndexLevelCalc.setExpiryTypeText(expiryTypeText);
				 
				 String reutersFieldTxt;
				 if(temp.getReutersFieald() != null){
					 reutersFieldTxt = CommonUtil.getBackupMessage(temp.getReutersFieald(), null, locale);
				 } else {
					 reutersFieldTxt = temp.getMarketFeedName();
				 }						 
				 if(temp.isLastValueEndOfDay()){
					 String lastValue = " " + CommonUtil.getBackupMessage("assetIndex.lastValue", null, locale);
					 reutersFieldTxt = reutersFieldTxt + lastValue;
				 }
				 
				 if(temp.isClosingLevelEndOfDay()){
					 String closingLevel = " " + CommonUtil.getBackupMessage("assetIndex.closingLevel", null, locale);
					 reutersFieldTxt = reutersFieldTxt + closingLevel;
				 }						 
				 assetIndexLevelCalc.setReutersFieldText(reutersFieldTxt);
				 
				 String expFormula = "";
				 if(!temp.isHourly()){
					 expFormula =  CommonUtil.getBackupMessage(temp.getExpFormulaFirst(), null, locale);
				 } else if(temp.getExpFormulaSecond() != null){
					 expFormula = CommonUtil.getBackupMessage(temp.getExpFormulaSecond(), null, locale);
				 } else {
					 expFormula =  CommonUtil.getBackupMessage(temp.getExpFormulaFirst(), null, locale);
				 }
				 assetIndexLevelCalc.setExpiryFormulaText(expFormula);						 
				 
				 assetIndexLevelCalcList.add(assetIndexLevelCalc);
			 }
		 }
	}

	public static AssetIndex getAssetIndexByMarketId(long marketId) throws SQLException {
		AssetIndex assetIndex = null;
		Connection con = null;
		try {
			con = getConnection();
			assetIndex = AssetIndexDAO.getAssetIndexByMarketId(con, marketId);
		} finally {
			closeConnection(con);
		}
		return assetIndex;
	}
	
	public static void updateAssetIndexByMarketId(AssetIndex assetIndex, long marketId) throws SQLException {
		Connection con = null;
		try {
			con = getConnection();
			AssetIndexDAO.updateAssetIndexByMarketId(con, assetIndex, marketId);
		} finally {
			closeConnection(con);
		}
	}
}