package il.co.etrader.bl_managers;

import il.co.etrader.bl_vos.UserBase;
import il.co.etrader.dao_managers.JobsDAOBase;
import il.co.etrader.jobs.AutomaticMailTemplateMaintenanceFeeBean;

import java.sql.Connection;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Iterator;
import java.util.List;

public class JobsManagerBase extends com.anyoption.common.managers.JobsManagerBase {
    /**
     * List the users to receive maintenance fee email.
     * 
     * @return List of <code>UserBase</code> to send the email to. NOTE: The maintenance fee collect date is placed in the <code>comment</code> property.
     * @throws SQLException
     */
    public static List<UserBase> getUsersListForMaintenanceFee(int collectFeeInDays, String view) throws SQLException {
        Connection conn = null;
        try {
            conn = getConnection();
            List<UserBase> ul = JobsDAOBase.getUsersListForMaintenanceFee(conn, view);
            Calendar c = Calendar.getInstance();
            c.add(Calendar.DAY_OF_YEAR, collectFeeInDays);
            String feeTime = new SimpleDateFormat("dd.MM.yy").format(c.getTime());
            for (Iterator<UserBase> i = ul.iterator(); i.hasNext();) {
                AutomaticMailTemplateMaintenanceFeeBean u = (AutomaticMailTemplateMaintenanceFeeBean) i.next();
                u.setFeeCollectTime(feeTime);
            }
            return ul;
        } finally {
            closeConnection(conn);
        }
    }

    public static void collectMaintenanceFee() throws SQLException {
        Connection conn = null;
        try {
            conn = getConnection();
            JobsDAOBase.collectMaintenanceFee(conn);
        } finally {
            closeConnection(conn);
        }
    }
}