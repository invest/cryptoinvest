//package il.co.etrader.bl_managers;
//
//import java.sql.Connection;
//import java.sql.SQLException;
//import java.util.HashMap;
//import java.util.Hashtable;
//import java.util.Map;
//import java.util.concurrent.ConcurrentHashMap;
//
//import org.apache.log4j.Logger;
//
//import com.anyoption.common.beans.base.Currency;
//import com.anyoption.common.daos.CurrenciesDAOBase;
//
//import il.co.etrader.util.ConstantsBase;
//
//public class CurrenciesManagerBase extends com.anyoption.common.managers.CurrenciesManagerBase {
//	private static final Logger log = Logger.getLogger(CurrenciesManagerBase.class);
//	
//	private static final String AMOUNT_FORMAT_PREFIX = "###,###,##0";
//	private static final String INPUT_AMOUNT_FORMAT_PREFIX = "###########0";
//	private static Map<Integer, String> decPointToAmountFormat = new ConcurrentHashMap<Integer, String>(2);
//	private static Map<Integer, String> decPointToInputAmountFormat = new ConcurrentHashMap<Integer, String>(2);
//	private static HashMap<Long, Currency> currencies;
//
//    public static HashMap<Long, Currency> getAllCurrencies() throws SQLException {
//    	loadCurrenciesIfEmpty();
//    	return currencies;
//    }
//    
//    public static String getAmountFormat(Currency currency, boolean isForInput) {
//    	int decPointDigits = 2;
//    	if (null != currency) {
//    	    decPointDigits = currency.getDecimalPointDigits();
//    	} else {
//    	    try {
//    	        decPointDigits = getCurrency(ConstantsBase.CURRENCY_BASE_ID).getDecimalPointDigits();
//    	    } catch (Exception e) {
//    	        log.error("Can't get decimal points to format amount.", e);
//    	    }
//    	}
//    	Map<Integer, String> formatMap = (isForInput ? decPointToInputAmountFormat : decPointToAmountFormat);
//    	String format = formatMap.get(decPointDigits);
//    	if (format == null) {
//    		format = (isForInput ? INPUT_AMOUNT_FORMAT_PREFIX : AMOUNT_FORMAT_PREFIX);
//    		if (decPointDigits > 0) {
//    			StringBuilder sb = new StringBuilder(format);
//    			sb.append(".")
//    				.append(String.format(String.format("%%0%dd", decPointDigits), 0));
//    			format = sb.toString();
//    		}
//    		
//    		if (decPointDigits < 0) {
//    			log.warn("Digits after the decimapl point are [" + decPointDigits
//    						+ "] - will use the default format [" + format + "]");
//    		} else {
//    			formatMap.put(decPointDigits, format);
//    		}
//    	}
//    	return format;
//    }
//
//    private static void loadCurrenciesIfEmpty() {
//		 if (null == currencies) {
//			Connection conn = null;
//			try {
//			    conn = getConnection();
//			    currencies = CurrenciesDAOBase.getAllCurrencies(conn);
//			} catch (SQLException sqle) {
//				log.error("Can't load currencies.", sqle);
//			} finally {
//			    closeConnection(conn);
//			}
//	    }
//	}
//
//	public static Currency getCurrencyByCode(String currencyCode) throws SQLException {
//		loadCurrenciesIfEmpty();
//		for (Currency currency : currencies.values()) {
//			if (currency.getCode().equalsIgnoreCase(currencyCode)) {
//				return currency;
//			}
//		}
//		return null;
//	}
//
//	public static Currency getCurrency(long currencyId) {
//		loadCurrenciesIfEmpty();
//	    return currencies.get(currencyId);
//	}
//}