
package il.co.etrader.bl_managers;

import il.co.etrader.bl_vos.CustomerReportInvestments;
import il.co.etrader.bl_vos.CustomerReportProfitData;
import il.co.etrader.bl_vos.CustomerReportSent;
import il.co.etrader.bl_vos.CustomerReportState;
import il.co.etrader.bl_vos.CustomerReportTransactions;
import il.co.etrader.dao_managers.CustomerReportDAOBase;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.Opportunity;
import com.anyoption.common.managers.BaseBLManager;

public class CustomerReportManager extends BaseBLManager {
	private static final Logger log = Logger.getLogger(CustomerReportManager.class);
	private static final String BLANK = "";

	
	public static HashMap<Long, HashMap<Boolean, HashMap<Long, ArrayList<CustomerReportInvestments>>>> getCustomerInvestmentsReportData(long reportId, long fromUserIdLimit, long lastUserIdLimit) throws SQLException {
		Connection conn = null;
		//<userId, <isSettled<oppType, Data>>>
		HashMap<Long, HashMap<Boolean, HashMap<Long, ArrayList<CustomerReportInvestments>>>> hm = new HashMap<Long, HashMap<Boolean,HashMap<Long,ArrayList<CustomerReportInvestments>>>>();		

		try {			
			conn = getConnection();			
			 ArrayList<CustomerReportInvestments> list = CustomerReportDAOBase.getAllInvestments(conn, reportId, fromUserIdLimit, lastUserIdLimit);	
			 
			 for(CustomerReportInvestments cr : list){
				 //Init the map
				 if(hm.get(cr.getUserId()) == null){
					 HashMap<Boolean, HashMap<Long, ArrayList<CustomerReportInvestments>>> hmB = new HashMap<Boolean, HashMap<Long,ArrayList<CustomerReportInvestments>>>(); 
					 hm.put(cr.getUserId(), hmB);
				 }
				 
				 if(hm.get(cr.getUserId()).get(cr.isSettled()) == null){
					 HashMap<Long, ArrayList<CustomerReportInvestments>> hmS = new HashMap<Long, ArrayList<CustomerReportInvestments>>();
					 hm.get(cr.getUserId()).put(cr.isSettled(), hmS); 
				 }
				 
				 long opportunityTypeId = -1;
				 if(cr.getOpportunityType() ==  Opportunity.TYPE_BINARY_0_100_BELOW){
					 opportunityTypeId = Opportunity.TYPE_BINARY_0_100_ABOVE;
				 } else {
					 opportunityTypeId = cr.getOpportunityType();
				 }
				 
				 if(hm.get(cr.getUserId()).get(cr.isSettled()).get(opportunityTypeId) == null){
					 ArrayList<CustomerReportInvestments> arrList = new ArrayList<CustomerReportInvestments>();
					 hm.get(cr.getUserId()).get(cr.isSettled()).put(opportunityTypeId, arrList);
				 }
				 
				 //Add list
				 hm.get(cr.getUserId()).get(cr.isSettled()).get(opportunityTypeId).add(cr);				 				 
			 }
			 
		} finally {
			closeConnection(conn);
		}
		for (Map.Entry<Long, HashMap<Boolean, HashMap<Long, ArrayList<CustomerReportInvestments>>>> entry : hm.entrySet()) {
		    log.debug("Key = " + entry.getKey() + ", Value = " + entry.getValue());
		}
		return hm;
	}
	
	public static HashMap<Long, ArrayList<CustomerReportTransactions>> getCustomerTransactionsReportData(long reportId, long fromUserIdLimit, long lastUserIdLimit, boolean isMonthly) throws SQLException {
		Connection conn = null;
		//<userId, list<data>>		
		HashMap<Long, ArrayList<CustomerReportTransactions>> hm = new HashMap<Long, ArrayList<CustomerReportTransactions>>();
		if(isMonthly){
			try {			
				conn = getConnection();			
				 ArrayList<CustomerReportTransactions> list = CustomerReportDAOBase.getAllTransactions(conn, reportId, fromUserIdLimit, lastUserIdLimit);	
				 
				 for(CustomerReportTransactions cr : list){
					 //Init the map
					 if(hm.get(cr.getUserId()) == null){
						 ArrayList<CustomerReportTransactions> dataList = new ArrayList<CustomerReportTransactions>(); 
						 hm.put(cr.getUserId(), dataList);
					 }				 			
					 //Add list
					 hm.get(cr.getUserId()).add(cr);
				 }
				 
			} finally {
				closeConnection(conn);
			}
			for (Map.Entry<Long, ArrayList<CustomerReportTransactions>> entry : hm.entrySet()) {
			    log.debug("Key = " + entry.getKey() + ", Value = " + entry.getValue());
			}
		}
		return hm;
	}
	
    public static ArrayList<Long> getAllUsersSorted(long reportId) throws SQLException{
    	Connection con = null;
    	try {
    		con = getConnection();
    		return CustomerReportDAOBase.getAllUsersSorted(con, reportId);
    	}finally {
    		closeConnection(con);
    	}
    }
	
    public static ArrayList<CustomerReportState> getLastReportState() throws SQLException{
    	Connection con = null;
    	try {
    		con = getConnection();
    		return CustomerReportDAOBase.getLastReportState(con);
    	}finally {
    		closeConnection(con);
    	}
    }
	
	
    public static void updateCustomerReportsIsSendErrNotification(long reportId) throws SQLException {
    	
    	Connection conn = null;
        try {
        	conn = getConnection();
        	CustomerReportDAOBase.updateCustomerReportsIsSendErrNotification(conn, reportId);           
        } finally {
            closeConnection(conn);
        }
    }
    
    public static void updateCustomerReportsIsSendReportsFinish(long reportId) throws SQLException {
    	
    	Connection conn = null;
        try {
        	conn = getConnection();
        	CustomerReportDAOBase.updateCustomerReportsIsSendReportsFinish(conn, reportId);           
        } finally {
            closeConnection(conn);
        }
    }
    
    public static void updateCustomerReportsJob(long lastRunTimeDays, long runInterval, long jobId) throws SQLException {
    	
    	Connection conn = null;
        try {
        	conn = getConnection();
        	CustomerReportDAOBase.updateCustomerReportsJob(conn, lastRunTimeDays, runInterval, jobId);           
        } finally {
            closeConnection(conn);
        }
    }
    
    public static void insertCustomerReportSent(long reportId, long userId, long isSent, String sentMsg)  throws SQLException {
    	
    	Connection conn = null;
        try {
        	conn = getConnection();
        	CustomerReportDAOBase.insertCustomerReportSent(conn, reportId,userId, isSent, sentMsg); 
        	;
        } finally {
            closeConnection(conn);
        }
    }
    
    public static  ArrayList<CustomerReportSent> getCustomerReportSendAll(long userId, long reportType, Date fromDate, Date toDate, long id, long isResent) throws SQLException{
    	Connection con = null;
    	try {
    		con = getConnection();
    		return CustomerReportDAOBase.getCustomerReportSendAll(con, userId, reportType, fromDate, toDate, id, isResent);
    	}finally {
    		closeConnection(con);
    	}
    }
    
    public static void updateCustomerReportReSent(long id, String comments) throws SQLException {
    	
    	Connection conn = null;
        try {
        	conn = getConnection();
        	CustomerReportDAOBase.updateCustomerReportReSent(conn, id, comments);           
        } finally {
            closeConnection(conn);
        }
    }
    
    public static  HashMap<Long, CustomerReportProfitData> getProfitData(long reportId, long fromUserIdLimit, long lastUserIdLimit, boolean isMonthly) throws SQLException{    	
    	HashMap<Long, CustomerReportProfitData> hm = new HashMap<Long, CustomerReportProfitData>();
    	if(isMonthly){
        	Connection con = null;
        	try {
        		con = getConnection();
        		hm = CustomerReportDAOBase.getProfitData(con, reportId, fromUserIdLimit, lastUserIdLimit);
        	}finally {
        		closeConnection(con);
        	}
    	}    	
    	return hm;
    }
    
	public static boolean isUserCloseByIssue(long userId) throws SQLException {
		Connection con = null;
		try {
			con = getConnection();
			return CustomerReportDAOBase.isUserCloseByIssue(con, userId);
		} finally {
			closeConnection(con);
		}
	}
    
    
}
