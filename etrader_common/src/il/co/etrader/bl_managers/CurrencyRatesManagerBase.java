///**
// * 
// */
//package il.co.etrader.bl_managers;
//
//
//import java.sql.Connection;
//import java.sql.SQLException;
//import java.util.Calendar;
//import java.util.Date;
//import java.util.HashMap;
//import java.util.Map;
//import java.util.TimeZone;
//import java.util.concurrent.Executors;
//import java.util.concurrent.ScheduledExecutorService;
//import java.util.concurrent.ThreadFactory;
//import java.util.concurrent.TimeUnit;
//
//import org.apache.log4j.Logger;
//
//import com.anyoption.common.beans.base.Currency;
//import com.anyoption.common.daos.CurrencyRatesDAO;
//import com.anyoption.common.managers.BaseBLManager;
//
//import il.co.etrader.util.ConstantsBase;
//
///**
// * @author pavelhe
// *
// */
//public class CurrencyRatesManagerBase extends BaseBLManager {
//	private static final Logger log = Logger.getLogger(CurrencyRatesManagerBase.class);
//	
//	private static final long RELOAD_RETRY_INTERVAL_MILLIS = 3 * 1000L;
//	
//	private static Map<Long, Double> investmentsRatesCache = new HashMap<Long, Double>();
//	
//	private static ScheduledExecutorService executor;
//	
//	static {
//		executor = Executors.newSingleThreadScheduledExecutor(new ThreadFactory() {
//			
//			@Override
//			public Thread newThread(Runnable r) {
//				Thread thread = new Thread(r);
//				thread.setName("INV-CURRENCY-RATES-CACHE");
//				return thread;
//			}
//		});
//	}
//	
//	/**
//	 * This method returns the investments rate for given currency, that is stored in the investments rates cache
//	 * 
//	 * @param currencyId - the currency id
//	 * @return the rate as double value. If rate is 0, then rate get was unsuccessful.
//	 * @throws SQLException 
//	 */
//	public static double getInvestmentsRate(long currencyId) {
//		return investmentsRatesCache.get(currencyId);
//	}
//	
//	/**
//	 * This method initializes investments currency rates cache and loads the currency rates.
//	 */
//	public static void loadInvestmentRartesCache() {
//		loadInvestmentRartesCache(new Date());
//	}
//	
//	private static void loadInvestmentRartesCache(Date rateDate) {
//		boolean reloadSuccess = false;
//		do {
//			log.info("Loading investments currency rates cache for date [" + rateDate + "]...");
//			try {
//				Map<Long, Currency> currencies = CurrenciesManagerBase.getAllCurrencies();
//				Map<Long, Double> cache = new HashMap<Long, Double>();
//				for (Long currencyId : currencies.keySet()) {
//					cache.put(currencyId, getInvestmentsRateFromDB(currencyId, rateDate));
//				}
//				investmentsRatesCache = cache;
//				reloadSuccess = true;
//			} catch (Exception e) {
//				log.error("Unable to reload currencies cache. Waiting for [" + RELOAD_RETRY_INTERVAL_MILLIS
//						+ "] milliseconds until next retry", e);
//				try {
//					Thread.sleep(RELOAD_RETRY_INTERVAL_MILLIS);
//				} catch (InterruptedException e1) {
//					log.warn("Investments rate cache initialization thread interrupted", e1);
//				}
//			}
//		} while (!reloadSuccess);
//		log.info("Loading investments currency rates cache successful [" + investmentsRatesCache + "]");
//		
//		scheduleCacheReload();
//	}
//	
//	private static void scheduleCacheReload() {
//		// Cache reload should happen at GMT+00:00 (UTC) midnight.
//		Calendar c = Calendar.getInstance(TimeZone.getTimeZone("GMT+00:00"));
//		c.add(Calendar.DAY_OF_MONTH, 1);
//		c.set(Calendar.HOUR_OF_DAY, 0);
//		c.set(Calendar.MINUTE, 0);
//		c.set(Calendar.SECOND, 0);
//		c.set(Calendar.MILLISECOND, 0);
//		long millisTillMidnight = (c.getTimeInMillis() - System.currentTimeMillis());
//		
//		executor.schedule(new Runnable() {
//			
//								@Override
//								public void run() {
//									try {
//										Calendar c = Calendar.getInstance();
//										/*
//										 * Make sure clock difference between server
//										 * and DB won't break rate cache logic.
//										 */
//										c.add(Calendar.HOUR_OF_DAY, 1);
//										loadInvestmentRartesCache(c.getTime());
//									} catch(Exception e) {
//										log.error("Exception occurrred while running rates cache reload task", e);
//									}
//								}
//							},
//							millisTillMidnight,
//							TimeUnit.MILLISECONDS);
//		log.debug("Investments rates cache scheduled for reload after [" + millisTillMidnight + "] milliseconds");
//	}
//	
//	private static double getInvestmentsRateFromDB(long currencyId, Date rateDate) throws SQLException {
//		double rawRate = 0.0D;
//		rawRate = CurrencyRatesManagerBase.convertToBaseAmount(1L, currencyId, rateDate);
//		double precision = Math.pow(10, ConstantsBase.RATE_PRECISISON);
//		return new Double(Math.round(rawRate * precision) / precision);
//	}
//	
//	public static Double convertToBaseAmount(long amount, long currencyId, Date date) throws SQLException {
//		Connection con = null;
//		try {
//			con = getConnection();
//			return CurrencyRatesDAO.convertToBaseAmount(con, amount, currencyId, date);
//		} finally {
//			closeConnection(con);
//		}
//	}
//
//	public static Double convertToEuroAmount(long amount, long currencyId, Date date) throws SQLException {
//		Connection con = null;
//		try {
//			con = getConnection();
//			return CurrencyRatesDAO.convertToEuroAmount(con, amount, currencyId, date);
//		} finally {
//			closeConnection(con);
//		}
//	}
//
//}
