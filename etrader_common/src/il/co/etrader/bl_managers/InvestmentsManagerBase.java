package il.co.etrader.bl_managers;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;

import javax.faces.context.FacesContext;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import com.anyoption.common.beans.Investment;
import com.anyoption.common.beans.InvestmentLimitsGroup;
import com.anyoption.common.beans.Opportunity;
import com.anyoption.common.beans.OpportunityType;
import com.anyoption.common.beans.TierUser;
import com.anyoption.common.bl_vos.OptionPlusQuote;
import com.anyoption.common.daos.GeneralDAO;
import com.anyoption.common.enums.CopyOpInvTypeEnum;
import com.anyoption.common.jms.LevelsCache;
import com.anyoption.common.jms.WebLevelsCache;
import com.anyoption.common.util.BonusFormulaDetails;

import il.co.etrader.bl_vos.CopyopInvestmentDetails;
import il.co.etrader.bl_vos.TierUserHistory;
import il.co.etrader.bl_vos.UserBase;
import il.co.etrader.dao_managers.BonusDAOBase;
import il.co.etrader.dao_managers.InvestmentTypesDAO;
import il.co.etrader.dao_managers.InvestmentsDAOBase;
import il.co.etrader.dao_managers.OpportunitiesDAOBase;
import il.co.etrader.dao_managers.TaxHistoryDAO;
import il.co.etrader.dao_managers.TiersDAOBase;
import il.co.etrader.dao_managers.UsersDAOBase;
import il.co.etrader.util.CommonUtil;
import il.co.etrader.util.ConstantsBase;

/**
 * Investment page manager.
 *
 * @author Tony
 */
public abstract class InvestmentsManagerBase extends com.anyoption.common.managers.InvestmentsManagerBase {
    private static final Logger log = Logger.getLogger(InvestmentsManagerBase.class);

	public static final int STATUS_CANCELED = 100;
	public static final int STATUS_SETTLED = 101;
	public static final int STATUS_VOID = 102;
	public static final int STATUS_NOT_SETTLED = 104;

	public static final int SCHEDULED_HOURLY = 1;
	public static final int SCHEDULED_DAILY = 2;
	public static final int SCHEDULED_WEEKLY = 3;
	public static final int SCHEDULED_MONTHLY = 4;
	public static final int SCHEDULED_ONE_TOUCH = 5;
	public static final int SCHEDULED_ALL = 0;

//	public static final int INVESTMENT_TYPE_CALL = 1;
//	public static final int INVESTMENT_TYPE_PUT = 2;
//	public static final int INVESTMENT_TYPE_ONE = 3;
//	public static final int INVESTMENT_TYPE_BUY = 4;
//	public static final int INVESTMENT_TYPE_SELL = 5;
	public static final int INVESTMENT_TYPE_BUBBLES = 6;

	public static final int INVESTMENT_ONE_TOUCH_WIN = 1;

	public static final long INVESTMENT_ONE_TOUCH_UP = 1;
	public static final long INVESTMENT_ONE_TOUCH_DOWN = 0;

	public static final long INVESTMENT_TYPE_ONE_DECIMAL = 2;
	
	public static final int SUSPENDED_TRUE 	= 1;
	public static final int SUSPENDED_FALSE = 0;
	public static final int SUSPENDED_ALL 	= 3;
	
	public static String getScheduledTxt(long scheduled) {
    	if (scheduled == InvestmentsManagerBase.SCHEDULED_HOURLY)
    		return CommonUtil.getMessage("opportunities.templates.hourly", null);
    	if (scheduled == InvestmentsManagerBase.SCHEDULED_DAILY)
    		return CommonUtil.getMessage("opportunities.templates.daily", null);
    	if (scheduled == InvestmentsManagerBase.SCHEDULED_MONTHLY)
    		return CommonUtil.getMessage("opportunities.templates.monthly", null);
    	if (scheduled == InvestmentsManagerBase.SCHEDULED_WEEKLY)
    		return CommonUtil.getMessage("opportunities.templates.weekly", null);
    	if (scheduled == InvestmentsManagerBase.SCHEDULED_ONE_TOUCH) {
    		return CommonUtil.getMessage("opportunities.templates.oneTouch", null);
    	}
    	if (scheduled == Opportunity.SCHEDULED_QUARTERLY) {
    		return CommonUtil.getMessage("opportunities.templates.quarterly", null);
    	}
    	if (scheduled == InvestmentsManagerBase.SCHEDULED_ALL) {
    		return CommonUtil.getMessage("opportunities.templates.all", null);
    	}
    	return "Illegal scheduled code!!!";
    }

    public static String getSuspendedTxt(int suspended) {
	switch (suspended) {
	case InvestmentsManagerBase.SUSPENDED_FALSE:
	    return CommonUtil.getMessage("opportunities.suspended.false", null);
	case InvestmentsManagerBase.SUSPENDED_TRUE:
	    return CommonUtil.getMessage("opportunities.suspended.true", null);
	default:
	    // case InvestmentsManagerBase.SUSPENDED_ALL:
	    return CommonUtil.getMessage("opportunities.suspended.all", null);
	}
    }

    public static com.anyoption.common.beans.Investment getInvestmentDetails(long id) throws SQLException {

        Connection conn = null;
        try {
            conn = getConnection();
            return InvestmentsDAOBase.getDetails(conn,id);
        } finally {
            closeConnection(conn);
        }

    }

    /**
     * Settle investment.
     *
     * @param investmentId the id of the investment to settle.
     * @param writerId
     * @throws SQLException
     */
    public static void settleInvestment(long investmentId, long writerId, long insuranceAmount, com.anyoption.common.beans.Investment inv, long optionPlusePrice, long loginId) throws SQLException {
        Connection conn = null;
        try {
            conn = getConnection();
            settleInvestment(conn, investmentId, writerId, insuranceAmount, inv, optionPlusePrice, ConstantsBase.LOG_BALANCE_SETTLE_INVESTMENT, loginId);
        } finally {
            closeConnection(conn);
        }
    }

//    /**
//     * settle investment
//     *
//     * @param conn
//     * @param investmentId
//     * @param writerId
//     * @param inv the investment to settle or null
//     * @param balanceLogCmd
//     * @param insuranceAmount the amount of the gm insurance, 0 if it wasnt bought
//     * @throws SQLException
//     */
//    public static void settleInvestment(Connection conn, long investmentId, long writerId, long GMInsuranceAmount, com.anyoption.common.beans.Investment inv,
//    						OracleConnection oracleCon, long optionPlusePrice, int balanceLogCmd, long loginId) throws SQLException {
//        com.anyoption.common.beans.Investment investment = inv;
//        long result;
//        long netResult;
//        boolean win = false;
//        boolean voidBet = false;
//        if (GMInsuranceAmount == 0 && optionPlusePrice == ConstantsBase.REGULAR_SETTLEMNT) { // if its not golden minutes settlement
//            investment = InvestmentsDAOBase.getInvestmentToSettle(conn, investmentId, true, false, null);
//            boolean callWin = false;
//            boolean putWin = false;
//            if (investment.getOpportunityTypeId() == Opportunity.TYPE_REGULAR || investment.getOpportunityTypeId() == Opportunity.TYPE_OPTION_PLUS) {
//            	callWin = investment.getTypeId() == INVESTMENT_TYPE_CALL && investment.getClosingLevel() > investment.getCurrentLevel();
//            	putWin = investment.getTypeId() == INVESTMENT_TYPE_PUT && investment.getClosingLevel() < investment.getCurrentLevel();
//            } else if (investment.getOpportunityTypeId() == Opportunity.TYPE_BINARY_0_100_ABOVE) {
//            	callWin = investment.getTypeId() == INVESTMENT_TYPE_BUY && investment.getClosingLevel() > investment.getOppCurrentLevel();
//            	putWin = investment.getTypeId() == INVESTMENT_TYPE_SELL && investment.getClosingLevel() <= investment.getOppCurrentLevel();
//            } else if (investment.getOpportunityTypeId() == Opportunity.TYPE_BINARY_0_100_BELOW) {
//            	putWin = investment.getTypeId() == INVESTMENT_TYPE_SELL && investment.getClosingLevel() >= investment.getOppCurrentLevel();
//            	callWin = investment.getTypeId() == INVESTMENT_TYPE_BUY && investment.getClosingLevel() < investment.getOppCurrentLevel();
//            }
//            boolean oneWin = false;
//            boolean isOneTouch = investment.getTypeId() == INVESTMENT_TYPE_ONE;
//            if (isOneTouch) {
//            	long upDown = InvestmentsDAOBase.getOneTouchInvestmentUpDown(conn, investmentId);
//            	if (upDown == INVESTMENT_ONE_TOUCH_UP) {
//            		oneWin = investment.getClosingLevel() >= investment.getCurrentLevel();
//            	} else if (upDown == INVESTMENT_ONE_TOUCH_DOWN) {
//            		oneWin = investment.getClosingLevel() <= investment.getCurrentLevel();
//            	} else if (upDown == -1) {
//            		if (log.isEnabledFor(Level.INFO)) {
//                    	log.log(Level.ERROR, "The one touch opportunity " + investment.getOpportunityId() + " related to this investment " + investment.getId() + " is invalid");
//                    }
//            	}
//            }
//
//            win = callWin || putWin || oneWin;
//
//            if (investment.getClosingLevel().doubleValue() == investment.getCurrentLevel().doubleValue() && investment.getTypeId() != INVESTMENT_TYPE_ONE && investment.getOpportunityTypeId() != Opportunity.TYPE_BINARY_0_100_ABOVE && investment.getOpportunityTypeId() != Opportunity.TYPE_BINARY_0_100_BELOW) {
//                voidBet = true;
//            }
//
//            if (log.isEnabledFor(Level.INFO)) {
//            	log.log(Level.INFO, "investment.getTypeId() = " + investment.getTypeId() + "; investment.getClosingLevel() = " + investment.getClosingLevel() + "; investment.getCurrentLevel() = " + investment.getCurrentLevel() + "; investment.isUpInvestment() = " + InvestmentsManagerBase.isUpInvestment(investment) + "; opp.current_level = " + investment.getOppCurrentLevel());
//            	log.log(Level.INFO, "putWin = " + putWin + "; callWin = " + callWin + "; isOneTouch = " + isOneTouch + "; win= " + win + "; voidBet= " + voidBet);
//            }
//        } else { //golden minutes settlement
//            if (GMInsuranceAmount > 0) {
//                win = true;
//            } else { //its option pluse settlement (only win or lose)
//                if (optionPlusePrice > investment.getAmount() - investment.getOptionPlusFee()) {
//                    win = true;
//                }
//            }
//        }
//
//        // NOTE: Here is the key moment - we take the win/lose and we expect no one else
//        // will change it before we finish the settlement of this investment.
//        // This shouldn't be a problem as far as someone don't manage to squeeze a cancel
//        // in the middle from the backend.
//
//
//        // subtract the premia amount
//        // investment.getOptionPlusFee is also 0100 fee
//        long invesAmount = investment.getAmount() - investment.getInsuranceAmountRU() - investment.getOptionPlusFee();
//        long bonusAmount = 0;
//        log.debug("settlemnt inv id: " + investment.getId() + " getBonusOddsChangeTypeId " + investment.getBonusOddsChangeTypeId() + " win " + win);
//        if (optionPlusePrice == ConstantsBase.REGULAR_SETTLEMNT) {
//            if (win) {
//            	if (investment.getOpportunityTypeId() == Opportunity.TYPE_BINARY_0_100_ABOVE || investment.getOpportunityTypeId() == Opportunity.TYPE_BINARY_0_100_BELOW) {
//            		result = 10000 * getBinary0100NumberOfContracts(investment);
//            		netResult = (10000 - invesAmount / getBinary0100NumberOfContracts(investment) * getBinary0100NumberOfContracts(investment));
//            	} else {
//	                result = Math.round(invesAmount * (1 + investment.getOddsWin()));
//	                netResult = Math.round(invesAmount * investment.getOddsWin());
//            	}
//            } else {
//            	if (investment.getOpportunityTypeId() == Opportunity.TYPE_BINARY_0_100_ABOVE || investment.getOpportunityTypeId() == Opportunity.TYPE_BINARY_0_100_BELOW) {
//            		result = 0;
//            		netResult = -invesAmount;
//            	} else {
//	                result = Math.round(invesAmount * (1 - investment.getOddsLose()));
//	                netResult = Math.round(-invesAmount * investment.getOddsLose());
//            	}
//            }
//        } else {
//            if (win) {
//                result = optionPlusePrice;
//                netResult = Math.round(optionPlusePrice - invesAmount);
//            } else {
//                result = optionPlusePrice;
//                netResult = Math.round(-1 * (invesAmount - optionPlusePrice));
//            }
//        }
//
//        long bonusTypeId = investment.getBonusOddsChangeTypeId();
//
//        if (bonusTypeId > 0){
//            BonusHandlerBase bh = BonusHandlerFactory.getInstance(bonusTypeId);
//            if (null != bh){
//            	try {
//            		InvestmentBonusData ibd = new InvestmentBonusData(investment.getOddsWin(), investment.getOddsLose(), 
//											            				investment.getBonusWinOdds(), investment.getBonusLoseOdds(),
//											            				investment.getId(), investment.getBonusUserId(),
//											            				investment.getAmount(), investment.getInsuranceAmountRU()) ;
//    				bonusAmount = bh.handleBonusOnSettleInvestment(conn, ibd, win);
//    			} catch (BonusHandlersException e) {
//    				log.error("Error in getBonusAmountOnSettleInvestment for investment " + investmentId, e);
//    			}
//            }
//        }
//
//        log.debug("settlemnt inv id: " + investment.getId() + " bonusAmount " + bonusAmount);
//
//        long winLose = 0;
//
//        UserBase u = new UserBase();
//        UsersDAOBase.getByUserId(conn, investment.getUserId(), u, true);
//
//        //tax should be calculated only for ETRADER users
//        if (CommonUtil.isHebrewSkin(u.getSkinId())) {
//	        winLose = u.getTotalWinLose();
//	        log.info("User :"+u.getId()+" win lose for the year :" +winLose);
//        }
//
//        //in case of a losing "next invest on us" investment the netResult is not added to the W\L
//        boolean isNextInvestOnUs = investment.getBonusOddsChangeTypeId() == 1 && !win;
//        if (!isNextInvestOnUs) {
//        	winLose += netResult - investment.getInsuranceAmountRU() - GMInsuranceAmount + bonusAmount - investment.getOptionPlusFee();
//        }
//
//        long tax = 0;
//        if (winLose > 0) {
////            tax = Math.round(winLose * ApplicationDataBase.getTaxPercentage());
//            // TODO: Tony: find a nice way to avoid hardcoding the tax %
//            tax = Math.round(winLose * ConstantsBase.TAX_PERCENTAGE);
//        }
//
//        if (log.isDebugEnabled()) {
//            String ls = System.getProperty("line.separator");
//            log.debug(ls + "Settle investment: " + investment.getId() + ls +
//                    " win: " + win + ls +
//                    " nextInvestOnUs: " + isNextInvestOnUs + ls +
//                    " voidBet: " + voidBet + ls +
//                    " result: " + result + ls +
//                    " netResult: " + netResult + ls +
//                    " winLose: " + winLose + ls +
//                    " tax: " + tax + ls);
//        }
//
//        investment.setUtcOffsetSettled(u.getUtcOffset());
//        long winC = 0;
//        long loseC = 0;
//        long premiaC = investment.getInsuranceAmountRU() + GMInsuranceAmount + investment.getOptionPlusFee();
//        if (netResult > 0) {  // win
//            winC = netResult;
//            loseC = premiaC;
//        } else if (netResult < 0) { // lose
//            winC = 0;
//            loseC = (-netResult) + premiaC;
//        } else {
//            winC = 0;
//            loseC = premiaC;
//        }
//        investment.setWin(winC);
//        investment.setLose(loseC);
//
//
//        boolean isOuterAutoCommit = conn.getAutoCommit();
//        try {
//        	if (isOuterAutoCommit) {
//        		conn.setAutoCommit(false);
//        	}
//            InvestmentsDAOBase.settleInvestmentUpdateUserBalances(conn, investment.getUserId(), result, tax, winLose);
//            boolean shouldUpdateTimeSettled = null != investment.getTimeSettled() ? false : true;
//            Date now = new Date();
//            InvestmentsDAOBase.settleInvestment(conn, investment.getUserId(), investmentId, result, voidBet, GMInsuranceAmount, winC, loseC, investment.getClosingLevel(), investment.getTimeQuoted(), now, shouldUpdateTimeSettled);
//            if (shouldUpdateTimeSettled && (investment.getOpportunityTypeId() == Opportunity.TYPE_BINARY_0_100_ABOVE ||
//            		investment.getOpportunityTypeId() == Opportunity.TYPE_BINARY_0_100_BELOW)) {
//            	InvestmentsDAOBase.updateTimeSettledBinary0100(conn, now, investmentId);
//            }
//            GeneralDAO.insertBalanceLog(conn, writerId, investment.getUserId(), ConstantsBase.TABLE_INVESTMENTS, investment.getId(), balanceLogCmd, u.getUtcOffset());
//            boolean bonusOddsWithTran = false;
//            //in case of a "next invest on us" losing investment or (odds change with bonus amount > 0) the user is granted with a bonus.
//            if (isNextInvestOnUs || (investment.getBonusOddsChangeTypeId() > 1 && bonusAmount > 0)) {
//            	long amount = isNextInvestOnUs ? -netResult : bonusAmount;
//                if (log.isInfoEnabled()) {
//                    log.info("granting bonus. Investment id: " + investment.getId() + " Bonus amount: " + amount);
//                 }
//            	grantBonus(conn, investment, writerId, amount, loginId);
//            	bonusOddsWithTran = true;
//            }         
//            
//            //Bonus Management System (BMS).
//            if (bonusTypeId > 0 && win && !bonusOddsWithTran) {
//            	log.debug("BMS, bonusTypeId (&& win && !bonusOddsWithTran): " + bonusTypeId);
//            	BonusDAOBase.updateAdjustedAmount(conn, investment.getBonusUserId(), investment.getWin());
//            }
//            log.debug("BMS, investment.getContractsStep():" + investment.getContractsStep());
//            log.debug("BMS, investment.getInvestmentsCount():" + investment.getInvestmentsCount());
//            ArrayList<BonusFormulaDetails> bonusFormulaD = BonusDAOBase.getBonusAmounts(conn, investmentId, false);
//            if (!bonusFormulaD.isEmpty()) {
//		        //coefficient -  multiplicative factor for the formula. 
//		        //double coefficient = investment.getOddsWin();//user win.   
//            	double coefficient = investment.getAmount() + investment.getWin() - investment.getLose();//settle end time: binary (win & lose), option+ (win & lose), onetouch (win & lose). take profit (before time).  
//		        log.debug("BMS, coefficient: " + coefficient + "=" + investment.getAmount() + "+" + investment.getWin() + "-" + investment.getLose());
//            	if (GMInsuranceAmount == 0 && optionPlusePrice == ConstantsBase.REGULAR_SETTLEMNT) {//settle end time: 0-100.
//		        	if (isBinary0100(investment)) {
//		        		coefficient = 0;	        		
//		        		if (win) {
//		        			coefficient = 10000 * getBinary0100NumberOfContracts(investment); //100 for wining * number of contracts * 100 (in cents)
//		        		} 
//		        		log.debug("BMS, settle end time 0-100. coefficient:" + coefficient);
//		        	}
//		        } else if (GMInsuranceAmount == 0) {//settle before time. not golden
//		        	coefficient = optionPlusePrice; //option+, 0100
//		        	log.debug("BMS, settle before time. not golden, coefficient:" + coefficient);
//		        }
//		        if (investment.getAmount() != 0) {
//			        for (BonusFormulaDetails item : bonusFormulaD) {
//			        	log.debug("\n\nBMS, settle investment formula \n*****************************\n" +
//			        			"" + item.getAdjustedAmount() + "+(" + (double)item.getAmount() + "/" + (double)investment.getAmount() + ")*" + coefficient + 
//			        			"=\n (double)item.getAmount() / (double)investment.getAmount() = " + (double)item.getAmount() / (double)investment.getAmount() + 
//			        			" ((double)item.getAmount() / (double)investment.getAmount()) * coefficient " + ((double)item.getAmount() / (double)investment.getAmount()) * coefficient + 
//			        			" BigDecimal.valueOf(((double)item.getAmount() / (double)investment.getAmount()) * coefficient).setScale(0, RoundingMode.HALF_UP).longValue() " + 
//			        			BigDecimal.valueOf(((double)item.getAmount() / (double)investment.getAmount()) * coefficient).setScale(0, RoundingMode.HALF_UP).longValue());
//			        	long adjustedAmount = item.getAdjustedAmount() + BigDecimal.valueOf(((double)item.getAmount() / (double)investment.getAmount()) * coefficient).setScale(0, RoundingMode.HALF_UP).longValue();
//			        	log.debug("\nBMS, adjusted_amount to be update at bonus_users: (id,adjustedAmount)" + "(" + item.getBonusUsersId() + "," + adjustedAmount + ")");
//			        	BonusDAOBase.updateAdjustedAmount(conn, item.getBonusUsersId(), adjustedAmount);
//			        	BonusDAOBase.insertBonusInvestments(conn, investmentId, item.getBonusUsersId(), 
//			        			BigDecimal.valueOf(((double)item.getAmount() / (double)investment.getAmount()) * coefficient).setScale(0, RoundingMode.HALF_UP).longValue(),
//			        			adjustedAmount, ConstantsBase.BONUS_INVESTMENTS_TYPE_SETTLED_INV);
//					}         
//		        }
//            }
//            if (isOuterAutoCommit) {
//            	conn.commit();
//            }
//            // is not manual resettlement
//            if(balanceLogCmd != ConstantsBase.LOG_BALANCE_RESETTLE_INVESTMENT) {
//        		if((CommonUtil.getProperty("copyop.isEnabled").equalsIgnoreCase("true"))) {
//		            	log.debug("About to send to copyop:"+investmentId);
//		            	if(investment.getOpportunityTypeId() == Opportunity.TYPE_REGULAR) {
//		            		if(investment.isLikeHourly()) {
//		            			if(u.getSkinId() != Skins.SKIN_ETRADER) {
//			            			CopyOpAsyncEventSender.sendEvent(new InvSettleEvent(investment.getUserId(), investmentId, result, CopyOpInvTypeEnum.of(investment.getCopyOpTypeId()), investment.getMarketId(),
//			            					winC, loseC, investment.getClosingLevel(), new Date(), investment.getCopyOpInvId(), u.getCurrencyId(), investment.getRate()));
//			            			log.debug("Send to copyop:"+investmentId);
//	            			}
//	            		}
//	            	}
//	            }
//            }
//        } catch (Exception e) {
//            try {
//            	if (isOuterAutoCommit) {
//            		conn.rollback();
//            	}
//            } catch (Exception ie) {
//                log.error("Can't rollback.", ie);
//            }
//            SQLException sqle = new SQLException();
//            sqle.initCause(e);
//            throw sqle;
//        } finally {
//            try {
//            	if (isOuterAutoCommit) {
//            		conn.setAutoCommit(true);
//            	}
//            } catch (Exception e) {
//                log.error("Can't set back to autocommit.", e);
//            }
//        }
//
//        try {
//            if (investment.getApiExternalUserId() > 0 && GMInsuranceAmount == 0 && optionPlusePrice == ConstantsBase.REGULAR_SETTLEMNT) {
//            	Notification notification = new Notification();
//            	notification.setNotification("userId:" + investment.getUserId() + ", amount:" + result + ", timeSettled:" + investment.getTimeEstClosing().getTime());
//            	notification.setReference(String.valueOf(investment.getId()));
//            	notification.setStatus(Notification.STATUS_QUEUED);
//            	long notificationType = Notification.TYPE_INSERT_INVESTMENT;
//            	if (balanceLogCmd == ConstantsBase.LOG_BALANCE_RESETTLE_INVESTMENT) {
//            		notificationType = Notification.TYPE_INSERT_INVESTMENT;
//            	}
//            	notification.setType(notificationType);
//            	notification.setExternalUserId(investment.getApiExternalUserId());
//            	NotificationManagerBase.insertNotifications(notification);
//            }
//        } catch (RuntimeException e) {
//            log.error("cant insert notification for external user id " + investment.getApiExternalUserId());
//        }
//
//
//        // add sms message to db queue if needed
//        // boolean isFreeSms = UsersDAOBase.isFreeSms(conn, investment.getUserId());
//        if (investment.isAcceptedSms()) {
//            log.info("Going to queued sms for invId: " + investment.getId() + ", userId: " + investment.getUserId());
//            Country country = CountryDAOBase.getById(conn, u.getCountryId());
//            String phoneCode = country.getPhoneCode();
//            if (null != u.getMobilePhone() && u.getMobilePhone().indexOf("0") == 0) { // leading zero (index 0)
//                u.setMobilePhone(u.getMobilePhone().substring(1));
//            }
//            //from now we will use english for AO and Hebrew for ET
//           /* Skins s = SkinsDAOBase.getById(conn, (int)u.getSkinId());
//            String langCode = LanguagesDAOBase.getCodeById(conn, s.getDefaultLanguageId());
//            if(null == langCode) { // take default
//                langCode = ConstantsBase.LOCALE_DEFAULT;
//            }*/
//            String[] params = new String[3];
//            investment.setCurrencyId(u.getCurrencyId());
//            if (GMInsuranceAmount != 0) { //GM : add premia to the amount
//                investment.setAmount(investment.getAmount() + GMInsuranceAmount);
//            }
////            long providerId = ConstantsBase.UNICELL_PROVIDER_AO;
//            long providerId =  ConstantsBase.MOBIVATE_PROVIDER;//ConstantsBase.MBLOX_PROVIDER;
//            String SenderName = "anyoption";
//            String separator = " ";
//            String langCode = ConstantsBase.LOCALE_DEFAULT;
//            if (country.isAlphaNumericSender()) {
//            	SenderName = country.getSupportPhone();
//            	SenderName = SenderName.replaceAll("[^0-9]", "");
//            	if(SenderName.length() > ConstantsBase.MAX_SIZE_SENDER_NAME){
//            		SenderName = SenderName.substring(country.getPhoneCode().length());
//            	}
//            } else if (CommonUtil.isHebrewSkin(u.getSkinId())) {
////            	providerId = ConstantsBase.UNICELL_PROVIDER_ET;
//            	SenderName = "etrader";
//            	separator = "|";
//            	langCode = ConstantsBase.ETRADER_LOCALE;
//            }
//            String senderNumber = CountryManagerBase.getById(u.getCountryId()).getSupportPhone();
//            //ResourceBundle bundle = ResourceBundle.getBundle("MessageResources", new Locale(langCode));
//            Locale locale = new Locale(langCode);
//            String refundTxt = CommonUtil.displayAmount(InvestmentsManagerBase.getRefund(investment) + (isNextInvestOnUs ? -netResult : bonusAmount), false, u.getCurrencyId(), true);
//            if (u.getCurrencyId() == ConstantsBase.CURRENCY_ILS_ID|| u.getCurrencyId() == ConstantsBase.CURRENCY_TRY_ID) {
//            	refundTxt = refundTxt + CommonUtil.getMessage(CommonUtil.getCurrencySymbolSMS(u.getCurrencyId()), params, locale);
//            } else {
//            	refundTxt = CommonUtil.getMessage(CommonUtil.getCurrencySymbolSMS(u.getCurrencyId()), params, locale) + refundTxt;
//            }
//            params[0] =  CommonUtil.getMessage(investment.getMarketName() + ".short", null, locale) + separator +
//                            CommonUtil.formatLevelByMarket(investment.getCurrentLevel(), investment.getMarketId(), investment.getDecimalPoint()) + separator +
//                            CommonUtil.getMessage(investment.getTypeName(), null, locale) + separator +
//                            CommonUtil.getTimeFormat(investment.getTimeEstClosing(), investment.getUtcOffsetCreated()) + separator;
//            params[1] = CommonUtil.formatLevelByMarket(investment.getClosingLevel(), investment.getMarketId(), investment.getDecimalPoint());
//            params[2] = refundTxt;
//            String msg = CommonUtil.getMessage("sms.settelment.message", params, locale);
//
//            try {
//                SMSManagerBase.sendTextMessage(SenderName, senderNumber, phoneCode + u.getMobilePhone(), msg, oracleCon, investmentId, SMSManagerBase.SMS_KEY_TYPE_SMS, providerId, SMS.DESCRIPTION_EXPIRY);
//            } catch (SMSException smse) {
//                log.warn("Failed to send SMS.", smse);
//            }
//        }
//    }

    public static boolean cancelInvestment(long id, long writer, Connection con) throws SQLException {
    	return cancelInvestment(id, writer, con, false, null, 0l);
    }

    public static boolean cancelInvestment(long id, long writer, boolean notSettledOnly, WebLevelsCache wlc, long userId) {
    	Connection con = null;
    	try {
			con = getConnection();
			return cancelInvestment(id, writer, con, notSettledOnly, wlc, userId);
		} catch (SQLException e) {
			log.debug("Unable to cancel investment with id = " + id, e);
			return false;
		} finally {
			closeConnection(con);
		}
    }

	public static boolean cancelInvestment(long id, long writer, Connection con, boolean notSettledOnly, WebLevelsCache wlc, long userId) throws SQLException {

		if (log.isEnabledFor(Level.DEBUG)) {
			String ls = System.getProperty("line.separator");
			log.log(Level.DEBUG, "Cancel Investment: InvestmentId:" + id + ls);
		}

		com.anyoption.common.beans.Investment i = InvestmentsDAOBase.getById(con, id);
		boolean isPayTax = false;

		if (i.getIsCanceled() == 1) {
			return false;
		}

		if (userId > 0 && i.getUserId() != userId) {
			log.warn("Investment user id is different than the given user id, investment user id = " + i.getUserId() + ", user id : " + userId);
			return false;
		}
		
		if (i.getIsSettled() == 1 && notSettledOnly) {
			return false;
		}

		double taxPercentage = ConstantsBase.TAX_PERCENTAGE;
		if (UsersDAOBase.isTaxExemption(con, i.getUserId())) {
			taxPercentage = 0;
		}

		UserBase user = UsersDAOBase.getUserById(con, i.getUserId());
		ArrayList<BonusFormulaDetails> bonusFormulaD = BonusDAOBase.getBonusAmounts(con, id, false);
		long adjustedAmount;
		
		//reverse bonus wagering
		BonusDAOBase.reverseBonusWagering(con, id, writer, userId);
		
		if (i.getIsSettled() == 0) { // not settled
			UsersDAOBase.addToBalance(con, i.getUserId(), i.getAmount());
			InvestmentsDAOBase.cancelInvestment(con, id, writer, false);
			if (!bonusFormulaD.isEmpty()) {
				for (BonusFormulaDetails item : bonusFormulaD) {
					log.debug("bonus amount = " + item.getAmount());
					adjustedAmount = item.getAdjustedAmount() + item.getAmount();
					log.debug("new adjustedAmount = " + adjustedAmount);
					BonusDAOBase.updateAdjustedAmount(con, item.getBonusUsersId(), adjustedAmount);
				}
			}
			if (user.getClassId() != ConstantsBase.USER_CLASS_TEST) {
				LevelsCache levelsCache = null;
				if (wlc == null) {
					FacesContext context = FacesContext.getCurrentInstance();
					levelsCache = (LevelsCache) context.getExternalContext().getApplicationMap().get("levelsCache");
				} else {
					levelsCache = wlc;
				}
	        	double secondParam = i.getId(); //id for not binary0100 opp
	        	if (isBinary0100(i)) {
	        		secondParam = -1 * i.getRate(); //in binary 0100 its the number of contracts param
	        	}
        		CommonUtil.sendNotifyForInvestment(
				        					i.getOpportunityId(),
				        					secondParam,
				        					(-1d) * i.getAmount() * i.getRate(),
				        					(-1L) * i.getAmount(),
				        					i.getTypeId(),
				        					i.getMarketId(),
				        					OpportunityType.PRODUCT_TYPE_BINARY,
				        					i.getCurrentLevel(),
				        					levelsCache,
				        					user,
				        					CopyOpInvTypeEnum.of(i.getCopyOpTypeId()));
			}

		} else {   // investment already settled

			/*
			 *  check if we can cancel the investment.
			 *  if settle-date done in period1(mid-year) and we take tax then we cannot cancel
			*/

			// find the year(text format) of the investment
            if (CommonUtil.isHebrewSkin(Long.parseLong(i.getSkin()))) {
    			Calendar cal = GregorianCalendar.getInstance();
    			cal.setTime(i.getTimeSettled());
    			long investmentYear = new Long(cal.get(Calendar.YEAR));
    			boolean isSettleInOnePeriod = InvestmentsDAOBase.isSettleInMidYear(con,id, String.valueOf(investmentYear));
    			long settleYear = Long.valueOf(Long.valueOf(investmentYear));

                isPayTax = TaxHistoryDAO.isPayMidYearTax(con,i.getUserId(),settleYear);
    			if (isSettleInOnePeriod) {
    				if (isPayTax) { // user pay tax and investment settle befor period1 job
    					if (log.isEnabledFor(Level.WARN)) {
    						String ls = System.getProperty("line.separator");
    						log.warn("Error With Cancel Investment, Tax already taken For Mid-Year" + ls);
    					}
    					return false;
    				}
    			}
            }

			//cancel investment
			InvestmentsDAOBase.cancelInvestment(con, id, writer, true);

			//amount to refund (to us or to user)
			long amount=0;
			if (i.getWin()>0) {
				// substract the win amount from balance
				amount = -(i.getWin() - i.getLose()) ;
			} else {
				// add the lose amount to balance
				amount = i.getLose();
			}
			// here the amount could be negative, that's why this method is used
			UsersManagerBase.addToBalanceNonNeg(con, i.getUserId(), amount, i.getRate(), i.getUtcOffsetCreated());

			//update bonus user Adjusted Amount
			if (!bonusFormulaD.isEmpty()) {
				for (BonusFormulaDetails item : bonusFormulaD) {
					log.debug("bonus amount = " + item.getAmount() + 
							" (double)(i.getWin() - i.getLose()) " + (double)(i.getWin() - i.getLose()) + 
							" ((double)item.getAmount() * (double)(i.getWin() - i.getLose())) = " + ((double)item.getAmount() * (double)(i.getWin() - i.getLose())) + 
							" ((double)item.getAmount() * (double)(i.getWin() - i.getLose()))/(double)i.getAmount() = " + ((double)item.getAmount() * (double)(i.getWin() - i.getLose()))/(double)i.getAmount() +
							" BigDecimal.valueOf(((double)item.getAmount() * (double)(i.getWin() - i.getLose()))/(double)i.getAmount()).setScale(0, RoundingMode.HALF_UP).longValue() " + BigDecimal.valueOf(((double)item.getAmount() * (double)(i.getWin() - i.getLose()))/(double)i.getAmount()).setScale(0, RoundingMode.HALF_UP).longValue());
					adjustedAmount = item.getAdjustedAmount() - BigDecimal.valueOf(((double)item.getAmount() * (double)(i.getWin() - i.getLose()))/(double)i.getAmount()).setScale(0, RoundingMode.HALF_UP).longValue();
					BonusDAOBase.updateAdjustedAmount(con, item.getBonusUsersId(), adjustedAmount);
				}
			}

			boolean nextInvestmentOnUsBonus = i.getBonusOddsChangeTypeId() == 1 && i.getLose() > 0;
			
            if (CommonUtil.isHebrewSkin(Long.parseLong(i.getSkin())) && !nextInvestmentOnUsBonus) {
    			//update balance with tax
    			long currentTax = UsersDAOBase.getTaxBalance(con, i.getUserId());
    			UsersDAOBase.addToBalance(con, i.getUserId(), currentTax);
    			
    			long winLose = user.getTotalWinLose();//InvestmentsDAOBase.getTotalWinLoseForTheYear(con, i.getUserId(), isPayTax, utcOffsetET);
    			
    			// recalculate total Win/Lose // TODO check if bonus and insurance are calculated in getWin/getLose 
    			if(i.getWin()>0) {
    				winLose = winLose - ((i.getWin() - i.getLose()));
    			} else {
    				winLose = winLose + i.getLose();
    			}
 
    			//recalculate tax (excluding this cancelled investment)
    			long newTax = 0;
    			if (winLose > 0) {
    				newTax = Math.round(winLose * taxPercentage);
    			}

    			UsersDAOBase.updateWinLose(con, i.getUserId(), winLose);
    			
    			//update new tax
    			UsersDAOBase.updateTax(con, i.getUserId(), newTax);
    			//upldate balance with new tax (deduct previous added tax)
    			UsersDAOBase.addToBalance(con, i.getUserId(), -newTax);
            }
		}


		//update balance history
		String table = ConstantsBase.TABLE_INVESTMENTS;
		String tlog = "Cancel Investment finished successfully.";
		int	command = ConstantsBase.LOG_BALANCE_CANCEL_INVESTMENT;
		String utcoffset =UsersDAOBase.getUtcOffset(con, i.getUserId());
		GeneralDAO.insertBalanceLog(con,writer,i.getUserId(), table, i.getId(), command, utcoffset);

		// Loyalty reverse points
		long investPoints = TiersDAOBase.getInvetPointsByInvestmentId(con, i.getId());
		if (investPoints > 0) {
			log.debug("Reverse LoyaltyPoints, investPoints: " +investPoints);
			TierUser tierU = TiersDAOBase.getTierUserByUserId(con, i.getUserId());
			long updatedPoints = tierU.getPoints() - investPoints;
			log.debug("Going to update tierUser points, currentPoints: " + tierU.getPoints() + ", " +
						"UpdatedPoints: " + updatedPoints);
			TiersManagerBase.updateTierUser(con, tierU.getId(), writer, updatedPoints);

	    	TierUserHistory h = TiersManagerBase.getTierUserHisIns(tierU.getTierId(), i.getUserId(), TiersManagerBase.TIER_ACTION_REVERSE_POINTS,
	    							writer,	updatedPoints, -investPoints, i.getId(), ConstantsBase.TABLE_INVESTMENTS, utcoffset);
			log.debug("Going to insert reverse action, " + h.toString());
	    	TiersDAOBase.insertIntoTierHistory(con, h);
		}

		//add log
		if (log.isEnabledFor(Level.DEBUG)) {
			String ls = System.getProperty("line.separator");
			log.log(Level.DEBUG, tlog + ls);
		}
		return true;

	}

	public static ArrayList getAllInvestmentsByOppId(long id) throws SQLException {

		Connection conn = null;
		try {
			conn = getConnection();
			return InvestmentsDAOBase.getAllInvestmentsByOppId(conn, id, true);
		} finally {
			closeConnection(conn);
		}

	}

	public static ArrayList<com.anyoption.common.beans.Investment> getAllInvestmentsByOppId(long id, Connection conn) throws SQLException {
			return InvestmentsDAOBase.getAllInvestmentsByOppId(conn, id, false);
	}

	public static long getWinLoseByOpp(long opId) throws SQLException {
		Connection con = getConnection();
		try {
			return OpportunitiesDAOBase.getWinLoseByOpp(con, opId);
		} finally {
			closeConnection(con);
		}
	}

//	/**
//	 * This method grants a bonus transaction to the user
//	 * @param con - connection to the DB
//	 * @param i - investment to cancel
//	 * @return - succeed
//	 * @throws SQLException
//	 */
//    public static void grantBonus(Connection con, com.anyoption.common.beans.Investment investment, long writerId, long amount, long loginId) throws SQLException {
//
//    	//get transaction
//    	Transaction bonus = getBonusTransaction(investment, amount, loginId);
//
//    	//add bonus transaction to user
//		TransactionsDAOBase.insertFromService(con, bonus);
//		if (log.isEnabledFor(Level.INFO)) {
//			String ls = System.getProperty("line.separator");
//			log.log(Level.INFO, "Bonus transaction inserted. TransactionId: " + bonus.getId() + " Amount: " + bonus.getAmount() + ls);
//		}
//
//		//update balance
//		UsersDAOBase.addToBalance(con, bonus.getUserId(), bonus.getAmount());
//
//		//update balance history
//		String table = ConstantsBase.TABLE_TRANSACTIONS;
//		int command = ConstantsBase.LOG_BALANCE_BONUS_DEPOSIT;
//		GeneralDAO.insertBalanceLog(con, writerId, investment.getUserId(), table, bonus.getId(), command, bonus.getUtcOffsetCreated());
//
//		//update log
//		String tlog = "Bonus was granted to User.";
//		if (log.isEnabledFor(Level.DEBUG)) {
//			String ls = System.getProperty("line.separator");
//			log.log(Level.DEBUG, tlog + ls);
//		}
//}

//	/**
//	 * This method receives an investment and create a bonus transaction according to that investment.
//	 * @param i - the investment to create a transaction (towards the customer) upon
//	 * @return
//	 */
//	private static Transaction getBonusTransaction(com.anyoption.common.beans.Investment i, long amount, long loginId) {
//		Transaction bonus = new Transaction();
//		bonus.setUserId(i.getUserId());
//		bonus.setAmount(amount);
//		bonus.setTypeId(TransactionsManagerBase.TRANS_TYPE_BONUS_DEPOSIT);
//		bonus.setStatusId(TransactionsManagerBase.TRANS_STATUS_SUCCEED);
//		bonus.setCreditCardId(null);
//		bonus.setComments("investment id: " + i.getId());
//		bonus.setDescription("log.balance.bonus.deposit");
//		bonus.setIp("IP NOT FOUND!");
//		bonus.setChequeId(null);
//		bonus.setTimeSettled(new Date());
//		bonus.setTimeCreated(new Date());
//		bonus.setUtcOffsetCreated(i.getUtcOffsetSettled());
//		bonus.setUtcOffsetSettled(i.getUtcOffsetSettled());
//		bonus.setWriterId(i.getWriterId());
//		bonus.setWireId(null);
//		bonus.setChargeBackId(null);
//		bonus.setBonusUserId(i.getBonusUserId());
//		bonus.setLoginId(loginId);
//		return bonus;
//	}

	public static ArrayList getAllInvestmentTypes() throws SQLException {
		Connection con = null;
		try {
			con = getConnection();
			return InvestmentTypesDAO.getAll(con);
		} finally {
			closeConnection(con);
		}
	}

//		Main method to test settleInvestment
//	  	public static void main(String [] args) {
//	      try {
//	            DriverManager.registerDriver(new OracleDriver());
//	            Connection con=  DriverManager.getConnection("jdbc:oracle:thin:@dbtest.etrader.co.il:1521:etrader","etrader","etrader");
//	            InvestmentsManagerBase.settleInvestment(con,2566939, 1);
//	      }catch(Exception e) {}
//	    }

    /**
     * get investment to settle.
     *
     * @param investmentId the id of the investment to settle.
     * @param oppSettled true if to bring the investment only if the opp setteld, false to bring it also if its not settled (use in 5 gold minutes)
     * @param isGM if its take profit (GM) true else false
     * @return the investment
     * @throws SQLException
     */
    public static com.anyoption.common.beans.Investment getInvestmentToSettle(long investmentId, boolean oppSettled, boolean isGM, Long userId) throws SQLException {
        Connection conn = null;
        com.anyoption.common.beans.Investment investment = null;
        try {
            conn = getConnection();
            investment = InvestmentsDAOBase.getInvestmentToSettle(conn, investmentId, oppSettled, isGM, userId);
        } finally {
            closeConnection(conn);
        }
        return investment;
    }

    public static HashMap<String, Integer> getInsuranceTime() throws SQLException {
        HashMap<String, Integer> l = null;
        Connection conn = getConnection();
        try {
            l = InvestmentsDAOBase.getInsuranceTime(conn);
        } finally {
            closeConnection(conn);
        }
        return l;
    }

    public static long getUserMinInvestmentLimit(long currencyId) throws SQLException {
		Connection con = getConnection();
		try {
			return InvestmentsDAOBase.getUserMinInvestmentLimit(con, currencyId);
		} finally {
			closeConnection(con);
		}
    }
    public static Opportunity getOpportunityById(long id) throws SQLException {
        Connection con = getConnection();
        try {
            return OpportunitiesDAOBase.getById(con, id);
        } finally {
            closeConnection(con);
        }
    }

    public static OptionPlusQuote getLastQuote(long userId, long investmentId, double price, Date sellTime) throws SQLException {
        Connection conn = getConnection();
        try {
            return InvestmentsDAOBase.getLastQuote(conn, investmentId, price, sellTime);
        } finally {
            closeConnection(conn);
        }
    }

	public static void updatePurchasedQuote(long quoteId) throws SQLException {
        Connection conn = getConnection();
        try {
            InvestmentsDAOBase.updatePurchasedQuote(conn, quoteId);
        } finally {
            closeConnection(conn);
        }
	}

	public static ArrayList<Long> getInvestmentsConnectedIDByInvId(long invId) throws SQLException {
        Connection conn = getConnection();
        try {
            return InvestmentsDAOBase.getInvestmentsConnectedIDByInvId(conn, invId);
        } finally {
            closeConnection(conn);
        }
	}

	/**
	 * In case the company's profit is higher than 'turnover*('profit_percent')+bonus amount' 
	 * in time frame of 3 month since the bonus was activated,
	 * the method would return true, the method would return false otherwise.
	 * @param conn 
	 * @param userId user unique id.
	 * @param bonusTimeActivated time when the bonus made active.
	 * @param bonusAmount the quantity of the bonus amount that the user was receive. 
	 * @return true if the method met the condition above, false otherwise.
	 * @throws SQLException
	 */
	public static boolean isCompanyProfitHigher(Connection conn, long userId, Date bonusTimeActivated, long bonusAmount) throws SQLException {
	    boolean isHigher = false;
	  
	    try {
	    	long invAmount = InvestmentsDAOBase.getInvestmentAmountOnTimeFrame(conn, userId, bonusTimeActivated);
	    	long houseRes = InvestmentsDAOBase.getHouseResultOnTimeFrame(conn, userId, bonusTimeActivated);	
	    	
	    	if ((invAmount * ConstantsBase.BONUS_MANAGEMENT_PROFIT_PERCENT + bonusAmount) < houseRes) {
	    		isHigher = true;
	    	}
	    } catch (SQLException sqle) {
	    	log.error("BMS Error!!! in isCompanyProfitHigher. ", sqle);
	    	throw sqle;
	    }
	    return isHigher;
	}

	/**
	 * get top last trades,
	 * Highest profit generated in USD terms from single in-the-money expiry.
	 *
	 * @param conn
	 * @param fromDate
	 * @param tillDate
	 * @return last top trades as ArrayList<Investment>
	 * @throws SQLException
	 */
    public static ArrayList<com.anyoption.common.beans.Investment> getTopTrades(Connection conn, String fromDate, String tillDate, long skinBusinessCaseId) throws SQLException {
    	return InvestmentsDAOBase.getLastTopTrades(conn, fromDate, tillDate, skinBusinessCaseId);
    }
    
    /**
     * Get copyop investment details
     * @param con
     * @param originalInvId
     * @return
     * @throws SQLException
     */
    public static CopyopInvestmentDetails getCopyopInvestmentDetails(long originalInvId) throws SQLException {
        Connection conn = getConnection();
        CopyopInvestmentDetails copyopInvestmentDetails = null;
        try {
        	copyopInvestmentDetails = InvestmentsDAOBase.getCopyopInvestmentDetails(conn, originalInvId);
        	if (copyopInvestmentDetails != null) { 
        		copyopInvestmentDetails.setCopyBy(InvestmentsDAOBase.getCopyopInvestmentsCopy(conn, originalInvId));
        	}        
        } finally {
            closeConnection(conn);
        }
        return copyopInvestmentDetails;
    }

	public static boolean isBinary0100(Investment i) {
		boolean isBinary0100 = false;
		if (i != null && (i.getTypeId() == InvestmentsManagerBase.INVESTMENT_TYPE_BUY
							|| i.getTypeId() == InvestmentsManagerBase.INVESTMENT_TYPE_SELL)) {
			isBinary0100 = true;
		}
		return isBinary0100;
	}

	public static boolean isDynamics(Investment i) {
		if (i != null && (i.getTypeId() == InvestmentsManagerBase.INVESTMENT_TYPE_DYNAMICS_BUY || i.getTypeId() == InvestmentsManagerBase.INVESTMENT_TYPE_DYNAMICS_SELL)) {
			return true;
		}
		return false;
	}
	
	public static boolean isDynamicsPartiallyClosed(Investment i) {
		if (isDynamics(i) && i.getReferenceInvestmentId() > 0) {
			return true;
		}
		return false;
	}
	
	public static Investment getDynamicsPartiallyClosedOriginalInvAmount(Investment i) throws SQLException {
		if (isDynamicsPartiallyClosed(i)) {
			Connection conn = getConnection();
			return InvestmentsDAOBase.getDynamicsPartiallyClosedOriginalInvAmount(conn, i);
		}
		return null;
	}
	
	public static boolean isBubbles(Investment i) {
		if (i.getTypeId() == InvestmentsManagerBase.INVESTMENT_TYPE_BUBBLES) {
			return true;
		}
		return false;
	}

	public static boolean isSettledBeforTime(Investment i) {
		if (i.getTimeEstClosing() == null || i.getTimeSettled() == null) {
			return false;
		}
		Calendar estClosing = Calendar.getInstance();
		estClosing.setTime(i.getTimeEstClosing());
		Calendar timeSettledCal = Calendar.getInstance();
		timeSettledCal.setTime(i.getTimeSettled());
		return timeSettledCal.before(estClosing);
	}

	public static boolean isTypeBinary0100Above(Investment i) {
		boolean above = false;
		if (i.getOpportunityTypeId() == Opportunity.TYPE_BINARY_0_100_ABOVE) {
			above = true;
		}
		return above;
	}

	public static boolean isTypeBinary0100Below(Investment i) {
		boolean below = false;
		if (i.getOpportunityTypeId() == Opportunity.TYPE_BINARY_0_100_BELOW) {
			below = true;
		}
		return below;
	}
	
	public static HashMap<Long, InvestmentLimitsGroup> getInvestmentLimitGroupsMap() throws SQLException {
		HashMap<Long, InvestmentLimitsGroup> investmentLimitGroups = new HashMap<Long, InvestmentLimitsGroup>();
		Connection con = null;
		try{
			con = getConnection();
			investmentLimitGroups = InvestmentsDAOBase.getInvestmentLimitGroupsMap(con);
		} finally {
			closeConnection(con);
		}
		return investmentLimitGroups;
	}
}