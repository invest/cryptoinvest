//package il.co.etrader.bl_managers;
//
//import il.co.etrader.dao_managers.CountryDAOBase;
//
//import java.sql.Connection;
//import java.sql.SQLException;
//
//import com.anyoption.common.beans.Country;
//import com.anyoption.common.managers.BaseBLManager;
//
//public class CountryManagerBase extends BaseBLManager {
//
//	/**
//	 * Get code by country id
//	 * @param countryId
//	 * @return
//	 * @throws SQLException
//	 */
//	public static String getCodeById(long countryId) throws SQLException {
//		Connection con = getConnection();
//		try {
//			return CountryDAOBase.getCodeById(con, countryId);
//		} finally {
//			closeConnection(con);
//		}
//	}
//
//	/**
//	 * Get country id by code
//	 * @param code
//	 * 		country code
//	 * @return
//	 * @throws SQLException
//	 */
//	public static long getIdByCode(String code) throws SQLException {
//		Connection con = getConnection();
//		try {
//			return CountryDAOBase.getIdByCode(con, code);
//		} finally {
//			closeConnection(con);
//		}
//	}
//	
//	/**
//	 * Get country name by id
//	 * @param id
//	 * 		country id
//	 * @return
//	 * @throws SQLException
//	 */
//	public static String getNameById(long id) throws SQLException {
//		Connection con = getConnection();
//		try {
//			return CountryDAOBase.getNameById(con, (int) id);
//		} finally {
//			closeConnection(con);
//		}
//	}
//
//    /**
//     * Get phone code by country id
//     * @param code
//     *      country code
//     * @return
//     * @throws SQLException
//     */
//    public static String getPhoneCodeById(long id) throws SQLException {
//        Connection con = getConnection();
//        try {
//            return CountryDAOBase.getPhoneCodeById(con, id);
//        } finally {
//            closeConnection(con);
//        }
//    }
//
//    public static Country getById(long countryId) throws SQLException {
//        Connection conn = null;
//        try {
//            conn = getConnection();
//            return CountryDAOBase.getById(conn, countryId);
//        } finally {
//            closeConnection(conn);
//        }
//    }
//
//    public static Country getById(Connection conn,  long countryId) throws SQLException {
//        try {
//            return CountryDAOBase.getById(conn, countryId);
//        } finally {
//
//        }
//    }
//
//}