package il.co.etrader.bl_managers;

import java.io.UnsupportedEncodingException;
import java.sql.Connection;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;

import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.AssetIndexMarket;
import com.anyoption.common.beans.Country;
import com.anyoption.common.beans.InvestmentLimitsGroup;
import com.anyoption.common.beans.Market;
import com.anyoption.common.beans.MarketGroup;
import com.anyoption.common.beans.ServerConfiguration;
import com.anyoption.common.beans.SkinCurrency;
import com.anyoption.common.beans.TierPointConversion;
import com.anyoption.common.beans.TournamentLanguageTabs;
import com.anyoption.common.beans.base.BankBranches;
import com.anyoption.common.beans.base.CreditCardType;
import com.anyoption.common.beans.base.Currency;
import com.anyoption.common.beans.base.Language;
import com.anyoption.common.beans.base.Opportunity;
import com.anyoption.common.beans.base.Skin;
import com.anyoption.common.bl_vos.BankBase;
import com.anyoption.common.daos.BanksDAOBase;
import com.anyoption.common.daos.CountryDAOBase;
import com.anyoption.common.daos.CurrenciesDAOBase;
import com.anyoption.common.daos.GeneralDAO;
import com.anyoption.common.daos.LanguagesDAOBase;
import com.anyoption.common.daos.TournamentDAOBase;
import com.anyoption.common.managers.BaseBLManager;
import com.anyoption.common.managers.GeneralManager;
import com.anyoption.common.managers.TournamentManagerBase;
import com.anyoption.common.util.Key;

import il.co.etrader.bl_vos.City;
import il.co.etrader.bl_vos.ErrorCode;
import il.co.etrader.bl_vos.PopulationEntryHisStatus;
import il.co.etrader.bl_vos.Skins;
import il.co.etrader.bl_vos.Writer;
import il.co.etrader.dao_managers.CitiesDAO;
import il.co.etrader.dao_managers.ClearingDAO;
import il.co.etrader.dao_managers.CreditCardTypesDAO;
import il.co.etrader.dao_managers.InvestmentsDAOBase;
import il.co.etrader.dao_managers.MarketsDAOBase;
import il.co.etrader.dao_managers.OpportunitiesDAOBase;
import il.co.etrader.dao_managers.PopulationsDAOBase;
import il.co.etrader.dao_managers.SkinCurrenciesDAOBase;
import il.co.etrader.dao_managers.SkinLanguagesDAOBase;
import il.co.etrader.dao_managers.SkinPaymentMethodDAOBase;
import il.co.etrader.dao_managers.SkinsDAOBase;
import il.co.etrader.dao_managers.TiersDAOBase;
import il.co.etrader.dao_managers.TransactionsDAOBase;
import il.co.etrader.dao_managers.UsersDAOBase;
import il.co.etrader.dao_managers.WritersDAO;
import il.co.etrader.util.CommonUtil;
import il.co.etrader.util.ConstantsBase;


/**
 * @author liors
 *
 */
public class ApplicationDataBase extends BaseBLManager {
	protected static final Logger logger = Logger.getLogger(ApplicationDataBase.class);

	protected static ArrayList<SelectItem> hours;
	protected static ArrayList<SelectItem> minutes;
	protected static ArrayList<SelectItem> days;
	protected static ArrayList<SelectItem> months;
	protected static ArrayList<SelectItem> years;

	protected static ArrayList<SelectItem> gmtOffsets;
	protected static ArrayList<SelectItem> retentionGmtOffsets;
	protected static ArrayList<SelectItem> retentionCountries;
	protected static ArrayList<SelectItem> currencies;             // selectedItem list
    protected static ArrayList<Currency> currenciesList;           // list of all currencies
	protected static ArrayList<SelectItem> languages;              // selectedItem list
	protected static ArrayList<Language> languagesList;
    protected static HashMap<Long, Language> languagesHash;

	// protected ArrayList classes;
    protected static ArrayList<SelectItem> expYears;
	protected static ArrayList<SelectItem> creditCardYears;
	protected static HashMap<String, String> cities;
	protected static ArrayList<SelectItem> mobilePref;
	private static ArrayList<String> mobilePrefPlain;
	protected static ArrayList<SelectItem> phonePref;
	protected static ArrayList<SelectItem> citiesList;

	protected static ArrayList<SelectItem> allCreditCardTypes;    // all credit cards
	protected static ArrayList<SelectItem> creditCardTypes;       // for etrader
	protected static ArrayList<SelectItem> creditCardTypesAo;     // for anyOption the list is different
	protected static ArrayList<SelectItem> creditCardTypesRegulation;
    protected static ArrayList<SelectItem> creditCardTypesAoChina; // for anyOption China the list is different
	protected static ArrayList<SelectItem> transStatuses;
	//protected static ArrayList transTypes;
	protected static HashMap<Long, Market> markets;
	protected static ArrayList<MarketGroup> marketsGroup;
	protected static String citiesListString;
	protected static ArrayList<SelectItem> marketsSI;
	protected static ArrayList<SelectItem> groupsSI;

	protected static ArrayList<Skins> skinsList;
	protected static HashMap<Long, Country> countries;
	protected static ArrayList<SelectItem> states;
	protected static ArrayList<SelectItem> statesCode;
	protected static ArrayList<SelectItem> countriesList;
	protected static ArrayList<SelectItem> countriesRegulatedList;
	protected static ArrayList<SelectItem> countriesNotRegulatedList;
	protected static HashMap<String, String> countryPhoneCodes;
	protected static ArrayList<Market> assetIndex;

	// support informations list
	protected static ArrayList<SelectItem> supportCountries;
	protected static HashMap<String, String> countrySupportPhones;
	protected static HashMap<String, String> countrySupportFaxes;

	protected static ArrayList<TierPointConversion> tierPointsConvertion;

	protected static HashMap<Integer, PopulationEntryHisStatus> popEntryHisStatusHash;
	public static HashMap<Integer, String> populationEntryType;

	// limit groups
	protected static HashMap<Long, InvestmentLimitsGroup> invLimitisGroupsMap;

	// online deposits writers
	protected static ArrayList<Writer> depositedWritersTV1;
	protected static ArrayList<Writer> depositedWritersTV2;

    //banks
    protected static HashMap<Long, BankBase> banks;
    protected static ArrayList<SelectItem> banksSI;
    protected static ArrayList<BankBranches> bankBranches;

    protected static HashMap<String, Long> languageCodesSkins;
    protected static HashMap<String, Long> languageCodesSkinsRegulated;
    protected static ArrayList<ErrorCode> clearingErrorCode;
    protected static ArrayList<SelectItem> opportunityProductTypes;
    protected static Map<Long, String> platformsMap;
    
    protected static HashMap<Long, String> predefinedDepositAmount;
    protected static ArrayList<Long> writersAllowedToViewCcFirst6Digits;
    private static ArrayList<TournamentLanguageTabs> tournamentLangTabs;

	private static Map<Key, Integer> epgProductIds;
	
	private static LocalDate dateMarkets;

    public static void init() throws SQLException {
		if (logger.isDebugEnabled()) {
		    logger.debug("ApplicationDataBase.init - loading data " + new Date());
        }
		hours = new ArrayList<SelectItem>();
		minutes = new ArrayList<SelectItem>();
		days = new ArrayList<SelectItem>();
		months = new ArrayList<SelectItem>();
		years = new ArrayList<SelectItem>();
		citiesList = new ArrayList<SelectItem>();
		cities = new HashMap<String, String>();
		mobilePref = new ArrayList<SelectItem>();
		mobilePrefPlain = new ArrayList<String>();
		phonePref = new ArrayList<SelectItem>();
		creditCardYears = new ArrayList<SelectItem>();
		expYears = new ArrayList<SelectItem>();
		skinsList = new ArrayList<Skins>();
		countries = new HashMap<Long, Country>();
		countriesList = new ArrayList<SelectItem>();
		countriesRegulatedList = new ArrayList<SelectItem>();
		countriesNotRegulatedList = new ArrayList<SelectItem>();
		currencies = new ArrayList<SelectItem>();
		languages = new ArrayList<SelectItem>();
		countryPhoneCodes = new HashMap<String, String>();
		currenciesList = new ArrayList<Currency>();
		languagesList = new ArrayList<Language>();
		states = new ArrayList<SelectItem>();
		statesCode = new ArrayList<SelectItem>();
		supportCountries = new ArrayList<SelectItem>();
		countrySupportPhones = new HashMap<String, String>();
		countrySupportFaxes = new HashMap<String, String>();
        languagesHash = new HashMap<Long, Language>();
        gmtOffsets = new ArrayList<SelectItem>();

        tierPointsConvertion = new ArrayList<TierPointConversion>();
        depositedWritersTV1 = new ArrayList<Writer>();
        depositedWritersTV2 = new ArrayList<Writer>();
        bankBranches = new ArrayList<BankBranches>();
        languageCodesSkins = new HashMap<String, Long>();
        languageCodesSkinsRegulated = new HashMap<String, Long>();
        writersAllowedToViewCcFirst6Digits = new ArrayList<Long>();

		mobilePref.add(new SelectItem("050"));
		mobilePref.add(new SelectItem("052"));
		mobilePref.add(new SelectItem("053"));
		mobilePref.add(new SelectItem("054"));
		mobilePref.add(new SelectItem("055"));
        mobilePref.add(new SelectItem("057"));
        mobilePref.add(new SelectItem("058"));

        mobilePrefPlain.add("050");
        mobilePrefPlain.add("052");
        mobilePrefPlain.add("053");
        mobilePrefPlain.add("054");
        mobilePrefPlain.add("055");
        mobilePrefPlain.add("057");
        mobilePrefPlain.add("058");

		phonePref.add(new SelectItem("02"));
		phonePref.add(new SelectItem("03"));
		phonePref.add(new SelectItem("04"));
		phonePref.add(new SelectItem("08"));
		phonePref.add(new SelectItem("09"));
		phonePref.add(new SelectItem("072"));
		phonePref.add(new SelectItem("073"));
		phonePref.add(new SelectItem("074"));
		phonePref.add(new SelectItem("076"));
        phonePref.add(new SelectItem("077"));
        phonePref.add(new SelectItem("078"));


		for (int i = 1; i < 32; i++) {
			if (i < 10) {
				days.add(new SelectItem("0" + String.valueOf(i), "0" + String.valueOf(i)));
            } else {
				days.add(new SelectItem(String.valueOf(i), String.valueOf(i)));
            }
		}
		for (int i = 0; i <= 23; i++) {
			if (i < 10) {
				hours.add(new SelectItem("0" + String.valueOf(i), "0" + String.valueOf(i)));
            } else {
				hours.add(new SelectItem(String.valueOf(i), String.valueOf(i)));
            }
		}
		for (int i = 0; i <= 59; i++) {
			if (i < 10) {
				minutes.add(new SelectItem("0" + String.valueOf(i), "0" + String.valueOf(i)));
            } else {
				minutes.add(new SelectItem(String.valueOf(i), String.valueOf(i)));
            }
		}

		String[] gmtOffsetsList = {"GMT-12:00","GMT-11:00","GMT-10:00","GMT-09:00","GMT-08:00","GMT-07:00",
				"GMT-06:00","GMT-05:00","GMT-04:00","GMT-03:00","GMT-02:00","GMT-01:00","GMT-00:00","GMT+01:00",
				"GMT+02:00","GMT+03:00","GMT+03:30","GMT+04:00","GMT+04:30","GMT+05:00","GMT+05:30","GMT+06:00",
				"GMT+07:00","GMT+08:00","GMT+10:00","GMT+11:00","GMT+12:00","GMT+13:00"};

		for (int i=0;i<gmtOffsetsList.length;++i){
			gmtOffsets.add(new SelectItem (gmtOffsetsList[i],gmtOffsetsList[i]));
		}


		GregorianCalendar g = new GregorianCalendar();
		int currentYear = g.get(GregorianCalendar.YEAR);

		//Extend year selection for VISA electron
		for (int i = currentYear; i <= currentYear + 20; i++) {
			String yearStr = String.valueOf(i);
			expYears.add(new SelectItem(yearStr, yearStr));
			yearStr = yearStr.substring(yearStr.length() - 2); // taking the 2 last numbers from the year number
			creditCardYears.add(new SelectItem(yearStr, yearStr));
		}

		for (int i = 1; i < 13; i++) {
			if (i < 10) {
				months.add(new SelectItem("0" + String.valueOf(i),"0" + String.valueOf(i)));
            } else {
				months.add(new SelectItem(String.valueOf(i),String.valueOf(i)));
            }
		}

		GregorianCalendar gc = new GregorianCalendar();
		for (int i = gc.get(GregorianCalendar.YEAR) - 18; i >= 1920; i--) {
			years.add(new SelectItem(String.valueOf(i), String.valueOf(i)));
        }

		Connection con = getConnection();
		try {
			// Load Static Data

			loadCities(con);

			currenciesList = CurrenciesDAOBase.getAllList(con);
			for (int i = 0; i < currenciesList.size(); i++) {
				Currency c = currenciesList.get(i);
				currencies.add(new SelectItem(new Long(c.getId()),c.getCode()));
			}

			languagesList = LanguagesDAOBase.getAll(con);
			for (int i = 0; i < languagesList.size(); i++) {
                Language c = languagesList.get(i);
				languages.add(new SelectItem(new Long(c.getId()), c.getCode()));
                languagesHash.put(c.getId(), c);
			}

			creditCardTypes = new ArrayList<SelectItem>();
			ArrayList<CreditCardType> ccTypes = CreditCardTypesDAO.getAll(con);
			for (int i = 0; i < ccTypes.size(); i++) {
				CreditCardType cc = (CreditCardType) ccTypes.get(i);
				if ( cc.getId() != TransactionsManagerBase.CC_TYPE_MAESTRO 
						&& cc.getId() != TransactionsManagerBase.CC_TYPE_CUP 
						   && cc.getId() != TransactionsManagerBase.CC_TYPE_DINERS ) {
					creditCardTypes.add(new SelectItem(String.valueOf(cc.getId()), cc.getName()));
				}
			}

			creditCardTypesAo = new ArrayList<SelectItem>();
			for (int i = 0; i < ccTypes.size(); i++) {
				CreditCardType cc = (CreditCardType) ccTypes.get(i);
				if ((cc.getId() != TransactionsManagerBase.CC_TYPE_ISRACARD) && (cc.getId() != TransactionsManagerBase.CC_TYPE_CUP) && (cc.getId()!=3)) {
					creditCardTypesAo.add(new SelectItem(String.valueOf(cc.getId()), cc.getName()));
				}
			}

			creditCardTypesRegulation = new ArrayList<SelectItem>();
			for (int i = 0; i < ccTypes.size(); i++) {
				CreditCardType cc = (CreditCardType) ccTypes.get(i);
				if (cc.getId() == TransactionsManagerBase.CC_TYPE_MASTERCARD || cc.getId() == TransactionsManagerBase.CC_TYPE_VISA) {
					creditCardTypesRegulation.add(new SelectItem(String.valueOf(cc.getId()), cc.getName()));
				}
			}

            creditCardTypesAoChina = new ArrayList<SelectItem>();
            for (int i = 0; i < ccTypes.size(); i++) {
                CreditCardType cc = (CreditCardType) ccTypes.get(i);
                if ((cc.getId() != TransactionsManagerBase.CC_TYPE_ISRACARD) && (cc.getId() != TransactionsManagerBase.CC_TYPE_AMEX)) {
                    creditCardTypesAoChina.add(new SelectItem(String.valueOf(cc.getId()), cc.getName()));
                }
            }

            allCreditCardTypes = new ArrayList<SelectItem>();
            for (int i = 0; i < ccTypes.size(); i++) {
                CreditCardType cc = (CreditCardType) ccTypes.get(i);
                allCreditCardTypes.add(new SelectItem(String.valueOf(cc.getId()), cc.getName()));
            }

			transStatuses = TransactionsDAOBase.getTransactionStatusesSI(con);

//			assetIndex = MarketsDAOBase.getAssetIndex(con);
			groupsSI = MarketsDAOBase.getGroupsSelectItems(con);

			markets = MarketsDAOBase.getAllMarkets(con);
			
			//tournament related object
    		tournamentLangTabs = TournamentDAOBase.getLangTabs(con);
    		
			predefinedDepositAmount = GeneralDAO.getAllPredefinedDepositAmounts(con);

            // get all countries and build countries list
    		ArrayList<Country> cList = CountryDAOBase.getAll(con);
			for (Country c : cList) {
				c.setPaymentsMethodCountry(CountryDAOBase.getPaymentMethodsByCountryId(con, c.getId()));
                countries.put(c.getId(), c);
				countriesList.add(new SelectItem(Long.valueOf(c.getId()).longValue(), c.getDisplayName()));
				countryPhoneCodes.put(String.valueOf(c.getId()), c.getPhoneCode());
				if(c.isRegulated()) {
					countriesRegulatedList.add(new SelectItem(Long.valueOf(c.getId()).longValue(), c.getDisplayName()));
					//countriesNotRegulatedList.add(new SelectItem(Long.valueOf(c.getId()).longValue(), c.getDisplayName()));
				}else {
					countriesNotRegulatedList.add(new SelectItem(Long.valueOf(c.getId()).longValue(), c.getDisplayName()));
				}
				// set support data
				if (!c.getSupportPhone().equals(ConstantsBase.COUNTRY_EMPTY_VALUE) &&
						!c.getSupportFax().equals(ConstantsBase.COUNTRY_EMPTY_VALUE)) {
					supportCountries.add(new SelectItem(Long.valueOf(c.getId()), c.getDisplayName()));
					countrySupportPhones.put(String.valueOf(c.getId()), c.getSupportPhone());
					countrySupportFaxes.put(String.valueOf(c.getId()), c.getSupportFax());
				}
			}
			
			//getAll Active Writers Allowed To View Cc First 6 Digits
			writersAllowedToViewCcFirst6Digits = WritersDAO.getAllActiveWritersAllowedToViewCcFirst6Digits(con);
			// get all skins
            skinsList = SkinsDAOBase.getAll(con);
            // fill currencies, languages and markets for each skin
            for (Skins skin : skinsList) {
            	skin.setSkinCurrenciesList(SkinCurrenciesDAOBase.getAllBySkin(con, skin.getId()));
            	HashMap<Long, SkinCurrency> skinCurrencies = new HashMap<Long, SkinCurrency>();
            	for (SkinCurrency sc : skin.getSkinCurrenciesList()) {
            		skinCurrencies.put(sc.getCurrencyId(), sc);
            	}
            	skin.setSkinCurrenciesHM(skinCurrencies);
            	skin.setSkinLanguagesList(SkinLanguagesDAOBase.getAllBySkin(con, skin.getId()));
            	//skin.setTreeItems(SkinsDAOBase.getTreeItems(con, skin.getId(), false));
            	skin.setTreeItems0100(SkinsDAOBase.getTreeItems(con, skin.getId(), true));
            	skin.setSkinPaymentMethodsList(SkinPaymentMethodDAOBase.getAllBySkinId(con, skin.getId()));
            	skin.setTiersLevels(TiersDAOBase.getTierLevels(con, skin.getId()));
            	skin.setNetreferCombinationId(SkinsDAOBase.getNetreferCombinationsId(con, skin.getId()));
            	skin.setReferpartnerCombinationId(SkinsDAOBase.getReferpartnerCombinationsId(con, skin.getId()));
                skin.setBankWireWithdraw(BanksDAOBase.getBankWireWithdrawBySkinSI(con, skin.getId()));
                if (skin.isLoadTickerMarkets()) {  // anyoption active skins
                	String langCode = getLanguage(skin.getDefaultLanguageId()).getCode().toLowerCase();
                	if (!skin.isCountryInLocaleUse()) {
                		if (skin.isRegulated()) {
                			languageCodesSkinsRegulated.put(langCode, Long.valueOf(skin.getId()));
                		} else {
                			languageCodesSkins.put(langCode, Long.valueOf(skin.getId()));
                		}

                	} else {
                		String countryCode = countries.get(skin.getDefaultCountryId()).getA2().toLowerCase();
                		if (skin.isRegulated()) {
                			languageCodesSkinsRegulated.put(langCode + "-" + countryCode, Long.valueOf(skin.getId()));
                		} else {
                			languageCodesSkins.put(langCode + "-" + countryCode, Long.valueOf(skin.getId()));
                		}
                	}
                }            	
            }
            // Language code "zh" should lead to skin 15. We have the same locale for skin 22 but this is out of anyoption for now
            languageCodesSkins.put("zh", new Long(15));

			states = CountryDAOBase.getStatesSI(con);
			statesCode = CountryDAOBase.getStatesCodeSI(con);

			tierPointsConvertion = TiersDAOBase.getTierPointsConvertions(con);
			popEntryHisStatusHash = PopulationsDAOBase.getPopEntryHistoryStatusHash(con);
	       	// 	population entry type
	        populationEntryType = PopulationsDAOBase.getPopulationEntryType(con);
	        invLimitisGroupsMap = InvestmentsDAOBase.getInvestmentLimitGroupsMap(con);

//          banks
            banks = BanksDAOBase.getBanks(con, ConstantsBase.BANK_TYPE_DEPOSIT, Skin.SKIN_BUSINESS_ETRADER);
            banksSI = CommonUtil.hashMap2SelectListString(banks, "getId", "getName");
            bankBranches = BanksDAOBase.getBanksBranches(con);
            clearingErrorCode = ClearingDAO.getAllErrorCodes(con);

            opportunityProductTypes = OpportunitiesDAOBase.getOpportunityProductTypes(con);
            
            platformsMap = UsersDAOBase.getPlatformsNames(con);
            epgProductIds = UsersDAOBase.getEPGProductIds(con);
		} catch (Exception e) {
            logger.debug("Cant init ApplicationDataBase!", e);
        } finally {
			closeConnection(con);
		}
	}

	/**
	 * Constructor that not load all data
	 * This is used, now, just for banners project
	 *
	 * @param dontLoadAll
	 * 		if true, load some data
	 * @throws Exception
	 */
	public static void initBanners() throws SQLException {
        if (logger.isDebugEnabled()) {
            logger.debug("ApplicationDataBase.initBanners - loading data(not all) " + new Date());
        }
        languagesHash = new HashMap<Long, Language>();
		Connection con = getConnection();

		try {
//          get all skins
		    skinsList = SkinsDAOBase.getAll(con);
            //get all languages id
            languagesList = LanguagesDAOBase.getAll(con);
            for (int i = 0; i < languagesList.size(); i++) {
                Language c = languagesList.get(i);
                languagesHash.put(c.getId(), c);
            }

			markets = new HashMap<Long, Market>();
			ArrayList list = MarketsDAOBase.getMarkets(con);
			for (int i = 0; i < list.size(); i++) {
				Market m = (Market) list.get(i);
				markets.put(new Long(m.getId()), m);
			}
        } catch (Exception e) {
            logger.warn("exception in init banners", e);
		} finally {
			closeConnection(con);
		}
         if (logger.isDebugEnabled()) {
            logger.debug("ApplicationDataBase.initBanners - finish loading data(not all) " + new Date());
        }
	}

	public String getCitiesListString() {
		return citiesListString;
	}
	
	public static String getCitiesListStringET() {
		return citiesListString;
	}

	public static List<String> getSuggestCities(String prefix, Integer n) throws SQLException, UnsupportedEncodingException {
		List<String> li = new ArrayList<String>();
		for (int i = 0; i < citiesList.size(); i++) {
			SelectItem si = citiesList.get(i);
			if (si.getLabel().startsWith(prefix)) {
				li.add(si.getLabel());
            }
		}
		return li;
	}

	public static String getGetSuggestCities() {
		return null;
	}

	public static HashMap<Long, Market> getMarkets() {
		LocalDate currentDate = LocalDateTime.now().toLocalDate();
		if(null == dateMarkets || currentDate.isAfter(dateMarkets) || null == markets) {
			Connection con = null;
			try {
				con = getConnection();		
				ArrayList<Market> list = MarketsDAOBase.getMarkets(con);
				if (null == markets) {
					markets = new HashMap<Long, Market>();
				}
				for (int i = 0; i < list.size(); i++) {
					Market m = (Market) list.get(i);
					markets.put(new Long(m.getId()), m);
				}
				dateMarkets = LocalDateTime.now().toLocalDate();
			} catch (SQLException ex) {
				logger.error("Can't lazy load markets", ex);
			} finally {
				closeConnection(con);
			}
		}
		return markets;
	}

	public static String getGroupName(long id) {
		for (int i = 0; i < groupsSI.size(); i++) {
			SelectItem si = groupsSI.get(i);
			Long l = (Long) si.getValue();
			if (id == l.longValue()) {
				return si.getLabel();
            }
		}
		return "group not found!!";
	}

	public static String getWriterName(long id) throws SQLException{
		Connection con = getConnection();
		try {
			return WritersDAO.getWriterName(con, id);
		} finally {
			closeConnection(con);
		}
	}

	public static Writer getWriter(long id) throws SQLException{
		Connection con = getConnection();
		try {
			return WritersDAO.get(con, id);
		} finally {
			closeConnection(con);
		}
	}

	public static ArrayList<SelectItem> getTransStatuses() {
		if(transStatuses == null) {
			Connection con = null;
			try {
				con = getConnection();	
				transStatuses = TransactionsDAOBase.getTransactionStatusesSI(con);
			}catch(SQLException ex) {
				logger.error("Can't lazzy load transStatuses", ex );
			} finally {
				closeConnection(con);
			}
		}
		return transStatuses;
	}

	public static String getTranStatusName(long id) {
		for (int i = 0; i < transStatuses.size(); i++) {
			SelectItem si = transStatuses.get(i);
			if (si.getValue().equals(String.valueOf(id))) {
				return si.getLabel();
            }
		}
        logger.warn("Transaction status not found for id: " + id);
		return "transaction status not found!!";
	}

	public String getTimezone() {
		return CommonUtil.getProperty("timezone");
	}

	public ArrayList<SelectItem> getScheduledListSI() {
		ArrayList<SelectItem> l = new ArrayList<SelectItem>();
        l.add(new SelectItem(new Long(InvestmentsManagerBase.SCHEDULED_HOURLY),CommonUtil.getMessage("opportunities.templates.hourly", null)));
		l.add(new SelectItem(new Long(InvestmentsManagerBase.SCHEDULED_DAILY),CommonUtil.getMessage("opportunities.templates.daily", null)));
		l.add(new SelectItem(new Long(InvestmentsManagerBase.SCHEDULED_WEEKLY),CommonUtil.getMessage("opportunities.templates.weekly", null)));
		l.add(new SelectItem(new Long(InvestmentsManagerBase.SCHEDULED_MONTHLY),CommonUtil.getMessage("opportunities.templates.monthly", null)));
		l.add(new SelectItem(new Long(Opportunity.SCHEDULED_QUARTERLY), CommonUtil.getMessage("opportunities.templates.quarterly", null)));
		return l;
	}

//	public ArrayList getTransTypes() {
//		return transTypes;
//	}

	public ArrayList<SelectItem> getAllCreditCardTypes() {
		return CommonUtil.translateSI(allCreditCardTypes);
	}

	public ArrayList<SelectItem> getCreditCardTypes() {
		return CommonUtil.translateSI(creditCardTypes);
	}

	/**
	 * @return the creditCardTypesAo for anyOption
	 */
	public ArrayList<SelectItem> getCreditCardTypesAo() {
		return CommonUtil.translateSI(creditCardTypesAo);
	}

	/**
	 * @return the creditCardTypesAo for anyOption
	 */
	public ArrayList<SelectItem> getCreditCardTypesRegulation() {
		return CommonUtil.translateSI(creditCardTypesRegulation);
	}

    /**
     * @return the creditCardTypesAoChina for anyOption
     */
    public ArrayList getCreditCardTypesAoChina() {
        return CommonUtil.translateSI(creditCardTypesAoChina);
    }

	/**
	 * @return the days
	 */
	public ArrayList getDays() {
		return days;
	}

	/**
	 * @return the months
	 */
	public ArrayList getMonths() {
		return months;
	}

	/**
	 * @return the years
	 */
	public ArrayList getYears() {
		return years;
	}

	/**
	 * @return the cities
	 */
	public static Map<String, String> getCities() {
		if(cities == null) {
			Connection con = null;
			try {
				con = getConnection();	
				loadCities(con);
			}catch(SQLException ex) {
				logger.error("Can't lazzy load cities", ex );
			} finally {
				closeConnection(con);
			}
		}
		return cities;
	}

	public ArrayList getCreditCardYears() {
		return creditCardYears;
	}

	public ArrayList getMobilePref() {
		return mobilePref;
	}

	public static ArrayList<String> getMobilePrefPlain() {
		return mobilePrefPlain;
	}

	public ArrayList getPhonePref() {
		return phonePref;
	}

	public static ArrayList getPhonePrefStatic(){
		return phonePref;
	}

	public ArrayList getCurrencies() {
		return currencies;
	}

	public ArrayList getCitiesList() {
		return citiesList;
	}

	public ArrayList<SelectItem> getGroupsSI() {
		return groupsSI;
	}

	public ArrayList<SelectItem> getMarketsSI() {
		return marketsSI;
	}

	public ArrayList getHours() {
		return hours;
	}

	public ArrayList getMinutes() {
		return minutes;
	}

	public String getLoginUrl() {
		return CommonUtil.getProperty("login.url");
	}

	public  String getCssUrl() {
		if (isCalcalistGame()) {
			return CommonUtil.getProperty("css.url.cal");
		}
		return CommonUtil.getProperty("css.url");
	}

	public TimeZone getTimeZone() {
		return Calendar.getInstance().getTimeZone();
	}

	public ArrayList<MarketGroup> getMarketsGroup() {
		return marketsGroup;
	}

	/**
	 * @return the skinsList
	 */
	public static ArrayList<Skins> getSkinsList() {
		return skinsList;
	}
	
	public static ArrayList<Long> getWritersAllowedToViewCcFirst6Digits() {
		if (writersAllowedToViewCcFirst6Digits.isEmpty() || writersAllowedToViewCcFirst6Digits == null) {
			Connection con = null;
			try {
				con = getConnection();
				writersAllowedToViewCcFirst6Digits = WritersDAO.getAllActiveWritersAllowedToViewCcFirst6Digits(con);
			} catch (SQLException ex) {
				logger.error("Can't lazzy load writers allowed to view first 6 digits", ex);
			} finally {
				closeConnection(con);
			}
		}
		return writersAllowedToViewCcFirst6Digits;
	}

	/**
	 * @return the countryPhoneCode by country id
	 */
	public static String getCountryPhoneCodes(String countryId) {
		return countryPhoneCodes.get(countryId);
	}

	/**
	 * @return support phone by country id
	 */
	public static String getSupportPhoneByCountryId(String countryId) {
		return countrySupportPhones.get(countryId);
	}

	/**
	 * @return support fax by country id
	 */
	public static String getSupportFaxeByCountryId(String countryId) {
		return countrySupportFaxes.get(countryId);
	}

	/**
	 * @return the currenciesList
	 */
	public static ArrayList<Currency> getCurrenciesList() {
		return currenciesList;
	}

	/**
	 * Return currency from the currenciesList by id
     *
	 * @param currencyid currency id
	 * @return currency instance
	 */
	public static Currency getCurrencyById(long currencyId) {
		Currency currency = null;
		for (Currency c : currenciesList) {
			if (c.getId() == currencyId) {
				currency = c;
				break;
			}
		}
		return currency;
	}

	/**
	 * Return currency id from the currenciesList by code
     *
	 * @param currencyid currency id
	 * @return currency instance
	 */
	public static long getCurrencyIdByCode(String currencyCode) {
		long currencyId = 0;
		for (Currency c : currenciesList) {
			if (c.getCode().equals(currencyCode)) {
				currencyId = c.getId();
				break;
			}
		}
		return currencyId;
	}

	/**
	 * Return skin from the SkinsList by id
     *
	 * @param skinId skin id
	 * @return Skins instance
	 */
	public static Skins getSkinById(long skinId) {
		Skins skin = null;
		for (Skins s : skinsList) {
			if (s.getId() == skinId) {
				skin = s;
				break;
			}
		}
		return skin;
	}

	/**
	 * Return A2 from the CountryList by id
	 * @param countryId
	 * @return
	 */
	public static String getCountryA2Code(long countryId) {
		Country c = countries.get(countryId);
		if (c.isHaveUkashSite()){
			return c.getA2();
		}
		return null;
	}

	/**
	 * Get Cloned instance for Skins object
     *
	 * @param skinId skin id
	 * @return Cloned Object Skins
	 */
	public static Skins getNewSkinById(long skinId) {
		Skins cacheSkin = getSkinById(skinId);
		Skins skin = null;
		try {
			skin = (Skins) cacheSkin.clone();
		} catch (CloneNotSupportedException e) {
			logger.error("Cannot clone Skins instance! " + e);
		}
		return skin;
	}

	/**
	 * Build marketsHomePage list for each skin
	 * Build the lists only one time.
	 * note: we removed this initialization from the constructor because we need the Locale in session.
	 * @throws SQLException
	 */
//	public static void buildMarketsHomePageList() throws SQLException {
//		//TODO
//	    boolean needToBuild = false;
//	    for (Skins skin : skinsList) {
//	        if (skin.getSkinMarketsHomePageList() == null) {
//	            needToBuild = true;
//	            break;
//	        }
//	    }
//	    if (needToBuild) {
//	        Connection con = null;
//	        try {
//	            con = getConnection();
//	            for (Skins skin : skinsList) {
//	                ArrayList<SelectItem> assets = MarketsDAOBase.getMarketsForHomepageSelect(con, skin.getId());
//	                assets.add(0, new SelectItem("", CommonUtil.getMessage("investment.header.market.slosestToExp", null)));
//	                skin.setSkinMarketsHomePageList(assets);
//	            }
//	        } finally {
//	            closeConnection(con);
//	        }
//	    }
//	}

	/**
	 * Function that return the Locale from the Session.
	 * Web + Backend override this function by diferent implementation.
     *
	 * @return Locale object
	 * @throws SQLException
	 */
	public Locale getUserLocale() throws Exception {
		return new Locale(ConstantsBase.LOCALE_DEFAULT);
	}

	/**
	 * Function that return the skin id.
	 * Web override this function.
	 * note: Backend not override this function, then the skinId is 1 for etrader
	 * @return skin id
	 */
	public Long getSkinId() {
		return Skin.SKIN_ETRADER;
	}

	public static Long getSkinId(HttpSession session, HttpServletRequest request) {
        return Skin.SKIN_ETRADER;
    }

	public boolean getIsLive() {
		String property = CommonUtil.getProperty(ConstantsBase.CURRENT_SYSTEM , "false");
		return null != property && property.equals("live");
	}

    public static boolean isBackend() {
        return getAppSource().equalsIgnoreCase(ConstantsBase.APPLICATION_SOURCE_BACKEND);
    }

    public static boolean isWeb() {
        return getAppSource().equalsIgnoreCase(ConstantsBase.APPLICATION_SOURCE_WEB);
    }

	/**
	 * @return the languagesList
	 */
	public static ArrayList<Language> getLanguagesList() {
		return languagesList;
	}

	/**
	 * @return the languages
	 */
	public static ArrayList<SelectItem> getLanguages() {
		return languages;
	}

	public static String getAppSource() {
		return CommonUtil.getProperty(ConstantsBase.APPLICATION_SOURCE , "false");
	}

//	public static ArrayList<Market> getAssetIndex() {
//		return assetIndex;
//	}

	/**
	 * this method returns a default offset of GMT+2
	 * @return
	 */
	public String getUtcOffset(String utcOffset) {
		return CommonUtil.formatJSUTCOffsetToString(ConstantsBase.UTCOFFSET_DEFAULT);
	}

	/**
	 * this method returns a default offset of GMT+2
	 * @return
	 */
	public String getUtcOffset() {
		return CommonUtil.formatJSUTCOffsetToString(ConstantsBase.UTCOFFSET_DEFAULT);
	}

	public ArrayList<SelectItem> getStatesList(){
		if(states == null) {
			Connection con = null;
			try {
				con = getConnection();	
				states = CountryDAOBase.getStatesSI(con);
			}catch(SQLException ex) {
				logger.error("Can't lazzy load states", ex );
			} finally {
				closeConnection(con);
			}
		}
		return states;
	}

	public ArrayList<SelectItem> getStatesCodeList(){
		if(statesCode == null) {
			Connection con = null;
			try {
				con = getConnection();	
				statesCode = CountryDAOBase.getStatesCodeSI(con);
			}catch(SQLException ex) {
				logger.error("Can't lazzy load statesCode", ex );
			} finally {
				closeConnection(con);
			}
		}
		return statesCode;
	}

	/**
	 * @return the supportPhones
	 */
	public static ArrayList<SelectItem> getSupportCountries() {
		return supportCountries;
	}

    public static Country getCountry(long countryId) {
        return countries.get(countryId);
    }

    public static Language getLanguage(long languageId) {
        return languagesHash.get(languageId);
    }

	/**
	 * @return the gmtOffsets
	 */
	public ArrayList getGmtOffsets() {
		return gmtOffsets;
	}

	public long getQualificationPeriodBySkin() {
		long skinId = getSkinId();
		Skins s = getSkinById(skinId);
		return (s.getTierQualificationPeriod()/30);  // in months
	}


	/**
	 * Get cashToPoints rate for points after invest calculation
	 * @param skinId user skin
	 * @param currencyId user currency
	 * @return
	 */
	public static double getCashToPointsRate(long skinId, long currencyId) {
		double rate = 0;
		Skins skin = getSkinById(skinId);
		for (SkinCurrency c : skin.getSkinCurrenciesList()) {
			if (c.getCurrencyId() == currencyId) {
				rate = c.getCashToPointsRate();
				break;
			}
		}
		return rate;
	}

	public static double getPointToCashPercent(long points) {
		if(tierPointsConvertion == null) {
			Connection con = null;
			try {
				con = getConnection();	
				tierPointsConvertion = TiersDAOBase.getTierPointsConvertions(con);
			}catch(SQLException ex) {
				logger.error("Can't lazzy load tierPointsConvertion", ex );
			} finally {
				closeConnection(con);
			}
		}
		
		double per = 0;
		for (TierPointConversion t : tierPointsConvertion) {
			if (points >= t.getFromNumPoints() && points <= t.getToNumPoints()) {
				per = t.getPercent();
			}
		}
		return (per/100);
	}

	/**
	 * check if we have calcalistGame session attribute
	 * @return
	 */
	public boolean isCalcalistGame() {
		FacesContext context = FacesContext.getCurrentInstance();
		HttpSession session = (HttpSession) context.getExternalContext().getSession(true);
		String inGame = (String) session.getAttribute(ConstantsBase.CALCALIST_GAME);
		if (null != inGame && inGame.equals("true") ) {
			return true;
		}
		return false;
	}

	/**
	 * @return the retentionGmtOffsets
	 */
	public ArrayList<SelectItem> getRetentionGmtOffsets() {
		retentionGmtOffsets = new ArrayList<SelectItem>();
		retentionGmtOffsets.add(new SelectItem ("0","All"));
		retentionGmtOffsets.addAll(gmtOffsets);

		return retentionGmtOffsets;
	}

	/**
	 * @return the retentionCountries
	 */
	public static ArrayList<SelectItem> getRetentionCountries() {
		retentionCountries = new ArrayList<SelectItem>();
		retentionCountries.add(new SelectItem (new Long(0),"All"));
		//retentionCountries.addAll(countriesList);
		//countriesList
		return retentionCountries;
	}

    public static boolean isGotUpdateForWriterDepositTV1() {
    	if (null != depositedWritersTV1 && depositedWritersTV1.size() > 0) {
    		return true;
    	}
    	return false;
    }

    public static boolean isGotUpdateForWriterDepositTV2() {
    	if (null != depositedWritersTV2 && depositedWritersTV2.size() > 0) {
    		return true;
    	}
    	return false;
    }

	/**
	 * @return the depositedWritersTV1+2
	 */
	public static ArrayList<Writer> getDepositedWritersTV1() {
		return depositedWritersTV1;
	}

	public static ArrayList<Writer> getDepositedWritersTV2() {
		return depositedWritersTV2;
	}

	public static void addWriterAfterSuccessDeposit(Writer w) {
		synchronized (depositedWritersTV1) {
			depositedWritersTV1.add(w);
		}
		synchronized (depositedWritersTV2) {
			depositedWritersTV2.add(w);
		}
	}

	public static void removeWriterAfterHandleTV1() {
		synchronized (depositedWritersTV1) {
			if (depositedWritersTV1.size() > 0) {
				depositedWritersTV1.remove(0);
			} else {
				logger.warn("Trying to remove writer from depositors empty list!");
			}
		}
	}

	public static void removeWriterAfterHandleTV2() {
		synchronized (depositedWritersTV2) {
			if (depositedWritersTV2.size() > 0) {
				depositedWritersTV2.remove(0);
			} else {
				logger.warn("Trying to remove writer from depositors empty list!");
			}
		}
	}
	/**
	 * @return the countries
	 */
	public static HashMap<Long, Country> getCountries() {
		return countries;
	}

	/**
	 * @param countries the countries to set
	 */
	public static void setCountries(HashMap<Long, Country> countries) {
		ApplicationDataBase.countries = countries;
	}

	/**
	 * @return the popEntryHisStatusHash
	 */
	public static HashMap<Integer, PopulationEntryHisStatus> getPopEntryHisStatusHash() {
		
		if(popEntryHisStatusHash == null) {
			Connection con = null;
			try {
				con = getConnection();	
				popEntryHisStatusHash = PopulationsDAOBase.getPopEntryHistoryStatusHash(con);
			}catch(SQLException ex) {
				logger.error("Can't lazzy load popEntryHisStatusHash", ex );
			} finally {
				closeConnection(con);
			}
		}
		return popEntryHisStatusHash;
	}

	/**
	 * @return the populationEntryType
	 */
	public static HashMap<Integer, String> getPopulationEntryType() {
		if(populationEntryType == null) {
			Connection con = null;
			try {
				con = getConnection();	
				populationEntryType = PopulationsDAOBase.getPopulationEntryType(con);
			}catch(SQLException ex) {
				logger.error("Can't lazzy load populationEntryType", ex );
			} finally {
				closeConnection(con);
			}
		}
		return populationEntryType;
	}

	/**
	 * @param get affiliate key from dynamicParam
	 * @return
	 */
	public static long getAffIdFromDynamicParameter(String dynamicParam) {
		long affKey = -1;
		try {
			if (dynamicParam.startsWith(ConstantsBase.REFERPARTNER_PREFIX)) {
				affKey = Long.parseLong(dynamicParam.split(ConstantsBase.REFERPARTNER_DELIMITER)[1]);
			} else {
				affKey = Long.parseLong(dynamicParam.split(ConstantsBase.NETREFER_DELIMITER)[0]);
			}
		} catch (Exception e) {
			logger.info("Problem parsing affiliateId, dp: " + dynamicParam);
			affKey = -1;
		}
		return affKey;
	}

	public static boolean isNetreferCombination(long combId) {
		for (Skins skin : skinsList) {
			for (Long comb : skin.getNetreferCombinationId()) {
				if (comb.longValue() == combId) {
					return true;
				}
			}
		}
		return false;
	}
	
	public static boolean isReferpartnerCombination(long combId) {
		for (Skins skin : skinsList) {
			for (Long comb : skin.getReferpartnerCombinationId()) {
				if (comb.longValue() == combId) {
					return true;
				}
			}
		}
		return false;
	}

	/**
	 * @return the invLimitisGroupsMap
	 */
	public static HashMap<Long, InvestmentLimitsGroup> getInvLimitisGroupsMap() {
		if(invLimitisGroupsMap == null) {
			Connection con = null;
			try {
				con = getConnection();	
				invLimitisGroupsMap = InvestmentsDAOBase.getInvestmentLimitGroupsMap(con);
			}catch(SQLException ex) {
				logger.error("Can't lazzy load invLimitisGroupsMap", ex );
			} finally {
				closeConnection(con);
			}
		}
		return invLimitisGroupsMap;
	}

    /**
     * @return the banks
     */
    public static HashMap<Long, BankBase> getBanks() {
		if(banks == null) {
			Connection con = null;
			try {
				con = getConnection();	
				banks = BanksDAOBase.getBanks(con, ConstantsBase.BANK_TYPE_DEPOSIT, Skin.SKIN_BUSINESS_ETRADER);
				banksSI = CommonUtil.hashMap2SelectListString(banks, "getId", "getName");
			}catch(SQLException ex) {
				logger.error("Can't lazzy load banks", ex );
			} finally {
				closeConnection(con);
			}
		}
        return banks;
    }

    /**
     * @param banks the banks to set
     */
    public static void setBanks(HashMap<Long, BankBase> banks) {
        ApplicationDataBase.banks = banks;
    }

    /**
     * @return the banksSI translated and sorted
     */
    public ArrayList<SelectItem> getBanksSI() {
    	if(banksSI == null) {
    		getBanks();
    	}
        ArrayList<SelectItem> temp = new ArrayList<SelectItem>(banksSI);
        Collections.copy(temp, CommonUtil.translateSI(banksSI));
        Collections.sort(temp, new CommonUtil.selectItemComparator());
        return temp;
    }

    /**
     * @param banksSI the banksSI to set
     */
    public void setBanksSI(ArrayList<SelectItem> banksSI) {
        ApplicationDataBase.banksSI = banksSI;
    }

    /**
     * @return the bankBranches
     */
    public static ArrayList<BankBranches> getBankBranches() {
    		if(bankBranches == null) {
    			Connection con = null;
    			try {
    				con = getConnection();	
    				bankBranches = BanksDAOBase.getBanksBranches(con);
    			}catch(SQLException ex) {
    				logger.error("Can't lazzy load bankBranches", ex );
    			} finally {
    				closeConnection(con);
    			}
    		}
    		return bankBranches;
   	}

    /**
     * @param bankBranches the bankBranches to set
     */
    public static void setBankBranches(ArrayList<BankBranches> bankBranches) {
        ApplicationDataBase.bankBranches = bankBranches;
    }

    public static Long getSkinByLanguageCode(String langCode, boolean isRegulated) {
    	if (isRegulated) {
    		return languageCodesSkinsRegulated.get(langCode);
    	}
    	return languageCodesSkins.get(langCode);
    }

	public static ArrayList<ErrorCode> getClearingErrorCode() {
		if(clearingErrorCode == null) {
			Connection con = null;
			try {
				con = getConnection();	
				clearingErrorCode = ClearingDAO.getAllErrorCodes(con);
			}catch(SQLException ex) {
				logger.error("Can't lazzy load clearingErrorCode", ex );
			} finally {
				closeConnection(con);
			}
		}
		return clearingErrorCode;
	}

	public static void setClearingErrorCode(ArrayList<ErrorCode> clearingErrorCode) {
		ApplicationDataBase.clearingErrorCode = clearingErrorCode;
	}

	/**
	 * @return the opportunityProductTypes
	 */
	public static ArrayList<SelectItem> getOpportunityProductTypes() {

		if(opportunityProductTypes == null) {
			Connection con = null;
			try {
				con = getConnection();	
				opportunityProductTypes = OpportunitiesDAOBase.getOpportunityProductTypes(con);
			}catch(SQLException ex) {
				logger.error("Can't lazzy load opportunityProductTypes", ex );
			} finally {
				closeConnection(con);
			}
		}
		return opportunityProductTypes;
	}

	public String getCheckHeapMemorySize() {
		int mb = 1024*1024;

        try {
			//Getting the runtime reference from system
			Runtime runtime = Runtime.getRuntime();

			//Print used memory
			logger.debug("JVM statistics [MB] Used Memory:" + (runtime.totalMemory() - runtime.freeMemory()) / mb +
			    " Free Memory:" + runtime.freeMemory() / mb +
			    " Total Memory:" + runtime.totalMemory() / mb +
			    " Max Memory:" + runtime.maxMemory() / mb);
		} catch (RuntimeException e) {
			logger.debug("cant get JVM statistics", e);
		}


        return "";
	}

	/**
	 * @param skinId
	 * @return support email string by skin.
	 */
	public static String getSupportEmailBySkinId(long skinId) {
		String supportEmailString = "";

		supportEmailString = getSkinById(Skin.SKINS_DEFAULT).getSupportEmail();
		if (skinId > 0) {
			try {
				supportEmailString = getSkinById(skinId).getSupportEmail();
			} catch (Exception e) {
				logger.error("ERROR! try to get support email by skin", e);
			}
		}
		return supportEmailString;
	}

    /**
     * @return server name
     */
    public String getServerPrefixName() {
        return getServerPrefixNameStatic();
    }

	/**
	 * @return server name
	 */
	public static String getServerPrefixNameStatic() {
        return "SNID: " + ServerConfiguration.getInstance().getServerName();
    }

//	public static long getRegulatedSkinIdByNonRegulatedSkinId(long skinId) {
//		if (skinId == Skins.SKIN_ENGLISH) {
//			return Skins.SKIN_REG_EN;
//		} else if (skinId == Skins.SKIN_GERMAN) {
//			return Skins.SKIN_REG_DE;
//		}  else if (skinId == Skins.SKIN_SPAIN) {
//			return Skins.SKIN_REG_ES;
//		}  else if (skinId == Skins.SKIN_FRANCE) {
//			return Skins.SKIN_REG_FR;
//		}  else if (skinId == Skins.SKIN_ITALIAN) {
//			return Skins.SKIN_REG_IT;
//		} else {
//			return skinId;
//		}
//	}
	
	private static void loadCities(Connection con) throws SQLException {
		citiesListString = "";
		ArrayList<City> list = CitiesDAO.getAll(con);
		for (int i = 0; i < list.size(); i++) {
			City city = (City) list.get(i);
			cities.put(String.valueOf(city.getId()), city.getName());

			citiesList.add(new SelectItem(String.valueOf(city.getId()), city.getName()));

			if (city.getName().indexOf("\"") == -1) {
				citiesListString += "\"" + city.getName() + "\",";
            } else {
				citiesListString += "\"" + city.getName().replaceAll("\"", "\\\\\"") + "\",";
            }
		}
		citiesListString = citiesListString.substring(0, citiesListString.length() - 1);

	}

	static void reset(){
		bankBranches = null;
		banks = null;
		banksSI = null;
		cities = null; 
		clearingErrorCode = null;
		transStatuses = null;
		opportunityProductTypes = null;
		markets = null;
		states = null;
		statesCode = null;
		tierPointsConvertion = null;
		popEntryHisStatusHash = null;
		populationEntryType = null;
		invLimitisGroupsMap = null;
		banks = null;
		banksSI = null;
		bankBranches = null;
	}
	
	public static void resetSkinAssetIndexMarkets(){
        for (Skins skin : skinsList) {           
        	skin.setAssetIndexMarkets(null);
        	skin.setGroupAssetIndexMarket(null);
        }
	}
	
	public static void initSkinAssetIndexMarkets() throws SQLException {
		Connection con = null;
		try {
			con = getConnection();
			initAssetIndexMarkets(con);
		} finally {
			closeConnection(con);
		}
	}

	protected static void initAssetIndexMarkets(Connection con) throws SQLException {
		for (Skins skin : skinsList) {
			Map<Long, List<AssetIndexMarket>> groupAssetIndexMarket = new HashMap<Long, List<AssetIndexMarket>>();
			skin.setAssetIndexMarkets(MarketsManager.getAssetIndexMarkets(skin.getId(), groupAssetIndexMarket));
			skin.setGroupAssetIndexMarket(groupAssetIndexMarket);
		}
	}

	/**
	 * @return the platformsMap
	 */
	public static Map<Long, String> getPlatformsMap() {
		return platformsMap;
	}
	
	public static HashMap<Long, String> getPredefinedDepositAmount() {
		if (predefinedDepositAmount == null) {
				predefinedDepositAmount = GeneralManager.getPredefinedDepositAmount();
		}
		return predefinedDepositAmount;
	}

	public static ArrayList<TournamentLanguageTabs> getTournamentLangTabs() {
		if(tournamentLangTabs == null){
			tournamentLangTabs = TournamentManagerBase.getLangTabs();
		}
		return tournamentLangTabs;
	}

	/**
	 * @return the epgProductId
	 */
	public static Map<Key, Integer> getEpgProductIds() {
		return epgProductIds;
	}
	
	public static int getEpgProductId(int businessCaseId, int platformId) {
		return epgProductIds.get(new Key(businessCaseId, platformId));
	}
}