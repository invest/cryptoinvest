package il.co.etrader.bl_managers;

import il.co.etrader.bl_vos.ClearingPriority;
import il.co.etrader.dao_managers.ConfigDAO;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;

import org.apache.log4j.Logger;

import com.anyoption.common.managers.BaseBLManager;

public class ConfigManager extends BaseBLManager {
	
	private static Logger log = Logger.getLogger(ConfigManager.class);
	
	public static final String LIVEPAGE_GLOBE_UPDATE_INTERVAL_PARAM_NAME = "livepage_globe_update_interval";
	public static final String LIVEPAGE_GLOBE_MIN_AMOUNT_PARAM_NAME = "livepage_globe_min_amount";
	public static final String LIVEPAGE_GLOBE_QUEUE_TTL_PARAM_NAME = "livepage_globe_queue_ttl";
	public static final String LIVEPAGE_GLOBE_FAKE_MIN_AMOUNT_PARAM_NAME = "livepage_globe_fake_min_amount";
	public static final String LIVEPAGE_GLOBE_FAKE_MAX_AMOUNT_PARAM_NAME = "livepage_globe_fake_max_amount";
	public static final String REWARDS_ACTIVATE_CHANGE_TIER_JOB_NAME = "rewards_activate_change_tier_job";
	
	public static final long LIVEPAGE_GLOBE_UPDATE_INTERVAL_DEFAULT_VALUE = 5L; // seconds
	public static final long LIVEPAGE_GLOBE_MIN_AMOUNT_DEFAULT_VALUE = 500L; // cents
	public static final long LIVEPAGE_GLOBE_QUEUE_TTL_DEFAULT_VALUE = 10L; // seconds
	public static final long LIVEPAGE_GLOBE_FAKE_MIN_AMOUNT_DEFAULT_VALUE = 500L; // cents
	public static final long LIVEPAGE_GLOBE_FAKE_MAX_AMOUNT_DEFAULT_VALUE = 10000L; // cents
	public static final long REWARDS_ACTIVATE_CHANGE_TIER_JOB_ACTIVE_VALUE = 1; 
	public static final long REWARDS_ACTIVATE_CHANGE_TIER_JOB_NON_ACTIVE_VALUE = 0;
	


	public static HashMap<Long, Long> getPriorityOfProvider() throws SQLException{
		Connection conn = getConnection();
		try{
			return ConfigDAO.getPriorityOfProvider(conn);
		} finally {
			closeConnection(conn);
		}
	}

	public static long getDaysNumForFailures() throws SQLException{
		Connection conn = getConnection();
		try{
			return ConfigDAO.getDaysNumForFailures(conn);
		} finally {
			closeConnection(conn);
		}
	}

	public static long getFailuresNumConsecutive() throws SQLException{
		Connection conn = getConnection();
		try{
			return ConfigDAO.getFailuresNumConsecutive(conn);
		} finally {
			closeConnection(conn);
		}
	}
	
	public static boolean updateParameter(long keyId, long value) throws SQLException {
		Connection con = getConnection();
		try{
			return ConfigDAO.updateParameters(con, keyId, value);
		} finally {
			closeConnection(con);
		}
	}

	public static ArrayList<ClearingPriority> getPriorityToChange() throws SQLException{
		Connection conn = getConnection();
		try{
			return ConfigDAO.getPriorityToChange(conn);
		} finally {
			closeConnection(conn);
		}
	}
	
	public static void updateReroutingPriorities(ArrayList<ClearingPriority> list) throws SQLException{
		Connection conn = getConnection();
		try{
			for(ClearingPriority cp : list){
				ConfigDAO.updateParameters(conn, cp.getId(), cp.getGroupId());
			}			
		} finally {
			closeConnection(conn);
		}		
	}
	
	/**
	 * TODO JavaDoc
	 * 
	 * @param parameterName
	 * @return
	 * @throws SQLException
	 */
	public static String getParameter(String parameterName) throws SQLException {
		Connection conn = getConnection();
		try {
			return ConfigDAO.getParameter(parameterName, conn);
		} finally {
			closeConnection(conn);
		}
	}
	
	/**
	 * TODO JavaDoc
	 * 
	 * @param parameterName
	 * @param defaultValue
	 * @return
	 */
	public static Long getLongParameter(String parameterName, Long defaultValue) {
		try {
			return Long.parseLong(getParameter(parameterName));
		} catch(Exception e) {
			log.warn("Unable to load parameter, returning default value", e);
			return defaultValue;
		}
	}
	
	public static boolean updateParametersByKey(String key, long value) throws SQLException {
		Connection con = getConnection();
		try{
			return ConfigDAO.updateParametersByKey(con, key, value);
		} finally {
			closeConnection(con);
		}
	}
	
}
