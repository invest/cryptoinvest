package il.co.etrader.bl_managers;

import il.co.etrader.bl_vos.ProviderDistribution;
import il.co.etrader.dao_managers.DistributionDAO;
import il.co.etrader.distribution.DistributionHistory;
import il.co.etrader.distribution.DistributionType;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Map;

import com.anyoption.common.beans.Transaction;
import com.anyoption.common.managers.BaseBLManager;

public class DistributionManager extends BaseBLManager {
	
	/**
	 * Returns Map of all providers of certain type
	 * for example : all CUP providers
	 * 
	 * @param type - for example CUP 
	 * @return
	 * @throws SQLException
	 */
	public static Map<Long, Integer> getProvidersForDistribution( DistributionType type ) throws SQLException{
		Connection conn  = getConnection();
		try{
			return DistributionDAO.getProvidersMapForDistribution( conn, type.getValue());
		} finally {
			closeConnection(conn);
		}
	}

	/**
	 * get the clearing provider linked to this user 
	 * 
	 * @param userId the user assigned 
	 * @param type	- for example CUP 
	 * @return
	 * @throws SQLException
	 */
	public static Long getAssignedProvider(long userId, DistributionType type) throws SQLException {
		Connection conn = getConnection();
		try{
			return DistributionDAO.getAssignedProvider( conn, userId, type.getValue());	
		} finally {
			closeConnection(conn);
		}
	}
	
	/**
	 * get all clearing provider ids for which the user has failed transactions
	 * 
	 * @param userId
	 * @param type - for example CUP 
	 * @return
	 * @throws SQLException
	 */
	public static ArrayList<Long> getFailedProviderIds( long userId, DistributionType type) throws SQLException {
		Connection conn = getConnection();
		try{
			return DistributionDAO.getFailedProviderIds(conn, userId, type.getValue());
		} finally {
			closeConnection(conn);
		}
	}
	
	/**
	 * get the assigned percentage for the clearing providers of a certain type
	 * 
	 * @param type
	 * @return
	 * @throws SQLException
	 */
	public static ArrayList<ProviderDistribution> getProvidersListForDistribution(DistributionType type) throws SQLException {
		Connection conn = getConnection();
		try{
			return DistributionDAO.getProvidersListForDistribution(conn, type.getValue());
		} finally {
			closeConnection(conn);
		}
	}
	
	/**
	 * delete all links user <-> provider for a given provider and distribution type
	 * 
	 * @param providerId - clearing provider id
	 * @param type - i.e. CUP
	 * @return
	 * @throws SQLException
	 */
	public static boolean cleanUpDistribution(long providerId, DistributionType type) throws SQLException {
		Connection conn = getConnection();
		try{
			return DistributionDAO.cleanUpDistribution(conn, providerId, type.getValue());
		} finally {
			closeConnection(conn);
		}
	}
	
	/**
	 * Update distribution percentage for a provider
	 * 
	 * @param providerId - clearing provider 
	 * @param newPercentage
	 * @return
	 * @throws SQLException
	 */
	public static boolean updateClearingProvider(long providerId, int newPercentage) throws SQLException {
		Connection conn = getConnection();
		try{
			return DistributionDAO.updateClearingProvider(conn, providerId, newPercentage);
		} finally {
			closeConnection(conn);
		}
	}
	
	/**
	 * Update distribution history when the distribution percentages change
	 * 
	 * @param type 
	 * @param writerId
	 * @param providerCsv
	 * @param percentageCsv
	 * @throws SQLException
	 */
	public static void updateDistributionHistory(DistributionType type, long writerId, String providerCsv, String percentageCsv) throws SQLException {
		Connection conn = getConnection();
		try{
			DistributionDAO.updateDistributionHistory(conn, type.getValue(), writerId, providerCsv, percentageCsv);
		} finally {
			closeConnection(conn);
		}
	}
	
	/**
	 * Create link user to provider ( assign the user to a provider )
	 * 
	 * @param t
	 * @param type
	 * @throws SQLException
	 */
	public static void createUserToProvider(Transaction t, DistributionType type) throws SQLException {
		Connection conn = getConnection();
		try{
			DistributionDAO.createUserToProvider(conn, t.getId(), t.getUserId(), t.getClearingProviderId(), type.getValue());
		} finally {
			closeConnection(conn);
		}
	}
	
	/**
	 * Returns a list of provider ids from a given distribution type ( CUP ) 
	 * 
	 * @param type
	 * @return
	 * @throws SQLException
	 */
	public static ArrayList<Long> getProvidersForType(DistributionType type) throws SQLException {
		Connection conn = getConnection();
		try{
			return DistributionDAO.getProvidersForType(conn, type.getValue());
		} finally {
			closeConnection(conn);
		}
	}

	/**
	 * Returns who and when did the last change for the distribution
	 * 
	 * @param type
	 * @return
	 * @throws SQLException
	 */
	public static DistributionHistory getDistributionHistory(DistributionType type) throws SQLException {
		Connection conn = getConnection();
		try{
			return DistributionDAO.getDistributionHistory(conn, type.getValue());
		} finally {
			closeConnection(conn);
		}
	}
}
