package il.co.etrader.reward.change.tier;

import il.co.etrader.bl_managers.BonusManagerBase;
import il.co.etrader.bl_managers.ConfigManager;
import il.co.etrader.dao_managers.ConfigDAO;
import il.co.etrader.util.JobUtil;

import java.sql.Connection;
import java.util.ArrayList;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.RewardUser;
import com.anyoption.common.beans.Writer;
import com.anyoption.common.daos.RewardDAO;
import com.anyoption.common.managers.RewardUsersHistoryManager;
import com.anyoption.common.managers.RewardUsersManager;

/**
 * Rewards change tiers JOB
 * @author eranl
 *
 */
public class RewardsChangeTierJob extends JobUtil {
	private static Logger log = Logger.getLogger(RewardsChangeTierJob.class);
	private static int conCounter = 0;

	/**
	 * 
	 * @param args
	 * @throws Exception
	 */
	public static void main(String[] args) throws Exception {
		log.info("Reward Change Tier Job started.");
		Connection con = null;
		try {			
			propFile = args[0];
			con = getConnection();
			String status = ConfigDAO.getParameter(ConfigManager.REWARDS_ACTIVATE_CHANGE_TIER_JOB_NAME, con);
			if (status.equalsIgnoreCase(String.valueOf(ConfigManager.REWARDS_ACTIVATE_CHANGE_TIER_JOB_ACTIVE_VALUE))) { // run the job
				ArrayList<RewardUser> rewardUsersList = RewardDAO.getRewardUsersAfterTierChange(con);
				for (RewardUser ru : rewardUsersList) {
					RewardUser rewardUser =  RewardDAO.getRewardUserAfterTierChange(con, ru.getUserId());					
					if (rewardUser != null) { // if user found need to fix his tier id
						log.debug("Going to update tier id for userId:" + rewardUser.getUserId() + " exp pnts:" + rewardUser.getExperiencePoints());
						RewardUsersManager.updateUserExperiencePoints(con, rewardUser, rewardUser.getExperiencePoints(), Writer.WRITER_ID_AUTO, BonusManagerBase.class, 
								"Reward Change Tier Job", RewardUsersHistoryManager.FIX_EXPERIENCE_POINTS);
					}
					if (++conCounter == 500){
	        			con.close();
	            		con = getConnection();
	            		conCounter = 0;
	            		log.info("reset connection");
	        		}										
				}	
				 ConfigDAO.updateParametersByKey(con, ConfigManager.REWARDS_ACTIVATE_CHANGE_TIER_JOB_NAME, 
						ConfigManager.REWARDS_ACTIVATE_CHANGE_TIER_JOB_NON_ACTIVE_VALUE); 
				
				
			}
		} catch (Exception e) {
			log.error("Error while trying to update reward user", e);
		} finally {
			con.close();
		} 
		log.info("Reward Change Tier Job ended.");
	}

}