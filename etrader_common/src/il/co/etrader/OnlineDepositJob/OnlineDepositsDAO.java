package il.co.etrader.OnlineDepositJob;

import il.co.etrader.bl_managers.TransactionsManagerBase;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.anyoption.common.beans.Transaction;
import com.anyoption.common.daos.DAOBase;

public class OnlineDepositsDAO extends DAOBase {

	/**
	 * Get online transaction in status 'authenticate'
	 * @param conn db connection
	 * @return
	 * @throws SQLException
	 */
    public static ArrayList<Transaction> getAuthenticateTrx(Connection conn) throws SQLException {
        PreparedStatement ps = null;
        ResultSet rs = null;
        ArrayList<Transaction> list = new ArrayList<Transaction>();
        try {
            String sql =
                "SELECT " +
                    "* " +
                "FROM " +
                    "transactions t, " +
                    "transaction_types tt, " +
                    "transaction_statuses ts " +
                "WHERE " +
                	"t.type_id = tt.id AND " +
                	"t.status_id = ts.id AND " +
                	"tt.class_type = ? AND " +
                	"t.type_id <> ? AND " +
                	"(t.status_id = ? OR (t.status_id = ? AND t.type_id  = ?))AND " +
                	"to_char(t.time_created,'YYYYMMDD') >= '20100516' AND " +
                	"(SYSDATE - INTERVAL '31' MINUTE) >= t.time_created ";

            ps = conn.prepareStatement(sql);
            ps.setLong(1, TransactionsManagerBase.TRANS_CLASS_DEPOSIT);
            ps.setLong(2, TransactionsManagerBase.TRANS_TYPE_DIRECT_BANK_DEPOSIT);  // have own service job
            ps.setLong(3, TransactionsManagerBase.TRANS_STATUS_AUTHENTICATE);
            ps.setLong(4, TransactionsManagerBase.TRANS_STATUS_STARTED);
            ps.setLong(5, TransactionsManagerBase.TRANS_TYPE_CASHU_DEPOSIT);

            rs = ps.executeQuery();
            while (rs.next()) {
            	Transaction vo = new Transaction();
                vo.setId(rs.getLong("id"));
                vo.setUserId(rs.getLong("user_id"));
                vo.setAmount(rs.getLong("amount"));
                vo.setTypeId(rs.getLong("type_id"));
                vo.setTimeCreated(convertToDate(rs.getTimestamp("time_created")));
                vo.setUtcOffsetCreated(rs.getString("utc_offset_created"));
                list.add(vo);
            }
        } finally {
            closeResultSet(rs);
        }
        return list;
    }

    public static boolean isHaveSucceedTrx(Connection conn, long trxId, long userId) throws SQLException {
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            String sql =
                "SELECT " +
                    "1 " +
                "FROM " +
                    "transactions t, " +
                    "transaction_types tt " +
                "WHERE " +
                	"t.type_id = tt.id AND " +
                	"tt.class_type = ? AND " +
                	"t.user_id = ? AND " +
                	"t.id > ? ";

            ps = conn.prepareStatement(sql);
            ps.setLong(1, TransactionsManagerBase.TRANS_CLASS_DEPOSIT);
            ps.setLong(2, userId);
            ps.setLong(3, trxId);

            rs = ps.executeQuery();
            if (rs.next()) {
            	return true;
            }
        } finally {
            closeResultSet(rs);
        }
        return false;
    }
}
