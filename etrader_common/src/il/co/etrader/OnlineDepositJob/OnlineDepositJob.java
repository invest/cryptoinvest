package il.co.etrader.OnlineDepositJob;


import il.co.etrader.util.JobUtil;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import org.apache.log4j.Logger;

import com.anyoption.common.beans.Transaction;

public class OnlineDepositJob extends JobUtil {

	private static Logger log = Logger.getLogger(OnlineDepositJob.class);
	private static HashMap<Long, Transaction> pulledTransactions = new HashMap<Long, Transaction>();

	public static void main(String[] args) throws Exception {
		propFile = args[0];
		long diffSeconds = 0;
		long jobStart = System.currentTimeMillis();
		log.debug("Strat run online deposit job");
        try {
			OnlineDepositsManager.loadPopulatioHandlers();
		} catch (Exception e1) {
			log.fatal("Can't load population handlers." + e1);
			return;
		}
        ArrayList<Transaction> transactions = null;
        try {
			transactions = OnlineDepositsManager.getAuthenticateTrx();
		} catch (SQLException e) {
			log.error("Error! problem to pull authenticate trx from DB," + e);
		}

		// fill relevant transactions
		for (Transaction t : transactions) {
			insertToPulledList(t);
		}

		try {
			ArrayList<Long> needToRemoveList = new ArrayList<Long>();
            for (Iterator<Long> iter = pulledTransactions.keySet().iterator() ; iter.hasNext();) {
            	Long id = iter.next();
            	Transaction t = pulledTransactions.get(id);
        		log.debug("going to update trx: " + t.getId());
        		OnlineDepositsManager.finishTransaction(t);
        		needToRemoveList.add(new Long(t.getId()));
            }

            // remove trx
            for (Long id : needToRemoveList) {
            	pulledTransactions.remove(id);
            }
		} catch (Exception e) {
			log.error("Failed to proceed update! " + e);
		}
		long jobEnd = System.currentTimeMillis();
		diffSeconds = (jobEnd - jobStart) / (1000);
		log.debug("Finished online deposit job, time: " + diffSeconds + " sec");
	}

    /**
     * Insert into pulled transactions Hash
     * @param trxId transaction id for insert
     * @param timeCreated time created of the transation
     */
    public static void insertToPulledList(Transaction t) {
    	if (null == pulledTransactions.get(new Long(t.getId()))) {
    		pulledTransactions.put(new Long(t.getId()), t);
    	}
    }
}
