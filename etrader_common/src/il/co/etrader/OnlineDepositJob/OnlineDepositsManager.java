package il.co.etrader.OnlineDepositJob;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.hibernate.TransactionException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.anyoption.common.beans.Transaction;
import com.anyoption.common.beans.base.Skin;
import com.anyoption.common.clearing.ClearingException;
import com.anyoption.common.clearing.InatecClearingProvider;
import com.anyoption.common.daos.GeneralDAO;
import com.anyoption.common.managers.RewardUserTasksManager;
import com.anyoption.common.managers.RewardUserTasksManager.TaskGroupType;
import com.anyoption.common.managers.TransactionsManagerBase.TransactionSource;
import com.anyoption.common.util.AppsflyerEventSender;
import com.anyoption.common.util.ClearingUtil;

import il.co.etrader.bl_managers.BonusManagerBase;
import il.co.etrader.bl_managers.PopulationsManagerBase;
import il.co.etrader.bl_managers.TransactionsManagerBase;
import il.co.etrader.bl_vos.UserBase;
import il.co.etrader.bl_vos.Writer;
import il.co.etrader.dao_managers.TransactionsDAOBase;
import il.co.etrader.dao_managers.UsersDAOBase;
import il.co.etrader.population.PopulationHandlersException;
import il.co.etrader.util.CommonUtil;
import il.co.etrader.util.ConstantsBase;
import il.co.etrader.util.JobUtil;

public class OnlineDepositsManager extends JobUtil{
	private static Logger log = Logger.getLogger(OnlineDepositsManager.class);
	private static HashMap<Long, String> skinName;
	private static HashMap<Long, String> currencySymbol;
	private static HashMap<Long, String> transactionStatus;
	private static final String TAB = " <br/> ";
	/**
	 * Get online authenticate transactions
	 * @return ArrayList of <Transaction>
	 * @throws SQLException
	 */
    public static ArrayList<Transaction> getAuthenticateTrx() throws SQLException {
        ArrayList<Transaction> l = null;
        Connection con = null;
        try {
            con = getConnection();
            l = OnlineDepositsDAO.getAuthenticateTrx(con);
        } finally {
			try {
				con.close();
			} catch (Exception e) {
				log.log(Level.ERROR,"Can't close",e);
			}
        }
        return l;
    }


	/**
	 * Load population handlers
	 * @throws PopulationHandlersException
	 */
    public static void loadPopulatioHandlers() throws PopulationHandlersException {
        Connection con = null;
        try {
            con = getConnection();
            PopulationsManagerBase.loadPopulatioHandlers(con);
        } catch (PopulationHandlersException phx) {
        	throw(phx);
		} catch (Throwable t) {
            throw new PopulationHandlersException("Error loading population handlers.", t);
        } finally {
			try {
				con.close();
			} catch (Exception e) {
				log.log(Level.ERROR,"Can't close",e);
			}
        }
    }

    public static void finishTransaction(Transaction t) throws ClearingException, SQLException, Exception  {
    	Connection con = null;
    	UserBase user = null;
        boolean deltaPayApproved = false;
    	fillSkinsDisplayName();
    	boolean deltapaySuccessAndUpdate = false;
    	try {
            con = getConnection();
            try {
            	log.debug("About to get user by user id: " + t.getUserId());
            	user = getUserByUserId(con, t.getUserId());
            } catch (Exception e) {
            	log.error("Problem with getting user from db", e);
            }

        	log.debug("Transaction id: " + t.getId() + ", Transaction type: " + t.getTypeId());
        	if (t.getTypeId() == TransactionsManagerBase.TRANS_TYPE_CUP_CHINA_DEPOSIT) {
        		deltaPayApproved = isDeltaPayApproved(t);
        		log.debug("Transaction deltapay id: " + t.getId());
        		try {
        			TransactionsManagerBase.afterDepositAction(user, t, TransactionSource.TRANS_FROM_JOB);
					if(deltaPayApproved) {
						con.setAutoCommit(false);
						log.debug("DeltaPay Approve");
			        	t.setStatusId(TransactionsManagerBase.TRANS_STATUS_SUCCEED);
						t.setComments("1000| " + "Permitted transaction |" );
						PopulationsManagerBase.deposit(con, user, true, 0, t);
						TransactionsManagerBase.afterTransactionSuccess(con, t.getId(), user.getId(), t.getAmount(), t.getWriterId(), 0L);
						UsersDAOBase.addToBalance(con, t.getUserId(), t.getAmount());
						GeneralDAO.insertBalanceLog(con, t.getWriterId(), t.getUserId(), ConstantsBase.TABLE_TRANSACTIONS, t.getId(),
								ConstantsBase.LOG_BALANCE_DELTA_PAY_CHINA_DEPOSIT, t.getUtcOffsetCreated());
						TransactionsDAOBase.updateDeltaPayTransaction(con, t.getId() ,t.getAuthNumber(), t.getStatusId(), t.getComments(), t.getUtcOffsetSettled());
						log.debug("About to send approved email to user: " + user.toString());
						sentEmailWithTransactionDetails(user, t, "Online Delta pay approved deposit", properties.getProperty("email.to.ao.zh"));
						con.commit();
						con.setAutoCommit(true);
						deltapaySuccessAndUpdate = true;
					}
        		} catch (Exception e) {
        			con.rollback();
        			con.setAutoCommit(true);
					e.printStackTrace();
				}
        	}

			if (user != null && !deltaPayApproved) {
	     		t.setStatusId(TransactionsManagerBase.TRANS_STATUS_FAILED);
	    		t.setDescription(null);
	    		t.setComments("authenticateService");
	    		String to = "";
	    		String subject = "";
	            if (!CommonUtil.isHebrewSkin(user.getSkinId())) {
	            	if (user.getSkinId() == Skin.SKIN_CHINESE) {
	            		subject = "Online Delta pay failed deposit";
	            		to = properties.getProperty("email.to.ao.zh");
	            	} else {
	            		subject = "Online failed deposit";
	            		to = properties.getProperty("email.to.ao");
	            	}
	            } else {
	            	subject = "Online failed deposit " +
	    	 		InatecClearingProvider.getMessageClass(t.getPaymentTypeId());
	            	to = properties.getProperty("email.to.et");
	            }
				try {
					PopulationsManagerBase.deposit(con, user, false, 0, t);
				} catch (Exception e) {
					throw new PopulationHandlersException("Problem with population failed deposit event ", e);
				}
	        	try {
	        		TransactionsDAOBase.updatePurchase(con, t.getId(), t.getStatusId(), t.getDescription(), t.getComments(), null, t.getUtcOffsetCreated(), Writer.WRITER_ID_AUTO);
	        	} catch (Exception e){
	        		throw new TransactionException("Problem with updating transaction ", e);
	        	}
	            log.debug("About to send failed email to user: " + user.toString());
	            if (user.getClassId() != ConstantsBase.USER_CLASS_TEST) {
		            sentEmailWithTransactionDetails(user, t, subject, to);
	            }
			}
    	} finally {
			try {
				con.close();
			} catch (Exception e) {
				log.log(Level.ERROR,"Can't close",e);
			}
    	}
    	if (deltapaySuccessAndUpdate) {
    		// Reward plan
    		int countRealDeposit = 0;
    		try {
            	countRealDeposit = TransactionsManagerBase.countRealDeposit( t.getUserId());
            	if (countRealDeposit == ConstantsBase.NUM_OF_REAL_SUCCESS_DEPOSIT) { // check if this first deposit
            		RewardUserTasksManager.rewardTasksHandler(TaskGroupType.FIRST_TIME_DEPOSIT_AMOUNT,  t.getUserId(),  
            				t.getAmount(),   t.getTypeId(), BonusManagerBase.class, (int) t.getWriterId());
            	}
            } catch (Exception e) {
                log.error(ConstantsBase.REWARD_PLAN_LOG_PREFIX + " RewardUserTasksManager exception after success transaction", e);
            }
    		
    		try {
    			if (countRealDeposit == ConstantsBase.NUM_OF_REAL_SUCCESS_DEPOSIT) {
    				new AppsflyerEventSender(t.getId(), ConstantsBase.SERVER_PIXEL_TYPE_FIRST_DEPOSIT).run();
    			}
    		} catch (Exception e1) {
    			log.error("cant run appsflyer event sender", e1);
    		}
    	}   
    }

	public static void fillSkinsDisplayName() {
		skinName = new HashMap<Long, String>();
		skinName.put(new Long(1), "Etrader");
		skinName.put(new Long(2), "AO-EN");
		skinName.put(new Long(3), "AO-TR");
		skinName.put(new Long(4), "AO-AR");
		skinName.put(new Long(5), "AO-ES");
		skinName.put(new Long(6), "ES-MI");
		skinName.put(new Long(7), "EN-GR");
		skinName.put(new Long(8), "AO-DE");
		skinName.put(new Long(9), "AO-IT");
		skinName.put(new Long(10), "AO-RU");
		skinName.put(new Long(12), "AO-FR");
		skinName.put(new Long(13), "EN-US");
		skinName.put(new Long(14), "ES-US");
		skinName.put(new Long(15), "AO-ZH");

		currencySymbol = new HashMap<Long, String>();
		currencySymbol.put(new Long(1), "ILS");
		currencySymbol.put(new Long(2), "USD");
		currencySymbol.put(new Long(3), "EUR");
		currencySymbol.put(new Long(4), "GBP");
		currencySymbol.put(new Long(5), "TRY");
		currencySymbol.put(new Long(6), "RUB");
		currencySymbol.put(new Long(7), "CNY");

		transactionStatus = new HashMap<Long, String>();
		transactionStatus.put(new Long(1), "started");
		transactionStatus.put(new Long(2), "succeed");
		transactionStatus.put(new Long(3), "failed");
		transactionStatus.put(new Long(4), "requested");
		transactionStatus.put(new Long(5), "canceled");
		transactionStatus.put(new Long(6), "reverse");
		transactionStatus.put(new Long(7), "pending");
		transactionStatus.put(new Long(8), "canceled bye trader");
		transactionStatus.put(new Long(9), "approved");
		transactionStatus.put(new Long(10), "authenticate");
		transactionStatus.put(new Long(11), "in progress");
		transactionStatus.put(new Long(12), "canceled by frauds");
		transactionStatus.put(new Long(13), "succeed deposit");
		transactionStatus.put(new Long(14), "charge back");
		transactionStatus.put(new Long(15), "cancel sm");
		transactionStatus.put(new Long(16), "cancel mnr");
	}

	public static void sentEmailWithTransactionDetails(UserBase user, Transaction t, String subject, String to) {
        Hashtable<String, String> serverProperties = new Hashtable<String, String>();
        serverProperties.put("url", properties.getProperty("email.url"));
        serverProperties.put("auth", properties.getProperty("email.auth"));
        serverProperties.put("user", properties.getProperty("email.user"));
        serverProperties.put("pass", properties.getProperty("email.pass"));
        // send mail to support
		// collect the parameters we neeed for the email
        Hashtable<String, String> emailProperties = new Hashtable<String, String>();
        emailProperties.put("from", properties.getProperty("email.from"));
		if (properties.getProperty("send.to").equalsIgnoreCase("Live")){
	   		emailProperties.put("to", to);
		} else {
			emailProperties.put("to", properties.getProperty("send.to"));
		}
        emailProperties.put("subject", subject + " - " + skinName.get(user.getSkinId()));

        String body = "";
        if (!CommonUtil.isParameterEmptyOrNull(user.getUserName())) {
        	body += "User name: " + user.getUserName() + TAB;
        } else {
        	body += "User name: none" + TAB;
        }
        if (!CommonUtil.isParameterEmptyOrNull(user.getFirstName())) {
        	body += "User first name: " + user.getFirstName() + TAB;
        } else {
        	body +=  "User first name: none" + TAB;
        }
        if (!CommonUtil.isParameterEmptyOrNull(user.getLastName())) {
        	body += "User last name: " + user.getLastName() + TAB;
        } else {
        	body +=  "User last name: none" + TAB;
        }
        if (user.getId() > 0) {
        	body += "User id: " + user.getId() + TAB;
        } else {
        	body +=  "User id: none" + TAB;
        }
        if (t.getId() > 0) {
        	body += "Transaction id: " + t.getId() + TAB;
        } else {
        	body += "Transaction id: none" + TAB;
        }
        if (t.getAmount() > 0 && user.getCurrencyId() > 0){
        	if (!CommonUtil.isParameterEmptyOrNull(currencySymbol.get(user.getCurrencyId()))) {
        		body += "Transaction amount: " + (t.getAmount()/100) + " " + currencySymbol.get(user.getCurrencyId()) + TAB ;
        	} else {
        		body += "Transaction amount: none" + TAB;
        	}
        } else {
        	body += "Transaction amount: none" + TAB;
        }
        if (null != t.getTimeCreated()) {
        	body += "Transaction time: " + CommonUtil.getDateAndTimeFormat(t.getTimeCreated(), "Israel") + TAB;
        } else {
        	body += "Transaction time: none" + TAB;
        }
        Long statusId = t.getStatusId();
        body += "Transaction status: " + transactionStatus.get(statusId) + TAB;
        if (!CommonUtil.isParameterEmptyOrNull(t.getComments())) {
        	body +="Comment: " + t.getComments() + TAB;
        } else {
        	body +="Comment: none" + TAB;
        }
        if (!CommonUtil.isParameterEmptyOrNull(t.getDescription())) {
        	body += "Description: " + t.getDescription();
        } else {
        	body += "Description: none";
        }

        emailProperties.put("body", body);
		CommonUtil.sendEmail(serverProperties, emailProperties, null);
	}

	public static boolean isDeltaPayApproved(Transaction t) {
		try {
			String url = "https://backoffice.vixipay.com/api/transact.php";
			String body =
				"affiliate=VANYOAO" +
				"&processing_mode=transaction_settled" +
				"&redirect=" + properties.getProperty("deltapay.url") +
				"&order_id=" + t.getId();

			String response = ClearingUtil.executePOSTRequest(url, body);
			log.debug("Delta pay response: " + response);
	        Document respDoc = ClearingUtil.parseXMLToDocument(response);
	        Element root = (Element) ClearingUtil.getNode(respDoc.getDocumentElement(), "");
	        Hashtable<String, String> params = ClearingUtil.getListNode(root, "input", "name", "value");
	        String status = params.get("status");
	        log.debug("Return status: " + status);
	        if (status.equalsIgnoreCase("APPROVED")) {
	        	t.setAuthNumber(params.get("transaction_no"));
				return true;
	        }
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

	public static UserBase getUserByUserId(Connection con, long userId) throws SQLException {
		PreparedStatement ps = null;
    	ResultSet rs = null;
    	UserBase vo = null;
		try {
			String sql = "SELECT " +
		   		  			"* " +
		   		  		 "FROM " +
		   		  		 	"users u, users_active_data uad " +
		   		  		 "WHERE " +
		   		  		 	"u.id = uad.user_id " +
		   		  		 	"AND u.id = ? ";

			ps = con.prepareStatement(sql);
			ps.setLong(1, userId);
			rs = ps.executeQuery();

    		if ( rs.next() ) {
    			vo = new UserBase();
    			UsersDAOBase.getVOBasic(rs, vo);
    		}
    		return vo;
		} finally {
			try {
				rs.close();
			} catch (Exception e) {
				log.log(Level.ERROR,"Can't close",e);
			}
			try {
				ps.close();
			} catch (Exception e) {
				log.log(Level.ERROR,"Can't close",e);
			}
    	}
	}
}