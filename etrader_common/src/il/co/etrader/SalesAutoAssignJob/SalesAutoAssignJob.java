package il.co.etrader.SalesAutoAssignJob;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import com.anyoption.common.bl_vos.PopulationEntryBase;

import il.co.etrader.bl_managers.PopulationEntriesManagerBase;
import il.co.etrader.bl_managers.PopulationsManagerBase;
import il.co.etrader.bl_vos.MarketingCampaign;
import il.co.etrader.bl_vos.RankData;
import il.co.etrader.bl_vos.Skins;
import il.co.etrader.bl_vos.StatusData;
import il.co.etrader.bl_vos.UserRank;
import il.co.etrader.bl_vos.UserStatus;
import il.co.etrader.bl_vos.Writer;
import il.co.etrader.bl_vos.WritersSkin;
import il.co.etrader.population.PopulationHandlersException;
import il.co.etrader.util.ConstantsBase;
import il.co.etrader.util.JobUtil;


public class SalesAutoAssignJob extends JobUtil {
	private static Logger log = Logger.getLogger(SalesAutoAssignJob.class);

	private static HashMap<Long, ArrayList<Writer>> writersBySkin = null;
	private static HashMap<Long, Writer> representatives = null;
	private static ArrayList<PopulationEntryBase> unAssignedEntries = null;
	private static ArrayList<PopulationEntryBase> assignedEntries = null;
	private static HashMap<Long, RankData> unassignedEntriesByRank = new HashMap<Long, RankData>();
	private static HashMap<Long, HashMap<Long, RankData>> unassignEntriesBySkinHM = null;
	private static ArrayList<UserRank> ranksArray;
	private static ArrayList<UserStatus> statusArray;
	private static HashMap<Long, Skins> skinsHM;
	private static int numberOfSkins;

	//private static final int PRIORITY_CONVERSION_ABOVE_1000 		= 1;
	private static final int PRIORITY_SUM_DEPOSIT_ABOVE_1000 		= 1;
	private static final int PRIORITY_OPEN_WITHDRAW 		  		= 2;
	private static final int PRIORITY_BALANCE_ABOVE_MIN_INVESTMENT  = 3;
	private static final int PRIORITY_LAST_LOGIN_MORE_THAN_A_WEEK  	= 4;
	private static final int PRIORITY_CAMPAIGN_HIGH_PRIORITY 		= 5;

	private static final int CONVERSION_AMOUNT = 100000;
	private static final int SUM_DEPOSIT_MIN_AMOUNT = 100000;

	@SuppressWarnings("unchecked")
	public static void main(String[] args) throws Exception {
		propFile = args[0];
		log.info("Sales auto assign Job started.");
		unassignedEntriesByRank.put(new Long(0), new RankData());

		//default filters
		String populationTypes = ConstantsBase.EMPTY_STRING;
		long skinId = 0;
		long businessSkin = 0;
		long languageId = 0;
		long assignedWriterId = 0;
		long campaignId = 0;
		String countries = "0";
		GregorianCalendar gc = new GregorianCalendar();
		Date toDate = gc.getTime();
		gc.add(Calendar.YEAR, -5);
		Date fromDate = gc.getTime();
		int colorType = 0;
		boolean isAssigned = false;
		long userId = 0;
		String userName = null;
		long contactId = 0;
		long supportMoreOptionsType = 0;
		String timeZone = null;
		String priorityId = null;
		String excludeCountrries = "0";
		boolean hideNoAnswer = false;
		String callsCount = null;
		String countriesGroup = "0";
		boolean isNotCalledPops = false;
		String excludeAffiliates ="0";
		String includeAffiliates ="0";
		int verifiedUsers = 0;
		long lastSalesDepRepId = 0;
		int campaignPriority = 0;
		boolean isSpecialCare = false;
		Date fromLogin = null;
		Date toLogin = null;
		long lastLoginInXMinutes = 0;
		boolean madeDepositButdidntMakeInv24HAfter = false;
		boolean balanceBelowMinInvestAmount = false;
		long userStatusId = 0;
		long userRankId = 0;
		long populationDept = 0;
		boolean isFromJob = true;

		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		//get all available writers
		try {
			con = getConnection();
			representatives = PopulationEntriesManagerBase.getComaRepSI(con, 0);
		} catch (Exception e) {
			log.log(Level.ERROR, "Error while Retrieving representatives from Db.", e);
			return;
	    } finally {
        	con.close();
        }

		//get all assign entries
		getAllAssignedEntries(populationTypes, skinId, businessSkin, languageId, assignedWriterId, campaignId, countries, fromDate, toDate, colorType,
				isAssigned, userId, userName, contactId, supportMoreOptionsType, timeZone, priorityId, excludeCountrries, hideNoAnswer, callsCount,
				countriesGroup, isNotCalledPops, includeAffiliates, excludeAffiliates, verifiedUsers, lastSalesDepRepId, campaignPriority, isSpecialCare,
				fromLogin, toLogin, lastLoginInXMinutes, balanceBelowMinInvestAmount, madeDepositButdidntMakeInv24HAfter, userStatusId, populationDept, userRankId, isFromJob,
				ConstantsBase.ALL_FILTER_ID, ConstantsBase.ALL_FILTER_ID, ConstantsBase.ALL_PLATFORMS_FILTER_ID, ConstantsBase.ALL_FILTER_ID);


		// fill for each writer his ranks data (number of ranks and for each rank his number of statuses)
		try {
			for (PopulationEntryBase peb : assignedEntries) {
				long writerId = peb.getAssignWriterId();
				long rankId = peb.getUserRankId();
				long statusId = peb.getUserStatusId();
				long entrySkinId = peb.getSkinId();
				log.info("writerId:" + peb.getAssignWriterId() + " rankId=" + rankId + " statusId=" + statusId + " skinId=" + entrySkinId + " " +  peb.getCurrEntryId());
				if (representatives.get(writerId) == null) { //assigned representative not found
					continue;
				}
				if (rankId != 0) {
					WritersSkin ws = representatives.get(writerId).getWriterSkins().get(entrySkinId);
					if (ws != null) { //check for writer skin (writer can have skin, but they don't have any limit on that skin)
						RankData rd =  representatives.get(writerId).getWriterSkins().get(entrySkinId).getRanksData().get(rankId);
						if (rd == null) {
							RankData tmp = new RankData();
							representatives.get(writerId).getWriterSkins().get(entrySkinId).getRanksData().put(rankId, tmp);
							rd = tmp;
						}
						if (statusId != 0) {
							StatusData sd = representatives.get(writerId).getWriterSkins().get(entrySkinId).getRanksData().get(rankId).getStatusData().get(statusId);
							if (sd == null) {
								StatusData tmp = new StatusData();
								representatives.get(writerId).getWriterSkins().get(entrySkinId).getRanksData().get(rankId).getStatusData().put(statusId, tmp);
								sd = tmp;
							}
							sd.getStatusArray().add(peb);
							representatives.get(writerId).getWriterSkins().get(entrySkinId).getRanksData().get(rankId).getStatusData().get(statusId).setStatusArray(sd.getStatusArray());
						}
					}
				}
			}
		} catch (Exception e) {
			log.log(Level.ERROR, "Error while splitting assign entries", e);
			return;
		}


		//display writers and their limits
		try {
			for (Iterator<Long> iter = representatives.keySet().iterator(); iter.hasNext();) {
				Long writerId = iter.next();
				Writer w = representatives.get(writerId);
				for (Long iter2 : w.getWriterSkins().keySet()  ) {
					Long writerSkinId = iter2;
					WritersSkin ws =  w.getWriterSkins().get(writerSkinId);
					for (Long iter3 : ws.getRankStatusLimits().keySet()) {
						Long rankId = iter3;
						Double rankLimit = ws.getRankStatusLimits().get(rankId).getLimit();
						for (Long iter4 : ws.getRankStatusLimits().get(rankId).getStatusLimit().keySet()) {
							Long statusId = iter4;
	    					Double statusLimit = ws.getRankStatusLimits().get(rankId).getStatusLimit().get(statusId);
	    					long numberOfRanks = 0;
	    					if (w.getWriterSkins().get(ws.getSkinId()).getRanksData().get(rankId) != null) {
	    						numberOfRanks = w.getWriterSkins().get(ws.getSkinId()).getRanksData().get(rankId).getNumberOfRanks1();
	    						log.info("writerId=" + w.getId() + " writerUserName=" + w.getUserName() +  " skinId=" + ws.getSkinId() + " assignmentLimit=" + ws.getAssignLimit() +
	    								" rankId=" + rankId + " rankLimit=" + rankLimit + " statusId=" + statusId + " statusLimit=" + statusLimit + " numberOfRanks=" + numberOfRanks);
	    					}

						}
					}
				}
			}
		} catch (Exception e) {
			log.log(Level.ERROR, "Error while display writers limits", e);
			return;
		}

		//split representatives by skin
		try {
			writersBySkin = new HashMap<Long, ArrayList<Writer>>();
			for (Iterator<Long> iter = representatives.keySet().iterator(); iter.hasNext();) {
				Long writerId = iter.next();
				Writer w = representatives.get(writerId);
				for (Long iter2 : w.getWriterSkins().keySet()  ) {
					Long writerSkinId = iter2;
					long wsSkinId = w.getWriterSkins().get(writerSkinId).getSkinId();
					ArrayList<Writer> writersArray = writersBySkin.get(wsSkinId);
					if (writersArray == null) { //create writers array
						ArrayList<Writer> tmp = new ArrayList<Writer>();
						writersBySkin.put(wsSkinId, tmp);
						writersArray = tmp;
					}
					writersArray.add(w);
				}
			}
		} catch (Exception e) {
			log.log(Level.ERROR, "Error while splitting representatives by skin", e);
			return;
		}

		//create ranks, statuses and skins array
		createRankStatusSkinList(con, pstmt, rs);

		//get all unassigned entries
		getAllUnassignEntries(populationTypes, skinId, businessSkin, languageId, assignedWriterId, campaignId, countries, fromDate, toDate, colorType,
				isAssigned, userId, userName, contactId, supportMoreOptionsType, timeZone, priorityId, excludeCountrries, hideNoAnswer, callsCount, countriesGroup,
				isNotCalledPops, includeAffiliates, excludeAffiliates, verifiedUsers, lastSalesDepRepId, campaignPriority, isSpecialCare, fromLogin, toLogin,
				lastLoginInXMinutes, balanceBelowMinInvestAmount, madeDepositButdidntMakeInv24HAfter, userStatusId, populationDept, userRankId, isFromJob,
				ConstantsBase.ALL_FILTER_ID, ConstantsBase.ALL_FILTER_ID, ConstantsBase.ALL_PLATFORMS_FILTER_ID, ConstantsBase.ALL_FILTER_ID);


			log.debug("Start splitting unassigned entries by rank and status");
			unassignEntriesBySkinHM = new HashMap<Long, HashMap<Long,RankData>>();
			try {
				for (PopulationEntryBase peb : unAssignedEntries) {
					long rankId = peb.getUserRankId();
					long statusId = peb.getUserStatusId();
					long entrySkinId = peb.getSkinId();
					StatusData sd = unassignedEntriesByRank.get(rankId).getStatusData().get(statusId);
					if (sd == null) { //create status
						StatusData tmp = new StatusData();
						unassignedEntriesByRank.get(rankId).getStatusData().put(statusId, tmp);
						sd = tmp;
					}
					sd.getStatusArray().add(peb);

					//fill ranks by skins hashmap
					HashMap<Long, RankData> rankDataHM = unassignEntriesBySkinHM.get(entrySkinId);
					if (rankDataHM == null) {
						HashMap<Long, RankData> tmp = new HashMap<Long, RankData>();
						unassignEntriesBySkinHM.put(entrySkinId, tmp);
						rankDataHM = tmp;
					}
					RankData rd = rankDataHM.get(rankId);
					if (rd == null) {
						RankData tmp = new RankData();
						rankDataHM.put(rankId, tmp);
						rd = tmp;
					}
					StatusData sd2 = rd.getStatusData().get(statusId);
					if (sd2 == null) {
						StatusData tmp = new StatusData();
						rd.getStatusData().put(statusId, tmp);
						sd2 = tmp;
					}
					sd2.getStatusArray().add(peb);
				}
				log.debug("End splitting unassigned entries by rank and status");
			} catch (Exception e) {
				log.log(Level.ERROR, "Error while splitting unassigned entries by rank and status", e);
				return;
			}

			//create report before assign
			StringBuffer report = createReport("BEFORE ASSIGN", true);


			//assign
			try {
				for (Long iter : unassignedEntriesByRank.keySet()) {
					Long rankId = iter;
					RankData rd = unassignedEntriesByRank.get(rankId);
					for (Long iter2 : rd.getStatusData().keySet()) {
						Long statusId = iter2;
						StatusData sd =  rd.getStatusData().get(statusId);
						//Priorities
						for (int p = 1 ; p < 7; p++) {
							for (PopulationEntryBase peb : sd.getStatusArray()) {

								switch (p) {
//									case PRIORITY_CONVERSION_ABOVE_1000:
//										// #1 conversion > 1000
//										if (peb.getConversion() > CONVERSION_AMOUNT) {
//											assign(peb);
//										}
//										break;
									case PRIORITY_SUM_DEPOSIT_ABOVE_1000:
										// #1 sum deposit > 1000 USD (all amount are converted to base amount)
										if (peb.getSumDepositsBaseAmount() > SUM_DEPOSIT_MIN_AMOUNT) {
											assign(peb, "PRIORITY_SUM_DEPOSIT_ABOVE_1000");
										}
										break;								
									case PRIORITY_OPEN_WITHDRAW:
										// #2 insert open withdraw
										if (peb.getCurrPopualtionTypeId() == PopulationsManagerBase.POP_TYPE_OPEN_WITHDRAW) {
											assign(peb, "PRIORITY_OPEN_WITHDRAW");
										}
										break;
									case PRIORITY_BALANCE_ABOVE_MIN_INVESTMENT:
										// #3 Balance > min investment amount, no investments in 24 hours
										if (!peb.isBalanceBelowMinInvestAmount() && peb.isMadeDepositButdidntMakeInv24HAfter()) {
											assign(peb, "PRIORITY_BALANCE_ABOVE_MIN_INVESTMENT");
										}
										break;
									case PRIORITY_LAST_LOGIN_MORE_THAN_A_WEEK:
										// #4 last login < today-7 days (more than a week passed since the last login)
										GregorianCalendar gc1 = new GregorianCalendar();
										gc1.add(GregorianCalendar.WEEK_OF_MONTH, -1);
										Date oneWeekAgo = gc1.getTime();
										if (peb.getTimeLastLogin() != null && peb.getTimeLastLogin().before(oneWeekAgo)) {
											assign(peb, "PRIORITY_LAST_LOGIN_MORE_THAN_A_WEEK");
										}
										break;
									case PRIORITY_CAMPAIGN_HIGH_PRIORITY:
										// #5 customers from marketing campaign/affiliates  high priority
										if (peb.getCampaignPriorityId() == MarketingCampaign.CAMPAIGN_PRIORITY_HIGH) {
											assign(peb, "PRIORITY_CAMPAIGN_HIGH_PRIORITY");
										}
										break;
									default: //other
										assign(peb, "OTHER");
										break;
									}
							}
						}
					}
				}
			} catch (Exception e) {
				log.log(Level.ERROR, "Error while assign entries by rank", e);
			}

			//create report after assign
			report.append(createReport("AFTER ASSIGN", false).toString());

			 // build the report
	        String reportBody = "<html><body>" +
	        						report.toString() +
	        					"</body></html>";


	    //  send email
        boolean sendEmail = Boolean.valueOf(getPropertyByFile("send.email", "false"));
        if (sendEmail) {
		    Hashtable<String,String> server = new Hashtable<String,String>();
		    server.put("url", getPropertyByFile("email.url"));
		    server.put("auth", getPropertyByFile("email.auth"));
		    server.put("user", getPropertyByFile("email.user"));
		    server.put("pass", getPropertyByFile("email.pass"));
		    server.put("contenttype", getPropertyByFile("email.contenttype", "text/html; charset=UTF-8"));

		    Hashtable<String,String> email = new Hashtable<String,String>();
		    email.put("subject", getPropertyByFile("job.email.subject"));
		    email.put("to", getPropertyByFile("job.email.to"));
		    email.put("from", getPropertyByFile("job.email.from"));
		    email.put("body", reportBody);
		    sendEmail(server, email);
        }

		log.info("Sales auto assign Job completed.");
	}


	private static void getAllAssignedEntries(String populationTypes, long skinId, long businessSkin, long languageId,
			long assignedWriterId, long campaignId, String countries, Date fromDate,
			Date toDate, int colorType, boolean isAssigned, long userId, String userName, long contactId,
			long supportMoreOptionsType, String timeZone, String priorityId, String excludeCountrries,boolean hideNoAnswer,
			String callsCount, String countriesGroup, boolean isNotCalledPops, String includeAffiliates,
			String excludeAffiliates, int verifiedUsers, long lastSalesDepRepId, int campaignPriority, boolean isSpecialCare,
			Date fromLogin, Date toLogin, long lastLoginInXMinutes, boolean balanceBelowMinInvestAmount, boolean madeDepositButdidntMakeInv24HAfter,
			long userStatusId, long populationDept, long userRankId, boolean isFromJob, long retentionTeamId, long everCalled, String userPlatform,
			long countryGroupTierId) throws SQLException {

		Connection con = null;
		try {
			log.debug("Going to get all assign entries");
			isAssigned = true;
			con = getConnection();
			assignedEntries = PopulationEntriesManagerBase.getAllEntries(con,
																	   populationTypes,
																	   skinId,
																	   businessSkin,
																	   languageId,
																	   assignedWriterId,
																	   campaignId,
																	   countries,
																	   fromDate,
																	   toDate,
																	   colorType,
																	   isAssigned,
																	   userId,
																	   userName,
																	   contactId,
																	   supportMoreOptionsType,
																	   timeZone,
																	   priorityId,
																	   excludeCountrries,
																	   hideNoAnswer,
																	   callsCount,
																	   countriesGroup,
																	   isNotCalledPops,
																	   includeAffiliates,
																	   excludeAffiliates,
																	   verifiedUsers,
																	   lastSalesDepRepId,
																	   campaignPriority,
																	   isSpecialCare,
																	   fromLogin,
																	   toLogin,
																	   lastLoginInXMinutes,
																	   balanceBelowMinInvestAmount,
																	   madeDepositButdidntMakeInv24HAfter,
																	   userStatusId,
																	   populationDept,
																	   userRankId,
																	   isFromJob,
																	   retentionTeamId,
																	   everCalled,
																	   "",
																	   userPlatform,
																	   countryGroupTierId);


			log.info("found " + assignedEntries.size() + " entries that writers where assigned");
			} catch (Exception e) {
	    		log.log(Level.ERROR, "Error while Retrieving users from Db.", e);
	        } finally {
	        	con.close();
	        }
	}


	private static void getAllUnassignEntries(String populationTypes, Long skinId, long businessSkin, long languageId,
										long assignedWriterId, long campaignId, String countries, Date fromDate,
										Date toDate, int colorType, boolean isAssigned, long userId, String userName, long contactId,
										long supportMoreOptionsType, String timeZone, String priorityId, String excludeCountrries,boolean hideNoAnswer,
										String callsCount, String countriesGroup, boolean isNotCalledPops, String includeAffiliates,
										String excludeAffiliates, int verifiedUsers, long lastSalesDepRepId, int campaignPriority, boolean isSpecialCare,
										Date fromLogin, Date toLogin, long lastLoginInXMinutes, boolean balanceBelowMinInvestAmount, boolean madeDepositButdidntMakeInv24HAfter,
										long userStatusId, long populationDept, long userRankId, boolean isFromJob, long retentionTeamId, long everCalled, String userPlatform,
										long countryGroupTierId) throws SQLException {

		Connection con = null;
		try {
			log.debug("Going to get all unassign entries");
			isAssigned = false;
			con = getConnection();
			unAssignedEntries = PopulationEntriesManagerBase.getAllEntries(con,
																   populationTypes,
																   skinId,
																   businessSkin,
																   languageId,
																   assignedWriterId,
																   campaignId,
																   countries,
																   fromDate,
																   toDate,
																   colorType,
																   isAssigned,
																   userId,
																   userName,
																   contactId,
																   supportMoreOptionsType,
																   timeZone,
																   priorityId,
																   excludeCountrries,
																   hideNoAnswer,
																   callsCount,
																   countriesGroup,
																   isNotCalledPops,
																   includeAffiliates,
																   excludeAffiliates,
																   verifiedUsers,
																   lastSalesDepRepId,
																   campaignPriority,
																   isSpecialCare,
																   fromLogin,
																   toLogin,
																   lastLoginInXMinutes,
																   balanceBelowMinInvestAmount,
																   madeDepositButdidntMakeInv24HAfter,
																   userStatusId,
																   populationDept,
																   userRankId,
																   isFromJob,
																   retentionTeamId,
																   everCalled,
																   "",
																   userPlatform,
																   countryGroupTierId);

			log.info("Found " + unAssignedEntries.size() + " unassign entries");
			} catch (Exception e) {
				log.log(Level.ERROR, "Error while retrieving all unassign entries", e);
		    } finally {
	        	con.close();
	        }
	}

	public static StringBuffer createReport(String title, boolean isBeforeAssign) {
		StringBuffer report = new StringBuffer();
		report.append("<br/><b>" +  title + "</b><br/>");

		if (isBeforeAssign) {
			report.append("Found " + unAssignedEntries.size()   + " unassigned entries <br/>");
			String info = ConstantsBase.EMPTY_STRING;

			//unassigned entries information
			for (long r = 0; r <= ranksArray.size(); r++) {
				for (long s = 1; s < statusArray.size() ; s++) {
					if (unassignedEntriesByRank.get(new Long(r)).getStatusData().get(new Long(s)) != null) {
						info += "RankId=" + r + " statusId=" + s + " size=" + unassignedEntriesByRank.get(new Long(r)).getStatusData().get(new Long(s)).getStatusArray().size() + "\n";
					}
				}
			}
			log.info("Summarized all unassigned entries by ranks and stauses:\n" + info);


			long[][][] entriesArrayBySkin = new long[numberOfSkins + 2][ranksArray.size()][statusArray.size()];
			//unassigned entries information by skin
			info = ConstantsBase.EMPTY_STRING;
			for (Long iter : unassignEntriesBySkinHM.keySet()) {
				Long hmSkinId = iter;
				for (Long iter2 : unassignEntriesBySkinHM.get(hmSkinId).keySet()) {
					Long rankId = iter2;
					for (Long iter3 : unassignEntriesBySkinHM.get(hmSkinId).get(rankId).getStatusData().keySet()) {
						Long statusId = iter3;
						long arraySize = unassignEntriesBySkinHM.get(hmSkinId).get(rankId).getStatusData().get(statusId).getStatusArray().size();
						info += "SkinId=" + hmSkinId + " RankId=" + rankId + " statusId=" + statusId + " size=" + arraySize + "\n";
						if (rankId > 0 && statusId > 0) {
							entriesArrayBySkin[hmSkinId.intValue() - 1][rankId.intValue() - 1][statusId.intValue() - 1] = arraySize;
						}
					}
				}
			}
			log.info("Summarized all unassigned entries by ranks and stauses by skin:\n" + info);

			String s1 = ConstantsBase.EMPTY_STRING;
			s1 = "<table>" +
					"<tr>";
			for (Long iter : skinsHM.keySet()) {
				Long skinId = iter;
				String skinName = getStringFromProperty(skinsHM.get(new Long(skinId)).getDisplayName());
				if (skinName == null) { //remove unused skins
					continue;
				}
				s1 +="<td>" +
						"<table border=\"1\">" +
							"<tr>"	+
								"<td colspan=\"3\" >";
									s1+= "" + skinName  + "" +
								"</td>" +
							"</tr>" +
							"<tr>" +
								"<td>" +
									"Rank" +
								"</td>" +
								"<td>" +
									"Status" +
								"</td>" +
								"<td>" +
									"Entries" +
								"</td>" +
							"</tr>";
				String color = "";
				for (int rankId = 1; rankId <= ranksArray.size(); rankId++) {
					if (color.equalsIgnoreCase("C6E2FF")) {
						color = "FFFFFF";
					} else {
						color = "C6E2FF";
					}
					for (int statusId = 1; statusId <= statusArray.size(); statusId++) {
						long size = entriesArrayBySkin[skinId.intValue() - 1][rankId - 1][statusId - 1];
						s1 +="<tr style=\"background-color:" + color + ";\">" +
								"<td>" +
									"" + getStringFromProperty(ranksArray.get(rankId - 1).getName()) + "" +
								"</td>" +
								"<td>" +
									"" + getStringFromProperty(statusArray.get(statusId - 1).getName()) + "" +
								"</td>" +
								"<td>" +
									"" + size + "" +
								"</td>" +
							"</tr>";
					}
				}
			s1 += 	"</table>" +
				"</td>";
			}
			s1 +=	"</tr>" +
				"</table><br/><br/>";
			report.append(s1.toString());
		}



        report.append("<table border=\"1\" align=\"center\"> " +
				"<tr align=\"center\">" +
				"<td><b>RankId</b></td>" +
				"<td><b>Status</b></td>");

		for (Long iter : writersBySkin.keySet()) {
			Long id = iter;
			ArrayList<Writer> writersArray = writersBySkin.get(id);
			for (Writer w: writersArray) {
				report.append("<td><b>" + w.getUserName() + "(" + getStringFromProperty(skinsHM.get(new Long(id)).getDisplayName()) + ")" + " limit=" + w.getWriterSkins().get(id).getAssignLimit() + "</b></td>");
			}
		}
		report.append("</tr>");


		for (long r = 1 ; r <= ranksArray.size(); r++) {
			report.append("<tr align=\"center\">");
			report.append("<td style=\"color: RED;\">Rank" + r + "</td>");
			report.append("<td align=\"center\">" +
								"<table align=\"center\" width=\"100px\">" +
									"<tr><td style=\"color: GREEN;\">status1</td></tr>" +
									"<tr><td style=\"color: GREEN;\">status2</td></tr>" +
									"<tr><td style=\"color: GREEN;\">status3</td></tr>" +
								"</table>" +
							"</td>");
				for (Long iter : writersBySkin.keySet()) {
					Long skinIdLong = iter;
					ArrayList<Writer> writersArray = writersBySkin.get(skinIdLong);
					for (Writer w: writersArray) {
						report.append("<td align=\"center\">" +
										"<table align=\"center\" width=\"200px\">");
						for (long s = 1 ; s <= statusArray.size(); s++) {
							RankData rd = w.getWriterSkins().get(skinIdLong).getRanksData().get(r);
							long num = 0;
							if (rd != null) {
								StatusData sd = rd.getStatusData().get(s);
								if (sd != null) {
									num = sd.getStatusArray().size();
								}
							}
							double limit = w.getWriterSkins().get(skinIdLong).getAssignLimit() * w.getWriterSkins().get(skinIdLong).getRankStatusLimits().get(r).getLimit() * w.getWriterSkins().get(skinIdLong).getRankStatusLimits().get(r).getStatusLimit().get(s);
							report.append("<tr><td>" + num + "/" + displayAmuont(limit) + "</td></tr>");
						}
						report.append("</table>" +
								"</td>");
				}
			}
				report.append("</tr>");
		}
		report.append("</table>");

		return report;
	}

	/**
	 * Assign population entry to available writer for the writer with the least entries
	 * @param peb
	 * @return
	 * @throws SQLException
	 */
	private static boolean assign(PopulationEntryBase peb, String assignReason) throws SQLException {
		long entrySkinId = peb.getSkinId();
		long entryRankId = peb.getUserRankId();
		long entryStatusId = peb.getUserStatusId();
		if (entryRankId == 0 || entryStatusId == 0 || peb.getAssignWriterId() != 0) {
			return false;
		}
		log.trace("Going to assign entryId=" + peb.getCurrEntryId());
		ArrayList<Writer> writers = writersBySkin.get(entrySkinId);
		if (writers != null) {
			Collections.sort(writers, new RankStatusComparator(entrySkinId, entryRankId, entryStatusId));
			for(Writer w : writers) {
				if(w.isCanAssign(entrySkinId, entryRankId, entryStatusId)) {
					//assign
					Connection con = null;
					boolean successAssign = false;
					boolean realAssign = Boolean.valueOf(getPropertyByFile("assign", "false"));
					log.info("Going to assign entryId=" + peb.getCurrEntryId() + " to writer=" + w.getId() + " writerName=" + w.getUserName() + " skinId=" + entrySkinId +
							" rankId=" + entryRankId + " statusId=" + entryStatusId + " \n reason=" + assignReason);
					if (realAssign) {
						try {
							con = getConnection();
							peb.setAssignWriterId(w.getId());
							successAssign = PopulationEntriesManagerBase.assign(con, peb, Writer.WRITER_ID_AUTO);
						} catch (PopulationHandlersException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						} catch (SQLException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						} catch (Exception e) {
							log.log(Level.ERROR, "Error while Retrieving representatives from Db.", e);
					    } finally {
				        	con.close();
				        }
					} else {
						successAssign = true;
					}
					if (successAssign) {
						RankData writerRD = w.getWriterSkins().get(entrySkinId).getRanksData().get(entryRankId);
						if (writerRD == null) {
							RankData tmp = new RankData();
							w.getWriterSkins().get(entrySkinId).getRanksData().put(entryRankId, tmp);
							writerRD = tmp;
						}
						StatusData writerSD = writerRD.getStatusData().get(entryStatusId);
						if (writerSD == null) {
							StatusData tmp = new StatusData();
							writerRD.getStatusData().put(entryStatusId, tmp);
							writerSD = tmp;
						}
						writerSD.getStatusArray().add(peb);
						break;
					}
				}
			}
		} else { //no available writers were found
			log.info("No available writers were found =" + peb.getCurrEntryId() + " skinId=" + peb.getSkinId());
		}
		return true;

	}

	private static void createRankStatusSkinList(Connection con, PreparedStatement pstmt, ResultSet rs) throws SQLException {
		//create skins array
		skinsHM = new HashMap<Long, Skins>();
		try {
			con = getConnection();
			String sql = " SELECT " +
							" id," +
							" display_name " +
						 " FROM " +
						 	" skins " +
						 " ORDER BY " +
						 	" id ";
			pstmt = con.prepareStatement(sql);
			rs = pstmt.executeQuery();
			while(rs.next()) {
				Skins skin = new Skins();
				skin.setId(rs.getInt("id"));
				skin.setDisplayName(rs.getString("display_name"));
				skinsHM.put(rs.getLong("id"), skin);
				numberOfSkins++;
			}


		} catch (Exception e) {
			log.log(Level.ERROR, "Error while retrieving users skins from Db.", e);
	    } finally {
			try {
				rs.close();
			} catch (Exception e) {
				log.log(Level.ERROR,"Can't close",e);
			}
			try {
				pstmt.close();
			} catch (Exception e) {
				log.log(Level.ERROR,"Can't close",e);
			}
	    	con.close();
	    }

		//create ranks array
		ranksArray = new ArrayList<UserRank>();
		try {
			con = getConnection();
			String sql = " SELECT " +
							" id, " +
							" rank_name " +
						 " FROM " +
						 	" users_rank " +
						 " ORDER BY " +
						 	" id ";
			pstmt = con.prepareStatement(sql);
			rs = pstmt.executeQuery();
			while(rs.next()) {
				long id = rs.getLong("id");
				unassignedEntriesByRank.put(id, new RankData());
				UserRank ur = new UserRank();
				ur.setId(id);
				ur.setName(rs.getString("rank_name"));
				ranksArray.add(ur);
			}


		} catch (Exception e) {
			log.log(Level.ERROR, "Error while Retrieving users rank from Db.", e);
	    } finally {
			try {
				rs.close();
			} catch (Exception e) {
				log.log(Level.ERROR,"Can't close",e);
			}
			try {
				pstmt.close();
			} catch (Exception e) {
				log.log(Level.ERROR,"Can't close",e);
			}
	    	con.close();
	    }

		//create status array
		statusArray = new ArrayList<UserStatus>();
		try {
			con = getConnection();
			String sql = " SELECT " +
							" id," +
							" status_name " +
						 " FROM " +
						 	" users_status " +
						 " ORDER BY " +
						 	" id ";
			pstmt = con.prepareStatement(sql);
			rs = pstmt.executeQuery();
			while(rs.next()) {
				for (long i = 0 ; i < ranksArray.size(); i++ ) {
					unassignedEntriesByRank.get(i).getStatusData().put(rs.getLong("id"), new StatusData());
				}
				UserStatus us = new UserStatus();
				us.setId(rs.getLong("id"));
				us.setName(rs.getString("status_name"));
				statusArray.add(us);
			}


		} catch (Exception e) {
			log.log(Level.ERROR, "Error while retrieving users status from Db.", e);
	    } finally {
			try {
				rs.close();
			} catch (Exception e) {
				log.log(Level.ERROR,"Can't close",e);
			}
			try {
				pstmt.close();
			} catch (Exception e) {
				log.log(Level.ERROR,"Can't close",e);
			}
	    	con.close();
	    }
	}


	private static String displayAmuont(double amount) {
		DecimalFormat sd = new DecimalFormat("###########0.00");
		double amountDecimal = amount;
		return sd.format(amountDecimal);
	}

	private static String getStringFromProperty(String param) {
		return getPropertyByFile(param, null);
	}

}

