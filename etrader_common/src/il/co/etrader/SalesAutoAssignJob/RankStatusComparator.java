package il.co.etrader.SalesAutoAssignJob;

import java.util.Comparator;

import il.co.etrader.bl_vos.Writer;

public class RankStatusComparator implements Comparator {
	
	private long skinId;
	private long rankId;
	private long statusId;
	
	public RankStatusComparator(long skinId, long rankId, long statusId) {
		this.skinId = skinId;
		this.rankId = rankId;
		this.statusId = statusId;
	}
	
    public int compare(Object o1, Object o2) {
		Writer w1 = (Writer)o1;
		Writer w2 = (Writer)o2;
		return new Double(w1.assignsDivLimit(skinId, rankId, statusId)).compareTo(new Double(w2.assignsDivLimit(skinId, rankId, statusId)));
	}

 }
