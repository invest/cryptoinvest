package il.co.etrader.AffiliateChinesePixels;

import il.co.etrader.bl_managers.TransactionsManagerBase;
import il.co.etrader.util.CommonUtil;
import il.co.etrader.util.JobUtil;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Hashtable;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import com.anyoption.common.util.ClearingUtil;

public class AffiliateChinesePixelsJob extends JobUtil {
	private static Logger log = Logger.getLogger(AffiliateChinesePixelsJob.class);
	private static StringBuffer report 			 = new StringBuffer();

	private static final String USD_MIN_DEPOSIT	 = "78";
	private static final String USD_MIN_TURNOVER = "315";
	private static final String CAMPAIGN_ID 	 = "1225";
	private static 		 String URL 			 = "https://www.chinesean.com/affiliate/tracking3.do";

	public static void main(String[] args) throws Exception {
		propFile = args[0];
		log.info("Affiliate Chinese Pixels Job started.");

	    report.append("<br><b>Affiliate Chinese Pixels Job Information:</b><table border=\"1\">");
	    report.append("<tr><td><b>Users Id</b></td><td><b>User Name</b></td><td><b>Turnover</b></td><td><b>Turnover till yesterday</b></td><td><b>Sum deposit</b></td><td><b>CHINESEANTXID</b></td><td><b>send pixel</b></td></tr>");

		double minDeposit 	= Double.valueOf(getPropertyByFile("usd.min.deposit", USD_MIN_DEPOSIT));
		double minTurnover 	= Double.valueOf(getPropertyByFile("usd.min.turnover", USD_MIN_TURNOVER));
		int campaignId 		= Integer.valueOf(getPropertyByFile("campaign.id", CAMPAIGN_ID));
		boolean sendPixel   = Boolean.valueOf(getPropertyByFile("send.pixel", "false"));

		Connection con = null;
		ResultSet rs = null;
		PreparedStatement ps = null;

		try {
			con = getConnection();
			//get all users from chinese campaign that has
			String sql =" SELECT " +
						"	u.id user_id, " +
						"   u.user_name user_name, " +
						"	u.dynamic_param, " +
						"   turnover, " +
						"	CASE WHEN turnover_till_yesterday IS null THEN 0 ELSE turnover_till_yesterday END turnover_till_yesterday,  " +
						"   sum_deposit_usd " +
						" FROM " +
						"	users u " +
						"	RIGHT JOIN " +
						"		(SELECT " +
						"	 	  u.id user_id, " +
						"    	  SUM(t.amount * t.rate / 100) as sum_deposit_usd " +
						"		 FROM " +
						"		 	users u," +
						"			marketing_campaigns mcam, " +
						"			marketing_combinations mcomb, " +
						"			transactions t, " +
						"			transaction_types tt " +
						"		 WHERE " +
						"			u.class_id <> 0 " +
						"			AND mcam.id = mcomb.campaign_id " +
						"			AND mcomb.id = u.combination_id " +
						"			AND u.id = t.user_id " +
						"			AND t.type_id = tt.id " +
						"			AND t.status_id in (" + TransactionsManagerBase.SUCCESS_DEPOSITS_TYPES + ") " +
						"			AND tt.class_type = ? " +  // real deposit
						"			AND mcam.id = ? " +
						"   	 GROUP BY u.id) deposit on deposit.user_id = u.id " +
						"	LEFT JOIN " +
						"		(SELECT " +
						"			u.id user_id," +
						"			SUM(i.amount * i.rate/100) as turnover " +
						"		 FROM " +
						"			users u, " +
						"			marketing_campaigns mcam, " +
						"			marketing_combinations mcomb, " +
						"			investments i " +
						"		 WHERE " +
						"			u.class_id <> 0 " +
						"			AND u.id = i.user_id " +
						"			AND mcam.id = mcomb.campaign_id " +
						"			AND mcomb.id = u.combination_id " +
						"   	  	AND i.is_settled = 1 " +
						"			AND i.is_canceled = 0 " +
						"			AND mcam.id = ? " +
						"	 	GROUP BY u.id) user_turnover on user_turnover.user_id = u.id " +
						" 	LEFT JOIN " +
						"	(SELECT " +
						"		u.id user_id, " +
						"		SUM(i.amount * i.rate / 100) as turnover_till_yesterday " +
						"	 FROM " +
						"		users u, " +
						"		marketing_campaigns mcam, " +
						"		marketing_combinations mcomb, " +
						"		investments i " +
						"	 WHERE " +
						"		u.class_id <> 0 " +
						"		AND mcam.id = mcomb.campaign_id " +
						"		AND mcomb.id = u.combination_id " +
						"		AND u.id = i.user_id " +
						"		AND i.is_settled = 1 " +
						"		AND i.is_canceled = 0 " +
						"		AND i.time_settled < sysdate - 1 " +
						"		AND mcam.id = ? " +
						"	GROUP BY u.id) yesterday_turnover on yesterday_turnover.user_id = u.id ";

			ps = con.prepareStatement(sql);
			ps.setLong(1, TransactionsManagerBase.TRANSACTION_CLASS_TYPE_REAL_DEPOSIT);
			ps.setInt(2, campaignId);
			ps.setInt(3, campaignId);
			ps.setInt(4, campaignId);
			rs = ps.executeQuery();
			while (rs.next()) {
				boolean isPixelSent = false;
				long userId = rs.getLong("user_id");
				String userName = rs.getString("user_name");
				String chineseantxid  = rs.getString("dynamic_param");
				Double turnover = rs.getDouble("turnover");
				Double turnoverTillYesterday = rs.getDouble("turnover_till_yesterday");
				Double sumDepositUsd = rs.getDouble("sum_deposit_usd");
				log.info("UserId=" + userId + " userName=" + userName + " turnover=" + turnover + " turnoverTillYesterday=" + turnoverTillYesterday +
						 " sumDepositUsd=" + sumDepositUsd + " chineseantxid(dp)=" + chineseantxid);
				report.append("<tr>");
                report.append("<td>").append(userId).append("</td>");
                report.append("<td>").append(userName).append("</td>");
                report.append("<td>").append("$" + turnover).append("</td>");
                report.append("<td>").append("$" + turnoverTillYesterday).append("</td>");
                report.append("<td>").append("$" + sumDepositUsd).append("</td>");
                report.append("<td>").append(chineseantxid).append("</td>");
				if (sumDepositUsd >= minDeposit && turnover >= minTurnover && turnoverTillYesterday < minTurnover
						&& !CommonUtil.isParameterEmptyOrNull(chineseantxid) && sendPixel) {
					isPixelSent = sendS2SPixel(userId, chineseantxid);
				}
				report.append("<td>").append(isPixelSent).append("</td>");
				report.append("</tr>");
			}

		} catch (Exception e) {
			log.log(Level.ERROR, "Error while Retrieving representatives from Db.", e);
			return;
	    } finally {
			try {
				rs.close();
			} catch (Exception e) {
				log.log(Level.ERROR,"Can't close",e);
			}
			try {
				ps.close();
			} catch (Exception e) {
				log.log(Level.ERROR,"Can't close",e);
			}
	    	con.close();
        }

	    sendEmail();
		log.info("Affiliate Chinese Pixels Job ended.");
	}

	public static boolean sendS2SPixel(long userId, String chineseantxid) {
        try {
        	//pId=10781&tracking=REFID&cpa=&cpl=TIERIDACT&cps=&txId=CHINESEANTXID
        	String body =
        		"pId=10781" +
        		"&tracking=" + userId +
        		"&cpa=" +
        		"&cpl=TIERIDACT" +
        		"&cps=" +
        		"&txId=" + chineseantxid;
        	log.debug("Going to send S2S pixel to:" + URL + body);
            String response = ClearingUtil.executePOSTRequest(URL, body);
            log.debug("Response from server:" + response);
            if (response.equalsIgnoreCase("OK")) {
            	return true;
            }
        } catch (Exception e) {
        	log.error("Error while trying to send pixel" + e);
        }
        return false;
	}

	public static void sendEmail() {
		 // build the report
        String reportBody = "<html><body>" +
        						report.toString() +
        					"</body></html>";

	    //  send email
	    Hashtable<String,String> server = new Hashtable<String,String>();
	    server.put("url", getPropertyByFile("email.url"));
	    server.put("auth", getPropertyByFile("email.auth"));
	    server.put("user", getPropertyByFile("email.user"));
	    server.put("pass", getPropertyByFile("email.pass"));
	    server.put("contenttype", getPropertyByFile("email.contenttype", "text/html; charset=UTF-8"));

	    Hashtable<String,String> email = new Hashtable<String,String>();
	    email.put("subject", getPropertyByFile("job.email.subject"));
	    email.put("to", getPropertyByFile("job.email.to"));
	    email.put("from", getPropertyByFile("job.email.from"));
	    email.put("body", reportBody);
	    sendEmail(server, email);
	}

}

