package il.co.etrader.directPaymentsMail;


public class Transaction  {

	private long id;
	private long userId;
	private int typeId;
	private String type;
	private long amount;
	private String currency;
	private String timeCreated;
	private String timeSettled;
	private String status;
	private String directPaymentType;
	private String comments;


	public long getAmount() {
		return amount;
	}
	public void setAmount(long amount) {
		this.amount = amount;
	}
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getTimeCreated() {
		return timeCreated;
	}
	public void setTimeCreated(String timeCreated) {
		this.timeCreated = timeCreated;
	}
	public String getTimeSettled() {
		return timeSettled;
	}
	public void setTimeSettled(String timeSettled) {
		this.timeSettled = timeSettled;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public long getUserId() {
		return userId;
	}
	public void setUserId(long userId) {
		this.userId = userId;
	}
	public String getComments() {
		return comments;
	}
	public void setComments(String comments) {
		this.comments = comments;
	}
	public String getDirectPaymentType() {
		return directPaymentType;
	}
	public void setDirectPaymentType(String directPaymentType) {
		this.directPaymentType = directPaymentType;
	}
	public int getTypeId() {
		return typeId;
	}
	public void setTypeId(int typeId) {
		this.typeId = typeId;
	}
	public String getCurrency() {
		return currency;
	}
	public void setCurrency(String currency) {
		this.currency = currency;
	}



}
