package il.co.etrader.directPaymentsMail;

import il.co.etrader.bl_managers.TransactionsManagerBase;
import il.co.etrader.util.JobUtil;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.Hashtable;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

public class DirectPaymentsMailJob extends JobUtil{

	private static Logger log = Logger.getLogger(DirectPaymentsMailJob.class);

	private static String startDate;
	private static String endDate;
	private static ArrayList<Transaction> directPaymentsList;

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		if (null == args || args.length < 1) {
            log.log(Level.FATAL, "Configuration file not specified, insert properties file name.");
            return;
        }
        propFile = args[0];

        startDate = getPropertyByFile("directPayments.start.date");
        endDate = getPropertyByFile("directPayments.end.date");

        // If datea weren't found, ser start date to be sysdate -1 , and end date to be sysdate
        if (startDate.indexOf("?") != -1){
        	SimpleDateFormat spf = new SimpleDateFormat("yyyy-MM-dd");
        	GregorianCalendar calendar = new GregorianCalendar();

        	endDate = spf.format(calendar.getTime());

        	calendar.add(GregorianCalendar.DATE, -1);
        	startDate = spf.format(calendar.getTime());
        }

        if (log.isEnabledFor(Level.INFO)) {
            log.log(Level.INFO, "Starting the DirectPaymentsMailJob job.");
            log.log(Level.INFO, "Starting retrieve direct payments from DB.");
        }

        retrieveDirectPaymentList();

        if (log.isEnabledFor(Level.INFO)) {
            log.log(Level.INFO, "finished retrieve direct payments from DB.");
            log.log(Level.INFO, "Starting sending Email.");
        }

        sendReportEmail();

        if (log.isEnabledFor(Level.INFO)) {
            log.log(Level.INFO, "finished sending Email.");
            log.log(Level.INFO, "finished the DirectPaymentsMailJob job.");
        }

	}

	private static void retrieveDirectPaymentList() {
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		String tranStatusStr = TransactionsManagerBase.TRANS_TYPE_DIRECT_BANK_DEPOSIT + "," +
							   TransactionsManagerBase.TRANS_TYPE_ENVOY_ONLINE_DEPOSIT + "," +
							   TransactionsManagerBase.TRANS_TYPE_ENVOY_BANKING_DEPOSIT + "," +
							   TransactionsManagerBase.TRANS_TYPE_ACH_DEPOSIT + "," +
							   TransactionsManagerBase.TRANS_TYPE_CASHU_DEPOSIT + "," +
							   TransactionsManagerBase.TRANS_TYPE_MONEYBOOKERS_DEPOSIT + "," +
							   TransactionsManagerBase.TRANS_TYPE_WEBMONEY_DEPOSIT + "," +
							   TransactionsManagerBase.TRANS_TYPE_CUP_CHINA_DEPOSIT + "," +
							   TransactionsManagerBase.TRANS_TYPE_UKASH_DEPOSIT;

		directPaymentsList = new ArrayList<Transaction>();

		try {
			conn = getConnection("directPayments.db.url","directPayments.db.user","directPayments.db.pass");

			String directPayment =
								" select " +
										" t.*, " +
										" ts.code status, " +
										" tt.code type, " +
										" CASE WHEN dpt.name is NULL then ' ' ELSE dpt.name END direct_payment_type, " +
										" c.code currency " +
								" from  transactions t, " +
										" transaction_statuses ts, " +
										" transaction_types tt, " +
										" payment_methods dpt, " +
										" users u," +
										" currencies c" +
								" where t.type_id in (" + tranStatusStr + ") " + // Inatec,Envoy,ACH,cashU,Ukash and Moneybookers Deposits
								  " and t.status_id = ts.id " +
								  " and t.type_id = tt.id " +
								  " and t.payment_type_id = dpt.id(+) " +
								  " and t.user_id = u.id " +
								  " and u.currency_id = c.id " +
								  " and u.class_id <> 0 " +
								  " and (t.time_created between to_date(?,'YYYY-MM-DD') and to_date(?,'YYYY-MM-DD') " +
								       " or " +
									   " t.time_settled between to_date(?,'YYYY-MM-DD') and to_date(?,'YYYY-MM-DD') ) ";

		pstmt = conn.prepareStatement(directPayment);

		pstmt.setString(1,startDate);
		pstmt.setString(2,endDate);
		pstmt.setString(3,startDate);
		pstmt.setString(4,endDate);

		rs = pstmt.executeQuery();

		while(rs.next()) {
			Transaction tran = new Transaction();
			tran.setId(rs.getLong("id"));
			tran.setUserId(rs.getLong("user_id"));
			tran.setAmount(rs.getLong("amount"));
			tran.setCurrency(rs.getString("currency"));
			tran.setTypeId(rs.getInt("type_id"));
			tran.setType(rs.getString("type"));
			tran.setTimeCreated(rs.getString("time_created"));
			tran.setTimeSettled(rs.getString("time_settled"));
			tran.setStatus(rs.getString("status"));
			tran.setComments(rs.getString("comments"));
			tran.setDirectPaymentType(rs.getString("direct_payment_type"));

			directPaymentsList.add(tran);
		}

		} catch (Exception e) {
			log.log(Level.FATAL,"Can't get direct payments " + e);
		} finally {
			try {
				rs.close();
			} catch (Exception e) {
				log.log(Level.ERROR,"Can't close",e);
			}

			try {
				pstmt.close();
			} catch (Exception e) {
				log.log(Level.ERROR,"Can't close",e);
			}

			try {
				conn.close();
			} catch (Exception e) {
				log.log(Level.ERROR,"Can't close",e);
			}
		}
	}

	private static void sendReportEmail(){
		StringBuffer reportBodyBuffer = new StringBuffer();

		reportBodyBuffer.append("<br><b>Direct Payments Transactions:</b><table border=\"1\"><tr>" +
									"<td><b>TransactionId</b></td>" +
									"<td><b>UserId</b></td>" +
									"<td><b>Type</b></td>" +
									"<td><b>Amount</b></td>" +
									"<td><b>Currency</b></td>" +
									"<td><b>Status</b></td>" +
									"<td><b>TimeCreated</b></td>" +
        							"<td><b>TimeSettled</b></td></tr>");

        for (int index=0; index <directPaymentsList.size(); ++index){
      	  Transaction tran = directPaymentsList.get(index);

      	  long amount = tran.getAmount()/100;
      	  String status = tran.getStatus().replace("TRANS_STATUS_", "");
      	  String type = tran.getType().replace("TRANS_TYPE_", "") + " " +
							tran.getDirectPaymentType();
      	  String timeSettled = tran.getTimeSettled();

      	  if (null == timeSettled){
      		timeSettled = "";
      	  }


      	  // Enter user to report.
      	  reportBodyBuffer.append("<tr>");
      	  reportBodyBuffer.append("<td>").append(tran.getId()).append("</td>");
      	  reportBodyBuffer.append("<td>").append(tran.getUserId()).append("</td>");
      	  reportBodyBuffer.append("<td>").append(type).append("</td>");
      	  reportBodyBuffer.append("<td>").append(amount).append("</td>");
      	  reportBodyBuffer.append("<td>").append(tran.getCurrency()).append("</td>");
      	  reportBodyBuffer.append("<td>").append(status).append("</td>");
    	  reportBodyBuffer.append("<td>").append(tran.getTimeCreated()).append("</td>");
    	  reportBodyBuffer.append("<td>").append(timeSettled).append("</td>");
      	  reportBodyBuffer.append("</tr>");

        }

		//  build the report
        String reportBody = "<html><body>" + reportBodyBuffer.toString() + "</body></html>";

  	    if (log.isEnabledFor(Level.INFO)) {
  	        log.log(Level.INFO, "Sending report email: " + reportBody);
  	    }

  	    //  send email
    	Hashtable<String,String> server = new Hashtable<String,String>();
  	    server.put("url", getPropertyByFile("directPayments.email.url"));
  	    server.put("auth", getPropertyByFile("directPayments.email.auth"));
  	    server.put("user", getPropertyByFile("directPayments.email.user"));
  	    server.put("pass", getPropertyByFile("directPayments.email.pass"));
  	    server.put("contenttype", getPropertyByFile("directPayments.email.contenttype", "text/html; charset=UTF-8"));

  	    Hashtable<String,String> email = new Hashtable<String,String>();
  	    if (null != endDate){
  	    	email.put("subject", getPropertyByFile("directPayments.email.subject") + ' ' + startDate + " To " + endDate);
  	    } else{
  	    	email.put("subject", getPropertyByFile("directPayments.email.subject") + ' ' + startDate);
  	    }
 	    email.put("to", getPropertyByFile("directPayments.email.to"));
  	    email.put("from", getPropertyByFile("directPayments.email.from"));
  	    email.put("body", reportBody);
  	    sendEmail(server, email);
    }
}
