package il.co.etrader.tomcat;

import java.util.HashMap;
import java.util.Hashtable;

import javax.naming.Context;
import javax.naming.Name;
import javax.naming.spi.ObjectFactory;

import org.apache.log4j.Logger;

public class ObjectSharingFactory implements ObjectFactory {
    private static final Logger log = Logger.getLogger(ObjectSharingFactory.class);
    
    private HashMap<String, Object> sharing;

    public ObjectSharingFactory() {
    	sharing = new HashMap<String, Object>();
    	log.info("Creating ObjectSharingFactory");
    }
    
    public Object getObjectInstance(Object obj, Name name, Context nameCtx, Hashtable environment) throws Exception {
    	log.debug("Return reference to sharing");
    	return sharing;
    }
}