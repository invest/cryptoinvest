package il.co.etrader.daily.balance.report;

import il.co.etrader.bl_managers.TransactionsManagerBase;
import il.co.etrader.util.CommonUtil;
import il.co.etrader.util.ConstantsBase;
import il.co.etrader.util.JobUtil;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Hashtable;
import java.util.zip.GZIPOutputStream;

import org.apache.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFRichTextString;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

public class DailyBalanceReportJob extends JobUtil{

	private static Logger log = Logger.getLogger(DailyBalanceReportJob.class);
	
	private final static String etUsers = "ET";
	private final static String aoUsers ="AO";
	private final static String etSkin = "= 1";
	private final static String aoSkin = "!= 1";
	private static String isAO;
	private static String skin;
	private final static int MAX_ROWS_PER_SHEET = 50000;
	

	public static void main(String[] args) throws Exception {
		propFile = args[0];
		if(propFile.contains("AO")){
			
			isAO = aoUsers;
			skin = aoSkin;
			}else {
				
				isAO = etUsers;
				skin = etSkin;
			}
		int daysBackForReport = 1;
		

		if (args.length > 1){
			daysBackForReport = Integer.valueOf(args[1]);
		}

		log.info("Starting the DailyBalanceReportJob"+isAO+" for " + daysBackForReport + " days back");

		SendXlsFile(daysBackForReport,skin);

		log.info("Finishing the DailyBalanceReportJob"+isAO+".");
	}

	private static void SendXlsFile(int daysBackForReport, String skin){
		HSSFWorkbook xlsWorkbook = new HSSFWorkbook();
		HSSFSheet xlsSheet= null;
		HSSFRow xlsRow = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		Connection con = null;
		int rowCounter = 0;
		int sheetCounter = 1;
		GregorianCalendar gc = new GregorianCalendar();

		gc.set(GregorianCalendar.HOUR_OF_DAY, 0);
		gc.set(GregorianCalendar.MINUTE, 0);
		gc.set(GregorianCalendar.SECOND, 0);
		gc.set(GregorianCalendar.MILLISECOND, 0);
		gc.add(GregorianCalendar.DAY_OF_MONTH, - daysBackForReport);

		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
		Date startDate = gc.getTime();
		String mailSubject = "Daily Balance Report "+isAO+" for " +  sdf.format(startDate);
		String fileName ="DailyBalanceReport" + isAO + "_" + sdf.format(startDate) + ".xls";
		if (daysBackForReport > 1){
			gc.add(GregorianCalendar.DAY_OF_MONTH, daysBackForReport - 1);
			Date endDate = gc.getTime();
			mailSubject += " until " +  sdf.format(endDate);
		}

		try{
			con = getConnection();

			String sqlFillUsersTable =
				" INSERT into DAILY_BALANCE_"+isAO+"_USERS " +
				" SELECT " +
					" A.user_id, " +
					" nvl(bh_open.balance,0)/100 as opening_balance, " +
					" nvl(bh_open.tax_balance,0)/100 as opening_tax, " +
					" bh_close.balance/100 as closing_balance, " +
					" bh_close.tax_balance/100 as closing_tax " +
				" FROM " +
					" balance_history bh_close, " +
					" (SELECT " +
						" bh.user_id, " +
						" MAX(case when bh.time_created < trunc(sysdate-?) then bh.id else null end) as open_bh_id, " +
						" MAX(case when bh.time_created < trunc(sysdate) then bh.id else null end) as close_bh_id " +
					" FROM " +
						" balance_history bh, " +
						" users u " +
					" WHERE bh.user_id = u.id "+
						" AND u.skin_id "+ skin + 
						" AND u.class_id <> 0 " +
					" GROUP BY  " +
						" bh.user_id) A " +
	  		        " left join balance_history bh_open on bh_open.id = A.open_bh_id AND bh_open.user_id = A.user_id " +
			    " WHERE " +
			    	" bh_close.id = A.close_bh_id " +
			    	//////////////////////////////////
				   	" AND bh_close.user_id = A.user_id " +
				   	" AND bh_close.balance <> 0 ";

			ps = con.prepareStatement(sqlFillUsersTable);
			ps.setInt(1, daysBackForReport);
			int insertsNum = ps.executeUpdate();

			if (insertsNum > 0){
				log.info("Finish updating DAILY_BALANCE_"+isAO+"_USERS");

				String sqlGetReportFields =
					" SELECT " +
						" B.*, " +
						" (select code from  currencies where id = B.currency_id)  curr_code ," +
						" OPENING_BALANCE - CLOSING_TAX + OPENING_TAX + DEP_CC - CANCEL_DEP + ADMIN_DEPOSITS - ADMIN_WITHDRAWALS + DEP_WIRE + CANCEL_WDR + REVERSE_WDR " +
							"  + CANCELLED_INVESTMENTS - WDR_CHEQUE - INSERT_INVESTMENTS + SETTLED_INVESTMENTS + DEP_CASH - TAX_PAYMENTS - WDR_CC " +
							"  + BONUS_DEPOSITS - BONUS_WITHRAWALS - WDR_BANK_WIRE + DEP_DIRECT + FIX_BALANCE_DEPOSIT - FIX_BALANCE_WITHDRAW " +
							"  + DEPOSIT_BY_COMPANY + DEP_PAYPAL - WDR_PAYPAL + DEP_CASHU + DEP_ACH + DEP_UKASH - CHB_DEPOSITS - WDR_ENVOY " +
							"  + DEP_MONEYBOOKERS + RESETTLE_INVESTMENT + EFT_DEPOSIT - EFT_WITHDRAW - MONEYBOOKERS_WITHDARW " +
							"  + DEP_WEBMONEY - WDR_WEBMONEY + DEP_DELTAPAY - WDR_DELTAPAY + DEP_CDPAY - WDR_CDPAY  + DEP_BAROPAY - WDR_BAROPAY + DEP_FIX_NEGATIVE_BALANCE + DEP_INATEC_IFRAME " +
							"  - M_FEE + M_FEE_CANCEL + DEP_CUP - WDR_CUP" +
                                " as CLOSING_BALANCE_CALC " +
					" FROM  " +
						" (SELECT " +
							" A.user_id, " +
							" A.currency_id, " +
							" A.opening_balance, " +
							" A.closing_balance, " +
							" A.opening_tax, " +
							" A.closing_tax, " +
							" sum(case when cmd = '" + ConstantsBase.LOG_BALANCE_CC_DEPOSIT  + "' then A.amount else 0 end)/100 DEP_CC, " +
							" sum(case when cmd = '" + ConstantsBase.LOG_BALANCE_CANCEL_DEPOSIT + "'  then A.amount else 0 end)/100 CANCEL_DEP, " +
							" sum(case when cmd = '" + ConstantsBase.LOG_BALANCE_ADMIN_DEPOSIT + "' then A.amount else 0 end)/100 ADMIN_DEPOSITS, " +
							" sum(case when cmd = '" + ConstantsBase.LOG_BALANCE_ADMIN_WITHDRAW + "' then A.amount else 0 end)/100 ADMIN_WITHDRAWALS, " +
							" sum(case when cmd = '" + ConstantsBase.LOG_BALANCE_WIRE_DEPOSIT + "' then A.amount else 0 end)/100 DEP_WIRE, " +
							" sum(case when cmd = '" + ConstantsBase.LOG_BALANCE_REJECT_WITHDRAW + "' then A.amount else 0 end)/100 CANCEL_WDR, " +
							" sum(case when cmd = '" + ConstantsBase.LOG_BALANCE_REVERSE_WITHDRAW + "' then A.amount else 0 end)/100 REVERSE_WDR, " +							
							" sum(case when cmd = '" + ConstantsBase.LOG_BALANCE_CANCEL_INVESTMENT + "' then reports.return_amount(A.table_id) else 0 end)/100 CANCELLED_INVESTMENTS, " +
							" sum(case when cmd = '" + ConstantsBase.LOG_BALANCE_CHEQUE_WITHDRAW + "' then A.amount else 0 end)/100 WDR_CHEQUE, " +
                            " sum(case when cmd = '" + ConstantsBase.LOG_BALANCE_INSERT_INVESTMENT + "' and A.type_id not in (4,5) then A.amount " +
                            		 " when cmd = '" + ConstantsBase.LOG_BALANCE_INSERT_INVESTMENT + "' and A.type_id in (4,5) then REPORTS.INSERT_AMOUNT(A.table_id) else 0 end)/100 INSERT_INVESTMENTS, " +  																					
							" sum(case when cmd = '" + ConstantsBase.LOG_BALANCE_SETTLE_INVESTMENT + "' then A.amount - A.lose + A.win else 0 end)/100 SETTLED_INVESTMENTS, " +
							" sum(case when cmd = '" + ConstantsBase.LOG_BALANCE_CASH_DEPOSIT + "' then A.amount else 0 end)/100 DEP_CASH, " +
							" sum(case when cmd = '" + ConstantsBase.LOG_BALANCE_TAX_PAYMENT + "' then A.amount else 0 end)/100 TAX_PAYMENTS, " +
							" sum(case when cmd = '" + ConstantsBase.LOG_BALANCE_CC_WITHDRAW + "' then A.amount else 0 end)/100 WDR_CC, " +
							" sum(case when cmd = '" + ConstantsBase.LOG_BALANCE_BONUS_DEPOSIT + "' then A.amount else 0 end)/100 BONUS_DEPOSITS, " +
							" sum(case when cmd = '" + ConstantsBase.LOG_BALANCE_BONUS_WITHDRAW + "' then A.amount else 0 end)/100 BONUS_WITHRAWALS, " +
							" sum(case when cmd = '" + ConstantsBase.LOG_BALANCE_BANK_WIRE + "' then A.amount else 0 end)/100 WDR_BANK_WIRE, " +
							" sum(case when cmd = '" + ConstantsBase.LOG_BALANCE_DIRECT_DEPOSIT + "' then A.amount else 0 end)/100 DEP_DIRECT, " +
							" sum(case when cmd = '" + ConstantsBase.LOG_BALANCE_FIX_BALANCE_DEPOSIT + "' then A.amount else 0 end)/100 FIX_BALANCE_DEPOSIT, " +
							" sum(case when cmd = '" + ConstantsBase.LOG_BALANCE_FIX_BALANCE_WITHDRAW + "' then A.amount else 0 end)/100 FIX_BALANCE_WITHDRAW, " +
							" sum(case when cmd = '" + ConstantsBase.LOG_BALANCE_DEPOSIT_BY_COMPANY + "' then A.amount else 0 end)/100 DEPOSIT_BY_COMPANY, " +
							" sum(case when cmd = '" + ConstantsBase.LOG_BALANCE_PAYPAL_DEPOSIT + "' then A.amount else 0 end)/100 DEP_PAYPAL, " +
							" sum(case when cmd = '" + ConstantsBase.LOG_BALANCE_PAYPAL_WITHDRAW + "' then A.amount else 0 end)/100 WDR_PAYPAL, " +
							" sum(case when cmd = '" + ConstantsBase.LOG_BALANCE_CASHU_DEPOSIT + "' then A.amount else 0 end)/100 DEP_CASHU, " +
							" sum(case when cmd = '" + ConstantsBase.LOG_BALANCE_ACH_DEPOSIT + "' then A.amount else 0 end)/100 DEP_ACH, " +
							" sum(case when cmd = '" + ConstantsBase.LOG_BALANCE_UKASH_DEPOSIT + "' then A.amount else 0 end)/100 DEP_UKASH, " +
							" sum(case when cmd = '" + ConstantsBase.LOG_BALANCE_CHARGE_BACK + "' then A.amount else 0 end)/100 CHB_DEPOSITS, " +
							" sum(case when cmd = '" + ConstantsBase.LOG_BALANCE_ENVOY_WITHDRAW + "' then A.amount else 0 end)/100 WDR_ENVOY, " +
							" sum(case when cmd = '" + ConstantsBase.LOG_BALANCE_MONEYBOOKERS_DEPOSIT + "' then A.amount else 0 end)/100 DEP_MONEYBOOKERS, " +
							" sum(case when cmd = '" + ConstantsBase.LOG_BALANCE_RESETTLE_INVESTMENT + "' then A.amount else 0 end)/100 RESETTLE_INVESTMENT, " +
                            " sum(case when cmd = '" + ConstantsBase.LOG_BALANCE_EFT_DEPOSIT + "' then A.amount else 0 end)/100 EFT_DEPOSIT, " +
                            " sum(case when cmd = '" + ConstantsBase.LOG_BALANCE_EFT_WITHDRAW + "' then A.amount else 0 end)/100 EFT_WITHDRAW, " +
                            " sum(case when cmd = '" + ConstantsBase.LOG_BALANCE_MONEYBOOKERS_WITHDARW + "' then A.amount else 0 end)/100 MONEYBOOKERS_WITHDARW, " +
                            " sum(case when cmd = '" + ConstantsBase.LOG_BALANCE_WEBMONEY_DEPOSIT + "' then A.amount else 0 end)/100 DEP_WEBMONEY, " +
                            " sum(case when cmd = '" + ConstantsBase.LOG_BALANCE_WEBMONEY_WITHDRAW + "' then A.amount else 0 end)/100 WDR_WEBMONEY, " +
                            " sum(case when cmd = '" + ConstantsBase.LOG_BALANCE_DELTA_PAY_CHINA_DEPOSIT + "' then A.amount else 0 end)/100 DEP_DELTAPAY, " +
                            " sum(case when cmd = '" + ConstantsBase.LOG_BALANCE_DELTA_PAY_CHINA_WITHDRAW + "' then A.amount else 0 end)/100 WDR_DELTAPAY, " +
                            " sum(case when cmd = '" + ConstantsBase.LOG_BALANCE_CDPAY_CHINA_DEPOSIT + "' then A.amount else 0 end)/100 DEP_CDPAY, " +
                            " sum(case when cmd = '" + ConstantsBase.LOG_BALANCE_CDPAY_CHINA_WITHDRAW + "' then A.amount else 0 end)/100 WDR_CDPAY, " +
                            " sum(case when cmd = '" + ConstantsBase.LOG_BALANCE_BAROPAY_DEPOSIT + "' then A.amount else 0 end)/100 DEP_BAROPAY, " +
                            " sum(case when cmd = '" + ConstantsBase.LOG_BALANCE_BAROPAY_WITHDRAW + "' then A.amount else 0 end)/100 WDR_BAROPAY, " +
                            " sum(case when cmd = '" + ConstantsBase.LOG_BALANCE_CUP_DEPOSIT + "' then A.amount else 0 end)/100 DEP_CUP, " +
                            " sum(case when cmd = '" + ConstantsBase.LOG_BALANCE_CUP_WITHDRAW + "' then A.amount else 0 end)/100 WDR_CUP, " +
                            " sum(case when cmd = '" + ConstantsBase.LOG_BALANCE_MAINTENANCE_FEE + "' then A.amount else 0 end)/100 M_FEE, " +
                            " sum(case when cmd = '" + ConstantsBase.LOG_BALANCE_MAINTENANCE_FEE_CANCEL + "' then A.amount else 0 end)/100 M_FEE_CANCEL, " +
                            " sum(case when cmd = '" + ConstantsBase.LOG_BALANCE_FIX_NEGATIVE_BALANCE_DEPOSIT + "' then A.amount else 0 end)/100 DEP_FIX_NEGATIVE_BALANCE, " +
                            " sum(case when cmd = '" + ConstantsBase.LOG_BALANCE_INATEC_IFRAME_DEPOSIT + "' then A.amount else 0 end)/100 DEP_INATEC_IFRAME, " +
							" count(cmd) bh_rec_num, " +
							" sum(case when A.dup_recs > 1 then 1 else 0 end) dup_recs_num, " +
							" sum(case when A.amount = 0 then 1 else 0 end) no_amount_recs_num, " +
							" sum(case when to_number(cmd) > " + ConstantsBase.MAX_LOG_BALANCE_ID + " then 1 else 0 end) new_enum_recs_num " +
						" FROM " +
							" (SELECT " +
							    " /*+ USE_HASH(u bh),USE_HASH(u i)*/" +
								" u.opening_balance, " +
								" u.opening_tax, " +
								" u.closing_balance, " +
								" u.closing_tax, " +
								" u.user_id, " +
								" usr.currency_id, " +
								" bh.command cmd, " +
								" i.type_id, " +
								" case " +
										" when bh.table_name = 'transactions' and " +
											" (t2t.class_type is null or (t2t.class_type != " + TransactionsManagerBase.TRANS_CLASS_FEES + " or  t2t.id in (" + TransactionsManagerBase.TRANS_TYPE_MAINTENANCE_FEE_CANCEL + ", " + TransactionsManagerBase.TRANS_TYPE_MAINTENANCE_FEE + " ))) then t.amount " +
										" when bh.table_name = 'transactions' and " +
											" t2t.class_type = " + TransactionsManagerBase.TRANS_CLASS_FEES + " and t2t.id not in (" + TransactionsManagerBase.TRANS_TYPE_MAINTENANCE_FEE_CANCEL + ", " + TransactionsManagerBase.TRANS_TYPE_MAINTENANCE_FEE + " ) then t.amount + t2.amount " +
										" when bh.table_name = 'investments' and i.is_canceled = 0 then i.amount  " +
										" when bh.table_name = 'investments' and i.is_canceled = 1 then (i.amount - i.house_result)  " +
										" when bh.table_name = 'bonus_users' then bu.bonus_amount " +
										" when bh.table_name = 'tax_history' then th.tax " +
										" when bh.table_name is null then null " +
										" else 0  " +
									" end amount, " +
								" bh.table_name, " +
								" bh.key_value table_id, " +
								" i.lose, " +
								" i.win, " +
								" count(*) dup_recs " +
							" FROM  " +
								" daily_balance_"+isAO+"_users u " +
									" left join balance_history bh on bh.user_id = u.user_id AND bh.time_created between trunc(sysdate-?) and trunc(sysdate) " +
										" left join transactions t on t.id = bh.key_value and bh.table_name = 'transactions' " +
											" left join transactions t2 on t.reference_id = t2.id " +
												" left join transaction_types t2t on t2.type_id = t2t.id " +
										" left join investments i on i.id = bh.key_value and bh.table_name = 'investments' " +
										" left join bonus_users bu on bu.id = bh.key_value and bh.table_name = 'bonus_users' " +
										" left join tax_history th on th.id = bh.key_value and bh.table_name = 'tax_history' " +
										" left join users usr on usr.id = u.user_id " +
							" GROUP BY " +
								" u.opening_balance, " +
								" u.opening_tax, " +
								" u.closing_balance, " +
								" u.closing_tax, " +
								" u.user_id, " +
								" usr.currency_id, " +
								" bh.command, " +
								" i.type_id, " +
								" case  " +
									" when bh.table_name = 'transactions' and " +
										" (t2t.class_type is null or (t2t.class_type != " + TransactionsManagerBase.TRANS_CLASS_FEES + " or  t2t.id in (" + TransactionsManagerBase.TRANS_TYPE_MAINTENANCE_FEE_CANCEL + ", " + TransactionsManagerBase.TRANS_TYPE_MAINTENANCE_FEE + " ))) then t.amount " +
									" when bh.table_name = 'transactions' and " +
										" t2t.class_type = " + TransactionsManagerBase.TRANS_CLASS_FEES + " and t2t.id not in (" + TransactionsManagerBase.TRANS_TYPE_MAINTENANCE_FEE_CANCEL + ", " + TransactionsManagerBase.TRANS_TYPE_MAINTENANCE_FEE + " ) then t.amount + t2.amount " +
										" when bh.table_name = 'investments' and i.is_canceled = 0 then i.amount  " +
										" when bh.table_name = 'investments' and i.is_canceled = 1 then i.amount - i.house_result  " +
										" when bh.table_name = 'bonus_users' then bu.bonus_amount " +
										" when bh.table_name = 'tax_history' then th.tax " +
										" when bh.table_name is null then null " +
										" else 0  " +
									" end, " +
								" bh.table_name, " +
								" bh.key_value, " +
								" i.lose, " +
								" i.win " +
							" )A   " +
						" GROUP BY  " +
							" A.user_id, " +
							" A.currency_id, " +
							" A.opening_balance, " +
							" A.opening_tax, " +
							" A.closing_balance, " +
							" A.closing_tax, " +
							" A.closing_balance, " +
							" A.closing_tax) B ";

				try {
					log.info("Start Report Sql");
					ps = con.prepareStatement(sqlGetReportFields);
					ps.setInt(1, daysBackForReport);
					rs = ps.executeQuery();

					log.info("Start Building Report");
					xlsSheet = xlsWorkbook.createSheet("Daily Balance" + sheetCounter);
					xlsRow = xlsSheet.createRow(rowCounter++);
					setColumnsNameInSheet(xlsRow);

					while (rs.next()) {
						if (rowCounter > MAX_ROWS_PER_SHEET) {
							rowCounter = 0;
							xlsSheet = xlsWorkbook.createSheet("Daily Balance" + ++sheetCounter);
							xlsRow = xlsSheet.createRow(rowCounter++);
							setColumnsNameInSheet(xlsRow);
						}						
						xlsRow = xlsSheet.createRow(rowCounter++);
						setColumnsValuesInSheet(xlsWorkbook, xlsRow, rs);
					}
					writeToExcel(xlsWorkbook,fileName);

					// send email
			        Hashtable<String,String> server = new Hashtable<String,String>();
			        server.put("url", getPropertyByFile("email.url"));
			        server.put("auth", getPropertyByFile("email.auth"));
			        server.put("user", getPropertyByFile("email.user"));
			        server.put("pass", getPropertyByFile("email.pass"));
			        server.put("contenttype", getPropertyByFile("email.contenttype", "text/html; charset=UTF-8"));

			        Hashtable<String,String> email = new Hashtable<String,String>();
			        email.put("subject", mailSubject);
			        email.put("to", getPropertyByFile("email.to"));
			        email.put("from", getPropertyByFile("email.from"));
			        email.put("body", "The report is attached to this mail");

			        File attachment = new File(fileName + ".gz");
			        File []attachments={attachment};
			        CommonUtil.sendEmail(server, email, attachments);
			        
			        deleteFile(attachment);

				}catch (Exception e) {
					log.fatal("Can't get DailyBalanceReport Data ", e);
				}
			} else{
				log.error("Error no records inserted to DAILY_BALANCE_"+isAO+"_USERS ");
			}
		} catch (Exception e) {
			log.fatal("Can't get DailyBalanceReport"+isAO+" ", e);
		} finally {
			closeResultSet(rs);
			closeStatement(ps);

			try {
				con.close();
			} catch (Exception e) {
				log.error("Can't close",e);
			}
		}
	}


	private static void zipExcelFile(String fName){
		GZIPOutputStream out= null;
		FileInputStream in = null;
		try{
			
			byte[] buf = new byte[4096];
            int bytesRead;
            out = new GZIPOutputStream(new FileOutputStream(fName+".gz"));
            in = new FileInputStream(fName);
            while ((bytesRead = in.read(buf)) != -1)
                out.write(buf, 0, bytesRead);
			
		}catch(IOException e){
			
			e.printStackTrace();
		}finally{
			
			if(out!=null){
				try {
					in.close();
					out.flush();
	                out.close();
	                File f = new File(fName);
	                f.delete();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}
	private static void writeToExcel(HSSFWorkbook xlsWorkbook, String fileName){
		FileOutputStream fos = null;
		try {
            fos = new FileOutputStream(new File(fileName));
            xlsWorkbook.write(fos);
            zipExcelFile(fileName);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (fos != null) {
                try {
                    fos.flush();
                    fos.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
	}

	private static void setColumnsNameInSheet(HSSFRow xlsRow){
		int cellCounter = 0;
		HSSFCell xlsCell = null;

		xlsCell = xlsRow.createCell(cellCounter++);
		xlsCell.setCellValue(new HSSFRichTextString("User ID"));
		xlsCell = xlsRow.createCell(cellCounter++);
		xlsCell.setCellValue(new HSSFRichTextString("Currency"));
		xlsCell = xlsRow.createCell(cellCounter++);
		xlsCell.setCellValue(new HSSFRichTextString("Opening Balance"));
		xlsCell = xlsRow.createCell(cellCounter++);
		xlsCell.setCellValue(new HSSFRichTextString("Opening Tax"));

		xlsCell = xlsRow.createCell(cellCounter++);
		xlsCell.setCellValue(new HSSFRichTextString("Deposits By CC"));
		xlsCell = xlsRow.createCell(cellCounter++);
		xlsCell.setCellValue(new HSSFRichTextString("Deposits By Cash"));
		xlsCell = xlsRow.createCell(cellCounter++);
		xlsCell.setCellValue(new HSSFRichTextString("Deposits By Online Methods"));
		xlsCell = xlsRow.createCell(cellCounter++);
		xlsCell.setCellValue(new HSSFRichTextString("Deposits By Paypal"));
		xlsCell = xlsRow.createCell(cellCounter++);
		xlsCell.setCellValue(new HSSFRichTextString("Deposits By CashU"));
		xlsCell = xlsRow.createCell(cellCounter++);
		xlsCell.setCellValue(new HSSFRichTextString("Deposits By Ach"));
		xlsCell = xlsRow.createCell(cellCounter++);
		xlsCell.setCellValue(new HSSFRichTextString("Deposits By Ukash"));
		xlsCell = xlsRow.createCell(cellCounter++);
		xlsCell.setCellValue(new HSSFRichTextString("Deposits By MoneyBookers"));
        xlsCell = xlsRow.createCell(cellCounter++);
        xlsCell.setCellValue(new HSSFRichTextString("Deposits By EFT"));
        xlsCell = xlsRow.createCell(cellCounter++);
        xlsCell.setCellValue(new HSSFRichTextString("Deposits By WebMoney"));
        xlsCell = xlsRow.createCell(cellCounter++);
        xlsCell.setCellValue(new HSSFRichTextString("Deposits By DeltaPay China"));
        xlsCell = xlsRow.createCell(cellCounter++);
        xlsCell.setCellValue(new HSSFRichTextString("Deposits By CDpay China"));
        xlsCell = xlsRow.createCell(cellCounter++);
        xlsCell.setCellValue(new HSSFRichTextString("Deposits By Baropay China"));
		xlsCell = xlsRow.createCell(cellCounter++);
		xlsCell.setCellValue(new HSSFRichTextString("CUP deposit"));
		xlsCell = xlsRow.createCell(cellCounter++);
        xlsCell.setCellValue(new HSSFRichTextString("Deposits By Inatec Iframe"));

		xlsCell = xlsRow.createCell(cellCounter++);
		xlsCell.setCellValue(new HSSFRichTextString("Withdrawals By Cheque"));
		xlsCell = xlsRow.createCell(cellCounter++);
		xlsCell.setCellValue(new HSSFRichTextString("Withdrawals By CC"));
		xlsCell = xlsRow.createCell(cellCounter++);
		xlsCell.setCellValue(new HSSFRichTextString("Withdrawals By Bank Wire"));
		xlsCell = xlsRow.createCell(cellCounter++);
		xlsCell.setCellValue(new HSSFRichTextString("Withdrawals By Paypal"));
		xlsCell = xlsRow.createCell(cellCounter++);
		xlsCell.setCellValue(new HSSFRichTextString("Withdrawals By Envoy"));
        xlsCell = xlsRow.createCell(cellCounter++);
        xlsCell.setCellValue(new HSSFRichTextString("Withdrawals By EFT"));
        xlsCell = xlsRow.createCell(cellCounter++);
        xlsCell.setCellValue(new HSSFRichTextString("Withdrawals By MoneyBookers"));
        xlsCell = xlsRow.createCell(cellCounter++);
        xlsCell.setCellValue(new HSSFRichTextString("Withdrawals By WebMoney"));
        xlsCell = xlsRow.createCell(cellCounter++);
        xlsCell.setCellValue(new HSSFRichTextString("Withdrawals By DeltaPay China"));
        xlsCell = xlsRow.createCell(cellCounter++);
        xlsCell.setCellValue(new HSSFRichTextString("Withdrawals By CDpay China"));
        xlsCell = xlsRow.createCell(cellCounter++);
        xlsCell.setCellValue(new HSSFRichTextString("Withdrawals By Baropay China"));
		xlsCell = xlsRow.createCell(cellCounter++);
		xlsCell.setCellValue(new HSSFRichTextString("CUP withdraw"));

		xlsCell = xlsRow.createCell(cellCounter++);
		xlsCell.setCellValue(new HSSFRichTextString("Admin Deposits"));
		xlsCell = xlsRow.createCell(cellCounter++);
		xlsCell.setCellValue(new HSSFRichTextString("Admin Withdrawals"));

		xlsCell = xlsRow.createCell(cellCounter++);
		xlsCell.setCellValue(new HSSFRichTextString("Bonus Deposits"));
		xlsCell = xlsRow.createCell(cellCounter++);
		xlsCell.setCellValue(new HSSFRichTextString("Bonus Withdrawals"));

		xlsCell = xlsRow.createCell(cellCounter++);
		xlsCell.setCellValue(new HSSFRichTextString("Deposits By Company"));

		xlsCell = xlsRow.createCell(cellCounter++);
		xlsCell.setCellValue(new HSSFRichTextString("ChargedBack Transaction"));
		xlsCell = xlsRow.createCell(cellCounter++);
		xlsCell.setCellValue(new HSSFRichTextString("Cancelled Deposits"));
		xlsCell = xlsRow.createCell(cellCounter++);
		xlsCell.setCellValue(new HSSFRichTextString("Cancelled Withdrawals"));
		xlsCell = xlsRow.createCell(cellCounter++);
		xlsCell.setCellValue(new HSSFRichTextString("Reversed Withdrawals"));
		
		xlsCell = xlsRow.createCell(cellCounter++);
		xlsCell.setCellValue(new HSSFRichTextString("Maintenance fee"));
		xlsCell = xlsRow.createCell(cellCounter++);
		xlsCell.setCellValue(new HSSFRichTextString("Cancel Maintenance fee"));

		xlsCell = xlsRow.createCell(cellCounter++);
		xlsCell.setCellValue(new HSSFRichTextString("Inserted Investments"));
		xlsCell = xlsRow.createCell(cellCounter++);
		xlsCell.setCellValue(new HSSFRichTextString("Settled Investments"));
		xlsCell = xlsRow.createCell(cellCounter++);
		xlsCell.setCellValue(new HSSFRichTextString("Cancelled Investments"));
		xlsCell = xlsRow.createCell(cellCounter++);
		xlsCell.setCellValue(new HSSFRichTextString("Resettled Investments"));

		xlsCell = xlsRow.createCell(cellCounter++);
		xlsCell.setCellValue(new HSSFRichTextString("Tax Payments"));

		xlsCell = xlsRow.createCell(cellCounter++);
		xlsCell.setCellValue(new HSSFRichTextString("Fix Balance Deposits"));
		xlsCell = xlsRow.createCell(cellCounter++);
		xlsCell.setCellValue(new HSSFRichTextString("Fix Balance Withdrawals"));
		xlsCell = xlsRow.createCell(cellCounter++);
		xlsCell.setCellValue(new HSSFRichTextString("Fix Negative Balance"));

		xlsCell = xlsRow.createCell(cellCounter++);
		xlsCell.setCellValue(new HSSFRichTextString("Closing Tax"));
		xlsCell = xlsRow.createCell(cellCounter++);
		xlsCell.setCellValue(new HSSFRichTextString("Closing Balance"));
		xlsCell = xlsRow.createCell(cellCounter++);
		xlsCell.setCellValue(new HSSFRichTextString("Calculated Closing Balance"));
		xlsCell = xlsRow.createCell(cellCounter++);
		xlsCell.setCellValue(new HSSFRichTextString("Closing Balance Difference"));
		xlsCell = xlsRow.createCell(cellCounter++);
		xlsCell.setCellValue(new HSSFRichTextString("Balance Activities"));

		xlsCell = xlsRow.createCell(cellCounter++);
		xlsCell.setCellValue("       ||");
		xlsCell = xlsRow.createCell(cellCounter++);
		xlsCell.setCellValue("Dupliacte Records");
		xlsCell = xlsRow.createCell(cellCounter++);
		xlsCell.setCellValue("New Table Records");
		xlsCell = xlsRow.createCell(cellCounter++);
		xlsCell.setCellValue("New Enum Records");
	}

	private static void setColumnsValuesInSheet(HSSFWorkbook xlsWorkbook, HSSFRow xlsRow, ResultSet rs) throws SQLException{
		int cellCounter = 0;
		HSSFCell xlsCell = null;
		HSSFCellStyle cellStyle = xlsWorkbook.createCellStyle();
		cellStyle.setWrapText(true);

		xlsCell = xlsRow.createCell(cellCounter++);
		xlsCell.setCellValue(new HSSFRichTextString(rs.getString("user_id")));
		xlsCell = xlsRow.createCell(cellCounter++);
		xlsCell.setCellValue(new HSSFRichTextString(rs.getString("curr_code")));
		xlsCell = xlsRow.createCell(cellCounter++);
		xlsCell.setCellValue(rs.getDouble("opening_balance"));
		xlsCell = xlsRow.createCell(cellCounter++);
		xlsCell.setCellValue(rs.getDouble("opening_tax"));

		xlsCell = xlsRow.createCell(cellCounter++);
		xlsCell.setCellValue(rs.getDouble("DEP_CC"));
		xlsCell = xlsRow.createCell(cellCounter++);
		xlsCell.setCellValue(rs.getDouble("DEP_CASH"));
		xlsCell = xlsRow.createCell(cellCounter++);
		xlsCell.setCellValue(rs.getDouble("DEP_DIRECT"));
		xlsCell = xlsRow.createCell(cellCounter++);
		xlsCell.setCellValue(rs.getDouble("DEP_PAYPAL"));
		xlsCell = xlsRow.createCell(cellCounter++);
		xlsCell.setCellValue(rs.getDouble("DEP_CASHU"));
		xlsCell = xlsRow.createCell(cellCounter++);
		xlsCell.setCellValue(rs.getDouble("DEP_ACH"));
		xlsCell = xlsRow.createCell(cellCounter++);
		xlsCell.setCellValue(rs.getDouble("DEP_UKASH"));
		xlsCell = xlsRow.createCell(cellCounter++);
		xlsCell.setCellValue(rs.getDouble("DEP_MONEYBOOKERS"));
        xlsCell = xlsRow.createCell(cellCounter++);
        xlsCell.setCellValue(rs.getDouble("EFT_DEPOSIT"));
        xlsCell = xlsRow.createCell(cellCounter++);
        xlsCell.setCellValue(rs.getDouble("DEP_WEBMONEY"));
        xlsCell = xlsRow.createCell(cellCounter++);
        xlsCell.setCellValue(rs.getDouble("DEP_DELTAPAY"));
        xlsCell = xlsRow.createCell(cellCounter++);
        xlsCell.setCellValue(rs.getDouble("DEP_CDPAY"));
        xlsCell = xlsRow.createCell(cellCounter++);
        xlsCell.setCellValue(rs.getDouble("DEP_BAROPAY"));
        xlsCell = xlsRow.createCell(cellCounter++);
        xlsCell.setCellValue(rs.getDouble("DEP_CUP"));
        xlsCell = xlsRow.createCell(cellCounter++);
        xlsCell.setCellValue(new HSSFRichTextString("DEP_INATEC_IFRAME"));
        
		xlsCell = xlsRow.createCell(cellCounter++);
		xlsCell.setCellValue(rs.getDouble("WDR_CHEQUE"));
		xlsCell = xlsRow.createCell(cellCounter++);
		xlsCell.setCellValue(rs.getDouble("WDR_CC"));
		xlsCell = xlsRow.createCell(cellCounter++);
		xlsCell.setCellValue(rs.getDouble("WDR_BANK_WIRE"));
		xlsCell = xlsRow.createCell(cellCounter++);
		xlsCell.setCellValue(rs.getDouble("WDR_PAYPAL"));
		xlsCell = xlsRow.createCell(cellCounter++);
		xlsCell.setCellValue(rs.getDouble("WDR_ENVOY"));
        xlsCell = xlsRow.createCell(cellCounter++);
        xlsCell.setCellValue(rs.getDouble("EFT_WITHDRAW"));
        xlsCell = xlsRow.createCell(cellCounter++);
        xlsCell.setCellValue(rs.getDouble("MONEYBOOKERS_WITHDARW"));
        xlsCell = xlsRow.createCell(cellCounter++);
        xlsCell.setCellValue(rs.getDouble("WDR_WEBMONEY"));
        xlsCell = xlsRow.createCell(cellCounter++);
        xlsCell.setCellValue(rs.getDouble("WDR_DELTAPAY"));
        xlsCell = xlsRow.createCell(cellCounter++);
        xlsCell.setCellValue(rs.getDouble("WDR_CDPAY"));
        xlsCell = xlsRow.createCell(cellCounter++);
        xlsCell.setCellValue(rs.getDouble("WDR_BAROPAY"));
        xlsCell = xlsRow.createCell(cellCounter++);
        xlsCell.setCellValue(rs.getDouble("WDR_CUP"));

		xlsCell = xlsRow.createCell(cellCounter++);
		xlsCell.setCellValue(rs.getDouble("ADMIN_DEPOSITS"));
		xlsCell = xlsRow.createCell(cellCounter++);
		xlsCell.setCellValue(rs.getDouble("ADMIN_WITHDRAWALS"));

		xlsCell = xlsRow.createCell(cellCounter++);
		xlsCell.setCellValue(rs.getDouble("BONUS_DEPOSITS"));
		xlsCell = xlsRow.createCell(cellCounter++);
		xlsCell.setCellValue(rs.getDouble("BONUS_WITHRAWALS"));

		xlsCell = xlsRow.createCell(cellCounter++);
		xlsCell.setCellValue(rs.getDouble("DEPOSIT_BY_COMPANY"));

		xlsCell = xlsRow.createCell(cellCounter++);
		xlsCell.setCellValue(rs.getDouble("CHB_DEPOSITS"));
		xlsCell = xlsRow.createCell(cellCounter++);
		xlsCell.setCellValue(rs.getDouble("CANCEL_DEP"));
		xlsCell = xlsRow.createCell(cellCounter++);
		xlsCell.setCellValue(rs.getDouble("CANCEL_WDR"));
		xlsCell = xlsRow.createCell(cellCounter++);
		xlsCell.setCellValue(rs.getDouble("REVERSE_WDR"));
		
		xlsCell = xlsRow.createCell(cellCounter++);
		xlsCell.setCellValue(rs.getDouble("M_FEE"));
		xlsCell = xlsRow.createCell(cellCounter++);
		xlsCell.setCellValue(rs.getDouble("M_FEE_CANCEL"));

		xlsCell = xlsRow.createCell(cellCounter++);
		xlsCell.setCellValue(rs.getDouble("INSERT_INVESTMENTS"));
		xlsCell = xlsRow.createCell(cellCounter++);
		xlsCell.setCellValue(rs.getDouble("SETTLED_INVESTMENTS"));
		xlsCell = xlsRow.createCell(cellCounter++);
		xlsCell.setCellValue(rs.getDouble("CANCELLED_INVESTMENTS"));
		xlsCell = xlsRow.createCell(cellCounter++);
		xlsCell.setCellValue(rs.getDouble("RESETTLE_INVESTMENT"));

		xlsCell = xlsRow.createCell(cellCounter++);
		xlsCell.setCellValue(rs.getDouble("TAX_PAYMENTS"));

		xlsCell = xlsRow.createCell(cellCounter++);
		xlsCell.setCellValue(rs.getDouble("FIX_BALANCE_DEPOSIT"));
		xlsCell = xlsRow.createCell(cellCounter++);
		xlsCell.setCellValue(rs.getDouble("FIX_BALANCE_WITHDRAW"));
        xlsCell = xlsRow.createCell(cellCounter++);
        xlsCell.setCellValue(rs.getDouble("DEP_FIX_NEGATIVE_BALANCE"));

		xlsCell = xlsRow.createCell(cellCounter++);
		xlsCell.setCellValue(rs.getDouble("closing_tax"));
		xlsCell = xlsRow.createCell(cellCounter++);
		xlsCell.setCellValue(rs.getDouble("closing_balance"));
		xlsCell = xlsRow.createCell(cellCounter++);
		xlsCell.setCellValue(rs.getDouble("CLOSING_BALANCE_CALC"));
		xlsCell = xlsRow.createCell(cellCounter++);
		xlsCell.setCellValue(rs.getDouble("closing_balance") - rs.getDouble("CLOSING_BALANCE_CALC"));
		xlsCell = xlsRow.createCell(cellCounter++);
		xlsCell.setCellValue(rs.getDouble("bh_rec_num"));

		xlsCell = xlsRow.createCell(cellCounter++);
		xlsCell.setCellValue("       ||");
		xlsCell = xlsRow.createCell(cellCounter++);
		xlsCell.setCellValue(rs.getInt("dup_recs_num"));
		xlsCell = xlsRow.createCell(cellCounter++);
		xlsCell.setCellValue(rs.getInt("no_amount_recs_num"));
		xlsCell = xlsRow.createCell(cellCounter++);
		xlsCell.setCellValue(rs.getInt("new_enum_recs_num"));
	}

    public static void closeStatement(final Statement stmt) {
        if (stmt != null) {
            try {
                stmt.close();
            } catch (SQLException ex) {
                log.error(stmt, ex);
            }
        }
    }

    public static void closeResultSet(final ResultSet rs) {
        if (rs != null) {
            try {
                rs.close();
            } catch (SQLException ex) {
                log.error(rs, ex);
            }
        }
    }
    
    private static void deleteFile(File file){
    	try{
    		
    		if(file.exists()){
    			log.info("Begin Delete File:" + file.getName());
    			if(file.delete()){
    				log.info("File Deleted");
    			} else {
    				log.info("File can't deleted");
    			}    			
    		}
    	}catch(Exception e){
 
    		e.printStackTrace();
 
    	}
    }
}
