package il.co.etrader.daily.requested.withdrawals;

import il.co.etrader.bl_managers.TransactionsManagerBase;
import il.co.etrader.bl_vos.Skins;
import il.co.etrader.util.CommonUtil;
import il.co.etrader.util.ConstantsBase;
import il.co.etrader.util.JobUtil;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Hashtable;

import org.apache.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFRichTextString;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import com.anyoption.common.daos.DAOBase;

/**
 * [DEV-1176]- Report for Chinese Requested withdrawals
 * @author EranL
 */
public class DailyRequestedWithdrawalsJob extends JobUtil{

	private static Logger log = Logger.getLogger(DailyRequestedWithdrawalsJob.class);
	
	public static void main(String[] args) throws Exception {
		propFile = args[0];		
		log.info("Starting DailyRequestedWithdrawalsJob");
		SendXlsFile();
		log.info("Finishing DailyRequestedWithdrawalsJob");
	}

	private static void SendXlsFile(){
		HSSFWorkbook xlsWorkbook = new HSSFWorkbook();
		HSSFSheet xlsSheet= null;
		HSSFRow xlsRow = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		Connection con = null;
		int rowCounter = 0;
		String fileName ="Daily_Requested_Withdrawals_Report.xls";
		//Set the date for subject  
		GregorianCalendar gc = new GregorianCalendar();
		gc.set(GregorianCalendar.HOUR_OF_DAY, 0);
		gc.set(GregorianCalendar.MINUTE, 0);
		gc.set(GregorianCalendar.SECOND, 0);
		gc.set(GregorianCalendar.MILLISECOND, 0);
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		Date startDate = gc.getTime();
		String environment = getPropertyByFile("environment");
		String mailSubject = "Daily Chinese requested withdrawals report for " +  sdf.format(startDate) + " " + environment;
		try {
			con = getConnection();
			String sql =" SELECT " +
						"	u.id user_Id, " +
						"	u.first_name, " +
						"	u.last_name, " +
						"	w.beneficiary_name, " +
						"	w.bank_name, " +
						"	w.branch_name, " +
						"	w.account_num || ' ' account_num, " +
						"	t.time_created, " +
						"	t.amount/100 amount, " +
						"	c.name_key Currency, " +
						"	t.amount/100 * rate amount_usd, " +
						"	t.id transaction_id " +
						" FROM " +
						"	users u, " +
						"	transactions t, " +
						"	transaction_types tt, " +
						"	transaction_statuses ts, " +
						"	wires w, " +
						"	currencies c " +
						" WHERE " +
						"	u.id = t.user_id " +
						"	AND t.type_id = tt.id " +
						"	AND t.status_id = ts.id " +
						"	AND t.wire_id = w.id " +
						"	AND c.id = u.currency_id " +
						"	AND u.class_id <> " + ConstantsBase.USER_CLASS_TEST +
						"	AND tt.class_type = " + TransactionsManagerBase.TRANS_TYPE_WIRE_DEPOSIT +
						"	AND ts.id = " + TransactionsManagerBase.TRANS_STATUS_REQUESTED +
						"	AND u.skin_id = " + Skins.SKIN_CHINESE +
						"	AND to_char(t.time_created,'yyyymmdd') <= to_char(sysdate,'yyyymmdd') " +
						"	AND to_char(t.time_created,'yyyymmdd') > to_char(sysdate - 3,'yyyymmdd')";

					log.info("Start Report Sql");
					ps = con.prepareStatement(sql);
					rs = ps.executeQuery();
					log.info("Start Building Report");
					xlsSheet = xlsWorkbook.createSheet("Daily withdraw");
					xlsRow = xlsSheet.createRow(rowCounter++);
					setColumnsNameInSheet(xlsRow);
					while (rs.next()) {
						xlsRow = xlsSheet.createRow(rowCounter++);
						setColumnsValuesInSheet(xlsWorkbook, xlsRow, rs);
					}
					writeToExcel(xlsWorkbook, fileName);

					// send email
			        Hashtable<String,String> server = new Hashtable<String,String>();
			        server.put("url", getPropertyByFile("email.url"));
			        server.put("auth", getPropertyByFile("email.auth"));
			        server.put("user", getPropertyByFile("email.user"));
			        server.put("pass", getPropertyByFile("email.pass"));
			        server.put("contenttype", getPropertyByFile("email.contenttype", "text/html; charset=UTF-8"));

			        Hashtable<String,String> email = new Hashtable<String,String>();
			        email.put("subject", mailSubject);
			        email.put("to", getPropertyByFile("email.to"));
			        email.put("from", getPropertyByFile("email.from"));
			        email.put("body", "The report is attached to this mail");

			        File attachment = new File(fileName);
			        File []attachments={attachment};
			        CommonUtil.sendEmail(server, email, attachments);

				} catch (Exception e) {
					log.fatal("Can't get Daily Chinese requested withdrawals report", e);
				} finally {
					closeResultSet(rs);
					closeStatement(ps);
				}
			try {
				con.close();
			} catch (Exception e) {
				log.error("Can't close",e);
			}				
	}

	private static void writeToExcel(HSSFWorkbook xlsWorkbook, String fileName){
		FileOutputStream fos = null;
		try {
            fos = new FileOutputStream(new File(fileName));
            xlsWorkbook.write(fos);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (fos != null) {
                try {
                    fos.flush();
                    fos.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
	}

	private static void setColumnsNameInSheet(HSSFRow xlsRow){
		int cellCounter = 0;
		HSSFCell xlsCell = null;
		xlsCell = xlsRow.createCell(cellCounter++);
		xlsCell.setCellValue(new HSSFRichTextString("User ID"));
		xlsCell = xlsRow.createCell(cellCounter++);
		xlsCell.setCellValue(new HSSFRichTextString("First Name"));
		xlsCell = xlsRow.createCell(cellCounter++);
		xlsCell.setCellValue(new HSSFRichTextString("Last Name"));
		xlsCell = xlsRow.createCell(cellCounter++);
		xlsCell.setCellValue(new HSSFRichTextString("Beneficiary Name"));
		xlsCell = xlsRow.createCell(cellCounter++);
		xlsCell.setCellValue(new HSSFRichTextString("Bank Name"));
		xlsCell = xlsRow.createCell(cellCounter++);
		xlsCell.setCellValue(new HSSFRichTextString("Branch Name"));
		xlsCell = xlsRow.createCell(cellCounter++);
		xlsCell.setCellValue(new HSSFRichTextString("Account Num"));
		xlsCell = xlsRow.createCell(cellCounter++);
		xlsCell.setCellValue(new HSSFRichTextString("Time Created"));
		xlsCell = xlsRow.createCell(cellCounter++);		
		xlsCell.setCellValue(new HSSFRichTextString("Amount"));
		xlsCell = xlsRow.createCell(cellCounter++);
		xlsCell.setCellValue(new HSSFRichTextString("Currency"));
		xlsCell = xlsRow.createCell(cellCounter++);
		xlsCell.setCellValue(new HSSFRichTextString("Amount USD"));
		xlsCell = xlsRow.createCell(cellCounter++);
		xlsCell.setCellValue(new HSSFRichTextString("Transaction Id"));
	}

	private static void setColumnsValuesInSheet(HSSFWorkbook xlsWorkbook, HSSFRow xlsRow, ResultSet rs) throws SQLException{
		int cellCounter = 0;
		HSSFCell xlsCell = null;
		HSSFCellStyle cellStyle = xlsWorkbook.createCellStyle();
		cellStyle.setWrapText(true);
		xlsCell = xlsRow.createCell(cellCounter++);
		xlsCell.setCellValue(new HSSFRichTextString(rs.getString("user_Id")));
		xlsCell = xlsRow.createCell(cellCounter++);
		xlsCell.setCellValue(rs.getString("first_name"));
		xlsCell = xlsRow.createCell(cellCounter++);
		xlsCell.setCellValue(rs.getString("last_name"));
		xlsCell = xlsRow.createCell(cellCounter++);
		xlsCell.setCellValue(rs.getString("beneficiary_name"));
		xlsCell = xlsRow.createCell(cellCounter++);
		xlsCell.setCellValue(rs.getString("bank_name"));
		xlsCell = xlsRow.createCell(cellCounter++);
		xlsCell.setCellValue(rs.getString("branch_name"));
		xlsCell = xlsRow.createCell(cellCounter++);
		xlsCell.setCellValue(rs.getString("account_num"));
		xlsCell = xlsRow.createCell(cellCounter++);
		xlsCell.setCellValue(returnDateFormat(DAOBase.convertToDate(rs.getTimestamp("time_created"))));		
		xlsCell = xlsRow.createCell(cellCounter++);
		xlsCell.setCellValue(rs.getDouble("amount"));
		xlsCell = xlsRow.createCell(cellCounter++);
		xlsCell.setCellValue(rs.getString("Currency"));
		xlsCell = xlsRow.createCell(cellCounter++);
		xlsCell.setCellValue(rs.getDouble("amount_usd"));
        xlsCell = xlsRow.createCell(cellCounter++);
        xlsCell.setCellValue(rs.getLong("transaction_id"));
        xlsCell = xlsRow.createCell(cellCounter++);
	}
	
	public static String returnDateFormat(Date d) {
		SimpleDateFormat hours = new SimpleDateFormat("HH:mm");
		SimpleDateFormat date = new SimpleDateFormat("dd/MM/yyyy");
		return date.format(d) + "  " + hours.format(d);
	}

}
