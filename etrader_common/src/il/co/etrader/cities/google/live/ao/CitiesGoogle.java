package il.co.etrader.cities.google.live.ao;

import il.co.etrader.util.ConstantsBase;
import il.co.etrader.util.JobUtil;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.TreeMap;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLSession;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import com.anyoption.common.util.ClearingUtil;

/**
 * Retrieve city from google API.
 * LIVE AO - later on we want to send the coordinates that we received from google and spin the Globe.
 * For start we want to do it only for depositors.
 * @author EranL
 *
 */
public class CitiesGoogle extends JobUtil {
	private static Logger log = Logger.getLogger(CitiesGoogle.class);
	public static HashMap<String, Long> userCities;

	public static void main(String[] args) throws Exception {
		log.info("Cities Google API Job started.");
		propFile = args[0];
		Connection con = null;
		ResultSet rs = null;
		PreparedStatement ps = null;
		userCities = new HashMap<String, Long>();
		getCitiesFromUsers(con, ps, rs);
		ValueComparator bvc =  new ValueComparator(userCities);
        TreeMap<String,Long> sorted_map = new TreeMap<String,Long>(bvc);
        sorted_map.putAll(userCities);
		for (String iter : sorted_map.keySet()) {
			String[] temp = iter.split("_");
			String country = temp[0];
			String city = temp[1];
			boolean res = getCoordinatedFromGoogle(con, ps, rs, country, city);
			if (!res) {
				log.info("Over query limit from google");
				break;
			}
		}
		log.info("Cities Google API Job finished.");
	}

	public static boolean getCoordinatedFromGoogle(Connection con, PreparedStatement ps, ResultSet rs,
			String userCountry, String userCity) throws IOException, ParserConfigurationException, SAXException, SQLException {
		String URL = "http://maps.googleapis.com/maps/api/geocode/xml?sensor=false&address=" + userCountry.replace(" ", "+") + "+" + userCity.replace(" ", "+");
		String response = executeGETRequest(URL, ConstantsBase.EMPTY_STRING, false);
		try {
			Document doc = parseXMLToDocument(response);
			Element root = (Element) ClearingUtil.getNode(doc.getDocumentElement(), "");
			String status = ClearingUtil.getElementValue(root, "status/*");
			if (null != status && status.equalsIgnoreCase("OVER_QUERY_LIMIT")) {
				return false;
			}
			String latitude = ConstantsBase.EMPTY_STRING;
			String longitude = ConstantsBase.EMPTY_STRING;
			String googleCountry = ConstantsBase.EMPTY_STRING;
			String googleCity = ConstantsBase.EMPTY_STRING;
			NodeList resultNodes = root.getElementsByTagName("result");
			if (null != resultNodes && resultNodes.getLength() > 0) {
				Node rootNode = resultNodes.item(0);
				Node latNode = ClearingUtil.getNode((Element)rootNode, "/geometry/location/lat");
				Node lngNode = ClearingUtil.getNode((Element)rootNode, "/geometry/location/lng");
				latitude = latNode.getFirstChild().getNodeValue();
				longitude = lngNode.getFirstChild().getNodeValue();
				//search for country and city from google xml
				for (int i = 0; i < resultNodes.getLength(); i++) {
					Node node = resultNodes.item(i);
					NodeList resultChilds = node.getChildNodes();
					for (int k = 0; k < resultChilds.getLength(); k++) {
						Node resultNode = resultChilds.item(k);
						String nodeName = resultNode.getNodeName();
						if (nodeName.equalsIgnoreCase("address_component")) {
							NodeList addressComponentNodes = resultNode.getChildNodes();
							String addressComponentValue = ""; //can be country or city
							for (int s = 0; s < addressComponentNodes.getLength(); s++) {
								Node addressComponentNode = addressComponentNodes.item(s);
								String addressComponentNodeName = addressComponentNode.getNodeName();
								if (addressComponentNodeName.equalsIgnoreCase("long_name")) {
									addressComponentValue = addressComponentNode.getFirstChild().getNodeValue();
								} else if (addressComponentNodeName.equalsIgnoreCase("type")) {
									String value = addressComponentNode.getFirstChild().getNodeValue();
									if (null != value && value.equalsIgnoreCase("country")) {
										googleCountry = addressComponentValue;
										break;
									} else if (null != value && googleCity.isEmpty()) {
										googleCity = addressComponentValue;
										break;
									}
								}
							}
						}
					}
				}				
			}			
			boolean cityExists = checkExistingCity(con, ps, rs, userCountry, userCity, googleCountry, googleCity, latitude, longitude);
			log.info("country=" + userCountry + " city=" + userCity  + " googleCountry=" + googleCountry + " googleCity=" + googleCity + " lng=" + latitude + " lat=" + longitude + " exists:" + cityExists);			
			if (!cityExists) {					
				long id = insertToCitiesGoogleTable(con, ps, rs, userCountry, userCity, googleCountry, googleCity, latitude, longitude, response, status);
				log.debug("Insert record id:" + id);
			} 
			Thread.sleep(2000);
		} catch (Exception e) {
			log.error(" ERROR while parsing google maps response." + e);
		}
		return true;
	}
	
	private static boolean checkExistingCity(Connection con, PreparedStatement ps, ResultSet rs, String userCountry, String userCity
			   , String googleCountry, String googleCity, String latitude, String longitude) throws SQLException {
		  try {
			  	con = getConnection();
				String sql =
					" SELECT" +
					"	* " +
					" FROM" +
					"	cities_google_coordinates " +
					" WHERE " +
					"	user_country_name = ? " +
					"	AND user_city_name = ? " +
					"	AND google_country_name = ? " +
					"	AND google_city_name = ? " +
					"	AND google_city_latitude = ? " +
					"	AND google_city_longtitude = ? ";

				ps = con.prepareStatement(sql);
				ps.setString(1, userCountry);
				ps.setString(2, userCity);
				ps.setString(3, googleCountry);
				ps.setString(4, googleCity);
				ps.setString(5, latitude);
				ps.setString(6, longitude);
				rs = ps.executeQuery();
				while (rs.next()) {
					return true;
				}
		    } finally {
		    	try {
					ps.close();
				} catch (Exception e) {
					log.log(Level.ERROR,"Can't close",e);
				}
		    	try {
		    		con.close();
				} catch (Exception e) {
					log.log(Level.ERROR,"Can't close",e);
				}

			}		
		  return false;
	}

	private static void getCitiesFromUsers(Connection con, PreparedStatement ps, ResultSet rs) throws SQLException {
		log.debug("Going to get cities from users");
		try {
			con = getConnection();
			String sql =" SELECT " +
						"	u.id, " +
						"   u.user_name, " +
						"   u.time_created, " +
						"   c.country_name, " +
						"   u.city_name " +
						" FROM " +
						"	users  u, " +
						"   countries c, " +
						"	transactions t, " +
						"	transaction_types tt " +
						" WHERE " +
						"   u.id = t.user_id " +
						"   AND c.id = u.country_id " +
						"   AND t.type_id = tt.id " +
						"   AND u.class_id <> 0 " +
						"   AND u.is_active = 1 " +
						"   AND tt.class_type = 1 " +
						"   AND u.skin_id <> 1 " +
						"	AND u.city_name is not null " +
						"   AND t.status_id in (2,7,8) " +
						"   AND u.id not in ( " +
						"				      SELECT " +
						"			              u2.id " +
						"				      FROM " +
						"			              users u2, " +
						"			              cities_google_coordinates cgc " +
						"			          WHERE " +
						"			              upper(cgc.user_city_name) = upper(u2.city_name) " +
						"     				)" +
						" GROUP BY " +
						"	u.id, " +
						"	u.user_name, " +
						"	u.time_created, " +
						"	c.country_name, " +
						"	u.city_name " +
						" ORDER BY u.id desc ";

			ps = con.prepareStatement(sql);
			rs = ps.executeQuery();
			while (rs.next()) {
				String country_name = rs.getString("country_name");
				String city_name = rs.getString("city_name");
				if (city_name == null || country_name == null) {
					continue;
				}
				String cityAndCountry = country_name.toUpperCase() + "_" + city_name.toUpperCase();
				if (userCities.get(cityAndCountry) == null) {
					userCities.put(cityAndCountry, new Long(1));
				} else {
					long value = userCities.get(cityAndCountry);
					value++;
					userCities.put(cityAndCountry, new Long(value));
				}
			}

		} catch (Exception e) {
			log.error("Exception while getting cities from users " + e);
		} finally {
			try {
				rs.close();
			} catch (Exception e) {
				log.log(Level.ERROR,"Can't close",e);
			}
			try {
				ps.close();
			} catch (Exception e) {
				log.log(Level.ERROR,"Can't close",e);
			}
			try {
				con.close();
			} catch (Exception e) {
				log.log(Level.ERROR,"Can't close",e);
			}
		}
		log.debug("Finish getting cities from users");
	}

    /**
     * Create DOM <code>Document</code> from XML <code>String</code>.
     *
     * @param xml the XML to parse
     * @return New DOM <code>Document</code> with the parsed XML.
     * @throws ParserConfigurationException
     * @throws SAXException
     * @throws IOException
     */
    private static Document parseXMLToDocument(String xml) throws ParserConfigurationException, SAXException, IOException {
        DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
        byte[] xmldata = xml.getBytes("UTF-8");
        ByteArrayInputStream bais = new ByteArrayInputStream(xmldata);
        Document resp = docBuilder.parse(bais);
        try {
        	bais.close();
        } catch (Exception e) {
        }
        return resp;
    }

   private static long insertToCitiesGoogleTable(Connection con, PreparedStatement ps, ResultSet rs, String userCountry, String userCity
		   , String googleCountry, String googleCity, String latitude, String longitude, String response, String status) throws SQLException {
	   	long id = 0;
		  try {
			  	con = getConnection();
				String sql =
					" INSERT INTO " +
					"	cities_google_coordinates(id, time_created, user_country_name, user_city_name, " +
					"	google_country_name, google_city_name, google_city_latitude, google_city_longtitude, google_xml, google_status) " +
					" VALUES " +
					"	(seq_cities_google_coordinates.nextval, sysdate,?,?,?,?,?,?,?,?) ";


				ps = con.prepareStatement(sql);
				ps.setString(1, userCountry);
				ps.setString(2, userCity);
				ps.setString(3, googleCountry);
				ps.setString(4, googleCity);
				ps.setString(5, latitude);
				ps.setString(6, longitude);
				ps.setString(7, response);
				ps.setString(8, status);
				ps.executeUpdate();
				id = getSeqCurValue(con, "seq_cities_google_coordinates");
		    } finally {
		    	try {
					ps.close();
				} catch (Exception e) {
					log.log(Level.ERROR,"Can't close",e);
				}
		    	try {
		    		con.close();
				} catch (Exception e) {
					log.log(Level.ERROR,"Can't close",e);
				}

			}
		  return id;
   }
   
   protected static long getSeqCurValue(Connection con, String seq) throws SQLException {
       PreparedStatement ps = null;
       ResultSet rs = null;
       try {
           ps = con.prepareStatement("select " + seq + ".currval from dual");
           rs = ps.executeQuery();
           if (rs.next()) {
               return rs.getLong(1);
           }
       } finally {
           closeResultSet(rs);
           closeStatement(ps);
       }
       return 0;
   }

   /**
    * Execute GET request and return the response as string.
    *
    * @param url the URL to POST to
    * @param queryRequest the request body
    * @param verifyHostname
    * @return String with the response from the server.
    * @throws IOException
    */
   public static String executeGETRequest(
           String url,
           String queryRequest,
           boolean verifyHostname) throws IOException {
       if (log.isEnabledFor(Level.DEBUG)) {
           log.log(Level.DEBUG, url);
           log.log(Level.DEBUG, queryRequest);
       }

       HostnameVerifier defaultHV = null;
       if (!verifyHostname) {
           // save the default one
           defaultHV = HttpsURLConnection.getDefaultHostnameVerifier();

           // create veirier that accept all
           HostnameVerifier hv = new HostnameVerifier() {
               public boolean verify(String urlHostName, SSLSession session) {
                   log.log(Level.WARN, "URL Host: " + urlHostName + " vs. " + session.getPeerHost());
                   return true;
               }
           };
           // and install it
           HttpsURLConnection.setDefaultHostnameVerifier(hv);
       }

       // set the connect and read timeouts for the HttpURLConnection
       // this is a Sun JVM specific way to do it
       System.setProperty("sun.net.client.defaultConnectTimeout", "30000");
       System.setProperty("sun.net.client.defaultReadTimeout", "30000");
       if (log.isEnabledFor(Level.DEBUG)) {
           String ls = System.getProperty("line.separator");
           log.log(Level.DEBUG,
                   ls +
                   "sun.net.client.defaultConnectTimeout: " +
                   System.getProperty("sun.net.client.defaultConnectTimeout") +
                   ls +
                   "sun.net.client.defaultReadTimeout: " +
                   System.getProperty("sun.net.client.defaultReadTimeout"));
       }
       String response = null;
       int trys = 1;
       while (true) {
       	long startTime = System.currentTimeMillis();
       	OutputStreamWriter writer = null;
           BufferedReader reader = null;
           try {
           	URL u = new URL(url);
	            HttpURLConnection conn = (HttpURLConnection)u.openConnection();
	            conn.setDoOutput(true);
	            writer = new OutputStreamWriter(conn.getOutputStream());
	            writer.write(queryRequest);
	            writer.flush();
	            try {
	            	writer.close();
               } catch (Exception e) {
                   log.log(Level.ERROR, "Can't close writer.", e);
               }
               if (log.isEnabledFor(Level.DEBUG)) {
                   log.log(Level.DEBUG, "Request sent.");
               }
               int respCode = conn.getResponseCode();
               if (log.isEnabledFor(Level.DEBUG)) {
                   log.log(Level.DEBUG, "ResponseCode: " + respCode);
               }
               StringBuffer xmlResp = new StringBuffer();
               String line = null;
               reader = new BufferedReader(new InputStreamReader(conn.getInputStream(), "UTF-8"));
               while ((line = reader.readLine()) != null) {
                   xmlResp.append(line);
               }
               reader.close();
               response = xmlResp.toString();

               if (log.isEnabledFor(Level.DEBUG)) {
                   log.log(Level.DEBUG, "Response received.");
                   log.log(Level.DEBUG, response);
               }
           } catch (IOException ioe) {
               if (trys < 3 &&
                       System.currentTimeMillis() - startTime < 10000) {
                   int sleep = 200;
                   if (log.isEnabledFor(Level.INFO)) {
                       log.log(Level.INFO, "Communication error", ioe);
                       log.log(Level.INFO, "Sleep " + sleep);
                   }
                   try {
                       Thread.sleep(sleep);
                   } catch (Exception e) {
                       log.log(Level.ERROR, "Sleep interrupted", e);
                   }
                   trys++;
                   if (log.isEnabledFor(Level.INFO)) {
                       log.log(Level.INFO, "Try " + trys);
                   }
                   continue;
               } else {
                   log.log(Level.ERROR, "Give up");
                   throw ioe;
               }
           } finally {
               if (null != reader) {
                   try {
                   	reader.close();
                   } catch (Exception e) {
                       log.log(Level.ERROR, "Can't close the buffered reader.", e);
                   }
               }
               if (null != writer) {
                   try {
                       writer.close();
                   } catch (Exception e) {
                       log.log(Level.ERROR, "Can't close the writer.", e);
                   }
               }
           }
           break;
       }

       if (!verifyHostname) {
           // restore the default verifier
           HttpsURLConnection.setDefaultHostnameVerifier(defaultHV);
       }

       return response;
   }
}

