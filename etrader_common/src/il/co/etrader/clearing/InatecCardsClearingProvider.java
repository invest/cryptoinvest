//package il.co.etrader.clearing;
//
//import javax.xml.parsers.DocumentBuilder;
//import javax.xml.parsers.DocumentBuilderFactory;
//
//import org.apache.log4j.Level;
//import org.apache.log4j.Logger;
//import org.w3c.dom.Document;
//import org.w3c.dom.Element;
//
//import com.anyoption.common.util.ClearingUtil;
//
//
///**
// * Implement the communication with Inatec.
// * Debit Processing Gateway
// *
// * @author Eran
// */
//public class InatecCardsClearingProvider extends InatecClearingProvider {
//
//	private static final Logger log = Logger.getLogger(InatecClearingProvider.class);
//
//
//    /**
//     * Create Inatec cards request XML document.
//     *
//     * @return New Document with the Inatec XML structure.
//     */
//    protected Document createRequest() {
//        Document request = null;
//        try {
//            DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
//            DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
//            request = docBuilder.newDocument();
//
//            Element root = request.createElement("InatecGW");
//            request.appendChild(root);
//            ClearingUtil.setAttribute(request, "", "xmlns", "https://www.taurus21.com/schema/cc");
//            ClearingUtil.setAttribute(request, "", "xmlns:xsi", "https://www.w3.org/2001/XMLSchema-instance");
//            ClearingUtil.setAttribute(request, "", "xsi:schemaLocation", "https://www.taurus21.com/schema/cc https://www.taurus21.com/schema/cc/mina-tenvelope.xsd");
//            ClearingUtil.setAttribute(request, "", "version", "1.00.000");
//
//        } catch (Exception e) {
//            log.log(Level.ERROR, "Failed to create request document.", e);
//        }
//        return request;
//    }
//
//}
