//package il.co.etrader.clearing;
//
//
//import il.co.etrader.bl_vos.UserBase;
//
//import java.io.Serializable;
//import java.math.BigDecimal;
//import java.sql.SQLException;
//
//import org.apache.log4j.Logger;
//import org.w3c.dom.Document;
//import org.w3c.dom.Element;
//
//import com.anyoption.common.beans.Transaction;
//import com.anyoption.common.beans.base.BaroPayRequest;
//import com.anyoption.common.clearing.ClearingInfo;
//import com.anyoption.common.util.ClearingUtil;
//
///**
// * BaroPayInfo clearing info class
// * @author Eyal O
// *
// */
//
///**
// * @author eyalo
// *
// */
//public class BaroPayInfo extends ClearingInfo implements Serializable {
//
//	private static final long serialVersionUID = 1L;
//
//	private static final Logger log = Logger.getLogger(BaroPayInfo.class);
//	private final String STATUS_URL = "jsp/baroPayStatusURL.jsf";
//	private final String STATUS_WITHDRAW_URL = "jsp/baroPayWithdrawStatusURL.jsf";
//	private String statusURL;
//	private String landLinePhone;
//	private String statusWithdrawURL;
//	private BaroPayRequest baroPayRequest;
//
//	//status URL fields
//	private String transaction_id;
//	private String user_id;
//	private String redirectURL;
//	private String merchantAccount;
//	private String status;
//	private String merchant_id;
//	private String senderName;
//	private String depositAmount;
//	private String xmlResponse;
//
//	public BaroPayInfo() {
//		this.baroPayRequest = new BaroPayRequest();
//    }
//
//	public BaroPayInfo(UserBase user, Transaction t, String language, String hpURL, String clientAccount,
//								String amount, boolean isLive, String imagesDomain, String senderName, String landLinePhone) throws SQLException{
//    	super(user, t.getAmount(), t.getTypeId(), t.getId(), t.getIp(), t.getAcquirerResponseId());
//
//    	if (isLive){
//    		this.statusURL = hpURL + STATUS_URL;
//    	} else {
//    		this.statusURL = "http://www.testenv.anyoption.com/" + STATUS_URL;
//    	}
//    	if (isLive){
//    		this.statusWithdrawURL = hpURL + STATUS_WITHDRAW_URL;
//    	} else {
//    		this.statusWithdrawURL = "http://www.testenv.anyoption.com/" + STATUS_WITHDRAW_URL;
//    	}
//    	this.depositAmount = amount;
//    	this.senderName = senderName;
//    	this.landLinePhone = landLinePhone;
//    	this.baroPayRequest = new BaroPayRequest();
//    }
//
//	/**
//	 * @return the redirectURL
//	 */
//	public String getRedirectURL() {
//		return redirectURL;
//	}
//
//	/**
//	 * @param redirectURL the redirectURL to set
//	 */
//	public void setRedirectURL(String redirectURL) {
//		this.redirectURL = redirectURL;
//	}
//
//	/**
//	 * @return the merchantAccount
//	 */
//	public String getMerchantAccount() {
//		return merchantAccount;
//	}
//
//	/**
//	 * @param merchantAccount the merchantAccount to set
//	 */
//	public void setMerchantAccount(String merchantAccount) {
//		this.merchantAccount = merchantAccount;
//	}
//	
//	/**
//	 * @return the status
//	 */
//	public String getStatus() {
//		return status;
//	}
//
//	/**
//	 * @param status the status to set
//	 */
//	public void setStatus(String status) {
//		this.status = status;
//	}
//	
//	/**
//	 * @return the merchant_id
//	 */
//	public String getMerchant_id() {
//		return merchant_id;
//	}
//
//	/**
//	 * @param merchant_id the merchant_id to set
//	 */
//	public void setMerchant_id(String merchant_id) {
//		this.merchant_id = merchant_id;
//	}
//
//	/**
//	 * @return the senderName
//	 */
//	public String getSenderName() {
//		return senderName;
//	}
//
//	/**
//	 * @param senderName the senderName to set
//	 */
//	public void setSenderName(String senderName) {
//		this.senderName = senderName;
//	}
//	
//	/**
//	 * @return the depositAmount
//	 */
//	public String getDepositAmount() {
//		return depositAmount;
//	}
//
//	/**
//	 * @param depositAmount the depositAmount to set
//	 */
//	public void setDepositAmount(String depositAmount) {
//		this.depositAmount = depositAmount;
//	}
//	
//	/**
//	 * @return the transaction_id
//	 */
//	public String getTransaction_id() {
//		return transaction_id;
//	}
//
//	/**
//	 * @param transaction_id the transaction_id to set
//	 */
//	public void setTransaction_id(String transaction_id) {
//		this.transaction_id = transaction_id;
//	}
//
//	/**
//	 * @return the user_id
//	 */
//	public String getUser_id() {
//		return user_id;
//	}
//
//	/**
//	 * @param user_id the user_id to set
//	 */
//	public void setUser_id(String user_id) {
//		this.user_id = user_id;
//	}
//
//	/**
//	 * @return the statusURL
//	 */
//	public String getStatusURL() {
//		return statusURL;
//	}
//
//	/**
//	 * @param statusURL the statusURL to set
//	 */
//	public void setStatusURL(String statusURL) {
//		this.statusURL = statusURL;
//	}
//	
//	/**
//	 * @return the landLinePhone
//	 */
//	public String getLandLinePhone() {
//		return landLinePhone;
//	}
//
//	/**
//	 * @param landLinePhone the landLinePhone to set
//	 */
//	public void setLandLinePhone(String landLinePhone) {
//		this.landLinePhone = landLinePhone;
//	}
//	
//	/**
//	 * @return the xmlResponse
//	 */
//	public String getXmlResponse() {
//		return xmlResponse;
//	}
//
//	/**
//	 * @param xmlResponse the xmlResponse to set
//	 */
//	public void setXmlResponse(String xmlResponse) {
//		this.xmlResponse = xmlResponse;
//	}
//	
//	/**
//	 * @return the statusWithdrawURL
//	 */
//	public String getStatusWithdrawURL() {
//		return statusWithdrawURL;
//	}
//
//	/**
//	 * @param statusWithdrawURL the statusWithdrawURL to set
//	 */
//	public void setStatusWithdrawURL(String statusWithdrawURL) {
//		this.statusWithdrawURL = statusWithdrawURL + STATUS_WITHDRAW_URL;
//	}
//	
//	/**
//	 * @return the baroPayRequest
//	 */
//	public BaroPayRequest getBaroPayRequest() {
//		return baroPayRequest;
//	}
//
//	/**
//	 * @param baroPayRequest the baroPayRequest to set
//	 */
//	public void setBaroPayRequest(BaroPayRequest baroPayRequest) {
//		this.baroPayRequest = baroPayRequest;
//	}
//	
//	/**
//	 * @return the withdarwAmount
//	 */
//	public String getFullAmountStr() {
//		return String.valueOf(new BigDecimal(amount).divide(new BigDecimal(100)));
//	}
//	
//	/**
//	 * Handle status URL from BaroPay
//	 * @param requestParameterMap
//	 * @return
//	 */
//	public boolean handleStatusUrl(String responseXML){
//		try {
//			log.info("handleStatusUrl...");
//			log.info("responseXML: " + responseXML);
//			Document respDoc = ClearingUtil.parseXMLToDocument(responseXML);
//			log.info("respDoc: " + respDoc);
//	        Element root = (Element) ClearingUtil.getNode(respDoc.getDocumentElement(), "");
//	        log.info("root: " + root);
//	        transaction_id = ClearingUtil.getElementValue(root, "DepositID/*");
//	        user_id = ClearingUtil.getElementValue(root, "MemberCode/*");
//	        currencySymbol = ClearingUtil.getElementValue(root, "Currency/*");
//	        depositAmount = ClearingUtil.getElementValue(root, "Amount/*");
//	        providerTransactionId = ClearingUtil.getElementValue(root, "PaymentID/*");
//	        result = ClearingUtil.getElementValue(root, "Transresult/*");
//			
//			String TAB = "\n";
//				log.info( TAB + "BaroPay status_url fields:" + TAB
//					+ "transaction_id=" + transaction_id + TAB
//					+ "user_id(MemberCode)=" + user_id + TAB
//					+ "currency_symbol=" + currencySymbol + TAB
//					+ "deposit_amount=" + depositAmount + TAB
//					+ "provider_transaction_id=" + providerTransactionId + TAB
//					+ "resault(Transresult)=" + result);
//		} catch (Exception e){
//			log.error("Error retrieving data from BaroPay status URL " + e);
//			return false;
//		}
//		return true;
//	}
//
//	public String returnStatusString(){
//		if (status.equals(BaroPayClearingProvider.STATUS_PROCESSED_CODE)){
//			return BaroPayClearingProvider.STATUS_PROCESSED_STRING;
//		} else if (status.equals(BaroPayClearingProvider.STATUS_CANCELLED_CODE)){
//			return BaroPayClearingProvider.STATUS_CANCELLED_STRING;
//		} else if (status.equals(BaroPayClearingProvider.STATUS_FAILED_CODE)){
//			return BaroPayClearingProvider.STATUS_FAILED_STRING;
//		} else if (status.equals(BaroPayClearingProvider.STATUS_PENDING_CODE)){
//			return BaroPayClearingProvider.STATUS_PENDING_STRING;
//		} else if (status.equals(BaroPayClearingProvider.STATUS_CHARGEBACK_CODE)){
//			return BaroPayClearingProvider.STATUS_CHARGEBACK_STRING;
//		}
//		return null;
//	}
//}