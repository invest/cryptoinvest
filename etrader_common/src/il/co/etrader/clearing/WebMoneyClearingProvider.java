//package il.co.etrader.clearing;
//
//import com.anyoption.common.clearing.ClearingException;
//import com.anyoption.common.clearing.ClearingInfo;
//import com.anyoption.common.clearing.ClearingProvider;
//import com.anyoption.common.clearing.ClearingProviderConfig;
//
///**
// * Implement the communication with WebMoney
// *
// * @author Eran
// */
//public class WebMoneyClearingProvider extends ClearingProvider {
//
//	/**  The field is used only in the test mode. It may have one of the following values:
//		0 or empty: All test payments will be successful;
//		1: All test payments will fail;
//		2: 80% of test payments will be successful, 20% of test payments will fail.*/
//	private String lmi_sim_mode;
//
//    public void authorize(ClearingInfo info) throws ClearingException {
//    	throw new ClearingException("Unsupported operation.");
//    }
//
//    public void capture(ClearingInfo info) throws ClearingException {
//    	throw new ClearingException("Unsupported operation.");
//    }
//
//    public void enroll(ClearingInfo info) throws ClearingException {
//    	throw new ClearingException("Unsupported operation.");
//    }
//
//    public void purchase(ClearingInfo info) throws ClearingException {
//    	throw new ClearingException("Unsupported operation.");
//    }
//
//    public void withdraw(ClearingInfo info) throws ClearingException {
//    	throw new ClearingException("Unsupported operation.");
//    }
//
//    public void bookback(ClearingInfo info) throws ClearingException {
//    	throw new ClearingException("Unsupported operation.");
//    }
//
//    public boolean isSupport3DEnrollement() {
//        return false;
//    }
//
//    /**
//     * Set clearing provider configuration.
//     *
//     * @throws ClearingException
//     */
//    public void setConfig(ClearingProviderConfig config) throws ClearingException {
//    	super.setConfig(config);
//    	lmi_sim_mode = config.getProps();
//    }
//
//    public void setProviderDetails(WebMoneyInfo info) {
//    	info.setRedirectURL(url);
//    	info.setLmi_payee_purse(username);
//    	info.setLmi_sim_mode(lmi_sim_mode);
//    }
//
//	@Override
//	public void cancel(ClearingInfo info) throws ClearingException {
//		// TODO Auto-generated method stub
//		
//	}
//
//}