package il.co.etrader.clearing.walpay;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;


public class MerchantRequestBuilder {

	private static String[] usa_states = {"AL","AK","AZ","AR","CA","CO","CT","DE","FL","GA","HI","ID","IL","IN","IA","KS","KY","LA","ME","MD","MA","MI","MN","MS","MO","MT","NE","NV","NH","NJ","NM","NY","NC","ND","OH","OK","OR","PA","RI","SC","SD","TN","TX","UT","VT","VA","WA","WV","WI","WY"};

	//ISO 4217
	//public static int CURRENCY_CODE_EUR = 978; 
	//public static int CURRENCY_CODE_USD = 840;
	//public static int CURRENCY_CODE_GBP = 826;
	
	public enum SUPPORTED_CURRENCIES  {EUR(978), USD(840), GBP(826);
		private int value;    

		private SUPPORTED_CURRENCIES(int value) {
			this.value = value;
		}

		public int getValue() {
			return value;
		}
	}
	
	// chapter 25.1 of the spec
	private static SimpleDateFormat merchantDateTimeFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"); 
	private static SimpleDateFormat DOBDateTimeFormat = new SimpleDateFormat("yyyy-MM-dd");
	
	/**
	 * Anyoption account to access walpay API 
	 */
	public static MerchantInfo createMerchantInfo(String merchantName, String merchantPin) {
		MerchantInfo mInfo = new MerchantInfo();
		mInfo.setMerchantName(merchantName);
		mInfo.setMerchantPIN(merchantPin);
		return mInfo;
	}
	
	/**
	 * @param orderDescription	Transaction description 
	 * @param amount 			Transaction Amount - Integer amount of the smallest currency unit, $45.21 = 4521 
	 * @param orderID			Merchant unique transaction identifier 
	 */
	public static TransactionInfo createAuthTransactionInfo(SUPPORTED_CURRENCIES currency, String orderDescription, long amount, String orderID) {
		TransactionInfo tInfo = new TransactionInfo();
		tInfo.setAmount(amount);
		tInfo.setCurrencyCode(currency.getValue());
		tInfo.setMerchantDateTime(merchantDateTimeFormat.format(Calendar.getInstance().getTime()));
		tInfo.setOrderDescription(orderDescription);
		tInfo.setOrderID(orderID);
		return tInfo;
	}
	
	
	/**
	 * @param orderDescription	Transaction description 
	 * @param amount 			Transaction Amount - Integer amount of the smallest currency unit, $45.21 = 4521 
	 * @param orderID			Merchant unique transaction identifier 
	 */
	public static TransactionInfo createTransactionInfo(SUPPORTED_CURRENCIES currency, long amount, String orderID) {
		TransactionInfo tInfo = new TransactionInfo();
		tInfo.setAmount(amount);
		tInfo.setCurrencyCode(currency.getValue());
		tInfo.setOrderID(orderID);
		return tInfo;
	}
	
	
	/**
	 * @param accountNumber Unique account number assigned by the merchant to identify a cardholder's account or cardholder's email address
	 * @param eMail
	 * @param ip
	 * @param address
	 * @param city
	 * @param countryA2 ISO 3166-1-alpha-2 country code 
	 * @param dob date of birth
	 * @param name
	 * @param surname
	 * @param telefon
	 * @param zip
	 */
	public static ClientInfo createClientInfo(String accountNumber, String eMail, String ip, String address, String city,  String countryA2, Date dob, String name, String surname, String telefon, String zip, Long stateId) {
		ClientInfo cInfo = new ClientInfo();
		cInfo.setAccountNumber(accountNumber);
		cInfo.setEmail(eMail);
		cInfo.setAddress(address);
		cInfo.setCity(city);
		cInfo.setClientIP(ip);
		cInfo.setCountry(countryA2);
		cInfo.setDOB(DOBDateTimeFormat.format(dob));		
		cInfo.setName(name);
		cInfo.setSurname(surname);
		cInfo.setTelephone(telefon);
		cInfo.setZip(zip);
		if(stateId != null) {
			if("US".equals(countryA2)) {
				int id = stateId.intValue()-1;
				cInfo.setState(usa_states[Math.max(id, 0)]);
			} else if ("GB".equals(countryA2)) {
				cInfo.setState("EN");
			} else if ("IE".equals(countryA2)) {
				cInfo.setState("DN");
			} else if ("CA".equals(countryA2)) {
				cInfo.setState("ON");
			}
			
		}
		return cInfo;
	}
	
	/**
	 * @param cardHolderName	Name on card
	 * @param pan				Card number 
	 * @param cvc2				four-digit code printed on the front side of the card above the number
	 * @param expiryDate		Card expiry date  
	 * @return
	 */
	public static PaymentInfo createPaymentInfo(String cardHolderName, String pan, String cvc2, String expiryDate) {
		PaymentInfo pInfo = new PaymentInfo();
		pInfo.setCardHolderName(cardHolderName);
		pInfo.setPAN(pan); 
		pInfo.setCVC2(cvc2);
		// format "MM-yyyy" chapter 25.1 of the spec
		pInfo.setExpiryDate(expiryDate);
		pInfo.setIssueNumber("");
		pInfo.setStartDate("");
		return pInfo;
	}
	
}
