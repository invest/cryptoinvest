//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, vJAXB 2.1.10 in JDK 6 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2013.01.16 at 04:34:29 PM EET 
//


package il.co.etrader.clearing.walpay;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.bg.anyoption.walpay package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _PAN_QNAME = new QName("", "PAN");
    private final static QName _ProcessorDescription_QNAME = new QName("", "ProcessorDescription");
    private final static QName _Telephone_QNAME = new QName("", "Telephone");
    private final static QName _OrderID_QNAME = new QName("", "OrderID");
    private final static QName _DOB_QNAME = new QName("", "DOB");
    private final static QName _ProcessorErrorDescription_QNAME = new QName("", "ProcessorErrorDescription");
    private final static QName _AddressCode_QNAME = new QName("", "AddressCode");
    private final static QName _OrderDescription_QNAME = new QName("", "OrderDescription");
    private final static QName _MerchantAccount_QNAME = new QName("", "MerchantAccount");
    private final static QName _StandardCode_QNAME = new QName("", "StandardCode");
    private final static QName _CVC2Code_QNAME = new QName("", "CVC2Code");
    private final static QName _IssueNumber_QNAME = new QName("", "IssueNumber");
    private final static QName _ProcessorErrorNo_QNAME = new QName("", "ProcessorErrorNo");
    private final static QName _TransactionID_QNAME = new QName("", "TransactionID");
    private final static QName _StartDate_QNAME = new QName("", "StartDate");
    private final static QName _RiskCode_QNAME = new QName("", "RiskCode");
    private final static QName _CardHolderName_QNAME = new QName("", "CardHolderName");
    private final static QName _ClientIP_QNAME = new QName("", "ClientIP");
    private final static QName _Zip_QNAME = new QName("", "Zip");
    private final static QName _ProcessorCode_QNAME = new QName("", "ProcessorCode");
    private final static QName _ResponseDescription_QNAME = new QName("", "ResponseDescription");
    private final static QName _Amount_QNAME = new QName("", "Amount");
    private final static QName _CVC2_QNAME = new QName("", "CVC2");
    private final static QName _AuthorisationCode_QNAME = new QName("", "AuthorisationCode");
    private final static QName _AccountNumber_QNAME = new QName("", "AccountNumber");
    private final static QName _ResponseCode_QNAME = new QName("", "ResponseCode");
    private final static QName _Country_QNAME = new QName("", "Country");
    private final static QName _CurrencyCode_QNAME = new QName("", "CurrencyCode");
    private final static QName _MerchantName_QNAME = new QName("", "MerchantName");
    private final static QName _City_QNAME = new QName("", "City");
    private final static QName _RiskDescription_QNAME = new QName("", "RiskDescription");
    private final static QName _ErrorNo_QNAME = new QName("", "ErrorNo");
    private final static QName _Source_QNAME = new QName("", "Source");
    private final static QName _Name_QNAME = new QName("", "Name");
    private final static QName _ExpiryDate_QNAME = new QName("", "ExpiryDate");
    private final static QName _Email_QNAME = new QName("", "Email");
    private final static QName _State_QNAME = new QName("", "State");
    private final static QName _MerchantPIN_QNAME = new QName("", "MerchantPIN");
    private final static QName _BillingDescriptor_QNAME = new QName("", "BillingDescriptor");
    private final static QName _RefID_QNAME = new QName("", "RefID");
    private final static QName _Address_QNAME = new QName("", "Address");
    private final static QName _ErrorDescription_QNAME = new QName("", "ErrorDescription");
    private final static QName _StandardDescription_QNAME = new QName("", "StandardDescription");
    private final static QName _Surname_QNAME = new QName("", "Surname");
    private final static QName _MerchantDateTime_QNAME = new QName("", "MerchantDateTime");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.bg.anyoption.walpay
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link PaymentInfo }
     * 
     */
    public PaymentInfo createPaymentInfo() {
        return new PaymentInfo();
    }

    /**
     * Create an instance of {@link MerchantFinalResponse }
     * 
     */
    public MerchantFinalResponse createMerchantFinalResponse() {
        return new MerchantFinalResponse();
    }

    /**
     * Create an instance of {@link TransactionInfo }
     * 
     */
    public TransactionInfo createTransactionInfo() {
        return new TransactionInfo();
    }

    /**
     * Create an instance of {@link ClientInfo }
     * 
     */
    public ClientInfo createClientInfo() {
        return new ClientInfo();
    }

    /**
     * Create an instance of {@link ErrorInfo }
     * 
     */
    public ErrorInfo createErrorInfo() {
        return new ErrorInfo();
    }

    /**
     * Create an instance of {@link MerchantInfo }
     * 
     */
    public MerchantInfo createMerchantInfo() {
        return new MerchantInfo();
    }

    /**
     * Create an instance of {@link ProcessorInfo }
     * 
     */
    public ProcessorInfo createProcessorInfo() {
        return new ProcessorInfo();
    }

    /**
     * Create an instance of {@link ResponseInfo }
     * 
     */
    public ResponseInfo createResponseInfo() {
        return new ResponseInfo();
    }

    /**
     * Create an instance of {@link MerchantRequest }
     * 
     */
    public MerchantRequest createMerchantRequest() {
        return new MerchantRequest();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "PAN")
    public JAXBElement<String> createPAN(String value) {
        return new JAXBElement<String>(_PAN_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ProcessorDescription")
    public JAXBElement<String> createProcessorDescription(String value) {
        return new JAXBElement<String>(_ProcessorDescription_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "Telephone")
    public JAXBElement<String> createTelephone(String value) {
        return new JAXBElement<String>(_Telephone_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "OrderID")
    public JAXBElement<String> createOrderID(String value) {
        return new JAXBElement<String>(_OrderID_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "DOB")
    public JAXBElement<String> createDOB(String value) {
        return new JAXBElement<String>(_DOB_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ProcessorErrorDescription")
    public JAXBElement<String> createProcessorErrorDescription(String value) {
        return new JAXBElement<String>(_ProcessorErrorDescription_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "AddressCode")
    public JAXBElement<Integer> createAddressCode(Integer value) {
        return new JAXBElement<Integer>(_AddressCode_QNAME, Integer.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "OrderDescription")
    public JAXBElement<String> createOrderDescription(String value) {
        return new JAXBElement<String>(_OrderDescription_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "MerchantAccount")
    public JAXBElement<Integer> createMerchantAccount(Integer value) {
        return new JAXBElement<Integer>(_MerchantAccount_QNAME, Integer.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "StandardCode")
    public JAXBElement<Integer> createStandardCode(Integer value) {
        return new JAXBElement<Integer>(_StandardCode_QNAME, Integer.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "CVC2Code")
    public JAXBElement<String> createCVC2Code(String value) {
        return new JAXBElement<String>(_CVC2Code_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "IssueNumber")
    public JAXBElement<String> createIssueNumber(String value) {
        return new JAXBElement<String>(_IssueNumber_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ProcessorErrorNo")
    public JAXBElement<String> createProcessorErrorNo(String value) {
        return new JAXBElement<String>(_ProcessorErrorNo_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "TransactionID")
    public JAXBElement<Integer> createTransactionID(Integer value) {
        return new JAXBElement<Integer>(_TransactionID_QNAME, Integer.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "StartDate")
    public JAXBElement<String> createStartDate(String value) {
        return new JAXBElement<String>(_StartDate_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "RiskCode")
    public JAXBElement<Integer> createRiskCode(Integer value) {
        return new JAXBElement<Integer>(_RiskCode_QNAME, Integer.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "CardHolderName")
    public JAXBElement<String> createCardHolderName(String value) {
        return new JAXBElement<String>(_CardHolderName_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ClientIP")
    public JAXBElement<String> createClientIP(String value) {
        return new JAXBElement<String>(_ClientIP_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "Zip")
    public JAXBElement<String> createZip(String value) {
        return new JAXBElement<String>(_Zip_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ProcessorCode")
    public JAXBElement<Integer> createProcessorCode(Integer value) {
        return new JAXBElement<Integer>(_ProcessorCode_QNAME, Integer.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ResponseDescription")
    public JAXBElement<String> createResponseDescription(String value) {
        return new JAXBElement<String>(_ResponseDescription_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "Amount")
    public JAXBElement<Integer> createAmount(Integer value) {
        return new JAXBElement<Integer>(_Amount_QNAME, Integer.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "CVC2")
    public JAXBElement<String> createCVC2(String value) {
        return new JAXBElement<String>(_CVC2_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "AuthorisationCode")
    public JAXBElement<Integer> createAuthorisationCode(Integer value) {
        return new JAXBElement<Integer>(_AuthorisationCode_QNAME, Integer.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "AccountNumber")
    public JAXBElement<String> createAccountNumber(String value) {
        return new JAXBElement<String>(_AccountNumber_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ResponseCode")
    public JAXBElement<Integer> createResponseCode(Integer value) {
        return new JAXBElement<Integer>(_ResponseCode_QNAME, Integer.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "Country")
    public JAXBElement<String> createCountry(String value) {
        return new JAXBElement<String>(_Country_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "CurrencyCode")
    public JAXBElement<Integer> createCurrencyCode(Integer value) {
        return new JAXBElement<Integer>(_CurrencyCode_QNAME, Integer.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "MerchantName")
    public JAXBElement<String> createMerchantName(String value) {
        return new JAXBElement<String>(_MerchantName_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "City")
    public JAXBElement<String> createCity(String value) {
        return new JAXBElement<String>(_City_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "RiskDescription")
    public JAXBElement<String> createRiskDescription(String value) {
        return new JAXBElement<String>(_RiskDescription_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ErrorNo")
    public JAXBElement<Integer> createErrorNo(Integer value) {
        return new JAXBElement<Integer>(_ErrorNo_QNAME, Integer.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "Source")
    public JAXBElement<String> createSource(String value) {
        return new JAXBElement<String>(_Source_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "Name")
    public JAXBElement<String> createName(String value) {
        return new JAXBElement<String>(_Name_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ExpiryDate")
    public JAXBElement<String> createExpiryDate(String value) {
        return new JAXBElement<String>(_ExpiryDate_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "Email")
    public JAXBElement<String> createEmail(String value) {
        return new JAXBElement<String>(_Email_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "State")
    public JAXBElement<String> createState(String value) {
        return new JAXBElement<String>(_State_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "MerchantPIN")
    public JAXBElement<String> createMerchantPIN(String value) {
        return new JAXBElement<String>(_MerchantPIN_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "BillingDescriptor")
    public JAXBElement<String> createBillingDescriptor(String value) {
        return new JAXBElement<String>(_BillingDescriptor_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "RefID")
    public JAXBElement<String> createRefID(String value) {
        return new JAXBElement<String>(_RefID_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "Address")
    public JAXBElement<String> createAddress(String value) {
        return new JAXBElement<String>(_Address_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ErrorDescription")
    public JAXBElement<String> createErrorDescription(String value) {
        return new JAXBElement<String>(_ErrorDescription_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "StandardDescription")
    public JAXBElement<String> createStandardDescription(String value) {
        return new JAXBElement<String>(_StandardDescription_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "Surname")
    public JAXBElement<String> createSurname(String value) {
        return new JAXBElement<String>(_Surname_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "MerchantDateTime")
    public JAXBElement<String> createMerchantDateTime(String value) {
        return new JAXBElement<String>(_MerchantDateTime_QNAME, String.class, null, value);
    }

}
