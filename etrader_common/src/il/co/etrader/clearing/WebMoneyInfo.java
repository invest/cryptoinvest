//package il.co.etrader.clearing;
//
//import java.io.Serializable;
//import java.sql.SQLException;
//import java.util.Map;
//
//import org.apache.log4j.Logger;
//
//import com.anyoption.common.beans.Transaction;
//import com.anyoption.common.beans.User;
//import com.anyoption.common.clearing.ClearingInfo;
//
///**
// * WebMoneyInfo clearing info class
// * @author Eran
// *
// */
//
//public class WebMoneyInfo extends ClearingInfo implements Serializable {
//
//	private static final long serialVersionUID = 1L;
//
//	private static final Logger log = Logger.getLogger(WebMoneyInfo.class);
//
//	public static final String SECRET_WORD_TEST = "3edcx";
//	public static final String SECRET_WORD_LIVE = "3edcx";
//
//	//Payment request form
//	private String lmi_result_url;
//	private String lmi_success_url;
//	private String lmi_fail_url;
//	private String lmi_sim_mode;
//	private String redirectURL;
//	private String depositAmount;
//
//	//Result URL fields
//	//Merchant’s purse to which the customer has made the payment. Format is a letter and 12 digits.
//	private String lmi_payee_purse;
//	//Amount paid by the customer; decimal separator is used.
//	private String lmi_payment_amount;
//	//Number of a purchase in accordance with the accounting system of the merchant received by the service from the website of the merchant.
//	private String lmi_payment_no;
//	//The parameter identifies the mode of the payment request processing. It may have two values:	0: The payment is made in the operative mode, funds are transferred from the customer’s purse to the merchant’s purse;	1: The payment is made in the test mode, funds are not transferred – the transfer is imitated.
//	private String lmi_mode;
//	//The bill number specified by the WebMoney Merchant Service during the payment request processing. It is a unique number in WebMoney Transfer
//	private String lmi_sys_invs_no;
//	//Payment number generated by the WebMoney Merchant Service during the processing of the payment request. It is a unique number in WebMoney Transfer.
//	private String lmi_sys_trans_no;
//	//Purse from which the customer has made the payment.
//	private String lmi_payer_purse;
//	//WM identifier of the customer.
//	private String lmi_payer_wm;
//	//The control signature is used to verify the integrity of data and authenticate the sender. The control signature algorithm is described in this section .
//	private String lmi_hash;
//	//Date and time of payment processing in WebMoney Transfer in YYYYMMDD HH:MM:SS format.
//	private String lmi_sys_trans_date;
//	//Secret Key is known only to the merchant and Merchant WebMoney Transfer. The parameter will be empty, if the Result URL is changed or not secured or the ’ Send the secret key to the Result URL’ option is disabled.
//	private String lmi_secret_key;
//
//
//	public WebMoneyInfo() {
//    }
//
//	public WebMoneyInfo(User user, Transaction t, String hpURL,
//							String amount, boolean isLive) throws SQLException {
//    	super(user, t.getAmount(), t.getTypeId(), t.getId(), t.getIp(), t.getAcquirerResponseId());
//    	this.depositAmount = amount;
//    }
//
//	/**
//	 * @return the lmi_fail_url
//	 */
//	public String getLmi_fail_url() {
//		return lmi_fail_url;
//	}
//
//	/**
//	 * @param lmi_fail_url the lmi_fail_url to set
//	 */
//	public void setLmi_fail_url(String lmi_fail_url) {
//		this.lmi_fail_url = lmi_fail_url;
//	}
//
//	/**
//	 * @return the lmi_hash
//	 */
//	public String getLmi_hash() {
//		return lmi_hash;
//	}
//
//	/**
//	 * @param lmi_hash the lmi_hash to set
//	 */
//	public void setLmi_hash(String lmi_hash) {
//		this.lmi_hash = lmi_hash;
//	}
//
//	/**
//	 * @return the lmi_mode
//	 */
//	public String getLmi_mode() {
//		return lmi_mode;
//	}
//
//	/**
//	 * @param lmi_mode the lmi_mode to set
//	 */
//	public void setLmi_mode(String lmi_mode) {
//		this.lmi_mode = lmi_mode;
//	}
//
//	/**
//	 * @return the lmi_payee_purse
//	 */
//	public String getLmi_payee_purse() {
//		return lmi_payee_purse;
//	}
//
//	/**
//	 * @param lmi_payee_purse the lmi_payee_purse to set
//	 */
//	public void setLmi_payee_purse(String lmi_payee_purse) {
//		this.lmi_payee_purse = lmi_payee_purse;
//	}
//
//	/**
//	 * @return the lmi_payer_purse
//	 */
//	public String getLmi_payer_purse() {
//		return lmi_payer_purse;
//	}
//
//	/**
//	 * @param lmi_payer_purse the lmi_payer_purse to set
//	 */
//	public void setLmi_payer_purse(String lmi_payer_purse) {
//		this.lmi_payer_purse = lmi_payer_purse;
//	}
//
//	/**
//	 * @return the lmi_payer_wm
//	 */
//	public String getLmi_payer_wm() {
//		return lmi_payer_wm;
//	}
//
//	/**
//	 * @param lmi_payer_wm the lmi_payer_wm to set
//	 */
//	public void setLmi_payer_wm(String lmi_payer_wm) {
//		this.lmi_payer_wm = lmi_payer_wm;
//	}
//
//	/**
//	 * @return the lmi_payment_amount
//	 */
//	public String getLmi_payment_amount() {
//		return lmi_payment_amount;
//	}
//
//	/**
//	 * @param lmi_payment_amount the lmi_payment_amount to set
//	 */
//	public void setLmi_payment_amount(String lmi_payment_amount) {
//		this.lmi_payment_amount = lmi_payment_amount;
//	}
//
//	/**
//	 * @return the lmi_payment_no
//	 */
//	public String getLmi_payment_no() {
//		return lmi_payment_no;
//	}
//
//	/**
//	 * @param lmi_payment_no the lmi_payment_no to set
//	 */
//	public void setLmi_payment_no(String lmi_payment_no) {
//		this.lmi_payment_no = lmi_payment_no;
//	}
//
//	/**
//	 * @return the lmi_result_url
//	 */
//	public String getLmi_result_url() {
//		return lmi_result_url;
//	}
//
//	/**
//	 * @param lmi_result_url the lmi_result_url to set
//	 */
//	public void setLmi_result_url(String lmi_result_url) {
//		this.lmi_result_url = lmi_result_url;
//	}
//
//	/**
//	 * @return the lmi_secret_key
//	 */
//	public String getLmi_secret_key() {
//		return lmi_secret_key;
//	}
//
//	/**
//	 * @param lmi_secret_key the lmi_secret_key to set
//	 */
//	public void setLmi_secret_key(String lmi_secret_key) {
//		this.lmi_secret_key = lmi_secret_key;
//	}
//
//	/**
//	 * @return the lmi_sim_mode
//	 */
//	public String getLmi_sim_mode() {
//		return lmi_sim_mode;
//	}
//
//	/**
//	 * @param lmi_sim_mode the lmi_sim_mode to set
//	 */
//	public void setLmi_sim_mode(String lmi_sim_mode) {
//		this.lmi_sim_mode = lmi_sim_mode;
//	}
//
//	/**
//	 * @return the lmi_success_url
//	 */
//	public String getLmi_success_url() {
//		return lmi_success_url;
//	}
//
//	/**
//	 * @param lmi_success_url the lmi_success_url to set
//	 */
//	public void setLmi_success_url(String lmi_success_url) {
//		this.lmi_success_url = lmi_success_url;
//	}
//
//	/**
//	 * @return the lmi_sys_invs_no
//	 */
//	public String getLmi_sys_invs_no() {
//		return lmi_sys_invs_no;
//	}
//
//	/**
//	 * @param lmi_sys_invs_no the lmi_sys_invs_no to set
//	 */
//	public void setLmi_sys_invs_no(String lmi_sys_invs_no) {
//		this.lmi_sys_invs_no = lmi_sys_invs_no;
//	}
//
//	/**
//	 * @return the lmi_sys_trans_date
//	 */
//	public String getLmi_sys_trans_date() {
//		return lmi_sys_trans_date;
//	}
//
//	/**
//	 * @param lmi_sys_trans_date the lmi_sys_trans_date to set
//	 */
//	public void setLmi_sys_trans_date(String lmi_sys_trans_date) {
//		this.lmi_sys_trans_date = lmi_sys_trans_date;
//	}
//
//	/**
//	 * @return the lmi_sys_trans_no
//	 */
//	public String getLmi_sys_trans_no() {
//		return lmi_sys_trans_no;
//	}
//
//	/**
//	 * @param lmi_sys_trans_no the lmi_sys_trans_no to set
//	 */
//	public void setLmi_sys_trans_no(String lmi_sys_trans_no) {
//		this.lmi_sys_trans_no = lmi_sys_trans_no;
//	}
//
//	/**
//	 * @return the redirectURL
//	 */
//	public String getRedirectURL() {
//		return redirectURL;
//	}
//
//	/**
//	 * @param redirectURL the redirectURL to set
//	 */
//	public void setRedirectURL(String redirectURL) {
//		this.redirectURL = redirectURL;
//	}
//
//	/**
//	 * @return the depositAmount
//	 */
//	public String getDepositAmount() {
//		return depositAmount;
//	}
//
//	/**
//	 * @param depositAmount the depositAmount to set
//	 */
//	public void setDepositAmount(String depositAmount) {
//		this.depositAmount = depositAmount;
//	}
//
//	/**
//	 * Handle Result URL from WenMoney
//	 * @param requestParameterMap
//	 * @return
//	 */
//	public boolean handleResultUrl(Map requestParameterMap){
//		try {
//		 lmi_payee_purse = (String) requestParameterMap.get("LMI_PAYEE_PURSE");
//		 lmi_payment_amount = (String) requestParameterMap.get("LMI_PAYMENT_AMOUNT");
//		 lmi_payment_no = (String) requestParameterMap.get("LMI_PAYMENT_NO");
//		 lmi_mode = (String) requestParameterMap.get("LMI_MODE");
//		 lmi_sys_invs_no = (String) requestParameterMap.get("LMI_SYS_INVS_NO");
//		 lmi_sys_trans_no = (String) requestParameterMap.get("LMI_SYS_TRANS_NO");
//		 lmi_payer_purse = (String) requestParameterMap.get("LMI_PAYER_PURSE");
//		 lmi_payer_wm = (String) requestParameterMap.get("LMI_PAYER_WM");
//		 lmi_hash = (String) requestParameterMap.get("LMI_HASH");
//		 lmi_sys_trans_date = (String) requestParameterMap.get("LMI_SYS_TRANS_DATE");
//		 lmi_secret_key = (String) requestParameterMap.get("LMI_SECRET_KEY");
//
//		 if (null != lmi_payment_no) {
//			 transactionId = Long.valueOf(lmi_payment_no);
//		 }
//
//		 String TAB = "\n";
//			log.info( TAB + "WebMoney status_url fields:" + TAB
//					+ "lmi_payee_purse=" + lmi_payee_purse + TAB
//					+ "lmi_payment_amount=" + lmi_payment_amount + TAB
//					+ "lmi_payment_no=" + lmi_payment_no + TAB
//					+ "lmi_mode=" + lmi_mode + TAB
//					+ "lmi_sys_invs_no=" + lmi_sys_invs_no + TAB
//					+ "lmi_sys_trans_no=" + lmi_sys_trans_no + TAB
//					+ "lmi_payer_purse=" + lmi_payer_purse + TAB
//					+ "lmi_payer_wm=" + lmi_payer_wm + TAB
//					+ "lmi_hash=" + lmi_hash + TAB
//					+ "lmi_sys_trans_date=" + lmi_sys_trans_date + TAB
//					+ "lmi_secret_key=" + lmi_secret_key);
//		} catch (Exception e){
//			log.error("Error retrieving data from WebMoney Result URL " + e);
//			return false;
//		}
//		return true;
//	}
//
//}