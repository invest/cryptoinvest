//package il.co.etrader.clearing;
//
//import java.util.regex.Matcher;
//import java.util.regex.Pattern;
//import java.util.regex.PatternSyntaxException;
//
//import org.apache.log4j.Logger;
//
//import com.anyoption.common.clearing.ACHInfo;
//import com.anyoption.common.clearing.ClearingException;
//import com.anyoption.common.clearing.ClearingInfo;
//import com.anyoption.common.clearing.ClearingProvider;
//import com.anyoption.common.util.ClearingUtil;
//
//
///**
// * Implement the communication with ACH.
// *
// * @author Asher
// */
//public class ACHClearingProvider extends ClearingProvider {
//
//    private static final Logger log = Logger.getLogger(ACHClearingProvider.class);
//
//    private static final String REQUEST_PARAM_PROD_TYPE = "ACHD";
//    private static final String REQUEST_PARAM_ACCOUNT_TYPE = "PC";
//    private static final String REQUEST_PARAM_SERVICE_NAME = "anyoption";
//
//    private static final int FNC_PURCHASE = 1;
//    private static final int FNC_TRANSACTION_STATUS = 2;
//
//    private static final String URL_SUFFIX_DEPOSIT = "/WSPayment";
//    private static final String URL_SUFFIX_GET_TRX_STATUS = "/getTransactionStatus";
//
//
//    public void authorize(ClearingInfo info) throws ClearingException {
//    	throw new ClearingException("Unsupported operation.");
//    }
//
//    public void capture(ClearingInfo info) throws ClearingException {
//    	throw new ClearingException("Unsupported operation.");
//    }
//
//    public void enroll(ClearingInfo info) throws ClearingException {
//    	throw new ClearingException("Unsupported operation.");
//    }
//
//    public void purchase(ClearingInfo info) throws ClearingException {
//        request((ACHInfo)info, FNC_PURCHASE);
//    }
//
//    public void withdraw(ClearingInfo info) throws ClearingException {
//    	throw new ClearingException("Unsupported operation.");
//    }
//
//    public void bookback(ClearingInfo info) throws ClearingException {
//    	throw new ClearingException("Unsupported operation.");
//    }
//
//    public boolean isSupport3DEnrollement() {
//        return false;
//    }
//
//    public void statusRequest(ClearingInfo info) throws ClearingException {
//    	 request((ACHInfo)info, FNC_TRANSACTION_STATUS);
//    }
//
//    /**
//     * Process a request through this provider.
//     *
//     * @param info thransaction info
//     * @param authorize authorize or capture request to make
//     * @throws ClearingException
//     */
//    private void request(ACHInfo info, int type) throws ClearingException {
//    	boolean throwCurrentException = false;
//        if (log.isTraceEnabled()) {
//            log.trace(info.toString());
//        }
//        try {
//        	String urlWithService = url;
//        	if (type == FNC_PURCHASE) {
//        		urlWithService += URL_SUFFIX_DEPOSIT;
//        	} else {
//        		urlWithService += URL_SUFFIX_GET_TRX_STATUS;
//        	}
//
//            String responseXML = ClearingUtil.executeGETRequest(urlWithService, createQuery(info, type), true);
//            if (log.isTraceEnabled()) {
//                log.trace(responseXML);
//            }
//            parseResponse(responseXML, info, type);
//        } catch (Exception e) {
//        	if (throwCurrentException) {
//        		throw (ClearingException)e;
//        	}
//            throw new ClearingException("Transaction failed.", e);
//        }
//    }
//
//    private String createQuery(ACHInfo info, int type) {
//    	String q = null;
//    	switch (type) {
//		case FNC_PURCHASE:
//	    	q = "merchantID=" + username + '&' +
//	        	"password=" + password + '&' +
//	        	"prodType=" + REQUEST_PARAM_PROD_TYPE + '&' +
//	        	"payeeName=" + info.getPayeeName() + '&' +
//	        	"payeeEmail=" + info.getPayeeEmail() + '&' +
//	        	"payeeAddress=" + info.getPayeeAddress() + '&' +
//	        	"payeeCity=" + info.getPayeeCity() + '&' +
//	        	"payeeStateProvince=" + info.getPayeeStateProvince() + '&' +
//	        	"payeeZipPostalCode=" + info.getPayeeZipCode() + '&' +
//	        	"payeePhoneNumber=" + info.getPayeePhone() + '&' +
//	        	"routingNo=" + info.getRoutingNo() + '&' +
//	        	"accountNo=" + info.getAccountNo() + '&' +
//	        	"accountType=" + REQUEST_PARAM_ACCOUNT_TYPE + '&' +
//	        	"currency=" + info.getCurrency() + '&' +
//	        	"orderID=" + String.valueOf(info.getTransactionId()) + '&' +
//	        	"serviceName=" + REQUEST_PARAM_SERVICE_NAME + '&' +
//	        	"amount=" + info.getAmount()/100;
//				break;
//		case FNC_TRANSACTION_STATUS:
//	    	q = "astechID=" + username + '&' +
//	        	"password=" + password + '&' +
//	        	"product=" + REQUEST_PARAM_PROD_TYPE + '&' +
//	        	"orderId=" + info.getTransactionId();
//	    		break;
//		default:
//			break;
//		}
//    	return q;
//    }
//
//    /**
//     * Parse the response.
//     *
//     * @param xml the XML to parse
//     * @param info the transaction info to fill the result values
//     * @param fnc the function that is called (FNC_CC_AUTHORIZATION/FNC_CC_CAPTURE_AUTHORIZATION)
//     * @throws Exception
//     */
//    public static void parseResponse(String xml, ACHInfo info, int type) throws Exception {
//    	switch (type) {
//		case FNC_PURCHASE:
//			info.setPayeeName(getFieldFromXml(xml, "payeeName"));
//			info.setPayeeEmail(getFieldFromXml(xml, "payeeEmail"));
//	    	info.setPayeeAddress(getFieldFromXml(xml, "payeeAddress"));
//	    	info.setPayeeCity(getFieldFromXml(xml, "payeeCity"));
//	    	info.setPayeeStateProvince(String.valueOf(getFieldFromXml(xml, "payeeStateProvince")));
//	    	info.setPayeeZipCode(getFieldFromXml(xml, "payeeZipPostalCode"));
//	    	info.setPayeePhone(getFieldFromXml(xml, "payeePhoneNumber"));
//	    	info.setRoutingNo(getFieldFromXml(xml, "routingNo"));
//	    	info.setAccountNo(getFieldFromXml(xml, "accountNo"));
//	    	info.setStatus(getFieldFromXml(xml, "status"));
//	    	info.setStatusCode(getFieldFromXml(xml, "statusCode"));
//	    	info.setStatusMessage(getFieldFromXml(xml, "statusMessage"));
//	    	int statusCode = Integer.valueOf(info.getStatusCode());
//	    	if (statusCode == 0) {
//	    		info.setSuccessful(true);
//        		info.setResult("1000");
//        		info.setMessage("Permitted transaction.");
//	    	} else {
//	    		info.setSuccessful(false);
//	    		info.setResult(null != info.getStatusCode() ? info.getStatusCode() : "999");
//	    		info.setMessage("Transaction failed.");
//	    	}
//	    	break;
//		case FNC_TRANSACTION_STATUS:
//			info.setStatus(getFieldFromXml(xml, "orderStatus"));
//			if (info.getStatus().equals("Settled")) {
//				info.setSuccessful(true);
//        		info.setResult("1000");
//        		info.setMessage("Permitted transaction.");
//			} else {
//				info.setSuccessful(false);
//				info.setResult("999");
//				info.setMessage("Transaction failed. status: " + info.getStatus());
//			}
//			break;
//		default:
//			break;
//    	}
//    }
//
//    private static String getFieldFromXml(String xml, String field) throws PatternSyntaxException{
//    	Pattern regex = Pattern.compile("<(" + field + ")>(.*)</\\1>",
//    			Pattern.CANON_EQ);
//    	Matcher matcher = regex.matcher(xml);
//    	if (matcher.find()) {
//    		return matcher.group(2);
//    	}
//    	return "";
//    }
//
//	@Override
//	public void cancel(ClearingInfo info) throws ClearingException {
//		// TODO Auto-generated method stub
//		log.info("NOT IMPLEMENTED !!!");
//	}
//}