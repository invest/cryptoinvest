//package il.co.etrader.clearing;
//
//import java.io.IOException;
//import java.net.URLEncoder;
//
//import javax.xml.parsers.ParserConfigurationException;
//
//import org.apache.log4j.Logger;
//import org.w3c.dom.Document;
//import org.w3c.dom.Element;
//import org.xml.sax.SAXException;
//
//import com.anyoption.common.clearing.CDPayInfo;
//import com.anyoption.common.clearing.ClearingException;
//import com.anyoption.common.clearing.ClearingInfo;
//import com.anyoption.common.clearing.ClearingProvider;
//import com.anyoption.common.util.ClearingUtil;
//
///**
// * @author liors
// *
// */
//public class CDPayClearingProvider extends ClearingProvider {
//	private static Logger log = Logger.getLogger(CDPayClearingProvider.class);
//	
//	@Override
//	public void authorize(ClearingInfo info) throws ClearingException {
//		throw new ClearingException("Unsupported operation.");
//		
//	}
//
//	@Override
//	public void capture(ClearingInfo info) throws ClearingException {
//		throw new ClearingException("Unsupported operation.");
//		
//	}
//
//	@Override
//	public void cancel(ClearingInfo info) throws ClearingException {
//		throw new ClearingException("Unsupported operation.");
//		
//	}
//
//	@Override
//	public void enroll(ClearingInfo info) throws ClearingException {
//		throw new ClearingException("Unsupported operation.");
//		
//	}
//
//	@Override
//	public void purchase(ClearingInfo info) throws ClearingException {
//		throw new ClearingException("Unsupported operation.");
//		
//	}
//
//	@Override
//	public void withdraw(ClearingInfo info) throws ClearingException {
//		throw new ClearingException("Unsupported operation.");
//		
//	}
//
//	@Override
//	public void bookback(ClearingInfo info) throws ClearingException {
//		if (log.isTraceEnabled()) {
//            log.trace(info.toString());
//        }
//		try {
//			CDPayInfo cdpayInfo = (CDPayInfo) info; 
//			log.info("Enter to cdpay BookBack for transaction:" + cdpayInfo);
//			String body = 
//					"version=" + URLEncoder.encode(String.valueOf(cdpayInfo.getVersion()), "UTF-8") +
//					"&merchant_account=" + URLEncoder.encode(cdpayInfo.getMerchantAccount(), "UTF-8") +
//					"&order_id=" + URLEncoder.encode(String.valueOf(cdpayInfo.getOrderId()), "UTF-8") +
//					"&amount=" + URLEncoder.encode(String.valueOf(cdpayInfo.getAmount()),"UTF-8") +
//					"&currency=" + URLEncoder.encode(cdpayInfo.getCurrencySymbol(),"UTF-8") +
//					"&refund_reason=" + URLEncoder.encode("anyreason","UTF-8") +
//					"&control=" + URLEncoder.encode(cdpayInfo.createRefundAuthenticationChecksum(),"UTF-8");
//			log.debug("post url: " + url + "\n" + " post body: " + body);
//			String response = ClearingUtil.executePOSTRequest(url + CDPayInfo.URL_POST_REFUND, body);
//			log.debug("cdpay refund response: " + response);
//			parseResponse(response, cdpayInfo);
//		} catch (Throwable t) {
//			throw new ClearingException("cdpay, Transaction failed.", t);
//		}
//	}
//
//	@Override
//	public boolean isSupport3DEnrollement() {
//		return false;
//	}
//	
//	public void setProviderDetails(CDPayInfo info) {
//    	info.setRedirectURL(url + CDPayInfo.URL_POST_DEPOSIT);
//    	info.setMerchantAccount(username);
//    	info.setMerchant_id(password); // password in our case equal to merchantId
//    	info.setPrivateKey(privateKey);
//    }
//	
//    /**
//     * Parse CDPay refund result.
//     *
//     * @param result the result XML
//     * @return <code>Hashtable</code> with result values.
//     * @throws ParserConfigurationException
//     * @throws SAXException
//     * @throws IOException
//     * @throws ClearingException 
//     */
//	//TODO: cdpay. unit testing.
//    private static void parseResponse(String response, CDPayInfo cdpayInfo) throws ParserConfigurationException, SAXException, IOException, ClearingException {
//    	Document responseDocument = ClearingUtil.parseXMLToDocument(response);
//          
//        try {
//        	Element rootOrder = (Element) ClearingUtil.getNode(responseDocument.getDocumentElement(), "");
//        	Element rootTransaction = (Element) ClearingUtil.getNode(responseDocument.getDocumentElement(), "transaction/");     
//        	String statusMsg = ClearingUtil.getElementValue(rootTransaction, "message/*");
//        	cdpayInfo.setStatusMessage(statusMsg);
//        	String cdpayTransactionId = ClearingUtil.getElementValue(rootOrder, "orderid/*");
//        	cdpayInfo.setCdpayTransactionId(Integer.valueOf(cdpayTransactionId));
//        	cdpayInfo.setOrderId(Integer.valueOf(cdpayTransactionId));
//        	cdpayInfo.setMerchantOrder(ClearingUtil.getElementValue(rootOrder, "merchantOrder/*"));
//        	//cdpayInfo.setTransactionId(Integer.valueOf(ClearingUtil.getElementValue(rootOrder, "merchantOrder/*")));
//        	cdpayInfo.setAmount(Integer.valueOf(ClearingUtil.getElementValue(rootOrder, "amount/*")));
//        	cdpayInfo.setCurrencySymbol(ClearingUtil.getElementValue(rootOrder, "currency/*"));
//        	//transaction
//        	cdpayInfo.setBankId(Integer.valueOf(ClearingUtil.getElementValue(rootTransaction, "bankid/*")));
//        	cdpayInfo.setBankTransactionId((ClearingUtil.getElementValue(rootTransaction, "banktranid/*")));
//        	String status = ClearingUtil.getElementValue(rootTransaction, "status/*");
//        	cdpayInfo.setStatus(status);
//        	
//        	//success.
//        	cdpayInfo.setResult(status);
//        	cdpayInfo.setAuthNumber(cdpayTransactionId);
//        	cdpayInfo.setMessage(statusMsg);
//        	cdpayInfo.setSuccessful(status.equals(STATUS_SUCCESSFUL));
//        	
//        	//refund - if succeed.      	
//        	Element rootRefund = (Element) ClearingUtil.getNode(responseDocument.getDocumentElement(), "refund");
//        	//TODO problem with getNode method: return null when there is more than one the same tag element request.
//        	if(rootRefund != null) {
//	        	String refundId = ClearingUtil.getElementValue(rootRefund, "refundid/*");
//	        	if (refundId != null) {
//	        		cdpayInfo.setRefundId(Integer.valueOf(refundId));
//	        		cdpayInfo.setRefundAmount(Double.valueOf(ClearingUtil.getElementValue(rootOrder, "refundamount/*")));
//	        		cdpayInfo.setRefundReqDate(ClearingUtil.getElementValue(rootOrder, "refundreqdate/*"));    		
//	        		cdpayInfo.setRefundCompleteDate(ClearingUtil.getElementValue(rootOrder, "refundcompletedate/*"));
//	        		cdpayInfo.setRefundStatus(ClearingUtil.getElementValue(rootOrder, "refundstatus/*"));
//	        	} else {
//	        		log.info("cdpay parse response, refund id was not found, seek the reason in `message` tag.");
//	        	}  
//        	} else {
//	        	log.info("Problem parsing rootRefund !!! ");
//	        }
//        } catch (Throwable t) {
//        	log.error("cdpay parseResponse," + cdpayInfo.getStatusMessage());
//			throw new ClearingException("cdpay, parseResponse failed." + cdpayInfo.getStatusMessage(), t);
//		}
//
//        if (log.isDebugEnabled()) {
//            log.debug(cdpayInfo.toString());
//        }
//    }
//    
//    /*
//      public static void main(String args[]) throws Exception {
//    	CDPayInfo cdpinfo = new CDPayInfo();
//    	//String s = "<?xml version=\"1.0\" encoding=\"iso-8859-1\"?><order><transaction_id>379502</transaction_id><merchant_account>1000001</merchant_account><merchant_order>3397764</merchant_order><orderid>379502</orderid><merchantAccount>1000001</merchantAccount><merchantOrder>3397764</merchantOrder><customer><first_name>any</first_name><last_name>option</last_name><firstName>any</firstName><lastName>option</lastName><ssn></ssn><birthday></birthday><address1>khtur</address1><address2/><city>c</city><zip_code>z</zip_code><zipcode>z</zipcode><state/><country></country><phone>0</phone><cellphone></cellphone><email>support@anyoption.com</email></customer><amount>110000</amount><currency>CNY</currency><transaction><bank_id>541</bank_id><bank_transaction_id>20130508175145</bank_transaction_id><bankid>541</bankid><banktranid>20130508175145</banktranid><bank_confirm_tran_id></bank_confirm_tran_id><error><status>A0</status><message>Paid / Refunded / Successful requested.</message></error><refunds><refund><refundid>2562</refundid><refundamount>10000</refundamount><refundreqdate>08-MAY-13</refundreqdate><refundcompletedate>08-MAY-13</refundcompletedate><refundstatus>Refund complete.</refundstatus></refund><refund><refundid>2563</refundid><refundamount>110000</refundamount><refundreqdate>08-MAY-13</refundreqdate><refundcompletedate>08-MAY-13</refundcompletedate><refundstatus>Refund complete.</refundstatus></refund></refunds></transaction><card><merchant_card_number></merchant_card_number><expiry_month></expiry_month><expiry_year></expiry_year></card></order>";
//    	String s = "<order><transaction_id>379605</transaction_id><merchant_account>1000001</merchant_account><merchant_order>3399248</merchant_order><orderid>379605</orderid><merchantAccount>1000001</merchantAccount><merchantOrder>3399248</merchantOrder><customer><first_name>any</first_name><last_name>option</last_name><firstName>any</firstName><lastName>option</lastName><ssn></ssn><birthday></birthday><address1>khtur</address1><address2/><city>c</city><zip_code>z</zip_code><zipcode>z</zipcode><state/><country></country><phone>0</phone><cellphone></cellphone><email>support@anyoption.com</email></customer><amount>100000</amount><currency>CNY</currency><transaction><bank_id>541</bank_id><bank_transaction_id>20130508224306</bank_transaction_id><bankid>541</bankid><banktranid>20130508224306</banktranid><bank_confirm_tran_id></bank_confirm_tran_id><error><status>E51</status><message>This transaction does not exist.</message></error><refunds/></transaction><card><merchant_card_number></merchant_card_number><expiry_month></expiry_month><expiry_year></expiry_year></card></order>";
//    	CDPayClearingProvider.parseResponse(s, cdpinfo);
//    }
//    */
//    
//    /*public static Document stringToDom(String xmlSource) 
//            throws SAXException, ParserConfigurationException, IOException {
//        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
//        DocumentBuilder builder = factory.newDocumentBuilder();
//        return builder.parse(new InputSource(new StringReader(xmlSource)));
//    }*/
//}
