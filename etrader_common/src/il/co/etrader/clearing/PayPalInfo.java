///**
// *
// */
//package il.co.etrader.clearing;
//
//
//import il.co.etrader.bl_vos.UserBase;
//import il.co.etrader.util.CommonUtil;
//
//import com.anyoption.common.beans.Transaction;
//import com.anyoption.common.clearing.ClearingInfo;
//
///**
// * PalPal clearing info class
// * @author Kobi
// *
// */
//public class PayPalInfo extends ClearingInfo {
//
//	String token;  // getting in setExpressCheckout respone
//	String paymentType;
//	String loginRedirectUrl;
//	String correlationId;
//
//	String shipToName;
//	String shipToCountryCode;
//	String shipToCountryName;
//	String shipToState;
//	String shipToStreet;
//	String shipToZip;
//	String shipToCity;
//	String payerBusiness;
//
//	String payerStatus;
//	String payerId;
//	String payerFirstName;
//	String payerLastName;
//	String payerEmail;
//	String payerCountryCode;
//	String payerAdrrStatus;
//
//	MassPayItem[] emails;
//
//    public PayPalInfo() {
//    }
//
//    public PayPalInfo(UserBase user, Transaction t, String paymentType){
//    	super(user, t.getAmount(), t.getTypeId(), t.getId(), t.getIp(), t.getAcquirerResponseId());
//    	this.paymentType = paymentType;
//    }
//
//	/**
//	 * @return the paymentType
//	 */
//	public String getPaymentType() {
//		return paymentType;
//	}
//
//	/**
//	 * @param paymentType the paymentType to set
//	 */
//	public void setPaymentType(String paymentType) {
//		this.paymentType = paymentType;
//	}
//
//	/**
//	 * @return the token
//	 */
//	public String getToken() {
//		return token;
//	}
//
//	/**
//	 * @param token the token to set
//	 */
//	public void setToken(String token) {
//		this.token = token;
//	}
//
//	/**
//	 * @return the loginRedirectUrl
//	 */
//	public String getLoginRedirectUrl() {
//		return loginRedirectUrl;
//	}
//
//	/**
//	 * @param loginRedirectUrl the loginRedirectUrl to set
//	 */
//	public void setLoginRedirectUrl(String loginRedirectUrl) {
//		this.loginRedirectUrl = loginRedirectUrl;
//	}
//
//	/**
//	 * @return the business
//	 */
//	public String getPayerBusiness() {
//		return payerBusiness;
//	}
//
//	/**
//	 * @param business the business to set
//	 */
//	public void setPayerBusiness(String payerBusiness) {
//		this.payerBusiness = payerBusiness;
//	}
//
//	/**
//	 * @return the correlationId
//	 */
//	public String getCorrelationId() {
//		return correlationId;
//	}
//
//	/**
//	 * @param correlationId the correlationId to set
//	 */
//	public void setCorrelationId(String correlationId) {
//		this.correlationId = correlationId;
//	}
//
//	/**
//	 * @return the payerAdrrStatus
//	 */
//	public String getPayerAdrrStatus() {
//		return payerAdrrStatus;
//	}
//
//	/**
//	 * @param payerAdrrStatus the payerAdrrStatus to set
//	 */
//	public void setPayerAdrrStatus(String payerAdrrStatus) {
//		this.payerAdrrStatus = payerAdrrStatus;
//	}
//
//	/**
//	 * @return the payerCountryCode
//	 */
//	public String getPayerCountryCode() {
//		return payerCountryCode;
//	}
//
//	/**
//	 * @param payerCountryCode the payerCountryCode to set
//	 */
//	public void setPayerCountryCode(String payerCountryCode) {
//		this.payerCountryCode = payerCountryCode;
//	}
//
//	/**
//	 * @return the shipToCity
//	 */
//	public String getShipToCity() {
//		return shipToCity;
//	}
//
//	/**
//	 * @param shipToCity the shipToCity to set
//	 */
//	public void setShipToCity(String shipToCity) {
//		this.shipToCity = shipToCity;
//	}
//
//	/**
//	 * @return the shipToCountryName
//	 */
//	public String getShipToCountryName() {
//		return shipToCountryName;
//	}
//
//	/**
//	 * @param shipToCountryName the shipToCountryName to set
//	 */
//	public void setShipToCountryName(String shipToCountryName) {
//		this.shipToCountryName = shipToCountryName;
//	}
//
//	/**
//	 * @return the payerEmail
//	 */
//	public String getPayerEmail() {
//		return payerEmail;
//	}
//
//	/**
//	 * @param payerEmail the payerEmail to set
//	 */
//	public void setPayerEmail(String payerEmail) {
//		this.payerEmail = payerEmail;
//	}
//
//	/**
//	 * @return the payerFirstName
//	 */
//	public String getPayerFirstName() {
//		return payerFirstName;
//	}
//
//	/**
//	 * @param payerFirstName the payerFirstName to set
//	 */
//	public void setPayerFirstName(String payerFirstName) {
//		this.payerFirstName = payerFirstName;
//	}
//
//	/**
//	 * @return the payerId
//	 */
//	public String getPayerId() {
//		return payerId;
//	}
//
//	/**
//	 * @param payerId the payerId to set
//	 */
//	public void setPayerId(String payerId) {
//		this.payerId = payerId;
//	}
//
//	/**
//	 * @return the payerLastName
//	 */
//	public String getPayerLastName() {
//		return payerLastName;
//	}
//
//	/**
//	 * @param payerLastName the payerLastName to set
//	 */
//	public void setPayerLastName(String payerLastName) {
//		this.payerLastName = payerLastName;
//	}
//
//	/**
//	 * @return the payerStatus
//	 */
//	public String getPayerStatus() {
//		return payerStatus;
//	}
//
//	/**
//	 * @param payerStatus the payerStatus to set
//	 */
//	public void setPayerStatus(String payerStatus) {
//		this.payerStatus = payerStatus;
//	}
//
//	/**
//	 * @return the shipToCountryCode
//	 */
//	public String getShipToCountryCode() {
//		return shipToCountryCode;
//	}
//
//	/**
//	 * @param shipToCountryCode the shipToCountryCode to set
//	 */
//	public void setShipToCountryCode(String shipToCountryCode) {
//		this.shipToCountryCode = shipToCountryCode;
//	}
//
//	/**
//	 * @return the shipToName
//	 */
//	public String getShipToName() {
//		return shipToName;
//	}
//
//	/**
//	 * @param shipToName the shipToName to set
//	 */
//	public void setShipToName(String shipToName) {
//		this.shipToName = shipToName;
//	}
//
//	/**
//	 * @return the shipToState
//	 */
//	public String getShipToState() {
//		return shipToState;
//	}
//
//	/**
//	 * @param shipToState the shipToState to set
//	 */
//	public void setShipToState(String shipToState) {
//		this.shipToState = shipToState;
//	}
//
//	/**
//	 * @return the shipToStreet
//	 */
//	public String getShipToStreet() {
//		return shipToStreet;
//	}
//
//	/**
//	 * @param shipToStreet the shipToStreet to set
//	 */
//	public void setShipToStreet(String shipToStreet) {
//		this.shipToStreet = shipToStreet;
//	}
//
//	/**
//	 * @return the shipToZip
//	 */
//	public String getShipToZip() {
//		return shipToZip;
//	}
//
//	/**
//	 * @param shipToZip the shipToZip to set
//	 */
//	public void setShipToZip(String shipToZip) {
//		this.shipToZip = shipToZip;
//	}
//
//	/**
//	 * Getting amountTxt without currency symbol
//	 */
//	public String getAmountText() {
//		return CommonUtil.displayAmount(amount, false, 0);
//	}
//
//	/**
//	 * @return the emails
//	 */
//	public MassPayItem[] getEmails() {
//		return emails;
//	}
//
//	/**
//	 * @param emails the emails to set
//	 */
//	public void setEmails(MassPayItem[] emails) {
//		this.emails = emails;
//	}
//
//	/**
//	 * Implement toString function
//	 */
//	   public String toString() {
//	        String ls = System.getProperty("line.separator");
//	        String superToStr = super.toString();  // base toString
//	        return ls + superToStr + ls + "PayPalInfo:" + ls +
//	        		"paymentType: " + paymentType + ls +
//	                "token: " + token;
//	    }
//
//
//	/**
//	 * MassPay item. saving information for each massPay (withdrawal action)
//	 * @author Kobi
//	 *
//	 */
//	public class MassPayItem {
//		private String email;
//		private long amount;
//		private long transactionId;
//
//		/**
//		 * @return the amount
//		 */
//		public long getAmount() {
//			return amount;
//		}
//
//		/**
//		 * @param amount the amount to set
//		 */
//		public void setAmount(long amount) {
//			this.amount = amount;
//		}
//
//		/**
//		 * @return the email
//		 */
//		public String getEmail() {
//			return email;
//		}
//
//		/**
//		 * @param email the email to set
//		 */
//		public void setEmail(String email) {
//			this.email = email;
//		}
//
//		/**
//		 * @return the transactionId
//		 */
//		public long getTransactionId() {
//			return transactionId;
//		}
//
//		/**
//		 * @param transactionId the transactionId to set
//		 */
//		public void setTransactionId(long transactionId) {
//			this.transactionId = transactionId;
//		}
//
//		public String getAmountText() {
//			return CommonUtil.displayAmount(this.amount, false, 0);
//		}
//
//	}
//
//}
