//package il.co.etrader.clearing;
//
//import il.co.etrader.bl_managers.ApplicationDataBase;
//import il.co.etrader.bl_vos.UserBase;
//import il.co.etrader.util.ConstantsBase;
//
//import java.io.Serializable;
//import java.math.BigDecimal;
//import java.util.Map;
//
//import javax.faces.context.FacesContext;
//
//import org.apache.log4j.Logger;
//
//import com.anyoption.common.beans.Transaction;
//import com.anyoption.common.clearing.ClearingInfo;
//
///**
// * DeltaPay clearing info class
// * @author Aviad
// *
// */
//
//public class DeltaPayInfo extends ClearingInfo implements Serializable {
//
//	private static final long serialVersionUID = 1L;
//
//	private static final Logger log = Logger.getLogger(DeltaPayInfo.class);
//	private final String RETURN_URL = "jsp/pages/deltaPayStatusURL.jsf";
//
//	private String affiliate;
//	private String payMethod;
//	private String proccessingMode;
//	private String redirect;
//	private String location;
//	private String terminalName;
//	private String agentName;
//	private String status;
//	private String cardTypeName;
//	private String transaction_no;
//	private String approval_no;
//	private String description;
//	private String order_id;
//	private String currencyName;
//	private String errorMessageFromRequset;// need to send from request to phaseListener
//	private boolean firstDeposit;// need to send from request to phaseListener
//
//	public DeltaPayInfo(){
//	}
//
//	public DeltaPayInfo(UserBase user, Transaction t, String homePageUrl) {
//		super(user, t.getAmount(), t.getTypeId(), t.getId(), t.getIp(), t.getAcquirerResponseId());
//		FacesContext context = FacesContext.getCurrentInstance();
//    	ApplicationDataBase ap = (ApplicationDataBase)context.getApplication().createValueBinding(ConstantsBase.BIND_APPLICATION_DATA).getValue(context);
//    	this.affiliate = "VANYOAO";
//		this.agentName = "ANYOISGagent1";
//    	if (ap.getIsLive()) {
//    		log.info("Got deltaPayInfo for LIVE");
//    		this.terminalName = "ANYOALOGCNY-1";
//    	} else {
//    		log.info("Got deltaPayInfo for NOT LIVE");
//    		this.terminalName = "ANYOALOGCNYTEST-1";
//    	}
//    	this.setCurrencyName("CNY");
//    	redirect = homePageUrl + RETURN_URL;
//    	//According to provider.. need to fill in default values, the customer will fill the real one when he will redirected
//    	this.setCardTypeName("VISA");
//    	this.setCcn("4111111111111111");
//    	this.setCvv("123");
//    	this.setExpireMonth("12");
//    	this.setExpireYear("99");
//	}
//
//
//	public String getApproval_no() {
//		return approval_no;
//	}
//
//	public void setApproval_no(String approval_no) {
//		this.approval_no = approval_no;
//	}
//
//	public String getCurrencyName() {
//		return currencyName;
//	}
//
//	public void setCurrencyName(String currencyName) {
//		this.currencyName = currencyName;
//	}
//
//	public String getDescription() {
//		return description;
//	}
//
//	public void setDescription(String description) {
//		this.description = description;
//	}
//
//	public String getOrder_id() {
//		return order_id;
//	}
//
//	public void setOrder_id(String order_id) {
//		this.order_id = order_id;
//	}
//
//	public String getTransaction_no() {
//		return transaction_no;
//	}
//
//	public void setTransaction_no(String transaction_no) {
//		this.transaction_no = transaction_no;
//	}
//
//	public String getCardTypeName() {
//		return cardTypeName;
//	}
//
//	public void setCardTypeName(String cardTypeName) {
//		this.cardTypeName = cardTypeName;
//	}
//
//	public boolean isFirstDeposit() {
//		return firstDeposit;
//	}
//
//	public void setFirstDeposit(boolean firstDeposit) {
//		this.firstDeposit = firstDeposit;
//	}
//
//	public String getErrorMessageFromRequset() {
//		return errorMessageFromRequset;
//	}
//
//	public void setErrorMessageFromRequset(String errorMessageFromRequset) {
//		this.errorMessageFromRequset = errorMessageFromRequset;
//	}
//
//	public String getAffiliate() {
//		return affiliate;
//	}
//
//	public void setAffiliate(String affiliate) {
//		this.affiliate = affiliate;
//	}
//
//	public String getAgentName() {
//		return agentName;
//	}
//
//	public void setAgentName(String agentName) {
//		this.agentName = agentName;
//	}
//
//	public String getLocation() {
//		return location;
//	}
//
//	public void setLocation(String location) {
//		this.location = location;
//	}
//
//	public String getPayMethod() {
//		return payMethod;
//	}
//
//	public void setPayMethod(String payMethod) {
//		this.payMethod = payMethod;
//	}
//
//	public String getProccessingMode() {
//		return proccessingMode;
//	}
//
//	public void setProccessingMode(String proccessingMode) {
//		this.proccessingMode = proccessingMode;
//	}
//
//	public String getRedirect() {
//		return redirect;
//	}
//
//	public void setRedirect(String redirect) {
//		this.redirect = redirect;
//	}
//
//	public String getTerminalName() {
//		return terminalName;
//	}
//
//	public void setTerminalName(String terminalName) {
//		this.terminalName = terminalName;
//	}
//
//	public String getStatus() {
//		return status;
//	}
//
//	public void setStatus(String status) {
//		this.status = status;
//	}
//
//	/**
//	 * @return the withdarwAmount
//	 */
//	public String getFullAmountStr() {
//		return String.valueOf(new BigDecimal(amount).divide(new BigDecimal(100)));
//	}
//
//	/**
//	 * Handle status URL from Moneybookers
//	 * @param requestParameterMap
//	 * @return
//	 */
//	public boolean handleStatusUrl(Map requestParameterMap){
//		try {
//		 transaction_no = (String) requestParameterMap.get("transaction_no");
//		 status = (String) requestParameterMap.get("status");
//		 description = (String) requestParameterMap.get("description");
//		 order_id =(String) requestParameterMap.get("order_id");
//		 approval_no = (String) requestParameterMap.get("approval_no");
//
//		 message = "Status=" + status + "|FailedReasonCode=" + description;
//		 String TAB = "\n";
//			log.info( TAB + "DeltaPayChina status_url fields:" + TAB
//					+ "transaction_no=" + transaction_no + TAB
//					+ "status=" + status + TAB
//					+ "approval_no=" + approval_no + TAB
//					+ "description=" + description);
//		} catch (Exception e){
//			log.error("Error retrieving data from DeltaPayChina status URL " + e);
//			return false;
//		}
//		return true;
//	}
//}