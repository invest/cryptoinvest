package il.co.etrader.clearing.jobs;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Hashtable;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import com.anyoption.common.beans.base.Skin;
import com.anyoption.common.util.AESUtil;

import il.co.etrader.bl_managers.TransactionsManagerBase;
import il.co.etrader.util.CommonUtil;
import il.co.etrader.util.ConstantsBase;
import il.co.etrader.util.JobUtil;


/**
 *
 *
 * @author
 */
public class BankWireASCIJob extends JobUtil {
    private static Logger log = Logger.getLogger(BankWireASCIJob.class);

    private static StringBuffer report = new StringBuffer();

    private static Writer output = null;
    private static File file = null;
    private static long amountSum = 0;
    private static long usersCount = 0;
    private static final String CRLF = "\r\n";
    private static String mailSubject;


    private static String getDetailsForBankWireForm() throws SQLException {

        Connection con = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        String fromDate = getPropertyByFile("from.date", ConstantsBase.EMPTY_STRING);
        String toDate = getPropertyByFile("to.date", ConstantsBase.EMPTY_STRING);

        report.append("<br/><b>MASAV Information:</b><table border=\"1\">");
        report.append("<tr><td><b>Users Id</b></td><td><b>Transaction id</b></td><td><b>Amount</b></td></tr>");

        String wiredIdsList = ""; //to update masav time

        try {
            con = getConnection();

            String transactionId = getPropertyByFile("transaction.id", ConstantsBase.EMPTY_STRING);
            String sql =
                "SELECT " +
                    "u.id, " +
                    "u.id_num, " +
                    "u.first_name, " +
                    "t.amount, " +
                    "w.branch, " +
                    "w.account_num, " +
                    "b.bank_code bank_code, " +
                    "t.id tran_id, " +
                    "w.id wire_id " +
                "FROM " +
                    "transactions t, " +
                    "users u, " +
                    "wires w, " +
                    "banks b " +
                "WHERE " +
                    "u.skin_id = " + Skin.SKIN_ETRADER + " AND " +
                    "u.class_id <> 0 AND " +
                    "w.masav_time IS null AND " +
                    "t.user_id = u.id AND " +
                    "t.type_id = " + TransactionsManagerBase.TRANS_TYPE_BANK_WIRE_WITHDRAW + " AND " +
//                    "t.type_id = " + TransactionsManagerBase.TRANS_TYPE_EFT_WITHDRAW + " AND " +
            		"w.id = t.wire_id AND " +
                    "t.status_id = " + TransactionsManagerBase.TRANS_STATUS_SUCCEED + " AND " +
                    "b.id = w.bank_id ";
            if (!CommonUtil.isParameterEmptyOrNull(fromDate)) {
            	sql += " AND t.time_settled >= to_date(" + "'" + fromDate + "'" + ",'DD/MM/YYYY') ";
            }
    		if (!CommonUtil.isParameterEmptyOrNull(toDate)) {
    			sql += " AND t.time_settled <= to_date(" + "'" + toDate + "'" + ",'DD/MM/YYYY') ";
    		}                                                    
                    
            if (!CommonUtil.isParameterEmptyOrNull(transactionId)) {
                sql += " AND t.id in ( " + transactionId + " )";
            }

            pstmt = con.prepareStatement(sql);
/*            if (!CommonUtil.IsParameterEmptyOrNull(transactionId)) {
                pstmt.setString(1, transactionId);
            }*/
            rs = pstmt.executeQuery();

            usersCount=0;
            amountSum=0;
            while ( rs.next() ) {

                usersCount++;
                amountSum += rs.getLong("amount");
            	// create row
                output.write('1');
                output.write("91763201"); //"masab" number
                output.write("00"); //currency
                output.write(characterWrapper("", "0", 6));
                output.write(characterWrapper(rs.getString("bank_code"), "0", 2)); //code bank
                output.write(characterWrapper(rs.getString("branch"), "0", 3)); //Branch number
                output.write("0000"); //account type
                output.write(characterWrapper(rs.getString("account_num"), "0", 9)); //account number
                output.write("0");
                output.write(characterWrapper(AESUtil.decrypt(rs.getString("id_num")), "0", 9)); //user id number
                output.write(characterWrapper(rs.getString("id"), " ", 16)); //user id
                output.write(characterWrapper(String.valueOf(rs.getLong("amount")), " ", 13)); //amount to pay
                output.write(characterWrapper(AESUtil.decrypt(rs.getString("id_num")), "0", 20)); //user id number
                output.write(characterWrapper("", "0", 8));//Payment period
                output.write("000");
                output.write("006");
                output.write(characterWrapper("", "0", 18));
                output.write(characterWrapper("", " ", 2));
                output.write(CRLF);

                report.append("<tr>");
                report.append("<td>").append(rs.getLong("id")).append("</td>");
                report.append("<td>").append(rs.getLong("tran_id")).append("</td>");
                report.append("<td>").append(rs.getLong("amount")/(double)(100)).append("</td>");
                report.append("</tr>");

                wiredIdsList += rs.getLong("wire_id") + ",";
            }

            report.append("</table>");


           }
        	catch (Exception e) {
        		log.log(Level.ERROR, "Error while Retrieving users from Db.", e);
                wiredIdsList = null;
            } finally {
            	try {
            		rs.close();
            	} catch (Exception e) {
            		log.log(Level.ERROR, "Can't close", e);
            	}
            	try {
            		pstmt.close();
            	} catch (Exception e) {
            		log.log(Level.ERROR, "Can't close", e);
            	}
            	try {
            		con.close();
            	} catch (Exception e) {
                log.log(Level.ERROR, "Can't close connection", e);
            	}
            }

            return wiredIdsList;

    }


    //TODO: before run, chage file to UTF-8

    /**
     * 856 tax form.
     *
     * @param args
     * 			the parameter is some file that include some properties
     * @throws SQLException
     * @throws IOException
     */
    public static void main(String[] args) throws SQLException, IOException {

    	if (null == args || args.length < 1) {
            log.log(Level.FATAL, "Configuration file not specified, insert properties file name.");
            return;
        }

        propFile = args[0];

        String wiredIdList = null;
        SimpleDateFormat sd = new SimpleDateFormat("dd.MM.yyyy");
        String tik_nikoim = "Masav";

        String fileName = tik_nikoim + "-" + sd.format(new Date());
        if (log.isEnabledFor(Level.INFO)) {
            log.log(Level.INFO, "Starting to build form 856. fileName: " + fileName);
        }
        sd = new SimpleDateFormat("yyMMdd");
        mailSubject = fileName;

	    file = new File(fileName);
	    output = new BufferedWriter(new FileWriter(file));

	    // add header row
	    output.write('K');
        output.write("91763201"); //"masab" number
        output.write("00"); //currency
        output.write(sd.format(new Date())); //date to pay
        output.write("0");
        output.write("001");
        output.write("0");
        output.write(sd.format(new Date())); //create date of the movie
        output.write("73020"); //Institution number
        output.write("000000");
        output.write(characterWrapper("מ''עב רדיירטיא", " ", 30));
        output.write(characterWrapper("", " ", 56));
        output.write("KOT");
	    output.write(CRLF);

	    // add content rows
        wiredIdList = getDetailsForBankWireForm();

        // add footer row
    	output.write("5");
        output.write("91763201"); //"masab" number
        output.write("00"); //currency
        output.write(sd.format(new Date())); //date to pay
        output.write("0");
        output.write("001");
        output.write(characterWrapper(String.valueOf(amountSum), "0", 15)); //rows sum
        output.write(characterWrapper("", "0", 15));
        output.write(characterWrapper(String.valueOf(usersCount), "0", 7)); //rows count
        output.write(characterWrapper("", "0", 7));
        output.write(characterWrapper("", " ", 63));
        output.write(CRLF);

        output.write(characterWrapper("", "9", 128));
        output.write(CRLF);

    	output.flush();
        output.close();

	    if (log.isEnabledFor(Level.INFO)) {
	        log.log(Level.INFO, "Bank wire Form creation completed.");
	    }

        if (log.isEnabledFor(Level.INFO)) {
            log.log(Level.INFO, "Bank wire update masav time start.");
        }
        int res = 1; //should send email the update run good or we dont have any list

        if (!CommonUtil.isParameterEmptyOrNull(wiredIdList)) {
            Connection con = null;
            PreparedStatement pstmt = null;

            try {
                con = getConnection();
                wiredIdList = wiredIdList.substring(0,wiredIdList.length()-1);
                String sql =
                        "UPDATE " +
                            "wires " +
                        "SET " +
                            "masav_time = sysdate " +
                        "WHERE " +
                            "id IN (" + wiredIdList + ")";

                pstmt = con.prepareStatement(sql);
                res = pstmt.executeUpdate();
            } catch (Exception e) {
                log.log(Level.ERROR, "Error while update masav time.", e);
                res = 0; //dont send email
            } finally {
                try {
                    pstmt.close();
                } catch (Exception e) {
                    log.log(Level.ERROR, "Can't close", e);
                }
                try {
                    con.close();
                } catch (Exception e) {
                log.log(Level.ERROR, "Can't close connection", e);
                }
            }

        }
        if (log.isEnabledFor(Level.INFO)) {
            log.log(Level.INFO, "Bank wire update masav time end.");
        }


//    build the report
        String reportBody;
        if (res >= 1) { //send email
            reportBody = "<html><body>" +
                                report.toString() +
                         "</body></html>";
        } else {
            reportBody = "<html><body>" +
                            " ERROR UPDATING MASAV TIME " +
                         "</body></html>";
        }

        if (log.isEnabledFor(Level.INFO)) {
            log.log(Level.INFO, "Sending report email: " + reportBody);
        }
//    send email
        Hashtable<String,String> server = new Hashtable<String,String>();
        server.put("url", getPropertyByFile("email.url"));
        server.put("auth", getPropertyByFile("email.auth"));
        server.put("user", getPropertyByFile("email.user"));
        server.put("pass", getPropertyByFile("email.pass"));
        server.put("contenttype", getPropertyByFile("email.contenttype", "text/html; charset=UTF-8"));

        Hashtable<String,String> email = new Hashtable<String,String>();
        email.put("subject", mailSubject);
        email.put("to", getPropertyByFile("bonus.email.to"));
        email.put("from", getPropertyByFile("bonus.email.from"));
        email.put("body", reportBody);

        File attachment = new File(fileName);
        File []attachments={attachment};
        CommonUtil.sendEmail(server, email, attachments);

	 }

    /**
     * Text wrapper left by character and column size
     * @param text
     * @param character
     * @param fieldSize
     * @return
     */
    public static String characterWrapper(String text, String character, long fieldSize) {
    	String out = "";
    	long len = fieldSize - text.length();
    	for (int i = 0 ; i < len ; i ++) {
    		out += character;
    	}
    	out += text;
		return out;
    }

    /**
     * Text wrapper right by character and column size
     * @param text
     * @param character
     * @param fieldSize
     * @return
     */
    public static String characterWrapperRight(String text, String character, long fieldSize) {
    	String out = text;
    	long len = fieldSize - text.length();
    	for (int i = 0 ; i < len ; i ++) {
    		out += character;
    	}
		return out;
    }

}
