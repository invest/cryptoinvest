package il.co.etrader.clearing.jobs;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Hashtable;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import com.anyoption.common.clearing.ClearingException;
import com.anyoption.common.clearing.ClearingInfo;
import com.anyoption.common.util.AESUtil;
import com.anyoption.common.util.ClearingUtil;

import il.co.etrader.bl_managers.ClearingManager;
import il.co.etrader.bl_managers.TransactionsManagerBase;
import il.co.etrader.util.CommonUtil;
import il.co.etrader.util.ConstantsBase;
import il.co.etrader.util.JobUtil;

/**
 * Go over the pending transactions from the previous day and make "capture" on them.
 *
 * @author Tony
 */
public class CaptureJob extends JobUtil {
    private static Logger log = Logger.getLogger(CaptureJob.class);

    /**
     * Goes over the pending transactions in the db from the previous day and make a "capture" request
     * for each of them.
     */
    private static void capturePendingTransactions() {
        String hours = getPropertyByFile("delay.hours");
        ReportSummary summery = new ReportSummary();
        StringBuffer report = new StringBuffer("<br><b>Capture transactions:</b><table border=\"1\">");
        report.append("<tr><td>User id</td><td>Transaction id</td><td>Provider</td><td>Status</td><td>Description</td><td>Amount</td><td>Currency</td></tr>");
        Connection conn = null;
        Connection conn2 = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        String utc_offset = ConstantsBase.EMPTY_STRING;
        String lastCurrencyCode = null;
        try {
            conn = getConnection();
            ClearingManager.loadClearingProviders(conn);
            String sql =
            	"SELECT " +
                    "u.id_num idnum, " +
                    "u.skin_id, " +
                    "u.currency_id, " +
                    "u.utc_offset, " +
                    "t.user_id userid, " +
                    "t.amount amount, " +
                    "t.xor_id_authorize, " +
                    "t.auth_number auth_number, " +
                    "t.capture_number capture_number, " +
                    "t.id tid, " +
                    "t.clearing_provider_id," +
                    "t.acquirer_response_id, " +
                    "c.cc_number ccnumber, " +
                    "c.cc_pass cvv, " +
                    "c.exp_month expmonth, " +
                    "curr.code, " +
                    "c.exp_year expyear, " +
                    "c.type_id type_id, " +
                    "w.user_name writer, " +
                    "c.recurring_transaction " +
            	"FROM " +
                    "transactions t " +
                    	"left JOIN transaction_postponed tp on t.id = tp.transaction_id, " +
                    "credit_cards c, " +
                    "writers w, " +
                    "users u, " +
                    "skins s, " +
                    "currencies curr, " +
                    "clearing_providers cp " +
            	"WHERE " +
            		"t.clearing_provider_id = cp.id AND " +
                    "t.user_id = u.id AND " +
                    "t.credit_card_id = c.id AND " +
                    "t.writer_id = w.id AND " +
                    "s.id = u.skin_id AND " +
                    "curr.id = u.currency_id AND " +
                    "u.class_id <> 0 AND " +
                    "t.status_id = " + TransactionsManagerBase.TRANS_STATUS_PENDING + " AND " +
                    "t.type_id = " + TransactionsManagerBase.TRANS_TYPE_CC_DEPOSIT + " AND " +
            		"cp.payment_group_id <> " + TransactionsManagerBase.PAYMENT_GROUP_EPG + " AND " ;

        	// Add an option to run job with specific transcation id
            String transactionId = getPropertyByFile("transaction.id", ConstantsBase.EMPTY_STRING);
            if (CommonUtil.isParameterEmptyOrNull(transactionId)) {
            	sql +=
        			" ((TO_CHAR(sysdate, 'mm') = c.exp_month AND TO_CHAR(sysdate, 'yy') = c.exp_year AND TO_CHAR(sysdate, 'mm') <> TO_CHAR(sysdate+1, 'mm')) OR " +
               		" (t.time_created + nvl(tp.number_of_days,  0) >= sysdate - 1 - "+hours+"/24 AND " +
               		" t.time_created + nvl(tp.number_of_days,  0) < sysdate - "+hours+"/24)) " ;
                if (log.isInfoEnabled()) {
                    log.info("No transaction.id specified");
                }
            } else {
            		sql += "t.id in ( " + transactionId + " ) ";
            }
            sql +=
                "ORDER BY " +
                    "curr.id, userid, tid";

            pstmt = conn.prepareStatement(sql);
            rs = pstmt.executeQuery();
            String providerName;
            while (rs.next()) {
                ClearingInfo info = new ClearingInfo();
                info.setTransactionId(rs.getInt("tid"));
                info.setCcn(AESUtil.decrypt(rs.getString("ccnumber")));
                info.setExpireMonth(rs.getString("expmonth"));
                info.setExpireYear(rs.getString("expyear"));
                info.setCvv(rs.getString("cvv"));
                info.setCcTypeId(rs.getInt("type_id"));
                String currentCurrencyCode = rs.getString("code");
                info.setCurrencySymbol(currentCurrencyCode);
                info.setCurrencyId(rs.getInt("currency_id"));
               	info.setCaptureNumber(rs.getString("capture_number"));
                info.setAuthNumber(rs.getString("auth_number"));
                info.setAmount(rs.getLong("amount"));
                info.setUserId(rs.getLong("userid"));
                info.setSkinId(rs.getLong("skin_id"));
                info.setProviderTransactionId(rs.getString("xor_id_authorize"));
                if (CommonUtil.isHebrewSkin(info.getSkinId())) {
                	info.setCardUserId(AESUtil.decrypt(rs.getString("idnum")));
                } else { // Ao users(no id number)
                	info.setCardUserId(ConstantsBase.NO_ID_NUM);
                }
                info.setTransactionType(TransactionsManagerBase.TRANS_TYPE_CC_DEPOSIT);
                info.setProviderId(rs.getLong("clearing_provider_id"));
                info.setAcquirerResponseId(rs.getString("acquirer_response_id"));
                info.setRecurringTransaction(rs.getString("recurring_transaction"));
                utc_offset = rs.getString("utc_offset"); //save user utc offset

                if (info.getProviderId() == ClearingManager.TESTING_PROVIDER_ID) {
                    log.info("Clearing provider is TESTING_PROVIDER_ID. Going to skip withdraw for transactionId=" + info.getTransactionId());
                    continue ;
                } 

                if (log.isEnabledFor(Level.INFO)) {
                    log.log(Level.INFO, "Going to capture transaction: " + info.toString() + " on provider: " + info.getProviderId());
                }
                try {
                    ClearingManager.capture(info);
                } catch (ClearingException ce) {
                    log.log(Level.ERROR, "Failed to capture transaction: " + info.toString(), ce);
                    info.setResult("999");
                    info.setMessage(ce.getMessage());
                }

                // currency code display
                if ( null == lastCurrencyCode || !lastCurrencyCode.equals(currentCurrencyCode) ) {
                    lastCurrencyCode = currentCurrencyCode;
                    report.append("<tr><td colspan=\"7\" align=\"center\" valign=\"middle\"><b>").append(lastCurrencyCode).append("</b></td></tr>");
                }

                if (info.isSuccessful()) {
                    report.append("<tr>");
                } else {
                    report.append("<tr style=\" color: #FF0000; font-weight: bold;\">");
                }

                providerName = ClearingManager.getProviderName(info.getProviderId());

                report.append("<td>").append(info.getUserId()).append("</td><td>");
                report.append(info.getTransactionId()).append("</td><td>");
                report.append(providerName).append("</td><td>");
                report.append(info.getResult()).append("</td><td>");
                report.append(info.getMessage()).append("</td><td>");
                report.append(ClearingUtil.formatAmount(info.getAmount())).append("</td><td>");
                report.append(currentCurrencyCode).append("</td></tr>\n");
                summery.addTransaction(rs.getString("writer"), providerName, info.getAmount(), currentCurrencyCode, info.isSuccessful());

                try {
	                conn2 = getConnection();
	                String updateSql;
		            if (info.isSuccessful()) {
		            	updateSql =
                            "UPDATE " +
                                "transactions " +
                            "SET " +
                                "time_settled = sysdate, " +
                                "status_id = " + TransactionsManagerBase.TRANS_STATUS_SUCCEED + ", " +
		            	 		"comments = '" + info.getResult() + "|" + info.getMessage() + "|" + info.getUserMessage() + "|" + info.getProviderTransactionId() + "', " +
		            	 		"xor_id_capture = '" + info.getProviderTransactionId() + "', " +
		            	 		"capture_number = '" + info.getAuthNumber() + "', " +
		            	 		"utc_offset_settled = '" + utc_offset + "' " +
                            "WHERE " +
                                "id = " + info.getTransactionId();
		            } else {
		            	updateSql =
                            "UPDATE " +
                                "transactions " +
                            "SET " +
                                "time_settled = sysdate, " +
                                "status_id = " + TransactionsManagerBase.TRANS_STATUS_FAILED + ", " +
		            			"comments = '" + info.getResult() + "|" + info.getMessage() + "|" + info.getUserMessage() + "|" + info.getProviderTransactionId() + "', " +
		          	 			"description = '" + ConstantsBase.ERROR_FAILED_CAPTURE + "', " +
                                "xor_id_capture = '" + info.getProviderTransactionId() + "', " +
		          	 			"utc_offset_settled = '" + utc_offset + "' " +
                            "WHERE " +
                                "id = " + info.getTransactionId();
		            }
		            conn2.prepareStatement(updateSql).executeUpdate();
                } finally {
                	try {
                		conn2.close();
                    } catch (Exception e) {
                    	log.log(Level.ERROR, "Can't close", e);
                    }
                }
            }
        } catch (Exception e) {
            log.log(Level.ERROR, "Error while capturing pending transactions.", e);
        } finally {
            try {
                rs.close();
            } catch (Exception e) {
                log.log(Level.ERROR, "Can't close", e);
            }
            try {
                pstmt.close();
            } catch (Exception e) {
                log.log(Level.ERROR, "Can't close", e);
            }
            try {
                conn.close();
            } catch (Exception e) {
                log.log(Level.ERROR, "Can't close", e);
            }

        }
        report.append("</table>");

        String reportBody = "<html><body>" +
            summery.getSummery() +
            report.toString() +
            "</body></html>";

        if (log.isEnabledFor(Level.INFO)) {
            log.log(Level.INFO, "Sending report email: " + reportBody);
        }

        // send email
        Hashtable<String, String> server = new Hashtable<String, String>();
        server.put("url", getPropertyByFile("email.url"));
        server.put("auth", getPropertyByFile("email.auth"));
        server.put("user", getPropertyByFile("email.user"));
        server.put("pass", getPropertyByFile("email.pass"));
        server.put("contenttype", getPropertyByFile("email.contenttype", "text/html; charset=UTF-8"));

        Hashtable<String, String> email = new Hashtable<String, String>();
        email.put("subject", getPropertyByFile("email.subject"));
        email.put("to", getPropertyByFile("email.to"));
        email.put("from", getPropertyByFile("email.from"));
        email.put("body", reportBody);
        sendEmail(server, email);
    }

    /**
     * Capture Job.
     *
     * @param args
     */
    public static void main(String[] args) {
        if (null == args || args.length < 1) {
            log.log(Level.FATAL, "Configuration file not specified, insert properties file name.");
            return;
        }
        propFile = args[0];

        if (log.isEnabledFor(Level.INFO)) {
            log.log(Level.INFO, "Starting the job.");
        }

        capturePendingTransactions();

        if (log.isEnabledFor(Level.INFO)) {
            log.log(Level.INFO, "Job completed.");
        }
    }
}