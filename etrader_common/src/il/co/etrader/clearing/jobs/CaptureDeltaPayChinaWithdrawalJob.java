package il.co.etrader.clearing.jobs;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import com.anyoption.common.beans.Transaction;
import com.anyoption.common.beans.base.Skin;
import com.anyoption.common.clearing.ClearingException;
import com.anyoption.common.clearing.ClearingInfo;
import com.anyoption.common.clearing.DeltaPayInfo;
import com.anyoption.common.daos.DAOBase;
import com.anyoption.common.daos.LanguagesDAOBase;
import com.anyoption.common.util.ClearingUtil;

import il.co.etrader.bl_managers.ClearingManager;
import il.co.etrader.bl_managers.TransactionsManagerBase;
import il.co.etrader.bl_managers.UsersManagerBase;
import il.co.etrader.bl_vos.Template;
import il.co.etrader.bl_vos.UserBase;
import il.co.etrader.bl_vos.Writer;
import il.co.etrader.dao_managers.SkinsDAOBase;
import il.co.etrader.dao_managers.TemplatesDAO;
import il.co.etrader.dao_managers.TransactionsDAOBase;
import il.co.etrader.dao_managers.UsersDAOBase;
import il.co.etrader.util.CommonUtil;
import il.co.etrader.util.ConstantsBase;
import il.co.etrader.util.JobUtil;
import il.co.etrader.util.SendTemplateEmail;

/**
 * Go over the pending transactions from the previous day and make "capture" on them.
 */
public class CaptureDeltaPayChinaWithdrawalJob extends JobUtil {
    private static Logger log = Logger.getLogger(CaptureDeltaPayChinaWithdrawalJob.class);

    public static int TRANSACTION_HAS_FEE = 0;

    public static int RUN_CREDIT = 2;  // (credit/bookBack)
    public static int DONT_RUN = 0;

    public static StringBuffer report = new StringBuffer();
    public static ReportSummary summery = new ReportSummary ();

	/**
	 * Goes over the approved (by accounting) transactions in the db from the previosday and make a "capture" request
	 * for each of them
	 */
	private static void captureWithdrawalTransactions() {
		report = new StringBuffer("<br><b>Capture withdraw transactions:</b><table border=\"1\">");
		report.append("<tr><td>User id</td><td>Transaction id</td><td>Provider</td><td>Status</td>" +
					  "<td>Description</td><td>Amount</td><td>Currency</td><td>Type</td></tr>");
		Connection conn1 = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		String utcOffset = ConstantsBase.EMPTY_STRING;

		String transactionId = getPropertyByFile("transaction.id", ConstantsBase.EMPTY_STRING);

		try {
			conn1 = getConnection();
            ClearingManager.loadClearingProviders(conn1);
			String sql =
            	"SELECT " +
                    "t.user_id userid, " +
                    "t.id tid, " +
                    "curr.code, "+
                    "t.amount amount, " +
                    "t.auth_number auth_number, " +
                    "w.user_name writer, " +
                    "u.user_name user_name, " +
                    "t.fee_cancel, " +
                    "s.default_xor_id, " +
                    "u.skin_id, " +
                    "u.utc_offset, " +
                    "u.currency_id, " +
                    "l.cc_fee, " +
                    "cntr.id AS country_id, " +
                    "cntr.a2 , " +
                    "t.ip, " +
                    "t.is_credit_withdrawal, " +
                    "t.deposit_reference_id, " +
                    "t.clearing_provider_id, " +
                    "t.type_id transaction_type_id, " +
                    "t.internals_amount, " +
                    "l.amt_for_low_withdraw, " +
                    "t.time_created, " +
                    "t.utc_offset_created " +
            	"FROM " +
                    "transactions t, " +
                    "writers w, " +
                    "users u, " +
                    "skins s, " +
                    "currencies curr, " +
                    "limits l, " +
                    "countries cntr " +
            	"WHERE " +
                    "t.user_id = u.id AND " +
                    "t.writer_id = w.id AND " +
                    "s.id = u.skin_id AND " +
                    "curr.id = u.currency_id AND " +
                    "u.limit_id = l.id AND " +
                    "u.country_id = cntr.id AND " +
                    "u.class_id <> 0 AND " +
                    "t.status_id = ? AND " +
                    "t.is_accounting_approved = ? AND ";


            	// Add an option to run job with specific transcation id
                if (CommonUtil.isParameterEmptyOrNull(transactionId)) {
                   	sql +=
                        "t.type_id = ? AND " +
                   		"t.time_created < sysdate ";

                    if (log.isInfoEnabled()) {
                        log.info("No transaction.id specified");
                    }
                } else {
                		sql +=
                			"t.id in ( " + transactionId + " ) AND " +
                			"(t.type_id = ? OR  t.type_id = ? ) ";
                }

                sql += "ORDER BY " +
                	"curr.id, userid ,tid";

			pstmt = conn1.prepareStatement(sql);

			pstmt.setLong(1, TransactionsManagerBase.TRANS_STATUS_APPROVED);
			pstmt.setLong(2, ConstantsBase.ACCOUNTING_APPROVED_YES);
			pstmt.setLong(3, TransactionsManagerBase.TRANS_TYPE_DELTAPAY_CHINA_WITHDRAW);
			if (!CommonUtil.isParameterEmptyOrNull(transactionId)) {
				pstmt.setLong(4, TransactionsManagerBase.TRANS_TYPE_INTERNAL_CREDIT);
			}

			rs = pstmt.executeQuery();

			while (rs.next()) {
                DeltaPayInfo info = new DeltaPayInfo();
                info.setRedirect(getPropertyByFile("redirect"));
                info.setAffiliate(getPropertyByFile("affiliate"));
                info.setTransactionId(rs.getInt("tid"));
                // this is for test, pass the currency usd in the property file because test account accept only usd
                String currentCurrencyCode = getPropertyByFile("currency");
                if (currentCurrencyCode.equals("?currency?")) {
                	currentCurrencyCode = rs.getString("code");
                }
                info.setCurrencySymbol(currentCurrencyCode);
                info.setCurrencyId(rs.getInt("currency_id"));
                info.setAuthNumber(rs.getString("auth_number"));
                info.setAmount(rs.getLong("amount"));
                info.setUserId(rs.getLong("userid"));
                info.setUserName(rs.getString("user_name"));
                info.setSkinId(rs.getLong("skin_id"));
                if (CommonUtil.isHebrewSkin(info.getSkinId())) {
                	info.setCardUserId(rs.getString("idnum"));
                } else {  // Ao users(no id number)
                	info.setCardUserId(ConstantsBase.NO_ID_NUM);
                }
                info.setCountryId(rs.getLong("country_id"));
                info.setCountryA2(rs.getString("a2"));
                info.setIp(rs.getString("ip"));
                //info.setTransactionType(TransactionsManagerBase.TRANS_TYPE_CC_WITHDRAW);
                info.setTransactionType(rs.getLong("transaction_type_id"));
                utcOffset = rs.getString("utc_offset"); //save user utc offset
                info.setProviderId(rs.getLong("clearing_provider_id"));
                long internalsAmount = rs.getLong("internals_amount");  // saved for failed splitted transactions
                //set fee indicaor and amount in case transaction has fee
                boolean hasFee = rs.getInt("fee_cancel") == TRANSACTION_HAS_FEE;
                long feeAmount = rs.getLong("cc_fee");
                long amountForLowWithdraw = rs.getLong("amt_for_low_withdraw");
                Date transactionTimeCreated = rs.getDate("time_created");
                String transactionUtcOffsetCreated = rs.getString("utc_offset_created");
                long amount = rs.getLong("amount");
                long amountAfterFee = amount - feeAmount;
                if (hasFee) {
                	info.setAmount(amountAfterFee);
                }

                // Credit logic
                ArrayList<Transaction> depositsList = new ArrayList<Transaction>();
                long creditAmountAllow = 0;
                boolean creditProblem = false;
                long depositRefId = rs.getLong("deposit_reference_id");

                depositsList = TransactionsDAOBase.getDeltaPayDepositListForCredit(conn1, info.getUserId());

                // calculate creditAmountAllow
                for (Transaction t : depositsList) {
                	creditAmountAllow += t.getCreditAmount();
                }

            	// credit
            	// check sum deposits in case it's cft disabled trx
            	long trxAmount = info.getAmount();
            	if (internalsAmount > 0) {
            		trxAmount =  info.getAmount() - internalsAmount;
            		if (trxAmount <= 0) {
                        log.error("Failed to capture withdrawal transaction(Credit), internals amount >= transaction amount!, " +
                        		"amount: " + info.getAmount() + " ,internalsAmount: " + internalsAmount );
                        info.setSuccessful(false);
                        info.setResult("999");
                        info.setMessage("Problem with creditWithdrawal, internalsAmount(" + internalsAmount + ") >= transaction amount(" + info.getAmount() + ").");
                        creditProblem = true;
            		}
            	}
        		if (!creditProblem && trxAmount > creditAmountAllow) {
                    log.error("Failed to capture withdrawal transaction(Credit), amount for withdrawal > creditAmount that allow, " +
                    		"amount: " + info.getAmount() + " ,creditAmountAllow: " + creditAmountAllow );
                    info.setSuccessful(false);
                    info.setResult("999");
                    info.setMessage("Problem with creditWithdrawal, withdrawalAmount(" + info.getAmount() + ") > creditAmount that allow(" + creditAmountAllow + ").");
                    creditProblem = true;
        		}

                boolean res = true;

            	// credit(bookBack) needed
            	if (creditProblem) {  // cannot operate credit action
					captureWithdrawalSingleTrx(	DONT_RUN, info, conn1, currentCurrencyCode, rs, utcOffset, hasFee, feeAmount,
												amountForLowWithdraw, transactionTimeCreated, transactionUtcOffsetCreated);
            	} else {
            		Transaction depTrx = null;
            		if (internalsAmount == 0) { // regular cases without failed internals
                		if (depositRefId > 0) {  // credit action from backend to specific deposit
                			depTrx = TransactionsDAOBase.getDepositTrxForCreditById(conn1, depositRefId);
                		}
            		}
        			if (null != depTrx) { // without split
        				info.setOriginalDepositId(depTrx.getId());
        				info.setProviderDepositId(depTrx.getXorIdCapture());
        				info.setAuthNumber(depTrx.getAuthNumber());
        				info.setProviderId(depTrx.getClearingProviderId());
						res = captureWithdrawalSingleTrx(	RUN_CREDIT, info, conn1, currentCurrencyCode, rs, utcOffset, hasFee, feeAmount,
															amountForLowWithdraw, transactionTimeCreated, transactionUtcOffsetCreated);
        				if (res) {
        					updateAfterCredit(info, depTrx.getId());
        				}
        			} else {  // split needed, cannot credit with 1 deposit trx(all trx with availableAmount<amount)  / recapture for failed internals
        				ArrayList<Transaction> internalTrx = new ArrayList<Transaction>();
        				long creditAmount = info.getAmount();


        				if (internalsAmount > 0) {  // reduce internals amount
        					creditAmount = info.getAmount() - internalsAmount;
        				}

        				for (Transaction t : depositsList) {
    						long availableAmount = t.getCreditAmount();
        					if (availableAmount > creditAmount) {   //  the amount that left for the withdrawal < availableAmount in trx
        						availableAmount = creditAmount;
        					}
        					t.setCreditAmount(availableAmount);
        					internalTrx.add(t);
        					creditAmount -= availableAmount;
        					if (creditAmount == 0) {
        						break;
        					}
        				}

        				// create the internal credit transactions
        				boolean resInternalsTrx = true;
        				HashMap<Long,Transaction> updateTrxList = new HashMap<Long,Transaction>(); // <withdrawalTrxId, internalTrxKey>
        				DeltaPayInfo splittedInfo = new DeltaPayInfo();    // save info after each innerTrx
        				splittedInfo.setRedirect(getPropertyByFile("redirect"));
        				long sumInternals = 0;

        				for (Transaction t : internalTrx) {
        					long availableAmount = t.getCreditAmount();
        					Transaction trx = createInternalTrx(info, utcOffset, availableAmount);
        					if (null == trx) {
        						resInternalsTrx = false;
        						break;
        					} else {
	        					DeltaPayInfo innerInfo = createInternalInfo(info, trx, t);
								res = captureWithdrawalSingleTrx(	RUN_CREDIT, innerInfo, conn1, currentCurrencyCode, rs, utcOffset,
																	false,
																	0, amountForLowWithdraw, transactionTimeCreated,
																	transactionUtcOffsetCreated);
	            				// save last run
	            				splittedInfo.setProviderId(innerInfo.getProviderId());
	            				splittedInfo.setResult(innerInfo.getResult());
	            				splittedInfo.setMessage(innerInfo.getMessage());
	            				splittedInfo.setSuccessful(innerInfo.isSuccessful());

	            				if (res) {
	            					long tempAmount = innerInfo.getAmount();
	            					innerInfo.setAmount(t.getAmount());
	            					updateAfterCredit(innerInfo, t.getId());
	            					innerInfo.setAmount(tempAmount);
	            					updateTrxList.put(trx.getId(), t);
	            					sumInternals += innerInfo.getAmount();
	            				} else {
	            					resInternalsTrx = false;
	            					break;
	            				}
        					}
        				}

        				// set run info in the splitted trx
        				if (null != splittedInfo) {
	        				info.setProviderId(splittedInfo.getProviderId());
	        				info.setResult(splittedInfo.getResult());
	        				info.setMessage(splittedInfo.getMessage());
	        				info.setSuccessful(splittedInfo.isSuccessful());
        				}

        				// update splitted transaction
			        	addSplittedTrxToReport(info, currentCurrencyCode, rs.getString("writer"));  // add splittedTrx to report + summary
						updateTransaction(	info, hasFee, feeAmount, utcOffset, resInternalsTrx, true, updateTrxList.keySet().toString(),
											sumInternals, amountForLowWithdraw, transactionTimeCreated, transactionUtcOffsetCreated);
        			}
            	}
                // Send email to user
                if(info.isSuccessful() &&
                		(info.getTransactionType() == TransactionsManagerBase.TRANS_TYPE_DELTAPAY_CHINA_WITHDRAW)) {
                   	String ls = System.getProperty("line.separator");
    				log.info("Going to send withdrawal email: " + ls +
    								"userId: " + info.getUserId() + ls +
    								"trxId: " + info.getTransactionId() + ls);
    		        try {
    		        	   Hashtable<String, String>emailProperties = new Hashtable<String, String>();
    				       long langId = SkinsDAOBase.getById(conn1, (int)info.getSkinId()).getDefaultLanguageId();
    				       String langCode = LanguagesDAOBase.getCodeById(conn1, langId);
    				       log.debug("skinId: " + info.getSkinId() + " , langId: " + langId + ", langCode: " + langCode);
    				       mailTemplate = getEmailTemplate(ConstantsBase.TEMPLATE_WITHDRAW_CC_SUCCEED,
    				    		   langCode, String.valueOf(info.getSkinId()));

    				       emailProperties = new Hashtable<String, String>();
    				       HashMap<String,String> params = new HashMap<String, String>();

    				       UserBase uBase = new UserBase();
    				       uBase.setUserName(info.getUserName());
    				       UsersDAOBase.getByUserName(conn1,uBase.getUserName(),uBase, true);
    				       setCityName(uBase);
   				    	   emailProperties.put("subject", properties.getProperty("withdrawal.user.email." + langCode));
    				       String currencySym = properties.getProperty(CommonUtil.getCurrencySymbol(info.getCurrencyId()));
    				       String amountTxt = currencySym + ClearingUtil.formatAmount(info.getAmount());

    				       params.put(SendTemplateEmail.PARAM_USER_ID,String.valueOf(uBase.getId()));
    		   		       params.put(SendTemplateEmail.PARAM_USER_FIRST_NAME,uBase.getFirstName());
    		   		       params.put(SendTemplateEmail.PARAM_USER_LAST_NAME, uBase.getLastName());
    		   		       params.put(SendTemplateEmail.PARAM_USER_ADDRESS_NUMBER, uBase.getStreetNo());
    		   		       params.put(SendTemplateEmail.PARAM_USER_ADDRESS_STREET, uBase.getStreet());
    		   		       params.put(SendTemplateEmail.PARAM_USER_ADDRESS_CITY, uBase.getCityName());
    		   		       params.put(SendTemplateEmail.PARAM_USER_ADDRESS_ZIP_CODE, uBase.getZipCode());
    		   		       params.put(SendTemplateEmail.PARAM_TRANSACTION_AMOUNT, amountTxt);
    		   		       params.put(SendTemplateEmail.PARAM_TRANSACTION_PAN, info.getCc4Digits());
    		   		       Date date = new Date();
    		   		       params.put(SendTemplateEmail.PARAM_DATE_DMMYYYY, new SimpleDateFormat("dd.MM.yy").format(date));
    		   		       params.put(SendTemplateEmail.PARAM_DATE_MMMMMDYYYY, new SimpleDateFormat("dd/MM/yyyy").format(date));

    		   		       if (null != uBase.getGender()) {
	    		   				String genderTxt = uBase.getGenderForTemplateEnHeUse();
	    		   				params.put(SendTemplateEmail.PARAM_GENDER_DESC_EN_OR_HE, properties.getProperty(genderTxt + "." + langCode));
    		   		       }

    					   try {
    						   emailProperties.put("body",getEmailBody(params));
    					   } catch (Exception e) {
    						   log.fatal("!!! ERROR >> getEmailBody: " + e.getMessage());
    						   System.exit(1);
    					   }

    					   // send email for tracking
						   String[] splitTo = properties.getProperty("succeed.email.to").split(";");
						   for (int i = 0; i < splitTo.length; i++) {
							    emailProperties.put("to", splitTo[i]);
								sendSingleEmail(properties, emailProperties, langCode);
						   }

						   // send email to user
    					   emailProperties.put("to", uBase.getEmail());
    					   sendSingleEmail(properties,emailProperties, langCode);

    					   // add to user MailBox
						   if (uBase.getSkinId() != Skin.SKIN_ETRADER) {
							   Template t = TemplatesDAO.get(conn1, Template.CC_WITHDRAWAL_SUCCESS_MAIL_ID);
							   UsersManagerBase.SendToMailBox(uBase, t, Writer.WRITER_ID_AUTO, langId, info.getTransactionId(), conn1, 0);
						   }

    		        } catch (Exception e) {
    		        	log.error("Error, Can't send withdrawal email! " + e);
    				}
                } else {
                	log.info("trx failed, withdrawal email not sent!");
                }
             }
        } catch (Exception e) {
            log.log(Level.ERROR, "Error while capturing pending transactions.", e);
        } finally {
            try {
                rs.close();
            } catch (Exception e) {
                log.log(Level.ERROR, "Can't close", e);
            }
            try {
                pstmt.close();
            } catch (Exception e) {
                log.log(Level.ERROR, "Can't close", e);
            }
            try {
                conn1.close();
            } catch (Exception e) {
                log.log(Level.ERROR, "Can't close", e);
            }
        }
        report.append("</table>");
    }


	private static void setCityName(UserBase userBase) {

		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			con = getConnection();
	        String sql =
                "SELECT " +
                    "u.city_id, u.city_name, c.name " +
                "FROM " +
                    "users u left join cities c on u.city_id=c.id " +
                "WHERE " +
                    "u.user_name = ?";


			ps = con.prepareStatement(sql);
			ps.setString(1, userBase.getUserName().toUpperCase());
			rs = ps.executeQuery();
			String cityName;
			if (rs.next()) {
				if (rs.getLong("city_id") > 0) {
					cityName = rs.getString("name");
				} else {
					cityName = rs.getString("city_name");
				}
				userBase.setCityNameForJob(cityName);
			}
		} catch (Exception e) {
            log.log(Level.ERROR, "Can't set cityName", e);
		} finally {
			DAOBase.closeResultSet(rs);
			DAOBase.closeStatement(ps);
			try {
                con.close();
            } catch (Exception e) {
                log.log(Level.ERROR, "Can't close", e);
            }

		}
	}

	/**
	 * This method create a fee transaction and sets it accoring to the clearing info
	 * @param info - Clearing info sent to Xor
	 * @return New fee transaction
	 */
	private static Transaction getFeeTransaction(ClearingInfo info) {
		return getFeeTransaction(info, TransactionsManagerBase.TRANS_TYPE_HOMO_FEE);
	}

	private static Transaction getFeeTransaction(ClearingInfo info, int transTypeId) {
		Transaction t = new Transaction();
    	t.setUserId(info.getUserId());
    	t.setTypeId(transTypeId);
    	t.setTimeCreated(new Date());
    	t.setStatusId(TransactionsManagerBase.TRANS_STATUS_SUCCEED);
    	t.setWriterId(Writer.WRITER_ID_AUTO);
    	t.setIp("IP NOT FOUND!");
    	t.setTimeSettled(new Date());
    	t.setProcessedWriterId(Writer.WRITER_ID_AUTO);
    	t.setReferenceId(new BigDecimal(info.getTransactionId()));
    	t.setAuthNumber(info.getAuthNumber());
    	t.setXorIdCapture(info.getProviderTransactionId());
    	return t;
	}

	/**
	 * withdrawal action to 1 trx.
	 * @param type withdrawal type(cft/credit)
	 * @param info ClearingInfo instance of the trx
	 * @param currentCurrencyCode currency code of the trx
	 * @param utcOffset trx offset
	 * @param hasFee in case we need to take fee
	 * @param feeAmount fee amount
	 * @param conn1 db connection
	 * @param rs ResultSet instance
	 * @return true in case the process completed successfully
	 * @throws SQLException
	 */
	public static boolean captureWithdrawalSingleTrx(int type, DeltaPayInfo info, Connection conn1, String currentCurrencyCode,
														ResultSet rs, String utcOffset, boolean hasFee, long feeAmount,
														long amountForLowWithdraw, Date transactionTimeCreated,
														String transactionUtcOffsetCreated) throws SQLException {

		String providerName = "";
		String withdrawalType = "";
		boolean captureRes = true;

		if (type == RUN_CREDIT) {
			withdrawalType = "(Credit)";
		}

        if (log.isEnabledFor(Level.INFO)) {
        	if (type == RUN_CREDIT) {
        		log.log(Level.INFO, "Going to capture transaction" + withdrawalType + ": " + info.toString());
        	} else {
        		log.log(Level.INFO, "Don't going to capture transaction, capture cannot proceed!" + info.toString());
        	}
        }
        try {
        	if (type == RUN_CREDIT) {
        		ClearingManager.setDeltaPayChinaBookBack(info);
        	}
        } catch (ClearingException ce) {
            log.log(Level.ERROR, "Failed to capture withdrawal transaction" + withdrawalType + ": " + info.toString(), ce);
            info.setResult("999");
            info.setMessage(ce.getMessage());
            captureRes = false;
        }

        // currency code display
        report.append("<tr><td colspan=\"7\" align=\"center\" valign=\"middle\"><b>").append(currentCurrencyCode).append("</b></td></tr>");

        if (info.isSuccessful()) {
            report.append("<tr>");
        } else {
        	if (type == RUN_CREDIT && info.getTransactionType() == TransactionsManagerBase.TRANS_TYPE_INTERNAL_CREDIT) {
        		report.append("<tr style=\" color: #C24641; font-weight: bold;\">");
        	} else {
        		report.append("<tr style=\" color: #FF0000; font-weight: bold;\">");
        	}
        }

        providerName = ClearingManager.getProviderName(info.getProviderId());

        report.append("<td>").append(info.getUserId()).append("</td><td>");

        String actionType = "";
        String splittedId = "";  // display splittedId for internal credit trx
        if (type == RUN_CREDIT && info.getTransactionType() == TransactionsManagerBase.TRANS_TYPE_INTERNAL_CREDIT) {
        	splittedId = "(splittedTrx:" + String.valueOf(rs.getInt("tid")) + ")";
        }
        report.append(info.getTransactionId()).append(splittedId).append("</td><td>");
        report.append(providerName).append("</td><td>");
        report.append(info.getResult()).append("</td><td>");
        report.append(info.getMessage()).append("</td><td>");
        report.append(ClearingUtil.formatAmount(info.getAmount())).append("</td><td>");
        report.append(currentCurrencyCode).append("</td><td>");
    	if (type == RUN_CREDIT && info.getTransactionType() == TransactionsManagerBase.TRANS_TYPE_INTERNAL_CREDIT) {
    		actionType = "Credit(internal)";
    	} else {
    		actionType = "Credit";
    	}

        report.append(actionType).append("</td></tr>\n");
        if ((!(type == RUN_CREDIT && info.getTransactionType() == TransactionsManagerBase.TRANS_TYPE_INTERNAL_CREDIT))
        		&& type != DONT_RUN) {
        	summery.addTransaction(rs.getString("writer"), providerName, info.getAmount(), currentCurrencyCode, info.isSuccessful());
        }

        if (info.isSuccessful()) {
        	captureRes = true;
        } else {
        	captureRes = false;
        }

        updateTransaction(info, hasFee, feeAmount, utcOffset, info.isSuccessful(), false, null, 0, amountForLowWithdraw, transactionTimeCreated, transactionUtcOffsetCreated);

        return captureRes;

    }

	/**
	 * Update transaction after Credit action.
	 * Need to update deposit_reference_id on the withdrawal trx and to update
	 * credit_amount on the proper deposit trx.
	 * @param info	ClearingInfo instance
	 * @param depositTrxId the proper deposit transaction for the withdrawal
	 */
	public static void updateAfterCredit(ClearingInfo info, long depositTrxId) {

		Connection con = null;
		PreparedStatement ps = null;
		PreparedStatement ps2 = null;

		try {
			con = getConnection();
			con.setAutoCommit(false);

			String sql = "UPDATE " +
						 	"transactions " +
						 "SET " +
						 	"deposit_reference_id = ? " +
							", is_credit_withdrawal = 1 ";

						 sql += "WHERE id = ? ";

			ps = con.prepareStatement(sql);
			ps.setLong(1, depositTrxId);
			ps.setLong(2, info.getTransactionId());
			ps.executeUpdate();

			String sql2 = "UPDATE " +
						  	 "transactions " +
						  "SET " +
						  	 "credit_amount = credit_amount + ? " +
						  "WHERE " +
						  	 "id = ? ";

			ps2 = con.prepareStatement(sql2);
			ps2.setLong(1, info.getAmount());
			ps2.setLong(2, depositTrxId);
			ps2.executeUpdate();
			con.commit();

		} catch (Exception e) {
			log.log(Level.ERROR, "ERROR! Can't update transaction after credit. ", e);
			try {
				con.rollback();
			} catch (Exception er) {
				log.log(Level.ERROR, "Can't rollBack! ", er);
			}
		} finally {
			try {
				con.setAutoCommit(true);
			} catch (SQLException e) {
				log.error("Can't set back to autocommit.", e);
			}
        	try {
        		ps.close();
        	} catch (Exception ep) {
        		log.log(Level.ERROR, "Can't close: PreparedStatement1!", ep);
        	}
        	try {
        		ps2.close();
        	} catch (Exception ep2) {
        		log.log(Level.ERROR, "Can't close: PreparedStatement2!", ep2);
        	}
        	try {
        		con.close();
        	} catch (Exception ec) {
        		log.log(Level.ERROR, "Can't close: connection!", ec);
			}
		}
	}


	/**
	 * Create an internal withdrawal transaction
	 * @param info ClearingInfo instance of the original withdrawal trx
	 * @param utcOffset user offset
	 * @param ccId creditCard id of the original trx
	 * @param userId userId of the original trx
	 * @return
	 */
	public static Transaction createInternalTrx(ClearingInfo info, String utcOffset, long amount) {
		Transaction tran = new Transaction();
		tran.setAmount(amount);
		tran.setComments(null);
		tran.setCreditCardId(null);
		tran.setIp(info.getIp());
		tran.setProcessedWriterId(Writer.WRITER_ID_AUTO);
		tran.setTimeSettled(null);
		tran.setTimeCreated(new Date());
		tran.setUtcOffsetCreated(utcOffset);
		tran.setTypeId(TransactionsManagerBase.TRANS_TYPE_INTERNAL_CREDIT);
		tran.setUserId(info.getUserId());
		tran.setWriterId(Writer.WRITER_ID_AUTO);
		tran.setWireId(null);
		tran.setChargeBackId(null);
		tran.setReceiptNum(null);
		tran.setChequeId(null);
		tran.setFeeCancel(true);
		tran.setSplittedReferenceId(info.getTransactionId());
		tran.setDescription(null);
		tran.setStatusId(TransactionsManagerBase.TRANS_STATUS_APPROVED);
		tran.setAccountingApproved(true);
		tran.setCreditWithdrawal(true);

		Connection con = null;
		try {
			con = getConnection();
			TransactionsDAOBase.insert(con, tran);
		} catch (Exception e) {
			log.log(Level.ERROR, "ERROR! Can't create internalCreditTrx for trxId: " + tran.getSplittedReferenceId() + ", " +  e);
			tran = null;
		} finally {
        	try {
        		con.close();
        	} catch (Exception ec) {
        		log.log(Level.ERROR, "Can't close: connection!", ec);
        		tran = null;
			}
		}
		return tran;
	}

	/**
	 * Create and return internal clearingInfo instance
	 * @param info ClearingInfo instance of the original transaction
	 * @param trx  Internal withdrawal transaction
	 * @param depoTrx Deposit transaction instance
	 * @return
	 */
	public static DeltaPayInfo createInternalInfo(DeltaPayInfo info, Transaction trx, Transaction depoTrx) {

		DeltaPayInfo innerInfo = new DeltaPayInfo();
		innerInfo.setRedirect(info.getRedirect());
		innerInfo.setAffiliate(info.getAffiliate());
		innerInfo.setTransactionId(trx.getId());
		innerInfo.setCcn(info.getCcn());
		innerInfo.setExpireMonth(info.getExpireMonth());
		innerInfo.setExpireYear(info.getExpireYear());
		innerInfo.setCvv(info.getCvv());
		innerInfo.setCcTypeId(info.getCcTypeId());
		innerInfo.setOwner(info.getOwner());
        innerInfo.setCurrencySymbol(info.getCurrencySymbol());
        innerInfo.setCurrencyId(info.getCurrencyId());
        innerInfo.setAmount(trx.getAmount());
        innerInfo.setUserId(trx.getUserId());
        innerInfo.setSkinId(info.getSkinId());
        innerInfo.setCountryId(info.getCountryId());
        innerInfo.setCountryA2(info.getCountryA2());
        innerInfo.setIp(trx.getIp());
        innerInfo.setTransactionType(trx.getTypeId());
        innerInfo.setCardUserId(info.getCardUserId());

        innerInfo.setOriginalDepositId(depoTrx.getId());
        innerInfo.setProviderDepositId(depoTrx.getXorIdCapture());
        innerInfo.setAuthNumber(depoTrx.getAuthNumber());
        innerInfo.setProviderId(depoTrx.getClearingProviderId());

        return innerInfo;
	}

	/**
	 * Add splitted trx to the report and to summary
	 * @param info ClearingInfo instance of the original trx
	 * @param currentCurrencyCode currency code
	 */
	public static void addSplittedTrxToReport(ClearingInfo info, String currentCurrencyCode, String writer) {
        if (info.isSuccessful()) {
            report.append("<tr>");
        } else {
            report.append("<tr style=\" color: #FF0000; font-weight: bold;\">");
        }

        String providerName = ClearingManager.getProviderName(info.getProviderId());

        report.append("<td>").append(info.getUserId()).append("</td><td>");
        String splittedStr = "(splittedTrx)";
        report.append(info.getTransactionId()).append(splittedStr).append("</td><td>");
        report.append(providerName).append("</td><td>");
        report.append(info.getResult()).append("</td><td>");
        report.append(info.getMessage()).append("</td><td>");
        report.append(ClearingUtil.formatAmount(info.getAmount())).append("</td><td>");
        report.append(currentCurrencyCode).append("</td><td>");
        report.append("Credit").append("</td></tr>\n");

        summery.addTransaction(writer, providerName, info.getAmount(), currentCurrencyCode, info.isSuccessful());
	}


	/**
	 * Update transaction after captureWithdrawal
	 * @param info ClearingInfo instance of the current trx that captured
	 * @param hasFee true in case we need to create fee transaction
	 * @param feeAmount fee amount to take
	 * @param utcOffset user utcOffset
	 * @throws SQLException
	 */
	public static void updateTransaction(ClearingInfo info, boolean hasFee, long feeAmount, String utcOffset, boolean isSuccessful,
	                                     boolean splittedUpdate, String internalTransactions, long internalsAmount, long amountForLowWithdraw,
	                                     Date transactionTimeCreated, String transactionUtcOffsetCreated) throws SQLException {

		Connection conn = null;
		PreparedStatement ps = null;
        try {
        	conn = getConnection();
            String updateSql;
        	if (isSuccessful) { //handle success from provider
              	//update transaction status to success and amount to the amount after fee (incase there was fee)
        		updateSql =
                    "UPDATE " +
                        "transactions " +
                    "SET " +
                        "time_settled = sysdate, " +
                        "status_id =  ?, ";
        				if(!splittedUpdate) {
        					updateSql += "comments = ? || '|' || ? || '|' || ? || '|' || ? , " +
        								 "xor_id_capture = ? , ";
        				} else {
        					updateSql += "comments =  ? || '|' || ? || '|' || ? || '| SplittedTrx to: ' || ? , ";
        				}

        				updateSql += "amount =  ?, " +
               						 "utc_offset_settled = ?, " +
               						 "clearing_provider_id = ? ";

						 if (splittedUpdate) {  // in case we update spllited trx(not after captue) and the trx was not credit on creation.
							 updateSql += ",is_credit_withdrawal = 1 ";
						 }

						 updateSql += "WHERE " +
						 				"id = ? ";

						 ps = conn.prepareStatement(updateSql);
						 ps.setInt(1, TransactionsManagerBase.TRANS_STATUS_SUCCEED);
						 ps.setString(2, info.getResult());
						 ps.setString(3, info.getMessage());
						 ps.setString(4, info.getUserMessage());
						 if (!splittedUpdate) {
							 ps.setString(5, info.getAuthNumber());
							 ps.setString(6, info.getAuthNumber());
							 ps.setLong(7, info.getAmount());
							 ps.setString(8, utcOffset);
							 ps.setLong(9, info.getProviderId());
							 ps.setLong(10, info.getTransactionId());

						 } else {
							 ps.setString(5, internalTransactions);
							 ps.setLong(6, info.getAmount());
							 ps.setString(7, utcOffset);
							 ps.setLong(8, info.getProviderId());
							 ps.setLong(9, info.getTransactionId());
						 }
				
				//!!!										   !!!
				//!!CRITICAL SECTION - must be atomic statement!!!
				//!!!										   !!!
				//method assumption - make one time functionality for each transaction.
				
        		conn.setAutoCommit(false); //in order to support rollback once and update or an insert was not succesfull

               	//insert new fee transaction
               	if (hasFee) {
               		Transaction feeTransaction;
					feeTransaction = getFeeTransaction(info); //creating the fee transaction accoring to the orig transaction
					feeTransaction.setAmount(feeAmount);

                	feeTransaction.setUtcOffsetCreated(utcOffset);
                	feeTransaction.setUtcOffsetSettled(utcOffset);
    				TransactionsDAOBase.insert(conn, feeTransaction);
    				if (log.isInfoEnabled()) {
    					String ls = System.getProperty("line.separator");
    					log.info("fee transaction inserted: " + ls + feeTransaction.toString());
    				}

    				//update Transaction reference
    				TransactionsDAOBase.updateRefId(conn, info.getTransactionId(), feeTransaction.getId());
               	}
        	} else { //handle failed from provider
        		//update transaction status to failed
        		updateSql =
                    "UPDATE " +
                        "transactions " +
                    "SET " +
                        "time_settled = sysdate, " +
                        "status_id = ? , " +
      	 				"comments = ? || '|' || ? || '|' || ? || '|' || ? , " +
      	 				"description = ? , ";
        				if (!splittedUpdate) {
        					updateSql += "xor_id_capture = ?, ";
        				}
        				if (splittedUpdate) {
        					updateSql += "internals_amount = internals_amount + " + internalsAmount + ", ";
        				}
        				updateSql +=  "utc_offset_settled = ?, " +
                        			  "clearing_provider_id = ? " +
                        			  "WHERE " +
                        			  		"id = ? ";

       				 ps = conn.prepareStatement(updateSql);
					 ps.setInt(1, TransactionsManagerBase.TRANS_STATUS_FAILED);
					 ps.setString(2, info.getResult());
					 ps.setString(3, info.getMessage());
					 ps.setString(4, info.getUserMessage());
					 ps.setString(5, info.getProviderTransactionId());
					 ps.setString(6, ConstantsBase.ERROR_FAILED_CAPTURE);
					 if (!splittedUpdate) {
						 ps.setString(7, info.getProviderTransactionId());
						 ps.setString(8, utcOffset);
						 ps.setLong(9, info.getProviderId());
						 ps.setLong(10, info.getTransactionId());
					 } else {
						 ps.setString(7, utcOffset);
						 ps.setLong(8, info.getProviderId());
						 ps.setLong(9, info.getTransactionId());
					 }

        	}
        	ps.executeUpdate();
        	conn.commit();
        } catch (Exception e) {
			log.log(Level.ERROR, "ERROR! Can't update transaction!. ", e);
			try {
				conn.rollback();
			} catch (Exception er) {
				log.log(Level.ERROR, "Can't rollBack! ", er);
			}
		} finally {
			try {
				conn.setAutoCommit(true);
			} catch (SQLException e) {
				log.error("Can't set back to autocommit.", e);
			}
			try {
        		conn.close();
        	} catch (Exception e) {
        		log.log(Level.ERROR, "Can't close: conn2", e);
        	}
		}
	}


    /**
     * Capture Withdrawal Job.
     *
     * @param args
     */
    public static void main(String[] args) {
        if (null == args || args.length < 1) {
            log.log(Level.FATAL, "Configuration file not specified, insert properties file name.");
            return;
        }
        propFile = args[0];

        if (log.isEnabledFor(Level.INFO)) {
            log.log(Level.INFO, "Starting the job.");
        }

        captureWithdrawalTransactions();

        String reportBody = "<html><body>" +
            summery.getSummery() +
            report.toString() +
            "</body></html>";

        if (log.isEnabledFor(Level.INFO)) {
            log.log(Level.INFO, "Sending report email: " + reportBody);
        }

        // send email
        Hashtable<String, String> server = new Hashtable<String, String>();
        server.put("url", getPropertyByFile("email.url"));
        server.put("auth", getPropertyByFile("email.auth"));
        server.put("user", getPropertyByFile("email.user"));
        server.put("pass", getPropertyByFile("email.pass"));
        server.put("contenttype", getPropertyByFile("email.contenttype", "text/html; charset=UTF-8"));

        Hashtable<String, String> email = new Hashtable<String, String>();
        email.put("subject", getPropertyByFile("email.subject"));
        email.put("to", getPropertyByFile("email.to"));
        email.put("from", getPropertyByFile("email.from"));
        email.put("body", reportBody);
        //sendEmail(server, email);

        if (log.isEnabledFor(Level.INFO)) {
            log.log(Level.INFO, "Job completed.");
        }
    }
}