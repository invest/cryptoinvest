//package il.co.etrader.clearing.jobs;
//
//import il.co.etrader.bl_managers.ClearingManager;
//import il.co.etrader.bl_managers.TransactionsManagerBase;
//import il.co.etrader.bl_vos.UserBase;
//import il.co.etrader.bl_vos.Writer;
//import il.co.etrader.clearing.PayPalInfo;
//import il.co.etrader.clearing.PayPalInfo.MassPayItem;
//import il.co.etrader.dao_managers.SkinsDAOBase;
//import il.co.etrader.dao_managers.TransactionsDAOBase;
//import il.co.etrader.dao_managers.UsersDAOBase;
//import il.co.etrader.util.CommonUtil;
//import il.co.etrader.util.ConstantsBase;
//import il.co.etrader.util.JobUtil;
//import il.co.etrader.util.SendTemplateEmail;
//
//import java.math.BigDecimal;
//import java.sql.Connection;
//import java.sql.PreparedStatement;
//import java.sql.ResultSet;
//import java.sql.SQLException;
//import java.text.SimpleDateFormat;
//import java.util.Date;
//import java.util.HashMap;
//import java.util.Hashtable;
//
//import org.apache.log4j.Level;
//import org.apache.log4j.Logger;
//
//import com.anyoption.common.beans.Transaction;
//import com.anyoption.common.clearing.ClearingException;
//import com.anyoption.common.clearing.ClearingInfo;
//import com.anyoption.common.daos.DAOBase;
//import com.anyoption.common.daos.LanguagesDAOBase;
//import com.anyoption.common.util.ClearingUtil;
//
///**
// * PayPal "capture" withdrawal.
// * Go over the pending payPal transactions and "capture" them via payPal API (Mass paym method)
// *
// * @author Kobi
// */
//public class PayPalWithdrawalJob extends JobUtil {
//    private static Logger log = Logger.getLogger(PayPalWithdrawalJob.class);
//
//    public static int TRANSACTION_HAS_FEE = 0;
//    public static int NUMBER_OF_HOURS_IN_A_DAY = 24;
//    public static StringBuffer report = new StringBuffer();
//    public static ReportSummary summery = new ReportSummary();
//    public static String lastCurrencyCode = null;
//
//	/**
//	 * Goes over the approved (by accounting) transactions in the db from the previosday and make a "capture" request
//	 * for each of them
//	 */
//	private static void captureWithdrawalTransactions() {
//		report = new StringBuffer("<br><b>Capture PayPal withdraw transactions:</b><table border=\"1\">");
//		report.append("<tr><td>User id</td><td>Transaction id</td><td>Status</td>" +
//					  "<td>Description</td><td>Amount</td><td>Currency</td></tr>");
//		Connection conn1 = null;
//		PreparedStatement pstmt = null;
//		ResultSet rs = null;
//		String utcOffset = ConstantsBase.EMPTY_STRING;
//
//		String transactionId = getPropertyByFile("transaction.id", ConstantsBase.EMPTY_STRING);
//
//		try {
//			conn1 = getConnection();
//            ClearingManager.loadClearingProviders(conn1);
//			String sql =
//            	"SELECT " +
//                    "t.user_id userid, " +
//                    "t.id tid, " +
//                    "curr.code, "+
//                    "t.amount amount, " +
//                    "w.user_name writer, " +
//                    "u.user_name user_name, " +
//                    "t.fee_cancel, " +
//                    "u.skin_id, " +
//                    "u.utc_offset, " +
//                    "u.currency_id, " +
//                    "l.paypal_fee, " +
//                    "t.paypal_email, " +
//                    "t.rate " +
//            	"FROM " +
//                    "transactions t, " +
//                    "writers w, " +
//                    "users u, " +
//                    "skins s, " +
//                    "currencies curr, " +
//                    "limits l " +
//            	"WHERE " +
//                    "t.user_id = u.id AND " +
//                    "t.writer_id = w.id AND " +
//                    "s.id = u.skin_id AND " +
//                    "curr.id = u.currency_id AND " +
//                    "u.limit_id = l.id AND " +
//                    "u.class_id <> 0 AND " +
//                    "u.skin_id <> 1 AND " +
//                    "t.status_id = ? AND " +
//                    "t.is_accounting_approved = ? AND ";
//
//                	// Add an option to run job with specific transcation id
//                    if (CommonUtil.isParameterEmptyOrNull(transactionId)) {
//                       	sql +=
//                            "t.type_id = ? AND " +
//                       		"t.time_created < sysdate ";
//
//                        if (log.isInfoEnabled()) {
//                            log.info("No transaction.id specified");
//                        }
//                    } else {
//                    		sql +=
//                    			"t.id = " + transactionId + " AND t.type_id = ? ";
//                    }
//
//                sql +=
//                	"ORDER BY " +
//                    	"curr.id, userid ,tid";
//
//			pstmt = conn1.prepareStatement(sql);
//
//			pstmt.setLong(1, TransactionsManagerBase.TRANS_STATUS_APPROVED);
//			pstmt.setLong(2, ConstantsBase.ACCOUNTING_APPROVED_YES);
//			pstmt.setLong(3, TransactionsManagerBase.TRANS_TYPE_PAYPAL_WITHDRAW);
//
//			rs = pstmt.executeQuery();
//
//			while (rs.next()) {
//                PayPalInfo info = new PayPalInfo();
//                String currentCurrencyCode = rs.getString("code");
//                info.setCurrencySymbol(currentCurrencyCode);
//                info.setCurrencyId(rs.getInt("currency_id"));
//                info.setUserId(rs.getLong("userid"));
//                info.setUserName(rs.getString("user_name"));
//                info.setSkinId(rs.getLong("skin_id"));
//                info.setTransactionType(TransactionsManagerBase.TRANS_TYPE_PAYPAL_WITHDRAW);
//                info.setTransactionId(rs.getInt("tid"));
//                info.setAmount(rs.getLong("amount"));
//                utcOffset = rs.getString("utc_offset"); //save user utc offset
//
//                //set fee indicaor and amount in case transaction has fee
//                boolean hasFee = rs.getInt("fee_cancel") == TRANSACTION_HAS_FEE;
//                long feeAmount = rs.getLong("paypal_fee");
//                long amount = rs.getLong("amount");
//                long amountAfterFee = amount - feeAmount;
//                if (hasFee) {
//                	info.setAmount(amountAfterFee);
//                }
//
//                // set PP Email. Note: there is an option to request N withdrawals per currency
//                MassPayItem[] emails = new MassPayItem[1];
//            	emails[0] = info.new MassPayItem();
//            	emails[0].setAmount(info.getAmount());
//            	emails[0].setEmail(rs.getString("paypal_email"));
//            	emails[0].setTransactionId(rs.getInt("tid"));
//
//            	// Handle TRY currency convertion to USD (not supported by PayPal)
//   			 	if (info.getCurrencyId() == ConstantsBase.CURRENCY_TRY_ID) {
//   			 		info.setCurrencySymbol("USD");
//   			 		emails[0].setAmount(Math.round(emails[0].getAmount() * rs.getDouble("rate")));
//   			 	}
//
//                info.setEmails(emails);
//
//                log.info("Going to capture paypal transaction: " + info.toString());
//                // call Mass pay method
//                try {
//                	ClearingManager.MassPay(info);
//                } catch (ClearingException ce) {
//                    log.log(Level.ERROR, "Failed to capture PayPal transaction: " + info.toString(), ce);
//                    info.setResult("999");
//                    info.setMessage(ce.getMessage());
//                }
//
//                // currency code display
//                if ( null == lastCurrencyCode || !lastCurrencyCode.equals(currentCurrencyCode) ) {
//                	lastCurrencyCode = currentCurrencyCode;
//                    report.append("<tr><td colspan=\"7\" align=\"center\" valign=\"middle\"><b>").append(lastCurrencyCode).append("</b></td></tr>");
//                }
//
//                if (info.isSuccessful()) {
//                    report.append("<tr>");
//                } else {
//               		report.append("<tr style=\" color: #FF0000; font-weight: bold;\">");
//                }
//
//                report.append("<td>").append(info.getUserId()).append("</td><td>");
//                report.append(info.getTransactionId()).append("</td><td>");
//                report.append(info.getResult()).append("</td><td>");
//                report.append(info.isSuccessful() ? info.getMessage() : info.getUserMessage()).append("</td><td>");
//                report.append(ClearingUtil.formatAmount(info.getAmount())).append("</td><td>");
//                report.append(currentCurrencyCode).append("</td></tr>\n");
//
//                updateTransaction(info, hasFee, feeAmount, utcOffset, info.isSuccessful());
//
//                summery.addTransaction(rs.getString("writer"), "PayPal", info.getAmount(), currentCurrencyCode, info.isSuccessful());
//
//               	// Send email to user
//                if(info.isSuccessful()) {
//                   	String ls = System.getProperty("line.separator");
//    				log.info("Going to send paypal withdrawal email: " + ls +
//    								"userId: " + info.getUserId() + ls +
//    								"trxId: " + info.getTransactionId() + ls);
//    		        try {
//    		        	   Hashtable<String, String>emailProperties = new Hashtable<String, String>();
//    				       long langId = SkinsDAOBase.getById(conn1, (int)info.getSkinId()).getDefaultLanguageId();
//    				       String langCode = LanguagesDAOBase.getCodeById(conn1, langId);
//    				       log.debug("skinId: " + info.getSkinId() + " , langId: " + langId + ", langCode: " + langCode);
//    				       mailTemplate = getEmailTemplate(ConstantsBase.TEMPLATE_WITHDRAW_PAYPAL_SUCCEED,
//    				    		   langCode, String.valueOf(info.getSkinId()));
//
//    				       emailProperties = new Hashtable<String, String>();
//    				       HashMap<String,String> params = new HashMap<String, String>();
//
//    				       UserBase uBase = new UserBase();
//    				       uBase.setUserName(info.getUserName());
//    				       UsersDAOBase.getByUserName(conn1,uBase.getUserName(),uBase, true);
//    				       setCityName(uBase);
//
//   				    	   emailProperties.put("subject", properties.getProperty("withdrawal.user.email.en"));
//
//    				       String currencySym = properties.getProperty(CommonUtil.getCurrencySymbol(info.getCurrencyId()));
//    				       String amountTxt = currencySym + ClearingUtil.formatAmount(info.getAmount());
//
//    				       params.put(SendTemplateEmail.PARAM_USER_ID,String.valueOf(uBase.getId()));
//    		   		       params.put(SendTemplateEmail.PARAM_USER_FIRST_NAME,uBase.getFirstName());
//    		   		       params.put(SendTemplateEmail.PARAM_USER_LAST_NAME, uBase.getLastName());
//    		   		       params.put(SendTemplateEmail.PARAM_USER_ADDRESS_NUMBER, uBase.getStreetNo());
//    		   		       params.put(SendTemplateEmail.PARAM_USER_ADDRESS_STREET, uBase.getStreet());
//    		   		       params.put(SendTemplateEmail.PARAM_USER_ADDRESS_CITY, uBase.getCityName());
//    		   		       params.put(SendTemplateEmail.PARAM_USER_ADDRESS_ZIP_CODE, uBase.getZipCode());
//    		   		       params.put(SendTemplateEmail.PARAM_TRANSACTION_AMOUNT, amountTxt);
//    		   		       params.put(SendTemplateEmail.PARAM_PAYPAL_EMAIL, info.getEmails()[0].getEmail());
//    		   		       Date date = new Date();
//    		   		       params.put(SendTemplateEmail.PARAM_DATE_DMMYYYY, new SimpleDateFormat("dd.MM.yy").format(date));
//
//    		   		       if (null != uBase.getGender()) {
//	    		   				String genderTxt = uBase.getGenderForTemplateEnHeUse();
//    		   					genderTxt += ".en";
//	    		   				params.put(SendTemplateEmail.PARAM_GENDER_DESC_EN_OR_HE, properties.getProperty(genderTxt));
//    		   		       }
//
//    					   try {
//    						   emailProperties.put("body",getEmailBody(params));
//    					   } catch (Exception e) {
//    						   log.fatal("!!! ERROR >> getEmailBody: " + e.getMessage());
//    						   System.exit(1);
//    					   }
//
//    					   // send email for tracking
//						   String[] splitTo = properties.getProperty("succeed.email.to").split(";");
//						   for (int i = 0; i < splitTo.length; i++) {
//							    emailProperties.put("to", splitTo[i]);
//								sendSingleEmail(properties, emailProperties, langCode);
//						   }
//
//						   // send email to user
//    					   emailProperties.put("to", uBase.getEmail());
//    					   sendSingleEmail(properties,emailProperties, langCode);
//
//    		        } catch (Exception e) {
//    		        	log.error("Error, Can't send paypal withdrawal email! " + e);
//    				}
//                } else {
//                	log.info("trx failed, paypal withdrawal email not sent!");
//                }
//             }
//        } catch (Exception e) {
//            log.log(Level.ERROR, "Error while capturing paypal transactions.", e);
//        } finally {
//            try {
//                rs.close();
//            } catch (Exception e) {
//                log.log(Level.ERROR, "Can't close", e);
//            }
//            try {
//                pstmt.close();
//            } catch (Exception e) {
//                log.log(Level.ERROR, "Can't close", e);
//            }
//            try {
//                conn1.close();
//            } catch (Exception e) {
//                log.log(Level.ERROR, "Can't close", e);
//            }
//        }
//        report.append("</table>");
//    }
//
//
//	private static void setCityName(UserBase userBase) {
//
//		Connection con = null;
//		PreparedStatement ps = null;
//		ResultSet rs = null;
//		try {
//			con = getConnection();
//	        String sql =
//                "SELECT " +
//                    "u.city_id, u.city_name, c.name " +
//                "FROM " +
//                    "users u left join cities c on u.city_id=c.id " +
//                "WHERE " +
//                    "u.user_name = ?";
//
//
//			ps = con.prepareStatement(sql);
//			ps.setString(1, userBase.getUserName().toUpperCase());
//			rs = ps.executeQuery();
//			String cityName;
//			if (rs.next()) {
//				if (rs.getLong("city_id") > 0) {
//					cityName = rs.getString("name");
//				} else {
//					cityName = rs.getString("city_name");
//				}
//				userBase.setCityNameForJob(cityName);
//			}
//		} catch (Exception e) {
//            log.log(Level.ERROR, "Can't set cityName", e);
//		} finally {
//			DAOBase.closeResultSet(rs);
//			DAOBase.closeStatement(ps);
//			try {
//                con.close();
//            } catch (Exception e) {
//                log.log(Level.ERROR, "Can't close", e);
//            }
//
//		}
//	}
//
//	/**
//	 * This method create a fee transaction and sets it accoring to the clearing info
//	 * @param info - Clearing info instance
//	 * @return New fee transaction
//	 */
//	private static Transaction getFeeTransaction(ClearingInfo info) {
//		Transaction t = new Transaction();
//    	t.setUserId(info.getUserId());
//    	t.setTypeId(TransactionsManagerBase.TRANS_TYPE_HOMO_FEE);
//    	t.setTimeCreated(new Date());
//    	t.setStatusId(TransactionsManagerBase.TRANS_STATUS_SUCCEED);
//    	t.setWriterId(Writer.WRITER_ID_AUTO);
//    	t.setIp("IP NOT FOUND!");
//    	t.setTimeSettled(new Date());
//    	t.setProcessedWriterId(Writer.WRITER_ID_AUTO);
//    	t.setReferenceId(new BigDecimal(info.getTransactionId()));
//    	return t;
//	}
//
//   /**
//	 * Update transaction after send it via PayPal
//	 * @param info ClearingInfo instance of the current trx that captured
//	 * @param hasFee true in case we need to create fee transaction
//	 * @param feeAmount fee amount to take
//	 * @param utcOffset user utcOffset
//	 * @throws SQLException
//	 */
//	public static void updateTransaction(ClearingInfo info, boolean hasFee, long feeAmount, String utcOffset, boolean isSuccessful) throws SQLException {
//
//		Connection conn = null;
//		PreparedStatement ps = null;
//        try {
//        	conn = getConnection();
//            String updateSql;
//        	if (isSuccessful) { //handle success from provider
//              	//update transaction status to success and amount to the amount after fee (incase there was fee)
//        		updateSql =
//                    "UPDATE " +
//                        "transactions " +
//                    "SET " +
//                        "time_settled = sysdate, " +
//                        "status_id = ?, " +
//        				"comments = ? || '|' || ? || '|' || ? , " +
//        				"amount =  ?, " +
//               			"utc_offset_settled = ? " +
//               		"WHERE " +
//               			"id = ? ";
//
//				 ps = conn.prepareStatement(updateSql);
//				 ps.setInt(1, TransactionsManagerBase.TRANS_STATUS_SUCCEED);
//				 ps.setString(2, info.getResult());
//				 ps.setString(3, info.getMessage());
//				 ps.setString(4, info.getUserMessage());
//				 ps.setLong(5, info.getAmount());
//				 ps.setString(6, utcOffset);
//				 ps.setLong(7, info.getTransactionId());
//
//				 conn.setAutoCommit(false); //in order to support rollback once and update or an insert was not succesfull
//
//               	 //insert new fee transaction
//               	 if (hasFee) {
//                	Transaction feeTransaction = getFeeTransaction(info); //creating the fee transaction accoring to the orig transaction
//                	feeTransaction.setAmount(feeAmount);
//                	feeTransaction.setUtcOffsetCreated(utcOffset);
//                	feeTransaction.setUtcOffsetSettled(utcOffset);
//    				TransactionsDAOBase.insert(conn, feeTransaction);
//    				if (log.isInfoEnabled()) {
//    					String ls = System.getProperty("line.separator");
//    					log.info("fee transaction inserted: " + ls + feeTransaction.toString());
//    				}
//
//    				//update Transaction reference
//    				TransactionsDAOBase.updateRefId(conn, info.getTransactionId(), feeTransaction.getId());
//               	}
//        	} else { //handle failed from provider
//        		//update transaction status to failed
//        		updateSql =
//                    "UPDATE " +
//                        "transactions " +
//                    "SET " +
//                        "time_settled = sysdate, " +
//                        "status_id = ? , " +
//      	 				"comments = ? || '|' || ? || '|' || ? , " +
//      	 				"description = ? , " +
//        			    "utc_offset_settled = ? " +
//    			    "WHERE " +
//    			  		"id = ? ";
//
//       				 ps = conn.prepareStatement(updateSql);
//					 ps.setInt(1, TransactionsManagerBase.TRANS_STATUS_FAILED);
//					 ps.setString(2, info.getResult());
//					 ps.setString(3, info.getMessage());
//					 ps.setString(4, info.getUserMessage());
//					 ps.setString(5, ConstantsBase.ERROR_FAILED_CAPTURE);
//					 ps.setString(6, utcOffset);
//					 ps.setLong(7, info.getTransactionId());
//        	}
//        	ps.executeUpdate();
//        	conn.commit();
//        } catch (Exception e) {
//			log.log(Level.ERROR, "ERROR! Can't update transaction!. ", e);
//			try {
//				conn.rollback();
//			} catch (Exception er) {
//				log.log(Level.ERROR, "Can't rollBack! ", er);
//			}
//		} finally {
//			try {
//				conn.setAutoCommit(true);
//			} catch (SQLException e) {
//				log.error("Can't set back to autocommit.", e);
//			}
//			try {
//        		conn.close();
//        	} catch (Exception e) {
//        		log.log(Level.ERROR, "Can't close: conn2", e);
//        	}
//		}
//	}
//
//
//    /**
//     * Capture PayPal Withdrawal Job.
//     *
//     * @param args
//     */
//    public static void main(String[] args) {
//        if (null == args || args.length < 1) {
//            log.log(Level.FATAL, "Configuration file not specified, insert properties file name.");
//            return;
//        }
//        propFile = args[0];
//
//        if (log.isEnabledFor(Level.INFO)) {
//            log.log(Level.INFO, "Starting the job.");
//        }
//
//        captureWithdrawalTransactions();
//
//        String reportBody = "<html><body>" +
//            summery.getSummery() +
//            report.toString() +
//            "</body></html>";
//
//        if (log.isEnabledFor(Level.INFO)) {
//            log.log(Level.INFO, "Sending report email: " + reportBody);
//        }
//
//        // send email
//        Hashtable<String, String> server = new Hashtable<String, String>();
//        server.put("url", getPropertyByFile("email.url"));
//        server.put("auth", getPropertyByFile("email.auth"));
//        server.put("user", getPropertyByFile("email.user"));
//        server.put("pass", getPropertyByFile("email.pass"));
//        server.put("contenttype", getPropertyByFile("email.contenttype", "text/html; charset=UTF-8"));
//
//        Hashtable<String, String> email = new Hashtable<String, String>();
//        email.put("subject", getPropertyByFile("email.subject"));
//        email.put("to", getPropertyByFile("email.to"));
//        email.put("from", getPropertyByFile("email.from"));
//        email.put("body", reportBody);
//        sendEmail(server, email);
//
//        if (log.isEnabledFor(Level.INFO)) {
//            log.log(Level.INFO, "Job completed.");
//        }
//    }
//}