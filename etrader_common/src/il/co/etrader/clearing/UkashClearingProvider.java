//package il.co.etrader.clearing;
//
//import il.co.etrader.bl_managers.ApplicationDataBase;
//import il.co.etrader.util.CommonUtil;
//import il.co.etrader.util.ConstantsBase;
//
//import java.math.BigDecimal;
//import java.text.SimpleDateFormat;
//import java.util.Date;
//
//import javax.xml.parsers.DocumentBuilder;
//import javax.xml.parsers.DocumentBuilderFactory;
//
//import org.apache.log4j.Level;
//import org.apache.log4j.Logger;
//import org.w3c.dom.Document;
//import org.w3c.dom.Element;
//
//import com.anyoption.common.clearing.ClearingException;
//import com.anyoption.common.clearing.ClearingInfo;
//import com.anyoption.common.clearing.ClearingProvider;
//import com.anyoption.common.clearing.UkashInfo;
//import com.anyoption.common.util.ClearingUtil;
//
///**
// * Implement the communication with Ukash.
// *
// * @author Eran
// */
//public class UkashClearingProvider extends ClearingProvider {
//
//    private static final Logger log = Logger.getLogger(UkashClearingProvider.class);
//
//    private static final String REQUEST_PARAM_CASH_WITHDRAWAL = "1";
//    private static final String REQUEST_PARAM_SERVICE_NAME = "anyoption";
//    private static final int FNC_CAPTURE = 1;
//    private static final String URL_SUFFIX_REDEEM = "/Redeem?";
//
//    public void authorize(ClearingInfo info) throws ClearingException {
//    	throw new ClearingException("Unsupported operation.");
//    }
//
//    //We dont need authorization, if status is accepted we add the amount to user balance immediately
//    public void capture(ClearingInfo info) throws ClearingException {
//    	request((UkashInfo)info, FNC_CAPTURE);
//    }
//
//    public void enroll(ClearingInfo info) throws ClearingException {
//    	throw new ClearingException("Unsupported operation.");
//    }
//
//    public void purchase(ClearingInfo info) throws ClearingException {
//    	throw new ClearingException("Unsupported operation.");
//    }
//
//    public void withdraw(ClearingInfo info) throws ClearingException {
//    	throw new ClearingException("Unsupported operation.");
//    }
//
//    public void bookback(ClearingInfo info) throws ClearingException {
//    	throw new ClearingException("Unsupported operation.");
//    }
//
//    public boolean isSupport3DEnrollement() {
//        return false;
//    }
//
//    /**
//     * Process a request through this provider.
//     *
//     * @param info thransaction info
//     * @throws ClearingException
//     */
//    protected void request(UkashInfo info, int type) throws ClearingException {
//        if (log.isTraceEnabled()) {
//            log.trace(info.toString());
//        }
//        try {
//        	Document request = createRequest();
//        	String urlWithService = url;
//        	switch (type) {
//	            case FNC_CAPTURE:
//	            	addFunction(request,info);
//	            	urlWithService += URL_SUFFIX_REDEEM;
//	            	break;
//	            default:
//	                throw new ClearingException("Unkown request type");
//        	}
//
//        	String requestXML = ClearingUtil.transformDocumentToString(request);
//        	requestXML = "sRequest="+requestXML;
//
//          	String responseXML = ClearingUtil.executePOSTRequest(urlWithService, requestXML);
//            parseResponse(responseXML, info);
//
//	    } catch (Throwable t) {
//	        throw new ClearingException("Transaction failed.", t);
//	    }
//
//    }
//
//    /**
//     * Create request XML document.
//     *
//     * @return New Document with the Ukash XML structure.
//     */
//    private Document createRequest() {
//        Document request = null;
//        try {
//            DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
//            DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
//            request = docBuilder.newDocument();
//
//            Element root = request.createElement("soap:Envelope");
//            request.appendChild(root);
//            ClearingUtil.setAttribute(request, "", "xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
//            ClearingUtil.setAttribute(request, "", "xmlns:xsd", "http://www.w3.org/2001/XMLSchema");
//            ClearingUtil.setAttribute(request, "", "xmlns:soap", "http://schemas.xmlsoap.org/soap/envelope/");
//
//        } catch (Exception e) {
//            log.log(Level.ERROR, "Failed to create request document.", e);
//        }
//        return request;
//    }
//
//
//    /**
//     * Add "deposit" function details.
//     *
//     * @param request
//     * @param info
//     * @param fnc
//     */
//    protected void addFunction(Document request, UkashInfo info) {
//        SimpleDateFormat sd = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//        Date d = new Date();
//        String date = sd.format(d);
//        String amount = CommonUtil.displayAmountForInput(info.getAmount(), false, info.getCurrencyId() );
//        Element root = request.getDocumentElement();
//        String bp = "ASTechTransaction/";
//
//        ClearingUtil.setNodeValue(root, bp + "ASTechId/*", this.username);
//
//        ClearingUtil.setNodeValue(root, bp + "ClientName/*", info.getPayeeName());
//        ClearingUtil.setNodeValue(root, bp + "ClientEmail/*", info.getEmail());
//        ClearingUtil.setNodeValue(root, bp + "ClientPhone/*", info.getPayeePhone());
//        ClearingUtil.setNodeValue(root, bp + "ServiceName/*", REQUEST_PARAM_SERVICE_NAME);
//        ClearingUtil.setNodeValue(root, bp + "OrderId/*", String.valueOf(info.getTransactionId()));
//        //UKashTransaction data
//        ClearingUtil.setNodeValue(root, bp + "UKashTransaction/voucherNumber/*", info.getVoucherNumber());
//        ClearingUtil.setNodeValue(root, bp + "UKashTransaction/redemptionType/*", REQUEST_PARAM_CASH_WITHDRAWAL);
//        ClearingUtil.setNodeValue(root, bp + "UKashTransaction/merchDateTime/*", date);
//        ClearingUtil.setNodeValue(root, bp + "UKashTransaction/voucherValue/*", amount);
//        ClearingUtil.setNodeValue(root, bp + "UKashTransaction/baseCurr/*", info.getUserCurrencyCode());
//        ClearingUtil.setNodeValue(root, bp + "UKashTransaction/ticketValue/*", "0");
//    }
//
//    /**
//     * Parse the response.
//     *
//     * @param xml the XML to parse
//     * @param info the transaction info to fill the result values
//     * @param messageClass the message class request that called
//     * @throws Exception
//     */
//    protected static void parseResponse(String xml, UkashInfo info) throws Exception {
//
//    	xml = xml.replaceAll("&lt;", "<");
//    	xml = xml.replaceAll("&gt;", ">");
//
//        log.debug("Ukash xml received. " + System.getProperty("line.separator") + xml);
//
//        Document resp = ClearingUtil.parseXMLToDocument(xml);
//
//        Element ASTechTransaction = (Element) ClearingUtil.getNode(resp.getDocumentElement(), "ASTechTransaction/");
//        Element UKashTransaction = (Element) ClearingUtil.getNode(resp.getDocumentElement(), "UKashTransaction/");
//        String status = ClearingUtil.getElementValue(UKashTransaction, "txCode/*");
//        String txDescription = ClearingUtil.getElementValue(UKashTransaction, "txDescription/*");
//        String ukashTransactionId   = ClearingUtil.getElementValue(UKashTransaction, "ukashTransactionId/*");
//
//        info.setAuthNumber(ukashTransactionId);
//        info.setProviderTransactionId(ukashTransactionId);
//
//        if (null != status) {
//    		String voucherCurrency = ClearingUtil.getElementValue(ASTechTransaction, "voucherCurrency/*");
//    		info.setVoucherCurrency(voucherCurrency);
//    		info.setStatus(Integer.valueOf(status));
//            // the two valid values are "0" - Accepted and "1" or "99" to Decline or Failed
//            if (status.equals("0")) { // Accepted
//                info.setSuccessful(true);
//                info.setResult("1000");
//                info.setMessage("Permitted transaction.");
//                info.setUserMessage(txDescription);
//                String voucherValue = ClearingUtil.getElementValue(ASTechTransaction, "voucherValue/*");
//                String settleAmountClientBaseCurrency = ClearingUtil.getElementValue(ASTechTransaction, "settleAmountClientBaseCurrency/*");
//                String clientBaseCurrency = ClearingUtil.getElementValue(ASTechTransaction, "clientBaseCurrency/*");
//                long amount = 0;
//                long userCurrency = ApplicationDataBase.getCurrencyIdByCode(clientBaseCurrency);
//                if (userCurrency != ConstantsBase.CURRENCY_TRY_ID ){
//                	amount = CommonUtil.calcAmount(settleAmountClientBaseCurrency);
//                    info.setVoucherValue(voucherValue);
//                    info.setAmount(amount);
//                } else { // Currency conversion for user TRY currency
//                	long voucherCurrencyId = ApplicationDataBase.getCurrencyIdByCode(voucherCurrency);
//	                amount = CommonUtil.calcAmount(voucherValue);
//	                long convertedAmount = convertToUserCurrency(voucherCurrencyId, userCurrency, amount); // Converst amount to user currency
//	                info.setAmount(convertedAmount);
//                }
//
//                if (!clientBaseCurrency.equalsIgnoreCase(voucherCurrency)){
//                	info.setDifferentCurrencies(true);
//                }
//            } else if (status.equals("1") || status.equals("99")) { // Decline or Failed
//                info.setSuccessful(false);
//                // fill the error part
//                String errCode     = ClearingUtil.getElementValue(UKashTransaction, "errCode/*");
//                String errDescription  = ClearingUtil.getElementValue(UKashTransaction, "errDescription/*");
//
//                if (null != errCode){
//                	info.setResult(errCode);
//                } else {
//                	info.setResult("999");
//                }
//
//                if (null != errDescription) {
//                	info.setMessage(errDescription) ;
//                } else if(status.equals("1")){//Decline with no error code and no error description
//                	info.setMessage("Ukash Decline") ;
//                } else {
//                	info.setMessage("Unknown response") ;
//                }
//
//                info.setUserMessage("Transaction failed.");
//
//            }
//        } else {
//            // try if the response is not with the strange error structure that we've seen
//        	info.setSuccessful(false);
//        }
//    }
//
//    /**
//     * Convert amount to user currency
//     * @param currencyCode
//     * @param userCurrency
//     * @param amount
//     * @return
//     */
//    public static long convertToUserCurrency(long voucherCurrency, long userCurrency, long amount){
//    	if (userCurrency != voucherCurrency) {
//    		double amountToUSD = CommonUtil.convertToBaseAmount(amount , voucherCurrency, new Date()); //First convert to base amount (USD)
//    		double convertedAmount = (1/CommonUtil.convertToBaseAmount(1, userCurrency, new Date()) * amountToUSD);
//    		BigDecimal bg = new BigDecimal(convertedAmount);
//    		return bg.longValue();
//    	}
//    	return amount;
//    }
//
//	@Override
//	public void cancel(ClearingInfo info) throws ClearingException {
//		// TODO Auto-generated method stub
//		log.info("NOT IMPLEMENTED !!!");
//	}
//
//}