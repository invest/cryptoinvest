package goodByEtraderBonus;

import il.co.etrader.util.JobUtil;

import java.sql.Connection;
import java.sql.SQLException;

import org.apache.log4j.Logger;

import com.anyoption.common.managers.BaseBLManager;
import com.anyoption.common.util.ConstantsBase;

/**
 * @author liors
 *
 */
public class Program extends JobUtil {
	private static final Logger logger = Logger.getLogger(Program.class);
	private static final String LOG_PREFIX_MSG = "JOB; ";
	
	public static void main(String[] args) {
		logger.info(LOG_PREFIX_MSG + "Start Job");
		Connection connection = null;
		if(args.length != 1 || args == null) {
			logger.error(LOG_PREFIX_MSG + "ERROR! file not found. Proper Usage is: java program filename.");
            return;
        }
        propFile = args[0];
        String issueActionComment = getPropertyByFile("issue.action.comment");
        boolean isLiveUsers = getPropertyByFile("only.live.users").equals("1");
        String specificUserId = getPropertyByFile("user.id", ConstantsBase.EMPTY_STRING);
        
		try {
	    	connection = getConnection();
	    	connection.setAutoCommit(false);
	    	
	    	GoodByEtraderBonus.doJob(connection, issueActionComment, isLiveUsers, specificUserId);
		} catch (Exception e) { 		
			logger.error(LOG_PREFIX_MSG + "ERROR! ");
			try {
				connection.rollback();
			} catch (SQLException ie) {
				logger.error(LOG_PREFIX_MSG + "in GoodByEtraderBonus job, Can't rollback.", ie);
			}
		} finally {
			try {
				connection.setAutoCommit(true);
			} catch (SQLException e) {
				logger.error(LOG_PREFIX_MSG + "in GoodByEtraderBonus job, Can't rollback.", e);
			}
			BaseBLManager.closeConnection(connection);
		}
		
		logger.info(LOG_PREFIX_MSG + "End Job");
	}
}
