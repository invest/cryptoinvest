/**
 * 
 */
package com.anyoption.common.util.jsf;

import javax.faces.context.FacesContext;

import com.anyoption.common.util.PresetationPatternsBase;

/**
 * Holds the initialisation logic for the (managed) bean.
 * 
 * @author dzhamaldv
 *
 */
public class PresetationPatternsWeb extends PresetationPatternsBase {
	
	/**
	 * Constructor.
	 */
	public PresetationPatternsWeb() {
		setDatePattern(FacesContext.getCurrentInstance().getExternalContext().getInitParameter("presentation.date.pattern"));
		setTimeAndDatePattern(FacesContext.getCurrentInstance().getExternalContext().getInitParameter("presentation.time_date.pattern"));
		setTimePattern(FacesContext.getCurrentInstance().getExternalContext().getInitParameter("presentation.time.pattern"));
	}

}
