package calcalistUsersUpdate;

import il.co.etrader.bl_managers.TransactionsManagerBase;
import il.co.etrader.bl_vos.UserBase;
import il.co.etrader.bl_vos.Writer;
import il.co.etrader.dao_managers.BonusDAOBase;
import il.co.etrader.dao_managers.UsersDAOBase;
import il.co.etrader.util.ConstantsBase;
import il.co.etrader.util.JobUtil;

import java.io.IOException;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Calendar;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import com.anyoption.common.beans.Bonus;
import com.anyoption.common.beans.base.BonusUsers;
import com.anyoption.common.beans.base.Currency;
import com.anyoption.common.bl_vos.BonusCurrency;
import com.anyoption.common.daos.CurrenciesDAOBase;



public class ActionJob extends JobUtil {
	private static Logger log = Logger.getLogger(ActionJob.class);
	private static StringBuffer report = new StringBuffer();
	public static void main(String[] args) throws SQLException, IOException {

		Statement st = null;
		ResultSet rs = null;
		Connection con= null;

        if (log.isEnabledFor(Level.INFO)) {
            log.log(Level.INFO, "Starting the job.");
        }

        propFile = args[0];
        try {

        	con = getConnection("db.url","db.user","db.pass");

	        //doReport(con);
	        //All the users on Calcalist who dont have account on Etrader and balance lower than 11000000
			String sql=	"SELECT " +
								"u.user_name " +
							"FROM " +
								"users u " +
							"WHERE " +
								"not exists " +
								"(SELECT " +
									"1 " +
								"FROM " +
									"users u2 " +
								"WHERE " +
									"u2.user_name = SUBSTR(u.user_name,1 ,LENGTH(u.user_name)-3) ) " +
							"AND " +
								"u.user_name LIKE '%/_CG' escape '/'" +
							"AND " +
								"u.balance < 11000000";

		        st = con.createStatement();
				rs = st.executeQuery(sql);
				//Create an account for Calcalist users

		    	while(rs.next()){
		    		duplicateGameUserToReal(rs.getString("user_name"),con);
		    	}

		    	//Calculate bonus for Calcalist users
		    	sql="SELECT " +
		    			"id,user_name,balance " +
		    		"FROM " +
		    			"users " +
		    		"WHERE " +
		    			"user_name LIKE '%/_CG' escape '/'" +
		    		"AND " +
		    			"balance >= 11000000";

	        	long bonusAmount = 0 ;
	        	long calcalistUserId = 0;
	        	long etraderUserId = 0;
	        	String calcalistUserName=null;
	        	String etraderUserName=null;

		        st = con.createStatement();
				rs = st.executeQuery(sql);



		  	while(rs.next()){
		   		bonusAmount = 0;
		    		calcalistUserId=rs.getLong("id");
		    		calcalistUserName=rs.getString("user_name");
		    		bonusAmount = rs.getLong("balance");

		    		bonusAmount = (bonusAmount - 10000000);
		    		bonusAmount /= 100;

		    		if (bonusAmount > 100000){
		    		 bonusAmount = 100000;
		    		}

		    		//Update bonus for Calcalist users who have account on Etrader
		    		etraderUserName = calcalistUserName.substring(0,calcalistUserName.length()-3);
		    		etraderUserId = returnUserID(etraderUserName,con);
		    	    insertBonusToUser(con,etraderUserId,bonusAmount);

		    }


        }
	        catch (SQLException e) {
				e.printStackTrace();
			}
	        finally{
	        //	st.close();
	        //	rs.close();
	        	closeConnection(con);
	        }

	}


	    public static void duplicateGameUserToReal(String userName,Connection con)throws SQLException{

			String ls = System.getProperty("line.separator");
			//con = getConnection("calcalist.db.url","calcalist.db.user","calcalist.db.pass");

			UserBase user = new UserBase();
			UsersDAOBase.getByUserName(con, userName,user,true);

			if (user.getId() > 0) {  // user exists
					user.setUserName(user.getUserNameCalGame());
					user.setClassId(ConstantsBase.USER_CLASS_PRIVATE);
					user.setAcceptedTerms(false);
					user.setBalance(0);
					user.setTaxBalance(0);
					user.setTaxExemption(false);
					user.setReferenceId(new BigDecimal(user.getId()));
					Calendar startShowDate = Calendar.getInstance();
					startShowDate.set(Calendar.YEAR,2009);
					startShowDate.set(Calendar.DAY_OF_MONTH, 18);
					startShowDate.set(Calendar.MONTH, Calendar.OCTOBER);
					startShowDate.set(Calendar.HOUR_OF_DAY, 0);
					startShowDate.set(Calendar.MINUTE, 0);
					startShowDate.set(Calendar.SECOND, 0);
					startShowDate.set(Calendar.MILLISECOND, 0);
					user.setTimeLastLogin(startShowDate.getTime());
				try {
					UsersDAOBase.insert(con, user);
				} catch (Exception e2) {
						log.error("can't insert new user!! ", e2);
					}

//					try {
////						UsersManagerBase.sendMailTemplateFile(ConstantsBase.TEMPLATE_WELCOME_MAIL_ID, ConstantsBase.WRITER_ID_AUTO,user);
//						HashMap<Long, Template> templates = TemplatesDAO.getAllHashMap(con);
//						Template welcomeTemplate = templates.get(Long.parseLong("1"));
//						UsersManagerBase.sendJobMailTemplateFile(welcomeTemplate, ConstantsBase.WRITER_ID_AUTO, user, new BonusUsers(),con,properties);
//					} catch (Exception e2) {
//						log.error("can't send registration mail!! ", e2);
//					}

				log.info("duplicate calcalist game user finished successfully. " + ls +
						user.toString() );
			} else { // user not found
				log.debug("failed to deuplicate calcalist game user: user not exists!");
			}
	    }


	/*    public static void doReport(Connection con)throws SQLException, IOException{

	        report.append("<br><b>Calclist:</b></br><table border=\"1\">");
	        report.append("<tr><td></td></tr>");
	        report.append("<tr><td><b>userName</b></td><td><b>email</b></td><td><b>Etrader balance</b></td></tr>");

	        PreparedStatement ps = null;
	        ResultSet rs = null;
	        String userName= null;
	        String userEmail= null;
	        long balance = 0;

	        try {
	             con = getConnection("calcalist.db.url","calcalist.db.user","calcalist.db.pass");


			    String sql = "SELECT " +
			    				"user_name,email,balance "+
			    			 "FROM " +
			    			 	"users u " +
			    			 "WHERE u.user_name LIKE '%/_CG' escape '/' " +
			                    "AND " +
			                    "balance >= 11000000 ";

			    ps = con.prepareStatement(sql);
				rs = ps.executeQuery();
				while (rs.next()) {

					userName = rs.getString("user_name");
					userEmail = rs.getString("email");
					balance = rs.getLong("balance");

					report.append("<tr>");
	                report.append("<td>").append(userName).append("</td>");
	                report.append("<td>").append(userEmail).append("</td>");
	                report.append("<td>").append(balance).append("</td>");
	                report.append("</tr>");
				}


		        }
		        catch (Exception e) {
	        		log.log(Level.ERROR, "Error while Retrieving users from Db.", e);
		        }
	        finally {
	        	try {
	        		rs.close();
	        	} catch (Exception e) {
	        		log.log(Level.ERROR, "Can't close", e);
	        	}
	        	try {
	        		ps.close();
	        	} catch (Exception e) {
	        		log.log(Level.ERROR, "Can't close", e);
	        	}
	        	  report.append("</table>");
	        }


	        // build the report
	        String reportBody = "<html><body>" +
	        						report.toString() +
	        					"</body></html>";

		    // create file that contain the report.
		    // the file name include the tax period and the year
		    Writer output = null;
		    File file = new File("Calcalist.html");
		    output = new BufferedWriter( new FileWriter(file) );
		    output.write(reportBody);
		    output.close();

		    //  send email
		    Hashtable<String,String> server = new Hashtable<String,String>();
		    server.put("url", getPropertyByFile("calcalist.email.url"));
		    server.put("auth", getPropertyByFile("calcalist.email.auth"));
		    server.put("user", getPropertyByFile("calcalist.email.user"));
		    server.put("pass", getPropertyByFile("calcalist.email.pass"));
		    server.put("contenttype", getPropertyByFile("calcalist.email.contenttype", "text/html; charset=UTF-8"));

		    Hashtable<String,String> email = new Hashtable<String,String>();
		    email.put("subject", getPropertyByFile("calcalist.email.subject"));
		    email.put("to", getPropertyByFile("calcalist.email.to"));
		    email.put("from", getPropertyByFile("calcalist.email.from"));
		    email.put("body", reportBody);
		    sendEmail(server, email);

		    if (log.isEnabledFor(Level.INFO)) {
		        log.log(Level.INFO, "Job completed.");
		    }


		    }

*/

		private static boolean insertBonusToUser(Connection con, long userId,long bonusAmount){
			 boolean res = false;
			 BonusUsers bonusUser = new BonusUsers();

			 bonusUser.setBonusId(103);
			 bonusUser.setUserId(userId);
			 bonusUser.setWriterId(Writer.WRITER_ID_AUTO);

			 UserBase user = new UserBase();
		   	 try {
		   		  UsersDAOBase.getByUserId(con, userId, user,true);
		     } catch (SQLException e) {
		   		  log.error("Error: getUserById " + userId + " Failed." + e);
		   		  return false;
		   	 }

		     if (null != user && user.getId() > 0){
	    		 try {
	     			Currency userCurrency = CurrenciesDAOBase.getById(con, user.getCurrencyId().intValue());
	     			user.setCurrency(userCurrency);
	     			bonusUser.setCurrency(userCurrency);

	     			res = insertBonusUser(bonusUser, user, Writer.WRITER_ID_AUTO, con,bonusAmount);

	 	    	 } catch (SQLException e1) {
	 	    		log.error("Error: Granting bonus to user " + userId + " Failed." + e1);
	 	    		return false;
	 	    	 }
		     }
		     if (res){
			   log.info("bonus inserted for user " + userId);
		     }else {
		       log.error("Error: Granting bonus to user " + userId + " Failed.");
		     }

		     return res;
		}


		 /**
	     * Insert bonus to user
	     * @param bUser BonusUser instnace
	     * @param currencyId user currency
	     * @return
	     * @throws SQLException
	     */
	    public static boolean insertBonusUser(BonusUsers bUser, UserBase user,long writerId,Connection con,long bonusAmount) throws SQLException {
	    	boolean res = false;

	        	//  get Bonus data for insert
	        	long bonusId = bUser.getBonusId();
				Bonus b = getBonusById(con,bonusId);
				BonusCurrency bc = BonusDAOBase.getBonusCurrency(con,bonusId, user.getCurrencyId());
				bc.setBonusAmount(bonusAmount);

				try {
					con.setAutoCommit(false);

					if (null != b && null != bc) {
						res = BonusDAOBase.insertBonusUser(con, bUser, b, bc, 0, true);
					}

		        	if (res) {
//		        		if (b.isHasSteps()){
//		        			ArrayList<BonusCurrency> bonusSteps = BonusDAOBase.getBonusSteps(con, bonusId, user.getCurrencyId());
//
//		        			res = BonusDAOBase.insertBonusUserSteps(con, bonusSteps, bUser.getId());
//		        		}

		        		if (bUser.getTypeId() == ConstantsBase.BONUS_TYPE_INSTANT_AMOUNT) {
		        			res = TransactionsManagerBase.insertBonusDeposit(user, bUser.getId(),writerId,bUser.getBonusAmount(),null,con, 0L);
		        		}
		        	}
		        	con.commit();
	        	} catch (Exception e) {
	        		log.error("Exception Insert new bonus to user! ", e);
	    			try {
	    				con.rollback();
	    			} catch (Throwable it) {
	    				log.error("Can't rollback.", it);
	    			}
	                SQLException sqle = new SQLException();
	                sqle.initCause(e);
	                throw sqle;
	        	} finally {
	                try {
	                    con.setAutoCommit(true);
	                } catch (Exception e) {
	                	log.error("Can't set back to autocommit.", e);
	                }
	    		}
	        return res;
	    }


	    public static Bonus getBonusById(Connection con, long bonusId) throws SQLException{
	        return BonusDAOBase.getBonusById(con, bonusId);
	    }

		public static long returnUserID(String userName,Connection con){

			long id=0;
			PreparedStatement ps = null;
			ResultSet rs = null;

			try{
	    	String sql="SELECT " +
							" id " +
						"FROM " +
							" users " +
						"WHERE  " +
						    " user_name like '" + userName +"'"  ;

	    	ps = con.prepareStatement(sql);
			rs = ps.executeQuery(sql);
			if (rs.next()){
				id = rs.getLong("id");
			}
			}
		    catch (Exception e) {
				log.log(Level.ERROR, "Error while Retrieving user from Db.", e);
		    }
		    finally {
		    	try {

		    		rs.close();
		    	} catch (Exception e) {
		    		log.log(Level.ERROR, "Can't close", e);
		    	}
		    	try {
		    		ps.close();
		    	} catch (Exception e) {
		    		log.log(Level.ERROR, "Can't close", e);
		    	}
		    }
		     return id;
		}

/*	    public static long calcBonus(long userid,Connection con){

			Statement st = null;
			ResultSet rs = null;
			long amount = 0;

			try{

			    String sql ="SELECT " +
			    				"sum(i.win-i.lose) win_lose " +
			    			"FROM " +
			    				"users u, investments i " +
			    			"WHERE " +
			    					"u.id= " + userid +
			    				"AND " +
			    					"u.id = i.user_id " +
			    				"AND " +
			    					"i.is_settled=1 " +
			    				"AND " +
			    					"i.is_canceled = 0 " +
			    				"AND " +
			    					"u.skin_id = 1 ";

		        st = con.createStatement();
				rs = st.executeQuery(sql);
			}
		    catch (Exception e) {
        		log.log(Level.ERROR, "Error while Retrieving users from Db.", e);
	        }
	        finally {
	        	try {

	        		rs.close();
	        	} catch (Exception e) {
	        		log.log(Level.ERROR, "Can't close", e);
	        	}
	        	try {
	        		st.close();
	        	} catch (Exception e) {
	        		log.log(Level.ERROR, "Can't close", e);
	        	}
	        }

	        return amount;

			    }

*/

	    /**
	     * Close db connection.
	     *
	     * @param conn the db conn to close
	     */
	    public static void closeConnection(Connection conn) {
	        if (null != conn) {
	            try {
	                conn.close();
	            } catch (SQLException sqle) {
	                log.error("Can't close connection.", sqle);
	            }
	        }
	    }

}

