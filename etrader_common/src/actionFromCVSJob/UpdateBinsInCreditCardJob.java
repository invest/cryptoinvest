package actionFromCVSJob;

import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import org.bouncycastle.crypto.CryptoException;

import com.anyoption.common.beans.CreditCard;
import com.anyoption.common.util.AESUtil;

import il.co.etrader.util.JobUtil;

public class UpdateBinsInCreditCardJob extends JobUtil {

	/**
	 * @author oshikl
	 * @param args
	 * @throws SQLException 
	 * @throws CryptoException 
	 * @throws NumberFormatException 
	 * @throws NoSuchPaddingException 
	 * @throws NoSuchAlgorithmException 
	 * @throws BadPaddingException 
	 * @throws IllegalBlockSizeException 
	 * @throws InvalidKeyException 
	 * @throws InvalidAlgorithmParameterException 
	 */
	public static void main(String[] args)	throws SQLException, NumberFormatException, CryptoException, InvalidKeyException,
											IllegalBlockSizeException, BadPaddingException, NoSuchAlgorithmException,
											NoSuchPaddingException, InvalidAlgorithmParameterException {
		
		propFile = args[0];
		// TODO Auto-generated method stub
		PreparedStatement ps = null;
		PreparedStatement ps2 = null;
		ResultSet rs = null;
		Connection conn = null;
		ArrayList<CreditCard> list =  new ArrayList<CreditCard>();
		String sqlUpdate = " UPDATE credit_cards cc set cc.bin = ? WHERE cc.id = ? ";
		String selectSql = " SELECT cc.id, cc.cc_number " +
						   " FROM credit_cards cc" +
						   " WHERE cc.bin is null ";
		try {
			conn = getConnection();
			ps = conn.prepareStatement(selectSql);
			rs = ps.executeQuery();
			while (rs.next()){
				CreditCard cc = new CreditCard();
				cc.setId(rs.getLong("id"));
				cc.setCcNumber(Long.valueOf(AESUtil.decrypt(rs.getString("cc_number"))));				
				list.add(cc);				
				}
			} finally {
				conn.close();
			}
		try{
			conn = getConnection();
			ps = conn.prepareStatement(sqlUpdate);
			
			final int batchSize = 1000;
			int count = 0;
			
			for(CreditCard cc : list){
				if(cc.getCcNumber() >  99999){
					ps.setLong(1, Long.parseLong(cc.getBinStr()));
					ps.setLong(2, cc.getId());
					ps.addBatch();
					
					
					if(++count % batchSize == 0) {
				        ps.executeBatch();
				    }
				} else {
					System.out.println(cc.getId());
				}
			}	
			ps.executeBatch(); 
			ps.close();				
		} finally {
			conn.close();
			System.out.println("Job Finished");
		}		
		}
	}
