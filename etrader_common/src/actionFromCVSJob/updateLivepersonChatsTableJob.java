package actionFromCVSJob;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import org.bouncycastle.crypto.CryptoException;

import il.co.etrader.bl_vos.Chat;
import il.co.etrader.util.CommonUtil;
import il.co.etrader.util.JobUtil;

public class updateLivepersonChatsTableJob extends JobUtil {

	/**
	 * @author oshikl
	 * @param args
	 * @throws SQLException 
	 * @throws CryptoException 
	 * @throws NumberFormatException 
	 */
	public static void main(String[] args) throws SQLException, NumberFormatException, CryptoException {
		
		propFile = args[0];
		// TODO Auto-generated method stub
		PreparedStatement ps = null;
		PreparedStatement ps2 = null;
		ResultSet rs = null;
		Connection conn = null;
		int index = 0 ;
		ArrayList<Chat> list =  new ArrayList<Chat>();
		String sqlUpdate = " UPDATE liveperson_chats lp set lp.non_html_chat = ?  WHERE lp.id = ? ";
		String selectSql = " select  lp.* " +
							" from liveperson_chats lp " +
							" where lp.non_html_chat is null " ;
				
		try {
			conn = getConnection();
			ps = conn.prepareStatement(selectSql);
			rs = ps.executeQuery();
			while (rs.next()){
				Chat vo = new Chat();
				vo.setId(rs.getLong("id"));
				vo.setNonHtmlChat(convertToNonHtmlChat(rs.getString("chat")));
				list.add(vo);	
				System.out.println(index++);
				}
			} finally {
				conn.close();
			}
		
//		for(Chat chat: list){
//			chat.setNonHtmlChat(convertToNonHtmlChat(chat.getChat()));
//		}	
		
		try{
			conn = getConnection();
			ps = conn.prepareStatement(sqlUpdate);
			
			final int batchSize = 1000;
			int count = 0;
			
			for(Chat vo : list){
				if(!CommonUtil.isParameterEmptyOrNull(vo.getNonHtmlChat())){
					ps.setString(1, vo.getNonHtmlChat());
					ps.setLong(2, vo.getId());
					ps.addBatch();					
					
					if(++count % batchSize == 0) {
				        ps.executeBatch();
				    }
					System.out.println("update:" + count);
				}
			}	
			ps.executeBatch(); 
			ps.close();				
		} finally {
			conn.close();
			System.out.println("Job Finished");
		}		
		}

	private static String convertToNonHtmlChat(String chat) {
		// TODO Auto-generated method stub
		String nonHtmlChat = "";
		while(!CommonUtil.isParameterEmptyOrNull(chat)){
			if(chat.startsWith("<")){
				chat = chat.substring(chat.indexOf(">") + 1);
			} else if (chat.startsWith("&")){
				chat = chat.substring(chat.indexOf(">") + 1);
			} else {
				nonHtmlChat += chat.substring(0, chat.indexOf("<")) + "\r\n";
				chat = chat.substring(chat.indexOf(">") + 1);
			}
		}
		
		return nonHtmlChat;
	}
}
