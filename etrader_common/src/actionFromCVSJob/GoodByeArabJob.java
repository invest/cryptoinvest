package actionFromCVSJob;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import com.anyoption.common.daos.LanguagesDAOBase;

import il.co.etrader.bl_managers.UsersManagerBase;
import il.co.etrader.bl_vos.Template;
import il.co.etrader.bl_vos.UserBase;
import il.co.etrader.bl_vos.Writer;
import il.co.etrader.dao_managers.SkinsDAOBase;
import il.co.etrader.dao_managers.TemplatesDAO;
import il.co.etrader.dao_managers.UsersDAOBase;
import il.co.etrader.util.CommonUtil;
import il.co.etrader.util.JobUtil;

public class GoodByeArabJob extends JobUtil {
	private static Logger log = Logger.getLogger(GoodByeArabJob.class);

	public static String newLine = "\n";
	public static String TEMPLATE = "closing_AR_skin.html";

	private static HashMap<Long, String> supportPhone = new HashMap<Long, String>();

	public static void main(String[] args) {

        if (log.isEnabledFor(Level.INFO)) {
            log.log(Level.INFO, "Starting the job.");
        }
        propFile = args[0];
        Connection con = null;
		try {
	    	con = getConnection();
            createSupportPhone(con);

	    	long skinId = Long.valueOf(properties.getProperty("skin.id"));
	    	long numberOfRow = Long.valueOf(properties.getProperty("numbers.of.row"));
	    	long totalUsers = Long.valueOf(properties.getProperty("numbers.of.total.users", "-1"));
	    	String endDate = properties.getProperty("endDate");
	    	HashMap<String,String> parameters = new HashMap<String, String>();
			parameters.put("dateDDMMYYYY", endDate);

			long count = 0; //how much user in total to run over

	    	long langId = SkinsDAOBase.getById(con, (int)skinId).getDefaultLanguageId();
	    	String langCode = LanguagesDAOBase.getCodeById(con, langId);
	    	log.info("skinId: " + skinId + " , langId: " + langId + ", langCode: " + langCode);
	    	Template t = TemplatesDAO.get(con, Template.CLOSE_ARAB_SKIN_ID);
	    	while (true) {
	    		//get 50 users each loop
	    		ArrayList<UserBase> users = UsersDAOBase.getBaseUsersBySkinIdAndNoIssue(con, skinId,
	    				Template.CLOSE_ARAB_SKIN_ID, properties.getProperty("closearskin.subject.ar"), numberOfRow,
	    				langId);
	    		log.info(users.size() + " were loaded from db count = " + count);


	    		if (users == null || users.size() == 0 || (totalUsers != -1 && count >= totalUsers)) {
	    			//end loop
	    			break;
	    		}

		    	for (UserBase user:users) {
		    		log.info("user id " + user.getId());
		    		//SUPPORT_PHONE = ;
		    		String supportPhoneTxt = supportPhone.get(user.getCountryId());
		    		if (supportPhoneTxt == null || supportPhoneTxt.length() == 0) {
		    			supportPhoneTxt = "+44-2080997262"; // default
		    		}
		//       		 put all users params
//		    		HashMap<String, String> params = new HashMap<String, String>();
//		    		params.put("dateDDMMYYYY", endDate);
//		    		params.put("userFirstName", user.getFirstName());
//		    		params.put("supportPhone", supportPhoneTxt);



//		            if (user.getContactByEmail()) {
//		            	sendSingleEmail(con, properties, t.getFileName(), params, t.getSubject(), null,
//		                        true, false, 0, user, null, t.getId());
//		            }

					// add to user MailBox
//		            log.info("b4 send user mail box " + !CommonUtil.isHebrewSkin(user.getSkinId())  + " skin id " + user.getSkinId());
					if (!CommonUtil.isHebrewSkin(user.getSkinId())) {
//						log.info("add to user mail box");
						UsersManagerBase.SendToMailBox(user, t, Writer.WRITER_ID_AUTO, user.getLanguageId(), null, parameters, con, 0);
					}
//					log.info("after send user mail box ");
					count++;
		    	}
	    	}

		} catch (Exception e) {
            log.info("cant run good bye arab job", e);
		} finally {
            if (null != con) {
                try {
                    con.close();
                } catch (SQLException e) {
                    log.info("cant close db con", e);
                }
            }
        }


        if (log.isEnabledFor(Level.INFO)) {
	        log.log(Level.INFO, "Job completed.");
	    }
    }

    private static void createSupportPhone(Connection con) {
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            String sql =" SELECT " +
                            " * " +
                        " FROM " +
                            " countries ";
            pstmt = con.prepareStatement(sql);
            rs = pstmt.executeQuery();
            while(rs.next()) {
                supportPhone.put(new Long(rs.getLong("id")), rs.getString("SUPPORT_PHONE"));
            }
        } catch (Exception e) {
            log.log(Level.FATAL,"Can't get countries " + e);
        } finally {
            try {
                rs.close();
            } catch (Exception e) {
                log.log(Level.ERROR,"Can't close",e);
            }

            try {
                pstmt.close();
            } catch (Exception e) {
                log.log(Level.ERROR,"Can't close",e);
            }
        }
    }
}

