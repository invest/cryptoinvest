package email.job;

import il.co.etrader.bl_vos.UserBase;

/**
 * @author liors
 *
 */
class User extends UserBase {
	private static final long serialVersionUID = 3840666604605583566L;
	private String passwordRevert;
		
	public String getPasswordRevert() {
		return passwordRevert;
	}
	
	public void setPasswordRevert(String passwordRevert) {
		this.passwordRevert = passwordRevert;
	}     
}
