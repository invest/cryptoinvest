package email.job;

import il.co.etrader.util.JobUtil;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Properties;

import mailTaxJob.Utils;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.velocity.Template;
import org.apache.velocity.app.VelocityEngine;

import com.anyoption.common.beans.base.Platform;
import com.anyoption.common.beans.base.Skin;
import com.anyoption.common.managers.BaseBLManager;

/**
 * @author liors
 *
 */
class Util extends JobUtil {	
	public static final String LOG_PREFIX_MSG = "email Job; ";
	private static Logger log = Logger.getLogger(Util.class);
//	private static Template emailTemplate;
	private static Hashtable<String, String> serverProperties;
	private static Hashtable<String, String> emailProperties;
	private static HashMap<Long, String> supportPhone = new HashMap<Long, String>();
	private static HashMap<Long, String> supportEmails = new HashMap<Long, String>();
	
	protected static ArrayList<User> getUsers(Connection connection, String sqlInternal) {
		//TODO implement
		log.info(LOG_PREFIX_MSG + " Going to get users");
		PreparedStatement ps = null;
    	ResultSet rs = null;
    	ArrayList<User> users = new ArrayList<User>();
    	try {
    		String sql = " SELECT " +
    					 "	users_list.user_id, " +
    					 "	users_list.email, " +
    					 "	users_list.skin_id, " +
    					 "	users_list.country_id, " +
    					 "	users_list.first_name, " +
    					 "	users_list.platform_id, " +
    					 "	users_list.password_revert " +	
    					 " FROM " +
 						" ( " + sqlInternal + " ) users_list ";

    		log.info(LOG_PREFIX_MSG + " SQL:" + sql);
    		ps = connection.prepareStatement(sql);   
			rs = ps.executeQuery();
    		while (rs.next()) {
    			User user = new User();
    			user.setId(rs.getInt("user_id"));
    			user.setEmail(rs.getString("email"));
    			user.setSkinId(rs.getLong("skin_id"));
    			user.setFirstName(rs.getString("first_name"));
    			user.setPasswordRevert(rs.getString("password_revert"));
    			user.setPlatformId(rs.getInt("platform_id"));  			
    			users.add(user);
    		}
    	} catch (Exception e) {
			log.error(LOG_PREFIX_MSG + " ERROR! getUsers", e);
		} finally {
			closeResultSet(rs);
        	closeStatement(ps);
		}	
    	log.info(LOG_PREFIX_MSG + " Finish getting users froom DB. list size:" + users.size());
    	return users;
	}
	
	protected static void sendEmails(ArrayList<User> users, String templateName) throws Exception {
		//TODO implement
		log.info(LOG_PREFIX_MSG + "Going to send email to users");
		HashMap<String, Object> params = new HashMap<String, Object>();
		try {
			initEmail(templateName);
			for (User user : users){					
        		String supportPhoneTxt = supportPhone.get(user.getCountryId());
        		if (supportPhoneTxt == null || supportPhoneTxt.length() == 0) {
        			supportPhoneTxt = "+44-2080997262"; // default
        		}
        		if (user.getSkinId() == Skin.SKIN_ETRADER) {
        			supportPhoneTxt = "0772282222"; // default
        		}
        		String supportEmail = supportEmails.get(user.getSkinId());
        		if (user.getPlatformId() == Platform.COPYOP) {
        			supportEmail = "support@copyop.com";
        		}
				params.put("userFirstName", user.getFirstName());
				params.put("userNewPassword", user.getPasswordRevert());
				params.put("supportPhoneNumber", supportPhoneTxt);
				params.put("supportEmail", supportEmail);		
				params.put("userEmail", user.getEmail());
				// email properties
				emailProperties.put("from", supportEmail); 
				emailProperties.put("body", getEmailBody(params));
				String userEmail = user.getEmail();
				if (!JobUtil.getPropertyByFile("send.to").equalsIgnoreCase("Live")){
					userEmail = JobUtil.getPropertyByFile("send.to");
				}  					
				emailProperties.put("to",userEmail);
				//sendEmail(serverProperties, emailProperties);
				Utils.sendEmail(serverProperties,emailProperties);
				emailProperties.remove("to");
				log.info(LOG_PREFIX_MSG + " Succefull sending user reset password to " + user.getEmail() + " ; " +  user.getId());
				insertDBRecord(user);
			}
		} catch (Exception e) {
			log.error("ERROR INIT email: ", e);
		}
	}
	
	private static void initEmail(String name) throws Exception {
		//TODO implement	
		// get the emailTemplate
		mailTemplate = getEmailTemplate(name);

		// set the server properties
		serverProperties = new Hashtable<String, String>();
		serverProperties = new Hashtable<String, String>();
		serverProperties.put("url",  getPropertyByFile("email.url"));
		serverProperties.put("user", getPropertyByFile("email.user"));
		serverProperties.put("pass", getPropertyByFile("email.pass"));
		serverProperties.put("auth", getPropertyByFile("email.auth"));
		// Set the email properties
		emailProperties = new Hashtable<String, String>();
		emailProperties.put("subject",getPropertyByFile("email.subject")); 
		//emailProperties.put("from",Utils.getProperty("")); //TODO
	}
	
	private static Template getEmailTemplate(String fileName) throws Exception {
		try {
			String templatePath = getPropertyByFile("template.path");
			Properties props = new Properties();			
			props.setProperty("file.resource.loader.path", templatePath);
			props.setProperty("resource.loader", "file");
			props.setProperty("file.resource.loader.class", "org.apache.velocity.runtime.resource.loader.FileResourceLoader");
			props.setProperty("file.resource.loader.cache", "true");
			VelocityEngine v = new VelocityEngine();
			v.init(props);
			return v.getTemplate(fileName, "UTF-8");
		} catch (Exception ex) {
			log.fatal("ERROR! Cannot find template : " + ex.getMessage());
			throw ex;
		}
	}
	
	public static void createSupportPhone(Connection con) { 
		PreparedStatement pstmt = null;
		ResultSet rs = null;		
		try {			
			String sql =" SELECT " +
							" * " +
						" FROM " +
							" countries ";
			pstmt = con.prepareStatement(sql);
			rs = pstmt.executeQuery();
			while(rs.next()) {
				supportPhone.put(new Long(rs.getLong("id")), rs.getString("SUPPORT_PHONE"));
			}
		} catch (Exception e) {
			log.log(Level.FATAL,"Can't get countries " + e);
		} finally {
			try {
				rs.close();
			} catch (Exception e) {
				log.log(Level.ERROR,"Can't close",e);
			}

			try {
				pstmt.close();
			} catch (Exception e) {
				log.log(Level.ERROR,"Can't close",e);
			}			
		}		
	}
	
	public static void createSupportEmail(Connection con) { 
		PreparedStatement pstmt = null;
		ResultSet rs = null;		
		try {			
			String sql =" SELECT " +
							" * " +
						" FROM " +
							" skins ";
			pstmt = con.prepareStatement(sql);
			rs = pstmt.executeQuery();
			while(rs.next()) {
				supportEmails.put(new Long(rs.getLong("id")), rs.getString("SUPPORT_EMAIL"));
			}
		} catch (Exception e) {
			log.log(Level.FATAL,"Can't get skins " + e);
		} finally {
			try {
				rs.close();
			} catch (Exception e) {
				log.log(Level.ERROR,"Can't close",e);
			}

			try {
				pstmt.close();
			} catch (Exception e) {
				log.log(Level.ERROR,"Can't close",e);
			}			
		}		
	}
	
    public static String fileToString(String fileName) {
		try {
			BufferedReader br = new BufferedReader(new FileReader(fileName));
	 		String row = new String();
	 		StringBuffer contents = new StringBuffer();
	 		// repeat until all lines have been read
	 		while ((row = br.readLine()) != null) {
		        contents.append(row);
		        contents.append(" ");
	 		}
	 		return contents.toString().substring(0,contents.toString().length()-1);
	 	} catch(IOException ioe) {
	 		log.fatal("Error reading users CVS file !!! Job aborted");
	    } finally {
	    	
	    }
	  return null;	     
	}
    
    public static void insertDBRecord(User user) {
		PreparedStatement ps = null;
		Connection conn = null;
    	try {
    		conn = getConnection();
    		String sql="  INSERT INTO USERS_RESET_PASS (user_id, time_created) " +
    					" VALUES " +
    					" (?, sysdate) ";     			
    		ps = conn.prepareStatement(sql);
    		ps.setLong(1, user.getId());
    		ps.executeUpdate();
    		conn.close();
    	} catch(SQLException se){
	    	log.fatal("Exception during performing action :", se);	    	
	    	se.printStackTrace();
	    } catch (Exception e) {
	        log.fatal("Exception during performing action :", e);
	        e.printStackTrace();
	    } finally {
			closeStatement(ps);
			BaseBLManager.closeConnection(conn);			
		}    	
    }
    	
}
