create or replace trigger trg_bu_files
  before update on files
  referencing old as old new as new
  for each row
declare
  cursor c_regulated is
    select s.is_regulated
    from   users u, skins s
    where  u.skin_id = s.id
    and    u.id = :new.user_id;

  r_regulated number := 0;
  r_done      number := 0;
begin

  open c_regulated;
  fetch c_regulated
    into r_regulated;
  close c_regulated;

  if r_regulated = 1
     and :new.is_approved_control = 1
     and :new.is_approved_control != :old.is_approved_control
  then
    r_done := 1;
  end if;

  if r_regulated = 0
     and :new.is_approved = 1
     and :new.is_approved != :old.is_approved
  then
    r_done := 1;
  end if;

  if r_done = 1
  then
    :new.file_status_id := 3;
  end if;
end;
/
