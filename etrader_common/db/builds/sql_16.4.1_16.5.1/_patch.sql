
@tables_data/core-3027.sql
commit;

------------------------------------------------------------------------
-- CORE-2192 BID, ASK, LAST Parameters upon Hover
-- Tony
-----------------------------------------------------------------------
ALTER TABLE MARKET_RATES ADD (LAST NUMBER, ASK NUMBER, BID NUMBER);

COMMENT ON COLUMN MARKET_RATES.LAST IS 'Reuters LAST value from which this rate was calculated';
COMMENT ON COLUMN MARKET_RATES.ASK IS 'Reuters ASK value from which this rate was calculated';
COMMENT ON COLUMN MARKET_RATES.BID IS 'Reuters BID value from which this rate was calculated';
------------------------------------------------------------------------
-- CORE-2192 BID, ASK, LAST Parameters upon Hover
-- Tony
-----------------------------------------------------------------------


---- transactions screen (BAC-1892); Victor
-- re-create the column to not use NVARCHAR2
alter table charge_backs rename column description to description_o;
alter table charge_backs add (description varchar2(500 char));

-- null the old value not to introduce chained rows in the table!
update charge_backs
set    description = description_o, description_o = null
where  description is null
and    description_o is not null;

commit;

create global temporary table gt_transactions
on commit delete rows
as
select * from transactions where 1=0;

begin
  dbms_stats.delete_table_stats(user, 'GT_TRANSACTIONS', force => true);
  dbms_stats.lock_table_stats(user, 'GT_TRANSACTIONS');
end;
/

alter table charge_backs drop (description_o);

declare
  l_exp varchar2(1000);
begin
  begin
    select column_expression
    into   l_exp
    from   user_ind_expressions
    where  index_name = 'IDX_TRNS_LONGTRNS';
  exception
    when no_data_found then
      null;
  end;

  if l_exp <> 'CASE  WHEN "TIME_SETTLED"-"TIME_CREATED">20 THEN "TIME_SETTLED" END '
     or l_exp is null
  then
    pkg_release.ddl_helper('drop index idx_trns_longtrns');
    pkg_release.ddl_helper('create index idx_trns_longtrns on transactions
 (case
    when time_settled - time_created > 20 then
      time_settled
  end) online');
  end if;
end;
/

begin
  pkg_release.ddl_helper('create index idx_invs_longsettled on investments (case when time_settled - time_created > 20 then time_settled end) online', -955);
end;
/
begin
  pkg_release.ddl_helper('create index idx_invs_longcanceled on investments (case when time_canceled - time_created > 20 then time_canceled end) online', -955);
end;
/

begin
  pkg_release.ddl_helper('create index idx_duis_dev_uniq_id on device_unique_ids_skin (device_unique_id) online', -955);
end;
/
    
@packages/pkg_transactions_backend.pck
---- END; transactions screen (BAC-1892); Victor


---- fixed writers creation
@packages/pkg_writers.pck


---- BAC-1977: Keesing Authentication; Victor
alter table files add (time_uploaded date);
comment on column files.time_uploaded is 'moment when the file was uploaded in the system (from writer or user)';

alter table file_types modify (is_credit_card_file not null);
alter table file_types add constraint uk_file_types unique (name);
alter table file_types add constraint cc_file_types_name check (name = lower(name));

alter table file_types add (is_keesing_type number default 0 not null);
update file_types set is_keesing_type = 1 where id in (1, 2, 18, 21);
commit;

alter table files add (ks_status_id number, ks_status_date date);
alter table files_history add (ks_status_id number, ks_status_date date, time_uploaded date);

comment on column files.ks_status_id is 'Status of communication with keesing system';
comment on column files.ks_status_date is 'When the current ks_status_id was set';

create index idx_files_ksstatus on files (case when ks_status_id <> 3 then ks_status_id end) compress;

create table ks_statuses
(
   id          number        not null
  ,description varchar2(500) not null
  ,constraint pk_ks_statuses primary key (id)
);

alter table files add constraint fk_files_ksta foreign key (ks_status_id) references ks_statuses(id);

insert into ks_statuses (id, description) values (1, 'Newly inserted documents that is pending to be send to the keesing system');
insert into ks_statuses (id, description) values (2, 'Document is being processed by the task communicating with the keesing system');
insert into ks_statuses (id, description) values (3, 'Document has been processed by the task communicating with the keesing system');
insert into ks_statuses (id, description) values (4, 'Document communication with the keesing system has failed');
commit;

create table ks_responses 
(
   id              number not null
  ,file_id         number not null
  ,time_created    date   not null
  ,is_active       number(1) not null
  ,first_name      varchar2(50 char)
  ,last_name       varchar2(50 char)
  ,nationality     varchar2(3)
  ,issuing_country varchar2(3)
  ,doc_number      varchar2(50 char)
  ,expiry_date     date
  ,personal_id     number
  ,date_of_birth   date
  ,gender          varchar2(1)
  ,doc_id          number
  ,check_status    number not null
  ,reason_nok      number
  ,response        varchar2(4000 char)
  ,constraint pk_ks_responses primary key (id)
);

create index idx_kesr_file_id on ks_responses(file_id);
create unique index idx_kesr_active on ks_responses(case when is_active = 1 then file_id end);

alter table ks_responses add constraint fk_kesr_files foreign key (file_id) references files(id);

comment on table ks_responses is '[KESR] Store responses received by the keesing system';

comment on column ks_responses.check_status is 'The overall status of the conducted check. Possible values are:
0: OK
1: Not OK
2: At Keesing
3: At KMAR *
4: Not authorized
5: Runtime Exception';

comment on column ks_responses.reason_nok is 'If the checkstatus is not OK, this field contains information on the reason. Possible values are:
1: Invalid number of pages
2: Black/White copy
3: VIS hit
4: MRZ not available
5: N/A';

create or replace public synonym ks_responses for ks_responses;
grant select on ks_responses to etrader_web_connect;

create table ks_compare_statuses
(
   ks_responses_id    number    not null
  ,flag_first_name    number(1)
  ,flag_last_name     number(1)
  ,flag_country       number(1)
  ,flag_date_of_birth number(1)
  ,flag_gender        number(1)
  ,constraint pk_ks_compare_statuses primary key (ks_responses_id)
);

comment on table ks_compare_statuses is 'Save comparision results between values stored in our DB and values received by keesing';

alter table ks_compare_statuses add constraint fk_kscs_kesr foreign key (ks_responses_id) references ks_responses(id);

create sequence seq_ks_responses cache 100;

begin
  pkg_release.ddl_helper('create index idx_files_approved on files (is_approved, nvl(is_support_rejected, 0), time_uploaded) compress 2 online', -955);
end;
/

begin
  pkg_release.ddl_helper('create index idx_popu_issueactionid on  population_users (entry_type_action_id) online', -955);
end;
/
  
begin
  pkg_release.ddl_helper('create index idx_popd_issueactionid on  population_delays (delay_action_id) online', -955);
end;
/

begin
  pkg_release.ddl_helper('create index idx_pdoc_issueid on  users_pending_docs (issue_id) online', -955);
end;
/

insert into writers
  (id
  ,user_name
  ,password
  ,first_name
  ,last_name
  ,street
  ,city_id
  ,zip_code
  ,email
  ,comments
  ,time_birth_date
  ,mobile_phone
  ,land_line_phone
  ,is_active
  ,street_no
  ,utc_offset
  ,group_id
  ,is_support_enable
  ,dept_id
  ,sales_type
  ,nick_name_first
  ,nick_name_last
  ,auto_assign
  ,cms_login
  ,last_failed_time
  ,failed_count
  ,sales_type_dept_id
  ,emp_id
  ,role_id
  ,time_created)
values
  (22000
  ,'KEESING'
  ,'Keesing$$$'
  ,'Keesing'
  ,'Keesing'
  ,null
  ,null
  ,null
  ,'na'
  ,null
  ,sysdate
  ,null
  ,null
  ,1
  ,null
  ,'GMT+02:00'
  ,0
  ,1
  ,9
  ,null
  ,null
  ,null
  ,0
  ,null
  ,null
  ,0
  ,null
  ,null
  ,null
  ,sysdate);
commit;

create or replace context ao_scontext using etrader.pkg_scontext;
create or replace context ao_gcontext using etrader.pkg_gcontext accessed globally;
create table exec_sync (sync_value number constraint pk_exec_sync primary key);

@packages/pkg_scontext.pck
@packages/pkg_gcontext.pck

@packages/pkg_audit.pck
@packages/pkg_userfiles.pck
@packages/pkg_countries.pck
@triggers/trg_au_files.trg
@triggers/trg_bu_files.trg

drop trigger after_files_upd;
drop trigger before_files;


begin
  pkg_writers.create_permissions('Users', 'file', 'view', 10);
  pkg_writers.create_permissions('Users', 'file', 'save', 20);
  pkg_writers.create_permissions('Users', 'file', 'controlApprove', 30);
  pkg_writers.create_permissions('Users', 'file', 'controlReject', 40);
  pkg_writers.create_permissions('Users', 'file', 'sendToKeesing', 50);
end;
/

begin
  pkg_writers.create_permissions('Documents', 'idsFile', 'view', 10);
  pkg_writers.create_permissions('Documents', 'idsFile', 'save', 20);
  pkg_writers.create_permissions('Documents', 'idsFile', 'controlApprove', 30);
  pkg_writers.create_permissions('Documents', 'idsFile', 'controlReject', 40);
end;
/

commit;

create or replace public synonym pkg_userfiles for pkg_userfiles;
grant execute on pkg_userfiles to etrader_web_connect;

create or replace public synonym pkg_countries for pkg_countries;
grant execute on pkg_countries to etrader_web_connect;

begin
  pkg_release.ddl_helper('alter table users add constraint cc_users_balance check (balance >= 0) enable novalidate', -2264);
end;
/


----END; BAC-1977: Keesing Authentication; Victor



-------------------------------------------------------------------------------------------------------------------------------------------
-- CORE-3154 [java BE + json] for popup checkbox save
-- Kiril
-------------------------------------------------------------------------------------------------------------------------------------------
ALTER TABLE USERS_ACTIVE_DATA ADD (SHOW_CANCEL_INVESTMENT NUMBER DEFAULT 1 NOT NULL);

COMMENT ON COLUMN USERS_ACTIVE_DATA.SHOW_CANCEL_INVESTMENT IS 'True if the user sees the cancel investment functionality';
-------------------------------------------------------------------------------------------------------------------------------------------
-- CORE-3154 [java BE + json] for popup checkbox save
-- End
-------------------------------------------------------------------------------------------------------------------------------------------



---------------------------------------------------------------
--BAC-1946 Keesing Authentication Jamal
---------------------------------------------------------------
ALTER TABLE FILES_ADDITIONAL_INFO ADD (CC_EXP_DATE NUMBER(1), COUNTRY NUMBER(1));
    
 update files_additional_info set cc_exp_date = 0;
    
 update files_additional_info  fa set
        fa.id_number = 1,
        fa.first_name = 1,
        fa.last_name = 1
 where fa.file_type = 18;
                                      
 update files_additional_info set country = 0;
    
 update files_additional_info  fa set
        fa.country = 1
 where fa.file_type in(1,2,21);
                                      
 insert into files_additional_info
   (file_type, id_number, first_name, last_name, exp_date, cc_exp_date)
 values
   (23, 0, 0, 0, 0, 1);
                                      
insert into files_additional_info
 (file_type, id_number, first_name, last_name, exp_date, cc_exp_date)
values
 (24, 0, 0, 0, 0, 1);

insert into jobs (ID, RUNNING, LAST_RUN_TIME, SERVER, RUN_INTERVAL, DESCRIPTION, JOB_CLASS, CONFIG, RUN_SERVERS, RUN_APPLICATIONS)
values (33, 0, sysdate, 'BE03', 1, 'Send to KEESING File', 'com.anyoption.backend.jobs.KeesingSendFileJob', null, 'BE03', 'backend');

commit;
---------------------------------------------------------------
---BAC-1946 END Keesing Authentication
---------------------------------------------------------------
 
 ------------------------------------------------------------------------
-- AR-1968 - [server] - AO + CO - Add support for web deeplink to internal pages
-- Eyal Goren
------------------------------------------------------------------------
CREATE TABLE DEEPLINK_PARAMS_TO_URL 
(
  ID             NUMBER NOT NULL 
, PARAMETER_NAME VARCHAR2(20) NOT NULL 
, ANYOPTION_URL  VARCHAR2(200) 
, COPYOP_URL     VARCHAR2(200) 
, TIME_CREATED   DATE NOT NULL
, TIME_UPDATED   DATE
, WRITER_ID      NUMBER NOT NULL
, CONSTRAINT DEEPLINK_PARAMS_URL_PK PRIMARY KEY (PARAMETER_NAME)
);

COMMENT ON COLUMN DEEPLINK_PARAMS_TO_URL.PARAMETER_NAME IS 'the dpn parameter value in the url';

COMMENT ON COLUMN DEEPLINK_PARAMS_TO_URL.ANYOPTION_URL IS 'the relative path in anyoption';

COMMENT ON COLUMN DEEPLINK_PARAMS_TO_URL.COPYOP_URL IS 'the relative path in copyop';

COMMENT ON COLUMN DEEPLINK_PARAMS_TO_URL.TIME_CREATED IS 'the time this row was created';

COMMENT ON COLUMN DEEPLINK_PARAMS_TO_URL.TIME_UPDATED IS 'the time this row was updated';

COMMENT ON COLUMN DEEPLINK_PARAMS_TO_URL.WRITER_ID IS 'the writer who change this row last';

grant select, insert, update on DEEPLINK_PARAMS_TO_URL to ETRADER_WEB_CONNECT;
CREATE public synonym DEEPLINK_PARAMS_TO_URL for DEEPLINK_PARAMS_TO_URL;

create sequence SEQ_DEEPLINK_PARAMS_TO_URL;
CREATE public synonym SEQ_DEEPLINK_PARAMS_TO_URL for SEQ_DEEPLINK_PARAMS_TO_URL;

INSERT
  INTO DEEPLINK_PARAMS_TO_URL
    (
      TIME_CREATED ,
      ANYOPTION_URL ,
      PARAMETER_NAME ,
      ID ,
      WRITER_ID
    )
    VALUES
    (
      sysdate ,
      '/jsp/pages/reverseWithdraw.jsf' ,
      'reverseWithdraw' ,
      SEQ_DEEPLINK_PARAMS_TO_URL.nextval,
      25 
    );
    
INSERT
  INTO DEEPLINK_PARAMS_TO_URL
    (
      TIME_CREATED ,
      ANYOPTION_URL ,
      PARAMETER_NAME ,
      ID ,
      WRITER_ID
    )
    VALUES
    (
      sysdate ,
      '/jsp/pages/deposit.jsf' ,
      'deposit' ,
      SEQ_DEEPLINK_PARAMS_TO_URL.nextval,
      25 
    );

-------------------------- end -----------------------------------------

------------------------------------------------------------------------
-- CORE-3211
-- Open India to Registrations and Deposits
-- Simeon
------------------------------------------------------------------------
update COUNTRIES set is_blocked=0 where id=97;
------------------------------------------------------------------------
-- END
-- CORE-3211
------------------------------------------------------------------------

--------------------- ar-905 - epg --------------------------------------------
INSERT INTO jobs (ID, RUNNING, LAST_RUN_TIME, SERVER, RUN_INTERVAL, DESCRIPTION, JOB_CLASS, CONFIG, RUN_SERVERS, RUN_APPLICATIONS)
VALUES (34, 0, sysdate, '01', 60, 'check epg transaction every hour if status is 10 and pass 3 hours from time created', 'il.co.etrader.jobs.EpgTransactionsStatusJob', 'statusUrl=https://easypaymentgateway.com/EPGCheckout/rest/status/merchantcall/repeat', '00;01;02;03', 'anyoption');
-------------------------------------------------------------------------------

--------------------------------------------------------------------------------------------------------------------------------------------------------------

--LioR SoLoMoN
--EPG Checkout integration BAC-1926
--[server] Ouroboros - EPG Checkout integration

--*****NOTICE! this code needs to execute after EPG db changes**** 

@tables_data/bac-1926.sql
commit;

ALTER TABLE TRANSACTION_REQUEST_RESPONSE ADD (PAYMENT_DETAILS VARCHAR2(2000) );

COMMENT ON COLUMN TRANSACTION_REQUEST_RESPONSE.PAYMENT_DETAILS IS 'Payment Details';       
--------------------------------------------------------------------------------------------------------------------------------------------------------------
          
---- Victor; first deposit fixes in production

@packages/pkg_deposit.pck

@triggers/trg_aui_transactions.trg
@triggers/trg_first_dep_users.trg

begin
  pkg_release.ddl_helper('drop trigger trg_aft_upd_transaction_status', -4080);
end;
/
  
begin
  pkg_release.ddl_helper('drop trigger trg_after_ins_transaction', -4080);
end;
/

@triggers/after_trans_trg.trg

begin
  pkg_release.ddl_helper('drop index users_idx_is_netrefer_user', -1418);
end;
/
begin
  pkg_release.ddl_helper('drop index idx_users_id_country_id', -1418);
end;
/
begin
  pkg_release.ddl_helper('drop index idx_users_id_land_line_phone', -1418);
end;
/

begin
  pkg_release.ddl_helper('create unique index idx_users_lastlogin on users (time_last_login, class_id, id) online', -955);
end;
/
begin
  pkg_release.ddl_helper('drop index idx_id_lastlogin', -1418);
end;
/

begin
  pkg_release.ddl_helper('create index idx_users_land_phone on users (land_line_phone) online', -955);
end;
/
begin
  pkg_release.ddl_helper('drop index idx_user_land_phone', -1418);
end;
/

begin
  pkg_release.ddl_helper('create index idx_users_email on users (email) online', -955);
end;
/
begin
  pkg_release.ddl_helper('create index idx_users_mobile_phone on users (mobile_phone) online', -955);
end;
/
begin
  pkg_release.ddl_helper('drop index mobile_phone_idx', -1418);
end;
/
begin
  pkg_release.ddl_helper('drop index mobile_phone_idx1', -1418);
end;
/

begin
  pkg_release.ddl_helper('create unique index idx_users_fdid on users (first_deposit_id, class_id, id) online', -955);
end;
/
begin
  pkg_release.ddl_helper('drop index ret_inx', -1418);
end;
/
begin
  pkg_release.ddl_helper('drop index users_idx_first_deposit_id', -1418);
end;
/

---- END Victor; first deposit fixes in production

----------------------------------------------------------------------------------
-- START Kristina AR-1518 AO + CO - Support FB continued flow (Web + Mobile)
----------------------------------------------------------------------------------        
-- remove duplicates
delete from skin_url_country_map where id in (
select id
from   (select count(*) over(partition by skin_id, url_id, country_id) cnt
              ,id
              ,skin_id
              ,url_id
              ,country_id
              ,row_number() over(partition by skin_id, url_id, country_id order by id desc) rn
        from   skin_url_country_map)
where  rn > 1);
commit;

begin
  pkg_release.ddl_helper('alter table skin_url_country_map add constraint uk_skin_url_country_map unique (skin_id, url_id, country_id)'
                        ,-2261);
end;
/

insert into urls values(16, 'www.copyop.com', 'copyop');   

insert into skin_url_country_map
  (id, skin_id, url_id, country_id)
  select seq_skin_url_country_map.nextval, skin_id, 16 url_id, country_id from skin_url_country_map where url_id = 2;
commit;
 
 ----------------------------------------------------------------------------------
-- END AR-1518
----------------------------------------------------------------------------------   


------ Victor; market_formulas(?)

create table market_data_sources
(
   id                       number        not null 
  ,name                     varchar2(50)  not null 
  ,data_receive_layer_class varchar2(200) not null
  ,constraint pk_market_data_sources primary key (id)
);

comment on table market_data_sources is '[MDSC] List of available exchanges updates feed sources. Reuters, SIX etc';

create public synonym market_data_sources for market_data_sources;
grant select, insert, update on market_data_sources to etrader_service;
grant select on market_data_sources to ETRADER_WEB_CONNECT;

insert into market_data_sources (id, name, data_receive_layer_class) values (1, 'Reuters', 'il.co.etrader.service.level.reuters.ReutersService');
commit;

create table market_formulas
(
   id                      number        not null 
  ,name                    varchar2(50)  not null 
  ,description             varchar2(200) 
  ,formula_class           varchar2(200) not null
  ,formula_params          varchar2(2000) 
  ,type                    number        not null
  ,formula_description_key varchar2(50)  not null
  ,constraint pk_market_formulas primary key (id)
);

comment on table market_formulas is '[MFOR] List of available level formulas for markets calculation of current/closing levels.';

create public synonym market_formulas for market_formulas;
grant select, insert, update on market_formulas to etrader_service;
grant select on market_formulas to ETRADER_WEB_CONNECT;

create table market_data_source_maps 
(
   id                          number         not null 
  ,market_id                   number         not null 
  ,market_data_source_id       number         not null 
  ,hour_level_formula_id       number         not null 
  ,hour_level_formula_params   varchar2(2000) 
  ,hour_closing_formula_id     number         not null 
  ,hour_closing_formula_params varchar2(2000) 
  ,day_closing_formula_id      number         not null 
  ,day_closing_formula_params  varchar2(2000) 
  ,long_term_formula_id        number 
  ,long_term_formula_params    varchar2(2000) 
  ,real_level_formula_id       number         not null 
  ,real_level_formula_params   varchar2(2000) 
  ,market_params               varchar2(2000) 
  ,market_disable_conditions   varchar2(2000)
  ,constraint market_data_source_maps_pk primary key (id)
  ,constraint uk_market_data_source_maps unique (market_id, market_data_source_id)
);

comment on table market_data_source_maps is '[MDSM] Configuration for each market for each data source.';

alter table market_data_source_maps
add constraint fk_mdsm_mark foreign key (market_id) references markets(id);

alter table market_data_source_maps
add constraint fk_mdsm_mdsc foreign key(market_data_source_id) references market_data_sources(id);

alter table market_data_source_maps
add constraint fk_mdsm_mfor_hourl foreign key(hour_level_formula_id) references market_formulas(id);
alter table market_data_source_maps
add constraint fk_mdsm_mfor_hourc foreign key(hour_closing_formula_id) references market_formulas(id);
alter table market_data_source_maps
add constraint fk_mdsm_mfor_longt foreign key(long_term_formula_id) references market_formulas(id);
alter table market_data_source_maps
add constraint fk_mdsm_mfor_reall foreign key(real_level_formula_id) references market_formulas(id);


create public synonym market_data_source_maps for market_data_source_maps;
grant select, insert, update on market_data_source_maps to etrader_service;
grant select on market_data_source_maps to ETRADER_WEB_CONNECT;
grant update (hour_level_formula_params) on market_data_source_maps to etrader_backend;

create sequence seq_market_data_source_maps;
create public synonym seq_market_data_source_maps for seq_market_data_source_maps;

@tables_data/core-665_market_formulas.sql
@tables_data/core-665_market_data_source_maps.sql

commit;

------ END; Victor; market_formulas

-------------------------------------------------------------------------------------------------------------------------------------------
---- BAC-1951;  [DB] Countries that should have or not Neteller are not correctly setted; Victor
-------------------------------------------------------------------------------------------------------------------------------------------

-- Ivory coast
update payment_methods_countries
set    exclude_ind = 0
where  payment_id = 58
and    country_id = 52;

-- Cook Islands and US Virgin Islands
update payment_methods_countries
set    exclude_ind = 1
where  payment_id = 58
and    country_id in (50, 228);
commit;
-------------------------------------------------------------------------------------------------------------------------------------------
---- END; BAC-1951;  [DB] Countries that should have or not Neteller are not correctly setted; Victor
-------------------------------------------------------------------------------------------------------------------------------------------

-------------------------------------------------------------------------------------------------------------------------------------------
-- BAC-1747 Ability to edit the general terms pages from Backend
-- Jamal
-------------------------------------------------------------------------------------------------------------------------------------------
@tables_data/terms_files.sql
@tables_data/terms_parts.sql
@tables_data/terms.sql
commit;
-------------------------------------------------------------------------------------------------------------------------------------------
-- END BAC-1747 Ability to edit the general terms pages from Backend;
-------------------------------------------------------------------------------------------------------------------------------------------

---- Victor; core-3267; create string aggregation function to stop using wm_concat
@types/stringagg_t.typ
@functions/stringagg.fnc

create or replace public synonym stringagg for stringagg;
grant execute on stringagg to etrader_web_connect;
---- END; Victor; create string aggregation 

@packages/pkg_markets.pck
