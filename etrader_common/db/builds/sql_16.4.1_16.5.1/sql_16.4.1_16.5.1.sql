set serveroutput on size 1000000
set trimspool on
set timing    off
set feedback  off
set verify    off
set lines     230
set pages     1000
set termout   off
set echo      off

variable l number
variable n number

exec :n := dbms_utility.get_time;

-- set the new and old database version.
-- "new_version" is what version will be the database after upload of the build.
define new_version = '16.5.1'
-- "old_version" is the version we expect in order to successfully upload the build.
define old_version = '16.4.1'
   
column spool_file new_value logname noprint
select 'sql_&old_version._&new_version..log' spool_file from dual;

spool &logname append
   
select to_char(sysdate, 'yyyy-mm-dd hh24:mi:ss') start_time from dual;

whenever sqlerror exit failure commit
whenever oserror exit failure commit

alter session set ddl_lock_timeout = 30;

-- check the current user. It should be ETRADER in order to continue.
-- consider using proxy authentication! i.e. sqlplus victor[etrader]; "alter user etrader grant connect through victor;"
exec pkg_release.check_dbuser;
exec pkg_release.check_version('&old_version')

set feedback  on
set verify    on
set echo on
@_patch.sql
set echo off

exec pkg_release.update_version('&new_version')
commit;

whenever sqlerror continue

--create restore point "post_&new_version" ;
select to_char(sysdate, 'yyyy-mm-dd hh24:mi:ss') end_time from dual;

exec dbms_output.put_line('Total run: ' || to_char((dbms_utility.get_time - :n) / 100) || ' seconds.');

spool off

exit
