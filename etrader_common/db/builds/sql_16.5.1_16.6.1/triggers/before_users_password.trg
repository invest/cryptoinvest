create or replace trigger before_users_password
  before insert or update of password_back on users
  for each row
begin
  :new.password_back := null;
end;
/
