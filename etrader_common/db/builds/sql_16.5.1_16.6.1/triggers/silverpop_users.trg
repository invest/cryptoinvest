create or replace trigger silverpop_users
  after insert on users
  for each row
begin
  insert into silverpop_users (id, user_id) values (seq_silverpop_users.nextval, :new.id);

  insert into users_active_data
    (id, user_id, has_bubbles, has_dynamics)
  values
    (seq_users_active_data.nextval, :new.id, decode(:new.skin_id, 1, 0, 1), decode(:new.skin_id, 18, 1, 20, 1, 0));

  marketing.insert_marketing_att(marketing_attribution_obj(user_id                    => :new.id
                                                          ,type_id                    => 1
                                                          ,combination_id             => :new.combination_id
                                                          ,dynamic_param              => :new.dynamic_param
                                                          ,aff_sub1                   => null
                                                          ,aff_sub2                   => null
                                                          ,aff_sub3                   => null
                                                          ,affiliate_key              => :new.affiliate_key
                                                          ,special_code               => :new.special_code
                                                          ,affiliate_system           => :new.affiliate_system
                                                          ,affiliate_system_user_time => :new.affiliate_system_user_time));
end;
/
