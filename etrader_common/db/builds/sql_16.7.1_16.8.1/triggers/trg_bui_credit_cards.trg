create or replace trigger trg_bui_credit_cards
  before insert or update of cc_number_back on credit_cards
  for each row
begin
  :new.cc_number_back := null;
end;
/
