-------------------------------------------------------------------------------------------------------------------------------------------
-- AR-2316 BE - Add affiliate manager field to marketing affiliate table
-- Eyal O
-------------------------------------------------------------------------------------------------------------------------------------------

ALTER TABLE MARKETING_AFFILATES 
ADD (AFF_MANAGER_WRITER_ID NUMBER );

ALTER TABLE MARKETING_AFFILATES
ADD CONSTRAINT MARKETING_AFFILATES_FK1 FOREIGN KEY
(
  AFF_MANAGER_WRITER_ID 
)
REFERENCES WRITERS
(
  ID 
)
ENABLE;

COMMENT ON COLUMN MARKETING_AFFILATES.AFF_MANAGER_WRITER_ID IS 'Foreign key to WRITERS table';

-------------------------------------------------------------------------------------------------------------------------------------------
-- END
-- AR-2316 BE - Add affiliate manager field to marketing affiliate table
-- Eyal O
-------------------------------------------------------------------------------------------------------------------------------------------

------------------
--BAC-2329 Pavel Tabakov
------------------

INSERT INTO issue_action_writer_permission
  (writer_id, issue_action_type_id, is_active, time_created)
VALUES
  (282, 47, 1, SYSDATE);
  
  INSERT INTO issue_action_writer_permission
  (writer_id, issue_action_type_id, is_active, time_created)
VALUES
  (388, 47, 1, SYSDATE);
  
  INSERT INTO issue_action_writer_permission
  (writer_id, issue_action_type_id, is_active, time_created)
VALUES
  (507, 47, 1, SYSDATE);
  
  INSERT INTO issue_action_writer_permission
  (writer_id, issue_action_type_id, is_active, time_created)
VALUES
  (1428, 47, 1, SYSDATE);
  
  INSERT INTO issue_action_writer_permission
  (writer_id, issue_action_type_id, is_active, time_created)
VALUES
  (1444, 47, 1, SYSDATE);
  
commit;
------------------
--BAC-2329 Pavel Tabakov
------------------
  
------------------------------------------------------------------------------------------------
-- [AR-2369] [DB] - AO/CO/Mobile&Web - Change Support Working Hours
-- Eyal O
------------------------------------------------------------------------------------------------
  
-- IT
UPDATE 
  skins s 
SET 
  s.SUPPORT_START_TIME = '06:00',
  s.SUPPORT_END_TIME = '19:00'
where
  s.id in (9, 20);
  
-- DE
UPDATE 
  skins s 
SET 
  s.SUPPORT_START_TIME = '06:00',
  s.SUPPORT_END_TIME = '18:00'
where
  s.id in (8, 19);
  
-- TR
UPDATE 
  skins s 
SET 
  s.SUPPORT_START_TIME = '07:00',
  s.SUPPORT_END_TIME = '15:00'
where
  s.id = 3;
  
-- RU
UPDATE 
  skins s 
SET 
  s.SUPPORT_START_TIME = '07:00',
  s.SUPPORT_END_TIME = '15:00'
where
  s.id = 10;
  
-- FR  
UPDATE 
  skins s 
SET 
  s.SUPPORT_START_TIME = '07:00',
  s.SUPPORT_END_TIME = '15:00'
where
  s.id in (12, 21);
  
-- ES  
UPDATE 
  skins s 
SET 
  s.SUPPORT_START_TIME = '06:00',
  s.SUPPORT_END_TIME = '19:00'
where
  s.id in (5, 18);

commit;  
------------------------------------------------------------------------------------------------
-- END
-- [AR-2369] [DB] - AO/CO/Mobile&Web - Change Support Working Hours
-- Eyal O
------------------------------------------------------------------------------------------------


--- victor; CORE-3757
--@packages/transactions_manager.pck


--- victor; BAC-2314
--@packages/pkg_writers.pck

--- victor; Move BUBBLES markets to new exchange; Convert "full day" bubble daily markets to weekly; Adjust "create weekly" procedure for markets creation
begin
  insert into exchanges (id, name, half_day_close_time) values (52, 'Bubbles', null);

  update markets
  set    exchange_id = 52
  where  type_id = 5
  and    exchange_id <> 52;

/* these markets were rollbacked to daily on production!
  update opportunity_templates
  set    scheduled = 3, time_est_closing = '00:00'
  where  market_id in (706, 707, 700)
  and    is_full_day = 1;
*/

exception
  when dup_val_on_index then
    --- change was already implemented - skip
    null;
end;
/

commit;

@packages/opportunities_manager.pck
--- END; victor;


--- victor; core-3749
begin
  pkg_release.ddl_helper('alter table bubbles_token modify (server_id not null, time_created not null, login_id not null, writer_id not null)', -1442);
end;
/

--------------------------
--BAC-2376 Pavel Tabakov
--------------------------
INSERT INTO ISSUE_ACTION_WRITER_PERMISSION
  (WRITER_ID, ISSUE_ACTION_TYPE_ID, IS_ACTIVE, TIME_CREATED)
VALUES
  (898, 48, 1, SYSDATE);
commit;  
-------------------------
--BAC-2376 Pavel Tabakov
-------------------------
-------------------------------------------------------------------------------------------------------------------------------------------
-- CORE-3749 [java] Bubbles Dedicated App
-- Kiril
-------------------------------------------------------------------------------------------------------------------------------------------
CREATE TABLE BDA_LIMITS 
(
  ID NUMBER NOT NULL 
, CURRENCY_ID NUMBER NOT NULL 
, MIN_DEPOSIT NUMBER NOT NULL 
, MAX_DEPOSIT NUMBER NOT NULL 
, MIN_INVESTMENT NUMBER NOT NULL 
, MAX_INVESTMENT NUMBER NOT NULL 
, CONSTRAINT BDA_LIMITS_PK PRIMARY KEY 
  (
    ID 
  )
  ENABLE 
);

ALTER TABLE BDA_LIMITS
ADD CONSTRAINT BDA_LIMITS_CURRENCY_ID_FK1 FOREIGN KEY
(
  CURRENCY_ID 
)
REFERENCES CURRENCIES
(
  ID 
)
ENABLE;

grant select on bda_limits to ETRADER_WEB_CONNECT;
create or replace public synonym bda_limits for bda_limits;

insert into BDA_LIMITS (ID, CURRENCY_ID, MIN_DEPOSIT, MAX_DEPOSIT, MIN_INVESTMENT, MAX_INVESTMENT) values (1, 1, 500, 5000000, 300, 500000);
insert into BDA_LIMITS (ID, CURRENCY_ID, MIN_DEPOSIT, MAX_DEPOSIT, MIN_INVESTMENT, MAX_INVESTMENT) values (2, 2, 2500, 5000000, 500, 500000);
insert into BDA_LIMITS (ID, CURRENCY_ID, MIN_DEPOSIT, MAX_DEPOSIT, MIN_INVESTMENT, MAX_INVESTMENT) values (3, 3, 2500, 5000000, 500, 500000);
insert into BDA_LIMITS (ID, CURRENCY_ID, MIN_DEPOSIT, MAX_DEPOSIT, MIN_INVESTMENT, MAX_INVESTMENT) values (4, 4, 2500, 5000000, 500, 500000);
insert into BDA_LIMITS (ID, CURRENCY_ID, MIN_DEPOSIT, MAX_DEPOSIT, MIN_INVESTMENT, MAX_INVESTMENT) values (5, 5, 7500, 5000000, 1500, 500000);
insert into BDA_LIMITS (ID, CURRENCY_ID, MIN_DEPOSIT, MAX_DEPOSIT, MIN_INVESTMENT, MAX_INVESTMENT) values (6, 6, 150000, 5000000, 30000, 500000);
insert into BDA_LIMITS (ID, CURRENCY_ID, MIN_DEPOSIT, MAX_DEPOSIT, MIN_INVESTMENT, MAX_INVESTMENT) values (7, 7, 17500, 5000000, 3500, 500000);
insert into BDA_LIMITS (ID, CURRENCY_ID, MIN_DEPOSIT, MAX_DEPOSIT, MIN_INVESTMENT, MAX_INVESTMENT) values (8, 8, 500, 5000000, 300, 500000);
insert into BDA_LIMITS (ID, CURRENCY_ID, MIN_DEPOSIT, MAX_DEPOSIT, MIN_INVESTMENT, MAX_INVESTMENT) values (9, 9, 20000, 5000000, 5000, 500000);
insert into BDA_LIMITS (ID, CURRENCY_ID, MIN_DEPOSIT, MAX_DEPOSIT, MIN_INVESTMENT, MAX_INVESTMENT) values (10, 10, 3000, 5000000, 500, 500000);
insert into BDA_LIMITS (ID, CURRENCY_ID, MIN_DEPOSIT, MAX_DEPOSIT, MIN_INVESTMENT, MAX_INVESTMENT) values (11, 11, 35000, 5000000, 7000, 500000);
insert into BDA_LIMITS (ID, CURRENCY_ID, MIN_DEPOSIT, MAX_DEPOSIT, MIN_INVESTMENT, MAX_INVESTMENT) values (12, 12, 60000, 5000000, 10000, 500000);

insert into writers (ID, USER_NAME, PASSWORD, FIRST_NAME, LAST_NAME, EMAIL, TIME_BIRTH_DATE, IS_ACTIVE, UTC_OFFSET, GROUP_ID, IS_SUPPORT_ENABLE, DEPT_ID, AUTO_ASSIGN, TIME_CREATED) values (26000, 'BUBBLES_BDA', 'Bubbles$$$', 'BubblesBDA', 'BubblesBDA', 'na', sysdate, 1, 'GMT+02:00', 0, 1, 9, 0, sysdate);
commit;
-------------------------------------------------------------------------------------------------------------------------------------------
-- End
-------------------------------------------------------------------------------------------------------------------------------------------


---- BAC-2318; victor; card holder history
create table card_holder_history
(
  credit_card_id number             not null,
  cc_number      varchar2(200 char) not null,
  user_id        number             not null,
  holder_name    varchar2(30 char),
  time_created   date               not null,
  operation_id   number             not null
);

comment on table card_holder_history is '[CCHH] Populated by AUI trigger on credit_cards. Keep track of changes on HOLDER_NAME field';
comment on column card_holder_history.operation_id is '1 - after update on credit_cards; 2 - after insert in credit_cards;';

create index idx_CCHH_user_id on card_holder_history(user_id);

@triggers/trg_aui_credit_cards.trg
@triggers/trg_bui_credit_cards.trg

drop trigger before_credit_cards_number;

@packages/pkg_credit_card.pck

grant execute on pkg_credit_card to etrader_web_connect;
create or replace public synonym pkg_credit_card for pkg_credit_card;
---- END BAC-2318; victor; card holder history

------------------------------------------------------------------------------------------------
-- BAC-2267  Ezeebil deposit and withdrawal
------------------------------------------------------------------------------------------------

------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------
------ WARNING WARNING WARNING WARNING - update with production values !!! 
------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------
INSERT INTO clearing_providers VALUES(58, 'com.anyoption.common.clearing.EzeebillClearingProvider', 'https://66.70.107.101/ecommerce-web/pay3p', 'TERM_0001', 'n/a', 'n/a', 'returnURL=jsp/pages/ezeeBillStatus.jsf;version=1.0;access_id=otl2LP8D;pay_type=CU;api_access=12345678;merch_id=100100111122801', 'Ezeebill', '0', '1', NULL, NULL, 'fYAa6lLvcWB76gn8cbvqBEa283let4iQ', NULL, 2, 0);
commit;
------------------------------------------------------------------------------------------------
-- END BAC-2267  
------------------------------------------------------------------------------------------------

--- CORE-3790; victor
@packages/pkg_investments_web.pck

grant execute on pkg_investments_web to etrader_web_connect;
create or replace public synonym pkg_investments_web for pkg_investments_web;
--- END CORE-3790; victor

-----------------------------------
-- BAC-2198 Pavel Tabakov
-----------------------------------
ALTER TABLE 
   credit_cards
ADD
   docs_not_required  NUMBER DEFAULT 0 NOT NULL CONSTRAINT cc_ccards_docs_not_req CHECK (docs_not_required IN(0,1));

COMMENT ON COLUMN credit_cards.docs_not_required IS '1 - user''s credit card becomes irrelevant for the personnel checkup, 0 - user''s credit card becomes relevant for the personnel checkup';
-----------------------------------
-- BAC-2198 Pavel Tabakov
-----------------------------------


---- CORE-3768; victor;
@packages/pkg_transactions.pck

create or replace public synonym pkg_transactions for pkg_transactions;
grant execute on pkg_transactions to etrader_service;
---- END CORE-3768; victor;


---- victor; optimize indexes on INVESTMENTS table
begin
  pkg_release.ddl_helper('create index idx_invs_issett_user_type on investments (is_settled, user_id, type_id) compress online', -955);
end;
/
  
begin
  pkg_release.ddl_helper('drop index idx_opt_invp', -1418);
end;
/

begin
  pkg_release.ddl_helper('drop index idx_optp', -1418);
end;
/

begin
  pkg_release.ddl_helper('create index idx_invs_userid_oppid_timc on investments (user_id, opportunity_id, time_created) compress 1 online', -955);
end;
/
---- END; victor; optimize indexes on INVESTMENTS table

-----------------------------------
-- BAC-2267 Mariya Lazarova
-----------------------------------
update PAYMENT_METHODS_COUNTRIES set EXCLUDE_IND = 0 where PAYMENT_ID in (62,63) and EXCLUDE_IND = 1;
commit;
-----------------------------------
-- BAC-2267 Mariya Lazarova
-----------------------------------

------------------------------------------------------------------------------------------------
--BAC-2205 [java] Skin Selector part 
------------------------------------------------------------------------------------------------
ALTER TABLE skins ADD visible_type_id NUMBER(1) default 1 not null;
  
comment on column SKINS.visible_type_id is '0-Not visible on web/mobile; 1-Only Regulated; 2-Only NON Regulated; 3-Both Reg and NON-Reg';
  
update skins s set s.visible_type_id = 0;

update skins s set s.visible_type_id = 3 where id in (16,18,19,20,21,23,24); 
 
update skins s set s.visible_type_id = 2 where id in (3,10,15,17);
commit;
 ------------------------------------------------------------------------------------------------
-- END BAC-2205 [java] Skin Selector part
------------------------------------------------------------------------------------------------
 
------------------------------------------------------------------------------------------------
-- [AR-2423] [DB] - BE - adding "countries quality group" filter to entries table
-- Eyal O
------------------------------------------------------------------------------------------------

-- COUNTRIES_GROUP_TIERS
CREATE TABLE COUNTRIES_GROUP_TIERS 
(
  ID NUMBER NOT NULL 
, DISPLAY_NAME VARCHAR2(200) 
, NAME VARCHAR2(100) 
, CONSTRAINT COUNTRIES_GROUP_TIERS_PK PRIMARY KEY 
  (
    ID 
  )
  ENABLE 
);

COMMENT ON COLUMN COUNTRIES_GROUP_TIERS.DISPLAY_NAME IS 'The key of tier''s name, it will convert dynamically';

COMMENT ON COLUMN COUNTRIES_GROUP_TIERS.NAME IS 'Represent the tier''s name';

-- COUNTRIES_GROUP
ALTER TABLE COUNTRIES_GROUP 
ADD (COUNTRIES_GROUP_TIER_ID NUMBER );

ALTER TABLE COUNTRIES_GROUP
ADD CONSTRAINT COUNTRIES_GROUP_FK1 FOREIGN KEY
(
  COUNTRIES_GROUP_TIER_ID 
)
REFERENCES COUNTRIES_GROUP_TIERS
(
  ID 
)
ENABLE;

COMMENT ON COLUMN COUNTRIES_GROUP.COUNTRIES_GROUP_TIER_ID IS 'Foreign key to the table COUNTRIES_GROUP_TIERS';


ALTER TABLE COUNTRIES_GROUP 
ADD (WRITER_ID NUMBER );

ALTER TABLE COUNTRIES_GROUP 
ADD (TIME_MODIFIED DATE );

ALTER TABLE COUNTRIES_GROUP
ADD CONSTRAINT COUNTRIES_GROUP_FK2 FOREIGN KEY
(
  WRITER_ID 
)
REFERENCES WRITERS
(
  ID 
)
ENABLE;

COMMENT ON COLUMN COUNTRIES_GROUP.WRITER_ID IS 'Foreign key to the table WRITERS';

COMMENT ON COLUMN COUNTRIES_GROUP.TIME_MODIFIED IS 'Record''s time modified';

-- COUNTRIES_GROUP_TIERS
INSERT
  INTO COUNTRIES_GROUP_TIERS
    (
      DISPLAY_NAME ,
      NAME ,
      ID
    )
    VALUES
    (
      'countries.group.tier.1' ,
      'Tier 1' ,
      1
    );
    
INSERT
  INTO COUNTRIES_GROUP_TIERS
    (
      DISPLAY_NAME ,
      NAME ,
      ID
    )
    VALUES
    (
      'countries.group.tier.2' ,
      'Tier 2' ,
      2
    );
    
INSERT
  INTO COUNTRIES_GROUP_TIERS
    (
      DISPLAY_NAME ,
      NAME ,
      ID
    )
    VALUES
    (
      'countries.group.tier.3' ,
      'Tier 3' ,
      3
    );
    
-- COUNTRIES_GROUP update tiers
-- tier 1
UPDATE
  COUNTRIES_GROUP countries_group
set
  countries_group.COUNTRIES_GROUP_TIER_ID = 1,
  countries_group.WRITER_ID = 0,
  countries_group.TIME_MODIFIED = sysdate
where
  countries_group.COUNTRY_ID in (
                                  SELECT 
                                    co.id 
                                  FROM
                                    COUNTRIES co,
                                    COUNTRIES_GROUP cop
                                  where
                                    co.id = cop.COUNTRY_ID
                                    and co.COUNTRY_NAME in (
                                      'France',
                                      'Germany',
                                      'Italy',
                                      'Liechtenstein',
                                      'Switzerland',
                                      'Turkey',
                                      'United Kingdom',
                                      'Mexico',
                                      'Netherlands',
                                      'New Zealand',
                                      'Peru',
                                      'Austria',
                                      'Chile',
                                      'Russia',
                                      'Spain',
                                      'Swaziland',
                                      'Sweden',
                                      'Australia',
                                      'Belgium',
                                      'Luxembourg',
                                      'South Africa',
                                      'Singapore'
                                    )
                                );
                                
-- tier 2
UPDATE
  COUNTRIES_GROUP countries_group
set
  countries_group.COUNTRIES_GROUP_TIER_ID = 2,
  countries_group.WRITER_ID = 0,
  countries_group.TIME_MODIFIED = sysdate
where
  countries_group.COUNTRY_ID in (
                                  SELECT 
                                    co.id 
                                  FROM
                                    COUNTRIES co,
                                    COUNTRIES_GROUP cop
                                  where
                                    co.id = cop.COUNTRY_ID
                                    and co.COUNTRY_NAME in (
                                      'Colombia',
                                      'Czech Republic',
                                      'Hungary',
                                      'Iceland',
                                      'Ireland',
                                      'Kuwait',
                                      'Qatar',
                                      'Saudi Arabia',
                                      'Slovakia',
                                      'Malaysia',
                                      'Mali',
                                      'Norway',
                                      'Portugal',
                                      'Suriname',
                                      'United Arab Emirates',
                                      'Finland',
                                      'Thailand',
                                      'Canada',
                                      'Denmark',
                                      'Malta'
                                    )
                                );
                                
-- tier 3
UPDATE
  COUNTRIES_GROUP countries_group
set
  countries_group.COUNTRIES_GROUP_TIER_ID = 3,
  countries_group.WRITER_ID = 0,
  countries_group.TIME_MODIFIED = sysdate
where
  countries_group.COUNTRIES_GROUP_TIER_ID is null;

commit;
  
-- countries_group_tiers
grant select, insert, update on countries_group_tiers to ETRADER_WEB_CONNECT;
create or replace public synonym countries_group_tiers for countries_group_tiers;

@packages/pkg_countries.pck

------------------------------------------------------------------------------------------------
-- END
-- [AR-2423] [DB] - BE - adding "countries quality group" filter to entries table
-- Eyal O
------------------------------------------------------------------------------------------------

---------------
-- BAC-2210 Pavel Tabakov
---------------

begin
  pkg_release.ddl_helper('ALTER TABLE writers ADD
   is_password_reset NUMBER DEFAULT 0 NOT NULL CONSTRAINT cc_writers_is_password_reset CHECK (is_password_reset IN (0, 1))'
                        ,-1430);
end;
/

COMMENT ON COLUMN writers.is_password_reset IS '1 - true, 0 - false';

@packages/pkg_writers.pck
---------------
-- BAC-2210 Pavel Tabakov
---------------

--- victor; requested by Peter
begin
  pkg_writers.create_permissions('Sales', 'countryGroup', 'view', 10);
end;
/
commit;

--- victor; requested by Jimy
update countries
set    is_regulated = 0
where  country_name not in ('Austria'
                           ,'Belgium'
                           ,'Bulgaria'
                           ,'Czech Republic'
                           ,'Cyprus'
                           ,'Denmark'
                           ,'Estonia'
                           ,'Finland'
                           ,'France'
                           ,'Germany'
                           ,'Greece'
                           ,'Hungary'
                           ,'Iceland'
                           ,'Italy'
                           ,'Ireland'
                           ,'Latvia'
                           ,'Liechtenstein'
                           ,'Lithuania'
                           ,'Luxembourg'
                           ,'Malta'
                           ,'Netherlands'
                           ,'Norway'
                           ,'Poland'
                           ,'Portugal'
                           ,'Romania'
                           ,'Slovakia'
                           ,'Slovenia'
                           ,'Spain'
                           ,'Sweden'
                           ,'United Kingdom'
                           ,'South Africa'
                           ,'Australia');
commit;

---------------
-- BAC-2235 Pavel Tabakov
---------------
-- FIRST STEP 
ALTER TABLE  credit_cards  ADD  cc_number_last_4_digits  VARCHAR2(4);
   
-- SECOND STEP - manual
-- execute main method from UpdateBinsInCreditCardJob.class

-- THIRD STEP - manual
--ALTER TABLE credit_cards MODIFY cc_number_last_4_digits NOT NULL;
---------------
-- BAC-2235 Pavel Tabakov
---------------

------------------
-- BAC-2427 Pavel Tabakov
------------------
UPDATE skins s SET s.is_active = 1 WHERE ID IN (13,14);
commit;
------------------
-- BAC-2427 Pavel Tabakov
------------------
						   
--- victor; add new skins to existing writers (13 and 14); BAC-2269
insert into writers_skin
  (id, skin_id, writer_id, assign_limit)
  select seq_writers_skin.nextval, 13, a.writer_id, null
  from   writers_skin a
  where  not exists (select 1
          from   writers_skin b
          where  b.writer_id = a.writer_id
          and    b.skin_id = 13)
  and    a.skin_id = 2;

insert into writers_skin
  (id, skin_id, writer_id, assign_limit)
  select seq_writers_skin.nextval, 14, a.writer_id, null
  from   writers_skin a
  where  not exists (select 1
          from   writers_skin b
          where  b.writer_id = a.writer_id
          and    b.skin_id = 14)
  and    a.skin_id = 5;

commit;
--- END; victor; add new skins to existing writers (13 and 14); BAC-2269
						   
--- victor; BAC-2471
begin
  pkg_release.ddl_helper('create index idx_bbll_skin_bin on bin_black_list (skin_id, from_bin) compress 1', -955);
end;
/

@packages/transactions_manager.pck
--- END; victor; BAC-2471

---- pavelt bac-2210
ALTER TABLE writers MODIFY (email NVARCHAR2(50));
---- pavelt bac-2210

--- victor; optimized view; already in production
@views/opportunities_to_settle.sql

--- victor; BAC-
@packages/pkg_netrefer_reports.pck
grant execute on pkg_netrefer_reports to etrader_web_connect;
create or replace public synonym pkg_netrefer_reports for pkg_netrefer_reports;

--- victor; bug fixes
@packages/pkg_transactions_backend.pck

----
--BAC-2521 Pavel Tabakov
----
INSERT INTO ISSUE_ACTION_WRITER_PERMISSION
  (WRITER_ID, ISSUE_ACTION_TYPE_ID, IS_ACTIVE, TIME_CREATED)
VALUES
  (898, 79, 1, SYSDATE);

INSERT INTO ISSUE_ACTION_WRITER_PERMISSION
  (WRITER_ID, ISSUE_ACTION_TYPE_ID, IS_ACTIVE, TIME_CREATED)
VALUES
  (898, 80, 1, SYSDATE);

INSERT INTO ISSUE_ACTION_WRITER_PERMISSION
  (WRITER_ID, ISSUE_ACTION_TYPE_ID, IS_ACTIVE, TIME_CREATED)
VALUES
  (898, 48, 1, SYSDATE);
commit;  
----
--BAC-2521 Pavel Tabakov
----

