create or replace package pkg_investments_web is

  -- Author  : VICTOR.SLAVOV
  -- Created : 2016-08-24 12:34:31
  -- Purpose : /etrader_web/src/il/co/etrader/web/dao_managers/InvestmentsDAO.java

  procedure get_lastuserinvestments
  (
    o_data    out sys_refcursor
   ,i_user_id in number
   ,i_numrows in number
  );

end pkg_investments_web;
/
create or replace package body pkg_investments_web is

  procedure get_lastuserinvestments
  (
    o_data    out sys_refcursor
   ,i_user_id in number
   ,i_numrows in number
  ) is
  begin
    open o_data for
      select m.display_name, m.id marketid
      from   (select *
              from   (select o.market_id, i.time_created
                      from   investments i
                      join   opportunities o
                      on     o.id = i.opportunity_id
                      where  o.opportunity_type_id = 1
                      and    i.time_created >= (select time_created from users where id = i_user_id)
                      and    i.user_id = i_user_id
                      order  by i.time_created desc)
              where  rownum <= i_numrows) s
      join   markets m
      on     m.id = s.market_id
      order  by s.time_created desc;
  end get_lastuserinvestments;

end pkg_investments_web;
/
