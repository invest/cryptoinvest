create or replace package pkg_countries is

  -- Author  : VICTOR.SLAVOV
  -- Created : 2016-05-25 11:48:54
  -- Purpose : 

  procedure load_countries(o_countries out sys_refcursor);
  procedure get_country_groups(o_countries_group out sys_refcursor);

  procedure load_filters
  (
    o_countries_group_types out sys_refcursor
   ,o_countries_group_tiers out sys_refcursor
   ,o_countries out sys_refcursor
  );

  procedure update_country_group
  (
    i_cg_type_id in number
   ,i_cg_tier_id in number
   ,i_writer_id  in number
   ,i_country_id in number
  );

end pkg_countries;
/
create or replace package body pkg_countries is

  procedure load_countries(o_countries out sys_refcursor) is
  begin
  
    open o_countries for
      select c.id
            ,c.country_name
            ,c.a2
            ,c.a3
            ,c.phone_code
            ,c.display_name
            ,c.support_phone
            ,c.support_fax
            ,c.gmt_offset
            ,c.is_sms_available
            ,c.is_have_ukash_site
            ,c.is_display_pop_call_me
            ,c.is_display_pop_rnd
            ,c.is_display_pop_short_reg
            ,c.is_display_pop_reg_attempt
            ,c.is_alphanumeric_sender
            ,c.is_netrefer_reg_block
            ,c.countries_group_netrefer_id
            ,c.capital_city
            ,c.capital_city_latitude
            ,c.capital_city_longitude
            ,c.is_regulated
            ,c.is_blocked
      from   countries c
      where  c.a2 not in ('KP')
      order  by country_name;
  
  end load_countries;

  procedure get_country_groups(o_countries_group out sys_refcursor) is
  begin
    open o_countries_group for
      select co.id country_id, co.display_name country_display_name, cg.countries_group_type_id, cg.countries_group_tier_id
      from   countries co
      join   countries_group cg
      on     co.id = cg.country_id;
  end get_country_groups;

  procedure load_filters
  (
    o_countries_group_types out sys_refcursor
   ,o_countries_group_tiers out sys_refcursor
   ,o_countries out sys_refcursor
  ) is
  begin
    open o_countries_group_types for
      select cgt.id, cgt.name value from countries_group_type cgt order by cgt.id;
  
    open o_countries_group_tiers for
      select cgt.id, cgt.display_name value from countries_group_tiers cgt order by cgt.id;
      
    open o_countries for  
      select cn.id, cn.COUNTRY_NAME value from countries cn order by cn.id;
  end load_filters;

  procedure update_country_group
  (
    i_cg_type_id in number
   ,i_cg_tier_id in number
   ,i_writer_id  in number
   ,i_country_id in number
  ) is
  begin
    update countries_group cg
    set    cg.countries_group_type_id = i_cg_type_id
          ,cg.countries_group_tier_id = i_cg_tier_id
          ,cg.writer_id               = i_writer_id
          ,cg.time_modified           = sysdate
    where  cg.COUNTRY_ID = i_country_id;  
  end update_country_group;

end pkg_countries;

/
