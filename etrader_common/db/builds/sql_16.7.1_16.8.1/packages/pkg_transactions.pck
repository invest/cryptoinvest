create or replace package pkg_transactions is

  -- Author  : VICTOR.SLAVOV
  -- Created : 2016-08-12 10:07:59
  -- Purpose : /etrader_level_service/src/il/co/etrader/service/transactionsIssues/daos/TransactionsIssuesDAO.java

  procedure get_last_trnsusers
  (
    o_data         out sys_refcursor
   ,i_last_minutes in number default 30
  );

end pkg_transactions;
/
create or replace package body pkg_transactions is

  procedure get_last_trnsusers
  (
    o_data         out sys_refcursor
   ,i_last_minutes in number default 30
  ) is
    l_fromdate date;
    l_todate   date;
  begin
    select date_value - i_last_minutes/(24*60), sysdate - i_last_minutes/(24*60)
    into   l_fromdate, l_todate
    from   db_parameters
    where  name = 'last_first_deposit_email_run';
  
    open o_data for
      select u.id
            ,u.user_name
            ,u.skin_id
            ,u.mobile_phone
            ,u.land_line_phone
            ,u.currency_id
            ,t.amount          first_deposit_amount
            ,t.time_created    first_deposit_time_created
            ,c.phone_code      country_phone_code
            ,c.country_name    country_name
      from   transactions t
      join   users u
      on     u.id = t.user_id
      and    u.first_deposit_id = t.id
      join   countries c
      on     u.country_id = c.id
      where  t.time_created between l_fromdate and l_todate
      and    not exists (select 1 from transactions_issues where transaction_id = t.id)
      and    u.class_id <> 0;
  
  end get_last_trnsusers;

end pkg_transactions;
/
