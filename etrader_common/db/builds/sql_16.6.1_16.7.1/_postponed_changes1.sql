
whenever sqlerror exit

spool _postponed_changes1.log

--- victor; AR-2110; [DB] - Field additions - writers commission - Addition 1
alter table writers_commission_dep add (last_issue_action_id number);
comment on column writers_commission_dep.last_issue_action_id is 'the last ISSUE_ACTIONS.id of the issue that made the deposit qualify';
@packages/writers_commission.pck

--- victor; AR-2115; [DB] - Field additions - writers commission - Addition 6
alter table writers_commission_inv add (last_issue_action_id number);
comment on column writers_commission_inv.last_issue_action_id is ' the last ISSUE_ACTIONS.id of the issue action that made the investment qualify into WRITERS_COMMISION_INV';
@packages/pkg_writers_commission_inv.pck



----------------------------------------------------------------------------------------
-- [AR-1956] Add users with lifetime volume > 100K $ as VIP (VIP)
-- Eyal O
----------------------------------------------------------------------------------------

@tables_data/AR-1956.sql

commit;

----------------------------------------------------------------------------------------
-- [AR-1956] Add users with lifetime volume > 100K $ as VIP (VIP)
-- Eyal O
----------------------------------------------------------------------------------------


spool off
