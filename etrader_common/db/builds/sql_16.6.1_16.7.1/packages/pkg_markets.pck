create or replace package pkg_markets is

  -- Author  : VICTORS
  -- Created : 2016-03-30 16:53:27
  -- Purpose : for MarketsDAO

  procedure delete_market_rates(pmarketslist in number_table);

  procedure create_market
  (
    o_new_market_id out number
   ,i_src_market_id in number
   ,i_writer_id     in number
  );

  procedure get_market
  (
    o_market    out sys_refcursor
   ,i_market_id in number
  );

  procedure update_market
  (
    i_market_id                    in number
   ,i_name                         in varchar2
   ,i_display_name                 in varchar2
   ,i_feed_name                    in varchar2
   ,i_writer_id                    in number
   ,i_decimal_point                in number
   ,i_market_group_id              in integer
   ,i_exchange_id                  in integer
   ,i_investment_limits_group_id   in number
   ,i_updates_pause                in number
   ,i_significant_percentage       in number
   ,i_real_updates_pause           in number
   ,i_real_significant_percentage  in number
   ,i_random_ceiling               in number
   ,i_random_floor                 in number
   ,i_first_shift_parameter        in number
   ,i_average_investments          in number
   ,i_pct_of_avg_investments       in number
   ,i_fixed_amount_for_shifting    in number
   ,i_type_of_shifting             in number
   ,i_disable_after_period         in integer
   ,i_curent_level_alert_perc      in number
   ,i_is_suspended                 in number
   ,i_suspended_message            in varchar2
   ,i_acceptable_level_deviation   in number
   ,i_is_has_future_agreement      in number
   ,i_is_next_agreement_same_day   in number
   ,i_banners_priority             in number
   ,i_sec_between_investments      in number
   ,i_sec_for_dev2                 in number
   ,i_amount_for_dev2              in varchar2
   ,i_max_exposure                 in number
   ,i_roll_up_qualify_percent      in number
   ,i_is_roll_up                   in number
   ,i_is_golden_minutes            in number
   ,i_insurance_premia_percent     in number
   ,i_insurance_qualify_percent    in number
   ,i_roll_up_premia_percent       in number
   ,i_no_auto_settle_after         in number
   ,i_shifting_group_id            in number
   ,i_option_plus_fee              in number
   ,i_option_plus_market_id        in number
   ,i_acceptable_level_deviation_3 in number
   ,i_sec_between_investments_ip   in number
   ,i_amount_for_dev3              in number
   ,i_dec_point_subtract_digits    in number
   ,i_amount_for_dev2_night        in number
   ,i_exposure_reached_amount      in number
   ,i_raise_exposure_up_amount     in number
   ,i_raise_exposure_down_amount   in number
   ,i_raise_call_lower_put_percent in number
   ,i_raise_put_lower_call_percent in number
   ,i_sec_for_dev2_mob             in number
   ,i_sec_between_investments_mob  in number
   ,i_sec_btwn_investments_ip_mob  in number
   ,i_quote_params                 in varchar2
   ,i_use_for_fake_investments     in number
   ,i_asset_index_exp_levlel_calc  in varchar2
   ,i_bubbles_pricing_level        in number
   ,i_type_id                      in number
   ,i_worst_case_return            in number
   ,i_limited_high_amnt            in number
   ,i_instant_spread               in number
   ,i_limited_from_amnt            in number
  );

  procedure get_market_formulas
  (
    o_data                  out sys_refcursor
   ,i_market_id             in number
   ,i_market_data_source_id in number
  );

  procedure update_market_formulas
  (
    i_market_id                   in number
   ,i_market_data_source_id       in number
   ,i_hour_level_formula_params   in varchar2
   ,i_hour_closing_formula_params in varchar2
   ,i_day_closing_formula_params  in varchar2
   ,i_long_term_formula_params    in varchar2
   ,i_real_level_formula_params   in varchar2
   ,i_market_params               in varchar2
   ,i_market_disable_conditions   in varchar2
  );

  procedure get_market_translations
  (
    o_data      out sys_refcursor
   ,i_market_id in number
  );

  procedure update_market_translations
  (
    i_market_id               in number
   ,i_language_id             in number
   ,i_name                    in varchar2
   ,i_short_name              in varchar2
   ,i_additional_text         in varchar2
   ,i_market_description      in clob
   ,i_page_description        in varchar2
   ,i_page_keywords           in varchar2
   ,i_page_title              in varchar2
   ,i_market_description_page in varchar2
  );

  procedure create_opportunities(i_market_id in number);

  function gen_insert
  (
    i_table_name in varchar2
   ,i_filter     in varchar2
   ,i_filter_val in varchar2
   ,i_seq        in varchar2
  ) return string_table
    pipelined;

  /* 
      -- Will extrackt all data for market by ID as "insert" statements 
      -- so that it can be recreated into another database.
      -- Make sure NLS_LANG is set to .UTF8 before generating the script!
      -- Should be executed with user ETRADER on target database.
      -- sample use from sqlplus:
      set heading off
      set linesize 10000
      set pagesize 50000
      set echo off
      set trimspo on
      set feedback off
      spool /tmp/create_market_<ID>.sql
      select * from table(etrader.pkg_markets.gen_create_market_script(<ID>));
      spool off
  */
  function gen_create_market_script(i_market_id in number) return string_table
    pipelined;
end pkg_markets;
/
create or replace package body pkg_markets is

  procedure delete_market_rates(pmarketslist in number_table) is
  begin
    if pmarketslist is not null
    then
      -- delete for given markets records older than hour and a half
      forall i in 1 .. pmarketslist.count
        delete from market_rates
        where  market_id = pmarketslist(i)
        and    rate_time < sysdate - 3 / 48;
    end if;
  end delete_market_rates;

  procedure create_market
  (
    o_new_market_id out number
   ,i_src_market_id in number
   ,i_writer_id     in number
  ) is
    l_new_market_id number;
    l_tmp           number;
  begin
  
    select max(id) + 1 into l_new_market_id from markets;
  
    insert into markets
      (id
      ,name
      ,display_name
      ,feed_name
      ,time_created
      ,writer_id
      ,decimal_point
      ,market_group_id
      ,exchange_id
      ,investment_limits_group_id
      ,updates_pause
      ,significant_percentage
      ,real_updates_pause
      ,real_significant_percentage
      ,random_ceiling
      ,random_floor
      ,first_shift_parameter
      ,average_investments
      ,percentage_of_avg_investments
      ,fixed_amount_for_shifting
      ,type_of_shifting
      ,disable_after_period
      ,curent_level_alert_perc
      ,is_suspended
      ,suspended_message
      ,acceptable_level_deviation
      ,is_has_future_agreement
      ,is_next_agreement_same_day
      ,banners_priority
      ,sec_between_investments
      ,sec_for_dev2
      ,amount_for_dev2
      ,max_exposure
      ,roll_up_qualify_percent
      ,is_roll_up
      ,is_golden_minutes
      ,insurance_premia_percent
      ,insurance_qualify_percent
      ,roll_up_premia_percent
      ,no_auto_settle_after
      ,shifting_group_id
      ,option_plus_fee
      ,option_plus_market_id
      ,acceptable_level_deviation_3
      ,sec_between_investments_ip
      ,amount_for_dev3
      ,decimal_point_subtract_digits
      ,amount_for_dev2_night
      ,exposure_reached_amount
      ,raise_exposure_up_amount
      ,raise_exposure_down_amount
      ,raise_call_lower_put_percent
      ,raise_put_lower_call_percent
      ,sec_for_dev2_mob
      ,sec_between_investments_mob
      ,sec_between_investments_ip_mob
      ,quote_params
      ,use_for_fake_investments
      ,asset_index_exp_levlel_calc
      ,bubbles_pricing_level
      ,type_id
      ,worst_case_return
      ,limited_high_amnt
      ,instant_spread
      ,limited_from_amnt)
      select l_new_market_id
            ,name || '.Copy'
            ,display_name || '.Copy'
            ,feed_name || '.Copy'
            ,sysdate time_created
            ,i_writer_id
            ,decimal_point
            ,market_group_id
            ,exchange_id
            ,investment_limits_group_id
            ,updates_pause
            ,significant_percentage
            ,real_updates_pause
            ,real_significant_percentage
            ,random_ceiling
            ,random_floor
            ,first_shift_parameter
            ,average_investments
            ,percentage_of_avg_investments
            ,fixed_amount_for_shifting
            ,type_of_shifting
            ,disable_after_period
            ,curent_level_alert_perc
            ,is_suspended
            ,suspended_message
            ,acceptable_level_deviation
            ,is_has_future_agreement
            ,is_next_agreement_same_day
            ,banners_priority
            ,sec_between_investments
            ,sec_for_dev2
            ,amount_for_dev2
            ,max_exposure
            ,roll_up_qualify_percent
            ,is_roll_up
            ,is_golden_minutes
            ,insurance_premia_percent
            ,insurance_qualify_percent
            ,roll_up_premia_percent
            ,no_auto_settle_after
            ,shifting_group_id
            ,option_plus_fee
            ,option_plus_market_id
            ,acceptable_level_deviation_3
            ,sec_between_investments_ip
            ,amount_for_dev3
            ,decimal_point_subtract_digits
            ,amount_for_dev2_night
            ,exposure_reached_amount
            ,raise_exposure_up_amount
            ,raise_exposure_down_amount
            ,raise_call_lower_put_percent
            ,raise_put_lower_call_percent
            ,sec_for_dev2_mob
            ,sec_between_investments_mob
            ,sec_between_investments_ip_mob
            ,quote_params
            ,use_for_fake_investments
            ,asset_index_exp_levlel_calc
            ,bubbles_pricing_level
            ,type_id
            ,worst_case_return
            ,limited_high_amnt
            ,instant_spread
            ,limited_from_amnt
      from   markets
      where  id = i_src_market_id;
  
    insert into asset_index
      (market_id, trading_days, friday_time, start_time, end_time, is_full_day, is_24_7, start_break_time, end_break_time)
      select l_new_market_id, trading_days, friday_time, start_time, end_time, is_full_day, is_24_7, start_break_time, end_break_time
      from   asset_index
      where  market_id = i_src_market_id;
  
    insert into opportunity_templates
      (id
      ,market_id
      ,time_first_invest
      ,time_est_closing
      ,time_last_invest
      ,odds_type_id
      ,writer_id
      ,opportunity_type_id
      ,is_active
      ,scheduled
      ,time_zone
      ,is_full_day
      ,is_half_day
      ,deduct_win_odds
      ,one_touch_decimal_point
      ,up_down
      ,is_open_graph
      ,odds_group
      ,max_inv_coeff)
      select seq_opportunity_templates.nextval
            ,l_new_market_id
            ,time_first_invest
            ,time_est_closing
            ,time_last_invest
            ,odds_type_id
            ,i_writer_id
            ,opportunity_type_id
            ,is_active
            ,scheduled
            ,time_zone
            ,is_full_day
            ,is_half_day
            ,deduct_win_odds
            ,one_touch_decimal_point
            ,up_down
            ,is_open_graph
            ,odds_group
            ,max_inv_coeff
      from   opportunity_templates
      where  market_id = i_src_market_id;
  
    select max(home_page_priority) + 1 into l_tmp from skin_market_group_markets;
  
    insert into skin_market_group_markets
      (id, skin_market_group_id, market_id, group_priority, home_page_priority, skin_display_group_id, ticker_priority, banners_priority)
      select seq_skin_market_group_markets.nextval
            ,skin_market_group_id
            ,l_new_market_id
            ,group_priority
            ,l_tmp home_page_priority
            ,skin_display_group_id
            ,ticker_priority
            ,banners_priority
      from   skin_market_group_markets t
      where  t.market_id = i_src_market_id;
  
    insert into asset_indexs_info
      (id
      ,market_id
      ,skin_id
      ,market_description_old
      ,additional_text
      ,page_title
      ,page_keywords
      ,page_description
      ,market_description_page
      ,market_description)
      select seq_asset_indexs_info.nextval
            ,l_new_market_id
            ,skin_id
            ,market_description_old
            ,additional_text
            ,page_title
            ,page_keywords
            ,page_description
            ,market_description_page
            ,market_description
      from   asset_indexs_info
      where  market_id = i_src_market_id;
  
    insert into market_data_source_maps
      (id
      ,market_id
      ,market_data_source_id
      ,hour_level_formula_id
      ,hour_level_formula_params
      ,hour_closing_formula_id
      ,hour_closing_formula_params
      ,day_closing_formula_id
      ,day_closing_formula_params
      ,long_term_formula_id
      ,long_term_formula_params
      ,real_level_formula_id
      ,real_level_formula_params
      ,market_params
      ,market_disable_conditions)
      select seq_market_data_source_maps.nextval
            ,l_new_market_id
            ,market_data_source_id
            ,hour_level_formula_id
            ,hour_level_formula_params
            ,hour_closing_formula_id
            ,hour_closing_formula_params
            ,day_closing_formula_id
            ,day_closing_formula_params
            ,long_term_formula_id
            ,long_term_formula_params
            ,real_level_formula_id
            ,real_level_formula_params
            ,market_params
            ,market_disable_conditions
      from   market_data_source_maps
      where  market_id = i_src_market_id;
  
    insert into market_name_skin
      (id, market_id, skin_id, name, short_name)
      select seq_market_name_skin.nextval, l_new_market_id, skin_id, name || '.Copy', short_name || '.Copy'
      from   market_name_skin
      where  market_id = i_src_market_id;
  
    o_new_market_id := l_new_market_id;
  end create_market;

  procedure get_market
  (
    o_market    out sys_refcursor
   ,i_market_id in number
  ) is
  begin
  
    open o_market for
      select id
            ,name
            ,display_name
            ,feed_name
            ,time_created
            ,writer_id
            ,decimal_point
            ,market_group_id
            ,exchange_id
            ,investment_limits_group_id
            ,updates_pause
            ,significant_percentage
            ,real_updates_pause
            ,real_significant_percentage
            ,random_ceiling
            ,random_floor
            ,first_shift_parameter
            ,average_investments
            ,percentage_of_avg_investments
            ,fixed_amount_for_shifting
            ,type_of_shifting
            ,disable_after_period
            ,curent_level_alert_perc
            ,is_suspended
            ,suspended_message
            ,acceptable_level_deviation
            ,is_has_future_agreement
            ,is_next_agreement_same_day
            ,banners_priority
            ,sec_between_investments
            ,sec_for_dev2
            ,amount_for_dev2
            ,max_exposure
            ,roll_up_qualify_percent
            ,is_roll_up
            ,is_golden_minutes
            ,insurance_premia_percent
            ,insurance_qualify_percent
            ,roll_up_premia_percent
            ,no_auto_settle_after
            ,shifting_group_id
            ,option_plus_fee
            ,option_plus_market_id
            ,acceptable_level_deviation_3
            ,sec_between_investments_ip
            ,amount_for_dev3
            ,decimal_point_subtract_digits
            ,amount_for_dev2_night
            ,exposure_reached_amount
            ,raise_exposure_up_amount
            ,raise_exposure_down_amount
            ,raise_call_lower_put_percent
            ,raise_put_lower_call_percent
            ,sec_for_dev2_mob
            ,sec_between_investments_mob
            ,sec_between_investments_ip_mob
            ,quote_params
            ,use_for_fake_investments
            ,asset_index_exp_levlel_calc
            ,bubbles_pricing_level
            ,type_id
            ,worst_case_return
            ,limited_high_amnt
            ,instant_spread
            ,limited_from_amnt
      from   markets
      where  id = i_market_id;
  
  end get_market;

  procedure update_market
  (
    i_market_id                    in number
   ,i_name                         in varchar2
   ,i_display_name                 in varchar2
   ,i_feed_name                    in varchar2
   ,i_writer_id                    in number
   ,i_decimal_point                in number
   ,i_market_group_id              in integer
   ,i_exchange_id                  in integer
   ,i_investment_limits_group_id   in number
   ,i_updates_pause                in number
   ,i_significant_percentage       in number
   ,i_real_updates_pause           in number
   ,i_real_significant_percentage  in number
   ,i_random_ceiling               in number
   ,i_random_floor                 in number
   ,i_first_shift_parameter        in number
   ,i_average_investments          in number
   ,i_pct_of_avg_investments       in number
   ,i_fixed_amount_for_shifting    in number
   ,i_type_of_shifting             in number
   ,i_disable_after_period         in integer
   ,i_curent_level_alert_perc      in number
   ,i_is_suspended                 in number
   ,i_suspended_message            in varchar2
   ,i_acceptable_level_deviation   in number
   ,i_is_has_future_agreement      in number
   ,i_is_next_agreement_same_day   in number
   ,i_banners_priority             in number
   ,i_sec_between_investments      in number
   ,i_sec_for_dev2                 in number
   ,i_amount_for_dev2              in varchar2
   ,i_max_exposure                 in number
   ,i_roll_up_qualify_percent      in number
   ,i_is_roll_up                   in number
   ,i_is_golden_minutes            in number
   ,i_insurance_premia_percent     in number
   ,i_insurance_qualify_percent    in number
   ,i_roll_up_premia_percent       in number
   ,i_no_auto_settle_after         in number
   ,i_shifting_group_id            in number
   ,i_option_plus_fee              in number
   ,i_option_plus_market_id        in number
   ,i_acceptable_level_deviation_3 in number
   ,i_sec_between_investments_ip   in number
   ,i_amount_for_dev3              in number
   ,i_dec_point_subtract_digits    in number
   ,i_amount_for_dev2_night        in number
   ,i_exposure_reached_amount      in number
   ,i_raise_exposure_up_amount     in number
   ,i_raise_exposure_down_amount   in number
   ,i_raise_call_lower_put_percent in number
   ,i_raise_put_lower_call_percent in number
   ,i_sec_for_dev2_mob             in number
   ,i_sec_between_investments_mob  in number
   ,i_sec_btwn_investments_ip_mob  in number
   ,i_quote_params                 in varchar2
   ,i_use_for_fake_investments     in number
   ,i_asset_index_exp_levlel_calc  in varchar2
   ,i_bubbles_pricing_level        in number
   ,i_type_id                      in number
   ,i_worst_case_return            in number
   ,i_limited_high_amnt            in number
   ,i_instant_spread               in number
   ,i_limited_from_amnt            in number
  ) is
  begin
    update markets m
    set    m.name                           = i_name
          ,m.display_name                   = i_display_name
          ,m.feed_name                      = i_feed_name
          ,m.writer_id                      = i_writer_id
          ,m.decimal_point                  = i_decimal_point
          ,m.market_group_id                = i_market_group_id
          ,m.exchange_id                    = i_exchange_id
          ,m.investment_limits_group_id     = i_investment_limits_group_id
          ,m.updates_pause                  = i_updates_pause
          ,m.significant_percentage         = i_significant_percentage
          ,m.real_updates_pause             = i_real_updates_pause
          ,m.real_significant_percentage    = i_real_significant_percentage
          ,m.random_ceiling                 = i_random_ceiling
          ,m.random_floor                   = i_random_floor
          ,m.first_shift_parameter          = i_first_shift_parameter
          ,m.average_investments            = i_average_investments
          ,m.percentage_of_avg_investments  = i_pct_of_avg_investments
          ,m.fixed_amount_for_shifting      = i_fixed_amount_for_shifting
          ,m.type_of_shifting               = i_type_of_shifting
          ,m.disable_after_period           = i_disable_after_period
          ,m.curent_level_alert_perc        = i_curent_level_alert_perc
          ,m.is_suspended                   = i_is_suspended
          ,m.suspended_message              = i_suspended_message
          ,m.acceptable_level_deviation     = i_acceptable_level_deviation
          ,m.is_has_future_agreement        = i_is_has_future_agreement
          ,m.is_next_agreement_same_day     = i_is_next_agreement_same_day
          ,m.banners_priority               = i_banners_priority
          ,m.sec_between_investments        = i_sec_between_investments
          ,m.sec_for_dev2                   = i_sec_for_dev2
          ,m.amount_for_dev2                = i_amount_for_dev2
          ,m.max_exposure                   = i_max_exposure
          ,m.roll_up_qualify_percent        = i_roll_up_qualify_percent
          ,m.is_roll_up                     = i_is_roll_up
          ,m.is_golden_minutes              = i_is_golden_minutes
          ,m.insurance_premia_percent       = i_insurance_premia_percent
          ,m.insurance_qualify_percent      = i_insurance_qualify_percent
          ,m.roll_up_premia_percent         = i_roll_up_premia_percent
          ,m.no_auto_settle_after           = i_no_auto_settle_after
          ,m.shifting_group_id              = i_shifting_group_id
          ,m.option_plus_fee                = i_option_plus_fee
          ,m.option_plus_market_id          = i_option_plus_market_id
          ,m.acceptable_level_deviation_3   = i_acceptable_level_deviation_3
          ,m.sec_between_investments_ip     = i_sec_between_investments_ip
          ,m.amount_for_dev3                = i_amount_for_dev3
          ,m.decimal_point_subtract_digits  = i_dec_point_subtract_digits
          ,m.amount_for_dev2_night          = i_amount_for_dev2_night
          ,m.exposure_reached_amount        = i_exposure_reached_amount
          ,m.raise_exposure_up_amount       = i_raise_exposure_up_amount
          ,m.raise_exposure_down_amount     = i_raise_exposure_down_amount
          ,m.raise_call_lower_put_percent   = i_raise_call_lower_put_percent
          ,m.raise_put_lower_call_percent   = i_raise_put_lower_call_percent
          ,m.sec_for_dev2_mob               = i_sec_for_dev2_mob
          ,m.sec_between_investments_mob    = i_sec_between_investments_mob
          ,m.sec_between_investments_ip_mob = i_sec_btwn_investments_ip_mob
          ,m.quote_params                   = i_quote_params
          ,m.use_for_fake_investments       = i_use_for_fake_investments
          ,m.asset_index_exp_levlel_calc    = i_asset_index_exp_levlel_calc
          ,m.bubbles_pricing_level          = i_bubbles_pricing_level
          ,m.type_id                        = i_type_id
          ,m.worst_case_return              = i_worst_case_return
          ,m.limited_high_amnt              = i_limited_high_amnt
          ,m.instant_spread                 = i_instant_spread
          ,m.limited_from_amnt              = i_limited_from_amnt
    where  id = i_market_id;
  
    if sql%notfound
    then
      raise_application_error(-20000, 'Market does not exists: ' || i_market_id);
    end if;
  end update_market;

  function gen_insert
  (
    i_table_name in varchar2
   ,i_filter     in varchar2
   ,i_filter_val in varchar2
   ,i_seq        in varchar2
  ) return string_table
    pipelined is
    l_cur         sys_refcursor;
    l_curid       number;
    l_desctab     dbms_sql.desc_tab;
    l_colcnt      number;
    l_namevar     varchar2(4000);
    l_numvar      number;
    l_datevar     date;
    l_clobvar     clob;
    l_out_columns varchar2(32767);
    l_out_values  varchar2(32767);
  begin
    for rec in (select table_name from user_tables where table_name = i_table_name)
    loop
      open l_cur for 'SELECT * FROM "' || rec.table_name || '" WHERE ' || i_filter || ' = :FILTER'
        using i_filter_val;
    
      l_curid := dbms_sql.to_cursor_number(l_cur);
      dbms_sql.describe_columns(l_curid, l_colcnt, l_desctab);
    
      l_out_columns := 'INSERT INTO ' || rec.table_name || '(';
    
      for indx in 1 .. l_colcnt
      loop
        l_out_columns := l_out_columns || l_desctab(indx).col_name || ',';
      
        if l_desctab(indx).col_type = 2
        then
          dbms_sql.define_column(l_curid, indx, l_numvar);
        elsif l_desctab(indx).col_type = 12
        then
          dbms_sql.define_column(l_curid, indx, l_datevar);
        elsif l_desctab(indx).col_type = 112
        then
          dbms_sql.define_column(l_curid, indx, l_clobvar);
        else
          dbms_sql.define_column(l_curid, indx, l_namevar, 4000);
        end if;
      
      end loop;
    
      l_out_columns := rtrim(l_out_columns, ',') || ')';
    
      while dbms_sql.fetch_rows(l_curid) > 0
      loop
        l_out_values := 'VALUES (';
      
        for indx in 1 .. l_colcnt
        loop
          if l_desctab(indx).col_name = 'ID'
              and i_seq is not null
          then
            l_out_values := l_out_values || i_seq || '.NEXTVAL,';
          else
            if (l_desctab(indx).col_type in (dbms_types.typecode_varchar, dbms_types.typecode_varchar2, dbms_types.typecode_nvarchar2))
            then
              dbms_sql.column_value(l_curid, indx, l_namevar);
              l_out_values := l_out_values || case
                                when l_namevar is null then
                                 'null'
                                else
                                 'q''{' || l_namevar || '}'''
                              end || ',';
            elsif (l_desctab(indx).col_type = dbms_types.typecode_number)
            then
              dbms_sql.column_value(l_curid, indx, l_numvar);
              l_out_values := l_out_values || nvl(to_char(l_numvar), 'null') || ',';
            elsif (l_desctab(indx).col_type = dbms_types.typecode_date)
            then
              dbms_sql.column_value(l_curid, indx, l_datevar);
              l_out_values := l_out_values || case
                                when l_datevar is null then
                                 'null'
                                else
                                 'to_date(''' || to_char(l_datevar, 'YYYYMMDDHH24MISS') || ''',''YYYYMMDDHH24MISS'')'
                              end || ',';
            elsif (l_desctab(indx).col_type = dbms_types.typecode_clob)
            then
              dbms_sql.column_value(l_curid, indx, l_clobvar);
              l_out_values := l_out_values || case
                                when l_clobvar is null then
                                 'null'
                                else
                                 'q''{' || l_clobvar || '}'''
                              end || ',';
            end if;
          end if;
        end loop;
      
        pipe row(l_out_columns);
        pipe row(rtrim(l_out_values, ',') || ');');
      end loop;
    
      dbms_sql.close_cursor(l_curid);
    end loop;
  
    return;
  end gen_insert;

  function gen_create_market_script(i_market_id in number) return string_table
    pipelined is
  begin
    pipe row('SET DEFINE OFF');
    pipe row('WHENEVER SQLERROR EXIT ROLLBACK');
  
    for i in (select column_value from table(pkg_markets.gen_insert('MARKETS', 'ID', i_market_id, null)))
    loop
      pipe row(i.column_value);
    end loop;
  
    for i in (select column_value from table(pkg_markets.gen_insert('ASSET_INDEX', 'MARKET_ID', i_market_id, null)))
    loop
      pipe row(i.column_value);
    end loop;
  
    for i in (select column_value
              from   table(pkg_markets.gen_insert('OPPORTUNITY_TEMPLATES', 'MARKET_ID', i_market_id, 'SEQ_OPPORTUNITY_TEMPLATES')))
    loop
      pipe row(i.column_value);
    end loop;
  
    for i in (select column_value
              from   table(pkg_markets.gen_insert('SKIN_MARKET_GROUP_MARKETS', 'MARKET_ID', i_market_id, 'SEQ_SKIN_URL_COUNTRY_MAP')))
    loop
      pipe row(i.column_value);
    end loop;
  
    for i in (select column_value from table(pkg_markets.gen_insert('ASSET_INDEXS_INFO', 'MARKET_ID', i_market_id, 'SEQ_ASSET_INDEXS_INFO')))
    loop
      pipe row(i.column_value);
    end loop;
  
    for i in (select column_value
              from   table(pkg_markets.gen_insert('MARKET_DATA_SOURCE_MAPS', 'MARKET_ID', i_market_id, 'SEQ_MARKET_DATA_SOURCE_MAPS')))
    loop
      pipe row(i.column_value);
    end loop;
  
    for i in (select column_value from table(pkg_markets.gen_insert('MARKET_NAME_SKIN', 'MARKET_ID', i_market_id, 'SEQ_MARKET_NAME_SKIN')))
    loop
      pipe row(i.column_value);
    end loop;
  
    pipe row('--- create opportunities from tomorrow.');
    pipe row('EXEC pkg_markets.create_opportunities(' || i_market_id || ')');
    pipe row('COMMIT;');
  
    return;
  end gen_create_market_script;

  procedure get_market_formulas
  (
    o_data                  out sys_refcursor
   ,i_market_id             in number
   ,i_market_data_source_id in number
  ) is
  begin
    open o_data for
      select id
            ,market_id
            ,market_data_source_id
            ,hour_level_formula_id
            ,hour_level_formula_params
            ,hour_closing_formula_id
            ,hour_closing_formula_params
            ,day_closing_formula_id
            ,day_closing_formula_params
            ,long_term_formula_id
            ,long_term_formula_params
            ,real_level_formula_id
            ,real_level_formula_params
            ,market_params
            ,market_disable_conditions
      from   market_data_source_maps
      where  market_id = i_market_id
      and    market_data_source_id = i_market_data_source_id;
  end get_market_formulas;

  procedure update_market_formulas
  (
    i_market_id                   in number
   ,i_market_data_source_id       in number
   ,i_hour_level_formula_params   in varchar2
   ,i_hour_closing_formula_params in varchar2
   ,i_day_closing_formula_params  in varchar2
   ,i_long_term_formula_params    in varchar2
   ,i_real_level_formula_params   in varchar2
   ,i_market_params               in varchar2
   ,i_market_disable_conditions   in varchar2
  ) is
  begin
    update market_data_source_maps
    set    hour_level_formula_params   = i_hour_level_formula_params
          ,hour_closing_formula_params = i_hour_closing_formula_params
          ,day_closing_formula_params  = i_day_closing_formula_params
          ,long_term_formula_params    = i_long_term_formula_params
          ,real_level_formula_params   = i_real_level_formula_params
          ,market_params               = i_market_params
          ,market_disable_conditions   = i_market_disable_conditions
    where  market_id = i_market_id
    and    market_data_source_id = i_market_data_source_id;
  
    if sql%notfound
    then
      raise_application_error(-20001
                             ,'No rows updated! i_market_id:' || i_market_id || '; i_market_data_source_id:' || i_market_data_source_id);
    end if;
  end update_market_formulas;

  procedure get_market_translations
  (
    o_data      out sys_refcursor
   ,i_market_id in number
  ) is
  begin
    open o_data for
      select *
      from   (select i.additional_text
                    ,i.market_description
                    ,i.page_description
                    ,i.page_keywords
                    ,i.page_title
                    ,i.market_description_page
                    ,s.default_language_id
                    ,ns.name
                    ,ns.short_name
                    ,row_number() over(partition by s.default_language_id order by i.skin_id) rn
              from   asset_indexs_info i
              join   market_name_skin ns
              on     ns.market_id = i.market_id
              and    ns.skin_id = i.skin_id
              join   skins s
              on     s.id = i.skin_id
              where  i.market_id = i_market_id)
      where  rn = 1;
  
  end get_market_translations;

  procedure update_market_translations
  (
    i_market_id               in number
   ,i_language_id             in number
   ,i_name                    in varchar2
   ,i_short_name              in varchar2
   ,i_additional_text         in varchar2
   ,i_market_description      in clob
   ,i_page_description        in varchar2
   ,i_page_keywords           in varchar2
   ,i_page_title              in varchar2
   ,i_market_description_page in varchar2
  ) is
  begin
    update asset_indexs_info a
    set    a.additional_text         = i_additional_text
          ,a.market_description      = i_market_description
          ,a.page_description        = i_page_description
          ,a.page_keywords           = i_page_keywords
          ,a.page_title              = i_page_title
          ,a.market_description_page = i_market_description_page
    where  a.market_id = i_market_id
    and    a.skin_id in (select s.id from skins s where s.default_language_id = i_language_id);
  
    update market_name_skin a
    set    a.name = i_name, a.short_name = i_short_name
    where  a.market_id = i_market_id
    and    a.skin_id in (select s.id from skins s where s.default_language_id = i_language_id);
  
  end update_market_translations;

  function checkfor_dopp
  (
    i_market_id in number
   ,i_days      in number
  ) return boolean is
    l_count   number;
    l_sysdate date := sysdate;
    l_ts_from timestamp := trunc(l_sysdate) + i_days;
    l_ts_to   timestamp := l_ts_from + numtodsinterval(1, 'DAY');
  begin
    select count(*)
    into   l_count
    from   opportunities o
    where  o.market_id = i_market_id
    and    o.scheduled in (1, 2)
    and    o.time_first_invest >= l_ts_from
    and    o.time_first_invest < l_ts_to;
  
    if l_count > 0
    then
      return false;
    else
      return true;
    end if;
  end checkfor_dopp;

  function checkfor_wopp(i_market_id in number) return boolean is
    l_count    number;
    l_sysdate  date := sysdate;
    l_nextsun1 timestamp := next_day(trunc(l_sysdate), 'SUNDAY');
    l_nextsun2 timestamp := next_day(l_nextsun1, 'SUNDAY');
  begin
    select count(*)
    into   l_count
    from   opportunities o
    where  o.market_id = i_market_id
    and    o.scheduled = 3
    and    o.time_first_invest >= l_nextsun1
    and    o.time_first_invest < l_nextsun2;
  
    if l_count > 0
    then
      return false;
    else
      return true;
    end if;
  end checkfor_wopp;

  function checkfor_mopp(i_market_id in number) return boolean is
    l_count    number;
    l_sysdate  date := sysdate;
    l_nextmon1 timestamp := add_months(trunc(l_sysdate, 'MM'), 1);
    l_nextmon2 timestamp := l_nextmon1 + numtoyminterval(1, 'MONTH');
  begin
    select count(*)
    into   l_count
    from   opportunities o
    where  o.market_id = i_market_id
    and    o.scheduled = 4
    and    o.time_first_invest >= l_nextmon1
    and    o.time_first_invest < l_nextmon2;
  
    if l_count > 0
    then
      return false;
    else
      return true;
    end if;
  end checkfor_mopp;

  procedure create_opportunities(i_market_id in number) is
  begin
  
    for i in 1 .. 5
    loop
      if checkfor_dopp(i_market_id, i)
      then
        opportunities_manager.create_daily_opportunities(i, i_market_id);
      end if;
    end loop;
  
    if checkfor_wopp(i_market_id)
    then
      opportunities_manager.create_week_opportunities(p_flag => 1, p_market => i_market_id);
    end if;
  
    if checkfor_mopp(i_market_id)
    then
      opportunities_manager.create_month_opportunities(p_flag => 1, p_market => i_market_id);
    end if;
  
  end create_opportunities;

end pkg_markets;
/
