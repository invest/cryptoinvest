create or replace package pkg_apiusers is

  -- Author  : VICTOR.SLAVOV
  -- Created : 2016-08-02 16:07:27
  -- Purpose : ApiDAOBase

  procedure get_ftd_users_bydate
  (
    o_data        out sys_refcursor
   ,i_apiuser_id  in number
   ,i_date_string in varchar2
  );

end pkg_apiusers;
/
create or replace package body pkg_apiusers is

  procedure get_ftd_users_bydate
  (
    o_data        out sys_refcursor
   ,i_apiuser_id  in number
   ,i_date_string in varchar2
  ) is
    l_date date := to_date(i_date_string, 'yyyy-mm-dd');
  begin
    open o_data for
      select t.id transaction_id
            ,t.amount * t.rate usd_amount
            ,t.user_id
            ,u.dynamic_param dynamic_parameter
            ,u.sub_affiliate_key sub_affiliate_id
            ,t.time_created
      from   api_users au
      join   api_user_activity aua
      on     au.id = aua.api_user_id
      join   users u
      on     u.id = aua.key_value
      join   transactions t
      on     t.id = u.first_deposit_id
      where  aua.type_id = 2
      and    au.id = i_apiuser_id
      and    u.is_active = 1
      and    u.class_id <> 0
      and    trunc(t.time_created) = l_date
      and    l_date > sysdate - 14;
  
  end get_ftd_users_bydate;

end pkg_apiusers;
/
