create or replace package pkg_withdraw_backend is

  -- Author  : VICTOR.SLAVOV
  -- Created : 2016-07-21 14:29:12
  -- Purpose : create withdraw transactions from backend; il.co.etrader.backend.bl_managers.TransactionsManager

  -- create withdraw for german payment methods
  procedure create_withdraw
  (
    i_user_id          in number
   ,i_amount           in number
   ,i_writer_id        in number
   ,i_withdraw_type_id in number
   ,i_comments         in varchar2
   ,i_rate             in number
  );
  
  PROCEDURE CREATE_GM_WITHDRAW(I_USER_ID          IN NUMBER,
                               I_AMOUNT           IN NUMBER,
                               I_WRITER_ID        IN NUMBER,
                               I_WITHDRAW_TYPE_ID IN NUMBER, --gmType
                               I_RATE             IN NUMBER,
                               I_BENEFICIARY_NAME IN VARCHAR2,
                               I_SWIFT_BIC_CODE   IN VARCHAR2,
                               I_IBAN             IN VARCHAR2,
                               I_ACCOUNT_NUMBER   IN NUMBER,
                               I_BANKNAME         IN VARCHAR2,
                               I_BRANCH_NUMBER    IN NUMBER,
                               I_BRANCH_ADDRESS   IN VARCHAR2,
                               I_ACCOUNT_INFO     IN VARCHAR2,
                               I_TRN_STATUS_ID    IN NUMBER);

end pkg_withdraw_backend;
/
create or replace package body pkg_withdraw_backend is

  procedure create_withdraw
  (
    i_user_id          in number
   ,i_amount           in number
   ,i_writer_id        in number
   ,i_withdraw_type_id in number
   ,i_comments         in varchar2
   ,i_rate             in number
  ) is
    l_trn              transactions%rowtype;
    l_sysdate          date := sysdate;
    l_user_utc_offset  users.utc_offset%type;
    l_user_balance     number;
    l_new_user_balance number;
    l_tax_balance      number;
  begin
    select u.utc_offset, u.balance, u.tax_balance
    into   l_user_utc_offset, l_user_balance, l_tax_balance
    from   users u
    where  u.id = i_user_id
    for    update;
  
    if l_user_balance < i_amount
    then
      raise_application_error(-20000, 'User balance is less then the withraw amount: ' || l_user_balance || ' < ' || i_amount);
    end if;
  
    update users u
    set    balance = balance - i_amount, u.time_modified = l_sysdate, u.utc_offset_modified = u.utc_offset
    where  u.id = i_user_id
    returning balance into l_new_user_balance;
  
    l_trn.user_id                := i_user_id;
    l_trn.type_id                := i_withdraw_type_id;
    l_trn.amount                 := i_amount;
    l_trn.status_id              := 2; -- select id from transaction_statuses where code = 'TRANS_STATUS_SUCCEED'
    l_trn.writer_id              := i_writer_id;
    l_trn.time_settled           := l_sysdate;
    l_trn.comments               := i_comments;
    l_trn.processed_writer_id    := i_writer_id;
    l_trn.time_created           := l_sysdate;
    l_trn.utc_offset_created     := l_user_utc_offset;
    l_trn.utc_offset_settled     := l_user_utc_offset;
    l_trn.is_accounting_approved := 0;
    l_trn.rate                   := i_rate;
    l_trn.ip                     := 'IP NOT FOUND!';
  
    insert into transactions t
      (id
      ,user_id
      ,type_id
      ,amount
      ,status_id
      ,writer_id
      ,time_settled
      ,comments
      ,processed_writer_id
      ,time_created
      ,utc_offset_created
      ,utc_offset_settled
      ,is_accounting_approved
      ,rate
      ,ip)
    values
      (seq_transactions.nextval
      ,l_trn.user_id
      ,l_trn.type_id
      ,l_trn.amount
      ,l_trn.status_id
      ,l_trn.writer_id
      ,l_trn.time_settled
      ,l_trn.comments
      ,l_trn.processed_writer_id
      ,l_trn.time_created
      ,l_trn.utc_offset_created
      ,l_trn.utc_offset_settled
      ,l_trn.is_accounting_approved
      ,l_trn.rate
      ,l_trn.ip) return id into l_trn.id;
  
    -- command = 4 - select * from enumerators where enumerator = 'log_balance' and to_number(code) = 4;
    insert into balance_history
      (id, time_created, user_id, balance, tax_balance, writer_id, table_name, key_value, command, utc_offset)
    values
      (seq_balance_history.nextval
      ,l_sysdate
      ,i_user_id
      ,l_new_user_balance
      ,l_tax_balance
      ,i_writer_id
      ,'transactions'
      ,l_trn.id
      ,4
      ,l_user_utc_offset);
  end create_withdraw;


PROCEDURE CREATE_GM_WITHDRAW(I_USER_ID          IN NUMBER,
                             I_AMOUNT           IN NUMBER,
                             I_WRITER_ID        IN NUMBER,
                             I_WITHDRAW_TYPE_ID IN NUMBER, --gmType
                             I_RATE             IN NUMBER,
                             I_BENEFICIARY_NAME IN VARCHAR2,
                             I_SWIFT_BIC_CODE   IN VARCHAR2,
                             I_IBAN             IN VARCHAR2,
                             I_ACCOUNT_NUMBER   IN NUMBER,
                             I_BANKNAME         IN VARCHAR2,
                             I_BRANCH_NUMBER    IN NUMBER,
                             I_BRANCH_ADDRESS   IN VARCHAR2,
                             I_ACCOUNT_INFO     IN VARCHAR2,
                             I_TRN_STATUS_ID    IN NUMBER) IS

  L_TRN             TRANSACTIONS%ROWTYPE;
  L_WIRES           WIRES%ROWTYPE;
  L_SYSDATE         DATE := SYSDATE;
  L_USER_UTC_OFFSET USERS.UTC_OFFSET%TYPE;

BEGIN

  L_WIRES.BRANCH           := I_BRANCH_NUMBER;
  L_WIRES.ACCOUNT_NUM      := I_ACCOUNT_NUMBER;
  L_WIRES.SWIFT            := I_SWIFT_BIC_CODE;
  L_WIRES.BENEFICIARY_NAME := I_BENEFICIARY_NAME;
  L_WIRES.BANK_NAME        := I_BANKNAME;
  L_WIRES.IBAN             := I_IBAN;
  L_WIRES.BRANCH_ADDRESS   := I_BRANCH_ADDRESS;
  L_WIRES.ACCOUNT_INFO     := I_ACCOUNT_INFO;

  INSERT INTO WIRES
    (ID,
     --BANK_ID,?
     BRANCH,
     ACCOUNT_NUM,
     SWIFT,
     BENEFICIARY_NAME,
     BANK_NAME,
     IBAN,
     BRANCH_ADDRESS,
     ACCOUNT_INFO)
  VALUES
    (SEQ_WIRES.NEXTVAL,
     --V_BANK_ID, ?
     L_WIRES.BRANCH,
     L_WIRES.ACCOUNT_NUM,
     L_WIRES.SWIFT,
     L_WIRES.BENEFICIARY_NAME,
     L_WIRES.BANK_NAME,
     L_WIRES.IBAN,
     L_WIRES.BRANCH_ADDRESS,
     L_WIRES.ACCOUNT_INFO) RETURN ID INTO L_WIRES.ID;

  SELECT U.UTC_OFFSET
    INTO L_USER_UTC_OFFSET
    FROM USERS U
   WHERE U.ID = I_USER_ID;

  L_TRN.USER_ID                := I_USER_ID;
  L_TRN.TYPE_ID                := I_WITHDRAW_TYPE_ID;
  L_TRN.AMOUNT                 := I_AMOUNT;
  L_TRN.STATUS_ID              := I_TRN_STATUS_ID;
  L_TRN.WRITER_ID              := I_WRITER_ID;
  L_TRN.TIME_SETTLED           := L_SYSDATE;
  L_TRN.PROCESSED_WRITER_ID    := I_WRITER_ID;
  L_TRN.TIME_CREATED           := L_SYSDATE;
  L_TRN.UTC_OFFSET_CREATED     := L_USER_UTC_OFFSET;
  L_TRN.UTC_OFFSET_SETTLED     := L_USER_UTC_OFFSET;
  L_TRN.IS_ACCOUNTING_APPROVED := 0;
  L_TRN.RATE                   := I_RATE;
  L_TRN.IP                     := 'IP NOT FOUND!';
  L_TRN.WIRE_ID                := L_WIRES.ID;

  INSERT INTO TRANSACTIONS T
    (ID,
     USER_ID,
     TYPE_ID,
     AMOUNT,
     STATUS_ID,
     WRITER_ID,
     TIME_SETTLED,
     PROCESSED_WRITER_ID,
     TIME_CREATED,
     UTC_OFFSET_CREATED,
     UTC_OFFSET_SETTLED,
     IS_ACCOUNTING_APPROVED,
     RATE,
     IP,
     WIRE_ID)
  VALUES
    (SEQ_TRANSACTIONS.NEXTVAL,
     L_TRN.USER_ID,
     L_TRN.TYPE_ID,
     L_TRN.AMOUNT,
     L_TRN.STATUS_ID,
     L_TRN.WRITER_ID,
     L_TRN.TIME_SETTLED,
     L_TRN.PROCESSED_WRITER_ID,
     L_TRN.TIME_CREATED,
     L_TRN.UTC_OFFSET_CREATED,
     L_TRN.UTC_OFFSET_SETTLED,
     L_TRN.IS_ACCOUNTING_APPROVED,
     L_TRN.RATE,
     L_TRN.IP,
     L_TRN.WIRE_ID) RETURN ID INTO L_TRN.ID;

END CREATE_GM_WITHDRAW;

end pkg_withdraw_backend;
/
