
create materialized view issue_65
  build deferred
refresh force on demand 
as
select * from (
select t.user_id,t.user_name,t.email,t.is_contact_by_email,t.fee from
(select u.id user_id,
        u.user_name user_name,
        u.email,
        u.is_contact_by_email,
        f.amount fee, ia.issue_action_type_id, rank() over (partition by i.user_id order by ia.action_time desc) rnk
from users u, issues i,issue_actions ia, fee_currency_map f
 where u.is_active = 0
 and f.fee_id = 1
 and u.currency_id = f.currency_id
 and u.balance > 0
 and u.class_id <> 0
 and u.time_created < add_months(trunc(sysdate), -6) + 7
 and i.user_id = u.id
 and ia.issue_id = i.id
 and ia.issue_action_type_id in (12,39)
) t
where t.rnk = 1 and t.issue_action_type_id = 39
                                     union all
select u.id user_id,
                  u.user_name user_name,
                  u.email,
                  is_contact_by_email,
                  f.amount fee
             from users u, fee_currency_map f
            where     u.class_id <> 0
                  and u.is_active = 1
                  and u.balance > 0
                  and f.fee_id = 1
                  and u.time_created < add_months(trunc(sysdate), -6) + 7
                  and u.currency_id = f.currency_id) z                  
                  where not exists
                             (select 1
                                from investments i
                               where     i.time_created >= add_months(sysdate, -6) + 7
                                     and z.user_id = i.user_id)
                  and not exists
                             (select 1
                                from issues iss
                               where     iss.time_created >= add_months(sysdate, -1)
                                     and z.user_id = iss.user_id
                                     and iss.subject_id = 65);

comment on materialized view issue_65  is 'snapshot table for snapshot ETRADER.ISSUE_65';
