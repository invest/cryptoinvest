

-------------------------------------------------------------------------------------------------------
--LioR SoLoMoN
--BAC-2135
--Change expected provider for withdrawls from approval screen

ALTER TABLE CLEARING_PROVIDERS ADD (CC_AVAILABLE NUMBER(1, 0) DEFAULT 0 NOT NULL);

COMMENT ON COLUMN CLEARING_PROVIDERS.CC_AVAILABLE IS '1 - If provider support Credit Card clearing ';

update CLEARING_PROVIDERS set CC_AVAILABLE = 1 where id in (3,4,15,23,33,37,41,43,44,45,46,47,50,51,52);
commit;
-------------------------------------------------------------------------------------------------------

    
----- victor; CORE-3509 Charge Maintenance Fee every month After 6 Months of No Activity
begin
  pkg_release.ddl_helper('create index idx_users_active on users (is_active, balance) compress online', -955);
end;
/

begin
  pkg_release.ddl_helper('drop index users_idx_is_active', -1418);
end;
/

begin
  dbms_refresh.destroy('maintanance_fee_rg');
exception
  when others then
    if sqlcode = -23404
    then
      null;
    else
      raise;
    end if;
end;
/

commit;

drop materialized view issue_64;
drop materialized view issue_65;
drop materialized view m_fee;
drop materialized view tournament_users_rank;

@materialized_views/ISSUE_64.sql
@materialized_views/ISSUE_65.sql
@materialized_views/M_FEE.sql

@views/tournament_users_rank.sql 

create index idx_tournament_users on tournament_users (tournament_id, user_id) online;

grant select on tournament_users_rank to etrader_web_connect;
create or replace public synonym tournament_users_rank for tournament_users_rank;

begin
  dbms_refresh.make(name                 => 'maintanance_fee_rg'
                   ,list                 => 'issue_64, issue_65, m_fee'
                   ,next_date            => trunc(sysdate) + 1 + 2/24
                   ,interval             => 'trunc(sysdate) + 1 + 2/24'
                   ,implicit_destroy     => false
                   ,lax                  => false
                   ,job                  => 0
                   ,rollback_seg         => null
                   ,push_deferred_rpc    => false
                   ,refresh_after_errors => true
                   ,purge_option         => null
                   ,parallelism          => null
                   ,heap_size            => null);
end;
/

commit;

-- Mat. views are created with defered refresh! 
-- The refresh group can be forcefully refreshed with  begin dbms_refresh.refresh('etrader.maintanance_fee_rg'); commit; end;

@packages/transactions_manager.pks

begin
  pkg_release.ddl_helper('grant select on m_fee to ao_readonly_role', -1917);
  pkg_release.ddl_helper('grant select on issue_64 to ao_readonly_role', -1917);
  pkg_release.ddl_helper('grant select on issue_65 to ao_readonly_role', -1917);
  pkg_release.ddl_helper('grant select on m_fee to etrader_dev', -1917);
  pkg_release.ddl_helper('grant select on issue_64 to "AO_Ilanit"', -1917);
  pkg_release.ddl_helper('grant select on issue_65 to "AO_Ilanit"', -1917);
end;
/


--- internal development; fix index names; 
begin
  pkg_release.ddl_helper('alter index idx_opp_time_act_closing rename to idx_opp_timeactclosing', -1418);
end;
/

begin
  pkg_release.ddl_helper('alter index idx_opp_time_act_closing rename to idx_opp_timeactclosing', -1418);
end;
/

begin
  pkg_release.ddl_helper('create index idx_opp_timeestclosing on opportunities (time_est_closing) compress online', -955);
end;
/

begin
  pkg_release.ddl_helper('drop index index_opportunities_tec', -1418);
end;
/

begin
  pkg_release.ddl_helper('create index idx_opp_timelastinvest on opportunities (time_last_invest) compress online', -955);
end;
/

----- END; victor; CORE-3509 Charge Maintenance Fee every month After 6 Months of No Activity
-------------------------------------------------------------------------------------------------------------------------------------------
-- CORE-3461 Digits After Decimal Point Should Be Unified for the Same Asset
-- Kiril
-------------------------------------------------------------------------------------------------------------------------------------------
update markets set decimal_point_subtract_digits = 0 where id in (699,700,701,706,707,720,721,722,723,724);
update markets set decimal_point = 6 where id = 700;
update markets set decimal_point = 4 where id = 707;
update markets set decimal_point = 6 where id = 706;
update markets set decimal_point = 6 where id = 721;
update markets set decimal_point = 6 where id = 723;
update markets set decimal_point = 6 where id = 722;
update markets set decimal_point = 6 where id = 724;
update markets set decimal_point = 4 where id = 720;
commit;
-------------------------------------------------------------------------------------------------------------------------------------------
-- End
-------------------------------------------------------------------------------------------------------------------------------------------

-----------------------------------------------
-- Start Pavel Tabakov BAC-2136
-----------------------------------------------
INSERT INTO TRANSACTION_TYPES
  (ID, CODE, DESCRIPTION, CLASS_TYPE, IS_RISKY, IS_DISPLAYED, IS_ACTIVE)
VALUES
  (59, 'DIRECT24_WITHDRAW', 'transactions.direct24.withdraw', 2, 0, 1, 1);

INSERT INTO TRANSACTION_TYPES
  (ID, CODE, DESCRIPTION, CLASS_TYPE, IS_RISKY, IS_DISPLAYED, IS_ACTIVE)
VALUES
  (60, 'GIROPAY_WITHDRAW', 'transactions.giropay.withdraw', 2, 0, 1, 1);

INSERT INTO TRANSACTION_TYPES
  (ID, CODE, DESCRIPTION, CLASS_TYPE, IS_RISKY, IS_DISPLAYED, IS_ACTIVE)
VALUES
  (61, 'EPS_WITHDRAW', 'transactions.eps.withdraw', 2, 0, 1, 1);
commit;
-----------------------------------------------
-- End Pavel Tabakov BAC-2136
-----------------------------------------------

-----------------------------------------------
-- Start Mariya Lazarova BAC-2209
-----------------------------------------------
update PAYMENT_METHODS_COUNTRIES set EXCLUDE_IND = 1 where PAYMENT_ID in (26,39) and EXCLUDE_IND = 0;
commit;
-----------------------------------------------
-- End Mariya Lazarova BAC-2209
-----------------------------------------------

----------------------------------------------------------------------------------------------------------------------
-- START Ivan Petkov CORE-3458 Bubbles Investments Web + App Are Missing Login_id and writer is always the same
----------------------------------------------------------------------------------------------------------------------
--
ALTER table BUBBLES_TOKEN ADD (LOGIN_ID NUMBER, WRITER_ID NUMBER);
--
update writers set user_name='BUBBLES_WEB' where id = 21000;
--
insert into writers
  (id
  ,user_name
  ,password
  ,first_name
  ,last_name
  ,street
  ,city_id
  ,zip_code
  ,email
  ,comments
  ,time_birth_date
  ,mobile_phone
  ,land_line_phone
  ,is_active
  ,street_no
  ,utc_offset
  ,group_id
  ,is_support_enable
  ,dept_id
  ,sales_type
  ,nick_name_first
  ,nick_name_last
  ,auto_assign
  ,cms_login
  ,last_failed_time
  ,failed_count
  ,sales_type_dept_id
  ,emp_id
  ,role_id
  ,time_created)
values
  (25000
  ,'BUBBLES_MOBILE'
  ,'Bubbles$$$'
  ,'Bubbles'
  ,'Bubbles'
  ,null
  ,null
  ,null
  ,'na'
  ,null
  ,sysdate
  ,null
  ,null
  ,1
  ,null
  ,'GMT+02:00'
  ,0
  ,1
  ,9
  ,null
  ,null
  ,null
  ,0
  ,null
  ,null
  ,0
  ,null
  ,null
  ,null
  ,sysdate);
commit;
----------------------------------------------------------------------------------------------------------------------
-- END Ivan Petkov CORE-3458
----------------------------------------------------------------------------------------------------------------------


---- victor; created index on COPYOP.copyop_frozen to speed up "unlinking"
begin
  pkg_release.ddl_helper('create unique index copyop.uk_copyop_frozen on copyop.copyop_frozen (user_id) online', -955);
end;
/

begin
  pkg_release.ddl_helper('alter table copyop.copyop_frozen add constraint uk_copyop_frozen unique (user_id)', -2261);
end;
/
-- END;

---- victor; CORE-3264 SQL cause high load in DB. sql_id f904szb0w0mpq
@packages/pkg_shoppingbag.pck

grant execute on pkg_shoppingbag to etrader_web_connect;
create or replace public synonym pkg_shoppingbag for pkg_shoppingbag;
---- END; victor; CORE-3264 SQL cause high load in DB. sql_id f904szb0w0mpq

-----------------------------------------------
-- Start Pavel Tabakov BAC-BAC-2085
-----------------------------------------------
INSERT INTO ISSUE_ACTION_WRITER_PERMISSION
  (WRITER_ID, ISSUE_ACTION_TYPE_ID, IS_ACTIVE, TIME_CREATED)
VALUES
  (282, 48, 1, SYSDATE);

INSERT INTO ISSUE_ACTION_WRITER_PERMISSION
  (WRITER_ID, ISSUE_ACTION_TYPE_ID, IS_ACTIVE, TIME_CREATED)
VALUES
  (1444, 48, 1, SYSDATE);

INSERT INTO ISSUE_ACTION_WRITER_PERMISSION
  (WRITER_ID, ISSUE_ACTION_TYPE_ID, IS_ACTIVE, TIME_CREATED)
VALUES
  (659, 48, 1, SYSDATE);

INSERT INTO ISSUE_ACTION_WRITER_PERMISSION
  (WRITER_ID, ISSUE_ACTION_TYPE_ID, IS_ACTIVE, TIME_CREATED)
VALUES
  (728, 48, 1, SYSDATE);
  
commit;  
  -----------------------------------------------
-- End Pavel Tabakov BAC-BAC-2085
-----------------------------------------------


---- victor; 1. CORE-3570; enable dynamics for more skins
@triggers/silverpop_users.trg

begin
  for i in (select a.user_id
            from   users u
            join   users_active_data a
            on     a.user_id = u.id
            where  skin_id in (3, 10, 16, 18, 19, 20, 21, 23, 24)
            and    a.has_dynamics = 0
            order  by u.id)
  loop
    update users_active_data
    set    has_dynamics = 1
    where  has_dynamics = 0
    and    user_id = i.user_id;
    commit;
  end loop;
end;
/

begin
  for i in (select a.user_id
            from   users u
            join   users_active_data a
            on     a.user_id = u.id
            where  u.skin_id in (15)
            and    a.has_bubbles = 0
            order  by u.id)
  loop
    update users_active_data
    set    has_bubbles = 1
    where  has_bubbles = 0
    and    user_id = i.user_id;
    
    commit;
  end loop;
end;
/

---- END;  victor; 1. CORE-3570; enable dynamics for more skins


-- added on prod; requested by Dzhamal
alter table terms_files modify (title varchar2(200 char));


--------------------------------------------------------------------------------------------------------
--LioR SoLoMoN.
--BAC-2111
--Update phone_code for country=Anguilla

update COUNTRIES set PHONE_CODE = 1264 where COUNTRY_NAME = 'Anguilla';
commit;
--No need changes for old users - it's from the country.

--------------------------------------------------------------------------------------------------------

--LioR SoLoMoN.
--BAC-2067
--Montenegro - change country phone code

update COUNTRIES set PHONE_CODE = 382 where COUNTRY_NAME = 'Montenegro';
commit;
--No need changes for old users - it's from the country.

--------------------------------------------------------------------------------------------------------

----------------------------------------------------------------------------------------
-- [AR-2263] [server] - Create job that will fire pixel to Adquant
-- Eyal O
----------------------------------------------------------------------------------------

-- SERVER_PIXELS_PUBLISHERS
INSERT
  INTO SERVER_PIXELS_PUBLISHERS
    (
      TIME_CREATED ,
      NAME ,
      ID ,
      WRITER_ID
    )
    VALUES
    (
      sysdate ,
      'Adquant' ,
      3 ,
      527
    );
commit;
    
-- SERVER_PIXELS_PUBLISHER_CONFIG
ALTER TABLE SERVER_PIXELS_PUBLISHER_CONFIG ADD (PLATFORM_ID NUMBER );

COMMENT ON COLUMN SERVER_PIXELS_PUBLISHER_CONFIG.PLATFORM_ID IS 'Foreign key to ''PLATFORMS''';
    
insert INTO SERVER_PIXELS_PUBLISHER_CONFIG
    (
      TIME_CREATED ,
      PIXEL_URL ,
      SERVER_PIXELS_TYPE_ID ,
      ID ,
      WRITER_ID ,
      HANDLER_CLASS ,
      SERVER_PIXELS_PUBLISHER_ID ,
      HANDLER_CLASS_PARAMS,
      PLATFORM_ID
    )
    VALUES
    (
      sysdate ,
      'http://s.mb4ao.com/cb/8-711612_10-3_7m7?order_id=$orderId' ,
      4 ,
      seq_SERVER_PIXELS_PUBLISHER_CO.nextVal,
      527 ,
      'com.anyoption.common.server.pixel.ServerPixelHandlerRegister' ,
      3 ,
      '{}',
      2
    );
    
insert INTO SERVER_PIXELS_PUBLISHER_CONFIG
    (
      TIME_CREATED ,
      PIXEL_URL ,
      SERVER_PIXELS_TYPE_ID ,
      ID ,
      WRITER_ID ,
      HANDLER_CLASS ,
      SERVER_PIXELS_PUBLISHER_ID ,
      HANDLER_CLASS_PARAMS,
      PLATFORM_ID
    )
    VALUES
    (
      sysdate ,
      'http://s.mb4ao.com/cb/8-711860_10-3_jbm?order_id=$orderId' ,
      1 ,
      seq_SERVER_PIXELS_PUBLISHER_CO.nextVal,
      527 ,
      'com.anyoption.common.server.pixel.ServerPixelHandlerFTD' ,
      3 ,
      '{}',
      2
    );
    
insert INTO SERVER_PIXELS_PUBLISHER_CONFIG
    (
      TIME_CREATED ,
      PIXEL_URL ,
      SERVER_PIXELS_TYPE_ID ,
      ID ,
      WRITER_ID ,
      HANDLER_CLASS ,
      SERVER_PIXELS_PUBLISHER_ID ,
      HANDLER_CLASS_PARAMS,
      PLATFORM_ID
    )
    VALUES
    (
      sysdate ,
      'http://s.mb4ao.com/cb/8-792576_10-3_orx?order_id=$orderId' ,
      4 ,
      seq_SERVER_PIXELS_PUBLISHER_CO.nextVal,
      527 ,
      'com.anyoption.common.server.pixel.ServerPixelHandlerRegister' ,
      3 ,
      '{}',
      3
    );
    
insert INTO SERVER_PIXELS_PUBLISHER_CONFIG
    (
      TIME_CREATED ,
      PIXEL_URL ,
      SERVER_PIXELS_TYPE_ID ,
      ID ,
      WRITER_ID ,
      HANDLER_CLASS ,
      SERVER_PIXELS_PUBLISHER_ID ,
      HANDLER_CLASS_PARAMS,
      PLATFORM_ID
    )
    VALUES
    (
      sysdate ,
      'http://s.mb4ao.com/cb/8-792578_10-3_jt4?order_id=$orderId' ,
      1 ,
      seq_SERVER_PIXELS_PUBLISHER_CO.nextVal,
      527 ,
      'com.anyoption.common.server.pixel.ServerPixelHandlerFTD' ,
      3 ,
      '{}',
      3
    );
commit;
    
-- DEVICE_UNIQUE_IDS_SKIN
ALTER TABLE DEVICE_UNIQUE_IDS_SKIN ADD (IDFA VARCHAR2(200), ADVERTISING_ID VARCHAR2(200));

COMMENT ON COLUMN DEVICE_UNIQUE_IDS_SKIN.IDFA IS 'The Advertising Identifier (IDFA) is a unique ID for each iOS device that mobile ad networks typically use to serve targeted ads. Users can choose to limit ad tracking by turning off this setting on their devices.';

COMMENT ON COLUMN DEVICE_UNIQUE_IDS_SKIN.ADVERTISING_ID IS 'The advertising ID is a unique, user-resettable ID for advertising, provided by Google Play services.';

-- DB_PARAMETERS
INSERT
INTO DB_PARAMETERS
  (
    ID,
    NAME,
    NUM_VALUE,
    STRING_VALUE,
    DATE_VALUE,
    COMMENTS
  )
  VALUES
  (
    16,
    'last_fire_pixel_run',
    null,
    null,
    sysdate,
    'The job will handle all actions by DATE_VALUE to avoid getting duplicate actions.'
  );
  
-- JOBS
INSERT INTO jobs (ID, RUNNING, LAST_RUN_TIME, SERVER, RUN_INTERVAL, DESCRIPTION, JOB_CLASS, CONFIG, RUN_SERVERS, RUN_APPLICATIONS)
VALUES (37, 0, to_date(to_char(sysdate, 'dd-mm-yyyy')||' 00:10:00','dd-mm-yyyy hh24:mi:ss'), 'BE02', 60, 'fire server pixel', 'il.co.etrader.jobs.FireServerPixelsJob', '', 'BE02', 'backend');

commit;

begin
  pkg_release.ddl_helper('alter table marketing_sources add constraint uk_marketing_sources unique (name)', -2261);
end;
/
begin
  pkg_release.ddl_helper('create index idx_fk_mcomp_msrc on marketing_campaigns (source_id)', -955);
end;
/
begin
  pkg_release.ddl_helper('create index idx_duis_dev_uk_id on device_unique_ids_skin (device_unique_id) online', -1408);
end;
/

@packages/pkg_server_pixels.pck

grant execute on pkg_server_pixels to etrader_web_connect;
create or replace public synonym pkg_server_pixels for pkg_server_pixels;

----------------------------------------------------------------------------------------
-- END
-- [AR-2263] [server] - Create job that will fire pixel to Adquant
-- Eyal O
----------------------------------------------------------------------------------------

----------------------------------------
--Pavel Tabakov BAC-2234
----------------------------------------

      UPDATE CLEARING_PROVIDERS CP
         SET CP.NAME = 'Wirecard'
       WHERE CP.ID = 3;
      
      UPDATE CLEARING_PROVIDERS CP
         SET CP.NAME = 'Wirecard 3D'
       WHERE CP.ID = 4;
      
      UPDATE CLEARING_PROVIDERS CP
         SET CP.NAME = 'AOPS Inatec TRY'
       WHERE CP.ID = 15;
      
      UPDATE CLEARING_PROVIDERS CP
         SET CP.NAME = 'AOPS Transact'
       WHERE CP.ID = 23;
      
      UPDATE CLEARING_PROVIDERS CP
         SET CP.NAME = 'OB Transact'
       WHERE CP.ID = 33;
      
      UPDATE CLEARING_PROVIDERS CP
         SET CP.NAME = 'AOPS Inatec PC21'
       WHERE CP.ID = 37;
      
      UPDATE CLEARING_PROVIDERS CP
         SET CP.NAME = 'OB Inatec'
       WHERE CP.ID = 41;
      
      UPDATE CLEARING_PROVIDERS CP
         SET CP.NAME = 'OB Copyop Transact EUR'
       WHERE CP.ID = 43;
      
      UPDATE CLEARING_PROVIDERS CP
         SET CP.NAME = 'OB Copyop Transact USD'
       WHERE CP.ID = 44;
      
      UPDATE CLEARING_PROVIDERS CP
         SET CP.NAME = 'AOPS Copyop Transact USD'
       WHERE CP.ID = 45;
      
      UPDATE CLEARING_PROVIDERS CP
         SET CP.NAME = 'AOPS Copyop Transact EUR'
       WHERE CP.ID = 46;
      
      UPDATE CLEARING_PROVIDERS CP
         SET CP.NAME = 'AOPS TR'
       WHERE CP.ID = 47;
      
      UPDATE CLEARING_PROVIDERS CP
         SET CP.NAME = 'OB Trustpay EUR'
       WHERE CP.ID = 50;
      
      UPDATE CLEARING_PROVIDERS CP
         SET CP.NAME = 'OB Trustpay USD'
       WHERE CP.ID = 51;
      
      UPDATE CLEARING_PROVIDERS CP
         SET CP.NAME = 'OB Transact South Africa'
       WHERE CP.ID = 52;
	   
commit;	   
----------------------------------------
--Pavel Tabakov BAC-2234
----------------------------------------

--- victor; the IDs in table clearing_providers should be generated manually as we have hardcoded IDs in the code!
begin
  pkg_release.ddl_helper('drop sequence seq_clearing_providers', -2289);
end;
/


---- victor; BAC-2064
begin
  pkg_writers.create_permissions('Users', 'file', 'updateFirstName', 60);
  pkg_writers.create_permissions('Users', 'file', 'updateLastName', 70);
  pkg_writers.create_permissions('Documents', 'idsFile', 'updateFirstName', 50);
  pkg_writers.create_permissions('Documents', 'idsFile', 'updateLastName', 60);
end;
/

commit;

---- victor
begin
  pkg_release.ddl_helper('alter table transaction_statuses add constraint uk_transaction_statuses unique (code)', -2261);
end;
/

begin
  pkg_release.ddl_helper('alter table transaction_statuses add constraint cc_trns_upper_code check (code = upper(code))', -2264);
end;
/

@packages/pkg_withdraw_backend.pck

grant execute on pkg_withdraw_backend to etrader_web_connect;
create or replace public synonym pkg_withdraw_backend for pkg_withdraw_backend;

---- victor; transactions screen fixes (not BW compat!)
begin
  pkg_release.ddl_helper('create index idx_fk_trpp_transactionid on transaction_postponed (transaction_id)', -955);
end;
/

@packages/pkg_transactions_backend.pck


----- victor; AR-1585 banner sliders
create table banners
(
  id            number            not null,
  name          varchar2(50 char) not null,
  description   varchar2(200 char), 
  height_pixels number            not null,
  width_pixels  number            not null,
  banner_place  varchar2(50 char) not null,
  constraint pk_banners primary key (id),
  constraint uk_banners unique (name),
  constraint uk_banners_placename unique (banner_place)
);

comment on table banners is '[BNRS] Holds the banner id with name and description';
comment on column banners.height_pixels is 'Height in pixels. Default 335';
comment on column banners.width_pixels is 'Width in pixels. Default 730';

create table banner_slider_link_types
(
  id          number            not null,
  name        varchar2(50 char) not null,
  description varchar2(200 char), 
  constraint pk_banner_slider_link_types primary key (id),
  constraint uk_banner_slider_link_types unique (name)
);

comment on table banner_slider_link_types is '[BSLT] Holds the slider link type (popup or not)';

create table banner_sliders
(
  id            number not null,
  banner_id     number not null,
  position      number not null,
  is_active     number(1) not null,
  time_created  date   not null,
  time_updated  date,
  writer_id     number not null,
  link_type_id  number not null,
  is_deleted    number not null,
  constraint pk_banner_sliders primary key (id),
  constraint fk_bnsl_wter foreign key (writer_id) references writers (id), 
  constraint fk_bnsl_bnrs foreign key (banner_id) references banners (id),
  constraint fk_bnsl_bslt foreign key (link_type_id) references banner_slider_link_types (id)
);

comment on table banner_sliders is '[BNSL] Holds the banner slider position and if it is active or not';
comment on column banner_sliders.position is 'The position/number of the slider on the homepage banner';
comment on column banner_sliders.is_active is '1 - shown on the homepage banner, 0 - not shown on the homepage banner';
comment on column banner_sliders.writer_id is 'The writer who created/updated the slider';
comment on column banner_sliders.is_deleted is 'When set to 1 the record is considered as deleted and will not be returned by procedures';

create table banner_slider_images
(
  id               number not null,
  banner_slider_id number not null,
  language_id      number not null,
  link_url         varchar2(200 char) not null,
  image_name       varchar2(50 char) not null,
  time_created     date not null,
  time_updated     date,
  writer_id        number not null,
  constraint pk_banner_slider_images primary key (id),
  constraint fk_bnsi_wter foreign key (writer_id)  references writers (id), 
  constraint fk_bnsi_bnsl foreign key (banner_slider_id)  references banner_sliders (id),
  constraint fk_bnsi_lang foreign key (language_id)  references languages (id),
  constraint uk_banner_slider_images unique (banner_slider_id, language_id)
);

comment on table banner_slider_images is '[BNSI] Holds the banner slider images per language with link and image name';
comment on column banner_slider_images.link_url is 'The url of the image';
comment on column banner_slider_images.writer_id is 'The writer who created/updated the image';

begin
  pkg_writers.create_permissions('Marketing', 'promotionManagement', 'view', 10);
end;
/
commit;

create sequence seq_banners_conf;

insert into banners (id, name, description, banner_place, height_pixels, width_pixels) values (1, 'home page banner', 'anyoption home page top banner', 'homepage_main', 335, 730);

insert into banner_slider_link_types (id, name, description) values (1, 'pop-up', 'will be opened in new tab');
insert into banner_slider_link_types (id, name, description) values (2, 'redirect', 'will be opened in the same tab');
commit;

@packages/pkg_promotion_management.pck

create or replace public synonym pkg_promotion_management for pkg_promotion_management;
grant execute on pkg_promotion_management to etrader_web_connect;

alter table languages add constraint uk_languages unique (code);

----- END; victor; AR-1585 banner sliders



---- victor; CORE-2770 [Server] Backend wizard
begin
  pkg_release.ddl_helper('alter table asset_index add constraint fk_aidx_mark foreign key (market_id) references markets(id) enable novalidate'
                        ,-2275);
end;
/

begin
  pkg_release.ddl_helper('alter table asset_index modify constraint fk_aidx_mark validate', -2298);
end;
/

begin
  pkg_release.ddl_helper('alter table market_name_skin add constraint pk_market_name_skin primary key (id)', -2260);
end;
/

begin
  pkg_release.ddl_helper('alter table market_name_skin add constraint uk_market_name_skin unique (market_id, skin_id)', -2261);
end;
/

delete from asset_indexs_level_calc
where  info_id in
       (select id
        from   (select id, market_id, skin_id, row_number() over(partition by market_id, skin_id order by id) rn from asset_indexs_info)
        where  rn > 1);

delete from asset_indexs_info
where  id in (select id
              from   (select id, market_id, skin_id, row_number() over(partition by market_id, skin_id order by id) rn from asset_indexs_info)
              where  rn > 1);
commit;

begin
  pkg_release.ddl_helper('alter table asset_indexs_info add constraint uk_asset_indexs_info unique (market_id, skin_id)', -2261);
end;
/

@types/string_table.tps
@packages/pkg_markets.pck

create or replace public synonym pkg_markets for pkg_markets;
grant execute on pkg_markets to etrader_web_connect;

begin
  pkg_writers.create_permissions('Administrator', 'createMarket', 'view', 10);
  pkg_writers.create_permissions('Administrator', 'marketFields', 'view', 10);
  pkg_writers.create_permissions('Administrator', 'marketTranslations', 'view', 10);
  pkg_writers.create_permissions('Administrator', 'marketSchedule', 'view', 10);
  pkg_writers.create_permissions('Administrator', 'marketPriorities', 'view', 10);
  pkg_writers.create_permissions('Administrator', 'marketFormulas', 'view', 10);
end;
/
commit;
---- END; victor; CORE-2770 [Server] Backend wizard


----------------------------------------
--Pavel Tabakov BAC-2273
----------------------------------------       
update transaction_class_types set name = 'fix.balance' where id=10;
update transaction_class_types set name = 'deposit.by.company' where id=11;
update transaction_class_types set name = 'real.deposits' where id=1;
update transaction_class_types set name = 'real.withdrawals' where id=2;
update transaction_class_types set name = 'internal.actions' where id=3;
update transaction_class_types set name = 'fees' where id=4;
update transaction_class_types set name = 'admin.deposits' where id=5;
update transaction_class_types set name = 'admin.withdrawals' where id=6;
update transaction_class_types set name = 'bonus.deposits' where id=7;
update transaction_class_types set name = 'bonus.withdrawals' where id=8;
update transaction_class_types set name = 'reverse.withdrawals' where id=9;
commit;
----------------------------------------
--Pavel Tabakov BAC-2273
---------------------------------------- 


----------------------------------------
--Pavlin CORE-3515	 CORE-3470 [json + java] add closing level to bubbles settlement and display in on closed investments
----------------------------------------       
begin
  pkg_release.ddl_helper('alter table investments add bubbles_closing_level number', -1430);
end;
/

COMMENT ON COLUMN investments.bubbles_closing_level IS 'settlement level - received by external( bubbles) server';
----------------------------------------
--Pavlin CORE-3515 End
----------------------------------------       

---- victor; core-3662
@packages/pkg_apiusers.pck

grant execute on pkg_apiusers to etrader_web_connect;
create or replace public synonym pkg_apiusers for pkg_apiusers;


--- victor; CORE-3715 (CORE-3661)
begin
  pkg_release.ddl_helper('alter table transactions_issues modify (time_inserted not null)', -1442);
end;
/

@procedures/transactions_issues_cycle_fill.prc

--- victor; fix index and enforce uniqueness on table; (proactive investigation and optimization)
begin
  pkg_release.ddl_helper('drop index skin_market_groups_index1', -1418);
end;
/
  
begin
  pkg_release.ddl_helper('create unique index idx_smgr_skingroup on skin_market_groups (skin_id, market_group_id)', -955);
end;
/

begin
  pkg_release.ddl_helper('alter table opportunity_odds_group add constraint uk_opportunity_odds_group unique (odds_group)', -2261);
end;
/

---- victor; change style for skin 15
update skins set    style = 'ao', templates = 'template_main' where  id = 15 and  style != 'ao';
commit;


-------------------------------------------------------------------------------------------------------------------------------------------
-- CORE-3731 [Chinese AO] Deposit limits for yuan currency
-- Kiril
-------------------------------------------------------------------------------------------------------------------------------------------
update limits set min_deposit = 100000, max_deposit = 1000000, min_first_deposit = 100000 where id = 7;
update predefined_deposit_amount set amount = 1000 where currency_id = 7;
commit;
-------------------------------------------------------------------------------------------------------------------------------------------
-- End
-------------------------------------------------------------------------------------------------------------------------------------------
-----------------------------------------------
-- Start Mariya Lazarova CORE-3690
-----------------------------------------------
update PAYMENT_METHODS_COUNTRIES set EXCLUDE_IND = 1 where PAYMENT_ID in (37,14,62,63) and EXCLUDE_IND = 0;
commit;
-----------------------------------------------
-- End Mariya Lazarova CORE-3690
-----------------------------------------------

--- optimize SQL in InvestmentsDAOBase (changes in server are not yet implemented); CORE-3250
@packages/pkg_investments.pck

grant execute on pkg_investments to etrader_web_connect;
create or replace public synonym pkg_investments for pkg_investments;

--- victor; changes after Pavlin investigated issue with forward investments
@triggers/after_investments.trg

----------------------------------------------------------------------------------------
-- [AR-1956] Add users with lifetime volume > 100K $ as VIP (VIP)
-- Eyal O
----------------------------------------------------------------------------------------

-- Add new table USERS_VIP_STATUS
CREATE TABLE USERS_VIP_STATUS 
(
  ID NUMBER NOT NULL 
, NAME VARCHAR2(100) NOT NULL 
, CONSTRAINT USERS_VIP_STATUS_PK PRIMARY KEY  (ID)
);

COMMENT ON COLUMN USERS_VIP_STATUS.NAME IS 'Represent the name of vip''s status';

INSERT INTO USERS_VIP_STATUS(ID, NAME) VALUES(1, 'Not VIP');
INSERT INTO USERS_VIP_STATUS(ID, NAME) VALUES(2, 'VIP');
INSERT INTO USERS_VIP_STATUS(ID, NAME) VALUES(3, 'VIP Restricted');
commit;

-- Add new columns to USERS_ACTIVE_DATA
ALTER TABLE USERS_ACTIVE_DATA 
ADD (VIP_STATUS_ID NUMBER DEFAULT 1 not null, LIFETIME_VOLUME NUMBER DEFAULT 0 not null);

ALTER TABLE USERS_ACTIVE_DATA
ADD CONSTRAINT USERS_ACTIVE_DATA_FK1 FOREIGN KEY (VIP_STATUS_ID)
  REFERENCES USERS_VIP_STATUS(ID) enable novalidate;

COMMENT ON COLUMN USERS_ACTIVE_DATA.VIP_STATUS_ID IS 'Foreign key to the table ''USERS_VIP_STATUS''';
COMMENT ON COLUMN USERS_ACTIVE_DATA.LIFETIME_VOLUME IS 'Summary of all investments which didn''t canceled (USD as small amount)';

----------------------------------------------------------------------------------------
-- [AR-1956] Add users with lifetime volume > 100K $ as VIP (VIP)
-- Eyal O
----------------------------------------------------------------------------------------

-------------------------------- AR-2236 ----------------------------
-- eyal goren
---------------------------------------------------------------------
@triggers/after_invp.trg

INSERT INTO enumerators (ID,ENUMERATOR,CODE,VALUE,DESCRIPTION) VALUES (247,'lifetime volume for vip','lifetime volume for vip','10000000','minimum lifetime volume in small amount for user to have vip status');
commit;

begin
  for i in (select u.user_id, u.lifetime_volume, nvl(a.val, 0) val, count(*) over() cnt, rownum rn
            from   users_active_data u
            left   join (select user_id, sum((amount - nvl(option_plus_fee, 0) - nvl(insurance_amount_ru, 0)) * rate) val
                        from   investments
                        where  is_canceled = 0
                        group  by user_id) a
            on     a.user_id = u.user_id)
  loop
    -- to keep track of the progress
    dbms_application_info.set_action(i.rn || '/' || i.cnt);
  
    if i.lifetime_volume <> i.val
    then
    
      update users_active_data
      set    lifetime_volume = i.val
      where  user_id = i.user_id
      and    lifetime_volume = i.lifetime_volume;
    
      if sql%notfound
      then
        -- Value changed since we start the script. Have to re-evaluate
        update users_active_data
        set    lifetime_volume = nvl((select sum((amount - nvl(option_plus_fee, 0) - nvl(insurance_amount_ru, 0)) * rate)
                                     from   investments
                                     where  is_canceled = 0
                                     and    user_id = i.user_id)
                                    ,0)
        where  user_id = i.user_id;
      end if;
    
      -- commit after every update as UAD is a busy table and we'll cause "long" locks otherwise
      commit;
    end if;
  end loop;
end;
/

--//bgtest
--INSERT INTO jobs (ID, RUNNING, LAST_RUN_TIME, SERVER, RUN_INTERVAL, DESCRIPTION, JOB_CLASS, CONFIG, RUN_SERVERS, RUN_APPLICATIONS)
--VALUES 			 (36, 0, sysdate, 'WW01BG', 1440, 'mark users as vip every day', 'il.co.etrader.jobs.MarkUsersVipJob', '', 'WW01BG', 'backend');

--//live
INSERT INTO jobs (ID, RUNNING, LAST_RUN_TIME, SERVER, RUN_INTERVAL, DESCRIPTION, JOB_CLASS, CONFIG, RUN_SERVERS, RUN_APPLICATIONS)
VALUES 			 (36, 0, to_date(to_char(sysdate, 'dd-mm-yyyy')||' 00:05:00','dd-mm-yyyy hh24:mi:ss'), '02', 1440, 'mark users as vip every day', 'il.co.etrader.jobs.MarkUsersVipJob', '', '02;03', 'backend');
commit;
------------------------------- end AR-2236 -------------------------


ALTER TABLE USERS_ACTIVE_DATA modify CONSTRAINT USERS_ACTIVE_DATA_FK1 enable validate;

--- victor; proactive investigation of AWRs from 01.08.2016 shows slow query in production DB with sql_id aqpcd30a4873v (top by CPU)
--- created index to fix the issue. (already applied to prod!)
begin
  pkg_release.ddl_helper('create index idx_fk_uawb_user_id on users_award_bonus (user_id) compress online', -955);
end;
/

