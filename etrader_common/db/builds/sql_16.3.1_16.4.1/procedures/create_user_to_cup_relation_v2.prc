CREATE OR REPLACE /* Formatted on 3.12.2013 �. 11:27:53 (QP5 v5.149.1003.31008) */
PROCEDURE CREATE_USER_TO_CUP_RELATION_V2
IS
   -- *****************************************************************
   -- Description:
   -- Create user to cup relation.
   --
   -- Input Parameters: N/A
   --
   -- Output Parameters: N/A
   --
   -- Error Conditions Raised: N/A
   --
   -- Author:      Pavlin Mihalev
   --
   -- Revision History
   -- Date            Author       Reason for Change
   -- ----------------------------------------------------------------
   -- 03 APRIL 2013     P. Mihalev    Created.
   -- 04 APRIL 2013     T. Georgiev    Fixes.
   -- ***************************************************************


   CURSOR c_main
   IS
      SELECT DISTINCT (t.user_Id) AS uidd
        FROM transactions t
       WHERE     t.clearing_provider_id = 22                      /*deltapay*/
             AND t.status_Id = 2                                   /*success*/
             AND t.type_id = 38                                    /*deposit*/
                               ;

   CURSOR c_distrib
   IS
      SELECT id
        FROM distribution_history
       WHERE is_active = 1 AND distribution_type = 1                   /*CUP*/
                                                    ;

   CURSOR c_trans (p_user_id NUMBER)
   IS
        SELECT Id
          FROM transactions
         WHERE user_id = p_user_id
      ORDER BY 1;

   v_distrib   NUMBER;
   v_trans     NUMBER;
BEGIN
   FOR tmp IN c_main
   LOOP
      OPEN c_distrib;

      FETCH c_distrib INTO v_distrib;

      CLOSE c_distrib;

      OPEN c_trans (tmp.uidd);

      FETCH c_trans INTO v_trans;

      CLOSE c_trans;

      INSERT INTO user_to_provider (id,
                                    provider_id,
                                    user_id,
                                    distribution_id,
                                    transaction_id,
                                    date_created,
                                    distribution_type)
           VALUES (SEQ_USER_TO_PROVIDER.NEXTVAL,
                   22                                          /*deltapay CP*/
                     ,
                   tmp.uidd,
                   v_distrib                                           /*CUP*/
                            ,
                   v_trans,
                   SYSDATE,
                   1                                                   /*CUP*/
                    );
   END LOOP;

   COMMIT;
EXCEPTION
   WHEN OTHERS
   THEN                                                  -- handles all errors
      ROLLBACK;
      DBMS_OUTPUT.Put_Line ('----rollback-----');
END;
/
