CREATE OR REPLACE /* Formatted on 3.12.2013 �. 11:27:59 (QP5 v5.149.1003.31008) */
PROCEDURE TEMP
AS
   -- Status
   ACTIVE                          NUMBER := 1;
   SLEEP                           NUMBER := 2;
   COMMA                           NUMBER := 3;

   count_active_users_update       NUMBER := 0;
   count_sleep_users_update        NUMBER := 0;
   count_comma_users_update        NUMBER := 0;
   count_dep_no_inv_24h_update     NUMBER := 0;

   status_active_min_num_of_days   NUMBER := 0;
   status_active_max_num_of_days   NUMBER := 0;
   status_sleep_min_num_of_days    NUMBER := 0;
   status_sleep_max_num_of_days    NUMBER := 0;
   status_comma_min_num_of_days    NUMBER := 0;
   status_comma_max_num_of_days    NUMBER := 0;

   v_us_active                     users_status%ROWTYPE;
   v_us_sleep                      users_status%ROWTYPE;
   v_us_comma                      users_status%ROWTYPE;
   last_action                     DATE := NULL;
   last_dep                        DATE := NULL;
   last_inv                        DATE := NULL;
   firstDepositTime                DATE := NULL;
BEGIN
   SELECT us.*
     INTO v_us_active
     FROM users_status us
    WHERE us.id = ACTIVE;

   SELECT us.*
     INTO v_us_sleep
     FROM users_status us
    WHERE us.id = SLEEP;

   SELECT us.*
     INTO v_us_comma
     FROM users_status us
    WHERE us.id = COMMA;

   DBMS_OUTPUT.PUT_LINE ('------------------------------------------------:');
   DBMS_OUTPUT.PUT_LINE ('USERS_STATUS_HANDLER started at: ' || SYSDATE);

   FOR v_user
      IN (SELECT u.id AS user_id,
                 u.status_id AS user_status_id,
                 u.first_deposit_id AS first_deposit_id,
                 u.rank_id AS user_rank_id
            FROM users u
           WHERE                                         --u.class_id <> 0 AND
                u    .is_active = 1
                 AND u.first_deposit_id IS NOT NULL
                 AND u.id = 482185)
   LOOP
      SELECT MAX (t.time_created)
        INTO last_dep
        FROM transactions t, transaction_types tt
       WHERE     t.user_id = v_user.user_id
             AND t.type_id = tt.id
             AND tt.class_type = 1;

      SELECT MAX (i.time_created)
        INTO last_inv
        FROM investments i
       WHERE i.user_id = v_user.user_id AND i.is_canceled = 0;

      IF (last_dep IS NOT NULL AND last_inv IS NOT NULL)
      THEN
         IF (last_dep > last_inv)
         THEN
            last_action := last_dep;
         ELSE
            last_action := last_inv;
         END IF;
      ELSE
         IF (last_dep IS NULL AND last_inv IS NOT NULL)
         THEN
            last_action := last_inv;
         ELSE
            last_action := last_dep;
         END IF;
      END IF;

      IF (last_action IS NULL
          OR last_action < SYSDATE - v_us_comma.min_num_of_days)
      THEN
         IF (v_user.user_status_id <> COMMA)
         THEN
            count_comma_users_update := count_comma_users_update + 1;

            --UPDATE users
            --SET status_id = COMMA
            --WHERE id = v_user.user_id;
            DBMS_OUTPUT.PUT_LINE ('ofer');
         --INSERT INTO users_status_hist (ID, USER_ID, USER_STATUS_ID, QUALIFICATION_DATE)
         --VALUES (SEQ_USERS_STATUS_HIST.nextval, v_user.user_id, COMMA, sysdate);
         END IF;
      ELSE
         IF (last_action <= SYSDATE - v_us_sleep.min_num_of_days
             AND last_action > SYSDATE - v_us_sleep.max_num_of_days)
         THEN
            IF (v_user.user_status_id <> SLEEP)
            THEN
               count_sleep_users_update := count_sleep_users_update + 1;

               -- UPDATE users
               --SET status_id = SLEEP
               --WHERE id = v_user.user_id;
               DBMS_OUTPUT.PUT_LINE ('ofer');
            --INSERT INTO users_status_hist (ID, USER_ID, USER_STATUS_ID, QUALIFICATION_DATE)
            --VALUES (SEQ_USERS_STATUS_HIST.nextval, v_user.user_id, SLEEP, sysdate);
            END IF;
         ELSE
            IF (last_action <= SYSDATE - v_us_active.min_num_of_days)
            THEN
               IF (v_user.user_status_id <> ACTIVE)
               THEN
                  count_active_users_update := count_active_users_update + 1;

                  --UPDATE users
                  --SET status_id = ACTIVE
                  --WHERE id = v_user.user_id;
                  DBMS_OUTPUT.PUT_LINE ('ofer');
               --INSERT INTO users_status_hist (ID, USER_ID, USER_STATUS_ID, QUALIFICATION_DATE)
               --VALUES (SEQ_USERS_STATUS_HIST.nextval, v_user.user_id, ACTIVE, sysdate);
               END IF;
            END IF;
         END IF;
      END IF;

      last_action := NULL;

      IF (last_action IS NULL)
      THEN
         IF (last_dep < last_inv - 1)
         THEN
            --UPDATE users
            --SET DEP_NO_INV_24H = 1
            --WHERE id = v_user.user_id;
            DBMS_OUTPUT.PUT_LINE ('ofer');
            count_dep_no_inv_24h_update := count_dep_no_inv_24h_update + 1;
         END IF;
      END IF;
   END LOOP;

   DBMS_OUTPUT.
    PUT_LINE ('update users to active= ' || count_active_users_update);
   DBMS_OUTPUT.
    PUT_LINE ('update users to sleep= ' || count_sleep_users_update);
   DBMS_OUTPUT.
    PUT_LINE ('update users to comma= ' || count_comma_users_update);
   DBMS_OUTPUT.
    PUT_LINE ('update users dep_no_inv_24h= ' || count_dep_no_inv_24h_update);
   DBMS_OUTPUT.PUT_LINE ('USERS_STATUS_HANDLER finished at: ' || SYSDATE);
   DBMS_OUTPUT.PUT_LINE ('------------------------------------------------:');
   NULL;
END TEMP;
/
