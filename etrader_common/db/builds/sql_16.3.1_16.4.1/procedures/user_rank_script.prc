CREATE OR REPLACE /* Formatted on 3.12.2013 �. 11:28:04 (QP5 v5.149.1003.31008) */
PROCEDURE USER_RANK_SCRIPT
AS
   tran_time_created   DATE := NULL;
   user_rank_id        NUMBER := 0;
   userAmount          NUMBER := 0;
--This handler do not insert to history
BEGIN
   DBMS_OUTPUT.PUT_LINE ('------------------------------------------------:');
   DBMS_OUTPUT.PUT_LINE ('USER_RANK_SCRIPT started at: ' || SYSDATE);

   FOR v_user IN (SELECT u.id AS user_id, u.first_deposit_id AS fdi
                    FROM users u
                   WHERE u.first_deposit_id IS NOT NULL AND u.rank_id = 0)
   LOOP
      SELECT t.time_created
        INTO tran_time_created
        FROM transactions t
       WHERE t.id = v_user.fdi;

      IF (tran_time_created > SYSDATE - 7)
      THEN
         UPDATE users
            SET rank_id = 1
          WHERE id = v_user.user_id;
      ELSE
         SELECT SUM ( (t.amount / 100) * t.rate)
           INTO userAmount
           FROM transactions t, transaction_types tt
          WHERE     t.user_id = v_user.user_id
                AND t.type_id = tt.id
                AND tt.class_type = 1
                AND t.status_id IN (2, 7, 8);

         IF (userAmount IS NOT NULL)
         THEN
            SELECT ur.id
              INTO user_rank_id
              FROM users_rank ur
             WHERE (userAmount >= ur.min_sum_of_deposits
                    AND userAmount < ur.max_sum_of_deposits)
                   OR (userAmount >= ur.min_sum_of_deposits
                       AND ur.max_sum_of_deposits IS NULL);

            UPDATE users
               SET rank_id = user_rank_id
             WHERE id = v_user.user_id;
         END IF;
      END IF;
   END LOOP;

   DBMS_OUTPUT.PUT_LINE ('------------------------------------------------:');
   DBMS_OUTPUT.PUT_LINE ('USER_RANK_SCRIPT finished at: ' || SYSDATE);
   NULL;
END USER_RANK_SCRIPT;
/
