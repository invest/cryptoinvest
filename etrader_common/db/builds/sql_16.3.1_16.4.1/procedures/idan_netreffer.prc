CREATE OR REPLACE /* Formatted on 3.12.2013 �. 11:27:55 (QP5 v5.149.1003.31008) */
PROCEDURE "IDAN_NETREFFER"
AS
   CURSOR c_tmp
   IS
      SELECT *
        FROM users u
       WHERE u.id IN
                (621831,
                 621742,
                 621738,
                 621727,
                 621644,
                 621582,
                 621564,
                 621558,
                 621537,
                 621494,
                 621462,
                 621352,
                 621344,
                 621295,
                 621275,
                 621218,
                 621210,
                 621203,
                 621153,
                 621145,
                 621140,
                 621136,
                 621128,
                 621081,
                 621056,
                 621038,
                 621035,
                 621033,
                 621014,
                 621009,
                 620989,
                 620966,
                 620956,
                 620936,
                 620927,
                 620908,
                 620890,
                 620867,
                 620863,
                 620859,
                 620854,
                 620853,
                 620832,
                 620831,
                 620824,
                 620777,
                 620735,
                 620654,
                 620633,
                 620629,
                 620569,
                 620562,
                 620546,
                 620498,
                 620483,
                 620402,
                 620391);
BEGIN
   FOR tmp IN c_tmp
   LOOP
      INSERT INTO IDAN_NETREFER_TEMP
           SELECT tmp.id "CustomerId",
                  TO_CHAR (dates.COLUMN_VALUE, 'YYYY-MM-DD') "Activity Date",
                  'anyoption' "Product ID",
                  NVL (sum_bonuses.bonuses, 0) "Bonuses",
                    (NVL (deposits.sum_deposits_ao, 0)) * 0.06
                  + NVL (cft_withdraws.cft_withdraws_num * 2, 0)
                  + NVL (wire_withdraws.wire_withdraws_fee, 0)
                     "Adjustments",
                  NVL (deposits.sum_deposits_ao, 0) "Deposits",
                  NVL (sums_investments.house_win_ao, 0)
                  - NVL (sum_fix_balances.fixBalance, 0)
                     "Gross Revenue",
                  NVL (sums_investments.turnover_ao, 0) "Turnover",
                  NVL (withdraws.sum_withdraws_ao, 0) "payouts",
                  NVL (sums_investments.investments_num, 0) "Transactions"
             FROM TABLE (
                     get_date_range (TO_DATE ('2013-10-30', 'YYYY-MM-DD'),
                                     TO_DATE ('2013-10-31', 'YYYY-MM-DD'))) dates
                  LEFT JOIN (  SELECT TO_CHAR (dates2.COLUMN_VALUE, 'YYYY-MM-DD')
                                         date1,
                                      SUM (
                                         (CASE
                                             WHEN (t.status_id IN (2, 7))
                                             THEN
                                                t.amount * t.rate
                                             ELSE
                                                t.amount * t.rate * (-1)
                                          END)
                                         / 100)
                                         AS sum_deposits_ao
                                 FROM transactions t,
                                      transaction_types tt,
                                      TABLE (
                                         get_date_range (
                                            TO_DATE ('2013-10-30', 'YYYY-MM-DD'),
                                            TO_DATE ('2013-10-31', 'YYYY-MM-DD'))) dates2
                                WHERE t.type_id = tt.id
                                      AND tt.class_type IN (1, 11) --  11: deposit by company
                                      AND ( (t.status_id IN (2, 7)
                                             AND TO_CHAR (t.time_created,
                                                          'YYYY-MM-DD') =
                                                    TO_CHAR (dates2.COLUMN_VALUE,
                                                             'YYYY-MM-DD'))
                                           OR (t.status_id IN (8, 12, 13)
                                               AND TO_CHAR (t.time_settled,
                                                            'YYYY-MM-DD') =
                                                      TO_CHAR (
                                                         dates2.COLUMN_VALUE,
                                                         'YYYY-MM-DD')
                                               AND TO_CHAR (t.time_settled,
                                                            'YYYY-MM-DD') <>
                                                      TO_CHAR (time_created,
                                                               'YYYY-MM-DD')))
                                      AND t.user_id = tmp.id
                             GROUP BY TO_CHAR (dates2.COLUMN_VALUE,
                                               'YYYY-MM-DD')) deposits
                     ON deposits.date1 =
                           TO_CHAR (dates.COLUMN_VALUE, 'YYYY-MM-DD')
                  ---------------------------------------------------------------------
                  LEFT JOIN (  SELECT TO_CHAR (t.time_created, 'YYYY-MM-DD')
                                         date1,
                                      SUM (
                                         CASE
                                            WHEN t.type_id = 22
                                            THEN
                                               -- if  TRANS_TYPE_FIX_BALANCE_DEPOSIT
                                               t.amount * t.rate
                                            ELSE
                                               -- if  TRANS_TYPE_FIX_BALANCE_WITHDRAW
                                               t.amount * t.rate * (-1)
                                         END)
                                      / 100
                                         fixBalance
                                 FROM transactions t
                                WHERE t.type_id IN (22, 23)
                                      AND --                          to_char(t.time_created,'YYYY-MM-DD') = to_char(sysdate -1,'YYYY-MM-DD') AND
                                          t.user_id = tmp.id
                             GROUP BY TO_CHAR (t.time_created, 'YYYY-MM-DD')) sum_fix_balances
                     ON sum_fix_balances.date1 =
                           TO_CHAR (dates.COLUMN_VALUE, 'YYYY-MM-DD')
                  ---------------------------------------------------------------------
                  LEFT JOIN (  SELECT TO_CHAR (t.time_settled, 'YYYY-MM-DD')
                                         date1,
                                      SUM (t.amount * t.rate / 100)
                                         AS sum_withdraws_ao
                                 FROM transactions t, transaction_types tt
                                WHERE     t.type_id = tt.id
                                      AND tt.class_type = 2
                                      AND t.status_id IN (2)
                                      --                        AND to_char(t.time_settled,'YYYY-MM-DD') = to_char(sysdate -1,'YYYY-MM-DD')
                                      AND t.user_id = tmp.id
                             GROUP BY TO_CHAR (t.time_settled, 'YYYY-MM-DD')) withdraws
                     ON withdraws.date1 =
                           TO_CHAR (dates.COLUMN_VALUE, 'YYYY-MM-DD')
                  ---------------------------------------------------------------------
                  LEFT JOIN (  SELECT TO_CHAR (t.time_settled, 'YYYY-MM-DD')
                                         date1,
                                      COUNT (t.id) AS cft_withdraws_num
                                 FROM transactions t
                                WHERE     t.type_id = 10        -- CC withdraw
                                      AND t.status_id IN (2)
                                      AND t.clearing_provider_id = 2 -- Xor Anyoption
                                      AND t.is_credit_withdrawal = 0
                                      --                        AND to_char(t.time_settled,'YYYY-MM-DD') = to_char(sysdate -1,'YYYY-MM-DD')
                                      AND t.user_id = tmp.id
                             GROUP BY TO_CHAR (t.time_settled, 'YYYY-MM-DD')) cft_withdraws
                     ON cft_withdraws.date1 =
                           TO_CHAR (dates.COLUMN_VALUE, 'YYYY-MM-DD')
                  ---------------------------------------------------------------------
                  LEFT JOIN (  SELECT TO_CHAR (t.time_settled, 'YYYY-MM-DD')
                                         date1,
                                      SUM (w.bank_fee_amount * t.rate)
                                         AS wire_withdraws_fee
                                 FROM transactions t, wires w
                                WHERE     t.type_id = 14 -- Bank wire withdraw
                                      AND t.status_id IN (2)
                                      AND t.wire_id = w.id
                                      --                        AND to_char(t.time_settled,'YYYY-MM-DD') = to_char(sysdate -1,'YYYY-MM-DD')
                                      AND t.user_id = tmp.id
                             GROUP BY TO_CHAR (t.time_settled, 'YYYY-MM-DD')) wire_withdraws
                     ON wire_withdraws.date1 =
                           TO_CHAR (dates.COLUMN_VALUE, 'YYYY-MM-DD')
                  ---------------------------------------------------------------------
                  LEFT JOIN (  SELECT date1,
                                      SUM (turnover_ao) turnover_ao,
                                      SUM (
                                         (CASE
                                             WHEN house_win_ao > 0
                                             THEN
                                                house_win_ao * 0.95
                                             ELSE
                                                house_win_ao
                                          END))
                                         house_win_ao,
                                      SUM (investments_num) investments_num
                                 FROM (  SELECT TO_CHAR (dates2.COLUMN_VALUE,
                                                         'YYYY-MM-DD')
                                                   date1,
                                                i.user_id,
                                                SUM (
                                                   (CASE
                                                       WHEN i.is_canceled = 0
                                                       THEN
                                                          i.amount * i.rate
                                                       ELSE
                                                          i.amount * i.rate * (-1)
                                                    END)
                                                   / 100)
                                                   AS turnover_ao,
                                                SUM (
                                                   (CASE
                                                       WHEN i.is_canceled = 0
                                                       THEN
                                                          i.house_result * i.rate
                                                       ELSE
                                                            i.house_result
                                                          * i.rate
                                                          * (-1)
                                                    END)
                                                   / 100)
                                                   AS house_win_ao,
                                                SUM (
                                                   CASE
                                                      WHEN i.is_canceled = 0 THEN 1
                                                      ELSE -1
                                                   END)
                                                   investments_num
                                           FROM investments i,
                                                TABLE (
                                                   get_date_range (
                                                      TO_DATE ('2013-10-30',
                                                               'YYYY-MM-DD'),
                                                      TO_DATE ('2013-10-31',
                                                               'YYYY-MM-DD'))) dates2
                                          WHERE i.is_settled = 1
                                                AND (TO_CHAR (i.time_settled,
                                                              'YYYY-MM-DD') =
                                                        TO_CHAR (
                                                           dates2.COLUMN_VALUE,
                                                           'YYYY-MM-DD')
                                                     OR TO_CHAR (i.time_canceled,
                                                                 'YYYY-MM-DD') =
                                                           TO_CHAR (
                                                              dates2.COLUMN_VALUE,
                                                              'YYYY-MM-DD'))
                                                AND i.user_id = tmp.id
                                       GROUP BY TO_CHAR (dates2.COLUMN_VALUE,
                                                         'YYYY-MM-DD'),
                                                i.user_id) user_inv
                             GROUP BY date1) sums_investments
                     ON sums_investments.date1 =
                           TO_CHAR (dates.COLUMN_VALUE, 'YYYY-MM-DD')
                  ---------------------------------------------------------------------
                  LEFT JOIN (  SELECT TO_CHAR (dates2.COLUMN_VALUE, 'YYYY-MM-DD')
                                         date1,
                                      SUM (
                                         CASE
                                            WHEN bt.class_type_id = 3
                                            THEN
                                               i.lose * i.rate -- Next invest on us
                                            ELSE
                                               bu.bonus_amount * t.rate -- Not next invest on us
                                         END)
                                      / 100
                                         bonuses
                                 FROM TABLE (
                                         get_date_range (
                                            TO_DATE ('2013-10-30', 'YYYY-MM-DD'),
                                            TO_DATE ('2013-10-31', 'YYYY-MM-DD'))) dates2,
                                      bonus_types bt,
                                            bonus_users bu
                                         LEFT JOIN
                                            investments i
                                         ON bu.id = i.bonus_user_id
                                      LEFT JOIN
                                         transactions t
                                      ON bu.id = t.bonus_user_id
                                         AND t.type_id = 12
                                WHERE bt.id = bu.type_id
                                      AND ( (bu.time_used IS NOT NULL
                                             AND TO_CHAR (bu.time_used,
                                                          'YYYY-MM-DD') =
                                                    TO_CHAR (dates2.COLUMN_VALUE,
                                                             'YYYY-MM-DD'))
                                           OR (bu.time_used IS NULL
                                               AND bu.time_done IS NOT NULL
                                               AND TO_CHAR (bu.time_done,
                                                            'YYYY-MM-DD') =
                                                      TO_CHAR (
                                                         dates2.COLUMN_VALUE,
                                                         'YYYY-MM-DD')))
                                      -- Next invest on us             -- Not next invest on us
                                      AND (bt.class_type_id != 3
                                           OR (bt.class_type_id = 3
                                               AND i.lose > 0))
                                      AND bu.user_id = tmp.id
                             GROUP BY TO_CHAR (dates2.COLUMN_VALUE,
                                               'YYYY-MM-DD')) sum_bonuses
                     ON sum_bonuses.date1 =
                           TO_CHAR (dates.COLUMN_VALUE, 'YYYY-MM-DD')
            ---------------------------------------------------------------------
            WHERE --        u.combination_id = mcom.id
                  --      AND mcom.campaign_id = 365 -- Need to change to netrefer campaign
                  --      AND u.class_id != 0
                  --      AND
                  (   sum_bonuses.bonuses <> 0
                   OR deposits.sum_deposits_ao <> 0
                   OR sums_investments.house_win_ao <> 0
                   OR sums_investments.turnover_ao <> 0
                   OR withdraws.sum_withdraws_ao <> 0
                   OR sums_investments.investments_num <> 0
                   OR sum_fix_balances.fixBalance <> 0
                   OR cft_withdraws.cft_withdraws_num <> 0
                   OR wire_withdraws.wire_withdraws_fee <> 0)
         ORDER BY dates.COLUMN_VALUE;

      DBMS_OUTPUT.PUT_LINE (tmp.id);
   END LOOP;

   DBMS_OUTPUT.PUT_LINE ('dasdsadsa');
   NULL;
END;
/
