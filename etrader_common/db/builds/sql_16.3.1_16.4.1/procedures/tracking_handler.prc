CREATE OR REPLACE /* Formatted on 3.12.2013 �. 11:28:00 (QP5 v5.149.1003.31008) */
PROCEDURE "TRACKING_HANDLER"
AS
   v_peh   population_entries_hist%ROWTYPE;
BEGIN
   -- Find all users that needs to be removed from tracking by time or by decline
   FOR v_pop_user
      IN (SELECT pu.id,
                 i.population_entry_id,
                 ia.action_time,
                 pu.curr_population_entry_id,
                 p.population_type_id,
                 pe.qualification_time,
                 pu.curr_assigned_writer_id AS curr_assigned_writer_id,
                 (CASE
                     WHEN (SYSDATE > ia.action_time + s.other_reactions_days)
                     THEN
                        1
                     ELSE
                        0
                  END)
                    AS is_time_end
            FROM             population_users pu
                          LEFT JOIN
                             users u
                          ON pu.user_id = u.id
                       LEFT JOIN
                          contacts c
                       ON pu.contact_id = c.id
                    LEFT JOIN
                       population_entries pe
                    ON pu.curr_population_entry_id = pe.id
                 LEFT JOIN
                    populations p
                 ON pe.population_id = p.id,
                 issue_actions ia,
                 issues i,
                 skins s
           WHERE     pu.entry_type_id = 2                          -- Tracking
                 AND pu.lock_history_id IS NULL
                 AND pu.entry_type_action_id = ia.id
                 AND ia.issue_id = i.id
                 AND ( (u.id IS NOT NULL AND s.id = u.skin_id)
                      OR (u.id IS NULL AND s.id = c.skin_id))
                 AND (SYSDATE > ia.action_time + s.other_reactions_days
                      OR                    -- users re/entered to decline pop
                         (p.population_type_id = 1
                          AND (pe.id != i.population_entry_id
                               OR pe.qualification_time > ia.action_time))))
   LOOP
      -- Init params
      v_peh := NULL;

      -- Insert a new population_entry_history
      SELECT SEQ_POPULATION_ENTRIES_HIST.NEXTVAL INTO v_peh.id FROM DUAL;

      -- If users' tracking time ended remove it
      -- Else user entered to first decline after it got into TRACKING remove it by decline
      IF (v_pop_user.is_time_end = 1)
      THEN
         v_peh.population_entry_id := v_pop_user.population_entry_id;
         v_peh.status_id := 202;               -- remove from tracking by time
      ELSE
         v_peh.population_entry_id := v_pop_user.curr_population_entry_id;
         v_peh.status_id := 206;            -- remove from tracking by decline
      END IF;

      v_peh.writer_id := 0;
      v_peh.assigned_writer_id := v_pop_user.curr_assigned_writer_id;
      v_peh.time_created := SYSDATE;
      v_peh.issue_action_id := NULL;

      INSERT INTO population_entries_hist
           VALUES v_peh;

      -- Update user entry in population_users table
      UPDATE population_users pu
         SET pu.entry_type_id = 1,            -- population.enrty.type.general
                                  pu.entry_type_action_id = NULL
       --pu.curr_assigned_writer_id = null
       WHERE pu.id = v_pop_user.id;


      COMMIT;
   END LOOP;

   NULL;
END TRACKING_HANDLER;
/
