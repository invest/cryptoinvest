-------- Create package for POPULATION --------
create or replace PACKAGE "POPULATION" is
  
  FUNCTION GET_NUM_MINUTES_BY_CONFIG(P_pop_type_id NUMBER, P_aff_key NUMBER, P_writer_id NUMBER)
	RETURN NUMBER;

  FUNCTION GET_NUM_MINUTES(P_pop_type_id NUMBER, P_aff_key NUMBER, P_writer_id NUMBER, P_num_minutes_by_skin NUMBER)
	RETURN NUMBER;
  
End POPULATION;

create or replace PACKAGE BODY "POPULATION" is

	FUNCTION GET_NUM_MINUTES_BY_CONFIG(P_pop_type_id NUMBER, P_aff_key NUMBER, P_writer_id NUMBER)
    RETURN NUMBER
  AS
    -- *** FUNCTION'S PARAMETERS ***
    F_num_minutes NUMBER;
    CURSOR c_get_num_minutes_by_config IS
    SELECT
      PTC_NUMBER_OF_MINUTES
    INTO
      F_num_minutes
    FROM
      (SELECT
        ptc.NUMBER_OF_MINUTES as PTC_NUMBER_OF_MINUTES
      FROM
        POPULATION_TYPES_CONFIG ptc
      WHERE
        (ptc.POPULATION_TYPE_ID is null or ptc.POPULATION_TYPE_ID = P_pop_type_id)
        and (ptc.AFFILIATE_KEY is null or ptc.AFFILIATE_KEY = P_aff_key)
        and (ptc.WRITER_ID is null or ptc.WRITER_ID = P_writer_id)
      ORDER BY
        ptc.WRITER_ID,
        ptc.AFFILIATE_KEY)
    WHERE
      rownum = 1;
  BEGIN  
    OPEN 
      c_get_num_minutes_by_config;
    FETCH 
      c_get_num_minutes_by_config 
    INTO 
      F_num_minutes;
    CLOSE c_get_num_minutes_by_config;
    
    RETURN F_num_minutes;
  END GET_NUM_MINUTES_BY_CONFIG;
  
  FUNCTION GET_NUM_MINUTES(P_pop_type_id NUMBER, P_aff_key NUMBER, P_writer_id NUMBER, P_num_minutes_by_skin NUMBER)
    RETURN NUMBER
  AS
    -- *** FUNCTION'S PARAMETERS ***
    F_num_minutes NUMBER;
  BEGIN
    F_num_minutes := GET_NUM_MINUTES_BY_CONFIG(P_pop_type_id, P_aff_key, P_writer_id);
    -- If number of minutes not found in POPULATION_TYPES_CONFIG 
    -- THEN get number of minutes by POPULATIONS table
    IF (F_num_minutes is null) THEN
      F_num_minutes := P_num_minutes_by_skin;
    END IF;
    RETURN F_num_minutes;
  END GET_NUM_MINUTES;
  
END POPULATION;