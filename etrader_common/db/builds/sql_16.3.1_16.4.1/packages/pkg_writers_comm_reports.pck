create or replace package pkg_writers_comm_reports is

  -- Author  : VICTOR.SLAVOV
  -- Created : 2016-05-16 18:28:38
  -- Purpose : 

  procedure get_data_by_user
  (
    o_users_data    out sys_refcursor
   ,i_writer_id     in number
   ,i_from_date     in date
   ,i_to_date       in date
   ,i_page_number   in number
   ,i_rows_per_page in number
  );

  procedure get_data_by_writer
  (
    o_writers_data  out sys_refcursor
   ,i_from_date     in date
   ,i_to_date       in date
   ,i_page_number   in number
   ,i_rows_per_page in number
  );

end pkg_writers_comm_reports;
/
create or replace package body pkg_writers_comm_reports is

  procedure get_data_by_user
  (
    o_users_data    out sys_refcursor
   ,i_writer_id     in number
   ,i_from_date     in date
   ,i_to_date       in date
   ,i_page_number   in number
   ,i_rows_per_page in number
  ) is
  begin
    open o_users_data for
      select user_id, trader_value, total_inv, total_count
      from   (select a.user_id
                    ,nvl(b.writer_trans, 0) / a.total_inv trader_value
                    ,a.total_inv
                    ,row_number() over(order by total_inv desc) rn
                    ,count(*) over() total_count
              from   (select i.user_id
                            ,sum((i.amount - i.insurance_amount_gm - i.insurance_amount_ru - i.option_plus_fee) * wci.factor * i.rate) total_inv
                      from   writers_commission_inv wci
                      join   investments i
                      on     i.id = wci.investments_id
                      where  i.is_canceled = 0
                      and    wci.time_created >= i_from_date
                      and    wci.time_created <= i_to_date
                      and    wci.writer_id = i_writer_id
                      group  by i.user_id) a
              left   join (select t.user_id, sum(t.amount * t.rate) writer_trans
                          from   writers_commission_dep wcd
                          join   transactions t
                          on     t.id = wcd.transaction_id
                          join   transaction_types tt
                          on     tt.id = t.type_id
                          where  t.status_id in (2, 7, 8)
                          and    tt.class_type = 1
                          and    wcd.time_created >= i_from_date
                          and    wcd.time_created <= i_to_date
                          and    wcd.writer_id = i_writer_id
                          group  by t.user_id) b
              on     a.user_id = b.user_id)
      where  rn >= (i_page_number - 1) * i_rows_per_page
      and    rn < i_page_number * i_rows_per_page
      order  by total_inv desc;
  end get_data_by_user;

  procedure get_data_by_writer
  (
    o_writers_data  out sys_refcursor
   ,i_from_date     in date
   ,i_to_date       in date
   ,i_page_number   in number
   ,i_rows_per_page in number
  ) is
  begin
    open o_writers_data for
      select writer_name, trader_value, total_inv, total_count
      from   (select w.user_name writer_name
                    ,nvl(b.writer_trans, 0) / a.total_inv trader_value
                    ,a.total_inv
                    ,row_number() over(order by user_name) rn
                    ,count(*) over() total_count
              from   (select wci.writer_id
                            ,sum((i.amount - i.insurance_amount_gm - i.insurance_amount_ru - i.option_plus_fee) * wci.factor * i.rate) total_inv
                      from   writers_commission_inv wci
                      join   investments i
                      on     i.id = wci.investments_id
                      where  i.is_canceled = 0
                      and    wci.time_created >= i_from_date
                      and    wci.time_created <= i_to_date
                      group  by wci.writer_id) a
              join   writers w
              on     w.id = a.writer_id
              left   join (select wcd.writer_id, sum(t.amount * t.rate) writer_trans
                          from   writers_commission_dep wcd
                          join   transactions t
                          on     t.id = wcd.transaction_id
                          join   transaction_types tt
                          on     tt.id = t.type_id
                          where  t.status_id in (2, 7, 8)
                          and    tt.class_type = 1
                          and    wcd.time_created >= i_from_date
                          and    wcd.time_created <= i_to_date
                          group  by wcd.writer_id) b
              on     b.writer_id = a.writer_id)
      where  rn >= (i_page_number - 1) * i_rows_per_page
      and    rn < i_page_number * i_rows_per_page
      order  by writer_name;
  end get_data_by_writer;

end pkg_writers_comm_reports;
/
