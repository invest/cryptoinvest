CREATE OR REPLACE PACKAGE REPORTS AS

FUNCTION GET_NEW_CUST_PER_SOURCE_REPORT (pi_days_before number)
RETURN NEW_CUST_PER_SOURCE_TBL_TYPE;


FUNCTION GET_CUST_FTD_WITHDREW_REPORT (pi_days_before number)
RETURN NEW_CUST_FTD_WITHDREW_TBL_TYPE;

FUNCTION GET_FTD_SKIN_SOURCE_REPORT (pi_days_before number)
RETURN NEW_FTD_SKIN_SOURCE_TBL_TYPE;  


 FUNCTION return_amount
     (c_key_value IN NUMBER)
      RETURN NUMBER;
   FUNCTION insert_amount
     (c_key_value IN NUMBER)
      RETURN NUMBER;
      
      
FUNCTION GET_CUST_PER_SOURCE_REPORT (pi_days_before number)
RETURN CUST_PER_SOURCE_TBL_TYPE;

FUNCTION GET_CUST_FTD_WITHD_REPORT (pi_days_before number)
RETURN CUST_FTD_WITHDREW_TBL_TYPE;

FUNCTION GET_FTD_SKIN_PER_SOURCE_REPORT (pi_days_before number, pi_skin_display_name varchar2)
RETURN FTD_SKIN_SOURCE_TBL_TYPE;

end;
/

CREATE OR REPLACE PACKAGE BODY REPORTS
AS

--MANAGER REPORTS
FUNCTION GET_NEW_CUST_PER_SOURCE_REPORT (pi_days_before number)
RETURN NEW_CUST_PER_SOURCE_TBL_TYPE
AS
v_return  NEW_CUST_PER_SOURCE_TBL_TYPE;
BEGIN
SELECT NEW_CUST_PER_SOURCE_OBJ_TYPE(  B.total_reg,B.total_reg_mob,B.total_reg_cop,B.total_reg_cop_mob,
                                      B.total_first_dep,B.total_first_dep_mob,B.total_first_dep_cop,B.total_first_dep_cop_mob,
                                      B.reg_affiliates,B.reg_affiliates_mob,B.reg_affiliates_cop,B.reg_affiliates_cop_mob,           
                                      B.first_dep_affiliates,B.first_dep_affiliates_mob,B.first_dep_affiliates_cop,B.first_dep_affiliates_cop_mob,           
                                      B.reg_tony_jason,B.reg_tony_jason_mob,B.reg_tony_jason_cop,B.reg_tony_jason_cop_mob,           
                                      B.first_dep_tony_jason,B.first_dep_tony_jason_mob,B.first_dep_tony_jason_cop,B.first_dep_tony_jason_cop_mob,
                                      B.reg_google_d,B.reg_google_d_mob,B.reg_google_d_cop,B.reg_google_d_cop_mob,           
                                      B.first_dep_google_d,B.first_dep_google_d_mob,B.first_dep_google_d_cop,B.first_dep_google_d_cop_mob,
                                      B.reg_beni,B.reg_beni_mob,B.reg_beni_cop,B.reg_beni_cop_mob,           
                                      B.first_dep_beni,B.first_dep_beni_mob,B.first_dep_beni_cop,B.first_dep_beni_cop_mob,
                                      B.reg_free,B.reg_free_cop,B.reg_free_mob,B.reg_free_cop_mob,      
                                      B.first_dep_free,B.first_dep_free_mob,B.first_dep_free_cop,B.first_dep_free_cop_mob,
                                      B.reg_ppc,B.reg_ppc_mob,B.reg_ppc_cop,B.reg_ppc_cop_mob,           
                                      B.first_dep_ppc,B.first_dep_ppc_mob,B.first_dep_ppc_cop,B.first_dep_ppc_cop_mob,
                                      B.reg_media_tlt,B.reg_media_tlt_mob,B.reg_media_tlt_cop,B.reg_media_tlt_cop_mob,           
                                      B.first_dep_media_tlt,B.first_dep_media_tlt_mob,B.first_dep_media_tlt_cop,B.first_dep_media_tlt_cop_mob,
                                      B.reg_media_tlv,B.reg_media_tlv_mob,B.reg_media_tlv_cop,B.reg_media_tlv_cop_mob,          
                                      B.first_dep_media_tlv,B.first_dep_media_tlv_mob,B.first_dep_media_tlv_cop,B.first_dep_media_tlv_cop_mob 
                                   )
BULK COLLECT INTO v_return
FROM
(SELECT     
           --TOTAL
           sum(A.total_reg) total_reg,  
           sum(A.total_reg_mob) total_reg_mob,  
           sum(A.total_reg_cop) total_reg_cop,  
           sum(A.total_reg_cop_mob) total_reg_cop_mob,
           
           sum(A.total_first_dep) total_first_dep, 
           sum(A.total_first_dep_mob) total_first_dep_mob, 
           sum(A.total_first_dep_cop) total_first_dep_cop, 
           sum(A.total_first_dep_cop_mob) total_first_dep_cop_mob,
           
           --AFFILIATE
           sum(A.reg_affiliates) reg_affiliates, 
           sum(A.reg_affiliates_mob) reg_affiliates_mob, 
           sum(A.reg_affiliates_cop) reg_affiliates_cop, 
           sum(A.reg_affiliates_cop_mob) reg_affiliates_cop_mob, 
          
           sum(A.first_dep_affiliates) first_dep_affiliates, 
           sum(A.first_dep_affiliates_mob) first_dep_affiliates_mob, 
           sum(A.first_dep_affiliates_cop) first_dep_affiliates_cop, 
           sum(A.first_dep_affiliates_cop_mob) first_dep_affiliates_cop_mob,
           
           --Tony and Jason FD
           sum(A.reg_tony_jason) reg_tony_jason, 
           sum(A.reg_tony_jason_mob) reg_tony_jason_mob, 
           sum(A.reg_tony_jason_cop) reg_tony_jason_cop, 
           sum(A.reg_tony_jason_cop_mob) reg_tony_jason_cop_mob, 
          
           sum(A.first_dep_tony_jason) first_dep_tony_jason, 
           sum(A.first_dep_tony_jason_mob) first_dep_tony_jason_mob, 
           sum(A.first_dep_tony_jason_cop) first_dep_tony_jason_cop, 
           sum(A.first_dep_tony_jason_cop_mob) first_dep_tony_jason_cop_mob,
           
           --Google D FD
           sum(A.reg_google_d) reg_google_d, 
           sum(A.reg_google_d_mob) reg_google_d_mob, 
           sum(A.reg_google_d_cop) reg_google_d_cop, 
           sum(A.reg_google_d_cop_mob) reg_google_d_cop_mob, 
          
           sum(A.first_dep_google_d) first_dep_google_d, 
           sum(A.first_dep_google_d_mob) first_dep_google_d_mob, 
           sum(A.first_dep_google_d_cop) first_dep_google_d_cop, 
           sum(A.first_dep_google_d_cop_mob) first_dep_google_d_cop_mob,
           
           
           --Beni FD
           sum(A.reg_beni) reg_beni, 
           sum(A.reg_beni_mob) reg_beni_mob, 
           sum(A.reg_beni_cop) reg_beni_cop, 
           sum(A.reg_beni_cop_mob) reg_beni_cop_mob, 
          
           sum(A.first_dep_beni) first_dep_beni, 
           sum(A.first_dep_beni_mob) first_dep_beni_mob, 
           sum(A.first_dep_beni_cop) first_dep_beni_cop, 
           sum(A.first_dep_beni_cop_mob) first_dep_beni_cop_mob,           
           
           --FREE
           sum(A.reg_free) reg_free, 
           sum(A.reg_free_cop) reg_free_cop, 
           sum(A.reg_free_mob) reg_free_mob, 
           sum(A.reg_free_cop_mob) reg_free_cop_mob, 
          
           sum(A.first_dep_free) first_dep_free, 
           sum(A.first_dep_free_mob) first_dep_free_mob, 
           sum(A.first_dep_free_cop) first_dep_free_cop, 
           sum(A.first_dep_free_cop_mob) first_dep_free_cop_mob,
           
           --PPC
           sum(A.reg_ppc) reg_ppc, 
           sum(A.reg_ppc_mob) reg_ppc_mob, 
           sum(A.reg_ppc_cop) reg_ppc_cop, 
           sum(A.reg_ppc_cop_mob) reg_ppc_cop_mob, 
          
           sum(A.first_dep_ppc) first_dep_ppc, 
           sum(A.first_dep_ppc_mob) first_dep_ppc_mob, 
           sum(A.first_dep_ppc_cop) first_dep_ppc_cop, 
           sum(A.first_dep_ppc_cop_mob) first_dep_ppc_cop_mob,

          --Media Tal T  
           sum(A.reg_media_tlt) reg_media_tlt, 
           sum(A.reg_media_tlt_mob) reg_media_tlt_mob, 
           sum(A.reg_media_tlt_cop) reg_media_tlt_cop, 
           sum(A.reg_media_tlt_cop_mob) reg_media_tlt_cop_mob, 
          
           sum(A.first_dep_media_tlt) first_dep_media_tlt, 
           sum(A.first_dep_media_tlt_mob) first_dep_media_tlt_mob, 
           sum(A.first_dep_media_tlt_cop) first_dep_media_tlt_cop, 
           sum(A.first_dep_media_tlt_cop_mob) first_dep_media_tlt_cop_mob,
           
           --MEDIA TLV  
           sum(A.reg_media_tlv) reg_media_tlv, 
           sum(A.reg_media_tlv_mob) reg_media_tlv_mob, 
           sum(A.reg_media_tlv_cop) reg_media_tlv_cop, 
           sum(A.reg_media_tlv_cop_mob) reg_media_tlv_cop_mob, 
          
           sum(A.first_dep_media_tlv) first_dep_media_tlv, 
           sum(A.first_dep_media_tlv_mob) first_dep_media_tlv_mob, 
           sum(A.first_dep_media_tlv_cop) first_dep_media_tlv_cop, 
           sum(A.first_dep_media_tlv_cop_mob) first_dep_media_tlv_cop_mob
        FROM( 
  select      
             0 total_reg, 
             0 total_reg_mob, 
             0 total_reg_cop, 
             0 total_reg_cop_mob, 
            --TOTAL FD
             count(case when t.writer_id   not in (200, 10000, 20000)  then u.id else null end) total_first_dep, 
             count(case when t.writer_id   in (200)  then u.id else null end) total_first_dep_mob, 
             count(case when t.writer_id   in (10000)  then u.id else null end) total_first_dep_cop, 
             count(case when t.writer_id   in (20000) then u.id else null end) total_first_dep_cop_mob,
             
             
             0 reg_affiliates, 
             0 reg_affiliates_mob,
             0 reg_affiliates_cop,  
             0 reg_affiliates_cop_mob,
             --AFFILIATE FD
             count(case when ums.channel = 'affiliete'
                   AND t.writer_id not in (200, 10000, 20000)
                   then u.id else null end) first_dep_affiliates,                          
             count(case when ums.channel = 'affiliete' 
                   AND t.writer_id in (200)
                   then u.id else null end) first_dep_affiliates_mob,                        
             count(case when ums.channel = 'affiliete'
                   AND t.writer_id in (10000)
                   then u.id else null end) first_dep_affiliates_cop,
             count(case when ums.channel = 'affiliete' 
                   AND t.writer_id in (20000)
                   then u.id else null end) first_dep_affiliates_cop_mob, 

             0 reg_tony_jason, 
             0 reg_tony_jason_mob,
             0 reg_tony_jason_cop,  
             0 reg_tony_jason_cop_mob,
             --Tony and Jason FD
             count(case when ums.channel = 'Jason and Tony'
                   AND t.writer_id not in (200, 10000, 20000)
                   then u.id else null end) first_dep_tony_jason,                          
             count(case when ums.channel = 'Jason and Tony' 
                   AND t.writer_id in (200)
                   then u.id else null end) first_dep_tony_jason_mob,                        
             count(case when ums.channel = 'Jason and Tony'
                   AND t.writer_id in (10000)
                   then u.id else null end) first_dep_tony_jason_cop,
             count(case when ums.channel = 'Jason and Tony' 
                   AND t.writer_id in (20000)
                   then u.id else null end) first_dep_tony_jason_cop_mob,
                     
             0 reg_google_d, 
             0 reg_google_d_mob,
             0 reg_google_d_cop,  
             0 reg_google_d_cop_mob,
             --google display FD
             count(case when ums.channel = 'google display'
                   AND t.writer_id not in (200, 10000, 20000)
                   then u.id else null end) first_dep_google_d,                          
             count(case when ums.channel = 'google display' 
                   AND t.writer_id in (200)
                   then u.id else null end) first_dep_google_d_mob,                        
             count(case when ums.channel = 'google display'
                   AND t.writer_id in (10000)
                   then u.id else null end) first_dep_google_d_cop,
             count(case when ums.channel = 'google display' 
                   AND t.writer_id in (20000)
                   then u.id else null end) first_dep_google_d_cop_mob,
                     
             0 reg_beni, 
             0 reg_beni_mob,
             0 reg_beni_cop,  
             0 reg_beni_cop_mob,
             --Beni FD
             count(case when ums.channel = 'Beni'
                   AND t.writer_id not in (200, 10000, 20000)
                   then u.id else null end) first_dep_beni,                          
             count(case when ums.channel = 'Beni' 
                   AND t.writer_id in (200)
                   then u.id else null end) first_dep_beni_mob,                        
             count(case when ums.channel = 'Beni'
                   AND t.writer_id in (10000)
                   then u.id else null end) first_dep_beni_cop,
             count(case when ums.channel = 'Beni' 
                   AND t.writer_id in (20000)
                   then u.id else null end) first_dep_beni_cop_mob,                                                               
             
             
             0 reg_free, 
             0 reg_free_mob, 
             0 reg_free_cop, 
             0 reg_free_cop_mob,
            --FREE FD
             count(case when ums.channel = 'free'
                       AND t.writer_id   not in (200, 10000, 20000)  then u.id else null end) first_dep_free, 
             count(case when ums.channel = 'free' 
                       AND t.writer_id   in (200)  then u.id else null end) first_dep_free_mob, 
             count(case when ums.channel = 'free'
                       AND t.writer_id   in (10000)  then u.id else null end) first_dep_free_cop, 
             count(case when ums.channel = 'free' 
                       AND t.writer_id   in (20000)  then u.id else null end) first_dep_free_cop_mob,
                       
             0 reg_ppc, 
             0 reg_ppc_mob, 
             0 reg_ppc_cop, 
             0 reg_ppc_cop_mob,
             --PPC FD
             count(case when ums.channel = 'ppc'
                         AND t.writer_id   not in (200, 10000, 20000)  then u.id else null end) first_dep_ppc, 
             count(case when ums.channel = 'ppc' 
                        AND t.writer_id   in (200)  then u.id else null end) first_dep_ppc_mob, 
             count(case when ums.channel = 'ppc' 
                        AND t.writer_id   in (10000)  then u.id else null end) first_dep_ppc_cop, 
             count(case when ums.channel = 'ppc' 
                        AND t.writer_id   in (20000)  then u.id else null end) first_dep_ppc_cop_mob,                                         
  
             0 reg_media_tlt, 
             0 reg_media_tlt_mob, 
             0 reg_media_tlt_cop, 
             0 reg_media_tlt_cop_mob,
             --Media Tal T 
             count(case when ums.channel = 'Media Tal T'
                      AND t.writer_id not in (200, 10000, 20000)
                   then u.id else null end) first_dep_media_tlt,
             count(case when ums.channel = 'Media Tal T'
                     AND t.writer_id in (200)
                   then u.id else null end) first_dep_media_tlt_mob,
             count(case when ums.channel = 'Media Tal T'
                      AND t.writer_id in (10000)
                   then u.id else null end) first_dep_media_tlt_cop,
             count(case when ums.channel = 'Media Tal T'
                      AND t.writer_id in (20000)
                   then u.id else null end) first_dep_media_tlt_cop_mob,
                     
                   
             0 reg_media_tlv, 
             0 reg_media_tlv_mob, 
             0 reg_media_tlv_cop, 
             0 reg_media_tlv_cop_mob,
             ----MEDIA TLV 
             count(case when ums.channel = 'Media Tal V'
                      AND t.writer_id not in (200, 10000, 20000)
                   then u.id else null end) first_dep_media_tlv,
             count(case when ums.channel = 'Media Tal V'
                     AND t.writer_id in (200)
                   then u.id else null end) first_dep_media_tlv_mob,
             count(case when ums.channel = 'Media Tal V'
                      AND t.writer_id in (10000)
                   then u.id else null end) first_dep_media_tlv_cop,
             count(case when ums.channel = 'Media Tal V'
                      AND t.writer_id in (20000)
                   then u.id else null end) first_dep_media_tlv_cop_mob
                     
             
         from             
             users u, 
             transactions t,
             USERS_MARKETING_SOURCES ums
           where  
             u.id = t.user_id 
             and u.class_id <> 0 
             and u.id = ums.user_id
             and t.id = u.first_deposit_id 
             and t.type_id in (select tt.id from  transaction_types tt where tt.class_type =  1 ) 
             and t.time_created between trunc(sysdate -  pi_days_before) and trunc(sysdate) 
             and t.status_id in ( 2,7,8 )
        union
        
     select      
            --TOTAL REG
             count(case when u.writer_id   not in (200, 10000, 20000)  then u.id else null end) total_reg, 
             count(case when u.writer_id   in (200)  then u.id else null end) total_reg_mob, 
             count(case when u.writer_id   in (10000)  then u.id else null end) total_reg_cop, 
             count(case when u.writer_id   in (20000) then u.id else null end) total_reg_cop_mob, 
             --
             0 total_first_dep, 
             0 total_first_dep_mob, 
             0 total_first_dep_cop, 
             0 total_first_dep_cop_mob,
             
                          
             --AFFILIATE FD
             count(case when ums.channel = 'affiliete' 
                   AND u.writer_id not in (200, 10000, 20000)
                   then u.id else null end) reg_affiliates,                          
             count(case when ums.channel = 'affiliete' 
                   AND u.writer_id in (200)
                   then u.id else null end) reg_affiliates_mob,                        
             count(case when ums.channel = 'affiliete' 
                   AND u.writer_id in (10000)
                   then u.id else null end) reg_affiliates_cop,
             count(case when ums.channel = 'affiliete' 
                   AND u.writer_id in (20000)
                   then u.id else null end) reg_affiliates_cop_mob, 
             --
             0 first_dep_affiliates,                          
             0 first_dep_affiliates_mob,                        
             0 first_dep_affiliates_cop,
             0 first_dep_affiliates_cop_mob,
             
             
             --Tony and Jason FD 
             count(case when ums.channel = 'Jason and Tony' 
                   AND u.writer_id not in (200, 10000, 20000)
                   then u.id else null end) reg_tony_jason,                          
             count(case when ums.channel = 'Jason and Tony' 
                   AND u.writer_id in (200)
                   then u.id else null end) reg_tony_jason_mob,                        
             count(case when ums.channel = 'Jason and Tony' 
                   AND u.writer_id in (10000)
                   then u.id else null end) reg_tony_jason_cop,
             count(case when ums.channel = 'Jason and Tony' 
                   AND u.writer_id in (20000)
                   then u.id else null end) reg_tony_jason_cop_mob, 
             --
             0 first_dep_tony_jason,                          
             0 first_dep_tony_jason_mob,                        
             0 first_dep_tony_jason_cop,
             0 first_dep_tony_jason_cop_mob, 
             
             --Google display  FD
             count(case when ums.channel = 'google display' 
                   AND u.writer_id not in (200, 10000, 20000)
                   then u.id else null end) reg_google_d,                          
             count(case when ums.channel = 'google display' 
                   AND u.writer_id in (200)
                   then u.id else null end) reg_google_d_mob,                        
             count(case when ums.channel = 'google display' 
                   AND u.writer_id in (10000)
                   then u.id else null end) reg_google_d_cop,
             count(case when ums.channel = 'google display' 
                   AND u.writer_id in (20000)
                   then u.id else null end) reg_google_d_cop_mob, 
             --
             0 first_dep_google_d,                          
             0 first_dep_google_d_mob,                        
             0 first_dep_google_d_cop,
             0 first_dep_google_d_cop_mob,
             
             --Beni display  FD
             count(case when ums.channel = 'Beni' 
                   AND u.writer_id not in (200, 10000, 20000)
                   then u.id else null end) reg_beni,                          
             count(case when ums.channel = 'Beni' 
                   AND u.writer_id in (200)
                   then u.id else null end) reg_beni_mob,                        
             count(case when ums.channel = 'Beni' 
                   AND u.writer_id in (10000)
                   then u.id else null end) reg_beni_cop,
             count(case when ums.channel = 'Beni' 
                   AND u.writer_id in (20000)
                   then u.id else null end) reg_beni_cop_mob, 
             --
             0 first_dep_beni,                          
             0 first_dep_beni_mob,                        
             0 first_dep_beni_cop,
             0 first_dep_beni_cop_mob,   

                          
            --FREE FD
             count(case when ums.channel = 'free'
                       AND u.writer_id   not in (200, 10000, 20000)  then u.id else null end) reg_free, 
             count(case when ums.channel = 'free' 
                       AND u.writer_id   in (200)  then u.id else null end) reg_free_mob, 
             count(case when ums.channel = 'free' 
                       AND u.writer_id   in (10000)  then u.id else null end) reg_free_cop, 
             count(case when ums.channel = 'free' 
                       AND u.writer_id   in (20000)  then u.id else null end) reg_free_cop_mob,
            --
             0 first_dep_free, 
             0 first_dep_free_mob, 
             0 first_dep_free_cop, 
             0 first_dep_free_cop_mob,                       
                       

             --PPC FD
             count(case when ums.channel = 'ppc' 
                        AND u.writer_id   not in (200, 10000, 20000)  then u.id else null end) reg_ppc, 
             count(case when ums.channel = 'ppc' 
                        AND u.writer_id   in (200)  then u.id else null end) reg_ppc_mob, 
             count(case when ums.channel = 'ppc' 
                        AND u.writer_id   in (10000)  then u.id else null end) reg_ppc_cop, 
             count(case when ums.channel = 'ppc' 
                        AND u.writer_id   in (20000)  then u.id else null end) reg_ppc_cop_mob,
             --
             0 first_dep_ppc, 
             0 first_dep_ppc_mob, 
             0 first_dep_ppc_cop, 
             0 first_dep_ppc_cop_mob,                                                                     
  

             --Media Tal T 
             count(case when ums.channel = 'Media Tal T'
                   AND u.writer_id not in (200, 10000, 20000)
                   then u.id else null end) reg_media_tlt,
             count(case when ums.channel = 'Media Tal T'
                   AND u.writer_id in (200)
                   then u.id else null end) reg_media_tlt_mob,
             count(case when ums.channel = 'Media Tal T'
                   AND u.writer_id in (10000)
                   then u.id else null end) reg_media_tlt_cop,
             count(case when ums.channel = 'Media Tal T'
                   AND u.writer_id in (20000)
                   then u.id else null end) reg_media_tlt_cop_mob,
             -- 
            0 first_dep_media_tlt,
            0 first_dep_media_tlt_mob,
            0 first_dep_media_tlt_cop,
            0 first_dep_media_tlt_cop_mob,
            
            
             ----MEDIA TLV
             count(case when ums.channel = 'Media Tal V'
                   AND u.writer_id not in (200, 10000, 20000)
                   then u.id else null end) reg_media_tlv,
             count(case when ums.channel = 'Media Tal V'
                   AND u.writer_id in (200)
                   then u.id else null end) reg_media_tlv_mob,
             count(case when ums.channel = 'Media Tal V'
                   AND u.writer_id in (10000)
                   then u.id else null end) reg_media_tlv_cop,
             count(case when ums.channel = 'Media Tal V'
                   AND u.writer_id in (20000)
                   then u.id else null end) reg_media_tlv_cop_mob,
             -- 
            0 first_dep_media_tlv,
            0 first_dep_media_tlv_mob,
            0 first_dep_media_tlv_cop,
            0 first_dep_media_tlv_cop_mob                     
                     
             
         from             
             users u, 
             USERS_MARKETING_SOURCES ums

           where  
             u.class_id <> 0 
             and u.id = ums.user_id 
             and u.time_created between trunc(sysdate - pi_days_before) and trunc(sysdate) 
     ) A
) B;
RETURN v_return;
END GET_NEW_CUST_PER_SOURCE_REPORT;

FUNCTION GET_CUST_FTD_WITHDREW_REPORT (pi_days_before number)
RETURN NEW_CUST_FTD_WITHDREW_TBL_TYPE
AS
v_return  NEW_CUST_FTD_WITHDREW_TBL_TYPE;
BEGIN
SELECT NEW_CUST_FTD_WITHDREW_OBJ_TYPE(  B.out_this_wthd ,B.out_this_wthd_mob ,B.out_this_wthd_cop ,B.out_this_wthd_cop_mob ,
                                        B.out_this_wthd_aff ,B.out_this_wthd_aff_mob ,B.out_this_wthd_aff_cop ,B.out_this_wthd_aff_cop_mob ,
                                        B.out_this_wthd_tj ,B.out_this_wthd_tj_mob ,B.out_this_wthd_tj_cop ,B.out_this_wthd_tj_cop_mob ,
                                        B.out_this_wthd_gd ,B.out_this_wthd_gd_mob ,B.out_this_wthd_gd_cop ,B.out_this_wthd_gd_cop_mob ,
                                        B.out_this_wthd_be ,B.out_this_wthd_be_mob ,B.out_this_wthd_be_cop ,B.out_this_wthd_be_cop_mob ,
                                        B.out_this_wthd_free ,B.out_this_wthd_free_mob ,B.out_this_wthd_free_cop ,B.out_this_wthd_free_cop_mob ,
                                        B.out_this_wthd_ppc ,B.out_this_wthd_ppc_mob ,B.out_this_wthd_ppc_cop ,B.out_this_wthd_ppc_cop_mob ,
                                        B.out_this_wthd_tlt ,B.out_this_wthd_tlt_mob ,B.out_this_wthd_tlt_cop ,B.out_this_wthd_tlt_cop_mob ,
                                        B.out_this_wthd_tlv ,B.out_this_wthd_tlv_mob ,B.out_this_wthd_tlv_cop ,B.out_this_wthd_tlv_cop_mob ,
                                        B.out_wk_wthd ,B.out_wk_wthd_mob ,B.out_wk_wthd_cop ,B.out_wk_wthd_cop_mob ,
                                        B.out_wk_wthd_aff ,B.out_wk_wthd_aff_mob ,B.out_wk_wthd_aff_cop ,B.out_wk_wthd_aff_cop_mob ,
                                        B.out_wk_wthd_tj ,B.out_wk_wthd_tj_mob ,B.out_wk_wthd_tj_cop ,B.out_wk_wthd_tj_cop_mob ,
                                        B.out_wk_wthd_gd ,B.out_wk_wthd_gd_mob ,B.out_wk_wthd_gd_cop ,B.out_wk_wthd_gd_cop_mob ,
                                        B.out_wk_wthd_be ,B.out_wk_wthd_be_mob ,B.out_wk_wthd_be_cop ,B.out_wk_wthd_be_cop_mob ,
                                        B.out_wk_wthd_free ,B.out_wk_wthd_free_mob ,B.out_wk_wthd_free_cop ,B.out_wk_wthd_free_cop_mob ,
                                        B.out_wk_wthd_ppc ,B.out_wk_wthd_ppc_mob ,B.out_wk_wthd_ppc_cop ,B.out_wk_wthd_ppc_cop_mob ,
                                        B.out_wk_wthd_tlt ,B.out_wk_wthd_tlt_mob ,B.out_wk_wthd_tlt_cop ,B.out_wk_wthd_tlt_cop_mob ,
                                        B.out_wk_wthd_tlv ,B.out_wk_wthd_tlv_mob ,B.out_wk_wthd_tlv_cop ,B.out_wk_wthd_tlv_cop_mob 
                                   )
BULK COLLECT INTO v_return
FROM
(SELECT --TOTAL
       A.out_this_wthd,
       A.out_this_wthd_mob,
       A.out_this_wthd_cop, 
       A.out_this_wthd_cop_mob,
       --AFFILIATES
      A.out_this_wthd_aff,
      A.out_this_wthd_aff_mob,
      A.out_this_wthd_aff_cop,
      A.out_this_wthd_aff_cop_mob,

      --Jason and Tony
      A.out_this_wthd_tj,
      A.out_this_wthd_tj_mob,
      A.out_this_wthd_tj_cop,
      A.out_this_wthd_tj_cop_mob,
  
      --Google Display
      A.out_this_wthd_gd,
      A.out_this_wthd_gd_mob,
      A.out_this_wthd_gd_cop,
      A.out_this_wthd_gd_cop_mob, 
  
      --Beni
      A.out_this_wthd_be,
      A.out_this_wthd_be_mob,
      A.out_this_wthd_be_cop,
      A.out_this_wthd_be_cop_mob,          
      --FREE
     A.out_this_wthd_free,        
     A.out_this_wthd_free_mob,     
     A.out_this_wthd_free_cop,     
     A.out_this_wthd_free_cop_mob,
     --PPC
     A.out_this_wthd_ppc,
     A.out_this_wthd_ppc_mob,
     A.out_this_wthd_ppc_cop,
     A.out_this_wthd_ppc_cop_mob,
     --Media Tal T
     A.out_this_wthd_tlt,
     A.out_this_wthd_tlt_mob,
     A.out_this_wthd_tlt_cop,
     A.out_this_wthd_tlt_cop_mob,
     --Media Tal V
     A.out_this_wthd_tlv,
     A.out_this_wthd_tlv_mob,
     A.out_this_wthd_tlv_cop,
     A.out_this_wthd_tlv_cop_mob,

     -----------------------------------------------------------Prior weeks--------------------------------------- 
  
    A.out_wk_wthd,
    A.out_wk_wthd_mob,
    A.out_wk_wthd_cop,
    A.out_wk_wthd_cop_mob, 
       --AFFILIATES
      A.out_wk_wthd_aff,
      A.out_wk_wthd_aff_mob,
      A.out_wk_wthd_aff_cop,
      A.out_wk_wthd_aff_cop_mob,
      
     --Tony and Jason
      A.out_wk_wthd_tj,
      A.out_wk_wthd_tj_mob,
      A.out_wk_wthd_tj_cop,
      A.out_wk_wthd_tj_cop_mob,
     --Google display
      A.out_wk_wthd_gd,
      A.out_wk_wthd_gd_mob,
      A.out_wk_wthd_gd_cop,
      A.out_wk_wthd_gd_cop_mob, 
      
     --Beni
      A.out_wk_wthd_be,
      A.out_wk_wthd_be_mob,
      A.out_wk_wthd_be_cop,
      A.out_wk_wthd_be_cop_mob,      
      
      --FREE
     A.out_wk_wthd_free,        
     A.out_wk_wthd_free_mob,     
     A.out_wk_wthd_free_cop,     
     A.out_wk_wthd_free_cop_mob,
     --PPC
     A.out_wk_wthd_ppc,
     A.out_wk_wthd_ppc_mob,
     A.out_wk_wthd_ppc_cop,
     A.out_wk_wthd_ppc_cop_mob,
     --Media Tal T
     A.out_wk_wthd_tlt,
     A.out_wk_wthd_tlt_mob,
     A.out_wk_wthd_tlt_cop,
     A.out_wk_wthd_tlt_cop_mob,
     --Media Tal V     
     A.out_wk_wthd_tlv,
     A.out_wk_wthd_tlv_mob,
     A.out_wk_wthd_tlv_cop,
     A.out_wk_wthd_tlv_cop_mob
                 
FROM   
(
          SELECT 
             --TOTAL
               count(case when ftd.time_created >= trunc(sysdate - pi_days_before) and ftd.writer_id   not in (200, 10000, 20000)  then user_id else null end) out_this_wthd, 
               count(case when ftd.time_created >= trunc(sysdate - pi_days_before) and ftd.writer_id   in (200) then user_id else null end) out_this_wthd_mob, 
               count(case when ftd.time_created >= trunc(sysdate - pi_days_before) and ftd.writer_id   in (10000)  then user_id else null end) out_this_wthd_cop, 
               count(case when ftd.time_created >= trunc(sysdate - pi_days_before) and ftd.writer_id   in (20000)  then user_id else null end) out_this_wthd_cop_mob, 
               --AFFILIATES
               count(case when ftd.time_created >= trunc(sysdate - pi_days_before) 
                       AND
                       ftd.channel = 'affiliete' 
                       AND ftd.writer_id   not in (200, 10000, 20000)  then user_id else null end) out_this_wthd_aff, 
               count(case when ftd.time_created >= trunc(sysdate - pi_days_before) 
                       AND
                       ftd.channel = 'affiliete' 
                       AND ftd.writer_id   in (200) then user_id else null end) out_this_wthd_aff_mob, 
               count(case when ftd.time_created >= trunc(sysdate - pi_days_before) 
                       AND
                       ftd.channel = 'affiliete' 
                       AND ftd.writer_id   in (10000)  then user_id else null end) out_this_wthd_aff_cop, 
               count(case when ftd.time_created >= trunc(sysdate - pi_days_before) 
                       AND
                       ftd.channel = 'affiliete'
                       AND ftd.writer_id   in (20000)  then user_id else null end) out_this_wthd_aff_cop_mob,
               --Tony and Jason
               count(case when ftd.time_created >= trunc(sysdate - pi_days_before) 
                       AND
                       ftd.channel = 'Jason and Tony' 
                       AND ftd.writer_id   not in (200, 10000, 20000)  then user_id else null end) out_this_wthd_tj, 
               count(case when ftd.time_created >= trunc(sysdate - pi_days_before) 
                       AND
                       ftd.channel = 'Jason and Tony' 
                       AND ftd.writer_id   in (200) then user_id else null end) out_this_wthd_tj_mob, 
               count(case when ftd.time_created >= trunc(sysdate - pi_days_before) 
                       AND
                       ftd.channel = 'Jason and Tony' 
                       AND ftd.writer_id   in (10000)  then user_id else null end) out_this_wthd_tj_cop, 
               count(case when ftd.time_created >= trunc(sysdate - pi_days_before) 
                       AND
                       ftd.channel = 'Jason and Tony'
                       AND ftd.writer_id   in (20000)  then user_id else null end) out_this_wthd_tj_cop_mob,
               --Google Displey
               count(case when ftd.time_created >= trunc(sysdate - pi_days_before) 
                       AND
                       ftd.channel = 'google display' 
                       AND ftd.writer_id   not in (200, 10000, 20000)  then user_id else null end) out_this_wthd_gd, 
               count(case when ftd.time_created >= trunc(sysdate - pi_days_before) 
                       AND
                       ftd.channel = 'google display' 
                       AND ftd.writer_id   in (200) then user_id else null end) out_this_wthd_gd_mob, 
               count(case when ftd.time_created >= trunc(sysdate - pi_days_before) 
                       AND
                       ftd.channel = 'google display' 
                       AND ftd.writer_id   in (10000)  then user_id else null end) out_this_wthd_gd_cop, 
               count(case when ftd.time_created >= trunc(sysdate - pi_days_before) 
                       AND
                       ftd.channel = 'google display'
                       AND ftd.writer_id   in (20000)  then user_id else null end) out_this_wthd_gd_cop_mob,

               --Beni
               count(case when ftd.time_created >= trunc(sysdate - pi_days_before) 
                       AND
                       ftd.channel = 'Beni' 
                       AND ftd.writer_id   not in (200, 10000, 20000)  then user_id else null end) out_this_wthd_be, 
               count(case when ftd.time_created >= trunc(sysdate - pi_days_before) 
                       AND
                       ftd.channel = 'Beni' 
                       AND ftd.writer_id   in (200) then user_id else null end) out_this_wthd_be_mob, 
               count(case when ftd.time_created >= trunc(sysdate - pi_days_before) 
                       AND
                       ftd.channel = 'Beni' 
                       AND ftd.writer_id   in (10000)  then user_id else null end) out_this_wthd_be_cop, 
               count(case when ftd.time_created >= trunc(sysdate - pi_days_before) 
                       AND
                       ftd.channel = 'Beni'
                       AND ftd.writer_id   in (20000)  then user_id else null end) out_this_wthd_be_cop_mob,                                                                        
               --FREE         
               count(case when ftd.time_created >= trunc(sysdate - pi_days_before) 
                          AND ftd.channel = 'free'            
                          AND ftd.writer_id   not in (200, 10000, 20000)  then user_id else null end) out_this_wthd_free, 
               count(case when ftd.time_created >= trunc(sysdate - pi_days_before) 
                          AND ftd.channel = 'free' 
                          AND ftd.writer_id   in (200) then user_id else null end) out_this_wthd_free_mob, 
               count(case when ftd.time_created >= trunc(sysdate - pi_days_before) 
                          AND ftd.channel = 'free' 
                          AND ftd.writer_id   in (10000)  then user_id else null end) out_this_wthd_free_cop, 
               count(case when ftd.time_created >= trunc(sysdate - pi_days_before) 
                          AND ftd.channel = 'free' 
                          AND ftd.writer_id   in (20000)  then user_id else null end) out_this_wthd_free_cop_mob,
                          
               --PPC
               count(case when ftd.time_created >= trunc(sysdate - pi_days_before) 
                          AND ftd.channel = 'ppc'
                          AND ftd.writer_id   not in (200, 10000, 20000)  then user_id else null end) out_this_wthd_ppc, 
               count(case when ftd.time_created >= trunc(sysdate - pi_days_before) 
                          AND ftd.channel = 'ppc'
                          AND ftd.writer_id   in (200) then user_id else null end) out_this_wthd_ppc_mob, 
               count(case when ftd.time_created >= trunc(sysdate - pi_days_before) 
                          AND ftd.channel = 'ppc'
                          AND ftd.writer_id   in (10000)  then user_id else null end) out_this_wthd_ppc_cop, 
               count(case when ftd.time_created >= trunc(sysdate - pi_days_before) 
                          AND ftd.channel = 'ppc'
                          AND ftd.writer_id   in (20000)  then user_id else null end) out_this_wthd_ppc_cop_mob,
               --Media Tal T
               count(case when ftd.time_created >= trunc(sysdate - pi_days_before) 
                   AND ftd.channel = 'Media Tal T'
                   AND ftd.writer_id   not in (200, 10000, 20000)  then user_id else null end) out_this_wthd_tlt, 
               count(case when ftd.time_created >= trunc(sysdate - pi_days_before) 
                   AND ftd.channel = 'Media Tal T'
                   AND ftd.writer_id   in (200) then user_id else null end) out_this_wthd_tlt_mob, 
               count(case when ftd.time_created >= trunc(sysdate - pi_days_before) 
                  AND ftd.channel = 'Media Tal T'
                  AND ftd.writer_id   in (10000)  then user_id else null end) out_this_wthd_tlt_cop, 
               count(case when ftd.time_created >= trunc(sysdate - pi_days_before) 
                  AND ftd.channel = 'Media Tal T'
                  AND ftd.writer_id   in (20000)  then user_id else null end) out_this_wthd_tlt_cop_mob,
                  
                --Media Tal V
               count(case when ftd.time_created >= trunc(sysdate - pi_days_before) 
                   AND ftd.channel = 'Media Tal V'
                   AND ftd.writer_id   not in (200, 10000, 20000)  then user_id else null end) out_this_wthd_tlv, 
               count(case when ftd.time_created >= trunc(sysdate - pi_days_before) 
                   AND ftd.channel = 'Media Tal V'
                   AND ftd.writer_id   in (200) then user_id else null end) out_this_wthd_tlv_mob, 
               count(case when ftd.time_created >= trunc(sysdate - pi_days_before) 
                  AND ftd.channel = 'Media Tal V'
                  AND ftd.writer_id   in (10000)  then user_id else null end) out_this_wthd_tlv_cop, 
               count(case when ftd.time_created >= trunc(sysdate - pi_days_before) 
                  AND ftd.channel = 'Media Tal V'
                  AND ftd.writer_id   in (20000)  then user_id else null end) out_this_wthd_tlv_cop_mob,                            
               -----------------------------------------------------------Prior weeks---------------------------------------                 
                            --TOTAL
               count(case when ftd.time_created < trunc(sysdate - pi_days_before) and ftd.writer_id   not in (200, 10000, 20000)  then user_id else null end) out_wk_wthd, 
               count(case when ftd.time_created < trunc(sysdate - pi_days_before) and ftd.writer_id   in (200) then user_id else null end) out_wk_wthd_mob, 
               count(case when ftd.time_created < trunc(sysdate - pi_days_before) and ftd.writer_id   in (10000)  then user_id else null end) out_wk_wthd_cop, 
               count(case when ftd.time_created < trunc(sysdate - pi_days_before) and ftd.writer_id   in (20000)  then user_id else null end) out_wk_wthd_cop_mob, 
               --AFFILIATES
               count(case when ftd.time_created < trunc(sysdate - pi_days_before) 
                       AND
                       ftd.channel = 'affiliete' 
                       AND ftd.writer_id   not in (200, 10000, 20000)  then user_id else null end) out_wk_wthd_aff, 
               count(case when ftd.time_created < trunc(sysdate - pi_days_before) 
                       AND
                       ftd.channel = 'affiliete'
                       AND ftd.writer_id   in (200) then user_id else null end) out_wk_wthd_aff_mob, 
               count(case when ftd.time_created < trunc(sysdate - pi_days_before) 
                       AND
                       ftd.channel = 'affiliete' 
                       AND ftd.writer_id   in (10000)  then user_id else null end) out_wk_wthd_aff_cop, 
               count(case when ftd.time_created < trunc(sysdate - pi_days_before) 
                       AND
                       ftd.channel = 'affiliete'
                       AND ftd.writer_id   in (20000)  then user_id else null end) out_wk_wthd_aff_cop_mob,
               --Tony and Jason
               count(case when ftd.time_created < trunc(sysdate - pi_days_before) 
                       AND
                       ftd.channel = 'Jason and Tony' 
                       AND ftd.writer_id   not in (200, 10000, 20000)  then user_id else null end) out_wk_wthd_tj, 
               count(case when ftd.time_created < trunc(sysdate - pi_days_before) 
                       AND
                       ftd.channel = 'Jason and Tony'
                       AND ftd.writer_id   in (200) then user_id else null end) out_wk_wthd_tj_mob, 
               count(case when ftd.time_created < trunc(sysdate - pi_days_before) 
                       AND
                       ftd.channel = 'Jason and Tony' 
                       AND ftd.writer_id   in (10000)  then user_id else null end) out_wk_wthd_tj_cop, 
               count(case when ftd.time_created < trunc(sysdate - pi_days_before) 
                       AND
                       ftd.channel = 'Jason and Tony'
                       AND ftd.writer_id   in (20000)  then user_id else null end) out_wk_wthd_tj_cop_mob, 
               --Google dislpay
               count(case when ftd.time_created < trunc(sysdate - pi_days_before) 
                       AND
                       ftd.channel = 'google display' 
                       AND ftd.writer_id   not in (200, 10000, 20000)  then user_id else null end) out_wk_wthd_gd, 
               count(case when ftd.time_created < trunc(sysdate - pi_days_before) 
                       AND
                       ftd.channel = 'google display'
                       AND ftd.writer_id   in (200) then user_id else null end) out_wk_wthd_gd_mob, 
               count(case when ftd.time_created < trunc(sysdate - pi_days_before) 
                       AND
                       ftd.channel = 'google display' 
                       AND ftd.writer_id   in (10000)  then user_id else null end) out_wk_wthd_gd_cop, 
               count(case when ftd.time_created < trunc(sysdate - pi_days_before) 
                       AND
                       ftd.channel = 'google display'
                       AND ftd.writer_id   in (20000)  then user_id else null end) out_wk_wthd_gd_cop_mob,
               --Beni
               count(case when ftd.time_created < trunc(sysdate - pi_days_before) 
                       AND
                       ftd.channel = 'Beni' 
                       AND ftd.writer_id   not in (200, 10000, 20000)  then user_id else null end) out_wk_wthd_be, 
               count(case when ftd.time_created < trunc(sysdate - pi_days_before) 
                       AND
                       ftd.channel = 'Beni'
                       AND ftd.writer_id   in (200) then user_id else null end) out_wk_wthd_be_mob, 
               count(case when ftd.time_created < trunc(sysdate - pi_days_before) 
                       AND
                       ftd.channel = 'Beni' 
                       AND ftd.writer_id   in (10000)  then user_id else null end) out_wk_wthd_be_cop, 
               count(case when ftd.time_created < trunc(sysdate - pi_days_before) 
                       AND
                       ftd.channel = 'Beni'
                       AND ftd.writer_id   in (20000)  then user_id else null end) out_wk_wthd_be_cop_mob,                                                                      
               --FREE         
               count(case when ftd.time_created < trunc(sysdate - pi_days_before) 
                          AND ftd.channel = 'free'            
                          AND ftd.writer_id   not in (200, 10000, 20000)  then user_id else null end) out_wk_wthd_free, 
               count(case when ftd.time_created < trunc(sysdate - pi_days_before) 
                          AND ftd.channel = 'free' 
                          AND ftd.writer_id   in (200) then user_id else null end) out_wk_wthd_free_mob, 
               count(case when ftd.time_created < trunc(sysdate - pi_days_before) 
                          AND ftd.channel = 'free' 
                          AND ftd.writer_id   in (10000)  then user_id else null end) out_wk_wthd_free_cop, 
               count(case when ftd.time_created < trunc(sysdate - pi_days_before) 
                          AND ftd.channel = 'free' 
                          AND ftd.writer_id   in (20000)  then user_id else null end) out_wk_wthd_free_cop_mob,
                          
               --PPC
               count(case when ftd.time_created < trunc(sysdate - pi_days_before) 
                          AND ftd.channel = 'ppc'
                          AND ftd.writer_id   not in (200, 10000, 20000)  then user_id else null end) out_wk_wthd_ppc, 
               count(case when ftd.time_created < trunc(sysdate - pi_days_before) 
                          AND ftd.channel = 'ppc'
                          AND ftd.writer_id   in (200) then user_id else null end) out_wk_wthd_ppc_mob, 
               count(case when ftd.time_created < trunc(sysdate - pi_days_before) 
                          AND ftd.channel = 'ppc'
                          AND ftd.writer_id   in (10000)  then user_id else null end) out_wk_wthd_ppc_cop, 
               count(case when ftd.time_created < trunc(sysdate - pi_days_before) 
                          AND ftd.channel = 'ppc'
                          AND ftd.writer_id   in (20000)  then user_id else null end) out_wk_wthd_ppc_cop_mob,
               --Media Tal T
               count(case when ftd.time_created < trunc(sysdate - pi_days_before) 
                   AND ftd.channel = 'Media Tal T'
                   AND ftd.writer_id   not in (200, 10000, 20000)  then user_id else null end) out_wk_wthd_tlt, 
               count(case when ftd.time_created < trunc(sysdate - pi_days_before) 
                   AND ftd.channel = 'Media Tal T'
                   AND ftd.writer_id   in (200) then user_id else null end) out_wk_wthd_tlt_mob, 
               count(case when ftd.time_created < trunc(sysdate - pi_days_before) 
                  AND ftd.channel = 'Media Tal T'
                  AND ftd.writer_id   in (10000)  then user_id else null end) out_wk_wthd_tlt_cop, 
               count(case when ftd.time_created < trunc(sysdate - pi_days_before) 
                  AND ftd.channel = 'Media Tal T'
                  AND ftd.writer_id   in (20000)  then user_id else null end) out_wk_wthd_tlt_cop_mob,
                  
                  
               --Media Tal V
               count(case when ftd.time_created < trunc(sysdate - pi_days_before) 
                   AND ftd.channel = 'Media Tal V'
                   AND ftd.writer_id   not in (200, 10000, 20000)  then user_id else null end) out_wk_wthd_tlv, 
               count(case when ftd.time_created < trunc(sysdate - pi_days_before) 
                   AND ftd.channel = 'Media Tal V'
                   AND ftd.writer_id   in (200) then user_id else null end) out_wk_wthd_tlv_mob, 
               count(case when ftd.time_created < trunc(sysdate - pi_days_before) 
                  AND ftd.channel = 'Media Tal V'
                  AND ftd.writer_id   in (10000)  then user_id else null end) out_wk_wthd_tlv_cop, 
               count(case when ftd.time_created < trunc(sysdate - pi_days_before) 
                  AND ftd.channel = 'Media Tal V'
                  AND ftd.writer_id   in (20000)  then user_id else null end) out_wk_wthd_tlv_cop_mob        
                            
               FROM 
                ( select 
                   u.id as user_id, 
                   t.writer_id, 
                   ums.channel,
                   t.time_created, 
                   max(tw.time_created) as wtr_time 
                 from  
                   users u, 
                   transactions t, 
                   users_marketing_sources ums,
                   transactions tw,  
                   MIN_FTD_BY_SKIN_CURR min_ftd
                 where  
                   u.id = t.user_id 
                   and u.class_id <> 0 
                   and u.id = ums.user_id 
                   and t.id = u.first_deposit_id 
                   and t.type_id in (select tt.id from  transaction_types tt where tt.class_type =  1 ) 
                   and t.status_id in ( 2,7,8 ) 
                         and tw.user_id = u.id 
                         and tw.type_id in (select tt.id from  transaction_types tt where tt.class_type =  2) 
                         and tw.status_id in (2) 
                         and tw.time_created between trunc(sysdate - pi_days_before  ) and trunc(sysdate)  
                         and u.currency_id = min_ftd.curr_currency_id 
                         and u.skin_id = min_ftd.skin_id 
                         and u.balance < min_ftd.min_ftd 
                     group by 
                       u.id, t.writer_id, t.time_created, ums.channel ) ftd 
                 WHERE 
                   not exists (select distinct user_id from investments i where (i.time_created between ftd.time_created and ftd.wtr_time) and i.user_id = ftd.user_id)
        ) A
) B;
RETURN v_return;
END GET_CUST_FTD_WITHDREW_REPORT;


FUNCTION GET_FTD_SKIN_SOURCE_REPORT (pi_days_before number)
RETURN NEW_FTD_SKIN_SOURCE_TBL_TYPE
AS
v_return  NEW_FTD_SKIN_SOURCE_TBL_TYPE;
BEGIN
SELECT NEW_FTD_SKIN_SOURCE_OBJ_TYPE(  B.skin_name,B.sum_first_dep,B.sum_first_dep_mob,B.sum_first_dep_cop,B.sum_first_dep_cop_mob,
                                      B.total_first_dep,B.total_first_dep_mob,B.total_first_dep_cop,B.total_first_dep_cop_mob,
                                      B.sum_fd_affiliates,B.sum_fd_affiliates_mob,B.sum_fd_affiliates_cop,B.sum_fd_affiliates_cop_mob,
                                      B.first_dep_affiliates,B.first_dep_affiliates_mob,B.first_dep_affiliates_cop,B.first_dep_affiliates_cop_mob,
                                      B.sum_fd_tony_jason,B.sum_fd_tony_jason_mob,B.sum_fd_tony_jason_cop,B.sum_fd_tony_jason_cop_mob,
                                      B.first_dep_tony_jason,B.first_dep_tony_jason_mob,B.first_dep_tony_jason_cop,B.first_dep_tony_jason_cop_mob,
                                      B.sum_fd_google_d,B.sum_fd_google_d_mob,B.sum_fd_google_d_cop,B.sum_fd_google_d_cop_mob,
                                      B.first_dep_google_d,B.first_dep_google_d_mob,B.first_dep_google_d_cop,B.first_dep_google_d_cop_mob,
                                      B.sum_fd_beni,B.sum_fd_beni_mob,B.sum_fd_beni_cop,B.sum_fd_beni_cop_mob,
                                      B.first_dep_beni,B.first_dep_beni_mob,B.first_dep_beni_cop,B.first_dep_beni_cop_mob,
                                      B.sum_fd_free,B.sum_fd_free_mob,B.sum_fd_free_cop,B.sum_fd_free_cop_mob,
                                      B.first_dep_free,B.first_dep_free_mob,B.first_dep_free_cop,B.first_dep_free_cop_mob,
                                      B.sum_fd_ppc,B.sum_fd_ppc_mob,B.sum_fd_ppc_cop,B.sum_fd_ppc_cop_mob,
                                      B.first_dep_ppc,B.first_dep_ppc_mob,B.first_dep_ppc_cop,B.first_dep_ppc_cop_mob,
                                      B.sum_fd_media_tlt,B.sum_fd_media_tlt_mob,B.sum_fd_media_tlt_cop,B.sum_fd_media_tlt_cop_mob,
                                      B.first_dep_media_tlt,B.first_dep_media_tlt_mob,B.first_dep_media_tlt_cop,B.first_dep_media_tlt_cop_mob,
                                      B.sum_fd_media_tlv,B.sum_fd_media_tlv_mob,B.sum_fd_media_tlv_cop,B.sum_fd_media_tlv_cop_mob,
                                      B.first_dep_media_tlv,B.first_dep_media_tlv_mob,B.first_dep_media_tlv_cop,B.first_dep_media_tlv_cop_mob

                                   )
BULK COLLECT INTO v_return
FROM
(SELECT      
      A.skin_name,
      
      --TOTALS 
      sum_first_dep,
      sum_first_dep_mob,
      sum_first_dep_cop,
      sum_first_dep_cop_mob,
      total_first_dep,
      total_first_dep_mob,
      total_first_dep_cop,
      total_first_dep_cop_mob,
      
      --AFFILIATE FD 
      sum_fd_affiliates,
      sum_fd_affiliates_mob,
      sum_fd_affiliates_cop,
      sum_fd_affiliates_cop_mob,
      first_dep_affiliates,
      first_dep_affiliates_mob,
      first_dep_affiliates_cop,
      first_dep_affiliates_cop_mob,      
      
      --Tony and Jason 
      sum_fd_tony_jason,
      sum_fd_tony_jason_mob,
      sum_fd_tony_jason_cop,
      sum_fd_tony_jason_cop_mob,
      first_dep_tony_jason,
      first_dep_tony_jason_mob,
      first_dep_tony_jason_cop,
      first_dep_tony_jason_cop_mob,
      
      --Google Display
      sum_fd_google_d,
      sum_fd_google_d_mob,
      sum_fd_google_d_cop,
      sum_fd_google_d_cop_mob,
      first_dep_google_d,
      first_dep_google_d_mob,
      first_dep_google_d_cop,
      first_dep_google_d_cop_mob,      
      
      --Beni
      sum_fd_beni,
      sum_fd_beni_mob,
      sum_fd_beni_cop,
      sum_fd_beni_cop_mob,
      first_dep_beni,
      first_dep_beni_mob,
      first_dep_beni_cop,
      first_dep_beni_cop_mob,
      
      --FREE FD
      sum_fd_free,
      sum_fd_free_mob,
      sum_fd_free_cop,
      sum_fd_free_cop_mob,
      first_dep_free,
      first_dep_free_mob,
      first_dep_free_cop,
      first_dep_free_cop_mob,
      
      --PPC FD
      sum_fd_ppc,
      sum_fd_ppc_mob,
      sum_fd_ppc_cop,
      sum_fd_ppc_cop_mob,
      first_dep_ppc,
      first_dep_ppc_mob,
      first_dep_ppc_cop,
      first_dep_ppc_cop_mob,
      
      --Media Tal T 
      sum_fd_media_tlt,
      sum_fd_media_tlt_mob,
      sum_fd_media_tlt_cop,
      sum_fd_media_tlt_cop_mob,
      first_dep_media_tlt,
      first_dep_media_tlt_mob,
      first_dep_media_tlt_cop,
      first_dep_media_tlt_cop_mob,
      
      --Media Tal V       
      sum_fd_media_tlv,
      sum_fd_media_tlv_mob,
      sum_fd_media_tlv_cop,
      sum_fd_media_tlv_cop_mob,
      first_dep_media_tlv,
      first_dep_media_tlv_mob,
      first_dep_media_tlv_cop,
      first_dep_media_tlv_cop_mob     
       FROM 
  (select    
            s1.display_name as skin_name,
           --TOTAL FD
             sum(case when t.writer_id   not in (200, 10000, 20000)  then t.amount * t.rate/100 else 0 end) sum_first_dep, 
             sum(case when t.writer_id   in (200)   then t.amount * t.rate/100 else 0 end) sum_first_dep_mob, 
             sum(case when t.writer_id   in (10000) then t.amount * t.rate/100 else 0 end) sum_first_dep_cop, 
             sum(case when t.writer_id   in (20000) then t.amount * t.rate/100 else 0 end) sum_first_dep_cop_mob, 
             count(case when t.writer_id   not in (200, 10000, 20000)  then u.id else null end) total_first_dep, 
             count(case when t.writer_id   in (200)  then u.id else null end) total_first_dep_mob, 
             count(case when t.writer_id   in (10000)  then u.id else null end) total_first_dep_cop, 
             count(case when t.writer_id   in (20000) then u.id else null end) total_first_dep_cop_mob,
             
            --AFFILIATE FD 
             sum(case when ums.channel = 'affiliete' 
                   AND t.writer_id not in (200, 10000, 20000)
                  then t.amount * t.rate/100 else 0 end) sum_fd_affiliates,                          
             sum(case when ums.channel = 'affiliete' 
                   AND t.writer_id in (200)
                   then t.amount * t.rate/100 else 0 end) sum_fd_affiliates_mob,                        
             sum(case when ums.channel = 'affiliete' 
                   AND t.writer_id in (10000)
                   then t.amount * t.rate/100 else 0 end) sum_fd_affiliates_cop,
             sum(case when ums.channel = 'affiliete' 
                   AND t.writer_id in (20000)
                   then t.amount * t.rate/100 else 0 end) sum_fd_affiliates_cop_mob,
             count(case when ums.channel = 'affiliete' 
                   AND t.writer_id not in (200, 10000, 20000)
                   then u.id else null end) first_dep_affiliates,                          
             count(case when ums.channel = 'affiliete' 
                   AND t.writer_id in (200)
                   then u.id else null end) first_dep_affiliates_mob,                        
             count(case when ums.channel = 'affiliete' 
                   AND t.writer_id in (10000)
                   then u.id else null end) first_dep_affiliates_cop,
             count(case when ums.channel = 'affiliete' 
                   AND t.writer_id in (20000)
                   then u.id else null end) first_dep_affiliates_cop_mob,
            --Tony and Jason FD 
             sum(case when ums.channel = 'Jason and Tony' 
                   AND t.writer_id not in (200, 10000, 20000)
                  then t.amount * t.rate/100 else 0 end) sum_fd_tony_jason,                          
             sum(case when ums.channel = 'Jason and Tony' 
                   AND t.writer_id in (200)
                   then t.amount * t.rate/100 else 0 end) sum_fd_tony_jason_mob,                        
             sum(case when ums.channel = 'Jason and Tony' 
                   AND t.writer_id in (10000)
                   then t.amount * t.rate/100 else 0 end) sum_fd_tony_jason_cop,
             sum(case when ums.channel = 'Jason and Tony' 
                   AND t.writer_id in (20000)
                   then t.amount * t.rate/100 else 0 end) sum_fd_tony_jason_cop_mob,
             count(case when ums.channel = 'Jason and Tony' 
                   AND t.writer_id not in (200, 10000, 20000)
                   then u.id else null end) first_dep_tony_jason,                          
             count(case when ums.channel = 'Jason and Tony' 
                   AND t.writer_id in (200)
                   then u.id else null end) first_dep_tony_jason_mob,                        
             count(case when ums.channel = 'Jason and Tony' 
                   AND t.writer_id in (10000)
                   then u.id else null end) first_dep_tony_jason_cop,
             count(case when ums.channel = 'Jason and Tony' 
                   AND t.writer_id in (20000)
                   then u.id else null end) first_dep_tony_jason_cop_mob,                      

            --Google Display FD 
             sum(case when ums.channel = 'google display' 
                   AND t.writer_id not in (200, 10000, 20000)
                  then t.amount * t.rate/100 else 0 end) sum_fd_google_d,                          
             sum(case when ums.channel = 'google display' 
                   AND t.writer_id in (200)
                   then t.amount * t.rate/100 else 0 end) sum_fd_google_d_mob,                        
             sum(case when ums.channel = 'google display' 
                   AND t.writer_id in (10000)
                   then t.amount * t.rate/100 else 0 end) sum_fd_google_d_cop,
             sum(case when ums.channel = 'google display' 
                   AND t.writer_id in (20000)
                   then t.amount * t.rate/100 else 0 end) sum_fd_google_d_cop_mob,
             count(case when ums.channel = 'google display' 
                   AND t.writer_id not in (200, 10000, 20000)
                   then u.id else null end) first_dep_google_d,                          
             count(case when ums.channel = 'google display' 
                   AND t.writer_id in (200)
                   then u.id else null end) first_dep_google_d_mob,                        
             count(case when ums.channel = 'google display' 
                   AND t.writer_id in (10000)
                   then u.id else null end) first_dep_google_d_cop,
             count(case when ums.channel = 'google display' 
                   AND t.writer_id in (20000)
                   then u.id else null end) first_dep_google_d_cop_mob,
                     
            --Beni FD 
             sum(case when ums.channel = 'Beni' 
                   AND t.writer_id not in (200, 10000, 20000)
                  then t.amount * t.rate/100 else 0 end) sum_fd_beni,                          
             sum(case when ums.channel = 'Beni' 
                   AND t.writer_id in (200)
                   then t.amount * t.rate/100 else 0 end) sum_fd_beni_mob,                        
             sum(case when ums.channel = 'Beni' 
                   AND t.writer_id in (10000)
                   then t.amount * t.rate/100 else 0 end) sum_fd_beni_cop,
             sum(case when ums.channel = 'Beni' 
                   AND t.writer_id in (20000)
                   then t.amount * t.rate/100 else 0 end) sum_fd_beni_cop_mob,
             count(case when ums.channel = 'Beni' 
                   AND t.writer_id not in (200, 10000, 20000)
                   then u.id else null end) first_dep_beni,                          
             count(case when ums.channel = 'Beni' 
                   AND t.writer_id in (200)
                   then u.id else null end) first_dep_beni_mob,                        
             count(case when ums.channel = 'Beni' 
                   AND t.writer_id in (10000)
                   then u.id else null end) first_dep_beni_cop,
             count(case when ums.channel = 'Beni' 
                   AND t.writer_id in (20000)
                   then u.id else null end) first_dep_beni_cop_mob, 
                                   
             --FREE FD
             sum(case when ums.channel = 'free'
                      and t.writer_id   not in (200, 10000, 20000)  then t.amount * t.rate/100 else 0 end) sum_fd_free, 
             sum(case when ums.channel = 'free' 
                       and t.writer_id   in (200)  then t.amount * t.rate/100 else 0 end) sum_fd_free_mob, 
             sum(case when ums.channel = 'free' 
                      and t.writer_id   in (10000)  then t.amount * t.rate/100 else 0 end) sum_fd_free_cop, 
             sum(case when ums.channel = 'free' 
                       and t.writer_id   in (20000)  then t.amount * t.rate/100 else 0 end) sum_fd_free_cop_mob,
             count(case when ums.channel = 'free'
                        and t.writer_id   not in (200, 10000, 20000)  then u.id else null end) first_dep_free, 
             count(case when ums.channel = 'free' 
                        and t.writer_id   in (200)  then u.id else null end) first_dep_free_mob, 
             count(case when ums.channel = 'free' 
                       and t.writer_id   in (10000)  then u.id else null end) first_dep_free_cop, 
             count(case when ums.channel = 'free' 
                        and t.writer_id   in (20000)  then u.id else null end) first_dep_free_cop_mob,
             --PPC FD                       
             sum(case when ums.channel = 'ppc' 
                         and t.writer_id   not in (200, 10000, 20000) then t.amount * t.rate/100 else 0 end) sum_fd_ppc, 
             sum(case when ums.channel = 'ppc' 
                        and t.writer_id   in (200)  then t.amount * t.rate/100 else 0 end) sum_fd_ppc_mob, 
             sum(case when ums.channel = 'ppc' 
                        and t.writer_id   in (10000)  then t.amount * t.rate/100 else 0 end) sum_fd_ppc_cop, 
             sum(case when ums.channel = 'ppc' 
                        and t.writer_id   in (20000)  then t.amount * t.rate/100 else 0 end) sum_fd_ppc_cop_mob,    
             count(case when ums.channel = 'ppc' 
                        and t.writer_id   not in (200, 10000, 20000)  then u.id else null end) first_dep_ppc, 
             count(case when ums.channel = 'ppc' 
                        and t.writer_id   in (200)  then u.id else null end) first_dep_ppc_mob, 
             count(case when ums.channel = 'ppc' 
                        and t.writer_id   in (10000)  then u.id else null end) first_dep_ppc_cop, 
             count(case when ums.channel = 'ppc' 
                        and t.writer_id   in (20000)  then u.id else null end) first_dep_ppc_cop_mob,                                         
               --Media Tal T 
             sum(case when ums.channel = 'Media Tal T'
                   AND t.writer_id not in (200, 10000, 20000)
                   then t.amount * t.rate/100 else 0 end) sum_fd_media_tlt,
             sum(case when ums.channel = 'Media Tal T'
                   AND t.writer_id in (200)
                   then t.amount * t.rate/100 else 0 end) sum_fd_media_tlt_mob,
             sum(case when ums.channel = 'Media Tal T'
                   AND t.writer_id in (10000)
                   then t.amount * t.rate/100 else 0 end) sum_fd_media_tlt_cop,
             sum(case when ums.channel = 'Media Tal T'
                   AND t.writer_id in (20000)
                   then t.amount * t.rate/100 else 0 end) sum_fd_media_tlt_cop_mob,
             count(case when ums.channel = 'Media Tal T'
                   AND t.writer_id not in (200, 10000, 20000)
                   then u.id else null end) first_dep_media_tlt,
             count(case when ums.channel = 'Media Tal T'
                   AND t.writer_id in (200)
                   then u.id else null end) first_dep_media_tlt_mob,
             count(case when ums.channel = 'Media Tal T'
                   AND t.writer_id in (10000)
                   then u.id else null end) first_dep_media_tlt_cop,
             count(case when ums.channel = 'Media Tal T'
                   AND t.writer_id in (20000)
                   then u.id else null end) first_dep_media_tlt_cop_mob,
                     
                   
                   --Media Tal V 
             sum(case when ums.channel = 'Media Tal V'
                   AND t.writer_id not in (200, 10000, 20000)
                   then t.amount * t.rate/100 else 0 end) sum_fd_media_tlv,
             sum(case when ums.channel = 'Media Tal V'
                   AND t.writer_id in (200)
                   then t.amount * t.rate/100 else 0 end) sum_fd_media_tlv_mob,
             sum(case when ums.channel = 'Media Tal V'
                   AND t.writer_id in (10000)
                   then t.amount * t.rate/100 else 0 end) sum_fd_media_tlv_cop,
             sum(case when ums.channel = 'Media Tal V'
                   AND t.writer_id in (20000)
                   then t.amount * t.rate/100 else 0 end) sum_fd_media_tlv_cop_mob,
             count(case when ums.channel = 'Media Tal V'
                   AND t.writer_id not in (200, 10000, 20000)
                   then u.id else null end) first_dep_media_tlv,
             count(case when ums.channel = 'Media Tal V'
                   AND t.writer_id in (200)
                   then u.id else null end) first_dep_media_tlv_mob,
             count(case when ums.channel = 'Media Tal V'
                   AND t.writer_id in (10000)
                   then u.id else null end) first_dep_media_tlv_cop,
             count(case when ums.channel = 'Media Tal V'
                   AND t.writer_id in (20000)
                   then u.id else null end) first_dep_media_tlv_cop_mob
                     
             
         from             
             users u, 
             transactions t, 
             USERS_MARKETING_SOURCES ums,
             skins s,
             skins s1 
           where  
             u.id = t.user_id 
             and u.class_id <> 0 
             and u.id = ums.user_id 
             and t.id = u.first_deposit_id 
             and t.type_id in (select tt.id from  transaction_types tt where tt.class_type =  1 ) 
             and t.time_created between trunc(sysdate -  pi_days_before) and trunc(sysdate) 
             and t.status_id in ( 2,7,8 )
             and s.id = u.skin_id
             and s1.id =s.type_id
          group by s1.display_name
          )A
) B;
RETURN v_return;
END GET_FTD_SKIN_SOURCE_REPORT;



FUNCTION return_amount (c_key_value IN NUMBER)
RETURN NUMBER IS s_amount NUMBER;


cursor p_par is
select i.amount,i.house_result,i.time_settled,i.time_canceled from investments i where i.id = c_key_value;

v_par p_par%ROWTYPE;

begin
  open p_par;
  fetch p_par into v_par;
  close p_par;

  if v_par.time_canceled  > v_par.time_settled then

    s_amount := v_par.house_result;

  elsif v_par.time_canceled is null then

    s_amount := 0;

  elsif v_par.time_canceled <= v_par.time_settled then

    s_amount := v_par.amount;

  end if;
RETURN(s_amount);
end return_amount;

FUNCTION insert_amount (c_key_value IN NUMBER)
RETURN NUMBER IS s_amount NUMBER;


cursor p_check is
select i.type_id,i.amount from investments i where i.id = c_key_value;


cursor p_par is
select (select sum(amount) from investments where group_inv_id = i.group_inv_id) amount_insert
    from balance_history bh, investments i
 where bh.key_value = c_key_value
     and i.id = bh.key_value
     and bh.command = 10;


insert_amount NUMBER;

v_par p_check%ROWTYPE;

begin

  open p_check;
  fetch p_check into v_par;
  close p_check;

if v_par.type_id in (4,5) then  
  
  open p_par;
  fetch p_par into insert_amount;
  close p_par;

  if insert_amount >= v_par.amount then

    s_amount := insert_amount;

  else 

    s_amount := v_par.amount;

  end if;
 else s_amount := v_par.amount;
 end if;
RETURN(s_amount);
end insert_amount;


--Manager Reports(NEW DYNAMIC BY CHANNEL)
FUNCTION GET_CUST_PER_SOURCE_REPORT (pi_days_before number)
RETURN CUST_PER_SOURCE_TBL_TYPE
AS
v_return  CUST_PER_SOURCE_TBL_TYPE;
BEGIN
SELECT CUST_PER_SOURCE_OBJ_TYPE(      B.channel,
                                      B.ao_non_mob_registered,B.ao_non_mob_ftd,B.ao_mob_registered,ao_mob_ftd,
                                      B.co_non_mob_registered,B.co_non_mob_ftd,B.co_mob_registered,B.co_mob_ftd
                                   )
BULK COLLECT INTO v_return
FROM
(select cc.channel, 
 nvl(ao_non_mob_registered,0) ao_non_mob_registered , 
 nvl(ao_non_mob_ftd,0) ao_non_mob_ftd,
 nvl(ao_mob_registered,0) ao_mob_registered, 
 nvl(ao_mob_ftd,0) ao_mob_ftd,
 nvl(co_non_mob_registered,0) co_non_mob_registered, 
 nvl(co_non_mob_ftd, 0) co_non_mob_ftd,
 nvl(co_mob_registered,0) co_mob_registered, 
 nvl(co_mob_ftd,0) co_mob_ftd
     from 
(select distinct channel 
 from 
 CHANNELS_UPDATED) cc,
(select       cu.channel,
             count(case when t.writer_id   not in (200, 10000, 20000)  then u.id else null end) ao_non_mob_ftd, 
             count(case when t.writer_id   in (200)  then u.id else null end) ao_mob_ftd, 
             count(case when t.writer_id   in (10000)  then u.id else null end) co_non_mob_ftd, 
             count(case when t.writer_id   in (20000) then u.id else null end) co_mob_ftd   

         from             
             users u, 
             transactions t,
             CHANNELS_UPDATED cu
           where  
             u.id = t.user_id 
             and u.class_id <> 0 
             and u.id = cu.id
             and t.id = u.first_deposit_id 
             and t.type_id in (select tt.id from  transaction_types tt where tt.class_type =  1 ) 
             and t.time_created between trunc(sysdate -  pi_days_before) and trunc(sysdate) 
             and t.status_id in ( 2,7,8 )
group by cu.channel) FTD,
(select       cu.channel,
             count(case when u.writer_id   not in (200, 10000, 20000)  then u.id else null end) ao_non_mob_registered, 
             count(case when u.writer_id   in (200)  then u.id else null end) ao_mob_registered, 
             count(case when u.writer_id   in (10000)  then u.id else null end) co_non_mob_registered, 
             count(case when u.writer_id   in (20000) then u.id else null end) co_mob_registered   
         from             
             users u, 
              CHANNELS_UPDATED cu
           where  
             u.class_id <> 0 
             and u.id = cu.id 
             and u.time_created between trunc(sysdate - pi_days_before) and trunc(sysdate) 
group by  cu.channel) reg    

where cc.channel = ftd.channel(+)        
and cc.channel = reg.channel(+)    
) B;
RETURN v_return;
END GET_CUST_PER_SOURCE_REPORT;

FUNCTION GET_CUST_FTD_WITHD_REPORT (pi_days_before number)
RETURN CUST_FTD_WITHDREW_TBL_TYPE
AS
v_return  CUST_FTD_WITHDREW_TBL_TYPE;
BEGIN
SELECT CUST_FTD_WITHDREW_OBJ_TYPE(  B.channel ,
                                  B.ao_out_this_wthd ,B.ao_out_this_wthd_mob ,B.co_out_this_wthd ,B.co_out_this_wthd_mob ,
                                  B.ao_out_wk_wthd ,B.ao_out_wk_wthd_mob ,B.co_out_wk_wthd ,B.co_out_wk_wthd_mob 
                                )
BULK COLLECT INTO v_return
FROM
(select cc.channel ,
nvl(ao_out_this_wthd,0) ao_out_this_wthd,
nvl(ao_out_this_wthd_mob, 0) ao_out_this_wthd_mob,
nvl(co_out_this_wthd,0) co_out_this_wthd,
nvl(co_out_this_wthd_mob, 0) co_out_this_wthd_mob,

nvl(ao_out_wk_wthd,0) ao_out_wk_wthd,
nvl(ao_out_wk_wthd_mob,0)ao_out_wk_wthd_mob,
nvl(co_out_wk_wthd,0)co_out_wk_wthd,
nvl(co_out_wk_wthd_mob,0)co_out_wk_wthd_mob


from 

(select distinct channel 
 from 
 CHANNELS_UPDATED) cc,

(select channel,
               count(case when ftd.time_created >= trunc(sysdate - pi_days_before) and ftd.writer_id   not in (200, 10000, 20000)  then user_id else null end) ao_out_this_wthd, 
               count(case when ftd.time_created >= trunc(sysdate - pi_days_before) and ftd.writer_id   in (200) then user_id else null end) ao_out_this_wthd_mob, 
               count(case when ftd.time_created >= trunc(sysdate - pi_days_before) and ftd.writer_id   in (10000)  then user_id else null end) co_out_this_wthd, 
               count(case when ftd.time_created >= trunc(sysdate - pi_days_before) and ftd.writer_id   in (20000)  then user_id else null end) co_out_this_wthd_mob, 
   

               count(case when ftd.time_created < trunc(sysdate - pi_days_before) and ftd.writer_id   not in (200, 10000, 20000)  then user_id else null end) ao_out_wk_wthd, 
               count(case when ftd.time_created < trunc(sysdate - pi_days_before) and ftd.writer_id   in (200) then user_id else null end) ao_out_wk_wthd_mob, 
               count(case when ftd.time_created < trunc(sysdate - pi_days_before) and ftd.writer_id   in (10000)  then user_id else null end) co_out_wk_wthd, 
               count(case when ftd.time_created < trunc(sysdate - pi_days_before) and ftd.writer_id   in (20000)  then user_id else null end) co_out_wk_wthd_mob   

              FROM 
                ( select 
                   u.id as user_id, 
                   t.writer_id, 
                   cu.channel,
                   t.time_created, 
                   max(tw.time_created) as wtr_time 
                 from  
                   users u, 
                   transactions t, 
                   CHANNELS_UPDATED cu,
                   transactions tw,  
                   MIN_FTD_BY_SKIN_CURR min_ftd
                 where  
                   u.id = t.user_id 
                   and u.class_id <> 0 
                   and u.id = cu.id 
                   and t.id = u.first_deposit_id 
                   and t.type_id in (select tt.id from  transaction_types tt where tt.class_type =  1 ) 
                   and t.status_id in ( 2,7,8 ) 
                         and tw.user_id = u.id 
                         and tw.type_id in (select tt.id from  transaction_types tt where tt.class_type =  2) 
                         and tw.status_id in (2) 
                         and tw.time_created between trunc(sysdate - pi_days_before  ) and trunc(sysdate)  
                         and u.currency_id = min_ftd.curr_currency_id 
                         and u.skin_id = min_ftd.skin_id 
                         and u.balance < min_ftd.min_ftd 
                     group by 
                       u.id, t.writer_id, t.time_created, cu.channel ) ftd 
                 WHERE 
                   not exists (select distinct user_id from investments i where (i.time_created between ftd.time_created and ftd.wtr_time) and i.user_id = ftd.user_id)

group by channel
) wthd
where cc.channel = wthd.channel(+)
) B;
RETURN v_return;
END GET_CUST_FTD_WITHD_REPORT;

FUNCTION GET_FTD_SKIN_PER_SOURCE_REPORT (pi_days_before number, pi_skin_display_name varchar2)
RETURN FTD_SKIN_SOURCE_TBL_TYPE
AS
v_return  FTD_SKIN_SOURCE_TBL_TYPE;
BEGIN
SELECT FTD_SKIN_SOURCE_OBJ_TYPE(  B.channel,
                                      B.ao_sum_first_dep,B.ao_sum_first_dep_mob,B.co_sum_first_dep,B.co_sum_first_dep_mob,
                                      B.ao_total_first_dep,B.ao_total_first_dep_mob,B.co_total_first_dep,B.co_total_first_dep_mob 
                                   )
BULK COLLECT INTO v_return
FROM
(select cc.channel, 
       nvl(ao_sum_first_dep,0) ao_sum_first_dep,
       nvl(ao_sum_first_dep_mob,0) ao_sum_first_dep_mob,
       nvl(co_sum_first_dep,0) co_sum_first_dep,
       nvl(co_sum_first_dep_mob,0) co_sum_first_dep_mob,
       
       nvl(ao_total_first_dep,0) ao_total_first_dep,
       nvl(ao_total_first_dep_mob,0) ao_total_first_dep_mob,
       nvl(co_total_first_dep,0) co_total_first_dep,
       nvl(co_total_first_dep_mob,0) co_total_first_dep_mob
       
              
       from 

(select distinct channel 
 from 
 CHANNELS_UPDATED) cc,
 
(select      
             cu.channel,
             sum(case when t.writer_id   not in (200, 10000, 20000)  then t.amount * t.rate/100 else 0 end) ao_sum_first_dep, 
             sum(case when t.writer_id   in (200)   then t.amount * t.rate/100 else 0 end) ao_sum_first_dep_mob, 
             sum(case when t.writer_id   in (10000) then t.amount * t.rate/100 else 0 end) co_sum_first_dep, 
             sum(case when t.writer_id   in (20000) then t.amount * t.rate/100 else 0 end) co_sum_first_dep_mob, 

             count(case when t.writer_id   not in (200, 10000, 20000)  then u.id else null end) ao_total_first_dep, 
             count(case when t.writer_id   in (200)  then u.id else null end) ao_total_first_dep_mob, 
             count(case when t.writer_id   in (10000)  then u.id else null end) co_total_first_dep, 
             count(case when t.writer_id   in (20000) then u.id else null end) co_total_first_dep_mob 

         from             
             users u, 
             transactions t, 
             CHANNELS_UPDATED cu,
             skins s,
             skins s1 
           where  
             u.id = t.user_id 
             and u.class_id <> 0 
             and u.id = cu.id 
             and t.id = u.first_deposit_id 
             and t.type_id in (select tt.id from  transaction_types tt where tt.class_type =  1 ) 
             and t.time_created between trunc(sysdate -  pi_days_before) and trunc(sysdate) 
             and s1.display_name = pi_skin_display_name
             and t.status_id in ( 2,7,8 )
             and s.id = u.skin_id
             and s1.id =s.type_id
          group by cu.channel) psk
          where cc.channel = psk.channel(+)           
) B;
RETURN v_return;
END GET_FTD_SKIN_PER_SOURCE_REPORT;

end;

/