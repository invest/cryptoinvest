CREATE OR REPLACE PACKAGE etrader.pkg_markets IS

  -- Author  : VICTORS
  -- Created : 2016-03-30 16:53:27
  -- Purpose : for MarketsDAO

  PROCEDURE delete_market_rates(pmarketslist IN numbers_table);

END pkg_markets;
/
CREATE OR REPLACE PACKAGE BODY etrader.pkg_markets IS

  PROCEDURE delete_market_rates(pmarketslist IN numbers_table) IS
  BEGIN
    IF pmarketslist IS NOT NULL THEN
      -- delete for given markets records older than hour and a half
      FORALL i IN 1 .. pmarketslist.count
        DELETE FROM market_rates
         WHERE market_id = pmarketslist(i)
           AND rate_time < SYSDATE - 3 / 48;
    END IF;
  END delete_market_rates;

END pkg_markets;
/
