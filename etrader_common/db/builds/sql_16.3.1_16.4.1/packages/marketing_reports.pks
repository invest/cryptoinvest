CREATE OR REPLACE /* Formatted on 3.12.2013 �. 11:20:40 (QP5 v5.149.1003.31008) */
PACKAGE "MARKETING_REPORTS"
IS
   FUNCTION GET_MARKETING_BASE_REPORT (pi_from_date    DATE,
                                       pi_to_date      DATE,
                                       pi_skin_id      NUMBER)
      RETURN MARKETING_BASE_REPORT_TBL_TYPE;

   FUNCTION GET_MARKETING_BASE_REP_BY_DATE (pi_from_date              DATE,
                                            pi_to_date                DATE,
                                            pi_skin_id                NUMBER,
                                            pi_source_id              NUMBER,
                                            pi_campaign_manager_id    NUMBER)
      RETURN MARK_BASE_REP_BY_DATE_TBL_TYPE;

   FUNCTION GET_MARKETING_DP_REPORT (pi_from_date              DATE,
                                     pi_to_date                DATE,
                                     pi_skin_id                NUMBER,
                                     pi_source_id              NUMBER,
                                     pi_campaign_manager_id    NUMBER)
      RETURN MARKETING_DP_REPORT_TBL_TYPE;

   FUNCTION GET_MARKETING_DP_REP_BY_DATE (pi_from_date              DATE,
                                          pi_to_date                DATE,
                                          pi_skin_id                NUMBER,
                                          pi_source_id              NUMBER,
                                          pi_campaign_manager_id    NUMBER)
      RETURN MARK_DP_REP_BY_DATE_TBL_TYPE;

   FUNCTION GET_MARKETING_HTTP_REF_REPORT (pi_from_date              DATE,
                                           pi_to_date                DATE,
                                           pi_skin_id                NUMBER,
                                           pi_source_id              NUMBER,
                                           pi_campaign_manager_id    NUMBER)
      RETURN MARKETING_HTTP_REFERER_TL_TYPE;
END MARKETING_REPORTS;
/


CREATE OR REPLACE /* Formatted on 3.12.2013 �. 11:20:40 (QP5 v5.149.1003.31008) */
PACKAGE BODY "MARKETING_REPORTS"
IS
   FUNCTION GET_MARKETING_BASE_REPORT (pi_from_date    DATE,
                                       pi_to_date      DATE,
                                       pi_skin_id      NUMBER)
      RETURN MARKETING_BASE_REPORT_TBL_TYPE
   AS
      v_return   MARKETING_BASE_REPORT_TBL_TYPE;
   BEGIN
      SELECT MARKETING_BASE_REPORT_OBJ_TYPE (A.campaign_manager,
                                             A.combination_id,
                                             A.campaign_name,
                                             A.source_name,
                                             A.medium_name,
                                             A.content_name,
                                             A.marketing_size_horizontal,
                                             A.marketing_size_vertical,
                                             A.m_location,
                                             A.landing_page_name,
                                             A.SKIN_ID,
                                             A.short_reg,
                                             A.registered_users_num,
                                             A.FTD,
                                             A.RFD,
                                             A.first_remarketing_dep_num,
                                             A.house_win,
                                             A.failed_users,
                                             A.fi_source_id,
                                             A.fi_campaign_manager_id)
        BULK COLLECT INTO v_return
        FROM (  SELECT wcm.user_name AS campaign_manager,
                       mcomb.id AS combination_id,
                       mc.name AS campaign_name,
                       ms.name AS source_name,
                       mm.name AS medium_name,
                       mcont.name AS content_name,
                       msize.size_horizontal AS marketing_size_horizontal,
                       msize.size_vertical AS marketing_size_vertical,
                       mlocation.location AS m_location,
                       mlandpage.name AS landing_page_name,
                       mcomb.skin_id AS SKIN_ID,
                       NVL (reg_cont.short_reg, 0) AS short_reg,     --if null
                       NVL (registered_users.registered_users_num, 0)
                          AS registered_users_num,                   --if null
                       NVL (first_depos.FTD, 0) AS FTD,              --if null
                       NVL (first_depos.RFD, 0) AS RFD,              --if null
                       NVL (rem_first_depos.first_remarketing_dep_num, 0)
                          AS first_remarketing_dep_num,              --if null
                       NVL (invst.house_win, 0) AS house_win,        --if null
                       NVL (registered_users.failed_users, 0) AS failed_users--filters
                       ,
                       ms.id AS fi_source_id,
                       wcm.id AS fi_campaign_manager_id
                  FROM marketing_campaigns mc,
                       writers wcm,
                       marketing_combinations mcomb,
                       marketing_sources ms,
                       marketing_contents mcont,
                       marketing_sizes msize,
                       marketing_locations mlocation,
                       marketing_landing_pages mlandpage,
                       marketing_mediums mm,
                       (  SELECT COUNT (*) AS short_reg, c.combination_id
                            FROM contacts c
                           WHERE NVL (c.user_id, 0) = 0
                                 AND (TRUNC (c.time_created) BETWEEN pi_from_date
                                                                 AND pi_to_date)
                                 AND c.class_id <> 0
                        GROUP BY c.combination_id) reg_cont,
                       (  SELECT COUNT (*) AS registered_users_num,
                                 u.combination_id,
                                 --faild_users
                                 SUM (
                                    CASE
                                       WHEN (SELECT DISTINCT
                                                    (t.user_id) AS fail_users
                                               FROM transactions t,
                                                    transaction_types tt
                                              WHERE     u.id = t.user_id
                                                    AND t.type_id = tt.id
                                                    AND tt.class_type = 1
                                                    AND t.status_id = 3
                                                    AND u.class_id <> 0
                                                    AND (t.description NOT LIKE
                                                            '%min%'
                                                         OR t.description
                                                               IS NULL)
                                                    AND NOT EXISTS
                                                               (SELECT t.id
                                                                  FROM transactions t2,
                                                                       transaction_types tt2
                                                                 WHERE u.id =
                                                                          t2.
                                                                           user_id
                                                                       AND t2.
                                                                            type_id =
                                                                              tt2.
                                                                               id
                                                                       AND tt2.
                                                                            class_type =
                                                                              1
                                                                       AND t2.
                                                                            status_id IN
                                                                              (2,
                                                                               7,
                                                                               8))) >
                                               0
                                       THEN
                                          1
                                       ELSE
                                          0
                                    END)
                                    AS failed_users
                            --
                            FROM users u
                           WHERE (TRUNC (u.time_created) BETWEEN pi_from_date
                                                             AND pi_to_date)
                                 AND u.class_id <> 0
                        GROUP BY u.combination_id) registered_users,
                       (  SELECT COUNT (*) AS FTD,
                                 SUM (
                                    CASE
                                       WHEN TRUNC (us.time_created) BETWEEN pi_from_date
                                                                        AND pi_to_date
                                       THEN
                                          1
                                       ELSE
                                          0
                                    END)
                                    AS RFD,
                                 us.combination_id
                            FROM users us, transactions tr
                           WHERE us.first_deposit_id = tr.id AND us.class_id <> 0
                                 AND (TRUNC (tr.time_created) BETWEEN pi_from_date
                                                                  AND pi_to_date)
                        GROUP BY us.combination_id) first_depos,
                       (  SELECT COUNT (*) AS first_remarketing_dep_num,
                                 us.combination_id
                            FROM users us, transactions tr, remarketing_logins rl
                           WHERE     rl.user_id = us.id
                                 AND us.first_deposit_id = tr.id
                                 AND us.class_id <> 0
                                 AND (TRUNC (tr.time_created) BETWEEN pi_from_date
                                                                  AND pi_to_date)
                        GROUP BY us.combination_id) rem_first_depos,
                       (  SELECT u.combination_id,
                                 SUM (i.house_result * i.rate / 100) AS house_win
                            FROM users u, investments i
                           WHERE     u.id = i.user_id
                                 AND i.is_settled = 1
                                 AND i.is_canceled = 0
                                 AND u.class_id <> 0
                                 AND (TRUNC (i.time_created) BETWEEN pi_from_date
                                                                 AND pi_to_date)
                        GROUP BY u.combination_id) invst
                 WHERE     mc.campaign_manager = wcm.id
                       AND mc.id = mcomb.campaign_id
                       AND ms.id = mc.source_id
                       AND mcomb.content_id = mcont.id
                       AND mcomb.medium_id = mm.id
                       AND mcomb.size_id = msize.id(+)
                       AND mcomb.location_id = mlocation.id(+)
                       AND mcomb.landing_page_id = mlandpage.id
                       AND mcomb.id = reg_cont.combination_id(+)
                       AND mcomb.id = registered_users.combination_id(+)
                       AND mcomb.id = first_depos.combination_id(+)
                       AND mcomb.id = rem_first_depos.combination_id(+)
                       AND mcomb.id = invst.combination_id(+)
                       --
                       AND (NVL (reg_cont.short_reg, 0) > 0
                            OR NVL (registered_users.registered_users_num, 0) >
                                  0
                            OR NVL (first_depos.FTD, 0) > 0
                            OR NVL (invst.house_win, 0) != 0
                            OR NVL (rem_first_depos.first_remarketing_dep_num,
                                    0) > 0)
                       --
                       AND DECODE (pi_skin_id, 0, 0, mcomb.skin_id) =
                              DECODE (pi_skin_id, 0, 0, pi_skin_id)
              /*and ms.id=decode(pi_source_id,0,ms.id,pi_source_id)
              and wcm.id=decode(pi_campaign_manager_id,0,wcm.id,pi_campaign_manager_id)*/
              ORDER BY mcomb.id) A;

      RETURN v_return;
   END GET_MARKETING_BASE_REPORT;

   ----------------------------------------------------------------------------------------------------------------

   ----------------------------------------------------------------------------------------------------------------
   FUNCTION GET_MARKETING_BASE_REP_BY_DATE (pi_from_date              DATE,
                                            pi_to_date                DATE,
                                            pi_skin_id                NUMBER,
                                            pi_source_id              NUMBER,
                                            pi_campaign_manager_id    NUMBER)
      RETURN MARK_BASE_REP_BY_DATE_TBL_TYPE
   AS
      v_return   MARK_BASE_REP_BY_DATE_TBL_TYPE;
   BEGIN
        SELECT MARK_BASE_REP_BY_DATE_OBJ_TYPE (gen_dates.def_date,
                                               A.campaign_manager,
                                               A.combination_id,
                                               A.campaign_name,
                                               A.source_name,
                                               A.medium_name,
                                               A.content_name,
                                               A.marketing_size_horizontal,
                                               A.marketing_size_vertical,
                                               A.m_location,
                                               A.landing_page_name,
                                               A.SKIN_ID,
                                               A.short_reg,
                                               A.registered_users_num,
                                               A.FTD,
                                               A.RFD,
                                               A.first_remarketing_dep_num,
                                               A.house_win,
                                               A.failed_users)
          BULK COLLECT INTO v_return
          FROM (SELECT sh_us.dates,
                       wcm.user_name AS campaign_manager,
                       mcomb.id AS combination_id,
                       mc.name AS campaign_name,
                       ms.name AS source_name,
                       mm.name AS medium_name,
                       mcont.name AS content_name,
                       msize.size_horizontal AS marketing_size_horizontal,
                       msize.size_vertical AS marketing_size_vertical,
                       mlocation.location AS m_location,
                       mlandpage.name AS landing_page_name,
                       mcomb.skin_id AS SKIN_ID,
                       sh_us.short_reg,
                       sh_us.registered_users_num,
                       sh_us.failed_users,
                       sh_us.FTD,
                       sh_us.RFD,
                       sh_us.first_remarketing_dep_num,
                       sh_us.house_win
                  FROM marketing_campaigns mc,
                       writers wcm,
                       marketing_combinations mcomb,
                       marketing_sources ms,
                       marketing_contents mcont,
                       marketing_sizes msize,
                       marketing_locations mlocation,
                       marketing_landing_pages mlandpage,
                       marketing_mediums mm,
                       --
                       (  SELECT z.dates,
                                 SUM (z.short_reg) AS short_reg,
                                 SUM (registered_users_num)
                                    AS registered_users_num,
                                 SUM (z.failed_users) AS failed_users,
                                 SUM (z.FTD) AS FTD,
                                 SUM (z.RFD) AS RFD,
                                 SUM (z.first_remarketing_dep_num)
                                    AS first_remarketing_dep_num,
                                 SUM (z.house_win) AS house_win,
                                 z.combination_id
                            FROM (  --SHORT_REG
                                    SELECT TRUNC (c.time_created) AS dates,
                                           COUNT (*) AS short_reg,
                                           0 AS registered_users_num,
                                           0 AS failed_users,
                                           0 AS FTD,
                                           0 AS RFD,
                                           0 AS first_remarketing_dep_num,
                                           0 AS house_win,
                                           c.combination_id
                                      FROM contacts c
                                     WHERE NVL (c.user_id, 0) = 0
                                           AND (TRUNC (c.time_created) BETWEEN pi_from_date
                                                                           AND pi_to_date)
                                           AND c.class_id <> 0
                                  GROUP BY c.combination_id,
                                           TRUNC (c.time_created)
                                  --registered_users_num and faild_user
                                  UNION ALL
                                    SELECT TRUNC (u.time_created) AS dates,
                                           0 AS short_reg,
                                           COUNT (*) AS registered_users_num,
                                           --faild_users
                                           SUM (
                                              CASE
                                                 WHEN (SELECT DISTINCT
                                                              (t.user_id)
                                                                 AS fail_users
                                                         FROM transactions t,
                                                              transaction_types tt
                                                        WHERE u.id = t.user_id
                                                              AND t.type_id =
                                                                     tt.id
                                                              AND tt.class_type =
                                                                     1
                                                              AND t.status_id = 3
                                                              AND u.class_id <> 0
                                                              AND (t.description NOT LIKE
                                                                      '%min%'
                                                                   OR t.
                                                                       description
                                                                         IS NULL)
                                                              AND NOT EXISTS
                                                                         (SELECT t.
                                                                                  id
                                                                            FROM transactions t2,
                                                                                 transaction_types tt2
                                                                           WHERE u.
                                                                                  id =
                                                                                    t2.
                                                                                     user_id
                                                                                 AND t2.
                                                                                      type_id =
                                                                                        tt2.
                                                                                         id
                                                                                 AND tt2.
                                                                                      class_type =
                                                                                        1
                                                                                 AND t2.
                                                                                      status_id IN
                                                                                        (2,
                                                                                         7,
                                                                                         8))) >
                                                         0
                                                 THEN
                                                    1
                                                 ELSE
                                                    0
                                              END)
                                              AS failed_users,
                                           0 AS FTD,
                                           0 AS RFD,
                                           0 AS first_remarketing_dep_num,
                                           0 AS house_win,
                                           u.combination_id
                                      FROM users u
                                     WHERE (TRUNC (u.time_created) BETWEEN pi_from_date
                                                                       AND pi_to_date)
                                           AND u.class_id <> 0
                                  GROUP BY u.combination_id,
                                           TRUNC (u.time_created)
                                  --FTD
                                  UNION ALL
                                    SELECT TRUNC (tr.time_created) AS dates,
                                           0 AS short_reg,
                                           0 AS registered_users_num,
                                           0 AS failed_users,
                                           COUNT (*) AS FTD,
                                           SUM (
                                              CASE
                                                 WHEN TRUNC (us.time_created) BETWEEN pi_from_date
                                                                                  AND pi_to_date
                                                 THEN
                                                    1
                                                 ELSE
                                                    0
                                              END)
                                              AS RFD,
                                           0 AS first_remarketing_dep_num,
                                           0 AS house_win,
                                           us.combination_id
                                      FROM users us, transactions tr
                                     WHERE us.first_deposit_id = tr.id
                                           AND us.class_id <> 0
                                           AND (TRUNC (tr.time_created) BETWEEN pi_from_date
                                                                            AND pi_to_date)
                                  GROUP BY TRUNC (tr.time_created),
                                           us.combination_id
                                  -- first_remarketing_dep_num
                                  UNION ALL
                                    SELECT TRUNC (tr.time_created) AS dates,
                                           0 AS short_reg,
                                           0 AS registered_users_num,
                                           0 AS failed_users,
                                           0 AS FTD,
                                           0 AS RFD,
                                           COUNT (*) AS first_remarketing_dep_num,
                                           0 AS house_win,
                                           us.combination_id
                                      FROM users us,
                                           transactions tr,
                                           remarketing_logins rl
                                     WHERE     us.id = rl.user_id
                                           AND us.first_deposit_id = tr.id
                                           AND us.class_id <> 0
                                           AND (TRUNC (tr.time_created) BETWEEN pi_from_date
                                                                            AND pi_to_date)
                                  GROUP BY TRUNC (tr.time_created),
                                           us.combination_id
                                  --HOUSE_WIN
                                  UNION ALL
                                    SELECT TRUNC (i.time_created) AS dates,
                                           0 AS short_reg,
                                           0 AS registered_users_num,
                                           0 AS failed_users,
                                           0 AS FTD,
                                           0 AS RFD,
                                           0 AS first_remarketing_dep_num,
                                           SUM (i.house_result * i.rate / 100)
                                              AS house_win,
                                           u.combination_id
                                      FROM users u, investments i
                                     WHERE     u.id = i.user_id
                                           AND i.is_settled = 1
                                           AND i.is_canceled = 0
                                           AND u.class_id <> 0
                                           AND (TRUNC (i.time_created) BETWEEN pi_from_date
                                                                           AND pi_to_date)
                                  GROUP BY TRUNC (i.time_created),
                                           u.combination_id) z
                        GROUP BY z.dates, z.combination_id) sh_us
                 --
                 WHERE     mc.campaign_manager = wcm.id
                       AND mc.id = mcomb.campaign_id
                       AND ms.id = mc.source_id
                       AND mcomb.content_id = mcont.id
                       AND mcomb.medium_id = mm.id
                       AND mcomb.size_id = msize.id(+)
                       AND mcomb.location_id = mlocation.id(+)
                       AND mcomb.landing_page_id = mlandpage.id
                       AND mcomb.id = sh_us.combination_id(+)
                       AND (   NVL (sh_us.short_reg, 0) > 0
                            OR NVL (sh_us.registered_users_num, 0) > 0
                            OR NVL (FTD, 0) > 0
                            OR NVL (house_win, 0) != 0
                            OR NVL (sh_us.registered_users_num, 0) > 0)
                       --
                       AND mcomb.skin_id =
                              DECODE (pi_skin_id, 0, mcomb.skin_id, pi_skin_id)
                       AND ms.id =
                              DECODE (pi_source_id, 0, ms.id, pi_source_id)
                       AND wcm.id =
                              DECODE (pi_campaign_manager_id,
                                      0, wcm.id,
                                      pi_campaign_manager_id)) A,
               (SELECT TRUNC (dates.COLUMN_VALUE) AS def_date
                  FROM TABLE (get_date_range (pi_from_date, pi_to_date)) dates) gen_dates
         WHERE gen_dates.def_date = A.DATES(+)
      ORDER BY gen_dates.def_date, A.combination_id;

      RETURN v_return;
   END GET_MARKETING_BASE_REP_BY_DATE;

   ----------------------------------------------------------------------------------------------------------------

   ----------------------------------------------------------------------------------------------------------------
   FUNCTION GET_MARKETING_DP_REPORT (pi_from_date              DATE,
                                     pi_to_date                DATE,
                                     pi_skin_id                NUMBER,
                                     pi_source_id              NUMBER,
                                     pi_campaign_manager_id    NUMBER)
      RETURN MARKETING_DP_REPORT_TBL_TYPE
   AS
      v_return   MARKETING_DP_REPORT_TBL_TYPE;
   BEGIN
      SELECT MARKETING_DP_REPORT_OBJ_TYPE (A.DP,
                                           A.campaign_manager,
                                           A.combination_id,
                                           A.campaign_name,
                                           A.source_name,
                                           A.medium_name,
                                           A.content_name,
                                           A.marketing_size_horizontal,
                                           A.marketing_size_vertical,
                                           A.m_location,
                                           A.landing_page_name,
                                           A.SKIN_ID,
                                           A.short_reg,
                                           A.registered_users_num,
                                           A.FTD,
                                           A.RFD,
                                           A.first_remarketing_dep_num,
                                           A.house_win)
        BULK COLLECT INTO v_return
        FROM (  SELECT DECODE (sh_us.DP, '0', '', sh_us.DP) AS DP,
                       wcm.user_name AS campaign_manager,
                       mcomb.id AS combination_id,
                       mc.name AS campaign_name,
                       ms.name AS source_name,
                       mm.name AS medium_name,
                       mcont.name AS content_name,
                       msize.size_horizontal AS marketing_size_horizontal,
                       msize.size_vertical AS marketing_size_vertical,
                       mlocation.location AS m_location,
                       mlandpage.name AS landing_page_name,
                       mcomb.skin_id AS SKIN_ID,
                       sh_us.short_reg,
                       sh_us.registered_users_num,
                       sh_us.FTD,
                       sh_us.RFD,
                       sh_us.first_remarketing_dep_num,
                       sh_us.house_win
                  FROM marketing_campaigns mc,
                       writers wcm,
                       marketing_combinations mcomb,
                       marketing_sources ms,
                       marketing_contents mcont,
                       marketing_sizes msize,
                       marketing_locations mlocation,
                       marketing_landing_pages mlandpage,
                       marketing_mediums mm,
                       --
                       (  SELECT z.DP,
                                 SUM (z.short_reg) AS short_reg,
                                 SUM (registered_users_num)
                                    AS registered_users_num,
                                 SUM (z.FTD) AS FTD,
                                 SUM (z.RFD) AS RFD,
                                 SUM (z.first_remarketing_dep_num)
                                    AS first_remarketing_dep_num,
                                 SUM (z.house_win) AS house_win,
                                 z.combination_id
                            FROM (  --SHORT_REG
                                    SELECT NVL (
                                              CAST (
                                                 c.dynamic_parameter AS NVARCHAR2 (1000)),
                                              0)
                                              AS DP,
                                           COUNT (*) AS short_reg,
                                           0 AS registered_users_num,
                                           0 AS FTD,
                                           0 AS RFD,
                                           0 AS first_remarketing_dep_num,
                                           0 AS house_win,
                                           c.combination_id
                                      FROM contacts c
                                     WHERE NVL (c.user_id, 0) = 0
                                           AND (TRUNC (c.time_created) BETWEEN pi_from_date
                                                                           AND pi_to_date)
                                           AND c.class_id <> 0
                                  GROUP BY c.combination_id, c.dynamic_parameter
                                  --registered_users_num
                                  UNION ALL
                                    SELECT NVL (u.dynamic_param, 0) AS DP,
                                           0 AS short_reg,
                                           COUNT (*) AS registered_users_num,
                                           0 AS FTD,
                                           0 AS RFD,
                                           0 AS first_remarketing_dep_num,
                                           0 AS house_win,
                                           u.combination_id
                                      FROM users u
                                     WHERE (TRUNC (u.time_created) BETWEEN pi_from_date
                                                                       AND pi_to_date)
                                           AND u.class_id <> 0
                                  GROUP BY u.combination_id, u.dynamic_param
                                  --FTD
                                  UNION ALL
                                    SELECT NVL (us.dynamic_param, 0) AS DP,
                                           0 AS short_reg,
                                           0 AS registered_users_num,
                                           COUNT (*) AS FTD,
                                           SUM (
                                              CASE
                                                 WHEN TRUNC (us.time_created) BETWEEN pi_from_date
                                                                                  AND pi_to_date
                                                 THEN
                                                    1
                                                 ELSE
                                                    0
                                              END)
                                              AS RFD,
                                           0 AS first_remarketing_dep_num,
                                           0 AS house_win,
                                           us.combination_id
                                      FROM users us, transactions tr
                                     WHERE us.first_deposit_id = tr.id
                                           AND us.class_id <> 0
                                           AND (TRUNC (tr.time_created) BETWEEN pi_from_date
                                                                            AND pi_to_date)
                                  GROUP BY us.dynamic_param, us.combination_id
                                  -- first_remarketing_dep_num
                                  UNION ALL
                                    SELECT NVL (us.dynamic_param, 0) AS DP,
                                           0 AS short_reg,
                                           0 AS registered_users_num,
                                           0 AS FTD,
                                           0 AS RFD,
                                           COUNT (*) AS first_remarketing_dep_num,
                                           0 AS house_win,
                                           us.combination_id
                                      FROM users us,
                                           transactions tr,
                                           remarketing_logins rl
                                     WHERE     us.id = rl.user_id
                                           AND us.first_deposit_id = tr.id
                                           AND us.class_id <> 0
                                           AND (TRUNC (tr.time_created) BETWEEN pi_from_date
                                                                            AND pi_to_date)
                                  GROUP BY us.dynamic_param, us.combination_id
                                  --HOUSE_WIN
                                  UNION ALL
                                    SELECT NVL (u.dynamic_param, 0) AS DP,
                                           0 AS short_reg,
                                           0 AS registered_users_num,
                                           0 AS FTD,
                                           0 AS RFD,
                                           0 AS first_remarketing_dep_num,
                                           SUM (i.house_result * i.rate / 100)
                                              AS house_win,
                                           u.combination_id
                                      FROM users u, investments i
                                     WHERE     u.id = i.user_id
                                           AND i.is_settled = 1
                                           AND i.is_canceled = 0
                                           AND u.class_id <> 0
                                           AND (TRUNC (i.time_created) BETWEEN pi_from_date
                                                                           AND pi_to_date)
                                  GROUP BY u.dynamic_param, u.combination_id) z
                        GROUP BY z.DP, z.combination_id) sh_us
                 --
                 WHERE     mc.campaign_manager = wcm.id
                       AND mc.id = mcomb.campaign_id
                       AND ms.id = mc.source_id
                       AND mcomb.content_id = mcont.id
                       AND mcomb.medium_id = mm.id
                       AND mcomb.size_id = msize.id(+)
                       AND mcomb.location_id = mlocation.id(+)
                       AND mcomb.landing_page_id = mlandpage.id
                       AND mcomb.id = sh_us.combination_id(+)
                       AND (   NVL (sh_us.short_reg, 0) > 0
                            OR NVL (sh_us.registered_users_num, 0) > 0
                            OR NVL (FTD, 0) > 0
                            OR NVL (house_win, 0) != 0
                            OR NVL (sh_us.registered_users_num, 0) > 0)
                       --
                       AND mcomb.skin_id =
                              DECODE (pi_skin_id, 0, mcomb.skin_id, pi_skin_id)
                       AND ms.id =
                              DECODE (pi_source_id, 0, ms.id, pi_source_id)
                       AND wcm.id =
                              DECODE (pi_campaign_manager_id,
                                      0, wcm.id,
                                      pi_campaign_manager_id)
              ORDER BY 1) A;

      RETURN v_return;
   END GET_MARKETING_DP_REPORT;

   ----------------------------------------------------------------------------------------------------------------

   ----------------------------------------------------------------------------------------------------------------
   FUNCTION GET_MARKETING_DP_REP_BY_DATE (pi_from_date              DATE,
                                          pi_to_date                DATE,
                                          pi_skin_id                NUMBER,
                                          pi_source_id              NUMBER,
                                          pi_campaign_manager_id    NUMBER)
      RETURN MARK_DP_REP_BY_DATE_TBL_TYPE
   AS
      v_return   MARK_DP_REP_BY_DATE_TBL_TYPE;
   BEGIN
        SELECT MARK_DP_REP_BY_DATE_OBJ_TYPE (gen_dates.def_date,
                                             A.DP,
                                             A.campaign_manager,
                                             A.combination_id,
                                             A.campaign_name,
                                             A.source_name,
                                             A.medium_name,
                                             A.content_name,
                                             A.marketing_size_horizontal,
                                             A.marketing_size_vertical,
                                             A.m_location,
                                             A.landing_page_name,
                                             A.SKIN_ID,
                                             A.short_reg,
                                             A.registered_users_num,
                                             A.FTD,
                                             A.RFD,
                                             A.first_remarketing_dep_num,
                                             A.house_win)
          BULK COLLECT INTO v_return
          FROM (SELECT sh_us.dates,
                       DECODE (sh_us.DP, '0', '', sh_us.DP) AS DP,
                       wcm.user_name AS campaign_manager,
                       mcomb.id AS combination_id,
                       mc.name AS campaign_name,
                       ms.name AS source_name,
                       mm.name AS medium_name,
                       mcont.name AS content_name,
                       msize.size_horizontal AS marketing_size_horizontal,
                       msize.size_vertical AS marketing_size_vertical,
                       mlocation.location AS m_location,
                       mlandpage.name AS landing_page_name,
                       mcomb.skin_id AS SKIN_ID,
                       sh_us.short_reg,
                       sh_us.registered_users_num,
                       sh_us.FTD,
                       sh_us.RFD,
                       sh_us.first_remarketing_dep_num,
                       sh_us.house_win
                  FROM marketing_campaigns mc,
                       writers wcm,
                       marketing_combinations mcomb,
                       marketing_sources ms,
                       marketing_contents mcont,
                       marketing_sizes msize,
                       marketing_locations mlocation,
                       marketing_landing_pages mlandpage,
                       marketing_mediums mm,
                       --
                       (  SELECT z.dates,
                                 z.DP,
                                 SUM (z.short_reg) AS short_reg,
                                 SUM (registered_users_num)
                                    AS registered_users_num,
                                 SUM (z.FTD) AS FTD,
                                 SUM (z.RFD) AS RFD,
                                 SUM (z.first_remarketing_dep_num)
                                    AS first_remarketing_dep_num,
                                 SUM (z.house_win) AS house_win,
                                 z.combination_id
                            FROM (  --SHORT_REG
                                    SELECT TRUNC (c.time_created) AS dates,
                                           NVL (
                                              CAST (
                                                 c.dynamic_parameter AS NVARCHAR2 (1000)),
                                              0)
                                              AS DP,
                                           COUNT (*) AS short_reg,
                                           0 AS registered_users_num,
                                           0 AS FTD,
                                           0 AS RFD,
                                           0 AS first_remarketing_dep_num,
                                           0 AS house_win,
                                           c.combination_id
                                      FROM contacts c
                                     WHERE NVL (c.user_id, 0) = 0
                                           AND (TRUNC (c.time_created) BETWEEN pi_from_date
                                                                           AND pi_to_date)
                                           AND c.class_id <> 0
                                  GROUP BY c.combination_id,
                                           c.dynamic_parameter,
                                           TRUNC (c.time_created)
                                  --registered_users_num
                                  UNION ALL
                                    SELECT TRUNC (u.time_created) AS dates,
                                           NVL (u.dynamic_param, 0) AS DP,
                                           0 AS short_reg,
                                           COUNT (*) AS registered_users_num,
                                           0 AS FTD,
                                           0 AS RFD,
                                           0 AS first_remarketing_dep_num,
                                           0 AS house_win,
                                           u.combination_id
                                      FROM users u
                                     WHERE (TRUNC (u.time_created) BETWEEN pi_from_date
                                                                       AND pi_to_date)
                                           AND u.class_id <> 0
                                  GROUP BY u.combination_id,
                                           u.dynamic_param,
                                           TRUNC (u.time_created)
                                  --FTD
                                  UNION ALL
                                    SELECT TRUNC (tr.time_created) AS dates,
                                           NVL (us.dynamic_param, 0) AS DP,
                                           0 AS short_reg,
                                           0 AS registered_users_num,
                                           COUNT (*) AS FTD,
                                           SUM (
                                              CASE
                                                 WHEN TRUNC (us.time_created) BETWEEN pi_from_date
                                                                                  AND pi_to_date
                                                 THEN
                                                    1
                                                 ELSE
                                                    0
                                              END)
                                              AS RFD,
                                           0 AS first_remarketing_dep_num,
                                           0 AS house_win,
                                           us.combination_id
                                      FROM users us, transactions tr
                                     WHERE us.first_deposit_id = tr.id
                                           AND us.class_id <> 0
                                           AND (TRUNC (tr.time_created) BETWEEN pi_from_date
                                                                            AND pi_to_date)
                                  GROUP BY us.dynamic_param,
                                           us.combination_id,
                                           TRUNC (tr.time_created)
                                  -- first_remarketing_dep_num
                                  UNION ALL
                                    SELECT TRUNC (tr.time_created) AS dates,
                                           NVL (us.dynamic_param, 0) AS DP,
                                           0 AS short_reg,
                                           0 AS registered_users_num,
                                           0 AS FTD,
                                           0 AS RFD,
                                           COUNT (*) AS first_remarketing_dep_num,
                                           0 AS house_win,
                                           us.combination_id
                                      FROM users us,
                                           transactions tr,
                                           remarketing_logins rl
                                     WHERE     us.id = rl.user_id
                                           AND us.first_deposit_id = tr.id
                                           AND us.class_id <> 0
                                           AND (TRUNC (tr.time_created) BETWEEN pi_from_date
                                                                            AND pi_to_date)
                                  GROUP BY us.dynamic_param,
                                           us.combination_id,
                                           TRUNC (tr.time_created)
                                  --HOUSE_WIN
                                  UNION ALL
                                    SELECT TRUNC (i.time_created) AS dates,
                                           NVL (u.dynamic_param, 0) AS DP,
                                           0 AS short_reg,
                                           0 AS registered_users_num,
                                           0 AS FTD,
                                           0 AS RFD,
                                           0 AS first_remarketing_dep_num,
                                           SUM (i.house_result * i.rate / 100)
                                              AS house_win,
                                           u.combination_id
                                      FROM users u, investments i
                                     WHERE     u.id = i.user_id
                                           AND i.is_settled = 1
                                           AND i.is_canceled = 0
                                           AND u.class_id <> 0
                                           AND (TRUNC (i.time_created) BETWEEN pi_from_date
                                                                           AND pi_to_date)
                                  GROUP BY u.dynamic_param,
                                           u.combination_id,
                                           TRUNC (i.time_created)) z
                        GROUP BY z.DP, z.combination_id, z.dates) sh_us
                 --
                 WHERE     mc.campaign_manager = wcm.id
                       AND mc.id = mcomb.campaign_id
                       AND ms.id = mc.source_id
                       AND mcomb.content_id = mcont.id
                       AND mcomb.medium_id = mm.id
                       AND mcomb.size_id = msize.id(+)
                       AND mcomb.location_id = mlocation.id(+)
                       AND mcomb.landing_page_id = mlandpage.id
                       AND mcomb.id = sh_us.combination_id(+)
                       AND (   NVL (sh_us.short_reg, 0) > 0
                            OR NVL (sh_us.registered_users_num, 0) > 0
                            OR NVL (FTD, 0) > 0
                            OR NVL (house_win, 0) != 0
                            OR NVL (sh_us.registered_users_num, 0) > 0)
                       --
                       AND mcomb.skin_id =
                              DECODE (pi_skin_id, 0, mcomb.skin_id, pi_skin_id)
                       AND ms.id =
                              DECODE (pi_source_id, 0, ms.id, pi_source_id)
                       AND wcm.id =
                              DECODE (pi_campaign_manager_id,
                                      0, wcm.id,
                                      pi_campaign_manager_id)) A,
               (SELECT TRUNC (dates.COLUMN_VALUE) AS def_date
                  FROM TABLE (get_date_range (pi_from_date, pi_to_date)) dates) gen_dates
         WHERE gen_dates.def_date = A.DATES(+)
      ORDER BY gen_dates.def_date, A.DP;

      RETURN v_return;
   END GET_MARKETING_DP_REP_BY_DATE;

   ----------------------------------------------------------------------------------------------------------------

   ----------------------------------------------------------------------------------------------------------------

   FUNCTION GET_MARKETING_HTTP_REF_REPORT (pi_from_date              DATE,
                                           pi_to_date                DATE,
                                           pi_skin_id                NUMBER,
                                           pi_source_id              NUMBER,
                                           pi_campaign_manager_id    NUMBER)
      RETURN MARKETING_HTTP_REFERER_TL_TYPE
   AS
      v_return   MARKETING_HTTP_REFERER_TL_TYPE;
   BEGIN
      SELECT MARKETING_HTTP_REFERER_TYPE (A.HTTP_REFERER,
                                          A.DSQ,
                                          A.DP,
                                          A.campaign_manager,
                                          A.combination_id,
                                          A.campaign_name,
                                          A.source_name,
                                          A.medium_name,
                                          A.SKIN_ID,
                                          A.short_reg,
                                          A.registered_users_num,
                                          A.FTD,
                                          A.first_remarketing_dep_num)
        BULK COLLECT INTO v_return
        FROM (  SELECT DECODE (sh_us.HTTP_REFERER, '0', '', sh_us.HTTP_REFERER)
                          AS HTTP_REFERER,
                       DECODE (sh_us.DSQ, '0', '', sh_us.DSQ) AS DSQ,
                       DECODE (sh_us.DP, '0', '', sh_us.DP) AS DP,
                       wcm.user_name AS campaign_manager,
                       mcomb.id AS combination_id,
                       mc.name AS campaign_name,
                       ms.name AS source_name,
                       mm.name AS medium_name,
                       mcomb.skin_id AS SKIN_ID,
                       sh_us.short_reg,
                       sh_us.registered_users_num,
                       sh_us.FTD,
                       sh_us.first_remarketing_dep_num
                  FROM marketing_campaigns mc,
                       writers wcm,
                       marketing_combinations mcomb,
                       marketing_sources ms,
                       marketing_contents mcont,
                       marketing_sizes msize,
                       marketing_locations mlocation,
                       marketing_landing_pages mlandpage,
                       marketing_mediums mm,
                       --
                       (  SELECT z.HTTP_REFERER,
                                 z.DSQ,
                                 z.DP,
                                 SUM (z.short_reg) AS short_reg,
                                 SUM (registered_users_num)
                                    AS registered_users_num,
                                 SUM (z.FTD) AS FTD,
                                 SUM (z.first_remarketing_dep_num)
                                    AS first_remarketing_dep_num,
                                 z.combination_id
                            FROM (  --SHORT_REG
                                    SELECT NVL (c.http_referer, 0) AS HTTP_REFERER,
                                           CAST ('0' AS NVARCHAR2 (300)) AS DSQ,
                                           NVL (
                                              CAST (
                                                 c.dynamic_parameter AS NVARCHAR2 (1000)),
                                              0)
                                              AS DP,
                                           COUNT (*) AS short_reg,
                                           0 AS registered_users_num,
                                           0 AS FTD,
                                           0 AS first_remarketing_dep_num,
                                           c.combination_id
                                      FROM contacts c
                                     WHERE NVL (c.user_id, 0) = 0
                                           AND (TRUNC (c.time_created) BETWEEN pi_from_date
                                                                           AND pi_to_date)
                                           AND c.class_id <> 0
                                  GROUP BY c.combination_id,
                                           c.http_referer,
                                           c.dynamic_parameter
                                  --registered_users_num
                                  UNION ALL
                                    SELECT NVL (u.http_referer, 0) AS HTTP_REFERER,
                                           NVL (u.DECODED_SOURCE_QUERY, 0) AS DSQ,
                                           NVL (u.dynamic_param, 0) AS DP,
                                           0 AS short_reg,
                                           COUNT (*) AS registered_users_num,
                                           0 AS FTD,
                                           0 AS first_remarketing_dep_num,
                                           u.combination_id
                                      FROM users u
                                     WHERE (TRUNC (u.time_created) BETWEEN pi_from_date
                                                                       AND pi_to_date)
                                           AND u.class_id <> 0
                                  GROUP BY u.combination_id,
                                           u.http_referer,
                                           u.DECODED_SOURCE_QUERY,
                                           u.dynamic_param
                                  --FTD
                                  UNION ALL
                                    SELECT NVL (us.http_referer, 0) AS HTTP_REFERER,
                                           NVL (us.DECODED_SOURCE_QUERY, 0) AS DSQ,
                                           NVL (us.dynamic_param, 0) AS DP,
                                           0 AS short_reg,
                                           0 AS registered_users_num,
                                           COUNT (*) AS FTD,
                                           0 AS first_remarketing_dep_num,
                                           us.combination_id
                                      FROM users us, transactions tr
                                     WHERE us.first_deposit_id = tr.id
                                           AND us.class_id <> 0
                                           AND (TRUNC (tr.time_created) BETWEEN pi_from_date
                                                                            AND pi_to_date)
                                  GROUP BY us.http_referer,
                                           us.DECODED_SOURCE_QUERY,
                                           us.dynamic_param,
                                           us.combination_id
                                  -- first_remarketing_dep_num
                                  UNION ALL
                                    SELECT NVL (us.http_referer, 0) AS HTTP_REFERER,
                                           NVL (us.DECODED_SOURCE_QUERY, 0) AS DSQ,
                                           NVL (us.dynamic_param, 0) AS DP,
                                           0 AS short_reg,
                                           0 AS registered_users_num,
                                           0 AS FTD,
                                           COUNT (*) AS first_remarketing_dep_num,
                                           us.combination_id
                                      FROM users us,
                                           transactions tr,
                                           remarketing_logins rl
                                     WHERE     us.id = rl.user_id
                                           AND us.first_deposit_id = tr.id
                                           AND us.class_id <> 0
                                           AND (TRUNC (tr.time_created) BETWEEN pi_from_date
                                                                            AND pi_to_date)
                                  GROUP BY us.http_referer,
                                           us.DECODED_SOURCE_QUERY,
                                           us.dynamic_param,
                                           us.combination_id) z
                        GROUP BY z.HTTP_REFERER,
                                 z.DSQ,
                                 z.DP,
                                 z.combination_id) sh_us
                 --
                 WHERE     mc.campaign_manager = wcm.id
                       AND mc.id = mcomb.campaign_id
                       AND ms.id = mc.source_id
                       AND mcomb.content_id = mcont.id
                       AND mcomb.medium_id = mm.id
                       AND mcomb.size_id = msize.id(+)
                       AND mcomb.location_id = mlocation.id(+)
                       AND mcomb.landing_page_id = mlandpage.id
                       AND mcomb.id = sh_us.combination_id(+)
                       AND (   NVL (sh_us.short_reg, 0) > 0
                            OR NVL (sh_us.registered_users_num, 0) > 0
                            OR NVL (FTD, 0) > 0
                            OR NVL (sh_us.registered_users_num, 0) > 0)
                       --
                       AND mcomb.skin_id =
                              DECODE (pi_skin_id, 0, mcomb.skin_id, pi_skin_id)
                       AND ms.id =
                              DECODE (pi_source_id, 0, ms.id, pi_source_id)
                       AND wcm.id =
                              DECODE (pi_campaign_manager_id,
                                      0, wcm.id,
                                      pi_campaign_manager_id)
              ORDER BY 1) A;

      RETURN v_return;
   END GET_MARKETING_HTTP_REF_REPORT;
END MARKETING_REPORTS;
/
