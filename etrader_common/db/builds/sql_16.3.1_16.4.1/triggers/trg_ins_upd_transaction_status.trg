create or replace TRIGGER ETRADER.TRG_INS_UPD_TRANSACTION_STATUS 
BEFORE INSERT OR UPDATE
OF STATUS_ID
ON ETRADER.TRANSACTIONS 
referencing new as new old as old
for each row
declare
cursor cmain is
select count(*) as chck from transaction_types tt where tt.class_type = 1 and :new.type_id = tt.id;

v_chck NUMBER;

begin

open cmain;
fetch cmain into v_chck;
close cmain;

-- Only for real deposit
IF v_chck > 0 THEN
    IF :new.status_id in (2,7,8) THEN
         -- Increment total sum of deposits for success deposit (It represent by small amount converted to USD).
         IF :old.status_id is null OR :old.status_id not in (2,7,8) THEN
              update 
                USERS_ACTIVE_DATA uad
              set
                uad.SUM_DEPOSITS = (uad.SUM_DEPOSITS + (:new.amount * :new.rate)),
                uad.DEPOSITS_COUNT = uad.DEPOSITS_COUNT + 1
              where 
                uad.USER_ID = :new.user_id;
              -- Retargeting campaign  
              MARKETING.TRANSACTIONS_HANDLER(:new.user_id, :new.id, :new.time_created, :new.login_id);
         END IF;
    ELSE IF :old.status_id in (2,7,8) THEN
             -- Decrement from total sum of deposits when deposit was in status 2,7,8 but changed to a status different than 2,7,8 (It represent by small amount converted to USD).
             update 
               USERS_ACTIVE_DATA uad
             set
               uad.SUM_DEPOSITS = (uad.SUM_DEPOSITS - (:new.amount * :new.rate)),
               uad.DEPOSITS_COUNT = uad.DEPOSITS_COUNT - 1
             where 
               uad.USER_ID = :new.user_id;
         END IF;
    END IF;
END IF;
end TRG_INS_UPD_TRANSACTION_STATUS;
/
