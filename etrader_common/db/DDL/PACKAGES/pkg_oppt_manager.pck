create or replace package pkg_oppt_manager is

  -- Author  : VICTOR.SLAVOV
  -- Created : 2016-07-06 12:13:43
  -- Purpose : 

  function days_tobitmask(i_market_id in number) return number;
  function bitmask_todays(i_mask in number) return varchar2;

  function to_ttz
  (
    i_hours in varchar2
   ,i_tz    in varchar2 default 'UTC'
   ,i_date  in date default sysdate
  ) return timestamp
    with time zone;

  procedure get_opps_topause
  (
    o_data       out sys_refcursor
   ,i_as_of_time in timestamp default null
  );

  procedure get_opps_toresume
  (
    o_data       out sys_refcursor
   ,i_as_of_time in timestamp default null
  );

  procedure create_daily_opportunities
  (
    i_days_ahead in number
   ,i_market_id  in number default 0
  );

  procedure create_weekly_opportunities
  (
    i_days_ahead in number default 8
   ,i_market_id  in number default 0
  );

  procedure create_monthly_opportunities
  (
    i_months_ahead in number default 1
   ,i_market_id    in number default 0
  );

  procedure create_quarterly_opportunities
  (
    i_quarter      in number default 1
   ,i_market_id    in number default 0
  );
  
  function is_last_week_day(
     i_market_id in number
     , i_target_day date
     , i_exchange_id number
     , i_days_ahead number) 
     return number;
     
   function is_last_month_day(
     i_market_id in number
     , i_target_day date
     , i_exchange_id number
     ) 
     return number;

  function get_opportunity_state
		(
			i_exchange_id in number,
			i_market_id in number,
			i_time_first_invest in timestamp with time zone,
			i_time_last_invest in timestamp with time zone,
			i_time_act_closing in date,
			i_scheduled in number,
			i_time in timestamp with time zone default current_timestamp
		) return number;
    
end pkg_oppt_manager;
/
create or replace package body pkg_oppt_manager is

  -- %param io_day 
  -- %param i_mask 
  -- %param i_adjust This should be +1 when adjust first_day of the period and -1 for last_day
  -- %param i_exchange_id 
  procedure adjust_day
  (
    io_day         in out date
   ,i_fullday_mask in number
   ,i_adjust       in number
   ,i_exchange_id  in number
   ,o_is_half_day  out number
  ) is
    type t_days_char is table of number index by varchar2(3);
    l_days             t_days_char;
    l_half_day_holiday number;
  begin
    l_days('SUN') := power(2, 6);
    l_days('MON') := power(2, 5);
    l_days('TUE') := power(2, 4);
    l_days('WED') := power(2, 3);
    l_days('THU') := power(2, 2);
    l_days('FRI') := power(2, 1);
    l_days('SAT') := power(2, 0);
  
    o_is_half_day := 0;
    loop
      select min(is_half_day)
      into   l_half_day_holiday
      from   exchange_holidays
      where  exchange_id = i_exchange_id
      and    holiday = trunc(io_day);
    
      if bitand(i_fullday_mask, l_days(to_char(io_day, 'DY'))) > 0
      then
        case
          when l_half_day_holiday is null then
            exit;
          when l_half_day_holiday = 0 then
            io_day := io_day + i_adjust;
          when l_half_day_holiday = 1 then
            o_is_half_day := 1;
            exit;
        end case;
      else
        io_day := io_day + i_adjust;
      end if;
    end loop;
  end adjust_day;

  -- Not all markets has row in ASSET_INDEX. Will throw no_data_found in such case.
  function days_tobitmask(i_market_id in number) return number is
    l_workdays_str asset_index.trading_days%type;
    l_workdays     number;
    l_len          pls_integer;
  begin
    select a.trading_days into l_workdays_str from asset_index a where a.market_id = i_market_id;
    l_len := length(l_workdays_str);
  
    if l_len = 7
    then
      l_workdays := 0;
      for i in 1 .. l_len
      loop
        l_workdays := l_workdays + case
                        when substr(l_workdays_str, i, 1) = '1' then
                         power(2, l_len - i)
                        else
                         0
                      end;
      end loop;
    end if;
  
    return l_workdays;
  end days_tobitmask;

  function bitmask_todays(i_mask in number) return varchar2 is
    l_ret varchar2(7);
  begin
    if i_mask between 0 and 127
    then
      for i in reverse 1 .. 7
      loop
        l_ret := l_ret || case
                   when bitand(i_mask, power(2, i - 1)) > 0 then
                    '1'
                   else
                    '0'
                 end;
      end loop;
    end if;
  
    return l_ret;
  end bitmask_todays;

  function to_ttz
  (
    i_hours in varchar2
   ,i_tz    in varchar2 default 'UTC'
   ,i_date  in date default sysdate
  ) return timestamp
    with time zone is
    d timestamp with time zone;
  begin
    d := to_timestamp_tz(to_char(i_date, 'yyyymmdd') || i_hours || i_tz, 'yyyymmdd hh24:mi tzr');
    return d;
  exception
    when others then
      if sqlcode = -1878
      -- invalid time specified due to DST
      then
        return null;
      else
        raise;
      end if;
  end to_ttz;

  procedure insert_opp
  (
    i_opportunity       in opportunities%rowtype
   ,i_worst_case_return in number
   ,i_market_group_id   in number
   ,i_max_exposure      in number
  ) is
    l_max_exposure number;
  begin
    if i_opportunity.time_est_closing > systimestamp
       and i_opportunity.time_first_invest < i_opportunity.time_last_invest
    then
    
      insert into opportunities
        (id
        ,market_id
        ,time_created
        ,time_first_invest
        ,time_est_closing
        ,time_last_invest
        ,is_published
        ,is_settled
        ,odds_type_id
        ,writer_id
        ,opportunity_type_id
        ,is_disabled
        ,scheduled
        ,deduct_win_odds
        ,is_open_graph
        ,quote_params
        ,odds_group
        ,max_inv_coeff
        ,is_disabled_service
        ,is_disabled_trader
        ,is_suspended)
      values
        (i_opportunity.id
        ,i_opportunity.market_id
        ,i_opportunity.time_created
        ,i_opportunity.time_first_invest
        ,i_opportunity.time_est_closing
        ,i_opportunity.time_last_invest
        ,i_opportunity.is_published
        ,i_opportunity.is_settled
        ,i_opportunity.odds_type_id
        ,i_opportunity.writer_id
        ,i_opportunity.opportunity_type_id
        ,i_opportunity.is_disabled
        ,i_opportunity.scheduled
        ,i_opportunity.deduct_win_odds
        ,i_opportunity.is_open_graph
        ,i_opportunity.quote_params
        ,i_opportunity.odds_group
        ,i_opportunity.max_inv_coeff
        ,i_opportunity.is_disabled_service
        ,i_opportunity.is_disabled_trader
        ,i_opportunity.is_suspended);
    
      if i_market_group_id in (4, 5) AND
                                      (to_char(SYS_EXTRACT_UTC(i_opportunity.time_first_invest), 'hh24:mi') >= '22:00' OR
                                      to_char(SYS_EXTRACT_UTC(i_opportunity.time_first_invest), 'hh24:mi') < '06:00') THEN
          SELECT to_number(e.value) into l_max_exposure FROM enumerators e WHERE e.code LIKE 'NIGHT_MAX_EXPOSURE';
      else
          l_max_exposure := i_max_exposure;
      end if;
    
      insert into opportunity_skin_group_map
        (id, opportunity_id, skin_group_id, time_created, writer_id, shift_parameter, max_exposure, worst_case_return)
        select seq_opportunity_skin_group_map.nextval
              ,i_opportunity.id
              ,id
              ,i_opportunity.time_created
              ,0
              ,0
              ,l_max_exposure
              ,i_worst_case_return
        from   skin_groups
        where  id <> 0;
    
    else
      dbms_output.put_line('Invalid opporunity date(s) for market_id ' || i_opportunity.market_id);
    end if;
  
  end insert_opp;

  procedure get_opps_topause
  (
    o_data       out sys_refcursor
   ,i_as_of_time in timestamp default null
  ) is
     type t_days is table of number index by varchar2(3);
  
    l_days            t_days;
    l_as_of_time timestamp := systimestamp;
    l_dow        varchar2(3);
  begin
    l_days('SUN') := power(2, 6);
    l_days('MON') := power(2, 5);
    l_days('TUE') := power(2, 4);
    l_days('WED') := power(2, 3);
    l_days('THU') := power(2, 2);
    l_days('FRI') := power(2, 1);
    l_days('SAT') := power(2, 0);

    if i_as_of_time is not null
    then
      l_as_of_time := from_tz(i_as_of_time, 'UTC');
    end if;
  
    l_dow := to_char(l_as_of_time, 'DY');
    open o_data for
      select a.id
      from   opportunities a
      join   markets b
      on     b.id = a.market_id
      left   join exchange_holidays c
      on     c.exchange_id = b.exchange_id
      and    c.holiday = trunc(l_as_of_time)
      where  a.is_published = 1

      and    exists (select 1
              from   market_pauses p
              where  p.market_id = a.market_id
              and    p.scheduled = a.scheduled
              and    p.is_half_day = (case
                       when c.is_half_day is null then
                        0
                       when c.is_half_day = 1 then
                        1
                       else
                        null
                     end)
              and    l_as_of_time >= to_ttz(p.pause_start, to_char(a.time_first_invest, 'TZR')) and l_as_of_time <
                     to_ttz(p.pause_end
                           ,to_char(a.time_last_invest, 'TZR')
                           ,l_as_of_time + numtodsinterval(case
                                                             when p.pause_start > p.pause_end then
                                                              1
                                                             else
                                                              0
                                                           end
                                                          ,'DAY'))
              and   bitand(p.valid_days, l_days(l_dow)) > 0
              and   (p.pause_start < p.pause_end OR trunc(i_as_of_time) != trunc(a.time_est_closing))); -- do not pause the night pause on the last day
  end get_opps_topause;

  procedure get_opps_toresume
  (
    o_data       out sys_refcursor
   ,i_as_of_time in timestamp default null
  ) is
    type t_days is table of number index by varchar2(3);
  
    l_days            t_days;
    l_as_of_time      timestamp := systimestamp;
    l_target_dow varchar2(3);
  begin
    l_days('SUN') := power(2, 6);
    l_days('MON') := power(2, 5);
    l_days('TUE') := power(2, 4);
    l_days('WED') := power(2, 3);
    l_days('THU') := power(2, 2);
    l_days('FRI') := power(2, 1);
    l_days('SAT') := power(2, 0);
  
    if i_as_of_time is not null
    then
      l_as_of_time := from_tz(i_as_of_time, 'UTC');
    end if;
  
    l_target_dow := to_char(i_as_of_time, 'DY');
  
    open o_data for
      select a.id
      from   opportunities a
      join   markets b
      on     b.id = a.market_id
      join   market_pauses p
      on     p.market_id = a.market_id
      and    p.scheduled = a.scheduled
      left   join exchange_holidays c
      on     c.exchange_id = b.exchange_id
      and    c.holiday = trunc(l_as_of_time)
      where  a.is_published = 0
      and    l_as_of_time between a.time_first_invest and a.time_last_invest

      and    trunc(i_as_of_time) != trunc(a.time_first_invest) --- do not resume opps that open today
            
      and    bitand(p.valid_days, l_days(l_target_dow)) > 0 --- check if its a working day for the market

      -- make sure we hit only markets and schedules that are paused at all
      and    (
            --- if there is no holiday defined
             (c.id is null and not exists (select 1
                                            from   market_pauses p
                                            where  p.market_id = a.market_id
                                            and    p.scheduled = a.scheduled
                                            and    p.is_active = 1
                                            and    p.is_half_day = 0
                                            and    ((l_as_of_time >=
                                                   to_ttz(p.pause_start
                                                         , to_char(a.time_first_invest, 'TZR')
                                                         ,l_as_of_time + numtodsinterval(case
                                                                                           when p.pause_start > p.pause_end then
                                                                                            -1
                                                                                           else
                                                                                            0
                                                                                         end
                                                                                        ,'DAY'))
                                                   and l_as_of_time <
                                                   to_ttz(p.pause_end
                                                         ,to_char(a.time_last_invest, 'TZR')))
                                                or
                                                  (l_as_of_time >=
                                                   to_ttz(p.pause_start
                                                         , to_char(a.time_first_invest, 'TZR'))
                                                   and l_as_of_time <
                                                   to_ttz(p.pause_end
                                                         ,to_char(a.time_last_invest, 'TZR')
                                                         ,l_as_of_time + numtodsinterval(case
                                                                                           when p.pause_start > p.pause_end then
                                                                                            1
                                                                                           else
                                                                                            0
                                                                                         end
                                                                                        ,'DAY'))))
                                            and   bitand(p.valid_days, l_days(l_target_dow)) > 0))
            
             or
            --- if there is holiday defined, but its half day holiday
             (c.is_half_day = 1 and not exists (select 1
                                                 from   market_pauses p
                                                 where  p.market_id = a.market_id
                                                 and    p.scheduled = a.scheduled
                                                 and    p.is_active = 1
                                                 and    p.is_half_day = 1
                                                 and    ((l_as_of_time >=
                                                        to_ttz(p.pause_start
                                                              , to_char(a.time_first_invest, 'TZR')
                                                              ,l_as_of_time + numtodsinterval(case
                                                                                           when p.pause_start > p.pause_end then
                                                                                            -1
                                                                                           else
                                                                                            0
                                                                                         end
                                                                                        ,'DAY'))
                                                        and l_as_of_time <
                                                        to_ttz(p.pause_end
                                                              ,to_char(a.time_last_invest, 'TZR')))
                                                        or      
                                                          (l_as_of_time >=
                                                           to_ttz(p.pause_start
                                                                 , to_char(a.time_first_invest, 'TZR'))
                                                           and l_as_of_time <
                                                           to_ttz(p.pause_end
                                                                 ,to_char(a.time_last_invest, 'TZR')
                                                                 ,l_as_of_time + numtodsinterval(case
                                                                                                   when p.pause_start > p.pause_end then
                                                                                                    1
                                                                                                   else
                                                                                                    0
                                                                                                 end
                                                                                                ,'DAY'))))
                                                  and   bitand(p.valid_days, l_days(l_target_dow)) > 0))
            
            );
  end get_opps_toresume;

  procedure create_daily_opportunities
  (
    i_days_ahead in number
   ,i_market_id  in number default 0
  ) is
    type t_days_char is table of number index by varchar2(3);
  
    l_days            t_days_char;
    l_opp             opportunities%rowtype;
    l_systimestamp    timestamp := systimestamp;
    l_target_day      date := trunc(l_systimestamp) + i_days_ahead;
    l_target_dow varchar2(3) := to_char(l_target_day, 'DY');
    l_create          boolean;    
 
  begin
    l_days('SUN') := power(2, 6);
    l_days('MON') := power(2, 5);
    l_days('TUE') := power(2, 4);
    l_days('WED') := power(2, 3);
    l_days('THU') := power(2, 2);
    l_days('FRI') := power(2, 1);
    l_days('SAT') := power(2, 0);
  
    for i in (select a.market_id
                    ,a.scheduled
                    ,a.time_first_invest
                    ,a.time_last_invest
                    ,a.time_est_closing
                    ,a.time_last_invest_halfday
                    ,a.time_est_closing_halfday
                    ,a.time_zone
                    ,a.odds_type_id
                    ,a.writer_id
                    ,a.opportunity_type_id
                    ,a.deduct_win_odds
                    ,a.is_open_graph
                    ,a.max_inv_coeff
                    ,b.market_group_id
                    ,b.worst_case_return
                    ,b.max_exposure
                    ,b.quote_params
                    ,b.exchange_id
                    ,(select replace(g.odds_group, g.odds_group_default, g.odds_group_default || 'd')
                      from   opportunity_odds_group g
                      where  g.odds_group = a.odds_group) odds_group_fixed
                    ,case
                       when a.time_first_invest >= a.time_last_invest then
                        1
                       else
                        0
                     end add_days_last_invest
                    ,case
                       when a.time_first_invest >= a.time_est_closing then
                        1
                       else
                        0
                     end add_days_est_closing
                    ,(select h.is_half_day
                      from   exchange_holidays h
                      where  h.exchange_id = b.exchange_id
                      and    h.holiday = l_target_day) half_day_holiday
                    ,sun_f * power(2, 6) + mon_f * power(2, 5) + tue_f * power(2, 4) + wed_f * power(2, 3) + thu_f * power(2, 2) +
                     fri_f * power(2, 1) + sat_f * power(2, 0) full_day_mask
                    ,sun_h * power(2, 6) + mon_h * power(2, 5) + tue_h * power(2, 4) + wed_h * power(2, 3) + thu_h * power(2, 2) +
                     fri_h * power(2, 1) + sat_h * power(2, 0) half_day_mask
              from   markets b
              join   opportunity_templates a
              on     a.market_id = b.id
              where  (i_market_id = 0 or b.id = i_market_id)
              and    a.is_active = 1
              and    a.scheduled in (1, 2)
              order  by a.market_id)
    loop      
      l_create := false;

      case
        when i.scheduled = 1 then
        
          case
            when i.half_day_holiday = 1
                 and bitand(l_days(l_target_dow), i.half_day_mask) > 0 then
              --- Half of the day is a holiday and this is "half day" template.
              l_create := true;
            when i.half_day_holiday is null 
                 and bitand(l_days(l_target_dow), i.full_day_mask) > 0 
                 then
              --- No holiday defined and this is a "full day" template.
              l_create := true;
            when i.half_day_holiday = 0 then
              --- Whole day is a holiday. Will not create opportunities.
              l_create := false;
            else
              --- Weekend. Will not create opportunities.
                l_create := false;
          end case;
        
        when i.scheduled = 2 then

          if (i.half_day_holiday = 1 or i.half_day_holiday is null)
             and bitand(l_days(l_target_dow), i.full_day_mask) > 0
             --- Last working day of the week the weekly opp takes the place of the day one if a weekly exists
             and is_last_week_day(i.market_id, l_target_day, i.exchange_id, i_days_ahead) = 0
             --- Last working day of the month the montly opp takes the place of the day one if a montly exists
             and is_last_month_day(i.market_id, l_target_day, i.exchange_id) = 0                    
          then
            l_create := true;
          end if;
        
      end case;
    
      if l_create
      then
      
        l_opp.id                := seq_opportunities.nextval;
        l_opp.market_id         := i.market_id;
        l_opp.time_created      := l_systimestamp;
        l_opp.time_first_invest := to_ttz(i.time_first_invest, i.time_zone, l_systimestamp + i_days_ahead);
      
        if (i.half_day_holiday = 1 and i.scheduled = 2)
        then
          l_opp.time_est_closing := to_ttz(i.time_est_closing_halfday, i.time_zone, l_systimestamp + i_days_ahead);
          l_opp.time_last_invest := to_ttz(i.time_last_invest_halfday, i.time_zone, l_systimestamp + i_days_ahead);
        else
          l_opp.time_est_closing := to_ttz(i.time_est_closing, i.time_zone, l_systimestamp + i_days_ahead + i.add_days_est_closing);
          l_opp.time_last_invest := to_ttz(i.time_last_invest, i.time_zone, l_systimestamp + i_days_ahead + i.add_days_last_invest);
        end if;
      
        l_opp.is_published        := 0;
        l_opp.is_settled          := 0;
        l_opp.odds_type_id        := i.odds_type_id;
        l_opp.writer_id           := i.writer_id;
        l_opp.opportunity_type_id := i.opportunity_type_id;
        l_opp.is_disabled         := 0;
        l_opp.is_disabled_service := 0;
        l_opp.is_disabled_trader  := 0;
        l_opp.scheduled           := i.scheduled;
        l_opp.deduct_win_odds     := i.deduct_win_odds;
        l_opp.is_open_graph       := i.is_open_graph;
        l_opp.quote_params        := i.quote_params;
        l_opp.max_inv_coeff       := i.max_inv_coeff;
        l_opp.odds_group          := i.odds_group_fixed;
        l_opp.is_suspended        := 0;
        --        l_opp.auto_shift_parameter := 0; ?
      
        insert_opp(l_opp, i.worst_case_return, i.market_group_id, i.max_exposure);
      end if;
    
    end loop;    
  end create_daily_opportunities;

  procedure update_times_by_day_template
  (
    i_market_id number,
    i_day_first timestamp,
    i_day_last timestamp,
    i_time_zone varchar2,
    i_use_half_day_time number,
    i_add_days_last_invest number,
    i_add_days_est_closing number,
    io_opp in out opportunities%rowtype
  ) is

    cursor c_day_template(i_market_id number, i_dow number) is
        select
            *
        from
            opportunity_templates
        where
            market_id = i_market_id
            and scheduled = 2
            and is_active = 1
            and bitand(sun_f * power(2, 6) + mon_f * power(2, 5) + tue_f * power(2, 4) + wed_f * power(2, 3) + thu_f * power(2, 2) +
                     fri_f * power(2, 1) + sat_f * power(2, 0), i_dow) > 0;

    type t_days_char is table of number index by varchar2(3);
  
    l_days              t_days_char;
    l_day_template      opportunity_templates%rowtype;
  begin
    l_days('SUN') := power(2, 6);
    l_days('MON') := power(2, 5);
    l_days('TUE') := power(2, 4);
    l_days('WED') := power(2, 3);
    l_days('THU') := power(2, 2);
    l_days('FRI') := power(2, 1);
    l_days('SAT') := power(2, 0);
   
    open c_day_template(i_market_id, l_days(to_char(i_day_first, 'DY')));
    fetch c_day_template into l_day_template;
    if c_day_template%found then
      io_opp.time_first_invest := to_ttz(l_day_template.time_first_invest, i_time_zone, i_day_first);
    end if;
    close c_day_template;

    open c_day_template(i_market_id, l_days(to_char(i_day_last, 'DY')));
    fetch c_day_template into l_day_template;
    if c_day_template%found then
      if i_use_half_day_time = 0
      then
        io_opp.time_est_closing := to_ttz(l_day_template.time_est_closing, i_time_zone, i_day_last + i_add_days_est_closing);
        io_opp.time_last_invest := to_ttz(l_day_template.time_last_invest, i_time_zone, i_day_last + i_add_days_last_invest);
      else
        io_opp.time_est_closing := to_ttz(nvl(l_day_template.time_est_closing_halfday, l_day_template.time_est_closing), i_time_zone, i_day_last);
        io_opp.time_last_invest := to_ttz(nvl(l_day_template.time_last_invest_halfday, l_day_template.time_last_invest), i_time_zone, i_day_last);
      end if;
    end if;
    close c_day_template;
  end;

  procedure create_weekly_opportunities
  (
    i_days_ahead in number default 8
   ,i_market_id  in number default 0
  ) is
    l_opp               opportunities%rowtype;
    l_systimestamp      timestamp := systimestamp;
    l_day_first         timestamp;
    l_day_last          timestamp;
    l_use_half_day_time number;
  begin
  
    for i in (select a.market_id
                    ,a.scheduled
                    ,a.time_first_invest
                    ,a.time_last_invest
                    ,a.time_est_closing
                    ,a.time_last_invest_halfday
                    ,a.time_est_closing_halfday
                    ,a.time_zone
                    ,a.odds_type_id
                    ,a.writer_id
                    ,a.opportunity_type_id
                    ,a.deduct_win_odds
                    ,a.is_open_graph
                    ,a.max_inv_coeff
                    ,b.market_group_id
                    ,case -- hack for the bubbles markets
                       when b.type_id = 5 then
                        b.worst_case_return
                     end worst_case_return
                    ,b.exchange_id
                    ,b.max_exposure
                    ,case
                       when a.time_first_invest >= a.time_last_invest then
                        1
                       else
                        0
                     end add_days_last_invest
                    ,case
                       when a.time_first_invest >= a.time_est_closing then
                        1
                       else
                        0
                     end add_days_est_closing
                   ,(select replace(g.odds_group, g.odds_group_default, g.odds_group_default || 'd')
                      from   opportunity_odds_group g
                      where  g.odds_group = a.odds_group) odds_group_fixed
                    ,sun_f * power(2, 6) + mon_f * power(2, 5) + tue_f * power(2, 4) + wed_f * power(2, 3) + thu_f * power(2, 2) +
                     fri_f * power(2, 1) + sat_f * power(2, 0) full_day_mask
              from   markets b
              join   opportunity_templates a
              on     a.market_id = b.id
              where  (i_market_id = 0 or b.id = i_market_id)
              and    a.is_active = 1
              and    a.scheduled = 3
              order  by a.market_id)
    loop
      if i.full_day_mask = 0 then
        continue;
      end if;
    
      l_day_first := next_day(trunc(l_systimestamp) + i_days_ahead - 7, 'SUN');
      l_day_last  := l_day_first + 6;

      --- find first working day of the week and place it in l_day_first
      adjust_day(l_day_first, i.full_day_mask, 1, i.exchange_id, l_use_half_day_time);
    
      --- find last working day of the week and place it in l_day_last
      --- l_use_half_day_time is set as out param here
      adjust_day(l_day_last, i.full_day_mask, -1, i.exchange_id, l_use_half_day_time);

      l_opp.id                := seq_opportunities.nextval;
      l_opp.market_id         := i.market_id;
      l_opp.time_created      := l_systimestamp;
      l_opp.time_first_invest := to_ttz(i.time_first_invest, i.time_zone, l_day_first);
    
      if l_use_half_day_time = 0
      then
        l_opp.time_est_closing := to_ttz(i.time_est_closing, i.time_zone, l_day_last + i.add_days_est_closing);
        l_opp.time_last_invest := to_ttz(i.time_last_invest, i.time_zone, l_day_last + i.add_days_last_invest);
      else
        l_opp.time_est_closing := to_ttz(nvl(i.time_est_closing_halfday, i.time_est_closing), i.time_zone, l_day_last);
        l_opp.time_last_invest := to_ttz(nvl(i.time_last_invest_halfday, i.time_last_invest), i.time_zone, l_day_last);
      end if;
    
      update_times_by_day_template(i.market_id, l_day_first, l_day_last, i.time_zone, l_use_half_day_time, i.add_days_last_invest, i.add_days_est_closing, l_opp);
    
      l_opp.is_published        := 0;
      l_opp.is_settled          := 0;
      l_opp.odds_type_id        := i.odds_type_id;
      l_opp.writer_id           := i.writer_id;
      l_opp.opportunity_type_id := i.opportunity_type_id;
      l_opp.is_disabled         := 0;
      l_opp.is_disabled_service := 0;
      l_opp.is_disabled_trader  := 0;
      l_opp.scheduled           := i.scheduled;
      l_opp.deduct_win_odds     := i.deduct_win_odds;
      l_opp.is_open_graph       := i.is_open_graph;
      l_opp.odds_group          := i.odds_group_fixed;
      l_opp.max_inv_coeff       := i.max_inv_coeff;
      l_opp.is_suspended        := 0;
      -- l_opp.max_exposure  := i.max_exposure;
      -- l_opp.auto_shift_parameter := 0; ?
    
      --- if the end of the week is also end of the month the montly opp will take the place of the weekly
      if is_last_month_day(i.market_id, l_day_last, i.exchange_id) = 0 then       
            insert_opp(l_opp, i.worst_case_return, i.market_group_id, i.max_exposure);
      end if;
    end loop;
  end create_weekly_opportunities;

  procedure create_monthly_opportunities
  (
    i_months_ahead in number default 1
   ,i_market_id    in number default 0
  ) is
  
    cursor c_has_quarter(i_market_id number) is
        select
            1
        from
            opportunity_templates
        where
            market_id = i_market_id
            and scheduled = 6
            and is_active = 1;
    
    l_opp               opportunities%rowtype;
    l_systimestamp      timestamp := systimestamp;
    l_day_first         timestamp;
    l_day_last          timestamp;
    l_use_half_day_time number;
    l_has_quarter       number;
  begin
  
    for i in (select a.market_id
                    ,a.scheduled
                    ,a.time_first_invest
                    ,a.time_last_invest
                    ,a.time_est_closing
                    ,a.time_last_invest_halfday
                    ,a.time_est_closing_halfday
                    ,a.time_zone
                    ,a.odds_type_id
                    ,a.writer_id
                    ,a.opportunity_type_id
                    ,a.deduct_win_odds
                    ,a.is_open_graph
                    ,a.max_inv_coeff
                    ,b.market_group_id
                    ,b.exchange_id
                    ,b.max_exposure
                    ,case
                       when a.time_first_invest >= a.time_last_invest then
                        1
                       else
                        0
                     end add_days_last_invest
                    ,case
                       when a.time_first_invest >= a.time_est_closing then
                        1
                       else
                        0
                     end add_days_est_closing
                    ,(select replace(g.odds_group, g.odds_group_default, g.odds_group_default || 'd')
                      from   opportunity_odds_group g
                      where  g.odds_group = a.odds_group) odds_group_fixed
                    ,sun_f * power(2, 6) + mon_f * power(2, 5) + tue_f * power(2, 4) + wed_f * power(2, 3) + thu_f * power(2, 2) +
                     fri_f * power(2, 1) + sat_f * power(2, 0) full_day_mask
              from   markets b
              join   opportunity_templates a
              on     a.market_id = b.id
              where  (i_market_id = 0 or b.id = i_market_id)
              and    a.is_active = 1
              and    a.scheduled = 4
              order  by a.market_id)
    loop
      if i.full_day_mask = 0 then
        continue;
      end if;

      l_has_quarter := 0;
      open c_has_quarter(i.market_id);
      fetch c_has_quarter into l_has_quarter;
      close c_has_quarter;
      
      -- don't create montly for the last month of the quarter if there is active quarter template for this market
      if trunc(add_months(l_systimestamp, i_months_ahead), 'MM') = add_months(trunc(add_months(l_systimestamp, i_months_ahead), 'Q'), 2)
          and l_has_quarter = 1 then
        continue;
      end if;

      l_day_first := trunc(add_months(l_systimestamp, i_months_ahead), 'MM');
      l_day_last  := last_day(l_day_first);
    
      --- find first working day of the week and place it in l_day_first
      adjust_day(l_day_first, i.full_day_mask, 1, i.exchange_id, l_use_half_day_time);
    
      --- find last working day of the week and place it in l_day_last
      --- l_use_half_day_time is set as out param here
      adjust_day(l_day_last, i.full_day_mask, -1, i.exchange_id, l_use_half_day_time);
    
      l_opp.id                := seq_opportunities.nextval;
      l_opp.market_id         := i.market_id;
      l_opp.time_created      := l_systimestamp;
      l_opp.time_first_invest := to_ttz(i.time_first_invest, i.time_zone, l_day_first);
    
      if l_use_half_day_time = 0
      then
        l_opp.time_est_closing := to_ttz(i.time_est_closing, i.time_zone, l_day_last + i.add_days_est_closing);
        l_opp.time_last_invest := to_ttz(i.time_last_invest, i.time_zone, l_day_last + i.add_days_last_invest);
      else
        l_opp.time_est_closing := to_ttz(nvl(i.time_est_closing_halfday, i.time_est_closing), i.time_zone, l_day_last);
        l_opp.time_last_invest := to_ttz(nvl(i.time_last_invest_halfday, i.time_last_invest), i.time_zone, l_day_last);
      end if;
    
      update_times_by_day_template(i.market_id, l_day_first, l_day_last, i.time_zone, l_use_half_day_time, i.add_days_last_invest, i.add_days_est_closing, l_opp);
    
      l_opp.is_published        := 0;
      l_opp.is_settled          := 0;
      l_opp.odds_type_id        := i.odds_type_id;
      l_opp.writer_id           := i.writer_id;
      l_opp.opportunity_type_id := i.opportunity_type_id;
      l_opp.is_disabled         := 0;
      l_opp.is_disabled_service := 0;
      l_opp.is_disabled_trader  := 0;
      l_opp.scheduled           := i.scheduled;
      l_opp.deduct_win_odds     := i.deduct_win_odds;
      l_opp.is_open_graph       := i.is_open_graph;
      l_opp.odds_group          := i.odds_group_fixed;
      l_opp.max_inv_coeff       := i.max_inv_coeff;
      l_opp.is_suspended        := 0;
      -- l_opp.max_exposure := i.max_exposure;
      -- l_opp.auto_shift_parameter := 0; ?
    
      insert_opp(l_opp, null, i.market_group_id, i.max_exposure);
    
    end loop;
  end create_monthly_opportunities;
  
  procedure create_quarterly_opportunities
  (
    i_quarter in number default 1
   ,i_market_id    in number default 0
  ) is
    l_opp               opportunities%rowtype;
    l_systimestamp      timestamp := systimestamp;
    l_day_first         timestamp;
    l_day_last          timestamp;
    l_use_half_day_time number;
  begin
  
    for i in (select a.market_id
                    ,a.scheduled
                    ,a.time_first_invest
                    ,a.time_last_invest
                    ,a.time_est_closing
                    ,a.time_last_invest_halfday
                    ,a.time_est_closing_halfday
                    ,a.time_zone
                    ,a.odds_type_id
                    ,a.writer_id
                    ,a.opportunity_type_id
                    ,a.deduct_win_odds
                    ,a.is_open_graph
                    ,a.max_inv_coeff
                    ,b.market_group_id
                    ,b.exchange_id
                    ,b.max_exposure
                    ,case
                       when a.time_first_invest >= a.time_last_invest then
                        1
                       else
                        0
                     end add_days_last_invest
                    ,case
                       when a.time_first_invest >= a.time_est_closing then
                        1
                       else
                        0
                     end add_days_est_closing
                    ,(select replace(g.odds_group, g.odds_group_default, g.odds_group_default || 'd')
                      from   opportunity_odds_group g
                      where  g.odds_group = a.odds_group) odds_group_fixed
                    ,sun_f * power(2, 6) + mon_f * power(2, 5) + tue_f * power(2, 4) + wed_f * power(2, 3) + thu_f * power(2, 2) +
                     fri_f * power(2, 1) + sat_f * power(2, 0) full_day_mask
              from   markets b
              join   opportunity_templates a
              on     a.market_id = b.id
              where  (i_market_id = 0 or b.id = i_market_id)
              and    a.is_active = 1
              and    a.scheduled = 6
              order  by a.market_id)
    loop
      if i.full_day_mask = 0 then
        continue;
      end if;

      l_day_first := add_months(trunc(add_months(l_systimestamp, i_quarter * 3), 'Q'), -1); -- 1 month back from the beginning of the quarter
      l_day_last  := last_day(add_months(l_day_first, 3));
    
      --- find first working day of the week and place it in l_day_first
      adjust_day(l_day_first, i.full_day_mask, 1, i.exchange_id, l_use_half_day_time);
    
      --- find last working day of the week and place it in l_day_last
      adjust_day(l_day_last, i.full_day_mask, -1, i.exchange_id, l_use_half_day_time);
    
      l_opp.id                := seq_opportunities.nextval;
      l_opp.market_id         := i.market_id;
      l_opp.time_created      := l_systimestamp;
      l_opp.time_first_invest := to_ttz(i.time_first_invest, i.time_zone, l_day_first);
    
      if l_use_half_day_time = 0
      then
        l_opp.time_est_closing := to_ttz(i.time_est_closing, i.time_zone, l_day_last + i.add_days_est_closing);
        l_opp.time_last_invest := to_ttz(i.time_last_invest, i.time_zone, l_day_last + i.add_days_last_invest);
      else
        l_opp.time_est_closing := to_ttz(nvl(i.time_est_closing_halfday, i.time_est_closing), i.time_zone, l_day_last);
        l_opp.time_last_invest := to_ttz(nvl(i.time_last_invest_halfday, i.time_last_invest), i.time_zone, l_day_last);
      end if;
    
      update_times_by_day_template(i.market_id, l_day_first, l_day_last, i.time_zone, l_use_half_day_time, i.add_days_last_invest, i.add_days_est_closing, l_opp);
    
      l_opp.is_published        := 0;
      l_opp.is_settled          := 0;
      l_opp.odds_type_id        := i.odds_type_id;
      l_opp.writer_id           := i.writer_id;
      l_opp.opportunity_type_id := i.opportunity_type_id;
      l_opp.is_disabled         := 0;
      l_opp.is_disabled_service := 0;
      l_opp.is_disabled_trader  := 0;
      l_opp.scheduled           := i.scheduled;
      l_opp.deduct_win_odds     := i.deduct_win_odds;
      l_opp.is_open_graph       := i.is_open_graph;
      l_opp.odds_group          := i.odds_group_fixed;
      l_opp.max_inv_coeff       := i.max_inv_coeff;
      l_opp.is_suspended        := 0;
      -- l_opp.max_exposure := i.max_exposure;
      -- l_opp.auto_shift_parameter := 0; ?
    
      insert_opp(l_opp, null, i.market_group_id, i.max_exposure);
    
    end loop;
  end create_quarterly_opportunities;
  
    -- Check if is last working week day and exist weekly opp template
  function is_last_week_day(i_market_id in number, i_target_day date, i_exchange_id number, i_days_ahead number) return number is
   cursor c_isExistOPType(i_market_id number, i_op_scheduled number) is
    select 1 from 
    opportunity_templates ot
     where ot.market_id = i_market_id
     and ot.scheduled = i_op_scheduled
     and ot.is_active = 1;
    
    l_isExistWeekly number;
    
    l_res number;
  begin
    l_isExistWeekly :=0;         
    l_res :=0;

    open c_isExistOPType(i_market_id, 3);
    fetch c_isExistOPType into l_isExistWeekly;
    close c_isExistOPType;
     IF (l_isExistWeekly = 1
                      and i_target_day = Week_last_working_day(i_exchange_id, floor((to_char(current_date, 'D') + i_days_ahead) / 7)))  THEN
       l_res :=1;               
     END IF;
    return l_res;
  end is_last_week_day;
  
  -- Check if is last working month day and exist moonth opp template
  function is_last_month_day(i_market_id in number, i_target_day date, i_exchange_id number) return number is
   cursor c_isExistOPType(i_market_id number, i_op_scheduled number) is
    select 1 from 
    opportunity_templates ot
     where ot.market_id = i_market_id
     and ot.scheduled = i_op_scheduled
     and ot.is_active = 1; 
         
    l_isExistMonthly number;
    
    l_res number;
  begin
    
    l_isExistMonthly:=0;          
    l_res :=0;

    open c_isExistOPType(i_market_id, 4);
    fetch c_isExistOPType into l_isExistMonthly;
    close c_isExistOPType;
     IF ( l_isExistMonthly = 1
                      and i_target_day = Month_last_working_day(i_exchange_id, 0))  THEN
       l_res :=1;               
     END IF;
    return l_res;
  end is_last_month_day;

    function get_opportunity_state
        (
            i_exchange_id in number,
            i_market_id in number,
            i_time_first_invest in timestamp with time zone,
            i_time_last_invest in timestamp with time zone,
            i_time_act_closing in date,
            i_scheduled in number,
            i_time in timestamp with time zone default current_timestamp
        ) return number is

    type t_days is table of number index by varchar2(3);
  
    cursor c_holiday(i_exchange_id number, i_time date) is
        select
            is_half_day
        from
            exchange_holidays
        where
            exchange_id = i_exchange_id
            and holiday = trunc(i_time);
        
    cursor c_pause_interval(i_market_id number, i_scheduled number, i_is_half_day number, i_tzr varchar2, i_day_mask number, i_time timestamp with time zone) is
        select 1
        from   market_pauses p
        where  p.market_id = i_market_id
        and    p.scheduled = i_scheduled
        and    p.is_half_day = i_is_half_day
        and    ((i_time between
               to_ttz(p.pause_start
                     , i_tzr
                     ,i_time + numtodsinterval(case
                                               when p.pause_start > p.pause_end then
                                                -1
                                               else
                                                0
                                             end
                                            ,'DAY'))
               and
               to_ttz(p.pause_end, i_tzr, i_time))
            or
              (i_time between
               to_ttz(p.pause_start, i_tzr, i_time)
               and
               to_ttz(p.pause_end
                     ,i_tzr
                     ,i_time + numtodsinterval(case
                                               when p.pause_start > p.pause_end then
                                                1
                                               else
                                                0
                                             end
                                            ,'DAY'))))
        and   bitand(p.valid_days, i_day_mask) > 0;

    cursor c_working_day(i_market_id number, i_scheduled number, i_day_mask number) is
        select  1
        from    opportunity_templates
        where   market_id = i_market_id
        and     scheduled = i_scheduled
        and     bitand(sun_f * power(2, 6) + mon_f * power(2, 5) + tue_f * power(2, 4) + wed_f * power(2, 3) + thu_f * power(2, 2) + fri_f * power(2, 1) + sat_f * power(2, 0)
                     , i_day_mask) > 0;

    l_is_half_day        number;
    l_pause_interval     number;
    l_working_day        number;
    l_days               t_days;
        
    begin
        l_days('SUN') := power(2, 6);
        l_days('MON') := power(2, 5);
        l_days('TUE') := power(2, 4);
        l_days('WED') := power(2, 3);
        l_days('THU') := power(2, 2);
        l_days('FRI') := power(2, 1);
        l_days('SAT') := power(2, 0);

        if i_time < i_time_first_invest then
            return 1; -- CREATED
        end if;
        if i_time > i_time_act_closing + 1/1440 then
            return 7; -- DONE
        end if;
        if i_time > i_time_act_closing then
            return 6; -- CLOSED
        end if;
        if i_time > i_time_last_invest + to_dsinterval('0 00:01:00') then
            return 5; -- CLOSING
        end if;
        if i_time > i_time_last_invest then
            return 4; -- CLOSING 1 MIN
        end if;
        if i_time > i_time_last_invest - to_dsinterval('0 00:10:00') then
            return 3; -- LAST 10 MIN
        end if;
        
        -- now we are between time_first_invest and time_last_invest of the opportunity
        
        -- for now no pauses for the hourly opportunities
        if i_scheduled = 1 then
            return 2; -- OPENED
        end if;
        
        open c_holiday(i_exchange_id, i_time);
        fetch c_holiday into l_is_half_day;
        close c_holiday;
        
        -- it is a full day holiday today
        if l_is_half_day = 0 then
            return 8; -- PAUSED
        end if;
        
        -- for the purpose of the next cursor call the value should be 1 for half day and 0 for full day
        if l_is_half_day is null then
            l_is_half_day := 0;
        end if;
        
        open c_pause_interval(i_market_id, i_scheduled, l_is_half_day, to_char(i_time_first_invest, 'TZR'), l_days(to_char(i_time, 'DY')), i_time);
        fetch c_pause_interval into l_pause_interval;
        close c_pause_interval;
        
        -- if it falls into a pause interval then it is paused
        if l_pause_interval = 1 then
            return 8; -- PAUSED
        end if;
        
        open c_working_day(i_market_id, i_scheduled, l_days(to_char(i_time, 'DY')));
        fetch c_working_day into l_working_day;
        close c_working_day;
        
        if l_working_day is null then
          return 8;
        end if;
        
        return 2; -- OPENED
    end get_opportunity_state;



end pkg_oppt_manager;
/