create or replace package pkg_transactions is

  -- Author  : VICTOR.SLAVOV
  -- Created : 2016-08-12 10:07:59
  -- Purpose : /etrader_level_service/src/il/co/etrader/service/transactionsIssues/daos/TransactionsIssuesDAO.java

  procedure get_last_trnsusers
  (
    o_data         out sys_refcursor
   ,i_last_minutes in number default 30
  );
  
  PROCEDURE GET_GM_PERM_DISPLAY(O_DIRECT24 OUT NUMBER,
                                O_GIROPAY  OUT NUMBER,
                                O_EPS      OUT NUMBER,
                                I_USER_ID  IN NUMBER);  
                                
  PROCEDURE GET_PERM_DISPLAY(O_DIRECT24 OUT NUMBER,
                                O_GIROPAY  OUT NUMBER,
                                O_EPS      OUT NUMBER,
                                O_IDEAL    OUT NUMBER,
                                I_USER_ID  IN NUMBER);                                  
                                
  PROCEDURE GET_TOTAL_DEPOSITS(O_TOTAL_DEPOSITS OUT NUMBER,
                               O_TOTAL_WITHDRAWALS OUT NUMBER,
                               I_GM_TYPE        IN NUMBER,
                               I_USER_ID        IN NUMBER);
                               
  PROCEDURE GET_TRANSACTION_CHANGES(I_TRANSACTION_ID IN NUMBER,
                                    O_TYPES OUT SYS_REFCURSOR,
                                    O_STATUSES OUT SYS_REFCURSOR);
                                    
  PROCEDURE INSERT_CRYPTO_TRADE_TRN(i_market             IN crypto_trade.market%TYPE,
                                    i_from_currency       IN crypto_trade.from_currency%TYPE,
                                    i_to_currency        IN crypto_trade.to_currency%TYPE,
                                    i_client_currency    IN crypto_trade.client_currency%TYPE,
                                 --   i_quantity           IN crypto_trade.quantity%TYPE,
                                    i_quantity_client    IN crypto_trade.quantity_client%TYPE,
                                    i_amount             IN crypto_trade.amount%TYPE,
                                    i_original_amount    IN crypto_trade.original_amount%TYPE,
                                    i_fee                IN crypto_trade.fee%TYPE,
                                    i_clearance_fee      IN crypto_trade.clearance_fee%TYPE,
                                    i_rate               IN crypto_trade.rate%TYPE,
                                    i_rate_market_client IN crypto_trade.rate_market_client%TYPE,
                                  --  i_rate_market        IN crypto_trade.rate_market%TYPE,
                                    --i_status             IN crypto_trade.status%TYPE,
                                    i_transaction_id     IN crypto_trade.transaction_id%TYPE,
                                    --i_uuid               IN crypto_trade.uuid%TYPE,
                                    --i_comments           IN crypto_trade.comments%TYPE,
                                    i_wallet             IN crypto_trade.wallet%TYPE,
                                    i_user_id            IN users.id%TYPE,
                                    i_currency_id        IN currencies.id%TYPE,
                                    i_currency_to_set    IN NUMBER);
                                    
  PROCEDURE GET_TOTAL_DEPOSITS_PER_MONTH(i_user_id              IN transactions.id%TYPE,
                                         o_total_deposits_month OUT transactions.amount%TYPE);
                                         
  PROCEDURE GET_ORDER_HISTORY(o_his_transactions OUT SYS_REFCURSOR,
                              i_user_id          IN transactions.id%TYPE,
                              i_results_from     IN NUMBER,
                              i_results_to       IN NUMBER);
END  pkg_transactions;
/
create or replace package body pkg_transactions is

  procedure get_last_trnsusers
  (
    o_data         out sys_refcursor
   ,i_last_minutes in number default 30
  ) is
    l_fromdate date;
    l_todate   date;
  begin
    select date_value - i_last_minutes/(24*60), sysdate - i_last_minutes/(24*60)
    into   l_fromdate, l_todate
    from   db_parameters
    where  name = 'last_first_deposit_email_run';
  
    open o_data for
      select u.id
            ,u.user_name
            ,u.skin_id
            ,u.mobile_phone
            ,u.land_line_phone
            ,u.currency_id
            ,t.amount          first_deposit_amount
            ,t.time_created    first_deposit_time_created
            ,c.phone_code      country_phone_code
            ,c.country_name    country_name
      from   transactions t
      join   users u
      on     u.id = t.user_id
      and    u.first_deposit_id = t.id
      join   countries c
      on     u.country_id = c.id
      where  t.time_created between l_fromdate and l_todate
      and    not exists (select 1 from transactions_issues where transaction_id = t.id)
      and    u.class_id <> 0;
  
  end get_last_trnsusers;
  
  PROCEDURE GET_GM_PERM_DISPLAY(O_DIRECT24 OUT NUMBER,
                                O_GIROPAY  OUT NUMBER,
                                O_EPS      OUT NUMBER,
                                I_USER_ID  IN NUMBER) IS
  
  BEGIN
  
    SELECT COUNT(*)
      INTO O_DIRECT24
      FROM TRANSACTIONS T
     WHERE T.USER_ID = I_USER_ID
       AND T.TYPE_ID = 17
       AND T.PAYMENT_TYPE_ID = 2
       AND T.STATUS_ID = 2; -- only status "succeed"
  
    SELECT COUNT(*)
      INTO O_GIROPAY
      FROM TRANSACTIONS T
     WHERE T.USER_ID = I_USER_ID
       AND T.TYPE_ID = 17
       AND T.PAYMENT_TYPE_ID = 3
       AND T.STATUS_ID = 2;
  
    SELECT COUNT(*)
      INTO O_EPS
      FROM TRANSACTIONS T
     WHERE T.USER_ID = I_USER_ID
       AND T.TYPE_ID = 17
       AND T.PAYMENT_TYPE_ID = 4
       AND T.STATUS_ID = 2;
  
  END GET_GM_PERM_DISPLAY;
  
  PROCEDURE GET_PERM_DISPLAY(O_DIRECT24 OUT NUMBER,
                                O_GIROPAY  OUT NUMBER,
                                O_EPS      OUT NUMBER,
                                O_IDEAL    OUT NUMBER,
                                I_USER_ID  IN NUMBER) IS
  
  BEGIN
  
    SELECT COUNT(*)
      INTO O_DIRECT24
      FROM TRANSACTIONS T
     WHERE T.USER_ID = I_USER_ID
       AND T.TYPE_ID = 17
       AND T.PAYMENT_TYPE_ID = 2
       AND T.STATUS_ID = 2; -- only status "succeed"
  
    SELECT COUNT(*)
      INTO O_GIROPAY
      FROM TRANSACTIONS T
     WHERE T.USER_ID = I_USER_ID
       AND T.TYPE_ID = 17
       AND T.PAYMENT_TYPE_ID = 3
       AND T.STATUS_ID = 2;
  
    SELECT COUNT(*)
      INTO O_EPS
      FROM TRANSACTIONS T
     WHERE T.USER_ID = I_USER_ID
       AND T.TYPE_ID = 17
       AND T.PAYMENT_TYPE_ID = 4
       AND T.STATUS_ID = 2;
    
    SELECT COUNT(*)
      INTO O_IDEAL
      FROM TRANSACTIONS T
     WHERE T.USER_ID = I_USER_ID
       AND T.TYPE_ID = 54
       AND T.STATUS_ID = 2;
  
  END GET_PERM_DISPLAY;
  
  PROCEDURE GET_TOTAL_DEPOSITS(O_TOTAL_DEPOSITS OUT NUMBER,
                               O_TOTAL_WITHDRAWALS OUT NUMBER,
                               I_GM_TYPE        IN NUMBER,
                               I_USER_ID        IN NUMBER) IS
 
    L_TRANSACTION_TYPE NUMBER := 17;
  
    DIRECT24_WITHDRAW CONSTANT NUMBER := 59;
    GIROPAY_WITHDRAW  CONSTANT NUMBER := 60;
    EPS_WITHDRAW      CONSTANT NUMBER := 61;
    IDEAL_WITHDRAW    CONSTANT NUMBER := 63;

  
  BEGIN    
     IF I_GM_TYPE = IDEAL_WITHDRAW THEN  
        L_TRANSACTION_TYPE := 54;
     END IF;
     
        SELECT nvl(SUM(T.AMOUNT), 0) AS TOTAL_DEPOSITS
          INTO O_TOTAL_DEPOSITS
          FROM TRANSACTIONS T
         WHERE T.USER_ID = I_USER_ID
           AND T.TYPE_ID = L_TRANSACTION_TYPE  
           AND T.PAYMENT_TYPE_ID = CASE
                 WHEN I_GM_TYPE = 61 THEN
                  4
                 WHEN I_GM_TYPE = 60 THEN
                  3
                 WHEN I_GM_TYPE = 59 THEN
                  2
                 WHEN I_GM_TYPE = 63 THEN
                  0
               END
           AND T.STATUS_ID = 2;
     
        SELECT 
          NVL(
            SUM(
              CASE
                WHEN TR.TYPE_ID IS NOT NULL 
                THEN T.AMOUNT - TR.AMOUNT
                ELSE T.AMOUNT
              END
              ), 
            0) AS TOTAL_WITHDRAWALS
          INTO O_TOTAL_WITHDRAWALS
        FROM 
          TRANSACTIONS T
          LEFT JOIN TRANSACTIONS TR ON (T.ID = TR.REFERENCE_ID AND TR.TYPE_ID = 6)
        WHERE 
          T.TYPE_ID = I_GM_TYPE
          AND T.USER_ID = I_USER_ID
          AND T.STATUS_ID <> 3; 
    
  END GET_TOTAL_DEPOSITS;

 PROCEDURE GET_TRANSACTION_CHANGES(I_TRANSACTION_ID IN NUMBER,
                                    O_TYPES OUT SYS_REFCURSOR,
                                    O_STATUSES OUT SYS_REFCURSOR) IS
  BEGIN
      OPEN O_TYPES FOR
           SELECT TMP.DESCRIPTION, TMP.CLASS_TYPE, TMP.ID FROM (
            SELECT DESCRIPTION, CLASS_TYPE, ID
                    FROM TRANSACTION_TYPES
                    WHERE ID IN (
                (SELECT TYPE_ID 
                 FROM TRANSACTION_TYPES_CHANGES
                 WHERE TO_TYPE = 1 
                       AND TYPE_ID <> 
                        (SELECT TT.ID
                          FROM TRANSACTIONS T 
                          INNER JOIN TRANSACTION_TYPES_CHANGES TTC 
                              ON T.TYPE_ID = TTC.TYPE_ID
                          JOIN TRANSACTION_TYPES TT
                              ON TTC.TYPE_ID = TT.ID
                          WHERE T.ID = I_TRANSACTION_ID
                              AND TTC.FROM_TYPE = 1   
                          )
                  )
                )
            ) TMP
            JOIN TRANSACTIONS T 
            ON T.ID = I_TRANSACTION_ID
            JOIN TRANSACTION_TYPES TT 
            ON T.TYPE_ID = TT.ID
            WHERE TMP.CLASS_TYPE = 
                  (CASE
                       WHEN TT.CLASS_TYPE = 1 THEN 1 
                       WHEN TT.CLASS_TYPE = 5 THEN 1 
                       WHEN TT.CLASS_TYPE = 2 THEN 2 
                       WHEN TT.CLASS_TYPE = 6 THEN 2 
                  END)
            ;
        
      OPEN O_STATUSES FOR
       SELECT TMP.DESCRIPTION, TMP.ID FROM (
          SELECT TS.DESCRIPTION, TS.ID 
          FROM TRANSACTION_STATUSES TS
          WHERE TS.ID IN (
                      SELECT DISTINCT STATUS_ID 
                      FROM TRANSACTION_STATUSES_CHANGES
                      WHERE TO_TYPE = 1 
                            AND STATUS_ID <> 
                              (SELECT TSC.STATUS_ID STATUS
                               FROM TRANSACTIONS T
                               INNER JOIN TRANSACTION_STATUSES_CHANGES TSC
                               ON TSC.TYPE_ID = T.TYPE_ID
                                  AND TSC.STATUS_ID = T.STATUS_ID
                               JOIN TRANSACTION_TYPES TT
                               ON T.TYPE_ID = TT.ID
                               JOIN TRANSACTION_STATUSES TS
                               ON T.STATUS_ID = TS.ID
                               WHERE T.ID = I_TRANSACTION_ID
                                     AND TSC.FROM_TYPE = 1
                              )
                      ) 
          ) TMP
      JOIN TRANSACTIONS T 
      ON T.ID = I_TRANSACTION_ID 
      JOIN TRANSACTION_TYPES TT
      ON T.TYPE_ID = TT.ID
      WHERE ( (TT.CLASS_TYPE = 1 AND TMP.ID <> 4) 
              OR (TT.CLASS_TYPE = 2 AND TMP.ID <> 11)
            );
  END GET_TRANSACTION_CHANGES;
  
PROCEDURE INSERT_CRYPTO_TRADE_TRN(i_market             IN crypto_trade.market%TYPE,
                                  i_from_currency       IN crypto_trade.from_currency%TYPE,
                                  i_to_currency        IN crypto_trade.to_currency%TYPE,
                                  i_client_currency    IN crypto_trade.client_currency%TYPE,
                                --  i_quantity           IN crypto_trade.quantity%TYPE,
                                  i_quantity_client    IN crypto_trade.quantity_client%TYPE,
                                  i_amount             IN crypto_trade.amount%TYPE,
                                  i_original_amount    IN crypto_trade.original_amount%TYPE,
                                  i_fee                IN crypto_trade.fee%TYPE,
                                  i_clearance_fee      IN crypto_trade.clearance_fee%TYPE,
                                  i_rate               IN crypto_trade.rate%TYPE,
                                  i_rate_market_client IN crypto_trade.rate_market_client%TYPE,
                                 -- i_rate_market        IN crypto_trade.rate_market%TYPE,
                                  --i_status             IN crypto_trade.status%TYPE,
                                  i_transaction_id     IN crypto_trade.transaction_id%TYPE,
                                 -- i_uuid               IN crypto_trade.uuid%TYPE,
                                 -- i_comments           IN crypto_trade.comments%TYPE,
                                  i_wallet             IN crypto_trade.wallet%TYPE,
                                  i_user_id            IN users.id%TYPE,
                                  i_currency_id        IN currencies.id%TYPE,
                                  i_currency_to_set    IN NUMBER) IS

  l_crypto_trade_id crypto_trade.id%TYPE;

BEGIN

IF i_currency_to_set = 1  THEN
  UPDATE USERS u
     SET u.currency_id = i_currency_id
   WHERE u.id = i_user_id;
END IF;

  INSERT INTO crypto_trade
    (id,
     market,
     from_currency,
     to_currency,
     client_currency,
   --  quantity,
     quantity_client,
     amount,
     original_amount,
     fee,
     clearance_fee,
     rate,
     rate_market_client,
    -- rate_market,
     status,
     transaction_id,
     --uuid,
    -- comments,
     time_created,
     time_opened,
     wallet,
     user_id)
  VALUES
    (crypto_trade_seq.nextval,
     i_market,
     i_from_currency,
     i_to_currency,
     i_client_currency,
    -- i_quantity,
     i_quantity_client,
     i_amount,
     i_original_amount,
     i_fee,
     i_clearance_fee,
     i_rate,
     i_rate_market_client,
    -- i_rate_market,
     0,--always 0 for newly inserted transactions
     i_transaction_id,
     --i_uuid,
    -- i_comments,
     SYSDATE,
     SYSDATE,
     i_wallet,
     i_user_id)
  returning id into l_crypto_trade_id;

END INSERT_CRYPTO_TRADE_TRN;


PROCEDURE GET_TOTAL_DEPOSITS_PER_MONTH(i_user_id              IN transactions.id%TYPE,
                                       o_total_deposits_month OUT transactions.amount%TYPE) IS

BEGIN

  SELECT NVL(SUM(tr.amount), 0) AS total_deposits_month
    INTO o_total_deposits_month
    FROM transactions tr
   WHERE tr.status_id = 2
     AND tr.user_id = i_user_id
     AND tr.time_created BETWEEN TRUNC(SYSDATE, 'mm') /*current_month_first_day*/
         AND TRUNC(last_day(SYSDATE), 'dd') /*current_month_last_day*/;

END GET_TOTAL_DEPOSITS_PER_MONTH;

PROCEDURE GET_ORDER_HISTORY(o_his_transactions OUT SYS_REFCURSOR,
                            i_user_id          IN transactions.id%TYPE,
                            i_results_from     IN NUMBER,
                            i_results_to       IN NUMBER) IS

BEGIN

  OPEN o_his_transactions FOR
  SELECT rownum, a.* FROM(
    SELECT user_id,
           tstamp,
           coin,
           wallet,
           currency,
           CASE
             WHEN status = 30 THEN
              price_per_coin
             ELSE
              price_per_coin_client
           END AS price_per_coin,
           CASE
             WHEN status = 30 THEN
              amount
             ELSE
              amount_client
           END AS amount,
           subtotal,
           total,
           fee,
           clearance_fee,
           payment_method,
           transaction_id,
           status,
           created_at,
           CASE
             WHEN status = 0 AND time_updated IS NOT NULL THEN
              time_updated
             ELSE
              closed_time
           END AS closed_time,
           comments,
           CASE
             WHEN status IN (1, 2, 4) THEN
              1
             WHEN status IN (0, 30) THEN
              2
             ELSE
              9
           END AS order_type
      FROM (SELECT t.user_id,
                   t.time_created AS tstamp,
                   ct.to_currency AS coin,
                   ct.wallet AS wallet,
                   ct.client_currency AS currency,
                   ct.rate_market AS price_per_coin,
                   ct.rate_market_client AS price_per_coin_client,
                   ct.quantity AS amount,
                   ct.quantity_client AS amount_client,
                   ct.original_amount - (nvl(ct.clearance_fee, 0) + ct.fee) AS subtotal,
                   ct.original_amount AS total,
                   ct.fee AS fee,
                   ct.clearance_fee AS clearance_fee,
                   t.type_id AS payment_method,
                   t.id AS transaction_id,
                   (CASE
                     WHEN t.status_id = 1 THEN
                      2 -- TRANS_STATUS_STARTED
                     WHEN t.status_id = 7 THEN
                      4 -- TRANS_STATUS_PENDING
                     WHEN ((t.status_id = 11) AND
                          ((cw.status IS NULL) OR (cw.status != 30))) THEN
                      1 --> TRANS_STATUS_IN_PROGRESS
                     WHEN cw.status = 30 THEN
                      30 -- COMPLETED
                     WHEN t.status_id = 3 THEN
                      0 -- TRANS_STATUS_CANCELED
                     WHEN t.status_id = 2 THEN
                      4
                     ELSE
                      999 --nvl(TO_CHAR(t.status_id), 'n/a') || ', ' || nvl(TO_CHAR(cw.status), 'n/a') || ' - Unknown'
                   END) AS status,
                   t.time_created AS created_at,
                   /*UNIX_TIMESTAMP(*/
                   cw.time_updated AS closed_time,
                   t.time_updated,
                   cw.comments
              FROM crypto_trade ct
              LEFT JOIN transactions t
                ON t.id = ct.transaction_id
              LEFT JOIN transaction_statuses s
                ON t.status_id = s.id
            -- LEFT JOIN transaction_types tt ON t.type_id = tt.id AND tt.class_type in (1,5,11)
              LEFT JOIN crypto_withdraw cw
                ON ct.transaction_id = cw.transaction_id
             WHERE t.user_id = i_user_id) sub_view
     ORDER BY user_id, order_type, tstamp DESC) a
     WHERE rownum BETWEEN i_results_from AND i_results_to;

END GET_ORDER_HISTORY;
END pkg_transactions;
/
