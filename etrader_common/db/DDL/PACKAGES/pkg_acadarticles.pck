create or replace
PACKAGE PKG_ACADARTICLES AS 

  PROCEDURE GET_ALL 
  (
    O_ARTICLES                  OUT SYS_REFCURSOR
    ,I_LOCALE                   IN VARCHAR2
    ,I_PAGE                     IN NUMBER
    ,I_ROWS_PER_PAGE            IN NUMBER
  );
  
  PROCEDURE ADD_ARTICLE 
  (
    I_IMAGE                     IN VARCHAR2
    ,I_ACTIVE                   IN NUMBER
    ,I_DELETED                  IN NUMBER
    ,I_HOME_PAGE_POSITION       IN NUMBER
  );

  PROCEDURE ADD_ARTICLE_TRANSLATIONS 
  (
    I_ID                        IN NUMBER  
    ,I_TITLE                    IN VARCHAR2
    ,I_SUBTITLE                 IN VARCHAR2
    ,I_AUTHOR                   IN VARCHAR2
    ,I_LOCALE                   IN VARCHAR2
    ,I_TOPICS                   IN STRING_TABLE
  );

  PROCEDURE EDIT_ARTICLE
  (
    I_ID                        IN NUMBER
    ,I_TITLE                    IN VARCHAR2
    ,I_SUBTITLE                 IN VARCHAR2
    ,I_AUTHOR                   IN VARCHAR2
    ,I_IMAGE                    IN VARCHAR2
    ,I_ACTIVE                   IN NUMBER
    ,I_DELETED                  IN NUMBER
    ,I_HOME_PAGE_POSITION       IN NUMBER
    ,I_LOCALE                   IN VARCHAR2
    ,I_WRITER_ID                IN NUMBER
    ,I_TOPICS                   IN STRING_TABLE
  );
  
  PROCEDURE DELETE_ARTICLE
  (
    I_ID                        IN NUMBER
  );
  
  PROCEDURE GET_TOPIC_SUGGESTIONS
  (
    O_SUGGESTIONS                OUT SYS_REFCURSOR
    ,I_PREFIX                    IN VARCHAR2
    ,I_LOCALE                    IN VARCHAR2
  );
  
  PROCEDURE GET_ARTICLE
  (
    O_ARTICLE                    OUT SYS_REFCURSOR
    ,I_ID                        IN NUMBER
    ,I_LOCALE                    IN VARCHAR2
  );
  
  PROCEDURE GET_TOPICS
  (
    O_TOPICS                     OUT SYS_REFCURSOR
    ,I_LOCALE                    IN VARCHAR2
  );
  
  PROCEDURE GET_HOME_PAGE_ARTICLES
  (
    O_HOME_PAGE_ARTICLES         OUT SYS_REFCURSOR
    ,I_LOCALE                    IN VARCHAR2
  );
  
  PROCEDURE SEARCH_ARTICLES
  (
    O_ARTICLES                  OUT SYS_REFCURSOR
    ,I_TOPICS                   IN NUMBER_TABLE
    ,I_LOCALE                   IN VARCHAR2
  );
  
  PROCEDURE GET_LAST_ARTICLE
  (
    O_ARTICLE                   OUT NUMBER
  );
END PKG_ACADARTICLES;
/
create or replace
PACKAGE BODY PKG_ACADARTICLES AS

  PROCEDURE CHECKTOPIC
  (
     RES              OUT NUMBER
    ,TOPIC           IN VARCHAR2
    ,I_LOCALE        IN VARCHAR2
  );

  PROCEDURE GET_ALL 
  (
    O_ARTICLES                  OUT SYS_REFCURSOR
    ,I_LOCALE                   IN VARCHAR2
    ,I_PAGE                     IN NUMBER
    ,I_ROWS_PER_PAGE            IN NUMBER
  ) IS
  BEGIN
     OPEN O_ARTICLES FOR
          SELECT * 
          FROM (
                SELECT 
                      T.*,
                      ROWNUM RN  
                FROM
                      (SELECT DISTINCT
                              A.ID,
                              A.IS_ACTIVE,
                              A.IS_DELETED,
                              A.TIME_CREATED,
                              A.TIME_UPDATED,
                              A.HOME_PAGE_POSITION,
                              AAT.TITLE,
                              AAT.SUBTITLE,
                              AAT.AUTHOR,
                              COUNT(*) OVER() TOTAL_COUNT 
                      FROM  
                              ACADEMY_ARTICLES A,
                              ACADEMY_ARTICLES_TRANSLATIONS AAT
                      WHERE 
                              A.IS_DELETED = 0
                              AND A.ID = AAT.ARTICLE_ID
                              AND (AAT.LOCALE = (SELECT LOCALE FROM ACADEMY_ARTICLES_TRANSLATIONS WHERE ARTICLE_ID = A.ID AND TITLE IS NOT NULL AND ROWNUM = 1))
                              AND AAT.TITLE IS NOT NULL
                      ORDER BY TIME_CREATED DESC
                      ) T
                WHERE
                      ROWNUM <= I_PAGE * I_ROWS_PER_PAGE
                )
          WHERE
                RN > (I_PAGE - 1) * I_ROWS_PER_PAGE;
    
  END GET_ALL;
  
  PROCEDURE ADD_ARTICLE 
  (
    I_IMAGE                    IN VARCHAR2
    ,I_ACTIVE                   IN NUMBER
    ,I_DELETED                  IN NUMBER
    ,I_HOME_PAGE_POSITION       IN NUMBER
  ) IS
  BEGIN
  
    INSERT INTO ACADEMY_ARTICLES
          (ID, IMAGE, IS_ACTIVE, IS_DELETED, HOME_PAGE_POSITION, TIME_CREATED, TIME_UPDATED)
    VALUES
          (SEQ_ACADEMY_ARTICLES.NEXTVAL, I_IMAGE, I_ACTIVE, I_DELETED, I_HOME_PAGE_POSITION, SYSDATE, SYSDATE);

  END ADD_ARTICLE;

  PROCEDURE ADD_ARTICLE_TRANSLATIONS 
  (
    I_ID                        IN NUMBER  
    ,I_TITLE                    IN VARCHAR2
    ,I_SUBTITLE                 IN VARCHAR2
    ,I_AUTHOR                   IN VARCHAR2
    ,I_LOCALE                   IN VARCHAR2
    ,I_TOPICS                   IN STRING_TABLE
  ) IS

  CURSOR C_ADDED_ARTICLE_TRANSLATION_ID IS
  SELECT * FROM (
      SELECT 
              AAT.ID 
          FROM 
              ACADEMY_ARTICLES A,
              ACADEMY_ARTICLES_TRANSLATIONS AAT
          WHERE
              A.ID = I_ID
              AND A.ID = AAT.ARTICLE_ID
          ORDER BY ID DESC)
  WHERE ROWNUM = 1;

  ARTICLE_TRANSLATION_ID NUMBER;
  RES_EXIST NUMBER;
  
  BEGIN
    IF I_TOPICS IS NOT NULL THEN
      FOR I IN 1 .. I_TOPICS.COUNT
        LOOP
            CHECKTOPIC(RES_EXIST, I_TOPICS(I), I_LOCALE);
            IF RES_EXIST = 0 THEN
              INSERT INTO ACADEMY_TOPICS_TRANSLATIONS
                    (ID, TOPIC_ID, TITLE, LOCALE)
                VALUES
                    (SEQ_ACADEMY_TOPICS_TRANSL.NEXTVAL, SEQ_ACADEMY_TOPICS.NEXTVAL, I_TOPICS(I), I_LOCALE);
            END IF;
        END LOOP;
    END IF;
    OPEN C_ADDED_ARTICLE_TRANSLATION_ID;
    FETCH C_ADDED_ARTICLE_TRANSLATION_ID INTO ARTICLE_TRANSLATION_ID;
    
    ARTICLE_TRANSLATION_ID := ARTICLE_TRANSLATION_ID + 1;
      
    INSERT INTO ACADEMY_ARTICLES_TRANSLATIONS
           (ID, ARTICLE_ID, TITLE, SUBTITLE, AUTHOR, LOCALE)
    VALUES
           (SEQ_ACADEMY_ARTICLES_TRANSL.NEXTVAL, I_ID, I_TITLE, I_SUBTITLE, I_AUTHOR, I_LOCALE); 
           
    IF I_TOPICS IS NOT NULL THEN
      FOR I IN 1 .. I_TOPICS.COUNT
        LOOP
            INSERT INTO ACADEMY_ARTICLES_TOPICS
                (ID, ARTICLE_TRANSLATION_ID, TOPIC_TRANSLATION_ID)
            VALUES
                (SEQ_ACADEMY_ARTICLES_TOPICS.NEXTVAL, ARTICLE_TRANSLATION_ID, (SELECT ID FROM ACADEMY_TOPICS_TRANSLATIONS WHERE TITLE = I_TOPICS(I) AND LOCALE = I_LOCALE));
        END LOOP;
    END IF;
  END ADD_ARTICLE_TRANSLATIONS;

  PROCEDURE EDIT_ARTICLE
  (
    I_ID                        IN NUMBER
    ,I_TITLE                    IN VARCHAR2
    ,I_SUBTITLE                 IN VARCHAR2
    ,I_AUTHOR                   IN VARCHAR2
    ,I_IMAGE                    IN VARCHAR2
    ,I_ACTIVE                   IN NUMBER
    ,I_DELETED                  IN NUMBER
    ,I_HOME_PAGE_POSITION       IN NUMBER
    ,I_LOCALE                   IN VARCHAR2
    ,I_WRITER_ID                IN NUMBER
    ,I_TOPICS                   IN STRING_TABLE
  ) IS
  
  RES_EXIST NUMBER;
  RES_ARTICLE NUMBER;
  BEGIN
    UPDATE  
							 ACADEMY_ARTICLES  
						 SET  
								 IMAGE = I_IMAGE,  
								 IS_ACTIVE = I_ACTIVE, 
								 IS_DELETED = I_DELETED, 
								 HOME_PAGE_POSITION = I_HOME_PAGE_POSITION,  
								 TIME_UPDATED = SYSDATE,
                 UPDATED_BY = I_WRITER_ID  
					    WHERE  
								 ID = I_ID;
            
      UPDATE
                 ACADEMY_ARTICLES_TRANSLATIONS
                SET
                  TITLE = I_TITLE,
                  SUBTITLE = I_SUBTITLE,
                  AUTHOR = I_AUTHOR
                WHERE
                  ARTICLE_ID = I_ID
                  AND LOCALE = I_LOCALE;    
                              
    DELETE FROM 
            ACADEMY_ARTICLES_TOPICS 
    WHERE
            ARTICLE_TRANSLATION_ID = (SELECT ID FROM ACADEMY_ARTICLES_TRANSLATIONS WHERE ARTICLE_ID = I_ID AND LOCALE = I_LOCALE);
    
    IF I_TOPICS IS NOT NULL THEN
      FOR I IN 1 .. I_TOPICS.COUNT
        LOOP
            CHECKTOPIC(RES_EXIST, I_TOPICS(I), I_LOCALE);
            IF RES_EXIST = 1 THEN
              INSERT INTO ACADEMY_ARTICLES_TOPICS
                  (ID, ARTICLE_TRANSLATION_ID, TOPIC_TRANSLATION_ID)
              VALUES
                  (
                    SEQ_ACADEMY_ARTICLES_TOPICS.NEXTVAL,
                    (SELECT ID FROM ACADEMY_ARTICLES_TRANSLATIONS WHERE ARTICLE_ID = I_ID AND LOWER(LOCALE) = LOWER(I_LOCALE)),
                    (SELECT ID FROM ACADEMY_TOPICS_TRANSLATIONS WHERE UPPER(TITLE) = UPPER(I_TOPICS(I)) AND LOWER(LOCALE) = LOWER(I_LOCALE))
                  );
            END IF;
            IF RES_EXIST = 0 AND I_TOPICS.COUNT > 0 THEN
              INSERT INTO ACADEMY_TOPICS_TRANSLATIONS
                  (ID, TOPIC_ID, TITLE, LOCALE)
              VALUES
                  (SEQ_ACADEMY_TOPICS_TRANSL.NEXTVAL, SEQ_ACADEMY_TOPICS.NEXTVAL, I_TOPICS(I), I_LOCALE);
                  
              INSERT INTO ACADEMY_ARTICLES_TOPICS
                  (ID, ARTICLE_TRANSLATION_ID, TOPIC_TRANSLATION_ID)
              VALUES
                  (
                    SEQ_ACADEMY_ARTICLES_TOPICS.NEXTVAL,
                    (SELECT ID FROM ACADEMY_ARTICLES_TRANSLATIONS WHERE ARTICLE_ID = I_ID AND LOWER(LOCALE) = LOWER(I_LOCALE)),
                    (SELECT ID FROM ACADEMY_TOPICS_TRANSLATIONS  WHERE UPPER(TITLE) = UPPER(I_TOPICS(I)) AND LOWER(LOCALE) = LOWER(I_LOCALE))
                  );
            END IF;
        END LOOP;
    END IF;
               
  END EDIT_ARTICLE;
  
  PROCEDURE DELETE_ARTICLE
  (
    I_ID                        IN NUMBER
  ) IS
  BEGIN
    UPDATE ACADEMY_ARTICLES
    SET 
        IS_DELETED = 1 
    WHERE
        ID = I_ID;
  END DELETE_ARTICLE;
  
  PROCEDURE GET_TOPIC_SUGGESTIONS
  (
    O_SUGGESTIONS                OUT SYS_REFCURSOR
    ,I_PREFIX                    IN VARCHAR2
    ,I_LOCALE                    IN VARCHAR2
  ) IS
  BEGIN
    OPEN O_SUGGESTIONS FOR
      SELECT
            ID
            ,TITLE
      FROM
            ACADEMY_TOPICS_TRANSLATIONS
      WHERE
            TITLE LIKE UPPER(I_PREFIX) || '%'
            AND LOCALE = I_LOCALE
            AND ROWNUM <= 15
      ORDER BY TITLE ASC;
  END GET_TOPIC_SUGGESTIONS;
  
  PROCEDURE GET_ARTICLE
  (
    O_ARTICLE                    OUT SYS_REFCURSOR
    ,I_ID                        IN NUMBER
    ,I_LOCALE                    IN VARCHAR2
  ) IS
    BEGIN
      OPEN O_ARTICLE FOR
                         SELECT 
                  A.ID, 
                  A.IMAGE, 
                  A.IS_ACTIVE, 
                  A.IS_DELETED, 
                  A.HOME_PAGE_POSITION, 
                  A.TIME_CREATED, 
                  A.TIME_UPDATED, 
                  AAT.TITLE,
                  AAT.SUBTITLE,
                  AAT.AUTHOR,
                  ATT.TITLE TOPIC_TITLE, 
                  AAT.LOCALE,
                  AT.TOPIC_TRANSLATION_ID
            FROM 
                  ACADEMY_ARTICLES A
            left JOIN
                  ACADEMY_ARTICLES_TRANSLATIONS AAT
            ON
                  AAT.ARTICLE_ID = A.ID
            LEFT JOIN  
                  ACADEMY_ARTICLES_TOPICS AT
            ON 
                  AT.ARTICLE_TRANSLATION_ID = AAT.ID
            LEFT JOIN 
                  ACADEMY_TOPICS_TRANSLATIONS ATT
            ON 
                  AT.TOPIC_TRANSLATION_ID = ATT.ID
            WHERE 
                  A.ID = I_ID
                  AND (I_LOCALE IS NULL OR ATT.LOCALE = I_LOCALE)
            ORDER BY ATT.LOCALE ASC;
    
    END GET_ARTICLE;
  
  PROCEDURE GET_TOPICS
  (
    O_TOPICS                     OUT SYS_REFCURSOR
    ,I_LOCALE                    IN VARCHAR2
  ) IS
  BEGIN
    OPEN O_TOPICS FOR
    SELECT * FROM (
             SELECT 
              DISTINCT ATT.* 
              ,COUNT(*) OVER (PARTITION BY AAT.TOPIC_TRANSLATION_ID) COUNTER
        FROM  
              ACADEMY_TOPICS_TRANSLATIONS ATT
        JOIN
              ACADEMY_ARTICLES_TOPICS AAT
        ON 
              ATT.ID = AAT.TOPIC_TRANSLATION_ID
        JOIN 
              ACADEMY_ARTICLES_TRANSLATIONS AT
        ON
              AT.ID = AAT.ARTICLE_TRANSLATION_ID
        JOIN
            ACADEMY_ARTICLES A
        ON
            A.ID = AT.ARTICLE_ID
        WHERE
              LOWER(ATT.LOCALE) = LOWER(I_LOCALE)
              AND A.IS_ACTIVE = 1
              AND A.IS_DELETED = 0
        ORDER BY 
              ATT.TITLE ASC)
      WHERE ROWNUM <= 40
      ORDER BY COUNTER DESC;
          
    END GET_TOPICS;
    
  PROCEDURE GET_HOME_PAGE_ARTICLES
  (
    O_HOME_PAGE_ARTICLES         OUT SYS_REFCURSOR
    ,I_LOCALE                    IN VARCHAR2
  ) IS
  BEGIN
    OPEN O_HOME_PAGE_ARTICLES FOR
      SELECT * 
      FROM (
             SELECT DISTINCT 
                        AA.ID, 
                        AA.HOME_PAGE_POSITION,
                        AA.IMAGE,
                        AA.IS_ACTIVE,
                        AA.IS_DELETED,
                        AA.TIME_CREATED,
                        AA.TIME_UPDATED,
                        AAT.TITLE,
                        AAT.SUBTITLE,
                        AAT.AUTHOR,
                        ROW_NUMBER() OVER (PARTITION BY AA.HOME_PAGE_POSITION ORDER BY TIME_UPDATED DESC) AS RN
            FROM 
                ACADEMY_ARTICLES AA
            JOIN ACADEMY_ARTICLES_TRANSLATIONS AAT
            ON AA.ID = AAT.ARTICLE_ID
            WHERE AA.ID IN(
                        SELECT MAX(ID) 
                        FROM 
                            ACADEMY_ARTICLES 
                        WHERE 
                            HOME_PAGE_POSITION > 0 
                            AND IS_ACTIVE = 1 
                            AND IS_DELETED = 0
                        GROUP BY TIME_UPDATED 
                        ) 
                  AND AAT.LOCALE =  I_LOCALE
            ORDER BY AA.TIME_UPDATED DESC
          )
    WHERE RN = 1
  ORDER BY HOME_PAGE_POSITION ASC;
    
END GET_HOME_PAGE_ARTICLES;
    
  PROCEDURE SEARCH_ARTICLES
  (
    O_ARTICLES                  OUT SYS_REFCURSOR
    ,I_TOPICS                   IN NUMBER_TABLE
    ,I_LOCALE                   IN VARCHAR2
  ) IS
  BEGIN
    OPEN O_ARTICLES FOR
      SELECT DISTINCT
            AA.*,
            AT.TITLE,
            AT.SUBTITLE,
            AT.AUTHOR
      FROM
            ACADEMY_ARTICLES AA      
      JOIN 
            ACADEMY_ARTICLES_TRANSLATIONS AT
      ON 
            AT.ARTICLE_ID = AA.ID
      JOIN 
            ACADEMY_ARTICLES_TOPICS AAT
      ON 
            AAT.ARTICLE_TRANSLATION_ID = AT.ID
      JOIN 
            ACADEMY_TOPICS_TRANSLATIONS ATT
      ON 
            ATT.ID = AAT.TOPIC_TRANSLATION_ID

      WHERE 
          AAT.TOPIC_TRANSLATION_ID IN (SELECT ID FROM ACADEMY_TOPICS_TRANSLATIONS WHERE (I_TOPICS IS NULL OR TOPIC_ID MEMBER OF I_TOPICS AND LOCALE = I_LOCALE))
          AND AA.IS_ACTIVE = 1
          AND AA.IS_DELETED = 0
          AND ATT.LOCALE = I_LOCALE
          AND AT.LOCALE = I_LOCALE; 
    END SEARCH_ARTICLES;

  PROCEDURE GET_LAST_ARTICLE
  (
    O_ARTICLE                   OUT NUMBER
  ) IS
  BEGIN
      SELECT 
            ID INTO O_ARTICLE
      FROM 
            (SELECT 
                  * 
             FROM
                  ACADEMY_ARTICLES
            ORDER BY
                  TIME_CREATED DESC
            )
     WHERE
          ROWNUM = 1;
          
    END GET_LAST_ARTICLE; 

  PROCEDURE CHECKTOPIC
  (
     RES              OUT NUMBER
    ,TOPIC            IN VARCHAR2
    ,I_LOCALE         IN VARCHAR2
  ) IS
    BEGIN 
      SELECT COUNT(*) INTO RES
      FROM ACADEMY_TOPICS_TRANSLATIONS
      WHERE UPPER(TITLE) = UPPER(TOPIC)
          AND LOWER(LOCALE) = LOWER(I_LOCALE);
          
  END CHECKTOPIC;
  
END PKG_ACADARTICLES;
/