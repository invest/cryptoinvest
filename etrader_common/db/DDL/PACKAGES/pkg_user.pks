CREATE OR REPLACE PACKAGE PKG_USER IS

  PROCEDURE GET_USER_BY_ID(O_DATA OUT SYS_REFCURSOR, I_USER_ID IN NUMBER);
  
  PROCEDURE GET_USER_BY_USERNAME(O_DATA OUT SYS_REFCURSOR, I_USERNAME IN VARCHAR2);
  
  procedure get_user_address(o_user_address OUT SYS_REFCURSOR, I_USER_ID IN NUMBER);
  
  --Remove after deploy
  PROCEDURE UPDATE_USER_DATE_OF_BIRTH
   (
    I_USER_ID IN NUMBER
   ,I_TIME_BIRTH_DATE IN DATE
   ,I_WRITER_ID IN NUMBER
   );
   --   
     
     PROCEDURE INSERT_CONTACT(I_NAME                IN CONTACTS.NAME%TYPE,
                              I_TYPE                IN CONTACTS.TYPE%TYPE,
                              I_PHONE               IN CONTACTS.PHONE%TYPE,
                              I_USER_ID             IN CONTACTS.USER_ID%TYPE,
                              I_SKIN_ID             IN CONTACTS.SKIN_ID%TYPE,
                              I_COUNTRY_ID          IN CONTACTS.COUNTRY_ID%TYPE,
                              I_EMAIL               IN CONTACTS.EMAIL%TYPE,
                              I_WRITER_ID           IN CONTACTS.WRITER_ID%TYPE,
                              I_COMBINATION_ID      IN CONTACTS.COMBINATION_ID%TYPE,
                              I_DYNAMIC_PARAMETER   IN CONTACTS.DYNAMIC_PARAMETER%TYPE,
                              I_UTC_OFFSET          IN CONTACTS.UTC_OFFSET%TYPE,
                              I_IP                  IN CONTACTS.IP%TYPE,
                              I_PHONE_TYPE          IN CONTACTS.PHONE_TYPE%TYPE,
                              I_USER_AGENT          IN CONTACTS.USER_AGENT%TYPE,
                              I_DFA_PLACEMENT_ID    IN CONTACTS.DFA_PLACEMENT_ID%TYPE,
                              I_DFA_CREATIVE_ID     IN CONTACTS.DFA_CREATIVE_ID%TYPE,
                              I_DFA_MACRO           IN CONTACTS.DFA_MACRO%TYPE,
                              I_FIRST_NAME          IN CONTACTS.FIRST_NAME%TYPE,
                              I_LAST_NAME           IN CONTACTS.LAST_NAME%TYPE,
                              I_MOBILE_PHONE        IN CONTACTS.MOBILE_PHONE%TYPE,
                              I_LAND_LINE_PHONE     IN CONTACTS.LAND_LINE_PHONE%TYPE,
                              I_IS_CONTACT_BY_EMAIL IN CONTACTS.IS_CONTACT_BY_EMAIL%TYPE,
                              I_IS_CONTACT_BY_SMS   IN CONTACTS.IS_CONTACT_BY_SMS%TYPE,
                              I_DEVICE_UNIQUE_ID    IN CONTACTS.DEVICE_UNIQUE_ID%TYPE,
                              I_HTTP_REFERER        IN CONTACTS.HTTP_REFERER%TYPE,
                              I_AFFILIATE_KEY       IN CONTACTS.AFFILIATE_KEY%TYPE,
                              I_AFF_SUB1            IN CONTACTS.AFF_SUB1%TYPE,
                              I_AFF_SUB2            IN CONTACTS.AFF_SUB2%TYPE,
                              I_AFF_SUB3            IN CONTACTS.AFF_SUB3%TYPE);

  PROCEDURE GET_USER_BY_EMAIL(O_DATA  OUT SYS_REFCURSOR,
                              I_EMAIL IN USERS.EMAIL%TYPE);
                              
  PROCEDURE UPDATE_USER_DATA
      (
        I_USER_ID IN NUMBER
        ,I_TIME_BIRTH_DATE IN DATE
        ,I_USER_FIRST_NAME IN VARCHAR2
        ,I_USER_LAST_NAME IN VARCHAR2
        ,I_WRITER_ID IN NUMBER
      );
      
       
   PROCEDURE SET_LAST_DEP_OR_INVEST_DATE
      (
        I_USER_ID IN NUMBER        
      );
      
   PROCEDURE CHECK_EXISTING_EMAILS
    (
    O_EMAILS                  OUT SYS_REFCURSOR
    ,I_EMAILS_FOR_CHECK       IN STRING_TABLE
    );
    PROCEDURE UPDATE_USER_SUSPEND
      (
        I_USER_ID IN NUMBER
       ,I_IS_SUSPEND IN NUMBER
      );
      
        PROCEDURE UPDATE_USER_PASS
      (
        I_USER_ID IN NUMBER
       ,I_PASSWORD IN VARCHAR2
      );
END PKG_USER;
/
CREATE OR REPLACE PACKAGE BODY PKG_USER IS

  PROCEDURE GET_USER_BY_ID(O_DATA OUT SYS_REFCURSOR, I_USER_ID IN NUMBER) IS
  BEGIN
    OPEN O_DATA FOR
      SELECT U.BALANCE,
             U.TAX_BALANCE,
             U.USER_NAME,
             U.PASSWORD,
             U.CURRENCY_ID,
             U.FIRST_NAME,
             U.LAST_NAME,
             U.STREET,
             U.CITY_ID,
             U.ZIP_CODE,
             U.TIME_CREATED,
             U.TIME_LAST_LOGIN,
             U.IS_ACTIVE user_is_active,
             U.EMAIL,
             U.COMMENTS,
             U.IP,
             U.TIME_BIRTH_DATE,
             U.IS_CONTACT_BY_EMAIL,
             U.MOBILE_PHONE,
             U.LAND_LINE_PHONE,
             U.GENDER,
             U.ID_NUM,
             U.LIMIT_ID,
             U.TIME_MODIFIED,
             U.CLASS_ID,
             U.STREET_NO,
             U.LAST_FAILED_TIME,
             U.FAILED_COUNT,
             U.KEYWORD,
             U.TAX_EXEMPTION,
             U.COMPANY_NAME,
             U.COMPANY_ID,
             U.REFERENCE_ID,
             U.IS_NEXT_INVEST_ON_US,
             U.IS_CONTACT_BY_SMS,
             U.ID_DOC_VERIFY,
             U.UTC_OFFSET_MODIFIED,
             U.LANGUAGE_ID,
             U.DOCS_REQUIRED,
             U.UTC_OFFSET,
             U.STATE_CODE,
             U.SKIN_ID,
             U.COUNTRY_ID,
             U.CITY_NAME,
             U.UTC_OFFSET_CREATED,
             U.IS_RISKY,
             U.COMBINATION_ID,
             U.DYNAMIC_PARAM,
             U.CONTACT_ID,
             U.IS_CONTACT_BY_PHONE,
             U.MID,
             U.IS_FALSE_ACCOUNT,
             U.IS_ACCEPTED_TERMS,
             U.TIER_USER_ID,
             U.TIME_FIRST_VISIT,
             U.DEVICE_FAMILY,
             U.WINLOSE_BALANCE,
             U.IS_DECLINED,
             U.IS_CONTACT_FOR_DAA,
             U.LAST_DECLINE_DATE,
             U.SPECIAL_CODE,
             U.AFFILIATE_KEY,
             U.SUB_AFFILIATE_KEY,
             U.CHAT_ID,
             U.WRITER_ID,
             U.PASSWORD_BACK,
             U.LIVEPERSON_SESSION_KEY,
             U.ID_NUM_BACK,
             U.CLICK_ID,
             U.IS_VIP,
             U.USER_AGENT,
             U.DEVICE_UNIQUE_ID,
             U.IS_AUTHORIZED_MAIL,
             U.AUTHORIZED_MAIL_REFUSED_TIMES,
             U.IS_NETREFER_USER,
             U.TIME_FIRST_AUTHORIZED,
             U.FILES_RISK_STATUS_ID,
             U.HTTP_REFERER,
             U.AFFILIATE_SYSTEM_USER_TIME,
             U.DFA_CREATIVE_ID,
             U.DFA_PLACEMENT_ID,
             U.DFA_MACRO,
             U.FIRST_DEPOSIT_ID,
             U.TIME_SC_TURNOVER,
             U.TIME_SC_HOUSE_RESULT,
             U.TIME_SC_MANUALY,
             U.SC_UPDATED_BY,
             U.BACKGROUND,
             U.TIP,
             U.TAG,
             U.STATUS_ID,
             U.DEP_NO_INV_24H,
             U.RANK_ID,
             U.WITHDRAW_ALERT,
             U.BONUS_ABUSER,
             U.DECODED_SOURCE_QUERY,
             U.WIN_LOSE,
             U.IS_STOP_REVERSE_WITHDRAW,
             U.IS_IMMEDIATE_WITHDRAW,
             U.AFFILIATE_SYSTEM,
             U.LAST_LOGIN_ID,
             U.PLATFORM_ID,
             U.MARKETING_ATTRIBUTION_TYPE_ID,
             U.MOBILE_PHONE_VALIDATED,
             UAD.ID,
             UAD.INV_SUM,
             UAD.INV_CNT,
             UAD.LAST_INV_ID,
             UAD.TX_SUM,
             UAD.TX_CNT,
             UAD.LAST_TX_ID,
             UAD.DEF_INV_AMOUNT,
             UAD.CUSTOM_STEP_ID,
             UAD.CUSTOM_AMOUNT,
             UAD.SUM_DEPOSITS,
             UAD.COPYOP_USER_STATUS,
             UAD.STATUS_UPDATE_DATE,
             UAD.AFF_SUB1,
             UAD.AFF_SUB2,
             UAD.AFF_SUB3,
             UAD.BUBBLES_PRICING_LEVEL,
             UAD.IS_NEED_CHANGE_PASSWORD,
             UAD.FIRST_DECLINE_AMOUNT_USD,
             UAD.HAS_BUBBLES,
             UAD.IS_DISABLE_CC_DEPOSITS,
             UAD.HAS_DYNAMICS,
             UAD.SENT_DEP_DOCUMENTS_WRITER_ID,
             UAD.SENT_DEP_DOCUMENTS_DATE_TIME,
             UAD.DEPOSITS_COUNT,
             UAD.SHOW_CANCEL_INVESTMENT,
             UAD.VIP_STATUS_ID,
             UAD.LIFETIME_VOLUME,
             S.NAME,
             S.DISPLAY_NAME,
             S.DEFAULT_COUNTRY_ID,
             S.DEFAULT_LANGUAGE_ID,
             S.DEFAULT_XOR_ID,
             S.DEFAULT_CURRENCY_ID,
             S.TICKER_ASSETS_COUNT,
             S.DEFAULT_COMBINATION_ID,
             S.DEPOSIT_DURING_CALL_DAYS,
             S.OTHER_REACTIONS_DAYS,
             S.BUSINESS_CASE_ID,
             S.TIER_QUALIFICATION_PERIOD,
             S.WORKING_DAYS,
             S.SUPPORT_START_TIME,
             S.SUPPORT_END_TIME,
             S.IS_LOAD_TICKER,
             S.IS_COUNTRY_IN_LOCALE_USE,
             S.LOCALE_SUBDOMAIN,
             S.SUBDOMAIN,
             S.IS_REGULATED,
             S.SUPPORT_EMAIL,
             S.CONTROL_COUNTING,
             S.STYLE,
             S.TEMPLATES,
             S.SKIN_GROUP_ID,
             S.MAX_COUNT_DEPOSIT,
             S.TYPE_ID,
             S.IS_ACTIVE skin_is_active,
             S.VISIBLE_TYPE_ID
        FROM USERS U
        JOIN USERS_ACTIVE_DATA UAD
          ON U.ID = UAD.USER_ID
        JOIN SKINS S
          ON U.SKIN_ID = S.ID
       WHERE U.ID = I_USER_ID;
  
  END GET_USER_BY_ID;
  
 PROCEDURE GET_USER_BY_USERNAME(O_DATA OUT SYS_REFCURSOR, I_USERNAME IN VARCHAR2) IS
  BEGIN
    OPEN O_DATA FOR
      SELECT u.id,
             u.utc_offset
        FROM USERS u
       WHERE upper(u.User_Name) = upper(I_USERNAME);
  
  END GET_USER_BY_USERNAME;
  
  procedure get_user_address(o_user_address OUT SYS_REFCURSOR, I_USER_ID IN NUMBER) IS
  BEGIN
    OPEN o_user_address FOR
    select  u.street
            ,u.street_no
            ,u.zip_code
            ,u.city_name
            ,c.country_name
    from 
            users u
            join   countries c
            on     c.id = u.country_id
    where u.id = I_USER_ID;
         
  end get_user_address;
  
  --Remove after deploy
  PROCEDURE UPDATE_USER_DATE_OF_BIRTH(I_USER_ID IN NUMBER, I_TIME_BIRTH_DATE IN DATE, I_WRITER_ID IN NUMBER) IS
    cursor c_get_dob is
    select u.time_birth_date
    from users u
    where u.id = I_USER_ID;

    l_date_of_birth_before date;    
  BEGIN
       open c_get_dob;
       fetch c_get_dob into l_date_of_birth_before;
       close c_get_dob;

       update users u 
       set u.time_birth_date = I_TIME_BIRTH_DATE
       where u.id = I_USER_ID;
       
       IF nvl(l_date_of_birth_before,trunc(sysdate)) != I_TIME_BIRTH_DATE THEN
          PKG_USER_HISTORY.CREATE_USER_DETAIL_HISTORY(    
           I_USER_ID
          ,I_WRITER_ID
          ,6
          ,l_date_of_birth_before
          ,I_TIME_BIRTH_DATE);
       END IF;
            
  END UPDATE_USER_DATE_OF_BIRTH;
  --
  
     PROCEDURE INSERT_CONTACT(I_NAME                IN CONTACTS.NAME%TYPE,
                              I_TYPE                IN CONTACTS.TYPE%TYPE,
                              I_PHONE               IN CONTACTS.PHONE%TYPE,
                              I_USER_ID             IN CONTACTS.USER_ID%TYPE,
                              I_SKIN_ID             IN CONTACTS.SKIN_ID%TYPE,
                              I_COUNTRY_ID          IN CONTACTS.COUNTRY_ID%TYPE,
                              I_EMAIL               IN CONTACTS.EMAIL%TYPE,
                              I_WRITER_ID           IN CONTACTS.WRITER_ID%TYPE,
                              I_COMBINATION_ID      IN CONTACTS.COMBINATION_ID%TYPE,
                              I_DYNAMIC_PARAMETER   IN CONTACTS.DYNAMIC_PARAMETER%TYPE,
                              I_UTC_OFFSET          IN CONTACTS.UTC_OFFSET%TYPE,
                              I_IP                  IN CONTACTS.IP%TYPE,
                              I_PHONE_TYPE          IN CONTACTS.PHONE_TYPE%TYPE,
                              I_USER_AGENT          IN CONTACTS.USER_AGENT%TYPE,
                              I_DFA_PLACEMENT_ID    IN CONTACTS.DFA_PLACEMENT_ID%TYPE,
                              I_DFA_CREATIVE_ID     IN CONTACTS.DFA_CREATIVE_ID%TYPE,
                              I_DFA_MACRO           IN CONTACTS.DFA_MACRO%TYPE,
                              I_FIRST_NAME          IN CONTACTS.FIRST_NAME%TYPE,
                              I_LAST_NAME           IN CONTACTS.LAST_NAME%TYPE,
                              I_MOBILE_PHONE        IN CONTACTS.MOBILE_PHONE%TYPE,
                              I_LAND_LINE_PHONE     IN CONTACTS.LAND_LINE_PHONE%TYPE,
                              I_IS_CONTACT_BY_EMAIL IN CONTACTS.IS_CONTACT_BY_EMAIL%TYPE,
                              I_IS_CONTACT_BY_SMS   IN CONTACTS.IS_CONTACT_BY_SMS%TYPE,
                              I_DEVICE_UNIQUE_ID    IN CONTACTS.DEVICE_UNIQUE_ID%TYPE,
                              I_HTTP_REFERER        IN CONTACTS.HTTP_REFERER%TYPE,
                              I_AFFILIATE_KEY       IN CONTACTS.AFFILIATE_KEY%TYPE,
                              I_AFF_SUB1            IN CONTACTS.AFF_SUB1%TYPE,
                              I_AFF_SUB2            IN CONTACTS.AFF_SUB2%TYPE,
                              I_AFF_SUB3            IN CONTACTS.AFF_SUB3%TYPE) IS 

BEGIN

  INSERT INTO CONTACTS
    (ID,
     NAME,
     TYPE,
     PHONE,
     USER_ID,
     SKIN_ID,
     COUNTRY_ID,
     EMAIL,
     TIME_CREATED,
     WRITER_ID,
     TIME_UPDATED,
     COMBINATION_ID,
     DYNAMIC_PARAMETER,
     UTC_OFFSET,
     IP,
     PHONE_TYPE,
     USER_AGENT,
     DFA_PLACEMENT_ID,
     DFA_CREATIVE_ID,
     DFA_MACRO,
     FIRST_NAME,
     LAST_NAME,
     MOBILE_PHONE,
     LAND_LINE_PHONE,
     IS_CONTACT_BY_EMAIL,
     IS_CONTACT_BY_SMS,
     DEVICE_UNIQUE_ID,
     HTTP_REFERER,
     AFFILIATE_KEY,
     AFF_SUB1,
     AFF_SUB2,
     AFF_SUB3)
  VALUES
    (SEQ_CONTACTS_ID.NEXTVAL,
     I_NAME,
     I_TYPE,
     I_PHONE,
     I_USER_ID,
     I_SKIN_ID,
     I_COUNTRY_ID,
     I_EMAIL,
     SYSDATE,
     I_WRITER_ID,
     SYSDATE,
     I_COMBINATION_ID,
     I_DYNAMIC_PARAMETER,
     I_UTC_OFFSET,
     I_IP,
     I_PHONE_TYPE,
     I_USER_AGENT,
     I_DFA_PLACEMENT_ID,
     I_DFA_CREATIVE_ID,
     I_DFA_MACRO,
     I_FIRST_NAME,
     I_LAST_NAME,
     I_MOBILE_PHONE,
     I_LAND_LINE_PHONE,
     I_IS_CONTACT_BY_EMAIL,
     I_IS_CONTACT_BY_SMS,
     I_DEVICE_UNIQUE_ID,
     I_HTTP_REFERER,
     I_AFFILIATE_KEY,
     I_AFF_SUB1,
     I_AFF_SUB2,
     I_AFF_SUB3);
END INSERT_CONTACT;

 PROCEDURE GET_USER_BY_EMAIL(O_DATA  OUT SYS_REFCURSOR,
                             I_EMAIL IN USERS.EMAIL%TYPE) IS
 BEGIN
   OPEN O_DATA FOR
     SELECT MAX(U.ID) AS user_id FROM USERS U WHERE lower(U.EMAIL) = lower(I_EMAIL);
 
 END GET_USER_BY_EMAIL;
 
  
  PROCEDURE UPDATE_USER_DATA(I_USER_ID IN NUMBER, I_TIME_BIRTH_DATE IN DATE, I_USER_FIRST_NAME IN VARCHAR2, I_USER_LAST_NAME IN VARCHAR2, I_WRITER_ID IN NUMBER) IS
          cursor c_get_dob is
          select u.time_birth_date
          from users u
          where u.id = I_USER_ID;
      
          cursor c_get_fname is
          select u.first_name 
          from users u
          where u.id = I_USER_ID;
          
          cursor c_get_lname is
          select u.last_name 
          from users u
          where u.id = I_USER_ID;
      
          l_date_of_birth_before date; 
          l_first_name_before varchar2(20);
          l_last_name_before varchar2(20);
        BEGIN
             open c_get_dob;
             fetch c_get_dob into l_date_of_birth_before;
             close c_get_dob;
             
             open c_get_fname;
             fetch c_get_fname into l_first_name_before;
             close c_get_fname;
             
             open c_get_lname;
             fetch c_get_lname into l_last_name_before;
             close c_get_lname;
      
             update users u 
             set u.time_birth_date = I_TIME_BIRTH_DATE
             where u.id = I_USER_ID;
             
             IF nvl(l_first_name_before,'x') != nvl(I_USER_FIRST_NAME,'x') THEN
              PKG_USER_HISTORY.CREATE_USER_DETAIL_HISTORY(    
                I_USER_ID
               ,I_WRITER_ID
               ,1
               ,l_first_name_before
               ,I_USER_FIRST_NAME);
               update users set first_name = I_USER_FIRST_NAME where id = I_USER_ID;
            END IF;
            --LN 2
            IF nvl(l_last_name_before,'x') != nvl(I_USER_LAST_NAME,'x') THEN
              PKG_USER_HISTORY.CREATE_USER_DETAIL_HISTORY(    
                I_USER_ID
               ,I_WRITER_ID
               ,2
               ,l_last_name_before
               ,I_USER_LAST_NAME);
               update users set last_name = I_USER_LAST_NAME where id = I_USER_ID;
            END IF;
            --Birthdate 6
            IF nvl(l_date_of_birth_before,trunc(sysdate)) != nvl(I_TIME_BIRTH_DATE,trunc(sysdate)) THEN
            PKG_USER_HISTORY.CREATE_USER_DETAIL_HISTORY(    
              I_USER_ID
             ,I_WRITER_ID
             ,6
             ,l_date_of_birth_before
             ,I_TIME_BIRTH_DATE);
             update users set time_birth_date = I_TIME_BIRTH_DATE where id = I_USER_ID;
          END IF;   
              
 END UPDATE_USER_DATA;

 PROCEDURE SET_LAST_DEP_OR_INVEST_DATE
   (
      I_USER_ID IN NUMBER
   ) IS
   l_test number;
        BEGIN
          update users_active_data ua
          set ua.last_deposit_or_invest_date = sysdate
          where user_id = I_USER_ID;
        END;


 PROCEDURE CHECK_EXISTING_EMAILS
  (
   O_EMAILS            OUT SYS_REFCURSOR
  ,I_EMAILS_FOR_CHECK   IN STRING_TABLE
  ) is 
  BEGIN
    open o_emails for
    select * 
    from users 
    where upper(email) member of i_emails_for_check
    and (is_active = 1 or (is_active = 0 and failed_count >= 4));  
  END CHECK_EXISTING_EMAILS;
  
  PROCEDURE UPDATE_USER_SUSPEND
      (
        I_USER_ID IN NUMBER
       ,I_IS_SUSPEND IN NUMBER
      ) IS
  BEGIN
       update users_active_data uad 
       set uad.is_non_reg_suspend = I_IS_SUSPEND
       where uad.user_id = I_USER_ID;            
  END UPDATE_USER_SUSPEND;
  
  PROCEDURE UPDATE_USER_PASS
      (
        I_USER_ID IN NUMBER
       ,I_PASSWORD IN VARCHAR2
      ) IS
  BEGIN
       update users 
       set password = I_PASSWORD
       where id = I_USER_ID;            
  END UPDATE_USER_PASS;
 END PKG_USER;
/
