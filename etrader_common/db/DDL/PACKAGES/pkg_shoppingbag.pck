create or replace package pkg_shoppingbag is

  -- Author  : VICTOR.SLAVOV
  -- Created : 2016-07-25 11:58:10
  -- Purpose : used in ShoppingBagDAO

  procedure get_users_opportunities
  (
    o_usersopps        out sys_refcursor
   ,i_users            in number_table
   ,i_time_est_closing in date
  );

end pkg_shoppingbag;
/
create or replace package body pkg_shoppingbag is

  procedure get_users_opportunities
  (
    o_usersopps        out sys_refcursor
   ,i_users            in number_table
   ,i_time_est_closing in date
  ) is
  begin
    open o_usersopps for
      with op as
       (select id, time_est_closing, time_created from opportunities where time_est_closing = cast(i_time_est_closing as timestamp))
      select distinct i.user_id, i.opportunity_id as opp_id
      from   investments i
      join   op
      on     op.id = i.opportunity_id
      where  i.is_canceled = 0
      and    i.time_created >= (select cast(min(time_created) as date) from op)
      and    i.user_id in (select /*+ cardinality(t,1) */
                            column_value
                           from   table(i_users) t)
      and    (i.time_settled is null or i.time_settled >= sys_extract_utc(op.time_est_closing));
  
  end get_users_opportunities;

end pkg_shoppingbag;
/
