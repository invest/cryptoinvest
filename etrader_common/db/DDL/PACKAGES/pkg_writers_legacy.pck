create or replace package pkg_writers_legacy is

  -- Author  : VICTOR.SLAVOV
  -- Created : 2016-05-19 16:01:31
  -- Purpose : 

  procedure get_retention_writers
  (
    o_writers   out sys_refcursor
   ,i_writer_id in number
  );

end pkg_writers_legacy;
/
create or replace package body pkg_writers_legacy is

  procedure get_retention_writers
  (
    o_writers   out sys_refcursor
   ,i_writer_id in number
  ) is
  begin
    open o_writers for
      select w.id, w.user_name
      from   writers w
      join   departments d
      on     d.id = w.dept_id
      where  w.is_active = 1
      and    exists (select 1
              from   roles r
              where  r.user_name = w.user_name
              and    r.role in ('retention', 'retentionM'))
      and    d.dept_name = 'Retention'
      and    exists (select 1 from writers_skin where skin_id in (select skin_id from writers_skin where writer_id = i_writer_id))
      order  by w.user_name;
  end get_retention_writers;

end pkg_writers_legacy;
/
