CREATE OR REPLACE PACKAGE PKG_USER_HISTORY IS

    PROCEDURE CREATE_USER_DETAIL_HISTORY(
    I_USER_ID IN NUMBER
   ,I_WRITER_ID IN NUMBER
   ,I_HISTORY_TYPE_ID IN NUMBER
   ,I_FIELD_BEFORE IN VARCHAR2
   ,I_FIELD_AFTER IN VARCHAR2);

END PKG_USER_HISTORY;
/


CREATE OR REPLACE PACKAGE BODY PKG_USER_HISTORY IS

  PROCEDURE CREATE_USER_DETAIL_HISTORY(
    I_USER_ID IN NUMBER
   ,I_WRITER_ID IN NUMBER
   ,I_HISTORY_TYPE_ID IN NUMBER
   ,I_FIELD_BEFORE IN VARCHAR2
   ,I_FIELD_AFTER IN VARCHAR2) IS
   
   CURSOR C_GET_USER_DATA IS
   SELECT U.USER_NAME, U.CLASS_ID 
   FROM USERS U
   WHERE U.ID = I_USER_ID;
   
  v_user_name varchar2(100);
  v_class_id number;
  
  v_users_details_history_id number;
   
  BEGIN
    open C_GET_USER_DATA;
    fetch C_GET_USER_DATA into v_user_name, v_class_id;
    close C_GET_USER_DATA;
    
    v_users_details_history_id := seq_users_details_history.nextval;
    insert into users_details_history
      (id, time_created, writer_id, user_id, user_name, class_id)
    values
      (v_users_details_history_id, sysdate, I_WRITER_ID, I_USER_ID, v_user_name, v_class_id);
      
      insert into users_details_history_changes
        (id, users_details_history_id, users_details_history_type_id, field_before, field_after)
      values
        (seq_users_details_his_changes.nextval, v_users_details_history_id, I_HISTORY_TYPE_ID, I_FIELD_BEFORE, I_FIELD_AFTER);
      
    
    
  END CREATE_USER_DETAIL_HISTORY;

END PKG_USER_HISTORY;
/
