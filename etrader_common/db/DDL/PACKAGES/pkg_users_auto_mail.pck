CREATE OR REPLACE PACKAGE PKG_USERS_AUTO_MAIL IS
 PROCEDURE INSERT_USER_AUTO_MAIL
     (
      I_USER_ID             IN NUMBER,
      I_REASON_ID           IN NUMBER
     );
     
 PROCEDURE GET_USERS_AUTO_MAIL
    (
      O_DATA            OUT SYS_REFCURSOR,
      I_REASON_ID       IN NUMBER
    );

 PROCEDURE UPDATE_USER_AUTO_MAIL_SENT
    (
     I_MAIL_ID       IN NUMBER_TABLE
    );

 PROCEDURE UPDATE_USER_AUTO_MAIL_ERR
    (
     I_MAIL_ID       IN NUMBER_TABLE
    );
    
 PROCEDURE NOTIF_REACHED_TOTAL_DEPOSIT
     (
      I_USER_ID             IN NUMBER,
      I_RATE_USD_EUR        IN NUMBER
     ); 

 PROCEDURE GET_USERS_NOTIFE_TTL_DEPOSITS
  (
    O_DATA            OUT SYS_REFCURSOR   
  );                
     
END PKG_USERS_AUTO_MAIL;
/

CREATE OR REPLACE PACKAGE BODY PKG_USERS_AUTO_MAIL IS

 PROCEDURE INSERT_USER_AUTO_MAIL
     (
      I_USER_ID             IN NUMBER,
      I_REASON_ID           IN NUMBER
      ) IS

BEGIN

 insert into users_auto_mail
   (id
   ,user_id
   ,reason_id)
 values
   (SEQ_USERS_AUTO_MAIL.NEXTVAL
   ,I_USER_ID
   ,I_REASON_ID);
END;

 PROCEDURE GET_USERS_AUTO_MAIL
  (
    O_DATA            OUT SYS_REFCURSOR,
    I_REASON_ID       IN NUMBER
  ) IS

  BEGIN
  --
    OPEN O_DATA FOR
     SELECT 
      uam.id
     ,uam.user_id
    FROM 
      users_auto_mail uam
   WHERE
      uam.is_send = 0
      and uam.reason_id = I_REASON_ID;
  END GET_USERS_AUTO_MAIL;
  

 PROCEDURE UPDATE_USER_AUTO_MAIL_SENT
      (
      I_MAIL_ID       IN NUMBER_TABLE
      ) IS

BEGIN
  update users_auto_mail m
   set 
    m.is_send = 1
   ,m.time_send = sysdate
  where 
    m.id member of I_MAIL_ID;     
END;  

 PROCEDURE UPDATE_USER_AUTO_MAIL_ERR
      (
      I_MAIL_ID       IN NUMBER_TABLE
      ) IS

BEGIN
  update users_auto_mail m
   set 
    m.is_send = 2
   ,m.time_error = sysdate
  where 
    m.id member of I_MAIL_ID;     
END;  

 PROCEDURE NOTIF_REACHED_TOTAL_DEPOSIT
     (
      I_USER_ID             IN NUMBER,
      I_RATE_USD_EUR        IN NUMBER
     ) IS
      
  cursor c_check_notife is
  select ua.user_id, ua.sum_deposits/100
  from 
        users_active_data ua,
        users u
  where ua.user_id = u.id
    and ua.sum_deposits*I_RATE_USD_EUR > 199500
    and ua.user_id not in (select user_id from users_auto_mail m where m.reason_id = 2)
    and ua.user_id = I_USER_ID;  

  v_user_id number;
  v_sum_dep number;
BEGIN
  open c_check_notife;
  fetch c_check_notife into v_user_id, v_sum_dep;
  if c_check_notife%found then
    insert into users_auto_mail
     (id
     ,user_id
     ,reason_id
     ,comments)
    values
     (SEQ_USERS_AUTO_MAIL.NEXTVAL
     ,I_USER_ID
     ,2
     ,'With total deposits in '|| v_sum_dep||' USD');
  end if;
close c_check_notife;   
END;

 PROCEDURE GET_USERS_NOTIFE_TTL_DEPOSITS
  (
    O_DATA            OUT SYS_REFCURSOR   
  ) IS

  BEGIN
  --
    OPEN O_DATA FOR
     SELECT 
      uam.id
     ,uam.user_id
     ,ur.questionnaire_done_date
     ,s.name skin_name
    FROM 
      users_auto_mail uam,
      users_regulation ur,
      users u,
      skins s
   WHERE
      uam.is_send = 0
      and uam.reason_id = 2
      and u.id = uam.user_id
      and u.skin_id = s.id
      and u.class_id = 1
      and uam.user_id = ur.user_id(+);
  END GET_USERS_NOTIFE_TTL_DEPOSITS;
   
END PKG_USERS_AUTO_MAIL;
/