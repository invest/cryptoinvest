CREATE OR REPLACE PACKAGE PKG_REROUTING IS

  PROCEDURE GET_REROUTING_PROVIDERS
    (
     O_DATA                      OUT SYS_REFCURSOR
    ,I_TRANSACTION_ID            IN NUMBER
    );
    
  PROCEDURE GET_PROVIDERS_CLASSES
  (
    O_DATA            OUT SYS_REFCURSOR
  );
  
  PROCEDURE GET_EXCEPT_ERR_CODES
  (
    O_DATA            OUT SYS_REFCURSOR
  );
  
  PROCEDURE GET_EXCEPT_ERR_COMMENTS
  (
    O_DATA            OUT SYS_REFCURSOR
  );
END PKG_REROUTING;
/

CREATE OR REPLACE PACKAGE BODY PKG_REROUTING IS
PROCEDURE GET_REROUTING_PROVIDERS
  (
    O_DATA            OUT SYS_REFCURSOR
   ,I_TRANSACTION_ID  IN NUMBER
  ) IS
  
  cursor c_dont_reroute is
   select 'x'
   from  
       transactions t,
       credit_cards cc,
       users u,
       skins s
   where 
       t.id = I_TRANSACTION_ID 
       and t.credit_card_id =  cc.id
       and t.user_id = u.id
       and u.skin_id = s.id
       --STOP rerute for TEST CC or Maestro CC or GoldBeam
       and (t.clearing_provider_id = 25 or cc.type_id=6 or s.business_case_id = 5); 

   v_tmp varchar2(1);

  BEGIN
  --check if can't reroute
  open c_dont_reroute;
  fetch c_dont_reroute into v_tmp;
   if c_dont_reroute%notfound then
          OPEN O_DATA FOR
     SELECT * FROM REROUTING_PROVIDERS re
      WHERE re.order_num IN   
          --ALL FIRST CLEARING_PROVIDERS FOR EACH CLASSES
        (SELECT min(rp.order_num) 
        FROM 
         REROUTING_PROVIDERS rp
        WHERE 
         RP.IS_ACTIVE = 1
      --limits
         AND RP.CLEARING_PROVIDER_ID NOT IN
           (select lmts.clearing_provider_id 
            from 
             transactions t,
             users u,
             skins s,
             (select rt.clearing_provider_id,
                     case when rt.rerouting_limits_type_id = 1 then (select c.id from countries c where c.id = rt.rerouting_limits_value)
                    else null end as country_id,
                     case when rt.rerouting_limits_type_id = 2 then (select cr.id from currencies cr where cr.id = rt.rerouting_limits_value)
                   else null end as currency_id,
                    case when rt.rerouting_limits_type_id = 3 then (select p.id from platforms p where p.id = rt.rerouting_limits_value)
                  else null end as platform_id,
                    case when rt.rerouting_limits_type_id = 4 then (select bc.id from skin_business_cases bc where bc.id = rt.rerouting_limits_value)
                  else null end as business_case_id         
             from 
              rerouting_limits rt
             ) lmts
            where 
           t.user_id = u.id
           and u.skin_id = s.id
           and
            ( u.country_id = lmts.country_id
              or u.currency_id = lmts.currency_id
              or (t.writer_id in (10000,20000) and lmts.platform_id = 3 )
              or u.currency_id = lmts.currency_id
              or s.business_case_id = lmts.business_case_id
            )
           and t.id = I_TRANSACTION_ID
           )
         --exclude org. attemp  provider classes
         AND RP.CLASS_TYPE_ID NOT IN
          (select rr.class_type_id from 
                  rerouting_providers rr,
                  transactions tre
            where 
                  rr.clearing_provider_id = tre.clearing_provider_id
                  and tre.id = I_TRANSACTION_ID
          )           
        GROUP BY rp.class_type_id)
      ORDER BY re.order_num;
   ELSE 
     -- return empty 
      OPEN O_DATA FOR
        select 'x' from dual where 1=2;
   end if;
  close c_dont_reroute;
  --
  END GET_REROUTING_PROVIDERS;
  
  PROCEDURE GET_PROVIDERS_CLASSES
  (
    O_DATA            OUT SYS_REFCURSOR
  ) IS

  BEGIN
  --
    OPEN O_DATA FOR
     SELECT 
      re.clearing_provider_id
     ,re.class_type_id 
    FROM 
      REROUTING_PROVIDERS re;
  END GET_PROVIDERS_CLASSES;
  
  PROCEDURE GET_EXCEPT_ERR_CODES
  (
    O_DATA            OUT SYS_REFCURSOR
  ) IS

  BEGIN
  --
    OPEN O_DATA FOR
     SELECT 
       a.provider_classes_id
      ,a.error_code
    FROM 
      REROUTING_EXCEPT_ERR_CODES a;
  END GET_EXCEPT_ERR_CODES;
  
  PROCEDURE GET_EXCEPT_ERR_COMMENTS
  (
    O_DATA            OUT SYS_REFCURSOR
  ) IS

  BEGIN
  --
    OPEN O_DATA FOR
     SELECT 
       a.error_comments
    FROM 
      REROUTING_EXCEPT_ERR_COMMENTS a;
  END GET_EXCEPT_ERR_COMMENTS;  
  
END PKG_REROUTING;
/