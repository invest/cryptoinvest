create or replace package pkg_withdraw_backend is

  -- Author  : VICTOR.SLAVOV
  -- Created : 2016-07-21 14:29:12
  -- Purpose : create withdraw transactions from backend; il.co.etrader.backend.bl_managers.TransactionsManager

  -- create withdraw for german payment methods
  procedure create_withdraw
  (
    i_user_id          in number
   ,i_amount           in number
   ,i_writer_id        in number
   ,i_withdraw_type_id in number
   ,i_comments         in varchar2
   ,i_rate             in number
  );
  
  PROCEDURE CREATE_GM_WITHDRAW(I_USER_ID          IN NUMBER,
                               I_AMOUNT           IN NUMBER,
                               I_WRITER_ID        IN NUMBER,
                               I_WITHDRAW_TYPE_ID IN NUMBER, --gmType
                               I_RATE             IN NUMBER,
                               I_BENEFICIARY_NAME IN VARCHAR2,
                               I_SWIFT_BIC_CODE   IN VARCHAR2,
                               I_IBAN             IN VARCHAR2,
                               I_ACCOUNT_NUMBER   IN NUMBER,
                               I_BANKNAME         IN VARCHAR2,
                               I_BRANCH_NUMBER    IN NUMBER,
                               I_BRANCH_ADDRESS   IN VARCHAR2,
                               I_ACCOUNT_INFO     IN VARCHAR2,
                               I_TRN_STATUS_ID    IN NUMBER,
                               I_TRN_FAILED_DESC  IN VARCHAR2,
                               i_qm_answer_id     IN NUMBER,
                               i_qm_text_answer   IN VARCHAR2,
                               i_IP               IN VARCHAR2,
                               i_fee_cancel       IN NUMBER);
                               
procedure create_withdraw_user_details
  (
    o_id               out withdraw_user_details.id%type
   ,i_user_id          in number
   ,i_beneficiary_name in varchar2
   ,i_swift            in varchar2
   ,i_iban             in varchar2
   ,i_account_num      in number
   ,i_bank_name        in varchar2
   ,i_branch_number    in number
   ,i_branch_address   in varchar2
   ,i_file_id          in number
   ,i_skrill_email     in varchar2
   ,i_crteated_writer_id in number
   
  );
procedure update_wthdr_wire_user_details
  (
    i_user_id          in number
   ,i_beneficiary_name in varchar2
   ,i_swift            in varchar2
   ,i_iban             in varchar2
   ,i_account_num      in number
   ,i_bank_name        in varchar2
   ,i_branch_number    in number
   ,i_branch_address   in varchar2
   ,i_file_id          in number 
   ,i_writer_id        in number  
  );                               

procedure update_wthdr_skrl_user_details
  (
    i_user_id          in number
   ,i_skrill_email in varchar2
   ,i_writer_id        in number
 );
 
 procedure update_wthdr_file_id
  (
    i_user_id          in number
   ,i_file_id          in number   
   ,i_writer_id        in number
 );
 
procedure get_withdraw_user_details
  (
    o_withdraw_user_details   out sys_refcursor
   ,i_user_id  in number
  );
  
procedure skiped_wthdr_bank_user_details
  (
    i_user_id      in number
   ,i_issue_action_id in number
  ); 
  
procedure show_wthdr_details_popup
  (
    o_show_popup      out number
   ,i_user_id in number
  );  

end pkg_withdraw_backend;
/
create or replace package body pkg_withdraw_backend is

  procedure create_withdraw
  (
    i_user_id          in number
   ,i_amount           in number
   ,i_writer_id        in number
   ,i_withdraw_type_id in number
   ,i_comments         in varchar2
   ,i_rate             in number
  ) is
    l_trn              transactions%rowtype;
    l_sysdate          date := sysdate;
    l_user_utc_offset  users.utc_offset%type;
    l_user_balance     number;
    l_new_user_balance number;
    l_tax_balance      number;
    l_command          number:= 4;
    
    DIRECT24_WITHDRAW constant number := 59;
    GIROPAY_WITHDRAW  constant number := 60;
    EPS_WITHDRAW      constant number := 61;
    
  begin
    select u.utc_offset, u.balance, u.tax_balance
    into   l_user_utc_offset, l_user_balance, l_tax_balance
    from   users u
    where  u.id = i_user_id
    for    update;
  
    if l_user_balance < i_amount
    then
      raise_application_error(-20000, 'User balance is less then the withraw amount: ' || l_user_balance || ' < ' || i_amount);
    end if;
  
    update users u
    set    balance = balance - i_amount, u.time_modified = l_sysdate, u.utc_offset_modified = u.utc_offset
    where  u.id = i_user_id
    returning balance into l_new_user_balance;
  
    l_trn.user_id                := i_user_id;
    l_trn.type_id                := i_withdraw_type_id;
    l_trn.amount                 := i_amount;
    l_trn.status_id              := 2; -- select id from transaction_statuses where code = 'TRANS_STATUS_SUCCEED'
    l_trn.writer_id              := i_writer_id;
    l_trn.time_settled           := l_sysdate;
    l_trn.comments               := i_comments;
    l_trn.processed_writer_id    := i_writer_id;
    l_trn.time_created           := l_sysdate;
    l_trn.utc_offset_created     := l_user_utc_offset;
    l_trn.utc_offset_settled     := l_user_utc_offset;
    l_trn.is_accounting_approved := 0;
    l_trn.rate                   := i_rate;
    l_trn.ip                     := 'IP NOT FOUND!';
  
    insert into transactions t
      (id
      ,user_id
      ,type_id
      ,amount
      ,status_id
      ,writer_id
      ,time_settled
      ,comments
      ,processed_writer_id
      ,time_created
      ,utc_offset_created
      ,utc_offset_settled
      ,is_accounting_approved
      ,rate
      ,ip)
    values
      (seq_transactions.nextval
      ,l_trn.user_id
      ,l_trn.type_id
      ,l_trn.amount
      ,l_trn.status_id
      ,l_trn.writer_id
      ,l_trn.time_settled
      ,l_trn.comments
      ,l_trn.processed_writer_id
      ,l_trn.time_created
      ,l_trn.utc_offset_created
      ,l_trn.utc_offset_settled
      ,l_trn.is_accounting_approved
      ,l_trn.rate
      ,l_trn.ip) return id into l_trn.id;
  
     if l_trn.type_id = DIRECT24_WITHDRAW then
        l_command := 55;
     elsif l_trn.type_id = GIROPAY_WITHDRAW then  
        l_command := 54;  
     elsif l_trn.type_id = EPS_WITHDRAW then
        l_command := 53; 
     end if;
               
    insert into balance_history
      (id, time_created, user_id, balance, tax_balance, writer_id, table_name, key_value, command, utc_offset)
    values
      (seq_balance_history.nextval
      ,l_sysdate
      ,i_user_id
      ,l_new_user_balance
      ,l_tax_balance
      ,i_writer_id
      ,'transactions'
      ,l_trn.id
      ,l_command
      ,l_user_utc_offset);
  end create_withdraw;
  
PROCEDURE CREATE_GM_WITHDRAW(I_USER_ID          IN NUMBER,
                               I_AMOUNT           IN NUMBER,
                               I_WRITER_ID        IN NUMBER,
                               I_WITHDRAW_TYPE_ID IN NUMBER, --gmType
                               I_RATE             IN NUMBER,
                               I_BENEFICIARY_NAME IN VARCHAR2,
                               I_SWIFT_BIC_CODE   IN VARCHAR2,
                               I_IBAN             IN VARCHAR2,
                               I_ACCOUNT_NUMBER   IN NUMBER,
                               I_BANKNAME         IN VARCHAR2,
                               I_BRANCH_NUMBER    IN NUMBER,
                               I_BRANCH_ADDRESS   IN VARCHAR2,
                               I_ACCOUNT_INFO     IN VARCHAR2,
                               I_TRN_STATUS_ID    IN NUMBER,
                               I_TRN_FAILED_DESC  IN VARCHAR2,
                               i_qm_answer_id     IN NUMBER,
                               i_qm_text_answer   IN VARCHAR2,
                               i_IP               IN VARCHAR2,
                               i_fee_cancel       IN NUMBER) IS
  
    L_TRN              TRANSACTIONS%ROWTYPE;
    L_WIRES            WIRES%ROWTYPE;
    L_SYSDATE          DATE := SYSDATE;
    L_USER_UTC_OFFSET  USERS.UTC_OFFSET%TYPE;
    L_USER_BALANCE     NUMBER;
    L_NEW_USER_BALANCE NUMBER;
    L_TAX_BALANCE      NUMBER;
    L_COMMAND          NUMBER:= 4;
  
    DIRECT24_WITHDRAW CONSTANT NUMBER := 59;
    GIROPAY_WITHDRAW  CONSTANT NUMBER := 60;
    EPS_WITHDRAW      CONSTANT NUMBER := 61;
    IDEAL_WITHDRAW    CONSTANT NUMBER := 63;
    
  BEGIN
  
    SELECT U.UTC_OFFSET, U.BALANCE, U.TAX_BALANCE
      INTO L_USER_UTC_OFFSET, L_USER_BALANCE, L_TAX_BALANCE
      FROM USERS U
     WHERE U.ID = I_USER_ID;
       
   IF I_TRN_STATUS_ID = 4
     THEN
    IF L_USER_BALANCE < I_AMOUNT THEN
      RAISE_APPLICATION_ERROR(-20000, 'User balance is less then the withraw amount: ' || L_USER_BALANCE || ' < ' || I_AMOUNT);
    END IF;
  
    UPDATE USERS U
       SET BALANCE               = BALANCE - I_AMOUNT,
           U.TIME_MODIFIED       = L_SYSDATE,
           U.UTC_OFFSET_MODIFIED = U.UTC_OFFSET
     WHERE U.ID = I_USER_ID
    RETURNING BALANCE INTO L_NEW_USER_BALANCE;
    END IF;
  
    L_WIRES.BRANCH           := I_BRANCH_NUMBER;
    L_WIRES.ACCOUNT_NUM      := I_ACCOUNT_NUMBER;
    L_WIRES.SWIFT            := I_SWIFT_BIC_CODE;
    L_WIRES.BENEFICIARY_NAME := I_BENEFICIARY_NAME;
    L_WIRES.BANK_NAME        := I_BANKNAME;
    L_WIRES.IBAN             := I_IBAN;
    L_WIRES.BRANCH_ADDRESS   := I_BRANCH_ADDRESS;
    L_WIRES.ACCOUNT_INFO     := I_ACCOUNT_INFO;
  
    INSERT INTO WIRES
      (ID,
       --BANK_ID,?
       BRANCH,
       ACCOUNT_NUM,
       SWIFT,
       BENEFICIARY_NAME,
       BANK_NAME,
       IBAN,
       BRANCH_ADDRESS,
       ACCOUNT_INFO)
    VALUES
      (SEQ_WIRES.NEXTVAL,
       --V_BANK_ID, ?
       L_WIRES.BRANCH,
       L_WIRES.ACCOUNT_NUM,
       L_WIRES.SWIFT,
       L_WIRES.BENEFICIARY_NAME,
       L_WIRES.BANK_NAME,
       L_WIRES.IBAN,
       L_WIRES.BRANCH_ADDRESS,
       L_WIRES.ACCOUNT_INFO) RETURN ID INTO L_WIRES.ID;
  
    L_TRN.USER_ID                := I_USER_ID;
    L_TRN.TYPE_ID                := I_WITHDRAW_TYPE_ID;
    L_TRN.AMOUNT                 := I_AMOUNT;
    L_TRN.STATUS_ID              := I_TRN_STATUS_ID;
    L_TRN.WRITER_ID              := I_WRITER_ID;
    L_TRN.TIME_SETTLED           := L_SYSDATE;
    L_TRN.PROCESSED_WRITER_ID    := I_WRITER_ID;
    L_TRN.TIME_CREATED           := L_SYSDATE;
    L_TRN.UTC_OFFSET_CREATED     := L_USER_UTC_OFFSET;
    L_TRN.UTC_OFFSET_SETTLED     := L_USER_UTC_OFFSET;
    L_TRN.IS_ACCOUNTING_APPROVED := 0;
    L_TRN.RATE                   := I_RATE;
    L_TRN.IP                     := I_IP;
    L_TRN.WIRE_ID                := L_WIRES.ID;
    L_TRN.DESCRIPTION            := I_TRN_FAILED_DESC;
    L_TRN.FEE_CANCEL             := i_fee_cancel;
    L_TRN.FEE_CANCEL_BY_ADMIN    := i_fee_cancel;
    L_TRN.UPDATE_BY_ADMIN        := i_fee_cancel;
  
    INSERT INTO TRANSACTIONS T
      (ID,
       USER_ID,
       TYPE_ID,
       AMOUNT,
       STATUS_ID,
       WRITER_ID,
       TIME_SETTLED,
       PROCESSED_WRITER_ID,
       TIME_CREATED,
       UTC_OFFSET_CREATED,
       UTC_OFFSET_SETTLED,
       IS_ACCOUNTING_APPROVED,
       RATE,
       IP,
       WIRE_ID,
       DESCRIPTION,
       FEE_CANCEL,
       FEE_CANCEL_BY_ADMIN,
       UPDATE_BY_ADMIN)
    VALUES
      (SEQ_TRANSACTIONS.NEXTVAL,
       L_TRN.USER_ID,
       L_TRN.TYPE_ID,
       L_TRN.AMOUNT,
       L_TRN.STATUS_ID,
       L_TRN.WRITER_ID,
       L_TRN.TIME_SETTLED,
       L_TRN.PROCESSED_WRITER_ID,
       L_TRN.TIME_CREATED,
       L_TRN.UTC_OFFSET_CREATED,
       L_TRN.UTC_OFFSET_SETTLED,
       L_TRN.IS_ACCOUNTING_APPROVED,
       L_TRN.RATE,
       L_TRN.IP,
       L_TRN.WIRE_ID,
       L_TRN.DESCRIPTION,
       L_TRN.FEE_CANCEL,
       L_TRN.FEE_CANCEL_BY_ADMIN,
       L_TRN.UPDATE_BY_ADMIN) RETURN ID INTO L_TRN.ID;
  
     IF L_TRN.TYPE_ID = DIRECT24_WITHDRAW THEN
        L_COMMAND := 55;
     ELSIF L_TRN.TYPE_ID = GIROPAY_WITHDRAW THEN  
        L_COMMAND := 54;  
     ELSIF L_TRN.TYPE_ID = EPS_WITHDRAW THEN
        L_COMMAND := 53; 
     ELSIF L_TRN.TYPE_ID = IDEAL_WITHDRAW THEN
        L_COMMAND := 56; 
     END IF;
  
   IF I_TRN_STATUS_ID = 4
     THEN
    INSERT INTO BALANCE_HISTORY
      (ID, TIME_CREATED, USER_ID, BALANCE, TAX_BALANCE, WRITER_ID, TABLE_NAME, KEY_VALUE, COMMAND, UTC_OFFSET)
    VALUES
      (SEQ_BALANCE_HISTORY.NEXTVAL, L_SYSDATE, I_USER_ID, L_NEW_USER_BALANCE, L_TAX_BALANCE, I_WRITER_ID, 'transactions', L_TRN.ID, L_COMMAND, L_USER_UTC_OFFSET);
    END IF;
       
   IF i_qm_answer_id IS NOT NULL
     THEN 
       INSERT INTO qm_user_answers
        (qm_answer_id, user_id, q_id, answer_id, q_group_id, text_answer, writer_id, transaction_id)
       VALUES
        (SEQ_QM_USR_ANS.NEXTVAL, I_USER_ID, 94, i_qm_answer_id, 9, i_qm_text_answer, I_WRITER_ID, L_TRN.ID);
    END IF;

  END CREATE_GM_WITHDRAW;

procedure create_withdraw_user_details
  (
    o_id               out withdraw_user_details.id%type
   ,i_user_id          in number
   ,i_beneficiary_name in varchar2
   ,i_swift            in varchar2
   ,i_iban             in varchar2
   ,i_account_num      in number
   ,i_bank_name        in varchar2
   ,i_branch_number    in number
   ,i_branch_address   in varchar2
   ,i_file_id          in number
   ,i_skrill_email     in varchar2
   ,i_crteated_writer_id in number
   
  ) is
  begin
  insert into withdraw_user_details
  (id
  ,user_id
  ,beneficiary_name
  ,swift
  ,iban
  ,account_num
  ,bank_name
  ,branch_number
  ,branch_address
  ,file_id
  ,skrill_email
  ,created_writer_id)
values
  (SEQ_withdraw_user_details.Nextval
  ,i_user_id
  ,i_beneficiary_name
  ,i_swift
  ,i_iban
  ,i_account_num
  ,i_bank_name
  ,i_branch_number
  ,i_branch_address
  ,i_file_id
  ,i_skrill_email
  ,i_crteated_writer_id) returning id into o_id;
end create_withdraw_user_details; 

procedure update_wthdr_wire_user_details
  (
    i_user_id          in number
   ,i_beneficiary_name in varchar2
   ,i_swift            in varchar2
   ,i_iban             in varchar2
   ,i_account_num      in number
   ,i_bank_name        in varchar2
   ,i_branch_number    in number
   ,i_branch_address   in varchar2
   ,i_file_id          in number   
   ,i_writer_id        in number   
  ) is
  begin
update withdraw_user_details
   set beneficiary_name = i_beneficiary_name,
       swift = i_swift,
       iban = i_iban,
       account_num = i_account_num,
       bank_name = i_bank_name,
       branch_number = i_branch_number,
       branch_address = i_branch_address,
       --file_id = i_file_id,
       updated_writer_id = i_writer_id
 where user_id = i_user_id;
end update_wthdr_wire_user_details;

procedure update_wthdr_skrl_user_details
  (
    i_user_id          in number
   ,i_skrill_email     in varchar2
   ,i_writer_id        in number
  ) is
  begin
update withdraw_user_details
   set skrill_email = i_skrill_email,
   updated_writer_id = i_writer_id
 where user_id = i_user_id;
end update_wthdr_skrl_user_details;

procedure update_wthdr_file_id
  (
    i_user_id          in number
   ,i_file_id          in number
   ,i_writer_id        in number   
  ) is
  begin
update withdraw_user_details
   set
       file_id = i_file_id,
       updated_writer_id = i_writer_id
 where user_id = i_user_id;
end update_wthdr_file_id;

procedure get_withdraw_user_details
  (
    o_withdraw_user_details   out sys_refcursor
   ,i_user_id  in number
  ) is
  begin
  
    open o_withdraw_user_details for
         select wud.*, f.file_name
              from  WITHDRAW_USER_DETAILS wud,
                    FILES F
      where wud.user_id = i_user_id
            and wud.file_id = f.id(+);
  end get_withdraw_user_details;
  
procedure skiped_wthdr_bank_user_details
  (
    i_user_id      in number
   ,i_issue_action_id in number
  ) is
  begin
update users_active_data
   set skiped_wthdr_bank_user_details = i_issue_action_id
 where user_id = i_user_id;
end skiped_wthdr_bank_user_details; 

procedure show_wthdr_details_popup
  (
    o_show_popup      out number
   ,i_user_id in number
  ) is
  cursor c_check_popup is
  select 'x' from
         users_regulation ur,
         users_active_data uad,
         withdraw_user_details wad
  where ur.user_id = uad.user_id
         and ur.user_id = wad.user_id(+)
         and ur.approved_regulation_step >= 3
         and uad.skiped_wthdr_bank_user_details is null
         and ( -- wad.id is null : WHEN NOT FOUND DATA NOT FILLED
               wad.id is null or 
               -- wad.created_writer_id not SYSTEM WHEN IS CREATE FROM BE and wad.updated_writer_id AND LAST CHANGEDS FROM BE, IF CHANGE FROM WEB NOT SHOW
               (wad.created_writer_id not in (select id from writers w where w.dept_id = 9) and nvl(wad.updated_writer_id,wad.created_writer_id) not in (select id from writers w where w.dept_id = 9))
             )
         and ur.user_id = i_user_id;

  v_check varchar2(1);
begin
  o_show_popup := 0;
  open c_check_popup;
  fetch c_check_popup into v_check;
  if c_check_popup%found then
     o_show_popup :=1;
     end if;
  close c_check_popup;

end show_wthdr_details_popup;  

end pkg_withdraw_backend;
/
