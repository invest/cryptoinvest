CREATE OR REPLACE PACKAGE CUSTOMER_REPORTS AS

PROCEDURE RUN_CUSTOMER_REPORTS_JOB;
PROCEDURE RUN_CUSTOMER_REPORTS_BY_TYPE(v_rep_type in number, v_from_date date, v_to_date date);
PROCEDURE CALCULATE_BALANCE(v_report_id in number, v_rep_type in number , v_from_date date, v_to_date date);
PROCEDURE CALCULATE_SETTLED_INVESTMENTS(v_report_id in number, v_rep_type in number , v_from_date date, v_to_date date);
PROCEDURE CALCULATE_NOT_SETTLED_INV(v_report_id in number, v_rep_type in number , v_from_date date, v_to_date date);
PROCEDURE CALCULATE_NO_INV_WITH_BALANCE(v_report_id in number, v_rep_type in number);
PROCEDURE CALCULATE_TOTAL_LOSS_PROFIT(v_report_id in number, v_rep_type in number,v_from_date date, v_to_date date);
PROCEDURE CALCULATE_TRANSACTIONS(v_report_id in number, v_rep_type in number, v_from_date date, v_to_date date);

FUNCTION IS_USER_COLSED_BY_ISSUE(p_user_id number)
  return number;

end;
/

CREATE OR REPLACE PACKAGE BODY CUSTOMER_REPORTS
AS

FIRST_FORTNIGHTLY CONSTANT number := 1;
SECOND_FORTNIGHTLY CONSTANT number := 2;
MONTHLY_REPORT CONSTANT number := 3;

STATE_FIN_BALANCE_CALCULATE CONSTANT number := 1;
STATE_FIN_INV_SETTLED CONSTANT number := 2;
STATE_FIN_INV_NOT_SETTLED CONSTANT number := 3;
STATE_FIN_BALANCE_WITHOUT_INV CONSTANT number := 4;
STATE_FIN_MONTHLY_TRANSACTIONS CONSTANT number := 5;
STATE_FIN_MONTHLY_LOSS_PROFIT CONSTANT number := 6;


STATE_FIN_ALL_INSERT CONSTANT number := 10;
STATE_FIN_SEND CONSTANT number := 11;

STATE_ERROR CONSTANT number := 999;

GMT_PLUS_H CONSTANT number := 2;

PROCEDURE RUN_CUSTOMER_REPORTS_JOB
IS
   v_rep_type number := -1;
  
   v_from_date date;
   v_to_date date;

   --For TEST TIME
   v_sysdate date := sysdate;
BEGIN
     if (to_char(v_sysdate,'dd') = '15') then
        v_rep_type := FIRST_FORTNIGHTLY;
        v_from_date := to_date('01-'||to_char(v_sysdate,'mm-yyyy') || '00:00:00','dd-mm-yyyy hh24:mi:ss');
        v_to_date := to_date('14-'||to_char(v_sysdate,'mm-yyyy') || '23:59:59','dd-mm-yyyy hh24:mi:ss');
        --RUN FIRST FORTNIGHTLY REP
       RUN_CUSTOMER_REPORTS_BY_TYPE(v_rep_type, v_from_date, v_to_date);
        
     elsif (to_char(v_sysdate,'dd') = '01') then 
        v_rep_type := SECOND_FORTNIGHTLY;
        v_from_date := to_date('15-'||to_char(v_sysdate - 1,'mm-yyyy')|| '00:00:00','dd-mm-yyyy hh24:mi:ss');
        v_to_date := to_date(to_char(v_sysdate - 1,'dd-mm-yyyy')|| '23:59:59','dd-mm-yyyy hh24:mi:ss');
       --RUN SECOND FORTNIGHTLY REP
        RUN_CUSTOMER_REPORTS_BY_TYPE(v_rep_type, v_from_date, v_to_date);
        
        --MONTHLY REP
        v_rep_type := MONTHLY_REPORT;
        v_from_date := to_date('01-'||to_char(v_sysdate - 1,'mm-yyyy')|| '00:00:00','dd-mm-yyyy hh24:mi:ss');
        v_to_date := to_date(to_char(v_sysdate - 1,'dd-mm-yyyy')|| '23:59:59','dd-mm-yyyy hh24:mi:ss');
       --RUN SECOND FORTNIGHTLY REP
        RUN_CUSTOMER_REPORTS_BY_TYPE(v_rep_type, v_from_date, v_to_date);
     end if;
/*     
        v_rep_type := MONTHLY_REPORT;
        v_from_date := to_date('01-07-2015 00:00:00','dd-mm-yyyy hh24:mi:ss');
        v_to_date := to_date('31-07-2015 23:59:59','dd-mm-yyyy hh24:mi:ss');
        RUN_CUSTOMER_REPORTS_BY_TYPE(v_rep_type, v_from_date, v_to_date);
*/
      commit;
   
END;

PROCEDURE RUN_CUSTOMER_REPORTS_BY_TYPE(v_rep_type in number, v_from_date date, v_to_date date)
IS
   v_log_msg CLOB :='';   
   v_report_id number := 0; 
   v_from_date_il date;
   v_to_date_il date;

BEGIN

IF v_rep_type = FIRST_FORTNIGHTLY or  v_rep_type = SECOND_FORTNIGHTLY or v_rep_type = MONTHLY_REPORT THEN        

   --0.Insert state 
   v_log_msg := 'START FROM_DATE '||to_char(v_from_date,'dd-mm-yyyy hh24:mi:ss')||' GMT and TO_DATE '||to_char(v_to_date,'dd-mm-yyyy hh24:mi:ss')
             ||' GMT  and '|| v_rep_type ||' type customer report at ' || to_char(sysdate,'dd-mm-yyyy hh24:mi:ss');
   v_report_id := seq_customer_report_state.nextval;
   insert into customer_report_state
     (id, from_date, to_date, type, log_msg)
   values
     (v_report_id, v_from_date, v_to_date, v_rep_type, v_log_msg);
     
     commit;
     
     --0.Change To IL time
     v_from_date_il := v_from_date - 1/24*GMT_PLUS_H;
     v_to_date_il := v_to_date - 1/24*GMT_PLUS_H;
     v_log_msg := v_log_msg || CHR(10) ||'0.Chancge DATEs to IL time - '||GMT_PLUS_H||'h FROM_DATE '||to_char(v_from_date_il,'dd-mm-yyyy hh24:mi:ss')||
                                             ' and TO_DATE '||to_char(v_to_date_il,'dd-mm-yyyy hh24:mi:ss');
     update customer_report_state set  log_msg = v_log_msg where id = v_report_id;
     commit;
     
     --1.Calculate Balance
     CALCULATE_BALANCE(v_report_id, v_rep_type, v_from_date_il, v_to_date_il);
      v_log_msg := v_log_msg || CHR(10) ||'1.FINISH CALCULATE BALANCE  TIME:' || to_char(sysdate,'dd-mm-yyyy hh24:mi:ss');
     update customer_report_state set STATE = STATE_FIN_BALANCE_CALCULATE, log_msg = v_log_msg where id = v_report_id;
     commit;
     
     --2.Begin Calculet The Investments
     CALCULATE_SETTLED_INVESTMENTS(v_report_id, v_rep_type, v_from_date_il, v_to_date_il);
     v_log_msg := v_log_msg || CHR(10) ||'2.FINISH CALCULATE SETTLED INVESTMENTS  TIME:' || to_char(sysdate,'dd-mm-yyyy hh24:mi:ss');
     update customer_report_state set STATE = STATE_FIN_INV_SETTLED, log_msg = v_log_msg where id = v_report_id;
     commit;
     
     --3.Begin Calculet The Not Set Investments
     CALCULATE_NOT_SETTLED_INV(v_report_id, v_rep_type, v_from_date_il, v_to_date_il);
     v_log_msg := v_log_msg || CHR(10) ||'3.FINISH CALCULATE NOT SETTLED INVESTMENTS  TIME:' || to_char(sysdate,'dd-mm-yyyy hh24:mi:ss');
     update customer_report_state set STATE = STATE_FIN_INV_NOT_SETTLED, log_msg = v_log_msg where id = v_report_id;
     commit;
     
     --4.Begin Calculet No Investments with Balance >0 
     CALCULATE_NO_INV_WITH_BALANCE(v_report_id, v_rep_type);
     v_log_msg := v_log_msg || CHR(10) ||'4.FINISH CALCULATE Balance > 0 and WITHOUT INVESTMENTS TIME:' || to_char(sysdate,'dd-mm-yyyy hh24:mi:ss');
     update customer_report_state set STATE = STATE_FIN_BALANCE_WITHOUT_INV, log_msg = v_log_msg where id = v_report_id;
     commit;
     
     if v_rep_type = MONTHLY_REPORT then      
       
       --5.Begin Calculet Monthly Transactions
       CALCULATE_TRANSACTIONS(v_report_id, v_rep_type,v_from_date, v_to_date);
       v_log_msg := v_log_msg || CHR(10) ||'5.Calculet Monthly Transactions:' || to_char(sysdate,'dd-mm-yyyy hh24:mi:ss');
       update customer_report_state set STATE = STATE_FIN_MONTHLY_TRANSACTIONS, log_msg = v_log_msg where id = v_report_id;
       
       --6.Begin Calculet Monthly LOSS/Profit
       CALCULATE_TOTAL_LOSS_PROFIT(v_report_id, v_rep_type, v_from_date, v_to_date);
       v_log_msg := v_log_msg || CHR(10) ||'6.Calculet Monthly LOSS/Profit TIME:' || to_char(sysdate,'dd-mm-yyyy hh24:mi:ss');
       update customer_report_state set STATE = STATE_FIN_MONTHLY_LOSS_PROFIT, log_msg = v_log_msg where id = v_report_id;
       
     end if;
     
     
     --**FINISHED**--
     --10.Finsh the CALCULATIONS/Inserts 
     v_log_msg := v_log_msg || CHR(10) ||'10.FINISH CALCULATIONS and INSERTS  TIME:' || to_char(sysdate,'dd-mm-yyyy hh24:mi:ss');
     update customer_report_state set STATE = STATE_FIN_ALL_INSERT, log_msg = v_log_msg where id = v_report_id;
     
     update customer_report_state s set s.time_finished = sysdate where s.id = v_report_id;
     commit;
          
END IF;
   EXCEPTION
   WHEN OTHERS THEN
     v_log_msg := v_log_msg || CHR(10) ||'999.An error was encountered - '||SQLCODE||' -ERROR- '||SQLERRM ||' TIME:' || to_char(sysdate,'dd-mm-yyyy hh24:mi:ss');
     update customer_report_state set STATE = STATE_ERROR, log_msg = v_log_msg where id = v_report_id;
     COMMIT;
END;

PROCEDURE CALCULATE_BALANCE(v_report_id in number, v_rep_type in number , v_from_date date, v_to_date date)
IS  
BEGIN

IF v_rep_type = FIRST_FORTNIGHTLY or  v_rep_type = SECOND_FORTNIGHTLY  THEN        

      insert into customer_report_user_balance
        (report_id, user_id, balance_end_date)
        --VALUES
          SELECT v_report_id, b.user_id, b.balance 
          FROM 
             (select bh.user_id, max(bh.id) as m_id 
                    from 
                      balance_history bh,
                      users u
                     where 
                      bh.user_id = u.id
                      and u.skin_id = 1
                      and u.class_id != 0 
                      and bh.time_created <= v_to_date
                     group by 
                      bh.user_id
             ) last_balance,
             balance_history b
             WHERE 
              b.id = last_balance.m_id
              AND b.balance != 0;  

ELSIF v_rep_type = MONTHLY_REPORT THEN

      insert into customer_report_user_balance
        (report_id, user_id, balance_begin_date, balance_end_date)
        --VALUES
           select v_report_id, u.id, nvl(start_balance.balance,0), nvl(end_balance.balance,0) from 
            users u,                  
           (SELECT  b.user_id, b.balance 
              FROM 
                 (select bh.user_id, max(bh.id) as m_id 
                        from 
                          balance_history bh,
                          users u
                         where 
                          bh.user_id = u.id
                          and u.skin_id = 1
                          and u.class_id != 0 
                          and bh.time_created <=  v_from_date
                         group by 
                          bh.user_id
                 ) last_balance,
                 balance_history b
                 WHERE 
                  b.id = last_balance.m_id
                  ) start_balance,
              (SELECT  b.user_id, b.balance 
                FROM 
                 (select bh.user_id, max(bh.id) as m_id 
                        from 
                          balance_history bh,
                          users u
                         where 
                          bh.user_id = u.id
                          and u.skin_id = 1
                          and u.class_id != 0 
                          and bh.time_created <=  v_to_date
                         group by 
                          bh.user_id
                 ) last_balance,
                 balance_history b
                 WHERE 
                  b.id = last_balance.m_id
                  ) end_balance
              where u.skin_id = 1
              and u.class_id !=0
              and u.id = end_balance.user_id(+)    
              and u.id = start_balance.user_id(+) 
              and (nvl(start_balance.balance,0) + nvl(end_balance.balance,0)) > 0; 
END IF;
END;

PROCEDURE CALCULATE_SETTLED_INVESTMENTS(v_report_id in number, v_rep_type in number , v_from_date date, v_to_date date)
IS  
BEGIN

          insert into CUSTOMER_REPORT_INV_DATA(
 select                 
                        SEQ_CUSTOMER_REPORT_DATA.NEXTVAL as Id,
                        v_report_id as report_id,
                        v_rep_type as report_type,
                        
                        settled_inv.user_id,
                        settled_inv.opportunity_id,
                        settled_inv.opportunity_type_id,
                        settled_inv.tt as rep_group,
                        settled_inv.investment_id,
                        settled_inv.market_id,

                        settled_inv.is_canceled,
                        settled_inv.is_settled ,

                        --direction
                        decode(settled_inv.type_id,4,1,5,2,settled_inv.type_id) direction,
                        --settled_inv.type_id  as direction,                        
                        --closing_level
                        case when tt  in (1,4,101,104) then to_char(settled_inv.closing_level)
                        when tt in (2,3) then 'customer.report.2.roll.forward.expiry.level'  
                        when tt in (5, 105) then 'customer.report.5.take.profit.expiry.level.comments'
                        when tt in (6,106) and to_date(settled_inv.est_closing,'YYYY-MM-DD HH24:MI:SS') > settled_inv.time_settled  then  'customer.report.6.option.plus.expiry.level'
                        when tt in (6,106) and to_date(settled_inv.est_closing,'YYYY-MM-DD HH24:MI:SS') <= settled_inv.time_settled  then  to_char(settled_inv.closing_level)
                        when tt in (7,107) and ot_win_lose > 0 then 'onetouch.investment.win'
                        when tt in (7,107) and ot_win_lose <= 0 then 'onetouch.investment.lose'
                        when tt in (8,108) and to_date(settled_inv.est_closing,'YYYY-MM-DD HH24:MI:SS') > settled_inv.time_settled  then  'customer.report.8.0.100.expiry.level'
                        when tt in (8,108) and to_date(settled_inv.est_closing,'YYYY-MM-DD HH24:MI:SS') <= settled_inv.time_settled  then  to_char(settled_inv.closing_level)
                        when tt in (9) and settled_inv.opportunity_type_id in (3) 
                          and to_date(settled_inv.est_closing,'YYYY-MM-DD HH24:MI:SS') > settled_inv.time_settled then 'customer.report.6.option.plus.expiry.level'  
                        when tt in (9) and  settled_inv.opportunity_type_id in (4,5) 
                          and to_date(settled_inv.est_closing,'YYYY-MM-DD HH24:MI:SS') > settled_inv.time_settled  then 'customer.report.8.0.100.expiry.level'
                         else ' ' end as closing_level,
                        
                        --purchase_level
                        settled_inv.current_level as purchase_level,
                        
                        --expiry_time
                        case when tt in (5,6,105,106) then settled_inv.inv_settled_time 
                        when tt in(9) then settled_inv.time_canceled 
                        else
                        to_date(settled_inv.est_closing,'YYYY-MM-DD HH24:MI:SS') end as expiry_time,
                        
                        --expiry_time_CANCEL_9_opp_or_0100
                        case when tt in(9) and settled_inv.opportunity_type_id = 3 
                          and to_date(settled_inv.est_closing,'YYYY-MM-DD HH24:MI:SS') > settled_inv.time_settled then 'customer.report.6.option.plus.expiry.level' 
                        when tt in(9) and settled_inv.opportunity_type_id = 8
                          and to_date(settled_inv.est_closing,'YYYY-MM-DD HH24:MI:SS') > settled_inv.time_settled then 'customer.report.8.0.100.expiry.level' 
                        else '' end as expiry_time_opp_or_0100,
                        
                        --time_created
                        settled_inv.time_created,
                        settled_inv.time_canceled,
                        
                        --amount
                        case when tt in (8,108) then settled_inv.amount*settled_inv.contracts_count
                          else
                        settled_inv.amount end as amount,
                        
                        --return
                        case when tt in (2,3) then settled_inv.amount
                          when tt in (8,108) then settled_inv.returns*settled_inv.contracts_count 
                          else 
                        settled_inv.returns end as returns,
                        
                       --return when canceled afetr settled
                        case when tt > 100 then settled_inv.house_result
                          else
                            0 end as RETURNS_CANCLED_AFTER_SET,
                        
                        --return if correct
                         0 RETURN_IF_CORRECT,
                         --return if Incorrect
                         0 RETURN_IF_INCORRECT,
                        
                        --comments
                        case when tt in (2) then 'customer.report.2.roll.forward.expiry.comments' 
                        when tt in (3) then 'customer.report.3.roll.forward.expiry.comments' 
                        when tt in (4, 104) then 'customer.report.4.roll.forward.expiry.comments'
                        when tt in (5, 105) then 'customer.report.5.take.profit.expiry.comments'
                        when tt in (6,106) then 'customer.report.6.option.plus.expiry.comments'
                        when tt in (8,108) then 'customer.report.8.0.100.expiry.comments'
                        when tt in (9) then 'customer.report.9.canceled.inv.comments'
                          else
                        '' end as comments,
                        settled_inv.roll_forward_commission,
                        settled_inv.take_profit_commission,
                        case when tt in (8,108) then settled_inv.option_plus_fee*settled_inv.contracts_count
                          else 
                        settled_inv.option_plus_fee end as option_plus_fee,
                        --0100
                        settled_inv.opp_current_level,
                        to_date(settled_inv.est_closing,'YYYY-MM-DD HH24:MI:SS') as opp_time_est_closing,
                        settled_inv.contracts_count,
                        sysdate as time_inserted,
                        nvl(crub.balance_begin_date, 0),
                        nvl(crub.balance_end_date, 0)
                   from 

                          (SELECT 
                              i.user_id,
                              i.id as investment_id, 
                              i.opportunity_id,
                              o.opportunity_type_id,
                              o.market_id, i.type_id,
                              o.closing_level,
                              i.current_level,
                              to_char(SYS_EXTRACT_UTC(o.time_est_closing), 'YYYY-MM-DD HH24:MI:SS') as est_closing,
                              i.time_settled as inv_settled_time,
                              i.time_created,
                              i.amount,
                              (i.amount + i.win - i.lose) returns,
                              i.insurance_amount_ru as roll_forward_commission,
                              i.insurance_amount_gm as take_profit_commission,
                              i.option_plus_fee,
                              (i.win - i.lose) ot_win_lose,
                              i.house_result,
                              o.current_level opp_current_level,
                              
                              --TYPE_OF_INVS BY REPORT REQ.
                              case 
                                when  --1. Regular BO Investments settled in the time period
                                 o.opportunity_type_id = 1 and i.is_settled = 1 and i.is_canceled = 0  and nvl(i.reference_investment_id,0) = 0 and i.insurance_amount_gm = 0 then 1
                                when  --2. Roll forward - A natural investment that settled due to rolling it forward
                                 nvl(ref_inv.id,0) > 0 and nvl(i.reference_investment_id,0) = 0 and i.insurance_flag = 2 and i.is_canceled = 1 and i.is_settled = 1 and i.canceled_writer_id = 0 then 2
                                when  --3. Roll forward – an investment created due to roll forward, then roll forwarded again
                                 nvl(ref_inv.id,0) > 0 and nvl(i.reference_investment_id,0) > 0 and i.insurance_flag = 2 and i.is_canceled = 1 and i.is_settled = 1 and i.canceled_writer_id = 0  then 3 
                                when  --4. Roll forward – an investment created due to roll forward (Whether once or twice), then settled naturally
                                 i.reference_investment_id > 0 and i.insurance_amount_ru > 0 and i.is_canceled = 0 and i.is_settled = 1  then 4  
                                when  --5. Investment settled due to Take profit GM
                                 i.insurance_flag = 1 and i.insurance_amount_gm > 0 and i.is_canceled = 0 and i.is_settled = 1  then 5
                                when  --6. Option+
                                 o.opportunity_type_id = 3 and i.is_canceled = 0 and i.is_settled = 1  then 6
                                when  --7. One Touch
                                 o.opportunity_type_id = 2 and i.is_canceled = 0 and i.is_settled = 1  then 7
                                 when --8. 0-100
                                 o.opportunity_type_id in (4,5) and i.is_canceled = 0 and i.is_settled = 1  then 8
                                when  --9. Option canceled BEFORE it expired
                                 i.is_canceled = 1 and i.canceled_writer_id != 0  and i.time_canceled <= i.time_settled and i.house_result = 0 then 9
                                -------CANCLED AFTER SETTLED---------------------
                                when  --101.BO Option canceled BEFORE it expired BO
                                  o.opportunity_type_id = 1 and nvl(i.reference_investment_id,0) = 0 and i.insurance_amount_gm = 0 and i.is_canceled = 1 and i.canceled_writer_id != 0  and i.time_canceled > i.time_settled then 101
                                when  --104.roll forward Option canceled BEFORE it expired investment created due to roll forward
                                  i.reference_investment_id > 0 and i.insurance_amount_ru > 0 and i.canceled_writer_id != 0  and i.time_canceled > i.time_settled then 104
                                when  --105.Investment settled due to Take profit  Option canceled BEFORE it expired investment created GM
                                  i.insurance_flag = 1 and i.insurance_amount_gm > 0 and i.canceled_writer_id != 0  and i.time_canceled > i.time_settled  then 105
                                when  --106. Option+ Option canceled BEFORE it expired
                                  o.opportunity_type_id = 3  and i.canceled_writer_id != 0  and i.time_canceled > i.time_settled  then 106
                                when  --107. One Touch
                                 o.opportunity_type_id = 2 and i.canceled_writer_id != 0  and i.time_canceled > i.time_settled  then 107
                                when --8. 0-100
                                 o.opportunity_type_id in (4,5) and i.is_canceled != 0 and i.time_canceled > i.time_settled  then 108
                               else 0 end  as tt
                                 
                              ,ref_inv.id,i.reference_investment_id,i.insurance_flag,i.is_canceled,i.is_settled ,i.canceled_writer_id,
                               i.time_canceled , i.time_settled, i.contracts_count

                        FROM 
                         (select ii.*,
                                row_number() over(partition by ii.user_id, ii.opportunity_id, ii.type_id, ii.amount, ii.current_level, ii.time_created, ii.time_settled, ii.group_inv_id order by ii.id desc) id_order,
                                count(*) over(partition by ii.user_id, ii.opportunity_id, ii.type_id, ii.amount, ii.current_level, ii.time_created, ii.time_settled, ii.group_inv_id) contracts_count
                           from investments ii
                          where  (ii.is_settled = 1)
                           
    
                            and ii.time_created >= v_from_date 
                            and ii.time_created <= v_to_date
                            and nvl(ii.id, 0) LIKE '%%') i,
                            opportunities o,
                            users u,
                            (select *
                              from investments i
                              where i.reference_investment_id > 0) ref_inv
                        WHERE 
                            i.user_id = u.id
                            and i.id_order = 1
                            and u.skin_id = 1
                            and u.class_id != 0
                            and i.opportunity_id = o.id
                            and i.time_settled is not null
                            and i.id = ref_inv.reference_investment_id(+)

                          --  and i.time_created >= to_date('01-01-2010 00:00:01','dd-mm-yyyy hh24:mi:ss') 
                           --- and i.time_created <= to_date('31-05-2015 23:59:59','dd-mm-yyyy hh24:mi:ss')
                            
                          --  and i.id in (7840548,7839015,7844769) 
                         --  and o.opportunity_type_id  in (4,5)
                        --  and i.user_id in (141918)
                          --and i.id = 28419869
                          ) settled_inv
                          ,(select * from customer_report_user_balance where report_id = v_report_id) crub
                          WHERE settled_inv.user_id = crub.user_id(+)
                          );
END;

PROCEDURE CALCULATE_NOT_SETTLED_INV(v_report_id in number, v_rep_type in number , v_from_date date, v_to_date date)
IS  
BEGIN

          insert into CUSTOMER_REPORT_INV_DATA(
  select                
                        SEQ_CUSTOMER_REPORT_DATA.NEXTVAL as Id,
                        v_report_id as report_id,
                        v_rep_type as report_type,
                        
                        settled_inv.user_id,
                        settled_inv.opportunity_id,
                        settled_inv.opportunity_type_id,
                        settled_inv.tt as rep_group,
                        settled_inv.investment_id,
                        settled_inv.market_id,

                        settled_inv.is_canceled,
                        settled_inv.is_settled ,

                        --direction
                        decode(settled_inv.type_id,4,1,5,2,settled_inv.type_id) direction,                       
                        --closing_level
                         '-1'  as closing_level,
                        
                        --purchase_level
                        settled_inv.current_level as purchase_level,
                        
                        --expiry_time
                        to_date(settled_inv.est_closing,'YYYY-MM-DD HH24:MI:SS') as expiry_time,
                        
                        --expiry_time_CANCEL_9_opp_or_0100
                          '' as expiry_time_opp_or_0100,
                        
                        --time_created
                        settled_inv.time_created,
                        settled_inv.time_canceled,
                        
                        --amount
                        case when tt in (24) then settled_inv.amount*settled_inv.contracts_count
                          else
                        settled_inv.amount end as amount,
                        
                        --return
                         0 as returns,
                         
                         0 as RETURNS_CANCLED_AFTER_SET,

                        --return if correct 
                         case when tt in (24) then
                           100*100*settled_inv.contracts_count 
                           else                           
                         (settled_inv.amount_without_fees*settled_inv.odds_win) + settled_inv.amount_without_fees 
                         end as RETURN_IF_CORRECT,
                         
                         --return if Incorrect
                         case when tt in (25,24) then 0
                           else
                            settled_inv.amount_without_fees* (1-settled_inv.odds_lose)*settled_inv.contracts_count
                            end as RETURN_IF_INCORRECT,
                        
                        --comments
                        case when tt in (22) then 'customer.report.22.roll.forward.not.set.comments' 
                        when tt in (23) then 'customer.report.23.option.plus.not.set.comments' 
                        when tt in (24) then 'customer.report.24.0.100.not.set.comments'
                          else
                        '' end as comments,
                        settled_inv.roll_forward_commission,
                        settled_inv.take_profit_commission,
                        case when tt in (24) then settled_inv.option_plus_fee*settled_inv.contracts_count
                          else 
                        settled_inv.option_plus_fee end as option_plus_fee,
                        --0100
                        settled_inv.opp_current_level,
                        to_date(settled_inv.est_closing,'YYYY-MM-DD HH24:MI:SS') as opp_time_est_closing,
                        settled_inv.contracts_count,
                        sysdate as time_inserted,
                        nvl(crub.balance_begin_date, 0),
                        nvl(crub.balance_end_date, 0)
                   from 

                          (SELECT 
                              i.user_id,
                              i.id as investment_id, 
                              i.opportunity_id,
                              o.opportunity_type_id,
                              o.market_id, i.type_id,
                              o.closing_level,
                              i.current_level,
                              to_char(SYS_EXTRACT_UTC(o.time_est_closing), 'YYYY-MM-DD HH24:MI:SS') as est_closing,
                              i.time_settled as inv_settled_time,
                              i.time_created,
                              i.amount,
                              (i.amount + i.win - i.lose) returns,
                              i.insurance_amount_ru as roll_forward_commission,
                              i.insurance_amount_gm as take_profit_commission,
                              i.option_plus_fee,
                              (i.win - i.lose) ot_win_lose,
                              i.house_result,
                              o.current_level opp_current_level,
                              --IF CORRECT AND NOT
                              i.odds_win,
                              i.odds_lose,
                              i.amount - nvl(i.insurance_amount_gm,0) - nvl(i.insurance_amount_ru,0) - nvl(i.option_plus_fee,0) as amount_without_fees,
                              nvl(oot.up_down,-1) as ot_up_down,
                              i.bonus_odds_change_type_id,
                              
                              --TYPE_OF_INVS BY REPORT REQ.
                              case 
                                when  --21. Regular BO Investments NOT settled in the time period
                                 o.opportunity_type_id = 1  and i.is_canceled = 0  and nvl(i.reference_investment_id,0) = 0 then 21
                                when  --22. Roll forward – an investment created due to roll forward (Whether once or twice), NOT settled 
                                 i.reference_investment_id > 0 and i.insurance_amount_ru > 0 and i.is_canceled = 0   then 22  
                                when  --23. Option+
                                 o.opportunity_type_id = 3 and i.is_canceled = 0  then 23
                                when --24. 0-100
                                 o.opportunity_type_id in (4,5) and i.is_canceled = 0 then 24
                                when  --25. One Touch
                                 o.opportunity_type_id = 2 and i.is_canceled = 0 then 25

   
                                else 0 end  as tt
                                 
                              ,ref_inv.id,i.reference_investment_id,i.insurance_flag,i.is_canceled,i.is_settled ,i.canceled_writer_id,
                               i.time_canceled , i.time_settled, i.contracts_count

                        FROM 
                         (select ii.*,
                                row_number() over(partition by ii.user_id, ii.opportunity_id, ii.type_id, ii.amount, ii.current_level, ii.time_created, ii.time_settled, ii.group_inv_id order by ii.id desc) id_order,
                                count(*) over(partition by ii.user_id, ii.opportunity_id, ii.type_id, ii.amount, ii.current_level, ii.time_created, ii.time_settled, ii.group_inv_id) contracts_count
                           from investments ii
                          where  (ii.is_settled = 0)
                           
    
                            and ii.time_created >= v_from_date  
                            and ii.time_created <= v_to_date
                            and nvl(ii.id, 0) LIKE '%%') i,
                            opportunities o,
                            users u,
                            (select *
                              from investments i
                              where i.reference_investment_id > 0) ref_inv,
                              opportunity_one_touch oot
                        WHERE 
                            i.user_id = u.id
                            and i.id_order = 1
                            and u.skin_id = 1
                            and u.class_id != 0
                            and i.opportunity_id = o.id          
                            and i.id = ref_inv.reference_investment_id(+)
                            and i.opportunity_id = oot.opportunity_id(+)

                          --  and i.time_created >= to_date('01-01-2010 00:00:01','dd-mm-yyyy hh24:mi:ss') 
                           --- and i.time_created <= to_date('31-05-2015 23:59:59','dd-mm-yyyy hh24:mi:ss')
                            
                          --  and i.id in (7840548,7839015,7844769) 
                         --  and o.opportunity_type_id  in (4,5)
                        --  and i.user_id in (141918)
                         -- and i.id = 28621308
                          ) settled_inv
                                                    ,(select * from customer_report_user_balance where report_id = v_report_id) crub
                          WHERE settled_inv.user_id = crub.user_id(+)
                   );
END;



PROCEDURE CALCULATE_NO_INV_WITH_BALANCE(v_report_id in number, v_rep_type in number)
IS  
v_t number;
BEGIN 
  v_t := 1;
insert into customer_report_inv_data
  (id, report_id, report_type, user_id,  balance_begin_date, balance_end_date)

  (select seq_customer_report_data.nextval, bal.report_id, v_rep_type, bal.user_id, 
          bal.balance_begin_date, bal.balance_end_date from 
          customer_report_user_balance  bal          
   where bal.user_id not in (select user_id from customer_report_inv_data dat where report_id = v_report_id)
         and bal.report_id = v_report_id
   );
END;

PROCEDURE CALCULATE_TRANSACTIONS(v_report_id in number, v_rep_type in number, v_from_date date, v_to_date date)
IS  


BEGIN
   insert into CUSTOMER_REPORT_TR_DATA(
select 
 SEQ_CUSTOMER_REPORT_TR_DATA.Nextval,
 v_report_id,
 v_rep_type, 
 user_id,
 id as transaction_id,
  types,
 --Date and  Time
 case when types in (1,2,3,9,10,11)
      then time_created 
      else time_settled 
 end as date_time,
  --UTC offset
 case when types in (1,2,3,9,10,11)
      then utc_offset_created
      when types in (7,8)   
        then ''
      else utc_offset_settled 
 end as utc_offset,
 -- Description
 case when types in (12)
         then 'customer.report.12.reverse.withdraw.description'
      when types in (13)
         then 'customer.report.13.reverse.withdraw.description'
      else  description
 end as description,
 --Credit
 case when types in (1,2,3,12,13,14)
      then amount 
      else null
  end as credit,
  --Debit
  case when types in (4,5,6,7,8,9,10,11)
       then amount 
       else null 
  end as debit,
  --Comments
  case when types in (3)
       then 'customer.report.3.deposits.pending.comments' 
       when types in (4)  
        then 'customer.report.4.deposit.failed.comments' 
       when types in (5)   
        then 'customer.report.5.deposit.cancel.comments.row2'
       when types in (6)  
        then 'customer.report.6.deposit.status.cancel.comments' 
       when types in (7)   
         then 'customer.report.7.deposit.charged.back.comments'
       when types in (8)   
         then 'customer.report.8.deposit.charged.back.comments' 
       when types in (11)       
         then 'customer.report.11.withdrawal.comments'
       when types in (14)
         then 'customer.report.14.withdrawal.failed.comments'    
       else '' 
  end as comments,
  
  time_created, 
  time_settled,
  utc_offset_created, 
  utc_offset_settled,
  sysdate 
  
    
 
from  

(select 
tr.user_id,
--1.Successful deposits  (status=2) created in the report's time frame (created and settled in the same time frame)
case when tt.class_type = 1 and tr.status_id = 2 
     and tr.time_created between v_from_date and v_to_date
     and tr.time_settled between v_from_date and v_to_date
 then 1
--2.ADMIN deposit,  Fix balance deposit, Fix negative, Fees canceled - Settled in the period, regardless of time created.   
  when tt.id in (4,22) and tr.time_settled between v_from_date and v_to_date
 then 2
--3.Deposits:   Pending (Status=7) at report's end time, created in the report's time frame    
  when tt.class_type = 1 and tr.status_id = 7 
     and tr.time_created between v_from_date and v_to_date
    --????????? and tr.time_settled between v_from_date and v_to_date
  then  3
--4.Deposit Failed (status=3) OR Deposit cancel (TYPES: Cancel by etrader (8), cancel fraud (12), cancel MNR (16)): when time created is from a previous report's time (time failed\ Canceled is in a subsequent report to creation).    
  when tt.class_type = 1 and tr.status_id in (3,8,12,16)
     and tr.time_created < v_from_date
     and tr.time_settled between v_from_date and v_to_date
  then  4
--5.Deposit Cancel (TYPES: Cancel by etrader (8), cancel fraud (12), cancel MNR (16)): time created and  time canceled are in the same report's time frame.    
  when tt.class_type = 1 and tr.status_id in (8,12,16)
     and tr.time_created between v_from_date and v_to_date
     and tr.time_settled between v_from_date and v_to_date
  then  5
--6.Deposit is in status Cancel SM (Support mistake) (Status = 15): time created is from a previous report's time, (time cancel SM is in a subsequent report to creation).    
  when tt.class_type = 1 and tr.status_id in (15)
     and tr.time_created < v_from_date
     and tr.time_settled between v_from_date and v_to_date
  then 6 
--9.Successful withdrawal    
  when tt.class_type in (2,8) and tr.status_id in (2)
     and tr.time_created between v_from_date and v_to_date
  then 9
--10.Fix withdraw, Admin withdraw, Fees   
  when tt.id in (5,23)
     and tr.time_created between v_from_date and v_to_date
  then 10  
--11.Withdrawal: Requested, approved or second approved.    
  when tt.class_type = 2 and tr.status_id in (4,9,17)
     and tr.time_created between v_from_date and v_to_date
  then 11
--12.Reverse withdraw (status=6)-  time created is from a previous report's time, (time reversed is in a subsequent report to creation).    
  when tt.class_type = 2 and tr.status_id in (6)
     and tr.time_settled between v_from_date and v_to_date 
     and tr.time_created < v_from_date
  then 12                            
--13.Reverse withdraw (status=6) -  time created and  time reversed is in the same report time frame    
  when tt.class_type = 2 and tr.status_id in (6)
     and tr.time_settled between v_from_date and v_to_date
     and tr.time_created between v_from_date and v_to_date
  then 13 
--14.Withdrawal failed (status=3)- time created is from a previous report's time: time failed is in a subsequent report to creation    
  when tt.class_type = 2 and tr.status_id in (3)
     and tr.time_settled between v_from_date and v_to_date
     and tr.time_created < v_from_date 
  then 14    
   else 999 end as types,
     tr.id,
     tr.time_created,
     tr.time_settled,
     tt.description,
     tr.amount,
     tr.utc_offset_created,
     tr.utc_offset_settled

from 
transactions tr,
transaction_types tt,
users u
where tr.type_id = tt.id
and u.id = tr.user_id
and u.skin_id = 1
and u.class_id != 0
and 
(
    (tr.time_settled between v_from_date and v_to_date) 
OR
    (tr.time_created between v_from_date and v_to_date) 
)
and tr.status_id != 14

union all

select 
tr.user_id,
--7.Deposit charged back (Status=14): time created is from a previous report's time,  time charged back is in a subsequent report to creation).
case when  tr.status_id = 14 
     and tr.time_created < v_from_date
 then 7
--8.Deposit Charged back (14): time created and time charged back is in the same report time period.
   when  tr.status_id = 14 
     and tr.time_created between v_from_date and v_to_date     
 then 8
   else 998 end as types,
     tr.id,
     tr.time_created,
     cb.time_created,
     tt.description,
     tr.amount,
     tr.utc_offset_created,
     tr.utc_offset_settled
from 
transactions tr,
transaction_types tt,
charge_backs cb,
users u
where tr.type_id = tt.id
and u.id = tr.user_id
and u.skin_id = 1
and u.class_id != 0
and cb.id = tr.charge_back_id
and cb.time_chb between v_from_date and v_to_date
and tr.status_id = 14
) trans
where trans.types != 999
);

END;


PROCEDURE CALCULATE_TOTAL_LOSS_PROFIT(v_report_id in number, v_rep_type in number,v_from_date date, v_to_date date)
IS  

BEGIN 
insert into CUSTOMER_REPORT_PROFIT_DATA(
      SELECT 
       SEQ_CUSTOMER_REPORT_PROFIT.NEXTVAL,
       v_report_id,
       v_rep_type,
       p.*
      FROM      
        (select       
         i.user_id, 
         sum(i.lose), 
         sum(i.win),
         sysdate 
        from 
         investments i,
         (select distinct inv.user_id 
          from 
              customer_report_inv_data inv
          where 
              inv.report_id = v_report_id
          union all
          select distinct tr.user_id from 
              customer_report_tr_data tr
          where 
               tr.report_id = v_report_id
               and tr.user_id not in (select distinct user_id from customer_report_inv_data where report_id = v_report_id)
         ) calc_users
        where 
        (i.time_settled between v_from_date and v_to_date)
        and i.user_id = calc_users.user_id
        and i.is_settled = 1
        and i.is_canceled = 0
       group by 
         i.user_id) p
       
);
  
END;

FUNCTION IS_USER_COLSED_BY_ISSUE(p_user_id number)
  return number is

cursor c_get_close_action is
select ia.issue_action_type_id from issues s,
issue_actions ia
where s.id = ia.issue_id
and ia.issue_action_type_id in (1,9,10,11,13)
and s.user_id = p_user_id;

v_issue_action_type_id number;
v_is_closed number := 0;

  BEGIN

  open c_get_close_action;
  fetch c_get_close_action into v_issue_action_type_id;
  if c_get_close_action%found then
    v_is_closed:=1;
  end if;  
  close c_get_close_action;

  return v_is_closed;
  END;

end;

/