create or replace package pkg_netrefer_reports is

  -- Author  : VICTOR.SLAVOV
  -- Created : 2016-08-19 10:57:52
  -- Purpose : Netrefer Activity report
  -- Note    : This is a statefull package!

  type t_userrep_rec is record(
     customerid        number
    ,country_id        varchar2(10)
    ,tag               varchar2(1000 char)
    ,registration_ip   varchar2(60)
    ,registration_date date
    ,customertypeid    number
    ,brand_id          number
    ,origin            varchar2(30));

  type t_activity_rec is record(
     customerid      number
    ,activity_date   date
    ,product_id      varchar2(100)
    ,bonuses         number
    ,adjustments     number
    ,sum_deposits    number
    ,gross_rev       number
    ,turnover        number
    ,sum_withdraws   number
    ,investments_num number
    ,brand_id        number
    ,fd_platform_id  number
    ,reg_platform_id number
    ,origin          varchar2(30));

  type t_activity_rec_tab is table of t_activity_rec;
  type t_userrep_rec_tab is table of t_userrep_rec;

  function nref_activity
  (
    p_fromdate in date
   ,p_todate   in date
  ) return t_activity_rec_tab
    pipelined;

  function nref_activity(i_date in date default null) return string_table
    pipelined;

  function user_report
  (
    p_fromdate in date
   ,p_todate   in date
  ) return t_userrep_rec_tab
    pipelined;

  function user_report(i_date in date default null) return string_table
    pipelined;

end pkg_netrefer_reports;
/
create or replace package body pkg_netrefer_reports is
  c_platform_ao constant number := 2;
  c_platform_co constant number := 3;

  cursor cr_user_report
  (
    p_fromdate date
   ,p_todate   date
  ) is
    select u.id customerid
          ,c.a2 country_id
          ,nvl(replace((case
                         when u.special_code is not null then
                          u.special_code
                         else
                          u.dynamic_param
                       end)
                      ,' '
                      ,'')
              ,' ') tag
          ,u.ip registration_ip
          ,u.time_created registration_date
          ,c.countries_group_netrefer_id customertypeid
          ,case
             when u.platform_id = 3 then
              4
             else
              case
                when u.skin_id in (22) then
                 2
                when u.skin_id in (9, 20) then
                 3
                else
                 1
              end
           end brand_id
          ,case
             when u.writer_id in (1, 10000, 21000) then
              'Web'
             when u.writer_id in (200, 20000, 23000, 24000, 25000) then
              'Mobile'
             else
              'Other'
           end origin
    from   users u
    join   countries c
    on     c.id = u.country_id
    and    u.affiliate_system_user_time >= p_fromdate
    and    u.affiliate_system_user_time < p_todate
    and    u.class_id <> 0
    and    u.affiliate_system = 1;

  cursor cr_activity_data
  (
    p_fromdate date
   ,p_todate   date
  ) is
    with data as
     (select /*+ OPT_PARAM('_B_TREE_BITMAP_PLANS','FALSE') */
       g.user_id customerid
      ,h.time_created fd_time_created
      ,case
         when h.writer_id in (1, 200, 21000, 23000, 25000) then
          2 --- platform "anyoption"
         when h.writer_id in (10000, 20000, 24000) then
          3 --- platform "copyop"
         else
          u.platform_id --- reg_platform_id
       end fd_platform_id
      ,u.platform_id reg_platform_id
      ,case
         when u.platform_id = 3 then
          4
         else
          case
            when u.skin_id in (22) then
             2
            when u.skin_id in (9, 20) then
             3
            else
             1
          end
       end brand_id
      ,g.bonuses
       
      ,sum_deposits_ao_web * 0.06 + cft_withdraws_num_ao_web * 2 + wire_withdraws_fee_ao_web adjustments_ao_web
      ,sum_deposits_co_web * 0.06 + cft_withdraws_num_co_web * 2 + wire_withdraws_fee_co_web adjustments_co_web
      ,sum_deposits_ao_mobile * 0.06 + cft_withdraws_num_ao_mobile * 2 + wire_withdraws_fee_ao_mobile adjustments_ao_mobile
      ,sum_deposits_co_mobile * 0.06 + cft_withdraws_num_co_mobile * 2 + wire_withdraws_fee_co_mobile adjustments_co_mobile
      ,sum_deposits_other * 0.06 + cft_withdraws_num_other * 2 + wire_withdraws_fee_other adjustments_other
       
      ,sum_deposits_ao_web
      ,sum_deposits_co_web
      ,sum_deposits_ao_mobile
      ,sum_deposits_co_mobile
      ,sum_deposits_other
       
      ,house_win_ao_web - fixbalance_ao_web gross_rev_ao_web
      ,house_win_co_web - fixbalance_co_web gross_rev_co_web
      ,house_win_ao_mobile - fixbalance_ao_mobile gross_rev_ao_mobile
      ,house_win_co_mobile - fixbalance_co_mobile gross_rev_co_mobile
      ,house_win_other - fixbalance_other gross_rev_other
       
      ,turnover_ao_web
      ,turnover_co_web
      ,turnover_ao_mobile
      ,turnover_co_mobile
      ,turnover_other
       
      ,sum_withdraws_ao_web
      ,sum_withdraws_co_web
      ,sum_withdraws_ao_mobile
      ,sum_withdraws_co_mobile
      ,sum_withdraws_other
       
      ,investments_num_ao_web
      ,investments_num_co_web
      ,investments_num_ao_mobile
      ,investments_num_co_mobile
      ,investments_num_other
      
      from   (select user_id
                    ,nvl(sum(sum_deposits_ao_web), 0) sum_deposits_ao_web
                    ,nvl(sum(sum_deposits_co_web), 0) sum_deposits_co_web
                    ,nvl(sum(sum_deposits_ao_mobile), 0) sum_deposits_ao_mobile
                    ,nvl(sum(sum_deposits_co_mobile), 0) sum_deposits_co_mobile
                    ,nvl(sum(sum_deposits_other), 0) sum_deposits_other
                     
                    ,nvl(sum(fixbalance_ao_web), 0) fixbalance_ao_web
                    ,nvl(sum(fixbalance_co_web), 0) fixbalance_co_web
                    ,nvl(sum(fixbalance_ao_mobile), 0) fixbalance_ao_mobile
                    ,nvl(sum(fixbalance_co_mobile), 0) fixbalance_co_mobile
                    ,nvl(sum(fixbalance_other), 0) fixbalance_other
                     
                    ,nvl(sum(sum_withdraws_ao_web), 0) sum_withdraws_ao_web
                    ,nvl(sum(sum_withdraws_co_web), 0) sum_withdraws_co_web
                    ,nvl(sum(sum_withdraws_ao_mobile), 0) sum_withdraws_ao_mobile
                    ,nvl(sum(sum_withdraws_co_mobile), 0) sum_withdraws_co_mobile
                    ,nvl(sum(sum_withdraws_other), 0) sum_withdraws_other
                    ,nvl(sum(cft_withdraws_num_ao_web), 0) cft_withdraws_num_ao_web
                    ,nvl(sum(cft_withdraws_num_co_web), 0) cft_withdraws_num_co_web
                    ,nvl(sum(cft_withdraws_num_ao_mobile), 0) cft_withdraws_num_ao_mobile
                    ,nvl(sum(cft_withdraws_num_co_mobile), 0) cft_withdraws_num_co_mobile
                    ,nvl(sum(cft_withdraws_num_other), 0) cft_withdraws_num_other
                    ,nvl(sum(wire_withdraws_fee_ao_web), 0) wire_withdraws_fee_ao_web
                    ,nvl(sum(wire_withdraws_fee_co_web), 0) wire_withdraws_fee_co_web
                    ,nvl(sum(wire_withdraws_fee_ao_mobile), 0) wire_withdraws_fee_ao_mobile
                    ,nvl(sum(wire_withdraws_fee_co_mobile), 0) wire_withdraws_fee_co_mobile
                    ,nvl(sum(wire_withdraws_fee_other), 0) wire_withdraws_fee_other
                     
                    ,nvl(sum(turnover_ao_web), 0) turnover_ao_web
                    ,nvl(sum(turnover_co_web), 0) turnover_co_web
                    ,nvl(sum(house_win_ao_web), 0) house_win_ao_web
                    ,nvl(sum(house_win_co_web), 0) house_win_co_web
                    ,nvl(sum(investments_num_ao_web), 0) investments_num_ao_web
                    ,nvl(sum(investments_num_co_web), 0) investments_num_co_web
                    ,nvl(sum(turnover_ao_mobile), 0) turnover_ao_mobile
                    ,nvl(sum(turnover_co_mobile), 0) turnover_co_mobile
                    ,nvl(sum(house_win_ao_mobile), 0) house_win_ao_mobile
                    ,nvl(sum(house_win_co_mobile), 0) house_win_co_mobile
                    ,nvl(sum(investments_num_ao_mobile), 0) investments_num_ao_mobile
                    ,nvl(sum(investments_num_co_mobile), 0) investments_num_co_mobile
                    ,nvl(sum(turnover_other), 0) turnover_other
                    ,nvl(sum(house_win_other), 0) house_win_other
                    ,nvl(sum(investments_num_other), 0) investments_num_other
                     
                    ,nvl(sum(bonuses), 0) bonuses
              from   (select t.user_id
                             
                            ,sum(case
                                   when t.writer_id in (1, 21000) then
                                    case
                                      when t.status_id in (2, 7) then
                                       t.amount * t.rate
                                      else
                                       -1 * t.amount * t.rate
                                    end
                                   else
                                    0
                                 end) / 100 sum_deposits_ao_web
                            ,sum(case
                                   when t.writer_id in (10000) then
                                    case
                                      when t.status_id in (2, 7) then
                                       t.amount * t.rate
                                      else
                                       -1 * t.amount * t.rate
                                    end
                                   else
                                    0
                                 end) / 100 sum_deposits_co_web
                            ,sum(case
                                   when t.writer_id in (200, 23000, 25000) then
                                    case
                                      when t.status_id in (2, 7) then
                                       t.amount * t.rate
                                      else
                                       -1 * t.amount * t.rate
                                    end
                                   else
                                    0
                                 end) / 100 sum_deposits_ao_mobile
                            ,sum(case
                                   when t.writer_id in (20000, 24000) then
                                    case
                                      when t.status_id in (2, 7) then
                                       t.amount * t.rate
                                      else
                                       -1 * t.amount * t.rate
                                    end
                                   else
                                    0
                                 end) / 100 sum_deposits_co_mobile
                            ,sum(case
                                   when t.writer_id not in (1, 10000, 200, 20000, 21000, 23000, 24000, 25000) then
                                    case
                                      when t.status_id in (2, 7) then
                                       t.amount * t.rate
                                      else
                                       -1 * t.amount * t.rate
                                    end
                                   else
                                    0
                                 end) / 100 sum_deposits_other
                             
                            ,null fixbalance_ao_web
                            ,null fixbalance_co_web
                            ,null fixbalance_ao_mobile
                            ,null fixbalance_co_mobile
                            ,null fixbalance_other
                             
                            ,null sum_withdraws_ao_web
                            ,null sum_withdraws_co_web
                            ,null sum_withdraws_ao_mobile
                            ,null sum_withdraws_co_mobile
                            ,null sum_withdraws_other
                            ,null cft_withdraws_num_ao_web
                            ,null cft_withdraws_num_co_web
                            ,null cft_withdraws_num_ao_mobile
                            ,null cft_withdraws_num_co_mobile
                            ,null cft_withdraws_num_other
                            ,null wire_withdraws_fee_ao_web
                            ,null wire_withdraws_fee_co_web
                            ,null wire_withdraws_fee_ao_mobile
                            ,null wire_withdraws_fee_co_mobile
                            ,null wire_withdraws_fee_other
                             
                            ,null turnover_ao_web
                            ,null turnover_co_web
                            ,null house_win_ao_web
                            ,null house_win_co_web
                            ,null investments_num_ao_web
                            ,null investments_num_co_web
                            ,null turnover_ao_mobile
                            ,null turnover_co_mobile
                            ,null house_win_ao_mobile
                            ,null house_win_co_mobile
                            ,null investments_num_ao_mobile
                            ,null investments_num_co_mobile
                            ,null turnover_other
                            ,null house_win_other
                            ,null investments_num_other
                             
                            ,null bonuses
                      from   (select writer_id, amount, rate, status_id, user_id
                              from   transactions
                              where  time_created >= p_fromdate
                              and    time_created < p_todate
                              and    status_id in (2, 7)
                              and    type_id in (select id from transaction_types where class_type in (1, 11))
                              
                              union all
                              
                              select writer_id, amount, rate, status_id, user_id
                              from   transactions
                              where  time_created >= p_fromdate - 20
                              and    time_created < p_todate
                              and    time_settled - time_created <= 20
                              and    time_settled >= p_fromdate
                              and    time_settled < p_todate
                              and    trunc(time_settled) <> trunc(time_created)
                              and    status_id in (8, 12, 13)
                              and    type_id in (select id from transaction_types where class_type in (1, 11))
                              
                              union all
                              
                              select /*+ index(a, idx_trns_longtrns) */
                               writer_id, amount, rate, status_id, user_id
                              from   transactions
                              where  case
                                       when time_settled - time_created > 20 then
                                        time_settled
                                     end >= p_fromdate
                              and    case
                                      when time_settled - time_created > 20 then
                                       time_settled
                                    end >= p_todate
                              and    trunc(time_settled) <> trunc(time_created)
                              and    status_id in (8, 12, 13)
                              and    type_id in (select id from transaction_types where class_type in (1, 11))
                              
                              union all
                              
                              select a.writer_id, a.amount, a.rate, a.status_id, a.user_id
                              from   transactions a
                              join   charge_backs cb
                              on     cb.id = a.charge_back_id
                              where  cb.time_created >= p_fromdate
                              and    cb.time_created < p_todate
                              and    trunc(cb.time_created) <> trunc(a.time_created)
                              and    a.status_id = 14
                              and    a.type_id in (select id from transaction_types where class_type in (1, 11))) t
                      group  by t.user_id
                      
                      union all
                      
                      select t.user_id
                             
                            ,null sum_deposits_ao_web
                            ,null sum_deposits_co_web
                            ,null sum_deposits_ao_mobile
                            ,null sum_deposits_co_mobile
                            ,null sum_deposits_other
                             
                            ,sum(case
                                   when t.writer_id in (1, 21000) then
                                    case
                                      when t.type_id = 22 then
                                       t.amount * t.rate
                                      else
                                       -1 * t.amount * t.rate
                                    end
                                   else
                                    0
                                 end) / 100 fixbalance_ao_web
                            ,sum(case
                                   when t.writer_id in (10000) then
                                    case
                                      when t.type_id = 22 then
                                       t.amount * t.rate
                                      else
                                       -1 * t.amount * t.rate
                                    end
                                   else
                                    0
                                 end) / 100 fixbalance_co_web
                            ,sum(case
                                   when t.writer_id in (200, 23000, 25000) then
                                    case
                                      when t.type_id = 22 then
                                       t.amount * t.rate
                                      else
                                       -1 * t.amount * t.rate
                                    end
                                   else
                                    0
                                 end) / 100 fixbalance_ao_mobile
                            ,sum(case
                                   when t.writer_id in (20000, 24000) then
                                    case
                                      when t.type_id = 22 then
                                       t.amount * t.rate
                                      else
                                       -1 * t.amount * t.rate
                                    end
                                   else
                                    0
                                 end) / 100 fixbalance_co_mobile
                            ,sum(case
                                   when t.writer_id not in (1, 10000, 200, 20000, 21000, 23000, 24000, 25000) then
                                    case
                                      when t.type_id = 22 then
                                       t.amount * t.rate
                                      else
                                       -1 * t.amount * t.rate
                                    end
                                   else
                                    0
                                 end) / 100 fixbalance_other
                             
                            ,null sum_withdraws_ao_web
                            ,null sum_withdraws_co_web
                            ,null sum_withdraws_ao_mobile
                            ,null sum_withdraws_co_mobile
                            ,null sum_withdraws_other
                            ,null cft_withdraws_num_ao_web
                            ,null cft_withdraws_num_co_web
                            ,null cft_withdraws_num_ao_mobile
                            ,null cft_withdraws_num_co_mobile
                            ,null cft_withdraws_num_other
                            ,null wire_withdraws_fee_ao_web
                            ,null wire_withdraws_fee_co_web
                            ,null wire_withdraws_fee_ao_mobile
                            ,null wire_withdraws_fee_co_mobile
                            ,null wire_withdraws_fee_other
                             
                            ,null turnover_ao_web
                            ,null turnover_co_web
                            ,null house_win_ao_web
                            ,null house_win_co_web
                            ,null investments_num_ao_web
                            ,null investments_num_co_web
                            ,null turnover_ao_mobile
                            ,null turnover_co_mobile
                            ,null house_win_ao_mobile
                            ,null house_win_co_mobile
                            ,null investments_num_ao_mobile
                            ,null investments_num_co_mobile
                            ,null turnover_other
                            ,null house_win_other
                            ,null investments_num_other
                             
                            ,null bonuses
                      from   transactions t
                      where  t.type_id in (22, 23) --- very selective index
                      and    t.time_created >= p_fromdate
                      and    t.time_created < p_todate
                      group  by t.user_id
                      
                      union all
                      
                      select t.user_id
                             
                            ,null sum_deposits_ao_web
                            ,null sum_deposits_co_web
                            ,null sum_deposits_ao_mobile
                            ,null sum_deposits_co_mobile
                            ,null sum_deposits_other
                             
                            ,null fixbalance_ao_web
                            ,null fixbalance_co_web
                            ,null fixbalance_ao_mobile
                            ,null fixbalance_co_mobile
                            ,null fixbalance_other
                             
                            ,sum(case
                                   when t.writer_id in (1, 21000) then
                                    t.amount * t.rate / 100
                                   else
                                    0
                                 end) sum_withdraws_ao_web
                            ,sum(case
                                   when t.writer_id in (10000) then
                                    t.amount * t.rate / 100
                                   else
                                    0
                                 end) sum_withdraws_co_web
                            ,sum(case
                                   when t.writer_id in (200, 23000, 25000) then
                                    t.amount * t.rate / 100
                                   else
                                    0
                                 end) sum_withdraws_ao_mobile
                            ,sum(case
                                   when t.writer_id in (20000, 24000) then
                                    t.amount * t.rate / 100
                                   else
                                    0
                                 end) sum_withdraws_co_mobile
                            ,sum(case
                                   when t.writer_id not in (1, 10000, 200, 20000, 21000, 23000, 24000, 25000) then
                                    t.amount * t.rate / 100
                                   else
                                    0
                                 end) sum_withdraws_other
                            ,count(case
                                     when t.writer_id in (1, 21000)
                                          and t.type_id = 10
                                          and t.clearing_provider_id = 2
                                          and t.is_credit_withdrawal = 0 then
                                      1
                                   end) cft_withdraws_num_ao_web
                            ,count(case
                                     when t.writer_id in (10000)
                                          and t.type_id = 10
                                          and t.clearing_provider_id = 2
                                          and t.is_credit_withdrawal = 0 then
                                      1
                                   end) cft_withdraws_num_co_web
                            ,count(case
                                     when t.writer_id in (200, 23000, 25000)
                                          and t.type_id = 10
                                          and t.clearing_provider_id = 2
                                          and t.is_credit_withdrawal = 0 then
                                      1
                                   end) cft_withdraws_num_ao_mobile
                            ,count(case
                                     when t.writer_id in (20000, 24000)
                                          and t.type_id = 10
                                          and t.clearing_provider_id = 2
                                          and t.is_credit_withdrawal = 0 then
                                      1
                                   end) cft_withdraws_num_co_mobile
                            ,count(case
                                     when t.writer_id not in (1, 10000, 200, 20000, 21000, 23000, 24000, 25000)
                                          and t.type_id = 10
                                          and t.clearing_provider_id = 2
                                          and t.is_credit_withdrawal = 0 then
                                      1
                                   end) cft_withdraws_num_other
                            ,sum(case
                                   when t.writer_id in (1, 21000) then
                                    nvl(w.bank_fee_amount, 0) * t.rate
                                   else
                                    0
                                 end) wire_withdraws_fee_ao_web
                            ,sum(case
                                   when t.writer_id in (10000) then
                                    nvl(w.bank_fee_amount, 0) * t.rate
                                   else
                                    0
                                 end) wire_withdraws_fee_co_web
                            ,sum(case
                                   when t.writer_id in (200, 23000, 25000) then
                                    nvl(w.bank_fee_amount, 0) * t.rate
                                   else
                                    0
                                 end) wire_withdraws_fee_ao_mobile
                            ,sum(case
                                   when t.writer_id in (20000, 24000) then
                                    nvl(w.bank_fee_amount, 0) * t.rate
                                   else
                                    0
                                 end) wire_withdraws_fee_co_mobile
                            ,sum(case
                                   when t.writer_id not in (1, 10000, 200, 20000, 21000, 23000, 24000, 25000) then
                                    nvl(w.bank_fee_amount, 0) * t.rate
                                   else
                                    0
                                 end) wire_withdraws_fee_other
                             
                            ,null turnover_ao_web
                            ,null turnover_co_web
                            ,null house_win_ao_web
                            ,null house_win_co_web
                            ,null investments_num_ao_web
                            ,null investments_num_co_web
                            ,null turnover_ao_mobile
                            ,null turnover_co_mobile
                            ,null house_win_ao_mobile
                            ,null house_win_co_mobile
                            ,null investments_num_ao_mobile
                            ,null investments_num_co_mobile
                            ,null turnover_other
                            ,null house_win_other
                            ,null investments_num_other
                             
                            ,null bonuses
                      from   (select /*+ index(a,IDX$$_266C80002P) */
                               user_id, writer_id, amount, rate, wire_id, type_id, clearing_provider_id, is_credit_withdrawal
                              from   transactions a
                              where  time_created >= p_fromdate - 20
                              and    time_created < p_todate
                              and    time_settled - time_created <= 20
                              and    time_settled >= p_fromdate
                              and    time_settled < p_todate
                              and    type_id in (select id from transaction_types where class_type = 2)
                              and    status_id = 2
                              
                              union all
                              
                              select user_id, writer_id, amount, rate, wire_id, type_id, clearing_provider_id, is_credit_withdrawal
                              from   transactions
                              where  case
                                       when time_settled - time_created > 20 then
                                        time_settled
                                     end >= p_fromdate
                              and    case
                                      when time_settled - time_created > 20 then
                                       time_settled
                                    end < p_todate
                              and    type_id in (select id from transaction_types where class_type = 2)
                              and    status_id = 2) t
                      left   join wires w
                      on     w.id = t.wire_id
                      and    t.type_id = 14
                      group  by t.user_id
                      
                      union all
                      
                      select i.user_id
                             
                            ,null sum_deposits_ao_web
                            ,null sum_deposits_co_web
                            ,null sum_deposits_ao_mobile
                            ,null sum_deposits_co_mobile
                            ,null sum_deposits_other
                             
                            ,null fixbalance_ao_web
                            ,null fixbalance_co_web
                            ,null fixbalance_ao_mobile
                            ,null fixbalance_co_mobile
                            ,null fixbalance_other
                             
                            ,null sum_withdraws_ao_web
                            ,null sum_withdraws_co_web
                            ,null sum_withdraws_ao_mobile
                            ,null sum_withdraws_co_mobile
                            ,null sum_withdraws_other
                            ,null cft_withdraws_num_ao_web
                            ,null cft_withdraws_num_co_web
                            ,null cft_withdraws_num_ao_mobile
                            ,null cft_withdraws_num_co_mobile
                            ,null cft_withdraws_num_other
                            ,null wire_withdraws_fee_ao_web
                            ,null wire_withdraws_fee_co_web
                            ,null wire_withdraws_fee_ao_mobile
                            ,null wire_withdraws_fee_co_mobile
                            ,null wire_withdraws_fee_other
                             
                            ,sum(case
                                   when i.writer_id in (1, 21000) then
                                    case
                                      when i.is_canceled = 0 then
                                       i.amount * i.rate
                                      else
                                       -1 * i.amount * i.rate
                                    end
                                   else
                                    0
                                 end) / 100 turnover_ao_web
                            ,sum(case
                                   when i.writer_id in (10000) then
                                    case
                                      when i.is_canceled = 0 then
                                       i.amount * i.rate
                                      else
                                       -1 * i.amount * i.rate
                                    end
                                   else
                                    0
                                 end) / 100 turnover_co_web
                            ,sum(case
                                   when i.writer_id in (1, 21000) then
                                    case
                                      when i.is_canceled = 0 then
                                       i.house_result * i.rate
                                      else
                                       -1 * i.house_result * i.rate
                                    end
                                   else
                                    0
                                 end) / 100 house_win_ao_web
                            ,sum(case
                                   when i.writer_id in (10000) then
                                    case
                                      when i.is_canceled = 0 then
                                       i.house_result * i.rate
                                      else
                                       -1 * i.house_result * i.rate
                                    end
                                   else
                                    0
                                 end) / 100 house_win_co_web
                            ,sum(case
                                   when i.writer_id in (1, 21000) then
                                    case
                                      when i.is_canceled = 0 then
                                       1
                                      else
                                       -1
                                    end
                                   else
                                    0
                                 end) investments_num_ao_web
                            ,sum(case
                                   when i.writer_id in (10000) then
                                    case
                                      when i.is_canceled = 0 then
                                       1
                                      else
                                       -1
                                    end
                                   else
                                    0
                                 end) investments_num_co_web
                            ,sum(case
                                   when i.writer_id in (200, 23000, 25000) then
                                    case
                                      when i.is_canceled = 0 then
                                       i.amount * i.rate
                                      else
                                       -1 * i.amount * i.rate
                                    end
                                   else
                                    0
                                 end) / 100 turnover_ao_mobile
                            ,sum(case
                                   when i.writer_id in (20000, 24000) then
                                    case
                                      when i.is_canceled = 0 then
                                       i.amount * i.rate
                                      else
                                       -1 * i.amount * i.rate
                                    end
                                   else
                                    0
                                 end) / 100 turnover_co_mobile
                            ,sum(case
                                   when i.writer_id in (200, 23000, 25000) then
                                    case
                                      when i.is_canceled = 0 then
                                       i.house_result * i.rate
                                      else
                                       -1 * i.house_result * i.rate
                                    end
                                   else
                                    0
                                 end) / 100 house_win_ao_mobile
                            ,sum(case
                                   when i.writer_id in (20000, 24000) then
                                    case
                                      when i.is_canceled = 0 then
                                       i.house_result * i.rate
                                      else
                                       -1 * i.house_result * i.rate
                                    end
                                   else
                                    0
                                 end) / 100 house_win_co_mobile
                            ,sum(case
                                   when i.writer_id in (200, 23000, 25000) then
                                    case
                                      when i.is_canceled = 0 then
                                       1
                                      else
                                       -1
                                    end
                                   else
                                    0
                                 end) investments_num_ao_mobile
                            ,sum(case
                                   when i.writer_id in (20000, 24000) then
                                    case
                                      when i.is_canceled = 0 then
                                       1
                                      else
                                       -1
                                    end
                                   else
                                    0
                                 end) investments_num_co_mobile
                            ,sum(case
                                   when i.writer_id not in (1, 10000, 200, 20000, 21000, 23000, 24000, 25000) then
                                    case
                                      when i.is_canceled = 0 then
                                       i.amount * i.rate
                                      else
                                       -1 * i.amount * i.rate
                                    end
                                   else
                                    0
                                 end) / 100 turnover_other
                            ,sum(case
                                   when i.writer_id not in (1, 10000, 200, 20000, 21000, 23000, 24000, 25000) then
                                    case
                                      when i.is_canceled = 0 then
                                       i.house_result * i.rate
                                      else
                                       -1 * i.house_result * i.rate
                                    end
                                   else
                                    0
                                 end) / 100 house_win_other
                            ,sum(case
                                   when i.writer_id not in (1, 10000, 200, 20000, 21000, 23000, 24000, 25000) then
                                    case
                                      when i.is_canceled = 0 then
                                       1
                                      else
                                       -1
                                    end
                                   else
                                    0
                                 end) investments_num_other
                             
                            ,null bonuses
                      from   (select /*+ full(a) */
                               id, writer_id, is_canceled, house_result, rate, user_id, is_settled, amount
                              from   investments a
                              where  (time_created >= p_fromdate - 20 and time_created < p_todate)
                              and    ((time_settled >= p_fromdate and time_settled < p_todate) or
                                    (time_canceled >= p_fromdate and time_canceled < p_todate))
                              
                              union
                              
                              select id, writer_id, is_canceled, house_result, rate, user_id, is_settled, amount
                              from   investments
                              where  case
                                       when time_settled - time_created > 20 then
                                        time_settled
                                     end >= p_fromdate
                              and    case
                                      when time_settled - time_created > 20 then
                                       time_settled
                                    end < p_todate
                              and    ((time_settled >= p_fromdate and time_settled < p_todate) or
                                    (time_canceled >= p_fromdate and time_canceled < p_todate))
                              
                              union
                              
                              select id, writer_id, is_canceled, house_result, rate, user_id, is_settled, amount
                              from   investments
                              where  case
                                       when time_canceled - time_created > 20 then
                                        time_canceled
                                     end >= p_fromdate
                              and    case
                                      when time_canceled - time_created > 20 then
                                       time_canceled
                                    end < p_todate
                              and    ((time_settled >= p_fromdate and time_settled < p_todate) or
                                    (time_canceled >= p_fromdate and time_canceled < p_todate))) i
                      where  i.is_settled = 1
                      group  by i.user_id
                      
                      union all
                      
                      select /*+ use_nl(bu,i) use_nl(bu,t) */
                       bu.user_id
                       
                      ,null sum_deposits_ao_web
                      ,null sum_deposits_co_web
                      ,null sum_deposits_ao_mobile
                      ,null sum_deposits_co_mobile
                      ,null sum_deposits_other
                       
                      ,null fixbalance_ao_web
                      ,null fixbalance_co_web
                      ,null fixbalance_ao_mobile
                      ,null fixbalance_co_mobile
                      ,null fixbalance_other
                       
                      ,null sum_withdraws_ao_web
                      ,null sum_withdraws_co_web
                      ,null sum_withdraws_ao_mobile
                      ,null sum_withdraws_co_mobile
                      ,null sum_withdraws_other
                      ,null cft_withdraws_num_ao_web
                      ,null cft_withdraws_num_co_web
                      ,null cft_withdraws_num_ao_mobile
                      ,null cft_withdraws_num_co_mobile
                      ,null cft_withdraws_num_other
                      ,null wire_withdraws_fee_ao_web
                      ,null wire_withdraws_fee_co_web
                      ,null wire_withdraws_fee_ao_mobile
                      ,null wire_withdraws_fee_co_mobile
                      ,null wire_withdraws_fee_other
                       
                      ,null turnover_ao_web
                      ,null turnover_co_web
                      ,null house_win_ao_web
                      ,null house_win_co_web
                      ,null investments_num_ao_web
                      ,null investments_num_co_web
                      ,null turnover_ao_mobile
                      ,null turnover_co_mobile
                      ,null house_win_ao_mobile
                      ,null house_win_co_mobile
                      ,null investments_num_ao_mobile
                      ,null investments_num_co_mobile
                      ,null turnover_other
                      ,null house_win_other
                      ,null investments_num_other
                       
                      ,sum(case
                             when bt.class_type_id = 3 then
                              nvl(t.amount, 0) * t.rate -- Next invest on us
                             else
                              bu.bonus_amount * t.rate -- Not next invest on us
                           end) / 100 bonuses
                      from   bonus_users bu
                      join   bonus_types bt
                      on     bt.id = bu.type_id
                      left   join investments i
                      on     i.bonus_user_id = bu.id
                      left   join transactions t
                      on     t.bonus_user_id = bu.id
                      and    t.type_id = 12
                      where  ((bu.time_used >= p_fromdate and bu.time_used < p_todate) or
                             (bu.time_used is null and bu.time_done >= p_fromdate and bu.time_done < p_todate))
                      and    not (bt.class_type_id = 3 and i.bonus_odds_change_type_id = 1 and i.win > i.lose)
                      group  by bu.user_id)
              group  by user_id) g
      join   users u
      on     u.id = g.user_id
      left   join transactions h
      on     h.id = u.first_deposit_id
      where  u.affiliate_system_user_time is not null
      and    u.class_id != 0
      and    u.affiliate_system = 1)
    
    select *
    from   (select customerid
                  ,p_fromdate activity_date
                  ,'anyoption' product_id
                  ,0 bonuses
                  ,case
                     when nvl(fd_platform_id, reg_platform_id) = reg_platform_id
                          and reg_platform_id = c_platform_ao then
                      adjustments_ao_web
                     else
                      0
                   end adjustments
                  ,case
                     when nvl(fd_platform_id, reg_platform_id) = reg_platform_id
                          and reg_platform_id = c_platform_ao then
                      sum_deposits_ao_web
                     else
                      0
                   end sum_deposits
                  ,case
                     when nvl(fd_platform_id, reg_platform_id) = reg_platform_id
                          and reg_platform_id = c_platform_ao then
                      gross_rev_ao_web
                     else
                      0
                   end gross_rev
                  ,case
                     when nvl(fd_platform_id, reg_platform_id) = reg_platform_id
                          and reg_platform_id = c_platform_ao then
                      turnover_ao_web
                     else
                      0
                   end turnover
                  ,case
                     when nvl(fd_platform_id, reg_platform_id) = reg_platform_id
                          and reg_platform_id = c_platform_ao then
                      sum_withdraws_ao_web
                     else
                      0
                   end sum_withdraws
                  ,case
                     when nvl(fd_platform_id, reg_platform_id) = reg_platform_id
                          and reg_platform_id = c_platform_ao then
                      investments_num_ao_web
                     else
                      0
                   end investments_num
                  ,brand_id
                  ,fd_platform_id
                  ,reg_platform_id
                  ,'web' origin
            from   data
            union all
            select customerid
                  ,p_fromdate
                  ,'anyoption' product_id
                  ,0 bonuses
                  ,case
                     when nvl(fd_platform_id, reg_platform_id) = reg_platform_id
                          and reg_platform_id = c_platform_ao then
                      adjustments_ao_mobile
                     else
                      0
                   end
                  ,case
                     when nvl(fd_platform_id, reg_platform_id) = reg_platform_id
                          and reg_platform_id = c_platform_ao then
                      sum_deposits_ao_mobile
                     else
                      0
                   end
                  ,case
                     when nvl(fd_platform_id, reg_platform_id) = reg_platform_id
                          and reg_platform_id = c_platform_ao then
                      gross_rev_ao_mobile
                     else
                      0
                   end
                  ,case
                     when nvl(fd_platform_id, reg_platform_id) = reg_platform_id
                          and reg_platform_id = c_platform_ao then
                      turnover_ao_mobile
                     else
                      0
                   end
                  ,case
                     when nvl(fd_platform_id, reg_platform_id) = reg_platform_id
                          and reg_platform_id = c_platform_ao then
                      sum_withdraws_ao_mobile
                     else
                      0
                   end
                  ,case
                     when nvl(fd_platform_id, reg_platform_id) = reg_platform_id
                          and reg_platform_id = c_platform_ao then
                      investments_num_ao_mobile
                     else
                      0
                   end
                  ,brand_id
                  ,fd_platform_id
                  ,reg_platform_id
                  ,'mobile' origin
            from   data
            union all
            select customerid
                  ,p_fromdate activity_date
                  ,'anyoption' product_id
                  ,0 bonuses
                  ,case
                     when nvl(fd_platform_id, reg_platform_id) = reg_platform_id
                          and reg_platform_id = c_platform_co then
                      adjustments_co_web
                     else
                      0
                   end adjustments_co
                  ,case
                     when nvl(fd_platform_id, reg_platform_id) = reg_platform_id
                          and reg_platform_id = c_platform_co then
                      sum_deposits_co_web
                     else
                      0
                   end sum_deposits_co
                  ,case
                     when nvl(fd_platform_id, reg_platform_id) = reg_platform_id
                          and reg_platform_id = c_platform_co then
                      gross_rev_co_web
                     else
                      0
                   end gross_rev_co
                  ,case
                     when nvl(fd_platform_id, reg_platform_id) = reg_platform_id
                          and reg_platform_id = c_platform_co then
                      turnover_co_web
                     else
                      0
                   end turnover_co
                  ,case
                     when nvl(fd_platform_id, reg_platform_id) = reg_platform_id
                          and reg_platform_id = c_platform_co then
                      sum_withdraws_co_web
                     else
                      0
                   end sum_withdraws_co
                  ,case
                     when nvl(fd_platform_id, reg_platform_id) = reg_platform_id
                          and reg_platform_id = c_platform_co then
                      investments_num_co_web
                     else
                      0
                   end investments_num_co
                  ,brand_id
                  ,fd_platform_id
                  ,reg_platform_id
                  ,'web' origin
            from   data
            union all
            select customerid
                  ,p_fromdate
                  ,'anyoption' product_id
                  ,0 bonuses
                  ,case
                     when nvl(fd_platform_id, reg_platform_id) = reg_platform_id
                          and reg_platform_id = c_platform_co then
                      adjustments_co_mobile
                     else
                      0
                   end
                  ,case
                     when nvl(fd_platform_id, reg_platform_id) = reg_platform_id
                          and reg_platform_id = c_platform_co then
                      sum_deposits_co_mobile
                     else
                      0
                   end
                  ,case
                     when nvl(fd_platform_id, reg_platform_id) = reg_platform_id
                          and reg_platform_id = c_platform_co then
                      gross_rev_co_mobile
                     else
                      0
                   end
                  ,case
                     when nvl(fd_platform_id, reg_platform_id) = reg_platform_id
                          and reg_platform_id = c_platform_co then
                      turnover_co_mobile
                     else
                      0
                   end
                  ,case
                     when nvl(fd_platform_id, reg_platform_id) = reg_platform_id
                          and reg_platform_id = c_platform_co then
                      sum_withdraws_co_mobile
                     else
                      0
                   end
                  ,case
                     when nvl(fd_platform_id, reg_platform_id) = reg_platform_id
                          and reg_platform_id = c_platform_co then
                      investments_num_co_mobile
                     else
                      0
                   end
                  ,brand_id
                  ,fd_platform_id
                  ,reg_platform_id
                  ,'mobile' origin
            from   data
            union all
            select customerid
                  ,p_fromdate
                  ,'anyoption' product_id
                  ,bonuses
                  ,adjustments_other
                  ,sum_deposits_other
                  ,gross_rev_other
                  ,turnover_other
                  ,sum_withdraws_other
                  ,investments_num_other
                  ,brand_id
                  ,fd_platform_id
                  ,reg_platform_id
                  ,'other' origin
            from   data)
    where  not (adjustments = 0 and sum_deposits = 0 and gross_rev = 0 and turnover = 0 and sum_withdraws = 0 and investments_num = 0)
    -- or     nvl(fd_platform_id, reg_platform_id) <> reg_platform_id      
    order  by customerid, origin;

  function nref_activity(i_date in date default null) return string_table
    pipelined is
    l_fromdate date := trunc(sysdate - 1 / 24);
    l_todate   date := l_fromdate + 1;
  begin
    if i_date is not null
    then
      l_fromdate := i_date;
      l_todate   := l_fromdate + 1;
    end if;
  
    pipe row('customerID,Activity_Date,Product_ID,Bonuses,Adjustments,Deposits,Gross_Revenue,Turnover,payouts,Transactions,BrandID,Origin');
  
    for i in (select * from table(nref_activity(l_fromdate, l_todate)))
    loop
      pipe row(i.customerid || ',' || to_char(i.activity_date, 'yyyy-mm-dd') || ',' || i.product_id || ',' || i.bonuses || ',' ||
               i.adjustments || ',' || i.sum_deposits || ',' || i.gross_rev || ',' || i.turnover || ',' || i.sum_withdraws || ',' ||
               i.investments_num || ',' || i.brand_id || ',' || i.origin);
    end loop;
  
    return;
  end nref_activity;

  function nref_activity
  (
    p_fromdate in date
   ,p_todate   in date
  ) return t_activity_rec_tab
    pipelined is
    l_data t_activity_rec_tab;
  begin
    open cr_activity_data(p_fromdate, p_todate);
    loop
      fetch cr_activity_data bulk collect
        into l_data limit 100;
    
      for i in 1 .. l_data.count
      loop
        pipe row(l_data(i));
      end loop;
    
      exit when cr_activity_data%notfound;
    end loop;
  
    close cr_activity_data;
  
    return;
  exception
    when no_data_needed then
      close cr_activity_data;
  end nref_activity;

  function user_report(i_date in date default null) return string_table
    pipelined is
    l_fromdate date := trunc(sysdate - 1 / 24);
    l_todate   date := l_fromdate + 1;
  begin
    if i_date is not null
    then
      l_fromdate := i_date;
      l_todate   := l_fromdate + 1;
    end if;
  
    pipe row('CustomerID,Country_ID,Tag,Registration_ip,Registration_Date,CustomerTypeID,BrandID,Origin');
  
    for i in (select * from table(user_report(l_fromdate, l_todate)))
    loop
      pipe row(i.customerid || ',' || i.country_id || ',' || i.tag || ',' || i.registration_ip || ',' ||
               to_char(i.registration_date, 'yyyy-mm-dd') || ',' || i.customertypeid || ',' || i.brand_id || ',' || i.origin);
    end loop;
  
    return;
  end user_report;

  function user_report
  (
    p_fromdate in date
   ,p_todate   in date
  ) return t_userrep_rec_tab
    pipelined is
    l_data t_userrep_rec_tab;
  begin
    open cr_user_report(p_fromdate, p_todate);
    loop
      fetch cr_user_report bulk collect
        into l_data limit 100;
    
      for i in 1 .. l_data.count
      loop
        pipe row(l_data(i));
      end loop;
    
      exit when cr_user_report%notfound;
    end loop;
  
    close cr_user_report;
  
    return;
  exception
    when no_data_needed then
      close cr_user_report;
  end user_report;

end pkg_netrefer_reports;
/
