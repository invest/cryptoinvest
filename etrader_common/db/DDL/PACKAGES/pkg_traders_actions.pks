create or replace
PACKAGE PKG_TRADERS_ACTIONS AS 

 procedure load_filters
  (
    o_traders           out sys_refcursor
   ,o_actions           out sys_refcursor
  );
  
  procedure GET_TRADERS_ACTIONS
  (
   o_traders_actions        out sys_refcursor
   ,i_fromdate              in date
   ,i_todate                in date
   ,i_trader_id             in number
   ,i_action_command_id     in number
   ,i_asset_id              in number
   ,i_page_number           in number
   ,i_rows_per_page         in number
   ,i_order_column          in number
   ,i_order_type            in number
   ,i_commands              in number_table
  );


END PKG_TRADERS_ACTIONS;
/

create or replace
PACKAGE BODY         PKG_TRADERS_ACTIONS AS

  procedure load_filters
  (
    o_traders           out sys_refcursor
   ,o_actions           out sys_refcursor
  ) is
  BEGIN
    open o_traders for
      select w.id id
      , w.user_name value
      from writers w
      where w.id in (select writer_id from log)
            and w.id <> 0;
  
    open o_actions for
      SELECT 
            CODE ID,
            VALUE VALUE 
      FROM 
            ENUMERATORS 
      WHERE 
            ENUMERATOR = 'log_commands' 
            AND CODE IN (7, 8, 24, 25, 26, 27, 28);

  END load_filters;

  procedure GET_TRADERS_ACTIONS
  (
    o_traders_actions       out sys_refcursor
   ,i_fromdate              in date
   ,i_todate                in date
   ,i_trader_id             in number
   ,i_action_command_id     in number
   ,i_asset_id              in number
   ,i_page_number           in number
   ,i_rows_per_page         in number
   ,i_order_column          in number
   ,i_order_type            in number
   ,i_commands              in number_table
  ) is
  BEGIN
    
    if i_todate - i_fromdate > 32
    then
      raise_application_error(-20000, 'Period is larger than a month');
    end if;
    
    open o_traders_actions for
        SELECT *
          FROM (SELECT A.*
                      ,rownum rn
                  FROM (SELECT l.id,l.writer_id trader_id
                              ,l.time_created
                              ,l.description action
                              ,l.table_name
                              ,l.key_value asset_id
                              ,l.command action_id
                              ,w.id writer_id
                              ,w.user_name user_name
                              ,m.name
                              ,count(*) over() total_count
                          FROM log l
                              ,writers w
                              ,markets m
                         WHERE w.id = l.writer_id
                           AND l.writer_id <> 0
                           AND m.id = (case
                                         when (l.table_name = 'markets') then l.key_value
                                         when (l.table_name = 'investment_limits') then l.key_value
                                         when l.table_name = 'opportunities' then
                                           (select o.market_id
                                              from opportunities o
                                             where o.id = l.key_value
                                           )
                                         end
                                      )
                           AND l.command member of i_commands
                           AND l.time_created >= i_fromdate  
                           AND l.time_created < i_todate
                           AND (i_trader_id is null or w.id = i_trader_id)
                           AND (i_action_command_id is null or l.command = i_action_command_id)
                           AND (i_asset_id is null 
                                OR i_asset_id = l.key_value 
                                OR i_asset_id = (select o.market_id
                                                    from opportunities o
                                                 where o.id = l.key_value) 
                                )
                         ORDER BY case when i_order_column = 1 and i_order_type = 1 then w.user_name end ASC,
                                  case when i_order_column = 1 and i_order_type = -1 then w.user_name end DESC,
                                  case when i_order_column = 2 and i_order_type = 1 then l.time_created end ASC,
                                  case when i_order_column = 2 and i_order_type = -1 then l.time_created end DESC,
                                  case when i_order_column = 3 and i_order_type = 1 then l.description end ASC,
                                  case when i_order_column = 3 and i_order_type = -1 then l.description end DESC,
                                  case when i_order_column = 4 and i_order_type = 1 then m.name end ASC,
                                  case when i_order_column = 4 and i_order_type = -1 then m.name end DESC
                       ) A
                 WHERE rownum <= i_page_number * i_rows_per_page
               ) B
         WHERE rn > (i_page_number - 1) * i_rows_per_page;
        
  END get_traders_actions;

END PKG_TRADERS_ACTIONS;
/