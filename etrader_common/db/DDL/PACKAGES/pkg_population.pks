CREATE OR REPLACE PACKAGE pkg_population IS

  PROCEDURE update_population_entry_tmp;

  PROCEDURE get_population_entry_tmp
  (
    o_population_entry_tmp OUT SYS_REFCURSOR
   ,i_min                  IN NUMBER
   ,i_max                  IN NUMBER
  );

END pkg_population;
/

CREATE OR REPLACE PACKAGE BODY pkg_population IS

  c_no_activity_months CONSTANT NUMBER := 3;
  c_no_issue_days      CONSTANT NUMBER := 30;

  PROCEDURE update_population_entry_tmp IS
  BEGIN
    -- empty table before insert
    EXECUTE IMMEDIATE 'truncate table population_entry_tmp';
  
    INSERT INTO population_entry_tmp
      SELECT rownum                      AS curr_row
            ,pu.id                       AS population_users_id
            ,pu.user_id
            ,pu.contact_id
            ,pu.curr_assigned_writer_id
            ,pu.curr_population_entry_id
            ,pu.entry_type_id
            ,pu.entry_type_action_id
            ,pu.lock_history_id
            ,pu.delay_id
            ,pu.last_sales_dep_rep_id
            ,pu.first_his_id
            ,pu.time_created
            ,pu.assign_backup
            ,pu.time_control
            ,pu.assign_type_id
            ,pu.time_assign
            ,pe.population_id
            ,pe.group_id
            ,pe.qualification_time
            ,pe.is_displayed
            ,pe.base_qualification_time
            ,p.population_type_id
            ,p.skin_id
            ,u.class_id
            ,u.user_name
      FROM users              u
          ,population_users   pu
          ,population_entries pe
          ,populations        p
          ,population_types   pt
      WHERE u.id = pu.user_id
      AND pe.id = pu.curr_population_entry_id
      AND pe.population_id = p.id
      AND p.population_type_id = pt.id
      AND ((pt.type_id IN (2, 3) AND pt.id <> 6) OR
            (pt.id = 6 AND u.first_deposit_id IS NOT NULL))
      AND pu.curr_assigned_writer_id IS NOT NULL
      AND pu.lock_history_id IS NULL
      AND -- not lock user
            ((NOT EXISTS ( -- pass 3 months since user was assigned
                          SELECT 1
                          FROM population_entries_hist peh
                               ,population_entries      pe1
                          WHERE pu.id = pe1.population_users_id
                          AND pe1.id = peh.population_entry_id
                          AND peh.status_id = 2
                          AND peh.time_created >
                                add_months(SYSDATE, -c_no_activity_months)) AND
             NOT EXISTS ( -- no new investment in last 3 month
                          SELECT 1
                          FROM investments i
                          WHERE i.time_created >
                                add_months(SYSDATE, -c_no_activity_months)
                          AND u.id = i.user_id) AND NOT EXISTS
             ( --real desposit AND withdraw  last 3 month
               SELECT 1
               FROM transactions      t
                    ,transaction_types tt
               WHERE t.type_id = tt.id
               AND tt.class_type IN (1, 2)
               AND --real desposit and withdraw
                     t.time_created >
                     add_months(SYSDATE, -c_no_activity_months)
               AND u.id = t.user_id)) OR
            (NOT EXISTS ( -- pass 3 months since user was assigned
                          SELECT 1
                          FROM population_entries_hist peh
                               ,population_entries      pe1
                          WHERE pu.id = pe1.population_users_id
                          AND pe1.id = peh.population_entry_id
                          AND peh.status_id = 2
                          AND peh.time_created > SYSDATE - c_no_issue_days) AND
             NOT EXISTS
             (SELECT 1
               FROM issues             i
                   ,issue_actions      ia
                   ,issue_action_types iat
               WHERE i.id = ia.issue_id
               AND ia.issue_action_type_id = iat.id
               AND iat.channel_id IN (1, 3)
               AND ia.action_time > SYSDATE - c_no_issue_days
               AND u.id = i.user_id
               AND ia.writer_id = pu.curr_assigned_writer_id)));
  
  END update_population_entry_tmp;

  PROCEDURE get_population_entry_tmp
  (
    o_population_entry_tmp OUT SYS_REFCURSOR
   ,i_min                  IN NUMBER
   ,i_max                  IN NUMBER
  ) IS
  BEGIN
  
    OPEN o_population_entry_tmp FOR
      SELECT tmp.*
      FROM population_entry_tmp tmp
      WHERE tmp.curr_row >= i_min
      AND tmp.curr_row < i_max
      ORDER BY curr_row;
  
  END get_population_entry_tmp;
END pkg_population;

/
