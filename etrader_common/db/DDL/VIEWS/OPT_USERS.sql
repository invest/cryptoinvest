create or replace view opt_users as
select u.first_name
      ,u.last_name
      ,u.zip_code
      ,u.writer_id
      ,withdraw_alert
      ,win_lose
      ,utc_offset_modified
      ,utc_offset_created
      ,u.utc_offset
      ,user_agent
      ,tip
      ,time_sc_turnover
      ,time_sc_manualy
      ,time_sc_house_result
      ,time_modified
      ,time_last_login
      ,time_first_visit
      ,time_first_authorized
      ,u.time_created
      ,u.time_updated
      ,u.time_birth_date
      ,tier_user_id
      ,tag
      ,sub_affiliate_key
      ,status_id
      ,state_code
      ,special_code
      ,u.skin_id
      ,sc_updated_by
      /*,rollup_fix_tax_balance
      ,rollup_fix_balance*/
      ,reference_id
      ,rank_id
      ,mid
      ,liveperson_session_key
      ,limit_id
      ,u.last_failed_time
      ,last_decline_date
      ,language_id
      ,keyword
      ,is_vip
      ,is_stop_reverse_withdraw
      ,is_risky
      ,is_next_invest_on_us
      ,is_netrefer_user
      ,is_immediate_withdraw
      ,is_false_account
      ,is_declined
      ,is_contact_for_daa
      ,is_contact_by_sms
      ,is_contact_by_phone
      ,is_contact_by_email
      ,is_authorized_mail
      ,u.is_active
      ,is_accepted_terms
      ,ip
      ,id_doc_verify
      ,u.id
      ,http_referer
      ,gender
      ,first_deposit_id
      ,files_risk_status_id
      ,u.failed_count
      ,u.dynamic_param
      ,docs_required
      ,dfa_placement_id
      ,dfa_macro
      ,dfa_creative_id
      ,device_unique_id
      ,dep_no_inv_24h
      ,decoded_source_query
      ,currency_id
      ,country_id
      ,u.contact_id
      ,company_name
      ,company_id
      ,u.comments
      ,combination_id
      ,click_id
      ,class_id
      ,city_name
      ,u.city_id
      ,u.chat_id
      ,bonus_abuser
      ,balance
      ,NVL(bu.bonus_amount-bu.bonus_winnings,0) as bonus_balance
      ,(u.balance-NVL(bu.bonus_amount,0)) as cash_balance
      ,background
      ,authorized_mail_refused_times
      ,affiliate_system_user_time
      ,affiliate_system
      ,affiliate_key
      ,u.email
      ,c.phone_code || u.mobile_phone as phone_number                  --1
      ,case
          when ur.suspended_reason_id = 0 or ur.user_id is null then 0
          else 1
       end as suspend                                                  --2
      ,case
          when u.is_active = 1 then '0'
          when ia.issue_action_type_id = 39 then '1'
          else '2'
       end is_closed_flag                                              --3
      ,mc.name campaign_name
      ,w.user_name campaign_manager                                ----4+5
      ,wr.user_name portfolio_manager                                  --6
      ,uvs.name vip_status
      ,case when poi.id is not null then 1 else 0 end as oploaded
      ,poi.days_to_poi
      ,case
         when xp.xmas_email is null then 0
         else 1
       end xmas_registered                                             --7
      ,rawtohex(u.email) email_encrypt
      ,case when pc.user_id is not null then 1 else 0 end as positive_contact_within_30_d
      ,ai.annual_income
      ,trading_experience
  from etrader.users u
  join etrader.users_active_data uad on uad.user_id = u.id
  join etrader.users_vip_status uvs on uvs.id = uad.vip_status_id
  join etrader.countries c on u.country_id = c.id                              --1
  left join etrader.users_regulation ur on u.id = ur.user_id                   --2
  left join (select u.id user_id, max (ia.id) maxi
               from etrader.users u
                    join etrader.issues i on u.id = i.user_id
                    join etrader.issue_actions ia on i.id = ia.issue_id
                    join etrader.issue_action_types iat on ia.issue_action_type_id = iat.id
              where u.is_active = 0 and iat.is_close_account = 1
              group by u.id
            ) ii on u.id = ii.user_id
  left join etrader.issue_actions ia on ii.maxi = ia.id              -------------3
  left join etrader.marketing_combinations mco on u.combination_id = mco.id ----4+5
  left join etrader.marketing_campaigns mc on mco.campaign_id = mc.id       ----4+5
  left join etrader.writers w on mc.writer_id = w.id            ----------------4+5
  left join etrader.population_users pu on u.id = pu.user_id                    --6
  left join etrader.writers wr on pu.curr_assigned_writer_id = wr.id            --6
  left join etrader_anal.xmas_promo xp on u.email = xp.xmas_email       --7
  left join (select u.id
                   ,round(min(f.time_uploaded) - ftd.time_created) days_to_poi
               from etrader.users u
               join etrader.users_ftds_bi ftd on u.id = ftd.user_id
               join etrader.files f on u.id = f.user_id
              where file_type_id in (1,2,22)
                and is_current = 1
                and file_name IS NOT NULL
              group by u.id
                      ,ftd.time_created
            ) poi on u.id = poi.id
  left join (select distinct i.user_id
               from etrader.issues i
               join etrader.issue_actions ia on i.id = ia.issue_id
               join etrader.issue_action_types iat on ia.issue_action_type_id = iat.id
              where iat.deposit_search_type in (1,2)
                and (sysdate - ia.action_time) <= 30
            ) pc on u.id = pc.user_id
  left join (select u.id
                   ,case
                      when ua.answer_id = 321 then 'Up to Ђ20,000'
                      when ua.answer_id = 322 then 'Ђ20,001-Ђ50,000'
                      when ua.answer_id = 323 then 'Ђ50,001-Ђ100,000'
                      when ua.answer_id = 324 then 'More then Ђ100.000'
                      else null
                    end as annual_income
               from etrader.users u
               join etrader.qm_user_answers ua on u.id = ua.user_id
              where ua.answer_id in (321,322,323,324)
            ) ai on u.id = ai.id
  left join (select u.id
                   ,case
                      when ua.answer_id=366 then 'None'
                      when ua.answer_id=367 then 'Less than a year'
                      when ua.answer_id=368 then '1 to 3 years'
                      when ua.answer_id=369 then 'More than 3 years'
                      when ua.answer_id=370 then 'Professional financial experience'
                      else null
                    end as trading_experience
               from etrader.users u
               join etrader.qm_user_answers ua on u.id = ua.user_id
              where ua.answer_id in (366,367,368,369,370)
            ) te on u.id = te.id
  left join (select user_id
                   ,sum(bu.adjusted_amount) as bonus_amount
                   ,case 
                      when sum(NVL(bu.adjusted_amount,0) - NVL(bu.bonus_amount,0)) > 0
                         then sum(NVL(bu.adjusted_amount,0) - NVL(bu.bonus_amount,0))
                      else 0
                    end as bonus_winnings
               from etrader.bonus_users bu
              where bu.bonus_state_id IN (1,2,3)
              group by user_id
            ) bu on u.id = bu.user_id
;
