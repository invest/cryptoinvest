CREATE OR REPLACE FORCE VIEW "ETRADER"."CP_ACCOUNTS" ("ACCOUNTID", "FIRSTNAME", "LASTNAME", "ISDEMO", "BASECURRENCY", "AGENTID", "AGENTNAME", "BALANCE", "OPENDATE", "HAS_REQUEST_CALLBACK", "CONTROL_APPROVED", "QUESTIONNAIRE_MANDATORY_DONE", "SKIN_NAME", "ENTRY_ID", "USERS_UPLOADED_POI") AS 
  select u.id accountid
      ,u.first_name firstname
      ,u.last_name lastname
      ,case
         when u.class_id = 0 then
          1
         else
          0
       end isdemo
      ,c.code basecurrency
      ,w.id agentid
      ,w.user_name agentname
      ,u.balance / 100 balance
      ,u.time_created opendate
      ,(select count(*)
        from   population_entries pe
        join   populations p
        on     p.id = pe.population_id
        where  pe.id = pu.curr_population_entry_id
        and    p.population_type_id = 6
        and    pu.curr_assigned_writer_id is not null) has_request_callback
      ,(select count(*)
        from   users_regulation_history
        where  approved_regulation_step = 7
        and    user_id = u.id
        and    rownum <= 1) control_approved
      ,(select max(time_created)
       from   users_regulation_history
       where  approved_regulation_step = 3
       and    time_created > sysdate - 14
       and    user_id = u.id
       and    rownum <= 1) questionnaire_mandatory_done,
       s.name skin_name,
       pu.curr_population_entry_id entry_id,
       nvl ((select distinct 1
                    from files
                    where user_id = u.id
                    and file_type_id in (1,2)
                    and file_name is not null
                   ), 0) users_uploaded_poi 
from   users u
join   currencies c
on     c.id = u.currency_id
join   skins s
on     s.id = u.SKIN_ID
left   join population_users pu
on     pu.user_id = u.id
left   join writers w
on     w.id = pu.curr_assigned_writer_id;