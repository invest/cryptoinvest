
create materialized view m_fee 
  build deferred
  refresh force on demand 
as
select *
  from (select t.id,
               t.balance,
               t.amount,
               t.tax_balance,
               t.currency_id,
               t.utc_offset
          from (select u.id,
                       u.balance,
                       f.amount,
                       u.tax_balance,
                       u.currency_id,
                       u.utc_offset,
                       ia.issue_action_type_id,
                       rank () over (partition by i.user_id order by ia.action_time desc) rnk
                  from users u,
                       issues i,
                       issue_actions ia,
                       fee_currency_map f
                 where     u.is_active = 0
                       and f.fee_id = 1
                       and u.currency_id = f.currency_id
                       and u.balance > 0
                       and u.class_id <> 0
                       and u.skin_id <> 1
                       and u.time_created < add_months(sysdate, -5)
                       and i.user_id = u.id
                       and ia.issue_id = i.id
                       and ia.issue_action_type_id in (12, 39)) t
         where t.rnk = 1 and t.issue_action_type_id = 39         
        union all       
        select u.id,
               u.balance,
               f.amount,
               u.tax_balance,
               u.currency_id,
               u.utc_offset
          from users u, fee_currency_map f
         where     u.class_id <> 0
         	   and u.skin_id <> 1
               and u.is_active = 1
               and f.fee_id = 1
               and u.balance > 0
               and u.time_created < add_months(sysdate, -6)
               and u.currency_id = f.currency_id) z
 where   not exists
                (select 1
                   from investments i
                  where     i.time_created >= add_months(sysdate, -6)
                        and z.id = i.user_id)
       and not exists
                  (select 1
                     from transactions t
                    where     t.time_created >= add_months(sysdate, -1)
                          and z.id = t.user_id
                          and t.type_id = 47)
       and not exists
                  (select 1
                     from transactions t, transaction_types tt
                    where     t.type_id = tt.id
                          and tt.class_type = 2
                          and t.status_id = 4
                          and z.id = t.user_id)
       and not exists
                  (select 1
                     from transactions t, transaction_types tt
                    where     t.type_id = tt.id
                          and tt.class_type = 1
                          and t.status_id in (2, 7)
                          and t.time_created > sysdate - 7
                          and z.id = t.user_id);

comment on materialized view m_fee  is 'snapshot table for snapshot ETRADER.M_FEE';
