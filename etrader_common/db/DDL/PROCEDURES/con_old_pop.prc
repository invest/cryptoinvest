CREATE OR REPLACE /* Formatted on 3.12.2013 �. 11:27:52 (QP5 v5.149.1003.31008) */
PROCEDURE CON_OLD_POP
AS
   sum_of_pop_convert   NUMBER := 0;
BEGIN
   DBMS_OUTPUT.PUT_LINE ('------------------------------------------------:');
   DBMS_OUTPUT.PUT_LINE ('CON_OLD_POP started at: ' || SYSDATE);

   FOR v_entry
      IN (SELECT p2.id new_pop_id, tab.entry_id AS entry_to_update
            FROM populations p2,
                 (SELECT pe.id AS entry_id, p.skin_id
                    FROM population_entries pe, populations p
                   WHERE pe.population_id = p.id
                         AND p.population_type_id IN (14, 15, 16, 17, 18, 19)) tab
           WHERE tab.skin_id = p2.skin_id AND p2.population_type_id = 23)
   LOOP
      sum_of_pop_convert := sum_of_pop_convert + 1;

      UPDATE population_entries
         SET population_id = v_entry.new_pop_id
       WHERE id = v_entry.entry_to_update;
   END LOOP;

   DBMS_OUTPUT.PUT_LINE ('------------------------------------------------:');
   DBMS_OUTPUT.PUT_LINE ('CON_OLD_POP finished at: ' || SYSDATE);
   DBMS_OUTPUT.PUT_LINE ('convert: ' || sum_of_pop_convert);
   NULL;
END CON_OLD_POP;
/
