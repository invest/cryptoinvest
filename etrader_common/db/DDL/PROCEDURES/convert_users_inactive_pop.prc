CREATE OR REPLACE /* Formatted on 3.12.2013 �. 11:27:52 (QP5 v5.149.1003.31008) */
PROCEDURE CONVERT_USERS_INACTIVE_POP
AS
   POP_RETENTION_ID        NUMBER := 23;
   populationEntryId       NUMBER := 0;
   populationId            NUMBER := 0;
   populationEntryHistId   NUMBER := 0;
   users_converted         NUMBER := 0;
BEGIN
   DBMS_OUTPUT.PUT_LINE ('------------------------------------------------:');
   DBMS_OUTPUT.
    PUT_LINE ('CONVERT_USERS_INACTIVE_POP started at: ' || SYSDATE);

   FOR v_user
      IN (SELECT u.id AS user_id,
                 pu.id AS population_user_id,
                 pe.id AS population_entry_id,
                 pu.curr_assigned_writer_id AS assigned_writer,
                 pe.qualification_time AS entry_qualification_time,
                 u.skin_id AS user_skin_id,
                 u.first_deposit_id AS first_deposit_id,
                 pu.entry_type_id AS entry_type_id
            FROM population_users pu,
                 population_entries pe,
                 populations p,
                 users u
           WHERE     pu.user_id = u.id
                 AND pu.curr_population_entry_id = pe.id
                 AND pe.population_id = p.id
                 AND pu.curr_assigned_writer_id IS NOT NULL
                 AND pu.curr_population_entry_id IS NULL
                 AND pu.user_id IS NOT NULL)
   LOOP
      SELECT p.id
        INTO populationId
        FROM populations p, population_types pt
       WHERE     pt.id = p.population_type_id
             AND p.skin_id = v_user.user_skin_id
             AND p.population_type_id = POP_RETENTION_ID;

      IF (v_user.population_user_id > 0 AND populationId > 0)
      THEN
         IF (v_user.first_deposit_id IS NOT NULL
             AND v_user.entry_type_id <> 5)
         THEN
            SELECT SEQ_POPULATION_ENTRIES_HIST.NEXTVAL
              INTO populationEntryHistId
              FROM DUAL;

            INSERT INTO population_entries_hist (id,
                                                 population_entry_id,
                                                 status_id,
                                                 issue_action_id,
                                                 writer_id,
                                                 time_created,
                                                 assigned_writer_id)
                 VALUES (populationEntryHistId,
                         v_user.population_entry_id,
                         119,
                         NULL,
                         0,
                         SYSDATE,
                         v_user.assigned_writer);


            SELECT SEQ_POPULATION_ENTRIES.NEXTVAL
              INTO populationEntryId
              FROM DUAL;

            INSERT INTO population_entries (id,
                                            population_id,
                                            GROUP_ID,
                                            qualification_time,
                                            population_users_id,
                                            is_displayed,
                                            base_qualification_time)
                 VALUES (populationEntryId,
                         populationId,
                         1,
                         SYSDATE,
                         v_user.population_user_id,
                         1,
                         SYSDATE);

            SELECT SEQ_POPULATION_ENTRIES_HIST.NEXTVAL
              INTO populationEntryHistId
              FROM DUAL;

            INSERT INTO population_entries_hist (id,
                                                 population_entry_id,
                                                 status_id,
                                                 issue_action_id,
                                                 writer_id,
                                                 time_created,
                                                 assigned_writer_id)
                 VALUES (populationEntryHistId,
                         populationEntryId,
                         1,
                         NULL,
                         0,
                         SYSDATE,
                         v_user.assigned_writer);

            UPDATE population_users pu
               SET entry_type_id = 1,
                   curr_population_entry_id = populationEntryId,
                   curr_assigned_writer_id = NULL
             WHERE pu.id = v_user.population_user_id;

            users_converted := users_converted + 1;
         ELSE
            SELECT SEQ_POPULATION_ENTRIES_HIST.NEXTVAL
              INTO populationEntryHistId
              FROM DUAL;

            INSERT INTO population_entries_hist (id,
                                                 population_entry_id,
                                                 status_id,
                                                 issue_action_id,
                                                 writer_id,
                                                 time_created,
                                                 assigned_writer_id)
                 VALUES (populationEntryHistId,
                         v_user.population_entry_id,
                         119,
                         NULL,
                         0,
                         SYSDATE,
                         v_user.assigned_writer);

            UPDATE population_users pu
               SET entry_type_id = v_user.entry_type_id,
                   curr_population_entry_id = NULL,
                   curr_assigned_writer_id = NULL
             WHERE pu.id = v_user.population_user_id;

            users_converted := users_converted + 1;
         END IF;
      END IF;
   END LOOP;

   DBMS_OUTPUT.
    PUT_LINE ('CONVERT_USERS_INACTIVE_POP finished at: ' || SYSDATE);
   DBMS_OUTPUT.PUT_LINE ('------------------------------------------------:');
   DBMS_OUTPUT.PUT_LINE ('users_converted: ' || users_converted);
   NULL;
END CONVERT_USERS_INACTIVE_POP;
/
