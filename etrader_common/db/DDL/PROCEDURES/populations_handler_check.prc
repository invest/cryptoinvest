CREATE OR REPLACE /* Formatted on 3.12.2013 �. 11:27:57 (QP5 v5.149.1003.31008) */
PROCEDURE "POPULATIONS_HANDLER_CHECK" (runType NUMBER)
AS
   -- Run Types
   RUN_TYPE_ALL_USERS             NUMBER := 1;
   RUN_TYPE_NEW_USERS             NUMBER := 2;
   RUN_TYPE_ONLY_CONTACTS         NUMBER := 3;

   v_pe                           population_entries%ROWTYPE;
   v_pu                           population_users%ROWTYPE;
   v_peh                          population_entries_hist%ROWTYPE;
   populationEntHist              population_entries_hist%ROWTYPE;

   populationEntryId              NUMBER;
   qualifiedPopualtionId          NUMBER;
   currentPopulationId            NUMBER;
   currentPopulationPriority      NUMBER;
   is_exist_pop                   NUMBER;
   isAssignedInThePastXDays       NUMBER;
   isExistDormentConvertionPop    NUMBER;

   popQualificationTime           DATE;
   popBaseQualificationTime       DATE;
   popBaseQualificationTimeOld    DATE;
   tranTime                       DATE;

   newPopualtionThreshold         NUMBER;
   qualificationStatus            NUMBER;
   assignedWriterId               NUMBER;

   users_popualtion_users_id      NUMBER;
   lastSalesDepositTime           DATE;

   v_counter                      NUMBER;
   isExist                        NUMBER;
   isExistSuccessfullDeposit      NUMBER;
   isExistTranIssue               NUMBER;
   isExistOpenWithdraw            NUMBER;
   isFirstMinimumBalanceCheck     NUMBER;
   userNumOfSuccesDeposits        NUMBER;
   userAmountOfSuccesDeposits     NUMBER;
   isCheckQualificationTime       NUMBER;

   count_new_qualified_users      NUMBER := 0;
   count_new_qualified_contacts   NUMBER := 0;
   runTypeName                    VARCHAR2 (30);
BEGIN
   DBMS_OUTPUT.PUT_LINE ('------------------------------------------------:');
   DBMS_OUTPUT.PUT_LINE ('POPULATIONS_HANDLER started at: ' || SYSDATE);

   CASE
      WHEN runType = RUN_TYPE_ALL_USERS
      THEN
         runTypeName := 'Run Type All Users';
      WHEN runType = RUN_TYPE_NEW_USERS
      THEN
         runTypeName := 'Run Type New Users';
      WHEN runType = RUN_TYPE_ONLY_CONTACTS
      THEN
         runTypeName := 'Run Type Only Contacts';
      ELSE
         DBMS_OUTPUT.PUT_LINE ('run type: ' || runType || ' doesnt exist');
   END CASE;

   DBMS_OUTPUT.PUT_LINE ('run type: ' || runTypeName);

   IF (runType != RUN_TYPE_ONLY_CONTACTS)
   THEN
      -- RUN MARKETING_HANDLER
      marketing_handler ();

      FOR v_usr
         IN (SELECT u.id user_id,
                    u.skin_id,
                    u.balance,
                    u.time_created,
                    u.currency_id,
                    u.is_declined,
                    u.is_contact_for_daa,
                    u.last_decline_date,
                    u.time_last_login,
                    u.contact_id,
                    u.time_sc_house_result,
                    u.first_deposit_id,
                    p.id population_id,
                    p.population_type_id curr_pop_type_id,
                    pu.id population_user_id,
                    pu.curr_population_entry_id,
                    pu.curr_assigned_writer_id,
                    pu.entry_type_id,
                    pu.entry_type_action_id,
                    pt.priority,
                    ilgc.VALUE min_investment_limit,
                    s.other_reactions_days
               FROM investment_limits il,
                    investment_limit_group_curr ilgc,
                    skins s,
                                users u
                             LEFT JOIN
                                population_users pu
                             ON u.id = pu.user_id
                          LEFT JOIN
                             population_entries pe
                          ON pu.curr_population_entry_id = pe.id
                       LEFT JOIN
                          populations p
                       ON pe.population_id = p.id
                    LEFT JOIN
                       population_types pt
                    ON p.population_type_id = pt.id
              WHERE u.currency_id = ilgc.currency_id
                    AND il.min_limit_group_id =
                           ilgc.investment_limit_group_id
                    AND il.id = 1                  -- Default binary limits id
                    AND u.skin_id = s.id
                    AND u.class_id != 0
                    AND u.is_active = 1
                    AND u.is_false_account = 0
                    AND u.is_contact_by_phone = 1
                    AND u.id NOT IN (SELECT USER_ID FROM frauds)
                    AND (pu.entry_type_id IS NULL OR pu.entry_type_id != 5) -- population.enrty.type.remove.permanently
                    AND pu.lock_history_id IS NULL
                    AND (pt.is_replaceable_by_handler = 1
                         OR pt.is_replaceable_by_handler IS NULL)
                    AND u.is_vip = 0
                    --AND u.time_sc_house_result is null
                    AND (runType = RUN_TYPE_ALL_USERS
                         OR u.time_created > SYSDATE - 1)
                    AND u.id = 134541)
      LOOP
         -- DBMS_OUTPUT.PUT_LINE('user12 : ' || v_usr.);
         v_peh := NULL;
         populationEntryId := 0;
         qualifiedPopualtionId := 0;
         newPopualtionThreshold := 0;
         isFirstMinimumBalanceCheck := 1;
         assignedWriterId := v_usr.curr_assigned_writer_id;
         users_popualtion_users_id := v_usr.population_user_id;
         lastSalesDepositTime := NULL;

         IF (v_usr.population_id IS NULL)
         THEN
            currentPopulationId := 0;
            currentPopulationPriority := 999999;            -- Lowest proirity
         ELSE
            currentPopulationId := v_usr.population_id;
            currentPopulationPriority := v_usr.priority;
         END IF;

         FOR v_population
            IN (  SELECT p.*, pt.is_delay_enable, pt.priority
                    FROM populations p, population_types pt
                   WHERE p.population_type_id = pt.id
                         AND (pt.priority < currentPopulationPriority
                              OR currentPopulationPriority = 10) -- Highest priority is 1
                         AND p.id != currentPopulationId
                         AND p.is_active = 1
                         AND p.skin_id = v_usr.skin_id
                         -- This pop type is handled by this procedure
                         AND pt.is_population_handler = 1
                         AND pt.handler_run_type = runType
                ORDER BY pt.priority                  -- Highest priority is 1
                                    )
         LOOP
            populationEntHist := NULL;
            popQualificationTime := NULL;
            popBaseQualificationTime := NULL;
            isCheckQualificationTime := 0;

            CASE
               ------------------------------------------------------------------------------------------------------------------------------------------------
               WHEN v_population.population_type_id = 1
               THEN                             --POPULATION_FIRST_DEPO_FAILED
                  IF (v_usr.is_declined = 1)
                  THEN
                     -- Check whether this user had some successed deposits in the past
                     SELECT MAX (t.id)
                       INTO isExistSuccessfullDeposit
                       FROM transactions t, transaction_types tt
                      WHERE     tt.class_type = 1
                            AND user_id = v_usr.user_id
                            AND type_id = tt.id
                            AND status_id IN (2, 7, 8);

                     IF (isExistSuccessfullDeposit IS NULL)
                     THEN
                        popBaseQualificationTime := v_usr.last_decline_date;
                        popQualificationTime := popBaseQualificationTime;

                        isCheckQualificationTime := 1;
                     END IF;
                  END IF;                   -- IF (v_usr.is_declined = 1) THEN
               ------------------------------------------------------------------------------------------------------------------------------------------------
               WHEN v_population.population_type_id = 2
               THEN                             -- POPULATION_LAST_DEPO_FAILED
                  -- If users doesn't want to ne called for decline after approve we don't insert him into DDA population.
                  IF (v_usr.is_contact_for_daa != 0 AND v_usr.is_declined = 1)
                  THEN
                     popBaseQualificationTime := v_usr.last_decline_date;
                     popQualificationTime :=
                        popBaseQualificationTime
                        + v_population.number_of_minutes / 1440;

                     IF (SYSDATE >= popQualificationTime)
                     THEN
                        -- Check whether this user had some successed deposits in the past
                        IF (isExistSuccessfullDeposit IS NOT NULL)
                        THEN
                           isCheckQualificationTime := 1;
                        END IF; -- IF (isExistSuccessfullDeposit is not null) THEN
                     END IF;
                  END IF;
               ------------------------------------------------------------------------------------------------------------------------------------------------
               WHEN v_population.population_type_id = 5
               THEN                              -- POPULATION_NO_DEPOSIT_FILL
                  popBaseQualificationTime := v_usr.time_created;
                  popQualificationTime :=
                     popBaseQualificationTime
                     + v_population.number_of_minutes / 1440;

                  IF (SYSDATE >= popQualificationTime)
                  THEN
                     SELECT MAX (t.id)
                       INTO isExist
                       FROM transactions t, transaction_types tt
                      WHERE     tt.class_type = 1
                            AND user_id = v_usr.user_id
                            AND type_id = tt.id;

                     IF (isExist IS NULL)
                     THEN
                        isCheckQualificationTime := 1;
                     END IF;
                  END IF;
               ------------------------------------------------------------------------------------------------------------------------------------------------
               WHEN v_population.population_type_id = 9
               THEN                              -- POPULATION_DORMANT_ACCOUNT
                  IF (v_usr.balance > v_usr.min_investment_limit)
                  THEN
                     -- IF we didn't find a sales deposit for that user before, search it.
                     IF (lastSalesDepositTime IS NULL)
                     THEN
                        SELECT MAX (t.time_created)
                          INTO lastSalesDepositTime
                          FROM transactions t, transactions_issues ti
                         WHERE t.id = ti.transaction_id
                               AND t.user_id = v_usr.user_id;
                     END IF;        --  IF (lastSalesDepositTime is null) THEN

                     -- If user had no sales deposit in the past or v_population.LAST_SALES_DEPOSIT_DAYS has passed since last sales deposit time... go on...
                     IF (lastSalesDepositTime IS NULL
                         OR SYSDATE >
                               lastSalesDepositTime
                               + v_population.last_sales_deposit_days)
                     THEN
                        popBaseQualificationTime :=
                           GET_LAST_INVEST_TIME (v_usr.user_id);

                        IF (popBaseQualificationTime IS NOT NULL)
                        THEN
                           -- If v_population.number_of_days has passed since last user login time
                           -- or v_population.max_last_login_time_days has passed since user got under investment limit... go on...
                           IF (v_usr.time_last_login <
                                  SYSDATE - v_population.last_invest_day
                               OR popBaseQualificationTime <
                                     SYSDATE
                                     - v_population.max_last_login_time_days)
                           THEN
                              popQualificationTime :=
                                 popBaseQualificationTime
                                 + v_population.last_invest_day;

                              IF (SYSDATE >= popQualificationTime)
                              THEN
                                 -- Check if user was above min balance during the all threshold time and didn't had a balance increase
                                 IF (get_is_dormant_above_balance (
                                        v_usr.user_id,
                                        v_population.last_invest_day,
                                        v_usr.min_investment_limit,
                                        v_usr.balance) = 1)
                                 THEN
                                    isCheckQualificationTime := 1;
                                 END IF;
                              END IF; -- IF (sysdate  >= popQualificationTime) THEN
                           END IF; -- IF (v_usr.time_last_login < sysdate - v_population.number_of_days...
                        END IF; -- IF (popBaseQualificationTime is not null) THEN
                     END IF; -- IF (lastSalesDepositTime is null OR sysdate > lastSalesDepositTime + v_population.last_sales_deposit_days) THEN
                  END IF; -- IF  (v_usr.balance > v_usr.min_investment_limit) THEN
               ------------------------------------------------------------------------------------------------------------------------------------------------
               WHEN v_population.population_type_id = 10
               THEN                               -- POPULATION_NO_INVESTMENTS
                  popBaseQualificationTime :=
                     GET_LAST_SUCCESS_DEPOSIT_TIME (v_usr.user_id);

                  IF (popBaseQualificationTime IS NOT NULL)
                  THEN
                     popQualificationTime :=
                        popBaseQualificationTime
                        + v_population.number_of_days;

                     IF (SYSDATE > popQualificationTime)
                     THEN
                        SELECT MAX (i.id)
                          INTO isExist
                          FROM investments i
                         WHERE v_usr.user_id = i.user_id;

                        IF (isExist IS NULL)
                        THEN
                           isCheckQualificationTime := 1;
                        END IF;
                     END IF;
                  END IF;
               ------------------------------------------------------------------------------------------------------------------------------------------------
               WHEN v_population.population_type_id = 22
               THEN                                -- POPULATION OPEN WITHDRAW
                  SELECT COUNT (t.id)
                    INTO isExistOpenWithdraw
                    FROM transactions t, transaction_types tt
                   WHERE     t.user_id = v_usr.user_id
                         AND tt.id = t.type_id
                         AND tt.class_type = 2
                         AND t.status_id = 4;

                  IF (isExistOpenWithdraw > 0)
                  THEN
                     isCheckQualificationTime := 1;
                     popQualificationTime := SYSDATE;
                  END IF;
               --------------------------------------------------------------------------------------------------------------------------------------------------
               WHEN v_population.population_type_id = 24
               THEN                           -- POPULATION_DORMENT_CONVERSION
                  IF (v_usr.first_deposit_id IS NOT NULL
                      AND v_usr.first_deposit_id > 0)
                  THEN
                     SELECT COUNT (ti.transaction_id)
                       INTO isExistTranIssue
                       FROM transactions_issues ti
                      WHERE ti.transaction_id = v_usr.first_deposit_id
                            AND ti.time_settled IS NULL;

                     IF (isExistTranIssue > 0)
                     THEN
                        IF (v_usr.curr_assigned_writer_id IS NULL)
                        THEN
                           SELECT t.time_created
                             INTO tranTime
                             FROM transactions t
                            WHERE t.id = v_usr.first_deposit_id;

                           IF (tranTime + 7 < SYSDATE)
                           THEN
                              SELECT COUNT (peh.ID)
                                INTO isAssignedInThePastXDays
                                FROM population_users pu,
                                     population_entries pe,
                                     population_entries_hist peh,
                                     populations p,
                                     population_entries_hist_status pehs
                               WHERE     pu.id = pe.population_users_id
                                     AND pe.id = peh.population_entry_id
                                     AND pu.user_id = v_usr.user_id
                                     AND p.id = pe.population_id
                                     AND pehs.id = peh.status_id
                                     AND peh.status_id = 2
                                     AND peh.time_created > tranTime; -- not assigned in the past X days, X = 7

                              IF (isAssignedInThePastXDays = 0)
                              THEN
                                 SELECT COUNT (peh.ID)
                                   INTO isExistDormentConvertionPop
                                   FROM population_users pu,
                                        population_entries pe,
                                        population_entries_hist peh,
                                        populations p,
                                        population_entries_hist_status pehs
                                  WHERE     pu.id = pe.population_users_id
                                        AND pe.id = peh.population_entry_id
                                        AND pu.user_id = v_usr.user_id
                                        AND p.id = pe.population_id
                                        AND pehs.id = peh.status_id
                                        AND p.population_type_id = 24;

                                 IF (isExistDormentConvertionPop = 0)
                                 THEN
                                    isCheckQualificationTime := 1;
                                    popQualificationTime := SYSDATE;
                                 END IF;
                              END IF;
                           END IF;
                        END IF;
                     END IF;
                  END IF;
               ------------------------------------------------------------------------------------------------------------------------------------------------
               ELSE         --IF  v_population.population_type_id wasn't found
                  -- No handler for this population type
                  DBMS_OUTPUT.
                   PUT_LINE (
                        'population_type_id: '
                     || v_population.population_type_id
                     || ' wasnt found');
            END CASE;

            -- IF isCheckQualificationTime = 1, User is qualified to that populaiton, now check his qualification time
            IF (isCheckQualificationTime = 1)
            THEN
               populationEntHist :=
                  GET_USER_LAST_POP_BY_TYPE (users_popualtion_users_id,
                                             v_population.population_type_id);

               IF (populationEntHist.status_id IS NULL
                   OR popBaseQualificationTime >=
                         populationEntHist.TIME_CREATED
                   OR (v_population.is_delay_enable = 0
                       AND NOT (populationEntHist.status_id IN
                                   (100, 101, 107, 222, 112, 602, 115)))
                   OR (v_population.is_delay_enable = 1
                       AND NOT (populationEntHist.status_id IN
                                   (100, 101, 107, 222, 112, 602, 115))))
               THEN
                  -- Find qualification time of this population last entry
                  BEGIN
                     SELECT base_qualification_time
                       INTO popBaseQualificationTimeOld
                       FROM population_entries pe
                      WHERE pe.id = populationEntHist.population_entry_id
                            AND qualification_time IS NOT NULL;
                  EXCEPTION
                     WHEN NO_DATA_FOUND
                     THEN
                        popBaseQualificationTimeOld := NULL;
                  END;

                  -- If the qualification time is the same don't need to open a new entry, re-enter to old one.
                  IF (popBaseQualificationTimeOld IS NOT NULL
                      AND popBaseQualificationTimeOld =
                             popBaseQualificationTime)
                  THEN
                     populationEntryId :=
                        populationEntHist.population_entry_id;
                  END IF; --  IF (popBaseQualificationTimeOld is not null AND popBaseQualificationTimeOld =  popBaseQualificationTime) THEN

                  -- Update user entry with this population id.
                  qualifiedPopualtionId := v_population.id;

                  -- Keep current popualtion threshold
                  newPopualtionThreshold := v_population.threshold;

                  -- If user was finally found qualified for current popualtion, break the loop
                  GOTO end_loop;
               END IF;         -- IF (populationEntHist.status_id is null ....
            END IF;                --   IF (isCheckQualificationTime = 1) THEN
         END LOOP;

        <<end_loop>>
         -- If got a newPopualtionId, insert user to new population.
         IF (qualifiedPopualtionId != 0)
         THEN
            count_new_qualified_users := count_new_qualified_users + 1;

            -- If this  is a contact.
            IF (v_usr.user_id = 0)
            THEN
               assignedWriterId := NULL;
            END IF;

            -- If user is already in a population, remove it first.
            IF (v_usr.curr_population_entry_id IS NOT NULL)
            THEN
               SELECT SEQ_POPULATION_ENTRIES_HIST.NEXTVAL
                 INTO v_peh.id
                 FROM DUAL;

               v_peh.population_entry_id := v_usr.curr_population_entry_id;
               v_peh.status_id := 105;
               v_peh.writer_id := 0;
               v_peh.assigned_writer_id := assignedWriterId;
               v_peh.time_created := SYSDATE;
               v_peh.issue_action_id := NULL;

               INSERT INTO population_entries_hist
                    VALUES v_peh;
            END IF;    -- IF (v_usr.curr_population_entry_id is not null) THEN

            -- If user doesn't have an entry in population_users, create one.
            IF (users_popualtion_users_id IS NULL)
            THEN
               SELECT SEQ_POPULATION_USERS.NEXTVAL
                 INTO users_popualtion_users_id
                 FROM DUAL;

               v_pu.id := users_popualtion_users_id;
               v_pu.user_id := v_usr.user_id;

               IF (v_usr.contact_id != 0)
               THEN
                  v_pu.contact_id := v_usr.contact_id;
               ELSE
                  v_pu.contact_id := NULL;
               END IF;

               v_pu.curr_assigned_writer_id := NULL;
               v_pu.curr_population_entry_id := NULL;
               v_pu.entry_type_id := 1;       -- population.enrty.type.general
               v_pu.entry_type_action_id := NULL;
               v_pu.lock_history_id := NULL;
               v_pu.time_created := sysdate;

               INSERT INTO population_users
                    VALUES v_pu;
            END IF;            --  IF (users_popualtion_users_id is null) THEN

            -- If population entry id hasn't been changed create a new one.
            IF (populationEntryId = 0)
            THEN
               -- Count the number of users in the new population in order to determine user's entry group.
               SELECT COUNT (pe.id)
                 INTO v_counter
                 FROM population_entries pe, population_users pu
                WHERE pe.id = pu.curr_population_entry_id
                      AND pe.population_id = qualifiedPopualtionId;

               -- Insert a new population_entry
               SELECT SEQ_POPULATION_ENTRIES.NEXTVAL
                 INTO populationEntryId
                 FROM DUAL;

               v_pe.id := populationEntryId;
               v_pe.population_id := qualifiedPopualtionId;
               v_pe.GROUP_ID :=
                  CASE
                     WHEN newPopualtionThreshold = 1
                     THEN
                        1
                     ELSE
                        CASE
                           WHEN MOD (v_counter, newPopualtionThreshold) = 0
                           THEN
                              0
                           ELSE
                              1
                        END
                  END;
               v_pe.population_users_id := users_popualtion_users_id;
               v_pe.qualification_time := popQualificationTime;
               v_pe.base_qualification_time := popBaseQualificationTime;
               v_pe.is_displayed := 1;

               INSERT INTO population_entries
                    VALUES v_pe;

               qualificationStatus := 1;                      -- Qualification
            ELSE
               qualificationStatus := 7;                    -- reQualification
            END IF;                        --  IF (populationEntryId = 0) THEN


            -- Insert a new population_entry_history
            SELECT SEQ_POPULATION_ENTRIES_HIST.NEXTVAL
              INTO v_peh.id
              FROM DUAL;

            v_peh.population_entry_id := populationEntryId;
            v_peh.status_id := qualificationStatus;
            v_peh.writer_id := 0;
            v_peh.assigned_writer_id := assignedWriterId;
            v_peh.time_created := SYSDATE;
            v_peh.issue_action_id := NULL;

            INSERT INTO population_entries_hist
                 VALUES v_peh;

            -- Update user entry in population_users table
            UPDATE population_users pu
               SET pu.curr_population_entry_id = populationEntryId,
                   pu.curr_assigned_writer_id = assignedWriterId
             WHERE pu.user_id = v_usr.user_id;
         END IF;                       -- IF (qualifiedPopualtionId != 0) THEN

         COMMIT;
      END LOOP;

      IF (runType = RUN_TYPE_ALL_USERS)
      THEN
         UPDATE db_parameters dp
            SET date_value = SYSDATE
          WHERE dp.id = 9;
      END IF;

      -- RUN DELAY HANDLER
      delay_handler ();

      -- RUN TRACKING HANDLER
      tracking_handler ();

      -- RUN SPECIAL HANDLER
      special_handler ();

      -- RUN CALLBACK HANDLER
      callback_handler ();

      IF (runType = RUN_TYPE_ALL_USERS)
      THEN
         -- RUN DORMENT CONVERSION HANDLER
         dorment_conversion_handler ();

         -- RUN USERS STATUS HANDLER
         users_status_handler ();

         -- RUN USERS RANK HANDLER
         users_rank_handler ();

         --RUN RETURN FROM TRAKING HANDLER
         return_from_tracking ();

         --RUN MANUALLY IMPORTED CON HANDLER
         manually_imported_con_handler ();
      END IF;

      DBMS_OUTPUT.
       PUT_LINE (
         'count_new_qualified_users  = ' || count_new_qualified_users);
   END IF;

   FOR v_con
      IN (SELECT c.id contact_id,
                 c.skin_id,
                 c.time_created,
                 c.TYPE,
                 p.id population_id,
                 p.population_type_id curr_pop_type_id,
                 pu.id population_user_id,
                 pu.curr_population_entry_id,
                 pu.curr_assigned_writer_id,
                 pu.entry_type_id,
                 pu.entry_type_action_id,
                 pt.priority,
                 s.other_reactions_days
            FROM skins s,
                             contacts c
                          LEFT JOIN
                             population_users pu
                          ON c.id = pu.contact_id
                       LEFT JOIN
                          population_entries pe
                       ON pu.curr_population_entry_id = pe.id
                    LEFT JOIN
                       populations p
                    ON pe.population_id = p.id
                 LEFT JOIN
                    population_types pt
                 ON p.population_type_id = pt.id
           WHERE     c.skin_id = s.id
                 AND (pu.entry_type_id IS NULL OR pu.entry_type_id != 5) -- population.enrty.type.remove.permanently
                 AND pu.lock_history_id IS NULL
                 AND (pt.is_replaceable_by_handler = 1
                      OR pt.is_replaceable_by_handler IS NULL)
                 AND c.TYPE IN (5, 6, 7) -- Short Reg Form contacts and register attempt
                 AND (c.land_line_phone IS NOT NULL
                      OR c.mobile_phone IS NOT NULL)
                 AND c.user_id = 0)
   LOOP
      v_peh := NULL;
      populationEntryId := 0;
      qualifiedPopualtionId := 0;
      newPopualtionThreshold := 0;
      assignedWriterId := v_con.curr_assigned_writer_id;
      users_popualtion_users_id := v_con.population_user_id;

      IF (v_con.population_id IS NULL)
      THEN
         currentPopulationId := 0;
         currentPopulationPriority := 999999;               -- Lowest proirity
      ELSE
         currentPopulationId := v_con.population_id;
         currentPopulationPriority := v_con.priority;
      END IF;

      FOR v_population
         IN (  SELECT p.*, pt.is_delay_enable
                 FROM populations p, population_types pt
                WHERE     p.population_type_id = pt.id
                      AND pt.priority < currentPopulationPriority -- Highest priority is 1
                      AND p.id != currentPopulationId
                      AND p.is_active = 1
                      AND p.skin_id = v_con.skin_id
                      -- This pop type is handled by this procedure
                      AND pt.is_population_handler = 1
                      AND pt.handler_run_type = RUN_TYPE_ONLY_CONTACTS
             ORDER BY pt.priority                     -- Highest priority is 1
                                 )
      LOOP
         populationEntHist := NULL;
         popQualificationTime := NULL;
         popBaseQualificationTime := NULL;
         isCheckQualificationTime := 0;

         CASE
            ------------------------------------------------------------------------------------------------------------------------------------------------
            WHEN v_population.population_type_id IN (20, 21, 27)
            THEN  --POPULATION_SHORT_REG_FORM AND POPLULATION_REGISTER_ATTEMPT
               popBaseQualificationTime := v_con.time_created;

               popQualificationTime :=
                  popBaseQualificationTime
                  + v_population.number_of_minutes / 1440;

               IF (popQualificationTime < SYSDATE)
               THEN
                  isCheckQualificationTime := 1;
               END IF;
            ------------------------------------------------------------------------------------------------------------------------------------------------
            ELSE            --IF  v_population.population_type_id wasn't found
               -- No handler for this population type
               DBMS_OUTPUT.
                PUT_LINE (
                     'population_type_id: '
                  || v_population.population_type_id
                  || ' wasnt found');
         END CASE;

         -- IF isCheckQualificationTime = 1, contact is qualified to that populaiton, now check his qualification time
         IF (isCheckQualificationTime = 1)
         THEN
            populationEntHist :=
               GET_USER_LAST_POP_BY_TYPE (users_popualtion_users_id,
                                          v_population.population_type_id);

            IF (populationEntHist.status_id IS NULL
                OR popBaseQualificationTime >= populationEntHist.TIME_CREATED
                OR (v_population.is_delay_enable = 0
                    AND NOT (populationEntHist.status_id IN
                                (100, 101, 107, 222, 112, 602, 115)))
                OR (v_population.is_delay_enable = 1
                    AND NOT (populationEntHist.status_id IN
                                (100, 101, 107, 222, 112, 602, 115))))
            THEN
               -- Find qualification time of this population last entry
               BEGIN
                  SELECT base_qualification_time
                    INTO popBaseQualificationTimeOld
                    FROM population_entries pe
                   WHERE pe.id = populationEntHist.population_entry_id
                         AND qualification_time IS NOT NULL;
               EXCEPTION
                  WHEN NO_DATA_FOUND
                  THEN
                     popBaseQualificationTimeOld := NULL;
               END;

               -- If the qualification time is the same don't need to open a new entry, re-enter to old one.
               IF (popBaseQualificationTimeOld IS NOT NULL
                   AND popBaseQualificationTimeOld = popBaseQualificationTime)
               THEN
                  populationEntryId := populationEntHist.population_entry_id;
               END IF; --  IF (popBaseQualificationTimeOld is not null AND popBaseQualificationTimeOld =  popBaseQualificationTime) THEN

               IF ( (v_con.TYPE = 5 AND v_population.population_type_id = 20)
                   OR (v_con.TYPE = 6
                       AND v_population.population_type_id = 21)
                   OR (v_con.TYPE = 7
                       AND v_population.population_type_id = 27))
               THEN
                  -- Update contact entry with this population id.
                  qualifiedPopualtionId := v_population.id;
                  -- Keep current popualtion threshold
                  newPopualtionThreshold := v_population.threshold;
                  -- If contact was finally found qualified for current popualtion, break the loop
                  GOTO end_loop;
               END IF;
            END IF;            -- IF (populationEntHist.status_id is null ....
         END IF;                   --   IF (isCheckQualificationTime = 1) THEN
      END LOOP;

     <<end_loop>>
      -- If got a newPopualtionId, insert contact to new population.
      IF (qualifiedPopualtionId != 0)
      THEN
         count_new_qualified_contacts := count_new_qualified_contacts + 1;

         -- If this contact is in general entry_type preform auto cancel assign.
         IF (v_con.entry_type_id = 1)
         THEN
            assignedWriterId := NULL;
         END IF;

         -- If contact is already in a population, remove it first.
         IF (v_con.curr_population_entry_id IS NOT NULL)
         THEN
            SELECT SEQ_POPULATION_ENTRIES_HIST.NEXTVAL
              INTO v_peh.id
              FROM DUAL;

            v_peh.population_entry_id := v_con.curr_population_entry_id;
            v_peh.status_id := 105;
            v_peh.writer_id := 0;
            v_peh.assigned_writer_id := assignedWriterId;
            v_peh.time_created := SYSDATE;
            v_peh.issue_action_id := NULL;

            INSERT INTO population_entries_hist
                 VALUES v_peh;
         END IF;       -- IF (v_con.curr_population_entry_id is not null) THEN

         -- If contact doesn't have an entry in population_users, create one.
         IF (users_popualtion_users_id IS NULL)
         THEN
            SELECT SEQ_POPULATION_USERS.NEXTVAL
              INTO users_popualtion_users_id
              FROM DUAL;

            v_pu.id := users_popualtion_users_id;
            v_pu.user_id := NULL;
            v_pu.contact_id := v_con.contact_id;
            v_pu.curr_assigned_writer_id := NULL;
            v_pu.curr_population_entry_id := NULL;
            v_pu.entry_type_id := 1;          -- population.enrty.type.general
            v_pu.entry_type_action_id := NULL;
            v_pu.lock_history_id := NULL;
            v_pu.time_created := SYSDATE;

            --                    DBMS_OUTPUT.PUT_LINE('contact_id: ' || v_con.contact_id);
            INSERT INTO population_users
                 VALUES v_pu;
         END IF;               --  IF (users_popualtion_users_id is null) THEN

         -- If population entry id hasn't been changed create a new one.
         IF (populationEntryId = 0)
         THEN
            -- Count the number of users in the new population in order to determine user's entry group.
            SELECT COUNT (pe.id)
              INTO v_counter
              FROM population_entries pe, population_users pu
             WHERE pe.id = pu.curr_population_entry_id
                   AND pe.population_id = qualifiedPopualtionId;

            -- Insert a new population_entry
            SELECT SEQ_POPULATION_ENTRIES.NEXTVAL
              INTO populationEntryId
              FROM DUAL;

            v_pe.id := populationEntryId;
            v_pe.population_id := qualifiedPopualtionId;
            v_pe.GROUP_ID :=
               CASE
                  WHEN newPopualtionThreshold = 1
                  THEN
                     1
                  ELSE
                     CASE
                        WHEN MOD (v_counter, newPopualtionThreshold) = 0
                        THEN
                           0
                        ELSE
                           1
                     END
               END;
            v_pe.population_users_id := users_popualtion_users_id;
            v_pe.qualification_time := popQualificationTime;
            v_pe.base_qualification_time := popBaseQualificationTime;
            v_pe.is_displayed := 1;

            INSERT INTO population_entries
                 VALUES v_pe;

            qualificationStatus := 1;                         -- Qualification
         ELSE
            qualificationStatus := 7;                       -- reQualification
         END IF;                           --  IF (populationEntryId = 0) THEN


         -- Insert a new population_entry_history
         SELECT SEQ_POPULATION_ENTRIES_HIST.NEXTVAL INTO v_peh.id FROM DUAL;

         v_peh.population_entry_id := populationEntryId;
         v_peh.status_id := qualificationStatus;
         v_peh.writer_id := 0;
         v_peh.assigned_writer_id := assignedWriterId;
         v_peh.time_created := SYSDATE;
         v_peh.issue_action_id := NULL;

         INSERT INTO population_entries_hist
              VALUES v_peh;

         -- Update contact entry in population_users table
         UPDATE population_users pu
            SET pu.curr_population_entry_id = populationEntryId,
                pu.curr_assigned_writer_id = assignedWriterId
          WHERE pu.contact_id = v_con.contact_id;
      END IF;                          -- IF (qualifiedPopualtionId != 0) THEN

      COMMIT;
   END LOOP;

   DBMS_OUTPUT.
    PUT_LINE (
      'count_new_qualified_conacts  = ' || count_new_qualified_contacts);

   DBMS_OUTPUT.PUT_LINE ('POPULATIONS_HANDLER finished at: ' || SYSDATE);
   DBMS_OUTPUT.PUT_LINE ('------------------------------------------------:');

   NULL;
END;
/
