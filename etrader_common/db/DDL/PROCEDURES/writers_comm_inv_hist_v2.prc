CREATE OR REPLACE /* Formatted on 3.12.2013 �. 11:28:06 (QP5 v5.149.1003.31008) */
PROCEDURE WRITERS_COMM_INV_HIST_V2 (s_time DATE, e_time DATE)
AS
   CURSOR c_main
   IS
      SELECT u.id,
             i.id i_id,
             NVL (ti.transaction_id, 0) sales_deposit,
             ti.time_settled qualified,
             i.time_created inv_time,
             rtf.factor AS factor
        FROM investments i,
             users u,
             transactions_issues ti,
             retention_turnover_factor rtf,
             opportunities op
       WHERE     i.time_created BETWEEN s_time AND e_time
             AND i.opportunity_id = op.id
             AND op.opportunity_type_id = rtf.opportunity_type_id
             --i.id between max_id+1 and new_max_id
             --and i.id not in (82385011,82385032,82385039,82385053,82385081,82385095,82385102,82385116,82385158,82385172,82385186)
             AND i.user_id = u.id
             AND u.class_id <> 0
             AND u.first_deposit_id = ti.transaction_id(+);

   CURSOR c_check_assigned_retention (
      p_user_id     NUMBER,
      p_inv_time    DATE)
   IS
        SELECT peh.assigned_writer_id, w.dept_id, peh.time_created
          FROM population_users pu,
               population_entries pe,
               population_entries_hist peh,
               writers w
         WHERE     pu.id = pe.population_users_id
               AND peh.population_entry_id = pe.id
               AND pu.user_id = p_user_id
               AND peh.assigned_writer_id IS NOT NULL
               AND peh.time_created <= p_inv_time
               AND peh.assigned_writer_id = w.id
               AND peh.status_id = 2
               AND peh.time_created > DATE '2012-09-01'                  --???
      ORDER BY peh.time_created DESC;

   CURSOR c_cancel_assignment (
      p_user_id        NUMBER,
      p_writer_id      NUMBER,
      p_assign_time    DATE,
      p_inv_time       DATE)
   IS
      SELECT COUNT (*) AS cancelation_cnt
        FROM population_users pu,
             population_entries pe,
             population_entries_hist peh,
             populations p,
             population_entries_hist_status pehs
       WHERE     pu.id = pe.population_users_id
             AND peh.population_entry_id = pe.id
             AND p.id = pe.population_id
             AND pehs.id = peh.status_id
             -- and peh.assigned_writer_id = p_writer_id  --due to bug (not closed assignment)
             AND pu.user_id = p_user_id
             AND peh.time_created BETWEEN p_assign_time AND p_inv_time
             AND (pehs.id = 6 OR pehs.is_cancel_assign = 1);

   CURSOR c_check_bonus (
      p_user_id     NUMBER,
      p_inv_time    DATE)
   IS
      SELECT COUNT (*) AS bonus_cnt
        FROM bonus_users bu
       WHERE     bu.bonus_state_id IN (2, 3)
             AND bu.user_id = p_user_id
             AND p_inv_time BETWEEN bu.start_date AND bu.end_date;

   CURSOR c_check_reached_call (
      rep            NUMBER,
      p_user_id      NUMBER,
      assign_time    DATE,
      c_inv_time     DATE)
   IS
      SELECT COUNT (*) cnt
        FROM issue_actions ia, issues i, issue_action_types iat
       WHERE     ia.writer_id = rep
             AND ia.action_time BETWEEN assign_time AND c_inv_time --peh.time_created --fix old condition(ia.action_time>= assign_time )
             AND i.user_id = p_user_id
             AND i.id = ia.issue_id
             --???
             /*and ia.channel_id=1
             and ia.reached_status_id=2*/
             AND ia.issue_action_type_id = iat.id
             AND iat.channel_id = 1
             AND iat.reached_status_id = 2
             --???
             AND ia.action_time > DATE '2012-09-01';                     --???

   r_check_assigned_rep         NUMBER := 0;
   r_check_assigned_retention   NUMBER := 0;
   r_check_assigned_time        DATE;
   r_check_bonus                NUMBER := 0;
   r_check_reached_call         NUMBER := 0;
   r_check_assigned_canceled    NUMBER := 0;
BEGIN
   DELETE WRITERS_COMMISSION_INV
    WHERE time_created BETWEEN s_time AND e_time;

   COMMIT;

   FOR tmp IN c_main
   LOOP
      r_check_assigned_rep := 0;
      r_check_assigned_retention := 0;
      r_check_assigned_time := NULL;
      r_check_bonus := 0;
      r_check_reached_call := 0;

      --dbms_output.put_line('user: '||tmp.id);
      --dbms_output.put_line('user: '||tmp.id||' invest: '||tmp.i_id||','||tmp.inv_time);
      OPEN c_check_assigned_retention (tmp.id, tmp.inv_time);

      FETCH c_check_assigned_retention
      INTO r_check_assigned_rep,
           r_check_assigned_retention,
           r_check_assigned_time;

      CLOSE c_check_assigned_retention;

      --dbms_output.put_line('1user: '||tmp.id||' invest: '||tmp.i_id||','||r_check_assigned_rep||','||r_check_assigned_retention||','||r_check_assigned_time);

      IF r_check_assigned_rep IS NULL OR r_check_assigned_retention != 3
      THEN
         GOTO END_LOOP;
      END IF;

      --dbms_output.put_line('2user: '||tmp.id||' invest: '||tmp.i_id||','||tmp.sales_deposit||','||tmp.qualified);
      IF tmp.sales_deposit > 0 AND tmp.qualified IS NULL
      THEN
         GOTO END_LOOP;
      END IF;

      IF tmp.sales_deposit > 0 AND tmp.qualified > tmp.inv_time
      THEN                         -- this condition is for historical data!!!
         GOTO END_LOOP;
      END IF;

      OPEN c_cancel_assignment (tmp.id,
                                r_check_assigned_rep,
                                r_check_assigned_time,
                                tmp.inv_time);

      FETCH c_cancel_assignment INTO r_check_assigned_canceled;

      CLOSE c_cancel_assignment;

      IF r_check_assigned_canceled > 0
      THEN
         GOTO END_LOOP;
      END IF;

      OPEN c_check_bonus (tmp.id, tmp.inv_time);

      FETCH c_check_bonus INTO r_check_bonus;

      CLOSE c_check_bonus;

      --dbms_output.put_line('3user: '||tmp.id||' invest: '||r_check_bonus);
      IF r_check_bonus != 0
      THEN
         GOTO END_LOOP;
      END IF;

      --dbms_output.put_line('4user: '||tmp.id||' invest: '||tmp.i_id||','||r_check_reached_call);
      --dbms_output.put_line('user: '||tmp.id||' invest: '||tmp.i_id||','||r_check_assigned_rep||','||tmp.id||','||r_check_assigned_time);
      OPEN c_check_reached_call (r_check_assigned_rep,
                                 tmp.id,
                                 r_check_assigned_time,
                                 tmp.inv_time);

      FETCH c_check_reached_call INTO r_check_reached_call;

      CLOSE c_check_reached_call;

      --dbms_output.put_line('user: '||tmp.id||' invest: '||tmp.i_id||','||r_check_reached_call);
      IF r_check_reached_call = 0
      THEN
         GOTO END_LOOP;
      END IF;

      --dbms_output.put_line('5user: '||tmp.id||' invest: '||tmp.i_id||','||r_check_assigned_rep||','||tmp.id||','||r_check_assigned_time);
      INSERT INTO writers_commission_inv (id,
                                          investments_id,
                                          writer_id,
                                          time_created,
                                          factor)
           VALUES (SEQ_WRITERS_COMMISSION_INV.NEXTVAL,
                   tmp.i_id,
                   r_check_assigned_rep,
                   tmp.inv_time,
                   tmp.factor);

     <<END_LOOP>>
      NULL;
   END LOOP;

   COMMIT;
END;
/
