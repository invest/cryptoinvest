create or replace PROCEDURE WRITERS_COMMISSION_DEP_HANDLER AS 
  PRC_last_run DATE;
  PRC_start_time DATE;
  PRC_dep_during_call_days NUMBER;
  PRC_db_parameter_id NUMBER := 14;
  PRC_count_deposits NUMBER;
  PRC_count_withdraws NUMBER;
  PRC_db_parameter_updated NUMBER;
BEGIN
  DBMS_OUTPUT.PUT_LINE('------------------------------------------------:');
  DBMS_OUTPUT.PUT_LINE('WRITERS_COMMISSION_DEP_HANDLER started at: ' || to_char(sysdate, 'yyyy-mm-dd hh24:mi:ss'));
  DBMS_OUTPUT.PUT_LINE('------------------------------------------------:');
  
  -- Get last time which procedure was running.
  PRC_last_run := WRITERS_COMMISSION.GET_DATE_FROM_DB_PARAMETERS(PRC_db_parameter_id);
  DBMS_OUTPUT.PUT_LINE('The last time which the procedure was running, PRC_last_run: ' || to_char(PRC_last_run, 'yyyy-mm-dd hh24:mi:ss'));
  DBMS_OUTPUT.PUT_LINE(' ');
  
  -- Get current time.
  PRC_start_time := sysdate;
  DBMS_OUTPUT.PUT_LINE('Current time, PRC_start_time: ' || to_char(PRC_start_time, 'yyyy-mm-dd hh24:mi:ss'));
  DBMS_OUTPUT.PUT_LINE(' ');
  
  -- Get the max deposit during call days in order not to miss last deposit during call transactions
  PRC_dep_during_call_days := WRITERS_COMMISSION.GET_MAX_DEP_DURING_CALL_DAYS;
  DBMS_OUTPUT.PUT_LINE('The max deposit during call timeframe, PRC_dep_during_call_days: ' || PRC_dep_during_call_days);
  DBMS_OUTPUT.PUT_LINE(' ');
  
  -- Deposit_handler
  PRC_count_deposits := WRITERS_COMMISSION.DEPOSIT_HANDLER(PRC_last_run, PRC_start_time, PRC_dep_during_call_days);
  DBMS_OUTPUT.PUT_LINE('Count number of commission deposits which added to the table WRITERS_COMMISSION_DEP, PRC_count_commission_dep: ' || PRC_count_deposits);
  DBMS_OUTPUT.PUT_LINE(' ');
  
  -- Withdraw_handler
  PRC_count_withdraws := WRITERS_COMMISSION.WITHDRAW_HANDLER(PRC_last_run, PRC_start_time, PRC_dep_during_call_days);
  DBMS_OUTPUT.PUT_LINE('Count number of commission withdraws which added to the table WRITERS_COMMISSION_DEP, PRC_count_withdraws: ' || PRC_count_withdraws);
  DBMS_OUTPUT.PUT_LINE(' ');
  
  -- Update DB_PARAMETERS with procedure's start time.
  PRC_db_parameter_updated := WRITERS_COMMISSION.UPDATE_DB_PARAMETERS(PRC_db_parameter_id, PRC_start_time);
  DBMS_OUTPUT.PUT_LINE(' ');

  DBMS_OUTPUT.PUT_LINE('------------------------------------------------:');
  DBMS_OUTPUT.PUT_LINE('WRITERS_COMMISSION_DEP_HANDLER finished at: ' || to_char(sysdate, 'yyyy-mm-dd hh24:mi:ss'));
  DBMS_OUTPUT.PUT_LINE('------------------------------------------------:');
  NULL;
END WRITERS_COMMISSION_DEP_HANDLER;