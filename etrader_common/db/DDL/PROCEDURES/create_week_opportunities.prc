CREATE OR REPLACE /* Formatted on 3.12.2013 �. 11:27:53 (QP5 v5.149.1003.31008) */
PROCEDURE "CREATE_WEEK_OPPORTUNITIES"
AS
   crrOppTmpl             opportunity_templates%ROWTYPE;
   crrMarket              markets%ROWTYPE;
   crrDayOfWeek           INTEGER;
   numOfHolidays          INTEGER;

   TYPE holidayCursorType IS REF CURSOR
      RETURN exchange_holidays%ROWTYPE;

   holiday_ct             holidayCursorType;
   holiday                exchange_holidays%ROWTYPE;
   halfDayCloseTime       opportunity_templates.time_est_closing%TYPE;
   halfDayLastInvest      opportunity_templates.time_last_invest%TYPE;
   toInsTimeFirstInvest   TIMESTAMP WITH TIME ZONE;
   toInsTimeEstClosing    TIMESTAMP WITH TIME ZONE;
   toInsTimeLastInvest    TIMESTAMP WITH TIME ZONE;
   wkSun                  DATE;
   wkMon                  DATE;
   wkTue                  DATE;
   wkWed                  DATE;
   wkThu                  DATE;
   wkFri                  DATE;
   wkSat                  DATE;
   isHoliday              INTEGER;
   toIns                  opportunities%ROWTYPE;
BEGIN
   crrDayOfWeek := TO_CHAR (CURRENT_DATE, 'D');
   wkSun := TRUNC (CURRENT_DATE + 8 - crrDayOfWeek);
   wkMon := wkSun + 1;
   wkTue := wkMon + 1;
   wkWed := wkTue + 1;
   wkThu := wkWed + 1;
   wkFri := wkThu + 1;
   wkSat := wkFri + 1;

   FOR item
      IN (SELECT A.market_id,
                 A.time_first_invest,
                 A.time_est_closing,
                 A.time_last_invest,
                 A.time_zone,
                 A.odds_type_id,
                 A.writer_id,
                 A.opportunity_type_id,
                 A.scheduled,
                 A.deduct_win_odds,
                 A.is_open_graph,
                 A.ODDS_GROUP,
                 B.exchange_id,
                 B.max_exposure
            FROM opportunity_templates A, markets B
           WHERE A.market_id = B.id AND A.is_active = 1 AND A.scheduled = 3)
   LOOP
      IF (   (item.exchange_id = 1)
          OR (item.exchange_id = 17)
          OR (item.exchange_id = 18))
      THEN
         SELECT COUNT (id)
           INTO numOfHolidays
           FROM exchange_holidays
          WHERE     exchange_id = item.exchange_id
                AND TRUNC (holiday) >= wkSun
                AND TRUNC (holiday) <= wkThu
                AND is_half_day = 0;

         IF numOfHolidays < 5
         THEN
            FOR i IN 0 .. 4
            LOOP
               OPEN holiday_ct FOR
                  SELECT *
                    FROM exchange_holidays
                   WHERE exchange_id = item.exchange_id
                         AND TRUNC (holiday) = wkSun + i;               -- AND

               --                        is_half_day = 0;
               FETCH holiday_ct INTO holiday;

               IF holiday_ct%NOTFOUND
               THEN
                  toInsTimeFirstInvest :=
                     TO_TIMESTAMP_TZ (
                           TO_CHAR (wkSun + i, 'yyyy-mm-dd')
                        || item.time_first_invest
                        || item.time_zone,
                        'yyyy-mm-dd hh24:mi TZR');

                  CLOSE holiday_ct;

                  EXIT;
               ELSE
                  IF holiday.is_half_day = 1
                  THEN
                     toInsTimeFirstInvest :=
                        TO_TIMESTAMP_TZ (
                              TO_CHAR (wkSun + i, 'yyyy-mm-dd')
                           || item.time_first_invest
                           || item.time_zone,
                           'yyyy-mm-dd hh24:mi TZR');

                     CLOSE holiday_ct;

                     EXIT;
                  END IF;
               END IF;

               CLOSE holiday_ct;
            END LOOP;

            FOR i IN REVERSE 0 .. 4
            LOOP
               OPEN holiday_ct FOR
                  SELECT *
                    FROM exchange_holidays
                   WHERE exchange_id = item.exchange_id
                         AND TRUNC (holiday) = wkSun + i;

               FETCH holiday_ct INTO holiday;

               IF holiday_ct%NOTFOUND
               THEN
                  IF (item.exchange_id = 1) AND i = 0
                  THEN
                     SELECT time_est_closing, time_last_invest
                       INTO halfDayCloseTime, halfDayLastInvest
                       FROM opportunity_templates
                      WHERE     market_id = item.market_id
                            AND scheduled = 2
                            AND is_full_day = 2
                            AND is_active = 1;

                     toInsTimeEstClosing :=
                        TO_TIMESTAMP_TZ (
                              TO_CHAR (wkSun + i, 'yyyy-mm-dd')
                           || halfDayCloseTime
                           || item.time_zone,
                           'yyyy-mm-dd hh24:mi TZR');
                     toInsTimeLastInvest :=
                        TO_TIMESTAMP_TZ (
                              TO_CHAR (wkSun + i, 'yyyy-mm-dd')
                           || halfDayLastInvest
                           || item.time_zone,
                           'yyyy-mm-dd hh24:mi TZR');

                     CLOSE holiday_ct;

                     EXIT;
                  ELSE
                     toInsTimeEstClosing :=
                        TO_TIMESTAMP_TZ (
                              TO_CHAR (wkSun + i, 'yyyy-mm-dd')
                           || item.time_est_closing
                           || item.time_zone,
                           'yyyy-mm-dd hh24:mi TZR');
                     toInsTimeLastInvest :=
                        TO_TIMESTAMP_TZ (
                              TO_CHAR (wkSun + i, 'yyyy-mm-dd')
                           || item.time_last_invest
                           || item.time_zone,
                           'yyyy-mm-dd hh24:mi TZR');

                     CLOSE holiday_ct;

                     EXIT;
                  END IF;
               ELSE
                  IF holiday.is_half_day = 1
                  THEN
                     SELECT time_est_closing, time_last_invest
                       INTO halfDayCloseTime, halfDayLastInvest
                       FROM opportunity_templates
                      WHERE     market_id = item.market_id
                            AND scheduled = 2
                            AND is_half_day = 1
                            AND is_active = 1;

                     toInsTimeEstClosing :=
                        TO_TIMESTAMP_TZ (
                              TO_CHAR (wkSun + i, 'yyyy-mm-dd')
                           || halfDayCloseTime
                           || item.time_zone,
                           'yyyy-mm-dd hh24:mi TZR');
                     toInsTimeLastInvest :=
                        TO_TIMESTAMP_TZ (
                              TO_CHAR (wkSun + i, 'yyyy-mm-dd')
                           || halfDayLastInvest
                           || item.time_zone,
                           'yyyy-mm-dd hh24:mi TZR');

                     CLOSE holiday_ct;

                     EXIT;
                  END IF;
               END IF;

               CLOSE holiday_ct;
            END LOOP;
         ELSE
            GOTO end_loop;
         END IF;
      END IF;

      IF NOT (   (item.exchange_id = 1)
              OR (item.exchange_id = 17)
              OR (item.exchange_id = 16)
              OR (item.exchange_id = 18))
      THEN
         SELECT COUNT (id)
           INTO numOfHolidays
           FROM exchange_holidays
          WHERE     exchange_id = item.exchange_id
                AND TRUNC (holiday) >= wkMon
                AND TRUNC (holiday) <= wkFri
                AND is_half_day = 0;

         IF numOfHolidays < 5
         THEN
            FOR i IN 0 .. 4
            LOOP
               OPEN holiday_ct FOR
                  SELECT *
                    FROM exchange_holidays
                   WHERE exchange_id = item.exchange_id
                         AND TRUNC (holiday) = wkMon + i;               -- AND

               --                        is_half_day = 0;
               FETCH holiday_ct INTO holiday;

               IF holiday_ct%NOTFOUND
               THEN                                        -- if not a holiday
                  -- if the jakarta week doesn't open on friday it should open by the wekly template close time
                  -- if its friday open it by daily half day template
                  IF item.market_id = 370 AND i = 4
                  THEN
                     -- if the jakarta week doesn't open on friday it should open by the wekly template close time
                     -- if its friday open it by daily half day template
                     SELECT TO_TIMESTAMP_TZ (
                                  TO_CHAR (wkMon + i, 'yyyy-mm-dd')
                               || time_first_invest
                               || time_zone,
                               'yyyy-mm-dd hh24:mi TZR')
                       INTO toInsTimeFirstInvest
                       FROM opportunity_templates
                      WHERE     market_id = 370
                            AND scheduled = 2
                            AND is_half_day = 1;
                  ELSE
                     toInsTimeFirstInvest :=
                        TO_TIMESTAMP_TZ (
                              TO_CHAR (wkMon + i, 'yyyy-mm-dd')
                           || item.time_first_invest
                           || item.time_zone,
                           'yyyy-mm-dd hh24:mi TZR');
                  END IF;

                  CLOSE holiday_ct;

                  EXIT;
               ELSE
                  IF holiday.is_half_day = 1
                  THEN
                     toInsTimeFirstInvest :=
                        TO_TIMESTAMP_TZ (
                              TO_CHAR (wkMon + i, 'yyyy-mm-dd')
                           || item.time_first_invest
                           || item.time_zone,
                           'yyyy-mm-dd hh24:mi TZR');

                     CLOSE holiday_ct;

                     EXIT;
                  END IF;
               END IF;

               CLOSE holiday_ct;
            END LOOP;

            FOR i IN REVERSE 0 .. 4
            LOOP
               OPEN holiday_ct FOR
                  SELECT *
                    FROM exchange_holidays
                   WHERE exchange_id = item.exchange_id
                         AND TRUNC (holiday) = wkMon + i;

               FETCH holiday_ct INTO holiday;

               IF holiday_ct%NOTFOUND
               THEN                                        -- if not a holiday
                  IF item.market_id = 15 AND i != 4
                  THEN
                     -- if the ILS week doesn't close on friday it should close by the day template close time
                     -- the week template is done for friday when it close at 13:00
                     SELECT TO_TIMESTAMP_TZ (
                                  TO_CHAR (wkMon + i, 'yyyy-mm-dd')
                               || time_est_closing
                               || time_zone,
                               'yyyy-mm-dd hh24:mi TZR'),
                            TO_TIMESTAMP_TZ (
                                  TO_CHAR (wkMon + i, 'yyyy-mm-dd')
                               || time_last_invest
                               || time_zone,
                               'yyyy-mm-dd hh24:mi TZR')
                       INTO toInsTimeEstClosing, toInsTimeLastInvest
                       FROM opportunity_templates
                      WHERE     market_id = 15
                            AND scheduled = 2
                            AND is_half_day = 0;
                  ELSE
                     toInsTimeEstClosing :=
                        TO_TIMESTAMP_TZ (
                              TO_CHAR (wkMon + i, 'yyyy-mm-dd')
                           || item.time_est_closing
                           || item.time_zone,
                           'yyyy-mm-dd hh24:mi TZR');
                     toInsTimeLastInvest :=
                        TO_TIMESTAMP_TZ (
                              TO_CHAR (wkMon + i, 'yyyy-mm-dd')
                           || item.time_last_invest
                           || item.time_zone,
                           'yyyy-mm-dd hh24:mi TZR');
                  END IF;

                  CLOSE holiday_ct;

                  EXIT;
               ELSE
                  IF holiday.is_half_day = 1
                  THEN
                     SELECT time_est_closing, time_last_invest
                       INTO halfDayCloseTime, halfDayLastInvest
                       FROM opportunity_templates
                      WHERE     market_id = item.market_id
                            AND scheduled = 2
                            AND is_half_day = 1
                            AND is_active = 1;

                     toInsTimeEstClosing :=
                        TO_TIMESTAMP_TZ (
                              TO_CHAR (wkMon + i, 'yyyy-mm-dd')
                           || halfDayCloseTime
                           || item.time_zone,
                           'yyyy-mm-dd hh24:mi TZR');
                     toInsTimeLastInvest :=
                        TO_TIMESTAMP_TZ (
                              TO_CHAR (wkMon + i, 'yyyy-mm-dd')
                           || halfDayLastInvest
                           || item.time_zone,
                           'yyyy-mm-dd hh24:mi TZR');

                     CLOSE holiday_ct;

                     EXIT;
                  END IF;
               END IF;

               CLOSE holiday_ct;
            END LOOP;
         ELSE
            GOTO end_loop;
         END IF;
      END IF;

      IF (item.exchange_id = 16)
      THEN
         SELECT COUNT (id)
           INTO numOfHolidays
           FROM exchange_holidays
          WHERE     exchange_id = item.exchange_id
                AND TRUNC (holiday) >= wkSun
                AND TRUNC (holiday) <= wkWed
                AND is_half_day = 0;

         IF numOfHolidays < 4
         THEN
            FOR i IN 0 .. 3
            LOOP
               OPEN holiday_ct FOR
                  SELECT *
                    FROM exchange_holidays
                   WHERE exchange_id = item.exchange_id
                         AND TRUNC (holiday) = wkSun + i;               -- AND

               --                        is_half_day = 0;
               FETCH holiday_ct INTO holiday;

               IF holiday_ct%NOTFOUND
               THEN                                        -- if not a holiday
                  toInsTimeFirstInvest :=
                     TO_TIMESTAMP_TZ (
                           TO_CHAR (wkSun + i, 'yyyy-mm-dd')
                        || item.time_first_invest
                        || item.time_zone,
                        'yyyy-mm-dd hh24:mi TZR');

                  CLOSE holiday_ct;

                  EXIT;
               ELSE
                  IF holiday.is_half_day = 1
                  THEN
                     toInsTimeFirstInvest :=
                        TO_TIMESTAMP_TZ (
                              TO_CHAR (wkSun + i, 'yyyy-mm-dd')
                           || item.time_first_invest
                           || item.time_zone,
                           'yyyy-mm-dd hh24:mi TZR');

                     CLOSE holiday_ct;

                     EXIT;
                  END IF;
               END IF;

               CLOSE holiday_ct;
            END LOOP;

            FOR i IN REVERSE 0 .. 3
            LOOP
               OPEN holiday_ct FOR
                  SELECT *
                    FROM exchange_holidays
                   WHERE exchange_id = item.exchange_id
                         AND TRUNC (holiday) = wkSun + i;

               FETCH holiday_ct INTO holiday;

               IF holiday_ct%NOTFOUND
               THEN                                        -- if not a holiday
                  toInsTimeEstClosing :=
                     TO_TIMESTAMP_TZ (
                           TO_CHAR (wkSun + i, 'yyyy-mm-dd')
                        || item.time_est_closing
                        || item.time_zone,
                        'yyyy-mm-dd hh24:mi TZR');
                  toInsTimeLastInvest :=
                     TO_TIMESTAMP_TZ (
                           TO_CHAR (wkSun + i, 'yyyy-mm-dd')
                        || item.time_last_invest
                        || item.time_zone,
                        'yyyy-mm-dd hh24:mi TZR');

                  CLOSE holiday_ct;

                  EXIT;
               ELSE
                  IF holiday.is_half_day = 1
                  THEN
                     SELECT time_est_closing, time_last_invest
                       INTO halfDayCloseTime, halfDayLastInvest
                       FROM opportunity_templates
                      WHERE     market_id = item.market_id
                            AND scheduled = 2
                            AND is_half_day = 1
                            AND is_active = 1;

                     toInsTimeEstClosing :=
                        TO_TIMESTAMP_TZ (
                              TO_CHAR (wkSun + i, 'yyyy-mm-dd')
                           || halfDayCloseTime
                           || item.time_zone,
                           'yyyy-mm-dd hh24:mi TZR');
                     toInsTimeLastInvest :=
                        TO_TIMESTAMP_TZ (
                              TO_CHAR (wkSun + i, 'yyyy-mm-dd')
                           || halfDayLastInvest
                           || item.time_zone,
                           'yyyy-mm-dd hh24:mi TZR');

                     CLOSE holiday_ct;

                     EXIT;
                  END IF;
               END IF;

               CLOSE holiday_ct;
            END LOOP;
         ELSE
            GOTO end_loop;
         END IF;
      END IF;

      -- check that the last working day of this week is not the same as the last working day of this month
      -- if yes don't create this opp
      IF TRUNC (toInsTimeEstClosing) =
            Month_last_working_day (item.exchange_id, 0)
      THEN
         GOTO end_loop;
      END IF;

      SELECT SEQ_OPPORTUNITIES.NEXTVAL INTO toIns.id FROM DUAL;

      toIns.market_id := item.market_id;
      toIns.time_created := CURRENT_TIMESTAMP;
      toIns.time_first_invest := toInsTimeFirstInvest;
      toIns.time_est_closing := toInsTimeEstClosing;
      toIns.time_last_invest := toInsTimeLastInvest;
      toIns.is_published := 0;
      toIns.is_settled := 0;
      toIns.odds_type_id := item.odds_type_id;
      toIns.writer_id := item.writer_id;
      toIns.opportunity_type_id := item.opportunity_type_id;
      toIns.is_disabled := 0;
      toIns.is_disabled_service := 0;
      toIns.is_disabled_trader := 0;
      toIns.scheduled := item.scheduled;
      toIns.deduct_win_odds := item.deduct_win_odds;
      toIns.is_open_graph := item.is_open_graph;
      toIns.max_exposure := item.max_exposure;
      toIns.Odds_Group := item.odds_group;

      INSERT INTO opportunities
           VALUES toIns;

     --        DBMS_OUTPUT.PUT_LINE('1, ' || item.market_id || ', ' || to_char(current_timestamp, 'yyyy-mm-dd hh24:mi TZR') || ', ' || to_char(toInsTimeFirstInvest, 'yyyy-mm-dd hh24:mi TZR') || ', ' || to_char(toInsTimeEstClosing, 'yyyy-mm-dd hh24:mi TZR') || ', ' || to_char(toInsTimeLastInvest, 'yyyy-mm-dd hh24:mi TZR') || ', ' || 1 || ', ' || 0 || ', ' || item.odds_type_id || ', ' || item.writer_id || ', ' || item.opportunity_type_id || ', ' || 0 || ', ' || item.scheduled || ', ' || item.deduct_win_odds || ', ' || item.max_exposure);
     <<end_loop>>
      NULL;
   END LOOP;
END;
/
