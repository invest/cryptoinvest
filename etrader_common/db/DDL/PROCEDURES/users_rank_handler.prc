CREATE OR REPLACE /* Formatted on 3.12.2013 �. 11:28:03 (QP5 v5.149.1003.31008) */
PROCEDURE USERS_RANK_HANDLER
AS
   RANK_NEWBIES       NUMBER := 1;
   transation_id      NUMBER := 0;
   transaction_time   DATE := NULL;
   user_rank_id       NUMBER := 1;
   num_of_days        NUMBER := 0;
   users_updated      NUMBER := 0;
--This handler takes all the newbies and check if 7 days has passed since first success deposit insert to history.
BEGIN
   SELECT ur.min_num_of_days_in_rank
     INTO num_of_days
     FROM users_rank ur
    WHERE ur.id = RANK_NEWBIES;

   DBMS_OUTPUT.PUT_LINE ('------------------------------------------------:');
   DBMS_OUTPUT.PUT_LINE ('USERS_RANK_HANDLER started at: ' || SYSDATE);

   FOR v_usr
      IN (SELECT u.id AS user_id, u.first_deposit_id AS fdi
            FROM users u
           WHERE u.rank_id = RANK_NEWBIES AND u.first_deposit_id IS NOT NULL)
   LOOP
      --DBMS_OUTPUT.PUT_LINE('user id : ' || v_usr.user_id || '  transaction_id :' || v_usr.fdi);
      SELECT t.time_created
        INTO transaction_time
        FROM transactions t
       WHERE t.id = v_usr.fdi;

      IF (transaction_time < SYSDATE - num_of_days)
      THEN
         SELECT tran_id
           INTO transation_id
           FROM (  SELECT t.id AS tran_id
                     FROM transactions t, transaction_types tt
                    WHERE     t.type_id = tt.id
                          AND tt.class_type = 1
                          AND t.status_id IN (2, 7, 8)
                          AND t.user_id = v_usr.user_id
                 ORDER BY t.time_created DESC)
          WHERE ROWNUM = 1;

         SELECT ur.id
           INTO user_rank_id
           FROM (SELECT SUM ( (t.amount / 100) * t.rate) AS userAmount
                   FROM transactions t, transaction_types tt
                  WHERE     t.user_id = v_usr.user_id
                        AND t.type_id = tt.id
                        AND tt.class_type = 1
                        AND t.status_id IN (2, 8, 7)) tab,
                users_rank ur
          WHERE (tab.userAmount > ur.min_sum_of_deposits
                 AND tab.userAmount <= ur.max_sum_of_deposits)
                OR (tab.userAmount > ur.min_sum_of_deposits
                    AND ur.max_sum_of_deposits IS NULL);

         UPDATE users
            SET rank_id = user_rank_id
          WHERE id = v_usr.user_id;

         INSERT INTO users_rank_hist (ID,
                                      USER_ID,
                                      USER_RANK_ID,
                                      QUALIFICATION_DATE,
                                      transaction_id)
              VALUES (SEQ_USERS_RANK_HIST.NEXTVAL,
                      v_usr.user_id,
                      user_rank_id,
                      SYSDATE,
                      transation_id);

         users_updated := users_updated + 1;
      END IF;
   END LOOP;

   DBMS_OUTPUT.PUT_LINE ('users updated : ' || users_updated);
   DBMS_OUTPUT.PUT_LINE ('USERS_RANK_HANDLER finished at: ' || SYSDATE);
   DBMS_OUTPUT.PUT_LINE ('------------------------------------------------:');
   NULL;
EXCEPTION
   WHEN NO_DATA_FOUND
   THEN
      DBMS_OUTPUT.put_line ('NO DATA FOUND: EXECUTION SUCCESSEFUL');
   WHEN OTHERS
   THEN
      DBMS_OUTPUT.put_line ('UNKNOWN ERROR');
END USERS_RANK_HANDLER;
/
