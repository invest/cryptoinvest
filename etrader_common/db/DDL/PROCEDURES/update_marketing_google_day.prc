CREATE OR REPLACE /* Formatted on 3.12.2013 �. 11:28:02 (QP5 v5.149.1003.31008) */
PROCEDURE "UPDATE_MARKETING_GOOGLE_DAY" (date_of_data DATE)
AS
   is_check_existing_data   NUMBER := 1;
   is_existing_data         NUMBER := 0;
   rows_count               NUMERIC := 0;
BEGIN
   IF (is_check_existing_data = 1)
   THEN
      SELECT COUNT (*)
        INTO is_existing_data
        FROM Marketing_Google_Report m
       WHERE date_of_data = m.day;
   END IF;

   IF (is_existing_data = 0)
   THEN
      DELETE FROM marketing_google_users_dp;

      INSERT INTO marketing_google_users_dp
         SELECT DISTINCT
                u.dynamic_param,
                u.combination_id,
                mu.is_keep_daily_data AS is_google_user
           FROM users u,
                (SELECT mc.id, ms.is_keep_daily_data
                   FROM marketing_combinations mc,
                        marketing_campaigns mca,
                        marketing_payment_recipients mp,
                        marketing_sources ms
                  WHERE     mc.campaign_id = mca.id
                        AND mca.payment_recipient_id = mp.id
                        AND ms.id = mca.source_id
                        AND ms.is_keep_daily_data IN (0, 1)) mu
          WHERE u.class_id <> 0 AND u.combination_id = mu.id;

      FOR v_data
         IN (SELECT DISTINCT u.dynamic_param,
                             u.combination_id,
                             clk.clicks_num,
                             reg_users.registered_users,
                             dep_users.reg_frst_dep_users,
                             dep_users.frst_dep_users,
                             rem_users.frst_rem_dep_users,
                             sums_transactions.sum_deposits,
                             sums_investments.turnover,
                             sums_investments.house_win,
                             u.is_google_user
               FROM marketing_google_users_dp u
                    LEFT JOIN (  SELECT u.dynamic_param,
                                        u.combination_id,
                                        COUNT (*) AS registered_users
                                   FROM users u,
                                        marketing_google_users_dp m
                                  WHERE u.class_id <> 0
                                        AND u.time_created >=
                                               TRUNC (date_of_data)
                                        AND u.time_created <
                                               TRUNC (date_of_data + 1)
                                        AND u.combination_id =
                                               m.combination_id
                                        AND NVL (u.dynamic_param, 0) =
                                               NVL (m.dynamic_param, 0)
                               GROUP BY u.dynamic_param,
                                        u.combination_id) reg_users
                       ON NVL (u.dynamic_param, 0) =
                             NVL (reg_users.dynamic_param, 0)
                          AND u.combination_id =
                                 reg_users.combination_id
                    LEFT JOIN (  SELECT u.dynamic_param,
                                        u.combination_id,
                                        COUNT (*) AS frst_dep_users,
                                        SUM (
                                           CASE
                                              WHEN TRUNC (
                                                      u.time_created) =
                                                      TRUNC (
                                                         date_of_data)
                                              THEN
                                                 1
                                              ELSE
                                                 0
                                           END)
                                           AS reg_frst_dep_users
                                   FROM users u,
                                        transactions t,
                                        marketing_google_users_dp m
                                  WHERE u.first_deposit_id = t.id
                                        AND t.time_created >=
                                               TRUNC (date_of_data)
                                        AND t.time_created <
                                               TRUNC (date_of_data + 1)
                                        AND u.class_id <> 0
                                        AND u.combination_id =
                                               m.combination_id
                                        AND NVL (u.dynamic_param, 0) =
                                               NVL (m.dynamic_param, 0)
                               GROUP BY u.dynamic_param,
                                        u.combination_id) dep_users
                       ON NVL (u.dynamic_param, 0) =
                             NVL (dep_users.dynamic_param, 0)
                          AND u.combination_id =
                                 dep_users.combination_id
                    LEFT JOIN (  SELECT u.dynamic_param,
                                        u.combination_id,
                                        COUNT (*) AS frst_rem_dep_users,
                                        SUM (
                                           CASE
                                              WHEN TRUNC (
                                                      u.time_created) =
                                                      TRUNC (
                                                         date_of_data)
                                              THEN
                                                 1
                                              ELSE
                                                 0
                                           END)
                                           AS reg_rem_frst_dep_users
                                   FROM users u,
                                        transactions t,
                                        marketing_google_users_dp m
                                  WHERE u.first_deposit_id = t.id
                                        AND t.time_created >=
                                               TRUNC (date_of_data)
                                        AND t.time_created <
                                               TRUNC (date_of_data + 1)
                                        AND u.class_id <> 0
                                        AND u.id IN
                                               (SELECT DISTINCT
                                                       (rl.user_id)
                                                  FROM remarketing_logins rl
                                                 WHERE user_id > 0)
                                        AND u.combination_id =
                                               m.combination_id
                                        AND NVL (u.dynamic_param, 0) =
                                               NVL (m.dynamic_param, 0)
                               GROUP BY u.dynamic_param,
                                        u.combination_id) rem_users
                       ON NVL (u.dynamic_param, 0) =
                             NVL (rem_users.dynamic_param, 0)
                          AND u.combination_id =
                                 rem_users.combination_id
                    LEFT JOIN (  SELECT cl.dynamic_param,
                                        cl.combination_id,
                                        COUNT (*) AS clicks_num
                                   FROM clicks cl,
                                        marketing_google_users_dp m
                                  WHERE cl.time_created >=
                                           TRUNC (date_of_data)
                                        AND cl.time_created <
                                               TRUNC (date_of_data + 1)
                                        AND cl.combination_id =
                                               m.combination_id
                                        AND NVL (cl.dynamic_param, 0) =
                                               NVL (m.dynamic_param, 0)
                               GROUP BY cl.dynamic_param,
                                        cl.combination_id) clk
                       ON NVL (u.dynamic_param, 0) =
                             NVL (clk.dynamic_param, 0)
                          AND u.combination_id = clk.combination_id
                    LEFT JOIN (  SELECT /*+ index(t TRANSACTIONS_IDX_TIME_C) */
                                        u.dynamic_param,
                                        u.combination_id,
                                        SUM (t.amount * t.rate / 100)
                                           AS sum_deposits
                                   FROM users u,
                                        transactions t,
                                        transaction_types tt,
                                        marketing_google_users_dp m
                                  WHERE     u.id = t.user_id
                                        AND t.type_id = tt.id
                                        AND tt.class_type = 1
                                        AND t.status_id IN (2, 7, 8)
                                        AND u.class_id <> 0
                                        AND u.combination_id =
                                               m.combination_id
                                        AND NVL (u.dynamic_param, 0) =
                                               NVL (m.dynamic_param, 0)
                                        AND t.time_created >=
                                               TRUNC (date_of_data)
                                        AND t.time_created <
                                               TRUNC (date_of_data + 1)
                               GROUP BY u.dynamic_param,
                                        u.combination_id) sums_transactions
                       ON NVL (u.dynamic_param, 0) =
                             NVL (sums_transactions.dynamic_param, 0)
                          AND u.combination_id =
                                 sums_transactions.combination_id
                    LEFT JOIN (  SELECT /*+ index(i INVESTMENTS_TIME_CREATED) */
                                        u.dynamic_param,
                                        u.combination_id,
                                        SUM (i.amount * i.rate / 100)
                                           AS turnover,
                                        SUM (
                                             i.house_result
                                           * i.rate
                                           / 100)
                                           AS house_win
                                   FROM users u,
                                        investments i,
                                        marketing_google_users_dp m
                                  WHERE     u.id = i.user_id
                                        AND i.is_settled = 1
                                        AND i.is_canceled = 0
                                        AND u.class_id <> 0
                                        AND u.combination_id =
                                               m.combination_id
                                        AND NVL (u.dynamic_param, 0) =
                                               NVL (m.dynamic_param, 0)
                                        AND i.time_created >=
                                               TRUNC (date_of_data)
                                        AND i.time_created <
                                               TRUNC (date_of_data + 1)
                               GROUP BY u.dynamic_param,
                                        u.combination_id) sums_investments
                       ON NVL (u.dynamic_param, 0) =
                             NVL (sums_investments.dynamic_param, 0)
                          AND u.combination_id =
                                 sums_investments.combination_id
              WHERE    reg_users.registered_users IS NOT NULL
                    OR dep_users.reg_frst_dep_users IS NOT NULL
                    OR rem_users.frst_rem_dep_users IS NOT NULL
                    OR dep_users.frst_dep_users IS NOT NULL
                    OR sums_transactions.sum_deposits IS NOT NULL
                    OR sums_investments.turnover IS NOT NULL
                    OR sums_investments.house_win IS NOT NULL
                    OR clk.clicks_num IS NOT NULL)
      LOOP
         -- Insert row.
         INSERT INTO Marketing_Google_Report
              VALUES (TRUNC (date_of_data),
                      v_data.dynamic_param,
                      v_data.combination_id,
                      v_data.clicks_num,
                      v_data.registered_users,
                      v_data.reg_frst_dep_users,
                      v_data.frst_dep_users,
                      v_data.sum_deposits,
                      v_data.turnover,
                      v_data.house_win,
                      v_data.frst_rem_dep_users,
                      v_data.is_google_user);

         rows_count := rows_count + 1;
      END LOOP;

      COMMIT;
      DBMS_OUTPUT.
       put_line (
         'There were ' || rows_count || ' rows added for ' || date_of_data);
   ELSE
      DBMS_OUTPUT.
       put_line (
         'ERROR! There is already an existing data for ' || date_of_data);
   END IF;
END UPDATE_MARKETING_GOOGLE_DAY;
/
