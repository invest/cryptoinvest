create or replace TRIGGER ETRADER.AFTER_TRAC_STATIC
AFTER INSERT
ON ETRADER.MARKETING_TRACKING
REFERENCING NEW AS NEW OLD AS OLD
FOR EACH ROW
DECLARE 

CURSOR c_main IS
select mts.http_referer, mts.dynamic_param, mts.combination_id from marketing_tracking_static mts where :NEW.marketing_tracking_static_id = mts.ID;

cursor c_user is 
select writer_id from users where id = :NEW.user_id;

cursor c_contact is 
select writer_id from contacts where id = :NEW.contact_id;

v_http VARCHAR2(4000 char);
v_dyn NVARCHAR2(1000);
v_comb NUMBER;
v_is_API_contact number;
v_writer_id NUMBER;
--add all non web writers, able to register users here
/*writer_BUBBLES_WEB NUMBER:=21000;
writer_AO_MINISITE NUMBER:=23000;	
writer_CO_MINISITE NUMBER:=24000;	*/
writer_BUBBLES_MOBILE NUMBER:=25000;
writer_BUBBLES_BDA NUMBER:=26000;
writer_COPYOP_WEB NUMBER:=10000;
writer_COPYOP_MOBILE NUMBER:=20000;
writer_MOBILE NUMBER:=200;

BEGIN

          select
               1 into v_is_API_contact 
           from
               contacts c
          where
               c.id = :NEW.CONTACT_ID
               and c.writer_id = 868;
               
          EXCEPTION                  
            WHEN no_data_found THEN
                  v_is_API_contact := 0;    


IF :NEW.MARKETING_TRACKING_ACTIVITY_ID = 4 AND :NEW.USER_ID <> 0 AND  v_is_API_contact= 0 THEN

open  c_user;
fetch  c_user into v_writer_id;
close  c_user;

if v_writer_id not in (writer_BUBBLES_MOBILE, writer_BUBBLES_BDA, writer_COPYOP_WEB, writer_COPYOP_MOBILE, writer_MOBILE) then
open  c_main;
fetch  c_main into v_http, v_dyn, v_comb;
close  c_main;
update USERS u set u.http_referer = v_http, u.dynamic_param = v_dyn, u.combination_id = v_comb WHERE u.id = :NEW.USER_ID;
end if;
END IF;

IF :NEW.MARKETING_TRACKING_ACTIVITY_ID in(2,3) AND :NEW.CONTACT_ID <> 0 AND  v_is_API_contact= 0 THEN

open  c_contact;
fetch  c_contact into v_writer_id;
close  c_contact;

if v_writer_id not in (writer_BUBBLES_MOBILE, writer_BUBBLES_BDA, writer_COPYOP_WEB, writer_COPYOP_MOBILE, writer_MOBILE) then

open  c_main;
fetch  c_main into v_http, v_dyn, v_comb;
close  c_main;
 
update CONTACTS c set c.http_referer = v_http, c.dynamic_parameter = v_dyn, c.combination_id = v_comb WHERE c.id = :NEW.CONTACT_ID;

end if;
END IF;

END;