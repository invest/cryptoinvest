create or replace TRIGGER AFTER_UAD_MARKETING
  AFTER UPDATE
    OF AFF_SUB1
      ,AFF_SUB2
      ,AFF_SUB3
    ON ETRADER.USERS_ACTIVE_DATA 
  REFERENCING NEW AS NEW OLD AS OLD
  FOR EACH ROW
DECLARE 
  T_marketing_att_obj_old MARKETING_ATTRIBUTION_OBJ;
  T_marketing_att_obj_new MARKETING_ATTRIBUTION_OBJ;
BEGIN
  T_marketing_att_obj_old := MARKETING_ATTRIBUTION_OBJ(:OLD.USER_ID, 0, 0, null, :OLD.AFF_SUB1, :OLD.AFF_SUB2, :OLD.AFF_SUB3, 0, null, 0, null);
  T_marketing_att_obj_new := MARKETING_ATTRIBUTION_OBJ(:NEW.USER_ID, 0, 0, null, :NEW.AFF_SUB1, :NEW.AFF_SUB2, :NEW.AFF_SUB3, 0, null, 0, null);
  MARKETING.UPDATE_MARKETING_ATT_AFF(T_marketing_att_obj_old, T_marketing_att_obj_new);
END;
/
