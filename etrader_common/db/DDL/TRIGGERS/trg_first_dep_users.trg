CREATE OR REPLACE TRIGGER 
"ETRADER"."TRG_FIRST_DEP_USERS" AFTER UPDATE OF "FIRST_DEPOSIT_ID" ON "ETRADER"."USERS" FOR EACH ROW 
declare
  l_count number := 0;
begin
  select count(*)
  into   l_count
  from   marketing_tracking
  where  user_id = :new.id
  and    marketing_tracking_activity_id = 5;

  if l_count = 0
  then
  
    insert into marketing_tracking
      (id
      ,marketing_tracking_static_id
      ,combination_id_dynamic
      ,time_dynamic
      ,http_referer_dynamic
      ,dynamic_param_dynamic
      ,contact_id
      ,user_id
      ,marketing_tracking_activity_id)
      select seq_marketing_tracking.nextval
            ,marketing_tracking_static_id
            ,combination_id_dynamic
            ,time_dynamic
            ,http_referer_dynamic
            ,dynamic_param_dynamic
            ,contact_id
            ,user_id
            ,5
      from   (select marketing_tracking_static_id
                    ,combination_id_dynamic
                    ,time_dynamic
                    ,http_referer_dynamic
                    ,dynamic_param_dynamic
                    ,contact_id
                    ,user_id
              from   marketing_tracking
              where  user_id = :new.id
              order  by id desc)
      where  rownum <= 1;
  
  end if;

  if :old.first_deposit_id is null
     and :new.first_deposit_id is not null
  then
   --User Regulation
    update users_regulation
    set    approved_regulation_step = 2
    where  user_id = :new.id
    and    approved_regulation_step < 2;
   --Send email after FDT    
    PKG_USERS_AUTO_MAIL.INSERT_USER_AUTO_MAIL(:new.ID, 1);
   --Send GBG 
    PKG_USERFILES.CREATE_GBG_RECORD(:new.ID, :new.COUNTRY_ID);
  
  end if;

end trg_first_dep_users;