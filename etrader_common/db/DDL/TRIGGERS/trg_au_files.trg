create or replace trigger trg_au_files
  after update on files
  referencing new as new old as old
  for each row
begin
  if updating
     and :new.uploader_id <> 0
     and :new.file_status_id <> 3
  then
    if pkg_audit.dif(:new.file_type_id, :old.file_type_id)
       or pkg_audit.dif(:new.writer_id, :old.writer_id)
       or pkg_audit.dif(:new.reference, :old.reference)
       or pkg_audit.dif(:new.file_name, :old.file_name)
       or pkg_audit.dif(:new.utc_offset_created, :old.utc_offset_created)
       or pkg_audit.dif(:new.cc_id, :old.cc_id)
       or pkg_audit.dif(:new.is_approved, :old.is_approved)
       or pkg_audit.dif(:new.file_status_id, :old.file_status_id)
       or pkg_audit.dif(:new.id_number, :old.id_number)
       or pkg_audit.dif(:new.is_color, :old.is_color)
       or pkg_audit.dif(:new.is_approved_control, :old.is_approved_control)
       or pkg_audit.dif(:new.is_control_rejected, :old.is_control_rejected)
       or pkg_audit.dif(:new.is_support_rejected, :old.is_support_rejected)
       or pkg_audit.dif(:new.reject_reason_id, :old.reject_reason_id)
       or pkg_audit.dif(:new.comments, :old.comments)
       or pkg_audit.dif(:new.rejection_writer_id, :old.rejection_writer_id)
       or pkg_audit.dif(:new.rejection_date, :old.rejection_date)
       or pkg_audit.dif(:new.uploader_id, :old.uploader_id)
       or pkg_audit.dif(:new.time_updated, :old.time_updated)
       or pkg_audit.dif(:new.issue_action_id, :old.issue_action_id)
       or pkg_audit.dif(:new.first_name, :old.first_name)
       or pkg_audit.dif(:new.last_name, :old.last_name)
       or pkg_audit.dif(:new.exp_date, :old.exp_date)
       or pkg_audit.dif(:new.country_id, :old.country_id)
       or pkg_audit.dif(:new.support_approved_writer_id, :old.support_approved_writer_id)
       or pkg_audit.dif(:new.time_support_approved, :old.time_support_approved)
       or pkg_audit.dif(:new.control_approved_writer_id, :old.control_approved_writer_id)
       or pkg_audit.dif(:new.time_control_approved, :old.time_control_approved)
       or pkg_audit.dif(:new.control_reject_writer_id, :old.control_reject_writer_id)
       or pkg_audit.dif(:new.time_control_rejected, :old.time_control_rejected)
       or pkg_audit.dif(:new.control_rejection_reason_id, :old.control_rejection_reason_id)
       or pkg_audit.dif(:new.control_rejection_comments, :old.control_rejection_comments)
       or pkg_audit.dif(:new.ks_status_id, :old.ks_status_id)
       or pkg_audit.dif(:new.ks_status_date, :old.ks_status_date)
       or pkg_audit.dif(:new.time_uploaded, :old.time_uploaded)
    then
      insert into files_history
        (id
        ,file_id
        ,file_type_id
        ,writer_id
        ,reference
        ,file_name
        ,utc_offset_created
        ,cc_id
        ,is_approved
        ,file_status_id
        ,id_number
        ,is_color
        ,is_approved_control
        ,is_control_rejected
        ,is_support_rejected
        ,reject_reason_id
        ,comments
        ,rejection_writer_id
        ,rejection_date
        ,uploader_id
        ,time_updated
        ,issue_action_id
        ,first_name
        ,last_name
        ,exp_date
        ,country_id
        ,support_approved_writer_id
        ,time_support_approved
        ,control_approved_writer_id
        ,time_control_approved
        ,control_reject_writer_id
        ,time_control_rejected
        ,trig_event
        ,control_rejection_reason_id
        ,control_rejection_comments
        ,ks_status_id
        ,ks_status_date
        ,time_uploaded)
      values
        (seq_files_history.nextval
        ,:new.id
        ,:new.file_type_id
        ,:new.writer_id
        ,:new.reference
        ,:new.file_name
        ,:new.utc_offset_created
        ,:new.cc_id
        ,:new.is_approved
        ,:new.file_status_id
        ,:new.id_number
        ,:new.is_color
        ,:new.is_approved_control
        ,:new.is_control_rejected
        ,:new.is_support_rejected
        ,:new.reject_reason_id
        ,:new.comments
        ,:new.rejection_writer_id
        ,:new.rejection_date
        ,:new.uploader_id
        ,:new.time_updated
        ,:new.issue_action_id
        ,:new.first_name
        ,:new.last_name
        ,:new.exp_date
        ,:new.country_id
        ,:new.support_approved_writer_id
        ,:new.time_support_approved
        ,:new.control_approved_writer_id
        ,:new.time_control_approved
        ,:new.control_reject_writer_id
        ,:new.time_control_rejected
        ,'UPDATE'
        ,:new.control_rejection_reason_id
        ,:new.control_rejection_comments
        ,:new.ks_status_id
        ,:new.ks_status_date
        ,:new.time_uploaded);
    end if;
  end if;

  if pkg_audit.dif(:new.file_name, :old.file_name)
  then
    update users_regulation set not_interested = 2 where user_id = :new.user_id;
  end if;
end;
/
