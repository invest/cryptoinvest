create or replace  TRIGGER etrader.trg_au_users_pci
  AFTER UPDATE OF ID_NUM, PASSWORD ON etrader.USERS FOR EACH ROW
BEGIN
  IF pkg_audit.dif(:OLD.ID_NUM, :NEW.ID_NUM) or pkg_audit.dif(:OLD.PASSWORD, :NEW.PASSWORD)
  THEN
    delete ENCRYPTION_MIGRATION_USERS where user_id=:NEW.ID;
  END if;
END;
/