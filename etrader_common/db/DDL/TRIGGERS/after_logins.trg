CREATE OR REPLACE TRIGGER AFTER_LOGINS
AFTER INSERT ON LOGINS
REFERENCING OLD AS old NEW AS new 
FOR EACH ROW
declare

cursor email_alert_c is
select ua.user_id, ua.alert_email_subject, ua.alert_email_recepients, u.user_name
from users_alerts ua, users u 
where ua.user_id = :new.user_id
  and ua.alert_type=2
  and ua.user_id=u.id;
  
user_id  number;
alert_email_subject varchar2(100);
alert_email_recepients varchar2(4000);
user_name varchar2(100);

BEGIN
  open email_alert_c;
  fetch email_alert_c into user_id, alert_email_subject, alert_email_recepients, user_name;
  if email_alert_c%FOUND then
    insert into users_alerts_log values(SEQ_USERS_ALERT_LOG.nextval, :new.user_id, '2', user_name||': '||alert_email_subject, user_name||': '||alert_email_subject||' user login ', alert_email_recepients, 0, null, systimestamp);
  end if;
  close email_alert_c;
END;
/
