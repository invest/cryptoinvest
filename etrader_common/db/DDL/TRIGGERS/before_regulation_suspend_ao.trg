create or replace
TRIGGER BEFORE_REGULATION_SUSPEND_AO
BEFORE INSERT OR UPDATE
ON USERS_REGULATION
REFERENCING NEW AS NEW OLD AS OLD
FOR EACH ROW

DECLARE

   type ARRAY_X_LOW is varray(20) OF INTEGER;--1-ILS, 2-USD, 3-EUR, 4-GBP, 5-TRY, 6-RUB, 7-CNY, 8-KRW, 9-SEK, 10-AUD, 11-ZAR, 12-n/a,13-n/a,14-n/a,15-n/a
   V_ARRAY_X_LOW ARRAY_X_LOW := ARRAY_X_LOW(-1, 500000, 500000, 350000, 1500000, 30000000, -1, -1, 4000000, 700000, 7000000, -1, -1, -1, -1);
   ----
   type ARRAY_Y_HIGH is varray(20) OF INTEGER;--1-ILS, 2-USD, 3-EUR, 4-GBP, 5-TRY, 6-RUB, 7-CNY, 8-KRW, 9-SEK, 10-AUD, 11-ZAR, 12-n/a,13-n/a,14-n/a,15-n/a
   V_ARRAY_Y_HIGH ARRAY_Y_HIGH := ARRAY_Y_HIGH(-1, 1000000, 1000000, 650000, 3000000, 60000000, -1, -1, 8500000, 1400000, 14000000, -1, -1, -1, -1);
   ----
   type ARRAY_FAILED is varray(20) OF INTEGER;--1-ILS, 2-USD, 3-EUR, 4-GBP, 5-TRY, 6-RUB, 7-CNY, 8-KRW, 9-SEK, 10-AUD, 11-ZAR, 12-n/a,13-n/a,14-n/a,15-n/a
   V_ARRAY_FAILED ARRAY_FAILED := ARRAY_FAILED(-1, 500000, 500000, 350000, 1500000, 30000000, -1, -1, 4000000, 700000, 7000000, -1, -1, -1, -1);


 C_SUSPEND_TRESHOLD_X_LOW INTEGER;
 C_SUSPEND_TRESHOLD_Y_HIGH INTEGER;
 C_SUSPEND_TRESHOLD_FAILED INTEGER;

  v_issue_id number;
  v_comment varchar2(4000);
  v_SUSPEND_TRESHOLD_BLOCK_AMNT  number :=0;
  v_SUSPEND_REASON number :=0;

  cursor c_get_user_curr is
  select  c.default_symbol
  from currencies c
  where id = :new.threshold_curr;


  v_curr_id INTEGER :=3;
  v_symbol VARCHAR2(20 CHAR):='eur';

  USER_ONCE_UNBLOCKED CONSTANT number := -1;
  v_amnt_txt VARCHAR2(100):= '';

BEGIN

  IF :NEW.SCORE_GROUP in (5,6,7) AND nvl(:OLD.THRESHOLD,-1) != nvl(:NEW.THRESHOLD,-1) AND nvl(:OLD.THRESHOLD_BLOCK,0) != USER_ONCE_UNBLOCKED AND nvl(:OLD.THRESHOLD_BLOCK,0) = 0 THEN
          open c_get_user_curr;
          fetch c_get_user_curr into  v_symbol;
          close c_get_user_curr;
          v_curr_id := :new.threshold_curr;

          IF(:NEW.SCORE_GROUP = 7) THEN
          --7 FAILeD
             C_SUSPEND_TRESHOLD_FAILED := V_ARRAY_FAILED(v_curr_id);
            if(C_SUSPEND_TRESHOLD_FAILED > 0 AND (:NEW.THRESHOLD >= C_SUSPEND_TRESHOLD_FAILED AND :NEW.SCORE_GROUP = 7))  THEN
              v_SUSPEND_TRESHOLD_BLOCK_AMNT :=C_SUSPEND_TRESHOLD_FAILED;
              v_SUSPEND_REASON := 13;
             end if;
          ELSE
          --5
            C_SUSPEND_TRESHOLD_X_LOW := V_ARRAY_X_LOW(v_curr_id);
            if(C_SUSPEND_TRESHOLD_X_LOW > 0 AND (:NEW.THRESHOLD >= C_SUSPEND_TRESHOLD_X_LOW AND :NEW.SCORE_GROUP = 5))  THEN
              v_SUSPEND_TRESHOLD_BLOCK_AMNT :=C_SUSPEND_TRESHOLD_X_LOW;
              v_SUSPEND_REASON := 13;
             end if;
              --6
            C_SUSPEND_TRESHOLD_Y_HIGH := V_ARRAY_Y_HIGH(v_curr_id);
              if(C_SUSPEND_TRESHOLD_Y_HIGH > 0 AND (:NEW.THRESHOLD >= C_SUSPEND_TRESHOLD_Y_HIGH AND :NEW.SCORE_GROUP = 6))  THEN
                v_SUSPEND_TRESHOLD_BLOCK_AMNT :=C_SUSPEND_TRESHOLD_Y_HIGH;
                v_SUSPEND_REASON := 14;
              end if;
          END IF;

         IF(v_SUSPEND_TRESHOLD_BLOCK_AMNT > 0) THEN
         --USERS WILL BE TRESHOLD BLOCK

           if(v_symbol = 'kr') then
             v_amnt_txt:= trim(TO_CHAR(v_SUSPEND_TRESHOLD_BLOCK_AMNT/100,'999,999,999,999'))||v_symbol;
           else
             v_amnt_txt:= v_symbol ||trim(TO_CHAR(v_SUSPEND_TRESHOLD_BLOCK_AMNT/100,'999,999,999,999'));
           end if;

           v_comment := 'User is suspended due to '||v_amnt_txt|| ' total investments loss';
           insert into issues (id, user_id, subject_id, priority_id, status_id, is_significant, time_created)
           values (seq_issues.nextval, :NEW.USER_ID, 70, 3, 3, 1, sysdate) returning id into v_issue_id;

           insert into issue_actions (id, issue_id, writer_id, action_time, action_time_offset, comments, is_significant, issue_action_type_id, CHANNEL_ID)
           values (seq_issue_actions.nextval, v_issue_id, 0, sysdate, 'GMT+00:00', v_comment, 1, 68, 7);

           :NEW.THRESHOLD_BLOCK := v_SUSPEND_TRESHOLD_BLOCK_AMNT;
           :NEW.COMMENTS := v_comment;

            insert into users_reg_activation_mail
              (id, user_id, suspended_reason_id,  is_send)
            values
              (seq_users_reg_activation_mail.nextval, :new.user_id, v_SUSPEND_REASON, 0);
         END IF;

 END IF;
  EXCEPTION
    WHEN OTHERS THEN
         dbms_output.put_line(SQLCODE);

END;