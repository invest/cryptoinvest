create or replace trigger trg_before_update_ecb_rates
  before update on currencies_rates_to_eur
  for each row
declare
begin
  insert into currencies_rates_to_eur_hist
    (currency_code, rate, dt_created_origin, dt_created)
  values
    (:old.currency_code, :old.rate, :old.dt_created, sysdate);

end trg_before_update_ecb_rates;

/