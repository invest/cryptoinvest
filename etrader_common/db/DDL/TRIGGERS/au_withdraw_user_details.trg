CREATE OR REPLACE TRIGGER 
"ETRADER"."AU_WITHDRAW_USER_DETAILS" AFTER UPDATE ON "ETRADER"."WITHDRAW_USER_DETAILS" FOR EACH ROW
begin
  --Beneficiary Name 32
IF nvl(:old.beneficiary_name,'x') != nvl(:new.beneficiary_name,'x') THEN
  PKG_USER_HISTORY.CREATE_USER_DETAIL_HISTORY(    
    :new.user_id
   ,:new.updated_writer_id
   ,32
   ,:old.beneficiary_name
   ,:new.beneficiary_name);
END IF;
--Swift 33
IF nvl(:old.swift,'x') != nvl(:new.swift,'x') THEN
  PKG_USER_HISTORY.CREATE_USER_DETAIL_HISTORY(    
    :new.user_id
   ,:new.updated_writer_id
   ,33
   ,:old.swift
   ,:new.swift);
END IF;
--Iban 34
IF nvl(:old.iban,'x') != nvl(:new.iban,'x') THEN
  PKG_USER_HISTORY.CREATE_USER_DETAIL_HISTORY(    
    :new.user_id
   ,:new.updated_writer_id
   ,34
   ,:old.iban
   ,:new.iban);
END IF;
--Account number 35
IF nvl(:old.account_num,0) != nvl(:new.account_num,0) THEN
  PKG_USER_HISTORY.CREATE_USER_DETAIL_HISTORY(    
    :new.user_id
   ,:new.updated_writer_id
   ,35
   ,:old.account_num
   ,:new.account_num);
END IF;
--Bank name 36
IF nvl(:old.bank_name,'x') != nvl(:new.bank_name,'x') THEN
  PKG_USER_HISTORY.CREATE_USER_DETAIL_HISTORY(    
    :new.user_id
   ,:new.updated_writer_id
   ,36
   ,:old.bank_name
   ,:new.bank_name);
END IF;
--Branch number 37
IF nvl(:old.branch_number,0) != nvl(:new.branch_number,0) THEN
  PKG_USER_HISTORY.CREATE_USER_DETAIL_HISTORY(    
    :new.user_id
   ,:new.updated_writer_id
   ,37
   ,:old.branch_number
   ,:new.branch_number);
END IF;
--Branch address 38
IF nvl(:old.branch_address,'x') != nvl(:new.branch_address,'x') THEN
  PKG_USER_HISTORY.CREATE_USER_DETAIL_HISTORY(    
    :new.user_id
   ,:new.updated_writer_id
   ,38
   ,:old.branch_address
   ,:new.branch_address);
END IF;
--Skrill email 39
IF nvl(:old.skrill_email,'x') != nvl(:new.skrill_email,'x') THEN
  PKG_USER_HISTORY.CREATE_USER_DETAIL_HISTORY(    
    :new.user_id
   ,:new.updated_writer_id
   ,39
   ,:old.skrill_email
   ,:new.skrill_email);
END IF;
end;
