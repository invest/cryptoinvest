create or replace trigger before_ins_users
  before insert on users
  for each row
declare
  
  CURSOR c_main is
  select 1 is_not_regulated
  from skins
  where id=:new.skin_id
    and is_regulated!=1
    and id not in( 25,26);
    
  l_netrefer_combination number;
  l_is_not_regulated_not_GB number := 0;
  
begin
  -- for API we are reciving only this format API_XXXXXX_YYYYYY_specialCode_OtherParams
  -- XXXXXX - sub affiliate key
  -- YYYYYY - affiliate key
  -- SpecialCode - offline code on Netrefer
  -- OtherParams - other tracking params
  if regexp_like(:new.dynamic_param, '^(API|BR)_\d{6}_\d{6}_.*')
  then

    select count(*)
    into   l_netrefer_combination
    from   marketing_combinations mc
    where  mc.id = :new.combination_id
    and    mc.campaign_id = 365;

    if l_netrefer_combination > 0
    then
      -- Convert delimited string to array
      :new.special_code  := regexp_substr(:new.dynamic_param, '[^_]+', 1, 4);
      :new.affiliate_key := regexp_substr(:new.dynamic_param, '[^_]+', 1, 3);
    end if;

  end if;
  
  open c_main;
  fetch c_main into l_is_not_regulated_not_GB;
  close c_main;
  
  if l_is_not_regulated_not_GB = 1 then
    raise_application_error(-20000, '!!!detected attempt for registration from non regulated skin and not Gold Beam!!!');
  end if;
end;
/
