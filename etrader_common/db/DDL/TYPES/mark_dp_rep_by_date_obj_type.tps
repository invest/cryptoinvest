CREATE OR REPLACE /* Formatted on 3.12.2013 �. 11:34:02 (QP5 v5.149.1003.31008) */
TYPE "MARK_DP_REP_BY_DATE_OBJ_TYPE" IS OBJECT
(dates DATE,
 DP NVARCHAR2 (1000),
 campaign_manager VARCHAR2 (30),
 combination_id NUMBER,
 campaign_name VARCHAR2 (200),
 source_name VARCHAR2 (50),
 medium_name VARCHAR2 (50),
 content_name VARCHAR2 (50),
 marketing_size_horizontal NUMBER,
 marketing_size_vertical NUMBER,
 m_location VARCHAR2 (4000),
 landing_page_name VARCHAR2 (50),
 SKIN_ID NUMBER,
 short_reg NUMBER,
 registered_users_num NUMBER,
 FTD NUMBER,
 RFD NUMBER,
 first_remarketing_dep_num NUMBER,
 house_win NUMBER);
/
