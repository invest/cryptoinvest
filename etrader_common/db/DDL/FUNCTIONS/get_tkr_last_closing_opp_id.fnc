CREATE OR REPLACE /* Formatted on 3.12.2013 �. 11:30:54 (QP5 v5.149.1003.31008) */
FUNCTION "GET_TKR_LAST_CLOSING_OPP_ID" (p_market_id IN NUMBER)
   RETURN NUMBER
AS
BEGIN
   FOR opp
      IN (  SELECT *
              FROM opportunities
             WHERE     NOT closing_level IS NULL
                   AND opportunity_type_id = 1
                   AND market_id = p_market_id
          ORDER BY time_est_closing DESC)
   LOOP
      IF opp.scheduled = 1
      THEN
         RETURN opp.id;
      ELSE
         RETURN NULL;
      END IF;
   END LOOP;

   RETURN NULL;
END;
/
