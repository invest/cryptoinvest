﻿create or replace function get_last_closing_level
(
  p_market_id in number
 ,p_time      in date
) return number as
  l_level number;
begin
  select closing_level
  into   l_level
  from   (select closing_level
          from   last_levels
          where  market_id = p_market_id
          and    time_est_closing <= cast(trunc(p_time) as timestamp)
          order  by time_est_closing desc)
  where  rownum <= 1;

  return(l_level);
end;
/
