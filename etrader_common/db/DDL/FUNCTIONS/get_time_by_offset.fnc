CREATE OR REPLACE /* Formatted on 3.12.2013 �. 11:30:54 (QP5 v5.149.1003.31008) */
FUNCTION "GET_TIME_BY_OFFSET" (p_date IN DATE, p_offset IN VARCHAR2)
   RETURN DATE
AS
   poffset           VARCHAR2 (9);
   pdate             DATE := NULL;

   offset            VARCHAR2 (9);
   offset_time       VARCHAR2 (9);

   hours_string      VARCHAR2 (2);
   minutes_string    VARCHAR2 (2);
   hours             NUMBER;
   minutes           NUMBER;

   time_difference   NUMBER;
   offset_sign       VARCHAR2 (1);
BEGIN
   IF (p_date IS NOT NULL)
   THEN
      pdate := p_date;
      poffset := p_offset;

      offset_time := poffset;

      strtok (offset, offset_time, '+');

      IF (offset_time IS NOT NULL)
      THEN
         offset_sign := '+';
      ELSE
         offset_sign := '-';
         offset_time := poffset;
         strtok (offset, offset_time, '-');
      END IF;

      strtok (hours_string, offset_time, ':');
      strtok (minutes_string, offset_time, ' ');

      hours := TO_NUMBER (hours_string, '99');
      minutes := TO_NUMBER (minutes_string, '99');

      time_difference := (hours + minutes / 60) / 24;

      IF (offset_sign = '-')
      THEN
         time_difference := time_difference * -1;
      END IF;

      pdate := pdate + time_difference;
   END IF;

   RETURN pdate;
END GET_TIME_BY_OFFSET;
/
