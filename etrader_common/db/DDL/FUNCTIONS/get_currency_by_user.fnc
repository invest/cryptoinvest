CREATE OR REPLACE /* Formatted on 3.12.2013 �. 11:30:47 (QP5 v5.149.1003.31008) */
FUNCTION "GET_CURRENCY_BY_USER" (p_user_id IN NUMBER)
   RETURN NUMBER
AS
   curr_id   NUMBER;
BEGIN
   SELECT currency_id
     INTO curr_id
     FROM (SELECT currency_id
             FROM users
            WHERE id = p_user_id)
    WHERE ROWNUM <= 1;

   RETURN (curr_id);
END;
/
