CREATE OR REPLACE /* Formatted on 3.12.2013 �. 11:30:52 (QP5 v5.149.1003.31008) */
FUNCTION "GET_POPULATION_ENTRY_CB_ID" (p_population_entry_id IN NUMBER)
   RETURN NUMBER
AS
BEGIN
   FOR v_action
      IN (  SELECT id, reaction_id
              FROM issue_actions
             WHERE issue_id =
                      (SELECT id
                         FROM issues
                        WHERE population_entry_id = p_population_entry_id)
          ORDER BY action_time DESC)
   LOOP
      IF v_action.reaction_id = 4
      THEN
         RETURN (v_action.id);
      ELSE
         -- not a call back
         RETURN (NULL);
      END IF;
   END LOOP;

   -- if no issue
   RETURN (NULL);
END;
/
