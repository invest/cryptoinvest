CREATE OR REPLACE /* Formatted on 3.12.2013 �. 11:30:47 (QP5 v5.149.1003.31008) */
FUNCTION "GET_CURRENCY_MARKET" (p_currency_id IN NUMBER)
   RETURN NUMBER
AS
   market_id   NUMBER;
BEGIN
   SELECT A.id
     INTO market_id
     FROM markets A, currencies B
    WHERE A.feed_name = B.code || '=' AND B.id = p_currency_id;

   RETURN (market_id);
END;
/
