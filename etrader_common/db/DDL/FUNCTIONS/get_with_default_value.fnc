 create or replace function get_with_default_value(p_group in varchar2)
  return varchar2 is
-- add "d" after pair which is choose to be default from BE
  v_group_out     varchar2(500);
  
  cursor c_get_def_value is
    select replace(g.odds_group,
                   g.odds_group_default,
                   g.odds_group_default || 'd') as def_group
      from opportunity_odds_group g
     where g.odds_group = p_group;

begin

  open c_get_def_value;
  fetch c_get_def_value
    into v_group_out;
  close c_get_def_value;

  return(v_group_out);
end get_with_default_value;