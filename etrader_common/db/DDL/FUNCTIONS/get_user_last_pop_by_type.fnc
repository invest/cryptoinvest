CREATE OR REPLACE /* Formatted on 3.12.2013 �. 11:30:56 (QP5 v5.149.1003.31008) */
FUNCTION "GET_USER_LAST_POP_BY_TYPE" (pop_users_id   IN NUMBER,
                                      p_pop_type     IN NUMBER)
   RETURN population_entries_hist%ROWTYPE
IS
   populationEntHist   population_entries_hist%ROWTYPE;
BEGIN
   SELECT PEHO.*
     INTO populationEntHist
     FROM (  SELECT peh.*
               FROM population_entries_hist peh,
                    population_entries_hist_status pehs
              WHERE peh.status_id = pehs.id AND pehs.is_remove = 1
                    AND peh.population_entry_id =
                           (SELECT MAX (pe.id)
                              FROM population_entries pe,
                                   population_users pu,
                                   populations p
                             WHERE     pe.population_users_id = pu.id
                                   AND pu.id = pop_users_id
                                   AND pe.population_id = p.id
                                   AND p.population_type_id = p_pop_type)
           ORDER BY peh.TIME_CREATED DESC) PEHO
    WHERE ROWNUM = 1;

   RETURN populationEntHist;
EXCEPTION
   WHEN NO_DATA_FOUND
   THEN
      RETURN NULL;
END GET_USER_LAST_POP_BY_TYPE;
/
