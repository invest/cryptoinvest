CREATE OR REPLACE /* Formatted on 3.12.2013 �. 11:30:49 (QP5 v5.149.1003.31008) */
FUNCTION "GET_LAST_DEPOSIT_BY_CC_TIME" (userID IN NUMBER)
   RETURN DATE
AS
   tran   transactions%ROWTYPE;


   CURSOR tranCur (UID NUMBER)
   IS
        SELECT t.*
          FROM transactions t, TRANSACTION_TYPES tt
         WHERE tt.CLASS_TYPE = 1 AND user_id = UID AND type_id = tt.id
      ORDER BY time_created DESC;
BEGIN
   OPEN tranCur (userID);

   FETCH tranCur INTO tran;

   IF tranCur%NOTFOUND
   THEN
      RETURN CURRENT_DATE;
   END IF;

   CLOSE tranCur;


   RETURN tran.TIME_CREATED;
END;
/
