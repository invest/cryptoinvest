CREATE OR REPLACE /* Formatted on 3.12.2013 �. 11:30:57 (QP5 v5.149.1003.31008) */
create or replace FUNCTION "IS_MULTIPLY_TO_CONVERT" (p_market_id IN NUMBER) RETURN NUMBER AS
	multiply NUMBER;
BEGIN
	IF p_market_id = 14 OR p_market_id = 16  OR p_market_id=289 THEN
		multiply := 1;
	ELSE
		multiply := 0;
	END IF;

	RETURN(multiply);
END;
