CREATE OR REPLACE /* Formatted on 3.12.2013 �. 11:30:48 (QP5 v5.149.1003.31008) */
FUNCTION "GET_IS_USER_UNDER_BALANCE" (p_user_id             IN NUMBER,
                                      p_time_threshold         NUMBER,
                                      p_balance_threshold      NUMBER)
   RETURN NUMBER
AS
   last_date         DATE;
   last_balance      NUMBER;
   last_balance_id   NUMBER;
BEGIN
   -- Find the last date in the last p_time_threshold days when the user's balance was higher than p_balance_threshold
   SELECT MAX (bh2.time_created)
     INTO last_date
     FROM balance_history bh2
    WHERE     bh2.user_id = p_user_id
          AND bh2.time_created > SYSDATE - p_time_threshold
          AND bh2.balance > p_balance_threshold;


   -- If we have such date exsits...
   IF (last_date IS NULL)
   THEN
      -- find the last balance history id before the last  p_time_threshold days.
      SELECT MAX (bh2.id)
        INTO last_balance_id
        FROM balance_history bh2
       WHERE bh2.time_created < SYSDATE - p_time_threshold
             AND bh2.user_id = p_user_id;

      IF (last_balance_id IS NULL)
      THEN
         RETURN 0;
      ELSE
         -- Find the balance of the  last balance history id
         SELECT bh3.balance
           INTO last_balance
           FROM balance_history bh3
          WHERE bh3.user_id = p_user_id AND bh3.id = last_balance_id;

         IF (last_balance > p_balance_threshold)
         THEN
            RETURN 0;
         ELSE
            RETURN 1;
         END IF;               -- if (last_balance > p_balance_threshold) then
      END IF;                             -- if (last_balance_id is null) then
   ELSE
      -- false
      RETURN 0;
   END IF;


   RETURN NULL;
END GET_IS_USER_UNDER_BALANCE;
/
