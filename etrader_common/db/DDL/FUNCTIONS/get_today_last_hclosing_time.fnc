CREATE OR REPLACE /* Formatted on 3.12.2013 �. 11:30:56 (QP5 v5.149.1003.31008) */
FUNCTION "GET_TODAY_LAST_HCLOSING_TIME" (p_market_id IN NUMBER)
   RETURN VARCHAR2
AS
BEGIN
   FOR v_entry
      IN (  SELECT TO_CHAR (time_est_closing,
                            'YYYY-MM-DD HH24:MI:SS.FF3 TZHTZM')
                      AS time_est_closing
              FROM opportunities
             WHERE market_id = p_market_id AND scheduled = 1
                   AND time_act_closing >
                          CURRENT_TIMESTAMP - TO_DSINTERVAL ('0 02:00:00')
          ORDER BY time_est_closing DESC)
   LOOP
      RETURN (v_entry.time_est_closing);
   END LOOP;

   RETURN (NULL);
END;
/
