CREATE OR REPLACE /* Formatted on 3.12.2013 �. 11:30:50 (QP5 v5.149.1003.31008) */
FUNCTION "GET_LAST_SUCCESS_DEPOSIT_TIME" (p_user_id IN NUMBER)
   RETURN DATE
AS
   last_success_deposit   DATE;
BEGIN
   SELECT MAX (t.time_created)
     INTO last_success_deposit
     FROM transactions t, TRANSACTION_TYPES tt
    WHERE     tt.CLASS_TYPE = 1
          AND                                                       -- deposit
             user_id = p_user_id
          AND type_id = tt.id
          AND t.status_id IN (2, 7);                                -- success

   RETURN last_success_deposit;
EXCEPTION
   WHEN NO_DATA_FOUND
   THEN
      RETURN CURRENT_DATE;
END;
/
