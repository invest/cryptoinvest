create or replace function convert_amount_to_eur_ecb(p_amount      IN NUMBER,
                                                     p_currency_id IN NUMBER)
  return number is
  amount number;
  l_rate number;
begin

  select eur.rate
    into l_rate
    from currencies c
   inner join currencies_rates_to_eur eur
      on c.code = eur.currency_code
   where c.id = p_currency_id;

  amount := p_amount * l_rate;

  return(amount);
end convert_amount_to_eur_ecb;
/