CREATE OR REPLACE /* Formatted on 3.12.2013 �. 11:30:48 (QP5 v5.149.1003.31008) */
FUNCTION "GET_INV_MATURITY_DATE" (p_date IN DATE)
   RETURN DATE
AS
   pdate                DATE := NULL;
   pdate_string         VARCHAR2 (100);
   pdate_day_string     VARCHAR2 (2);
   pdate_mount_string   VARCHAR2 (2);
   pdate_year_string    VARCHAR2 (4);

   pdate_mount          NUMBER;
BEGIN
   IF (p_date IS NOT NULL)
   THEN
      pdate_string := TO_CHAR (p_date, 'DD-MM-YYYY');
      --            DBMS_OUTPUT.PUT_LINE('pdate is: ' || pdate_string);

      strtok (pdate_day_string, pdate_string, '-');
      strtok (pdate_mount_string, pdate_string, '-');
      strtok (pdate_year_string, pdate_string, ' ');

      pdate_mount := TO_NUMBER (pdate_mount_string, '99');

      IF (pdate_mount > 6)
      THEN
         pdate_mount_string := '12';
         pdate_day_string := '31';
      ELSE
         pdate_mount_string := '06';
         pdate_day_string := '30';
      END IF;

      pdate :=
         TO_DATE (
               pdate_day_string
            || pdate_mount_string
            || pdate_year_string
            || '00:00',
            'DDMMYYYYHH24:MI');
   --          DBMS_OUTPUT.PUT_LINE('pdate is: ' || pdate);

   END IF;

   RETURN pdate;
END GET_INV_MATURITY_DATE;
/
