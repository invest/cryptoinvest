CREATE OR REPLACE /* Formatted on 3.12.2013 �. 11:30:59 (QP5 v5.149.1003.31008) */
FUNCTION trn_min_date
   RETURN DATE
IS
   v_date_1   DATE;
   v_date_2   DATE;
BEGIN
   SELECT date_value
     INTO v_date_1
     FROM ETRADER.DB_PARAMETERS
    WHERE name = 'last_transactions_issues_s_run';

   SELECT date_value
     INTO v_date_2
     FROM ETRADER.DB_PARAMETERS
    WHERE name = 'last_transactions_issues_run';

   IF v_date_1 IS NULL OR v_date_2 IS NULL
   THEN
      RETURN SYSDATE;
   ELSIF v_date_1 < v_date_2
   THEN
      RETURN v_date_1;
   ELSE
      RETURN v_date_2;
   END IF;
END;
/
