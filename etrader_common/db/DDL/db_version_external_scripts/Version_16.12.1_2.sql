create or replace public synonym pkg_oppt_manager for pkg_oppt_manager;
grant execute on pkg_oppt_manager to etrader_service;
grant execute on pkg_oppt_manager to etrader_web;

update opportunity_templates o
set    sun_f = case
                 when (o.scheduled in (1, 2) and o.is_full_day = 2) then
                  1
                 when o.is_full_day = 1
                      and bitand(pkg_oppt_manager.days_tobitmask(o.market_id), power(2, 6)) > 0 then
                  1
				 when o.scheduled in (3, 4)
					  and bitand(pkg_oppt_manager.days_tobitmask(o.market_id), power(2, 6)) > 0 then
				  1
                 else
                  0
               end
      ,sun_h = case
                 when (o.scheduled in (1) and o.is_half_day = 1)
                      and bitand(pkg_oppt_manager.days_tobitmask(o.market_id), power(2, 6)) > 0 then
                  1
				 when o.scheduled in (3, 4)
					  and bitand(pkg_oppt_manager.days_tobitmask(o.market_id), power(2, 6)) > 0 then
				  1
                 else
                  0
               end
      ,mon_f = case
                 when o.is_full_day = 1
                      and bitand(pkg_oppt_manager.days_tobitmask(o.market_id), power(2, 5)) > 0 then
                  1
				 when o.scheduled in (3, 4)
					  and bitand(pkg_oppt_manager.days_tobitmask(o.market_id), power(2, 5)) > 0 then
				  1
                 else
                  0
               end
      ,mon_h = case
                 when (o.scheduled in (1, 2) and o.is_half_day = 1)
                      and bitand(pkg_oppt_manager.days_tobitmask(o.market_id), power(2, 5)) > 0 then
                  1
				 when o.scheduled in (3, 4)
					  and bitand(pkg_oppt_manager.days_tobitmask(o.market_id), power(2, 5)) > 0 then
				  1
                 else
                  0
               end
      ,tue_f = case
                 when o.is_full_day = 1
                      and bitand(pkg_oppt_manager.days_tobitmask(o.market_id), power(2, 4)) > 0 then
                  1
				 when o.scheduled in (3, 4)
					  and bitand(pkg_oppt_manager.days_tobitmask(o.market_id), power(2, 4)) > 0 then
				  1
                 else
                  0
               end
      ,tue_h = case
                 when (o.scheduled in (1, 2) and o.is_half_day = 1)
                      and bitand(pkg_oppt_manager.days_tobitmask(o.market_id), power(2, 4)) > 0 then
                  1
				 when o.scheduled in (3, 4)
					  and bitand(pkg_oppt_manager.days_tobitmask(o.market_id), power(2, 4)) > 0 then
				  1
                 else
                  0
               end
      ,wed_f = case
                 when o.is_full_day = 1
                      and bitand(pkg_oppt_manager.days_tobitmask(o.market_id), power(2, 3)) > 0 then
                  1
				 when o.scheduled in (3, 4)
					  and bitand(pkg_oppt_manager.days_tobitmask(o.market_id), power(2, 3)) > 0 then
				  1
                 else
                  0
               end
      ,wed_h = case
                 when (o.scheduled in (1, 2) and o.is_half_day = 1)
                      and bitand(pkg_oppt_manager.days_tobitmask(o.market_id), power(2, 3)) > 0 then
                  1
				 when o.scheduled in (3, 4)
					  and bitand(pkg_oppt_manager.days_tobitmask(o.market_id), power(2, 3)) > 0 then
				  1
                 else
                  0
               end
      ,thu_f = case
                 when o.is_full_day = 1
                      and bitand(pkg_oppt_manager.days_tobitmask(o.market_id), power(2, 2)) > 0 then
                  1
				 when o.scheduled in (3, 4)
					  and bitand(pkg_oppt_manager.days_tobitmask(o.market_id), power(2, 2)) > 0 then
				  1
                 else
                  0
               end
      ,thu_h = case
                 when (o.scheduled in (1, 2) and o.is_half_day = 1)
                      and bitand(pkg_oppt_manager.days_tobitmask(o.market_id), power(2, 2)) > 0 then
                  1
				 when o.scheduled in (3, 4)
					  and bitand(pkg_oppt_manager.days_tobitmask(o.market_id), power(2, 2)) > 0 then
				  1
                 else
                  0
               end
      ,fri_f = case
                 when o.is_full_day = 1
                      and bitand(pkg_oppt_manager.days_tobitmask(o.market_id), power(2, 1)) > 0 then
                  1
				 when o.scheduled in (3, 4)
					  and bitand(pkg_oppt_manager.days_tobitmask(o.market_id), power(2, 1)) > 0 then
				  1
                 else
                  0
               end
      ,fri_h = case
                 when (o.scheduled in (1, 2) and o.is_half_day = 1)
                      and bitand(pkg_oppt_manager.days_tobitmask(o.market_id), power(2, 1)) > 0 then
                  1
				 when o.scheduled in (3, 4)
					  and bitand(pkg_oppt_manager.days_tobitmask(o.market_id), power(2, 1)) > 0 then
				  1
                 else
                  0
               end
      ,sat_f = case
                 when (o.scheduled in (1, 2) and o.is_full_day = 3) then
                  1
                 when o.is_full_day = 1
                      and bitand(pkg_oppt_manager.days_tobitmask(o.market_id), power(2, 0)) > 0 then
                  1
                 else
                  0
               end
      ,sat_h = case
                 when (o.scheduled in (1, 2) and o.is_half_day = 1)
                      and bitand(pkg_oppt_manager.days_tobitmask(o.market_id), power(2, 0)) > 0 then
                  1
                 else
                  0
               end;
commit;


-- alter table opportunity_templates rename column is_full_day to is_full_day_obs;         
-- alter table opportunity_templates rename column is_half_day to is_half_day_obs;

-- alter table opportunity_templates modify (is_full_day_obs null, is_half_day_obs null);

-- comment on column opportunity_templates.is_full_day_obs is 'Column is obsolete. Use sun_f,mon_f,tue_f,wed_f,thu_f,fri_f,sat_f flags.';
-- comment on column opportunity_templates.is_half_day_obs is 'Column is obsolete. Use sun_h,mon_h,tue_h,wed_h,thu_h,fri_h,sat_h flags.';

--- update half day times for daily templates
update opportunity_templates a
set    (time_est_closing_halfday, time_last_invest_halfday) =
       (select c.time_est_closing, c.time_last_invest
        from   (select b.id
                      ,b.market_id
                      ,b.time_est_closing
                      ,b.time_last_invest
                      ,row_number() over(partition by b.market_id order by b.id) rn
                from   opportunity_templates b
                where  b.is_active = 1
                and    b.scheduled = 2
                and    b.is_half_day = 1) c
        where  c.rn = 1
        and    c.market_id = a.market_id)
where  a.is_active = 1
and    a.scheduled = 2
and    a.is_full_day in (1,2);


--- update half day times for weekly templates
update opportunity_templates a
set    (time_est_closing_halfday, time_last_invest_halfday) =
       (select c.time_est_closing, c.time_last_invest
        from   (select b.id
                      ,b.market_id
                      ,b.time_est_closing
                      ,b.time_last_invest
                      ,row_number() over(partition by b.market_id order by b.id) rn
                from   opportunity_templates b
                where  b.is_active = 1
                and    b.scheduled = 2
                and    b.is_half_day = 1) c
        where  c.rn = 1
        and    c.market_id = a.market_id)
where  a.is_active = 1
and    a.scheduled = 3
and    a.is_full_day = 1;

--- update half day times for monthly templates
update opportunity_templates a
set    (time_est_closing_halfday, time_last_invest_halfday) =
       (select c.time_est_closing, c.time_last_invest
        from   (select b.id
                      ,b.market_id
                      ,b.time_est_closing
                      ,b.time_last_invest
                      ,row_number() over(partition by b.market_id order by b.id) rn
                from   opportunity_templates b
                where  b.is_active = 1
                and    b.scheduled = 2
                and    b.is_half_day = 1) c
        where  c.rn = 1
        and    c.market_id = a.market_id)
where  a.is_active = 1
and    a.scheduled = 4
and    a.is_full_day = 1;

--- disable all "half day" templates as we will stop to use them
--update opportunity_templates
--set    is_active = 0
--where  is_active = 1
--and    is_half_day in (0, 1) and is_full_day = 0;

commit;


update opportunity_templates set sun_f = 0, sun_h = 0 where market_id = 16;
update opportunity_templates set sun_f = 0, sun_h = 0 where market_id = 287;
update opportunity_templates set sun_f = 0, sun_h = 0 where market_id = 289;

update opportunity_templates set mon_f = 1, mon_h = 1, tue_f = 1, tue_h = 1, wed_f = 1, wed_h = 1, thu_f = 1, thu_h = 1, fri_f = 1, fri_h = 1 where market_id = 619 and is_active = 1 and scheduled = 2;
update opportunity_templates set mon_f = 1, mon_h = 1, tue_f = 1, tue_h = 1, wed_f = 1, wed_h = 1, thu_f = 1, thu_h = 1, fri_f = 1, fri_h = 1 where market_id = 651 and is_active = 1 and scheduled = 2;

--update opportunity_templates
--set    is_active = 0
--where  is_active = 1
--and    is_half_day in (0, 1) and is_full_day = 0
--and    scheduled in (1, 2);

insert into market_pauses
  (id, market_id, scheduled, pause_start, pause_end, is_half_day, valid_days)
  select seq_market_pauses.nextval, market_id, 3, time_last_invest, time_first_invest, 0, 0
  from   opportunity_templates
  where  is_active = 1
  and    scheduled = 2
  and    sun_f + mon_f + tue_f + wed_f + thu_f + fri_f + sat_f > 0;
insert into market_pauses
  (id, market_id, scheduled, pause_start, pause_end, is_half_day, valid_days)
  select seq_market_pauses.nextval, market_id, 3, time_last_invest_halfday, time_first_invest, 1, 0
  from   opportunity_templates
  where  is_active = 1
  and    scheduled = 2
  and    sun_f + mon_f + tue_f + wed_f + thu_f + fri_f + sat_f > 0
  and    time_last_invest_halfday is not null
  and    not market_id = 3;
insert into market_pauses
  (id, market_id, scheduled, pause_start, pause_end, is_half_day, valid_days)
values
  (seq_market_pauses.nextval, 3, 3, '14:10', '10:00', 1, 0);
  
insert into market_pauses
  (id, market_id, scheduled, pause_start, pause_end, is_half_day, valid_days)
  select seq_market_pauses.nextval, market_id, 4, time_last_invest, time_first_invest, 0, 0
  from   opportunity_templates
  where  is_active = 1
  and    scheduled = 2
  and    sun_f + mon_f + tue_f + wed_f + thu_f + fri_f + sat_f > 0;
insert into market_pauses
  (id, market_id, scheduled, pause_start, pause_end, is_half_day, valid_days)
  select seq_market_pauses.nextval, market_id, 4, time_last_invest_halfday, time_first_invest, 1, 0
  from   opportunity_templates
  where  is_active = 1
  and    scheduled = 2
  and    sun_f + mon_f + tue_f + wed_f + thu_f + fri_f + sat_f > 0
  and    time_last_invest_halfday is not null
  and    not market_id = 3;
insert into market_pauses
  (id, market_id, scheduled, pause_start, pause_end, is_half_day, valid_days)
values
  (seq_market_pauses.nextval, 3, 4, '14:10', '10:00', 1, 0);
 


update market_pauses set valid_days = 126; 
update market_pauses set valid_days = 126;

update market_pauses set valid_days = 64 where market_id = 3 and pause_start = '16:10';
update market_pauses set valid_days = 60 where market_id = 3 and pause_start != '16:10';

commit;

