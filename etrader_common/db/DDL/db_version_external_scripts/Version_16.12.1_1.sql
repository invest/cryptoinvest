alter table opportunity_templates add (
   sun_f number(1) default 0 not null
  ,sun_h number(1) default 0 not null
  ,mon_f number(1) default 0 not null
  ,mon_h number(1) default 0 not null
  ,tue_f number(1) default 0 not null
  ,tue_h number(1) default 0 not null
  ,wed_f number(1) default 0 not null
  ,wed_h number(1) default 0 not null
  ,thu_f number(1) default 0 not null
  ,thu_h number(1) default 0 not null
  ,fri_f number(1) default 0 not null
  ,fri_h number(1) default 0 not null
  ,sat_f number(1) default 0 not null
  ,sat_h number(1) default 0 not null
  ,time_est_closing_halfday varchar2(5 ) 
  ,time_last_invest_halfday varchar2(5 ) 
);

--alter table opportunity_templates add constraint cc_oppt_templ check (
--  sun_f + sun_h + mon_f + mon_h + tue_f + tue_h + wed_f + wed_h + thu_f + thu_h + fri_f + fri_h + sat_f + sat_h > 0
--);

comment on column opportunity_templates.sun_f is 'Flag; 1-true,0-false; to indicate that this template is a "full day" template for Sunday. This is the meaning of all xxx_f columns.';
comment on column opportunity_templates.sun_h is 'Flag; 1-true,0-false; to indicate that this template is a "half day" template for Sunday. This is the meaning of all xxx_h columns.';


------------------------------- MARKET PAUSES ---------------------------------------

create table market_pauses (
  id                 number      nul null
  ,market_id         number      not null
  ,scheduled         number      not null -- can be daily(2); weekly(3) and monthly(4) ?
  ,is_half_day       number(1)   not null -- 1-half day; 0-full day; means that the pause is valid only if its a half day
  ,pause_start       varchar2(5) not null -- HH:MM; time zone is from the opportunity_templates table
  ,pause_end         varchar2(5) not null -- HH:MM; time zone is from the opportunity_templates table
  ,valid_days        number      not null -- bitmask for days when the pause is valid. The most sagnificant bit is SUN
  ,constraint pk_market_pauses primary key (market_id, scheduled, is_half_day, pause_start, pause_end)
)
organization index;  

alter table market_pauses add constraint fk_marp_markets foreign key (market_id) references markets (id);

comment on column market_pauses.scheduled     is 'can be daily(2); weekly(3) and monthly(4)';
comment on column market_pauses.is_half_day   is '0-full day; 1-half day - means that the pause is valid only if its a half day';
comment on column market_pauses.pause_start   is 'HH:MM; time zone is from the opportunity_templates table';
comment on column market_pauses.pause_end     is 'HH:MM; time zone is from the opportunity_templates table';
comment on column market_pauses.valid_days    is 'bitmask for days when the pause is valid. The most sagnificant bit is SUN';

create sequence seq_market_pauses;

insert into market_pauses
  (id, market_id, scheduled, pause_start, pause_end, is_half_day, valid_days)
  select seq_market_pauses.nextval, id, 2, '12:00', '14:30', 0, 0 from markets where exchange_id = 24;
insert into market_pauses
  (id, market_id, scheduled, pause_start, pause_end, is_half_day, valid_days)
  select  seq_market_pauses.nextval, id, 3, '12:00', '14:30', 0, 0 from markets where exchange_id = 24;
insert into market_pauses
  (id, market_id, scheduled, pause_start, pause_end, is_half_day, valid_days)
  select  seq_market_pauses.nextval, id, 4, '12:00', '14:30', 0, 0 from markets where exchange_id = 24;

insert into market_pauses
  (id, market_id, scheduled, pause_start, pause_end, is_half_day, valid_days)
  select  seq_market_pauses.nextval, id, 2, '11:00', '12:40', 0, 0 from markets where exchange_id = 4;
insert into market_pauses
  (id, market_id, scheduled, pause_start, pause_end, is_half_day, valid_days)
  select  seq_market_pauses.nextval, id, 3, '11:00', '12:40', 0, 0 from markets where exchange_id = 4;
insert into market_pauses
  (id, market_id, scheduled, pause_start, pause_end, is_half_day, valid_days)
  select  seq_market_pauses.nextval, id, 4, '11:00', '12:40', 0, 0 from markets where exchange_id = 4;

insert into market_pauses
  (id, market_id, scheduled, pause_start, pause_end, is_half_day, valid_days)
  select  seq_market_pauses.nextval, id, 2, '11:30', '13:00', 0, 0 from markets where exchange_id = 25;
insert into market_pauses
  (id, market_id, scheduled, pause_start, pause_end, is_half_day, valid_days)
  select  seq_market_pauses.nextval, id, 3, '11:30', '13:00', 0, 0 from markets where exchange_id = 25;
insert into market_pauses
  (id, market_id, scheduled, pause_start, pause_end, is_half_day, valid_days)
  select  seq_market_pauses.nextval, id, 4, '11:30', '13:00', 0, 0 from markets where exchange_id = 25;

insert into market_pauses
  (id, market_id, scheduled, pause_start, pause_end, is_half_day, valid_days)
  select  seq_market_pauses.nextval, id, 2, '12:00', '13:40', 0, 0 from markets where exchange_id = 7;
insert into market_pauses
  (id, market_id, scheduled, pause_start, pause_end, is_half_day, valid_days)
  select  seq_market_pauses.nextval, id, 3, '12:00', '13:40', 0, 0 from markets where exchange_id = 7;
insert into market_pauses
  (id, market_id, scheduled, pause_start, pause_end, is_half_day, valid_days)
  select  seq_market_pauses.nextval, id, 4, '12:00', '13:40', 0, 0 from markets where exchange_id = 7;

insert into market_pauses
  (id, market_id, scheduled, pause_start, pause_end, is_half_day, valid_days)
  select  seq_market_pauses.nextval, id, 2, '12:00', '13:40', 0, 0 from markets where exchange_id = 30;
insert into market_pauses
  (id, market_id, scheduled, pause_start, pause_end, is_half_day, valid_days)
  select  seq_market_pauses.nextval, id, 3, '12:00', '13:40', 0, 0 from markets where exchange_id = 30;
insert into market_pauses
  (id, market_id, scheduled, pause_start, pause_end, is_half_day, valid_days)
  select  seq_market_pauses.nextval, id, 4, '12:00', '13:40', 0, 0 from markets where exchange_id = 30;

insert into market_pauses
  (id, market_id, scheduled, pause_start, pause_end, is_half_day, valid_days)
  select  seq_market_pauses.nextval, id, 2, '12:30', '14:10', 0, 0 from markets where exchange_id = 39;
insert into market_pauses
  (id, market_id, scheduled, pause_start, pause_end, is_half_day, valid_days)
  select  seq_market_pauses.nextval, id, 3, '12:30', '14:10', 0, 0 from markets where exchange_id = 39;
insert into market_pauses
  (id, market_id, scheduled, pause_start, pause_end, is_half_day, valid_days)
  select  seq_market_pauses.nextval, id, 4, '12:30', '14:10', 0, 0 from markets where exchange_id = 39;

insert into market_pauses
  (id, market_id, scheduled, pause_start, pause_end, is_half_day, valid_days)
  select  seq_market_pauses.nextval, id, 2, '12:45', '14:40', 0, 0 from markets where exchange_id = 31;
insert into market_pauses
  (id, market_id, scheduled, pause_start, pause_end, is_half_day, valid_days)
  select  seq_market_pauses.nextval, id, 3, '12:45', '14:40', 0, 0 from markets where exchange_id = 31;
insert into market_pauses
  (id, market_id, scheduled, pause_start, pause_end, is_half_day, valid_days)
  select  seq_market_pauses.nextval, id, 4, '12:45', '14:40', 0, 0 from markets where exchange_id = 31;

commit;
