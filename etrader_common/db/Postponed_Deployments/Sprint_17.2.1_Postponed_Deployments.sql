--****************
--Postponed Deployments
--BEGIN 17.2.1
--****************

-------------------------------------------------------------------------------------------------------------------------------------------
-- START Ivan Petkov CORE-4129 Add Czech to AO and CO
-------------------------------------------------------------------------------------------------------------------------------------------
declare
  l_new_skin           skins%rowtype;
  l_src_config_skin_id number := 16;
  l_tmp_id             number;
begin
  select * into l_new_skin from skins where id = l_src_config_skin_id;

  insert into languages 
    (id, code, display_name)
  values
    (14, 'cs', 'languages.cs');

  --- update fields for the new skin
  l_new_skin.id                   := 27;
  l_new_skin.visible_type_id      := 1;
  l_new_skin.default_currency_id  := 3;
  l_new_skin.default_country_id   := 56;
  l_new_skin.default_language_id  := 14;
  l_new_skin.type_id              := 27;

  insert into skins values l_new_skin;

  update skins set display_name = 'skins.cz', LOCALE_SUBDOMAIN = 'cs', SUBDOMAIN = 'cs', name = 'Czech EUR REG' where id = 27;

  insert into asset_indexs_info
    (id, market_id, skin_id, market_description, additional_text, page_title, page_keywords, page_description, market_description_page)
    (select seq_asset_indexs_info.nextval
           ,market_id
           ,l_new_skin.id
           ,market_description
           ,additional_text
           ,page_title
           ,page_keywords
           ,page_description
           ,market_description_page
     from   asset_indexs_info
     where  skin_id = l_src_config_skin_id);

  insert into bonus_skins
    (id, bonus_id, skin_id)
    (select seq_bonus_skins.nextval, bonus_id, l_new_skin.id from bonus_skins where skin_id = l_src_config_skin_id);

  insert into mailbox_templates
    (id, name, subject, type_id, time_created, writer_id, sender_id, template, is_high_priority, skin_id, language_id, popup_type_id)
    select seq_mailbox_templates.nextval
          ,name
          ,subject
          ,type_id
          ,sysdate
          ,writer_id
          ,sender_id
          ,template
          ,is_high_priority
          ,l_new_skin.id
          ,14
          ,popup_type_id
    from   mailbox_templates
    where  skin_id = l_src_config_skin_id;

  insert into market_name_skin
    (id, market_id, skin_id, name, short_name)
    select seq_market_name_skin.nextval, market_id, l_new_skin.id, name, short_name
    from   market_name_skin
    where  skin_id = l_src_config_skin_id;

  insert into messages
    (id, text, web_screen, start_eff_date, end_eff_date, language_id, skin_id)
    select seq_messages.nextval, text, web_screen, start_eff_date, end_eff_date, 14, l_new_skin.id
    from   messages
    where  skin_id = l_src_config_skin_id;

  insert into one_touch_markets_skins
    (id, market_id, skin_id, priority)
    select seq_one_touch_markets_skins.nextval, market_id, l_new_skin.id, priority
    from   one_touch_markets_skins
    where  skin_id = l_src_config_skin_id;

  insert into populations
    (id
    ,name
    ,skin_id
    ,language_id
    ,start_date
    ,end_date
    ,threshold
    ,num_issues
    ,is_active
    ,refresh_time_period
    ,number_of_minutes
    ,number_of_days
    ,writer_id
    ,time_created
    ,balance_amount
    ,last_invest_day
    ,population_type_id
    ,number_of_deposits
    ,not_interested_delay
    ,dept_id
    ,max_last_login_time_days
    ,last_sales_deposit_days)
    select seq_populations.nextval
          ,name
          ,l_new_skin.id
          ,14
          ,start_date
          ,end_date
          ,threshold
          ,num_issues
          ,is_active
          ,refresh_time_period
          ,number_of_minutes
          ,number_of_days
          ,writer_id
          ,time_created
          ,balance_amount
          ,last_invest_day
          ,population_type_id
          ,number_of_deposits
          ,not_interested_delay
          ,dept_id
          ,max_last_login_time_days
          ,last_sales_deposit_days
    from   populations
    where  skin_id = l_src_config_skin_id;

  insert into skin_currencies
    (id, skin_id, currency_id, limit_id, is_default, cash_to_points_rate)
    select seq_skin_currencies.nextval, l_new_skin.id, currency_id, limit_id, is_default, cash_to_points_rate
    from   skin_currencies
    where  skin_id = l_src_config_skin_id;

  insert into skin_languages
    (id, skin_id, language_id)
    select seq_skin_languages.nextval, l_new_skin.id, 14 from skin_languages where skin_id = l_src_config_skin_id;

  for i in (select * from skin_market_groups where skin_id = l_src_config_skin_id)
  loop
    insert into skin_market_groups
      (id, skin_id, market_group_id, priority)
    values
      (seq_skin_market_groups.nextval, l_new_skin.id, i.market_group_id, i.priority)
    returning id into l_tmp_id;
  
    insert into skin_market_group_markets
      (id, skin_market_group_id, market_id, group_priority, home_page_priority, skin_display_group_id, ticker_priority, banners_priority)
      select seq_skin_market_group_markets.nextval
            ,l_tmp_id
            ,market_id
            ,group_priority
            ,home_page_priority
            ,skin_display_group_id
            ,ticker_priority
            ,banners_priority
      from   skin_market_group_markets
      where  skin_market_group_id = i.id;
  end loop;

  insert into terms
    (id, platform_id, skin_id, part_id, file_id)
    select seq_terms.nextval, platform_id, l_new_skin.id, part_id, file_id from terms where skin_id = l_src_config_skin_id;

  insert into skin_payment_methods
    (id, skin_id, transaction_type_id)
    select seq_skin_payment_methods.nextval, l_new_skin.id, transaction_type_id
    from   skin_payment_methods
    where  skin_id = l_src_config_skin_id;

  insert into skin_templates
    (id, template_id, skin_id)
    select seq_skin_templates.nextval, template_id, l_new_skin.id from skin_templates where skin_id = l_src_config_skin_id;

insert into SKIN_URL_COUNTRY_MAP
(id, skin_id, url_id, country_id)
values
(seq_skin_url_country_map.nextval, 27, 2, 56);

insert into SKIN_URL_COUNTRY_MAP
(id, skin_id, url_id, country_id)
values
(seq_skin_url_country_map.nextval, 27, 16, 56);

insert into currencies_rules
(id, COUNTRY_ID, skin_id, default_currency_id, comments, time_created)
values
(SEQ_SKIN_URL_COUNTRY_MAP.nextval, 56, 27, 3, 'BAC-203', sysdate);

  insert into writers_skin
    (id, skin_id, writer_id, assign_limit)
    select seq_writers_skin.nextval, l_new_skin.id, writer_id, assign_limit from writers_skin where skin_id = l_src_config_skin_id;

end;
/
-------------------------------------------------------------------------------------------------------------------------------------------
-- END Ivan Petkov CORE-4129 Add Czech to AO and CO
-------------------------------------------------------------------------------------------------------------------------------------------

-------------------------------------------------------------------------------------------------------------------------------------------
-- START Ivan Petkov CORE-4127 Add Polish to AO and CO
-------------------------------------------------------------------------------------------------------------------------------------------
declare
  l_new_skin           skins%rowtype;
  l_src_config_skin_id number := 16;
  l_tmp_id             number;
begin
  select * into l_new_skin from skins where id = l_src_config_skin_id;

  insert into languages 
    (id, code, display_name)
  values
    (15, 'pl', 'languages.pl');

  --- update fields for the new skin
  l_new_skin.id                   := 28;
  l_new_skin.visible_type_id      := 1;
  l_new_skin.default_currency_id  := 3;
  l_new_skin.default_country_id   := 166;
  l_new_skin.default_language_id  := 15;
  l_new_skin.type_id              := 28;

  insert into skins values l_new_skin;

  update skins set display_name = 'skins.pl', LOCALE_SUBDOMAIN = 'pl', SUBDOMAIN = 'pl', name = 'Polish EUR REG' where id = 28;

  insert into asset_indexs_info
    (id, market_id, skin_id, market_description, additional_text, page_title, page_keywords, page_description, market_description_page)
    (select seq_asset_indexs_info.nextval
           ,market_id
           ,l_new_skin.id
           ,market_description
           ,additional_text
           ,page_title
           ,page_keywords
           ,page_description
           ,market_description_page
     from   asset_indexs_info
     where  skin_id = l_src_config_skin_id);

  insert into bonus_skins
    (id, bonus_id, skin_id)
    (select seq_bonus_skins.nextval, bonus_id, l_new_skin.id from bonus_skins where skin_id = l_src_config_skin_id);

  insert into mailbox_templates
    (id, name, subject, type_id, time_created, writer_id, sender_id, template, is_high_priority, skin_id, language_id, popup_type_id)
    select seq_mailbox_templates.nextval
          ,name
          ,subject
          ,type_id
          ,sysdate
          ,writer_id
          ,sender_id
          ,template
          ,is_high_priority
          ,l_new_skin.id
          ,15
          ,popup_type_id
    from   mailbox_templates
    where  skin_id = l_src_config_skin_id;

  insert into market_name_skin
    (id, market_id, skin_id, name, short_name)
    select seq_market_name_skin.nextval, market_id, l_new_skin.id, name, short_name
    from   market_name_skin
    where  skin_id = l_src_config_skin_id;

  insert into messages
    (id, text, web_screen, start_eff_date, end_eff_date, language_id, skin_id)
    select seq_messages.nextval, text, web_screen, start_eff_date, end_eff_date, 15, l_new_skin.id
    from   messages
    where  skin_id = l_src_config_skin_id;

  insert into one_touch_markets_skins
    (id, market_id, skin_id, priority)
    select seq_one_touch_markets_skins.nextval, market_id, l_new_skin.id, priority
    from   one_touch_markets_skins
    where  skin_id = l_src_config_skin_id;

  insert into populations
    (id
    ,name
    ,skin_id
    ,language_id
    ,start_date
    ,end_date
    ,threshold
    ,num_issues
    ,is_active
    ,refresh_time_period
    ,number_of_minutes
    ,number_of_days
    ,writer_id
    ,time_created
    ,balance_amount
    ,last_invest_day
    ,population_type_id
    ,number_of_deposits
    ,not_interested_delay
    ,dept_id
    ,max_last_login_time_days
    ,last_sales_deposit_days)
    select seq_populations.nextval
          ,name
          ,l_new_skin.id
          ,15
          ,start_date
          ,end_date
          ,threshold
          ,num_issues
          ,is_active
          ,refresh_time_period
          ,number_of_minutes
          ,number_of_days
          ,writer_id
          ,time_created
          ,balance_amount
          ,last_invest_day
          ,population_type_id
          ,number_of_deposits
          ,not_interested_delay
          ,dept_id
          ,max_last_login_time_days
          ,last_sales_deposit_days
    from   populations
    where  skin_id = l_src_config_skin_id;

  insert into skin_currencies
    (id, skin_id, currency_id, limit_id, is_default, cash_to_points_rate)
    select seq_skin_currencies.nextval, l_new_skin.id, currency_id, limit_id, is_default, cash_to_points_rate
    from   skin_currencies
    where  skin_id = l_src_config_skin_id;

  insert into skin_languages
    (id, skin_id, language_id)
    select seq_skin_languages.nextval, l_new_skin.id, 15 from skin_languages where skin_id = l_src_config_skin_id;

  for i in (select * from skin_market_groups where skin_id = l_src_config_skin_id)
  loop
    insert into skin_market_groups
      (id, skin_id, market_group_id, priority)
    values
      (seq_skin_market_groups.nextval, l_new_skin.id, i.market_group_id, i.priority)
    returning id into l_tmp_id;
  
    insert into skin_market_group_markets
      (id, skin_market_group_id, market_id, group_priority, home_page_priority, skin_display_group_id, ticker_priority, banners_priority)
      select seq_skin_market_group_markets.nextval
            ,l_tmp_id
            ,market_id
            ,group_priority
            ,home_page_priority
            ,skin_display_group_id
            ,ticker_priority
            ,banners_priority
      from   skin_market_group_markets
      where  skin_market_group_id = i.id;
  end loop;

  insert into terms
    (id, platform_id, skin_id, part_id, file_id)
    select seq_terms.nextval, platform_id, l_new_skin.id, part_id, file_id from terms where skin_id = l_src_config_skin_id;

  insert into skin_payment_methods
    (id, skin_id, transaction_type_id)
    select seq_skin_payment_methods.nextval, l_new_skin.id, transaction_type_id
    from   skin_payment_methods
    where  skin_id = l_src_config_skin_id;

  insert into skin_templates
    (id, template_id, skin_id)
    select seq_skin_templates.nextval, template_id, l_new_skin.id from skin_templates where skin_id = l_src_config_skin_id;

insert into SKIN_URL_COUNTRY_MAP
(id, skin_id, url_id, country_id)
values
(seq_skin_url_country_map.nextval, 28, 2, 166);

insert into SKIN_URL_COUNTRY_MAP
(id, skin_id, url_id, country_id)
values
(seq_skin_url_country_map.nextval, 28, 16, 166);

insert into currencies_rules
(id, COUNTRY_ID, skin_id, default_currency_id, comments, time_created)
values
(SEQ_SKIN_URL_COUNTRY_MAP.nextval, 166, 28, 3, 'BAC-203', sysdate);

  insert into writers_skin
    (id, skin_id, writer_id, assign_limit)
    select seq_writers_skin.nextval, l_new_skin.id, writer_id, assign_limit from writers_skin where skin_id = l_src_config_skin_id;

end;
/
-------------------------------------------------------------------------------------------------------------------------------------------
-- END Ivan Petkov CORE-4127 Add Polish to AO and CO
-------------------------------------------------------------------------------------------------------------------------------------------

-------------------------------------------------------------------------------------------------------------------------------------------
-- START Ivan Petkov CORE-4128 Add Portugese to AO and CO
-------------------------------------------------------------------------------------------------------------------------------------------
declare
  l_new_skin           skins%rowtype;
  l_src_config_skin_id number := 16;
  l_tmp_id             number;
begin
  select * into l_new_skin from skins where id = l_src_config_skin_id;

  insert into languages 
    (id, code, display_name)
  values
    (16, 'pt', 'languages.pt');

  --- update fields for the new skin
  l_new_skin.id                   := 29;
  l_new_skin.visible_type_id      := 1;
  l_new_skin.default_currency_id  := 3;
  l_new_skin.default_country_id   := 167;
  l_new_skin.default_language_id  := 16;
  l_new_skin.type_id              := 29;

  insert into skins values l_new_skin;

  update skins set display_name = 'skins.pt', LOCALE_SUBDOMAIN = 'pt', SUBDOMAIN = 'pt', name = 'Portugal EUR REG' where id = 29;

  insert into asset_indexs_info
    (id, market_id, skin_id, market_description, additional_text, page_title, page_keywords, page_description, market_description_page)
    (select seq_asset_indexs_info.nextval
           ,market_id
           ,l_new_skin.id
           ,market_description
           ,additional_text
           ,page_title
           ,page_keywords
           ,page_description
           ,market_description_page
     from   asset_indexs_info
     where  skin_id = l_src_config_skin_id);

  insert into bonus_skins
    (id, bonus_id, skin_id)
    (select seq_bonus_skins.nextval, bonus_id, l_new_skin.id from bonus_skins where skin_id = l_src_config_skin_id);

  insert into mailbox_templates
    (id, name, subject, type_id, time_created, writer_id, sender_id, template, is_high_priority, skin_id, language_id, popup_type_id)
    select seq_mailbox_templates.nextval
          ,name
          ,subject
          ,type_id
          ,sysdate
          ,writer_id
          ,sender_id
          ,template
          ,is_high_priority
          ,l_new_skin.id
          ,16
          ,popup_type_id
    from   mailbox_templates
    where  skin_id = l_src_config_skin_id;

  insert into market_name_skin
    (id, market_id, skin_id, name, short_name)
    select seq_market_name_skin.nextval, market_id, l_new_skin.id, name, short_name
    from   market_name_skin
    where  skin_id = l_src_config_skin_id;

  insert into messages
    (id, text, web_screen, start_eff_date, end_eff_date, language_id, skin_id)
    select seq_messages.nextval, text, web_screen, start_eff_date, end_eff_date, 16, l_new_skin.id
    from   messages
    where  skin_id = l_src_config_skin_id;

  insert into one_touch_markets_skins
    (id, market_id, skin_id, priority)
    select seq_one_touch_markets_skins.nextval, market_id, l_new_skin.id, priority
    from   one_touch_markets_skins
    where  skin_id = l_src_config_skin_id;

  insert into populations
    (id
    ,name
    ,skin_id
    ,language_id
    ,start_date
    ,end_date
    ,threshold
    ,num_issues
    ,is_active
    ,refresh_time_period
    ,number_of_minutes
    ,number_of_days
    ,writer_id
    ,time_created
    ,balance_amount
    ,last_invest_day
    ,population_type_id
    ,number_of_deposits
    ,not_interested_delay
    ,dept_id
    ,max_last_login_time_days
    ,last_sales_deposit_days)
    select seq_populations.nextval
          ,name
          ,l_new_skin.id
          ,16
          ,start_date
          ,end_date
          ,threshold
          ,num_issues
          ,is_active
          ,refresh_time_period
          ,number_of_minutes
          ,number_of_days
          ,writer_id
          ,time_created
          ,balance_amount
          ,last_invest_day
          ,population_type_id
          ,number_of_deposits
          ,not_interested_delay
          ,dept_id
          ,max_last_login_time_days
          ,last_sales_deposit_days
    from   populations
    where  skin_id = l_src_config_skin_id;

  insert into skin_currencies
    (id, skin_id, currency_id, limit_id, is_default, cash_to_points_rate)
    select seq_skin_currencies.nextval, l_new_skin.id, currency_id, limit_id, is_default, cash_to_points_rate
    from   skin_currencies
    where  skin_id = l_src_config_skin_id;

  insert into skin_languages
    (id, skin_id, language_id)
    select seq_skin_languages.nextval, l_new_skin.id, 16 from skin_languages where skin_id = l_src_config_skin_id;

  for i in (select * from skin_market_groups where skin_id = l_src_config_skin_id)
  loop
    insert into skin_market_groups
      (id, skin_id, market_group_id, priority)
    values
      (seq_skin_market_groups.nextval, l_new_skin.id, i.market_group_id, i.priority)
    returning id into l_tmp_id;
  
    insert into skin_market_group_markets
      (id, skin_market_group_id, market_id, group_priority, home_page_priority, skin_display_group_id, ticker_priority, banners_priority)
      select seq_skin_market_group_markets.nextval
            ,l_tmp_id
            ,market_id
            ,group_priority
            ,home_page_priority
            ,skin_display_group_id
            ,ticker_priority
            ,banners_priority
      from   skin_market_group_markets
      where  skin_market_group_id = i.id;
  end loop;

  insert into terms
    (id, platform_id, skin_id, part_id, file_id)
    select seq_terms.nextval, platform_id, l_new_skin.id, part_id, file_id from terms where skin_id = l_src_config_skin_id;

  insert into skin_payment_methods
    (id, skin_id, transaction_type_id)
    select seq_skin_payment_methods.nextval, l_new_skin.id, transaction_type_id
    from   skin_payment_methods
    where  skin_id = l_src_config_skin_id;

  insert into skin_templates
    (id, template_id, skin_id)
    select seq_skin_templates.nextval, template_id, l_new_skin.id from skin_templates where skin_id = l_src_config_skin_id;

insert into SKIN_URL_COUNTRY_MAP
(id, skin_id, url_id, country_id)
values
(seq_skin_url_country_map.nextval, 29, 2, 167);

insert into SKIN_URL_COUNTRY_MAP
(id, skin_id, url_id, country_id)
values
(seq_skin_url_country_map.nextval, 29, 16, 167);

insert into currencies_rules
(id, COUNTRY_ID, skin_id, default_currency_id, comments, time_created)
values
(SEQ_SKIN_URL_COUNTRY_MAP.nextval, 167, 29, 3, 'BAC-203', sysdate);

  insert into writers_skin
    (id, skin_id, writer_id, assign_limit)
    select seq_writers_skin.nextval, l_new_skin.id, writer_id, assign_limit from writers_skin where skin_id = l_src_config_skin_id;

end;
/
-------------------------------------------------------------------------------------------------------------------------------------------
-- END Ivan Petkov CORE-4128 Add Portugese to AO and CO
-------------------------------------------------------------------------------------------------------------------------------------------

--****************
--Postponed Deployments
--END 17.2.1
--****************
