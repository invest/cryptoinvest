-------------------------------------------------------------------------------------------------------------------------------------------
-- BEGIN tasks related to the Goldbeam. 
-------------------------------------------------------------------------------------------------------------------------------------------

-------------------------------------------------------------------------------------------------------------------------------------------
-- START Youriy
-- BAC-4064 - [SQL] BAC-4063 GB blocked/allowed countries
-------------------------------------------------------------------------------------------------------------------------------------------
----------
-- Checking the ODT countries
--
select id
      ,country_name
      ,is_regulated
      ,is_blocked
      ,case
         when (is_regulated = 1) and (is_blocked = 0)
           then 'OK'
           else 'X'
       end status
  from etrader.countries
 where country_name in
       ('Austria'
       ,'Belgium'
       ,'Bulgaria'
       ,'Czech Republic'
       ,'Cyprus'
       ,'Denmark'
       ,'Estonia'
       ,'Finland'
       ,'France'
       ,'Germany'
       ,'Greece'
       ,'Hungary'
       ,'Italy'
       ,'Ireland'
       ,'Latvia'
       ,'Liechtenstein'
       ,'Lithuania'
       ,'Luxembourg'
       ,'Malta'
       ,'Netherlands'
       ,'Poland'
       ,'Portugal'
       ,'Romania'
       ,'Slovakia'
       ,'Slovenia'
       ,'Spain'
       ,'Sweden'
       ,'Switzerland'
       ,'South Africa'
       ,'United Kingdom' /* UK */
       )
 order by country_name;

----------
-- Checking the Blocked countries
--
select id
      ,country_name
      ,is_regulated
      ,is_blocked
      ,case
         when (is_blocked != 0)
           then 'OK'
           else 'X'
       end status
  from etrader.countries
 where id in
       (2   /* Afghanistan       */
       ,5   /* American Samoa    */
       ,11  /* Argentina         */
       ,14  /* Australia         */
       ,28  /* Bosnia            */
       ,23  /* Belize            */
       ,31  /* Brazil            */
       ,38  /* Canada            */
       ,44  /* China             */
       ,61  /* East Timor        */
       ,87  /* Guam              */
       ,94  /* Hong Kong         */
       ,97  /* India             */
       ,99  /* Iran              */
       ,100 /* Iraq              */
       ,1   /* Israel            */
       ,104 /* Japan             */
       ,111 /* Laos              */
       ,113 /* Lebanon           */
       ,116 /* Libya             */
       ,135 /* Moldova           */
       ,148 /* New Zealand       */
       ,154 /* North Korea       */
       ,158 /* Pakistan          */
       ,168 /* Puerto Rico       */
       ,172 /* Russia            */
       ,184 /* Singapore         */
       ,196 /* Sudan             */
       ,202 /* Syria             */
       ,212 /* Turkey            */
       ,216 /* Uganda            */
       ,217 /* Ukraine           */
       ,220 /* United States     */
       ,228 /* Virgin Islands US */
       ,231 /* Yemen             */
       )
 order by country_name;

----------
-- Backing up the table Countries.
--
CREATE TABLE ETRADER.COUNTRIES_BKP_BAC4063 AS SELECT * FROM ETRADER.COUNTRIES;

----------
--
CREATE TABLE ETRADER.COUNTRIES_SET_BAC4063
(COUNTRY_ID   NUMBER            NOT NULL
,COUNTRY_NAME VARCHAR2(40 CHAR) NOT NULL
,GROUP_ID     NUMBER            NOT NULL
);

COMMENT ON COLUMN ETRADER.COUNTRIES_SET_BAC4063.GROUP_ID IS 'Country Group IDs: 1 - ODT, 2 - Blocked, 3 - Belize.';

CREATE UNIQUE INDEX ETRADER.IDX_COUNTRIES_SET_BAC4063_PK ON ETRADER.COUNTRIES_SET_BAC4063 (COUNTRY_ID);

ALTER TABLE ETRADER.COUNTRIES_SET_BAC4063 ADD
  CONSTRAINT COUNTRIES_SET_BAC4063_PK PRIMARY KEY (COUNTRY_ID)
   USING INDEX ETRADER.IDX_COUNTRIES_SET_BAC4063_PK  ENABLE;

----------
-- Inserting the ODT countries.
--
INSERT INTO ETRADER.COUNTRIES_SET_BAC4063 (COUNTRY_ID, COUNTRY_NAME, GROUP_ID) VALUES (15, 'Austria', 1);                                                                       
INSERT INTO ETRADER.COUNTRIES_SET_BAC4063 (COUNTRY_ID, COUNTRY_NAME, GROUP_ID) VALUES (22, 'Belgium', 1);                                                                       
INSERT INTO ETRADER.COUNTRIES_SET_BAC4063 (COUNTRY_ID, COUNTRY_NAME, GROUP_ID) VALUES (33, 'Bulgaria', 1);                                                                      
INSERT INTO ETRADER.COUNTRIES_SET_BAC4063 (COUNTRY_ID, COUNTRY_NAME, GROUP_ID) VALUES (55, 'Cyprus', 1);                                                                        
INSERT INTO ETRADER.COUNTRIES_SET_BAC4063 (COUNTRY_ID, COUNTRY_NAME, GROUP_ID) VALUES (56, 'Czech Republic', 1);                                                                
INSERT INTO ETRADER.COUNTRIES_SET_BAC4063 (COUNTRY_ID, COUNTRY_NAME, GROUP_ID) VALUES (57, 'Denmark', 1);                                                                       
INSERT INTO ETRADER.COUNTRIES_SET_BAC4063 (COUNTRY_ID, COUNTRY_NAME, GROUP_ID) VALUES (67, 'Estonia', 1);                                                                       
INSERT INTO ETRADER.COUNTRIES_SET_BAC4063 (COUNTRY_ID, COUNTRY_NAME, GROUP_ID) VALUES (72, 'Finland', 1);                                                                       
INSERT INTO ETRADER.COUNTRIES_SET_BAC4063 (COUNTRY_ID, COUNTRY_NAME, GROUP_ID) VALUES (73, 'France', 1);                                                                        
INSERT INTO ETRADER.COUNTRIES_SET_BAC4063 (COUNTRY_ID, COUNTRY_NAME, GROUP_ID) VALUES (80, 'Germany', 1);                                                                       
INSERT INTO ETRADER.COUNTRIES_SET_BAC4063 (COUNTRY_ID, COUNTRY_NAME, GROUP_ID) VALUES (83, 'Greece', 1);                                                                        
INSERT INTO ETRADER.COUNTRIES_SET_BAC4063 (COUNTRY_ID, COUNTRY_NAME, GROUP_ID) VALUES (95, 'Hungary', 1);                                                                       
INSERT INTO ETRADER.COUNTRIES_SET_BAC4063 (COUNTRY_ID, COUNTRY_NAME, GROUP_ID) VALUES (101, 'Ireland', 1);                                                                      
INSERT INTO ETRADER.COUNTRIES_SET_BAC4063 (COUNTRY_ID, COUNTRY_NAME, GROUP_ID) VALUES (102, 'Italy', 1);                                                                        
INSERT INTO ETRADER.COUNTRIES_SET_BAC4063 (COUNTRY_ID, COUNTRY_NAME, GROUP_ID) VALUES (112, 'Latvia', 1);                                                                       
INSERT INTO ETRADER.COUNTRIES_SET_BAC4063 (COUNTRY_ID, COUNTRY_NAME, GROUP_ID) VALUES (117, 'Liechtenstein', 1);                                                                
INSERT INTO ETRADER.COUNTRIES_SET_BAC4063 (COUNTRY_ID, COUNTRY_NAME, GROUP_ID) VALUES (118, 'Lithuania', 1);                                                                    
INSERT INTO ETRADER.COUNTRIES_SET_BAC4063 (COUNTRY_ID, COUNTRY_NAME, GROUP_ID) VALUES (119, 'Luxembourg', 1);                                                                   
INSERT INTO ETRADER.COUNTRIES_SET_BAC4063 (COUNTRY_ID, COUNTRY_NAME, GROUP_ID) VALUES (127, 'Malta', 1);                                                                        
INSERT INTO ETRADER.COUNTRIES_SET_BAC4063 (COUNTRY_ID, COUNTRY_NAME, GROUP_ID) VALUES (145, 'Netherlands', 1);                                                                  
INSERT INTO ETRADER.COUNTRIES_SET_BAC4063 (COUNTRY_ID, COUNTRY_NAME, GROUP_ID) VALUES (166, 'Poland', 1);                                                                       
INSERT INTO ETRADER.COUNTRIES_SET_BAC4063 (COUNTRY_ID, COUNTRY_NAME, GROUP_ID) VALUES (167, 'Portugal', 1);                                                                     
INSERT INTO ETRADER.COUNTRIES_SET_BAC4063 (COUNTRY_ID, COUNTRY_NAME, GROUP_ID) VALUES (171, 'Romania', 1);                                                                      
INSERT INTO ETRADER.COUNTRIES_SET_BAC4063 (COUNTRY_ID, COUNTRY_NAME, GROUP_ID) VALUES (185, 'Slovakia', 1);                                                                     
INSERT INTO ETRADER.COUNTRIES_SET_BAC4063 (COUNTRY_ID, COUNTRY_NAME, GROUP_ID) VALUES (186, 'Slovenia', 1);                                                                     
INSERT INTO ETRADER.COUNTRIES_SET_BAC4063 (COUNTRY_ID, COUNTRY_NAME, GROUP_ID) VALUES (189, 'South Africa', 1);                                                                 
INSERT INTO ETRADER.COUNTRIES_SET_BAC4063 (COUNTRY_ID, COUNTRY_NAME, GROUP_ID) VALUES (192, 'Spain', 1);                                                                        
INSERT INTO ETRADER.COUNTRIES_SET_BAC4063 (COUNTRY_ID, COUNTRY_NAME, GROUP_ID) VALUES (200, 'Sweden', 1);                                                                       
INSERT INTO ETRADER.COUNTRIES_SET_BAC4063 (COUNTRY_ID, COUNTRY_NAME, GROUP_ID) VALUES (201, 'Switzerland', 1);                                                                  
INSERT INTO ETRADER.COUNTRIES_SET_BAC4063 (COUNTRY_ID, COUNTRY_NAME, GROUP_ID) VALUES (219, 'United Kingdom', 1);                                                               
COMMIT;

----------
-- Inserting the Blocked countries.
--
INSERT INTO ETRADER.COUNTRIES_SET_BAC4063 (COUNTRY_ID, COUNTRY_NAME, GROUP_ID) VALUES (1, 'Israel', 2);                                                                         
INSERT INTO ETRADER.COUNTRIES_SET_BAC4063 (COUNTRY_ID, COUNTRY_NAME, GROUP_ID) VALUES (2, 'Afghanistan', 2);                                                                    
INSERT INTO ETRADER.COUNTRIES_SET_BAC4063 (COUNTRY_ID, COUNTRY_NAME, GROUP_ID) VALUES (5, 'American Samoa', 2);                                                                 
INSERT INTO ETRADER.COUNTRIES_SET_BAC4063 (COUNTRY_ID, COUNTRY_NAME, GROUP_ID) VALUES (11, 'Argentina', 2);                                                                     
INSERT INTO ETRADER.COUNTRIES_SET_BAC4063 (COUNTRY_ID, COUNTRY_NAME, GROUP_ID) VALUES (14, 'Australia', 2);                                                                     
INSERT INTO ETRADER.COUNTRIES_SET_BAC4063 (COUNTRY_ID, COUNTRY_NAME, GROUP_ID) VALUES (23, 'Belize', 2);                                                                        
INSERT INTO ETRADER.COUNTRIES_SET_BAC4063 (COUNTRY_ID, COUNTRY_NAME, GROUP_ID) VALUES (28, 'Bosnia', 2);                                                                        
INSERT INTO ETRADER.COUNTRIES_SET_BAC4063 (COUNTRY_ID, COUNTRY_NAME, GROUP_ID) VALUES (31, 'Brazil', 2);                                                                        
INSERT INTO ETRADER.COUNTRIES_SET_BAC4063 (COUNTRY_ID, COUNTRY_NAME, GROUP_ID) VALUES (38, 'Canada', 2);                                                                        
INSERT INTO ETRADER.COUNTRIES_SET_BAC4063 (COUNTRY_ID, COUNTRY_NAME, GROUP_ID) VALUES (44, 'China', 2);                                                                         
INSERT INTO ETRADER.COUNTRIES_SET_BAC4063 (COUNTRY_ID, COUNTRY_NAME, GROUP_ID) VALUES (61, 'East Timor', 2);                                                                    
INSERT INTO ETRADER.COUNTRIES_SET_BAC4063 (COUNTRY_ID, COUNTRY_NAME, GROUP_ID) VALUES (87, 'Guam', 2);                                                                          
INSERT INTO ETRADER.COUNTRIES_SET_BAC4063 (COUNTRY_ID, COUNTRY_NAME, GROUP_ID) VALUES (94, 'Hong Kong', 2);                                                                     
INSERT INTO ETRADER.COUNTRIES_SET_BAC4063 (COUNTRY_ID, COUNTRY_NAME, GROUP_ID) VALUES (97, 'India', 2);                                                                         
INSERT INTO ETRADER.COUNTRIES_SET_BAC4063 (COUNTRY_ID, COUNTRY_NAME, GROUP_ID) VALUES (99, 'Iran', 2);                                                                          
INSERT INTO ETRADER.COUNTRIES_SET_BAC4063 (COUNTRY_ID, COUNTRY_NAME, GROUP_ID) VALUES (100, 'Iraq', 2);                                                                         
INSERT INTO ETRADER.COUNTRIES_SET_BAC4063 (COUNTRY_ID, COUNTRY_NAME, GROUP_ID) VALUES (104, 'Japan', 2);                                                                        
INSERT INTO ETRADER.COUNTRIES_SET_BAC4063 (COUNTRY_ID, COUNTRY_NAME, GROUP_ID) VALUES (111, 'Laos', 2);                                                                         
INSERT INTO ETRADER.COUNTRIES_SET_BAC4063 (COUNTRY_ID, COUNTRY_NAME, GROUP_ID) VALUES (113, 'Lebanon', 2);                                                                      
INSERT INTO ETRADER.COUNTRIES_SET_BAC4063 (COUNTRY_ID, COUNTRY_NAME, GROUP_ID) VALUES (116, 'Libya', 2);                                                                        
INSERT INTO ETRADER.COUNTRIES_SET_BAC4063 (COUNTRY_ID, COUNTRY_NAME, GROUP_ID) VALUES (135, 'Moldova', 2);                                                                      
INSERT INTO ETRADER.COUNTRIES_SET_BAC4063 (COUNTRY_ID, COUNTRY_NAME, GROUP_ID) VALUES (148, 'New Zealand', 2);                                                                  
INSERT INTO ETRADER.COUNTRIES_SET_BAC4063 (COUNTRY_ID, COUNTRY_NAME, GROUP_ID) VALUES (154, 'North Korea', 2);                                                                  
INSERT INTO ETRADER.COUNTRIES_SET_BAC4063 (COUNTRY_ID, COUNTRY_NAME, GROUP_ID) VALUES (158, 'Pakistan', 2);                                                                     
INSERT INTO ETRADER.COUNTRIES_SET_BAC4063 (COUNTRY_ID, COUNTRY_NAME, GROUP_ID) VALUES (168, 'Puerto Rico', 2);                                                                  
INSERT INTO ETRADER.COUNTRIES_SET_BAC4063 (COUNTRY_ID, COUNTRY_NAME, GROUP_ID) VALUES (172, 'Russia', 2);                                                                       
INSERT INTO ETRADER.COUNTRIES_SET_BAC4063 (COUNTRY_ID, COUNTRY_NAME, GROUP_ID) VALUES (184, 'Singapore', 2);                                                                    
INSERT INTO ETRADER.COUNTRIES_SET_BAC4063 (COUNTRY_ID, COUNTRY_NAME, GROUP_ID) VALUES (196, 'Sudan', 2);                                                                        
INSERT INTO ETRADER.COUNTRIES_SET_BAC4063 (COUNTRY_ID, COUNTRY_NAME, GROUP_ID) VALUES (202, 'Syria', 2);                                                                        
INSERT INTO ETRADER.COUNTRIES_SET_BAC4063 (COUNTRY_ID, COUNTRY_NAME, GROUP_ID) VALUES (212, 'Turkey', 2);                                                                       
INSERT INTO ETRADER.COUNTRIES_SET_BAC4063 (COUNTRY_ID, COUNTRY_NAME, GROUP_ID) VALUES (216, 'Uganda', 2);                                                                       
INSERT INTO ETRADER.COUNTRIES_SET_BAC4063 (COUNTRY_ID, COUNTRY_NAME, GROUP_ID) VALUES (217, 'Ukraine', 2);                                                                      
INSERT INTO ETRADER.COUNTRIES_SET_BAC4063 (COUNTRY_ID, COUNTRY_NAME, GROUP_ID) VALUES (220, 'United States', 2);                                                                
INSERT INTO ETRADER.COUNTRIES_SET_BAC4063 (COUNTRY_ID, COUNTRY_NAME, GROUP_ID) VALUES (228, 'Virgin Islands US', 2);                                                            
INSERT INTO ETRADER.COUNTRIES_SET_BAC4063 (COUNTRY_ID, COUNTRY_NAME, GROUP_ID) VALUES (231, 'Yemen', 2);                                                                        
COMMIT;

----------
-- Setting the Belize countries.
-- It is expected to update 161 countries in less than a second.
--
update etrader.countries u
   set u.is_regulated = 0
      ,u.is_blocked   = 0
 where u.id in (select c.id
                  from etrader.countries c
                  left join etrader.countries_set_bac4063 x on (c.id = x.country_id)
                 where x.country_id is null
               );
commit;

----------
-- Checking the Belize countries.
--
select c.id
      ,c.country_name
      ,c.is_regulated
      ,c.is_blocked
      ,case
         when (c.is_regulated = 0) and (c.is_blocked = 0)
           then 'OK'
           else 'X'
       end status
  from etrader.countries c
  left join etrader.countries_set_bac4063 x on (c.id = x.country_id)
 where x.country_id is null
 order by c.id;
-------------------------------------------------------------------------------------------------------------------------------------------
-- END Youriy
-- BAC-4064 - [SQL] BAC-4063 GB blocked/allowed countries
-------------------------------------------------------------------------------------------------------------------------------------------

-------------------------------------------------------------------------------------------------------------------------------------------
--Radi
--BAC-3662 [server] Goldbeam - Latin entity (anyoption)
-------------------------------------------------------------------------------------------------------------------------------------------
insert into SKIN_BUSINESS_CASES
	(id, code, description)
values
	(5, 'CASE_GOLDBEAM', 'business.case.goldbeam');

update skins set business_case_id = 5 where id in (25,26);

update skins set display_name = 'skins.goldbeam.en', name = 'English Goldbeam' where id = 25;
update skins set display_name = 'skins.goldbeam.es', name = 'Spanish Goldbeam' where id = 26;

delete from SKIN_CURRENCIES where skin_id in (25,26) and currency_id in (5,6);

insert into skin_currencies
    (id, skin_id, currency_id, limit_id, is_default, cash_to_points_rate)
values
    (seq_skin_currencies.nextval,25, 7, 7, 1, 0.1);
    
insert into skin_currencies
    (id, skin_id, currency_id, limit_id, is_default, cash_to_points_rate)
values
    (seq_skin_currencies.nextval,25, 7, 7, 1, 0.1);

-- update countries 
--   set is_regulated = 0
-- where 
-- id not in (15, 22, 33, 55, 56, 57, 67, 72, 73, 80, 83, 95, 96, 101, 102, 112, 117, 118, 119, 127, 145, 156, 166, 167, 171, 185, 186, 192, 200, 219, 189, 14);

update skins set visible_type_id = 4 where id in (25,26);

update skin_url_country_map set skin_id = 26 
where url_id = 2 
and country_id in ( 
                   select id from countries 
                   where is_regulated = 0
                   and is_blocked = 0
                  )
and skin_id in (
                select id from skins 
                where default_language_id = 5
                );
				
update skin_url_country_map set skin_id = 25
where url_id = 2 
and country_id in ( 
                   select id from countries 
                   where is_regulated = 0
                   and is_blocked = 0
                  )
and skin_id in (
                select id from skins 
                where default_language_id <> 5
                );
				
update clearing_routes 
set business_skin_id = 5 
where country_id in (
					 select id 
					 from countries 
					 where is_regulated = 0 
						   and is_blocked = 0
					);

commit;

-- run trg
@TRIGGERS/before_ins_users.trg
-------------------------------------------------------------------------------------------------------------------------------------------
--END
--BAC-3662 [server] Goldbeam - Latin entity (anyoption)
-------------------------------------------------------------------------------------------------------------------------------------------
					
-------------------------------------------------------------------------------------------------------------------------------------------
--Radi
--BAC-3948 CNY currency is not available for ES Goldbeam user
-------------------------------------------------------------------------------------------------------------------------------------------
insert into skin_currencies
    (id, skin_id, currency_id, limit_id, is_default, cash_to_points_rate)
values
    (seq_skin_currencies.nextval,26, 7, 7, 1, 0.1);
commit;
-------------------------------------------------------------------------------------------------------------------------------------------
--END
--BAC-3948 CNY currency is not available for ES Goldbeam user
-------------------------------------------------------------------------------------------------------------------------------------------

-------------------------------------------------------------------------------------------------------------------------------------------
-- END tasks related to the Goldbeam. 
-------------------------------------------------------------------------------------------------------------------------------------------
