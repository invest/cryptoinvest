#!/bin/bash
#
#
# Script Variables (change them to suit your needs)
export TNS_ADMIN=/opt/oracle-client/11g
export NLS_LANG=AMERICAN_AMERICA.UTF8
export PATH=/opt/oracle-client/11g:/sbin:/usr/sbin:/bin:/usr/bin:/usr/X11R6/bin:/usr/local/bin:/usr/local/sbin

cd /usr/local/etrader-jobs/Reports/Activated_Accounts

sqlplus -s "etrader_anal/ANAL3tr4d3r@(DESCRIPTION=(ADDRESS=(PROTOCOL=TCP)(HOST=dg01.etrader.co.il)(PORT=1521))(CONNECT_DATA=(SID = etrader3)))" <<eof1
set pages
ALTER SESSION SET NLS_DATE_FORMAT='YYYYMMDD HH24:MI' ;
SET MARKUP HTML ON
spool Activated_Accounts_Month_Report.xls
@Activated_Accounts.sql
spool off
exit
eof1

echo -e "see attached file" |mutt -a Activated_Accounts_Month_Report.xls -s "Activated Accounts - Month Report "`date +%F` Ayan@anyoption.com

rm -f *.xls