-- Eyal O -- 23/05/13
-- static -  Up_Down_Dynamic

insert into static_landing_pages (id, name, type) values (SEQ_STATIC_LP.nextval, 'Up_Down_Dynamic',1);

Insert into STATIC_LANDING_PAGES_LANGUAGE
(ID,STATIC_LANDING_PAGE_ID,LANGUAGE_ID, skin_id)
select
    SEQ_STATIC_LP_LANG.nextval,
    slp.id,
    8,
    8
from
     static_landing_pages slp
where
    slp.name = 'Up_Down_Dynamic';


Insert into STATIC_LANDING_PAGES_LANGUAGE
(ID,STATIC_LANDING_PAGE_ID,LANGUAGE_ID, skin_id)
select
    SEQ_STATIC_LP_LANG.nextval,
    slp.id,
    2,
    2
from
     static_landing_pages slp
where
    slp.name = 'Up_Down_Dynamic';


Insert into STATIC_LANDING_PAGES_LANGUAGE
(ID,STATIC_LANDING_PAGE_ID,LANGUAGE_ID, skin_id)
select
    SEQ_STATIC_LP_LANG.nextval,
    slp.id,
    5,
    5
from
     static_landing_pages slp
where
    slp.name = 'Up_Down_Dynamic';


Insert into STATIC_LANDING_PAGES_LANGUAGE
(ID,STATIC_LANDING_PAGE_ID,LANGUAGE_ID, skin_id)
select
    SEQ_STATIC_LP_LANG.nextval,
    slp.id,
    9,
    12
from
     static_landing_pages slp
where
    slp.name = 'Up_Down_Dynamic';

Insert into STATIC_LANDING_PAGES_LANGUAGE
(ID,STATIC_LANDING_PAGE_ID,LANGUAGE_ID, skin_id)
select
    SEQ_STATIC_LP_LANG.nextval,
    slp.id,
    4,
    10
from
     static_landing_pages slp
where
    slp.name = 'Up_Down_Dynamic';


Insert into STATIC_LANDING_PAGES_LANGUAGE
(ID,STATIC_LANDING_PAGE_ID,LANGUAGE_ID, skin_id)
select
    SEQ_STATIC_LP_LANG.nextval,
    slp.id,
    3,
    3
from
     static_landing_pages slp
where
    slp.name = 'Up_Down_Dynamic';

------------------------------------------------------------------------
-- Eyal O -- 23/05/13
-- static -  AO_FolkTestimonials

insert into static_landing_pages (id, name, type) values (SEQ_STATIC_LP.nextval, 'AO_FolkTestimonials',1);

Insert into STATIC_LANDING_PAGES_LANGUAGE
(ID,STATIC_LANDING_PAGE_ID,LANGUAGE_ID, skin_id)
select
    SEQ_STATIC_LP_LANG.nextval,
    slp.id,
    8,
    8
from
     static_landing_pages slp
where
    slp.name = 'AO_FolkTestimonials';


Insert into STATIC_LANDING_PAGES_LANGUAGE
(ID,STATIC_LANDING_PAGE_ID,LANGUAGE_ID, skin_id)
select
    SEQ_STATIC_LP_LANG.nextval,
    slp.id,
    2,
    2
from
     static_landing_pages slp
where
    slp.name = 'AO_FolkTestimonials';


Insert into STATIC_LANDING_PAGES_LANGUAGE
(ID,STATIC_LANDING_PAGE_ID,LANGUAGE_ID, skin_id)
select
    SEQ_STATIC_LP_LANG.nextval,
    slp.id,
    5,
    5
from
     static_landing_pages slp
where
    slp.name = 'AO_FolkTestimonials';


Insert into STATIC_LANDING_PAGES_LANGUAGE
(ID,STATIC_LANDING_PAGE_ID,LANGUAGE_ID, skin_id)
select
    SEQ_STATIC_LP_LANG.nextval,
    slp.id,
    9,
    12
from
     static_landing_pages slp
where
    slp.name = 'AO_FolkTestimonials';

Insert into STATIC_LANDING_PAGES_LANGUAGE
(ID,STATIC_LANDING_PAGE_ID,LANGUAGE_ID, skin_id)
select
    SEQ_STATIC_LP_LANG.nextval,
    slp.id,
    4,
    10
from
     static_landing_pages slp
where
    slp.name = 'AO_FolkTestimonials';


Insert into STATIC_LANDING_PAGES_LANGUAGE
(ID,STATIC_LANDING_PAGE_ID,LANGUAGE_ID, skin_id)
select
    SEQ_STATIC_LP_LANG.nextval,
    slp.id,
    3,
    3
from
     static_landing_pages slp
where
    slp.name = 'AO_FolkTestimonials';

Insert into STATIC_LANDING_PAGES_LANGUAGE
(ID,STATIC_LANDING_PAGE_ID,LANGUAGE_ID, skin_id)
select
    SEQ_STATIC_LP_LANG.nextval,
    slp.id,
    10,
    15
from
     static_landing_pages slp
where
    slp.name = 'AO_FolkTestimonials';
    
----------------------------------------------------------------------------
-- Eyal O -- 23/05/13
-- static - [dev - 1659] upload LP-AO_MakeMoney_Red_Videos

insert into static_landing_pages (id, name, type) values (SEQ_STATIC_LP.nextval, 'AO_MakeMoney_Red_Videos',3);

Insert into STATIC_LANDING_PAGES_LANGUAGE
(ID,STATIC_LANDING_PAGE_ID,LANGUAGE_ID, skin_id)
select
    SEQ_STATIC_LP_LANG.nextval,
    slp.id,
    5,
    5
from
     static_landing_pages slp
where
    slp.name = 'AO_MakeMoney_Red_Videos';    
    
---------------------------------------------------------------------------------------

--LioR SoLoMoN -- 03/06/13. dev - 1693. 

insert into static_landing_pages (id, name, type) values (SEQ_STATIC_LP.nextval, 'Binary_Option_Tricks',3);

Insert into STATIC_LANDING_PAGES_LANGUAGE
(ID,STATIC_LANDING_PAGE_ID,LANGUAGE_ID, skin_id)
select
    SEQ_STATIC_LP_LANG.nextval,
    slp.id,
    8,
    8
from
     static_landing_pages slp
where
    slp.name = 'Binary_Option_Tricks';
   
    
Insert into STATIC_LANDING_PAGES_LANGUAGE
(ID,STATIC_LANDING_PAGE_ID,LANGUAGE_ID, skin_id)
select
    SEQ_STATIC_LP_LANG.nextval,
    slp.id,
    2,
    2
from
     static_landing_pages slp
where
    slp.name = 'Binary_Option_Tricks';
    
    
Insert into STATIC_LANDING_PAGES_LANGUAGE
(ID,STATIC_LANDING_PAGE_ID,LANGUAGE_ID, skin_id)
select
    SEQ_STATIC_LP_LANG.nextval,
    slp.id,
    5,
    5
from
     static_landing_pages slp
where
    slp.name = 'Binary_Option_Tricks';
    
    
    
Insert into STATIC_LANDING_PAGES_LANGUAGE
(ID,STATIC_LANDING_PAGE_ID,LANGUAGE_ID, skin_id)
select
    SEQ_STATIC_LP_LANG.nextval,
    slp.id,
    9,
    12
from
     static_landing_pages slp
where
    slp.name = 'Binary_Option_Tricks';
-----------------------------------------------------------------------------------------------

--LioR SoLoMoN -- 03/06/13. dev - 1709. 

insert into static_landing_pages (id, name, type) values (SEQ_STATIC_LP.nextval, 'Google_AO_Money_InterviewVideo',1);

Insert into STATIC_LANDING_PAGES_LANGUAGE
(ID,STATIC_LANDING_PAGE_ID,LANGUAGE_ID, skin_id)
select
    SEQ_STATIC_LP_LANG.nextval,
    slp.id,
    10,
    15
from
     static_landing_pages slp
where
    slp.name = 'Google_AO_Money_InterviewVideo';
    
---------------------------------------------------------------------------------------------------------
--LioR SoLoMoN -- 04/06/13. dev - 1727. 

insert into static_landing_pages (id, name, type) values (SEQ_STATIC_LP.nextval, 'Google_AO_M5_ShortFormVideo',1);

Insert into STATIC_LANDING_PAGES_LANGUAGE
(ID,STATIC_LANDING_PAGE_ID,LANGUAGE_ID, skin_id)
select
    SEQ_STATIC_LP_LANG.nextval,
    slp.id,
    10,
    15
from
     static_landing_pages slp
where
    slp.name = 'Google_AO_M5_ShortFormVideo';
    
    
insert into static_landing_pages (id, name, type) values (SEQ_STATIC_LP.nextval, 'Media_AO_M5_ShortFormVideo',1);

Insert into STATIC_LANDING_PAGES_LANGUAGE
(ID,STATIC_LANDING_PAGE_ID,LANGUAGE_ID, skin_id)
select
    SEQ_STATIC_LP_LANG.nextval,
    slp.id,
    10,
    15
from
     static_landing_pages slp
where
    slp.name = 'Media_AO_M5_ShortFormVideo';
    
------------------------------------------------------------------------------------------------------------------
--Ofer 23-06-2013
--DEV-1859 AO_Money_SecretVideo in fr- media version
Insert into STATIC_LANDING_PAGES_LANGUAGE
(ID,STATIC_LANDING_PAGE_ID,LANGUAGE_ID, skin_id)
select
    SEQ_STATIC_LP_LANG.nextval,
    slp.id,
    9,
   12
from
     static_landing_pages slp
where
    slp.name = 'AO_Money_SecretVideo';

-------------------------------------------------------------------------------------------------------------------
--LioR SoLoMoN -- 07/07/13. dev - 1927, 1928, 1929. 

insert into static_landing_pages (id, name, type) values (SEQ_STATIC_LP.nextval, 'Google_AO_Branding_ShortCleanVideo',3);

Insert into STATIC_LANDING_PAGES_LANGUAGE
(ID,STATIC_LANDING_PAGE_ID,LANGUAGE_ID, skin_id)
select
    SEQ_STATIC_LP_LANG.nextval,
    slp.id,
    2,
    2
from
     static_landing_pages slp
where
    slp.name = 'Google_AO_Branding_ShortCleanVideo';    
    
Insert into STATIC_LANDING_PAGES_LANGUAGE
(ID,STATIC_LANDING_PAGE_ID,LANGUAGE_ID, skin_id)
select
    SEQ_STATIC_LP_LANG.nextval,
    slp.id,
    8,
    8
from
     static_landing_pages slp
where
    slp.name = 'Google_AO_Branding_ShortCleanVideo';    
    
    
 Insert into STATIC_LANDING_PAGES_LANGUAGE
(ID,STATIC_LANDING_PAGE_ID,LANGUAGE_ID, skin_id)
select
    SEQ_STATIC_LP_LANG.nextval,
    slp.id,
    4,
    10
from
     static_landing_pages slp
where
    slp.name = 'Google_AO_Branding_ShortCleanVideo';    
    
    
 Insert into STATIC_LANDING_PAGES_LANGUAGE
(ID,STATIC_LANDING_PAGE_ID,LANGUAGE_ID, skin_id)
select
    SEQ_STATIC_LP_LANG.nextval,
    slp.id,
    3,
    3
from
     static_landing_pages slp
where
    slp.name = 'Google_AO_Branding_ShortCleanVideo'; 
     
----

insert into static_landing_pages (id, name, type) values (SEQ_STATIC_LP.nextval, 'Binary_Option_Tricks_NL',3);

Insert into STATIC_LANDING_PAGES_LANGUAGE
(ID,STATIC_LANDING_PAGE_ID,LANGUAGE_ID, skin_id)
select
    SEQ_STATIC_LP_LANG.nextval,
    slp.id,
    2,
    2
from
     static_landing_pages slp
where
    slp.name = 'Binary_Option_Tricks_NL';  
    
    
Insert into STATIC_LANDING_PAGES_LANGUAGE
(ID,STATIC_LANDING_PAGE_ID,LANGUAGE_ID, skin_id)
select
    SEQ_STATIC_LP_LANG.nextval,
    slp.id,
    8,
    8
from
     static_landing_pages slp
where
    slp.name = 'Binary_Option_Tricks_NL';   
    
Insert into STATIC_LANDING_PAGES_LANGUAGE
(ID,STATIC_LANDING_PAGE_ID,LANGUAGE_ID, skin_id)
select
    SEQ_STATIC_LP_LANG.nextval,
    slp.id,
    5,
    5
from
     static_landing_pages slp
where
    slp.name = 'Binary_Option_Tricks_NL';   
    
Insert into STATIC_LANDING_PAGES_LANGUAGE
(ID,STATIC_LANDING_PAGE_ID,LANGUAGE_ID, skin_id)
select
    SEQ_STATIC_LP_LANG.nextval,
    slp.id,
    9,
    12
from
     static_landing_pages slp
where
    slp.name = 'Binary_Option_Tricks_NL'; 
    
----

insert into static_landing_pages (id, name, type) values (SEQ_STATIC_LP.nextval, 'ET_Up_Down_Dynamic',1);

Insert into STATIC_LANDING_PAGES_LANGUAGE
(ID,STATIC_LANDING_PAGE_ID,LANGUAGE_ID, skin_id)
select
    SEQ_STATIC_LP_LANG.nextval,
    slp.id,
    1,
    1
from
     static_landing_pages slp
where
    slp.name = 'ET_Up_Down_Dynamic';  


--------------------------------------------------------------------------------------------------------------------------------    
--LioR SoLoMoN -- 09/07/13. dev - 1931. 

insert into static_landing_pages (id, name, type) values (SEQ_STATIC_LP.nextval, 'Risk_Free_Trade_v1',1);

Insert into STATIC_LANDING_PAGES_LANGUAGE
(ID,STATIC_LANDING_PAGE_ID,LANGUAGE_ID, skin_id)
select
    SEQ_STATIC_LP_LANG.nextval,
    slp.id,
    2,
    2
from
     static_landing_pages slp
where
    slp.name = 'Risk_Free_Trade_v1';  
    
    
Insert into STATIC_LANDING_PAGES_LANGUAGE
(ID,STATIC_LANDING_PAGE_ID,LANGUAGE_ID, skin_id)
select
    SEQ_STATIC_LP_LANG.nextval,
    slp.id,
    5,
    5
from
     static_landing_pages slp
where
    slp.name = 'Risk_Free_Trade_v1';  
     
Insert into STATIC_LANDING_PAGES_LANGUAGE
(ID,STATIC_LANDING_PAGE_ID,LANGUAGE_ID, skin_id)
select
    SEQ_STATIC_LP_LANG.nextval,
    slp.id,
    9,
    12
from
     static_landing_pages slp
where
    slp.name = 'Risk_Free_Trade_v1';   
    
Insert into STATIC_LANDING_PAGES_LANGUAGE
(ID,STATIC_LANDING_PAGE_ID,LANGUAGE_ID, skin_id)
select
    SEQ_STATIC_LP_LANG.nextval,
    slp.id,
    4,
    10
from
     static_landing_pages slp
where
    slp.name = 'Risk_Free_Trade_v1';  

Insert into STATIC_LANDING_PAGES_LANGUAGE
(ID,STATIC_LANDING_PAGE_ID,LANGUAGE_ID, skin_id)
select
    SEQ_STATIC_LP_LANG.nextval,
    slp.id,
   3,
    3
from
     static_landing_pages slp
where
    slp.name = 'Risk_Free_Trade_v1';  

Insert into STATIC_LANDING_PAGES_LANGUAGE
(ID,STATIC_LANDING_PAGE_ID,LANGUAGE_ID, skin_id)
select
    SEQ_STATIC_LP_LANG.nextval,
    slp.id,
    10,
    15
from
     static_landing_pages slp
where
    slp.name = 'Risk_Free_Trade_v1'; 
    
------------------------------------------------------------------------------
insert into static_landing_pages (id, name, type) values (SEQ_STATIC_LP.nextval, 'Risk_Free_Trade_v2',1);

Insert into STATIC_LANDING_PAGES_LANGUAGE
(ID,STATIC_LANDING_PAGE_ID,LANGUAGE_ID, skin_id)
select
    SEQ_STATIC_LP_LANG.nextval,
    slp.id,
    2,
    2
from
     static_landing_pages slp
where
    slp.name = 'Risk_Free_Trade_v2';  
    
    
Insert into STATIC_LANDING_PAGES_LANGUAGE
(ID,STATIC_LANDING_PAGE_ID,LANGUAGE_ID, skin_id)
select
    SEQ_STATIC_LP_LANG.nextval,
    slp.id,
    5,
    5
from
     static_landing_pages slp
where
    slp.name = 'Risk_Free_Trade_v2';  
    
   
   
Insert into STATIC_LANDING_PAGES_LANGUAGE
(ID,STATIC_LANDING_PAGE_ID,LANGUAGE_ID, skin_id)
select
    SEQ_STATIC_LP_LANG.nextval,
    slp.id,
    9,
    12
from
     static_landing_pages slp
where
    slp.name = 'Risk_Free_Trade_v2';   
    
    
    
Insert into STATIC_LANDING_PAGES_LANGUAGE
(ID,STATIC_LANDING_PAGE_ID,LANGUAGE_ID, skin_id)
select
    SEQ_STATIC_LP_LANG.nextval,
    slp.id,
    4,
    10
from
     static_landing_pages slp
where
    slp.name = 'Risk_Free_Trade_v2';  
    
    
    
Insert into STATIC_LANDING_PAGES_LANGUAGE
(ID,STATIC_LANDING_PAGE_ID,LANGUAGE_ID, skin_id)
select
    SEQ_STATIC_LP_LANG.nextval,
    slp.id,
   3,
    3
from
     static_landing_pages slp
where
    slp.name = 'Risk_Free_Trade_v2';  
    
    
Insert into STATIC_LANDING_PAGES_LANGUAGE
(ID,STATIC_LANDING_PAGE_ID,LANGUAGE_ID, skin_id)
select
    SEQ_STATIC_LP_LANG.nextval,
    slp.id,
    10,
    15
from
     static_landing_pages slp
where
    slp.name = 'Risk_Free_Trade_v2';  
-----------------------------------------------------------------------

--Ofer 29-07-2013
--DEV-2018 71_generic_survey lp in korean
Insert into STATIC_LANDING_PAGES_LANGUAGE
(ID,STATIC_LANDING_PAGE_ID,LANGUAGE_ID, skin_id)
select
    SEQ_STATIC_LP_LANG.nextval,
    slp.id,
    11,
   17
from
     static_landing_pages slp
where
    slp.name = '70_generic_short_survey';
-----------------------------------------------------------------------

--Ofer 29-07-2013
--DEV-1967 up&down lp in korean
Insert into STATIC_LANDING_PAGES_LANGUAGE
(ID,STATIC_LANDING_PAGE_ID,LANGUAGE_ID, skin_id)
select
    SEQ_STATIC_LP_LANG.nextval,
    slp.id,
    11,
   17
from
     static_landing_pages slp
where
    slp.name = 'Up_Down_Dynamic';
-----------------------------------------------------------------------

--Ofer 29-07-2013
--DEV-1994 Google_AO_Branding_ShortCleanVideo fr and es
Insert into STATIC_LANDING_PAGES_LANGUAGE
(ID,STATIC_LANDING_PAGE_ID,LANGUAGE_ID, skin_id)
select
    SEQ_STATIC_LP_LANG.nextval,
    slp.id,
    5,
   5
from
     static_landing_pages slp
where
    slp.name = 'Google_AO_Branding_ShortCleanVideo';   
    
Insert into STATIC_LANDING_PAGES_LANGUAGE
(ID,STATIC_LANDING_PAGE_ID,LANGUAGE_ID, skin_id)
select
    SEQ_STATIC_LP_LANG.nextval,
    slp.id,
    9,
   12
from
     static_landing_pages slp
where
    slp.name = 'Google_AO_Branding_ShortCleanVideo';   
  
---------------------------------------------------------------------
--Ofer 29-07-2013
--DEV-1966 AO_Money_SecretVideo_ShortForm lp's
insert into static_landing_pages (id, name, type) values (SEQ_STATIC_LP.nextval, 'AO_Money_SecretVideo_ShortForm',1);

Insert into STATIC_LANDING_PAGES_LANGUAGE
(ID,STATIC_LANDING_PAGE_ID,LANGUAGE_ID, skin_id)
select
    SEQ_STATIC_LP_LANG.nextval,
    slp.id,
    8,
   8
from
     static_landing_pages slp
where
    slp.name = 'AO_Money_SecretVideo_ShortForm';   

  
Insert into STATIC_LANDING_PAGES_LANGUAGE
(ID,STATIC_LANDING_PAGE_ID,LANGUAGE_ID, skin_id)
select
    SEQ_STATIC_LP_LANG.nextval,
    slp.id,
    2,
   2
from
     static_landing_pages slp
where
    slp.name = 'AO_Money_SecretVideo_ShortForm';  
    
    
Insert into STATIC_LANDING_PAGES_LANGUAGE
(ID,STATIC_LANDING_PAGE_ID,LANGUAGE_ID, skin_id)
select
    SEQ_STATIC_LP_LANG.nextval,
    slp.id,
    5,
   5
from
     static_landing_pages slp
where
    slp.name = 'AO_Money_SecretVideo_ShortForm';  

    
Insert into STATIC_LANDING_PAGES_LANGUAGE
(ID,STATIC_LANDING_PAGE_ID,LANGUAGE_ID, skin_id)
select
    SEQ_STATIC_LP_LANG.nextval,
    slp.id,
    9,
   12
from
     static_landing_pages slp
where
    slp.name = 'AO_Money_SecretVideo_ShortForm';  
    
    
Insert into STATIC_LANDING_PAGES_LANGUAGE
(ID,STATIC_LANDING_PAGE_ID,LANGUAGE_ID, skin_id)
select
    SEQ_STATIC_LP_LANG.nextval,
    slp.id,
    4,
   10
from
     static_landing_pages slp
where
    slp.name = 'AO_Money_SecretVideo_ShortForm';  
    
    
Insert into STATIC_LANDING_PAGES_LANGUAGE
(ID,STATIC_LANDING_PAGE_ID,LANGUAGE_ID, skin_id)
select
    SEQ_STATIC_LP_LANG.nextval,
    slp.id,
    3,
   3
from
     static_landing_pages slp
where
    slp.name = 'AO_Money_SecretVideo_ShortForm';  
-----------------------------------------------------------------------------
--Ofer 06-08-2013
--DEV-1958 80_generic_short

insert into static_landing_pages (id, name, type) values (SEQ_STATIC_LP.nextval, '80_generic_short',1);


Insert into STATIC_LANDING_PAGES_LANGUAGE
(ID,STATIC_LANDING_PAGE_ID,LANGUAGE_ID, skin_id)
select
    SEQ_STATIC_LP_LANG.nextval,
    slp.id,
    8,
   8
from
     static_landing_pages slp
where
    slp.name = '80_generic_short';   
    
Insert into STATIC_LANDING_PAGES_LANGUAGE
(ID,STATIC_LANDING_PAGE_ID,LANGUAGE_ID, skin_id)
select
    SEQ_STATIC_LP_LANG.nextval,
    slp.id,
    2,
   2
from
     static_landing_pages slp
where
    slp.name = '80_generic_short';   
    
Insert into STATIC_LANDING_PAGES_LANGUAGE
(ID,STATIC_LANDING_PAGE_ID,LANGUAGE_ID, skin_id)
select
    SEQ_STATIC_LP_LANG.nextval,
    slp.id,
    5,
   5
from
     static_landing_pages slp
where
    slp.name = '80_generic_short';   
    
Insert into STATIC_LANDING_PAGES_LANGUAGE
(ID,STATIC_LANDING_PAGE_ID,LANGUAGE_ID, skin_id)
select
    SEQ_STATIC_LP_LANG.nextval,
    slp.id,
    9,
   12
from
     static_landing_pages slp
where
    slp.name = '80_generic_short';   
    
Insert into STATIC_LANDING_PAGES_LANGUAGE
(ID,STATIC_LANDING_PAGE_ID,LANGUAGE_ID, skin_id)
select
    SEQ_STATIC_LP_LANG.nextval,
    slp.id,
    4,
   10
from
     static_landing_pages slp
where
    slp.name = '80_generic_short';   

Insert into STATIC_LANDING_PAGES_LANGUAGE
(ID,STATIC_LANDING_PAGE_ID,LANGUAGE_ID, skin_id)
select
    SEQ_STATIC_LP_LANG.nextval,
    slp.id,
    3,
   3
from
     static_landing_pages slp
where
    slp.name = '80_generic_short';   

-----------------------------------------------------------------------------
--Ofer 06-08-2013
--DEV-1965 upload 80% profit for affiliates

Insert into STATIC_LANDING_PAGES_LANGUAGE
(ID,STATIC_LANDING_PAGE_ID,LANGUAGE_ID, skin_id)
select
    SEQ_STATIC_LP_LANG.nextval,
    slp.id,
    8,
   8
from
     static_landing_pages slp
where
    slp.name = '80profit_CDN';  

Insert into STATIC_LANDING_PAGES_LANGUAGE
(ID,STATIC_LANDING_PAGE_ID,LANGUAGE_ID, skin_id)
select
    SEQ_STATIC_LP_LANG.nextval,
    slp.id,
    5,
   5
from
     static_landing_pages slp
where
    slp.name = '80profit_CDN';  
    
Insert into STATIC_LANDING_PAGES_LANGUAGE
(ID,STATIC_LANDING_PAGE_ID,LANGUAGE_ID, skin_id)
select
    SEQ_STATIC_LP_LANG.nextval,
    slp.id,
    9,
   12
from
     static_landing_pages slp
where
    slp.name = '80profit_CDN';   
    
Insert into STATIC_LANDING_PAGES_LANGUAGE
(ID,STATIC_LANDING_PAGE_ID,LANGUAGE_ID, skin_id)
select
    SEQ_STATIC_LP_LANG.nextval,
    slp.id,
    4,
   10
from
     static_landing_pages slp
where
    slp.name = '80profit_CDN';   
    
Insert into STATIC_LANDING_PAGES_LANGUAGE
(ID,STATIC_LANDING_PAGE_ID,LANGUAGE_ID, skin_id)
select
    SEQ_STATIC_LP_LANG.nextval,
    slp.id,
    3,
   3
from
     static_landing_pages slp
where
    slp.name = '80profit_CDN';   
    
Insert into STATIC_LANDING_PAGES_LANGUAGE
(ID,STATIC_LANDING_PAGE_ID,LANGUAGE_ID, skin_id)
select
    SEQ_STATIC_LP_LANG.nextval,
    slp.id,
    10,
   15
from
     static_landing_pages slp
where
    slp.name = '80profit_CDN';   
    
----------------------------------------------------------
--DEV-2209
--Ofer 13-08-13

insert into static_landing_pages (id, name, type) values (SEQ_STATIC_LP.nextval, 'Google_AO_M5_Tudou',3);

Insert into STATIC_LANDING_PAGES_LANGUAGE
(ID,STATIC_LANDING_PAGE_ID,LANGUAGE_ID, skin_id)
select
    SEQ_STATIC_LP_LANG.nextval,
    slp.id,
    10,
   15
from
     static_landing_pages slp
where
    slp.name = 'Google_AO_M5_Tudou';   
    
insert into static_landing_pages (id, name, type) values (SEQ_STATIC_LP.nextval, 'Google_AO_M5_Youku',3);

Insert into STATIC_LANDING_PAGES_LANGUAGE
(ID,STATIC_LANDING_PAGE_ID,LANGUAGE_ID, skin_id)
select
    SEQ_STATIC_LP_LANG.nextval,
    slp.id,
    10,
   15
from
     static_landing_pages slp
where
    slp.name = 'Google_AO_M5_Youku';
--------------------------------------------------------------
--Ofer 15-08-13
--Dev 2183


insert into static_landing_pages (id, name, type) values (SEQ_STATIC_LP.nextval, 'features',1);

Insert into STATIC_LANDING_PAGES_LANGUAGE
(ID,STATIC_LANDING_PAGE_ID,LANGUAGE_ID, skin_id)
select
    SEQ_STATIC_LP_LANG.nextval,
    slp.id,
    8,
   8
from
     static_landing_pages slp
where
    slp.name = 'features';   
    
    
Insert into STATIC_LANDING_PAGES_LANGUAGE
(ID,STATIC_LANDING_PAGE_ID,LANGUAGE_ID, skin_id)
select
    SEQ_STATIC_LP_LANG.nextval,
    slp.id,
    2,
   2
from
     static_landing_pages slp
where
    slp.name = 'features';  
    
Insert into STATIC_LANDING_PAGES_LANGUAGE
(ID,STATIC_LANDING_PAGE_ID,LANGUAGE_ID, skin_id)
select
    SEQ_STATIC_LP_LANG.nextval,
    slp.id,
    5,
   5
from
     static_landing_pages slp
where
    slp.name = 'features';  
    
Insert into STATIC_LANDING_PAGES_LANGUAGE
(ID,STATIC_LANDING_PAGE_ID,LANGUAGE_ID, skin_id)
select
    SEQ_STATIC_LP_LANG.nextval,
    slp.id,
    9,
   12
from
     static_landing_pages slp
where
    slp.name = 'features';  
    
Insert into STATIC_LANDING_PAGES_LANGUAGE
(ID,STATIC_LANDING_PAGE_ID,LANGUAGE_ID, skin_id)
select
    SEQ_STATIC_LP_LANG.nextval,
    slp.id,
    6,
   9
from
     static_landing_pages slp
where
    slp.name = 'features';
    
Insert into STATIC_LANDING_PAGES_LANGUAGE
(ID,STATIC_LANDING_PAGE_ID,LANGUAGE_ID, skin_id)
select
    SEQ_STATIC_LP_LANG.nextval,
    slp.id,
    4,
   10
from
     static_landing_pages slp
where
    slp.name = 'features'; 
    
Insert into STATIC_LANDING_PAGES_LANGUAGE
(ID,STATIC_LANDING_PAGE_ID,LANGUAGE_ID, skin_id)
select
    SEQ_STATIC_LP_LANG.nextval,
    slp.id,
    3,
   3
from
     static_landing_pages slp
where
    slp.name = 'features';  
    
Insert into STATIC_LANDING_PAGES_LANGUAGE
(ID,STATIC_LANDING_PAGE_ID,LANGUAGE_ID, skin_id)
select
    SEQ_STATIC_LP_LANG.nextval,
    slp.id,
    10,
   15
from
     static_landing_pages slp
where
    slp.name = 'features';    
    
    
---------------------------------------------------------------
--LioR SoLoMoN - 27/08/2013 - dev's 2264 & 2248

insert into static_landing_pages (id, name, type) values (SEQ_STATIC_LP.nextval, 'Google_ET_M5_Video',3);

Insert into STATIC_LANDING_PAGES_LANGUAGE
(ID,STATIC_LANDING_PAGE_ID,LANGUAGE_ID, skin_id)
select
    SEQ_STATIC_LP_LANG.nextval,
    slp.id,
    1,
    1
from
     static_landing_pages slp
where
    slp.name = 'Google_ET_M5_Video';
    
    
insert into static_landing_pages (id, name, type) values (SEQ_STATIC_LP.nextval, 'Media_ET_M5_Video',3);

Insert into STATIC_LANDING_PAGES_LANGUAGE
(ID,STATIC_LANDING_PAGE_ID,LANGUAGE_ID, skin_id)
select
    SEQ_STATIC_LP_LANG.nextval,
    slp.id,
    1,
    1
from
     static_landing_pages slp
where
    slp.name = 'Media_ET_M5_Video';
    
    
Insert into STATIC_LANDING_PAGES_LANGUAGE
(ID,STATIC_LANDING_PAGE_ID,LANGUAGE_ID, skin_id)
select
    SEQ_STATIC_LP_LANG.nextval,
    slp.id,
    2,
    2
from
     static_landing_pages slp
where
    slp.name = '70_generic_short_testimonials';
    
    
 ---------------------------------------------------------------------------------------------------------------------------------------
 --dev 2423 - 168 vip pages with short form
 
 insert into static_landing_pages (id, name, type) values (SEQ_STATIC_LP.nextval, 'Google_168_Money_ShortClean',1);
Insert into STATIC_LANDING_PAGES_LANGUAGE
(ID,STATIC_LANDING_PAGE_ID,LANGUAGE_ID, skin_id)
select
    SEQ_STATIC_LP_LANG.nextval,
    slp.id,
    10,
    22
from
     static_landing_pages slp
where
    slp.name = 'Google_168_Money_ShortClean';
    
    
insert into static_landing_pages (id, name, type) values (SEQ_STATIC_LP.nextval, 'Google_168_Forex_ShortClean',1);
Insert into STATIC_LANDING_PAGES_LANGUAGE
(ID,STATIC_LANDING_PAGE_ID,LANGUAGE_ID, skin_id)
select
    SEQ_STATIC_LP_LANG.nextval,
    slp.id,
    10,
    22
from
     static_landing_pages slp
where
    slp.name = 'Google_168_Forex_ShortClean';
    
    
insert into static_landing_pages (id, name, type) values (SEQ_STATIC_LP.nextval, 'Google_168_Binary_ShortClean',1);
Insert into STATIC_LANDING_PAGES_LANGUAGE
(ID,STATIC_LANDING_PAGE_ID,LANGUAGE_ID, skin_id)
select
    SEQ_STATIC_LP_LANG.nextval,
    slp.id,
    10,
    22
from
     static_landing_pages slp
where
    slp.name = 'Google_168_Binary_ShortClean';
    
    
    
insert into static_landing_pages (id, name, type) values (SEQ_STATIC_LP.nextval, 'Google_168_Trade_ShortClean',1);
Insert into STATIC_LANDING_PAGES_LANGUAGE
(ID,STATIC_LANDING_PAGE_ID,LANGUAGE_ID, skin_id)
select
    SEQ_STATIC_LP_LANG.nextval,
    slp.id,
    10,
    22
from
     static_landing_pages slp
where
    slp.name = 'Google_168_Trade_ShortClean';
    
    
insert into static_landing_pages (id, name, type) values (SEQ_STATIC_LP.nextval, 'Google_168_Bursa_ShortClean',1);
Insert into STATIC_LANDING_PAGES_LANGUAGE
(ID,STATIC_LANDING_PAGE_ID,LANGUAGE_ID, skin_id)
select
    SEQ_STATIC_LP_LANG.nextval,
    slp.id,
    10,
    22
from
     static_landing_pages slp
where
    slp.name = 'Google_168_Bursa_ShortClean';
    
    
---------------------------------------------------------------------------------------------------------------------------------------------------
--Lior Rabin 
--29.10.13, dev 2447 - Upload the Make money lp with form

insert into static_landing_pages (id, name, type) values (SEQ_STATIC_LP.nextval, 'Google_AO_Branding_ShortFormCleanVideo',1);

Insert into STATIC_LANDING_PAGES_LANGUAGE
(ID,STATIC_LANDING_PAGE_ID,LANGUAGE_ID, skin_id)
select
    SEQ_STATIC_LP_LANG.nextval,
    slp.id,
    8,
    8
from
     static_landing_pages slp
where
    slp.name = 'Google_AO_Branding_ShortFormCleanVideo';
    
    
Insert into STATIC_LANDING_PAGES_LANGUAGE
(ID,STATIC_LANDING_PAGE_ID,LANGUAGE_ID, skin_id)
select
    SEQ_STATIC_LP_LANG.nextval,
    slp.id,
    2,
    2
from
     static_landing_pages slp
where
    slp.name = 'Google_AO_Branding_ShortFormCleanVideo';
    
    
Insert into STATIC_LANDING_PAGES_LANGUAGE
(ID,STATIC_LANDING_PAGE_ID,LANGUAGE_ID, skin_id)
select
    SEQ_STATIC_LP_LANG.nextval,
    slp.id,
    5,
    5
from
     static_landing_pages slp
where
    slp.name = 'Google_AO_Branding_ShortFormCleanVideo';
    
    
    
Insert into STATIC_LANDING_PAGES_LANGUAGE
(ID,STATIC_LANDING_PAGE_ID,LANGUAGE_ID, skin_id)
select
    SEQ_STATIC_LP_LANG.nextval,
    slp.id,
    9,
   12
from
     static_landing_pages slp
where
    slp.name = 'Google_AO_Branding_ShortFormCleanVideo';
    
    
    
Insert into STATIC_LANDING_PAGES_LANGUAGE
(ID,STATIC_LANDING_PAGE_ID,LANGUAGE_ID, skin_id)
select
    SEQ_STATIC_LP_LANG.nextval,
    slp.id,
   6,
   9
from
     static_landing_pages slp
where
    slp.name = 'Google_AO_Branding_ShortFormCleanVideo';
    
    
    
Insert into STATIC_LANDING_PAGES_LANGUAGE
(ID,STATIC_LANDING_PAGE_ID,LANGUAGE_ID, skin_id)
select
    SEQ_STATIC_LP_LANG.nextval,
    slp.id,
   4,
  10
from
     static_landing_pages slp
where
    slp.name = 'Google_AO_Branding_ShortFormCleanVideo';
    
    
Insert into STATIC_LANDING_PAGES_LANGUAGE
(ID,STATIC_LANDING_PAGE_ID,LANGUAGE_ID, skin_id)
select
    SEQ_STATIC_LP_LANG.nextval,
    slp.id,
   3,
  3
from
     static_landing_pages slp
where
    slp.name = 'Google_AO_Branding_ShortFormCleanVideo';
    
---------------------------------------------------------------------------------------------------------------------------------------------------
--Lior Rabin 
--24.11.13, dev 2643 - upload fixed 168 lps

insert into static_landing_pages (id, name, type) values (SEQ_STATIC_LP.nextval, 'Google_168_Binary_Clean',3);

Insert into STATIC_LANDING_PAGES_LANGUAGE
(ID,STATIC_LANDING_PAGE_ID,LANGUAGE_ID, skin_id)
select
    SEQ_STATIC_LP_LANG.nextval,
    slp.id,
    10,
    22
from
     static_landing_pages slp
where
    slp.name = 'Google_168_Binary_Clean';
    
insert into static_landing_pages (id, name, type) values (SEQ_STATIC_LP.nextval, 'Google_168_Bursa_Clean',3);

Insert into STATIC_LANDING_PAGES_LANGUAGE
(ID,STATIC_LANDING_PAGE_ID,LANGUAGE_ID, skin_id)
select
    SEQ_STATIC_LP_LANG.nextval,
    slp.id,
    10,
    22
from
     static_landing_pages slp
where
    slp.name = 'Google_168_Bursa_Clean';
    
insert into static_landing_pages (id, name, type) values (SEQ_STATIC_LP.nextval, 'Google_168_Forex_Clean',3);

Insert into STATIC_LANDING_PAGES_LANGUAGE
(ID,STATIC_LANDING_PAGE_ID,LANGUAGE_ID, skin_id)
select
    SEQ_STATIC_LP_LANG.nextval,
    slp.id,
    10,
    22
from
     static_landing_pages slp
where
    slp.name = 'Google_168_Forex_Clean';
    
insert into static_landing_pages (id, name, type) values (SEQ_STATIC_LP.nextval, 'Google_168_Invest_Clean',3);

Insert into STATIC_LANDING_PAGES_LANGUAGE
(ID,STATIC_LANDING_PAGE_ID,LANGUAGE_ID, skin_id)
select
    SEQ_STATIC_LP_LANG.nextval,
    slp.id,
    10,
    22
from
     static_landing_pages slp
where
    slp.name = 'Google_168_Invest_Clean';
    
insert into static_landing_pages (id, name, type) values (SEQ_STATIC_LP.nextval, 'Google_168_Money_Clean',3);

Insert into STATIC_LANDING_PAGES_LANGUAGE
(ID,STATIC_LANDING_PAGE_ID,LANGUAGE_ID, skin_id)
select
    SEQ_STATIC_LP_LANG.nextval,
    slp.id,
    10,
    22
from
     static_landing_pages slp
where
    slp.name = 'Google_168_Money_Clean';
    
insert into static_landing_pages (id, name, type) values (SEQ_STATIC_LP.nextval, 'Google_168_Trade_Clean',3);

Insert into STATIC_LANDING_PAGES_LANGUAGE
(ID,STATIC_LANDING_PAGE_ID,LANGUAGE_ID, skin_id)
select
    SEQ_STATIC_LP_LANG.nextval,
    slp.id,
    10,
    22
from
     static_landing_pages slp
where
    slp.name = 'Google_168_Trade_Clean';
    
insert into static_landing_pages (id, name, type) values (SEQ_STATIC_LP.nextval, '168_Risk_Free_Trade',1);

Insert into STATIC_LANDING_PAGES_LANGUAGE
(ID,STATIC_LANDING_PAGE_ID,LANGUAGE_ID, skin_id)
select
    SEQ_STATIC_LP_LANG.nextval,
    slp.id,
    10,
    22
from
     static_landing_pages slp
where
    slp.name = '168_Risk_Free_Trade';
    
insert into static_landing_pages (id, name, type) values (SEQ_STATIC_LP.nextval, '168_Bitcoin_Clean',1);

Insert into STATIC_LANDING_PAGES_LANGUAGE
(ID,STATIC_LANDING_PAGE_ID,LANGUAGE_ID, skin_id)
select
    SEQ_STATIC_LP_LANG.nextval,
    slp.id,
    10,
    22
from
     static_landing_pages slp
where
    slp.name = '168_Bitcoin_Clean';
    
---------------------------------------------------------------------------------------------------------------------------------------------------
--Lior Rabin 
--25.11.13, dev 2615 - ExoClick_AO_Money_SecretVideo_SF

insert into static_landing_pages (id, name, type) values (SEQ_STATIC_LP.nextval, 'ExoClick_AO_Money_SecretVideo_SF',2);

insert into STATIC_LANDING_PAGES_LANGUAGE
(ID,STATIC_LANDING_PAGE_ID,LANGUAGE_ID, skin_id)
select
    SEQ_STATIC_LP_LANG.nextval,
    slp.id,
    8,
    8
from
     static_landing_pages slp
where
    slp.name = 'ExoClick_AO_Money_SecretVideo_SF';
	
insert into STATIC_LANDING_PAGES_LANGUAGE
(ID,STATIC_LANDING_PAGE_ID,LANGUAGE_ID, skin_id)
select
    SEQ_STATIC_LP_LANG.nextval,
    slp.id,
    2,
    2
from
     static_landing_pages slp
where
    slp.name = 'ExoClick_AO_Money_SecretVideo_SF';
	
insert into STATIC_LANDING_PAGES_LANGUAGE
(ID,STATIC_LANDING_PAGE_ID,LANGUAGE_ID, skin_id)
select
    SEQ_STATIC_LP_LANG.nextval,
    slp.id,
    5,
    5
from
     static_landing_pages slp
where
    slp.name = 'ExoClick_AO_Money_SecretVideo_SF';
	
insert into STATIC_LANDING_PAGES_LANGUAGE
(ID,STATIC_LANDING_PAGE_ID,LANGUAGE_ID, skin_id)
select
    SEQ_STATIC_LP_LANG.nextval,
    slp.id,
    9,
    12
from
     static_landing_pages slp
where
    slp.name = 'ExoClick_AO_Money_SecretVideo_SF';
	
insert into STATIC_LANDING_PAGES_LANGUAGE
(ID,STATIC_LANDING_PAGE_ID,LANGUAGE_ID, skin_id)
select
    SEQ_STATIC_LP_LANG.nextval,
    slp.id,
    4,
    10
from
     static_landing_pages slp
where
    slp.name = 'ExoClick_AO_Money_SecretVideo_SF';
	
insert into STATIC_LANDING_PAGES_LANGUAGE
(ID,STATIC_LANDING_PAGE_ID,LANGUAGE_ID, skin_id)
select
    SEQ_STATIC_LP_LANG.nextval,
    slp.id,
    3,
    3
from
     static_landing_pages slp
where
    slp.name = 'ExoClick_AO_Money_SecretVideo_SF';
    
---------------------------------------------------------------------------------------------------------------------------------------------------
--Lior Rabin 
--25.11.13, dev 2616 - AO_Money_SecretVideo_SF

insert into static_landing_pages (id, name, type) values (SEQ_STATIC_LP.nextval, 'AO_Money_SecretVideo_SF',2);

insert into STATIC_LANDING_PAGES_LANGUAGE
(ID,STATIC_LANDING_PAGE_ID,LANGUAGE_ID, skin_id)
select
    SEQ_STATIC_LP_LANG.nextval,
    slp.id,
    8,
    8
from
     static_landing_pages slp
where
    slp.name = 'AO_Money_SecretVideo_SF';
	
insert into STATIC_LANDING_PAGES_LANGUAGE
(ID,STATIC_LANDING_PAGE_ID,LANGUAGE_ID, skin_id)
select
    SEQ_STATIC_LP_LANG.nextval,
    slp.id,
    2,
    2
from
     static_landing_pages slp
where
    slp.name = 'AO_Money_SecretVideo_SF';
	
insert into STATIC_LANDING_PAGES_LANGUAGE
(ID,STATIC_LANDING_PAGE_ID,LANGUAGE_ID, skin_id)
select
    SEQ_STATIC_LP_LANG.nextval,
    slp.id,
    5,
    5
from
     static_landing_pages slp
where
    slp.name = 'AO_Money_SecretVideo_SF';
	
insert into STATIC_LANDING_PAGES_LANGUAGE
(ID,STATIC_LANDING_PAGE_ID,LANGUAGE_ID, skin_id)
select
    SEQ_STATIC_LP_LANG.nextval,
    slp.id,
    9,
    12
from
     static_landing_pages slp
where
    slp.name = 'AO_Money_SecretVideo_SF';
	
insert into STATIC_LANDING_PAGES_LANGUAGE
(ID,STATIC_LANDING_PAGE_ID,LANGUAGE_ID, skin_id)
select
    SEQ_STATIC_LP_LANG.nextval,
    slp.id,
    6,
    20
from
     static_landing_pages slp
where
    slp.name = 'AO_Money_SecretVideo_SF';
	
insert into STATIC_LANDING_PAGES_LANGUAGE
(ID,STATIC_LANDING_PAGE_ID,LANGUAGE_ID, skin_id)
select
    SEQ_STATIC_LP_LANG.nextval,
    slp.id,
    4,
    10
from
     static_landing_pages slp
where
    slp.name = 'AO_Money_SecretVideo_SF';
	
insert into STATIC_LANDING_PAGES_LANGUAGE
(ID,STATIC_LANDING_PAGE_ID,LANGUAGE_ID, skin_id)
select
    SEQ_STATIC_LP_LANG.nextval,
    slp.id,
    3,
    3
from
     static_landing_pages slp
where
    slp.name = 'AO_Money_SecretVideo_SF';
    
---------------------------------------------------------------------------------------------------------------------------------------------------
--Lior Rabin 
--25.11.13, dev 2617 - LP_testimonials2_EU

insert into static_landing_pages (id, name, type) values (SEQ_STATIC_LP.nextval, 'LP_testimonials2_EU',2);

insert into STATIC_LANDING_PAGES_LANGUAGE
(ID,STATIC_LANDING_PAGE_ID,LANGUAGE_ID, skin_id)
select
    SEQ_STATIC_LP_LANG.nextval,
    slp.id,
    8,
    8
from
     static_landing_pages slp
where
    slp.name = 'LP_testimonials2_EU';
	
insert into STATIC_LANDING_PAGES_LANGUAGE
(ID,STATIC_LANDING_PAGE_ID,LANGUAGE_ID, skin_id)
select
    SEQ_STATIC_LP_LANG.nextval,
    slp.id,
    2,
    2
from
     static_landing_pages slp
where
    slp.name = 'LP_testimonials2_EU';
	
insert into STATIC_LANDING_PAGES_LANGUAGE
(ID,STATIC_LANDING_PAGE_ID,LANGUAGE_ID, skin_id)
select
    SEQ_STATIC_LP_LANG.nextval,
    slp.id,
    5,
    5
from
     static_landing_pages slp
where
    slp.name = 'LP_testimonials2_EU';
	
insert into STATIC_LANDING_PAGES_LANGUAGE
(ID,STATIC_LANDING_PAGE_ID,LANGUAGE_ID, skin_id)
select
    SEQ_STATIC_LP_LANG.nextval,
    slp.id,
    9,
    12
from
     static_landing_pages slp
where
    slp.name = 'LP_testimonials2_EU';
	
insert into STATIC_LANDING_PAGES_LANGUAGE
(ID,STATIC_LANDING_PAGE_ID,LANGUAGE_ID, skin_id)
select
    SEQ_STATIC_LP_LANG.nextval,
    slp.id,
    6,
    20
from
     static_landing_pages slp
where
    slp.name = 'LP_testimonials2_EU';
	
insert into STATIC_LANDING_PAGES_LANGUAGE
(ID,STATIC_LANDING_PAGE_ID,LANGUAGE_ID, skin_id)
select
    SEQ_STATIC_LP_LANG.nextval,
    slp.id,
    3,
    3
from
     static_landing_pages slp
where
    slp.name = 'LP_testimonials2_EU';
	
insert into STATIC_LANDING_PAGES_LANGUAGE
(ID,STATIC_LANDING_PAGE_ID,LANGUAGE_ID, skin_id)
select
    SEQ_STATIC_LP_LANG.nextval,
    slp.id,
    10,
    15
from
     static_landing_pages slp
where
    slp.name = 'LP_testimonials2_EU';
    
---------------------------------------------------------------------------------------------------------------------------------------------------
--dev 2644 - New page for etrader- until
insert into static_landing_pages (id, name, type) values (SEQ_STATIC_LP.nextval, 'ET_Until_NF',3);

Insert into STATIC_LANDING_PAGES_LANGUAGE
(ID,STATIC_LANDING_PAGE_ID,LANGUAGE_ID, skin_id)
select
    SEQ_STATIC_LP_LANG.nextval,
    slp.id,
    1,
    1
from
     static_landing_pages slp
where
    slp.name = 'ET_Until_NF'; 