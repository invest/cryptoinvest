select  'COUNT ALL', 'COUNT SUCCESS','AMOUNT SUCESS','PERCENT SUCESS','PROVIDER','SPREAD'
from dual
union all
select * from
(select to_char(mm.c_all),
       to_char(mm.c_success),
       to_char(mm.c_success_am),
       to_char(round(mm.c_success / mm.c_all * 100, 2)) c_success_percent,
       to_char(nvl(mm.c_provider, 'TOTAL')) c_provider,
       to_char(round(mm.c_all / sum(decode(mm.c_provider, null, mm.c_all, 0))
             over() * 100,
             2)) c_spread     
from (select count(t.id) c_all,
               sum(decode(t.status_id, 2, 1, 7, 1, 0)) c_success,
               sum(decode(t.status_id, 2, amount, 7, amount, 0)) / 100 c_success_am,
               cp.Name c_provider
          from transactions t, user_to_provider utp, clearing_providers cp
         where t.type_id = 38
           and t.user_id = utp.user_id
           and utp.provider_id = cp.id
           --and trunc(t.time_created)=trunc(sysdate-1)
           and trunc(t.time_created) between trunc(sysdate-7) and trunc(sysdate-1)
           --and trunc(t.time_created) between trunc(sysdate-30) and trunc(sysdate-1)
         group by ROLLUP(cp.Name)
         order by cp.Name nulls first) mm
order by 5);