--******************************************
--BEGIN v1.0.0  NEW CRYPTO INVEST PROJECT: USPEH!!!  
--******************************************

insert into file_types
  (id, name, is_credit_card_file, is_keesing_type, file_group_id)
values
  (39, 'filetypes.passport.front', 0, 0, 10);
  
 insert into file_types
  (id, name, is_credit_card_file, is_keesing_type, file_group_id)
values
  (40, 'filetypes.passport.back', 0, 0, 10);
  
 insert into file_types
  (id, name, is_credit_card_file, is_keesing_type, file_group_id)
values
  (41, 'filetypes.national.id.front', 0, 0, 10);  

 insert into file_types
  (id, name, is_credit_card_file, is_keesing_type, file_group_id)
values
  (42, 'filetypes.national.id.back', 0, 0, 10);  
  

 insert into file_types
  (id, name, is_credit_card_file, is_keesing_type, file_group_id)
values
  (43, 'filetypes.passport.selfie', 0, 0, 11);    
  
  
insert into file_types
  (id, name, is_credit_card_file, is_keesing_type, file_group_id)
values
  (44, 'filetypes.national.id.selfie', 0, 0, 11);    
  

insert into file_types
  (id, name, is_credit_card_file, is_keesing_type, file_group_id)
values
  (45, 'filetypes.proof.of.residence', 0, 0, 12);
  
  insert into files_additional_info
  (file_type, id_number, first_name, last_name, exp_date, cc_exp_date, country, date_of_birth)
values
  (39, 0, 1, 1, 1, 0, 1, 1);
  
  insert into files_additional_info
  (file_type, id_number, first_name, last_name, exp_date, cc_exp_date, country, date_of_birth)
values
  (40, 0, 1, 1, 1, 0, 1, 1);
  
  insert into files_additional_info
  (file_type, id_number, first_name, last_name, exp_date, cc_exp_date, country, date_of_birth)
values
  (41, 0, 1, 1, 1, 0, 1, 1);
  
  @pkg_user_documents.pck
  @pkg_user.pks
  
-- Create table
create table LANGUAGE_COUNTRY_MAP
(
  language_id NUMBER not null,
  country_id  NUMBER not null
)
tablespace USERS
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
-- Create/Recreate primary, unique and foreign key constraints 
alter table LANGUAGE_COUNTRY_MAP
  add constraint LAN_CNTRY_MAP_UQ unique (LANGUAGE_ID, COUNTRY_ID)
  using index 
  tablespace USERS
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
alter table LANGUAGE_COUNTRY_MAP
  add constraint COUNTRY_ID_FK foreign key (COUNTRY_ID)
  references COUNTRIES (ID);
alter table LANGUAGE_COUNTRY_MAP
  add constraint LANGUAGE_ID_FK foreign key (LANGUAGE_ID)
  references LANGUAGES (ID);
-- Grant/Revoke object privileges 
grant select, update, insert on ETRADER.LANGUAGE_COUNTRY_MAP to ETRADER_WEB_CONNECT;
create public synonym LANGUAGE_COUNTRY_MAP for ETRADER.LANGUAGE_COUNTRY_MAP;


insert into language_country_map (LANGUAGE_ID, COUNTRY_ID)
values (2, 1);

insert into language_country_map (LANGUAGE_ID, COUNTRY_ID)
values (2, 2);

insert into language_country_map (LANGUAGE_ID, COUNTRY_ID)
values (2, 3);

insert into language_country_map (LANGUAGE_ID, COUNTRY_ID)
values (2, 4);

insert into language_country_map (LANGUAGE_ID, COUNTRY_ID)
values (2, 5);

insert into language_country_map (LANGUAGE_ID, COUNTRY_ID)
values (5, 6);

insert into language_country_map (LANGUAGE_ID, COUNTRY_ID)
values (2, 7);

insert into language_country_map (LANGUAGE_ID, COUNTRY_ID)
values (2, 8);

insert into language_country_map (LANGUAGE_ID, COUNTRY_ID)
values (2, 9);

insert into language_country_map (LANGUAGE_ID, COUNTRY_ID)
values (2, 10);

insert into language_country_map (LANGUAGE_ID, COUNTRY_ID)
values (5, 11);

insert into language_country_map (LANGUAGE_ID, COUNTRY_ID)
values (2, 12);

insert into language_country_map (LANGUAGE_ID, COUNTRY_ID)
values (2, 13);

insert into language_country_map (LANGUAGE_ID, COUNTRY_ID)
values (2, 14);

insert into language_country_map (LANGUAGE_ID, COUNTRY_ID)
values (8, 15);

insert into language_country_map (LANGUAGE_ID, COUNTRY_ID)
values (2, 16);

insert into language_country_map (LANGUAGE_ID, COUNTRY_ID)
values (2, 17);

insert into language_country_map (LANGUAGE_ID, COUNTRY_ID)
values (2, 18);

insert into language_country_map (LANGUAGE_ID, COUNTRY_ID)
values (2, 19);

insert into language_country_map (LANGUAGE_ID, COUNTRY_ID)
values (2, 20);

insert into language_country_map (LANGUAGE_ID, COUNTRY_ID)
values (2, 21);

insert into language_country_map (LANGUAGE_ID, COUNTRY_ID)
values (9, 22);

insert into language_country_map (LANGUAGE_ID, COUNTRY_ID)
values (5, 23);

insert into language_country_map (LANGUAGE_ID, COUNTRY_ID)
values (2, 24);

insert into language_country_map (LANGUAGE_ID, COUNTRY_ID)
values (2, 25);

insert into language_country_map (LANGUAGE_ID, COUNTRY_ID)
values (2, 26);

insert into language_country_map (LANGUAGE_ID, COUNTRY_ID)
values (5, 27);

insert into language_country_map (LANGUAGE_ID, COUNTRY_ID)
values (2, 28);

insert into language_country_map (LANGUAGE_ID, COUNTRY_ID)
values (2, 29);

insert into language_country_map (LANGUAGE_ID, COUNTRY_ID)
values (2, 30);

insert into language_country_map (LANGUAGE_ID, COUNTRY_ID)
values (2, 31);

insert into language_country_map (LANGUAGE_ID, COUNTRY_ID)
values (2, 32);

insert into language_country_map (LANGUAGE_ID, COUNTRY_ID)
values (2, 33);

insert into language_country_map (LANGUAGE_ID, COUNTRY_ID)
values (2, 34);

insert into language_country_map (LANGUAGE_ID, COUNTRY_ID)
values (2, 35);

insert into language_country_map (LANGUAGE_ID, COUNTRY_ID)
values (2, 36);

insert into language_country_map (LANGUAGE_ID, COUNTRY_ID)
values (2, 37);

insert into language_country_map (LANGUAGE_ID, COUNTRY_ID)
values (2, 38);

insert into language_country_map (LANGUAGE_ID, COUNTRY_ID)
values (2, 39);

insert into language_country_map (LANGUAGE_ID, COUNTRY_ID)
values (2, 40);

insert into language_country_map (LANGUAGE_ID, COUNTRY_ID)
values (2, 41);

insert into language_country_map (LANGUAGE_ID, COUNTRY_ID)
values (2, 42);

insert into language_country_map (LANGUAGE_ID, COUNTRY_ID)
values (5, 43);

insert into language_country_map (LANGUAGE_ID, COUNTRY_ID)
values (2, 44);

insert into language_country_map (LANGUAGE_ID, COUNTRY_ID)
values (2, 45);

insert into language_country_map (LANGUAGE_ID, COUNTRY_ID)
values (2, 46);

insert into language_country_map (LANGUAGE_ID, COUNTRY_ID)
values (5, 47);

insert into language_country_map (LANGUAGE_ID, COUNTRY_ID)
values (2, 48);

insert into language_country_map (LANGUAGE_ID, COUNTRY_ID)
values (2, 49);

insert into language_country_map (LANGUAGE_ID, COUNTRY_ID)
values (2, 50);

insert into language_country_map (LANGUAGE_ID, COUNTRY_ID)
values (5, 51);

insert into language_country_map (LANGUAGE_ID, COUNTRY_ID)
values (2, 52);

insert into language_country_map (LANGUAGE_ID, COUNTRY_ID)
values (2, 53);

insert into language_country_map (LANGUAGE_ID, COUNTRY_ID)
values (5, 54);

insert into language_country_map (LANGUAGE_ID, COUNTRY_ID)
values (2, 55);

insert into language_country_map (LANGUAGE_ID, COUNTRY_ID)
values (2, 56);

insert into language_country_map (LANGUAGE_ID, COUNTRY_ID)
values (2, 57);

insert into language_country_map (LANGUAGE_ID, COUNTRY_ID)
values (2, 58);

insert into language_country_map (LANGUAGE_ID, COUNTRY_ID)
values (2, 59);

insert into language_country_map (LANGUAGE_ID, COUNTRY_ID)
values (5, 60);

insert into language_country_map (LANGUAGE_ID, COUNTRY_ID)
values (2, 61);

insert into language_country_map (LANGUAGE_ID, COUNTRY_ID)
values (5, 62);

insert into language_country_map (LANGUAGE_ID, COUNTRY_ID)
values (2, 63);

insert into language_country_map (LANGUAGE_ID, COUNTRY_ID)
values (5, 64);

insert into language_country_map (LANGUAGE_ID, COUNTRY_ID)
values (5, 65);

insert into language_country_map (LANGUAGE_ID, COUNTRY_ID)
values (2, 66);

insert into language_country_map (LANGUAGE_ID, COUNTRY_ID)
values (2, 67);

insert into language_country_map (LANGUAGE_ID, COUNTRY_ID)
values (2, 68);

insert into language_country_map (LANGUAGE_ID, COUNTRY_ID)
values (2, 69);

insert into language_country_map (LANGUAGE_ID, COUNTRY_ID)
values (2, 70);

insert into language_country_map (LANGUAGE_ID, COUNTRY_ID)
values (2, 71);

insert into language_country_map (LANGUAGE_ID, COUNTRY_ID)
values (2, 72);

insert into language_country_map (LANGUAGE_ID, COUNTRY_ID)
values (9, 73);

insert into language_country_map (LANGUAGE_ID, COUNTRY_ID)
values (2, 74);

insert into language_country_map (LANGUAGE_ID, COUNTRY_ID)
values (2, 75);

insert into language_country_map (LANGUAGE_ID, COUNTRY_ID)
values (2, 77);

insert into language_country_map (LANGUAGE_ID, COUNTRY_ID)
values (2, 78);

insert into language_country_map (LANGUAGE_ID, COUNTRY_ID)
values (2, 79);

insert into language_country_map (LANGUAGE_ID, COUNTRY_ID)
values (8, 80);

insert into language_country_map (LANGUAGE_ID, COUNTRY_ID)
values (2, 81);

insert into language_country_map (LANGUAGE_ID, COUNTRY_ID)
values (2, 82);

insert into language_country_map (LANGUAGE_ID, COUNTRY_ID)
values (2, 83);

insert into language_country_map (LANGUAGE_ID, COUNTRY_ID)
values (2, 84);

insert into language_country_map (LANGUAGE_ID, COUNTRY_ID)
values (2, 85);

insert into language_country_map (LANGUAGE_ID, COUNTRY_ID)
values (2, 86);

insert into language_country_map (LANGUAGE_ID, COUNTRY_ID)
values (2, 87);

insert into language_country_map (LANGUAGE_ID, COUNTRY_ID)
values (5, 88);

insert into language_country_map (LANGUAGE_ID, COUNTRY_ID)
values (2, 89);

insert into language_country_map (LANGUAGE_ID, COUNTRY_ID)
values (2, 90);

insert into language_country_map (LANGUAGE_ID, COUNTRY_ID)
values (2, 91);

insert into language_country_map (LANGUAGE_ID, COUNTRY_ID)
values (5, 93);

insert into language_country_map (LANGUAGE_ID, COUNTRY_ID)
values (2, 94);

insert into language_country_map (LANGUAGE_ID, COUNTRY_ID)
values (2, 95);

insert into language_country_map (LANGUAGE_ID, COUNTRY_ID)
values (2, 96);

insert into language_country_map (LANGUAGE_ID, COUNTRY_ID)
values (2, 97);

insert into language_country_map (LANGUAGE_ID, COUNTRY_ID)
values (2, 98);

insert into language_country_map (LANGUAGE_ID, COUNTRY_ID)
values (2, 99);

insert into language_country_map (LANGUAGE_ID, COUNTRY_ID)
values (2, 100);

insert into language_country_map (LANGUAGE_ID, COUNTRY_ID)
values (2, 101);

insert into language_country_map (LANGUAGE_ID, COUNTRY_ID)
values (6, 102);

insert into language_country_map (LANGUAGE_ID, COUNTRY_ID)
values (2, 103);

insert into language_country_map (LANGUAGE_ID, COUNTRY_ID)
values (2, 104);

insert into language_country_map (LANGUAGE_ID, COUNTRY_ID)
values (2, 105);

insert into language_country_map (LANGUAGE_ID, COUNTRY_ID)
values (2, 106);

insert into language_country_map (LANGUAGE_ID, COUNTRY_ID)
values (2, 107);

insert into language_country_map (LANGUAGE_ID, COUNTRY_ID)
values (2, 108);

insert into language_country_map (LANGUAGE_ID, COUNTRY_ID)
values (2, 109);

insert into language_country_map (LANGUAGE_ID, COUNTRY_ID)
values (2, 110);

insert into language_country_map (LANGUAGE_ID, COUNTRY_ID)
values (2, 111);

insert into language_country_map (LANGUAGE_ID, COUNTRY_ID)
values (2, 112);

insert into language_country_map (LANGUAGE_ID, COUNTRY_ID)
values (2, 113);

insert into language_country_map (LANGUAGE_ID, COUNTRY_ID)
values (2, 114);

insert into language_country_map (LANGUAGE_ID, COUNTRY_ID)
values (2, 115);

insert into language_country_map (LANGUAGE_ID, COUNTRY_ID)
values (2, 116);

insert into language_country_map (LANGUAGE_ID, COUNTRY_ID)
values (2, 117);

insert into language_country_map (LANGUAGE_ID, COUNTRY_ID)
values (2, 118);

insert into language_country_map (LANGUAGE_ID, COUNTRY_ID)
values (9, 119);

insert into language_country_map (LANGUAGE_ID, COUNTRY_ID)
values (2, 120);

insert into language_country_map (LANGUAGE_ID, COUNTRY_ID)
values (2, 121);

insert into language_country_map (LANGUAGE_ID, COUNTRY_ID)
values (2, 122);

insert into language_country_map (LANGUAGE_ID, COUNTRY_ID)
values (2, 123);

insert into language_country_map (LANGUAGE_ID, COUNTRY_ID)
values (2, 124);

insert into language_country_map (LANGUAGE_ID, COUNTRY_ID)
values (2, 125);

insert into language_country_map (LANGUAGE_ID, COUNTRY_ID)
values (2, 126);

insert into language_country_map (LANGUAGE_ID, COUNTRY_ID)
values (2, 127);

insert into language_country_map (LANGUAGE_ID, COUNTRY_ID)
values (2, 128);

insert into language_country_map (LANGUAGE_ID, COUNTRY_ID)
values (2, 129);

insert into language_country_map (LANGUAGE_ID, COUNTRY_ID)
values (2, 130);

insert into language_country_map (LANGUAGE_ID, COUNTRY_ID)
values (2, 131);

insert into language_country_map (LANGUAGE_ID, COUNTRY_ID)
values (2, 132);

insert into language_country_map (LANGUAGE_ID, COUNTRY_ID)
values (5, 133);

insert into language_country_map (LANGUAGE_ID, COUNTRY_ID)
values (2, 134);

insert into language_country_map (LANGUAGE_ID, COUNTRY_ID)
values (2, 135);

insert into language_country_map (LANGUAGE_ID, COUNTRY_ID)
values (2, 136);

insert into language_country_map (LANGUAGE_ID, COUNTRY_ID)
values (2, 137);

insert into language_country_map (LANGUAGE_ID, COUNTRY_ID)
values (2, 138);

insert into language_country_map (LANGUAGE_ID, COUNTRY_ID)
values (2, 139);

insert into language_country_map (LANGUAGE_ID, COUNTRY_ID)
values (2, 140);

insert into language_country_map (LANGUAGE_ID, COUNTRY_ID)
values (2, 141);

insert into language_country_map (LANGUAGE_ID, COUNTRY_ID)
values (2, 142);

insert into language_country_map (LANGUAGE_ID, COUNTRY_ID)
values (2, 143);

insert into language_country_map (LANGUAGE_ID, COUNTRY_ID)
values (2, 144);

insert into language_country_map (LANGUAGE_ID, COUNTRY_ID)
values (12, 145);

insert into language_country_map (LANGUAGE_ID, COUNTRY_ID)
values (2, 146);

insert into language_country_map (LANGUAGE_ID, COUNTRY_ID)
values (2, 147);

insert into language_country_map (LANGUAGE_ID, COUNTRY_ID)
values (2, 148);

insert into language_country_map (LANGUAGE_ID, COUNTRY_ID)
values (5, 149);

insert into language_country_map (LANGUAGE_ID, COUNTRY_ID)
values (2, 150);

insert into language_country_map (LANGUAGE_ID, COUNTRY_ID)
values (2, 151);

insert into language_country_map (LANGUAGE_ID, COUNTRY_ID)
values (2, 152);

insert into language_country_map (LANGUAGE_ID, COUNTRY_ID)
values (2, 153);

insert into language_country_map (LANGUAGE_ID, COUNTRY_ID)
values (2, 154);

insert into language_country_map (LANGUAGE_ID, COUNTRY_ID)
values (2, 156);

insert into language_country_map (LANGUAGE_ID, COUNTRY_ID)
values (2, 157);

insert into language_country_map (LANGUAGE_ID, COUNTRY_ID)
values (2, 158);

insert into language_country_map (LANGUAGE_ID, COUNTRY_ID)
values (2, 159);

insert into language_country_map (LANGUAGE_ID, COUNTRY_ID)
values (5, 160);

insert into language_country_map (LANGUAGE_ID, COUNTRY_ID)
values (2, 161);

insert into language_country_map (LANGUAGE_ID, COUNTRY_ID)
values (5, 162);

insert into language_country_map (LANGUAGE_ID, COUNTRY_ID)
values (5, 163);

insert into language_country_map (LANGUAGE_ID, COUNTRY_ID)
values (2, 164);

insert into language_country_map (LANGUAGE_ID, COUNTRY_ID)
values (2, 165);

insert into language_country_map (LANGUAGE_ID, COUNTRY_ID)
values (2, 166);

insert into language_country_map (LANGUAGE_ID, COUNTRY_ID)
values (2, 167);

insert into language_country_map (LANGUAGE_ID, COUNTRY_ID)
values (5, 168);

insert into language_country_map (LANGUAGE_ID, COUNTRY_ID)
values (2, 169);

insert into language_country_map (LANGUAGE_ID, COUNTRY_ID)
values (2, 170);

insert into language_country_map (LANGUAGE_ID, COUNTRY_ID)
values (2, 171);

insert into language_country_map (LANGUAGE_ID, COUNTRY_ID)
values (2, 172);

insert into language_country_map (LANGUAGE_ID, COUNTRY_ID)
values (2, 173);

insert into language_country_map (LANGUAGE_ID, COUNTRY_ID)
values (2, 174);

insert into language_country_map (LANGUAGE_ID, COUNTRY_ID)
values (2, 175);

insert into language_country_map (LANGUAGE_ID, COUNTRY_ID)
values (2, 177);

insert into language_country_map (LANGUAGE_ID, COUNTRY_ID)
values (2, 178);

insert into language_country_map (LANGUAGE_ID, COUNTRY_ID)
values (2, 180);

insert into language_country_map (LANGUAGE_ID, COUNTRY_ID)
values (2, 181);

insert into language_country_map (LANGUAGE_ID, COUNTRY_ID)
values (2, 182);

insert into language_country_map (LANGUAGE_ID, COUNTRY_ID)
values (2, 183);

insert into language_country_map (LANGUAGE_ID, COUNTRY_ID)
values (2, 184);

insert into language_country_map (LANGUAGE_ID, COUNTRY_ID)
values (2, 185);

insert into language_country_map (LANGUAGE_ID, COUNTRY_ID)
values (2, 186);

insert into language_country_map (LANGUAGE_ID, COUNTRY_ID)
values (2, 187);

insert into language_country_map (LANGUAGE_ID, COUNTRY_ID)
values (2, 188);

insert into language_country_map (LANGUAGE_ID, COUNTRY_ID)
values (2, 189);

insert into language_country_map (LANGUAGE_ID, COUNTRY_ID)
values (2, 191);

insert into language_country_map (LANGUAGE_ID, COUNTRY_ID)
values (5, 192);

insert into language_country_map (LANGUAGE_ID, COUNTRY_ID)
values (2, 193);

insert into language_country_map (LANGUAGE_ID, COUNTRY_ID)
values (2, 194);

insert into language_country_map (LANGUAGE_ID, COUNTRY_ID)
values (2, 196);

insert into language_country_map (LANGUAGE_ID, COUNTRY_ID)
values (2, 197);

insert into language_country_map (LANGUAGE_ID, COUNTRY_ID)
values (2, 199);

insert into language_country_map (LANGUAGE_ID, COUNTRY_ID)
values (2, 200);

insert into language_country_map (LANGUAGE_ID, COUNTRY_ID)
values (2, 201);

insert into language_country_map (LANGUAGE_ID, COUNTRY_ID)
values (2, 202);

insert into language_country_map (LANGUAGE_ID, COUNTRY_ID)
values (2, 203);

insert into language_country_map (LANGUAGE_ID, COUNTRY_ID)
values (2, 204);

insert into language_country_map (LANGUAGE_ID, COUNTRY_ID)
values (2, 205);

insert into language_country_map (LANGUAGE_ID, COUNTRY_ID)
values (2, 206);

insert into language_country_map (LANGUAGE_ID, COUNTRY_ID)
values (2, 207);

insert into language_country_map (LANGUAGE_ID, COUNTRY_ID)
values (2, 208);

insert into language_country_map (LANGUAGE_ID, COUNTRY_ID)
values (2, 209);

insert into language_country_map (LANGUAGE_ID, COUNTRY_ID)
values (2, 210);

insert into language_country_map (LANGUAGE_ID, COUNTRY_ID)
values (2, 211);

insert into language_country_map (LANGUAGE_ID, COUNTRY_ID)
values (3, 212);

insert into language_country_map (LANGUAGE_ID, COUNTRY_ID)
values (2, 213);

insert into language_country_map (LANGUAGE_ID, COUNTRY_ID)
values (2, 215);

insert into language_country_map (LANGUAGE_ID, COUNTRY_ID)
values (2, 216);

insert into language_country_map (LANGUAGE_ID, COUNTRY_ID)
values (2, 217);

insert into language_country_map (LANGUAGE_ID, COUNTRY_ID)
values (2, 218);

insert into language_country_map (LANGUAGE_ID, COUNTRY_ID)
values (2, 219);

insert into language_country_map (LANGUAGE_ID, COUNTRY_ID)
values (2, 220);

insert into language_country_map (LANGUAGE_ID, COUNTRY_ID)
values (5, 221);

insert into language_country_map (LANGUAGE_ID, COUNTRY_ID)
values (2, 222);

insert into language_country_map (LANGUAGE_ID, COUNTRY_ID)
values (2, 223);

insert into language_country_map (LANGUAGE_ID, COUNTRY_ID)
values (2, 224);

insert into language_country_map (LANGUAGE_ID, COUNTRY_ID)
values (5, 225);

insert into language_country_map (LANGUAGE_ID, COUNTRY_ID)
values (2, 226);

insert into language_country_map (LANGUAGE_ID, COUNTRY_ID)
values (2, 227);

insert into language_country_map (LANGUAGE_ID, COUNTRY_ID)
values (2, 228);

insert into language_country_map (LANGUAGE_ID, COUNTRY_ID)
values (2, 230);

insert into language_country_map (LANGUAGE_ID, COUNTRY_ID)
values (2, 231);

insert into language_country_map (LANGUAGE_ID, COUNTRY_ID)
values (2, 233);

insert into language_country_map (LANGUAGE_ID, COUNTRY_ID)
values (2, 234);

insert into language_country_map (LANGUAGE_ID, COUNTRY_ID)
values (2, 235);

insert into language_country_map (LANGUAGE_ID, COUNTRY_ID)
values (2, 236);

insert into language_country_map (LANGUAGE_ID, COUNTRY_ID)
values (2, 237);

update countries c set c.is_blocked = 0;  
--******************************************
--BEGIN v1.0.0  NI-642: USPEH!!!  
--******************************************

create table CRYPTHO_CONFIG
(
  config_key   VARCHAR2(50) not null,
  config_value VARCHAR2(255) not null
)
tablespace USERS
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
  
  alter table CRYPTHO_CONFIG
  add constraint CONFIG_KEY_PK primary key (CONFIG_KEY)
  using index 
  tablespace USERS
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );

insert into CRYPTHO_CONFIG
(config_key, config_value)
VALUES
('captcha', 'false');

grant select, update, insert on ETRADER.cryptho_config to ETRADER_WEB_CONNECT;
create public synonym cryptho_config for ETRADER.cryptho_config;


--******************************************
--END v1.0.0  NI-642: USPEH!!!  
--******************************************

-------------------------------------------------------------------------------------------------------------------------------------------
-- NI-643 Crypto Invest Ph2 Create Registartion Page
-- Kiril
-------------------------------------------------------------------------------------------------------------------------------------------
ALTER TABLE USERS  
MODIFY (FIRST_NAME NULL);

ALTER TABLE USERS  
MODIFY (LAST_NAME NULL);

ALTER TABLE USERS  
MODIFY (ID_NUM NULL);

ALTER TABLE USERS  
MODIFY (UTC_OFFSET_MODIFIED NULL);

ALTER TABLE USERS  
MODIFY (UTC_OFFSET NULL);

ALTER TABLE USERS  
MODIFY (UTC_OFFSET_CREATED NULL);

insert into platforms values (10, 'cryptoinvest', null, null);

CREATE TABLE CRYPTO_TOKENS 
(
  ID NUMBER(38) NOT NULL 
, USER_ID NUMBER(38) NOT NULL 
, TIME_CREATED DATE NOT NULL 
, VALIDITY DATE NOT NULL 
, TOKEN VARCHAR2(40) NOT NULL 
, TOKEN_TYPE NUMBER NOT NULL 
, CONSTRAINT CRYPTO_TOKENS_PK PRIMARY KEY 
  (
    ID 
  )
  ENABLE 
);

ALTER TABLE CRYPTO_TOKENS
ADD CONSTRAINT FK_USERS_CRYPTO_TOKENS FOREIGN KEY
(
  USER_ID 
)
REFERENCES USERS
(
  ID 
)
ENABLE;

COMMENT ON COLUMN CRYPTO_TOKENS.TOKEN_TYPE IS '1 - verify email, 2 - reset password';

CREATE PUBLIC SYNONYM CRYPTO_TOKENS FOR ETRADER.CRYPTO_TOKENS;
GRANT SELECT, INSERT, UPDATE ON CRYPTO_TOKENS TO ETRADER_WEB_CONNECT;

CREATE SEQUENCE SEQ_CRYPTO_TOKENS;
CREATE PUBLIC SYNONYM SEQ_CRYPTO_TOKENS FOR SEQ_CRYPTO_TOKENS;

ALTER TABLE CRYPTO_TOKENS 
ADD (IS_USED NUMBER DEFAULT 0 );
-------------------------------------------------------------------------------------------------------------------------------------------
-- End
-------------------------------------------------------------------------------------------------------------------------------------------

--------------------------------------------------------------------------------------------
--Regulation
--------------------------------------------------------------------------------------------
ALTER TABLE users_regulation
  RENAME COLUMN wc_approval TO filled_personal_information;
  

ALTER TABLE USERS_REGULATION_HISTORY
  RENAME COLUMN wc_approval TO filled_personal_information;  

alter table
   users_regulation
drop column WC_COMMENT ; 

alter table
   USERS_REGULATION_HISTORY
drop  column WC_COMMENT;     
 --run trigger:
  @after_regulation.trg
  
  --------------------------------------------------------------------------------------------
--Crypto_trade&Crypto_withdraw tables START
--------------------------------------------------------------------------------------------
`
delete from limits l where l.id not in(2,3,4);
--disable USERS_LIMIT_ID_FK in users

alter table limits add clearance_fee number default 0;

update limits ll
set ll.bank_wire_fee = 8.5;

update limits ll
set ll.clearance_fee = 5;

update limits ll
set ll.cc_fee = 3.5;

CREATE TABLE crypto_trade (
  id number NOT NULL,
  market varchar2(20) DEFAULT NULL,
  from_currency varchar2(10) DEFAULT NULL,
  to_currency varchar2(10) DEFAULT NULL,
  client_currency varchar2(10) DEFAULT NULL,
  quantity decimal(37,8) DEFAULT NULL,
  quantity_client decimal(37,8) DEFAULT NULL,
  amount NUMBER NOT NULL,
  original_amount NUMBER NOT NULL,
  fee decimal(37,2) DEFAULT NULL,
  clearance_fee decimal(37,2) DEFAULT NULL,
  rate decimal(37,8) DEFAULT NULL,
  rate_market_client decimal(37,6) DEFAULT NULL,
  rate_market decimal(37,8) DEFAULT NULL,
  status number(3) DEFAULT NULL,
  transaction_id number(11) DEFAULT NULL,
  uuid varchar2(100) DEFAULT NULL,
  comments varchar2(1000) DEFAULT NULL,
  time_created timestamp(3) DEFAULT CURRENT_TIMESTAMP(3) NOT NULL,
  time_opened timestamp(3) DEFAULT NULL,
  time_done timestamp(3) DEFAULT NULL,
  wallet varchar2(255) DEFAULT NULL,
  user_id number not null,
  CONSTRAINT crypto_trade_id_pk PRIMARY KEY (id)
);


grant select, update, insert on ETRADER.crypto_trade to ETRADER_WEB_CONNECT;
create public synonym crypto_trade for ETRADER.crypto_trade;

COMMENT ON COLUMN crypto_trade.market IS 'Example: "USD-BTC".';
COMMENT ON COLUMN crypto_trade.from_currency IS 'USD, EUR, GBP';
COMMENT ON COLUMN crypto_trade.to_currency IS 'Bitcoins (BTC), Ethereum (ETH)';
COMMENT ON COLUMN crypto_trade.client_currency IS 'The fiat currency of the client.';
COMMENT ON COLUMN crypto_trade.quantity IS 'The crypto quantity based on the actual rate, taken from the BITTREX.';
COMMENT ON COLUMN crypto_trade.quantity_client IS 'The crypto quantity requested by the client.';
COMMENT ON COLUMN crypto_trade.amount IS 'The amount in USD converted from the client-s original currency according to the "Rate" column.';
COMMENT ON COLUMN crypto_trade.original_amount IS 'The original amount in the client-s original currency.';
COMMENT ON COLUMN crypto_trade.fee IS 'The fee in client-s currency charged by Invest.';
COMMENT ON COLUMN crypto_trade.clearance_fee IS 'Clearance fee.';
COMMENT ON COLUMN crypto_trade.rate IS 'The rate "USD-to-the-client-fiat-currency" from the database. It is periodically feeded from the BITTREX.';
COMMENT ON COLUMN crypto_trade.rate_market_client IS 'The rate "Crypto-to-Fiat-currency" from the client screen.';
COMMENT ON COLUMN crypto_trade.rate_market IS 'The rate "Crypto-to-Fiat-currency" from the BITTREX.';

COMMENT ON COLUMN crypto_trade.status IS 'The status of the transaction. Refers the table crypto_trade_statuses.';
COMMENT ON COLUMN crypto_trade.transaction_id IS 'The Transaction_Id from our MySQL database.';
COMMENT ON COLUMN crypto_trade.uuid IS 'Uuid fom BITTREX.';
COMMENT ON COLUMN crypto_trade.wallet IS 'Wallet Id - it is a hash value.';


CREATE SEQUENCE crypto_trade_seq START WITH 1;

create public synonym crypto_trade_seq for ETRADER.crypto_trade_seq;

CREATE TABLE crypto_withdraw (
  id number(11) NOT NULL,
  currency varchar2(10) DEFAULT NULL,
  quantity decimal(37,8) DEFAULT NULL,
  wallet varchar2(255) DEFAULT NULL,
  transaction_id number(11) DEFAULT NULL,
  crypt_trade_id varchar2(255) DEFAULT NULL,
  status number(3) DEFAULT NULL,
  uuid varchar2(100) DEFAULT NULL,
  comments varchar2(1000) DEFAULT NULL,
  email_notified number(1) DEFAULT '0' NOT NULL,
  time_created timestamp(3) DEFAULT CURRENT_TIMESTAMP(3) NOT NULL,
  time_updated timestamp(3) DEFAULT NULL,
  CONSTRAINT crypto_withdraw_id_pk PRIMARY KEY (id)
);

grant select, update, insert on ETRADER.crypto_withdraw to ETRADER_WEB_CONNECT;
create public synonym crypto_withdraw for ETRADER.crypto_withdraw;

COMMENT ON COLUMN crypto_withdraw.currency IS 'Fiat currency.';
COMMENT ON COLUMN crypto_withdraw.quantity IS 'The quantity based on the actual rate, taken from the BITTREX.';
COMMENT ON COLUMN crypto_withdraw.wallet IS 'Wallet Id - it is a hash value.';
COMMENT ON COLUMN crypto_withdraw.transaction_id IS 'The Transaction_Id from our MySQL database.';
COMMENT ON COLUMN crypto_withdraw.crypt_trade_id IS 'Bittrex id.';
COMMENT ON COLUMN crypto_withdraw.status IS 'The status of the transaction. Refers the table crypto_withdraw_statuses.';
COMMENT ON COLUMN crypto_withdraw.uuid IS 'Uuid fom BITTREX.';


CREATE SEQUENCE crypto_withdraw_seq START WITH 1;
create public synonym crypto_withdraw_seq for ETRADER.crypto_withdraw_seq;

create sequence SEQ_CRYPTO_WITHDRAW
minvalue 1
maxvalue 9999999999999999999999999999
start with 1
increment by 1;
create public synonym SEQ_CRYPTO_WITHDRAW for ETRADER.SEQ_CRYPTO_WITHDRAW;

alter table transactions add time_updated date;
alter table gt_transactions add time_updated date;

@pkg_transactions


--------------------------------------------------------------------------------------------
--Crypto_trade&Crypto_withdraw tables END
--------------------------------------------------------------------------------------------

-------------------------------------------------------------------------------------------------------------------------------------------
-- NI-658 [BE] Crypto Invest Ph2 Transaction tab
-- Kiril
-------------------------------------------------------------------------------------------------------------------------------------------
ALTER TABLE TRANSACTIONS 
ADD (INIT_AMOUNT NUMBER DEFAULT 0 );

ALTER TABLE TRANSACTIONS 
ADD (ASSIGNEE VARCHAR2(50) DEFAULT NULL );

ALTER TABLE gt_transactions 
ADD (INIT_AMOUNT NUMBER DEFAULT 0 );

ALTER TABLE gt_transactions 
ADD (ASSIGNEE VARCHAR2(50) DEFAULT NULL );

CREATE TABLE CRYPTO_TRANSACTION_RESOLUTIONS 
(
  TRANSACTION_ID NUMBER NOT NULL 
, ASSIGNEE VARCHAR2(50) NOT NULL 
, STATUS NUMBER 
, WRITER_COMMENT VARCHAR2(100) 
, WRITER_ID NUMBER 
, TIME_CREATED DATE 
, TIME_UPDATED DATE 
, CONSTRAINT CRYPTO_TRANSACTION_RESOLUT_PK PRIMARY KEY 
  (
    TRANSACTION_ID 
  , ASSIGNEE 
  )
  ENABLE 
);

ALTER TABLE CRYPTO_TRANSACTION_RESOLUTIONS
ADD CONSTRAINT FK_TRANSACTIONS_CRYPTO_T_R FOREIGN KEY
(
  TRANSACTION_ID 
)
REFERENCES TRANSACTIONS
(
  ID 
)
ENABLE;

ALTER TABLE CRYPTO_TRANSACTION_RESOLUTIONS
ADD CONSTRAINT FK_WRITERS_CRYPTO_T_R FOREIGN KEY
(
  WRITER_ID 
)
REFERENCES WRITERS
(
  ID 
)
ENABLE;

ALTER TABLE CRYPTO_TRANSACTION_RESOLUTIONS 
ADD (REJECT_REASON_ID NUMBER );

grant select, update, insert on CRYPTO_TRANSACTION_RESOLUTIONS to ETRADER_WEB_CONNECT;
create public synonym CRYPTO_TRANSACTION_RESOLUTIONS for ETRADER.CRYPTO_TRANSACTION_RESOLUTIONS;

insert into writer_screen_permissions (id, spacename, screenname, permission, permorder)
    values (WRITER_SCREEN_PERMISSIONS_SEQ.nextval, 'Transaction', 'cryptoTransactions', 'view', 10);
insert into writer_screen_permissions (id, spacename, screenname, permission, permorder)
    values (WRITER_SCREEN_PERMISSIONS_SEQ.nextval, 'Transaction', 'cryptoTransactions', 'finance', 20);
insert into writer_screen_permissions (id, spacename, screenname, permission, permorder)
    values (WRITER_SCREEN_PERMISSIONS_SEQ.nextval, 'Transaction', 'cryptoTransactions', 'support', 30);
insert into writer_screen_permissions (id, spacename, screenname, permission, permorder)
    values (WRITER_SCREEN_PERMISSIONS_SEQ.nextval, 'Transaction', 'cryptoTransactions', 'risk', 40);
insert into writer_screen_permissions (id, spacename, screenname, permission, permorder)
    values (WRITER_SCREEN_PERMISSIONS_SEQ.nextval, 'Transaction', 'cryptoTransactions', 'withdraw', 50);
    
ALTER TABLE FILES 
ADD (TRANSACTION_ID NUMBER );

ALTER TABLE FILES
ADD CONSTRAINT FK_TRANSACTIONS_FILES FOREIGN KEY
(
  TRANSACTION_ID 
)
REFERENCES TRANSACTIONS
(
  ID 
)
ENABLE;
-------------------------------------------------------------------------------------------------------------------------------------------
-- End
-------------------------------------------------------------------------------------------------------------------------------------------

-------------------------------------------------------------------------------------------------------------------------------------------
-- send-support-email
-- Kiril
-------------------------------------------------------------------------------------------------------------------------------------------
CREATE TABLE SUPPORT_EMAILS 
(
  ID NUMBER NOT NULL 
, USER_ID NUMBER 
, FULL_NAME VARCHAR2(45) 
, EMAIL VARCHAR2(50) 
, PHONE VARCHAR2(20) 
, SUBJECT VARCHAR2(40) 
, BODY VARCHAR2(4000) 
, TIME_CREATED DATE 
, CONSTRAINT SUPPORT_EMAILS_PK PRIMARY KEY 
  (
    ID 
  )
  ENABLE 
);

ALTER TABLE SUPPORT_EMAILS
ADD CONSTRAINT FK_USERS_SUPPORT_EMAILS FOREIGN KEY
(
  USER_ID 
)
REFERENCES USERS
(
  ID 
)
ENABLE;

CREATE PUBLIC SYNONYM SUPPORT_EMAILS FOR ETRADER.SUPPORT_EMAILS;
GRANT SELECT, INSERT, UPDATE ON SUPPORT_EMAILS TO ETRADER_WEB_CONNECT;

CREATE SEQUENCE SEQ_SUPPORT_EMAILS;
CREATE PUBLIC SYNONYM SEQ_SUPPORT_EMAILS FOR SEQ_SUPPORT_EMAILS;
-------------------------------------------------------------------------------------------------------------------------------------------
-- End
-------------------------------------------------------------------------------------------------------------------------------------------

-------------------------------------------------------------------------------------------------------------------------------------------
-- BE Screens Crypto deposit & Crypto leads
-------------------------------------------------------------------------------------------------------------------------------------------

insert into writer_screen_permissions
  (id, spacename, screenname, permission, permorder)
values
  (WRITER_SCREEN_PERMISSIONS_SEQ.nextval,
   'Transaction',
   'cryptoDeposits',
   'view',
   10);
   
   
CREATE OR REPLACE TYPE string_table AS TABLE OF VARCHAR2(100);

insert into writer_screen_permissions
  (id, spacename, screenname, permission, permorder)
values
  (WRITER_SCREEN_PERMISSIONS_SEQ.nextval,
   'Users',
   'cryptoLeads',
   'view',
   10);
-------------------------------------------------------------------------------------------------------------------------------------------
-- END
-------------------------------------------------------------------------------------------------------------------------------------------
 
-------------------------------------------------------------------------------------------------------------------------------------------
-- ni-763-card-pay
-- Kiril
-------------------------------------------------------------------------------------------------------------------------------------------
insert into clearing_providers (id, provider_class, url, user_name, password, name, is_active, cc_available, is_3d_available, terminal_id)
  values (70, 'com.anyoption.common.clearing.CardPayClearingProvider', 'https://sandbox.cardpay.com/MI/cardpayment.html', 13473, '1uj76eNSMqG3', 'CardPay', 1, 1, 1, 0);

update clearing_providers set is_active = 0 where id <> 70;

insert into transaction_types (id, code, description, class_type, is_active) values (70, 'TRANS_TYPE_CARDPAY_DEPOSIT', 'transaction.cardpay.iframe.deposit', 1, 1);

@pkg_userfiles
-------------------------------------------------------------------------------------------------------------------------------------------
-- End
-------------------------------------------------------------------------------------------------------------------------------------------

-------------------------------------------------------------------------------------------------------------------------------------------
-- BE Screens User Documents
-------------------------------------------------------------------------------------------------------------------------------------------

insert into writer_screen_permissions
  (id, spacename, screenname, permission, permorder)
values
  (WRITER_SCREEN_PERMISSIONS_SEQ.nextval,
   'Documents',
   'userDocuments',
   'view',
   10);
   
 @pkg_user_documents.pck
 
 
insert into writer_screen_permissions
  (id, spacename, screenname, permission, permorder)
values
  (WRITER_SCREEN_PERMISSIONS_SEQ.nextval,
   'Documents',
   'userDocuments',
   'regulationApprove',
   20);
   
insert into writer_screen_permissions
  (id, spacename, screenname, permission, permorder)
values
  (WRITER_SCREEN_PERMISSIONS_SEQ.nextval,
   'Documents',
   'userDocuments',
   'regulationApproveManager',
   30);
 
-------------------------------------------------------------------------------------------------------------------------------------------
-- End
-------------------------------------------------------------------------------------------------------------------------------------------

-------------------------------------------------------------------------------------------------------------------------------------------
-- send-grid-mail
-- Kiril
-------------------------------------------------------------------------------------------------------------------------------------------
CREATE TABLE EMAIL_TEMPLATE_TYPES 
(
  ID NUMBER(38) NOT NULL 
, PROVIDER_ID VARCHAR2(50) NOT NULL 
, CODE VARCHAR2(30) NOT NULL 
, DESCRIPTION VARCHAR2(50) 
, CONSTRAINT EMAIL_TEMPLATE_TYPES_PK PRIMARY KEY 
  (
    ID 
  )
  ENABLE 
);

CREATE TABLE EMAIL_TEMPLATES 
(
  ID NUMBER(38) NOT NULL 
, USER_ID NUMBER(38) NOT NULL 
, TYPE_ID NUMBER(38) NOT NULL 
, PARAMS VARCHAR2(2000) 
, TIME_CREATED DATE NOT NULL 
, TIME_SENT DATE 
, SUCCESS NUMBER 
, CONSTRAINT EMAIL_TEMPLATES_PK PRIMARY KEY 
  (
    ID 
  )
  ENABLE 
);

ALTER TABLE EMAIL_TEMPLATES
ADD CONSTRAINT FK_EMAIL_TEMPLATE_TYPES FOREIGN KEY
(
  TYPE_ID 
)
REFERENCES EMAIL_TEMPLATE_TYPES
(
  ID 
)
ENABLE;

ALTER TABLE EMAIL_TEMPLATES
ADD CONSTRAINT FK_USERS_EMAIL_TEMPLATES FOREIGN KEY
(
  USER_ID 
)
REFERENCES USERS
(
  ID 
)
ENABLE;

ALTER TABLE EMAIL_TEMPLATES 
ADD (EMAIL_FROM VARCHAR2(50) );

ALTER TABLE EMAIL_TEMPLATES 
ADD (EMAIL_TO VARCHAR2(50) );

ALTER TABLE EMAIL_TEMPLATES 
ADD (SUBJECT VARCHAR2(100) );

ALTER TABLE EMAIL_TEMPLATES 
ADD (PRIORITY NUMBER );

CREATE PUBLIC SYNONYM EMAIL_TEMPLATE_TYPES FOR ETRADER.EMAIL_TEMPLATE_TYPES;
GRANT SELECT, INSERT, UPDATE ON EMAIL_TEMPLATE_TYPES TO ETRADER_WEB_CONNECT;

CREATE PUBLIC SYNONYM EMAIL_TEMPLATES FOR ETRADER.EMAIL_TEMPLATES;
GRANT SELECT, INSERT, UPDATE ON EMAIL_TEMPLATES TO ETRADER_WEB_CONNECT;

CREATE SEQUENCE SEQ_EMAIL_TEMPLATES;
CREATE PUBLIC SYNONYM SEQ_EMAIL_TEMPLATES FOR SEQ_EMAIL_TEMPLATES;

insert into jobs (id, running, server, run_interval, job_class, config, run_servers, run_applications) 
  values (2, 1, 'WW01BG', 1, 'com.invest.common.email.SendMailJob', 'batch_processing_limit=100', 'X_XWW01BG', 'X_Xbackend');
  
insert into jobs (id, running, server, run_interval, description, job_class, run_servers, run_applications) 
  values (1, 1, 'WW01BG', 10000000, 'Send automatically mail templates to email and inbox', 'il.co.etrader.jobs.AutomaticMailTemplatesJob', 'X_XWW01BG', 'X_Xanyoption');

CREATE TABLE EMAIL_PROVIDERS 
(
  ID NUMBER NOT NULL 
, PROVIDER_CLASS VARCHAR2(100) NOT NULL 
, NAME VARCHAR2(50) NOT NULL 
, API_KEY VARCHAR2(100) 
, CONSTRAINT EMAIL_PROVIDERS_PK PRIMARY KEY 
  (
    ID 
  )
  ENABLE 
);

CREATE PUBLIC SYNONYM EMAIL_PROVIDERS FOR ETRADER.EMAIL_PROVIDERS;
GRANT SELECT, INSERT, UPDATE ON EMAIL_PROVIDERS TO ETRADER_WEB_CONNECT;

insert into email_providers (id, provider_class, name, api_key) 
  values (1, 'com.invest.common.email.providers.SendGridProviderSender', 'SendGrid', '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');

ALTER TABLE EMAIL_TEMPLATE_TYPES  
MODIFY (CODE VARCHAR2(50 BYTE) );

insert into email_template_types (id, provider_id, code) values (1, 'b3ded273-369c-41e4-9c4b-e610fc6c15c4', 'TEMPLATE_TYPE_VERIFICATION');
insert into email_template_types (id, provider_id, code) values (2, '0c5449f2-a8fe-4ac4-8b52-748b23eae5bf', 'TEMPLATE_TYPE_FORGOT_PASSWORD');
insert into email_template_types (id, provider_id, code) values (3, '131b613c-e543-4ada-a154-b7dbf4572f18', 'TEMPLATE_TYPE_FORGOT_PASSWORD_GENERAL');
insert into email_template_types (id, provider_id, code) values (4, '1d004d08-1317-4a24-98f0-57a01a12d613', 'TEMPLATE_TYPE_EDIT_PROFILE');
insert into email_template_types (id, provider_id, code) values (5, '0d1ae066-4d06-4ad2-9d06-6b3753e61476', 'TEMPLATE_TYPE_EDIT_PROFILE_GENERAL');
insert into email_template_types (id, provider_id, code) values (6, '3e431382-23f6-4d2d-8a6c-074d1ba5398e', 'TEMPLATE_TYPE_BANK_DETAILS');
insert into email_template_types (id, provider_id, code) values (7, '752b693a-7340-4db0-8b11-a4383db59eaa', 'TEMPLATE_TYPE_CC_TRANSACTION_APPROVED');
insert into email_template_types (id, provider_id, code) values (8, '3a987443-6ab1-47a9-85ca-a77d9b3b203a', 'TEMPLATE_TYPE_CC_TRANSACTION_FAILED');
insert into email_template_types (id, provider_id, code) values (9, 'a5fc66db-7947-4b6e-bb3b-9eb5e5899c5d', 'TEMPLATE_TYPE_WIRE_TRANSACTION_APPROVED');
insert into email_template_types (id, provider_id, code) values (10, 'b391bd88-3263-4ac6-baef-119756e915a7', 'TEMPLATE_TYPE_WIRE_TRANSACTION_FAILED');
insert into email_template_types (id, provider_id, code) values (11, '803dd3eb-f979-47fb-ac6e-4f5c2d143e8b', 'TEMPLATE_TYPE_CRYPTO_SENT');
insert into email_template_types (id, provider_id, code) values (12, '77144df1-a316-44b9-bb78-11d8002c0b16', 'TEMPLATE_TYPE_DOCUMENT_REJECTED');
insert into email_template_types (id, provider_id, code) values (13, '26773221-602c-4093-ba57-be3f89fa3904', 'TEMPLATE_TYPE_DOCUMENTS_APPROVED');
insert into email_template_types (id, provider_id, code) values (14, '68af68ba-5716-4c99-9401-53f6d2882f6c', 'TEMPLATE_TYPE_DOCUMENTS_RECEIVED');
-------------------------------------------------------------------------------------------------------------------------------------------
-- End
-------------------------------------------------------------------------------------------------------------------------------------------

-------------------------------------------------------------------------------------------------------------------------------------------
-- Users Documents screen
-- Pavel
-------------------------------------------------------------------------------------------------------------------------------------------
@pkg_transactions_backend
-------------------------------------------------------------------------------------------------------------------------------------------
-- End
-------------------------------------------------------------------------------------------------------------------------------------------


-------------------------------------------------------------------------------------------------------------------------------------------
-- Currencies Rates to Base Amount (EUR)
-- Pavel
-------------------------------------------------------------------------------------------------------------------------------------------
CREATE TABLE currencies_rates_to_eur
(
  currency_code   VARCHAR2(3 CHAR) not null,
  rate            NUMBER,
  dt_created DATE
);

grant SELECT, UPDATE, INSERT ON ETRADER.currencies_rates_to_eur to ETRADER_WEB_CONNECT;
create public synonym currencies_rates_to_eur for ETRADER.currencies_rates_to_eur;

INSERT INTO currencies_rates_to_eur
  (currency_code, rate, dt_created)
VALUES
  ('USD', 1.1737, SYSDATE);

INSERT INTO currencies_rates_to_eur
  (currency_code, rate, dt_created)
VALUES
  ('GBP', 0.87673, SYSDATE);
  
INSERT INTO currencies_rates_to_eur
  (currency_code, rate, dt_created)
VALUES
  ('ILS', 4.1836, SYSDATE);

INSERT INTO currencies_rates_to_eur
  (currency_code, rate, dt_created)
VALUES
  ('TRY', 5.4193, SYSDATE);

INSERT INTO currencies_rates_to_eur
  (currency_code, rate, dt_created)
VALUES
  ('RUB', 72.6626, SYSDATE);

INSERT INTO currencies_rates_to_eur
  (currency_code, rate, dt_created)
VALUES
  ('CNY', 7.5166, SYSDATE);

INSERT INTO currencies_rates_to_eur
  (currency_code, rate, dt_created)
VALUES
  ('KRW', 1254.88, SYSDATE);

INSERT INTO currencies_rates_to_eur
  (currency_code, rate, dt_created)
VALUES
  ('SEK', 10.2583, SYSDATE);

INSERT INTO currencies_rates_to_eur
  (currency_code, rate, dt_created)
VALUES
  ('AUD', 1.5311, SYSDATE);

INSERT INTO currencies_rates_to_eur
  (currency_code, rate, dt_created)
VALUES
  ('ZAR', 14.7053, SYSDATE);

INSERT INTO currencies_rates_to_eur
  (currency_code, rate, dt_created)
VALUES
  ('CZK', 25.696, SYSDATE);

INSERT INTO currencies_rates_to_eur
  (currency_code, rate, dt_created)
VALUES
  ('PLN', 4.2848, SYSDATE);

CREATE TABLE currencies_rates_to_eur_hist
(
  currency_code   VARCHAR2(3 CHAR) NOT null,
  rate          NUMBER,
  dt_created_origin DATE,
  dt_created DATE
);

grant SELECT, UPDATE, INSERT ON ETRADER.currencies_rates_to_eur_hist to ETRADER_WEB_CONNECT;
CREATE public synonym currencies_rates_to_eur_hist for ETRADER.currencies_rates_to_eur_hist;

@convert_amount_to_eur_ecb.func

grant EXECUTE ON ETRADER.convert_amount_to_eur_ecb to ETRADER_WEB_CONNECT;
CREATE public synonym convert_amount_to_eur_ecb for ETRADER.convert_amount_to_eur_ecb;

INSERT INTO jobs (id, running, server, run_interval, job_class, run_servers, run_applications) 
  VALUES (3, 1, '?????', 1440, 'com.invest.common.currencyrates.CurrencyRatesECBJob', '???????', 'jsonService');
-------------------------------------------------------------------------------------------------------------------------------------------
-- End
-------------------------------------------------------------------------------------------------------------------------------------------

-------------------------------------------------------------------------------------------------------------------------------------------
-- Add miner fee (Bittrex commission for withdraw)
-------------------------------------------------------------------------------------------------------------------------------------------
  
CREATE TABLE bittrex_commissions (
  id number(11) NOT NULL,
  crypto_currency varchar2(20) DEFAULT NULL,
  commission decimal(37,8) DEFAULT NULL,
  time_created timestamp(3) DEFAULT CURRENT_TIMESTAMP(3) NOT NULL,
  CONSTRAINT bittrex_commissions_id_pk PRIMARY KEY (id)
);

grant select, update, insert on ETRADER.bittrex_commissions to ETRADER_WEB_CONNECT;
create public synonym bittrex_commissions for ETRADER.bittrex_commissions;

ALTER TABLE crypto_trade ADD bittrex_commission decimal(37,8) DEFAULT NULL;
 
insert into bittrex_commissions
  (id, crypto_currency, commission)
values
  (1, 'ETH', 0.006);

insert into bittrex_commissions
  (id, crypto_currency, commission)
values
  (2, 'BTC', 0.0005);

ALTER TABLE crypto_withdraw ADD bittrex_commission decimal(37,8);

-------------------------------------------------------------------------------------------------------------------------------------------
-- END Add miner fee (Bittrex commission for withdraw)
-------------------------------------------------------------------------------------------------------------------------------------------

-------------------------------------------------------------------------------------------------------------------------------------------
-- Add withdrawal history functionality
-------------------------------------------------------------------------------------------------------------------------------------------
-- consider that in MySQL is auto_incremented while in Oracle is sequence, we use same buy_order sar!!!
CREATE TABLE crypto_withdrawal_history (
  id number(11) NOT NULL,
  payment_uuid varchar(100) DEFAULT NULL,
  currency varchar(10) DEFAULT NULL,
  amount decimal(37,8) DEFAULT NULL,
  address varchar(255) DEFAULT NULL,
  opened timestamp(3) DEFAULT NULL,
  authorized number(1) DEFAULT NULL,
  pending_payment number(1) DEFAULT NULL,
  tx_cost decimal(37,8) DEFAULT NULL,
  tx_id varchar(100) DEFAULT NULL,
  canceled number(1) DEFAULT NULL,
  invalid_address number(1) DEFAULT NULL,
  time_created timestamp(3) DEFAULT CURRENT_TIMESTAMP(3),
  CONSTRAINT crypto_with_hist_id_pk PRIMARY KEY (id)
);

CREATE SEQUENCE crypto_withdrawal_history_seq START WITH 1;

create public synonym crypto_withdrawal_history_seq for ETRADER.crypto_withdrawal_history_seq;

grant select, update, insert on ETRADER.crypto_withdrawal_history to ETRADER_WEB_CONNECT;
create public synonym crypto_withdrawal_history for ETRADER.crypto_withdrawal_history;

-------------------------------------------------------------------------------------------------------------------------------------------
-- END Add withdrawal history functionality
-------------------------------------------------------------------------------------------------------------------------------------------

-------------------------------------------------------------------------------------------------------------------------------------------
-- Skip buy limit functionality
-------------------------------------------------------------------------------------------------------------------------------------------
create table CRYPTO_CONFIG
(
  config_key   VARCHAR2(64) not null,
  config_value VARCHAR2(255) not null
)
tablespace USERS
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
-- Create/Recreate primary, unique and foreign key constraints 
alter table CRYPTO_CONFIG
  add constraint CRYPTO_CONFIG_PK primary key (CONFIG_KEY)
  using index 
  tablespace USERS
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
-- Grant/Revoke object privileges 
grant select, insert, update on CRYPTO_CONFIG to ETRADER_WEB_CONNECT;
create public synonym CRYPTO_CONFIG for ETRADER.CRYPTO_CONFIG;

insert into crypto_config
  (config_key, config_value)
values
  ('SKIP_BITREX_CONFIRMATION', 1);
  
  insert into crypto_config
  (config_key, config_value)
values
  ('API_KEY', 'c8406539054f48fbad6908940f4557e2');
  
  insert into crypto_config
  (config_key, config_value)
values
  ('API_SECRET', '577ff778509746918436ee10b4e8ae1f');
  
  insert into crypto_config
  (config_key, config_value)
values
  ('ENVIRONMENT', 'test');
  
  insert into crypto_config
  (config_key, config_value)
values
  ('MANUAL_MODE', 0);
  
  insert into crypto_config
  (config_key, config_value)
values
  ('SCHEDULED_PERIOD', 60);
  
  insert into crypto_config
  (config_key, config_value)
values
  ('TASK_COUNT', 1);
  
    insert into crypto_config
  (config_key, config_value)
values
  ('TICKER_HASH_INTERVAL', 30000);
  
    insert into crypto_config
  (config_key, config_value)
values
  ('URL', 'http://10.32.0.85:8080/bittrex_comm_test/test/api');
  
  insert into writer_screen_permissions
  (id, spacename, screenname, permission, permorder)
values
  (WRITER_SCREEN_PERMISSIONS_SEQ.nextval,
   'Users',
   'cryptoLeads',
   'buyLimitSkip',
   20);
   
-------------------------------------------------------------------------------------------------------------------------------------------
-- Disable Triggers
-------------------------------------------------------------------------------------------------------------------------------------------
  --!!!DISABLE the next Triggers:
  before_ins_users;
  PREVENT_LOGIN;
  before_insert_trans;
  
  
  
-------------------------------------------------------------------------------------------------------------------------------------------
-- END Skip buy limit functionality
-------------------------------------------------------------------------------------------------------------------------------------------

-------------------------------------------------------------------------------------------------------------------------------------------
-- ni-631-shutdown-to-website
-- Kiril
-------------------------------------------------------------------------------------------------------------------------------------------
insert into crypto_config (config_key, config_value) values ('DEPOSIT_DISABLED', 0);
insert into writer_screen_permissions (id, spacename, screenname, permission, permorder)
    values (WRITER_SCREEN_PERMISSIONS_SEQ.nextval, 'Transaction', 'cryptoTransactions', 'disableDeposit', 60);
-------------------------------------------------------------------------------------------------------------------------------------------
-- End
-------------------------------------------------------------------------------------------------------------------------------------------

-------------------------------------------------------------------------------------------------------------------------------------------
-- ni-1283-reject-resons
-- Kiril
-------------------------------------------------------------------------------------------------------------------------------------------
delete from file_type_reject_reasons where file_type_id = 4 and reject_reason_id in (18,19,20,21,22,23,24,25,26,44);
insert into file_type_reject_reasons (file_type_id, reject_reason_id) values (4, 1);
insert into file_type_reject_reasons (file_type_id, reject_reason_id) values (4, 2);
insert into file_type_reject_reasons (file_type_id, reject_reason_id) values (4, 28);
insert into file_type_reject_reasons (file_type_id, reject_reason_id) values (35, 0);
insert into file_type_reject_reasons (file_type_id, reject_reason_id) values (35, 1);
insert into file_type_reject_reasons (file_type_id, reject_reason_id) values (35, 2);
insert into file_type_reject_reasons (file_type_id, reject_reason_id) values (35, 5);
insert into file_type_reject_reasons (file_type_id, reject_reason_id) values (35, 28);
update file_reject_reasons set enabled = 1 where id = 2;

insert into file_reject_reasons (id, reason_name, enabled, reason_mail_param) values (49, 'file.rejected.third.party.deposit', 1, 'file.rejected.third.party.deposit');
insert into file_reject_reasons (id, reason_name, enabled, reason_mail_param) values (50, 'file.rejected.min.deposit', 1, 'file.rejected.min.deposit');
insert into file_reject_reasons (id, reason_name, enabled, reason_mail_param) values (51, 'file.rejected.max.daily.deposit', 1, 'file.rejected.max.daily.deposit');
insert into file_reject_reasons (id, reason_name, enabled, reason_mail_param) values (52, 'file.rejected.max.monthly.deposit', 1, 'file.rejected.max.monthly.deposit');

update file_type_reject_reasons set reject_reason_id = 49 where file_type_id = 4 and reject_reason_id = 28;
update file_type_reject_reasons set reject_reason_id = 50 where file_type_id = 4 and reject_reason_id = 1;
update file_type_reject_reasons set reject_reason_id = 51 where file_type_id = 4 and reject_reason_id = 2;
update file_type_reject_reasons set reject_reason_id = 52 where file_type_id = 4 and reject_reason_id = 5;
update file_type_reject_reasons set reject_reason_id = 49 where file_type_id = 35 and reject_reason_id = 28;
update file_type_reject_reasons set reject_reason_id = 50 where file_type_id = 35 and reject_reason_id = 1;
update file_type_reject_reasons set reject_reason_id = 51 where file_type_id = 35 and reject_reason_id = 2;
update file_type_reject_reasons set reject_reason_id = 52 where file_type_id = 35 and reject_reason_id = 5;
-------------------------------------------------------------------------------------------------------------------------------------------
-- End
-------------------------------------------------------------------------------------------------------------------------------------------

-------------------------------------------------------------------------------------------------------------------------------------------
-- NI-1345-Update deposit limits
-- Metodi
-------------------------------------------------------------------------------------------------------------------------------------------
ALTER TABLE limits
ADD (max_bank_wire_per_day number DEFAULT 0 );

ALTER TABLE limits
ADD (max_bank_wire_per_month number DEFAULT 0 );


UPDATE LIMITS SET min_deposit=25000, max_deposit=1000000, max_deposit_per_day=1000000, max_deposit_per_month=2000000, min_bank_wire=25000, max_bank_wire=2000000, max_bank_wire_per_day=2000000, max_bank_wire_per_month=5000000 
where id=2;
commit;

UPDATE LIMITS SET min_deposit=25000, max_deposit=800000, max_deposit_per_day=800000, max_deposit_per_month=1500000, min_bank_wire=25000, max_bank_wire=1500000, max_bank_wire_per_day=1500000, max_bank_wire_per_month=4000000
where id=3;
commit;


UPDATE LIMITS SET min_deposit=25000, max_deposit=700000, max_deposit_per_day=700000, max_deposit_per_month=1400000, min_bank_wire=25000, max_bank_wire=1400000, max_bank_wire_per_day=1400000, max_bank_wire_per_month=3500000 
where id=4;
commit;
-------------------------------------------------------------------------------------------------------------------------------------------
-- End
-------------------------------------------------------------------------------------------------------------------------------------------

-------------------------------------------------------------------------------------------------------------------------------------------
-- Missed services development
-- Kiril
-------------------------------------------------------------------------------------------------------------------------------------------
insert into crypto_config (config_key, config_value) values ('WITHDRAWAL_HISTORY_SCHEDULED_PERIOD', 60);
CREATE SEQUENCE SEQ_CRYPTO_WITHDRAWAL_HISTORY;
CREATE PUBLIC SYNONYM SEQ_CRYPTO_WITHDRAWAL_HISTORY FOR SEQ_CRYPTO_WITHDRAWAL_HISTORY;
-------------------------------------------------------------------------------------------------------------------------------------------
-- End
-------------------------------------------------------------------------------------------------------------------------------------------

-------------------------------------------------------------------------------------------------------------------------------------------
-- NI-1444-Cannot reject first deposit transaction
-- Metodi
-------------------------------------------------------------------------------------------------------------------------------------------

exec dbms_aqadm.start_queue(queue_name => 'Q_FIRST_DEPOSIT');

-------------------------------------------------------------------------------------------------------------------------------------------
-- End
-------------------------------------------------------------------------------------------------------------------------------------------

-------------------------------------------------------------------------------------------------------------------------------------------
-- NI-1259 TO BE DONE BEFORE GO LIVE
-- Kiril
-------------------------------------------------------------------------------------------------------------------------------------------
delete from limits l where l.id in(2,4);
--Insert into ETRADER.LIMITS (ID,NAME,MIN_DEPOSIT,MIN_WITHDRAW,MAX_DEPOSIT,MAX_WITHDRAW,MAX_DEPOSIT_PER_DAY,MAX_DEPOSIT_PER_MONTH,TIME_LAST_UPDATE,WRITER_ID,COMMENTS,TIME_CREATED,MIN_BANK_WIRE,FREE_INVEST_MIN,MAX_BANK_WIRE,FREE_INVEST_MAX,BANK_WIRE_FEE,CC_FEE,CHEQUE_FEE,DEPOSIT_DOCS_LIMIT1,DEPOSIT_DOCS_LIMIT2,MIN_FIRST_DEPOSIT,PAYPAL_FEE,ENVOY_FEE,DEPOSIT_ALERT,AMT_FOR_LOW_WITHDRAW,CLEARANCE_FEE,MAX_BANK_WIRE_PER_DAY,MAX_BANK_WIRE_PER_MONTH) values (2,'basic dollar',25000,3000,1000000,99999999,1000000,2000000,to_date('20-APR-15 10:44:34','DD-MON-RR HH24:MI:SS'),1,null,to_date('20-APR-15 10:44:37','DD-MON-RR HH24:MI:SS'),25000,100000,2000000,300000,8.5,3.5,3000,200000,500000,20000,0,3000,2000,20000,5,2000000,5000000);
--Insert into ETRADER.LIMITS (ID,NAME,MIN_DEPOSIT,MIN_WITHDRAW,MAX_DEPOSIT,MAX_WITHDRAW,MAX_DEPOSIT_PER_DAY,MAX_DEPOSIT_PER_MONTH,TIME_LAST_UPDATE,WRITER_ID,COMMENTS,TIME_CREATED,MIN_BANK_WIRE,FREE_INVEST_MIN,MAX_BANK_WIRE,FREE_INVEST_MAX,BANK_WIRE_FEE,CC_FEE,CHEQUE_FEE,DEPOSIT_DOCS_LIMIT1,DEPOSIT_DOCS_LIMIT2,MIN_FIRST_DEPOSIT,PAYPAL_FEE,ENVOY_FEE,DEPOSIT_ALERT,AMT_FOR_LOW_WITHDRAW,CLEARANCE_FEE,MAX_BANK_WIRE_PER_DAY,MAX_BANK_WIRE_PER_MONTH) values (3,'basic euro',25000,2500,800000,99999999,800000,1500000,to_date('20-APR-15 10:40:49','DD-MON-RR HH24:MI:SS'),1,null,to_date('20-APR-15 10:40:53','DD-MON-RR HH24:MI:SS'),25000,100000,1500000,300000,8.5,3.5,2500,200000,500000,20000,0,2500,2000,20000,5,1500000,4000000);
--Insert into ETRADER.LIMITS (ID,NAME,MIN_DEPOSIT,MIN_WITHDRAW,MAX_DEPOSIT,MAX_WITHDRAW,MAX_DEPOSIT_PER_DAY,MAX_DEPOSIT_PER_MONTH,TIME_LAST_UPDATE,WRITER_ID,COMMENTS,TIME_CREATED,MIN_BANK_WIRE,FREE_INVEST_MIN,MAX_BANK_WIRE,FREE_INVEST_MAX,BANK_WIRE_FEE,CC_FEE,CHEQUE_FEE,DEPOSIT_DOCS_LIMIT1,DEPOSIT_DOCS_LIMIT2,MIN_FIRST_DEPOSIT,PAYPAL_FEE,ENVOY_FEE,DEPOSIT_ALERT,AMT_FOR_LOW_WITHDRAW,CLEARANCE_FEE,MAX_BANK_WIRE_PER_DAY,MAX_BANK_WIRE_PER_MONTH) values (4,'basic pound',25000,2000,700000,99999999,700000,1400000,to_date('06-JAN-09 13:23:29','DD-MON-RR HH24:MI:SS'),9,null,to_date('06-JAN-09 13:23:43','DD-MON-RR HH24:MI:SS'),25000,100000,1400000,300000,8.5,3.5,2000,200000,500000,20000,0,2000,1000,20000,5,1400000,3500000);
-------------------------------------------------------------------------------------------------------------------------------------------
-- End
-------------------------------------------------------------------------------------------------------------------------------------------

-------------------------------------------------------------------------------------------------------------------------------------------
-- NI-1438 Invalid objects in Dev and Staging
-- Metodi
-------------------------------------------------------------------------------------------------------------------------------------------

drop view ETRADER.OPT_USERS;
drop view ETRADER.OPT_USERS_REGULATION;
drop view ETRADER.VS_ACCOUNTS;
drop view ETRADER.VS_ACCOUNTS_NAMES;
-------------------------------------------------------------------------------------------------------------------------------------------
-- End
-------------------------------------------------------------------------------------------------------------------------------------------

