grant select, update, insert on ETRADER.LAST_LEVELS to ETRADER_WEB_CONNECT;
create public synonym LAST_LEVELS  for ETRADER.LAST_LEVELS;

grant select, update, insert on ETRADER.COUNTRIES to ETRADER_WEB_CONNECT;
create public synonym COUNTRIES for ETRADER.COUNTRIES;

grant select, update, insert on ETRADER.LANGUAGES to ETRADER_WEB_CONNECT;
create public synonym LANGUAGES for ETRADER.LANGUAGES;

grant select, update, insert on ETRADER.SKINS to ETRADER_WEB_CONNECT;
create public synonym SKINS for ETRADER.SKINS;

grant select, update, insert on ETRADER.ASSET_INDEX to ETRADER_WEB_CONNECT;
create public synonym ASSET_INDEX for ETRADER.ASSET_INDEX;

grant select, update, insert on ETRADER.MARKET_DISPLAY_GROUPS to ETRADER_WEB_CONNECT;
create public synonym MARKET_DISPLAY_GROUPS for ETRADER.MARKET_DISPLAY_GROUPS;

grant select, update, insert on ETRADER.MARKET_RATES to ETRADER_WEB_CONNECT;
create public synonym MARKET_RATES for ETRADER.MARKET_RATES;

create public synonym SEQ_MARKET_RATES for ETRADER.SEQ_MARKET_RATES;

grant select, update, insert on ETRADER.SKIN_CURRENCIES to ETRADER_WEB_CONNECT;
create public synonym SKIN_CURRENCIES for ETRADER.SKIN_CURRENCIES;

grant select, update, insert on ETRADER.SKIN_CURRENCY_LIMITS_MAP to ETRADER_WEB_CONNECT;
create public synonym SKIN_CURRENCY_LIMITS_MAP for ETRADER.SKIN_CURRENCY_LIMITS_MAP;

grant select, update, insert on ETRADER.SKIN_LANGUAGES to ETRADER_WEB_CONNECT;
create public synonym SKIN_LANGUAGES for ETRADER.SKIN_LANGUAGES;

grant select, update, insert on ETRADER.SKIN_MARKET_GROUPS to ETRADER_WEB_CONNECT;
create public synonym SKIN_MARKET_GROUPS for ETRADER.SKIN_MARKET_GROUPS;

grant select, update, insert on ETRADER.SKIN_MARKET_GROUP_MARKETS to ETRADER_WEB_CONNECT;
create public synonym SKIN_MARKET_GROUP_MARKETS for ETRADER.SKIN_MARKET_GROUP_MARKETS;

grant select, update, insert on ETRADER.SKIN_PAYMENT_METHODS to ETRADER_WEB_CONNECT;
create public synonym SKIN_PAYMENT_METHODS for ETRADER.SKIN_PAYMENT_METHODS;

grant select, update, insert on ETRADER.URLS to ETRADER_WEB_CONNECT;
create public synonym URLS for ETRADER.URLS;

grant select, update, insert on ETRADER.SKIN_URL_COUNTRY_MAP to ETRADER_WEB_CONNECT;
create public synonym SKIN_URL_COUNTRY_MAP for ETRADER.SKIN_URL_COUNTRY_MAP;

grant select, update, insert on ETRADER.STATES to ETRADER_WEB_CONNECT;
create public synonym STATES for ETRADER.STATES;

grant select, update, insert on ETRADER.WRITERS_SKIN to ETRADER_WEB_CONNECT;
create public synonym WRITERS_SKIN for ETRADER.WRITERS_SKIN;

create public synonym SEQ_WRITERS_SKIN for ETRADER.SEQ_WRITERS_SKIN;

grant select, update, insert on ETRADER.XOR_TERMINALS to ETRADER_WEB_CONNECT;
create public synonym XOR_TERMINALS for ETRADER.XOR_TERMINALS;


