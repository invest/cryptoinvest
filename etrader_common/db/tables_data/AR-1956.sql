
-- Insert into ISSUE_ACTION_TYPES
INSERT INTO ISSUE_ACTION_TYPES (
  ID ,
  NAME ,
  CHANNEL_ID ,
  REACHED_STATUS_ID ,
  TMP_REACTION_ACTIVITY_ID ,
  IS_ACTIVE ,
  DEPOSIT_SEARCH_TYPE ,
  IS_TRANSACTION_ISSUES_S_ACTION ,
  IS_TRACKING_ACTION ,
  IS_CLOSE_ACCOUNT
)
VALUES
(
  81,
  'issue.action.make.vip',
  7,
  '',
  '',
  1,
  0,
  0,
  0,
  0
);

INSERT INTO ISSUE_ACTION_TYPES (
  ID ,
  NAME ,
  CHANNEL_ID ,
  REACHED_STATUS_ID ,
  TMP_REACTION_ACTIVITY_ID ,
  IS_ACTIVE ,
  DEPOSIT_SEARCH_TYPE ,
  IS_TRANSACTION_ISSUES_S_ACTION ,
  IS_TRACKING_ACTION ,
  IS_CLOSE_ACCOUNT
)
VALUES
(
  82,
  'issue.action.revoke.vip',
  7,
  '',
  '',
  1,
  0,
  0,
  0,
  0
);

INSERT INTO ISSUE_ACTION_TYPES (
  ID ,
  NAME ,
  CHANNEL_ID ,
  REACHED_STATUS_ID ,
  TMP_REACTION_ACTIVITY_ID ,
  IS_ACTIVE ,
  DEPOSIT_SEARCH_TYPE ,
  IS_TRANSACTION_ISSUES_S_ACTION ,
  IS_TRACKING_ACTION ,
  IS_CLOSE_ACCOUNT
)
VALUES
(
  83,
  'issue.action.restrict.vip',
  7,
  '',
  '',
  1,
  0,
  0,
  0,
  0
);

-- Insert into ISSUE_ACTION_TYPES
INSERT INTO ISSUE_ACTION_TYPES (
  ID ,
  NAME ,
  CHANNEL_ID ,
  REACHED_STATUS_ID ,
  TMP_REACTION_ACTIVITY_ID ,
  IS_ACTIVE ,
  DEPOSIT_SEARCH_TYPE ,
  IS_TRANSACTION_ISSUES_S_ACTION ,
  IS_TRACKING_ACTION ,
  IS_CLOSE_ACCOUNT
)
VALUES
(
  84,
  'issue.action.unrestrict.vip',
  7,
  '',
  '',
  1,
  0,
  0,
  0,
  0
);

-- ISSUE_SUBJECTS
  INSERT
  INTO ISSUE_SUBJECTS
    (
      ID ,
      NAME ,
      IS_DISPLAYED ,
      REAL_NAME
    )
    VALUES
    (
      77,
      'issue.subject.vip',
      1	,
      'vip'
    );
    
  -- ISSUE_ACTION_ROLES  
  INSERT
  INTO ISSUE_ACTION_ROLES
    (
      ID ,
      ISSUE_ACTION_TYPE_ID ,
      ROLE_ID ,
      SCREEN_ID
    )
    VALUES
    (
      SEQ_ISSUE_ACTION_ROLES.nextval,
      81,
      8,
      1
    );
    
  INSERT
  INTO ISSUE_ACTION_ROLES
    (
      ID ,
      ISSUE_ACTION_TYPE_ID ,
      ROLE_ID ,
      SCREEN_ID
    )
    VALUES
    (
      SEQ_ISSUE_ACTION_ROLES.nextval,
      82,
      8,
      1
    );
    
  INSERT
  INTO ISSUE_ACTION_ROLES
    (
      ID ,
      ISSUE_ACTION_TYPE_ID ,
      ROLE_ID ,
      SCREEN_ID
    )
    VALUES
    (
      SEQ_ISSUE_ACTION_ROLES.nextval,
      83,
      8,
      1
    );
    
  INSERT
  INTO ISSUE_ACTION_ROLES
    (
      ID ,
      ISSUE_ACTION_TYPE_ID ,
      ROLE_ID ,
      SCREEN_ID
    )
    VALUES
    (
      SEQ_ISSUE_ACTION_ROLES.nextval,
      84,
      8,
      1
    );
