-- TODO fix formula constraint, formulas should be nullable
-- Binary markets
-- .DJI like indices
INSERT INTO MARKET_DATA_SOURCE_MAPS (
  ID
, MARKET_ID
, MARKET_DATA_SOURCE_ID
, HOUR_LEVEL_FORMULA_ID
, HOUR_LEVEL_FORMULA_PARAMS
, HOUR_CLOSING_FORMULA_ID
, HOUR_CLOSING_FORMULA_PARAMS
, DAY_CLOSING_FORMULA_ID
, DAY_CLOSING_FORMULA_PARAMS
, LONG_TERM_FORMULA_ID
, LONG_TERM_FORMULA_PARAMS
, REAL_LEVEL_FORMULA_ID
, REAL_LEVEL_FORMULA_PARAMS
, MARKET_PARAMS
, MARKET_DISABLE_CONDITIONS 
) VALUES (
SEQ_MARKET_DATA_SOURCE_MAPS.NEXTVAL
, 5
, 1
, 3
, '{"field1Name": "TRDPRC_1", "lastTradeTimeField": "TRDTIM_1", "subscriptionConfig": {".DJI":{"identifier":".DJI"}}}'
, 3
, '{"field1Name": "TRDPRC_1", "lastTradeTimeField": "TRDTIM_1", "subscriptionConfig": {".DJI":{"identifier":".DJI"}}}'
, 3
, '{"field1Name": "TRDPRC_1", "lastTradeTimeField": "TRDTIM_1", "subscriptionConfig": {".DJI":{"identifier":".DJI"}}}'
, 9
, '{"interestSubscription": "USD1MD=", "interestAskField": "ASK", "interestBidField": "BID", "daysConstant": "360", "subscriptionConfig": {".DJI":{"identifier":".DJI"}, "USD1MD=":{"identifier":"USD1MD="}}}'
, 3
, '{"field1Name": "TRDPRC_1", "lastTradeTimeField": "TRDTIM_1", "subscriptionConfig": {".DJI":{"identifier":".DJI"}}}'
, '{"dontCloseAfterXSecNoUpdate": "60", "timeZone": "GMT", "shouldBeClosedByMonitoring": "true"}'
, null
);

INSERT INTO MARKET_DATA_SOURCE_MAPS (
  ID
, MARKET_ID
, MARKET_DATA_SOURCE_ID
, HOUR_LEVEL_FORMULA_ID
, HOUR_LEVEL_FORMULA_PARAMS
, HOUR_CLOSING_FORMULA_ID
, HOUR_CLOSING_FORMULA_PARAMS
, DAY_CLOSING_FORMULA_ID
, DAY_CLOSING_FORMULA_PARAMS
, LONG_TERM_FORMULA_ID
, LONG_TERM_FORMULA_PARAMS
, REAL_LEVEL_FORMULA_ID
, REAL_LEVEL_FORMULA_PARAMS
, MARKET_PARAMS
, MARKET_DISABLE_CONDITIONS 
) VALUES (
SEQ_MARKET_DATA_SOURCE_MAPS.NEXTVAL
, 4
, 1
, 3
, '{"field1Name": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {".FTSE":{"identifier":".FTSE"}}}'
, 3
, '{"field1Name": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {".FTSE":{"identifier":".FTSE"}}}'
, 3
, '{"field1Name": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {".FTSE":{"identifier":".FTSE"}}}'
, 9
, '{"interestSubscription": "GBP1MD=", "interestAskField": "ASK", "interestBidField": "BID", "daysConstant": "365", "subscriptionConfig": {".FTSE":{"identifier":".FTSE"}, "GBP1MD=":{"identifier":"GBP1MD="}}}'
, 3
, '{"field1Name": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {".FTSE":{"identifier":".FTSE"}}}'
, '{"dontCloseAfterXSecNoUpdate": "60", "timeZone": "GMT", "shouldBeClosedByMonitoring": "true"}'
, null
);

INSERT INTO MARKET_DATA_SOURCE_MAPS (
  ID
, MARKET_ID
, MARKET_DATA_SOURCE_ID
, HOUR_LEVEL_FORMULA_ID
, HOUR_LEVEL_FORMULA_PARAMS
, HOUR_CLOSING_FORMULA_ID
, HOUR_CLOSING_FORMULA_PARAMS
, DAY_CLOSING_FORMULA_ID
, DAY_CLOSING_FORMULA_PARAMS
, LONG_TERM_FORMULA_ID
, LONG_TERM_FORMULA_PARAMS
, REAL_LEVEL_FORMULA_ID
, REAL_LEVEL_FORMULA_PARAMS
, MARKET_PARAMS
, MARKET_DISABLE_CONDITIONS 
) VALUES (
SEQ_MARKET_DATA_SOURCE_MAPS.NEXTVAL
, 7
, 1
, 3
, '{"field1Name": "TRDPRC_1", "lastTradeTimeField": "TRDTIM_1", "subscriptionConfig": {".IXIC":{"identifier":".IXIC"}}}'
, 3
, '{"field1Name": "TRDPRC_1", "lastTradeTimeField": "TRDTIM_1", "subscriptionConfig": {".IXIC":{"identifier":".IXIC"}}}'
, 3
, '{"field1Name": "TRDPRC_1", "lastTradeTimeField": "TRDTIM_1", "subscriptionConfig": {".IXIC":{"identifier":".IXIC"}}}'
, 9
, '{"interestSubscription": "USD1MD=", "interestAskField": "ASK", "interestBidField": "BID", "daysConstant": "360", "subscriptionConfig": {".IXIC":{"identifier":".IXIC"}, "USD1MD=":{"identifier":"USD1MD="}}}'
, 3
, '{"field1Name": "TRDPRC_1", "lastTradeTimeField": "TRDTIM_1", "subscriptionConfig": {".IXIC":{"identifier":".IXIC"}}}'
, '{"dontCloseAfterXSecNoUpdate": "60", "timeZone": "GMT", "shouldBeClosedByMonitoring": "true"}'
, null
);

INSERT INTO MARKET_DATA_SOURCE_MAPS (
  ID
, MARKET_ID
, MARKET_DATA_SOURCE_ID
, HOUR_LEVEL_FORMULA_ID
, HOUR_LEVEL_FORMULA_PARAMS
, HOUR_CLOSING_FORMULA_ID
, HOUR_CLOSING_FORMULA_PARAMS
, DAY_CLOSING_FORMULA_ID
, DAY_CLOSING_FORMULA_PARAMS
, LONG_TERM_FORMULA_ID
, LONG_TERM_FORMULA_PARAMS
, REAL_LEVEL_FORMULA_ID
, REAL_LEVEL_FORMULA_PARAMS
, MARKET_PARAMS
, MARKET_DISABLE_CONDITIONS 
) VALUES (
SEQ_MARKET_DATA_SOURCE_MAPS.NEXTVAL
, 8
, 1
, 3
, '{"field1Name": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {".GDAXI":{"identifier":".GDAXI"}}}'
, 3
, '{"field1Name": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {".GDAXI":{"identifier":".GDAXI"}}}'
, 3
, '{"field1Name": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {".GDAXI":{"identifier":".GDAXI"}}}'
, 9
, '{"interestSubscription": "EUR1MD=", "interestAskField": "ASK", "interestBidField": "BID", "daysConstant": "365", "subscriptionConfig": {".GDAXI":{"identifier":".GDAXI"}, "EUR1MD=":{"identifier":"EUR1MD="}}}'
, 3
, '{"field1Name": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {".GDAXI":{"identifier":".GDAXI"}}}'
, '{"dontCloseAfterXSecNoUpdate": "60", "timeZone": "Europe/Berlin", "shouldBeClosedByMonitoring": "true"}'
, null
);

INSERT INTO MARKET_DATA_SOURCE_MAPS (
  ID
, MARKET_ID
, MARKET_DATA_SOURCE_ID
, HOUR_LEVEL_FORMULA_ID
, HOUR_LEVEL_FORMULA_PARAMS
, HOUR_CLOSING_FORMULA_ID
, HOUR_CLOSING_FORMULA_PARAMS
, DAY_CLOSING_FORMULA_ID
, DAY_CLOSING_FORMULA_PARAMS
, LONG_TERM_FORMULA_ID
, LONG_TERM_FORMULA_PARAMS
, REAL_LEVEL_FORMULA_ID
, REAL_LEVEL_FORMULA_PARAMS
, MARKET_PARAMS
, MARKET_DISABLE_CONDITIONS 
) VALUES (
SEQ_MARKET_DATA_SOURCE_MAPS.NEXTVAL
, 9
, 1
, 3
, '{"field1Name": "TRDPRC_1", "lastTradeTimeField": "TRDTIM_1", "subscriptionConfig": {".N225":{"identifier":".N225"}}}'
, 3
, '{"field1Name": "TRDPRC_1", "lastTradeTimeField": "TRDTIM_1", "subscriptionConfig": {".N225":{"identifier":".N225"}}}'
, 3
, '{"field1Name": "TRDPRC_1", "lastTradeTimeField": "TRDTIM_1", "subscriptionConfig": {".N225":{"identifier":".N225"}}}'
, 9
, '{"interestSubscription": "JPY1MD=", "interestAskField": "ASK", "interestBidField": "BID", "daysConstant": "365", "subscriptionConfig": {".N225":{"identifier":".N225"}, "JPY1MD=":{"identifier":"JPY1MD="}}}'
, 3
, '{"field1Name": "TRDPRC_1", "lastTradeTimeField": "TRDTIM_1", "subscriptionConfig": {".N225":{"identifier":".N225"}}}'
, '{"dontCloseAfterXSecNoUpdate": "60", "timeZone": "GMT", "shouldBeClosedByMonitoring": "true"}'
, null
);

INSERT INTO MARKET_DATA_SOURCE_MAPS (
  ID
, MARKET_ID
, MARKET_DATA_SOURCE_ID
, HOUR_LEVEL_FORMULA_ID
, HOUR_LEVEL_FORMULA_PARAMS
, HOUR_CLOSING_FORMULA_ID
, HOUR_CLOSING_FORMULA_PARAMS
, DAY_CLOSING_FORMULA_ID
, DAY_CLOSING_FORMULA_PARAMS
, LONG_TERM_FORMULA_ID
, LONG_TERM_FORMULA_PARAMS
, REAL_LEVEL_FORMULA_ID
, REAL_LEVEL_FORMULA_PARAMS
, MARKET_PARAMS
, MARKET_DISABLE_CONDITIONS 
) VALUES (
SEQ_MARKET_DATA_SOURCE_MAPS.NEXTVAL
, 11
, 1
, 3
, '{"field1Name": "TRDPRC_1", "lastTradeTimeField": "TRDTIM_1", "subscriptionConfig": {".HSI":{"identifier":".HSI"}}}'
, 3
, '{"field1Name": "TRDPRC_1", "lastTradeTimeField": "TRDTIM_1", "subscriptionConfig": {".HSI":{"identifier":".HSI"}}}'
, 3
, '{"field1Name": "TRDPRC_1", "lastTradeTimeField": "TRDTIM_1", "subscriptionConfig": {".HSI":{"identifier":".HSI"}}}'
, 9
, '{"interestSubscription": "HKD1MD=", "interestAskField": "ASK", "interestBidField": "BID", "daysConstant": "365", "subscriptionConfig": {".HSI":{"identifier":".HSI"}, "HKD1MD=":{"identifier":"HKD1MD="}}}'
, 3
, '{"field1Name": "TRDPRC_1", "lastTradeTimeField": "TRDTIM_1", "subscriptionConfig": {".HSI":{"identifier":".HSI"}}}'
, '{"dontCloseAfterXSecNoUpdate": "60", "timeZone": "GMT", "shouldBeClosedByMonitoring": "true"}'
, null
);

INSERT INTO MARKET_DATA_SOURCE_MAPS (
  ID
, MARKET_ID
, MARKET_DATA_SOURCE_ID
, HOUR_LEVEL_FORMULA_ID
, HOUR_LEVEL_FORMULA_PARAMS
, HOUR_CLOSING_FORMULA_ID
, HOUR_CLOSING_FORMULA_PARAMS
, DAY_CLOSING_FORMULA_ID
, DAY_CLOSING_FORMULA_PARAMS
, LONG_TERM_FORMULA_ID
, LONG_TERM_FORMULA_PARAMS
, REAL_LEVEL_FORMULA_ID
, REAL_LEVEL_FORMULA_PARAMS
, MARKET_PARAMS
, MARKET_DISABLE_CONDITIONS 
) VALUES (
SEQ_MARKET_DATA_SOURCE_MAPS.NEXTVAL
, 207
, 1
, 3
, '{"field1Name": "TRDPRC_1", "lastTradeTimeField": "TRDTIM_1", "subscriptionConfig": {".BSESN":{"identifier":".BSESN"}}}'
, 3
, '{"field1Name": "TRDPRC_1", "lastTradeTimeField": "TRDTIM_1", "subscriptionConfig": {".BSESN":{"identifier":".BSESN"}}}'
, 3
, '{"field1Name": "TRDPRC_1", "lastTradeTimeField": "TRDTIM_1", "subscriptionConfig": {".BSESN":{"identifier":".BSESN"}}}'
, 9
, '{"interestSubscription": "INR1MD=", "interestAskField": "ASK", "interestBidField": "BID", "daysConstant": "365", "subscriptionConfig": {".BSESN":{"identifier":".BSESN"}, "INR1MD=":{"identifier":"INR1MD="}}}'
, 3
, '{"field1Name": "TRDPRC_1", "lastTradeTimeField": "TRDTIM_1", "subscriptionConfig": {".BSESN":{"identifier":".BSESN"}}}'
, '{"dontCloseAfterXSecNoUpdate": "60", "timeZone": "GMT", "shouldBeClosedByMonitoring": "true"}'
, null
);

-- not active
INSERT INTO MARKET_DATA_SOURCE_MAPS (
  ID
, MARKET_ID
, MARKET_DATA_SOURCE_ID
, HOUR_LEVEL_FORMULA_ID
, HOUR_LEVEL_FORMULA_PARAMS
, HOUR_CLOSING_FORMULA_ID
, HOUR_CLOSING_FORMULA_PARAMS
, DAY_CLOSING_FORMULA_ID
, DAY_CLOSING_FORMULA_PARAMS
, LONG_TERM_FORMULA_ID
, LONG_TERM_FORMULA_PARAMS
, REAL_LEVEL_FORMULA_ID
, REAL_LEVEL_FORMULA_PARAMS
, MARKET_PARAMS
, MARKET_DISABLE_CONDITIONS 
) VALUES (
SEQ_MARKET_DATA_SOURCE_MAPS.NEXTVAL
, 248
, 1
, 3
, '{"field1Name": "TRDPRC_1", "lastTradeTimeField": "TRDTIM_1", "subscriptionConfig": {".TASI":{"identifier":".TASI"}}}'
, 3
, '{"field1Name": "TRDPRC_1", "lastTradeTimeField": "TRDTIM_1", "subscriptionConfig": {".TASI":{"identifier":".TASI"}}}'
, 3
, '{"field1Name": "TRDPRC_1", "lastTradeTimeField": "TRDTIM_1", "subscriptionConfig": {".TASI":{"identifier":".TASI"}}}'
, 9
, '{"interestSubscription": "SAR1MD=", "interestAskField": "ASK", "interestBidField": "BID", "daysConstant": "365", "subscriptionConfig": {".TASI":{"identifier":".TASI"}, "SAR1MD=":{"identifier":"SAR1MD="}}}'
, 3
, '{"field1Name": "TRDPRC_1", "lastTradeTimeField": "TRDTIM_1", "subscriptionConfig": {".TASI":{"identifier":".TASI"}}}'
, '{"dontCloseAfterXSecNoUpdate": "60", "timeZone": "GMT", "shouldBeClosedByMonitoring": "true"}'
, null
);

INSERT INTO MARKET_DATA_SOURCE_MAPS (
  ID
, MARKET_ID
, MARKET_DATA_SOURCE_ID
, HOUR_LEVEL_FORMULA_ID
, HOUR_LEVEL_FORMULA_PARAMS
, HOUR_CLOSING_FORMULA_ID
, HOUR_CLOSING_FORMULA_PARAMS
, DAY_CLOSING_FORMULA_ID
, DAY_CLOSING_FORMULA_PARAMS
, LONG_TERM_FORMULA_ID
, LONG_TERM_FORMULA_PARAMS
, REAL_LEVEL_FORMULA_ID
, REAL_LEVEL_FORMULA_PARAMS
, MARKET_PARAMS
, MARKET_DISABLE_CONDITIONS 
) VALUES (
SEQ_MARKET_DATA_SOURCE_MAPS.NEXTVAL
, 292
, 1
, 3
, '{"field1Name": "TRDPRC_1", "lastTradeTimeField": "TRDTIM_1", "subscriptionConfig": {".IBEX":{"identifier":".IBEX"}}}'
, 3
, '{"field1Name": "TRDPRC_1", "lastTradeTimeField": "TRDTIM_1", "subscriptionConfig": {".IBEX":{"identifier":".IBEX"}}}'
, 3
, '{"field1Name": "TRDPRC_1", "lastTradeTimeField": "TRDTIM_1", "subscriptionConfig": {".IBEX":{"identifier":".IBEX"}}}'
, 9
, '{"interestSubscription": "EUR1MD=", "interestAskField": "ASK", "interestBidField": "BID", "daysConstant": "365", "subscriptionConfig": {".IBEX":{"identifier":".IBEX"}, "EUR1MD=":{"identifier":"EUR1MD="}}}'
, 3
, '{"field1Name": "TRDPRC_1", "lastTradeTimeField": "TRDTIM_1", "subscriptionConfig": {".IBEX":{"identifier":".IBEX"}}}'
, '{"dontCloseAfterXSecNoUpdate": "60", "timeZone": "GMT", "shouldBeClosedByMonitoring": "true"}'
, null
);

INSERT INTO MARKET_DATA_SOURCE_MAPS (
  ID
, MARKET_ID
, MARKET_DATA_SOURCE_ID
, HOUR_LEVEL_FORMULA_ID
, HOUR_LEVEL_FORMULA_PARAMS
, HOUR_CLOSING_FORMULA_ID
, HOUR_CLOSING_FORMULA_PARAMS
, DAY_CLOSING_FORMULA_ID
, DAY_CLOSING_FORMULA_PARAMS
, LONG_TERM_FORMULA_ID
, LONG_TERM_FORMULA_PARAMS
, REAL_LEVEL_FORMULA_ID
, REAL_LEVEL_FORMULA_PARAMS
, MARKET_PARAMS
, MARKET_DISABLE_CONDITIONS 
) VALUES (
SEQ_MARKET_DATA_SOURCE_MAPS.NEXTVAL
, 294
, 1
, 3
, '{"field1Name": "TRDPRC_1", "lastTradeTimeField": "TRDTIM_1", "subscriptionConfig": {".SSE180":{"identifier":".SSE180"}}}'
, 3
, '{"field1Name": "TRDPRC_1", "lastTradeTimeField": "TRDTIM_1", "subscriptionConfig": {".SSE180":{"identifier":".SSE180"}}}'
, 3
, '{"field1Name": "TRDPRC_1", "lastTradeTimeField": "TRDTIM_1", "subscriptionConfig": {".SSE180":{"identifier":".SSE180"}}}'
, 9
, '{"interestSubscription": "CNY1MD=", "interestAskField": "ASK", "interestBidField": "BID", "daysConstant": "365", "subscriptionConfig": {".SSE180":{"identifier":".SSE180"}, "CNY1MD=":{"identifier":"CNY1MD="}}}'
, 3
, '{"field1Name": "TRDPRC_1", "lastTradeTimeField": "TRDTIM_1", "subscriptionConfig": {".SSE180":{"identifier":".SSE180"}}}'
, '{"dontCloseAfterXSecNoUpdate": "60", "timeZone": "GMT", "shouldBeClosedByMonitoring": "true"}'
, null
);

INSERT INTO MARKET_DATA_SOURCE_MAPS (
  ID
, MARKET_ID
, MARKET_DATA_SOURCE_ID
, HOUR_LEVEL_FORMULA_ID
, HOUR_LEVEL_FORMULA_PARAMS
, HOUR_CLOSING_FORMULA_ID
, HOUR_CLOSING_FORMULA_PARAMS
, DAY_CLOSING_FORMULA_ID
, DAY_CLOSING_FORMULA_PARAMS
, LONG_TERM_FORMULA_ID
, LONG_TERM_FORMULA_PARAMS
, REAL_LEVEL_FORMULA_ID
, REAL_LEVEL_FORMULA_PARAMS
, MARKET_PARAMS
, MARKET_DISABLE_CONDITIONS 
) VALUES (
SEQ_MARKET_DATA_SOURCE_MAPS.NEXTVAL
, 371
, 1
, 3
, '{"field1Name": "TRDPRC_1", "lastTradeTimeField": "TRDTIM_1", "subscriptionConfig": {".DFMGI":{"identifier":".DFMGI"}}}'
, 3
, '{"field1Name": "TRDPRC_1", "lastTradeTimeField": "TRDTIM_1", "subscriptionConfig": {".DFMGI":{"identifier":".DFMGI"}}}'
, 3
, '{"field1Name": "TRDPRC_1", "lastTradeTimeField": "TRDTIM_1", "subscriptionConfig": {".DFMGI":{"identifier":".DFMGI"}}}'
, 9
, '{"interestSubscription": "AED1MD=", "interestAskField": "ASK", "interestBidField": "BID", "daysConstant": "365", "subscriptionConfig": {".DFMGI":{"identifier":".DFMGI"}, "AED1MD=":{"identifier":"AED1MD="}}}'
, 3
, '{"field1Name": "TRDPRC_1", "lastTradeTimeField": "TRDTIM_1", "subscriptionConfig": {".DFMGI":{"identifier":".DFMGI"}}}'
, '{"dontCloseAfterXSecNoUpdate": "240", "timeZone": "GMT", "shouldBeClosedByMonitoring": "true"}'
, null
);

INSERT INTO MARKET_DATA_SOURCE_MAPS (
  ID
, MARKET_ID
, MARKET_DATA_SOURCE_ID
, HOUR_LEVEL_FORMULA_ID
, HOUR_LEVEL_FORMULA_PARAMS
, HOUR_CLOSING_FORMULA_ID
, HOUR_CLOSING_FORMULA_PARAMS
, DAY_CLOSING_FORMULA_ID
, DAY_CLOSING_FORMULA_PARAMS
, LONG_TERM_FORMULA_ID
, LONG_TERM_FORMULA_PARAMS
, REAL_LEVEL_FORMULA_ID
, REAL_LEVEL_FORMULA_PARAMS
, MARKET_PARAMS
, MARKET_DISABLE_CONDITIONS 
) VALUES (
SEQ_MARKET_DATA_SOURCE_MAPS.NEXTVAL
, 390
, 1
, 3
, '{"field1Name": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {".MDAX":{"identifier":".MDAX"}}}'
, 3
, '{"field1Name": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {".MDAX":{"identifier":".MDAX"}}}'
, 3
, '{"field1Name": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {".MDAX":{"identifier":".MDAX"}}}'
, 9
, '{"interestSubscription": "EUR1MD=", "interestAskField": "ASK", "interestBidField": "BID", "daysConstant": "365", "subscriptionConfig": {".MDAX":{"identifier":".MDAX"}, "EUR1MD=":{"identifier":"EUR1MD="}}}'
, 3
, '{"field1Name": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {".MDAX":{"identifier":".MDAX"}}}'
, '{"dontCloseAfterXSecNoUpdate": "60", "timeZone": "Europe/Berlin", "shouldBeClosedByMonitoring": "true"}'
, null
);

INSERT INTO MARKET_DATA_SOURCE_MAPS (
  ID
, MARKET_ID
, MARKET_DATA_SOURCE_ID
, HOUR_LEVEL_FORMULA_ID
, HOUR_LEVEL_FORMULA_PARAMS
, HOUR_CLOSING_FORMULA_ID
, HOUR_CLOSING_FORMULA_PARAMS
, DAY_CLOSING_FORMULA_ID
, DAY_CLOSING_FORMULA_PARAMS
, LONG_TERM_FORMULA_ID
, LONG_TERM_FORMULA_PARAMS
, REAL_LEVEL_FORMULA_ID
, REAL_LEVEL_FORMULA_PARAMS
, MARKET_PARAMS
, MARKET_DISABLE_CONDITIONS 
) VALUES (
SEQ_MARKET_DATA_SOURCE_MAPS.NEXTVAL
, 391
, 1
, 3
, '{"field1Name": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {".TECDAX":{"identifier":".TECDAX"}}}'
, 3
, '{"field1Name": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {".TECDAX":{"identifier":".TECDAX"}}}'
, 3
, '{"field1Name": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {".TECDAX":{"identifier":".TECDAX"}}}'
, 9
, '{"interestSubscription": "EUR1MD=", "interestAskField": "ASK", "interestBidField": "BID", "daysConstant": "365", "subscriptionConfig": {".TECDAX":{"identifier":".TECDAX"}, "EUR1MD=":{"identifier":"EUR1MD="}}}'
, 3
, '{"field1Name": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {".TECDAX":{"identifier":".TECDAX"}}}'
, '{"dontCloseAfterXSecNoUpdate": "120", "timeZone": "Europe/Berlin", "shouldBeClosedByMonitoring": "true"}'
, null
);

INSERT INTO MARKET_DATA_SOURCE_MAPS (
  ID
, MARKET_ID
, MARKET_DATA_SOURCE_ID
, HOUR_LEVEL_FORMULA_ID
, HOUR_LEVEL_FORMULA_PARAMS
, HOUR_CLOSING_FORMULA_ID
, HOUR_CLOSING_FORMULA_PARAMS
, DAY_CLOSING_FORMULA_ID
, DAY_CLOSING_FORMULA_PARAMS
, LONG_TERM_FORMULA_ID
, LONG_TERM_FORMULA_PARAMS
, REAL_LEVEL_FORMULA_ID
, REAL_LEVEL_FORMULA_PARAMS
, MARKET_PARAMS
, MARKET_DISABLE_CONDITIONS 
) VALUES (
SEQ_MARKET_DATA_SOURCE_MAPS.NEXTVAL
, 549
, 1
, 3
, '{"field1Name": "TRDPRC_1", "lastTradeTimeField": "TRDTIM_1", "subscriptionConfig": {".FTMIB":{"identifier":".FTMIB"}}}'
, 3
, '{"field1Name": "TRDPRC_1", "lastTradeTimeField": "TRDTIM_1", "subscriptionConfig": {".FTMIB":{"identifier":".FTMIB"}}}'
, 3
, '{"field1Name": "TRDPRC_1", "lastTradeTimeField": "TRDTIM_1", "subscriptionConfig": {".FTMIB":{"identifier":".FTMIB"}}}'
, 9
, '{"interestSubscription": "EUR1MD=", "interestAskField": "ASK", "interestBidField": "BID", "daysConstant": "365", "subscriptionConfig": {".FTMIB":{"identifier":".FTMIB"}, "EUR1MD=":{"identifier":"EUR1MD="}}}'
, 3
, '{"field1Name": "TRDPRC_1", "lastTradeTimeField": "TRDTIM_1", "subscriptionConfig": {".FTMIB":{"identifier":".FTMIB"}}}'
, '{"dontCloseAfterXSecNoUpdate": "60", "timeZone": "GMT", "shouldBeClosedByMonitoring": "true"}'
, null
);

INSERT INTO MARKET_DATA_SOURCE_MAPS (
  ID
, MARKET_ID
, MARKET_DATA_SOURCE_ID
, HOUR_LEVEL_FORMULA_ID
, HOUR_LEVEL_FORMULA_PARAMS
, HOUR_CLOSING_FORMULA_ID
, HOUR_CLOSING_FORMULA_PARAMS
, DAY_CLOSING_FORMULA_ID
, DAY_CLOSING_FORMULA_PARAMS
, LONG_TERM_FORMULA_ID
, LONG_TERM_FORMULA_PARAMS
, REAL_LEVEL_FORMULA_ID
, REAL_LEVEL_FORMULA_PARAMS
, MARKET_PARAMS
, MARKET_DISABLE_CONDITIONS 
) VALUES (
SEQ_MARKET_DATA_SOURCE_MAPS.NEXTVAL
, 550
, 1
, 3
, '{"field1Name": "TRDPRC_1", "lastTradeTimeField": "TRDTIM_1", "subscriptionConfig": {".PSI20":{"identifier":".PSI20"}}}'
, 3
, '{"field1Name": "TRDPRC_1", "lastTradeTimeField": "TRDTIM_1", "subscriptionConfig": {".PSI20":{"identifier":".PSI20"}}}'
, 3
, '{"field1Name": "TRDPRC_1", "lastTradeTimeField": "TRDTIM_1", "subscriptionConfig": {".PSI20":{"identifier":".PSI20"}}}'
, 9
, '{"interestSubscription": "EUR1MD=", "interestAskField": "ASK", "interestBidField": "BID", "daysConstant": "365", "subscriptionConfig": {".PSI20":{"identifier":".PSI20"}, "EUR1MD=":{"identifier":"EUR1MD="}}}'
, 3
, '{"field1Name": "TRDPRC_1", "lastTradeTimeField": "TRDTIM_1", "subscriptionConfig": {".PSI20":{"identifier":".PSI20"}}}'
, '{"dontCloseAfterXSecNoUpdate": "60", "timeZone": "GMT", "shouldBeClosedByMonitoring": "true"}'
, null
);

INSERT INTO MARKET_DATA_SOURCE_MAPS (
  ID
, MARKET_ID
, MARKET_DATA_SOURCE_ID
, HOUR_LEVEL_FORMULA_ID
, HOUR_LEVEL_FORMULA_PARAMS
, HOUR_CLOSING_FORMULA_ID
, HOUR_CLOSING_FORMULA_PARAMS
, DAY_CLOSING_FORMULA_ID
, DAY_CLOSING_FORMULA_PARAMS
, LONG_TERM_FORMULA_ID
, LONG_TERM_FORMULA_PARAMS
, REAL_LEVEL_FORMULA_ID
, REAL_LEVEL_FORMULA_PARAMS
, MARKET_PARAMS
, MARKET_DISABLE_CONDITIONS 
) VALUES (
SEQ_MARKET_DATA_SOURCE_MAPS.NEXTVAL
, 574
, 1
, 3
, '{"field1Name": "TRDPRC_1", "lastTradeTimeField": "TRDTIM_1", "subscriptionConfig": {".AXJO":{"identifier":".AXJO"}}}'
, 3
, '{"field1Name": "TRDPRC_1", "lastTradeTimeField": "TRDTIM_1", "subscriptionConfig": {".AXJO":{"identifier":".AXJO"}}}'
, 3
, '{"field1Name": "TRDPRC_1", "lastTradeTimeField": "TRDTIM_1", "subscriptionConfig": {".AXJO":{"identifier":".AXJO"}}}'
, 9
, '{"interestSubscription": "AUD1MD=", "interestAskField": "ASK", "interestBidField": "BID", "daysConstant": "365", "subscriptionConfig": {".AXJO":{"identifier":".AXJO"}, "AUD1MD=":{"identifier":"AUD1MD="}}}'
, 3
, '{"field1Name": "TRDPRC_1", "lastTradeTimeField": "TRDTIM_1", "subscriptionConfig": {".AXJO":{"identifier":".AXJO"}}}'
, '{"dontCloseAfterXSecNoUpdate": "60", "timeZone": "GMT", "shouldBeClosedByMonitoring": "true"}'
, null
);

INSERT INTO MARKET_DATA_SOURCE_MAPS (
  ID
, MARKET_ID
, MARKET_DATA_SOURCE_ID
, HOUR_LEVEL_FORMULA_ID
, HOUR_LEVEL_FORMULA_PARAMS
, HOUR_CLOSING_FORMULA_ID
, HOUR_CLOSING_FORMULA_PARAMS
, DAY_CLOSING_FORMULA_ID
, DAY_CLOSING_FORMULA_PARAMS
, LONG_TERM_FORMULA_ID
, LONG_TERM_FORMULA_PARAMS
, REAL_LEVEL_FORMULA_ID
, REAL_LEVEL_FORMULA_PARAMS
, MARKET_PARAMS
, MARKET_DISABLE_CONDITIONS 
) VALUES (
SEQ_MARKET_DATA_SOURCE_MAPS.NEXTVAL
, 579
, 1
, 3
, '{"field1Name": "TRDPRC_1", "lastTradeTimeField": "TRDTIM_1", "subscriptionConfig": {".TOPX500":{"identifier":".TOPX500"}}}'
, 3
, '{"field1Name": "TRDPRC_1", "lastTradeTimeField": "TRDTIM_1", "subscriptionConfig": {".TOPX500":{"identifier":".TOPX500"}}}'
, 3
, '{"field1Name": "TRDPRC_1", "lastTradeTimeField": "TRDTIM_1", "subscriptionConfig": {".TOPX500":{"identifier":".TOPX500"}}}'
, 9
, '{"interestSubscription": "JPY1MD=", "interestAskField": "ASK", "interestBidField": "BID", "daysConstant": "365", "subscriptionConfig": {".TOPX500":{"identifier":".TOPX500"}, "JPY1MD=":{"identifier":"JPY1MD="}}}'
, 3
, '{"field1Name": "TRDPRC_1", "lastTradeTimeField": "TRDTIM_1", "subscriptionConfig": {".TOPX500":{"identifier":".TOPX500"}}}'
, '{"dontCloseAfterXSecNoUpdate": "60", "timeZone": "GMT", "shouldBeClosedByMonitoring": "true"}'
, null
);

INSERT INTO MARKET_DATA_SOURCE_MAPS (
  ID
, MARKET_ID
, MARKET_DATA_SOURCE_ID
, HOUR_LEVEL_FORMULA_ID
, HOUR_LEVEL_FORMULA_PARAMS
, HOUR_CLOSING_FORMULA_ID
, HOUR_CLOSING_FORMULA_PARAMS
, DAY_CLOSING_FORMULA_ID
, DAY_CLOSING_FORMULA_PARAMS
, LONG_TERM_FORMULA_ID
, LONG_TERM_FORMULA_PARAMS
, REAL_LEVEL_FORMULA_ID
, REAL_LEVEL_FORMULA_PARAMS
, MARKET_PARAMS
, MARKET_DISABLE_CONDITIONS 
) VALUES (
SEQ_MARKET_DATA_SOURCE_MAPS.NEXTVAL
, 599
, 1
, 3
, '{"field1Name": "TRDPRC_1", "lastTradeTimeField": "TRDTIM_1", "subscriptionConfig": {".FTITLMS":{"identifier":".FTITLMS"}}}'
, 3
, '{"field1Name": "TRDPRC_1", "lastTradeTimeField": "TRDTIM_1", "subscriptionConfig": {".FTITLMS":{"identifier":".FTITLMS"}}}'
, 3
, '{"field1Name": "TRDPRC_1", "lastTradeTimeField": "TRDTIM_1", "subscriptionConfig": {".FTITLMS":{"identifier":".FTITLMS"}}}'
, 9
, '{"interestSubscription": "EUR1MD=", "interestAskField": "ASK", "interestBidField": "BID", "daysConstant": "365", "subscriptionConfig": {".FTITLMS":{"identifier":".FTITLMS"}, "EUR1MD=":{"identifier":"EUR1MD="}}}'
, 3
, '{"field1Name": "TRDPRC_1", "lastTradeTimeField": "TRDTIM_1", "subscriptionConfig": {".FTITLMS":{"identifier":".FTITLMS"}}}'
, '{"dontCloseAfterXSecNoUpdate": "60", "timeZone": "GMT", "shouldBeClosedByMonitoring": "true"}'
, null
);

INSERT INTO MARKET_DATA_SOURCE_MAPS (
  ID
, MARKET_ID
, MARKET_DATA_SOURCE_ID
, HOUR_LEVEL_FORMULA_ID
, HOUR_LEVEL_FORMULA_PARAMS
, HOUR_CLOSING_FORMULA_ID
, HOUR_CLOSING_FORMULA_PARAMS
, DAY_CLOSING_FORMULA_ID
, DAY_CLOSING_FORMULA_PARAMS
, LONG_TERM_FORMULA_ID
, LONG_TERM_FORMULA_PARAMS
, REAL_LEVEL_FORMULA_ID
, REAL_LEVEL_FORMULA_PARAMS
, MARKET_PARAMS
, MARKET_DISABLE_CONDITIONS 
) VALUES (
SEQ_MARKET_DATA_SOURCE_MAPS.NEXTVAL
, 657
, 1
, 3
, '{"field1Name": "TRDPRC_1", "lastTradeTimeField": "TRDTIM_1", "subscriptionConfig": {".AEX":{"identifier":".AEX"}}}'
, 3
, '{"field1Name": "TRDPRC_1", "lastTradeTimeField": "TRDTIM_1", "subscriptionConfig": {".AEX":{"identifier":".AEX"}}}'
, 3
, '{"field1Name": "TRDPRC_1", "lastTradeTimeField": "TRDTIM_1", "subscriptionConfig": {".AEX":{"identifier":".AEX"}}}'
, 9
, '{"interestSubscription": "EUR1MD=", "interestAskField": "ASK", "interestBidField": "BID", "daysConstant": "365", "subscriptionConfig": {".AEX":{"identifier":".AEX"}, "EUR1MD=":{"identifier":"EUR1MD="}}}'
, 3
, '{"field1Name": "TRDPRC_1", "lastTradeTimeField": "TRDTIM_1", "subscriptionConfig": {".AEX":{"identifier":".AEX"}}}'
, '{"dontCloseAfterXSecNoUpdate": "60", "timeZone": "GMT", "shouldBeClosedByMonitoring": "true"}'
, null
);

INSERT INTO MARKET_DATA_SOURCE_MAPS (
  ID
, MARKET_ID
, MARKET_DATA_SOURCE_ID
, HOUR_LEVEL_FORMULA_ID
, HOUR_LEVEL_FORMULA_PARAMS
, HOUR_CLOSING_FORMULA_ID
, HOUR_CLOSING_FORMULA_PARAMS
, DAY_CLOSING_FORMULA_ID
, DAY_CLOSING_FORMULA_PARAMS
, LONG_TERM_FORMULA_ID
, LONG_TERM_FORMULA_PARAMS
, REAL_LEVEL_FORMULA_ID
, REAL_LEVEL_FORMULA_PARAMS
, MARKET_PARAMS
, MARKET_DISABLE_CONDITIONS 
) VALUES (
SEQ_MARKET_DATA_SOURCE_MAPS.NEXTVAL
, 690
, 1
, 3
, '{"field1Name": "TRDPRC_1", "lastTradeTimeField": "TRDTIM_1", "subscriptionConfig": {".OMXS30":{"identifier":".OMXS30"}}}'
, 3
, '{"field1Name": "TRDPRC_1", "lastTradeTimeField": "TRDTIM_1", "subscriptionConfig": {".OMXS30":{"identifier":".OMXS30"}}}'
, 3
, '{"field1Name": "TRDPRC_1", "lastTradeTimeField": "TRDTIM_1", "subscriptionConfig": {".OMXS30":{"identifier":".OMXS30"}}}'
, 9
, '{"interestSubscription": "SEK1MD=", "interestAskField": "ASK", "interestBidField": "BID", "daysConstant": "365", "subscriptionConfig": {".OMXS30":{"identifier":".OMXS30"}, "SEK1MD=":{"identifier":"SEK1MD="}}}'
, 3
, '{"field1Name": "TRDPRC_1", "lastTradeTimeField": "TRDTIM_1", "subscriptionConfig": {".OMXS30":{"identifier":".OMXS30"}}}'
, '{"dontCloseAfterXSecNoUpdate": "60", "timeZone": "GMT", "shouldBeClosedByMonitoring": "true"}'
, null
);
-- End of .DJI like indices

-- TEVA.TA like markets
INSERT INTO MARKET_DATA_SOURCE_MAPS (
  ID
, MARKET_ID
, MARKET_DATA_SOURCE_ID
, HOUR_LEVEL_FORMULA_ID
, HOUR_LEVEL_FORMULA_PARAMS
, HOUR_CLOSING_FORMULA_ID
, HOUR_CLOSING_FORMULA_PARAMS
, DAY_CLOSING_FORMULA_ID
, DAY_CLOSING_FORMULA_PARAMS
, LONG_TERM_FORMULA_ID
, LONG_TERM_FORMULA_PARAMS
, REAL_LEVEL_FORMULA_ID
, REAL_LEVEL_FORMULA_PARAMS
, MARKET_PARAMS
, MARKET_DISABLE_CONDITIONS 
) VALUES (
SEQ_MARKET_DATA_SOURCE_MAPS.NEXTVAL
, 12
, 1
, 5
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "TRDPRC_1", "lastTradeTimeField": "TIMACT", "subscriptionConfig": {"TEVA.TA":{"identifier":"TEVA.TA"}}}'
, 2
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "TRDPRC_1", "lastTradeTimeField": "TIMACT", "subscriptionConfig": {"TEVA.TA":{"identifier":"TEVA.TA"}}}'
, 3
, '{"field1Name": "OFF_CLOSE", "lastTradeTimeField": "TIMACT", "subscriptionConfig": {"TEVA.TA":{"identifier":"TEVA.TA"}}}'
, 9
, '{"interestSubscription": "ILS1MD=", "interestAskField": "ASK", "interestBidField": "BID", "daysConstant": "365", "subscriptionConfig": {"TEVA.TA":{"identifier":"TEVA.TA"}, "ILS1MD=":{"identifier":"ILS1MD="}}}'
, 2
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "TRDPRC_1", "lastTradeTimeField": "TIMACT", "subscriptionConfig": {"TEVA.TA":{"identifier":"TEVA.TA"}}}'
, '{"dontCloseAfterXSecNoUpdate": "60", "timeZone": "GMT", "shouldBeClosedByMonitoring": "true"}'
, q'{{"com.anyoption.service.level.condition.AskBidDistanceMarketDisableCondition": {'subscriptionName': 'TEVA.TA', 'askField': 'ASK', 'bidField': 'BID', 'lastField': 'TRDPRC_1', 'distance': '0.02'}, "com.anyoption.service.level.condition.AskBidInverseMarketDisableCondition": {'subscriptionName': 'TEVA.TA', 'askField': 'ASK', 'bidField': 'BID'}}}'
);

INSERT INTO MARKET_DATA_SOURCE_MAPS (
  ID
, MARKET_ID
, MARKET_DATA_SOURCE_ID
, HOUR_LEVEL_FORMULA_ID
, HOUR_LEVEL_FORMULA_PARAMS
, HOUR_CLOSING_FORMULA_ID
, HOUR_CLOSING_FORMULA_PARAMS
, DAY_CLOSING_FORMULA_ID
, DAY_CLOSING_FORMULA_PARAMS
, LONG_TERM_FORMULA_ID
, LONG_TERM_FORMULA_PARAMS
, REAL_LEVEL_FORMULA_ID
, REAL_LEVEL_FORMULA_PARAMS
, MARKET_PARAMS
, MARKET_DISABLE_CONDITIONS 
) VALUES (
SEQ_MARKET_DATA_SOURCE_MAPS.NEXTVAL
, 23
, 1
, 5
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "TRDPRC_1", "lastTradeTimeField": "TIMACT", "subscriptionConfig": {"DLEKG.TA":{"identifier":"DLEKG.TA"}}}'
, 2
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "TRDPRC_1", "lastTradeTimeField": "TIMACT", "subscriptionConfig": {"DLEKG.TA":{"identifier":"DLEKG.TA"}}}'
, 3
, '{"field1Name": "OFF_CLOSE", "lastTradeTimeField": "TIMACT", "subscriptionConfig": {"DLEKG.TA":{"identifier":"DLEKG.TA"}}}'
, 9
, '{"interestSubscription": "ILS1MD=", "interestAskField": "ASK", "interestBidField": "BID", "daysConstant": "365", "subscriptionConfig": {"DLEKG.TA":{"identifier":"DLEKG.TA"}, "ILS1MD=":{"identifier":"ILS1MD="}}}'
, 2
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "TRDPRC_1", "lastTradeTimeField": "TIMACT", "subscriptionConfig": {"DLEKG.TA":{"identifier":"DLEKG.TA"}}}'
, '{"dontCloseAfterXSecNoUpdate": "60", "timeZone": "GMT", "shouldBeClosedByMonitoring": "true"}'
, q'{{"com.anyoption.service.level.condition.AskBidDistanceMarketDisableCondition": {'subscriptionName': 'DLEKG.TA', 'askField': 'ASK', 'bidField': 'BID', 'lastField': 'TRDPRC_1', 'distance': '0.02'}, "com.anyoption.service.level.condition.AskBidInverseMarketDisableCondition": {'subscriptionName': 'DLEKG.TA', 'askField': 'ASK', 'bidField': 'BID'}}}'
);
-- End of TEVA.TA like markets

-- EUR= like currencies
INSERT INTO MARKET_DATA_SOURCE_MAPS (
  ID
, MARKET_ID
, MARKET_DATA_SOURCE_ID
, HOUR_LEVEL_FORMULA_ID
, HOUR_LEVEL_FORMULA_PARAMS
, HOUR_CLOSING_FORMULA_ID
, HOUR_CLOSING_FORMULA_PARAMS
, DAY_CLOSING_FORMULA_ID
, DAY_CLOSING_FORMULA_PARAMS
, LONG_TERM_FORMULA_ID
, LONG_TERM_FORMULA_PARAMS
, REAL_LEVEL_FORMULA_ID
, REAL_LEVEL_FORMULA_PARAMS
, MARKET_PARAMS
, MARKET_DISABLE_CONDITIONS 
) VALUES (
SEQ_MARKET_DATA_SOURCE_MAPS.NEXTVAL
, 16
, 1
, 7
, '{"last5UpdatesFormulaConfig": {"field1Name": "ASK", "field2Name": "BID", "subscriptionConfig": {"EUR=": {"identifier": "EUR="}}}, "avgOf2FormulaConfig": {"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "PRIMACT_1", "lastTradeTimeField": "TIMACT", "subscriptionConfig": {"EUR=FXAL":{"identifier":"EUR=FXAL"}}}, "fXALParameter": "0"}'
, 1
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "PRIMACT_1", "lastTradeTimeField": "TIMACT", "subscriptionConfig": {"EUR=":{"identifier":"EUR="}}, "ignoreTradePrice": "true"}'
, 1
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "PRIMACT_1", "lastTradeTimeField": "TIMACT", "subscriptionConfig": {"EUR=":{"identifier":"EUR="}}, "ignoreTradePrice": "true"}'
, 8
, '{"interestSubscription1": "USD1MD=", "interestSubscription2": "EUR1MD=", "interestAskField": "ASK", "interestBidField": "BID", "daysConstant1": "36000", "daysConstant2": "36000", "spotConstant": "2", "divideC16ByC15": "true", "subscriptionConfig": {"EUR=":{"identifier":"EUR="}, "USD1MD=":{"identifier":"USD1MD="}, "EUR1MD=":{"identifier":"EUR1MD="}}}'
, 1
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "PRIMACT_1", "lastTradeTimeField": "TIMACT", "subscriptionConfig": {"EUR=":{"identifier":"EUR="}}}'
, '{"dontCloseAfterXSecNoUpdate": "60", "timeZone": "GMT", "shouldBeClosedByMonitoring": "false"}'
, null
);

INSERT INTO MARKET_DATA_SOURCE_MAPS (
  ID
, MARKET_ID
, MARKET_DATA_SOURCE_ID
, HOUR_LEVEL_FORMULA_ID
, HOUR_LEVEL_FORMULA_PARAMS
, HOUR_CLOSING_FORMULA_ID
, HOUR_CLOSING_FORMULA_PARAMS
, DAY_CLOSING_FORMULA_ID
, DAY_CLOSING_FORMULA_PARAMS
, LONG_TERM_FORMULA_ID
, LONG_TERM_FORMULA_PARAMS
, REAL_LEVEL_FORMULA_ID
, REAL_LEVEL_FORMULA_PARAMS
, MARKET_PARAMS
, MARKET_DISABLE_CONDITIONS 
) VALUES (
SEQ_MARKET_DATA_SOURCE_MAPS.NEXTVAL
, 14
, 1
, 7
, '{"last5UpdatesFormulaConfig": {"field1Name": "ASK", "field2Name": "BID", "subscriptionConfig": {"GBP=": {"identifier": "GBP="}}}, "avgOf2FormulaConfig": {"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "PRIMACT_1", "lastTradeTimeField": "TIMACT", "subscriptionConfig": {"GBP=FXAL":{"identifier":"GBP=FXAL"}}}, "fXALParameter": "0"}'
, 1
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "PRIMACT_1", "lastTradeTimeField": "TIMACT", "subscriptionConfig": {"GBP=":{"identifier":"GBP="}}, "ignoreTradePrice": "true"}'
, 1
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "PRIMACT_1", "lastTradeTimeField": "TIMACT", "subscriptionConfig": {"GBP=":{"identifier":"GBP="}}, "ignoreTradePrice": "true"}'
, 8
, '{"interestSubscription1": "USD1MD=", "interestSubscription2": "GBP1MD=", "interestAskField": "ASK", "interestBidField": "BID", "daysConstant1": "36500", "daysConstant2": "36500", "spotConstant": "2", "divideC16ByC15": "true", "subscriptionConfig": {"GBP=":{"identifier":"GBP="}, "USD1MD=":{"identifier":"USD1MD="}, "GBP1MD=":{"identifier":"GBP1MD="}}}'
, 1
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "PRIMACT_1", "lastTradeTimeField": "TIMACT", "subscriptionConfig": {"GBP=":{"identifier":"GBP="}}}'
, '{"dontCloseAfterXSecNoUpdate": "60", "timeZone": "GMT", "shouldBeClosedByMonitoring": "false"}'
, null
);

INSERT INTO MARKET_DATA_SOURCE_MAPS (
  ID
, MARKET_ID
, MARKET_DATA_SOURCE_ID
, HOUR_LEVEL_FORMULA_ID
, HOUR_LEVEL_FORMULA_PARAMS
, HOUR_CLOSING_FORMULA_ID
, HOUR_CLOSING_FORMULA_PARAMS
, DAY_CLOSING_FORMULA_ID
, DAY_CLOSING_FORMULA_PARAMS
, LONG_TERM_FORMULA_ID
, LONG_TERM_FORMULA_PARAMS
, REAL_LEVEL_FORMULA_ID
, REAL_LEVEL_FORMULA_PARAMS
, MARKET_PARAMS
, MARKET_DISABLE_CONDITIONS 
) VALUES (
SEQ_MARKET_DATA_SOURCE_MAPS.NEXTVAL
, 17
, 1
, 7
, '{"last5UpdatesFormulaConfig": {"field1Name": "ASK", "field2Name": "BID", "subscriptionConfig": {"TRY=": {"identifier": "TRY="}}}, "avgOf2FormulaConfig": {"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "PRIMACT_1", "lastTradeTimeField": "TIMACT", "subscriptionConfig": {"TRY=FXAL":{"identifier":"TRY=FXAL"}}}, "fXALParameter": "0"}'
, 1
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "PRIMACT_1", "lastTradeTimeField": "TIMACT", "subscriptionConfig": {"TRY=":{"identifier":"TRY="}}, "ignoreTradePrice": "true"}'
, 1
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "PRIMACT_1", "lastTradeTimeField": "TIMACT", "subscriptionConfig": {"TRY=":{"identifier":"TRY="}}, "ignoreTradePrice": "true"}'
, 8
, '{"interestSubscription1": "USD1MD=", "interestSubscription2": "TRY1MD=", "interestAskField": "ASK", "interestBidField": "BID", "daysConstant1": "36500", "daysConstant2": "36500", "spotConstant": "2", "divideC16ByC15": "false", "subscriptionConfig": {"TRY=":{"identifier":"TRY="}, "USD1MD=":{"identifier":"USD1MD="}, "TRY1MD=":{"identifier":"TRY1MD="}}}'
, 1
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "PRIMACT_1", "lastTradeTimeField": "TIMACT", "subscriptionConfig": {"TRY=":{"identifier":"TRY="}}}'
, '{"dontCloseAfterXSecNoUpdate": "60", "timeZone": "GMT", "shouldBeClosedByMonitoring": "false"}'
, null
);

INSERT INTO MARKET_DATA_SOURCE_MAPS (
  ID
, MARKET_ID
, MARKET_DATA_SOURCE_ID
, HOUR_LEVEL_FORMULA_ID
, HOUR_LEVEL_FORMULA_PARAMS
, HOUR_CLOSING_FORMULA_ID
, HOUR_CLOSING_FORMULA_PARAMS
, DAY_CLOSING_FORMULA_ID
, DAY_CLOSING_FORMULA_PARAMS
, LONG_TERM_FORMULA_ID
, LONG_TERM_FORMULA_PARAMS
, REAL_LEVEL_FORMULA_ID
, REAL_LEVEL_FORMULA_PARAMS
, MARKET_PARAMS
, MARKET_DISABLE_CONDITIONS 
) VALUES (
SEQ_MARKET_DATA_SOURCE_MAPS.NEXTVAL
, 18
, 1
, 7
, '{"last5UpdatesFormulaConfig": {"field1Name": "ASK", "field2Name": "BID", "subscriptionConfig": {"JPY=": {"identifier": "JPY="}}}, "avgOf2FormulaConfig": {"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "PRIMACT_1", "lastTradeTimeField": "TIMACT", "subscriptionConfig": {"JPY=FXAL":{"identifier":"JPY=FXAL"}}}, "fXALParameter": "0"}'
, 1
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "PRIMACT_1", "lastTradeTimeField": "TIMACT", "subscriptionConfig": {"JPY=":{"identifier":"JPY="}}, "ignoreTradePrice": "true"}'
, 1
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "PRIMACT_1", "lastTradeTimeField": "TIMACT", "subscriptionConfig": {"JPY=":{"identifier":"JPY="}}, "ignoreTradePrice": "true"}'
, 8
, '{"interestSubscription1": "USD1MD=", "interestSubscription2": "JPY1MD=", "interestAskField": "ASK", "interestBidField": "BID", "daysConstant1": "36500", "daysConstant2": "36500", "spotConstant": "2", "divideC16ByC15": "false", "subscriptionConfig": {"JPY=":{"identifier":"JPY="}, "USD1MD=":{"identifier":"USD1MD="}, "JPY1MD=":{"identifier":"JPY1MD="}}}'
, 1
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "PRIMACT_1", "lastTradeTimeField": "TIMACT", "subscriptionConfig": {"JPY=":{"identifier":"JPY="}}}'
, '{"dontCloseAfterXSecNoUpdate": "60", "timeZone": "GMT", "shouldBeClosedByMonitoring": "false"}'
, null
);

INSERT INTO MARKET_DATA_SOURCE_MAPS (
  ID
, MARKET_ID
, MARKET_DATA_SOURCE_ID
, HOUR_LEVEL_FORMULA_ID
, HOUR_LEVEL_FORMULA_PARAMS
, HOUR_CLOSING_FORMULA_ID
, HOUR_CLOSING_FORMULA_PARAMS
, DAY_CLOSING_FORMULA_ID
, DAY_CLOSING_FORMULA_PARAMS
, LONG_TERM_FORMULA_ID
, LONG_TERM_FORMULA_PARAMS
, REAL_LEVEL_FORMULA_ID
, REAL_LEVEL_FORMULA_PARAMS
, MARKET_PARAMS
, MARKET_DISABLE_CONDITIONS 
) VALUES (
SEQ_MARKET_DATA_SOURCE_MAPS.NEXTVAL
, 26
, 1
, 7
, '{"last5UpdatesFormulaConfig": {"field1Name": "ASK", "field2Name": "BID", "subscriptionConfig": {"ZAR=": {"identifier": "ZAR="}}}, "avgOf2FormulaConfig": {"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "PRIMACT_1", "lastTradeTimeField": "TIMACT", "subscriptionConfig": {"ZAR=FXAL":{"identifier":"ZAR=FXAL"}}}, "fXALParameter": "0"}'
, 1
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "PRIMACT_1", "lastTradeTimeField": "TIMACT", "subscriptionConfig": {"ZAR=":{"identifier":"ZAR="}}, "ignoreTradePrice": "true"}'
, 1
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "PRIMACT_1", "lastTradeTimeField": "TIMACT", "subscriptionConfig": {"ZAR=":{"identifier":"ZAR="}}, "ignoreTradePrice": "true"}'
, 8
, '{"interestSubscription1": "USD1MD=", "interestSubscription2": "ZAR1MD=", "interestAskField": "ASK", "interestBidField": "BID", "daysConstant1": "36500", "daysConstant2": "36500", "spotConstant": "2", "divideC16ByC15": "false", "subscriptionConfig": {"ZAR=":{"identifier":"ZAR="}, "USD1MD=":{"identifier":"USD1MD="}, "ZAR1MD=":{"identifier":"ZAR1MD="}}}'
, 1
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "PRIMACT_1", "lastTradeTimeField": "TIMACT", "subscriptionConfig": {"ZAR=":{"identifier":"ZAR="}}}'
, '{"dontCloseAfterXSecNoUpdate": "60", "timeZone": "GMT", "shouldBeClosedByMonitoring": "false"}'
, null
);

INSERT INTO MARKET_DATA_SOURCE_MAPS (
  ID
, MARKET_ID
, MARKET_DATA_SOURCE_ID
, HOUR_LEVEL_FORMULA_ID
, HOUR_LEVEL_FORMULA_PARAMS
, HOUR_CLOSING_FORMULA_ID
, HOUR_CLOSING_FORMULA_PARAMS
, DAY_CLOSING_FORMULA_ID
, DAY_CLOSING_FORMULA_PARAMS
, LONG_TERM_FORMULA_ID
, LONG_TERM_FORMULA_PARAMS
, REAL_LEVEL_FORMULA_ID
, REAL_LEVEL_FORMULA_PARAMS
, MARKET_PARAMS
, MARKET_DISABLE_CONDITIONS 
) VALUES (
SEQ_MARKET_DATA_SOURCE_MAPS.NEXTVAL
, 287
, 1
, 7
, '{"last5UpdatesFormulaConfig": {"field1Name": "ASK", "field2Name": "BID", "subscriptionConfig": {"EURJPY=": {"identifier": "EURJPY="}}}, "avgOf2FormulaConfig": {"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "PRIMACT_1", "lastTradeTimeField": "TIMACT", "subscriptionConfig": {"EURJPY=FXAL":{"identifier":"EURJPY=FXAL"}}}, "fXALParameter": "0"}'
, 1
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "PRIMACT_1", "lastTradeTimeField": "TIMACT", "subscriptionConfig": {"EURJPY=":{"identifier":"EURJPY="}}, "ignoreTradePrice": "true"}'
, 1
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "PRIMACT_1", "lastTradeTimeField": "TIMACT", "subscriptionConfig": {"EURJPY=":{"identifier":"EURJPY="}}, "ignoreTradePrice": "true"}'
, 8
, '{"interestSubscription1": "EUR1MD=", "interestSubscription2": "JPY1MD=", "interestAskField": "ASK", "interestBidField": "BID", "daysConstant1": "36500", "daysConstant2": "36500", "spotConstant": "2", "divideC16ByC15": "false", "subscriptionConfig": {"EURJPY=":{"identifier":"EURJPY="}, "EUR1MD=":{"identifier":"EUR1MD="}, "JPY1MD=":{"identifier":"JPY1MD="}}}'
, 1
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "PRIMACT_1", "lastTradeTimeField": "TIMACT", "subscriptionConfig": {"EURJPY=":{"identifier":"EURJPY="}}}'
, '{"dontCloseAfterXSecNoUpdate": "60", "timeZone": "GMT", "shouldBeClosedByMonitoring": "false"}'
, null
);

INSERT INTO MARKET_DATA_SOURCE_MAPS (
  ID
, MARKET_ID
, MARKET_DATA_SOURCE_ID
, HOUR_LEVEL_FORMULA_ID
, HOUR_LEVEL_FORMULA_PARAMS
, HOUR_CLOSING_FORMULA_ID
, HOUR_CLOSING_FORMULA_PARAMS
, DAY_CLOSING_FORMULA_ID
, DAY_CLOSING_FORMULA_PARAMS
, LONG_TERM_FORMULA_ID
, LONG_TERM_FORMULA_PARAMS
, REAL_LEVEL_FORMULA_ID
, REAL_LEVEL_FORMULA_PARAMS
, MARKET_PARAMS
, MARKET_DISABLE_CONDITIONS 
) VALUES (
SEQ_MARKET_DATA_SOURCE_MAPS.NEXTVAL
, 289
, 1
, 7
, '{"last5UpdatesFormulaConfig": {"field1Name": "ASK", "field2Name": "BID", "subscriptionConfig": {"AUD=": {"identifier": "AUD="}}}, "avgOf2FormulaConfig": {"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "PRIMACT_1", "lastTradeTimeField": "TIMACT", "subscriptionConfig": {"AUD=FXAL":{"identifier":"AUD=FXAL"}}}, "fXALParameter": "0"}'
, 1
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "PRIMACT_1", "lastTradeTimeField": "TIMACT", "subscriptionConfig": {"AUD=":{"identifier":"AUD="}}, "ignoreTradePrice": "true"}'
, 1
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "PRIMACT_1", "lastTradeTimeField": "TIMACT", "subscriptionConfig": {"AUD=":{"identifier":"AUD="}}, "ignoreTradePrice": "true"}'
, 8
, '{"interestSubscription1": "USD1MD=", "interestSubscription2": "AUD1MD=", "interestAskField": "ASK", "interestBidField": "BID", "daysConstant1": "36500", "daysConstant2": "36500", "spotConstant": "2", "divideC16ByC15": "true", "subscriptionConfig": {"AUD=":{"identifier":"AUD="}, "USD1MD=":{"identifier":"USD1MD="}, "AUD1MD=":{"identifier":"AUD1MD="}}}'
, 1
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "PRIMACT_1", "lastTradeTimeField": "TIMACT", "subscriptionConfig": {"AUD=":{"identifier":"AUD="}}}'
, '{"dontCloseAfterXSecNoUpdate": "60", "timeZone": "GMT", "shouldBeClosedByMonitoring": "false"}'
, null
);

INSERT INTO MARKET_DATA_SOURCE_MAPS (
  ID
, MARKET_ID
, MARKET_DATA_SOURCE_ID
, HOUR_LEVEL_FORMULA_ID
, HOUR_LEVEL_FORMULA_PARAMS
, HOUR_CLOSING_FORMULA_ID
, HOUR_CLOSING_FORMULA_PARAMS
, DAY_CLOSING_FORMULA_ID
, DAY_CLOSING_FORMULA_PARAMS
, LONG_TERM_FORMULA_ID
, LONG_TERM_FORMULA_PARAMS
, REAL_LEVEL_FORMULA_ID
, REAL_LEVEL_FORMULA_PARAMS
, MARKET_PARAMS
, MARKET_DISABLE_CONDITIONS 
) VALUES (
SEQ_MARKET_DATA_SOURCE_MAPS.NEXTVAL
, 290
, 1
, 7
, '{"last5UpdatesFormulaConfig": {"field1Name": "ASK", "field2Name": "BID", "subscriptionConfig": {"NZD=": {"identifier": "NZD="}}}, "avgOf2FormulaConfig": {"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "PRIMACT_1", "lastTradeTimeField": "TIMACT", "subscriptionConfig": {"NZD=FXAL":{"identifier":"NZD=FXAL"}}}, "fXALParameter": "0"}'
, 1
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "PRIMACT_1", "lastTradeTimeField": "TIMACT", "subscriptionConfig": {"NZD=":{"identifier":"NZD="}}, "ignoreTradePrice": "true"}'
, 1
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "PRIMACT_1", "lastTradeTimeField": "TIMACT", "subscriptionConfig": {"NZD=":{"identifier":"NZD="}}, "ignoreTradePrice": "true"}'
, 8
, '{"interestSubscription1": "USD1MD=", "interestSubscription2": "NZD1MD=", "interestAskField": "ASK", "interestBidField": "BID", "daysConstant1": "36500", "daysConstant2": "36500", "spotConstant": "2", "divideC16ByC15": "true", "subscriptionConfig": {"NZD=":{"identifier":"NZD="}, "USD1MD=":{"identifier":"USD1MD="}, "NZD1MD=":{"identifier":"NZD1MD="}}}'
, 1
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "PRIMACT_1", "lastTradeTimeField": "TIMACT", "subscriptionConfig": {"NZD=":{"identifier":"NZD="}}}'
, '{"dontCloseAfterXSecNoUpdate": "60", "timeZone": "GMT", "shouldBeClosedByMonitoring": "false"}'
, null
);

INSERT INTO MARKET_DATA_SOURCE_MAPS (
  ID
, MARKET_ID
, MARKET_DATA_SOURCE_ID
, HOUR_LEVEL_FORMULA_ID
, HOUR_LEVEL_FORMULA_PARAMS
, HOUR_CLOSING_FORMULA_ID
, HOUR_CLOSING_FORMULA_PARAMS
, DAY_CLOSING_FORMULA_ID
, DAY_CLOSING_FORMULA_PARAMS
, LONG_TERM_FORMULA_ID
, LONG_TERM_FORMULA_PARAMS
, REAL_LEVEL_FORMULA_ID
, REAL_LEVEL_FORMULA_PARAMS
, MARKET_PARAMS
, MARKET_DISABLE_CONDITIONS 
) VALUES (
SEQ_MARKET_DATA_SOURCE_MAPS.NEXTVAL
, 291
, 1
, 7
, '{"last5UpdatesFormulaConfig": {"field1Name": "ASK", "field2Name": "BID", "subscriptionConfig": {"EURGBP=": {"identifier": "EURGBP="}}}, "avgOf2FormulaConfig": {"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "PRIMACT_1", "lastTradeTimeField": "TIMACT", "subscriptionConfig": {"EURGBP=FXAL":{"identifier":"EURGBP=FXAL"}}}, "fXALParameter": "0"}'
, 1
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "PRIMACT_1", "lastTradeTimeField": "TIMACT", "subscriptionConfig": {"EURGBP=":{"identifier":"EURGBP="}}, "ignoreTradePrice": "true"}'
, 1
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "PRIMACT_1", "lastTradeTimeField": "TIMACT", "subscriptionConfig": {"EURGBP=":{"identifier":"EURGBP="}}, "ignoreTradePrice": "true"}'
, 8
, '{"interestSubscription1": "EUR1MD=", "interestSubscription2": "GBP1MD=", "interestAskField": "ASK", "interestBidField": "BID", "daysConstant1": "36500", "daysConstant2": "36500", "spotConstant": "2", "divideC16ByC15": "false", "subscriptionConfig": {"EURGBP=":{"identifier":"EURGBP="}, "EUR1MD=":{"identifier":"EUR1MD="}, "GBP1MD=":{"identifier":"GBP1MD="}}}'
, 1
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "PRIMACT_1", "lastTradeTimeField": "TIMACT", "subscriptionConfig": {"EURGBP=":{"identifier":"EURGBP="}}}'
, '{"dontCloseAfterXSecNoUpdate": "60", "timeZone": "GMT", "shouldBeClosedByMonitoring": "false"}'
, null
);

INSERT INTO MARKET_DATA_SOURCE_MAPS (
  ID
, MARKET_ID
, MARKET_DATA_SOURCE_ID
, HOUR_LEVEL_FORMULA_ID
, HOUR_LEVEL_FORMULA_PARAMS
, HOUR_CLOSING_FORMULA_ID
, HOUR_CLOSING_FORMULA_PARAMS
, DAY_CLOSING_FORMULA_ID
, DAY_CLOSING_FORMULA_PARAMS
, LONG_TERM_FORMULA_ID
, LONG_TERM_FORMULA_PARAMS
, REAL_LEVEL_FORMULA_ID
, REAL_LEVEL_FORMULA_PARAMS
, MARKET_PARAMS
, MARKET_DISABLE_CONDITIONS 
) VALUES (
SEQ_MARKET_DATA_SOURCE_MAPS.NEXTVAL
, 351
, 1
, 7
, '{"last5UpdatesFormulaConfig": {"field1Name": "ASK", "field2Name": "BID", "subscriptionConfig": {"GBPJPY=": {"identifier": "GBPJPY="}}}, "avgOf2FormulaConfig": {"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "PRIMACT_1", "lastTradeTimeField": "TIMACT", "subscriptionConfig": {"GBPJPY=FXAL":{"identifier":"GBPJPY=FXAL"}}}, "fXALParameter": "0"}'
, 1
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "PRIMACT_1", "lastTradeTimeField": "TIMACT", "subscriptionConfig": {"GBPJPY=":{"identifier":"GBPJPY="}}, "ignoreTradePrice": "true"}'
, 1
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "PRIMACT_1", "lastTradeTimeField": "TIMACT", "subscriptionConfig": {"GBPJPY=":{"identifier":"GBPJPY="}}, "ignoreTradePrice": "true"}'
, 8
, '{"interestSubscription1": "GBP1MD=", "interestSubscription2": "JPY1MD=", "interestAskField": "ASK", "interestBidField": "BID", "daysConstant1": "36500", "daysConstant2": "36500", "spotConstant": "2", "divideC16ByC15": "false", "subscriptionConfig": {"GBPJPY=":{"identifier":"GBPJPY="}, "GBP1MD=":{"identifier":"GBP1MD="}, "JPY1MD=":{"identifier":"JPY1MD="}}}'
, 1
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "PRIMACT_1", "lastTradeTimeField": "TIMACT", "subscriptionConfig": {"GBPJPY=":{"identifier":"GBPJPY="}}}'
, '{"dontCloseAfterXSecNoUpdate": "60", "timeZone": "GMT", "shouldBeClosedByMonitoring": "false"}'
, null
);

INSERT INTO MARKET_DATA_SOURCE_MAPS (
  ID
, MARKET_ID
, MARKET_DATA_SOURCE_ID
, HOUR_LEVEL_FORMULA_ID
, HOUR_LEVEL_FORMULA_PARAMS
, HOUR_CLOSING_FORMULA_ID
, HOUR_CLOSING_FORMULA_PARAMS
, DAY_CLOSING_FORMULA_ID
, DAY_CLOSING_FORMULA_PARAMS
, LONG_TERM_FORMULA_ID
, LONG_TERM_FORMULA_PARAMS
, REAL_LEVEL_FORMULA_ID
, REAL_LEVEL_FORMULA_PARAMS
, MARKET_PARAMS
, MARKET_DISABLE_CONDITIONS 
) VALUES (
SEQ_MARKET_DATA_SOURCE_MAPS.NEXTVAL
, 352
, 1
, 7
, '{"last5UpdatesFormulaConfig": {"field1Name": "ASK", "field2Name": "BID", "subscriptionConfig": {"CHF=": {"identifier": "CHF="}}}, "avgOf2FormulaConfig": {"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "PRIMACT_1", "lastTradeTimeField": "TIMACT", "subscriptionConfig": {"CHF=FXAL":{"identifier":"CHF=FXAL"}}}, "fXALParameter": "0"}'
, 1
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "PRIMACT_1", "lastTradeTimeField": "TIMACT", "subscriptionConfig": {"CHF=":{"identifier":"CHF="}}, "ignoreTradePrice": "true"}'
, 1
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "PRIMACT_1", "lastTradeTimeField": "TIMACT", "subscriptionConfig": {"CHF=":{"identifier":"CHF="}}, "ignoreTradePrice": "true"}'
, 8
, '{"interestSubscription1": "USD1MD=", "interestSubscription2": "CHF1MD=", "interestAskField": "ASK", "interestBidField": "BID", "daysConstant1": "36500", "daysConstant2": "36500", "spotConstant": "2", "divideC16ByC15": "false", "subscriptionConfig": {"CHF=":{"identifier":"CHF="}, "USD1MD=":{"identifier":"USD1MD="}, "CHF1MD=":{"identifier":"CHF1MD="}}}'
, 1
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "PRIMACT_1", "lastTradeTimeField": "TIMACT", "subscriptionConfig": {"CHF=":{"identifier":"CHF="}}}'
, '{"dontCloseAfterXSecNoUpdate": "60", "timeZone": "GMT", "shouldBeClosedByMonitoring": "false"}'
, null
);

INSERT INTO MARKET_DATA_SOURCE_MAPS (
  ID
, MARKET_ID
, MARKET_DATA_SOURCE_ID
, HOUR_LEVEL_FORMULA_ID
, HOUR_LEVEL_FORMULA_PARAMS
, HOUR_CLOSING_FORMULA_ID
, HOUR_CLOSING_FORMULA_PARAMS
, DAY_CLOSING_FORMULA_ID
, DAY_CLOSING_FORMULA_PARAMS
, LONG_TERM_FORMULA_ID
, LONG_TERM_FORMULA_PARAMS
, REAL_LEVEL_FORMULA_ID
, REAL_LEVEL_FORMULA_PARAMS
, MARKET_PARAMS
, MARKET_DISABLE_CONDITIONS 
) VALUES (
SEQ_MARKET_DATA_SOURCE_MAPS.NEXTVAL
, 604
, 1
, 7
, '{"last5UpdatesFormulaConfig": {"field1Name": "ASK", "field2Name": "BID", "subscriptionConfig": {"RUB=": {"identifier": "RUB="}}}, "avgOf2FormulaConfig": {"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "PRIMACT_1", "lastTradeTimeField": "TIMACT", "subscriptionConfig": {"RUB=FXAL":{"identifier":"RUB=FXAL"}}}, "fXALParameter": "0"}'
, 1
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "PRIMACT_1", "lastTradeTimeField": "TIMACT", "subscriptionConfig": {"RUB=":{"identifier":"RUB="}}, "ignoreTradePrice": "true"}'
, 1
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "PRIMACT_1", "lastTradeTimeField": "TIMACT", "subscriptionConfig": {"RUB=":{"identifier":"RUB="}}, "ignoreTradePrice": "true"}'
, 8
, '{"interestSubscription1": "USD1MD=", "interestSubscription2": "RUB1MD=", "interestAskField": "ASK", "interestBidField": "BID", "daysConstant1": "36500", "daysConstant2": "36500", "spotConstant": "2", "divideC16ByC15": "false", "subscriptionConfig": {"RUB=":{"identifier":"RUB="}, "USD1MD=":{"identifier":"USD1MD="}, "RUB1MD=":{"identifier":"RUB1MD="}}}'
, 1
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "PRIMACT_1", "lastTradeTimeField": "TIMACT", "subscriptionConfig": {"RUB=":{"identifier":"RUB="}}}'
, '{"dontCloseAfterXSecNoUpdate": "60", "timeZone": "GMT", "shouldBeClosedByMonitoring": "false"}'
, null
);

INSERT INTO MARKET_DATA_SOURCE_MAPS (
  ID
, MARKET_ID
, MARKET_DATA_SOURCE_ID
, HOUR_LEVEL_FORMULA_ID
, HOUR_LEVEL_FORMULA_PARAMS
, HOUR_CLOSING_FORMULA_ID
, HOUR_CLOSING_FORMULA_PARAMS
, DAY_CLOSING_FORMULA_ID
, DAY_CLOSING_FORMULA_PARAMS
, LONG_TERM_FORMULA_ID
, LONG_TERM_FORMULA_PARAMS
, REAL_LEVEL_FORMULA_ID
, REAL_LEVEL_FORMULA_PARAMS
, MARKET_PARAMS
, MARKET_DISABLE_CONDITIONS 
) VALUES (
SEQ_MARKET_DATA_SOURCE_MAPS.NEXTVAL
, 619
, 1
, 7
, '{"last5UpdatesFormulaConfig": {"field1Name": "ASK", "field2Name": "BID", "subscriptionConfig": {"CNY=": {"identifier": "CNY="}}}, "avgOf2FormulaConfig": {"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "PRIMACT_1", "lastTradeTimeField": "TIMACT", "subscriptionConfig": {"CNY=FXAL":{"identifier":"CNY=FXAL"}}}, "fXALParameter": "0"}'
, 1
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "PRIMACT_1", "lastTradeTimeField": "TIMACT", "subscriptionConfig": {"CNY=":{"identifier":"CNY="}}, "ignoreTradePrice": "true"}'
, 1
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "PRIMACT_1", "lastTradeTimeField": "TIMACT", "subscriptionConfig": {"CNY=":{"identifier":"CNY="}}, "ignoreTradePrice": "true"}'
, 8
, '{"interestSubscription1": "USD1MD=", "interestSubscription2": "CNY1MD=", "interestAskField": "ASK", "interestBidField": "BID", "daysConstant1": "36500", "daysConstant2": "36500", "spotConstant": "2", "divideC16ByC15": "false", "subscriptionConfig": {"CNY=":{"identifier":"CNY="}, "USD1MD=":{"identifier":"USD1MD="}, "CNY1MD=":{"identifier":"CNY1MD="}}}'
, 1
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "PRIMACT_1", "lastTradeTimeField": "TIMACT", "subscriptionConfig": {"CNY=":{"identifier":"CNY="}}}'
, '{"dontCloseAfterXSecNoUpdate": "60", "timeZone": "GMT", "shouldBeClosedByMonitoring": "false"}'
, null
);

INSERT INTO MARKET_DATA_SOURCE_MAPS (
  ID
, MARKET_ID
, MARKET_DATA_SOURCE_ID
, HOUR_LEVEL_FORMULA_ID
, HOUR_LEVEL_FORMULA_PARAMS
, HOUR_CLOSING_FORMULA_ID
, HOUR_CLOSING_FORMULA_PARAMS
, DAY_CLOSING_FORMULA_ID
, DAY_CLOSING_FORMULA_PARAMS
, LONG_TERM_FORMULA_ID
, LONG_TERM_FORMULA_PARAMS
, REAL_LEVEL_FORMULA_ID
, REAL_LEVEL_FORMULA_PARAMS
, MARKET_PARAMS
, MARKET_DISABLE_CONDITIONS 
) VALUES (
SEQ_MARKET_DATA_SOURCE_MAPS.NEXTVAL
, 637
, 1
, 7
, '{"last5UpdatesFormulaConfig": {"field1Name": "ASK", "field2Name": "BID", "subscriptionConfig": {"KRW=": {"identifier": "KRW="}}}, "avgOf2FormulaConfig": {"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "PRIMACT_1", "lastTradeTimeField": "TIMACT", "subscriptionConfig": {"KRW=FXAL":{"identifier":"KRW=FXAL"}}}, "fXALParameter": "0"}'
, 1
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "PRIMACT_1", "lastTradeTimeField": "TIMACT", "subscriptionConfig": {"KRW=":{"identifier":"KRW="}}, "ignoreTradePrice": "true"}'
, 1
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "PRIMACT_1", "lastTradeTimeField": "TIMACT", "subscriptionConfig": {"KRW=":{"identifier":"KRW="}}, "ignoreTradePrice": "true"}'
, 8
, '{"interestSubscription1": "USD1MD=", "interestSubscription2": "KRW1M=", "interestAskField": "ASK", "interestBidField": "BID", "daysConstant1": "36500", "daysConstant2": "36500", "spotConstant": "2", "divideC16ByC15": "false", "subscriptionConfig": {"KRW=":{"identifier":"KRW="}, "USD1MD=":{"identifier":"USD1MD="}, "KRW1M=":{"identifier":"KRW1M="}}}'
, 1
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "PRIMACT_1", "lastTradeTimeField": "TIMACT", "subscriptionConfig": {"KRW=":{"identifier":"KRW="}}}'
, '{"dontCloseAfterXSecNoUpdate": "60", "timeZone": "GMT", "shouldBeClosedByMonitoring": "false"}'
, null
);

INSERT INTO MARKET_DATA_SOURCE_MAPS (
  ID
, MARKET_ID
, MARKET_DATA_SOURCE_ID
, HOUR_LEVEL_FORMULA_ID
, HOUR_LEVEL_FORMULA_PARAMS
, HOUR_CLOSING_FORMULA_ID
, HOUR_CLOSING_FORMULA_PARAMS
, DAY_CLOSING_FORMULA_ID
, DAY_CLOSING_FORMULA_PARAMS
, LONG_TERM_FORMULA_ID
, LONG_TERM_FORMULA_PARAMS
, REAL_LEVEL_FORMULA_ID
, REAL_LEVEL_FORMULA_PARAMS
, MARKET_PARAMS
, MARKET_DISABLE_CONDITIONS 
) VALUES (
SEQ_MARKET_DATA_SOURCE_MAPS.NEXTVAL
, 651
, 1
, 1
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "PRIMACT_1", "lastTradeTimeField": "TIMACT", "subscriptionConfig": {"BGN=":{"identifier":"BGN="}}}'
, 1
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "PRIMACT_1", "lastTradeTimeField": "TIMACT", "subscriptionConfig": {"BGN=":{"identifier":"BGN="}}, "ignoreTradePrice": "true"}'
, 1
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "PRIMACT_1", "lastTradeTimeField": "TIMACT", "subscriptionConfig": {"BGN=":{"identifier":"BGN="}}, "ignoreTradePrice": "true"}'
, null
, null
, 1
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "PRIMACT_1", "lastTradeTimeField": "TIMACT", "subscriptionConfig": {"BGN=":{"identifier":"BGN="}}}'
, '{"dontCloseAfterXSecNoUpdate": "60", "timeZone": "GMT", "shouldBeClosedByMonitoring": "true"}'
, null
);

INSERT INTO MARKET_DATA_SOURCE_MAPS (
  ID
, MARKET_ID
, MARKET_DATA_SOURCE_ID
, HOUR_LEVEL_FORMULA_ID
, HOUR_LEVEL_FORMULA_PARAMS
, HOUR_CLOSING_FORMULA_ID
, HOUR_CLOSING_FORMULA_PARAMS
, DAY_CLOSING_FORMULA_ID
, DAY_CLOSING_FORMULA_PARAMS
, LONG_TERM_FORMULA_ID
, LONG_TERM_FORMULA_PARAMS
, REAL_LEVEL_FORMULA_ID
, REAL_LEVEL_FORMULA_PARAMS
, MARKET_PARAMS
, MARKET_DISABLE_CONDITIONS 
) VALUES (
SEQ_MARKET_DATA_SOURCE_MAPS.NEXTVAL
, 661
, 1
, 7
, '{"last5UpdatesFormulaConfig": {"field1Name": "ASK", "field2Name": "BID", "subscriptionConfig": {"AUDJPY=": {"identifier": "AUDJPY="}}}, "avgOf2FormulaConfig": {"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "PRIMACT_1", "lastTradeTimeField": "TIMACT", "subscriptionConfig": {"AUDJPY=FXAL":{"identifier":"AUDJPY=FXAL"}}}, "fXALParameter": "0"}'
, 1
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "PRIMACT_1", "lastTradeTimeField": "TIMACT", "subscriptionConfig": {"AUDJPY=":{"identifier":"AUDJPY="}}, "ignoreTradePrice": "true"}'
, 1
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "PRIMACT_1", "lastTradeTimeField": "TIMACT", "subscriptionConfig": {"AUDJPY=":{"identifier":"AUDJPY="}}, "ignoreTradePrice": "true"}'
, 8
, '{"interestSubscription1": "AUD1MD=", "interestSubscription2": "JPY1MD=", "interestAskField": "ASK", "interestBidField": "BID", "daysConstant1": "36500", "daysConstant2": "36500", "spotConstant": "2", "divideC16ByC15": "true", "subscriptionConfig": {"AUDJPY=":{"identifier":"AUDJPY="}, "AUD1MD=":{"identifier":"AUD1MD="}, "JPY1MD=":{"identifier":"JPY1MD="}}}'
, 1
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "PRIMACT_1", "lastTradeTimeField": "TIMACT", "subscriptionConfig": {"AUDJPY=":{"identifier":"AUDJPY="}}}'
, '{"dontCloseAfterXSecNoUpdate": "60", "timeZone": "GMT", "shouldBeClosedByMonitoring": "false"}'
, null
);

INSERT INTO MARKET_DATA_SOURCE_MAPS (
  ID
, MARKET_ID
, MARKET_DATA_SOURCE_ID
, HOUR_LEVEL_FORMULA_ID
, HOUR_LEVEL_FORMULA_PARAMS
, HOUR_CLOSING_FORMULA_ID
, HOUR_CLOSING_FORMULA_PARAMS
, DAY_CLOSING_FORMULA_ID
, DAY_CLOSING_FORMULA_PARAMS
, LONG_TERM_FORMULA_ID
, LONG_TERM_FORMULA_PARAMS
, REAL_LEVEL_FORMULA_ID
, REAL_LEVEL_FORMULA_PARAMS
, MARKET_PARAMS
, MARKET_DISABLE_CONDITIONS 
) VALUES (
SEQ_MARKET_DATA_SOURCE_MAPS.NEXTVAL
, 681
, 1
, 7
, '{"last5UpdatesFormulaConfig": {"field1Name": "ASK", "field2Name": "BID", "subscriptionConfig": {"SEK=": {"identifier": "SEK="}}}, "avgOf2FormulaConfig": {"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "PRIMACT_1", "lastTradeTimeField": "TIMACT", "subscriptionConfig": {"SEK=FXAL":{"identifier":"SEK=FXAL"}}}, "fXALParameter": "0"}'
, 1
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "PRIMACT_1", "lastTradeTimeField": "TIMACT", "subscriptionConfig": {"SEK=":{"identifier":"SEK="}}, "ignoreTradePrice": "true"}'
, 1
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "PRIMACT_1", "lastTradeTimeField": "TIMACT", "subscriptionConfig": {"SEK=":{"identifier":"SEK="}}, "ignoreTradePrice": "true"}'
, 8
, '{"interestSubscription1": "USD1MD=", "interestSubscription2": "SEK1MD=", "interestAskField": "ASK", "interestBidField": "BID", "daysConstant1": "36500", "daysConstant2": "36500", "spotConstant": "2", "divideC16ByC15": "false", "subscriptionConfig": {"SEK=":{"identifier":"SEK="}, "USD1MD=":{"identifier":"USD1MD="}, "SEK1MD=":{"identifier":"SEK1MD="}}}'
, 1
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "PRIMACT_1", "lastTradeTimeField": "TIMACT", "subscriptionConfig": {"SEK=":{"identifier":"SEK="}}}'
, '{"dontCloseAfterXSecNoUpdate": "60", "timeZone": "GMT", "shouldBeClosedByMonitoring": "false"}'
, null
);

INSERT INTO MARKET_DATA_SOURCE_MAPS (
  ID
, MARKET_ID
, MARKET_DATA_SOURCE_ID
, HOUR_LEVEL_FORMULA_ID
, HOUR_LEVEL_FORMULA_PARAMS
, HOUR_CLOSING_FORMULA_ID
, HOUR_CLOSING_FORMULA_PARAMS
, DAY_CLOSING_FORMULA_ID
, DAY_CLOSING_FORMULA_PARAMS
, LONG_TERM_FORMULA_ID
, LONG_TERM_FORMULA_PARAMS
, REAL_LEVEL_FORMULA_ID
, REAL_LEVEL_FORMULA_PARAMS
, MARKET_PARAMS
, MARKET_DISABLE_CONDITIONS 
) VALUES (
SEQ_MARKET_DATA_SOURCE_MAPS.NEXTVAL
, 682
, 1
, 7
, '{"last5UpdatesFormulaConfig": {"field1Name": "ASK", "field2Name": "BID", "subscriptionConfig": {"NOK=": {"identifier": "NOK="}}}, "avgOf2FormulaConfig": {"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "PRIMACT_1", "lastTradeTimeField": "TIMACT", "subscriptionConfig": {"NOK=FXAL":{"identifier":"NOK=FXAL"}}}, "fXALParameter": "0"}'
, 1
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "PRIMACT_1", "lastTradeTimeField": "TIMACT", "subscriptionConfig": {"NOK=":{"identifier":"NOK="}}, "ignoreTradePrice": "true"}'
, 1
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "PRIMACT_1", "lastTradeTimeField": "TIMACT", "subscriptionConfig": {"NOK=":{"identifier":"NOK="}}, "ignoreTradePrice": "true"}'
, 8
, '{"interestSubscription1": "USD1MD=", "interestSubscription2": "NOK1MD=", "interestAskField": "ASK", "interestBidField": "BID", "daysConstant1": "36500", "daysConstant2": "36500", "spotConstant": "2", "divideC16ByC15": "false", "subscriptionConfig": {"NOK=":{"identifier":"NOK="}, "USD1MD=":{"identifier":"USD1MD="}, "NOK1MD=":{"identifier":"NOK1MD="}}}'
, 1
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "PRIMACT_1", "lastTradeTimeField": "TIMACT", "subscriptionConfig": {"NOK=":{"identifier":"NOK="}}}'
, '{"dontCloseAfterXSecNoUpdate": "60", "timeZone": "GMT", "shouldBeClosedByMonitoring": "false"}'
, null
);

INSERT INTO MARKET_DATA_SOURCE_MAPS (
  ID
, MARKET_ID
, MARKET_DATA_SOURCE_ID
, HOUR_LEVEL_FORMULA_ID
, HOUR_LEVEL_FORMULA_PARAMS
, HOUR_CLOSING_FORMULA_ID
, HOUR_CLOSING_FORMULA_PARAMS
, DAY_CLOSING_FORMULA_ID
, DAY_CLOSING_FORMULA_PARAMS
, LONG_TERM_FORMULA_ID
, LONG_TERM_FORMULA_PARAMS
, REAL_LEVEL_FORMULA_ID
, REAL_LEVEL_FORMULA_PARAMS
, MARKET_PARAMS
, MARKET_DISABLE_CONDITIONS 
) VALUES (
SEQ_MARKET_DATA_SOURCE_MAPS.NEXTVAL
, 687
, 1
, 7
, '{"last5UpdatesFormulaConfig": {"field1Name": "ASK", "field2Name": "BID", "subscriptionConfig": {"CAD=": {"identifier": "CAD="}}}, "avgOf2FormulaConfig": {"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "PRIMACT_1", "lastTradeTimeField": "TIMACT", "subscriptionConfig": {"CAD=FXAL":{"identifier":"CAD=FXAL"}}}, "fXALParameter": "0"}'
, 1
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "PRIMACT_1", "lastTradeTimeField": "TIMACT", "subscriptionConfig": {"CAD=":{"identifier":"CAD="}}, "ignoreTradePrice": "true"}'
, 1
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "PRIMACT_1", "lastTradeTimeField": "TIMACT", "subscriptionConfig": {"CAD=":{"identifier":"CAD="}}, "ignoreTradePrice": "true"}'
, 8
, '{"interestSubscription1": "USD1MD=", "interestSubscription2": "CAD1MD=", "interestAskField": "ASK", "interestBidField": "BID", "daysConstant1": "36500", "daysConstant2": "36500", "spotConstant": "2", "divideC16ByC15": "false", "subscriptionConfig": {"CAD=":{"identifier":"CAD="}, "USD1MD=":{"identifier":"USD1MD="}, "CAD1MD=":{"identifier":"CAD1MD="}}}'
, 1
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "PRIMACT_1", "lastTradeTimeField": "TIMACT", "subscriptionConfig": {"CAD=":{"identifier":"CAD="}}}'
, '{"dontCloseAfterXSecNoUpdate": "60", "timeZone": "GMT", "shouldBeClosedByMonitoring": "false"}'
, null
);

INSERT INTO MARKET_DATA_SOURCE_MAPS (
  ID
, MARKET_ID
, MARKET_DATA_SOURCE_ID
, HOUR_LEVEL_FORMULA_ID
, HOUR_LEVEL_FORMULA_PARAMS
, HOUR_CLOSING_FORMULA_ID
, HOUR_CLOSING_FORMULA_PARAMS
, DAY_CLOSING_FORMULA_ID
, DAY_CLOSING_FORMULA_PARAMS
, LONG_TERM_FORMULA_ID
, LONG_TERM_FORMULA_PARAMS
, REAL_LEVEL_FORMULA_ID
, REAL_LEVEL_FORMULA_PARAMS
, MARKET_PARAMS
, MARKET_DISABLE_CONDITIONS 
) VALUES (
SEQ_MARKET_DATA_SOURCE_MAPS.NEXTVAL
, 692
, 1
, 7
, '{"last5UpdatesFormulaConfig": {"field1Name": "ASK", "field2Name": "BID", "subscriptionConfig": {"CZK=": {"identifier": "CZK="}}}, "avgOf2FormulaConfig": {"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "PRIMACT_1", "lastTradeTimeField": "TIMACT", "subscriptionConfig": {"CZK=FXAL":{"identifier":"CZK=FXAL"}}}, "fXALParameter": "0"}'
, 1
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "PRIMACT_1", "lastTradeTimeField": "TIMACT", "subscriptionConfig": {"CZK=":{"identifier":"CZK="}}, "ignoreTradePrice": "true"}'
, 1
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "PRIMACT_1", "lastTradeTimeField": "TIMACT", "subscriptionConfig": {"CZK=":{"identifier":"CZK="}}, "ignoreTradePrice": "true"}'
, 8
, '{"interestSubscription1": "USD1MD=", "interestSubscription2": "CZK1MD=", "interestAskField": "ASK", "interestBidField": "BID", "daysConstant1": "36500", "daysConstant2": "36500", "spotConstant": "2", "divideC16ByC15": "false", "subscriptionConfig": {"CZK=":{"identifier":"CZK="}, "USD1MD=":{"identifier":"USD1MD="}, "CZK1MD=":{"identifier":"CZK1MD="}}}'
, 1
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "PRIMACT_1", "lastTradeTimeField": "TIMACT", "subscriptionConfig": {"CZK=":{"identifier":"CZK="}}}'
, '{"dontCloseAfterXSecNoUpdate": "60", "timeZone": "GMT", "shouldBeClosedByMonitoring": "false"}'
, null
);

INSERT INTO MARKET_DATA_SOURCE_MAPS (
  ID
, MARKET_ID
, MARKET_DATA_SOURCE_ID
, HOUR_LEVEL_FORMULA_ID
, HOUR_LEVEL_FORMULA_PARAMS
, HOUR_CLOSING_FORMULA_ID
, HOUR_CLOSING_FORMULA_PARAMS
, DAY_CLOSING_FORMULA_ID
, DAY_CLOSING_FORMULA_PARAMS
, LONG_TERM_FORMULA_ID
, LONG_TERM_FORMULA_PARAMS
, REAL_LEVEL_FORMULA_ID
, REAL_LEVEL_FORMULA_PARAMS
, MARKET_PARAMS
, MARKET_DISABLE_CONDITIONS 
) VALUES (
SEQ_MARKET_DATA_SOURCE_MAPS.NEXTVAL
, 698
, 1
, 7
, '{"last5UpdatesFormulaConfig": {"field1Name": "ASK", "field2Name": "BID", "subscriptionConfig": {"DKK=": {"identifier": "DKK="}}}, "avgOf2FormulaConfig": {"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "PRIMACT_1", "lastTradeTimeField": "TIMACT", "subscriptionConfig": {"DKK=FXAL":{"identifier":"DKK=FXAL"}}}, "fXALParameter": "0"}'
, 1
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "PRIMACT_1", "lastTradeTimeField": "TIMACT", "subscriptionConfig": {"DKK=":{"identifier":"DKK="}}, "ignoreTradePrice": "true"}'
, 1
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "PRIMACT_1", "lastTradeTimeField": "TIMACT", "subscriptionConfig": {"DKK=":{"identifier":"DKK="}}, "ignoreTradePrice": "true"}'
, 8
, '{"interestSubscription1": "USD1MD=", "interestSubscription2": "DKK1MD=", "interestAskField": "ASK", "interestBidField": "BID", "daysConstant1": "36500", "daysConstant2": "36500", "spotConstant": "2", "divideC16ByC15": "false", "subscriptionConfig": {"DKK=":{"identifier":"DKK="}, "USD1MD=":{"identifier":"USD1MD="}, "DKK1MD=":{"identifier":"DKK1MD="}}}'
, 1
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "PRIMACT_1", "lastTradeTimeField": "TIMACT", "subscriptionConfig": {"DKK=":{"identifier":"DKK="}}}'
, '{"dontCloseAfterXSecNoUpdate": "60", "timeZone": "GMT", "shouldBeClosedByMonitoring": "false"}'
, null
);
-- End of EUR= like currencies

-- GC like commodities
INSERT INTO MARKET_DATA_SOURCE_MAPS (
  ID
, MARKET_ID
, MARKET_DATA_SOURCE_ID
, HOUR_LEVEL_FORMULA_ID
, HOUR_LEVEL_FORMULA_PARAMS
, HOUR_CLOSING_FORMULA_ID
, HOUR_CLOSING_FORMULA_PARAMS
, DAY_CLOSING_FORMULA_ID
, DAY_CLOSING_FORMULA_PARAMS
, LONG_TERM_FORMULA_ID
, LONG_TERM_FORMULA_PARAMS
, REAL_LEVEL_FORMULA_ID
, REAL_LEVEL_FORMULA_PARAMS
, MARKET_PARAMS
, MARKET_DISABLE_CONDITIONS 
) VALUES (
SEQ_MARKET_DATA_SOURCE_MAPS.NEXTVAL
, 20
, 1
, 1
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "TRDPRC_1", "lastTradeTimeField": "TIMACT", "subscriptionConfig": {"XAU=ALL":{"identifier":"XAU=ALL","switchToFuture":"false","switchToFuturePrefixLength":"7"}}}'
, 1
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "TRDPRC_1", "lastTradeTimeField": "TIMACT", "subscriptionConfig": {"XAU=ALL":{"identifier":"XAU=ALL","switchToFuture":"false","switchToFuturePrefixLength":"7"}}, "ignoreTradePrice": "true"}'
, 1
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "TRDPRC_1", "lastTradeTimeField": "TIMACT", "subscriptionConfig": {"XAU=ALL":{"identifier":"XAU=ALL","switchToFuture":"false","switchToFuturePrefixLength":"7"}}, "ignoreTradePrice": "true"}'
, null
, null
, 1
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "TRDPRC_1", "lastTradeTimeField": "TIMACT", "subscriptionConfig": {"XAU=ALL":{"identifier":"XAU=ALL","switchToFuture":"false","switchToFuturePrefixLength":"7"}}}'
, '{"dontCloseAfterXSecNoUpdate": "240", "timeZone": "GMT", "shouldBeClosedByMonitoring": "true"}'
, null
);

INSERT INTO MARKET_DATA_SOURCE_MAPS (
  ID
, MARKET_ID
, MARKET_DATA_SOURCE_ID
, HOUR_LEVEL_FORMULA_ID
, HOUR_LEVEL_FORMULA_PARAMS
, HOUR_CLOSING_FORMULA_ID
, HOUR_CLOSING_FORMULA_PARAMS
, DAY_CLOSING_FORMULA_ID
, DAY_CLOSING_FORMULA_PARAMS
, LONG_TERM_FORMULA_ID
, LONG_TERM_FORMULA_PARAMS
, REAL_LEVEL_FORMULA_ID
, REAL_LEVEL_FORMULA_PARAMS
, MARKET_PARAMS
, MARKET_DISABLE_CONDITIONS 
) VALUES (
SEQ_MARKET_DATA_SOURCE_MAPS.NEXTVAL
, 137
, 1
, 1
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "TRDPRC_1", "lastTradeTimeField": "TIMACT", "subscriptionConfig": {"XAG=":{"identifier":"XAG=","switchToFuture":"false","switchToFuturePrefixLength":"4"}}}'
, 1
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "TRDPRC_1", "lastTradeTimeField": "TIMACT", "subscriptionConfig": {"XAG=":{"identifier":"XAG=","switchToFuture":"false","switchToFuturePrefixLength":"4"}}, "ignoreTradePrice": "true"}'
, 1
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "TRDPRC_1", "lastTradeTimeField": "TIMACT", "subscriptionConfig": {"XAG=":{"identifier":"XAG=","switchToFuture":"false","switchToFuturePrefixLength":"4"}}, "ignoreTradePrice": "true"}'
, null
, null
, 1
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "TRDPRC_1", "lastTradeTimeField": "TIMACT", "subscriptionConfig": {"XAG=":{"identifier":"XAG=","switchToFuture":"false","switchToFuturePrefixLength":"4"}}}'
, '{"dontCloseAfterXSecNoUpdate": "240", "timeZone": "GMT", "shouldBeClosedByMonitoring": "true"}'
, null
);

INSERT INTO MARKET_DATA_SOURCE_MAPS (
  ID
, MARKET_ID
, MARKET_DATA_SOURCE_ID
, HOUR_LEVEL_FORMULA_ID
, HOUR_LEVEL_FORMULA_PARAMS
, HOUR_CLOSING_FORMULA_ID
, HOUR_CLOSING_FORMULA_PARAMS
, DAY_CLOSING_FORMULA_ID
, DAY_CLOSING_FORMULA_PARAMS
, LONG_TERM_FORMULA_ID
, LONG_TERM_FORMULA_PARAMS
, REAL_LEVEL_FORMULA_ID
, REAL_LEVEL_FORMULA_PARAMS
, MARKET_PARAMS
, MARKET_DISABLE_CONDITIONS 
) VALUES (
SEQ_MARKET_DATA_SOURCE_MAPS.NEXTVAL
, 685
, 1
, 1
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {"NGv1":{"identifier":"NGv1","switchToFuture":"true","switchToFuturePrefixLength":"2"}}}'
, 1
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {"NGv1":{"identifier":"NGv1","switchToFuture":"true","switchToFuturePrefixLength":"2"}}}'
, 1
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {"NGv1":{"identifier":"NGv1","switchToFuture":"true","switchToFuturePrefixLength":"2"}}}'
, null
, null
, 1
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {"NGv1":{"identifier":"NGv1","switchToFuture":"true","switchToFuturePrefixLength":"2"}}}'
, '{"dontCloseAfterXSecNoUpdate": "240", "timeZone": "GMT", "shouldBeClosedByMonitoring": "true"}'
, null
);

INSERT INTO MARKET_DATA_SOURCE_MAPS (
  ID
, MARKET_ID
, MARKET_DATA_SOURCE_ID
, HOUR_LEVEL_FORMULA_ID
, HOUR_LEVEL_FORMULA_PARAMS
, HOUR_CLOSING_FORMULA_ID
, HOUR_CLOSING_FORMULA_PARAMS
, DAY_CLOSING_FORMULA_ID
, DAY_CLOSING_FORMULA_PARAMS
, LONG_TERM_FORMULA_ID
, LONG_TERM_FORMULA_PARAMS
, REAL_LEVEL_FORMULA_ID
, REAL_LEVEL_FORMULA_PARAMS
, MARKET_PARAMS
, MARKET_DISABLE_CONDITIONS 
) VALUES (
SEQ_MARKET_DATA_SOURCE_MAPS.NEXTVAL
, 21
, 1
, 1
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {"CLv1":{"identifier":"CLv1","switchToFuture":"true","switchToFuturePrefixLength":"2"}}}'
, 3
, '{"field1Name": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {"CLv1":{"identifier":"CLv1","switchToFuture":"true","switchToFuturePrefixLength":"2"}}}'
, 3
, '{"field1Name": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {"CLv1":{"identifier":"CLv1","switchToFuture":"true","switchToFuturePrefixLength":"2"}}}'
, null
, null
, 3
, '{"field1Name": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {"CLv1":{"identifier":"CLv1","switchToFuture":"true","switchToFuturePrefixLength":"2"}}}'
, '{"dontCloseAfterXSecNoUpdate": "120", "timeZone": "GMT", "shouldBeClosedByMonitoring": "true"}'
, null
);
-- End of GC like commodities

-- C like stocks
-- TODO lastTradeTimeField conflict?
INSERT INTO MARKET_DATA_SOURCE_MAPS (
  ID
, MARKET_ID
, MARKET_DATA_SOURCE_ID
, HOUR_LEVEL_FORMULA_ID
, HOUR_LEVEL_FORMULA_PARAMS
, HOUR_CLOSING_FORMULA_ID
, HOUR_CLOSING_FORMULA_PARAMS
, DAY_CLOSING_FORMULA_ID
, DAY_CLOSING_FORMULA_PARAMS
, LONG_TERM_FORMULA_ID
, LONG_TERM_FORMULA_PARAMS
, REAL_LEVEL_FORMULA_ID
, REAL_LEVEL_FORMULA_PARAMS
, MARKET_PARAMS
, MARKET_DISABLE_CONDITIONS 
) VALUES (
SEQ_MARKET_DATA_SOURCE_MAPS.NEXTVAL
, 128
, 1
, 5
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {"C":{"identifier":"C"}}}'
, 2
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {"C":{"identifier":"C"}}}'
, 3
, '{"field1Name": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {"C":{"identifier":"C"}}}'
, 9
, '{"interestSubscription": "USD1MD=", "interestAskField": "ASK", "interestBidField": "BID", "daysConstant": "360", "subscriptionConfig": {"C":{"identifier":"C"}, "USD1MD=":{"identifier":"USD1MD="}}}'
, 2
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {"C":{"identifier":"C"}}}'
, '{"dontCloseAfterXSecNoUpdate": "60", "timeZone": "GMT", "shouldBeClosedByMonitoring": "true"}'
, q'{{"com.anyoption.service.level.condition.AskBidDistanceMarketDisableCondition": {'subscriptionName': 'C', 'askField': 'ASK', 'bidField': 'BID', 'lastField': 'TRDPRC_1', 'distance': '0.02'}, "com.anyoption.service.level.condition.AskBidInverseMarketDisableCondition": {'subscriptionName': 'C', 'askField': 'ASK', 'bidField': 'BID'}}}'
);

INSERT INTO MARKET_DATA_SOURCE_MAPS (
  ID
, MARKET_ID
, MARKET_DATA_SOURCE_ID
, HOUR_LEVEL_FORMULA_ID
, HOUR_LEVEL_FORMULA_PARAMS
, HOUR_CLOSING_FORMULA_ID
, HOUR_CLOSING_FORMULA_PARAMS
, DAY_CLOSING_FORMULA_ID
, DAY_CLOSING_FORMULA_PARAMS
, LONG_TERM_FORMULA_ID
, LONG_TERM_FORMULA_PARAMS
, REAL_LEVEL_FORMULA_ID
, REAL_LEVEL_FORMULA_PARAMS
, MARKET_PARAMS
, MARKET_DISABLE_CONDITIONS 
) VALUES (
SEQ_MARKET_DATA_SOURCE_MAPS.NEXTVAL
, 694
, 1
, 5
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {"KO":{"identifier":"KO"}}}'
, 2
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {"KO":{"identifier":"KO"}}}'
, 3
, '{"field1Name": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {"KO":{"identifier":"KO"}}}'
, 9
, '{"interestSubscription": "USD1MD=", "interestAskField": "ASK", "interestBidField": "BID", "daysConstant": "360", "subscriptionConfig": {"KO":{"identifier":"KO"}, "USD1MD=":{"identifier":"USD1MD="}}}'
, 2
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {"KO":{"identifier":"KO"}}}'
, '{"dontCloseAfterXSecNoUpdate": "60", "timeZone": "GMT", "shouldBeClosedByMonitoring": "true"}'
, q'{{"com.anyoption.service.level.condition.AskBidDistanceMarketDisableCondition": {'subscriptionName': 'KO', 'askField': 'ASK', 'bidField': 'BID', 'lastField': 'TRDPRC_1', 'distance': '0.02'}, "com.anyoption.service.level.condition.AskBidInverseMarketDisableCondition": {'subscriptionName': 'KO', 'askField': 'ASK', 'bidField': 'BID'}}}'
);

INSERT INTO MARKET_DATA_SOURCE_MAPS (
  ID
, MARKET_ID
, MARKET_DATA_SOURCE_ID
, HOUR_LEVEL_FORMULA_ID
, HOUR_LEVEL_FORMULA_PARAMS
, HOUR_CLOSING_FORMULA_ID
, HOUR_CLOSING_FORMULA_PARAMS
, DAY_CLOSING_FORMULA_ID
, DAY_CLOSING_FORMULA_PARAMS
, LONG_TERM_FORMULA_ID
, LONG_TERM_FORMULA_PARAMS
, REAL_LEVEL_FORMULA_ID
, REAL_LEVEL_FORMULA_PARAMS
, MARKET_PARAMS
, MARKET_DISABLE_CONDITIONS 
) VALUES (
SEQ_MARKET_DATA_SOURCE_MAPS.NEXTVAL
, 713
, 1
, 5
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {"PG":{"identifier":"PG"}}}'
, 2
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {"PG":{"identifier":"PG"}}}'
, 3
, '{"field1Name": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {"PG":{"identifier":"PG"}}}'
, 9
, '{"interestSubscription": "USD1MD=", "interestAskField": "ASK", "interestBidField": "BID", "daysConstant": "360", "subscriptionConfig": {"PG":{"identifier":"PG"}, "USD1MD=":{"identifier":"USD1MD="}}}'
, 2
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {"PG":{"identifier":"PG"}}}'
, '{"dontCloseAfterXSecNoUpdate": "60", "timeZone": "GMT", "shouldBeClosedByMonitoring": "true"}'
, q'{{"com.anyoption.service.level.condition.AskBidDistanceMarketDisableCondition": {'subscriptionName': 'PG', 'askField': 'ASK', 'bidField': 'BID', 'lastField': 'TRDPRC_1', 'distance': '0.02'}, "com.anyoption.service.level.condition.AskBidInverseMarketDisableCondition": {'subscriptionName': 'PG', 'askField': 'ASK', 'bidField': 'BID'}}}'
);

INSERT INTO MARKET_DATA_SOURCE_MAPS (
  ID
, MARKET_ID
, MARKET_DATA_SOURCE_ID
, HOUR_LEVEL_FORMULA_ID
, HOUR_LEVEL_FORMULA_PARAMS
, HOUR_CLOSING_FORMULA_ID
, HOUR_CLOSING_FORMULA_PARAMS
, DAY_CLOSING_FORMULA_ID
, DAY_CLOSING_FORMULA_PARAMS
, LONG_TERM_FORMULA_ID
, LONG_TERM_FORMULA_PARAMS
, REAL_LEVEL_FORMULA_ID
, REAL_LEVEL_FORMULA_PARAMS
, MARKET_PARAMS
, MARKET_DISABLE_CONDITIONS 
) VALUES (
SEQ_MARKET_DATA_SOURCE_MAPS.NEXTVAL
, 499
, 1
, 5
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {"MCD":{"identifier":"MCD"}}}'
, 2
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {"MCD":{"identifier":"MCD"}}}'
, 3
, '{"field1Name": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {"MCD":{"identifier":"MCD"}}}'
, 9
, '{"interestSubscription": "USD1MD=", "interestAskField": "ASK", "interestBidField": "BID", "daysConstant": "360", "subscriptionConfig": {"MCD":{"identifier":"MCD"}, "USD1MD=":{"identifier":"USD1MD="}}}'
, 2
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {"MCD":{"identifier":"MCD"}}}'
, '{"dontCloseAfterXSecNoUpdate": "60", "timeZone": "GMT", "shouldBeClosedByMonitoring": "true"}'
, q'{{"com.anyoption.service.level.condition.AskBidDistanceMarketDisableCondition": {'subscriptionName': 'MCD', 'askField': 'ASK', 'bidField': 'BID', 'lastField': 'TRDPRC_1', 'distance': '0.02'}, "com.anyoption.service.level.condition.AskBidInverseMarketDisableCondition": {'subscriptionName': 'MCD', 'askField': 'ASK', 'bidField': 'BID'}}}'
);

INSERT INTO MARKET_DATA_SOURCE_MAPS (
  ID
, MARKET_ID
, MARKET_DATA_SOURCE_ID
, HOUR_LEVEL_FORMULA_ID
, HOUR_LEVEL_FORMULA_PARAMS
, HOUR_CLOSING_FORMULA_ID
, HOUR_CLOSING_FORMULA_PARAMS
, DAY_CLOSING_FORMULA_ID
, DAY_CLOSING_FORMULA_PARAMS
, LONG_TERM_FORMULA_ID
, LONG_TERM_FORMULA_PARAMS
, REAL_LEVEL_FORMULA_ID
, REAL_LEVEL_FORMULA_PARAMS
, MARKET_PARAMS
, MARKET_DISABLE_CONDITIONS 
) VALUES (
SEQ_MARKET_DATA_SOURCE_MAPS.NEXTVAL
, 696
, 1
, 5
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {"NKE":{"identifier":"NKE"}}}'
, 2
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {"NKE":{"identifier":"NKE"}}}'
, 3
, '{"field1Name": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {"NKE":{"identifier":"NKE"}}}'
, 9
, '{"interestSubscription": "USD1MD=", "interestAskField": "ASK", "interestBidField": "BID", "daysConstant": "360", "subscriptionConfig": {"NKE":{"identifier":"NKE"}, "USD1MD=":{"identifier":"USD1MD="}}}'
, 2
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {"NKE":{"identifier":"NKE"}}}'
, '{"dontCloseAfterXSecNoUpdate": "60", "timeZone": "GMT", "shouldBeClosedByMonitoring": "true"}'
, q'{{"com.anyoption.service.level.condition.AskBidDistanceMarketDisableCondition": {'subscriptionName': 'NKE', 'askField': 'ASK', 'bidField': 'BID', 'lastField': 'TRDPRC_1', 'distance': '0.02'}, "com.anyoption.service.level.condition.AskBidInverseMarketDisableCondition": {'subscriptionName': 'NKE', 'askField': 'ASK', 'bidField': 'BID'}}}'
);

INSERT INTO MARKET_DATA_SOURCE_MAPS (
  ID
, MARKET_ID
, MARKET_DATA_SOURCE_ID
, HOUR_LEVEL_FORMULA_ID
, HOUR_LEVEL_FORMULA_PARAMS
, HOUR_CLOSING_FORMULA_ID
, HOUR_CLOSING_FORMULA_PARAMS
, DAY_CLOSING_FORMULA_ID
, DAY_CLOSING_FORMULA_PARAMS
, LONG_TERM_FORMULA_ID
, LONG_TERM_FORMULA_PARAMS
, REAL_LEVEL_FORMULA_ID
, REAL_LEVEL_FORMULA_PARAMS
, MARKET_PARAMS
, MARKET_DISABLE_CONDITIONS 
) VALUES (
SEQ_MARKET_DATA_SOURCE_MAPS.NEXTVAL
, 229
, 1
, 5
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {"MS":{"identifier":"MS"}}}'
, 2
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {"MS":{"identifier":"MS"}}}'
, 3
, '{"field1Name": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {"MS":{"identifier":"MS"}}}'
, 9
, '{"interestSubscription": "USD1MD=", "interestAskField": "ASK", "interestBidField": "BID", "daysConstant": "360", "subscriptionConfig": {"MS":{"identifier":"MS"}, "USD1MD=":{"identifier":"USD1MD="}}}'
, 2
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {"MS":{"identifier":"MS"}}}'
, '{"dontCloseAfterXSecNoUpdate": "60", "timeZone": "GMT", "shouldBeClosedByMonitoring": "true"}'
, q'{{"com.anyoption.service.level.condition.AskBidDistanceMarketDisableCondition": {'subscriptionName': 'MS', 'askField': 'ASK', 'bidField': 'BID', 'lastField': 'TRDPRC_1', 'distance': '0.02'}, "com.anyoption.service.level.condition.AskBidInverseMarketDisableCondition": {'subscriptionName': 'MS', 'askField': 'ASK', 'bidField': 'BID'}}}'
);

INSERT INTO MARKET_DATA_SOURCE_MAPS (
  ID
, MARKET_ID
, MARKET_DATA_SOURCE_ID
, HOUR_LEVEL_FORMULA_ID
, HOUR_LEVEL_FORMULA_PARAMS
, HOUR_CLOSING_FORMULA_ID
, HOUR_CLOSING_FORMULA_PARAMS
, DAY_CLOSING_FORMULA_ID
, DAY_CLOSING_FORMULA_PARAMS
, LONG_TERM_FORMULA_ID
, LONG_TERM_FORMULA_PARAMS
, REAL_LEVEL_FORMULA_ID
, REAL_LEVEL_FORMULA_PARAMS
, MARKET_PARAMS
, MARKET_DISABLE_CONDITIONS 
) VALUES (
SEQ_MARKET_DATA_SOURCE_MAPS.NEXTVAL
, 298
, 1
, 5
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {"BBVA.MC":{"identifier":"BBVA.MC"}}}'
, 2
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {"BBVA.MC":{"identifier":"BBVA.MC"}}}'
, 3
, '{"field1Name": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {"BBVA.MC":{"identifier":"BBVA.MC"}}}'
, 9
, '{"interestSubscription": "EUR1MD=", "interestAskField": "ASK", "interestBidField": "BID", "daysConstant": "365", "subscriptionConfig": {"BBVA.MC":{"identifier":"BBVA.MC"}, "EUR1MD=":{"identifier":"EUR1MD="}}}'
, 2
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {"BBVA.MC":{"identifier":"BBVA.MC"}}}'
, '{"dontCloseAfterXSecNoUpdate": "120", "timeZone": "GMT", "shouldBeClosedByMonitoring": "true"}'
, q'{{"com.anyoption.service.level.condition.AskBidDistanceMarketDisableCondition": {'subscriptionName': 'BBVA.MC', 'askField': 'ASK', 'bidField': 'BID', 'lastField': 'TRDPRC_1', 'distance': '0.02'}, "com.anyoption.service.level.condition.AskBidInverseMarketDisableCondition": {'subscriptionName': 'BBVA.MC', 'askField': 'ASK', 'bidField': 'BID'}}}'
);

INSERT INTO MARKET_DATA_SOURCE_MAPS (
  ID
, MARKET_ID
, MARKET_DATA_SOURCE_ID
, HOUR_LEVEL_FORMULA_ID
, HOUR_LEVEL_FORMULA_PARAMS
, HOUR_CLOSING_FORMULA_ID
, HOUR_CLOSING_FORMULA_PARAMS
, DAY_CLOSING_FORMULA_ID
, DAY_CLOSING_FORMULA_PARAMS
, LONG_TERM_FORMULA_ID
, LONG_TERM_FORMULA_PARAMS
, REAL_LEVEL_FORMULA_ID
, REAL_LEVEL_FORMULA_PARAMS
, MARKET_PARAMS
, MARKET_DISABLE_CONDITIONS 
) VALUES (
SEQ_MARKET_DATA_SOURCE_MAPS.NEXTVAL
, 299
, 1
, 5
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {"TEF.MC":{"identifier":"TEF.MC"}}}'
, 2
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {"TEF.MC":{"identifier":"TEF.MC"}}}'
, 3
, '{"field1Name": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {"TEF.MC":{"identifier":"TEF.MC"}}}'
, 9
, '{"interestSubscription": "EUR1MD=", "interestAskField": "ASK", "interestBidField": "BID", "daysConstant": "365", "subscriptionConfig": {"TEF.MC":{"identifier":"TEF.MC"}, "EUR1MD=":{"identifier":"EUR1MD="}}}'
, 2
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {"TEF.MC":{"identifier":"TEF.MC"}}}'
, '{"dontCloseAfterXSecNoUpdate": "60", "timeZone": "GMT", "shouldBeClosedByMonitoring": "true"}'
, q'{{"com.anyoption.service.level.condition.AskBidDistanceMarketDisableCondition": {'subscriptionName': 'TEF.MC', 'askField': 'ASK', 'bidField': 'BID', 'lastField': 'TRDPRC_1', 'distance': '0.02'}, "com.anyoption.service.level.condition.AskBidInverseMarketDisableCondition": {'subscriptionName': 'TEF.MC', 'askField': 'ASK', 'bidField': 'BID'}}}'
);

INSERT INTO MARKET_DATA_SOURCE_MAPS (
  ID
, MARKET_ID
, MARKET_DATA_SOURCE_ID
, HOUR_LEVEL_FORMULA_ID
, HOUR_LEVEL_FORMULA_PARAMS
, HOUR_CLOSING_FORMULA_ID
, HOUR_CLOSING_FORMULA_PARAMS
, DAY_CLOSING_FORMULA_ID
, DAY_CLOSING_FORMULA_PARAMS
, LONG_TERM_FORMULA_ID
, LONG_TERM_FORMULA_PARAMS
, REAL_LEVEL_FORMULA_ID
, REAL_LEVEL_FORMULA_PARAMS
, MARKET_PARAMS
, MARKET_DISABLE_CONDITIONS 
) VALUES (
SEQ_MARKET_DATA_SOURCE_MAPS.NEXTVAL
, 543
, 1
, 5
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {"SAN.MC":{"identifier":"SAN.MC"}}}'
, 2
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {"SAN.MC":{"identifier":"SAN.MC"}}}'
, 3
, '{"field1Name": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {"SAN.MC":{"identifier":"SAN.MC"}}}'
, 9
, '{"interestSubscription": "EUR1MD=", "interestAskField": "ASK", "interestBidField": "BID", "daysConstant": "365", "subscriptionConfig": {"SAN.MC":{"identifier":"SAN.MC"}, "EUR1MD=":{"identifier":"EUR1MD="}}}'
, 2
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {"SAN.MC":{"identifier":"SAN.MC"}}}'
, '{"dontCloseAfterXSecNoUpdate": "60", "timeZone": "GMT", "shouldBeClosedByMonitoring": "true"}'
, q'{{"com.anyoption.service.level.condition.AskBidDistanceMarketDisableCondition": {'subscriptionName': 'SAN.MC', 'askField': 'ASK', 'bidField': 'BID', 'lastField': 'TRDPRC_1', 'distance': '0.02'}, "com.anyoption.service.level.condition.AskBidInverseMarketDisableCondition": {'subscriptionName': 'SAN.MC', 'askField': 'ASK', 'bidField': 'BID'}}}'
);

INSERT INTO MARKET_DATA_SOURCE_MAPS (
  ID
, MARKET_ID
, MARKET_DATA_SOURCE_ID
, HOUR_LEVEL_FORMULA_ID
, HOUR_LEVEL_FORMULA_PARAMS
, HOUR_CLOSING_FORMULA_ID
, HOUR_CLOSING_FORMULA_PARAMS
, DAY_CLOSING_FORMULA_ID
, DAY_CLOSING_FORMULA_PARAMS
, LONG_TERM_FORMULA_ID
, LONG_TERM_FORMULA_PARAMS
, REAL_LEVEL_FORMULA_ID
, REAL_LEVEL_FORMULA_PARAMS
, MARKET_PARAMS
, MARKET_DISABLE_CONDITIONS 
) VALUES (
SEQ_MARKET_DATA_SOURCE_MAPS.NEXTVAL
, 309
, 1
, 5
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {"JPM":{"identifier":"JPM"}}}'
, 2
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {"JPM":{"identifier":"JPM"}}}'
, 3
, '{"field1Name": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {"JPM":{"identifier":"JPM"}}}'
, 9
, '{"interestSubscription": "USD1MD=", "interestAskField": "ASK", "interestBidField": "BID", "daysConstant": "360", "subscriptionConfig": {"JPM":{"identifier":"JPM"}, "USD1MD=":{"identifier":"USD1MD="}}}'
, 2
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {"JPM":{"identifier":"JPM"}}}'
, '{"dontCloseAfterXSecNoUpdate": "60", "timeZone": "GMT", "shouldBeClosedByMonitoring": "true"}'
, q'{{"com.anyoption.service.level.condition.AskBidDistanceMarketDisableCondition": {'subscriptionName': 'JPM', 'askField': 'ASK', 'bidField': 'BID', 'lastField': 'TRDPRC_1', 'distance': '0.02'}, "com.anyoption.service.level.condition.AskBidInverseMarketDisableCondition": {'subscriptionName': 'JPM', 'askField': 'ASK', 'bidField': 'BID'}}}'
);

INSERT INTO MARKET_DATA_SOURCE_MAPS (
  ID
, MARKET_ID
, MARKET_DATA_SOURCE_ID
, HOUR_LEVEL_FORMULA_ID
, HOUR_LEVEL_FORMULA_PARAMS
, HOUR_CLOSING_FORMULA_ID
, HOUR_CLOSING_FORMULA_PARAMS
, DAY_CLOSING_FORMULA_ID
, DAY_CLOSING_FORMULA_PARAMS
, LONG_TERM_FORMULA_ID
, LONG_TERM_FORMULA_PARAMS
, REAL_LEVEL_FORMULA_ID
, REAL_LEVEL_FORMULA_PARAMS
, MARKET_PARAMS
, MARKET_DISABLE_CONDITIONS 
) VALUES (
SEQ_MARKET_DATA_SOURCE_MAPS.NEXTVAL
, 394
, 1
, 5
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "TRDPRC_1", "lastTradeTimeField": "EXCHTIM", "subscriptionConfig": {"DBKGn.DE":{"identifier":"DBKGn.DE"}}}'
, 2
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "TRDPRC_1", "lastTradeTimeField": "EXCHTIM", "subscriptionConfig": {"DBKGn.DE":{"identifier":"DBKGn.DE"}}}'
, 3
, '{"field1Name": "TRDPRC_1", "lastTradeTimeField": "EXCHTIM", "subscriptionConfig": {"DBKGn.DE":{"identifier":"DBKGn.DE"}}}'
, 9
, '{"interestSubscription": "EUR1MD=", "interestAskField": "ASK", "interestBidField": "BID", "daysConstant": "360", "subscriptionConfig": {"DBKGn.DE":{"identifier":"DBKGn.DE"}, "EUR1MD=":{"identifier":"EUR1MD="}}}'
, 2
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "TRDPRC_1", "lastTradeTimeField": "EXCHTIM", "subscriptionConfig": {"DBKGn.DE":{"identifier":"DBKGn.DE"}}}'
, '{"dontCloseAfterXSecNoUpdate": "60", "timeZone": "GMT", "shouldBeClosedByMonitoring": "true"}'
, q'{{"com.anyoption.service.level.condition.AskBidDistanceMarketDisableCondition": {'subscriptionName': 'DBKGn.DE', 'askField': 'ASK', 'bidField': 'BID', 'lastField': 'TRDPRC_1', 'distance': '0.02'}, "com.anyoption.service.level.condition.AskBidInverseMarketDisableCondition": {'subscriptionName': 'DBKGn.DE', 'askField': 'ASK', 'bidField': 'BID'}}}'
);

INSERT INTO MARKET_DATA_SOURCE_MAPS (
  ID
, MARKET_ID
, MARKET_DATA_SOURCE_ID
, HOUR_LEVEL_FORMULA_ID
, HOUR_LEVEL_FORMULA_PARAMS
, HOUR_CLOSING_FORMULA_ID
, HOUR_CLOSING_FORMULA_PARAMS
, DAY_CLOSING_FORMULA_ID
, DAY_CLOSING_FORMULA_PARAMS
, LONG_TERM_FORMULA_ID
, LONG_TERM_FORMULA_PARAMS
, REAL_LEVEL_FORMULA_ID
, REAL_LEVEL_FORMULA_PARAMS
, MARKET_PARAMS
, MARKET_DISABLE_CONDITIONS 
) VALUES (
SEQ_MARKET_DATA_SOURCE_MAPS.NEXTVAL
, 537
, 1
, 5
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "TRDPRC_1", "lastTradeTimeField": "EXCHTIM", "subscriptionConfig": {"ALVG.DE":{"identifier":"ALVG.DE"}}}'
, 2
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "TRDPRC_1", "lastTradeTimeField": "EXCHTIM", "subscriptionConfig": {"ALVG.DE":{"identifier":"ALVG.DE"}}}'
, 3
, '{"field1Name": "TRDPRC_1", "lastTradeTimeField": "EXCHTIM", "subscriptionConfig": {"ALVG.DE":{"identifier":"ALVG.DE"}}}'
, 9
, '{"interestSubscription": "EUR1MD=", "interestAskField": "ASK", "interestBidField": "BID", "daysConstant": "360", "subscriptionConfig": {"ALVG.DE":{"identifier":"ALVG.DE"}, "EUR1MD=":{"identifier":"EUR1MD="}}}'
, 2
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "TRDPRC_1", "lastTradeTimeField": "EXCHTIM", "subscriptionConfig": {"ALVG.DE":{"identifier":"ALVG.DE"}}}'
, '{"dontCloseAfterXSecNoUpdate": "60", "timeZone": "GMT", "shouldBeClosedByMonitoring": "true"}'
, q'{{"com.anyoption.service.level.condition.AskBidDistanceMarketDisableCondition": {'subscriptionName': 'ALVG.DE', 'askField': 'ASK', 'bidField': 'BID', 'lastField': 'TRDPRC_1', 'distance': '0.02'}, "com.anyoption.service.level.condition.AskBidInverseMarketDisableCondition": {'subscriptionName': 'ALVG.DE', 'askField': 'ASK', 'bidField': 'BID'}}}'
);

INSERT INTO MARKET_DATA_SOURCE_MAPS (
  ID
, MARKET_ID
, MARKET_DATA_SOURCE_ID
, HOUR_LEVEL_FORMULA_ID
, HOUR_LEVEL_FORMULA_PARAMS
, HOUR_CLOSING_FORMULA_ID
, HOUR_CLOSING_FORMULA_PARAMS
, DAY_CLOSING_FORMULA_ID
, DAY_CLOSING_FORMULA_PARAMS
, LONG_TERM_FORMULA_ID
, LONG_TERM_FORMULA_PARAMS
, REAL_LEVEL_FORMULA_ID
, REAL_LEVEL_FORMULA_PARAMS
, MARKET_PARAMS
, MARKET_DISABLE_CONDITIONS 
) VALUES (
SEQ_MARKET_DATA_SOURCE_MAPS.NEXTVAL
, 544
, 1
, 5
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "TRDPRC_1", "lastTradeTimeField": "EXCHTIM", "subscriptionConfig": {"EONGn.DE":{"identifier":"EONGn.DE"}}}'
, 2
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "TRDPRC_1", "lastTradeTimeField": "EXCHTIM", "subscriptionConfig": {"EONGn.DE":{"identifier":"EONGn.DE"}}}'
, 3
, '{"field1Name": "TRDPRC_1", "lastTradeTimeField": "EXCHTIM", "subscriptionConfig": {"EONGn.DE":{"identifier":"EONGn.DE"}}}'
, 9
, '{"interestSubscription": "EUR1MD=", "interestAskField": "ASK", "interestBidField": "BID", "daysConstant": "360", "subscriptionConfig": {"EONGn.DE":{"identifier":"EONGn.DE"}, "EUR1MD=":{"identifier":"EUR1MD="}}}'
, 2
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "TRDPRC_1", "lastTradeTimeField": "EXCHTIM", "subscriptionConfig": {"EONGn.DE":{"identifier":"EONGn.DE"}}}'
, '{"dontCloseAfterXSecNoUpdate": "60", "timeZone": "GMT", "shouldBeClosedByMonitoring": "true"}'
, q'{{"com.anyoption.service.level.condition.AskBidDistanceMarketDisableCondition": {'subscriptionName': 'EONGn.DE', 'askField': 'ASK', 'bidField': 'BID', 'lastField': 'TRDPRC_1', 'distance': '0.02'}, "com.anyoption.service.level.condition.AskBidInverseMarketDisableCondition": {'subscriptionName': 'EONGn.DE', 'askField': 'ASK', 'bidField': 'BID'}}}'
);

INSERT INTO MARKET_DATA_SOURCE_MAPS (
  ID
, MARKET_ID
, MARKET_DATA_SOURCE_ID
, HOUR_LEVEL_FORMULA_ID
, HOUR_LEVEL_FORMULA_PARAMS
, HOUR_CLOSING_FORMULA_ID
, HOUR_CLOSING_FORMULA_PARAMS
, DAY_CLOSING_FORMULA_ID
, DAY_CLOSING_FORMULA_PARAMS
, LONG_TERM_FORMULA_ID
, LONG_TERM_FORMULA_PARAMS
, REAL_LEVEL_FORMULA_ID
, REAL_LEVEL_FORMULA_PARAMS
, MARKET_PARAMS
, MARKET_DISABLE_CONDITIONS 
) VALUES (
SEQ_MARKET_DATA_SOURCE_MAPS.NEXTVAL
, 590
, 1
, 5
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "TRDPRC_1", "lastTradeTimeField": "EXCHTIM", "subscriptionConfig": {"DAIGn.DE":{"identifier":"DAIGn.DE"}}}'
, 2
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "TRDPRC_1", "lastTradeTimeField": "EXCHTIM", "subscriptionConfig": {"DAIGn.DE":{"identifier":"DAIGn.DE"}}}'
, 3
, '{"field1Name": "TRDPRC_1", "lastTradeTimeField": "EXCHTIM", "subscriptionConfig": {"DAIGn.DE":{"identifier":"DAIGn.DE"}}}'
, 9
, '{"interestSubscription": "EUR1MD=", "interestAskField": "ASK", "interestBidField": "BID", "daysConstant": "360", "subscriptionConfig": {"DAIGn.DE":{"identifier":"DAIGn.DE"}, "EUR1MD=":{"identifier":"EUR1MD="}}}'
, 2
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "TRDPRC_1", "lastTradeTimeField": "EXCHTIM", "subscriptionConfig": {"DAIGn.DE":{"identifier":"DAIGn.DE"}}}'
, '{"dontCloseAfterXSecNoUpdate": "60", "timeZone": "GMT", "shouldBeClosedByMonitoring": "true"}'
, q'{{"com.anyoption.service.level.condition.AskBidDistanceMarketDisableCondition": {'subscriptionName': 'DAIGn.DE', 'askField': 'ASK', 'bidField': 'BID', 'lastField': 'TRDPRC_1', 'distance': '0.02'}, "com.anyoption.service.level.condition.AskBidInverseMarketDisableCondition": {'subscriptionName': 'DAIGn.DE', 'askField': 'ASK', 'bidField': 'BID'}}}'
);

INSERT INTO MARKET_DATA_SOURCE_MAPS (
  ID
, MARKET_ID
, MARKET_DATA_SOURCE_ID
, HOUR_LEVEL_FORMULA_ID
, HOUR_LEVEL_FORMULA_PARAMS
, HOUR_CLOSING_FORMULA_ID
, HOUR_CLOSING_FORMULA_PARAMS
, DAY_CLOSING_FORMULA_ID
, DAY_CLOSING_FORMULA_PARAMS
, LONG_TERM_FORMULA_ID
, LONG_TERM_FORMULA_PARAMS
, REAL_LEVEL_FORMULA_ID
, REAL_LEVEL_FORMULA_PARAMS
, MARKET_PARAMS
, MARKET_DISABLE_CONDITIONS 
) VALUES (
SEQ_MARKET_DATA_SOURCE_MAPS.NEXTVAL
, 697
, 1
, 5
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "TRDPRC_1", "lastTradeTimeField": "EXCHTIM", "subscriptionConfig": {"ADSGn.DE":{"identifier":"ADSGn.DE"}}}'
, 2
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "TRDPRC_1", "lastTradeTimeField": "EXCHTIM", "subscriptionConfig": {"ADSGn.DE":{"identifier":"ADSGn.DE"}}}'
, 3
, '{"field1Name": "TRDPRC_1", "lastTradeTimeField": "EXCHTIM", "subscriptionConfig": {"ADSGn.DE":{"identifier":"ADSGn.DE"}}}'
, 9
, '{"interestSubscription": "EUR1MD=", "interestAskField": "ASK", "interestBidField": "BID", "daysConstant": "360", "subscriptionConfig": {"ADSGn.DE":{"identifier":"ADSGn.DE"}, "EUR1MD=":{"identifier":"EUR1MD="}}}'
, 2
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "TRDPRC_1", "lastTradeTimeField": "EXCHTIM", "subscriptionConfig": {"ADSGn.DE":{"identifier":"ADSGn.DE"}}}'
, '{"dontCloseAfterXSecNoUpdate": "60", "timeZone": "GMT", "shouldBeClosedByMonitoring": "true"}'
, q'{{"com.anyoption.service.level.condition.AskBidDistanceMarketDisableCondition": {'subscriptionName': 'ADSGn.DE', 'askField': 'ASK', 'bidField': 'BID', 'lastField': 'TRDPRC_1', 'distance': '0.02'}, "com.anyoption.service.level.condition.AskBidInverseMarketDisableCondition": {'subscriptionName': 'ADSGn.DE', 'askField': 'ASK', 'bidField': 'BID'}}}'
);

INSERT INTO MARKET_DATA_SOURCE_MAPS (
  ID
, MARKET_ID
, MARKET_DATA_SOURCE_ID
, HOUR_LEVEL_FORMULA_ID
, HOUR_LEVEL_FORMULA_PARAMS
, HOUR_CLOSING_FORMULA_ID
, HOUR_CLOSING_FORMULA_PARAMS
, DAY_CLOSING_FORMULA_ID
, DAY_CLOSING_FORMULA_PARAMS
, LONG_TERM_FORMULA_ID
, LONG_TERM_FORMULA_PARAMS
, REAL_LEVEL_FORMULA_ID
, REAL_LEVEL_FORMULA_PARAMS
, MARKET_PARAMS
, MARKET_DISABLE_CONDITIONS 
) VALUES (
SEQ_MARKET_DATA_SOURCE_MAPS.NEXTVAL
, 702
, 1
, 5
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "TRDPRC_1", "lastTradeTimeField": "EXCHTIM", "subscriptionConfig": {"BMWG.DE":{"identifier":"BMWG.DE"}}}'
, 2
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "TRDPRC_1", "lastTradeTimeField": "EXCHTIM", "subscriptionConfig": {"BMWG.DE":{"identifier":"BMWG.DE"}}}'
, 3
, '{"field1Name": "TRDPRC_1", "lastTradeTimeField": "EXCHTIM", "subscriptionConfig": {"BMWG.DE":{"identifier":"BMWG.DE"}}}'
, 9
, '{"interestSubscription": "EUR1MD=", "interestAskField": "ASK", "interestBidField": "BID", "daysConstant": "360", "subscriptionConfig": {"BMWG.DE":{"identifier":"BMWG.DE"}, "EUR1MD=":{"identifier":"EUR1MD="}}}'
, 2
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "TRDPRC_1", "lastTradeTimeField": "EXCHTIM", "subscriptionConfig": {"BMWG.DE":{"identifier":"BMWG.DE"}}}'
, '{"dontCloseAfterXSecNoUpdate": "60", "timeZone": "GMT", "shouldBeClosedByMonitoring": "true"}'
, q'{{"com.anyoption.service.level.condition.AskBidDistanceMarketDisableCondition": {'subscriptionName': 'BMWG.DE', 'askField': 'ASK', 'bidField': 'BID', 'lastField': 'TRDPRC_1', 'distance': '0.02'}, "com.anyoption.service.level.condition.AskBidInverseMarketDisableCondition": {'subscriptionName': 'BMWG.DE', 'askField': 'ASK', 'bidField': 'BID'}}}'
);

INSERT INTO MARKET_DATA_SOURCE_MAPS (
  ID
, MARKET_ID
, MARKET_DATA_SOURCE_ID
, HOUR_LEVEL_FORMULA_ID
, HOUR_LEVEL_FORMULA_PARAMS
, HOUR_CLOSING_FORMULA_ID
, HOUR_CLOSING_FORMULA_PARAMS
, DAY_CLOSING_FORMULA_ID
, DAY_CLOSING_FORMULA_PARAMS
, LONG_TERM_FORMULA_ID
, LONG_TERM_FORMULA_PARAMS
, REAL_LEVEL_FORMULA_ID
, REAL_LEVEL_FORMULA_PARAMS
, MARKET_PARAMS
, MARKET_DISABLE_CONDITIONS 
) VALUES (
SEQ_MARKET_DATA_SOURCE_MAPS.NEXTVAL
, 546
, 1
, 5
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {"ENI.MI":{"identifier":"ENI.MI"}}}'
, 2
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {"ENI.MI":{"identifier":"ENI.MI"}}}'
, 3
, '{"field1Name": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {"ENI.MI":{"identifier":"ENI.MI"}}}'
, 9
, '{"interestSubscription": "EUR1MD=", "interestAskField": "ASK", "interestBidField": "BID", "daysConstant": "365", "subscriptionConfig": {"ENI.MI":{"identifier":"ENI.MI"}, "EUR1MD=":{"identifier":"EUR1MD="}}}'
, 2
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {"ENI.MI":{"identifier":"ENI.MI"}}}'
, '{"dontCloseAfterXSecNoUpdate": "60", "timeZone": "GMT", "shouldBeClosedByMonitoring": "true"}'
, q'{{"com.anyoption.service.level.condition.AskBidDistanceMarketDisableCondition": {'subscriptionName': 'ENI.MI', 'askField': 'ASK', 'bidField': 'BID', 'lastField': 'TRDPRC_1', 'distance': '0.02'}, "com.anyoption.service.level.condition.AskBidInverseMarketDisableCondition": {'subscriptionName': 'ENI.MI', 'askField': 'ASK', 'bidField': 'BID'}}}'
);

INSERT INTO MARKET_DATA_SOURCE_MAPS (
  ID
, MARKET_ID
, MARKET_DATA_SOURCE_ID
, HOUR_LEVEL_FORMULA_ID
, HOUR_LEVEL_FORMULA_PARAMS
, HOUR_CLOSING_FORMULA_ID
, HOUR_CLOSING_FORMULA_PARAMS
, DAY_CLOSING_FORMULA_ID
, DAY_CLOSING_FORMULA_PARAMS
, LONG_TERM_FORMULA_ID
, LONG_TERM_FORMULA_PARAMS
, REAL_LEVEL_FORMULA_ID
, REAL_LEVEL_FORMULA_PARAMS
, MARKET_PARAMS
, MARKET_DISABLE_CONDITIONS 
) VALUES (
SEQ_MARKET_DATA_SOURCE_MAPS.NEXTVAL
, 547
, 1
, 5
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {"FIA.MI":{"identifier":"FIA.MI"}}}'
, 2
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {"FIA.MI":{"identifier":"FIA.MI"}}}'
, 3
, '{"field1Name": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {"FIA.MI":{"identifier":"FIA.MI"}}}'
, 9
, '{"interestSubscription": "EUR1MD=", "interestAskField": "ASK", "interestBidField": "BID", "daysConstant": "365", "subscriptionConfig": {"FIA.MI":{"identifier":"FIA.MI"}, "EUR1MD=":{"identifier":"EUR1MD="}}}'
, 2
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {"FIA.MI":{"identifier":"FIA.MI"}}}'
, '{"dontCloseAfterXSecNoUpdate": "60", "timeZone": "GMT", "shouldBeClosedByMonitoring": "true"}'
, q'{{"com.anyoption.service.level.condition.AskBidDistanceMarketDisableCondition": {'subscriptionName': 'FIA.MI', 'askField': 'ASK', 'bidField': 'BID', 'lastField': 'TRDPRC_1', 'distance': '0.02'}, "com.anyoption.service.level.condition.AskBidInverseMarketDisableCondition": {'subscriptionName': 'FIA.MI', 'askField': 'ASK', 'bidField': 'BID'}}}'
);

INSERT INTO MARKET_DATA_SOURCE_MAPS (
  ID
, MARKET_ID
, MARKET_DATA_SOURCE_ID
, HOUR_LEVEL_FORMULA_ID
, HOUR_LEVEL_FORMULA_PARAMS
, HOUR_CLOSING_FORMULA_ID
, HOUR_CLOSING_FORMULA_PARAMS
, DAY_CLOSING_FORMULA_ID
, DAY_CLOSING_FORMULA_PARAMS
, LONG_TERM_FORMULA_ID
, LONG_TERM_FORMULA_PARAMS
, REAL_LEVEL_FORMULA_ID
, REAL_LEVEL_FORMULA_PARAMS
, MARKET_PARAMS
, MARKET_DISABLE_CONDITIONS 
) VALUES (
SEQ_MARKET_DATA_SOURCE_MAPS.NEXTVAL
, 548
, 1
, 5
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {"GASI.MI":{"identifier":"GASI.MI"}}}'
, 2
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {"GASI.MI":{"identifier":"GASI.MI"}}}'
, 3
, '{"field1Name": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {"GASI.MI":{"identifier":"GASI.MI"}}}'
, 9
, '{"interestSubscription": "EUR1MD=", "interestAskField": "ASK", "interestBidField": "BID", "daysConstant": "365", "subscriptionConfig": {"GASI.MI":{"identifier":"GASI.MI"}, "EUR1MD=":{"identifier":"EUR1MD="}}}'
, 2
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {"GASI.MI":{"identifier":"GASI.MI"}}}'
, '{"dontCloseAfterXSecNoUpdate": "60", "timeZone": "GMT", "shouldBeClosedByMonitoring": "true"}'
, q'{{"com.anyoption.service.level.condition.AskBidDistanceMarketDisableCondition": {'subscriptionName': 'GASI.MI', 'askField': 'ASK', 'bidField': 'BID', 'lastField': 'TRDPRC_1', 'distance': '0.02'}, "com.anyoption.service.level.condition.AskBidInverseMarketDisableCondition": {'subscriptionName': 'GASI.MI', 'askField': 'ASK', 'bidField': 'BID'}}}'
);

INSERT INTO MARKET_DATA_SOURCE_MAPS (
  ID
, MARKET_ID
, MARKET_DATA_SOURCE_ID
, HOUR_LEVEL_FORMULA_ID
, HOUR_LEVEL_FORMULA_PARAMS
, HOUR_CLOSING_FORMULA_ID
, HOUR_CLOSING_FORMULA_PARAMS
, DAY_CLOSING_FORMULA_ID
, DAY_CLOSING_FORMULA_PARAMS
, LONG_TERM_FORMULA_ID
, LONG_TERM_FORMULA_PARAMS
, REAL_LEVEL_FORMULA_ID
, REAL_LEVEL_FORMULA_PARAMS
, MARKET_PARAMS
, MARKET_DISABLE_CONDITIONS 
) VALUES (
SEQ_MARKET_DATA_SOURCE_MAPS.NEXTVAL
, 600
, 1
, 5
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {"CRDI.MI":{"identifier":"CRDI.MI"}}}'
, 2
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {"CRDI.MI":{"identifier":"CRDI.MI"}}}'
, 3
, '{"field1Name": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {"CRDI.MI":{"identifier":"CRDI.MI"}}}'
, 9
, '{"interestSubscription": "EUR1MD=", "interestAskField": "ASK", "interestBidField": "BID", "daysConstant": "365", "subscriptionConfig": {"CRDI.MI":{"identifier":"CRDI.MI"}, "EUR1MD=":{"identifier":"EUR1MD="}}}'
, 2
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {"CRDI.MI":{"identifier":"CRDI.MI"}}}'
, '{"dontCloseAfterXSecNoUpdate": "60", "timeZone": "GMT", "shouldBeClosedByMonitoring": "true"}'
, q'{{"com.anyoption.service.level.condition.AskBidDistanceMarketDisableCondition": {'subscriptionName': 'CRDI.MI', 'askField': 'ASK', 'bidField': 'BID', 'lastField': 'TRDPRC_1', 'distance': '0.02'}, "com.anyoption.service.level.condition.AskBidInverseMarketDisableCondition": {'subscriptionName': 'CRDI.MI', 'askField': 'ASK', 'bidField': 'BID'}}}'
);

INSERT INTO MARKET_DATA_SOURCE_MAPS (
  ID
, MARKET_ID
, MARKET_DATA_SOURCE_ID
, HOUR_LEVEL_FORMULA_ID
, HOUR_LEVEL_FORMULA_PARAMS
, HOUR_CLOSING_FORMULA_ID
, HOUR_CLOSING_FORMULA_PARAMS
, DAY_CLOSING_FORMULA_ID
, DAY_CLOSING_FORMULA_PARAMS
, LONG_TERM_FORMULA_ID
, LONG_TERM_FORMULA_PARAMS
, REAL_LEVEL_FORMULA_ID
, REAL_LEVEL_FORMULA_PARAMS
, MARKET_PARAMS
, MARKET_DISABLE_CONDITIONS 
) VALUES (
SEQ_MARKET_DATA_SOURCE_MAPS.NEXTVAL
, 601
, 1
, 5
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {"ISP.MI":{"identifier":"ISP.MI"}}}'
, 2
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {"ISP.MI":{"identifier":"ISP.MI"}}}'
, 3
, '{"field1Name": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {"ISP.MI":{"identifier":"ISP.MI"}}}'
, 9
, '{"interestSubscription": "EUR1MD=", "interestAskField": "ASK", "interestBidField": "BID", "daysConstant": "365", "subscriptionConfig": {"ISP.MI":{"identifier":"ISP.MI"}, "EUR1MD=":{"identifier":"EUR1MD="}}}'
, 2
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {"ISP.MI":{"identifier":"ISP.MI"}}}'
, '{"dontCloseAfterXSecNoUpdate": "60", "timeZone": "GMT", "shouldBeClosedByMonitoring": "true"}'
, q'{{"com.anyoption.service.level.condition.AskBidDistanceMarketDisableCondition": {'subscriptionName': 'ISP.MI', 'askField': 'ASK', 'bidField': 'BID', 'lastField': 'TRDPRC_1', 'distance': '0.02'}, "com.anyoption.service.level.condition.AskBidInverseMarketDisableCondition": {'subscriptionName': 'ISP.MI', 'askField': 'ASK', 'bidField': 'BID'}}}'
);

INSERT INTO MARKET_DATA_SOURCE_MAPS (
  ID
, MARKET_ID
, MARKET_DATA_SOURCE_ID
, HOUR_LEVEL_FORMULA_ID
, HOUR_LEVEL_FORMULA_PARAMS
, HOUR_CLOSING_FORMULA_ID
, HOUR_CLOSING_FORMULA_PARAMS
, DAY_CLOSING_FORMULA_ID
, DAY_CLOSING_FORMULA_PARAMS
, LONG_TERM_FORMULA_ID
, LONG_TERM_FORMULA_PARAMS
, REAL_LEVEL_FORMULA_ID
, REAL_LEVEL_FORMULA_PARAMS
, MARKET_PARAMS
, MARKET_DISABLE_CONDITIONS 
) VALUES (
SEQ_MARKET_DATA_SOURCE_MAPS.NEXTVAL
, 602
, 1
, 5
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {"TLIT.MI":{"identifier":"TLIT.MI"}}}'
, 2
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {"TLIT.MI":{"identifier":"TLIT.MI"}}}'
, 3
, '{"field1Name": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {"TLIT.MI":{"identifier":"TLIT.MI"}}}'
, 9
, '{"interestSubscription": "EUR1MD=", "interestAskField": "ASK", "interestBidField": "BID", "daysConstant": "365", "subscriptionConfig": {"TLIT.MI":{"identifier":"TLIT.MI"}, "EUR1MD=":{"identifier":"EUR1MD="}}}'
, 2
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {"TLIT.MI":{"identifier":"TLIT.MI"}}}'
, '{"dontCloseAfterXSecNoUpdate": "60", "timeZone": "GMT", "shouldBeClosedByMonitoring": "true"}'
, q'{{"com.anyoption.service.level.condition.AskBidDistanceMarketDisableCondition": {'subscriptionName': 'TLIT.MI', 'askField': 'ASK', 'bidField': 'BID', 'lastField': 'TRDPRC_1', 'distance': '0.02'}, "com.anyoption.service.level.condition.AskBidInverseMarketDisableCondition": {'subscriptionName': 'TLIT.MI', 'askField': 'ASK', 'bidField': 'BID'}}}'
);

INSERT INTO MARKET_DATA_SOURCE_MAPS (
  ID
, MARKET_ID
, MARKET_DATA_SOURCE_ID
, HOUR_LEVEL_FORMULA_ID
, HOUR_LEVEL_FORMULA_PARAMS
, HOUR_CLOSING_FORMULA_ID
, HOUR_CLOSING_FORMULA_PARAMS
, DAY_CLOSING_FORMULA_ID
, DAY_CLOSING_FORMULA_PARAMS
, LONG_TERM_FORMULA_ID
, LONG_TERM_FORMULA_PARAMS
, REAL_LEVEL_FORMULA_ID
, REAL_LEVEL_FORMULA_PARAMS
, MARKET_PARAMS
, MARKET_DISABLE_CONDITIONS 
) VALUES (
SEQ_MARKET_DATA_SOURCE_MAPS.NEXTVAL
, 609
, 1
, 5
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {"JUVE.MI":{"identifier":"JUVE.MI"}}}'
, 2
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {"JUVE.MI":{"identifier":"JUVE.MI"}}}'
, 3
, '{"field1Name": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {"JUVE.MI":{"identifier":"JUVE.MI"}}}'
, 9
, '{"interestSubscription": "EUR1MD=", "interestAskField": "ASK", "interestBidField": "BID", "daysConstant": "365", "subscriptionConfig": {"JUVE.MI":{"identifier":"JUVE.MI"}, "EUR1MD=":{"identifier":"EUR1MD="}}}'
, 2
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {"JUVE.MI":{"identifier":"JUVE.MI"}}}'
, '{"dontCloseAfterXSecNoUpdate": "60", "timeZone": "GMT", "shouldBeClosedByMonitoring": "true"}'
, q'{{"com.anyoption.service.level.condition.AskBidDistanceMarketDisableCondition": {'subscriptionName': 'JUVE.MI', 'askField': 'ASK', 'bidField': 'BID', 'lastField': 'TRDPRC_1', 'distance': '0.02'}, "com.anyoption.service.level.condition.AskBidInverseMarketDisableCondition": {'subscriptionName': 'JUVE.MI', 'askField': 'ASK', 'bidField': 'BID'}}}'
);

INSERT INTO MARKET_DATA_SOURCE_MAPS (
  ID
, MARKET_ID
, MARKET_DATA_SOURCE_ID
, HOUR_LEVEL_FORMULA_ID
, HOUR_LEVEL_FORMULA_PARAMS
, HOUR_CLOSING_FORMULA_ID
, HOUR_CLOSING_FORMULA_PARAMS
, DAY_CLOSING_FORMULA_ID
, DAY_CLOSING_FORMULA_PARAMS
, LONG_TERM_FORMULA_ID
, LONG_TERM_FORMULA_PARAMS
, REAL_LEVEL_FORMULA_ID
, REAL_LEVEL_FORMULA_PARAMS
, MARKET_PARAMS
, MARKET_DISABLE_CONDITIONS 
) VALUES (
SEQ_MARKET_DATA_SOURCE_MAPS.NEXTVAL
, 551
, 1
, 5
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {"NBL":{"identifier":"NBL"}}}'
, 2
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {"NBL":{"identifier":"NBL"}}}'
, 3
, '{"field1Name": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {"NBL":{"identifier":"NBL"}}}'
, 9
, '{"interestSubscription": "USD1MD=", "interestAskField": "ASK", "interestBidField": "BID", "daysConstant": "360", "subscriptionConfig": {"NBL":{"identifier":"NBL"}, "USD1MD=":{"identifier":"USD1MD="}}}'
, 2
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {"NBL":{"identifier":"NBL"}}}'
, '{"dontCloseAfterXSecNoUpdate": "60", "timeZone": "GMT", "shouldBeClosedByMonitoring": "true"}'
, q'{{"com.anyoption.service.level.condition.AskBidDistanceMarketDisableCondition": {'subscriptionName': 'NBL', 'askField': 'ASK', 'bidField': 'BID', 'lastField': 'TRDPRC_1', 'distance': '0.02'}, "com.anyoption.service.level.condition.AskBidInverseMarketDisableCondition": {'subscriptionName': 'NBL', 'askField': 'ASK', 'bidField': 'BID'}}}'
);

INSERT INTO MARKET_DATA_SOURCE_MAPS (
  ID
, MARKET_ID
, MARKET_DATA_SOURCE_ID
, HOUR_LEVEL_FORMULA_ID
, HOUR_LEVEL_FORMULA_PARAMS
, HOUR_CLOSING_FORMULA_ID
, HOUR_CLOSING_FORMULA_PARAMS
, DAY_CLOSING_FORMULA_ID
, DAY_CLOSING_FORMULA_PARAMS
, LONG_TERM_FORMULA_ID
, LONG_TERM_FORMULA_PARAMS
, REAL_LEVEL_FORMULA_ID
, REAL_LEVEL_FORMULA_PARAMS
, MARKET_PARAMS
, MARKET_DISABLE_CONDITIONS 
) VALUES (
SEQ_MARKET_DATA_SOURCE_MAPS.NEXTVAL
, 553
, 1
, 5
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "TRDPRC_1", "lastTradeTimeField": "TRDTIM_1", "subscriptionConfig": {"GAZP.MM":{"identifier":"GAZP.MM"}}}'
, 2
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "TRDPRC_1", "lastTradeTimeField": "TRDTIM_1", "subscriptionConfig": {"GAZP.MM":{"identifier":"GAZP.MM"}}}'
, 3
, '{"field1Name": "TRDPRC_1", "lastTradeTimeField": "TRDTIM_1", "subscriptionConfig": {"GAZP.MM":{"identifier":"GAZP.MM"}}}'
, 9
, '{"interestSubscription": "RUB1MD=", "interestAskField": "ASK", "interestBidField": "BID", "daysConstant": "360", "subscriptionConfig": {"GAZP.MM":{"identifier":"GAZP.MM"}, "RUB1MD=":{"identifier":"RUB1MD="}}}'
, 2
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "TRDPRC_1", "lastTradeTimeField": "TRDTIM_1", "subscriptionConfig": {"GAZP.MM":{"identifier":"GAZP.MM"}}}'
, '{"dontCloseAfterXSecNoUpdate": "60", "timeZone": "GMT", "shouldBeClosedByMonitoring": "true"}'
, q'{{"com.anyoption.service.level.condition.AskBidDistanceMarketDisableCondition": {'subscriptionName': 'GAZP.MM', 'askField': 'ASK', 'bidField': 'BID', 'lastField': 'TRDPRC_1', 'distance': '0.02'}, "com.anyoption.service.level.condition.AskBidInverseMarketDisableCondition": {'subscriptionName': 'GAZP.MM', 'askField': 'ASK', 'bidField': 'BID'}}}'
);

INSERT INTO MARKET_DATA_SOURCE_MAPS (
  ID
, MARKET_ID
, MARKET_DATA_SOURCE_ID
, HOUR_LEVEL_FORMULA_ID
, HOUR_LEVEL_FORMULA_PARAMS
, HOUR_CLOSING_FORMULA_ID
, HOUR_CLOSING_FORMULA_PARAMS
, DAY_CLOSING_FORMULA_ID
, DAY_CLOSING_FORMULA_PARAMS
, LONG_TERM_FORMULA_ID
, LONG_TERM_FORMULA_PARAMS
, REAL_LEVEL_FORMULA_ID
, REAL_LEVEL_FORMULA_PARAMS
, MARKET_PARAMS
, MARKET_DISABLE_CONDITIONS 
) VALUES (
SEQ_MARKET_DATA_SOURCE_MAPS.NEXTVAL
, 554
, 1
, 5
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "TRDPRC_1", "lastTradeTimeField": "TRDTIM_1", "subscriptionConfig": {"ROSN.MM":{"identifier":"ROSN.MM"}}}'
, 2
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "TRDPRC_1", "lastTradeTimeField": "TRDTIM_1", "subscriptionConfig": {"ROSN.MM":{"identifier":"ROSN.MM"}}}'
, 3
, '{"field1Name": "TRDPRC_1", "lastTradeTimeField": "TRDTIM_1", "subscriptionConfig": {"ROSN.MM":{"identifier":"ROSN.MM"}}}'
, 9
, '{"interestSubscription": "RUB1MD=", "interestAskField": "ASK", "interestBidField": "BID", "daysConstant": "360", "subscriptionConfig": {"ROSN.MM":{"identifier":"ROSN.MM"}, "RUB1MD=":{"identifier":"RUB1MD="}}}'
, 2
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "TRDPRC_1", "lastTradeTimeField": "TRDTIM_1", "subscriptionConfig": {"ROSN.MM":{"identifier":"ROSN.MM"}}}'
, '{"dontCloseAfterXSecNoUpdate": "60", "timeZone": "GMT", "shouldBeClosedByMonitoring": "true"}'
, q'{{"com.anyoption.service.level.condition.AskBidDistanceMarketDisableCondition": {'subscriptionName': 'ROSN.MM', 'askField': 'ASK', 'bidField': 'BID', 'lastField': 'TRDPRC_1', 'distance': '0.02'}, "com.anyoption.service.level.condition.AskBidInverseMarketDisableCondition": {'subscriptionName': 'ROSN.MM', 'askField': 'ASK', 'bidField': 'BID'}}}'
);

INSERT INTO MARKET_DATA_SOURCE_MAPS (
  ID
, MARKET_ID
, MARKET_DATA_SOURCE_ID
, HOUR_LEVEL_FORMULA_ID
, HOUR_LEVEL_FORMULA_PARAMS
, HOUR_CLOSING_FORMULA_ID
, HOUR_CLOSING_FORMULA_PARAMS
, DAY_CLOSING_FORMULA_ID
, DAY_CLOSING_FORMULA_PARAMS
, LONG_TERM_FORMULA_ID
, LONG_TERM_FORMULA_PARAMS
, REAL_LEVEL_FORMULA_ID
, REAL_LEVEL_FORMULA_PARAMS
, MARKET_PARAMS
, MARKET_DISABLE_CONDITIONS 
) VALUES (
SEQ_MARKET_DATA_SOURCE_MAPS.NEXTVAL
, 555
, 1
, 5
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "TRDPRC_1", "lastTradeTimeField": "TRDTIM_1", "subscriptionConfig": {"SBER.MM":{"identifier":"SBER.MM"}}}'
, 2
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "TRDPRC_1", "lastTradeTimeField": "TRDTIM_1", "subscriptionConfig": {"SBER.MM":{"identifier":"SBER.MM"}}}'
, 3
, '{"field1Name": "TRDPRC_1", "lastTradeTimeField": "TRDTIM_1", "subscriptionConfig": {"SBER.MM":{"identifier":"SBER.MM"}}}'
, 9
, '{"interestSubscription": "RUB1MD=", "interestAskField": "ASK", "interestBidField": "BID", "daysConstant": "360", "subscriptionConfig": {"SBER.MM":{"identifier":"SBER.MM"}, "RUB1MD=":{"identifier":"RUB1MD="}}}'
, 2
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "TRDPRC_1", "lastTradeTimeField": "TRDTIM_1", "subscriptionConfig": {"SBER.MM":{"identifier":"SBER.MM"}}}'
, '{"dontCloseAfterXSecNoUpdate": "60", "timeZone": "GMT", "shouldBeClosedByMonitoring": "true"}'
, q'{{"com.anyoption.service.level.condition.AskBidDistanceMarketDisableCondition": {'subscriptionName': 'SBER.MM', 'askField': 'ASK', 'bidField': 'BID', 'lastField': 'TRDPRC_1', 'distance': '0.02'}, "com.anyoption.service.level.condition.AskBidInverseMarketDisableCondition": {'subscriptionName': 'SBER.MM', 'askField': 'ASK', 'bidField': 'BID'}}}'
);

INSERT INTO MARKET_DATA_SOURCE_MAPS (
  ID
, MARKET_ID
, MARKET_DATA_SOURCE_ID
, HOUR_LEVEL_FORMULA_ID
, HOUR_LEVEL_FORMULA_PARAMS
, HOUR_CLOSING_FORMULA_ID
, HOUR_CLOSING_FORMULA_PARAMS
, DAY_CLOSING_FORMULA_ID
, DAY_CLOSING_FORMULA_PARAMS
, LONG_TERM_FORMULA_ID
, LONG_TERM_FORMULA_PARAMS
, REAL_LEVEL_FORMULA_ID
, REAL_LEVEL_FORMULA_PARAMS
, MARKET_PARAMS
, MARKET_DISABLE_CONDITIONS 
) VALUES (
SEQ_MARKET_DATA_SOURCE_MAPS.NEXTVAL
, 565
, 1
, 5
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {"XOM":{"identifier":"XOM"}}}'
, 2
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {"XOM":{"identifier":"XOM"}}}'
, 3
, '{"field1Name": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {"XOM":{"identifier":"XOM"}}}'
, 9
, '{"interestSubscription": "USD1MD=", "interestAskField": "ASK", "interestBidField": "BID", "daysConstant": "360", "subscriptionConfig": {"XOM":{"identifier":"XOM"}, "USD1MD=":{"identifier":"USD1MD="}}}'
, 2
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {"XOM":{"identifier":"XOM"}}}'
, '{"dontCloseAfterXSecNoUpdate": "60", "timeZone": "GMT", "shouldBeClosedByMonitoring": "true"}'
, q'{{"com.anyoption.service.level.condition.AskBidDistanceMarketDisableCondition": {'subscriptionName': 'XOM', 'askField': 'ASK', 'bidField': 'BID', 'lastField': 'TRDPRC_1', 'distance': '0.02'}, "com.anyoption.service.level.condition.AskBidInverseMarketDisableCondition": {'subscriptionName': 'XOM', 'askField': 'ASK', 'bidField': 'BID'}}}'
);

INSERT INTO MARKET_DATA_SOURCE_MAPS (
  ID
, MARKET_ID
, MARKET_DATA_SOURCE_ID
, HOUR_LEVEL_FORMULA_ID
, HOUR_LEVEL_FORMULA_PARAMS
, HOUR_CLOSING_FORMULA_ID
, HOUR_CLOSING_FORMULA_PARAMS
, DAY_CLOSING_FORMULA_ID
, DAY_CLOSING_FORMULA_PARAMS
, LONG_TERM_FORMULA_ID
, LONG_TERM_FORMULA_PARAMS
, REAL_LEVEL_FORMULA_ID
, REAL_LEVEL_FORMULA_PARAMS
, MARKET_PARAMS
, MARKET_DISABLE_CONDITIONS 
) VALUES (
SEQ_MARKET_DATA_SOURCE_MAPS.NEXTVAL
, 575
, 1
, 5
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {"BHP.AX":{"identifier":"BHP.AX"}}}'
, 2
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {"BHP.AX":{"identifier":"BHP.AX"}}}'
, 3
, '{"field1Name": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {"BHP.AX":{"identifier":"BHP.AX"}}}'
, 9
, '{"interestSubscription": "AUD1MD=", "interestAskField": "ASK", "interestBidField": "BID", "daysConstant": "365", "subscriptionConfig": {"BHP.AX":{"identifier":"BHP.AX"}, "AUD1MD=":{"identifier":"AUD1MD="}}}'
, 2
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {"BHP.AX":{"identifier":"BHP.AX"}}}'
, '{"dontCloseAfterXSecNoUpdate": "60", "timeZone": "GMT", "shouldBeClosedByMonitoring": "true"}'
, q'{{"com.anyoption.service.level.condition.AskBidDistanceMarketDisableCondition": {'subscriptionName': 'BHP.AX', 'askField': 'ASK', 'bidField': 'BID', 'lastField': 'TRDPRC_1', 'distance': '0.02'}, "com.anyoption.service.level.condition.AskBidInverseMarketDisableCondition": {'subscriptionName': 'BHP.AX', 'askField': 'ASK', 'bidField': 'BID'}}}'
);

INSERT INTO MARKET_DATA_SOURCE_MAPS (
  ID
, MARKET_ID
, MARKET_DATA_SOURCE_ID
, HOUR_LEVEL_FORMULA_ID
, HOUR_LEVEL_FORMULA_PARAMS
, HOUR_CLOSING_FORMULA_ID
, HOUR_CLOSING_FORMULA_PARAMS
, DAY_CLOSING_FORMULA_ID
, DAY_CLOSING_FORMULA_PARAMS
, LONG_TERM_FORMULA_ID
, LONG_TERM_FORMULA_PARAMS
, REAL_LEVEL_FORMULA_ID
, REAL_LEVEL_FORMULA_PARAMS
, MARKET_PARAMS
, MARKET_DISABLE_CONDITIONS 
) VALUES (
SEQ_MARKET_DATA_SOURCE_MAPS.NEXTVAL
, 576
, 1
, 5
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {"ANZ.AX":{"identifier":"ANZ.AX"}}}'
, 2
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {"ANZ.AX":{"identifier":"ANZ.AX"}}}'
, 3
, '{"field1Name": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {"ANZ.AX":{"identifier":"ANZ.AX"}}}'
, 9
, '{"interestSubscription": "AUD1MD=", "interestAskField": "ASK", "interestBidField": "BID", "daysConstant": "365", "subscriptionConfig": {"ANZ.AX":{"identifier":"ANZ.AX"}, "AUD1MD=":{"identifier":"AUD1MD="}}}'
, 2
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {"ANZ.AX":{"identifier":"ANZ.AX"}}}'
, '{"dontCloseAfterXSecNoUpdate": "60", "timeZone": "GMT", "shouldBeClosedByMonitoring": "true"}'
, q'{{"com.anyoption.service.level.condition.AskBidDistanceMarketDisableCondition": {'subscriptionName': 'ANZ.AX', 'askField': 'ASK', 'bidField': 'BID', 'lastField': 'TRDPRC_1', 'distance': '0.02'}, "com.anyoption.service.level.condition.AskBidInverseMarketDisableCondition": {'subscriptionName': 'ANZ.AX', 'askField': 'ASK', 'bidField': 'BID'}}}'
);

INSERT INTO MARKET_DATA_SOURCE_MAPS (
  ID
, MARKET_ID
, MARKET_DATA_SOURCE_ID
, HOUR_LEVEL_FORMULA_ID
, HOUR_LEVEL_FORMULA_PARAMS
, HOUR_CLOSING_FORMULA_ID
, HOUR_CLOSING_FORMULA_PARAMS
, DAY_CLOSING_FORMULA_ID
, DAY_CLOSING_FORMULA_PARAMS
, LONG_TERM_FORMULA_ID
, LONG_TERM_FORMULA_PARAMS
, REAL_LEVEL_FORMULA_ID
, REAL_LEVEL_FORMULA_PARAMS
, MARKET_PARAMS
, MARKET_DISABLE_CONDITIONS 
) VALUES (
SEQ_MARKET_DATA_SOURCE_MAPS.NEXTVAL
, 714
, 1
, 5
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {"NAB.AX":{"identifier":"NAB.AX"}}}'
, 2
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {"NAB.AX":{"identifier":"NAB.AX"}}}'
, 3
, '{"field1Name": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {"NAB.AX":{"identifier":"NAB.AX"}}}'
, 9
, '{"interestSubscription": "AUD1MD=", "interestAskField": "ASK", "interestBidField": "BID", "daysConstant": "365", "subscriptionConfig": {"NAB.AX":{"identifier":"NAB.AX"}, "AUD1MD=":{"identifier":"AUD1MD="}}}'
, 2
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {"NAB.AX":{"identifier":"NAB.AX"}}}'
, '{"dontCloseAfterXSecNoUpdate": "60", "timeZone": "GMT", "shouldBeClosedByMonitoring": "true"}'
, q'{{"com.anyoption.service.level.condition.AskBidDistanceMarketDisableCondition": {'subscriptionName': 'NAB.AX', 'askField': 'ASK', 'bidField': 'BID', 'lastField': 'TRDPRC_1', 'distance': '0.02'}, "com.anyoption.service.level.condition.AskBidInverseMarketDisableCondition": {'subscriptionName': 'NAB.AX', 'askField': 'ASK', 'bidField': 'BID'}}}'
);

INSERT INTO MARKET_DATA_SOURCE_MAPS (
  ID
, MARKET_ID
, MARKET_DATA_SOURCE_ID
, HOUR_LEVEL_FORMULA_ID
, HOUR_LEVEL_FORMULA_PARAMS
, HOUR_CLOSING_FORMULA_ID
, HOUR_CLOSING_FORMULA_PARAMS
, DAY_CLOSING_FORMULA_ID
, DAY_CLOSING_FORMULA_PARAMS
, LONG_TERM_FORMULA_ID
, LONG_TERM_FORMULA_PARAMS
, REAL_LEVEL_FORMULA_ID
, REAL_LEVEL_FORMULA_PARAMS
, MARKET_PARAMS
, MARKET_DISABLE_CONDITIONS 
) VALUES (
SEQ_MARKET_DATA_SOURCE_MAPS.NEXTVAL
, 577
, 1
, 5
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {"8058.T":{"identifier":"8058.T"}}}'
, 2
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {"8058.T":{"identifier":"8058.T"}}}'
, 3
, '{"field1Name": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {"8058.T":{"identifier":"8058.T"}}}'
, 9
, '{"interestSubscription": "JPY1MD=", "interestAskField": "ASK", "interestBidField": "BID", "daysConstant": "360", "subscriptionConfig": {"8058.T":{"identifier":"8058.T"}, "JPY1MD=":{"identifier":"JPY1MD="}}}'
, 2
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {"8058.T":{"identifier":"8058.T"}}}'
, '{"dontCloseAfterXSecNoUpdate": "60", "timeZone": "GMT", "shouldBeClosedByMonitoring": "true"}'
, q'{{"com.anyoption.service.level.condition.AskBidDistanceMarketDisableCondition": {'subscriptionName': '8058.T', 'askField': 'ASK', 'bidField': 'BID', 'lastField': 'TRDPRC_1', 'distance': '0.02'}, "com.anyoption.service.level.condition.AskBidInverseMarketDisableCondition": {'subscriptionName': '8058.T', 'askField': 'ASK', 'bidField': 'BID'}}}'
);

INSERT INTO MARKET_DATA_SOURCE_MAPS (
  ID
, MARKET_ID
, MARKET_DATA_SOURCE_ID
, HOUR_LEVEL_FORMULA_ID
, HOUR_LEVEL_FORMULA_PARAMS
, HOUR_CLOSING_FORMULA_ID
, HOUR_CLOSING_FORMULA_PARAMS
, DAY_CLOSING_FORMULA_ID
, DAY_CLOSING_FORMULA_PARAMS
, LONG_TERM_FORMULA_ID
, LONG_TERM_FORMULA_PARAMS
, REAL_LEVEL_FORMULA_ID
, REAL_LEVEL_FORMULA_PARAMS
, MARKET_PARAMS
, MARKET_DISABLE_CONDITIONS 
) VALUES (
SEQ_MARKET_DATA_SOURCE_MAPS.NEXTVAL
, 578
, 1
, 5
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {"7203.T":{"identifier":"7203.T"}}}'
, 2
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {"7203.T":{"identifier":"7203.T"}}}'
, 3
, '{"field1Name": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {"7203.T":{"identifier":"7203.T"}}}'
, 9
, '{"interestSubscription": "JPY1MD=", "interestAskField": "ASK", "interestBidField": "BID", "daysConstant": "360", "subscriptionConfig": {"7203.T":{"identifier":"7203.T"}, "JPY1MD=":{"identifier":"JPY1MD="}}}'
, 2
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {"7203.T":{"identifier":"7203.T"}}}'
, '{"dontCloseAfterXSecNoUpdate": "60", "timeZone": "GMT", "shouldBeClosedByMonitoring": "true"}'
, q'{{"com.anyoption.service.level.condition.AskBidDistanceMarketDisableCondition": {'subscriptionName': '7203.T', 'askField': 'ASK', 'bidField': 'BID', 'lastField': 'TRDPRC_1', 'distance': '0.02'}, "com.anyoption.service.level.condition.AskBidInverseMarketDisableCondition": {'subscriptionName': '7203.T', 'askField': 'ASK', 'bidField': 'BID'}}}'
);

INSERT INTO MARKET_DATA_SOURCE_MAPS (
  ID
, MARKET_ID
, MARKET_DATA_SOURCE_ID
, HOUR_LEVEL_FORMULA_ID
, HOUR_LEVEL_FORMULA_PARAMS
, HOUR_CLOSING_FORMULA_ID
, HOUR_CLOSING_FORMULA_PARAMS
, DAY_CLOSING_FORMULA_ID
, DAY_CLOSING_FORMULA_PARAMS
, LONG_TERM_FORMULA_ID
, LONG_TERM_FORMULA_PARAMS
, REAL_LEVEL_FORMULA_ID
, REAL_LEVEL_FORMULA_PARAMS
, MARKET_PARAMS
, MARKET_DISABLE_CONDITIONS 
) VALUES (
SEQ_MARKET_DATA_SOURCE_MAPS.NEXTVAL
, 585
, 1
, 5
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {"EDF.PA":{"identifier":"EDF.PA"}}}'
, 2
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {"EDF.PA":{"identifier":"EDF.PA"}}}'
, 3
, '{"field1Name": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {"EDF.PA":{"identifier":"EDF.PA"}}}'
, 9
, '{"interestSubscription": "EUR1MD=", "interestAskField": "ASK", "interestBidField": "BID", "daysConstant": "360", "subscriptionConfig": {"EDF.PA":{"identifier":"EDF.PA"}, "EUR1MD=":{"identifier":"EUR1MD="}}}'
, 2
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {"EDF.PA":{"identifier":"EDF.PA"}}}'
, '{"dontCloseAfterXSecNoUpdate": "60", "timeZone": "GMT", "shouldBeClosedByMonitoring": "true"}'
, q'{{"com.anyoption.service.level.condition.AskBidDistanceMarketDisableCondition": {'subscriptionName': 'EDF.PA', 'askField': 'ASK', 'bidField': 'BID', 'lastField': 'TRDPRC_1', 'distance': '0.02'}, "com.anyoption.service.level.condition.AskBidInverseMarketDisableCondition": {'subscriptionName': 'EDF.PA', 'askField': 'ASK', 'bidField': 'BID'}}}'
);

INSERT INTO MARKET_DATA_SOURCE_MAPS (
  ID
, MARKET_ID
, MARKET_DATA_SOURCE_ID
, HOUR_LEVEL_FORMULA_ID
, HOUR_LEVEL_FORMULA_PARAMS
, HOUR_CLOSING_FORMULA_ID
, HOUR_CLOSING_FORMULA_PARAMS
, DAY_CLOSING_FORMULA_ID
, DAY_CLOSING_FORMULA_PARAMS
, LONG_TERM_FORMULA_ID
, LONG_TERM_FORMULA_PARAMS
, REAL_LEVEL_FORMULA_ID
, REAL_LEVEL_FORMULA_PARAMS
, MARKET_PARAMS
, MARKET_DISABLE_CONDITIONS 
) VALUES (
SEQ_MARKET_DATA_SOURCE_MAPS.NEXTVAL
, 586
, 1
, 5
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {"RENA.PA":{"identifier":"RENA.PA"}}}'
, 2
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {"RENA.PA":{"identifier":"RENA.PA"}}}'
, 3
, '{"field1Name": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {"RENA.PA":{"identifier":"RENA.PA"}}}'
, 9
, '{"interestSubscription": "EUR1MD=", "interestAskField": "ASK", "interestBidField": "BID", "daysConstant": "360", "subscriptionConfig": {"RENA.PA":{"identifier":"RENA.PA"}, "EUR1MD=":{"identifier":"EUR1MD="}}}'
, 2
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {"RENA.PA":{"identifier":"RENA.PA"}}}'
, '{"dontCloseAfterXSecNoUpdate": "60", "timeZone": "GMT", "shouldBeClosedByMonitoring": "true"}'
, q'{{"com.anyoption.service.level.condition.AskBidDistanceMarketDisableCondition": {'subscriptionName': 'RENA.PA', 'askField': 'ASK', 'bidField': 'BID', 'lastField': 'TRDPRC_1', 'distance': '0.02'}, "com.anyoption.service.level.condition.AskBidInverseMarketDisableCondition": {'subscriptionName': 'RENA.PA', 'askField': 'ASK', 'bidField': 'BID'}}}'
);

INSERT INTO MARKET_DATA_SOURCE_MAPS (
  ID
, MARKET_ID
, MARKET_DATA_SOURCE_ID
, HOUR_LEVEL_FORMULA_ID
, HOUR_LEVEL_FORMULA_PARAMS
, HOUR_CLOSING_FORMULA_ID
, HOUR_CLOSING_FORMULA_PARAMS
, DAY_CLOSING_FORMULA_ID
, DAY_CLOSING_FORMULA_PARAMS
, LONG_TERM_FORMULA_ID
, LONG_TERM_FORMULA_PARAMS
, REAL_LEVEL_FORMULA_ID
, REAL_LEVEL_FORMULA_PARAMS
, MARKET_PARAMS
, MARKET_DISABLE_CONDITIONS 
) VALUES (
SEQ_MARKET_DATA_SOURCE_MAPS.NEXTVAL
, 587
, 1
, 5
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {"SOGN.PA":{"identifier":"SOGN.PA"}}}'
, 2
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {"SOGN.PA":{"identifier":"SOGN.PA"}}}'
, 3
, '{"field1Name": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {"SOGN.PA":{"identifier":"SOGN.PA"}}}'
, 9
, '{"interestSubscription": "EUR1MD=", "interestAskField": "ASK", "interestBidField": "BID", "daysConstant": "360", "subscriptionConfig": {"SOGN.PA":{"identifier":"SOGN.PA"}, "EUR1MD=":{"identifier":"EUR1MD="}}}'
, 2
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {"SOGN.PA":{"identifier":"SOGN.PA"}}}'
, '{"dontCloseAfterXSecNoUpdate": "60", "timeZone": "GMT", "shouldBeClosedByMonitoring": "true"}'
, q'{{"com.anyoption.service.level.condition.AskBidDistanceMarketDisableCondition": {'subscriptionName': 'SOGN.PA', 'askField': 'ASK', 'bidField': 'BID', 'lastField': 'TRDPRC_1', 'distance': '0.02'}, "com.anyoption.service.level.condition.AskBidInverseMarketDisableCondition": {'subscriptionName': 'SOGN.PA', 'askField': 'ASK', 'bidField': 'BID'}}}'
);

INSERT INTO MARKET_DATA_SOURCE_MAPS (
  ID
, MARKET_ID
, MARKET_DATA_SOURCE_ID
, HOUR_LEVEL_FORMULA_ID
, HOUR_LEVEL_FORMULA_PARAMS
, HOUR_CLOSING_FORMULA_ID
, HOUR_CLOSING_FORMULA_PARAMS
, DAY_CLOSING_FORMULA_ID
, DAY_CLOSING_FORMULA_PARAMS
, LONG_TERM_FORMULA_ID
, LONG_TERM_FORMULA_PARAMS
, REAL_LEVEL_FORMULA_ID
, REAL_LEVEL_FORMULA_PARAMS
, MARKET_PARAMS
, MARKET_DISABLE_CONDITIONS 
) VALUES (
SEQ_MARKET_DATA_SOURCE_MAPS.NEXTVAL
, 129
, 1
, 5
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {"AAPL.O":{"identifier":"AAPL.O"}}}'
, 2
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {"AAPL.O":{"identifier":"AAPL.O"}}}'
, 3
, '{"field1Name": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {"AAPL.O":{"identifier":"AAPL.O"}}}'
, 9
, '{"interestSubscription": "USD1MD=", "interestAskField": "ASK", "interestBidField": "BID", "daysConstant": "360", "subscriptionConfig": {"AAPL.O":{"identifier":"AAPL.O"}, "USD1MD=":{"identifier":"USD1MD="}}}'
, 2
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {"AAPL.O":{"identifier":"AAPL.O"}}}'
, '{"dontCloseAfterXSecNoUpdate": "60", "timeZone": "GMT", "shouldBeClosedByMonitoring": "true"}'
, q'{{"com.anyoption.service.level.condition.AskBidDistanceMarketDisableCondition": {'subscriptionName': 'AAPL.O', 'askField': 'ASK', 'bidField': 'BID', 'lastField': 'TRDPRC_1', 'distance': '0.02'}, "com.anyoption.service.level.condition.AskBidInverseMarketDisableCondition": {'subscriptionName': 'AAPL.O', 'askField': 'ASK', 'bidField': 'BID'}}}'
);

INSERT INTO MARKET_DATA_SOURCE_MAPS (
  ID
, MARKET_ID
, MARKET_DATA_SOURCE_ID
, HOUR_LEVEL_FORMULA_ID
, HOUR_LEVEL_FORMULA_PARAMS
, HOUR_CLOSING_FORMULA_ID
, HOUR_CLOSING_FORMULA_PARAMS
, DAY_CLOSING_FORMULA_ID
, DAY_CLOSING_FORMULA_PARAMS
, LONG_TERM_FORMULA_ID
, LONG_TERM_FORMULA_PARAMS
, REAL_LEVEL_FORMULA_ID
, REAL_LEVEL_FORMULA_PARAMS
, MARKET_PARAMS
, MARKET_DISABLE_CONDITIONS 
) VALUES (
SEQ_MARKET_DATA_SOURCE_MAPS.NEXTVAL
, 130
, 1
, 5
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {"MSFT.O":{"identifier":"MSFT.O"}}}'
, 2
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {"MSFT.O":{"identifier":"MSFT.O"}}}'
, 3
, '{"field1Name": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {"MSFT.O":{"identifier":"MSFT.O"}}}'
, 9
, '{"interestSubscription": "USD1MD=", "interestAskField": "ASK", "interestBidField": "BID", "daysConstant": "360", "subscriptionConfig": {"MSFT.O":{"identifier":"MSFT.O"}, "USD1MD=":{"identifier":"USD1MD="}}}'
, 2
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {"MSFT.O":{"identifier":"MSFT.O"}}}'
, '{"dontCloseAfterXSecNoUpdate": "60", "timeZone": "GMT", "shouldBeClosedByMonitoring": "true"}'
, q'{{"com.anyoption.service.level.condition.AskBidDistanceMarketDisableCondition": {'subscriptionName': 'MSFT.O', 'askField': 'ASK', 'bidField': 'BID', 'lastField': 'TRDPRC_1', 'distance': '0.02'}, "com.anyoption.service.level.condition.AskBidInverseMarketDisableCondition": {'subscriptionName': 'MSFT.O', 'askField': 'ASK', 'bidField': 'BID'}}}'
);

INSERT INTO MARKET_DATA_SOURCE_MAPS (
  ID
, MARKET_ID
, MARKET_DATA_SOURCE_ID
, HOUR_LEVEL_FORMULA_ID
, HOUR_LEVEL_FORMULA_PARAMS
, HOUR_CLOSING_FORMULA_ID
, HOUR_CLOSING_FORMULA_PARAMS
, DAY_CLOSING_FORMULA_ID
, DAY_CLOSING_FORMULA_PARAMS
, LONG_TERM_FORMULA_ID
, LONG_TERM_FORMULA_PARAMS
, REAL_LEVEL_FORMULA_ID
, REAL_LEVEL_FORMULA_PARAMS
, MARKET_PARAMS
, MARKET_DISABLE_CONDITIONS 
) VALUES (
SEQ_MARKET_DATA_SOURCE_MAPS.NEXTVAL
, 132
, 1
, 5
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {"GOOG.O":{"identifier":"GOOG.O"}}}'
, 2
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {"GOOG.O":{"identifier":"GOOG.O"}}}'
, 3
, '{"field1Name": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {"GOOG.O":{"identifier":"GOOG.O"}}}'
, 9
, '{"interestSubscription": "USD1MD=", "interestAskField": "ASK", "interestBidField": "BID", "daysConstant": "360", "subscriptionConfig": {"GOOG.O":{"identifier":"GOOG.O"}, "USD1MD=":{"identifier":"USD1MD="}}}'
, 2
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {"GOOG.O":{"identifier":"GOOG.O"}}}'
, '{"dontCloseAfterXSecNoUpdate": "60", "timeZone": "GMT", "shouldBeClosedByMonitoring": "true"}'
, q'{{"com.anyoption.service.level.condition.AskBidDistanceMarketDisableCondition": {'subscriptionName': 'GOOG.O', 'askField': 'ASK', 'bidField': 'BID', 'lastField': 'TRDPRC_1', 'distance': '0.02'}, "com.anyoption.service.level.condition.AskBidInverseMarketDisableCondition": {'subscriptionName': 'GOOG.O', 'askField': 'ASK', 'bidField': 'BID'}}}'
);

INSERT INTO MARKET_DATA_SOURCE_MAPS (
  ID
, MARKET_ID
, MARKET_DATA_SOURCE_ID
, HOUR_LEVEL_FORMULA_ID
, HOUR_LEVEL_FORMULA_PARAMS
, HOUR_CLOSING_FORMULA_ID
, HOUR_CLOSING_FORMULA_PARAMS
, DAY_CLOSING_FORMULA_ID
, DAY_CLOSING_FORMULA_PARAMS
, LONG_TERM_FORMULA_ID
, LONG_TERM_FORMULA_PARAMS
, REAL_LEVEL_FORMULA_ID
, REAL_LEVEL_FORMULA_PARAMS
, MARKET_PARAMS
, MARKET_DISABLE_CONDITIONS 
) VALUES (
SEQ_MARKET_DATA_SOURCE_MAPS.NEXTVAL
, 566
, 1
, 5
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {"INTC.O":{"identifier":"INTC.O"}}}'
, 2
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {"INTC.O":{"identifier":"INTC.O"}}}'
, 3
, '{"field1Name": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {"INTC.O":{"identifier":"INTC.O"}}}'
, 9
, '{"interestSubscription": "USD1MD=", "interestAskField": "ASK", "interestBidField": "BID", "daysConstant": "360", "subscriptionConfig": {"INTC.O":{"identifier":"INTC.O"}, "USD1MD=":{"identifier":"USD1MD="}}}'
, 2
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {"INTC.O":{"identifier":"INTC.O"}}}'
, '{"dontCloseAfterXSecNoUpdate": "60", "timeZone": "GMT", "shouldBeClosedByMonitoring": "true"}'
, q'{{"com.anyoption.service.level.condition.AskBidDistanceMarketDisableCondition": {'subscriptionName': 'INTC.O', 'askField': 'ASK', 'bidField': 'BID', 'lastField': 'TRDPRC_1', 'distance': '0.02'}, "com.anyoption.service.level.condition.AskBidInverseMarketDisableCondition": {'subscriptionName': 'INTC.O', 'askField': 'ASK', 'bidField': 'BID'}}}'
);

INSERT INTO MARKET_DATA_SOURCE_MAPS (
  ID
, MARKET_ID
, MARKET_DATA_SOURCE_ID
, HOUR_LEVEL_FORMULA_ID
, HOUR_LEVEL_FORMULA_PARAMS
, HOUR_CLOSING_FORMULA_ID
, HOUR_CLOSING_FORMULA_PARAMS
, DAY_CLOSING_FORMULA_ID
, DAY_CLOSING_FORMULA_PARAMS
, LONG_TERM_FORMULA_ID
, LONG_TERM_FORMULA_PARAMS
, REAL_LEVEL_FORMULA_ID
, REAL_LEVEL_FORMULA_PARAMS
, MARKET_PARAMS
, MARKET_DISABLE_CONDITIONS 
) VALUES (
SEQ_MARKET_DATA_SOURCE_MAPS.NEXTVAL
, 617
, 1
, 5
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {"BIDU.O":{"identifier":"BIDU.O"}}}'
, 2
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {"BIDU.O":{"identifier":"BIDU.O"}}}'
, 3
, '{"field1Name": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {"BIDU.O":{"identifier":"BIDU.O"}}}'
, 9
, '{"interestSubscription": "USD1MD=", "interestAskField": "ASK", "interestBidField": "BID", "daysConstant": "360", "subscriptionConfig": {"BIDU.O":{"identifier":"BIDU.O"}, "USD1MD=":{"identifier":"USD1MD="}}}'
, 2
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {"BIDU.O":{"identifier":"BIDU.O"}}}'
, '{"dontCloseAfterXSecNoUpdate": "60", "timeZone": "GMT", "shouldBeClosedByMonitoring": "true"}'
, q'{{"com.anyoption.service.level.condition.AskBidDistanceMarketDisableCondition": {'subscriptionName': 'BIDU.O', 'askField': 'ASK', 'bidField': 'BID', 'lastField': 'TRDPRC_1', 'distance': '0.02'}, "com.anyoption.service.level.condition.AskBidInverseMarketDisableCondition": {'subscriptionName': 'BIDU.O', 'askField': 'ASK', 'bidField': 'BID'}}}'
);

INSERT INTO MARKET_DATA_SOURCE_MAPS (
  ID
, MARKET_ID
, MARKET_DATA_SOURCE_ID
, HOUR_LEVEL_FORMULA_ID
, HOUR_LEVEL_FORMULA_PARAMS
, HOUR_CLOSING_FORMULA_ID
, HOUR_CLOSING_FORMULA_PARAMS
, DAY_CLOSING_FORMULA_ID
, DAY_CLOSING_FORMULA_PARAMS
, LONG_TERM_FORMULA_ID
, LONG_TERM_FORMULA_PARAMS
, REAL_LEVEL_FORMULA_ID
, REAL_LEVEL_FORMULA_PARAMS
, MARKET_PARAMS
, MARKET_DISABLE_CONDITIONS 
) VALUES (
SEQ_MARKET_DATA_SOURCE_MAPS.NEXTVAL
, 620
, 1
, 5
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {"FB.O":{"identifier":"FB.O"}}}'
, 2
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {"FB.O":{"identifier":"FB.O"}}}'
, 3
, '{"field1Name": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {"FB.O":{"identifier":"FB.O"}}}'
, 9
, '{"interestSubscription": "USD1MD=", "interestAskField": "ASK", "interestBidField": "BID", "daysConstant": "360", "subscriptionConfig": {"FB.O":{"identifier":"FB.O"}, "USD1MD=":{"identifier":"USD1MD="}}}'
, 2
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {"FB.O":{"identifier":"FB.O"}}}'
, '{"dontCloseAfterXSecNoUpdate": "60", "timeZone": "GMT", "shouldBeClosedByMonitoring": "true"}'
, q'{{"com.anyoption.service.level.condition.AskBidDistanceMarketDisableCondition": {'subscriptionName': 'FB.O', 'askField': 'ASK', 'bidField': 'BID', 'lastField': 'TRDPRC_1', 'distance': '0.02'}, "com.anyoption.service.level.condition.AskBidInverseMarketDisableCondition": {'subscriptionName': 'FB.O', 'askField': 'ASK', 'bidField': 'BID'}}}'
);

INSERT INTO MARKET_DATA_SOURCE_MAPS (
  ID
, MARKET_ID
, MARKET_DATA_SOURCE_ID
, HOUR_LEVEL_FORMULA_ID
, HOUR_LEVEL_FORMULA_PARAMS
, HOUR_CLOSING_FORMULA_ID
, HOUR_CLOSING_FORMULA_PARAMS
, DAY_CLOSING_FORMULA_ID
, DAY_CLOSING_FORMULA_PARAMS
, LONG_TERM_FORMULA_ID
, LONG_TERM_FORMULA_PARAMS
, REAL_LEVEL_FORMULA_ID
, REAL_LEVEL_FORMULA_PARAMS
, MARKET_PARAMS
, MARKET_DISABLE_CONDITIONS 
) VALUES (
SEQ_MARKET_DATA_SOURCE_MAPS.NEXTVAL
, 627
, 1
, 5
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {"AMZN.O":{"identifier":"AMZN.O"}}}'
, 2
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {"AMZN.O":{"identifier":"AMZN.O"}}}'
, 3
, '{"field1Name": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {"AMZN.O":{"identifier":"AMZN.O"}}}'
, 9
, '{"interestSubscription": "USD1MD=", "interestAskField": "ASK", "interestBidField": "BID", "daysConstant": "360", "subscriptionConfig": {"AMZN.O":{"identifier":"AMZN.O"}, "USD1MD=":{"identifier":"USD1MD="}}}'
, 2
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {"AMZN.O":{"identifier":"AMZN.O"}}}'
, '{"dontCloseAfterXSecNoUpdate": "60", "timeZone": "GMT", "shouldBeClosedByMonitoring": "true"}'
, q'{{"com.anyoption.service.level.condition.AskBidDistanceMarketDisableCondition": {'subscriptionName': 'AMZN.O', 'askField': 'ASK', 'bidField': 'BID', 'lastField': 'TRDPRC_1', 'distance': '0.02'}, "com.anyoption.service.level.condition.AskBidInverseMarketDisableCondition": {'subscriptionName': 'AMZN.O', 'askField': 'ASK', 'bidField': 'BID'}}}'
);

INSERT INTO MARKET_DATA_SOURCE_MAPS (
  ID
, MARKET_ID
, MARKET_DATA_SOURCE_ID
, HOUR_LEVEL_FORMULA_ID
, HOUR_LEVEL_FORMULA_PARAMS
, HOUR_CLOSING_FORMULA_ID
, HOUR_CLOSING_FORMULA_PARAMS
, DAY_CLOSING_FORMULA_ID
, DAY_CLOSING_FORMULA_PARAMS
, LONG_TERM_FORMULA_ID
, LONG_TERM_FORMULA_PARAMS
, REAL_LEVEL_FORMULA_ID
, REAL_LEVEL_FORMULA_PARAMS
, MARKET_PARAMS
, MARKET_DISABLE_CONDITIONS 
) VALUES (
SEQ_MARKET_DATA_SOURCE_MAPS.NEXTVAL
, 708
, 1
, 5
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {"PYPL.O":{"identifier":"PYPL.O"}}}'
, 2
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {"PYPL.O":{"identifier":"PYPL.O"}}}'
, 3
, '{"field1Name": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {"PYPL.O":{"identifier":"PYPL.O"}}}'
, 9
, '{"interestSubscription": "USD1MD=", "interestAskField": "ASK", "interestBidField": "BID", "daysConstant": "360", "subscriptionConfig": {"PYPL.O":{"identifier":"PYPL.O"}, "USD1MD=":{"identifier":"USD1MD="}}}'
, 2
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {"PYPL.O":{"identifier":"PYPL.O"}}}'
, '{"dontCloseAfterXSecNoUpdate": "60", "timeZone": "GMT", "shouldBeClosedByMonitoring": "true"}'
, q'{{"com.anyoption.service.level.condition.AskBidDistanceMarketDisableCondition": {'subscriptionName': 'PYPL.O', 'askField': 'ASK', 'bidField': 'BID', 'lastField': 'TRDPRC_1', 'distance': '0.02'}, "com.anyoption.service.level.condition.AskBidInverseMarketDisableCondition": {'subscriptionName': 'PYPL.O', 'askField': 'ASK', 'bidField': 'BID'}}}'
);

INSERT INTO MARKET_DATA_SOURCE_MAPS (
  ID
, MARKET_ID
, MARKET_DATA_SOURCE_ID
, HOUR_LEVEL_FORMULA_ID
, HOUR_LEVEL_FORMULA_PARAMS
, HOUR_CLOSING_FORMULA_ID
, HOUR_CLOSING_FORMULA_PARAMS
, DAY_CLOSING_FORMULA_ID
, DAY_CLOSING_FORMULA_PARAMS
, LONG_TERM_FORMULA_ID
, LONG_TERM_FORMULA_PARAMS
, REAL_LEVEL_FORMULA_ID
, REAL_LEVEL_FORMULA_PARAMS
, MARKET_PARAMS
, MARKET_DISABLE_CONDITIONS 
) VALUES (
SEQ_MARKET_DATA_SOURCE_MAPS.NEXTVAL
, 593
, 1
, 5
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {"DIS":{"identifier":"DIS"}}}'
, 2
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {"DIS":{"identifier":"DIS"}}}'
, 3
, '{"field1Name": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {"DIS":{"identifier":"DIS"}}}'
, 9
, '{"interestSubscription": "USD1MD=", "interestAskField": "ASK", "interestBidField": "BID", "daysConstant": "360", "subscriptionConfig": {"DIS":{"identifier":"DIS"}, "USD1MD=":{"identifier":"USD1MD="}}}'
, 2
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {"DIS":{"identifier":"DIS"}}}'
, '{"dontCloseAfterXSecNoUpdate": "60", "timeZone": "GMT", "shouldBeClosedByMonitoring": "true"}'
, q'{{"com.anyoption.service.level.condition.AskBidDistanceMarketDisableCondition": {'subscriptionName': 'DIS', 'askField': 'ASK', 'bidField': 'BID', 'lastField': 'TRDPRC_1', 'distance': '0.02'}, "com.anyoption.service.level.condition.AskBidInverseMarketDisableCondition": {'subscriptionName': 'DIS', 'askField': 'ASK', 'bidField': 'BID'}}}'
);

INSERT INTO MARKET_DATA_SOURCE_MAPS (
  ID
, MARKET_ID
, MARKET_DATA_SOURCE_ID
, HOUR_LEVEL_FORMULA_ID
, HOUR_LEVEL_FORMULA_PARAMS
, HOUR_CLOSING_FORMULA_ID
, HOUR_CLOSING_FORMULA_PARAMS
, DAY_CLOSING_FORMULA_ID
, DAY_CLOSING_FORMULA_PARAMS
, LONG_TERM_FORMULA_ID
, LONG_TERM_FORMULA_PARAMS
, REAL_LEVEL_FORMULA_ID
, REAL_LEVEL_FORMULA_PARAMS
, MARKET_PARAMS
, MARKET_DISABLE_CONDITIONS 
) VALUES (
SEQ_MARKET_DATA_SOURCE_MAPS.NEXTVAL
, 598
, 1
, 5
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {"BAC":{"identifier":"BAC"}}}'
, 2
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {"BAC":{"identifier":"BAC"}}}'
, 3
, '{"field1Name": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {"BAC":{"identifier":"BAC"}}}'
, 9
, '{"interestSubscription": "USD1MD=", "interestAskField": "ASK", "interestBidField": "BID", "daysConstant": "360", "subscriptionConfig": {"BAC":{"identifier":"BAC"}, "USD1MD=":{"identifier":"USD1MD="}}}'
, 2
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {"BAC":{"identifier":"BAC"}}}'
, '{"dontCloseAfterXSecNoUpdate": "60", "timeZone": "GMT", "shouldBeClosedByMonitoring": "true"}'
, q'{{"com.anyoption.service.level.condition.AskBidDistanceMarketDisableCondition": {'subscriptionName': 'BAC', 'askField': 'ASK', 'bidField': 'BID', 'lastField': 'TRDPRC_1', 'distance': '0.02'}, "com.anyoption.service.level.condition.AskBidInverseMarketDisableCondition": {'subscriptionName': 'BAC', 'askField': 'ASK', 'bidField': 'BID'}}}'
);

INSERT INTO MARKET_DATA_SOURCE_MAPS (
  ID
, MARKET_ID
, MARKET_DATA_SOURCE_ID
, HOUR_LEVEL_FORMULA_ID
, HOUR_LEVEL_FORMULA_PARAMS
, HOUR_CLOSING_FORMULA_ID
, HOUR_CLOSING_FORMULA_PARAMS
, DAY_CLOSING_FORMULA_ID
, DAY_CLOSING_FORMULA_PARAMS
, LONG_TERM_FORMULA_ID
, LONG_TERM_FORMULA_PARAMS
, REAL_LEVEL_FORMULA_ID
, REAL_LEVEL_FORMULA_PARAMS
, MARKET_PARAMS
, MARKET_DISABLE_CONDITIONS 
) VALUES (
SEQ_MARKET_DATA_SOURCE_MAPS.NEXTVAL
, 628
, 1
, 5
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {"PFE":{"identifier":"PFE"}}}'
, 2
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {"PFE":{"identifier":"PFE"}}}'
, 3
, '{"field1Name": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {"PFE":{"identifier":"PFE"}}}'
, 9
, '{"interestSubscription": "USD1MD=", "interestAskField": "ASK", "interestBidField": "BID", "daysConstant": "360", "subscriptionConfig": {"PFE":{"identifier":"PFE"}, "USD1MD=":{"identifier":"USD1MD="}}}'
, 2
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {"PFE":{"identifier":"PFE"}}}'
, '{"dontCloseAfterXSecNoUpdate": "60", "timeZone": "GMT", "shouldBeClosedByMonitoring": "true"}'
, q'{{"com.anyoption.service.level.condition.AskBidDistanceMarketDisableCondition": {'subscriptionName': 'PFE', 'askField': 'ASK', 'bidField': 'BID', 'lastField': 'TRDPRC_1', 'distance': '0.02'}, "com.anyoption.service.level.condition.AskBidInverseMarketDisableCondition": {'subscriptionName': 'PFE', 'askField': 'ASK', 'bidField': 'BID'}}}'
);

INSERT INTO MARKET_DATA_SOURCE_MAPS (
  ID
, MARKET_ID
, MARKET_DATA_SOURCE_ID
, HOUR_LEVEL_FORMULA_ID
, HOUR_LEVEL_FORMULA_PARAMS
, HOUR_CLOSING_FORMULA_ID
, HOUR_CLOSING_FORMULA_PARAMS
, DAY_CLOSING_FORMULA_ID
, DAY_CLOSING_FORMULA_PARAMS
, LONG_TERM_FORMULA_ID
, LONG_TERM_FORMULA_PARAMS
, REAL_LEVEL_FORMULA_ID
, REAL_LEVEL_FORMULA_PARAMS
, MARKET_PARAMS
, MARKET_DISABLE_CONDITIONS 
) VALUES (
SEQ_MARKET_DATA_SOURCE_MAPS.NEXTVAL
, 629
, 1
, 5
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {"AIG":{"identifier":"AIG"}}}'
, 2
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {"AIG":{"identifier":"AIG"}}}'
, 3
, '{"field1Name": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {"AIG":{"identifier":"AIG"}}}'
, 9
, '{"interestSubscription": "USD1MD=", "interestAskField": "ASK", "interestBidField": "BID", "daysConstant": "360", "subscriptionConfig": {"AIG":{"identifier":"AIG"}, "USD1MD=":{"identifier":"USD1MD="}}}'
, 2
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {"AIG":{"identifier":"AIG"}}}'
, '{"dontCloseAfterXSecNoUpdate": "60", "timeZone": "GMT", "shouldBeClosedByMonitoring": "true"}'
, q'{{"com.anyoption.service.level.condition.AskBidDistanceMarketDisableCondition": {'subscriptionName': 'AIG', 'askField': 'ASK', 'bidField': 'BID', 'lastField': 'TRDPRC_1', 'distance': '0.02'}, "com.anyoption.service.level.condition.AskBidInverseMarketDisableCondition": {'subscriptionName': 'AIG', 'askField': 'ASK', 'bidField': 'BID'}}}'
);

INSERT INTO MARKET_DATA_SOURCE_MAPS (
  ID
, MARKET_ID
, MARKET_DATA_SOURCE_ID
, HOUR_LEVEL_FORMULA_ID
, HOUR_LEVEL_FORMULA_PARAMS
, HOUR_CLOSING_FORMULA_ID
, HOUR_CLOSING_FORMULA_PARAMS
, DAY_CLOSING_FORMULA_ID
, DAY_CLOSING_FORMULA_PARAMS
, LONG_TERM_FORMULA_ID
, LONG_TERM_FORMULA_PARAMS
, REAL_LEVEL_FORMULA_ID
, REAL_LEVEL_FORMULA_PARAMS
, MARKET_PARAMS
, MARKET_DISABLE_CONDITIONS 
) VALUES (
SEQ_MARKET_DATA_SOURCE_MAPS.NEXTVAL
, 632
, 1
, 5
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {"GG":{"identifier":"GG"}}}'
, 2
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {"GG":{"identifier":"GG"}}}'
, 3
, '{"field1Name": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {"GG":{"identifier":"GG"}}}'
, 9
, '{"interestSubscription": "USD1MD=", "interestAskField": "ASK", "interestBidField": "BID", "daysConstant": "360", "subscriptionConfig": {"GG":{"identifier":"GG"}, "USD1MD=":{"identifier":"USD1MD="}}}'
, 2
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {"GG":{"identifier":"GG"}}}'
, '{"dontCloseAfterXSecNoUpdate": "60", "timeZone": "GMT", "shouldBeClosedByMonitoring": "true"}'
, q'{{"com.anyoption.service.level.condition.AskBidDistanceMarketDisableCondition": {'subscriptionName': 'GG', 'askField': 'ASK', 'bidField': 'BID', 'lastField': 'TRDPRC_1', 'distance': '0.02'}, "com.anyoption.service.level.condition.AskBidInverseMarketDisableCondition": {'subscriptionName': 'GG', 'askField': 'ASK', 'bidField': 'BID'}}}'
);

INSERT INTO MARKET_DATA_SOURCE_MAPS (
  ID
, MARKET_ID
, MARKET_DATA_SOURCE_ID
, HOUR_LEVEL_FORMULA_ID
, HOUR_LEVEL_FORMULA_PARAMS
, HOUR_CLOSING_FORMULA_ID
, HOUR_CLOSING_FORMULA_PARAMS
, DAY_CLOSING_FORMULA_ID
, DAY_CLOSING_FORMULA_PARAMS
, LONG_TERM_FORMULA_ID
, LONG_TERM_FORMULA_PARAMS
, REAL_LEVEL_FORMULA_ID
, REAL_LEVEL_FORMULA_PARAMS
, MARKET_PARAMS
, MARKET_DISABLE_CONDITIONS 
) VALUES (
SEQ_MARKET_DATA_SOURCE_MAPS.NEXTVAL
, 631
, 1
, 5
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {"SLW":{"identifier":"SLW"}}}'
, 2
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {"SLW":{"identifier":"SLW"}}}'
, 3
, '{"field1Name": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {"SLW":{"identifier":"SLW"}}}'
, 9
, '{"interestSubscription": "USD1MD=", "interestAskField": "ASK", "interestBidField": "BID", "daysConstant": "360", "subscriptionConfig": {"SLW":{"identifier":"SLW"}, "USD1MD=":{"identifier":"USD1MD="}}}'
, 2
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {"SLW":{"identifier":"SLW"}}}'
, '{"dontCloseAfterXSecNoUpdate": "60", "timeZone": "GMT", "shouldBeClosedByMonitoring": "true"}'
, q'{{"com.anyoption.service.level.condition.AskBidDistanceMarketDisableCondition": {'subscriptionName': 'SLW', 'askField': 'ASK', 'bidField': 'BID', 'lastField': 'TRDPRC_1', 'distance': '0.02'}, "com.anyoption.service.level.condition.AskBidInverseMarketDisableCondition": {'subscriptionName': 'SLW', 'askField': 'ASK', 'bidField': 'BID'}}}'
);

INSERT INTO MARKET_DATA_SOURCE_MAPS (
  ID
, MARKET_ID
, MARKET_DATA_SOURCE_ID
, HOUR_LEVEL_FORMULA_ID
, HOUR_LEVEL_FORMULA_PARAMS
, HOUR_CLOSING_FORMULA_ID
, HOUR_CLOSING_FORMULA_PARAMS
, DAY_CLOSING_FORMULA_ID
, DAY_CLOSING_FORMULA_PARAMS
, LONG_TERM_FORMULA_ID
, LONG_TERM_FORMULA_PARAMS
, REAL_LEVEL_FORMULA_ID
, REAL_LEVEL_FORMULA_PARAMS
, MARKET_PARAMS
, MARKET_DISABLE_CONDITIONS 
) VALUES (
SEQ_MARKET_DATA_SOURCE_MAPS.NEXTVAL
, 591
, 1
, 5
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {"GS":{"identifier":"GS"}}}'
, 2
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {"GS":{"identifier":"GS"}}}'
, 3
, '{"field1Name": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {"GS":{"identifier":"GS"}}}'
, 9
, '{"interestSubscription": "USD1MD=", "interestAskField": "ASK", "interestBidField": "BID", "daysConstant": "360", "subscriptionConfig": {"GS":{"identifier":"GS"}, "USD1MD=":{"identifier":"USD1MD="}}}'
, 2
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {"GS":{"identifier":"GS"}}}'
, '{"dontCloseAfterXSecNoUpdate": "60", "timeZone": "GMT", "shouldBeClosedByMonitoring": "true"}'
, q'{{"com.anyoption.service.level.condition.AskBidDistanceMarketDisableCondition": {'subscriptionName': 'GS', 'askField': 'ASK', 'bidField': 'BID', 'lastField': 'TRDPRC_1', 'distance': '0.02'}, "com.anyoption.service.level.condition.AskBidInverseMarketDisableCondition": {'subscriptionName': 'GS', 'askField': 'ASK', 'bidField': 'BID'}}}'
);

INSERT INTO MARKET_DATA_SOURCE_MAPS (
  ID
, MARKET_ID
, MARKET_DATA_SOURCE_ID
, HOUR_LEVEL_FORMULA_ID
, HOUR_LEVEL_FORMULA_PARAMS
, HOUR_CLOSING_FORMULA_ID
, HOUR_CLOSING_FORMULA_PARAMS
, DAY_CLOSING_FORMULA_ID
, DAY_CLOSING_FORMULA_PARAMS
, LONG_TERM_FORMULA_ID
, LONG_TERM_FORMULA_PARAMS
, REAL_LEVEL_FORMULA_ID
, REAL_LEVEL_FORMULA_PARAMS
, MARKET_PARAMS
, MARKET_DISABLE_CONDITIONS 
) VALUES (
SEQ_MARKET_DATA_SOURCE_MAPS.NEXTVAL
, 658
, 1
, 5
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {"PHG.AS":{"identifier":"PHG.AS"}}}'
, 2
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {"PHG.AS":{"identifier":"PHG.AS"}}}'
, 3
, '{"field1Name": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {"PHG.AS":{"identifier":"PHG.AS"}}}'
, 9
, '{"interestSubscription": "EUR1MD=", "interestAskField": "ASK", "interestBidField": "BID", "daysConstant": "360", "subscriptionConfig": {"PHG.AS":{"identifier":"PHG.AS"}, "EUR1MD=":{"identifier":"EUR1MD="}}}'
, 2
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {"PHG.AS":{"identifier":"PHG.AS"}}}'
, '{"dontCloseAfterXSecNoUpdate": "60", "timeZone": "GMT", "shouldBeClosedByMonitoring": "true"}'
, q'{{"com.anyoption.service.level.condition.AskBidDistanceMarketDisableCondition": {'subscriptionName': 'PHG.AS', 'askField': 'ASK', 'bidField': 'BID', 'lastField': 'TRDPRC_1', 'distance': '0.02'}, "com.anyoption.service.level.condition.AskBidInverseMarketDisableCondition": {'subscriptionName': 'PHG.AS', 'askField': 'ASK', 'bidField': 'BID'}}}'
);

INSERT INTO MARKET_DATA_SOURCE_MAPS (
  ID
, MARKET_ID
, MARKET_DATA_SOURCE_ID
, HOUR_LEVEL_FORMULA_ID
, HOUR_LEVEL_FORMULA_PARAMS
, HOUR_CLOSING_FORMULA_ID
, HOUR_CLOSING_FORMULA_PARAMS
, DAY_CLOSING_FORMULA_ID
, DAY_CLOSING_FORMULA_PARAMS
, LONG_TERM_FORMULA_ID
, LONG_TERM_FORMULA_PARAMS
, REAL_LEVEL_FORMULA_ID
, REAL_LEVEL_FORMULA_PARAMS
, MARKET_PARAMS
, MARKET_DISABLE_CONDITIONS 
) VALUES (
SEQ_MARKET_DATA_SOURCE_MAPS.NEXTVAL
, 659
, 1
, 5
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {"HEIN.AS":{"identifier":"HEIN.AS"}}}'
, 2
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {"HEIN.AS":{"identifier":"HEIN.AS"}}}'
, 3
, '{"field1Name": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {"HEIN.AS":{"identifier":"HEIN.AS"}}}'
, 9
, '{"interestSubscription": "EUR1MD=", "interestAskField": "ASK", "interestBidField": "BID", "daysConstant": "360", "subscriptionConfig": {"HEIN.AS":{"identifier":"HEIN.AS"}, "EUR1MD=":{"identifier":"EUR1MD="}}}'
, 2
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {"HEIN.AS":{"identifier":"HEIN.AS"}}}'
, '{"dontCloseAfterXSecNoUpdate": "60", "timeZone": "GMT", "shouldBeClosedByMonitoring": "true"}'
, q'{{"com.anyoption.service.level.condition.AskBidDistanceMarketDisableCondition": {'subscriptionName': 'HEIN.AS', 'askField': 'ASK', 'bidField': 'BID', 'lastField': 'TRDPRC_1', 'distance': '0.02'}, "com.anyoption.service.level.condition.AskBidInverseMarketDisableCondition": {'subscriptionName': 'HEIN.AS', 'askField': 'ASK', 'bidField': 'BID'}}}'
);

INSERT INTO MARKET_DATA_SOURCE_MAPS (
  ID
, MARKET_ID
, MARKET_DATA_SOURCE_ID
, HOUR_LEVEL_FORMULA_ID
, HOUR_LEVEL_FORMULA_PARAMS
, HOUR_CLOSING_FORMULA_ID
, HOUR_CLOSING_FORMULA_PARAMS
, DAY_CLOSING_FORMULA_ID
, DAY_CLOSING_FORMULA_PARAMS
, LONG_TERM_FORMULA_ID
, LONG_TERM_FORMULA_PARAMS
, REAL_LEVEL_FORMULA_ID
, REAL_LEVEL_FORMULA_PARAMS
, MARKET_PARAMS
, MARKET_DISABLE_CONDITIONS 
) VALUES (
SEQ_MARKET_DATA_SOURCE_MAPS.NEXTVAL
, 660
, 1
, 5
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {"UNc.AS":{"identifier":"UNc.AS"}}}'
, 2
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {"UNc.AS":{"identifier":"UNc.AS"}}}'
, 3
, '{"field1Name": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {"UNc.AS":{"identifier":"UNc.AS"}}}'
, 9
, '{"interestSubscription": "EUR1MD=", "interestAskField": "ASK", "interestBidField": "BID", "daysConstant": "360", "subscriptionConfig": {"UNc.AS":{"identifier":"UNc.AS"}, "EUR1MD=":{"identifier":"EUR1MD="}}}'
, 2
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {"UNc.AS":{"identifier":"UNc.AS"}}}'
, '{"dontCloseAfterXSecNoUpdate": "60", "timeZone": "GMT", "shouldBeClosedByMonitoring": "true"}'
, q'{{"com.anyoption.service.level.condition.AskBidDistanceMarketDisableCondition": {'subscriptionName': 'UNc.AS', 'askField': 'ASK', 'bidField': 'BID', 'lastField': 'TRDPRC_1', 'distance': '0.02'}, "com.anyoption.service.level.condition.AskBidInverseMarketDisableCondition": {'subscriptionName': 'UNc.AS', 'askField': 'ASK', 'bidField': 'BID'}}}'
);

INSERT INTO MARKET_DATA_SOURCE_MAPS (
  ID
, MARKET_ID
, MARKET_DATA_SOURCE_ID
, HOUR_LEVEL_FORMULA_ID
, HOUR_LEVEL_FORMULA_PARAMS
, HOUR_CLOSING_FORMULA_ID
, HOUR_CLOSING_FORMULA_PARAMS
, DAY_CLOSING_FORMULA_ID
, DAY_CLOSING_FORMULA_PARAMS
, LONG_TERM_FORMULA_ID
, LONG_TERM_FORMULA_PARAMS
, REAL_LEVEL_FORMULA_ID
, REAL_LEVEL_FORMULA_PARAMS
, MARKET_PARAMS
, MARKET_DISABLE_CONDITIONS 
) VALUES (
SEQ_MARKET_DATA_SOURCE_MAPS.NEXTVAL
, 594
, 1
, 5
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {"TEVA.K":{"identifier":"TEVA.K"}}}'
, 2
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {"TEVA.K":{"identifier":"TEVA.K"}}}'
, 3
, '{"field1Name": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {"TEVA.K":{"identifier":"TEVA.K"}}}'
, 9
, '{"interestSubscription": "USD1MD=", "interestAskField": "ASK", "interestBidField": "BID", "daysConstant": "360", "subscriptionConfig": {"TEVA.K":{"identifier":"TEVA.K"}, "USD1MD=":{"identifier":"USD1MD="}}}'
, 2
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {"TEVA.K":{"identifier":"TEVA.K"}}}'
, '{"dontCloseAfterXSecNoUpdate": "60", "timeZone": "GMT", "shouldBeClosedByMonitoring": "true"}'
, q'{{"com.anyoption.service.level.condition.AskBidDistanceMarketDisableCondition": {'subscriptionName': 'TEVA.K', 'askField': 'ASK', 'bidField': 'BID', 'lastField': 'TRDPRC_1', 'distance': '0.02'}, "com.anyoption.service.level.condition.AskBidInverseMarketDisableCondition": {'subscriptionName': 'TEVA.K', 'askField': 'ASK', 'bidField': 'BID'}}}'
);

INSERT INTO MARKET_DATA_SOURCE_MAPS (
  ID
, MARKET_ID
, MARKET_DATA_SOURCE_ID
, HOUR_LEVEL_FORMULA_ID
, HOUR_LEVEL_FORMULA_PARAMS
, HOUR_CLOSING_FORMULA_ID
, HOUR_CLOSING_FORMULA_PARAMS
, DAY_CLOSING_FORMULA_ID
, DAY_CLOSING_FORMULA_PARAMS
, LONG_TERM_FORMULA_ID
, LONG_TERM_FORMULA_PARAMS
, REAL_LEVEL_FORMULA_ID
, REAL_LEVEL_FORMULA_PARAMS
, MARKET_PARAMS
, MARKET_DISABLE_CONDITIONS 
) VALUES (
SEQ_MARKET_DATA_SOURCE_MAPS.NEXTVAL
, 648
, 1
, 5
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {"TWTR.K":{"identifier":"TWTR.K"}}}'
, 2
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {"TWTR.K":{"identifier":"TWTR.K"}}}'
, 3
, '{"field1Name": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {"TWTR.K":{"identifier":"TWTR.K"}}}'
, 9
, '{"interestSubscription": "USD1MD=", "interestAskField": "ASK", "interestBidField": "BID", "daysConstant": "360", "subscriptionConfig": {"TWTR.K":{"identifier":"TWTR.K"}, "USD1MD=":{"identifier":"USD1MD="}}}'
, 2
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {"TWTR.K":{"identifier":"TWTR.K"}}}'
, '{"dontCloseAfterXSecNoUpdate": "60", "timeZone": "GMT", "shouldBeClosedByMonitoring": "true"}'
, q'{{"com.anyoption.service.level.condition.AskBidDistanceMarketDisableCondition": {'subscriptionName': 'TWTR.K', 'askField': 'ASK', 'bidField': 'BID', 'lastField': 'TRDPRC_1', 'distance': '0.02'}, "com.anyoption.service.level.condition.AskBidInverseMarketDisableCondition": {'subscriptionName': 'TWTR.K', 'askField': 'ASK', 'bidField': 'BID'}}}'
);

INSERT INTO MARKET_DATA_SOURCE_MAPS (
  ID
, MARKET_ID
, MARKET_DATA_SOURCE_ID
, HOUR_LEVEL_FORMULA_ID
, HOUR_LEVEL_FORMULA_PARAMS
, HOUR_CLOSING_FORMULA_ID
, HOUR_CLOSING_FORMULA_PARAMS
, DAY_CLOSING_FORMULA_ID
, DAY_CLOSING_FORMULA_PARAMS
, LONG_TERM_FORMULA_ID
, LONG_TERM_FORMULA_PARAMS
, REAL_LEVEL_FORMULA_ID
, REAL_LEVEL_FORMULA_PARAMS
, MARKET_PARAMS
, MARKET_DISABLE_CONDITIONS 
) VALUES (
SEQ_MARKET_DATA_SOURCE_MAPS.NEXTVAL
, 679
, 1
, 5
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {"BABA.K":{"identifier":"BABA.K"}}}'
, 2
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {"BABA.K":{"identifier":"BABA.K"}}}'
, 3
, '{"field1Name": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {"BABA.K":{"identifier":"BABA.K"}}}'
, 9
, '{"interestSubscription": "USD1MD=", "interestAskField": "ASK", "interestBidField": "BID", "daysConstant": "360", "subscriptionConfig": {"BABA.K":{"identifier":"BABA.K"}, "USD1MD=":{"identifier":"USD1MD="}}}'
, 2
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {"BABA.K":{"identifier":"BABA.K"}}}'
, '{"dontCloseAfterXSecNoUpdate": "60", "timeZone": "GMT", "shouldBeClosedByMonitoring": "true"}'
, q'{{"com.anyoption.service.level.condition.AskBidDistanceMarketDisableCondition": {'subscriptionName': 'BABA.K', 'askField': 'ASK', 'bidField': 'BID', 'lastField': 'TRDPRC_1', 'distance': '0.02'}, "com.anyoption.service.level.condition.AskBidInverseMarketDisableCondition": {'subscriptionName': 'BABA.K', 'askField': 'ASK', 'bidField': 'BID'}}}'
);

INSERT INTO MARKET_DATA_SOURCE_MAPS (
  ID
, MARKET_ID
, MARKET_DATA_SOURCE_ID
, HOUR_LEVEL_FORMULA_ID
, HOUR_LEVEL_FORMULA_PARAMS
, HOUR_CLOSING_FORMULA_ID
, HOUR_CLOSING_FORMULA_PARAMS
, DAY_CLOSING_FORMULA_ID
, DAY_CLOSING_FORMULA_PARAMS
, LONG_TERM_FORMULA_ID
, LONG_TERM_FORMULA_PARAMS
, REAL_LEVEL_FORMULA_ID
, REAL_LEVEL_FORMULA_PARAMS
, MARKET_PARAMS
, MARKET_DISABLE_CONDITIONS 
) VALUES (
SEQ_MARKET_DATA_SOURCE_MAPS.NEXTVAL
, 705
, 1
, 5
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {"RACE.K":{"identifier":"RACE.K"}}}'
, 2
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {"RACE.K":{"identifier":"RACE.K"}}}'
, 3
, '{"field1Name": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {"RACE.K":{"identifier":"RACE.K"}}}'
, 9
, '{"interestSubscription": "USD1MD=", "interestAskField": "ASK", "interestBidField": "BID", "daysConstant": "360", "subscriptionConfig": {"RACE.K":{"identifier":"RACE.K"}, "USD1MD=":{"identifier":"USD1MD="}}}'
, 2
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {"RACE.K":{"identifier":"RACE.K"}}}'
, '{"dontCloseAfterXSecNoUpdate": "60", "timeZone": "GMT", "shouldBeClosedByMonitoring": "true"}'
, q'{{"com.anyoption.service.level.condition.AskBidDistanceMarketDisableCondition": {'subscriptionName': 'RACE.K', 'askField': 'ASK', 'bidField': 'BID', 'lastField': 'TRDPRC_1', 'distance': '0.02'}, "com.anyoption.service.level.condition.AskBidInverseMarketDisableCondition": {'subscriptionName': 'RACE.K', 'askField': 'ASK', 'bidField': 'BID'}}}'
);

INSERT INTO MARKET_DATA_SOURCE_MAPS (
  ID
, MARKET_ID
, MARKET_DATA_SOURCE_ID
, HOUR_LEVEL_FORMULA_ID
, HOUR_LEVEL_FORMULA_PARAMS
, HOUR_CLOSING_FORMULA_ID
, HOUR_CLOSING_FORMULA_PARAMS
, DAY_CLOSING_FORMULA_ID
, DAY_CLOSING_FORMULA_PARAMS
, LONG_TERM_FORMULA_ID
, LONG_TERM_FORMULA_PARAMS
, REAL_LEVEL_FORMULA_ID
, REAL_LEVEL_FORMULA_PARAMS
, MARKET_PARAMS
, MARKET_DISABLE_CONDITIONS 
) VALUES (
SEQ_MARKET_DATA_SOURCE_MAPS.NEXTVAL
, 695
, 1
, 5
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {"PEP":{"identifier":"PEP"}}}'
, 2
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {"PEP":{"identifier":"PEP"}}}'
, 3
, '{"field1Name": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {"PEP":{"identifier":"PEP"}}}'
, 9
, '{"interestSubscription": "USD1MD=", "interestAskField": "ASK", "interestBidField": "BID", "daysConstant": "360", "subscriptionConfig": {"PEP":{"identifier":"PEP"}, "USD1MD=":{"identifier":"USD1MD="}}}'
, 2
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {"PEP":{"identifier":"PEP"}}}'
, '{"dontCloseAfterXSecNoUpdate": "60", "timeZone": "GMT", "shouldBeClosedByMonitoring": "true"}'
, q'{{"com.anyoption.service.level.condition.AskBidDistanceMarketDisableCondition": {'subscriptionName': 'PEP', 'askField': 'ASK', 'bidField': 'BID', 'lastField': 'TRDPRC_1', 'distance': '0.02'}, "com.anyoption.service.level.condition.AskBidInverseMarketDisableCondition": {'subscriptionName': 'PEP', 'askField': 'ASK', 'bidField': 'BID'}}}'
);

INSERT INTO MARKET_DATA_SOURCE_MAPS (
  ID
, MARKET_ID
, MARKET_DATA_SOURCE_ID
, HOUR_LEVEL_FORMULA_ID
, HOUR_LEVEL_FORMULA_PARAMS
, HOUR_CLOSING_FORMULA_ID
, HOUR_CLOSING_FORMULA_PARAMS
, DAY_CLOSING_FORMULA_ID
, DAY_CLOSING_FORMULA_PARAMS
, LONG_TERM_FORMULA_ID
, LONG_TERM_FORMULA_PARAMS
, REAL_LEVEL_FORMULA_ID
, REAL_LEVEL_FORMULA_PARAMS
, MARKET_PARAMS
, MARKET_DISABLE_CONDITIONS 
) VALUES (
SEQ_MARKET_DATA_SOURCE_MAPS.NEXTVAL
, 626
, 1
, 5
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {"CSCO.O":{"identifier":"CSCO.O"}}}'
, 2
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {"CSCO.O":{"identifier":"CSCO.O"}}}'
, 3
, '{"field1Name": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {"CSCO.O":{"identifier":"CSCO.O"}}}'
, 9
, '{"interestSubscription": "USD1MD=", "interestAskField": "ASK", "interestBidField": "BID", "daysConstant": "360", "subscriptionConfig": {"CSCO.O":{"identifier":"CSCO.O"}, "USD1MD=":{"identifier":"USD1MD="}}}'
, 2
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {"CSCO.O":{"identifier":"CSCO.O"}}}'
, '{"dontCloseAfterXSecNoUpdate": "60", "timeZone": "GMT", "shouldBeClosedByMonitoring": "true"}'
, q'{{"com.anyoption.service.level.condition.AskBidDistanceMarketDisableCondition": {'subscriptionName': 'CSCO.O', 'askField': 'ASK', 'bidField': 'BID', 'lastField': 'TRDPRC_1', 'distance': '0.02'}, "com.anyoption.service.level.condition.AskBidInverseMarketDisableCondition": {'subscriptionName': 'CSCO.O', 'askField': 'ASK', 'bidField': 'BID'}}}'
);
-- End of C like stocks

-- VOD.L like stocks
INSERT INTO MARKET_DATA_SOURCE_MAPS (
  ID
, MARKET_ID
, MARKET_DATA_SOURCE_ID
, HOUR_LEVEL_FORMULA_ID
, HOUR_LEVEL_FORMULA_PARAMS
, HOUR_CLOSING_FORMULA_ID
, HOUR_CLOSING_FORMULA_PARAMS
, DAY_CLOSING_FORMULA_ID
, DAY_CLOSING_FORMULA_PARAMS
, LONG_TERM_FORMULA_ID
, LONG_TERM_FORMULA_PARAMS
, REAL_LEVEL_FORMULA_ID
, REAL_LEVEL_FORMULA_PARAMS
, MARKET_PARAMS
, MARKET_DISABLE_CONDITIONS 
) VALUES (
SEQ_MARKET_DATA_SOURCE_MAPS.NEXTVAL
, 133
, 1
, 5
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {"VOD.L":{"identifier":"VOD.L"}}}'
, 2
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {"VOD.L":{"identifier":"VOD.L"}}}'
, 12
, '{"field1Name": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "specificValueName": "GEN_VAL1", "presentDayFieldName": "GV1_DATE", "secondTimeFieldName": "EXCHTIM", "subscriptionConfig": {"VOD.L":{"identifier":"VOD.L"}}}'
, 9
, '{"interestSubscription": "GBP1MD=", "interestAskField": "ASK", "interestBidField": "BID", "daysConstant": "365", "subscriptionConfig": {"VOD.L":{"identifier":"VOD.L"}, "GBP1MD=":{"identifier":"GBP1MD="}}}'
, 2
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {"VOD.L":{"identifier":"VOD.L"}}}'
, '{"dontCloseAfterXSecNoUpdate": "60", "timeZone": "GMT", "shouldBeClosedByMonitoring": "true"}'
, q'{{"com.anyoption.service.level.condition.AskBidDistanceMarketDisableCondition": {'subscriptionName': 'VOD.L', 'askField': 'ASK', 'bidField': 'BID', 'lastField': 'TRDPRC_1', 'distance': '0.02'}, "com.anyoption.service.level.condition.AskBidInverseMarketDisableCondition": {'subscriptionName': 'VOD.L', 'askField': 'ASK', 'bidField': 'BID'}}}'
);

INSERT INTO MARKET_DATA_SOURCE_MAPS (
  ID
, MARKET_ID
, MARKET_DATA_SOURCE_ID
, HOUR_LEVEL_FORMULA_ID
, HOUR_LEVEL_FORMULA_PARAMS
, HOUR_CLOSING_FORMULA_ID
, HOUR_CLOSING_FORMULA_PARAMS
, DAY_CLOSING_FORMULA_ID
, DAY_CLOSING_FORMULA_PARAMS
, LONG_TERM_FORMULA_ID
, LONG_TERM_FORMULA_PARAMS
, REAL_LEVEL_FORMULA_ID
, REAL_LEVEL_FORMULA_PARAMS
, MARKET_PARAMS
, MARKET_DISABLE_CONDITIONS 
) VALUES (
SEQ_MARKET_DATA_SOURCE_MAPS.NEXTVAL
, 251
, 1
, 5
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {"BARC.L":{"identifier":"BARC.L"}}}'
, 2
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {"BARC.L":{"identifier":"BARC.L"}}}'
, 12
, '{"field1Name": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "specificValueName": "GEN_VAL1", "presentDayFieldName": "GV1_DATE", "secondTimeFieldName": "EXCHTIM", "subscriptionConfig": {"BARC.L":{"identifier":"BARC.L"}}}'
, 9
, '{"interestSubscription": "GBP1MD=", "interestAskField": "ASK", "interestBidField": "BID", "daysConstant": "365", "subscriptionConfig": {"BARC.L":{"identifier":"BARC.L"}, "GBP1MD=":{"identifier":"GBP1MD="}}}'
, 2
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {"BARC.L":{"identifier":"BARC.L"}}}'
, '{"dontCloseAfterXSecNoUpdate": "60", "timeZone": "GMT", "shouldBeClosedByMonitoring": "true"}'
, q'{{"com.anyoption.service.level.condition.AskBidDistanceMarketDisableCondition": {'subscriptionName': 'BARC.L', 'askField': 'ASK', 'bidField': 'BID', 'lastField': 'TRDPRC_1', 'distance': '0.02'}, "com.anyoption.service.level.condition.AskBidInverseMarketDisableCondition": {'subscriptionName': 'BARC.L', 'askField': 'ASK', 'bidField': 'BID'}}}'
);

INSERT INTO MARKET_DATA_SOURCE_MAPS (
  ID
, MARKET_ID
, MARKET_DATA_SOURCE_ID
, HOUR_LEVEL_FORMULA_ID
, HOUR_LEVEL_FORMULA_PARAMS
, HOUR_CLOSING_FORMULA_ID
, HOUR_CLOSING_FORMULA_PARAMS
, DAY_CLOSING_FORMULA_ID
, DAY_CLOSING_FORMULA_PARAMS
, LONG_TERM_FORMULA_ID
, LONG_TERM_FORMULA_PARAMS
, REAL_LEVEL_FORMULA_ID
, REAL_LEVEL_FORMULA_PARAMS
, MARKET_PARAMS
, MARKET_DISABLE_CONDITIONS 
) VALUES (
SEQ_MARKET_DATA_SOURCE_MAPS.NEXTVAL
, 415
, 1
, 5
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {"BP.L":{"identifier":"BP.L"}}}'
, 2
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {"BP.L":{"identifier":"BP.L"}}}'
, 12
, '{"field1Name": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "specificValueName": "GEN_VAL1", "presentDayFieldName": "GV1_DATE", "secondTimeFieldName": "EXCHTIM", "subscriptionConfig": {"BP.L":{"identifier":"BP.L"}}}'
, 9
, '{"interestSubscription": "GBP1MD=", "interestAskField": "ASK", "interestBidField": "BID", "daysConstant": "365", "subscriptionConfig": {"BP.L":{"identifier":"BP.L"}, "GBP1MD=":{"identifier":"GBP1MD="}}}'
, 2
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {"BP.L":{"identifier":"BP.L"}}}'
, '{"dontCloseAfterXSecNoUpdate": "120", "timeZone": "GMT", "shouldBeClosedByMonitoring": "true"}'
, q'{{"com.anyoption.service.level.condition.AskBidDistanceMarketDisableCondition": {'subscriptionName': 'BP.L', 'askField': 'ASK', 'bidField': 'BID', 'lastField': 'TRDPRC_1', 'distance': '0.02'}, "com.anyoption.service.level.condition.AskBidInverseMarketDisableCondition": {'subscriptionName': 'BP.L', 'askField': 'ASK', 'bidField': 'BID'}}}'
);

INSERT INTO MARKET_DATA_SOURCE_MAPS (
  ID
, MARKET_ID
, MARKET_DATA_SOURCE_ID
, HOUR_LEVEL_FORMULA_ID
, HOUR_LEVEL_FORMULA_PARAMS
, HOUR_CLOSING_FORMULA_ID
, HOUR_CLOSING_FORMULA_PARAMS
, DAY_CLOSING_FORMULA_ID
, DAY_CLOSING_FORMULA_PARAMS
, LONG_TERM_FORMULA_ID
, LONG_TERM_FORMULA_PARAMS
, REAL_LEVEL_FORMULA_ID
, REAL_LEVEL_FORMULA_PARAMS
, MARKET_PARAMS
, MARKET_DISABLE_CONDITIONS 
) VALUES (
SEQ_MARKET_DATA_SOURCE_MAPS.NEXTVAL
, 595
, 1
, 5
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {"TSCO.L":{"identifier":"TSCO.L"}}}'
, 2
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {"TSCO.L":{"identifier":"TSCO.L"}}}'
, 12
, '{"field1Name": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "specificValueName": "GEN_VAL1", "presentDayFieldName": "GV1_DATE", "secondTimeFieldName": "EXCHTIM", "subscriptionConfig": {"TSCO.L":{"identifier":"TSCO.L"}}}'
, 9
, '{"interestSubscription": "GBP1MD=", "interestAskField": "ASK", "interestBidField": "BID", "daysConstant": "365", "subscriptionConfig": {"TSCO.L":{"identifier":"TSCO.L"}, "GBP1MD=":{"identifier":"GBP1MD="}}}'
, 2
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {"TSCO.L":{"identifier":"TSCO.L"}}}'
, '{"dontCloseAfterXSecNoUpdate": "60", "timeZone": "GMT", "shouldBeClosedByMonitoring": "true"}'
, q'{{"com.anyoption.service.level.condition.AskBidDistanceMarketDisableCondition": {'subscriptionName': 'TSCO.L', 'askField': 'ASK', 'bidField': 'BID', 'lastField': 'TRDPRC_1', 'distance': '0.02'}, "com.anyoption.service.level.condition.AskBidInverseMarketDisableCondition": {'subscriptionName': 'TSCO.L', 'askField': 'ASK', 'bidField': 'BID'}}}'
);
-- End of VOD.L like stocks

-- NQ like indices
INSERT INTO MARKET_DATA_SOURCE_MAPS (
  ID
, MARKET_ID
, MARKET_DATA_SOURCE_ID
, HOUR_LEVEL_FORMULA_ID
, HOUR_LEVEL_FORMULA_PARAMS
, HOUR_CLOSING_FORMULA_ID
, HOUR_CLOSING_FORMULA_PARAMS
, DAY_CLOSING_FORMULA_ID
, DAY_CLOSING_FORMULA_PARAMS
, LONG_TERM_FORMULA_ID
, LONG_TERM_FORMULA_PARAMS
, REAL_LEVEL_FORMULA_ID
, REAL_LEVEL_FORMULA_PARAMS
, MARKET_PARAMS
, MARKET_DISABLE_CONDITIONS 
) VALUES (
SEQ_MARKET_DATA_SOURCE_MAPS.NEXTVAL
, 136
, 1
, 2
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {"NQc1":{"identifier":"NQc1","switchToFuture":"true","switchToFuturePrefixLength":"2"}}}'
, 2
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {"NQc1":{"identifier":"NQc1","switchToFuture":"true","switchToFuturePrefixLength":"2"}}}'
, 2
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {"NQc1":{"identifier":"NQc1","switchToFuture":"true","switchToFuturePrefixLength":"2"}}}'
, null
, null
, 3
, '{"field1Name": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {"NQc1":{"identifier":"NQc1","switchToFuture":"true","switchToFuturePrefixLength":"2"}}}'
, '{"dontCloseAfterXSecNoUpdate": "60", "timeZone": "GMT", "shouldBeClosedByMonitoring": "true"}'
, q'{{"com.anyoption.service.level.condition.AskBidSpreadMarketDisableCondition": {'subscriptionName': 'NQc1', 'switchToFuture':'true', 'switchToFuturePrefixLength':'2', 'askField': 'ASK', 'bidField': 'BID', 'spread': '1.7'}}}'
);

INSERT INTO MARKET_DATA_SOURCE_MAPS (
  ID
, MARKET_ID
, MARKET_DATA_SOURCE_ID
, HOUR_LEVEL_FORMULA_ID
, HOUR_LEVEL_FORMULA_PARAMS
, HOUR_CLOSING_FORMULA_ID
, HOUR_CLOSING_FORMULA_PARAMS
, DAY_CLOSING_FORMULA_ID
, DAY_CLOSING_FORMULA_PARAMS
, LONG_TERM_FORMULA_ID
, LONG_TERM_FORMULA_PARAMS
, REAL_LEVEL_FORMULA_ID
, REAL_LEVEL_FORMULA_PARAMS
, MARKET_PARAMS
, MARKET_DISABLE_CONDITIONS 
) VALUES (
SEQ_MARKET_DATA_SOURCE_MAPS.NEXTVAL
, 541
, 1
, 2
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {"FDXc1":{"identifier":"FDXc1","switchToFuture":"true","switchToFuturePrefixLength":"2"}}}'
, 2
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {"FDXc1":{"identifier":"FDXc1","switchToFuture":"true","switchToFuturePrefixLength":"2"}}}'
, 2
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {"FDXc1":{"identifier":"FDXc1","switchToFuture":"true","switchToFuturePrefixLength":"2"}}}'
, null
, null
, 3
, '{"field1Name": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {"FDXc1":{"identifier":"FDXc1","switchToFuture":"true","switchToFuturePrefixLength":"2"}}}'
, '{"dontCloseAfterXSecNoUpdate": "60", "timeZone": "GMT", "shouldBeClosedByMonitoring": "true"}'
, q'{{"com.anyoption.service.level.condition.AskBidSpreadMarketDisableCondition": {'subscriptionName': 'FDXc1', 'switchToFuture':'true', 'switchToFuturePrefixLength':'2', 'askField': 'ASK', 'bidField': 'BID', 'spread': '3.4'}}}'
);

INSERT INTO MARKET_DATA_SOURCE_MAPS (
  ID
, MARKET_ID
, MARKET_DATA_SOURCE_ID
, HOUR_LEVEL_FORMULA_ID
, HOUR_LEVEL_FORMULA_PARAMS
, HOUR_CLOSING_FORMULA_ID
, HOUR_CLOSING_FORMULA_PARAMS
, DAY_CLOSING_FORMULA_ID
, DAY_CLOSING_FORMULA_PARAMS
, LONG_TERM_FORMULA_ID
, LONG_TERM_FORMULA_PARAMS
, REAL_LEVEL_FORMULA_ID
, REAL_LEVEL_FORMULA_PARAMS
, MARKET_PARAMS
, MARKET_DISABLE_CONDITIONS 
) VALUES (
SEQ_MARKET_DATA_SOURCE_MAPS.NEXTVAL
, 542
, 1
, 2
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {"ESc1":{"identifier":"ESc1","switchToFuture":"true","switchToFuturePrefixLength":"2"}}}'
, 2
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {"ESc1":{"identifier":"ESc1","switchToFuture":"true","switchToFuturePrefixLength":"2"}}}'
, 2
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {"ESc1":{"identifier":"ESc1","switchToFuture":"true","switchToFuturePrefixLength":"2"}}}'
, null
, null
, 3
, '{"field1Name": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {"ESc1":{"identifier":"ESc1","switchToFuture":"true","switchToFuturePrefixLength":"2"}}}'
, '{"dontCloseAfterXSecNoUpdate": "60", "timeZone": "GMT", "shouldBeClosedByMonitoring": "true"}'
, q'{{"com.anyoption.service.level.condition.AskBidSpreadMarketDisableCondition": {'subscriptionName': 'ESc1', 'switchToFuture':'true', 'switchToFuturePrefixLength':'2', 'askField': 'ASK', 'bidField': 'BID', 'spread': '1.7'}}}'
);

INSERT INTO MARKET_DATA_SOURCE_MAPS (
  ID
, MARKET_ID
, MARKET_DATA_SOURCE_ID
, HOUR_LEVEL_FORMULA_ID
, HOUR_LEVEL_FORMULA_PARAMS
, HOUR_CLOSING_FORMULA_ID
, HOUR_CLOSING_FORMULA_PARAMS
, DAY_CLOSING_FORMULA_ID
, DAY_CLOSING_FORMULA_PARAMS
, LONG_TERM_FORMULA_ID
, LONG_TERM_FORMULA_PARAMS
, REAL_LEVEL_FORMULA_ID
, REAL_LEVEL_FORMULA_PARAMS
, MARKET_PARAMS
, MARKET_DISABLE_CONDITIONS 
) VALUES (
SEQ_MARKET_DATA_SOURCE_MAPS.NEXTVAL
, 588
, 1
, 2
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {"FCEc1":{"identifier":"FCEc1","switchToFuture":"true","switchToFuturePrefixLength":"3"}}}'
, 2
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {"FCEc1":{"identifier":"FCEc1","switchToFuture":"true","switchToFuturePrefixLength":"3"}}}'
, 2
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {"FCEc1":{"identifier":"FCEc1","switchToFuture":"true","switchToFuturePrefixLength":"3"}}}'
, null
, null
, 3
, '{"field1Name": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {"FCEc1":{"identifier":"FCEc1","switchToFuture":"true","switchToFuturePrefixLength":"2"}}}'
, '{"dontCloseAfterXSecNoUpdate": "60", "timeZone": "GMT", "shouldBeClosedByMonitoring": "true"}'
, q'{{"com.anyoption.service.level.condition.AskBidSpreadMarketDisableCondition": {'subscriptionName': 'FCEc1', 'switchToFuture':'true', 'switchToFuturePrefixLength':'2', 'askField': 'ASK', 'bidField': 'BID', 'spread': '1.7'}}}'
);
-- End of NQ like indices

-- .SPX
INSERT INTO MARKET_DATA_SOURCE_MAPS (
  ID
, MARKET_ID
, MARKET_DATA_SOURCE_ID
, HOUR_LEVEL_FORMULA_ID
, HOUR_LEVEL_FORMULA_PARAMS
, HOUR_CLOSING_FORMULA_ID
, HOUR_CLOSING_FORMULA_PARAMS
, DAY_CLOSING_FORMULA_ID
, DAY_CLOSING_FORMULA_PARAMS
, LONG_TERM_FORMULA_ID
, LONG_TERM_FORMULA_PARAMS
, REAL_LEVEL_FORMULA_ID
, REAL_LEVEL_FORMULA_PARAMS
, MARKET_PARAMS
, MARKET_DISABLE_CONDITIONS 
) VALUES (
SEQ_MARKET_DATA_SOURCE_MAPS.NEXTVAL
, 6
, 1
, 6
, '{"firstSubscriptionName": ".SPX", "firstSubscriptionField1": "TRDPRC_1", "secondSubscriptionField1": "ASK", "secondSubscriptionField2": "BID", "secondSubscriptionField3": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "traderParamName": "sp500", "subscriptionConfig": {".SPX":{"identifier":".SPX"}, "ESc1": {"identifier": "ESc1", "switchToFuture":"true", "switchToFuturePrefixLength":"2"}}}'
, 3
, '{"field1Name": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {".SPX":{"identifier":".SPX"}}}'
, 3
, '{"field1Name": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {".SPX":{"identifier":".SPX"}}}'
, 9
, '{"interestSubscription": "USD1MD=", "interestAskField": "ASK", "interestBidField": "BID", "daysConstant": "360", "subscriptionConfig": {".SPX":{"identifier":".SPX"}, "USD1MD=":{"identifier":"USD1MD="}}}'
, 3
, '{"field1Name": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {".SPX":{"identifier":".SPX"}}}'
, '{"dontCloseAfterXSecNoUpdate": "60", "timeZone": "GMT", "shouldBeClosedByMonitoring": "true"}'
, null
);
-- End of .SPX

-- .FCHI
INSERT INTO MARKET_DATA_SOURCE_MAPS (
  ID
, MARKET_ID
, MARKET_DATA_SOURCE_ID
, HOUR_LEVEL_FORMULA_ID
, HOUR_LEVEL_FORMULA_PARAMS
, HOUR_CLOSING_FORMULA_ID
, HOUR_CLOSING_FORMULA_PARAMS
, DAY_CLOSING_FORMULA_ID
, DAY_CLOSING_FORMULA_PARAMS
, LONG_TERM_FORMULA_ID
, LONG_TERM_FORMULA_PARAMS
, REAL_LEVEL_FORMULA_ID
, REAL_LEVEL_FORMULA_PARAMS
, MARKET_PARAMS
, MARKET_DISABLE_CONDITIONS 
) VALUES (
SEQ_MARKET_DATA_SOURCE_MAPS.NEXTVAL
, 10
, 1
, 6
, '{"firstSubscriptionName": ".FCHI", "firstSubscriptionField1": "TRDPRC_1", "secondSubscriptionField1": "ASK", "secondSubscriptionField2": "BID", "secondSubscriptionField3": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "traderParamName": "cac", "subscriptionConfig": {".FCHI":{"identifier":".FCHI"}, "FCEc1": {"identifier": "FCEc1", "switchToFuture":"true", "switchToFuturePrefixLength":"3"}}}'
, 3
, '{"field1Name": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {".FCHI":{"identifier":".FCHI"}}}'
, 3
, '{"field1Name": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {".FCHI":{"identifier":".FCHI"}}}'
, 9
, '{"interestSubscription": "EUR1MD=", "interestAskField": "ASK", "interestBidField": "BID", "daysConstant": "360", "subscriptionConfig": {".FCHI":{"identifier":".FCHI"}, "EUR1MD=":{"identifier":"EUR1MD="}}}'
, 3
, '{"field1Name": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {".FCHI":{"identifier":".FCHI"}}}'
, '{"dontCloseAfterXSecNoUpdate": "60", "timeZone": "GMT", "shouldBeClosedByMonitoring": "true"}'
, null
);
-- End of .FCHI
-- End of binary markets

-- Option+ markets
-- GC.Option+
INSERT INTO MARKET_DATA_SOURCE_MAPS (
  ID
, MARKET_ID
, MARKET_DATA_SOURCE_ID
, HOUR_LEVEL_FORMULA_ID
, HOUR_LEVEL_FORMULA_PARAMS
, HOUR_CLOSING_FORMULA_ID
, HOUR_CLOSING_FORMULA_PARAMS
, DAY_CLOSING_FORMULA_ID
, DAY_CLOSING_FORMULA_PARAMS
, LONG_TERM_FORMULA_ID
, LONG_TERM_FORMULA_PARAMS
, REAL_LEVEL_FORMULA_ID
, REAL_LEVEL_FORMULA_PARAMS
, MARKET_PARAMS
, MARKET_DISABLE_CONDITIONS 
) VALUES (
SEQ_MARKET_DATA_SOURCE_MAPS.NEXTVAL
, 606
, 1
, 1
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "TRDPRC_1", "lastTradeTimeField": "TIMACT", "subscriptionConfig": {"XAU=ALL":{"identifier":"XAU=ALL","switchToFuture":"false","switchToFuturePrefixLength":"7"}}}'
, 1
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "TRDPRC_1", "lastTradeTimeField": "TIMACT", "subscriptionConfig": {"XAU=ALL":{"identifier":"XAU=ALL","switchToFuture":"false","switchToFuturePrefixLength":"7"}}, "ignoreTradePrice": "true"}'
, 1
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "TRDPRC_1", "lastTradeTimeField": "TIMACT", "subscriptionConfig": {"XAU=ALL":{"identifier":"XAU=ALL","switchToFuture":"false","switchToFuturePrefixLength":"7"}}, "ignoreTradePrice": "true"}'
, null
, null
, 1
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "TRDPRC_1", "lastTradeTimeField": "TIMACT", "subscriptionConfig": {"XAU=ALL":{"identifier":"XAU=ALL","switchToFuture":"false","switchToFuturePrefixLength":"7"}}}'
, '{"dontCloseAfterXSecNoUpdate": "240", "timeZone": "GMT", "shouldBeClosedByMonitoring": "true"}'
, null
);
-- End of GC.Option+

-- NQ option+ like indices
INSERT INTO MARKET_DATA_SOURCE_MAPS (
  ID
, MARKET_ID
, MARKET_DATA_SOURCE_ID
, HOUR_LEVEL_FORMULA_ID
, HOUR_LEVEL_FORMULA_PARAMS
, HOUR_CLOSING_FORMULA_ID
, HOUR_CLOSING_FORMULA_PARAMS
, DAY_CLOSING_FORMULA_ID
, DAY_CLOSING_FORMULA_PARAMS
, LONG_TERM_FORMULA_ID
, LONG_TERM_FORMULA_PARAMS
, REAL_LEVEL_FORMULA_ID
, REAL_LEVEL_FORMULA_PARAMS
, MARKET_PARAMS
, MARKET_DISABLE_CONDITIONS 
) VALUES (
SEQ_MARKET_DATA_SOURCE_MAPS.NEXTVAL
, 589
, 1
, 2
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {"FCEc1":{"identifier":"FCEc1","switchToFuture":"true","switchToFuturePrefixLength":"3"}}}'
, 2
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {"FCEc1":{"identifier":"FCEc1","switchToFuture":"true","switchToFuturePrefixLength":"3"}}}'
, 2
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {"FCEc1":{"identifier":"FCEc1","switchToFuture":"true","switchToFuturePrefixLength":"3"}}}'
, null
, null
, 3
, '{"field1Name": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {"FCEc1":{"identifier":"FCEc1","switchToFuture":"true","switchToFuturePrefixLength":"3"}}}'
, '{"dontCloseAfterXSecNoUpdate": "60", "timeZone": "GMT", "shouldBeClosedByMonitoring": "true"}'
, q'{{"com.anyoption.service.level.condition.AskBidSpreadMarketDisableCondition": {'subscriptionName': 'FCEc1', 'switchToFuture':'true', 'switchToFuturePrefixLength':'3', 'askField': 'ASK', 'bidField': 'BID', 'spread': '1.7'}}}'
);

INSERT INTO MARKET_DATA_SOURCE_MAPS (
  ID
, MARKET_ID
, MARKET_DATA_SOURCE_ID
, HOUR_LEVEL_FORMULA_ID
, HOUR_LEVEL_FORMULA_PARAMS
, HOUR_CLOSING_FORMULA_ID
, HOUR_CLOSING_FORMULA_PARAMS
, DAY_CLOSING_FORMULA_ID
, DAY_CLOSING_FORMULA_PARAMS
, LONG_TERM_FORMULA_ID
, LONG_TERM_FORMULA_PARAMS
, REAL_LEVEL_FORMULA_ID
, REAL_LEVEL_FORMULA_PARAMS
, MARKET_PARAMS
, MARKET_DISABLE_CONDITIONS 
) VALUES (
SEQ_MARKET_DATA_SOURCE_MAPS.NEXTVAL
, 560
, 1
, 2
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {"ESc1":{"identifier":"ESc1","switchToFuture":"true","switchToFuturePrefixLength":"2"}}}'
, 2
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {"ESc1":{"identifier":"ESc1","switchToFuture":"true","switchToFuturePrefixLength":"2"}}}'
, 2
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {"ESc1":{"identifier":"ESc1","switchToFuture":"true","switchToFuturePrefixLength":"2"}}}'
, null
, null
, 3
, '{"field1Name": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {"ESc1":{"identifier":"ESc1","switchToFuture":"true","switchToFuturePrefixLength":"2"}}}'
, '{"dontCloseAfterXSecNoUpdate": "60", "timeZone": "GMT", "shouldBeClosedByMonitoring": "true"}'
, q'{{"com.anyoption.service.level.condition.AskBidSpreadMarketDisableCondition": {'subscriptionName': 'ESc1', 'switchToFuture':'true', 'switchToFuturePrefixLength':'2', 'askField': 'ASK', 'bidField': 'BID', 'spread': '1.7'}}}'
);

INSERT INTO MARKET_DATA_SOURCE_MAPS (
  ID
, MARKET_ID
, MARKET_DATA_SOURCE_ID
, HOUR_LEVEL_FORMULA_ID
, HOUR_LEVEL_FORMULA_PARAMS
, HOUR_CLOSING_FORMULA_ID
, HOUR_CLOSING_FORMULA_PARAMS
, DAY_CLOSING_FORMULA_ID
, DAY_CLOSING_FORMULA_PARAMS
, LONG_TERM_FORMULA_ID
, LONG_TERM_FORMULA_PARAMS
, REAL_LEVEL_FORMULA_ID
, REAL_LEVEL_FORMULA_PARAMS
, MARKET_PARAMS
, MARKET_DISABLE_CONDITIONS 
) VALUES (
SEQ_MARKET_DATA_SOURCE_MAPS.NEXTVAL
, 559
, 1
, 2
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {"FDXc1":{"identifier":"FDXc1","switchToFuture":"true","switchToFuturePrefixLength":"3"}}}'
, 2
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {"FDXc1":{"identifier":"FDXc1","switchToFuture":"true","switchToFuturePrefixLength":"3"}}}'
, 2
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {"FDXc1":{"identifier":"FDXc1","switchToFuture":"true","switchToFuturePrefixLength":"3"}}}'
, null
, null
, 3
, '{"field1Name": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {"FDXc1":{"identifier":"FDXc1","switchToFuture":"true","switchToFuturePrefixLength":"3"}}}'
, '{"dontCloseAfterXSecNoUpdate": "60", "timeZone": "GMT", "shouldBeClosedByMonitoring": "true"}'
, q'{{"com.anyoption.service.level.condition.AskBidSpreadMarketDisableCondition": {'subscriptionName': 'FDXc1', 'switchToFuture':'true', 'switchToFuturePrefixLength':'3', 'askField': 'ASK', 'bidField': 'BID', 'spread': '3.4'}}}'
);
-- End of NQ option+ like indices

-- CL option+ like stocks
INSERT INTO MARKET_DATA_SOURCE_MAPS (
  ID
, MARKET_ID
, MARKET_DATA_SOURCE_ID
, HOUR_LEVEL_FORMULA_ID
, HOUR_LEVEL_FORMULA_PARAMS
, HOUR_CLOSING_FORMULA_ID
, HOUR_CLOSING_FORMULA_PARAMS
, DAY_CLOSING_FORMULA_ID
, DAY_CLOSING_FORMULA_PARAMS
, LONG_TERM_FORMULA_ID
, LONG_TERM_FORMULA_PARAMS
, REAL_LEVEL_FORMULA_ID
, REAL_LEVEL_FORMULA_PARAMS
, MARKET_PARAMS
, MARKET_DISABLE_CONDITIONS 
) VALUES (
SEQ_MARKET_DATA_SOURCE_MAPS.NEXTVAL
, 653
, 1
, 1
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "TRDPRC_1", "lastTradeTimeField": "TIMACT", "subscriptionConfig": {"XAG=":{"identifier":"XAG=","switchToFuture":"false","switchToFuturePrefixLength":"2"}}}'
, 1
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "TRDPRC_1", "lastTradeTimeField": "TIMACT", "subscriptionConfig": {"XAG=":{"identifier":"XAG=","switchToFuture":"false","switchToFuturePrefixLength":"2"}}, "ignoreTradePrice": "true"}'
, 1
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "TRDPRC_1", "lastTradeTimeField": "TIMACT", "subscriptionConfig": {"XAG=":{"identifier":"XAG=","switchToFuture":"false","switchToFuturePrefixLength":"2"}}, "ignoreTradePrice": "true"}'
, null
, null
, 1
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "TRDPRC_1", "lastTradeTimeField": "TIMACT", "subscriptionConfig": {"XAG=":{"identifier":"XAG=","switchToFuture":"false","switchToFuturePrefixLength":"2"}}}'
, '{"dontCloseAfterXSecNoUpdate": "120", "timeZone": "GMT", "shouldBeClosedByMonitoring": "true"}'
, null
);

INSERT INTO MARKET_DATA_SOURCE_MAPS (
  ID
, MARKET_ID
, MARKET_DATA_SOURCE_ID
, HOUR_LEVEL_FORMULA_ID
, HOUR_LEVEL_FORMULA_PARAMS
, HOUR_CLOSING_FORMULA_ID
, HOUR_CLOSING_FORMULA_PARAMS
, DAY_CLOSING_FORMULA_ID
, DAY_CLOSING_FORMULA_PARAMS
, LONG_TERM_FORMULA_ID
, LONG_TERM_FORMULA_PARAMS
, REAL_LEVEL_FORMULA_ID
, REAL_LEVEL_FORMULA_PARAMS
, MARKET_PARAMS
, MARKET_DISABLE_CONDITIONS 
) VALUES (
SEQ_MARKET_DATA_SOURCE_MAPS.NEXTVAL
, 558
, 1
, 1
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {"CLv1":{"identifier":"CLv1","switchToFuture":"true","switchToFuturePrefixLength":"2"}}}'
, 3
, '{"field1Name": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {"CLv1":{"identifier":"CLv1","switchToFuture":"true","switchToFuturePrefixLength":"2"}}}'
, 3
, '{"field1Name": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {"CLv1":{"identifier":"CLv1","switchToFuture":"true","switchToFuturePrefixLength":"2"}}}'
, null
, null
, 3
, '{"field1Name": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {"CLv1":{"identifier":"CLv1","switchToFuture":"true","switchToFuturePrefixLength":"2"}}}'
, '{"dontCloseAfterXSecNoUpdate": "120", "timeZone": "GMT", "shouldBeClosedByMonitoring": "true"}'
, null
);
-- End of CL option+ like stocks

-- EUR= option+ like currencies
INSERT INTO MARKET_DATA_SOURCE_MAPS (
  ID
, MARKET_ID
, MARKET_DATA_SOURCE_ID
, HOUR_LEVEL_FORMULA_ID
, HOUR_LEVEL_FORMULA_PARAMS
, HOUR_CLOSING_FORMULA_ID
, HOUR_CLOSING_FORMULA_PARAMS
, DAY_CLOSING_FORMULA_ID
, DAY_CLOSING_FORMULA_PARAMS
, LONG_TERM_FORMULA_ID
, LONG_TERM_FORMULA_PARAMS
, REAL_LEVEL_FORMULA_ID
, REAL_LEVEL_FORMULA_PARAMS
, MARKET_PARAMS
, MARKET_DISABLE_CONDITIONS 
) VALUES (
SEQ_MARKET_DATA_SOURCE_MAPS.NEXTVAL
, 552
, 1
, 7
, '{"last5UpdatesFormulaConfig": {"field1Name": "ASK", "field2Name": "BID", "subscriptionConfig": {"EUR=": {"identifier": "EUR="}}}, "avgOf2FormulaConfig": {"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "PRIMACT_1", "lastTradeTimeField": "TIMACT", "subscriptionConfig": {"EUR=FXAL":{"identifier":"EUR=FXAL"}}}, "fXALParameter": "0"}'
, 1
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "PRIMACT_1", "lastTradeTimeField": "TIMACT", "subscriptionConfig": {"EUR=":{"identifier":"EUR="}}, "ignoreTradePrice": "true"}'
, 1
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "PRIMACT_1", "lastTradeTimeField": "TIMACT", "subscriptionConfig": {"EUR=":{"identifier":"EUR="}}, "ignoreTradePrice": "true"}'
, null
, null
, 1
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "PRIMACT_1", "lastTradeTimeField": "TIMACT", "subscriptionConfig": {"EUR=":{"identifier":"EUR="}}}'
, '{"dontCloseAfterXSecNoUpdate": "60", "timeZone": "GMT", "shouldBeClosedByMonitoring": "false"}'
, null
);

INSERT INTO MARKET_DATA_SOURCE_MAPS (
  ID
, MARKET_ID
, MARKET_DATA_SOURCE_ID
, HOUR_LEVEL_FORMULA_ID
, HOUR_LEVEL_FORMULA_PARAMS
, HOUR_CLOSING_FORMULA_ID
, HOUR_CLOSING_FORMULA_PARAMS
, DAY_CLOSING_FORMULA_ID
, DAY_CLOSING_FORMULA_PARAMS
, LONG_TERM_FORMULA_ID
, LONG_TERM_FORMULA_PARAMS
, REAL_LEVEL_FORMULA_ID
, REAL_LEVEL_FORMULA_PARAMS
, MARKET_PARAMS
, MARKET_DISABLE_CONDITIONS 
) VALUES (
SEQ_MARKET_DATA_SOURCE_MAPS.NEXTVAL
, 561
, 1
, 7
, '{"last5UpdatesFormulaConfig": {"field1Name": "ASK", "field2Name": "BID", "subscriptionConfig": {"JPY=": {"identifier": "JPY="}}}, "avgOf2FormulaConfig": {"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "PRIMACT_1", "lastTradeTimeField": "TIMACT", "subscriptionConfig": {"JPY=FXAL":{"identifier":"JPY=FXAL"}}}, "fXALParameter": "0"}'
, 1
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "PRIMACT_1", "lastTradeTimeField": "TIMACT", "subscriptionConfig": {"JPY=":{"identifier":"JPY="}}, "ignoreTradePrice": "true"}'
, 1
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "PRIMACT_1", "lastTradeTimeField": "TIMACT", "subscriptionConfig": {"JPY=":{"identifier":"JPY="}}, "ignoreTradePrice": "true"}'
, null
, null
, 1
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "PRIMACT_1", "lastTradeTimeField": "TIMACT", "subscriptionConfig": {"JPY=":{"identifier":"JPY="}}}'
, '{"dontCloseAfterXSecNoUpdate": "60", "timeZone": "GMT", "shouldBeClosedByMonitoring": "false"}'
, null
);

INSERT INTO MARKET_DATA_SOURCE_MAPS (
  ID
, MARKET_ID
, MARKET_DATA_SOURCE_ID
, HOUR_LEVEL_FORMULA_ID
, HOUR_LEVEL_FORMULA_PARAMS
, HOUR_CLOSING_FORMULA_ID
, HOUR_CLOSING_FORMULA_PARAMS
, DAY_CLOSING_FORMULA_ID
, DAY_CLOSING_FORMULA_PARAMS
, LONG_TERM_FORMULA_ID
, LONG_TERM_FORMULA_PARAMS
, REAL_LEVEL_FORMULA_ID
, REAL_LEVEL_FORMULA_PARAMS
, MARKET_PARAMS
, MARKET_DISABLE_CONDITIONS 
) VALUES (
SEQ_MARKET_DATA_SOURCE_MAPS.NEXTVAL
, 581
, 1
, 7
, '{"last5UpdatesFormulaConfig": {"field1Name": "ASK", "field2Name": "BID", "subscriptionConfig": {"AUD=": {"identifier": "AUD="}}}, "avgOf2FormulaConfig": {"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "PRIMACT_1", "lastTradeTimeField": "TIMACT", "subscriptionConfig": {"AUD=FXAL":{"identifier":"AUD=FXAL"}}}, "fXALParameter": "0"}'
, 1
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "PRIMACT_1", "lastTradeTimeField": "TIMACT", "subscriptionConfig": {"AUD=":{"identifier":"AUD="}}, "ignoreTradePrice": "true"}'
, 1
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "PRIMACT_1", "lastTradeTimeField": "TIMACT", "subscriptionConfig": {"AUD=":{"identifier":"AUD="}}, "ignoreTradePrice": "true"}'
, null
, null
, 1
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "PRIMACT_1", "lastTradeTimeField": "TIMACT", "subscriptionConfig": {"AUD=":{"identifier":"AUD="}}}'
, '{"dontCloseAfterXSecNoUpdate": "60", "timeZone": "GMT", "shouldBeClosedByMonitoring": "false"}'
, null
);

INSERT INTO MARKET_DATA_SOURCE_MAPS (
  ID
, MARKET_ID
, MARKET_DATA_SOURCE_ID
, HOUR_LEVEL_FORMULA_ID
, HOUR_LEVEL_FORMULA_PARAMS
, HOUR_CLOSING_FORMULA_ID
, HOUR_CLOSING_FORMULA_PARAMS
, DAY_CLOSING_FORMULA_ID
, DAY_CLOSING_FORMULA_PARAMS
, LONG_TERM_FORMULA_ID
, LONG_TERM_FORMULA_PARAMS
, REAL_LEVEL_FORMULA_ID
, REAL_LEVEL_FORMULA_PARAMS
, MARKET_PARAMS
, MARKET_DISABLE_CONDITIONS 
) VALUES (
SEQ_MARKET_DATA_SOURCE_MAPS.NEXTVAL
, 605
, 1
, 7
, '{"last5UpdatesFormulaConfig": {"field1Name": "ASK", "field2Name": "BID", "subscriptionConfig": {"GBP=": {"identifier": "GBP="}}}, "avgOf2FormulaConfig": {"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "PRIMACT_1", "lastTradeTimeField": "TIMACT", "subscriptionConfig": {"GBP=FXAL":{"identifier":"GBP=FXAL"}}}, "fXALParameter": "0"}'
, 1
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "PRIMACT_1", "lastTradeTimeField": "TIMACT", "subscriptionConfig": {"GBP=":{"identifier":"GBP="}}, "ignoreTradePrice": "true"}'
, 1
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "PRIMACT_1", "lastTradeTimeField": "TIMACT", "subscriptionConfig": {"GBP=":{"identifier":"GBP="}}, "ignoreTradePrice": "true"}'
, null
, null
, 1
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "PRIMACT_1", "lastTradeTimeField": "TIMACT", "subscriptionConfig": {"GBP=":{"identifier":"GBP="}}}'
, '{"dontCloseAfterXSecNoUpdate": "60", "timeZone": "GMT", "shouldBeClosedByMonitoring": "false"}'
, null
);

INSERT INTO MARKET_DATA_SOURCE_MAPS (
  ID
, MARKET_ID
, MARKET_DATA_SOURCE_ID
, HOUR_LEVEL_FORMULA_ID
, HOUR_LEVEL_FORMULA_PARAMS
, HOUR_CLOSING_FORMULA_ID
, HOUR_CLOSING_FORMULA_PARAMS
, DAY_CLOSING_FORMULA_ID
, DAY_CLOSING_FORMULA_PARAMS
, LONG_TERM_FORMULA_ID
, LONG_TERM_FORMULA_PARAMS
, REAL_LEVEL_FORMULA_ID
, REAL_LEVEL_FORMULA_PARAMS
, MARKET_PARAMS
, MARKET_DISABLE_CONDITIONS 
) VALUES (
SEQ_MARKET_DATA_SOURCE_MAPS.NEXTVAL
, 683
, 1
, 7
, '{"last5UpdatesFormulaConfig": {"field1Name": "ASK", "field2Name": "BID", "subscriptionConfig": {"SEK=": {"identifier": "SEK="}}}, "avgOf2FormulaConfig": {"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "PRIMACT_1", "lastTradeTimeField": "TIMACT", "subscriptionConfig": {"SEK=FXAL":{"identifier":"SEK=FXAL"}}}, "fXALParameter": "0"}'
, 1
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "PRIMACT_1", "lastTradeTimeField": "TIMACT", "subscriptionConfig": {"SEK=":{"identifier":"SEK="}}, "ignoreTradePrice": "true"}'
, 1
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "PRIMACT_1", "lastTradeTimeField": "TIMACT", "subscriptionConfig": {"SEK=":{"identifier":"SEK="}}, "ignoreTradePrice": "true"}'
, null
, null
, 1
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "PRIMACT_1", "lastTradeTimeField": "TIMACT", "subscriptionConfig": {"SEK=":{"identifier":"SEK="}}}'
, '{"dontCloseAfterXSecNoUpdate": "60", "timeZone": "GMT", "shouldBeClosedByMonitoring": "false"}'
, null
);

INSERT INTO MARKET_DATA_SOURCE_MAPS (
  ID
, MARKET_ID
, MARKET_DATA_SOURCE_ID
, HOUR_LEVEL_FORMULA_ID
, HOUR_LEVEL_FORMULA_PARAMS
, HOUR_CLOSING_FORMULA_ID
, HOUR_CLOSING_FORMULA_PARAMS
, DAY_CLOSING_FORMULA_ID
, DAY_CLOSING_FORMULA_PARAMS
, LONG_TERM_FORMULA_ID
, LONG_TERM_FORMULA_PARAMS
, REAL_LEVEL_FORMULA_ID
, REAL_LEVEL_FORMULA_PARAMS
, MARKET_PARAMS
, MARKET_DISABLE_CONDITIONS 
) VALUES (
SEQ_MARKET_DATA_SOURCE_MAPS.NEXTVAL
, 684
, 1
, 7
, '{"last5UpdatesFormulaConfig": {"field1Name": "ASK", "field2Name": "BID", "subscriptionConfig": {"NOK=": {"identifier": "NOK="}}}, "avgOf2FormulaConfig": {"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "PRIMACT_1", "lastTradeTimeField": "TIMACT", "subscriptionConfig": {"NOK=FXAL":{"identifier":"NOK=FXAL"}}}, "fXALParameter": "0"}'
, 1
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "PRIMACT_1", "lastTradeTimeField": "TIMACT", "subscriptionConfig": {"NOK=":{"identifier":"NOK="}}, "ignoreTradePrice": "true"}'
, 1
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "PRIMACT_1", "lastTradeTimeField": "TIMACT", "subscriptionConfig": {"NOK=":{"identifier":"NOK="}}, "ignoreTradePrice": "true"}'
, null
, null
, 1
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "PRIMACT_1", "lastTradeTimeField": "TIMACT", "subscriptionConfig": {"NOK=":{"identifier":"NOK="}}}'
, '{"dontCloseAfterXSecNoUpdate": "60", "timeZone": "GMT", "shouldBeClosedByMonitoring": "false"}'
, null
);
-- End of EUR= option+ like currencies

-- C option+ like stocks
INSERT INTO MARKET_DATA_SOURCE_MAPS (
  ID
, MARKET_ID
, MARKET_DATA_SOURCE_ID
, HOUR_LEVEL_FORMULA_ID
, HOUR_LEVEL_FORMULA_PARAMS
, HOUR_CLOSING_FORMULA_ID
, HOUR_CLOSING_FORMULA_PARAMS
, DAY_CLOSING_FORMULA_ID
, DAY_CLOSING_FORMULA_PARAMS
, LONG_TERM_FORMULA_ID
, LONG_TERM_FORMULA_PARAMS
, REAL_LEVEL_FORMULA_ID
, REAL_LEVEL_FORMULA_PARAMS
, MARKET_PARAMS
, MARKET_DISABLE_CONDITIONS 
) VALUES (
SEQ_MARKET_DATA_SOURCE_MAPS.NEXTVAL
, 597
, 1
, 5
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {"GOOG.O":{"identifier":"GOOG.O"}}}'
, 3
, '{"field1Name": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {"GOOG.O":{"identifier":"GOOG.O"}}}'
, 3
, '{"field1Name": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {"GOOG.O":{"identifier":"GOOG.O"}}}'
, null
, null
, 2
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {"GOOG.O":{"identifier":"GOOG.O"}}}'
, '{"dontCloseAfterXSecNoUpdate": "60", "timeZone": "GMT", "shouldBeClosedByMonitoring": "true"}'
, q'{{"com.anyoption.service.level.condition.AskBidDistanceMarketDisableCondition": {'subscriptionName': 'GOOG.O', 'askField': 'ASK', 'bidField': 'BID', 'lastField': 'TRDPRC_1', 'distance': '0.02'}, "com.anyoption.service.level.condition.AskBidInverseMarketDisableCondition": {'subscriptionName': 'GOOG.O', 'askField': 'ASK', 'bidField': 'BID'}}}'
);

INSERT INTO MARKET_DATA_SOURCE_MAPS (
  ID
, MARKET_ID
, MARKET_DATA_SOURCE_ID
, HOUR_LEVEL_FORMULA_ID
, HOUR_LEVEL_FORMULA_PARAMS
, HOUR_CLOSING_FORMULA_ID
, HOUR_CLOSING_FORMULA_PARAMS
, DAY_CLOSING_FORMULA_ID
, DAY_CLOSING_FORMULA_PARAMS
, LONG_TERM_FORMULA_ID
, LONG_TERM_FORMULA_PARAMS
, REAL_LEVEL_FORMULA_ID
, REAL_LEVEL_FORMULA_PARAMS
, MARKET_PARAMS
, MARKET_DISABLE_CONDITIONS 
) VALUES (
SEQ_MARKET_DATA_SOURCE_MAPS.NEXTVAL
, 623
, 1
, 5
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {"FB.O":{"identifier":"FB.O"}}}'
, 3
, '{"field1Name": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {"FB.O":{"identifier":"FB.O"}}}'
, 3
, '{"field1Name": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {"FB.O":{"identifier":"FB.O"}}}'
, null
, null
, 2
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {"FB.O":{"identifier":"FB.O"}}}'
, '{"dontCloseAfterXSecNoUpdate": "60", "timeZone": "GMT", "shouldBeClosedByMonitoring": "true"}'
, q'{{"com.anyoption.service.level.condition.AskBidDistanceMarketDisableCondition": {'subscriptionName': 'FB.O', 'askField': 'ASK', 'bidField': 'BID', 'lastField': 'TRDPRC_1', 'distance': '0.02'}, "com.anyoption.service.level.condition.AskBidInverseMarketDisableCondition": {'subscriptionName': 'FB.O', 'askField': 'ASK', 'bidField': 'BID'}}}'
);

INSERT INTO MARKET_DATA_SOURCE_MAPS (
  ID
, MARKET_ID
, MARKET_DATA_SOURCE_ID
, HOUR_LEVEL_FORMULA_ID
, HOUR_LEVEL_FORMULA_PARAMS
, HOUR_CLOSING_FORMULA_ID
, HOUR_CLOSING_FORMULA_PARAMS
, DAY_CLOSING_FORMULA_ID
, DAY_CLOSING_FORMULA_PARAMS
, LONG_TERM_FORMULA_ID
, LONG_TERM_FORMULA_PARAMS
, REAL_LEVEL_FORMULA_ID
, REAL_LEVEL_FORMULA_PARAMS
, MARKET_PARAMS
, MARKET_DISABLE_CONDITIONS 
) VALUES (
SEQ_MARKET_DATA_SOURCE_MAPS.NEXTVAL
, 630
, 1
, 5
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {"AAPL.O":{"identifier":"AAPL.O"}}}'
, 3
, '{"field1Name": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {"AAPL.O":{"identifier":"AAPL.O"}}}'
, 3
, '{"field1Name": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {"AAPL.O":{"identifier":"AAPL.O"}}}'
, null
, null
, 2
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {"AAPL.O":{"identifier":"AAPL.O"}}}'
, '{"dontCloseAfterXSecNoUpdate": "60", "timeZone": "GMT", "shouldBeClosedByMonitoring": "true"}'
, q'{{"com.anyoption.service.level.condition.AskBidDistanceMarketDisableCondition": {'subscriptionName': 'AAPL.O', 'askField': 'ASK', 'bidField': 'BID', 'lastField': 'TRDPRC_1', 'distance': '0.02'}, "com.anyoption.service.level.condition.AskBidInverseMarketDisableCondition": {'subscriptionName': 'AAPL.O', 'askField': 'ASK', 'bidField': 'BID'}}}'
);

INSERT INTO MARKET_DATA_SOURCE_MAPS (
  ID
, MARKET_ID
, MARKET_DATA_SOURCE_ID
, HOUR_LEVEL_FORMULA_ID
, HOUR_LEVEL_FORMULA_PARAMS
, HOUR_CLOSING_FORMULA_ID
, HOUR_CLOSING_FORMULA_PARAMS
, DAY_CLOSING_FORMULA_ID
, DAY_CLOSING_FORMULA_PARAMS
, LONG_TERM_FORMULA_ID
, LONG_TERM_FORMULA_PARAMS
, REAL_LEVEL_FORMULA_ID
, REAL_LEVEL_FORMULA_PARAMS
, MARKET_PARAMS
, MARKET_DISABLE_CONDITIONS 
) VALUES (
SEQ_MARKET_DATA_SOURCE_MAPS.NEXTVAL
, 655
, 1
, 5
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {"DAIGn.DE":{"identifier":"DAIGn.DE"}}}'
, 3
, '{"field1Name": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {"DAIGn.DE":{"identifier":"DAIGn.DE"}}}'
, 3
, '{"field1Name": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {"DAIGn.DE":{"identifier":"DAIGn.DE"}}}'
, null
, null
, 2
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {"DAIGn.DE":{"identifier":"DAIGn.DE"}}}'
, '{"dontCloseAfterXSecNoUpdate": "60", "timeZone": "GMT", "shouldBeClosedByMonitoring": "true"}'
, q'{{"com.anyoption.service.level.condition.AskBidDistanceMarketDisableCondition": {'subscriptionName': 'DAIGn.DE', 'askField': 'ASK', 'bidField': 'BID', 'lastField': 'TRDPRC_1', 'distance': '0.02'}, "com.anyoption.service.level.condition.AskBidInverseMarketDisableCondition": {'subscriptionName': 'DAIGn.DE', 'askField': 'ASK', 'bidField': 'BID'}}}'
);

INSERT INTO MARKET_DATA_SOURCE_MAPS (
  ID
, MARKET_ID
, MARKET_DATA_SOURCE_ID
, HOUR_LEVEL_FORMULA_ID
, HOUR_LEVEL_FORMULA_PARAMS
, HOUR_CLOSING_FORMULA_ID
, HOUR_CLOSING_FORMULA_PARAMS
, DAY_CLOSING_FORMULA_ID
, DAY_CLOSING_FORMULA_PARAMS
, LONG_TERM_FORMULA_ID
, LONG_TERM_FORMULA_PARAMS
, REAL_LEVEL_FORMULA_ID
, REAL_LEVEL_FORMULA_PARAMS
, MARKET_PARAMS
, MARKET_DISABLE_CONDITIONS 
) VALUES (
SEQ_MARKET_DATA_SOURCE_MAPS.NEXTVAL
, 656
, 1
, 5
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {"BP.L":{"identifier":"BP.L"}}}'
, 3
, '{"field1Name": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {"BP.L":{"identifier":"BP.L"}}}'
, 3
, '{"field1Name": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {"BP.L":{"identifier":"BP.L"}}}'
, null
, null
, 2
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {"BP.L":{"identifier":"BP.L"}}}'
, '{"dontCloseAfterXSecNoUpdate": "60", "timeZone": "GMT", "shouldBeClosedByMonitoring": "true"}'
, q'{{"com.anyoption.service.level.condition.AskBidDistanceMarketDisableCondition": {'subscriptionName': 'BP.L', 'askField': 'ASK', 'bidField': 'BID', 'lastField': 'TRDPRC_1', 'distance': '0.02'}, "com.anyoption.service.level.condition.AskBidInverseMarketDisableCondition": {'subscriptionName': 'BP.L', 'askField': 'ASK', 'bidField': 'BID'}}}'
);
-- End of C option+ like stocks
-- .IBEX.Option+
INSERT INTO MARKET_DATA_SOURCE_MAPS (
  ID
, MARKET_ID
, MARKET_DATA_SOURCE_ID
, HOUR_LEVEL_FORMULA_ID
, HOUR_LEVEL_FORMULA_PARAMS
, HOUR_CLOSING_FORMULA_ID
, HOUR_CLOSING_FORMULA_PARAMS
, DAY_CLOSING_FORMULA_ID
, DAY_CLOSING_FORMULA_PARAMS
, LONG_TERM_FORMULA_ID
, LONG_TERM_FORMULA_PARAMS
, REAL_LEVEL_FORMULA_ID
, REAL_LEVEL_FORMULA_PARAMS
, MARKET_PARAMS
, MARKET_DISABLE_CONDITIONS 
) VALUES (
SEQ_MARKET_DATA_SOURCE_MAPS.NEXTVAL
, 570
, 1
, 3
, '{"field1Name": "TRDPRC_1", "lastTradeTimeField": "TRDTIM_1", "subscriptionConfig": {".IBEX":{"identifier":".IBEX"}}}'
, 3
, '{"field1Name": "TRDPRC_1", "lastTradeTimeField": "TRDTIM_1", "subscriptionConfig": {".IBEX":{"identifier":".IBEX"}}}'
, 3
, '{"field1Name": "TRDPRC_1", "lastTradeTimeField": "TRDTIM_1", "subscriptionConfig": {".IBEX":{"identifier":".IBEX"}}}'
, null
, null
, 3
, '{"field1Name": "TRDPRC_1", "lastTradeTimeField": "TRDTIM_1", "subscriptionConfig": {".IBEX":{"identifier":".IBEX"}}}'
, '{"dontCloseAfterXSecNoUpdate": "60", "timeZone": "GMT", "shouldBeClosedByMonitoring": "true"}'
, null
);
-- End of
-- End of option+ markets

-- Bubbles markets
INSERT INTO MARKET_DATA_SOURCE_MAPS (
  ID
, MARKET_ID
, MARKET_DATA_SOURCE_ID
, HOUR_LEVEL_FORMULA_ID
, HOUR_LEVEL_FORMULA_PARAMS
, HOUR_CLOSING_FORMULA_ID
, HOUR_CLOSING_FORMULA_PARAMS
, DAY_CLOSING_FORMULA_ID
, DAY_CLOSING_FORMULA_PARAMS
, LONG_TERM_FORMULA_ID
, LONG_TERM_FORMULA_PARAMS
, REAL_LEVEL_FORMULA_ID
, REAL_LEVEL_FORMULA_PARAMS
, MARKET_PARAMS
, MARKET_DISABLE_CONDITIONS 
) VALUES (
SEQ_MARKET_DATA_SOURCE_MAPS.NEXTVAL
, 700
, 1
, 3
, '{"field1Name": "ASK", "lastTradeTimeField": "TIMACT", "subscriptionConfig": {"EUR=":{"identifier":"EUR="}}}'
, 3
, '{"field1Name": "ASK", "lastTradeTimeField": "TIMACT", "subscriptionConfig": {"EUR=":{"identifier":"EUR="}}}'
, 3
, '{"field1Name": "ASK", "lastTradeTimeField": "TIMACT", "subscriptionConfig": {"EUR=":{"identifier":"EUR="}}}'
, null
, null
, 3
, '{"field1Name": "ASK", "lastTradeTimeField": "TIMACT", "subscriptionConfig": {"EUR=":{"identifier":"EUR="}}}'
, '{"dontCloseAfterXSecNoUpdate": "60", "timeZone": "GMT", "shouldBeClosedByMonitoring": "false"}'
, null
);

INSERT INTO MARKET_DATA_SOURCE_MAPS (
  ID
, MARKET_ID
, MARKET_DATA_SOURCE_ID
, HOUR_LEVEL_FORMULA_ID
, HOUR_LEVEL_FORMULA_PARAMS
, HOUR_CLOSING_FORMULA_ID
, HOUR_CLOSING_FORMULA_PARAMS
, DAY_CLOSING_FORMULA_ID
, DAY_CLOSING_FORMULA_PARAMS
, LONG_TERM_FORMULA_ID
, LONG_TERM_FORMULA_PARAMS
, REAL_LEVEL_FORMULA_ID
, REAL_LEVEL_FORMULA_PARAMS
, MARKET_PARAMS
, MARKET_DISABLE_CONDITIONS 
) VALUES (
SEQ_MARKET_DATA_SOURCE_MAPS.NEXTVAL
, 706
, 1
, 3
, '{"field1Name": "ASK", "lastTradeTimeField": "TIMACT", "subscriptionConfig": {"GBP=":{"identifier":"GBP="}}}'
, 3
, '{"field1Name": "ASK", "lastTradeTimeField": "TIMACT", "subscriptionConfig": {"GBP=":{"identifier":"GBP="}}}'
, 3
, '{"field1Name": "ASK", "lastTradeTimeField": "TIMACT", "subscriptionConfig": {"GBP=":{"identifier":"GBP="}}}'
, null
, null
, 3
, '{"field1Name": "ASK", "lastTradeTimeField": "TIMACT", "subscriptionConfig": {"GBP=":{"identifier":"GBP="}}}'
, '{"dontCloseAfterXSecNoUpdate": "60", "timeZone": "GMT", "shouldBeClosedByMonitoring": "false"}'
, null
);

INSERT INTO MARKET_DATA_SOURCE_MAPS (
  ID
, MARKET_ID
, MARKET_DATA_SOURCE_ID
, HOUR_LEVEL_FORMULA_ID
, HOUR_LEVEL_FORMULA_PARAMS
, HOUR_CLOSING_FORMULA_ID
, HOUR_CLOSING_FORMULA_PARAMS
, DAY_CLOSING_FORMULA_ID
, DAY_CLOSING_FORMULA_PARAMS
, LONG_TERM_FORMULA_ID
, LONG_TERM_FORMULA_PARAMS
, REAL_LEVEL_FORMULA_ID
, REAL_LEVEL_FORMULA_PARAMS
, MARKET_PARAMS
, MARKET_DISABLE_CONDITIONS 
) VALUES (
SEQ_MARKET_DATA_SOURCE_MAPS.NEXTVAL
, 707
, 1
, 3
, '{"field1Name": "ASK", "lastTradeTimeField": "TIMACT", "subscriptionConfig": {"JPY=":{"identifier":"JPY="}}}'
, 3
, '{"field1Name": "ASK", "lastTradeTimeField": "TIMACT", "subscriptionConfig": {"JPY=":{"identifier":"JPY="}}}'
, 3
, '{"field1Name": "ASK", "lastTradeTimeField": "TIMACT", "subscriptionConfig": {"JPY=":{"identifier":"JPY="}}}'
, null
, null
, 3
, '{"field1Name": "ASK", "lastTradeTimeField": "TIMACT", "subscriptionConfig": {"JPY=":{"identifier":"JPY="}}}'
, '{"dontCloseAfterXSecNoUpdate": "60", "timeZone": "GMT", "shouldBeClosedByMonitoring": "false"}'
, null
);

INSERT INTO MARKET_DATA_SOURCE_MAPS (
  ID
, MARKET_ID
, MARKET_DATA_SOURCE_ID
, HOUR_LEVEL_FORMULA_ID
, HOUR_LEVEL_FORMULA_PARAMS
, HOUR_CLOSING_FORMULA_ID
, HOUR_CLOSING_FORMULA_PARAMS
, DAY_CLOSING_FORMULA_ID
, DAY_CLOSING_FORMULA_PARAMS
, LONG_TERM_FORMULA_ID
, LONG_TERM_FORMULA_PARAMS
, REAL_LEVEL_FORMULA_ID
, REAL_LEVEL_FORMULA_PARAMS
, MARKET_PARAMS
, MARKET_DISABLE_CONDITIONS 
) VALUES (
SEQ_MARKET_DATA_SOURCE_MAPS.NEXTVAL
, 720
, 1
, 3
, '{"field1Name": "ASK", "lastTradeTimeField": "TIMACT", "subscriptionConfig": {"RUB=":{"identifier":"RUB="}}}'
, 3
, '{"field1Name": "ASK", "lastTradeTimeField": "TIMACT", "subscriptionConfig": {"RUB=":{"identifier":"RUB="}}}'
, 3
, '{"field1Name": "ASK", "lastTradeTimeField": "TIMACT", "subscriptionConfig": {"RUB=":{"identifier":"RUB="}}}'
, null
, null
, 3
, '{"field1Name": "ASK", "lastTradeTimeField": "TIMACT", "subscriptionConfig": {"RUB=":{"identifier":"RUB="}}}'
, '{"dontCloseAfterXSecNoUpdate": "60", "timeZone": "GMT", "shouldBeClosedByMonitoring": "false"}'
, null
);

INSERT INTO MARKET_DATA_SOURCE_MAPS (
  ID
, MARKET_ID
, MARKET_DATA_SOURCE_ID
, HOUR_LEVEL_FORMULA_ID
, HOUR_LEVEL_FORMULA_PARAMS
, HOUR_CLOSING_FORMULA_ID
, HOUR_CLOSING_FORMULA_PARAMS
, DAY_CLOSING_FORMULA_ID
, DAY_CLOSING_FORMULA_PARAMS
, LONG_TERM_FORMULA_ID
, LONG_TERM_FORMULA_PARAMS
, REAL_LEVEL_FORMULA_ID
, REAL_LEVEL_FORMULA_PARAMS
, MARKET_PARAMS
, MARKET_DISABLE_CONDITIONS 
) VALUES (
SEQ_MARKET_DATA_SOURCE_MAPS.NEXTVAL
, 721
, 1
, 3
, '{"field1Name": "ASK", "lastTradeTimeField": "TIMACT", "subscriptionConfig": {"TRY=":{"identifier":"TRY="}}}'
, 3
, '{"field1Name": "ASK", "lastTradeTimeField": "TIMACT", "subscriptionConfig": {"TRY=":{"identifier":"TRY="}}}'
, 3
, '{"field1Name": "ASK", "lastTradeTimeField": "TIMACT", "subscriptionConfig": {"TRY=":{"identifier":"TRY="}}}'
, null
, null
, 3
, '{"field1Name": "ASK", "lastTradeTimeField": "TIMACT", "subscriptionConfig": {"TRY=":{"identifier":"TRY="}}}'
, '{"dontCloseAfterXSecNoUpdate": "60", "timeZone": "GMT", "shouldBeClosedByMonitoring": "false"}'
, null
);

INSERT INTO MARKET_DATA_SOURCE_MAPS (
  ID
, MARKET_ID
, MARKET_DATA_SOURCE_ID
, HOUR_LEVEL_FORMULA_ID
, HOUR_LEVEL_FORMULA_PARAMS
, HOUR_CLOSING_FORMULA_ID
, HOUR_CLOSING_FORMULA_PARAMS
, DAY_CLOSING_FORMULA_ID
, DAY_CLOSING_FORMULA_PARAMS
, LONG_TERM_FORMULA_ID
, LONG_TERM_FORMULA_PARAMS
, REAL_LEVEL_FORMULA_ID
, REAL_LEVEL_FORMULA_PARAMS
, MARKET_PARAMS
, MARKET_DISABLE_CONDITIONS 
) VALUES (
SEQ_MARKET_DATA_SOURCE_MAPS.NEXTVAL
, 722
, 1
, 3
, '{"field1Name": "ASK", "lastTradeTimeField": "TIMACT", "subscriptionConfig": {"AUD=":{"identifier":"AUD="}}}'
, 3
, '{"field1Name": "ASK", "lastTradeTimeField": "TIMACT", "subscriptionConfig": {"AUD=":{"identifier":"AUD="}}}'
, 3
, '{"field1Name": "ASK", "lastTradeTimeField": "TIMACT", "subscriptionConfig": {"AUD=":{"identifier":"AUD="}}}'
, null
, null
, 3
, '{"field1Name": "ASK", "lastTradeTimeField": "TIMACT", "subscriptionConfig": {"AUD=":{"identifier":"AUD="}}}'
, '{"dontCloseAfterXSecNoUpdate": "60", "timeZone": "GMT", "shouldBeClosedByMonitoring": "false"}'
, null
);

INSERT INTO MARKET_DATA_SOURCE_MAPS (
  ID
, MARKET_ID
, MARKET_DATA_SOURCE_ID
, HOUR_LEVEL_FORMULA_ID
, HOUR_LEVEL_FORMULA_PARAMS
, HOUR_CLOSING_FORMULA_ID
, HOUR_CLOSING_FORMULA_PARAMS
, DAY_CLOSING_FORMULA_ID
, DAY_CLOSING_FORMULA_PARAMS
, LONG_TERM_FORMULA_ID
, LONG_TERM_FORMULA_PARAMS
, REAL_LEVEL_FORMULA_ID
, REAL_LEVEL_FORMULA_PARAMS
, MARKET_PARAMS
, MARKET_DISABLE_CONDITIONS 
) VALUES (
SEQ_MARKET_DATA_SOURCE_MAPS.NEXTVAL
, 723
, 1
, 3
, '{"field1Name": "ASK", "lastTradeTimeField": "TIMACT", "subscriptionConfig": {"CHF=":{"identifier":"CHF="}}}'
, 3
, '{"field1Name": "ASK", "lastTradeTimeField": "TIMACT", "subscriptionConfig": {"CHF=":{"identifier":"CHF="}}}'
, 3
, '{"field1Name": "ASK", "lastTradeTimeField": "TIMACT", "subscriptionConfig": {"CHF=":{"identifier":"CHF="}}}'
, null
, null
, 3
, '{"field1Name": "ASK", "lastTradeTimeField": "TIMACT", "subscriptionConfig": {"CHF=":{"identifier":"CHF="}}}'
, '{"dontCloseAfterXSecNoUpdate": "60", "timeZone": "GMT", "shouldBeClosedByMonitoring": "false"}'
, null
);

INSERT INTO MARKET_DATA_SOURCE_MAPS (
  ID
, MARKET_ID
, MARKET_DATA_SOURCE_ID
, HOUR_LEVEL_FORMULA_ID
, HOUR_LEVEL_FORMULA_PARAMS
, HOUR_CLOSING_FORMULA_ID
, HOUR_CLOSING_FORMULA_PARAMS
, DAY_CLOSING_FORMULA_ID
, DAY_CLOSING_FORMULA_PARAMS
, LONG_TERM_FORMULA_ID
, LONG_TERM_FORMULA_PARAMS
, REAL_LEVEL_FORMULA_ID
, REAL_LEVEL_FORMULA_PARAMS
, MARKET_PARAMS
, MARKET_DISABLE_CONDITIONS 
) VALUES (
SEQ_MARKET_DATA_SOURCE_MAPS.NEXTVAL
, 724
, 1
, 3
, '{"field1Name": "ASK", "lastTradeTimeField": "TIMACT", "subscriptionConfig": {"ZAR=":{"identifier":"ZAR="}}}'
, 3
, '{"field1Name": "ASK", "lastTradeTimeField": "TIMACT", "subscriptionConfig": {"ZAR=":{"identifier":"ZAR="}}}'
, 3
, '{"field1Name": "ASK", "lastTradeTimeField": "TIMACT", "subscriptionConfig": {"ZAR=":{"identifier":"ZAR="}}}'
, null
, null
, 3
, '{"field1Name": "ASK", "lastTradeTimeField": "TIMACT", "subscriptionConfig": {"ZAR=":{"identifier":"ZAR="}}}'
, '{"dontCloseAfterXSecNoUpdate": "60", "timeZone": "GMT", "shouldBeClosedByMonitoring": "false"}'
, null
);
-- End of bubbles markets

-- Binary 0100 markets
INSERT INTO MARKET_DATA_SOURCE_MAPS (
  ID
, MARKET_ID
, MARKET_DATA_SOURCE_ID
, HOUR_LEVEL_FORMULA_ID
, HOUR_LEVEL_FORMULA_PARAMS
, HOUR_CLOSING_FORMULA_ID
, HOUR_CLOSING_FORMULA_PARAMS
, DAY_CLOSING_FORMULA_ID
, DAY_CLOSING_FORMULA_PARAMS
, LONG_TERM_FORMULA_ID
, LONG_TERM_FORMULA_PARAMS
, REAL_LEVEL_FORMULA_ID
, REAL_LEVEL_FORMULA_PARAMS
, MARKET_PARAMS
, MARKET_DISABLE_CONDITIONS 
) VALUES (
SEQ_MARKET_DATA_SOURCE_MAPS.NEXTVAL
, 644
, 1
, 10
, '{"internalFormulaClassName": "com.anyoption.service.level.formula.AvgOf2Formula", "formulaConfig": {"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "TRDPRC_1", "lastTradeTimeField": "TIMACT", "subscriptionConfig": {"XAU=ALL":{"identifier":"XAU=ALL","switchToFuture":"false","switchToFuturePrefixLength":"7"}}}}'
, 1
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "TRDPRC_1", "lastTradeTimeField": "TIMACT", "subscriptionConfig": {"XAU=ALL":{"identifier":"XAU=ALL","switchToFuture":"false","switchToFuturePrefixLength":"7"}}, "ignoreTradePrice": "true"}'
, 1
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "TRDPRC_1", "lastTradeTimeField": "TIMACT", "subscriptionConfig": {"XAU=ALL":{"identifier":"XAU=ALL","switchToFuture":"false","switchToFuturePrefixLength":"7"}}, "ignoreTradePrice": "true"}'
, null
, null
, 1
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "TRDPRC_1", "lastTradeTimeField": "TIMACT", "subscriptionConfig": {"XAU=ALL":{"identifier":"XAU=ALL","switchToFuture":"false","switchToFuturePrefixLength":"7"}}}'
, '{"dontCloseAfterXSecNoUpdate": "240", "timeZone": "GMT", "shouldBeClosedByMonitoring": "true"}'
, null
);

INSERT INTO MARKET_DATA_SOURCE_MAPS (
  ID
, MARKET_ID
, MARKET_DATA_SOURCE_ID
, HOUR_LEVEL_FORMULA_ID
, HOUR_LEVEL_FORMULA_PARAMS
, HOUR_CLOSING_FORMULA_ID
, HOUR_CLOSING_FORMULA_PARAMS
, DAY_CLOSING_FORMULA_ID
, DAY_CLOSING_FORMULA_PARAMS
, LONG_TERM_FORMULA_ID
, LONG_TERM_FORMULA_PARAMS
, REAL_LEVEL_FORMULA_ID
, REAL_LEVEL_FORMULA_PARAMS
, MARKET_PARAMS
, MARKET_DISABLE_CONDITIONS 
) VALUES (
SEQ_MARKET_DATA_SOURCE_MAPS.NEXTVAL
, 645
, 1
, 10
, '{"internalFormulaClassName": "com.anyoption.service.level.formula.IdentationFormula", "formulaConfig": {"field1Name": "TRDPRC_1", "lastTradeTimeField": "TRDTIM_1", "subscriptionConfig": {".DJI":{"identifier":".DJI"}}}}'
, 3
, '{"field1Name": "TRDPRC_1", "lastTradeTimeField": "TRDTIM_1", "subscriptionConfig": {".DJI":{"identifier":".DJI"}}}'
, 3
, '{"field1Name": "TRDPRC_1", "lastTradeTimeField": "TRDTIM_1", "subscriptionConfig": {".DJI":{"identifier":".DJI"}}}'
, null
, null
, 3
, '{"field1Name": "TRDPRC_1", "lastTradeTimeField": "TRDTIM_1", "subscriptionConfig": {".DJI":{"identifier":".DJI"}}}'
, '{"dontCloseAfterXSecNoUpdate": "60", "timeZone": "GMT", "shouldBeClosedByMonitoring": "true"}'
, null
);

INSERT INTO MARKET_DATA_SOURCE_MAPS (
  ID
, MARKET_ID
, MARKET_DATA_SOURCE_ID
, HOUR_LEVEL_FORMULA_ID
, HOUR_LEVEL_FORMULA_PARAMS
, HOUR_CLOSING_FORMULA_ID
, HOUR_CLOSING_FORMULA_PARAMS
, DAY_CLOSING_FORMULA_ID
, DAY_CLOSING_FORMULA_PARAMS
, LONG_TERM_FORMULA_ID
, LONG_TERM_FORMULA_PARAMS
, REAL_LEVEL_FORMULA_ID
, REAL_LEVEL_FORMULA_PARAMS
, MARKET_PARAMS
, MARKET_DISABLE_CONDITIONS 
) VALUES (
SEQ_MARKET_DATA_SOURCE_MAPS.NEXTVAL
, 635
, 1
, 10
, '{"internalFormulaClassName": "com.anyoption.service.level.formula.AvgOf2Formula", "formulaConfig": {"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {"CLv1":{"identifier":"CLv1","switchToFuture":"true","switchToFuturePrefixLength":"2"}}}}'
, 3
, '{"field1Name": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {"CLv1":{"identifier":"CLv1","switchToFuture":"true","switchToFuturePrefixLength":"2"}}}'
, 3
, '{"field1Name": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {"CLv1":{"identifier":"CLv1","switchToFuture":"true","switchToFuturePrefixLength":"2"}}}'
, null
, null
, 3
, '{"field1Name": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {"CLv1":{"identifier":"CLv1","switchToFuture":"true","switchToFuturePrefixLength":"2"}}}'
, '{"dontCloseAfterXSecNoUpdate": "120", "timeZone": "GMT", "shouldBeClosedByMonitoring": "true"}'
, null
);

INSERT INTO MARKET_DATA_SOURCE_MAPS (
  ID
, MARKET_ID
, MARKET_DATA_SOURCE_ID
, HOUR_LEVEL_FORMULA_ID
, HOUR_LEVEL_FORMULA_PARAMS
, HOUR_CLOSING_FORMULA_ID
, HOUR_CLOSING_FORMULA_PARAMS
, DAY_CLOSING_FORMULA_ID
, DAY_CLOSING_FORMULA_PARAMS
, LONG_TERM_FORMULA_ID
, LONG_TERM_FORMULA_PARAMS
, REAL_LEVEL_FORMULA_ID
, REAL_LEVEL_FORMULA_PARAMS
, MARKET_PARAMS
, MARKET_DISABLE_CONDITIONS 
) VALUES (
SEQ_MARKET_DATA_SOURCE_MAPS.NEXTVAL
, 636
, 1
, 10
, '{"internalFormulaClassName": "com.anyoption.service.level.formula.IdentationFormula", "formulaConfig": {"field1Name": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {".GDAXI":{"identifier":".GDAXI"}}}}'
, 3
, '{"field1Name": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {".GDAXI":{"identifier":".GDAXI"}}}'
, 3
, '{"field1Name": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {".GDAXI":{"identifier":".GDAXI"}}}'
, null
, null
, 3
, '{"field1Name": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {".GDAXI":{"identifier":".GDAXI"}}}'
, '{"dontCloseAfterXSecNoUpdate": "60", "timeZone": "Europe/Berlin", "shouldBeClosedByMonitoring": "true"}'
, null
);

INSERT INTO MARKET_DATA_SOURCE_MAPS (
  ID
, MARKET_ID
, MARKET_DATA_SOURCE_ID
, HOUR_LEVEL_FORMULA_ID
, HOUR_LEVEL_FORMULA_PARAMS
, HOUR_CLOSING_FORMULA_ID
, HOUR_CLOSING_FORMULA_PARAMS
, DAY_CLOSING_FORMULA_ID
, DAY_CLOSING_FORMULA_PARAMS
, LONG_TERM_FORMULA_ID
, LONG_TERM_FORMULA_PARAMS
, REAL_LEVEL_FORMULA_ID
, REAL_LEVEL_FORMULA_PARAMS
, MARKET_PARAMS
, MARKET_DISABLE_CONDITIONS 
) VALUES (
SEQ_MARKET_DATA_SOURCE_MAPS.NEXTVAL
, 625
, 1
, 11
, '{"internalFormulaClassName": "com.anyoption.service.level.formula.Last5UpdatesAvgOf2Formula", "formulaConfig": {"field1Name": "ASK", "field2Name": "BID", "subscriptionConfig": {"EUR=": {"identifier": "EUR="}}}}'
, 1
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "PRIMACT_1", "lastTradeTimeField": "TIMACT", "subscriptionConfig": {"EUR=":{"identifier":"EUR="}}, "ignoreTradePrice": "true"}'
, 1
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "PRIMACT_1", "lastTradeTimeField": "TIMACT", "subscriptionConfig": {"EUR=":{"identifier":"EUR="}}, "ignoreTradePrice": "true"}'
, null
, null
, 1
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "PRIMACT_1", "lastTradeTimeField": "TIMACT", "subscriptionConfig": {"EUR=":{"identifier":"EUR="}}}'
, '{"dontCloseAfterXSecNoUpdate": "60", "timeZone": "GMT", "shouldBeClosedByMonitoring": "false"}'
, null
);

INSERT INTO MARKET_DATA_SOURCE_MAPS (
  ID
, MARKET_ID
, MARKET_DATA_SOURCE_ID
, HOUR_LEVEL_FORMULA_ID
, HOUR_LEVEL_FORMULA_PARAMS
, HOUR_CLOSING_FORMULA_ID
, HOUR_CLOSING_FORMULA_PARAMS
, DAY_CLOSING_FORMULA_ID
, DAY_CLOSING_FORMULA_PARAMS
, LONG_TERM_FORMULA_ID
, LONG_TERM_FORMULA_PARAMS
, REAL_LEVEL_FORMULA_ID
, REAL_LEVEL_FORMULA_PARAMS
, MARKET_PARAMS
, MARKET_DISABLE_CONDITIONS 
) VALUES (
SEQ_MARKET_DATA_SOURCE_MAPS.NEXTVAL
, 633
, 1
, 11
, '{"internalFormulaClassName": "com.anyoption.service.level.formula.Last5UpdatesAvgOf2Formula", "formulaConfig": {"field1Name": "ASK", "field2Name": "BID", "subscriptionConfig": {"GBP=": {"identifier": "GBP="}}}}'
, 1
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "PRIMACT_1", "lastTradeTimeField": "TIMACT", "subscriptionConfig": {"GBP=":{"identifier":"GBP="}}, "ignoreTradePrice": "true"}'
, 1
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "PRIMACT_1", "lastTradeTimeField": "TIMACT", "subscriptionConfig": {"GBP=":{"identifier":"GBP="}}, "ignoreTradePrice": "true"}'
, null
, null
, 1
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "PRIMACT_1", "lastTradeTimeField": "TIMACT", "subscriptionConfig": {"GBP=":{"identifier":"GBP="}}}'
, '{"dontCloseAfterXSecNoUpdate": "60", "timeZone": "GMT", "shouldBeClosedByMonitoring": "false"}'
, null
);

INSERT INTO MARKET_DATA_SOURCE_MAPS (
  ID
, MARKET_ID
, MARKET_DATA_SOURCE_ID
, HOUR_LEVEL_FORMULA_ID
, HOUR_LEVEL_FORMULA_PARAMS
, HOUR_CLOSING_FORMULA_ID
, HOUR_CLOSING_FORMULA_PARAMS
, DAY_CLOSING_FORMULA_ID
, DAY_CLOSING_FORMULA_PARAMS
, LONG_TERM_FORMULA_ID
, LONG_TERM_FORMULA_PARAMS
, REAL_LEVEL_FORMULA_ID
, REAL_LEVEL_FORMULA_PARAMS
, MARKET_PARAMS
, MARKET_DISABLE_CONDITIONS 
) VALUES (
SEQ_MARKET_DATA_SOURCE_MAPS.NEXTVAL
, 634
, 1
, 11
, '{"internalFormulaClassName": "com.anyoption.service.level.formula.Last5UpdatesAvgOf2Formula", "formulaConfig": {"field1Name": "ASK", "field2Name": "BID", "subscriptionConfig": {"EURJPY=": {"identifier": "EURJPY="}}}}'
, 1
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "PRIMACT_1", "lastTradeTimeField": "TIMACT", "subscriptionConfig": {"EURJPY=":{"identifier":"EURJPY="}}, "ignoreTradePrice": "true"}'
, 1
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "PRIMACT_1", "lastTradeTimeField": "TIMACT", "subscriptionConfig": {"EURJPY=":{"identifier":"EURJPY="}}, "ignoreTradePrice": "true"}'
, null
, null
, 1
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "PRIMACT_1", "lastTradeTimeField": "TIMACT", "subscriptionConfig": {"EURJPY=":{"identifier":"EURJPY="}}}'
, '{"dontCloseAfterXSecNoUpdate": "60", "timeZone": "GMT", "shouldBeClosedByMonitoring": "false"}'
, null
);

INSERT INTO MARKET_DATA_SOURCE_MAPS (
  ID
, MARKET_ID
, MARKET_DATA_SOURCE_ID
, HOUR_LEVEL_FORMULA_ID
, HOUR_LEVEL_FORMULA_PARAMS
, HOUR_CLOSING_FORMULA_ID
, HOUR_CLOSING_FORMULA_PARAMS
, DAY_CLOSING_FORMULA_ID
, DAY_CLOSING_FORMULA_PARAMS
, LONG_TERM_FORMULA_ID
, LONG_TERM_FORMULA_PARAMS
, REAL_LEVEL_FORMULA_ID
, REAL_LEVEL_FORMULA_PARAMS
, MARKET_PARAMS
, MARKET_DISABLE_CONDITIONS 
) VALUES (
SEQ_MARKET_DATA_SOURCE_MAPS.NEXTVAL
, 688
, 1
, 11
, '{"internalFormulaClassName": "com.anyoption.service.level.formula.Last5UpdatesAvgOf2Formula", "formulaConfig": {"field1Name": "ASK", "field2Name": "BID", "subscriptionConfig": {"SEK=": {"identifier": "SEK="}}}}'
, 1
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "PRIMACT_1", "lastTradeTimeField": "TIMACT", "subscriptionConfig": {"SEK=":{"identifier":"SEK="}}, "ignoreTradePrice": "true"}'
, 1
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "PRIMACT_1", "lastTradeTimeField": "TIMACT", "subscriptionConfig": {"SEK=":{"identifier":"SEK="}}, "ignoreTradePrice": "true"}'
, null
, null
, 1
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "PRIMACT_1", "lastTradeTimeField": "TIMACT", "subscriptionConfig": {"SEK=":{"identifier":"SEK="}}}'
, '{"dontCloseAfterXSecNoUpdate": "60", "timeZone": "GMT", "shouldBeClosedByMonitoring": "false"}'
, null
);

INSERT INTO MARKET_DATA_SOURCE_MAPS (
  ID
, MARKET_ID
, MARKET_DATA_SOURCE_ID
, HOUR_LEVEL_FORMULA_ID
, HOUR_LEVEL_FORMULA_PARAMS
, HOUR_CLOSING_FORMULA_ID
, HOUR_CLOSING_FORMULA_PARAMS
, DAY_CLOSING_FORMULA_ID
, DAY_CLOSING_FORMULA_PARAMS
, LONG_TERM_FORMULA_ID
, LONG_TERM_FORMULA_PARAMS
, REAL_LEVEL_FORMULA_ID
, REAL_LEVEL_FORMULA_PARAMS
, MARKET_PARAMS
, MARKET_DISABLE_CONDITIONS 
) VALUES (
SEQ_MARKET_DATA_SOURCE_MAPS.NEXTVAL
, 689
, 1
, 11
, '{"internalFormulaClassName": "com.anyoption.service.level.formula.Last5UpdatesAvgOf2Formula", "formulaConfig": {"field1Name": "ASK", "field2Name": "BID", "subscriptionConfig": {"AUD=": {"identifier": "AUD="}}}}'
, 1
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "PRIMACT_1", "lastTradeTimeField": "TIMACT", "subscriptionConfig": {"AUD=":{"identifier":"AUD="}}, "ignoreTradePrice": "true"}'
, 1
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "PRIMACT_1", "lastTradeTimeField": "TIMACT", "subscriptionConfig": {"AUD=":{"identifier":"AUD="}}, "ignoreTradePrice": "true"}'
, null
, null
, 1
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "PRIMACT_1", "lastTradeTimeField": "TIMACT", "subscriptionConfig": {"AUD=":{"identifier":"AUD="}}}'
, '{"dontCloseAfterXSecNoUpdate": "60", "timeZone": "GMT", "shouldBeClosedByMonitoring": "false"}'
, null
);
-- End of binary 0100 markets

-- Dynamics markets
INSERT INTO MARKET_DATA_SOURCE_MAPS (
  ID
, MARKET_ID
, MARKET_DATA_SOURCE_ID
, HOUR_LEVEL_FORMULA_ID
, HOUR_LEVEL_FORMULA_PARAMS
, HOUR_CLOSING_FORMULA_ID
, HOUR_CLOSING_FORMULA_PARAMS
, DAY_CLOSING_FORMULA_ID
, DAY_CLOSING_FORMULA_PARAMS
, LONG_TERM_FORMULA_ID
, LONG_TERM_FORMULA_PARAMS
, REAL_LEVEL_FORMULA_ID
, REAL_LEVEL_FORMULA_PARAMS
, MARKET_PARAMS
, MARKET_DISABLE_CONDITIONS 
) VALUES (
SEQ_MARKET_DATA_SOURCE_MAPS.NEXTVAL
, 719
, 1
, 11
, '{"internalFormulaClassName": "com.anyoption.service.level.formula.Last5UpdatesAvgOf2Formula", "formulaConfig": {"field1Name": "ASK", "field2Name": "BID", "subscriptionConfig": {"EUR=": {"identifier": "EUR="}}}}'
, 1
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "PRIMACT_1", "lastTradeTimeField": "TIMACT", "subscriptionConfig": {"EUR=":{"identifier":"EUR="}}, "ignoreTradePrice": "true"}'
, 1
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "PRIMACT_1", "lastTradeTimeField": "TIMACT", "subscriptionConfig": {"EUR=":{"identifier":"EUR="}}, "ignoreTradePrice": "true"}'
, null
, null
, 1
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "PRIMACT_1", "lastTradeTimeField": "TIMACT", "subscriptionConfig": {"EUR=":{"identifier":"EUR="}}}'
, '{"dontCloseAfterXSecNoUpdate": "60", "timeZone": "GMT", "shouldBeClosedByMonitoring": "false"}'
, null
);

INSERT INTO MARKET_DATA_SOURCE_MAPS (
  ID
, MARKET_ID
, MARKET_DATA_SOURCE_ID
, HOUR_LEVEL_FORMULA_ID
, HOUR_LEVEL_FORMULA_PARAMS
, HOUR_CLOSING_FORMULA_ID
, HOUR_CLOSING_FORMULA_PARAMS
, DAY_CLOSING_FORMULA_ID
, DAY_CLOSING_FORMULA_PARAMS
, LONG_TERM_FORMULA_ID
, LONG_TERM_FORMULA_PARAMS
, REAL_LEVEL_FORMULA_ID
, REAL_LEVEL_FORMULA_PARAMS
, MARKET_PARAMS
, MARKET_DISABLE_CONDITIONS 
) VALUES (
SEQ_MARKET_DATA_SOURCE_MAPS.NEXTVAL
, 725
, 1
, 11
, '{"internalFormulaClassName": "com.anyoption.service.level.formula.Last5UpdatesAvgOf2Formula", "formulaConfig": {"field1Name": "ASK", "field2Name": "BID", "subscriptionConfig": {"GBP=": {"identifier": "GBP="}}}}'
, 1
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "PRIMACT_1", "lastTradeTimeField": "TIMACT", "subscriptionConfig": {"GBP=":{"identifier":"GBP="}}, "ignoreTradePrice": "true"}'
, 1
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "PRIMACT_1", "lastTradeTimeField": "TIMACT", "subscriptionConfig": {"GBP=":{"identifier":"GBP="}}, "ignoreTradePrice": "true"}'
, null
, null
, 1
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "PRIMACT_1", "lastTradeTimeField": "TIMACT", "subscriptionConfig": {"GBP=":{"identifier":"GBP="}}}'
, '{"dontCloseAfterXSecNoUpdate": "60", "timeZone": "GMT", "shouldBeClosedByMonitoring": "false"}'
, null
);

INSERT INTO MARKET_DATA_SOURCE_MAPS (
  ID
, MARKET_ID
, MARKET_DATA_SOURCE_ID
, HOUR_LEVEL_FORMULA_ID
, HOUR_LEVEL_FORMULA_PARAMS
, HOUR_CLOSING_FORMULA_ID
, HOUR_CLOSING_FORMULA_PARAMS
, DAY_CLOSING_FORMULA_ID
, DAY_CLOSING_FORMULA_PARAMS
, LONG_TERM_FORMULA_ID
, LONG_TERM_FORMULA_PARAMS
, REAL_LEVEL_FORMULA_ID
, REAL_LEVEL_FORMULA_PARAMS
, MARKET_PARAMS
, MARKET_DISABLE_CONDITIONS 
) VALUES (
SEQ_MARKET_DATA_SOURCE_MAPS.NEXTVAL
, 726
, 1
, 11
, '{"internalFormulaClassName": "com.anyoption.service.level.formula.Last5UpdatesAvgOf2Formula", "formulaConfig": {"field1Name": "ASK", "field2Name": "BID", "subscriptionConfig": {"AUD=": {"identifier": "AUD="}}}}'
, 1
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "PRIMACT_1", "lastTradeTimeField": "TIMACT", "subscriptionConfig": {"AUD=":{"identifier":"AUD="}}, "ignoreTradePrice": "true"}'
, 1
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "PRIMACT_1", "lastTradeTimeField": "TIMACT", "subscriptionConfig": {"AUD=":{"identifier":"AUD="}}, "ignoreTradePrice": "true"}'
, null
, null
, 1
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "PRIMACT_1", "lastTradeTimeField": "TIMACT", "subscriptionConfig": {"AUD=":{"identifier":"AUD="}}}'
, '{"dontCloseAfterXSecNoUpdate": "60", "timeZone": "GMT", "shouldBeClosedByMonitoring": "false"}'
, null
);

INSERT INTO MARKET_DATA_SOURCE_MAPS (
  ID
, MARKET_ID
, MARKET_DATA_SOURCE_ID
, HOUR_LEVEL_FORMULA_ID
, HOUR_LEVEL_FORMULA_PARAMS
, HOUR_CLOSING_FORMULA_ID
, HOUR_CLOSING_FORMULA_PARAMS
, DAY_CLOSING_FORMULA_ID
, DAY_CLOSING_FORMULA_PARAMS
, LONG_TERM_FORMULA_ID
, LONG_TERM_FORMULA_PARAMS
, REAL_LEVEL_FORMULA_ID
, REAL_LEVEL_FORMULA_PARAMS
, MARKET_PARAMS
, MARKET_DISABLE_CONDITIONS 
) VALUES (
SEQ_MARKET_DATA_SOURCE_MAPS.NEXTVAL
, 727
, 1
, 11
, '{"internalFormulaClassName": "com.anyoption.service.level.formula.Last5UpdatesAvgOf2Formula", "formulaConfig": {"field1Name": "ASK", "field2Name": "BID", "subscriptionConfig": {"SEK=": {"identifier": "SEK="}}}}'
, 1
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "PRIMACT_1", "lastTradeTimeField": "TIMACT", "subscriptionConfig": {"SEK=":{"identifier":"SEK="}}, "ignoreTradePrice": "true"}'
, 1
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "PRIMACT_1", "lastTradeTimeField": "TIMACT", "subscriptionConfig": {"SEK=":{"identifier":"SEK="}}, "ignoreTradePrice": "true"}'
, null
, null
, 1
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "PRIMACT_1", "lastTradeTimeField": "TIMACT", "subscriptionConfig": {"SEK=":{"identifier":"SEK="}}}'
, '{"dontCloseAfterXSecNoUpdate": "60", "timeZone": "GMT", "shouldBeClosedByMonitoring": "false"}'
, null
);

INSERT INTO MARKET_DATA_SOURCE_MAPS (
  ID
, MARKET_ID
, MARKET_DATA_SOURCE_ID
, HOUR_LEVEL_FORMULA_ID
, HOUR_LEVEL_FORMULA_PARAMS
, HOUR_CLOSING_FORMULA_ID
, HOUR_CLOSING_FORMULA_PARAMS
, DAY_CLOSING_FORMULA_ID
, DAY_CLOSING_FORMULA_PARAMS
, LONG_TERM_FORMULA_ID
, LONG_TERM_FORMULA_PARAMS
, REAL_LEVEL_FORMULA_ID
, REAL_LEVEL_FORMULA_PARAMS
, MARKET_PARAMS
, MARKET_DISABLE_CONDITIONS 
) VALUES (
SEQ_MARKET_DATA_SOURCE_MAPS.NEXTVAL
, 728
, 1
, 11
, '{"internalFormulaClassName": "com.anyoption.service.level.formula.Last5UpdatesAvgOf2Formula", "formulaConfig": {"field1Name": "ASK", "field2Name": "BID", "subscriptionConfig": {"EURJPY=": {"identifier": "EURJPY="}}}}'
, 1
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "PRIMACT_1", "lastTradeTimeField": "TIMACT", "subscriptionConfig": {"EURJPY=":{"identifier":"EURJPY="}}, "ignoreTradePrice": "true"}'
, 1
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "PRIMACT_1", "lastTradeTimeField": "TIMACT", "subscriptionConfig": {"EURJPY=":{"identifier":"EURJPY="}}, "ignoreTradePrice": "true"}'
, null
, null
, 1
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "PRIMACT_1", "lastTradeTimeField": "TIMACT", "subscriptionConfig": {"EURJPY=":{"identifier":"EURJPY="}}}'
, '{"dontCloseAfterXSecNoUpdate": "60", "timeZone": "GMT", "shouldBeClosedByMonitoring": "false"}'
, null
);

INSERT INTO MARKET_DATA_SOURCE_MAPS (
  ID
, MARKET_ID
, MARKET_DATA_SOURCE_ID
, HOUR_LEVEL_FORMULA_ID
, HOUR_LEVEL_FORMULA_PARAMS
, HOUR_CLOSING_FORMULA_ID
, HOUR_CLOSING_FORMULA_PARAMS
, DAY_CLOSING_FORMULA_ID
, DAY_CLOSING_FORMULA_PARAMS
, LONG_TERM_FORMULA_ID
, LONG_TERM_FORMULA_PARAMS
, REAL_LEVEL_FORMULA_ID
, REAL_LEVEL_FORMULA_PARAMS
, MARKET_PARAMS
, MARKET_DISABLE_CONDITIONS 
) VALUES (
SEQ_MARKET_DATA_SOURCE_MAPS.NEXTVAL
, 729
, 1
, 10
, '{"internalFormulaClassName": "com.anyoption.service.level.formula.AvgOf2Formula", "formulaConfig": {"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {"CLv1":{"identifier":"CLv1","switchToFuture":"true","switchToFuturePrefixLength":"2"}}}}'
, 3
, '{"field1Name": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {"CLv1":{"identifier":"CLv1","switchToFuture":"true","switchToFuturePrefixLength":"2"}}}'
, 3
, '{"field1Name": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {"CLv1":{"identifier":"CLv1","switchToFuture":"true","switchToFuturePrefixLength":"2"}}}'
, null
, null
, 3
, '{"field1Name": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {"CLv1":{"identifier":"CLv1","switchToFuture":"true","switchToFuturePrefixLength":"2"}}}'
, '{"dontCloseAfterXSecNoUpdate": "120", "timeZone": "GMT", "shouldBeClosedByMonitoring": "true"}'
, null
);

INSERT INTO MARKET_DATA_SOURCE_MAPS (
  ID
, MARKET_ID
, MARKET_DATA_SOURCE_ID
, HOUR_LEVEL_FORMULA_ID
, HOUR_LEVEL_FORMULA_PARAMS
, HOUR_CLOSING_FORMULA_ID
, HOUR_CLOSING_FORMULA_PARAMS
, DAY_CLOSING_FORMULA_ID
, DAY_CLOSING_FORMULA_PARAMS
, LONG_TERM_FORMULA_ID
, LONG_TERM_FORMULA_PARAMS
, REAL_LEVEL_FORMULA_ID
, REAL_LEVEL_FORMULA_PARAMS
, MARKET_PARAMS
, MARKET_DISABLE_CONDITIONS 
) VALUES (
SEQ_MARKET_DATA_SOURCE_MAPS.NEXTVAL
, 730
, 1
, 10
, '{"internalFormulaClassName": "com.anyoption.service.level.formula.IdentationFormula", "formulaConfig": {"field1Name": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {".GDAXI":{"identifier":".GDAXI"}}}}'
, 3
, '{"field1Name": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {".GDAXI":{"identifier":".GDAXI"}}}'
, 3
, '{"field1Name": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {".GDAXI":{"identifier":".GDAXI"}}}'
, null
, null
, 3
, '{"field1Name": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {".GDAXI":{"identifier":".GDAXI"}}}'
, '{"dontCloseAfterXSecNoUpdate": "60", "timeZone": "Europe/Berlin", "shouldBeClosedByMonitoring": "true"}'
, null
);

INSERT INTO MARKET_DATA_SOURCE_MAPS (
  ID
, MARKET_ID
, MARKET_DATA_SOURCE_ID
, HOUR_LEVEL_FORMULA_ID
, HOUR_LEVEL_FORMULA_PARAMS
, HOUR_CLOSING_FORMULA_ID
, HOUR_CLOSING_FORMULA_PARAMS
, DAY_CLOSING_FORMULA_ID
, DAY_CLOSING_FORMULA_PARAMS
, LONG_TERM_FORMULA_ID
, LONG_TERM_FORMULA_PARAMS
, REAL_LEVEL_FORMULA_ID
, REAL_LEVEL_FORMULA_PARAMS
, MARKET_PARAMS
, MARKET_DISABLE_CONDITIONS 
) VALUES (
SEQ_MARKET_DATA_SOURCE_MAPS.NEXTVAL
, 731
, 1
, 10
, '{"internalFormulaClassName": "com.anyoption.service.level.formula.IdentationFormula", "formulaConfig": {"field1Name": "TRDPRC_1", "lastTradeTimeField": "TRDTIM_1", "subscriptionConfig": {".DJI":{"identifier":".DJI"}}}}'
, 3
, '{"field1Name": "TRDPRC_1", "lastTradeTimeField": "TRDTIM_1", "subscriptionConfig": {".DJI":{"identifier":".DJI"}}}'
, 3
, '{"field1Name": "TRDPRC_1", "lastTradeTimeField": "TRDTIM_1", "subscriptionConfig": {".DJI":{"identifier":".DJI"}}}'
, null
, null
, 3
, '{"field1Name": "TRDPRC_1", "lastTradeTimeField": "TRDTIM_1", "subscriptionConfig": {".DJI":{"identifier":".DJI"}}}'
, '{"dontCloseAfterXSecNoUpdate": "60", "timeZone": "GMT", "shouldBeClosedByMonitoring": "true"}'
, null
);

INSERT INTO MARKET_DATA_SOURCE_MAPS (
  ID
, MARKET_ID
, MARKET_DATA_SOURCE_ID
, HOUR_LEVEL_FORMULA_ID
, HOUR_LEVEL_FORMULA_PARAMS
, HOUR_CLOSING_FORMULA_ID
, HOUR_CLOSING_FORMULA_PARAMS
, DAY_CLOSING_FORMULA_ID
, DAY_CLOSING_FORMULA_PARAMS
, LONG_TERM_FORMULA_ID
, LONG_TERM_FORMULA_PARAMS
, REAL_LEVEL_FORMULA_ID
, REAL_LEVEL_FORMULA_PARAMS
, MARKET_PARAMS
, MARKET_DISABLE_CONDITIONS 
) VALUES (
SEQ_MARKET_DATA_SOURCE_MAPS.NEXTVAL
, 732
, 1
, 10
, '{"internalFormulaClassName": "com.anyoption.service.level.formula.AvgOf2Formula", "formulaConfig": {"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "TRDPRC_1", "lastTradeTimeField": "TIMACT", "subscriptionConfig": {"XAU=ALL":{"identifier":"XAU=ALL","switchToFuture":"false","switchToFuturePrefixLength":"7"}}}}'
, 1
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "TRDPRC_1", "lastTradeTimeField": "TIMACT", "subscriptionConfig": {"XAU=ALL":{"identifier":"XAU=ALL","switchToFuture":"false","switchToFuturePrefixLength":"7"}}, "ignoreTradePrice": "true"}'
, 1
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "TRDPRC_1", "lastTradeTimeField": "TIMACT", "subscriptionConfig": {"XAU=ALL":{"identifier":"XAU=ALL","switchToFuture":"false","switchToFuturePrefixLength":"7"}}, "ignoreTradePrice": "true"}'
, null
, null
, 1
, '{"field1Name": "ASK", "field2Name": "BID", "lastTradePriceField": "TRDPRC_1", "lastTradeTimeField": "TIMACT", "subscriptionConfig": {"XAU=ALL":{"identifier":"XAU=ALL","switchToFuture":"false","switchToFuturePrefixLength":"7"}}}'
, '{"dontCloseAfterXSecNoUpdate": "240", "timeZone": "GMT", "shouldBeClosedByMonitoring": "true"}'
, null
);
-- End of dynamics markets

-- Tel Aviv 25 market
INSERT INTO MARKET_DATA_SOURCE_MAPS (
  ID
, MARKET_ID
, MARKET_DATA_SOURCE_ID
, HOUR_LEVEL_FORMULA_ID
, HOUR_LEVEL_FORMULA_PARAMS
, HOUR_CLOSING_FORMULA_ID
, HOUR_CLOSING_FORMULA_PARAMS
, DAY_CLOSING_FORMULA_ID
, DAY_CLOSING_FORMULA_PARAMS
, LONG_TERM_FORMULA_ID
, LONG_TERM_FORMULA_PARAMS
, REAL_LEVEL_FORMULA_ID
, REAL_LEVEL_FORMULA_PARAMS
, MARKET_PARAMS
, MARKET_DISABLE_CONDITIONS 
) VALUES (
SEQ_MARKET_DATA_SOURCE_MAPS.NEXTVAL
, 3
, 1
, 13
, '{"mainSubscriptionName": ".TA25", "lastTradePriceField": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "secondarySubscriptionField1": "ASK", "secondarySubscriptionField2": "BID", "secondarySubscriptionField3": "TRDPRC_1", "optionIgnoreAskBidSpread": "100", "subscriptionConfig": {".TA25":{"identifier":".TA25"}}}'
, 3
, '{"field1Name": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {".TA25":{"identifier":".TA25"}}}'
, 12
, '{"field1Name": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "specificValueName": "HST_CLOSE", "presentDayFieldName": "HSTCLSDATE", "secondTimeFieldName": "SALTIM", "subscriptionConfig": {".TA25":{"identifier":".TA25"}}}'
, 9
, '{"interestSubscription": "ILS1MD=", "interestAskField": "ASK", "interestBidField": "BID", "daysConstant": "365", "subscriptionConfig": {".TA25":{"identifier":".TA25"}, "ILS1MD=":{"identifier":"ILS1MD="}}}'
, 3
, '{"field1Name": "TRDPRC_1", "lastTradeTimeField": "SALTIM", "subscriptionConfig": {".TA25":{"identifier":".TA25"}}}'
, '{"dontCloseAfterXSecNoUpdate": "120", "timeZone": "GMT", "shouldBeClosedByMonitoring": "true"}'
, null
);
-- End of Tel Aviv 25 market