REM INSERTING into STATES
Insert into STATES (ID,STATE_NAME) values (1,'Alabama');
Insert into STATES (ID,STATE_NAME) values (2,'Alaska');
Insert into STATES (ID,STATE_NAME) values (3,'Arizona');
Insert into STATES (ID,STATE_NAME) values (4,'Arkansas');
Insert into STATES (ID,STATE_NAME) values (5,'California');
Insert into STATES (ID,STATE_NAME) values (6,'Colorado');
Insert into STATES (ID,STATE_NAME) values (7,'Connecticut');
Insert into STATES (ID,STATE_NAME) values (8,'Delaware');
Insert into STATES (ID,STATE_NAME) values (9,'Florida');
Insert into STATES (ID,STATE_NAME) values (10,'Georgia');
Insert into STATES (ID,STATE_NAME) values (11,'Hawaii');
Insert into STATES (ID,STATE_NAME) values (12,'Idaho');
Insert into STATES (ID,STATE_NAME) values (13,'Illinois');
Insert into STATES (ID,STATE_NAME) values (14,'Indiana');
Insert into STATES (ID,STATE_NAME) values (15,'Iowa');
Insert into STATES (ID,STATE_NAME) values (16,'Kansas');
Insert into STATES (ID,STATE_NAME) values (17,'Kentucky');
Insert into STATES (ID,STATE_NAME) values (18,'Louisiana');
Insert into STATES (ID,STATE_NAME) values (19,'Maine');
Insert into STATES (ID,STATE_NAME) values (20,'Maryland');
Insert into STATES (ID,STATE_NAME) values (21,'Massachusetts');
Insert into STATES (ID,STATE_NAME) values (22,'Michigan');
Insert into STATES (ID,STATE_NAME) values (23,'Minnesota');
Insert into STATES (ID,STATE_NAME) values (24,'Mississippi');
Insert into STATES (ID,STATE_NAME) values (25,'Missouri');
Insert into STATES (ID,STATE_NAME) values (26,'Montana');
Insert into STATES (ID,STATE_NAME) values (27,'Nebraska');
Insert into STATES (ID,STATE_NAME) values (28,'Nevada');
Insert into STATES (ID,STATE_NAME) values (29,'New Hampshire');
Insert into STATES (ID,STATE_NAME) values (30,'New Jersey');
Insert into STATES (ID,STATE_NAME) values (31,'New Mexico');
Insert into STATES (ID,STATE_NAME) values (32,'New York');
Insert into STATES (ID,STATE_NAME) values (33,'North Carolina');
Insert into STATES (ID,STATE_NAME) values (34,'North Dakota');
Insert into STATES (ID,STATE_NAME) values (35,'Ohio');
Insert into STATES (ID,STATE_NAME) values (36,'Oklahoma');
Insert into STATES (ID,STATE_NAME) values (37,'Oregon');
Insert into STATES (ID,STATE_NAME) values (38,'Pennsylvania');
Insert into STATES (ID,STATE_NAME) values (39,'Rhode Island');
Insert into STATES (ID,STATE_NAME) values (40,'South Carolina');
Insert into STATES (ID,STATE_NAME) values (41,'South Dakota');
Insert into STATES (ID,STATE_NAME) values (42,'Tennessee');
Insert into STATES (ID,STATE_NAME) values (43,'Texas');
Insert into STATES (ID,STATE_NAME) values (44,'Utah');
Insert into STATES (ID,STATE_NAME) values (45,'Vermont');
Insert into STATES (ID,STATE_NAME) values (46,'Virginia');
Insert into STATES (ID,STATE_NAME) values (47,'Washington');
Insert into STATES (ID,STATE_NAME) values (48,'West Virginia');
Insert into STATES (ID,STATE_NAME) values (49,'Wisconsin');
Insert into STATES (ID,STATE_NAME) values (50,'Wyoming');
