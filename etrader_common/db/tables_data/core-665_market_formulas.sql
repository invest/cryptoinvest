INSERT INTO MARKET_FORMULAS (
	ID,
	NAME,
	DESCRIPTION,
	FORMULA_CLASS,
	FORMULA_PARAMS,
	TYPE,
	FORMULA_DESCRIPTION_KEY
	) VALUES (
	1,
	'AVG of two',
	'Calculate the AVG of two configured fields.',
	'com.anyoption.service.level.formula.AvgOf2Formula',
	q'{{'field1Name': 'ASK', 'field2Name': 'BID', 'RIC': 'GCv1', 'switchToFuture': 'true', 'switchToFuturePrefixLength': '2'}}',
	1,
	'AvgOf2Formula'
);

INSERT INTO MARKET_FORMULAS (
	ID,
	NAME,
	DESCRIPTION,
	FORMULA_CLASS,
	FORMULA_PARAMS,
	TYPE,
	FORMULA_DESCRIPTION_KEY
	) VALUES (
	2,
	'AVG of three',
	null,
	'com.anyoption.service.level.formula.AvgOf3Formula',
	null,
	1,
	'AvgOf3Formula'
);

INSERT INTO MARKET_FORMULAS (
	ID,
	NAME,
	DESCRIPTION,
	FORMULA_CLASS,
	FORMULA_PARAMS,
	TYPE,
	FORMULA_DESCRIPTION_KEY
	) VALUES (
	3,
	'Identation formula',
	null,
	'com.anyoption.service.level.formula.IdentationFormula',
	null,
	1,
	'IdentationFormula'
);

INSERT INTO MARKET_FORMULAS (
	ID,
	NAME,
	DESCRIPTION,
	FORMULA_CLASS,
	FORMULA_PARAMS,
	TYPE,
	FORMULA_DESCRIPTION_KEY
	) VALUES (
	4,
	'Last five updates AVG of two',
	null,
	'com.anyoption.service.level.formula.Last5UpdatesAvgOf2Formula',
	null,
	1,
	'Last5UpdatesAvgOf2Formula'
);

INSERT INTO MARKET_FORMULAS (
	ID,
	NAME,
	DESCRIPTION,
	FORMULA_CLASS,
	FORMULA_PARAMS,
	TYPE,
	FORMULA_DESCRIPTION_KEY
	) VALUES (
	5,
	'Last five updates AVG of three',
	null,
	'com.anyoption.service.level.formula.Last5UpdatesAvgOf3Formula',
	null,
	1,
	'Last5UpdatesAvgOf3Formula'
);

INSERT INTO MARKET_FORMULAS (
	ID,
	NAME,
	DESCRIPTION,
	FORMULA_CLASS,
	FORMULA_PARAMS,
	TYPE,
	FORMULA_DESCRIPTION_KEY
	) VALUES (
	6,
	'Tow subscribtions AVG of three',
	null,
	'com.anyoption.service.level.formula.TowSubscriptionsAvgOf3Formula',
	null,
	1,
	'TowSubscriptionsAvgOf3Formula'
);

INSERT INTO MARKET_FORMULAS (
	ID,
	NAME,
	DESCRIPTION,
	FORMULA_CLASS,
	FORMULA_PARAMS,
	TYPE,
	FORMULA_DESCRIPTION_KEY
	) VALUES (
	7,
	'Currency FXAL',
	null,
	'com.anyoption.service.level.formula.CurrencyFXALFormula',
	null,
	1,
	'CurrencyFXALFormula'
);

INSERT INTO MARKET_FORMULAS (
	ID,
	NAME,
	DESCRIPTION,
	FORMULA_CLASS,
	FORMULA_PARAMS,
	TYPE,
	FORMULA_DESCRIPTION_KEY
	) VALUES (
	8,
	'Currency long term',
	null,
	'com.anyoption.service.level.formula.CurrencyLongTermFormula',
	null,
	2,
	'CurrencyLongTermFormula'
);

INSERT INTO MARKET_FORMULAS (
	ID,
	NAME,
	DESCRIPTION,
	FORMULA_CLASS,
	FORMULA_PARAMS,
	TYPE,
	FORMULA_DESCRIPTION_KEY
	) VALUES (
	9,
	'Index stock long term',
	null,
	'com.anyoption.service.level.formula.IndexStockLongTermFormula',
	null,
	2,
	'IndexStockLongTermFormula'
);

INSERT INTO MARKET_FORMULAS (
	ID,
	NAME,
	DESCRIPTION,
	FORMULA_CLASS,
	FORMULA_PARAMS,
	TYPE,
	FORMULA_DESCRIPTION_KEY
	) VALUES (
	10,
	'Binary 0100 general',
	null,
	'com.anyoption.service.level.formula.Binary0100GeneralFormula',
	null,
	1,
	'Binary0100GeneralFormula'
);

INSERT INTO MARKET_FORMULAS (
	ID,
	NAME,
	DESCRIPTION,
	FORMULA_CLASS,
	FORMULA_PARAMS,
	TYPE,
	FORMULA_DESCRIPTION_KEY
	) VALUES (
	11,
	'Binary 0100 currency',
	null,
	'com.anyoption.service.level.formula.Binary0100CurrencyFormula',
	null,
	1,
	'Binary0100CurrencyFormula'
);

INSERT INTO MARKET_FORMULAS (
	ID,
	NAME,
	DESCRIPTION,
	FORMULA_CLASS,
	FORMULA_PARAMS,
	TYPE,
	FORMULA_DESCRIPTION_KEY
	) VALUES (
	12,
	'Present Day Identation formula',
	null,
	'com.anyoption.service.level.formula.PresentDayIdentationFormula',
	null,
	1,
	'PresentDayIdentationFormula'
);

INSERT INTO MARKET_FORMULAS (
	ID,
	NAME,
	DESCRIPTION,
	FORMULA_CLASS,
	FORMULA_PARAMS,
	TYPE,
	FORMULA_DESCRIPTION_KEY
	) VALUES (
	13,
	'Tel Aviv 25 index formula',
	null,
	'com.anyoption.service.level.formula.TelAviv25IndexFormula',
	null,
	1,
	'TelAviv25IndexFormula'
);