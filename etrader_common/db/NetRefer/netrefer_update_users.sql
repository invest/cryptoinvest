-- Script 1 - comb 365, dp
update 
  users u
set   
  u.affiliate_system_user_time = trunc(sysdate-1/24),
  u.affiliate_system = 1
where 
  u.affiliate_key != 0 
  and u.affiliate_key is not null
  and u.affiliate_system_user_time is null
  and (trunc(u.time_first_authorized) = trunc(sysdate-1/24)
        OR
        u.id in (select t.user_id 
                 from transactions t, users_first_deposit ufd
                 where t.id = ufd.first_deposit_id
                   and ufd.user_id = t.user_id
                   and trunc(t.time_created) = trunc(sysdate-1/24)))
  and u.combination_id in (select id 
                          from marketing_combinations mcom
                          where mcom.campaign_id = 365) 
  and (LENGTH(SUBSTR(u.dynamic_param,INSTR(u.dynamic_param,'_')+1)) = 32 
  		OR ( -- AFFILIATE API USERS 
  	   		 LENGTH(SUBSTR(u.dynamic_param,INSTR(u.dynamic_param,'_')+1)) = 3
  	   		 AND u.dynamic_param like '%_API'
  	   	   ));
-- Script 2 - special code
update 
  users u
set   
  u.affiliate_system_user_time = trunc(sysdate-1/24),
  u.affiliate_key = (select mac.affiliate_key 
                     from marketing_affilates_code mac 
                     where mac.special_code = u.special_code and rownum = 1),
  u.affiliate_system = 1
where 
  (u.affiliate_key = 0 OR u.affiliate_key is null OR u.dynamic_param like 'API_%' )
  and u.special_code is not null
  and u.affiliate_system_user_time is null
  and (trunc(u.time_first_authorized) = trunc(sysdate-1/24)
        OR
        u.id in (select t.user_id 
                 from transactions t, users_first_deposit ufd
                 where t.id = ufd.first_deposit_id
                   and ufd.user_id = t.user_id
                   and trunc(t.time_created) = trunc(sysdate-1/24)))
  and exists (select mac.affiliate_key 
              from marketing_affilates_code mac 
              where mac.special_code = u.special_code);
-- Script 3 - dp, no affiliate_key
update 
  users u
set
  u.affiliate_system_user_time = trunc(sysdate-1/24),
  u.affiliate_key = SUBSTR(u.dynamic_param,0,INSTR(u.dynamic_param,'_')-1),
  u.affiliate_system = 1
where
   (u.affiliate_key = 0 OR u.affiliate_key is null)
  and u.affiliate_system_user_time is null
  and (trunc(u.time_first_authorized) = trunc(sysdate-1/24)
        OR
        u.id in (select t.user_id 
                 from transactions t, users_first_deposit ufd
                 where t.id = ufd.first_deposit_id
                   and ufd.user_id = t.user_id
                   and trunc(t.time_created) = trunc(sysdate-1/24)))
   and (LENGTH(SUBSTR(u.dynamic_param,INSTR(u.dynamic_param,'_')+1)) = 32
		   OR ( -- AFFILIATE API USERS 
		  	   		 LENGTH(SUBSTR(u.dynamic_param,INSTR(u.dynamic_param,'_')+1)) = 3
		  	   		 AND u.dynamic_param like '%_API'
		  	  ))   
   and REGEXP_LIKE(SUBSTR(u.dynamic_param,0,INSTR(u.dynamic_param,'_')-1), '^[0-9]') 
   and (u.dynamic_param not like 'RPOA_%' OR u.combination_id not in
          (select 
                  id 
          from 
                  marketing_combinations mcom
          where 
                  mcom.campaign_id = 1535));
-- Script 4 - comb 365, no dp
update 
  users u
set   
  u.affiliate_system_user_time = trunc(sysdate-1/24),
  u.affiliate_key = 655276, 
  u.special_code = 'financialqueen',
  u.affiliate_system = 1
where 
  (u.affiliate_key = 0 OR u.affiliate_key is null)
  and u.affiliate_system_user_time is null
  and (trunc(u.time_first_authorized) = trunc(sysdate-1/24)
        OR
        u.id in (select t.user_id 
                 from transactions t, users_first_deposit ufd
                 where t.id = ufd.first_deposit_id
                   and ufd.user_id = t.user_id
                   and trunc(t.time_created) = trunc(sysdate-1/24)))
  and u.combination_id in (select id 
                          from marketing_combinations mcom
                          where mcom.campaign_id = 365) 
  and u.dynamic_param is null ;