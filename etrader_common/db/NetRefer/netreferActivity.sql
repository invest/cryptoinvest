select * from (

SELECT
          u.id customerID,
          to_char(sysdate-1/24,'yyyy-mm-dd') "Activity Date",
          'anyoption' "Product ID",
          0  "Bonuses",
          (NVL(deposits.sum_deposits_ao,0)) *0.06 + NVL(cft_withdraws.cft_withdraws_num * 2,0)
                  + NVL (wire_withdraws.wire_withdraws_fee,0) "Adjustments",
          NVL(deposits.sum_deposits_ao,0)  "Deposits",
          NVL(sums_investments.house_win_ao,0)  - NVL(sum_fix_balances.fixBalance,0)  "Gross Revenue",
          NVL(sums_investments.turnover_ao,0) "Turnover",
          NVL(withdraws.sum_withdraws_ao,0) "payouts",
          NVL(sums_investments.investments_num,0) "Transactions",
          decode(u.skin_id, 22, 2, 9, 3, 20, 3, 1) "BrandID",
          'web' "Origin"
FROM  
              users u
              LEFT JOIN
                (SELECT 
                        t.user_id,
                        SUM((CASE WHEN (t.status_id IN (2, 7)) THEN  t.amount*t.rate ELSE t.amount*t.rate*(-1) END) /100) as sum_deposits_ao
                  FROM 
                        transaction_types tt, 
                        transactions t 
                              left join charge_backs cb on cb.id = t.CHARGE_BACK_ID
                  WHERE t.type_id = tt.id
                        AND t.writer_id in (1,10000,21000) --web
                        AND tt.class_type in (1, 11)   --  11: deposit by company
                        AND (
                                     (t.status_id IN (2, 7)  AND to_char(t.time_created,'dd/mm/yyyy') = to_char(sysdate-1/24,'dd/mm/yyyy'))
                                     OR
                                     (t.status_id IN (8,12,13)  AND to_char(t.time_settled,'dd/mm/yyyy') = to_char(sysdate-1/24,'dd/mm/yyyy') AND
                                      to_char(t.time_settled,'dd/mm/yyyy') <> to_char(t.time_created,'dd/mm/yyyy') )
                                      OR
                                      (t.status_id = 14 AND to_char(cb.time_created,'dd/mm/yyyy') = to_char(sysdate-1/24,'dd/mm/yyyy') AND
                                      to_char(t.time_created,'dd/mm/yyyy') <> to_char(cb.time_created,'dd/mm/yyyy'))
                                   )
                        AND t.user_id in (select u2.id
                                           from users u2
                                           )
                  GROUP BY t.user_id) deposits on u.id = deposits.user_id
                  ---------------------------------------------------------------------
                  LEFT JOIN
                                    (SELECT  t.user_id, SUM(CASE WHEN t.type_id = 22 THEN
                                                                           -- if  TRANS_TYPE_FIX_BALANCE_DEPOSIT
                                                                           t.amount * t.rate
                                                                       ELSE
                                                                           -- if  TRANS_TYPE_FIX_BALANCE_WITHDRAW
                                                                           t.amount * t.rate * (-1) END)/100 fixBalance
                  FROM transactions t
                  WHERE
                          t.writer_id in (1,10000,21000) AND --web
                          t.type_id in (22,23) AND
                          to_char(t.time_created,'dd/mm/yyyy') = to_char(sysdate-1/24,'dd/mm/yyyy') AND
                          t.user_id in (select u2.id
                                        from users u2
                                        )
                  GROUP BY t.user_id) sum_fix_balances on u.id = sum_fix_balances.user_id
                  ---------------------------------------------------------------------
                 LEFT JOIN
                (SELECT t.user_id,  SUM(t.amount*t.rate/100) as sum_withdraws_ao
                  FROM transactions t, transaction_types tt
                  WHERE t.type_id = tt.id
                        AND t.writer_id in (1,10000,21000) --web
                        AND tt.class_type = 2
                        AND t.status_id IN (2)
                        AND to_char(t.time_settled,'dd/mm/yyyy') = to_char(sysdate-1/24,'dd/mm/yyyy')
                        AND t.user_id in (select u2.id
                                          from users u2
                                          )
                  GROUP BY t.user_id) withdraws on u.id = withdraws.user_id
                ---------------------------------------------------------------------
                LEFT JOIN
                (SELECT t.user_id,  COUNT(t.id) as cft_withdraws_num
                  FROM transactions t
                  WHERE t.type_id = 10 -- CC withdraw
                        AND t.writer_id in (1,10000,21000) --web
                        AND t.status_id IN (2)
                        AND t.clearing_provider_id = 2 -- Xor Anyoption
                        AND t.is_credit_withdrawal = 0
                        AND to_char(t.time_settled,'dd/mm/yyyy') = to_char(sysdate-1/24,'dd/mm/yyyy')
                        AND t.user_id in (select u2.id
                                          from users u2
                                          )
                  GROUP BY t.user_id) cft_withdraws on u.id = cft_withdraws.user_id
                  ---------------------------------------------------------------------
                   LEFT JOIN
                (SELECT t.user_id,  SUM(w.bank_fee_amount* t.rate) as wire_withdraws_fee
                  FROM transactions t, wires w
                  WHERE t.type_id = 14 -- Bank wire withdraw
                        AND t.writer_id in (1,10000,21000) --web
                        AND t.status_id IN (2)
                        AND t.wire_id = w.id
                        AND to_char(t.time_settled,'dd/mm/yyyy') = to_char(sysdate-1/24,'dd/mm/yyyy')
                        AND t.user_id in (select u2.id
                                          from users u2
                                          )
                  GROUP BY t.user_id) wire_withdraws on u.id = wire_withdraws.user_id
                 ---------------------------------------------------------------------
                  LEFT JOIN
                 (SELECT i.user_id,
                                  SUM((CASE WHEN i.is_canceled = 0 THEN i.amount*i.rate ELSE  i.amount*i.rate*(-1) END)/100) as turnover_ao,
                                  SUM((CASE WHEN i.is_canceled = 0 THEN i.house_result*i.rate ELSE  i.house_result*i.rate*(-1) END)/100) as house_win_ao,
                                  SUM(CASE WHEN i.is_canceled = 0 THEN 1 ELSE -1 END) investments_num
                  FROM   investments i
                  WHERE  i.is_settled = 1
                       AND i.writer_id in (1,10000,21000) --web
                       AND (to_char(i.time_settled,'dd/mm/yyyy') = to_char(sysdate-1/24,'dd/mm/yyyy')
                                  OR
                                  to_char(i.time_canceled,'dd/mm/yyyy') = to_char(sysdate-1/24,'dd/mm/yyyy'))
                      AND i.user_id in (select u2.id
                                        from users u2
                                        )
                  GROUP BY  i.user_id) sums_investments on u.id = sums_investments.user_id

WHERE
     u.affiliate_system_user_time is not null
     AND u.class_id != 0
     AND(deposits.sum_deposits_ao <> 0
     		or sums_investments.house_win_ao <> 0
            or sums_investments.turnover_ao <> 0
            or withdraws.sum_withdraws_ao <> 0
            or sums_investments.investments_num <> 0
            or sum_fix_balances.fixBalance <> 0
            or cft_withdraws.cft_withdraws_num <>0
            or wire_withdraws.wire_withdraws_fee <> 0 )
	AND u.affiliate_system = 1                  

union 
                  
SELECT
          u.id customerID,
          to_char(sysdate-1/24,'yyyy-mm-dd') "Activity Date",
          'anyoption' "Product ID",
          0 "Bonuses",
          (NVL(deposits.sum_deposits_ao,0)) *0.06 + NVL(cft_withdraws.cft_withdraws_num * 2,0)
                  + NVL (wire_withdraws.wire_withdraws_fee,0) "Adjustments",
          NVL(deposits.sum_deposits_ao,0)  "Deposits",
          NVL(sums_investments.house_win_ao,0)  - NVL(sum_fix_balances.fixBalance,0)  "Gross Revenue",
          NVL(sums_investments.turnover_ao,0) "Turnover",
          NVL(withdraws.sum_withdraws_ao,0) "payouts",
          NVL(sums_investments.investments_num,0) "Transactions",
          decode(u.skin_id, 22, 2, 9, 3, 20, 3, 1) "BrandID",
          'mobile' "Origin"
FROM  
              users u
              LEFT JOIN
                (SELECT 
                        t.user_id,
                        SUM((CASE WHEN (t.status_id IN (2, 7)) THEN  t.amount*t.rate ELSE t.amount*t.rate*(-1) END) /100) as sum_deposits_ao
                  FROM 
                        transaction_types tt, 
                        transactions t 
                              left join charge_backs cb on cb.id = t.CHARGE_BACK_ID
                  WHERE t.type_id = tt.id
                        AND t.writer_id in (200,20000) -- mobile
                        AND tt.class_type in (1, 11)   --  11: deposit by company
                        AND (
                                     (t.status_id IN (2, 7)  AND to_char(t.time_created,'dd/mm/yyyy') = to_char(sysdate-1/24,'dd/mm/yyyy'))
                                     OR
                                     (t.status_id IN (8,12,13)  AND to_char(t.time_settled,'dd/mm/yyyy') = to_char(sysdate-1/24,'dd/mm/yyyy') AND
                                      to_char(t.time_settled,'dd/mm/yyyy') <> to_char(t.time_created,'dd/mm/yyyy') )
                                      OR
                                      (t.status_id = 14 AND to_char(cb.time_created,'dd/mm/yyyy') = to_char(sysdate-1/24,'dd/mm/yyyy') AND
                                      to_char(t.time_created,'dd/mm/yyyy') <> to_char(cb.time_created,'dd/mm/yyyy'))
                                   )
                        AND t.user_id in (select u2.id
                                           from users u2
                                           )
                  GROUP BY t.user_id) deposits on u.id = deposits.user_id
                  ---------------------------------------------------------------------
                  LEFT JOIN
                                    (SELECT  t.user_id, SUM(CASE WHEN t.type_id = 22 THEN
                                                                           -- if  TRANS_TYPE_FIX_BALANCE_DEPOSIT
                                                                           t.amount * t.rate
                                                                       ELSE
                                                                           -- if  TRANS_TYPE_FIX_BALANCE_WITHDRAW
                                                                           t.amount * t.rate * (-1) END)/100 fixBalance
                  FROM transactions t
                  WHERE
                          t.writer_id in (200,20000) AND -- mobile
                          t.type_id in (22,23) AND
                          to_char(t.time_created,'dd/mm/yyyy') = to_char(sysdate-1/24,'dd/mm/yyyy') AND
                          t.user_id in (select u2.id
                                        from users u2
                                        )
                  GROUP BY t.user_id) sum_fix_balances on u.id = sum_fix_balances.user_id
                  ---------------------------------------------------------------------
                 LEFT JOIN
                (SELECT t.user_id,  SUM(t.amount*t.rate/100) as sum_withdraws_ao
                  FROM transactions t, transaction_types tt
                  WHERE t.type_id = tt.id
                        AND t.writer_id in (200,20000) -- mobile
                        AND tt.class_type = 2
                        AND t.status_id IN (2)
                        AND to_char(t.time_settled,'dd/mm/yyyy') = to_char(sysdate-1/24,'dd/mm/yyyy')
                        AND t.user_id in (select u2.id
                                          from users u2
                                          )
                  GROUP BY t.user_id) withdraws on u.id = withdraws.user_id
                ---------------------------------------------------------------------
                LEFT JOIN
                (SELECT t.user_id,  COUNT(t.id) as cft_withdraws_num
                  FROM transactions t
                  WHERE t.type_id = 10 -- CC withdraw
                        AND t.writer_id in (200,20000) -- mobile
                        AND t.status_id IN (2)
                        AND t.clearing_provider_id = 2 -- Xor Anyoption
                        AND t.is_credit_withdrawal = 0
                        AND to_char(t.time_settled,'dd/mm/yyyy') = to_char(sysdate-1/24,'dd/mm/yyyy')
                        AND t.user_id in (select u2.id
                                          from users u2
                                          )
                  GROUP BY t.user_id) cft_withdraws on u.id = cft_withdraws.user_id
                  ---------------------------------------------------------------------
                   LEFT JOIN
                (SELECT t.user_id,  SUM(w.bank_fee_amount* t.rate) as wire_withdraws_fee
                  FROM transactions t, wires w
                  WHERE t.type_id = 14 -- Bank wire withdraw
                        AND t.writer_id in (200,20000) -- mobile
                        AND t.status_id IN (2)
                        AND t.wire_id = w.id
                        AND to_char(t.time_settled,'dd/mm/yyyy') = to_char(sysdate-1/24,'dd/mm/yyyy')
                        AND t.user_id in (select u2.id
                                          from users u2
                                          )
                  GROUP BY t.user_id) wire_withdraws on u.id = wire_withdraws.user_id
                 ---------------------------------------------------------------------
                  LEFT JOIN
                 (SELECT i.user_id,
                                  SUM((CASE WHEN i.is_canceled = 0 THEN i.amount*i.rate ELSE  i.amount*i.rate*(-1) END)/100) as turnover_ao,
                                  SUM((CASE WHEN i.is_canceled = 0 THEN i.house_result*i.rate ELSE  i.house_result*i.rate*(-1) END)/100) as house_win_ao,
                                  SUM(CASE WHEN i.is_canceled = 0 THEN 1 ELSE -1 END) investments_num
                  FROM   investments i
                  WHERE  i.is_settled = 1
                       AND i.writer_id in (200,20000) -- mobile
                       AND (to_char(i.time_settled,'dd/mm/yyyy') = to_char(sysdate-1/24,'dd/mm/yyyy')
                                  OR
                                  to_char(i.time_canceled,'dd/mm/yyyy') = to_char(sysdate-1/24,'dd/mm/yyyy'))
                      AND i.user_id in (select u2.id
                                        from users u2
                                        )
                  GROUP BY  i.user_id) sums_investments on u.id = sums_investments.user_id
                    ---------------------------------------------------------------------
WHERE 
    u.affiliate_system_user_time is not null
    AND u.class_id != 0      
    AND(deposits.sum_deposits_ao <> 0
    		or sums_investments.house_win_ao <> 0
            or sums_investments.turnover_ao <> 0
            or withdraws.sum_withdraws_ao <> 0
            or sums_investments.investments_num <> 0
            or sum_fix_balances.fixBalance <> 0
            or cft_withdraws.cft_withdraws_num <>0
            or wire_withdraws.wire_withdraws_fee <> 0 )
  	AND u.affiliate_system = 1                  

union

SELECT
          u.id customerID,
          to_char(sysdate-1/24,'yyyy-mm-dd') "Activity Date",
          'anyoption' "Product ID",
          NVL(sum_bonuses.bonuses,0) "Bonuses",
          (NVL(deposits.sum_deposits_ao,0)) *0.06 + NVL(cft_withdraws.cft_withdraws_num * 2,0)
                  + NVL (wire_withdraws.wire_withdraws_fee,0) "Adjustments",
          NVL(deposits.sum_deposits_ao,0)  "Deposits",
          NVL(sums_investments.house_win_ao,0)  - NVL(sum_fix_balances.fixBalance,0)  "Gross Revenue",
          NVL(sums_investments.turnover_ao,0) "Turnover",
          NVL(withdraws.sum_withdraws_ao,0) "payouts",
          NVL(sums_investments.investments_num,0) "Transactions",
          decode(u.skin_id, 22, 2, 9, 3, 20, 3, 1) "BrandID",
          'other' "Origin"
FROM  
              users u
              LEFT JOIN
                (SELECT 
                        t.user_id,
                        SUM((CASE WHEN (t.status_id IN (2, 7)) THEN  t.amount*t.rate ELSE t.amount*t.rate*(-1) END) /100) as sum_deposits_ao
                  FROM 
                        transaction_types tt, 
                        transactions t 
                              left join charge_backs cb on cb.id = t.CHARGE_BACK_ID
                  WHERE t.type_id = tt.id
                        AND t.writer_id not in (1,10000,200,20000,21000) --other
                        AND tt.class_type in (1, 11)   --  11: deposit by company
                        AND (
                                     (t.status_id IN (2, 7)  AND to_char(t.time_created,'dd/mm/yyyy') = to_char(sysdate-1/24,'dd/mm/yyyy'))
                                     OR
                                     (t.status_id IN (8,12,13)  AND to_char(t.time_settled,'dd/mm/yyyy') = to_char(sysdate-1/24,'dd/mm/yyyy') AND
                                      to_char(t.time_settled,'dd/mm/yyyy') <> to_char(t.time_created,'dd/mm/yyyy') )
                                      OR
                                      (t.status_id = 14 AND to_char(cb.time_created,'dd/mm/yyyy') = to_char(sysdate-1/24,'dd/mm/yyyy') AND
                                      to_char(t.time_created,'dd/mm/yyyy') <> to_char(cb.time_created,'dd/mm/yyyy'))
                                   )
                        AND t.user_id in (select u2.id
                                           from users u2
                                           )
                  GROUP BY t.user_id) deposits on u.id = deposits.user_id
                  ---------------------------------------------------------------------
                  LEFT JOIN
                                    (SELECT  t.user_id, SUM(CASE WHEN t.type_id = 22 THEN
                                                                           -- if  TRANS_TYPE_FIX_BALANCE_DEPOSIT
                                                                           t.amount * t.rate
                                                                       ELSE
                                                                           -- if  TRANS_TYPE_FIX_BALANCE_WITHDRAW
                                                                           t.amount * t.rate * (-1) END)/100 fixBalance
                  FROM transactions t
                  WHERE
                          t.writer_id not in (1,10000,200,20000,21000) AND --other
                          t.type_id in (22,23) AND
                          to_char(t.time_created,'dd/mm/yyyy') = to_char(sysdate-1/24,'dd/mm/yyyy') AND
                          t.user_id in (select u2.id
                                        from users u2
                                        )
                  GROUP BY t.user_id) sum_fix_balances on u.id = sum_fix_balances.user_id
                  ---------------------------------------------------------------------
                 LEFT JOIN
                (SELECT t.user_id,  SUM(t.amount*t.rate/100) as sum_withdraws_ao
                  FROM transactions t, transaction_types tt
                  WHERE t.type_id = tt.id
                        AND t.writer_id not in (1,10000,200,20000,21000) --other
                        AND tt.class_type = 2
                        AND t.status_id IN (2)
                        AND to_char(t.time_settled,'dd/mm/yyyy') = to_char(sysdate-1/24,'dd/mm/yyyy')
                        AND t.user_id in (select u2.id
                                          from users u2
                                          )
                  GROUP BY t.user_id) withdraws on u.id = withdraws.user_id
                ---------------------------------------------------------------------
                LEFT JOIN
                (SELECT t.user_id,  COUNT(t.id) as cft_withdraws_num
                  FROM transactions t
                  WHERE t.type_id = 10 -- CC withdraw
                        AND t.writer_id not in (1,10000,200,20000,21000) --other
                        AND t.status_id IN (2)
                        AND t.clearing_provider_id = 2 -- Xor Anyoption
                        AND t.is_credit_withdrawal = 0
                        AND to_char(t.time_settled,'dd/mm/yyyy') = to_char(sysdate-1/24,'dd/mm/yyyy')
                        AND t.user_id in (select u2.id
                                          from users u2
                                          )
                  GROUP BY t.user_id) cft_withdraws on u.id = cft_withdraws.user_id
                  ---------------------------------------------------------------------
                   LEFT JOIN
                (SELECT t.user_id,  SUM(w.bank_fee_amount* t.rate) as wire_withdraws_fee
                  FROM transactions t, wires w
                  WHERE t.type_id = 14 -- Bank wire withdraw
                        AND t.writer_id not in (1,10000,200,20000,21000) --other
                        AND t.status_id IN (2)
                        AND t.wire_id = w.id
                        AND to_char(t.time_settled,'dd/mm/yyyy') = to_char(sysdate-1/24,'dd/mm/yyyy')
                        AND t.user_id in (select u2.id
                                          from users u2
                                          )
                  GROUP BY t.user_id) wire_withdraws on u.id = wire_withdraws.user_id
                 ---------------------------------------------------------------------
                  LEFT JOIN
                 (SELECT i.user_id,
                                  SUM((CASE WHEN i.is_canceled = 0 THEN i.amount*i.rate ELSE  i.amount*i.rate*(-1) END)/100) as turnover_ao,
                                  SUM((CASE WHEN i.is_canceled = 0 THEN i.house_result*i.rate ELSE  i.house_result*i.rate*(-1) END)/100) as house_win_ao,
                                  SUM(CASE WHEN i.is_canceled = 0 THEN 1 ELSE -1 END) investments_num
                  FROM   investments i
                  WHERE  i.is_settled = 1
                       AND i.writer_id not in (1,10000,200,20000,21000) --other
                       AND (to_char(i.time_settled,'dd/mm/yyyy') = to_char(sysdate-1/24,'dd/mm/yyyy')
                                  OR
                                  to_char(i.time_canceled,'dd/mm/yyyy') = to_char(sysdate-1/24,'dd/mm/yyyy'))
                      AND i.user_id in (select u2.id
                                        from users u2
                                        )
                  GROUP BY  i.user_id) sums_investments on u.id = sums_investments.user_id
                  ---------------------------------------------------------------------
                   LEFT JOIN
                  (SELECT bu.user_id, SUM(CASE WHEN bt.class_type_id = 3  THEN
                                                          NVL(t.amount ,0) * t.rate -- Next invest on us
                                                      ELSE
                                                            bu.bonus_amount * t.rate -- Not next invest on us
                                                      END
                                                      )/100  bonuses
                    FROM  bonus_types bt,
                                  bonus_users bu
                                        LEFT JOIN investments i ON bu.id = i.bonus_user_id
                                        LEFT JOIN transactions t ON bu.id = t.bonus_user_id    and t.type_id=12
                    WHERE bt.id = bu.type_id                     
                            AND ((bu.time_used is not null AND to_char(bu.time_used,'dd/mm/yyyy') = to_char(sysdate-1/24 ,'dd/mm/yyyy'))
                                      OR
                                      (bu.time_used is null AND bu.time_done is not null AND to_char(bu.time_done,'dd/mm/yyyy') = to_char(sysdate-1/24 ,'dd/mm/yyyy')))
                                      -- Next invest on us             -- Not next invest on us
                            AND  NOT(bt.class_type_id = 3 AND i.bonus_odds_change_type_id = 1  AND  (i.win-i.lose) > 0 )
                            AND bu.user_id in (select u2.id
                                               from users u2
                                               )
                    GROUP BY bu.user_id) sum_bonuses on u.id = sum_bonuses.user_id
                    ---------------------------------------------------------------------
WHERE 
    u.affiliate_system_user_time is not null 
    AND u.class_id != 0
    AND(sum_bonuses.bonuses <> 0
    		or deposits.sum_deposits_ao <> 0
            or sums_investments.house_win_ao <> 0
            or sums_investments.turnover_ao <> 0
            or withdraws.sum_withdraws_ao <> 0
            or sums_investments.investments_num <> 0
            or sum_fix_balances.fixBalance <> 0
            or cft_withdraws.cft_withdraws_num <>0
            or wire_withdraws.wire_withdraws_fee <> 0 )
	AND u.affiliate_system = 1                  
)                  
 order by customerID