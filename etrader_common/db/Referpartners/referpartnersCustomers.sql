SELECT 
    u.id CustomerID,
    c.a2 Country_ID,
    nvl(replace(u.dynamic_param, ' ', ''),' ') as  Tag,
    u.ip Registration_ip,
    nvl(to_char(u.time_created,'yyyy-mm-dd'), ' ') Registration_Date,
    c.COUNTRIES_GROUP_NETREFER_ID CustomerTypeID
FROM 
    users u,
    countries c
WHERE
    u.country_id = c.id
    and trunc(u.affiliate_system_user_time) = trunc(sysdate-1/24)
    and u.class_id <> 0
    and u.affiliate_system = 2;